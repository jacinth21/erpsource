/*
 * Module: GmLogisticsBean.java Author: Dhinakaran James Project: Globus Medical App Date-Written:
 * 01 Mar 2006 Security: Unclassified Description: GmLogisticsBean Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.operations.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmAutoCompleteTransBean;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmTxnSplitBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.inventory.beans.GmLocationBean;
import com.globus.operations.shipping.beans.GmShippingTransBean;
import com.globus.prodmgmnt.beans.GmPartNumSearchBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmLogisticsBean extends GmBean {

  public GmLogisticsBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmLogisticsBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  GmCommonClass gmCommon = new GmCommonClass();
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  // Initialize
  // the
  // Logger
  // Class.

  /**
   * loadConsignDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadConsignDetails(String strConsignId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    HashMap hmReturn = new HashMap();

    try {
      sbQuery.append(" SELECT C504_CONSIGNMENT_ID CID, GET_CONSIGN_BILL_ADD('");
      sbQuery.append(strConsignId);
      sbQuery
          .append("') BILLADD , to_char(C504_LAST_UPDATED_DATE,GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) UDATE, GET_CONSIGN_SHIP_ADD('");
      sbQuery.append(strConsignId);
      sbQuery
          .append("') SHIPADD , GET_USER_NAME(C504_LAST_UPDATED_BY) NAME,GET_USER_NAME(C504_CREATED_BY) CREATEDBY,");
      sbQuery.append(" C504_DELIVERY_CARRIER SCARR, C504_DELIVERY_MODE SMODE, ");
      sbQuery
          .append(" C504_TRACKING_NUMBER TRACK, to_char(C504_SHIP_DATE,GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) SDATE ");
      sbQuery.append(" FROM T504_CONSIGNMENT ");
      sbQuery.append(" WHERE C504_CONSIGNMENT_ID = '");
      sbQuery.append(strConsignId);
      sbQuery.append("'");
      sbQuery.append(" AND C504_VOID_FL IS NULL ");

      hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmLogisticsBean:loadConsignDetails", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadConsignDetails

  /**
   * loadConsignAckDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadConsignAckDetails(String strConsignId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    HashMap hmReturn = new HashMap();

    try {
      sbQuery.append(" SELECT A.C504_CONSIGNMENT_ID CID, B.C701_DISTRIBUTOR_NAME NAME, ");
      sbQuery.append(" to_char(A.C504_LAST_UPDATED_DATE,'Day Monthdd,yyyy' ) UDATE, ");
      sbQuery.append(" to_char(A.C504_LAST_UPDATED_DATE+1,'Day Monthdd,yyyy') NDATE ");
      sbQuery.append(" FROM T504_CONSIGNMENT A, T701_DISTRIBUTOR B ");
      sbQuery.append(" WHERE A.C504_CONSIGNMENT_ID = '");
      sbQuery.append(strConsignId);
      sbQuery.append("' AND A.C701_DISTRIBUTOR_ID = B.C701_DISTRIBUTOR_ID ");
      sbQuery.append(" AND A.C504_VOID_FL IS NULL ");

      hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmLogisticsBean:loadConsignAckDetails", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadConsignAckDetails

  /**
   * loadConsignLists - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadConsignLists(String strOpt, HashMap hmTransRules) throws AppError {
    GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    ArrayList alPurpose = new ArrayList();
    ArrayList alEmpList = new ArrayList();
    alEmpList = gmLogon.getEmployeeList();

    hmTransRules = GmCommonClass.parseNullHashMap(hmTransRules);
    String strCodeList = "";

    try {
      if (strOpt.equals("InHouse")) {
        alPurpose = gmCommon.getCodeList("CONPU");
      } else if (strOpt.equals("Repack")) {
        alPurpose = gmCommon.getCodeList("CONPP");
      } else if (strOpt.equals("QuaranIn")) {
        alPurpose = gmCommon.getCodeList("CONQI");
      } else if (strOpt.equals("QuaranOut")) {
        alPurpose = gmCommon.getCodeList("CONSC");
      } else if (strOpt.equals("InvenIn")) {
        alPurpose = gmCommon.getCodeList("CONIN");
      } else if (strOpt.equals("BulkOut")) {
        alPurpose = gmCommon.getCodeList("CONBO");
      } else if (strOpt.equals("BulkIn")) {
        alPurpose = gmCommon.getCodeList("CONBI");
      } else if (strOpt.equals("InHouseCsg")) {
        alPurpose = gmCommon.getCodeList("CONBI");
      } else if (strOpt.equals("RawMatScrap")) {
        alPurpose = gmCommon.getCodeList("RAWSC");
      } else if (strOpt.equals("RawMatQuar")) {
        alPurpose = gmCommon.getCodeList("RAWQN");
      } else if (strOpt.equals("RawMatFG")) {
        alPurpose = gmCommon.getCodeList("RAWFG");
      } else if (strOpt.equals("FGRawMat")) {
        alPurpose = gmCommon.getCodeList("FGRAW");
      }

      hmReturn.put("EMPLIST", alEmpList);
      hmReturn.put("PURPOSE", alPurpose);

      if (hmTransRules.size() > 0) {

        strCodeList = GmCommonClass.parseNull((String) hmTransRules.get("TRANS_CODELIST"));
        if (!strCodeList.equals("")) {
          alEmpList = gmLogon.getEmployeeList();
          alPurpose = gmCommon.getCodeList(strCodeList);
          hmReturn.put("EMPLIST", alEmpList);
          hmReturn.put("PURPOSE", alPurpose);
        }
      }
    } catch (Exception e) {
      GmLogError.log("Exception in GmLogisticsBean:loadConsignLists", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadConsignLists

  /**
   * loadCartForConsign - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadCartForConsign(String strPartNum, String strTransType) throws AppError {

    GmPartNumSearchBean gmPartSearchBean = new GmPartNumSearchBean(getGmDataStoreVO());

    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    String strPartCompFiltr = "";
    StringTokenizer strTok = new StringTokenizer(strPartNum, ",");
    String strCompanyID = getCompId();

    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      strTransType = strTransType.equals("") ? "NULL" : strTransType;

      while (strTok.hasMoreTokens()) {
        strPartNum = strTok.nextToken();
        strPartCompFiltr = gmPartSearchBean.getPartCompanyFilter(strPartNum, "T205");
        StringBuffer sbQuery = new StringBuffer();
        sbQuery
            .append(" SELECT T205.C205_PART_NUMBER_ID ID, NVL(T2052.C2052_EQUITY_PRICE,0.0) PRICE, ");
        sbQuery
            .append(" T205.C205_PART_NUM_DESC PDESC , GET_QTY_IN_STOCK(T205.C205_PART_NUMBER_ID) STOCK, ");
        sbQuery.append(" GET_QTY_IN_QUARANTINE(T205.C205_PART_NUMBER_ID) QSTOCK , ");
        sbQuery.append(" GET_QTY_IN_PACKAGING(T205.C205_PART_NUMBER_ID) REPACK_STOCK, ");
        sbQuery
            .append(" GET_QTY_IN_BULK(T205.C205_PART_NUMBER_ID) BSTOCK, GET_PARTNUMBER_FA_WIP_QTY(T205.C205_PART_NUMBER_ID) WIPQTY, ");
        if (strTransType.equals("90816")) {
          sbQuery.append("GET_QTY_IN_INHOUSEINV(T205.C205_PART_NUMBER_ID) AQTY ");
        } else {
          sbQuery.append(" Get_Partnumber_Qty(T205.C205_PART_NUMBER_ID,");
          sbQuery.append(strTransType);
          sbQuery.append(") - get_qty_in_transaction_rm(T205.C205_PART_NUMBER_ID) AQTY");
        }
        sbQuery.append(", '0' IASTOCK,GET_QTY_IN_INV(T205.C205_PART_NUMBER_ID," + strTransType
            + ") QTY_IN_INV "); // IASTOCK - from
        // Inventory
        // Adjustment should
        // always show 0,
        // OSTOCK
        // ORW STOCK QTY
        sbQuery.append(" FROM T205_PART_NUMBER T205 ,T2052_PART_PRICE_MAPPING T2052");
        sbQuery.append(" WHERE T205.C205_PART_NUMBER_ID = '");
        sbQuery.append(strPartNum);
        sbQuery.append("'");
        sbQuery.append(" AND T205.C205_PART_NUMBER_ID  = T2052.C205_PART_NUMBER_ID(+) ");
        sbQuery.append(strPartCompFiltr);
        sbQuery.append(" AND T2052.C1900_COMPANY_ID(+) = '");
        sbQuery.append(strCompanyID);
        sbQuery.append("'");
        sbQuery.append(" AND t2052.c2052_void_fl(+) is null ");
        log.debug(" ## # # loadCart:" + sbQuery.toString());
        hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
        hmResult.put(strPartNum, hmReturn);
      }

    } catch (Exception e) {
      GmLogError.log("Exception in GmLogisticsBean:loadCartForConsign", "Exception is:" + e);
    }
    return hmResult;
  } // End of loadCartForConsign

  /**
   * loadNextConsignId - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public String loadNextConsignId(String strOpt) throws AppError {
    HashMap hmReturn = new HashMap();
    String strConsignId = "";
    StringBuffer sbQuery = new StringBuffer();
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      sbQuery.append(" SELECT	GET_NEXT_CONSIGN_ID('");
      sbQuery.append(strOpt);
      sbQuery.append("') CID FROM DUAL ");
      hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
      strConsignId = (String) hmReturn.get("CID");

    } catch (Exception e) {
      GmLogError.log("Exception in GmLogisticsBean:loadNextConsignId", "Exception is:" + e);
    }
    return strConsignId;
  } // End of loadNextConsignId

  /**
   * saveItemConsign - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap saveItemConsign(String strConsignId, HashMap hmParam, String strInputStr,
      String strUsername) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    String strMsg = "";
    String strType = (String) hmParam.get("TYPE");
    String strPurpose = (String) hmParam.get("PURPOSE");
    String strShipTo = GmCommonClass.parseZero((String) hmParam.get("SHIPTO"));
    String strShipToId = (String) hmParam.get("SHIPTOID");
    String strFinalComments = (String) hmParam.get("COMMENTS");

    log.debug(" @@@@@@ GOing to call @@@@@@@@@");

    gmDBManager.setPrepareString("GM_SAVE_INHOUSE_ITEM_CONSIGN", 9);

    gmDBManager.registerOutParameter(9, java.sql.Types.CHAR);
    gmDBManager.setString(1, strConsignId);
    gmDBManager.setInt(2, Integer.parseInt(strType));
    gmDBManager.setInt(3, Integer.parseInt(strPurpose));
    gmDBManager.setInt(4, Integer.parseInt(strShipTo));
    gmDBManager.setString(5, strShipToId);
    gmDBManager.setString(6, strFinalComments);
    gmDBManager.setString(7, strUsername);
    gmDBManager.setString(8, strInputStr);
    gmDBManager.execute();
    strMsg = gmDBManager.getString(9);
    gmDBManager.commit();
    hmReturn.put("MSG", strMsg);

    return hmReturn;
  } // End of saveItemConsign

  /**
   * updateItemConsign - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap updateItemConsign(String strConsignId, String strInputStr, String strShipFl,
      String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    HashMap hmReturn = new HashMap();

    gmDBManager.setPrepareString("GM_SAVE_ITEM_BUILD", 5);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);

    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strShipFl);
    gmDBManager.setString(4, strUserId);

    gmDBManager.execute();
    strMsg = gmDBManager.getString(5);
    gmDBManager.commit();
    hmReturn.put("MSG", strMsg);

    return hmReturn;
  } // End of updateItemConsign

  /**
   * saveConsignShipDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap saveConsignShipDetails(String strConsignId, String strShipDate,
      String strShipCarr, String strShipMode, String strTrack, String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    HashMap hmReturn = new HashMap();

    gmDBManager.setPrepareString("GM_SAVE_CONSIGN_SHIP", 7);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(7, java.sql.Types.CHAR);

    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strShipDate);
    gmDBManager.setInt(3, Integer.parseInt(strShipCarr));
    gmDBManager.setInt(4, Integer.parseInt(strShipMode));
    gmDBManager.setString(5, strTrack);
    gmDBManager.setString(6, strUserId);

    gmDBManager.execute();
    strMsg = gmDBManager.getString(7);
    gmDBManager.close();
    hmReturn.put("MSG", strMsg);

    return hmReturn;
  } // End of saveConsignShipDetails

  /**
   * loadConsignReport - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadConsignReport(String strType, String strDistId, String strSetId,
      String strFromDt, String strToDt) throws AppError {
    GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
    GmCommonClass gmCommon = new GmCommonClass();

    HashMap hmReturn = new HashMap();
    ArrayList alDistributor = new ArrayList();
    ArrayList alSets = new ArrayList();
    ArrayList alReturn = new ArrayList();
    String strIdString = "";

    try {

      alSets = gmProject.loadSetMap("1601");
      hmReturn.put("SETLIST", alSets);

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      if (strType.equals("INHOUSE")) {
        alDistributor = gmCommon.getCodeList("CONPU");
        hmReturn.put("DISTRIBUTORLIST", alDistributor);
        strIdString = "C504_INHOUSE_PURPOSE";
      }

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT C504_CONSIGNMENT_ID CID, C504_COMMENTS COMMENTS, GET_SET_NAME(C207_SET_ID) SNAME, ");
      sbQuery.append(" to_char(C504_SHIP_DATE,GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) SDATE ");
      sbQuery.append(" FROM T504_CONSIGNMENT ");
      sbQuery.append(" WHERE ");
      sbQuery.append(strIdString);
      sbQuery.append(" ='");
      sbQuery.append(strDistId);
      sbQuery.append("'");
      if (!strSetId.equals("00")) {
        sbQuery.append(" AND C207_SET_ID ='");
        sbQuery.append(strSetId);
        sbQuery.append("'");
      }
      sbQuery.append(" AND C504_VOID_FL IS NULL ");
      sbQuery.append(" AND C207_SET_ID IS NOT NULL ");
      sbQuery.append(" AND C504_SHIP_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("',GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("',GET_RULE_VALUE('DATEFMT', 'DATEFORMAT'))");
      sbQuery.append("ORDER BY C504_SHIP_DATE");

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("REPORT", alReturn);

    } catch (Exception e) {
      GmLogError.log("Exception in GmLogisticsBean:loadSalesRep", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadConsignReport

  /**
   * loadConsignReportNew - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadConsignReportNew(String strType, String strFromDt, String strToDt)
      throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();

    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery.append(" SELECT C207_SET_ID ID, GET_SET_NAME(C207_SET_ID) SNAME, ");
      if (strType.equals("SET") || strType.equals("DIST")) {
        sbQuery
            .append(" C701_DISTRIBUTOR_ID DID, GET_DISTRIBUTOR_NAME(C701_DISTRIBUTOR_ID) DNAME, ");
        sbQuery.append(" GET_DIST_SET_COUNT(C207_SET_ID,C701_DISTRIBUTOR_ID,'DIST','");
        sbQuery.append(strFromDt);
        sbQuery.append("','");
        sbQuery.append(strToDt);
        sbQuery.append("') COUNT ");
      } else if (strType.equals("INSET") || strType.equals("INTYPE")) {
        sbQuery.append(" C504_INHOUSE_PURPOSE DID, GET_CODE_NAME(C504_INHOUSE_PURPOSE) DNAME, ");
        sbQuery.append(" GET_DIST_SET_COUNT(C207_SET_ID,C504_INHOUSE_PURPOSE,'INHOUSE','");
        sbQuery.append(strFromDt);
        sbQuery.append("','");
        sbQuery.append(strToDt);
        sbQuery.append("') COUNT ");
      }

      sbQuery.append(" FROM T504_CONSIGNMENT ");
      sbQuery.append(" WHERE C207_SET_ID IS NOT NULL AND C504_STATUS_FL = '4' ");
      sbQuery.append(" AND C504_VOID_FL IS NULL ");
      sbQuery.append(" AND C504_SHIP_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("',GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("',GET_RULE_VALUE('DATEFMT', 'DATEFORMAT'))");

      if (strType.equals("SET") || strType.equals("DIST")) {
        sbQuery.append(" AND C701_DISTRIBUTOR_ID IS NOT NULL ");
        sbQuery.append(" GROUP BY C207_SET_ID, C701_DISTRIBUTOR_ID ");

        if (strType.equals("SET")) {
          sbQuery.append(" ORDER BY SNAME ");
        } else if (strType.equals("DIST")) {
          sbQuery.append(" ORDER BY DNAME, ID ");
        }
      } else if (strType.equals("INSET") || strType.equals("INTYPE")) {
        sbQuery.append(" AND C701_DISTRIBUTOR_ID IS NULL AND C704_ACCOUNT_ID = '01'");
        sbQuery.append(" GROUP BY C207_SET_ID , C504_INHOUSE_PURPOSE ");

        if (strType.equals("INSET")) {
          sbQuery.append(" ORDER BY SNAME ");
        } else if (strType.equals("INTYPE")) {
          sbQuery.append(" ORDER BY DNAME, SNAME ");
        }
      }
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("REPORT", alReturn);

    } catch (Exception e) {
      GmLogError.log("Exception in GmLogisticsBean:loadConsignReportNew", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadConsignReportNew

  /**
   * loadItemConsignReport - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadItemConsignReport(String strType, String strFromDt, String strToDt)
      throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();

    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT C701_DISTRIBUTOR_ID DID, GET_DISTRIBUTOR_NAME(C701_DISTRIBUTOR_ID) DNAME,");
      sbQuery
          .append(" C504_CONSIGNMENT_ID CONID, to_char(C504_SHIP_DATE,GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) SDATE, C504_SHIP_TO_ID SHIPTO, C504_INHOUSE_PURPOSE PID, ");
      sbQuery
          .append(" GET_CODE_NAME(C504_INHOUSE_PURPOSE) PURPOSE, GET_CONSIGN_SHIP_ADD(C504_CONSIGNMENT_ID) EMP, ");
      sbQuery.append(" C504_COMMENTS COMMENTS");
      sbQuery.append(" FROM T504_CONSIGNMENT ");
      sbQuery.append(" WHERE C207_SET_ID IS NULL AND C504_STATUS_FL = '4' ");
      sbQuery.append(" AND C504_VOID_FL IS NULL ");
      sbQuery.append(" AND C504_SHIP_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("',GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("',GET_RULE_VALUE('DATEFMT', 'DATEFORMAT'))");
      if (strType.equals("DIST")) {
        sbQuery.append(" AND C701_DISTRIBUTOR_ID IS NOT NULL ");
        sbQuery.append(" ORDER BY DNAME,C504_CONSIGNMENT_ID DESC");
      } else {
        sbQuery.append(" AND C701_DISTRIBUTOR_ID IS NULL ");
        sbQuery.append(" AND C704_ACCOUNT_ID = '01' ");
        sbQuery.append(" AND C504_TYPE = '4112' ");// For InHouse
        // Consignments,
        // Hardcode // for
        // 4114 also
        sbQuery.append(" ORDER BY C504_INHOUSE_PURPOSE,C504_CONSIGNMENT_ID DESC ");
      }

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

      hmReturn.put("REPORT", alReturn);

    } catch (Exception e) {
      GmLogError.log("Exception in GmLogisticsBean:loadItemConsignReport", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadItemConsignReport

  /**
   * rollConsignmentToInventory - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap rollConsignmentToInventory(String strConsignId, String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strPrepareString = null;

    HashMap hmReturn = new HashMap();

    gmDBManager.setPrepareString("GM_ROLL_CONSIGN_INV", 3);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);

    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strUserId);

    gmDBManager.execute();

    strBeanMsg = gmDBManager.getString(3);
    gmDBManager.close();
    hmReturn.put("MSG", strBeanMsg);

    return hmReturn;
  } // End of rollConsignmentToInventory

  /**
   * updateReprocessMaterial - This method will
   * 
   * @param String strUsername
   * @return HashMap
   * @exception AppError
   */
  public HashMap updateReprocessMaterial(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmLocationBean gmLocationBean = new GmLocationBean(getGmDataStoreVO());

    String strMsg = "";
    log.debug("hmParam = " + hmParam);
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strConsignId = GmCommonClass.parseNull((String) hmParam.get("CNID"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("CNTYPE"));
    String strVerifiedBy = GmCommonClass.parseNull((String) hmParam.get("VERIFYBY"));
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("LOGREASON"));
    String strPnumLcn = GmCommonClass.parseNull((String) hmParam.get("PNUMLCNSTR"));
    String strOperation = GmCommonClass.parseNull((String) hmParam.get("OPERATION"));
    gmDBManager.setPrepareString("GM_UPDATE_REPROCESS_MATERIAL", 6);
    gmDBManager.registerOutParameter(6, java.sql.Types.CHAR);
    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strComments);
    gmDBManager.setString(3, strType);
    gmDBManager.setString(4, strVerifiedBy);
    gmDBManager.setString(5, strUserID);
    gmDBManager.execute();

    strMsg = gmDBManager.getString(6);
    hmReturn.put("MSG", strMsg);

    if (strType.equals("4113") && !strLogReason.equals(""))// ---4113
    // Quarantine
    {
      gmCommonBean.saveLog(gmDBManager, strConsignId, strLogReason, strUserID, strType);
    }
    if (strType.equals("4116") || strType.equals("4114") || strType.equals("4118")) {
      gmLocationBean.savePartLocationDetails(gmDBManager, strConsignId, strPnumLcn, strOperation,
          strUserID);
    }
    gmDBManager.commit();
    return hmReturn;
  } // end of updateReprocessMaterial

  /**
   * reportDashboardItems - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public ArrayList reportDashboardItems() throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT * FROM (");
    sbQuery
        .append(" SELECT C504_CONSIGNMENT_ID CID, NVL(GET_RULE_VALUE('CON_TYPE',C504_TYPE),'C') CON_TYPE,");
    sbQuery
        .append(" decode(C504_STATUS_FL,2,'Y','-') CONTROLFL, C504_TYPE CTYPE, GET_CODE_NAME(C504_TYPE) TNAME, ");
    sbQuery
        .append(" C504_SHIP_REQ_FL SHIPREQFL, C504_COMMENTS COMMENTS, GET_USER_NAME(C504_SHIP_TO_ID) UNAME, ");
    sbQuery
        .append(" to_char(C504_CREATED_DATE,get_rule_value ('DATEFMT','DATEFORMAT')) CDATE , C504_REPROCESS_ID REFID ");
    sbQuery.append(" FROM T504_CONSIGNMENT ");
    sbQuery.append(" WHERE C504_STATUS_FL > 1 AND C504_STATUS_FL < 4 AND C207_SET_ID IS NULL ");
    sbQuery.append(" AND C504_VOID_FL IS NULL ");
    sbQuery.append(" AND C704_ACCOUNT_ID = '01' ");
    sbQuery.append(" AND c5040_plant_id = " + getCompPlantId());
    sbQuery.append(" AND C504_TYPE IN (40051,40052,40053,40054,40055,40056,100069,100075,100076) ");
    sbQuery.append(" UNION ");
    sbQuery
        .append(" select C412_INHOUSE_TRANS_ID CID , NVL(GET_RULE_VALUE('CON_TYPE',C412_TYPE),'I') CON_TYPE, decode(C412_STATUS_FL,2,'Y','-') CONTROLFL , C412_TYPE CTYPE,");
    sbQuery
        .append(" GET_CODE_NAME(C412_TYPE) TNAME, decode(C412_VERIFY_FL,NULL,'1','-') SHIPREQFL, C412_COMMENTS COMMENTS,");
    sbQuery
        .append(" GET_USER_NAME(C412_CREATED_BY) UNAME, TO_CHAR(C412_CREATED_DATE,get_rule_value ('DATEFMT','DATEFORMAT')) CDATE, C412_REF_ID REFID ");
    sbQuery.append(" FROM T412_INHOUSE_TRANSACTIONS");
    sbQuery
        .append(" WHERE C412_STATUS_FL > 1 AND C412_STATUS_FL < 4 AND C412_TYPE IN (40051,40052,40053,40054,40055,40056,100069,100075,100076,56025,56045) ");
    sbQuery.append(" AND c5040_plant_id = " + getCompPlantId());
    sbQuery.append(" AND c412_void_fl IS NULL");
    sbQuery.append(")ORDER BY TNAME, CDATE DESC");

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alReturn;
  } // End of reportDashboardItems

  /**
   * viewInHouseTransDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap viewInHouseTransDetails(String strConsignId) {
    HashMap hmReturn = new HashMap();

    StringBuffer sbQuery = new StringBuffer();
    String strCompDateFormat = new String();

    String strLoanerFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "LOANERRECONFILTER"));
    strLoanerFilter = strLoanerFilter.equals("") ? getCompId() : getCompPlantId();

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      strCompDateFormat = getCompDateFmt();
      log.debug("strCompDateFormat==========" + strCompDateFormat);
      // String strSource = "50183";
      sbQuery.append(" SELECT t412.C412_INHOUSE_TRANS_ID CID, ");
      sbQuery
          .append(" DECODE(t412.C412_TYPE,'9112',DECODE (NVL(t412.C412_RELEASE_TO_ID,'0'),'0',GET_ACCOUNT_NAME ('01'),GET_USER_NAME (t412.C412_RELEASE_TO_ID)),"
              + "                               '1006571',DECODE (NVL(t412.C412_RELEASE_TO_ID,'0'),'0',GET_ACCOUNT_NAME ('01'),GET_USER_NAME (t412.C412_RELEASE_TO_ID)),GET_ACCOUNT_NAME('01')) ANAME,");
      sbQuery
          .append(" GET_USER_NAME(t412.C412_CREATED_BY) UNAME,GET_ACCOUNT_NAME(t504.C704_ACCOUNT_ID) ACCNAME ,");
      sbQuery.append(" to_char(t412.C412_CREATED_DATE,'" + strCompDateFormat
          + "'|| ' HH:MI AM') CDATE, t412.C412_VERIFY_FL VFL, ");
      sbQuery.append("t907.C907_SHIPPED_DT  SHIPDT, ");
      sbQuery
          .append(" t412.C412_STATUS_FL SFL,get_transaction_status(t412.C412_STATUS_FL, '4110') STATUSVALUE , t412.C412_TYPE CTYPE, t412.C412_COMMENTS FCOMMENTS, ");
      sbQuery.append(" t412.C412_COMMENTS COMMENTS, GET_CODE_NAME(t412.C412_TYPE) TYPE,");
      sbQuery.append(" GET_LOG_COMMENTS('" + strConsignId + "','1241') LOGCOMMENTS,");
      sbQuery
          .append(" t412.C412_VERIFIED_BY VUSER, t412.C412_VERIFIED_DATE VDATE, GET_USER_NAME(t412.C412_RELEASE_TO_ID) RNAME, ");
      sbQuery
          .append(" GET_CODE_NAME(t412.C412_INHOUSE_PURPOSE) PURP, GET_USER_NAME(t412.C412_LAST_UPDATED_BY) LUNAME, ");
      sbQuery.append(" to_char(t412.C412_LAST_UPDATED_DATE,'" + strCompDateFormat
          + "'|| ' HH:MI AM') LUDATE,");
      sbQuery
          .append(" DECODE (get_cs_rep_loaner_bill_nm_add (t412.c412_ref_id),' ', 'Globus Medical',");

      sbQuery.append(" get_cs_rep_loaner_bill_nm_add (t412.c412_ref_id)) billnm");
      sbQuery
          .append(" ,DECODE (get_cs_loaner_hist_bill_nm_add (t412.c412_ref_id,C504A_LOANER_TRANSACTION_ID),' ', 'Globus Medical',");
      sbQuery
          .append(" get_cs_loaner_hist_bill_nm_add (t412.c412_ref_id,C504A_LOANER_TRANSACTION_ID)) histbillnm");
      sbQuery.append(" ,GET_TRANS_LOCATION_ADDRESS('");
      sbQuery.append(strConsignId);
      sbQuery.append("',t412.C412_TYPE) trbillnm");
      sbQuery
          .append(" ,Gm_pkg_CM_shipping_info.get_ship_add(t412.c412_inhouse_trans_id,decode(C412_TYPE,50154,'50183','')) shipadd");
      sbQuery
          .append(" , gm_pkg_cm_shipping_info.get_track_no(t412.c412_inhouse_trans_id, '50183') TRACKNO,");
      sbQuery
          .append(" gm_pkg_cm_shipping_info.get_carrier(t412.c412_inhouse_trans_id, '50183') CARRIER,");
      sbQuery
          .append(" gm_pkg_cm_shipping_info.get_ship_mode(t412.c412_inhouse_trans_id, '50183') SHIPMODE");
      sbQuery.append(" , GM_PKG_OP_REQUEST_MASTER.GET_TAG_ID(t412.C412_REF_ID) TAGID ");
      sbQuery
          .append(" ,t412.c412_ref_id REFID, t504.c207_set_id SETID, get_set_name (t504.c207_set_id) SETNM ");
      sbQuery.append(" ,get_cs_fch_loaner_etchid (t412.c412_ref_id) ETCHID ");
      sbQuery
          .append(" ,decode(nvl(t504.c704_account_id,''),'','',GET_REP_NAME(GET_REP_ID(t504.c704_account_id))) REPNM ");
      sbQuery
          .append(" ,decode(nvl(t504.c701_distributor_id,''),'','',GET_DISTRIBUTOR_NAME(t504.c701_distributor_id)) DISTNAME ");
      sbQuery
          .append(" ,get_compid_from_distid(t504.C701_DISTRIBUTOR_ID) COMPANYID , t412.c1900_company_id COMPANYCD ,gm_pkg_op_loaner.get_fgle_surgery_date(t412.C412_INHOUSE_TRANS_ID) SURGERYDATE,GET_CS_SHIP_NAME(t907.C901_SHIP_TO,t907.C907_SHIP_TO_ID) SHIPTONAME");
      sbQuery
      .append(" ,gm_pkg_op_storage_building.get_building_short_name(t412.c5057_building_id) BUILDINGNAME");//for PMT-36675
      sbQuery
      .append(" , DECODE(t412.c412_type, 50154, DECODE(t907.c901_ship_to, 4122,t907.c907_ship_to_id,''),'') HOSPACC");  //add HOSPACC for PMT-42950 
      sbQuery
          .append(" FROM T412_INHOUSE_TRANSACTIONS t412, t504_consignment t504,t907_shipping_info t907");
      sbQuery.append(" WHERE t412.C412_INHOUSE_TRANS_ID ='");
      sbQuery.append(strConsignId);
      sbQuery.append("'");
      //Removed company and plant to load the transaction details in different companies PMT-29683
      // Added CompanyId and PlantId for viewInHouseTransDetails
      //sbQuery.append(" AND  (t412.C1900_COMPANY_ID =" + strLoanerFilter);
      //sbQuery.append(" OR  t412.C5040_PLANT_ID =" + strLoanerFilter);
      //sbQuery.append(" ) ");
      sbQuery.append(" AND t412.c412_ref_id = t504.c504_consignment_id (+)");
      sbQuery.append(" AND  t412.C412_INHOUSE_TRANS_ID   = t907.C907_REF_ID(+)");
      sbQuery.append(" AND t907.c907_void_fl(+) is null");
      log.debug("viewInHouseTransDetails " + sbQuery.toString());
      hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmLogisticsBean:viewInHouseTransDetails", "Exception is:" + e);
    }
    return hmReturn;
  } // End of viewInHouseTransDetails

  /**
   * viewInHouseTransItemsDetails - This method will
   * 
   * @param String strConsignId
   * @param String strType
   * @return HashMap
   * @exception AppError
   */
  public HashMap viewInHouseTransItemsDetails(String strConsignId, String strType) throws AppError {
    HashMap hmParam = new HashMap();
    hmParam.put("CONSIGNID", strConsignId);
    hmParam.put("TYPE", strType);
    return viewInHouseTransItemsDetails(hmParam);
  }

  /**
   * viewInHouseTransItemsDetails - This method will
   * 
   * @param HashMap
   * @return HashMap
   * @exception AppError
   */
  public HashMap viewInHouseTransItemsDetails(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    HashMap hmReturn = new HashMap();
    HashMap hmUserName = new HashMap();
    String strLoanerFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "LOANERRECONFILTER"));
    strLoanerFilter = strLoanerFilter.equals("") ? getCompId() : getCompPlantId();

    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      StringBuffer sbQuery = new StringBuffer();
      StringBuffer sbSelectQuery = new StringBuffer();
      String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
      GmResourceBundleBean gmResourceBundleBeanPaperwork =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
      String strSortFlag =
          GmCommonClass.parseNull(gmResourceBundleBeanPaperwork.getProperty("SORT_BY_LOT_FL"));
      String strConsignId = GmCommonClass.parseNull((String) hmParam.get("CONSIGNID"));
      String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
      String strRemVoidFl = GmCommonClass.parseNull((String) hmParam.get("REMVOIDFL"));
      String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
      sbQuery
          .append(" SELECT  C205_PART_NUMBER_ID PNUM, get_partdesc_by_company(C205_PART_NUMBER_ID) PDESC, ");
      sbQuery
          .append(" decode(GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T413.C205_PART_NUMBER_ID),'',decode(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE_AS_CHAR(T413.C205_PART_NUMBER_ID,T413.C413_CONTROL_NUMBER),'','10' || T413.C413_CONTROL_NUMBER,'17' || GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE_AS_CHAR(T413.C205_PART_NUMBER_ID,T413.C413_CONTROL_NUMBER) || '10' || T413.C413_CONTROL_NUMBER),decode(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE_AS_CHAR(T413.C205_PART_NUMBER_ID,T413.C413_CONTROL_NUMBER),'','01' || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T413.C205_PART_NUMBER_ID)  || '10' || T413.C413_CONTROL_NUMBER,'01' || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T413.C205_PART_NUMBER_ID) || '17' || GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE_AS_CHAR(T413.C205_PART_NUMBER_ID,T413.C413_CONTROL_NUMBER) || '10' || T413.C413_CONTROL_NUMBER)) BARCODEEXP");
      sbQuery
          .append(" ,decode(GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T413.C205_PART_NUMBER_ID),'',decode(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE_AS_CHAR(T413.C205_PART_NUMBER_ID,T413.C413_CONTROL_NUMBER),'','(10)' || T413.C413_CONTROL_NUMBER,'(17)' || GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE_AS_CHAR(T413.C205_PART_NUMBER_ID,T413.C413_CONTROL_NUMBER) || '(10)' || T413.C413_CONTROL_NUMBER),decode(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE_AS_CHAR(T413.C205_PART_NUMBER_ID,T413.C413_CONTROL_NUMBER),'','(01)' || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T413.C205_PART_NUMBER_ID)  || '(10)' || T413.C413_CONTROL_NUMBER,'(01)' || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T413.C205_PART_NUMBER_ID) || '(17)' || GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE_AS_CHAR(T413.C205_PART_NUMBER_ID,T413.C413_CONTROL_NUMBER) || '(10)' || T413.C413_CONTROL_NUMBER)) BARCODEEXPVAL");
      sbQuery
          .append(" , GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T413.C205_PART_NUMBER_ID,T413.C413_CONTROL_NUMBER) EXPDATE");
      sbQuery.append(" ,C413_ITEM_QTY IQTY, C413_CONTROL_NUMBER CNUM, C413_ITEM_PRICE PRICE ");
      sbQuery
          .append(" , NVL (gm_pkg_op_item_control_rpt.get_initiated_location (t413.c205_part_number_id, '");
      sbQuery.append(strConsignId);
      sbQuery
          .append("', DECODE (t412.c412_status_fl, '2', 'pick', '3', 'put', 'pick')), gm_pkg_op_inv_scan.get_part_location(t413.c205_part_number_id, t412.c412_type)) SUGLOCATION ");

      if (strType != null && (strType.equals("4127") || strType.equals("4119"))) {
        sbQuery.append(" ,C413_STATUS_FL, get_cs_loaner_recon_qty('");
        sbQuery.append(strConsignId);
        sbQuery
            .append("',C205_PART_NUMBER_ID) RECONQTY, gm_pkg_op_fetch_recon.get_sold_qty(t413.C412_INHOUSE_TRANS_ID,C205_PART_NUMBER_ID) soldqty ");
      }
      sbQuery.append(" FROM T413_INHOUSE_TRANS_ITEMS t413, t412_inhouse_transactions t412");
      sbQuery
          .append(" WHERE t412.C412_INHOUSE_TRANS_ID = t413.C412_INHOUSE_TRANS_ID AND t413.C412_INHOUSE_TRANS_ID = '");
      sbQuery.append(strConsignId);
      sbQuery.append("' ");
      if (!strRemVoidFl.equals("Y")) {
        sbQuery.append(" and C413_VOID_FL is null ");
      }
      //Removed company and plant to load the transaction details in different companies PMT-29683
      // Added CompanyId and PlantId for viewInHouseTransItemsDetails
      //sbQuery.append(" AND ( t412.C1900_COMPANY_ID =" + strLoanerFilter);
      //sbQuery.append(" OR  t412.C5040_PLANT_ID =" + strLoanerFilter);
      //sbQuery.append(" ) ");

      if (strType != null && (strType.equals("4127") || strType.equals("4119"))) {
        sbQuery.append(" AND C413_STATUS_FL = 'Q' ");
      }
      if (strSortFlag.equals("YES")) {
        sbQuery.append(" ORDER BY PNUM, CNUM");
      } else {
        sbQuery.append(" ORDER BY C205_PART_NUMBER_ID ");
      }
      // Below code is not needed as the short name of the user is taking
      // from GmUserVO

      /*
       * if(!strUserID.equals("")){ sbSelectQuery.append(
       * "(SELECT C101_USER_SH_NAME PRINTUSER FROM T101_USER WHERE C101_USER_ID = '" );
       * sbSelectQuery.append(strUserID); sbSelectQuery.append(" ')"); hmUserName =
       * gmDBManager.querySingleRecord(sbSelectQuery.toString()); hmReturn.put("USERNAME",
       * hmUserName); }
       */

      log.debug("sbQuery ::: " + sbQuery);
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("SETLOAD", alReturn);

    } catch (Exception e) {
      GmLogError.log("Exception in GmLogisticsBean:viewInHouseTransItemsDetails", "Exception is:"
          + e);
    }
    return hmReturn;
  } // End of viewInHouseTransItemsDetails

  /**
   * saveReturnsReprocess - This method will
   * 
   * @param String strUsername
   * @return HashMap
   * @exception AppError
   */
  /*
   * public HashMap saveReturnsReprocess(String strRAId, HashMap hmParam, String strUserId) throws
   * AppError { DBConnectionWrapper dbCon = null; dbCon = new DBConnectionWrapper();
   * 
   * CallableStatement gmDBManager = null; Connection conn = null;
   * 
   * String strCNStr = (String)hmParam.get("CNSTR"); String strQNStr = (String)hmParam.get("QNSTR");
   * String strPNStr = (String)hmParam.get("PNSTR");
   * 
   * String strMsg = ""; String strReturnIds = ""; String strPrepareString = null;
   * 
   * HashMap hmReturn = new HashMap(); log.debug("RAID Value is " + strRAId+ " Param values is " +
   * hmParam);
   * 
   * try { conn = dbCon.getConnection();
   * 
   * strPrepareString = dbCon.getStrPrepareString("GM_SAVE_REPROCESS_RETURNS",7); gmDBManager =
   * conn.prepareCall(strPrepareString); /* register out parameter and set input parameters
   * 
   * gmDBManager.registerOutParameter(6,java.sql.Types.CHAR);
   * gmDBManager.registerOutParameter(7,java.sql.Types.CHAR); gmDBManager.setString(1,strRAId);
   * gmDBManager.setString(2,strCNStr); gmDBManager.setString(3,strQNStr);
   * gmDBManager.setString(4,strPNStr); gmDBManager.setString(5,strUserId);
   * 
   * gmDBManager.execute(); strReturnIds = gmDBManager.getString(6); strMsg =
   * gmDBManager.getString(7);
   * 
   * log.debug(" Return ID is " + strReturnIds);
   * 
   * hmReturn.put("RETURNIDS",strReturnIds); hmReturn.put("ORAMSG",strMsg);
   * 
   * }catch(Exception e) { GmLogError.log("Exception in
   * GmLogisticsBean:saveReturnsReprocess","Exception is:"+e); throw new AppError(e); } finally {
   * try { if (gmDBManager != null) { gmDBManager.close(); }//Enf of if (gmDBManager != null)
   * if(conn!=null) { conn.close(); /* closing the Connections } } catch(Exception e) { throw new
   * AppError(e); }//End of catch finally { gmDBManager = null; conn = null; dbCon = null; } }
   * return hmReturn; } //end of saveReturnsReprocess
   */

  /**
   * loadInHouseTransactionsReport - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public RowSetDynaClass loadInHouseTransactionsReport(HashMap hmParam) throws AppError {
    RowSetDynaClass rsResult = null;
    StringBuffer sbQuery = new StringBuffer();
    String strDateFormat = getCompDateFmt();
    String strType = GmCommonClass.parseZero((String) hmParam.get("TYPE"));
    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
    String strTransactionId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strRefId = GmCommonClass.parseNull((String) hmParam.get("REFID"));
    String strCreatedBy = GmCommonClass.parseZero((String) hmParam.get("CREATEDBY"));
    String strStatus = GmCommonClass.parseZero((String) hmParam.get("STATUS"));
    String strControlNum = GmCommonClass.parseNull((String) hmParam.get("CONTROLNUM"));
    String strEtchId = GmCommonClass.parseNull((String) hmParam.get("ETCHID"));
    String strTransDetails = GmCommonClass.parseNull((String) hmParam.get("TRANSDETAILS"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    sbQuery
        .append(" SELECT T412.C412_INHOUSE_TRANS_ID CONID, T412.C412_VERIFIED_DATE SDATE, 'I' CON_TYPE, ");
    sbQuery.append(" GET_USER_NAME(T412.C412_RELEASE_TO_ID) SNAME, T412.C412_RELEASE_TO SHIPTO, ");
    sbQuery
        .append(" T412.C412_INHOUSE_PURPOSE PID, GET_CODE_NAME(T412.C412_INHOUSE_PURPOSE) REASON, ");
    sbQuery.append(" T412.C412_TYPE TID,  GET_CODE_NAME(T412.C412_TYPE) TYPE, ");
    sbQuery
        .append(" GET_CONSIGN_SHIP_ADD(T412.C412_INHOUSE_TRANS_ID) EMP, T412.C412_COMMENTS COMMENTS");
    sbQuery
        .append(" ,get_user_name(t412.C412_CREATED_BY) CUSER ,get_user_name(t412.C412_VERIFIED_BY) VUSER ");
    sbQuery
        .append(" ,  (CASE WHEN ( t412.C412_STATUS_FL )  = 0 THEN 'Pending Release'  WHEN   ( t412.C412_STATUS_FL > 0 AND  t412.C412_STATUS_FL <= 2 AND C412_VERIFY_FL IS NULL ) THEN 'Pending Control #'");
    sbQuery.append(" WHEN ( t412.C412_STATUS_FL = 3) THEN 'Pending Verification' ");
    sbQuery.append(" WHEN t412.C412_STATUS_FL = 4 THEN 'Closed' END) SFL ");
    sbQuery.append(" ,t412.C412_REF_ID REPROCESSID ");
    if (strTransDetails.equals("Y")) {
      sbQuery
          .append(", C205_PART_NUMBER_ID PNUM, get_partdesc_by_company(C205_PART_NUMBER_ID) PDESC, TO_NUMBER(C413_ITEM_QTY) IQTY");
    }
    sbQuery.append(" FROM T412_INHOUSE_TRANSACTIONS T412 ");
    if (strTransDetails.equals("Y")) {
      sbQuery.append(", T413_INHOUSE_TRANS_ITEMS T413 ");
    }
    sbQuery.append(" WHERE T412.C412_VOID_FL IS NULL ");
    sbQuery.append(" AND T412.c5040_plant_id = " + getCompPlantId());
    if (strTransDetails.equals("Y")) {
      sbQuery.append(" AND T413.C412_INHOUSE_TRANS_ID = T412.C412_INHOUSE_TRANS_ID ");
    }
    if (strStatus.equals("4") && !strFromDt.equals("") && !strToDt.equals("")) {
      sbQuery.append(" AND T412.C412_VERIFIED_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("','" + strDateFormat + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDateFormat + "') + 1");
    } else if (strStatus.equals("0") && !strFromDt.equals("") && !strToDt.equals("")) {
      sbQuery.append(" AND ( T412.C412_VERIFIED_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("','" + strDateFormat + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDateFormat + "')+ 1");
      sbQuery.append(" OR ");
      sbQuery.append(" T412.C412_CREATED_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("','" + strDateFormat + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDateFormat + "') + 1) ");
    }
    if (!strType.equals("0")) {
      sbQuery.append(" AND T412.C412_TYPE = '");
      sbQuery.append(strType);
      sbQuery.append("'");
    }

    if (!strPartNum.equals("")) {
      if (strTransDetails.equals("Y")) {
        sbQuery.append(" AND T413.C205_PART_NUMBER_ID = '");
        sbQuery.append(strPartNum);
        sbQuery.append("'");
      } else {
        sbQuery
            .append(" AND T412.C412_INHOUSE_TRANS_ID IN (SELECT T413.C412_INHOUSE_TRANS_ID FROM T413_INHOUSE_TRANS_ITEMS T413  WHERE T413.c205_part_number_id =  '");
        sbQuery.append(strPartNum);
        sbQuery.append("')");
      }

    }

    if (!strTransactionId.equals("")) {
      sbQuery.append(" AND T412.C412_INHOUSE_TRANS_ID =  '");
      sbQuery.append(strTransactionId);
      sbQuery.append("'");
    }

    if (!strRefId.equals("")) {
      sbQuery.append(" AND T412.C412_REF_ID =  '");
      sbQuery.append(strRefId);
      sbQuery.append("'");
    }

    if (!strCreatedBy.equals("0")) {
      sbQuery.append(" AND T412.C412_CREATED_BY =  '");
      sbQuery.append(strCreatedBy);
      sbQuery.append("'");
    }

    if (!strStatus.equals("0")) {
      sbQuery.append(" AND T412.C412_STATUS_FL =  '");
      sbQuery.append(strStatus);
      sbQuery.append("'");
    }

    if (!strControlNum.equals("")) {
      sbQuery
          .append(" AND T412.C412_INHOUSE_TRANS_ID IN (SELECT T413.C412_INHOUSE_TRANS_ID FROM T413_INHOUSE_TRANS_ITEMS T413  WHERE T413.C413_CONTROL_NUMBER =  '");
      sbQuery.append(strControlNum);
      sbQuery.append("')");
    }

    if (!strEtchId.equals("")) {
      sbQuery.append("  AND T412.C412_REF_ID IN ");
      sbQuery
          .append("  ( SELECT T504A_C.C504_CONSIGNMENT_ID FROM T504A_CONSIGNMENT_LOANER T504A_C WHERE T504A_C.C504A_ETCH_ID = '");
      sbQuery.append(strEtchId);
      sbQuery.append("')");
    }

    // sbQuery.append(" ORDER BY T412.C412_INHOUSE_PURPOSE,T412.C412_INHOUSE_TRANS_ID DESC ");
    sbQuery.append(" UNION ALL "); //Changed UNION to UNION ALL.Beacuse of this PMT(PMT-50839 Quantity is wrong on Reprocess Material Report)
    sbQuery
        .append(" SELECT t504.c504_consignment_id CONID, t504.C504_VERIFIED_DATE SDATE, 'C' CON_TYPE,");
    sbQuery.append(" get_user_name (t504.c504_ship_to_id)SNAME , C504_SHIP_TO SHIPTO,");
    sbQuery
        .append(" t504.c504_inhouse_purpose PID,  get_code_name (t504.c504_inhouse_purpose) REASON    ,t504.c504_type TID,");
    sbQuery
        .append(" get_code_name (t504.c504_type) TYPE, GET_CONSIGN_SHIP_ADD(t504.c504_consignment_id) EMP,t504.C504_COMMENTS COMMENTS,");
    sbQuery
        .append(" get_user_name (t504.c504_created_by) cuser, get_user_name (t504.C504_VERIFIED_BY) VUSER, ");
    sbQuery.append(" (CASE WHEN  t504.C504_STATUS_FL = 0 THEN 'Initiated' ");
    sbQuery.append(" WHEN t504.C504_STATUS_FL = 1 THEN 'Pending Control #' ");
    sbQuery
        .append(" WHEN t504.C504_STATUS_FL = 2 AND t504.c207_set_id is NULL THEN 'Pending Control #' ");
    sbQuery
        .append(" WHEN (t504.C504_STATUS_FL = 2 AND t504.c207_set_id is NOT NULL AND c504_verify_fl IS NULL )  THEN 'Pending Verification' ");
    sbQuery.append(" WHEN ( t504.C504_STATUS_FL = 2 AND c504_verify_fl = 1) THEN 'Verified' ");
    sbQuery.append(" WHEN t504.C504_STATUS_FL = 3 THEN 'Pending Shipping' ");
    sbQuery
        .append(" WHEN t504.C504_STATUS_FL = 4 THEN 'Closed' END) SFL ,t504.C504_REPROCESS_ID REPROCESSID ");
    if (strTransDetails.equals("Y")) {
      sbQuery
          .append(" ,t505.C205_PART_NUMBER_ID PNUM, get_partdesc_by_company(t505.C205_PART_NUMBER_ID) PDESC, TO_NUMBER(t505.C505_ITEM_QTY) IQTY");
    }
    sbQuery.append(" FROM t504_consignment t504 ");
    if (strTransDetails.equals("Y")) {
      sbQuery.append(", t505_item_consignment t505 ");
    }
    sbQuery.append(" WHERE t504.c504_void_fl IS NULL ");
    sbQuery.append(" AND t504.c5040_plant_id = " + getCompPlantId());
    if (strTransDetails.equals("Y")) {
      sbQuery.append(" AND t504.c504_consignment_id = t505.c504_consignment_id ");
    }
    if (strStatus.equals("4") && !strFromDt.equals("") && !strToDt.equals("")) {
      sbQuery.append(" AND ( t504.C504_VERIFIED_DATE BETWEEN");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("','" + strDateFormat + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDateFormat + "') + 1)"); // Velu added
      // bracket.
    } else if (strStatus.equals("0") && !strFromDt.equals("") && !strToDt.equals("")) {
      sbQuery.append(" AND ( t504.C504_VERIFIED_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("','" + strDateFormat + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDateFormat + "') + 1");
      sbQuery.append(" OR ");
      sbQuery.append(" t504.C504_CREATED_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("','" + strDateFormat + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDateFormat + "') + 1) ");
    }

    if (!strType.equals("0")) {
      sbQuery.append(" AND t504.c504_type = '");
      sbQuery.append(strType);
      sbQuery.append("'");
    }
    if (!strPartNum.equals("")) {
      if (strTransDetails.equals("Y")) {
        sbQuery.append(" AND t505.C205_PART_NUMBER_ID = '");
        sbQuery.append(strPartNum);
        sbQuery.append("'");
      } else {
        sbQuery
            .append(" AND t504.c504_consignment_id IN (SELECT t505.c504_consignment_id FROM t505_item_consignment t505  WHERE t505.c205_part_number_id =  '");
        sbQuery.append(strPartNum);
        sbQuery.append("')");
      }
    }
    if (!strTransactionId.equals("")) {
      sbQuery.append(" AND t504.c504_consignment_id =  '");
      sbQuery.append(strTransactionId);
      sbQuery.append("'");
    }

    if (!strRefId.equals("")) {
      sbQuery.append(" AND t504.C504_REPROCESS_ID =  '");
      sbQuery.append(strRefId);
      sbQuery.append("'");
    }

    if (!strCreatedBy.equals("0")) {
      sbQuery.append(" AND t504.C504_CREATED_BY =  '");
      sbQuery.append(strCreatedBy);
      sbQuery.append("'");
    }

    if (!strStatus.equals("0")) {
      sbQuery.append(" AND t504.C504_STATUS_FL =  '");
      sbQuery.append(strStatus);
      sbQuery.append("'");
    }
    if (!strControlNum.equals("")) {
      sbQuery
          .append(" AND t504.C504_CONSIGNMENT_ID IN (SELECT t504.c504_consignment_id FROM t505_item_consignment t505  WHERE t505.C505_CONTROL_NUMBER =  '");
      sbQuery.append(strControlNum);
      sbQuery.append("')");
    }

    if (!strEtchId.equals("")) {
      sbQuery.append("  AND t504.C504_REPROCESS_ID IN ");
      sbQuery
          .append("  ( SELECT T504A_C.C504_CONSIGNMENT_ID FROM T504A_CONSIGNMENT_LOANER T504A_C WHERE T504A_C.C504A_ETCH_ID = '");
      sbQuery.append(strEtchId);
      sbQuery.append("')");
    }
    sbQuery.append("   ORDER BY PID ,CONID DESC  ");

    log.debug(" Query for Reprocess Material Report " + sbQuery.toString());

    rsResult = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());// queryMultipleRecords(sbQuery.toString());

    return rsResult;
  } // End of loadInHouseTransactionsReport

  /**
   * saveDemandParDetail - This method will be save / update the par detail
   * 
   * @param inputString , strUserId
   * @exception AppError
   */
  public void saveSetBuildDashboard(String inputString, String strUserId) throws AppError {
    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      gmDBManager.setPrepareString("gm_pkg_op_set_build.gm_sav_dashboard", 2);
      gmDBManager.setString(1, strUserId);
      gmDBManager.setString(2, inputString);
      gmDBManager.execute();
      gmDBManager.commit();
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
  }

  public void reconfigConsignToRequest(String strConsignId, String strRequestId,
      String strInputString, String strAddPartsInputString, boolean blnIsBackOrderPresent,
      String strUserId) throws AppError {
    String strVoidfl = "";
    String strTxnId = "";
    String strMessage = "";
    String strRepTxns = "";
    String strRepTxn = "";
    String strToken = "";
    StringBuffer sbMessage = new StringBuffer();
    HashMap hm = new HashMap();
    GmDBManager gmDBManager = new GmDBManager();

    gmDBManager.setPrepareString("gm_pkg_op_process_request.GM_SAV_RECONFIG_PART_TO", 8);

    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strInputString);

    if (blnIsBackOrderPresent) {
      gmDBManager.setString(3, "1");
    } else {
      gmDBManager.setString(3, "");
    }

    if (strAddPartsInputString.equals("")) {
      gmDBManager.setString(4, ""); // for insert records
    } else {
      gmDBManager.setString(4, "1");
    }

    gmDBManager.setString(5, strUserId);
    gmDBManager.setString(6, strAddPartsInputString);
    gmDBManager.registerOutParameter(7, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(8, java.sql.Types.CHAR);
    gmDBManager.execute();
    strVoidfl = GmCommonClass.parseNull(gmDBManager.getString(7));
    strTxnId = GmCommonClass.parseNull(gmDBManager.getString(8));

    gmDBManager.commit();
    log.debug("successful" + strVoidfl + " strTxnId " + strTxnId);

    if (!strTxnId.equals("")) {
      strRepTxns =
          GmCommonClass.parseNull((strTxnId.indexOf("|") >= 0) ? strTxnId.substring(
              strTxnId.indexOf("|") + 1, strTxnId.length()) : "");
    }
    if (!strTxnId.equals("")) {
      strTxnId =
          GmCommonClass.parseNull((strTxnId.indexOf("|") >= 0) ? strTxnId.substring(0,
              strTxnId.indexOf("|")) : "");
    }

    log.debug("successful" + strVoidfl + " strRepTxns " + strRepTxns + "  strTxnId " + strTxnId);

    if (!strTxnId.equals("") && strTxnId.charAt(0) == ',') {
      log.debug(" strTxnId " + strTxnId);
      strTxnId = strTxnId.substring(1);
    }

    sbMessage.append(" Transaction ");
    sbMessage.append(strTxnId);
    sbMessage.append(" created successfully ");
    // <a href="#"
    // onclick="javascript:fnPicSlip('  || '''' || v_transid  || '''' || ');">'
    // || v_transid || '</a>
    if (!strRepTxns.equals("")) {
      StringTokenizer stToken = new StringTokenizer(strRepTxns, ",");
      while (stToken.hasMoreTokens()) {
        strToken = stToken.nextToken();
        strRepTxn +=
            "<a href='#' onclick='javascript:fnPicSlip(\"" + strToken + "\",\"\",\"" + strConsignId
                + "\",\"I\");'>" + strToken + "</a> and ";
      }

      strRepTxn = strRepTxn.substring(0, strRepTxn.length() - 4);

      sbMessage
          .append(" <br><br><span style='font-weight: normal;color: black;font-size: 12px;'><center>The "
              + strRepTxn
              + " transaction(s) has been created and it is available on inhouse dashboard for further processing.</center></span>");
    }

    log.debug(" Message is " + sbMessage.toString());

    if ("Y".equals(strVoidfl)) {
      // throw new AppError(new SQLException("Error Occured", null,
      // 20078));
      strMessage =
          "Consignment   ".concat(strConsignId).concat(" has been voided and request ")
              .concat(strRequestId).concat(" marked as Back Order ");

      if (!strRepTxns.equals("")) {
        strMessage +=
            "<br><br><span style='font-weight: normal;color: black;font-size: 12px;'><center>The "
                + strRepTxn
                + " transaction(s) has been created and it is available on inhouse dashboard for further processing.</center></span>";
      }
      throw new AppError(strMessage, "", 'E');

    }

    if (!strTxnId.equals("") || !strRepTxns.equals("")) {
      throw new AppError(sbMessage.toString(), "", 'S');
    }

  }

  public HashMap getLastupdatedDate(final String strConsID) throws AppError {
    StringBuffer sb = new StringBuffer();
    HashMap hmValue = new HashMap();
    try {

      sb.append(" SELECT  C504_LAST_UPDATED_DATE  lastUpdateDate FROM t504_consignment WHERE c504_consignment_id = '"
          + strConsID + "'");
      log.debug("HERE: " + sb.toString());

      GmDBManager gmDBManager = new GmDBManager();

      // hmValue = gmDBManager.querySingleDate(sb.toString());
      return hmValue;
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }

  }

  public HashMap getConsignPartsToAdd(final String strConsignId) throws AppError {

    ArrayList alParts = null;
    HashMap hmParts = new HashMap();

    try {

      GmDBManager gmDBManager = new GmDBManager();

      gmDBManager.setPrepareString("gm_pkg_op_process_request.GM_FCH_PARTS_TO_ADD", 2);
      gmDBManager.setString(1, strConsignId);
      gmDBManager.registerOutParameter(2, OracleTypes.CURSOR); // header
      // query
      // 1

      gmDBManager.execute();

      alParts = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
      gmDBManager.commit(); // not necessary

      hmParts.put("ADDPARTS", alParts);

      return hmParts;
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }

  }

  /**
   * getBackOrderReport - This method will generate the details of the parts that are in back order
   * 
   * @param String strRequestId -
   * @return List of DynaBeans
   * @exception AppError
   */
  public List fetchBackOrderReport(String strRequestId) throws AppError {
    RowSetDynaClass rdBODetail;
    GmDBManager gmDBManager = new GmDBManager();

    gmDBManager.setPrepareString("gm_pkg_op_request_summary.gm_fch_backorder_report", 2);
    gmDBManager.setString(1, strRequestId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    rdBODetail = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return rdBODetail.getRows();

  }

  /**
   * getChildRequest - This method will generate the details of the Child request
   * 
   * @param String strRequestId -
   * @return List of DynaBeans
   * @exception AppError
   */
  public List fetchChildRequest(String strRequestId) throws AppError {
    RowSetDynaClass rdCRDetail;
    GmDBManager gmDBManager = new GmDBManager();

    gmDBManager.setPrepareString("gm_pkg_op_request_summary.gm_fch_child_request", 2);
    gmDBManager.setString(1, strRequestId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    rdCRDetail = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return rdCRDetail.getRows();

  }

  /**
   * initiateSetBuild - This method will
   * 
   * @param HashMap hmParam
   * @return HashMap
   * @exception AppError
   */
  public HashMap initiateSetBuild(HashMap hmParam) throws AppError, Exception {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmShippingTransBean gmShippingTransBean = new GmShippingTransBean(getGmDataStoreVO());
    GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean(getGmDataStoreVO());
    String strMaterialRequestId = "";
    String strConsignId = "";
    String strShipToId = "";
    String strRequestSource = "";
    String strRequestByType = "50625";

    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strConsignmentType = GmCommonClass.parseNull((String) hmParam.get("CONSIGNMENTTYPE"));
    String strInHousePurpose = GmCommonClass.parseNull((String) hmParam.get("INHOUSEPURPOSE"));
    String strDistributorId = GmCommonClass.parseNull((String) hmParam.get("DISTRIBUTORID"));
    String strRepId = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strShipTo = GmCommonClass.parseNull((String) hmParam.get("SHIPTO"));
    String strAccountId = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
    String strEmployeeId = GmCommonClass.parseNull((String) hmParam.get("EMPLOYEEID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strDepartmentId = GmCommonClass.parseNull((String) hmParam.get("DEPTID"));
    String strRequiredDate = GmCommonClass.parseNull((String) hmParam.get("REQUIREDDATE"));
    String strPlannedShipDate = GmCommonClass.parseNull((String) hmParam.get("PLANSHIPDATE"));
    String strDateFmt = getCompDateFmt();// GmCommonClass.parseNull((String)
    Date dtPlanShipDt = GmCommonClass.getStringToDate(strPlannedShipDate, getCompDateFmt());
    strShipTo = strShipTo.equals("0") ? "" : strShipTo;
    strDistributorId =
        strDistributorId.equals("01") ? "" : strDistributorId.equals("0") ? "" : strDistributorId;
    log.debug(" strShipTo  is  " + strShipTo);

    /*
     * If Dept is Logistics (2014) then request source is build set
     */
    if (strDepartmentId.equals("2014")) {
      strRequestSource = "50617";

    } else {
      strRequestSource = "50618";
    }
    log.debug(" Distributor Id is  " + strDistributorId);

    /*
     * 4121 - Sales Rep 4123 - Employee
     */

    if (strShipTo.equals("4121")) {
      strRequestByType = "50626";
    } else if (strShipTo.equals("4123")) {
      strShipToId = "50625";
    }

    if (strConsignmentType.equals("0")) {
      strConsignmentType = "40021";
    }

    HashMap hmReturn = new HashMap();
    gmDBManager.setPrepareString("gm_pkg_op_process_request.gm_sav_initiate_set", 18);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(17, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(18, java.sql.Types.CHAR);

    gmDBManager.setString(1, "");
    gmDBManager.setString(2, strRequiredDate);
    // gmDBManager.setString(2,"");
    gmDBManager.setString(3, strRequestSource);
    gmDBManager.setString(4, "");
    gmDBManager.setString(5, strSetId);
    gmDBManager.setInt(6, Integer.parseInt(strConsignmentType));
    gmDBManager.setString(7, strDistributorId);
    gmDBManager.setInt(8, Integer.parseInt(strRequestByType));
    gmDBManager.setString(9, strUserId);
    gmDBManager.setString(10, strShipTo);
    gmDBManager.setString(11, strShipToId);
    gmDBManager.setString(12, "");
    gmDBManager.setInt(13, 15); // Status 15 is backlog
    gmDBManager.setString(14, strUserId);
    gmDBManager.setString(15, strInHousePurpose);
    gmDBManager.setDate(16, dtPlanShipDt);

    gmDBManager.execute();
    strMaterialRequestId = gmDBManager.getString(17);
    strConsignId = gmDBManager.getString(18);

    if (!strShipTo.equals("") && !strShipTo.equals("0")) {
      log.debug("saving shipping record");
      hmParam.put("REFID", strMaterialRequestId);
      hmParam.put("SOURCE", "50184"); // request
      gmShippingTransBean.saveShipDetails(gmDBManager, hmParam);
    }
    gmTxnSplitBean.checkAndSplitConsignamnet(gmDBManager, strMaterialRequestId, strUserId);
    gmDBManager.commit();

    log.debug("Consignment Id is " + strConsignId);
    log.debug("Material Request Id is " + strMaterialRequestId);

    hmReturn.put("MATERIALREQUESTID", strMaterialRequestId);
    hmReturn.put("CONSIGNID", strConsignId);
    /*
     * hmResult = gmOperationsBean.viewBuiltSetDetails(strConsignId);
     * hmReturn.put("CONDETAILS",hmResult);
     */

    return hmReturn;

  }

  /**
   * saveSetMaster - This method will save build set details
   * 
   * @param HashMap
   * @return HashMap
   * @exception AppError
   */
  public void saveSetMaster(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_SAVE_SET_BUILD", 7);
    String strMsg;
    String strFlag = "";
    String strPlantId = GmCommonClass.parseNull(getGmDataStoreVO().getPlantid());
    String strCountryCode = GmCommonClass.parseNull(GmCommonClass.countryCode);
    String strCompleteFlag = GmCommonClass.getCheckBoxValue((String) hmParam.get("COMPLETEFLAG"));
    String strVerifyFlagVal = GmCommonClass.getCheckBoxValue((String) hmParam.get("VERIFYFLAG"));

    String strConsignId = GmCommonClass.parseNull((String) hmParam.get("CONSIGNID"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    
    String strSetType = GmCommonClass.parseNull((String) hmParam.get("CBO_TYPE"));
    String strSetID = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strSetName = GmCommonClass.parseNull((String) hmParam.get("SETNAME"));
    
    if (strCountryCode.equals("en")) {
      strFlag = strCompleteFlag == "Y" ? "1.10" : "1"; // 1.10 - Complete,
      // 1 - WIP
      strFlag = strVerifyFlagVal == "Y" ? "1.20" : strFlag; // 1.20 -
      // Pending
      // FG
    } else {
      strFlag = strCompleteFlag == "Y" ? "2" : "1"; // 2 -- Build Set, 1
      // -- WIP
      strFlag = strVerifyFlagVal == "Y" ? "2" : strFlag;
    }

    String strVerifyFlag = strVerifyFlagVal == "Y" ? "1" : "0";
     
    //Method call to clear cache if new set is Added for PMT-37080
    if(strVerifyFlag.equals("1") && strSetType.equals("4127")){ //4127 - Product Loaner
    	GmAutoCompleteTransBean gmAutoCompleteTransBean =
    	        new GmAutoCompleteTransBean(getGmDataStoreVO());
    	    HashMap hmJmsParam = new HashMap();
    	    hmJmsParam.put("ID", strSetID);
    	    hmJmsParam.put("OLD_NM", "");
    	    hmJmsParam.put("NEW_NM", strSetName);
    	    hmJmsParam.put("METHOD", "LoanerSetList");
    	    hmJmsParam.put("companyInfo", "");
    	    gmAutoCompleteTransBean.saveDetailsToCache(hmJmsParam);
    }
    String strUsername = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    log.debug(" Flag " + strFlag + " strVerifyFlag " + strVerifyFlag + " strCompleteFlag "
        + strCompleteFlag);
    log.debug(" strInputString " + strInputString);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(7, java.sql.Types.CHAR);

    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strInputString);
    gmDBManager.setString(3, strComments);
    gmDBManager.setString(4, strFlag);
    gmDBManager.setString(5, strVerifyFlag);
    gmDBManager.setString(6, strUsername);

    gmDBManager.execute();
    strMsg = gmDBManager.getString(7);
    gmDBManager.commit();
    
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
            new GmAutoCompleteReportBean(getGmDataStoreVO());
    String strKey = "PRODUCT_LOANER_SET_" + strPlantId + ":AUTO";   //PRODUCT_LOANER_SET_3000:AUTO
        GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
        gmCacheManager.removeKey(strKey);
        log.debug(" Set key removed "+strKey);
    
  }

  /**
   * fetchLoanerIDLogDetails - This method will fetch the details
   * 
   * @param String
   * @return RowSetDynaClass
   * @exception AppError
   */

  public RowSetDynaClass fetchLNLogDetails(String strLoanerCnId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    RowSetDynaClass rsDynaResult = null;
    gmDBManager.setPrepareString("gm_pkg_op_ln_cn_swap.gm_fch_lnswap_log_details", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR); // header
    gmDBManager.setString(1, strLoanerCnId);
    gmDBManager.execute();
    rsDynaResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return rsDynaResult;
  }

  /**
   * getXmlGridData - This method will fetchLNLogDetails
   * 
   * @param ArrayList
   * @return String
   * @exception AppError
   */
  public String getXmlGridData(ArrayList alParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alParam);
    templateUtil.setTemplateName("GmLnCnSWAPLog.vm");
    templateUtil.setTemplateSubDir("operations/logistics/templates");
    return templateUtil.generateOutput();
  }

  /**
   * fetchPicSlipHeaderInfo - This method will WareHouse Pickslip Header Details
   * 
   * @param String strConsignId , String strTxnType
   * @return HashMap
   * @exception AppError
   */
  public HashMap fetchPicSlipHeaderInfo(String strConsignId, String strTxnType) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    HashMap hmReturn = new HashMap();

    String whFromAddr = "";
    String whToAddr = "";
    String whConsignName = "";
    String whPackslipHead = "";

    gmDBManager.setPrepareString("gm_pkg_op_inv_warehouse.gm_fch_picslip_info", 6);
    gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(4, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(6, java.sql.Types.CHAR);

    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strTxnType);
    gmDBManager.execute();
    whFromAddr = gmDBManager.getString(3);
    whToAddr = gmDBManager.getString(4);
    whConsignName = gmDBManager.getString(5);
    whPackslipHead = gmDBManager.getString(6);
    gmDBManager.close();
    hmReturn.put("WHFROMADD", whFromAddr);
    if (whToAddr != null) {
      hmReturn.put("SHIPADD", whToAddr);
    }
    if (whConsignName != null) {
      hmReturn.put("TRBILLNM", whConsignName);
      hmReturn.put("SHOWSTS", "NO");
    }
    hmReturn.put("PACKSLIPHEAD", whPackslipHead);
    return hmReturn;

  }

  /**
   * viewLoanerTransItemsDetails - This method will load the part details section while clicking "Y"
   * icon in Loaner Reconcilliation Screen
   * 
   * @param String strConsignId, String strType
   * @return HashMap
   * @exception AppError
   */
  public HashMap viewLoanerTransItemsDetails(String strConsignId, String strType) throws AppError {
    ArrayList alReturn = new ArrayList();
    HashMap hmReturn = new HashMap();
    HashMap hmUserName = new HashMap();
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT  C205_PART_NUMBER_ID PNUM, get_partdesc_by_company(C205_PART_NUMBER_ID) PDESC, ");
      sbQuery.append(" gm_pkg_op_fetch_recon.get_item_qty(t413.C412_INHOUSE_TRANS_ID,C205_PART_NUMBER_ID) IQTY , MAX(C413_CONTROL_NUMBER) CNUM, C413_ITEM_PRICE PRICE ");
      sbQuery
          .append(" , NVL (gm_pkg_op_item_control_rpt.get_initiated_location (t413.c205_part_number_id, '");
      sbQuery.append(strConsignId);
      sbQuery
          .append("', DECODE (t412.c412_status_fl, '2', 'pick', '3', 'put', 'pick')), gm_pkg_op_inv_scan.get_part_location(t413.c205_part_number_id, t412.c412_type)) SUGLOCATION ");

      if (strType != null && (strType.equals("4127") || strType.equals("4119"))) {
        sbQuery.append(" ,C413_STATUS_FL, get_cs_loaner_recon_qty('");
        sbQuery.append(strConsignId);
        sbQuery
            .append("',C205_PART_NUMBER_ID) RECONQTY, gm_pkg_op_fetch_recon.get_sold_qty(t413.C412_INHOUSE_TRANS_ID,C205_PART_NUMBER_ID) soldqty ");
      }
      sbQuery.append(" FROM T413_INHOUSE_TRANS_ITEMS t413, t412_inhouse_transactions t412");
      sbQuery
          .append(" WHERE t412.C412_INHOUSE_TRANS_ID = t413.C412_INHOUSE_TRANS_ID AND t413.C412_INHOUSE_TRANS_ID = '");
      sbQuery.append(strConsignId);
      sbQuery.append("' ");
      sbQuery.append(" AND  t412.C5040_PLANT_ID =" + getCompPlantId());

      if (strType != null && (strType.equals("4127") || strType.equals("4119"))) {
        sbQuery.append(" AND C413_STATUS_FL = 'Q' ");
      }
      sbQuery.append(" GROUP BY C205_PART_NUMBER_ID,t413.C412_INHOUSE_TRANS_ID,t412.c412_status_fl,t412.c412_type,C413_STATUS_FL,C413_ITEM_PRICE ");
      sbQuery.append(" ORDER BY C205_PART_NUMBER_ID ");

      log.debug("viewLoanerTransItemsDetails ::: " + sbQuery);
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("SETLOAD", alReturn);

    } catch (Exception e) {
      GmLogError.log("Exception in GmLogisticsBean:viewLoanerTransItemsDetails", "Exception is:"
          + e);
    }
    return hmReturn;
  } // End of viewLoanerTransItemsDetails

}// end of class GmLogisticsBean
