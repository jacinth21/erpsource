/*
 * Module: GmOperationsBean.java Author: Dhinakaran James Project: Globus Medical App Date-Written:
 * 11 Mar 2004 Security: Unclassified Description: Testing Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.operations.beans;

import java.sql.ResultSet; 
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;


// import com.globus.logon.beans.GmLogonBean;


// import oracle.jdbc.driver.OracleCallableStatement;

public class GmNCMRBean extends GmBean {
  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmNCMRBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  Logger log = GmLogger.getInstance(this.getClass().getName());
  /**
   * reportNCMRDetails - This method will return the NCMR based on the IN Parameter
   * 
   * @param String strNCMRId
   * @param String strFromDt
   * @param String strToDt
   * @param String strVendorName
   * @param String strPartNum
   * @param String strPartDesc
   * @return ArrayList
   * @exception AppError
   **/

  public ArrayList reportNCMRDetails(HashMap hmParam) {
    ArrayList alReturn = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strPartID = GmCommonClass.parseNull((String) hmParam.get("PARTID"));
    String strPartName = GmCommonClass.parseNull((String) hmParam.get("PARTNM"));
    String strVendorName = GmCommonClass.parseNull((String) hmParam.get("VENDORNM"));
    String strSort = GmCommonClass.parseNull((String) hmParam.get("SORT"));
    String strSortType = GmCommonClass.parseNull((String) hmParam.get("SORTTYPE"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strNCMRId = GmCommonClass.parseNull((String) hmParam.get("NCMRNO"));
    String strNonConf = GmCommonClass.parseNull((String) hmParam.get("NONCONF"));
    String strDHRId = GmCommonClass.parseNull((String) hmParam.get("DHRID"));
    String strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
    String strEVALID = GmCommonClass.parseNull((String) hmParam.get("EVALID"));
    String strTypeNew = GmCommonClass.parseNull((String) hmParam.get("TYPENEW"));
    String[] strArrayId = new String[10];
    Logger log = GmLogger.getInstance(this.getClass().getName());
    try {
      // DBConnectionWrapper dbCon = null;
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      // dbCon = new DBConnectionWrapper();

      sbQuery.append(" SELECT t409.C409_CREATED_DATE CDATE, ");
      sbQuery.append(" t409.C409_NCMR_ID NCMRID,	GET_USER_SH_NAME(t409.C409_CLOSEOUT_BY) CLSBY, ");
      sbQuery.append(" t409.C205_PART_NUMBER_ID PNUM, ");
      sbQuery.append(" t409.C408_DHR_ID DHRID, ");
      sbQuery.append(" GET_PARTNUM_DESC(t409.C205_PART_NUMBER_ID) PDESC, ");
      sbQuery.append(" t408.C408_CONTROL_NUMBER CNUM, ");
      sbQuery.append(" t409.C402_WORK_ORDER_ID WOID, ");
      sbQuery.append(" t408.C408_QTY_REJECTED QTYREJ, ");
      sbQuery.append(" t409.C409_CLOSEOUT_QTY CLOSEQTY, ");
      sbQuery.append(" GET_VENDOR_SH_NAME(t409.C301_VENDOR_ID) VNAME, ");
      sbQuery.append(" GET_CODE_NAME(t409.C409_DISPOSITION) DISP, ");
      sbQuery.append(" GET_CODE_NAME(t409.C409_RESPONSIBILITY) RESPON, ");
      sbQuery.append(" t409.C409_DISP_DATE DDATE,  ");
      sbQuery.append(" t409.C409_CLOSEOUT_DATE CLOSEDATE, ");
      sbQuery.append(" GET_CODE_NAME(t409.C901_REASON_TYPE) REASON_TYPE, ");
      sbQuery.append("  t409.c409_status_fl STATUS, T409.C901_Type TYPE ");
      sbQuery.append(" FROM T409_NCMR t409, T408_DHR t408  ");
      sbQuery.append(" WHERE t409.C408_DHR_ID = t408.C408_DHR_ID ");
      // sbQuery.append(" AND  T409.C1900_COMPANY_ID = " + getCompId());
      sbQuery.append(" AND T409.c5040_plant_id = " + getCompPlantId());

      sbQuery.append(" AND t408.C408_VOID_FL IS NULL ");
      sbQuery.append(" AND t409.C409_VOID_FL IS NULL ");
      if (!strNCMRId.equals("")) {
        sbQuery.append(" AND t409.C409_NCMR_ID = '");
        sbQuery.append(strNCMRId);
        sbQuery.append("'AND (t409.c901_type='51062'or t409.c901_type is null)");
      }

      if (!strEVALID.equals("")) {
        sbQuery.append(" AND t409.C409_NCMR_ID = '");
        sbQuery.append(strEVALID);
        sbQuery.append("' AND t409.c901_type='51061'");
      }

      if (!strVendorId.equals("")) {
        sbQuery.append(" AND t409.C301_VENDOR_ID = ");
        sbQuery.append(strVendorId);
      }

      if (!strNonConf.equals("")) {
        sbQuery.append(" AND UPPER(t409.C409_REJ_REASON) ");
        sbQuery.append(" LIKE UPPER('%");
        sbQuery.append(strNonConf);
        sbQuery.append("%' )");
      }

      if (!strDHRId.equals("")) {
        sbQuery.append(" AND t409.C408_DHR_ID = '");
        sbQuery.append(strDHRId);
        sbQuery.append("'");
      }

      if (!strFromDt.equals("")) {
        sbQuery.append(" AND TRUNC(t409.C409_CREATED_DATE) >= TO_DATE('");
        sbQuery.append(strFromDt);
        sbQuery.append("', '" + getCompDateFmt() + "') ");
      }

      if (!strToDt.equals("")) {
        sbQuery.append(" AND TRUNC(t409.C409_CREATED_DATE) <= TO_DATE('");
        sbQuery.append(strToDt);
        sbQuery.append("', '" + getCompDateFmt() + "') ");
      }

      if (!strPartID.trim().equals("")) {
        if (!strPartID.equals("")) {
          strArrayId = strPartID.trim().split(",");
        }
        sbQuery.append(" AND (");
        int length = strArrayId.length;
        for (int i = 0; i < length; i++) {

          if (i == (length - 1)) {
            sbQuery.append(" t409.C205_PART_NUMBER_ID LIKE ");
            sbQuery.append("'%" + strArrayId[i].trim() + "%'");
          } else {
            sbQuery.append(" t409.C205_PART_NUMBER_ID LIKE ");
            sbQuery.append("'%" + strArrayId[i].trim() + "%' OR");
          }
        }
        sbQuery.append(" )");
      }

      if (!strPartName.equals("")) {
        sbQuery.append(" AND t409.C205_PART_NUMBER_ID IN (");
        sbQuery.append(" SELECT C205_PART_NUMBER_ID FROM T205_PART_NUMBER ");
        sbQuery.append(" WHERE UPPER(TRIM(C205_PART_NUM_DESC)) LIKE UPPER('%");
        sbQuery.append(strPartName);
        sbQuery.append("%') )");
      }

      if (!strVendorName.equals("")) {
        sbQuery.append(" AND t409.C301_VENDOR_ID IN ( ");
        sbQuery.append(" SELECT C301_VENDOR_ID FROM T301_VENDOR ");
        sbQuery.append(" WHERE UPPER(TRIM(C301_VENDOR_SH_NAME)) LIKE UPPER('%");
        sbQuery.append(strVendorName);
        sbQuery.append("%') OR UPPER(TRIM(C301_VENDOR_NAME)) LIKE UPPER('%");
        sbQuery.append(strVendorName);
        sbQuery.append("%') ) ");

      }

      if (strTypeNew.equals("1") && strDHRId.trim().equals("")) {
        if (strType.equals("0")) {
          sbQuery.append(" AND t409.c901_type='51061' AND t409.c409_status_fl='0'");
        } else if (strType.equals("1")) {
          sbQuery.append(" AND t409.c901_type='51061' AND t409.c409_status_fl<>'0'");
        } else {
          sbQuery.append(" AND t409.c901_type='51061' ");
        }
      } else if (strTypeNew.equals("0") && strDHRId.trim().equals(""))

      {
        sbQuery.append(" AND (t409.c901_type='51062' or t409.c901_type is null)");
      }
      // To filter based on the type closed, open and all
      if (strType.equals("0") && strDHRId.trim().equals("")) {
        sbQuery.append(" AND t409.C409_CLOSEOUT_DATE IS NULL ");
      } else if (strType.equals("1") && !strTypeNew.equals("1") && strDHRId.trim().equals("")) {
        sbQuery.append(" AND t409.C409_CLOSEOUT_DATE IS NOT NULL ");
      }
      sbQuery.append(" Order by t409.C409_CREATED_DATE desc");


      /********************************************
       * Code to handle sort functionality
       *******************************************/
      if (!strSort.equals("")) {
        /*
         * if (strSort.equals("CDATE") || strSort.equals("DDATE") || strSort.equals("CLOSEDATE")) {
         * sbQuery.append(" ORDER BY TO_DATE(" + strSort +
         * ", GET_RULE_VALUE('DATEFMT','DATEFORMAT'))"); } else {
         */
        sbQuery.append(" ORDER BY " + strSort);
        // }

        // Sort order information
        if (strSortType.equals("1")) {
          sbQuery.append(" asc ");
        } else {
          sbQuery.append(" desc ");
        }
      }
      log.debug("Query is : " + sbQuery.toString());
      gmDBManager.setPrepareString(sbQuery.toString());
      // alReturn = dbCon.queryMultipleRecords(sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecord();
      log.debug("All Return is : " + alReturn);
    } catch (Exception e) {
      GmLogError.log("Exception in GmOperationsBean:getNCMRDetails", "Exception is:" + e);
    }
    return alReturn;
  } // End of getNCMRDetails
  /**
   * getXmlGridData - This method used to show the NCMR report
   * 
   * @param HashMap 
   * @return String
   * @exception AppError
   **/
  public String getXmlGridData(HashMap hmParam) throws AppError {

	    GmTemplateUtil templateUtil = new GmTemplateUtil();
	    String strSessCompanyLocale =
	        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
	    templateUtil.setDataList("alReturn", (ArrayList) hmParam.get("REPORT"));
	    templateUtil.setDataMap("hmParam", hmParam);
	    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
	        "properties.labels.operations.GmNCMRReport", strSessCompanyLocale));
	    templateUtil.setTemplateName("GmNCMRReport.vm");
	    templateUtil.setTemplateSubDir("operations/templates");
	  
	    return templateUtil.generateOutput();
	  } 

}
