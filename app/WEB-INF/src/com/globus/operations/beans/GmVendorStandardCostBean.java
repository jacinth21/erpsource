package com.globus.operations.beans;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**GmVendorStandardCostBean:This Class which is used to save standard cost bulk
 * 						  Upload Details
 * @author pvigneshwaran
 *
 */
public class GmVendorStandardCostBean extends GmBean {

	/**
	 * 
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
	 * @param gmDataStoreVO
	 * @throws AppError
	 */
	public GmVendorStandardCostBean(GmDataStoreVO gmDataStoreVO)
			throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
	
	/**saveStandardCostBulkUpload:This Method used to save Standard Cost upload Details in t302_table
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String saveStandardCostBulkUpload(HashMap hmParam) throws AppError {


		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


		String strJsonString = "";

		String strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
		String strPartNumbers = GmCommonClass.parseNull((String) hmParam.get("STRPARTNUMBERS"));
		String strInputstr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		
		gmDBManager.setPrepareString("gm_pkg_pur_vendor_cost.gm_sav_standard_cost_bulk_upload",5);

		gmDBManager.registerOutParameter(5, OracleTypes.CLOB);

		gmDBManager.setString(1, strVendorId);
		gmDBManager.setString(2, strPartNumbers);
		gmDBManager.setString(3, strInputstr);
		gmDBManager.setString(4, strUserId);
		gmDBManager.execute();
		strJsonString = GmCommonClass.parseNull(gmDBManager.getString(5));
		gmDBManager.commit();

		return strJsonString;
	}

}
