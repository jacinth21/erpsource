package com.globus.operations.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

import oracle.jdbc.driver.OracleTypes;

public class GmChangeLotBean extends GmBean{
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j
	
	  public GmChangeLotBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	  public GmChangeLotBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	/**
	   * Used to Save Lot Error Detail using New Lot
	   * @param hmParams
	   * @return
	   * @throws AppError
	   */
	  public String saveChangeLotNumber(HashMap hmReturn) throws AppError {
		  GmDBManager gmDBManager = new GmDBManager();
		    String strChangeLotString = "";
		    String strTransactionId = "";
		    String strPartNumber = "";
		    String strQuantity = "";
		    String strOldCntrlNumber = "";
		    String strNewCntrlNumber = "";
		    String strUserId = "";
		    strTransactionId = GmCommonClass.parseNull((String) hmReturn.get("TRANSACTIONID"));
		    strPartNumber = GmCommonClass.parseNull((String) hmReturn.get("PARTNUMBER"));
		    strQuantity = GmCommonClass.parseNull((String) hmReturn.get("QUANTITY"));
		    strOldCntrlNumber = GmCommonClass.parseNull((String) hmReturn.get("OLDCNTRLNUMBER"));
		    strNewCntrlNumber = GmCommonClass.parseNull((String) hmReturn.get("NEWCNTRLNUMBER"));
		    strUserId = GmCommonClass.parseNull((String) hmReturn.get("USERID"));
		    
	        gmDBManager.setPrepareString("gm_pkg_op_change_lot.gm_save_change_lot", 7);
		    gmDBManager.registerOutParameter(7,OracleTypes.VARCHAR);
			gmDBManager.setString(1, strTransactionId);
			gmDBManager.setString(2, strPartNumber);
			gmDBManager.setString(3, strQuantity);
			gmDBManager.setString(4, strOldCntrlNumber);
			gmDBManager.setString(5, strNewCntrlNumber);
			gmDBManager.setString(6, strUserId);
			gmDBManager.execute();
			strChangeLotString = GmCommonClass.parseNull(gmDBManager.getString(7));
			gmDBManager.commit();
			return strChangeLotString;
		  }

}
