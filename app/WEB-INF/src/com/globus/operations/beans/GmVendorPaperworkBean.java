package com.globus.operations.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.common.forms.GmVendorPaperworkForm;
import com.globus.valueobject.common.GmDataStoreVO;

import oracle.jdbc.driver.OracleTypes;

public class GmVendorPaperworkBean extends GmBean{

	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing Logger
	
	
	
	public GmVendorPaperworkBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	

	  public GmVendorPaperworkBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	/**
	 * fetchPODetailsbyGUID - Method gets call from the action to get the PO details
	 * for loading PO Paperwork
	 * 
	 * @return HashMap
	 **/

	public HashMap fetchPODetailsbyGUID(String strGUID) throws AppError {

		HashMap hmReturn = new HashMap();
        GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
        gmDBManager.setPrepareString("gm_pkg_op_vendorportal.gm_fch_podetails_by_guid", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strGUID);
		gmDBManager.execute();
        hmReturn = GmCommonClass
				.parseNullHashMap((HashMap) gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2)));

		gmDBManager.close();
		log.debug("hmReturn.size() -->" + hmReturn.size());
        return hmReturn;
    }

	/**
	 * fetchWODetailsbyGUID - Method gets call from the action to get the WO details
	 * for loading WO Paperwork
	 * 
	 * @return HashMap
	 **/
	public HashMap fetchWODetailsbyGUID(String strGUID) throws AppError {

		HashMap hmReturn = new HashMap();
        GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
        gmDBManager.setPrepareString("gm_pkg_op_vendorportal.gm_fch_wodetails_by_guid", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strGUID);
		gmDBManager.execute();
        hmReturn = GmCommonClass
				.parseNullHashMap((HashMap) gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2)));

		gmDBManager.close();
		log.debug("hmReturnwopaperwork.size() -->" + hmReturn.size());
        return hmReturn;
    }

}
