/*
 * Module: GmOperationsBean.java Author: Dhinakaran James Project: Globus Medical App Date-Written:
 * 11 Mar 2004 Security: Unclassified Description: Testing Bean
 * 
 * Revision Log (12/18/08 bvidyasankar DHR Issue 1366)
 * --------------------------------------------------------- 12/18/08 bvidyasankar Added Status_fl
 * and Work_Order_id in procedure call in modifyDHRforRec() 12/23/08 bvidyasankar Modified input
 * parameter type of of getDHRList to HashMap and added an additional filter project id (Issue 1976)
 */

package com.globus.operations.beans;

import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.jobs.GmJob;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmLoanerBean extends GmSalesFilterConditionBean {

  // Instantiating the Logger
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  public GmLoanerBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmLoanerBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }
  static ArrayList alLoanerRequestActiveSets;

  /**
   * get SetMappingDetails - This method will return all the set mapping details
   * 
   * @param HashMap
   * 
   * @return ArrayList
   * @exception AppError
   **/

  public ArrayList fetchSetMappingDetails(HashMap hmParam) throws AppError {

    String strLoanerSetID = "";
    String strConsignSetID = "";
    ArrayList alResult = null;
    strLoanerSetID = GmCommonClass.parseNull((String) hmParam.get("LOANERSETID"));
    strConsignSetID = GmCommonClass.parseNull((String) hmParam.get("CONSIGNSETID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_loaner.gm_fch_setmap", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLoanerSetID);
    gmDBManager.setString(2, strConsignSetID);
    gmDBManager.execute();

    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    return alResult;
  }

  /**
   * get editMappingDetails - This method will return hashMap of set Map
   * 
   * @param String
   * 
   * @return HashMap
   * @exception AppError
   **/

  public HashMap fetchEditMappingDetails(String strLoanerSetID) throws AppError {
    HashMap hResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_loaner.gm_fch_editsetmap", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLoanerSetID);
    gmDBManager.execute();
    hResult = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hResult;
  }

  /**
   * Save SetMappingDetails - This method will save the set map info
   * 
   * @param HashMap
   * 
   * @return void
   * @exception AppError
   **/
  public void saveSetMappingDetails(HashMap hmParam) throws AppError {
    String strLoanerSetID = "";
    String strConsignSetID = "";
    String strUserID = "";
    ArrayList alResult = null;
    HashMap hResult = null;
    strLoanerSetID = GmCommonClass.parseNull((String) hmParam.get("LOANERSETID"));
    strConsignSetID = GmCommonClass.parseNull((String) hmParam.get("CONSIGNSETID"));
    strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_loaner.gm_sav_setmap", 3);
    gmDBManager.setString(1, strLoanerSetID);
    gmDBManager.setString(2, strConsignSetID);
    gmDBManager.setString(3, strUserID);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * author: Gopinathan fetchLoanerTransferLog - This method fetch Loaner Transfer records for
   * report
   * 
   * @param HashMap
   * 
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList fetchLoanerTransferLog(HashMap hmParam) throws AppError {

    ArrayList alResult = new ArrayList();
    String strDistFrm = GmCommonClass.parseNull((String) hmParam.get("FROMDISTRIBUTOR"));
    String strDistTo = GmCommonClass.parseNull((String) hmParam.get("TODISTRIBUTOR"));
    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strTranFromDt = GmCommonClass.parseNull((String) hmParam.get("TRANSFERDTFROM"));
    String strTranToDt = GmCommonClass.parseNull((String) hmParam.get("TRANSFERDTTO"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STRSTATUSFL"));
    log.debug("===========strSetId=======" + strSetId);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_loaner_transfer.gm_fch_loaner_transfer_log", 7);

    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);

    gmDBManager.setString(1, strDistFrm);
    gmDBManager.setString(2, strDistTo);
    gmDBManager.setString(3, strSetId);
    gmDBManager.setString(4, strTranFromDt);
    gmDBManager.setString(5, strTranToDt);
    gmDBManager.setString(6, strStatus);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(7));
    gmDBManager.close();
    return alResult;
  }

  /**
   * end of fetchLoanerTransferLog method
   */

  /**
   * Description : Fetch Rep Details and also Create the Report Data and sends the detail in Mail
   * Author : Rajkumar Jayakumar
   */
  public void sendLoanerTansferMail(String strAllConsIds, String strFromDistName) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmRepDet = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    try {
      String strCc = "";
      String strToRepEmail = "";
      String strRep_ID = "";
      String strRepName = "";
      String strRepEmail = "";
      String strAssrepID = "";
      String strAssRepName = "";
      String strAssRepEmail = "";
      String strFromRepName = "";
      
      log.debug("Extracted strAllConsIds: " + strAllConsIds);
      gmDBManager.setPrepareString("GM_PKG_OP_LOANER_TRANSFER.gm_fch_loantransf_rep_details", 2);
      gmDBManager.setString(1, strAllConsIds);
      gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
      gmDBManager.execute();
      ArrayList alRepDet =
          GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
              .getObject(2)));
      log.debug("the value inside alRepDet*****" + alRepDet);
      gmDBManager.close();

      for (int i = 0; i < alRepDet.size(); i++) {
        hmRepDet = (HashMap) alRepDet.get(i);

        strRepName = (String) hmRepDet.get("REP_NAME");
        strRep_ID = (String) hmRepDet.get("REP_ID");
        strToRepEmail = (String) hmRepDet.get("TO_REP_EMAIL");
        strAssRepName = GmCommonClass.parseNull((String) hmRepDet.get("ASSOCREPNM"));
        strAssrepID = GmCommonClass.parseNull((String) hmRepDet.get("ASSOCREPID"));
        strAssRepEmail = GmCommonClass.parseNull((String) hmRepDet.get("ASSOCREPEMAIL"));
        log.debug("the value inside strAssRepEmail*****" + strAssRepEmail);

        /*
         * Depending on the number of sets that are being transferred,that many emails are sent to
         * the rep. Check the if condition here only one e-mail is being sent out with the number of
         * sets that are being transferred.
         */
        if (!strAssrepID.equals("")) {
          strRepName = strAssRepName;
          strToRepEmail = strAssRepEmail;
        }

        if (!strRepEmail.equals(strToRepEmail)) {
          gmDBManager = new GmDBManager(getGmDataStoreVO());
          gmDBManager.setPrepareString("GM_PKG_OP_LOANER_TRANSFER.gm_fch_loantrans_email_details",
              4);
          gmDBManager.setString(1, GmCommonClass.parseNull(strAllConsIds));
          gmDBManager.setString(2, GmCommonClass.parseNull(strRep_ID));
          gmDBManager.setString(3, GmCommonClass.parseNull(strAssrepID));
          gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
          gmDBManager.execute();
          ArrayList alRepData = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
          gmDBManager.close();
          strFromRepName = GmCommonClass.parseNull((String) hmRepDet.get("FROM_REP_NAME")); 
          hmRepDet.put("FROM_REP_NAME", strFromDistName);

          GmEmailProperties gmEmailProperties = new GmEmailProperties();
          String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
          GmResourceBundleBean gmResourceBundleBean =
        		  GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
          gmEmailProperties.setSender(gmResourceBundleBean.getProperty("GmLoanerTransferEmail."
              + GmEmailProperties.FROM));
          strCc = gmResourceBundleBean.getProperty("GmLoanerTransferEmail." + GmEmailProperties.CC);
          log.debug("the value inside strCc*****" + strCc);
          gmEmailProperties.setCc((String) hmRepDet.get("CC_REP_EMAIL") + "," + strCc);
          gmEmailProperties.setMimeType(gmResourceBundleBean.getProperty("GmLoanerTransferEmail."
              + GmEmailProperties.MIME_TYPE));
          gmEmailProperties.setSubject(gmResourceBundleBean.getProperty("GmLoanerTransferEmail."
              + GmEmailProperties.SUBJECT));
          gmEmailProperties.setRecipients(strToRepEmail);

          hmRepDet
              .put("GMCSPHONE", gmResourceBundleBean.getProperty("GmCommon" + ".GlobusCSPhone"));
          hmRepDet
              .put("GMCSEMAIL", gmResourceBundleBean.getProperty("GmCommon" + ".GlobusCSEmail"));

          GmJasperMail jasperMail = new GmJasperMail();
          jasperMail.setJasperReportName("/GmLoanerTransfer.jasper");
          jasperMail.setAdditionalParams(hmRepDet);
          jasperMail.setReportData(alRepData);
          jasperMail.setEmailProperties(gmEmailProperties);
          hmReturn = jasperMail.sendMail();
          strRepEmail = strToRepEmail;
          log.debug("strRepEmail==>" + strRepEmail);
        }
      }
    } catch (Exception e) {
      log.error("Exception in sendLoanerTransferEmail : " + e.getMessage());
      hmReturn.put(GmJob.EXCEPTION, e);
    }
  }

  /**
   * End of Rajkumar Jayakumar Code
   */
  /**
   * fetch OpenLoanerRequests - This method will get OpenLoanerRequests info
   * 
   * @param String Author : Kulanthaivelu Ramasamy
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList fetchOpenLoanerRequests(String strDistId, String strReqType) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_op_loaner.gm_fch_open_requests", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR); // header
    gmDBManager.setString(1, strDistId);
    gmDBManager.setString(2, strReqType);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    return alResult;
  }

  /**
   * fetchLoanxtnsShippedToday - To fetch loaners that are shipped today
   * 
   * @param String Author : Vprasath
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList fetchLoanxtnsShippedToday(String strLoaners) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_op_loaner_transfer.gm_fch_loanxtns_ship_today", 2);
    gmDBManager.setString(1, strLoaners);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR); // header
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return alResult;
  }

  /**
   * saveLoanerTransfer - To transfer loaners between the given distributors
   * 
   * @param String
   * @Author : Vprasath
   * @return ArrayList
   * @exception AppError
   **/
  public String saveLoanerTransfer(HashMap hmParam) throws AppError {
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("STRINPUTSTRING"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strFromDistName = GmCommonClass.parseNull((String) hmParam.get("FROMDISTNAME"));
    String strToDistName = GmCommonClass.parseNull((String) hmParam.get("TODISTNAME"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strMsg = "distributor ";
    String strMessage = "";
    String strLoaners = "";
    String strReq = "";
    String strDate = "";
    String strSet = "";
    String strTransType = "";
    String strDistId = "";
    Iterator iLoanersToday;
    String strLoanersToday = "";
    String strRepid = "";
    String strAccId = "";
    String strAssocRepid = "";
    ArrayList alLnXtnsDetails = new ArrayList();
    HashMap hmLnXtnsDetails = new HashMap();
    if (strType.equals("4119")) {
      strMsg = "employee ";
    }
    // perform loaner transfer
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_loaner_transfer.gm_op_sav_loaner_transfer", 8);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strUserId);
    gmDBManager.setString(3, strType);
    gmDBManager.registerOutParameter(4, Types.VARCHAR);
    gmDBManager.registerOutParameter(5, Types.VARCHAR);
    gmDBManager.registerOutParameter(6, Types.VARCHAR);
    gmDBManager.registerOutParameter(7, Types.VARCHAR);
    gmDBManager.registerOutParameter(8, Types.VARCHAR);
    gmDBManager.execute();

    String strTransfLoanTxn = GmCommonClass.parseNull(gmDBManager.getString(4));
    String strFromReps = GmCommonClass.parseNull(gmDBManager.getString(5));
    String strFromAccounts = GmCommonClass.parseNull(gmDBManager.getString(6));
    String strToReps = GmCommonClass.parseNull(gmDBManager.getString(7));
    String strtoAccounts = GmCommonClass.parseNull(gmDBManager.getString(8));
    gmDBManager.commit();
    log.debug("strTransfLoanTxn " + strTransfLoanTxn);
    // create a comma seperated string of the CNids from the passed inputstring
    StringTokenizer strToken = new StringTokenizer(strInputString, "^|");
    while (strToken.hasMoreTokens()) {
      strLoaners += strToken.nextElement();
      strSet += strToken.nextElement();
      strReq += strToken.nextElement();
      strDate += strToken.nextElement();
      strTransType += strToken.nextElement();
      strDistId += strToken.nextElement();
      strRepid += strToken.nextElement();
      strAssocRepid += strToken.nextElement();
      strAccId += strToken.nextElement();
      if (strToken.hasMoreElements()) {
        strLoaners += ", ";
        strSet += ", ";
        strReq += ", ";
        strDate += ", ";
        strTransType += ", ";
        strDistId += ", ";
        strRepid += ", ";
        strAssocRepid += ", ";
        strAccId += ", ";
      }
      log.debug("strLoaners getting transferred are " + strLoaners);

    }
    // fetch loaner extension transactions that are shipped today for the transferred loaners
    alLnXtnsDetails = fetchLoanxtnsShippedToday(strLoaners);
    iLoanersToday = alLnXtnsDetails.iterator();

    while (iLoanersToday.hasNext()) {
      hmLnXtnsDetails = (HashMap) iLoanersToday.next();
      strLoanersToday += (String) hmLnXtnsDetails.get("REFID");
      strLoanersToday += ", ";
    }

    if (!strLoanersToday.equals("")) {
      strLoanersToday = strLoanersToday.substring(0, strLoanersToday.length() - 2);
    }


    log.debug("strLoanersToday : " + strLoanersToday);

    // send an email if loaner is transferred sucessfully
    sendLoanerTansferMail(strLoaners, strFromDistName);
    hmParam.put("INPUTSTRING", strLoaners);
    updateSendEmailFlg(hmParam);// update the email flg in table
    log.debug("email flg updated");
    // create the output message
    
   // If transaction is inhouse loaner transfer-4119
    if (strType.equals("4119")) {
         strMessage =
                "Loaner(s)".concat(strLoaners) + " was transferred from " + strMsg.concat(strFromDistName)
                    + " to " + strMsg.concat(strToDistName);
        }else{
        	strMessage =
            		"The requested set(s) have been transferred from "+ strFromReps+ " : " +strFromAccounts+" to "+strToReps+ " : "+strtoAccounts;
        }

    if (!strLoanersToday.equals("")) {
      strMessage =
          strMessage.concat(".<br> LN Transactions shipped today for the transferred loaners are ");
      strMessage = strMessage.concat(strLoanersToday);
    }
    if (!strTransfLoanTxn.equals("")) {
      strTransfLoanTxn = strTransfLoanTxn.substring(0, strTransfLoanTxn.length() - 1);
      strMessage = strMessage.concat(".<br> Open LN Transactions transferred are ");
      strMessage = strMessage.concat(strTransfLoanTxn);
    }
    if (!strMessage.equals("")) {
      log.debug("strMessage success msg is " + strMessage);
      throw new AppError(strMessage, "", 'S');
    }
    return strMessage;
  }

  /**
   * saveEditLoanerTransferDate - to overwrite the transfer the date after the loaner is transferred
   * 
   * @param hmParam
   * @return
   * @throws AppError
   */
  public void editLoanerTransferDate(HashMap hmParam) throws AppError {
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    // perform loaner transfer
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_loaner_transfer.gm_op_sav_loaner_transfer_Date", 2);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * To Save the email flag after the email is sent
   * 
   * @param hmParam
   * @throws AppError
   */
  public void updateSendEmailFlg(HashMap hmParam) throws AppError {
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    // perform loaner transfer
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    log.debug("saving flg strInputString" + strInputString);
    gmDBManager.setPrepareString("gm_pkg_op_loaner_transfer.gm_op_sav_send_email_flg", 2);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  public ArrayList fetchProcessTransaction(String strConId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_op_loaner.gm_fch_process_transaction", 2);
    gmDBManager.setString(1, strConId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR); // header
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return alResult;
  }

  /**
   * update the inhouse loaner
   * 
   * @param hmParam
   * @throws AppError
   */
  public void updateInhouseLoaner(HashMap hmParam) throws AppError {
    String strTxnId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    log.debug("Updating the inhouse loaner");
    gmDBManager.setPrepareString("gm_pkg_op_loaner.gm_sav_inhouse_loaner ", 2);
    gmDBManager.setString(1, strTxnId);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  public ArrayList fetchFieldSalesLoanerDetail(HashMap hmParam) throws AppError {

    ArrayList alReturn = new ArrayList();
    initializeParameters(hmParam);

    String strSetIds = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strEtchId = GmCommonClass.parseNull((String) hmParam.get("ETCHID"));
    String strConId = GmCommonClass.parseNull((String) hmParam.get("CNID"));
    String strOverdue = GmCommonClass.parseNull((String) hmParam.get("DAYSOVERDUE"));
    String strSetName = GmCommonClass.parseNull((String) hmParam.get("SETNAME"));
    String strRequestID = GmCommonClass.parseNull((String) hmParam.get("REQID"));
    String strDueDays = GmCommonClass.parseNull((String) hmParam.get("RETURNIN"));
    String strDistIds = GmCommonClass.parseNull((String) hmParam.get("DISTIDS"));
    String strUserIdString = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT t526.c525_product_request_id reqid, t504.c504_consignment_id CONID ,t504.c207_set_id SETID ");
    sbQuery
        .append(" ,get_set_name(t504.c207_set_id) SNAME, t504a.c504a_etch_id ETCHID,t504a.c504a_status_fl LSFL,");
    sbQuery
        .append(" gm_pkg_op_loaner.get_loaner_status(T504A.C504A_STATUS_FL) LOANSFL , get_rep_name(t504b.c703_sales_rep_id) repname");
    sbQuery
        .append(" ,get_distributor_name(t504.C701_DISTRIBUTOR_ID) distname,get_account_name(t504b.c704_account_id) accname  ");
    sbQuery
        .append(" ,DECODE(t504a.c504a_status_fl,20,TRUNC(CURRENT_DATE) - t504a.c504a_loaner_dt) ELPDAYS");
    sbQuery.append(" ,TRUNC(CURRENT_DATE)- t504a.c504a_expected_return_dt OVERDUE ");
    sbQuery.append(" ,t504a.c504a_expected_return_dt EDATE,  t504a.c504a_loaner_dt LDATE ");
    sbQuery
        .append(" ,decode (t504a.c504a_expected_return_dt,TRUNC(CURRENT_DATE)+2,'Y','N') isdue ");
    sbQuery.append(", GET_REP_NAME(t504b.c703_ass_rep_id) assocrepname ");
    sbQuery
        .append(" FROM t504_consignment t504, t504a_consignment_loaner t504a,t504a_loaner_transaction t504b, ");
    sbQuery.append(" t526_product_request_detail t526 ");
    if (!strSetName.equals("") && strRequestID.equals("")) {
      sbQuery.append(", t207_set_master t207");
    }
    sbQuery.append(getAccessFilterClauseWithRepID());
    sbQuery.append(" ,t907_shipping_info t907 ");
    sbQuery.append(" WHERE t504.c504_status_fl    = '4' AND t504.c504_verify_fl = '1' ");
    sbQuery.append(" AND T504.C504_VOID_FL       IS NULL AND t504.c504_type = 4127 ");;
    sbQuery.append(" AND t504.c504_consignment_id = t504a.c504_consignment_id");
    sbQuery.append(" AND t504.c504_consignment_id = t504b.c504_consignment_id");
    sbQuery
        .append(" and t526.C526_PRODUCT_REQUEST_DETAIL_ID = t504b.C526_PRODUCT_REQUEST_DETAIL_ID");
    sbQuery.append(" AND t526.C526_VOID_FL is null");
    // commented this code for PMT-308
    // sbQuery.append(" and t504b.C703_SALES_REP_ID = t703.C703_SALES_REP_ID");
    // sbQuery.append(" and t504b.C704_ACCOUNT_ID = t704.C704_ACCOUNT_ID");
    // sbQuery.append(" AND t504b.c703_sales_rep_id   = V700.REP_ID");
    // sbQuery.append(" and t504.C701_DISTRIBUTOR_ID = t701.C701_DISTRIBUTOR_ID");
    sbQuery
        .append(" and t504b.C504A_RETURN_DT is null AND t504b.c504a_void_fl is null AND t504a.c504a_void_fl IS NULL");
    sbQuery.append(" AND t504.C701_DISTRIBUTOR_ID   = V700.D_ID");
    // The below lines are added for PMT-2202.
    sbQuery.append(" AND t504b.c703_sales_rep_id = V700.rep_id ");
    sbQuery
        .append(" AND T907.C907_REF_ID = t504.c504_consignment_id AND T526.C525_Product_Request_Id = T907.C525_Product_Request_Id");
    sbQuery.append(" AND T907.C907_STATUS_FL  = '40'");
    sbQuery.append(" AND T907.C907_VOID_FL IS NULL");
    sbQuery.append(" AND T907.C907_Shipped_Dt IS NOT NULL");
    sbQuery.append(" AND TRUNC (t907.C907_SHIPPED_DT) <= TRUNC (CURRENT_DATE)");



    if (!strDueDays.equals("") && !strDueDays.equals("0")) {
      sbQuery.append(" AND ");
      sbQuery.append(" t504a.c504a_expected_return_dt = TRUNC(CURRENT_DATE +");
      sbQuery.append(strDueDays);
      sbQuery.append(")");
    }
    if (!strDistIds.equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(" t504.C701_DISTRIBUTOR_ID IN (");
      sbQuery.append(strDistIds);
      sbQuery.append(")");
    }
    if (!strSetIds.equals("")) {
      strSetIds = strSetIds.replaceAll(",", "','");
      sbQuery.append(" AND ");
      sbQuery.append(" t504.c207_set_id IN ('");
      sbQuery.append(strSetIds);
      sbQuery.append("')");
    }
    if (!strSetName.equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(" t207.c207_set_id = t504.c207_set_id");
      sbQuery.append(" AND UPPER(t207.c207_set_nm) LIKE UPPER('%");
      sbQuery.append(strSetName);
      sbQuery.append("%')");
    }
    if (!strRequestID.equals("")) {
      // The below lines are commented to show the data based on Request No text field.
      /*
       * sbQuery.append(" AND  t504.c207_set_id in (select t207.c207_set_id  from ");
       * sbQuery.append(" t207_set_master t207, t526_product_request_detail t526 ");
       * sbQuery.append(" where t526.c207_set_id =  t207.c207_set_id ");
       */
      sbQuery.append(" AND t526.c525_product_request_id = ");
      sbQuery.append(strRequestID);
      // sbQuery.append(")");
    }
    if (!strStatus.equals("")) {
      strStatus = strStatus.replaceAll(",", "','");
      sbQuery.append(" AND ");
      sbQuery.append(" t504a.c504a_status_fl IN ('");
      sbQuery.append(strStatus);
      sbQuery.append("')");
    } else {
      sbQuery.append(" AND t504a.c504a_status_fl   IN ('20','5','10','21')");//21-Disputed -added for PMT-50977
    }
    if (!strEtchId.equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(" UPPER(t504a.c504a_etch_id) LIKE UPPER('%");
      sbQuery.append(strEtchId);
      sbQuery.append("%')");
    }
    if (!strConId.equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(" UPPER(t504.c504_consignment_id) LIKE UPPER('%");
      sbQuery.append(strConId);
      sbQuery.append("%')");
    }
    if (!strOverdue.equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(" t504a.c504a_expected_return_dt < TRUNC(CURRENT_DATE)");
    }
    sbQuery.append(" ORDER BY t504a.c504a_expected_return_dt DESC");

    log.debug(" Query for FieldSalesLoanerDetail is " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alReturn;
  }

  /*
   * commenting this code as its still not gone to production. public ArrayList
   * fetchLoanerStatusLog(HashMap hmParams)throws AppError{ String
   * strUser=(String)hmParams.get("USERID"); String strSetId=(String)hmParams.get("SETID"); String
   * strStatus=(String)hmParams.get("STATUSID"); String strFromDate=(String)hmParams.get("FRMDATE");
   * String strToDate=(String)hmParams.get("TODATE");
   * 
   * GmDBManager gmDBManager = new GmDBManager(); ArrayList alResult = new ArrayList();
   * 
   * gmDBManager.setPrepareString("gm_pkg_op_loaner.gm_fch_Loaner_status_log", 6);
   * gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
   * 
   * gmDBManager.setString(1, strUser); gmDBManager.setString(2, strSetId); gmDBManager.setString(3,
   * strStatus); gmDBManager.setString(4, strFromDate); gmDBManager.setString(5, strToDate);
   * gmDBManager.execute();
   * 
   * alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));
   * 
   * gmDBManager.close();
   * 
   * return alResult; }
   * 
   * public ArrayList fetchSetList()throws AppError { GmDBManager gmDBManager = new GmDBManager();
   * ArrayList alResult = new ArrayList();
   * gmDBManager.setPrepareString("gm_pkg_op_loaner.gm_fch_set_list", 1);
   * gmDBManager.registerOutParameter(1, OracleTypes.CURSOR); // header gmDBManager.execute();
   * alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
   * gmDBManager.close();
   * 
   * return alResult; }
   */
  // method to fetch loaner requests where surgery date > sysdate-10
  public ArrayList fetchLoanerRequests(String strReqforId, String strLoanerType) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_op_loaner_transfer.gm_fch_loaner_requests", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strReqforId);
    gmDBManager.setString(2, strLoanerType);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    return alResult;
  }

  // this method to fetch in-house loaner set status.
  public ArrayList fetchInHouseLoanerSetStatus(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strDistIds = GmCommonClass.parseNull((String) hmParam.get("DISTIDS"));

    gmDBManager.setPrepareString("gm_pkg_op_loaner_transfer.gm_fch_inhouse_loaner_status", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, strDistIds);
    gmDBManager.setString(2, strType);
    gmDBManager.setString(3, strStatus);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
    gmDBManager.close();
    return alResult;
  }
  
  /**
   * getLoanerRequestActiveSets(): fetches the active loaner set list dropdowns without Demo Sets by adding the demo sets type.
   * 
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList getLoanerRequestActiveSets() throws AppError { 
	 alLoanerRequestActiveSets = new ArrayList();	 
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_loaner_allocation.gm_fch_loaner_req_active_sets", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR); // Return the cursor
    gmDBManager.execute();
    alLoanerRequestActiveSets = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return alLoanerRequestActiveSets;
  }
}// end of class GmLoanerBean
