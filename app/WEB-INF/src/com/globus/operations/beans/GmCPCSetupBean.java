/*
 * Module:       GmCPCSetupBean
 * Author:       Thamarai selvan
 * Project:      Globus Medical App
 * Date-Written: 14 Mar 2007
 * Revision Log (mm/dd/yy initials description)
 * ---------------------------------------------------------
 * mm/dd/yy xxx  What you changed
 *
 */


package com.globus.operations.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmCPCSetupBean extends GmBean {
	
	public GmCPCSetupBean(GmDataStoreVO gmDataStoreVO) throws AppError {
	super(gmDataStoreVO);
	// TODO Auto-generated constructor stub
    }
	GmCommonClass gmCommon = new GmCommonClass();
	GmPartyBean gmPartyBean = new GmPartyBean();
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
	// Initialize
	// the
	// Logger
	// Class.

	ArrayList alCPCList = new ArrayList();
	
	// to load CPC Name in the dropdown
	public ArrayList loadCpcList() throws AppError,Exception {
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_cpc_rpt.gm_fch_cpc_data",1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReturn = GmCommonClass.parseNullArrayList((ArrayList)gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1)));
		gmDBManager.close();
		return alReturn;
	} // End of fetchAcctShipDtls
	
	// to save CPC Details
	public String saveCPCDtls(HashMap hmParam)throws AppError  {
		
	    String strCpcId = GmCommonClass.parseNull((String)hmParam.get("CPCID"));
	    String strCpcName = GmCommonClass.parseNull((String)hmParam.get("CPCNAME"));
	    String strCpcAddr = GmCommonClass.parseNull((String)hmParam.get("CPCADDR"));
	    String strCpcBarLevel = GmCommonClass.parseNull((String)hmParam.get("CPCBARCODE"));
	    String strCpcFinLevel = GmCommonClass.parseNull((String)hmParam.get("CPCFINAL"));
	    String strCpcActiveFl = GmCommonClass.parseNull((String)hmParam.get("ACTIVEFL"));
	    String strLog = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
	    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	    GmCommonBean gmCom = new GmCommonBean(getGmDataStoreVO());
            GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
            gmDBManager.setPrepareString("gm_pkg_op_cpc_txn.gm_upd_cpc_data",7);
            gmDBManager.registerOutParameter(2, java.sql.Types.CHAR);
            gmDBManager.setString(1,strUserId);
            gmDBManager.setString(2,strCpcId);
            gmDBManager.setString(3,strCpcName);
            gmDBManager.setString(4,strCpcAddr);
            gmDBManager.setString(5,strCpcBarLevel);
            gmDBManager.setString(6,strCpcFinLevel);
            gmDBManager.setString(7,strCpcActiveFl);
            gmDBManager.execute();
            strCpcId =  GmCommonClass.parseNull((String)gmDBManager.getString(2));
            if (!strLog.equals("")) {
        	gmCom.saveLog(gmDBManager, strCpcId, strLog, strUserId, "1295");
    	    }
            gmDBManager.commit();
            return strCpcId;
	}// End of saveAcctShipSetup
	
	//to load cpc screen values when drop down changes
	public HashMap loadEditCpcInfo(String strCPCId) throws AppError,Exception {		
	    HashMap hmReturn = new HashMap();
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_op_cpc_rpt.gm_fch_cpc_item",2);
	    gmDBManager.setString(1,strCPCId);
	    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	    gmDBManager.execute();
	    hmReturn = GmCommonClass.parseNullHashMap((HashMap)gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2)));
	    gmDBManager.close();
	    return hmReturn;
	} // End of loadEditCpcInfo
	
	// get the count of cpc name
	public int getCpcCount(String strCpcName) throws AppError,Exception {
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setFunctionString("gm_pkg_op_cpc_rpt.get_cpc_count",1);
	    gmDBManager.registerOutParameter(1, OracleTypes.INTEGER);
	    gmDBManager.setString(2, strCpcName); 
	    gmDBManager.execute();
	    int count = gmDBManager.getInt(1);
	    gmDBManager.close();
	    return count;
	} // End of getCpcCount

}
