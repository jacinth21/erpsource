/*
 * This file is for the methods which are using for Part Label process screen
 */

package com.globus.operations.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmPartLabelingBean extends GmBean {

  // Instantiating the Logger
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  public GmPartLabelingBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmPartLabelingBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * savePartApprRejDetails - Method gets call if the part number is partially approving/rejecting
   * 
   * @param HashMap hmParam
   * @return String
   * @exception AppError
   **/
  public String savePartApprRejDetails(HashMap hmParam) throws AppError {
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strLog = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    String strPNQNTxnId = initiatePNQNTxn(gmDBManager, hmParam);
    updatePTRDTransaction(gmDBManager, hmParam);
    savePTRDStatus(gmDBManager, hmParam);
    if (!strLog.equals("")) {
      // 103931: Part Re-Desig Log
      gmCommonBean.saveLog(gmDBManager, GmCommonClass.parseNull((String) hmParam.get("TXNID")),
          strLog, GmCommonClass.parseNull((String) hmParam.get("USERID")), "103931");
    }
    gmDBManager.commit();
    return strPNQNTxnId;
  }

  /**
   * savePartApprRejDetails - Method gets call if all the part numbers are approved
   * 
   * @param HashMap hmParam
   * @return String
   * @exception AppError
   **/
  public String savePartApproveDetails(HashMap hmParam) throws AppError {
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strLog = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    savePTRDStatus(gmDBManager, hmParam);
    if (!strLog.equals("")) {
      // 103931: Part Re-Desig Log
      gmCommonBean.saveLog(gmDBManager, GmCommonClass.parseNull((String) hmParam.get("TXNID")),
          strLog, GmCommonClass.parseNull((String) hmParam.get("USERID")), "103931");
    }
    gmDBManager.commit();
    return "";
  }

  /**
   * savePartRejectDetails - Method gets call if all the part numbers are rejected
   * 
   * @param HashMap hmParam
   * @return String
   * @exception AppError
   **/
  public String savePartRejectDetails(HashMap hmParam) throws AppError {
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strLog = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    String strPNQNTxnId = initiatePNQNTxn(gmDBManager, hmParam);
    cancelPTRD(gmDBManager, hmParam);
    if (!strLog.equals("")) {
      // 103931: Part Re-Desig Log
      gmCommonBean.saveLog(gmDBManager, GmCommonClass.parseNull((String) hmParam.get("TXNID")),
          strLog, GmCommonClass.parseNull((String) hmParam.get("USERID")), "103931");
    }
    gmDBManager.commit();
    strPNQNTxnId = strPNQNTxnId + "|S";
    return strPNQNTxnId;
  }

  /**
   * savePTRDStatus - Method to update the PTRD status to 2
   * 
   * @param HashMap hmParam
   * @return
   * @exception AppError
   **/
  public void savePTRDStatus(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
    gmDBManager.setPrepareString("gm_pkg_op_partlabeling_txn.gm_sav_part_label_status", 2);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("TXNID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("USERID")));
    gmDBManager.execute();
  }

  /**
   * savePTRDStatus - Method to initiate PNQN transaction
   * 
   * @param HashMap hmParam
   * @return
   * @exception AppError
   **/
  public String initiatePNQNTxn(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
    GmLogisticsBean gmLogisticsBean = new GmLogisticsBean(getGmDataStoreVO());
    String strNewTxnId = gmLogisticsBean.loadNextConsignId("REPACK-QUARAN");
    String strTxnId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    gmDBManager.setPrepareString("GM_SAVE_INHOUSE_ITEM_CONSIGN", 9);
    // 400079: Repack To Quarantine; 120420: From RePackage
    gmDBManager.registerOutParameter(9, java.sql.Types.CHAR);
    gmDBManager.setString(1, strNewTxnId);
    gmDBManager.setInt(2, Integer.parseInt("400079"));
    gmDBManager.setInt(3, Integer.parseInt("120420"));
    gmDBManager.setInt(4, Integer.parseInt("0"));
    gmDBManager.setString(5, strUserId);
    gmDBManager.setString(6, "PNQN created from " + strTxnId);
    gmDBManager.setString(7, strUserId);
    gmDBManager.setString(8, GmCommonClass.parseNull((String) hmParam.get("HREJSTRING")));
    gmDBManager.execute();
    updatePNQNStatus(gmDBManager, strNewTxnId, strUserId);
    return strNewTxnId;
  }

  /**
   * cancelPTRD - Method to void the PTRD transaction
   * 
   * @param HashMap hmParam
   * @return
   * @exception AppError
   **/
  public void cancelPTRD(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
    gmDBManager.setPrepareString("gm_pkg_common_cancel_op.gm_op_sav_void_inhousetxn", 2);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("TXNID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("USERID")));
    gmDBManager.execute();
  }

  /**
   * cancelPTRD - Method to update the PNQN status to Pending verification since the control number
   * is getting from PTRD
   * 
   * @param HashMap hmParam
   * @return
   * @exception AppError
   **/
  public void updatePNQNStatus(GmDBManager gmDBManager, String strTxnId, String strUserId)
      throws AppError {
    gmDBManager.setPrepareString("gm_pkg_op_partlabeling_txn.gm_op_upd_inhousetxn", 2);
    gmDBManager.setString(1, strTxnId);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
  }

  /**
   * cancelPTRD - Method to void the rejected parts in PTRD transaction
   * 
   * @param HashMap hmParam
   * @return
   * @exception AppError
   **/
  public void updatePTRDTransaction(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
    gmDBManager.setPrepareString("gm_pkg_op_partlabeling_txn.gm_op_upd_part_label", 3);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("TXNID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("USERID")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("HREJSTRING")));
    gmDBManager.execute();
  }
  /**
   * savePartLabelParameter()- this method is used to save the Part Label Parameter
   * 
   * @param HashMap hmParam
   * @return
   * @exception AppError
   **/

  public String savePartLabelParameter(HashMap hmParams) throws AppError {
    String strRef_Id = GmCommonClass.parseNull((String) hmParams.get("REFID"));
    String strAttribType = GmCommonClass.parseNull((String) hmParams.get("ATTRIBTYPE"));
    String strAttrVal = GmCommonClass.parseNull((String) hmParams.get("ATTRIBVALUE"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    HashMap hmRuturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_partlabeling_txn.gm_sav_printlabel_parameters", 4);
    gmDBManager.setString(1, strRef_Id);
    gmDBManager.setString(2, strAttribType);
    gmDBManager.setString(3, strAttrVal);
    gmDBManager.setString(4, strUserID);
    gmDBManager.execute();
    gmDBManager.commit();
    return null;
  }
  /**
   *  savePartLabelParameter()- this method is used to save the Part details
   * 
   * @param HashMap hmParam
   * @return
   * @exception AppError
   **/
  public void savePrintlabelProcess(HashMap hmParams) throws AppError {
	  GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strRef_Id = GmCommonClass.parseNull((String) hmParams.get("REFID"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strLog = GmCommonClass.parseNull((String) hmParams.get("TXT_LOGREASON"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_partlabeling_txn.gm_sav_printlabel_process", 2);
    gmDBManager.setString(1, strRef_Id);
    gmDBManager.setString(2, strUserID);
    gmDBManager.execute();
    if (!strLog.equals("")) {
        // 103931: Part Re-Desig Log
        gmCommonBean.saveLog(gmDBManager, GmCommonClass.parseNull((String) hmParams.get("REFID")),
            strLog, GmCommonClass.parseNull((String) hmParams.get("USERID")), "103931");
      }
    gmDBManager.commit();
  }
}// end of class GmPartLabelingBean
