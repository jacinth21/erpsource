/**
 * 
 */
package com.globus.operations.beans;

import java.util.HashMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.operations.abs.beans.GmUDIBean;

import org.apache.log4j.Logger;

/**
 * @author tsekaran
 *
 */
public class GmUSUDIBean extends GmUDIBean {
	
	public  void saveUDIInfo(GmDBManager gmDBmanager, HashMap hmInput){
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
		
		
		try
		{
		    
		    if(gmDBmanager == null){
		    	gmDBmanager = new GmDBManager();
		    }

		    String strTxnId    = GmCommonClass.parseNull((String)hmInput.get("TXNID")); 
		    String strUserId   = GmCommonClass.parseNull((String)hmInput.get("USERID")); 
		    
		    log.debug("strTxnId="+strTxnId);
		
		    gmDBmanager.setPrepareString("GM_UPDATE_UDI_FORMAT",2);

		    gmDBmanager.setString(1,strTxnId);
		    gmDBmanager.setString(2,strUserId);

		    gmDBmanager.execute();
		    
		    gmDBmanager.commit();
		}
		catch(Exception e)
		{
			GmLogError.log("Exception in GmOperationBean:saveUDIInfo",	"Exception is:" + e);
		}
	}
	

}
