package com.globus.operations.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmDonorLotRptBean extends GmBean {
Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j

public GmDonorLotRptBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmDonorLotRptBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }
	
	/**
     * fetchLotCodeProdInfo - Getting Lot Details based on the Donor
     * @param HashMap hmParam  
     * @return HashMap
     * @exception AppError
    **/ 
	public HashMap fetchLotCodeProdInfo(HashMap hmParam) throws AppError{		
		HashMap hmReturn = new HashMap();
		ArrayList alLotDetails = new ArrayList();
		String strDonorId = GmCommonClass.parseNull((String)hmParam.get("DONORID"));
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());          
        gmDBManager.setPrepareString("gm_pkg_op_donorlot_rpt.gm_fch_lot_info", 2);
        gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
        gmDBManager.setString(1, strDonorId);
        gmDBManager.execute();
        alLotDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
        hmReturn.put("LOTDETAILS", alLotDetails);
		log.debug("hmReturn" +hmReturn);
        gmDBManager.close();        
		return hmReturn;
	}	
}
