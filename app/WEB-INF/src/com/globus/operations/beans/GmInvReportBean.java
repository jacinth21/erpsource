/*
 * Module: GmInvReportBean.java Author: Dhinakaran James Project: Globus Medical App Date-Written:
 * 13 Feb 2007 Security: Unclassified Description:
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.operations.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmInvReportBean extends GmBean {

  GmCommonClass gmCommon = new GmCommonClass();

  // Instantiating the Logger
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  // Initialize
  // the
  // Logger
  // Class.
  public GmInvReportBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info. R
   * 
   * @param gmDataStore
   */
  public GmInvReportBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * getPartNumSales - This method will
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public ArrayList getPartNumInventory(String strProjId) throws AppError {
    ArrayList alResult = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT T205.C205_PART_NUMBER_ID PNUM,T205.C205_PART_NUM_DESC PDESC, ");
    sbQuery
        .append(" NVL(get_partnumber_qty(T205.C205_PART_NUMBER_ID,90813) - GET_QTY_IN_TRANSACTIONS_QUARAN(T205.C205_PART_NUMBER_ID),0) INQUARAN, C205_REV_NUM DRAWREV, ");
    sbQuery
        .append(" get_partnumber_qty(T205.C205_PART_NUMBER_ID,90814) INBULK, get_partnumber_qty(T205.C205_PART_NUMBER_ID,90812) INRETURNS, ");
    // hardcoding for biomaterials - temporary till new inv report -
    // James Feb 28, 2008
    sbQuery.append(" Get_Partnumber_Qty(T205.C205_PART_NUMBER_ID,90802) AQTY ");
    // sbQuery.append(" GET_PARTNUMBER_FA_QTY(C205_PART_NUMBER_ID)
    // FA,");
    // sbQuery.append(" GET_PARTNUMBER_FA_WIP_QTY(C205_PART_NUMBER_ID)
    // FAWIP ");
    sbQuery.append(" FROM T205_PART_NUMBER T205, T2023_PART_COMPANY_MAPPING T2023 ");
    sbQuery.append(" WHERE C202_PROJECT_ID ='");
    sbQuery.append(strProjId);
    sbQuery.append("' ");
    sbQuery.append(" AND T205.C205_PART_NUMBER_ID = T2023.C205_PART_NUMBER_ID ");
    sbQuery.append(" AND T2023.C1900_COMPANY_ID = " + getCompId());
    sbQuery.append(" AND T2023.C2023_VOID_FL IS NULL ");
    sbQuery.append(" ORDER BY T205.C205_PART_NUMBER_ID");
    log.debug(" Query for getPartNumInventory " + sbQuery.toString());
    alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alResult;
  } // End of getPartNumInventory

  /**
   * getPartNumSales - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public ArrayList getPartNumInventory(HashMap hmParam) throws AppError {

    ArrayList alResult = new ArrayList();
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
    String strProjId = GmCommonClass.parseZero((String) hmParam.get("PROJID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT PNUM,PDESC,INQUARAN,DRAWREV,INBULK,INRETURNS,AQTY FROM( SELECT T205.C205_PART_NUMBER_ID PNUM,C205_PART_NUM_DESC PDESC, ");
    sbQuery
//        .append(" - NVL(Get_Partnumber_Qty(T205.C205_PART_NUMBER_ID,90813) C205_REV_NUM DRAWREV, ");
    .append("get_qty_in_quarantine(T205.C205_PART_NUMBER_ID) INQUARAN, C205_REV_NUM DRAWREV, ");
    sbQuery 
//        .append(" Get_Partnumber_Qty(T205.C205_PART_NUMBER_ID,90814) INBULK,Get_Partnumber_Qty(T205.C205_PART_NUMBER_ID,90812) INRETURNS, ");
    .append("  get_qty_in_bulk(T205.C205_PART_NUMBER_ID) INBULK, GET_QTY_IN_RETURNS_HOLD(T205.C205_PART_NUMBER_ID)INRETURNS,");
    // hardcoding for biomaterials - temporary till new inv report -
    // James Feb 28, 2008
//    sbQuery.append(" Get_Partnumber_Qty(T205.C205_PART_NUMBER_ID,90802) AQTY,"); 
    // sbQuery.append(" GET_PARTNUMBER_FA_QTY(C205_PART_NUMBER_ID)
    // FA,");
    // sbQuery.append(" GET_PARTNUMBER_FA_WIP_QTY(C205_PART_NUMBER_ID)
    // FAWIP ");
    sbQuery.append("GET_MFG_PARTNUM_INV_QTY(T205.C205_PART_NUMBER_ID)  AQTY ");
    sbQuery.append(" FROM T205_PART_NUMBER T205, T2023_PART_COMPANY_MAPPING T2023 ");
    sbQuery.append(" WHERE C205_ACTIVE_FL = 'Y' ");
    if (!strProjId.equals("0")) {
      sbQuery.append(" AND C202_PROJECT_ID ='");
      sbQuery.append(strProjId);
      sbQuery.append("' ");
    }
    if (!strPartNum.equals("")) {
    	
    sbQuery.append(" AND REGEXP_LIKE(T205.c205_part_number_id,REGEXP_REPLACE('");
      sbQuery.append(strPartNum);
      sbQuery.append("','[+]','\\+'))");
    	
    }
    sbQuery.append(" AND T205.C205_PART_NUMBER_ID = T2023.C205_PART_NUMBER_ID ");
    sbQuery.append(" AND T2023.C1900_COMPANY_ID = " + getCompId());
    sbQuery.append(" AND T2023.C2023_VOID_FL IS NULL ");
    sbQuery.append(" ORDER BY T205.C205_PART_NUMBER_ID ) where  INQUARAN > 0 ");
    log.debug(" Query for getPartNumInventory " + sbQuery.toString());
    alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());


    return alResult;
  } // End of getPartNumInventory


  /**
   * getPartNumInventoryByProject - This method will
   * 
   * @param String strProjId
   * @return ArrayList
   * @exception AppError This method is a new one that uses a CURSOR. Created by Richard.
   */
  public ArrayList getPartNumInventoryByProject(String strProjId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());



    String strBeanMsg = "";
    String strMsg = "";

    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;
    gmDBManager.setPrepareString("GM_REPORT_PROJECT_PART_LIST", 2);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strProjId);

    gmDBManager.execute();
    rs = (ResultSet) gmDBManager.getObject(2);
    alReturn = gmDBManager.returnArrayList(rs);
    gmDBManager.close();

    return alReturn;
  } // End of getPartNumInventoryByProject

  /**
   * reportTransactionDetails - This method will return the Transaction Details for the listed IN
   * Parameter
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList reportTransactionDetails(HashMap hmParam) {
    ArrayList alReturn = new ArrayList();
    ArrayList alTempResult = new ArrayList();
    ArrayList alDBResult = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strPartID = GmCommonClass.parseNull((String) hmParam.get("PARTID"));
    String strPartName = GmCommonClass.parseNull((String) hmParam.get("PARTNM"));
    String strSort = GmCommonClass.parseNull((String) hmParam.get("SORT"));
    String strSortType = GmCommonClass.parseNull((String) hmParam.get("SORTTYPE"));
    String strTransId = GmCommonClass.parseNull((String) hmParam.get("TRANSID"));
    String strTransType = GmCommonClass.parseNull((String) hmParam.get("TRANSTYPE"));
    String strPartNumInputString = "";
    String strCompDateFormat = getCompDateFmt();
    boolean blIsFilter = false;

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      if (strPartID.length() > 500) {
        alTempResult = GmCommonClass.convertStringToList(strPartID, 500, ",");
      } else {

        alTempResult = new ArrayList();
        alTempResult.add(strPartID);
      }

      if (alTempResult != null) {
        for (Iterator itr = alTempResult.iterator(); itr.hasNext();)
          strPartNumInputString = (String) itr.next();
        if (strPartNumInputString.endsWith(",")) {
          strPartNumInputString =
              strPartNumInputString.substring(0, strPartNumInputString.length() - 1);
        }

      }

      sbQuery.append(" SELECT to_char(A.C214_CREATED_DT, '" + strCompDateFormat
          + "'|| ' HH24:MI:SS') CDATE, ");
      sbQuery.append(" GET_USER_NAME(A.C214_CREATED_BY) CBY, ");
      sbQuery.append(" A.C205_PART_NUMBER_ID PNUM, ");
      sbQuery.append(" B.C205_PART_NUM_DESC PDESC, ");
      sbQuery.append(" A.C214_ORIG_QTY OQTY, ");
      sbQuery.append(" A.C214_TRANS_QTY TQTY, ");
      sbQuery.append(" A.C214_NEW_QTY NQTY, ");
      sbQuery.append(" GET_CODE_NAME(A.C901_TYPE) TTYPE, ");
      sbQuery.append(" A.C901_TYPE TTYPEID, ");
      sbQuery.append(" A.C214_ID TID");
      sbQuery.append(" FROM T214_TRANSACTIONS A, ");
      sbQuery.append(" T205_PART_NUMBER B ");
      sbQuery.append(" WHERE A.C205_PART_NUMBER_ID = B.C205_PART_NUMBER_ID ");

      if (!strFromDt.equals("")) {
        sbQuery.append(" AND TRUNC(A.C214_CREATED_DT) >= TO_DATE('");
        sbQuery.append(strFromDt);
        sbQuery.append("', '" + strCompDateFormat + "') ");
        blIsFilter = true;
      }

      if (!strToDt.equals("")) {
        sbQuery.append(" AND TRUNC(A.C214_CREATED_DT) <= TO_DATE('");
        sbQuery.append(strToDt);
        sbQuery.append("', '" + strCompDateFormat + "') ");
        blIsFilter = true;
      }

      if (!strPartNumInputString.equals("")) {
        sbQuery.append(" AND A.C205_PART_NUMBER_ID IN ( ");
        sbQuery.append(strPartNumInputString);
        sbQuery.append(")");
        blIsFilter = true;
      }

      if (!strPartName.equals("")) {
        sbQuery.append(" AND UPPER(TRIM(C205_PART_NUM_DESC)) LIKE UPPER('");
        sbQuery.append(strPartName);
        sbQuery.append("') ");
        blIsFilter = true;
      }

      if (!strTransId.equals("")) {
        sbQuery.append(" AND A.C214_ID = '");
        sbQuery.append(strTransId);
        sbQuery.append("'");
        blIsFilter = true;
      }

      log.debug("^^^ strTransType ^^^" + strTransType);

      if (!strTransType.equals("")) {
        sbQuery.append(" AND A.C901_TRANSACTION_TYPE = ");
        sbQuery.append(strTransType);
      }
      // Added Plant_id to load records based on plant_id
      sbQuery.append(" AND  A.C5040_PLANT_ID =" + getCompPlantId());

      /*******************************************************************
       * Code to handle sort functionality
       ******************************************************************/
      if (!strSort.equals("")) {
        if (strSort.equals("CDATE")) {
          sbQuery.append(" ORDER BY  C214_CREATED_DT ");
        } else {
          sbQuery.append(" ORDER BY " + strSort);
        }

        // Sort order information
        if (strSortType.equals("1")) {
          sbQuery.append(" asc ");
        } else {
          sbQuery.append(" desc ");
        }

      } else {
        sbQuery.append(" ORDER BY A.C205_PART_NUMBER_ID, C214_TRANSACTIONS_ID desc");
      }

      log.debug(" Query for reporting transaction details " + sbQuery.toString());
      if (blIsFilter) {
        alDBResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
      }
      alReturn.addAll(alDBResult);
    } catch (Exception e) {
      GmLogError.log("Exception in GmInvReportBean:reportTransactionDetails", "Exception is:" + e);
    }
    return alReturn;
  } // End of reportTransactionDetails

  /**
   * getPartNumInvStatus - This method will
   * 
   * @param String strPartNumId
   * @return ArrayList
   * @exception AppError This method returns Inventory values for a particular part
   */
  public ArrayList getPartNumInvStatus(String strPartNum) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;
    gmDBManager.setPrepareString("GM_REPORT_PARTNUM_INV_STATUS", 2);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPartNum);

    gmDBManager.execute();
    rs = (ResultSet) gmDBManager.getObject(2);
    alReturn = gmDBManager.returnArrayList(rs);
    gmDBManager.close();

    return alReturn;
  } // End of getPartNumInvStatus

  /**
   * getPartNumInvReport - This method will
   * 
   * @param String strPartNumId
   * @return ArrayList
   * @exception AppError This method returns Inventory values for a particular part
   */

  public ArrayList getPartNumInvReport(HashMap hmParam) throws AppError {
    ArrayList alResult = new ArrayList();
    ArrayList alTempResult = new ArrayList();
    ArrayList alDBResult = new ArrayList();
    String strPartNumInputString = "";
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
    String strProjId = GmCommonClass.parseZero((String) hmParam.get("PROJID"));
    String strSubComponentFlag = GmCommonClass.parseNull((String) hmParam.get("SUBCOMPONENTFL"));
    String strExcludedInvMapping =
        GmCommonClass.parseNull((String) hmParam.get("EXCLUDEDINVENTORY"));
    String strEnableObsolete = GmCommonClass.parseNull((String) hmParam.get("ENABLEOBSOLETE"));
    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    log.debug("##### getPartNumInvReport: regexp: " + strPartNum + " >" + strProjId + " >"
        + strSubComponentFlag + " >" + strEnableObsolete + " >> strExcludedInvMapping >>"
        + strExcludedInvMapping);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strSrType = GmCommonClass.parseNull((String) hmParam.get("SRTYPE"));
    /**
     * Since regexp_like function is called in database, not able to give more than 500 characters,
     * hence the below function is used to overcome this problem.
     **/
    /**
     * Regexp_like function is called within this function
     * "gm_pkg_op_inventory. gm_fch_partnum_inv_rpt".
     **/
    if (strSrType.equals("BACKORDER")) {
      gmDBManager.setPrepareString("gm_pkg_op_inventory.gm_fch_partnum_inv_rpt", 8);
      gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
      gmDBManager.setString(1, strPartNum);
      gmDBManager.setString(2, strSubComponentFlag);
      gmDBManager.setString(3, strEnableObsolete);
      gmDBManager.setString(4, strProjId);
      gmDBManager.setString(5, strSrType);
      gmDBManager.setString(6, strExcludedInvMapping);
      gmDBManager.setString(7, strSetId);
      gmDBManager.execute();
      alDBResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(8));
      alResult.addAll(alDBResult);
    } else {
      if (strPartNum != null && strPartNum.length() > 500) {

        alTempResult = GmCommonClass.convertStringToList(strPartNum, 500, "|");
      } else {
        alTempResult = new ArrayList();
        alTempResult.add(strPartNum);
      }
      if (alTempResult != null) {
        for (Iterator itr = alTempResult.iterator(); itr.hasNext();) {
          strPartNumInputString = (String) itr.next();
          if (strPartNumInputString.endsWith("|")) {
            strPartNumInputString =
                strPartNumInputString.substring(0, strPartNumInputString.length() - 1);
          }
          gmDBManager.setPrepareString("gm_pkg_op_inventory.gm_fch_partnum_inv_rpt", 8);
          gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
          gmDBManager.setString(1, strPartNumInputString);
          gmDBManager.setString(2, strSubComponentFlag);
          gmDBManager.setString(3, strEnableObsolete);
          gmDBManager.setString(4, strProjId);
          gmDBManager.setString(5, strSrType);
          gmDBManager.setString(6, strExcludedInvMapping);
          gmDBManager.setString(7, strSetId);
          gmDBManager.execute();
          alDBResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(8));
          alResult.addAll(alDBResult);
        }
      }
    }
    gmDBManager.close();
    return alResult;
  }

  /*
   * public ArrayList getPartNumInvReport(HashMap hmParam) throws AppError { ArrayList alResult =
   * new ArrayList(); String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
   * String strProjId = GmCommonClass.parseZero((String) hmParam.get("PROJID")); String
   * strSubComponentFlag = GmCommonClass.parseNull((String) hmParam.get("SUBCOMPONENTFL"));
   * log.debug("##### getPartNumInvReport: regexp: " + strPartNum); String strEnableObsolete =
   * GmCommonClass.parseNull((String) hmParam.get("ENABLEOBSOLETE")); try {
   * 
   * DBConnectionWrapper dbCon = null; dbCon = new DBConnectionWrapper();
   * 
   * StringBuffer sbQuery = new StringBuffer();
   * sbQuery.append(" SELECT t205.c205_part_number_id pnum ,t205.c205_part_num_desc pdesc "); //
   * sbQuery.append(
   * " ,GET_MFG_PARTNUM_INV_QTY(t205.c205_part_number_id) - get_qty_in_transaction_rm (t205.c205_part_number_id) rmqty "
   * );
   * sbQuery.append(" ,NVL (rm_avail, 0) - get_qty_in_transaction_rm (t205.c205_part_number_id) rmqty "
   * ); sbQuery.append(
   * " ,(t205.c205_qty_in_quarantine  - nvl(( quar_alloc_to_scrap + quar_alloc_to_inv),0)  ) inquaran "
   * );
   * sbQuery.append(" ,t205.c205_qty_in_bulk  - (nvl(in_wip_set,0) + nvl(bulk_alloc_to_inv,0)) inbulk "
   * ); sbQuery.append(" ,(in_wip_set-TBE_ALLOC) in_wip_set ,TBE_ALLOC ");
   * sbQuery.append(" ,t205.c205_qty_in_returns_hold inreturns "); sbQuery.append(
   * " ,c205_rev_num drawrev ,GET_PARTNUMBER_PO_PEND_QTY(t205.c205_part_number_id) popend "); //
   * sbQuery
   * .append(" ,GET_PARTNUMBER_PO_WIP_QTY(t205.c205_part_number_id) POWIP, sales_allocated ,cons_alloc "
   * ); sbQuery.append(" ,sales_allocated ,cons_alloc "); sbQuery.append(
   * " ,quar_alloc_self_quar, quar_alloc_to_scrap ,quar_alloc_to_inv ,bulk_alloc_to_set, bulk_alloc_to_inv"
   * ); sbQuery.append(" ,inv_alloc_to_pack "); sbQuery.append(
   * " ,c205_qty_in_stock - ( NVL(inv_alloc_to_pack,0) + NVL(sales_allocated,0) + NVL(cons_alloc,0)+ NVL(quar_alloc_self_quar,0)+ NVL(bulk_alloc_to_set,0) "
   * ); sbQuery.append(" + NVL(loan_alloc,0) ) INSHELF "); sbQuery.append(
   * " ,(nvl(sales_allocated,0) + nvl(cons_alloc,0) + nvl(quar_alloc_self_quar,0) + nvl(bulk_alloc_to_set,0) + NVL(loan_alloc,0) ) SHELFALLOC "
   * ); sbQuery.append(
   * " ,( quar_alloc_to_scrap + quar_alloc_to_inv) QUARALLOC , bulk_alloc_to_inv BULKALLOC ");
   * 
   * sbQuery.append(
   * " ,rm_alloc_qty, mfg_tbr, mfg_release, powip, dhr_wip, dhr_pen_qc_ins, dhr_pen_qc_rel, dhr_qc_appr, build_set_qty"
   * );
   * 
   * // sbQuery.append(
   * " , 0 rm_alloc_qty, 0 mfg_tbr, 0 mfg_release, 0 dhr_wip, 0 dhr_pen_qc_ins, 0 dhr_pen_qc_rel, 0 dhr_qc_app"
   * ); sbQuery.append(
   * " ,( C205_QTY_IN_RETURNS_HOLD - ( NVL(quar_alloc_self_quar,0)+NVL(inv_alloc_to_pack,0))) RETURNAVAIL , "
   * ); sbQuery.append(" ( NVL(quar_alloc_self_quar,0)+NVL(inv_alloc_to_pack,0)) RETURNALLOC ");
   * 
   * sbQuery.append(" FROM t205_part_number t205, (SELECT  b.c205_part_number_id ");
   * sbQuery.append(" ,sum(b.c502_item_qty) sales_allocated ");
   * sbQuery.append(" FROM t501_order a ,t502_item_order b ");
   * sbQuery.append(" WHERE a.c501_order_id = b.c501_order_id");
   * sbQuery.append(" AND a.c501_status_fl < 3 AND a.c501_status_fl > 0 AND a.c501_void_fl IS NULL"
   * );
   * sbQuery.append(" AND NVL (a.c901_order_type, -9999) <> 2524 and a.C501_SHIPPING_DATE IS NULL "
   * ); sbQuery.append(" AND b.c901_type = 50300 "); sbQuery.append(" AND b.c502_item_qty > 0 ");
   * sbQuery.append(" GROUP BY b.c205_part_number_id ) SLS,"); sbQuery.append(
   * " (SELECT b.c205_part_number_id ,SUM(DECODE(SIGN(4113 - a.c504_type),1, b.c505_item_qty,0)) cons_alloc "
   * );
   * sbQuery.append(" ,SUM(DECODE(a.c504_type, 4113, b.c505_item_qty,0))  quar_alloc_self_quar ");
   * sbQuery.append(" ,SUM(DECODE(a.c504_type, 4114, b.c505_item_qty,0))  inv_alloc_to_pack ");
   * sbQuery.append(" ,SUM(DECODE(a.c504_type, 4115, b.c505_item_qty,0))  quar_alloc_to_scrap ");
   * sbQuery.append(" ,SUM(DECODE(a.c504_type, 4116, b.c505_item_qty,0))  quar_alloc_to_inv ");
   * sbQuery.append(" FROM t504_consignment a ,t505_item_consignment b");
   * sbQuery.append(" WHERE A.C504_CONSIGNMENT_ID = B.C504_CONSIGNMENT_ID AND a.c504_status_fl < 4"
   * ); sbQuery.append(
   * " AND a.c207_set_id IS NULL AND a.c504_type IN (4110,4111,4112,4113,4114,4115,4116) ");
   * sbQuery.append(" AND a.c504_void_fl IS NULL AND a.c504_ship_date IS NULL");
   * sbQuery.append(" GROUP BY b.c205_part_number_id ) CONS,");
   * 
   * // Bulk Allocation sbQuery.append(
   * " (SELECT  b.c205_part_number_id ,SUM( DECODE(a.c412_type,4117,c413_item_qty,0) ) bulk_alloc_to_set"
   * ); sbQuery.append(" ,SUM( DECODE(a.c412_type,4118,c413_item_qty,0) ) bulk_alloc_to_inv ");
   * sbQuery.append(" , NVL (SUM (DECODE (a.c412_type "); sbQuery.append(", 50150, c413_item_qty ");
   * sbQuery.append(", 50155, c413_item_qty "); sbQuery.append(", 50154, c413_item_qty ");
   * sbQuery.append(", 40054, c413_item_qty "); sbQuery.append(", 50160, c413_item_qty ");
   * sbQuery.append(", 0 "); sbQuery.append(") "); sbQuery.append(") "); sbQuery.append(", 0 ");
   * sbQuery.append(") loan_alloc "); sbQuery.append("  ,NVL (SUM (DECODE (a.c412_type ");
   * sbQuery.append("                     , 4118, c413_item_qty ");
   * sbQuery.append(", 50157, c413_item_qty "); sbQuery.append(", 50152, c413_item_qty");
   * sbQuery.append(", 40053, c413_item_qty"); sbQuery.append(", 50161, c413_item_qty");
   * sbQuery.append(", 0 "); sbQuery.append(")"); sbQuery.append(")"); sbQuery.append(", 0");
   * sbQuery.append(") part_alloc_to_inv");
   * 
   * sbQuery.append(" FROM t412_inhouse_transactions a ,t413_inhouse_trans_items b ");
   * sbQuery.append
   * (" WHERE a.c412_inhouse_trans_id = b.c412_inhouse_trans_id AND a.c412_verified_date IS NULL");
   * sbQuery.append(" AND a.c412_void_fl IS NULL "); sbQuery.append(
   * " AND a.c412_type IN (4117, 4118, 50150, 50155, 50154, 40054, 50160, 50161, 40053, 50152, 50157) "
   * ); sbQuery.append(" GROUP BY b.c205_part_number_id ) BLKTXN, ");
   * 
   * sbQuery.append(" (SELECT a.c205_part_number_id , SUM(NVL(a.c505_item_qty,0)) in_wip_set ");
   * sbQuery.append(" ,SUM (DECODE (a.C505_CONTROL_NUMBER, 'TBE', a.c505_item_qty, 0)) TBE_ALLOC ");
   * sbQuery.append(" FROM t505_item_consignment a ,t504_consignment b");
   * sbQuery.append(" WHERE a.c504_consignment_id = b.c504_consignment_id ");
   * sbQuery.append(" AND TRIM(a.c505_control_number) is not null AND b.c504_status_fl < 3 ");
   * sbQuery
   * .append(" AND b.c207_set_id IS NOT NULL AND b.c504_verify_fl = '0' AND b.c504_void_fl IS NULL "
   * ); sbQuery.append(" GROUP BY a.c205_part_number_id ) WIP ");
   * 
   * 
   * 
   * sbQuery.append(
   * " ,(SELECT   t408.c205_part_number_id, SUM (NVL (t408.c408_shipped_qty, 0) - NVL (t408.c408_qty_rejected, 0)+ NVL (t409.c409_closeout_qty, 0)) powip"
   * ); // sbQuery.append(
   * ", SUM (DECODE (t408.c408_status_fl, 0, (  NVL (t408.c408_shipped_qty, 0) - NVL (t408.c408_qty_rejected, 0)+ NVL (t409.c409_closeout_qty, 0)), 0)) dhr_wip"
   * ); sbQuery.append(
   * ", SUM (DECODE (t408.c408_status_fl, 0, (NVL (t408.c408_qty_received, 0) ), 0)) dhr_wip");
   * 
   * sbQuery.append(
   * ", SUM (DECODE (t408.c408_status_fl, 1, (  NVL (t408.c408_shipped_qty, 0) - NVL (t408.c408_qty_rejected, 0) + NVL (t409.c409_closeout_qty, 0)), 0 )) dhr_pen_qc_ins"
   * ); sbQuery.append(
   * ", SUM (DECODE (t408.c408_status_fl, 2, (  NVL (t408.c408_shipped_qty, 0) - NVL (t408.c408_qty_rejected, 0) + NVL (t409.c409_closeout_qty, 0)), 0)) dhr_pen_qc_rel"
   * ); sbQuery.append(
   * ", SUM (DECODE (t408.c408_status_fl, 3, (  NVL (t408.c408_shipped_qty, 0) - NVL (t408.c408_qty_rejected, 0) + NVL (t409.c409_closeout_qty, 0)), 0)) dhr_qc_appr"
   * ); sbQuery.append(" FROM t408_dhr t408, t409_ncmr t409");
   * sbQuery.append(" WHERE t408.c408_dhr_id = t409.c408_dhr_id(+) AND t408.c408_status_fl < 4");
   * sbQuery.append(" AND t408.c408_void_fl IS NULL");
   * sbQuery.append(" AND t409.c409_void_fl IS NULL");
   * sbQuery.append(" GROUP BY t408.c205_part_number_id) dhr");
   * 
   * sbQuery.append(
   * ", (SELECT   t304.c205_part_number_id, NVL (SUM (t304.c304_item_qty), 0) mfg_total_alloc");
   * sbQuery.append(
   * ", SUM (DECODE (t303.c303_status_fl, 0, t304.c304_item_qty, 1, t304.c304_item_qty, 0)) mfg_tbr"
   * );
   * sbQuery.append(", SUM (DECODE (t303.c303_status_fl, 2, t304.c304_item_qty, 0)) mfg_release");
   * sbQuery
   * .append(" FROM t303_material_request t303, t304_material_request_item t304, t408_dhr t408");
   * sbQuery.append(" WHERE t303.c303_material_request_id = t304.c303_material_request_id");
   * sbQuery.append(" AND t408.c408_dhr_id = t303.c408_dhr_id");
   * sbQuery.append(" AND t408.c408_status_fl = 0");
   * sbQuery.append(" GROUP BY t304.c205_part_number_id) mfgrun");
   * sbQuery.append(", (SELECT c205_part_number_id, NVL (c205_available_qty, 0) rm_avail");
   * sbQuery.append(" FROM t205c_part_qty");
   * sbQuery.append(" WHERE c901_transaction_type = 90802) rmavail");
   * 
   * sbQuery.append(
   * ", (SELECT   t413.c205_part_number_id, NVL (SUM (t413.c413_item_qty), 0) rm_alloc_qty");
   * sbQuery.append(" FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413");
   * sbQuery.append(" WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id");
   * sbQuery.append(" AND t412.c412_verified_date IS NULL");
   * sbQuery.append(" AND t412.c412_void_fl IS NULL");
   * sbQuery.append(" AND t412.c412_type IN (40054, 40051, 40052)");
   * sbQuery.append(" GROUP BY t413.c205_part_number_id) rmalloc");
   * 
   * sbQuery.append(" , (SELECT   b.c205_part_number_id, SUM (b.c505_item_qty) build_set_qty");
   * sbQuery.append(" FROM t504_consignment a, t505_item_consignment b");
   * sbQuery.append(" WHERE a.c504_consignment_id = b.c504_consignment_id");
   * sbQuery.append(" AND a.c504_status_fl IN (2, 3)");
   * sbQuery.append(" AND a.c207_set_id IS NOT NULL");
   * sbQuery.append(" AND a.c504_verify_fl = '1'"); sbQuery.append(" AND a.c504_ship_date IS NULL");
   * sbQuery.append(" AND a.c504_void_fl IS NULL");
   * sbQuery.append(" AND TRIM (b.c505_control_number) IS NOT NULL");
   * sbQuery.append(" GROUP BY b.c205_part_number_id) consbuilt");
   * 
   * 
   * sbQuery.append(" WHERE t205.C205_PART_NUMBER_ID IS NOT NULL "); if (!strPartNum.equals("")) {
   * sbQuery.append(" AND REGEXP_LIKE(t205.c205_part_number_id,'"); sbQuery.append(strPartNum);
   * sbQuery.append("')"); }
   * sbQuery.append(" AND t205.c205_part_number_id = sls.c205_part_number_id (+)");
   * sbQuery.append(" AND t205.c205_part_number_id = cons.c205_part_number_id (+)");
   * sbQuery.append(" AND t205.c205_part_number_id = blktxn.c205_part_number_id (+)");
   * sbQuery.append(" AND t205.c205_part_number_id = wip.c205_part_number_id (+)");
   * 
   * sbQuery.append(" AND t205.c205_part_number_id = dhr.c205_part_number_id(+)");
   * sbQuery.append(" AND t205.c205_part_number_id = mfgrun.c205_part_number_id(+)");
   * sbQuery.append(" AND t205.c205_part_number_id = rmavail.c205_part_number_id(+)");
   * sbQuery.append(" AND t205.c205_part_number_id = rmalloc.c205_part_number_id(+)");
   * sbQuery.append(" AND t205.c205_part_number_id = consbuilt.c205_part_number_id(+)");
   * 
   * if (!strSubComponentFlag.equals("Y")) {
   * sbQuery.append(" AND t205.c205_sub_component_fl IS NULL "); } // c901_status_id: 20369,
   * Obsolete. if (strEnableObsolete.equals("")){
   * sbQuery.append(" AND  t205.c901_status_id <> '20369' ") ; } if (!strProjId.equals("0")) {
   * sbQuery.append(" AND t205.C202_PROJECT_ID ='"); sbQuery.append(strProjId); sbQuery.append("'");
   * } // sbQuery.append(" AND INSTR(t205.C205_PART_NUMBER_ID, '.', 1, 2) = // 0 ");
   * sbQuery.append(" ORDER BY t205.c205_part_number_id "); log.debug(" Query for INventory Report "
   * + sbQuery.toString()); alResult = dbCon.queryMultipleRecords(sbQuery.toString());
   * 
   * } catch (Exception e) { String strTrace = GmCommonClass.getExceptionStackTrace(e, "\n");
   * GmLogError.log("Exception in GmInvReportBean:getPartNumInvReport", "Exception is:" + strTrace);
   * } return alResult; } // End of getPartNumInvReport
   */
  /**
   * getBuiltSetListReport - This method will
   * 
   * @param String strPartNumId
   * @return ArrayList
   * @exception AppError This method returns
   */
  public RowSetDynaClass getBuiltSetListReport() throws AppError {
    RowSetDynaClass resultSet = null;
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      String strCompPlantFilter = "";
      strCompPlantFilter =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "BUILT_SET_BY_SETS")); // BUILT_SET_BY_SETS-Rule
                                                                                                 // Value:=
                                                                                                 // plant.
      strCompPlantFilter = strCompPlantFilter.equals("") ? getCompId() : getCompPlantId();

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT c207_set_id SET_ID, get_set_name(c207_set_id) SET_NAME, c504_consignment_id CN_ID, to_number('1') CNT ");
      sbQuery.append(" , c504_verified_date VDATE, get_user_name(c504_verified_by) VERBY , get_tagid_from_txn_id(c504_consignment_id) TAG_ID ");
      sbQuery
          .append(" ,DECODE(C504_STATUS_FL,2,'Built Sets',3,'Shipping/Verification') STATUS,  get_code_name(C504_TYPE) TYPE ");
      sbQuery
          .append(" ,DECODE(C504_TYPE,4110,get_distributor_name(C701_DISTRIBUTOR_ID),4112,get_code_name(C504_INHOUSE_PURPOSE)) PURPOSE ");
      sbQuery.append(" FROM t504_consignment ");
      sbQuery.append(" WHERE  c504_status_fl IN (2,3) AND c207_set_id IS NOT NULL ");
      sbQuery.append(" AND c504_verify_fl = 1 AND c504_void_fl IS NULL");
      // Fetch records based company/plant for EDC countries
      sbQuery.append(" AND ( C1900_COMPANY_ID = " + strCompPlantFilter);
      sbQuery.append(" OR C5040_PLANT_ID = " + strCompPlantFilter);
      sbQuery.append(") ");
      sbQuery.append(" ORDER BY c207_set_id");

      resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    } catch (Exception e) {
      GmLogError.log("Exception in GmInvReportBean:getBuiltSetListReport", "Exception is:" + e);
    }
    return resultSet;
  } // End of getBuiltSetListReport

  /**
   * getBuiltSetPartsReport - This method will
   * 
   * @param String strPartNumId
   * @return ArrayList
   * @exception AppError This method returns
   */

  public ArrayList getBuiltSetPartsReport(HashMap hmQueryParams) throws AppError {
    ArrayList alResult = new ArrayList();

    try {

      final String partNumRegexp =
          GmCommonClass.parseNull((String) hmQueryParams.get("REGEXPATTERN"));
      final String strProjId = GmCommonClass.parseNull((String) hmQueryParams.get("PROJID"));

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      String strCompPlantFilter = "";
      strCompPlantFilter =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "BUILT_SET_BY_PARTS")); // BUILT_SET_BY_PARTS-Rule
                                                                                                  // Value:=
                                                                                                  // plant.
      strCompPlantFilter = strCompPlantFilter.equals("") ? getCompId() : getCompPlantId();

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT a.c504_consignment_id CN_ID, a.c207_set_id SET_ID, GET_SET_NAME(a.c207_set_id) SET_NAME ");
      sbQuery
          .append(" ,b.c205_part_number_id PNUM, SUM(b.c505_item_qty) QTY , get_partnum_desc(B.C205_PART_NUMBER_ID) PDESC ");
      sbQuery
          .append(" ,get_tagid_from_txn_id(a.c504_consignment_id) TAG_ID  ");
      sbQuery.append(" FROM t504_consignment a, t505_item_consignment b ");

      if (!strProjId.equals("0"))
        sbQuery.append("  , t205_part_number  t205  ");

      sbQuery.append(" WHERE a.c504_consignment_id = b.c504_consignment_id ");
      sbQuery
          .append(" AND a.c504_status_fl IN (2,3) AND a.c207_set_id IS NOT NULL AND a.c504_verify_fl = 1 AND c504_void_fl IS NULL");
      // Fetch records based company/plant for EDC countries
      sbQuery.append(" AND ( a.C1900_COMPANY_ID = " + strCompPlantFilter);
      sbQuery.append(" OR a.C5040_PLANT_ID = " + strCompPlantFilter);
      sbQuery.append(") ");
      // sbQuery.append(" AND regexp_like (b.c205_part_number_id, NVL (
      // '"+ partNumRegexp +"', b.c205_part_number_id) )" );
      if (!partNumRegexp.equals(""))
       
    	  sbQuery.append(" AND regexp_like (b.c205_part_number_id,REGEXP_REPLACE('" + partNumRegexp + "','[+]','\\+'))");

      if (!strProjId.equals("0")) {
        sbQuery.append(" AND  t205.c205_part_number_id = b.c205_part_number_id ");
        sbQuery.append(" AND  t205.c202_project_id = '" + strProjId + "' ");
      }

      sbQuery.append(" GROUP BY a.c504_consignment_id, a.c207_set_id, b.c205_part_number_id ");
      sbQuery.append(" ORDER BY a.c207_set_id, a.c504_consignment_id, b.c205_part_number_id ");
      log.debug("Query:" + sbQuery.toString());
      alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
    } catch (Exception e) {
      GmLogError.log("Exception in GmInvReportBean:getBuiltSetPartsReport", "Exception is:" + e);
    }
    return alResult;
  } // End of getBuiltSetPartsReport

  /**
   * getReworkPartsListReport - This method will
   * 
   * @return ArrayList
   * @exception AppError This method returns
   */
  public RowSetDynaClass getReworkPartsListReport(HashMap hmQueryParams) throws AppError {
    RowSetDynaClass resultSet = null;
    try {
      final String strPartNumRegexp =
          GmCommonClass.parseNull((String) hmQueryParams.get("REGEXPATTERN"));
      final String strProjId = GmCommonClass.parseNull((String) hmQueryParams.get("PROJID"));

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT t402.c205_part_number_id PNUM, get_partnum_desc(t402.c205_part_number_id) PDESC, ");
      sbQuery
          .append(" SUM(GET_WO_PEND_QTY(t402.c402_work_order_id, t402.c402_qty_ordered)) PEND_CNT , GET_VENDOR_NAME(t401.C301_VENDOR_ID)  VENDOR_NAME, t401.c401_purchase_ord_id PURCHASE_ID, t402.c402_work_order_id  WORK_ID ");
      sbQuery.append(" FROM t401_purchase_order t401, t402_work_order t402 ");

      if (!strProjId.equals("0"))
        sbQuery.append("  , t205_part_number  t205  ");

      sbQuery.append(" WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id ");
      sbQuery.append(" AND t401.c401_type = 3101 AND t401.c401_void_fl IS NULL ");
      sbQuery.append(" AND t402.c402_void_fl IS NULL AND t402.c402_status_fl < 3 ");

      if (!strPartNumRegexp.equals(""))
       
    	  sbQuery
          .append(" AND regexp_like ( t402.c205_part_number_id,REGEXP_REPLACE('" + strPartNumRegexp + "','[+]','\\+'))");

      if (!strProjId.equals("0")) {
        sbQuery.append(" AND  t205.c205_part_number_id = t402.c205_part_number_id ");
        sbQuery.append(" AND  t205.c202_project_id = '" + strProjId + "' ");
      }
      sbQuery.append(" AND  t401.C1900_COMPANY_ID = t402.C1900_COMPANY_ID");
      sbQuery.append(" AND  t401.C1900_COMPANY_ID =" + getCompId());
      sbQuery
          .append(" GROUP BY t402.c205_part_number_id ,  GET_VENDOR_NAME(t401.C301_VENDOR_ID), t401.c401_purchase_ord_id , t402.c402_work_order_id ");
      sbQuery.append(" ORDER BY t402.c205_part_number_id ");

      log.debug("##### Rework query : " + sbQuery.toString());

      resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    } catch (Exception e) {
      GmLogError.log("Exception in GmInvReportBean:getReworkPartsListReport", "Exception is:" + e);
    }
    return resultSet;
  } // End of getReworkPartsListReport

  /**
   * getQuarantineListReport - This method will
   * 
   * @return ArrayList
   * @exception AppError This method returns
   */

  public RowSetDynaClass getQuarantineListReport(HashMap hmQueryParams) throws AppError {
    RowSetDynaClass resultSet = null;
    try {

      final String strPartNumRegexp =
          GmCommonClass.parseNull((String) hmQueryParams.get("REGEXPATTERN"));
      final String strProjId = GmCommonClass.parseNull((String) hmQueryParams.get("PROJID"));

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      // sbQuery.append("select * from ( ");
      sbQuery
          .append(" SELECT t205.c205_part_number_id PNUM, t205.c205_part_num_desc PDESC, t202.c202_project_nm PROJID ");
      sbQuery.append(" ,NVL (c205_available_qty, 0) QTY ");
      sbQuery.append(" FROM t205_part_number t205, t205c_part_qty t205c , t202_project t202 ");
      sbQuery.append(" WHERE t205.c205_part_number_id IS NOT NULL ");
      sbQuery.append(" AND t205c.c205_available_qty > 0 ");
      sbQuery.append(" AND t205.C205_PART_NUMBER_ID = t205c.c205_part_number_id ");
      sbQuery.append(" AND t205c.c901_transaction_type = 90813 ");// 90813:Quarantine Inventory
      sbQuery.append(" AND t205c.C5040_PLANT_ID =  " + getCompPlantId());
      sbQuery.append(" AND T202.C202_PROJECT_ID = t205.c202_project_id  ");

      if (!strPartNumRegexp.equals(""))
         
    	  sbQuery
          .append(" AND regexp_like ( t205.c205_part_number_id, REGEXP_REPLACE('" + strPartNumRegexp + "','[+]','\\+'))");

      if (!strProjId.equals("0"))
        sbQuery.append(" AND t205.c202_project_id = '" + strProjId + "' ");

      // sbQuery.append(" ) WHERE QTY  > 0 ");

      sbQuery.append(" ORDER BY  t205.c205_part_number_id, t202.c202_project_nm ");

      log.debug("Quarantine query : " + sbQuery.toString());

      resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    } catch (Exception e) {
      GmLogError.log("Exception in GmInvReportBean:getQuarantineListReport", "Exception is:" + e);
    }
    return resultSet;
  } // End of getQuarantineListReport

  /**
   * getReturnsHoldListReport - This method will
   * 
   * @return ArrayList
   * @exception AppError This method returns
   */
  public RowSetDynaClass getReturnsHoldListReport(HashMap hmQueryParams) throws AppError {
    RowSetDynaClass resultSet = null;
    try {

      final String strPartNumRegexp =
          GmCommonClass.parseNull((String) hmQueryParams.get("REGEXPATTERN"));
      final String strProjId = GmCommonClass.parseNull((String) hmQueryParams.get("PROJID"));

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery.append("select * from ( ");
      sbQuery
          .append(" SELECT t205.c205_part_number_id PNUM, t205.c205_part_num_desc PDESC, get_project_name(t205.c202_project_id) PROJID ");
      sbQuery.append(" ,get_partnumber_qty (t205.c205_part_number_id, 90812) QTY ");
      sbQuery.append(" FROM t205_part_number t205 WHERE t205.c205_part_number_id IS NOT NULL");

      if (!strPartNumRegexp.equals(""))
       
    	  sbQuery
          .append(" AND regexp_like ( t205.c205_part_number_id, REGEXP_REPLACE('" + strPartNumRegexp + "','[+]','\\+'))");

      if (!strProjId.equals("0"))
        sbQuery.append(" AND t205.c202_project_id = '" + strProjId + "' ");

      sbQuery.append(" ) WHERE QTY  > 0 ");
      sbQuery.append(" ORDER BY  PROJID, PNUM ");

      resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    } catch (Exception e) {
      GmLogError.log("Exception in GmInvReportBean:getReturnsHoldListReport", "Exception is:" + e);
    }
    return resultSet;
  } // End of getReturnsHoldListReport

  /**
   * getInHouseListReport - This method will
   * 
   * @return ArrayList
   * @exception AppError This method returns
   */
  public RowSetDynaClass getLoanerSetsListReport(String strType) throws AppError {
    RowSetDynaClass resultSet = null;

    String strFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "LOANER_SET_LIST"));/*
                                                                                             * LOANER_SET_LIST
                                                                                             * - To
                                                                                             * fetch
                                                                                             * the
                                                                                             * records
                                                                                             * for
                                                                                             * EDC
                                                                                             * entity
                                                                                             * countries
                                                                                             * based
                                                                                             * on
                                                                                             * plant
                                                                                             */
    strFilter = strFilter.equals("") ? getCompId() : getCompPlantId();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT t504.c504_consignment_id CONID ,t504.c207_set_id SETID ,get_set_name(t504.c207_set_id) SNAME ,t504a.c504a_etch_id ETCHID ");
    sbQuery.append(" ,gm_pkg_op_loaner.get_loaner_status(T504A.C504A_STATUS_FL)  LOANSFL,get_tagid_from_txn_id(t504.c504_consignment_id) TAG_ID ");
    sbQuery.append(" FROM t504_consignment t504, t504a_consignment_loaner t504a ");
    sbQuery.append(" WHERE t504.c504_status_fl = '4' AND t504.c504_verify_fl = '1' ");
    sbQuery.append(" AND t504.c207_set_id IS NOT NULL AND t504.c504_type =");
    sbQuery.append(strType);
    sbQuery
        .append(" AND c504_void_fl IS NULL AND t504.c504_consignment_id = t504a.c504_consignment_id (+) ");
    sbQuery.append(" AND T504A.c504a_status_fl !=60");// inactive
    sbQuery.append(" AND ( T504.C1900_company_id = " + strFilter);
    sbQuery.append(" OR T504.C5040_plant_id = " + strFilter);
    sbQuery.append(") ");
    sbQuery.append(" ORDER BY setid , CONID DESC ");
    log.debug("getLoanerSetsListReport query : " + sbQuery.toString());
    resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());

    return resultSet;
  } // End of getReturnsHoldListReport

  /**
   * getLoanerPartsListReport - This method will
   * 
   * @return ArrayList
   * @exception AppError This method returns
   */

  public RowSetDynaClass getLoanerPartsListReport(HashMap hmQueryParams) throws AppError {
    RowSetDynaClass resultSet = null;
    //PC-4148 Remove Try Catch for Email Exception(TSK-23804)

      final String strType = GmCommonClass.parseNull((String) hmQueryParams.get("TYPE"));
      final String strPartNumRegexp =
          GmCommonClass.parseNull((String) hmQueryParams.get("REGEXPATTERN"));
      final String strProjId = GmCommonClass.parseNull((String) hmQueryParams.get("PROJID"));

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT t504.c504_consignment_id CONID ,t504.c207_set_id SETID ,get_set_name(t504.c207_set_id) SNAME , t504a.c504a_etch_id ETCHID ");
      // sbQuery.append("
      // ,decode(t504a.c504a_status_fl,0,'Available',1,'Pending
      // Shipping',2,'Pending Return',3,'WIP-Loaners',4,'Pending
      // Verification') LOANSFL ");
      sbQuery
          .append(" ,t505.C205_PART_NUMBER_ID PNUM,(sum(t505.C505_ITEM_QTY) - NVL(miss_qty,0)) QTY , get_tagid_from_txn_id(t504.c504_consignment_id) TAG_ID ");
      sbQuery
          .append(" FROM t504_consignment t504, t504a_consignment_loaner t504a, t505_item_consignment t505 ");

      if (!strProjId.equals("0")) {
        sbQuery.append("  , t205_part_number  t205  ");
      }
      // **************************************
      // Table for fetching inhouse missing part
      // **************************************
      sbQuery.append(" , (SELECT   t412.c412_ref_id CNID, t413.c205_part_number_id, ");
      sbQuery.append("          SUM (t413.c413_item_qty) miss_qty ");
      sbQuery.append(" FROM t412_inhouse_transactions t412, ");
      sbQuery.append(" t413_inhouse_trans_items t413 ");
      if (!strProjId.equals("0")) {
        sbQuery.append("  , t205_part_number  t205  ");
      }
      sbQuery.append(" WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id ");
      sbQuery.append(" AND t412.c412_void_fl IS NULL ");
      sbQuery.append(" AND t413.c413_void_fl IS NULL ");
      sbQuery.append(" AND t413.c413_status_fl = 'Q' ");
      sbQuery.append(" AND t412.c5040_plant_id = '" + getCompPlantId() + "'");

      if (strType.equals("4127")) {
        sbQuery.append(" AND t412.c412_type = 50159 ");
      } else {
        sbQuery.append(" AND t412.c412_type = 1006571 ");
      }

      if (!strPartNumRegexp.equals("")) {
        
    	  sbQuery
          .append(" AND regexp_like ( t413.c205_part_number_id,REGEXP_REPLACE('" + strPartNumRegexp + "','[+]','\\+'))");
      }
      if (!strProjId.equals("0")) {
        sbQuery.append(" AND  t205.c205_part_number_id = t413.c205_part_number_id ");
        sbQuery.append(" AND  t205.c202_project_id = '" + strProjId + "' ");
      }
      sbQuery.append(" GROUP BY t412.c412_ref_id , t413.c205_part_number_id) miss ");
      // **************************************
      // Table for missing parts end
      // **************************************
      sbQuery.append(" WHERE t504.c504_status_fl = 4 AND t504.c504_verify_fl = 1 ");
      sbQuery.append(" AND t504.c207_set_id IS NOT NULL AND t504.c504_type =");
      sbQuery.append(strType);
      sbQuery.append(" AND t504.c504_consignment_id = t504a.c504_consignment_id (+) ");
      sbQuery
          .append(" AND t504.C504_CONSIGNMENT_ID = t505.C504_CONSIGNMENT_ID and trim(t505.C505_CONTROL_NUMBER) IS NOT NULL ");
      sbQuery.append(" and t504.C504_VOID_FL IS NULL and t505.C505_VOID_FL IS NULL");
      sbQuery.append(" AND T504A.c504a_status_fl !=60");// inactive
      sbQuery.append(" AND t505.c205_part_number_id = miss.c205_part_number_id (+) ");
      sbQuery.append(" AND t505.c504_consignment_id = miss.CNID (+) ");
      sbQuery.append(" AND t504a.c5040_plant_id = '" + getCompPlantId() + "'");


      if (!strPartNumRegexp.equals("")) {
    	  
    	  sbQuery
          .append(" AND regexp_like ( t505.c205_part_number_id, REGEXP_REPLACE('" + strPartNumRegexp + "','[+]','\\+'))");
      }
      if (!strProjId.equals("0")) {
        sbQuery.append(" AND  t205.c205_part_number_id = t505.c205_part_number_id ");
        sbQuery.append(" AND  t205.c202_project_id = '" + strProjId + "' ");
      }

      sbQuery
          .append(" group by t504.c504_consignment_id, t504.c207_set_id, t504a.c504a_etch_id, t505.C205_PART_NUMBER_ID, miss_qty ");
      sbQuery.append(" ORDER BY setid , CONID DESC ");
      log.debug("Query......" + sbQuery.toString());
      resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    
    return resultSet;
  } // End of getLoanerPartsListReport

  /**
   * getReturnsListReport - This method will
   * 
   * @return ArrayList
   * @exception AppError This method returns
   */
  public RowSetDynaClass getReturnsListReport() throws AppError {
    RowSetDynaClass resultSet = null;
    try {


      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT	C506_RMA_ID RAID, GET_CODE_NAME(C506_TYPE) TYPE, C506_CREDIT_DATE CDATE ");
      sbQuery.append(" FROM T506_RETURNS ");
      sbQuery.append(" WHERE C506_STATUS_FL = 2 AND C501_REPROCESS_DATE IS NULL ");
      sbQuery.append(" AND  C506_VOID_FL IS NULL ");
      sbQuery.append(" AND  C5040_PLANT_ID =" + getCompPlantId());
      sbQuery
          .append(" AND 0 < (SELECT SUM (c507_item_qty) FROM t507_returns_item t507 WHERE t507.c506_rma_id = T506_RETURNS.c506_rma_id) ");
      sbQuery.append(" ORDER BY C506_TYPE,C506_RMA_ID DESC ");


      resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
      log.debug("sbQuery.toString():  " + sbQuery.toString());
    } catch (Exception e) {
      GmLogError.log("Exception in GmInvReportBean:getReturnsListReport", "Exception is:" + e);
    }
    return resultSet;
  } // End of getReturnsListReport

  /**
   * getReturnsPartsListReport - This method will
   * 
   * @return ArrayList
   * @exception AppError This method returns
   */
  public RowSetDynaClass getReturnsPartsListReport(HashMap hmQueryParams) throws AppError {
    RowSetDynaClass resultSet = null;
    try {

      final String strPartNumRegexp =
          GmCommonClass.parseNull((String) hmQueryParams.get("REGEXPATTERN"));
      final String strProjId = GmCommonClass.parseNull((String) hmQueryParams.get("PROJID"));

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT	t507.C506_RMA_ID RAID, GET_CODE_NAME(C506_TYPE) TYPE, C506_CREDIT_DATE CDATE, ");
      sbQuery.append(" t507.C205_PART_NUMBER_ID PNUM, sum(t507.C507_ITEM_QTY) QTY");
      sbQuery.append(" FROM T506_RETURNS t506, t507_returns_item t507 ");

      if (!strProjId.equals("0"))
        sbQuery.append("  , t205_part_number  t205  ");

      sbQuery.append(" WHERE C506_STATUS_FL = 2 AND C501_REPROCESS_DATE IS NULL ");
      sbQuery.append(" AND  C506_VOID_FL IS NULL ");
      sbQuery.append(" AND t506.C506_RMA_ID = t507.C506_RMA_ID ");
      sbQuery.append(" AND trim(t507.C507_CONTROL_NUMBER) IS NOT NULL ");
      sbQuery.append(" AND t507.C507_STATUS_FL = 'C' ");
      sbQuery.append(" AND t506.C5040_PLANT_ID =" + getCompPlantId());

      if (!strPartNumRegexp.equals(""))
        
    	  sbQuery
          .append(" AND regexp_like ( t507.c205_part_number_id,REGEXP_REPLACE('" + strPartNumRegexp + "','[+]','\\+'))");

      if (!strProjId.equals("0")) {
        sbQuery.append(" AND  t205.c205_part_number_id = t507.c205_part_number_id ");
        sbQuery.append(" AND  t205.c202_project_id = '" + strProjId + "' ");
      }

      sbQuery
          .append(" GROUP BY t507.C506_RMA_ID , t506.C506_TYPE, t506.C506_CREDIT_DATE, t507.C205_PART_NUMBER_ID ");
      sbQuery.append(" ORDER BY C506_TYPE,t507.C506_RMA_ID , t507.C205_PART_NUMBER_ID DESC ");

      resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    } catch (Exception e) {
      GmLogError.log("Exception in GmInvReportBean:getReturnsPartsListReport", "Exception is:" + e);
    }
    return resultSet;
  } // End of getReturnsPartsListReport

  /**
   * loadInvLockReportLists - This method fetches the lists necessary for Inventory Lock Reports
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadInvLockReportLists(String strType) throws AppError {

    HashMap hmResult = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ResultSet rsResult = null;
    ArrayList alReturn = new ArrayList();

    gmDBManager.setPrepareString("gm_pkg_op_inventory.gm_fc_fch_invtypes", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    rsResult = (ResultSet) gmDBManager.getObject(1);
    alReturn = gmDBManager.returnArrayList(rsResult);
    hmResult.put("INVTYPES", alReturn);

    gmDBManager.setPrepareString("gm_pkg_op_inventory.gm_fc_fch_invlockids", 2);
    gmDBManager.setString(1, strType);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    rsResult = (ResultSet) gmDBManager.getObject(2);
    alReturn = gmDBManager.returnArrayList(rsResult);
    hmResult.put("LOCKLIST", alReturn);

    gmDBManager.close();

    return hmResult;
  } // End of loadInvLockReportLists

  /**
   * getInvLockReport - This Method is used to get data for the Inv Crosstab Report
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap getInvLockReport(String strLockId, String strTypes, String strPartNumFormat,
      String strProjId) throws AppError {
    HashMap hmapFinalValue;
    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ResultSet rsHeader = null;
    ResultSet rsData = null;

    try {
      gmDBManager.setPrepareString("gm_pkg_op_inventory.gm_fc_fch_invheader", 2);
      gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
      gmDBManager.setString(1, strTypes);
      gmDBManager.execute();
      rsHeader = (ResultSet) gmDBManager.getObject(2);

      strProjId = strProjId.equals("0") ? "" : strProjId;

      gmDBManager.setPrepareString("gm_pkg_op_inventory.gm_fc_fch_invdetails", 4);
      gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
      gmDBManager.setInt(1, Integer.parseInt(strLockId));
      gmDBManager.setString(2, strPartNumFormat);
      gmDBManager.setString(3, strProjId);
      gmDBManager.execute();
      rsData = (ResultSet) gmDBManager.getObject(4);

      hmapFinalValue = gmCrossReport.GenerateCrossTabReport(rsHeader, rsData);
      gmDBManager.close();
    }

    catch (Exception exp) {
      gmDBManager.close();
      throw new AppError(exp);
    }
    return hmapFinalValue;

  }// End of getInvLockReport

  /**
   * this function calls the respective function and returns the qty for the given partnumber
   * 
   * @param strPartNumber
   * @return
   * @throws AppError
   */
  public int getQtyForPart(String strPartNumber, String strType) throws AppError {
    int intQty = 0;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_op_inventory.get_qty_forpart", 2);
    gmDBManager.registerOutParameter(1, java.sql.Types.INTEGER);
    gmDBManager.setString(2, strPartNumber);
    gmDBManager.setInt(3, Integer.parseInt(strType));
    gmDBManager.execute();
    intQty = gmDBManager.getInt(1);
    log.debug("qty forpart fetched is  " + intQty);
    gmDBManager.close();

    return intQty;
  } // End of getQtyInstock

  /**
   * getInventoryBuckets - This method fetches the InventoryBuckets
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap getInventoryBuckets(HashMap hmParam) throws AppError {
    String strInvViewType = "";
    String strIncludeInvCodeId = "";
    String strExcludedInvCodeId = "";
    String strIncludedInvColumns = "";
    String strExcludedInvColumns = "";
    HashMap hmResult = new HashMap();
    GmCommonClass gmCommonClass = new GmCommonClass();
    ArrayList alDefaultInvColumns = new ArrayList();
    ArrayList alExcludedInvColumns = new ArrayList();
    strInvViewType = GmCommonClass.parseNull((String) hmParam.get("INVTYPE"));
    alDefaultInvColumns =
        GmCommonClass.parseNullArrayList(((ArrayList) hmParam.get("DEFAULTCOLS")));
    for (int i = 0; i < alDefaultInvColumns.size(); i++) {
      if (!strInvViewType.equals("")) {
        HashMap hmIncludedInv = (HashMap) alDefaultInvColumns.get(i);
        strIncludeInvCodeId = (String) hmIncludedInv.get("CODEID");
      } else {
        strIncludeInvCodeId = (String) alDefaultInvColumns.get(i);
      }
      strIncludedInvColumns += strIncludeInvCodeId + ",";
    }

    if (!strInvViewType.equals("")) {
      strIncludedInvColumns =
          strIncludedInvColumns.substring(0, strIncludedInvColumns.length() - 1);
    } else {
      strIncludedInvColumns = strIncludedInvColumns.substring(1);
      strIncludedInvColumns =
          strIncludedInvColumns.substring(0, strIncludedInvColumns.length() - 1);
    }
    alExcludedInvColumns = gmCommonClass.getCodeListNotIn("MANINV", strIncludedInvColumns);
    for (int j = 0; j < alExcludedInvColumns.size(); j++) {
      HashMap hmExcludedInv = (HashMap) alExcludedInvColumns.get(j);
      strExcludedInvCodeId = (String) hmExcludedInv.get("CODEID");
      strExcludedInvColumns += strExcludedInvCodeId + ",";
    }
    hmResult.put("INCLUDEINVENTORY", strIncludedInvColumns);
    hmResult.put("EXCLUDEDINVNTORY", strExcludedInvColumns);
    return hmResult;
  } // End of getInventoryBuckets

  /**
   * getInventoryBuckets - This method fetches the Excluded Inventory Buckets Mapping
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap fetchInventoryExcludemapping(HashMap hmParam) throws AppError {
    String strIncludedInvColumns = "";
    String strExcludedInvColumns = "";
    String strExcludedInvMapping = "";
    String strExcludedInvMappID = "";
    strIncludedInvColumns = GmCommonClass.parseNull((String) hmParam.get("INCLUDEINVENTORY"));
    strExcludedInvColumns = GmCommonClass.parseNull((String) hmParam.get("EXCLUDEINVENTORY"));
    strExcludedInvColumns = strExcludedInvColumns.substring(0, strExcludedInvColumns.length() - 1);
    HashMap hmResult = new HashMap();
    ArrayList alExcludedInvMapping = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_inv_warehouse.gm_fch_InvExclude_Map", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strIncludedInvColumns);
    gmDBManager.setString(2, strExcludedInvColumns);
    gmDBManager.execute();
    alExcludedInvMapping = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    for (int j = 0; j < alExcludedInvMapping.size(); j++) {
      HashMap hmExcludedInvMap = (HashMap) alExcludedInvMapping.get(j);
      strExcludedInvMappID = (String) hmExcludedInvMap.get("EXCLUDEMAPPING");
      strExcludedInvMapping += strExcludedInvMappID + ",";
    }
    gmDBManager.close();
    hmResult.put("EXCLUDEDINVMAPPING", strExcludedInvMapping);
    return hmResult;
  } // End of fetchInventoryExcludemapping

/**
   * getXmlGridData - This method used to show Transactions log Report
   * 
   * @parameter HashMap
   * @return String
   * @exception AppError
   */
public String getXmlGridData(HashMap hmParam) throws AppError {

		    GmTemplateUtil templateUtil = new GmTemplateUtil();
		    String strSessCompanyLocale =
		        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
		    templateUtil.setDataList("alReturn", (ArrayList) hmParam.get("REPORT"));
		    templateUtil.setDataMap("hmParam", hmParam);
		    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
		        "properties.labels.operations.GmTransactionLog", strSessCompanyLocale));
		    templateUtil.setTemplateName("GmTranLogReport.vm");
		    templateUtil.setTemplateSubDir("operations/templates");
		 
		    return templateUtil.generateOutput();
 } 

public String getSetName(String strSetNm) throws AppError {
	String strSetName = "";
	GmDBManager gmDBManager = new GmDBManager();
	gmDBManager.setFunctionString("GET_SET_NAME", 1);
	gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
	gmDBManager.setString(2, strSetNm);
    gmDBManager.execute();
    strSetName = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
	return strSetName;
}


}// end of class GmInvReportBean
