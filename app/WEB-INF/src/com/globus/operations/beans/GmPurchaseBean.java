/*
 * Module: GmPurchaseBean.java Author: Dhinakaran James Project: Globus Medical App Date-Written: 27
 * Jun 2005 Security: Unclassified Description: Operations - Purchasing Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.operations.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.prodmgmnt.beans.GmPartNumSearchBean;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmPurchaseBean extends GmBean {


  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  GmCommonClass gmCommon = new GmCommonClass();

  public GmPurchaseBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmPurchaseBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * getVendorList - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * 
   * @return HashMap
   * @exception AppError
   **/
  public HashMap getVendorList(String strType) throws AppError {
    ArrayList alReturn = new ArrayList();
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT C301_VENDOR_ID ID,  C301_VENDOR_NAME NAME ,");
    sbQuery.append(" GET_CODE_NAME(GET_VENDOR_CURRENCY (C301_VENDOR_ID)) VENDCURR ");
    sbQuery.append(" FROM T301_VENDOR");
    sbQuery.append(" WHERE c1900_company_id =" + getCompId());
    if (!strType.equals("")) {
      sbQuery.append(" AND C301_VENDOR_TYPE ='");
      sbQuery.append(strType);
      sbQuery.append("'");
    }
    sbQuery.append(" ORDER BY C301_VENDOR_NAME");
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    hmReturn.put("VENDORLIST", alReturn);

    return hmReturn;
  } // End of getVendorList


  /**
   * loadCart - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadCart(String strPOId, String strVendId, String strPartNums) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    String strPartNum = "";
    StringTokenizer strTok = new StringTokenizer(strPartNums, ",");
    StringBuffer sbQuery = new StringBuffer();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    if (strPOId.equals("")) {
      sbQuery.setLength(0);
      sbQuery.append(" SELECT S401_PURCHASE.NEXTVAL FROM dual ");
      hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
      strPOId = (String) hmReturn.get("NEXTVAL");
      if (strPOId.length() == 1) {
        strPOId = "GM-PO-0" + strPOId;
      } else {
        strPOId = "GM-PO-" + strPOId;
      }
      hmResult.put("POID", strPOId);
    }

    while (strTok.hasMoreTokens()) {
      strPartNum = strTok.nextToken();
      sbQuery.setLength(0);
      sbQuery.append(" SELECT C205_PART_NUMBER_ID PID, C405_COST_PRICE PRICE, ");
      sbQuery.append(" GET_PARTNUM_DESC(C205_PART_NUMBER_ID) PDESC ");
      sbQuery.append(" FROM T405_VENDOR_PRICING_DETAILS ");
      sbQuery.append(" WHERE C301_VENDOR_ID = '");
      sbQuery.append(strVendId);
      sbQuery.append("' AND C205_PART_NUMBER_ID ='");
      sbQuery.append(strPartNum);
      sbQuery.append("'");
      sbQuery.append(" AND C405_VOID_FL IS NULL ");

      hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
      hmResult.put(strPartNum, hmReturn);
    }


    return hmResult;
  } // End of loadCart


  /**
   * getPartVendorPriceList - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap getPartVendorPriceList(String strProjId, String strPartNums, String strVendorId,
      String strQtyToOrderFlag) throws AppError {

    ArrayList alReturn = new ArrayList();
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmPartNumSearchBean gmPartSearchBean = new GmPartNumSearchBean(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT A.C205_PART_NUMBER_ID PNUM, A.C205_PART_NUM_DESC PDESC, ");
    sbQuery
        .append(" GET_VENDOR_SH_NAME(B.C301_VENDOR_ID) VNAME ,GET_CODE_NAME(t301.c301_currency) VENDCURR, B.C405_COST_PRICE CPRICE, A.C205_REV_NUM DRAWREV, ");
    sbQuery.append(" B.C405_PRICE_VALID_DATE VDATE, B.C405_QTY_QUOTED QTYQ, B.C301_VENDOR_ID VID ");
    sbQuery
        .append(" ,A.C205_PART_DRAWING DRAWNUM, GET_CODE_NAME(B.C901_UOM_ID) UOM, B.C405_UOM_QTY UOMQTY, C405_PRICING_ID    PID");
    sbQuery
        .append(" ,C.C205_AVAILABLE_QTY AQTY , A.C901_STATUS_ID SID, get_code_name(A.C901_STATUS_ID) STATUS,GET_VALIDATED_STATUS (A.C205_PART_NUMBER_ID, B.C301_VENDOR_ID) VALIDFL,");
    sbQuery
    .append(" GET_VERIFIED_STATUS (A.C205_PART_NUMBER_ID) VERIFL ");
    
    sbQuery
    .append(" , DECODE(t205d.C205D_ATTRIBUTE_VALUE,'103360','Yes','No') WO_DCO , NVL(t205d.C205D_ATTRIBUTE_VALUE ,103361) WO_DCO_ID   ");
    sbQuery
        .append(" FROM T205_PART_NUMBER A, T405_VENDOR_PRICING_DETAILS B, T205C_PART_QTY C, T2023_PART_COMPANY_MAPPING T2023  ");
    sbQuery
    .append(" , T205D_PART_ATTRIBUTE t205d,T301_VENDOR t301 ");
    sbQuery
        .append(" WHERE A.C205_PART_NUMBER_ID = B.C205_PART_NUMBER_ID(+) AND B.C405_VOID_FL IS NULL ");
    
    sbQuery
    	.append(" AND  A.C205_PART_NUMBER_ID = t205d.C205_PART_NUMBER_ID(+) ");
	sbQuery
		.append(" AND t205d.C205D_VOID_FL(+) is NULL ");
	sbQuery
		.append(" and t205d.C901_ATTRIBUTE_TYPE(+) = 103380 ");
	sbQuery
		.append(" AND B.C301_VENDOR_ID = t301.C301_VENDOR_ID ");
    
    sbQuery.append(" AND A.C202_PROJECT_ID = NVL('");
    sbQuery.append(strProjId);
    sbQuery.append("',A.C202_PROJECT_ID) AND A.C205_ACTIVE_FL = 'Y' ");
    sbQuery.append("  AND B.C1900_COMPANY_ID =" + getCompId());
    sbQuery.append(" AND A.C205_PART_NUMBER_ID = T2023.C205_PART_NUMBER_ID ");
    sbQuery.append(" AND T2023.C1900_COMPANY_ID = B.C1900_COMPANY_ID");
    sbQuery.append(" AND T2023.C2023_VOID_FL IS NULL ");
    if (!strPartNums.equals("")) {
 
    sbQuery.append(" AND REGEXP_LIKE(A.c205_part_number_id,REGEXP_REPLACE('");
      sbQuery.append(strPartNums);
      sbQuery.append("','[+]','\\+'))");
    	
    }
    if (!strVendorId.equals("0")) {
      sbQuery.append(" AND B.C301_VENDOR_ID = NVL('");
      sbQuery.append(strVendorId);
      sbQuery.append("',B.C301_VENDOR_ID) ");
    }
    if (strQtyToOrderFlag.equals("Y")) {
      sbQuery.append(" AND C.C205_AVAILABLE_QTY > 0");
    }
    sbQuery
        .append(" AND A.C901_STATUS_ID = 20367 AND B.C405_COST_PRICE IS NOT NULL AND B.c405_active_fl = 'Y' ");
    sbQuery.append(" AND C.C901_TRANSACTION_TYPE (+) = 90801 ");
    sbQuery.append(" AND C.C5040_PLANT_ID(+) = " + getCompPlantId());
    sbQuery.append(" AND A.C205_PART_NUMBER_ID = C.C205_PART_NUMBER_ID(+) ");
    sbQuery.append(" ORDER BY A.C205_PART_NUMBER_ID, B.C301_VENDOR_ID, B.C405_COST_PRICE");

    log.debug(" Query to fetch Vendor Price List is " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    hmReturn.put("VENDORPRICELIST", alReturn);


    return hmReturn;
  } // End of getPartVendorPriceList



  /**
   * initatePOProcess - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap initatePOProcess(String strProjId, String strPOString, String strWOString,
      String strType, String strUsername) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strMsg = "";

    HashMap hmReturn = new HashMap();
    HashMap hmVendor = new HashMap();

    ArrayList alReturn = new ArrayList();

    StringTokenizer strTok = new StringTokenizer(strPOString, "|");
    StringTokenizer strInnerTok = null;

    String strVendorId = "";
    String strPOTotal = "";
    String strTemp = "";
    String strPOTemp = "'";
    String strPO = "";

    String strPartNum = "";
    String strQty = "";
    String strCost = "";
    String strRev = "";
    String strCriticalFl = "";
    String strDHRId = "";
    String strFARFl = "";
    String strVLDFl = "";
    String strLotCount = "";
    String strPriceId = "";

    HashMap hmLoop = new HashMap();
    String strReturnPO = "";
    int i = 0;
    String strCompanyId = getGmDataStoreVO().getCmpid();

    // FOR PO
    while (strTok.hasMoreTokens()) {
      strTemp = strTok.nextToken();
      strInnerTok = new StringTokenizer(strTemp, ",");

      while (strInnerTok.hasMoreTokens()) {
        strVendorId = strInnerTok.nextToken();
        strPOTotal = strInnerTok.nextToken();
      }
      gmDBManager.setPrepareString("GM_PLACE_PO", 7);

      /*
       * register out parameter and set input parameters
       */
      gmDBManager.registerOutParameter(6, OracleTypes.CHAR);
      gmDBManager.registerOutParameter(7, OracleTypes.CHAR);

      gmDBManager.setString(1, strVendorId);
      gmDBManager.setString(2, strPOTotal);
      gmDBManager.setString(3, strUsername);
      gmDBManager.setInt(4, Integer.parseInt(strType));
      gmDBManager.setString(5, "");
      gmDBManager.execute();
      strReturnPO = gmDBManager.getString(6);
      strMsg = gmDBManager.getString(7);
      log.debug(" Msg is " + strMsg);

      hmVendor.put(strVendorId, strReturnPO);
      strPOTemp = strPOTemp + strReturnPO + "','";
      
      hmReturn.put("MSG", strMsg);
      
    } // ENd of WHILE -- FOR PO

    strTok = new StringTokenizer(strWOString, "|");
    log.debug(" Tokens are " + strTok.countTokens());
    // For WO
    while (strTok.hasMoreTokens()) {
      strTemp = strTok.nextToken();
      strInnerTok = new StringTokenizer(strTemp, "^");

      while (strInnerTok.hasMoreTokens()) {
        strPartNum = strInnerTok.nextToken();
        strRev = strInnerTok.nextToken();
        strVendorId = strInnerTok.nextToken();
        strQty = strInnerTok.nextToken();
        strCost = strInnerTok.nextToken();
        strCriticalFl = strInnerTok.nextToken();
        strVLDFl = strInnerTok.nextToken();
        strLotCount = strInnerTok.nextToken();
        strDHRId = strInnerTok.nextToken();
        strFARFl = strInnerTok.nextToken();
        strPriceId = strInnerTok.nextToken();
      }
      strCriticalFl = strCriticalFl.equals("N") ? "" : strCriticalFl;
      strDHRId = strDHRId.equals("N") ? "" : strDHRId;
      strFARFl = strFARFl.equals("N") ? "" : strFARFl;

      strPO = (String) hmVendor.get(strVendorId);
      log.debug(" PO value is " + strPO);
      gmDBManager.setPrepareString("GM_PLACE_WO", 15);

      /*
       * register out parameter and set input parameters
       */
      gmDBManager.registerOutParameter(15, OracleTypes.CHAR);

      gmDBManager.setString(1, strPO);
      gmDBManager.setString(2, strPartNum);
      gmDBManager.setString(3, strRev);
      gmDBManager.setString(4, strVendorId);
      gmDBManager.setInt(5, Integer.parseInt(strQty));
      gmDBManager.setDouble(6, Double.parseDouble(strCost));
      gmDBManager.setString(7, strCriticalFl);
      gmDBManager.setString(8, strDHRId);
      gmDBManager.setString(9, strFARFl);
      gmDBManager.setString(10, strVLDFl);
      gmDBManager.setString(11, strLotCount);
      gmDBManager.setInt(12, Integer.parseInt(strPriceId));
      gmDBManager.setInt(13, Integer.parseInt(strType));
      gmDBManager.setString(14, strUsername);
      gmDBManager.execute();

      strMsg = gmDBManager.getString(15);

      hmReturn.put("MSG", strMsg);

    } // ENd of WHILE -- FOR WO
    gmDBManager.commit();

    strPOTemp = strPOTemp.substring(0, strPOTemp.length() - 2);
    String[] strPoIds = strPOTemp.split(",");
    for (int j = 0; j < strPoIds.length; j++) {
      sendFAREmail(strPoIds[j].substring(1, strPoIds[j].length() - 1));
    }

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT C401_PURCHASE_ORD_ID POID, GET_VENDOR_NAME(C301_VENDOR_ID) VNAME, ");
    sbQuery.append(" C401_DATE_REQUIRED REQDT,    C401_PO_TOTAL_AMOUNT TOTAL, ");
    sbQuery.append(" GET_USER_NAME(C401_CREATED_BY) CUSER, C301_VENDOR_ID VID, ");
    sbQuery.append(" GET_CODE_NAME(GET_VENDOR_CURRENCY(C301_VENDOR_ID)) VENDCURR");
    sbQuery.append(" FROM T401_PURCHASE_ORDER ");
    sbQuery.append(" WHERE C401_PURCHASE_ORD_ID IN (");
    sbQuery.append(strPOTemp);
    sbQuery.append(") ORDER BY C401_PURCHASE_ORD_ID");

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    hmReturn.put("POLIST", alReturn);


    return hmReturn;
  } // End of initatePOProcess

  /**
   * sendFAREmail - This method will
   * 
   * @param String strPOId
   * @return void
   * @exception AppError
   **/
  public void sendFAREmail(String strPOId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    ArrayList alWODetails = new ArrayList();
    HashMap hmWoDetails = new HashMap();
    HashMap hmRuleData = new HashMap();

    String strWoid = "";
    String strPnum = "";
    String strPdesc = "";
    String strPrjname = "";
    String strVname = "";
    String strCrby = "";
    String strCrdt = "";
    String strMailfrm = "";
    String strMailTo = "";
    String[] strCC = new String[0];
    String strCompanyId = "";

    gmDBManager.setPrepareString("gm_pkg_op_purchasing.gm_fch_far_wo", 2);
    gmDBManager.setString(1, strPOId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();

    strCompanyId = getGmDataStoreVO().getCmpid();
    strMailfrm = GmCommonClass.getRuleValueByCompany("MAILFRM", "EMAIL", strCompanyId);

    strMailTo = GmCommonClass.getRuleValueByCompany("FARPLACED", "EMAIL", strCompanyId);
    alWODetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    for (Iterator iter = alWODetails.iterator(); iter.hasNext();) {
      hmWoDetails = (HashMap) iter.next();
      strWoid = (String) hmWoDetails.get("WOID");
      strPnum = (String) hmWoDetails.get("PNUM");
      strPdesc = (String) hmWoDetails.get("PDESC");
      strPrjname = (String) hmWoDetails.get("PRJNAME");
      strVname = (String) hmWoDetails.get("VNAME");
      strCrby = (String) hmWoDetails.get("CRBY");
      strCrdt = (String) hmWoDetails.get("CRDT");

      StringBuffer sbMailBdy = new StringBuffer();
      sbMailBdy.append("Part Number: ");
      sbMailBdy.append(strPnum);
      sbMailBdy.append("\n");
      sbMailBdy.append("Description: ");
      sbMailBdy.append(strPdesc);
      sbMailBdy.append("\n");
      sbMailBdy.append("Project: ");
      sbMailBdy.append(strPrjname);
      sbMailBdy.append("\n");
      sbMailBdy.append("Purchase Order: ");
      sbMailBdy.append(strPOId);
      sbMailBdy.append("\n");
      sbMailBdy.append("Work Order: ");
      sbMailBdy.append(strWoid);
      sbMailBdy.append("\n");
      sbMailBdy.append("Vendor: ");
      sbMailBdy.append(strVname);
      sbMailBdy.append("\n");
      sbMailBdy.append("Created By: ");
      sbMailBdy.append(strCrby);
      sbMailBdy.append("\n");
      sbMailBdy.append("Created On: ");
      sbMailBdy.append(strCrdt);
      sbMailBdy.append("\n");

      gmCommon.sendMail(strMailfrm, GmCommonClass.StringtoArray(strMailTo, ";"), strCC,
          "First article order placed for Part Number:" + strPnum, sbMailBdy.toString());

    }
  }

  /**
   * viewPO -  This method contains following fetch
   *  1. Fetch PO details.
   *  2. Fetch Work Order Details.
   *  3. Fetch UDI related rules.
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap viewPO(String strPOId, String strVendorId, String strUserId) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmParams = new HashMap();
    ArrayList alReturn = new ArrayList();
    ArrayList alUDIParams = new ArrayList();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    // Calling new method to get viewPODetails PMT-44229  
    hmReturn=viewPODetails(strPOId);   
    alReturn = getWOList(strPOId);
    hmParams.put("CODEGRPID", "UDISRC");
    alUDIParams = gmCommonBean.fetchUDIRuleParams(hmParams);
    hmReturn.put("ITEMDETAILS", alReturn);
    hmReturn.put("UDIDETAILS", alUDIParams);

    return hmReturn;
  } // End of viewPO
  
  
  /**
   * viewPODetails -  This method contains following fetch
   *  Fetch PO details. (Called from BBA Bulk Receiving Transaction)   *
   *  @Author: gpalani PMT-44229      
   * @param String strPOId
   * @return HashMap
   * @exception AppError
   **/
  public HashMap viewPODetails(String strPOId) throws AppError {
	  HashMap hmReturn = new HashMap();
	    HashMap hmResult = new HashMap();
	    StringBuffer sbPODetailsQuery = new StringBuffer();
	    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
	    String strPoType = "";
	    String strDHRCount="";
	    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
	    GmResourceBundleBean gmResourceBundleBean =
	        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	    String strCountryCode =
	        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("COUNTRYCODE"));

	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

	    sbPODetailsQuery
	        .append(" SELECT  A.C301_VENDOR_NAME ||'<BR>&nbsp;'|| A.C301_BILL_ADD1 ||'<BR>&nbsp;'|| A.C301_BILL_CITY ");
	    sbPODetailsQuery
	        .append(" ||'<BR>&nbsp;'|| GET_CODE_NAME_ALT(A.C301_BILL_STATE) ||'&nbsp;'|| A.C301_BILL_ZIP VADD, A.C301_VENDOR_ID VID, ");
	    sbPODetailsQuery.append(" B.C401_CREATED_DATE PODATE, B.C401_DATE_REQUIRED RQDATE, ");
	    sbPODetailsQuery
	        .append(" B.C401_PO_TOTAL_AMOUNT TOTAL, B.C401_PURCHASE_ORD_ID POID, B.C401_CREATED_BY CUSER, ");
	    sbPODetailsQuery
	        .append(" GET_USER_NAME(B.C401_CREATED_BY) CNAME , B.C401_PO_NOTES COMMENTS, A.C301_VENDOR_NAME VNAME, B.C401_TYPE POTYPE ");
	    
	    sbPODetailsQuery.append(" ,GET_RULE_VALUE(B.C401_TYPE,'MFG_PO_TYPE') MFGTYPE ");
	    sbPODetailsQuery.append(" , A.C301_LOT_CODE LCODE , get_code_name(B.C401_TYPE) POTYPENM,C301_CURRENCY CURR , GET_CODE_NAME(GET_VENDOR_CURRENCY(A.C301_VENDOR_ID)) VENDCURR");
	    sbPODetailsQuery.append(" , get_code_name(C301_CURRENCY) CURRCD, B.c401_publish_fl PUBFL, B.c401_published_dt PUBLDT ");
	    sbPODetailsQuery.append(" ,  GET_USER_NAME(B.C401_PUBLISHED_BY) PUBLBY, get_code_name(B.C901_status) STATUS, A.C301_VENDOR_PORTAL_FL VENFL, ");
	    sbPODetailsQuery.append(" get_dhr_cnt('");
	    sbPODetailsQuery.append(strPOId);
	    sbPODetailsQuery.append("') DHRCOUNT");
	    sbPODetailsQuery.append(" FROM  T301_VENDOR A, T401_PURCHASE_ORDER B ");

	    if (strPOId.toUpperCase().indexOf("GM-PO") != -1 || strPOId.toUpperCase().indexOf("-") != -1
	        || strPOId.toUpperCase().indexOf("GM-MF") != -1 || !strCountryCode.equals("en")) {
	    	sbPODetailsQuery.append(" WHERE B.C401_PURCHASE_ORD_ID ='");
	    	sbPODetailsQuery.append(strPOId);
	      sbPODetailsQuery.append("' AND A.C301_VENDOR_ID = B.C301_VENDOR_ID");

	    } else {
	    	sbPODetailsQuery.append(" WHERE (B.C401_PURCHASE_ORD_ID ='GM-PO-");
	    	sbPODetailsQuery.append(strPOId);
	    	sbPODetailsQuery.append("' OR B.C401_PURCHASE_ORD_ID ='GM-MF-");
	    	sbPODetailsQuery.append(strPOId);
	        sbPODetailsQuery.append("') AND A.C301_VENDOR_ID = B.C301_VENDOR_ID");
	    }
	    sbPODetailsQuery.append(" AND B.C401_VOID_FL IS NULL");
	    // Appending company filter
	    sbPODetailsQuery.append(" AND  B.C1900_COMPANY_ID = " + getCompId());
	    log.debug(" Query for View PO Details operation is " + sbPODetailsQuery.toString());
	    hmResult = gmDBManager.querySingleRecord(sbPODetailsQuery.toString());
	    strPoType = (String) hmResult.get("POTYPE");
	    strDHRCount = (String) hmResult.get("DHRCOUNT");
	    log.debug(" PO type in PO Details "	 + strPoType);
	    hmReturn.put("POTYPE", strPoType);
	    hmReturn.put("PODETAILS", hmResult); // Vendor Address
	    hmReturn.put("DHRCOUNT",strDHRCount);

	  	    return hmReturn;
	  } // End of viewPODetails
  

  /**
   * viewWO - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap viewWO(String strWOId, String strUserId) throws AppError {
	  HashMap hmReturn = new HashMap();
	    HashMap hmResult = new HashMap();
	    ArrayList alReturn = new ArrayList();
	    ArrayList alSubWorkOrder = new ArrayList();
	    ArrayList alSubDHR = new ArrayList();
	    StringBuffer sbQuery = new StringBuffer();
	    String strSubDHRId = "";
	    String strSterFl = "";
	    String strSubCompFl = "";
	    String strDI = "";
	    String strvendoeDesign = "";
	    String strDIPrefix = "";
	    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
	    GmResourceBundleBean gmResourceBundleBean =
	        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);

	    String strUDI = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("WO.UDI"));
	    String strvendortext =
	        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("WO.VEND_DESIGN"));
	    String strLaserMarktext =
	        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("WO.VEND_DESIGN_LASER"));
	    String strPI = "";
	    String strDateFormat = getCompDateFmt();
	    String strDefualtDIVal =
	        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("WO.DIDEFAULT"));
	    String strDefualtPIVal =
	        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("WO.PIDEFAULT"));
	    GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());

	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

	    sbQuery
	        .append(" SELECT t402.C402_WORK_ORDER_ID WID, t402.C401_PURCHASE_ORD_ID POID, T402A.wopartnum PARTNUM, ");
	    sbQuery.append(" T402A.wovname VNAME, to_char(t402.C402_CREATED_DATE,'" + strDateFormat
	        + "') CDATE, ");
	    sbQuery.append(" t402.c402_qty_ordered QTY, T402A.wodesc PDESC, T402A.womcfl MCFL, ");
	    sbQuery
	        .append(" T402A.woccfl CCFL, t301.C301_LOT_CODE LC, t205.c202_project_id PRJID, get_project_name(t205.c202_project_id) PRJNAME, ");
	    sbQuery
	        .append(" t402.C402_CREATED_BY CUSER, GET_CODE_NAME(T402A.womspec) MSPEC, T402A.wodraw DRAW, ");
	    sbQuery.append(" to_char(t402.c402_wo_due_date,'" + strDateFormat
	            + "') RDATE, T402A.worevnum DRAWREV, t402.C402_CRITICAL_FL CFL, ");
	    sbQuery.append(" to_char(t402.c402_wo_due_date,'" + strDateFormat
	        + "') WODATE, ");
	    sbQuery.append(" t402.C408_DHR_ID WORKDHRID, GET_DOC_FOOTER(to_char(t402.C402_CREATED_DATE,'"
	        + strDateFormat + "'),'GM-G001') FOOTER, ");
	    sbQuery
	        .append(" t402.C402_STERILIZATION_FL STERFL, t205.C205_SUB_COMPONENT_FL SUBCOMPFL, t205.C205_SUB_ASMBLY_FL SUBASMBFL, ");
	    sbQuery.append(" t402.C402_FAR_FL FARFL, t402.C402_STATUS_FL STATUSFL, NVL(T402A.wodi,'') DI,");
	    sbQuery
	        .append(" T402A.wohfl HTFL,DECODE(t402.C402_FAR_FL,'Y','',t402.c402_validation_fl) VALIDATIONFL,DECODE(C402_FAR_FL,'Y',1,DECODE(c402_validation_fl, 'Y',2,3)) FLAGORD , ");
	    sbQuery.append(" T402A.woudietch UDIFL ,");
	    sbQuery.append(" T402A.wodietch  DIFL, ");
	    sbQuery.append(" T402A.wopietch  PIFL , ");
	    sbQuery
	        .append(" NVL(TRIM(GET_PART_ATTRIBUTE_VALUE (t402.C205_PART_NUMBER_ID, '4000517')),'103421') vendorDesign , ");
	    sbQuery.append(" T402A.wodiprefix  udiprefix , ");
	    sbQuery.append(" T402A.wopi piconfnm ");
	    // -- PC-2135 Inhouse MWO paperwork 2020 - Product Traveler and DHR Review
	    sbQuery.append(" , t401.C401_TYPE po_type, get_bom_id(T402A.wopartnum) BOMID ");
	    sbQuery
	        .append(" FROM T402_WORK_ORDER t402, T205_PART_NUMBER t205, T301_VENDOR t301, T401_PURCHASE_ORDER t401, (");
	    sbQuery.append("  SELECT C402_WORK_ORDER_ID woid, ");
	    sbQuery
	        .append("    MAX (DECODE (c901_attribute_type, 103402, c402a_attribute_value)) wodraw, ");
	    sbQuery
	        .append("     MAX (DECODE (c901_attribute_type, 103403, c402a_attribute_value)) womspec,");
	    sbQuery
	        .append("     MAX (DECODE (c901_attribute_type, 103404, c402a_attribute_value)) womcfl, ");
	    sbQuery
	        .append("     MAX (DECODE (c901_attribute_type, 103405, c402a_attribute_value)) wohfl, ");
	    sbQuery
	        .append("     MAX (DECODE (c901_attribute_type, 103406, c402a_attribute_value)) woccfl, ");
	    sbQuery
	        .append("     MAX (DECODE (c901_attribute_type, 103407, c402a_attribute_value)) woudietch, ");
	    sbQuery
	        .append("     MAX (DECODE (c901_attribute_type, 103408, c402a_attribute_value)) wodietch, ");
	    sbQuery
	        .append("     MAX (DECODE (c901_attribute_type, 103409, c402a_attribute_value)) wopietch, ");
	    sbQuery
	        .append("     MAX (DECODE (c901_attribute_type, 4000678, c402a_attribute_value)) wopartnum, ");
	    sbQuery
	        .append("     MAX (DECODE (c901_attribute_type, 4000680, c402a_attribute_value)) worevnum, ");
	    sbQuery
	        .append("     MAX (DECODE (c901_attribute_type, 4000681, c402a_attribute_value)) wodesc, ");
	    sbQuery
	        .append("     MAX (DECODE (c901_attribute_type, 4000682, c402a_attribute_value)) wovname, ");
	    sbQuery
	        .append("     MAX (DECODE (c901_attribute_type, 4000683, c402a_attribute_value)) wodi, ");
	    sbQuery
	        .append("     MAX (DECODE (c901_attribute_type, 4000684, c402a_attribute_value)) wodiprefix, ");
	    sbQuery.append("     MAX (DECODE (c901_attribute_type, 4000685, c402a_attribute_value)) wopi ");


	    sbQuery
	        .append("    FROM t402a_wo_attribute WHERE c402a_void_fl IS NULL AND c402_work_order_id = '");
	    sbQuery.append(strWOId);
	    sbQuery.append("' GROUP BY C402_WORK_ORDER_ID )  T402A ");
	    sbQuery
	        .append(" WHERE t402.C205_PART_NUMBER_ID = t205.C205_PART_NUMBER_ID AND t402.C301_VENDOR_ID = t301.C301_VENDOR_ID ");
	    sbQuery.append(" AND t402.C401_PURCHASE_ORD_ID = t401.C401_PURCHASE_ORD_ID ");
	    sbQuery.append(" AND t402.C402_WORK_ORDER_ID   = T402A.woid(+) ");
	    sbQuery.append(" AND t402.C402_VOID_FL IS NULL AND t402.C402_WORK_ORDER_ID ='");
	    sbQuery.append(strWOId);
	    sbQuery.append("' ORDER BY t402.C205_PART_NUMBER_ID,FLAGORD,t402.C402_WORK_ORDER_ID");
	    log.debug("Query =====" + sbQuery.toString());
	    hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
	    strvendoeDesign = GmCommonClass.parseNull((String) hmResult.get("VENDORDESIGN"));

	    if (strvendoeDesign.equals("103420")) {// 103420 -- vendor design parts- True
	      strDI = strvendortext;
	      strUDI = strvendortext;
	      strPI = strvendortext;
	    } else {
	      strDI = GmCommonClass.parseNull((String) hmResult.get("DI"));
	      strDIPrefix = GmCommonClass.parseNull((String) hmResult.get("UDIPREFIX"));
	      if (!strDI.equals("")) {
	        strDI = strDIPrefix + strDI;
	        strPI = GmCommonClass.parseNull((String) hmResult.get("PICONFNM"));
	      } else {
	        strPI = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("WO.PI"));
	      }
	    }

	    hmResult.put("DIDEFAULT", strDefualtDIVal); // DI Default value to N/A
	    hmResult.put("PIDEFAULT", strDefualtPIVal); // PI Default value to N/A
	    hmResult.put("DI", strDI);
	    hmResult.put("UDI", strUDI);
	    hmResult.put("PI", strPI);
	    hmResult.put("VENDOR_DESIGN", strvendoeDesign);
	    hmResult.put("LASER_MARK", strLaserMarktext);
	    hmReturn.put("WODETAILS", hmResult); // Work Order Details
	    if (hmResult != null) {
	      strSubDHRId = (String) hmResult.get("WORKDHRID");
	      strSterFl = (String) hmResult.get("STERFL");
	      strSubCompFl = (String) hmResult.get("SUBASMBFL");
	    }
	    alSubWorkOrder = loadSubWorkOrder(strWOId);
	    hmReturn.put("SUBWODETAILS", alSubWorkOrder); // Sub-Work Order Details
	    alSubDHR = gmOper.getSubDHRDetails(strSubDHRId, strSterFl, strSubCompFl);
	    hmReturn.put("SUBDHRDETAILS", alSubDHR); // Sub-DHR Details



	  return hmReturn;
  } // End of viewWO

  /**
   * getPOList - This method will
   * 
   * @param hmParam
   * @return HashMap
   * @exception AppError
   **/
  public HashMap getPOList(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alResult = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    String strCompDFmt = getCompDateFmt();

    String strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDID"));
    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FRMDT"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strId = GmCommonClass.parseNull((String) hmParam.get("POID"));
    String strPOType = GmCommonClass.parseNull((String) hmParam.get("POTYPE"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strAmount = GmCommonClass.parseNull((String) hmParam.get("POAMT"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    sbQuery.append(" SELECT C401_PURCHASE_ORD_ID ID, C401_PURCHASE_ORD_DATE PODATE,");
    sbQuery.append(" GET_CODE_NAME(GET_VENDOR_CURRENCY(C301_VENDOR_ID)) VENDCURR, ");
    sbQuery.append(" C401_PO_TOTAL_AMOUNT TOTAL, C401_DATE_REQUIRED REQDATE, ");
    sbQuery
        .append(" GET_USER_NAME(C401_CREATED_BY) UNAME, GET_VENDOR_NAME(C301_VENDOR_ID) VENDNM ");
    sbQuery
        .append(" ,GET_LOG_FLAG(C401_PURCHASE_ORD_ID, 1203) CALL_FLAG , get_code_name(C401_TYPE) POTYPENM, get_latest_log_comments(C401_PURCHASE_ORD_ID,1203) LASTCOMMENTS ");
    sbQuery.append(" FROM T401_PURCHASE_ORDER ");
    sbQuery.append(" WHERE C401_VOID_FL IS NULL ");
    sbQuery.append(" AND C1900_COMPANY_ID =" + getCompId());

    if (!strFromDt.equals("") && !strToDt.equals("")) {
      sbQuery.append(" AND C401_PURCHASE_ORD_DATE BETWEEN");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("','" + strCompDFmt + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strCompDFmt + "')");
    }
    if (!strVendorId.equals("") && !strVendorId.equals("0")) {
      sbQuery.append(" AND C301_VENDOR_ID = ");
      sbQuery.append(strVendorId);
    }
    if (!strPOType.equals("")) {
      sbQuery.append(" AND C401_TYPE = '");
      sbQuery.append(strPOType);
      sbQuery.append("'");
    }

    if (!strId.equals("")) {
      sbQuery.append(" AND C401_PURCHASE_ORD_ID like '%");
      sbQuery.append(strId);
      sbQuery.append("'");
    }
    if (!strUserId.equals("0") && !strUserId.equals("")) {
      sbQuery.append(" AND C401_CREATED_BY = '");
      sbQuery.append(strUserId);
      sbQuery.append("'");
    }
    if (!strAmount.equals("")) {
      sbQuery.append(" AND C401_PO_TOTAL_AMOUNT >= ");
      sbQuery.append(strAmount);
    }

    sbQuery.append(" ORDER BY C401_PURCHASE_ORD_ID DESC");
    log.debug("QUERY:" + sbQuery.toString());
    alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
    hmReturn.put("POLIST", alResult);
    return hmReturn;
  } // End of getPOList


  /**
   * getWOList - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList getWOList(String strPOId) throws AppError {
    ArrayList alResult = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_PKG_WO_TXN.gm_get_wo_list", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPOId);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alResult;
  } // End of getWOList


  /**
   * getAllWOForPrint - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList getAllWOForPrint(String strPOId) throws AppError {
    ArrayList alReturn = new ArrayList();
    HashMap hmLoop = new HashMap();
    HashMap hmLoopWO = new HashMap();
    ArrayList alResult = new ArrayList();
    ArrayList alLoop = new ArrayList();

    StringBuffer sbQuery = new StringBuffer();
    String strTemp = "";
    int intSize = 0;

    alResult = getWOList(strPOId);
    intSize = alResult.size();
    for (int i = 0; i < intSize; i++) {
      hmLoop = (HashMap) alResult.get(i);
      strTemp = (String) hmLoop.get("WID");
      hmLoopWO = viewWO(strTemp, "SystemUser");
      alReturn.add(hmLoopWO);
    }


    return alReturn;
  } // End of getAllWOForPrint

  /**
   * getFaxCoverDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap getFaxCoverDetails(String strPOId) throws AppError {
    HashMap hmReturn = new HashMap();

    StringBuffer sbQuery = new StringBuffer();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    sbQuery.append(" SELECT A.C101_USER_ID ID, GET_USER_NAME(A.C101_USER_ID) UNAME , ");
    sbQuery.append(" B.C301_CONTACT_PERSON VPERSON, B.C301_PHONE VPHONE, B.C301_FAX VFAX, ");
    sbQuery.append(" A.C101_WORK_PHONE ||' extn:'|| A.C101_PH_EXTN UPHONE, ");
    sbQuery.append(" C.C401_PURCHASE_ORD_ID POID, CURRENT_DATE PDATE, A.C101_EMAIL_ID EMAIL ");
    sbQuery.append(" FROM T101_USER A, T301_VENDOR B, T401_PURCHASE_ORDER C ");
    sbQuery.append(" WHERE B.C301_VENDOR_ID = C.C301_VENDOR_ID ");
    sbQuery.append(" AND C.C401_CREATED_BY = A.C101_USER_ID AND C.C401_PURCHASE_ORD_ID like '%");
    sbQuery.append(strPOId);
    sbQuery.append("' AND C.C401_VOID_FL IS NULL");
    hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());


    return hmReturn;
  } // End of getFaxCoverDetails


  /**
   * updatePO - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap updatePO(String strPOId, String strPOTotal, String strReqDate, String strComments, String strPublishedfl,
      String strUsername, String alPOType) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strMsg = "";

    HashMap hmReturn = new HashMap();

    int intSize = 0;


    gmDBManager.setPrepareString("GM_UPDATE_PO_OPER", 8);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(8, java.sql.Types.CHAR);

    gmDBManager.setString(1, strPOId);
    gmDBManager.setString(2, strReqDate);
    gmDBManager.setString(3, strComments);
    gmDBManager.setString(4, strUsername);
    gmDBManager.setDouble(5, Double.parseDouble(strPOTotal));
    gmDBManager.setString(6, strPublishedfl);
    gmDBManager.setString(7, alPOType);

    gmDBManager.execute();
    strMsg = gmDBManager.getString(8);
    gmDBManager.commit();

    hmReturn.put("MSG", strMsg);


    return hmReturn;
  } // end of updatePO

  /**
   * updateWO - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap updateWO(String strInputStr, String strUsername) throws AppError {
    String strMsg = "";
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("GM_UPDATE_WO_OPER", 3);
    gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);
    gmDBManager.setString(1, strInputStr);
    gmDBManager.setString(2, strUsername);


    gmDBManager.execute();
    strMsg = gmDBManager.getString(3);
    gmDBManager.commit();
    hmReturn.put("MSG", strMsg);
    return hmReturn;
  } // end of updateWO


  /**
   * getPartNumInvDetails - This method will
   * 
   * @param String
   * @param String
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList getPartNumInvDetails(String strPartNum, String strOpt) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;


    gmDBManager.setPrepareString("GM_OP_FCH_PARTINVDETAILS", 3);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strOpt);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();


    return alReturn;
  } // End of getPartNumInvDetails



  /**
   * loadSubWorkOrder -
   **/
  public ArrayList loadSubWorkOrder(String strWOId) throws AppError {
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    /*
     * To be removed after testing partnum mapping WO changes sbQuery.append(
     * " SELECT DISTINCT A.C206_SUB_ASMBLY_ID PNUM, GET_CODE_NAME(A.C206_MATERIAL_SPEC) MSPEC, ");
     * sbQuery.append(" A.C206_SUB_ASMBLY_DRAWING DRAW, A.C206_SUB_ASMBLY_DESC PDESC, ");
     * sbQuery.append(" B.C403_REV_NUM REV ");
     * sbQuery.append(" FROM T206_SUB_ASSEMBLY A, T403_SUB_WORK_ORDER B ");
     * sbQuery.append(" WHERE A.C206_SUB_ASMBLY_ID = B. C206_SUB_ASMBLY_ID AND B.C402_WORK_ORDER_ID ='"
     * ); sbQuery.append(strWOId); sbQuery.append("' ORDER BY A.C206_SUB_ASMBLY_ID");
     */
    sbQuery
        .append(" SELECT DISTINCT T205.C205_PART_NUMBER_ID PNUM, GET_CODE_NAME(T205.C205_MATERIAL_SPEC) MSPEC, ");
    sbQuery.append(" T205.C205_PART_DRAWING DRAW, T205.C205_PART_NUM_DESC PDESC, ");
    sbQuery.append(" T403.C403_REV_NUM REV ");
    sbQuery.append(" FROM T205_PART_NUMBER T205, T403_SUB_WORK_ORDER T403 ");
    sbQuery
        .append(" WHERE T205.C205_PART_NUMBER_ID = T403.C206_SUB_ASMBLY_ID AND T403.C402_WORK_ORDER_ID ='");
    sbQuery.append(strWOId);
    sbQuery.append("' ORDER BY T205.C205_PART_NUMBER_ID ");

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());



    return alReturn;
  } // End of loadSubWorkOrder


  /**
   * getPOReport - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap getPOReport(String strPartNums, String strFromDt, String strToDt) throws AppError {
	    HashMap hmReturn = new HashMap();
	    ArrayList alResult = new ArrayList();
	    StringBuffer sbQuery = new StringBuffer();
	    String strCompDFmt = getCompDateFmt();

	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    sbQuery.append(" SELECT t401.C401_PURCHASE_ORD_ID ID, t401.C401_PURCHASE_ORD_DATE PODATE,");
	    sbQuery.append(" t401.C401_DATE_REQUIRED REQDATE, t402.C402_WORK_ORDER_ID WORKID, ");
	    sbQuery
	        .append(" GET_USER_NAME(t401.C401_CREATED_BY) UNAME, GET_VENDOR_SH_NAME(t401.C301_VENDOR_ID) VENDNM, ");
	    sbQuery.append(" t402.C205_PART_NUMBER_ID PNUM, GET_PARTNUM_DESC(t402.C205_PART_NUMBER_ID) PDESC, ");
	    sbQuery
	        .append(" GET_CODE_NAME(GET_VENDOR_CURRENCY(t402.C301_VENDOR_ID)) VENDCURR, t402.C402_COST_PRICE CPRICE, t402.C402_QTY_ORDERED QTYORD, t402.C402_CRITICAL_FL CFL, ");
	    sbQuery
	        .append(" GET_WO_PEND_QTY(t402.C402_WORK_ORDER_ID,t402.C402_QTY_ORDERED) PEND, t401.C301_VENDOR_ID VID, ");
	    sbQuery
	        .append(" GET_WO_WIP_QTY(t402.C402_WORK_ORDER_ID) WIP, t402.C402_STATUS_FL SFL, GET_WO_STOCK_QTY(t402.C402_WORK_ORDER_ID) STOCK ");
	    sbQuery
	        .append(" ,get_code_name(t401.C401_TYPE) POTYPENM , get_log_flag (t402.C402_WORK_ORDER_ID, 1232) WLOG ");
	    sbQuery.append(" ,t402.C402_REV_NUM WOREV, t402.C402_WO_DUE_DATE  WODATE, t402.C402_WO_LEAD_DATE WOLEADDATE ");
	    sbQuery.append(" FROM T401_PURCHASE_ORDER t401, T402_WORK_ORDER t402 ");
	    sbQuery.append(" WHERE t401.C401_PURCHASE_ORD_ID = t402.C401_PURCHASE_ORD_ID ");
	    sbQuery
	        .append(" AND t401.C401_VOID_FL IS NULL AND t402.C402_VOID_FL IS NULL AND t402.C205_PART_NUMBER_ID IN (");
	    sbQuery.append(strPartNums);
	    sbQuery.append(")");
	    sbQuery.append(" AND t401.C1900_COMPANY_ID =" + getCompId());
	    if (!strFromDt.equals("")) {
	      sbQuery.append(" AND t401.C401_PURCHASE_ORD_DATE BETWEEN");
	      sbQuery.append(" to_date('");
	      sbQuery.append(strFromDt);
	      sbQuery.append("','" + strCompDFmt + "') AND to_date('");
	      sbQuery.append(strToDt);
	      sbQuery.append("','" + strCompDFmt + "')");
	    }

	    sbQuery.append(" ORDER BY t402.C205_PART_NUMBER_ID, t401.C401_PURCHASE_ORD_ID DESC");
	    log.debug("POREPORT:" + sbQuery.toString());
	    alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
	    hmReturn.put("POREPORT", alResult);


	   return hmReturn;
  } // End of getPOReport

  /**
   * getDHRForSterilization - This method will
   * 
   * @return HashMap
   * @exception AppError
   **/
  public HashMap getDHRForSterilization(String strVendorId) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    ArrayList alResult = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();


    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    sbQuery.append(" SELECT C408_DHR_ID DHRID, C205_PART_NUMBER_ID PARTNUM, C301_VENDOR_ID VID, ");
    sbQuery.append(" GET_PARTNUM_DESC(C205_PART_NUMBER_ID) PDESC, C408_STATUS_FL SFL, ");
    sbQuery.append(" GET_VENDOR_NAME(C301_VENDOR_ID) VNAME, C408_CREATED_DATE CDATE , ");
    sbQuery
        .append(" GET_DHR_STERILIZE_PEND_QTY(C408_DHR_ID,C408_QTY_ON_SHELF) QTY, C408_CONTROL_NUMBER CNUM, ");
    sbQuery.append(" GET_VENDOR_PART_COST('");
    sbQuery.append(strVendorId);
    sbQuery.append("',C205_PART_NUMBER_ID) COST ");
    sbQuery.append(" FROM T408_DHR T408");
    sbQuery.append(" WHERE C408_STATUS_FL = '4' AND C408_STERILE_STATUS_FL = '1'");
    sbQuery.append(" AND T408.C408_VOID_FL IS NULL ");
    sbQuery.append(" ORDER BY C205_PART_NUMBER_ID, C408_DHR_ID DESC ");

    alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
    hmReturn.put("DHRREPORT", alResult);
    hmResult = getVendorList("3008"); // hardcoding for filetering by Sterlizing Vendors
    alResult = (ArrayList) hmResult.get("VENDORLIST");
    hmReturn.put("VENDORLIST", alResult);



    return hmReturn;
  } // End of getDHRForSterilization


  /**
   * getPOPendReportByVendor - This method will return report for Outstanding PO's grouped by Vendor
   * 
   * @return HashMap
   * @exception AppError
   **/
  // the below code is added for PMT-4801 and the return type is changed to RowSetDynaClass which is
  // the fix for sorting the columns in wrapper.
  public ArrayList getPOPendReportByVendor(HashMap hmParam) throws AppError { //Changed for the PMT-30903 Pending PO Report - By Vendor Improvement 
    String strType = GmCommonClass.parseNull((String) hmParam.get("STRTYPE"));
    String strVendorId = GmCommonClass.parseZero((String) hmParam.get("STRVENDORID"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("STRPARTNUM"));
    String strProjId = GmCommonClass.parseNull((String) hmParam.get("STRPROJID"));
    String strTypeOfPO = GmCommonClass.parseZero((String) hmParam.get("STRTYPEOFPO"));
    String strUserId = GmCommonClass.parseZero((String) hmParam.get("EMPID"));
    RowSetDynaClass rdResult = null;

    ArrayList alReturn = new ArrayList();

    strType = strType.equals("0") ? null : strType;
    strProjId = strProjId.equals("0") ? null : strProjId;
    // ArrayList alResult = new ArrayList();
    RowSetDynaClass rdReturn = null;
    StringBuffer sbQuery = new StringBuffer();

    ArrayList alResult = new ArrayList();
    ArrayList alTempPartNumList = new ArrayList();
    ArrayList alDBResult = new ArrayList();
    String strPartNumInputString = "";
    Calendar now = Calendar.getInstance();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    if (strPartNum != null && strPartNum.length() > 500) {
      alTempPartNumList = GmCommonClass.convertStringToList(strPartNum, 500, "|");
    } else {
      alTempPartNumList = new ArrayList();
      alTempPartNumList.add(strPartNum);
    }


    if (alTempPartNumList != null) {
      for (Iterator itr = alTempPartNumList.iterator(); itr.hasNext();) {
        strPartNumInputString = (String) itr.next();
        if (strPartNumInputString.endsWith("|")) {
          strPartNumInputString =
              strPartNumInputString.substring(0, strPartNumInputString.length() - 1);
        }
        gmDBManager.setPrepareString("gm_op_fch_pend_rpt_vendor", 7);
        gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);

        gmDBManager.setString(1, strType);
        gmDBManager.setInt(2, Integer.parseInt(strVendorId));
        gmDBManager.setString(3, strPartNumInputString);
        gmDBManager.setString(4, strProjId);
        gmDBManager.setInt(5, Integer.parseInt(strTypeOfPO));
        gmDBManager.setString(7, strUserId);
        gmDBManager.execute();
        alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));

        // alResult.addAll(alDBResult);
      }
    }
    gmDBManager.close();

    return alReturn;
  } // End of getPOPendReportByVendor


  public HashMap updateVerifyDHR(String strVendorId, String strInputStr, String strPackSlip,
      String strComments, String strRecDate, String strType, String strUsername) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strBeanMsg = "";
    String strMsg = "";

    HashMap hmReturn = new HashMap();

    int intSize = 0;
    gmDBManager.setPrepareString("GM_OP_UPD_INFORMALRECV", 8);

    gmDBManager.registerOutParameter(8, java.sql.Types.CHAR);

    gmDBManager.setInt(1, Integer.parseInt(strVendorId));
    gmDBManager.setString(2, strComments);
    gmDBManager.setString(3, strPackSlip);
    gmDBManager.setString(4, strInputStr);
    gmDBManager.setString(5, strRecDate);
    gmDBManager.setString(6, strType);
    gmDBManager.setString(7, strUsername);

    gmDBManager.execute();

    String strPdtIds = gmDBManager.getString(8);
    log.debug(" DHR id value is " + strPdtIds);
    gmDBManager.commit();
    hmReturn.put("PDTID", strPdtIds);
    log.debug(" Outside Commit --- " + hmReturn);
    return hmReturn;
  }


  public HashMap loadSubpart(HashMap hmParam) throws AppError {

    RowSetDynaClass rdSubResult = null;

    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strProjectID = GmCommonClass.parseNull((String) hmParam.get("PROJECTLISTID"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));

    log.debug(" values in hmParam " + hmParam);
    gmDBManager.setPrepareString("gm_pkg_op_subpart.gm_fch_subpart_order", 3);
    // gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);

    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strProjectID);

    gmDBManager.execute();

    // String strMAstatus = gmDBManager.getString(3);
    rdSubResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));

    gmDBManager.close();
    hmReturn.put("SUBPARTORDER", rdSubResult);

    return hmReturn;

  }



  public void saveSubpartOrder(HashMap hmParam) throws AppError {
    log.debug("HmParam: " + hmParam);
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTR"));
    String strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJECTLISTID"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_subpart.gm_sav_subpart_order", 4);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strProjectId);
    gmDBManager.setString(3, strPartNum);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();

    gmDBManager.commit();


  }


  public RowSetDynaClass loadpartMapping(HashMap hmParam) throws AppError {

    RowSetDynaClass rdPartResult = null;

    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strProjectID = GmCommonClass.parseNull((String) hmParam.get("PROJECTLISTID"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
    String strOrderPart = GmCommonClass.parseNull((String) hmParam.get("DISPLAYORDERPARTS"));

    log.debug(" values in hmParam " + hmParam);
    gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_fch_part_mapping", 4);
    // gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);

    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strProjectID);
    gmDBManager.setString(3, strOrderPart);

    gmDBManager.execute();
    rdPartResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));

    gmDBManager.close();

    return rdPartResult;

  }

  public String getXmlGridData(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataList("alReturn", (ArrayList) hmParam.get("REPORT"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.GmPOReport", strSessCompanyLocale));
    templateUtil.setTemplateName("GmPOReport.vm");
    templateUtil.setTemplateSubDir("operations/templates");
    log.debug("::AlReturn List::" + templateUtil.getDataList("alReturn"));
    return templateUtil.generateOutput();
  }
  /**
   * getPOXmlGridData - This method is used to show the PO reports using VM file.
   * 
   * @parameter HashMap
   * @return String
   * @exception AppError
   **/
  public String getPOXmlGridData(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataList("alReturn", (ArrayList) hmParam.get("POREPORT"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.GmPOReportByPart", strSessCompanyLocale));
    templateUtil.setTemplateName("GmPOPartReport.vm");
    templateUtil.setTemplateSubDir("operations/templates");

    return templateUtil.generateOutput();
  }
  /**
   * initiateBatchPOProcess - This method is used to Raise the Po for around 400 parts.
   * 
   * @return HashMap
   * @exception AppError
   **/
  public HashMap initiateBatchPOProcess(String strVendorID, String strPoType, String strPartBatch,
      String strUserName) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_purchasing.gm_sav_initiate_po", 6);

    gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
    gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);

    gmDBManager.setString(1, strVendorID);
    gmDBManager.setString(2, strPoType);
    gmDBManager.setString(3, strPartBatch);
    gmDBManager.setString(4, strUserName);

    gmDBManager.execute();
    String strPOId = gmDBManager.getString(5);
    String strMessage = GmCommonClass.parseNull(gmDBManager.getString(6));
    hmReturn.put("POID", strPOId);
    hmReturn.put("MSG", strMessage);

    if (!strMessage.equals("")) {
      gmDBManager.close();
      throw new AppError("" + strMessage + "", "", 'S');
    } else {
      gmDBManager.commit();
    }



    return hmReturn;

  }

  /**
   * fetchPODetail - This method is used to fetch the PO details.
   * 
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList fetchPODetail(String strPOId) throws AppError {
    ArrayList alPODtl = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_purchasing.gm_fch_po_details", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPOId);
    gmDBManager.execute();
    alPODtl = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alPODtl;
  }

  /**
   * loadVendorDetails - This method is used to reload the values based on vendor id for Batch PO .
   * 
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadVendorDetails(String strVendorID) throws AppError {
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT    C301_VENDOR_NAME VNAME, C301_VENDOR_SH_NAME VSHNAME, C301_LOT_CODE LCODE, ");
    sbQuery
        .append(" C901_STATUS_ID STATUS, C301_CONTACT_PERSON CPERSON, C301_BILL_ADD1 ADD1, GET_CODE_NAME(GET_VENDOR_CURRENCY(");
    sbQuery.append(strVendorID);
    sbQuery.append(")) CURR, ");
    sbQuery
        .append(" C301_BILL_ADD2 ADD2, C301_BILL_CITY CITY, C301_BILL_STATE STATE, C301_BILL_COUNTRY COUNTRY, ");
    sbQuery
        .append(" C301_BILL_ZIP ZIP, C301_PAYMENT_TERMS PAYMENT, C301_PHONE PHONE, C301_FAX FAX, C301_ACTIVE_FL AFLAG,  C301_SEC199_FL SEC199, ");
    sbQuery.append(" C301_RM_INVOICE_FL RMINVOICE , C301_RM_PERCENTAGE SPLITPERCENT ");
    sbQuery.append(" FROM T301_VENDOR");
    sbQuery.append(" WHERE c301_vendor_id = ");
    sbQuery.append(strVendorID);
    HashMap hmResult = gmDBManager.querySingleRecord(sbQuery.toString());


    log.debug("sbQuery ::: " + sbQuery);

    return hmResult;

  }

  /**
   * validateProdRequestID - This method is used to validate the request id raised based on the
   * company id.
   * 
   * @param String strReqId,String strRequestTxnType
   * @exception AppError
   * @return void
   */
  public void validatePOID(String strPoId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_purchasing.gm_validate_po", 1);
    gmDBManager.setString(1, strPoId);
    gmDBManager.execute();
    gmDBManager.close();
  }

  /**
   * validatePOPlant - This method is used to validate the PO based on the plant.
   * 
   * @param String strPOId
   * @exception AppError
   * @return void
   */
  public void validatePOPlant(String strPOId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_purchasing.gm_validate_po_plant", 1);
    gmDBManager.setString(1, strPOId);
    gmDBManager.execute();
    gmDBManager.close();
  }

  /**
   * validateProductPlant - This method is used to validate the PO based on the plant while
   * initiating the PO.
   * 
   * @param String strProjId,strWOString
   * @exception AppError
   * @return void
   */
  public void validateProductPlant(String strProjId, String strWOString) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_op_purchasing.gm_validate_product_plant", 2);
    gmDBManager.setString(1, strProjId);
    gmDBManager.setString(2, strWOString);
    gmDBManager.execute();
    gmDBManager.close();
  }


  /**
   * validatePOProdPlant - This method is used to validate the PO based on the plant while
   * inititating the PO from Ajax Call.
   * 
   * @param String strProjId,strWOString
   * @exception AppError
   * @return String strErrorMsg
   */
  public String validatePOProdPlant(String strProjId, String strWOString) throws AppError {
    String strErrorMsg = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_op_purchasing.gm_validate_product_plant", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strProjId);
    gmDBManager.setString(2, strWOString);
    gmDBManager.execute();
    strErrorMsg = GmCommonClass.parseNull(gmDBManager.getString(3));
    gmDBManager.close();
    return strErrorMsg;
  }
  
  /**
   * updateWOPartDetails - This method is used to getting Work order details ,part number to reload part number details
   * @param String strInputStr,strUserId
   * @exception AppError
   * @return void
   */
  public void updateWOPartDetails(String strInputStr, String strUserId) throws AppError {
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("GM_PKG_WO_TXN.gm_upd_wo_part_info", 2);
	    gmDBManager.setString(1, strInputStr);
	    gmDBManager.setString(2, strUserId);
	    gmDBManager.execute();
	    gmDBManager.commit();
	    gmDBManager.close();
	  }

  public String getPOPendVenGridData(HashMap hmParam) throws AppError {
	    GmTemplateUtil templateUtil = new GmTemplateUtil();
	    String strSessCompanyLocale =
	        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
	    templateUtil.setDataList("alReturn", (ArrayList) hmParam.get("alReturn"));
	    templateUtil.setDataMap("hmParam", hmParam);
	    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
	        "properties.labels.operations.GmPOReportPendVendor", strSessCompanyLocale));
	    templateUtil.setTemplateName("GmPOReportPendVendor.vm");
	    templateUtil.setTemplateSubDir("operations/templates");
	    log.debug("::AlReturn List::" + templateUtil.getDataList("alReturn"));
	    return templateUtil.generateOutput();
	  }
  
  /**
   * validateDR - This is method used DR Id passed get PO Details
   * @param strDRId
   * @return strPOid
   */
   public String validateDR(String strDRId){
	      String strPOid = "";
	      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	      gmDBManager.setPrepareString("gm_pkg_op_purchasing.gm_validate_dr_id", 2);
		  gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
		  gmDBManager.setString(1, strDRId);
		  gmDBManager.execute();
		  strPOid = GmCommonClass.parseNull(gmDBManager.getString(2));
	      gmDBManager.close();
	  return strPOid;
  }
   
   /**
    * fetchAutoPublishFl - This is method used to fetch AutoPublish Flag for the vendor
    * @param strVendorId
    * @return strPOid
    */
    public String fetchAutoPublishFl(String strPoId, String strVendorId){
 	      String strAutoPublish = "";
 	      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
 	      gmDBManager.setFunctionString("gm_pkg_op_purchasing.get_auto_publish_fl", 2);
 		  gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
 		  gmDBManager.setString(2, strPoId);
 		  gmDBManager.setString(3, strVendorId);
 		  gmDBManager.execute();
 		 strAutoPublish = GmCommonClass.parseNull(gmDBManager.getString(1));
 		 log.debug(" strAutoPublish==>" + strAutoPublish);
 	      gmDBManager.close();
 	  return strAutoPublish;
   }
  
}// end of class GmPurchaseBean
