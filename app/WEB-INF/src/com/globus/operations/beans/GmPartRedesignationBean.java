/**
 * 
 */
package com.globus.operations.beans;

import java.sql.ResultSet;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author arajan
 * 
 */
public class GmPartRedesignationBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j

  public GmPartRedesignationBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

	public GmPartRedesignationBean(GmDataStoreVO gmDataStoreVO) {
	    super(gmDataStoreVO);
	 }
	
	/**
     * getPartRedesignationDtls - Getting the part details to the screen
     * @param HashMap hmParam  
     * @return HashMap
     * @exception AppError
    **/ 
	public HashMap getPartRedesignationDtls(HashMap hmParam) throws AppError{
	    ArrayList alCtrlDtls = new ArrayList();
	    ArrayList alPartsList = new ArrayList();
	    ArrayList alFromPart = new ArrayList();
	    HashMap hmReturn = new HashMap();
	    String strPartId = GmCommonClass.parseNull((String) hmParam.get("HSCANNEDPARTSTR"));
	    String strDonorId = GmCommonClass.parseNull((String) hmParam.get("HSCANNEDDONORSTR"));
	    String strCtrlNumStr = GmCommonClass.parseNull((String) hmParam.get("HSCANNEDLOTSTR"));
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_part_redesignation.gm_fch_part_redesignation", 4);
	    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
	    /* gmDBManager.registerOutParameter(4, OracleTypes.CURSOR); */
	    gmDBManager.setString(1, strPartId);
	    gmDBManager.setString(2, strDonorId);
	    gmDBManager.setString(4, strCtrlNumStr);
	    gmDBManager.execute();
	    alCtrlDtls = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
	    // alPartsList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
	    hmReturn.put("CTRLDTLS", alCtrlDtls);
	    /* hmReturn.put("PARTLIST", alPartsList); */
	    gmDBManager.close();
	    return hmReturn;
	  }

	
	public HashMap saveDonorNumPTRD(String strTransId, String strUserId) throws AppError{
		ArrayList alTransDtls = new ArrayList();
		String strContrlNum="";
		HashMap hmReturn = new HashMap();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());          
		gmDBManager.setPrepareString("gm_pkg_part_redesignation.gm_sav_donor_part_redesignation",2);
		gmDBManager.registerOutParameter(2, OracleTypes.CHAR);
		gmDBManager.setString(1, strTransId);
		gmDBManager.execute();
		strContrlNum = GmCommonClass.parseNull(gmDBManager.getString(2));
	    gmDBManager.commit();
		hmReturn.put("TRANSDTLS", strContrlNum);
		gmDBManager.close();
		return hmReturn;
}
	
	/**
     * savePartRedesignationDtls - Save the part details
     * @param HashMap hmParam  
     * @return HashMap
     * @exception AppError
	 * @throws ParseException 
    **/ 
	public String savePartRedesignationDtls(HashMap hmParam) throws AppError, Exception{
		ArrayList alCtrlDtls = new ArrayList();
		ArrayList alPartsList = new ArrayList();
		HashMap hmReturn = new HashMap();
		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		 
		String strPartNum 		  = GmCommonClass.parseNull((String)hmParam.get("PARTID"));
		String strInputString 	  = GmCommonClass.parseNull((String)hmParam.get("HINPUTSTRING"));
		String strCntNumInvString = GmCommonClass.parseNull((String)hmParam.get("HCTLNUMINVSTRING"));
		String strUserId 		  = GmCommonClass.parseNull((String)hmParam.get("USERID"));
        String strTxnId           = "";
		String strLog             = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		GmDBManager gmDBManager   = new GmDBManager(getGmDataStoreVO());   
		
		//PMT-36506-Converting String to Date
		String stgDt              = GmCommonClass.parseNull((String) hmParam.get("STGDT")); 
		String strDateFormat      =  getGmDataStoreVO().getCmpdfmt();
	    Date dtStagedate          = GmCommonClass.getStringToDate(stgDt,strDateFormat);
		  
        gmDBManager.setPrepareString("gm_pkg_part_redesignation.gm_sav_part_redesignation", 6);
        gmDBManager.registerOutParameter(6, OracleTypes.CHAR);
        gmDBManager.setString(1, strPartNum);
        gmDBManager.setString(2, strInputString);
        gmDBManager.setString(3, strCntNumInvString);
        gmDBManager.setString(4, strUserId);    
        gmDBManager.setDate(5, dtStagedate);    

    gmDBManager.execute();
    strTxnId = GmCommonClass.parseNull(gmDBManager.getString(6));
    if (!strLog.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strTxnId, strLog, strUserId, "103931");
    }

    gmDBManager.commit();
    return strTxnId;
  }

  /**
   * getTransDetails - Method to Fetch the Header and Cart Details for the PTRD Pic Slip
   * 
   * @param HashMap hmParam
   * @return HashMap
   * @exception AppError
   **/
  public HashMap getTransDetails(HashMap hmParam) throws AppError {
    HashMap hmHeaderDetails = new HashMap();
    ArrayList alCartDetails = new ArrayList();
    ArrayList alFromPart = new ArrayList();
    HashMap hmReturn = new HashMap();
    String StrTransId = GmCommonClass.parseNull((String) hmParam.get("TRANSID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    StringBuffer sbCartQuery = new StringBuffer();
    String strCmpDtFmt = GmCommonClass.parseNull(getCompDateFmt());
    String strTimeDateFOrmat = strCmpDtFmt + " HH:mm:ss";

    sbQuery
        .append(" SELECT c412_inhouse_trans_id ID , GET_CODE_NAME(c412_type) TYPE , GET_CODE_NAME(c412_inhouse_purpose) PURPOSE ");
    sbQuery
        .append(" , c412_created_date CDATE , c412_comments TRANSLOG , get_user_name(c412_created_by) CBY , c412_type TRANSTYPE ");
    sbQuery.append("  FROM t412_inhouse_transactions  ");
    sbQuery.append(" WHERE c412_inhouse_trans_id = '");
    sbQuery.append(StrTransId);
    sbQuery.append("' AND c412_void_fl IS NULL");
    // log.debug("The Header Details Query is ****** "+sbQuery.toString());
    hmHeaderDetails = gmDBManager.querySingleRecord(sbQuery.toString());

    sbCartQuery
        .append(" SELECT t413.c205_part_number_id PRODUCTNO , GET_PARTNUM_DESC(t413.c205_part_number_id) PRODDESCRPTION ");
    sbCartQuery.append(" , TO_CHAR(t2550.c2550_expiry_date,'" + strCmpDtFmt
        + "') PRODEXPIRY , T413.C413_Control_Number ALLOGRAFTNO ");
    sbCartQuery
        .append(" , GET_PART_SIZE(t413.c205_part_number_id,t2550.c2550_control_number)  PRODSIZE ");
    sbCartQuery
        .append(" , T2550.C2540_Donor_Number PRODDONOR , T413.C413_Item_Qty IQTY , T413.C413_Item_Price PRICE , T413.C205_Client_Part_Number_Id CLIENTPART ");
    sbCartQuery
        .append("   FROM (SELECT t2550.c205_part_number_id  ,t2550.c2550_control_number ,t2550.c2550_expiry_date ,t2550.c2550_custom_size ,t2540.c2540_donor_number ");
    sbCartQuery
        .append(" , t2550.c2540_donor_id  FROM t2550_part_control_number t2550 ,t2540_donor_master t2540 ");
    sbCartQuery.append("   WHERE NVL(t2550.c2540_donor_id,-999) = t2540.c2540_donor_id (+) ");
    sbCartQuery.append("   AND T2550.C205_Part_Number_Id In ( Select t413.C205_Part_Number_Id ");
    sbCartQuery
        .append("   FROM T413_Inhouse_Trans_Items t413  where  t413.C412_Inhouse_Trans_Id = '"
            + StrTransId + "')) t2550,T413_Inhouse_Trans_Items t413 ");
    sbCartQuery.append("   WHERE t413.C412_Inhouse_Trans_Id = '" + StrTransId
        + "' AND t413.c205_part_number_id  = t2550.c205_part_number_id (+) ");
    sbCartQuery
        .append("   AND T413.C413_Control_Number = T2550.C2550_Control_Number (+)  AND T413.c413_void_fl IS NULL ORDER BY PRODUCTNO , ALLOGRAFTNO");
    // log.debug("The Part Details Query is ****** "+sbCartQuery.toString());
    alCartDetails = gmDBManager.queryMultipleRecords(sbCartQuery.toString());
    hmReturn.put("CARTDETAILS", alCartDetails);
    hmReturn.put("HEADERDATA", hmHeaderDetails);
    return hmReturn;
  }

  /**
   * validateToPart - Method to validate 'To Part' number
   * 
   * @param String String
   * @return String
   * @exception AppError
   **/
  public String validateToPart(String strFromPart, String strToPart) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_part_redesignation.get_topart_num_valid_cnt", 2);
    gmDBManager.registerOutParameter(1, OracleTypes.NUMBER);
    gmDBManager.setString(2, strFromPart);
    gmDBManager.setString(3, strToPart);
    gmDBManager.execute();
    String strValidFl = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    return strValidFl;
  }

}
