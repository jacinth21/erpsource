package com.globus.operations.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmDHRBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  GmCommonClass gmCommon = new GmCommonClass();

  public GmDHRBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmDHRBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public ArrayList fetchControlNumberList(HashMap hmParams) throws AppError {
    StringBuilder sbQuery = new StringBuilder();
    GmDBManager db = new GmDBManager(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();
    String strPartNumber = GmCommonClass.parseNull((String) hmParams.get("PARTNUMBER"));
    String strPartDesc = GmCommonClass.parseNull((String) hmParams.get("PARTDESC"));
    String strFromDt = GmCommonClass.parseNull((String) hmParams.get("FROMDT"));
    String strToDt = GmCommonClass.parseNull((String) hmParams.get("TODT"));
    String strCtrlNM = GmCommonClass.parseNull((String) hmParams.get("CONTROLNM"));
    String strCompDateFormat = getCompDateFmt();

    sbQuery.append(" SELECT t2550.c2550_part_control_number_id id,");
    sbQuery.append(" t2550.c205_part_number_id partid,");
    sbQuery.append(" GET_PARTNUM_DESC (t2550.c205_part_number_id) pdesc,");
    sbQuery.append(" t2550.c2550_control_number controlid,");
    sbQuery.append(" t2550.C2550_REV_LEVEL rev,");
    sbQuery.append("to_char(t2550.C2550_EXPIRY_DATE,'");
    sbQuery.append(strCompDateFormat);
    sbQuery.append("') expirydt,");
    sbQuery.append(" GET_USER_NAME (t2550.c2550_last_updated_by) createdby,");
    sbQuery.append(" TO_CHAR (t2550.c2550_last_updated_date,'" + strCompDateFormat
        + "') createddate,");
    sbQuery.append(" GET_LOG_FLAG (NVL (t2550.c2550_part_control_number_id, ''), 92092) LOGFLG,");
    sbQuery.append(" t205.c205_product_material prodtype ");
    sbQuery.append("  ,t2550.c301_vendor_id vendorid ");
    sbQuery.append(" ,t2540.c2540_donor_number donornm ");
    sbQuery
        .append(" FROM t2550_part_control_number t2550 ,t205_part_number t205 ,t2540_donor_master t2540");
    sbQuery.append(" WHERE  t2550.c205_part_number_id IN (");
    sbQuery.append(" SELECT c205_part_number_id ");
    sbQuery.append(" FROM t205_part_number ");
    sbQuery.append(" WHERE c205_part_number_id LIKE ");
    sbQuery.append(" '%'  || NVL ('" + strPartNumber + "', c205_part_number_id) || '%' ");
    sbQuery.append(" AND LOWER (c205_part_num_desc) LIKE ");
    sbQuery.append(" '%' || NVL (LOWER ('" + strPartDesc
        + "'), LOWER (c205_part_num_desc)) || '%') ");
    sbQuery.append(" AND t205.c205_part_number_id = t2550.c205_part_number_id ");
    sbQuery.append(" AND t2550.c2540_donor_id = t2540.c2540_donor_id(+) ");
    // Lot Number Filter
    sbQuery.append(" AND c2550_control_number LIKE ");
    sbQuery.append(" '%'  || NVL ('" + strCtrlNM + "', c2550_control_number) || '%' ");
    // From/To Dates Filter
    if (!strFromDt.equals("")) {
      sbQuery.append(" AND t2550.C2550_MFG_DATE >= TO_DATE ('" + strFromDt + "', '"
          + strCompDateFormat + "')   ");
    }
    if (!strToDt.equals("")) {
      sbQuery.append("  AND trunc(t2550.C2550_MFG_DATE) <= TO_DATE ('" + strToDt + "', '"
          + strCompDateFormat + "')   ");
    }
    sbQuery.append(" ORDER BY t2550.c2550_last_updated_date DESC ");
    db.setPrepareString(sbQuery.toString());
    log.debug(" Query for fetchControlNumberList: " + sbQuery.toString());
    alReturn = db.queryMultipleRecord();
    return alReturn;
  }

  public String saveControlNumberDetails(HashMap hmParam) throws AppError {
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strCtrlAsd_ID = GmCommonClass.parseNull((String) hmParam.get("CTRLASD_ID"));
    String strPartNM = GmCommonClass.parseNull((String) hmParam.get("PARTNM"));
    String strCtrlNM = GmCommonClass.parseNull((String) hmParam.get("CONTROLNM"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_controlnumber.gm_sav_control_num_detail", 4);
    gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
    gmDBManager.setString(1, strCtrlAsd_ID);
    gmDBManager.setString(2, strPartNM);
    gmDBManager.setString(3, strCtrlNM);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();
    strCtrlAsd_ID = GmCommonClass.parseNull(gmDBManager.getString(1));
    if (!strLogReason.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strCtrlAsd_ID, strLogReason, strUserId, "92092");
    }
    gmDBManager.commit();
    return strCtrlAsd_ID;
  }

  public HashMap fetchControlNumberDetails(HashMap hmParam) throws AppError {
    HashMap hmResult = new HashMap();
    String strCtrlAsd_ID = GmCommonClass.parseNull((String) hmParam.get("CTRLASD_ID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_controlnumber.gm_fch_control_num_detail", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strCtrlAsd_ID);
    gmDBManager.execute();
    hmResult = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmResult;
  }

  public String checkLockFlag(HashMap hmParam) throws AppError {
    String strCtrlAsd_ID = GmCommonClass.parseNull((String) hmParam.get("CTRLASD_ID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_controlnumber.gm_chk_lock_fl", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CHAR);
    gmDBManager.setString(1, strCtrlAsd_ID);
    gmDBManager.execute();
    String strReturn = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.close();
    return strReturn;
  }

  public ArrayList fetchSplitLocations(HashMap hmParams) throws AppError {
    GmDBManager db = new GmDBManager(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();
    String strDhrId = GmCommonClass.parseNull((String) hmParams.get("DHRID"));
    String strLocationGrp = GmCommonClass.parseNull((String) hmParams.get("LOCGRP"));
    db.setPrepareString("gm_pkg_op_dhr.gm_fch_split_locations", 3);
    db.registerOutParameter(3, OracleTypes.CURSOR);
    db.setString(1, strDhrId);
    db.setString(2, strLocationGrp);
    db.execute();
    alReturn = db.returnArrayList((ResultSet) db.getObject(3));
    db.close();
    return alReturn;
  }

  /**
   * @return ArrayList
   * @throws AppError This method is used to load the Voided DHR report.
   * */
  public ArrayList fetchVoidedDHRReprot(HashMap hmParam) throws AppError {
    log.debug("Enter into fetchVoidedDHRReprot in GmDHRBean");
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    ArrayList alVoidedReport = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    String strDHRId = GmCommonClass.parseNull((String) hmParam.get("DHRID"));
    String strPoId = GmCommonClass.parseNull((String) hmParam.get("POID"));
    String strInvId = GmCommonClass.parseNull((String) hmParam.get("INVID"));
    String strPnum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
    String strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
    String strAppDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));

    sbQuery
        .append(" SELECT GET_VENDOR_NAME (T408.C301_VENDOR_ID) VNAME, T408.C205_PART_NUMBER_ID PNUM, GET_PARTNUM_DESC (T408.C205_PART_NUMBER_ID) PDESC, ");
    sbQuery
        .append(" T408.C408_DHR_ID DHRID , T408.C401_PURCHASE_ORD_ID POID , T408.C408_QTY_RECEIVED QTY, T408.C408_CONTROL_NUMBER CNUM, ");
    sbQuery
        .append(" T406.C406_INVOICE_ID INVID , T406.C406_INVOICE_DT INVDT, T408.C408_RECEIVED_DATE RECEIVEDDT, ");
    sbQuery
        .append(" get_user_name (T408.C408_LAST_UPDATED_by) VOIDEDBY , T408.C408_LAST_UPDATED_DATE VOIDEDDT, GET_LATEST_LOG_COMMENTS(T408.C408_DHR_ID,1205) COMMENTS");
    sbQuery
        .append(" FROM T408_DHR T408, T406_PAYMENT T406  WHERE T408.C406_OLD_PAYMENT_ID = T406.C406_PAYMENT_ID(+) AND T408.C408_VOID_FL = 'Y' AND T408.C408_LAST_UPDATED_DATE IS NOT NULL");

    if (!strDHRId.equals("")) {// Filter based on DHR Id.
      sbQuery.append(" AND T408.C408_DHR_ID like upper('%");
      sbQuery.append(strDHRId);
      sbQuery.append("%')");
    }
    if (!strPoId.equals("")) {// Filter based on PO Id.
      sbQuery.append(" AND T408.C401_PURCHASE_ORD_ID like upper('%");
      sbQuery.append(strPoId);
      sbQuery.append("%')");
    }
    if (!strInvId.equals("")) {// Filter based on Invoice Id.
      sbQuery.append(" AND T406.C406_INVOICE_ID ='");
      sbQuery.append(strInvId);
      sbQuery.append("'");
    }
    if (!strPnum.equals("")) {// Filter based on Part Number.
      sbQuery.append(" AND T408.C205_PART_NUMBER_ID ='");
      sbQuery.append(strPnum);
      sbQuery.append("'");
    }
    if (!strVendorId.equals("0") && !strVendorId.equals("")) {// Filter based on Vendor ID.
      sbQuery.append(" AND T408.C301_VENDOR_ID= '");
      sbQuery.append(strVendorId);
      sbQuery.append("'");

    }
    if (!strFromDate.equals("")) {// Filter Based on From Date.
      sbQuery.append(" AND TRUNC(T408.C408_LAST_UPDATED_DATE) >= TO_DATE('");
      sbQuery.append(strFromDate);
      sbQuery.append("' , '");
      sbQuery.append(strAppDateFmt);
      sbQuery.append("' )");
    }
    if (!strToDate.equals("")) {// Filter Based on To Date.
      sbQuery.append(" AND TRUNC(T408.C408_LAST_UPDATED_DATE) <= TO_DATE('");
      sbQuery.append(strToDate);
      sbQuery.append("' , '");
      sbQuery.append(strAppDateFmt);
      sbQuery.append("' )");
    }
    sbQuery.append(" AND  T408.C1900_COMPANY_ID ='" + getCompId() + "'");
    sbQuery.append(" AND T408.C5040_PLANT_ID = '" + getGmDataStoreVO().getPlantid() + "'");
    sbQuery.append(" ORDER BY VOIDEDDT DESC");
    log.debug("Query for getting USer List " + sbQuery.toString());
    alVoidedReport = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alVoidedReport;
  }

  /**
   * @return String
   * @throws AppError This method is used to save the transaction id to skip exp validation.
   * */
  public String saveCnumSkipExpiryDate(HashMap hmParam) throws AppError {

    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strCtrlAsd_ID = GmCommonClass.parseNull((String) hmParam.get("CTRLASD_ID"));
    String strTxnId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strPartNM = GmCommonClass.parseNull((String) hmParam.get("PARTNM")); // PARTNM
    String strPart = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
    String strCtrlNM = GmCommonClass.parseNull((String) hmParam.get("CONTROLNM"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    String strTranLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    strPartNM = strPartNM.equals("") ? strPart : strPartNM;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_controlnumber.gm_sav_skip_cnum_expiry_date", 5);
    gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);
    gmDBManager.setString(1, strPartNM);
    gmDBManager.setString(2, strCtrlNM);
    gmDBManager.setString(3, strUserId);
    gmDBManager.setString(4, strTxnId);
    gmDBManager.execute();
    strCtrlAsd_ID = GmCommonClass.parseNull(gmDBManager.getString(5));
    if (!strLogReason.equals("")) {
      strLogReason =
          strLogReason + ". For the transaction " + strTxnId + " part " + strPartNM
              + " and control number " + strCtrlNM;
      gmCommonBean.saveLog(gmDBManager, strCtrlAsd_ID, strLogReason, strUserId, "92092");
      strTranLogReason =
          " One month lot expiry validation skipped for the transaction " + strTxnId + " part "
              + strPartNM + " and control number " + strCtrlNM + ". Reason:" + strTranLogReason;
      gmCommonBean.saveLog(gmDBManager, strTxnId, strTranLogReason, strUserId, "4000317");
    }
    gmDBManager.commit();
    return strCtrlAsd_ID;
  }
}
