/*
 * Module: GmVendorBean.java Author: Dhinakaran James Project: Globus Medical App Date-Written: 11
 * Mar 2004 Security: Unclassified Description: Testing Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.operations.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
// import org.apache.struts.action.ActionForm;


import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

// import com.globus.common.formbeans.GmCommonContactForm;

public class GmVendorBean extends GmBean {

  GmCommonClass gmCommon = new GmCommonClass();


  public GmVendorBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmVendorBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }


  ArrayList alType = new ArrayList();
  ArrayList alState = new ArrayList();
  ArrayList alCountry = new ArrayList();
  ArrayList alPay = new ArrayList();
  ArrayList alCurrency = new ArrayList();
  ArrayList alVendor = new ArrayList();
  ArrayList alCostType = new ArrayList();
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  /**
   * loadVendor - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadVendor() throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmTemp = new HashMap();
    hmTemp.put("CHECKACTIVEFL", "N"); // check for active flag status in vendor list

    ArrayList alStatus = new ArrayList();

    alStatus = gmCommon.getCodeList("VENDS");
    alState = gmCommon.getCodeList("STATE", getGmDataStoreVO());
    alCountry = gmCommon.getCodeList("CNTY");
    alPay = gmCommon.getCodeList("PAY");
    alCurrency = gmCommon.getCodeList("CURRN");
    alCostType =gmCommon.getCodeList("VENCTP");
    hmResult = getVendorList(hmTemp);

    hmReturn.put("VENDORLIST", hmResult.get("VENDORLIST"));
    hmReturn.put("STATUS", alStatus);
    hmReturn.put("STATE", alState);
    hmReturn.put("COUNTRY", alCountry);
    hmReturn.put("PAYMENT", alPay);
    hmReturn.put("CURRENCY", alCurrency);
    hmReturn.put("COSTTYPE", alCostType);


    return hmReturn;
  } // End of loadVendor

  /**
   * loadVendor - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadEditVendor(String strId) throws AppError {
	  
    HashMap hmReturn = new HashMap();
    ArrayList alStatus = new ArrayList();
    HashMap hmTemp = new HashMap();
    hmTemp.put("CHECKACTIVEFL", "N");

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT	C301_VENDOR_NAME VNAME, C301_VENDOR_SH_NAME VSHNAME, C301_LOT_CODE LCODE, ");
    sbQuery
        .append(" C901_STATUS_ID STATUS, C301_CONTACT_PERSON CPERSON, C301_BILL_ADD1 ADD1, C301_CURRENCY CURR, ");
    sbQuery
        .append(" C301_BILL_ADD2 ADD2, C301_BILL_CITY CITY, C301_BILL_STATE STATE, C301_BILL_COUNTRY COUNTRY, ");
    sbQuery
        .append(" C301_BILL_ZIP ZIP, C301_PAYMENT_TERMS PAYMENT, C301_PHONE PHONE, C301_FAX FAX, C301_ACTIVE_FL AFLAG,  C301_SEC199_FL SEC199, ");
    sbQuery.append(" C301_RM_INVOICE_FL RMINVOICE , C301_RM_PERCENTAGE SPLITPERCENT,c901_vendor_cost_type COSTTYPE,C301_VENDOR_PORTAL_FL VENDORPORTALMAP,C101_PARTY_ID VENDORID, c301_auto_publish_fl AUTOPUBLISHFL");
	//PMT-50795 : Capture Purchasing Agent - Vendor Setup screen
	sbQuery.append(" ,C301_PURCHASING_AGENT_ID PUR_AGENT,C301_EMAIL_ID EMAIL ");
    sbQuery.append(" FROM T301_VENDOR");
    sbQuery.append(" WHERE C101_PARTY_ID = ");
    sbQuery.append(strId);
    HashMap hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    hmReturn.put("VENDORDETAILS", hmResult);

    alStatus = gmCommon.getCodeList("VENDS");
    alState = gmCommon.getCodeList("STATE", getGmDataStoreVO());
    alCountry = gmCommon.getCodeList("CNTY");
    alPay = gmCommon.getCodeList("PAY");
    alCurrency = gmCommon.getCodeList("CURRN");
    alCostType =gmCommon.getCodeList("VENCTP");
    hmResult = getVendorList(hmTemp);

    hmReturn.put("VENDORLIST", hmResult.get("VENDORLIST"));
    hmReturn.put("STATUS", alStatus);
    hmReturn.put("STATE", alState);
    hmReturn.put("COUNTRY", alCountry);
    hmReturn.put("PAYMENT", alPay);
    hmReturn.put("CURRENCY", alCurrency);
    hmReturn.put("COSTTYPE", alCostType);
    

    return hmReturn;
  } // End of loadEditVendor

  /**
   * saveVendor - This method will
   * 
   * @param HashMap hmParam
   * @param String strUsername
   * @param String strAction
   * @return HashMap
   * @exception AppError
   **/
  public String saveVendor(HashMap hmParam, String strUsername, String strAction) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCom = new GmCommonBean(getGmDataStoreVO());


    HashMap hmReturn = new HashMap();
    String strReturnId = "";

    String strVendorNm = GmCommonClass.parseNull((String) hmParam.get("VNAME"));
    String strVendorShNm = GmCommonClass.parseNull((String) hmParam.get("VSHNAME"));
    String strLotCode = GmCommonClass.parseNull((String) hmParam.get("LCODE"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strContactPerson = GmCommonClass.parseNull((String) hmParam.get("CPERSON"));
    String strAddress1 = GmCommonClass.parseNull((String) hmParam.get("ADD1"));
    String strAddress2 = GmCommonClass.parseNull((String) hmParam.get("ADD2"));
    String strCity = GmCommonClass.parseNull((String) hmParam.get("CITY"));
    String strState = GmCommonClass.parseNull((String) hmParam.get("STATE"));
    String strCountry = GmCommonClass.parseNull((String) hmParam.get("COUNTRY"));
    String strPinCode = GmCommonClass.parseNull((String) hmParam.get("PIN"));
    String strPayment = GmCommonClass.parseNull((String) hmParam.get("PAYMENT"));
    String strPhone = GmCommonClass.parseNull((String) hmParam.get("PHONE"));
    String strFax = GmCommonClass.parseNull((String) hmParam.get("FAX"));
    String strActiveFl = GmCommonClass.parseNull((String) hmParam.get("AFLAG"));
    String strPartyId = GmCommonClass.parseZero((String) hmParam.get("VID"));
    String strCurrency = GmCommonClass.parseNull((String) hmParam.get("CURR"));
    String strLog = GmCommonClass.parseNull((String) hmParam.get("LOG"));

    String strSec199FL = GmCommonClass.parseNull((String) hmParam.get("SEC199"));
    String strPercentage = GmCommonClass.parseNull((String) hmParam.get("SPLITPERCENT"));
    String strRMInvoiceFL = GmCommonClass.parseNull((String) hmParam.get("RMINVOICE"));
    String strVendorCostType = GmCommonClass.parseNull((String)hmParam.get("VENDOR_COST_TYPE"));
    String strVendorAccess = GmCommonClass.parseNull((String) hmParam.get("VENDOR_PORT_ACC"));
    String strAutoPublishFl = GmCommonClass.parseNull((String) hmParam.get("AUTO_PUBLISH_FL"));
	//PMT-50795-->Capture Purchasing Agent - Vendor Setup screen
	String strPurchasingAgent = GmCommonClass.parseNull((String) hmParam.get("PURCHASING_AGENT"));
	String strEmail = GmCommonClass.parseNull((String) hmParam.get("EMAIL"));
    
    gmDBManager.setPrepareString("GM_SAVE_VENDOR", 28);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(28, java.sql.Types.CHAR);
    gmDBManager.setString(1, strVendorNm);
    gmDBManager.setString(2, strVendorShNm);
    gmDBManager.setString(3, strLotCode);
    gmDBManager.setInt(4, Integer.parseInt(strStatus));
    gmDBManager.setString(5, strContactPerson);
    gmDBManager.setString(6, strAddress1);
    gmDBManager.setString(7, strAddress2);
    gmDBManager.setString(8, strCity);
    gmDBManager.setInt(9, Integer.parseInt(strState));
    gmDBManager.setInt(10, Integer.parseInt(strCountry));
    gmDBManager.setString(11, strPinCode);
    gmDBManager.setInt(12, Integer.parseInt(strPayment));
    gmDBManager.setString(13, strPhone);
    gmDBManager.setString(14, strFax);
    gmDBManager.setString(15, strActiveFl);
    gmDBManager.setInt(16, Integer.parseInt(strUsername));
    gmDBManager.setInt(17, Integer.parseInt(strCurrency));
    gmDBManager.setString(18, strAction);
    gmDBManager.setInt(19, Integer.parseInt(strPartyId));
    gmDBManager.setString(20, strSec199FL);
    gmDBManager.setString(21, strPercentage);
    gmDBManager.setString(22, strRMInvoiceFL);
    gmDBManager.setString(23, strVendorCostType);
    gmDBManager.setString(24, strVendorAccess);
    gmDBManager.setString(25, strAutoPublishFl);
	gmDBManager.setString(26, strPurchasingAgent);
	gmDBManager.setString(27, strEmail);

    gmDBManager.execute();
    
    // PC-5767  Vendor name redis refresh is not happening for DHR Report -By vendor
    GmCacheManager gmCacheManager = new GmCacheManager();
    String strKey = "VENDOR_LIST:" + gmCom.getCompId() + ":AUTO"; //VENDOR_LIST:1000:AUTO
    //form the redis key dynamically on the basis of company id, sample key:VENDOR_LIST:1000:AUTO
    log.debug("strKey VENDOR_LIST ===================>"+strKey);
    gmCacheManager.removeKey(strKey); //remove the cache manager key  



    strReturnId = String.valueOf(gmDBManager.getInt(28));
  
    if (!strLog.equals("")) {

      gmCom.saveLog(gmDBManager, strReturnId, strLog, strUsername, "1217");


      // (Connection conn, String strId, String strComments, String strUserID, String strType)
    }

    gmDBManager.commit();

    return strReturnId;

  }


  /**
   * getVendorPriceList - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap getVendorPriceList(String strPartNum,String strVendorCostType) throws AppError {
    ArrayList alReturn = new ArrayList();
//    ArrayList alReturn1 = new ArrayList();
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    // Joined vendor table to fetch vendor price details by vendor cost type
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT	t405.C405_PRICING_ID PID, t405.C301_VENDOR_ID VID, GET_VENDOR_NAME(t405.C301_VENDOR_ID) ||' - ('|| GET_CODE_NAME(GET_VENDOR_CURRENCY(t405.C301_VENDOR_ID)) || ')'  VNM, ");
    sbQuery.append(" t405.C205_PART_NUMBER_ID PNUM, t405.C405_COST_PRICE CPRICE, t405.C405_ACTIVE_FL AFL, ");
    sbQuery.append(" t405.C405_QTY_QUOTED QTYQ, t405.C405_DELIVERY_FRAME DFRAME, t405.C405_VENDOR_QUOTE_ID QID, ");
    sbQuery.append(" GET_CODE_NAME(t405.C901_UOM_ID) UOM, t405.C405_UOM_QTY UOMQTY, t405.C405_LOCK_FL LFL ");
    sbQuery.append(" ,t405.C901_UOM_ID UOMID ");
    sbQuery.append(" , GET_VALIDATED_STATUS (t405.C205_PART_NUMBER_ID, t405.C301_VENDOR_ID) VALIDATE_FL ");
    sbQuery.append(" , GET_VERIFIED_STATUS (t405.C205_PART_NUMBER_ID) VERIFIED_FL ");
    sbQuery.append(" FROM T405_VENDOR_PRICING_DETAILS t405,T301_VENDOR t301 ");
    sbQuery.append(" WHERE t405.C405_VOID_FL IS NULL ");
    sbQuery.append(" AND t405.C301_VENDOR_ID = t301.C301_VENDOR_ID ");
    sbQuery.append(" AND t301.C901_VENDOR_COST_TYPE = '"+strVendorCostType);
    sbQuery.append("' AND t405.C1900_COMPANY_ID =" + getCompId());
    sbQuery.append(" AND t405.C205_PART_NUMBER_ID = '");
    sbQuery.append(strPartNum);
    sbQuery.append("' ORDER BY t405.C405_COST_PRICE");
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    hmReturn.put("PRICELIST", alReturn);

//Commenting below code for PMT-51082
//    sbQuery.setLength(0);
//    sbQuery
//        .append(" SELECT	C301_VENDOR_ID ID, C301_VENDOR_NAME  ||' - ('|| GET_CODE_NAME(GET_VENDOR_CURRENCY(C301_VENDOR_ID)) || ')'  NAME ");
//    sbQuery.append(" FROM T301_VENDOR  WHERE C1900_COMPANY_ID =" + getCompId());
//    sbQuery.append(" ORDER BY C301_VENDOR_NAME");
//    alReturn1 = gmDBManager.queryMultipleRecords(sbQuery.toString());
//    hmReturn.put("VENDORLIST", alReturn1);

    return hmReturn;
  } // End of getVendorPriceList

  /**
   * saveVendorPriceList - This method saves the vendor pricing for a part
   * 
   * @param String InputString: that contains the ^ separated values
   * @param String strPartNum
   * @param String strUserName
   * @return HashMap
   * @exception AppError
   **/
  public HashMap saveVendorPriceList(String strInputString, String strPartNum, String strUsername)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    HashMap hmReturn = new HashMap();

    gmDBManager.setPrepareString("GM_SAVE_VENDOR_PRICING", 3);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strPartNum);
    gmDBManager.setString(3, strUsername);

    gmDBManager.execute();
    gmDBManager.commit();

    return hmReturn;
  } // end of saveVendorPriceList

  /**
   * getVendorList - This method will
   * 
   * @param HashMap hmParam
   * @return HashMap
   * @exception AppError
   **/
  /**
 * @param hmParam
 * @return
 * @throws AppError
 */
public HashMap getVendorList(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    HashMap hmReturn = new HashMap();
    	/*Get costype flag and cost*/
    String strCostTypeFlag= GmCommonClass.parseNull((String) hmParam.get("COST_TYPE_FL"));
    String strCost= GmCommonClass.parseNull((String) hmParam.get("COST_TYPE"));
    
    String strCheckActiveFlag = GmCommonClass.parseNull((String) hmParam.get("CHECKACTIVEFL"));
    String strCheckVPAccessFlag = GmCommonClass.parseNull((String) hmParam.get("CHECKVPACCESSFL"));
    String CompanyId=getCompId();
    String strCompanyID=GmCommonClass.parseNull((String) hmParam.get("COMPANY_ID"));
    CompanyId = CompanyId.isEmpty() ? strCompanyID : CompanyId ;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT	T301.C301_VENDOR_ID ID,	T301.C301_VENDOR_NAME NAME, T101.C101_PARTY_ID VID, ");
    sbQuery
        .append(" T101.C101_PARTY_NM VNAME, T301.C301_ACTIVE_FL ACTFL FROM T301_VENDOR T301, T101_PARTY T101 ");
    sbQuery.append(" WHERE T301.C101_PARTY_ID = T101.C101_PARTY_ID ");
    if (strCheckActiveFlag.equals("Y")) {
      sbQuery.append(" AND T301.C301_ACTIVE_FL IS NULL ");
    }
    // Appending company filter
    sbQuery.append(" AND  t301.C1900_COMPANY_ID = " + CompanyId);
    /*Standard cost*/
    if (strCostTypeFlag.equals("Y")) {
        sbQuery.append(" AND C901_VENDOR_COST_TYPE = "+strCost);
      }
    // only load vendor who are enabled vendor portal access
    if (strCheckVPAccessFlag.equals("Y")) {
        sbQuery.append(" AND C301_VENDOR_PORTAL_FL = '"+strCheckVPAccessFlag+"' ");
      }
    sbQuery.append(" ORDER  BY UPPER(T301.C301_VENDOR_NAME) ");

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    hmReturn.put("VENDORLIST", alReturn);

    return hmReturn;
  } // End of getVendorList

  /**
   * saveVendorMapping - This method will save the vendor mapping details
   * 
   * @param HashMap hmParam
   * @exception AppError
   **/

  public void saveVendorMapping(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strPrepareString = null;
    String strVendorId = (String) hmParam.get("VENDORID");
    String strVendorMapCatId = (String) hmParam.get("HVENDCATMAPID");
    String strVendorType = (String) hmParam.get("VENDORTYPE");
    String strVendorCategory = (String) hmParam.get("VENDORCATEGORY");
    String strInActiveFlag = (String) hmParam.get("INACTIVEFLAG");
    String strUserId = (String) hmParam.get("USERID");



    gmDBManager.setPrepareString("GM_OP_SAV_VENDOR_MAPPING", 6);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.setString(1, strVendorId);
    gmDBManager.setString(2, strVendorMapCatId);
    gmDBManager.setString(3, strVendorType);
    gmDBManager.setString(4, strVendorCategory);
    gmDBManager.setString(5, strInActiveFlag);
    gmDBManager.setString(6, strUserId);

    gmDBManager.execute();
    gmDBManager.commit();



  }

  /**
   * fetchVendorMapInfo - This method will fetch the vendor category mapping details
   * 
   * @param String strVendorId
   * @return HashMap if the call is to edit vendor category mapping info RowSetDynaClass if the call
   *         is to display the vendor category mapping info for that vendor
   * @exception AppError
   **/
  public RowSetDynaClass fetchVendorMapInfo(String strVendorId) throws AppError {
    RowSetDynaClass rsDynaResult = null;


    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT T1001.C1001_PARTY_CATEGORY_ID ID, GET_CODE_NAME (T1001.C901_TYPE) VENDTYPE ");
    sbQuery
        .append(" , GET_CODE_NAME (T1001.C901_CATEGORY) VENDCAT, DECODE (UPPER (T1001.C1001_ACTIVE_FL), 'Y', 'InActive', 'Active') ACTIVEFL ");
    sbQuery.append(" FROM T1001_PARTY_CATEGORY T1001 ");
    sbQuery.append(" WHERE T1001.C101_PARTY_ID =");
    sbQuery.append(strVendorId);
    sbQuery.append(" AND t1001.C1001_VOID_FL IS NULL ");
    log.debug(" Query for fetchVendorMapInfo is " + sbQuery.toString());
    rsDynaResult = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());


    return rsDynaResult;
  }

  /**
   * fetchEditVendorMapInfo - This method will fetch the vendor category mapping details for a
   * particular category of a vendor
   * 
   * @param String strVendorMapCatId
   * @return HashMap if the call is to edit vendor category mapping info RowSetDynaClass if the call
   *         is to display the vendor category mapping info for that vendor
   * @exception AppError
   **/
  public HashMap fetchEditVendorMapInfo(String strVendorMapCatId) throws AppError {
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append("  SELECT T1001.C101_PARTY_ID VENDORID, T1001.C901_TYPE VENDORTYPE, T1001.C901_CATEGORY VENDORCATEGORY ");
    sbQuery.append(" , T1001.C1001_ACTIVE_FL INACTIVEFLAG ");
    sbQuery.append(" FROM T1001_PARTY_CATEGORY T1001 ");
    sbQuery.append(" WHERE T1001.C1001_PARTY_CATEGORY_ID =");
    sbQuery.append(strVendorMapCatId);
    sbQuery.append(" AND t1001.C1001_VOID_FL IS NULL ");
    log.debug(" Query for fetchEditVendorMapInfo is " + sbQuery.toString());
    hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());


    return hmReturn;
  }

  /**
   * fchPartVendorStatus - This method will fetch the validated and verified status
   * 
   * @param String strPartNum and strVendorId
   * @return String strStatus
   * @exception AppError
   **/
  public String fchPartVendorStatus(String strPartNum, String strVendorId) throws AppError {
    StringBuffer strStatus = new StringBuffer();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    // getting validated flag status
    gmDBManager.setFunctionString("GET_VALIDATED_STATUS", 2);
    gmDBManager.setString(2, strPartNum);
    gmDBManager.setString(3, strVendorId);
    gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
    gmDBManager.execute();
    strStatus.append(GmCommonClass.parseNull(gmDBManager.getString(1)));
    strStatus.append(",");
    // getting verified flag status
    gmDBManager.setFunctionString("GET_VERIFIED_STATUS", 1);
    gmDBManager.setString(2, strPartNum);
    gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
    gmDBManager.execute();
    strStatus.append(GmCommonClass.parseNull(gmDBManager.getString(1)));
    strStatus.append(",");

    gmDBManager.close();

    return strStatus.toString();
  }

  /**
   * Fetch vendor pricing details.
   * 
   * @param hmParam the hm param
   * @return the array list
   * @throws AppError the app error
   */
  public ArrayList fetchVendorPricingDetails(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
    String strSubComFl = GmCommonClass.parseNull((String) hmParam.get("SHOWSUBCOMPONENTFL"));
    String strLocked = GmCommonClass.parseNull((String) hmParam.get("LOCKED"));
    String strActive = GmCommonClass.parseNull((String) hmParam.get("ACTIVE"));
    //Added vendor cost type for PMT-51082
    String strVendorCostType = GmCommonClass.parseNull((String) hmParam.get("COST_TYPE"));

    gmDBManager.setPrepareString("gm_pkg_pur_vendor_pricing.gm_pur_fch_vendor_pricing", 8);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("PROJECTID")));
    gmDBManager.setString(2, strVendorId.equals("0") ? "" : strVendorId);
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER")));
    gmDBManager.setString(4, strSubComFl.equals("on") ? "Y" : strSubComFl);
    gmDBManager.setString(5, strLocked.equals("0") ? "" : strLocked);
    gmDBManager.setString(6, strActive.equals("0") ? "" : strActive);
    gmDBManager.setString(7, strVendorCostType);
    gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
    gmDBManager.execute();

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(8)));
    gmDBManager.close();

    return alReturn;
  }

  /**
   * Save vendor pricing details.
   * 
   * @param hmParam the hm param
   * @throws AppError the app error
   */
  public String saveVendorPricingDetails(HashMap hmParam) throws AppError {

    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strErrMsg = "";
    String strErrVal = "";
    String strFinalErrMsg = "";
    gmDBManager.setPrepareString("gm_pkg_pur_vendor_pricing.gm_pur_save_vendor_pricing", 5);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("PROJECTID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("VENDORID")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING")));
    gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParam.get("USERID")));
    gmDBManager.registerOutParameter(5, OracleTypes.CLOB);
    gmDBManager.execute();
    strFinalErrMsg = GmCommonClass.parseNull(gmDBManager.getString(5));

    // if there is any error occurs while creating transaction, we consolidate those error message
    // and throwing AppError
    if (!strFinalErrMsg.equals("")) {
      gmDBManager.close();
      // throw new AppError(strFinalErrMsg, "", 'E');
    } else {
      gmDBManager.commit();
    }
    return strFinalErrMsg;
  }
  public String saveStandardCost(HashMap hmParam) throws AppError {
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

	    String strMsg = "";
	    String strPartNum = "";
	    String strCost = "";
	    String strVendorId = "";
	    String strUserId =  "";
	    strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
	    strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
	    strCost = GmCommonClass.parseNull((String) hmParam.get("COST"));
	    strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
	    gmDBManager.setPrepareString("gm_pkg_pur_vendor_cost.gm_sav_standard_cost", 4);
	    gmDBManager.registerOutParameter(4, java.sql.Types.CHAR);
	    gmDBManager.setString(1, strVendorId);
	    gmDBManager.setString(2, strPartNum);
	    gmDBManager.setString(3, strCost);
	    gmDBManager.setString(4, strUserId);
	    gmDBManager.execute();

	    strMsg = "Standard Cost Updated Succesfully based on partNumber:"+strPartNum;
	    gmDBManager.commit();

	    return strMsg;
	  } 
}// end of class GmTestBean
