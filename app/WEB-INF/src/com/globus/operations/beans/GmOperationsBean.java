/*
 * Module: GmOperationsBean.java Author: Dhinakaran James Project: Globus Medical App Date-Written:
 * 11 Mar 2004 Security: Unclassified Description: Testing Bean
 * 
 * Revision Log (12/18/08 bvidyasankar DHR Issue 1366)
 * --------------------------------------------------------- 12/18/08 bvidyasankar Added Status_fl
 * and Work_Order_id in procedure call in modifyDHRforRec() 12/23/08 bvidyasankar Modified input
 * parameter type of of getDHRList to HashMap and added an additional filter project id (Issue 1976)
 */

package com.globus.operations.beans;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
// Adding the JMS consumer class import statement
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.common.util.GmTemplateUtil;
import com.globus.jms.consumers.beans.GmJIRAProcessBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.abs.beans.GmUDIBean;
import com.globus.operations.factory.beans.GmUDIFactoryBean;
import com.globus.operations.inventory.beans.GmLocationBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.operations.logistics.beans.GmTicketBean;


public class GmOperationsBean extends GmBean {

  GmCommonClass gmCommon = new GmCommonClass();
  public static final String TEMPLATE_NAME = "GmNCMREvaluationDueEmailJob";
  // Instantiating the Logger
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  public GmOperationsBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmOperationsBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * initiateSetBuild - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap initiateSetBuild(String strSetId, String strType, String strUsername)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strMsg = "";
    String strConsignId = "";

    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();

    gmDBManager.setPrepareString("GM_INITIATE_SET_BUILD", 5);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(4, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);

    gmDBManager.setString(1, strSetId);
    gmDBManager.setString(2, strType);
    gmDBManager.setString(3, strUsername);


    gmDBManager.execute();
    strConsignId = gmDBManager.getString(4);
    strMsg = gmDBManager.getString(5);

    hmReturn.put("MSG", strMsg);
    hmReturn.put("CONSIGNID", strConsignId);

    hmResult = viewBuiltSetDetails(strConsignId);

    hmReturn.put("CONDETAILS", hmResult); // Vendor Address
    gmDBManager.commit();


    return hmReturn;
  } // end of initiateSetBuild

  /**
   * viewBuiltSetDetails - This method will
   * 
   * @param String strConsignId
   * @return HashMap
   * @exception AppError
   **/
  public HashMap viewBuiltSetDetails(String strConsignId) throws AppError {
    HashMap hmParam = new HashMap();
    hmParam.put("CONSIGNID", strConsignId);
    return viewBuiltSetDetails(hmParam);
  }

  /**
   * viewBuiltSetDetails - This method will
   * 
   * @param HashMap
   * @return HashMap
   * @exception AppError
   **/
  public HashMap viewBuiltSetDetails(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();

    StringBuffer sbQuery = new StringBuffer();
    String strConsignId = GmCommonClass.parseNull((String) hmParam.get("CONSIGNID"));
    String strRemVoidFl = GmCommonClass.parseNull((String) hmParam.get("REMVOIDFL"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    sbQuery
        .append(" SELECT t504.C504_CONSIGNMENT_ID CID, DECODE(t504.C504_TYPE,106703,t504.c520_request_id,106704,t504.c520_request_id,t504.C504_REF_ID) REFID, t504.C207_SET_ID SETID, t207.C207_SET_NM SNAME, ");
    sbQuery
        .append(" GET_ACCOUNT_NAME(t504.C704_ACCOUNT_ID) ANAME, GET_USER_NAME(t504.C504_CREATED_BY) UNAME, ");
    sbQuery.append(" to_char(t504.C504_CREATED_DATE,'" + getCompDateFmt()
        + "' ||' HH:MI AM') CDATE, t504.C504_VERIFY_FL VFL, ");
    sbQuery.append(" to_char(t504.c504_ship_date,'" + getCompDateFmt() + "' ||'') SDATE,");
    sbQuery
        .append(" t504.C504_SHIP_REQ_FL SHIPFL, t504.C504_STATUS_FL SFL, t504.C504_TYPE CTYPE, t504.C504_FINAL_COMMENTS FCOMMENTS, ");
    sbQuery
        .append(" GET_ALL_COMMENTS(t504.C520_REQUEST_ID,'1235','N') LOG_COMMENTS,  t504.C504_COMMENTS COMMENTS, GET_DISTRIBUTOR_NAME(t504.C701_DISTRIBUTOR_ID) DNAME, ");
    sbQuery
        .append(" t504.C504_VERIFIED_BY VUSER, t504.C504_VERIFIED_DATE VDATE, GET_USER_NAME(t504.C504_SHIP_TO_ID) RNAME, t504.c504_ship_to CSHIPTO, get_plant_name(t504.c504_ship_to_id) PNAME, ");
    sbQuery
        .append(" gm_pkg_cm_shipping_info.get_ship_add(t504.C504_CONSIGNMENT_ID,DECODE(t504.c504_type,400085,4000518,400086,4000518,106703,26240435,106704,26240435,50181)) SHIPADD, GET_CODE_NAME(t504.C504_TYPE) TYPE, ");
    sbQuery
        .append(" GET_CODE_NAME(t504.C504_INHOUSE_PURPOSE) PURP, GET_USER_NAME(t504.C504_LAST_UPDATED_BY) LUNAME, ");
    sbQuery.append(" to_char(t504.C504_LAST_UPDATED_DATE,'" + getCompDateFmt()
        + "' ||' HH:MI AM') LUDATE, t504.c504_reprocess_id RAID,");
    sbQuery
        .append(" GET_COMPID_FROM_DISTID(t504.C701_DISTRIBUTOR_ID) COMPANYID, GM_PKG_OP_REQUEST_MASTER.GET_REQUEST_ATTRIBUTE(t504.C520_REQUEST_ID,'1006420') CSTPO ,get_rule_value('PARTSIZE','SHOWPARTSIZE') SHOWSIZE ");
    sbQuery.append(", t504.c1910_division_id division_id, t505.errcnt ERRCNT ");
    sbQuery
    .append(", gm_pkg_op_storage_building.get_building_short_name(c5057_building_id) BUILDINGNAME,DECODE(get_rule_value('"+getCompId()+"','TISSUE_VALIDATION'),'Y',t207.c207_tissue_fl,'N') TISSUEFL ");
    sbQuery
        .append(" FROM T504_CONSIGNMENT t504,t207_set_master t207, (SELECT count(1) errcnt  FROM t505_item_consignment WHERE c504_consignment_id = '"
            + strConsignId
            + "' AND c505_error_dtls IS NOT NULL AND c505_control_number IS NOT NULL AND c505_void_fl IS NULL) t505 ");
    sbQuery.append(" WHERE t504.C504_CONSIGNMENT_ID ='");
    sbQuery.append(strConsignId);
    sbQuery.append("'");
    if (!strRemVoidFl.equals("Y")) {
      sbQuery.append(" AND t504.C504_VOID_FL IS NULL AND  t504.c207_set_id  = t207.c207_set_id(+) AND t207.c207_void_fl(+) IS NULL ");
    }
    log.debug("sbQuery.toString() ***** " + sbQuery.toString());
    hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());


    return hmReturn;
  } // End of viewBuiltSetDetails

  /**
   * saveSetMaster - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap saveSetMaster(String strConsignId, String strInputString, String strComments,
      String strFlag, String strVerifyFlag, String strUsername) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();

    String strMsg = "";
    HashMap hmReturn = new HashMap();

    gmDBManager.setPrepareString("GM_SAVE_SET_BUILD", 7);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(7, java.sql.Types.CHAR);

    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strInputString);
    gmDBManager.setString(3, strComments);
    gmDBManager.setString(4, strFlag);
    gmDBManager.setString(5, strVerifyFlag);
    gmDBManager.setString(6, strUsername);

    gmDBManager.execute();
    strMsg = gmDBManager.getString(7);

    hmReturn.put("MSG", strMsg);
    // hmResult = viewBuiltSetDetails(strConsignId);

    // hmReturn.put("CONDETAILS",hmResult); // Vendor Address
    gmDBManager.commit();

    return hmReturn;
  } // end of saveSetMaster

  public HashMap loadSavedSetMaster(String strConsignId) throws AppError {
    HashMap hmParam = new HashMap();
    hmParam.put("CONSIGNID", strConsignId);
    return loadSavedSetMaster(hmParam);
  }

  /*
   * loadOUSSavedSetMaster - This method will return GOP OUS Consignment Txn details.
   * 
   * @param HashMap hmParam
   * 
   * @return HashMap
   * 
   * @exception AppError
   */
  public HashMap loadOUSSavedSetMaster(HashMap hmParam) throws AppError {

    HashMap hmValue = new HashMap();
    ArrayList alList = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
    String strConsignmentId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));

    gmDBManager.setPrepareString("gm_pkg_oppr_request_summary.gm_fch_set_master", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strConsignmentId);

    gmDBManager.execute();
    alList =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    hmValue.put("SETLOAD", alList);
    return hmValue;
  } // End of loadGOPConsignDetails

  /**
   * loadSavedSetMaster - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadSavedSetMaster(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmTemp = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    String strSortFlag =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SORT_BY_LOT_FL"));
    StringBuffer sbQuery = new StringBuffer();
    String strConsignId = GmCommonClass.parseNull((String) hmParam.get("CONSIGNID"));
    String strRemVoidFl = GmCommonClass.parseNull((String) hmParam.get("REMVOIDFL"));
    String strSkipPlantFl = GmCommonClass.parseNull((String) hmParam.get("SKIPPLANTCHECK"));

    sbQuery.append("SELECT  T505.C205_PART_NUMBER_ID PNUM ");
    sbQuery
        .append(", get_partdesc_by_company(T505.C205_PART_NUMBER_ID) PDESC, T505.C901_WAREHOUSE_TYPE WHTYPE");
    sbQuery.append(",NVL(T505.C505_ITEM_QTY,0) IQTY");
    sbQuery.append(",T505.C505_CONTROL_NUMBER CNUM");
    sbQuery.append(",NVL(T505.C505_ITEM_PRICE,0) PRICE ");
    sbQuery
        .append(",GET_COUNT_PARTID_CONSIGN(T505.C504_CONSIGNMENT_ID,T505.C205_PART_NUMBER_ID) QTYUSED");
    sbQuery
        .append(" ,decode(GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T505.C205_PART_NUMBER_ID),'',decode(to_char(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T505.C205_PART_NUMBER_ID,T505.C505_CONTROL_NUMBER),'YYMMDD'),'','10' || T505.C505_CONTROL_NUMBER,'17' || to_char(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T505.C205_PART_NUMBER_ID,T505.C505_CONTROL_NUMBER),'YYMMDD') || '10' || T505.C505_CONTROL_NUMBER),decode(to_char(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T505.C205_PART_NUMBER_ID,T505.C505_CONTROL_NUMBER),'YYMMDD'),'','01' || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T505.C205_PART_NUMBER_ID)  || '10' || T505.C505_CONTROL_NUMBER,'01' || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T505.C205_PART_NUMBER_ID) || '17' || to_char(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T505.C205_PART_NUMBER_ID,T505.C505_CONTROL_NUMBER),'YYMMDD') || '10' || T505.C505_CONTROL_NUMBER)) BARCODEEXP");
    sbQuery
        .append(" ,decode(GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T505.C205_PART_NUMBER_ID),'',decode(to_char(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T505.C205_PART_NUMBER_ID,T505.C505_CONTROL_NUMBER),'YYMMDD'),'','(10)' || T505.C505_CONTROL_NUMBER,'(17)' || to_char(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T505.C205_PART_NUMBER_ID,T505.C505_CONTROL_NUMBER),'YYMMDD') || '(10)' || T505.C505_CONTROL_NUMBER),decode(to_char(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T505.C205_PART_NUMBER_ID,T505.C505_CONTROL_NUMBER),'YYMMDD'),'','(01)' || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T505.C205_PART_NUMBER_ID)  || '(10)' || T505.C505_CONTROL_NUMBER,'(01)' || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T505.C205_PART_NUMBER_ID) || '(17)' || to_char(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T505.C205_PART_NUMBER_ID,T505.C505_CONTROL_NUMBER),'YYMMDD') || '(10)' || T505.C505_CONTROL_NUMBER)) BARCODEEXPVAL");
    sbQuery
        .append(" , GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T505.C205_PART_NUMBER_ID,T505.C505_CONTROL_NUMBER) EXPDATE");
    sbQuery
        .append(" , to_char(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T505.C205_PART_NUMBER_ID,T505.C505_CONTROL_NUMBER),'"
            + getCompDateFmt() + "') EXPRYDATE ");


    sbQuery
        .append(",GET_QTY_IN_BULK(T505.C205_PART_NUMBER_ID) QTYBULK, GET_QTY_IN_INV(T505.C205_PART_NUMBER_ID,T505.C901_WAREHOUSE_TYPE)");
    sbQuery.append(",SETDT.CRITFL");
    sbQuery.append(",SETDT.CRITQTY");
    sbQuery.append(",SETDT.SETFL");
    sbQuery.append(",SETDT.CTRITAG");
    sbQuery.append(",SETDT.CRITTAG");
    sbQuery.append(",NVL(SETDT.QTY, 0) QTY");
    sbQuery
        .append(" , NVL (gm_pkg_op_item_control_rpt.get_initiated_location (t505.c205_part_number_id, '");
    sbQuery.append(strConsignId);
    sbQuery
        .append("', DECODE (t504.c504_status_fl, '2', 'pick', '3', 'put', 'pick')), gm_pkg_op_inv_scan.get_part_location(t505.c205_part_number_id, t504.c504_type)) SUGLOCATION ");
    // Need to get the location cd for OUS pick slip
    sbQuery.append(" , gm_pkg_op_item_control_rpt.get_location_cd (t505.c205_part_number_id, '");
    sbQuery.append(strConsignId);
    sbQuery
        .append("', DECODE (t504.c504_status_fl, '2', 'pick', '3', 'put', 'pick'), t504.c504_type,'',t505.c505_control_number) SUGLOCATIONCD ");
    // sbQuery.append(",get_distributor_inter_party_id (c701_distributor_id) inter_party_id");
    // sbQuery.append(", DECODE(T504.C504_INHOUSE_PURPOSE, 50061, 0, DECODE(t504.C504_STATUS_FL , 4,NVL (t505.c505_item_price, 0),NVL(get_ac_cogs_value (t505.c205_part_number_id, 4900),0))) NEWPRICE");
    sbQuery
        .append(", DECODE(T504.C504_INHOUSE_PURPOSE, 50061, 0, DECODE(t504.C504_STATUS_FL , 4,NVL (t505.c505_item_price, 0), NVL(get_account_part_pricing (NULL, t505.c205_part_number_id, get_distributor_inter_party_id (t504.c701_distributor_id)),0))) NEWPRICE");

    sbQuery
        .append(",get_part_attribute_value(t505.c205_part_number_id, 92340) tagfl , get_part_size(t505.c205_part_number_id, t505.c505_control_number ) partsize,get_partnum_material_type(T505.C205_PART_NUMBER_ID) PARTMTRELTYPE, t505.c505_error_dtls ERRORDTLS ");
    sbQuery.append(" FROM    T504_CONSIGNMENT T504 ");
    sbQuery.append(", T505_ITEM_CONSIGNMENT T505");
    sbQuery.append(", (SELECT T208.C205_PART_NUMBER_ID, T208.C208_CRITICAL_FL CRITFL");
    sbQuery
        .append(",DECODE(T208.C208_CRITICAL_QTY,NULL,T208.C208_SET_QTY,T208.C208_CRITICAL_QTY) CRITQTY");
    sbQuery.append(",T208.C208_INSET_FL SETFL ");
    sbQuery.append(",T208.C208_CRITICAL_TAG CTRITAG");
    sbQuery.append(",GET_CODE_NAME(T208.C901_CRITICAL_TYPE) CRITTAG");
    sbQuery.append(",T208.C208_SET_QTY QTY");

    sbQuery.append(" FROM T208_SET_DETAILS T208, T504_CONSIGNMENT T504");
    sbQuery.append(" WHERE T504.C504_CONSIGNMENT_ID = '");
    sbQuery.append(strConsignId);
    sbQuery
        .append("' AND T208.C207_SET_ID = T504.C207_SET_ID AND T208.C208_VOID_FL IS NULL) SETDT");
    sbQuery.append(" WHERE  T504.C504_CONSIGNMENT_ID = '");
    sbQuery.append(strConsignId);
    sbQuery.append("' AND T504.C504_CONSIGNMENT_ID = T505.C504_CONSIGNMENT_ID");
    if (!strSkipPlantFl.equals("Y")) {
      sbQuery.append(" AND  T504.C5040_PLANT_ID =" + getCompPlantId());
    }
    if (!strRemVoidFl.equals("Y")) {
      sbQuery.append(" AND T505.C505_VOID_FL is NULL");
    }
    sbQuery.append(" AND T505.C205_PART_NUMBER_ID = SETDT.C205_PART_NUMBER_ID (+)");
    if (strSortFlag.equals("YES")) {
      sbQuery.append(" ORDER BY T505.C205_PART_NUMBER_ID, T505.C505_CONTROL_NUMBER");
    } else {
      sbQuery.append(" ORDER BY T505.C205_PART_NUMBER_ID ");
    }
    log.debug("Set Load Query ****** " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    hmReturn.put("SETLOAD", alReturn);

    if (alReturn.size() == 0) {

      throw new AppError(new SQLException("Error Occured", null, 20696));

    }
    /*
     * sbQuery.setLength(0); sbQuery.append(" SELECT	C207_SET_ID SETID ");
     * sbQuery.append(" FROM T504_CONSIGNMENT "); sbQuery.append(" WHERE C504_CONSIGNMENT_ID = '");
     * sbQuery.append(strConsignId); sbQuery.append("'");
     * sbQuery.append(" AND C504_VOID_FL IS NULL "); log.debug(" SETID query " +
     * sbQuery.toString()); hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
     */


    String strSetId = "";// (String)hmResult.get("SETID"); todo Commented on May 08th.. Need to
                         // verify this later

    // Adding the SetId to the return HashMap. This is done for Rollback Set CSG.
    // The Rollback button would be displayed in GmConsignShip.jsp only if the SetId is not null
    // hmReturn.put("SETID",strSetId);

    if (!strSetId.equals("")) {
      sbQuery.setLength(0);
      sbQuery
          .append(" SELECT	C.C205_PART_NUMBER_ID , GET_PARTNUM_DESC(C.C205_PART_NUMBER_ID), C.C208_SET_QTY, 'Add to Set' ");
      sbQuery.append(" FROM  T504_CONSIGNMENT A, T208_SET_DETAILS C  ");
      sbQuery.append(" WHERE A.C207_SET_ID = '");
      sbQuery.append(strSetId);
      sbQuery.append("' AND A.C207_SET_ID = C.C207_SET_ID AND A.C504_CONSIGNMENT_ID = '");
      sbQuery.append(strConsignId);
      sbQuery.append("' AND C.C208_SET_QTY <> 0 AND C208_INSET_FL = 'Y' ");
      sbQuery.append(" AND A.C504_VOID_FL IS NULL ");
      sbQuery.append(" AND C.C205_PART_NUMBER_ID NOT IN (SELECT  C205_PART_NUMBER_ID FROM ");
      sbQuery.append(" T505_ITEM_CONSIGNMENT WHERE C504_CONSIGNMENT_ID = '");
      sbQuery.append(strConsignId);
      sbQuery.append("')");
      sbQuery.append("UNION");
      sbQuery
          .append(" SELECT C205_PART_NUMBER_ID, GET_PARTNUM_DESC(C205_PART_NUMBER_ID), C505_ITEM_QTY , 'Remove from Set' ");
      sbQuery.append(" FROM T505_ITEM_CONSIGNMENT WHERE C504_CONSIGNMENT_ID ='");
      sbQuery.append(strConsignId);
      sbQuery.append("' AND C205_PART_NUMBER_ID IN ( SELECT C.C205_PART_NUMBER_ID");
      sbQuery.append(" FROM  T504_CONSIGNMENT A, T208_SET_DETAILS C ");
      sbQuery.append(" WHERE A.C207_SET_ID = '");
      sbQuery.append(strSetId);
      sbQuery.append("' AND A.C207_SET_ID = C.C207_SET_ID AND A.C504_CONSIGNMENT_ID = '");
      sbQuery.append(strConsignId);
      sbQuery.append("' AND C.C208_SET_QTY = 0 AND A.C504_VOID_FL IS NULL )");



      // alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString()); todo Commented on May
      // 08th.. to verify this sql
      hmReturn.put("MISSINGPARTS", new ArrayList());

      if (!strSetId.equals("")) {
        sbQuery.setLength(0);
        sbQuery.append(" SELECT C504_CONSIGNMENT_ID CONID ");
        sbQuery.append(" FROM T504_CONSIGNMENT ");
        sbQuery.append(" WHERE C207_SET_ID = '");
        sbQuery.append(strSetId);
        sbQuery.append("' AND C504_STATUS_FL < 2 ");
        sbQuery.append(" AND C504_VOID_FL IS NULL ");


        // alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString()); Commented on May 08th..
        // to verify this sql
        hmReturn.put("SIMILARSETS", new ArrayList());
      }
    }


    return hmReturn;
  } // End of loadSavedSetMaster

  /**
   * reportDashboard - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap reportDashboard(String strFlag, String strDeptId) throws AppError {
    ArrayList alSetResult = new ArrayList();
    ArrayList alDHRReturn = new ArrayList();
    ArrayList alReturnsReturn = new ArrayList();
    ArrayList alNCMRReturn = new ArrayList();
    ArrayList alItemConsign = new ArrayList();
    GmLogisticsBean gmLogis = new GmLogisticsBean();

    HashMap hmReturn = new HashMap();

    if (strDeptId.equals("O") || strDeptId.equals("Z")) // For Operations/Receiving Dept
    {
      alDHRReturn = getDHRDashboard("3100,3101"); // FOR DHR Dashboard
      alNCMRReturn = getNCMRDashboard(); // For NCMR Dashboard
    }
    if (strDeptId.equals("L") || strDeptId.equals("Z") || strDeptId.equals("C")) // For Logistics
                                                                                 // Team
    {
      alReturnsReturn = getReturnsDashboard("REPROCESS", ""); // FOR Returns Dashboard
    }
    if (strDeptId.equals("Q")) // For Quality Team
    {
      alNCMRReturn = getNCMRDashboard(); // For NCMR Dashboard
    }
    alItemConsign = gmLogis.reportDashboardItems(); // For Item Consignment & InHouse Transactions
                                                    // Dashboard

    hmReturn.put("DHRDASH", alDHRReturn);
    hmReturn.put("NCMRDASH", alNCMRReturn);
    hmReturn.put("ITEMS", alItemConsign);
    hmReturn.put("INISETS", alSetResult);
    hmReturn.put("RETURNSDASH", alReturnsReturn);


    return hmReturn;
  } // End of reportDashboard

  /**
   * getPartNumSales - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList getPartNumSales(String strProjId, String strFromDt, String strToDt)
      throws AppError {
    ArrayList alResult = new ArrayList();


    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT T205.C205_PART_NUMBER_ID ID, T205.C202_PROJECT_ID PROJID, ");
    sbQuery.append(" GET_PARTNUM_DESC(T205.C205_PART_NUMBER_ID) PDESC, ");
    sbQuery.append(" ORD.QTY SALESCOUNT, CONS.QTY CONSIGNCOUNT, INHOUSE.QTY INHOUSECOUNT ");
    sbQuery.append(" FROM T205_PART_NUMBER T205, ");
    sbQuery.append(" (");
    sbQuery.append(" SELECT nvl(sum(A.C502_ITEM_QTY),0) QTY, C205_PART_NUMBER_ID");
    sbQuery.append(" FROM T502_ITEM_ORDER A, T501_ORDER B");
    sbQuery.append(" WHERE A.C501_ORDER_ID = B.C501_ORDER_ID");
    sbQuery.append(" AND C501_ORDER_DATE BETWEEN to_date('");
    sbQuery.append(strFromDt);
    sbQuery.append("',GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) AND to_date('");
    sbQuery.append(strToDt);
    sbQuery.append("',GET_RULE_VALUE('DATEFMT', 'DATEFORMAT'))");
    sbQuery.append(" AND C501_DELETE_FL IS NULL");
    sbQuery.append(" AND C501_VOID_FL IS NULL");
    sbQuery.append(" AND A.C502_VOID_FL IS NULL");
    sbQuery.append(" AND NVL (c901_order_type, -999) <> 2524 ");
    sbQuery.append(" AND NVL (c901_order_type, -9999) NOT IN ( ");
    sbQuery.append(" SELECT t906.c906_rule_value ");
    sbQuery.append(" FROM t906_rules t906 ");
    sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
    sbQuery.append(" AND c906_rule_id = 'EXCLUDE') ");
    sbQuery.append(" GROUP BY C205_PART_NUMBER_ID");
    sbQuery.append(" )ORD,");
    sbQuery.append(" (SELECT nvl(sum(A.C505_ITEM_QTY),0) QTY, C205_PART_NUMBER_ID");
    sbQuery.append(" FROM T505_ITEM_CONSIGNMENT A, T504_CONSIGNMENT B");
    sbQuery.append(" WHERE A.C504_CONSIGNMENT_ID = B.C504_CONSIGNMENT_ID");
    sbQuery.append(" AND B.C504_SHIP_DATE BETWEEN to_date('");
    sbQuery.append(strFromDt);
    sbQuery.append("',GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) AND to_date('");
    sbQuery.append(strToDt);
    sbQuery.append("',GET_RULE_VALUE('DATEFMT', 'DATEFORMAT'))");
    sbQuery.append(" AND trim(A.C505_CONTROL_NUMBER) IS NOT NULL");
    sbQuery.append(" AND B.C704_ACCOUNT_ID IS NULL");
    sbQuery.append(" AND B.C504_VOID_FL IS NULL");
    sbQuery.append("  AND B.C504_CONSIGNMENT_ID NOT IN (SELECT C.C504_CONSIGNMENT_ID C ");
    sbQuery.append(" FROM T504A_CONSIGNMENT_EXCESS C ");
    sbQuery.append(" WHERE C.C504_CONSIGNMENT_ID = B.C504_CONSIGNMENT_ID)");
    sbQuery.append(" GROUP BY C205_PART_NUMBER_ID");
    sbQuery.append(" )CONS,");
    sbQuery.append(" (");
    sbQuery.append(" SELECT nvl(sum(A.C505_ITEM_QTY),0) QTY, C205_PART_NUMBER_ID");
    sbQuery.append(" FROM T505_ITEM_CONSIGNMENT A, T504_CONSIGNMENT B");
    sbQuery.append(" WHERE A.C504_CONSIGNMENT_ID = B.C504_CONSIGNMENT_ID");
    sbQuery.append(" AND B.C504_SHIP_DATE BETWEEN to_date('");
    sbQuery.append(strFromDt);
    sbQuery.append("',GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) AND to_date('");
    sbQuery.append(strToDt);
    sbQuery.append("',GET_RULE_VALUE('DATEFMT', 'DATEFORMAT'))");
    sbQuery.append(" AND B.C704_ACCOUNT_ID = '01'");
    sbQuery.append(" AND B.C504_VOID_FL IS NULL");
    sbQuery.append(" GROUP BY C205_PART_NUMBER_ID");
    sbQuery.append(" )INHOUSE ");
    sbQuery.append(" WHERE T205.C205_PART_NUMBER_ID = ORD.C205_PART_NUMBER_ID(+)");
    sbQuery.append(" AND T205.C205_PART_NUMBER_ID = CONS.C205_PART_NUMBER_ID(+)");
    sbQuery.append(" AND T205.C205_PART_NUMBER_ID = INHOUSE.C205_PART_NUMBER_ID(+)");
    sbQuery.append(" AND T205.C202_PROJECT_ID ='");
    sbQuery.append(strProjId);
    sbQuery.append("' ");
    sbQuery.append(" ORDER BY T205.C205_PART_NUMBER_ID");

    alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());


    return alResult;
  } // End of getPartNumSales


  /**
   * initiateDHR - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap initiateDHR(String strVendorId, String strInputStr, String strPackSlip,
      String strComments, String strRecDate, String strType, String strUsername) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strMsg = "";
    String strPdtIds = "";

    HashMap hmReturn = new HashMap();
    
    String strBatchProcessService =
        GmCommonClass.parseNull(GmCommonClass.getString("BATCH_PROCESS_SERVICE"));

    gmDBManager.setPrepareString("GM_INITIATE_DHR", 9);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(8, OracleTypes.CLOB);
    gmDBManager.registerOutParameter(9, java.sql.Types.CHAR);

    gmDBManager.setInt(1, Integer.parseInt(strVendorId));
    gmDBManager.setString(2, strComments);
    gmDBManager.setString(3, strPackSlip);
    gmDBManager.setString(4, strInputStr);
    gmDBManager.setString(5, strRecDate);
    gmDBManager.setString(6, strType);
    gmDBManager.setString(7, strUsername);
    gmDBManager.execute();
    strPdtIds = GmCommonClass.parseNull(gmDBManager.getString(8));
    strMsg = GmCommonClass.parseNull(gmDBManager.getString(9));
    hmReturn.put("MSG", strMsg);
    hmReturn.put("PDTID", strPdtIds);
    log.debug("strPdtIds=" + strPdtIds);
    gmDBManager.commit();



    if (strMsg != null && strMsg.length() > 0) {
      throw new AppError(strMsg, "", 'E');
    }


    return hmReturn;
  } // end of initiateDHR





  /**
   * updateLotMaster - This method will update insert data in t2550
   * 
   * @param String strInputStr
   * @param String strUsername
   * @return HashMap
   * @exception AppError
   **/
  public void updateLotMaster(String strInputStr, String strUsername) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();

    HashMap hmInput = new HashMap();
    hmInput.put("TXNID", strInputStr);
    hmInput.put("USERID", strUsername);

    GmUDIFactoryBean gmUDIFactoryBean = new GmUDIFactoryBean();

    GmUDIBean gmUDIBean = gmUDIFactoryBean.getInstance(GmCommonClass.countryCode);
    gmUDIBean.saveUDIInfo(gmDBManager, hmInput);

  }

  /**
   * getDHRList - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap getDHRList(HashMap hmDHRReport) throws AppError {

    ArrayList alReturn = new ArrayList();
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    RowSetDynaClass resultSet = null;

    String strId = GmCommonClass.parseNull((String) hmDHRReport.get("ID"));
    String strDHRIds = GmCommonClass.parseNull((String) hmDHRReport.get("DHRID"));
    String strFromDt = GmCommonClass.parseNull((String) hmDHRReport.get("FROMDT"));
    String strToDt = GmCommonClass.parseNull((String) hmDHRReport.get("TODT"));
    String strMode = GmCommonClass.parseNull((String) hmDHRReport.get("OPT"));
    String strProjID = GmCommonClass.parseNull((String) hmDHRReport.get("PROJID"));
    String strPartNum = GmCommonClass.parseNull((String) hmDHRReport.get("PARTNUM"));
    String strControlNum = GmCommonClass.parseNull((String) hmDHRReport.get("CONTROLNUM"));
    String strDHRStatus = GmCommonClass.parseNull((String) hmDHRReport.get("DHRSTATUS"));
    String strDateFormat = getCompDateFmt();

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT	T408.C408_DHR_ID ID, get_log_flag (T408.C402_WORK_ORDER_ID,1238) WLOG,T408.C205_PART_NUMBER_ID PNUM , GET_PARTNUM_DESC(T408.C205_PART_NUMBER_ID) PDESC, ");
    sbQuery
        .append(" T408.C408_QTY_RECEIVED QTYREC, T408.C408_CONTROL_NUMBER CNUM, T408.C408_CREATED_DATE CDATE ");
    sbQuery
        .append(", T408.C408_PACKING_SLIP PSLIP, C408_UPDATE_INV_FL , T402.C401_PURCHASE_ORD_ID POID, GET_VENDOR_SH_NAME(T408.C301_VENDOR_ID) VENDNM, ");
    sbQuery
        .append(" T408.C402_WORK_ORDER_ID WORKID, T408.C408_STATUS_FL SFL, T408.C301_VENDOR_ID VID, T408.C408_QTY_ON_SHELF SHELFQTY, ");
    sbQuery
        .append(" T409.C409_NCMR_ID NCMRID, GET_WO_UOM_TYPE_QTY(T402.C402_WORK_ORDER_ID) UOMANDQTY, ");
    sbQuery
        .append(" T408.c2550_expiry_date EXPDT , get_donor_number(T408.C2540_DONOR_ID) DONORNUMBER ");
    sbQuery.append(" , GET_PARTNUM_MATERIAL_TYPE(T408.C205_PART_NUMBER_ID) PRODTYPE");
    sbQuery.append(" , GET_PART_ATTR_VALUE_BY_COMP(T408.C205_PART_NUMBER_ID,106040," + getCompId()
        + ") SERIALNUMNEEDFL");
    if (strMode.equals("Track")) {
      sbQuery
          .append(", T408.C408_CREATED_DATE RECVTS, GET_USER_SH_NAME(T408.C408_CREATED_BY) RECVBY,");
      sbQuery.append(" to_char(T408.C408_CREATED_DATE,'HH:MI AM') RECVTIME, ");
      sbQuery
          .append(" T408.C408_INSPECTED_TS INSPTS, T408.C408_QTY_INSPECTED INSPQTY, GET_USER_SH_NAME(T408.C408_INSPECTED_BY) INSPBY,");
      sbQuery.append(" to_char(T408.C408_INSPECTED_TS,'HH:MI AM') INSPTIME, ");
      sbQuery
          .append(" T408.C408_PACKAGED_TS PACKTS, T408.C408_QTY_PACKAGED PACKQTY, GET_USER_SH_NAME(T408.C408_PACKAGED_BY) PACKBY,");
      sbQuery.append(" to_char(T408.C408_PACKAGED_TS,'HH:MI AM') PACKTIME, ");
      sbQuery
          .append(" T408.C408_VERIFIED_TS VERTS, GET_USER_SH_NAME(T408.C408_VERIFIED_BY) VERBY, ");
      sbQuery.append(" to_char(T408.C408_VERIFIED_TS,'HH:MI AM') VERTIME ");
    }
    sbQuery.append(" FROM T408_DHR T408, T402_WORK_ORDER T402, T409_NCMR T409 ");
    sbQuery.append(" WHERE T408.C402_WORK_ORDER_ID = T402.C402_WORK_ORDER_ID ");
    sbQuery.append(" AND T408.C408_DHR_ID = T409.C408_DHR_ID (+) ");
    sbQuery.append(" AND T402.C402_VOID_FL IS NULL ");
    sbQuery.append(" AND T408.C408_VOID_FL IS NULL ");
    sbQuery.append(" AND T409.C409_VOID_FL(+) IS NULL ");
    sbQuery.append(" AND  T408.C1900_COMPANY_ID = " + getCompId());
    sbQuery.append(" AND T408.c5040_plant_id = " + getCompPlantId());
    if (strMode.equals("Vendor")) {
      if (!strId.equals("")) {
        sbQuery.append(" AND  T408.C301_VENDOR_ID ='");
        sbQuery.append(strId);
        sbQuery.append("' ");
      }
      if (!strDHRIds.equals("")) {
        sbQuery.append(" AND T408.C408_DHR_ID ='");
        sbQuery.append(strDHRIds);
        sbQuery.append("' ");
      }

      if (!strPartNum.equals("")) {
        sbQuery.append(" AND T408.C205_PART_NUMBER_ID like '");
        sbQuery.append(strPartNum);
        sbQuery.append("%'");
      };

      if (!strControlNum.equals("")) {
        sbQuery.append(" AND  T408.C408_CONTROL_NUMBER like UPPER('");
        sbQuery.append(strControlNum);
        sbQuery.append("%')");
      };

    } else if (strMode.equals("Part")) {
      if (!strId.equals("")) {
        sbQuery.append(" AND  T408.C205_PART_NUMBER_ID IN (");
        sbQuery.append(strId);
        sbQuery.append(") ");
      }
      if (!strProjID.equals("0") && !strProjID.equals("")) {
        sbQuery.append(" AND  T408.C205_PART_NUMBER_ID IN (");
        sbQuery
            .append(" SELECT t205.c205_part_number_id FROM t205_part_number t205 WHERE c202_project_id = '");
        sbQuery.append(strProjID);
        sbQuery
            .append("' UNION  SELECT c205_part_number_id FROM t2021_part_project_mapping  WHERE c202_project_id = '");
        sbQuery.append(strProjID);
        sbQuery.append("' AND c2021_void_fl  IS NULL)");
      }
    } else if (strMode.equals("DHR")) {
      sbQuery.append(" AND  T408.C301_VENDOR_ID ='");
      sbQuery.append(strId);
      sbQuery.append("'");
      sbQuery.append(" AND T408.C408_DHR_ID IN (");
      sbQuery.append(strDHRIds);
      sbQuery.append(") ");
    }

    else if (strMode.equals("Track")) {
      if (!strDHRIds.equals("")) {
        sbQuery.append(" AND T408.C408_DHR_ID ='");
        sbQuery.append(strDHRIds);
        sbQuery.append("' ");
      }

      if (!strDHRStatus.equals("")) {
        sbQuery.append(" AND T408.C408_STATUS_FL = DECODE('");
        sbQuery.append(strDHRStatus);
        sbQuery.append("', 80200, 1, 80201, 2, 80202, 3, 80203, 4, T408.C408_STATUS_FL)  ");
      }

      if (!strPartNum.equals("")) {
        sbQuery.append(" AND T408.C205_PART_NUMBER_ID like '");
        sbQuery.append(strPartNum);
        sbQuery.append("%'");
      }

      if (!strProjID.equals("0") && !strProjID.equals("")) {
        sbQuery.append(" AND  T408.C205_PART_NUMBER_ID IN (");
        sbQuery
            .append(" SELECT t205.c205_part_number_id FROM t205_part_number t205 WHERE c202_project_id = '");
        sbQuery.append(strProjID);
        sbQuery
            .append("' UNION  SELECT c205_part_number_id FROM t2021_part_project_mapping  WHERE c202_project_id = '");
        sbQuery.append(strProjID);
        sbQuery.append("'   AND c2021_void_fl  IS NULL)");
      }
    }
    if (!strFromDt.equals("")) {
      sbQuery.append(" AND TRUNC(t408.c408_created_date) >= TO_DATE('");
      sbQuery.append(strFromDt);
      sbQuery.append("','" + strDateFormat + "') ");
    }

    if (!strToDt.equals("")) {
      sbQuery.append(" AND TRUNC(t408.c408_created_date) <= TO_DATE('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDateFormat + "') ");
    }

    if (strMode.equals("Part")) {
      sbQuery.append(" ORDER BY T408.C205_PART_NUMBER_ID, T408.C408_CREATED_DATE desc");
    } else {
      sbQuery.append(" ORDER BY T408.C408_CREATED_DATE DESC");
    }

    log.debug("Query for DHR List is " + sbQuery.toString());

    if (strMode.equals("Track")) {// Need to get the result in RowSetDynaClass form to
                                  // "GmDHRTrackReport.jsp"
      resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
      hmReturn.put("DHRLIST", resultSet);
    } else {
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("DHRLIST", alReturn);
    }

    return hmReturn;
  } // End of getDHRList

  /**
   * getDHRDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap getDHRDetails(String strDHRId) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmInHouseTxns = new HashMap();
    ArrayList alResult = new ArrayList();
    ArrayList alLotListing = new ArrayList();
    GmDonorInfoBean gmDonorInfoBean = new GmDonorInfoBean(getGmDataStoreVO());
    String strDHRIdforSub = "";
    String strSterFl = "";
    String strSubCompFl = "";
    String strLotCode = "";
    String strDateFormat = getCompDateFmt();
    StringBuffer sbQuery = new StringBuffer();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    sbQuery
        .append(" SELECT A.C408_DHR_ID ID,A.c408_udi_no UDINO, GET_VENDOR_NAME(A.C301_VENDOR_ID) VNAME, A.C301_VENDOR_ID VID, F.C301_LOT_CODE LCODE, ");
    sbQuery
        .append(" C.C401_PURCHASE_ORD_ID POID, get_part_attribute_value(D.C205_PART_NUMBER_ID,80134) CNUMFMT, ");
    sbQuery
        .append(" NVL(get_rule_value(D.C205_PART_NUMBER_ID,'SKIPCONTROLNUMVAL'),DECODE(get_part_attribute_value(D.C205_PART_NUMBER_ID,80132),'80130','Y')) SKIPFL, ");
    sbQuery
        .append(" A.C205_PART_NUMBER_ID PNUM, get_partdesc_by_company(A.C205_PART_NUMBER_ID) PDESC, ");
    // -- PC-2135 Inhouse MWO paperwork 2020 - Product Traveler and DHR Review add CUSER for user id
    sbQuery
        .append(" A.C408_QTY_RECEIVED QTYREC, A.C402_WORK_ORDER_ID WOID, A.C408_CONTROL_NUMBER CNUM, A.C408_CREATED_BY CUSER, ");
    sbQuery.append("  GET_USER_NAME(A.C408_CREATED_BY) UNAME, A.C408_MANF_DATE MDATE ");
    sbQuery.append(" ,A.C408_CREATED_DATE CDATE, C.C402_QTY_ORDERED QTYORD, ");
    sbQuery.append(" D.C202_PROJECT_ID PROJID, GET_CODE_NAME(D.C205_PACKAGING_PRIM) PACKPRIM, ");
    sbQuery
        .append(" GET_CODE_NAME(D.C205_PACKAGING_SEC) PACKSEC, GET_PROJECT_INSERT(A.C205_PART_NUMBER_ID) INSID, ");
    sbQuery
        .append(" A.C408_QTY_INSPECTED QTYINSP, A.C408_QTY_REJECTED QTYREJ, C.C402_SUB_COMPONENT_FL SUBFL, ");
    sbQuery
        .append(" A.C408_INSPECTED_BY INSPBY, A.C408_PACKAGED_BY PACKBY, A.C408_VERIFIED_BY VERIFYBY, ");
    sbQuery
        .append(" GET_USER_NAME(A.C408_INSPECTED_BY) INSPBYNM, GET_USER_NAME(A.C408_PACKAGED_BY) PACKBYNM, ");
    sbQuery.append(" GET_USER_NAME(A.C408_VERIFIED_BY) VERIFYBYNM, A.C408_QTY_PACKAGED QTYPACK, ");
    sbQuery
        .append(" A.C408_QTY_ON_SHELF QTYSHELF, A.C408_VERIFIED_BY VERIFYBY, E.C409_REJ_REASON REJREASON, ");
    sbQuery
        .append(" GET_USER_NAME(A.C408_VERIFIED_BY) VERIFYBYNM, E.C409_NCMR_ID NCMRID, C.C402_CRITICAL_FL CFL, ");
    sbQuery
        .append(" E.C409_STATUS_FL NCMRFL, E.C409_CLOSEOUT_QTY CLOSEQTY, C.C402_STERILIZATION_FL STERFL, ");
    sbQuery.append(" to_char(A.C408_CREATED_DATE,'" + strDateFormat
        + " hh:mi AM') RECTS, to_char(A.C408_INSPECTED_TS,'" + strDateFormat
        + " hh:mi AM') INSTS, ");
    sbQuery.append(" to_char(A.C408_PACKAGED_TS,'" + strDateFormat
        + " hh:mi AM') PACKTS, to_char(A.C408_VERIFIED_TS,'" + strDateFormat
        + " hh:mi AM') VERTS, ");
    sbQuery
        .append(" C.C408_DHR_ID WORKDHRID, D.C205_SUB_COMPONENT_FL SUBCOMPFL, A.C408_LABEL_SKIP_FL SKIPLBLFL, A.C408_PACK_SKIP_FL SKIPPACKFL, ");
    sbQuery.append(" D.C205_PRODUCT_CLASS PCLASS, GET_DOC_FOOTER (TO_CHAR(C.C402_CREATED_DATE, '");
    sbQuery.append(strDateFormat);
    sbQuery.append("'),'GM-G001') FOOTER,");
    sbQuery.append("  GET_DOC_FOOTER(to_char(A.C408_CREATED_DATE,'" + strDateFormat
        + "'),'GM-G008') RFFOOTER,"); // MFGDHR
    sbQuery.append("  GET_DOC_FOOTER(to_char(A.C408_CREATED_DATE,'" + strDateFormat
        + "'),'GM-G007') PRTFOOTER,"); // MFGDHR
    sbQuery
        .append(" D.C205_SUB_ASMBLY_FL SUBASMBFL, C.C402_FAR_FL FARFL, A.C408_PACKING_SLIP PKSLIP, A.C408_COMMENTS COMMENTS, ");
    sbQuery.append(" A.C408_STATUS_FL SFL,  A.c408_udi_verified UDIVERIFY,");
    sbQuery.append("GET_DOC_FOOTER(TO_CHAR(A.C408_CREATED_DATE, '");
    sbQuery.append(strDateFormat);
    sbQuery.append("'),'GM-G003') PTFOOTER,");
    sbQuery.append(" GET_DOC_FILE_NAME(TO_CHAR(A.C408_CREATED_DATE, '");
    sbQuery.append(strDateFormat);
    sbQuery.append("'),'GM-G003') PTFILENAME,");
    sbQuery.append(" A.C408_RECEIVED_DATE RECEIVEDDATE ");
    sbQuery.append(" ,A.C408_VOID_FL DHRVFL, ");
    sbQuery
        .append(" GET_CODE_NAME(E.C409_DISPOSITION) DISP, E.C409_RTS_FL RTSFL, GET_CODE_NAME(E.C409_RESPONSIBILITY) RESPON, ");
    sbQuery.append(" E.C409_DISP_REMARKS DISPREMARKS, GET_USER_NAME(E.C409_DISP_BY) DISPBY ");
    sbQuery.append(" ,GET_CODE_NAME(E.C901_REASON_TYPE) REASON_TYPE ");
    sbQuery.append(" ,E.C901_REASON_TYPE REASON_ID ");
    sbQuery.append(" ,E.C409_DISP_DATE DISPDATE");
    sbQuery
        .append(" ,GET_BACK_ORDER_FLAG(A.C205_PART_NUMBER_ID) BACKORDER_FLAG, A.c408_shipped_qty SHIPQTY");
    sbQuery
        .append(" ,GET_WO_UOM_TYPE_QTY(C.C402_WORK_ORDER_ID) UOMANDQTY, C.C402_WORK_ORDER_ID WOID ");
    sbQuery.append(" ,A.C408_LAST_UPDATED_DATE LDATE ");
    sbQuery.append(" ,GET_RULE_VALUE(A.C301_VENDOR_ID , 'DHRPAY') DHRVENDORNAME ");
    sbQuery
        .append(" ,get_potype_from_woid(C.C402_WORK_ORDER_ID) WOTYPE,get_log_flag (A.C402_WORK_ORDER_ID,1238) WLOG ");
    sbQuery.append(" ,CASE ");
    sbQuery.append(" WHEN A.c408_status_fl = '0' THEN 'C' ");
    sbQuery.append(" WHEN A.c408_status_fl = '1' THEN 'I' ");
    sbQuery.append(" WHEN A.c408_status_fl = '2' THEN 'P' ");
    sbQuery.append(" WHEN A.c408_status_fl = '3' THEN 'V' ");
    sbQuery.append(" END SMODE ");
    sbQuery.append(" ,get_rule_value(D.C205_PART_NUMBER_ID,'SKIP-OPER-PACK')SKIPPACKFLG ");
    sbQuery.append(" ,get_rule_value(D.C205_PART_NUMBER_ID,'DHR-RM-UPDATE')RMUPDATEFL ");
    sbQuery.append(" ,get_part_attribute_value(A.c205_part_number_id, 51070) FLGPNUMCHK ");
    sbQuery.append(",GET_DOC_FOOTER (TO_CHAR(A.C408_CREATED_DATE,'");
    sbQuery.append(strDateFormat);
    sbQuery.append("'),'GM-K001') INSFOOTER ");
    sbQuery.append(",GET_DOC_FOOTER (TO_CHAR(A.C408_CREATED_DATE,'");
    sbQuery.append(strDateFormat);
    sbQuery.append("'),'GM-M001') DRFOOTER ");
    sbQuery.append(",GET_DOC_FILE_NAME (TO_CHAR(A.C408_CREATED_DATE, '");
    sbQuery.append(strDateFormat);
    sbQuery.append("'),'GM-M001') DRFILENAME ");
    sbQuery.append(" ,C.c402_validation_fl VALDFL, A.c2550_expiry_date EXPDT, ");
    sbQuery
        .append(" GET_PARTNUM_MATERIAL_TYPE(A.C205_PART_NUMBER_ID) PRODTYPE, get_donor_number(A.c2540_donor_id) DONORNUMBER  ");
    sbQuery.append(" , GET_DOC_FILE_NAME (TO_CHAR(A.C408_CREATED_DATE,'");
    sbQuery.append(strDateFormat);
    sbQuery.append("'),'GM-K001') LPFILENAME ");// To get the JSP for labeling & Packaging
    sbQuery.append(" , GET_DOC_FILE_NAME (TO_CHAR (A.C408_CREATED_DATE,'" + strDateFormat
        + "'),'GM-G008') RFFILENAME "); // MFGDHR
    sbQuery.append(" , GET_DOC_FILE_NAME (TO_CHAR (A.C408_CREATED_DATE,'" + strDateFormat
        + "'),'GM-G007') PRTFILENAME ,a.C408_RW_FL RWFL"); // MFGDHR
    sbQuery.append(" , GET_PART_ATTR_VALUE_BY_COMP(A.C205_PART_NUMBER_ID,106040," + getCompId()
        + ") SERIALNUMNEEDFL");
    sbQuery.append(" , DECODE(get_part_attribute_value(A.C205_PART_NUMBER_ID,'4000517'),'103420','Y','N') VENDRDSNFL, G.C2060_DI_NUMBER DINUMBR");
    sbQuery.append(" , gm_pkg_pd_rpt_udi.get_part_udi_format (A.C205_PART_NUMBER_ID) UDIFORMAT ");
    sbQuery
        .append(" FROM T408_DHR A, T402_WORK_ORDER C , T205_PART_NUMBER D, T409_NCMR E, t301_vendor F, T2060_DI_PART_MAPPING G");
    sbQuery.append(" WHERE A.C408_DHR_ID like '%");
    sbQuery.append(strDHRId);
    sbQuery.append("' AND A.c402_work_order_id = C.C402_WORK_ORDER_ID ");
    sbQuery.append(" AND A.C301_VENDOR_ID  = F.C301_VENDOR_ID  ");
    sbQuery.append(" AND A.C205_PART_NUMBER_ID = D.C205_PART_NUMBER_ID ");
    sbQuery.append(" AND A.C205_PART_NUMBER_ID = G.C205_PART_NUMBER_ID(+) ");
    sbQuery.append(" AND A.C408_DHR_ID = E.C408_DHR_ID(+) ");
    //Removed company and plant to load the transaction details in different companies PMT-29683
    // Appending company filter
    //sbQuery.append(" AND A.C1900_COMPANY_ID = " + getCompId());
    //sbQuery.append(" AND A.c5040_plant_id = " + getCompPlantId());
    sbQuery.append(" AND A.C408_VOID_FL IS NULL ");
    sbQuery.append(" AND E.C409_VOID_FL (+) IS NULL ");
    log.debug("getdhrdetails--> " + sbQuery.toString());
    hmResult = GmCommonClass.parseNullHashMap(gmDBManager.querySingleRecord(sbQuery.toString()));
    if (hmResult.size() > 0) {
      strDHRIdforSub = (String) hmResult.get("WORKDHRID");
      strDHRIdforSub = strDHRIdforSub.equals("") ? strDHRId : strDHRIdforSub;
      strSterFl = GmCommonClass.parseNull((String) hmResult.get("STERFL"));
      strSubCompFl = GmCommonClass.parseNull((String) hmResult.get("SUBASMBFL"));
      strLotCode = GmCommonClass.parseNull((String) hmResult.get("LCODE"));
    }

    hmReturn.put("DHRDETAILS", hmResult); // DHR Details
    log.debug("DHRDETAILS==>" + hmResult.size());
    alResult = getSubDHRDetails(strDHRIdforSub, strSterFl, strSubCompFl);
    alLotListing = gmDonorInfoBean.fetchLotListing(strDHRId);
    hmReturn.put("LOTLIST", alLotListing);

    hmReturn.put("SUBDHRDETAILS", alResult); // Sub-DHR Details
    hmReturn.put("LCODE", strLotCode); // Vendor Lot Code



    return hmReturn;
  } // End of getDHRDetails

  /**
   * getSubDHRDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList getSubDHRDetails(String strDHRId, String strSterFl, String strSubCompFl)
      throws AppError {
    ArrayList alResult = new ArrayList();

    StringBuffer sbQuery = new StringBuffer();
    strDHRId = GmCommonClass.parseNull(strDHRId);
    strSterFl = GmCommonClass.parseNull(strSterFl);
    strSubCompFl = GmCommonClass.parseNull(strSubCompFl);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    if (strSterFl.equals("1") && strSubCompFl.equals("")) {
      sbQuery
          .append(" SELECT C408_DHR_ID SUBDHRID, C205_PART_NUMBER_ID SUBASMBID, C408_MANF_DATE MDATE, ");
      sbQuery
          .append(" get_partdesc_by_company(C205_PART_NUMBER_ID) PDESC, C408_CONTROL_NUMBER CNUM ");
      sbQuery.append(" FROM T408_DHR  T408");
      sbQuery.append(" WHERE C408_DHR_ID ='");
      sbQuery.append(strDHRId);
      sbQuery.append("'");
      sbQuery.append(" AND T408.C408_VOID_FL IS NULL ");
    } else {
      sbQuery
          .append(" SELECT C411_SUB_DHR_ID SUBDHRID, C206_SUB_ASMBLY_ID SUBASMBID, C411_MANF_DATE MDATE, ");
      sbQuery.append(" GET_PARTNUM_DESC(C206_SUB_ASMBLY_ID) PDESC, C411_CONTROL_NUMBER CNUM ");
      sbQuery.append(" FROM T411_SUB_DHR ");
      sbQuery.append(" WHERE C408_DHR_ID ='");
      sbQuery.append(strDHRId);
      sbQuery.append("' ORDER BY C411_SUB_DHR_ID ");
    }
    alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alResult;
  } // End of getSubDHRDetails

  /**
   * getDHRDashboard - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList getDHRDashboard(String strPOType) throws AppError {
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    /*
     * sbQuery.append(" SELECT	C408_DHR_ID ID, C205_PART_NUMBER_ID PARTNUM, C301_VENDOR_ID VID, ");
     * sbQuery.append(" GET_PARTNUM_DESC(C205_PART_NUMBER_ID) PDESC, C408_STATUS_FL FL, ");
     * sbQuery.append(
     * " GET_VENDOR_NAME(C301_VENDOR_ID) VNAME, to_char(C408_CREATED_DATE,GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) CDATE  "
     * ); sbQuery.append(" FROM T408_DHR T408 "); sbQuery.append(" WHERE C408_STATUS_FL < 4 ");
     * sbQuery.append(" AND T408.C408_VOID_FL IS NULL ");
     * sbQuery.append(" ORDER BY C301_VENDOR_ID, C408_CREATED_DATE DESC");
     */
    sbQuery
        .append(" SELECT	T408.C408_DHR_ID ID, T408.C205_PART_NUMBER_ID PARTNUM, T408.C301_VENDOR_ID VID, ");
    sbQuery.append(" GET_PARTNUM_DESC(T408.C205_PART_NUMBER_ID) PDESC, C408_STATUS_FL FL,");
    sbQuery.append(" GET_VENDOR_NAME(T408.C301_VENDOR_ID) VNAME, C408_CREATED_DATE CDATE ");
    sbQuery.append(" ,C408_LAST_UPDATED_DATE LDATE ,T408.C408_CONTROL_NUMBER CNUM ");
    sbQuery.append(" ,GET_MF_WO_MATREQ_STATUS(T408.C408_DHR_ID) MATSFL ");
    sbQuery
        .append(" ,T408.C408_QTY_RECEIVED QTYREC, T408.C408_SHIPPED_QTY QTYSHIP, GET_BACK_ORDER_FLAG(T408.C205_PART_NUMBER_ID) BACKORDER_FLAG ");
    sbQuery.append(" FROM T408_DHR T408, T402_WORK_ORDER T402, T401_PURCHASE_ORDER t401 ");
    sbQuery.append(" WHERE C408_STATUS_FL < 4 ");
    sbQuery.append(" AND T408.C408_VOID_FL IS NULL ");
    sbQuery.append(" and T402.C402_WORK_ORDER_ID = T408.C402_WORK_ORDER_ID ");
    sbQuery.append(" and t401.C401_PURCHASE_ORD_ID = t402.C401_PURCHASE_ORD_ID ");
    sbQuery.append(" and t401.C401_TYPE IN (");
    sbQuery.append(strPOType);
    sbQuery.append(")");
    // Appending company filter
    sbQuery.append(" AND  T408.C1900_COMPANY_ID = " + getCompId());
    sbQuery.append(" AND  T408.c5040_plant_id = " + getCompPlantId());
    if (strPOType.equals("3104,3105"))// Dashboard - Manufacturing DHRs
    {
      sbQuery.append(" ORDER BY T408.C205_PART_NUMBER_ID, UPPER(VNAME),  C408_CREATED_DATE DESC");
    } else {
      sbQuery.append(" ORDER BY UPPER(VNAME), T408.C205_PART_NUMBER_ID, C408_CREATED_DATE DESC");
    }
    log.debug(" Query is " + sbQuery.toString());

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());


    return alReturn;
  } // End of getDHRDashboard


  /**
   * updateDHRforSubControl - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap updateDHRforSubControl(String strDHRId, String strInputStr, String strUsername)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strMsg = "";
    HashMap hmReturn = new HashMap();


    gmDBManager.setPrepareString("GM_UPDATE_DHR_SUB_CONTROL", 4);
    /*
     * register out parameter and set input parameters
     */

    gmDBManager.registerOutParameter(4, java.sql.Types.CHAR);

    gmDBManager.setString(1, strDHRId);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strUsername);

    gmDBManager.execute();
    strMsg = gmDBManager.getString(4);
    gmDBManager.close();
    hmReturn.put("MSG", strMsg);


    return hmReturn;
  } // end of updateDHRforSubControl


  /**
   * updateDHRforInsp - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap updateDHRforInsp(String strDHRId, HashMap hmParam, String strUsername)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmJIRAProcessBean gmJIRAProcessBean = new GmJIRAProcessBean(getGmDataStoreVO());
    GmTicketBean gmTicketBean = new GmTicketBean();
    String strMsg = "";
    String strNCMRId = "";

    HashMap hmEvalDetails = new HashMap();
    HashMap hmEvalDetailsNew = new HashMap();

    // HashMap hmReturn = new HashMap();
    String strEmail = "";
    String strEmailnxt = "";

    HashMap hmParams = new HashMap();
    HashMap hmReturn = new HashMap();
    String strQtyInpsected = (String) hmParam.get("QTYISNP");
    String strQtyRejected = (String) hmParam.get("QTYREJ");
    String strInspectedBy = (String) hmParam.get("INSPBY");
    String strReason = (String) hmParam.get("REJREASON");
    String strStatusFl = (String) hmParam.get("STATUSFL");
    String strSterileFl = (String) hmParam.get("STERILEFL");
    String strReasonType = (String) hmParam.get("REJREASONTYPE");
    String strUpdStatusFlag = (String) hmParam.get("UPDSTATUSFL");
    String strUdiCheckFl = GmCommonClass.parseNull((String) hmParam.get("UDICHKFL"));
    gmDBManager.setPrepareString("GM_UPDATE_DHR_INSPECT", 13);

    // gmDBManager.setPrepareString("GM_UPDATE_DHR_INSPECT", 12);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(12, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(13, java.sql.Types.CHAR);
    /*
     * gmDBManager.registerOutParameter(11, java.sql.Types.CHAR);
     * gmDBManager.registerOutParameter(12, java.sql.Types.CHAR);
     */

    gmDBManager.setString(1, strDHRId);
    gmDBManager.setDouble(2, Double.parseDouble(strQtyInpsected));
    gmDBManager.setDouble(3, Double.parseDouble(strQtyRejected));
    gmDBManager.setInt(4, Integer.parseInt(strInspectedBy));
    gmDBManager.setInt(5, Integer.parseInt(strReasonType));
    gmDBManager.setString(6, strReason);
    gmDBManager.setString(7, strStatusFl);
    gmDBManager.setString(8, strSterileFl);
    gmDBManager.setString(9, strUsername);
    gmDBManager.setString(10, strUpdStatusFlag);
    gmDBManager.setString(11, strUdiCheckFl);
    gmDBManager.execute();
    // strNCMRId = gmDBManager.getString(11);
    // strMsg = gmDBManager.getString(12);
    strNCMRId = gmDBManager.getString(12);
    strMsg = gmDBManager.getString(13);

    hmReturn.put("NCMRID", strNCMRId);
    hmReturn.put("MSG", strMsg);
    gmDBManager.commit();
    /* code for testing email */
    ArrayList alEvalDtls = new ArrayList();
    GmDBManager gmDBManagerNew = new GmDBManager(getGmDataStoreVO());
    gmDBManagerNew.setPrepareString("GM_PKG_OP_NCMR.gm_fch_eval_email_det", 2);

    gmDBManagerNew.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManagerNew.setString(1, strNCMRId);
    gmDBManagerNew.execute();
    alEvalDtls = gmDBManagerNew.returnArrayList((ResultSet) gmDBManagerNew.getObject(2));
    ArrayList alTemp = new ArrayList(alEvalDtls);
    HashMap hmPars = new HashMap();

    for (int j = 0; j < alEvalDtls.size(); j++) {
      ArrayList alEvalPrint = new ArrayList();
      hmEvalDetails = (HashMap) alEvalDtls.get(j);
      strEmail = GmCommonClass.parseNull((String) hmEvalDetails.get("TO_EMAIL"));
      GmEmailProperties gmEmailProperties = getEmailProperties(TEMPLATE_NAME);
      gmEmailProperties.setRecipients(strEmail);
      GmJasperMail jasperMail = new GmJasperMail();
      jasperMail.setJasperReportName("/GmInHouseNCMREvalRemJob.jasper");

      for (int k = j; k < alTemp.size(); k++) {
        hmEvalDetailsNew = (HashMap) alTemp.get(k);
        strEmailnxt = GmCommonClass.parseNull((String) hmEvalDetailsNew.get("TO_EMAIL"));
        if (strEmail.equals(strEmailnxt)) {
          hmPars.put("EVALID", GmCommonClass.parseNull((String) hmEvalDetailsNew.get("EVALID")));
          hmPars.put("PNUM", GmCommonClass.parseNull((String) hmEvalDetailsNew.get("PNUM")));
          hmPars.put("PDESC", GmCommonClass.parseNull((String) hmEvalDetailsNew.get("PDESC")));
          hmPars.put("VNAME", GmCommonClass.parseNull((String) hmEvalDetailsNew.get("VNAME")));
          hmPars.put("DHRID", GmCommonClass.parseNull((String) hmEvalDetailsNew.get("DHRID")));
          hmPars.put("DQTY", GmCommonClass.parseNull((String) hmEvalDetailsNew.get("DQTY")));
          hmPars.put("NQTY", GmCommonClass.parseNull((String) hmEvalDetailsNew.get("NQTY")));
          hmPars.put("NRES", GmCommonClass.parseNull((String) hmEvalDetailsNew.get("NRES")));
          hmPars.put("ORIGINATOR", GmCommonClass.parseNull((String) hmEvalDetailsNew.get("UNAME")));
          hmPars.put("PROJID", GmCommonClass.parseNull((String) hmEvalDetailsNew.get("PROJID")));
          hmPars.put("PROJNM", GmCommonClass.parseNull((String) hmEvalDetailsNew.get("PROJNM")));
          hmPars.put("USERID", GmCommonClass.parseNull((String) hmEvalDetailsNew.get("USERID")));
          alEvalPrint.add(GmCommonClass.parseNullHashMap(hmPars));
          j = k;

        }
      }
      jasperMail.setReportData(GmCommonClass.parseNullArrayList(alEvalPrint));

      jasperMail.setEmailProperties(gmEmailProperties);

      hmReturn = jasperMail.sendMail();
    }
    //PC-2400 JMS call to create Jira Ticket when Evaluation ID is created if rejected qty is greater than 0
    if(!strQtyRejected.equals("0")) {
    	//hmPars.put("ISSUETYPE", "CREATE"); //To Create Ticket for Evaluation ID
    	//gmJIRAProcessBean.createJiraticketForNCMR(hmPars);
    	gmTicketBean.createNCMREvalTicket(hmPars);
    }
    gmDBManagerNew.close();

    return hmReturn;
  } // end of updateDHRforInsp

  public GmEmailProperties getEmailProperties(String strTemplate) {
    GmEmailProperties emailProps = new GmEmailProperties();
    emailProps
        .setSender(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.FROM));
    emailProps.setSubject(GmCommonClass.getEmailProperty(strTemplate + "."
        + GmEmailProperties.SUBJECT));
    emailProps.setMimeType(GmCommonClass.getEmailProperty(strTemplate + "."
        + GmEmailProperties.MIME_TYPE));
    return emailProps;
  }

  /**
   * updateDHRforPackVerify - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap updateDHRforPackVerify(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmLocationBean gmLocationBean = new GmLocationBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    String strDHRId = GmCommonClass.parseNull((String) hmParam.get("DHRID"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strWOId = GmCommonClass.parseNull((String) hmParam.get("WOID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("STRINPUTSTRING"));
    String strSkipLabelFl = GmCommonClass.parseNull((String) hmParam.get("SKIPLABELFLAG"));
    String strPartSterFl = GmCommonClass.parseNull((String) hmParam.get("PARTSTERFLAG"));
    String strUsername = GmCommonClass.parseNull((String) hmParam.get("USERIDTRANS"));
    String strAction = GmCommonClass.parseNull((String) hmParam.get("ACTION"));
    String strUpdStatusFl = GmCommonClass.parseNull((String) hmParam.get("UPDSTATUSFL"));
    String strPnumLcn = GmCommonClass.parseNull((String) hmParam.get("PNUMLCNSTR"));
    String strOperation = GmCommonClass.parseNull((String) hmParam.get("OPERATION"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strUdiCheckFl = GmCommonClass.parseNull((String) hmParam.get("UDICHKFL"));

    gmDBManager.setPrepareString("GM_UPDATE_DHR_PKVER", 10);

    gmDBManager.setString(1, strDHRId);
    gmDBManager.setString(2, strPartNum);
    gmDBManager.setString(3, strWOId);
    gmDBManager.setString(4, strInputStr);
    gmDBManager.setString(5, strSkipLabelFl);
    gmDBManager.setString(6, strPartSterFl);
    gmDBManager.setInt(7, Integer.parseInt(strUsername));
    gmDBManager.setString(8, strAction);
    gmDBManager.setString(9, strUpdStatusFl);
    gmDBManager.setString(10, strUdiCheckFl);
    gmDBManager.execute();

    gmDBManager.commit();

    return hmReturn;
  } // end of updateDHRforInsp



  /**
   * getNCMRDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap getNCMRDetails(String strNCMRId, String strAction) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    ArrayList alAction = new ArrayList();
    ArrayList alResp = new ArrayList();
    ArrayList alEmp = new ArrayList();
    String strCompanyID = getCompId();
    GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
    log.debug("Inside getNCMRDetails");
    StringBuffer sbQuery = new StringBuffer();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    sbQuery
        .append(" SELECT A.C409_NCMR_ID NCMRID,B.C401_PURCHASE_ORD_ID POID, A.C408_DHR_ID ID, GET_USER_NAME(C409_CLOSEOUT_BY) CLSBY, GET_VENDOR_NAME(A.C301_VENDOR_ID) VNAME, ");
    sbQuery.append(" A.C205_PART_NUMBER_ID PNUM, GET_PARTNUM_DESC(A.C205_PART_NUMBER_ID) PDESC, ");
    sbQuery.append(" A.C402_WORK_ORDER_ID WOID, GET_USER_NAME(A.C409_CREATED_BY) UNAME,");
    sbQuery
        .append(" B.C408_CONTROL_NUMBER CNUM, B.C408_QTY_RECEIVED QTYREC, B.C408_QTY_INSPECTED QTYINSP, ");
    sbQuery
        .append(" A.C409_CREATED_DATE CDATE, B.C408_QTY_REJECTED QTYREJ, A.C409_REJ_REASON REJREASON, ");
    sbQuery
        .append(" GET_CODE_NAME(A.C409_DISPOSITION) DISP, A.C409_RTS_FL RTSFL, GET_CODE_NAME(A.C409_RESPONSIBILITY) RESPON, ");
    sbQuery
        .append(" A.C409_DISP_REMARKS REMARKS, GET_USER_NAME(A.C409_DISP_BY) DISPBY, A.C409_CLOSEOUT_QTY CLOSEQTY, ");
    sbQuery
        .append(" A.C409_DISP_DATE DDATE, A.C409_CLOSEOUT_DATE CLOSEDATE, A.C409_CAPA_FL CAPAFL, ");
    sbQuery.append(" A.C409_EVAL_REMARKS EVALREMARKS, A.C409_EVAL_DATE EVALDATE,");
    sbQuery
        .append("  A.c409_status_fl  STATUS , GET_PARTNUM_MATERIAL_TYPE(B.C205_PART_NUMBER_ID) PRODTYPE, get_donor_number(B.c2540_donor_id) DONORNUMBER, A.C301_VENDOR_ID VID");
    sbQuery.append("  , get_part_attr_value_by_comp(B.C205_PART_NUMBER_ID,'106040'," + strCompanyID
        + ") SERIALNUMNEEDFL");
    sbQuery.append(" FROM T409_NCMR A, T408_DHR B ");
    sbQuery.append(" WHERE A.C409_NCMR_ID ='");
    sbQuery.append(strNCMRId);
    sbQuery.append("' AND A.C408_DHR_ID = B.C408_DHR_ID");
    // Appending company filter
    sbQuery.append(" AND A.C1900_COMPANY_ID = " + strCompanyID);
    sbQuery.append(" AND A.c5040_plant_id = " + getCompPlantId());
    sbQuery.append(" AND B.C408_VOID_FL IS NULL ");
    sbQuery.append(" AND A.C409_VOID_FL IS NULL ");
    hmResult = gmDBManager.querySingleRecord(sbQuery.toString());


    hmReturn.put("NCMRDETAILS", hmResult); // NCMR Details

    if (strAction.equals("UpdateNCMR")) {
      alAction = gmCommon.getCodeList("NCMRA");
      alResp = gmCommon.getCodeList("NCMRR");
      alEmp = gmLogon.getEmployeeList();

      hmReturn.put("ACTION", alAction);
      hmReturn.put("RESPON", alResp);
      hmReturn.put("EMP", alEmp);
    }

    log.debug(" Query value is " + sbQuery.toString());
    return hmReturn;
  } // End of getNCMRDetails

  public HashMap updateNCMRRejQty(String strDHRId, String strQtyRej, String strUserId)
      throws AppError {
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_op_ncmr.gm_change_rej_qty", 3);

    gmDBManager.setString(1, strDHRId);
    gmDBManager.setString(2, strQtyRej);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();

    hmReturn.put("ERRMSG", "Success");
    gmDBManager.commit();


    return hmReturn;

  }

  /**
   * getNCMRDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap updateEvalToNCMR(String strNCMRId, HashMap hmParam, String userId) throws AppError {
    // int strType=51062;
    HashMap hmReturn = new HashMap();
    String strEvalRemarks = GmCommonClass.parseNull((String) hmParam.get("EVALREMARKS"));
    String strEvalDate = GmCommonClass.parseNull((String) hmParam.get("EVALDATE"));
    String strHMode = GmCommonClass.parseNull((String) hmParam.get("HMODE"));
    log.debug("strNCMRId :" + strNCMRId + " userId:" + userId);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_op_ncmr.gm_sav_ncmr", 5);

    gmDBManager.registerOutParameter(1, OracleTypes.CHAR);// java.sql.Types.CHAR
    gmDBManager.setString(1, strNCMRId);
    gmDBManager.setString(2, strEvalRemarks);
    gmDBManager.setString(3, strEvalDate);
    gmDBManager.setString(4, userId);
    gmDBManager.setString(5, strHMode);
    gmDBManager.execute();
    String strNCMRID = GmCommonClass.parseNull(gmDBManager.getString(1));
    log.debug("NCMRID is:" + strNCMRID);
    hmReturn.put("NCMRID", strNCMRID);
    gmDBManager.commit();

    return hmReturn;
  } // End of getNCMRDetails

  /**
   * getNCMRDashboard - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList getNCMRDashboard() throws AppError {
    ArrayList alReturn = new ArrayList();


    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT	C409_NCMR_ID NID, C408_DHR_ID DHRID, C205_PART_NUMBER_ID PNUM, GET_PARTNUM_DESC(C205_PART_NUMBER_ID) PDESC,");
    sbQuery.append(" C402_WORK_ORDER_ID WORKID, C301_VENDOR_ID VID, C409_CREATED_DATE CDATE, ");
    sbQuery.append(" GET_VENDOR_NAME(C301_VENDOR_ID) VNAME,C409_STATUS_FL SFL ");
    sbQuery.append(" FROM T409_NCMR");
    sbQuery.append(" WHERE C409_STATUS_FL < 3 ");
    sbQuery.append(" AND C409_VOID_FL IS NULL ");
    sbQuery.append(" ORDER BY UPPER(VNAME), C409_CREATED_DATE DESC ");

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());


    return alReturn;
  } // End of getNCMRDashboard



  /**
   * getReturnsDashboard - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList getReturnsDashboard(String strStatus, String strAccCurrId) throws AppError {
    ArrayList alReturn = new ArrayList();

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      String strQueryReturnsDashBoard = getReturnsDashBoardQuery(strStatus, strAccCurrId);
      alReturn = gmDBManager.queryMultipleRecords(strQueryReturnsDashBoard);

    } catch (Exception e) {
      throw new AppError(e);
    }
    return alReturn;
  } // End of getReturnsDashboard

  public RowSetDynaClass getReturnsDashboardDyna(String strStatus, String strAccCurrId)
      throws AppError {
    RowSetDynaClass resultSet = null;

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      String strQueryReturnsDashBoard = getReturnsDashBoardQuery(strStatus, strAccCurrId);
      resultSet = gmDBManager.QueryDisplayTagRecordset(strQueryReturnsDashBoard);
      log.debug("RowSetDyna result Set is  " + resultSet.getRows().toArray());

    } catch (Exception e) {
      throw new AppError(e);
    }
    return resultSet;
  } // End of getReturnsDashboard


  private String getReturnsDashBoardQuery(String strStatus, String strAccCurrId) {
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT    T506.C506_RMA_ID RAID, T506.C506_COMMENTS COMMENTS, T506.C506_CREATED_DATE CDATE, ");
    sbQuery
        .append(" GET_USER_NAME(T506.C506_CREATED_BY) PER, GET_CODE_NAME(T506.C506_TYPE) TYPE, T506.C506_TYPE TYPEID, ");
    sbQuery.append(" GET_CODE_NAME(T506.C506_REASON) REASON, T506.C506_EXPECTED_DATE EDATE,");
    sbQuery
        .append(" T506.C701_DISTRIBUTOR_ID DID ,GET_DISTRIBUTOR_NAME(T506.C701_DISTRIBUTOR_ID) DNAME, ");
    sbQuery.append(" T506.C704_ACCOUNT_ID ACCID, GET_ACCOUNT_NAME(T506.C704_ACCOUNT_ID) ANAME, ");
    sbQuery.append(" T506.C501_ORDER_ID ORDID, T506.C501_REPROCESS_DATE RPDATE,");
    sbQuery
        .append(" DECODE(T506.C506_STATUS_FL,0,'-',1,'Y') STATUS_FL, NVL(GET_TOTAL_RETURN_AMT(T506.C506_RMA_ID),0) RAMT, ");
    sbQuery.append(" C506_RETURN_DATE RDATE, GET_SET_NAME(T506.C207_SET_ID) SNAME ");
    sbQuery.append(" ,T506.C501_ORDER_ID PARENTOID ");

    if (strStatus.equals("ACCT")) {
      sbQuery.append(" ,t101.C101_PARTY_NM paracctnm,t101.c101_party_id paracctid");
      sbQuery.append(" FROM T506_RETURNS T506 ,t704_account t704, t101_party t101 ,T501_ORDER t501 ");
      sbQuery.append(" WHERE T506.C506_VOID_FL IS NULL ");
      sbQuery.append(" AND T506.c704_account_id = t704.c704_account_id AND T501.C501_ORDER_ID(+) = T506.C501_ORDER_ID AND T501.C501_VOID_FL IS NULL  ");
      sbQuery.append("  AND t704.c101_party_id = t101.c101_party_id ");
      sbQuery.append("  AND t101.c101_void_fl IS NULL ");
      sbQuery.append("  AND T506.c1900_company_id ='" + getGmDataStoreVO().getCmpid() + "' ");
      sbQuery.append(" AND t704.c901_currency = " + strAccCurrId);
    } else {
      sbQuery.append(" FROM T506_RETURNS T506 ");
      sbQuery.append(" WHERE T506.C506_VOID_FL IS NULL ");
      sbQuery.append("   AND T506.C5040_PLANT_ID = '" + getCompPlantId() + "'");
    }

    if (strStatus.equals("ACCT")) {
    	sbQuery.append(" AND t704.c704_account_id =  t501.c704_account_id(+) ");
    	sbQuery.append(" AND NVL(t704.c901_account_type, -999) <> 26240482 "); // PMT-31253 added this line to exclude OUS RAs showing in US Dashboard
      sbQuery.append(" AND T506.C506_AC_CREDIT_DT IS NULL AND T506.C506_TYPE = '3300' ");
      sbQuery.append(" AND T506.C506_REASON NOT IN (3316,3317) ");
      sbQuery.append(" ORDER BY T506.C506_RMA_ID DESC ");
    } else if (strStatus.equals("CUSTSER")) {
      sbQuery.append(" AND T506.C506_STATUS_FL = 1 ");
      sbQuery.append(" ORDER BY T506.C506_RMA_ID DESC ");
    } else if (strStatus.equals("OPERALL")) {
      sbQuery.append(" AND T506.C506_STATUS_FL = 0 ");
      sbQuery.append(" ORDER BY T506.C506_TYPE, DNAME, ANAME, T506.C506_RMA_ID DESC ");
    } else if (strStatus.equals("REPROCESS")) {
      sbQuery.append(" AND T506.C506_STATUS_FL IN (1,2) AND T506.C501_REPROCESS_DATE IS NULL ");
      sbQuery
          .append(" AND 0 < (SELECT SUM(C507_ITEM_QTY) FROM T507_RETURNS_ITEM T507 WHERE T507.C506_RMA_ID = T506.C506_RMA_ID) ");
      sbQuery.append(" ORDER BY T506.C506_TYPE,T506.C506_RMA_ID DESC ");
    }

    log.debug(" Query value is " + sbQuery.toString());
    return sbQuery.toString();
  }

  /**
   * getSetBuildDashboard - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList getSetBuildDashboard(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    String strStatFlag = (String) hmParam.get("SFL");
    String strVeriFlag = (String) hmParam.get("VFL");
    String strSetId = (String) hmParam.get("SETID");
    String strConsignId = (String) hmParam.get("CONID");
    String strReprocessID = (String) hmParam.get("REPROCESSID");
    String strCompPlantFilter = "";
    String strDateFmt = "";
    strCompPlantFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "BUILT_SET_DASH"));/*
                                                                                            * BUILT_SET_DASH
                                                                                            * - To
                                                                                            * fetch
                                                                                            * the
                                                                                            * records
                                                                                            * for
                                                                                            * EDC
                                                                                            * entity
                                                                                            * countries
                                                                                            * based
                                                                                            * on
                                                                                            * plant
                                                                                            */
    strCompPlantFilter = strCompPlantFilter.equals("") ? getCompId() : getCompPlantId();
    strDateFmt = getCompDateFmt();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT A.C504_CONSIGNMENT_ID CID, C.C520_REQUEST_ID REQID, B.C207_SET_ID SID, B.C207_SET_ID ||'/'||B.C207_SET_NM SNAME, ");
    sbQuery.append(" GET_USER_NAME(A.C504_CREATED_BY) PER, to_char(A.C504_CREATED_DATE, '"
        + strDateFmt + "' ) IDATE, to_char(C.C520_REQUIRED_DATE,'" + strDateFmt + "') REQDATE, ");
    sbQuery.append(" A.C504_COMMENTS COMMENTS, A.C504_STATUS_FL SFL, C504_REPROCESS_ID REPROCID, ");
    sbQuery.append(" A.C504_VERIFY_FL VFL, ");
    sbQuery.append(" get_code_name(C.C901_REQUEST_SOURCE) REQSRC, ");
    sbQuery
        .append(" DECODE(C504_STATUS_FL,0,'Initiated', 1, 'WIP', 2, 'Pending Verification', 'Pending Release') STATUS ");
    sbQuery.append(" FROM T504_CONSIGNMENT A, T207_SET_MASTER B, T520_REQUEST C ");
    sbQuery
        .append(" WHERE A.C207_SET_ID = B.C207_SET_ID AND A.C520_REQUEST_ID = C.C520_REQUEST_ID AND A.C207_SET_ID = C.C207_SET_ID ");

    if (!strStatFlag.equals("")) {
      sbQuery.append(" AND A.C504_STATUS_FL IN (");
      // sbQuery.append(strStatFlag);
      sbQuery.append(" '" + GmCommonClass.getStringWithQuotes(strStatFlag) + "'");
      sbQuery.append(")");
    } else {
      sbQuery.append(" AND A.C504_STATUS_FL < 3");
    }

    if (!strVeriFlag.equals("")) {
      sbQuery.append(" AND A.C504_VERIFY_FL = '");
      sbQuery.append(strVeriFlag);
      sbQuery.append("' ");
    } else {
      sbQuery.append(" AND A.C504_VERIFY_FL = '0'");
    }


    if (!strSetId.equals("")) {
      sbQuery.append(" AND B.C207_SET_ID = '");
      sbQuery.append(strSetId);
      sbQuery.append("' ");
    }

    if (!strConsignId.equals("")) {
      sbQuery.append(" AND A.C504_CONSIGNMENT_ID = '");
      sbQuery.append(strConsignId);
      sbQuery.append("' ");
    }

    /* reprocess */

    if (!strReprocessID.equals("")) {
      sbQuery.append(" AND A.C504_REPROCESS_ID like'%");
      sbQuery.append(strReprocessID);
      sbQuery.append("' ");
    }


    sbQuery.append(" AND C504_VOID_FL IS NULL ");
    sbQuery.append(" AND ( A.C1900_COMPANY_ID = " + strCompPlantFilter);
    sbQuery.append(" OR    A.C5040_PLANT_ID = " + strCompPlantFilter);
    sbQuery.append(") ");
    sbQuery.append(" ORDER BY B.C207_SET_ID, A.C504_CREATED_DATE DESC");

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    log.debug(" Query for getSetBuildDashboard " + sbQuery.toString());

    return alReturn;
  } // End of getSetBuildDashboard


  /**
   * loadReturnsSavedSetMaster - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadReturnsSavedSetMaster(String strRAId, String strAction) throws AppError {
    ArrayList alReturn = new ArrayList();
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT	C205_PART_NUMBER_ID PNUM, GET_PARTNUM_DESC(C205_PART_NUMBER_ID) PDESC, ");
    sbQuery.append(" GET_SET_PARTNUM_QTY_RETURN(C506_RMA_ID,C205_PART_NUMBER_ID) QTY, ");
    sbQuery.append(" C507_ITEM_QTY IQTY, C507_CONTROL_NUMBER CNUM, C507_ITEM_PRICE PRICE,  ");
    sbQuery.append(" C507_ORG_CONTROL_NUMBER ORG_CNUM , C901_TYPE ITYPE ");
    sbQuery.append(" FROM T507_RETURNS_ITEM ");
    sbQuery.append(" WHERE C506_RMA_ID = '");
    sbQuery.append(strRAId);
    sbQuery.append("'");
    if (strAction.equals("Reprocess")) {
      sbQuery.append(" AND C507_STATUS_FL = 'C' ");
    }
    sbQuery.append(" ORDER BY C205_PART_NUMBER_ID ");

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    hmReturn.put("SETLOAD", alReturn);


    return hmReturn;
  } // End of loadReturnsSavedSetMaster


  /**
   * saveReturnsSetMaster - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap saveReturnsSetMaster(String strRAId, String strInputString, String strRetDate,
      String strFlag, String strUsername) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    String strMsg = "";

    HashMap hmReturn = new HashMap();


    gmDBManager.setPrepareString("GM_SAVE_RETURN_SET_BUILD", 6);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(6, java.sql.Types.CHAR);
    gmDBManager.setString(1, strRAId);
    gmDBManager.setString(2, strInputString);
    gmDBManager.setString(3, strRetDate);
    gmDBManager.setString(4, strFlag);
    gmDBManager.setString(5, strUsername);

    gmDBManager.execute();
    strMsg = gmDBManager.getString(6);
    gmDBManager.commit();

    hmReturn.put("MSG", strMsg);


    return hmReturn;
  } // end of saveReturnsSetMaster


  /**
   * saveReconfigureSets - This method is used to reconfigure the set list if the Set Master was
   * changed and also to reload control numbers from Master CN's -- Added 4/27/2007 James
   * 
   * @param String strConsignId - COntains the original CN number
   * @param String strSetId - Contains the Set ID. This is used to refer T208_SET_DETAILS to get
   *        latest config
   * @param String strReloadId - To pull Control Numbers from an existing Master CN
   * @param String strAction - Either Reconfigure or Reload
   * @return String
   * @exception AppError
   **/
  public String saveReconfigureSets(String strConsignId, String strSetId, String strReloadId,
      String strAction) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();

    String strMsg = "";

    gmDBManager.setPrepareString("GM_SAVE_RECONFIGURE_SET_BUILD", 5);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);
    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strSetId);
    gmDBManager.setString(3, strReloadId);
    gmDBManager.setString(4, strAction);

    gmDBManager.execute();
    strMsg = gmDBManager.getString(5);
    gmDBManager.commit();

    return strMsg;
  } // end of saveReconfigureSets

  /**
   * updateNCMRforDisp - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap updateNCMRforDisp(String strNCMRId, HashMap hmParam, String strUsername)
      throws AppError {


    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    int intSize = 0;
    String strMsg = "";
    String strDispAction = "";
    String strRtsFl = "";
    String strRespon = "";
    String strRemarks = "";
    String strAuthBy = "";
    String strCloseQty = "";
    String strDispDate = "";
    String strPartNum = "";
    String strQtyRej = "";
    String strReason = "";
    String strVendor = "";
    String strControl = "";
    String strRejectionReason = "";
    HashMap hmReturn = new HashMap();
    HashMap hmEmailDetails = new HashMap();



    strDispAction = GmCommonClass.parseNull((String) hmParam.get("ACTION"));
    strRtsFl = GmCommonClass.parseNull((String) hmParam.get("RTSFL"));
    strRespon = GmCommonClass.parseNull((String) hmParam.get("RESPON"));
    strRemarks = GmCommonClass.parseNull((String) hmParam.get("REMARKS"));
    strAuthBy = GmCommonClass.parseNull((String) hmParam.get("AUTHBY"));
    strCloseQty = GmCommonClass.parseZero((String) hmParam.get("CLOSEQTY"));
    strDispDate = GmCommonClass.parseNull((String) hmParam.get("DISPDT"));
    strRejectionReason = GmCommonClass.parseNull((String) hmParam.get("REJREASON"));



    gmDBManager.setPrepareString("GM_UPDATE_NCMR_ACTION", 11);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(11, java.sql.Types.CHAR);
    gmDBManager.setString(1, strNCMRId);
    gmDBManager.setInt(2, Integer.parseInt(strDispAction));
    gmDBManager.setString(3, strRtsFl);
    gmDBManager.setInt(4, Integer.parseInt(strRespon));
    gmDBManager.setString(5, strRemarks);
    gmDBManager.setString(6, strAuthBy);
    gmDBManager.setInt(7, Integer.parseInt(strCloseQty));
    gmDBManager.setString(8, strDispDate);
    gmDBManager.setString(9, strUsername);
    gmDBManager.setString(10, strRejectionReason);
    gmDBManager.execute();
    strMsg = gmDBManager.getString(11);
    gmDBManager.commit();


    hmReturn = getNCMRDetails(strNCMRId, "");
    hmReturn = (HashMap) hmReturn.get("NCMRDETAILS");


    strReason = GmCommonClass.parseNull((String) hmReturn.get("REJREASON"));
    strDispAction = GmCommonClass.parseNull((String) hmReturn.get("DISP"));
    // strDispDate = GmCommonClass.parseNull((String) hmReturn.get("DDATE"));
    strDispDate =
        GmCommonClass.getStringFromDate((java.util.Date) hmReturn.get("DDATE"), getCompDateFmt());
    strAuthBy = GmCommonClass.parseNull((String) hmReturn.get("DISPBY"));
    strPartNum = GmCommonClass.parseNull((String) hmReturn.get("PNUM"));
    strQtyRej = GmCommonClass.parseNull((String) hmReturn.get("QTYREJ"));
    strVendor = GmCommonClass.parseNull((String) hmReturn.get("VNAME"));
    strRtsFl = GmCommonClass.parseZero((String) hmReturn.get("RTSFL"));
    strRtsFl = strRtsFl.equals("1") ? "Yes" : "No";
    strRemarks = GmCommonClass.parseNull((String) hmReturn.get("REMARKS"));
    strControl = GmCommonClass.parseNull((String) hmReturn.get("CNUM"));

    // Start: Mihir Added to auto email when NCMR is generated
    ArrayList alReturn = new ArrayList();
    HashMap hmDetails = new HashMap();
    StringBuffer sbPartNCMRsMailMsg = new StringBuffer();
    alReturn = GmCommonClass.parseNullArrayList(fetchPartNCMRs(strPartNum));
    intSize = alReturn.size();
    if (intSize > 0) {
      sbPartNCMRsMailMsg
          .append(" <tr>----------------------------------------------------------------------</tr>");
      sbPartNCMRsMailMsg.append(" <tr>NCMRs generated for the same part number:</tr>");
      sbPartNCMRsMailMsg.append(" <tr><td>DATE ISSUED </td><td>NCMR ID</td><td>&nbsp;&nbsp;&nbsp;VENDOR</td></tr>");
      for (int i = 0; i < intSize; i++) {
        hmDetails = (HashMap) alReturn.get(i);
        sbPartNCMRsMailMsg.append(" <tr><td>");
        sbPartNCMRsMailMsg.append(GmCommonClass.getStringFromDate((Date) hmDetails.get("CDATE"),
            getCompDateFmt()));
        sbPartNCMRsMailMsg.append("  </td><td>");
        sbPartNCMRsMailMsg.append((String) hmDetails.get("NCMRID"));
        sbPartNCMRsMailMsg.append(" </td><td>&nbsp;&nbsp;&nbsp;");
        sbPartNCMRsMailMsg.append((String) hmDetails.get("VNAME"));
        sbPartNCMRsMailMsg.append(" </td></tr>");
      }
      sbPartNCMRsMailMsg
          .append(" <tr>----------------------------------------------------------------------</tr>");
      log.debug(sbPartNCMRsMailMsg.toString());

    }// End: Mihir Added to auto email when NCMR is generated

    String strMailIds = "";
    String strCCMailIds = "";

    HashMap hmRuleData = new HashMap();
    hmRuleData.put("RULEID", "NCMR");
    hmRuleData.put("RULEGROUP", "EMAIL");

    strMailIds =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("NCMR", "EMAIL", getCompId()));
    strMailIds =
        strMailIds.equals("") ? GmCommonClass.parseNull(gmCommonBean.getRuleValue(hmRuleData))
            : strMailIds;
    log.debug("strMailIds: " + strMailIds);

    StringBuffer sbMailMsg = new StringBuffer();
    sbMailMsg.append("<html><body ><table width='100%' cellpadding='0' cellspacing='0' border='0'>");
    sbMailMsg.append("Hi,");
    sbMailMsg.append("<tr>An action was taken on an NCMR. Following are the details:</tr>");
    sbMailMsg.append(" <tr><td>NCMR ID: </td>");
    sbMailMsg.append(" <td> ");
    sbMailMsg.append(strNCMRId);
    sbMailMsg.append(" </td></tr> ");
    sbMailMsg.append(" <tr><td>Action Taken On: </td>");
    sbMailMsg.append(" <td> ");
    sbMailMsg.append(strDispDate);
    sbMailMsg.append(" </td></tr> ");
    sbMailMsg.append(" <tr><td>Action Taken: </td>");
    sbMailMsg.append(" <td> ");
    sbMailMsg.append(strDispAction);
    sbMailMsg.append(" </td></tr> ");
    sbMailMsg.append(" <tr><td>Return to Supplier ?:</td> ");
    sbMailMsg.append(" <td> ");
    sbMailMsg.append(strRtsFl);
    sbMailMsg.append(" </td></tr> ");
    sbMailMsg.append(" <tr><td>Authorized By: </td>");
    sbMailMsg.append(" <td> ");
    sbMailMsg.append(strAuthBy);
    sbMailMsg.append(" </td></tr> ");
    sbMailMsg.append(" <tr><td>Remarks: </td>");
    sbMailMsg.append(strRemarks);
    sbMailMsg.append(" </td></tr> ");
    sbMailMsg.append(" <tr><td>Part Number: </td> ");
    sbMailMsg.append(strPartNum);
    sbMailMsg.append(" </td></tr> ");
    sbMailMsg.append(" <tr><td>Control Number: </td> ");
    sbMailMsg.append(strControl);
    sbMailMsg.append(" </td></tr> ");
    sbMailMsg.append("<tr><td>Qty Rejected: </td> ");
    sbMailMsg.append(strQtyRej);
    sbMailMsg.append(" </td></tr> ");
    sbMailMsg.append(" <tr><td>Reason for Rejection: </td>");
    sbMailMsg.append(strReason);
    sbMailMsg.append(" </td></tr> ");
    sbMailMsg.append(" <tr><td>Vendor: </td> ");
    sbMailMsg.append(strVendor);
    sbMailMsg.append(" </td></tr> ");
    sbMailMsg.append(" <tr> ");
    sbMailMsg.append(sbPartNCMRsMailMsg);
    sbMailMsg.append(" </tr> ");
    sbMailMsg.append(" <tr>**Please DO NOT respond to this mail**</tr></table></body></html>");
    // sbMailMsg.append(" \n\nFor Questions/Clarifications regarding this email, contact Barry McOwen / Keith Perkins");

    hmReturn.put("MSG", strMsg);
    hmEmailDetails.put("strFrom", "GMEP<gmep@globusmedical.com>");
    hmEmailDetails.put("strTo", GmCommonClass.StringtoArray(strMailIds, ";"));
    hmEmailDetails.put("strCc", GmCommonClass.StringtoArray(strCCMailIds, ";"));
    hmEmailDetails.put("strSubject", "NCMR Action Taken: "+ strNCMRId);
    hmEmailDetails.put("strMessageDesc", sbMailMsg.toString()); 
    hmEmailDetails.put("strMimeType", "text/html"); 
    try {
    	GmCommonClass.sendMail(hmEmailDetails);
      } catch (Exception ex) {
        log.error("Exception in sending in updateNCMRforDisp" + ex.getMessage());
      }
    return hmReturn;
  } // end of updateNCMRforDisp


  /**
   * updateNCMRforCloseout - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap updateNCMRforCloseout(String strNCMRId, String strCloseDate, String strCAPAFl,
      String strUsername) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strMsg = "";

    HashMap hmReturn = new HashMap();


    gmDBManager.setPrepareString("GM_UPDATE_NCMR_CLOSE", 5);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);
    gmDBManager.setString(1, strNCMRId);
    gmDBManager.setString(2, strCloseDate);
    gmDBManager.setString(3, strCAPAFl);
    gmDBManager.setString(4, strUsername);

    gmDBManager.execute();
    strMsg = gmDBManager.getString(5);
    gmDBManager.commit();
    hmReturn.put("MSG", strMsg);

    return hmReturn;
  } // end of updateNCMRforCloseout


  /**
   * loadSetConsignAndReturnReport - This method will
   * 
   * @param String strFrmDate
   * @param String strToDate
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadSetConsignAndReturnReport(String strFrmDate, String strToDate) throws AppError {
    ArrayList alReturn = new ArrayList();
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    String strCompPlantFilter = "";
    strCompPlantFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "SETS_TRANSACTIONS"));// LOAD_SET_LOG-Rule
                                                                                              // Value:=
                                                                                              // plant.
    strCompPlantFilter = strCompPlantFilter.equals("") ? getCompId() : getCompPlantId();
    sbQuery.append(" SELECT T207.C207_SET_ID SETID, T207.C207_SET_NM SNAME, ");
    sbQuery.append(" GET_SET_COUNT(T207.C207_SET_ID,'CONSIGN', ");
    sbQuery.append("TO_DATE('" + strFrmDate + "','" + getCompDateFmt() + "'),");
    sbQuery.append("TO_DATE('" + strToDate + "','" + getCompDateFmt() + "'), '");
    sbQuery.append(getCompPlantId());
    sbQuery.append("') CONCNT, ");
    sbQuery.append(" GET_SET_COUNT(T207.C207_SET_ID,'RETURN', ");
    sbQuery.append("TO_DATE('" + strFrmDate + "','" + getCompDateFmt() + "'),");
    sbQuery.append("TO_DATE('" + strToDate + "','" + getCompDateFmt() + "'), '");
    sbQuery.append(getCompPlantId());
    sbQuery.append("') RETCNT ");
    sbQuery
        .append(" FROM T207_SET_MASTER T207, T2080_SET_COMPANY_MAPPING T2080 WHERE T207.C901_SET_GRP_TYPE ='1601'");
    sbQuery.append(" AND T207.C207_SET_ID = T2080.C207_SET_ID ");
    sbQuery.append(" AND T2080.C2080_VOID_FL IS NULL ");
    // Fetch records based company/plant for EDC countries
    sbQuery.append("AND (T2080.C1900_company_id  = '" + getCompId()
        + "' OR T2080.c1900_company_id  IN(");
    sbQuery
        .append("SELECT c1900_company_id FROM t5041_plant_company_mapping WHERE C5041_PRIMARY_FL = 'Y' AND c5041_void_fl IS NULL AND c5040_plant_id ='"
            + strCompPlantFilter + "')");
    sbQuery.append(") ");
    sbQuery.append(" ORDER BY T207.C207_SET_NM ");
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    hmReturn.put("SETREPORT", alReturn);

    return hmReturn;
  } // End of loadSetConsignReturnReport



  /**
   * loadItemQuarantineReport - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public RowSetDynaClass loadItemQuarantineReport(HashMap hmParam) throws AppError {
    RowSetDynaClass rsReturn = null;
    String strDateFormat = getCompDateFmt();
    String strType = GmCommonClass.parseZero((String) hmParam.get("TYPE"));
    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
    String strTransactionId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strRefId = GmCommonClass.parseNull((String) hmParam.get("REFID"));
    String strCreatedBy = GmCommonClass.parseZero((String) hmParam.get("CREATEDBY"));
    String strStatus = GmCommonClass.parseZero((String) hmParam.get("STATUS"));
    String strControlNum = GmCommonClass.parseNull((String) hmParam.get("CONTROLNUM"));
    String strEtchId = GmCommonClass.parseNull((String) hmParam.get("ETCHID"));
    String strTransDetails = GmCommonClass.parseNull((String) hmParam.get("TRANSDETAILS"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT T504.C504_CONSIGNMENT_ID CONID, T504.C504_VERIFIED_DATE SDATE, 'C' CON_TYPE, ");
    sbQuery
        .append(" T504.C504_SHIP_TO_ID SHIPTO, T504.C504_INHOUSE_PURPOSE PID, GET_CODE_NAME(T504.C504_INHOUSE_PURPOSE) REASON, ");
    sbQuery
        .append(" T504.C504_TYPE TID, GET_CODE_NAME(T504.C504_TYPE) TYPE, GET_USER_NAME(T504.C504_SHIP_TO_ID) SNAME, ");
    sbQuery
        .append(" GET_CONSIGN_SHIP_ADD(T504.C504_CONSIGNMENT_ID) EMP, T504.C504_COMMENTS COMMENTS ");
    sbQuery
        .append(" ,get_user_name(T504.C504_CREATED_BY) CUSER, get_user_name(T504.C504_VERIFIED_BY) VUSER, ");
    sbQuery.append(" (CASE WHEN  t504.C504_STATUS_FL = 0 THEN 'Initiated' ");
    sbQuery.append(" WHEN t504.C504_STATUS_FL = 1 THEN 'Pending Control #' ");
    sbQuery
        .append(" WHEN ( t504.C504_STATUS_FL = 2 AND c504_verify_fl IS NULL )  THEN 'Pending Verification' ");
    sbQuery.append(" WHEN ( t504.C504_STATUS_FL = 2 AND c504_verify_fl = 1) THEN 'Verified' ");
    sbQuery.append(" WHEN t504.C504_STATUS_FL = 3 THEN 'Pending Shipping' ");
    sbQuery.append(" WHEN t504.C504_STATUS_FL = 4 THEN 'Closed' END) SFL ");
    sbQuery.append(" ,t504.C504_REPROCESS_ID REPROCESSID ");
    if (strTransDetails.equals("Y")) {
      sbQuery
          .append(", C205_PART_NUMBER_ID PNUM, GET_PARTNUM_DESC(C205_PART_NUMBER_ID) PDESC, TO_NUMBER(C505_ITEM_QTY) IQTY");
    }
    sbQuery.append(" FROM T504_CONSIGNMENT T504 ");
    if (strTransDetails.equals("Y")) {
      sbQuery.append(", T505_ITEM_CONSIGNMENT T505 ");
    }
    sbQuery.append(" WHERE T504.C504_VOID_FL IS NULL  ");
    // Added plant id for load records depending on PlantId.
    sbQuery.append(" AND  T504.C5040_PLANT_ID = " + getCompPlantId());

    if (strTransDetails.equals("Y")) {
      sbQuery.append(" AND T504.C504_CONSIGNMENT_ID = T505.C504_CONSIGNMENT_ID ");
      sbQuery.append(" AND T505.C505_VOID_FL is null");
    }
    if (strType.equals("4112") && !strFromDt.equals("") && !strFromDt.equals("")) {
      sbQuery.append(" AND T504.C504_SHIP_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("','" + strDateFormat + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDateFormat + "')");
    } else if (strStatus.equals("4") && !strFromDt.equals("") && !strFromDt.equals("")) {
      sbQuery.append(" AND T504.C504_VERIFIED_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("','" + strDateFormat + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDateFormat + "')");
    } else if (strStatus.equals("0") && !strFromDt.equals("") && !strFromDt.equals("")) {
      sbQuery.append(" AND ( T504.C504_VERIFIED_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("','" + strDateFormat + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDateFormat + "')");
      sbQuery.append(" OR ");
      sbQuery.append(" T504.C504_CREATED_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("','" + strDateFormat + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDateFormat + "')) ");
    }

    if (!strType.equals("0")) {
      sbQuery.append(" AND T504.C504_TYPE = '");
      sbQuery.append(strType);
      sbQuery.append("'");
    }

    if (!strPartNum.equals("")) {
      if (strTransDetails.equals("Y")) {
        sbQuery.append(" AND T505.C205_PART_NUMBER_ID = '");
        sbQuery.append(strPartNum);
        sbQuery.append("'");
      } else {
        sbQuery
            .append(" AND t504.c504_consignment_id IN (SELECT t505.c504_consignment_id FROM T505_ITEM_CONSIGNMENT T505 WHERE t505.c205_part_number_id =  '");
        sbQuery.append(strPartNum);
        sbQuery.append("')");
      }
    }

    if (!strTransactionId.equals("")) {
      sbQuery.append(" AND t504.c504_consignment_id =  '");
      sbQuery.append(strTransactionId);
      sbQuery.append("'");
    }

    if (!strRefId.equals("")) {
      sbQuery.append(" AND t504.C504_REPROCESS_ID =  '");
      sbQuery.append(strRefId);
      sbQuery.append("'");
    }

    if (!strCreatedBy.equals("0")) {
      sbQuery.append(" AND t504.C504_CREATED_BY =  '");
      sbQuery.append(strCreatedBy);
      sbQuery.append("'");
    }

    if (!strStatus.equals("0")) {
      sbQuery.append(" AND t504.C504_STATUS_FL =  '");
      sbQuery.append(strStatus);
      sbQuery.append("'");
    }

    if (!strControlNum.equals("")) {
      if (strTransDetails.equals("Y")) {
        sbQuery.append(" AND T505. C505_CONTROL_NUMBER = '");
        sbQuery.append(strControlNum);
        sbQuery.append("'");
      } else {
        sbQuery
            .append(" AND t504.c504_consignment_id IN (SELECT t505.c504_consignment_id FROM T505_ITEM_CONSIGNMENT T505 WHERE t505.C505_CONTROL_NUMBER =  '");
        sbQuery.append(strControlNum);
        sbQuery.append("')");
      }
    }

    if (!strEtchId.equals("")) {
      sbQuery
          .append(" AND t504.c504_consignment_id IN (SELECT t504A.c504_consignment_id FROM T504A_CONSIGNMENT_LOANER T504A WHERE t504A.C504A_ETCH_ID ='");
      sbQuery.append(strEtchId);
      sbQuery.append("')");
    }

    sbQuery.append(" ORDER BY T504.C504_INHOUSE_PURPOSE,T504.C504_CONSIGNMENT_ID DESC ");

    log.debug(" Query for ItemQuarantine Report " + sbQuery.toString());

    rsReturn = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());

    return rsReturn;
  } // End of loadItemQuarantineReport


  /**
   * modifyDHRforRec - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap modifyDHRforRec(String strDHRId, HashMap hmParam, String strUsername)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strMsg = "";
    String strPrepareString = null;

    String strQtyRec = "";
    String strControl = "";
    String strPackSlip = "";
    String strComments = "";
    String strManfDate = "";
    String strStatusFl = "";
    String strWOID = "";
    String strExpiryDate = "";
    String strUDI = "";

    HashMap hmReturn = new HashMap();
    strQtyRec = (String) hmParam.get("QTYREC");
    strControl = (String) hmParam.get("CONTROL");
    strPackSlip = (String) hmParam.get("PKSLIP");
    strComments = (String) hmParam.get("COMMENTS");
    strManfDate = (String) hmParam.get("MANFDT");
    strStatusFl = (String) hmParam.get("STATUSFL");
    strWOID = (String) hmParam.get("WOID");
    strExpiryDate = GmCommonClass.parseNull((String) hmParam.get("EXPDT"));
    strUDI = (String) hmParam.get("UDINO");
    
    gmDBManager.setPrepareString("GM_MODIFY_DHR_RECEIVE", 11);

    gmDBManager.setString(1, strDHRId);
    gmDBManager.setInt(2, Integer.parseInt(strQtyRec));
    gmDBManager.setString(3, strControl);
    gmDBManager.setString(4, strPackSlip);
    gmDBManager.setString(5, strComments);
    gmDBManager.setString(6, strManfDate);
    gmDBManager.setString(7, strUsername);
    gmDBManager.setString(8, strWOID);
    gmDBManager.setString(9, strStatusFl);
    gmDBManager.setString(10, strExpiryDate);
    gmDBManager.setString(11, strUDI);

    gmDBManager.execute();
    gmDBManager.commit();
    return hmReturn;
  } // end of modifyDHRforRec

  /**
   * updateDHRforRollback
   * 
   * @param
   * @return
   * @exception AppError
   **/

  public HashMap updateDHRforRollback(String strDHRId, String strAction, String strUserName)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strMsg = "";
    String strPrepareString = null;
    HashMap hmEmailDetails = new HashMap();
    HashMap hmReturn = new HashMap();


    gmDBManager.setPrepareString("GM_UPDATE_DHR_ROLLBACK", 5);
    /*
     * register out parameter and set input parameters
     */

    gmDBManager.registerOutParameter(4, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);

    gmDBManager.setString(1, strDHRId);
    gmDBManager.setString(2, strAction);
    gmDBManager.setString(3, strUserName);

    gmDBManager.execute();

    /*
     * Get the value from out parameters in PRC and store in hmReturn HashMap obj.
     */
    String strInvVoidFl = gmDBManager.getString(4);
    hmEmailDetails = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(5));

    hmReturn.put("VOIDFL", strInvVoidFl);
    hmReturn.put("EMAILDETAILS", hmEmailDetails);

    gmDBManager.commit();

    return hmReturn;
  } // end of updateDHRforRollback


  /**
   * savePartNumManAdjust - This method will
   * 
   * @param String strPartNumId
   * @exception AppError This method saves manual adjustment entry and returns Inventory values for
   *            a particular part
   **/
  public ArrayList savePartNumManAdjust(String strPartNum, String strQty, String strUserId)
      throws AppError {
    GmInvReportBean gmInv = new GmInvReportBean();

    GmDBManager gmDBManager = new GmDBManager();

    ArrayList alReturn = new ArrayList();



    gmDBManager.setPrepareString("GM_UPDATE_PARTNUM_MAN_ADJUST", 3);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.setString(1, strPartNum);
    gmDBManager.setInt(2, Integer.parseInt(strQty));
    gmDBManager.setString(3, strUserId);

    gmDBManager.execute();

    alReturn = gmInv.getPartNumInvStatus(strPartNum);
    gmDBManager.commit();

    return alReturn;
  } // End of savePartNumManAdjust


  /**
   * getPartNCMRs - This method will return all the NCMRs for a particular parts.
   * 
   * @param String PartNum
   * 
   * @return ArrayList
   * @exception AppError
   **/

  public ArrayList fetchPartNCMRs(String strPartNum) throws AppError {

    ArrayList alResult = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_ncmr.gm_fch_ncmr_email_det", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));

    gmDBManager.close();

    return alResult;

  }// End of getPartNCMRs

  public String fetchRAId(String strConsignId) throws AppError {

    GmDBManager gmDBManager = new GmDBManager();
    String strRaId = "";
    gmDBManager.setFunctionString("gm_pkg_op_ln_cn_swap.get_ra_id", 1);

    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strConsignId);
    gmDBManager.execute();
    strRaId = gmDBManager.getString(1);
    log.debug("strRaId  is   " + strRaId);
    gmDBManager.close();
    return strRaId;
  }

  /**
   * get SetMappingDetails - This method will return all the set mapping details
   * 
   * @param HashMap
   * 
   * @return ArrayList
   * @exception AppError
   **/

  public ArrayList fetchSetMappingDetails(HashMap hmParam) throws AppError {

    String strLoanerSetID = "";
    String strConsignSetID = "";
    ArrayList alResult = null;
    strLoanerSetID = GmCommonClass.parseNull((String) hmParam.get("LOANERSETID"));
    strConsignSetID = GmCommonClass.parseNull((String) hmParam.get("CONSIGNSETID"));
    log.debug("strLoanerSetID:::::::" + strLoanerSetID + ":::::strConsignSetID:::::"
        + strConsignSetID);
    RowSetDynaClass rsDynaResult = null;
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_op_loaner.gm_fch_SetMap", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLoanerSetID);
    gmDBManager.setString(2, strConsignSetID);
    gmDBManager.execute();

    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    log.debug("fetchSetMappingDetails after getting results rows:::" + alResult.size());
    gmDBManager.close();
    return alResult;
  }


  /**
   * get editMappingDetails - This method will return particular rows
   * 
   * @param String
   * 
   * @return HashMap
   * @exception AppError
   **/

  public HashMap fetchEditMappingDetails(String strLoanerSetID) throws AppError {
    HashMap hResult = null;
    log.debug("strLoanerSetID:::::::" + strLoanerSetID);
    RowSetDynaClass rsDynaResult = null;
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_op_loaner.gm_fch_editSetMap", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLoanerSetID);
    gmDBManager.execute();
    hResult = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    log.debug("fetchEditMappingDetails after getting results rows:::" + hResult.size());
    gmDBManager.close();
    return hResult;
  }

  /**
   * Save SetMappingDetails - This method will save the records
   * 
   * @param HashMap
   * 
   * @return void
   * @exception AppError
   **/
  public void saveSetMappingDetails(HashMap hmParam) throws AppError {
    String strLoanerSetID = "";
    String strConsignSetID = "";
    String strUserID = "";
    ArrayList alResult = null;
    HashMap hResult = null;
    strLoanerSetID = GmCommonClass.parseNull((String) hmParam.get("LOANERSETID"));
    strConsignSetID = GmCommonClass.parseNull((String) hmParam.get("CONSIGNSETID"));
    strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    log.debug("saveSetMapping LoanerSetID:::" + strLoanerSetID + "::ConsignSetID::"
        + strConsignSetID);
    RowSetDynaClass rsDynaResult = null;
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_op_loaner.gm_sav_SetMap", 3);
    gmDBManager.setString(1, strLoanerSetID);
    gmDBManager.setString(2, strConsignSetID);
    gmDBManager.setString(3, strUserID);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  public String getXmlGridData(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    ArrayList alResult = new ArrayList();
    HashMap hmReturn = new HashMap();
    String strTemplate = (String) hmParam.get("TEMPLATE");
    String strTemplatePath = (String) hmParam.get("TEMPLATEPATH");
    alResult = (ArrayList) hmParam.get("LRESULT");
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataMap("hReturn", hmReturn);
    templateUtil.setTemplateName(strTemplate);
    templateUtil.setTemplateSubDir(strTemplatePath);
    return templateUtil.generateOutput();
  }

  /**
   * checkPartControlNumber - This method will fetch records
   * 
   * @exception AppError
   **/
  public HashMap checkPartControlNumber(String strPartNumber, String strControlNumber)
      throws AppError {
    String strMsg = "";
    HashMap hmReturn = new HashMap();
    GmDBManager db = new GmDBManager();
    db.setPrepareString("gm_pkg_op_dhr.gm_control_validation", 4);
    db.registerOutParameter(3, OracleTypes.CURSOR);
    db.registerOutParameter(4, java.sql.Types.CHAR);
    db.setString(1, strPartNumber);
    db.setString(2, strControlNumber);
    db.execute();
    hmReturn = db.returnHashMap((ResultSet) db.getObject(3));
    strMsg = GmCommonClass.parseNull(db.getString(4));
    hmReturn.put("MSG", strMsg);
    db.close();
    return hmReturn;
  }

  /*
   * Description: This method is used to save the request attribute values Author: Rajkumar J
   */
  public void saveRequestAttribute(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
    String strReqId = GmCommonClass.parseNull((String) hmParam.get("REQID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strInpStr = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTR"));
    gmDBManager.setPrepareString("gm_pkg_op_request_master.gm_sav_request_attribute", 3);
    gmDBManager.setString(1, strReqId);
    gmDBManager.setString(2, strInpStr);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
  }

  /**
   * validateFGBinId - This method is used to check the availability of FG Bin Id
   * 
   * @param String strFgBinId
   * @return String
   * @exception AppError
   */
  public String validateFGBinId(String strFgBinId) throws AppError {
    String strErrMsg = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_inv_scan.gm_validate_fg_bin_tote", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strFgBinId);
    gmDBManager.execute();
    strErrMsg = GmCommonClass.parseNull(gmDBManager.getString(2));// Getting the validation message
    gmDBManager.close();
    return strErrMsg;
  }

  /**
   * validateFGBinId - This method is used to check the availability of FG Bin Id
   * 
   * @param String strFgBinId
   * @return String
   * @exception AppError
   */
  public String validateControlNoPart(String strPartNo, String strControlNo) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_op_dhr.gm_chk_control_number_part", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strControlNo);
    gmDBManager.setString(2, strPartNo);
    gmDBManager.execute();
    strPartNo = GmCommonClass.parseNull(gmDBManager.getString(2));// Getting the validation message
    gmDBManager.close();
    return strPartNo;
  }

  /*
   * saveItemLotNum - Method to call the procedure to validate the lot numbers save the lot numbers
   * 
   * @param HashMap
   * 
   * @return String
   * 
   * @exception AppError
   */
  public String saveItemLotNum(String strConsignId, HashMap hmParams, String strInputString,
      String strShipFl) throws AppError {

    String strTxnId = GmCommonClass.parseNull((String) hmParams.get("TXNID"));
    String strTxnType = GmCommonClass.parseNull((String) hmParams.get("TYPE"));
    String strRelVerFl = GmCommonClass.parseNull((String) hmParams.get("SHIPFL"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strComments = GmCommonClass.parseNull((String) hmParams.get("CRCOMMENTS"));
    String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strErrFl = "";

    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    hmParams.put("CONNECTION", gmDBManager);// Getting the connection to a hashmap
    gmDBManager.setPrepareString("gm_pkg_op_inhouse_master.gm_sav_othr_inhouse_lot_num", 6);
    gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strTxnType);
    gmDBManager.setString(3, strInputString);
    gmDBManager.setString(4, strShipFl);
    gmDBManager.setString(5, strUserID);
    gmDBManager.execute();
    strErrFl = GmCommonClass.parseNull(gmDBManager.getString(6));

    if (!strComments.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strConsignId, strComments, strUserID, "1268");
    }

    gmDBManager.commit();
    return strErrFl;
  }

  public HashMap loadSavedSetMasterDetails(String strConsignId) throws AppError {
    HashMap hmParam = new HashMap();
    hmParam.put("CONSIGNID", strConsignId);
    return loadSavedSetMasterDetails(hmParam);
  }


  public HashMap loadSavedSetMasterDetails(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmTemp = new HashMap();
    log.debug("hmParam>>> " + hmParam);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    String strSortFlag =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SORT_BY_LOT_FL"));
    StringBuffer sbQuery = new StringBuffer();
    String strConsignId = GmCommonClass.parseNull((String) hmParam.get("CONSIGNID"));
    String strRemVoidFl = GmCommonClass.parseNull((String) hmParam.get("REMVOIDFL"));
    sbQuery.append("SELECT  T505.C205_PART_NUMBER_ID PNUM ");
    sbQuery
        .append(", get_partdesc_by_company(T505.C205_PART_NUMBER_ID) PDESC, T505.C901_WAREHOUSE_TYPE WHTYPE");
    sbQuery.append(",NVL(T505.C505_ITEM_QTY,0) IQTY");
    sbQuery.append(",T505.C505_CONTROL_NUMBER CNUM");
    sbQuery.append(",NVL(T505.C505_ITEM_PRICE,0) PRICE ");
    sbQuery
        .append(",GET_COUNT_PARTID_CONSIGN(T505.C504_CONSIGNMENT_ID,T505.C205_PART_NUMBER_ID) QTYUSED");
    sbQuery
        .append(" ,decode(GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T505.C205_PART_NUMBER_ID),'',decode(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T505.C205_PART_NUMBER_ID,T505.C505_CONTROL_NUMBER),'','10' || T505.C505_CONTROL_NUMBER,'17' || GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T505.C205_PART_NUMBER_ID,T505.C505_CONTROL_NUMBER) || '10' || T505.C505_CONTROL_NUMBER),decode(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T505.C205_PART_NUMBER_ID,T505.C505_CONTROL_NUMBER),'','01' || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T505.C205_PART_NUMBER_ID)  || '10' || T505.C505_CONTROL_NUMBER,'01' || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T505.C205_PART_NUMBER_ID) || '17' || GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T505.C205_PART_NUMBER_ID,T505.C505_CONTROL_NUMBER) || '10' || T505.C505_CONTROL_NUMBER)) BARCODEEXP");
    sbQuery
        .append(" ,decode(GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T505.C205_PART_NUMBER_ID),'',decode(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T505.C205_PART_NUMBER_ID,T505.C505_CONTROL_NUMBER),'','(10)' || T505.C505_CONTROL_NUMBER,'(17)' || GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T505.C205_PART_NUMBER_ID,T505.C505_CONTROL_NUMBER) || '(10)' || T505.C505_CONTROL_NUMBER),decode(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T505.C205_PART_NUMBER_ID,T505.C505_CONTROL_NUMBER),'','(01)' || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T505.C205_PART_NUMBER_ID)  || '(10)' || T505.C505_CONTROL_NUMBER,'(01)' || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T505.C205_PART_NUMBER_ID) || '(17)' || GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T505.C205_PART_NUMBER_ID,T505.C505_CONTROL_NUMBER) || '(10)' || T505.C505_CONTROL_NUMBER)) BARCODEEXPVAL");
    sbQuery
        .append(" , GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T505.C205_PART_NUMBER_ID,T505.C505_CONTROL_NUMBER) EXPDATE");
    sbQuery
    .append(" , to_char(GM_PKG_OP_UDI_LABEL.get_manf_date_from_dhr(T505.C205_PART_NUMBER_ID, T505.C505_CONTROL_NUMBER),'"
        + getCompDateFmt() + "') MANUFDATE ");
    sbQuery
        .append(",GET_QTY_IN_BULK(T505.C205_PART_NUMBER_ID) QTYBULK, GET_QTY_IN_INV(T505.C205_PART_NUMBER_ID,T505.C901_WAREHOUSE_TYPE)");
    sbQuery.append(",SETDT.CRITFL");
    sbQuery.append(",SETDT.CRITQTY");
    sbQuery.append(",SETDT.SETFL");
    sbQuery.append(",SETDT.CTRITAG");
    sbQuery.append(",SETDT.CRITTAG");
    sbQuery.append(",NVL(SETDT.QTY, 0) QTY");
    sbQuery
        .append(" , NVL (gm_pkg_op_item_control_rpt.get_initiated_location (t505.c205_part_number_id, '");
    sbQuery.append(strConsignId);
    sbQuery
        .append("', DECODE (t504.c504_status_fl, '2', 'pick', '3', 'put', 'pick')), gm_pkg_op_inv_scan.get_part_location(t505.c205_part_number_id, t504.c504_type)) SUGLOCATION ");
    // Need to get the location cd for OUS pick slip
    sbQuery.append(" , gm_pkg_op_item_control_rpt.get_location_cd (t505.c205_part_number_id, '");
    sbQuery.append(strConsignId);
    sbQuery
        .append("', DECODE (t504.c504_status_fl, '2', 'pick', '3', 'put', 'pick'), t504.c504_type,'',t505.c505_control_number) SUGLOCATIONCD ");
    // sbQuery.append(",get_distributor_inter_party_id (c701_distributor_id) inter_party_id");
    // sbQuery.append(", DECODE(T504.C504_INHOUSE_PURPOSE, 50061, 0, DECODE(t504.C504_STATUS_FL , 4,NVL (t505.c505_item_price, 0),NVL(get_ac_cogs_value (t505.c205_part_number_id, 4900),0))) NEWPRICE");
    sbQuery
        .append(", DECODE(T504.C504_INHOUSE_PURPOSE, 50061, 0, DECODE(t504.C504_STATUS_FL , 4,NVL (t505.c505_item_price, 0), NVL(get_account_part_pricing (NULL, t505.c205_part_number_id, get_distributor_inter_party_id (t504.c701_distributor_id)),0))) NEWPRICE");

    sbQuery
        .append(",get_part_attribute_value(t505.c205_part_number_id, 92340) tagfl , get_part_size(t505.c205_part_number_id, t505.c505_control_number ) partsize,get_partnum_material_type(T505.C205_PART_NUMBER_ID) PARTMTRELTYPE, t505.c505_error_dtls ERRORDTLS ");
    sbQuery.append(" FROM    T504_CONSIGNMENT T504 ");
    sbQuery.append(", T505_ITEM_CONSIGNMENT T505");
    sbQuery.append(", (SELECT T208.C205_PART_NUMBER_ID, T208.C208_CRITICAL_FL CRITFL");
    sbQuery
        .append(",DECODE(T208.C208_CRITICAL_QTY,NULL,T208.C208_SET_QTY,T208.C208_CRITICAL_QTY) CRITQTY");
    sbQuery.append(",T208.C208_INSET_FL SETFL ");
    sbQuery.append(",T208.C208_CRITICAL_TAG CTRITAG");
    sbQuery.append(",GET_CODE_NAME(T208.C901_CRITICAL_TYPE) CRITTAG");
    sbQuery.append(",T208.C208_SET_QTY QTY");

    sbQuery.append(" FROM T208_SET_DETAILS T208, T504_CONSIGNMENT T504");
    sbQuery.append(" WHERE T504.C504_CONSIGNMENT_ID = '");
    sbQuery.append(strConsignId);
    sbQuery
        .append("' AND T208.C207_SET_ID = T504.C207_SET_ID AND T208.C208_VOID_FL IS NULL) SETDT");
    sbQuery.append(" WHERE  T504.C504_CONSIGNMENT_ID = '");
    sbQuery.append(strConsignId);
    sbQuery.append("' AND T504.C504_CONSIGNMENT_ID = T505.C504_CONSIGNMENT_ID");
    // sbQuery.append(" AND  T504.C5040_PLANT_ID =" + getCompPlantId());
    if (!strRemVoidFl.equals("Y")) {
      sbQuery.append(" AND T505.C505_VOID_FL is NULL");
    }
    sbQuery.append(" AND T505.C205_PART_NUMBER_ID = SETDT.C205_PART_NUMBER_ID (+)");
    if (strSortFlag.equals("YES")) {
      sbQuery.append(" ORDER BY T505.C205_PART_NUMBER_ID, T505.C505_CONTROL_NUMBER");
    } else {
      sbQuery.append(" ORDER BY T505.C205_PART_NUMBER_ID ");
    }
    log.debug("Set Load Query ****** " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    hmReturn.put("SETLOAD", alReturn);

    if (alReturn.size() == 0) {

      throw new AppError(new SQLException("Error Occured", null, 20696));

    }
    /*
     * sbQuery.setLength(0); sbQuery.append(" SELECT	C207_SET_ID SETID ");
     * sbQuery.append(" FROM T504_CONSIGNMENT "); sbQuery.append(" WHERE C504_CONSIGNMENT_ID = '");
     * sbQuery.append(strConsignId); sbQuery.append("'");
     * sbQuery.append(" AND C504_VOID_FL IS NULL "); log.debug(" SETID query " +
     * sbQuery.toString()); hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
     */


    String strSetId = "";// (String)hmResult.get("SETID"); todo Commented on May 08th.. Need to
                         // verify this later

    // Adding the SetId to the return HashMap. This is done for Rollback Set CSG.
    // The Rollback button would be displayed in GmConsignShip.jsp only if the SetId is not null
    // hmReturn.put("SETID",strSetId);

    if (!strSetId.equals("")) {
      sbQuery.setLength(0);
      sbQuery
          .append(" SELECT	C.C205_PART_NUMBER_ID , GET_PARTNUM_DESC(C.C205_PART_NUMBER_ID), C.C208_SET_QTY, 'Add to Set' ");
      sbQuery.append(" FROM  T504_CONSIGNMENT A, T208_SET_DETAILS C  ");
      sbQuery.append(" WHERE A.C207_SET_ID = '");
      sbQuery.append(strSetId);
      sbQuery.append("' AND A.C207_SET_ID = C.C207_SET_ID AND A.C504_CONSIGNMENT_ID = '");
      sbQuery.append(strConsignId);
      sbQuery.append("' AND C.C208_SET_QTY <> 0 AND C208_INSET_FL = 'Y' ");
      sbQuery.append(" AND A.C504_VOID_FL IS NULL ");
      sbQuery.append(" AND C.C205_PART_NUMBER_ID NOT IN (SELECT  C205_PART_NUMBER_ID FROM ");
      sbQuery.append(" T505_ITEM_CONSIGNMENT WHERE C504_CONSIGNMENT_ID = '");
      sbQuery.append(strConsignId);
      sbQuery.append("')");
      sbQuery.append("UNION");
      sbQuery
          .append(" SELECT C205_PART_NUMBER_ID, GET_PARTNUM_DESC(C205_PART_NUMBER_ID), C505_ITEM_QTY , 'Remove from Set' ");
      sbQuery.append(" FROM T505_ITEM_CONSIGNMENT WHERE C504_CONSIGNMENT_ID ='");
      sbQuery.append(strConsignId);
      sbQuery.append("' AND C205_PART_NUMBER_ID IN ( SELECT C.C205_PART_NUMBER_ID");
      sbQuery.append(" FROM  T504_CONSIGNMENT A, T208_SET_DETAILS C ");
      sbQuery.append(" WHERE A.C207_SET_ID = '");
      sbQuery.append(strSetId);
      sbQuery.append("' AND A.C207_SET_ID = C.C207_SET_ID AND A.C504_CONSIGNMENT_ID = '");
      sbQuery.append(strConsignId);
      sbQuery.append("' AND C.C208_SET_QTY = 0 AND A.C504_VOID_FL IS NULL )");



      // alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString()); todo Commented on May
      // 08th.. to verify this sql
      hmReturn.put("MISSINGPARTS", new ArrayList());

      if (!strSetId.equals("")) {
        sbQuery.setLength(0);
        sbQuery.append(" SELECT C504_CONSIGNMENT_ID CONID ");
        sbQuery.append(" FROM T504_CONSIGNMENT ");
        sbQuery.append(" WHERE C207_SET_ID = '");
        sbQuery.append(strSetId);
        sbQuery.append("' AND C504_STATUS_FL < 2 ");
        sbQuery.append(" AND C504_VOID_FL IS NULL ");


        // alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString()); Commented on May 08th..
        // to verify this sql
        hmReturn.put("SIMILARSETS", new ArrayList());
      }
    }


    return hmReturn;
  }

  /**
   * loadConsignAttribute - This method is to fetch consignment attribute value
   * 
   * @param String
   * @return hmReturn
   * @exception AppError
   */
  public HashMap loadConsignAttribute(String strConsignId) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();
    HashMap hmTemp = new HashMap();
    HashMap hmReturn = new HashMap();
    sbQuery
        .append(" select get_code_name_alt(c901_attribute_type) NAME, C504D_ATTRIBUTE_VALUE VALUE from T504D_CONSIGNMENT_ATTRIBUTE ");
    sbQuery.append(" where C504_CONSIGNMENT_ID = '");
    sbQuery.append(strConsignId);
    sbQuery.append("' AND C504D_VOID_FL IS NULL ");
    log.debug("sbQuery:" + sbQuery);

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    if (alReturn != null) {
      for (Iterator it = alReturn.iterator(); it.hasNext();) {
        hmTemp = (HashMap) it.next();
        hmReturn.put(hmTemp.get("NAME"), hmTemp.get("VALUE"));
      }
    }
    return hmReturn;
  }
}// end of class GmOperationsBean
