package com.globus.operations.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author pvigneshwaran
 *
 */
/**
 * @author pvigneshwaran
 *
 */
public class GmVendorReportBean extends GmBean {

  GmCommonClass gmCommon = new GmCommonClass();

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmVendorReportBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * reportYTDByDist - This Method is used to fetch the sales YTD by Distributor List Will have
   * additional Filter, Yet to Decide on the same
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strToMonth
   * @param String strToYear
   * 
   * 
   * @return HashMap
   * @exception AppError
   **/
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  public HashMap reportYTDByVendor(String strFromMonth, String strFromYear, String strToMonth,
      String strToYear, String strCondition) throws AppError {
    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbWhereCondtion = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();
    HashMap hmapFinalValue; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Final value report details
    GmCrossTabReport gmCrossTabRep = new GmCrossTabReport(getGmDataStoreVO());

    sbWhereCondtion.append("");
    // Code to frame the Where Condition
    if (strFromMonth != null && strFromYear != null) {
      sbWhereCondtion.append(" T401.C401_PURCHASE_ORD_DATE >= TO_DATE('");
      sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
      sbWhereCondtion.append("','MM/YYYY') ");
    }

    if (strToMonth != null && strToYear != null) {
      if (sbWhereCondtion != null) {
        sbWhereCondtion.append(" AND");
      }
      sbWhereCondtion.append(" T401.C401_PURCHASE_ORD_DATE <= LAST_DAY(TO_DATE('");
      sbWhereCondtion.append(strToMonth + "/" + strToYear);
      sbWhereCondtion.append("','MM/YYYY')) ");
    }

    // If the From and To Dates values are not passed then system will perform the below operation
    if (sbWhereCondtion.toString().equals("")) {
      sbWhereCondtion.append(" T401.C401_PURCHASE_ORD_DATE >= ");
      sbWhereCondtion.append("ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -12)  ");
      sbWhereCondtion.append("AND T401.C401_PURCHASE_ORD_DATE <= CURRENT_DATE ");

      // To Store the from and the to date
      hmapFromToDate = gmCrossTabRep.getFromToDate();
    }
    // Appending company filter
    sbWhereCondtion.append(" AND  t401.C1900_COMPANY_ID = " + getCompId());

    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(T401.C401_PURCHASE_ORD_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append("FROM T401_PURCHASE_ORDER T401 WHERE ");
    sbHeaderQuery.append(sbWhereCondtion.toString());
    sbHeaderQuery
        .append("ORDER BY TO_DATE(TO_CHAR(T401.C401_PURCHASE_ORD_DATE,'Mon YY'),'MON YY')");

    // ***********************************************************
    // Query to Fetch the Distributor month report
    // ***********************************************************
    sbDetailQuery.append("SELECT   T301.C301_VENDOR_ID DISTRIBUTOR_ID ");
    sbDetailQuery.append(" ,T301.C301_VENDOR_NAME NAME ");
    sbDetailQuery.append(" ,TO_CHAR(C401_PURCHASE_ORD_DATE,'Mon YY') R_DATE ");
    sbDetailQuery.append(" ,SUM(C401_PO_TOTAL_AMOUNT) AMOUNT ");
    sbDetailQuery.append(" FROM     T401_PURCHASE_ORDER T401 ");
    sbDetailQuery.append(" ,T301_VENDOR T301 ");
    sbDetailQuery.append(" WHERE T301.C301_VENDOR_ID = T401 .C301_VENDOR_ID ");
    sbDetailQuery.append(" AND T401.C401_VOID_FL IS NULL	");
    sbDetailQuery.append(" AND	");
    sbDetailQuery.append(sbWhereCondtion.toString());

    // Appending company filter
    sbDetailQuery.append(" AND  t401.C1900_COMPANY_ID = " + getCompId());

    sbDetailQuery.append(" GROUP BY T301.C301_VENDOR_ID ");
    sbDetailQuery.append(" ,T301.C301_VENDOR_NAME ");
    sbDetailQuery.append(" , TO_CHAR(C401_PURCHASE_ORD_DATE,'Mon YY')");
    sbDetailQuery.append(" ORDER BY T301.C301_VENDOR_NAME,");
    sbDetailQuery.append(" TO_DATE(TO_CHAR(C401_PURCHASE_ORD_DATE,'Mon YY'),'MON YY')");

    log.debug(" Header query for vendor report " + sbHeaderQuery.toString());
    log.debug(" sbDetailQuery query for vendor report " + sbDetailQuery.toString());

    hmapFinalValue =
        gmCrossTabRep.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
    hmapFinalValue.put("FromDate", hmapFromToDate);
    return hmapFinalValue;

  }

  /**
   * reportYTDByDist - This Method is used to fetch the sales YTD by Distributor List Will have
   * additional Filter, Yet to Decide on the same
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strToMonth
   * @param String strToYear
   * 
   * @return HashMap
   * @exception AppError
   **/
  public HashMap reportYTDByPart(String strProjectID, String strType, String strFromMonth,
      String strFromYear, String strToMonth, String strToYear, String strCondition) throws AppError {
    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbWhereCondtion = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();
    HashMap hmapFinalValue = new HashMap();; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Final value report details
    GmCrossTabReport gmCrossTabRep = new GmCrossTabReport(getGmDataStoreVO());
    strProjectID = GmCommonClass.parseZero(strProjectID);
    sbWhereCondtion.append("");
    // Code to frame the Where Condition
    if (strFromMonth != null && strFromYear != null) {
      sbWhereCondtion.append(" T401.C401_PURCHASE_ORD_DATE >= TO_DATE('");
      sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
      sbWhereCondtion.append("','MM/YYYY') ");
    }

    if (strToMonth != null && strToYear != null) {
      if (sbWhereCondtion != null) {
        sbWhereCondtion.append(" AND");
      }
      sbWhereCondtion.append(" T401.C401_PURCHASE_ORD_DATE <= LAST_DAY(TO_DATE('");
      sbWhereCondtion.append(strToMonth + "/" + strToYear);
      sbWhereCondtion.append("','MM/YYYY')) ");
    }

    // If the From and To Dates values are not passed then system
    // will perform the below operation
    if (sbWhereCondtion.toString().equals("")) {
      sbWhereCondtion.append(" T401.C401_PURCHASE_ORD_DATE >= ");
      sbWhereCondtion.append("ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -12)  ");
      sbWhereCondtion.append("AND T401.C401_PURCHASE_ORD_DATE <= CURRENT_DATE ");

      // To Store the from and the to date
      hmapFromToDate = gmCrossTabRep.getFromToDate();
    }

    // Appending company filter
    sbWhereCondtion.append(" AND  t401.C1900_COMPANY_ID =" + getCompId());


    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(T401.C401_PURCHASE_ORD_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append("FROM T401_PURCHASE_ORDER T401 WHERE ");
    sbHeaderQuery.append(sbWhereCondtion.toString());
    sbHeaderQuery
        .append("ORDER BY TO_DATE(TO_CHAR(T401.C401_PURCHASE_ORD_DATE,'Mon YY'),'MON YY')");

    // ***********************************************************
    // Query to Fetch the Distributor month report
    // ***********************************************************

    if (strProjectID.equals("0")) {
      sbDetailQuery.append(" SELECT T205.C202_PROJECT_ID DISTRIBUTOR_ID  , ");
      sbDetailQuery
          .append(" T205.C202_PROJECT_ID ||'-'|| get_project_name(T205.C202_PROJECT_ID) NAME ");
    } else {
      sbDetailQuery.append("SELECT   T402.C205_PART_NUMBER_ID DISTRIBUTOR_ID ");
      sbDetailQuery.append(" ,T402.C205_PART_NUMBER_ID  || ' - ' || T205.C205_PART_NUM_DESC NAME ");
    }
    sbDetailQuery.append(" ,TO_CHAR(C401_PURCHASE_ORD_DATE,'Mon YY') R_DATE ");
    if (strType.equals("0")) {
      sbDetailQuery.append(" ,SUM(C402_QTY_ORDERED) AMOUNT ");
    } else {
      sbDetailQuery.append(" ,SUM(C402_QTY_ORDERED * C402_COST_PRICE) AMOUNT ");
    }
    sbDetailQuery.append(" FROM     T401_PURCHASE_ORDER T401 ");
    sbDetailQuery.append(" ,T402_WORK_ORDER T402 ");
    sbDetailQuery.append(" ,T205_PART_NUMBER T205 ");
    sbDetailQuery.append(" WHERE T401.C401_PURCHASE_ORD_ID = T402.C401_PURCHASE_ORD_ID ");
    sbDetailQuery.append(" AND	   T402.C205_PART_NUMBER_ID  = T205.C205_PART_NUMBER_ID");
    sbDetailQuery.append(" AND T401.C401_VOID_FL IS NULL AND T402.C402_VOID_FL IS NULL	");
    if (!strProjectID.equals("0")) {
      sbDetailQuery.append(" AND T205.C202_PROJECT_ID = '");
      sbDetailQuery.append(strProjectID);
      sbDetailQuery.append("'	");
    }
    sbDetailQuery.append(" AND	");
    sbDetailQuery.append(sbWhereCondtion.toString());

    // Appending company filter
    sbDetailQuery.append(" AND  t401.C1900_COMPANY_ID =" + getCompId());

    if (strProjectID.equals("0")) {
      sbDetailQuery
          .append(" GROUP BY T205.C202_PROJECT_ID, TO_CHAR(C401_PURCHASE_ORD_DATE,'Mon YY') ");
      sbDetailQuery.append(" ORDER BY T205.C202_PROJECT_ID,");
    } else {
      sbDetailQuery.append(" GROUP BY T402.C205_PART_NUMBER_ID ");
      sbDetailQuery.append(" ,T205.C205_PART_NUM_DESC ");
      sbDetailQuery.append(" ,TO_CHAR(C401_PURCHASE_ORD_DATE,'Mon YY')");
      sbDetailQuery.append(" ORDER BY T205.C205_PART_NUM_DESC,");
    }
    sbDetailQuery.append(" TO_DATE(TO_CHAR(C401_PURCHASE_ORD_DATE,'Mon YY'),'MON YY')");

    if (strProjectID != null) {
      hmapFinalValue =
          gmCrossTabRep.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
    }
    hmapFinalValue.put("FromDate", hmapFromToDate);
    return hmapFinalValue;

  }


  /**
   * reportVendor - This method will
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList reportVendor(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    RowSetDynaClass rdResult = null;
    ArrayList alReturn = new ArrayList();
    String strTypeIds = (String) hmParam.get("TYPE");
    String strCatIds = (String) hmParam.get("CATEGORY");
    String strStatIds = (String) hmParam.get("STATUS");
    String strDtFmt = GmCommonClass.parseNull((String)getGmDataStoreVO().getCmpdfmt());
    boolean blFlag = false;

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT	t101.C101_PARTY_ID ID,	t101.C101_PARTY_NM NAME, C301_LOT_CODE LCODE, ");
    sbQuery.append(" C301_CONTACT_PERSON PER, C301_EMAIL_ID EMAIL,C301_ACTIVE_FL FLAG, ");
    sbQuery.append(" C301_BILL_ADD1 ADD1, C301_BILL_ADD2 ADD2, ");
    sbQuery.append(" C301_BILL_CITY CITY, GET_CODE_NAME(C301_BILL_STATE) STATE, ");
    sbQuery.append(" GET_CODE_NAME(C301_BILL_COUNTRY) COUNTRY, C301_BILL_ZIP ZIP, ");
    sbQuery
        .append(" GET_CODE_NAME(C301_PAYMENT_TERMS) PAY, C301_PHONE PHONE, C301_FAX FAX , GET_CODE_NAME(C301_CURRENCY) CURRENCY ,get_user_name(C301_PURCHASING_AGENT_ID) PUR_AGENT");//PMT-51211 Display Purchasing agent in the Vendor Report
    //PC-3301 - Add Active/Inactive Date in Vendor Report
    sbQuery.append(" ,TO_CHAR(c301_active_date,'"+strDtFmt+"') ACTIVEDT, TO_CHAR(c301_inactive_date,'"+strDtFmt+"') INACTIVEDT ");
    sbQuery.append(" ,c301_inactive_date_history_fl HISTFL, c301_vendor_id VENDORID ");
    sbQuery.append(" FROM T301_VENDOR t301, T101_PARTY t101 ");
    sbQuery.append(" WHERE t301.C101_PARTY_ID = t101.C101_PARTY_ID ");

    if (!strTypeIds.equals("") || !strCatIds.equals("")) {
      sbQuery.append(" AND  t301.C101_PARTY_ID IN (SELECT C101_PARTY_ID ");
      sbQuery.append(" FROM T1001_PARTY_CATEGORY WHERE ");

      if (!strTypeIds.equals("")) {
        sbQuery.append(" C901_TYPE IN (");
        sbQuery.append(strTypeIds);
        sbQuery.append(")");
        blFlag = true;
      }

      if (!strCatIds.equals("")) {
        if (blFlag) {
          sbQuery.append(" AND ");
        }
        sbQuery.append(" C901_CATEGORY IN (");
        sbQuery.append(strCatIds);
        sbQuery.append(")");
      }

      sbQuery.append(" ) ");
    }

    if (!strStatIds.equals("")) {
      sbQuery.append(" AND C901_STATUS_ID IN (");
      sbQuery.append(strStatIds);
      sbQuery.append(")");
    }

    sbQuery.append(" AND  t301.C1900_COMPANY_ID =" + getCompId());

    sbQuery.append(" ORDER BY NAME ");
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alReturn;
  } // End of reportVendor

  /**
   * loadVendorLists - This method will
   * 
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadVendorLists() throws AppError {
    HashMap hmReturn = new HashMap();
    GmCommonClass gmCommon = new GmCommonClass();
    ArrayList alStatus = new ArrayList();
    ArrayList alType = new ArrayList();
    ArrayList alPFly = new ArrayList();

    try {
      alType = gmCommon.getCodeList("VEND");
      alStatus = gmCommon.getCodeList("VENDS");
      alPFly = gmCommon.getCodeList("PFLY");

      hmReturn.put("TYPE", alType);
      hmReturn.put("STATUS", alStatus);
      hmReturn.put("PFLY", alPFly);

    } catch (Exception e) {
      GmLogError.log("Exception in GmVendorReportBean:loadVendorLists", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadVendorLists


  public ArrayList loadVendorInfoReport(HashMap hmParam) throws AppError {
    log.debug("hmParam : " + hmParam);

    ArrayList alResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strVendorID = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
    strVendorID = strVendorID.equals("0") ? "" : strVendorID;
    String strLocation = GmCommonClass.parseNull((String) hmParam.get("LOCATION"));
    strLocation = strLocation.equals("0") ? "" : strLocation;
    String strMonth = GmCommonClass.parseNull((String) hmParam.get("MONTH"));
    String strProjectID = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
    strProjectID = strProjectID.equals("0") ? "" : strProjectID;
    String strActiveFl = GmCommonClass.parseNull((String) hmParam.get("ACTIVEFL"));
    String strPOType = GmCommonClass.parseNull((String) hmParam.get("POTYPE"));
    strPOType = strPOType.equals("0") ? "" : strPOType;

    gmDBManager.setPrepareString("gm_op_fch_vendor_info_rpt", 7);
    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
    gmDBManager.setString(1, strVendorID);
    gmDBManager.setString(2, strLocation);
    gmDBManager.setInt(3, Integer.parseInt(strMonth));
    gmDBManager.setString(4, strProjectID);
    gmDBManager.setString(5, strActiveFl);
    gmDBManager.setString(6, strPOType);
    gmDBManager.execute();

    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(7));
    log.debug("alResult : " + alResult);
    gmDBManager.close();
    return alResult;
  }
  
  

	  
	  /**
	   * This method will get standard cost details
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public ArrayList fetchStandardCostDtls(HashMap hmParam) throws AppError {
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    ArrayList allDetails = new ArrayList();
		    String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("STRPARTNUMBERS"));
		    String strId = GmCommonClass.parseNull((String) hmParam.get("STRVENDORID"));
		    String strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
		  
		    gmDBManager.setPrepareString("gm_pkg_pur_vendor_cost.gm_fch_standard_cost", 4);
		    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		    gmDBManager.setString(1, strPartNumber);
		    gmDBManager.setString(2, strId);
		    gmDBManager.setString(3, strProjectId);
		    gmDBManager.execute();
		    allDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
		    gmDBManager.close();
		    return allDetails;
		  
	  }
	  
	  
	  
	  /**This method will fetch standardcost history details
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public ArrayList fetchStandardCostHistory(HashMap hmParam) throws AppError {

		   ArrayList allHistory= new ArrayList();
		 
		    String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
		    String strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    gmDBManager.setPrepareString("gm_pkg_pur_vendor_cost.gm_fch_standard_cost_history", 3);
		    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		    gmDBManager.setString(1, strPartNumber);
		    gmDBManager.setString(2, strVendorId);
		    gmDBManager.execute();
		    allHistory = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		    gmDBManager.close();
		    return allHistory;
		  }
	  
	  

	  /**
	   * This Method will edit standard cost based on part number
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public HashMap fetchEditCost(HashMap hmParam) throws AppError {
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    HashMap hmDetails = new HashMap();
		    String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
		    String strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
		    gmDBManager.setPrepareString("gm_pkg_pur_vendor_cost.gm_fch_std_cost_edit_dtls", 3);
		    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		    gmDBManager.setString(1, strPartNumber);
		    gmDBManager.setString(2, strVendorId);
		    gmDBManager.execute();
		    hmDetails = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
		    gmDBManager.close();
		    return hmDetails;
		  
	  }

}// end of GmVendorReportBean
