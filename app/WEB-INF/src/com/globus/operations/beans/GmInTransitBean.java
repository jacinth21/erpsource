package com.globus.operations.beans;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.util.GmTemplateUtil;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.abs.beans.GmUDIBean;
import com.globus.operations.factory.beans.GmUDIFactoryBean;
import com.globus.operations.inventory.beans.GmLocationBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmInTransitBean extends GmBean {

	GmCommonClass gmCommon = new GmCommonClass();
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
	                                                              
	public GmInTransitBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	}

	  /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */

	public GmInTransitBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}

	/**
	 * intrasitSplitValues - This method is used to save the split values of INTRA
	 * 
	 * @param HashMap hmParams
	 * @return HashMap
	 * @exception AppError
	 */
	public HashMap intrasitSplitValues(HashMap hmParams) throws AppError {
			
		String strConsId = GmCommonClass.parseNull((String) hmParams.get("CONSIGNID"));
		String strPurpose = GmCommonClass.parseNull((String) hmParams.get("PURP"));
		String strComments = GmCommonClass.parseNull((String) hmParams.get("COMMENTS"));
		String strFgvalue = GmCommonClass.parseNull((String) hmParams.get("FGINPUTSTR"));
		String strQnvalue = GmCommonClass.parseNull((String) hmParams.get("QNINPUTSTR"));
		String strRpvalue = GmCommonClass.parseNull((String) hmParams.get("RPINPUTSTR"));
		String strUserid = GmCommonClass.parseNull((String) hmParams.get("USERID"));
	
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_in_transit_trans.gm_intrasit_sav_inhouse_txn", 8);
	   
		gmDBManager.setString(1, strConsId);    
		gmDBManager.setInt(2, 4000097); 
		gmDBManager.setString(3, strComments);
		gmDBManager.setString(4, strFgvalue);
		gmDBManager.setString(5, strQnvalue);
		gmDBManager.setString(6, strRpvalue);
		gmDBManager.setString(7, strUserid);
		gmDBManager.registerOutParameter(8,java.sql.Types.VARCHAR);
		gmDBManager.execute();
		String hmSuccessMessage =gmDBManager.getString(8);
		HashMap hmSucessDetails=new HashMap();
		hmSucessDetails.put("INTRAVAL",hmSuccessMessage);
		gmDBManager.commit();
		return hmSucessDetails;
	}
	/**
		 * fetchIntraCodelookupValues - This method is get the intransit code lookup values
		 * 
		 * @param No params
		 * @return HashMap
		 * @exception AppError
	*/
	public HashMap fetchIntraCodelookupValues() throws AppError {

		ArrayList alList = new ArrayList();
	    ArrayList alListreg = new ArrayList();
	    HashMap hmCodelookup = new HashMap();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_in_transit_trans.gm_intrasit_fch_codelookup_val",2);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
	    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	    gmDBManager.execute();  
	    alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
	    alListreg = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
	    hmCodelookup.put("INTRACONSIGNMENT", alList);
	    hmCodelookup.put("INTRASET", alListreg);
	    return hmCodelookup;
	}
		
	/**
		 * intrasitBuildSet - This method used to create new CN and request for the ICT set
		 * @param hmParams
		 * @throws AppError
	*/
	public void intrasitBuildSet(HashMap hmParams) throws AppError {
		
		String strConsignId = GmCommonClass.parseNull((String) hmParams.get("CONSIGNMENTID"));
		String strSetId = GmCommonClass.parseNull((String) hmParams.get("SETID")); 
		String strUserid = GmCommonClass.parseNull((String) hmParams.get("USERID"));			  
				  
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_in_transit_trans.gm_intrasit_sav_build_set", 5);    
		gmDBManager.registerOutParameter(4, java.sql.Types.VARCHAR);
		gmDBManager.registerOutParameter(5, java.sql.Types.VARCHAR);
		gmDBManager.setString(1, strConsignId);
		gmDBManager.setString(2, strSetId);
		gmDBManager.setString(3, strUserid);   
		gmDBManager.execute();    
		String strNewRequestId = GmCommonClass.parseNull(gmDBManager.getString(4));
		String strNewConsignId = GmCommonClass.parseNull(gmDBManager.getString(5));
		gmDBManager.commit();
		gmDBManager.close();
		return;
	}
	
	public String fetchOwnerCompID(String strBillTo) throws AppError {

		String strBillComp = "";
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setFunctionString("gm_pkg_op_process_inhouse_txn.GET_DIST_COMPID",1);
		gmDBManager.registerOutParameter(1, java.sql.Types.VARCHAR);
		gmDBManager.setString(2, strBillTo);
	    gmDBManager.execute();  
	    strBillComp = GmCommonClass.parseNull(gmDBManager.getString(1));	
	    return strBillComp;
	}


}//End of class GmIntransitBean
