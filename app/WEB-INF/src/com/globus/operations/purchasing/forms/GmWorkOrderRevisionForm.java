package com.globus.operations.purchasing.forms;

import java.util.ArrayList;
import java.util.Date;

import com.globus.common.forms.GmCommonForm;

public class GmWorkOrderRevisionForm  extends GmCommonForm{
	
	private String projectId ="";
	private String projectName ="";
	private String pnum ="";
	private String poId  ="";
	private String woActionId  ="";
	private Date dtWOFromDate = null;
	private Date dtWOToDate = null;
	private String woJsonString ="";
	private String updateAccessFl ="";
	private String ignoreAccessFl ="";
	private String workOrderId ="";
	private String woCurrentRev ="";
	private ArrayList alWoAction = new ArrayList();
	private String pnumSuffix ="";
	private String status ="";
	
	/**
	 * @return the projectId
	 */
	public String getProjectId() {
		return projectId;
	}
	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}
	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	/**
	 * @return the pnum
	 */
	public String getPnum() {
		return pnum;
	}
	/**
	 * @param pnum the pnum to set
	 */
	public void setPnum(String pnum) {
		this.pnum = pnum;
	}
	/**
	 * @return the poId
	 */
	public String getPoId() {
		return poId;
	}
	/**
	 * @param poId the poId to set
	 */
	public void setPoId(String poId) {
		this.poId = poId;
	}
	/**
	 * @return the woActionId
	 */
	public String getWoActionId() {
		return woActionId;
	}
	/**
	 * @param woActionId the woActionId to set
	 */
	public void setWoActionId(String woActionId) {
		this.woActionId = woActionId;
	}
	
	
	/**
	 * @return the dtWOFromDate
	 */
	public Date getDtWOFromDate() {
		return dtWOFromDate;
	}
	/**
	 * @param dtWOFromDate the dtWOFromDate to set
	 */
	public void setDtWOFromDate(Date dtWOFromDate) {
		this.dtWOFromDate = dtWOFromDate;
	}
	/**
	 * @return the dtWOToDate
	 */
	public Date getDtWOToDate() {
		return dtWOToDate;
	}
	/**
	 * @param dtWOToDate the dtWOToDate to set
	 */
	public void setDtWOToDate(Date dtWOToDate) {
		this.dtWOToDate = dtWOToDate;
	}
	/**
	 * @return the woJsonString
	 */
	public String getWoJsonString() {
		return woJsonString;
	}
	/**
	 * @param woJsonString the woJsonString to set
	 */
	public void setWoJsonString(String woJsonString) {
		this.woJsonString = woJsonString;
	}
	/**
	 * @return the updateAccessFl
	 */
	public String getUpdateAccessFl() {
		return updateAccessFl;
	}
	/**
	 * @param updateAccessFl the updateAccessFl to set
	 */
	public void setUpdateAccessFl(String updateAccessFl) {
		this.updateAccessFl = updateAccessFl;
	}
	/**
	 * @return the ignoreAccessFl
	 */
	public String getIgnoreAccessFl() {
		return ignoreAccessFl;
	}
	/**
	 * @param ignoreAccessFl the ignoreAccessFl to set
	 */
	public void setIgnoreAccessFl(String ignoreAccessFl) {
		this.ignoreAccessFl = ignoreAccessFl;
	}
	/**
	 * @return the workOrderId
	 */
	public String getWorkOrderId() {
		return workOrderId;
	}
	/**
	 * @param workOrderId the workOrderId to set
	 */
	public void setWorkOrderId(String workOrderId) {
		this.workOrderId = workOrderId;
	}
	/**
	 * @return the woCurrentRev
	 */
	public String getWoCurrentRev() {
		return woCurrentRev;
	}
	/**
	 * @param woCurrentRev the woCurrentRev to set
	 */
	public void setWoCurrentRev(String woCurrentRev) {
		this.woCurrentRev = woCurrentRev;
	}
	/**
	 * @return the alWoAction
	 */
	public ArrayList getAlWoAction() {
		return alWoAction;
	}
	/**
	 * @param alWoAction the alWoAction to set
	 */
	public void setAlWoAction(ArrayList alWoAction) {
		this.alWoAction = alWoAction;
	}
	/**
	 * @return the pnumSuffix
	 */
	public String getPnumSuffix() {
		return pnumSuffix;
	}
	/**
	 * @param pnumSuffix the pnumSuffix to set
	 */
	public void setPnumSuffix(String pnumSuffix) {
		this.pnumSuffix = pnumSuffix;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	
}
