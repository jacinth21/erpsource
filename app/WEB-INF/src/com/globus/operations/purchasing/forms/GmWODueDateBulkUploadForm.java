package com.globus.operations.purchasing.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCommonForm;
import com.globus.common.forms.GmLogForm;

/**
 * Form used to store request values
 * 
 * @author asingarvel
 *
 */
public class GmWODueDateBulkUploadForm extends GmCommonForm{

	private String inputStr = "";
	private String woStr = "";
	/**
	 * @return the inputStr
	 */
	public String getInputStr() {
		return inputStr;
	}
	/**
	 * @param inputStr the inputStr to set
	 */
	public void setInputStr(String inputStr) {
		this.inputStr = inputStr;
	}
	/**
	 * @return the woStr
	 */
	public String getWoStr() {
		return woStr;
	}
	/**
	 * @param woStr the woStr to set
	 */
	public void setWoStr(String woStr) {
		this.woStr = woStr;
	}

}