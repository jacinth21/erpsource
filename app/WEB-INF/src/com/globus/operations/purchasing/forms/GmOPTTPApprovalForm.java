package com.globus.operations.purchasing.forms;

import java.util.ArrayList;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.forms.GmCommonForm;

public class GmOPTTPApprovalForm extends GmCommonForm{
	 private static final long serialVersionUID = 1L;
	 private String ttpId = "";
	 private String ttpName = "";
	 private String status = "";
	 private String lockPeriod="";
	 private String category="";
	 private String inputString="";
	 private String ttpMonth=GmCalenderOperations.getCurrentMM();
	 private String ttpYear=String.valueOf(GmCalenderOperations.getCurrentYear());
	 private String searchttpId="";
	 private String hCompanyID = ""; 
	 private String strInvalidName="";
	 private String gridData="";
	 private String ttpDetailId="";
	 private String strXmlGridData="";
	 private String strLockAccessFl="";//PMT-32804
	 
	 private ArrayList alStatus  = new ArrayList();
	 private ArrayList alLockPeriod  = new ArrayList();
	 private ArrayList alCategory  = new ArrayList();
	 private ArrayList alMonth  = new ArrayList();
	 private ArrayList alYear  = new ArrayList();
	 private ArrayList alTTPList  = new ArrayList();
	/**
	 * @return the ttpId
	 */
	public String getTtpId() {
		return ttpId;
	}

	/**
	 * @param ttpId the ttpId to set
	 */
	public void setTtpId(String ttpId) {
		this.ttpId = ttpId;
	}

	/**
	 * @return the ttpName
	 */
	public String getTtpName() {
		return ttpName;
	}

	/**
	 * @param ttpName the ttpName to set
	 */
	public void setTtpName(String ttpName) {
		this.ttpName = ttpName;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}

	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}




	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the alCategory
	 */
	public ArrayList getAlCategory() {
		return alCategory;
	}

	/**
	 * @param alCategory the alCategory to set
	 */
	public void setAlCategory(ArrayList alCategory) {
		this.alCategory = alCategory;
	}


	/**
	 * @return the lockPeriod
	 */
	public String getLockPeriod() {
		return lockPeriod;
	}

	/**
	 * @param lockPeriod the lockPeriod to set
	 */
	public void setLockPeriod(String lockPeriod) {
		this.lockPeriod = lockPeriod;
	}

	/**
	 * @return the alLockPeriod
	 */
	public ArrayList getAlLockPeriod() {
		return alLockPeriod;
	}

	/**
	 * @param alLockPeriod the alLockPeriod to set
	 */
	public void setAlLockPeriod(ArrayList alLockPeriod) {
		this.alLockPeriod = alLockPeriod;
	}

	/**
	 * @return the inputString
	 */
	public String getInputString() {
		return inputString;
	}

	/**
	 * @param inputString the inputString to set
	 */
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}

	public ArrayList getAlYear() {
		return alYear;
	}

	public void setAlYear(ArrayList alYear) {
		this.alYear = alYear;
	}

	public ArrayList getAlMonth() {
		return alMonth;
	}

	public void setAlMonth(ArrayList alMonth) {
		this.alMonth = alMonth;
	}

	public String getTtpMonth() {
		return ttpMonth;
	}

	public void setTtpMonth(String ttpMonth) {
		this.ttpMonth = ttpMonth;
	}

	public String getTtpYear() {
		return ttpYear;
	}

	public void setTtpYear(String ttpYear) {
		this.ttpYear = ttpYear;
	}



	public ArrayList getAlTTPList() {
		return alTTPList;
	}

	public void setAlTTPList(ArrayList alTTPList) {
		this.alTTPList = alTTPList;
	}

	public String getSearchttpId() {
		return searchttpId;
	}

	public void setSearchttpId(String searchttpId) {
		this.searchttpId = searchttpId;
	}

	public String gethCompanyID() {
		return hCompanyID;
	}

	public void sethCompanyID(String hCompanyID) {
		this.hCompanyID = hCompanyID;
	}

	public String getStrInvalidName() {
		return strInvalidName;
	}

	public void setStrInvalidName(String strInvalidName) {
		this.strInvalidName = strInvalidName;
	}

	public String getGridData() {
		return gridData;
	}

	public void setGridData(String gridData) {
		this.gridData = gridData;
	}

	public String getTtpDetailId() {
		return ttpDetailId;
	}

	public void setTtpDetailId(String ttpDetailId) {
		this.ttpDetailId = ttpDetailId;
	}

	public String getStrXmlGridData() {
		return strXmlGridData;
	}

	public void setStrXmlGridData(String strXmlGridData) {
		this.strXmlGridData = strXmlGridData;
	}

	/**
	 * @return the strLockAccessFl
	 */
	public String getStrLockAccessFl() {
		return strLockAccessFl;
	}

	/**
	 * @param strLockAccessFl the strLockAccessFl to set
	 */
	public void setStrLockAccessFl(String strLockAccessFl) {
		this.strLockAccessFl = strLockAccessFl;
	}
	 
}
