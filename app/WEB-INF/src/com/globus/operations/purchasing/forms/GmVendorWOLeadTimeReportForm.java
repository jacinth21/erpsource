package com.globus.operations.purchasing.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.formbeans.GmCommonForm;
import com.globus.common.beans.GmLogger; 

import org.apache.log4j.Logger; 


public class GmVendorWOLeadTimeReportForm extends GmCommonForm
{
    private static final long serialVersionUID = 1L;
    
    private String projectID = "";
    private String likeSearch = "";
    private String partNums = "";
    private String searchprojectID="";
    private String searchvendorId="";
    private String vendorId="";
    private String strPurAgent = "";
    private String strDivision = "";
    
    private ArrayList alPartSearch = new ArrayList();
    private ArrayList alPurchaseAgent = new ArrayList();
    private ArrayList alDivision  = new ArrayList();
	/**
	 * @return the projectID
	 */
	public String getProjectID() {
		return projectID;
	}
	/**
	 * @param projectID the projectID to set
	 */
	public void setProjectID(String projectID) {
		this.projectID = projectID;
	}
	/**
	 * @return the likeSearch
	 */
	public String getLikeSearch() {
		return likeSearch;
	}
	/**
	 * @param likeSearch the likeSearch to set
	 */
	public void setLikeSearch(String likeSearch) {
		this.likeSearch = likeSearch;
	}
	/**
	 * @return the partNums
	 */
	public String getPartNums() {
		return partNums;
	}
	/**
	 * @param partNums the partNums to set
	 */
	public void setPartNums(String partNums) {
		this.partNums = partNums;
	}
	/**
	 * @return the searchprojectID
	 */
	public String getSearchprojectID() {
		return searchprojectID;
	}
	/**
	 * @param searchprojectID the searchprojectID to set
	 */
	public void setSearchprojectID(String searchprojectID) {
		this.searchprojectID = searchprojectID;
	}
	/**
	 * @return the searchvendorId
	 */
	public String getSearchvendorId() {
		return searchvendorId;
	}
	/**
	 * @param searchvendorId the searchvendorId to set
	 */
	public void setSearchvendorId(String searchvendorId) {
		this.searchvendorId = searchvendorId;
	}
	/**
	 * @return the vendorId
	 */
	public String getVendorId() {
		return vendorId;
	}
	/**
	 * @param vendorId the vendorId to set
	 */
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	/**
	 * @return the strPurAgent
	 */
	public String getStrPurAgent() {
		return strPurAgent;
	}
	/**
	 * @param strPurAgent the strPurAgent to set
	 */
	public void setStrPurAgent(String strPurAgent) {
		this.strPurAgent = strPurAgent;
	}
	/**
	 * @return the strDivision
	 */
	public String getStrDivision() {
		return strDivision;
	}
	/**
	 * @param strDivision the strDivision to set
	 */
	public void setStrDivision(String strDivision) {
		this.strDivision = strDivision;
	}
	/**
	 * @return the alPartSearch
	 */
	public ArrayList getAlPartSearch() {
		return alPartSearch;
	}
	/**
	 * @param alPartSearch the alPartSearch to set
	 */
	public void setAlPartSearch(ArrayList alPartSearch) {
		this.alPartSearch = alPartSearch;
	}
	/**
	 * @return the alPurchaseAgent
	 */
	public ArrayList getAlPurchaseAgent() {
		return alPurchaseAgent;
	}
	/**
	 * @param alPurchaseAgent the alPurchaseAgent to set
	 */
	public void setAlPurchaseAgent(ArrayList alPurchaseAgent) {
		this.alPurchaseAgent = alPurchaseAgent;
	}
	/**
	 * @return the alDivision
	 */
	public ArrayList getAlDivision() {
		return alDivision;
	}
	/**
	 * @param alDivision the alDivision to set
	 */
	public void setAlDivision(ArrayList alDivision) {
		this.alDivision = alDivision;
	}
}