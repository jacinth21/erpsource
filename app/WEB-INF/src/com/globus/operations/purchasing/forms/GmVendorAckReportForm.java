/**
 * 
 */
package com.globus.operations.purchasing.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCommonForm;

/**
 * @author asingaravel
 *
 */
public class GmVendorAckReportForm extends GmCommonForm{
	
	private ArrayList alPartSearch = new ArrayList();
    private ArrayList alDueDays = new ArrayList();
    private ArrayList alPuchaseAgentName = new ArrayList();
    
    private String  ackDt         = ""; 
    private String  projectID        = "";
    private String  dueDays          = "";
    private String  likeSearch         = "";
    private String  partNums         = "";
    private String  showClosedWOFl    = "";
    private String  noOfDays          = "";
    private String  searchprojectID="";
    private String  searchvendorId="";
    private String  fromDate="";
    private String  toDate="";
    private String  vendorId="";
    private String  strPurAgentName = "";
    
    

	/**
	 * @return the alPuchaseAgentName
	 */
	public ArrayList getAlPuchaseAgentName() {
		return alPuchaseAgentName;
	}
	/**
	 * @param alPuchaseAgentName the alPuchaseAgentName to set
	 */
	public void setAlPuchaseAgentName(ArrayList alPuchaseAgentName) {
		this.alPuchaseAgentName = alPuchaseAgentName;
	}
	/**
	 * @return the strPurAgentName
	 */
	public String getStrPurAgentName() {
		return strPurAgentName;
	}
	/**
	 * @param strPurAgentName the strPurAgentName to set
	 */
	public void setStrPurAgentName(String strPurAgentName) {
		this.strPurAgentName = strPurAgentName;
	}
	/**
	 * @return the alPartSearch
	 */
	public ArrayList getAlPartSearch() {
		return alPartSearch;
	}
	/**
	 * @param alPartSearch the alPartSearch to set
	 */
	public void setAlPartSearch(ArrayList alPartSearch) {
		this.alPartSearch = alPartSearch;
	}
	/**
	 * @return the alDueDays
	 */
	public ArrayList getAlDueDays() {
		return alDueDays;
	}
	/**
	 * @param alDueDays the alDueDays to set
	 */
	public void setAlDueDays(ArrayList alDueDays) {
		this.alDueDays = alDueDays;
	}
	/**
	 * @return the ackDt
	 */
	public String getAckDt() {
		return ackDt;
	}
	/**
	 * @param ackDt the ackDt to set
	 */
	public void setAckDt(String ackDt) {
		this.ackDt = ackDt;
	}
	/**
	 * @return the projectID
	 */
	public String getProjectID() {
		return projectID;
	}
	/**
	 * @param projectID the projectID to set
	 */
	public void setProjectID(String projectID) {
		this.projectID = projectID;
	}
	/**
	 * @return the dueDays
	 */
	public String getDueDays() {
		return dueDays;
	}
	/**
	 * @param dueDays the dueDays to set
	 */
	public void setDueDays(String dueDays) {
		this.dueDays = dueDays;
	}
	/**
	 * @return the likeSearch
	 */
	public String getLikeSearch() {
		return likeSearch;
	}
	/**
	 * @param likeSearch the likeSearch to set
	 */
	public void setLikeSearch(String likeSearch) {
		this.likeSearch = likeSearch;
	}
	/**
	 * @return the partNums
	 */
	public String getPartNums() {
		return partNums;
	}
	/**
	 * @param partNums the partNums to set
	 */
	public void setPartNums(String partNums) {
		this.partNums = partNums;
	}
	/**
	 * @return the showClosedWOFl
	 */
	public String getShowClosedWOFl() {
		return showClosedWOFl;
	}
	/**
	 * @param showClosedWOFl the showClosedWOFl to set
	 */
	public void setShowClosedWOFl(String showClosedWOFl) {
		this.showClosedWOFl = showClosedWOFl;
	}
	/**
	 * @return the noOfDays
	 */
	public String getNoOfDays() {
		return noOfDays;
	}
	/**
	 * @param noOfDays the noOfDays to set
	 */
	public void setNoOfDays(String noOfDays) {
		this.noOfDays = noOfDays;
	}
	/**
	 * @return the searchprojectID
	 */
	public String getSearchprojectID() {
		return searchprojectID;
	}
	/**
	 * @param searchprojectID the searchprojectID to set
	 */
	public void setSearchprojectID(String searchprojectID) {
		this.searchprojectID = searchprojectID;
	}
	/**
	 * @return the searchvendorId
	 */
	public String getSearchvendorId() {
		return searchvendorId;
	}
	/**
	 * @param searchvendorId the searchvendorId to set
	 */
	public void setSearchvendorId(String searchvendorId) {
		this.searchvendorId = searchvendorId;
	}
	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}
	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	/**
	 * @return the vendorId
	 */
	public String getVendorId() {
		return vendorId;
	}
	/**
	 * @param vendorId the vendorId to set
	 */
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}    
}
