package com.globus.operations.purchasing.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.formbeans.GmCommonForm;
import com.globus.common.beans.GmLogger; 

import org.apache.log4j.Logger; 


public class GmWorkOrderVendorCommitForm extends GmCommonForm
{
    private static final long serialVersionUID = 1L;
    
    private ArrayList alPartSearch = new ArrayList();
    private ArrayList alDueDays = new ArrayList();
    private HashMap hmReturn = new HashMap();
    
    private String  commitDt         = "";
    private String  commitQty         = "";
    private String  gridData         = ""; 
    private String inputString = "";
    private String workOrdId = "";
    private String  projectID        = "";
    private String  dueDays          = "";
    private String  likeSearch         = "";
    private String  partNums         = "";
    private String  showClosedWOFl    = "";
    private String  noOfDays          = "";
    private String searchprojectID="";
    private String searchvendorId="";
    private String fromDate="";
    private String toDate="";
    private String strAccessfl="";
    private String vendorId="";
    

	/**
	 * @return the alPartSearch
	 */
	public ArrayList getAlPartSearch() {
		return alPartSearch;
	}

	/**
	 * @param alPartSearch the alPartSearch to set
	 */
	public void setAlPartSearch(ArrayList alPartSearch) {
		this.alPartSearch = alPartSearch;
	}

	/**
	 * @return the alDueDays
	 */
	public ArrayList getAlDueDays() {
		return alDueDays;
	}

	/**
	 * @param alDueDays the alDueDays to set
	 */
	public void setAlDueDays(ArrayList alDueDays) {
		this.alDueDays = alDueDays;
	}

	/**
	 * @return the hmReturn
	 */
	public HashMap getHmReturn() {
		return hmReturn;
	}

	/**
	 * @param hmReturn the hmReturn to set
	 */
	public void setHmReturn(HashMap hmReturn) {
		this.hmReturn = hmReturn;
	}

	/**
	 * @return the commitDt
	 */
	public String getCommitDt() {
		return commitDt;
	}

	/**
	 * @param commitDt the commitDt to set
	 */
	public void setCommitDt(String commitDt) {
		this.commitDt = commitDt;
	}

	/**
	 * @return the commitQty
	 */
	public String getCommitQty() {
		return commitQty;
	}

	/**
	 * @param commitQty the commitQty to set
	 */
	public void setCommitQty(String commitQty) {
		this.commitQty = commitQty;
	}

	/**
	 * @return the gridData
	 */
	public String getGridData() {
		return gridData;
	}

	/**
	 * @param gridData the gridData to set
	 */
	public void setGridData(String gridData) {
		this.gridData = gridData;
	}

	/**
	 * @return the inputString
	 */
	public String getInputString() {
		return inputString;
	}

	/**
	 * @param inputString the inputString to set
	 */
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}

	/**
	 * @return the workOrdId
	 */
	public String getWorkOrdId() {
		return workOrdId;
	}

	/**
	 * @param workOrdId the workOrdId to set
	 */
	public void setWorkOrdId(String workOrdId) {
		this.workOrdId = workOrdId;
	}

	/**
	 * @return the projectID
	 */
	public String getProjectID() {
		return projectID;
	}

	/**
	 * @param projectID the projectID to set
	 */
	public void setProjectID(String projectID) {
		this.projectID = projectID;
	}

	/**
	 * @return the dueDays
	 */
	public String getDueDays() {
		return dueDays;
	}

	/**
	 * @param dueDays the dueDays to set
	 */
	public void setDueDays(String dueDays) {
		this.dueDays = dueDays;
	}

	/**
	 * @return the likeSearch
	 */
	public String getLikeSearch() {
		return likeSearch;
	}

	/**
	 * @param likeSearch the likeSearch to set
	 */
	public void setLikeSearch(String likeSearch) {
		this.likeSearch = likeSearch;
	}

	/**
	 * @return the partNums
	 */
	public String getPartNums() {
		return partNums;
	}

	/**
	 * @param partNums the partNums to set
	 */
	public void setPartNums(String partNums) {
		this.partNums = partNums;
	}

	/**
	 * @return the showClosedWOFl
	 */
	public String getShowClosedWOFl() {
		return showClosedWOFl;
	}

	/**
	 * @param showClosedWOFl the showClosedWOFl to set
	 */
	public void setShowClosedWOFl(String showClosedWOFl) {
		this.showClosedWOFl = showClosedWOFl;
	}

	/**
	 * @return the noOfDays
	 */
	public String getNoOfDays() {
		return noOfDays;
	}

	/**
	 * @param noOfDays the noOfDays to set
	 */
	public void setNoOfDays(String noOfDays) {
		this.noOfDays = noOfDays;
	}

	/**
	 * @return the searchprojectID
	 */
	public String getSearchprojectID() {
		return searchprojectID;
	}

	/**
	 * @param searchprojectID the searchprojectID to set
	 */
	public void setSearchprojectID(String searchprojectID) {
		this.searchprojectID = searchprojectID;
	}

	/**
	 * @return the searchvendorId
	 */
	public String getSearchvendorId() {
		return searchvendorId;
	}

	/**
	 * @param searchvendorId the searchvendorId to set
	 */
	public void setSearchvendorId(String searchvendorId) {
		this.searchvendorId = searchvendorId;
	}

	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the strAccessfl
	 */
	public String getStrAccessfl() {
		return strAccessfl;
	}

	/**
	 * @param strAccessfl the strAccessfl to set
	 */
	public void setStrAccessfl(String strAccessfl) {
		this.strAccessfl = strAccessfl;
	}

	/**
	 * @return the vendorId
	 */
	public String getVendorId() {
		return vendorId;
	}

	/**
	 * @param vendorId the vendorId to set
	 */
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
    
    
    
}