package com.globus.operations.purchasing.forms;
import java.util.ArrayList;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.forms.GmCommonForm;
/**
 * @author pvigneshwaran
 *
 */
public class GmPurchasingClassificationForm extends GmCommonForm{
	 private String pnum ="";
	 private String pnumSuffix ="";
	 private String projectId ="";
	 private String projectName ="";
	 private String divisionId="";
	 private String rankId="";
	 
	 private String abcMonth=GmCalenderOperations.getCurrentMM();
	 private String abcYear=String.valueOf(GmCalenderOperations.getCurrentYear());
	
	 
	 private ArrayList alDivision  = new ArrayList();
	 private ArrayList alClassiffication  = new ArrayList();
	 private ArrayList alMonth  = new ArrayList();
	 private ArrayList alYear  = new ArrayList();
	 /**
	 * @return the pnum
	 */
	public String getPnum() {
		return pnum;
	}
	/**
	 * @param pnum the pnum to set
	 */
	public void setPnum(String pnum) {
		this.pnum = pnum;
	}
	/**
	 * @return the pnumSuffix
	 */
	public String getPnumSuffix() {
		return pnumSuffix;
	}
	/**
	 * @param pnumSuffix the pnumSuffix to set
	 */
	public void setPnumSuffix(String pnumSuffix) {
		this.pnumSuffix = pnumSuffix;
	}
	/**
	 * @return the projectId
	 */
	public String getProjectId() {
		return projectId;
	}
	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	/**
	 * @return the divisionId
	 */
	public String getDivisionId() {
		return divisionId;
	}
	/**
	 * @param divisionId the divisionId to set
	 */
	public void setDivisionId(String divisionId) {
		this.divisionId = divisionId;
	}
	/**
	 * @return the rankId
	 */
	public String getRankId() {
		return rankId;
	}
	/**
	 * @param rankId the rankId to set
	 */
	public void setRankId(String rankId) {
		this.rankId = rankId;
	}
	/**
	 * @return the abcMonth
	 */
	public String getAbcMonth() {
		return abcMonth;
	}
	/**
	 * @param abcMonth the abcMonth to set
	 */
	public void setAbcMonth(String abcMonth) {
		this.abcMonth = abcMonth;
	}
	/**
	 * @return the abcYear
	 */
	public String getAbcYear() {
		return abcYear;
	}
	/**
	 * @param abcYear the abcYear to set
	 */
	public void setAbcYear(String abcYear) {
		this.abcYear = abcYear;
	}
	/**
	 * @return the alDivision
	 */
	public ArrayList getAlDivision() {
		return alDivision;
	}
	/**
	 * @param alDivision the alDivision to set
	 */
	public void setAlDivision(ArrayList alDivision) {
		this.alDivision = alDivision;
	}
	/**
	 * @return the alClassiffication
	 */
	public ArrayList getAlClassiffication() {
		return alClassiffication;
	}
	/**
	 * @param alClassiffication the alClassiffication to set
	 */
	public void setAlClassiffication(ArrayList alClassiffication) {
		this.alClassiffication = alClassiffication;
	}
	/**
	 * @return the alMonth
	 */
	public ArrayList getAlMonth() {
		return alMonth;
	}
	/**
	 * @param alMonth the alMonth to set
	 */
	public void setAlMonth(ArrayList alMonth) {
		this.alMonth = alMonth;
	}
	/**
	 * @return the alYear
	 */
	public ArrayList getAlYear() {
		return alYear;
	}
	/**
	 * @param alYear the alYear to set
	 */
	public void setAlYear(ArrayList alYear) {
		this.alYear = alYear;
	}
	
	 /**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}
	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	
}
