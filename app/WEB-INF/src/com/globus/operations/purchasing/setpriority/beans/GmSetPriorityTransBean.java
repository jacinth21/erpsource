package com.globus.operations.purchasing.setpriority.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**GmSetPriorityTransBean - class used for save ,allocate and confirm the set priority Report Details
 * @author tramasamy
 *
 */
public class GmSetPriorityTransBean extends GmBean {
	 /**
	 * this used for enabling logging
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/** This  Constructor will validate and populate company info
	 * @param gmDataStoreVO
	 * @throws AppError
	 */
	public GmSetPriorityTransBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
	
	/**saveSetPriorityUploadDtls:This method used to save set priority mapping details into t5402 table
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String saveSetPriorityUploadDtls(HashMap hmParam) throws AppError {


		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


		String strJsonString = "";

		String strPriorityId = GmCommonClass.parseNull((String) hmParam
				.get("PRIORITYID"));
		String strInputstr = GmCommonClass.parseNull((String) hmParam
				.get("INPUTSTRING"));
		String strUserId = GmCommonClass.parseNull((String) hmParam
				.get("USERID"));

		gmDBManager.setPrepareString(
				"gm_pkg_op_set_priority_trans.gm_sav_set_priority_bulk_upload",
				4);

		gmDBManager.registerOutParameter(4, OracleTypes.CLOB);

		gmDBManager.setString(1, strPriorityId);
		gmDBManager.setString(2, strInputstr);
		gmDBManager.setString(3, strUserId);

		gmDBManager.execute();
		strJsonString = GmCommonClass.parseNull(gmDBManager.getString(4));
		gmDBManager.commit();

		return strJsonString;
	}

	/**
	 * removeSetPriorityUploadDtls:This method void the set priority mapping
	 * details when remove delete icon
	 * 
	 * @param str_inputstr
	 * @param strUserId
	 * @param strPriorityId
	 * @return
	 * @throws AppError
	 */
	public String removeSetPriorityUploadDtls(String str_inputstr,String strUserId,String strPriorityId) throws AppError {

		
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	
		gmDBManager.setPrepareString("gm_pkg_op_set_priority_trans.gm_remove_set_priority_map_dtls", 4);
		
		gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
		
		gmDBManager.setString(1, str_inputstr);
		gmDBManager.setString(2, strUserId);
		gmDBManager.setString(3, strPriorityId);
		
		gmDBManager.execute();
	    String	strStatus = GmCommonClass.parseNull(gmDBManager.getString(4));
		gmDBManager.commit();
		return strStatus;
	}
	
	
	/**allocateSetPriority:This method used to change set priority master status into allocation inprogress 
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String allocateSetPriority(HashMap hmParam) throws AppError {

		
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		String strJsonString = "";

		String strPriorityId = GmCommonClass.parseNull((String) hmParam.get("PRIORITYID"));
		String strCurrentStatus = GmCommonClass.parseNull((String) hmParam.get("SPSTATUSID"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		
		gmDBManager.setPrepareString("gm_pkg_op_set_priority_trans.gm_allocate_set_priority", 5);
		
		gmDBManager.registerOutParameter(5, OracleTypes.CLOB);
		
		gmDBManager.setString(1, strPriorityId);
		gmDBManager.setString(2, strCurrentStatus);
		gmDBManager.setString(3, "108302");//Allocation In progress(new status)
		gmDBManager.setString(4, strUserId);
		
		gmDBManager.execute();
		strJsonString = GmCommonClass.parseNull(gmDBManager.getString(5));
		gmDBManager.commit();
		return strJsonString;
	}
	
	/**confirmSetPriority - This method used to change the SP status "Allocated to Confirm In progress"
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String confirmSetPriority(HashMap hmParam) throws AppError{
		
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
    HashMap hmJMSParam = new HashMap();
	String strJsonString = "";
	String strQueueName="";
	String strConsumerClass ="";
	
	String strPriorityId = GmCommonClass.parseNull((String) hmParam.get("PRIORITYID"));
	String strStatusId = GmCommonClass.parseNull((String) hmParam.get("SPSTATUSID"));
	String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
	
	gmDBManager.setPrepareString("gm_pkg_op_set_priority_trans.gm_sav_confirm_set_priority", 5);
	gmDBManager.registerOutParameter(5, OracleTypes.CLOB);
	gmDBManager.setString(1, strPriorityId);
	gmDBManager.setString(2, strStatusId);
	gmDBManager.setInt(3, 108305);
	gmDBManager.setInt(4, Integer.parseInt(strUserID));
	gmDBManager.execute();
	strJsonString = GmCommonClass.parseNull(gmDBManager.getString(5));
	gmDBManager.commit();
     
	if(strJsonString.equals("")){// If Error is Null to call the JMS for update the SP status 
	strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_TTP_SUMMARY_QUEUE"));
	hmJMSParam.put("QUEUE_NAME", strQueueName);
	strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("SET_PRIORITY_CONSUMER_CLASS"));
	hmJMSParam.put("CONSUMERCLASS", strConsumerClass);
	hmJMSParam.put("PRIORITYID", strPriorityId);
	hmJMSParam.put("COMPANYINFO",strCompanyInfo);

	gmConsumerUtil.sendMessage(hmJMSParam);
	}
	return strJsonString;
}
	
	/**processSetPriorityAllocation -This method used to allocate the request qty and once confirmed 
	 *  to assign the priority id for each request.  
	 * 
	 * @param hmParam
	 * @throws AppError
	 */
	public void processSetPriorityAllocation(HashMap hmParam) throws AppError {

		String strPriorityId = GmCommonClass.parseNull((String) hmParam
				.get("PRIORITYID"));
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_set_priority_allocation.gm_allocate_set_priority_main",1);
		gmDBManager.setString(1, strPriorityId);
		gmDBManager.execute();
		gmDBManager.commit();

	}
}
