package com.globus.operations.purchasing.setpriority.actions;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.operations.purchasing.setpriority.beans.GmSetPriorityReportBean;
import com.globus.operations.purchasing.setpriority.beans.GmSetPriorityTransBean;
import com.globus.operations.purchasing.setpriority.forms.GmSetPriorityUploadForm;


public class GmSetPriorityUploadAction extends GmDispatchAction {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	
	/**loadSetPriorityUpload:This method used to load when click from  left link
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward loadSetPriorityUpload(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
		// call the instantiate method
		instantiate(request, response);
		GmSetPriorityUploadForm gmSetPriorityUploadForm = (GmSetPriorityUploadForm) form;
		gmSetPriorityUploadForm.loadSessionParameters(request);
		GmSetPriorityReportBean gmSetPriorityReportBean = new GmSetPriorityReportBean(getGmDataStoreVO());
        GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
        
		 HashMap hmParam = new HashMap();
		 HashMap hmAccess = new HashMap();
		 String strPartyId = "";
		 String strButtonAcc = "";
		 hmParam = GmCommonClass.getHashMapFromForm(gmSetPriorityUploadForm);
		//Get 1000 value from MAX DATA for Validation if Excel copy to grid data is 1000 above
		String  strMaxUploadData = GmCommonClass.parseNull(GmCommonClass.getRuleValue("MAX_DATA", "PD_DATA_SETUP"));
	    gmSetPriorityUploadForm.setStrMaxUploadData(strMaxUploadData);
		//Button access for set Priority upload
		 strPartyId = GmCommonClass.parseNull((String) gmSetPriorityUploadForm.getSessPartyId());
		 hmAccess = GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId, "SET_UPL_BTN_ACCESS")));
		 strButtonAcc = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
		 log.debug(" Set Priority Upload - Permissions Flag = " + hmAccess);
		 gmSetPriorityUploadForm.setButtonAccessFl(strButtonAcc);
		return mapping.findForward("gmSetPriorityUpload");
	}
	
	
	/**fetchSetPriorityUpload:This method fetch set priority mapping detais from t5402 table
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward fetchSetPriorityUpload(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
		// call the instantiate method
		 instantiate(request, response);
		 GmSetPriorityUploadForm gmSetPriorityUploadForm = (GmSetPriorityUploadForm) form;
		 gmSetPriorityUploadForm.loadSessionParameters(request);
		 GmSetPriorityReportBean gmSetPriorityReportBean = new GmSetPriorityReportBean(getGmDataStoreVO());
		 GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
		 
		 HashMap hmParam = new HashMap();
		 HashMap hmAccess = new HashMap();
		 String strPartyId = "";
		 String strButtonAcc = "";
	     //Get 1000 value from MAX DATA for Validation if Excel copy to grid data is 1000 above
		 String  strMaxUploadData = GmCommonClass.parseNull(GmCommonClass.getRuleValue("MAX_DATA", "PD_DATA_SETUP"));
		 gmSetPriorityUploadForm.setStrMaxUploadData(strMaxUploadData);
		 
		 hmParam = GmCommonClass.getHashMapFromForm(gmSetPriorityUploadForm);
			//Button access for set Priority upload
		 strPartyId = GmCommonClass.parseNull((String) gmSetPriorityUploadForm.getSessPartyId());
	     hmAccess = GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId, "SET_UPL_BTN_ACCESS")));
	     strButtonAcc = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
	     log.debug(" Set Priority Upload - Permissions Flag = " + hmAccess);
	     gmSetPriorityUploadForm.setButtonAccessFl(strButtonAcc);
	     
		 String strPriorityId = GmCommonClass.parseNull((String) hmParam.get("PRIORITYID"));
		
		 String strJsonString = GmCommonClass.parseNull(gmSetPriorityReportBean.fetchSetPriorityUploadDtls(strPriorityId));
		
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJsonString.equals("")) {
			pw.write(strJsonString);
		}
		pw.flush();
		return null;
	}
	
	
	/**saveSetPriorityUpload:This method used to save set priority details
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward saveSetPriorityUpload(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
		// call the instantiate method
		instantiate(request, response);
		GmSetPriorityUploadForm gmSetPriorityUploadForm = (GmSetPriorityUploadForm) form;
		gmSetPriorityUploadForm.loadSessionParameters(request);
		GmSetPriorityTransBean gmSetPriorityTransBean = new GmSetPriorityTransBean(getGmDataStoreVO());

		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmSetPriorityUploadForm);
		
		String strJsonString = GmCommonClass.parseNull(gmSetPriorityTransBean.saveSetPriorityUploadDtls(hmParam));
	
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJsonString.equals("")) {
			pw.write(strJsonString);
		}
		pw.flush();
		return null;
	}
	
	/**deleteSetPriorityUpload:This method used to void the set priority mapping details
	 * when delete icon click and return status as open
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward deleteSetPriorityUpload(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
		// call the instantiate method
		instantiate(request, response);
		GmSetPriorityUploadForm gmSetPriorityUploadForm = (GmSetPriorityUploadForm) form;
		gmSetPriorityUploadForm.loadSessionParameters(request);
		GmSetPriorityTransBean gmSetPriorityTransBean = new GmSetPriorityTransBean(getGmDataStoreVO());
		
		HashMap hmParam = new HashMap();
		
		hmParam = GmCommonClass.getHashMapFromForm(gmSetPriorityUploadForm);
		
		String strinputstr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strPriorityId = GmCommonClass.parseNull((String) hmParam.get("PRIORITYID"));
		
        String Status = gmSetPriorityTransBean.removeSetPriorityUploadDtls(strinputstr,strUserId,strPriorityId);
    
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!Status.equals("")) {
			pw.write(Status);
		}
		pw.flush();
		return null;
	}
	
	/**saveAllocateRequest:This method used to save allocate request details via JMS Queue
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward saveAllocateRequest(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
		// call the instantiate method
		instantiate(request, response);
		GmSetPriorityUploadForm gmSetPriorityUploadForm = (GmSetPriorityUploadForm) form;
		gmSetPriorityUploadForm.loadSessionParameters(request);
		GmSetPriorityTransBean gmSetPriorityTransBean = new GmSetPriorityTransBean(getGmDataStoreVO());

		HashMap hmParam = new HashMap();
		HashMap hmJMSParam = new HashMap();
		String strConsumerClass = "";
		String strQueueName = "";
		
		hmParam = GmCommonClass.getHashMapFromForm(gmSetPriorityUploadForm);
		String strPriorityId = GmCommonClass.parseNull((String) hmParam.get("PRIORITYID"));
		
		String strJsonString = GmCommonClass.parseNull(gmSetPriorityTransBean.allocateSetPriority(hmParam));
		
		GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
		// Called JMS For update the status from allocation Inprogress to  allocate or allocate failed Status. 
		strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_TTP_SUMMARY_QUEUE"));
		hmJMSParam.put("QUEUE_NAME", strQueueName);
		strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("SET_PRIORITY_CONSUMER_CLASS"));
		hmJMSParam.put("CONSUMERCLASS", strConsumerClass);
		hmJMSParam.put("COMPANYINFO",GmCommonClass.parseNull(request.getParameter("companyInfo")));
		hmJMSParam.put("PRIORITYID", strPriorityId);
		gmConsumerUtil.sendMessage(hmJMSParam);

	
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJsonString.equals("")) {
			pw.write(strJsonString);
		}
		pw.flush();
		return null;
	}
	
}
