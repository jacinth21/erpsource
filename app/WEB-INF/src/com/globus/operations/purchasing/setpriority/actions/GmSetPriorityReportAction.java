package com.globus.operations.purchasing.setpriority.actions;

import java.io.PrintWriter;
import java.io.IOException;
import java.text.ParseException;

import java.util.ArrayList;
import java.util.HashMap;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.operations.purchasing.setpriority.forms.GmSetPriorityReportForm;
import com.globus.operations.purchasing.setpriority.beans.GmSetPriorityReportBean;
import com.globus.operations.purchasing.setpriority.beans.GmSetPriorityTransBean;


/**GmSetPriorityReportAction:This class used to load set priority report details 
 * @author pvigneshwaran
 *
 */
public class GmSetPriorityReportAction extends GmDispatchAction{
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	/**fetchSetPriorityMasterDtls:This method used to fetch master details from t5400 table
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward fetchSetPriorityMasterDtls(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
		// call the instantiate method
		instantiate(request, response);
		GmSetPriorityReportForm gmSetPriorityReportForm = (GmSetPriorityReportForm) form;
		gmSetPriorityReportForm.loadSessionParameters(request);
		GmSetPriorityReportBean gmSetPriorityReportBean = new GmSetPriorityReportBean(getGmDataStoreVO());

		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmSetPriorityReportForm);
		
		String strPriorityId = GmCommonClass.parseNull((String) hmParam.get("PRIORITYID"));
		String strJsonString = GmCommonClass.parseNull(gmSetPriorityReportBean.fetchSetPriorityMaster(strPriorityId));
	
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJsonString.equals("")) {
			pw.write(strJsonString);
		}
		pw.flush();
		return null;
	}
	 /** loadSetPriorityReports - this method used load the SP reports screen
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 * @throws ParseException
	 */

	public ActionForward loadSetPriorityReports(ActionMapping actionMapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
		      IOException{
		
		// call the instantiate method
		instantiate(request, response);
		GmSetPriorityReportForm gmSetPriorityReportForm = (GmSetPriorityReportForm)form;
		GmSetPriorityReportBean gmSetPriorityRepportBean = new GmSetPriorityReportBean(getGmDataStoreVO());
		
		ArrayList alSpType = new ArrayList();
		ArrayList alSpQuarter = new ArrayList();
		ArrayList alSpYear = new ArrayList();
		ArrayList alSpStatus = new ArrayList();
		
		alSpType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("SPRTYP"));
		alSpQuarter = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("QUARTE"));
		alSpYear = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("YEAR"));
	    alSpStatus = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("SPRSTA"));
	    		
		gmSetPriorityReportForm.setAlSpType(alSpType);
		gmSetPriorityReportForm.setAlSpQuarter(alSpQuarter);
		gmSetPriorityReportForm.setAlSpYear(alSpYear);
		gmSetPriorityReportForm.setAlSpStatus(alSpStatus);
		
	
		return actionMapping.findForward("gmSetPriorityReport");
		
		
		
	}
	
	
	 /** fetchSetPriorityReports - this method used to fetch the Set Priority Reports
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 * @throws ParseException
	 */
	
	public ActionForward fetchSetPriorityReports(ActionMapping actionMapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
		      IOException{
		
			instantiate(request, response);
		 	GmSetPriorityReportForm gmSetPriorityReportForm = (GmSetPriorityReportForm)form;
			gmSetPriorityReportForm.loadSessionParameters(request);
			GmSetPriorityReportBean gmSetPriorityReportBean = new GmSetPriorityReportBean(getGmDataStoreVO());
			HashMap hmParam = new HashMap();
			 hmParam =  GmCommonClass.getHashMapFromForm(gmSetPriorityReportForm);
			 
			 String strSetPriorityReportJsonString =
			          GmCommonClass.parseNull(gmSetPriorityReportBean.fetchSetPriorityReports(hmParam));
			   
			        response.setContentType("text/json");
			        PrintWriter pw = response.getWriter();
			        if (!strSetPriorityReportJsonString.equals("")) {
			        pw.write(strSetPriorityReportJsonString);
			        }
			        pw.flush();
			      return null; 
	}
	
	/**loadSetPriorityDetailsReport - This method used to Load the set Priority Detail Report
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward loadSetPriorityDetailsReport(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		// call the instantiate method
		instantiate(request, response);
		GmSetPriorityReportForm gmSetPriorityReportForm = (GmSetPriorityReportForm) form;
		gmSetPriorityReportForm.loadSessionParameters(request);
		GmSetPriorityReportBean gmSetPriorityReportBean = new GmSetPriorityReportBean(getGmDataStoreVO());
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();

		HashMap hmParam = new HashMap();
		HashMap hmAccess = new HashMap();
		String strPartyId = "";
		String strButtonAcc = "";
		String strJsonSetPriorityDtls = "";

		hmParam = GmCommonClass.getHashMapFromForm(gmSetPriorityReportForm);

		// Button access for set Priority details screen
		strPartyId = GmCommonClass.parseNull((String) gmSetPriorityReportForm.getSessPartyId());
		hmAccess = GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId, "SET_UPL_BTN_ACCESS")));
		strButtonAcc = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
		log.debug(" Set Priority Upload - Permissions Flag = " + hmAccess);
		gmSetPriorityReportForm.setButtonAccessFl(strButtonAcc);

		return mapping.findForward("gmSPDetailReport");
	}
	
	/** fetchSetPriorityDetailsReport - this method used to fetch the SP details - based on SP id
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward fetchSetPriorityDetailsReport(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		// call the instantiate method
		instantiate(request, response);
		GmSetPriorityReportForm gmSetPriorityReportForm = (GmSetPriorityReportForm) form;
		gmSetPriorityReportForm.loadSessionParameters(request);
		GmSetPriorityReportBean gmSetPriorityReportBean = new GmSetPriorityReportBean(
				getGmDataStoreVO());
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();

		HashMap hmParam = new HashMap();
		HashMap hmAccess = new HashMap();
		String strPartyId = "";
		String strButtonAcc = "";
		String strJsonSetPriorityDtls = "";

		hmParam = GmCommonClass.getHashMapFromForm(gmSetPriorityReportForm);

		// Button access for set Priority Details screen
		strPartyId = GmCommonClass.parseNull((String) gmSetPriorityReportForm.getSessPartyId());
		hmAccess = GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId, "SET_UPL_BTN_ACCESS")));
		strButtonAcc = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
		log.debug(" Set Priority Upload - Permissions Flag = " + hmAccess);
		gmSetPriorityReportForm.setButtonAccessFl(strButtonAcc);

		String strPriorityId = GmCommonClass.parseNull((String) hmParam.get("PRIORITYID"));
		strJsonSetPriorityDtls = GmCommonClass.parseNull(gmSetPriorityReportBean.fetchSetPriorityDetailsReports(strPriorityId));

		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJsonSetPriorityDtls.equals("")) {
			pw.write(strJsonSetPriorityDtls);
		}
		pw.flush();
		return null;
	}
	
	/**confirmSetPriority - This method used to update the status from allocated to Confirm Inprogress
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward confirmSetPriority(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
		// call the instantiate method
		instantiate(request, response);
		GmSetPriorityReportForm gmSetPriorityReportForm = (GmSetPriorityReportForm) form;
		gmSetPriorityReportForm.loadSessionParameters(request);
		GmSetPriorityTransBean gmSetPriorityTransBean = new GmSetPriorityTransBean(getGmDataStoreVO());
		
		HashMap hmParam = new HashMap();
		String strJsonSPConfirmDtls ="";
		hmParam = GmCommonClass.getHashMapFromForm(gmSetPriorityReportForm);
		hmParam.put("COMPANYINFO", GmCommonClass.parseNull(request.getParameter("companyInfo")));
		strJsonSPConfirmDtls = GmCommonClass.parseNull(gmSetPriorityTransBean.confirmSetPriority(hmParam));
	
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJsonSPConfirmDtls.equals("")) {
			pw.write(strJsonSPConfirmDtls);
		}
		pw.flush();
		return null;
	}
}
