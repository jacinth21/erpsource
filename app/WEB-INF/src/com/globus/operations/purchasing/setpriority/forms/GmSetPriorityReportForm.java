package com.globus.operations.purchasing.setpriority.forms;

import java.util.ArrayList;


import com.globus.operations.purchasing.setpriority.forms.GmSetPriorityHeaderForm;

public class GmSetPriorityReportForm extends GmSetPriorityHeaderForm{
	
	
	private String gridString = "";
	private String buttonAccessFl = "";

	
	private ArrayList alSpType = new ArrayList();
	private ArrayList alSpQuarter = new ArrayList();
	private ArrayList alSpYear = new ArrayList();
	private ArrayList alSpStatus = new ArrayList();
	/**
	 * @return the gridString
	 */
	public String getGridString() {
		return gridString;
	}
	
	/**
	 * @param gridString the gridString to set
	 */
	
	public void setGridString(String gridString) {
		this.gridString = gridString;
	}
	
	/**
	 * @return the buttonAccessFl
	*/
	public String getButtonAccessFl() {
		return buttonAccessFl;
	}
	/**
	 * @param buttonAccessFl the buttonAccessFl to set
	 */
	public void setButtonAccessFl(String buttonAccessFl) {
		this.buttonAccessFl = buttonAccessFl;
	}
	
	/**
	 * @return the alSpType
	*/
	public ArrayList getAlSpType() {
		return alSpType;
	}
	/**
	 * @param alSpType the alSpType to set
	 */
	public void setAlSpType(ArrayList alSpType) {
		this.alSpType = alSpType;
	}
	
	/**
	 * @return the alSpQuarter
	 */
	public ArrayList getAlSpQuarter() {
		return alSpQuarter;
	}
	/**
	 * @param alSpQuarter the alSpQuarter to set
	 */
	public void setAlSpQuarter(ArrayList alSpQuarter) {
		this.alSpQuarter = alSpQuarter;
	}
	
	/**
	 * @return the alSpYear
	 */
	
	public ArrayList getAlSpYear() {
		return alSpYear;
	}
	
	/**
	 * @param alSpYear the alSpYear to set
	 */
	public void setAlSpYear(ArrayList alSpYear) {
		this.alSpYear = alSpYear;
	}
	
	/**
	 * @return the alSpStatus
	 */
	
	public ArrayList getAlSpStatus() {
		return alSpStatus;
	}
	
	/**
	 * @param alSpStatus the alSpStatus to set
	 */
	
	public void setAlSpStatus(ArrayList alSpStatus) {
		this.alSpStatus = alSpStatus;
	}	
	
}
