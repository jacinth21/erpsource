package com.globus.operations.purchasing.setpriority.forms;

import java.util.Date;

import com.globus.common.forms.GmCommonForm;
import com.globus.common.beans.GmCalenderOperations;

public class GmSetPriorityHeaderForm extends GmCommonForm{
	
	
	private String priorityId = "";
	private String priorityName = "";
	private String spPredefinedQty = "";
	private String spYear = String.valueOf(GmCalenderOperations.getCurrentYear());
	private String spQuarter = "";
	private String spSequenceNo = "";
	private String spAllocatedDate = "";
	private String spStatusId = "";
	private String spStatusName = "";
	private String spTypeId = "";
	private String spTypeName = "";
	private String spInitiatedBy = "";
	private String spInitiatedDate="";
	private String spSubmittedBy = "";
	private String spSubmittedDate = "";
	/**
	 * @return the priorityId
	 */
	public String getPriorityId() {
		return priorityId;
	}
	/**
	 * @param priorityId the priorityId to set
	 */
	public void setPriorityId(String priorityId) {
		this.priorityId = priorityId;
	}
	/**
	 * @return the priorityName
	 */
	public String getPriorityName() {
		return priorityName;
	}
	/**
	 * @param priorityName the priorityName to set
	 */
	public void setPriorityName(String priorityName) {
		this.priorityName = priorityName;
	}
	/**
	 * @return the spPredefinedQty
	 */
	public String getSpPredefinedQty() {
		return spPredefinedQty;
	}
	/**
	 * @param spPredefinedQty the spPredefinedQty to set
	 */
	public void setSpPredefinedQty(String spPredefinedQty) {
		this.spPredefinedQty = spPredefinedQty;
	}
	/**
	 * @return the spYear
	 */
	public String getSpYear() {
		return spYear;
	}
	/**
	 * @param spYear the spYear to set
	 */
	public void setSpYear(String spYear) {
		this.spYear = spYear;
	}
	/**
	 * @return the spQuarter
	 */
	public String getSpQuarter() {
		return spQuarter;
	}
	/**
	 * @param spQuarter the spQuarter to set
	 */
	public void setSpQuarter(String spQuarter) {
		this.spQuarter = spQuarter;
	}
	/**
	 * @return the spSequenceNo
	 */
	public String getSpSequenceNo() {
		return spSequenceNo;
	}
	/**
	 * @param spSequenceNo the spSequenceNo to set
	 */
	public void setSpSequenceNo(String spSequenceNo) {
		this.spSequenceNo = spSequenceNo;
	}
	/**
	 * @return the spAllocatedDate
	 */
	public String getSpAllocatedDate() {
		return spAllocatedDate;
	}
	/**
	 * @param spAllocatedDate the spAllocatedDate to set
	 */
	public void setSpAllocatedDate(String spAllocatedDate) {
		this.spAllocatedDate = spAllocatedDate;
	}
	/**
	 * @return the spStatusId
	 */
	public String getSpStatusId() {
		return spStatusId;
	}
	/**
	 * @param spStatusId the spStatusId to set
	 */
	public void setSpStatusId(String spStatusId) {
		this.spStatusId = spStatusId;
	}
	/**
	 * @return the spStatusName
	 */
	public String getSpStatusName() {
		return spStatusName;
	}
	/**
	 * @param spStatusName the spStatusName to set
	 */
	public void setSpStatusName(String spStatusName) {
		this.spStatusName = spStatusName;
	}
	/**
	 * @return the spTypeId
	 */
	public String getSpTypeId() {
		return spTypeId;
	}
	/**
	 * @param spTypeId the spTypeId to set
	 */
	public void setSpTypeId(String spTypeId) {
		this.spTypeId = spTypeId;
	}
	/**
	 * @return the spTypeName
	 */
	public String getSpTypeName() {
		return spTypeName;
	}
	/**
	 * @param spTypeName the spTypeName to set
	 */
	public void setSpTypeName(String spTypeName) {
		this.spTypeName = spTypeName;
	}
	/**
	 * @return the spInitiatedBy
	 */
	public String getSpInitiatedBy() {
		return spInitiatedBy;
	}
	/**
	 * @param spInitiatedBy the spInitiatedBy to set
	 */
	public void setSpInitiatedBy(String spInitiatedBy) {
		this.spInitiatedBy = spInitiatedBy;
	}
	/**
	 * @return the spInitiatedDate
	 */
	public String getSpInitiatedDate() {
		return spInitiatedDate;
	}
	/**
	 * @param spInitiatedDate the spInitiatedDate to set
	 */
	public void setSpInitiatedDate(String spInitiatedDate) {
		this.spInitiatedDate = spInitiatedDate;
	}
	/**
	 * @return the spSubmittedBy
	 */
	public String getSpSubmittedBy() {
		return spSubmittedBy;
	}
	/**
	 * @param spSubmittedBy the spSubmittedBy to set
	 */
	public void setSpSubmittedBy(String spSubmittedBy) {
		this.spSubmittedBy = spSubmittedBy;
	}
	/**
	 * @return the spSubmittedDate
	 */
	public String getSpSubmittedDate() {
		return spSubmittedDate;
	}
	/**
	 * @param spSubmittedDate the spSubmittedDate to set
	 */
	public void setSpSubmittedDate(String spSubmittedDate) {
		this.spSubmittedDate = spSubmittedDate;
	}
}
