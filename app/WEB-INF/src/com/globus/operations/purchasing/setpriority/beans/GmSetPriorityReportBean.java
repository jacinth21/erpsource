package com.globus.operations.purchasing.setpriority.beans;

import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.db.GmDBManager;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmRuleBean;
import com.globus.valueobject.common.GmDataStoreVO;

import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import com.globus.common.beans.GmLogger;

/**GmSetPriorityReportBean:This class used for set priority Report Details
 * @author pvigneshwaran
 *
 */
public class GmSetPriorityReportBean extends GmBean {
	 /**
	 * this used for enabling logging
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	/** This  Constructor will validate and populate company info
	 * @param gmDataStoreVO
	 * @throws AppError
	 */
	public GmSetPriorityReportBean(GmDataStoreVO gmDataStoreVO)
			throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Email Template Name 
	 */
	public static final String TEMPLATE_NAME = "GmSetPriorityEmail";
	
	/**fetchSetPriorityMaster:This method used to fetch set priority master details
	 * @param strPriorityId
	 * @return
	 * @throws AppError
	 */
	public String fetchSetPriorityMaster(String strPriorityId) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strJsonString = "";
		gmDBManager.setPrepareString("gm_pkg_op_set_priority_report.gm_fch_set_priority_master", 2);

		gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
		gmDBManager.setString(1, strPriorityId);
		gmDBManager.execute();
		strJsonString = GmCommonClass.parseNull(gmDBManager.getString(2));
		gmDBManager.close();
		return strJsonString;
	}
	
	/**fetchSetPriorityUploadDtls:This method used to fetch set priority set priority mapping details
	 * @param strPriorityId
	 * @return
	 * @throws AppError
	 */
	public String fetchSetPriorityUploadDtls(String strPriorityId) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strJsonString = "";
		gmDBManager.setPrepareString("gm_pkg_op_set_priority_report.gm_fch_set_priority_upload_dtls", 2);

		gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
		gmDBManager.setString(1, strPriorityId);
		gmDBManager.execute();
		strJsonString = GmCommonClass.parseNull(gmDBManager.getString(2));
		gmDBManager.close();
		return strJsonString;
	}
	
    /** fetchSetPriorityReports - This method used to fetch the Set Priority Reports.
	 * @param hmParam
	 * @return String
	 * @throws AppError
	 */
	
	public String fetchSetPriorityReports(HashMap hmParam) throws AppError{
		
		
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		
		 String strSetPriorityID = "";
		 String strSetPriorityStatus = "";
		 String strSetPriorityQuarter = "";
		 String strSetPriorityYear = "";
		 String strSetPriorityType = "";
		 String strSetPriorityRptJSONString = "";
		  
		  strSetPriorityID = GmCommonClass.parseNull((String)hmParam.get("PRIORITYID"));
		  strSetPriorityStatus = GmCommonClass.parseZero((String)hmParam.get("SPSTATUSID"));
		  strSetPriorityQuarter = GmCommonClass.parseZero((String)hmParam.get("SPQUARTER"));
		  strSetPriorityYear = GmCommonClass.parseZero((String)hmParam.get("SPYEAR"));
		  strSetPriorityType = GmCommonClass.parseZero((String)hmParam.get("SPTYPEID"));
		  	  
		  strSetPriorityID = (!strSetPriorityID.equals("")) ? strSetPriorityID.toUpperCase() : "";
	
		  
		  gmDBManager.setPrepareString("gm_pkg_op_set_priority_report.gm_fch_set_priority_report",6);
		  gmDBManager.registerOutParameter(6,OracleTypes.CLOB);
		  gmDBManager.setString(1, strSetPriorityID);
		  gmDBManager.setString(2, strSetPriorityYear);
		  gmDBManager.setString(3, strSetPriorityQuarter);
		  gmDBManager.setString(4, strSetPriorityStatus);
		  gmDBManager.setString(5, strSetPriorityType);
		  gmDBManager.execute();
		  strSetPriorityRptJSONString = GmCommonClass.parseNull(gmDBManager.getString(6));
		  gmDBManager.close();
	 
		  return strSetPriorityRptJSONString;	
	}
	
	/**fetchSetPriorityDetailsReports - this method used to fetch the set priority details report
	 * @param strPriorityId
	 * @return
	 * @throws AppError
	 */
	public String fetchSetPriorityDetailsReports(String strPriorityId) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strJsonString = "";
		gmDBManager.setPrepareString("gm_pkg_op_set_priority_report.gm_fch_set_priority_details_report", 2);

		gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
		gmDBManager.setString(1, strPriorityId);
		gmDBManager.execute();
		strJsonString = GmCommonClass.parseNull(gmDBManager.getString(2));
		gmDBManager.close();
		return strJsonString;
	}
	/**
	 * sendMailNotification - this method used to send the Email Notification to
	 * user Regarding set priorty Id Status (Allocation or Confirm or Allocation
	 * Failed)
	 * 
	 * @param strPriorityId
	 * @param strPriorityName
	 * @throws AppError
	 */
	public void sendMailNotification(HashMap hmParam) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		GmEmailProperties gmEmailProperties = new GmEmailProperties();
		GmJasperMail jasperMail = new GmJasperMail();
		GmRuleBean gmRuleBean = new GmRuleBean();
		HashMap hmReturnDetails = new HashMap();
		HashMap hmTemp = new HashMap();
		ArrayList alResult = new ArrayList();
		HashMap hmReturn = new HashMap();
		String strFrom = "";
		String strTo = "";
		String strMimeType = "";
		String strSubject = "";
		String strMessageBody = "";
		String strJasperName = "";
		String strPriorityId = GmCommonClass.parseNull((String) hmParam
				.get("PRIORITYID"));
		String strUserId = GmCommonClass.parseNull((String) hmParam
				.get("USERID"));
		String strPriorityName = "";
		String strStatusName = "";
		String strStatusID = "";
		String strCCMailID = "";
		String strcmpId = GmCommonClass
				.parseNull(getGmDataStoreVO().getCmpid());
		String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
		// strPriorityId="USS-193-001";
		// This method used fetch failed set priority
		alResult = fetchFailedSetPriorityDetails(strPriorityId);

		GmResourceBundleBean gmResourceBundleBean =
				GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
		GmResourceBundleBean gmResourceBundle = GmCommonClass.getResourceBundleBean("properties.labels.operations.purchasing.setpriority.GmSetPriorityEmailNotification",strCompanyLocale);
		
		strFrom = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.FROM));
        strMimeType = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "."+ GmEmailProperties.MIME_TYPE));
		strSubject = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.SUBJECT));
		hmTemp.put("ALRESULT", alResult);
		// this used for get Email header
		hmReturn = fetchHeaderDetails(strPriorityId, strUserId);

		strTo = GmCommonClass.parseNull((String) hmReturn.get("TOEMAILID"));
		strPriorityName = GmCommonClass.parseNull((String) hmReturn.get("SPNAME"));
		strStatusName = GmCommonClass.parseNull((String) hmReturn.get("STATUSNM"));
		strStatusID = GmCommonClass.parseNull((String) hmReturn.get("STATUSID"));
		strCCMailID = GmCommonClass.parseNull((String) hmReturn.get("CCEMAILID"));

		// Get Message Body Based On Set priority Status
		if (strStatusID.equals("108303")) {
			strMessageBody = GmCommonClass.parseNull(gmResourceBundle.getProperty("ALLOCATION_FAILED"));
		} else if (strStatusID.equals("108304")) {
			strMessageBody = GmCommonClass.parseNull(gmResourceBundle.getProperty("ALLOCATION_SUCCESS"));
		} else if (strStatusID.equals("108306")) {
			strMessageBody = GmCommonClass.parseNull(gmResourceBundle.getProperty("CONFIRM_SUCCESS"));
		}

		if (alResult.size() > 0) {

			strSubject = GmCommonClass.replaceAll(strSubject, "#<STATUS>",
					strStatusName);
			strSubject = GmCommonClass.replaceAll(strSubject, "#<SP_NAME>",
					strPriorityName);
			strSubject = GmCommonClass.replaceAll(strSubject, "#<SP_ID>",
					strPriorityId);
			
			jasperMail.setReportData(alResult);

		} else {
				strSubject = GmCommonClass.replaceAll(strSubject, "#<STATUS>",
						strStatusName);
				strSubject = GmCommonClass.replaceAll(strSubject, "#<SP_NAME>",
						strPriorityName);
				strSubject = GmCommonClass.replaceAll(strSubject, "#<SP_ID>",
						strPriorityId);
             jasperMail.setReportData(null);
		}
		gmEmailProperties.setMimeType(strMimeType);
		gmEmailProperties.setSender(strFrom);
		gmEmailProperties.setRecipients(strTo);
		gmEmailProperties.setCc(strCCMailID);
		gmEmailProperties.setSubject(strSubject);
		hmTemp.put("MESSAGEBODY", strMessageBody);

		strJasperName = "/GmSetPriorityNotify.jasper";
		jasperMail.setJasperReportName(strJasperName);
		jasperMail.setAdditionalParams(hmTemp);
		jasperMail.setEmailProperties(gmEmailProperties);
		hmReturnDetails = jasperMail.sendMail();
	}

	/**
	 * fetchFailedSetPriorityDetails - This method used to call fetch failed set
	 * priority and send the array list values
	 * 
	 * @param strPriorityId
	 * @return
	 * @throws AppError
	 */
	public ArrayList fetchFailedSetPriorityDetails(String strPriorityId)
			throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		ArrayList alreturn = new ArrayList();
		
		gmDBManager.setPrepareString("gm_pkg_op_set_priority_report.gm_fch_failed_set_priority_details",2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strPriorityId);
		gmDBManager.execute();
		alreturn = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2)));
		gmDBManager.close();
		return alreturn;
	}

	/**
	 * fetchHeaderDetails - this method used for get Email header details based
	 * on SP ID
	 * 
	 * @param strPriorityId
	 * @param strUserId
	 * @return
	 * @throws AppError
	 */
	public HashMap fetchHeaderDetails(String strPriorityId, String strUserId)
			throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		HashMap hmReturn = new HashMap();
		gmDBManager.setPrepareString("gm_pkg_op_set_priority_report.gm_fch_email_header", 3);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.setString(1, strPriorityId);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();
		hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		return hmReturn;
	}
}

