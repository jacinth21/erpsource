package com.globus.operations.purchasing.setpriority.forms;

import java.util.ArrayList;
import java.util.Date;

import com.globus.operations.purchasing.setpriority.forms.GmSetPriorityHeaderForm;

/**GmSetPriorityUploadForm:This form used to save set priority transactions.
 * @author pvigneshwaran
 *
 */
public class GmSetPriorityUploadForm extends GmSetPriorityHeaderForm{

	private String inputString="";
	private String buttonAccessFl="";
	private String strMaxUploadData = "";
	  
	/**
	 * @return the strMaxUploadData
	 */
	public String getStrMaxUploadData() {
		return strMaxUploadData;
	}
	/**
	 * @param strMaxUploadData the strMaxUploadData to set
	 */
	public void setStrMaxUploadData(String strMaxUploadData) {
		this.strMaxUploadData = strMaxUploadData;
	}
	/**
	 * @return the inputString
	 */
	public String getInputString() {
		return inputString;
	}
	/**
	 * @param inputString the inputString to set
	 */
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}
	/**
	 * @return the buttonAccessFl
	 */
	public String getButtonAccessFl() {
		return buttonAccessFl;
	}
	/**
	 * @param buttonAccessFl the buttonAccessFl to set
	 */
	public void setButtonAccessFl(String buttonAccessFl) {
		this.buttonAccessFl = buttonAccessFl;
	}
}
