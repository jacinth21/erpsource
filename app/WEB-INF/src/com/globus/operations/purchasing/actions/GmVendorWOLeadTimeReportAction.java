package com.globus.operations.purchasing.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.purchasing.beans.GmWODueDateBulkUploadBean;
import com.globus.operations.purchasing.forms.GmVendorWOLeadTimeReportForm;
import com.globus.logon.beans.GmLogonBean;
import com.globus.corporate.beans.GmDivisionBean;

/**
 * @author asingaravel
 *
 */
public class GmVendorWOLeadTimeReportAction extends GmDispatchAction
{
	 Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
       

	    /**Description:This Method will Load Vendor Lead Time Report screen
	     * @param mapping
	     * @param form
	     * @param request
	     * @param response
	     * @return
	     * @throws Exception
	     */
		public ActionForward loadVendorWOLeadTimeRpt(ActionMapping mapping, ActionForm form, HttpServletRequest request,
				HttpServletResponse response) throws Exception {

			instantiate(request, response);
			GmVendorWOLeadTimeReportForm gmVendorWOLeadTimeReportForm = (GmVendorWOLeadTimeReportForm) form;
			gmVendorWOLeadTimeReportForm.loadSessionParameters(request);

			GmLogonBean gmLogonBean = new GmLogonBean();
			GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());

			ArrayList alPartSearch = new ArrayList();
			ArrayList alPurchaseAgent = new ArrayList();
			ArrayList alDivision = new ArrayList();
			HashMap hmParam = new HashMap();
			hmParam = GmCommonClass.getHashMapFromForm(gmVendorWOLeadTimeReportForm);

			alPartSearch = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LIKES"));
			alPurchaseAgent = GmCommonClass.parseNullArrayList(gmLogonBean.getEmployeeList("300", "W"));
			alDivision = GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmParam));

			gmVendorWOLeadTimeReportForm.setAlPartSearch(alPartSearch);
			gmVendorWOLeadTimeReportForm.setAlPurchaseAgent(alPurchaseAgent);
			gmVendorWOLeadTimeReportForm.setAlDivision(alDivision);

			return mapping.findForward("GmVendorWOLeadTimeReport");
		}
		
	    /**fetchVendorWOLeadTimeRpt: method used to fetch Vendor Lead Time Report screen
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return
		 * @throws AppError
		 * @throws Exception
		 */
		public ActionForward fetchVendorWOLeadTimeRpt(ActionMapping mapping,
						ActionForm form, HttpServletRequest request,
						HttpServletResponse response) throws AppError, Exception {

			instantiate(request, response);
			GmVendorWOLeadTimeReportForm gmVendorWOLeadTimeReportForm = (GmVendorWOLeadTimeReportForm) form;
			gmVendorWOLeadTimeReportForm.loadSessionParameters(request);
					
			GmWODueDateBulkUploadBean gmWODueDateBulkUploadBean = new GmWODueDateBulkUploadBean(getGmDataStoreVO());
					
			HashMap hmParam = new HashMap();
			HashMap hmRegStr = new HashMap();
			String strJsonRptString = "";
			String strPartNumFormat = "";
			hmParam = GmCommonClass.getHashMapFromForm(gmVendorWOLeadTimeReportForm);
			
			//PartNumber Search Using lietral,Like-Prefix,Like_suffix					
			hmRegStr.put("PARTNUM", gmVendorWOLeadTimeReportForm.getPartNums());
			hmRegStr.put("SEARCH", gmVendorWOLeadTimeReportForm.getLikeSearch());
			strPartNumFormat = GmCommonClass.createRegExpString(hmRegStr);
			hmParam.put("PART", strPartNumFormat);
					
			//Get Report details using json string
			strJsonRptString = gmWODueDateBulkUploadBean.fetchVendorWOLeadTImeReport(hmParam);
			
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			if (!strJsonRptString.equals("")) {
				pw.write(strJsonRptString);
				pw.flush();	
			}	
		  	       	
			return null;	
		}
}