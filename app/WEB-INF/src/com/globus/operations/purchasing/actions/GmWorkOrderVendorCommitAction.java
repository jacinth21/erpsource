package com.globus.operations.purchasing.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.operations.beans.GmPurchaseBean;
import com.globus.operations.inventory.forms.GmFieldSalesWarehouseForm;
import com.globus.operations.purchasing.beans.GmWorkOrderVendorCommitBean;
import com.globus.operations.purchasing.forms.GmWorkOrderVendorCommitForm;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.beans.GmCalenderOperations;

/**
 * @author pvigneshwaran
 *
 */
public class GmWorkOrderVendorCommitAction extends GmDispatchAction
{
	 Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
       
	 /** loadVendorCommitEdit-to get vendor commit details
	     * @param mapping
	     * @param form
	     * @param request
	     * @param response
	     * @return
	     * @throws AppError
	     * @throws Exception
	     */
	    public ActionForward loadVendorCommitEdit(ActionMapping mapping, ActionForm form,
	    	      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
	    	  instantiate(request, response);
	    	
	    	 GmWorkOrderVendorCommitForm gmWorkOrderVendCommitForm = (GmWorkOrderVendorCommitForm) form;
	    	 GmWorkOrderVendorCommitBean gmWorkOrderVendCommitBean = new GmWorkOrderVendorCommitBean(getGmDataStoreVO());
	    	 GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
	    	 GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
	         GmPurchaseBean gmPurchaseBean = new GmPurchaseBean(getGmDataStoreVO());// To get PO Details
	      
	         HashMap hmReturn = new HashMap();
	         HashMap hmParam = new HashMap();
	         HashMap hmAccess = new HashMap();
	         
	         ArrayList alItemDetails = new ArrayList();
	         ArrayList alVendorDetails = new ArrayList();
	         
	         String strVendorId =  "";
	         String strApplnDateFmt = "";
	         String strApplJSDateFmt = "";
	         String strSessCompanyLocale = "";
	         String strPartyId = "";
	         String strSetupAccessflag = "";
	         
	         gmWorkOrderVendCommitForm.loadSessionParameters(request);
	         hmParam = GmCommonClass.getHashMapFromForm(gmWorkOrderVendCommitForm);
	         strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
	    	 strApplJSDateFmt = (String) session.getAttribute("strSessJSDateFmt");
	    	 strSessCompanyLocale =
	       	        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
	    	 strPartyId = (String) session.getAttribute("strSessPartyId");
	         hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId, "VENDOR_COMMIT_ACCESS"));  	
	         strSetupAccessflag = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
	         gmWorkOrderVendCommitForm.setStrAccessfl(strSetupAccessflag);
	           	  
	         strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
	         if(strVendorId.equals("")){
	        	 strVendorId= GmCommonClass.parseNull((String)request.getParameter("strVendorId"));
	         }
	         hmReturn = gmPurchaseBean.viewPO(strVendorId, "", "");
	         hmReturn.put("HMPARAM", hmParam);
	         alVendorDetails = GmCommonClass.parseNullArrayList(gmWorkOrderVendCommitBean.loadVendorCommitDtls(hmReturn));
	         
	         hmReturn.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
	         hmReturn.put("ALVENDORDETAILS", alVendorDetails);
	         hmReturn.put("TEMPNAME", "GmWOVendorCommitEdit.vm");
	         hmReturn.put("TEMPPATH", "operations/purchasing/templates");
	         hmReturn.put("RESOURCEBNDLPATH", "properties.labels.operations.purchasing.GmWOVendorCommitEdit");
	         hmReturn.put("SESSDATEFMT", strApplnDateFmt);
	         hmReturn.put("SESSJSDATEFMT", strApplJSDateFmt);
	         
	         gmWorkOrderVendCommitForm.setHmReturn(hmReturn);
	         String strXmlData = getXmlGridData(alVendorDetails,hmReturn);
	         gmWorkOrderVendCommitForm.setGridData(strXmlData);
	    	    return mapping.findForward("GmWOVendorCommitEdit");

	    	  }
	    

	    	  
	    /** saveVendorCommit- save vendor commit WO details
	     * @param mapping
	     * @param form
	     * @param request
	     * @param response
	     * @return
	     * @throws AppError
	     * @throws Exception
	     */
	    public ActionForward saveVendorCommit(ActionMapping mapping, ActionForm form,
	    	      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
	    	  instantiate(request, response);
	    	 GmWorkOrderVendorCommitForm gmWorkOrderVendCommitForm = (GmWorkOrderVendorCommitForm) form;
	    	 GmWorkOrderVendorCommitBean gmWorkOrderVendCommitBean = new GmWorkOrderVendorCommitBean(getGmDataStoreVO());
	    	 GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
	    	 gmWorkOrderVendCommitForm.loadSessionParameters(request);
	    	 
	    	 HashMap hmParam = new HashMap();
	    	 String strVendorId = "";
	         
	    	 hmParam = GmCommonClass.getHashMapFromForm(gmWorkOrderVendCommitForm);
	    	 strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
	    	 gmWorkOrderVendCommitBean.saveVendorCommit(hmParam);
	    	 String strRedirectURL = "//gmVendorCommit.do?method=loadVendorCommitEdit&strVendorId=" + strVendorId; 
	    	 return actionRedirect(strRedirectURL, request);

	    }
	    /**Description:This Method will Load Vendor Commit Report Details
	     * @param mapping
	     * @param form
	     * @param request
	     * @param response
	     * @return
	     * @throws Exception
	     */
	    public ActionForward loadVendorCommitRpt(ActionMapping mapping, ActionForm form,
	            HttpServletRequest request, HttpServletResponse response) throws Exception {
	          instantiate(request, response);
	          GmWorkOrderVendorCommitForm gmWorkOrderVendCommitForm = (GmWorkOrderVendorCommitForm) form;
	          gmWorkOrderVendCommitForm.loadSessionParameters(request);
	          GmWorkOrderVendorCommitBean gmWorkOrderVendCommitBean = new GmWorkOrderVendorCommitBean(getGmDataStoreVO());
	          GmPurchaseBean gmPurchaseBean = new GmPurchaseBean(getGmDataStoreVO());// To get PO Details
	          HashMap hmParam = new HashMap();
	          HashMap hmTemp = new HashMap();
	          HashMap hmParams = new HashMap();
	          ArrayList alVendorList = new ArrayList();
	          ArrayList alPartSearch = new ArrayList();
	          ArrayList alResult = new ArrayList();
	          ArrayList alOperators = new ArrayList();
	          ArrayList alReturn = new ArrayList();
	          hmParam = GmCommonClass.getHashMapFromForm(gmWorkOrderVendCommitForm);
	          String strSessCompanyLocale ="";
	          String strPartNumFormat = "";
	          String strPOId="";
	          String strXmlGridData="";
	          String strApplnDateFmt="";
	          String strOpt="";
	          strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
	          strSessCompanyLocale = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
	          strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
	          hmTemp.put("CHECKACTIVEFL","");// Display user who are all active
	          //Create Search dropdown
	          alPartSearch = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LIKES"));
	          alOperators = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("OPERS"));
	          gmWorkOrderVendCommitForm.setAlPartSearch(alPartSearch);
	          gmWorkOrderVendCommitForm.setAlDueDays(alOperators);
	          hmParam.put("SESSDATEFMT", strApplnDateFmt);
	          hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
	          hmParam.put("TEMPPATH", "operations/purchasing/templates");
	          hmParam.put("TEMPNAME", "GmWOVendorCommitReport.vm");
	          hmParam.put("RESOURCEBNDLPATH", "properties.labels.operations.purchasing.GmWOVendorCommitReport");
	          hmParams.put("PARTNUM", gmWorkOrderVendCommitForm.getPartNums());
	          hmParams.put("SEARCH", gmWorkOrderVendCommitForm.getLikeSearch());
	          strPartNumFormat = GmCommonClass.createRegExpString(hmParams);
	          hmParam.put("PARTNUMBER", strPartNumFormat);
	          if (strOpt.equals("Load")) {
	        	  alResult = gmWorkOrderVendCommitBean
					.loadVendorCommitRptDtls(hmParam);
	        		  alResult = applySearchCriteria(alResult,gmWorkOrderVendCommitForm);	 
	        	  strXmlGridData = getXmlGridData(alResult, hmParam);
	        	  gmWorkOrderVendCommitForm.setGridData(strXmlGridData); 	        	
		}
	         

	          
	          return mapping.findForward("GmWOVendorCommitReport");
	        }
	    
	    /**
		 * @param alResult
		 * @param gmWorkOrderVendCommitForm
		 * @return
		 * @throws AppError
		 */
	    public ArrayList applySearchCriteria(ArrayList alResult,GmWorkOrderVendorCommitForm gmWorkOrderVendCommitForm) throws AppError {
			GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
			String strDueDaysRange = GmCommonClass.parseNull(gmWorkOrderVendCommitForm.getDueDays());
			String strNoOfDays = GmCommonClass.parseNull(gmWorkOrderVendCommitForm.getNoOfDays());
			if (!strNoOfDays.equals("")) {
				gmSearchCriteria.addSearchCriteria("COMMIT_DUE_DAYS", strNoOfDays,strDueDaysRange);
			}
			gmSearchCriteria.query(alResult);
			return alResult;
		}
	    /**
	     * @param alItemDetails
	     * @param hmParam
	     * @return
	     * @throws AppError
	     */
	    public String getXmlGridData(ArrayList alItemDetails, HashMap hmParam) throws AppError {
	        GmTemplateUtil templateUtil = new GmTemplateUtil();
	        String strSessCompanyLocale =
	            GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
	        String strTempletPath = GmCommonClass.parseNull((String) hmParam.get("TEMPPATH"));
	        String strTempletName = GmCommonClass.parseNull((String) hmParam.get("TEMPNAME"));
	        String strResourceBdlePath = GmCommonClass.parseNull((String) hmParam.get("RESOURCEBNDLPATH"));
	        templateUtil.setDataList("alResult", alItemDetails);
	        templateUtil.setDataMap("hmApplnParam", hmParam);
	        templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strResourceBdlePath, strSessCompanyLocale));
	        templateUtil.setTemplateSubDir(strTempletPath);
	        templateUtil.setTemplateName(strTempletName);
	        return templateUtil.generateOutput();
	      }
    
}
