package com.globus.operations.purchasing.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.io.PrintWriter;
import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.actions.GmAction;
//Screen Forms and beans
import com.globus.operations.purchasing.orderplanning.beans.GmOPTTPSetupBean;
import com.globus.operations.purchasing.orderplanning.beans.GmOPTTPReportBean;
import com.globus.operations.purchasing.forms.GmOPTTPApprovalForm;



public class GmOPTTPApprovalAction extends GmDispatchAction {
	 Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
	 
	/**LoadTTPApproval:Method used to load ttp Approval Information
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward loadTTPApproval(ActionMapping actionMapping, ActionForm form,
    HttpServletRequest request,HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmOPTTPApprovalForm gmOPTTPApprovalForm = (GmOPTTPApprovalForm) form;
		gmOPTTPApprovalForm.loadSessionParameters(request);
		GmOPTTPSetupBean gmOPTTPSetupBean = new GmOPTTPSetupBean(getGmDataStoreVO());
		//Initialize
		HashMap hmParam = new HashMap();
		HashMap hmTemp = new HashMap();
		ArrayList alLockPeriod = new ArrayList();
		String strOpt = "";

		hmParam = GmCommonClass.getHashMapFromForm(gmOPTTPApprovalForm);
		// get Code Group Value
		gmOPTTPApprovalForm.setAlTTPList(gmOPTTPSetupBean.loadTTPList(hmTemp));
		gmOPTTPApprovalForm.setAlStatus(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("TTPST",getGmDataStoreVO())));
		gmOPTTPApprovalForm.setAlCategory(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("TTPCAT",getGmDataStoreVO())));
		gmOPTTPApprovalForm.setAlMonth(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("MONTH",getGmDataStoreVO())));
		gmOPTTPApprovalForm.setAlYear(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("YEAR",getGmDataStoreVO())));

		return actionMapping.findForward("GmOPTTPApproval");
	}
		
		/**fetchTTPApproval:This Method used to fetchTTPApproval Details
		 * @param actionMapping
		 * @param form
		 * @param request
		 * @param response
		 * @return
		 * @throws AppError
		 * @throws ServletException
		 * @throws IOException
		 * @throws ParseException
		 */
		public  ActionForward  fetchTTPApproval(ActionMapping actionMapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,IOException, ParseException {
			
		instantiate(request, response);
		GmOPTTPApprovalForm gmOPTTPApprovalForm = (GmOPTTPApprovalForm) form;
		gmOPTTPApprovalForm.loadSessionParameters(request);
		GmOPTTPSetupBean gmOPTTPSetupBean = new GmOPTTPSetupBean(getGmDataStoreVO());
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
       //Initialize
		ArrayList alLoadDtls = new ArrayList();   
	    HashMap hmReturn = new HashMap();
		HashMap hmParam = new HashMap();
		HashMap hmTemp = new HashMap();
		HashMap hmAccess = new HashMap();
		
		String strxmlGridData = "";
		String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
		String strSessCompanyLocale = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
		String strPartyId = "";
	    String strLockGenAcc = "";
	    
		hmParam = GmCommonClass.getHashMapFromForm(gmOPTTPApprovalForm);
		
		//
		strPartyId = GmCommonClass.parseNull((String) gmOPTTPApprovalForm.getSessPartyId());
    	hmAccess = GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId, "GOP_TTP_LOCKGEN")));
    	strLockGenAcc = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
    	gmOPTTPApprovalForm.setStrLockAccessFl(strLockGenAcc);
		
		
		// get Code Group Value
		gmOPTTPApprovalForm.setAlStatus(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("TTPST",getGmDataStoreVO())));
		gmOPTTPApprovalForm.setAlCategory(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("TTPCAT",getGmDataStoreVO())));
		gmOPTTPApprovalForm.setAlMonth(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("MONTH",getGmDataStoreVO())));
		gmOPTTPApprovalForm.setAlYear(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("YEAR",getGmDataStoreVO())));
		
		//fetch ttp approval details
		alLoadDtls = GmCommonClass.parseNullArrayList(gmOPTTPSetupBean.fetchTTPApprovalDtls(hmParam));
	    
	    hmReturn.put("SESSDATEFMT", getGmDataStoreVO().getCmpdfmt());
	    hmReturn.put("TEMPLATE_NM", "GmOPTTPApprovalDtls.vm");
	    hmReturn.put("LABEL_PROPERTIES_NM","properties.labels.operations.purchasing.GmOPTTPApproval");
	    // based on array list to get the Grid xml string
	    strxmlGridData = gmOPTTPSetupBean.generateOutput(alLoadDtls, hmReturn);
	    gmOPTTPApprovalForm.setStrXmlGridData(strxmlGridData);
	    
	    
	    return actionMapping.findForward("GmOPTTPApproval");
	}
	
		/**saveTTPApproval:Save Approval Details Using JMS 
		 * @param actionMapping
		 * @param form
		 * @param request
		 * @param response
		 * @return
		 * @throws AppError
		 * @throws ServletException
		 * @throws IOException
		 * @throws ParseException
		 */
		public  ActionForward  saveTTPApproval(ActionMapping actionMapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
		IOException, ParseException {

		instantiate(request, response);
		GmOPTTPApprovalForm gmOPTTPApprovalForm = (GmOPTTPApprovalForm) form;
		gmOPTTPApprovalForm.loadSessionParameters(request);
		GmOPTTPSetupBean gmOPTTPSetupBean = new GmOPTTPSetupBean(getGmDataStoreVO());
		//Initialize
		HashMap hmParam = new HashMap();
		String strTTPId = "";
		String strStatusId = "";
		String strCategoryId = "";
		String strMonth="";
		String strYear="";
		String strInvalidTTPName="";
		ArrayList alInvalidTTP = null;
		Iterator itrInvalidTTP = null;
		HashMap hmTemp = null;
		
		hmParam = GmCommonClass.getHashMapFromForm(gmOPTTPApprovalForm);
		String strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
		hmParam.put("COMPANYINFO",strCompanyInfo);
		
        // PMT-32804 : TTP Summary lock and generate
		//Get Invalid TTP Name as a List and iterate form the Invalid TTP name.
		
		alInvalidTTP = GmCommonClass.parseNullArrayList((ArrayList)gmOPTTPSetupBean.saveTTPApprovalDtls(hmParam)); 
		
		if(alInvalidTTP.size() != 0){
			itrInvalidTTP = alInvalidTTP.iterator();
			while(itrInvalidTTP.hasNext()){
				hmTemp = (HashMap) itrInvalidTTP.next();
				strInvalidTTPName = strInvalidTTPName + (String) hmTemp.get("TTP_NAME") + "<br>";
			}
			
		}
		log.debug("Errro TTP string "+strInvalidTTPName);
		gmOPTTPApprovalForm.setStrInvalidName(strInvalidTTPName);
		
		//Call ProcessTTPApprovalJMS Before Redirect to fetch TTP Approval Page
		gmOPTTPSetupBean.processTTPApprovalJMS(hmParam);
		
		
		//Redirect to fetch TTP Approval Page
		strTTPId = GmCommonClass.parseZero((String) hmParam.get("TTPID"));
		strStatusId = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
		strMonth=GmCommonClass.parseNull((String) hmParam.get("TTPMONTH"));
	    strYear=GmCommonClass.parseNull((String) hmParam.get("TTPYEAR"));		
		strCategoryId = GmCommonClass.parseNull((String) hmParam.get("CATEGORY"));
		
		
		String strForwardPage = "/gmTTPSummaryApprove.do?method=fetchTTPApproval&ttpYear="+strYear+"&ttpMonth="+strMonth+"&ttpId="+strTTPId+"&category="+strCategoryId+"&status="+strStatusId+"&strInvalidName="+strInvalidTTPName;
		return actionRedirect(strForwardPage, request);
	}
	
		
		/**loadFailedTTPInformation:Load Failed TTP Information when we click hyperlink in failed Status column
		 * @param actionMapping
		 * @param form
		 * @param request
		 * @param response
		 * @return
		 * @throws AppError
		 * @throws ServletException
		 * @throws IOException
		 * @throws ParseException
		 */
		public  ActionForward  loadFailedTTPInformation(ActionMapping actionMapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
		IOException, ParseException {
			
		instantiate(request, response);
		GmOPTTPApprovalForm gmOPTTPApprovalForm = (GmOPTTPApprovalForm) form;
		gmOPTTPApprovalForm.loadSessionParameters(request);
		GmOPTTPSetupBean gmOPTTPSetupBean = new GmOPTTPSetupBean(getGmDataStoreVO());
        //Initialize
		
		ArrayList alLoadDtls = new ArrayList();   
	    HashMap hmReturn = new HashMap();
		HashMap hmParam = new HashMap();
		

		hmParam = GmCommonClass.getHashMapFromForm(gmOPTTPApprovalForm);
		String strxmlGridData = "";
        //Fetch failed ttp information 
		alLoadDtls = GmCommonClass.parseNullArrayList(gmOPTTPSetupBean.fetchTTPFailedDtls(hmParam));
		
	    hmReturn.put("SESSDATEFMT", getGmDataStoreVO().getCmpdfmt());
	    hmReturn.put("TEMPLATE_NM", "GmOPTTPFailedDtls.vm");
	    hmReturn.put("LABEL_PROPERTIES_NM","properties.labels.operations.purchasing.GmOPTTPApproval");
	    // based on array list to get the Grid xml string
	    strxmlGridData = gmOPTTPSetupBean.generateOutput(alLoadDtls, hmReturn);
	    gmOPTTPApprovalForm.setStrXmlGridData(strxmlGridData);
	    
	    
		return actionMapping.findForward("GmOPTTPFailedReport");
	}	
		}
