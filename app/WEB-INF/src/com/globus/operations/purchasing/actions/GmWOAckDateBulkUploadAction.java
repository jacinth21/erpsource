package com.globus.operations.purchasing.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.beans.GmResourceBundleBean;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.purchasing.beans.GmWOAckDateBulkUploadBean;
import com.globus.operations.purchasing.forms.GmWOAckDateBulkUploadForm;
import com.globus.operations.beans.GmPurchaseBean;

/**
 * This class is used to show screen and update ack date for work order
 * @author asingaravel
 *
 */
public class GmWOAckDateBulkUploadAction extends GmDispatchAction
{
	 Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
       

	    /**Description:This Method will Load WO Ack date bulk upload screen
	     * @param mapping
	     * @param form
	     * @param request
	     * @param response
	     * @return
	     * @throws Exception
	     */
		public ActionForward loadWOAckDateUpload(ActionMapping mapping, ActionForm form, HttpServletRequest request,
				HttpServletResponse response) throws Exception {

			instantiate(request, response);
			GmWOAckDateBulkUploadForm gmWOAckDateBulkUploadForm = (GmWOAckDateBulkUploadForm) form;
			gmWOAckDateBulkUploadForm.loadSessionParameters(request);

			return mapping.findForward("GmWOAckDateBulkUpload");
		}
		 /**updateWOAckDateUpload: method used to update Ack date for the work order
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return
		 * @throws AppError
		 * @throws Exception
		 */
		public ActionForward updateWOAckDateUpload(ActionMapping mapping,
						ActionForm form, HttpServletRequest request,
						HttpServletResponse response) throws AppError, Exception {

			instantiate(request, response);
			GmWOAckDateBulkUploadForm gmWOAckDateBulkUploadForm = (GmWOAckDateBulkUploadForm) form;
			gmWOAckDateBulkUploadForm.loadSessionParameters(request);
					
			GmWOAckDateBulkUploadBean gmWOAckDateBulkUploadBean = new GmWOAckDateBulkUploadBean(getGmDataStoreVO());
					
			HashMap hmParam = new HashMap();
			String strJsonString = "";
			String strCompanyInfo = "";
			hmParam = GmCommonClass.getHashMapFromForm(gmWOAckDateBulkUploadForm);
			
			strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
			hmParam.put("COMPINFO", strCompanyInfo);
					
			//Get Report details using json string
			strJsonString = gmWOAckDateBulkUploadBean.updateWOAckDateUpload(hmParam);
			
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			if (!strJsonString.equals("")) {
				pw.write(strJsonString);
				pw.flush();	
			}	
		  	       	
			return null;	
		}
}