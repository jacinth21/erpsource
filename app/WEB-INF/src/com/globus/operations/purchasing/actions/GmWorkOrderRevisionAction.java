package com.globus.operations.purchasing.actions;

import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.operations.purchasing.beans.GmWorkOrderRevisionBean;
import com.globus.operations.purchasing.forms.GmWorkOrderRevisionForm;
import com.globus.operations.purchasing.forms.GmWorkOrderVendorCommitForm;
import com.globus.jms.consumers.beans.GmInitiateVendorJMS;

public class GmWorkOrderRevisionAction extends GmDispatchAction {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/** loadWORevisionDashboard -  This method used to load the Work order revision dashboard 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward loadWORevisionDashboard(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmWorkOrderRevisionForm gmWorkOrderRevisionForm = (GmWorkOrderRevisionForm) form;
		//get the value from code group and set into Action drop down
		gmWorkOrderRevisionForm.setAlWoAction(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("WORACT",getGmDataStoreVO())));
		HashMap hmParam = new HashMap();
		String strApplnDateFmt = "";
		String strOpt ="";
		String strDispatchTo ="";
		gmWorkOrderRevisionForm.loadSessionParameters(request);
		hmParam = GmCommonClass.getHashMapFromForm(gmWorkOrderRevisionForm);
		strOpt = GmCommonClass.parseNull(gmWorkOrderRevisionForm.getStrOpt());
	
		// Getting the current date from GmCalenderOperation.
		strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
		GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
		String todayDate = GmCalenderOperations.getCurrentDate(strApplnDateFmt);
		int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
		String firstDayofMonth = GmCalenderOperations.getAnyDateOfMonth(currMonth, 1, strApplnDateFmt);
		gmWorkOrderRevisionForm.setDtWOFromDate(GmCalenderOperations.getDate(firstDayofMonth, strApplnDateFmt));
		gmWorkOrderRevisionForm.setDtWOToDate(GmCalenderOperations.getDate(todayDate, strApplnDateFmt));
        if(strOpt.equals("WODASHBOARD")){
        	 strDispatchTo="GmWORevisionDash";
        }
        if(strOpt.equals("WOREPORT")){
        	 strDispatchTo="GmWORevisionRpt";
        }
        return mapping.findForward(strDispatchTo);
	}

	/** reloadWORevisionRpt - This method used to load the WO revision (updated/ignored) report 
	 *  
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return 
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward reloadWORevisionRpt(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmWorkOrderRevisionForm gmWorkOrderRevisionForm = (GmWorkOrderRevisionForm) form;
		GmWorkOrderRevisionBean gmWorkOrderRevisionBean = new GmWorkOrderRevisionBean(
				getGmDataStoreVO());
		HashMap hmParam = new HashMap();
		String strJsonWORevString = "";
		gmWorkOrderRevisionForm.loadSessionParameters(request);
		hmParam = GmCommonClass.getHashMapFromForm(gmWorkOrderRevisionForm);


		HashMap hmRegStr = new HashMap();
		hmRegStr.put("PARTNUM", gmWorkOrderRevisionForm.getPnum());
		hmRegStr.put("SEARCH", gmWorkOrderRevisionForm.getPnumSuffix());
		String strPartNumFormat = GmCommonClass.createRegExpString(hmRegStr);

		hmParam.put("PART", strPartNumFormat);
		hmParam.put("DATEFORMAT", getGmDataStoreVO().getCmpdfmt());
		strJsonWORevString = gmWorkOrderRevisionBean.loadWORevisionRpt(hmParam);
		gmWorkOrderRevisionForm.setWoJsonString(strJsonWORevString);
		
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJsonWORevString.equals("")) {
			pw.write(strJsonWORevString);
		}
		pw.flush();
		return null;
	}

	/** saveWORevision - This method used to update the WO revision from Old to new revision. 
	 *   --Change the The revision status from open to inprogress 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward saveWORevision(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)
			throws AppError, Exception {

		instantiate(request, response);
		GmWorkOrderRevisionForm gmWorkOrderRevisionForm = (GmWorkOrderRevisionForm) form;
		GmWorkOrderRevisionBean gmWORevisionBean = new GmWorkOrderRevisionBean(getGmDataStoreVO());
		GmInitiateVendorJMS gmInitiateVendorJMS = new GmInitiateVendorJMS(getGmDataStoreVO());
		gmWorkOrderRevisionForm.loadSessionParameters(request);
		HashMap hmParam = new HashMap();
		HashMap hmJMSParam = new HashMap();
		HashMap hmRegStr = new HashMap();
		HashMap hmEmailParam = new HashMap();
		String strApplnDateFmt = "";
		String strProjectId ="";
		String strPoid = "";
		String strWOAction = "";
		String strStatus = "";
		Date dtWOFromDT =null;
		Date dtWOToDT = null;
		String strWOFromDt = "";
		String strWOToDt ="";
		String strQueueName ="";
		String strConsumerClass="";
		String strComapanyInfo="";
		
		hmParam = GmCommonClass.getHashMapFromForm(gmWorkOrderRevisionForm);
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		hmRegStr.put("PARTNUM", gmWorkOrderRevisionForm.getPnum());
		hmRegStr.put("SEARCH", gmWorkOrderRevisionForm.getPnumSuffix());
		String strPartNum = GmCommonClass.createRegExpString(hmRegStr);
		

		String strWorkOrderId = gmWORevisionBean.updateWORevision(hmParam);
		
		String strWOId = GmCommonClass.parseNull((String) hmParam.get("WORKORDERID"));
		strComapanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		
  	    	hmEmailParam.put("REFID", strWOId);
         	hmEmailParam.put("REFTYPE", "108441");//Work Order Type
         	hmEmailParam.put("REFACTION", "26240620");//Work Order Action
         	hmEmailParam.put("MAILSTATUS", "0"); //mail status for send now
         	hmEmailParam.put("USERID", strUserId);
         	hmEmailParam.put("COMPANYINFO", GmCommonClass.parseNull(request.getParameter("companyInfo")));
         	gmInitiateVendorJMS.initVendorEmailJMS(hmEmailParam);   //jms call for WO REV update
         	 log.debug("hmReturn-=================== "+hmEmailParam);
      
        
		gmWorkOrderRevisionForm.setWorkOrderId(strWorkOrderId);
		GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
		
		// Called JMS For update the status from Inprogress to  closed or failed Status. 
		strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_TTP_SUMMARY_QUEUE"));
		hmJMSParam.put("QUEUE_NAME", strQueueName);
		strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("WO_REV_CONSUMER_CLASS"));
		hmJMSParam.put("CONSUMERCLASS", strConsumerClass);
		hmJMSParam.put("STATUS", "108243");
		hmJMSParam.put("COMPANYINFO",GmCommonClass.parseNull(request.getParameter("companyInfo")));
		hmJMSParam.put("USERID", strUserId);
		hmJMSParam.put("ACTION", "REV_UPDATE");
		gmConsumerUtil.sendMessage(hmJMSParam);

		 strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();

		 strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
		 strPoid = GmCommonClass.parseNull((String) hmParam.get("POID"));
		 strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
		 
       // After saved  - redirect to Work order Revision Dashboard reloadWORevisionDashboard)
		String strRedirectURL = "/gmWoRevision.do?method=reloadWORevisionRpt&projectId="
				+ strProjectId
				+ "&pnum="
				+ strPartNum
				+ "&poId="
				+ strPoid
				+ "&status=108242";
		return actionRedirect(strRedirectURL, request);

	}

}
