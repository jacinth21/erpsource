package com.globus.operations.purchasing.actions;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.PrintWriter;
import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.actions.GmAction;
import com.globus.corporate.beans.GmDivisionBean;
//Screen Forms and beans
import com.globus.operations.purchasing.beans.GmPurchasingClassificationBean;
import com.globus.operations.purchasing.forms.GmPurchasingClassificationForm;



/**
 * @author pvigneshwaran
 *
 */
/**GmPurchasingClassificationReportAction: This class  used to show  the partnumber classification details
 * @param 
 * @return
 * @throws 
 */
public class GmPurchasingClassificationReportAction extends GmDispatchAction {
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
	
	/**loadPartClassificationRpt:This method used to load Report when left link is clicked
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward loadPartClassificationRpt(ActionMapping actionMapping, ActionForm form,
		    HttpServletRequest request,HttpServletResponse response) throws AppError, Exception {

				instantiate(request, response);
				GmPurchasingClassificationForm gmPurchaseClassificationForm = (GmPurchasingClassificationForm) form;
				gmPurchaseClassificationForm.loadSessionParameters(request);
				GmPurchasingClassificationBean gmPurchaseClassificationBean = new GmPurchasingClassificationBean(getGmDataStoreVO());
			    GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());
				//Initialize
				HashMap hmParam = new HashMap();
				// get Code Group Value
			    gmPurchaseClassificationForm.setAlDivision(GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmParam)));
				gmPurchaseClassificationForm.setAlClassiffication(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PURCLA",getGmDataStoreVO())));
				gmPurchaseClassificationForm.setAlMonth(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("MONTH",getGmDataStoreVO())));
				gmPurchaseClassificationForm.setAlYear(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("YEAR",getGmDataStoreVO())));

				return actionMapping.findForward("GmPurchasingClassificationReport");
			}
	
	
	
		/**fetchPartClassificationRpt:This method used to fetch part classification Report Details
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return
		 * @throws AppError
		 * @throws Exception
		 */
		public ActionForward fetchPartClassificationRpt(ActionMapping mapping,
						ActionForm form, HttpServletRequest request,
						HttpServletResponse response) throws AppError, Exception {
			try{
					//Initialize
					instantiate(request, response);
					GmPurchasingClassificationForm gmPurchaseClassificationForm = (GmPurchasingClassificationForm) form;
					gmPurchaseClassificationForm.loadSessionParameters(request);
					GmPurchasingClassificationBean gmPurchaseClassificationBean = new GmPurchasingClassificationBean(getGmDataStoreVO());
					
					HashMap hmParam = new HashMap();
					String strJsonRptString = "";
					hmParam = GmCommonClass.getHashMapFromForm(gmPurchaseClassificationForm);
			
					//PartNumber Search Using lietral,Like-Prefix,Like_suffix
					HashMap hmRegStr = new HashMap();
				    hmRegStr.put("PARTNUM", gmPurchaseClassificationForm.getPnum());
					hmRegStr.put("SEARCH", gmPurchaseClassificationForm.getPnumSuffix());
					String strPartNumFormat = GmCommonClass.createRegExpString(hmRegStr);
					hmParam.put("PART", strPartNumFormat);
					
					//Get Report details using json string
					strJsonRptString = gmPurchaseClassificationBean.fetchPartClassificationRpt(hmParam);
				
					response.setContentType("text/json");
					PrintWriter pw = response.getWriter();
					if (!strJsonRptString.equals("")) 
						{
						pw.write(strJsonRptString);
						pw.flush();	
						}	
		  	       }catch(Exception e){
				     response.setStatus(300);
			       }			
			return null;	
			}
	
	
}
