package com.globus.operations.purchasing.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.beans.GmResourceBundleBean;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.purchasing.beans.GmWODueDateBulkUploadBean;
import com.globus.operations.purchasing.forms.GmWODueDateBulkUploadForm;
import com.globus.operations.beans.GmPurchaseBean;

/**
 * This class is used to show screen and update date for Bulk due date upload
 * @author asingaravel
 *
 */
public class GmWODueDateBulkUploadAction extends GmDispatchAction
{
	 Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
       

	    /**Description:This Method will Load WO Due date bulk upload screen
	     * @param mapping
	     * @param form
	     * @param request
	     * @param response
	     * @return
	     * @throws Exception
	     */
		public ActionForward loadWODueDateUpload(ActionMapping mapping, ActionForm form, HttpServletRequest request,
				HttpServletResponse response) throws Exception {

			instantiate(request, response);
			GmWODueDateBulkUploadForm gmWODueDateBulkUploadForm = (GmWODueDateBulkUploadForm) form;
			gmWODueDateBulkUploadForm.loadSessionParameters(request);

			return mapping.findForward("GmWODueDateBulkUpload");
		}
		 /**updateWODueDateUpload: method used to update due date for the work order
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return
		 * @throws AppError
		 * @throws Exception
		 */
		public ActionForward updateWODueDateUpload(ActionMapping mapping,
						ActionForm form, HttpServletRequest request,
						HttpServletResponse response) throws AppError, Exception {

			instantiate(request, response);
			GmWODueDateBulkUploadForm gmWODueDateBulkUploadForm = (GmWODueDateBulkUploadForm) form;
			gmWODueDateBulkUploadForm.loadSessionParameters(request);
					
			GmWODueDateBulkUploadBean gmWODueDateBulkUploadBean = new GmWODueDateBulkUploadBean(getGmDataStoreVO());
					
			HashMap hmParam = new HashMap();
			String strJsonString = "";
			String strCompanyInfo = "";
			hmParam = GmCommonClass.getHashMapFromForm(gmWODueDateBulkUploadForm);
			
			strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
			hmParam.put("COMPINFO", strCompanyInfo);
					
			//Get Report details using json string
			strJsonString = gmWODueDateBulkUploadBean.updateWODueDateUpload(hmParam);
			
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			if (!strJsonString.equals("")) {
				pw.write(strJsonString);
				pw.flush();	
			}	
		  	       	
			return null;	
		}
		 /**fetchWOPrintDetails: method used to fetch print WO details when print button clicked
			 * @param mapping
			 * @param form
			 * @param request
			 * @param response
			 * @return
			 * @throws AppError
			 * @throws Exception
			 */
		public ActionForward fetchWOPrintDetails(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
				HttpServletResponse response) throws Exception {

			instantiate(request, response);
			GmWODueDateBulkUploadForm gmWODueDateBulkUploadForm = (GmWODueDateBulkUploadForm) actionForm;
			gmWODueDateBulkUploadForm.loadSessionParameters(request);
			
			String strForward = "";
			String strWOIDs = "";

			HashMap hmResult = new HashMap();
			HashMap hmReturn = new HashMap();
			ArrayList alReturn = new ArrayList();
		
			GmPurchaseBean gmPurc = new GmPurchaseBean(getGmDataStoreVO());
			GmWODueDateBulkUploadBean gmWODueDateBulkUploadBean = new GmWODueDateBulkUploadBean(getGmDataStoreVO());

			String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
			
			GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Paperwork",strCompanyLocale);
			strWOIDs = GmCommonClass.parseNull((String) gmWODueDateBulkUploadForm.getWoStr());
			
			alReturn = gmWODueDateBulkUploadBean.fetchWOPrintDetails(strWOIDs);
			
			hmReturn.put("SUBREPORT_DIR",
					session.getServletContext().getRealPath(GmCommonClass.getString("GMJASPERLOCATION")) + "\\");
			hmReturn.put("ALLWO", alReturn);
			HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(getGmDataStoreVO().getCmpid(), "");
			hmReturn.put("LOGO", GmCommonClass.parseNull((String) hmCompanyAddress.get("COMP_LOGO")));
			hmReturn.put("gmResourceBundleBean", gmResourceBundleBean);
			request.setAttribute("hmReturn", hmReturn);
			
			String strRptName =
		              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("WO.JASPERNM"));
		    request.setAttribute("STRRPTNAME", strRptName);
			strForward = "GmWOPrintAll";

			return actionMapping.findForward(strForward);
		}
}