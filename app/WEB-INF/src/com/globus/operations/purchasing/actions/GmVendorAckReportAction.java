package com.globus.operations.purchasing.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.logon.beans.GmLogonBean;
import com.globus.common.actions.GmDispatchAction;
import com.globus.operations.purchasing.forms.GmVendorAckReportForm;
import com.globus.operations.purchasing.forms.GmWorkOrderVendorCommitForm;
import com.globus.operations.purchasing.beans.GmVendorAckReportBean;
import com.globus.common.beans.GmSearchCriteria;

/** This class is used to show vendor acknowledge report screen
 * @author asingaravel
 *
 */
public class GmVendorAckReportAction extends GmDispatchAction {
	 Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
     
	    /**Description:This Method will Load vendor acknowledge report screen
	     * @param mapping
	     * @param form
	     * @param request
	     * @param response
	     * @return
	     * @throws Exception
	     */
		public ActionForward loadVendorAckReport(ActionMapping mapping, ActionForm form, HttpServletRequest request,
				HttpServletResponse response) throws Exception {

			instantiate(request, response);
			GmVendorAckReportForm gmVendorAckReportForm = (GmVendorAckReportForm) form;
			gmVendorAckReportForm.loadSessionParameters(request);
			GmLogonBean gmLogonBean = new GmLogonBean();
			
			ArrayList alPartSearch = new ArrayList();
			ArrayList alOperators = new ArrayList();
			ArrayList alPuchaseAgentName = new ArrayList();
			
			alPartSearch = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LIKES"));
			alOperators = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("OPERS"));
			alPuchaseAgentName = GmCommonClass.parseNullArrayList(gmLogonBean.getEmployeeList("300", "W"));
			gmVendorAckReportForm.setAlPartSearch(alPartSearch);
			gmVendorAckReportForm.setAlDueDays(alOperators);
			gmVendorAckReportForm.setAlPuchaseAgentName(alPuchaseAgentName);
			
			return mapping.findForward("GmVendorAckReport");
		}
		
		 /**Description:This Method will fetch and show data for vendor acknowledge report screen
	     * @param mapping
	     * @param form
	     * @param request
	     * @param response
	     * @return
	     * @throws Exception
	     */
		public ActionForward fetchVendorWOAckDtls(ActionMapping mapping, ActionForm form, HttpServletRequest request,
				HttpServletResponse response) throws Exception {

			instantiate(request, response);
			GmVendorAckReportForm gmVendorAckReportForm = (GmVendorAckReportForm) form;
			gmVendorAckReportForm.loadSessionParameters(request);
			
			GmVendorAckReportBean gmVendorAckReportBean = new GmVendorAckReportBean(getGmDataStoreVO());
			GmWSUtil gmWSUtil = new GmWSUtil();
			
			HashMap hmParam = new HashMap();
			HashMap hmRegStr = new HashMap();
			ArrayList alList = new ArrayList();
			String strJsonRptString = "";
			String strPartNumFormat = "";
			hmParam = GmCommonClass.getHashMapFromForm(gmVendorAckReportForm);
			
			//PartNumber Search Using lietral,Like-Prefix,Like_suffix					
			hmRegStr.put("PARTNUM", gmVendorAckReportForm.getPartNums());
			hmRegStr.put("SEARCH", gmVendorAckReportForm.getLikeSearch());
			strPartNumFormat = GmCommonClass.createRegExpString(hmRegStr);
			hmParam.put("PART", strPartNumFormat);
					
			//Get Report details using json string
			alList = gmVendorAckReportBean.fetchVendorWOAckDtls(hmParam);
			
			if(alList.size()>0) {
					strJsonRptString = gmWSUtil.parseArrayListToJson(alList);
			}
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			if (!strJsonRptString.equals("")) {
				pw.write(strJsonRptString);
				pw.flush();	
			}	
		  	       	
			return null;	
		}
	    /**
		 * @param alResult
		 * @param gmVendorAckReportForm
		 * @return
		 * @throws AppError
		 */
//	    public ArrayList applySearchCriteria(ArrayList alResult,GmVendorAckReportForm gmVendorAckReportForm) throws AppError {
//			GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
//			String strDueDaysRange = GmCommonClass.parseNull(gmVendorAckReportForm.getDueDays());
//			String strNoOfDays = GmCommonClass.parseNull(gmVendorAckReportForm.getNoOfDays());
//			if (!strNoOfDays.equals("")) {
//				gmSearchCriteria.addSearchCriteria("ACKDUEDAY", strNoOfDays,strDueDaysRange);
//			}
//			gmSearchCriteria.query(alResult);
//			return alResult;
//		}
}
