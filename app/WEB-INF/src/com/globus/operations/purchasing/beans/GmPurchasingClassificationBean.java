package com.globus.operations.purchasing.beans;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;




import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.common.beans.GmLogger; 
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.util.GmTemplateUtil;

import org.apache.log4j.Logger;




/**
 * @author pvigneshwaran
 *
 */
/**GmPurchasingClassificationBean: This class  used to show  the partnumber classification details
 * @param 
 * @return
 * @throws 
 */
public class GmPurchasingClassificationBean extends GmBean{

	public GmPurchasingClassificationBean(GmDataStoreVO gmDataStoreVO)
			throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
	
	/**fetchPartClassificationRpt:This method used fetch the partnumber classification details
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String fetchPartClassificationRpt(HashMap hmParam) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		
		
		//Initialize
		String strJsonRptString = "";
		String strDate = GmCommonClass.parseNull((String) hmParam.get("ABCMONTH")) + "/" + GmCommonClass.parseNull((String) hmParam.get("ABCYEAR"));
		
		
		gmDBManager.setPrepareString("gm_pkg_oppr_pur_part_classification.gm_fch_part_classification_rpt", 6);
		gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
		gmDBManager.setString(1,  GmCommonClass.parseNull((String) hmParam.get("PART")));
		gmDBManager.setString(2,  GmCommonClass.parseNull((String) hmParam.get("PROJECTID")));
		gmDBManager.setString(3,  GmCommonClass.parseZero((String) hmParam.get("DIVISIONID")));
		gmDBManager.setString(4,  GmCommonClass.parseZero((String) hmParam.get("RANKID")));
		gmDBManager.setString(5,  strDate);
		gmDBManager.execute();
		//Json String
		strJsonRptString = GmCommonClass.parseNull(gmDBManager.getString(6));
		
		gmDBManager.close();

		return strJsonRptString;

	}
	
}
