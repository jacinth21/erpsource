package com.globus.operations.purchasing.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.operations.beans.GmVendorEmailInterface;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.util.GmTemplateUtil;

/**GmDHREmailBean:This bean class used to send email details for DHR
 * @author pvigneshwaran
 *
 */
public class GmDHREmailBean extends GmBean implements GmVendorEmailInterface {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	GmCommonClass gmCommon = new GmCommonClass();
	/**
	 * @param gmDataStoreVO
	 * @throws AppError
	 */
	public GmDHREmailBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
	 /**
	 * 
	 */
	public GmDHREmailBean() {
		    super(GmCommonClass.getDefaultGmDataStoreVO());
    }
	
	/* (non-Javadoc)
	 * @see com.globus.operations.beans.GmVendorEmailInterface#saveVendorEmail(java.util.HashMap)
	 */
	public void saveVendorEmail(HashMap hmParam) throws AppError {
		// TODO Auto-generated method stub
		GmDBManager gmDBManger = new GmDBManager(getGmDataStoreVO());

		String strDHRId = GmCommonClass.parseNull((String) hmParam.get("REFID"));
		String strRefAct = GmCommonClass.parseNull((String) hmParam.get("REFACTION"));
		String strRefType = GmCommonClass.parseNull((String) hmParam.get("REFTYPE"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		
		gmDBManger.setPrepareString("gm_pkg_vendor_mail_txn.gm_sav_dhr_email_dtls", 4);
		
		gmDBManger.setString(1, strDHRId);
		gmDBManger.setString(2, strRefType);
		gmDBManger.setString(3, strRefAct);
		gmDBManger.setString(4, strUserId);
		gmDBManger.execute();
		gmDBManger.commit();
	}
	/* (non-Javadoc)
	 * @see com.globus.operations.beans.GmVendorEmailInterface#fetchEmailDetail(java.util.HashMap)
	 */
	public ArrayList fetchEmailDetail(HashMap hmParam) throws AppError {
		// TODO Auto-generated method stub
		 	ArrayList alReturn = new ArrayList();
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    String strSessCompanyLocale = GmCommonClass.parseNull(GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid()));

		    
			String strMailStatus = GmCommonClass.parseNull((String) hmParam.get("MAILSTATUS"));   
			String strRefaction = GmCommonClass.parseNull((String) hmParam.get("REFACTION"));
			
		    gmDBManager.setPrepareString("gm_pkg_vendor_mail_fch.gm_fch_dhr_email_dtl", 3);
		    gmDBManager.setString(1, strMailStatus);
		    gmDBManager.setString(2, strRefaction);
		    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		    gmDBManager.execute();
		    alReturn =GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3)));
		    gmDBManager.close();
		    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
		    return alReturn;
		
	
	}	
	/**getDHRClosedCount:This method used to get closed DHR count
	 * @param strWOId
	 * @return
	 * @throws AppError
	 */
	public int getDHRClosedCount(String strWOId) throws AppError{
	    int strCount;
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());	
	    gmDBManager.setPrepareString("gm_pkg_vendor_mail_fch.gm_fch_po_closed_count", 2);
	    gmDBManager.setString(1,  GmCommonClass.parseNull(strWOId));
	    gmDBManager.registerOutParameter(2, OracleTypes.NUMBER);
	    gmDBManager.execute();
	    strCount = gmDBManager.getInt(2);
	    gmDBManager.close();
		return strCount;
	}
	
	  /**
	   * sendNCMRCreationMail - This method used to send mail while creating NCMR
	   * 
	   * @param HashMap 
	   * @return String
	   * @exception AppError
	   **/
	  public void sendNCMRCreationMail(String strNCMRId,HashMap hmReturn) throws AppError {
		  log.debug("Inside sendNCMRCreationMail");
		  String strEmail="";
		  ArrayList alNCMRDetails = new ArrayList();
		  HashMap hmNCMRDtls = new HashMap();
		  hmNCMRDtls = (HashMap) hmReturn.get("NCMRDETAILS");
		  log.debug("hmNCMRDtls sendNCMRCreationMail"+hmNCMRDtls);
		  alNCMRDetails.add(hmNCMRDtls);
		  log.debug("alNCMRDetails>//////"+alNCMRDetails.size());
		  log.debug("alNCMRDetails>//////"+alNCMRDetails);
		  String TEMPLATE_NAME = "GmNCMRCreate";
		  GmEmailProperties gmEmailProperties = getEmailProperties(TEMPLATE_NAME);
		  strEmail = fetchvendormail(strNCMRId, hmReturn);
		  if (!strEmail.equals("")) {
		  gmEmailProperties.setRecipients(strEmail);
		  
		  GmJasperMail jasperMail = new GmJasperMail();
		  jasperMail.setJasperReportName("/GmNCMREmailDetails.jasper");
	      jasperMail.setAdditionalParams(hmNCMRDtls);
		  jasperMail.setReportData(alNCMRDetails);
		  jasperMail.setEmailProperties(gmEmailProperties);
		  hmReturn = jasperMail.sendMail();
		  log.debug("hmReturnmail"+hmReturn);
		  }
	      
	  }  
	  
	  /**
	   * fetchvendormail - This method used to fetch vendor mail details
	   * 
	   * @param HashMap 
	   * @return String
	   * @exception AppError
	   **/
	  public String fetchvendormail(String strNCMRId,HashMap hmReturn) throws AppError {
		  log.debug("Inside fetchvendormail");
		  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		  HashMap hmNCMRDtls = (HashMap) hmReturn.get("NCMRDETAILS");
		  log.debug("hmNCMRDtls fetchvendormail"+hmNCMRDtls);
		  String strVendorId = GmCommonClass.parseNull((String)hmNCMRDtls.get("VID"));
		  log.debug("strVendorId fetchvendormail"+strVendorId);
		  gmDBManager.setFunctionString("gm_pkg_vendor_mail_fch.GET_VENDOR_MAIL_ID", 2);
		  gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		  gmDBManager.setString(2, strVendorId);
		  gmDBManager.setInt(3, Integer.parseInt("26240816")); //PO Rejected qty Email type
		  gmDBManager.execute();
		  String strEmail = GmCommonClass.parseNull(gmDBManager.getString(1));
		  log.debug("strEmail--------"+strEmail);
		  gmDBManager.close();
		  return strEmail;
	  }
		  
	  /**
	   * getEmailProperties - This method used to get Emailproperties
	   * 
	   * @return String
	   **/  
	 private GmEmailProperties getEmailProperties(String strTemplate) {
		 log.debug("strTemplate"+strTemplate);
		GmEmailProperties emailProps = new GmEmailProperties();
		emailProps.setSender(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.FROM));
		
		emailProps.setSubject(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.SUBJECT));
		emailProps.setMimeType(GmCommonClass.getEmailProperty(strTemplate + "." +     GmEmailProperties.MIME_TYPE));
		emailProps.setCc(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.CC));
		emailProps.setEmailHeaderName(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.EMAIL_HEADER_NM));
		return emailProps;
	 
	}
	
}
