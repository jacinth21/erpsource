package com.globus.operations.purchasing.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmVendorPOReportBean extends GmBean {


	  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
	                                                               // Class.

	  GmCommonClass gmCommon = new GmCommonClass();

	  public GmVendorPOReportBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	  public GmVendorPOReportBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	  
	  /**
	   * publishBlkPO - This method will publish vendor PO report
	   * 
	   * @param String strPOId, String strPassword, String strUserID
	   * @return HashMap
	   * @exception AppError
	   **/
	  public HashMap publishBlkPO(String strPOId, String strPublishFL, String strUserID) throws AppError {
		HashMap hmReturn = new HashMap();
	    String strMsg = "";
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());	

	    gmDBManager.setPrepareString("GM_PKG_VENDOR_PO_REPORT.gm_sav_blk_publish_po", 4);

	    /*
	     * register out parameter and set input parameters
	     */
	    gmDBManager.registerOutParameter(4, java.sql.Types.CHAR);

	    gmDBManager.setString(1, strPOId);
	    gmDBManager.setString(2, strUserID);
	    gmDBManager.setString(3, strPublishFL);

	    gmDBManager.execute();
	    strMsg = gmDBManager.getString(4);
	    gmDBManager.commit();

	    hmReturn.put("MSG", strMsg);


	    return hmReturn;
	  } // end of updatePO
	  
	  /**
	   * getVendorPOList - This method will fetch PO reports has vendor portal accesfl 
	   * 
	   * @param hmParam
	   * @return HashMap
	   * @exception AppError
	   **/
	  public HashMap getVendorPOList(HashMap hmParam) throws AppError {
	    HashMap hmReturn = new HashMap();
	    ArrayList alResult = new ArrayList();
	    StringBuffer sbQuery = new StringBuffer();
	    String strCompDFmt = getCompDateFmt();

	    String strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDID"));
	    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FRMDT"));
	    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
	    String strId = GmCommonClass.parseNull((String) hmParam.get("POID"));
	    String strPOType = GmCommonClass.parseNull((String) hmParam.get("POTYPE"));
	    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	    String strAmount = GmCommonClass.parseNull((String) hmParam.get("POAMT"));
	    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
	    String strCheckVPAccessFlag = GmCommonClass.parseNull((String) hmParam.get("CHECKVPACCESSFL"));
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    sbQuery.append(" SELECT C401_PURCHASE_ORD_ID ID, C401_PURCHASE_ORD_DATE PODATE,");
	    sbQuery.append(" GET_CODE_NAME(GET_VENDOR_CURRENCY(C301_VENDOR_ID)) VENDCURR, ");
	    sbQuery.append(" C401_PO_TOTAL_AMOUNT TOTAL, C401_DATE_REQUIRED REQDATE, ");
	    sbQuery
	        .append(" GET_USER_NAME(C401_CREATED_BY) UNAME, GET_VENDOR_NAME(C301_VENDOR_ID) VENDNM ");
	    sbQuery
	        .append(" ,GET_LOG_FLAG(C401_PURCHASE_ORD_ID, 1203) CALL_FLAG , get_code_name(C401_TYPE) POTYPENM, get_latest_log_comments(C401_PURCHASE_ORD_ID,1203) LASTCOMMENTS ");
	    sbQuery.append(" ,GM_PKG_VENDOR_PO_REPORT.GET_VENDOR_PORTAL_ACCESS_FL(C301_VENDOR_ID) VENDPAFL ,C401_PUBLISH_FL PUBFL, C901_STATUS STATUS, get_code_name(C901_STATUS) STATUSNM ");
	    sbQuery.append(" FROM T401_PURCHASE_ORDER ");
	    sbQuery.append(" WHERE C401_VOID_FL IS NULL ");
	    sbQuery.append(" AND C1900_COMPANY_ID =" + getCompId());

	    if (!strFromDt.equals("") && !strToDt.equals("")) {
	      sbQuery.append(" AND C401_PURCHASE_ORD_DATE BETWEEN");
	      sbQuery.append(" to_date('");
	      sbQuery.append(strFromDt);
	      sbQuery.append("','" + strCompDFmt + "') AND to_date('");
	      sbQuery.append(strToDt);
	      sbQuery.append("','" + strCompDFmt + "')");
	    }
	    if (!strVendorId.equals("") && !strVendorId.equals("0")) {
	      sbQuery.append(" AND C301_VENDOR_ID = ");
	      sbQuery.append(strVendorId);
	    }
	    if (!strPOType.equals("")) {
	      sbQuery.append(" AND C401_TYPE = '");
	      sbQuery.append(strPOType);
	      sbQuery.append("'");
	    }

	    if (!strId.equals("")) {
	      sbQuery.append(" AND C401_PURCHASE_ORD_ID like '%");
	      sbQuery.append(strId);
	      sbQuery.append("'");
	    }
	    if (!strUserId.equals("0") && !strUserId.equals("")) {
	      sbQuery.append(" AND C401_CREATED_BY = '");
	      sbQuery.append(strUserId);
	      sbQuery.append("'");
	    }
	    if (!strStatus.equals("0") && !strStatus.equals("")) {
	        sbQuery.append(" AND C901_STATUS = '");
	        sbQuery.append(strStatus);
	        sbQuery.append("'");
	      }
	    if (!strAmount.equals("")) {
	      sbQuery.append(" AND C401_PO_TOTAL_AMOUNT >= ");
	      sbQuery.append(strAmount);
	    }
	    if(strCheckVPAccessFlag.equals("Y")){    	
	    	sbQuery.append(" AND C301_VENDOR_ID IN (SELECT C301_VENDOR_ID FROM  T301_VENDOR WHERE C301_VENDOR_PORTAL_FL = '"+strCheckVPAccessFlag+"'  ");
	    	sbQuery.append(" AND C301_ACTIVE_FL IS NULL ) ");
	    }
	    sbQuery.append(" ORDER BY C401_PURCHASE_ORD_ID DESC");
	    log.debug("QUERY:" + sbQuery.toString());
	    alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
	    hmReturn.put("POLIST", alResult);
	    log.debug("alResult>>>>>>:" + alResult.size());
	    return hmReturn;
	  } // End of getVendorPOList
	  
	  
	  /**
	   * getVDXmlGridData - This method is used to show the PO reports For Vendors.
	   * 
	   * @parameter HashMap
	   * @return String
	   * @exception AppError
	   **/
	  public String getVDXmlGridData(HashMap hmParam) throws AppError {
		    GmTemplateUtil templateUtil = new GmTemplateUtil();
		    String strSessCompanyLocale =
		        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
		    templateUtil.setDataList("alReturn", (ArrayList) hmParam.get("REPORT"));
		    templateUtil.setDataMap("hmParam", hmParam);
		    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
		        "properties.labels.operations.GmPOReport", strSessCompanyLocale));
		    templateUtil.setTemplateName("GmVendorPOReport.vm");
		    templateUtil.setTemplateSubDir("operations/templates");
		    log.debug("::AlReturn List::" + templateUtil.getDataList("alReturn"));
		    return templateUtil.generateOutput();
		  }
}
