package com.globus.operations.purchasing.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

import oracle.jdbc.driver.OracleTypes;

/** This bean class is used to fetch report data for vendor acknowledge report
 * @author asingaravel
 *
 */
public class GmVendorAckReportBean extends GmBean {

	public GmVendorAckReportBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	
	/**Description:This Method used to fetch Vendor Acknowledge Report Details
	   * @param hmParam
	   * @return
	   * @throws AppError
	   */
	  public ArrayList fetchVendorWOAckDtls(HashMap hmParam) throws AppError {
	  		ArrayList alReturn = new ArrayList();
	  		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	  		ArrayList alList = new ArrayList();

	  		String strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
	  		String strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
	  		String strClosedStatusFl = GmCommonClass.parseNull((String) hmParam.get("SHOWCLOSEDWOFL"));
	  		String strCommitFrmDt = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
	  		String strCommitToDt = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
	  		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PART"));
	  		String strPurAgent = GmCommonClass.parseZero((String) hmParam.get("STRPURAGENTNAME"));
	  		
	  		gmDBManager.setPrepareString("gm_pkg_pur_wo_vendor_commit.gm_fch_vendor_wo_ack_report", 7);
	  		gmDBManager.setString(1, strVendorId.equals("0") ? "" : strVendorId);
	  		gmDBManager.setString(2, strProjectId.equals("0") ? "" : strProjectId);
	  		gmDBManager.setString(3, strPartNum);
	  		gmDBManager.setString(4, strCommitFrmDt);
	  		gmDBManager.setString(5, strCommitToDt);
	  		gmDBManager.setString(6, strPurAgent);
	  		gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
	  		gmDBManager.execute();

	  		alList = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(7)));
	  		gmDBManager.close();
	  		
	  		return alList;
	 }
}
