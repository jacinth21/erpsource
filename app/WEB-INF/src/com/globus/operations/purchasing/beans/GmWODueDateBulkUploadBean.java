package com.globus.operations.purchasing.beans;

import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.ArrayList;
import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.db.GmDBManager;
import com.globus.common.beans.GmLogger; 
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.jms.consumers.beans.GmInitiateVendorJMS;
import com.globus.operations.beans.GmPurchaseBean;

import org.apache.log4j.Logger;
/**
 * @author Agilan Singaravel
 *
 */
/**GmWODueDateBulkUploadBean: This class used to fetch and show the vendor wo lead time report
 * @param 
 * @return
 * @throws 
 */
public class GmWODueDateBulkUploadBean extends GmBean{

	public GmWODueDateBulkUploadBean(GmDataStoreVO gmDataStoreVO)
			throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
	
	/**fetchVendorWOLeadTImeReport:This method used fetch the lead time report data
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String fetchVendorWOLeadTImeReport(HashMap hmParam) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());	
		//Initialize
		String strJsonRptString = "";
		
		gmDBManager.setPrepareString("gm_pkg_wo_lead_time_rpt.gm_fch_vendor_wo_lead_time_rpt", 6);
		gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
		gmDBManager.setString(1,  GmCommonClass.parseNull((String) hmParam.get("VENDORID")));
		gmDBManager.setString(2,  GmCommonClass.parseNull((String) hmParam.get("PART")));
		gmDBManager.setString(3,  GmCommonClass.parseZero((String) hmParam.get("STRPURAGENT")));
		gmDBManager.setString(4,  GmCommonClass.parseZero((String) hmParam.get("STRDIVISION")));
		gmDBManager.setString(5,  GmCommonClass.parseNull((String) hmParam.get("PROJECTID")));
		gmDBManager.execute();
		//Json String
		strJsonRptString = GmCommonClass.parseNull(gmDBManager.getString(6));
		gmDBManager.close();

		return strJsonRptString;
	}
	/**updateWODueDateUpload: This method used to update work order due date
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String updateWODueDateUpload(HashMap hmParam) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());	
		//Initialize
		String strJsonString = "";
		String strJsonWoErrString = "";
		String strJsonPoDtlString = "";
		String strJsonEmailString = "";
		
		gmDBManager.setPrepareString("gm_pkg_wo_lead_time_txn.gm_upd_wo_due_date_process", 6);
		gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
		gmDBManager.registerOutParameter(5, OracleTypes.CLOB);
		gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
		gmDBManager.setString(1,  GmCommonClass.parseNull((String) hmParam.get("WOSTR")));
		//In INPUTSTR string contains WO id,due date like GM-WO-2027192,12/30/2020|GM-WO-2026972,01/10/2021
		gmDBManager.setString(2,  GmCommonClass.parseNull((String) hmParam.get("INPUTSTR")));
		gmDBManager.setString(3,  GmCommonClass.parseNull((String) hmParam.get("USERID")));
		gmDBManager.execute();
		
		//Json String
		strJsonWoErrString = GmCommonClass.parseNull(gmDBManager.getString(4));
		strJsonPoDtlString = GmCommonClass.parseNull(gmDBManager.getString(5));
		strJsonEmailString = GmCommonClass.parseNull(gmDBManager.getString(6));
		gmDBManager.commit();
		gmDBManager.close();
		
		strJsonString = strJsonWoErrString + '^' +strJsonPoDtlString;
		if(!strJsonEmailString.equals(""))
		{
			sendWODueUpdateEmail(strJsonEmailString,hmParam);
		}
		return strJsonString;
	}
	/**fetchWOPrintDetails: This method used to fetch work order details to print
	 * @param strWoId
	 * @return
	 * @throws AppError
	 */
	public ArrayList fetchWOPrintDetails(String strWoIds) throws AppError{
		
		GmPurchaseBean gmPurchaseBean = new GmPurchaseBean(getGmDataStoreVO());
		
		ArrayList alReturn = new ArrayList();
		HashMap hmLoopWO = new HashMap();
		
		StringTokenizer strTok = new StringTokenizer(strWoIds, ",");
		while (strTok.hasMoreTokens()) {
			strWoIds = strTok.nextToken();
			hmLoopWO = gmPurchaseBean.viewWO(strWoIds, "SystemUser");
			alReturn.add(hmLoopWO);
		}
		
		return alReturn;
	}
	
	//PC#3577-vendor_portal_WO_due_update_notification
	public void sendWODueUpdateEmail(String strWOIds, HashMap hmParam)throws AppError{
		
		GmInitiateVendorJMS gmInitiateVendorJMS = new GmInitiateVendorJMS(getGmDataStoreVO());
		String strCompanyInfo = "";
		String strUserId = "";
		HashMap hmJMSParam = new HashMap();
		strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPINFO"));	
		hmJMSParam.put("REFID", strWOIds);
		hmJMSParam.put("REFTYPE", "108441"); //work order type
		hmJMSParam.put("REFACTION", "26241178"); //WO due Update Action
		hmJMSParam.put("MAILSTATUS", "0"); //to send instant mail
		hmJMSParam.put("USERID", strUserId);
		hmJMSParam.put("COMPANYINFO", strCompanyInfo);
		log.debug("hmJMSParam>>>>>"+hmJMSParam);
		gmInitiateVendorJMS.initVendorEmailJMS(hmJMSParam); 
		
	}
}