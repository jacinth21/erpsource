package com.globus.operations.purchasing.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;

/*PMT-32450 - Work Order Revision Update Dashboard 
 * to update the new partnum revision on  open work orders and save to T402C_WORK_ORDER_REVISION table 
 * and display the record in work order revision dashboard and we can update or ignore the revision 
 * */

public class GmWorkOrderRevisionBean extends GmBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	GmCommonClass gmCommon = new GmCommonClass();
	public GmWorkOrderRevisionBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}

	/**
	 * loadWORevisionRpt - This Methods will load the Updated Work order
	 * revision Details
	 * 
	 * @param hmParam
	 * @return JSON String
	 * @throws AppError
	 */
	public String loadWORevisionRpt(HashMap hmParam) throws AppError {
		
		GmDBManager gmDBManger = new GmDBManager(getGmDataStoreVO());
		
		String strJsonWORevString = "";
		
		String strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
		String strPart = GmCommonClass.parseNull((String) hmParam.get("PART"));
		String strPoid = GmCommonClass.parseNull((String) hmParam.get("POID"));
		String strWOAction = GmCommonClass.parseZero((String) hmParam.get("WOACTIONID"));
		String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
		String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("DATEFORMAT"));
		Date dtWOFromDT = (Date) hmParam.get("DTWOFROMDATE");
		Date dtWOToDT = (Date) hmParam.get("DTWOTODATE");
		String strWOFromDt = GmCommonClass.getStringFromDate(dtWOFromDT,strDateFmt);
		String strWOToDt = GmCommonClass.getStringFromDate(dtWOToDT, strDateFmt);
		
		gmDBManger.setPrepareString("GM_PKG_WO_TXN.gm_fch_wo_revision_dtls", 8);
		gmDBManger.setString(1, strProjectId);
		gmDBManger.setString(2, strPart);
		gmDBManger.setString(3, strPoid);
		gmDBManger.setString(4, strWOAction.equals("0") ? "" : strWOAction);
		gmDBManger.setString(5, strStatus);
		gmDBManger.setString(6, strWOFromDt);
		gmDBManger.setString(7, strWOToDt);
		gmDBManger.registerOutParameter(8, OracleTypes.CLOB);
		gmDBManger.execute();
		strJsonWORevString = GmCommonClass.parseNull(gmDBManger.getString(8));
		gmDBManger.close();
	
		return strJsonWORevString;
	}

	/**
	 * updateWORevision - This Method used for change work order status from
	 * open to Inprogress based on work order
	 * 
	 * @param hmParam
	 * @return Work Order ID
	 * @throws AppError
	 */
	public String updateWORevision(HashMap hmParam) throws AppError {
		
		GmDBManager gmDBManger = new GmDBManager(getGmDataStoreVO());
		
		String strWoId = GmCommonClass.parseNull((String) hmParam.get("WORKORDERID"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strWorkOrderId = "";
		
		gmDBManger.setPrepareString("GM_PKG_WO_TXN.gm_upd_bulk_wo_revision", 3);
		gmDBManger.setString(1, strWoId);
		gmDBManger.setString(2, strUserId);
		gmDBManger.registerOutParameter(3, OracleTypes.CLOB);
		gmDBManger.execute();
		strWorkOrderId = GmCommonClass.parseNull(gmDBManger.getString(3));
		gmDBManger.commit();
		
		return strWorkOrderId;
	}

	/**
	 * processWoRevision - This method Used to Update the work order revision
	 * status from Inprogress to closed or failed status
	 * 
	 * @param strStatus
	 * @param strUserId
	 * @throws AppError
	 * 
	 */
	public void processWoRevision(String strStatus, String strUserId)
			throws AppError {

		GmDBManager gmDBManger = new GmDBManager(getGmDataStoreVO());

		gmDBManger.setPrepareString("GM_PKG_WO_TXN.gm_wo_revision_update", 2);
		gmDBManger.setString(1, strStatus);
		gmDBManger.setString(2, strUserId);
		gmDBManger.execute();
		gmDBManger.commit();

	}

	/**
	 * sendWorkOrderUpdateEmail - This Method Used to send email notification through JMS Called 
	 * @param strPdraw
	 * @param strPmaterialDesc
	 * @param strPrevNum
	 * @param strPclass
	 * @param strPnum
	 * @param strUserId
	 * @param strPdesc
	 * @throws AppError
	 */
	public void sendWorkOrderUpdateEmail(String strPdraw,String strPmaterialDesc, String strPrevNum, String strPclass,String strPnum, String strUserId, String strPdesc) 
			               throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		
		ArrayList alOpenWO = new ArrayList();
	    StringBuffer sbBody = new StringBuffer();
	    StringBuffer sbHead = new StringBuffer();
	    StringBuffer sbMailMsg = new StringBuffer();
	    
	    HashMap hmTemp = new HashMap();
	    HashMap hmTempLoop = new HashMap();
	    
	    int disTime = 0;
	    
	   
	    String strEmailId = "";
	    String strCCId ="";
	    String strNoDisplayFl = "NC";
		String strRevisionUpdate="";
		String strMailfrm="";

	
		strMailfrm = GmCommonClass.getRuleValue("REV_UPD_MAIL_FRM", "EMAIL"); //"GMEP<gmep@globusmedical.com>"
		strCCId = GmCommonClass.getRuleValue("REV_UPD_MAIL_CC", "EMAIL");//djames@globusmedical.com
		
		
		gmDBManager.setPrepareString("GM_PKG_WO_TXN.gm_fch_open_wo_details", 6);
		
		gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
		
		gmDBManager.setString(1, strPnum);
		gmDBManager.setString(2, strPclass);
		gmDBManager.setString(3, strPmaterialDesc);
		gmDBManager.setString(4, strPdraw);
		gmDBManager.setString(5, strPrevNum);
	
		gmDBManager.execute();
		
	    alOpenWO = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6)));
	    
		gmDBManager.close();
		
		if(!alOpenWO.equals("")){
	    HashSet setEmailIds = new HashSet();
	    sbHead.append("<br>");
		sbHead.append("Following Work Orders may require a change because the following parameters has been changed for ");
		sbHead.append("<br>");
		sbHead.append(" Part Number: " + strPnum + " : " + strPdesc);
		sbHead.append("<br><br>");
		int intSize = alOpenWO.size();
		      for (int i = 0; i < intSize; i++) {
		        hmTemp = (HashMap) alOpenWO.get(i);
		        if (hmTemp.get("PNUM").equals(strPnum) && disTime == 0) {
		          if (!strNoDisplayFl.equals(GmCommonClass.parseNull((String) hmTemp.get("MCERT")))) {
		            sbHead.append("Material Certification : "
		                + GmCommonClass.parseNull((String) hmTemp.get("MCERT")));
		            sbHead.append("<br>");
		          }
		          if (!strNoDisplayFl.equals(GmCommonClass.parseNull((String) hmTemp.get("CCERT")))) {
		            sbHead.append("Certificate of Compliance : "
		                + GmCommonClass.parseNull((String) hmTemp.get("CCERT")));
		            sbHead.append("<br>");
		          }
		          if (!strNoDisplayFl.equals(GmCommonClass.parseNull((String) hmTemp.get("HTEST")))) {
		            sbHead.append("Hard Test Required : "
		                + GmCommonClass.parseNull((String) hmTemp.get("HTEST")));
		            sbHead.append("<br>");
		          }
		          if (!strNoDisplayFl.equals(GmCommonClass.parseNull((String) hmTemp.get("PCLASS")))) {
		            sbHead.append("Sterilization required : "
		                + GmCommonClass.parseNull((String) hmTemp.get("PCLASS")));
		            sbHead.append("<br>");
		          }
		          if (!strNoDisplayFl.equals(GmCommonClass.parseNull((String) hmTemp.get("DRAW")))) {
		            sbHead.append("Drawing : " + GmCommonClass.parseNull((String) hmTemp.get("DRAW")));
		            sbHead.append("<br>");
		          }
		          if (!strNoDisplayFl.equals(GmCommonClass.parseNull((String) hmTemp.get("MSPEC")))) {
		            sbHead.append("Material Specification : "
		                + GmCommonClass.parseNull((String) hmTemp.get("MSPEC")));
		            sbHead.append("<br>");
		          }
		          if (!strNoDisplayFl.equals(GmCommonClass.parseNull((String) hmTemp.get("REV")))) {
		            sbHead.append("Revision : " + GmCommonClass.parseNull((String) hmTemp.get("REV")));
		            sbHead.append("<br>");
		            strRevisionUpdate="Y";
		          }
		          sbHead.append("<br>");
		          sbHead.append("Purchase Order&nbsp;&nbsp;&nbsp;Work Order&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vendor Name");
		          sbHead.append("<br>");
		          sbHead.append("---------------------------------------------------------------");
		          sbHead.append("<br>");
		          disTime++;
		        }// End if

		        if (!GmCommonClass.parseNull((String) hmTemp.get("WOID")).equals("")) {
		          setEmailIds.add(GmCommonClass.parseNull((String) hmTemp.get("EMAIL")));
		        }
		      }// For Loop
		   /*Before send mail,If Revision is changed in part number setup screen call saveWoRevisionBulkUpdate method
		    * to insert record in new table.
		    * 		      
		    */
		  	 if(strRevisionUpdate.equals("Y")){
		  		saveWoRevisionBulkUpdate(strPnum,strUserId);
			  }
		          
		      for (Iterator setItr = setEmailIds.iterator(); setItr.hasNext();) {
		        strEmailId = GmCommonClass.parseNull((String) setItr.next());
		        
		        for (int i = 0; i < intSize; i++) {
		          hmTemp = (HashMap) alOpenWO.get(i);
		         
		          if (GmCommonClass.parseNull((String) hmTemp.get("EMAIL")).equals(strEmailId)) {
		            sbBody.append("<br>");
		            sbBody.append(hmTemp.get("POID"));
		            sbBody.append("&nbsp;&nbsp;");
		            sbBody.append(hmTemp.get("WOID"));
		            sbBody.append("&nbsp;&nbsp;");
		            sbBody.append(hmTemp.get("VNAME"));
		            sbBody.append("<br>");
		          }// If Condition
		        }// inner body loop
		       
		        sbMailMsg.append(sbHead);
		        sbMailMsg.append(sbBody);
		     
		        gmCommon.sendMail(strMailfrm,
		         GmCommonClass.StringtoArray(strEmailId, ";"),
		         GmCommonClass.StringtoArray(strCCId, ";"), "Part Number detail Change:" + strPnum,
		        sbMailMsg.toString());
		        sbBody.setLength(0);
		        sbMailMsg.setLength(0);
		      }// Outer Loop		      
	}	
} 
	
	
	
	/**saveWoRevisionBulkUpdate-Save data in t402c table when partnumber revision is changed
	 * @param strPnum
	 * @param strUserId
	 * @throws AppError
	 */
	public void saveWoRevisionBulkUpdate(String strPnum, String strUserId)
			throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("GM_PKG_WO_TXN.gm_sav_wo_revision_bulk_update", 2);
		gmDBManager.setString(1, strPnum);// partnum or work order id
		gmDBManager.setString(2, strUserId);// User Id is missing
		gmDBManager.execute();
		gmDBManager.commit();
	}
	
	
}
