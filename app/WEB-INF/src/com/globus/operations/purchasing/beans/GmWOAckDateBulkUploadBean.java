package com.globus.operations.purchasing.beans;

import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.ArrayList;
import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.db.GmDBManager;
import com.globus.common.beans.GmLogger; 
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.jms.consumers.beans.GmInitiateVendorJMS;
import com.globus.operations.beans.GmPurchaseBean;

import org.apache.log4j.Logger;
/**
 * @author Agilan Singaravel
 *
 */
/**GmWOAckDateBulkUploadBean: This class used to fetch and show the vendor ack date
 * @param 
 * @return
 * @throws 
 */
public class GmWOAckDateBulkUploadBean extends GmBean{

	public GmWOAckDateBulkUploadBean(GmDataStoreVO gmDataStoreVO)
			throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
	
	
	/**updateWOAckDateUpload: This method used to update work order ack date
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String updateWOAckDateUpload(HashMap hmParam) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());	
		//Initialize
		String strJsonWoErrString = "";
		
		gmDBManager.setPrepareString("gm_pkg_wo_lead_time_txn.gm_upd_wo_ack_date_process", 4);
		gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
		gmDBManager.setString(1,  GmCommonClass.parseNull((String) hmParam.get("WOSTR")));
		//In INPUTSTR string contains WO id,ack date,qty like GM-WO-2027192,12/30/2020,10|GM-WO-2026972,01/10/2021,30
		gmDBManager.setString(2,  GmCommonClass.parseNull((String) hmParam.get("INPUTSTR")));
		gmDBManager.setString(3,  GmCommonClass.parseNull((String) hmParam.get("USERID")));
		gmDBManager.execute();
		
		//Json String
		strJsonWoErrString = GmCommonClass.parseNull(gmDBManager.getString(4));
		gmDBManager.commit();
		gmDBManager.close();
		
		return strJsonWoErrString;
	}
}