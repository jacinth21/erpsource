package com.globus.operations.purchasing.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import oracle.jdbc.driver.OracleTypes;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.operations.beans.GmVendorEmailInterface;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmVendorInvoiceEmailBean extends GmBean implements GmVendorEmailInterface {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	GmCommonClass gmCommon = new GmCommonClass();

	public GmVendorInvoiceEmailBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}

	public GmVendorInvoiceEmailBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	}
	
	/* (non-Javadoc)
	 * @see com.globus.operations.beans.GmVendorEmailInterface#saveVendorEmail(java.util.HashMap)
	 */
	public void saveVendorEmail(HashMap hmParam) throws AppError {
		log.debug("saveVendorEmail" + hmParam);
		GmDBManager gmDBManger = new GmDBManager(getGmDataStoreVO());

		String strRefId = GmCommonClass.parseNull((String) hmParam.get("REFID"));
		String strRefAct = GmCommonClass.parseNull((String) hmParam.get("REFACTION"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strRefType = GmCommonClass.parseNull((String) hmParam.get("REFTYPE"));
		gmDBManger.setPrepareString("gm_pkg_vendor_mail_txn.gm_sav_invoice_email_dtl", 4);
		gmDBManger.setString(1, strRefId);
		gmDBManger.setString(2, strRefType);
		gmDBManger.setString(3, strRefAct);
		gmDBManger.setString(4, strUserId);
		gmDBManger.execute();
		gmDBManger.commit();
	}

		/* (non-Javadoc)
	 * @see com.globus.operations.beans.GmVendorEmailInterface#fetchEmailDetail(java.util.HashMap)
	 */
	public ArrayList fetchEmailDetail(HashMap hmParam) throws AppError {
		
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		HashMap hmPODetails = new HashMap();
		String strSessCompanyLocale = GmCommonClass.parseNull(GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid()));

		String strMailStatus = GmCommonClass.parseNull((String) hmParam.get("MAILSTATUS"));
		String strrefaction = GmCommonClass.parseNull((String) hmParam.get("REFACTION"));
		log.debug("strPOId strrefaction >>>>> " + strrefaction + strMailStatus);
		gmDBManager.setPrepareString("gm_pkg_vendor_mail_fch.gm_fch_invoice_email_dtl", 3);
		gmDBManager.setString(1, strMailStatus);
		gmDBManager.setString(2, strrefaction);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();

		alReturn = GmCommonClass.parseNullArrayList(gmDBManager
				.returnArrayList((ResultSet) gmDBManager.getObject(3)));
		gmDBManager.close();
		log.debug("alReturn============== " + alReturn.size());
		hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);

		return alReturn;
	}
}
