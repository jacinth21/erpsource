package com.globus.operations.purchasing.beans;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;
// import org.apache.struts.action.ActionForm;



import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmWorkOrderVendorCommitBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmWorkOrderVendorCommitBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }
  
  /**
   * saveVendorCommit - This method saves the vendor commit for a work order
   * 
   * @param hmParam
   * @return HashMap
   * @exception AppError
   **/
  public void saveVendorCommit(HashMap hmParam)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strPOId = "";
    String strInputString = "";
    String strUserId =  "";
    
    strPOId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
    strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
    
    gmDBManager.setPrepareString("gm_pkg_pur_wo_vendor_commit.gm_sav_vendor_commit", 3);
    gmDBManager.setString(1, strPOId);
    gmDBManager.setString(2, strInputString);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();

    gmDBManager.commit();

  } // end of saveVendorCommit
  
  
  /**
   * loadVendorCommitDtls - This method fetch the vendor commit for a Work order
   * 
   * @param hmParam
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList loadVendorCommitDtls(HashMap hmParam) throws AppError {
	    ArrayList alReturn = new ArrayList();
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    HashMap hmPODetails = new HashMap();
	    
	    hmPODetails = (HashMap)hmParam.get("PODETAILS");		
		String strPOId = GmCommonClass.parseNull((String) hmPODetails.get("POID"));
	    
	    gmDBManager.setPrepareString("gm_pkg_pur_wo_vendor_commit.gm_fch_vendor_commit_dtls", 2);
	    gmDBManager.setString(1, strPOId);
	    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	    gmDBManager.execute();
	 
	    alReturn =
	       GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
	            .getObject(2)));
	   gmDBManager.close();
	    return alReturn;
	  }
  
  /**Description:This Method used to fetch VendorCommit Report Details
   * @param hmParam
   * @return
   * @throws AppError
   */
  public ArrayList loadVendorCommitRptDtls(HashMap hmParam) throws AppError {
  		ArrayList alReturn = new ArrayList();
  		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

  		String strVendorId = GmCommonClass.parseNull((String) hmParam
  				.get("VENDORID"));
  		String strProjectId = GmCommonClass.parseNull((String) hmParam
  				.get("PROJECTID"));
  		String strClosedStatusFl = GmCommonClass.parseNull((String) hmParam
  				.get("SHOWCLOSEDWOFL"));
  		String strCommitFrmDt = GmCommonClass.parseNull((String) hmParam
  				.get("FROMDATE"));
  		String strCommitToDt = GmCommonClass.parseNull((String) hmParam
  				.get("TODATE"));
  		String strPartNum = GmCommonClass.parseNull((String) hmParam
  				.get("PARTNUMBER"));
        if(!strClosedStatusFl.equals("")){
      	   strClosedStatusFl="Y";
         }else{
        	 strClosedStatusFl="N"; 
         }
  		gmDBManager.setPrepareString(
  				"gm_pkg_pur_wo_vendor_commit.gm_fch_vendor_commit_report", 7);
  		gmDBManager.setString(1, strVendorId.equals("0") ? "" : strVendorId);
  		gmDBManager.setString(2, strProjectId.equals("0") ? "" : strProjectId);
  		gmDBManager.setString(3, strPartNum);
  		gmDBManager.setString(4, strClosedStatusFl);
  		gmDBManager.setString(5, strCommitFrmDt);
  		gmDBManager.setString(6, strCommitToDt);
  		gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
  		gmDBManager.execute();

  		alReturn = GmCommonClass.parseNullArrayList(gmDBManager
  				.returnArrayList((ResultSet) gmDBManager.getObject(7)));
  		gmDBManager.close();
  		return alReturn;
  	  }
  
}
