/**
 * Licensed under the Artistic License; you may not use this file
 * except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://displaytag.sourceforge.net/license.html
 *
 * THIS PACKAGE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */
package com.globus.operations.purchasing.orderplanning.displaytag;

import java.text.DecimalFormat;

import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.TableDecorator;
import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;


/**
 * This class is a decorator of the TestObjects that we keep in our List. This class provides a number of methods for
 * formatting data, creating dynamic links, and exercising some aspects of the display:table API functionality.
 *  
 * @author Rakhi Gandhi
 * @version $Revision$ ($Author$)
 */
public class DTOPMonthlyDemandSheetWrapper extends TableDecorator
{

    private DynaBean db;
    private String strValue;
    private String strSheetName;
    private String strValueFormatted;
    private String strMonth;
    private String strYear;
    private String strDemandId;
    private String strDSStatus;
    private String strAccessTypeId;
    private StringBuffer sbValue;

    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
    String strImagePath = GmCommonClass.getString("GMIMAGES");
    
    /**
     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
     */
    public DTOPMonthlyDemandSheetWrapper()
    {
        super();
    }

    
    public String getDMSHEETID()
    {
        db =  	(DynaBean) this.getCurrentRowObject();
        strValue = (String)db.get("DMSHEETID");
        strMonth = (String)db.get("LDTMONTH");
        strYear  =  (String)db.get("LDTYEAR");   
        strDemandId = (String)db.get("DMID");
        strDSStatus = (String)db.get("STATUSID");
        strSheetName = (String)db.get("DEMNM");
        strAccessTypeId =GmCommonClass.parseNull(String.valueOf(db.get("ACCESSTYPEID")));
        strSheetName = strSheetName.replaceAll("\\s", "&nbsp;");
    
        String strLimitedAccess= GmCommonClass.parseNull((String)getPageContext().getRequest().getAttribute("DMNDSHT_LIMITED_ACC"));
        sbValue = new StringBuffer();
      //  System.out.println("In decorator " + strValue );
	    //return "<a  href=/gmDSSummary.do?demandSheetMonthId=01&strOpt=fetch&demandSheetId=" + strValue + "> "+ " </a>" ;
       // System.out.println("<a  href=/gmDSSummary.do?strOpt=fetch&demandSheetMonthId=" + strValue + "&monthId="+strMonth+"&yearId="+strYear+"&demandSheetId="+(String)db.get("DMID")+"> "+" Hi" + " </a>");
        sbValue.append("<a  href=javascript:fnReLoadDSSummary(");
        sbValue.append(strValue);
        sbValue.append(",'");
        sbValue.append(strMonth);
        sbValue.append("',");
        sbValue.append(strYear);
        sbValue.append(",");
        sbValue.append(strDemandId);
        sbValue.append("); title=\"Click to Load Demand Sheet Summary\"> <img src='");
        sbValue.append(strImagePath);
        sbValue.append("/edit_icon.gif' border='0' /> </a> ");
        
        if(strAccessTypeId.equals("102623")){//102623 - Editable sheet 
        sbValue.append("&nbsp;");
        sbValue.append(" <a href=javascript:fnReLoadSheet(");
        sbValue.append(strValue);
        sbValue.append(",'");
        sbValue.append(strSheetName);
        sbValue.append("',");
        sbValue.append(strDSStatus);
        sbValue.append("); title=\"Click to Reload Sheet\"> <img src='");
        sbValue.append(strImagePath);
        sbValue.append("/return_black.gif' border='0' height='15' width='15' /> ");
        sbValue.append("</a>");
        sbValue.append("&nbsp;");
        //If the Logged in user is from International CUstomer Service department and based on access the status change will be enabled.
        if(!strLimitedAccess.equalsIgnoreCase("true")){
        sbValue.append(" <a href=javascript:fnChangeStatus(");
        sbValue.append(strValue);
        sbValue.append(",'");
        sbValue.append(strSheetName);
        sbValue.append("',");
        sbValue.append(strDSStatus);
        sbValue.append("); title=\"Change Status from Approved to Open\"> <img src='");
        sbValue.append(strImagePath);
        sbValue.append("/o.gif' border='0' height='14' width='14' /> ");
        sbValue.append("</a>");
        }
        }

        return sbValue.toString();
        //return "<a  href=/gmDSSummary.do?strOpt=fetch&demandSheetMonthId=" + strValue + "&monthId="+strMonth+"&yearId="+strYear+"&demandSheetId="+(String)db.get("DMID")+"   > "+ " <img src='"+strImagePath+"/edit_icon.gif" + "' border='0' />"  + " </a>" ;
    }

}
