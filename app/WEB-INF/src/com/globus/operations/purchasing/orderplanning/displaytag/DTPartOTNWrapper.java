package com.globus.operations.purchasing.orderplanning.displaytag;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTPartOTNWrapper extends TableDecorator
{
    private String strPar; 
    private String strCom;
    
    private String strPartNumber;
    private String strType = "";
    private String strHistory = "";
    private DynaBean db ;
    private int intCount = 0;
    private String strRefID ="";
    public StringBuffer  strValue = new StringBuffer();
    String strImagePath = GmCommonClass.getString("GMIMAGES");
    
    
    /**
     * Returns the string for text field.
     * @return String
     */
    public String getOTNVALUE()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strPar = String.valueOf(db.get("OTNVALUE"));
        strHistory = String.valueOf(db.get("HISTORY_FL"));
        strPartNumber   = (String)db.get("PNUM");
        strPar = strPar.equals("null")?"":strPar;
        strRefID   = String.valueOf(db.get("REFID"));
         
        strValue.setLength(0);
        strValue.append("<input type=text style='text-align:center' name=otnvalue");
        strValue.append(String.valueOf(intCount) );
        strValue.append(" size = 5 value='");
        strValue.append(strPar);
        strValue.append("' onBlur ='fnOTNValidate(this.value,"+intCount+")' ");
        strValue.append(" MAXLENGTH=5 > ");
        strValue.append("<input type = hidden name = pnum");
        strValue.append(String.valueOf(intCount) );
        strValue.append(" value='");
        strValue.append(strPartNumber);
        strValue.append("'>");
        strValue.append("<input type=hidden name ='hOTNVal");
		strValue.append(intCount);
		strValue.append("' value='");
		strValue.append(strPar);
		strValue.append("'> ");
       
        if (strHistory.equals("Y"))
        {
        	strValue.append("    <img style='cursor:hand' src='/images/icon_History.gif'" +  
             "border='0'" + 
                 "onClick=\"javascript:fnOTNHistory( '"+ 1053 +"','" + strRefID + "') \" />" ); 
        }
        
        
        intCount++;
        return strValue.toString();
        
        
    }
     
       
    public String getCOMMENTS()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strPartNumber   = (String)db.get("PNUM");
        strType = db.get("RLOG").toString();

                 
        if (strType.equals("Y") )
        {
            return "<img id='imgEditA' style='cursor:hand' src='/images/phone-icon_ans.gif'" +  
            " width='20' height='17'" + 
                "onClick=\"javascript:fnLog( '"+4000105+"','" + strPartNumber + "') \" />"; 
        }
        else
        {
            return "<img id='imgEditA' style='cursor:hand' src='/images/phone_icon.jpg'" +  
            " width='20' height='17' " + 
                "onClick=\"javascript:fnLog( '"+4000105+"','" + strPartNumber + "') \" />";
        }
                
    }
}


