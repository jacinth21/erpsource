package com.globus.operations.purchasing.orderplanning.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.purchasing.orderplanning.beans.GmOPForecastReportBean;
import com.globus.operations.purchasing.orderplanning.forms.GmOPDemandSheetAjaxForm;

/*
 * The name has Ajax because we have to move all the ajax actions from demand sheet summary action
 */

public class GmOPDemandSheetAjaxAction extends GmAction{
	
	   public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
		   GmOPDemandSheetAjaxForm gmOPDemandSheetAjaxForm = (GmOPDemandSheetAjaxForm)form;
		   gmOPDemandSheetAjaxForm.loadSessionParameters(request);
		   GmOPForecastReportBean gmOPForecastReportBean = new GmOPForecastReportBean();
           Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
	       String strOpt = gmOPDemandSheetAjaxForm.getStrOpt();
	       HashMap hmParam =  GmCommonClass.getHashMapFromForm(gmOPDemandSheetAjaxForm);
	       
	       /* This will setup jobs for pull multipart sheets. */  
	       if (strOpt.equals("pullopensheets"))
	        {	  	   
	    	   gmOPDemandSheetAjaxForm.setStrMsg(GmCommonClass.parseNull((String)gmOPForecastReportBean.pullDemandSheets(hmParam)));
	        }
	       log.debug("Test Test");
	        if(strOpt.equals("opensheets") || strOpt.equals("pullopensheets"))
	        {	        	
	        	gmOPDemandSheetAjaxForm.setAlResult(gmOPForecastReportBean.fchOpenSheets(hmParam));	        	
	        }	        
	        else if(strOpt.equals("multisheetdetails"))
	        {
	        	gmOPDemandSheetAjaxForm.setHmReport(gmOPForecastReportBean.fchMultiPartDetails(hmParam));
	        }
	        request.setAttribute("FORMNAME", "frmOPDemandSheetAjax");
	        return mapping.findForward(strOpt);
	        }
}
