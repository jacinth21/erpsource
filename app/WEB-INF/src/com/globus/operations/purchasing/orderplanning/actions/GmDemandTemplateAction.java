package com.globus.operations.purchasing.orderplanning.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.purchasing.orderplanning.beans.GmDemandTemplateBean;
import com.globus.operations.purchasing.orderplanning.forms.GmDemandTemplateForm;

public class GmDemandTemplateAction extends GmAction {
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
public ActionForward execute(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response)
		throws Exception {
	log.debug("=====GmDemandTemplateAction======");
	
	String strOpt = "";
	String strDemandTempltId = "";
	String strForward = "";
	String strPartyId = "";
	String strUpdateFl = "";
	ArrayList alDemandTemplt = new ArrayList();
	ArrayList alDSOwner = new ArrayList();
	HashMap hmAccess = new HashMap();
	
	GmLogonBean gmLogonBean = new GmLogonBean();
	GmDemandTemplateBean gmDemandTemplateBean = new GmDemandTemplateBean(); 
	GmAccessControlBean gmAccessControlBean   = new GmAccessControlBean();
	
	GmDemandTemplateForm gmDemandTemplateForm = (GmDemandTemplateForm)form;
	gmDemandTemplateForm.loadSessionParameters(request);
	
	strOpt = GmCommonClass.parseNull(gmDemandTemplateForm.getStrOpt());
	strPartyId = GmCommonClass.parseNull((String) gmDemandTemplateForm.getSessPartyId());
	hmAccess = GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId, "DMD_TEMP_SETUP")));
    strUpdateFl = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
	
	log.debug("=====strOpt======"+strOpt);
	if(strOpt.equals("edit")){
		fetchDemandTemplate(mapping, form, request, response);
		strOpt = "load";
	}else if(strOpt.equals("save")){
		saveDemandTemplate(mapping, form, request, response);
		strDemandTempltId = GmCommonClass.parseNull(gmDemandTemplateForm.getDemandTempltId());
		strForward = "/gmDemandTemplate.do?strOpt=edit&demandTempltId="+strDemandTempltId;
		ActionRedirect actionRedirect = new ActionRedirect(strForward);
	    return actionRedirect;
	}else if(strOpt.equals("load_template_map")){
		fetchDemandMapping(mapping, form, request, response);
		strForward = "GmDemandTempltMapping";
	}else if(strOpt.equals("save_template_map")){
		saveDemandTemplateMapping(mapping, form, request, response);
		strDemandTempltId = GmCommonClass.parseNull(gmDemandTemplateForm.getDemandTempltId());
		strForward = "/gmDemandTemplate.do?strOpt=load_template_map&demandTempltId="+strDemandTempltId;
		ActionRedirect actionRedirect = new ActionRedirect(strForward);
	    return actionRedirect;
	}else if(strOpt.equals("load_template_summary")){
		fetchDemandTemplateSummary(mapping, form, request, response);
		strForward = "GmDemandTempltSummary";
	}else if(strOpt.equals("save_template_summary")){
		createDemandSheet(mapping, form, request, response);
		strDemandTempltId = GmCommonClass.parseNull(gmDemandTemplateForm.getDemandTempltId());
		strForward = "/gmDemandTemplate.do?strOpt=load_template_summary&demandTempltId="+strDemandTempltId;
		ActionRedirect actionRedirect = new ActionRedirect(strForward);
	    return actionRedirect;
	}else if(strOpt.equals("inactive")){
		inActiveDemandTemplate(mapping, form, request, response);
		strDemandTempltId = GmCommonClass.parseNull(gmDemandTemplateForm.getDemandTempltId());
		strForward = "/gmDemandTemplate.do?strOpt=edit&demandTempltId="+strDemandTempltId;
		ActionRedirect actionRedirect = new ActionRedirect(strForward);
	    return actionRedirect;
	}
	
	if(strOpt.equals("load")){
		alDemandTemplt = GmCommonClass.parseNullArrayList(gmDemandTemplateBean.fetchDemandTemplateList());
	    alDSOwner = GmCommonClass.parseNullArrayList(gmLogonBean.getEmployeeList("300", "W','P"));
	    
		gmDemandTemplateForm.setAlCompany(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("COMP")));
		gmDemandTemplateForm.setAlDemandTemplt(alDemandTemplt);
	    gmDemandTemplateForm.setAlDSOwner(alDSOwner);
	    gmDemandTemplateForm.setDemandRuleVal(GmCommonClass.getRuleValue("DMD_PERIOD", "DS_TEMPLATE_DEFAULT"));
	    gmDemandTemplateForm.setForecastRuleVal(GmCommonClass.getRuleValue("FORECAST_PERIOD", "DS_TEMPLATE_DEFAULT"));
	    gmDemandTemplateForm.setSetBuildRuleVal(GmCommonClass.getRuleValue("SB_FORECAST_PERIOD", "DS_TEMPLATE_DEFAULT"));
	    strForward = "GmDemandTemplateSetup";
	}
	gmDemandTemplateForm.setSubmitFlag(strUpdateFl);
	return mapping.findForward(strForward);
}

private void fetchDemandTemplate(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) throws AppError{
	
	String strDemandTempltId = "";
	String strInActiveFl = "";
	HashMap hmReturn = new HashMap();
	ArrayList alLogReasons = new ArrayList();
    
	GmDemandTemplateBean gmDemandTemplateBean = new GmDemandTemplateBean(); 
	GmCommonBean gmCommonBean = new GmCommonBean();
	
	GmDemandTemplateForm gmDemandTemplateForm = (GmDemandTemplateForm)form;
	
	strDemandTempltId = GmCommonClass.parseNull(gmDemandTemplateForm.getDemandTempltId());
	hmReturn = GmCommonClass.parseNullHashMap(gmDemandTemplateBean.fetchDemandTemplate(strDemandTempltId));
	GmCommonClass.getFormFromHashMap(gmDemandTemplateForm,hmReturn);
	strInActiveFl = GmCommonClass.parseNull((String)hmReturn.get("INACTIVEFLAG"));
	strInActiveFl = strInActiveFl.equals("Y")?"on":"off";
	gmDemandTemplateForm.setInactiveFlag(strInActiveFl);
    alLogReasons = gmCommonBean.getLog(strDemandTempltId,"1227",GmCommonClass.getString("JNDI_CONNECTION"));
    gmDemandTemplateForm.setAlLogReasons(alLogReasons);
}

private void saveDemandTemplate(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) throws AppError{
	
	String strDemandTempltId = "";
	HashMap hmParam = new HashMap();
	GmDemandTemplateBean gmDemandTemplateBean = new GmDemandTemplateBean(); 
    
	GmDemandTemplateForm gmDemandTemplateForm = (GmDemandTemplateForm)form;
	
	hmParam = GmCommonClass.getHashMapFromForm(gmDemandTemplateForm);
	strDemandTempltId = GmCommonClass.parseNull(gmDemandTemplateBean.saveDemandTemplate(hmParam));
	gmDemandTemplateForm.setDemandTempltId(strDemandTempltId);
}

private void fetchDemandMapping(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) throws AppError{
	
	String strDemandTempltId = "";
	HashMap hmReturn = new HashMap();
	HashMap hmTemplt = new HashMap();
    
	GmDemandTemplateBean gmDemandTemplateBean = new GmDemandTemplateBean(); 
	
	GmDemandTemplateForm gmDemandTemplateForm = (GmDemandTemplateForm)form;
	
	strDemandTempltId = GmCommonClass.parseNull(gmDemandTemplateForm.getDemandTempltId());
	hmTemplt = GmCommonClass.parseNullHashMap(gmDemandTemplateBean.fetchDemandTemplate(strDemandTempltId));
	GmCommonClass.getFormFromHashMap(gmDemandTemplateForm,hmTemplt);
	hmReturn = GmCommonClass.parseNullHashMap(gmDemandTemplateBean.fetchTemplateMapping(strDemandTempltId));
    gmDemandTemplateForm.setAlGroupAvail((ArrayList)hmReturn.get("GROUP_AVAIL"));
    gmDemandTemplateForm.setAlGroupAssoc((ArrayList)hmReturn.get("GROUP_ASSOC"));
    gmDemandTemplateForm.setAlConSetAvail((ArrayList)hmReturn.get("CONSET_AVAIL"));
    gmDemandTemplateForm.setAlConSetAssoc((ArrayList)hmReturn.get("CONSET_ASSOC"));
    gmDemandTemplateForm.setAlInhSetAvail((ArrayList)hmReturn.get("INHSET_AVAIL"));
    gmDemandTemplateForm.setAlInhSetAssoc((ArrayList)hmReturn.get("INHSET_ASSOC"));

}

private void saveDemandTemplateMapping(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) throws AppError{
	
	HashMap hmParam = new HashMap();
	GmDemandTemplateBean gmDemandTemplateBean = new GmDemandTemplateBean(); 
    
	GmDemandTemplateForm gmDemandTemplateForm = (GmDemandTemplateForm)form;
	
	hmParam = GmCommonClass.getHashMapFromForm(gmDemandTemplateForm);
	gmDemandTemplateBean.saveDemandTemplateMapping(hmParam);
}


private void fetchDemandTemplateSummary(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) throws AppError{
	ArrayList alDemandSheets = new ArrayList();
	String strDemandTempltId = "";
	GmDemandTemplateBean gmDemandTemplateBean = new GmDemandTemplateBean(); 
    
	GmDemandTemplateForm gmDemandTemplateForm = (GmDemandTemplateForm)form;
	fetchDemandMapping(mapping, form, request, response);
	strDemandTempltId = GmCommonClass.parseNull(gmDemandTemplateForm.getDemandTempltId());
	alDemandSheets = GmCommonClass.parseNullArrayList((ArrayList)gmDemandTemplateBean.fetchDemandSheetByTemplate(strDemandTempltId));
	gmDemandTemplateForm.setAlDemandSheets(alDemandSheets);
}

private void createDemandSheet(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) throws AppError{
	
	HashMap hmParam = new HashMap();
	GmDemandTemplateBean gmDemandTemplateBean = new GmDemandTemplateBean(); 
    
	GmDemandTemplateForm gmDemandTemplateForm = (GmDemandTemplateForm)form;
	
	hmParam = GmCommonClass.getHashMapFromForm(gmDemandTemplateForm);
	gmDemandTemplateBean.createDemandSheet(hmParam);
}

private void inActiveDemandTemplate(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) throws AppError{
	HashMap hmParam = new HashMap();
	GmDemandTemplateBean gmDemandTemplateBean = new GmDemandTemplateBean(); 
    
	GmDemandTemplateForm gmDemandTemplateForm = (GmDemandTemplateForm)form;
	
	hmParam = GmCommonClass.getHashMapFromForm(gmDemandTemplateForm);
	gmDemandTemplateBean.inActiveDemandTemplate(hmParam);
}
}
