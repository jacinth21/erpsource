/**
 * FileName    : GmDemandSheetAjaxForm.java 
 * Description :
 * Author      : vprasath
 * Date        : Aug 15, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.operations.purchasing.orderplanning.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

/**
 * @author vprasath
 *
 */
public class GmOPDemandSheetAjaxForm extends GmCommonForm {
	 private String demandSheetMonthId = "";
	 private String partDetails = "";
	 private String forecastMonths = "";
	 private String demandSheetString = "";
	 private String statusId = "";
	 private String monthId ="";
	 private String yearId = "";
	 private String inventoryId = "";
	 private String strMsg = "";
	 private HashMap hmReport = new HashMap();
	 private List alResult = new ArrayList();
	 
	 
	public String getDemandSheetMonthId() {
		return demandSheetMonthId;
	}
	public void setDemandSheetMonthId(String demandSheetMonthId) {
		this.demandSheetMonthId = demandSheetMonthId;
	}
	
	public String getPartDetails() {
		return partDetails;
	}
	public void setPartDetails(String partDetails) {
		this.partDetails = partDetails;
	}
	public HashMap getHmReport() {
		return hmReport;
	}
	public void setHmReport(HashMap hmReport) {
		this.hmReport = hmReport;
	}
	public List getAlResult() {
		return alResult;
	}
	public void setAlResult(List alResult) {
		this.alResult = alResult;
	}
	public String getForecastMonths() {
		return forecastMonths;
	}
	public void setForecastMonths(String forecastMonths) {
		this.forecastMonths = forecastMonths;
	}
	public String getDemandSheetString() {
		return demandSheetString;
	}
	public void setDemandSheetString(String demandSheetString) {
		this.demandSheetString = demandSheetString;
	}
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	/**
	 * @return the monthId
	 */
	public String getMonthId() {
		return monthId;
	}
	/**
	 * @param monthId the monthId to set
	 */
	public void setMonthId(String monthId) {
		this.monthId = monthId;
	}
	/**
	 * @return the yearId
	 */
	public String getYearId() {
		return yearId;
	}
	/**
	 * @param yearId the yearId to set
	 */
	public void setYearId(String yearId) {
		this.yearId = yearId;
	}
	/**
	 * @return the inventoryId
	 */
	public String getInventoryId() {
		return inventoryId;
	}
	/**
	 * @param inventoryId the inventoryId to set
	 */
	public void setInventoryId(String inventoryId) {
		this.inventoryId = inventoryId;
	}
	/**
	 * @return the strMsg
	 */
	public String getStrMsg() {
		return strMsg;
	}
	/**
	 * @param strMsg the strMsg to set
	 */
	public void setStrMsg(String strMsg) {
		this.strMsg = strMsg;
	}
	 
	 

}
