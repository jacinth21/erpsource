package com.globus.operations.purchasing.orderplanning.forms;

import java.util.ArrayList;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.forms.GmCommonForm;

/**
 * @author pvigneshwaran
 *
 */
public class GmOPTTPNewSummaryForm extends GmCommonForm{
	
	private String ttpId = "";
    private String monthId = GmCalenderOperations.getCurrentMM();
    private String yearId = String.valueOf(GmCalenderOperations.getCurrentYear());
    private String inventoryId = "";
    private String ttpForecastMonths = "";
    private String demandSheetIds = "";
    private String levelId="";
    private String companyID="";
    private String lvlHierarchy="";
	private String gridData="";
	private String accessId = "";
	private String ttpName="";
	private String hCompanyID = "";
	private String levelFl = "";
	
	/**
	 * @return the ttpName
	 */
	public String getTtpName() {
		return ttpName;
	}
	/**
	 * @param ttpName the ttpName to set
	 */
	public void setTtpName(String ttpName) {
		this.ttpName = ttpName;
	}
	/**
	 * @return the ttpId
	 */
	public String getTtpId() {
		return ttpId;
	}
	/**
	 * @param ttpId the ttpId to set
	 */
	public void setTtpId(String ttpId) {
		this.ttpId = ttpId;
	}
	/**
	 * @return the monthId
	 */
	public String getMonthId() {
		return monthId;
	}
	/**
	 * @param monthId the monthId to set
	 */
	public void setMonthId(String monthId) {
		this.monthId = monthId;
	}
	/**
	 * @return the yearId
	 */
	public String getYearId() {
		return yearId;
	}
	/**
	 * @param yearId the yearId to set
	 */
	public void setYearId(String yearId) {
		this.yearId = yearId;
	}
	/**
	 * @return the inventoryId
	 */
	public String getInventoryId() {
		return inventoryId;
	}
	/**
	 * @param inventoryId the inventoryId to set
	 */
	public void setInventoryId(String inventoryId) {
		this.inventoryId = inventoryId;
	}
	/**
	 * @return the ttpForecastMonths
	 */
	public String getTtpForecastMonths() {
		return ttpForecastMonths;
	}
	/**
	 * @param ttpForecastMonths the ttpForecastMonths to set
	 */
	public void setTtpForecastMonths(String ttpForecastMonths) {
		this.ttpForecastMonths = ttpForecastMonths;
	}
	/**
	 * @return the demandSheetIds
	 */
	public String getDemandSheetIds() {
		return demandSheetIds;
	}
	/**
	 * @param demandSheetIds the demandSheetIds to set
	 */
	public void setDemandSheetIds(String demandSheetIds) {
		this.demandSheetIds = demandSheetIds;
	}
	/**
	 * @return the levelId
	 */
	public String getLevelId() {
		return levelId;
	}
	/**
	 * @param levelId the levelId to set
	 */
	public void setLevelId(String levelId) {
		this.levelId = levelId;
	}
	/**
	 * @return the companyID
	 */
	public String getCompanyID() {
		return companyID;
	}
	/**
	 * @param companyID the companyID to set
	 */
	public void setCompanyID(String companyID) {
		this.companyID = companyID;
	}
	/**
	 * @return the lvlHierarchy
	 */
	public String getLvlHierarchy() {
		return lvlHierarchy;
	}
	/**
	 * @param lvlHierarchy the lvlHierarchy to set
	 */
	public void setLvlHierarchy(String lvlHierarchy) {
		this.lvlHierarchy = lvlHierarchy;
	}
	/**
	 * @return the gridData
	 */
	public String getGridData() {
		return gridData;
	}
	/**
	 * @param gridData the gridData to set
	 */
	public void setGridData(String gridData) {
		this.gridData = gridData;
	}
	/**
	 * @return the accessId
	 */
	public String getAccessId() {
		return accessId;
	}
	/**
	 * @param accessId the accessId to set
	 */
	public void setAccessId(String accessId) {
		this.accessId = accessId;
	}
	/**
	 * @return the hCompanyID
	 */
	public String gethCompanyID() {
		return hCompanyID;
	}
	/**
	 * @param hCompanyID the hCompanyID to set
	 */
	public void sethCompanyID(String hCompanyID) {
		this.hCompanyID = hCompanyID;
	}
	/**
	 * @return the levelFl
	 */
	public String getLevelFl() {
		return levelFl;
	}
	/**
	 * @param levelFl the levelFl to set
	 */
	public void setLevelFl(String levelFl) {
		this.levelFl = levelFl;
	}
	 	
	
	

}
