package com.globus.operations.purchasing.orderplanning.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import java.util.ArrayList;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.purchasing.orderplanning.beans.GmOPTTPReportBean;
import com.globus.operations.purchasing.orderplanning.beans.GmOPForecastTransBean;
import com.globus.operations.purchasing.orderplanning.forms.GmOPDrillDownForm;

public class GmOPTTPDrillDownAction extends GmAction {

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

	// Initialize
	// the
	// Logger
	// Class.

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			GmOPTTPReportBean gmOPTTPReportBean = new GmOPTTPReportBean();
			GmOPForecastTransBean gmOPForecastTransBean = new GmOPForecastTransBean();
			GmOPDrillDownForm gmOPDrillDownForm = (GmOPDrillDownForm) form;
			HashMap hmReport = new HashMap();
			HashMap hmReturn = new HashMap();
			HashMap hmParams = new HashMap();
			String strOpt = "";

			strOpt = gmOPDrillDownForm.getStrOpt();
			log.debug("strOpt: " + strOpt);
			if (strOpt.equals("PPQDrillDown")) {
				ArrayList alPPQDetail = new ArrayList();
				String strPartNum = gmOPDrillDownForm.getPartNumber();
				String strInventoryLockId = gmOPDrillDownForm.getInventoryLockId();
				String strParentPartQty = gmOPDrillDownForm.getParentPartQty();
				log.debug("strPartNum : " + strPartNum + "--strInventoryLockId:" + strInventoryLockId);
				hmParams.put("PARTNUMBERS", strPartNum);
				hmParams.put("INVENTORYLOCKID", strInventoryLockId);

				hmReturn = gmOPForecastTransBean.fetchPPQDetails(hmParams);
				alPPQDetail = (ArrayList) hmReturn.get("PARENTPARTDETAILS");
				// log.debug("alPPQDetail:"+ alPPQDetail);

				gmOPDrillDownForm.setAlParentPartDetails(alPPQDetail);
				gmOPDrillDownForm.setPartNumber(strPartNum);
				gmOPDrillDownForm.setPartDescription((String) hmReturn.get("PDESC"));
				gmOPDrillDownForm.setParentPartQty(strParentPartQty);
				return mapping.findForward("parentPartDetails");

			} else {
				HashMap hmParam = GmCommonClass.getHashMapFromForm(gmOPDrillDownForm);
				log.debug(" values inside param --- " + hmParam);
				hmReport = gmOPTTPReportBean.fetchPartDemandSheetDetails(hmParam);
				gmOPDrillDownForm.setPartDetails((String) hmReport.get("PARTDETAILS"));
				gmOPDrillDownForm.setHmReport((HashMap) hmReport.get("CROSSTABREPORT"));
				return mapping.findForward("partSheetDetails");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.debug("Exception " + e);
		}
		return mapping.findForward("partSheetDetails");
	}

}
