package com.globus.operations.purchasing.orderplanning.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;

public class GmGlobalMappingReportBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
	 * listGlobalMapping - This method will report on Global Mapping
	 * 
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList listGlobalMapping(HashMap hmParam) throws AppError {
		ArrayList alResult = new ArrayList();
		String strCompanyId = GmCommonClass.parseNull((String)hmParam.get("COMPANYID"));
		String strLevelId = GmCommonClass.parseNull((String)hmParam.get("LEVELID"));
		String strValueId = GmCommonClass.parseNull((String)hmParam.get("VALUEID"));
		String strAccessId = GmCommonClass.parseNull((String)hmParam.get("ACCESSID"));
		String strInventoryId = GmCommonClass.parseNull((String)hmParam.get("INVENTORYID"));
		
		strCompanyId = strCompanyId.equals("0")?"":strCompanyId; 
		strLevelId = strLevelId.equals("0")?"":strLevelId; 
		strValueId = strValueId.equals("0")?"":strValueId; 
		strAccessId = strAccessId.equals("0")?"":strAccessId;
		strInventoryId = strInventoryId.equals("0")?"":strInventoryId; 
		
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		gmDBManager.setPrepareString("gm_pkg_oppr_global_mapping_rpt.gm_fch_ds_global_mapping_dtl", 6);
		gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
		gmDBManager.setString(1, strCompanyId);
		gmDBManager.setString(2, strLevelId);
		gmDBManager.setString(3, strValueId);
		gmDBManager.setString(4, strAccessId);
		gmDBManager.setString(5, strInventoryId);
		gmDBManager.execute();

		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));
		gmDBManager.close();
		return alResult;
	}
	/**
	 * fetchInventoryLevelList - This method will List the values for Value and Inventory Dropdowns
	 * @param String strCodeGrp
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList fetchInventoryLevelList(String strGroup) throws AppError {
		ArrayList alResult = new ArrayList();		
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		if(strGroup.equals("INVENTORY")){
			gmDBManager.setPrepareString("gm_pkg_oppr_global_mapping_rpt.gm_fch_glbmap_Inventory", 1);
		}else if(strGroup.equals("LEVELVALUE")){
			gmDBManager.setPrepareString("gm_pkg_oppr_global_mapping_rpt.gm_fch_glbmap_LevelValue", 1);
		}
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();
		return alResult;
	} // Close of fetchInventoryLevelList()
}
