/**
 * FileName    : GmSetMappingAction.java 
 * Description :
 * Author      : vprasath
 * Date        : Aug 8, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.operations.purchasing.orderplanning.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.purchasing.orderplanning.beans.GmOPDemandSetupBean;
import com.globus.operations.purchasing.orderplanning.forms.GmOPDemandSheetSetupForm;
 
public class GmOPSetMappingAction extends GmAction {
	
	 public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
	        try {
	        GmOPDemandSheetSetupForm gmOPDemandSheetSetupForm = (GmOPDemandSheetSetupForm)form;
	        gmOPDemandSheetSetupForm.loadSessionParameters(request);
	        GmOPDemandSetupBean gmOPDemandSetupBean = new GmOPDemandSetupBean();
	        
	        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

	        String strOpt = gmOPDemandSheetSetupForm.getStrOpt();
	        String strDemandSheetId = gmOPDemandSheetSetupForm.getDemandSheetId();
	        String strInput = gmOPDemandSheetSetupForm.getHinputSetAction();       
	        RowSetDynaClass rdResult = null;
	        
	        log.debug(GmCommonClass.getHashMapFromForm(gmOPDemandSheetSetupForm));
	        log.debug (" Testing. ..... ");
	        if (strOpt.equals("save"))
	        { 
	            gmOPDemandSetupBean.saveSetMap(strInput); 
	        } 
	        
	        rdResult = gmOPDemandSetupBean.loadSetMapList(strDemandSheetId);
	        gmOPDemandSheetSetupForm.setLdtResult(rdResult.getRows());
	        gmOPDemandSheetSetupForm.setAlSetActionType((ArrayList)GmCommonClass.getCodeList("SATYP"));
	      
	        }
	        catch(Exception exp){
	            exp.printStackTrace();
	            throw new AppError(exp);
	        }
	        
	        return mapping.findForward("success");
	    }


}
