package com.globus.operations.purchasing.orderplanning.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;

public class GmOPDemandReportBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
																	// Initialize
																	// the
																	// Logger
																	// Class.

	/**
	 * loadTemplateReport - This method will fetch the Template Mapping Report Details	 * 
	 * @param HashMap - hmParam
	 * @return RowSetDynaClass
	 * @exception AppError , Exception
	 */
	public RowSetDynaClass loadTemplateReport(HashMap hmParam) throws AppError,Exception {
		RowSetDynaClass rdResult = null;
		String strTemplateId = GmCommonClass.parseNull((String) hmParam.get("TEMPLATENAME"));
		String strSetID      = GmCommonClass.parseNull((String) hmParam.get("TEMPLATESETID"));
		String strSetGrpNm   = GmCommonClass.parseNull((String) hmParam.get("TEMPLATESETGRPNM"));
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		log.debug(" loadTemplateReport ");
		gmDBManager.setPrepareString("gm_pkg_oppr_dmd_mas_rpt.gm_fch_template_rpt", 4);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.setString(1, strTemplateId);
		gmDBManager.setString(2, strSetID);
		gmDBManager.setString(3, strSetGrpNm);
		gmDBManager.execute();
		rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));
		gmDBManager.close();
		return rdResult;
	} // End of loadTemplateReport()
	

	/**
	 * fetchTTPReport - This method will report on the TTP
	 * 
	 * @return RowSetDynaClass
	 * @exception AppError
	 */
	public RowSetDynaClass fetchTTPReport() throws AppError {
		RowSetDynaClass rdResult = null;
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		log.debug(" inside fetch TTP report ");
		gmDBManager.setPrepareString("Gm_pkg_oppr_ttp.gm_fc_fch_ttp_report", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();

		rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();

		return rdResult;
	}
	/**
	 * fetchTTPSummarySheets - This method will fetch the TTP Summary Sheet records only.
	 * 
	 * @return HashMap
	 * @exception AppError
	 */
	public HashMap fetchTTPSummarySheets(HashMap hmParam) throws AppError {
		ArrayList alSheetName = new ArrayList();
		HashMap hmFinalValue = new HashMap();
		
		String strTTPId = GmCommonClass.parseNull((String) hmParam.get("TTPID"));
		String strMonth = GmCommonClass.parseNull((String) hmParam.get("MONTHID"));
		String strYear = GmCommonClass.parseNull((String) hmParam.get("YEARID"));
		String strMonYear = strMonth.concat("/").concat(strYear);
		String strDemandSheetIds = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETIDS"));
		String strInventoryId = GmCommonClass.parseNull((String) hmParam.get("INVENTORYID"));
		String strForecastMonth = GmCommonClass.parseNull((String) hmParam.get("TTPFORECASTMONTHS"));
		String strStatus = "";
		String strFinalDate = "";
		String strDSStatus = "";
		log.debug("strDemandSheetIds = "+strDemandSheetIds);
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		gmDBManager.setPrepareString("gm_pkg_oppr_ttp_summary.gm_fc_fch_ttp_summary_sheets", 9);
		gmDBManager.setString(1, strTTPId);
		gmDBManager.setString(2, strMonYear);
		gmDBManager.setString(3, strDemandSheetIds);
		gmDBManager.setString(4, strInventoryId);
		gmDBManager.setString(5, strForecastMonth);
		gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(7, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(8, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(9, OracleTypes.VARCHAR);
		gmDBManager.execute();
		alSheetName = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));
		strStatus = (String) gmDBManager.getObject(7);
		strFinalDate = (String) gmDBManager.getObject(8);
		strDSStatus = (String) gmDBManager.getObject(9);
		
		gmDBManager.close();
		
		hmFinalValue.put("STATUS", strStatus);
		hmFinalValue.put("SHEETNAME", alSheetName);
		hmFinalValue.put("FINALDATE", strFinalDate);
		hmFinalValue.put("DSSTATUS", strDSStatus);
		return hmFinalValue;
	}
	 /**
	 * This method will fetch the Inventory id
	 * @author  
	 * @param String
	 */
	public String fetchTTPInventoryid(HashMap hmParam) throws AppError{
		String strInvId = "";
		String strTTPId = GmCommonClass.parseNull((String) hmParam.get("TTPID"));
		String strLevelId = GmCommonClass.parseZero((String) hmParam.get("LEVELID"));
		String strCompanyId = GmCommonClass.parseZero((String) hmParam.get("HCOMPANYID"));	
		strCompanyId=strCompanyId.equals("0")?"":strCompanyId;
		strLevelId=strLevelId.equals("0")?"":strLevelId;
		
		log.debug("hmParam"+hmParam);
		
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		gmDBManager.setFunctionString("gm_pkg_oppr_ttp_summary.gm_fch_ttp_inventoryid", 5);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, strTTPId);
		gmDBManager.setString(3, strLevelId); 
		gmDBManager.setString(4, strCompanyId);
		gmDBManager.setString(5, (String)hmParam.get("MONTHID")); 
		gmDBManager.setString(6, (String)hmParam.get("YEARID")); 
		gmDBManager.execute();
		strInvId = GmCommonClass.parseNull((String)gmDBManager.getString(1));
		gmDBManager.close();
		log.debug(" strInvId --> "+strInvId);
		return strInvId;
	}
	 /**
	 * This method will fetch the access type
	 * @author  
	 * @param String
	 */
	public String fetchTTPAccessType(HashMap hmParam) throws AppError{
		String strAccessType = "";
		String strDemandSheetIds = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETIDS"));	
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		gmDBManager.setFunctionString("gm_pkg_oppr_ttp_summary.gm_fch_ttp_access_type", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, strDemandSheetIds);
		gmDBManager.execute();
		strAccessType = GmCommonClass.parseNull((String)gmDBManager.getString(1));
		gmDBManager.close();
		log.debug(" strAccessType --> "+strAccessType);
		return strAccessType;
	}
	/**
	 * fetchTTPSummaryReport - Cross tab report for TTP Summary
	 * 
	 * @return HashMap
	 * @exception AppError
	 */
	public HashMap fetchTTPSummaryReport(HashMap hmParam) throws AppError {
		//log.debug("Parameters in Bean: " + hmParam);

		GmCrossTabReport gmCrossReport = new GmCrossTabReport();		
		ResultSet rsForecastHeader = null;
		ResultSet rsForecastDetails = null;
		ResultSet rsInventoryHeader = null;
		ResultSet rsInventoryDetails = null;
		ResultSet rsAdditionalHeader = null;
		ResultSet rsAdditionalDetails = null;

		HashMap hmForecast = new HashMap();
		HashMap hmInventory = new HashMap();
		HashMap hmFinalValue = new HashMap();
		HashMap hmLinkInfo = new HashMap();
		HashMap hmAddInfo = new HashMap();

		ArrayList alInventoryDetails = new ArrayList();
		ArrayList alTotQtyRequired = new ArrayList();
		ArrayList alTotQtyOrder = new ArrayList();
		ArrayList alUnitPrice = new ArrayList();
		ArrayList alAdjustmentList = new ArrayList();
		ArrayList alSheetName = new ArrayList();
		ArrayList alPendingRequest = new ArrayList();
		ArrayList alPNeedList = new ArrayList();
		ArrayList alParentSubList = new ArrayList();
		ArrayList alAddInfoDetails = new ArrayList();
		HashMap hmSelectedFilter = new HashMap();
		ArrayList alInvDetails = new ArrayList();//PMT-30031 GOP_To show Inventory details in TTP summary
		
		String strTTPId = GmCommonClass.parseNull((String) hmParam.get("TTPID"));
		String strMonth = GmCommonClass.parseNull((String) hmParam.get("MONTHID"));
		String strYear = GmCommonClass.parseNull((String) hmParam.get("YEARID"));
		String strMonYear = strMonth.concat("/").concat(strYear);
		String strDemandSheetIds = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETIDS"));
		String strInventoryId = GmCommonClass.parseNull((String) hmParam.get("INVENTORYID"));
		String strForecastMonth= GmCommonClass.parseZero((String) hmParam.get("TTPFORECASTMONTHS"));
		String strLevelId = GmCommonClass.parseZero((String) hmParam.get("LEVELID"));
		String strLevelHierarchy = GmCommonClass.parseZero((String) hmParam.get("LEVELHIERARCHY"));
		hmSelectedFilter = GmCommonClass.parseNullHashMap((HashMap) hmParam.get("SELECTEDHEADER"));
		strLevelId=strLevelId.equals("0")?"":strLevelId;	
		String strStatus = "";
		String strDSStatus = "";
		String strFinalDate = "";
		String strTTPType = "";		

	//    log.debug(" ***************** " + strTTPId + " - " + " - " + strMonYear + " - " + strDemandSheetIds);
	//    log.debug("Inventory ID: " + strInventoryId + " Forecast Months " + strForecastMonth);


		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		try {
			gmDBManager.setPrepareString("gm_pkg_oppr_ttp_summary.gm_fc_fch_ttp_mon_summary", 21);
			gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
			gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);
			gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(9, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(10, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(11, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(12, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(13, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(14, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(15, OracleTypes.VARCHAR);
			gmDBManager.registerOutParameter(16, OracleTypes.VARCHAR);
			gmDBManager.registerOutParameter(17, OracleTypes.VARCHAR);
			gmDBManager.registerOutParameter(18, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(19, OracleTypes.VARCHAR);
			gmDBManager.registerOutParameter(20, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(21, OracleTypes.CURSOR);
			
			gmDBManager.setString(1, strTTPId);
			gmDBManager.setString(2, strMonYear);
			gmDBManager.setString(3, strDemandSheetIds);
			gmDBManager.setString(4, strLevelHierarchy);
			gmDBManager.setString(5, strInventoryId);
			gmDBManager.setString(6, strForecastMonth);
			gmDBManager.execute();
			
			strInventoryId = (String) gmDBManager.getString(5);
			strForecastMonth = (String) gmDBManager.getString(6);
			rsForecastHeader = (ResultSet) gmDBManager.getObject(7);
			rsForecastDetails = (ResultSet) gmDBManager.getObject(8);
			rsInventoryHeader = (ResultSet) gmDBManager.getObject(9);
			//rsInventoryDetails = (ResultSet) gmDBManager.getObject(10);
			
			alTotQtyRequired = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(11)));
			alTotQtyOrder = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(12)));
			alUnitPrice = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(13)));
			alSheetName =GmCommonClass.parseNullArrayList( gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(14)));
			strStatus = GmCommonClass.parseNull((String) gmDBManager.getObject(15));
			strFinalDate = GmCommonClass.parseNull((String) gmDBManager.getObject(16));
			strTTPType = GmCommonClass.parseNull((String) gmDBManager.getObject(17));
			alParentSubList = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(18)));
			strDSStatus = GmCommonClass.parseNull((String) gmDBManager.getObject(19));
			rsAdditionalHeader =  (ResultSet) gmDBManager.getObject(20);
			rsAdditionalDetails =  (ResultSet) gmDBManager.getObject(21);
			  //PMT-30031 GOP_To show Inventory details in TTP summary
            alInvDetails = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(10)));	
            
	        gmCrossReport.setGroupFl(true);	
	        gmCrossReport.setOverRideFl(true);
			gmCrossReport.setFixedColumn(true);
			gmCrossReport.setFixedParams(8, "CROSSOVERFL");
			gmCrossReport.setFixedParams(7,GmTTPHeader.DESCRIPTION);
			
	        hmForecast = gmCrossReport.GenerateCrossTabReport(rsForecastHeader, rsForecastDetails);
	        ArrayList alHeader =	GmCommonClass.parseNullArrayList((ArrayList)hmForecast.get("Header"));
	        ArrayList alDetails =	GmCommonClass.parseNullArrayList((ArrayList)hmForecast.get("Details"));
	        
	        if(alDetails.size() ==0 )
	        {
	        	//throw new AppError("Sheet is not loaded for the selected time period", "", 'E');
	        	hmFinalValue =  new HashMap();
	        	return hmFinalValue;
	        }
	      
	        
		        gmCrossReport = new GmCrossTabReport(); 
				//hmInventory = gmCrossReport.GenerateCrossTabReport(rsInventoryHeader, rsInventoryDetails);
		        hmInventory.put("Details", alInvDetails);
		        
				gmCrossReport = new GmCrossTabReport();
				hmAddInfo=gmCrossReport.GenerateCrossTabReport(rsAdditionalHeader, rsAdditionalDetails);
				gmDBManager.close();
				
				gmCrossReport.setSkipExtraColumn(true);
				
				if(alUnitPrice != null && alUnitPrice.size() == 0)
				{			  
				  alHeader.add(GmTTPHeader.UNIT_COST);
				}
				
				
				hmLinkInfo = getLinkInfo("Inv");
				alInventoryDetails = GmCommonClass.parseNullArrayList((ArrayList) hmInventory.get("Details"));	
				if(alInventoryDetails != null && alInventoryDetails.size() == 0)
				{			  
				  alHeader.add(GmTTPHeader.IN_STOCK);
				  alHeader.add(GmTTPHeader.PO);
				  alHeader.add(GmTTPHeader.DHR);
				  alHeader.add(GmTTPHeader.BUILD_SETS);
				  alHeader.add(GmTTPHeader.TOTAL_INVENTORY);
				  alHeader.add(GmTTPHeader.RW_INVENTORY);
				}
				hmFinalValue = gmCrossReport.linkCrossTab(hmForecast, alInventoryDetails, hmLinkInfo);
				
				
				
				hmLinkInfo = getLinkInfo("TotQtyReq");
				if(alTotQtyRequired != null && alTotQtyRequired.size() == 0)
				{			  
				  alHeader.add(GmTTPHeader.FORECAST_QTY);
				  alHeader.add(GmTTPHeader.TPR);
				}
				hmFinalValue = gmCrossReport.linkCrossTab(hmFinalValue, alTotQtyRequired, hmLinkInfo);
				
				hmLinkInfo = getLinkInfo("UnitPrice");			
				hmFinalValue = gmCrossReport.linkCrossTab(hmFinalValue, alUnitPrice, hmLinkInfo);
				
				hmLinkInfo = getLinkInfo("AddInfo");
				alAddInfoDetails = (ArrayList) hmAddInfo.get("Details");
				if(alAddInfoDetails != null && alAddInfoDetails.size() == 0)
				{			  
				  alHeader.add(GmTTPHeader.SET_PAR);
				  alHeader.add(GmTTPHeader.OTN);
				}
				hmFinalValue = gmCrossReport.linkCrossTab(hmFinalValue, alAddInfoDetails, hmLinkInfo);
				
				if (alTotQtyOrder.size() > 0) {
					hmLinkInfo = getLinkInfo("OrderQty");
					hmFinalValue = gmCrossReport.linkCrossTab(hmFinalValue, alTotQtyOrder, hmLinkInfo);
				} else {
					hmLinkInfo = getLinkInfo("OrdQty");
					alAdjustmentList = adjustmentList((ArrayList) hmFinalValue.get("Details"), alTotQtyOrder);
					hmFinalValue = gmCrossReport.linkCrossTab(hmFinalValue, alAdjustmentList, hmLinkInfo);
				}			
			}
			catch (Exception exp) {
				exp.printStackTrace();
				gmDBManager.close();
				throw new AppError(exp);
			}
		
			GmTTPCalculations gmTTPCalculations = new GmTTPCalculations();
			gmTTPCalculations.setTTPLevelHierarchy(strLevelHierarchy);
			gmTTPCalculations.setTTPLevelID(strLevelId);
			gmTTPCalculations.loadCalculatedValues(hmFinalValue, alParentSubList,  Integer.parseInt(strForecastMonth), strMonYear);
			gmTTPCalculations.organizeHeader((ArrayList)hmFinalValue.get("Header")
		   			                        , hmSelectedFilter, strLevelId, strLevelHierarchy
		   			                        , Integer.parseInt(strForecastMonth));
			
			hmFinalValue.put("STATUS", strStatus);
			hmFinalValue.put("SHEETNAME", alSheetName);
			hmFinalValue.put("FINALDATE", strFinalDate);
			hmFinalValue.put("TTPTYPE", strTTPType);
			hmFinalValue.put("DSSTATUS", strDSStatus);
			hmFinalValue.put("TTPCOLUMNTYPE",gmTTPCalculations.getTTPColumnType());
			hmFinalValue.put("FORECASTMONTH", strForecastMonth);
		
		return hmFinalValue;
	}
	private ArrayList parentNeedList(ArrayList alTTPData, ArrayList alParentSubList, int iAlOrdSize) {
		Iterator itDetails = alTTPData.iterator();
		Iterator itParentSubDetails = alParentSubList.iterator();
		ArrayList alPNeed = new ArrayList();
		HashMap hmTempDetails = new HashMap();
		HashMap hmAllTTPData = new HashMap();
		HashMap hmParentSubData = new HashMap();
		HashMap hmSubPartRowObj = new HashMap();
		HashMap hmParentPartRowObj = new HashMap();
		HashMap hmSubPartRowObjTemp = new HashMap();
		
		String strSubPart ="";
		String strParentPart ="";
		String strQty ="";
		String strPO = "";
		String strOrdQty = "";
		String strInv = "";
		String strTotReq = "";
		String strTempSubPart = "";
		String strTempNeed = "";
		String strUnitCost = "";
		
		int iAlNeedSize = 0;
		while (itDetails.hasNext()){
			hmAllTTPData = (HashMap) itDetails.next();
			hmTempDetails.put((String)hmAllTTPData.get("ID"),hmAllTTPData);
		}
		//log.debug("alParentSubList size = "+alParentSubList.size());
		//log.debug("hmTempDetails = "+hmTempDetails);
		while (itParentSubDetails.hasNext()){
			int iPNeed = 0;
			int iTotReq = 0;
			int iOrdQty = 0;
			int iNeedCnt = 0;
			double dbOrdCost = 0;
			hmParentSubData = (HashMap) itParentSubDetails.next();
			strSubPart = (String)hmParentSubData.get("SUBPART");
		    strParentPart = (String)hmParentSubData.get("PARENTPART");
		    strQty = (String)hmParentSubData.get("QTY");
		    
		    hmSubPartRowObj = GmCommonClass.parseNullHashMap((HashMap) hmTempDetails.get(strSubPart));
		    hmParentPartRowObj = GmCommonClass.parseNullHashMap((HashMap)hmTempDetails.get(strParentPart));
		    //From Parent Part
		    strPO = GmCommonClass.parseZero((String)hmParentPartRowObj.get("PO"));
		    strOrdQty = GmCommonClass.parseZero((String)hmParentPartRowObj.get("Order Qty"));
		    //From Subpart Part
		    strInv = GmCommonClass.parseZero((String)hmSubPartRowObj.get("Total Inventory"));
		    strTotReq = GmCommonClass.parseZero((String)hmSubPartRowObj.get("Total Req"));
		    strUnitCost = GmCommonClass.parseZero((String)hmSubPartRowObj.get("Unit Cost $"));
		    
		    iAlNeedSize = GmCommonClass.parseNullArrayList(alPNeed).size();
		    for(int k=0; k<iAlNeedSize; k++)
		    {
		    	iNeedCnt = 0;
		    	HashMap hmPneed = (HashMap)alPNeed.get(k);
		    	strTempSubPart = (String)hmPneed.get("ID");	
		    	if(strTempSubPart.equals(strSubPart))
		    	{
		    		iNeedCnt = 1;
		    		strTempNeed = (String)hmPneed.get("PNEED");
		    		iPNeed = Integer.parseInt(strTempNeed)+ (Integer.parseInt(strPO) + Integer.parseInt(strOrdQty)) * Integer.parseInt(strQty);
		    		hmPneed.put("PNEED", Integer.toString(iPNeed));
		    		iTotReq = (Integer.parseInt(strPO) + Integer.parseInt(strOrdQty)) * Integer.parseInt(strQty) + Integer.parseInt(strTotReq);
		    		break;
		    	}
		    }
		    if(iNeedCnt == 0)
		    {
		    	HashMap hmParentNeed = new HashMap();
		    	iPNeed = (Integer.parseInt(strPO) + Integer.parseInt(strOrdQty)) * Integer.parseInt(strQty);
		    	iTotReq = iPNeed + Integer.parseInt(strTotReq);	
		    	hmSubPartRowObjTemp = GmCommonClass.parseNullHashMap((HashMap) hmTempDetails.get(strSubPart));
		    	if(!hmSubPartRowObjTemp.isEmpty()){
			    	hmParentNeed.put("ID", strSubPart);
			    	hmParentNeed.put("PNEED", Integer.toString(iPNeed));		    			    
			    	alPNeed.add(hmParentNeed);
		    	}
		    }
		    iOrdQty = iTotReq - Integer.parseInt(strInv);
		    iOrdQty = iOrdQty < 0 ? 0: iOrdQty;
		    dbOrdCost = iOrdQty * Double.parseDouble(strUnitCost);
	    	hmSubPartRowObj.put("Total Req", Integer.toString(iTotReq));
	    	if (iAlOrdSize <= 0)
		    {
	    		hmSubPartRowObj.put("Order Qty", Integer.toString(iOrdQty));
	    		hmSubPartRowObj.put("Order Cost$", String.valueOf(dbOrdCost));
		    }
		}		
		log.debug("alPNeed = "+alPNeed);				
		return alPNeed;
	}
	/**
	 * getLinkInfo - This Method is used to to get the link object information
	 * 
	 * @return HashMap
	 * @exception AppError
	 */
	private HashMap getLinkInfo(String strType) {
		HashMap hmLinkDetails = new HashMap();
		HashMap hmapValue = new HashMap();
		ArrayList alLinkList = new ArrayList();

		// First parameter holds the link object name
		hmLinkDetails.put("LINKID", "ID");

		if (strType.equals("Inv")) {
			// Parameter to be added to link list
			hmapValue.put("KEY", GmTTPHeader.LINK_KEY_IN_STOCK);
			hmapValue.put("VALUE", GmTTPHeader.IN_STOCK);
			alLinkList.add(hmapValue);
			/*
			hmapValue = new HashMap();
			hmapValue.put("KEY", "PO DHR ");
			hmapValue.put("VALUE", "PO + DHR");
			alLinkList.add(hmapValue);
			*/
			
			hmapValue = new HashMap();
			hmapValue.put("KEY", GmTTPHeader.LINK_KEY_PO);
			hmapValue.put("VALUE",GmTTPHeader.PO);
			alLinkList.add(hmapValue);
			
			hmapValue = new HashMap();
			hmapValue.put("KEY", GmTTPHeader.LINK_KEY_DHR);
			hmapValue.put("VALUE",GmTTPHeader.DHR);
			alLinkList.add(hmapValue);
			
			hmapValue = new HashMap();
			hmapValue.put("KEY", GmTTPHeader.LINK_KEY_BUILD_SETS);
			hmapValue.put("VALUE",GmTTPHeader.BUILD_SETS);
			alLinkList.add(hmapValue);

			hmapValue = new HashMap();
			hmapValue.put("KEY", GmTTPHeader.LINK_KEY_TOTAL_INVENTORY);
			hmapValue.put("VALUE",GmTTPHeader.TOTAL_INVENTORY);
			alLinkList.add(hmapValue);
			
			hmapValue = new HashMap();
			hmapValue.put("KEY", GmTTPHeader.LINK_KEY_RW_INVENTORY);
			hmapValue.put("VALUE",GmTTPHeader.RW_INVENTORY);
			alLinkList.add(hmapValue);

			hmapValue = new HashMap();
			hmapValue.put("KEY", GmTTPHeader.LINK_KEY_QUAR_AVAIL);
			hmapValue.put("VALUE",GmTTPHeader.QUAR_AVAIL);
			alLinkList.add(hmapValue);
			
			hmapValue = new HashMap();
			hmapValue.put("KEY", GmTTPHeader.LINK_KEY_QUAR_ALLOC);
			hmapValue.put("VALUE",GmTTPHeader.QUAR_ALLOC);
			alLinkList.add(hmapValue);
			
			/*
			hmapValue = new HashMap();
			hmapValue.put("KEY", "Parent Part Qty");
			hmapValue.put("VALUE", "Parent Part Qty");
			alLinkList.add(hmapValue);
			*/
			hmLinkDetails.put("POSITION", "AFTER");
		} else if (strType.equals("TotQtyReq")) {
			// Parameter to be added to link list

			hmapValue = new HashMap();
			hmapValue.put("KEY", GmTTPHeader.LINK_KEY_FORECAST_QTY);
			hmapValue.put("VALUE",GmTTPHeader.FORECAST_QTY);
			alLinkList.add(hmapValue);
			
			hmapValue = new HashMap();
			hmapValue.put("KEY", GmTTPHeader.LINK_KEY_FORECAST_QTY_US);
			hmapValue.put("VALUE",GmTTPHeader.FORECAST_QTY_US);
			alLinkList.add(hmapValue);
			
			hmapValue = new HashMap();
			hmapValue.put("KEY", GmTTPHeader.LINK_KEY_FORECAST_QTY_OUS);
			hmapValue.put("VALUE",GmTTPHeader.FORECAST_QTY_OUS);
			alLinkList.add(hmapValue);
			
			hmapValue = new HashMap();
			hmapValue.put("KEY", GmTTPHeader.LINK_KEY_TPR);
			hmapValue.put("VALUE",GmTTPHeader.TPR);
			alLinkList.add(hmapValue);
			
			hmapValue = new HashMap();
			hmapValue.put("KEY", GmTTPHeader.LINK_KEY_TPR_US);
			hmapValue.put("VALUE", GmTTPHeader.TPR_US);
			alLinkList.add(hmapValue);
			
			hmapValue = new HashMap();
			hmapValue.put("KEY", GmTTPHeader.LINK_KEY_TPR_OUS);
			hmapValue.put("VALUE",GmTTPHeader.TPR_OUS);
			alLinkList.add(hmapValue);

			hmLinkDetails.put("POSITION", "AFTER");
		}

		else if (strType.equals("OrdQty")) {
			hmapValue = new HashMap();
			hmapValue.put("KEY", GmTTPHeader.LINK_KEY_ORDER_QTY);
			hmapValue.put("VALUE",GmTTPHeader.ORDER_QTY);
			alLinkList.add(hmapValue);

			hmapValue = new HashMap();
			hmapValue.put("KEY",GmTTPHeader.LINK_KEY_ORDER_COST );
			hmapValue.put("VALUE",GmTTPHeader.ORDER_COST);
			alLinkList.add(hmapValue);

			hmLinkDetails.put("POSITION", "AFTER");
		}

		else if (strType.equals("UnitPrice")) {
			// Parameter to be added to link list
			hmapValue = new HashMap();
			hmapValue.put("KEY", GmTTPHeader.LINK_KEY_UNIT_COST);
			hmapValue.put("VALUE",GmTTPHeader.UNIT_COST);
			alLinkList.add(hmapValue);
			
			hmapValue = new HashMap();
			hmapValue.put("KEY",GmTTPHeader.LINK_KEY_LEAD_TIME);
			hmapValue.put("VALUE",GmTTPHeader.LEAD_TIME_ATTRIBUTE);
			alLinkList.add(hmapValue);
			
			hmapValue = new HashMap();
			hmapValue.put("KEY", GmTTPHeader.LINK_KEY_CALC_LEAD_TIME);
			hmapValue.put("VALUE",GmTTPHeader.CALC_LEAD_TIME);
			alLinkList.add(hmapValue);
			
			hmLinkDetails.put("POSITION", "AFTER");
			hmLinkDetails.put("COLUMNNAME", GmTTPHeader.DESCRIPTION);
		}else if (strType.equals("AddInfo")) {
			// Parameter to be added to link list
			hmapValue = new HashMap();
			hmapValue.put("KEY",GmTTPHeader.LINK_KEY_SET_PAR);
			hmapValue.put("VALUE",GmTTPHeader.SET_PAR);
			alLinkList.add(hmapValue);
						
			hmapValue = new HashMap();
			hmapValue.put("KEY",GmTTPHeader.LINK_KEY_OTN);
			hmapValue.put("VALUE",GmTTPHeader.OTN);
			alLinkList.add(hmapValue);
			
			hmapValue = new HashMap();
			hmapValue.put("KEY",GmTTPHeader.LINK_KEY_OTN_US);
			hmapValue.put("VALUE",GmTTPHeader.OTN_US);
			alLinkList.add(hmapValue);
			
			hmapValue = new HashMap();
			hmapValue.put("KEY",GmTTPHeader.LINK_KEY_OTN_OUS);
			hmapValue.put("VALUE", GmTTPHeader.OTN_OUS);
			alLinkList.add(hmapValue);
			
			hmLinkDetails.put("POSITION", "AFTER");			
		} else if (strType.equals("OrderQty")) {
			// Parameter to be added to link list
			// Parameter to be added to link list
			hmapValue.put("KEY", GmTTPHeader.LINK_KEY_QTY);
			hmapValue.put("VALUE", GmTTPHeader.ORDER_QTY);
			alLinkList.add(hmapValue);

			hmapValue = new HashMap();
			hmapValue.put("KEY", GmTTPHeader.LINK_KEY_TOTAL_COST);
			hmapValue.put("VALUE",GmTTPHeader.ORDER_COST);
			alLinkList.add(hmapValue);

			hmLinkDetails.put("POSITION", "AFTER");
			
		}
		else if(strType.equals("PNeed")) {
			hmapValue.put("KEY", GmTTPHeader.LINK_KEY_PARENT_NEED);
			hmapValue.put("VALUE", GmTTPHeader.PARENT_NEED);
			alLinkList.add(hmapValue);
			hmLinkDetails.put("POSITION", "BEFORE");
			hmLinkDetails.put("COLUMNNAME", GmTTPHeader.TPR);
		}
		
		hmLinkDetails.put("LINKVALUE", alLinkList);

		return hmLinkDetails;
	}

	/*
	 * This method is used to calculate the order pricing based on order qty
	 * 
	 */
	private ArrayList adjustmentList(ArrayList alDetails, ArrayList alTotQtyOrder) {
		Iterator itDetails = alDetails.iterator();
		ArrayList alTemp = new ArrayList();
		HashMap hmTempDetails = new HashMap();
		String strOrdQty = "0";
		String strOrdCost = "0";
		int intGroupingCount = 1;
		int intTotalRequired = 0;
		int intTotalInventory = 0;
		int intOrdQty = 0;
		double dblOrdCost = 0.0;
		double dblUnitPrice = 0.0;
		double dblTotalL2 = 0.0;
		double dblTotalL3 = 0.0;
		int intOrdQtyL2 = 0;
		int intOrdQtyL3 = 0;

		if (alTotQtyOrder.size() == 0) {
			while (itDetails.hasNext()) {
				hmTempDetails = (HashMap) itDetails.next();

				intTotalRequired = Integer.parseInt(GmCommonClass.parseZero((String) hmTempDetails.get("Total Req")));
				intTotalInventory = Integer.parseInt(GmCommonClass.parseZero((String) hmTempDetails.get("Total Inventory")));
				//intTotalInventory += Integer.parseInt(GmCommonClass.parseZero((String) hmTempDetails.get("Parent Part Qty")));
				intGroupingCount = Integer.parseInt(GmCommonClass.parseZero((String) hmTempDetails.get("GROUPINGCOUNT")));
				
				if (intTotalInventory > intTotalRequired && intGroupingCount == 0) {
					strOrdQty = "0";
					strOrdCost = "0";
				} else {
					intOrdQty = intTotalRequired - intTotalInventory;
					dblUnitPrice = Double.parseDouble(GmCommonClass.parseZero((String) hmTempDetails.get("Unit Cost $")));
					if (intGroupingCount == 0) {
						dblOrdCost = intOrdQty * dblUnitPrice;
						dblTotalL2 = dblTotalL2 + dblOrdCost;
						intOrdQtyL2 = intOrdQtyL2 + intOrdQty;
						strOrdCost = String.valueOf(dblOrdCost);
						strOrdQty = String.valueOf(intOrdQty);
					} else if (intGroupingCount == 1) {
						// log.debug(" total 2 is " + dblTotalL2);
						dblTotalL3 = dblTotalL2 + dblTotalL3;
						intOrdQtyL3 = intOrdQtyL2 + intOrdQtyL3;
						strOrdCost = String.valueOf(dblTotalL2);
						strOrdQty = String.valueOf(intOrdQtyL2);
						dblTotalL2 = 0.0;
						intOrdQtyL2 = 0;
					} else if (intGroupingCount == 3) {
						strOrdCost = String.valueOf(dblTotalL3);
						strOrdQty = String.valueOf(intOrdQtyL3);
					}
				}
				HashMap hmTemp = new HashMap();
				hmTemp.put("ID", hmTempDetails.get("ID"));
				hmTemp.put("ORDQTY", strOrdQty);
				hmTemp.put("ORDCOST", strOrdCost);
				alTemp.add(hmTemp);
			}
		}
		return alTemp;
	}

    /**
     * executeJob - This method will execute the job for the specified demand
     * sheet
     */
    public void executeJob(String strDemandSheetId,HashMap hmParam) throws AppError {
    	GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
    	gmDBManager.setPrepareString("gm_pkg_op_ld_demand.gm_op_sav_load_detail", 3);
    	gmDBManager.setString(1, strDemandSheetId);
    	gmDBManager.setInt(2,91951);  //Hard coded Ref_Type
    	gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("USERID")));
       	gmDBManager.execute();
       	gmDBManager.commit();
    }


	public ArrayList fetchTTPForecastRequest(HashMap hmParam) throws AppError {
		RowSetDynaClass rdResult = null;
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		gmDBManager.setPrepareString("Gm_pkg_oppr_ttp_summary.gm_fc_fch_request", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETIDS")));
		gmDBManager.execute();
		rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return (ArrayList) rdResult.getRows();
	}

    public HashMap fetchTPRReport(HashMap hmParam) throws AppError
    {
   	 log.debug("Enter");
     	HashMap hmReturn = new HashMap();
     	RowSetDynaClass rdResult = null;    	 
        GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
        gmDBManager.setPrepareString("Gm_pkg_oppr_ttp_summary.gm_fc_fch_tprdetails",12);
        gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
        gmDBManager.registerOutParameter(9, OracleTypes.CURSOR);
        gmDBManager.registerOutParameter(10, OracleTypes.VARCHAR);
        gmDBManager.registerOutParameter(11, OracleTypes.CURSOR);
        gmDBManager.registerOutParameter(12, OracleTypes.CURSOR);
        
        String demandsheetid = GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETID"));
        
        gmDBManager.setString(1, GmCommonClass.parseNull((String)hmParam.get("PNUM")));
        
        gmDBManager.setString(2, GmCommonClass.parseNull((String)hmParam.get("INVENTORYID")));       
        gmDBManager.setString(3, GmCommonClass.parseNull((String)hmParam.get("PSTATUS")));
        gmDBManager.setString(4, GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETMONTHID"))); //demand sheet id
        gmDBManager.setString(5, GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETID"))); //demand master id
        gmDBManager.setString(6, GmCommonClass.parseNull((String)hmParam.get("SETID")));
        gmDBManager.setString(7, GmCommonClass.parseNull((String)hmParam.get("TYPE")));
        log.debug("hmParam is "+ hmParam);
        gmDBManager.execute();
        
        rdResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(8));
        
        hmReturn.put("TPRREPORT", rdResult.getRows());
        rdResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(9));
        hmReturn.put("TPRVOIDREPORT", rdResult.getRows());         
        hmReturn.put("PDESC",gmDBManager.getString(10));
        rdResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(11));
        hmReturn.put("TPRSALESREPORT",rdResult.getRows());
        
        rdResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(12));
        hmReturn.put("TPRLOANERBOREPORT",rdResult.getRows());
        
        gmDBManager.close();
        log.debug("Exit");
        return hmReturn;
    }

	/**
	 * fetchTemplates - This method will fetch all the Active Templates	 * 
	 * @return ArrayList
	 * @exception AppError , Exception
	 */
	 public ArrayList fetchTemplates() throws AppError,Exception {		
		ArrayList alTemplateList = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		gmDBManager.setPrepareString("gm_pkg_oppr_dmd_mas_rpt.gm_fch_templates", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute(); 
		alTemplateList =  gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();		
		return alTemplateList;
	} // End of fetchTemplates()
	/**
	 * fetchGrowthComments - This method will fetch the Growth Comments for a Particular Part* 
	 * @param HashMap - hmParam
	 * @return RowSetDynaClass
	 * @exception AppError , Exception
	 */
	public RowSetDynaClass fetchGrowthComments(HashMap hmParam) throws AppError,Exception {
		RowSetDynaClass rdResult = null;
		String strPartNum 		 = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBERS"));
		String strDemandMonthId  = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETMONTHID"));
		String strDemandSheetId  = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETID"));
		String strMonthYear   	 = GmCommonClass.parseNull((String) hmParam.get("MONTHYEAR"));
		GmDBManager gmDBManager  = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		gmDBManager.setPrepareString("gm_pkg_oppr_sheet_summary.gm_fch_grwthComment", 5);
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		gmDBManager.setString(1, strPartNum);
		gmDBManager.setString(2, strDemandMonthId);
		gmDBManager.setString(3, strDemandSheetId);
		gmDBManager.setString(4, strMonthYear);
		gmDBManager.execute();
		rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(5));
		gmDBManager.close();
		return rdResult;
	} // End of fetchGrowthComments()
	
	 /**
     * fetchAggregatePartDrillDown - This Method is used to get crosstab details for Part
     * @param v  
     * @return HashMap
     * @exception AppError
   **/
	
    public HashMap fetchAggregatePartDrillDown(HashMap hmParam) throws AppError
    {         
	    HashMap hmReturn = new HashMap();
	    ArrayList alForecastHeader = new ArrayList();
	      
	    ResultSet rsHeader = null;
	    ResultSet rsDetailsExpected = null;      
	    GmCrossTabReport gmCrossReport = new GmCrossTabReport();
	    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS); 
	      
	    String strDemandSheetId = GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETMONTHID")); 
	    String strGroupInfo = GmCommonClass.parseNull((String)hmParam.get("GROUPINFO"));
	    String strPartNums = GmCommonClass.parseNull((String)hmParam.get("PARTNUMBERS")); 
	    String strForeCastMonths = GmCommonClass.parseNull((String)hmParam.get("FORECASTMONTHS")); 
	    String strGroupID = GmCommonClass.parseNull((String)hmParam.get("GROUPID"));
	    String strDrilldownType = GmCommonClass.parseNull((String)hmParam.get("DRILLDOWNTYPE"));
	    
	    if(strGroupID.equals(""))
	    	strGroupID = strGroupInfo;
	    
	    log.debug("hmParam>>>>.>: "+ hmParam);    
	    // If no sheet id then return null 
	    if (strDemandSheetId.equals(""))
	    {
	        return hmReturn;
	    }
	     
	    // To fetch the transaction information
	    gmDBManager.setPrepareString("gm_pkg_oppr_sheet_summary.gm_fch_aggregatepart_drilldown",8);
	    gmDBManager.registerOutParameter(7,OracleTypes.CURSOR);
	    gmDBManager.registerOutParameter(8,OracleTypes.CURSOR);      
	    gmDBManager.setString(1,strDemandSheetId);
	    gmDBManager.setString(2,strGroupInfo);
	    gmDBManager.setString(3,strPartNums); 
	    gmDBManager.setInt(4,Integer.parseInt(strForeCastMonths));
	    gmDBManager.setString(5,strGroupID);
	    gmDBManager.setString(6,strDrilldownType);
	    gmDBManager.execute();        
	    
	    rsHeader = (ResultSet)gmDBManager.getObject(7);
	    rsDetailsExpected = (ResultSet)gmDBManager.getObject(8);       
	    gmCrossReport.setGroupFl(true);
	    gmCrossReport.setOverRideFl(true);
	    gmCrossReport.setRowOverRideFl(true);
	    gmCrossReport.setFixedColumn(true); 
	    gmCrossReport.setAcceptExtraColumn(true); 
	    gmCrossReport.setExtraColumnParams(4,"");
	    gmCrossReport.setExtraColumnParams(7,"HISTORY_FL");
	    gmCrossReport.setExtraColumnParams(8,"COMM_FL");
	    gmCrossReport.setFixedParams(10,"PDESC"); 
	    
	    hmReturn = gmCrossReport.GenerateCrossTabReport(rsHeader,rsDetailsExpected);
	    log.debug("hmReturn: "+ hmReturn);   
	 
	    gmDBManager.close();
	     
	    return hmReturn;
    }

	/**Method:fetchTTPNewSummaryDtls:This method used to fetch summary details for new summary tab
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public ArrayList fetchTTPNewSummaryDtls(HashMap hmParam) throws AppError {
		ArrayList alFetchTTPNewList = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		//Parameter values
		String strLevelId = GmCommonClass.parseZero((String) hmParam.get("LEVELID"));
		// Level ID zero then - its a multi parts - So setting the default level id (Globus medical)
		//strLevelId = strLevelId.equals("0") ? "100800" : strLevelId;
		
		String strMonth =  GmCommonClass.parseNull((String) hmParam.get("MONTHID"));
		String strYear =  GmCommonClass.parseNull((String) hmParam.get("YEARID"));
		String strMonthYear = strMonth.concat("/").concat(strYear);
		//call package
		gmDBManager.setPrepareString("gm_pkg_oppr_ttp_summary.gm_fch_new_ttp_summary_dtls", 8);
		gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
		//set parameters
		gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("TTPID")));
		gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("TTPNAME")));
		gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETIDS")));
		gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParam.get("INVENTORYID")));
		gmDBManager.setString(5, strLevelId);
		//gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParam.get("COMPANYID")));
		
		gmDBManager.setString(6, GmCommonClass.parseZero((String) hmParam.get("TTPFORECASTMONTHS")));
		//gmDBManager.setString(7, GmCommonClass.parseNull((String) hmParam.get("MONTHID")));
		//gmDBManager.setString(8, GmCommonClass.parseNull((String) hmParam.get("YEARID")));
	    gmDBManager.setString(7, strMonthYear);
		gmDBManager.execute();
		//Get summary details using arraylist
		alFetchTTPNewList = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(8)));
		gmDBManager.close();
		return alFetchTTPNewList;
	}
    
}
