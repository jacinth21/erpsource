package com.globus.operations.purchasing.orderplanning.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmAutoCompleteTransBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.purchasing.orderplanning.beans.GmOPTTPSetupBean;
import com.globus.operations.purchasing.orderplanning.forms.GmOPTTPGroupingForm;

public class GmOPTTPGroupingAction extends GmAction
{


    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
        try {
        GmOPTTPGroupingForm gmOPTTPGroupingForm = (GmOPTTPGroupingForm)form;
        gmOPTTPGroupingForm.loadSessionParameters(request);
        GmOPTTPSetupBean gmOPTTPSetupBean = new GmOPTTPSetupBean();
        GmLogonBean gmLogonBean = new GmLogonBean();
        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

        String strOpt = gmOPTTPGroupingForm.getStrOpt();
        String strTTPId = gmOPTTPGroupingForm.getTtpId();
        HashMap hmDemandSheet = new HashMap();   
        HashMap hmTemp = new HashMap();
        HashMap hmParam = new HashMap();
        HashMap hmValues = new HashMap();
        ArrayList alUnSelectedDS = new ArrayList();
        ArrayList alSelectedDS = new ArrayList();
        ArrayList alTTPList = new ArrayList();
        ArrayList alTTPOwner = new ArrayList();
        log.debug("strOpt is " + strOpt);
        
        if (strOpt.equals("save"))
        {
            hmParam = GmCommonClass.getHashMapFromForm(gmOPTTPGroupingForm);
            String strUnSelectedDS  = GmCommonClass.createInputString(gmOPTTPGroupingForm.getCheckUnSelectedDS());
            String strSelectedDS = GmCommonClass.createInputString(gmOPTTPGroupingForm.getCheckSelectedDS());
            String strComma = "";
            
            log.debug(" The values of strUnSelectedGroup "+strUnSelectedDS);
            if (strUnSelectedDS.length() > 0 && strSelectedDS.length() > 0 ){
                strComma = ",";
            }
            
            String strGroupInputString = strUnSelectedDS.concat(strComma).concat(strSelectedDS);
            hmParam.put("INPUTSTRING",strGroupInputString);
            
            log.debug(" Group Input String is  " + strGroupInputString);
            strTTPId =  gmOPTTPSetupBean.saveTTPGrouping(hmParam);
         // JMS call to update the values to cache server
            GmAutoCompleteTransBean gmAutoCompleteTransBean =
                new GmAutoCompleteTransBean(GmCommonClass.getDefaultGmDataStoreVO());
            HashMap hmCacheUpd = new HashMap();
            // to get the values
            String strNewTTPName = GmCommonClass.parseNull((String) hmParam.get("TTPNAME"));
            String strOldTTPName = GmCommonClass.parseNull((String) hmParam.get("HOLD_TTPNAME"));
            // set the values
            hmCacheUpd.put("ID", strTTPId);
            hmCacheUpd.put("OLD_NM", strOldTTPName);
            hmCacheUpd.put("NEW_NM", strNewTTPName);
            hmCacheUpd.put("METHOD", "synTTPList");
            hmCacheUpd.put("STROPT", "GOP");         
            hmCacheUpd.put("companyInfo", GmCommonClass.parseNull(request.getParameter("companyInfo")));
            gmAutoCompleteTransBean.saveDetailsToCache(hmCacheUpd);
        }
        log.debug(" strTTPId is " +strTTPId);
        if (!strTTPId.equals(""))
        {
            hmValues = gmOPTTPSetupBean.fetchEditTTPGroupMap(strTTPId);
            gmOPTTPGroupingForm = (GmOPTTPGroupingForm)GmCommonClass.getFormFromHashMap(gmOPTTPGroupingForm,hmValues);
        }
        
        hmDemandSheet = gmOPTTPSetupBean.fetchTTPGroupMap(strTTPId);
        
        alUnSelectedDS = GmCommonClass.parseNullArrayList((ArrayList)hmDemandSheet.get("ALUNSELECTED"));
        alSelectedDS = GmCommonClass.parseNullArrayList((ArrayList)hmDemandSheet.get("ALSELECTED")); 
        alTTPList = gmOPTTPSetupBean.loadTTPList(hmTemp);
        alTTPOwner = gmLogonBean.getEmployeeList("300","W");
        log.debug(" value in hmDemandSheet " + hmDemandSheet);

        gmOPTTPGroupingForm.setAlCategory(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("TTPCAT",getGmDataStoreVO())));//PMT-49826-Call the Code lookup method to get values from:TTPCAT
        
        gmOPTTPGroupingForm.setAlTTPOwner(alTTPOwner);
        gmOPTTPGroupingForm.setAlUnSelectedDS(alUnSelectedDS);
        gmOPTTPGroupingForm.setAlSelectedDS(alSelectedDS); 
        
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
        return mapping.findForward("success");
    }
}
