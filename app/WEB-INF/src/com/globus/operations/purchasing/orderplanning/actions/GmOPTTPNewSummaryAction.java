package com.globus.operations.purchasing.orderplanning.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.beans.GmCalenderOperations;

import java.util.GregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.globus.operations.purchasing.orderplanning.beans.GmOPDemandReportBean;
import com.globus.operations.purchasing.orderplanning.forms.GmOPTTPNewSummaryForm;

public class GmOPTTPNewSummaryAction extends GmDispatchAction {
	 Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
	 
	/**This method used to load the new TTP summary template.Once click the "New Summary" tab - this method to be called.
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward loadTTPNewSummaryDtls(ActionMapping actionMapping, ActionForm form,
 HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);

		GmOPTTPNewSummaryForm gmOPTTPNewSummaryForm = (GmOPTTPNewSummaryForm) form;
		gmOPTTPNewSummaryForm.loadSessionParameters(request);
		GmOPDemandReportBean gmOPDemandReportBean = new GmOPDemandReportBean();

		// Initialize
		ArrayList alReturn = new ArrayList();
		HashMap hmApplnParam = new HashMap();
		HashMap hmParam = new HashMap();
		HashMap hmValues = new HashMap();
		//Decalare variables
		String strxmlGridData = "";
		String strSessCompanyLocale = "";
		String strApplnDateFmt = "";
		String strMonth = "";
		String strYear = "";
		String strLevelId = "";
		String strAccessId="";
		String strLevelFl = "";
		int monthLabel = 4;
		//Get HashMap Values
		hmParam = GmCommonClass.getHashMapFromForm(gmOPTTPNewSummaryForm);
		

		//Get current month and current year to get next four month and year values
		strMonth = GmCommonClass.parseZero((String) hmParam.get("MONTHID"));
		strYear = GmCommonClass.parseZero((String) hmParam.get("YEARID"));
		strLevelId = GmCommonClass.parseZero((String) hmParam.get("LEVELID"));
		
		//call getMonthLabelDtls
		hmApplnParam = getMonthLabelDtls(strMonth, strYear,monthLabel);

		strSessCompanyLocale = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
		strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
		//Get Summary details
		alReturn = GmCommonClass.parseNullArrayList(gmOPDemandReportBean.fetchTTPNewSummaryDtls(hmParam));

		
		//Get access Id and set to form
        strAccessId = gmOPDemandReportBean.fetchTTPAccessType(hmParam);
		gmOPTTPNewSummaryForm.setAccessId(strAccessId);
		
		strLevelFl = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strLevelId,"ALL_TTP_COLUMNS"));
		gmOPTTPNewSummaryForm.setLevelFl(strLevelFl);

		hmApplnParam.put("SESSDATEFMT", strApplnDateFmt);
		hmApplnParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
		hmApplnParam.put("ALRETURN", alReturn);
		hmApplnParam.put("TEMPNAME", "GmOPTTPNewSummary.vm");
		hmApplnParam.put("TEMPPATH","operations/purchasing/orderplanning/templates");
		hmApplnParam.put("RESOURCEBNDLPATH","properties.labels.operations.purchasing.GmOPTTPNewSummary");
		//Get Grid data set to String 
		String strXmlData = getXmlGridData(alReturn, hmApplnParam);
		gmOPTTPNewSummaryForm.setGridData(strXmlData);

		return actionMapping.findForward("gmOPTTPNewSummary");
	}
	
	/**This method used to generate the XML based on ArrayList values.
	 * @param alResult
	 * @param hmApplnParam
	 * @return
	 * @throws AppError
	 */
	public String getXmlGridData(ArrayList alResult, HashMap hmApplnParam)throws AppError {

		GmTemplateUtil templateUtil = new GmTemplateUtil();
		String strSessCompanyLocale = GmCommonClass.parseNull((String) hmApplnParam.get("STRSESSCOMPANYLOCALE"));
		String strTempletPath = GmCommonClass.parseNull((String) hmApplnParam.get("TEMPPATH"));
		String strTempletName = GmCommonClass.parseNull((String) hmApplnParam.get("TEMPNAME"));
		String strResourceBdlePath = GmCommonClass.parseNull((String) hmApplnParam.get("RESOURCEBNDLPATH"));
		templateUtil.setDataList("alResult", alResult);
		templateUtil.setDataMap("hmApplnParam", hmApplnParam);
		templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strResourceBdlePath, strSessCompanyLocale));
		templateUtil.setTemplateSubDir(strTempletPath);
		templateUtil.setTemplateName(strTempletName);
		return templateUtil.generateOutput();

	}
	
	/**This method used to get the 4 month label information and returns to Hashmap
	 * Passing current month and current year as parameter to get next four month and year values.
	 * @param strMonth
	 * @param strYear
	 * @return
	 * @throws AppError
	 */
	public HashMap getMonthLabelDtls(String strMonth, String strYear, int monthLabel)
			throws AppError {

		HashMap hmMonth = new HashMap();

		SimpleDateFormat month_date = new SimpleDateFormat("MMM yy");
		int intToyear = Integer.parseInt(strYear);
		int intToMonth = Integer.parseInt(strMonth);

		int month1 = intToMonth - 1;
		int day1 = 01;
		for (int i = 0; i < monthLabel; i++) {
			Calendar calendar = new GregorianCalendar(intToyear, month1, day1);
			calendar.add(Calendar.MONTH, i);
			String month_name = month_date.format(calendar.getTime());
			hmMonth.put("MONTH" + (i + 1), month_name);
		}
		return hmMonth;

	}
}