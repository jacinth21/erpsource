package com.globus.operations.purchasing.orderplanning.forms;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.validator.routines.DateValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmCommonForm;

public class GmOPDemandSheetSummaryForm extends GmCommonForm
{

    private String demandSheetId = "";
    private String demandSheetMonthId = "";
    private String monthId = GmCalenderOperations.getCurrentMM();
    private String yearId = String.valueOf(GmCalenderOperations.getCurrentYear());
    private String [] checkedGroupId = new String[100];
    private String checkedGroupsString = "";
    private String partNumbers = "";
    private String overrideFlag = "";
    private String demandPeriod = "";
    private String forecastPeriod = "";
    private String loadDate = "";
    private String status = "";
    private String statusId = "";
    private String approvedby = "";
    private String approveddate = "";
    private String appcomments = "";
    private String groupInfo = "";
    private String demandTypeNm = "";
    private String demandTypeId = "";
    private String unitrprice = "Unit";
    private String forecastMonths = "4";
    private String hstatus = "";
    private String requestPeriod = "";
    private String refId = "";
    private String checkSetDtl = "";
    private String checkDesc = "";
    private String hideSheetCommentsTab = "";
    private String hideMonthlyCommentsTab = "";
    private String hideGrowthTab = "";
    private String hideApprovalTab = "";
    private String accessType = "";
    private String levelID = "";
    private String strXmlString="";
    private String month = "";
    private String monthYear = "";
	private String xmlGridData  = "";
	private String growthDrillDown = "";
	private String setID = "";
	private String growthDrillDownMonth = "";
	private String searchdemandSheetId = "";
	private String ttpId = "";
	 
	/**
	 * 
	 */
	private String groupID = "";
	private String drillDownType = "";
       
    public String getGroupID() {
		return groupID;
	}
	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}
	private ArrayList alTPRReport = new ArrayList();
    private ArrayList alTPRVoidReport = new ArrayList();
    
    private HashMap hmDemandSheetDetail = new HashMap();
    private HashMap hmSubPartDetail = new HashMap();
    private HashMap hmParentPartDetail = new HashMap();
    private HashMap hmPartDetail = new HashMap();
    
    private HashMap hmAggrPartDetail = new HashMap();
    
    private ArrayList alMonth = new ArrayList();
    private ArrayList alYear = new ArrayList();
    private ArrayList alGroupList = new ArrayList();
    private ArrayList alPartList = new ArrayList();
    private ArrayList alDemandSheetList = new ArrayList();
    
    private ArrayList alRegionList = new ArrayList();
    private List ldtResult = null;
    private List ldCommentList = new ArrayList();
    
    private ArrayList alRequest = new ArrayList();
    
    private ArrayList alForeastGrowth = new ArrayList();
    
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    
    /**
     * @return Returns the ldtResult.
     */
    public List getLdtResult()
    {
        return ldtResult;
    }
    /**
     * @param ldtResult The ldtResult to set.
     */
    public void setLdtResult(List ldtResult)
    {
        this.ldtResult = ldtResult;
    }
    /**
     * @return Returns the overrideFlag.
     */
    public String getOverrideFlag()
    {
        return overrideFlag;
    }
    /**
     * @param overrideFlag The overrideFlag to set.
     */
    public void setOverrideFlag(String overrideFlag)
    {
        this.overrideFlag = overrideFlag;
    }
    /**
     * @return Returns the alGroupList.
     */
    public ArrayList getAlGroupList()
    {
        return alGroupList;
    }
    /**
     * @param alGroupList The alGroupList to set.
     */
    public void setAlGroupList(ArrayList alGroupList)
    {
        this.alGroupList = alGroupList;
    }
    /**
     * @return Returns the alMonth.
     */
    public ArrayList getAlMonth()
    {
        return alMonth;
    }
    /**
     * @param alMonth The alMonth to set.
     */
    public void setAlMonth(ArrayList alMonth)
    {
        this.alMonth = alMonth;
    }
    /**
     * @return Returns the alYear.
     */
    public ArrayList getAlYear()
    {
        return alYear;
    }
    /**
     * @param alYear The alYear to set.
     */
    public void setAlYear(ArrayList alYear)
    {
        this.alYear = alYear;
    }
   
    /**
     * @return Returns the checkedGroupId.
     */
    public String[] getCheckedGroupId()
    {
        return checkedGroupId;
    }
    /**
     * @param checkedGroupId The checkedGroupId to set.
     */
    public void setCheckedGroupId(String[] checkedGroupId)
    {
        this.checkedGroupId = checkedGroupId;
    }
    /**
     * @return Returns the demandSheetId.
     */
    public String getDemandSheetId()
    {
        return demandSheetId;
    }
    /**
     * @param demandSheetId The demandSheetId to set.
     */
    public void setDemandSheetId(String demandSheetId)
    {
        this.demandSheetId = demandSheetId;
    }
    /**
     * @return Returns the monthId.
     */
    public String getMonthId()
    {
        return monthId;
    }
    /**
     * @param monthId The monthId to set.
     */
    public void setMonthId(String monthId)
    {
        this.monthId = monthId;
    }
    
    /**
     * @return Returns the partNumbers.
     */
    public String getPartNumbers()
    {
        return partNumbers;
    }
    /**
     * @param partNumbers The partNumbers to set.
     */
    public void setPartNumbers(String partNumbers)
    {
        this.partNumbers = partNumbers;
    }
    /**
     * @return Returns the yearId.
     */
    public String getYearId()
    {
        return yearId;
    }
    /**
     * @param yearId The yearId to set.
     */
    public void setYearId(String yearId)
    {
        this.yearId = yearId;
    }
    /**
     * @return Returns the alDemandSheetList.
     */
    public ArrayList getAlDemandSheetList()
    {
        return alDemandSheetList;
    }
    /**
     * @param alDemandSheetList The alDemandSheetList to set.
     */
    public void setAlDemandSheetList(ArrayList alDemandSheetList)
    {
        this.alDemandSheetList = alDemandSheetList;
    }
    /**
     * @return Returns the demandPeriod.
     */
    public String getDemandPeriod()
    {
        return demandPeriod;
    }
    /**
     * @param demandPeriod The demandPeriod to set.
     */
    public void setDemandPeriod(String demandPeriod)
    {
        this.demandPeriod = demandPeriod;
    }
    /**
     * @return Returns the forecastPeriod.
     */
    public String getForecastPeriod()
    {
        return forecastPeriod;
    }
    /**
     * @param forecastPeriod The forecastPeriod to set.
     */
    public void setForecastPeriod(String forecastPeriod)
    {
        this.forecastPeriod = forecastPeriod;
    }
    /**
     * @return Returns the loadDate.
     */
    public String getLoadDate()
    {
        return loadDate;
    }
    /**
     * @param loadDate The loadDate to set.
     */
    public void setLoadDate(String loadDate)
    {
        this.loadDate = loadDate;
    }
    /**
     * @return Returns the status.
     */
    public String getStatus()
    {
        return status;
    }
    /**
     * @param status The status to set.
     */
    public void setStatus(String status)
    {
        this.status = status;
    }
    /**
     * @return Returns the statusId.
     */
    public String getStatusId()
    {
        return statusId;
    }
    /**
     * @param statusId The statusId to set.
     */
    public void setStatusId(String statusId)
    {
        this.statusId = statusId;
    }
    /**
     * @return Returns the demandSheetMonthId.
     */
    public String getDemandSheetMonthId()
    {
        return demandSheetMonthId;
    }
    /**
     * @param demandSheetMonthId The demandSheetMonthId to set.
     */
    public void setDemandSheetMonthId(String demandSheetMonthId)
    {
        this.demandSheetMonthId = demandSheetMonthId;
    }
    /**
     * @return Returns the alPartList.
     */
    public ArrayList getAlPartList() {
        return alPartList;
    }
    /**
     * @param alPartList The alPartList to set.
     */
    public void setAlPartList(ArrayList alPartList) {
        this.alPartList = alPartList;
    }
    /**
     * @return Returns the appcomments.
     */
    public String getAppcomments() {
        return appcomments;
    }
    /**
     * @param appcomments The appcomments to set.
     */
    public void setAppcomments(String appcomments) {
        this.appcomments = appcomments;
    }
    /**
     * @return Returns the approvedby.
     */
    public String getApprovedby() {
        return approvedby;
    }
    /**
     * @param approvedby The approvedby to set.
     */
    public void setApprovedby(String approvedby) {
        this.approvedby = approvedby;
    }
    /**
     * @return Returns the approveddate.
     */
    public String getApproveddate() {
        return approveddate;
    }
    /**
     * @param approveddate The approveddate to set.
     */
    public void setApproveddate(String approveddate) {
        this.approveddate = approveddate;
    }
    /**
     * @return Returns the alDemandSheetDetail.
     */
    public HashMap getHmDemandSheetDetail() {
        return hmDemandSheetDetail;
    }
    /**
     * @param alDemandSheetDetail The alDemandSheetDetail to set.
     */
    public void setHmDemandSheetDetail(HashMap hmDemandSheetDetail) {
        this.hmDemandSheetDetail = hmDemandSheetDetail;
    }
    /**
     * @return Returns the groupInfo.
     */
    public String getGroupInfo()
    {
        return groupInfo;
    }
    /**
     * @param groupInfo The groupInfo to set.
     */
    public void setGroupInfo(String groupInfo)
    {
        this.groupInfo = groupInfo;
    }
    
	/**
	 * @return Returns the demandTypeId.
	 */
	public String getDemandTypeId() {
		return demandTypeId;
	}
	/**
	 * @param demandTypeId The demandTypeId to set.
	 */
	public void setDemandTypeId(String demandTypeId) {
		this.demandTypeId = demandTypeId;
	}
	/**
	 * @return Returns the demandTypeNm.
	 */
	public String getDemandTypeNm() {
		return demandTypeNm;
	}
	/**
	 * @param demandTypeNm The demandTypeNm to set.
	 */
	public void setDemandTypeNm(String demandTypeNm) {
		this.demandTypeNm = demandTypeNm;
	}
	/**
	 * @param Returns the alRegionList.
	 */
	public ArrayList getAlRegionList() {
		return alRegionList;
	}
	/**
	 * @param alRegionList the RegionList to set
	 */
	public void setAlRegionList(ArrayList alRegionList) {
		this.alRegionList = alRegionList;
	}
	public String getCheckedGroupsString() {
		return checkedGroupsString;
	}
	public void setCheckedGroupsString(String checkedGroupsString) {
		this.checkedGroupsString = checkedGroupsString;
	}
	public String getUnitrprice() {		
		
		return unitrprice;
	}
	public void setUnitrprice(String unitrprice) {
		this.unitrprice = unitrprice;
	}
	public String getForecastMonths() {
		return forecastMonths;
	}
	public void setForecastMonths(String forecastMonths) {
		this.forecastMonths = forecastMonths;
	}
	public String getHstatus() {
		return hstatus;
	}
	public void setHstatus(String hstatus) {
		this.hstatus = hstatus;
	}
	public String getRequestPeriod() {
		return requestPeriod;
	}
	public void setRequestPeriod(String requestPeriod) {
		this.requestPeriod = requestPeriod;
	}
	public ArrayList getAlRequest() {
		return alRequest;
	}
	public void setAlRequest(ArrayList alRequest) {
		this.alRequest = alRequest;
	}
	public ArrayList getAlForeastGrowth() {
		return alForeastGrowth;
	}
	public void setAlForeastGrowth(ArrayList alForeastGrowth) {
		this.alForeastGrowth = alForeastGrowth;
	}
	public String getRefId() {
		return refId;
	}
	public void setRefId(String refId) {
		this.refId = refId;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public ArrayList getAlTPRReport() {
		return alTPRReport;
	}
	public void setAlTPRReport(ArrayList alTPRReport) {
		this.alTPRReport = alTPRReport;
	}
	public ArrayList getAlTPRVoidReport() {
		return alTPRVoidReport;
	}
	public void setAlTPRVoidReport(ArrayList alTPRVoidReport) {
		this.alTPRVoidReport = alTPRVoidReport;
	}
	public HashMap getHmSubPartDetail() {
		return hmSubPartDetail;
	}
	public void setHmSubPartDetail(HashMap hmSubPartDetail) {
		this.hmSubPartDetail = hmSubPartDetail;
	}
	public HashMap getHmParentPartDetail() {
		return hmParentPartDetail;
	}
	public void setHmParentPartDetail(HashMap hmParentPartDetail) {
		this.hmParentPartDetail = hmParentPartDetail;
	}
	public HashMap getHmPartDetail() {
		return hmPartDetail;
	}
	public void setHmPartDetail(HashMap hmPartDetail) {
		this.hmPartDetail = hmPartDetail;
	}
	/**
	 * @return the checkSetDtl
	 */
	public String getCheckSetDtl() {
		return checkSetDtl;
	}
	/**
	 * @param checkSetDtl the checkSetDtl to set
	 */
	public void setCheckSetDtl(String checkSetDtl) {
		this.checkSetDtl = checkSetDtl;
	}
	/**
	 * @return the checkDesc
	 */
	public String getCheckDesc() {
		return checkDesc;
	}
	/**
	 * @param checkDesc the checkDesc to set
	 */
	public void setCheckDesc(String checkDesc) {
		this.checkDesc = checkDesc;
	}
	/**
	 * @return the hideSheetCommentsTab
	 */
	public String getHideSheetCommentsTab() {
		return hideSheetCommentsTab;
	}
	/**
	 * @param hideSheetCommentsTab the hideSheetCommentsTab to set
	 */
	public void setHideSheetCommentsTab(String hideSheetCommentsTab) {
		this.hideSheetCommentsTab = hideSheetCommentsTab;
	}
	/**
	 * @return the hideMonthlyCommentsTab
	 */
	public String getHideMonthlyCommentsTab() {
		return hideMonthlyCommentsTab;
	}
	/**
	 * @param hideMonthlyCommentsTab the hideMonthlyCommentsTab to set
	 */
	public void setHideMonthlyCommentsTab(String hideMonthlyCommentsTab) {
		this.hideMonthlyCommentsTab = hideMonthlyCommentsTab;
	}
	/**
	 * @return the hideGrowthTab
	 */
	public String getHideGrowthTab() {
		return hideGrowthTab;
	}
	/**
	 * @param hideGrowthTab the hideGrowthTab to set
	 */
	public void setHideGrowthTab(String hideGrowthTab) {
		this.hideGrowthTab = hideGrowthTab;
	}
	/**
	 * @return the hideApprovalTab
	 */
	public String getHideApprovalTab() {
		return hideApprovalTab;
	}
	/**
	 * @param hideApprovalTab the hideApprovalTab to set
	 */
	public void setHideApprovalTab(String hideApprovalTab) {
		this.hideApprovalTab = hideApprovalTab;
	}
	/**
	 * @return the accessType
	 */
	public String getAccessType() {
		return accessType;
	}
	/**
	 * @param accessType the accessType to set
	 */
	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}
	/**
	 * @return the levelID
	 */
	public String getLevelID() {
		return levelID;
	}
	/**
	 * @param levelID the levelID to set
	 */
	public void setLevelID(String levelID) {
		this.levelID = levelID;
	}
	/**
	 * @return the strXmlString
	 */
	public String getStrXmlString() {
		return strXmlString;
	}
	/**
	 * @param strXmlString the strXmlString to set
	 */
	public void setStrXmlString(String strXmlString) {
		this.strXmlString = strXmlString;
	}
	/**
	 * @return the monthYear
	 */
	public String getMonthYear() {
		return monthYear;
	}
	/**
	 * @param monthYear the monthYear to set
	 */
	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}
	/**
	 * @return the xmlGridData
	 */
	public String getXmlGridData() {
		return xmlGridData;
	}
	/**
	 * @param xmlGridData the xmlGridData to set
	 */
	public void setXmlGridData(String xmlGridData) {
		this.xmlGridData = xmlGridData;
	}
	/**
	 * @return the ldCommentList
	 */
	public List getLdCommentList() {
		return ldCommentList;
	}
	/**
	 * @param ldCommentList the ldCommentList to set
	 */
	public void setLdCommentList(List ldCommentList) {
		this.ldCommentList = ldCommentList;
	}
	/**
	 * @return the hmAggrPartDetail
	 */
	public HashMap getHmAggrPartDetail() {
		return hmAggrPartDetail;
	}
	/**
	 * @param hmAggrPartDetail the hmAggrPartDetail to set
	 */
	public void setHmAggrPartDetail(HashMap hmAggrPartDetail) {
		this.hmAggrPartDetail = hmAggrPartDetail;
	}
	/**
	 * @return the growthDrillDown
	 */
	public String getGrowthDrillDown() {
		return growthDrillDown;
	}
	/**
	 * @param growthDrillDown the growthDrillDown to set
	 */
	public void setGrowthDrillDown(String growthDrillDown) {
		this.growthDrillDown = growthDrillDown;
	}
	/**
	 * @return the setID
	 */
	public String getSetID() {
		return setID;
	}
	/**
	 * @param setID the setID to set
	 */
	public void setSetID(String setID) {
		this.setID = setID;
	}
	/**
	 * @return the growthDrillDownMonth
	 */
	public String getGrowthDrillDownMonth() {
		return growthDrillDownMonth;
	}
	/**
	 * @param growthDrillDownMonth the growthDrillDownMonth to set
	 */
	public void setGrowthDrillDownMonth(String growthDrillDownMonth) {
		this.growthDrillDownMonth = growthDrillDownMonth;
	}
	/**
	 * @return the drillDownType
	 */
	public String getDrillDownType() {
		return drillDownType;
	}
	/**
	 * @param drillDownType the drillDownType to set
	 */
	public void setDrillDownType(String drillDownType) {
		this.drillDownType = drillDownType;
	}
	public String getSearchdemandSheetId() {
		return searchdemandSheetId;
	}
	public void setSearchdemandSheetId(String searchdemandSheetId) {
		this.searchdemandSheetId = searchdemandSheetId;
	}
	
	/**
	 * @return the ttpId
	 */
	public String getTtpId() {
		return ttpId;
	}
	/**
	 * @param ttpId the ttpId to set
	 */
	public void setTtpId(String ttpId) {
		this.ttpId = ttpId;
	}
	
	}
