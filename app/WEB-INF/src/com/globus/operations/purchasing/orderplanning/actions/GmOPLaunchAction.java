/**
 * FileName    : GmLaunchAction.java 
 * Description :
 * Author      : Brinal
 * Date        : Sept 30th, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.operations.purchasing.orderplanning.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.purchasing.orderplanning.beans.GmDemandTemplateBean;
import com.globus.operations.purchasing.orderplanning.beans.GmOPDemandSetupBean;
import com.globus.operations.purchasing.orderplanning.forms.GmOPDemandSheetSetupForm;
 
public class GmOPLaunchAction extends GmAction {
	
	 public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
	        GmOPDemandSheetSetupForm gmOPDemandSheetSetupForm = (GmOPDemandSheetSetupForm)form;
	        gmOPDemandSheetSetupForm.loadSessionParameters(request);
	        GmOPDemandSetupBean gmOPDemandSetupBean = new GmOPDemandSetupBean();
	        GmDemandTemplateBean gmDemandTemplateBean =  new GmDemandTemplateBean();
	        GmCommonBean gmCommonBean = new GmCommonBean ();
	        GmAccessControlBean gmAccessControlBean= new GmAccessControlBean();
	        RowSetDynaClass rdResult = null;
	        HashMap hmParams = new HashMap();
	        HashMap hmAccess = new HashMap();
	        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

	        gmOPDemandSheetSetupForm.setAlDates(gmCommonBean.getDateList());
	        
	        hmParams = GmCommonClass.getHashMapFromForm(gmOPDemandSheetSetupForm);
	        
	        String strOpt = gmOPDemandSheetSetupForm.getStrOpt();
	        String strDemandSheetId = gmOPDemandSheetSetupForm.getDemandSheetId();
	        String strInputStr = gmOPDemandSheetSetupForm.getHinputStr();
	        String strPartyId 	 = GmCommonClass.parseNull((String) gmOPDemandSheetSetupForm.getSessPartyId());
	        String strAccessType = "";
	        String strAccessFl = "";
	        	                	        
	        log.debug (" Testing. ..... "+strInputStr );
	        if (strOpt.equals("save"))
	        { 
	            gmOPDemandSetupBean.saveLaunchDates(hmParams); 
	        } 
	        
	        rdResult = gmOPDemandSetupBean.loadSetMapListforLaunch(strDemandSheetId);
	        gmOPDemandSheetSetupForm.setLdtResult(rdResult.getRows());
	        strAccessType = GmCommonClass.parseNull(gmDemandTemplateBean.getSheetAccessType(strDemandSheetId));
	        gmOPDemandSheetSetupForm.setAccessType(strAccessType);
	        
        	hmAccess    = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,"DEMAND_SHEET_EDIT"));
			strAccessFl = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
			gmOPDemandSheetSetupForm.setEditAccess(strAccessFl);
	        
	        return mapping.findForward("GmLaunch");
	    }


}
