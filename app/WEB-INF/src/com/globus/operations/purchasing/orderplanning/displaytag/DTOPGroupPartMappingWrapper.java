 
package com.globus.operations.purchasing.orderplanning.displaytag;

import java.util.HashMap;

import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;

import com.globus.clinical.forms.GmClinicalDownloadForm;
import com.globus.operations.purchasing.orderplanning.forms.GmOPGroupPartMapForm;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import java.text.DecimalFormat;

 
public class DTOPGroupPartMappingWrapper extends TableDecorator
{

    private DynaBean db;
    private String strValue; 
    
    private String strPnum = "";
    private String strPdesc = "";
    double dbTotal = 0.00;
    Double dblObj;
    private String strPriceTYpe = "";
    private String strGroupid = "";
    private int intCount = 0; 
    private String strchecked = "";
    private String strHistoryfl = "";
    private String strPrimaryLockFl="";
    private String strPartPrimaryLock="";
    private String strEnable="";
    private String strGroupOwner="";
    private int intPnumCount = 0;
    private int intPlCount = 0;
    private GmOPGroupPartMapForm gmOPGroupPartMapForm;
    private DecimalFormat moneyFormat;
    private String strListTWPrice = "";
	
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
    HashMap hmSearchResult;

    public DTOPGroupPartMappingWrapper()
    {
    	super();
        // Formats for displaying money.
        this.moneyFormat = new DecimalFormat("#,###,###.00"); 
    }
    
    public void init(PageContext pageContext, Object decorated, TableModel tableModel) {
		super.init(pageContext, decorated, tableModel);
		gmOPGroupPartMapForm = (GmOPGroupPartMapForm)getPageContext().getAttribute("frmOPGroupPartMapping", PageContext.REQUEST_SCOPE);	
	}
	
    
    public String getPNUM()
    {
    	 
    	hmSearchResult = (HashMap) this.getCurrentRowObject();
    	strGroupid = (String)hmSearchResult.get("GROUPID");
    	strPnum = (String)hmSearchResult.get("PNUM");
    	strPriceTYpe = GmCommonClass.parseZero(String.valueOf(hmSearchResult.get("PRICING_TYPE")));
    	strHistoryfl = (String)hmSearchResult.get("HISTORYFL");
    	intPnumCount = Integer.parseInt(String.valueOf(hmSearchResult.get("PNUM_CNT")));
    	
    	//MNTTASK-4733 - 	Pricing by Group Tab - When login Pricing Team should not able to view Part Number History.
    	String strDisableLink = GmCommonClass.parseNull(gmOPGroupPartMapForm.getStrDisableLink());
    	
    	if (!strDisableLink.equals("")){
    		intPnumCount = 0;
    		strHistoryfl = "N";	
    	}

    	//if (strPriceTYpe.equals("52080")||strPriceTYpe.equals("52081") )
    	if (intPnumCount > 1 )
        {
    		 
			strValue = "&nbsp;<a title='Shared Part'  href=\"javascript:fnViewPartGrp( '"+ strPnum + "') \" /><font color= blue >" + strPnum + "</font></a>";
			
	    /*	strValue =  "&nbsp;" + strPnum +"&nbsp;<a title='Shared Part'><img id='imgEditA' style='cursor:hand' src='/images/ordsum.gif'" +  
	        " width='12' height='13'" + 
	        "onClick=\"javascript:fnViewPartGrp( '"+ strPnum + "') \" /></a>"  ;*/
        }
    	else {
    		strValue = "&nbsp;" + strPnum;  
    	}
    	
    	if (strHistoryfl.equals("Y") )
        {
    		strValue = strValue +  "&nbsp;<a title='Click to open part history'><img id='imgEdit' style='cursor:hand' src='/images/icon_History.gif'" +  
	        "onClick=\"javascript:fnPartHistory( '"+ strPnum + "') \" /></a>"; 
        }
         	
        
        return strValue;
    }

    
    public String getPDESC()
    {
    	 
    	hmSearchResult = (HashMap) this.getCurrentRowObject(); 
    	strPdesc = (String)hmSearchResult.get("PDESC");
    	 
    	 
    		strValue =  GmCommonClass.getStringWithTM(strPdesc); 
        	
        
        return strValue;
    }
    
    public String getPRICING_TYPE() { 
		hmSearchResult = (HashMap) this.getCurrentRowObject(); 
		strValue =""; 
		strPriceTYpe = GmCommonClass.parseZero(String.valueOf(hmSearchResult.get("PRICING_TYPE"))); 
		strPnum = (String)hmSearchResult.get("PNUM");
		intPnumCount = Integer.parseInt(String.valueOf(hmSearchResult.get("PNUM_CNT")));
		strPartPrimaryLock = GmCommonClass.parseZero(String.valueOf(hmSearchResult.get("PART_PRIMARY_LOCK")));
		
		if (intPnumCount > 1 )
		{
			if (strPriceTYpe.equals("52080")||strPriceTYpe.equals("52081") )
	        {
				if(strPriceTYpe.equals("52080")) strchecked="checked";  else strchecked="";
				if(strPartPrimaryLock.equals("Y"))strEnable="disabled"; else strEnable="";
				strValue = "<input "+strEnable+" class=RightText " + strchecked + " value="+strPnum+" type='checkbox' name='Chk_BO"+intCount+"'>&nbsp;";
				strValue = strValue + "&nbsp;<input   type='hidden'  name='Chk_PartPriceType"+intCount+"' value='"+strPriceTYpe+"'>&nbsp;";
	        } 
		}
		else 
			strValue = "<input   type='hidden'  name='Chk_BO"+intCount+"'>&nbsp;"; 
		intCount++; 
		return strValue;
	}
    
    public String getPRIMARYLOCK() { 
		hmSearchResult = (HashMap) this.getCurrentRowObject(); 
		strValue =""; 
		strPrimaryLockFl =  GmCommonClass.parseNull(String.valueOf(hmSearchResult.get("PRIMARY_LOCK_FL")));
		strPriceTYpe = GmCommonClass.parseZero(String.valueOf(hmSearchResult.get("PRICING_TYPE")));
		strPnum = GmCommonClass.parseNull((String)hmSearchResult.get("PNUM"));
		strGroupOwner =  GmCommonClass.parseNull((String)hmSearchResult.get("GROUP_OWNER"));
		intPnumCount = Integer.parseInt(String.valueOf(hmSearchResult.get("PNUM_CNT")));
		if (intPnumCount > 1 )
		{
			if(strGroupOwner.equals(gmOPGroupPartMapForm.getUserId()))strEnable=""; else strEnable="disabled";
			if(strPriceTYpe.equals("52080")){
				if(strPrimaryLockFl.equals("Y")) strchecked="checked";  else strchecked="";
				strValue = "<input "+ strEnable+" class=RightText " + strchecked + " value="+strPnum+" type='checkbox' name='Chk_PL"+intPlCount+"'>&nbsp;";
			}else
				strValue = "<input   type='hidden'  name='Chk_PL"+intPlCount+"'>&nbsp;";
		}
		else 
			strValue = "<input   type='hidden'  name='Chk_PL"+intPlCount+"'>&nbsp;"; 
		intPlCount++; 
		return strValue;
	}
    
    
    
    
  /*  public String addRowClass()
    {
    	hmSearchResult = (HashMap) this.getCurrentRowObject();
    	intPnumCount = Integer.parseInt(String.valueOf(hmSearchResult.get("PNUM_CNT")));
    	String strRowClass ="";
    	if (intPnumCount > 1 )
		{
    		 strRowClass =   "rowbgcolor" ;
		}
    	return strRowClass;
    }
    */
    public String addRowId()
    {
    	hmSearchResult = (HashMap) this.getCurrentRowObject();
    	intPnumCount = Integer.parseInt(String.valueOf(hmSearchResult.get("PNUM_CNT")));
    	String strRowClass ="";
    	if (intPnumCount > 1 )
		{
    		 strRowClass =   "multi" ;
    		 
		}
    	return strRowClass;
    }
    
    public String getLISTP()
	{
    	String strListPrice = "";
    	hmSearchResult = (HashMap) this.getCurrentRowObject();
    	strListPrice = GmCommonClass.parseNull((String)hmSearchResult.get("LISTP"));
    	// When click P icon will display price. If it is null then should be display empty.
    	if(strListPrice.equals("")){
    		strListPrice = null;
    	}else{
    		dbTotal = Double.parseDouble((hmSearchResult.get("LISTP")).toString());
    		dblObj = Double.valueOf(String.valueOf(dbTotal));
    		strListPrice = this.moneyFormat.format(dblObj);
    	}
		return strListPrice;
		  	 
    }  
    public String getTW()
	{
    	String strTWPrice = "";
    	hmSearchResult = (HashMap) this.getCurrentRowObject();
    	strTWPrice = GmCommonClass.parseNull((String)hmSearchResult.get("TW"));

    	// When click P icon will display price. If it is null then should be display empty.
    	if(strTWPrice.equals("")){
    		strTWPrice = null;
    	}else{
    		dbTotal = Double.parseDouble((hmSearchResult.get("TW")).toString());
    		dblObj = Double.valueOf(String.valueOf(dbTotal));
    		strTWPrice = this.moneyFormat.format(dblObj);
    	}
		return strTWPrice;
		  	 
    }
  /*  public String finishRow() { 
    	hmSearchResult = (HashMap) this.getCurrentRowObject();
    	intPnumCount = Integer.parseInt(String.valueOf(hmSearchResult.get("PNUM_CNT")));
    	 
                return "\n<tr class=rowbgcolor ><td>  </td></tr>"; 
         
    		 
    }*/

}
