package com.globus.operations.purchasing.orderplanning.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import oracle.jdbc.driver.OracleTypes;
import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;

public class GmDemandMasterSetupBean
{
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.   
           
       /**
        * fetchEditableDemandSheet - This Method is used to get editable demand sheet
        * @param HashMap hmParam  
        * @return ArrayList
        * @exception AppError
      **/
        public ArrayList fetchEditableDemandSheet(HashMap hmParam) throws AppError
        {
          
          ArrayList alSheetList = new ArrayList();
          GmCacheManager gmCacheManager = new GmCacheManager(GmDNSNamesEnum.DMWEBGLOBUS); 
          String strEditType=GmCommonClass.parseNull((String)hmParam.get("EDITTYPE")); 
          String strTemplateId = GmCommonClass.parseNull((String)hmParam.get("TEMPLATEID"));
          String strRegionId = GmCommonClass.parseNull((String)hmParam.get("REGIONID"));
          String strSheetType = GmCommonClass.parseNull((String)hmParam.get("SHEETTYPEID"));
			
			if (strTemplateId.equals("0")){
				strTemplateId = "";
			}
			if (strRegionId.equals("0")){
				strRegionId = "";
			}
			if (strSheetType.equals("0")){
				strSheetType = "";
			}
          // To fetch the transaction information
			gmCacheManager.setPrepareString("gm_pkg_oppr_dmd_mas_rpt.gm_fch_editable_dmd_sheet",4);
			gmCacheManager.registerOutParameter(4,OracleTypes.CURSOR);
			gmCacheManager.setString(1,strTemplateId);
			gmCacheManager.setString(2,strRegionId);
			gmCacheManager.setString(3,strSheetType);
			gmCacheManager.execute();
			gmCacheManager.setKey("EDIT_DEMANDSHEET:"+strTemplateId+":"+strRegionId+":"+strSheetType);
          
          alSheetList = gmCacheManager.returnArrayList((ResultSet)gmCacheManager.getObject(4));
          
          gmCacheManager.close();

          return alSheetList;
        }
        /**
         * fetchOTNParts - This Method is used to get otn parts
         * @param HashMap hmParam  
         * @return ArrayList
         * @exception AppError
       **/ 
        public ArrayList fetchOTNParts(HashMap hmParam) throws AppError {

    		RowSetDynaClass rdResult = null;
    		String strDemandMasterId = GmCommonClass.parseNull((String)hmParam.get("DEMANDMASTERID"));
    	    String strPartInput = GmCommonClass.parseNull((String)hmParam.get("PARTNUMBERS"));
    	    
    		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
    		gmDBManager.setPrepareString("gm_pkg_oppr_dmd_mas_rpt.gm_fch_part_otn", 3);
    		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    		gmDBManager.setString(1, strDemandMasterId);
    		gmDBManager.setString(2, strPartInput);
    		
    		gmDBManager.execute(); 
    		
    		rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
    		gmDBManager.close();

    		return (ArrayList) rdResult.getRows();
    	}
        /**
    	 * savePartOTN - This method will be save / update the otn detail
    	 * 
    	 * @param HashMap hmParam
    	 * @exception AppError
    	 */
    	public void savePartOTN(HashMap hmParam) throws AppError {
    		String inputString = GmCommonClass.parseNull((String)hmParam.get("HINPUTSTR"));
    	    String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
    		
    		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
    		gmDBManager.setPrepareString("gm_pkg_oppr_dmd_mas_txn.gm_sav_part_otn", 2);
    		gmDBManager.setString(1, strUserId);
    		gmDBManager.setString(2, inputString);
    			
    		gmDBManager.execute();
    			
    		gmDBManager.commit();
    		
    	}
    	
    	/**
         * fetchLeadtimeParts - This Method is used to get Lead Time for parts
         * @param HashMap hmParam  
         * @return HashMap
         * @exception AppError
       **/ 
    	public HashMap fetchLeadtimeParts(HashMap hmParam) throws AppError {

			RowSetDynaClass rdSubResult = null;
			HashMap hmReturn = new HashMap();
			
			GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.WEBGLOBUS);
			log.debug("hmParam in fetchLeadtimeParts == "+hmParam);
			String strProjectID = GmCommonClass.parseNull((String) hmParam.get("PROJECTLISTID"));
			String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));

			gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_fch_part_leadtime", 3);
			gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
			gmDBManager.setString(1, strPartNum);
			gmDBManager.setString(2, strProjectID); 
			gmDBManager.execute(); 

			rdSubResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
		 
			gmDBManager.close();
			hmReturn.put("LEADTIMEPARTS", rdSubResult);
		 	 
			return hmReturn;
		}
		
    	/**
         * savePartLeadTime - This Method is used to save Lead Time for parts
         * @param HashMap hmParam  
         * @return void
         * @exception AppError
       **/ 
		public void savePartLeadTime (HashMap hmParam)throws AppError
	    {
			log.debug("HmParam: "+ hmParam);
			String strAttributeType = "4000101";
			String strInputString = GmCommonClass.parseNull((String)hmParam.get("HINPUTSTR"));
			String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
	       
	        GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.WEBGLOBUS);                
	        gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_sav_part_leadtime",3);
	        gmDBManager.setString(1,strAttributeType);
	        gmDBManager.setString(2,strInputString);
	        gmDBManager.setString(3,strUserId);
	        gmDBManager.execute();
	        gmDBManager.commit();
	    }
		 /**
         * fetchWorldWideSheet - This Method is used to fetch demand sheets accross the worldwide consignment set and inhouse consignment sheet only
         * @return ArrayList
         * @exception AppError
       **/
         public ArrayList fetchWorldWideSheet(String strDemandTypes) throws AppError{
           
           ArrayList alwldwideDmdSheet = new ArrayList();
           GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);            
           gmDBManager.setPrepareString("gm_pkg_oppr_dmd_mas_rpt.gm_fch_worldwide_dmd_sheet",2);
           gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);  
           gmDBManager.setString(1, strDemandTypes);
           gmDBManager.execute();           
           alwldwideDmdSheet = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));           
           gmDBManager.close();
           return alwldwideDmdSheet;
         }
         
         /**
     	 * fetchSheetSetParDtls - This method will fetch the PAR Details for the Set ID's	 * 
     	 * @param HashMap - hmParam
     	 * @return RowSetDynaClass
     	 * @exception AppError , Exception
     	 */
     	public RowSetDynaClass fetchSheetSetParDtls(HashMap hmParam) throws AppError,Exception {
     		RowSetDynaClass rdResult = null;
     		String strDemandSheetId = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETNAME"));
     		String strDemandSetID      = GmCommonClass.parseNull((String) hmParam.get("DEMANDSETID"));
     		String strDemandSetNm   = GmCommonClass.parseNull((String) hmParam.get("DEMANDSETNM"));
     		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
     		gmDBManager.setPrepareString("gm_pkg_oppr_dmd_mas_rpt.gm_fch_set_par", 4);
     		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
     		gmDBManager.setString(1, strDemandSheetId);
     		gmDBManager.setString(2, strDemandSetID);
     		gmDBManager.setString(3, strDemandSetNm);
     		gmDBManager.execute();
     		rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));
     		gmDBManager.close();
     		return rdResult;
     	} // End of fetchSheetSetParDtls()
     	/**
         * saveSetPAR - This Method is used to save PAR for the Set's
         * @param HashMap hmParam  
         * @return void
         * @exception AppError
       **/ 
		public void saveSetPAR(HashMap hmParam)throws AppError  {
			String strInputString = GmCommonClass.parseNull((String)hmParam.get("HINPUTSTR"));
			String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));	       
	        GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);                
	        gmDBManager.setPrepareString("gm_pkg_oppr_dmd_mas_txn.gm_sav_set_par",2);
	        gmDBManager.setString(1,strInputString);
	        gmDBManager.setString(2,strUserId);
	        gmDBManager.execute();
	        gmDBManager.commit();
	    }// End of saveSetPAR
     	
		/**
		 * fetchTemplates - This method will fetch all the Active Templates	 * 
		 * @return ArrayList
		 * @exception AppError
		 */
		 public ArrayList fetchTemplates() throws AppError {		
			ArrayList alTemplates = new ArrayList();
			GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
			gmDBManager.setPrepareString("gm_pkg_oppr_dmd_mas_rpt.gm_fch_templates", 1);
			gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
			gmDBManager.execute(); 
			alTemplates =  gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
			gmDBManager.close();		
			return alTemplates;
		} // End of fetchTemplates
		 
		/**
		* fetchTemplates - This method will fetch all the Active Templates	 * 
		* @return ArrayList
		* @exception AppError
		*/
		 public ArrayList fetchRegions(HashMap hmParam) throws AppError {		
			log.debug("HmParam in fetchRegions= "+ hmParam);
			ArrayList alRegions = new ArrayList();
			String strGroupIds = "REGN";
			strGroupIds = strGroupIds + "," + "DIV"; 
			GmDBManager gmDBManager = new GmDBManager (GmDNSNamesEnum.DMWEBGLOBUS);
			gmDBManager.setPrepareString("gm_pkg_oppr_dmd_mas_rpt.gm_fch_region", 2);
			gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
			gmDBManager.setString(1,strGroupIds);
			gmDBManager.execute(); 
			alRegions =  gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
			log.debug("alRegions= "+alRegions);
			gmDBManager.close();		
			return alRegions;
		} // End of fetchRegions
		 
		/**
		* fetchTemplates - This method will fetch all the Active Templates	 * 
		* @return ArrayList
		* @exception AppError
		*/
		public ArrayList fetchDemandSheets(HashMap hmParam) throws AppError {
			log.debug("HmParam in fetchDemandSheets= "+ hmParam);
			String strTemplateId = GmCommonClass.parseNull((String)hmParam.get("TEMPLATEID"));
			String strRegionId = GmCommonClass.parseNull((String)hmParam.get("REGIONID"));
			String strSheetType = GmCommonClass.parseNull((String)hmParam.get("SHEETTYPEID"));
			
			if (strTemplateId.equals("")){
				strTemplateId = "0";
			}
			if (strRegionId.equals("")){
				strRegionId = "0";
			}
			if (strSheetType.equals("")){
				strSheetType = "0";
			}
			
			ArrayList alDemandSheets = new ArrayList();
			 GmCacheManager gmCacheManager = new GmCacheManager(GmDNSNamesEnum.DMWEBGLOBUS);
			 gmCacheManager.setPrepareString("gm_pkg_oppr_dmd_mas_rpt.gm_fch_demand_sheet", 4);
			 gmCacheManager.registerOutParameter(4, OracleTypes.CURSOR);
			 gmCacheManager.setString(1,strTemplateId);
			 gmCacheManager.setString(2,strRegionId);
			 gmCacheManager.setString(3,strSheetType);
			 gmCacheManager.execute(); 
			 gmCacheManager.setKey("FETCH_DEMANDSHEET:"+strTemplateId+":"+strRegionId+":"+strSheetType);
			alDemandSheets =  gmCacheManager.returnArrayList((ResultSet) gmCacheManager.getObject(4));
			log.debug("alDemandSheets= "+alDemandSheets);
			gmCacheManager.close();		
			return alDemandSheets;
		} // End of fetchDemandSheets

		/**fetchDemandAverageOverride - This method will fetch demand override avg details.
		 * @param hmParam
		 * @return HashMap
		 * @throws AppError
		 */
		public HashMap fetchDemandAverageOverride(HashMap hmParam) throws AppError {
			log.debug("HmParam in fetchDemandAverageOverride= "+ hmParam);
			String strOverrideId = GmCommonClass.parseNull((String)hmParam.get("DEMANDOVERRIDEID"));
			String strDemandMasterId = GmCommonClass.parseNull((String)hmParam.get("DEMANDMASTERID"));
			String strRefID = GmCommonClass.parseNull((String)hmParam.get("REFID"));	
			
			HashMap hmReturn = new HashMap();
			GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
			gmDBManager.setPrepareString("gm_pkg_oppr_dmd_mas_rpt.gm_fch_dmd_avg_override", 4);
			gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
			gmDBManager.setString(1,strOverrideId);
			gmDBManager.setString(2,strDemandMasterId);
			gmDBManager.setString(3,strRefID);
			gmDBManager.execute(); 
			
			hmReturn =  GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet)gmDBManager.getObject(4)));
			gmDBManager.close();		
			return hmReturn;
		} // End of fetchDemandAverageOverride
		
    	/**saveDemandAverageOverride - This will save the demand override details.
    	 * @param hmParam
    	 * @return String
    	 * @throws AppError
    	 */
    	public String saveDemandAverageOverride(HashMap hmParam) throws AppError {
    		String strOverrideId="";
    		log.debug("HmParam in saveDemandAverageOverride= "+ hmParam);
    		String strDemandMasterId = GmCommonClass.parseNull((String)hmParam.get("DEMANDMASTERID"));
    	    String strDemandSheetId = GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETID"));	
    	    String strRefId = GmCommonClass.parseNull((String)hmParam.get("REFID"));
    	    String strRefType = GmCommonClass.parseNull((String)hmParam.get("REFTYPE"));
    	    String strQty = GmCommonClass.parseNull((String)hmParam.get("TXTWEIGTHEDAVG"));
    	    String strComments = GmCommonClass.parseNull((String)hmParam.get("TXT_LOGREASON"));	
    	    String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
    	        	    
    		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
    		gmDBManager.setPrepareString("gm_pkg_oppr_dmd_mas_txn.gm_sav_dmd_avg_override", 8);
    		gmDBManager.setString(1, strDemandMasterId);
    		gmDBManager.setString(2, strDemandSheetId);
    		gmDBManager.setString(3, strRefId);
    		gmDBManager.setString(4, strRefType);
    		gmDBManager.setString(5, strQty);
    		gmDBManager.setString(6, strComments);
    		gmDBManager.setString(7, strUserId);
    		gmDBManager.registerOutParameter(8, OracleTypes.VARCHAR);
    		
    		gmDBManager.execute();    		
    		strOverrideId=GmCommonClass.parseNull((String)gmDBManager.getString(8));
    		
    		gmDBManager.commit();
    		
    		return strOverrideId;
    		
    	}
}
