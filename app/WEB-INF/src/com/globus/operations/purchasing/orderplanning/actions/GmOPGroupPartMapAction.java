
package com.globus.operations.purchasing.orderplanning.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.purchasing.orderplanning.beans.GmOPDemandSetupBean;
import com.globus.operations.purchasing.orderplanning.forms.GmOPGroupPartMapForm;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmOPGroupPartMapAction extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		GmOPGroupPartMapForm gmOPGroupPartMapForm = (GmOPGroupPartMapForm) form;
		gmOPGroupPartMapForm.loadSessionParameters(request);
		GmOPDemandSetupBean gmOPDemandSetupBean = new GmOPDemandSetupBean();
		GmProjectBean gmProjectBean = new GmProjectBean();
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code
		// to
		// Initialize
		// the
		// Logger
		// Class.

		String strAction = gmOPGroupPartMapForm.getHaction();
		String strGroupId = gmOPGroupPartMapForm.getGroupId();		
		String strPartInput = GmCommonClass.removeSpaces(gmOPGroupPartMapForm.getPartNumbers());
		String strPartInputString = "";
		String strPartListFlag = GmCommonClass.parseNull(gmOPGroupPartMapForm.getStrPartListFlag());
		String strPricingByGroupFl = GmCommonClass.parseNull(gmOPGroupPartMapForm.getStrPricingByGroupFl()); 
		//PMT-758 - When click P icon in Group Part Pricing screen, should be display List & TW price.
		String strHPartPriceVal =GmCommonClass.parseNull( gmOPGroupPartMapForm.getStrHPartPriceFl());
		String strOption = "";
		String strPartyId = gmOPGroupPartMapForm.getSessPartyId();
		String accessFlag = "";
		String strPublishFL = "";
		
		log.debug(" strAction is " + strAction); 
		 
		HashMap hmAccess = new HashMap();
	
		HashMap hmParam = GmCommonClass.getHashMapFromForm(gmOPGroupPartMapForm);
		
		if (strAction.equals("save")) {
			// strPartInputString =
			// GmCommonClass.createInputString(gmOPGroupPartMapForm.getCheckPartNumbers());
			// hmParam.put("INPUTSTRING",strPartInputString);
			strPartInputString = GmCommonClass.parseNull(gmOPGroupPartMapForm.getInputString());

			log.debug(" values inside hmParam " + hmParam);
			strGroupId = gmOPDemandSetupBean.saveGroupPartMapping(hmParam);

			gmOPGroupPartMapForm.setGroupId(strGroupId);
			gmOPGroupPartMapForm.setPartNumbers("");
		}  
		
		if (strAction.equals("save")||strAction.equals("edit") )
		{	strGroupId = gmOPGroupPartMapForm.getGroupId();
			HashMap hmValues = gmOPDemandSetupBean.fetchGroupPartInfo(strGroupId);
			log.debug(" values fetched for edit " + hmValues);
			gmOPGroupPartMapForm = (GmOPGroupPartMapForm) GmCommonClass.getFormFromHashMap(gmOPGroupPartMapForm, hmValues);
			strPublishFL = gmOPGroupPartMapForm.getPublishfl(); 		 
			hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "1161"); 
			accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL")); 
			
			if (!accessFlag.equals("Y") && strPublishFL.equals("Y")) {
				gmOPGroupPartMapForm.setStrEnableGrpType("disabled");
				// MNTTASK-6106 - After Publish group, this party only should change below drop downs.
				gmOPGroupPartMapForm.setStrDisablePricedType("disabled");
				gmOPGroupPartMapForm.setStrDisableGroupName("disabled");
			}
			log.debug(" strEnableGrpType --> " + gmOPGroupPartMapForm.getStrEnableGrpType()); 
		}

		hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "1160"); 
		accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
		
		log.debug(" accessFlag for group part map--> " + accessFlag);
		
		if (!accessFlag.equals("Y")) {
			gmOPGroupPartMapForm.setEnableGrpPartMapChange("disabled");
		}
		log.debug(" EnableGrpPartMapChange --> " + gmOPGroupPartMapForm.getEnableGrpPartMapChange());
		
		log.debug(" values in gmPartGroupForm form --> " + GmCommonClass.getHashMapFromForm(gmOPGroupPartMapForm));

		ArrayList alGroupList = gmOPDemandSetupBean.loadDemandGroupList(hmParam);
		gmOPGroupPartMapForm.setAlGroupList(alGroupList);

		ArrayList alSalesGrp = new ArrayList();
		alSalesGrp = gmProjectBean.loadSetMap("GROUPPARTMAPPING");
		// hmTemp.put("SALESGROUP",alSalesGrp);
		gmOPGroupPartMapForm.setAlSetID(alSalesGrp);

		ArrayList alGroupType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("GRPTYP"));
		gmOPGroupPartMapForm.setAlGroupType(alGroupType);

		ArrayList alPricedType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("GPMTYP"));		
		gmOPGroupPartMapForm.setAlPricedType(alPricedType);  
		if (!strGroupId.equals("0") && !strAction.equals("loadParts")) {
			String hactiveflag = gmOPGroupPartMapForm.getHactiveflag();			
			if (hactiveflag.equals("true"))
				gmOPGroupPartMapForm.setActiveflag("");
			/*
			 * String strInActiveFlag =
			 * GmCommonClass.getCheckBoxValue(gmOPGroupPartMapForm.getActiveflag());
			 * 
			 * log.debug(" strInActiveFlag is >>>>>>>>>>>>>>>>>> " +
			 * strInActiveFlag); if(strInActiveFlag.equals("")) {
			 * gmOPGroupPartMapForm.setActiveflag("on"); }
			 */			
			gmOPGroupPartMapForm.setGroupId(strGroupId);			
			hmParam = GmCommonClass.getHashMapFromForm(gmOPGroupPartMapForm);			
		}		
		String strSubComponentFlag = GmCommonClass.getCheckBoxValue(gmOPGroupPartMapForm.getEnableSubComponent());		
		strGroupId = gmOPGroupPartMapForm.getGroupId();		
		if (!strGroupId.equals("0") || strAction.equals("loadParts")) {
			log.debug(" strGroupId ins " + strGroupId);

			if (!strPartInput.equals("")) {

				strPartInput = strPartInput.replaceAll(",", "|^");
				strPartInput = "^".concat(strPartInput);
			}
			log.debug(" strPartInputString -- " + strPartInput);
			hmParam.put("STRACTION", strAction);
			hmParam.put("STRGROUPID", strGroupId);
			hmParam.put("STRPARTINPUT", strPartInput);
			hmParam.put("STRSUBCOMPONENTFLAG", strSubComponentFlag);
			hmParam.put("STRPRICINGBYGROUPFL", strPricingByGroupFl);
			hmParam.put("STRPARTPRICEVAL", strHPartPriceVal);
			ArrayList alList = gmOPDemandSetupBean.loadPartGroupMapping(hmParam);
			request.setAttribute("RDGROUPPARTMAP", alList);
		}
		strOption = "success";

		if (strPartListFlag.equals("Y"))
			strOption = "loadPartList";
		
		
		return mapping.findForward(strOption);
	}

}
