package com.globus.operations.purchasing.orderplanning.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import com.globus.accounts.servlets.GMDHRFlagInvoiceServlet;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.operations.purchasing.orderplanning.beans.GmDemandMasterSetupBean;
import com.globus.operations.purchasing.orderplanning.forms.GmSheetSetPARForm;
import com.globus.common.beans.GmLogger; 

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger; 


public class GmSheetSetPARAction extends GmDispatchAction
{
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    
    public ActionForward sheetSetPARUpdReport(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws AppError ,Exception
    {   
    	GmSheetSetPARForm gmSheetSetPARForm = (GmSheetSetPARForm) form;
    	gmSheetSetPARForm.loadSessionParameters(request);		
    	GmCommonClass gmCommonClass = new GmCommonClass();
		HashMap hmParam = new HashMap();		
		hmParam = GmCommonClass.getHashMapFromForm(gmSheetSetPARForm);       
		GmDemandMasterSetupBean gmDemandMasterSetup = new GmDemandMasterSetupBean();
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
		RowSetDynaClass rdDemandSheetReport = null;
        log.debug(" inside loadTemplateReport ");
        ArrayList alDemandSheetList = new ArrayList();
        String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));      
        alDemandSheetList = GmCommonClass.parseNullArrayList((ArrayList)gmDemandMasterSetup.fetchWorldWideSheet("40021"));  //Load only WorldWide Consign type sheets.
        String strPartyId = GmCommonClass.parseNull((String) gmSheetSetPARForm.getSessPartyId());
        String strUpdateFl = "";
        HashMap hmAccess = new HashMap();
        hmAccess = GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId, "SET_PAR")));
        strUpdateFl = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
        if(strOpt.equals("reload")){        	
             rdDemandSheetReport = gmDemandMasterSetup.fetchSheetSetParDtls(hmParam);
             gmSheetSetPARForm.setReturnReport(rdDemandSheetReport.getRows());        
        }     
        if(strOpt.equals("save")){
        	 gmDemandMasterSetup.saveSetPAR(hmParam);        	 
        	 String strDemandSheetId = GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETNAME"));
     	     String strSetID = GmCommonClass.parseNull((String)hmParam.get("DEMANDSETID"));
     	     String strSetName = GmCommonClass.parseNull((String)hmParam.get("DEMANDSETNM"));
       	  	 String strRedirectURL = "/gmSheetSetPAR.do?method=sheetSetPARUpdReport&strOpt=reload&demandSheetName="+strDemandSheetId+"&demandSetId="+strSetID+"&demandSetNm="+strSetName;
       	  	 ActionRedirect actionRedirect = new ActionRedirect(strRedirectURL);
 		     return actionRedirect;
        }
        gmSheetSetPARForm.setUpdatefl(strUpdateFl);       
        gmSheetSetPARForm.setAlDemandSheetList((alDemandSheetList));
        return mapping.findForward("gmSheetSetPAR");
    }   
}









