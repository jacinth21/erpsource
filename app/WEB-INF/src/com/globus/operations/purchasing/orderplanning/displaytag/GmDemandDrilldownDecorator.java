package com.globus.operations.purchasing.orderplanning.displaytag;


import java.util.HashMap;
import org.apache.log4j.Logger;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.crosstab.beans.GmRowHighlightDecorator;

public class GmDemandDrilldownDecorator extends GmRowHighlightDecorator {

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
 
 
	/*
	 * To Override the TD Style of the column Name
	 */

	public String getStyle(String strColumnName) {
		HashMap hmRow = getCurrentRow();
		String strStyle = "";
		String strDemandType = (String) getAttribute("DEMANDTYPE");			
		if (strColumnName.equals("Name")) {
			String strID = GmCommonClass.parseNull((String) hmRow.get("CROSSOVERF"));
			if (strID.equals("Y")) {
				strStyle = "YellowShadeTD";
			}
		}
		 
			String strPnum = (String)hmRow.get("ID"); 
		 	String strComments = (String)hmRow.get(strColumnName+"COMM_FL");
		 	String strHistoryFl = (String)hmRow.get(strColumnName+"HISTORY_FL"); 
			boolean bolComments = false;
			boolean bolHistoryFl = false;
			
	 
			if(!strColumnName.trim().equalsIgnoreCase("Name")){
				if(strComments!=null && strComments.equals("Y")){
					bolComments =true;
					strStyle = "ShadeMedRedGrid";
				}
				
				if(strHistoryFl!=null && strHistoryFl.equals("Y")){
					bolHistoryFl = true;
					strStyle = "ShadeMedYellowGrid";
				}
				if(bolHistoryFl && bolComments ){
					strStyle = "ShadeDarkBrownGrid";
				}
			}
			
		 
		if (strStyle.equals("")) {
			strStyle = super.getStyle(strColumnName);
		}
		return strStyle;
	}
}
