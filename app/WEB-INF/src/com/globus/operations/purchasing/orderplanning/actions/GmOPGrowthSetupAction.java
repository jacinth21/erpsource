package com.globus.operations.purchasing.orderplanning.actions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.purchasing.orderplanning.beans.GmDemandMasterSetupBean;
import com.globus.operations.purchasing.orderplanning.beans.GmOPDemandSetupBean;
import com.globus.operations.purchasing.orderplanning.beans.GmOPGrowthSetupBean;
import com.globus.operations.purchasing.orderplanning.forms.GmOPGrowthSetupForm;

public class GmOPGrowthSetupAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    Logger log = GmLogger.getInstance(this.getClass().getName());// Code

    log.debug("Enter into GmOPGrowthSetupAction execute method");
    GmOPGrowthSetupForm gmOPGrowthSetupForm = (GmOPGrowthSetupForm) form;
    gmOPGrowthSetupForm.loadSessionParameters(request);

    GmOPGrowthSetupBean gmOPGrowthSetupBean = new GmOPGrowthSetupBean();
    GmOPDemandSetupBean gmOPDemandSetupBean = new GmOPDemandSetupBean();
    GmDemandMasterSetupBean gmDemandMasterSetupBean = new GmDemandMasterSetupBean();
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();

    String strTodaysDate = (String) request.getSession().getAttribute("strSessTodaysDate");
    log.debug("strTodaysDate is " + strTodaysDate);

    String strDeptId =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessDeptId"));
    String strPartyId = GmCommonClass.parseNull(gmOPGrowthSetupForm.getSessPartyId());
    String strOpt = gmOPGrowthSetupForm.getStrOpt();

    String strAccessFilter = "";
    String strUpdFl = "";
    String strMsg = "";
    String strGrowthID = "";

    HashMap hmParam = new HashMap();

    HashMap hmReturn = new HashMap();
    HashMap hmDemandSheet = new HashMap();
    HashMap hmGrowthDetails = new HashMap();
    HashMap hmAccess = new HashMap();

    ArrayList alTemplates = new ArrayList();
    ArrayList alSheetTypes = new ArrayList();
    ArrayList alRegions = new ArrayList();
    ArrayList alDemandSheets = new ArrayList();    

    // The filter condition should be applied for ICS Dept users alone.
    if (strDeptId.equals("ICS"))
      strAccessFilter = this.getAccessFilter(request, response);

    hmDemandSheet.put("ACCESS_FILTER", strAccessFilter);

    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "GROWTH_SETUP"));
    strUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmOPGrowthSetupForm.setAccessFl(strUpdFl);

    String strDemandSheetId = gmOPGrowthSetupForm.getDemandSheetId();
    String strReferenceType = gmOPGrowthSetupForm.getRefType();
    log.debug("strDemandSheetId== " + strDemandSheetId);
    log.debug("strReferenceType== " + strReferenceType);

    hmParam = GmCommonClass.getHashMapFromForm(gmOPGrowthSetupForm);

    String strPnum = (String) hmParam.get("PNUM");

    hmParam.put("TODAYSDATE", strTodaysDate);

    if (strOpt.equals("save")) {
      hmReturn = gmOPGrowthSetupBean.saveGrowthDetails(hmParam);
      strGrowthID = (String) hmReturn.get("GROWTHID");
      gmOPGrowthSetupForm.setGrowthId(strGrowthID);
    }

    if (strOpt.equals("reload") || strOpt.equals("save")) {
      ArrayList alGrowthRef =
          gmOPGrowthSetupBean.fetchGrowthReference(strDemandSheetId, strReferenceType);
      boolean hasValue = false;

      strMsg =
          (strMsg.equals("") && alGrowthRef.size() == 0) ? "Reference values does not exist for selected parameters"
              : strMsg;
      gmOPGrowthSetupForm.setMsg(strMsg);
      gmOPGrowthSetupForm.setAlRefIds(alGrowthRef);

      gmOPGrowthSetupForm.reset();

      if (!strPnum.equals("")) {
        Iterator itrRefIds = gmOPGrowthSetupForm.getAlRefIds().iterator();

        while (itrRefIds.hasNext()) {
          HashMap hmGrowth = (HashMap) itrRefIds.next();
          if (GmCommonClass.parseNull((String) hmGrowth.get("REFID")).equals(strPnum)) {
            gmOPGrowthSetupForm.setReferences(strPnum);
            hasValue = true;
            break;
          }
        }

        if (!hasValue) {
          strMsg = "Reference values does not exist for selected parameters";
          gmOPGrowthSetupForm.setMsg(strMsg);
        }
      }

      log.debug("HmParam in Gm Growth Setup Action :" + hmParam);

      fetchDates(gmOPGrowthSetupForm, hmParam);

      hmGrowthDetails = gmOPGrowthSetupBean.fetchGrowthDetails(hmParam);
      gmOPGrowthSetupForm.setGrowthDetailsMap(hmGrowthDetails);

    }

    // gmOPGrowthSetupForm.setAlDemandSheets(gmOPDemandSetupBean.loadDemandSheetList(hmDemandSheet));

    alTemplates = gmDemandMasterSetupBean.fetchTemplates();
    gmOPGrowthSetupForm.setAlTemplates(alTemplates);

    alSheetTypes = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DMDTP")); // //Extra
                                                                                         // (Set,
                                                                                         // Part)
    gmOPGrowthSetupForm.setAlSheetTypes(alSheetTypes);

    alRegions = gmDemandMasterSetupBean.fetchRegions(hmParam);
    gmOPGrowthSetupForm.setAlRegions(alRegions);

    alDemandSheets = gmDemandMasterSetupBean.fetchDemandSheets(hmParam);
    gmOPGrowthSetupForm.setAlDemandSheets(alDemandSheets);

    if (alDemandSheets.size() == 1) {
      HashMap hmDemandSheetId = GmCommonClass.parseNullHashMap((HashMap) alDemandSheets.get(0));
      gmOPGrowthSetupForm.setDemandSheetId(GmCommonClass.parseNull((String) hmDemandSheetId
          .get("DMID")));
      strDemandSheetId = gmOPGrowthSetupForm.getDemandSheetId();
    }

    // This is to set the From,toDate during onLoad Condition.
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat dateFormat = new SimpleDateFormat("MMM'/'yy");
    SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    String hFromDate = "";
    String hToDate = "";

    String strFromDate = dateFormat.format(cal.getTime());

    if (gmOPGrowthSetupForm.getFromDate() == "") {
      gmOPGrowthSetupForm.setFromDate(strFromDate);
    }
    hFromDate = formatter.format(dateFormat.parse(gmOPGrowthSetupForm.getFromDate()));

    cal.add(Calendar.MONTH, 11);
    String strToDate = dateFormat.format(cal.getTime());

    if (gmOPGrowthSetupForm.getToDate() == "") {
      gmOPGrowthSetupForm.setToDate(strToDate);
    }
    hToDate = formatter.format(dateFormat.parse(gmOPGrowthSetupForm.getToDate()));
    log.debug(" hFromDate is : " + hFromDate + "\n" + " hToDate :" + hToDate);
    // Date Format to be used in the calendar .
    request.setAttribute("hidden_FromDate", hFromDate);
    request.setAttribute("hidden_ToDate", hToDate);

    if (strDemandSheetId != null && !strDemandSheetId.equals("") && !strDemandSheetId.equals("0")) {
      gmOPGrowthSetupForm.setAlRefTypes(GmCommonClass.getCodeList("GRFTP",
          gmOPGrowthSetupBean.fetchDemandType(strDemandSheetId)));
    }
    if (strOpt.equals("reload_sheet")) {
    	try{
    		gmOPGrowthSetupBean.scheduleSheetReload(hmParam);
    	}catch(Exception e){
    		if(e.getMessage() != null && e.getMessage().indexOf("^") != -1)
    		{
    			String[] Msg =new String[2];
    			Msg = e.getMessage().split("\\^");
    			request.setAttribute("hDisplayNm","&nbsp;"); //To Avoid showing the "Back to Previous Screen" Link.
    			throw new AppError(GmCommonClass.parseNull(Msg[1]),"",'S');
    		}
    	}

      strDemandSheetId = (String) hmReturn.get("DEMANDSHEETID");
      request.setAttribute("hType", "S");
      throw new AppError("Scheduling Demand sheet execution for <U><I>"+ gmOPGrowthSetupForm.getDemandSheetNm() + "</I></U> performed.", "", 'S');
    }

    log.debug("Exit from GmOPGrowthSetupAction execute method");

    return mapping.findForward("success");
  }

  private void fetchDates(GmOPGrowthSetupForm gmOPGrowthSetupForm, HashMap hmParam)
      throws Exception {
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code
    log.debug("called fetchDates function.............................................");
    HashMap hmRuleData = new HashMap();
    HashMap hmSheetDetails = new HashMap();
    GmOPDemandSetupBean gmOPDemandSetupBean = new GmOPDemandSetupBean();
    GmCommonBean gmCommonBean = new GmCommonBean();
    Date launchDate = new Date();

    int intLaunchMonth = 0;
    int intLaunchYear = 0;
    DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    String strTodaysDate = (String) hmParam.get("TODAYSDATE");
    log.debug("strTodaysDate is " + strTodaysDate);
    Date todaysDate = dateFormat.parse(strTodaysDate);

    // fetch project cutoff date to determine if project is old or new
    hmRuleData.put("RULEID", "CUTOFFPRJDT");
    hmRuleData.put("RULEGROUP", "PROJECT");
    String strPrjCutoffDt = GmCommonClass.parseNull(gmCommonBean.getRuleValue(hmRuleData));
    Date dtPrjCutoffDt = dateFormat.parse(strPrjCutoffDt);
    log.debug("dtPrjCutoffDt " + dtPrjCutoffDt);

    // fetch demandsheetid created date
    hmSheetDetails =
        gmOPDemandSetupBean.fetchDemandSheetInfo(gmOPGrowthSetupForm.getDemandSheetId());
    String strSheetCrDt = GmCommonClass.parseNull((String) hmSheetDetails.get("CREATEDDT"));
    Date dtSheetCrDt = dateFormat.parse(strSheetCrDt);
    log.debug("dtSheetCrDt " + dtSheetCrDt);

    if (dtSheetCrDt.compareTo(dtPrjCutoffDt) > 0) {
      gmOPGrowthSetupForm.setPrjStatus("NEW");
    } else
      gmOPGrowthSetupForm.setPrjStatus("OLD");

    // fetch LaunchDate
    launchDate = gmOPDemandSetupBean.fetchLaunchDate(hmParam);
    if (launchDate == null) {
      gmOPGrowthSetupForm.setLaunchDt("");
      launchDate = new Date();
      hmParam.put("LAUNCHDATE", "");
    } else {
      hmParam.put("LAUNCHDATE", dateFormat.format(launchDate));
      gmOPGrowthSetupForm.setLaunchDt(dateFormat.format(launchDate).toString());
    }

    Calendar cal = GregorianCalendar.getInstance();
    cal.setTime(launchDate);

    intLaunchMonth = cal.get(cal.MONTH);
    intLaunchMonth += 1;
    intLaunchYear = cal.get(cal.YEAR);

    if (launchDate.compareTo(todaysDate) > 0) {
      gmOPGrowthSetupForm.setValidateMonth(intLaunchMonth);
      gmOPGrowthSetupForm.setValidateYear(intLaunchYear);
    } else {
      gmOPGrowthSetupForm.setValidateMonth(GmCalenderOperations.getCurrentMonth());
      gmOPGrowthSetupForm.setValidateYear(GmCalenderOperations.getCurrentYear());
    }
  }

  private void populateForm(GmOPGrowthSetupForm gmOPGrowthSetupForm, ArrayList alGrowthDetails) {
    Iterator iteratorGrowth = alGrowthDetails.iterator();
    while (iteratorGrowth.hasNext()) {
      HashMap hmTemp = (HashMap) iteratorGrowth.next();
      int iMonth = Integer.parseInt((String) hmTemp.get("REFMONTH"));

      switch (iMonth) {
        case 1:
          gmOPGrowthSetupForm.setMonth1((String) hmTemp.get("REFVALUE"));
          gmOPGrowthSetupForm.setRefMonth1((String) hmTemp.get("REFMONTHTYPE"));
          gmOPGrowthSetupForm.setGrowthId((String) hmTemp.get("GROWTHID"));
          gmOPGrowthSetupForm.setControlValue1((String) hmTemp.get("CONTROLVALUE"));
          break;
        case 2:
          gmOPGrowthSetupForm.setMonth2((String) hmTemp.get("REFVALUE"));
          gmOPGrowthSetupForm.setRefMonth2((String) hmTemp.get("REFMONTHTYPE"));
          gmOPGrowthSetupForm.setControlValue2((String) hmTemp.get("CONTROLVALUE"));
          break;
        case 3:
          gmOPGrowthSetupForm.setMonth3((String) hmTemp.get("REFVALUE"));
          gmOPGrowthSetupForm.setRefMonth3((String) hmTemp.get("REFMONTHTYPE"));
          gmOPGrowthSetupForm.setControlValue3((String) hmTemp.get("CONTROLVALUE"));
          break;
        case 4:
          gmOPGrowthSetupForm.setMonth4((String) hmTemp.get("REFVALUE"));
          gmOPGrowthSetupForm.setRefMonth4((String) hmTemp.get("REFMONTHTYPE"));
          gmOPGrowthSetupForm.setControlValue4((String) hmTemp.get("CONTROLVALUE"));
          break;
        case 5:
          gmOPGrowthSetupForm.setMonth5((String) hmTemp.get("REFVALUE"));
          gmOPGrowthSetupForm.setRefMonth5((String) hmTemp.get("REFMONTHTYPE"));
          gmOPGrowthSetupForm.setControlValue5((String) hmTemp.get("CONTROLVALUE"));
          break;
        case 6:
          gmOPGrowthSetupForm.setMonth6((String) hmTemp.get("REFVALUE"));
          gmOPGrowthSetupForm.setRefMonth6((String) hmTemp.get("REFMONTHTYPE"));
          gmOPGrowthSetupForm.setControlValue6((String) hmTemp.get("CONTROLVALUE"));
          break;
        case 7:
          gmOPGrowthSetupForm.setMonth7((String) hmTemp.get("REFVALUE"));
          gmOPGrowthSetupForm.setRefMonth7((String) hmTemp.get("REFMONTHTYPE"));
          gmOPGrowthSetupForm.setControlValue7((String) hmTemp.get("CONTROLVALUE"));
          break;
        case 8:
          gmOPGrowthSetupForm.setMonth8((String) hmTemp.get("REFVALUE"));
          gmOPGrowthSetupForm.setRefMonth8((String) hmTemp.get("REFMONTHTYPE"));
          gmOPGrowthSetupForm.setControlValue8((String) hmTemp.get("CONTROLVALUE"));
          break;
        case 9:
          gmOPGrowthSetupForm.setMonth9((String) hmTemp.get("REFVALUE"));
          gmOPGrowthSetupForm.setRefMonth9((String) hmTemp.get("REFMONTHTYPE"));
          gmOPGrowthSetupForm.setControlValue9((String) hmTemp.get("CONTROLVALUE"));
          break;
        case 10:
          gmOPGrowthSetupForm.setMonth10((String) hmTemp.get("REFVALUE"));
          gmOPGrowthSetupForm.setRefMonth10((String) hmTemp.get("REFMONTHTYPE"));
          gmOPGrowthSetupForm.setControlValue10((String) hmTemp.get("CONTROLVALUE"));
          break;
        case 11:
          gmOPGrowthSetupForm.setMonth11((String) hmTemp.get("REFVALUE"));
          gmOPGrowthSetupForm.setRefMonth11((String) hmTemp.get("REFMONTHTYPE"));
          gmOPGrowthSetupForm.setControlValue11((String) hmTemp.get("CONTROLVALUE"));
          break;
        case 12:
          gmOPGrowthSetupForm.setMonth12((String) hmTemp.get("REFVALUE"));
          gmOPGrowthSetupForm.setRefMonth12((String) hmTemp.get("REFMONTHTYPE"));
          gmOPGrowthSetupForm.setControlValue12((String) hmTemp.get("CONTROLVALUE"));
          break;
      }

    }
  }
}
