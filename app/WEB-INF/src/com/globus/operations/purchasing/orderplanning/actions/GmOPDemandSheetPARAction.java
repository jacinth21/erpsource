package com.globus.operations.purchasing.orderplanning.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
 
import com.globus.operations.purchasing.orderplanning.beans.GmDemandMasterSetupBean;
import com.globus.operations.purchasing.orderplanning.beans.GmOPDemandSetupBean;
import com.globus.operations.purchasing.orderplanning.forms.GmOPDemandSheetSetupForm;

public class GmOPDemandSheetPARAction extends GmAction{

	public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
        try {
        GmOPDemandSheetSetupForm gmOPDemandSheetSetupForm = (GmOPDemandSheetSetupForm)form;
        gmOPDemandSheetSetupForm.loadSessionParameters(request);
        GmOPDemandSetupBean gmOPDemandSetupBean = new GmOPDemandSetupBean();
        GmDemandMasterSetupBean gmDemandMasterSetup = new GmDemandMasterSetupBean();
        GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

        String strOpt = gmOPDemandSheetSetupForm.getStrOpt();
        String strInput = gmOPDemandSheetSetupForm.getHinputStr();
        String strDemandMasterId = gmOPDemandSheetSetupForm.getDemandMasterId();
        String strUserId = gmOPDemandSheetSetupForm.getUserId();
        String strPartInput = GmCommonClass.removeSpaces(gmOPDemandSheetSetupForm.getPartNumbers());
        String strPopType = gmOPDemandSheetSetupForm.getPopType();
        String strDemandSheetId = gmOPDemandSheetSetupForm.getDemandSheetId();
        String strpartNumber = gmOPDemandSheetSetupForm.getHpartnumber();
        String strparvalue = gmOPDemandSheetSetupForm.getHparvalue();
        
        HashMap hmTemp = new HashMap();
              
        ArrayList alDemandSheetList = new ArrayList();
        ArrayList alList = new ArrayList();
        
        HashMap hmAccess = new HashMap();
        String strUpdateFl = "";
        String strPartyId = GmCommonClass.parseNull((String) gmOPDemandSheetSetupForm.getSessPartyId());
        hmAccess = GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId, "ITEM_PAR")));
        strUpdateFl = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
        gmOPDemandSheetSetupForm.setUpdatefl(strUpdateFl); 
        log.debug(GmCommonClass.getHashMapFromForm(gmOPDemandSheetSetupForm));
         
        log.debug("strInput is" + strInput);
       // request.setAttribute("PopType", strPopType);
        if (strOpt.equals("save"))
        {
           
        	 gmOPDemandSetupBean.saveDemandParDetail(strInput, strUserId);
        	  strOpt = "edit";
        	  if(strPopType.equals("pop")) 
          	{   
        		  gmOPDemandSetupBean.saveDemandSheetDetail(strDemandSheetId, strpartNumber, strparvalue );
           		  strOpt = "";
          	}
        }
        
        if (strOpt.equals("edit"))
        {
        	log.debug("strPopType is" + strPopType); 
        	alList = gmOPDemandSetupBean.loadDemandParDetail(strDemandMasterId, strPartInput);
        	log.debug(" values fetched for edit " + alList);
            request.setAttribute("RDDEMANDPAR",alList);
                        
        }
       
        }
        catch(Exception exp){
            exp.printStackTrace();
            throw new AppError(exp);
        }
        
        return mapping.findForward("success");
    }
}
