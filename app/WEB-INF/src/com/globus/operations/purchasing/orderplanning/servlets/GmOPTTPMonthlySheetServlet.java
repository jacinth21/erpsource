package com.globus.operations.purchasing.orderplanning.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.servlets.GmServlet;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.purchasing.orderplanning.beans.GmGlobalMappingReportBean;
import com.globus.operations.purchasing.orderplanning.beans.GmOPTTPReportBean;

	public class GmOPTTPMonthlySheetServlet extends GmServlet
	{

		
		public void init(ServletConfig config) throws ServletException
		{
			super.init(config);

		}

	  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
		{
			HttpSession session = request.getSession(false);
			String strComnPath = GmCommonClass.getString("GMCOMMON");
			String strForecastPath = GmCommonClass.getString("GMORDERPLANNING");
			String strDispatchTo = strForecastPath.concat("/GmOPMonthlySheetReport.jsp");
		
			HashMap hmReturn = new HashMap();
			HashMap hmResult = new HashMap();
			HashMap hmParam = new HashMap();
			ArrayList alReturn = new ArrayList();
			RowSetDynaClass resultSet = null ;
			
			GmCommonClass gmCommon = new GmCommonClass();
			GmOPTTPReportBean gmOPTTPReportBean = new GmOPTTPReportBean();
			
			HashMap hmLists;
			ArrayList alList ;
			HashMap hmParams = new HashMap();
			String strSheetNm , strStatus, strSheetType, strMonth, strYear , strTTP, strSheetOwner ;
			String strLevel = "";
			String strValue = "";
			String strAccessType = "";
			String strSheetNmRegex ;
			String strDeptId = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptSeq"));
			String strAccessFilter = "";
			 String strTTPNm = "";
			
			try
			{
				checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
				instantiate(request,response);
				
				//The filter condition should be applied for ICS Dept users alone.
				if (strDeptId.equals("ICS"))
						strAccessFilter =  this.getAccessFilter(request, response);
				
				log.debug(" strAccessFilter is "+strAccessFilter);
				hmParams.put("ACCESS_FILTER", strAccessFilter);
				
				HashMap hmPartyParam = new HashMap();
				hmPartyParam.put("PartyId", (String)session.getAttribute("strSessPartyId"));
				hmPartyParam.put("FunctionId","1015");//Corresponds to Demand Sheet Access Function Name
		
				HashMap hmPartyAccess = (new GmAccessFilter()).fetchPartyAccessPermission(hmPartyParam);				
				log.debug(" hmPartyAccess is :"+hmPartyAccess);
				
				String strUpdateFlg = GmCommonClass.parseNull((String)hmPartyAccess.get("UPDFL"));
				String strVoidFlg = GmCommonClass.parseNull((String)hmPartyAccess.get("VOIDFL"));
				String strReadFlg = GmCommonClass.parseNull((String)hmPartyAccess.get("READFL"));
				String strLimitedAccess  = "";
				if(strUpdateFlg!="" && !strUpdateFlg.equalsIgnoreCase("Y")){
					strLimitedAccess="true";
					request.setAttribute("DMNDSHT_LIMITED_ACC", strLimitedAccess);
				}
				
				if (strAction.equals("Load"))
				{
					
					hmLists = getDropdownLists(gmOPTTPReportBean);
				    request.setAttribute("HMLISTS", hmLists);
					
				    
				    
					hmParams.put("SHEETNMREGEX","" );
					hmParams.put("STATUS", "50550" );
					hmParams.put("SHEETTYPE", "0" );
					strMonth = Integer.toString( GmCalenderOperations.getCurrentMonth() ); 
					if( GmCalenderOperations.getCurrentMonth() <= 9 )
						strMonth = "0" + Integer.toString( GmCalenderOperations.getCurrentMonth() );
					
					strYear = Integer.toString( GmCalenderOperations.getCurrentYear() );
					hmParams.put("LOADMONTH", strMonth); //TODO change this
					hmParams.put("LOADYEAR", strYear ); //TODO change this
					hmParams.put("TTPNAME", "0" );
					hmParams.put("ACCESSTYPE", "102623" );
					
					hmParams.put("SHEETOWNER", "0" );
					
					log.debug("current month : "+ strMonth);
					
					/* Code is commenting for resolving the BUG-2597[no need to pull the data while comming from Left link]
					resultSet = gmOPTTPReportBean.reportMonthlySheetSummary(hmParams);
					request.setAttribute("results",resultSet); */
					
					
					request.setAttribute("SHEETNM", "");
					request.setAttribute("STATUS","50550");
					request.setAttribute("SHEETTYPE","0" );
					request.setAttribute("MONTH", strMonth);
					request.setAttribute("YEAR", strYear);
					request.setAttribute("TTP","" );
					request.setAttribute("SHEETOWNER","0" );
					request.setAttribute("ACCESSTYPE","102623" );
				}
				
				else if (strAction.equals("Reload"))
				{
					
					strSheetNm    = GmCommonClass.parseNull(request.getParameter("txt_SheetName"));
					strStatus     = GmCommonClass.parseNull(request.getParameter("Cbo_Status"));
					strSheetType  = GmCommonClass.parseNull(request.getParameter("Cbo_SheetType"));
					strMonth   = GmCommonClass.parseNull(request.getParameter("Cbo_Month"));
					strYear      = GmCommonClass.parseNull(request.getParameter("Cbo_Year"));
					strLevel  = GmCommonClass.parseNull(request.getParameter("Cbo_Level"));
					strValue   = GmCommonClass.parseNull(request.getParameter("Cbo_Value"));
					strAccessType      = GmCommonClass.parseNull(request.getParameter("Cbo_AccessType"));
					
					strTTP  = GmCommonClass.parseNull(request.getParameter("Cbo_ttp"));
					strSheetOwner  = GmCommonClass.parseNull(request.getParameter("Cbo_SheetOwner"));
					strTTPNm  = GmCommonClass.parseNull(request.getParameter("searchCbo_ttp"));
					
					log.debug("Monthly selection: "+ strSheetNm + " " + strStatus + " " + strSheetType + " " + strMonth + " " + strYear + " " + strTTP + " strAccessType **** "+strAccessType);
				
					/*hmParams=new HashMap();
					hmParams.put("PARTNUM", strSheetNm);
					hmParams.put("SEARCH", "");
										
					String strSheetNmRegex = GmCommonClass.createRegExpString(hmParams);*/
					
					if ( strSheetNm.equals(""))
						strSheetNmRegex = "";
					else
						strSheetNmRegex = "%"+strSheetNm+"%";
															
					log.debug(strSheetNmRegex);
					
					hmParams.put("SHEETNMREGEX",strSheetNmRegex );
					hmParams.put("STATUS",strStatus );
					hmParams.put("SHEETTYPE",strSheetType );
					hmParams.put("LOADMONTH",strMonth );
					hmParams.put("LOADYEAR",strYear );
					hmParams.put("TTPNAME",strTTP );
					hmParams.put("LEVEL",strLevel );
					hmParams.put("VALUE",strValue );
					hmParams.put("ACCESSTYPE",strAccessType );
					
					hmParams.put("SHEETOWNER",strSheetOwner );
					
					resultSet = gmOPTTPReportBean.reportMonthlySheetSummary(hmParams);
					//resultSet = gmOPTTPReportBean.reportMonthlySheetSummary();
					request.setAttribute("results",resultSet);
					
					request.setAttribute("SHEETNM", strSheetNm);
					request.setAttribute("STATUS",strStatus);
					request.setAttribute("SHEETTYPE",strSheetType );
					request.setAttribute("MONTH", strMonth);
					request.setAttribute("YEAR", strYear);
					request.setAttribute("TTP",strTTP);
					request.setAttribute("LEVEL",strLevel );
					request.setAttribute("VALUE",strValue );
					request.setAttribute("ACCESSTYPE",strAccessType );
					
					request.setAttribute("SHEETOWNER",strSheetOwner );
				    request.setAttribute("TTPNM",strTTPNm);
					
					hmLists = getDropdownLists(gmOPTTPReportBean);
				    request.setAttribute("HMLISTS", hmLists);
					
				}
				
				else if (strAction.equals("SaveRequest"))
				{
					
				}
				else if(strAction.equals("PrintRequest"))
				{

				}

				request.setAttribute("hAction",strAction);
				
				String strExcel = GmCommonClass.parseNull(request.getParameter("hExcel"));
				String strExport = GmCommonClass.parseNull(request.getParameter("d-5394226-e"));// display tag excel export parameter
				if (strExcel.equals("Excel") && strExport.equals("2")){ //display tag excel export
					request.setAttribute("hMode","MonthlySheetSummaryExcel");
					response.setContentType("application/vnd.ms-excel;"); //UTF-8 is not working for display tag
			    	response.setHeader("Content-disposition","attachment;filename=GmOPTTPMonthlySheetServlet.xls");
				}
				
				dispatch(strDispatchTo,request,response);
			}
			catch (Exception e)
			{
					session.setAttribute("hAppErrorMessage",e.getMessage());
					strDispatchTo = strComnPath + "/GmError.jsp";
					gotoPage(strDispatchTo,request,response);
			}// End of catch
		}//End of service method

	  	
	  	
	  	private HashMap getDropdownLists(GmOPTTPReportBean gmOPTTPReportBean) throws AppError
	  	{
	  		
	  		HashMap hmLists = new HashMap();
	  		
	  		ArrayList alList ;
	  		GmLogonBean gmLogonBean	= new GmLogonBean();
			GmGlobalMappingReportBean gmGlobalMappingReportBean = new GmGlobalMappingReportBean();
			
	  		alList = GmCommonClass.getCodeList("SHTST");
			hmLists.put("ALSTATUS", alList);
			
			alList = GmCommonClass.getCodeList("DMDTP");
			hmLists.put("ALSHEETTYPE", alList);
								
			alList =   GmCommonClass.getCodeList("MONTH");//GmCommonClass.parseNullArrayList(gmCommonBean.getMonthList());					
			hmLists.put("ALMONTH", alList);
								
		    alList =  GmCommonClass.getCodeList("YEAR");//GmCommonClass.parseNullArrayList(gmCommonBean.getYearList());
		    hmLists.put("ALYEAR", alList);
		    
			
		    alList = gmLogonBean.getEmployeeList("300", "W','P");
		    hmLists.put("ALSHEETOWNER", alList);
		    
		    alList = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("GMLVL"));
			hmLists.put("ALLEVEL", alList);
			
			alList = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("GMACTY"));
			hmLists.put("ALACCESSTYPE", alList);
			alList = GmCommonClass.parseNullArrayList(gmGlobalMappingReportBean.fetchInventoryLevelList("LEVELVALUE"));				
			hmLists.put("ALVALUE", alList);
		    return hmLists;
	  		
	  	}
	
	}// End of GmTTPMonthlySheetServlet

