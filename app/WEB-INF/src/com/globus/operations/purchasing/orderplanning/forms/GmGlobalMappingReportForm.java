package com.globus.operations.purchasing.orderplanning.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmGlobalMappingReportForm extends GmCommonForm{
private String companyId ="";
private String levelId ="";
private String valueId ="";
private String accessId ="";
private String inventoryId ="";
private String xmlString ="";

private ArrayList alCompanyId = new ArrayList();
private ArrayList alLevelId = new ArrayList();
private ArrayList alValueId = new ArrayList();
private ArrayList alAccessId = new ArrayList();
private ArrayList alInventoryId = new ArrayList();


/**
 * @return the alCompanyId
 */
public ArrayList getAlCompanyId() {
	return alCompanyId;
}
/**
 * @param alCompanyId the alCompanyId to set
 */
public void setAlCompanyId(ArrayList alCompanyId) {
	this.alCompanyId = alCompanyId;
}
/**
 * @return the alLevelId
 */
public ArrayList getAlLevelId() {
	return alLevelId;
}
/**
 * @param alLevelId the alLevelId to set
 */
public void setAlLevelId(ArrayList alLevelId) {
	this.alLevelId = alLevelId;
}
/**
 * @return the alValueId
 */
public ArrayList getAlValueId() {
	return alValueId;
}
/**
 * @param alValueId the alValueId to set
 */
public void setAlValueId(ArrayList alValueId) {
	this.alValueId = alValueId;
}
/**
 * @return the alAccessId
 */
public ArrayList getAlAccessId() {
	return alAccessId;
}
/**
 * @param alAccessId the alAccessId to set
 */
public void setAlAccessId(ArrayList alAccessId) {
	this.alAccessId = alAccessId;
}
/**
 * @return the alInventoryId
 */
public ArrayList getAlInventoryId() {
	return alInventoryId;
}
/**
 * @param alInventoryId the alInventoryId to set
 */
public void setAlInventoryId(ArrayList alInventoryId) {
	this.alInventoryId = alInventoryId;
}
/**
 * @return the companyId
 */
public String getCompanyId() {
	return companyId;
}
/**
 * @param companyId the companyId to set
 */
public void setCompanyId(String companyId) {
	this.companyId = companyId;
}
/**
 * @return the levelId
 */
public String getLevelId() {
	return levelId;
}
/**
 * @param levelId the levelId to set
 */
public void setLevelId(String levelId) {
	this.levelId = levelId;
}
/**
 * @return the valueId
 */
public String getValueId() {
	return valueId;
}
/**
 * @param valueId the valueId to set
 */
public void setValueId(String valueId) {
	this.valueId = valueId;
}
/**
 * @return the accessId
 */
public String getAccessId() {
	return accessId;
}
/**
 * @param accessId the accessId to set
 */
public void setAccessId(String accessId) {
	this.accessId = accessId;
}
/**
 * @return the inventoryId
 */
public String getInventoryId() {
	return inventoryId;
}
/**
 * @param inventoryId the inventoryId to set
 */
public void setInventoryId(String inventoryId) {
	this.inventoryId = inventoryId;
}
/**
 * @return the xmlString
 */
public String getXmlString() {
	return xmlString;
}
/**
 * @param xmlString the xmlString to set
 */
public void setXmlString(String xmlString) {
	this.xmlString = xmlString;
}

}
