package com.globus.operations.purchasing.orderplanning.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;
 
import com.globus.operations.purchasing.orderplanning.beans.GmDemandMasterSetupBean;
import com.globus.operations.purchasing.orderplanning.beans.GmOPDemandSetupBean;
import com.globus.operations.purchasing.orderplanning.forms.GmPartOTNSetupForm;
import com.globus.sales.event.beans.GmEventSetupBean;

public class GmPartOTNSetupAction extends GmAction{

	public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
      
        GmPartOTNSetupForm gmPartOTNSetupForm = (GmPartOTNSetupForm)form;
        gmPartOTNSetupForm.loadSessionParameters(request);
        GmEventSetupBean gmEventSetupBean 	= new GmEventSetupBean();
        GmDemandMasterSetupBean gmDemandMasterSetupBean = new GmDemandMasterSetupBean();
        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
        
        HashMap hmParam = new HashMap();        
        ArrayList alDemandSheetList = new ArrayList();
        ArrayList alOTNList = new ArrayList();
        ArrayList alTemplates = new ArrayList();
		ArrayList alSheetTypes = new ArrayList();
		ArrayList alRegions = new ArrayList();
		
        String strPartyId 	 = GmCommonClass.parseNull((String) gmPartOTNSetupForm.getSessPartyId());
        String strOpt = gmPartOTNSetupForm.getStrOpt();       
        String strDemandMasterId = "";
        
        hmParam=GmCommonClass.getHashMapFromForm(gmPartOTNSetupForm);      
        
        // to save the OTN part values
        if (strOpt.equals("save"))
        {
              gmDemandMasterSetupBean.savePartOTN(hmParam);
              
              String strTemplateId = GmCommonClass.parseNull((String)hmParam.get("TEMPLATEID"));
              String strSheetTypeId = GmCommonClass.parseNull((String)hmParam.get("SHEETTYPEID"));
              String strRegionId = GmCommonClass.parseNull((String)hmParam.get("REGIONID"));
              strDemandMasterId = GmCommonClass.parseNull((String)hmParam.get("DEMANDMASTERID"));
      	      String strPartInput = GmCommonClass.parseNull((String)hmParam.get("PARTNUMBERS"));
        	  String strRedirectURL = "/gmPartOTNSetupAction.do?strOpt=edit&demandMasterId="+strDemandMasterId+"&partNumbers="+strPartInput+"&templateId="+strTemplateId+"&sheetTypeId="+strSheetTypeId+"&regionId="+strRegionId;
  			  ActionRedirect actionRedirect = new ActionRedirect(strRedirectURL);
  		      return actionRedirect;
        }
        
    	
        //fetching to load editable demand sheet dropdown
        alDemandSheetList = gmDemandMasterSetupBean.fetchEditableDemandSheet(hmParam);
        gmPartOTNSetupForm.setAlDemandSheetName(alDemandSheetList);
        
    	if(alDemandSheetList.size() == 1){
    		HashMap hmDemandSheetId = GmCommonClass.parseNullHashMap((HashMap) alDemandSheetList.get(0));
    		gmPartOTNSetupForm.setDemandMasterId(GmCommonClass.parseNull((String)hmDemandSheetId.get("DMID")));
    		String strDemandSheetId = GmCommonClass.parseNull((String)hmDemandSheetId.get("DMID"));
    		hmParam.put("DEMANDMASTERID",strDemandSheetId);
    		alOTNList = gmDemandMasterSetupBean.fetchOTNParts(hmParam);
        	gmPartOTNSetupForm.setAlOTNList(alOTNList);
        	
    	}
        
        // to fetch the OTN part values
        if (strOpt.equals("edit"))
        {
        	strDemandMasterId = GmCommonClass.parseNull((String)hmParam.get("DEMANDMASTERID"));
        	if(!strDemandMasterId.equals("0")){
	        	alOTNList = gmDemandMasterSetupBean.fetchOTNParts(hmParam);
	        	gmPartOTNSetupForm.setAlOTNList(alOTNList);
        	}
        }
        
		alTemplates = gmDemandMasterSetupBean.fetchTemplates();
		gmPartOTNSetupForm.setAlTemplates(alTemplates);
		
		alSheetTypes = GmCommonClass.parseNullArrayList((ArrayList)GmCommonClass.getCodeList("DMDTP")); ////Extra (Set, Part)
		
		/* The following code is added for resolving the BUG-2593 [No need of "Sales" value in Sheet Type Dropdown]*/
		GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
		gmSearchCriteria.addSearchCriteria("CODEID", "40020", GmSearchCriteria.NOTEQUALS);
		alSheetTypes = (ArrayList)gmSearchCriteria.query(alSheetTypes);
		
		gmPartOTNSetupForm.setAlSheetTypes(alSheetTypes);
		
    	alRegions = gmDemandMasterSetupBean.fetchRegions(hmParam);
    	gmPartOTNSetupForm.setAlRegions(alRegions);

        //fetching access for submitting data
        String strAccessFl = gmEventSetupBean.getEventAccess(strPartyId, "EVENT", "DEMAND_SHEET_OTN");
        gmPartOTNSetupForm.setAccessFl(strAccessFl);
        
        
        return mapping.findForward("success");
    }
}
