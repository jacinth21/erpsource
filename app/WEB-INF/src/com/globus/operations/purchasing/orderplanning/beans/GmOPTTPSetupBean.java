package com.globus.operations.purchasing.orderplanning.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.common.util.GmWSUtil;
import com.globus.common.beans.GmLogger; 
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.util.GmTemplateUtil;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;


public class GmOPTTPSetupBean extends GmBean
{
	public GmOPTTPSetupBean()
	{
		
		super(GmCommonClass.getDefaultGmDataStoreVO());

	}
	
	public GmOPTTPSetupBean(GmDataStoreVO gmDataStoreVO) throws AppError 
	{
		super(gmDataStoreVO);
	}

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    
    /**
     * loadTTPList - This method will be used to fetch the TTP
     * @param hmParam - to be used later for any filters
     * @return ArrayList - will be used by drop down tag
     * @exception AppError
     */
    public ArrayList loadTTPList(HashMap hmParam) throws AppError
    {
        ArrayList alTTPList = new ArrayList();

        GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
        gmDBManager.setPrepareString("gm_pkg_oppr_ttp.gm_fc_fch_ttplist",1);
        gmDBManager.registerOutParameter(1,OracleTypes.CURSOR);
        gmDBManager.execute();
        
        alTTPList = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(1)));
        gmDBManager.close();

        return alTTPList;
    }
    /**
     * loadTTPLevelList - This method will be used to fetch the TTP
     * @param hmParam - to be used later for any filters
     * @return ArrayList - will be used by drop down tag
     * @exception AppError
     */
    public ArrayList loadTTPLevelList(String strCompanyID) throws AppError
    {
        ArrayList alTTPLevelList = new ArrayList();

        GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
        gmDBManager.setPrepareString("gm_pkg_oppr_ttp.gm_fc_fch_ttplevel",2);
        gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
        gmDBManager.setString(1,strCompanyID);
        gmDBManager.execute();
        
        alTTPLevelList = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2)));
        gmDBManager.close();

        return alTTPLevelList;
    }
    /**
     * fetchTTPGroupMap - This method will fetch the TTP - Demand Sheet mapping info
     * @param hmParam - strTTPId
     * @exception AppError
     */
    public HashMap fetchTTPGroupMap (String strTTPId)throws AppError
    {
        HashMap hmReturn = new HashMap();
        ArrayList alUnselected = new ArrayList();
        ArrayList alSelected = new ArrayList();

        GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
        
        gmDBManager.setPrepareString("gm_pkg_oppr_ttp.gm_fc_fch_ttp_dstemplate_map",3);
        
        gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
        gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
        
        gmDBManager.setString(1,strTTPId);
        
        gmDBManager.execute();
        
        alUnselected = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
        alSelected = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3)); 
        gmDBManager.commit();
        
        hmReturn.put("ALSELECTED",alSelected); 
        hmReturn.put("ALUNSELECTED",alUnselected);
        return hmReturn;
    }
    
    /**
     * fetchEditTTPGroupMap - This method will fetch the TTP - Demand Sheet mapping info
     * @param hmParam - strTTPId
     * @exception AppError
     */
    public HashMap fetchEditTTPGroupMap (String strTTPId)throws AppError
    {
        HashMap hmReturn = new HashMap();
        GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
            StringBuffer sbQuery = new StringBuffer();
            sbQuery.append(" SELECT C4050_TTP_ID TTPID , C4050_TTP_NM TTPNAME  ");
            sbQuery.append(" , C4050_PRIMARY_USER TTPOWNER,C901_TTP_CATEGORY CATEGORY");//PMT-49826 Get saved Category id and set to form
            sbQuery.append(" FROM T4050_TTP ");
            sbQuery.append(" WHERE  C4050_VOID_FL IS NULL ");
            sbQuery.append(" AND C4050_TTP_ID =  ?  ");
            
            log.debug("Query inside fetchGroupPartInfo is " + sbQuery.toString());

            gmDBManager.setPrepareString(sbQuery.toString());
            gmDBManager.setString(1,strTTPId);
            hmReturn = gmDBManager.querySingleRecord();
            return hmReturn;
    }
    
    /**
     * saveTTPGrouping - This method will be save / update the demand sheets associated to a TTP 
     * @param hmParam - parameters to be saved / updated
     * @exception AppError
     */
    public String saveTTPGrouping (HashMap hmParam)throws AppError
    {
        String strTTPIdFromDb = "";
        GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
        
        String strTTPId = GmCommonClass.parseNull((String)hmParam.get("TTPID"));
        String strTTPName = GmCommonClass.parseNull((String)hmParam.get("TTPNAME"));
        String strPrimaryUser = (String)hmParam.get("TTPOWNER");
        String strUserId = (String)hmParam.get("USERID");
        String strInputString = (String)hmParam.get("INPUTSTRING");
        String strCategory = GmCommonClass.parseNull((String)hmParam.get("CATEGORY"));//PMT-49826 -Save category id in t4050table
 
        gmDBManager.setPrepareString("gm_pkg_oppr_ttp.gm_fc_sav_ttpdsmap",7);
        
        gmDBManager.registerOutParameter(7,java.sql.Types.CHAR);
        
        gmDBManager.setString(1,strTTPId);
        gmDBManager.setString(2,strTTPName);
        gmDBManager.setString(3,strPrimaryUser);
        gmDBManager.setString(4,strUserId);
        gmDBManager.setString(5,strInputString);
        gmDBManager.setString(6,strCategory);
        
        gmDBManager.execute();
        
        strTTPIdFromDb = gmDBManager.getString(7);
        gmDBManager.commit();
        return strTTPIdFromDb;
    } 
   
	/**fetchTTPApprovalDtls:This Method used to fetchTTPApproval Details
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public ArrayList fetchTTPApprovalDtls(HashMap hmParam) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO());
		
		ArrayList alReturn = new ArrayList();
		//Initialize
		String strTTPId = "";
		String strStatusId = "";
		String strLockPeriod = "";
		String strCategoryId = "";
		String strMonth="";
		String strYear="";
		
		strTTPId = GmCommonClass.parseZero((String) hmParam.get("TTPID"));
		strStatusId = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
		strMonth=GmCommonClass.parseNull((String) hmParam.get("TTPMONTH"));
	    strYear=GmCommonClass.parseNull((String) hmParam.get("TTPYEAR"));		
		strCategoryId = GmCommonClass.parseZero((String) hmParam.get("CATEGORY"));
		strLockPeriod = strMonth.concat("/").concat(strYear);
		
		gmDBManager.setPrepareString("gm_pkg_oppr_ttp_finalize_setup.gm_fch_approval_dtls", 5);
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		gmDBManager.setString(1, strTTPId);
		gmDBManager.setString(2, strStatusId);
		gmDBManager.setString(3, strLockPeriod);
		gmDBManager.setString(4, strCategoryId);
		gmDBManager.execute();
		
		//Get the ttp summary screen approval details
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
		gmDBManager.close();
		
		return alReturn;

	}
	
	
	/**This method used to processTTPApproval using JMS 
	 * @param objMessageObject
	 * @param jobDataMap
	 * @throws AppError
	 */
	public void processTTPApproval(String strUserId,String strLockPeriod) throws AppError {
		
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		gmDBManager.setPrepareString("gm_pkg_oppr_ttp_finalize_setup.gm_process_ttp_approval", 2);
		gmDBManager.setString(1, strLockPeriod);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();

	}
	
	 
	/**saveTTPApprovalDtls:This method used to save approval details
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public ArrayList saveTTPApprovalDtls(HashMap hmParam) throws AppError {

		    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
	       //Intialize
		    String strInputStr="";
		    String strUserID="";
		    String strMonth="";
			String strYear="";
			String strLockPeriod = "";
			String strInvalidTTPName="";
			ArrayList alErrorTTPName = new ArrayList ();
			
		    strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
			strMonth=GmCommonClass.parseNull((String) hmParam.get("TTPMONTH"));
		    strYear=GmCommonClass.parseNull((String) hmParam.get("TTPYEAR"));
		    strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		    strLockPeriod = strMonth.concat("/").concat(strYear);
		
		    String strAction = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
		    gmDBManager.setPrepareString("gm_pkg_oppr_ttp_finalize_setup.gm_sav_ttp_approval_dtls", 5);
			gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		    gmDBManager.setString(1, strInputStr);
		    gmDBManager.setString(2, strLockPeriod);
		    gmDBManager.setString(3, strAction);
		    gmDBManager.setString(4, strUserID);
		   
		    gmDBManager.execute();
		    //Get Invalid TTP Name
		    alErrorTTPName = GmCommonClass.parseNullArrayList((ArrayList)gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5)));
	
			gmDBManager.commit();
			
			return alErrorTTPName;
		  }
	

		/**processTTPApprovalJMS:This method used to send message for JMS Call
		 * @param hmParam
		 * @throws AppError
		 */
		public void processTTPApprovalJMS(HashMap hmParam) throws AppError {
		    
			GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();	      
		    String strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_TTP_SUMMARY_QUEUE"));
		    hmParam.put("QUEUE_NAME", strQueueName);
		    // ORDER_CONSUMER_CLASS is mentioned in JMSConsumerConfig. properties file.This key contain the
		    // path for : com.globus.jms.consumers.processors.GmTTPApprovalConsumerJob
		    String strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("TTP_SUMMARY_CONSUMER_CLASS"));
		    hmParam.put("HMHOLDVALUES", hmParam);
		    hmParam.put("CONSUMERCLASS", strConsumerClass);
		    log.debug("strQueueName=>"+strQueueName+" strConsumerClass=>"+strConsumerClass+" hmParam=>>>"+hmParam);
		    gmConsumerUtil.sendMessage(hmParam);      
		}
		
		
		/**fetchTTPFailedDtls;This method used to fetchTTPFailedDetails
		 * @param hmParam
		 * @return
		 * @throws AppError
		 */
		public ArrayList fetchTTPFailedDtls(HashMap hmParam) throws AppError {

			GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
			ArrayList alReturn = new ArrayList();
			//Intialize
			String strTTPDetailId = "";
			
			
			strTTPDetailId = GmCommonClass.parseZero((String) hmParam.get("TTPDETAILID"));
			gmDBManager.setPrepareString("gm_pkg_oppr_ttp_finalize_setup.gm_fch_ttp_failed_dtls", 2);
			gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
			gmDBManager.setString(1, strTTPDetailId);
			gmDBManager.execute();
		
			//Get failed Information
			alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
			gmDBManager.commit();
			gmDBManager.close();
			
			return alReturn;

		}	
	
		 /**
		   * 
		   * generateOutput - Array list converted to XML string using (VM)
		   * 
		   * @param alGridData
		   * @param hmDtls
		   * @return String
		   * @throws AppError
		   */
		  public String generateOutput(ArrayList alGridData, HashMap hmDtls) throws AppError {
		    GmTemplateUtil gmTemplateUtil = new GmTemplateUtil();
		    String strTemplateName = GmCommonClass.parseNull((String) hmDtls.get("TEMPLATE_NM"));
		    String strSessCompanyLocale = GmCommonClass.parseNull((String) hmDtls.get("STRSESSCOMPANYLOCALE"));
		    String strLabelProperties = GmCommonClass.parseNull((String) hmDtls.get("LABEL_PROPERTIES_NM"));
		   
		    gmTemplateUtil.setDataList("alGridData", alGridData);
		    gmTemplateUtil.setDataMap("hmApplnParam", hmDtls);
		    gmTemplateUtil.setTemplateName(strTemplateName);
		    gmTemplateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strLabelProperties,strSessCompanyLocale));
		    gmTemplateUtil.setTemplateSubDir("operations/purchasing/templates");
		    return gmTemplateUtil.generateOutput();

		  }
		  
		  
		/**
		 * updateTTPDetailsStatus - This method used to update the TTP details
		 * status. Status to be update on approval/Approval failed/ Locked/ Locked
		 * failed Based on condition/operation
		 * 
		 * @param hmParam
		 * @throws AppError
		 */
		public void updateTTPDetailsStatus(HashMap hmParam) throws AppError {
	
			GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
			String strTTPId = GmCommonClass.parseNull((String) hmParam.get("TTPID"));
			String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
			String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
			String strDate = GmCommonClass.parseNull((String) hmParam.get("MONTHID")) + "/"	+ GmCommonClass.parseNull((String) hmParam.get("YEARID"));
			
			gmDBManager.setPrepareString("gm_pkg_oppr_ttp_finalize_setup.gm_upd_ttp_details_status", 3);
			gmDBManager.setString(1, strTTPId);
			gmDBManager.setString(2, strStatus);
			gmDBManager.setString(3, strUserId);
			gmDBManager.execute();
			gmDBManager.commit();
	
		}
	
		/**
		 * processTTPLockDetailsJMS - This method used to save ttp lock details using JMS call
		 * 
		 * @param hmParam
		 * @throws AppError
		 */
		public void processTTPLockDetailsJMS(HashMap hmParam) throws AppError {
	
			log.debug(" values inside hmParam " + hmParam);
	
			GmOPForecastTransBean gmOPForecastTransBean = new GmOPForecastTransBean(getGmDataStoreVO());
			ArrayList alfetchLockDetails = null;
			String strTTPDetailsId = "";
			String strInventoryLockId = "";
			String strUserId = "";
			HashMap hmLockInfo = null;
			GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
	
			// 1. To fetch the TTP details
			// 26240573 Lock In-Progress (to setting the status)
			
			hmParam.put("STATUS", "26240573");
			alfetchLockDetails = GmCommonClass.parseNullArrayList(fetchTTPApprovalDtls(hmParam));
			log.error(" alfetchLockDetails ==> " + alfetchLockDetails.size());
	
			// 2. Loop the TTP details
	
			Iterator itrLockDetails = alfetchLockDetails.iterator();
			HashMap hmLockDtlsRow = new HashMap();
	
			while (itrLockDetails.hasNext()) {
	
				hmLockDtlsRow = GmCommonClass.parseNullHashMap((HashMap) itrLockDetails.next());
				//log.debug(" hmLockDtlsRow ==> " + hmLockDtlsRow);
				// to get the lock details information
				strTTPDetailsId = GmCommonClass.parseNull((String) hmLockDtlsRow.get("TTP_DETAIL_ID"));
				hmLockInfo = GmCommonClass.parseNullHashMap(fetchTTPDetailsLockInformation(strTTPDetailsId, gmDBManager));
				strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
				hmLockInfo.put("USERID", GmCommonClass.parseNull((String) hmParam.get("USERID")));
				// to get the Inventory Lock id
				strInventoryLockId = GmCommonClass.parseNull((String) hmLockInfo.get("INVENTORYID"));
				
				// Based on T4052 status we are driven, TTP summary. In case if status not updated properly in T4052 then, we need to avoid the Lock and generate.
				// checking Inventory ID not empty then only calling to Lock and generate.
				
				if(!strInventoryLockId.equals("")){
					//log.debug(" hmLockInfo ==> " + hmLockInfo);
					// saveTTPLockDetails (hmLockInfo);
					try {
						log.debug("Before calling the saveTTPLinkData ");
						
						gmOPForecastTransBean.saveTTPLinkData(hmLockInfo);
						
						log.debug("After calling the saveTTPLinkData ");
						//Commenting the below call to prevent push vendor capacity in TTP Lock(PMT-46985)
						//gmOPForecastTransBean.pushVendorCapacityDtls(strTTPDetailsId,strUserId);
												
					} catch (Exception ex) {

						log.error("TTP Details id ==> "+strTTPDetailsId +" Inventory Id ==> "+ strInventoryLockId + " *** Error details strTTPDetailsId >> " + ex.toString() );
						
					} // end try catch block
					
				} // end Inventory check
	
			}// end while loop
			
			// close the DB connection
			
			gmDBManager.close();
		}
	
		/**
		 * This method used to fetch the TTP lock information.
		 * Based on the data we are calling the existing method and do the Lock.
		 *   Demand sheet ids, Demand type and Order Qty string
		 *   
		 * @param strTTPDetailId
		 * @param gmDBManager
		 * @return
		 * @throws AppError
		 */
		
		private HashMap fetchTTPDetailsLockInformation(String strTTPDetailId, GmDBManager gmDBManager)
				throws AppError {
			HashMap hmReturn = new HashMap();
			log.debug(" TTP Details ids " + strTTPDetailId);
			
			gmDBManager.setPrepareString("gm_pkg_oppr_ttp_finalize_setup.gm_fch_ttp_details_lock_info",	3);
			gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
			gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
			gmDBManager.setString(1, strTTPDetailId);
			gmDBManager.execute();
			
			String strInputStr = GmCommonClass.parseNull(gmDBManager.getString(2));
			hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
			
			hmReturn.put("ORDSTR", strInputStr);
			//log.debug(" hmReturn ==> " + hmReturn);
			return hmReturn;
	
		}
				 
		  
}
