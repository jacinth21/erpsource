package com.globus.operations.purchasing.orderplanning.displaytag;

import java.util.HashMap;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;
import  com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonConstants;
import com.globus.common.beans.GmLogger;
 

public class DTOPRequestDetailWrapper extends TableDecorator
{
         
    private String strRequestId;
    private String strRequestfor;
	private String strCallFlag;
	private String strHistory = "";
	private String strType ="";
    HashMap hmSearchResult = new HashMap ();
	private String strImgSrc;
	private String strClogFlag = "";
	private String strSetId = "";
	private String strDmTypeId = "";
	String strImagePath = GmCommonClass.getString("GMIMAGES");
    
    private DynaBean db ;
    
    public StringBuffer  strValue = new StringBuffer();
  
 Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS); 
    /**
     * Returns the string for RequestId field icon.
     * @return String
     */
 
    
    public String getREQUEST_ID()
    {
        db =    (DynaBean) this.getCurrentRowObject();               
        strRequestId   = GmCommonClass.parseNull((String)db.get("REQUEST_ID"));
        strRequestfor   = GmCommonClass.parseNull((String)db.get("REQUEST_FOR"));
        String strConId =  GmCommonClass.parseNull((String)db.get("CONSIGNMENT_ID"));
        strSetId = GmCommonClass.parseNull((String)db.get("SET_ID"));
        strClogFlag = String.valueOf(db.get("WLOG"));
        strDmTypeId = String.valueOf(db.get("DEMANDTYPEID"));
        
        String strJNDIConnection = GmCommonClass.parseNull(GmCommonClass.getString("JNDI_CONNECTION")); 
        strValue.setLength(0);
                 
    	strValue.append("  <img style='cursor:hand' src='/images/edit_icon.gif'");
    	strValue.append("  border='0' onClick=javascript:fnMediateReqEdit('");
    	strValue.append(strRequestId);
    	strValue.append("','");
    	strValue.append(strRequestfor);
    	strValue.append("','");
    	strValue.append(strSetId);
    	strValue.append("','");
    	strValue.append(strDmTypeId);
    	strValue.append("'); />" );
    	strValue.append(" <img style='cursor:hand' width='15' height='15'");
    	strValue.append(" title='Click to add comments' border='0' ");
    	strValue.append(" onClick=javascript:fnOpenReqLog('");
    	strValue.append(strJNDIConnection);
    	strValue.append("','");
    	strValue.append(strRequestId);
    	 if(strClogFlag.equals("N"))
    	{
    		
    		strValue.append("'); src='/images//phone_icon.jpg' />" );
    	}
    	else
    	{
    		strValue.append("'); src='/images//phone-icon_ans.gif' />" );
    	}
    	strValue.append("<a href= javascript:fnRequestDetails('");
    	strValue.append(strRequestId);
    	strValue.append("','");
    	strValue.append(strConId);
    	strValue.append("')>");
    	strValue.append(strRequestId);
    	strValue.append("</a>");
    	
    	return strValue.toString();
                
    }
    
    public String getHISTORY()	
    {
        db =    (DynaBean) this.getCurrentRowObject();
        
        strHistory = String.valueOf(db.get("HISTORY_FL"));
        
        
       // System.out.println("PARVALUE : "+strPar);        
        strValue.setLength(0);
         
      
        if (strHistory.equals("Y") )
        {
        	strValue.append("    <img style='cursor:hand' src='/images/icon_History.gif'" +  
             "border='0'" + 
                 "onClick=\"javascript:fnParHistory( '"+ strRequestId +"','" + 1237 + "') \" />" ); 
        }
        
        return strValue.toString();
        //return "<input type = text name = parvalue size = 10 value="+strPar+">";
        
    }

    public String getCONSIGNMENT_ID ()
    {
    	 db =    (DynaBean) this.getCurrentRowObject();
	    StringBuffer strValue = new StringBuffer(); 	    
	    String strConId =  GmCommonClass.parseNull((String)db.get("CONSIGNMENT_ID"));
	    strValue.append("  <a href= javascript:fnConsignmentVer('" + strConId +"')>" +strConId + "</a>") ;
	    return strValue.toString();
	}
}
