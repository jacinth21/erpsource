

package com.globus.operations.purchasing.orderplanning.forms;

import com.globus.common.forms.GmCommonForm;
import java.util.ArrayList;
import java.util.List;

public class GmPartLeadTimeSetupFrom extends GmCommonForm 
{
   private String pnum = "";
   private ArrayList projectList = new ArrayList();
   private List returnList = new ArrayList();  
   
   private String projectListID = "";
   private String hinputStr = "";
   private String accessFl = "";
   private String defaultLeadWeek = "";
   private String cbo_search = "";
   private ArrayList alWildSearch = new ArrayList();
 
public String getHinputStr() {
	return hinputStr;
}

public void setHinputStr(String hinputStr) {
	this.hinputStr = hinputStr;
}

public String getPnum() {
	return pnum;
}

public void setPnum(String pnum) {
	this.pnum = pnum;
}

public ArrayList getProjectList() {
	return projectList;
}

public void setProjectList(ArrayList projectList) {
	this.projectList = projectList;
}
 

public String getProjectListID() {
	return projectListID;
}

public void setProjectListID(String projectListID) {
	this.projectListID = projectListID;
}

public List getReturnList() {
	return returnList;
}

public void setReturnList(List returnList) {
	this.returnList = returnList;
}

public String getAccessFl() {
	return accessFl;
}

public void setAccessFl(String accessFl) {
	this.accessFl = accessFl;
}

/**
 * @return the defaultLeadWeek
 */
public String getDefaultLeadWeek() {
	return defaultLeadWeek;
}

/**
 * @param defaultLeadWeek the defaultLeadWeek to set
 */
public void setDefaultLeadWeek(String defaultLeadWeek) {
	this.defaultLeadWeek = defaultLeadWeek;
}

/**
 * @return the cbo_search
 */
public String getCbo_search() {
	return cbo_search;
}

/**
 * @param cboSearch the cbo_search to set
 */
public void setCbo_search(String cboSearch) {
	cbo_search = cboSearch;
}

/**
 * @return the alWildSearch
 */
public ArrayList getAlWildSearch() {
	return alWildSearch;
}

/**
 * @param alWildSearch the alWildSearch to set
 */
public void setAlWildSearch(ArrayList alWildSearch) {
	this.alWildSearch = alWildSearch;
} 

}
