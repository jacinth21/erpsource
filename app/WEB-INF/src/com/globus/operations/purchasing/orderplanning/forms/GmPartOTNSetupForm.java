package com.globus.operations.purchasing.orderplanning.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmPartOTNSetupForm extends GmCommonForm
{
    
    private String partNumbers = "";
    private String hinputStr = "";
    private String demandMasterId = "";
    private String accessFl = "";
    private String templateId = "";
	private String sheetTypeId = "";
	private String regionId = "";
    
	private ArrayList alDemandSheetName = new ArrayList();
	private ArrayList alOTNList = new ArrayList();
	private ArrayList alTemplates = new ArrayList();
	private ArrayList alSheetTypes = new ArrayList();
	private ArrayList alRegions = new ArrayList();
	
	public String getPartNumbers() {
		return partNumbers;
	}

	public void setPartNumbers(String partNumbers) {
		this.partNumbers = partNumbers;
	}

	public String getHinputStr() {
		return hinputStr;
	}

	public void setHinputStr(String hinputStr) {
		this.hinputStr = hinputStr;
	}

	public String getDemandMasterId() {
		return demandMasterId;
	}

	public void setDemandMasterId(String demandMasterId) {
		this.demandMasterId = demandMasterId;
	}

	public String getAccessFl() {
		return accessFl;
	}

	public void setAccessFl(String accessFl) {
		this.accessFl = accessFl;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public String getSheetTypeId() {
		return sheetTypeId;
	}

	public void setSheetTypeId(String sheetTypeId) {
		this.sheetTypeId = sheetTypeId;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	public ArrayList getAlDemandSheetName() {
		return alDemandSheetName;
	}

	public void setAlDemandSheetName(ArrayList alDemandSheetName) {
		this.alDemandSheetName = alDemandSheetName;
	}
  
	public ArrayList getAlOTNList() {
		return alOTNList;
	}

	public void setAlOTNList(ArrayList alOTNList) {
		this.alOTNList = alOTNList;
	}

	public ArrayList getAlTemplates() {
		return alTemplates;
	}

	public void setAlTemplates(ArrayList alTemplates) {
		this.alTemplates = alTemplates;
	}

	public ArrayList getAlSheetTypes() {
		return alSheetTypes;
	}

	public void setAlSheetTypes(ArrayList alSheetTypes) {
		this.alSheetTypes = alSheetTypes;
	}

	public ArrayList getAlRegions() {
		return alRegions;
	}

	public void setAlRegions(ArrayList alRegions) {
		this.alRegions = alRegions;
	}

    
}
