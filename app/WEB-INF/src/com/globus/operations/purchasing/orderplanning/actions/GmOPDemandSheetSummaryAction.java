package com.globus.operations.purchasing.orderplanning.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.operations.purchasing.orderplanning.beans.GmOPDemandReportBean;
import com.globus.operations.purchasing.orderplanning.beans.GmOPDemandSetupBean;
import com.globus.operations.purchasing.orderplanning.beans.GmOPForecastReportBean;
import com.globus.operations.purchasing.orderplanning.forms.GmOPDemandSheetSummaryForm;

public class GmOPDemandSheetSummaryAction extends GmAction
{
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.

    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
	try {
		GmOPDemandSheetSummaryForm gmOPDemandSheetSummaryForm = (GmOPDemandSheetSummaryForm)form;
	    gmOPDemandSheetSummaryForm.loadSessionParameters(request);
	    GmOPDemandSetupBean gmOPDemandSetupBean = new GmOPDemandSetupBean();
	    GmCommonBean gmCommonBean = new GmCommonBean();
	    GmOPForecastReportBean gmOPForecastReportBean = new GmOPForecastReportBean(); 
       
 	

		    
	    // String strOpt = validateDate(gmOPDemandSheetSummaryForm);
	    String strOpt = gmOPDemandSheetSummaryForm.getStrOpt();
	    String strDemandSheetId = gmOPDemandSheetSummaryForm.getDemandSheetId();
	    String strDemandSheetMonthId = gmOPDemandSheetSummaryForm.getDemandSheetMonthId();
	    String refID = gmOPDemandSheetSummaryForm.getRefId();
    	String month = gmOPDemandSheetSummaryForm.getMonth();
    	String checkSetDtl = gmOPDemandSheetSummaryForm.getCheckSetDtl();
    	String checkDesc = gmOPDemandSheetSummaryForm.getCheckDesc();
    	String strMonthYear = gmOPDemandSheetSummaryForm.getMonthYear();
	    
	    String strPartInput = "";
	    HashMap hmDemandSheet = new HashMap();   
	    HashMap hmTemp = new HashMap();
	    HashMap hmParam = new HashMap();
	    HashMap hmValues = new HashMap();
	    
	    HashMap hmapValue = new HashMap();
		
	    
	    ArrayList alDemandSheetList = new ArrayList();
	    ArrayList alMonth = new ArrayList();
	    ArrayList alYear = new ArrayList();
	    ArrayList alGroupList = new ArrayList();
	    ArrayList alidmon = new ArrayList();
	    List lResult = null;
	    boolean blnApprove = false;
	    
	    String strAccessType = "";
    	String strLevelID  = "";
	    
	    gmOPDemandSheetSummaryForm.setHideSheetCommentsTab("YES"); // THis is to hide the Sheet Comments tab for any Demand Sheet.
	    gmOPDemandSheetSummaryForm.setHideMonthlyCommentsTab("YES"); // THis is to hide the Monthly Comments tab for any Demand Sheet.

	    RowSetDynaClass rdDemandSheetReport = null;
	    
		String strDeptId = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessDeptId"));
		String strAccessFilter = "";

		//The filter condition should be applied for ICS Dept users alone.
		if (strDeptId.equals("ICS"))
			strAccessFilter =  this.getAccessFilter(request, response);
		
	    hmTemp.put("ACCESS_FILTER", strAccessFilter);
	 
        // Code to get the Party Access Condition.
        HttpSession session = request.getSession(false);
        HashMap hmPartyParam = new HashMap();
		hmPartyParam.put("PartyId", (String)session.getAttribute("strSessPartyId"));
		hmPartyParam.put("FunctionId","1015");//Corresponds to Demand Sheet Access Function Name

		HashMap hmPartyAccess = (new GmAccessFilter()).fetchPartyAccessPermission(hmPartyParam);
		
		String strUpdateFlg = GmCommonClass.parseNull((String)hmPartyAccess.get("UPDFL"));
		String strVoidFlg = GmCommonClass.parseNull((String)hmPartyAccess.get("VOIDFL"));
		String strReadFlg = GmCommonClass.parseNull((String)hmPartyAccess.get("READFL"));
		
		// If the party is not having update access for the the function, the will have limited access in Demand Sheet 
		// summary and in Monthly demand sheet.
		String strLimitedAccess  = "";
		if(strUpdateFlg!="" && !strUpdateFlg.equalsIgnoreCase("Y")){
			strLimitedAccess="true";
			request.setAttribute("DMNDSHT_LIMITED_ACC", strLimitedAccess);
		}
		
	   if (strOpt.equals("fetchPPP"))
	    {
	    	GmOPDemandReportBean gmOPDemandReportBean = new GmOPDemandReportBean();
	    	HashMap hmParams = GmCommonClass.getHashMapFromForm(gmOPDemandSheetSummaryForm);             
                        
       	 	HashMap hmReturn =  gmOPDemandReportBean.fetchTPRReport(hmParams);   
       	    gmOPDemandSheetSummaryForm.setAlTPRReport((ArrayList)hmReturn.get("TPRREPORT"));
       	    gmOPDemandSheetSummaryForm.setAlTPRVoidReport((ArrayList)hmReturn.get("TPRVOIDREPORT"));
	    	return mapping.findForward("fetchppp");	          
	    }
	   
	   strAccessType = gmOPDemandSheetSummaryForm.getAccessType();
   	   strLevelID  = gmOPDemandSheetSummaryForm.getLevelID(); 
	    
	    
	    if(strOpt.equals("fetch")){
	    	gmOPDemandSheetSummaryForm.setUnitrprice("Unit");
	    	gmOPDemandSheetSummaryForm.setForecastMonths("4");
	    	gmOPDemandSheetSummaryForm.setPartNumbers("");
	    }
	    hmParam = GmCommonClass.getHashMapFromForm(gmOPDemandSheetSummaryForm);
	   

	    if (strOpt.equals("partdrilldown"))
	    {
	    	GmOPDemandReportBean gmOPDemandReportBean = new GmOPDemandReportBean();
	    	HashMap hmAggrReturn = new HashMap();
	    	if (!strAccessType.equals("102623")&&!strLevelID.equals("102584"))
	    	{
	    		hmAggrReturn = gmOPDemandReportBean.fetchAggregatePartDrillDown(hmParam);
	    		gmOPDemandSheetSummaryForm.setHmAggrPartDetail(hmAggrReturn);
	    		return mapping.findForward("aggregatepartdrilldown");
	    	}
	    	else {
	    		lResult = gmOPDemandSetupBean.fetchPartDrillDown(hmParam).getRows();
	 	        gmOPDemandSheetSummaryForm.setLdtResult(lResult);
	 	        return mapping.findForward("partdrilldown");
	    	}
	       
	    }
	    
	    if (strOpt.equals("approvefetch"))
	    {
	        hmValues = gmOPForecastReportBean.fetchapproveDetail(strDemandSheetMonthId);
	        gmOPDemandSheetSummaryForm = (GmOPDemandSheetSummaryForm)GmCommonClass.getFormFromHashMap(gmOPDemandSheetSummaryForm,hmValues);
	        return mapping.findForward("approvefetch");
	    }
	     
	    if (strOpt.equals("approve"))
	    {
	    	try{
	    	blnApprove = true;
	        gmOPForecastReportBean.approveDemandSheet(hmParam);
	    	}
	    	catch(Exception ex)
	    	{
	    		request.setAttribute("ErrorMessage", ex.getMessage());
	    	}
	        return mapping.findForward("approve");
	    }
	    
	    if (strOpt.equals("allpartdrilldown"))
	    {
	    	hmValues = gmOPDemandSetupBean.loadPartQtyDetails(hmParam);
	    	gmOPDemandSheetSummaryForm.setHmDemandSheetDetail(hmValues);
	    	gmOPDemandSheetSummaryForm.setHmSubPartDetail((HashMap)hmValues.get("SUBPART"));
	    	gmOPDemandSheetSummaryForm.setHmParentPartDetail((HashMap)hmValues.get("PARENTPART"));
	    	gmOPDemandSheetSummaryForm.setHmPartDetail((HashMap)hmValues.get("PARTDETAILS"));
	        return mapping.findForward("allpartdrilldown");
	    }
	    if (strOpt.equals("viewGrowthComments")){
	    	strPartInput = gmOPDemandSheetSummaryForm.getPartNumbers();
	    	GmOPDemandReportBean gmOPDemandReportBean = new GmOPDemandReportBean();
	    	HashMap hmParams = GmCommonClass.getHashMapFromForm(gmOPDemandSheetSummaryForm);  
	    	rdDemandSheetReport =  gmOPDemandReportBean.fetchGrowthComments(hmParams);
	    	gmOPDemandSheetSummaryForm.setLdCommentList(rdDemandSheetReport.getRows());
       	    gmOPDemandSheetSummaryForm.setPartNumbers(strPartInput);       	 	
	        return mapping.findForward("gmGrowthComments");
	    }
	    //************* Method to fetch Demand Sheet information ********* 
	    
	    hmDemandSheet = gmOPForecastReportBean.fetchMonthlyDemandSheetInfo(hmParam);
	    gmOPDemandSheetSummaryForm = (GmOPDemandSheetSummaryForm)GmCommonClass.getFormFromHashMap(gmOPDemandSheetSummaryForm,hmDemandSheet);
	        
	    strAccessType = gmOPDemandSheetSummaryForm.getAccessType();
    	strLevelID  = gmOPDemandSheetSummaryForm.getLevelID();
    	
	    if (strAccessType.equals("102622"))
	    {
	    	gmOPDemandSheetSummaryForm.setHideGrowthTab("YES"); // THis is to hide the  Growth Info Tab at the aggregated sheet.

	    }
	    if (strAccessType.equals("102622")&&!strLevelID.equals("102580"))
	    {
	    	gmOPDemandSheetSummaryForm.setHideApprovalTab("YES") ; //THis is to hide the  Approval Tab for readonly sheet and company is not globus and algea.For Algea and Globus, we should show the Approval Tab.

	    }
	  
	    
	    //  To fecth sheet information (on load condition)
	    //  To fetch the sheet information and the tab from TTP Monthly Summary Screen
	    if (strOpt.equals("fetch") || strOpt.equals("ttpfetch"))
	    {	    	
	        hmParam.put("DEMANDSHEETMONTHID",gmOPDemandSheetSummaryForm.getDemandSheetMonthId());
	    }
	    
	    // Reload condition 
	    if (strOpt.equals("report"))
	    {
	        String strSelectedGroup = gmOPDemandSheetSummaryForm.getCheckedGroupsString(); //Form.getCheckedGroupId());
	        strPartInput = gmOPDemandSheetSummaryForm.getPartNumbers();
	        if (!strPartInput.equals("") ){
	            strPartInput = strPartInput.replaceAll(",","|");
	        }
	
	        hmParam.put("GRPINPUTSTRING",strSelectedGroup);
	        hmParam.put("PARTINPUTSTRING",strPartInput);
	    }
	    
	    // fetch sheet detail information 
	    hmValues = gmOPForecastReportBean.reportDemandSheetSummary(hmParam);
	    gmOPDemandSheetSummaryForm.setHmDemandSheetDetail(hmValues);
	    
	    if (strOpt.equals("fetchSheet"))
	    {
	        log.debug("fetch Sheet Information **** redirect ");
	        return mapping.findForward("sheetfetch");
	    }
	    
	    
	    // To fetch group information 
	    strDemandSheetMonthId = GmCommonClass.parseNull((String)hmDemandSheet.get("DEMANDSHEETMONTHID"));
	    alGroupList = GmCommonClass.parseNullArrayList(gmOPForecastReportBean.fetchDemandGroupMap(strDemandSheetMonthId,strDemandSheetId));
	    gmOPDemandSheetSummaryForm.setAlGroupList(alGroupList);
	
	    
    
	    alMonth = GmCommonClass.parseNullArrayList(gmCommonBean.getMonthList()); 
	    alYear =  GmCommonClass.parseNullArrayList(gmCommonBean.getYearList());
	    
	
	    gmOPDemandSheetSummaryForm.setAlMonth(alMonth);
	    gmOPDemandSheetSummaryForm.setAlYear(alYear);
	
	    if(strOpt.equals("filterfetch"))
	    	return mapping.findForward("filterfetch"); //on fetch
	    if(strOpt.equals("ttpfetch"))
	    	return mapping.findForward("ttpfetch"); //on fetch
	   
	    if(blnApprove)
	    	return mapping.findForward("success"); //on fetch
	    
	    if (strOpt.equals("report") )
	    	return mapping.findForward("sheetfetch"); //on report and approve
	    
	    if (strOpt.equals("setsheetreport"))
	    {
	        RowSetDynaClass rdResult = null;
	    	rdResult = gmOPForecastReportBean.fchSetSheetReport(refID,month);
	    	List alResult = rdResult.getRows();
	    	gmOPDemandSheetSummaryForm.setLdtResult(alResult);
	    	return mapping.findForward("setsheetreport");	          
	    }
	    
	    
		return mapping.findForward("success"); //on fetch
	    
	    }
	    catch(Exception exp)
	    {
	        log.debug("Inside Catch");
	        exp.printStackTrace();	       
	        throw new AppError(exp);
	    }

	}

}
