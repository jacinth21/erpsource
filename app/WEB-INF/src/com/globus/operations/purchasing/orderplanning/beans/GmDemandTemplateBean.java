package com.globus.operations.purchasing.orderplanning.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;

public class GmDemandTemplateBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	/**
	 * fetchDemandTemplateList - This method will be used to fetch the demand template list
	 * 
	 * @return ArrayList - will be used by drop down tag
	 * @exception AppError
	 */
	public ArrayList fetchDemandTemplateList() throws AppError {
		ArrayList alResult = new ArrayList();

		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		gmDBManager.setPrepareString("gm_pkg_oppr_dmd_templt_rpt.gm_fch_demand_template_list", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();
		return alResult;
	}

	/**
	 * fetchDemandTemplate - This method will be used to fetch the demand template detail
	 * @param String - Demand Template ID
	 * @return HashMap - 
	 * @exception AppError
	 */
	public HashMap fetchDemandTemplate(String strDemandTemplateID) throws AppError {
		HashMap hmReturn = new HashMap();

		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		gmDBManager.setPrepareString("gm_pkg_oppr_dmd_templt_rpt.gm_fch_demand_template", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strDemandTemplateID);
		gmDBManager.execute();
		hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();

		return hmReturn;
	}

	/**
	 * saveDemandSheet - This method will be save / update the demand sheet template
	 * 
	 * @param hmParam -
	 *            parameters to be saved / updated
	 * @exception AppError
	 */
	public String saveDemandTemplate(HashMap hmParam) throws AppError {
		GmCommonBean gmCommonBean= new GmCommonBean();
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		String strDemandTempltId = GmCommonClass.parseNull((String) hmParam.get("DEMANDTEMPLTID"));
		String strDemandTempltNm = GmCommonClass.parseNull((String) hmParam.get("DEMANDTEMPLTNM"));
		String strDemandPeriod = GmCommonClass.parseNull((String) hmParam.get("DEMANDPERIOD"));
		String strForecastPeriod = GmCommonClass.parseNull((String) hmParam.get("FORECASTPERIOD"));
		String strSetBuild = GmCommonClass.parseNull((String) hmParam.get("SETBUILD"));
		String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
		String strInActiveFlag = GmCommonClass.getCheckBoxValue((String) hmParam.get("INACTIVEFLAG"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strDemandSheetOwner = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETOWNER"));
		String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		
		gmDBManager.setPrepareString("gm_pkg_oppr_dmd_templt_txn.gm_sav_demand_template", 10);

		gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
		gmDBManager.setString(1, strDemandTempltId);
		gmDBManager.setString(2, strDemandTempltNm);
		gmDBManager.setString(3, strDemandPeriod);
		gmDBManager.setString(4, strForecastPeriod);
		gmDBManager.setString(5, strSetBuild);
		gmDBManager.setString(6, strCompanyId);
		gmDBManager.setString(7, strDemandSheetOwner);
		gmDBManager.setString(8, strInActiveFlag);
		gmDBManager.setString(9, strLogReason);
		gmDBManager.setString(10, strUserId);
		
		gmDBManager.execute();

		strDemandTempltId = GmCommonClass.parseNull(gmDBManager.getString(1));
		gmDBManager.commit();

		return strDemandTempltId;
	}

	/**
	 * fetchTemplateMapping - This method will be used to fetch the demand template mapping detail
	 * @param String - Demand Template ID
	 * @return HashMap - 
	 * @exception AppError
	 */
	public HashMap fetchTemplateMapping(String strDemandTemplateID) throws AppError {
		HashMap hmReturn = new HashMap();
		ArrayList alGroupAvail = new ArrayList();
		ArrayList alGroupAssoc = new ArrayList();
		ArrayList alConSetAvail = new ArrayList();
		ArrayList alConSetAssoc = new ArrayList();
		ArrayList alInhSetAvail = new ArrayList();
		ArrayList alInhSetAssoc = new ArrayList();
		
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		gmDBManager.setPrepareString("gm_pkg_oppr_dmd_templt_rpt.gm_fch_template_mapping", 7);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
		gmDBManager.setString(1, strDemandTemplateID);
		gmDBManager.execute();
		alGroupAvail = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2)));
		alGroupAssoc = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3)));
		alConSetAvail = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4)));
		alConSetAssoc = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5)));
		alInhSetAvail = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6)));
		alInhSetAssoc = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(7)));
		gmDBManager.close();

		hmReturn.put("GROUP_AVAIL",alGroupAvail);
		hmReturn.put("GROUP_ASSOC",alGroupAssoc);
		hmReturn.put("CONSET_AVAIL",alConSetAvail);
		hmReturn.put("CONSET_ASSOC",alConSetAssoc);
		hmReturn.put("INHSET_AVAIL",alInhSetAvail);
		hmReturn.put("INHSET_ASSOC",alInhSetAssoc);
		return hmReturn;
	}

	/**
	 * saveDemandTemplateMapping - This method will be save / update the demand sheet template mapping detail
	 * 
	 * @param hmParam -
	 *            parameters to be saved / updated
	 * @exception AppError
	 */
	public void saveDemandTemplateMapping(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		String strDemandTempltId = GmCommonClass.parseNull((String) hmParam.get("DEMANDTEMPLTID"));
		String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		log.debug("======strInputString======="+strInputString);
		gmDBManager.setPrepareString("gm_pkg_oppr_dmd_templt_txn.gm_sav_dmd_tmplt_mapping", 3);

		gmDBManager.setString(1, strDemandTempltId);
		gmDBManager.setString(2, strInputString);
		gmDBManager.setString(3, strUserId);
		
		gmDBManager.execute();

		gmDBManager.commit();

	}
	/**
	 * createDemandSheet - This method will be save / update the demand sheet from template
	 * 
	 * @param hmParam -
	 *            parameters to be saved / updated
	 * @exception AppError
	 */
	public void createDemandSheet(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		String strDemandTempltId = GmCommonClass.parseNull((String) hmParam.get("DEMANDTEMPLTID"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		
		gmDBManager.setPrepareString("gm_pkg_oppr_dmd_templt_txn.gm_sav_gen_demand_sheet", 2);

		gmDBManager.setString(1, strDemandTempltId);
		gmDBManager.setString(2, strUserId);
		
		gmDBManager.execute();
		gmDBManager.commit();

	}
	/**
	 * fetchDemandTemplateList - This method will be used to fetch the demand template list
	 * 
	 * @return ArrayList - will be used by drop down tag
	 * @exception AppError
	 */
	public ArrayList fetchDemandSheetByTemplate(String strDemandTempltId) throws AppError {
		ArrayList alResult = new ArrayList();

		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		gmDBManager.setPrepareString("gm_pkg_oppr_dmd_templt_rpt.gm_fch_demand_sheet_by_tmplt", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strDemandTempltId);
		gmDBManager.execute();
		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return alResult;
	}
	/**
	 * createDemandSheet - This method will be inactive demand sheet template
	 * 
	 * @param hmParam -
	 *            parameters to be saved / updated
	 * @exception AppError
	 */
	public void inActiveDemandTemplate(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		String strDemandTempltId = GmCommonClass.parseNull((String) hmParam.get("DEMANDTEMPLTID"));
		String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		String strInActiveFlag = GmCommonClass.getCheckBoxValue((String) hmParam.get("INACTIVEFLAG"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		
		gmDBManager.setPrepareString("gm_pkg_oppr_dmd_templt_txn.gm_sav_inactive_template", 4);

		gmDBManager.setString(1, strDemandTempltId);
		gmDBManager.setString(2, strInActiveFlag);
		gmDBManager.setString(3, strLogReason);
		gmDBManager.setString(4, strUserId);
		
		gmDBManager.execute();
		gmDBManager.commit();

	}
	
	/**
	 * getSheetAccessType - This method will get Sheet Access type
	 * 
	 * @param String -
	 *            
	 * @exception AppError
	 */
	public String getSheetAccessType(String strSheetId) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		String strAccessType = "";
		gmDBManager.setFunctionString("gm_pkg_oppr_dmd_templt_rpt.get_sheet_access_type", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, strSheetId);	 
		gmDBManager.execute();
		strAccessType = gmDBManager.getString(1); 
		gmDBManager.close();
		return strAccessType;

	}		
	 
}
