package com.globus.operations.purchasing.orderplanning.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import com.globus.common.beans.AppError;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.purchasing.orderplanning.beans.GmDemandMasterSetupBean;
import com.globus.operations.purchasing.orderplanning.forms.GmDemandAvgOverrideForm;

public class GmDemandAvgOverrideAction extends GmAction
{

    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
    
    	GmDemandAvgOverrideForm gmDemandAvgOverrideForm = (GmDemandAvgOverrideForm)form;
    	gmDemandAvgOverrideForm.loadSessionParameters(request);
    	
    	GmDemandMasterSetupBean gmDemandMasterSetupBean = new GmDemandMasterSetupBean();
    	GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    	GmCommonBean gmCommonBean = new GmCommonBean();
    	Logger log = GmLogger.getInstance(this.getClass().getName());

        String strOpt  = GmCommonClass.parseNull((String)gmDemandAvgOverrideForm.getStrOpt());
        String strDemandSheetId = GmCommonClass.parseNull((String) gmDemandAvgOverrideForm.getDemandSheetId());
        String strPartyId = GmCommonClass.parseNull((String) gmDemandAvgOverrideForm.getSessPartyId());
        String strJNDIConnection = GmCommonClass.parseNull((String) GmCommonClass.getString("JNDI_CONNECTION"));
        
        String strAccessFl =""; 
        String strOverrideID="";
        String strDemandMasterId="";
        String strSystemWeightedAvg="";
        String strRefId="";
        String strForward="override";
        
        HashMap hmParam = new HashMap();
        HashMap hmAccess = new HashMap();
        HashMap hmOverridedetails = new HashMap();
        
        ArrayList alLogReasons = new ArrayList();      
        
    	hmAccess = GmCommonClass.parseNullHashMap((HashMap)gmAccessControlBean.getAccessPermissions(strPartyId, "DMD_OVERRIDE"));
    	strAccessFl = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
		gmDemandAvgOverrideForm.setStrAccessFl(strAccessFl);		
		
		hmParam = GmCommonClass.getHashMapFromForm(gmDemandAvgOverrideForm);
		
		if(strOpt.equals("load")){
			
			hmOverridedetails =GmCommonClass.parseNullHashMap((HashMap) gmDemandMasterSetupBean.fetchDemandAverageOverride(hmParam));
			//Fetch Log Reasons
			strOverrideID = GmCommonClass.parseNull((String)hmOverridedetails.get("DEMANDOVERRIDEID"));
			log.debug("::strOverrideID::"+strOverrideID);
			alLogReasons = GmCommonClass.parseNullArrayList((ArrayList)gmCommonBean.getLog(strOverrideID,"4000344",strJNDIConnection));
			gmDemandAvgOverrideForm.setAlLogReasons(alLogReasons);
			
			GmCommonClass.getFormFromHashMap(gmDemandAvgOverrideForm,hmOverridedetails);			
		}else if(strOpt.equals("save")){
			strOverrideID = GmCommonClass.parseNull((String)gmDemandMasterSetupBean.saveDemandAverageOverride(hmParam));
			log.debug("::strOverrideID::"+strOverrideID);
			strDemandMasterId = GmCommonClass.parseNull((String)gmDemandAvgOverrideForm.getDemandMasterId());
			strRefId = GmCommonClass.parseNull((String)gmDemandAvgOverrideForm.getRefID());
			strSystemWeightedAvg = GmCommonClass.parseNull((String)gmDemandAvgOverrideForm.getSystemWeigthedAvg());
			
			strForward = "/gmDemandAvgOverride.do?strOpt=load&demandOverrideId="+strOverrideID+"&demandMasterId="+strDemandMasterId+"&refID="+strRefId+"&systemWeigthedAvg="+strSystemWeightedAvg;
			ActionRedirect actionRedirect = new ActionRedirect(strForward);
		    return actionRedirect;
		}		
		
        return mapping.findForward(strForward);
    }

}
