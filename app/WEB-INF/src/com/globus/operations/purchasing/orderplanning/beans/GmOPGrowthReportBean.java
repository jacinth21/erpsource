/**
 * 
 */
package com.globus.operations.purchasing.orderplanning.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;

/**
 * @author vprasath
 *
 */
public class GmOPGrowthReportBean
{
    Logger log = GmLogger.getInstance(this.getClass().getName());
    /**
     * test here!!!!
     */
    public GmOPGrowthReportBean()
    {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public HashMap getGrowthReport(HashMap hmParam) throws AppError
    {
        StringBuffer sbHeaderQuery = new StringBuffer();      
        StringBuffer sbDetailQuery = new StringBuffer();        
        String strWhereClause = "";
        HashMap hmapFinalValue; // Final value report details
        GmCrossTabReport gmCrossTabReport = new GmCrossTabReport();
        
        String strType = (String)hmParam.get("REFINPUTS");
        
        log.debug("hmParam"+hmParam);
        log.debug("strType"+strType);
        strWhereClause = getWhereClause(hmParam);
        // ***************************************************************************
        // Query to Fetch Header information 
        // ***************************************************************************

        sbHeaderQuery.append(" SELECT DISTINCT TO_CHAR(T4031.C4031_START_DATE,'Mon YY') S_DATE");                 
        sbHeaderQuery.append(" FROM "); 
        sbHeaderQuery.append(" T4030_DEMAND_GROWTH_MAPPING T4030, "); 
        sbHeaderQuery.append(" T4031_GROWTH_DETAILS T4031 "); 
        sbHeaderQuery.append(" WHERE T4030.C4030_DEMAND_GROWTH_ID = T4031.C4030_DEMAND_GROWTH_ID ");
        if(!strWhereClause.equals(""))
        {
            sbHeaderQuery.append(strWhereClause);
        }
        sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(T4031.C4031_START_DATE,'Mon YY'),'MON YY')");

        // ***************************************************************************
        // Query to Fetch Detail informatio 
        // ***************************************************************************
        
        sbDetailQuery.append(" SELECT T4030.C4030_REF_ID ID, ");
        sbDetailQuery.append(" T4030.C4030_REF_ID || ':' || GET_ENTITY_DESC(T4030.C4030_REF_ID,T4030.C901_REF_TYPE) NAME, "); 
        sbDetailQuery.append(" TO_CHAR(T4031.C4031_START_DATE,'Mon YY') S_DATE , "); 
        sbDetailQuery.append(" NVL(T4031.C4031_VALUE,0) AMOUNT ");  
        sbDetailQuery.append(" FROM "); 
        sbDetailQuery.append(" T4030_DEMAND_GROWTH_MAPPING T4030, "); 
        sbDetailQuery.append(" T4031_GROWTH_DETAILS T4031 "); 
        if (strType.equals("20297,"))
        {
        	sbDetailQuery.append(" ,T4010_GROUP T4010 ");
        }
        sbDetailQuery.append(" WHERE T4030.C4030_DEMAND_GROWTH_ID = T4031.C4030_DEMAND_GROWTH_ID ");
        sbDetailQuery.append(" AND T4030.C4030_VOID_FL IS NULL ");
        sbDetailQuery.append(" AND T4031.C4031_VOID_FL IS NULL ");
        
        if (strType.equals("20297,"))
        {
        	sbDetailQuery.append(" AND T4030.C4030_REF_ID = T4010.C4010_GROUP_ID ");
            sbDetailQuery.append(" AND T4010.C901_TYPE = 40045 ");        	
        }
        
        if(!strWhereClause.equals(""))
        {                   
            sbDetailQuery.append(strWhereClause);
        }
        
        sbDetailQuery.append(" ORDER BY T4030.C4030_REF_ID, T4031.C4031_START_DATE ");
        
        log.debug("******* HEADER " + sbHeaderQuery.toString());
        log.debug("******* DETAILS " + sbDetailQuery.toString());
        hmapFinalValue = gmCrossTabReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString(),GmDNSNamesEnum.DMWEBGLOBUS);
      
        return hmapFinalValue;    
    }
 private String getWhereClause(HashMap hmParam)
 {
     StringBuffer sbWhereClause = new StringBuffer();
     
     String strDemandSheetId = GmCommonClass.parseZero((String)hmParam.get("DEMANDSHEETID"));
     String strRefInputs = GmCommonClass.parseNull((String)hmParam.get("REFINPUTS"));
     String strFromDate = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
     String strToDate = GmCommonClass.parseNull((String)hmParam.get("TODATE"));
     
     strFromDate = strFromDate.equals("") ? GmCalenderOperations.getCurrentMonth()+"/01/"+GmCalenderOperations.getCurrentYear() : strFromDate; 
     strToDate =  strToDate.equals("") ? GmCalenderOperations.getCurrentMonth()+"/01/"+(GmCalenderOperations.getCurrentYear() + 1) : strToDate;
     
     if(!strDemandSheetId.equals("0"))
     {
         sbWhereClause.append(" AND C4020_DEMAND_MASTER_ID = "+ strDemandSheetId );          
     }
     
     if(!strRefInputs.equals(""))
     {
         sbWhereClause.append(" AND T4030.C901_REF_TYPE IN ('"+GmCommonClass.getStringWithQuotes(strRefInputs)+"') ");
     }
     
     if(!strFromDate.equals("") && !strToDate.equals(""))
     {         
         sbWhereClause.append("AND T4031.C4031_START_DATE >= TO_DATE('"+strFromDate+"','MM/DD/YYYY')");
         sbWhereClause.append("AND T4031.C4031_END_DATE <= TO_DATE('"+strToDate+"','MM/DD/YYYY')");
     }
    
    return  sbWhereClause.toString();
 }
}
