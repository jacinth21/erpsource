package com.globus.operations.purchasing.orderplanning.forms;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.forms.GmCommonForm;

public class GmSheetSetPARForm extends GmCommonForm 
{
    private String demandSheetName = "";
    private String demandSetId = "";
    private String demandSetNm = "";
    private String updatefl    = "";
    private String hinputStr   = "";
    private List returnReport  = new ArrayList();
    
    private ArrayList alDemandSheetList = new ArrayList();

	/**
	 * @return the demandSheetName
	 */
	public String getDemandSheetName() {
		return demandSheetName;
	}

	/**
	 * @param demandSheetName the demandSheetName to set
	 */
	public void setDemandSheetName(String demandSheetName) {
		this.demandSheetName = demandSheetName;
	}

	/**
	 * @return the demandSetId
	 */
	public String getDemandSetId() {
		return demandSetId;
	}

	/**
	 * @param demandSetId the demandSetId to set
	 */
	public void setDemandSetId(String demandSetId) {
		this.demandSetId = demandSetId;
	}

	/**
	 * @return the demandSetNm
	 */
	public String getDemandSetNm() {
		return demandSetNm;
	}

	/**
	 * @param demandSetNm the demandSetNm to set
	 */
	public void setDemandSetNm(String demandSetNm) {
		this.demandSetNm = demandSetNm;
	}

	/**
	 * @return the updatefl
	 */
	public String getUpdatefl() {
		return updatefl;
	}

	/**
	 * @param updatefl the updatefl to set
	 */
	public void setUpdatefl(String updatefl) {
		this.updatefl = updatefl;
	}

	/**
	 * @return the alDemandSheetList
	 */
	public ArrayList getAlDemandSheetList() {
		return alDemandSheetList;
	}

	/**
	 * @param alDemandSheetList the alDemandSheetList to set
	 */
	public void setAlDemandSheetList(ArrayList alDemandSheetList) {
		this.alDemandSheetList = alDemandSheetList;
	}
	
	/**
	 * @return the hinputStr
	 */
	public String getHinputStr() {
		return hinputStr;
	}

	/**
	 * @param hinputStr the hinputStr to set
	 */
	public void setHinputStr(String hinputStr) {
		this.hinputStr = hinputStr;
	}

	/**
	 * @return the returnReport
	 */
	public List getReturnReport() {
		return returnReport;
	}

	/**
	 * @param returnReport the returnReport to set
	 */
	public void setReturnReport(List returnReport) {
		this.returnReport = returnReport;
	}	
	
}
