package com.globus.operations.purchasing.orderplanning.beans;



import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.operations.forecast.beans.GmTTPSetupBean;

public class GmOPForecastReportBean
{
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
      

    /**
      * reportDSSummary - This Method is used to get crosstab details for Demand Sheet
      * @param String strStudyListId, String strSiteId  
      * @return HashMap
      * @exception AppError
    **/
    
      
          public HashMap reportDemandSheetSummary(HashMap hmParam) throws AppError
      {        log.debug(" values to fetch ds summary " + hmParam);
      
      HashMap hmReturn = new HashMap();
      ArrayList alForecastHeader = new ArrayList();
        
      ResultSet rsHeader = null;
      ResultSet rsDetailsExpected = null;      
      GmCrossTabReport gmCrossReport = new GmCrossTabReport();
      GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS); 
        
      String strDemandSheetId = GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETMONTHID")); 
      String strGroupIds = GmCommonClass.parseNull((String)hmParam.get("GRPINPUTSTRING"));
      String strPartNums = GmCommonClass.parseNull((String)hmParam.get("PARTINPUTSTRING"));
      String strOverrideFlag = GmCommonClass.getCheckBoxValue((String)hmParam.get("OVERRIDEFLAG"));
      String strForeCastMonths = GmCommonClass.parseNull((String)hmParam.get("FORECASTMONTHS"));
      String strUnitType = GmCommonClass.parseNull((String) hmParam.get("UNITRPRICE"));
      String strCheckSetDtl = GmCommonClass.parseNull((String) hmParam.get("CHECKSETDTL"));
      String strCheckDesc = GmCommonClass.parseNull((String) hmParam.get("CHECKDESC"));
      
      
      // If no sheet id then return null 
      if (strDemandSheetId.equals(""))
      {
          return hmReturn;
      }
       
      // To fetch the transaction information
      gmDBManager.setPrepareString("gm_pkg_oppr_sheet_summary.gm_fc_fch_dsdetails",13);
      gmDBManager.registerOutParameter(9,OracleTypes.CURSOR);
      gmDBManager.registerOutParameter(10,OracleTypes.CURSOR);
      gmDBManager.registerOutParameter(11,OracleTypes.CURSOR);
      gmDBManager.registerOutParameter(12,java.sql.Types.CHAR);
      gmDBManager.registerOutParameter(13,java.sql.Types.CHAR);        
      gmDBManager.setString(1,strDemandSheetId);
      gmDBManager.setString(2,strGroupIds);
      gmDBManager.setString(3,strPartNums);
      gmDBManager.setString(4,strOverrideFlag);
      gmDBManager.setString(5,strForeCastMonths);
      gmDBManager.setString(6,strUnitType);
      gmDBManager.setString(7,strCheckSetDtl);
      gmDBManager.setString(8,strCheckDesc);
      
      gmDBManager.execute();        
      
      rsHeader = (ResultSet)gmDBManager.getObject(9);
      rsDetailsExpected = (ResultSet)gmDBManager.getObject(10);       
      gmCrossReport.setGroupFl(true);
      gmCrossReport.setOverRideFl(true);
      gmCrossReport.setRowOverRideFl(true);
      gmCrossReport.setFixedColumn(true);
      gmCrossReport.setAcceptExtraColumn(true);
      gmCrossReport.setFixedParams(9,"CROSSOVERF");
      gmCrossReport.setFixedParams(10,"PS_FLAG");
      gmCrossReport.setFixedParams(11,"SETDTL"); 
      gmCrossReport.setExtraColumnParams(4,"");
      gmCrossReport.setExtraColumnParams(6,"HFL");
      gmCrossReport.setExtraColumnParams(12,"COMM_FL");
      gmCrossReport.setExtraColumnParams(13,"LEVEL_ID");
      
      
      hmReturn = gmCrossReport.GenerateCrossTabReport(rsHeader,rsDetailsExpected);
      log.debug("hmReturn: "+ hmReturn);
      alForecastHeader = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(11));        
      hmReturn.put("FORECASTHEAD", alForecastHeader);
      hmReturn.put("DEMANDEND",  gmDBManager.getString(12));
      hmReturn.put("SHEETSTATUS",  gmDBManager.getString(13));
      
      gmDBManager.close();
       
      return hmReturn;
      }
      

      /**
       * reportDSSummary - This Method is used to get crosstab details for Demand Sheet
       * @param String strStudyListId, String strSiteId  
       * @return HashMap
       * @exception AppError
     **/
       public HashMap reportDemandSheetGrowthSummary(String strDemandSheetMonthId) throws AppError
       {
         
         HashMap hmReturn = new HashMap();

         ResultSet rsHeader = null;
         ResultSet rsDetailsExpected = null;
         GmCrossTabReport gmCrossReport = new GmCrossTabReport();
         GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS); 
         String strStatus = "";  
         
         try{
         // To fetch the transaction information
         gmDBManager.setPrepareString("gm_pkg_oppr_sheet_summary.gm_fc_fch_dsgrowthdetail",4);
         gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
         gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
         gmDBManager.registerOutParameter(4,OracleTypes.VARCHAR);
         gmDBManager.setString(1,strDemandSheetMonthId);
         gmDBManager.execute();
         
         rsHeader = (ResultSet)gmDBManager.getObject(2);
         rsDetailsExpected = (ResultSet)gmDBManager.getObject(3);
         strStatus = gmDBManager.getString(4);
         gmCrossReport.setFixedColumn(true);
         gmCrossReport.setFixedParams(6,"GTYPE");
         
         hmReturn = gmCrossReport.GenerateCrossTabReport(rsHeader,rsDetailsExpected);

         gmDBManager.close();
         }
       
         catch(Exception exp){
        	 gmDBManager.close();
        	 throw new AppError(exp);
     	 }   
         log.debug("Values for demand sheet summary are " + hmReturn);
         hmReturn.put("SHEETSTATUS", strStatus);
         
         return hmReturn;
       }
       
       
       /**
        * reportDSRequestDetailSummary - This Method is used to get crosstab details for Demand Sheet
        * @param String strDemandSheetMonthId, String strDemandTypeId  
        * @return ArrayList
        * @exception AppError
      **/
       public ArrayList reportDemandSheetRequestSummary(String strDemandSheetMonthId, String strDemandTypeId) throws AppError{
    	 RowSetDynaClass rdResult = null;
    	 GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
         gmDBManager.setPrepareString("gm_pkg_oppr_sheet_summary.gm_fc_fch_requestdetail",3);         
         gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
         gmDBManager.setString(1,strDemandSheetMonthId);
         gmDBManager.setString(2,strDemandTypeId);           
         gmDBManager.execute();
         rdResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(3));
         gmDBManager.close();
         return (ArrayList)rdResult.getRows();
       }
               
        /**
         * reportDSGrowthRequestDetails - This Method is used to get crosstab details for Demand Sheet
         * @param String strDemandSheetMonthId , String strGrowthDrillDown
         * @return ArrayList
         * @exception AppError
       **/
       public ArrayList reportDSGrowthRequestDetails(HashMap hmParam) throws AppError{
           RowSetDynaClass rdResult = null;
           GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS); 
           String strDemandSheetMonthId = GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETMONTHID")); 
           String strGrowthDrillDown = GmCommonClass.parseNull((String)hmParam.get("GROWTHDRILLDOWN")); 
           String strSetID = GmCommonClass.parseNull((String)hmParam.get("SETID"));
           String strMonth = GmCommonClass.parseNull((String)hmParam.get("GROWTHDRILLDOWNMONTH"));
                     
           gmDBManager.setPrepareString("gm_pkg_oppr_sheet_summary.gm_fch_growth_request_detail",5);           
           gmDBManager.registerOutParameter(5,OracleTypes.CURSOR);                      
           gmDBManager.setString(1,strDemandSheetMonthId);
           gmDBManager.setString(2,strGrowthDrillDown); 
           gmDBManager.setString(3,strSetID); 
           gmDBManager.setString(4,strMonth); 
           gmDBManager.execute();         
           rdResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(5));   
        //   alResult = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(5)));           
           gmDBManager.close();                  
           return (ArrayList)rdResult.getRows();         
         }
       
       /**
        * reportDemandSheetPartHistory - This Method is used to get crosstab Part Override information
        * @param String strStudyListId, String strSiteId  
        * @return HashMap
        * @exception AppError
      **/
        public ArrayList reportDemandSheetPartHistory(String strDemandSheetMonthId) throws AppError
        {
          
          ArrayList alPartList = new ArrayList();
          GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS); 
            
          // To fetch the transaction information
          gmDBManager.setPrepareString("gm_pkg_oppr_sheet_summary.gm_fc_fch_dsparthistory",2);
          gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
          gmDBManager.setString(1,strDemandSheetMonthId);
          gmDBManager.execute();
          
          alPartList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
          
          log.debug("Values for demand sheet Part summary are " + alPartList);
          gmDBManager.close();

          return alPartList;
        }

        
        /**
         * fetchDemandSheetMapping - This Method is used to get Region information
         * @param String strDemandSheetMonthId  
         * @return ArrayList
         * @exception AppError
       **/
         public ArrayList fetchDemandSheetMapping(String strDemandSheetMonthId) throws AppError
         {
           
           ArrayList alRegionList = new ArrayList();
           GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS); 
             
           // To fetch the transaction information
           gmDBManager.setPrepareString("gm_pkg_oppr_sheet_summary.gm_fc_fch_dsmapping",2);
           gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
           gmDBManager.setString(1,strDemandSheetMonthId);
           gmDBManager.execute();
           
           alRegionList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
           
           gmDBManager.close();

           return alRegionList;
         }
        
      /**
       * fetchGroupFromDS - This method will fetch the Groups associated with a DS
       * @param demandSheetId
       * @return - ArrayList
       * @exception AppError
       */
      public ArrayList fetchDemandGroupMap (String strDemandSheetMonthId,String strDemandSheetId)throws AppError
      {
          ArrayList alGroupList = new ArrayList();

          GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
          gmDBManager.setPrepareString("gm_pkg_oppr_sheet_summary.gm_fc_fch_grpfords",3);
          gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
          gmDBManager.setString(1,strDemandSheetMonthId);
          gmDBManager.setString(2,strDemandSheetId);
          gmDBManager.execute();
          alGroupList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3));
          gmDBManager.commit();
          
          return alGroupList;
      }
      
      /**
       * fetchMonthlyDemandSheetInfo - This method will fetch demand sheet data from t4040  
       * @param strDemandSheetId 
       * @return HashMap
       * @exception AppError
       */
      public HashMap fetchMonthlyDemandSheetInfo(HashMap hmParam) throws AppError
      {
          HashMap hmReturn = new HashMap();
          log.debug("HmParam : "+hmParam);
          String strDemandSheetId = GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETID")); 
          String strMonth = GmCommonClass.parseNull((String)hmParam.get("MONTHID"));
          String strYear = GmCommonClass.parseNull((String)hmParam.get("YEARID"));
          String strMonYear = strMonth.concat("/").concat(strYear);
          
          GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
          gmDBManager.setPrepareString("gm_pkg_oppr_sheet_summary.gm_fc_fch_dsmonthdetails",3);
          gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
          gmDBManager.setString(1,strDemandSheetId);
          gmDBManager.setString(2,strMonYear);
          gmDBManager.execute();
          hmReturn = gmDBManager.returnHashMap((ResultSet)gmDBManager.getObject(3));
          gmDBManager.commit();
              
          return hmReturn;
      }
      
      /**
       * approveDemandSheet - This method will approve the demand sheet for that month
       * @param hmParam
       * @return - void
       * @exception AppError
       */
      public HashMap fetchapproveDetail (String strDemandSheetMonthId)throws AppError
      {
          HashMap hmReturn = new HashMap();
          
          GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
          gmDBManager.setPrepareString("gm_pkg_oppr_sheet_summary.gm_fc_fch_dsapprovaldetail",2);
          gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
          gmDBManager.setString(1,strDemandSheetMonthId);
          //gmDBManager.setString(2,strUserId);
    
          gmDBManager.execute();

          
          hmReturn = gmDBManager.returnHashMap((ResultSet)gmDBManager.getObject(2));
          gmDBManager.commit();
          
          return  hmReturn;   
      }
      
      /**
       * approveDemandSheet - This method will approve the demand sheet for that month
       * @param hmParam
       * @return - void
       * @exception AppError
       */
      public void approveDemandSheet (HashMap hmParam)throws AppError
      {
          GmCommonBean gmCommon = new GmCommonBean ();
          
          String strDemandSheetMonthId  = GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETMONTHID"));
          String strComments  = GmCommonClass.parseNull((String)hmParam.get("APPCOMMENTS"));
          String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
          
          log.debug("Input Param:"+strDemandSheetMonthId+", "+strComments+", "+strUserId);
          
	      // PMT-32804 : TTP Summary lock and generate
          // If approve TTP sheet via screen - to Update the TTP details status
          
          String strStatus = "50582"; // 50582  Approved 
          GmOPTTPSetupBean gmOPTTPSetupBean = new GmOPTTPSetupBean();
          
          GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
          gmDBManager.setPrepareString("gm_pkg_oppr_sheet.gm_fc_sav_approveds",3);
          gmDBManager.setString(1,strDemandSheetMonthId);
          gmDBManager.setString(2,strUserId);
          gmDBManager.setString(3,strComments);
          try{
          gmDBManager.execute();
          
          // save approval comments
          gmCommon.saveLog(gmDBManager, strDemandSheetMonthId, strComments,strUserId, "1226" );          
          gmDBManager.commit();
          }catch (Exception ex){
        	  strStatus = "50583"; // "50583" Approve Failed
        	  throw new AppError(ex);
          }finally{
        	  hmParam.put("STATUS", strStatus); // Approved.
        	  log.debug(" Inside the exception ");
              gmOPTTPSetupBean.updateTTPDetailsStatus(hmParam);  
          }
          
          
      }
      
      public RowSetDynaClass fchSetSheetReport(String strSheetReportRefId,String strSheetReportMonthId) throws AppError
      {         
        RowSetDynaClass rdResult = null;        
        GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);        
        gmDBManager.setPrepareString("gm_pkg_oppr_sheet_summary.gm_fch_setSheet",3);       
        gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);                   
        gmDBManager.setString(1,strSheetReportRefId);
        gmDBManager.setString(2,strSheetReportMonthId);                   
        gmDBManager.execute();
        rdResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(3));        
        gmDBManager.close();
        return  rdResult ;
        
      }
      
  public HashMap fchMultiPartDetails (HashMap hmParam) throws AppError
  {
      log.debug(" values to fetch ds summary " + hmParam);
      
      HashMap hmReturn = new HashMap();
      HashMap hmParentDetails = new HashMap();
      HashMap hmChildDetails = new HashMap();
	  HashMap hmTotalResult = new HashMap();
	  ArrayList alParentDetails = new ArrayList();
	  
      ResultSet rsHeader = null;
      ResultSet rsDetailsExpected = null;      
      ResultSet rsParentHeader = null;
      ResultSet rsParentDetailsExpected = null; 
      GmCrossTabReport gmCrossReport = new GmCrossTabReport();
      GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS); 
        
      String strDemandSheetId = GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETMONTHID")); 
      String strPnum = GmCommonClass.parseNull((String)hmParam.get("PARTDETAILS"));      
      String strForeCastMonths = GmCommonClass.parseNull((String)hmParam.get("FORECASTMONTHS"));
      String strInentoryId = GmCommonClass.parseNull((String) hmParam.get("INVENTORYID"));
      String strMonth = GmCommonClass.parseNull((String) hmParam.get("MONTHID")); //pas inv id 
	  String strYear = GmCommonClass.parseNull((String) hmParam.get("YEARID"));
	  String strMonYear = strMonth.concat("/").concat(strYear);
	  
      try{
      // To fetch the transaction information
      gmDBManager.setPrepareString("gm_pkg_oppr_sheet_summary.gm_op_fch_multisheet_details",9);
      gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
      gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
      gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
      gmDBManager.registerOutParameter(9, OracleTypes.CURSOR);
      gmDBManager.setString(1,strDemandSheetId);
      gmDBManager.setString(2,strPnum);      
      gmDBManager.setString(3,strForeCastMonths);
      gmDBManager.setString(4,strMonYear);
      gmDBManager.setString(5,strInentoryId);
      gmDBManager.execute();              
      rsHeader = (ResultSet)gmDBManager.getObject(6);
      rsDetailsExpected = (ResultSet)gmDBManager.getObject(7);             
      gmCrossReport.setFixedColumn(true);
      //gmCrossReport.setFixedParams(5,"PAR");      
      hmChildDetails = gmCrossReport.GenerateCrossTabReport(rsHeader,rsDetailsExpected);
      hmReturn.put("CHILDDETAILS",hmChildDetails);
      
      rsParentHeader = (ResultSet)gmDBManager.getObject(8);
      rsParentDetailsExpected = (ResultSet)gmDBManager.getObject(9);             
      gmCrossReport.setFixedColumn(true);
      //gmCrossReport.setFixedParams(5,"PAR");      
      hmParentDetails = gmCrossReport.GenerateCrossTabReport(rsParentHeader,rsParentDetailsExpected);
      gmDBManager.close();      
      HashMap hmChildDetailsFooter =GmCommonClass.parseNullHashMap((HashMap)hmChildDetails.get("Footer"));
      HashMap hmParentDetailsFooter = GmCommonClass.parseNullHashMap((HashMap)hmParentDetails.get("Footer"));
      alParentDetails = GmCommonClass.parseNullArrayList((ArrayList)hmParentDetails.get("Details"));

      //If parent list has data, then we need display the Total of the child, Parent footers.
      if(alParentDetails.size()>0){            
          Set atrbSet = hmChildDetailsFooter.keySet();
          String strAtribName = "";
          double total =0.00;
          double childTotal =0.00;
          double parentTotal =0.00;
          for(Iterator it=atrbSet.iterator(); it.hasNext();){
          	    total = 0;
                strAtribName = GmCommonClass.parseNull((String)it.next());  
                if(strAtribName.equals("Name")){     //This is to display the Total as value of First column 
              	    hmTotalResult.put(strAtribName,"Total");
                }else{
              	    childTotal  =  Double.parseDouble(GmCommonClass.parseZero((String)hmChildDetailsFooter.get(strAtribName)));
              	    parentTotal =  Double.parseDouble(GmCommonClass.parseZero((String)hmParentDetailsFooter.get(strAtribName)));
                    total = childTotal + parentTotal;
                    hmTotalResult.put(strAtribName,String.valueOf(GmCommonClass.roundDigit(total, 0)));
                }
           }
        }
      //below Footer HashMap is to calculate the  Two cross Tab totals.
      hmParentDetails.put("MultiFooterTotal",(HashMap)hmTotalResult);
      hmReturn.put("PARENTDETAILS",hmParentDetails); 
      
      }      
      catch(Exception exp){
        gmDBManager.close();
        throw new AppError(exp);
      }   
      return hmReturn;
  } 
   
  public ArrayList fchOpenSheets(HashMap hmParam) throws AppError
  {         
	String strDemandSheetId =  GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETMONTHID")); 
    RowSetDynaClass rdResult = null;        
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);  
    log.debug(" values in fchOpenSheets " + hmParam);
    gmDBManager.setPrepareString("gm_pkg_oppr_sheet_summary.gm_op_fch_multipart_opensheets",2);       
    gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
    gmDBManager.setString(1,strDemandSheetId);
    gmDBManager.execute();
    rdResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(2));        
    gmDBManager.close();
    log.debug("Open Sheets" + (ArrayList)rdResult.getRows());
    return (ArrayList)rdResult.getRows() ;
    
  }
  
  /*  This Method is called to pull the Forecast data for all the Shared Parts into the Multi Part TTP.
   *  For the passed in demand Sheet, get the forecast information for the shared part and load it into the 
   *  t4042_demand_sheet_detail for the Multi Part Demand Sheet. 
   */
  public String pullDemandSheets(HashMap hmParam) throws AppError
  { 	         
	 
	  String strDemandSheetString = GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETSTRING")); 
	  String strUserID = GmCommonClass.parseNull((String)hmParam.get("USERID"));
	  log.debug("strDemandSheetString: "+ strDemandSheetString+ "strUserID: "+ strUserID);
	  String strTemp ="";
	  String strDSId ="";
	  String strDSName ="";
	  String strTempSheetDetails="";
	  String strErrDSDetails="";
	  String strReturnMsg="";
	  ArrayList outList = null;
	  HashMap hmDemandSheetDtls = new HashMap();
	  
	  StringTokenizer strTok = new StringTokenizer(strDemandSheetString,"|");
	  StringTokenizer strInnerTok = null;
	  GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
	  while (strTok.hasMoreTokens())
	  {
			strTempSheetDetails = strTok.nextToken();
			strInnerTok = new StringTokenizer(strTempSheetDetails,",");
			while (strInnerTok.hasMoreTokens())
			{
				 strDSId = GmCommonClass.parseNull((String)strInnerTok.nextToken());
				 strDSName = GmCommonClass.parseNull((String)strInnerTok.nextToken());
			}
			log.debug("strDSId::"+strDSId+":strDSName:"+strDSName);
			hmDemandSheetDtls.put(strDSId,strDSName);	 
			
		    gmDBManager.setPrepareString("gm_pkg_oppr_ld_demand.gm_op_sav_load_detail",3);       
		    gmDBManager.setString(1,strDSId);
		    gmDBManager.setString(2,"4000123");  //Demand Sheet Type
		    gmDBManager.setString(3,strUserID);  
		    try{ 
			    gmDBManager.execute();
			}catch(Exception e){
				//code for roll back the txn.
				strErrDSDetails = strErrDSDetails + "<br>"+strDSName +",";
			}
	  }
	  try{
	  	gmDBManager.commit();
	  }catch(Exception e){};
	  log.debug("strErrDSDetails::"+strErrDSDetails);
	  if(!strErrDSDetails.equals("")){
		  strReturnMsg = "The following demand Sheet are already schedule for Pull. "+strErrDSDetails.substring(0, strErrDSDetails.lastIndexOf(","));
	  }
	  log.debug("strReturnMsg "+strReturnMsg);
	  return strReturnMsg;
  }
  }
