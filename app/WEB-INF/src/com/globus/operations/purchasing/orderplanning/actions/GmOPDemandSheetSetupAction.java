package com.globus.operations.purchasing.orderplanning.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.purchasing.orderplanning.beans.GmOPDemandSetupBean;
import com.globus.operations.purchasing.orderplanning.forms.GmOPDemandSheetSetupForm;

public class GmOPDemandSheetSetupAction extends Action {
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    try {
      GmOPDemandSheetSetupForm gmOPDemandSheetSetupForm = (GmOPDemandSheetSetupForm) form;
      gmOPDemandSheetSetupForm.loadSessionParameters(request);
      GmOPDemandSetupBean GmOPDemandSetupBean = new GmOPDemandSetupBean();
      GmCommonBean gmCommonBean = new GmCommonBean();
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
      GmLogonBean gmLogonBean = new GmLogonBean();

      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.


      String strOpt = gmOPDemandSheetSetupForm.getStrOpt();
      String strDemandSheetId = gmOPDemandSheetSetupForm.getDemandSheetId();
      String strPartyId = GmCommonClass.parseNull(gmOPDemandSheetSetupForm.getSessPartyId());
      String strAccessFl = "";
      String strForward = "";

      HashMap hmTemp = new HashMap();
      HashMap hmParam = new HashMap();
      HashMap hmValues = new HashMap();
      HashMap hmAccess = new HashMap();
      ArrayList alDemandSheetList = new ArrayList();
      ArrayList alLogReasons = new ArrayList();
      ArrayList alDSOwner = new ArrayList();

      log.debug(GmCommonClass.getHashMapFromForm(gmOPDemandSheetSetupForm));
      log.debug(" Testing. ..... ");
      if (strOpt.equals("save") || strOpt.equals("inactivate")) {
        inActivateDemandMaster(mapping, form, request, response);
        strForward = "/gmOPDemandSheetSetup.do?strOpt=edit&demandSheetId=" + strDemandSheetId;
        ActionRedirect actionRedirect = new ActionRedirect(strForward);
        return actionRedirect;
      } else if (strOpt.equals("edit")) {
        hmValues = GmOPDemandSetupBean.fetchDemandSheetInfo(strDemandSheetId);
        log.debug(" values fetched for edit " + hmValues);
        gmOPDemandSheetSetupForm =
            (GmOPDemandSheetSetupForm) GmCommonClass.getFormFromHashMap(gmOPDemandSheetSetupForm,
                hmValues);
        hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
                "DEMAND_SHEET_EDIT"));
        strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        gmOPDemandSheetSetupForm.setEditAccess(strAccessFl);
      }

      gmOPDemandSheetSetupForm.setAlHierarchyList(GmOPDemandSetupBean.loadHierarchyList());
      // displaying comments
      alLogReasons =
          gmCommonBean.getLog(strDemandSheetId, "1227", GmCommonClass.getString("JNDI_CONNECTION"));
      gmOPDemandSheetSetupForm.setAlLogReasons(alLogReasons);

      alDSOwner = gmLogonBean.getEmployeeList("300", "W','P");
      gmOPDemandSheetSetupForm.setAlDSOwner(alDSOwner);
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }

    return mapping.findForward("success");
  }

  private void inActivateDemandMaster(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError {
    GmOPDemandSetupBean GmOPDemandSetupBean = new GmOPDemandSetupBean();
    HashMap hmParam = new HashMap();
    String strDemandSheetId = "";
    GmOPDemandSheetSetupForm gmOPDemandSheetSetupForm = (GmOPDemandSheetSetupForm) form;
    hmParam = GmCommonClass.getHashMapFromForm(gmOPDemandSheetSetupForm);
    strDemandSheetId = GmOPDemandSetupBean.saveDemandSheet(hmParam);
    gmOPDemandSheetSetupForm.setDemandSheetId(strDemandSheetId);
  }
}
