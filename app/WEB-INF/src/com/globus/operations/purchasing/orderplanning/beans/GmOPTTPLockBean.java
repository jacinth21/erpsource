package com.globus.operations.purchasing.orderplanning.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmOPTTPLockBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger

  
	//Adding Default and Parameterized constructor to ensure the company id is passed correctly.
	//Author:gpalani
  public GmOPTTPLockBean(GmDataStoreVO gmDataStoreVO) throws AppError 
  	  {
	    super(gmDataStoreVO);
	  }
  
  public GmOPTTPLockBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * fetchSetParDetails - This method will fetch the set Par details
   * 
   * @param hmParam
   * @exception AppError
   */
  public ArrayList fetchSetParDetails(HashMap hmParam) throws AppError {
    ArrayList alParDetails = new ArrayList();
    String strDemandSheets = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETIDS"));

    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_oppr_ttp.gm_fc_fch_setpar_details", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strDemandSheets);
    gmDBManager.execute();
    alParDetails =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();

    return alParDetails;
  }

  /**
   * fetchTTPRequestsDetails - This method will fetch request details
   * 
   * @param hmParam
   * @exception AppError
   */
  public ArrayList fetchTTPRequestsDetails(HashMap hmParam) throws AppError {
    ArrayList alRequestDetails = new ArrayList();
    String strDemandSheets = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETIDS"));

    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_oppr_ttp.gm_fc_fch_request_details", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strDemandSheets);
    gmDBManager.execute();
    alRequestDetails =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();

    return alRequestDetails;
  }

  /**
   * saveTTPDetails - This method will save the TTP details in DM Operation database
   * 
   * @param hmParam
   * @exception AppError
   */
  public HashMap saveTTPDetails(HashMap hmParam, GmDBManager gmDBManager) throws AppError {
    GmOPTTPSetupBean gmOPTTPSetupBean = new GmOPTTPSetupBean();
    String[] strPnumOrd = null;
    String strTTPDtlId = "";
    String strTTPType = "";
    HashMap hmOut = new HashMap();
    String strTTPId = GmCommonClass.parseNull((String) hmParam.get("TTPID"));
    String strDemandSheets = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETIDS"));
    String strForecastMonth = GmCommonClass.parseNull((String) hmParam.get("TTPFORECASTMONTHS"));
    String strInventoryId = GmCommonClass.parseNull((String) hmParam.get("INVENTORYID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strOrdQty = GmCommonClass.parseNull((String) hmParam.get("ORDSTR"));

    strPnumOrd = GmCommonClass.getStringTillLength("|", 4000, strOrdQty);
    ArrayList alRequestDetails = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_oppr_ttp.gm_fc_sav_ttp_dsmonthly", 9);

    for (int k = 0; k < strPnumOrd.length; k++) {
      if (strPnumOrd[k] == null)
        break;
      gmDBManager.registerOutParameter(8, OracleTypes.VARCHAR);
      gmDBManager.registerOutParameter(9, OracleTypes.VARCHAR);
      gmDBManager.setString(1, strTTPId);
      gmDBManager.setString(2, strDemandSheets);
      gmDBManager.setString(3, strForecastMonth);
      gmDBManager.setString(4, strInventoryId);
      gmDBManager.setString(5, strUserId);
      gmDBManager.setString(6, strPnumOrd[k]);
      gmDBManager.setString(7, Integer.toString(k));
      gmDBManager.setString(8, strTTPDtlId);

      gmDBManager.execute();
      strTTPDtlId = GmCommonClass.parseNull(gmDBManager.getString(8));
      strTTPType = GmCommonClass.parseNull(gmDBManager.getString(9));
    }

    hmOut.put("DMTTPDETAILID", strTTPDtlId);
    hmOut.put("DMTTPTYPE", strTTPType);

    return hmOut;
  }

  /**
   * saveTTPDetailsInGlobusApp - This method will save TTP details in web globus database
   * 
   * @param hmParam
   * @exception AppError
   */
  public void saveTTPDetailsInGlobusApp(HashMap hmParam, GmDBManager gmDBManagerTTPWeb,
      GmDBManager gmDBManagerDM) throws AppError {
// Adding log.error to verify if the company id is passed when locking the template
	  log.error("saveTTPDetailsInGlobusApp: Company ID"+getCompId() + "saveTTPDetailsInGlobusApp:Plant ID" + getCompPlantId());
	
      // PMT-32804 : TTP Summary lock and generate
	  // Code changed for Lock and generate (JMS) - This method used for JMS and portal.
	  // Handle the exception and update to T4052 table.
	  
	saveOrderQty(hmParam, gmDBManagerTTPWeb);
    String strErrorRequest = saveRequestDetails(hmParam, gmDBManagerTTPWeb, gmDBManagerDM);
    String strErrorPAR = saveParDetails(hmParam, gmDBManagerTTPWeb, gmDBManagerDM);
    
    strErrorRequest = strErrorRequest.equals("")? "": "The following Request error - while moving the common pool : "+strErrorRequest +"<br>";
    strErrorPAR = strErrorPAR.equals("")? "": "The following Set PAR Request error - while moving the common pool : "+strErrorPAR;
    
    log.debug(" Error details are "+ strErrorRequest );
    log.debug(" PAR error details  ==> "+strErrorPAR);
    
    if(!strErrorRequest.equals("") || !strErrorPAR.equals("")){
    	log.debug(" our error details are ==> "+strErrorRequest.concat(strErrorPAR));
    	throw new AppError(strErrorRequest.concat(strErrorPAR), "", 'E');
    }
    
  }

  /**
   * saveOrderQty - This method will save order qty and order cost in webglobus database
   * 
   * @param hmParam
   * @exception AppError
   */
  public void saveOrderQty(HashMap hmParam, GmDBManager gmDBManager) throws AppError {
    String[] strPnumOrd = null;
    String strDmTTPDtlId = "DM" + GmCommonClass.parseNull((String) hmParam.get("DMTTPDETAILID"));
    String strDmTTPType = GmCommonClass.parseNull((String) hmParam.get("DMTTPTYPE"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strOrdStr = GmCommonClass.parseNull((String) hmParam.get("ORDSTR"));
    strPnumOrd = GmCommonClass.getStringTillLength("|", 4000, strOrdStr);
    log.error("Part Num String Before Looping**"+strPnumOrd);
    gmDBManager.setPrepareString("gm_pkg_gop_ttp_lock.gm_fc_sav_ord_qty", 4);

    for (int k = 0; k < strPnumOrd.length; k++) {

    log.error("Part Num String loop** Part no#"+strPnumOrd[k]+"Demand Master ID#"+strDmTTPDtlId);

      if (strPnumOrd[k] == null)
      break;
      gmDBManager.setString(1, strPnumOrd[k]);
      gmDBManager.setString(2, strDmTTPDtlId);
      gmDBManager.setString(3, strDmTTPType);
      gmDBManager.setString(4, strUserId);
      gmDBManager.execute();
    }
  }

  /**
   * saveRequestDetails - This method will save request details in web globus database
   * 
   * @param hmParam
   * @exception AppError
   */
  public String saveRequestDetails(HashMap hmParam, GmDBManager gmDBManagerTTPWeb,
      GmDBManager gmDBManagerDM) throws AppError {
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strErrorRequest = "";
    ArrayList alRequestDetails =
        GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("REQUESTDETAILS"));
    Iterator itrRequestDetails = alRequestDetails.iterator();
    gmDBManagerTTPWeb.setPrepareString("gm_pkg_gop_ttp_lock.gm_fc_sav_request_qty", 9);

    while (itrRequestDetails.hasNext()) {
      HashMap hmRequestRow = GmCommonClass.parseNullHashMap((HashMap) itrRequestDetails.next());
      String strDemandID = GmCommonClass.parseNull((String) hmRequestRow.get("ID"));
      String strMasterDemandID = GmCommonClass.parseNull((String) hmRequestRow.get("MASTERID"));
      Date dPeriod = (Date) hmRequestRow.get("PERIOD");
      String strQty = GmCommonClass.parseNull((String) hmRequestRow.get("QTY"));
      String strSetID = GmCommonClass.parseNull((String) hmRequestRow.get("SET_ID"));
      String strRequestBy = GmCommonClass.parseNull((String) hmRequestRow.get("PUSER"));
      String strDemandType = GmCommonClass.parseNull((String) hmRequestRow.get("DTYPE"));
     // Adding log.error to verify if the company id is passed when locking the template
      log.error("saveRequestDetails: DemandID:" + strDemandID + "saveRequestDetails: MasterDemandID:" 
       + strMasterDemandID + "saveRequestDetails: DemandType:" + strDemandType + "saveRequestDetails: Demand period:" 
       + dPeriod + "saveRequestDetails : Company ID" + getCompId() + "saveRequestDetails: Plant ID" + getCompPlantId());
      
      

      // gmDBManagerTTPWeb.registerOutParameter(9, OracleTypes.CURSOR);
      gmDBManagerTTPWeb.registerOutParameter(9, OracleTypes.CLOB);
      gmDBManagerTTPWeb.setString(1, strDemandID);
      gmDBManagerTTPWeb.setString(2, strMasterDemandID);
      gmDBManagerTTPWeb.setDate(3, dPeriod);
      gmDBManagerTTPWeb.setString(4, strQty);
      gmDBManagerTTPWeb.setString(5, strSetID);
      gmDBManagerTTPWeb.setString(6, strRequestBy);
      gmDBManagerTTPWeb.setString(7, strDemandType);
      gmDBManagerTTPWeb.setString(8, strUserId);

      gmDBManagerTTPWeb.execute();
      
      strErrorRequest = strErrorRequest + GmCommonClass.parseNull(gmDBManagerTTPWeb.getString(9));
      log.debug(" Out Request error details "+strErrorRequest);
    }
    log.debug(" Final Error Out Request error details "+strErrorRequest);
    return strErrorRequest;
  }

  /**
   * saveParDetails - This method will save par details to the web globus database
   * 
   * @param hmParam
   * @exception AppError
   */
  public String saveParDetails(HashMap hmParam, GmDBManager gmDBManagerTTPWeb,
      GmDBManager gmDBManagerDM) throws AppError {
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strErrorPARDetails = "";
    ArrayList alParDetails = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("SETPAR"));
    Iterator itrParDetails = alParDetails.iterator();
    gmDBManagerTTPWeb.setPrepareString("gm_pkg_gop_ttp_lock.gm_fc_sav_set_par", 8);

    while (itrParDetails.hasNext()) {
      HashMap hmParRow = GmCommonClass.parseNullHashMap((HashMap) itrParDetails.next());
      String strDemandID = GmCommonClass.parseNull((String) hmParRow.get("ID"));
      String strDemandMasterID = GmCommonClass.parseNull((String) hmParRow.get("MASTERID"));
      String strSetID = GmCommonClass.parseNull((String) hmParRow.get("SET_ID"));
      String strQty = GmCommonClass.parseNull((String) hmParRow.get("QTY"));
      String strRequestBy = GmCommonClass.parseNull((String) hmParRow.get("PUSER"));
      String strDemandType = GmCommonClass.parseNull((String) hmParRow.get("DTYPE"));

      gmDBManagerTTPWeb.registerOutParameter(8, OracleTypes.CLOB);
      gmDBManagerTTPWeb.setString(1, strDemandID);
      gmDBManagerTTPWeb.setString(2, strDemandMasterID);
      gmDBManagerTTPWeb.setString(3, strQty);
      gmDBManagerTTPWeb.setString(4, strSetID);
      gmDBManagerTTPWeb.setString(5, strRequestBy);
      gmDBManagerTTPWeb.setString(6, strDemandType);
      gmDBManagerTTPWeb.setString(7, strUserId);
      log.debug(" before exeucte the method  ");
      gmDBManagerTTPWeb.execute();
      strErrorPARDetails = strErrorPARDetails + GmCommonClass.parseNull(gmDBManagerTTPWeb.getString(8));
      log.debug(" Error PAR details "+strErrorPARDetails);
      
         
    }
    log.debug(" saveParDetails Final Error  "+strErrorPARDetails);
    return strErrorPARDetails;
  } 
}
