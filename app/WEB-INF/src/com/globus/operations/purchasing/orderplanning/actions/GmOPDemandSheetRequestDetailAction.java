package com.globus.operations.purchasing.orderplanning.actions;

import java.util.HashMap;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;

import com.globus.common.beans.GmLogger;
import com.globus.operations.purchasing.orderplanning.beans.GmOPForecastReportBean;
import com.globus.operations.purchasing.orderplanning.forms.GmOPDemandSheetSummaryForm;

public class GmOPDemandSheetRequestDetailAction extends GmAction
{
    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
        try {
        	GmOPDemandSheetSummaryForm gmOPDemandSheetSummaryForm = (GmOPDemandSheetSummaryForm)form;
        	gmOPDemandSheetSummaryForm.loadSessionParameters(request);
            GmOPForecastReportBean gmOPForecastReportBean = new GmOPForecastReportBean(); 
            Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
        
            String strDemandSheetMonthId = gmOPDemandSheetSummaryForm.getDemandSheetMonthId();
            String strDemandTypeId = gmOPDemandSheetSummaryForm.getDemandTypeId();
            
            ArrayList alRequestDetaillist= new ArrayList();
           
            log.debug(" strDemandSheetMonthId ***" + strDemandSheetMonthId );
            alRequestDetaillist = gmOPForecastReportBean.reportDemandSheetRequestSummary(strDemandSheetMonthId, strDemandTypeId);
             
          //  request.setAttribute("RDREQUESTDETAIL",alRequestDetaillist);
            gmOPDemandSheetSummaryForm.setAlRequest(alRequestDetaillist);
            }
            catch(Exception exp)
            {
                
                exp.printStackTrace();
                throw new AppError(exp);
            }
            return mapping.findForward("success");
        }

}
