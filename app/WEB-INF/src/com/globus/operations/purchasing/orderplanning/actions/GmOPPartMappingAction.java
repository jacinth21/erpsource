//Source file: C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\accounts\\physicalaudit\\actions\\GmLockTagAction.java

package com.globus.operations.purchasing.orderplanning.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.operations.beans.GmPurchaseBean;
import com.globus.operations.purchasing.orderplanning.forms.GmOPSubPartOrderForm;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmOPPartMappingAction extends GmAction {
	// private String strOpt = LOAD / RELOAD / SAVE;

	/**
	 * @return org.apache.struts.action.ActionForward
	 * @roseuid 48D1292D0247
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {

			GmOPSubPartOrderForm gmOPSubPartOrderForm = (GmOPSubPartOrderForm) form;
			gmOPSubPartOrderForm.loadSessionParameters(request);

			GmProjectBean gmProj = new GmProjectBean();
			GmPurchaseBean gmPurchaseBean = new GmPurchaseBean();
			GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
			Logger log = GmLogger.getInstance(this.getClass().getName());

			String strOpt = gmOPSubPartOrderForm.getStrOpt();
			String strUserId = gmOPSubPartOrderForm.getUserId();

			RowSetDynaClass rdPartResult = null;

			HashMap hmParam = new HashMap();

			ArrayList alProject = new ArrayList();
			

			if (strOpt.equals("reload")) {
				
				
				String hsubcheck = gmOPSubPartOrderForm.getHsubpartNotcheck();
	            String hparentcheck = gmOPSubPartOrderForm.getHparentpartNotcheck();
	            
	            if(hsubcheck.equals("true")) gmOPSubPartOrderForm.setDisplaySubParts("");
	            if(hparentcheck.equals("true")) gmOPSubPartOrderForm.setDisplayParentParts("");
	            
	            hmParam = GmCommonClass.getHashMapFromForm(gmOPSubPartOrderForm);
				log.debug(" hmParam. ..... " + hmParam);
				rdPartResult = gmPurchaseBean.loadpartMapping(hmParam);

				log.debug(" rdPartResult.getRows().size() ..... " + rdPartResult.getRows().size());
				gmOPSubPartOrderForm.setReturnList(rdPartResult.getRows());

				if (gmOPSubPartOrderForm.getDisplayOrderParts().equals("on")) {
					gmSearchCriteria.addSearchCriteria("TOORDER", "N", GmSearchCriteria.NOTEQUALS);
				} else if (!(gmOPSubPartOrderForm.getDisplaySubParts().equals("on")&&gmOPSubPartOrderForm.getDisplayParentParts().equals("on"))){
					if  (gmOPSubPartOrderForm.getDisplaySubParts().equals("on")){

						gmSearchCriteria.addSearchCriteria("PART_TYPE", "SP", GmSearchCriteria.EQUALS);
					}
					if (gmOPSubPartOrderForm.getDisplayParentParts().equals("on")) {

						gmSearchCriteria.addSearchCriteria("PART_TYPE", "PP", GmSearchCriteria.EQUALS);
					}
				}
				gmSearchCriteria.query(rdPartResult.getRows());
			}
			HashMap hmProj = new HashMap();
			hmProj.put("COLUMN", "ID");
			hmProj.put("STATUSID", "20301"); // Filter all approved projects
			alProject = gmProj.reportProject(hmProj);
			gmOPSubPartOrderForm.setProjectList(alProject);

		} catch (Exception exp) {
			exp.printStackTrace();
			throw new AppError(exp);
		}

		return mapping.findForward("success");
	}

}
