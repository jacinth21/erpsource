package com.globus.operations.purchasing.orderplanning.forms;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmCancelForm;

public class GmOPGroupPartMapForm extends GmCancelForm
{
    
    private String groupId = "0";
    private String groupName = "";
    private String activeflag = "on";
    private String partNumbers = "";
    private String selectAll = "";
    private String enableSubComponent= "";
     
    
    private String setID = ""; 
    private String groupType = "";
    private String hpnum = "";
    private String hinputPrimaryParts = "";  //shared part pricing type inputs
    private String strPartListFlag = "";
    private String inputString = "";		// checked parts inputs
    
    private String hactiveflag ="";
    private String hviewEdit ="";
    private String groupNM ="";
    private String pricedType = "";
    private String strPricingByGroupFl = "";
    private String publishfl =""; 
    
	private String strEnableGrpType = "";
    
    private ArrayList alSetID = new ArrayList();
    private ArrayList alGroupType = new ArrayList();
    private ArrayList alPricedType = new ArrayList();
	private List ldtResult = null;
	private String enableGrpPartMapChange = "";
	private String strDisableLink = "";
	private String strDisablePricedType = "";
	private String strDisableGroupName = "";
	private String strHPartPriceFl ="";

	public String getStrHPartPriceFl() {
		return strHPartPriceFl;
	}

	public void setStrHPartPriceFl(String strHPartPriceFl) {
		this.strHPartPriceFl = strHPartPriceFl;
	}

	public String getStrDisablePricedType() {
		return strDisablePricedType;
	}

	public void setStrDisablePricedType(String strDisablePricedType) {
		this.strDisablePricedType = strDisablePricedType;
	}

	public String getStrDisableGroupName() {
		return strDisableGroupName;
	}

	public void setStrDisableGroupName(String strDisableGroupName) {
		this.strDisableGroupName = strDisableGroupName;
	}

	public String getStrDisableLink() {
		return strDisableLink;
	}

	public void setStrDisableLink(String strDisableLink) {
		this.strDisableLink = strDisableLink;
	}

	private String[] checkPartNumbers = new String [20]; 
    private ArrayList alGroupList = new ArrayList();
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    
    /**
	 * @return the strPricingByGroupFl
	 */
	public String getStrPricingByGroupFl() {
		return strPricingByGroupFl;
	}

	/**
	 * @param strPricingByGroupFl the strPricingByGroupFl to set
	 */
	public void setStrPricingByGroupFl(String strPricingByGroupFl) {
		this.strPricingByGroupFl = strPricingByGroupFl;
	}
	
    /**
     * @return Returns the activeflag.
     */
    public String getActiveflag()
    {
        return activeflag;
    }

    /**
     * @param activeflag The activeflag to set.
     */
    public void setActiveflag(String activeflag)
    {
        this.activeflag = activeflag;
    }

    /**
     * @return Returns the alGroupList.
     */
    public ArrayList getAlGroupList()
    {
        return alGroupList;
    }

    /**
     * @param alGroupList The alGroupList to set.
     */
    public void setAlGroupList(ArrayList alGroupList)
    {
        this.alGroupList = alGroupList;
    }

    /**
     * @return Returns the groupId.
     */
    public String getGroupId()
    {
        return groupId;
    }

    /**
     * @param groupId The groupId to set.
     */
    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    /**
     * @return Returns the groupName.
     */
    public String getGroupName()
    {
        return groupName;
    }

    /**
     * @param groupName The groupName to set.
     */
    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    /**
     * @return Returns the partNumbers.
     */
    public String getPartNumbers()
    {
        return partNumbers;
    }

    /**
     * @param partNumbers The partNumbers to set.
     */
    public void setPartNumbers(String partNumbers)
    {
        this.partNumbers = partNumbers;
    }

    /**
     * @return Returns the checkPartNumbers.
     */
    public String[] getCheckPartNumbers()
    {
        return checkPartNumbers;
    }

    /**
     * @param checkPartNumbers The checkPartNumbers to set.
     */
    public void setCheckPartNumbers(String[] checkPartNumbers)
    {
            this.checkPartNumbers = checkPartNumbers;
    }

    /**
     * @return Returns the selectAll.
     */
    public String getSelectAll()
    {
        return selectAll;
    }

    /**
     * @param selectAll The selectAll to set.
     */
    public void setSelectAll(String selectAll)
    {
        this.selectAll = selectAll;
    }
    
    /**
     * @return Returns the checkbox value.
     */
    public String getEnableSubComponent()
    {
        return enableSubComponent;
    }

    /**
     * @param enableSubComponent The checkbox value to set.
     */
    public void setEnableSubComponent(String enableSubComponent)
    {
        this.enableSubComponent = enableSubComponent;
    }


	/**
	 * @return the setID
	 */
	public String getSetID() {
		return setID;
	}

	/**
	 * @param setID the setID to set
	 */
	public void setSetID(String setID) {
		this.setID = setID;
	}

	/**
	 * @return the groupType
	 */
	public String getGroupType() {
		return groupType;
	}

	/**
	 * @param groupType the groupType to set
	 */
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	/**
	 * @return the alSetID
	 */
	public ArrayList getAlSetID() {
		return alSetID;
	}

	/**
	 * @param alSetID the alSetID to set
	 */
	public void setAlSetID(ArrayList alSetID) {
		this.alSetID = alSetID;
	}

	/**
	 * @return the alGroupType
	 */
	public ArrayList getAlGroupType() {
		return alGroupType;
	}

	/**
	 * @param alGroupType the alGroupType to set
	 */
	public void setAlGroupType(ArrayList alGroupType) {
		this.alGroupType = alGroupType;
	}

	/**
	 * @return the ldtResult
	 */
	public List getLdtResult() {
		return ldtResult;
	}

	/**
	 * @param ldtResult the ldtResult to set
	 */
	public void setLdtResult(List ldtResult) {
		this.ldtResult = ldtResult;
	}

	/**
	 * @return the hpnum
	 */
	public String getHpnum() {
		return hpnum;
	}

	/**
	 * @param hpnum the hpnum to set
	 */
	public void setHpnum(String hpnum) {
		this.hpnum = hpnum;
	}

	/**
	 * @return the hinputPrimaryParts
	 */
	public String getHinputPrimaryParts() {
		return hinputPrimaryParts;
	}

	/**
	 * @param hinputPrimaryParts the hinputPrimaryParts to set
	 */
	public void setHinputPrimaryParts(String hinputPrimaryParts) {
		this.hinputPrimaryParts = hinputPrimaryParts;
	}

	/**
	 * @return the strPartListFlag
	 */
	public String getStrPartListFlag() {
		return strPartListFlag;
	}

	/**
	 * @param strPartListFlag the strPartListFlag to set
	 */
	public void setStrPartListFlag(String strPartListFlag) {
		this.strPartListFlag = strPartListFlag;
	}

	/**
	 * @return the inputString
	 */
	public String getInputString() {
		return inputString;
	}

	/**
	 * @param inputString the inputString to set
	 */
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}

	/**
	 * @return the hactiveflag
	 */
	public String getHactiveflag() {
		return hactiveflag;
	}

	/**
	 * @param hactiveflag the hactiveflag to set
	 */
	public void setHactiveflag(String hactiveflag) {
		this.hactiveflag = hactiveflag;
	}

	/**
	 * @return the hviewEdit
	 */
	public String getHviewEdit() {
		return hviewEdit;
	}

	/**
	 * @param hviewEdit the hviewEdit to set
	 */
	public void setHviewEdit(String hviewEdit) {
		this.hviewEdit = hviewEdit;
	}

	/**
	 * @return the groupNM
	 */
	public String getGroupNM() {
		return groupNM;
	}

	/**
	 * @param groupNM the groupNM to set
	 */
	public void setGroupNM(String groupNM) {
		this.groupNM = groupNM;
	}

	
	public String getPricedType() {
		return pricedType;
	}

	public ArrayList getAlPricedType() {
		return alPricedType;
	}

	public void setAlPricedType(ArrayList alPricedType) {
		this.alPricedType = alPricedType;
	}

	public void setPricedType(String pricedType) {
		this.pricedType = pricedType;
	}
 
	 
	/**
	 * @return the publishfl
	 */
	public String getPublishfl() {
		return publishfl;
	}

	/**
	 * @param publishfl the publishfl to set
	 */
	public void setPublishfl(String publishfl) {
		this.publishfl = publishfl;
	}

	/**
	 * @return the strEnableGrpType
	 */
	public String getStrEnableGrpType() {
		return strEnableGrpType;
	}

	/**
	 * @param strEnableGrpType the strEnableGrpType to set
	 */
	public void setStrEnableGrpType(String strEnableGrpType) {
		this.strEnableGrpType = strEnableGrpType;
	}
	
	/**
	 * @return the enableGrpPartMapChange
	 */
	public String getEnableGrpPartMapChange() {
		return enableGrpPartMapChange;
	}

	/**
	 * @param enableGrpPartMapChange the enableGrpPartMapChange to set
	 */
	public void setEnableGrpPartMapChange(String enableGrpPartMapChange) {
		this.enableGrpPartMapChange = enableGrpPartMapChange;
	}

	

	
}
