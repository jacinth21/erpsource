package com.globus.operations.purchasing.orderplanning.displaytag;

import java.util.HashMap;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;
import  com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonConstants;
import com.globus.common.beans.GmLogger;
 

public class DTOPOpenSheetsWrapper extends TableDecorator
{
   Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS); 
   private int chkCnt = 0;
   
    /**
     * Returns the string for RequestId field icon.
     * @return String
     */

   public String getID()
    {
    	StringBuffer  strValue = new StringBuffer();
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();               
    	String strID   = GmCommonClass.parseNull((String)db.get("ID"));
    	String strName   = GmCommonClass.parseNull((String)db.get("NAME"));
    	strValue.setLength(0);
    	strValue.append("<input type='hidden' name='hmultiSheetname"+chkCnt+"' value='"+strName+"'>");
    	strValue.append("<input type='checkbox' name='multiSheet'"+ chkCnt++ +"value="+strID+" checked id="+strID+">"); 
       	return strValue.toString();               
    }    
       
       
}
