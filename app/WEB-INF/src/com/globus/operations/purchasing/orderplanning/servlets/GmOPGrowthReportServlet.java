/**
 * 
 */
package com.globus.operations.purchasing.orderplanning.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.operations.purchasing.orderplanning.beans.GmDemandMasterSetupBean;
import com.globus.operations.purchasing.orderplanning.beans.GmOPDemandSetupBean;
import com.globus.operations.purchasing.orderplanning.beans.GmOPGrowthReportBean;

/**
 * @author vprasath
 */
public class GmOPGrowthReportServlet extends GmServlet
{

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);

    }

    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession(false);
        String strComnPath = GmCommonClass.getString("GMCOMMON");
        String strOperPath = GmCommonClass.getString("GMORDERPLANNING");
        String strDispatchTo = strOperPath.concat("/GmOPGrowthReport.jsp");        
        Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing

        try
        {
            log.debug("Enter");
            checkSession(response, session); // Checks if the current session is valid, else redirecting to
                                                // SessionExpiry page
            String strAction = request.getParameter("hAction");
            if (strAction == null)
            {
                strAction = (String) session.getAttribute("hAction");
            }
            strAction = (strAction == null) ? "Load" : strAction;

            HashMap hmReturn = new HashMap();  
            ArrayList alDemandSheetList = new ArrayList();
            ArrayList alRefTypes = new ArrayList();
            GmOPGrowthReportBean gmOPGrowthReportBean = new GmOPGrowthReportBean();
            GmOPDemandSetupBean gmOPDemandSetupBean = new GmOPDemandSetupBean();
            GmDemandMasterSetupBean gmDemandMasterSetupBean = new GmDemandMasterSetupBean();
            
            String strDemandSheetId = "";
            String strRefInputs = "";           
            String strFromDate = "";
            String strToDate = "";
            String strDemandSheetName="";

            strDemandSheetId = GmCommonClass.parseNull((String)request.getParameter("demandSheetId"));
            strRefInputs = GmCommonClass.parseNull((String) request.getParameter("refInputs"));            
            strFromDate = GmCommonClass.parseNull((String) request.getParameter("Txt_FromDate"));
            strToDate = GmCommonClass.parseNull((String) request.getParameter("Txt_ToDate"));
            strDemandSheetName= GmCommonClass.parseNull((String)request.getParameter("searchdemandSheetId"));

            HashMap hmValues = new HashMap();
            hmValues.put("DEMANDSHEETID", strDemandSheetId);
            hmValues.put("REFINPUTS", strRefInputs);
            hmValues.put("FROMDATE", strFromDate);
            hmValues.put("TODATE", strToDate);
            
            if(strAction.equals("LoadReport"))
            {                
                hmReturn = gmOPGrowthReportBean.getGrowthReport(hmValues);
            }
             
           // include editable sheets only      
            hmReturn.put("DEMAND_SHEET_NAME",strDemandSheetName);
           
            alRefTypes = GmCommonClass.getCodeList("GRFTP");
            hmReturn.put("REFTYPES", alRefTypes);

            request.setAttribute("hmReturn", hmReturn);

            log.debug("Exit");
            dispatch(strDispatchTo, request, response);
        }// End of try
        catch (Exception e)
        {
            e.printStackTrace();
            session.setAttribute("hAppErrorMessage", e.getMessage());
            strDispatchTo = strComnPath + "/GmError.jsp";
            gotoPage(strDispatchTo, request, response);
        }// End of catch
    }// End of service method

}
