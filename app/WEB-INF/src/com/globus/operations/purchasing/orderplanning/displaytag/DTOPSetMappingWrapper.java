package com.globus.operations.purchasing.orderplanning.displaytag;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;

import com.globus.common.beans.GmCommonClass;
import com.globus.operations.purchasing.orderplanning.forms.GmOPDemandSheetSetupForm;


public class DTOPSetMappingWrapper extends TableDecorator
{
    private String strSetActionType; 
    private String strSetActionTypeNM;
    private String strStackMonth;
    private DynaBean db ;
    private int intCount = 0;
    private int intSize = 0;
    private int intmonthSize =0;
    private int intMonCount = 0;
    public StringBuffer  strValue = new StringBuffer();
    private ArrayList alList = new ArrayList();
    private HashMap hmVal = new HashMap();
    String strCodeID;
    String strCodeNM;
	String strSelected;
	
	String strDisabled;
	String strSetID;
	
	private ArrayList alnostack = new ArrayList();
	private ArrayList almonths = new ArrayList();
	GmOPDemandSheetSetupForm gmOPDemandSheetSetupForm;
    /**
     * Returns the string for text field.
     * @return String
     */
	
	public void init(PageContext pageContext, Object decorated, TableModel tableModel) {
		super.init(pageContext, decorated, tableModel);
		gmOPDemandSheetSetupForm = (GmOPDemandSheetSetupForm) getPageContext().getAttribute("frmOPDemandSheetSetup", PageContext.REQUEST_SCOPE);

		alnostack = gmOPDemandSheetSetupForm.getAlSetActionType();
		intSize = alnostack.size(); 

	//	strReconcilecombo = (String) getPageContext().getAttribute("reconcilecombo", PageContext.REQUEST_SCOPE);
		
		// log.debug("alUserList: "+ alUserList);
	}
	
    public String getSETACTIONTYPE()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strSetActionType = String.valueOf(db.get("SETACTIONTYPE"));
      //  strSetActionTypeNM   = (String)db.get("SETACTIONTYPENM");
      /*  try {
        	alList = GmCommonClass.getCodeList("SATYP");
        	intSize = alList.size();
        }
        catch(Exception exp){
            exp.printStackTrace();
            }*/
        strValue.setLength(0);
        strValue.append("<select name = setactiontype");
        strValue.append(String.valueOf(intCount) );
        strValue.append(" OnChange='fnUpdate(this.name);' > "); 
        strValue.append("<option value= >"); 
        strValue.append("[Choose One]</option>");                       
    /*    strValue.append("<option value='");
        strValue.append(strSetActionType);
        strValue.append("'>");
        strValue.append(strSetActionTypeNM);
       */
        
        for (int i=0;i<intSize;i++)
        {
				hmVal = (HashMap)alnostack.get(i);
				strCodeID = (String)hmVal.get("CODEID");
				strSelected = strSetActionType.equals(strCodeID)?"selected":"";
				strValue.append("<option " + strSelected + " value= " ) ;
				strValue.append( strCodeID +  ">" + hmVal.get("CODENM") + "</option>");
		}
        strValue.append( "</select>");
        intCount++;
        return strValue.toString();
       
        //return "<input type = text name = parvalue size = 10 value="+strPar+">";
        
    }
     
    public String getMONTHID()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strStackMonth = String.valueOf(db.get("MONTHID"));
        strSetID = String.valueOf(db.get("SETID"));
        
        if(strStackMonth.equals("null")) strStackMonth ="";
        if(strSetID.equals("null")) strSetID ="";
        
        strSetActionType = String.valueOf(db.get("SETACTIONTYPE"));
        
        if (strSetActionType.equals("50291")||strSetActionType.equals("null") )   //no stack
        	strDisabled ="disabled";
        else
        	strDisabled ="";
        
        strValue.setLength(0);
        strValue.append("<input type = text name = stackmonth");
        strValue.append(String.valueOf(intMonCount) );
        strValue.append(" maxlength='2' size = 3 value='");
        strValue.append(strStackMonth);
        strValue.append("' ");
        strValue.append(strDisabled);
        strValue.append("> month");
        intMonCount++;
        return strValue.toString();
       
        //return "<input type = text name = parvalue size = 10 value="+strPar+">";
        
    }
     
    
}


