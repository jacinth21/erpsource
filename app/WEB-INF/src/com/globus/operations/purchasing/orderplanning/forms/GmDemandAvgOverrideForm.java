package com.globus.operations.purchasing.orderplanning.forms;

import com.globus.common.forms.GmLogForm;

public class GmDemandAvgOverrideForm extends GmLogForm{

	private String demandSheetId = "";     
	private String demandMasterId = "";    
	private String demandOverrideId = "";
	private String demandSheetNm = "";
	private String strDescription = "";
	private String refID = "";             
	private String refType = "";
	private String refTypeNm = "";
	private String systemWeigthedAvg = ""; 
	private String txtWeigthedAvg = "";
	private String overrideWeigthedAvg = "";
	private String historyFl = "";
	private String strAccessFl = "";
	/**
	 * @return the strDescription
	 */
	public String getStrDescription() {
		return strDescription;
	}
	/**
	 * @param strDescription the strDescription to set
	 */
	public void setStrDescription(String strDescription) {
		this.strDescription = strDescription;
	}
	/**
	 * @return the demandSheetId
	 */
	public String getDemandSheetId() {
		return demandSheetId;
	}
	/**
	 * @param demandSheetId the demandSheetId to set
	 */
	public void setDemandSheetId(String demandSheetId) {
		this.demandSheetId = demandSheetId;
	}
	/**
	 * @return the demandMasterId
	 */
	public String getDemandMasterId() {
		return demandMasterId;
	}
	/**
	 * @param demandMasterId the demandMasterId to set
	 */
	public void setDemandMasterId(String demandMasterId) {
		this.demandMasterId = demandMasterId;
	}
	/**
	 * @return the demandOverrideId
	 */
	public String getDemandOverrideId() {
		return demandOverrideId;
	}
	/**
	 * @param demandOverrideId the demandOverrideId to set
	 */
	public void setDemandOverrideId(String demandOverrideId) {
		this.demandOverrideId = demandOverrideId;
	}
	/**
	 * @return the demandSheetNm
	 */
	public String getDemandSheetNm() {
		return demandSheetNm;
	}
	/**
	 * @param demandSheetNm the demandSheetNm to set
	 */
	public void setDemandSheetNm(String demandSheetNm) {
		this.demandSheetNm = demandSheetNm;
	}
	/**
	 * @return the refType
	 */
	public String getRefType() {
		return refType;
	}
	/**
	 * @param refType the refType to set
	 */
	public void setRefType(String refType) {
		this.refType = refType;
	}
	/**
	 * @return the systemWeigthedAvg
	 */
	public String getSystemWeigthedAvg() {
		return systemWeigthedAvg;
	}
	/**
	 * @param systemWeigthedAvg the systemWeigthedAvg to set
	 */
	public void setSystemWeigthedAvg(String systemWeigthedAvg) {
		this.systemWeigthedAvg = systemWeigthedAvg;
	}

	/**
	 * @return the strAccessFl
	 */
	public String getStrAccessFl() {
		return strAccessFl;
	}
	/**
	 * @param strAccessFl the strAccessFl to set
	 */
	public void setStrAccessFl(String strAccessFl) {
		this.strAccessFl = strAccessFl;
	}
	/**
	 * @return the refID
	 */
	public String getRefID() {
		return refID;
	}
	/**
	 * @param refID the refID to set
	 */
	public void setRefID(String refID) {
		this.refID = refID;
	}
	/**
	 * @return the overrideWeigthedAvg
	 */
	public String getOverrideWeigthedAvg() {
		return overrideWeigthedAvg;
	}
	/**
	 * @param overrideWeigthedAvg the overrideWeigthedAvg to set
	 */
	public void setOverrideWeigthedAvg(String overrideWeigthedAvg) {
		this.overrideWeigthedAvg = overrideWeigthedAvg;
	}
	/**
	 * @return the historyFl
	 */
	public String getHistoryFl() {
		return historyFl;
	}
	/**
	 * @param historyFl the historyFl to set
	 */
	public void setHistoryFl(String historyFl) {
		this.historyFl = historyFl;
	}
	/**
	 * @return the refTypeNm
	 */
	public String getRefTypeNm() {
		return refTypeNm;
	}
	/**
	 * @param refTypeNm the refTypeNm to set
	 */
	public void setRefTypeNm(String refTypeNm) {
		this.refTypeNm = refTypeNm;
	}
	/**
	 * @return the txtWeigthedAvg
	 */
	public String getTxtWeigthedAvg() {
		return txtWeigthedAvg;
	}
	/**
	 * @param txtWeigthedAvg the txtWeigthedAvg to set
	 */
	public void setTxtWeigthedAvg(String txtWeigthedAvg) {
		this.txtWeigthedAvg = txtWeigthedAvg;
	}

}
