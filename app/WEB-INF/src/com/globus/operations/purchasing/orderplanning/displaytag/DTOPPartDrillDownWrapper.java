package com.globus.operations.purchasing.orderplanning.displaytag;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TotalTableDecorator;
import com.globus.common.beans.GmCommonClass;

public class DTOPPartDrillDownWrapper extends TotalTableDecorator
{
    private DynaBean db ;
    private String strType = "";
    private String strID = "";
    private String strCountryID = "";
    
    public String getCSGID()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strType     = GmCommonClass.parseNull((String)db.get("STYPE"));
        strID      = GmCommonClass.parseNull((String)db.get("CSGID"));
        strCountryID = GmCommonClass.parseNull((String)db.get("COUNTRYID"));

        if (strType.equals("40020") )
        {
             return "&nbsp;<a class='red' href=javascript:fnPrintPack('" + 
             strID + "')> "  + strID + "</a>" ;
        }
        else if (strType.equals("40022") )
        {
             return "&nbsp;<a href=javascript:fnPrintConsignVer('" + strID + "')> "  + strID + "</a>" ;
        }
        else if (strType.equals("40021") )	// 40021 - Consignment
        {
            return "&nbsp;<a href=javascript:fnPrintConsignment('" + strID + "','" + strCountryID + "')> "  + strID + "</a>" ;
        }
        else if (strType.equals("3301") || strType.equals("3302"))	// 3301-Return Consignment - Set , 3302- Return Consignment - Items
        {
        	return "&nbsp;<a href=javascript:fnPrintReturnsVer('" + strID + "','" + strCountryID + "')> "  + strID + "</a>" ;
       }
        else {
        	return "&nbsp;"+strID;
        }
    }
}
