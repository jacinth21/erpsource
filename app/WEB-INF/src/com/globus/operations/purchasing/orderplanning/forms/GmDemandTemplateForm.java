package com.globus.operations.purchasing.orderplanning.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmLogForm;

public class GmDemandTemplateForm extends GmLogForm{

	private String demandTempltId = "";
	private String demandTempltNm = ""; 
	private String demandPeriod = "";
	private String forecastPeriod = "";
	private String setBuild = "";
	private String demandRuleVal = "";
	private String forecastRuleVal = "";
	private String setBuildRuleVal = "";
	private String companyId = "";
	private String demandSheetOwner = "";
	private String companyNm = "";
	private String ownerNm = "";
	private String sheetStatus = "";
	private String inactiveFlag = "";
	private String inputString = "";
	private String submitFlag = "";
	private String sheetStatusId = "";
	
    private String[] checkGroupAvail = new String [5];
    private String[] checkGroupAssoc = new String [5];
    private String[] checkConSetAvail = new String [5];
    private String[] checkConSetAssoc = new String [5];
    private String[] checkInhSetAvail = new String [5];
    private String[] checkInhSetAssoc = new String [5];
	
	private ArrayList alDemandTemplt =  new ArrayList();
	private ArrayList alCompany =  new ArrayList();
	private ArrayList alDSOwner =  new ArrayList();
	private ArrayList alGroupAvail = new ArrayList();
	private ArrayList alGroupAssoc = new ArrayList();
	private ArrayList alConSetAvail = new ArrayList();
	private ArrayList alConSetAssoc = new ArrayList();
	private ArrayList alInhSetAvail = new ArrayList();
	private ArrayList alInhSetAssoc = new ArrayList();
	private ArrayList alDemandSheets = new ArrayList();
	

	
	/**
	 * @return the companyNm
	 */
	public String getCompanyNm() {
		return companyNm;
	}
	/**
	 * @param companyNm the companyNm to set
	 */
	public void setCompanyNm(String companyNm) {
		this.companyNm = companyNm;
	}
	/**
	 * @return the ownerNm
	 */
	public String getOwnerNm() {
		return ownerNm;
	}
	/**
	 * @param ownerNm the ownerNm to set
	 */
	public void setOwnerNm(String ownerNm) {
		this.ownerNm = ownerNm;
	}
	/**
	 * @return the sheetStatus
	 */
	public String getSheetStatus() {
		return sheetStatus;
	}
	/**
	 * @param sheetStatus the sheetStatus to set
	 */
	public void setSheetStatus(String sheetStatus) {
		this.sheetStatus = sheetStatus;
	}
	/**
	 * @return the alDemandSheets
	 */
	public ArrayList getAlDemandSheets() {
		return alDemandSheets;
	}
	/**
	 * @param alDemandSheets the alDemandSheets to set
	 */
	public void setAlDemandSheets(ArrayList alDemandSheets) {
		this.alDemandSheets = alDemandSheets;
	}
	/**
	 * @return the inputString
	 */
	public String getInputString() {
		return inputString;
	}
	/**
	 * @param inputString the inputString to set
	 */
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}
	/**
	 * @return the checkGroupAvail
	 */
	public String[] getCheckGroupAvail() {
		return checkGroupAvail;
	}
	/**
	 * @param checkGroupAvail the checkGroupAvail to set
	 */
	public void setCheckGroupAvail(String[] checkGroupAvail) {
		this.checkGroupAvail = checkGroupAvail;
	}
	/**
	 * @return the checkGroupAssoc
	 */
	public String[] getCheckGroupAssoc() {
		return checkGroupAssoc;
	}
	/**
	 * @param checkGroupAssoc the checkGroupAssoc to set
	 */
	public void setCheckGroupAssoc(String[] checkGroupAssoc) {
		this.checkGroupAssoc = checkGroupAssoc;
	}
	/**
	 * @return the checkConSetAvail
	 */
	public String[] getCheckConSetAvail() {
		return checkConSetAvail;
	}
	/**
	 * @param checkConSetAvail the checkConSetAvail to set
	 */
	public void setCheckConSetAvail(String[] checkConSetAvail) {
		this.checkConSetAvail = checkConSetAvail;
	}
	/**
	 * @return the checkConSetAssoc
	 */
	public String[] getCheckConSetAssoc() {
		return checkConSetAssoc;
	}
	/**
	 * @param checkConSetAssoc the checkConSetAssoc to set
	 */
	public void setCheckConSetAssoc(String[] checkConSetAssoc) {
		this.checkConSetAssoc = checkConSetAssoc;
	}
	/**
	 * @return the checkInhSetAvail
	 */
	public String[] getCheckInhSetAvail() {
		return checkInhSetAvail;
	}
	/**
	 * @param checkInhSetAvail the checkInhSetAvail to set
	 */
	public void setCheckInhSetAvail(String[] checkInhSetAvail) {
		this.checkInhSetAvail = checkInhSetAvail;
	}
	/**
	 * @return the checkInhSetAssoc
	 */
	public String[] getCheckInhSetAssoc() {
		return checkInhSetAssoc;
	}
	/**
	 * @param checkInhSetAssoc the checkInhSetAssoc to set
	 */
	public void setCheckInhSetAssoc(String[] checkInhSetAssoc) {
		this.checkInhSetAssoc = checkInhSetAssoc;
	}
	/**
	 * @return the alGroupAvail
	 */
	public ArrayList getAlGroupAvail() {
		return alGroupAvail;
	}
	/**
	 * @param alGroupAvail the alGroupAvail to set
	 */
	public void setAlGroupAvail(ArrayList alGroupAvail) {
		this.alGroupAvail = alGroupAvail;
	}
	/**
	 * @return the alGroupAssoc
	 */
	public ArrayList getAlGroupAssoc() {
		return alGroupAssoc;
	}
	/**
	 * @param alGroupAssoc the alGroupAssoc to set
	 */
	public void setAlGroupAssoc(ArrayList alGroupAssoc) {
		this.alGroupAssoc = alGroupAssoc;
	}
	/**
	 * @return the alConSetAvail
	 */
	public ArrayList getAlConSetAvail() {
		return alConSetAvail;
	}
	/**
	 * @param alConSetAvail the alConSetAvail to set
	 */
	public void setAlConSetAvail(ArrayList alConSetAvail) {
		this.alConSetAvail = alConSetAvail;
	}
	/**
	 * @return the alConSetAssoc
	 */
	public ArrayList getAlConSetAssoc() {
		return alConSetAssoc;
	}
	/**
	 * @param alConSetAssoc the alConSetAssoc to set
	 */
	public void setAlConSetAssoc(ArrayList alConSetAssoc) {
		this.alConSetAssoc = alConSetAssoc;
	}
	/**
	 * @return the alInhSetAvail
	 */
	public ArrayList getAlInhSetAvail() {
		return alInhSetAvail;
	}
	/**
	 * @param alInhSetAvail the alInhSetAvail to set
	 */
	public void setAlInhSetAvail(ArrayList alInhSetAvail) {
		this.alInhSetAvail = alInhSetAvail;
	}
	/**
	 * @return the alInhSetAssoc
	 */
	public ArrayList getAlInhSetAssoc() {
		return alInhSetAssoc;
	}
	/**
	 * @param alInhSetAssoc the alInhSetAssoc to set
	 */
	public void setAlInhSetAssoc(ArrayList alInhSetAssoc) {
		this.alInhSetAssoc = alInhSetAssoc;
	}
	/**
	 * @return the setBuild
	 */
	public String getSetBuild() {
		return setBuild;
	}
	/**
	 * @param setBuild the setBuild to set
	 */
	public void setSetBuild(String setBuild) {
		this.setBuild = setBuild;
	}
	/**
	 * @return the setBuildRuleVal
	 */
	public String getSetBuildRuleVal() {
		return setBuildRuleVal;
	}
	/**
	 * @param setBuildRuleVal the setBuildRuleVal to set
	 */
	public void setSetBuildRuleVal(String setBuildRuleVal) {
		this.setBuildRuleVal = setBuildRuleVal;
	}
	/**
	 * @return the demandRuleVal
	 */
	public String getDemandRuleVal() {
		return demandRuleVal;
	}
	/**
	 * @param demandRuleVal the demandRuleVal to set
	 */
	public void setDemandRuleVal(String demandRuleVal) {
		this.demandRuleVal = demandRuleVal;
	}
	/**
	 * @return the forecastRuleVal
	 */
	public String getForecastRuleVal() {
		return forecastRuleVal;
	}
	/**
	 * @param forecastRuleVal the forecastRuleVal to set
	 */
	public void setForecastRuleVal(String forecastRuleVal) {
		this.forecastRuleVal = forecastRuleVal;
	}
	/**
	 * @return the demandTempltId
	 */
	public String getDemandTempltId() {
		return demandTempltId;
	}
	/**
	 * @param demandTempltId the demandTempltId to set
	 */
	public void setDemandTempltId(String demandTempltId) {
		this.demandTempltId = demandTempltId;
	}
	/**
	 * @return the demandTempltNm
	 */
	public String getDemandTempltNm() {
		return demandTempltNm;
	}
	/**
	 * @param demandTempltNm the demandTempltNm to set
	 */
	public void setDemandTempltNm(String demandTempltNm) {
		this.demandTempltNm = demandTempltNm;
	}
	/**
	 * @return the demandPeriod
	 */
	public String getDemandPeriod() {
		return demandPeriod;
	}
	/**
	 * @param demandPeriod the demandPeriod to set
	 */
	public void setDemandPeriod(String demandPeriod) {
		this.demandPeriod = demandPeriod;
	}
	/**
	 * @return the forecastPeriod
	 */
	public String getForecastPeriod() {
		return forecastPeriod;
	}
	/**
	 * @param forecastPeriod the forecastPeriod to set
	 */
	public void setForecastPeriod(String forecastPeriod) {
		this.forecastPeriod = forecastPeriod;
	}
	/**
	 * @return the companyId
	 */
	public String getCompanyId() {
		return companyId;
	}
	/**
	 * @param companyId the companyId to set
	 */
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	/**
	 * @return the demandSheetOwner
	 */
	public String getDemandSheetOwner() {
		return demandSheetOwner;
	}
	/**
	 * @param demandSheetOwner the demandSheetOwner to set
	 */
	public void setDemandSheetOwner(String demandSheetOwner) {
		this.demandSheetOwner = demandSheetOwner;
	}
	/**
	 * @return the inactiveFlag
	 */
	public String getInactiveFlag() {
		return inactiveFlag;
	}
	/**
	 * @param inactiveFlag the inactiveFlag to set
	 */
	public void setInactiveFlag(String inactiveFlag) {
		this.inactiveFlag = inactiveFlag;
	}
	/**
	 * @return the submitFlag
	 */
	public String getSubmitFlag() {
		return submitFlag;
	}
	/**
	 * @param submitFlag the submitFlag to set
	 */
	public void setSubmitFlag(String submitFlag) {
		this.submitFlag = submitFlag;
	}
	/**
	 * @return the sheetStatusId
	 */
	public String getSheetStatusId() {
		return sheetStatusId;
	}
	/**
	 * @param sheetStatusId the sheetStatusId to set
	 */
	public void setSheetStatusId(String sheetStatusId) {
		this.sheetStatusId = sheetStatusId;
	}
	/**
	 * @return the alDemandTemplt
	 */
	public ArrayList getAlDemandTemplt() {
		return alDemandTemplt;
	}
	/**
	 * @param alDemandTemplt the alDemandTemplt to set
	 */
	public void setAlDemandTemplt(ArrayList alDemandTemplt) {
		this.alDemandTemplt = alDemandTemplt;
	}
	/**
	 * @return the alCompany
	 */
	public ArrayList getAlCompany() {
		return alCompany;
	}
	/**
	 * @param alCompany the alCompany to set
	 */
	public void setAlCompany(ArrayList alCompany) {
		this.alCompany = alCompany;
	}
	/**
	 * @return the alDSOwner
	 */
	public ArrayList getAlDSOwner() {
		return alDSOwner;
	}
	/**
	 * @param alDSOwner the alDSOwner to set
	 */
	public void setAlDSOwner(ArrayList alDSOwner) {
		this.alDSOwner = alDSOwner;
	}
	
}
