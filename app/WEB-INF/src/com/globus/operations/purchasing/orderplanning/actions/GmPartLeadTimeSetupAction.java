package com.globus.operations.purchasing.orderplanning.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.purchasing.orderplanning.beans.GmDemandMasterSetupBean;
import com.globus.operations.purchasing.orderplanning.forms.GmPartLeadTimeSetupFrom;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmPartLeadTimeSetupAction extends GmAction {
	
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Logger log = GmLogger.getInstance(this.getClass().getName());
		
		String strDispatchTo="gmPartLeadTimeSetup";
		
		GmPartLeadTimeSetupFrom gmPartLeadTimeSetupFrom = (GmPartLeadTimeSetupFrom) form;
		gmPartLeadTimeSetupFrom.loadSessionParameters(request);
			 
		GmProjectBean gmProjectBean = new GmProjectBean();
		GmDemandMasterSetupBean gmDemandMasterSetupBean = new GmDemandMasterSetupBean();
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
		GmCommonBean gmCommonBean = new GmCommonBean();

		String strPartyId = GmCommonClass.parseNull((String)gmPartLeadTimeSetupFrom.getSessPartyId());
		String strOpt = gmPartLeadTimeSetupFrom.getStrOpt();
		HashMap hmRuleData = new HashMap();
		String strRuleID = "4000101";
		String strRuleGrpId = "LEADTIME";
		String strSearch = "";
		String strPartNum = "";
		String strPartNumFormat = "";
		hmRuleData.put("RULEID", strRuleID);
		hmRuleData.put("RULEGROUP", strRuleGrpId);
		String strDefaultLead = GmCommonClass.parseNull((String)gmCommonBean.getRuleValue(hmRuleData));
		ArrayList alWildSearch = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LIKES"));
		RowSetDynaClass rdSubResult = null;
		
		HashMap hmParam = new HashMap();
		HashMap hmReturn = new HashMap();
		HashMap hmAccess = new HashMap();
		ArrayList alProject = new ArrayList();
		
		String strUpdFl = "";
			
		hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId, "LEAD_TIME"));
		strUpdFl = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
		gmPartLeadTimeSetupFrom.setAccessFl(strUpdFl);
		gmPartLeadTimeSetupFrom.setDefaultLeadWeek(strDefaultLead);
		
		hmParam = GmCommonClass.getHashMapFromForm(gmPartLeadTimeSetupFrom); 
		
		if (strOpt.equals("save"))
		{
			log.debug(" hmParam.....Before Save " + hmParam);
			gmDemandMasterSetupBean.savePartLeadTime(hmParam);
			
			String strProjectListId = GmCommonClass.parseNull((String)hmParam.get("PROJECTLISTID"));
    	    strPartNum = GmCommonClass.parseNull((String)hmParam.get("PNUM"));
      	  	String strRedirectURL = "/gmPartLeadTimeSetup.do?strOpt=reload&projectListID="+strProjectListId+"&pnum="+strPartNum;
      	  	ActionRedirect actionRedirect = new ActionRedirect(strRedirectURL);
		    return actionRedirect;
		}
		if (strOpt.equals("reload")){
			strSearch = GmCommonClass.parseNull((String) gmPartLeadTimeSetupFrom.getCbo_search());
			strPartNum = GmCommonClass.parseNull((String) gmPartLeadTimeSetupFrom.getPnum());
			strPartNumFormat = strPartNum;
			if (strSearch.equals("90181")) {
				strPartNumFormat = ("^").concat(strPartNumFormat);
				strPartNumFormat = strPartNumFormat.replaceAll(",", "|^");
			} else if (strSearch.equals("90182")) {
				strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|");
				strPartNumFormat = strPartNumFormat.concat("$");
			}else{
				strPartNumFormat = strPartNumFormat.replaceAll(",", "|");
			}
			hmParam.put("PNUM", strPartNumFormat);
			hmReturn = gmDemandMasterSetupBean.fetchLeadtimeParts(hmParam);
			log.debug(" hmReturn...... " + hmReturn);
			rdSubResult = (RowSetDynaClass) hmReturn.get("LEADTIMEPARTS");
			gmPartLeadTimeSetupFrom.setReturnList(rdSubResult.getRows());	 
			gmPartLeadTimeSetupFrom.setCbo_search(strSearch);
		} 

		HashMap hmProj = new HashMap();
		hmProj.put("COLUMN", "ID");
		hmProj.put("STATUSID", "20301"); // Filter all approved projects
		alProject = gmProjectBean.reportProject(hmProj); 
		gmPartLeadTimeSetupFrom.setProjectList(alProject);
		gmPartLeadTimeSetupFrom.setAlWildSearch(alWildSearch);			 
		return mapping.findForward(strDispatchTo);
	}

}
