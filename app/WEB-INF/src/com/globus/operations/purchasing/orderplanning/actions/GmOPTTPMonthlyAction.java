package com.globus.operations.purchasing.orderplanning.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.purchasing.orderplanning.beans.GmOPDemandReportBean;
import com.globus.operations.purchasing.orderplanning.beans.GmOPForecastTransBean;
import com.globus.operations.purchasing.orderplanning.beans.GmOPTTPSetupBean;
import com.globus.operations.purchasing.orderplanning.forms.GmOPTTPMonthlyForm;


public class GmOPTTPMonthlyAction extends GmAction
{
   
    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
        try {
        GmOPTTPMonthlyForm gmOPTTPMonthlyForm = (GmOPTTPMonthlyForm)form;
        gmOPTTPMonthlyForm.loadSessionParameters(request);
        GmOPForecastTransBean gmOPForecastTransBean = new GmOPForecastTransBean();
        GmOPTTPSetupBean gmOPTTPSetupBean = new GmOPTTPSetupBean();
        GmCommonBean gmCommonBean = new GmCommonBean();
        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
        
        HashMap hmTemp = new HashMap();
        HashMap hmParam = new HashMap();
        HashMap hmValues = new HashMap();
        String strInvId = "";
        hmParam = GmCommonClass.getHashMapFromForm(gmOPTTPMonthlyForm);
        String strOpt = GmCommonClass.parseNull((String)gmOPTTPMonthlyForm.getStrOpt());
        String strCompanyID = GmCommonClass.parseNull((String)gmOPTTPMonthlyForm.gethCompanyID());
        log.debug(" Action is " + strOpt  + " Forecast months  " + gmOPTTPMonthlyForm.getTtpForecastMonths()+"<<strCompanyID>>"+strCompanyID);
        String strInvLockType = "20430";
   	   //PMT-28391To get the forecast show/hide flag from Rules.Rule id - SHOW_FORECAST,Rule Group - TTP_FORECAST
        String strShowForecastFl = GmCommonClass.parseNull(GmCommonClass.getRuleValue("SHOW_FORECAST", "TTP_FORECAST"));
        gmOPTTPMonthlyForm.setStrShowForecastFl(strShowForecastFl);
        
        hmParam.put("INVLOCKTYPE", strInvLockType);
        if (strOpt.equals("edit")) {
        	ArrayList alSelectedSheet=GmCommonClass.parseNullArrayList(gmOPForecastTransBean.loadTTPLinkData(hmParam));
        	gmOPTTPMonthlyForm.setSubmitSts(alSelectedSheet.size()>0?"":"disabled");
        	gmOPTTPMonthlyForm.setAlSelectedSheet(alSelectedSheet);
            gmOPTTPMonthlyForm.setAlInventoryIdList(gmOPForecastTransBean.loadInventoryIDList(hmParam));
            gmOPTTPMonthlyForm.setTtpForecastMonths(gmOPForecastTransBean.getForecastMonthRule());  
        }
        if (!strOpt.equals("") && !strCompanyID.equals("")) {
        	gmOPTTPMonthlyForm.setAlTTPLevel((ArrayList)gmOPTTPSetupBean.loadTTPLevelList(strCompanyID)); // Should Return alTTPLevel 
        }
        
        // Get the Inventory ID and set it to the Form, this will be used to pre-populate the Inventory Dropdown in the UI.
        strInvId = (new GmOPDemandReportBean()).fetchTTPInventoryid(hmParam);
        gmOPTTPMonthlyForm.sethInventoryId(strInvId);
        
        log.debug(strInvId+" count of loadTTPList  " + gmOPTTPSetupBean.loadTTPList(hmTemp)+" values in gmPartGroupForm form --> " + GmCommonClass.getHashMapFromForm(gmOPTTPMonthlyForm));
        gmOPTTPMonthlyForm.setAlTTPList(gmOPTTPSetupBean.loadTTPList(hmTemp));
        gmOPTTPMonthlyForm.setAlMonth(GmCommonClass.parseNullArrayList(gmCommonBean.getMonthList()));
        gmOPTTPMonthlyForm.setAlYear(GmCommonClass.parseNullArrayList(gmCommonBean.getYearList()));
        
        }
        catch(Exception exp){
            exp.printStackTrace();
            throw new AppError(exp);
        }
        return mapping.findForward("success");
    }
}
