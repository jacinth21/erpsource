package com.globus.operations.purchasing.orderplanning.forms;

import java.util.ArrayList;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.forms.GmCommonForm;

public class GmOPDemandMapForm extends GmCommonForm
{
    private String demandSheetName = "";
    private String demandSheetTypeId = "";
    private String demandSheetId = "";
    private String demandMapMode = "";

    private String[] checkUnSelectedGS = new String [5];
    private String[] checkSelectedGS = new String [5];
    private String[] checkUnSelectedRI = new String [5];
    private String[] checkSelectedRI = new String [5];
    
    private ArrayList alUnSelectedGS = new ArrayList();
    private ArrayList alSelectedGS = new ArrayList();
    private ArrayList alUnSelectedRI = new ArrayList();
    private ArrayList alSelectedRI = new ArrayList();
    private ArrayList alRegions = new ArrayList();
    private ArrayList alSelectedRegions = new ArrayList(); 
    private ArrayList alDemandSheetType = new ArrayList();
    private String requestPeriod = "2";
    
    /**
     * @return Returns the alGroupList.
     */
   
    /**
     * @return Returns the alDemandSheetType.
     */
    public ArrayList getAlDemandSheetType()
    {
        return alDemandSheetType;
    }
    /**
     * @param alDemandSheetType The alDemandSheetType to set.
     */
    public void setAlDemandSheetType(ArrayList alDemandSheetType)
    {
        this.alDemandSheetType = alDemandSheetType;
    }
    /**
     * @return Returns the alRegions.
     */
    public ArrayList getAlRegions()
    {
        return alRegions;
    }
    /**
     * @param alRegions The alRegions to set.
     */
    public void setAlRegions(ArrayList alRegions)
    {
        this.alRegions = alRegions;
    }
    /**
     * @return Returns the alSelectedGroupList.
     */
   
    /**
     * @return Returns the alSelectedRegions.
     */
    public ArrayList getAlSelectedRegions()
    {
        return alSelectedRegions;
    }
    /**
     * @param alSelectedRegions The alSelectedRegions to set.
     */
    public void setAlSelectedRegions(ArrayList alSelectedRegions)
    {
        this.alSelectedRegions = alSelectedRegions;
    }
    /**
     * @return Returns the demandSheetName.
     */
    public String getDemandSheetName()
    {
        return demandSheetName;
    }
    /**
     * @param demandSheetName The demandSheetName to set.
     */
    public void setDemandSheetName(String demandSheetName)
    {
        this.demandSheetName = demandSheetName;
    }
    /**
     * @return Returns the demandSheetType.
     */
    public String getDemandSheetTypeId()
    {
        return demandSheetTypeId;
    }
    /**
     * @param demandSheetType The demandSheetType to set.
     */
    public void setDemandSheetTypeId(String demandSheetTypeId)
    {
        this.demandSheetTypeId = demandSheetTypeId;
    }
    /**
     * @return Returns the demandMapMode.
     */
    public String getDemandMapMode()
    {
        return demandMapMode;
    }
    /**
     * @param demandMapMode The demandMapMode to set.
     */
    public void setDemandMapMode(String demandMapMode)
    {
        this.demandMapMode = demandMapMode;
    }
    /**
     * @return Returns the demandSheetId.
     */
    public String getDemandSheetId()
    {
        return demandSheetId;
    }
    /**
     * @param demandSheetId The demandSheetId to set.
     */
    public void setDemandSheetId(String demandSheetId)
    {
        this.demandSheetId = demandSheetId;
    }
   
    /**
     * @return Returns the checkSelectedGS.
     */
    public String[] getCheckSelectedGS()
    {
        return checkSelectedGS;
    }
    /**
     * @param checkSelectedGS The checkSelectedGS to set.
     */
    public void setCheckSelectedGS(String[] checkSelectedGS)
    {
        this.checkSelectedGS = checkSelectedGS;
    }
    /**
     * @return Returns the checkUnSelectedGS.
     */
    public String[] getCheckUnSelectedGS()
    {
        return checkUnSelectedGS;
    }
    /**
     * @param checkUnSelectedGS The checkUnSelectedGS to set.
     */
    public void setCheckUnSelectedGS(String[] checkUnSelectedGS)
    {
        this.checkUnSelectedGS = checkUnSelectedGS;
    }
    /**
     * @return Returns the alSelectedGS.
     */
    public ArrayList getAlSelectedGS()
    {
        return alSelectedGS;
    }
    /**
     * @param alSelectedGS The alSelectedGS to set.
     */
    public void setAlSelectedGS(ArrayList alSelectedGS)
    {
        this.alSelectedGS = alSelectedGS;
    }
    /**
     * @return Returns the alSelectedRI.
     */
    public ArrayList getAlSelectedRI()
    {
        return alSelectedRI;
    }
    /**
     * @param alSelectedRI The alSelectedRI to set.
     */
    public void setAlSelectedRI(ArrayList alSelectedRI)
    {
        this.alSelectedRI = alSelectedRI;
    }
    /**
     * @return Returns the alUnSelectedGS.
     */
    public ArrayList getAlUnSelectedGS()
    {
        return alUnSelectedGS;
    }
    /**
     * @param alUnSelectedGS The alUnSelectedGS to set.
     */
    public void setAlUnSelectedGS(ArrayList alUnSelectedGS)
    {
        this.alUnSelectedGS = alUnSelectedGS;
    }
    /**
     * @return Returns the alUnSelectedRI.
     */
    public ArrayList getAlUnSelectedRI()
    {
        return alUnSelectedRI;
    }
    /**
     * @param alUnSelectedRI The alUnSelectedRI to set.
     */
    public void setAlUnSelectedRI(ArrayList alUnSelectedRI)
    {
        this.alUnSelectedRI = alUnSelectedRI;
    }
    /**
     * @return Returns the checkSelectedRI.
     */
    public String[] getCheckSelectedRI()
    {
        return checkSelectedRI;
    }
    /**
     * @param checkSelectedRI The checkSelectedRI to set.
     */
    public void setCheckSelectedRI(String[] checkSelectedRI)
    {
        this.checkSelectedRI = checkSelectedRI;
    }
    /**
     * @return Returns the checkUnSelectedRI.
     */
    public String[] getCheckUnSelectedRI()
    {
        return checkUnSelectedRI;
    }
    /**
     * @param checkUnSelectedRI The checkUnSelectedRI to set.
     */
    public void setCheckUnSelectedRI(String[] checkUnSelectedRI)
    {
        this.checkUnSelectedRI = checkUnSelectedRI;
    }
    
    /**
     * @return Returns the requestPeriod.
     */
	public String getRequestPeriod() {
		return requestPeriod;
	}
	/**
     * @param requestPeriod The requestPeriod to set.
     */
	public void setRequestPeriod(String requestPeriod) {
		this.requestPeriod = requestPeriod;
	}
}
