package com.globus.operations.purchasing.orderplanning.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmFilterConditionBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmOPDemandSetupBean extends GmFilterConditionBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmOPDemandSetupBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmOPDemandSetupBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
  }


  // Initialize
  // the
  // Logger
  // Class.
  GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

  /**
   * loadDemandSheetList - This method will be used to fetch the demand sheet
   * 
   * @param hmParam - to be used later for any filters
   * @return ArrayList - will be used by drop down tag
   * @exception AppError
   */
  public ArrayList loadDemandSheetList(HashMap hmParam) throws AppError {
    ArrayList alDemandSheet = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());
    String strFilterInActive = null;

    if (hmParam != null) {
      strFilterInActive = (String) hmParam.get("FILTER_INACTIVE");
    }

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT C4020_DEMAND_MASTER_ID CODEID, C4020_DEMAND_NM CODENM ");
    sbQuery.append(" FROM T4020_DEMAND_MASTER t4020 ");
    sbQuery.append(" WHERE C4020_VOID_FL IS NULL ");
    sbQuery.append(" AND C901_DEMAND_TYPE != 4000103 ");// Exclude Templates
    sbQuery.append(getListDemandSheetFilter(hmParam));

    if (strFilterInActive == null || ("YES").equalsIgnoreCase(strFilterInActive)) {
      sbQuery.append(" AND C4020_INACTIVE_FL IS NULL ");
    }
    sbQuery.append(" ORDER BY UPPER(C4020_DEMAND_NM) ");


    log.debug(" Query to fetchDemandSheetList is " + sbQuery.toString());

    alDemandSheet = gmDBManager.queryMultipleRecords(sbQuery.toString());


    return alDemandSheet;
  }


  /**
   * saveDemandSheet - This method will be save / update the demand sheet data
   * 
   * @param hmParam - parameters to be saved / updated
   * @exception AppError
   */
  public String saveDemandSheet(HashMap hmParam) throws AppError {
    String strDemandSheetFromDb = "";
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());
    String strInActiveFlag = "";
    String strDemandSheetId = (String) hmParam.get("DEMANDSHEETID");
    String strDemandSheetName = (String) hmParam.get("DEMANDSHEETNAME");
    String strDemandPeriod = (String) hmParam.get("DEMANDPERIOD");
    String strForecastPeriod = (String) hmParam.get("FORECASTPERIOD");
    String strHierarchyId = (String) hmParam.get("HIERARCHYID");
    String strIncludeCurrMonth =
        GmCommonClass.getCheckBoxValue((String) hmParam.get("INCLUDECURRMONTH"));
    String strInActiveFl = GmCommonClass.parseNull((String) hmParam.get("ACTIVEFLAG"));
    String strUserId = (String) hmParam.get("USERID");
    String strDemandSheetOwner = (String) hmParam.get("DEMANDSHEETOWNER");
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));

    if (strInActiveFl.equalsIgnoreCase("on")) {
      strInActiveFlag = "Y";
    } else {
      strInActiveFlag = "";
    }

    gmDBManager.setPrepareString("gm_pkg_oppr_sheet.gm_fc_sav_demsheet", 16);

    gmDBManager.registerOutParameter(16, java.sql.Types.CHAR);

    gmDBManager.setString(1, strDemandSheetId);
    gmDBManager.setString(2, strDemandSheetName);
    gmDBManager.setString(3, strDemandPeriod);
    gmDBManager.setString(4, strForecastPeriod);
    gmDBManager.setString(5, strHierarchyId);
    gmDBManager.setString(6, strIncludeCurrMonth);
    gmDBManager.setString(7, strInActiveFlag);
    gmDBManager.setString(8, strUserId);
    gmDBManager.setString(9, strDemandSheetOwner);
    gmDBManager.setString(10, "");
    gmDBManager.setString(11, "");
    gmDBManager.setString(12, "");
    gmDBManager.setString(13, "");
    gmDBManager.setString(14, "");
    gmDBManager.setString(15, strLogReason);
    gmDBManager.execute();

    strDemandSheetFromDb = gmDBManager.getString(16);
    log.debug("strDemandSheetFromDb  " + strDemandSheetFromDb);
    /*
     * Below section is commented, log is inserted inside the above procedure itself. if
     * (!strLogReason.equals("")) { gmCommonBean.saveLog(gmDBManager, strDemandSheetFromDb,
     * strLogReason, strUserId, "1227"); // 1227 // - // Log // Code // Number // for // Demand //
     * Sheet }
     */
    gmDBManager.commit();

    return strDemandSheetFromDb;
  }

  /**
   * fetchDemandSheetInfo - This method will be fetch demand sheet data for editing
   * 
   * @param strDemandSheetId
   * @return HashMap
   * @exception AppError
   */
  public HashMap fetchDemandSheetInfo(String strDemandSheetId) throws AppError {
    /*
     * HashMap hmReturn = new HashMap();
     * 
     * 
     * GmDBManager gmDBManager = new GmDBManager ();
     * 
     * StringBuffer sbQuery = new StringBuffer(); sbQuery.append(" SELECT C4020_DEMAND_MASTER_ID
     * DEMANDSHEETID , C4020_DEMAND_NM DEMANDSHEETNAME , C901_DEMAND_TYPE DEMANDSHEETTYPE,
     * GET_CODE_NAME(C901_DEMAND_TYPE) DEMANDSHEETTYPENAME "); sbQuery.append(" ,
     * C4020_DEMAND_PERIOD DEMANDPERIOD, C4020_FORECAST_PERIOD FORECASTPERIOD ,
     * DECODE(C4020_INCLUDE_CURRENT_FL, 'Y', 'on', 'off') INCLUDECURRMONTH "); sbQuery.append(" ,
     * DECODE(C4020_INACTIVE_FL,'Y','on','off') ACTIVEFLAG,
     * gm_pkg_oppr_sheet.get_hierarchy_id('60260','"); sbQuery.append(strDemandSheetId);
     * sbQuery.append("') HIERARCHYID "); sbQuery.append(" FROM T4020_DEMAND_MASTER ");
     * sbQuery.append(" WHERE C4020_VOID_FL IS NULL "); sbQuery.append(" AND C4020_DEMAND_MASTER_ID
     * = ? ");
     * 
     * log.debug("Query inside fetchEditDemandSheetInfo is " + sbQuery.toString());
     * 
     * gmDBManager.setPrepareString(sbQuery.toString()); gmDBManager.setString(1,strDemandSheetId);
     * 
     * hmReturn = gmDBManager.querySingleRecord();
     */

    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_oppr_sheet.gm_fc_fch_demsheet_detail", 2);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);

    gmDBManager.setString(1, strDemandSheetId);

    gmDBManager.execute();

    hmReturn =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();

    return hmReturn;

  }

  /**
   * loadDemandGroupList - This method will be used to load the Group list for which parts are
   * associated to
   * 
   * @param hmParam - to be used later for any filters
   * @return ArrayList - will be used by drop down tag
   * @exception AppError
   */
  public ArrayList loadDemandGroupList(HashMap hmParam) throws AppError {
    ArrayList alGroupList = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());

    String strGroupType = GmCommonClass.parseNull((String) hmParam.get("STROPT"));

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT C4010_GROUP_ID CODEID, C4010_GROUP_NM CODENM , C901_GROUP_TYPE PID, c207_set_id SETID  ");
    sbQuery.append(" FROM T4010_GROUP ");
    sbQuery.append(" WHERE C4010_VOID_FL IS NULL ");


    sbQuery.append(" AND C901_TYPE = ? ");
    sbQuery.append(" ORDER BY C4010_GROUP_NM ");

    log.debug("Query inside loadDemandGroupList is " + sbQuery.toString());

    gmDBManager.setPrepareString(sbQuery.toString());
    gmDBManager.setString(1, strGroupType);
    alGroupList = gmDBManager.queryMultipleRecord();
    return alGroupList;
  }

  /**
   * loadPartGroupMapping - This method will be used to parts associated with the selected group
   * 
   * @param strGroupId, strPartNums
   * @return RowSetDynaClass - will be used by displaytag
   * @exception AppError
   */
  public ArrayList loadPartGroupMapping(HashMap hmParam) throws AppError {

    ArrayList alList = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();

    String strAction = GmCommonClass.parseNull((String) hmParam.get("STRACTION"));
    String strGroupId = GmCommonClass.parseNull((String) hmParam.get("STRGROUPID"));
    String strPartNums = GmCommonClass.parseNull((String) hmParam.get("STRPARTINPUT"));
    String strSubComponentFl = GmCommonClass.parseNull((String) hmParam.get("STRSUBCOMPONENTFLAG"));
    String strPricingByGroupFl =
        GmCommonClass.parseNull((String) hmParam.get("STRPRICINGBYGROUPFL"));

    // PMT-758 - When click P icon in Group part pricing screen, then should be display List & TW
    // price in popup screen.
    String strPartPriceVal = GmCommonClass.parseNull((String) hmParam.get("STRPARTPRICEVAL"));
    if (!strAction.equals("save")) {
      sbQuery
          .append(" SELECT 0 GROUPID, '' GROUPNM, C205_PART_NUMBER_ID PNUM , C205_PART_NUM_DESC PDESC, DECODE("
              + "get_part_pricing_type(C205_PART_NUMBER_ID),  ");
      sbQuery
          .append(" 0, 0, 52081) PRICING_TYPE, gm_pkg_pd_group_pricing.get_part_history_flag(C205_PART_NUMBER_ID,"
              + strGroupId
              + ") historyfl, get_part_pricing_type(C205_PART_NUMBER_ID)+1 PNUM_CNT, gm_pkg_pd_group_pricing.get_part_primary_lock_flag(C205_PART_NUMBER_ID) PART_PRIMARY_LOCK, 'N' PRIMARY_LOCK_FL, '0' GROUP_OWNER ");
      if (strPartPriceVal.equalsIgnoreCase("Y")) {
        sbQuery
            .append(", get_part_price(C205_PART_NUMBER_ID,'L') listp , get_part_price(C205_PART_NUMBER_ID, 'E') tw");
      }
      sbQuery.append(" FROM T205_PART_NUMBER ");
      /*sbQuery.append(" WHERE  REGEXP_LIKE(C205_PART_NUMBER_ID ,'");
      sbQuery.append(strPartNums);
      sbQuery.append("')");*/
      
      sbQuery.append(" WHERE  REGEXP_LIKE(C205_PART_NUMBER_ID , REGEXP_REPLACE('");
      sbQuery.append(strPartNums);
      sbQuery.append("','[+]','\\+'))");
      
      if (!strSubComponentFl.equals("Y")) {
        sbQuery.append(" AND C205_SUB_COMPONENT_FL IS NULL ");
      }
      // The popup showing and displaying only Obsolete-20369,Approved-20367 values when click the
      // Pricing By Group Tab in Pricing Request Screen.
      if (strPricingByGroupFl.equals("Y")) {
        sbQuery.append(" AND C901_STATUS_ID IN (20367)");
      }
      sbQuery.append(" MINUS ");

      sbQuery
          .append(" SELECT 0 GROUPID, '' GROUPNM, C205_PART_NUMBER_ID PNUM , GET_PARTNUM_DESC(C205_PART_NUMBER_ID) PDESC,  DECODE("
              + "get_part_pricing_type(C205_PART_NUMBER_ID),  ");
      sbQuery
          .append(" 0, 0, 52081) PRICING_TYPE, gm_pkg_pd_group_pricing.get_part_history_flag(C205_PART_NUMBER_ID,"
              + strGroupId
              + ") historyfl, get_part_pricing_type(C205_PART_NUMBER_ID)+1 PNUM_CNT, gm_pkg_pd_group_pricing.get_part_primary_lock_flag(C205_PART_NUMBER_ID) PART_PRIMARY_LOCK, 'N' PRIMARY_LOCK_FL, '0' GROUP_OWNER  ");
      if (strPartPriceVal.equalsIgnoreCase("Y")) {
        sbQuery
            .append(", get_part_price(C205_PART_NUMBER_ID,'L') listp , get_part_price(C205_PART_NUMBER_ID, 'E') tw");
      }
      sbQuery.append(" FROM T4011_GROUP_DETAIL ");
     /* sbQuery.append(" WHERE  REGEXP_LIKE(C205_PART_NUMBER_ID ,'");
      sbQuery.append(strPartNums);
      sbQuery.append("')");*/
      
      sbQuery.append(" WHERE  REGEXP_LIKE(C205_PART_NUMBER_ID , REGEXP_REPLACE('");
      sbQuery.append(strPartNums);
      sbQuery.append("','[+]','\\+'))");
      
      sbQuery.append(" AND  C4010_GROUP_ID = ");
      sbQuery.append(strGroupId);

      sbQuery.append(" UNION ");
    }

    sbQuery
        .append(" SELECT C4010_GROUP_ID GROUPID, GET_GROUP_NAME(C4010_GROUP_ID) GROUPNM, t4011.C205_PART_NUMBER_ID PNUM , GET_PARTNUM_DESC(t4011.C205_PART_NUMBER_ID) PDESC, C901_PART_PRICING_TYPE PRICING_TYPE , c4011_part_number_historyfl historyfl, get_part_pricing_type(t4011.C205_PART_NUMBER_ID) PNUM_CNT, gm_pkg_pd_group_pricing.get_part_primary_lock_flag(t4011.C205_PART_NUMBER_ID) PART_PRIMARY_LOCK, NVL(c4011_primary_part_lock_fl,'N') PRIMARY_LOCK_FL, gm_pkg_pd_group_pricing.get_group_primary_owner(C4010_GROUP_ID) GROUP_OWNER  ");
    if (strPartPriceVal.equalsIgnoreCase("Y")) {
      sbQuery
          .append(", get_part_price(T205.C205_PART_NUMBER_ID,'L') listp , get_part_price(T205.C205_PART_NUMBER_ID, 'E') tw");
    }
    sbQuery.append(" FROM T4011_GROUP_DETAIL t4011,t205_part_number t205");
    sbQuery.append(" WHERE  C4010_GROUP_ID = ");
    sbQuery.append(strGroupId);
    sbQuery.append(" AND T4011.C205_PART_NUMBER_ID = T205.C205_PART_NUMBER_ID ");

    // The popup showing and displaying only Obsolete-20369,Approved-20367 values when click the
    // Pricing By Group Tab in Pricing Request Screen.
    if (strPricingByGroupFl.equals("Y")) {
      sbQuery.append(" AND C901_STATUS_ID IN (20367)");
    }
    sbQuery.append(" ORDER BY PNUM ");

    log.debug("Query inside loadPartGroupMapping is " + sbQuery.toString());

    alList = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alList;
  }

  /**
   * saveGroupPartMapping - This method will be save / update the parts associated to a group
   * 
   * @param hmParam - parameters to be saved / updated
   * @exception AppError
   */
  public String saveGroupPartMapping(HashMap hmParam) throws AppError {
    String strGroupIdFromDb = "";
    String strExceptionParts = "";
    String strMessage = "";
    String strInActiveFlag = "";
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());
    String strGroupId = GmCommonClass.parseNull((String) hmParam.get("GROUPID"));
    String strGroupName = GmCommonClass.parseNull((String) hmParam.get("GROUPNAME"));
    String strGroupType = GmCommonClass.parseNull((String) hmParam.get("STROPT")); // "40045";
    String strInActiveFl = GmCommonClass.parseNull((String) hmParam.get("HACTIVEFLAG"));

    // This
    // is
    // for
    // Demand
    // Group

    String strInputString = (String) hmParam.get("INPUTSTRING");
    String strUserId = (String) hmParam.get("USERID");
    if (strInActiveFl.equals("true")) {
      strInActiveFlag = "Y";
    } else {
      strInActiveFlag = "";
    }
    String strSystemId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strSystemGrpType = GmCommonClass.parseNull((String) hmParam.get("GROUPTYPE"));
    String strPricedType = GmCommonClass.parseNull((String) hmParam.get("PRICEDTYPE"));
    String strPrimaryPartInputs = (String) hmParam.get("HINPUTPRIMARYPARTS");

    gmDBManager.setPrepareString("gm_pkg_oppr_sheet.gm_fc_sav_grouppartmap", 12);

    gmDBManager.registerOutParameter(11, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(12, java.sql.Types.CHAR);
    log.debug("Before Sending to Database  strGroupId: " + strGroupId);
    gmDBManager.setString(1, strGroupId);
    gmDBManager.setString(2, strGroupName);
    gmDBManager.setString(3, strGroupType);
    gmDBManager.setString(4, strInActiveFlag);
    gmDBManager.setString(5, strInputString);

    gmDBManager.setString(6, strSystemId);
    gmDBManager.setString(7, strSystemGrpType);
    gmDBManager.setString(8, strPrimaryPartInputs);

    gmDBManager.setString(9, strUserId);
    gmDBManager.setString(10, strPricedType);
    gmDBManager.execute();

    strGroupIdFromDb = gmDBManager.getString(11);
    strExceptionParts = GmCommonClass.parseNull(gmDBManager.getString(12));
    log.debug("strExceptionParts.........." + strExceptionParts);
    if (!strExceptionParts.equals("")) {
      strMessage =
          "The parts "
              + strExceptionParts
              + " can not be added to the group, because the part price is different from the group price.";
      throw new AppError(strMessage, "", 'E');
    }
    gmDBManager.commit();
    log.debug("strGroupIdFromDb : " + strGroupIdFromDb);

    return strGroupIdFromDb;
  }

  /**
   * fetchGroupPartInfo - This method will be fetch GroupPartInfo
   * 
   * @param strDemandSheetId
   * @return HashMap
   * @exception AppError
   */
  public HashMap fetchGroupPartInfo(String strGroupId) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());

    try {

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT C4010_GROUP_ID GROUPID , C4010_GROUP_NM GROUPNAME , C207_SET_ID SETID , C901_GROUP_TYPE GROUPTYPE , C901_PRICED_TYPE PRICEDTYPE, c4010_publish_fl PUBLISHFL  ");
      sbQuery.append(" , DECODE(C4010_INACTIVE_FL,'Y','off','on') ACTIVEFLAG ");
      sbQuery.append(" FROM T4010_GROUP ");
      sbQuery.append(" WHERE  C4010_VOID_FL IS NULL ");
      sbQuery.append(" AND C4010_GROUP_ID =  ?  ");

      log.debug("Query inside fetchGroupPartInfo is " + sbQuery.toString());

      gmDBManager.setPrepareString(sbQuery.toString());
      gmDBManager.setString(1, strGroupId);
      hmReturn = gmDBManager.querySingleRecord();
    }

    catch (Exception e) {
      // GmLogError.log("Exception in
      // GmPartNumBean:fetchEditDemandSheetInfo","Exception is:"+e);
      e.printStackTrace();
    }
    return hmReturn;
  }

  /**
   * fetchDemandGroupMap - This method will fetch the Group info for a particular demand sheet
   * 
   * @param hmParam - demandSheetId, Reference Type
   * @exception AppError
   */
  public HashMap fetchDemandGroupMap(String strDemandSheetId, String strDemandSheetTypeId,
      String strRefType) throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alUnselected = new ArrayList();
    ArrayList alSelected = new ArrayList();
    String requestPeriod = "";

    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_oppr_sheet.gm_fc_fch_demandmap", 6);

    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);

    gmDBManager.setString(1, strDemandSheetId);
    gmDBManager.setString(2, strDemandSheetTypeId);
    gmDBManager.setString(3, strRefType);

    gmDBManager.execute();

    alUnselected =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(4)));
    alSelected =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    requestPeriod = GmCommonClass.parseNull(gmDBManager.getString(6));

    gmDBManager.close();

    hmReturn.put("ALSELECTED", alSelected);
    hmReturn.put("ALUNSELECTED", alUnselected);
    hmReturn.put("REQUESTPERIOD", requestPeriod);
    return hmReturn;
  }

  /**
   * saveDemandSheetMapping - This method will save the Groups selected for a specific Demand Sheet
   * 
   * @param hmParam - parameters to be saved / updated
   * @exception AppError
   */
  public void saveDemandSheetMapping(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());
    log.debug(" inside save.... " + hmParam);

    String strDemandSheetId = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETID"));
    String strDemandSheetType = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETTYPEID"));
    String strDemandMapType = GmCommonClass.parseNull((String) hmParam.get("MAPTYPE"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    // String strRequestPeriod = GmCommonClass.parseNull((String) hmParam.get("REQUESTPERIOD"));

    gmDBManager.setPrepareString("gm_pkg_oppr_sheet.gm_fc_sav_demandsheetmap", 4);

    gmDBManager.setString(1, strDemandSheetId);
    gmDBManager.setString(2, strDemandSheetType);
    gmDBManager.setString(3, strDemandMapType);
    gmDBManager.setString(4, strInputString);

    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * loadHierarchyList - This method will be used to fetch the Hierarchy List
   * 
   * @return ArrayList - will be used by drop down tag
   * @exception AppError
   */
  public ArrayList loadHierarchyList() throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_cm_hierarchy.gm_cm_fch_hierarchy_list", 1);

    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(1)));
    gmDBManager.close();
    return alReturn;
  }

  /**
   * fetchPartDrillDown - This method will drill down on the specific part
   * 
   * @return RowSetDynaClass
   * @exception AppError
   */
  public RowSetDynaClass fetchPartDrillDown(HashMap hmParam) throws AppError {
    RowSetDynaClass rdResult = null;
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());

    String strDemandSheetId = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETMONTHID"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBERS"));

    // Below code is commented & changed the key to fix GOP-548 bug.(During Actual drilldown from
    // Demand sheet summary screen,
    // on an individual item under the set detail section it should pull the transactions for proper
    // set consignments).
    // String strGroupInfo = GmCommonClass.parseNull((String) hmParam.get("GROUPINFO"));
    String strGroupInfo = GmCommonClass.parseNull((String) hmParam.get("GROUPID"));

    log.debug(" values in hmParam " + hmParam);
    gmDBManager.setPrepareString("gm_pkg_oppr_sheet_summary.gm_fc_fch_partdrilldown", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);

    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strDemandSheetId);
    gmDBManager.setString(3, strGroupInfo);

    gmDBManager.execute();

    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));
    gmDBManager.close();

    return rdResult;
  }

  /**
   * loadDemandParDetail - This method will fetch the Par detail info
   * 
   * @param strDemandMasterId, strPartInput
   * @exception AppError
   */
  public ArrayList loadDemandParDetail(String strDemandMasterId, String strPartInput)
      throws AppError {

    RowSetDynaClass rdResult = null;
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_oppr_sheet.gm_fch_demand_par_info", 3);

    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strDemandMasterId);
    gmDBManager.setString(2, strPartInput);

    gmDBManager.execute();

    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    return (ArrayList) rdResult.getRows();
  }

  /**
   * saveDemandParDetail - This method will be save / update the par detail
   * 
   * @param inputString, strUserId
   * @exception AppError
   */
  public void saveDemandParDetail(String inputString, String strUserId) throws AppError {
    // String strDemandSheetFromDb = "";
    try {
      GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());

      gmDBManager.setPrepareString("gm_pkg_oppr_sheet.gm_sav_demand_par_info", 2);

      // gmDBManager.registerOutParameter(3,java.sql.Types.CHAR);

      gmDBManager.setString(1, strUserId);
      gmDBManager.setString(2, inputString);

      gmDBManager.execute();

      // strDemandSheetFromDb = gmDBManager.getString(3);
      gmDBManager.commit();
    } catch (Exception exp) {

      exp.printStackTrace();
    }

    // return strDemandSheetFromDb;
  }

  /**
   * saveDemandParDetail - This method will be save / update the par detail
   * 
   * @param inputString, strUserId
   * @exception AppError
   */
  public void saveDemandSheetDetail(String DSId, String strPartNumber, String strParValue)
      throws AppError {
    // String strDemandSheetFromDb = "";
    try {
      GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());

      gmDBManager.setPrepareString("gm_pkg_oppr_sheet.gm_fc_upt_demandsheetpar", 3);

      gmDBManager.setString(1, DSId);
      gmDBManager.setString(2, strPartNumber);
      gmDBManager.setString(3, strParValue);

      gmDBManager.execute();
      gmDBManager.commit();
    } catch (Exception exp) {

      exp.printStackTrace();
    }

    // return strDemandSheetFromDb;
  }

  /**
   * loadPartHistory - This method will load Par Info
   * 
   * @param strDemandMasterId, strPartId
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadDemandsheetPar(String strDemandMasterId, String strPartId) throws AppError {

    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_oppr_sheet.gm_fch_demand_par_history", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    log.debug(" strDemandMasterId in bean is " + strDemandMasterId + " - strPartInput  is "
        + strPartId);
    gmDBManager.setString(1, strDemandMasterId);
    gmDBManager.setString(2, strPartId);
    gmDBManager.execute();

    hmReturn =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(3)));
    gmDBManager.close();
    return hmReturn;

  }

  /**
   * loadPartHistory - This method will load Par History Info
   * 
   * @param strDemandMasterId, strDetailId
   * @return ArrayList
   * @exception AppError
   */

  public ArrayList loadPartHistory(String strDemandMasterId, String strDetailId) throws AppError {

    RowSetDynaClass rdResult = null;
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_cm_audit_trail.gm_fch_parthistory", 3);

    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);

    gmDBManager.setString(1, strDemandMasterId);
    gmDBManager.setString(2, strDetailId);
    gmDBManager.execute();

    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    return (ArrayList) rdResult.getRows();
  }

  /**
   * loadPartHistory - This method will load Set Mapping Info
   * 
   * @param strDemandMasterId
   * @return ArrayList
   * @exception AppError
   */

  public RowSetDynaClass loadSetMapList(String strDemandMasterId) throws AppError {

    RowSetDynaClass rdResult = null;

    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_oppr_sheet.gm_fc_fch_setmap", 2);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strDemandMasterId);
    gmDBManager.execute();

    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return rdResult;
  }

  /**
   * saveDemandParDetail - This method will be save / update the par detail
   * 
   * @param inputString, strUserId
   * @exception AppError
   */
  public void saveSetMap(String inputString) throws AppError {
    // String strDemandSheetFromDb = "";

    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_oppr_sheet.gm_fc_sav_setmap", 1);
    gmDBManager.setString(1, inputString);
    gmDBManager.execute();

    gmDBManager.commit();

  }

  /**
   * loadPartQtyDetails - This Method is used to get crosstab details for Part Qty
   * 
   * @param
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadPartQtyDetails(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmSubReturn = new HashMap();
    HashMap hmParentReturn = new HashMap();
    ArrayList alForecastHeader = new ArrayList();
    HashMap hmPartReturn = new HashMap();

    ResultSet rsSubHeader = null;
    ResultSet rsParentHeader = null;
    ResultSet rsSubDetails = null;
    ResultSet rsParentDetails = null;

    ResultSet rsHeader = null;
    ResultSet rsDetails = null;

    GmCrossTabReport gmCrossReport = new GmCrossTabReport();
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());

    String strDemandSheetId = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETMONTHID"));
    String strPartNums = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBERS"));
    String strForeCastMonths = GmCommonClass.parseNull((String) hmParam.get("FORECASTMONTHS"));
    String strRefId = GmCommonClass.parseNull((String) hmParam.get("REFID"));
    String strMonthId = GmCommonClass.parseNull((String) hmParam.get("MONTHID"));
    String strYearId = GmCommonClass.parseNull((String) hmParam.get("YEARID"));
    String strDate = strMonthId + "/" + strYearId;

    gmDBManager.setPrepareString("gm_pkg_oppr_sheet_summary.gm_fc_fch_allpart_details", 13);
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(9, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(10, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(11, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(12, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(13, OracleTypes.CURSOR);

    gmDBManager.setString(1, strDemandSheetId);
    gmDBManager.setString(2, strPartNums);
    gmDBManager.setString(3, strForeCastMonths);
    gmDBManager.setString(4, strDate);
    gmDBManager.setString(5, strRefId);

    gmDBManager.execute();

    rsSubHeader = (ResultSet) gmDBManager.getObject(6);
    rsParentHeader = (ResultSet) gmDBManager.getObject(7);
    rsSubDetails = (ResultSet) gmDBManager.getObject(8);
    rsParentDetails = (ResultSet) gmDBManager.getObject(9);
    rsHeader = (ResultSet) gmDBManager.getObject(12);
    rsDetails = (ResultSet) gmDBManager.getObject(13);

    hmSubReturn = gmCrossReport.GenerateCrossTabReport(rsSubHeader, rsSubDetails);
    hmParentReturn = gmCrossReport.GenerateCrossTabReport(rsParentHeader, rsParentDetails);
    hmPartReturn = gmCrossReport.GenerateCrossTabReport(rsHeader, rsDetails);

    alForecastHeader = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(10));
    String strPartDesc = gmDBManager.getString(11);

    hmReturn.put("SUBPART", hmSubReturn);
    hmReturn.put("PARENTPART", hmParentReturn);
    hmReturn.put("FORECASTHEAD", alForecastHeader);
    hmReturn.put("PARTDESC", strPartDesc);
    hmReturn.put("PARTDETAILS", hmPartReturn);
    gmDBManager.close();

    return hmReturn;
  }

  /**
   * this method fetches the set map list for launch screen
   * 
   * @param strDemandMasterId
   * @return
   * @throws AppError
   */
  public RowSetDynaClass loadSetMapListforLaunch(String strDemandMasterId) throws AppError {

    RowSetDynaClass rdResult = null;

    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_oppr_sheet.gm_fch_setmap_for_launch", 2);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strDemandMasterId);
    gmDBManager.execute();

    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return rdResult;
  }

  /**
   * this method fetches the set map list for launch screen
   * 
   * @param hmParam
   * @return
   * @throws AppError
   */
  public void saveLaunchDates(HashMap hmParam) throws AppError {
    log.debug(" called");
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());
    String strDemandSheetId = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETID"));
    String strHInputStr = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTR"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    log.debug("strUserId is" + strUserId);
    gmDBManager.setPrepareString("gm_pkg_oppr_sheet.gm_sav_launchdates", 3);

    gmDBManager.setString(1, strDemandSheetId);
    gmDBManager.setString(2, strHInputStr);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
    gmDBManager.close();

  }

  /**
   * this method fetches the launch date
   * 
   * @param hmParam
   * @return
   * @throws AppError
   */
  public Date fetchLaunchDate(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());
    Date launchDate = null;
    String strDemandSheetId = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETID"));
    String strRefType = GmCommonClass.parseNull((String) hmParam.get("REFTYPE"));
    String strRefId = GmCommonClass.parseNull((String) hmParam.get("REFID"));

    gmDBManager.setFunctionString("gm_pkg_oppr_sheet.get_launch_date", 3);
    gmDBManager.registerOutParameter(1, OracleTypes.DATE);
    gmDBManager.setString(2, strDemandSheetId);
    gmDBManager.setString(3, strRefId);
    gmDBManager.setString(4, strRefType);
    gmDBManager.execute();

    if (gmDBManager.getObject(1) != null) {
      launchDate = (Date) gmDBManager.getObject(1);
    }

    log.debug("launchDate " + launchDate);
    gmDBManager.close();

    return launchDate;

  }


  public RowSetDynaClass fetchMultiPartGroup(HashMap hmParam) throws AppError {

    RowSetDynaClass rdResult = null;
    String strPnum = GmCommonClass.parseNull((String) hmParam.get("HPNUM"));

    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_oppr_sheet.gm_fch_multi_part_grp", 2);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPnum);
    gmDBManager.execute();

    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return rdResult;
  }


  public RowSetDynaClass fetchGroupPartHistory(HashMap hmParam) throws AppError {


    RowSetDynaClass rdResult = null;
    String strPnum = GmCommonClass.parseNull((String) hmParam.get("HPNUM"));

    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_oppr_sheet.gm_fch_group_part_history", 2);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPnum);
    gmDBManager.execute();

    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return rdResult;

  }

  public RowSetDynaClass fetchAllGroupPartHistory(HashMap hmParam) throws AppError {


    RowSetDynaClass rdResult = null;
    String strGroupId = GmCommonClass.parseNull((String) hmParam.get("GROUPID"));

    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_oppr_sheet.gm_fch_all_group_part_history", 2);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strGroupId);
    gmDBManager.execute();

    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return rdResult;

  }


  /**
   * loadGroupList - This method will be used to load the Group list
   * 
   * @param hmParam - to be used later for any filters
   * @return ArrayList - will be used by drop down tag
   * @exception AppError
   */
  public ArrayList loadGroupList(HashMap hmParam) throws AppError {
    ArrayList alGroupList = new ArrayList();

    GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean(getGmDataStoreVO());

    alGroupList = gmDemandSetupBean.loadDemandGroupList(hmParam);

    return alGroupList;
  }



}
