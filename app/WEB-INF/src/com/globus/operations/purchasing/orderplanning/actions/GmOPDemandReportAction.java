package com.globus.operations.purchasing.orderplanning.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.operations.forecast.beans.GmDemandReportBean;
import com.globus.operations.forecast.forms.GmDemandSheetReportForm;
import com.globus.operations.purchasing.orderplanning.beans.GmOPDemandReportBean;
import com.globus.operations.purchasing.orderplanning.forms.GmOPTTPMonthlyForm;
import com.globus.common.beans.GmLogger; 
import org.apache.log4j.Logger; 



public class GmOPDemandReportAction extends GmDispatchAction
{
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    
    public ActionForward loadTemplateReport(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws AppError ,Exception
		{   
			GmDemandSheetReportForm gmDemandSheetReportForm = (GmDemandSheetReportForm) form;
			gmDemandSheetReportForm.loadSessionParameters(request);		
			GmCommonClass gmCommonClass = new GmCommonClass();
			HashMap hmParam = new HashMap();		
			hmParam = GmCommonClass.getHashMapFromForm(gmDemandSheetReportForm);       	      
			RowSetDynaClass rdDemandSheetReport = null;
			GmOPDemandReportBean gmOPDemandReportBean = new GmOPDemandReportBean();
			log.debug(" inside loadTemplateReport ");
			ArrayList alTemplates = new ArrayList();
			String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));      
			alTemplates = gmOPDemandReportBean.fetchTemplates();
			if(strOpt.equals("TemplateReport")){
				rdDemandSheetReport = gmOPDemandReportBean.loadTemplateReport(hmParam);
				gmDemandSheetReportForm.setLdResult(rdDemandSheetReport.getRows());
			}
			gmDemandSheetReportForm.setAlTemplates(alTemplates);
			
			return mapping.findForward("gmOPDemandSheetReport");
		}
   
    public ActionForward loadTTPReport(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response)
    {   
        RowSetDynaClass rdTTPReport = null;
        GmOPDemandReportBean gmOPDemandReportBean = new GmOPDemandReportBean();
        
        log.debug(" inside demandTTPReport ");
        try
        {
            rdTTPReport = gmOPDemandReportBean.fetchTTPReport();
            log.debug(" size of result " + rdTTPReport.getRows().size());
            request.setAttribute("results",rdTTPReport);
            
        } catch (Exception e)
        {
            e.printStackTrace();
            log.debug("Exception " + e);
         //   throw new AppError(e);
        }

        return mapping.findForward("success");
    }
    
    public ActionForward fetchTPRReport(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) 
    {    	 
         String strForward="tprreport";
         try
         {
        	 GmOPDemandReportBean gmOPDemandReportBean = new GmOPDemandReportBean();
             GmOPTTPMonthlyForm gmTTPMonthlyForm = (GmOPTTPMonthlyForm) form;         
             gmTTPMonthlyForm.loadSessionParameters(request);
             
             String strPartNum = GmCommonClass.parseNull((String) gmTTPMonthlyForm.getPnum());
     		 String strType  = GmCommonClass.parseNull((String) gmTTPMonthlyForm.getType());
     		 if(strPartNum.indexOf("^") > 0){
     			strType = strPartNum.substring(strPartNum.indexOf("^")+1);
     			strPartNum = strPartNum.substring(0, strPartNum.indexOf("^"));	
     		 }
     		 gmTTPMonthlyForm.setPnum(strPartNum);
     		 gmTTPMonthlyForm.setType(strType);    
     		 
             log.debug(" gmTTPMonthlyForm.getPnum()" + gmTTPMonthlyForm.getPnum()); 
             HashMap hmParam = GmCommonClass.getHashMapFromForm(gmTTPMonthlyForm);             
             
             
        	 HashMap hmReturn =  gmOPDemandReportBean.fetchTPRReport(hmParam);        	 
             gmTTPMonthlyForm.setAlTPRReport((ArrayList)hmReturn.get("TPRREPORT"));
             gmTTPMonthlyForm.setAlTPRVoidReport((ArrayList)hmReturn.get("TPRVOIDREPORT"));
             gmTTPMonthlyForm.setPdesc((String)hmReturn.get("PDESC"));
             gmTTPMonthlyForm.setAlTPRSalesReport((ArrayList)hmReturn.get("TPRSALESREPORT"));
             gmTTPMonthlyForm.setAlTPRBoLoanerReport((ArrayList)hmReturn.get("TPRLOANERBOREPORT"));
             if(gmTTPMonthlyForm.getPnum().equals("")){ //whenever part num is null, need to show set report details.
            	 strForward = "tprsetreport";
             }
             
         } catch (Exception e)
         {
             e.printStackTrace();
             log.debug("Exception " + e);
          //   throw new AppError(e);
         }

         return mapping.findForward(strForward);
    }    
}
