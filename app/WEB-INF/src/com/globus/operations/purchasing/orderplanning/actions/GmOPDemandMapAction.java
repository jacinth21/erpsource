package com.globus.operations.purchasing.orderplanning.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.purchasing.orderplanning.beans.GmOPDemandSetupBean;
import com.globus.operations.purchasing.orderplanning.forms.GmOPDemandMapForm;

public class GmOPDemandMapAction extends GmAction
{

    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
        try {
        GmOPDemandMapForm gmOPDemandMapForm = (GmOPDemandMapForm)form;
        gmOPDemandMapForm.loadSessionParameters(request);
        GmOPDemandSetupBean gmOPDemandSetupBean = new GmOPDemandSetupBean();
        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

        String strOpt = gmOPDemandMapForm.getStrOpt();
        String strDemandSheetId = gmOPDemandMapForm.getDemandSheetId();
        String strDemandSheetTypeId = gmOPDemandMapForm.getDemandSheetTypeId();
        
        if (strDemandSheetTypeId.equals("40020")) // 40020 - Sales 
        {
        	 gmOPDemandMapForm.setRequestPeriod("0");
            
        }
        String strRequestPeriod = gmOPDemandMapForm.getRequestPeriod();
        log.debug("first here is test requestperiod is " + strRequestPeriod  );       
        HashMap hmParam = new HashMap();
        HashMap hmValues = new HashMap();
        HashMap hmGSValues = new HashMap();
        HashMap hmRIValues = new HashMap();
        ArrayList alDemandSheetType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DMDTP"));
        ArrayList alUnSelectedGS = new ArrayList(); 
        ArrayList alSelectedGS = new ArrayList();
        ArrayList alUnSelectedRI = new ArrayList();
        ArrayList alSelectedRI = new ArrayList();
        
        String strGSType = "";
        String strRIType = "";
        String GROUP_DEMAND_MAP_TYPE = "40030";
        String SET_DEMAND_MAP_TYPE = "40031";
        String INH_SET_DEMAND_MAP_TYPE = "4000109";
        String REGION_DEMAND_MAP_TYPE = "40032";
        String INHOUSE_DEMAND_MAP_TYPE = "40033";
        
        if (strDemandSheetTypeId.equals("40020")) // 40020 - Sales 
        {
            strGSType = GROUP_DEMAND_MAP_TYPE;
            strRIType = REGION_DEMAND_MAP_TYPE;
        }
        if (strDemandSheetTypeId.equals("40021")) // 40021 - Consignment 
        {
            strGSType = SET_DEMAND_MAP_TYPE;
            strRIType = REGION_DEMAND_MAP_TYPE;
        }
        if (strDemandSheetTypeId.equals("40022")) // 40022 - InHouse 
        {
            strGSType = INH_SET_DEMAND_MAP_TYPE;
            strRIType = INHOUSE_DEMAND_MAP_TYPE;
        }

        log.debug(" strOpt is " + strOpt + "Demand Sheet type " +strDemandSheetTypeId );

        if (strOpt.equals("save"))
        {
            hmParam = GmCommonClass.getHashMapFromForm(gmOPDemandMapForm);
            String strUnSelectedGS  = GmCommonClass.createInputString(gmOPDemandMapForm.getCheckUnSelectedGS());
            String strSelectedGS = GmCommonClass.createInputString(gmOPDemandMapForm.getCheckSelectedGS());
            String strComma = "";
            
            log.debug(" The values of strUnSelectedGroup "+strUnSelectedGS);
            
            if (strUnSelectedGS.length() > 0 && strSelectedGS.length() > 0 ){
                strComma = ",";
            }
            
            String strGroupInputString = strUnSelectedGS.concat(strComma).concat(strSelectedGS);
            hmParam.put("INPUTSTRING",strGroupInputString);
            hmParam.put("MAPTYPE",strGSType);
            
            log.debug(" Group Input String is  " + strGroupInputString);
            gmOPDemandSetupBean.saveDemandSheetMapping (hmParam);

            String strUnSelectedRI  = GmCommonClass.createInputString(gmOPDemandMapForm.getCheckUnSelectedRI());
            String strSelectedRI = GmCommonClass.createInputString(gmOPDemandMapForm.getCheckSelectedRI());
            strComma = "";
            
            log.debug(" The values of strUnSelectedGroup "+strUnSelectedRI);
            if (strUnSelectedRI.length() > 0 && strSelectedRI.length() > 0 ){
                strComma = ",";
            }
            
            String strRIInputString = strUnSelectedRI.concat(strComma).concat(strSelectedRI);
            hmParam.put("INPUTSTRING",strRIInputString);
            hmParam.put("MAPTYPE",strRIType);
            
            log.debug(" strRIInputString is  " + strGroupInputString);
            gmOPDemandSetupBean.saveDemandSheetMapping (hmParam);

        }
        
        log.debug(" values in gmOPDemandMapForm form --> " + GmCommonClass.getHashMapFromForm(gmOPDemandMapForm));
        if (strDemandSheetTypeId.equals("40020")) // 40020 - Sales 
        {
        	//gmOPDemandMapForm.setRequestPeriod("0");
            hmGSValues = gmOPDemandSetupBean.fetchDemandGroupMap(strDemandSheetId,strDemandSheetTypeId,GROUP_DEMAND_MAP_TYPE);
            hmRIValues = gmOPDemandSetupBean.fetchDemandGroupMap(strDemandSheetId,strDemandSheetTypeId,REGION_DEMAND_MAP_TYPE);
        }
        else if (strDemandSheetTypeId.equals("40021")) // 40021 - Consignment 
        {
            hmGSValues = gmOPDemandSetupBean.fetchDemandGroupMap(strDemandSheetId,strDemandSheetTypeId,SET_DEMAND_MAP_TYPE);
            hmRIValues = gmOPDemandSetupBean.fetchDemandGroupMap(strDemandSheetId,strDemandSheetTypeId,REGION_DEMAND_MAP_TYPE);
        }
        else if (strDemandSheetTypeId.equals("40022")) // 40022 - InHouse 
        {
            hmGSValues = gmOPDemandSetupBean.fetchDemandGroupMap(strDemandSheetId,strDemandSheetTypeId,INH_SET_DEMAND_MAP_TYPE);
            hmRIValues = gmOPDemandSetupBean.fetchDemandGroupMap(strDemandSheetId,strDemandSheetTypeId,INHOUSE_DEMAND_MAP_TYPE);
        }
        
        alUnSelectedGS = GmCommonClass.parseNullArrayList((ArrayList)hmGSValues.get("ALUNSELECTED"));
        alSelectedGS = GmCommonClass.parseNullArrayList((ArrayList)hmGSValues.get("ALSELECTED")); 
        
        alUnSelectedRI = GmCommonClass.parseNullArrayList((ArrayList)hmRIValues.get("ALUNSELECTED"));
        alSelectedRI = GmCommonClass.parseNullArrayList((ArrayList)hmRIValues.get("ALSELECTED")); 
        
        log.debug(" value in hmRIValues " + hmRIValues);
        strRequestPeriod = GmCommonClass.parseNull((String)hmGSValues.get("REQUESTPERIOD"));
        //log.debug(" Count of values in unselected " + alUnSelectedRI.size());
        
        
        gmOPDemandMapForm.setAlUnSelectedGS(alUnSelectedGS);
        gmOPDemandMapForm.setAlSelectedGS(alSelectedGS); 
        gmOPDemandMapForm.setAlUnSelectedRI(alUnSelectedRI);
        gmOPDemandMapForm.setAlSelectedRI(alSelectedRI); 
        
        gmOPDemandMapForm.setAlDemandSheetType(alDemandSheetType);
        
        gmOPDemandMapForm.setRequestPeriod(strRequestPeriod);
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
            throw new AppError(exp);
        }
        return mapping.findForward("success");
    }



}
