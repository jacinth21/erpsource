package com.globus.operations.purchasing.orderplanning.forms;

import java.util.ArrayList;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.forms.GmCommonForm;

public class GmOPTTPMonthlyForm extends GmCommonForm
{

    private String ttpId = "";
    private String levelId = "";
    private String monthId = GmCalenderOperations.getCurrentMM();
    private String yearId = String.valueOf(GmCalenderOperations.getCurrentYear());
    private String inventoryId = "";
    private String finalizeDate = "";
    private String ttpForecastMonths = "4";
    private String ttpName = "";
    private String hCompanyID = ""; 
	private String selectAll = "";
	private String lblInventory = "";
	private String submitSts = "";
	private String hInventoryId = "";
	private String searchttpId="";
	
	
	private String[] checkUnSelectedSheet = new String [20];
    private String[] checkSelectedSheet = new String [20];

    private ArrayList alUnSelectedSheet = new ArrayList();
    private ArrayList alSelectedSheet = new ArrayList();
    private ArrayList alTTPList  = new ArrayList();
    private ArrayList alMonth = new ArrayList();
    private ArrayList alYear  = new ArrayList();
    private ArrayList alInventoryIdList  = new ArrayList();
    private ArrayList alTTPLevel  = new ArrayList();  
    
    private String pnum = "";
    private String pdesc = "";    
    private String monYear = "";
    private String strShowForecastFl="";
    
    private ArrayList alTPRReport = new ArrayList();
    private ArrayList alTPRVoidReport = new ArrayList();
    private ArrayList alTPRSalesReport = new ArrayList();
    private ArrayList alTPRBoLoanerReport = new ArrayList();
    
    private String demandSheetId = "";
    private String demandSheetMonthId = "";
    private String pstatus = "";  //PBO, PBL, PCS
	private String hLvlHierarchy = "";
   
	private String setId = "";
	private String type = "";

    public String getSubmitSts() {
		return submitSts;
	}

	public void setSubmitSts(String submitSts) {
		this.submitSts = submitSts;
	}
   public String gethLvlHierarchy() {
		return hLvlHierarchy;
	}
   
	public void sethLvlHierarchy(String hLvlHierarchy) {
		this.hLvlHierarchy = hLvlHierarchy;
	}
	
    public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	/**
     * @return Returns the alMonth.
     */
    public ArrayList getAlMonth()
    {
        return alMonth;
    }
    /**
     * @param alMonth The alMonth to set.
     */
    public void setAlMonth(ArrayList alMonth)
    {
        this.alMonth = alMonth;
    }
    /**
     * @return Returns the alSelectedSheet.
     */
    public ArrayList getAlSelectedSheet()
    {
        return alSelectedSheet;
    }
    /**
     * @param alSelectedSheet The alSelectedSheet to set.
     */
    public void setAlSelectedSheet(ArrayList alSelectedSheet)
    {
        this.alSelectedSheet = alSelectedSheet;
    }
    /**
     * @return Returns the alTTPList.
     */
    public ArrayList getAlTTPList()
    {
        return alTTPList;
    }
    /**
     * @param alTTPList The alTTPList to set.
     */
    public void setAlTTPList(ArrayList alTTPList)
    {
        this.alTTPList = alTTPList;
    }
    /**
     * @return Returns the alUnSelectedSheet.
     */
    public ArrayList getAlUnSelectedSheet()
    {
        return alUnSelectedSheet;
    }
    /**
     * @param alUnSelectedSheet The alUnSelectedSheet to set.
     */
    public void setAlUnSelectedSheet(ArrayList alUnSelectedSheet)
    {
        this.alUnSelectedSheet = alUnSelectedSheet;
    }
    /**
     * @return Returns the alYear.
     */
    public ArrayList getAlYear()
    {
        return alYear;
    }
    /**
     * @param alYear The alYear to set.
     */
    public void setAlYear(ArrayList alYear)
    {
        this.alYear = alYear;
    }
    /**
     * @return Returns the checkSelectedSheet.
     */
    public String[] getCheckSelectedSheet()
    {
        return checkSelectedSheet;
    }
    /**
     * @param checkSelectedSheet The checkSelectedSheet to set.
     */
    public void setCheckSelectedSheet(String[] checkSelectedSheet)
    {
        this.checkSelectedSheet = checkSelectedSheet;
    }
    /**
     * @return Returns the checkUnSelectedSheet.
     */
    public String[] getCheckUnSelectedSheet()
    {
        return checkUnSelectedSheet;
    }
    /**
     * @param checkUnSelectedSheet The checkUnSelectedSheet to set.
     */
    public void setCheckUnSelectedSheet(String[] checkUnSelectedSheet)
    {
        this.checkUnSelectedSheet = checkUnSelectedSheet;
    }
    /**
     * @return Returns the monthId.
     */
    public String getMonthId()
    {
        return monthId;
    }
    /**
     * @param monthId The monthId to set.
     */
    public void setMonthId(String monthId)
    {
        this.monthId = monthId;
    }
    /**
     * @return Returns the ttpId.
     */
    public String getTtpId()
    {
        return ttpId;
    }
    /**
     * @param ttpId The ttpId to set.
     */
    public void setTtpId(String ttpId)
    {
        this.ttpId = ttpId;
    }
    /**
     * @return Returns the yearId.
     */
    public String getYearId()
    {
        return yearId;
    }
    /**
     * @param yearId The yearId to set.
     */
    public void setYearId(String yearId)
    {
        this.yearId = yearId;
    }
    /**
     * @return Returns the alInventoryIdList.
     */
    public ArrayList getAlInventoryIdList()
    {
        return alInventoryIdList;
    }
    /**
     * @param alInventoryIdList The alInventoryIdList to set.
     */
    public void setAlInventoryIdList(ArrayList alInventoryIdList)
    {
        this.alInventoryIdList = alInventoryIdList;
    }
    /**
     * @return Returns the finalizeDate.
     */
    public String getFinalizeDate()
    {
        return finalizeDate;
    }
    /**
     * @param finalizeDate The finalizeDate to set.
     */
    public void setFinalizeDate(String finalizeDate)
    {
        this.finalizeDate = finalizeDate;
    }
    /**
     * @return Returns the forecastMonths.
     */
    public String getTtpForecastMonths()
    {
        return ttpForecastMonths;
    }
    /**
     * @param forecastMonths The forecastMonths to set.
     */
    public void setTtpForecastMonths(String ttpForecastMonths)
    {
        this.ttpForecastMonths = ttpForecastMonths;
    }
    /**
     * @return Returns the inventoryId.
     */
    public String getInventoryId()
    {
        return inventoryId;
    }
    /**
     * @param inventoryId The inventoryId to set.
     */
    public void setInventoryId(String inventoryId)
    {
        this.inventoryId = inventoryId;
    }
	public String getSelectAll() {
		return selectAll;
	}
	public void setSelectAll(String selectAll) {
		this.selectAll = selectAll;
	}
	public String getPnum() {
		return pnum;
	}
	public void setPnum(String pnum) {
		this.pnum = pnum;
	}
	public String getMonYear() {
		return monYear;
	}
	public void setMonYear(String monYear) {
		this.monYear = monYear;
	}
	public ArrayList getAlTPRReport() {
		return alTPRReport;
	}
	public void setAlTPRReport(ArrayList alTPRReport) {
		this.alTPRReport = alTPRReport;
	}
	public ArrayList getAlTPRVoidReport() {
		return alTPRVoidReport;
	}
	/**
	 * @return the strShowForecastFl
	 */
	public String getStrShowForecastFl() {
		return strShowForecastFl;
	}

	/**
	 * @param strShowForecastFl the strShowForecastFl to set
	 */
	public void setStrShowForecastFl(String strShowForecastFl) {
		this.strShowForecastFl = strShowForecastFl;
	}

	public void setAlTPRVoidReport(ArrayList alTPRVoidReport) {
		this.alTPRVoidReport = alTPRVoidReport;
	}
	public String getPdesc() {
		return pdesc;
	}
	public void setPdesc(String pdesc) {
		this.pdesc = pdesc;
	}
	public String getDemandSheetId() {
		return demandSheetId;
	}
	public void setDemandSheetId(String demandSheetId) {
		this.demandSheetId = demandSheetId;
	}
	public String getDemandSheetMonthId() {
		return demandSheetMonthId;
	}
	public void setDemandSheetMonthId(String demandSheetMonthId) {
		this.demandSheetMonthId = demandSheetMonthId;
	}
	public String getPstatus() {
		return pstatus;
	}
	public void setPstatus(String pstatus) {
		this.pstatus = pstatus;
	}
	public String getSetId() {
		return setId;
	}
	public void setSetId(String setId) {
		this.setId = setId;
	}
	public ArrayList getAlTPRSalesReport() {
		return alTPRSalesReport;
	}
	public void setAlTPRSalesReport(ArrayList alTPRSalesReport) {
		this.alTPRSalesReport = alTPRSalesReport;
	}

	public ArrayList getAlTPRBoLoanerReport() {
		return alTPRBoLoanerReport;
	}
	public void setAlTPRBoLoanerReport(ArrayList alTPRBoLoanerReport) {
		this.alTPRBoLoanerReport = alTPRBoLoanerReport;
	}

	 /**
	 * @return the ttpName
	 */
	public String getTtpName() {
		return ttpName;
	}
	/**
	 * @param ttpName the ttpName to set
	 */
	public void setTtpName(String ttpName) {
		this.ttpName = ttpName;
	}
	/**
	 * @return the hCompanyID
	 */
	public String gethCompanyID() {
		return hCompanyID;
	}
	/**
	 * @param hCompanyID the hCompanyID to set
	 */
	public void sethCompanyID(String hCompanyID) {
		this.hCompanyID = hCompanyID;
	}
	/**
	 * @return the alTTPLevel
	 */
	public ArrayList getAlTTPLevel() {
		return alTTPLevel;
	}
	/**
	 * @param alTTPLevel the alTTPLevel to set
	 */
	public void setAlTTPLevel(ArrayList alTTPLevel) {
		this.alTTPLevel = alTTPLevel;
	}
	/**
	 * @return the levelId
	 */
	public String getLevelId() {
		return levelId;
	}
	/**
	 * @param levelId the levelId to set
	 */
	public void setLevelId(String levelId) {
		this.levelId = levelId;
	}
	/**
	 * @return the lblInventory
	 */
	public String getLblInventory() {
		return lblInventory;
	}
	/**
	 * @param lblInventory the lblInventory to set
	 */
	public void setLblInventory(String lblInventory) {
		this.lblInventory = lblInventory;
	}
	
	public String gethInventoryId() {
		return hInventoryId;
	}

	public void sethInventoryId(String hInventoryId) {
		this.hInventoryId = hInventoryId;
	}
	/**
	 * @return the SearchttpId
	 */
	public String getSearchttpId() {
		return searchttpId;
	}
	/**
	 * @param SearchttpId the SearchttpId to set
	 */
	public void setSearchttpId(String searchttpId) {
		this.searchttpId = searchttpId;
	}
}
