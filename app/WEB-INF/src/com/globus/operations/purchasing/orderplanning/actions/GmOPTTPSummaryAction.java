package com.globus.operations.purchasing.orderplanning.actions;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.operations.purchasing.orderplanning.beans.GmOPDemandReportBean;
import com.globus.operations.purchasing.orderplanning.beans.GmOPForecastTransBean;
import com.globus.operations.purchasing.orderplanning.beans.GmOPTTPSetupBean;
import com.globus.operations.purchasing.orderplanning.forms.GmOPTTPSummaryForm;



public class GmOPTTPSummaryAction extends GmAction
{
	
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
    	
    	//Adding instantiate method to pass the company id in the request. PMT: 14418
    	//Author :gpalani Nov 2017
    	
        instantiate(request, response);
    	
    	GmOPTTPSummaryForm gmOPTTPSummaryForm = (GmOPTTPSummaryForm)form;
		gmOPTTPSummaryForm.loadSessionParameters(request);
		GmOPTTPSetupBean gmOPTTPSetupBean = new GmOPTTPSetupBean();
		GmOPDemandReportBean gmOPDemandReportBean = new GmOPDemandReportBean();
		GmOPForecastTransBean gmOPForecastTransBean = new GmOPForecastTransBean(getGmDataStoreVO());
    	GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    	
		GmCommonBean gmCommonBean = new GmCommonBean();
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
		GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
		   
		String strOpt = gmOPTTPSummaryForm.getStrOpt();
		HashMap hmTemp = new HashMap();
		HashMap hmParam = new HashMap();
		HashMap hmValues = new HashMap();
		HashMap hmFilters = new HashMap();
		
		ArrayList alFilters= new ArrayList();
		
	    hmParam = GmCommonClass.getHashMapFromForm(gmOPTTPSummaryForm);

	    HashMap hmAccess = new HashMap();
	    HashMap hmITGOPAccess = new HashMap();
        String strPartyId = "";
    	String strLockGenAcc = "";
    	String strITGOPFl = "";
    	strPartyId = GmCommonClass.parseNull((String) gmOPTTPSummaryForm.getSessPartyId());
    	hmAccess = GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId, "GOP_TTP_LOCKGEN")));
    	strLockGenAcc = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
    	//PMT-46635 - To get Access for Old Summary Tab
    	hmITGOPAccess = GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId, "IT_GOP_VIEW_ONLY")));
    	strITGOPFl =  GmCommonClass.parseNull((String)hmITGOPAccess.get("UPDFL"));
    	gmOPTTPSummaryForm.setStrLockGenAccess(strLockGenAcc);
    	gmOPTTPSummaryForm.setStrITGOPFl(strITGOPFl);
    	 //PMT-28391To get the forecast show/hide flag from Rules.Rule id - SHOW_FORECAST,Rule Group - TTP_FORECAST
        String strShowForecastFl = GmCommonClass.parseNull(GmCommonClass.getRuleValue("SHOW_FORECAST", "TTP_FORECAST"));
        gmOPTTPSummaryForm.setStrShowForecastFl(strShowForecastFl);
	    /*//lock and generate will call from submit
	    if (strOpt.equals("lock"))
	    {
	    	gmOPTTPSetupBean.lockTTPSummary(hmParam);
	        strOpt = "report";
	    }*/
	    String strUnSelectedDS  = GmCommonClass.createInputString(gmOPTTPSummaryForm.getCheckSelectedSheet());
	    String strSelectedDS = GmCommonClass.createInputString(gmOPTTPSummaryForm.getCheckUnSelectedSheet());
	    String strLevelID=GmCommonClass.parseNull((String)gmOPTTPSummaryForm.getLevelId());
	    String strLevelHierarchy=GmCommonClass.parseNull((String)gmOPTTPSummaryForm.gethLvlHierarchy());
	    String strCodeGrp = "";
	    String strComma = "";
	    if (strUnSelectedDS.length() > 0 && strSelectedDS.length() > 0 ){
	        strComma = ",";
	    }        
	    String strDSInputString = strUnSelectedDS.concat(strComma).concat(strSelectedDS);
	    if(!strDSInputString.equals(""))
	    {
	    	gmOPTTPSummaryForm.setDemandSheetIds(strDSInputString);        
	    	hmParam.put("DEMANDSHEETIDS",strDSInputString);
	    }
	    else
	    {
	    	hmParam.put("DEMANDSHEETIDS",gmOPTTPSummaryForm.getDemandSheetIds());
	    }
	    //...end
	    if(strOpt.equals("save"))
	    {
	    	hmParam.put("ORDSTR", gmOPTTPSummaryForm.getStrPnumOrd());
	    	gmOPForecastTransBean.saveTTPLinkData(hmParam);
	    	strOpt = "report";
	    }
	    if (strOpt.equals("report") || strOpt.equals("reportSummary") || strOpt.equals("reportDetails"))
	    {
	
		    String strDemandSheetIds = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETIDS"));
	    	 if(strDemandSheetIds.trim().equals(""))	 	    	
	     	{
	     	  gmOPTTPSummaryForm.setDemandSheetIds(loadDemandSheetIDS(hmParam));
	     	  hmParam.put("DEMANDSHEETIDS",gmOPTTPSummaryForm.getDemandSheetIds());
	     	}
	    	 
	        // While change the Month/Year - Auto set the Inventory id
	    	 String strInventoryID = GmCommonClass.parseNull((String)hmParam.get("INVENTORYID")); 
		     if(strInventoryID.equals("")){	
		       	strInventoryID = gmOPDemandReportBean.fetchTTPInventoryid(hmParam);
		       	gmOPTTPSummaryForm.setInventoryId(strInventoryID);
		       	hmParam.put("INVENTORYID",strInventoryID);
		     }

	    	 alFilters=GmCommonClass.getCodeList("TTPFTR");
	    	
	    	
	    	 
	    	if(strOpt.equals("report"))
	        {
	        	hmValues = gmOPDemandReportBean.fetchTTPSummarySheets(hmParam);
	        }
	        else
	        {   
	        	// +++++dummy method calls to test the filters from front end++++++++++++++++
	        	// need to be replaced with actual code, 
	        	// first scenario - All Filters Selected  and Level ID & Level Value IS Not NULL 
	        	    hmFilters=getSelectedFiltersFromRequest(request,alFilters);
	        	    hmFilters.put("Unit Cost $","Y"); 
		        	hmParam.put("SELECTEDHEADER",hmFilters); 
		        	gmOPTTPSummaryForm.setQuarAlloc(GmCommonClass.parseNull((String)hmFilters.get("Quar. Alloc")));
		        	gmOPTTPSummaryForm.setQuarAvail(GmCommonClass.parseNull((String)hmFilters.get("Quar. Avail")));
		        	
		        	
		        	hmParam.put("LEVELID",strLevelID);
			        hmParam.put("LEVELHIERARCHY",strLevelHierarchy);
	        	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			   
		        hmValues = gmOPDemandReportBean.fetchTTPSummaryReport(hmParam);
		        gmOPTTPSummaryForm.setAccessType(gmOPDemandReportBean.fetchTTPAccessType(hmParam));
		        gmOPTTPSummaryForm.setTtpType(GmCommonClass.parseNull((String)hmValues.get("TTPTYPE")));
	        }
	        if(strOpt.equals("reportDetails")){
			        ArrayList alDetails = (ArrayList)hmValues.get("Details");
			        
			        if(!gmOPTTPSummaryForm.getOrderQty().equals("") && !gmOPTTPSummaryForm.getQtyOper().equals("0"))
			        {	                	
				        gmSearchCriteria.addSearchCriteria("Order Qty", gmOPTTPSummaryForm.getOrderQty(), gmOPTTPSummaryForm.getQtyOper());	        	      	        
			        }
			        if(!gmOPTTPSummaryForm.getOrderCost().equals("") && !gmOPTTPSummaryForm.getOrdOper().equals("0"))
			        {	        
				        gmSearchCriteria.addSearchCriteria("Order Cost $", gmOPTTPSummaryForm.getOrderCost(), gmOPTTPSummaryForm.getOrdOper());	        	      	        
			        }                
			        if (!gmOPTTPSummaryForm.getPnum().equals(""))
			        {
			        	gmSearchCriteria.addSearchCriteria("Name", gmOPTTPSummaryForm.getPnum(), GmSearchCriteria.LIKE);        	
			        }
			        if (!gmOPTTPSummaryForm.getOrdType().equals(""))
			        {
			        	gmSearchCriteria.addSearchCriteria("ptype", gmOPTTPSummaryForm.getOrdType(), GmSearchCriteria.EQUALS);        	
			        }   
			        if(gmOPTTPSummaryForm.getSummaryTotal().equals("true"))
			        {
			        	gmSearchCriteria.addSearchCriteria("ID", "4050,4051,4052,4053,Total", GmSearchCriteria.IN);
			        }
			        gmSearchCriteria.query(alDetails);  
	        }
	        
	       request.setAttribute("HMVALUES",hmValues);	        
	    } 
	    
	    if (strOpt.equals("report")||strOpt.equals("reportLevel"))
	    {
	    	gmOPTTPSummaryForm.setAlTTPList(gmOPTTPSetupBean.loadTTPList(hmTemp));
	    	//gmOPTTPSummaryForm.setLevelId(gmOPTTPSummaryForm.getLevelId());
	        String strCompanyID = GmCommonClass.parseNull((String)gmOPTTPSummaryForm.gethCompanyID());
        	if (!strCompanyID.equals("")) {
        		gmOPTTPSummaryForm.setAlTTPLevel((ArrayList)gmOPTTPSetupBean.loadTTPLevelList(strCompanyID)); // Should Return alTTPLevel 
        		if(strOpt.equals("reportLevel")){
        		gmOPTTPSummaryForm.setStrOpt("");
        		}
            }
	    }
	    if(strOpt.equals("reportFCRequest"))
	    {    	
	    	gmOPTTPSummaryForm.setAlForeCastReport(gmOPDemandReportBean.fetchTTPForecastRequest(hmParam));
	    	//log.debug("Forecast Request: "+gmOPTTPSummaryForm.getAlForeCastReport());
	    }
	    //log.debug(" hmParam values is " + hmParam);    
	    gmOPTTPSummaryForm.setAlTTPList(gmOPTTPSetupBean.loadTTPList(hmTemp));
	    gmOPTTPSummaryForm.setAlOperators(GmCommonClass.getCodeList("OPERS"));
	    gmOPTTPSummaryForm.setAlOrderTypes(GmCommonClass.getCodeList("PFLY"));        
	    gmOPTTPSummaryForm.setAlMonth(GmCommonClass.parseNullArrayList(gmCommonBean.getMonthList()));
	    gmOPTTPSummaryForm.setAlYear(GmCommonClass.parseNullArrayList(gmCommonBean.getYearList()));    
	    if( !strLevelHierarchy.equals("102580") && !strLevelID.equals("")){//102580 - company or multipart	    	
	    	strCodeGrp = "102700"; 
	    }
	    gmOPTTPSummaryForm.setAlFilters(GmCommonClass.getCodeList("TTPFTR",strCodeGrp)); 
	    //log.debug(" Deman sheet ids --- " + gmOPTTPSummaryForm.getDemandSheetIds());
	    
	    // setting the forecast month values (old summary filter - new filed)
	    if(gmOPTTPSummaryForm.getTempForecastMonths().equals("")){
   		 gmOPTTPSummaryForm.setTempForecastMonths(gmOPTTPSummaryForm.getTtpForecastMonths());
   	 	}
    
	   if(strOpt.equals("fetchFilter") || strOpt.equals("reportFCRequest") || strOpt.equals("reportSummary") || strOpt.equals("reportDetails") )
	    {
	    	return mapping.findForward(strOpt);
	    }
    	
	    
	    return mapping.findForward("success");
}

    private HashMap getSelectedFiltersFromRequest(HttpServletRequest request,ArrayList alFilters)
    {
    	HashMap hmExtraParam = new HashMap();    	
    	
    	Iterator itr=alFilters.iterator();
		 while (itr.hasNext()){
			 HashMap hmValue=(HashMap)itr.next();
			 String  CODEID = (String)hmValue.get("CODEID");
			 String  CODENMALT = (String)hmValue.get("CODENMALT");
			 String strFlag = GmCommonClass.parseNull((String)request.getParameter("chk_"+CODEID));
			 hmExtraParam.put (CODENMALT,strFlag.equals("true") ? "Y" : "N");
		 }
    	
    	log.debug("hmExtraParam" + hmExtraParam);
    	
    	return hmExtraParam;
    	    
    }
    
    private String loadDemandSheetIDS(HashMap hmParam)throws AppError
    {
    	ArrayList alData = new ArrayList();
    	StringBuffer strDemandSheetIDS = new StringBuffer();
    	GmOPForecastTransBean gmOPForecastTransBean = new GmOPForecastTransBean(getGmDataStoreVO());
    	alData=(ArrayList)gmOPForecastTransBean.loadTTPLinkData(hmParam);
    	Iterator itr=alData.iterator();
    	while(itr.hasNext())
    	{
    		HashMap hmValue=(HashMap)itr.next();
    		strDemandSheetIDS.append((String)hmValue.get("ID"));
    		strDemandSheetIDS.append(",");
    	}
    	return strDemandSheetIDS.toString();
    	    
    }
    
}
