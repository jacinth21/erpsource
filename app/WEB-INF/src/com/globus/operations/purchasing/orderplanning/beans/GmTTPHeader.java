package com.globus.operations.purchasing.orderplanning.beans;

public class GmTTPHeader {
	
	public static final String Name = "ID";
	public static final String DESCRIPTION = "Description";
	public static final String UNIT_COST = "Unit Cost $";
	public static final String ID = "Name";
	public static final String LEAD_TIME_ATTRIBUTE = "Lead Time";
	
	public static final String IN_STOCK = "In Stock";
	public static final String PO = "PO";
	public static final String DHR = "DHR";
	public static final String BUILD_SETS = "Built Sets";
	public static final String TOTAL_INVENTORY = "Total Inventory";
	public static final String RW_INVENTORY = "RW Inventory";
	
	public static final String QUAR_AVAIL = "Quar. Avail";
	public static final String QUAR_ALLOC = "Quar. Alloc";
	
	public static final String FORECAST_QTY = "Forecast Qty";
	public static final String FORECAST_QTY_US = "Forecast Qty US";
	public static final String FORECAST_QTY_OUS = "Forecast Qty OUS";
	
	public static final String TPR = "TPR";
	public static final String TPR_US = "TPR US";
	public static final String TPR_OUS = "TPR OUS";
	
	public static final String LTA = "LTA";
	public static final String LTA_US = "LTA US";
	public static final String LTA_OUS = "LTA OUS";
	
	public static final String PARENT_LTA = "Parent LTA";
	public static final String PARENT_LTA_US = "Parent LTA US";
	public static final String PARENT_LTA_OUS = "Parent LTA OUS";
	
	public static final String PARENT_NEED = "Parent Need";
	public static final String PARENT_NEED_US = "Parent Need US";
	public static final String PARENT_NEED_OUS = "Parent Need OUS";

	public static final String OTN = "OTN";	
	public static final String OTN_US = "OTN US";
	public static final String OTN_OUS = "OTN OUS";

	public static final String SET_PAR = "Set PAR";
	public static final String TOTAL_REQUIRED = "Total Required";
	
	public static final String ORDER_QTY = "Order Qty";
	public static final String ORDER_COST = "Order Cost $";
	
	// Column Headers that will not be displayed but used for calculation purpose
	
	public static final String CALC_LEAD_TIME = "Calc Lead Time";
	public static final String CALCULATED_LEAD_TIME = "CALCULATED_LEAD_TIME";
	public static final String PREVIOUS_LEAD_TIME = "PREVIOUS_LEAD_TIME";
	public static final String CALC_PARENT_NEED = "CALC_PARENT_NEED"; 
	public static final String CALC_US_PARENT_NEED = "CALC_US_PARENT_NEED"; 
	public static final String CALC_OUS_PARENT_NEED = "CALC_OUS_PARENT_NEED"; 
	public static final String PREVIOUS_LEVEL = "PLEVEL";
	public static final String SPLIT = "SPLIT";
	
	public static final String PREV_CALC_US_PARENT_NEED = "PREV_CALC_US_PARENT_NEED";
	public static final String PREV_CALC_OUS_PARENT_NEED = "PREV_CALC_OUS_PARENT_NEED";
	public static final String PREV_CALC_PARENT_NEED = "PREV_CALC_PARENT_NEED";
	
	public static final String PREV_CALC_US_PARENT_LTA = "PREV_CALC_US_PARENT_LTA";
	public static final String PREV_CALC_OUS_PARENT_LTA = "PREV_CALC_OUS_PARENT_LTA";
	public static final String PREV_CALC_PARENT_LTA = "PREV_CALC_PARENT_LTA";
	
	public static final String PREVIOUS_FAMILY = "PREVIOUS_FAMILY";
	
	
	// Link Info Keys
	
	public static final String LINK_KEY_IN_STOCK = "In Stock ";
	public static final String LINK_KEY_PO = "Open PO ";
	public static final String LINK_KEY_DHR = "Open DHR ";
	public static final String LINK_KEY_BUILD_SETS = "Build Set";
	public static final String LINK_KEY_TOTAL_INVENTORY = "Total Inventory";
	public static final String LINK_KEY_RW_INVENTORY = "RW Inventory";
	
	public static final String LINK_KEY_QUAR_AVAIL = "Quarantine Allocated (Scrap) ";	
	public static final String LINK_KEY_QUAR_ALLOC = "Quarantine Allocated (Inventory) ";
	
	public static final String LINK_KEY_FORECAST_QTY = "FORECAST_QTY";
	public static final String LINK_KEY_FORECAST_QTY_US = "FORECAST_QTY_US";	
	public static final String LINK_KEY_FORECAST_QTY_OUS = "FORECAST_QTY_OUS";
	
	public static final String LINK_KEY_TPR = "TPR";
	public static final String LINK_KEY_TPR_US = "TPR_US";
	public static final String LINK_KEY_TPR_OUS = "TPR_OUS";
	
	public static final String LINK_KEY_ORDER_QTY = "ORDQTY";
	public static final String LINK_KEY_ORDER_COST = "ORDCOST";
	
	public static final String LINK_KEY_UNIT_COST = "UNIT_PRICE";
	public static final String LINK_KEY_LEAD_TIME = "LEAD_TIME";	
	public static final String LINK_KEY_CALC_LEAD_TIME = "CALC_LEAD_TIME";
	
	public static final String LINK_KEY_OTN = "OTN";
	public static final String LINK_KEY_OTN_US = "OTN US";
	public static final String LINK_KEY_OTN_OUS = "OTN OUS";
	
	public static final String LINK_KEY_SET_PAR = "Set PAR";

	public static final String LINK_KEY_QTY = "QTY";	
	public static final String LINK_KEY_TOTAL_COST = "TOTALCOST";
	
	public static final String LINK_KEY_PARENT_NEED = "PNEED";
	
	// Column Positions
	
	public static final int FORECAST_MONTHS = 4;
	
}
