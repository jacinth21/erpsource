package com.globus.operations.purchasing.orderplanning.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.purchasing.orderplanning.beans.GmGlobalMappingReportBean;
import com.globus.operations.purchasing.orderplanning.forms.GmGlobalMappingReportForm;

public class GmGlobalMappingReportAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                 // Class.

    GmGlobalMappingReportBean gmGlobalMappingReportBean = new GmGlobalMappingReportBean();

    ArrayList alTemp = new ArrayList();
    ArrayList alResult = new ArrayList();
    HashMap hmParam = new HashMap();
    String strXmlString = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));

    GmGlobalMappingReportForm gmGlobalMappingReportForm = (GmGlobalMappingReportForm) form;
    gmGlobalMappingReportForm.loadSessionParameters(request);

    hmParam = GmCommonClass.getHashMapFromForm(gmGlobalMappingReportForm);
    alResult =
        GmCommonClass.parseNullArrayList(gmGlobalMappingReportBean.listGlobalMapping(hmParam));
    strXmlString = generateOutPut(alResult, strSessCompanyLocale);
    gmGlobalMappingReportForm.setXmlString(strXmlString);

    gmGlobalMappingReportForm.setAlCompanyId(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("COMP")));
    gmGlobalMappingReportForm.setAlLevelId(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("GMLVL")));
    gmGlobalMappingReportForm.setAlAccessId(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("GMACTY")));
    gmGlobalMappingReportForm.setAlInventoryId(GmCommonClass
        .parseNullArrayList(gmGlobalMappingReportBean.fetchInventoryLevelList("INVENTORY")));
    gmGlobalMappingReportForm.setAlValueId(GmCommonClass
        .parseNullArrayList(gmGlobalMappingReportBean.fetchInventoryLevelList("LEVELVALUE")));
    return mapping.findForward("GmGlobalMappingRpt");
  }

  private String generateOutPut(ArrayList alResult, String strSessCompanyLocale) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.purchasing.orderplanning.GmGlobalMappingRptt",
        strSessCompanyLocale));
    templateUtil.setTemplateSubDir("operations/purchasing/orderplanning/templates");
    templateUtil.setTemplateName("GmGlobalMappingRpt.vm");
    return templateUtil.generateOutput();
  }
}
