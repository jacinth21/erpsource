package com.globus.operations.purchasing.orderplanning.forms;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmCancelForm;

public class GmOPDemandSheetSetupForm extends GmCancelForm
{
    private String demandSheetId = "";
    private String demandSheetName = "";
    private String demandSheetType = "";
    private String demandSheetTypeName = "";
    private String demandPeriod = "";
    private String forecastPeriod = "";
    private String activeflag = "";
    private String includeCurrMonth = "";
    private String hierarchyId = "";
    private String demandSheetOwner = "";
    
    private String partNumbers = "";
    private String hinputStr = "";
    private String demandMasterId = "";
    private String logId = "";
    private String logType = "";
    private String popType = "";
    private String hpartnumber = "";
    private String hparvalue = "";
    private String requestPeriod = "";
    private String setActionType = "";
    private String hinputSetAction = "";

    private String levelId = "";
    private String levelValue = "";
    private String accessType = "";
    private String accessTypeId = "";
    private String editAccess = "";
    private String templtInactiveFl = "";
    private String updatefl = "";
    private String searchdemandMasterId = "";
    
    private ArrayList alDemandSheetName = new ArrayList();
    private ArrayList alHierarchyList = new ArrayList();
    private ArrayList alDates = new ArrayList();
    
    private ArrayList alDSOwner = new ArrayList();
    private ArrayList alSetActionType = new ArrayList();
    private List ldtResult = new ArrayList();
    
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
     
    /*public void reset(ActionMapping mapping, HttpServletRequest request)
    {
        log.debug(" Inside reset ");
       this.strOpt = "";
       this.demandSheetId = "";
       this.demandSheetName = "";
       this.demandSheetType = "";
       this.demandSheetTypeName = "";
       this.demandPeriod = "";
       this.forecastPeriod = "";
       this.activeflag = "";
       this.includeCurrMonth = "";
       
    }*/
  
    
    
    /**
     * @return Returns the activeflag.
     */
    public String getActiveflag()
    {
        return activeflag;
    }

    /**
	 * @return the templtInactiveFl
	 */
	public String getTempltInactiveFl() {
		return templtInactiveFl;
	}

	/**
	 * @param templtInactiveFl the templtInactiveFl to set
	 */
	public void setTempltInactiveFl(String templtInactiveFl) {
		this.templtInactiveFl = templtInactiveFl;
	}

	/**
	 * @return the editAccess
	 */
	public String getEditAccess() {
		return editAccess;
	}

	/**
	 * @param editAccess the editAccess to set
	 */
	public void setEditAccess(String editAccess) {
		this.editAccess = editAccess;
	}

	/**
	 * @return the levelId
	 */
	public String getLevelId() {
		return levelId;
	}

	/**
	 * @param levelId the levelId to set
	 */
	public void setLevelId(String levelId) {
		this.levelId = levelId;
	}

	/**
	 * @return the levelValue
	 */
	public String getLevelValue() {
		return levelValue;
	}

	/**
	 * @param levelValue the levelValue to set
	 */
	public void setLevelValue(String levelValue) {
		this.levelValue = levelValue;
	}

	/**
	 * @return the accessType
	 */
	public String getAccessType() {
		return accessType;
	}

	/**
	 * @param accessType the accessType to set
	 */
	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}

	/**
	 * @return the accessTypeId
	 */
	public String getAccessTypeId() {
		return accessTypeId;
	}

	/**
	 * @param accessTypeId the accessTypeId to set
	 */
	public void setAccessTypeId(String accessTypeId) {
		this.accessTypeId = accessTypeId;
	}

	/**
     * @param activeflag The activeflag to set.
     */
    public void setActiveflag(String activeflag)
    {
        this.activeflag = activeflag;
    }

    /**
     * @return Returns the alDemandSheetName.
     */
    public ArrayList getAlDemandSheetName()
    {
        return alDemandSheetName;
    }

    /**
     * @param alDemandSheetName The alDemandSheetName to set.
     */
    public void setAlDemandSheetName(ArrayList alDemandSheetName)
    {
        this.alDemandSheetName = alDemandSheetName;
    }
   
    /**
     * @return Returns the demandPeriod.
     */
    public String getDemandPeriod()
    {
        return demandPeriod;
    }

    /**
     * @param demandPeriod The demandPeriod to set.
     */
    public void setDemandPeriod(String demandPeriod)
    {
        this.demandPeriod = demandPeriod;
    }

    /**
     * @return Returns the demandSheetId.
     */
    public String getDemandSheetId()
    {
        return demandSheetId;
    }

    /**
     * @param demandSheetId The demandSheetId to set.
     */
    public void setDemandSheetId(String demandSheetId)
    {
        this.demandSheetId = demandSheetId;
    }

    /**
     * @return Returns the demandSheetName.
     */
    public String getDemandSheetName()
    {
        return demandSheetName;
    }

    /**
     * @param demandSheetName The demandSheetName to set.
     */
    public void setDemandSheetName(String demandSheetName)
    {
        this.demandSheetName = demandSheetName;
    }

    /**
     * @return Returns the demandSheetType.
     */
    public String getDemandSheetType()
    {
        return demandSheetType;
    }

    /**
     * @param demandSheetType The demandSheetType to set.
     */
    public void setDemandSheetType(String demandSheetType)
    {
        this.demandSheetType = demandSheetType;
    }

    /**
     * @return Returns the forecastPeriod.
     */
    public String getForecastPeriod()
    {
        return forecastPeriod;
    }

    /**
     * @param forecastPeriod The forecastPeriod to set.
     */
    public void setForecastPeriod(String forecastPeriod)
    {
        this.forecastPeriod = forecastPeriod;
    }

    /**
     * @return Returns the includeCurrMonth.
     */
    public String getIncludeCurrMonth()
    {
        return includeCurrMonth;
    }

    /**
     * @param includeCurrMonth The includeCurrMonth to set.
     */
    public void setIncludeCurrMonth(String includeCurrMonth)
    {
        this.includeCurrMonth = includeCurrMonth;
    }


    /**
     * @return Returns the demandSheetTypeName.
     */
    public String getDemandSheetTypeName()
    {
        return demandSheetTypeName;
    }


    /**
     * @param demandSheetTypeName The demandSheetTypeName to set.
     */
    public void setDemandSheetTypeName(String demandSheetTypeName)
    {
        this.demandSheetTypeName = demandSheetTypeName;
    }


    /**
     * @return Returns the alHierarchyList.
     */
    public ArrayList getAlHierarchyList()
    {
        return alHierarchyList;
    }


    /**
     * @param alHierarchyList The alHierarchyList to set.
     */
    public void setAlHierarchyList(ArrayList alHierarchyList)
    {
        this.alHierarchyList = alHierarchyList;
    }


    /**
     * @return Returns the hierarchyId.
     */
    public String getHierarchyId()
    {
        return hierarchyId;
    }


    /**
     * @param hierarchyId The hierarchyId to set.
     */
    public void setHierarchyId(String hierarchyId)
    {
        this.hierarchyId = hierarchyId;
    }

    /**
     * @return Returns the alDSOwner
     */
	public ArrayList getAlDSOwner() {
		return alDSOwner;
	}
	
	/**
     * @param alDSOwner The alDSOwner to set.
     */
	public void setAlDSOwner(ArrayList alDSOwner) {
		this.alDSOwner = alDSOwner;
	}

	/**
     * @return Returns the demandSheetOwner
     */
	public String getDemandSheetOwner() {
		return demandSheetOwner;
	}
	
	/**
     * @param demandSheetOwner The demandSheetOwner to set.
     */
	public void setDemandSheetOwner(String demandSheetOwner) {
		this.demandSheetOwner = demandSheetOwner;
	}
	
	/**
     * @return Returns the partNumbers.
     */
    public String getPartNumbers()
    {
        return partNumbers;
    }

    /**
     * @param partNumbers The partNumbers to set.
     */
    public void setPartNumbers(String partNumbers)
    {
        this.partNumbers = partNumbers;
    }

    /**
     * @return Returns the hinputStr.
     */
	public String getHinputStr() {
		return hinputStr;
	}
	
	/**
     * @param hinputStr The hinputStr to set.
     */
	public void setHinputStr(String hinputStr) {
		this.hinputStr = hinputStr;
	}

	/**
     * @return Returns the demandMasterId.
     */
	public String getDemandMasterId() {
		return demandMasterId;
	}

	/**
     * @param demandMasterId The demandMasterId to set.
     */
	public void setDemandMasterId(String demandMasterId) {
		this.demandMasterId = demandMasterId;
	}

	public String getLogId() {
		return logId;
	}

	public void setLogId(String logId) {
		this.logId = logId;
	}

	public String getLogType() {
		return logType;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}

	public String getPopType() {
		return popType;
	}

	public void setPopType(String popType) {
		this.popType = popType;
	}

	public String getHpartnumber() {
		return hpartnumber;
	}

	public void setHpartnumber(String hpartnumber) {
		this.hpartnumber = hpartnumber;
	}

	public String getHparvalue() {
		return hparvalue;
	}

	public void setHparvalue(String hparvalue) {
		this.hparvalue = hparvalue;
	}
	
	 /**
     * @return Returns the requestPeriod.
     */
	public String getRequestPeriod() {
		return requestPeriod;
	}
	/**
     * @param requestPeriod The requestPeriod to set.
     */
	public void setRequestPeriod(String requestPeriod) {
		this.requestPeriod = requestPeriod;
	}

	public String getSetActionType() {
		return setActionType;
	}

	public void setSetActionType(String setActionType) {
		this.setActionType = setActionType;
	}

	public ArrayList getAlSetActionType() {
		return alSetActionType;
	}

	public void setAlSetActionType(ArrayList alSetActionType) {
		this.alSetActionType = alSetActionType;
	}

	public List getLdtResult() {
		return ldtResult;
	}

	public void setLdtResult(List ldtResult) {
		this.ldtResult = ldtResult;
	}

	public String getHinputSetAction() {
		return hinputSetAction;
	}

	public void setHinputSetAction(String hinputSetAction) {
		this.hinputSetAction = hinputSetAction;
	}

	/**
	 * @return the alDates
	 */
	public ArrayList getAlDates() {
		return alDates;
	}

	/**
	 * @param alDates the alDates to set
	 */
	public void setAlDates(ArrayList alDates) {
		this.alDates = alDates;
	}

	/**
	 * @return the updatefl
	 */
	public String getUpdatefl() {
		return updatefl;
	}

	/**
	 * @param updatefl the updatefl to set
	 */
	public void setUpdatefl(String updatefl) {
		this.updatefl = updatefl;
	}

	public String getSearchdemandMasterId() {
		return searchdemandMasterId;
	}

	public void setSearchdemandMasterId(String searchdemandMasterId) {
		this.searchdemandMasterId = searchdemandMasterId;
	}

}
