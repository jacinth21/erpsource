package com.globus.operations.purchasing.orderplanning.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.purchasing.orderplanning.beans.GmOPForecastReportBean;
import com.globus.operations.purchasing.orderplanning.forms.GmOPDemandSheetSummaryForm;

public class GmOPDSGrowthRequestDetailAction extends GmAction {
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    GmOPDemandSheetSummaryForm gmOPDemandSheetSummaryForm = (GmOPDemandSheetSummaryForm) form;
    gmOPDemandSheetSummaryForm.loadSessionParameters(request);
    GmOPForecastReportBean gmOPForecastReportBean = new GmOPForecastReportBean();
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                 // Class.

    String strApplDateFmt = GmCommonClass.parseNull(gmOPDemandSheetSummaryForm.getApplnDateFmt());
    String strDemandSheetMonthId = gmOPDemandSheetSummaryForm.getDemandSheetMonthId();
    String strOpt = GmCommonClass.parseNull(gmOPDemandSheetSummaryForm.getStrOpt());
    String strFindForword = "";
    String strSetID = GmCommonClass.parseNull(gmOPDemandSheetSummaryForm.getSetID());
    String strGrowthDrillDown = "";
    String strXmlData = "";
    String strTemplateName = "";
    strGrowthDrillDown = GmCommonClass.parseNull(gmOPDemandSheetSummaryForm.getGrowthDrillDown());
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    log.debug(" strDemandSheetMonthId ***" + strDemandSheetMonthId);
    log.debug(" strOpt ***" + strOpt);
    HashMap hmParam = new HashMap();
    HashMap hmParams = new HashMap();
    hmParams = GmCommonClass.getHashMapFromForm(gmOPDemandSheetSummaryForm);
    ArrayList alRequestGrowth = new ArrayList();
    ArrayList alGrowthDrillDown = new ArrayList();
    if (strOpt.equals("growthvsrequest")) {
      // strTemplateName = "GmOPDSGrowthRequestDetail.vm";
      alRequestGrowth = gmOPForecastReportBean.reportDSGrowthRequestDetails(hmParams);
      gmOPDemandSheetSummaryForm.setAlForeastGrowth(alRequestGrowth);
      /*
       * hmParam.put("SESSDATEFMT",strApplDateFmt);
       * hmParam.put("ALREQUESTDETAILLIST",alRequestGrowth);
       * hmParam.put("DMDSHTID",strDemandSheetMonthId); hmParam.put("TEMPLATENM",strTemplateName);
       * strXmlData = GmCommonClass.parseNull(getXmlGridData(hmParam));
       * response.setContentType("text/xml;charset=utf-8"); response.setHeader("Cache-Control",
       * "no-cache"); PrintWriter writer = response.getWriter(); writer.println(strXmlData);
       * writer.flush(); writer.close();
       */
      strFindForword = "success";
    } else if (strOpt.equals("growthDrillDown")) {
      strTemplateName = "GmOPDSGrowthDrillDownDetails.vm";
      alGrowthDrillDown = gmOPForecastReportBean.reportDSGrowthRequestDetails(hmParams);
      hmParam.put("ALREQUESTDETAILLIST", alGrowthDrillDown);
      hmParam.put("TEMPLATENM", strTemplateName);
      hmParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
      strXmlData = GmCommonClass.parseNull(getXmlGridData(hmParam));
      strFindForword = "gmGrowthRequestDrillDown";
      gmOPDemandSheetSummaryForm.setSetID(strSetID);
      gmOPDemandSheetSummaryForm.setXmlGridData(strXmlData);
    }
    return mapping.findForward(strFindForword);
  }

  private String getXmlGridData(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    ArrayList alResult = new ArrayList();
    String strTemplateNm = "";
    alResult = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALREQUESTDETAILLIST"));
    strTemplateNm = GmCommonClass.parseNull((String) hmParam.get("TEMPLATENM"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));

    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.purchasing.orderplanning.GmOPGrowthRequestDrillDownDetails",
        strSessCompanyLocale));
    templateUtil.setTemplateSubDir("operations/purchasing/orderplanning/templates");
    templateUtil.setTemplateName(strTemplateNm);
    return templateUtil.generateOutput();
  }
}
