package com.globus.operations.purchasing.orderplanning.displaytag;

import java.util.HashMap;
import org.apache.log4j.Logger;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.crosstab.beans.GmRowHighlightDecorator;

public class GmOPDemandSheetSummaryDecorator extends GmRowHighlightDecorator {

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 


	public String get(String strColumnName) {
		String strReturnValue = "";
		HashMap hmRow = getCurrentRow();
		StringBuffer sbReturn = new StringBuffer();
		String strLinkType="";
		String strDemandMasterID = (String) getAttribute("DMID");
		String strDemandSheetID = (String) getAttribute("DSID");
		String strSheetstatus = (String) getAttribute("SHEETSTATUS");
		String strMonthYear = (String) getAttribute("DEMANDMONTH"); 
		
		if (strColumnName.equals("PAR") && strSheetstatus.equals("50550")) {
			String strID = (String) hmRow.get("ID");			
			String strValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
			if (strValue.equals("")) {
				strReturnValue = "-";
			} else if(getGroupingCount().equals("0")){
				strReturnValue = "<a href= javascript:fnCallEditPAR('" + strID + "','"  + strDemandSheetID + "','"+ strDemandMasterID + "')>" + strValue + "</a>";
			}
			else {
				strReturnValue =   strValue ;
			}
		} 
		else if(strColumnName.equals("PBO")|| strColumnName.equals("PBL")||strColumnName.equals("PCS")){
			
			String strStatus = "";
			String strPnum = (String)hmRow.get("ID");			
			if(strColumnName.equals("PBO")) strStatus = "10";
			if(strColumnName.equals("PBL")) strStatus = "15";
			//For PCS, we should include Ready to Consign and Ready to Ship.
			if(strColumnName.equals("PCS")) strStatus = "20,30";
			String strValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
			if (strValue.equals("")||strValue.equals("0")) {
				strReturnValue = "-";
			} else if(getGroupingCount().equals("0")){
				strReturnValue = "<a href= javascript:fnCallPPP('" + strPnum + "','"  + strStatus + "','"+ strDemandSheetID + "','"+ strDemandMasterID +"')>" + strValue + "</a>";
			}
			else {
				 if(strPnum.indexOf("^") >= 0){ //this is to enable the SET LEVEL Hyperlink.
					 strReturnValue = "<a href= javascript:fnCallPPP('" + strPnum + "','"  + strStatus + "','"+ strDemandSheetID + "','"+ strDemandMasterID +"')>" + strValue + "</a>";
				 }else{
					 strReturnValue = strValue;
				 }
			}
			 
		}
		else if (strColumnName.equals("Name")) {
			String strID = GmCommonClass.parseNull((String) hmRow.get("ID"));	 	
			String strName = GmCommonClass.parseNull((String) hmRow.get("Name"));
			String strGroupCount = GmCommonClass.parseNull((String) hmRow.get("GROUPINGCOUNT"));
			String strSetDtl = GmCommonClass.parseNull((String) hmRow.get("SETDTL"));
			String strDemandType = GmCommonClass.parseNull((String) getAttribute("DEMANDTYPE"));  
			String strForecastMonths = GmCommonClass.parseNull((String) getAttribute("FORECASTMONTHS"));  
			String strValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
			//log.debug(" Values in row " + hmRow);
			String strRowOverRideValue = GmCommonClass.parseNull((String) hmRow.get("ROWOVERRIDEFL"));

			sbReturn.setLength(0);
			if (strDemandType.equals("40020")&& !strGroupCount.equals("0") || ((strDemandType.equals("40021")||strDemandType.equals("40022"))&& strSetDtl.equals("Y"))&& !strGroupCount.equals("0")
					||(strDemandType.equals("40021")||strDemandType.equals("40022"))&& strGroupCount.equals("1"))  
		 	{
		 	}
			else
			{
			if (strRowOverRideValue.equals("Y")) {
				sbReturn.append("<img id='imgEdit' style='cursor:hand' ");
				sbReturn.append("src='/images/icon_History.gif' ");
				sbReturn.append(" width='12' height='13' ");
				sbReturn.append("title='View Comments'");
				sbReturn.append("onClick=\"javascript:fnViewGrowthComments('");
				sbReturn.append(strID);
				sbReturn.append("' , '" + strDemandSheetID + "' , '" + strDemandMasterID + "', '"+ strMonthYear + "')\"/> ");
			}
			else{
                sbReturn.append("<img id='imgEditP' style='cursor:hand' ");
                sbReturn.append("src='/images/consignment.gif' ");
                sbReturn.append("title='View Comments'");
                sbReturn.append("onClick=\"javascript:fnViewGrowthComments('");
                sbReturn.append(strID);
                sbReturn.append("' , '" + strDemandSheetID + "' , '" + strDemandMasterID + "', '"+ strMonthYear + "')\"/> ");
			}
		 	} 
			// Add Icon 'A' 		
			if (strDemandType.equals("40020")&& !strGroupCount.equals("0") ||((strDemandType.equals("40021")||strDemandType.equals("40022"))&& strSetDtl.equals("Y"))&& !strGroupCount.equals("0")
					||(strDemandType.equals("40021")||strDemandType.equals("40022"))&& strGroupCount.equals("1"))   //hide 'A' for (sales) group
		 	{
		 		}
			else
			{
				sbReturn.append("<img id='imgEdit' style='cursor:hand' ");
				sbReturn.append("src='/images/a.gif' ");
				sbReturn.append("title='View Actual Values'");
				if (!strDemandType.equals("40023"))
				{
				sbReturn.append("onClick=\"javascript:fnCallActuals('");
				sbReturn.append(strID);
				sbReturn.append("' , '" + strLinkType + "', '" + strForecastMonths + "' )\"/> ");
				}
				else
				{
					sbReturn.append("onClick=\"javascript:fnCallMultiSheet('");
					sbReturn.append(strID);
					sbReturn.append("' , '" + strDemandSheetID + "' )\"/> ");	
				}
		 	}
			// Add Icon 'P' 		
			if (strDemandType.equals("40020")&& !strGroupCount.equals("0") || ((strDemandType.equals("40021")||strDemandType.equals("40022"))&& strSetDtl.equals("Y"))&& !strGroupCount.equals("0")
					||(strDemandType.equals("40021")||strDemandType.equals("40022"))&& strGroupCount.equals("1"))    
			{
			}
			else
			{
			sbReturn.append("<img id='imgEditP' style='cursor:hand' ");
			sbReturn.append("src='/images/product_icon.jpg'");
			sbReturn.append("title='View Part Detials' ");
			sbReturn.append("onClick=\"javascript:fnCallDisp('");
			sbReturn.append(strID);
			sbReturn.append("' , '" + strLinkType + "' )\"/> ");			
			
			
			String strPSFlag = (String) hmRow.get("PS_FLAG");
			if (strPSFlag != null)
			{
			sbReturn.append("<img id='imgEditP'  width='12' height='12'  style='cursor:hand' ");
			sbReturn.append("src='/images/ParentChild.png'");
			sbReturn.append("title='Sub Part Details' ");
			sbReturn.append("onClick=\"javascript:fnSubComponentDrillDown('");
			sbReturn.append(strID);
			sbReturn.append("' )\"/> ");						
			}
		 	}
			sbReturn.append(strValue);
			strReturnValue = sbReturn.toString();
			// 

		}else if (strColumnName.equals("Demand Weighted Avg")){ 
			String strValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
			String strRefId = GmCommonClass.parseNull((String) hmRow.get("ID"));
			//String strPartId = GmCommonClass.parseNull((String) hmRow.get("ID"));
			String strLevelId = GmCommonClass.parseNull((String) hmRow.get(strColumnName+"LEVEL_ID"));
			String strGroupCount = GmCommonClass.parseNull((String) hmRow.get("GROUPINGCOUNT"));
			String strDemandType = GmCommonClass.parseNull((String) getAttribute("DEMANDTYPE"));
			String strSetDtl = GmCommonClass.parseNull((String) hmRow.get("SETDTL"));
			String strRefType="";
			String strPartIdPre=strRefId;
			String strPartIdSuf=strRefId;
			
			if(strGroupCount.equals("0")){  //Demand Part override
				strRefType = "102960";      //Setting the T4032 Ref type as Part.
				//Taking the Part ID from the Query returned column.
				if(strRefId.indexOf("^") != -1){
					strRefId = strRefId.substring(0, strRefId.indexOf("^"));
				}
			}
			if(strGroupCount.equals("2")){   //Demand Set override
				strRefType = "102961";       //Setting the T4032 Ref type as Set.
				//Taking the Set ID from the Query returned column.
				if(strRefId.indexOf("^") != -1){
					strRefId = strRefId.substring(strRefId.indexOf("^")+1);
				}
			}
			strPartIdPre = strPartIdPre.substring(0,strPartIdPre.indexOf("^")+1);  
			strPartIdSuf = strPartIdSuf.substring(strPartIdSuf.indexOf("^")+1);
			
			//To display the Null,zero values as '-' in UI.
			if (strValue.equals("")||strValue.equals("0")) {
				strValue = "-";
			} 

			//Hyperlink is for Company level and Parts (group count=0), Sets(group count=2). 
			//changes for PC-2339 Weighted Avg Override, open windowopener for stelkast parts
			if((strLevelId.equals("102580"))&&(strGroupCount.equals("0") || strGroupCount.equals("2")) ){
				if( strDemandType.equals("40020")){         //Enabling the hyperlink for sales sheet (Demand Type = 40020).					
					strReturnValue = "<a href= \"javascript:fnOverrideAverage('" + strValue + "','"+ strDemandSheetID + "','"+ strDemandMasterID +"','"+ strRefId + "','"+ strRefType + "')\">" + strValue + "</a>";
				}
				else if(strPartIdSuf.equals("-9999") || strPartIdPre.equals("^")){  //To enable the hyperlink for item consignment, sets.
					//Taking the below values from input part string and do checking.
					//For item consignment input string will be, 124.000^-9999. 
					//For the sets the input string will be, ^924.901.
					strReturnValue = "<a href= \"javascript:fnOverrideAverage('" + strValue + "','"+ strDemandSheetID + "','"+ strDemandMasterID +"','"+ strRefId + "','"+ strRefType + "')\">" + strValue + "</a>";
				}
				else{
					strReturnValue = strValue;	
				}				
			}else{
				strReturnValue = strValue;
			}
		}else {
			strReturnValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
		}
		return strReturnValue;
	}

	/*
	 * To Override the TD Style of the column Name
	 */

	public String getStyle(String strColumnName) {
		HashMap hmRow = getCurrentRow();
		String strStyle = "";
		String strDemandType = (String) getAttribute("DEMANDTYPE");			
		if (strColumnName.equals("Name")) {
			String strID = GmCommonClass.parseNull((String) hmRow.get("CROSSOVERF"));
			if (strID.equals("Y")) {
				strStyle = "YellowShadeTD";
			}
		}
		String strPnum = (String)hmRow.get("ID");
		String strGroupCount = GmCommonClass.parseNull((String) hmRow.get("GROUPINGCOUNT"));
		String strSetDtl = GmCommonClass.parseNull((String) hmRow.get("SETDTL")); 
		
	 	String strComments = GmCommonClass.parseNull((String)hmRow.get(strColumnName+"COMM_FL"));
	 	String strHistoryFl = GmCommonClass.parseNull((String)hmRow.get(strColumnName+"HFL"));
	 	  
		boolean bolComments = false;
		boolean bolHistoryFl = false;
		
		if (strDemandType.equals("40020")&& !strGroupCount.equals("0") ||((strDemandType.equals("40021")||strDemandType.equals("40022"))&& strSetDtl.equals("Y"))&& !strGroupCount.equals("0")
				||(strDemandType.equals("40021")||strDemandType.equals("40022"))&& strGroupCount.equals("1"))   //hide 'A' for (sales) group
	 	{
	 		}
		else
		{
		
			if(!strColumnName.trim().equalsIgnoreCase("Name")){
				if(strComments!=null && strComments.equals("Y")){
					bolComments =true;
					strStyle = "ShadeMedRedGrid";
				}
				
				if(strHistoryFl!=null && strHistoryFl.equals("Y")){
					bolHistoryFl = true;
					strStyle = "ShadeMedYellowGrid";
				}
				if(bolHistoryFl && bolComments ){
					strStyle = "ShadeDarkBrownGrid";
				}
			}
		}
		
		if (strStyle.equals("")) {
			strStyle = super.getStyle(strColumnName);
		}	 
		return strStyle;
	}
}
