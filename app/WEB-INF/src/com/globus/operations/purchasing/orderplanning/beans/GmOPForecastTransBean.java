package com.globus.operations.purchasing.orderplanning.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmOPForecastTransBean extends GmBean
{
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    

	//Adding Default and Parameterized constructor to ensure the company id is passed correctly.
	//Author:gpalani
    public GmOPForecastTransBean() throws AppError 
    {
    	super (GmCommonClass.getDefaultGmDataStoreVO());
    }
    
    public GmOPForecastTransBean(GmDataStoreVO gmDataStoreVO) throws AppError 
    {
    	super (gmDataStoreVO);
    }
    /**
     * loadTTPLinkData - This method will load the TTP / Sheet link data
     * @param hmParam - hmParam
     * @return HashMap
     * @exception AppError
     */
    public ArrayList loadTTPLinkData (HashMap hmParam)throws AppError
    {
        String strTTPId = GmCommonClass.parseNull((String)hmParam.get("TTPID")); 
        String strMonth = GmCommonClass.parseNull((String)hmParam.get("MONTHID"));
        String strYear = GmCommonClass.parseNull((String)hmParam.get("YEARID"));
        String strLevelId = GmCommonClass.parseNull((String)hmParam.get("LEVELID"));
        String strCompanyID = GmCommonClass.parseNull((String)hmParam.get("HCOMPANYID"));
        String strMonYear = strMonth.concat("/").concat(strYear);
        
        HashMap hmReturn = new HashMap();
        ArrayList alTTPLinkData = new ArrayList();
        GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);        
        
        // To fetch header information
        gmDBManager.setPrepareString("gm_pkg_oppr_ttp.gm_fc_fch_ttp_dsmonthly",5);
        gmDBManager.registerOutParameter(5,OracleTypes.CURSOR);
        gmDBManager.setString(1,strTTPId);
        gmDBManager.setString(2,strMonYear);
        gmDBManager.setString(3,strLevelId);
        gmDBManager.setString(4,strCompanyID);
        gmDBManager.execute();
        alTTPLinkData = GmCommonClass.parseNullArrayList((ArrayList)gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(5)));           
        gmDBManager.close();
        
        return alTTPLinkData;
    }
    
    /**
     * saveTTPLinkData - This method will save the TTP / Sheet grouping
     * @param hmParam - hmParam
     * @return void
     * @exception AppError
     */
    public void saveTTPLinkData (HashMap hmParam)throws AppError
    {
        log.debug(" values inside hmParam " + hmParam);
        GmOPTTPLockBean gmOPTTPLockBean = new GmOPTTPLockBean(getGmDataStoreVO());
        GmOPTTPSetupBean gmOPTTPSetupBean = new GmOPTTPSetupBean();
        
    	//Passing the datastorevo to ensure the company id is passed correctly.
    	//Author:gpalani
        GmDBManager gmDBManagerDM = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO() );
        GmDBManager gmDBManagerTTPWeb = new GmDBManager(GmDNSNamesEnum.EXTTIME_WEBGLOBUS, getGmDataStoreVO());
        // PMT-32804 : TTP Summary lock and generate
        // to get the ttp details id
        String strTTPDetailId = "";
        String strTTPStatus = "26240574"; // 26240574 Locked
        
        try {
        ArrayList alRequestDetails = new ArrayList();
        ArrayList alParDetails = new ArrayList();
        HashMap hmOut = gmOPTTPLockBean.saveTTPDetails(hmParam, gmDBManagerDM);
        hmParam.putAll(hmOut);
        strTTPDetailId = GmCommonClass.parseNull((String) hmParam.get("DMTTPDETAILID"));
        alRequestDetails = gmOPTTPLockBean.fetchTTPRequestsDetails(hmParam);
   		hmParam.put("REQUESTDETAILS", alRequestDetails);
        alParDetails = gmOPTTPLockBean.fetchSetParDetails(hmParam);
        hmParam.put("SETPAR", alParDetails);
        log.error("Before call saveTTPDetailsInGlobusApp ==> "+strTTPDetailId);
        gmOPTTPLockBean.saveTTPDetailsInGlobusApp(hmParam, gmDBManagerTTPWeb, gmDBManagerDM );
        gmDBManagerDM.commit();
        gmDBManagerTTPWeb.commit();
        log.error("After call saveTTPDetailsInGlobusApp ==> "+strTTPDetailId);
    	}
    	catch(AppError ex)
    	{
    		gmDBManagerDM.close();
    		gmDBManagerTTPWeb.close();
    		strTTPStatus = "26240575"; // 26240575 - Lock Failed
       		throw new AppError(ex);
    	}finally{
    		 log.debug(" Finally block strTTPDetailId >> "+strTTPDetailId);
    		 hmParam.put("MONTHID", hmParam.get("TTPMONTH"));
    		 hmParam.put("YEARID", hmParam.get("TTPYEAR"));
    		hmParam.put("STATUS", strTTPStatus);
            // to update the TTP details status
            gmOPTTPSetupBean.updateTTPDetailsStatus(hmParam);
    	}
        
        
        
        log.debug(" strTTPDetailId >> "+strTTPDetailId);
    }
 
    /**
     * loadInventoryIDList - This method will load the inventory list
     * @param hmParam - hmParam
     * @return void
     * @exception AppError
     */
    public ArrayList loadInventoryIDList (HashMap hmParam)throws AppError
    {
        ArrayList alInventoryList = new ArrayList();
        GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
        String strTTPID = GmCommonClass.parseNull((String)hmParam.get("TTPID")); 
        String strInvLockType = GmCommonClass.parseNull((String)hmParam.get("INVLOCKTYPE")); 
        String strLevelId = GmCommonClass.parseNull((String)hmParam.get("LEVELID"));
        String strCompanyID = GmCommonClass.parseNull((String)hmParam.get("HCOMPANYID"));
        log.debug(" values inside loadTTPLinkData " + hmParam);
        gmDBManager.setPrepareString("gm_pkg_oppr_ttp.gm_fc_fch_inventoryidlist",5);
        gmDBManager.registerOutParameter(5,OracleTypes.CURSOR);
        gmDBManager.setString(1,strTTPID);
        gmDBManager.setString(2,strInvLockType);
        gmDBManager.setString(3,strLevelId);
        gmDBManager.setString(4,strCompanyID);
        gmDBManager.execute();
        
        alInventoryList =GmCommonClass.parseNullArrayList((ArrayList) gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(5)));
        gmDBManager.close();
        return alInventoryList;
    }
    
    /**
     * getForecastMonthRule - returns the default forecast months from rule table
     */
    public String getForecastMonthRule ()throws AppError
    {
        String strForecastMonth = "4";
        String strRuleId = "FORECASTMONTHS";
        String strRuleGroupId = "ORDERPLANNING";
        GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
        StringBuffer sbQuery = new StringBuffer();
        
        sbQuery.append(" SELECT GET_RULE_VALUE('");
        sbQuery.append(strRuleId);
        sbQuery.append("','");
        sbQuery.append(strRuleGroupId);
        sbQuery.append("') FORECASTMONTH FROM DUAL ");
        
        log.debug("Query for Forecast month rulevalue " + sbQuery.toString());
        strForecastMonth = (String)gmDBManager.querySingleRecord(sbQuery.toString()).get("FORECASTMONTH");
        gmDBManager.close();
        return strForecastMonth;
    
    }
    /**
	 * fetchPPQDetails
	 * 
	 * @param
	 * @return HashMap
	 * @exception AppError
	 */
	public HashMap fetchPPQDetails(HashMap hmParam) throws AppError {
		HashMap hmReturn = new HashMap();
		RowSetDynaClass rdPartDetails = null;
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		String strPartNums = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBERS"));
		String strInventoryLockId = GmCommonClass.parseNull((String) hmParam.get("INVENTORYLOCKID"));
		gmDBManager.setPrepareString("gm_pkg_oppr_ttp_summary.gm_fc_fch_ppqdetails", 4);
		gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);

		gmDBManager.setString(1, strPartNums);
		gmDBManager.setInt(2, Integer.parseInt(strInventoryLockId));
		// log.debug("strPartNums:"+ strPartNums + "--strInventoryLockId : "+strInventoryLockId);

		gmDBManager.execute();
		rdPartDetails = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));
		hmReturn.put("PDESC", gmDBManager.getString(3));
		hmReturn.put("PARENTPARTDETAILS", rdPartDetails.getRows());
		gmDBManager.close();
		return hmReturn;
	}
	
	/**
	 * pushVendorCapacityDtls is used to Populating TTP View by Vendor after locking ttp finalizing
	 * 
	 * @param String strTTPDetailsId, String strUserId
	 * @return 
	 * @exception AppError
	 */
	public void pushVendorCapacityDtls(String strTTPDetailsId,String strUserId) throws AppError {
		
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		gmDBManager.setPrepareString("gm_pkg_oppr_ld_ttp_capa_master_txn.gm_push_vendor_capa_lock_dtls",2);
		gmDBManager.setString(1, strTTPDetailsId);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();	
		gmDBManager.commit();
	}
    
}
