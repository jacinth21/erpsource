package com.globus.operations.purchasing.orderplanning.displaytag;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;

public class DTLeadTimeWrapper extends TableDecorator
{
    private String strLeadTime;
    private String strLeadTimeField;
    private String strParNumber;
    private String strType = "";
    private String strHistory = "";
    private DynaBean db ;
    private int intCount = 0;
    
    public StringBuffer  strValue = new StringBuffer();
    String strImagePath = GmCommonClass.getString("GMIMAGES");
    
    /**
     * Returns the string for text field.
     * @return String
     */
   
    public String getLEADTIME()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strLeadTime = String.valueOf(db.get("LEADTIME"));
        strLeadTimeField = String.valueOf(db.get("LEADTIME_FIELD"));
        strHistory = String.valueOf(db.get("HISTORY_FL"));
        strParNumber   = (String)db.get("PNUM");
        strLeadTime = strLeadTime.equals("null")?"":strLeadTime;
        
        strValue.setLength(0);
        if (strLeadTimeField.equals("Y") )
        {
	        strValue.append("<input style='text-align:center' type = 'text' name = txt_leadtimeval");
	        strValue.append(String.valueOf(intCount) );
	        strValue.append(" size = 5 value='");
	        strValue.append(strLeadTime.trim());
	        strValue.append("' onBlur ='fnLeadTimeValidate(this)");
	        strValue.append("'MAXLENGTH='2'> ");
	        strValue.append("<input type = 'hidden' name = pnum");
	        strValue.append(String.valueOf(intCount) );
	        strValue.append(" value='");
	        strValue.append(strParNumber);
	        strValue.append("'> ");
	        strValue.append("<input type=hidden name ='hLeadTimeVal");
			strValue.append(intCount);
			strValue.append("' value='");
			strValue.append(strLeadTime);
			strValue.append("'> ");
        }
        
        if (strHistory.equals("Y") && strLeadTimeField.equals("Y") && !strLeadTime.equals("") )
        {
        	strValue.append("    <img style='cursor:hand' src='/images/icon_History.gif'" +  
             "border='0'" + 
                 "onClick=\"javascript:fnLeadtimeHistory( '"+ strParNumber +"','" + 1052 + "') \" />" ); 
        }
        
        intCount++;
        return strValue.toString();
        
    }
    
    public String getCOMMENTS()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strParNumber   = (String)db.get("PNUM");
        strType = db.get("COMMENTS").toString();

        if (strType.equals("Y") )
        {
            return "<img id='imgEditA' style='cursor:hand' src='/images/phone-icon_ans.gif'" +  
            " width='20' height='17'" + 
                "onClick=\"javascript:fnLog( '"+4000104+"','" + strParNumber + "') \" />"; 
        }
        else
        {
            return "<img id='imgEditA' style='cursor:hand' src='/images/phone_icon.jpg'" +  
            " width='20' height='17' " + 
                "onClick=\"javascript:fnLog( '"+4000104+"','" + strParNumber + "') \" />";
        }
    }
}


