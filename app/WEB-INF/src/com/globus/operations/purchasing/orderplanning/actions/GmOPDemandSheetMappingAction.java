package com.globus.operations.purchasing.orderplanning.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;

import com.globus.common.beans.GmLogger;
import com.globus.operations.purchasing.orderplanning.beans.GmOPForecastReportBean;
import com.globus.operations.purchasing.orderplanning.forms.GmOPDemandSheetSummaryForm;

public class GmOPDemandSheetMappingAction extends GmAction
{
    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
        try {
            
        	GmOPDemandSheetSummaryForm gmOPDemandSheetSummaryForm = (GmOPDemandSheetSummaryForm)form;
            GmOPForecastReportBean gmOPForecastReportBean = new GmOPForecastReportBean();
            Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
            ArrayList alRegionList = new ArrayList();            
            HashMap hmDemandSheet = new HashMap();
            String strDemandSheetMonthId = gmOPDemandSheetSummaryForm.getDemandSheetMonthId();            
            HashMap hmParam = GmCommonClass.getHashMapFromForm(gmOPDemandSheetSummaryForm);
            alRegionList = gmOPForecastReportBean.fetchDemandSheetMapping(strDemandSheetMonthId);
            gmOPDemandSheetSummaryForm.setAlRegionList(alRegionList);            
            hmDemandSheet = gmOPForecastReportBean.fetchMonthlyDemandSheetInfo(hmParam);
            gmOPDemandSheetSummaryForm = (GmOPDemandSheetSummaryForm)GmCommonClass.getFormFromHashMap(gmOPDemandSheetSummaryForm,hmDemandSheet);  
            
            log.debug("HmParam in Action : "+hmParam);
        }
        catch(Exception exp)
        {            
            exp.printStackTrace();
            throw new AppError(exp);
        }
        return mapping.findForward("success");
    }

}
