package com.globus.operations.purchasing.orderplanning.displaytag;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTOPTPRWrapper extends TableDecorator
{ 
    
    GmLogger log = GmLogger.getInstance();

    public String getREQID()
    {
    	 DynaBean db ;
    	StringBuffer strValue = new StringBuffer(); 
    	db =    (DynaBean) this.getCurrentRowObject();
    	String strReqId = (String)db.get("REQID");
    	 strValue.setLength(0); 
 		
 		strValue.append("  <a href= javascript:fnViewDetails('" +strReqId +"')>" + strReqId + "</a>") ;
 	//	strValue.append(strReqId) ;
 				
 		//log.debug("String value which will be returned is " + strValue.toString());
 		return strValue.toString();
    }
    
    public String getCONS_ID()
    {
    	 DynaBean db ;
    	 db =    (DynaBean) this.getCurrentRowObject();
    	StringBuffer strValue = new StringBuffer(); 
	 	String strConId =  GmCommonClass.parseNull(((String)db.get("CONS_ID")));
	 	if(strConId.equals(""))
	 		return strConId;
	 		
	 	strValue.setLength(0);
		strValue.append("  <a href= javascript:fnConsgPrint('" + strConId +"')>" + strConId + "</a>") ;
	//	 log.debug("String vGalue which will be returned is " + strValue.toString());
	 	return strValue.toString();
		
    } 
    public String getINHOUSE_TRANS_ID(){
    
		 DynaBean db ;
		 db =    (DynaBean) this.getCurrentRowObject();
		StringBuffer strValue = new StringBuffer(); 
		String strInhouseTransID =  GmCommonClass.parseNull(((String)db.get("INHOUSE_TRANS_ID")));
		if(strInhouseTransID.equals(""))
			return strInhouseTransID;
			
		strValue.setLength(0);
		strValue.append("  <a href= javascript:fnViewInhouseDetails('" + strInhouseTransID +"')>" + strInhouseTransID + "</a>") ;
		return strValue.toString();

    }
    public String getORDID()
    { 	 
    	 DynaBean db ;
    	 db =    (DynaBean) this.getCurrentRowObject();
    	StringBuffer strValue = new StringBuffer(); 
	 	String strOrdId =  GmCommonClass.parseNull(((String)db.get("ORDID")));
	 	if(strOrdId.equals(""))
	 		return strOrdId;
	 		
	 	strValue.setLength(0);
		strValue.append("  <a href= javascript:fnViewOrder('" + strOrdId +"')>" + strOrdId + "</a>") ;	
	 	return strValue.toString();
		
    } 
    
    
    
}
