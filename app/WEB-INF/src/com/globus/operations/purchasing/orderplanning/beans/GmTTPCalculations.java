package com.globus.operations.purchasing.orderplanning.beans;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;

public class GmTTPCalculations {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	
	String strLevelHierarchy = "0";
	String strLevelID = "0";
	String strTTPColumnType = "";
	
	boolean bMonYear = false;
	private final int idefaultWeeks = 7; // need to take this value from rule table

	double iTotalParentLTA = 0;
	double iSubTotalTotalParentLTA = 0;
	
	double iTotalLTA =0 ;
	double iSubTotalTotalLTA = 0;
	
	double iTotalLTAUS =0 ;
	double iSubTotalTotalLTAUS = 0;
	
	double iTotalLTAOUS =0 ;
	double iSubTotalTotalLTAOUS = 0;
	
	double iTotalParentLTAUS =0 ;
	double iSubTotalTotalParentLTAUS = 0;
	
	double iTotalParentLTAOUS =0 ;
	double iSubTotalTotalParentLTAOUS = 0;
	
	double iTotalParentNeed =0 ;
	double iSubTotalTotalParentNeed = 0;
	
	double iTotalOrderQty =0 ;
	double iSubTotalTotalOrderQty = 0;
	
	double iTotalOrderCost =0 ;
	double iSubTotalTotalOrderCost = 0;
	
	double iTotalUnitCost =0 ;
	double iSubTotalTotalUnitCost = 0;
	
	HashMap hmTTPFooter = new HashMap();
	ArrayList alEmptyHeaders = new ArrayList();
	
	/**
     * setTTPLevelHierarchy - Value Object method exposed to other beans
     *              
     * @param String strLevelHierarchy
     * @author vprasath 
     */
	
	
	public void setTTPLevelHierarchy(String strLevelHierarchy)
	{
		this.strLevelHierarchy = GmCommonClass.parseZero(strLevelHierarchy).trim();		
		//log.debug("strLevelHierarchy: " + this.strLevelHierarchy);
	}
	
	/**
     * setTTPLevelID - Value Object method exposed to other beans
     *              
     * @param String strLevelID
     * @author vprasath 
     */
	public void setTTPLevelID(String strLevelID)
	{
		this.strLevelID = GmCommonClass.parseZero(strLevelID).trim();
		//log.debug("strLevelID: " + this.strLevelID);
	}
	
	/**
     * getTTPColumnType - Value Object method exposed to other beans
     *              
     * @param 
     * @author vprasath 
     */
	public String getTTPColumnType()
	{
		return strTTPColumnType; 
	}
	
	/**
     * isMultiPartTTP - Check and return if the selcted sheet is multipart or not
     *              
     * @param 
     * @author vprasath 
     */
	private boolean isMultiPartTTP()
	{
		String strHierarchy =  this.strLevelHierarchy;
		String strLevelID =  this.strLevelID;
		return strHierarchy.equals("") && strLevelID.equals("");
	}
	
	/**
     * loadCalculatedValues - This is the parent method that is exposed to the other beans to call.
     * This method does the following, 
     * 
     *   1. Add the calculated columns to the alHeader
     *   2. Load the Parent LTA Columns
     *   3. Load the Lead Time Qty Columns
     *   4. Calculate the Total for the columns and load it 
     *   5. Remove the prefix for the forecast months columns
     *   6. Remove the columns with the 0 as the total
     *              
     * @param HashMap hmValue, ArrayList alParentPartDetails, int num_foreCastMonth, String monYear 
     * @author vprasath 
     */
	
	public void loadCalculatedValues(HashMap hmValue, ArrayList alParentPartDetails, int num_foreCastMonth, String monYear) throws AppError
	{
		ArrayList alTTPHeader = (ArrayList)hmValue.get("Header");
		ArrayList alTTPDetails = (ArrayList)hmValue.get("Details");
		hmTTPFooter = (HashMap)hmValue.get("Footer");
		Iterator itrTTPDetails = alTTPDetails.iterator();
		
		int headerIndex = alTTPHeader.indexOf(GmTTPHeader.FORECAST_QTY_OUS);
		alTTPHeader.add(++headerIndex,GmTTPHeader.LTA);
		alTTPHeader.add(++headerIndex,GmTTPHeader.LTA_US);
		alTTPHeader.add(++headerIndex,GmTTPHeader.LTA_OUS);
		
		alTTPHeader.add(++headerIndex,GmTTPHeader.PARENT_LTA);
		alTTPHeader.add(++headerIndex,GmTTPHeader.PARENT_LTA_US);
		alTTPHeader.add(++headerIndex,GmTTPHeader.PARENT_LTA_OUS);
			
		alTTPHeader.add(++headerIndex,GmTTPHeader.PARENT_NEED);
		alTTPHeader.add(++headerIndex,GmTTPHeader.PARENT_NEED_US);
		alTTPHeader.add(++headerIndex,GmTTPHeader.PARENT_NEED_OUS);
		
		headerIndex = alTTPHeader.indexOf(GmTTPHeader.ORDER_QTY);
		alTTPHeader.add(headerIndex++, GmTTPHeader.TOTAL_REQUIRED);
		

		loadParentPartLTA(alTTPDetails, alParentPartDetails, num_foreCastMonth, monYear  );	

		

		while(itrTTPDetails.hasNext())
		{
		   HashMap hmRow = (HashMap)itrTTPDetails.next();
		   loadLeadTimeAdj(alTTPHeader, hmRow,num_foreCastMonth,monYear);
		   loadOrderQtyAndCost(hmRow);
		   calculateTotal(hmRow);
		   removePrefixfromForeCastMonth(hmRow);
		}
		removeColumnsBasedonFooterValue(hmTTPFooter, alTTPHeader);

	}
	
	/**
     * removeColumnsBasedonFooterValue - If a column is there with all the values as 0 then that column
     * should not be displayed, so take the footer hashmap and check if any column has a zero value
     * if any column is 0 then check in alHeader that this column is existing, if it is existing remove 
     * the column from there.
     *              
     * @param HashMap hmTTPFooter, ArrayList alTTPHeader
     * @author vprasath 
     */
	private void removeColumnsBasedonFooterValue(HashMap hmTTPFooter, ArrayList alTTPHeader) throws AppError
	{
		HashMap hmDefaultHeader = new HashMap();
		ArrayList alOptionalHeader = new ArrayList();
		
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		gmDBManager.setPrepareString("gm_pkg_oppr_ttp_summary.gm_fch_ttp_header", 4);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.setString(1,"");
		gmDBManager.setString(2,"OPTIONALCOL");
		gmDBManager.setString(3,"");
		gmDBManager.execute(); 
		hmDefaultHeader = GmCommonClass.parseNullHashMap(gmDBManager.returnConvertedHashMap((ResultSet) gmDBManager.getObject(4)));
		gmDBManager.close();
		alOptionalHeader.addAll(hmDefaultHeader.keySet());
		Iterator itrOptionalHeader = alOptionalHeader.iterator();
		
		while(itrOptionalHeader.hasNext())
		{
			String strHeader =  (String)itrOptionalHeader.next();
			double dFooterVal = getDoubleValueFromHashMap(hmTTPFooter, strHeader);
			if(dFooterVal == 0)
			{
				alTTPHeader.remove(strHeader);
			}
		}
	}
	
	/**
     * loadOrderQtyAndCost -  hmRow will have the part details , take the details from there
     * and apply the calculation for that part and store the lead time back in the same hashmap
     * 
     * Formula is explained with examples in
     * http://confluence.spineit.net/display/PRJ/GOP_R303F+TTP+Summary+-+Calculations
     *              
     * @param HashMap hmRow
     * @author vprasath 
     */
	private void loadOrderQtyAndCost(HashMap hmRow)
	{
		String strPartNum =  GmCommonClass.parseNull((String)hmRow.get("ID"));
		double iParentNeed =  getDoubleValueFromHashMap(hmRow,GmTTPHeader.PARENT_NEED);
		double iTPR =  getDoubleValueFromHashMap(hmRow,GmTTPHeader.TPR);
		double iForeCastQty =  getDoubleValueFromHashMap(hmRow,GmTTPHeader.FORECAST_QTY);
		double iLeadTimeAdj =  getDoubleValueFromHashMap(hmRow,GmTTPHeader.LTA);
		double iOneTimeNeed =  getDoubleValueFromHashMap(hmRow,GmTTPHeader.OTN);
		double iSafetyStock =  getDoubleValueFromHashMap(hmRow,GmTTPHeader.SET_PAR);
		
		double totalRequired = Math.max(0, iParentNeed + iTPR + iForeCastQty + iLeadTimeAdj + iOneTimeNeed + iSafetyStock);
				
		hmRow.put(GmTTPHeader.TOTAL_REQUIRED,""+ Math.round(totalRequired));
		
		double iUSParentNeed =  getDoubleValueFromHashMap(hmRow,GmTTPHeader.PARENT_NEED_US);
		double iUSTPR =  getDoubleValueFromHashMap(hmRow,GmTTPHeader.TPR_US);
		double iUSForeCastQty =  getDoubleValueFromHashMap(hmRow,GmTTPHeader.FORECAST_QTY_US);
		double iUSLeadTimeAdj =  getDoubleValueFromHashMap(hmRow,GmTTPHeader.LTA_US);
		double iUSOneTimeNeed =  getDoubleValueFromHashMap(hmRow,GmTTPHeader.OTN_US);
		double iUSSafetyStock =  getDoubleValueFromHashMap(hmRow,GmTTPHeader.SET_PAR);
		double iTotalInv =  getDoubleValueFromHashMap(hmRow,GmTTPHeader.TOTAL_INVENTORY);
		
		double totalReqUS = Math.max(0, iUSParentNeed + iUSTPR + iUSForeCastQty + iUSLeadTimeAdj + iUSOneTimeNeed + iUSSafetyStock);
		
		double iOUSParentNeed =  getDoubleValueFromHashMap(hmRow,GmTTPHeader.PARENT_NEED_OUS);
		double iOUSTPR =  getDoubleValueFromHashMap(hmRow,GmTTPHeader.TPR_OUS);
		double iOUSForeCastQty =  getDoubleValueFromHashMap(hmRow,GmTTPHeader.FORECAST_QTY_OUS);
		double iOUSLeadTimeAdj =  getDoubleValueFromHashMap(hmRow,GmTTPHeader.LTA_OUS);
		double iOUSOneTimeNeed =  getDoubleValueFromHashMap(hmRow,GmTTPHeader.OTN_OUS);
		double iRWInv =  getDoubleValueFromHashMap(hmRow,GmTTPHeader.RW_INVENTORY);
		
		double totalReqOUS = Math.max(0, Math.max(0,(iOUSParentNeed + iOUSTPR + iOUSForeCastQty + iOUSLeadTimeAdj + iOUSOneTimeNeed)) - iRWInv );
		
		double totalOrderQty = Math.max(0, ((totalReqUS + totalReqOUS) - iTotalInv));
		
		hmRow.put(GmTTPHeader.ORDER_QTY, ""+ Math.round(totalOrderQty));
		double dUnitCost = getDoubleValueFromHashMap(hmRow,GmTTPHeader.UNIT_COST);
		hmRow.put(GmTTPHeader.ORDER_COST, ""+ Math.round(totalOrderQty) * dUnitCost);
		
	}
	
	/**
     * removePrefixfromForeCastMonth -  Forecast Months has FC added infront of it, 
     * it shouldn't be display like that, so remove FC- from the column prefix.
     * 
     * @param HashMap hmRow
     * @author vprasath 
     */	
	
	private void removePrefixfromForeCastMonth(HashMap hmRow)
	{
		String strPartNum =  GmCommonClass.parseNull((String)hmRow.get("ID"));
		
	   Iterator itr = hmRow.keySet().iterator();
	   HashMap hmTempRow = new HashMap();
	   while(itr.hasNext())
	   {
		  String strColumnName = GmCommonClass.parseNull((String)itr.next());
		 
		  if(strColumnName.startsWith("FC-"))
		  {
			  String strValue = GmCommonClass.parseNull((String)hmRow.get(strColumnName));
			  strColumnName = strColumnName.replace("FC-", "");
			  hmTempRow.put(strColumnName, strValue);
		  }
	   }
	   	   
	   hmRow.putAll(hmTempRow);
   }

	/**
     * calculateTotal -  Total for all the newly added columns will be added using the method,
     * 2 different totals need to be calculated
     * 
     * 1. Sub Total
     * 2. Total
     * 
     * Take the GROUPING ID from the hmRow,
     * 
     *  1. if it is "0" then it is a row that represents the part details, so add the value in the hashmap to the subtotal and total
     *  2. if it is "3" then it is a row that represents the total, so put the calulated value for total in to this hashmap
     *  3. if it is "2" then it is a row that represents the sub total, so put the calulated value for sub total in to this hashmap and reset the variables to 0.

     * @param HashMap hmRow
     * @author vprasath 
     */
	
	private void calculateTotal(HashMap hmRow)
	{
		int iGroupingCount =  getIntegerValueFromHashMap(hmRow, "GROUPINGCOUNT");

		
		if(iGroupingCount == 0)
		{
			// LTA
			
			iTotalLTA += getDoubleValueFromHashMap(hmRow, GmTTPHeader.LTA);	
			iSubTotalTotalLTA += getDoubleValueFromHashMap(hmRow, GmTTPHeader.LTA);
			
			// LTA US
			
			iTotalLTAUS += getDoubleValueFromHashMap(hmRow, GmTTPHeader.LTA_US);	
			iSubTotalTotalLTAUS += getDoubleValueFromHashMap(hmRow, GmTTPHeader.LTA_US);
			
			// LTA OUS
			
			iTotalLTAOUS += getDoubleValueFromHashMap(hmRow, GmTTPHeader.LTA_OUS);	
			iSubTotalTotalLTAOUS += getDoubleValueFromHashMap(hmRow, GmTTPHeader.LTA_OUS);
			
			// Parent LTA
			
			iTotalParentLTA += getDoubleValueFromHashMap(hmRow, GmTTPHeader.PARENT_LTA);	
			iSubTotalTotalParentLTA += getDoubleValueFromHashMap(hmRow, GmTTPHeader.PARENT_LTA);
			
			// Parent LTA US
			
			iTotalParentLTAUS += getDoubleValueFromHashMap(hmRow, GmTTPHeader.PARENT_LTA_US);	
			iSubTotalTotalParentLTAUS += getDoubleValueFromHashMap(hmRow, GmTTPHeader.PARENT_LTA_US);
			
			// Parent LTA OUS
			
			iTotalParentLTAOUS += getDoubleValueFromHashMap(hmRow, GmTTPHeader.PARENT_LTA_OUS);	
			iSubTotalTotalParentLTAOUS += getDoubleValueFromHashMap(hmRow, GmTTPHeader.PARENT_LTA_OUS);
			
			// Parent Need
			
			iTotalParentNeed += getDoubleValueFromHashMap(hmRow, GmTTPHeader.PARENT_NEED);	
			iSubTotalTotalParentNeed += getDoubleValueFromHashMap(hmRow, GmTTPHeader.PARENT_NEED);
			
			// Order Qty
			
			iTotalOrderQty += getDoubleValueFromHashMap(hmRow, GmTTPHeader.ORDER_QTY);	
			iSubTotalTotalOrderQty  += getDoubleValueFromHashMap(hmRow, GmTTPHeader.ORDER_QTY);
			
			// Order Cost
			
			iTotalOrderCost += getDoubleValueFromHashMap(hmRow, GmTTPHeader.ORDER_COST);	
			iSubTotalTotalOrderCost += getDoubleValueFromHashMap(hmRow, GmTTPHeader.ORDER_COST);
			
			// Unit Cost
			
			iTotalUnitCost += getDoubleValueFromHashMap(hmRow, GmTTPHeader.UNIT_COST);	
			iSubTotalTotalUnitCost += getDoubleValueFromHashMap(hmRow, GmTTPHeader.UNIT_COST);
		}
		else if (iGroupingCount == 3)
		{
			hmRow.put(GmTTPHeader.LTA,""+ Math.round(iTotalLTA));
			hmTTPFooter.put(GmTTPHeader.LTA,""+ Math.round(iTotalLTA));
			hmRow.put(GmTTPHeader.LTA_US,""+ Math.round(iTotalLTAUS));
			hmTTPFooter.put(GmTTPHeader.LTA_US,""+ Math.round(iTotalLTAUS));
			hmRow.put(GmTTPHeader.LTA_OUS,""+ Math.round(iTotalLTAOUS));
			hmTTPFooter.put(GmTTPHeader.LTA_OUS,""+ Math.round(iTotalLTAOUS));
			hmRow.put(GmTTPHeader.PARENT_LTA,""+ Math.round(iTotalParentLTA));
			hmTTPFooter.put(GmTTPHeader.PARENT_LTA,""+ Math.round(iTotalParentLTA));
			hmRow.put(GmTTPHeader.PARENT_LTA_US,""+ Math.round(iTotalParentLTAUS));
			hmTTPFooter.put(GmTTPHeader.PARENT_LTA_US,""+ Math.round(iTotalParentLTAUS));
			hmRow.put(GmTTPHeader.PARENT_LTA_OUS,""+ Math.round(iTotalParentLTAOUS));
			hmTTPFooter.put(GmTTPHeader.PARENT_LTA_OUS,""+ Math.round(iTotalParentLTAOUS));
			hmRow.put(GmTTPHeader.PARENT_NEED,""+ Math.round(iTotalParentNeed));
			hmTTPFooter.put(GmTTPHeader.PARENT_NEED,""+ Math.round(iTotalParentNeed));
			hmRow.put(GmTTPHeader.ORDER_QTY,""+ Math.round(iTotalOrderQty));
			hmTTPFooter.put(GmTTPHeader.ORDER_QTY,""+ Math.round(iTotalOrderQty));
			hmRow.put(GmTTPHeader.ORDER_COST,""+ Math.round(iTotalOrderCost));
			hmTTPFooter.put(GmTTPHeader.ORDER_COST,""+ Math.round(iTotalOrderCost));
			hmRow.put(GmTTPHeader.UNIT_COST,""+ Math.round(iTotalUnitCost));
			hmTTPFooter.put(GmTTPHeader.UNIT_COST,""+ Math.round(iTotalUnitCost));
		}
		else
		{
			hmRow.put(GmTTPHeader.LTA,""+Math.round(iSubTotalTotalLTA));
			iSubTotalTotalLTA =0;

			hmRow.put(GmTTPHeader.LTA_US,""+Math.round(iSubTotalTotalLTAUS));
			iSubTotalTotalLTAUS =0;
			
			hmRow.put(GmTTPHeader.LTA_OUS,""+Math.round(iSubTotalTotalLTAOUS));
			iSubTotalTotalLTAOUS =0;
			
			hmRow.put(GmTTPHeader.PARENT_LTA,""+Math.round(iSubTotalTotalParentLTA));
			iSubTotalTotalParentLTA =0;
			
			hmRow.put(GmTTPHeader.PARENT_LTA_US,""+Math.round(iSubTotalTotalParentLTAUS));
			iSubTotalTotalParentLTAUS =0;
			
			hmRow.put(GmTTPHeader.PARENT_LTA_OUS,""+Math.round(iSubTotalTotalParentLTAOUS));
			iSubTotalTotalParentLTAOUS =0;
			
			hmRow.put(GmTTPHeader.PARENT_NEED,""+Math.round(iSubTotalTotalParentNeed));
			iSubTotalTotalParentNeed =0;
			
			hmRow.put(GmTTPHeader.ORDER_QTY,""+Math.round(iSubTotalTotalOrderQty));
			iSubTotalTotalOrderQty =0;
			
			hmRow.put(GmTTPHeader.ORDER_COST,""+Math.round(iSubTotalTotalOrderCost));
			iSubTotalTotalOrderCost =0;
			
			hmRow.put(GmTTPHeader.UNIT_COST,""+Math.round(iSubTotalTotalUnitCost));
			iSubTotalTotalUnitCost =0;
		}	
	}
	
	/**
     * loadLeadTimeAdj -  hmRow will have the part details , take the lead time value from there
     * and apply the calculation for that part and store the lead time back in the same hashmap
     * 
     * Formula is explained with examples in
     * http://confluence.spineit.net/display/PRJ/GOP_R303F+TTP+Summary+-+Calculations
     *              
     * @param ArrayList alHeader, HashMap hmRow, int num_foreCastMonth, String monYear
     * @author vprasath 
     */
	
	private void loadLeadTimeAdj(ArrayList alHeader, HashMap hmRow, int num_foreCastMonth, String monYear)
	{
		// Lead Time Adj. Qty Calculation
		int no_of_ex_weeks = 0;	
		double months_to_add = 0;
		Date last_mon_fc;
		double leadTimeAdjQty = 0;
		double leadTimeUSAdjQty = 0;
		double leadTimeOUSAdjQty = 0;
		int iAddRemoveMon =1;
	
		int iLeadTimeWeeks = getIntegerValueFromHashMap(hmRow, GmTTPHeader.LEAD_TIME_ATTRIBUTE);
		no_of_ex_weeks = iLeadTimeWeeks - idefaultWeeks ;
		String strPartNum =  GmCommonClass.parseNull((String)hmRow.get("ID"));

		if(no_of_ex_weeks < 0)
		{
			no_of_ex_weeks = no_of_ex_weeks * -1; 			
			iAddRemoveMon = -1;
			num_foreCastMonth = num_foreCastMonth - 1;
		}
		
		last_mon_fc = addMonths(getMonth(monYear), num_foreCastMonth); // 08/01/2013
		
			for ( int i=1 ; i<= no_of_ex_weeks; i++)
			{
				 months_to_add = months_to_add + 0.25; // 0.25 is a hard coded value to represent one week of a month
				 if(i % 4 == 0)
				 {					 
					 String strColName = format(last_mon_fc, "MMM yy");
					 leadTimeAdjQty = leadTimeAdjQty + getDoubleValueFromHashMap(hmRow, "FC-"+strColName); 
					 leadTimeUSAdjQty = leadTimeUSAdjQty +getDoubleValueFromHashMap(hmRow, "FC_US-"+strColName); 
					 leadTimeOUSAdjQty = leadTimeOUSAdjQty + getDoubleValueFromHashMap(hmRow, "FC_OUS-"+strColName); 
					 last_mon_fc = addMonths(last_mon_fc, iAddRemoveMon);
					 months_to_add = 0;
				 }				
			}
			 if (months_to_add > 0)
			 {
				 String strColName = format(last_mon_fc, "MMM yy"); // Sep-13
				 leadTimeAdjQty = leadTimeAdjQty + (getDoubleValueFromHashMap(hmRow, "FC-"+strColName)  * months_to_add); // 0 + ( hmRow.get("FC Sep-13") * 0.75)
				 leadTimeUSAdjQty = leadTimeUSAdjQty + (getDoubleValueFromHashMap(hmRow, "FC_US-"+strColName)  * months_to_add);// 0 + ( hmRow.get(" US FC Sep-13") * 0.75)
				 leadTimeOUSAdjQty = leadTimeOUSAdjQty + (getDoubleValueFromHashMap(hmRow, "FC_OUS-"+strColName) * months_to_add); // 0 + ( hmRow.get("OUS FC Sep-13") * 0.75)
			 }
			 
			 hmRow.put(GmTTPHeader.LTA,""+ (Math.round(leadTimeAdjQty) * iAddRemoveMon));
			 hmRow.put(GmTTPHeader.LTA_US,""+ (Math.round(leadTimeUSAdjQty)* iAddRemoveMon));
			 hmRow.put(GmTTPHeader.LTA_OUS,""+ (Math.round(leadTimeOUSAdjQty) * iAddRemoveMon));

	}
	
	/**
     * getDoubleValueFromHashMap -  This is a utility method to get a value from the hashmap and 
     * convert that to an double and returns the double value. This method is used as a wrapper for the "hasmap.get" method. 
     * The advantage is, after getting the value from the hashmap, we can manipulate the data before returning it.
     * For Example, if the sheet is not a company level sheet, the lead time calculation need to be skipped, so the 
     * actual value is replaced with the default value.          
     *              
     * @param HashMap hmRow, String strColumnName
     * @author vprasath 
     */
	private double getDoubleValueFromHashMap(HashMap hmRow, String strColumnName)
	{
		String strValue = "";
		double dOut = 0;
		if(strColumnName.equals(GmTTPHeader.LEAD_TIME_ATTRIBUTE) || strColumnName.equals(GmTTPHeader.CALC_LEAD_TIME))
		{
			if(!strLevelHierarchy.equals("0") && !strLevelHierarchy.equals("102580")) // If strLevel is not company, then do not use the actual value, use the default value for calculation
			{
				strValue = ""+idefaultWeeks; 
			}
			else
			{
				strValue = hmRow.get(strColumnName) == null ? ""+idefaultWeeks : GmCommonClass.parseZero((String)hmRow.get(strColumnName)); // 10
			}
		}
		else if(strColumnName.equals(GmTTPHeader.UNIT_COST) || strColumnName.equals(GmTTPHeader.ORDER_COST))
		{
			////log.debug("(!strLevelID.equals() && !strLevelID.equals(100800)) || !isMultiPartTTP()" + ((!strLevelID.equals("") && !strLevelID.equals("100800")) || !isMultiPartTTP()));
			if(!strLevelHierarchy.equals("0") && !strLevelHierarchy.equals("102580")) // If strLevel is not company, then do not use the actual value, use the default value for calculation
			{
				strValue = "0"; 
			}
			else
			{
				strValue = GmCommonClass.parseZero((String)hmRow.get(strColumnName)); // 10
			}
		}
		else
		{
			strValue = GmCommonClass.parseZero((String)hmRow.get(strColumnName));
		}

		dOut = Double.parseDouble(strValue);
		
		return dOut;
	}

	/**
     * getIntegerValueFromHashMap -  This is a utility method to get a value from the hashmap and 
     * convert that to an integer and returns the integer value. This method is used as a wrapper for the "hasmap.get" method. 
     * The advantage is, after getting the value from the hashmap, we can manipulate the data before returning it.
     * For Example, if the sheet is not a company level sheet, the lead time calculation need to be skipped, so the 
     * actual value is replaced with the default value.          
     *              
     * @param HashMap hmRow, String strColumnName
     * @author vprasath 
     */
	
	private int getIntegerValueFromHashMap(HashMap hmRow, String strColumnName)
	{
		String strValue = "";
		if(strColumnName.equals(GmTTPHeader.LEAD_TIME_ATTRIBUTE) || strColumnName.equals(GmTTPHeader.CALC_LEAD_TIME))
		{
			if(!strLevelHierarchy.equals("0") && !strLevelHierarchy.equals("102580")) // If strLevel is not company, then do not use the actual value, use the default value for calculation
			{
				strValue = ""+idefaultWeeks; 
			}
			else
			{
				strValue = hmRow.get(strColumnName) == null ? ""+idefaultWeeks : GmCommonClass.parseZero((String)hmRow.get(strColumnName)); // 10
			}
		}
		else
		{
			strValue = GmCommonClass.parseZero((String)hmRow.get(strColumnName));
		}
		
		return Integer.parseInt(strValue);
	}
	
	/**
     * format -  This is a utility method to format a date with the given date format.
     *              
     * @param String monYear
     * @author vprasath 
     */
	
	private String format(Date strDate, String strFormat)
	{
		DateFormat formatter  = new SimpleDateFormat(strFormat);
		return formatter.format(strDate);
	}

	
	/**
     * addMonths -  This is a utility method to convert the string 'MM/YYYY' to a date object 'MM/DD/YYYY'
     *              
     * @param String monYear
     * @author vprasath 
     */
	
	private Date getMonth(String monYear) 
	{
		DateFormat formatter ; 
		Date date ; 
		
		try {
		String dateArr[] = monYear.split("/");
		
		   formatter = new SimpleDateFormat("MM/dd/yyyy");
		   date = formatter.parse(dateArr[0]+"/01/"+dateArr[1]);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			date = null;
		}
		return date;
	}

	/**
     * addMonths -  This is a utility method to find the month when a number is added to the given month
     *              For Ex, if 2 is added to 01/01/2013, it returns 03/01/2013
     *                      if -2 is added to 03/01/2013, it returns 01/01/2013
     * 
     * @param Date date, int numMonths
     * @author vprasath 
     */
	private Date addMonths(Date date, int numMonths){
		date.setMonth((date.getMonth() + numMonths) );
	    return date;
	}
	
	/**
     * loadParentPartLTA - This method is used to calculate the PLTA and Parent Need for all the subparts
     * inside the details arraylist.
     * 
     * 1. alParentSubList - will have the details like parent part, lead time  of all the sub parts in the ttp sheet.
     * 2. alTTPData - Arraylist of hashmap ( hmRow ) of all the parts that need to be displayed
     * 
     * High Level Explanation:
     * 
     * 1. Loop the alTTPData and create a hashmap "hmTempDetails" with  key as part number and value as  hmRow.
     * 2. alParentSubList is already sorted in the SQL and it is sorted such that highest level parent 
     *    parts in the top to low level sub parts at the bottom ( refer gm_pkg_oppr_ttp_summary.gm_ld_parent_sub_parts).
     * 3. Loop alParentSubList, take the parent part and get the details of the parent part from hmTempDetails 
     * 	  and calculate the lead time and store it in the same hashmap
     * 4. Formula is explained with examples in
     *    http://confluence.spineit.net/display/PRJ/GOP_R303F+TTP+Summary+-+Calculations
     *
     * @param ArrayList alTTPData, ArrayList alParentSubList, int num_foreCastMonth, String monYear
     * @author vprasath 
     */
	
	private void loadParentPartLTA(ArrayList alTTPData, ArrayList alParentSubList, int num_foreCastMonth, String monYear) throws AppError {
		Iterator itDetails = alTTPData.iterator();
		Iterator itParentSubDetails = alParentSubList.iterator();
		ArrayList alPNeed = new ArrayList();
		HashMap hmTempDetails = new HashMap();
		HashMap hmRow = new HashMap();
		HashMap hmParentSubData = new HashMap();
		HashMap hmSubPartRowObj = new HashMap();
		HashMap hmParentPartRowObj = new HashMap();
		HashMap hmFamilyPartRowObj = new HashMap();
		
		int iAlNeedSize = 0;
		while (itDetails.hasNext()){
			hmRow = (HashMap) itDetails.next();
			hmTempDetails.put((String)hmRow.get("ID"),hmRow);
		}
		
		while (itParentSubDetails.hasNext()){
			double iPNeed = 0;
			double iTotReq = 0;
			double iOrdQty = 0;
			double iNeedCnt = 0;
			double dbOrdCost = 0;
			String strSubPart ="";
			String strParentPart ="";
			String strQty ="";
			String strPO = "";
			String strOrdQty = "";
			String strInv = "";
			String strTotReq = "";
			String strTempSubPart = "";
			String strTempNeed = "";
			String strUnitCost = "";
			int no_of_ex_weeks = 0;	
			double months_to_add = 0;
			Date last_mon_fc;
			double leadTimeAdjQty = 0;
			double leadTimeUSAdjQty = 0;
			double leadTimeOUSAdjQty = 0;
			int iAddRemoveMon =1;
			String strLevel = "";
			int iLeadTimeWeeks = 0;
			String strCalculatedLeadTime = "";
			int iCalLeadTime = 0;
			double dCalculatedParentNeed = 0;
			double dCalculatedUSParentNeed = 0;
			double dCalculatedOUSParentNeed = 0;
			double dCalculatedLTA = 0;
			double dCalculatedUSLTA = 0;
			double dCalculatedOUSLTA = 0;
			String strFamilyPart = "";
			
			hmParentSubData = (HashMap) itParentSubDetails.next();
			strSubPart = GmCommonClass.parseZero((String)hmParentSubData.get("SUBPART"));
		    strParentPart = GmCommonClass.parseZero((String)hmParentSubData.get("PARENTPART"));
		    strQty = GmCommonClass.parseZero((String)hmParentSubData.get("ACTUAL_QTY"));
		    strLevel = GmCommonClass.parseZero((String)hmParentSubData.get("LEVEL"));
		    strCalculatedLeadTime = GmCommonClass.parseZero((String)hmParentSubData.get("CALC_LEAD_TIME"));
		    strFamilyPart = GmCommonClass.parseNull((String)hmParentSubData.get("ACTUAL_SUB_PART"));
		    	
		    String strCalArr[] = strCalculatedLeadTime.split(",");
		    
		     for(int i=0; i< strCalArr.length;i++)
		     {
		    	 if(strCalArr[i] != null && !strCalArr[i].equals(""))
		    	 {
		    		 iCalLeadTime += Integer.parseInt(strCalArr[i]);
		    	 }
		     }
		    
		    int iLevel = Integer.parseInt(strLevel);
		    double dSubPartQty = Double.parseDouble(strQty);
		    
		    hmSubPartRowObj = GmCommonClass.parseNullHashMap((HashMap) hmTempDetails.get(strSubPart));
		    hmParentPartRowObj = GmCommonClass.parseNullHashMap((HashMap)hmTempDetails.get(strParentPart));
		    hmFamilyPartRowObj = GmCommonClass.parseNullHashMap((HashMap)hmTempDetails.get(strFamilyPart));
		    	
		    iLeadTimeWeeks = getIntegerValueFromHashMap(hmSubPartRowObj, GmTTPHeader.LEAD_TIME_ATTRIBUTE) + iCalLeadTime ;
		    	
		    int iPreviousLevel = getIntegerValueFromHashMap(hmSubPartRowObj, GmTTPHeader.PREVIOUS_LEVEL) == 0 ? iLevel :
		    					 getIntegerValueFromHashMap(hmSubPartRowObj, GmTTPHeader.PREVIOUS_LEVEL);
		    
		    if(hmParentPartRowObj.size() == 0)
		    {
		    	continue;
		    }
		    
		    int temp_num_foreCastMonth = 0;
		
		    if(strSubPart.equals(strFamilyPart))
		    {
		    	HashMap hmTemp=this.addMoreForecastMonths(leadTimeUSAdjQty,leadTimeOUSAdjQty
		    											 ,num_foreCastMonth,monYear,hmParentPartRowObj);
		    	leadTimeUSAdjQty = (Double)hmTemp.get("leadTimeUSAdjQty");
		    	leadTimeOUSAdjQty = (Double)hmTemp.get("leadTimeOUSAdjQty");  
		    }

		    hmSubPartRowObj.put(GmTTPHeader.PREVIOUS_LEVEL, strLevel);
			no_of_ex_weeks = iLeadTimeWeeks - idefaultWeeks ;
			
			if(no_of_ex_weeks < 0)
			{
				no_of_ex_weeks = no_of_ex_weeks * -1; 			
				iAddRemoveMon = -1;
				num_foreCastMonth = num_foreCastMonth - 1;
			}
			last_mon_fc = addMonths(getMonth(monYear), num_foreCastMonth); // 08/01/2013
			
			for ( int i=1 ; i<= no_of_ex_weeks; i++)
			{
				 months_to_add = months_to_add + 0.25; // 0.25 is a hard coded value to represent one week of a month
				 if(i % 4 == 0)
				 {					 
					 String strColName = format(last_mon_fc, "MMM yy");
					 leadTimeUSAdjQty = leadTimeUSAdjQty +getDoubleValueFromHashMap(hmParentPartRowObj, "FC_US-"+strColName); 
					 leadTimeOUSAdjQty = leadTimeOUSAdjQty + getDoubleValueFromHashMap(hmParentPartRowObj, "FC_OUS-"+strColName); 
					 last_mon_fc = addMonths(last_mon_fc, iAddRemoveMon);
					 months_to_add = 0;
				 }				
			}
			
			 if (months_to_add > 0)
			 {
				 String strColName = format(last_mon_fc, "MMM yy"); // Sep-13				
				 leadTimeUSAdjQty = leadTimeUSAdjQty + (getDoubleValueFromHashMap(hmParentPartRowObj, "FC_US-"+strColName)  * months_to_add);// 0 + ( hmRow.get(" US FC Sep-13") * 0.75)
				 leadTimeOUSAdjQty = leadTimeOUSAdjQty + (getDoubleValueFromHashMap(hmParentPartRowObj, "FC_OUS-"+strColName) * months_to_add); // 0 + ( hmRow.get("OUS FC Sep-13") * 0.75)
			 }
			 
			 double dUS_Parent_Need  =   Math.max(0,(leadTimeUSAdjQty +  
					 getDoubleValueFromHashMap(hmParentPartRowObj,GmTTPHeader.FORECAST_QTY_US) + 
					 getDoubleValueFromHashMap(hmParentPartRowObj,GmTTPHeader.TPR_US) +
					 getDoubleValueFromHashMap(hmParentPartRowObj,strSubPart+"-"+GmTTPHeader.PARENT_NEED_US) +
					 getDoubleValueFromHashMap(hmParentPartRowObj,GmTTPHeader.SET_PAR) +
					 getDoubleValueFromHashMap(hmParentPartRowObj,GmTTPHeader.OTN_US)
					 ) - 
					 getDoubleValueFromHashMap(hmParentPartRowObj,GmTTPHeader.TOTAL_INVENTORY));
			 
			 
			 		dCalculatedUSParentNeed = ((dUS_Parent_Need +  getDoubleValueFromHashMap(hmParentPartRowObj,GmTTPHeader.PO)) * dSubPartQty) +
			 									getDoubleValueFromHashMap(hmFamilyPartRowObj,strSubPart+"-"+GmTTPHeader.PARENT_NEED_US);
			 		
			 		dCalculatedUSLTA = (dUS_Parent_Need * dSubPartQty) +
										getDoubleValueFromHashMap(hmFamilyPartRowObj,strSubPart+"-"+GmTTPHeader.LTA_US);
			 
			 		hmFamilyPartRowObj.put(strSubPart+"-"+GmTTPHeader.PARENT_NEED_US, ""+Math.max(dCalculatedUSParentNeed,0));
			 		hmFamilyPartRowObj.put(strSubPart+"-"+GmTTPHeader.LTA_US, ""+Math.max(dCalculatedUSLTA,0));
			 
			 double dRemaining_Inv_Qty = Math.max(0, getDoubleValueFromHashMap(hmParentPartRowObj,GmTTPHeader.TOTAL_INVENTORY) - 
					 								  (leadTimeUSAdjQty +  
					 								   getDoubleValueFromHashMap(hmParentPartRowObj,GmTTPHeader.FORECAST_QTY_US) + 
					 								   getDoubleValueFromHashMap(hmParentPartRowObj,GmTTPHeader.TPR_US) +
					 								   getDoubleValueFromHashMap(hmParentPartRowObj,GmTTPHeader.OTN_US) +
					 								   getDoubleValueFromHashMap(hmParentPartRowObj,GmTTPHeader.SET_PAR) 
					 								   + getDoubleValueFromHashMap(hmParentPartRowObj,strSubPart+"-"+GmTTPHeader.PARENT_NEED_US)
					 								   ));
			 
			 double dOUS_Parent_Need  =  Math.max(0,(leadTimeOUSAdjQty +  
					 getDoubleValueFromHashMap(hmParentPartRowObj,GmTTPHeader.FORECAST_QTY_OUS) + 
					 getDoubleValueFromHashMap(hmParentPartRowObj,GmTTPHeader.TPR_OUS) +
					 getDoubleValueFromHashMap(hmParentPartRowObj,strSubPart+"-"+GmTTPHeader.PARENT_NEED_OUS)) +
					 getDoubleValueFromHashMap(hmParentPartRowObj,GmTTPHeader.OTN_OUS) - 
					 (getDoubleValueFromHashMap(hmParentPartRowObj,GmTTPHeader.RW_INVENTORY) + dRemaining_Inv_Qty));
			 			 
			 		dCalculatedOUSParentNeed =  (dOUS_Parent_Need * dSubPartQty ) +
			 								getDoubleValueFromHashMap(hmFamilyPartRowObj,strSubPart+"-"+GmTTPHeader.PARENT_NEED_OUS);
			 		dCalculatedOUSLTA = dCalculatedOUSParentNeed;
			 		
			 		hmFamilyPartRowObj.put(strSubPart+"-"+GmTTPHeader.PARENT_NEED_OUS, ""+ Math.max(dCalculatedOUSParentNeed,0));
			 		hmFamilyPartRowObj.put(strSubPart+"-"+GmTTPHeader.LTA_OUS, ""+ Math.max(dCalculatedOUSLTA,0));
			 
			 double dParentNeed = dUS_Parent_Need +  dOUS_Parent_Need;
			 dCalculatedParentNeed =  dCalculatedUSParentNeed + dCalculatedOUSParentNeed;
			 dCalculatedLTA = dCalculatedUSLTA + dCalculatedOUSLTA;
			 
			 hmFamilyPartRowObj.put(strSubPart+"-"+GmTTPHeader.PARENT_NEED, ""+Math.max(dCalculatedParentNeed,0));
			 hmFamilyPartRowObj.put(strSubPart+"-"+GmTTPHeader.LTA, ""+Math.max(dCalculatedLTA,0));
			 
			 
			 if(strSubPart.equals(strFamilyPart))
			 {
				 double dTotalParentNeed = ((getDoubleValueFromHashMap(hmFamilyPartRowObj,strSubPart+"-"+GmTTPHeader.PARENT_NEED)  *  iAddRemoveMon)); 
				 double dUSTotalParentNeed = ((getDoubleValueFromHashMap(hmFamilyPartRowObj,strSubPart+"-"+GmTTPHeader.PARENT_NEED_US)  *  iAddRemoveMon)); 
				 double dOUSTotalParentNeed = ((getDoubleValueFromHashMap(hmFamilyPartRowObj,strSubPart+"-"+GmTTPHeader.PARENT_NEED_OUS)  *  iAddRemoveMon)); 

				 hmSubPartRowObj.put(GmTTPHeader.PARENT_NEED, "" + (Math.round(dTotalParentNeed)));
				 hmSubPartRowObj.put(GmTTPHeader.PARENT_NEED_US, "" + (Math.round(dUSTotalParentNeed)));
				 hmSubPartRowObj.put(GmTTPHeader.PARENT_NEED_OUS, "" + (Math.round(dOUSTotalParentNeed)));
				 
				
				 double dParentLTA = ((getDoubleValueFromHashMap(hmFamilyPartRowObj,strSubPart+"-"+GmTTPHeader.LTA)  *  iAddRemoveMon) +
									   getDoubleValueFromHashMap(hmSubPartRowObj, GmTTPHeader.LTA)); 

				double dParentLTAUS =((getDoubleValueFromHashMap(hmFamilyPartRowObj,strSubPart+"-"+GmTTPHeader.LTA_US)  *  iAddRemoveMon) +
										getDoubleValueFromHashMap(hmSubPartRowObj, GmTTPHeader.LTA_US)); 
				
				double dParentLTAOUS =((getDoubleValueFromHashMap(hmFamilyPartRowObj,strSubPart+"-"+GmTTPHeader.LTA_OUS)  *  iAddRemoveMon) +
										getDoubleValueFromHashMap(hmSubPartRowObj, GmTTPHeader.LTA_OUS)); 
				
				hmSubPartRowObj.put(GmTTPHeader.PARENT_LTA, "" + Math.round(dParentLTA));
				hmSubPartRowObj.put(GmTTPHeader.PARENT_LTA_US,"" + Math.round(dParentLTAUS));
				hmSubPartRowObj.put(GmTTPHeader.PARENT_LTA_OUS,"" + Math.round(dParentLTAOUS));				

			 }			
		}		
	}

	/* This method will be called for the Non-Sterile Parts and system will check how months of forecast to be added.
	 * based on that, the Forecast Months data for US, OUS will be retrieved and will be added to the lead Time calculation. 
	 * 
	 */
	private HashMap addMoreForecastMonths(double leadTimeUSAdjQty, double leadTimeOUSAdjQty, 
										int num_foreCastMonth, String monYear
										,HashMap hmParentPartRowObj)
	{
		int add_months_rules = Integer.parseInt(GmCommonClass.getRuleValue("MONTHS_TO_ADD","GOP-NON-STERILE-PNED"));
		
    	int temp_num_foreCastMonth = 0;
    	HashMap hmReturn = new HashMap();
    	
    	for(int i=1;i<=add_months_rules;i++){
    		
	    	temp_num_foreCastMonth = num_foreCastMonth;
	    	Date temp_last_mon_fc = addMonths(getMonth(monYear), temp_num_foreCastMonth+i);
	    	String tempstrColName = format(temp_last_mon_fc, "MMM yy");
	    	leadTimeUSAdjQty = leadTimeUSAdjQty + (getDoubleValueFromHashMap(hmParentPartRowObj, "FC_US-"+tempstrColName) );
	    	leadTimeOUSAdjQty = leadTimeOUSAdjQty + (getDoubleValueFromHashMap(hmParentPartRowObj, "FC_OUS-"+tempstrColName) );
	    	
    	}
    	hmReturn.put("leadTimeUSAdjQty", leadTimeUSAdjQty);
    	hmReturn.put("leadTimeOUSAdjQty", leadTimeOUSAdjQty);
    	
    	log.debug(" # # # hmReturn addMoreForecastMonths "+hmReturn);
		
    	return hmReturn;
	}

	/**
     * organizeHeader - This method is the parent method for organizing the header
     *  1. Make a database call to fetch the default columns for the selected level ( US / Company etc..)
     *  2. Add the forecast months to the default header
     *  3. Add the filter columns to the  default header that are selected by the users
     *  4. Now Default header has all the columns needed, but not in the sorted order, but alHeader has all 
     *     columns in the sorted order but with few extra columns, so remove the extra columns from alHeader
     *     by referring the default header.
     *     
     * @param ArrayList alHeader, HashMap hmSelectedFilters, String strLevelId, String strLevelHierarchy,  int num_Forecast_Months
     * @author vprasath 
     */
	
	public ArrayList organizeHeader(ArrayList alHeader, HashMap hmSelectedFilters, String strLevelId, String strLevelHierarchy,  int num_Forecast_Months) throws AppError
	{
		String strSplit = GmCommonClass.parseNull((String)hmSelectedFilters.get("SPLIT"));
				
		HashMap hmDefaultHeader = new HashMap();
		HashMap hmReturnHeader = new HashMap();
		ArrayList alDefaultHeader = new ArrayList();
		
		strLevelId = GmCommonClass.parseZero(strLevelId);
		strLevelHierarchy = GmCommonClass.parseZero(strLevelHierarchy);
		
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		gmDBManager.setPrepareString("gm_pkg_oppr_ttp_summary.gm_fch_ttp_header", 4);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.setString(1,strSplit);
		gmDBManager.setString(2,strLevelId);
		gmDBManager.setString(3,strLevelHierarchy);
		gmDBManager.execute(); 
		hmDefaultHeader = GmCommonClass.parseNullHashMap(gmDBManager.returnConvertedHashMap((ResultSet) gmDBManager.getObject(4)));
		gmDBManager.close();
		alDefaultHeader.addAll(hmDefaultHeader.keySet());
		
		addFCMonthsToHeader(alDefaultHeader, alHeader, num_Forecast_Months);
		
		addSelectedFilters(alDefaultHeader, alHeader, hmSelectedFilters);
		
		removeUnwantedHeader(alDefaultHeader, alHeader);
		
		strTTPColumnType = hmDefaultHeader.size() > 0 ? GmCommonClass.parseNull((String)(hmDefaultHeader.values().iterator().next())) : strTTPColumnType;
	
		return alDefaultHeader;		
	}
	
	/**
     * addFCMonthsToHeader -  Fetching the default header from the database
     * will not have the forecast months as these forecast columns change every month
     * so this method is used to find the forecast months and add it to the 
     * default header.
     *
     * @param ArrayList alDefaultHeader, ArrayList alHeader, int iNoOfMonths
     * @author vprasath 
     */
	public void addFCMonthsToHeader(ArrayList alDefaultHeader, ArrayList alHeader, int iNoOfMonths)
	{
		Iterator itrMonHeader = alHeader.iterator();
		ArrayList alTempHeader = new ArrayList();

		// Add the months to display in the front end to the default header
		while(itrMonHeader.hasNext())
		{
			String strColumnName = (String)itrMonHeader.next();
			 if (strColumnName.startsWith("FC-") && iNoOfMonths > 0)
			 {
				 iNoOfMonths -- ;
				 strColumnName = strColumnName.replaceAll("FC-", "");
				 alDefaultHeader.add(strColumnName);
				 alTempHeader.add(strColumnName);
			 }
		}
		int header = findPositionToAdd(GmTTPHeader.FORECAST_MONTHS, alHeader);
		alHeader.addAll(++header, alTempHeader);
	}

	/**
     * findPositionToAdd -  alHeader has all the column headers except the calculated columns
     * in the sorted order, to add a new column to this alHeader, we need to find a position
     * to which our column can be added, this method can be used to find that. For now, this method 
     * is used only to find the position for the forecast month, by extending the case 
     * statement, we can do it for any column that needed this. 
     *
     * @param int iColumnPosition, ArrayList alHeader
     * @author vprasath 
     */
	
	private int findPositionToAdd(int iColumnPosition, ArrayList alHeader)
	{
		int iPos = 0;
		
		switch(iColumnPosition) {
		 case  GmTTPHeader.FORECAST_MONTHS :
				iPos =	alHeader.indexOf(GmTTPHeader.CALC_LEAD_TIME);
						if(iPos == -1)
						{
							iPos =	alHeader.indexOf(GmTTPHeader.UNIT_COST);
		
							 if(iPos == -1)
							 {
								 iPos =	alHeader.indexOf(GmTTPHeader.DESCRIPTION);
							 }
						 }
				break;
		}
		
		return iPos;
	}
	
	/**
     * addSelectedFilters - The intention of this method is to add all the filters that 
     * are selected by users. Since we are consolidating all the 
     * needed column headers in alDefaultHeader, this is necessary to add the user 
     * selected filters as well. One Exception is, if "Split US / OUS"
     * is selected, there is no one specific column to add to alDefaultHeader, the 
     * needed columns for that specific filter is fetched from database. 
     * refer organizeHeader ( gm_pkg_oppr_ttp_summary.gm_fch_ttp_header )
     *  
     * @param ArrayList alDefaultHeader, ArrayList alHeader, HashMap hmSelectedFilters 
     * @author vprasath 
     */
	
	public void addSelectedFilters(ArrayList alDefaultHeader, ArrayList alHeader, HashMap hmSelectedFilters)
	{
		Iterator itrSelectedFilters = hmSelectedFilters.keySet().iterator();		
		String strSplit = GmCommonClass.parseNull((String)hmSelectedFilters.get(GmTTPHeader.SPLIT));
		String strParentLTA = GmCommonClass.parseNull((String)hmSelectedFilters.get(GmTTPHeader.PARENT_LTA));
		

			if(strSplit.equals("Y") && strParentLTA.equals("Y"))
			{
				alDefaultHeader.add(GmTTPHeader.PARENT_LTA_US);
				alDefaultHeader.add(GmTTPHeader.PARENT_LTA_OUS);
			}
			else if(!strSplit.equals("Y") && strParentLTA.equals("Y"))
			{
				alDefaultHeader.add(GmTTPHeader.PARENT_LTA);
			}

		while(itrSelectedFilters.hasNext())
		{
			String strHeaderKey = GmCommonClass.parseNull((String)itrSelectedFilters.next());		
			String strHeaderValue = GmCommonClass.parseNull((String)hmSelectedFilters.get(strHeaderKey));
			if(strHeaderValue.equals("Y") && !strHeaderKey.equals(GmTTPHeader.PARENT_LTA))
			{
				alDefaultHeader.add(strHeaderKey);
			}
		}
	}
	
	/**
     * removeUnwantedHeader - alHeader - will have the header list in the sorted order,
     * but it will have the columns that are not needed to be displayed as well,
     * and alDefaultHeader will have only the column headers that need to be
     * displayed, but it will not have the columns in the needed sort order.
     * So loop through the alHeader and remove the columns that are not 
     * necessary by referring to the alDefaultHeader
     *  
     * @param ArrayList alDefaultHeader, ArrayList alHeader 
     * @author vprasath 
     */	
	private void removeUnwantedHeader(ArrayList alDefaultHeader, ArrayList alHeader)
	{
			Iterator itrHeader = alHeader.iterator();
			while(itrHeader.hasNext())
			{
				if(!alDefaultHeader.contains(((String)itrHeader.next()).trim()))
				{
					itrHeader.remove();
				}
			}
	}
}
