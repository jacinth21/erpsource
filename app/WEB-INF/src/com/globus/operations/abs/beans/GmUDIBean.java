/**
 * 
 */
package com.globus.operations.abs.beans;

import java.util.HashMap;

import com.globus.common.db.GmDBManager;

/**
 * @author tsekaran
 *
 */
public abstract class GmUDIBean {
	public abstract void saveUDIInfo(GmDBManager gmDBmanager, HashMap hmInput);
	

}
