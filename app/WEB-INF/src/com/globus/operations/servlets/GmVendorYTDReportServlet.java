/*****************************************************************************
 * File : GmSalesYTDReportServlet Desc : This Servlet will be used for all Sales YTD Report
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.servlets.GmServlet;
import com.globus.operations.beans.GmVendorReportBean;

public class GmVendorYTDReportServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strDispatchTo = "";
    String strJSPName = "";
    String strDistributorID = "";
    String strFromMonth = "";
    String strFromYear = "";
    String strToMonth = "";
    String strToYear = "";

    String strShowArrowFl = "";
    String strShowPercentFl = "";
    String strHideAmount = "";
    String strHeader = "";


    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      String strAction = request.getParameter("hAction");
      String strSessCompanyLocale = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
      GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.operations.GmVendorReportYTD", strSessCompanyLocale);
      GmCommonClass gmCommon = new GmCommonClass();
      GmVendorReportBean gmVendor = new GmVendorReportBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean();
      HashMap hmReturn = new HashMap();
      HashMap hmFromDate = new HashMap();

      String strCondition = null;

      String strId = "";
      ArrayList alDistList = new ArrayList();
      ArrayList alMonthDropDown = new ArrayList();
      ArrayList alYearDropDown = new ArrayList();



      if (strAction == null) {
        strAction = (String) session.getAttribute("strSessOpt");
      }

      strFromMonth = request.getParameter("Cbo_FromMonth");
      strFromYear = request.getParameter("Cbo_FromYear");
      strToMonth = request.getParameter("Cbo_ToMonth");
      strToYear = request.getParameter("Cbo_ToYear");
      strShowArrowFl = request.getParameter("Chk_ShowArrowFl");
      strShowPercentFl = request.getParameter("Chk_ShowPercentFl");
      strHideAmount = request.getParameter("Chk_HideAmountFl");

      strAction = (strAction == null) ? "LoadDistributor" : strAction;

      // To get the Access Level information
      strCondition = getAccessCondition(request, response);

      if (strAction.equals("LoadVendor")) {
        strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SALES_BY_VENDOR_BY_MONTH")); 
        hmReturn =
            gmVendor.reportYTDByVendor(strFromMonth, strFromYear, strToMonth, strToYear,
                strCondition);
      }

      // To fetch the value for the unload condition
      hmFromDate = (HashMap) hmReturn.get("FromDate");
      if (hmFromDate.get("FirstTime") != null) {
        strFromMonth = (String) hmFromDate.get("FROMMONTH");
        strFromYear = (String) hmFromDate.get("FROMYEAR");
        strToMonth = (String) hmFromDate.get("TOMONTH");
        strToYear = (String) hmFromDate.get("TOYEAR");
      }
      alMonthDropDown = GmCommonClass.parseNullArrayList(gmCommonBean.getMonthList());
      alYearDropDown = GmCommonClass.parseNullArrayList(gmCommonBean.getYearList());

      hmReturn.put("ALMONTHDROPDOWN", alMonthDropDown);
      hmReturn.put("ALYEARDROPDOWN", alYearDropDown);

      request.setAttribute("hAction", strAction);
      request.setAttribute("strHeader", strHeader);
      request.setAttribute("Cbo_FromMonth", strFromMonth);
      request.setAttribute("Cbo_FromYear", strFromYear);
      request.setAttribute("Cbo_ToMonth", strToMonth);
      request.setAttribute("Cbo_ToYear", strToYear);
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("Chk_ShowArrowFl", strShowArrowFl);
      request.setAttribute("Chk_ShowPercentFl", strShowPercentFl);
      request.setAttribute("Chk_HideAmountFl", strHideAmount);

      strJSPName = "/GmVendorReportYTD.jsp";
      dispatch(strOperPath.concat(strJSPName), request, response);
    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of
