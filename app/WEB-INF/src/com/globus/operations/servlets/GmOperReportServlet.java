/*****************************************************************************
 * File : GmOperReportServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.beans.GmVendorReportBean;

public class GmOperReportServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strDispatchTo = "";

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      GmVendorReportBean gmVendor = new GmVendorReportBean(getGmDataStoreVO());

      ArrayList alReturn = new ArrayList();
      ArrayList alResult = new ArrayList();
      HashMap hmParam = new HashMap();
      HashMap hmReturn = new HashMap();
      String strSessCompanyLocale =GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
      String strTypeIds = GmCommonClass.parseNull(request.getParameter("hTypeIds"));
      String strCatIds = GmCommonClass.parseNull(request.getParameter("hCatIds"));
      String strStatIds = GmCommonClass.parseNull(request.getParameter("hStatIds"));
      String strExcel = "";
      String strExport = "";
      String strGridXmlData = "";

      hmParam.put("TYPE", strTypeIds);
      hmParam.put("CATEGORY", strCatIds);
      hmParam.put("STATUS", strStatIds);
/*
 * PMT-30886*/
      alResult = gmVendor.reportVendor(hmParam);
      request.setAttribute("VENDORREPORTDATA", alResult);
      request.setAttribute("alReturn", alReturn);

      hmReturn = gmVendor.loadVendorLists();
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("hmParam", hmParam);
      hmParam.put("VENDORREPORTDATA", alResult);
      hmParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
      log.debug("hmParam"+hmParam);
	  strGridXmlData = generateOutput(hmParam);
      request.setAttribute("GRIDXMLDATA", strGridXmlData);
      // The Below Code is added for PMT-5022 which is used to render special character when click
      // on Excel.
      strExcel = GmCommonClass.parseNull(request.getParameter("hExcel"));
      strExport = GmCommonClass.parseNull(request.getParameter("d-5394226-e"));// display tag excel
                                                                               // export parameter

      if (strExcel.equals("Excel") && strExport.equals("2")) { // display tag excel export
        request.setAttribute("hMode", "PaymentListExcel");
        response.setContentType("application/vnd.ms-excel;charset=Cp1256"); // UTF-8 is not working
                                                                            // for display tag
        response.setHeader("Content-disposition", "attachment;filename=GmOperReportServlet.xls");
      }

      dispatch(strOperPath.concat("/GmVendorReport.jsp"), request, response);
      if (strExcel.equals("Excel") && strExport.equals("2")) {
        response.setContentType("application/vnd.ms-excel;charset=Cp1256"); // UTF-8 is not working
                                                                            // for display tag
      }

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
  private String generateOutput(HashMap hmParam) throws Exception {
          GmTemplateUtil templateUtil = new GmTemplateUtil();
          log.debug("generateOutput "+hmParam);
          String strSessCompanyLocale = GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
          templateUtil.setDataMap("hmParam", hmParam);
          templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean("properties.labels.operations.GmVendorReport", strSessCompanyLocale));
          templateUtil.setTemplateSubDir("operations/templates");
          templateUtil.setTemplateName("GmVendorReport.vm");
          return templateUtil.generateOutput();
 } 
}// End of GmOperReportServlet
