/*****************************************************************************
 * File : GmReprocessMaterialServlet.java Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmRuleBean;
import com.globus.common.servlets.GmServlet;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.beans.GmLogisticsBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.beans.GmPurchaseBean;
import com.globus.operations.inventory.beans.GmLocationBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmReprocessMaterialServlet extends GmServlet {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  public HashMap setArrayListValues(HashMap hmReturn) {
    GmLogonBean gmLogonBean = new GmLogonBean(getGmDataStoreVO());
    try {
      ArrayList alStatus = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CGSRP"));
      ArrayList alCreatedBy = gmLogonBean.getEmployeeList("300", "");
      ArrayList alReturn =
          GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CONTY,CONOP,LONOP,BLTOP"));
      hmReturn.put("CODELIST", alReturn);
      hmReturn.put("ALSTATUS", alStatus);
      hmReturn.put("ALCREATEDBY", alCreatedBy);
    } catch (Exception exp) {
      exp.printStackTrace();
    }
    return hmReturn;
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strDispatchTo = strOperPath.concat("/GmInHouseItemConsignVerify.jsp");

    try {
      /* checkSession(response, session); */// Checks if the current session is
      // valid, else redirecting to
      // SessionExpiry page
      instantiate(request, response);
      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strOpt = request.getParameter("hOpt");

      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;

      GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
      GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
      GmPurchaseBean gmPurc = new GmPurchaseBean(getGmDataStoreVO());
      GmCommonClass gmCommon = new GmCommonClass();
      GmLogisticsBean gmLogis = new GmLogisticsBean(getGmDataStoreVO());
      GmRuleBean gmRuleBean = new GmRuleBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmLocationBean gmLocationBean = new GmLocationBean(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      HashMap hmResult = new HashMap();
      HashMap hmParam = new HashMap();
      ArrayList alReturn = new ArrayList();
      ArrayList alLcnPartMap = new ArrayList();

      RowSetDynaClass rsInHouseTrans = null;
      RowSetDynaClass rsConsignTrans = null;

      ArrayList alTransDetails = new ArrayList();

      String strUserId = (String) session.getAttribute("strSessUserId");
      String strConsignId = "";
      String strComments = "";
      String strVerifiedBy = "";
      String strType = "";
      String strPartNum = "";
      String strTransactionId = "";
      String strRefId = "";
      String strCreatedBy = "";
      String strStatus = "";
      String strControlNum = "";
      String strEtchId = "";
      String strShowDetails = "";
      String strTransType = "";

      String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
      // Locale
      GmResourceBundleBean gmResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);

      GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
      String strTodaysDate = GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
      String strAccess = "";
      String strLogReason = "";
      int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
      String strMonthBeginDate =
          GmCalenderOperations.getFirstDayOfMonth(currMonth, getGmDataStoreVO().getCmpdfmt());
      String strFromDt =
          request.getParameter("Txt_FromDate") == null ? strMonthBeginDate : request
              .getParameter("Txt_FromDate");
      String strToDt =
          request.getParameter("Txt_ToDate") == null ? strTodaysDate : request
              .getParameter("Txt_ToDate");
      request.setAttribute("hFrmDt", strFromDt);
      request.setAttribute("hToDt", strToDt);

      hmParam.put("FROMDT", strFromDt);
      hmParam.put("TODT", strToDt);

      log.debug(" Action is " + strAction + " opt is " + strOpt);

      hmParam.put("RULEID", strOpt);
      HashMap hmTransRules = gmCommonBean.fetchTransactionRules(hmParam);
      request.setAttribute("TRANS_RULES", hmTransRules);
      String strIncludeRuleEng =
          GmCommonClass.parseNull((String) hmTransRules.get("INCLUDE_RULE_ENGINE"));
      log.debug("hmTransRules==>" + hmTransRules);
      if ((strOpt != null && !strOpt.equals("")) && hmTransRules != null) {
        // //Access Rule COde/////
        String strOperType = "VERIFY";
        strOperType =
            GmCommonClass.parseNull(request.getParameter("hMode")).equals("") ? strOperType
                : request.getParameter("hMode");
        strOperType = "FN_" + strOperType;
        GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
        log.debug("User ID == " + strUserId);
        strOperType = (String) hmTransRules.get(strOperType);
        log.debug("Request URI == " + strOpt + "_" + strOperType);
        if (strOperType != null) {
          HashMap hmAccess =
              GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                  strOperType));
          strAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
          request.setAttribute("ACCESSFL", strAccess);
          session.setAttribute("ACCESSFL", strAccess);
          if (!strAccess.equals("Y")) {
            throw new AppError("", "20685");
          }
        }
        // ////////////
      }
      if (strAction.equals("Load") && strOpt.equals("Report")) {
        request.setAttribute("hmReturn", setArrayListValues(hmReturn));
        request.setAttribute("hAction", strAction);

        strDispatchTo = strOperPath.concat("/GmItemQuarantineReport.jsp");
      } else if (strAction.equals("Report")) {
        int intType = 0;
        strType = GmCommonClass.parseZero(request.getParameter("Cbo_Type"));
        strPartNum = GmCommonClass.parseNull(request.getParameter("Txt_PartNum"));
        intType = Integer.parseInt(GmCommonClass.parseZero(strType));
        strTransactionId = GmCommonClass.parseNull(request.getParameter("Txt_TxnId"));
        strRefId = GmCommonClass.parseNull(request.getParameter("Txt_RefId"));
        strCreatedBy = GmCommonClass.parseZero(request.getParameter("Cbo_CreatedBy"));
        strStatus = GmCommonClass.parseZero(request.getParameter("Cbo_Status"));
        strControlNum = GmCommonClass.parseNull(request.getParameter("Txt_ControlNum"));
        strEtchId = GmCommonClass.parseNull(request.getParameter("Txt_EtchId"));
        strShowDetails = GmCommonClass.getCheckBoxValue((request.getParameter("chkDetails")));

        log.debug("strShowDetails (Action) = " + strShowDetails);

        hmParam.put("TYPE", strType);
        hmParam.put("FROMDT", strFromDt);
        hmParam.put("TODT", strToDt);
        hmParam.put("PNUM", strPartNum);
        hmParam.put("TXNID", strTransactionId);
        hmParam.put("REFID", strRefId);
        hmParam.put("CREATEDBY", strCreatedBy);
        hmParam.put("STATUS", strStatus);
        hmParam.put("CONTROLNUM", strControlNum);
        hmParam.put("ETCHID", strEtchId);
        hmParam.put("TRANSDETAILS", strShowDetails);
        hmParam.put("DATEFMT", strApplDateFmt);

        log.debug(" TYpe is " + intType + " strEtchId " + strEtchId);
        List lReturn = null;
        if (((intType >= 4110 && intType <= 4116) || intType == 0 || (intType > 400056  && intType != 26241141))&&(intType!=400068 && intType!=400065)) { //400068 is code id for inventory Adjustment to shelf
          rsConsignTrans = gmOper.loadItemQuarantineReport(hmParam);                                                               //400065 is code id for shelf to inventory Adjustment  // 26241141 - loaner recon to QN
          if (lReturn != null) {
            lReturn.addAll(rsConsignTrans.getRows());
          } else {
            lReturn = rsConsignTrans.getRows();
          }
        }
        if (((intType > 4116 && (intType < 400056 || intType == 400085 || intType == 400086 || intType == 26241141)) || !strEtchId        
            .equals("")) || intType == 0 || (intType >= 400076 && intType <= 400085)|| (intType==400068 || intType==400065)){ //26241141 - loaner recon to QN
          rsInHouseTrans = gmLogis.loadInHouseTransactionsReport(hmParam);
          lReturn = rsInHouseTrans.getRows();
        }

        // List lReturn = rsInHouseTrans.getRows();
        log.debug("lReturn = " + lReturn);
        // if(rsConsignTrans != null)
        // {
        // lReturn.addAll(rsConsignTrans.getRows());
        // }

        // log.debug(" Size of InHouse Trans is " + lReturn.size());
        request.setAttribute("alReportData", lReturn);
        // hmReturn.put("REPORT",alInHouseTrans);

        request.setAttribute("hmReturn", setArrayListValues(hmReturn));
        request.setAttribute("hAction", strAction);
        request.setAttribute("hType", strType);
        request.setAttribute("showTransDetails", strShowDetails);

        strDispatchTo = strOperPath.concat("/GmItemQuarantineReport.jsp");
      } else if (strAction.equals("LoadItemShip")) {
        GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
        strConsignId = request.getParameter("hId") == null ? "" : request.getParameter("hId");
        // strConsignId =
        // (String)session.getAttribute("strSessConsignId")==null?"":(String)session.getAttribute("strSessConsignId");
        strType = request.getParameter("hType") == null ? "" : request.getParameter("hType");
        log.debug(" Type is " + strType);

        if (strType.equals("4117") || strType.equals("4118") || strType.equals("40051")
            || strType.equals("40052") || strType.equals("40053") || strType.equals("40054")
            || strType.equals("100069") || strType.equals("100075") || strType.equals("100076")) {
          hmResult = gmLogis.viewInHouseTransDetails(strConsignId);
          hmReturn.put("CONDETAILS", hmResult);
          hmResult = gmLogis.viewInHouseTransItemsDetails(strConsignId, strType);
          hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));

        } else {
          hmResult = gmOper.loadSavedSetMaster(strConsignId);
          hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
          hmResult = gmOper.viewBuiltSetDetails(strConsignId);
          hmReturn.put("CONDETAILS", hmResult);
        }
        log.debug("hmReturn: " + hmReturn);
        // code for location part mapping detail
        if (strType.equals("4116") || strType.equals("4114") || strType.equals("4118")) {
          hmParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
          hmParam.put("VMFILEPATH", "properties.labels.operations.inventory.GmPartLocationMap");
          hmParam.put("TXNTYPE", strType);
          hmParam.put("TXNID", strConsignId);
          alLcnPartMap = gmLocationBean.fetchPartLocationDetails(hmParam);
          hmParam.put("RLIST", alLcnPartMap);
          hmParam.put("TEMPLATE", "GmPartLocationMap.vm");
          hmParam.put("TEMPLATEPATH", "operations/inventory/templates");
          String strGridXmlData = gmLocationBean.getXmlGridData(hmParam);
          // log.debug("strGridXmlData======"+strGridXmlData);
          hmReturn.put("GRIDXMLDATA", strGridXmlData);
        }
        // end...
        alReturn = gmLogon.getEmployeeList();
        hmReturn.put("EMPLIST", alReturn);

        log.debug("hmReturn: " + hmReturn);

        if (strType.equals("4113")) // 4113 Quarantine
        {
          alReturn = gmCommonBean.getLog(strConsignId, strType);
          request.setAttribute("hmLog", alReturn);
        }
        log.debug("After 4113: ");

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "EditVerify");
        request.setAttribute("hConsignId", strConsignId);
        if (strType.equals("4114") || strType.equals("4113") || strType.equals("4116")
            || !strIncludeRuleEng.equalsIgnoreCase("NO")) {
          strTransType = gmRuleBean.fetchTransId(strType, "0");
          log.debug("strTransType = " + strTransType + "..strDispatchTo  = " + strDispatchTo);
          request.setAttribute("RE_FORWARD", "gmInHouseItemConsignVerify");
          request.setAttribute("RE_TXN", strTransType);
          request.setAttribute("TxnTypeID", strType);
          request.setAttribute("txnStatus", "PROCESS");
          strDispatchTo = "/gmRuleEngine.do";
        } else if (!strIncludeRuleEng.equalsIgnoreCase("NO")) {
          log.debug("strTransType = " + strTransType + "..strDispatchTo  = " + strDispatchTo);
          request.setAttribute("RE_FORWARD", "gmInHouseItemConsignVerify");
          request.setAttribute("RE_TXN", "INHOUSETRANS");
          request.setAttribute("TxnTypeID", strType);
          request.setAttribute("txnStatus", "PROCESS");
          strDispatchTo = "/gmRuleEngine.do";
        } else {
          strDispatchTo = strOperPath.concat("/GmInHouseItemConsignVerify.jsp");
        }

      } else if (strAction.equals("Verify")) {

        HashMap hmConsParam = new HashMap();
        ArrayList alSetLoad = new ArrayList();
        strConsignId =
            request.getParameter("hConsignId") == null ? "" : request.getParameter("hConsignId");
        strComments =
            request.getParameter("Txt_Comments") == null ? "" : request
                .getParameter("Txt_Comments");
        strVerifiedBy =
            request.getParameter("Cbo_VerEmp") == null ? "" : request.getParameter("Cbo_VerEmp");
        strType =
            request.getParameter("hConsignType") == null ? "" : request
                .getParameter("hConsignType");
        strLogReason =
            request.getParameter("Txt_LogReason") == null ? "" : request
                .getParameter("Txt_LogReason");
        String strPnumLcn = GmCommonClass.parseNull(request.getParameter("hpnumLcnStr"));
        log.debug("type = " + strType);
        hmConsParam.put("CNID", strConsignId);
        hmConsParam.put("COMMENTS", strComments);
        hmConsParam.put("CNTYPE", strType);
        hmConsParam.put("VERIFYBY", strVerifiedBy);
        hmConsParam.put("USERID", strUserId);
        hmConsParam.put("LOGREASON", strLogReason);
        hmConsParam.put("PNUMLCNSTR", strPnumLcn);
        hmConsParam.put("OPERATION", "93327");
        hmResult = gmLogis.updateReprocessMaterial(hmConsParam);
        if (strType.equals("4117") || strType.equals("4118") || strType.equals("40051")
            || strType.equals("40052") || strType.equals("40053") || strType.equals("40054")
            || strType.equals("100069") || strType.equals("100075") || strType.equals("100076")) {
          hmResult = gmLogis.viewInHouseTransDetails(strConsignId);
          hmReturn.put("CONDETAILS", hmResult);
          hmResult = gmLogis.viewInHouseTransItemsDetails(strConsignId, strType);
          alSetLoad = (ArrayList) hmResult.get("SETLOAD");
          hmReturn.put("SETLOAD", alSetLoad);
        } else {
          hmResult = gmOper.loadSavedSetMaster(strConsignId);
          alSetLoad = (ArrayList) hmResult.get("SETLOAD");
          hmReturn.put("SETLOAD", alSetLoad);
          hmResult = gmOper.viewBuiltSetDetails(strConsignId);
          hmReturn.put("CONDETAILS", hmResult);
        }

        // code for location part mapping detail
        if (strType.equals("4116") || strType.equals("4114") || strType.equals("4118")) {

          hmParam.put("TXNTYPE", strType);
          hmParam.put("TXNID", strConsignId);
          alLcnPartMap = gmLocationBean.fetchPartLocationDetails(hmParam);
          hmParam.put("RLIST", alLcnPartMap);
          hmParam.put("TEMPLATE", "GmPartLocationMap.vm");
          hmParam.put("TEMPLATEPATH", "operations/inventory/templates");
          String strGridXmlData = gmLocationBean.getXmlGridData(hmParam);
          // log.debug("strGridXmlData======"+strGridXmlData);
          hmReturn.put("GRIDXMLDATA", strGridXmlData);
        }
        // end...

        request.setAttribute("hType", strType);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "Verified");
        request.setAttribute("hConsignId", strConsignId);
        if (!strType.equals("")) {
          strTransType = gmRuleBean.fetchTransId(strType, "0");
          if (strTransType.equals("")) {
            strTransType = "INHOUSETRANS";
          }
          if (!strIncludeRuleEng.equalsIgnoreCase("NO")) {
            request.setAttribute("RE_FORWARD", "gmInHouseItemConsignVerify");
            request.setAttribute("RE_TXN", strTransType);
            request.setAttribute("txnStatus", "PROCESS");
            request.setAttribute("TxnTypeID", strType);
            strDispatchTo = "/gmRuleEngine.do";
          } else {
            strDispatchTo = strOperPath.concat("/GmInHouseItemConsignVerify.jsp");
          }
        }
      } else if (strAction.equals("ViewInHouse")) {
        strConsignId = request.getParameter("hId") == null ? "" : request.getParameter("hId");
        request.setAttribute("source", "50183");
        hmResult = gmLogis.viewInHouseTransDetails(strConsignId);
        hmReturn.put("CONDETAILS", hmResult);
        hmResult = gmLogis.viewInHouseTransItemsDetails(strConsignId, strType);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        alReturn = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strConsignId, "1268"));
        if (alReturn.size() > 0) {
          for (int i = 0; i < alReturn.size(); i++) {
            HashMap hmLog = (HashMap) alReturn.get(i);
            strComments += GmCommonClass.parseNull((String) hmLog.get("COMMENTS"));
            strComments += "<BR>";
          }
          request.setAttribute("CRCOMMENTS", strComments);
        }

        String strReForward =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty("PICK_SLIP.ITEM_REFORWARD"));

        if (strReForward.equals("gmItemPicSlip")) {
          strDispatchTo = strCustPath.concat("/GmItemPicSlip.jsp");
        } else {
          strDispatchTo = strCustPath.concat("/GmItemPicSlipGlobal.jsp");
        }

        request.setAttribute("hType", strType);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "ViewInHouse");
        request.setAttribute("hConsignId", strConsignId);
      }
      request.setAttribute("HMPARAM", hmParam);
      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmReprocessMaterialServlet
