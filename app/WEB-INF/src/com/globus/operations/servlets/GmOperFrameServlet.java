/*****************************************************************************
 * File			 : GmOperFrameServlet
 * Desc		 	 : Receive menu which selected from Accts_Header.jsp,then
 *                 store it in session
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;
import com.globus.common.beans.*;
import com.globus.common.servlets.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class GmOperFrameServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);

		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strRootPath = GmCommonClass.getString("GMROOT");
		String strDispatchTo = "";
		String strPageToLoad = "";
		String strSubMenu = "";
		String strFrom = "";
		String strId = "";
		String strMode = "";
		String strType = "";
		
		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
				strFrom = request.getParameter("hFrom");

				if (strFrom != null && strFrom.equals("Dashboard"))
				{
					strId = request.getParameter("hId");
					session.setAttribute("strSessConsignId",strId);
					session.setAttribute("hAction","EditLoadDash");
				}
				else if (strFrom != null && strFrom.equals("VendorReport"))
				{
					strId = request.getParameter("hId");
					session.setAttribute("strSessVendorId",strId);
					session.setAttribute("hAction","EditLoad");
				}
				else if (strFrom != null && strFrom.equals("DashboardDHR"))
				{
					strId = request.getParameter("hId");
					strMode = request.getParameter("hMode");

					session.setAttribute("strSessDHRId",strId);
					session.setAttribute("strSessMode",strMode);
					// Adding the Page details to identify the origin of the request and hence displaying error message accordingly in GmDHRUpdate.jsp
					session.setAttribute("FromPage",strFrom);
					session.setAttribute("hAction","UpdateDHR");
				}
				else if (strFrom != null && strFrom.equals("DashboardReturn"))
				{
					strId = request.getParameter("hId");
					session.setAttribute("strSessReturnId",strId);
					session.setAttribute("hAction","Reprocess");
				}				
				else if (strFrom != null && strFrom.equals("DashboardReturn"))
				{
					strId = request.getParameter("hId");
					session.setAttribute("strSessReturnId",strId);
					session.setAttribute("hAction","EditLoadDash");
				}	
				else if (strFrom != null && strFrom.equals("DashboardNCMR"))
				{
					strId = request.getParameter("hId");
					strMode = request.getParameter("hMode");

					session.setAttribute("strSessNCMRId",strId);
					session.setAttribute("strSessMode",strMode);

					session.setAttribute("hAction","UpdateNCMR");
				}
				else if (strFrom != null && strFrom.equals("ItemDashboard"))
				{
					strId = request.getParameter("hId");
					strType = request.getParameter("hType");
					session.setAttribute("strSessConsignId",strId);
					session.setAttribute("TXN_TYPE",strType);
					session.setAttribute("hAction","EditControl");
				}
				else if (strFrom != null && strFrom.equals("LoadItemShip"))
				{
					strId = request.getParameter("hId");
					session.setAttribute("strSessConsignId",strId);
					session.setAttribute("hAction","LoadItemShip");
				}
				session.removeAttribute("strSessOpt");
				strPageToLoad = request.getParameter("hPgToLoad");
				strSubMenu = request.getParameter("hSubMenu");

				strPageToLoad = (strPageToLoad == null)?"Blank":strPageToLoad;
				strSubMenu = (strSubMenu == null)?"Home":strSubMenu;
				
				session.setAttribute("strSessPgToLoad",strPageToLoad);
				session.setAttribute("strSessSubMenu",strSubMenu);

				gotoPage(strRootPath.concat("/GmOperations.jsp"),request,response);

		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of  GmOperFrameServlet