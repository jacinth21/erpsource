/*****************************************************************************
 * File : GmNCMRServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException; 
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.jms.consumers.beans.GmInitiateVendorJMS;
import com.globus.jms.consumers.beans.GmJIRAProcessBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.logistics.beans.GmTicketBean;


public class GmNCMRServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strDispatchTo = strOperPath.concat("/GmNCMRUpdate.jsp");
 // PC-2521 - Close NCTKT when corresponding Evaluation ID is closed.
    GmTicketBean gmTicketBean = new GmTicketBean();
    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.

      String strAction = request.getParameter("hAction");
      log.debug("strAction : " + strAction);
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");

      }
      strAction = (strAction == null) ? "Load" : strAction;
      GmCommonClass gmCommon = new GmCommonClass();

      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
      
      GmInitiateVendorJMS gmInitiateVendorJMS = new GmInitiateVendorJMS(getGmDataStoreVO());
      GmJIRAProcessBean gmJIRAProcessBean = new GmJIRAProcessBean(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      HashMap hmParam = new HashMap();
      ArrayList alReturn = new ArrayList();
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strMode = "";
      String strNCMRId = "";
      String strDispAction = "";
      String strRtsFl = "";
      String strRespon = "";
      String strRemarks = "";
      String strAuthBy = "";
      String strCloseQty = "";
      String strDispDate = "";
      String strCloseDate = "";
      String strCAPAFl = "";
      String strNCMRList = "";
      String strLogReason = "";
      String strhQtyRej = "";
      String strhDHRId = "";
      String strCompanyInfo = "";
      strNCMRList = GmCommonClass.parseNull(request.getParameter("hNCMRLIST"));
      String strRejectionReason = "";
      log.debug(" NCMR Action is " + strAction);

      if (strAction.equals("PrintNCMR")) // Print Version
      {
        strNCMRId = request.getParameter("hId") == null ? "" : request.getParameter("hId");
        hmReturn = gmOper.getNCMRDetails(strNCMRId, strAction);
        log.debug("hmReturn1--------------"+hmReturn);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        strDispatchTo = strOperPath.concat("/GmNCMRPrint.jsp");
      } else if (strAction.equals("PrintEVAL")) // Print Version
      {
        strNCMRId = request.getParameter("hId") == null ? "" : request.getParameter("hId");
        hmReturn = gmOper.getNCMRDetails(strNCMRId, strAction);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        strDispatchTo = strOperPath.concat("/GmEVALPrint.jsp");
      } else if (strAction.equals("UpdateNCMR")) {
        strNCMRId = request.getParameter("hNCMRId");

        /*
         * if (strNCMRId == null) { strNCMRId = (String)session.getAttribute("strSessNCMRId")== null
         * ?"":(String)session.getAttribute("strSessNCMRId"); }
         */

        strMode = request.getParameter("hMode");
        /*
         * if (strMode == null) { strMode = (String)session.getAttribute("strSessMode")== null
         * ?"":(String)session.getAttribute("strSessMode"); }
         */
        hmReturn = gmOper.getNCMRDetails(strNCMRId, strAction);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hMode", strMode);
        request.setAttribute("hNCMRLIST", strNCMRList);

        // To fecth the Update Log Information
        if (strNCMRList.equals("YES")) {
          alReturn = gmCommonBean.getLog(strNCMRId);
          request.setAttribute("hmLog", alReturn);
        }
        log.debug("strMode------:" + strMode);
      } else if (strAction.equals("A")) {
        strNCMRId = request.getParameter("hNCMRId") == null ? "" : request.getParameter("hNCMRId");
        strDispAction =
            request.getParameter("Cbo_Action") == null ? "" : request.getParameter("Cbo_Action");
        strRtsFl = request.getParameter("Chk_RTS") == null ? "" : request.getParameter("Chk_RTS");
        strRtsFl = strRtsFl.equals("on") ? "1" : "0";
        strRespon =
            request.getParameter("Cbo_Respon") == null ? "" : request.getParameter("Cbo_Respon");
        strRemarks =
            request.getParameter("Txt_Remarks") == null ? "" : request.getParameter("Txt_Remarks");
        strAuthBy =
            request.getParameter("Cbo_AuthEmp") == null ? "" : request.getParameter("Cbo_AuthEmp");
        strCloseQty =
            request.getParameter("Txt_CloseQty") == null ? "" : request
                .getParameter("Txt_CloseQty");
        strDispDate =
            request.getParameter("Txt_Date") == null ? "" : request.getParameter("Txt_Date");
        strRejectionReason =
            request.getParameter("Txt_RejectionReason") == null ? "" : request
                .getParameter("Txt_RejectionReason");
        // strhQtyRej = GmCommonClass.parseNull(request.getParameter("hQtyRej"));
        // strhDHRId = GmCommonClass.parseNull(request.getParameter("hDHRId"));

        hmParam.put("ACTION", strDispAction);
        hmParam.put("RTSFL", strRtsFl);
        hmParam.put("RESPON", strRespon);
        hmParam.put("REMARKS", strRemarks);
        hmParam.put("AUTHBY", strAuthBy);
        hmParam.put("CLOSEQTY", strCloseQty);
        hmParam.put("DISPDT", strDispDate);
        hmParam.put("REJREASON", strRejectionReason);
        // hmParam.put("QTYREJ",strhQtyRej);
        // hmParam.put("DHRID",strhDHRId);

        hmReturn = gmOper.updateNCMRforDisp(strNCMRId, hmParam, strUserId);
        hmReturn = gmOper.getNCMRDetails(strNCMRId, strAction);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "UPD");
        request.setAttribute("hMode", strMode);

        // To fecth the Update Log Information
        if (strNCMRList.equals("YES")) {
          strLogReason =
              request.getParameter("Txt_LogReason") == null ? "" : request
                  .getParameter("Txt_LogReason");
          gmCommonBean.saveLog(strNCMRId, strLogReason, strUserId, "1202");
        }
      } else if (strAction.equals("Q")) {
        strNCMRId = request.getParameter("hNCMRId") == null ? "" : request.getParameter("hNCMRId");
        strhQtyRej = GmCommonClass.parseNull(request.getParameter("hQtyRej"));
        strhDHRId = GmCommonClass.parseNull(request.getParameter("hDHRId"));
        log.debug("strhQtyRej------:" + strhQtyRej);
        log.debug("strhDHRId------:" + strhDHRId);
        hmReturn = gmOper.updateNCMRRejQty(strhDHRId, strhQtyRej, strUserId);
        hmReturn = gmOper.getNCMRDetails(strNCMRId, strAction);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "UPD");
        request.setAttribute("hMode", strMode);
        request.setAttribute("hNCMRLIST", "YES");
        log.debug("strNCMRList:::::::::::::::::::::" + strNCMRList);
        // To fecth the Update Log Information
        if (strNCMRList.equals("YES")) {
          strLogReason =
              request.getParameter("Txt_LogReason") == null ? "" : request
                  .getParameter("Txt_LogReason");
          gmCommonBean.saveLog(strNCMRId, strLogReason, strUserId, "1202");

        }
        alReturn = gmCommonBean.getLog(strNCMRId);
        request.setAttribute("hmLog", alReturn);

      } else if (strAction.equals("C")) {
        strNCMRId = request.getParameter("hNCMRId") == null ? "" : request.getParameter("hNCMRId");
        strCloseDate =
            request.getParameter("Txt_CloseDate") == null ? "" : request
                .getParameter("Txt_CloseDate");
        strCAPAFl =
            request.getParameter("Chk_CAPA") == null ? "" : request.getParameter("Chk_CAPA");
        strCAPAFl = strCAPAFl.equals("on") ? "1" : "0";

        log.debug(" Closeout date is " + strCloseDate);

        hmReturn = gmOper.updateNCMRforCloseout(strNCMRId, strCloseDate, strCAPAFl, strUserId);

        // To fecth the Update Log Information
        if (strNCMRList.equals("YES")) {
          strLogReason =
              request.getParameter("Txt_LogReason") == null ? "" : request
                  .getParameter("Txt_LogReason");
          gmCommonBean.saveLog(strNCMRId, strLogReason, strUserId, "1202");
        }
        hmReturn = gmOper.getNCMRDetails(strNCMRId, strAction);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "UPD");
        request.setAttribute("hMode", "D");
      }
      // To View the Updated NCMR Information
      else if (strAction.equals("P")) {
        log.debug("Inside strAction:" + strAction);
        strNCMRId = request.getParameter("hNCMRId") == null ? "" : request.getParameter("hNCMRId");
        hmReturn = gmOper.getNCMRDetails(strNCMRId, strAction);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "UPD");
        request.setAttribute("hMode", "D");
      }
      // When user click on PendingEval ('Y')
      else if (strAction.equals("UpdateNCMREVAL")) {
        strMode = request.getParameter("hMode");
        strNCMRId = request.getParameter("hNCMRId");
        log.debug("Inside UpdateNCMREVAL conditionnnnnnnnnnnnnnn strMode:" + strMode
            + " strNCMRId:" + strNCMRId);
        request.setAttribute("hAction", strAction);
        hmReturn = gmOper.getNCMRDetails(strNCMRId, strAction);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hMode", strMode);

      } else if (strAction.equals("CreateNCMR")) {
        strMode = request.getParameter("hMode");
        String strEvalRemarks = request.getParameter("hEvalRemarks");
        String strEvalDate = request.getParameter("hEvalDate");
        log.debug("strEvalRemarks:" + strEvalRemarks + " strEvalDate:" + strEvalDate);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hMode", strMode);
        hmParam.put("EVALREMARKS", strEvalRemarks);
        hmParam.put("EVALDATE", strEvalDate);
        hmParam.put("HMODE", strMode);
        strNCMRId = request.getParameter("hNCMRId") == null ? "" : request.getParameter("hNCMRId");
        log.debug("strNCMRId--------------->"+strNCMRId);
        
        
        hmReturn = gmOper.updateEvalToNCMR(strNCMRId, hmParam, strUserId);
        strNCMRId = GmCommonClass.parseNull((String) hmReturn.get("NCMRID"));
        log.debug("strNCMRId" + strNCMRId);
        hmReturn = gmOper.getNCMRDetails(strNCMRId, strAction);
        log.debug("hmReturngetNCMRDetails" + hmReturn);
        strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
        
        if(!strNCMRId.equals("")){
        	hmReturn.put("NCMRID", strNCMRId);
            hmReturn.put("REFTYPE", "26240816"); //26240816 ncmr type
            hmReturn.put("MAILSTATUS", "0"); //to send instant mail
            hmReturn.put("USERID", strUserId);
            hmReturn.put("COMPANYINFO", strCompanyInfo);
        	gmInitiateVendorJMS.initVendorEmailJMS(hmReturn);
        }
     
     // Closing JIRA Ticket when Evaluation processing changes not moving so Commenting below JMS Call 
        hmReturn.put("EVALID", strNCMRId);
	 	hmReturn.put("HACTION", strAction);
	 	hmReturn.put("ISSUETYPE", "CLOSE"); 
	 	hmReturn.put("REFTYPE", "110520"); //NCMR Evaluation 
	 //	gmJIRAProcessBean.createJiraticketForNCMR(hmReturn);
	 // PC-2521 - Close NCTKT when corresponding Evaluation ID is closed.
        gmTicketBean.closeNCMREvalTicket(hmReturn);	 	
        request.setAttribute("hmReturn", hmReturn);
        log.debug("CreateNCMRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR:" + strNCMRId + hmReturn);
      } else if (strAction.equals("Cancel")) {
        strMode = request.getParameter("hMode");
        String strEvalRemarks = request.getParameter("hEvalRemarks");
        String strEvalDate = request.getParameter("hEvalDate");
        // log.debug("strEvalRemarks:"+strEvalRemarks+" strEvalDate:"+strEvalDate);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hMode", strMode);
        hmParam.put("EVALREMARKS", strEvalRemarks);
        hmParam.put("EVALDATE", strEvalDate);
        hmParam.put("HMODE", "X");// x for cancel
        strNCMRId = request.getParameter("hNCMRId") == null ? "" : request.getParameter("hNCMRId");
        log.debug("hmParam:" + hmParam);
        hmReturn = gmOper.updateEvalToNCMR(strNCMRId, hmParam, strUserId);
        // strNCMRId = GmCommonClass.parseNull((String)hmReturn.get("NCMRID"));
        hmReturn = gmOper.getNCMRDetails(strNCMRId, strAction);

       // Closing JIRA Ticket when Evaluation processing changes not moving so Commenting below JMS Call  
        hmReturn.put("EVALID", strNCMRId);
	    hmReturn.put("HACTION", strAction);
	    hmReturn.put("ISSUETYPE", "CLOSE"); 
	    hmReturn.put("REFTYPE", "110520"); //NCMR Evaluation 
//	    gmJIRAProcessBean.createJiraticketForNCMR(hmReturn);
	 // PC-2521 - Close NCTKT when corresponding Evaluation ID is closed.
	    gmTicketBean.closeNCMREvalTicket(hmReturn);
        request.setAttribute("hmReturn", hmReturn);
        log.debug("CreateNCMRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR:" + strNCMRId + hmReturn);
	} 


      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmNCMRServlet
