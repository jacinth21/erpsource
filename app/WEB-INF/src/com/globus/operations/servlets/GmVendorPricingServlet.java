/*****************************************************************************
 * File : GmVendorPricingServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.operations.beans.GmVendorBean;
import com.globus.prodmgmnt.beans.GmProjectBean;


public class GmVendorPricingServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strDispatchTo = "";

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.

      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      GmCommonClass gmCommon = new GmCommonClass();
      GmVendorBean gmVendor = new GmVendorBean(getGmDataStoreVO());
      GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
      HashMap hmReturn = new HashMap();
      HashMap hmTemp = new HashMap();
      hmTemp.put("COLUMN", "ID");

      String strId = "";
      String strPartNum = "";
      String strVendorId = "";
      String strRowId = "";
      String strStatus = "";
      ArrayList alReturn = new ArrayList();
      String strInputString = "";
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strPartyId = (String) session.getAttribute("strSessPartyId");
      String strVendorCostType = "106560";// Default cost type as vendor price
      String strVendorCostFl = "";
      HashMap hmBean = new HashMap();
      HashMap hmAccess = new HashMap();

      strId = request.getParameter("Cbo_Proj") == null ? "" : request.getParameter("Cbo_Proj");
      strPartNum = request.getParameter("Cbo_Part") == null ? "" : request.getParameter("Cbo_Part");
      strVendorId = GmCommonClass.parseNull(request.getParameter("hvendorid"));
      strRowId = GmCommonClass.parseNull(request.getParameter("hrowid"));
      
      // Access flag for standard cost vendor type PMT-51082
      hmAccess =
              GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
                  "STD_COST_VENDOR_ACC"));
      strVendorCostFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      
      //standard cost vendor type access value is Y, Then setting type as Standard cost
      if(strVendorCostFl.equals("Y")){
    	  strVendorCostType = "106561"; // cost type as Standard cost
      }
      
      // to fetch the verified and validated flag from ajax call
      if (strAction.equals("status")) {
        strStatus = gmVendor.fchPartVendorStatus(strPartNum, strVendorId);
        response.setContentType("text/xml");
        PrintWriter pw = response.getWriter();
        pw.write(strStatus + strRowId);
        pw.flush();
        pw.close();
      } else {
        alReturn = gmProject.reportProject(hmTemp);
        hmReturn.put("PROJECTS", alReturn);


        if (!strAction.equals("Load")) {
          alReturn = gmProject.reportPartNumByProject(strId);
          hmReturn.put("PARTNUMS", alReturn);
          if (strAction.equals("Go") || strAction.equals("Save")) {
            if (strAction.equals("Save")) {
              strInputString =
                  request.getParameter("inputString") == null ? "" : request
                      .getParameter("inputString");
              hmBean = gmVendor.saveVendorPriceList(strInputString, strPartNum, strUserId);
            }
            alReturn = gmProject.reportPartNumByProject(strId);
            hmReturn.put("PARTNUMS", alReturn);
            hmBean = gmVendor.getVendorPriceList(strPartNum,strVendorCostType);// PMT-51082 Passing vendor cost type to fetch report by vendorcost 
            hmReturn.put("PRICELIST", hmBean.get("PRICELIST"));
            //PMT-51082 Setting cost type flag value as Y 
            hmBean.put("COST_TYPE_FL", "Y");
            hmBean.put("COST_TYPE", strVendorCostType);
            hmBean = gmVendor.getVendorList(hmBean);
            hmReturn.put("VENDORLIST", hmBean.get("VENDORLIST"));
            alReturn = gmCommon.getCodeList("UOM");
            hmReturn.put("UOMLIST", alReturn);
            strAction = "Go";
          }
        }

        request.setAttribute("hProjId", strId);
        request.setAttribute("hPartNum", strPartNum);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        dispatch(strOperPath.concat("/GmVendorPriceSetup.jsp"), request, response);
      }
    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmVendorPricingServlet
