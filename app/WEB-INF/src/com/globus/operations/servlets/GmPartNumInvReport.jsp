 <%
/**********************************************************************************
 * File		 		: GmPartNumInvReport.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.text.*" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- Imports for Logger -->
<%@ page import="com.globus.common.beans.GmLogger"%>
<%@ page import="com.globus.common.beans.GmCommonConstants"%> 
<%@ page import="org.apache.log4j.Logger"%> 

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);
	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	String strCommonPath = GmCommonClass.getString("GMCOMMON");

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmLoop = new HashMap();
	HashMap hcboVal = new HashMap();

	String strProjId = (String)request.getAttribute("hProjId")==null?"":(String)request.getAttribute("hProjId");
	String strPartNum = (String)request.getAttribute("hPartNum")==null?"":(String)request.getAttribute("hPartNum");
	String strSearch = (String)request.getAttribute("hSearch")==null?"":(String)request.getAttribute("hSearch");
	String strSubComponentFlag = GmCommonClass.parseNull((String)request.getAttribute("hSubComponentFlag")) == "Y" ? "checked" : "" ;
	String strPartDesc = "";
	String strQtyInStock = "";
	String strQtyInShelf = "";
	String strQtyInShelfAlloc = "";
	
	String strQtyInBulk = "";
	String strQtyInWIPSets = "";
	String strQtyInBulkAlloc = "";
	
	String strQtyInQuarantine = "";
	String strQtyInQuarantineAlloc = "";
	
	String strQtyInReturnsHold = "";
	String strQtyInPO = "";
	String strQtyInWIP = "";
	String strQtyInFA = "";
	String strQtyInFAWIP = "";
	String strShelfInAlloc = "";
	String strProjName = "";
	String strRawMatQty = "";
	String strTBE = "";

	String strShade = "";
	String strCodeID = "";
	String strSelected = "";
	int intLoop = 0;

	ArrayList alProject = new ArrayList();
	alProject = (ArrayList)session.getAttribute("PROJLIST");

	ArrayList alSets = new ArrayList();
	ArrayList alReport = new ArrayList();

	if (hmReturn != null) 
	{
		alReport = (ArrayList)hmReturn.get("REPORT");
	}

	int intProjLength = alProject.size();
	int intSetLength = alSets.size();
	if (alReport != null)
	{
		intLoop = alReport.size();
	}
	
	SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy K:m:s a");
	String strDate = (String)df.format(new java.util.Date());
	String strUserName = (String)session.getAttribute("strSessShName");
	String strAccessId = (String)session.getAttribute("strSessAccLvl") ==null?"":(String)session.getAttribute("strSessAccLvl");
	String strDeptId = 	(String)session.getAttribute("strSessDeptId") == null?"":(String)session.getAttribute("strSessDeptId");
	int intAccId = Integer.parseInt(strAccessId);
	boolean viewAllFl = false;
	if (strDeptId.equals("Z") || (strDeptId.equals("O") && intAccId > 3))
	{
		viewAllFl = true;
	}
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Inventory Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>
function fnGo()
{
	var pnum = document.frmAccount.hPartNum.value;
	var viewall = document.frmAccount.hViewAll.value;
	
	if (viewall != 'true')
	{
		if (pnum == '' && document.frmAccount.Cbo_ProjId.value == 0)
		{
			alert("Invalid Search criteria. Either choose a project or enter product family or part number");
			return false;
		}
	}
	
	if (pnum != '')
	{
		document.frmAccount.hOpt.value = "MultipleParts";
		document.frmAccount.Cbo_ProjId.selectedIndex = 0;
	}
	document.frmAccount.hAction.value = "Go";
	document.frmAccount.submit();
}

var prevtr = 0;

function changeTRBgColor(val,BgColor) {
   var obj1 = eval("document.all.tr"+prevtr);
   obj1.style.background = "";
   var obj = eval("document.all.tr"+val);
   prevtr = val;
   obj.style.background = BgColor;
}

function fnViewDetails(pnum,act)
{
	windowOpener("/GmPartInvReportServlet?hAction=DrillDown&hOpt="+act+"&hPartNum="+encodeURIComponent(pnum),"INVPOP","resizable=yes,scrollbars=yes,top=300,left=300,width=710,height=200");
}

function fnPrintDownVer(projid,type)
{
	var pnum = document.frmAccount.hPartNum.value;
	var hopt = '';
	var hsearch = '';
	if (pnum != '')
	{
		hopt = "MultipleParts";
	}
	
	hsearch = document.frmAccount.Cbo_Search.value;
	
	windowOpener("/GmPartInvReportServlet?hAction="+type+"&Cbo_Search="+hsearch+"&hOpt="+hopt+"&hPartNum="+encodeURIComponent(pnum)+"&Cbo_ProjId="+projid,"INVPRINT","resizable=yes,scrollbars=yes,top=300,left=200,width=800,height=490");
}

function fnLoad()
{
	var sel = document.frmAccount.hSearch.value;
	if (sel != '')
	{
		document.frmAccount.Cbo_Search.value = sel;
	}
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10"  onLoad="javascript:fnLoad();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmPartInvReportServlet">
<input type="hidden" name="hId" value="<%=strProjId%>">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="hSearch" value="<%=strSearch%>">
<input type="hidden" name="hViewAll" value="<%=viewAllFl%>">
	<table border="0" width="900" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="20" width="1" class="Line"></td>
			<td colspan="2" height="1" bgcolor="#666666"></td>
			<td rowspan="20" width="1" class="Line"></td>
		</tr>
		<tr>
			<td colspan="2" height="25" class="RightDashBoardHeader">Report - Part Number Inventory	</td>
		</tr>
		<tr><td class="Line" height="1" colspan="2"></td></tr>
		<tr>
			<td class="RightText" align="right" HEIGHT="24">&nbsp;Part Number:</td>
			<td>&nbsp;<input type="text" size="57" value="<%=strPartNum%>" name="hPartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3>
							
			&nbsp;&nbsp;<select name="Cbo_Search" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
							<option value="0" >[Choose One]
							<option value="LIT" >Literal
							<option value="LIKEPRE" >Like - Prefix
							<option value="LIKESUF" >Like - Suffix
						</select>
			&nbsp;&nbsp;<input type="checkbox" name="subComponentFl" <%=strSubComponentFlag%> > Enable Subcomponents 
			</td>
		</tr>			
		<tr>
			<td class="RightText" align="right" HEIGHT="24">&nbsp;Project List:</td>
			<td>&nbsp;<select name="Cbo_ProjId" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
<%
					hcboVal = new HashMap();
			  		for (int i=0;i<intProjLength;i++)
			  		{
			  			hcboVal = (HashMap)alProject.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strProjName = GmCommonClass.getStringWithTM((String)hcboVal.get("NAME"));
						strSelected = strProjId.equals(strCodeID)?"selected":"";
%>
					<option <%=strSelected%> value="<%=strCodeID%>"><%=strCodeID%> / <%=strProjName%></option>
<%
			  		}
%>

			</select>
			&nbsp;
			<input type="button" value="&nbsp;&nbsp;Go&nbsp;&nbsp;" name="Btn_Go" class="button" onClick="javascript:fnGo()" tabindex=3>
<%
			if (intLoop > 0)
			{
%>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value="Print Version" class="Button" onClick="javascript:fnPrintDownVer('<%=strProjId%>','Print');">
			&nbsp;&nbsp;<input type="button" value="Excel Download" class="Button" onClick="javascript:fnPrintDownVer('<%=strProjId%>','Excel');">
<%
			}
%>
			</td>
		</tr>
		<tr><td class="Line" colspan="2"></td></tr>
		<tr>
			<td colspan="2" align="center">
			Report as of: <%=strDate%>   By:<%=strUserName%><BR>
				<div style="overflow:auto; height:400px;">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
 					<thead>
					  <tr bgcolor="#eeeeee" class="RightTableCaption" style="position:relative; top:expression(this.offsetParent.scrollTop);">
						<th HEIGHT="24" align="center" width="60">Part Number</th>
						<th width="400">&nbsp;Description</th>
						<th colspan="3" align="center" class="ShadeDarkGreenTD">FG<BR>INVENTORY</th>
						<th align="center" class="ShadeDarkGrayTD">RM<BR>INVENTORY</th>
						<th colspan="4" align="center" class="ShadeDarkBlueTD">BULK<BR>INVENTORY</th>
						<th width="100" colspan="2" align="center" class="ShadeDarkOrangeTD">QUARANTINE INVENTORY</th>
						<th align="center" class="ShadeDarkYellowTD">QTY<BR>In</th>
						<th width="60" class="ShadeDarkBrownTD">QTY In<br>PO</th>
						<th width="60" align="center" class="ShadeDarkGrayTD">QTY In<br>DHR</th>
					</tr>
					<tr style="position:relative; top:expression(this.offsetParent.scrollTop);">
						<td colspan="2" height="1">
						<td colspan="13" class="Line" height="1"></td>
					</tr>  
					<tr bgcolor="#eeeeee" class="RightTableCaption" style="position:relative; top:expression(this.offsetParent.scrollTop);">
						<th colspan="2">&nbsp;</th>
						<th width="50" align="center" class="ShadeDarkGreenTD">On<br>Shelf</th>
						<th width="50" align="center" class="ShadeDarkGreenTD">Alloc. Out</th>
						<th width="50" align="center" class="ShadeDarkGreenTD">Alloc. In</th>
						<th width="50" align="center" class="ShadeDarkGrayTD">On<br>Shelf</th>
						<th width="50" align="center" class="ShadeDarkBlueTD">WIP Sets</th>
						<th width="50" align="center" class="ShadeDarkBlueTD">TBE Sets</th>
						<th width="50" align="center" class="ShadeDarkBlueTD">Loose</th>
						<th width="50" align="center" class="ShadeDarkBlueTD">Alloc.</th>
						<th width="50" align="center" class="ShadeDarkOrangeTD">Avail.</th>
						<th width="50" align="center" class="ShadeDarkOrangeTD">Alloc.</th>
						<th width="50" align="center" class="ShadeDarkYellowTD">RETURNS HOLD</th>
						<th class="ShadeDarkBrownTD">&nbsp;</th>
						<th class="ShadeDarkGrayTD">&nbsp;</th>
					</tr>
					<tr style="position:relative; top:expression(this.offsetParent.scrollTop);"><td colspan="15" class="Line" height="1"></td></tr>					
				</thead>
				<tbody>
<%
			if (intLoop > 0)
			{
				int intStock = 0;
				int intBulk = 0;
				int intReturns = 0;
				int intPO = 0;
				int intDHR = 0;
				
				for (int i = 0;i < intLoop ;i++ )
				{
					hmLoop = (HashMap)alReport.get(i);
					strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
					strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));

					strQtyInShelf = GmCommonClass.parseZero((String)hmLoop.get("INSHELF"));
					strQtyInShelfAlloc = GmCommonClass.parseZero((String)hmLoop.get("SHELFALLOC"));
					strQtyInShelfAlloc = strQtyInShelfAlloc.equals("0")?strQtyInShelfAlloc:"<a class=RightText href=javascript:fnViewDetails('"+strPartNum+"','SALLOC');>"+strQtyInShelfAlloc+"</a>";
					strShelfInAlloc = GmCommonClass.parseZero((String)hmLoop.get("INV_ALLOC_TO_PACK"));
					strShelfInAlloc = strShelfInAlloc.equals("0")?strShelfInAlloc:"<a class=RightText href=javascript:fnViewDetails('"+strPartNum+"','PALLOC');>"+strShelfInAlloc+"</a>";
					
					strRawMatQty = GmCommonClass.parseZero((String)hmLoop.get("RMQTY"));
					
					strQtyInQuarantine = GmCommonClass.parseZero((String)hmLoop.get("INQUARAN"));
					strQtyInQuarantineAlloc = GmCommonClass.parseZero((String)hmLoop.get("QUARALLOC"));
					strQtyInQuarantineAlloc = strQtyInQuarantineAlloc.equals("0")?strQtyInQuarantineAlloc:"<a class=RightText href=javascript:fnViewDetails('"+strPartNum+"','QALLOC');>"+strQtyInQuarantineAlloc+"</a>";
					
					strQtyInBulk = GmCommonClass.parseZero((String)hmLoop.get("INBULK"));
					strQtyInWIPSets = GmCommonClass.parseZero((String)hmLoop.get("IN_WIP_SET"));
					strQtyInWIPSets = strQtyInWIPSets.equals("0")?strQtyInWIPSets:"<a class=RightText href=javascript:fnViewDetails('"+strPartNum+"','WIPSET');>"+strQtyInWIPSets+"</a>";
					strTBE = GmCommonClass.parseZero((String)hmLoop.get("TBE_ALLOC"));
					strTBE = strTBE.equals("0")?strTBE:"<a class=RightText href=javascript:fnViewDetails('"+strPartNum+"','TBESET');>"+strTBE+"</a>";
					strQtyInBulkAlloc = GmCommonClass.parseZero((String)hmLoop.get("BULKALLOC"));
					strQtyInBulkAlloc = strQtyInBulkAlloc.equals("0")?strQtyInBulkAlloc:"<a class=RightText href=javascript:fnViewDetails('"+strPartNum+"','BALLOC');>"+strQtyInBulkAlloc+"</a>";
					
					strQtyInReturnsHold = GmCommonClass.parseZero((String)hmLoop.get("INRETURNS"));
					strQtyInReturnsHold = strQtyInReturnsHold.equals("0")?strQtyInReturnsHold:"<a class=RightText href=javascript:fnViewDetails('"+strPartNum+"','RHOLD');>"+strQtyInReturnsHold+"</a>";
					
					strQtyInPO = GmCommonClass.parseZero((String)hmLoop.get("POPEND"));
					strQtyInWIP = GmCommonClass.parseZero((String)hmLoop.get("POWIP"));
					strQtyInPO = strQtyInPO.equals("0")?strQtyInPO:"<a class=RightText href=javascript:fnViewDetails('"+strPartNum+"','PO');>"+strQtyInPO+"</a>";
					
%>
						<tr id="tr<%=i%>" onClick="changeTRBgColor(<%=i%>,'#AACCE8');">
							<td height="20" class="RightText">&nbsp;<%=strPartNum%></td>
							<td class="RightTextAS">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
							<td align="center" class="ShadeMedGreenTD"><%=strQtyInShelf%>&nbsp;</td>
							<td align="center" class="ShadeLightGreenTD"><%=strQtyInShelfAlloc%>&nbsp;</td>
							<td align="center" class="ShadeMedGreenTD"><%=strShelfInAlloc%>&nbsp;</td>
							<td class="ShadeLightGrayTD" align="right"><%=strRawMatQty%>&nbsp;</td>
							<td class="ShadeMedBlueTD"><%=strQtyInWIPSets%>&nbsp;</td>
							<td class="ShadeLightBlueTD"><%=strTBE%>&nbsp;</td>
							<td class="ShadeMedBlueTD"><%=strQtyInBulk%>&nbsp;</td>
							<td class="ShadeLightBlueTD"><%=strQtyInBulkAlloc%>&nbsp;</td>
							<td class="ShadeMedOrangeTD"><%=strQtyInQuarantine%>&nbsp;</td>
							<td class="ShadeLightOrangeTD"><%=strQtyInQuarantineAlloc%>&nbsp;</td>
							<td class="ShadeMedYellowTD" align="center"><%=strQtyInReturnsHold%>&nbsp;</td>
							<td class="ShadeMedBrownTD" align="center"><%=strQtyInPO%>&nbsp;</td>
							<td class="ShadeLightGrayTD" align="right"><%=strQtyInWIP%>&nbsp;</td>
						</tr>
						<tr><td colspan="15" bgcolor="#cccccc" height="1"></td></tr>
<%
				}
			} else	{
%>
						<tr>
							<td height="30" colspan="15" align="center" class="RightTextBlue">No data available !</td>
						</tr>
<%
		}
%>
				</tbody>
				</table>
			</div>				
			</td>
		</tr>
		<tr><td class="Line" colspan="2"></td></tr>
	</table>
		
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
