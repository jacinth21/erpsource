/*****************************************************************************
 * File : GmDHRUpdateServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.jms.consumers.beans.GmInitiateVendorJMS;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.beans.GmDHRBean;
import com.globus.operations.beans.GmLogisticsBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.beans.GmVendorBean;
import com.globus.operations.inventory.beans.GmLocationBean;
import com.globus.operations.logistics.beans.GmBackOrderReportBean;
import com.globus.operations.purchasing.beans.GmDHREmailBean;




public class GmDHRUpdateServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strManfPath = GmCommonClass.getString("GMMANF");
    String strDispatchTo = strOperPath.concat("/GmDHRUpdate.jsp");
    HashMap hmDHRDetails = new HashMap();
    String strLoc = "DHRLC";
    try {
      instantiate(request, response);
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.

      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      GmCommonClass gmCommon = new GmCommonClass();
      GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
      GmVendorBean gmVendor = new GmVendorBean(getGmDataStoreVO());
      GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
      GmLocationBean gmLocationBean = new GmLocationBean(getGmDataStoreVO());
      GmLogisticsBean gmLogis = new GmLogisticsBean(getGmDataStoreVO());
      GmInitiateVendorJMS gmInitiateVendorJMS = new GmInitiateVendorJMS(getGmDataStoreVO());
      GmDHREmailBean gmDHREmailBean = new GmDHREmailBean(getGmDataStoreVO());
      HashMap hmReturn = new HashMap();
      HashMap hmParam = new HashMap();
  	  HashMap hmEmailParam = new HashMap();
      ArrayList alReturn = new ArrayList();
      ArrayList alLcnPartMap = new ArrayList();
      String strDHRId = "";
      String strVendorId = "";
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strMode = "";
      String strQtyInpsected = "";
      String strQtyRejected = "";
      String strInspectedBy = "";
      String strQty = "";
      String strUserIdTrans = "";
      String strPartNum = "";
      String strReason = "";
      String strReasonType = "";
      String strSterFl = "";
      String strNCMRId = "";
      Double dbRejCnt = 0.0;
      String strWOId = "";
      String strStatusFl = "";
      String strInputStr = "";
      String strSkipLabelFl = "";
      String strPartSterFl = "";
      String strDHRIds = "";
      String strDhrId = "";
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

      String strUpdStatusFl = ""; // This value will contain if the Updation is happening at
                                  // Inspection / Receiving / Packaging
      String strUDIChkFl = "";
      String strPortal =
          request.getParameter("hPortal") == null ? "" : request.getParameter("hPortal");
      String strRE_TXN = GmCommonClass.parseNull(request.getParameter("RE_TXN"));
      strUDIChkFl = GmCommonClass.parseNull(request.getParameter("Chk_UDI"));
      if (strAction.equals("C")) // For Control Numbers of Sub-Assembly Parts
      {
        strDHRId = request.getParameter("hDHRId") == null ? "" : request.getParameter("hDHRId");
        strInputStr =
            request.getParameter("hInputString") == null ? "" : request
                .getParameter("hInputString");
        hmReturn = gmOper.updateDHRforSubControl(strDHRId, strInputStr, strUserId);

        hmReturn = gmOper.getDHRDetails(strDHRId);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hMode", strAction);
      } else if (strAction.equals("I")) // Inspection
      {
        strDHRId = GmCommonClass.parseNull(request.getParameter("hDHRId"));
        strQtyInpsected = GmCommonClass.parseNull(request.getParameter("Txt_QtyInsp"));
        strQtyRejected = GmCommonClass.parseNull(request.getParameter("Txt_QtyRej"));
        strInspectedBy = GmCommonClass.parseNull(request.getParameter("Cbo_InspEmp"));
        strReasonType = GmCommonClass.parseNull(request.getParameter("Cbo_ReasonType"));
        strUpdStatusFl = GmCommonClass.parseNull(request.getParameter("hStatusFl"));// This value
                                                                                    // will contain
                                                                                    // if the
                                                                                    // Updation is
                                                                                    // happening at
                                                                                    // Inspection /
                                                                                    // Receiving /
                                                                                    // Packaging

        strReason = GmCommonClass.parseNull(request.getParameter("Txt_RejReason"));
        strSterFl = GmCommonClass.parseNull(request.getParameter("hSterFl"));
        String strNCMRFl = GmCommonClass.parseNull(request.getParameter("hNCMRFl"));
        // strStatusFl = strSterFl.equals("1")?"3":"2"; // If Sterilization is required, then skip
        // packaging

        dbRejCnt = Double.parseDouble(strQtyRejected);

        strStatusFl = "2";

        hmParam.put("QTYISNP", strQtyInpsected);
        hmParam.put("QTYREJ", strQtyRejected);
        hmParam.put("INSPBY", strInspectedBy);
        hmParam.put("REJREASONTYPE", strReasonType);
        hmParam.put("REJREASON", strReason);
        hmParam.put("STATUSFL", strStatusFl);
        hmParam.put("STERILEFL", strSterFl);
        hmParam.put("UPDSTATUSFL", strUpdStatusFl);
        hmParam.put("UDICHKFL", strUDIChkFl);

        //PMT-35878 DHR Crossed Inspection Email Notification
        hmReturn = gmOper.updateDHRforInsp(strDHRId, hmParam, strUserId);
     	hmEmailParam.put("REFID", strDHRId);//DHR ID
      	hmEmailParam.put("REFTYPE", "108442");//DHR
      	hmEmailParam.put("REFACTION", "108408");//DHR CROSSED INSPECTION
      	hmEmailParam.put("MAILSTATUS", "0");//
      	hmEmailParam.put("USERID", strUserId);
      	hmEmailParam.put("COMPANYINFO", GmCommonClass.parseNull(request.getParameter("companyInfo")));
      	gmInitiateVendorJMS.initVendorEmailJMS(hmEmailParam);//JMS Call
        if (dbRejCnt > 0) {
          strNCMRId = (String) hmReturn.get("NCMRID");
        }
        hmReturn = gmOper.getDHRDetails(strDHRId);
        ArrayList alReturnReasons = gmCommon.getCodeList("NCRTP");

        alReturn = gmLogon.getEmployeeList();
        hmReturn.put("EMPLIST", alReturn);

        request.setAttribute("hNCMRReason", alReturnReasons);
        request.setAttribute("hNCMRId", strNCMRId);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hMode", strAction);
      } else if (strAction.equals("P") || strAction.equals("V")) // Package
      {
        strDHRId = request.getParameter("hDHRId") == null ? "" : request.getParameter("hDHRId");
        strQty =
            GmCommonClass.parseZero(request.getParameter("Txt_Qty") == null ? "" : request
                .getParameter("Txt_Qty"));
        strUserIdTrans =
            request.getParameter("Cbo_Emp") == null ? "" : request.getParameter("Cbo_Emp");
        strPartNum =
            request.getParameter("hPartNum") == null ? "" : request.getParameter("hPartNum");
        strWOId = request.getParameter("hWOId") == null ? "" : request.getParameter("hWOId");
        strSkipLabelFl =
            request.getParameter("Chk_Skip") == null ? "" : request.getParameter("Chk_Skip");
        strSkipLabelFl = strSkipLabelFl.equals("on") ? "1" : "";
        strSterFl = request.getParameter("hSterFl") == null ? "" : request.getParameter("hSterFl");
        strPartSterFl =
            request.getParameter("hPartSterFl") == null ? "" : request.getParameter("hPartSterFl");
        strPartSterFl = strSterFl.equals("1") ? "" : strPartSterFl;
        strUpdStatusFl = GmCommonClass.parseNull(request.getParameter("hStatusFl"));// This value
                                                                                    // will contain
                                                                                    // if the
                                                                                    // Updation is
                                                                                    // happening at
                                                                                    // Inspection
                                                                                    // (1)/
                                                                                    // Receiving (3)
                                                                                    // /
                                                                                    // Packaging(2)
        String strPnumLcn = GmCommonClass.parseNull(request.getParameter("hpnumLcnStr"));

        String strBQty = GmCommonClass.parseZero(request.getParameter("Txt_BQty"));
        String strRQty = GmCommonClass.parseZero(request.getParameter("Txt_RQty"));
        String strQQty = GmCommonClass.parseZero(request.getParameter("Txt_QQty"));
        String strFQty = GmCommonClass.parseZero(request.getParameter("Txt_FQty"));
        String strRMQty = GmCommonClass.parseZero(request.getParameter("Txt_RMQty"));
        String strInpStr = GmCommonClass.parseNull(request.getParameter("hInpStr"));;
        String strInputString = strQty;
        if (strAction.equals("P"))
          strInputString = strInpStr;// "100065," + strBQty + "|100066," + strRQty + "|100067," +
                                     // strQty + "|100068," + strQQty + "|100069," + strRMQty +
                                     // "|100070," + strFQty + "|";

        hmParam.put("DHRID", strDHRId);
        hmParam.put("PARTNUM", strPartNum);
        hmParam.put("WOID", strWOId);
        hmParam.put("STRINPUTSTRING", strInputString);
        hmParam.put("SKIPLABELFLAG", strSkipLabelFl);
        hmParam.put("PARTSTERFLAG", strPartSterFl);
        hmParam.put("USERIDTRANS", strUserIdTrans);
        hmParam.put("ACTION", strAction);
        hmParam.put("UPDSTATUSFL", strUpdStatusFl);
        hmParam.put("PNUMLCNSTR", strPnumLcn);
        hmParam.put("OPERATION", "93327");
        hmParam.put("USERID", strUserId);
        hmParam.put("UDICHKFL", strUDIChkFl);
        // hmReturn =
        // gmOper.updateDHRforPackVerify(strDHRId,strPartNum,strWOId,strQty,strSkipLabelFl,strPartSterFl,strUserIdTrans,strAction,strUpdStatusFl);
        hmReturn = gmOper.updateDHRforPackVerify(hmParam);
        //get Count for DHR closed
        int strCount =  gmDHREmailBean.getDHRClosedCount(strWOId);
        if (strCount == 0 && strAction.equals("V")) {
        //PMT-35878 Closed Purchase Order Email Notification
     	hmEmailParam.put("REFID", strDHRId);
      	hmEmailParam.put("REFTYPE", "108442");//DHR
      	hmEmailParam.put("REFACTION", "108409");//DHR Closed
      	hmEmailParam.put("USERID", strUserId);
      	hmEmailParam.put("MAILSTATUS", "0");//
      	hmEmailParam.put("COMPANYINFO", GmCommonClass.parseNull(request.getParameter("companyInfo")));
      	gmInitiateVendorJMS.initVendorEmailJMS(hmEmailParam);//JMS Call
        }
        hmReturn = gmOper.getDHRDetails(strDHRId);
        log.debug("hmReturn==>" + hmReturn);
        alReturn = gmLogon.getEmployeeList();
        hmReturn.put("EMPLIST", alReturn);
        if (strAction.equals("V")) {
          strAction = "C";
        }

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hMode", strAction);
      }

      if (strPortal.equals("Manf")) {
        strDispatchTo = strManfPath.concat("/GmManfDHRUpdate.jsp");
      } else {
        strDispatchTo = strOperPath.concat("/GmDHRUpdate.jsp");
      }
      if (strAction.equals("DhrPrintAll")) {
        HashMap hmResult = new HashMap();
        strDHRIds = GmCommonClass.parseNull(request.getParameter("hId"));
        StringTokenizer st = new StringTokenizer(strDHRIds, ",");
        request.setAttribute("source", "50183");
        int i = 0;
        while (st.hasMoreTokens()) {
          strDhrId = st.nextToken().trim();
          StringTokenizer stTok = new StringTokenizer(strDhrId, "$");
          strDhrId = stTok.nextToken();
          String strSource = stTok.nextToken();
          request.setAttribute("source" + i, strSource);
          log.debug("strDhrId" + i + strDhrId);
          log.debug("strSource" + i + strSource);
          hmResult = gmLogis.viewInHouseTransItemsDetails(strDhrId, "");
          hmReturn.put("SETLOAD" + i, hmResult.get("SETLOAD"));
          hmResult = gmLogis.viewInHouseTransDetails(strDhrId);
          hmReturn.put("SETDETAILS" + i, hmResult);
          alReturn.add(hmReturn);
          i++;
        }
        request.setAttribute("ALRETURN", alReturn);
        log.debug("alReturn==>" + alReturn);
        strDispatchTo = strOperPath.concat("/GmDHRPrintAll.jsp");
      }
      if (strPortal.equals("Manf")) {
        strLoc = "MWLC";
      } else if (strPortal.equals("Supply")) {
        strLoc = "SHLC";
      } else {
        strLoc = "DHRLC";
      }
      hmParam.put("DHRID", strDHRId);
      hmParam.put("LOCGRP", strLoc);
      log.debug("strLoc==>" + strLoc);
      GmDHRBean gmDHRBean = new GmDHRBean(getGmDataStoreVO());
      ArrayList alLocations = gmDHRBean.fetchSplitLocations(hmParam);
      HashMap hmParams = new HashMap();
      hmDHRDetails = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("DHRDETAILS"));
      hmParams.put("strAction", strRE_TXN);
      String strRejQty = GmCommonClass.parseZero((String) hmDHRDetails.get("QTYREJ"));
      String strNCMRFl = GmCommonClass.parseZero((String) hmDHRDetails.get("NCMRFL"));

      log.debug("strNCMRFl==>" + strNCMRFl);
      log.debug("strRejQty==>" + strRejQty);

      hmParams.put("strNCMRFl", strNCMRFl);
      hmParams.put("strRejQty", strRejQty);
      hmParams.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
      String strXmlData = getXmlGridData(alLocations, hmParams);
      request.setAttribute("XMLDATA", strXmlData);


      String strInputString = "PART," + hmDHRDetails.get("PNUM") + "|";
      log.debug("strInputString==>" + strInputString);
      GmBackOrderReportBean gmBackOrderReportBean = new GmBackOrderReportBean(getGmDataStoreVO());
      RowSetDynaClass rdResult = null;
      rdResult = gmBackOrderReportBean.loadBODHRReoprt(strInputString, strDHRId);
      request.setAttribute("ldhrPartResult", rdResult.getRows());
      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method

  public String getXmlGridData(ArrayList alParam, HashMap hmParams) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParams.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataList("alReturn", alParam);
    templateUtil.setDataMap("hmParams", hmParams);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.GmLocationDisplay", strSessCompanyLocale));
    templateUtil.setTemplateName("GmLocationDisplay.vm");
    templateUtil.setTemplateSubDir("operations/templates");
    return templateUtil.generateOutput();
  }
}// End of GmDHRUpdateServlet
