/*****************************************************************************
 * File : GmDHRModifyServlet.java Desc :
 * 
 * Version : 1.0 ---------------------------------- Version : 1.1 Author : Basudev T Vidyasankar
 * Date : 12/18/2008
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.beans.GmDHRBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.beans.GmPurchaseBean;
import com.globus.operations.beans.GmVendorBean;

public class GmDHRModifyServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strDispatchTo = strOperPath.concat("/GmDHRModify.jsp");

    // Instantiating the Logger
    GmLogger log = GmLogger.getInstance();

    try {
      instantiate(request, response);
      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      GmCommonClass gmCommon = new GmCommonClass();
      GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
      GmVendorBean gmVendor = new GmVendorBean(getGmDataStoreVO());
      GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmPurchaseBean gmPurc = new GmPurchaseBean(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      HashMap hmParam = new HashMap();
      HashMap hmTemp = new HashMap();
      HashMap hmResult = new HashMap();
      hmTemp.put("CHECKACTIVEFL", "N"); // check for active flag status in vendor list

      ArrayList alReturn = new ArrayList();
      String strDHRId = "";
      String strVendorId = "";
      String strQtyRec = "";
      String strControl = "";
      String strPackSlip = "";
      String strComments = "";
      String strManfDate = "";

      String strUserId = (String) session.getAttribute("strSessUserId");
      String strMode = "";
      String strQtyInpsected = "";
      String strQtyRejected = "";
      String strInspectedBy = "";
      String strQty = "";
      String strUserIdTrans = "";
      String strPartNum = "";
      String strReason = "";
      String strSterFl = "";
      String strNCMRId = "";
      int intRejCnt = 0;
      String strWOId = "";
      String strStatusFl = "";
      String strInputStr = "";
      String strSkipLabelFl = "";
      String strPartSterFl = "";
      String strLogReason = "";
      String strPoId = "";
      String strSkipFl = "";
      String strLotCode = "";
      String strExpiryDate = "";
      String strJulianRule = "";
      String strUDI = "";
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
      GmResourceBundleBean gmResourceBundleBeanlbl =
          GmCommonClass.getResourceBundleBean("properties.labels.operations.GmDHRModify",
              strSessCompanyLocale);
      GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
      String strTodaysDate = GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
      int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
      String strMonthBeginDate =
          GmCalenderOperations.getFirstDayOfMonth(currMonth, getGmDataStoreVO().getCmpdfmt());
      String strSMode = "";
      String strFromDt =
          request.getParameter("hFrmDt") == null ? strMonthBeginDate : request
              .getParameter("hFrmDt");
      String strToDt =
          request.getParameter("hToDt") == null ? strTodaysDate : request.getParameter("hToDt");
      String strSysDate = strTodaysDate;
      strSysDate = strSysDate.equals("") ? request.getParameter("hTodaysDate") : strSysDate;

      if (strAction.equals("Load")) {
        hmReturn = gmVendor.getVendorList(hmTemp);
        session.setAttribute("VENDORLIST", hmReturn.get("VENDORLIST"));
        session.setAttribute("MODE", "EDIT");
        request.setAttribute("hMode", "Report");
        request.setAttribute("hVendId", strVendorId);
        request.setAttribute("hFrmDt", strFromDt);
        request.setAttribute("hToDt", strToDt);
        request.setAttribute("hTodaysDate", strSysDate);
        request.setAttribute("hOpt", "Vendor");
        strDispatchTo = strOperPath.concat("/GmDHRList.jsp");
      } else if (strAction.equals("Rec")) // Load for Receiving Changes
      {
        strDHRId = request.getParameter("hDHRId") == null ? "" : request.getParameter("hDHRId");
        hmReturn = gmOper.getDHRDetails(strDHRId);

        hmTemp = (HashMap) hmReturn.get("DHRDETAILS");
        strPoId = GmCommonClass.parseNull((String) hmTemp.get("POID"));
        strSkipFl = GmCommonClass.parseNull((String) hmTemp.get("SKIPFL"));
        hmResult = gmPurc.viewPO(strPoId, "", "");

        HashMap hmPODetails = (HashMap) hmResult.get("PODETAILS");

        strLotCode = GmCommonClass.parseNull((String) hmPODetails.get("LCODE"));
        strJulianRule =
            GmCommonClass.parseNull(GmCommonClass.getRuleValue("JULIAN_DATE", "LOTVALIDATION"));
        request.setAttribute("JULIANRULE", strJulianRule);
        request.setAttribute("hLotCode", strLotCode);
        request.setAttribute("hSkipFl", strSkipFl);
        request.setAttribute("hTodaysDate", strSysDate);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
      } else if (strAction.equals("UpdateRec")) // Receiving Changes
      {
        strDHRId = request.getParameter("hDHRId") == null ? "" : request.getParameter("hDHRId");
        strQtyRec =
            request.getParameter("Txt_QtyRec") == null ? "" : request.getParameter("Txt_QtyRec");
        strControl =
            request.getParameter("Txt_CNum") == null ? "" : request.getParameter("Txt_CNum");
        strPackSlip =
            request.getParameter("Txt_PkSlip") == null ? "" : request.getParameter("Txt_PkSlip");
        strComments =
            request.getParameter("Txt_Comments") == null ? "" : request
                .getParameter("Txt_Comments");
        strManfDate =
            request.getParameter("hManfDt") == null ? "" : request.getParameter("hManfDt");
        strExpiryDate = GmCommonClass.parseNull(request.getParameter("Txt_ExpDate"));
        /*** Added as part of DHR Issue 1366**Start ***/
        strWOId = request.getParameter("hWOId") == null ? "" : request.getParameter("hWOId");
        strStatusFl =
            request.getParameter("hStatusFl") == null ? "" : request.getParameter("hStatusFl");
        String strPrevQty =
            request.getParameter("hPrevQty") == null ? "" : request.getParameter("hPrevQty");
        strUDI = GmCommonClass.parseNull(request.getParameter("Txt_UDI"));
        /*** Added as part of DHR Issue 1366**End ***/

        hmParam.put("QTYREC", strQtyRec);
        hmParam.put("CONTROL", strControl);
        hmParam.put("PKSLIP", strPackSlip);
        hmParam.put("COMMENTS", strComments);
        hmParam.put("MANFDT", strManfDate);
        hmParam.put("WOID", strWOId);
        hmParam.put("STATUSFL", strStatusFl);
        hmParam.put("EXPDT", strExpiryDate);
        hmParam.put("UDINO", strUDI);
        hmReturn = gmOper.modifyDHRforRec(strDHRId, hmParam, strUserId);
        hmReturn = gmOper.getDHRDetails(strDHRId);
        // Setting the previous quantity to disable/enable the Regenrate button
        hmReturn.put("hPrevQty", strPrevQty);

        strJulianRule =
            GmCommonClass.parseNull(GmCommonClass.getRuleValue("JULIAN_DATE", "LOTVALIDATION"));
        request.setAttribute("JULIANRULE", strJulianRule);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "Rec");
      } else if (strAction.equals("Rollback")) {
        strDHRId = request.getParameter("hDHRId") == null ? "" : request.getParameter("hDHRId");
        log.debug("Roll -- strDHRId==>" + strDHRId);
        hmReturn = gmOper.getDHRDetails(strDHRId);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hMode", strAction);
        // To get the call log information
        alReturn = gmCommonBean.getLog(strDHRId);
        request.setAttribute("hmLog", alReturn);

        strDispatchTo = strOperPath.concat("/GmDHRView.jsp");
      } else if (strAction.equals("UpdateRollback")) // Receiving Changes
      {
        strDHRId = request.getParameter("hDHRId") == null ? "" : request.getParameter("hDHRId");
        // To update the Call Log Information if any
        strLogReason =
            request.getParameter("Txt_LogReason") == null ? "" : request
                .getParameter("Txt_LogReason");


        hmReturn = gmOper.updateDHRforRollback(strDHRId, strAction, strUserId);

        if (!strLogReason.equals("")) {
          gmCommonBean.saveLog(strDHRId, strLogReason, strUserId, "1205");
        }

        hmReturn = gmOper.getDHRDetails(strDHRId);

        HashMap hmDhrDtls = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("DHRDETAILS"));
        strLotCode = GmCommonClass.parseNull((String) hmReturn.get("LCODE"));
        request.setAttribute("hLotCode", strLotCode);
        request.setAttribute("hmReturn", hmReturn);
        String strRollBackMsg = "";
        strSMode = GmCommonClass.parseNull((String) hmDhrDtls.get("SMODE"));
        String lblDhr = GmCommonClass.parseNull(gmResourceBundleBeanlbl.getProperty("LBL_DHR"));
        if (strSMode.equalsIgnoreCase("I")) {// Pending Inspection

          strRollBackMsg =
              lblDhr
                  + strDHRId
                  + GmCommonClass.parseNull(gmResourceBundleBeanlbl
                      .getProperty("LBL_PENDING_INSPECTION"));
        } else {
          strRollBackMsg =
              lblDhr
                  + strDHRId
                  + GmCommonClass.parseNull(gmResourceBundleBeanlbl
                      .getProperty("LBL_PENDING_PROCESSING_STATUS"));
        }
        session.setAttribute("hAppErrorMessage", strRollBackMsg);
        request.setAttribute("hType", "S");
        strDispatchTo = strComnPath.concat("/GmError.jsp");
        // strAction = "Rec";
      }

      else if (strAction.equals("voidDHR")) {
        strDHRId = request.getParameter("hDHRId") == null ? "" : request.getParameter("hDHRId");

        hmReturn = gmOper.getDHRDetails(strDHRId);
        log.debug("hmReturn with getDHRDetails value inside Void DHR is " + hmReturn);

        // To get the call log information
        alReturn = gmCommonBean.getLog(strDHRId);
        log.debug("Size of alReturn with call Log details inside Void DHR is " + alReturn.size());

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hMode", strAction);
        request.setAttribute("hmLog", alReturn);
        strDispatchTo = strOperPath.concat("/GmDHRView.jsp");
      }

      else if (strAction.equals("UpdateVoid")) // Receiving Changes
      {
        strDHRId = request.getParameter("hDHRId") == null ? "" : request.getParameter("hDHRId");
        log.debug("strDHRId: " + strDHRId);

        // To update the Call Log Information if any
        strLogReason =
            request.getParameter("Txt_LogReason") == null ? "" : request
                .getParameter("Txt_LogReason");


        if (!strLogReason.equals("")) {
          gmCommonBean.saveLog(strDHRId, strLogReason, strUserId, "1205");
        }
        // To get the call log information
        alReturn = gmCommonBean.getLog(strDHRId);
        request.setAttribute("hmLog", alReturn);
        log.debug("Size of alReturn with call Log details inside Update Void is " + alReturn.size());

        hmReturn = gmOper.updateDHRforRollback(strDHRId, strAction, strUserId);
       
        String p_inv_void_fl = (String) hmReturn.get("VOIDFL");

        HashMap hmEmailDetails = new HashMap();
        hmEmailDetails = (HashMap) hmReturn.get("EMAILDETAILS");

        hmReturn = gmOper.getDHRDetails(strDHRId);
        log.debug("hmReturn with getDHRDetails value inside Update Void is " + hmReturn);

        // Create email to account showing voided DHR
        if (p_inv_void_fl != null) {
          // Send email to Accounting
          HashMap hmParams = new HashMap();
          hmParams.put("DHRID", strDHRId);
          hmParams.put("INVVDFL", p_inv_void_fl);
          hmParams.put("LOGREASON", strLogReason);
       

          
          sendEmail(hmEmailDetails, hmParams);
        }

        request.setAttribute("hMode", strAction);
        request.setAttribute("hmReturn", hmReturn);
        String strVoidMsg = GmCommonClass.parseNull(gmResourceBundleBeanlbl
            .getProperty("LBL_VOID_DHR")) + strDHRId + GmCommonClass.parseNull(gmResourceBundleBeanlbl
                .getProperty("LBL_PERFOMED"));
        session.setAttribute("hAppErrorMessage", strVoidMsg);
        request.setAttribute("hType", "S");
        strDispatchTo = strComnPath.concat("/GmError.jsp");
        // strAction = "Load";
      }
      String strLoc = "DHRLC";
      if (strDHRId.indexOf("MWO") > 0) {
        strLoc = "MWLC";
      } else if (strDHRId.indexOf("SHR") > 0) {
        strLoc = "SHLC";
      }
      log.debug("strLoc==>" + strLoc + " strDHRId==>" + strDHRId + "-" + strDHRId.indexOf("MWO")
          + "-" + strDHRId.indexOf("SHR"));
      request.setAttribute("hAction", strAction);
      hmParam.put("DHRID", strDHRId);
      hmParam.put("LOCGRP", strLoc);
      GmDHRBean gmDHRBean = new GmDHRBean(getGmDataStoreVO());
      ArrayList alLocations = gmDHRBean.fetchSplitLocations(hmParam);

      String strXmlData = getXmlGridData(alLocations, strSessCompanyLocale);
      request.setAttribute("XMLDATA", strXmlData);
      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      //log.error("Exception is " + e);
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method

  private void sendEmail(HashMap hmEmailDetails, HashMap hmParams) throws Exception {
    StringBuilder sbMessage = new StringBuilder();
    GmEmailProperties gmEmailProperties = new GmEmailProperties();
    String strDHRId = GmCommonClass.parseNull((String) hmParams.get("DHRID"));
    String p_inv_void_fl = GmCommonClass.parseNull((String) hmParams.get("INVVDFL"));
    String strLogReason = GmCommonClass.parseNull((String) hmParams.get("LOGREASON"));;

    // Add the following values to string buffer by getting from hash map
    // Get FROM, TO, CC and Subject from EmailTemplate.properties
    String strEmailFileName = "GmDHRInvoiceRefersalEMail.";
    String strTo = GmCommonClass.getEmailProperty(strEmailFileName + GmEmailProperties.TO);
    String strCC = GmCommonClass.getEmailProperty(strEmailFileName + GmEmailProperties.CC);
    String strFrom = GmCommonClass.getEmailProperty(strEmailFileName + GmEmailProperties.FROM);
    String strSubject =
        GmCommonClass.getEmailProperty(strEmailFileName + GmEmailProperties.SUBJECT);

    String strVendorName = GmCommonClass.parseNull((String) hmEmailDetails.get("VNAME"));
    String strPONumber = GmCommonClass.parseNull((String) hmEmailDetails.get("POID"));
    String strPartNumber = GmCommonClass.parseNull((String) hmEmailDetails.get("PARTNM"));
    String strInvoiceID = GmCommonClass.parseNull((String) hmEmailDetails.get("INVID"));
    String strVoidedBy = GmCommonClass.parseNull((String) hmEmailDetails.get("VOIDBY"));
    String strVoidedDate = GmCommonClass.parseNull((String) hmEmailDetails.get("VOIDDT"));
    String strQty = GmCommonClass.parseNull((String) hmEmailDetails.get("QTY"));
    String strPOInitiatedEmail =
        GmCommonClass.parseNull((String) hmEmailDetails.get("PO_INITIATED_EMAIL"));

    if (!strPOInitiatedEmail.equals("")) {// Here we are attaching the mail Id in CC for the user
                                          // who is raising PO.
      strCC += "," + strPOInitiatedEmail;
    }


    // Build Message Body
    strSubject = GmCommonClass.replaceAll(strSubject, "#<VENDOR_NAME>", strVendorName);
    sbMessage.append("Please be advised that a DHR is ");
    sbMessage.append("voided by the receiving team, the ");
    sbMessage.append("following is the information of the ");
    sbMessage.append("voided record.\n\n");
    sbMessage.append("Vendor Name: " + strVendorName + "\t\n");
    sbMessage.append("P.O. # : " + strPONumber + "\t\n");
    sbMessage.append("DHR # : " + strDHRId + "\t\n");
    sbMessage.append("Part # : " + strPartNumber + "\t\n");
    sbMessage.append("Qty Reversed: " + strQty + "\t\n");
    sbMessage.append("Invoice Id : " + strInvoiceID);
   	
    if (p_inv_void_fl.equals("Y")) {
      sbMessage.append("  ( This invoice is voided as there is ");
      sbMessage.append("no other DHR associated to this ");
      sbMessage.append("invoice )");
    }
    sbMessage.append("\t\nVoided By : " + strVoidedBy + "\t\n");
    sbMessage.append("Voided Date: " + strVoidedDate+ "\t\n");
    sbMessage.append("Void Comments: " + strLogReason);
    
    // Set email properties
    gmEmailProperties.setSender(strFrom);
    gmEmailProperties.setRecipients(strTo);
    gmEmailProperties.setCc(strCC);
    gmEmailProperties.setSubject(strSubject);
    gmEmailProperties.setMessage(sbMessage.toString());

    // Send Email
    GmCommonClass.sendMail(gmEmailProperties);
  }

  public String getXmlGridData(ArrayList alParam, String strSessCompanyLocale) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alReturn", alParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.GmLocationDisplay", strSessCompanyLocale));
    templateUtil.setTemplateName("GmLocationDisplay.vm");
    templateUtil.setTemplateSubDir("operations/templates");
    return templateUtil.generateOutput();
  }
}// End of GmDHRModifyServlet
