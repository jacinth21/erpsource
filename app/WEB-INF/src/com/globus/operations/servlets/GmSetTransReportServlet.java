/*****************************************************************************
 * File : GmSetTransReportServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.operations.beans.GmOperationsBean;

public class GmSetTransReportServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strDispatchTo = strOperPath.concat("/GmSetTransReport.jsp");

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      GmCommonClass gmCommon = new GmCommonClass();
      GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      HashMap hmResult = new HashMap();
      ArrayList alReturn = new ArrayList();
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strAppDateFmt =
          GmCommonClass.parseNull((String) session.getAttribute("strSessApplDateFmt"));
      String strFromDate = "";
      String strToDate = "";
      String strSetId = "";
      String strMode = "";
      GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
      int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
      String strTodaysDate = GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
      String strCountryCode = GmCommonClass.parseNull(GmCommonClass.countryCode);
      String strMonthBeginDate =
          GmCalenderOperations.getFirstDayOfMonth(currMonth, getGmDataStoreVO().getCmpdfmt());
      /*
       * if (strCountryCode.equals("en")) { strMonthBeginDate = strTodaysDate.replaceAll("/\\d\\d/",
       * "/01/"); } else { strMonthBeginDate = strTodaysDate.replace("\\d\\d/", "01/"); }
       */

      if (strAction.equals("Load") || strAction.equals("Reload")) {
        if (strAction.equals("Load")) {
          strFromDate = strMonthBeginDate;
          strToDate = strTodaysDate;
        } else {
          strFromDate =
              request.getParameter("Txt_FrmDt") == null ? "" : (String) request
                  .getParameter("Txt_FrmDt");
          strToDate =
              request.getParameter("Txt_ToDt") == null ? "" : (String) request
                  .getParameter("Txt_ToDt");
        }

        log.debug("strFromDate===========" + strFromDate);
        log.debug("strToDate===========" + strToDate);
        request.setAttribute("hFrmDt", strFromDate);
        request.setAttribute("hToDt", strToDate);
        hmResult = gmOper.loadSetConsignAndReturnReport(strFromDate, strToDate);
        request.setAttribute("hmReturn", hmResult);
      } else if (strAction.equals("LoadSet")) {
        strSetId =
            request.getParameter("hSetId") == null ? "" : (String) request.getParameter("hSetId");
        strMode =
            request.getParameter("hMode") == null ? "SET" : (String) request.getParameter("hMode");

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hSetId", strSetId);
        request.setAttribute("hMode", strMode);
      }

      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmSetTransReportServlet
