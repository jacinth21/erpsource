/*****************************************************************************
 * File : GmPartInvReportServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.operations.beans.GmInvReportBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.beans.GmPurchaseBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.common.beans.GmResourceBundleBean;

public class GmPartInvReportServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strInvPath = GmCommonClass.getString("GMINVENTORY");
    String strDispatchTo = strOperPath.concat("/GmPartNumInvReport.jsp");
    HashMap hmParams = new HashMap();

    try {
      // checkSession(response, session); // Checks if the current session
      // is valid, else redirecting to
      // SessionExpiry page
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code
      // to
      // Initialize
      // the
      // Logger
      // Class.
     String strPartNs = GmCommonClass.parseNull(request.getParameter("hPartNum"));
     String strSetIds = GmCommonClass.parseNull(request.getParameter("hSetId"));
     String strProjIds = GmCommonClass.parseNull(request.getParameter("Cbo_ProjId"));
     String strSetName = "";
     String strSessCompanyLocale =
    	        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    GmResourceBundleBean gmResourceBundleBeanlbl =
    	        GmCommonClass.getResourceBundleBean("properties.labels.operations.GmPartNumInvReport",
    	            strSessCompanyLocale);
     String strTitle = "";
     //Load the screen title based on Set Id, Part number and Project
     if((strPartNs != "" && strPartNs != null)&& (strSetIds == "" || strSetIds == null) && (strProjIds == "" || strProjIds == null)){
    	 strTitle = GmCommonClass.parseNull(gmResourceBundleBeanlbl.getProperty("LBL_REPORT_BY_PART"));
     }
     else if((strSetIds != "" && strSetIds != null)&& (strPartNs == "" || strPartNs == null) && (strProjIds == "" || strProjIds == null)){
    	 strTitle = GmCommonClass.parseNull(gmResourceBundleBeanlbl.getProperty("LBL_REPORT_BY_SETID"));
     }
     else if((strProjIds != "" && strProjIds != null)&& (strPartNs == "" || strPartNs == null) && (strSetIds == "" || strSetIds == null)){
    	 strTitle = GmCommonClass.parseNull(gmResourceBundleBeanlbl.getProperty("LBL_REPORT_BY_PROJID"));
     }else{
    	 strTitle = GmCommonClass.parseNull(gmResourceBundleBeanlbl.getProperty("LBL_REPORT_COMMON"));
    	      }
      String strAction = request.getParameter("hAction");
      log.debug(" action from req " + strAction);
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strOpt = request.getParameter("hOpt");
      String strType = request.getParameter("Type");
      log.debug("strType " + strType);
      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;

      GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
      GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
      GmPurchaseBean gmPurc = new GmPurchaseBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmInvReportBean gmInv = new GmInvReportBean(getGmDataStoreVO());
      GmCommonClass gmCommonClass = new GmCommonClass();
      GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
      HashMap hmReturn = new HashMap();
      HashMap hmTemp = new HashMap();
      HashMap hmExcludedBuckets = new HashMap();
      HashMap hmBucketValues = new HashMap();
      HashMap hmIncludeValues = new HashMap();
      HashMap hmInvColumns = new HashMap();
      hmTemp.put("COLUMN", "ID");
      hmTemp.put("STATUSID", "20301"); // Filter all approved projects
      ArrayList alReturn = new ArrayList();
      ArrayList alLog = new ArrayList();

      String strUserId = (String) session.getAttribute("strSessUserId");
      String strProjId = "";
      String strProjNm = "";
      String strPartNum = "";
      String strSearch = "";
      String strPartNumFormat = "";
      String strQtyAdj = "";
      String strInvLockId = "";
      String strInvTypes = "";
      String strInvViewType = GmCommonClass.parseNull(request.getParameter("Inv_View"));
      String strDefaultSelectedCols = "";
      strDefaultSelectedCols = GmCommonClass.parseNull(request.getParameter("hIncludedColumns"));
      // While loading the Main-inv rpt from Trend rpt drilldown action(strOpt : MultipleParts), the
      // view type should have default columns.
      if (strOpt.equals("Main") || strOpt.equals("MultipleParts")
          && strDefaultSelectedCols.equals("")) {
        strInvViewType = "50001";
      }
      // if view type is empty and default columns is empty then we can set it as a inventory type
      // is CS.
      if (strInvViewType.equals("") && strDefaultSelectedCols.equals("")) {
        strInvViewType = "50001";
      }

      String strIncludedInvColumns = "";
      String strExcludedInvColumns = "";
      String strExcludedInvMapping = "";
      String strSetId = "";
      log.debug(" action is " + strAction + " opt is " + strOpt);

      alReturn = gmProj.reportProject(hmTemp);
      session.setAttribute("PROJLIST", alReturn);

      ArrayList alAllInventory = new ArrayList();
      ArrayList alTempAllInventory = new ArrayList();
      ArrayList alDefaultInvColumns = new ArrayList();
      ArrayList alSelectedinvColumns = new ArrayList();

      alAllInventory = gmAutoCompleteReportBean.getCodeList("MANINV", getGmDataStoreVO()); 

      List alSelectedColsList = new ArrayList(Arrays.asList(strDefaultSelectedCols.split(",")));
      if (!strInvViewType.equals("")) {
        alDefaultInvColumns =
            GmCommonClass.parseNullArrayList(gmCommonClass.getCodeList("MANINV", strInvViewType));
      } else {
        alDefaultInvColumns = (ArrayList) alSelectedColsList;
      }
      if (!strAction.equals("DrillDown")) {
        hmInvColumns.put("DEFAULTCOLS", alDefaultInvColumns);
        hmInvColumns.put("INVTYPE", strInvViewType);
        hmIncludeValues = gmInv.getInventoryBuckets(hmInvColumns);
        strIncludedInvColumns =
            GmCommonClass.parseNull((String) hmIncludeValues.get("INCLUDEINVENTORY"));
        strExcludedInvColumns =
            GmCommonClass.parseNull((String) hmIncludeValues.get("EXCLUDEDINVNTORY"));
      }

      request.setAttribute("INCLUDEINV", strIncludedInvColumns);
      request.setAttribute("EXCLUDEINV", strExcludedInvColumns);
      request.setAttribute("INVENTORY", alAllInventory);
      request.setAttribute("DEFAULTINVENTORY", alDefaultInvColumns);
      request.setAttribute("TITLE", strTitle);
      // ///
      if (strAction.equals("Load")) {
        if (strOpt.equals("Bulk")) {
          strDispatchTo = strOperPath.concat("/GmPartNumBulkInvReport.jsp");
        } else if (strOpt.equals("Manual")) {
          strDispatchTo = strOperPath.concat("/GmPartNumInvStatus.jsp");
          request.setAttribute("hOpt", strOpt);
          request.setAttribute("hAction", strAction);
        } else if (strOpt.equals("LockInv")) {
          hmReturn = gmInv.loadInvLockReportLists(strType);
          strDispatchTo = strInvPath.concat("/GmPartNumLockInvReport.jsp");
          request.setAttribute("hOpt", strOpt);
          request.setAttribute("hAction", strAction);
          request.setAttribute("hmReturn", hmReturn);
          request.setAttribute("Type", strType);
        }
      }

      // //////

      else if (strAction.equals("Go") || strAction.equals("Print") || strAction.equals("Excel")
          || strAction.equals("GoBulk")) {
        strPartNum = GmCommonClass.parseNull(request.getParameter("hPartNum"));
        strProjId = GmCommonClass.parseNull(request.getParameter("Cbo_ProjId"));
        strProjNm = GmCommonClass.parseNull(request.getParameter("searchCbo_ProjId"));
        strSearch = GmCommonClass.parseNull(request.getParameter("Cbo_Search"));
        String strSubComponentFlag =
            GmCommonClass.getCheckBoxValue(request.getParameter("subComponentFl"));
        //PMT-32321 Expand All/ Collapse all functionality in Pending - By Vendor screen
        strSubComponentFlag = strSubComponentFlag.equals("") ? GmCommonClass.parseNull(request.getParameter("subComponentFl")) : strSubComponentFlag;
        String strEnableObsolete =
            GmCommonClass.getCheckBoxValue(request.getParameter("enableObsolete"));
        String strPageType = GmCommonClass.parseNull(request.getParameter("hPageType"));
        String strScreenType = GmCommonClass.parseNull(request.getParameter("hScreenType"));
        String strIncInvColumns = GmCommonClass.parseNull(request.getParameter("hIncludedColumns"));
        String strExcInvColumns = GmCommonClass.parseNull(request.getParameter("hExcludedColumns"));
        strSetId = GmCommonClass.parseNull(request.getParameter("hSetId"));
        HashMap hmParam = new HashMap();
        hmParam.put("SRTYPE", strScreenType);
        strPartNumFormat = strPartNum;
        if(strSearch.equals("LIT")){
			strPartNumFormat = ("^").concat(strPartNumFormat);
			strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|^");
			strPartNumFormat = strPartNumFormat.concat("$");	
		}else if (strSearch.equals("LIKEPRE")) {
          strPartNumFormat = ("^").concat(strPartNumFormat);
          strPartNumFormat = strPartNumFormat.replaceAll(",", "|^");
        } else if (strSearch.equals("LIKESUF")) {
          strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|");
          strPartNumFormat = strPartNumFormat.concat("$");
        } else if (strSearch.equals("") && strOpt.equals("Manual")) {
          strPartNumFormat = strPartNumFormat.concat("$");
        } else if (strScreenType.equals("BACKORDER")) {
          strPartNumFormat = strPartNumFormat;
        } else {
          strPartNumFormat = strPartNumFormat.replaceAll(",", "|");
        }

        hmBucketValues.put("INCLUDEINVENTORY", strIncludedInvColumns);
        hmBucketValues.put("EXCLUDEINVENTORY", strExcludedInvColumns);
        if (!strExcludedInvColumns.equals("")) {
          hmExcludedBuckets = gmInv.fetchInventoryExcludemapping(hmBucketValues);
          strExcludedInvMapping =
              gmCommonClass.parseNull((String) hmExcludedBuckets.get("EXCLUDEDINVMAPPING"));
        }
        if(!strSetId.equals("")){
            strSetName = gmInv.getSetName(strSetId);
        }

        hmParam.put("PNUM", strPartNumFormat);
        hmParam.put("PROJID", strProjId);
        hmParam.put("PROJNM", strProjNm);
        hmParam.put("SUBCOMPONENTFL", strSubComponentFlag);
        hmParam.put("ENABLEOBSOLETE", strEnableObsolete);
        hmParam.put("EXCLUDEDINVENTORY", strExcludedInvMapping);
        hmParam.put("SETID", strSetId);
        log.debug(" PNUM b4 is " + strPartNum + " after is " + strPartNumFormat
            + " strSubComponentFlag " + strSubComponentFlag);

        alReturn = gmInv.getPartNumInvReport(hmParam);
        log.debug("alReturn===>" + alReturn);
        if (strOpt.equals("PartNum") || strOpt.equals("Manual")) {
          strDispatchTo = strOperPath.concat("/GmPartNumInvStatus.jsp");
          // To get Log information
          alLog = gmCommonBean.getLog(strPartNum, "1205");
          request.setAttribute("hmLog", alLog);
        }

        log.debug(" Project Id is " + strProjId);
        hmReturn.put("REPORT", alReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hProjId", strProjId);
        request.setAttribute("hProjNm", strProjNm);
        request.setAttribute("hPartNum", strPartNum);
        request.setAttribute("hSubComponentFlag", strSubComponentFlag);
        request.setAttribute("hEnableObsolete", strEnableObsolete);

        request.setAttribute("hPageType", strPageType);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hOpt", strOpt);
        request.setAttribute("hSearch", strSearch);
        request.setAttribute("Type", strType);
        // While loading the Main-inv rpt from Trend rpt drilldown action(strOpt :
        // MultipleParts),strIncInvColumns is NULL.
        // That time the Main-inv rpt should load with default columns.
        strIncInvColumns = strIncInvColumns.equals("") ? strIncludedInvColumns : strIncInvColumns;
        request.setAttribute("INCLUDEINV", strIncInvColumns);
        request.setAttribute("EXCLUDEINV", strExcInvColumns);
        request.setAttribute("hSetId", strSetId);
        request.setAttribute("hSetName", strSetName);
        if (strAction.equals("Print")) {
          strDispatchTo = strOperPath.concat("/GmPartNumInvPrint.jsp");
        } else if (strAction.equals("Excel")) {
          response.setContentType("application/vnd.ms-excel");
          response.setHeader("Content-disposition", "attachment;filename=InventoryReport.xls");
          strDispatchTo = strOperPath.concat("/GmPartNumInvExcel.jsp");
        } else if (strAction.equals("GoBulk")) {
          strDispatchTo = strOperPath.concat("/GmPartNumBulkInvReport.jsp");
        }
      } else if (strAction.equals("DrillDown")) {
        strPartNum = GmCommonClass.parseNull(request.getParameter("hPartNum"));
        alReturn = gmPurc.getPartNumInvDetails(strPartNum, strOpt);
        hmReturn.put("REPORT", alReturn);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hOpt", strOpt);
        log.debug("hOpt==>" + strOpt);
        strDispatchTo = strOperPath.concat("/GmInvDetailsPopup.jsp");
      }
      /*
       * else if (strAction.equals("Print") || strAction.equals("Excel")) { strProjId =
       * request.getParameter("hProjId")== null ?"":request.getParameter("hProjId"); strSearch =
       * request.getParameter("hSearch")== null ?"":request.getParameter("hSearch");
       * 
       * alReturn = gmInv.getPartNumInventoryByProject(strProjId); hmReturn.put("REPORT",alReturn);
       * request.setAttribute("hmReturn",hmReturn); request.setAttribute("hAction",strAction); if
       * (strAction.equals("Print")) { strDispatchTo = strOperPath.concat("/GmPartNumInvPrint.jsp");
       * }else { response.setContentType("application/vnd.ms-excel");
       * response.setHeader("Content-disposition","attachment;filename=InventoryReport.xls");
       * strDispatchTo = strOperPath.concat("/GmPartNumInvExcel.jsp"); } }
       */
      else if (strAction.equals("Adjust")) {
        strPartNum = GmCommonClass.parseNull(request.getParameter("hPartNum"));
        strQtyAdj =
            request.getParameter("Txt_QtyAdj") == null ? "" : request.getParameter("Txt_QtyAdj");
        alReturn = gmOper.savePartNumManAdjust(strPartNum, strQtyAdj, strUserId);

        // To save Log Information if any
        String strLogReason =
            request.getParameter("Txt_LogReason") == null ? "" : request
                .getParameter("Txt_LogReason");

        if (!strLogReason.equals("")) {
          gmCommonBean.saveLog(strPartNum, strLogReason, strUserId, "1205");
        }
        // To get Log information
        alLog = gmCommonBean.getLog(strPartNum);
        request.setAttribute("hmLog", alLog);

        strDispatchTo = strOperPath.concat("/GmPartNumInvStatus.jsp");
        hmReturn.put("REPORT", alReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hPartNum", strPartNum);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hOpt", strOpt);
        request.setAttribute("Type", strType);
      } else if (strAction.equals("ReloadLock")) {
        strPartNum = GmCommonClass.parseNull(request.getParameter("hPartNum"));
        strProjId = GmCommonClass.parseNull(request.getParameter("Cbo_ProjId"));
        strProjNm = GmCommonClass.parseNull(request.getParameter("searchCbo_ProjId"));
        strSearch = GmCommonClass.parseNull(request.getParameter("Cbo_Search"));
        strInvLockId = GmCommonClass.parseNull(request.getParameter("Cbo_InvId"));
        strInvTypes = GmCommonClass.parseNull(request.getParameter("hInvTypes"));

        hmParams.put("PARTNUM", strPartNum);
        hmParams.put("SEARCH", strSearch);
        strPartNumFormat = GmCommonClass.createRegExpString(hmParams);

        hmReturn = gmInv.loadInvLockReportLists(strType);

        hmTemp = gmInv.getInvLockReport(strInvLockId, strInvTypes, strPartNumFormat, strProjId);

        strDispatchTo = strInvPath.concat("/GmPartNumLockInvReport.jsp");
        request.setAttribute("hOpt", strOpt);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hPartNum", strPartNum);
        request.setAttribute("hSearch", strSearch);
        request.setAttribute("hProjId", strProjId);
        request.setAttribute("hProjNm", strProjNm);
        request.setAttribute("hInvId", strInvLockId);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hmReport", hmTemp);
        request.setAttribute("Type", strType);
      }

      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmPartInvReportServlet
