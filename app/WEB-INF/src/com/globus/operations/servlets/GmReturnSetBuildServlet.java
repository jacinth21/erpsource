/*****************************************************************************
 * File : GmReturnSetBuildServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.beans.GmLogisticsBean;
import com.globus.operations.beans.GmOperationsBean;

public class GmReturnSetBuildServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                 // Class.

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strReturnsPath = GmCommonClass.getString("GMRETURNS");

    String strDispatchTo = strReturnsPath.concat("/GmReturnAccept.jsp");



    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strOpt = request.getParameter("hOpt");

      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;

      GmOperationsBean gmOper = new GmOperationsBean();
      GmCustomerBean gmCust = new GmCustomerBean();
      GmLogisticsBean gmLogis = new GmLogisticsBean();

      HashMap hmReturn = new HashMap();
      HashMap hmResult = new HashMap();
      HashMap hmParam = new HashMap();
      ArrayList alReturn = new ArrayList();
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strPartNum = "";
      String strQty = "";
      String strControl = "";
      String strOrgControl = "";
      String strCount = "";
      String strRAId = "";
      String strRetDate = "";
      String strFlag = "";
      String strCNStr = "";
      String strQNStr = "";
      String strPNStr = "";
      String strOraMsg = "";
      String strItemType = "";

      int intCount = 0;

      log.debug(" Action is " + strAction + " strOpt is " + strOpt);

      if (strAction.equals("Load")) {
        alReturn = gmOper.getReturnsDashboard("OPERALL", "");
        hmReturn.put("PENDRETURNS", alReturn);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        strDispatchTo = strOperPath.concat("/GmReturnsReceive.jsp");

      }
      /*
       * else if (strAction.equals("EditLoadDash")) { strRAId =
       * request.getParameter("hRAId")==null?"":request.getParameter("hRAId"); hmResult =
       * gmCust.loadReturnsDetails(strRAId,strAction); request.setAttribute("hmReturn",hmResult);
       * request.setAttribute("hAction",strAction); }
       */
      /*
       * else if (strAction.equals("Save") || strAction.equals("Edit")) { strRAId =
       * request.getParameter("hRAId")==null?"":request.getParameter("hRAId"); strCount =
       * request.getParameter("hCnt")==null?"":request.getParameter("hCnt"); strRetDate =
       * request.getParameter("Txt_RetDate")==null?"":request.getParameter("Txt_RetDate"); strFlag =
       * request.getParameter("Chk_Flag")==null?"0":"1"; intCount = Integer.parseInt(strCount);
       * 
       * String strInputString = ""; for (int i=0;i<=intCount;i++) { strPartNum =
       * request.getParameter("hPartNum"+i)==null?"":request.getParameter("hPartNum"+i);
       * strOrgControl =
       * request.getParameter("hOrgCNum"+i)==null?"":request.getParameter("hOrgCNum"+i); if
       * (!strPartNum.equals("")) { strQty = request.getParameter("Txt_Qty"+i); strQty =
       * strQty.equals("")?"0":strQty; strControl = request.getParameter("Txt_CNum"+i); strControl =
       * strControl.equals("")?" ":strControl; strItemType =
       * GmCommonClass.parseNull(request.getParameter("hItemType"+i));
       * 
       * strInputString = strInputString + strPartNum + "," + strQty + "," + strControl + "," +
       * strOrgControl + "," + strItemType + "|"; } }
       * 
       * hmReturn =
       * gmOper.saveReturnsSetMaster(strRAId,strInputString,strRetDate,strFlag,strUserId);
       * 
       * hmResult = gmCust.loadReturnsDetails(strRAId,strAction);
       * request.setAttribute("hmReturn",hmResult);
       * 
       * request.setAttribute("hRetDate",strRetDate); request.setAttribute("hmReturn",hmResult);
       * request.setAttribute("hAction","EditLoad");
       * 
       * } else if (strAction.equals("Reprocess")) { //strRAId =
       * (String)session.getAttribute("strSessReturnId"
       * )==null?"":(String)session.getAttribute("strSessReturnId"); strRAId =
       * GmCommonClass.parseNull(request.getParameter("hId")); hmResult =
       * gmCust.loadReturnsDetails(strRAId,strAction); request.setAttribute("hmReturn",hmResult);
       * request.setAttribute("hAction",strAction); strDispatchTo =
       * strOperPath.concat("/GmReturnsReprocess.jsp"); } else if
       * (strAction.equals("ReprocessSave")) { strRAId =
       * request.getParameter("hRAId")==null?"":request.getParameter("hRAId"); strCNStr =
       * request.getParameter("hCNStr")==null?"":request.getParameter("hCNStr"); strQNStr =
       * request.getParameter("hQNStr")==null?"":request.getParameter("hQNStr"); strPNStr =
       * request.getParameter("hPNStr")==null?"":request.getParameter("hPNStr");
       * hmParam.put("CNSTR",strCNStr); hmParam.put("QNSTR",strQNStr);
       * hmParam.put("PNSTR",strPNStr);
       * 
       * hmResult = gmLogis.saveReturnsReprocess(strRAId,hmParam,strUserId); String strIds =
       * (String)hmResult.get("RETURNIDS"); strOraMsg = (String)hmResult.get("ORAMSG"); hmResult =
       * gmCust.loadReturnsDetails(strRAId,"Reprocess"); request.setAttribute("hmReturn",hmResult);
       * request.setAttribute("hAction",strAction); request.setAttribute("hReturnIds",strIds);
       * request.setAttribute("hOraMsg",strOraMsg); strDispatchTo =
       * strOperPath.concat("/GmReturnsReprocess.jsp"); }
       */

      dispatch(strDispatchTo, request, response);

    }// End of try

    catch (AppError e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
      return;
    }

    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch

  }// End of service method

}// End of GmReturnSetBuildServlet
