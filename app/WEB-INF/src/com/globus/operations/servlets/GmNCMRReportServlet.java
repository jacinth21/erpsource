/*****************************************************************************
 * File : GmNCMRServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.operations.beans.GmNCMRBean;

public class GmNCMRReportServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strDispatchTo = "";
    String strGridXmlData = "";
    HashMap hmParam = new HashMap();

    try {
      instantiate(request, response);
      String strAction = request.getParameter("hAction");
      GmCommonClass gmCommon = new GmCommonClass();
      GmNCMRBean gmNCMR = new GmNCMRBean(getGmDataStoreVO());

      ArrayList alReturn = new ArrayList();
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strSessCompanyLocale =GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

      String strFromDt = GmCommonClass.parseNull(request.getParameter("Txt_FromDate"));
      String strToDt = GmCommonClass.parseNull(request.getParameter("Txt_ToDate"));
      String strPartID = GmCommonClass.parseNull(request.getParameter("Txt_PartNum"));
      String strPartName = GmCommonClass.parseNull(request.getParameter("Txt_PartName"));
      String strVendorName = GmCommonClass.parseNull(request.getParameter("Txt_VendorName"));
      String strSort = GmCommonClass.parseNull(request.getParameter("hSort"));
      String strSortType = GmCommonClass.parseNull(request.getParameter("hSortAscDesc"));
      String strType = GmCommonClass.parseZero(request.getParameter("Cbo_Type"));
      String strNCMRId = GmCommonClass.parseNull(request.getParameter("Txt_NCMRNO"));
      String strNonConf = GmCommonClass.parseNull(request.getParameter("Txt_NonConf"));
      String strDHRId = GmCommonClass.parseNull(request.getParameter("Txt_DHRNO"));
      String strEVALID = GmCommonClass.parseNull(request.getParameter("Txt_EVALID"));
      String strTypeNew = GmCommonClass.parseZero(request.getParameter("Cbo_TypeNew"));
      log.debug("strEVALID:" + strEVALID + " strDHRId:" + strDHRId);

      strVendorName = strVendorName.replaceAll("@", "&");
      hmParam.put("FROMDT", strFromDt);
      hmParam.put("TODT", strToDt);
      hmParam.put("PARTID", strPartID);
      hmParam.put("PARTNM", strPartName);
      hmParam.put("VENDORNM", strVendorName);
      hmParam.put("SORT", strSort);
      hmParam.put("SORTTYPE", strSortType);
      hmParam.put("TYPE", strType);
      hmParam.put("NCMRNO", strNCMRId);
      hmParam.put("NONCONF", strNonConf);
      hmParam.put("EVALID", strEVALID);
      hmParam.put("TYPENEW", strTypeNew);
      hmParam.put("DHRID", strDHRId);



      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }

      strAction = (strAction == null) ? "LoadNCMRDetails" : strAction;

      if (strAction.equals("LoadNCMRDetails") || strAction.equals("Excel")) // Report Version
      {
        // strNCMRId = request.getParameter("hId")== null ?"":request.getParameter("hId");

        alReturn = gmNCMR.reportNCMRDetails(hmParam);
        request.setAttribute("alReturn", alReturn);
        request.setAttribute("hAction", strAction);

        request.setAttribute("hFromDate", strFromDt);
        request.setAttribute("hToDate", strToDt);
        request.setAttribute("hPartNum", strPartID);
        request.setAttribute("hPartName", strPartName);
        request.setAttribute("hVendorName", strVendorName);
        request.setAttribute("hSortAscDesc", strSortType);
        request.setAttribute("hSort", strSort);
        request.setAttribute("Cbo_Type", strType);
        request.setAttribute("Cbo_TypeNew", strTypeNew);
        request.setAttribute("hNCMRId", strNCMRId);
        request.setAttribute("hNonConf", strNonConf);
        request.setAttribute("hDHRId", strDHRId);
        request.setAttribute("hEVALId", strEVALID);
        if (strAction.equals("Excel")) {
          response.setContentType("application/vnd.ms-excel");
          response.setHeader("Content-disposition", "attachment;filename=NCMRExcelReport.xls");
          strDispatchTo = strOperPath.concat("/GmNCMRExcelReport.jsp");
        } else {
          strDispatchTo = strOperPath.concat("/GmNCMRReport.jsp");
        }
        
        hmParam.put("REPORT", alReturn);
        hmParam.put("gmDataStoreVO", getGmDataStoreVO());
        hmParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
        log.debug("hmParam>>>>"+hmParam);
        strGridXmlData = gmNCMR.getXmlGridData(hmParam);
        request.setAttribute("GRIDXMLDATA", strGridXmlData);
        
      }

      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmNCMRServlet
