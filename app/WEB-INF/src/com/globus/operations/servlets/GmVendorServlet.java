/*****************************************************************************
 * File : GmVendorServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException; 
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.operations.beans.GmVendorBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.common.beans.GmAccessControlBean;
public class GmVendorServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strDispatchTo = "";
    String strVendorAccessFlag="";
    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page

      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      GmVendorBean gmVendor = new GmVendorBean(getGmDataStoreVO());
      GmCommonBean gmCom = new GmCommonBean();
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
	  GmLogonBean gmLogonBean = new GmLogonBean(getGmDataStoreVO());
	  
      HashMap hmAccess = new HashMap();
      HashMap hmReturn = new HashMap();
      String strId = "";
      ArrayList alReturn = new ArrayList();
	  ArrayList alPurchasingAgent = new ArrayList();
	  
      String strAccesFlag="";
      String strUserId="";
      String strSetupAccessflag="";
      strUserId = (String) session.getAttribute("strSessUserId");
      hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId, "VEN_COST_TYPE_ACCESS"));  	
      strAccesFlag = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
	  /*PMT-50795-->Capture Purchasing Agent - Vendor Setup screen*/
	  alPurchasingAgent = gmLogonBean.getEmployeeList("300", "W");
	  
      /*PMT-26730-->System Manager Changes*/
      hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId, "VENDOR_SETUP_ACS"));  	
      strSetupAccessflag = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
      /*PMT-35869-->Enable Vendor Portal Access to Vendor*/
      hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId, "VENDOR_PORTAL_MAP_AC"));     
      strVendorAccessFlag = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
      if (strAction.equals("Load")) {
        hmReturn = gmVendor.loadVendor();
      } else if (strAction.equals("Add") || strAction.equals("Edit")) {

        strUserId = (String) session.getAttribute("strSessUserId");
        String strVendorNm = GmCommonClass.parseNull(request.getParameter("Txt_VendNm"));
        String strVendorShNm = GmCommonClass.parseNull(request.getParameter("Txt_VendShNm"));
        String strLotCode = GmCommonClass.parseNull(request.getParameter("Txt_LotCode"));
        String strStatus = GmCommonClass.parseNull(request.getParameter("Cbo_Status"));
        String strContactPerson = GmCommonClass.parseNull(request.getParameter("Txt_ContPer"));
        String strAddress1 = GmCommonClass.parseNull(request.getParameter("Txt_Add1"));
        String strAddress2 = GmCommonClass.parseNull(request.getParameter("Txt_Add2"));
        String strCity = GmCommonClass.parseNull(request.getParameter("Txt_City"));
        String strState = GmCommonClass.parseNull(request.getParameter("Cbo_State"));
        String strCountry = GmCommonClass.parseNull(request.getParameter("Cbo_Country"));
        String strPinCode = GmCommonClass.parseNull(request.getParameter("Txt_PinCode"));
        String strPayment = GmCommonClass.parseNull(request.getParameter("Cbo_Payment"));
        String strPhone = GmCommonClass.parseNull(request.getParameter("Txt_Phn"));
        String strFax = GmCommonClass.parseNull(request.getParameter("Txt_Fax"));
        String strActiveFl = GmCommonClass.parseNull(request.getParameter("Chk_ActiveFl"));
        String strCurrency = GmCommonClass.parseNull(request.getParameter("Cbo_Curr"));
        String strLog = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
        String strSec199FL = GmCommonClass.parseNull(request.getParameter("Chk_Sec199Fl"));
        String strPercentage = GmCommonClass.parseNull(request.getParameter("Txt_percentage"));
        String strRMInvoiceFL = GmCommonClass.parseNull(request.getParameter("Chk_RMinvoiceFl"));
        String strVendorCostType=GmCommonClass.parseNull(request.getParameter("vendor_Type"));
        String strVendorAccess = GmCommonClass.parseNull(request.getParameter("Chk_VPAccessFl"));
        String strAutoPublishFl = GmCommonClass.parseNull(request.getParameter("Chk_AutoPublishFl"));
		/*PMT-50795-->Capture Purchasing Agent - Vendor Setup screen*/
		String strPurchasingAgent = GmCommonClass.parseNull(request.getParameter("Cbo_Purchasing_agent"));
		String strEmail = GmCommonClass.parseNull(request.getParameter("Txt_Email"));

         log.debug("strVendorCostType"+strVendorCostType);		 


        HashMap hmValues = new HashMap();
        hmValues.put("VNAME", strVendorNm);
        hmValues.put("VSHNAME", strVendorShNm);
        hmValues.put("LCODE", strLotCode);
        hmValues.put("STATUS", strStatus);
        hmValues.put("CPERSON", strContactPerson);
        hmValues.put("ADD1", strAddress1);
        hmValues.put("ADD2", strAddress2);
        hmValues.put("CITY", strCity);
        hmValues.put("STATE", strState);
        hmValues.put("COUNTRY", strCountry);
        hmValues.put("PIN", strPinCode);
        hmValues.put("PAYMENT", strPayment);
        hmValues.put("PHONE", strPhone);
        hmValues.put("FAX", strFax);
        hmValues.put("AFLAG", strActiveFl);
        hmValues.put("CURR", strCurrency);
        hmValues.put("LOG", strLog);
        hmValues.put("SEC199", strSec199FL);
        hmValues.put("SPLITPERCENT", strPercentage);
        hmValues.put("RMINVOICE", strRMInvoiceFL);
        hmValues.put("VENDOR_COST_TYPE", strVendorCostType);
        hmValues.put("VENDOR_PORT_ACC", strVendorAccess);
        hmValues.put("AUTO_PUBLISH_FL", strAutoPublishFl);
		hmValues.put("PURCHASING_AGENT", strPurchasingAgent);
		hmValues.put("EMAIL", strEmail);
		


        strId = GmCommonClass.parseZero(request.getParameter("hId"));
        log.debug("strPartyId is ***" + strId);
        hmValues.put("VID", strId);

        HashMap hmSaveReturn = new HashMap();
        strId = gmVendor.saveVendor(hmValues, strUserId, strAction);
        
        hmReturn = gmVendor.loadEditVendor(strId);
        alReturn = gmCom.getLog(strId, "1217");

        request.setAttribute("hmLog", alReturn);
      } else if (strAction.equals("EditLoad")) {
        strId = GmCommonClass.parseNull(request.getParameter("hId"));
        hmReturn = gmVendor.loadEditVendor(strId);
        alReturn = gmCom.getLog(strId, "1217");
        request.setAttribute("hmLog", alReturn);
      }

	  hmReturn.put("PURCHASINGAGENT",alPurchasingAgent);
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("hId", strId);
      request.setAttribute("strAccesFlag",strAccesFlag);
      request.setAttribute("hstrSetupAccessflag", strSetupAccessflag);  /*PMT-26730-->System Manager Changes*/
      request.setAttribute("hstrVendorAccessFlag", strVendorAccessFlag); /*PMT-35869-->Enable Vendor Portal Access to Vendor*/

      dispatch(strOperPath.concat("/GmVendorSetup.jsp"), request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmVendorServlet
