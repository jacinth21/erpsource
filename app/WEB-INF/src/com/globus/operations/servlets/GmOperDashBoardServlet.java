/*****************************************************************************
 * File : GmOperDashBoardServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.operations.beans.GmLogisticsBean;
import com.globus.operations.beans.GmOperationsBean;

public class GmOperDashBoardServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strDispatchTo = "";
    String strDeptId = (String) session.getAttribute("strSessDeptId");
    String strOpt = GmCommonClass.parseNull((String) session.getAttribute("strSessOpt"));
    if (strOpt.equals("")) {
      strOpt = GmCommonClass.parseNull(request.getParameter("hOpt"));
    }
    try {
      instantiate(request, response);
      String strAction = request.getParameter("hAction");
      strAction = (strAction == null) ? "Load" : strAction;
      strDeptId = (String) session.getAttribute("strSessDeptId");
      GmCommonClass gmCommon = new GmCommonClass();
      GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
      GmLogisticsBean gmLogisticsBean = new GmLogisticsBean(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      String strId = "";

      String strStatFlag = "";
      String strVeriFlag = "";
      String strSetId = "";
      String strConsignId = "";
      String strInput = "";
      String strReprocessId="";

      strStatFlag = GmCommonClass.parseNull(request.getParameter("staInputs"));
      strVeriFlag = GmCommonClass.parseNull(request.getParameter("veriFlag"));
      strSetId = GmCommonClass.parseNull(request.getParameter("setID"));
      strConsignId = GmCommonClass.parseNull(request.getParameter("consignID"));
      strReprocessId = GmCommonClass.parseNull(request.getParameter("reprocessID"));
      strInput = GmCommonClass.parseNull(request.getParameter("hinputStr"));

      HashMap hmValues = new HashMap();
      hmValues.put("SFL", strStatFlag);
      hmValues.put("VFL", strVeriFlag);
      hmValues.put("SETID", strSetId);
      hmValues.put("CONID", strConsignId);
      hmValues.put("REPROCESSID", strReprocessId);

      ArrayList alDashStatus = new ArrayList();
      ArrayList alReturn = new ArrayList();
      log.debug("strOpt is " + strOpt);
      /*
       * if (strAction.equals("Load")) { hmReturn = gmOper.reportDashboard("0",strDeptId);
       * request.setAttribute("hmReturn",hmReturn); } else if (strAction.equals("Add") ||
       * strAction.equals("Edit")) { String strUserId =
       * (String)session.getAttribute("strSessUserId");
       * 
       * }
       */
      if (strOpt.equals("DashBoard")) {
        alDashStatus = GmCommonClass.getCodeList("GSTAS");
        hmReturn.put("DASHSTATUS", alDashStatus);
        log.debug("alDashStatus is " + alDashStatus);
        log.debug("strAction is " + strAction);

        if (strAction.equals("save")) {
          String strUserId = (String) session.getAttribute("strSessUserId");
          gmLogisticsBean.saveSetBuildDashboard(strInput, strUserId);
          log.debug("testing...strInput is " + strInput);
          strAction = "LoadDashBoard";
        }

        if (strAction.equals("LoadDashBoard")) {
          log.debug("testing...hmValues is " + hmValues);
          alReturn = gmOper.getSetBuildDashboard(hmValues);
          hmReturn.put("INISETS", alReturn);
        }


        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hOpt", strOpt);
        dispatch(strOperPath.concat("/GmSetBuildDashBoard.jsp"), request, response);
      } else {

        strAction = "OPERDASHBOARD";

        if (strDeptId.equals("L")) {
          strAction = "LOGISDASHBOARD";
        } else if (strDeptId.equals("Q")) {
          strAction = "QUALDASHBOARD";
        } else if (strDeptId.equals("F")) {
          strAction = "RECVDASHBOARD";
        } else if (strDeptId.equals("V")) {
          strAction = "INVDASHBOARD";
        }
        request.setAttribute("hAction", strAction);
        dispatch(strCustPath.concat("/GmDashBoardHome.jsp"), request, response);
      }
    }// End of try
    catch (Exception e) {
      System.out.println("EXCEPTION IN OPERDASHBOARD SERVLET:");
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath.concat("/GmError.jsp");
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmOperDashBoardServlet
