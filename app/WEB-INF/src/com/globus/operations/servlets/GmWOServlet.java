/*****************************************************************************
 * File : GmWOServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.servlets.GmServlet;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.beans.GmPurchaseBean;

public class GmWOServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strDispatchTo = strOperPath.concat("/GmWOView.jsp");

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      GmCommonClass gmCommon = new GmCommonClass();
      GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
      GmPurchaseBean gmPurc = new GmPurchaseBean(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      ArrayList alReturn = new ArrayList();
      String strWOId = "";
      String strVendorId = "";
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strDocRev = "";
      String strDocFl = "";
      String strJspFile = "";
      // to get the PO type.
      String strPOType = "";

      String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
      GmResourceBundleBean gmResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);

      if (strAction.equals("Load")) {

      } else if (strAction.equals("ViewWO")) {
        strWOId = request.getParameter("hWOId") == null ? "" : request.getParameter("hWOId");
        hmReturn = gmPurc.viewWO(strWOId, strUserId);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hWOId", strWOId);
      } else if (strAction.equals("PrintWO")) {
        strWOId = request.getParameter("hWOId") == null ? "" : request.getParameter("hWOId");
        strDocRev = request.getParameter("hDocRev") == null ? "" : request.getParameter("hDocRev");
        strDocFl = request.getParameter("hDocFl") == null ? "" : request.getParameter("hDocFl");
        hmReturn = gmPurc.viewWO(strWOId, strUserId);
        // The below changes is added for UDI-16,Based on PO created Date GmWorkOrderPrint.jasper
        // will be called or GmWoPrintAllD.jsp
        hmReturn.put("SUBREPORT_DIR",
            session.getServletContext().getRealPath(GmCommonClass.getString("GMJASPERLOCATION"))
                + "\\");
        HashMap hmCompanyAddress =
            GmCommonClass.fetchCompanyAddress(getGmDataStoreVO().getCmpid(), "");
        hmReturn.put("LOGO", GmCommonClass.parseNull((String) hmCompanyAddress.get("COMP_LOGO")));
        hmReturn.put("gmResourceBundleBean", gmResourceBundleBean);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hWOId", strWOId);
        // -- PC-2135 Inhouse MWO paperwork 2020 - Product Traveler and DHR Review
        HashMap hmWODetails = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("WODETAILS"));
        // to get the PO type.
        strPOType = GmCommonClass.parseNull((String) hmWODetails.get("PO_TYPE"));
        if (strDocFl.equals("Y")) {
          String strRptName =
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("WO.JASPERNM"));
       // to reset the new jasper template -- PC-2135 Inhouse MWO paperwork 2020 - Product Traveler and DHR Review
        if(strPOType.equals("3104")){ 
        	 String strManfWORptName = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("WO.3014_JASPERNM"));
        	 strRptName = strManfWORptName.equals("") ? strRptName:  strManfWORptName;
        }
          request.setAttribute("STRRPTNAME", strRptName);
          strJspFile = "/GmWOPrint";
        } else {
          strJspFile = "/GmWOPrint".concat(strDocRev);
        }
        strDispatchTo = strOperPath.concat(strJspFile).concat(".jsp");
      }
      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmWOServlet
