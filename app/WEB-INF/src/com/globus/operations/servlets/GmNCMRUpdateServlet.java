/*****************************************************************************
 * File : GmOrderVoidServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.operations.beans.GmNCMRBean;
import com.globus.operations.beans.GmPurchaseBean;

public class GmNCMRUpdateServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strDispatchTo = "";

    try {
      GmCommonClass gmCommon = new GmCommonClass();
      GmPurchaseBean gmPurc = new GmPurchaseBean(getGmDataStoreVO());
      GmNCMRBean gmNCMR = new GmNCMRBean(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      HashMap hmParam = new HashMap();
      ArrayList alReturn = new ArrayList();
      HashMap hmLoadReturn = new HashMap();

      String strSessOrderId = "";
      // String strUserId = (String)session.getAttribute("strSessUserId");
      // String strItemsId = "";
      String strVendorId = "";
      String strFormat = "";
      // String strOrdId = "";
      String strAction = "";
      // String strPartNums = "";
      String strFromDate = "";
      String strToDate = "";

      HashMap hmValues = new HashMap();


      GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
      int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
      strFromDate =
          GmCalenderOperations.getFirstDayOfMonth(currMonth, getGmDataStoreVO().getCmpdfmt());
      strToDate = GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt());

      String strMode = request.getParameter("hMode");
      if (strMode == null) {
        strMode = (String) session.getAttribute("hMode");
        session.removeAttribute("hMode");
      }
      strMode = (strMode == null) ? "Load" : strMode;

      // **********************************************************
      // Load the NCMR List Screen with Vendor Information
      // **********************************************************
      hmReturn = gmPurc.getVendorList("");

      // **************************************************************************************
      // Load the NCMR List Screen with Vendor Information and NCMR List for the selected date
      // **************************************************************************************
      if (strMode.equals("Reload")) {
        strVendorId =
            request.getParameter("Cbo_VendorId") == null ? "" : request
                .getParameter("Cbo_VendorId");
        strFromDate =
            request.getParameter("Txt_FromDate") == null ? "" : (String) request
                .getParameter("Txt_FromDate");
        strToDate =
            request.getParameter("Txt_ToDate") == null ? "" : (String) request
                .getParameter("Txt_ToDate");

        hmParam.put("FROMDT", strFromDate);
        hmParam.put("TODT", strToDate);
        hmParam.put("VENDORID", strVendorId);
        alReturn = gmNCMR.reportNCMRDetails(hmParam);
        hmReturn.put("NCMRLIST", alReturn);
        request.setAttribute("hVendorID", strVendorId);
      }


      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("hMode", strMode);
      request.setAttribute("hFrom", strFromDate);
      request.setAttribute("hTo", strToDate);

      strDispatchTo = strOperPath.concat("/GmNCMRList.jsp");

      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmOrderVoidServlet
