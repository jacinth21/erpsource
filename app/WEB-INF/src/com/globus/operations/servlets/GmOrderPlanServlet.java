/*****************************************************************************
 * File : GmOrderPlanServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException; 
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.jms.consumers.beans.GmInitiateVendorJMS;
import com.globus.operations.beans.GmPurchaseBean;
import com.globus.operations.beans.GmVendorBean;
import com.globus.prodmgmnt.beans.GmProjectBean;


public class GmOrderPlanServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strPurOperPath = GmCommonClass.getString("GMPUROPERATIONS");
    String strDispatchTo = strOperPath.concat("/GmOrderPlan.jsp");

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.
      GmCommonClass gmCommon = new GmCommonClass();

      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strOpt = request.getParameter("hOpt");

      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;

      GmPurchaseBean gmPurc = new GmPurchaseBean(getGmDataStoreVO());
      GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
      GmVendorBean gmVendor = new GmVendorBean(getGmDataStoreVO());
      GmInitiateVendorJMS gmInitiateVendorJMS = new GmInitiateVendorJMS(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      ArrayList alReturn = new ArrayList();
      String strProjId = "";
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strPOString = "";
      String strWOString = "";
      String strType = "";
      String strPartNum = "";
      String strPartNumFormat = "";
      String strSearch = "";
      String strVendorId = "";
      ArrayList alPoType = new ArrayList();
      ArrayList alVend = new ArrayList();
      HashMap hmTemp = new HashMap();
      HashMap hmParams = new HashMap();
      HashMap hmVendor = new HashMap();
      HashMap hmLoop = new HashMap();
      String strQtyToOrderFlag = "";
      String strCompanyId = "";
      String strViacellCmpFl = "";
      String strErrorMsg = "";
      String strCompanyInfo = "";
      String strPOID = "";

      strCompanyId = getGmDataStoreVO().getCmpid();
      strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
      
      strViacellCmpFl = GmCommonClass.getRuleValue(strCompanyId, "VIACELLCMPNY");
      request.setAttribute("VIACELLCOMPFL",strViacellCmpFl);
      
      request.setAttribute("WODCO_VLDN_COMPANY_FL",GmCommonClass.getRuleValue(strCompanyId, "WO_DCO_VLDN_COMPANY"));
      request.setAttribute("WODCO_VLDN_PO_TYPE",GmCommonClass.getRuleValue("WODCO_VLDN_PO_TYPE", "WODCO_VALIDATION"));
      

      if (strAction.equals("Load")) {
        hmTemp = new HashMap();
        hmTemp.put("COLUMN", "ID");
        hmTemp.put("STATUSID", "20301"); // Filter all approved projects
        alReturn = gmProject.reportProject(hmTemp);
        alPoType = gmCommon.getCodeList("POTYP");
        hmTemp.put("CHECKACTIVEFL", "N"); // check for active flag status in vendor list

        hmReturn = gmVendor.getVendorList(hmTemp);
        session.setAttribute("VENDORLIST", hmReturn.get("VENDORLIST"));
        session.setAttribute("PROJECTS", alReturn);
        request.setAttribute("hAction", strAction);
        session.setAttribute("alPOTYPE", alPoType);
      } else if (strAction.equals("PriceLoad")) {
        strProjId = GmCommonClass.parseNull(request.getParameter("hProjId"));
        strProjId = strProjId.equals("0") ? "" : strProjId;
        strType = GmCommonClass.parseNull(request.getParameter("Cbo_PoType"));       
        strSearch = GmCommonClass.parseNull(request.getParameter("Cbo_Search"));
        strVendorId = GmCommonClass.parseNull(request.getParameter("Cbo_VendId"));
        strPartNum = GmCommonClass.parseNull(request.getParameter("Txt_PartNum"));
        strQtyToOrderFlag = GmCommonClass.parseNull(request.getParameter("Chk_QtyToOrder"));
        strQtyToOrderFlag = strQtyToOrderFlag.equals("on") ? "Y" : "";

        if (!strSearch.equals("") && !strPartNum.equals("")) {
          hmParams.put("PARTNUM", strPartNum);
          hmParams.put("SEARCH", strSearch);

          strPartNumFormat = GmCommonClass.createRegExpString(hmParams);
        }

        hmReturn =gmPurc.getPartVendorPriceList(strProjId, strPartNumFormat, strVendorId,strQtyToOrderFlag);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hProjId", strProjId);
        request.setAttribute("hPoType", strType);
        request.setAttribute("hVendId", strVendorId);
        request.setAttribute("hSearch", strSearch);
        request.setAttribute("hPartNum", strPartNum);
        request.setAttribute("hQtyFlag", strQtyToOrderFlag);
      } else if (strAction.equals("Initiate")) {
        strProjId   = GmCommonClass.parseNull(request.getParameter("hProjId"));
        strPOString = GmCommonClass.parseNull(request.getParameter("hPOString"));
        strWOString = GmCommonClass.parseNull(request.getParameter("hWOString"));
        strType     = GmCommonClass.parseNull(request.getParameter("Cbo_PoType"));
        log.debug(" values inside Initiate is strProjId" + strProjId + " strPOString "
            + strPOString + " strWOString " + strWOString + " strType " + strType);
        
        //validateProductPlant: This method is used to validate the PO based on the plant while initiating PO
        gmPurc.validateProductPlant(strProjId,strWOString);

        hmReturn = gmPurc.initatePOProcess(strProjId, strPOString, strWOString, strType, strUserId);
        log.debug("hmReturn ordplanserv"+hmReturn);
        ArrayList alPOList = (ArrayList) hmReturn.get("POLIST");
        
        for (int i = 0; i < alPOList.size(); i++) {
            hmLoop = (HashMap) alPOList.get(i);
            strPOID = GmCommonClass.parseNull((String) hmLoop.get("POID"));
            log.debug(" strPOID==>" + strPOID);
            strVendorId = GmCommonClass.parseNull((String) hmLoop.get("VID"));
            
            String strAutoPublishFl = GmCommonClass.parseNull(gmPurc.fetchAutoPublishFl(strPOID, strVendorId)); 
            log.debug(" strAutoPublishFl==>" + strAutoPublishFl);
            log.debug(" strVendorId==>" + strVendorId);
            
            //if auto publish flag is Y for vendor then it will call JMS and send mail
            if(strAutoPublishFl.equals("Y")){
            	hmReturn.put("REFID", strPOID); 
            	hmReturn.put("REFTYPE", "108440"); //purchase order type
            	hmReturn.put("REFACTION", "108400"); //Published type
            	hmReturn.put("MAILSTATUS", "0"); //to send instant mail
            	hmReturn.put("USERID", strUserId);
            	hmReturn.put("COMPANYINFO", strCompanyInfo);
            
            	gmInitiateVendorJMS.initVendorEmailJMS(hmReturn); //  PO JMS call
            }
        }
        
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hProjId", strProjId);
        request.setAttribute("hAction", "Load");

        strDispatchTo = strOperPath.concat("/GmPOList.jsp");
      } else if (strAction.equals("ViewHistory")) {
        strPOString = request.getParameter("hPO") == null ? "" : request.getParameter("hPO");
        strPOString = "'".concat(strPOString).concat("'");
        hmReturn = gmPurc.getPOReport(strPOString, "", "");

        request.setAttribute("hmReturn", hmReturn);
        strDispatchTo = strOperPath.concat("/GmPOHistory.jsp");
      } else if (strAction.equals("validatePlant")) {
    	strProjId   = GmCommonClass.parseNull(request.getParameter("projId"));
    	strWOString = GmCommonClass.parseNull(request.getParameter("woString"));
    	
    	strErrorMsg = gmPurc.validatePOProdPlant(strProjId,strWOString); // Getting the Error message from the validatePOProdPlant()
    	response.setContentType("text/xml");
        PrintWriter pw = response.getWriter();
        pw.write(strErrorMsg);
        pw.flush();
        pw.close();
        return;
      } else if (strAction.equals("LoadVendor")) {
          strVendorId = GmCommonClass.parseNull(request.getParameter("Cbo_VendId"));
          hmReturn = gmVendor.getVendorList(hmTemp);
          request.setAttribute("VENDORLIST", hmReturn.get("VENDORLIST"));
          alPoType = gmCommon.getCodeList("POTYP");        
          request.setAttribute("ALPOTYPE", alPoType);
          	strType = GmCommonClass.parseZero(request.getParameter("Cbo_Po_Type"));          	
          	
          // to set the default to PO type as Product
          strType = strType.equals("0") ? "3100" : strType;
         	

          if (strOpt.equals("ReLoad")) {
            hmVendor = gmPurc.loadVendorDetails(strVendorId);

            String strShName = GmCommonClass.parseNull((String) hmVendor.get("VSHNAME"));
            String strContact = GmCommonClass.parseNull((String) hmVendor.get("CPERSON"));
            String strLotCode = GmCommonClass.parseNull((String) hmVendor.get("LCODE"));
            String strCurrency = GmCommonClass.parseNull((String) hmVendor.get("CURR"));
            String strPhone = GmCommonClass.parseNull((String) hmVendor.get("PHONE"));
            String strFax = GmCommonClass.parseNull((String) hmVendor.get("FAX"));

            request.setAttribute("VENDORID", strVendorId);
            request.setAttribute("VSHNAME", strShName);
            request.setAttribute("CPERSON", strContact);
            request.setAttribute("LCODE", strLotCode);
            request.setAttribute("CURR", strCurrency);
            request.setAttribute("PHONE", strPhone);
            request.setAttribute("FAX", strFax);

          }
          strDispatchTo = strPurOperPath.concat("/GmBatchPO.jsp");

        } else if (strAction.equals("BatchPO")) {	
          strVendorId = GmCommonClass.parseNull(request.getParameter("Cbo_VendId"));
          strType = GmCommonClass.parseZero(request.getParameter("Cbo_Po_Type"));
         	

          String strPartBatch = request.getParameter("hInputStr");
          hmReturn = gmPurc.initiateBatchPOProcess(strVendorId, strType, strPartBatch, strUserId);
         	


          String strPOId = GmCommonClass.parseNull(((String) hmReturn.get("POID")));
          gmPurc.sendFAREmail(strPOId);
          ArrayList alPODtl = gmPurc.fetchPODetail(strPOId);
          hmReturn.put("POLIST", alPODtl);
          request.setAttribute("hmReturn", hmReturn);
          request.setAttribute("hAction", strAction);
          request.setAttribute("hAction", "Load");
          strDispatchTo = strOperPath.concat("/GmPOList.jsp");
        }     
      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmOrderPlanServlet
