/*****************************************************************************
 * File : GmPOProcessServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.beans.GmPurchaseBean;

public class GmPOProcessServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strDispatchTo = "";
    String strDispatchTo1 = strOperPath.concat("/GmPOProcess.jsp");

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      String strAction = request.getParameter("hAction");
      strAction = (strAction == null) ? "Load" : strAction;

      GmCommonClass gmCommon = new GmCommonClass();
      GmOperationsBean gmOper = new GmOperationsBean();
      GmCustomerBean gmCust = new GmCustomerBean();
      GmPurchaseBean gmPurc = new GmPurchaseBean(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      HashMap hmOrder = new HashMap();
      HashMap hmValues = new HashMap();
      HashMap hmResult = new HashMap();
      HashMap hmCart = new HashMap();
      ArrayList alReturn = new ArrayList();

      String strVendId = "";
      String strReqDate = "";
      String strPOId = "";
      String strOMode = "";
      String strPName = "";
      String strRepId = "";
      String strShipTo = "";
      String strPO = "";
      String strPartNums = "";
      String strhPartNums = "";
      String strDelPartNum = "";
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strOrdId = "";
      String strTotal = "";

      if (strAction.equals("Load")) {
        session.removeAttribute("hmReturn"); // Clear Order details
        session.removeAttribute("hmCart"); // Clear Cart Details
        hmReturn = gmPurc.getVendorList("");
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
      } else if (strAction.equals("Go")) {
        strVendId = request.getParameter("hVendId") == null ? "" : request.getParameter("hVendId");
        strReqDate =
            request.getParameter("Txt_Dreq") == null ? "" : request.getParameter("Txt_Dreq");
        strPOId = request.getParameter("hPOId") == null ? "" : request.getParameter("hPOId");

        strPartNums =
            request.getParameter("Txt_PartNum") == null ? "" : request.getParameter("Txt_PartNum");
        strhPartNums =
            request.getParameter("hPartNums") == null ? "" : request.getParameter("hPartNums");

        if (strhPartNums != null && strhPartNums != "") {
          strPartNums = strhPartNums.concat(",").concat(strPartNums);
        }

        hmReturn = gmPurc.loadCart(strPOId, strVendId, strPartNums);

        session.setAttribute("hmOrder", hmReturn);
        session.setAttribute("hAction", "PopOrd");
        session.setAttribute("strPartNums", strPartNums);
        strDispatchTo1 = strOperPath.concat("/GmPOAddToCart.jsp");

      } else if (strAction.equals("UpdateCart") || strAction.equals("PlaceOrder")
          || strAction.equals("RemoveCart") || strAction.equals("UpdateOrder")) {
        strPartNums =
            request.getParameter("hPartNums") == null ? "" : request.getParameter("hPartNums");
        strDelPartNum =
            request.getParameter("hDelPartNum") == null ? "" : request.getParameter("hDelPartNum");

        HashMap hmTempOrd = (HashMap) session.getAttribute("hmOrder");
        HashMap hmTempCart = new HashMap();

        StringTokenizer strTok = new StringTokenizer(strPartNums, ",");
        String strPartNum = "";
        String strTempPartNums = "";
        hmCart = new HashMap();
        HashMap hmLoop = new HashMap();
        String strQty = "";
        String strPrice = "";
        int i = 0;

        while (strTok.hasMoreTokens()) {
          hmLoop = new HashMap();
          i++;
          strPartNum = strTok.nextToken();
          if (strDelPartNum.equals(strPartNum)) {
            hmTempOrd.remove(strPartNum);
          } else {
            strQty =
                request.getParameter("Txt_Qty" + i) == null ? "" : request.getParameter("Txt_Qty"
                    + i);
            strPrice =
                request.getParameter("hPrice" + i) == null ? "" : request
                    .getParameter("hPrice" + i);
            hmLoop.put("QTY", strQty);
            hmLoop.put("PRICE", strPrice);
            hmTempCart.put(strPartNum, hmLoop);

            strTempPartNums = strTempPartNums.concat(strPartNum);
            strTempPartNums = strTempPartNums.concat(",");
          }

        }
        if (!strTempPartNums.equals("")) {
          strTempPartNums = strTempPartNums.substring(0, strTempPartNums.length() - 1);
        }
        session.setAttribute("hmCart", hmTempCart);
        session.setAttribute("hmOrder", hmTempOrd);

        if (strAction.equals("RemoveCart")) {
          session.setAttribute("strPartNums", strTempPartNums);
          session.setAttribute("hAction", "PopOrd");
        } else if (strAction.equals("PlaceOrder")) {
          strVendId = request.getParameter("hAccId") == null ? "" : request.getParameter("hAccId");
          strRepId = request.getParameter("hRepId") == null ? "" : request.getParameter("hRepId");
          strShipTo =
              request.getParameter("hShipTo") == null ? "" : request.getParameter("hShipTo");
          strPO = request.getParameter("Txt_PO") == null ? "" : request.getParameter("Txt_PO");
          strOrdId = request.getParameter("hOrdId") == null ? "" : request.getParameter("hOrdId");
          strTotal = request.getParameter("hTotal") == null ? "" : request.getParameter("hTotal");
          strOMode = request.getParameter("hOMode") == null ? "" : request.getParameter("hOMode");
          strPName = request.getParameter("hPName") == null ? "" : request.getParameter("hPName");

          hmOrder = new HashMap();
          hmOrder.put("ORDID", strOrdId);
          hmOrder.put("ACCID", strVendId);
          hmOrder.put("REPID", strRepId);
          hmOrder.put("SHIPTO", strShipTo);
          hmOrder.put("PO", strPO);
          hmOrder.put("TOTAL", strTotal);
          hmOrder.put("OMODE", strOMode);
          hmOrder.put("PNAME", strPName);

          hmReturn = gmCust.placeOrder(hmOrder, hmTempCart, strPartNums, strUserId);
          session.setAttribute("hAction", "OrdPlaced");
          strDispatchTo1 = strOperPath.concat("/GmPOAddToCart.jsp");
        } else if (strAction.equals("UpdateOrder")) {
          strOrdId = request.getParameter("hOrdId") == null ? "" : request.getParameter("hOrdId");
          strTotal = request.getParameter("hTotal") == null ? "" : request.getParameter("hTotal");
          hmReturn = gmCust.updateOrder(strOrdId, strTotal, hmTempCart, strPartNums, strUserId);
          session.setAttribute("hAction", "OrdPlaced");
          strDispatchTo1 = strOperPath.concat("/GmPOAddToCart.jsp");

        }
        strDispatchTo1 = strOperPath.concat("/GmPOAddToCart.jsp");
      }

      if (strAction.equals("Go") || strAction.equals("Load")) {
        dispatch(strDispatchTo1, request, response);
      } else {
        gotoPage(strDispatchTo1, request, response);
      }

    }// End of try
    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmPOProcessServlet
