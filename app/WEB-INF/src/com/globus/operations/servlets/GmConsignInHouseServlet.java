/*****************************************************************************
 * File : GmConsignInHouseServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmRuleBean;
import com.globus.common.servlets.GmServlet;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.jms.consumers.beans.GmTransSplitBean;
import com.globus.operations.beans.GmLogisticsBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.returns.beans.GmReturnsBean;


public class GmConsignInHouseServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");

    String strDispatchTo = strOperPath.concat("/GmInHouseItemConsignSetup.jsp");

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j
      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strMode = request.getParameter("hMode") == null ? "" : request.getParameter("hMode");

      // String strOpt = (String)session.getAttribute("hOpt");
      System.out.println("strMode==>" + strMode);
      String strOpt = (String) session.getAttribute("strSessOpt");
      strOpt = strOpt == null || strOpt.equals("") ? "" : strOpt;

      GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
      GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
      GmLogisticsBean gmLogis = new GmLogisticsBean(getGmDataStoreVO());
      GmRuleBean gmRuleBean = new GmRuleBean(getGmDataStoreVO());
      GmReturnsBean gmReturn = new GmReturnsBean(getGmDataStoreVO());
      GmTransSplitBean gmTransSplitBean = new GmTransSplitBean(getGmDataStoreVO()); //For PMT-33513

      HashMap hmReturn = new HashMap();
      HashMap hmResult = new HashMap();
      HashMap hmParam = new HashMap();
      HashMap hmStorageBuildInfo = new HashMap(); //for PMT-33513

      ArrayList alReturn = new ArrayList();
      String strConsignId = "";
      String strType = "";
      String strBillTo = "";
      String strPurpose = "";
      String strShipTo = "";
      String strShipToId = "";
      String strComments = "";
      String strShipFl = "";
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strPartNums = "";
      String strhPartNums = "";
      String strDelPartNum = "";
      String strInputStr = "";
      String strPartNum = "";
      String strTempPartNums = "";
      String strAddCartType = "";

      String strInputString = "";
      String strQty = "";
      String strPrice = "";
      String strControl = "";
      String strLoanerType = "0";
      String strReTxn = "";
      String strTransType = "NULL";
      String strAccess = "";
      String strTransIds = "";
      int intTransQty = 0;
      int curTransQty = 0;
      int intExQty = 0;
      String strSrcTransIds = "";
      String strTransForCart = "";
      String strRowCount = "";
      String strCompanyId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid()); 
      log.debug("GmConsignInHouseServlet-Company==> "+strCompanyId);
      log.debug(" Action is " + strAction + " Option is " + strOpt);

      String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
      // Locale
      GmResourceBundleBean gmResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
      // -- TO GET NUMBER OF ROWS ALLOWED TO ADD IN CART.
      strRowCount =
          GmCommonClass
              .parseNull(GmCommonClass.getRuleValue("ADD_TO_CART_ROWS", "INHOUSEROWCOUNT"));
      session.setAttribute("CARTROWCOUNT", strRowCount);

      ArrayList alQDList =
          GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CARTOP", getGmDataStoreVO()));
      GmCommonBean gmCommonBean = new GmCommonBean();
      hmParam.put("RULEID", strOpt);
      HashMap hmTransRules = gmCommonBean.fetchTransactionRules(hmParam);
      String strRulesOpt = "";
      String strIncludeRuleEng = "";

      strRulesOpt = GmCommonClass.parseNull((String) hmTransRules.get("STR_OPT"));
      strTransType = GmCommonClass.parseNull((String) hmTransRules.get("TRANS_TYPE"));

      request.setAttribute("TRANS_RULES", hmTransRules);
      session.setAttribute("TRANS_RULES", hmTransRules);
      strIncludeRuleEng = GmCommonClass.parseNull((String) hmTransRules.get("INCLUDE_RULE_ENGINE"));
      log.debug("hmTransRules" + hmTransRules);

      if (!strOpt.equals("") && hmTransRules != null) {
        // //Access Rule COde/////
        String strOperType = "INITIATE";
        strOperType =
            GmCommonClass.parseNull(request.getParameter("hMode")).equals("") ? strOperType
                : request.getParameter("hMode");
        strOperType = "FN_" + strOperType;
        GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
        log.debug("User ID == " + strUserId);
        strOperType = (String) hmTransRules.get(strOperType);
        log.debug("Request URI == " + strOpt + "_" + strOperType);
        if (strOperType != null) {
          HashMap hmAccess =
              GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                  strOperType));
          strAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
          request.setAttribute("ACCESSFL", strAccess);
          session.setAttribute("ACCESSFL", strAccess);
          if (!strAccess.equals("Y")) {
            throw new AppError("", "20685");
          }
        }
        // ////////////
      }



      if (strAction.equals("EditLoad") || strAction.equals("GoCart")
          || strAction.equals("UpdateCart") || strAction.equals("PlaceOrder")
          || strAction.equals("RemoveCart") || strAction.equals("UpdateOrder")) {
        strType = request.getParameter("Cbo_Type") == null ? "" : request.getParameter("Cbo_Type");
        strPurpose =
            request.getParameter("Cbo_Purpose") == null ? "0" : (String) request
                .getParameter("Cbo_Purpose");
        strBillTo =
            request.getParameter("Cbo_BillTo") == null ? "" : request.getParameter("Cbo_BillTo");
        strShipTo =
            request.getParameter("Cbo_ShipTo") == null ? "" : request.getParameter("Cbo_ShipTo");
        strShipToId =
            request.getParameter("Cbo_Values") == null ? "0" : request.getParameter("Cbo_Values");
        strComments =
            request.getParameter("Txt_Comments") == null ? "" : request
                .getParameter("Txt_Comments");
        strShipFl = request.getParameter("Chk_ShipFlag") == null ? "0" : "1";

        hmParam.put("TYPE", strType);
        hmParam.put("PURPOSE", strPurpose);
        hmParam.put("BILLTO", strBillTo);
        hmParam.put("SHIPTO", strShipTo);
        hmParam.put("SHIPTOID", strShipToId);
        hmParam.put("COMMENTS", strComments);
        hmParam.put("SHIPFL", strShipFl);
        hmParam.put("QDLIST", alQDList);
        session.setAttribute("PARAM", hmParam);
      }

      if (strAction.equals("Load") && strOpt.equals("")) {
        hmReturn = gmLogis.loadConsignLists(strOpt, hmTransRules);
        hmReturn.put("CONLISTS", hmReturn);
        session.setAttribute("hmReturn", hmReturn);
        session.setAttribute("hAction", strAction);
      } else if (strAction.equals("Load") && strOpt.equals("InHouse") || strAction.equals("Load")
          && strOpt.equals("Repack") || strAction.equals("Load") && strOpt.equals("QuaranIn")
          || strAction.equals("Load") && strOpt.equals("QuaranOut") || strAction.equals("Load")
          && strOpt.equals("InvenIn") || strAction.equals("Load") && strOpt.equals("BulkOut")
          || strAction.equals("Load") && strOpt.equals("BulkIn") || strAction.equals("Load")
          && strOpt.equals("RawMatScrap") || strAction.equals("Load")
          && strOpt.equals("RawMatQuar") || strAction.equals("Load") && strOpt.equals("RawMatFG")
          || strAction.equals("Load") && strOpt.equals("FGRawMat") || strAction.equals("Load")
          && strOpt.equals(strRulesOpt)) {
        hmReturn = gmLogis.loadConsignLists(strOpt, hmTransRules);
        hmReturn.put("QDLIST", alQDList);
        hmReturn.put("CONLISTS", hmReturn);
        session.setAttribute("hmReturn", hmReturn);
        session.setAttribute("hAction", strAction);
      } else if (strAction.equals("EditLoad")) {
        strConsignId = gmLogis.loadNextConsignId(strOpt);
        session.setAttribute("CONSIGNID", strConsignId);
        session.setAttribute("hAction", strAction);
      } else if (strAction.equals("GoCart")) {
        strAddCartType =
            request.getParameter("hAddCartType") == null ? "106457" : request
                .getParameter("hAddCartType");// -- get add cart type from dropdown , is its null
                                              // 106457- By part
        strPartNums =
            request.getParameter("Txt_PartNum") == null ? "" : request.getParameter("Txt_PartNum");
        strhPartNums =
            request.getParameter("hPartNums") == null ? "" : request.getParameter("hPartNums");
        if (strhPartNums != null && !strhPartNums.equals("")) {
          strPartNums = strhPartNums.concat(",").concat(strPartNums);
        }
        if (strType.equals("40051") || strType.equals("40052") || strType.equals("40053")) {
          strTransType = "90802";
        } else if (strType.equals("40054")) {
          strTransType = "NULL";
        }
        if (strAddCartType.equals("106458")) { // -- 106458 - By transaction type
          // fetch values by transaction id.
          // we have one text field for both transaction(By part and By transaction), for
          // variation using strSrcTransIds as temp variable
          strSrcTransIds = strPartNums;
          strTransForCart =
              request.getParameter("Cbo_Type") == null ? "" : request.getParameter("Cbo_Type");
          hmReturn = gmReturn.loadPartInfoFromTransId(strSrcTransIds, strTransForCart);
          session.setAttribute("ADDCARTTYPE", strAddCartType);
        } else {// -- 106457 - By part type

          hmReturn = gmLogis.loadCartForConsign(strPartNums, strTransType);
        }

        session.setAttribute("hmOrder", hmReturn);
        session.setAttribute("hAction", "PopOrd");
        session.setAttribute("strPartNums", strPartNums);
      } else if (strAction.equals("UpdateCart") || strAction.equals("PlaceOrder")
          || strAction.equals("RemoveCart") || strAction.equals("UpdateOrder")) {
        strPartNums =
            request.getParameter("hPartNums") == null ? "" : request.getParameter("hPartNums");
        strDelPartNum =
            request.getParameter("hDelPartNum") == null ? "" : request.getParameter("hDelPartNum");
        strAddCartType =
            request.getParameter("hAddCartType") == null ? "106457" : request
                .getParameter("hAddCartType");// -- get add cart type from dropdown , is its null
                                              // 106457- By part
        HashMap hmTempOrd = (HashMap) session.getAttribute("hmOrder");
        HashMap hmTempCart = new HashMap();

        StringTokenizer strTok = null;
        // 106458- type is transacton by
        if (strAddCartType.equals("106458")) {
          String strTransPart =
              request.getParameter("hTransPartNums") == null ? "" : request
                  .getParameter("hTransPartNums");
          // -- get transaction part details keys to iterate transaction details
          strTok = new StringTokenizer(strTransPart, ",");
        } else {
          strTok = new StringTokenizer(strPartNums, ",");
        }


        HashMap hmLoop = new HashMap();
        int i = 0;

        while (strTok.hasMoreTokens()) {
          hmLoop = new HashMap();
          i++;
          strPartNum = strTok.nextToken();

          if (strDelPartNum.equals(strPartNum)) {
            hmTempOrd.remove(strPartNum);
          } else {
            strQty =
                request.getParameter("Txt_Qty" + i) == null ? "" : request.getParameter("Txt_Qty"
                    + i);
            strPrice =
                request.getParameter("hPrice" + i) == null ? "" : request
                    .getParameter("hPrice" + i);
            strControl =
                strOpt.equals("BulkOut") || strOpt.equals("BulkIn")
                    || strOpt.equals("QUARAN-INVAD") || strOpt.equals("REPACK-INVAD") ? "NOC#" : "";
            hmLoop.put("QTY", strQty);
            hmLoop.put("PRICE", strPrice);
            hmLoop.put("CONTROL", strControl);
            hmTempCart.put(strPartNum, hmLoop);
            // 106458- transaction by type
            // if type is 106458 and action is PlaceOrder , getting control number and transaction
            // quantity
            if (strAddCartType.equals("106458") && strAction.equals("PlaceOrder")) {
              strPartNum =
                  request.getParameter("hPNum" + i) == null ? "" : request
                      .getParameter("hPNum" + i);
              strControl =
                  request.getParameter("hCtlNum" + i) == null ? "" : request.getParameter("hCtlNum"
                      + i);// -- get control number from transaction
              intTransQty =
                  Integer.parseInt(request.getParameter("hOldTransQty" + i) == null ? "0" : request
                      .getParameter("hOldTransQty" + i));// -- get transaction qty from transaction
              curTransQty = Integer.parseInt(strQty);// -- get cart qty
              // Checks cart qty is greater than source transaction qty
              if (curTransQty > intTransQty) {
                // -- subtraction cart qty and source transaction qty to get added qty
                intExQty = curTransQty - intTransQty;
                // seperate remaining qty with control number as empty
                strInputStr =
                    strInputStr
                        + strPartNum.concat(",").concat(Integer.toString(intTransQty)).concat(",")
                            .concat(strControl).concat(",").concat(strPrice).concat("|")
                            .concat(strPartNum).concat(",").concat(Integer.toString(intExQty))
                            .concat(",").concat("").concat(",").concat(strPrice);

              } else {
                strInputStr =
                    strInputStr
                        + strPartNum.concat(",").concat(strQty).concat(",").concat(strControl)
                            .concat(",").concat(strPrice);
              }

            } else {
              strInputStr =
                  strInputStr
                      + strPartNum.concat(",").concat(strQty).concat(",").concat(strControl)
                          .concat(",").concat(strPrice);
            }


            strInputStr = strInputStr.concat("|");

            strTempPartNums = strTempPartNums.concat(strPartNum);
            strTempPartNums = strTempPartNums.concat(",");
          }

        }


        if (!strTempPartNums.equals("")) {
          strTempPartNums = strTempPartNums.substring(0, strTempPartNums.length() - 1);
        }
        // if type is 106458 and action is RemoveCart , add transaction part keys and transaction id
        if (strAddCartType.equals("106458") && strAction.equals("RemoveCart")) {
          // strSrcTransIds is temp variable for source transaction ids
          strSrcTransIds = strPartNums;
          hmTempOrd.put("TRANSPARTNUMS", strTempPartNums);
          hmTempOrd.put("TRANSIDS", strSrcTransIds);
        }
        session.setAttribute("hmCart", hmTempCart);
        session.setAttribute("hmOrder", hmTempOrd);

        log.debug(" Temp Part Nums in Cart  is " + hmTempCart);
        log.debug(" Temp Order value is " + hmTempOrd);

        if (strAction.equals("RemoveCart")) {
          if (strAddCartType.equals("106458")) {
            // if type is 106458 - By transaction, add transaction partnum in session
            session.setAttribute("strPartNums", strPartNums);
          } else {
            // if type is 106457 - By part
            session.setAttribute("strPartNums", strTempPartNums);
          }
          session.setAttribute("hAction", "PopOrd");
        } else if (strAction.equals("PlaceOrder")) {
          strConsignId = (String) session.getAttribute("CONSIGNID");
          hmReturn = gmLogis.saveItemConsign(strConsignId, hmParam, strInputStr, strUserId);
          if (strAddCartType.equals("106458")) { // -- 106458 - By transaction
            // save copy transaction details to T414_TRANSACTION_LOG table
            // strSrcTransIds is temp variable for source transaction ids
            strSrcTransIds = strPartNums;
            gmReturn.saveItemConsignByTrans(strConsignId, strSrcTransIds, strUserId);
          }

          session.setAttribute("hAction", "OrdPlaced");
          
        //JMS call for Storing Building Info PMT-33513 
          log.debug("Storing building information GmConsignInHouseServlet ");
          hmStorageBuildInfo.put("TXNID", strConsignId);//consignment id
          hmStorageBuildInfo.put("TXNTYPE", "INHOUSE412");
          gmTransSplitBean.updateStorageBuildingInfo(hmStorageBuildInfo);  
          // session.removeAttribute("strSessOpt");
        } else if (strAction.equals("UpdateOrder")) {
          log.debug(" Inside Update Order");
        }

      } else if (strAction.equals("PrintVersion")) {
        strConsignId = request.getParameter("hId") == null ? "" : request.getParameter("hId");

        hmResult = gmOper.loadSavedSetMaster(strConsignId);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));

        hmResult = gmLogis.loadConsignDetails(strConsignId);
        hmReturn.put("SETDETAILS", hmResult);

        request.setAttribute("hmReturn", hmReturn);
        strDispatchTo = strOperPath.concat("/GmSetConsignPrint.jsp");
        strOpt = "";
      } else if (strAction.equals("Ack")) {
        strConsignId = request.getParameter("hId") == null ? "" : request.getParameter("hId");

        hmResult = gmLogis.loadConsignAckDetails(strConsignId);
        hmReturn.put("ACKDETAILS", hmResult);

        request.setAttribute("hmReturn", hmReturn);
        strDispatchTo = strOperPath.concat("/GmConsignAckPrint.jsp");
        strOpt = "";
      } else if (strAction.equals("EditControl")) {
        strConsignId =
            (String) session.getAttribute("strSessConsignId") == null ? "" : (String) session
                .getAttribute("strSessConsignId");

        hmResult = gmOper.loadSavedSetMaster(strConsignId);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmResult = gmOper.viewBuiltSetDetails(strConsignId);
        hmReturn.put("CONDETAILS", hmResult);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "EditLoad");
        request.setAttribute("hConsignId", strConsignId);
        strDispatchTo = strOperPath.concat("/GmItemConsignEdit.jsp");
      } else if (strAction.equals("SaveControl")) {
        String strCount = "";
        int intCount = 0;
        strConsignId =
            request.getParameter("hConsignId") == null ? "" : request.getParameter("hConsignId");
        strCount = request.getParameter("hCnt") == null ? "" : request.getParameter("hCnt");
        strShipFl =
            request.getParameter("Chk_ShipFl") == null ? "2" : request.getParameter("Chk_ShipFl");
        strComments = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
        intCount = Integer.parseInt(strCount);

        for (int i = 0; i <= intCount; i++) {
          strPartNum =
              request.getParameter("hPartNum" + i) == null ? "" : request.getParameter("hPartNum"
                  + i);
          if (!strPartNum.equals("")) {
            strQty = request.getParameter("Txt_Qty" + i);
            strQty = strQty.equals("") ? "0" : strQty;
            strControl = request.getParameter("Txt_CNum" + i);
            strControl = strControl.equals("") ? " " : strControl;
            strPrice = request.getParameter("hPrice" + i);
            strInputString =
                strInputString.concat(strPartNum).concat(",").concat(strQty).concat(",")
                    .concat(strControl).concat(",").concat(strPrice).concat("|");
          }
        }

        hmReturn = gmLogis.updateItemConsign(strConsignId, strInputString, strShipFl, strUserId);
        if (!strComments.equals("")) {
          gmCommonBean.saveLog(strConsignId, strComments, strUserId, "1268");
        }
        hmResult = gmOper.loadSavedSetMaster(strConsignId);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmResult = gmOper.viewBuiltSetDetails(strConsignId);
        hmReturn.put("CONDETAILS", hmResult);
        alReturn = gmCommonBean.getLog(strConsignId, "1268");
        log.debug("alReturn====>" + alReturn);
        request.setAttribute("hmLog", alReturn);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "EditLoad");
        request.setAttribute("hConsignId", strConsignId);
        request.setAttribute("hShipFl", strShipFl);
        strDispatchTo = strOperPath.concat("/GmItemConsignEdit.jsp");
      } else if (strAction.equals("LoadItemShip")) {
        strConsignId =
            (String) session.getAttribute("strSessConsignId") == null ? "" : (String) session
                .getAttribute("strSessConsignId");
        hmResult = gmOper.loadSavedSetMaster(strConsignId);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmResult = gmLogis.loadConsignDetails(strConsignId);
        hmReturn.put("SETDETAILS", hmResult);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hMode", "SHIP");
        hmResult = gmCust.loadShipping();
        request.setAttribute("hmShipLoad", hmResult);
        strDispatchTo = strOperPath.concat("/GmConsignShip.jsp");
      } else if (strAction.equals("PicSlip")) {
        String strSource = GmCommonClass.parseNull(request.getParameter("ruleSource"));
        String strRefId = GmCommonClass.parseNull(request.getParameter("refId"));
        String strAddId = GmCommonClass.parseNull(request.getParameter("addressid"));
        String strRemVoidFl = GmCommonClass.parseNull(request.getParameter("hRemVoidFl"));
        // hRemVoidFl parameter is coming from status log report.This report should open PicSlip for
        // voided txns also.
        log.debug("ruleSource .......= " + strSource);
        strConsignId =
            request.getParameter("hConsignId") == null ? "" : request.getParameter("hConsignId");
        hmParam.put("CONSIGNID", strConsignId);
        hmParam.put("TYPE", strType);
        hmParam.put("REMVOIDFL", strRemVoidFl);
        hmResult = gmLogis.viewInHouseTransItemsDetails(hmParam);
        log.debug("hmResult 1= " + hmResult);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmResult = gmLogis.viewInHouseTransDetails(strConsignId);
        log.debug("hmResult 2= " + hmResult);
        hmReturn.put("CONDETAILS", hmResult);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("source", "50183"); // Loaner Extn
        log.debug("printing pic slip");

        String strReForward =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty("PICK_SLIP.ITEM_REFORWARD"));

        alReturn = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strConsignId, "1268"));
        if (alReturn.size() > 0) {
          for (int i = 0; i < alReturn.size(); i++) {
            HashMap hmLog = (HashMap) alReturn.get(i);
            strComments += GmCommonClass.parseNull((String) hmLog.get("COMMENTS"));
            strComments += "<BR>";
          }
          request.setAttribute("CRCOMMENTS", strComments);
        }
        if (strReForward.equals("gmItemPicSlip")) {
        	  if(strCompanyId.equals("1020")){
        		  strDispatchTo = strCustPath.concat("/GmITItemPicSlip.jsp");
        	  }else{
        		  strDispatchTo = strCustPath.concat("/GmItemPicSlip.jsp");
        	  }
          
        } else {
          strDispatchTo = strCustPath.concat("/GmItemPicSlipGlobal.jsp");
        }

        if (strSource.equals("50162") || strSource.equals("50161") || strSource.equals("50158")
            || strSource.equals("50157") || strSource.equals("50160") || strSource.equals("40054")
            || strSource.equals("50155") || strSource.equals("50183")) {

          if (strSource.equals("50155") || strSource.equals("50157") || strSource.equals("50158")) {
            strLoanerType = gmRuleBean.fetchLoanerType(strRefId);
          }
          strReTxn = gmRuleBean.fetchTransId(strSource, strLoanerType);
          if (strSource.equals("50162") || strSource.equals("50161")) {
            strReTxn = "SETRECONFIGWRAPPER";
          }
          if (strSource.equals("50183")) {
            strReTxn = "SHIPOUTWRAPPER";
            request.setAttribute("SOURCE", strSource);
            request.setAttribute("ADDRESSID", strAddId);
            request.setAttribute("REFID", strConsignId);
          }
          log.debug("strTrans = " + strReTxn);
          log.debug("strLoanerType = " + strLoanerType);
          request.setAttribute("RE_FORWARD", strReForward);
          request.setAttribute("RE_TXN", strReTxn);
          request.setAttribute("txnStatus", "PROCESS");
          strDispatchTo = "/gmRuleEngine.do";
        }

        strOpt = "";
      } else if (strAction.equals("ByTransaction")) {// validation call for transaction by type
        strTransIds = request.getParameter("Src_Trans_Id");
        strType = request.getParameter("Trans_Type");
        String strErrMsg = gmReturn.validateCopyTransactions(strTransIds, strType);
        strOpt = "";
        response.setContentType("text/xml");
        PrintWriter pw = response.getWriter();
        pw.write(strErrMsg);
        pw.flush();
        pw.close();
        strDispatchTo = null;
      }

      if (strMode.equals("InHouse") || strOpt.equals("InHouse") || strMode.equals("Repack")
          || strOpt.equals("Repack") || strMode.equals("QuaranIn") || strOpt.equals("QuaranIn")
          || strMode.equals("QuaranOut") || strOpt.equals("QuaranOut") || strOpt.equals("InvenIn")
          || strOpt.equals("BulkOut") || strOpt.equals("BulkIn") || strOpt.equals("RawMatScrap")
          || strOpt.equals("RawMatQuar") || strOpt.equals("RawMatFG") || strOpt.equals("FGRawMat")) {
        strDispatchTo = strOperPath.concat("/GmInHouseItemConsignSetup.jsp");
      }

      String strLoanerPaperworkFlag =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("LOANER_PAPERWORK_FLAG"));
      String strRuleSrc = GmCommonClass.parseNull(request.getParameter("ruleSource"));
      if (strLoanerPaperworkFlag.equals("YES") && strAction.equals("PicSlip")
          && strRuleSrc.equals("")) {
        strDispatchTo = strCustPath.concat("/GmPackSlipJapan.jsp");
      }

      if (strAction.equals("EditControl") || strAction.equals("SaveControl")
          || strAction.equals("LoadItemShip") || strAction.equals("PicSlip")) {
        dispatch(strDispatchTo, request, response);
      } else {
        gotoPage(strDispatchTo, request, response);
      }


    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmConsignInHouseServlet
