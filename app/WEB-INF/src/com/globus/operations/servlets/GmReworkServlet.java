/*****************************************************************************
 * File : GmReworkServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.operations.beans.GmInvReportBean;
import com.globus.operations.beans.GmPurchaseBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmReworkServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strDispatchTo = strOperPath.concat("/GmReworkOrder.jsp");

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strOpt = request.getParameter("hOpt");

      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;

      GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
      GmPurchaseBean gmPurc = new GmPurchaseBean(getGmDataStoreVO());
      GmInvReportBean gmInv = new GmInvReportBean(getGmDataStoreVO());

      String strProjId = "";
      String strVendorId = "";
      String strSearch = "";
      String strPartNum = "";
      String strPartNumFormat = "";
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strVendorCurrecny = "";

      HashMap hmParams = new HashMap();
      HashMap hmReturn = new HashMap();
      ArrayList alReturn = new ArrayList();

      if (strAction.equals("Load") && strOpt.equals("Rework")) {
        HashMap hmTemp = new HashMap();
        hmTemp.put("COLUMN", "ID");
        hmTemp.put("STATUSID", "20301"); // Filter all approved projects

        alReturn = gmProject.reportProject(hmTemp);
        session.setAttribute("PROJECTS", alReturn);
        hmReturn = gmPurc.getVendorList("");
        session.setAttribute("VENDORS", hmReturn.get("VENDORLIST"));
        request.setAttribute("hAction", strAction);
      } else if (strAction.equals("Load") && strOpt.equals("Sterlize")) {
        hmReturn = gmPurc.getVendorList("3008");
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        strDispatchTo = strOperPath.concat("/GmSterilizeOrder.jsp");
      } else if (strAction.equals("SterlizeLoad")) {
        strVendorId = request.getParameter("hVenId") == null ? "" : request.getParameter("hVenId");
        hmReturn = gmPurc.getDHRForSterilization(strVendorId);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hVendId", strVendorId);
        strDispatchTo = strOperPath.concat("/GmSterilizeOrder.jsp");
      } else if (strAction.equals("PriceLoad")) {
        strProjId = GmCommonClass.parseNull(request.getParameter("hProjId"));
        strVendorId = GmCommonClass.parseNull(request.getParameter("hVendId"));
        // Added code for PMT-384 rework on PO
        strSearch = GmCommonClass.parseNull(request.getParameter("Cbo_Search"));
        strPartNum = GmCommonClass.parseNull(request.getParameter("Txt_PartNum"));

        if (!strSearch.equals("") && !strPartNum.equals("")) {
          hmParams.put("PARTNUM", strPartNum);
          hmParams.put("SEARCH", strSearch);

          strPartNumFormat = GmCommonClass.createRegExpString(hmParams);
        }

        hmParams.put("PNUM", strPartNumFormat);
        hmParams.put("PROJID", strProjId);

        alReturn = gmInv.getPartNumInventory(hmParams);
        hmReturn.put("VENDORPRICELIST", alReturn);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hProjId", strProjId);
        request.setAttribute("hVendId", strVendorId);
        request.setAttribute("hSearch", strSearch);
        request.setAttribute("hPartNum", strPartNum);
      }
      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmReworkServlet
