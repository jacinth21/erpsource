/*****************************************************************************
 * File : GmPOReportServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.beans.GmPurchaseBean;
import com.globus.operations.beans.GmVendorBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmPOReportServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strDispatchTo = strOperPath.concat("/GmPOReport.jsp");
    String strGridXmlData = "";
    HashMap hmParams = new HashMap();
    try {
      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strOpt = request.getParameter("hOpt");

      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

      GmVendorBean gmVendor = new GmVendorBean(getGmDataStoreVO());
      GmPurchaseBean gmPurc = new GmPurchaseBean(getGmDataStoreVO());
      GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
      GmCommonClass gmCommon = new GmCommonClass();

      HashMap hmReturn = new HashMap();
      HashMap hmResult = new HashMap();
      HashMap hmTemp = new HashMap();
      HashMap hmParam = new HashMap();
      ArrayList alUsers = new ArrayList();
      ArrayList alTypes = new ArrayList();
      ArrayList alReturn = new ArrayList();
      ArrayList alPODetails = new ArrayList();
      ArrayList alPOAct = new ArrayList();
      ArrayList alResult = new ArrayList();
      RowSetDynaClass rowSetPOReport = null;

      GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
      HashMap hmProTemp = new HashMap();
      hmProTemp.put("COLUMN", "ID");
      hmProTemp.put("STATUSID", "20301"); // Filter all approved projects
      ArrayList alProject = GmCommonClass.parseNullArrayList(gmProj.reportProject(hmProTemp));
      request.setAttribute("ALPROJECT", alProject);



      hmTemp.put("CHECKACTIVEFL", "N"); // check for active flag status in vendor list
      GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
      String strVendorId = "";
      String strTodaysDate = GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt()); // (String)
                                                                                                   // session.getAttribute("strSessTodaysDate");
      String strMonthBeginDate = "";
      strMonthBeginDate =
          GmCalenderOperations.getFirstDayOfMonth(GmCalenderOperations.getCurrentMonth() - 1,
              getGmDataStoreVO().getCmpdfmt());// strTodaysDate.replaceAll("/\\d\\d/", "/01/");
      String strFromDt = "";
      String strToDt = "";
      String strPartNums = "";
      String strId = "";
      String strPOType = "";
      String strUserId = "";
      String strAmount = "";
      String strProjId = "";
      String strPartNum = "";
      String strSearch = "";
      String strPartNumFormat = "";
      String strTypeOfPO = "";
      String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
      if (strAction.equals("Load") && strOpt.equals("Vendor")) {
        hmResult = gmVendor.getVendorList(hmTemp);
        hmReturn.put("VENDORLIST", hmResult.get("VENDORLIST"));
        alUsers = gmLogon.getEmployeeList("300", "W");
        hmReturn.put("USERLIST", alUsers);
        alPOAct = GmCommonClass.parseNullArrayList(gmCommon.getCodeList("POACT"));
        alTypes = gmCommon.getCodeList("POTYP");
        hmReturn.put("POTYPE", alTypes);
        hmReturn.put("POACTION", alPOAct);

        hmParam.put("FRMDT", strMonthBeginDate);
        hmParam.put("TODT", strTodaysDate);
       // Below code commented to show type of PO is All Types (PC-3255)
       // hmParam.put("POTYPE", "3100");
        hmParam.put("VENDID", "");

        hmResult = gmPurc.getPOList(hmParam);
        hmReturn.put("REPORT", hmResult.get("POLIST"));
        hmReturn.put("gmDataStoreVO", getGmDataStoreVO());
        hmReturn.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
        strGridXmlData = gmPurc.getXmlGridData(hmReturn);
        log.debug("hmReturn" + hmReturn);
      } else if (strAction.equals("Load") && strOpt.equals("Part")) {
        strDispatchTo = strOperPath.concat("/GmPOReportByPart.jsp");
      } else if (strAction.equals("Load") && strOpt.equals("PendVend")) {
        hmReturn = gmVendor.getVendorList(hmTemp);
        session.setAttribute("VENDORLIST", hmReturn.get("VENDORLIST"));
        alTypes = gmCommon.getCodeList("POTYP");
        alPOAct = GmCommonClass.parseNullArrayList(gmCommon.getCodeList("POACT"));
        hmReturn.put("POACTION", alPOAct);
        session.setAttribute("POTYPELIST", alTypes);
        alUsers = gmLogon.getEmployeeList("300", "W");
        session.setAttribute("USERLIST", alUsers);
        strDispatchTo = strOperPath.concat("/GmPOReportPendVendor.jsp");

        // hmReturn = gmPurc.getPOPendReportByVendor("Pending");
        // request.setAttribute("hmReturn",hmReturn);
      } else if (strAction.equals("Go") && strOpt.equals("PendVend")) {
        RowSetDynaClass resultSet = null;
        strPartNum = GmCommonClass.parseNull(request.getParameter("hPartNum"));
        strProjId = GmCommonClass.parseNull(request.getParameter("Cbo_ProjId"));
        strSearch = GmCommonClass.parseNull(request.getParameter("Cbo_Search"));
        strTypeOfPO = GmCommonClass.parseNull(request.getParameter("hTypeId"));
        strUserId = GmCommonClass.parseNull(request.getParameter("Cbo_UserId"));
        hmParams.put("PARTNUM", strPartNum);
        hmParams.put("SEARCH", strSearch);
        strPartNumFormat = GmCommonClass.createRegExpString(hmParams);
        hmParam.put("SESSDATEFMT", strApplnDateFmt);


        strVendorId =
            request.getParameter("hVendorId") == null ? "" : request.getParameter("hVendorId");
        strPOType =
            request.getParameter("Cbo_Type") == null ? "" : request.getParameter("Cbo_Type");
        strDispatchTo = strOperPath.concat("/GmPOReportPendVendor.jsp");

        hmParam.put("STRTYPE", strPOType);
        hmParam.put("STRVENDORID", strVendorId);
        hmParam.put("STRPARTNUM", strPartNumFormat);
        hmParam.put("STRPROJID", strProjId);
        hmParam.put("STRTYPEOFPO", strTypeOfPO);
        hmParam.put("EMPID", strUserId);

        // the below code is added for PMT-4801 and the return type is changed to RowSetDynaClass
        // which is the fix for sorting the columns in wrapper.
        // alReturn = (ArrayList)gmPurc.getPOPendReportByVendor(hmParam).getRows();
        //resultSet = gmPurc.getPOPendReportByVendor(hmParam);
        alResult = gmPurc.getPOPendReportByVendor(hmParam);

        hmParam.put("alReturn", alResult);
        strGridXmlData = gmPurc.getPOPendVenGridData(hmParam);
        request.setAttribute("RSDPOREPORT", rowSetPOReport);
        request.setAttribute("hVendorId", strVendorId);
        request.setAttribute("alReturn", alReturn);
        request.setAttribute("alPODetails", alPODetails);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hType", strPOType);
        request.setAttribute("hProjId", strProjId);
        request.setAttribute("hPartNum", strPartNum);
        request.setAttribute("hSearch", strSearch);
        request.setAttribute("hTypeId", strTypeOfPO);
        request.setAttribute("hmParam", hmParam);
        request.setAttribute("hmResult", hmResult);
        request.setAttribute("GRIDXMLDATA", strGridXmlData);
      } else if (strAction.equals("Go")) {
        strFromDt = GmCommonClass.parseNull(request.getParameter("Txt_FromDate"));;
        strToDt = GmCommonClass.parseNull(request.getParameter("Txt_ToDate"));
        strPOType = GmCommonClass.parseNull(request.getParameter("hPOType"));
        strVendorId = GmCommonClass.parseNull(request.getParameter("hVendorId"));
        strUserId = GmCommonClass.parseNull(request.getParameter("Cbo_UserId"));
        strId = GmCommonClass.parseNull(request.getParameter("Txt_POId"));
        strAmount = GmCommonClass.parseNull(request.getParameter("Txt_POAmt"));

        hmParam.put("FRMDT", strFromDt);
        hmParam.put("TODT", strToDt);
        hmParam.put("POTYPE", strPOType);
        hmParam.put("VENDID", strVendorId);
        hmParam.put("USERID", strUserId);
        hmParam.put("POID", strId);
        hmParam.put("POAMT", strAmount);

        if (strOpt.equals("Vendor")) {
          hmResult = gmVendor.getVendorList(hmTemp);
          hmReturn.put("VENDORLIST", hmResult.get("VENDORLIST"));
          alUsers = gmLogon.getEmployeeList("300", "W");
          hmReturn.put("USERLIST", alUsers);
          alTypes = gmCommon.getCodeList("POTYP");
          alPOAct = GmCommonClass.parseNullArrayList(gmCommon.getCodeList("POACT"));
          hmReturn.put("POACTION", alPOAct);
          hmReturn.put("POTYPE", alTypes);

          hmParam.put("FRMDT", strFromDt);
          hmParam.put("TODT", strToDt);
          hmParam.put("POTYPE", strPOType);
          hmParam.put("VENDID", strVendorId);

          hmResult = gmPurc.getPOList(hmParam);
          hmReturn.put("REPORT", hmResult.get("POLIST"));
          hmReturn.put("gmDataStoreVO", getGmDataStoreVO());
          strGridXmlData = gmPurc.getXmlGridData(hmReturn);

        } else if (strOpt.equals("Part")) {
          strPartNums =
              request.getParameter("Txt_PartNum") == null ? "" : request
                  .getParameter("Txt_PartNum");
          int intCnt = 0;
          intCnt = strPartNums.indexOf(",");

          if (intCnt != -1) {
            strId = "'".concat(strPartNums).concat(",");
            strId = strId.replaceAll(",", "','");
            strId = strId.substring(0, strId.length() - 2);
          } else {
            strId = "'".concat(strPartNums) + "'";
          }
          hmReturn = gmPurc.getPOReport(strId, strFromDt, strToDt);
          hmReturn.put("gmDataStoreVO", getGmDataStoreVO());
          strGridXmlData = gmPurc.getPOXmlGridData(hmReturn);
          request.setAttribute("hPartNums", strPartNums);
          strDispatchTo = strOperPath.concat("/GmPOReportByPart.jsp");

        }
      }
      request.setAttribute("hOpt", strOpt);
      request.setAttribute("hAction", strAction);
      request.setAttribute("hPOType", strPOType);
      request.setAttribute("hFrmDt", strFromDt);
      request.setAttribute("hToDt", strToDt);
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("hmParam", hmParam);
      request.setAttribute("GRIDXMLDATA", strGridXmlData);
      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmPOReportServlet
