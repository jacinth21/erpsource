/*****************************************************************************
 * File : GmInvListReportServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.operations.beans.GmInvReportBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmInvListReportServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    final String strComnPath = GmCommonClass.getString("GMCOMMON");
    final String strInvPath = GmCommonClass.getString("GMINVENTORY");
    String strDispatchTo = "";

    HashMap hmParams = new HashMap();
    HashMap hmQueryParams = new HashMap();

    try {
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.
      RowSetDynaClass resultSet = null;

      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;



      String strOpt = request.getParameter("hOpt");



      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
        log.debug("session strOpt hopt:" + strOpt);
      }
      strOpt = (strOpt == null) ? "" : strOpt;



      GmInvReportBean gmInv = new GmInvReportBean(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      ArrayList alReturn = new ArrayList();

      GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
      HashMap hmTemp = new HashMap();
      hmTemp.put("COLUMN", "ID");
      hmTemp.put("STATUSID", "20301"); // Filter all approved projects
      ArrayList alProject = GmCommonClass.parseNullArrayList(gmProj.reportProject(hmTemp));
      request.setAttribute("ALPROJECT", alProject);

      String strUserId = (String) session.getAttribute("strSessUserId");
      String strProjId = "";
      String strPartNum = "";
      String strSearch = "";
      String strPartNumFormat = "";
      String strType = "";

      if (strAction.equals("Load")) {
        if (strOpt.equals("BuiltSet")) {
          strDispatchTo = strInvPath.concat("/GmBuiltSetListReport.jsp");
          resultSet = gmInv.getBuiltSetListReport();
        }
        if (strOpt.equals("BuiltSetParts")) {
          strDispatchTo = strInvPath.concat("/GmBuiltSetPartsReport.jsp");
          // empty list initially
          alReturn = new ArrayList(); // gmInv.getBuiltSetPartsReport();
          request.setAttribute("ALREPORT", alReturn);
        } else if (strOpt.equals("ReturnsHold")) {
          strDispatchTo = strInvPath.concat("/GmQuarantineListReport.jsp");
          // resultSet = null;// gmInv.getReturnsHoldListReport();
        } else if (strOpt.equals("Quarantine")) {
          strDispatchTo = strInvPath.concat("/GmQuarantineListReport.jsp");
          // resultSet = null; // gmInv.getQuarantineListReport();
        } else if (strOpt.equals("Rework")) {
          strDispatchTo = strInvPath.concat("/GmReworkListReport.jsp");
          // resultSet = null;//gmInv.getReworkPartsListReport();
        } else if (strOpt.equals("InHouse") || strOpt.equals("Loaner")) {
          strType = strOpt.equals("InHouse") ? "4119" : "4127";
          strDispatchTo = strInvPath.concat("/GmInHouseListReport.jsp");
          resultSet = gmInv.getLoanerSetsListReport(strType);
        } else if (strOpt.equals("InHousePart") || strOpt.equals("LoanerPart")) {
          strType = strOpt.equals("InHousePart") ? "4119" : "4127";
          strDispatchTo = strInvPath.concat("/GmLoanerPartsReport.jsp");
          // resultSet = null;//gmInv.getLoanerPartsListReport(strType);
        } else if (strOpt.equals("ReturnsPart")) {
          strDispatchTo = strInvPath.concat("/GmReturnsPartsReport.jsp");
          // resultSet = null; // gmInv.getReturnsPartsListReport();
        } else if (strOpt.equals("Returns")) {
          strDispatchTo = strInvPath.concat("/GmReturnsListReport.jsp");
          resultSet = gmInv.getReturnsListReport();
        } else if (strOpt.equals("ManfPart")) {
          strDispatchTo = strInvPath.concat("/GmManfListReport.jsp");
          alReturn = gmInv.getPartNumInventory("GM00-03");
          request.setAttribute("ALREPORT", alReturn);
        }
      } else if (strAction.equals("Go")) {
        strPartNum = GmCommonClass.parseNull(request.getParameter("hPartNum"));
        strSearch = GmCommonClass.parseNull(request.getParameter("Cbo_Search"));
        strProjId = GmCommonClass.parseNull(request.getParameter("Cbo_ProjId"));

        hmParams.clear();
        hmParams.put("PARTNUM", strPartNum);
        hmParams.put("SEARCH", strSearch);


        strPartNumFormat = GmCommonClass.createRegExpString(hmParams);

        log.debug("Regex is : " + strPartNumFormat);

        hmQueryParams.put("REGEXPATTERN", strPartNumFormat);
        hmQueryParams.put("PROJID", strProjId);


        // above block with new params to bean method. Query methods changed.

        if (strOpt.equals("BuiltSetParts")) {
          strDispatchTo = strInvPath.concat("/GmBuiltSetPartsReport.jsp");
          alReturn = gmInv.getBuiltSetPartsReport(hmQueryParams);
          
         //PMT-22758
          ArrayList alReturnParts = new ArrayList(); 
          alReturnParts = gmInv.getBuiltSetPartsReport(hmQueryParams);
          int alReturnSize = alReturnParts.size();
          HashMap<String, String> hmBuiltSetParts = new HashMap();
          String strPartDescTmp = "";
          String strPartDesc = "";
          ArrayList alReturnBuiltParts = new ArrayList();
          if(alReturnSize > 0){
          for(int i = 0; i < alReturnSize; i++){
        	  hmBuiltSetParts = (HashMap) alReturnParts.get(i) ;
        	  strPartDescTmp = GmCommonClass.parseNull((String)hmBuiltSetParts.get("PDESC")); 
        	  strPartDesc = GmCommonClass.getStringWithTM(strPartDescTmp);
        	  hmBuiltSetParts.replace("PDESC", strPartDesc);
        	  alReturnBuiltParts.add(hmBuiltSetParts);
        	  
       }
          request.setAttribute("ALREPORT", alReturnBuiltParts);
        }
        }
       
        if (strOpt.equals("Quarantine")) {
          strDispatchTo = strInvPath.concat("/GmQuarantineListReport.jsp");
          resultSet = gmInv.getQuarantineListReport(hmQueryParams);
        }

        else if (strOpt.equals("Rework")) {
          strDispatchTo = strInvPath.concat("/GmReworkListReport.jsp");
          resultSet = gmInv.getReworkPartsListReport(hmQueryParams);
        }

        else if (strOpt.equals("ReturnsHold")) {
          strDispatchTo = strInvPath.concat("/GmQuarantineListReport.jsp");
          resultSet = gmInv.getReturnsHoldListReport(hmQueryParams);
        }

        else if (strOpt.equals("ReturnsPart")) {
          strDispatchTo = strInvPath.concat("/GmReturnsPartsReport.jsp");
          resultSet = gmInv.getReturnsPartsListReport(hmQueryParams);
        }

        else if (strOpt.equals("InHousePart") || strOpt.equals("LoanerPart")) {

          strType = strOpt.equals("InHousePart") ? "4119" : "4127";
          strDispatchTo = strInvPath.concat("/GmLoanerPartsReport.jsp");
          hmQueryParams.put("TYPE", strType);
          resultSet = gmInv.getLoanerPartsListReport(hmQueryParams);
        }
      }// end Go

      hmReturn.put("USERID", strUserId);
      request.setAttribute("REPORT", resultSet);
      request.setAttribute("hAction", strAction);
      request.setAttribute("hProjId", strProjId);
      request.setAttribute("hPartNum", strPartNum);
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("hOpt", strOpt);
      request.setAttribute("hSearch", strSearch);

      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmInvListReportServlet
