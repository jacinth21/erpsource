/*****************************************************************************
 * File : GmPOServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.servlets.GmServlet;
import com.globus.jms.consumers.beans.GmInitiateVendorJMS;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.beans.GmPurchaseBean;
import com.globus.operations.beans.GmVendorBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmPOServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strDispatchTo = strOperPath.concat("/GmPOView.jsp");

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      String strAction = request.getParameter("hAction");
      String strCloseFlag = request.getParameter("hCloseFlag");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      GmCommonClass gmCommon = new GmCommonClass();
      GmVendorBean gmVendor = new GmVendorBean(getGmDataStoreVO());
      GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
      GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
      GmPurchaseBean gmPurc = new GmPurchaseBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
      GmInitiateVendorJMS gmInitiateVendorJMS = new GmInitiateVendorJMS(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      HashMap hmAccess = new HashMap();
      HashMap hmParam = new HashMap();
      ArrayList alReturn = new ArrayList();
      ArrayList alPoList = new ArrayList();
      String strPOId = "";
      String strVendorId = "";
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strReqDate = "";
      String strComments = "";
      String strDocRev = "";
      String strDocFl = "";
      String strJspFile = "";
      String strInputStr = "";
      String strPOTotal = "";
      String strLogReason = "";
      String strPOAction = "";
      String strPublishedfl = "";
      String strhPublishFL = "";      
      String strCompanyInfo = "";
      String strPartyId = (String) session.getAttribute("strSessPartyId");
      String alPOType= "";

      String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
      GmResourceBundleBean gmResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);

      hmAccess = gmAccessControlBean.getAccessPermissions(strUserId, "ADD_INSTR_ACC");// To get the
                                                                                      // access for
                                                                                      // showing/hiding
                                                                                      // comments
                                                                                      // textbox in
                                                                                      // GmPOView.jsp
      String strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      request.setAttribute("ACCESSFL", strAccessFlag);
      //Access based Edit PO of Purchasing Module based on security group (PMT-52276)
      // getting edit po access based on the user loged in and security event
      hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "PO_EDIT_GRP");
      String strUpdateAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      request.setAttribute("POEDITACCESSFL", strUpdateAccessFlag);
      if (strAction.equals("Load")) {

      } else if (strAction.equals("ViewPO")) {
        strPOId = request.getParameter("hPOId") == null ? "" : request.getParameter("hPOId");
        strVendorId = request.getParameter("hVenId") == null ? "" : request.getParameter("hVenId");
        hmReturn = gmPurc.viewPO(strPOId, strVendorId, strUserId);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hCloseFlag", strCloseFlag);
      } else if (strAction.equals("PrintPO")) {
        strPOId = request.getParameter("hPOId") == null ? "" : request.getParameter("hPOId");
        strVendorId = request.getParameter("hVenId") == null ? "" : request.getParameter("hVenId");
        hmReturn = gmPurc.viewPO(strPOId, strVendorId, strUserId);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hPOId", strPOId);
        strDispatchTo = strOperPath.concat("/GmPOPrint.jsp");
      } else if (strAction.equals("PrintAllWO")) {
        strPOId = request.getParameter("hPOId") == null ? "" : request.getParameter("hPOId");
        strDocRev = request.getParameter("hDocRev") == null ? "" : request.getParameter("hDocRev");
        strDocFl = request.getParameter("hDocFl") == null ? "" : request.getParameter("hDocFl");

        alReturn = gmPurc.getAllWOForPrint(strPOId);
        hmReturn.put("SUBREPORT_DIR",
            session.getServletContext().getRealPath(GmCommonClass.getString("GMJASPERLOCATION"))
                + "\\");
        hmReturn.put("ALLWO", alReturn);
        HashMap hmCompanyAddress =
            GmCommonClass.fetchCompanyAddress(getGmDataStoreVO().getCmpid(), "");
        hmReturn.put("LOGO", GmCommonClass.parseNull((String) hmCompanyAddress.get("COMP_LOGO")));
        hmReturn.put("gmResourceBundleBean", gmResourceBundleBean);
        request.setAttribute("hmReturn", hmReturn);
        // The below changes is added for UDI-16,Based on PO created Date GmWorkOrderPrint.jasper
        // will be called or GmWoPrintAllD.jsp
        if (strDocFl.equals("Y")) {
          String strRptName =
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("WO.JASPERNM"));
          request.setAttribute("STRRPTNAME", strRptName);
          strJspFile = "/GmWOPrintAll";
        } else {
          strJspFile = "/GmWOPrintAll".concat(strDocRev);
        }
        strDispatchTo = strOperPath.concat(strJspFile).concat(".jsp");

      } else if (strAction.equals("PrintFax")) {
        strPOId = request.getParameter("hPOId") == null ? "" : request.getParameter("hPOId");
        hmReturn = gmPurc.getFaxCoverDetails(strPOId);
        hmReturn.put("FAX", hmReturn);
        request.setAttribute("hmReturn", hmReturn);
        strDispatchTo = strOperPath.concat("/GmPOFaxCover.jsp");
      } else if (strAction.equals("Save")) {
        strPOId = request.getParameter("hPOId") == null ? "" : request.getParameter("hPOId");
        strVendorId = request.getParameter("hVenId") == null ? "" : request.getParameter("hVenId");
        strReqDate =
            request.getParameter("Txt_ReqDate") == null ? "" : (String) request
                .getParameter("Txt_ReqDate");
        strComments =
            request.getParameter("Txt_Comments") == null ? "" : (String) request
                .getParameter("Txt_Comments");
        strPOTotal =
            request.getParameter("hPOTotal") == null ? "" : (String) request
                .getParameter("hPOTotal"); 
        alPOType =
                request.getParameter("hPOType") == null ? "" : (String) request
                    .getParameter("hPOType");
        
        strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
        
        hmParam.put("REFID", strPOId);
        hmParam.put("REFTYPE", "108440"); //purchase order type
        hmParam.put("REFACTION", "108400"); //update Purchase Order Action
        hmParam.put("MAILSTATUS", "0"); //to send instant mail
        hmParam.put("USERID", strUserId);
        hmParam.put("COMPANYINFO", strCompanyInfo);
        hmParam.put("POTYPE", alPOType);
 
        strPublishedfl = GmCommonClass.parseNull((String) request.getParameter("Chk_PublishFl"));
        hmReturn = gmPurc.updatePO(strPOId, strPOTotal, strReqDate, strComments, strPublishedfl, strUserId, alPOType);
        strLogReason = GmCommonClass.parseNull((String) request.getParameter("Txt_LogReason"));               
        strhPublishFL =  GmCommonClass.parseNull((String) request.getParameter("hPublishFL"));
        if(strPublishedfl.equals("Y") && strhPublishFL == ""){
        	strLogReason = strLogReason+"Published";
        	gmInitiateVendorJMS.initVendorEmailJMS(hmParam);	// Po jms call
        }
        if (!strLogReason.equals("")) {
          gmCommonBean.saveLog(strPOId, strLogReason, strUserId, "1203"); // 1203 type is PO CallLog
        }
        hmReturn = gmPurc.viewPO(strPOId, strVendorId, strUserId);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "ViewPO");
      } else if (strAction.equals("EditPO")) {
        strPOId = request.getParameter("hPOId") == null ? "" : request.getParameter("hPOId");
        strVendorId = request.getParameter("hVenId") == null ? "" : request.getParameter("hVenId");
        hmReturn = gmPurc.viewPO(strPOId, strVendorId, strUserId);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "ViewPO");
        alPoList = GmCommonClass.parseNullArrayList(gmCommon.getCodeList("POOPT", getGmDataStoreVO()));
        hmReturn.put("ALPOLIST", alPoList);


        // To get the call log information
        alReturn = gmCommonBean.getLog(strPOId);
        request.setAttribute("hmLog", alReturn);
        strDispatchTo = strOperPath.concat("/GmPOEdit.jsp");
      } else if (strAction.equals("UpdatePO")) {
    	   strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
        strPOId = request.getParameter("hPOId") == null ? "" : request.getParameter("hPOId");
        strReqDate =
            request.getParameter("Txt_ReqDate") == null ? "" : (String) request
                .getParameter("Txt_ReqDate");
        strComments =
            request.getParameter("Txt_Comments") == null ? "" : (String) request
                .getParameter("Txt_Comments");
        strPOTotal =
            request.getParameter("hPOTotal") == null ? "" : (String) request
                .getParameter("hPOTotal");
        strInputStr =
            request.getParameter("hWOString") == null ? "" : (String) request
                .getParameter("hWOString");
        alPOType =
                request.getParameter("hPOType") == null ? "" : (String) request
                    .getParameter("hPOType");
        
        strPOAction =
            request.getParameter("Cbo_PO_Action") == null ? "" : (String) request
                .getParameter("Cbo_PO_Action");       
        strPublishedfl = GmCommonClass.parseNull((String) request.getParameter("Chk_PublishFl"));
        // As strPOTotal is not passed from UI, we will first update the Work Order and update the
        // PO table later.
        // Code modified for MNTTASK-1876
        if (strPOAction.equals("106426")) { //update WO
        hmReturn = gmPurc.updateWO(strInputStr, strUserId); // For Saving WO - July 13
        String strMsg = GmCommonClass.parseNull((String) hmReturn.get("MSG"));
        if (!strMsg.equals("")) {
          request.setAttribute("hType", "S");
          throw new AppError("" + strMsg + "", "", 'S');
        }
        }else if(strPOAction.equals("106425")) {   // Update Part # Attribute
            gmPurc.updateWOPartDetails(strInputStr, strUserId);
        }
        
        hmParam.put("REFID", strPOId);
        hmParam.put("REFTYPE", "108440"); //purchase order type
        hmParam.put("REFACTION", "108405"); //update Purchase Order Action
        hmParam.put("MAILSTATUS", "0"); //to send instant mail
        hmParam.put("USERID", strUserId);
        hmParam.put("COMPANYINFO", strCompanyInfo);
        hmParam.put("POTYPE",alPOType);
        if(strPublishedfl.equals("Y")){
        	hmParam.put("REFACTION", "108400");   //publish purchase order action 
        }
        log.debug("hmParam>>>>"+hmParam);
        if(strPOAction.equals("106426") || strPOAction.equals("106425")){
        	  hmReturn = gmPurc.updatePO(strPOId, strPOTotal, strReqDate, strComments, strPublishedfl, strUserId, alPOType); // Saving
																									// Details
        	  if(strPOAction.equals("106425")){
        		  hmParam.put("REFACTION", "26240620"); //WO order update action
        	  }
        	  log.debug("po servlet"+hmParam);
        	  gmInitiateVendorJMS.initVendorEmailJMS(hmParam);	// Po jms call
        }
      
        // To update the Call Log Information if any
        strLogReason =
            request.getParameter("Txt_LogReason") == null ? "" : request
                .getParameter("Txt_LogReason");
        
        strhPublishFL =  GmCommonClass.parseNull((String) request.getParameter("hPublishFL")); // To check whether the PO is already Published or not
        if(strPublishedfl.equals("Y") && strhPublishFL == ""){
        	strLogReason = strLogReason+", Published";
        }
        if (!strLogReason.equals("")) {
          gmCommonBean.saveLog(strPOId, strLogReason, strUserId, "1203");
        }

        hmReturn = gmPurc.viewPO(strPOId, strVendorId, strUserId);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "ViewPO");

        // To get the call log information
        alReturn = gmCommonBean.getLog(strPOId);
        request.setAttribute("hmLog", alReturn);
        
        alPoList = GmCommonClass.parseNullArrayList(gmCommon.getCodeList("POOPT", getGmDataStoreVO()));
        hmReturn.put("ALPOLIST", alPoList);
        hmReturn.put("POACTION", strPOAction);
        
        strDispatchTo = strOperPath.concat("/GmPOEdit.jsp");
      }
      log.debug("strDispatchTo :--> " + strDispatchTo);
      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmPOServlet
