/*****************************************************************************
 * File : GmSetBuildServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.operations.beans.GmLogisticsBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.requests.beans.GmRequestReportBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.operations.inventory.beans.GmInvLocationBean;


public class GmSetBuildServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strLogisPath = GmCommonClass.getString("GMLOGISTICS");
    String strDispatchTo = strOperPath.concat("/GmSetBuildSetup.jsp");
    String strStrutsPageURL = "";
    String strRequestId = "";
    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.

      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;


      String strOpt = request.getParameter("hOpt");
      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
        log.debug("session strOpt hopt:" + strOpt);
      }
      strOpt = (strOpt == null) ? "" : strOpt;
      if (strOpt.equals("SetReconfig")) {
        strAction = "SetReconfigure";
      }

      String strTodaysDate = (String) session.getAttribute("strSessTodaysDate");
      log.debug("Toda: " + strTodaysDate);

      GmCommonClass gmCommon = new GmCommonClass();
      GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
      GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
      GmLogisticsBean gmLogisticsBean = new GmLogisticsBean(getGmDataStoreVO());
      GmRequestReportBean gmRequestReportBean = new GmRequestReportBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
      GmInvLocationBean   gmInvLocationBean = new GmInvLocationBean();
      String strCompanyInfo=GmCommonClass.parseNull(request.getParameter("companyInfo"));
      HashMap hmReturn = new HashMap();
      HashMap hmResult = new HashMap();
      HashMap hmLists = new HashMap();
      ArrayList alReturn = new ArrayList();
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strPartNum = "";
      String strSetId = "";
      String strQty = "";
      String strControl = "";
      String strCount = "";
      String strConsignId = "";
      String strComments = "";
      String strFlag = "";
      String strVerifyFlag = "";
      String strMode = "";
      int intCount = 0;
      int trans_type =0;
      HashMap hmInnerLoop = new HashMap();
      HashMap hmLoop = new HashMap();
      HashMap hmAccess = new HashMap();
      ArrayList alLoop = new ArrayList();
      String strReturn = "";
      String strReloadId = "";
      String strReconfigAction;
      String strReleaseTo;
      boolean blnIsBackOrderPresent = false;

      String strBulk = "";
      String strSetQty = "";
      String strAddPartQty = "";

      log.debug(" Action is " + strAction);

      if (strAction.equals("Load")) {
        alReturn = gmProj.loadSetMap("1601");
        hmReturn.put("SETLIST", alReturn);
        request.setAttribute("hmReturn", hmReturn);
      } else if (strAction.equals("LoadSet")) {
        strSetId =
            request.getParameter("hSetId") == null ? "" : (String) request.getParameter("hSetId");
        strMode =
            request.getParameter("hMode") == null ? "SET" : (String) request.getParameter("hMode");
        alReturn = gmProj.loadSetMap("1601");
        hmReturn.put("SETLIST", alReturn);
        alReturn = gmProj.fetchSetMaping(strSetId, strMode);
        hmReturn.put("SETLOAD", alReturn);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hSetId", strSetId);
        request.setAttribute("hMode", strMode);
      } else if (strAction.equals("Initiate")) {
        strSetId = GmCommonClass.parseNull(request.getParameter("Cbo_Proj"));
        strMode = request.getParameter("hMode") == null ? "SET" : request.getParameter("hMode");

        HashMap hmParam = new HashMap();
        hmParam.put("SETID", strSetId);
        hmParam.put("USERID", strUserId);

        hmReturn = gmLogisticsBean.initiateSetBuild(hmParam);
        String strMaterialRequestId = (String) hmReturn.get("MATERIALREQUESTID");
        String strConsignmentId = (String) hmReturn.get("CONSIGNID");
        strStrutsPageURL =
            "/gmSetBuildProcess.do?requestId=" + strMaterialRequestId + "&consignID="
                + strConsignmentId;
        /*
         * hmReturn.put("CONDETAILS",hmReturn.get("CONDETAILS"));
         * 
         * alReturn = gmProj.loadSetMap("1601"); hmReturn.put("SETLIST",alReturn); alReturn =
         * gmProj.fetchSetMaping(strSetId,strMode); hmReturn.put("SETLOAD",alReturn);
         * 
         * request.setAttribute("hmReturn",hmReturn); request.setAttribute("hAction","EditLoad");
         * request.setAttribute("hSetId",strSetId);
         */
      } else if (strAction.equals("Save") || strAction.equals("Edit")) {
        strConsignId =request.getParameter("hConsignId") == null ? "" : (String) request
                	  .getParameter("hConsignId");
        strCount =
            request.getParameter("hCnt") == null ? "" : (String) request.getParameter("hCnt");
        strComments =
            request.getParameter("Txt_Comments") == null ? "" : (String) request
                .getParameter("Txt_Comments");
        strFlag = request.getParameter("Chk_Flag") == null ? "1" : "2";
        log.debug("strFlag" + strFlag);

        strVerifyFlag = request.getParameter("Chk_VerFlag") == null ? "0" : "1";
        strFlag = strVerifyFlag.equals("1") ? "2" : strFlag;

        intCount = Integer.parseInt(strCount);
        int intcnt = 0;
        String strInt = "";
        String strInputString = "";
        for (int i = 0; i <= intCount; i++) {
          strPartNum =
              request.getParameter("hPartNum" + i) == null ? "" : (String) request
                  .getParameter("hPartNum" + i);
          if (!strPartNum.equals("")) {
            strQty = request.getParameter("Txt_Qty" + i);
            strQty = strQty.equals("") ? "0" : strQty;
            strControl = request.getParameter("Txt_CNum" + i);
            strControl = strControl.equals("") ? " " : strControl;
            strInputString = strInputString + strPartNum + "^" + strQty + "^" + strControl + "|";
          }
        }

        hmReturn =
            gmOper.saveSetMaster(strConsignId, strInputString, strComments, strFlag, strVerifyFlag,
                strUserId);
        
      
     String strTransType = GmCommonClass.parseNull((String)hmReturn.get("TXNTYPE"));
        HashMap hmLnParam = new HashMap ();
        hmLnParam.put("TRANS_ID", strConsignId);
        hmLnParam.put("REFID", strConsignId);
        hmLnParam.put("TRANS_TYPE", strTransType);
        hmLnParam.put("USER_ID", strUserId);
        hmLnParam.put("WAREHOUSE_TYPE", "6");
        hmLnParam.put("COMPANY_INFO",strCompanyInfo);
        gmInvLocationBean.updateLoanerStockSheetInfo(hmLnParam);

        alReturn = gmProj.loadSetMap("1601");
        hmReturn.put("SETLIST", alReturn);
        hmResult = gmOper.loadSavedSetMaster(strConsignId);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmReturn.put("MISSINGPARTS", hmResult.get("MISSINGPARTS"));
        hmReturn.put("SIMILARSETS", hmResult.get("SIMILARSETS"));
        hmResult = gmOper.viewBuiltSetDetails(strConsignId);
        hmReturn.put("CONDETAILS", hmResult);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "EditLoad");
      } else if (strAction.equals("EditLoadDash")) {
        // strConsignId =
        // (String)session.getAttribute("strSessConsignId")==null?"":(String)session.getAttribute("strSessConsignId");
        strConsignId = GmCommonClass.parseNull(request.getParameter("hId"));
        alReturn = gmProj.loadSetMap("1601");
        hmReturn.put("SETLIST", alReturn);
        hmResult = gmOper.loadSavedSetMaster(strConsignId);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmReturn.put("MISSINGPARTS", hmResult.get("MISSINGPARTS"));
        hmReturn.put("SIMILARSETS", hmResult.get("SIMILARSETS"));
        hmResult = gmOper.viewBuiltSetDetails(strConsignId);
        hmReturn.put("CONDETAILS", hmResult);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
      } else if (strAction.equals("PicSlip")) {
        strConsignId =
            request.getParameter("hConsignId") == null ? "" : (String) request
                .getParameter("hConsignId");
        hmResult = gmOper.loadSavedSetMaster(strConsignId);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmReturn.put("MISSINGPARTS", hmResult.get("MISSINGPARTS"));
        hmResult = gmOper.viewBuiltSetDetails(strConsignId);
        hmReturn.put("CONDETAILS", hmResult);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        strDispatchTo = strOperPath.concat("/GmSetPicSlip.jsp");
      } else if (strAction.equals("Reconfigure") || strAction.equals("Reload")) {
        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
        strSetId = GmCommonClass.parseNull(request.getParameter("hSetId"));
        strReloadId = GmCommonClass.parseNull(request.getParameter("Cbo_SimSet"));
        strReturn = gmOper.saveReconfigureSets(strConsignId, strSetId, strReloadId, strAction);

        hmResult = gmOper.loadSavedSetMaster(strConsignId);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmReturn.put("MISSINGPARTS", hmResult.get("MISSINGPARTS"));
        hmReturn.put("SIMILARSETS", hmResult.get("SIMILARSETS"));
        hmResult = gmOper.viewBuiltSetDetails(strConsignId);
        hmReturn.put("CONDETAILS", hmResult);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "EditLoad");
      } else if (strAction.equals("SetReconfigure")) {
        request.setAttribute("hAction", "LoadReconfig");
        strDispatchTo = strLogisPath.concat("/GmSetReconfig.jsp");
        log.debug(strDispatchTo);

      } else if (strAction.equals("ReloadReconfig")) {
        String requestID = "";
        // session.setAttribute("LoadConsignTime", GmCalenderOperations.getDateTime() );

        strConsignId = GmCommonClass.parseNull(request.getParameter("consignmentID"));
        String strRequestPurpose =
            GmCommonClass.parseNull(request.getParameter("strRequestPurpose"));

        // When select Reconfigure Consigment, if we have OUS Sales Replenishment purpose then only
        // mapped user allowed.
        hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                "OUS_REPL_CONS_EDT_AC"));
        String strUpdateAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        String strReadAccess = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
        String strVoidAccess = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));
        log.debug("strPurpose:" + strRequestPurpose + ":Access:" + strUpdateAccess + ":"
            + strReadAccess + ":" + strVoidAccess);
        if (strRequestPurpose.equals("4000097") && !strUpdateAccess.equals("Y")
            && !strReadAccess.equals("Y") && !strVoidAccess.equals("Y")) {
          throw new AppError("", "20592");
        }

        if (!strConsignId.equals("")) {
          requestID = gmRequestReportBean.fetchRequestId(strConsignId);
        } else {
          requestID = GmCommonClass.parseNull(request.getParameter("radPart"));
          if (requestID.equals("")) {
            requestID = GmCommonClass.parseNull(request.getParameter("requestId"));
          }
          strConsignId = gmRequestReportBean.fetchConsignmentIdFromMR(requestID);
        }
        log.debug("request id: " + requestID);

        gmRequestReportBean.validateRequest(requestID, "ConsignmentReconfig");
        log.debug("consignmentid: " + strConsignId);

        hmResult = gmOper.loadSavedSetMaster(strConsignId);

        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmReturn.put("MISSINGPARTS", hmResult.get("MISSINGPARTS"));
        hmReturn.put("SIMILARSETS", hmResult.get("SIMILARSETS"));

        hmResult = gmOper.viewBuiltSetDetails(strConsignId);
        hmReturn.put("CONDETAILS", hmResult);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "EditLoad");



        hmLists.put("RELTYPE", GmCommonClass.getCodeList("RCFTP")); // ReconfigureType
        hmLists.put("RELTRG", GmCommonClass.getCodeList("RCFTR")); // Reconfigure target - for built
                                                                   // sets - shelf/bulk
        request.setAttribute("hmLists", hmLists);
        log.debug("hmLists in line 293 is :" + hmLists);
        // backorder
        strRequestId = gmRequestReportBean.fetchRequestId(strConsignId);
        log.debug("BO:" + strRequestId);

        request.setAttribute("BACKORDERLIST", gmLogisticsBean.fetchBackOrderReport(strRequestId));
        request.setAttribute("REQID", strRequestId);
        log.debug("MR:" + strRequestId);
        log.debug("strOpt in line 301 is &&&  " + strOpt + "  &&&");
        // add section

        hmResult = gmLogisticsBean.getConsignPartsToAdd(strConsignId);
        log.debug(hmResult.get("ADDPARTS"));
        request.setAttribute("addParts", hmResult);
        request.setAttribute("CONFIGSOURCE", strOpt);


        request.setAttribute("RE_FORWARD", "gmSetReconfig");
        request.setAttribute("RE_TXN", "SETRECONFIGWRAPPER");
        request.setAttribute("txnStatus", "PROCESS");

        strDispatchTo = "/gmRuleEngine.do";
      } else if (strAction.equals("SubmitReconfig")) // including add parts submit also here-combine
                                                     // two submits
      {
        log.debug("combine submit");

        // TODOs: UI validation - quantity is number and both actions are chosen.
        strConsignId =
            request.getParameter("hConsignId") == null ? "" : (String) request
                .getParameter("hConsignId");
        strCount =
            request.getParameter("hCnt") == null ? "" : (String) request.getParameter("hCnt");

        log.debug("count " + strCount);
        log.debug("count " + strConsignId);

        /*
         * log.debug("DATE: " + gmLogisticsBean.getLastupdatedDate(strConsignId));
         * 
         * Timestamp ts = (Timestamp)
         * gmLogisticsBean.getLastupdatedDate(strConsignId).get("LASSTUPDATEDATE");
         * 
         * if( ts != null ) { if ( ((Date)session.getAttribute("LoadConsignTime")).compareTo(ts) < 0
         * ) throw new Exception(
         * "This consignment has been updated after the screen was loaded. Please refresh the screen."
         * ); }
         */

        intCount = Integer.parseInt(strCount);
        String strInputString = "";

        for (int i = 0; i <= intCount; i++) {
          strPartNum =
              request.getParameter("hPartNum" + i) == null ? "" : (String) request
                  .getParameter("hPartNum" + i);
          if (!strPartNum.equals("")) {
            strQty = request.getParameter("Txt_ReconfigQty" + i);
            strQty = strQty.equals("") ? "0" : strQty;

            strControl = request.getParameter("hControlNum" + i);
            strControl = strControl.equals("") ? " " : strControl;

            strReconfigAction = request.getParameter("Cbo_Action" + i);
            strReconfigAction = strReconfigAction.equals("") ? "0" : strReconfigAction;
            strReleaseTo = request.getParameter("Cbo_ReleaseTo" + i);
            strReleaseTo = strReleaseTo.equals("") ? "0" : strReleaseTo;

            if (!strReconfigAction.equals("0") && !strReleaseTo.equals("0")) // Validate both
                                                                             // actions selected.
            {
              strInputString =
                  strInputString + strPartNum + "," + strQty + "," + strControl + ","
                      + strReconfigAction + "," + strReleaseTo + "|";
              if (strReconfigAction.equals("90803"))
                blnIsBackOrderPresent = true;
            }
          }
        }

        log.debug(strInputString);

        // request.setAttribute("");


        // hmReturn = gmLogisticsBean.reconfigConsignToRequest( strConsignId, strInputString,
        // blnIsBackOrderPresent , strUserId );

        // ----
        // Add parts combined submit
        log.debug("Add parts");
        strCount =
            request.getParameter("hAddPartsCnt") == null ? "" : (String) request
                .getParameter("hAddPartsCnt");

        intCount = Integer.parseInt(strCount);
        String strAddPartsInputString = "";



        for (int i = 0; i <= intCount; i++) {
          strPartNum =
              request.getParameter("hAddPartNo" + i) == null ? "" : (String) request
                  .getParameter("hAddPartNo" + i);
          if (!strPartNum.equals("")) {

            strBulk = GmCommonClass.parseNull(request.getParameter("hBulk" + i));
            strBulk = strBulk.equals("") ? "0" : strBulk;


            strSetQty = GmCommonClass.parseNull(request.getParameter("hAddPartSetQty" + i)); // hSetQty
            strSetQty = strSetQty.equals("") ? "0" : strSetQty;

            log.debug("SET Q : " + "hAddPartSetQty" + i);
            log.debug("SET Q : " + strSetQty);
            strAddPartQty = request.getParameter("txt_addpartqty" + i);
            strAddPartQty = strAddPartQty.equals("") ? "0" : strAddPartQty;



            if (!strAddPartQty.equals("0")) // if qty selected
            {
              strAddPartsInputString =
                  strAddPartsInputString + strPartNum + "," + strBulk + "," + strSetQty + ","
                      + strAddPartQty + "|";
              if (Integer.parseInt(strBulk) < Integer.parseInt(strAddPartQty))
                blnIsBackOrderPresent = true;
            }
          }
        }

        log.debug(strAddPartsInputString);

        // backorder
        strRequestId = gmRequestReportBean.fetchRequestId(strConsignId);
        log.debug("BO:" + strRequestId);
        gmLogisticsBean.reconfigConsignToRequest(strConsignId, strRequestId, strInputString,
            strAddPartsInputString, blnIsBackOrderPresent, strUserId);
        // ---
        // Reload the screen with updates

        hmResult = gmOper.loadSavedSetMaster(strConsignId);

        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmReturn.put("MISSINGPARTS", hmResult.get("MISSINGPARTS"));
        hmReturn.put("SIMILARSETS", hmResult.get("SIMILARSETS"));

        hmResult = gmOper.viewBuiltSetDetails(strConsignId);
        hmReturn.put("CONDETAILS", hmResult);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "EditLoad");



        hmLists.put("RELTYPE", GmCommonClass.getCodeList("RCFTP")); // ReconfigureType
        hmLists.put("RELTRG", GmCommonClass.getCodeList("RCFTR")); // Reconfigure target - for built
                                                                   // sets - shelf/bulk
        request.setAttribute("hmLists", hmLists);



        request.setAttribute("BACKORDERLIST", gmLogisticsBean.fetchBackOrderReport(strRequestId));
        request.setAttribute("REQID", strRequestId);
        log.debug("MR:" + strRequestId);

        // add section

        hmResult = gmLogisticsBean.getConsignPartsToAdd(strConsignId);
        log.debug(hmResult.get("ADDPARTS"));
        request.setAttribute("addParts", hmResult);

        strDispatchTo = strLogisPath.concat("/GmSetReconfig.jsp");
      }
      request.setAttribute("hOpt", strOpt);
      if (strAction.equals("Initiate")) {
        log.debug("strStrutsPageURL inside initiate " + strStrutsPageURL);
        gotoStrutsPage(strStrutsPageURL, request, response);
      } else {
        dispatch(strDispatchTo, request, response);
      }

    }// End of try
    catch (AppError e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      request.setAttribute("hType", e.getType());
      strDispatchTo = strComnPath + "/GmError.jsp";
      dispatch(strDispatchTo, request, response);

    } catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmSetBuildServlet
