/*****************************************************************************
 * File			 : GmTranLogReportServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import com.globus.common.beans.*;
import com.globus.operations.beans.*;
import com.globus.common.servlets.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.naming.*;
import java.util.*;

import com.globus.common.beans.GmLogger; 
import org.apache.log4j.Logger;


public class GmTranLogReportServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
  		instantiate(request, response);
		HttpSession session = request.getSession(false);
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
		
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strOperPath = GmCommonClass.getString("GMOPERATIONS");
		String strDispatchTo = "";
		String strGridXmlData = "";
		HashMap hmParam = new HashMap();
        ArrayList alPTType = new ArrayList();
        
		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
			String strAction = request.getParameter("hAction");

			GmInvReportBean gmInv = new GmInvReportBean(getGmDataStoreVO());

			ArrayList alReturn = new ArrayList();
			String strUserId = 	(String)session.getAttribute("strSessUserId");
			
			String strFromDt = GmCommonClass.parseNull(request.getParameter("Txt_FromDate"));
			String strToDt = GmCommonClass.parseNull(request.getParameter("Txt_ToDate"));
			String strPartID = GmCommonClass.parseNull(request.getParameter("Txt_PartNum"));
			String strPartName = GmCommonClass.parseNull(request.getParameter("Txt_PartName"));
			String strSort = GmCommonClass.parseNull(request.getParameter("hSort"));
			String strSortType = GmCommonClass.parseNull(request.getParameter("hSortAscDesc"));
			String strTransId = GmCommonClass.parseNull(request.getParameter("Txt_TransId"));
            
            String strSeletedType = request.getParameter("cboType");
            String strPartNumFormat ="";
          	 if(!strPartID.equals("")){
          		strPartNumFormat = strPartID+",";
               	strPartNumFormat = strPartNumFormat.replaceAll("," ,"','");
               	strPartNumFormat= "'"+strPartNumFormat;
               	strPartNumFormat =  strPartNumFormat.substring(0,strPartNumFormat.length()-2);
          	 }
          	 
            
            strSeletedType = (strSeletedType == null)?"90800":strSeletedType;
            alPTType = GmCommonClass.getCodeList("PTTXY");
            
			hmParam.put("FROMDT",strFromDt);
			hmParam.put("TODT",strToDt);
			hmParam.put("PARTID",strPartNumFormat);
			hmParam.put("PARTNM",strPartName);
			hmParam.put("SORT",strSort);
			hmParam.put("SORTTYPE",strSortType);
			hmParam.put("TRANSID",strTransId);
            hmParam.put("TRANSTYPE",strSeletedType);
			
			if (strAction == null)
			{
				strAction = (String)session.getAttribute("hAction");
			}
			
			strAction = (strAction == null)?"LoadNCMRDetails":strAction;
			
			//log.debug("strAction " + strAction);	
			//strNCMRId = request.getParameter("hId")== null ?"":request.getParameter("hId");
			//log.debug(" Parameters for Reporting transaction details " + hmParam);
			alReturn = gmInv.reportTransactionDetails(hmParam);
			//log.debug(" Size for Transaction details are " + alReturn.size() +  " values are " + alReturn);
			request.setAttribute("alReturn",alReturn);
			request.setAttribute("hAction",strAction);
			hmParam.put("REPORT", alReturn);
			strGridXmlData = gmInv.getXmlGridData(hmParam);
		    request.setAttribute("XMLGRIDDATA", strGridXmlData);
			request.setAttribute("hFromDate",strFromDt );
			request.setAttribute("hToDate",strToDt );
			request.setAttribute("hPartNum",strPartID );
			request.setAttribute("hPartName", strPartName );
			request.setAttribute("hSortAscDesc", strSortType );
			request.setAttribute("hSort", strSort );
			request.setAttribute("hTransId",strTransId);
            request.setAttribute("halPTType",alPTType);
            request.setAttribute("hSelectedType" ,strSeletedType);
			
			strDispatchTo = strOperPath.concat("/GmTransactionLog.jsp");
			dispatch(strDispatchTo,request,response);

		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmTranLogReportServlet