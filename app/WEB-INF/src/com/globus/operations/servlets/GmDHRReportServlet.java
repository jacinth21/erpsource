/*****************************************************************************
 * File : GmDHRReportServlet Desc :
 * 
 * Version : 1.0 ---------------------------------- 12/23/2008 bvidyasankar Modified for adding
 * filter for Project Id (Issue 1976)
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.operations.beans.GmDHRReportBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.beans.GmVendorBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.event.beans.GmEventSetupBean;

public class GmDHRReportServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strDispatchTo = strOperPath.concat("/GmDHRList.jsp");
    String strGridXmlData = "";
    try {
      instantiate(request, response);
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      String strByVenNm = GmCommonClass.parseNull(request.getParameter("hFromPage"));
      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strOpt = request.getParameter("hOpt");

      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;
      log.debug("hAction is " + strAction + " hOpt is " + strOpt);
      GmCommonClass gmCommon = new GmCommonClass();
      GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
      GmDHRReportBean gmDHR = new GmDHRReportBean(getGmDataStoreVO());
      GmVendorBean gmVendor = new GmVendorBean(getGmDataStoreVO());
      GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      HashMap hmTemp = new HashMap();
      /****** Added as part of Issue 1976***start ***/
      HashMap hmDHRFilter = new HashMap();
      HashMap hmParam = new HashMap();
      GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
      /****** Added as part of Issue 1976***End ***/

      hmTemp.put("CHECKACTIVEFL", "N"); // check for active flag status in vendor list

      ArrayList alReturn = new ArrayList();
      ArrayList alProject = new ArrayList();
      ArrayList alDHRStatus = new ArrayList();
      RowSetDynaClass resultSet = null;
      String strWOId = "";
      String strVendorId = "";
      String strVendorNM = "";
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strFromDt = "";
      String strToDt = "";
      String strPartNums = "";
      String strId = "";
      String strProjID = "";
      String strDHRID = "";
      String strPartNum = "";
      String strControlNum = "";
      String strDHRStatus = "";
      String strAccessFl = "";
      String strProjNm = "";
      strVendorId = request.getParameter("hVendorId") == null ? "" : request.getParameter("hVendorId");
      if (strOpt.equals("Vendor") || strOpt.equals("Part") || strOpt.equals("Track")) {
        if (strAction.equals("Load") && strOpt.equals("Vendor")) {
          hmReturn = gmVendor.getVendorList(hmTemp);
          session.setAttribute("VENDORLIST", hmReturn.get("VENDORLIST"));
          strDispatchTo = strOperPath.concat("/GmDHRReportByVendor.jsp");
        } else if (strAction.equals("Load") && strOpt.equals("Part")) {
          strDispatchTo = strOperPath.concat("/GmDHRReport.jsp");
        } else if (strAction.equals("Load") && strOpt.equals("Track")) {
          strDispatchTo = strOperPath.concat("/GmDHRTrackingReport.jsp");
        } else if (strAction.equals("Go")) {
          if (strOpt.equals("Vendor")) {
            // To get the access flag to rollback DHR
            strAccessFl =
                gmEventSetupBean.getEventAccess(strUserId, "GROUP", "DHR_ROLLBACK_ACCESS"); // to
                                                                                            // get
                                                                                            // the
                                                                                            // accessibility
                                                                                            // of
                                                                                            // user
                                                                                            // to
                                                                                            // see
                                                                                            // the
                                                                                            // screen
            request.setAttribute("strRollBackAccess", strAccessFl);

            strVendorId =
                request.getParameter("hVendorId") == null ? "" : request.getParameter("hVendorId");
            strId = strVendorId;
            strVendorNM =
                    request.getParameter("hVendorId") == null ? "" : request.getParameter("searchhVendorId");
            strDHRID = GmCommonClass.parseNull(request.getParameter("Txt_DHRID"));
            strPartNum = GmCommonClass.parseNull(request.getParameter("Txt_PartNum"));
            strControlNum = GmCommonClass.parseNull(request.getParameter("Txt_ControlNum"));
            request.setAttribute("hDHRId", strDHRID);
            request.setAttribute("hPartNum", strPartNum);
            request.setAttribute("hControlNum", strControlNum);
            request.setAttribute("hVendId", strVendorId);
            request.setAttribute("hVendNM", strVendorNM);
            if(strByVenNm.equals("ByVendor")){
            	strDispatchTo = strOperPath.concat("/GmDHRReportByVendor.jsp");
            }
            
          } else if (strOpt.equals("Part")) {
            strPartNums = GmCommonClass.parseNull(request.getParameter("Txt_PartNum"));
            strProjID = GmCommonClass.parseNull(request.getParameter("Cbo_Project"));
            strProjNm = GmCommonClass.parseNull(request.getParameter("searchCbo_Project"));
            log.debug("1@....ProjectId..." + strProjID);
            int intCnt = 0;
            intCnt = strPartNums.indexOf(",");

            if (intCnt != -1) {
              strId = strPartNums;
              strId = "'".concat(strId).concat(",");
              strId = strId.replaceAll(",", "','");
              strId = strId.substring(0, strId.length() - 2);
            } else if (!strPartNums.equals("")) {
              strId = "'".concat(strPartNums) + "'";
            }
            strDispatchTo = strOperPath.concat("/GmDHRReport.jsp");

          } else if (strOpt.equals("Track")) {
            strDHRID = GmCommonClass.parseNull(request.getParameter("Txt_DHRID"));
            strPartNum = GmCommonClass.parseNull(request.getParameter("Txt_PartNum"));
            strDHRStatus = GmCommonClass.parseNull(request.getParameter("Cbo_Status"));
            strProjID = GmCommonClass.parseNull(request.getParameter("Cbo_Project"));
            strProjNm = GmCommonClass.parseNull(request.getParameter("searchCbo_Project"));
            request.setAttribute("hDHRId", strDHRID);
            request.setAttribute("hPartNum", strPartNum);
            request.setAttribute("hStatus", strDHRStatus);

            strDispatchTo = strOperPath.concat("/GmDHRTrackingReport.jsp");
          }

          strFromDt = GmCommonClass.parseNull(request.getParameter("Txt_FromDate"));
          strToDt = GmCommonClass.parseNull(request.getParameter("Txt_ToDate"));

          request.setAttribute("hAction", strAction);
          request.setAttribute("hVendId", strVendorId);
          request.setAttribute("hVendNM", strVendorNM);
          request.setAttribute("hFrmDt", strFromDt);
          request.setAttribute("hToDt", strToDt);
          request.setAttribute("hPartNums", strPartNums);
          request.setAttribute("hProjId", strProjID);
          request.setAttribute("hProjNm", strProjNm);

          hmDHRFilter.put("ID", strId);
          hmDHRFilter.put("DHRID", strDHRID);
          hmDHRFilter.put("FROMDT", strFromDt);
          hmDHRFilter.put("TODT", strToDt);
          hmDHRFilter.put("OPT", strOpt);
          hmDHRFilter.put("PROJID", strProjID);
          hmDHRFilter.put("PARTNUM", strPartNum);
          hmDHRFilter.put("CONTROLNUM", strControlNum);
          hmDHRFilter.put("DHRSTATUS", strDHRStatus);
          hmDHRFilter.put("PROJNM", strProjNm);
          hmDHRFilter.put("VENDNAME", strVendorNM);

          hmReturn = gmOper.getDHRList(hmDHRFilter);
          if(strAction.equals("Go")){        //PMT-27028 - It allow only DHR report for strOpt is part and vendor
        	  if (strOpt.equals("Part") || strOpt.equals("Vendor")) {
        		  	hmReturn.put("OPT", strOpt);
        		  	hmReturn.put("gmDataStoreVO", getGmDataStoreVO());
        		  	strGridXmlData = gmDHR.getXmlGridData(hmReturn);
        		  	request.setAttribute("XMLGRIDDATA", strGridXmlData);
        	  }
          }
          request.setAttribute("hmReturn", hmReturn);
          if (!strOpt.equals("Track")) {
            ArrayList alReport = (ArrayList) hmReturn.get("DHRLIST");
            request.setAttribute("DHRLIST", alReport);
          } else {// Need to get the result in RowSetDynaClass form to "GmDHRTrackReport.jsp"
            resultSet = (RowSetDynaClass) hmReturn.get("DHRLIST");
            request.setAttribute("DHRLIST", resultSet);
          }
        }
      }

      if (strOpt.equals("Part") || strOpt.equals("Track")) {
        hmParam.put("COLUMN", "ID");
        alProject = gmProject.reportProject(hmParam);
        alDHRStatus = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DHRSTS"));
        request.setAttribute("PROJLIST", alProject);
        request.setAttribute("DHRSTATUS", alDHRStatus);
      }
      request.setAttribute("hFromPage", request.getParameter("hFromPage"));
      request.setAttribute("hOpt", strOpt);
      request.setAttribute("hMode", "Report");
      request.setAttribute("hVendId", strVendorId);
      request.setAttribute("hVendNM", strVendorNM);
      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmDHRReportServlet
