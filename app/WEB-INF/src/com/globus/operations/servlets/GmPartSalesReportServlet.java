/*****************************************************************************
 * File			 : GmPartSalesReportServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmPartSalesReportServlet extends GmServlet
{
  @Override
	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strOperPath = GmCommonClass.getString("GMOPERATIONS");
		String strDispatchTo = strOperPath.concat("/GmPartNumSalesReport.jsp");

		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
				String strAction = request.getParameter("hAction");
				if (strAction == null)
				{
					strAction = (String)session.getAttribute("hAction");
				}
				strAction = (strAction == null)?"Load":strAction;

				GmCommonClass gmCommon = new GmCommonClass();
      GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
      GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());

				HashMap hmReturn = new HashMap();
                HashMap hmTemp = new HashMap();
                hmTemp.put("COLUMN","ID");
                hmTemp.put("STATUSID","20301"); // Filter all launched projects
				ArrayList alReturn = new ArrayList();

				String strProjId = "";
				String strReportMode = "";
				String strUserId = 	(String)session.getAttribute("strSessUserId");
				String strFromDt = "";
				String strToDt = "";

				if (strAction.equals("Load"))
				{
					alReturn = gmProj.reportProject(hmTemp);
					session.setAttribute("PROJLIST",alReturn);
				}
				else if (strAction.equals("Go"))
				{
					strProjId = request.getParameter("Cbo_ProjId")== null ?"":request.getParameter("Cbo_ProjId");
					strFromDt = request.getParameter("Txt_FromDate")== null ?"":request.getParameter("Txt_FromDate");
					strToDt = request.getParameter("Txt_ToDate")== null ?"":request.getParameter("Txt_ToDate");
					request.setAttribute("hAction",strAction);
					request.setAttribute("hProjId",strProjId);
					request.setAttribute("hFrmDt",strFromDt);
					request.setAttribute("hToDt",strToDt);

					alReturn = gmOper.getPartNumSales(strProjId,strFromDt,strToDt);
					hmReturn.put("REPORT",alReturn);
					request.setAttribute("hmReturn",hmReturn);
				}
				dispatch(strDispatchTo,request,response);

		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmPartSalesReportServlet