/*****************************************************************************
 * File : GmPOReceiveServlet Desc :
 * 
 * Version : 1.0 --------------------------------------- 12/19/2008 bvidyasankar Made changes as
 * part issue 1476
 ******************************************************************************/
package com.globus.operations.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmRuleBean;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.jms.consumers.beans.GmDHRPriorityUpdateBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.manufact.beans.GmManfBean;
import com.globus.operations.beans.GmDHRBean;
import com.globus.operations.beans.GmDonorInfoBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.beans.GmPurchaseBean;
import com.globus.operations.beans.GmVendorBean;
import com.globus.operations.inventory.beans.GmLocationBean;
import com.globus.operations.logistics.beans.GmBackOrderReportBean;
import com.globus.jms.consumers.beans.GmInitiateVendorJMS;

public class GmPOReceiveServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strDispatchTo = strOperPath.concat("/GmPOReceive.jsp");
    String strManfPath = GmCommonClass.getString("GMMANF");
    String strLoc = "DHRLC";
    try {
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.
      instantiate(request, response);
      GmManfBean gmManf = new GmManfBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmBackOrderReportBean gmBackOrderReportBean = new GmBackOrderReportBean(getGmDataStoreVO());

      RowSetDynaClass rdResult = null;

      log = GmLogger.getInstance(this.getClass().getName());

      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strType = request.getParameter("hType");
      strType = (strType == null) ? "" : strType;

      String strOpt = (String) session.getAttribute("strSessOpt");
      strOpt = strOpt == null || strOpt.equals("") ? "" : strOpt;

      log.debug("Action is " + strAction + " Opt Value is " + strOpt);

      GmCommonClass gmCommon = new GmCommonClass();
      GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
      GmVendorBean gmVendor = new GmVendorBean(getGmDataStoreVO());
      GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
      GmPurchaseBean gmPurc = new GmPurchaseBean(getGmDataStoreVO());
      GmLocationBean gmLocationBean = new GmLocationBean(getGmDataStoreVO());
      GmRuleBean gmRuleBean = new GmRuleBean(getGmDataStoreVO());
      GmDonorInfoBean gmDonorInfoBean = new GmDonorInfoBean(getGmDataStoreVO());
      GmDHRPriorityUpdateBean gmDHRUpdateDHRPriority=new GmDHRPriorityUpdateBean(); 
      GmInitiateVendorJMS gmInitiateVendorJMS = new GmInitiateVendorJMS(getGmDataStoreVO());
      
      HashMap hmReturn = new HashMap();
      HashMap hmDHRDetails = new HashMap();
      HashMap hmDHRFilter = new HashMap();
      HashMap hmParam = new HashMap();
      HashMap hmTemp = new HashMap();
      HashMap hmResult = new HashMap();
      HashMap hmDonorDtls = new HashMap();
      HashMap hmPoDetailsMap = new HashMap();
      HashMap hmEmailParam = new HashMap();
      
      ArrayList alReturn = new ArrayList();
      ArrayList alLcnPartMap = new ArrayList();
      ArrayList alDonorDtls = new ArrayList();

      String strPO = "";
      String strDHRId = "";
      String strVendorId = "";
      String strInputStr = "";
      String strPackSlip = "";
      String strComments = "";
      String strRecDate = "";
      String strMode = "";
      String strPortal = "";
      String strFromPage = "";
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strCheckDHRVoid = "";
      String strPoType = "";
      String strPartNum = "";
      String strTransId = "";
      String strJulianRule = "";
      String strCntNum = "";
      String strPartProdClass = "";
      String strVendorRule = ""; 
      String strVendorRuleId = "";
      String strLoadPo = "";

      String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
      GmResourceBundleBean gmResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);

      if (strAction.equals("Load")) {
        if (strOpt.equals("DHR")) {
          request.setAttribute("hAction", "Load");
          strDispatchTo = strOperPath.concat("/GmDHRUpdate.jsp");
        } else {
          request.setAttribute("hAction", "Load");
        }
      } else if (strAction.equals("LoadPO")) {
        strPO = request.getParameter("Txt_PO") == null ? "" : request.getParameter("Txt_PO");
        strJulianRule = GmCommonClass.parseNull(GmCommonClass.getRuleValue("JULIAN_DATE", "LOTVALIDATION"));
        strLoadPo = GmCommonClass.parseNull(gmPurc.validateDR(strPO));      // DR ID pass for get PO ID in PMT-35876 
        	  if (!strLoadPo.equals("")){
                  strPO = strLoadPo;
        	  }
        // validatePOPlant := To validate the PO based on the plant
        gmPurc.validatePOPlant(strPO);
        hmReturn = gmPurc.viewPO(strPO, "", "");
        hmPoDetailsMap = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("PODETAILS"));  //added for PMT-31248
        strVendorRuleId = GmCommonClass.parseNull((String) hmPoDetailsMap.get("VID"));    //added for PMT-31248
        strVendorRule =
                GmCommonClass.parseNull(GmCommonClass.getRuleValue(strVendorRuleId, "SKIPLOTVAL"));   //added for PMT-31248

        // Getting PO Type. This would be used to bypass control # verification for Mfg Supply Po's
        // - 3103
        strPoType = (String) hmReturn.get("POTYPE");
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hPO", strPO);
        request.setAttribute("hType", strType);
        request.setAttribute("hPoType", strPoType);
        request.setAttribute("hAction", strAction);
        request.setAttribute("RE_FORWARD", "gmPOReceive");
        request.setAttribute("RE_TXN", "50900");
        request.setAttribute("txnStatus", "PROCESS");
        request.setAttribute("JULIANRULE", strJulianRule);
        request.setAttribute("VENDORRULE", strVendorRule);    //added for PMT-31248
        strDispatchTo = "/gmRuleEngine.do";
        } else if (strAction.equals("Initiate") && strOpt.equals("INFORMALRECV")) {
        strVendorId =
            request.getParameter("hVendorId") == null ? "" : request.getParameter("hVendorId");
        strInputStr =
            request.getParameter("hInputString") == null ? "" : request
                .getParameter("hInputString");
        strPackSlip =
            request.getParameter("Txt_Pack") == null ? "" : request.getParameter("Txt_Pack");
        strComments =
            request.getParameter("Txt_Comments") == null ? "" : request
                .getParameter("Txt_Comments");
        strRecDate =
            request.getParameter("Txt_RecDate") == null ? "" : request.getParameter("Txt_RecDate");
        strUserId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));

        hmReturn =
            gmPurc.updateVerifyDHR(strVendorId, strInputStr, strPackSlip, strComments, strRecDate,
                strType, strUserId);

        String strIds = (String) hmReturn.get("PDTID");
        log.debug(" Inside Informal recv -- " + strIds);
        strIds = (String) hmReturn.get("PDTID");
        strIds = strIds.replaceAll(",", "','");
        strIds = "'".concat(strIds);
        strIds = strIds.substring(0, strIds.length() - 2);

        hmDHRFilter.put("ID", strVendorId);
        hmDHRFilter.put("DHRID", strIds);
        hmDHRFilter.put("OPT", "DHR");

        hmReturn = gmOper.getDHRList(hmDHRFilter);
        log.debug(" hmReturn values is " + hmReturn);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hVendorId", strVendorId);
        request.setAttribute("hAction", "Load");
        strDispatchTo = strOperPath.concat("/GmDHRList.jsp");
      } else if (strAction.equals("printFAR")) {
        String strWOId = request.getParameter("hWOId") == null ? "" : request.getParameter("hWOId");
        HashMap hmReturnWO = gmPurc.viewWO(strWOId, strUserId);
        request.setAttribute("hmReturn", hmReturnWO);
        strDispatchTo = strOperPath.concat("/GmFARPrint.jsp");
      } else if (strAction.equals("Initiate")) {
        strVendorId =
            request.getParameter("hVendorId") == null ? "" : request.getParameter("hVendorId");
        strInputStr =
            request.getParameter("hInputString") == null ? "" : request
                .getParameter("hInputString");
        strPackSlip =
            request.getParameter("Txt_Pack") == null ? "" : request.getParameter("Txt_Pack");
        strComments =
            request.getParameter("Txt_Comments") == null ? "" : request
                .getParameter("Txt_Comments");
        strRecDate =
            request.getParameter("Txt_RecDate") == null ? "" : request.getParameter("Txt_RecDate");
        strPoType = GmCommonClass.parseNull(request.getParameter("hPoType"));

        hmReturn =
        		GmCommonClass.parseNullHashMap(gmOper.initiateDHR(strVendorId, strInputStr, strPackSlip, strComments, strRecDate,
                    strType, strUserId));
        String strIds = (String) hmReturn.get("PDTID");
        String strDHRs = (String) hmReturn.get("PDTID");
        //PMT-35878 Received Shipment Email Notification
     	hmEmailParam.put("REFID", strDHRs);
      	hmEmailParam.put("REFTYPE", "108442");//DHR
      	hmEmailParam.put("REFACTION", "108407");//Shipment Accepted
    	hmEmailParam.put("MAILSTATUS", "0");//
      	hmEmailParam.put("USERID", strUserId);
      	hmEmailParam.put("COMPANYINFO", GmCommonClass.parseNull(request.getParameter("companyInfo")));
      	gmInitiateVendorJMS.initVendorEmailJMS(hmEmailParam);//JMS Call
        gmDHRUpdateDHRPriority.UpdateDHRPriority(strDHRs,strUserId);
        strIds = strIds.replaceAll(",", "','");
        strIds = "'".concat(strIds);
        strIds = strIds.substring(0, strIds.length() - 2);

        hmDHRFilter.put("ID", strVendorId);
        hmDHRFilter.put("DHRID", strIds);
        hmDHRFilter.put("OPT", "DHR");

        hmReturn = gmOper.getDHRList(hmDHRFilter);

        log.debug(" Values inside DHRList " + hmReturn);

        if (strPoType.equals("3103")) {
          log.debug(" INside 3103 ");
          gmManf.informPoReceive(hmReturn);
        }

        // After initiate DHR, Update/Insert Lot Master table t2550
        gmOper.updateLotMaster(strDHRs, strUserId);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hVendorId", strVendorId);
        request.setAttribute("hAction", "Load");
        strDispatchTo = strOperPath.concat("/GmDHRList.jsp");
      } else if (strAction.equals("ViewDHR") || strAction.equals("ViewDHRPrint")
          || strAction.equals("ViewManfDHRPrint") || strAction.equals("ViewDHRPrintNew")) {
        strVendorId =
            request.getParameter("hVendorId") == null ? "" : (String) request
                .getParameter("hVendorId");
        strDHRId =
            request.getParameter("hDHRId") == null ? "" : (String) request.getParameter("hDHRId");
        String strWOId = request.getParameter("hWOId") == null ? "" : request.getParameter("hWOId");
        hmReturn = gmOper.getDHRDetails(strDHRId);
        hmTemp = (HashMap) hmReturn.get("DHRDETAILS");
        log.debug("hmTemp = " + hmTemp);
        strPartNum = (String) hmTemp.get("PNUM");
        hmParam.put("INPUTSTRING", strPartNum);
        log.debug("hmParam" + strPartNum);
        HashMap hmCompanyAddress =
            GmCommonClass.fetchCompanyAddress(getGmDataStoreVO().getCmpid(), "");
        hmReturn.put("LOGO", GmCommonClass.parseNull((String) hmCompanyAddress.get("COMP_LOGO")));
        hmReturn.put("gmResourceBundleBean", gmResourceBundleBean);
        HashMap hmReturnWODetails = gmPurc.viewWO(strWOId, strUserId);
        // The below changes is added for UDI-16,Based on PO created Date GmWorkOrderPrint.jasper
        // will be called or GmWoPrintAllD.jsp
        hmReturnWODetails.put("SUBREPORT_DIR",
            session.getServletContext().getRealPath(GmCommonClass.getString("GMJASPERLOCATION"))
                + "\\");
        RowSetDynaClass rsBackOrderFlag = gmBackOrderReportBean.loadBackOrderByPartReport(hmParam);

        request.setAttribute("hmReturnWODetails", hmReturnWODetails);
        request.setAttribute("rsBackOrderFlag", rsBackOrderFlag);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hVendorId", strVendorId);
        request.setAttribute("hMode", "Load");
        if (strAction.equals("ViewDHRPrint")) {
          // START Mihir Added for Condition- Low Shelf Zero Shelf; Alert :
          // "Low Shelf Stock"/"Zero Shelf"
          HashMap hmParamForcast = new HashMap();
          hmParamForcast.put("INPUTTRENDCAL", "TREND,40020|TREND,40021|TREND,40022|");// sales,
                                                                                      // consignment
                                                                                      // ,In-house
                                                                                      // Consignment
                                                                                      // resp.
          hmParamForcast.put("PARTNUM", strPartNum); // Part Number.
          hmParamForcast.put("SALESGRPTYPE", "50271"); // Sales Group Type is for "All Parts"
          hmParamForcast.put("CHECKTYPE", "50242"); // CHECKTYPE is Item, hard Coded.
          hmParamForcast.put("TRENDTYPE", "50277"); // Trend Type is "Forcast"
          hmParamForcast.put("TRENDPERIOD", "1"); // i.e 1-15 days
          hmParamForcast.put("FORECASTPERIOD", "3"); // For 3 months
          hmParamForcast.put("TRENDDURATION", "50282"); // 15 days

          HashMap hmResultTrendReport = gmBackOrderReportBean.loadTrendReport(hmParamForcast);
          request.setAttribute("hmResultTrendReport", hmResultTrendReport);
          // END Mihir Added for Condition- Low Shelf And Zero Shelf; Alert :
          // "Low Shelf Stock"/"Zero Shelf"

          // Fetch the "Donor Parameters" to the screen
          hmResult = gmDonorInfoBean.fetchDonorInfo(strDHRId);
          alDonorDtls = GmCommonClass.parseNullArrayList((ArrayList) hmResult.get("DONORDTLS"));

          if (alDonorDtls.size() > 0)
            hmDonorDtls = (HashMap) alDonorDtls.get(0);
          request.setAttribute("hmDonorDtls", hmDonorDtls);
          // strMode = GmCommonClass.parseNull(request.getParameter("hMode"));
          String strRptName =
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("WO.JASPERNM"));
          request.setAttribute("STRRPTNAME", strRptName);
          strDispatchTo = strOperPath.concat("/GmDHRPrint.jsp");
          strMode = GmCommonClass.parseNull((String) hmTemp.get("SMODE"));
          log.debug("in PO receive..." + strMode);
          if (!strMode.equals("")) {
            strTransId = getTransactionId(strMode);
            request.setAttribute("PRINTDHR", "printDHR");
            request.setAttribute("transMode", strMode);
            request.setAttribute("RE_FORWARD", "gmDHRPrint");
            request.setAttribute("RE_TXN", strTransId);
            request.setAttribute("txnStatus", "PROCESS");
            strDispatchTo = "/gmRuleEngine.do";
          }
        } else if (strAction.equals("ViewDHRPrintNew")) {
          // should create a new jsp
          strDispatchTo = strOperPath.concat("/GmDHRPrintTemp.jsp");
        } else if (strAction.equals("ViewManfDHRPrint")) {
          strDispatchTo = strManfPath.concat("/GmMFGDhrPrint.jsp");
        } else {
          strDispatchTo = strOperPath.concat("/GmDHRView.jsp");
        }
      } else if (strAction.equals("UpdateDHR")) {
        // strDHRId =
        // (String)session.getAttribute("strSessDHRId")==null?"":(String)session.getAttribute("strSessDHRId");
        strDHRId = GmCommonClass.parseNull(request.getParameter("hId"));
        // strMode =
        // (String)session.getAttribute("strSessMode")==null?"":(String)session.getAttribute("strSessMode");
        strMode = GmCommonClass.parseNull(request.getParameter("hMode"));
        // Getting the Origin page information from GmOperFrameServlet
        // strFromPage =
        // (String)session.getAttribute("FromPage")==null?"":(String)session.getAttribute("FromPage");
        strFromPage = GmCommonClass.parseNull(request.getParameter("hFrom"));
        // strPortal =
        // (String)session.getAttribute("FromPortal")==null?"":(String)session.getAttribute("FromPortal");
        strPortal = GmCommonClass.parseNull(request.getParameter("hPortal"));

        hmReturn = gmOper.getDHRDetails(strDHRId);
        log.debug(" Details of the DHR is " + hmReturn);
        hmDHRDetails = (HashMap) hmReturn.get("DHRDETAILS");
        strCheckDHRVoid = GmCommonClass.parseNull((String) hmDHRDetails.get("DHRVFL"));
        log.debug(" strCheckDHRVoid is  " + strCheckDHRVoid);

        // code for location part mapping detail
        String strPnum = GmCommonClass.parseNull((String) hmDHRDetails.get("PNUM"));
        // code for PMT-2720
        strCntNum = GmCommonClass.parseNull((String) hmDHRDetails.get("CNUM")).toUpperCase();
        strPoType = GmCommonClass.parseNull((String) hmDHRDetails.get("WOTYPE"));
        strPartProdClass = GmCommonClass.parseNull((String) hmDHRDetails.get("PCLASS"));

        hmParam.put("TXNTYPE", "DHR");
        hmParam.put("PNUMSTR", strPnum);
        // alLcnPartMap = gmLocationBean.fetchPartLocationDetails(hmParam);

        hmParam.put("RLIST", alLcnPartMap);
        hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
        hmParam.put("VMFILEPATH", "properties.labels.operations.inventory.GmDHRPartLocationMap");
        hmParam.put("TEMPLATE", "GmDHRPartLocationMap.vm");
        hmParam.put("TEMPLATEPATH", "operations/inventory/templates");
        String strGridXmlData = gmLocationBean.getXmlGridData(hmParam);
        // log.debug("strGridXmlData======"+strGridXmlData);
        hmReturn.put("GRIDXMLDATA", strGridXmlData);

        // end...

        if (strMode.equals("")) {
          strMode = GmCommonClass.parseNull((String) hmDHRDetails.get("SMODE"));
        }
        // verifying if the DHR Flag is NULL. If the Flag is not NULL, then redirect to DHR View JSP
        // with the strAction as updateVoid
        if (strCheckDHRVoid.equalsIgnoreCase("Y")) {
          alReturn = gmCommonBean.getLog(strDHRId);
          request.setAttribute("hmLog", alReturn);
          request.setAttribute("hMode", "UpdateVoid");
          request.setAttribute("hmReturn", hmReturn);
          strDispatchTo = strOperPath.concat("/GmDHRView.jsp");
        } else {
          alReturn = gmLogon.getEmployeeList();
          hmReturn.put("EMPLIST", alReturn);
          ArrayList alReturnReasons = gmCommon.getCodeList("NCRTP");
          request.setAttribute("hmReturn", hmReturn);
          request.setAttribute("hMode", strMode);
          // hAction is changed from Load to UpdateDHR as part of Issue 1476
          request.setAttribute("hAction", "UpdateDHR");
          // Setting the Origin page information which helps display Error Message in
          // GmDHRUpdate.jsp
          request.setAttribute("FromPage", strFromPage);
          request.setAttribute("hNCMRReason", alReturnReasons);
          request.setAttribute("hPortal", strPortal);
          if (strPortal.equals("Manf")) {
            strDispatchTo = strManfPath.concat("/GmManfDHRUpdate.jsp");
            strLoc = "MWLC";
          } else if (strPortal.equals("Supply")) {
            strDispatchTo = strManfPath.concat("/GmManfSupplyUpdate.jsp");
            strLoc = "SHLC";
          } else {
            // -- Po type: Rework,Part:Non-Sterile,Control Number: NOC# then skip to call the
            // /gmRuleEngine.do.
            if (strPoType.equals("3101") && strPartProdClass.equals("4031")
                && strCntNum.equals("NOC#")) {
              strDispatchTo = strOperPath.concat("/GmDHRUpdate.jsp");
            } else {
              strTransId = getTransactionId(strMode);
              if (!strTransId.trim().equals("")) {
                request.setAttribute("transMode", strMode);
                request.setAttribute("RE_FORWARD", "gmDHRUpdate");
                request.setAttribute("RE_TXN", strTransId);
                request.setAttribute("txnStatus", "PROCESS");
                strDispatchTo = "/gmRuleEngine.do";
              } else {
                throw new AppError("The following DHR Transaction <b> " + strDHRId
                    + "</b> already verified/voided", "", 'E');
              }
            }
          }
        }
      }
      if (strPortal.equals("Manf") || strDHRId.indexOf("MWO") > 0) {
        strLoc = "MWLC";
      } else if (strPortal.equals("Supply") || strDHRId.indexOf("SHR") > 0) {
        strLoc = "SHLC";
      } else {
        strLoc = "DHRLC";
      }
      hmParam.put("DHRID", strDHRId);
      hmParam.put("LOCGRP", strLoc);

      log.debug("strLoc==>" + strLoc + "  strDHRId==>" + strDHRId);

      GmDHRBean gmDHRBean = new GmDHRBean(getGmDataStoreVO());
      ArrayList alLocations = gmDHRBean.fetchSplitLocations(hmParam);
      HashMap hmParams = new HashMap();
      hmParams.put("strAction", strMode);

      // String strNCMRId=
      // (GmCommonClass.parseNull((String)hmDHRDetails.get("NCMRID")).equals(""))?"-":GmCommonClass.parseNull((String)hmDHRDetails.get("NCMRID"));
      String strRejQty = GmCommonClass.parseZero((String) hmDHRDetails.get("QTYREJ"));
      String strNCMRFl = GmCommonClass.parseNull((String) hmDHRDetails.get("NCMRFL"));

      log.debug("strNCMRFl==>" + strNCMRFl);
      log.debug("strRejQty==>" + strRejQty);

      // hmParams.put("strNCMRId",strNCMRId);
      hmParams.put("strRejQty", strRejQty);
      hmParams.put("strNCMRFl", strNCMRFl);
      hmParams.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
      String strXmlData = getXmlGridData(alLocations, hmParams);
      request.setAttribute("XMLDATA", strXmlData);

      String strInputString = "PART," + hmDHRDetails.get("PNUM") + "|";
      rdResult = gmBackOrderReportBean.loadBODHRReoprt(strInputString, strDHRId);
      request.setAttribute("ldhrPartResult", rdResult.getRows());
      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method

  public String getTransactionId(String strMode) {
    String strTransId = "";
    if (strMode.equals("I"))
      strTransId = "50901";
    else if (strMode.equals("P"))
      strTransId = "50902";
    else if (strMode.equals("V"))
      strTransId = "50903";
    return strTransId;
  }

  public String getXmlGridData(ArrayList alParam, HashMap hmParams) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParams.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataList("alReturn", alParam);
    templateUtil.setDataMap("hmParams", hmParams);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.GmLocationDisplay", strSessCompanyLocale));
    templateUtil.setTemplateName("GmLocationDisplay.vm");
    templateUtil.setTemplateSubDir("operations/templates");
    return templateUtil.generateOutput();
  }

}// End of GmPOReceiveServlet
