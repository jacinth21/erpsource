package com.globus.webservice.logon.resources;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.accounts.tax.beans.GmSalesTaxRptBean;
import com.globus.accounts.tax.beans.GmSalesTaxTransBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmCustSalesSetupBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.valueobject.accounts.GmValidShipAddressVo;
import com.globus.valueobject.accounts.GmValidateAddressVO;
import com.globus.webservice.common.resources.GmResource;

@Path("validateShipAddress")
public class GmAccountShipAddValidateResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  // for post request
  @POST
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmValidateAddressVO tempShipAddressValidate(String strInput) throws Exception {

    log.debug("strInput: " + strInput);
    GmValidateAddressVO gmValidateAddressVO = new GmValidateAddressVO();


    ArrayList alAccountList = new ArrayList();
    GmCustSalesSetupBean gmCustSalesSetupBean = new GmCustSalesSetupBean();
    GmSalesTaxTransBean gmSalesTaxTransBean = new GmSalesTaxTransBean();
    GmSalesTaxRptBean gmSalesTaxRptBean = new GmSalesTaxRptBean();
    GmSalesBean gmSalesBean = new GmSalesBean();
    String strAccountId = "";
    StringBuffer sbString = new StringBuffer();
    HashMap hmTemp;
    alAccountList = gmSalesTaxRptBean.fetchUsAcccountList();
    log.debug(" Account List " + alAccountList.size());
    HashMap hmAccData = new HashMap();
    HashMap hmParam = new HashMap();
    GmValidateAddressVO gmValidateAddress = new GmValidateAddressVO();
    //
    String shipAdd = "";
    String shipAdd2 = "";
    String shipCity = "";
    String shipState = "";
    String shipCountry = "";
    String shipzipCode = "";
    if(sbString.toString().equals("")){
      for (int i = 0; i < alAccountList.size(); i++) {
        log.debug(" Account count :: "+i);
        hmTemp = new HashMap();
        hmTemp = (HashMap) alAccountList.get(i);
        strAccountId = (String) hmTemp.get("ID");
        /*hmTemp.put("ACCID", strAccountId);

        hmAccData = gmCustSalesSetupBean.fetchAcctShipDtls(hmTemp);*/
        

        shipAdd = GmCommonClass.parseNull((String) hmTemp.get("SHIPADD1"));
        shipAdd2 = "";
        shipCity = GmCommonClass.parseNull((String) hmTemp.get("SHIPCITY"));
        shipState = GmCommonClass.parseNull((String) hmTemp.get("SHIPSTATE"));
        shipCountry = GmCommonClass.parseNull((String) hmTemp.get("SHIPCOUNTRY"));
        shipzipCode = GmCommonClass.parseNull((String) hmTemp.get("SHIPZIP"));

        //if (shipCountry.equals("1101")) { // US only
          //shipState = GmCommonClass.getCodeAltName(shipState);
          //shipCountry = GmCommonClass.getCodeAltName(shipCountry);

          hmParam = new HashMap ();
          hmParam.put("METHOD", "VALIDATE_ADD");

          if (!shipzipCode.equals("") && !shipzipCode.equals("-")) {
            shipzipCode = "&PostalCode=" + URLEncoder.encode(shipzipCode);
          } else {
            shipzipCode = "";
          }
          hmParam.put(
              "PARAM",
              "Line1=" + URLEncoder.encode(shipAdd) + "&Line2=" + URLEncoder.encode(shipAdd2)
                  + "&Line3=" + URLEncoder.encode("") + "&City=" + URLEncoder.encode(shipCity)
                  + "&Region=" + URLEncoder.encode(shipState) + "&Country="
                  + URLEncoder.encode(shipCountry) + shipzipCode);

          gmValidateAddress = gmSalesTaxTransBean.validateAvaTaxService(hmParam);
          GmValidShipAddressVo gmValidShipAddressVo = gmValidateAddress.getAddress();
          String strResultCode = gmValidateAddress.getResultCode();

          if (strResultCode.equalsIgnoreCase("Success")) {
            shipAdd = gmValidShipAddressVo.getLine1();
            shipAdd2 = gmValidShipAddressVo.getLine2();
            shipCity = gmValidShipAddressVo.getCity();
            shipState =
                gmSalesTaxRptBean.fetchCodeIdFromCodeGrpAndCodeAlt("STATE",
                    gmValidShipAddressVo.getRegion());
            shipCountry =
                gmSalesTaxRptBean.fetchCodeIdFromCodeGrpAndCodeAlt("CNTY",
                    gmValidShipAddressVo.getCountry());
            shipzipCode = gmValidShipAddressVo.getPostalCode();

            sbString.append(strAccountId + "^" + shipAdd + "^" + shipCity + "^" + shipState + "^" + shipCountry + "^" + shipzipCode + "^Y" + "|");

          } else {
            sbString.append(strAccountId + "^" + "" + "^" + "" + "^" + "" + "^" + "" + "^" + "" + "^N" + "|");
          }
        }
    }
      gmSalesTaxTransBean.updateTempShipAddress(sbString.toString(), "30301");
      log.debug("Shipping address validated successfully....");
    //}

    return gmValidateAddressVO;
  }


}
