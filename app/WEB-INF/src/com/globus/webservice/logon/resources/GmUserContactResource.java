
/**********************************************************************************
 * File		 		: GmUserContactResource.java
 * Desc		 		: This file is used to get users personal info
 * Version	 		: 1.0
 * author			: Arunkumar.A
************************************************************************************/



package com.globus.webservice.logon.resources;

import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.log4j.Logger;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.logon.beans.GmUserContactBean;
import com.globus.valueobject.logon.GmUserContactVO;
import com.globus.webservice.common.resources.GmResource;

@Path("usercontactinfo")
public class GmUserContactResource extends GmResource  {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmUserContactVO  fetchUserContact(String  strInput) throws AppError {

		GmUserContactVO gmUserContactVO= new GmUserContactVO();
	    GmWSUtil gmWSUtil = new GmWSUtil();
	    HashMap hmUserContact = new HashMap();
	    GmUserContactBean gmUserContactBean=new GmUserContactBean();
		gmWSUtil.parseJsonToVO(strInput, gmUserContactVO);
		validateToken(gmUserContactVO.getToken());
		String strUserid = gmUserContactVO.getUserid();
		if(strUserid.equals(""))
	  	 strUserid = gmUserVO.getUserid();
	    hmUserContact = gmUserContactBean.getUserContact(strUserid);
	    log.debug(hmUserContact);
		gmWSUtil.getVOFromHashMap(hmUserContact, gmUserContactVO);
		
		return gmUserContactVO;
	}
}