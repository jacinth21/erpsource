package com.globus.webservice.logon.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.it.beans.GmLdapBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.logon.beans.GmUserTokenBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.common.GmDeviceInfoVO;
import com.globus.valueobject.logon.GmLogOnVO;
import com.globus.valueobject.logon.GmLogonAccessVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.webservice.common.resources.GmResource;

@Path("auth")
public class GmLogonResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());
  GmLogonBean gmLogonBean = new GmLogonBean();

  // for post request
  @POST
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmLogOnVO authenticate(String strInput) throws AppError {

    String strToken = "";
    Boolean btemp = false;

    log.debug("strInput: " + strInput);

    GmLogOnVO gmLogOnVO = new GmLogOnVO();
    GmWSUtil gmWSUtil = new GmWSUtil();

    GmLdapBean gmLdapBean = new GmLdapBean();
    HashMap hmCredential = new HashMap();
    HashMap hmUserInfo = new HashMap();

    gmWSUtil.parseJsonToVO(strInput, gmLogOnVO);
    hmCredential = gmWSUtil.getHashMapFromVO(gmLogOnVO);
    String strUserName = GmCommonClass.parseNull((String) hmCredential.get("USERNM"));
    // to get the LdapID based on User
    String strLdapId = gmLdapBean.getLdapIdFromUserName(strUserName);
    hmCredential.put("LDAPID", strLdapId);
    gmLogonBean.authenticate(hmCredential);
    hmUserInfo = gmLogonBean.fetchUserDetailsAfterAuthentication(hmCredential);
    log.debug("hmUserInfo: " + hmUserInfo);
    HashMap hmCurrDetails =
        GmCommonClass.getCurrSymbolFormatfrmComp(GmCommonClass.parseNull((String) hmUserInfo
            .get("COMPID")));
    log.debug("hmCurrDetails: " + hmCurrDetails);

    String strCompanyLocale =
        GmCommonClass.getCompanyLocale(GmCommonClass.parseNull((String) hmUserInfo.get("COMPID")));

    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);

    String strcompname = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYNAME"));
    String strcmpadress =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYADDRESS"));
    String strcompphn = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYPHONE"));
    String strcompfax = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYFAX"));
    String strremitadress =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMREMITTOFORDO"));
    /*
     * To view the PDF in online -Get the PDF saving path constant properties key from rule entry in
     * login time
     */
    String pdffilepath = GmCommonClass.parseNull(GmCommonClass.getRuleValue("PDFPATH", "DOPDF"));

    // appending company address info for DO App
    hmCurrDetails.put("STRCOMPNAME", strcompname);
    hmCurrDetails.put("STRCMPADRESS", strcmpadress);
    hmCurrDetails.put("STRCOMPPHN", strcompphn);
    hmCurrDetails.put("STRCOMPFAX", strcompfax);
    hmCurrDetails.put("STRREMITADRESS", strremitadress);
    hmCurrDetails.put("PDFFILEPATH", pdffilepath);
    hmCurrDetails.put("EGNYTETKN", System.getProperty("ENV_EGNYTEAUTHTOKEN"));
    hmCurrDetails.put("EGNYTEFL", GmCommonClass.getString("EGNYTESERVERENABLE"));
    hmCurrDetails.put("RIMBGUIDEID", System.getProperty("ENV_RIMBGUIDEFILEID"));
    hmCurrDetails.put("AUTOUPDTIMER", GmCommonClass.getString("APPAUTOUPDTIMER"));
    gmWSUtil.getVOFromHashMap(hmCurrDetails, gmLogOnVO, gmLogOnVO.getMappingProperties());
    gmWSUtil.getVOFromHashMap(hmUserInfo, gmLogOnVO, gmLogOnVO.getMappingProperties());
    strToken = GmTokenManager.getNewToken();
    gmLogOnVO.setToken(strToken);
    log.debug("hmUserInfo.get(COMPID"+hmUserInfo.get("COMPID"));
    gmLogOnVO.setCompid(hmUserInfo.get("COMPID").toString());
    GmTokenManager.addToken(strToken, gmLogOnVO);

    HashMap hmParam = new HashMap();
    hmParam.put("TOKEN", gmLogOnVO.getToken());
    hmParam.put("STATUS", "103101"); // Active Token Status
    hmParam.put("USERID", gmLogOnVO.getUserid());
  
  // This method will save the User Token
    (new GmUserTokenBean()).saveUserToken(hmParam);

    gmLogOnVO.setPasswd("");
    gmLogOnVO.setPin("");

    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    gmLogOnVO.setListGmSecurityEventVO(gmAccessControlBean.getSecurityEvents(
        gmLogOnVO.getPartyid(), "SYNC_DATA,SETTINGS"));

    return gmLogOnVO;
  }

  @Path("deviceinfo")
  @POST
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmDeviceInfoVO saveDeviceInfo(String strInput) throws AppError {

    GmDeviceInfoVO gmDeviceInfoVO = new GmDeviceInfoVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmDeviceInfoVO);
    // validate device uuid and device type is not empty
    HashMap hmValidate = new HashMap();
    hmValidate.put("uuid", "20608");
    hmValidate.put("devicetype", "20610");
    (new GmProdCatUtil()).validateInput(gmDeviceInfoVO, hmValidate);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmDeviceInfoVO.getToken());

    HashMap hmParam = new HashMap();
    hmParam.put("USERID", gmUserVO.getUserid());
    hmParam.put("UUID", GmCommonClass.parseNull(gmDeviceInfoVO.getUuid()));
    hmParam.put("DEVICE_TYPE", GmCommonClass.parseNull(gmDeviceInfoVO.getDevicetype()));
    hmParam.put("DEVICETOKEN", GmCommonClass.parseNull(gmDeviceInfoVO.getDevicetoken()));

    // This method will save the Device Info.
    (new GmUserTokenBean()).saveDeviceInfo(hmParam);

    return gmDeviceInfoVO;
  }


  /**
   * 
   * This method is used to get all access details for the users
   * 
   * @throws Exception
   * 
   */
  @POST
  @Path("AppModuleAccess")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmLogonAccessVO fetchAppModuleAccess(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {
    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
    GmLogonAccessVO gmLogonAccessVO = new GmLogonAccessVO();
    GmLogOnVO gmLogOnVO = new GmLogOnVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    ArrayList alAccessResult = new ArrayList();
    HashMap hmCredential = new HashMap();
    HashMap hmReturn = new HashMap();
    log.debug("fetchAppModuleAccess strInput " + strInput);
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmLogOnVO);
    gmWSUtil.parseJsonToVO(strInput, gmLogOnVO);
    hmCredential = gmWSUtil.getHashMapFromVO(gmLogOnVO);
    String strUserId = GmCommonClass.parseNull((String) hmCredential.get("USERID"));
    alAccessResult = GmCommonClass.parseNullArrayList(gmLogonBean.fetchAppModuleAccess(strUserId));
    gmLogonAccessVO.setGmLogonAccess(alAccessResult);
    return gmLogonAccessVO;
  }

  /**
   * 
   * This method is used to get all Account Id based on the User Id
   * 
   * @throws Exception
   * 
   */
  @POST
  @Path("AppViewAccess")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmLogonAccessVO fetchAppViewAccess(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {
    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
    GmLogonAccessVO gmLogonAccessVO = new GmLogonAccessVO();
    // GmLogOnVO gmLogOnVO = new GmLogOnVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    ObjectMapper mapper = new ObjectMapper();
    ArrayList alAccessResult = new ArrayList();
    ArrayList alViewResult = new ArrayList();
    HashMap hmCredential = new HashMap();
    HashMap hmReturn = new HashMap();
    log.debug("fetchAppViewAccess strInput " + strInput);
    // validate token and parse JSON into VO
    GmLogOnVO gmLogOnVO = mapper.readValue(strInput, GmLogOnVO.class);
    parseInputAndValidateToken(strInput, gmLogOnVO);
    String strUserId = gmLogOnVO.getUserid();
    String strAcclvl = gmLogOnVO.getAcslvl();
    hmReturn = gmLogonBean.fetchAppViewAccess(strUserId, strAcclvl);
    alViewResult = (ArrayList) hmReturn.get("ALVIEWRESULT");
    alAccessResult = (ArrayList) hmReturn.get("ALACCESSRESULT");
    gmLogonAccessVO.setAlAccountId(alViewResult);
    gmLogonAccessVO.setAlAccess(alAccessResult);
    return gmLogonAccessVO;
  }
}