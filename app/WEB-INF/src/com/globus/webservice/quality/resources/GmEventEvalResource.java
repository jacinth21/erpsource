package com.globus.webservice.quality.resources;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;



import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.bean.GmCRMActivityBean;
import com.globus.quality.beans.GmEventEvalBean;
import com.globus.valueobject.quality.GmEventEvalVO;
 

/**
 * @author jgurunathan
 * This resource useing for event eval form save and fetch 	
 */
@Path("form")

public class GmEventEvalResource extends GmResource {

	
	 /**
	 * Method declare for logger
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	
	/**
	 * SAVE THE FORM WITH/WITHOUT GENERATE PDF
	 * @param strInput
	 * @return
	 * @throws AppError, JsonGenerationException, JsonMappingException, JsonParseException, IOException, Exception
	 */
	@POST
	  @Path("save") 
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  public GmEventEvalVO saveEventEvalForm(String strInput) throws AppError,
	      JsonGenerationException, JsonMappingException, JsonParseException, IOException, Exception {
		
		GmEventEvalVO gmEvalFormVO = new GmEventEvalVO();
	    GmWSUtil gmWSUtil = new GmWSUtil();
	    
	    GmJasperReport gmJasperReport = new GmJasperReport();
	   
	    List<GmEventEvalVO> listGmEvalFormVO =
	        new ArrayList<GmEventEvalVO>();
	   
	    gmWSUtil.parseJsonToVO(strInput, gmEvalFormVO);
	   
	    validateToken(gmEvalFormVO.getToken());
	    GmEventEvalBean gmEvalFormBean = new GmEventEvalBean(gmEvalFormVO);
	   
	    gmEvalFormVO.setUserid(gmUserVO.getUserid());
	    
	    HashMap hmInput = new HashMap();
	   
	    hmInput = gmWSUtil.getHashMapFromVO(gmEvalFormVO);
	    String strOpt = GmCommonClass.parseNull((String) hmInput.get("STROPT"));
	    String cmpDtFmt = GmCommonClass.parseNull((String) hmInput.get("CMPDFMT"));
	    ArrayList alReturn = new ArrayList();
	    if(strOpt.equals("draft")){
		    HashMap hmReturn = gmEvalFormBean.save(hmInput);
	    	String strResult= GmCommonClass.parseNull((String) hmReturn.get("FORMID"));
	    	String strErrorMsg= GmCommonClass.parseNull((String) hmReturn.get("ERRORMSG"));
	    	if(!strErrorMsg.equals("")){
	    		throw new AppError(strErrorMsg, "", 'E');
	    	}else{
	    		gmEvalFormVO.setEvalformid(strResult);
	    	}
		}
	    if(strOpt.equals("pdf"))
	    {	
	    	
			  HashMap hmReturn = gmEvalFormBean.save(hmInput);
			  HashMap hmPdfInput = new HashMap();
			  String strResult= GmCommonClass.parseNull((String) hmReturn.get("FORMID"));
			  hmPdfInput.put("FORMID", strResult);
			  hmPdfInput.put("CMPDFMT", cmpDtFmt);
			  
			  String strErrorMsg= GmCommonClass.parseNull((String) hmReturn.get("ERRORMSG"));
			  log.debug("strResult >>>>>>>>>>>>" + strResult);
			  if(!strErrorMsg.equals("")){
			     throw new AppError(strErrorMsg, "", 'E');
			  }else{
			     gmEvalFormVO.setEvalformid(strResult);
			  }
			  /*PMT-40110 : Due to this PMT try catch block has been added  */
			  try { 	
				HashMap hashData = new HashMap();		    
				hashData = gmEvalFormBean.fetchPDFData(hmPdfInput);
				String strFilePath =
				            createPDFFile(strResult, gmJasperReport, new HashMap(hashData),"GmEventEval.jasper");
				String createDate = gmEvalFormBean.updateCreatedate(strResult);
			   if(strFilePath != ""){
				   String strMailstatus = sendEvalMail(strFilePath);
				   gmEvalFormVO.setMailstatus(strMailstatus);
			    }
	    	} catch (Exception ex) {
	    		gmEvalFormVO.setMailstatus("fail");
	    		log.error("saveEventEvalForm==>"+ GmCommonClass.getExceptionStackTrace(ex));
	        } 
	      	     
	    }
	    return gmEvalFormVO;
	     
	  }

	/**
	 * FETCH THE FORM DETAILS TO MAKE CHANGES
	 * @param strInput
	 * @return
	 * @throws AppError, JsonGenerationException, JsonMappingException, JsonParseException, IOException, Exception
	 */
	@POST
	  @Path("fetch")  
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  public List<GmEventEvalVO> fetchEventEvalForm (String strInput) throws AppError,
	      JsonGenerationException, JsonMappingException, JsonParseException, IOException, Exception {
		 
	    GmWSUtil gmWSUtil = new GmWSUtil();
	   
	    ObjectMapper mapper = new ObjectMapper();
	    GmEventEvalVO gmEvalFormVO = mapper.readValue(strInput, GmEventEvalVO.class);
	    ArrayList alReturn = new ArrayList();
	     List<GmEventEvalVO> listGmEvalFormVO =   new ArrayList<GmEventEvalVO>();
	    validateToken(gmEvalFormVO.getToken());
	    GmEventEvalBean gmEvalFormBean = new GmEventEvalBean(gmUserVO);
	    
	    String repId = gmEvalFormVO.getRepid();
	    gmEvalFormVO.setUserid(gmUserVO.getUserid());
	    
	    HashMap hmInput = new HashMap();
	    gmWSUtil.parseJsonToVO(strInput, gmEvalFormVO);
	    hmInput = gmWSUtil.getHashMapFromVO(gmEvalFormVO);
	     
	    listGmEvalFormVO = gmEvalFormBean.fetchForm(hmInput);    //fetch the form
	    
	    	gmEvalFormVO.setListGmEvalFormVO(listGmEvalFormVO); 
	      
	   
	    return listGmEvalFormVO;
	     
	  }
	
	/**
	 * TO FETCH THE LIST OF FORM TO DASHBOARD
	 * @param strInput
	 * @return
	 * @throws AppError, JsonGenerationException, JsonMappingException, JsonParseException, IOException, Exception
	 */
	@POST
	  @Path("fetchlist") 
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  public List<GmEventEvalVO> fetchEventEvalRpt(String strInput) throws AppError,
	      JsonGenerationException, JsonMappingException, JsonParseException, IOException, Exception {
		 
	    GmWSUtil gmWSUtil = new GmWSUtil();
	   
	    ObjectMapper mapper = new ObjectMapper();
	    GmEventEvalVO gmEvalFormVO = mapper.readValue(strInput, GmEventEvalVO.class);
	    ArrayList alReturn = new ArrayList();
	     List<GmEventEvalVO> listGmEvalFormVO =   new ArrayList<GmEventEvalVO>();
	    validateToken(gmEvalFormVO.getToken());
	    GmEventEvalBean gmEvalFormBean = new GmEventEvalBean(gmUserVO);
	    String repId = gmEvalFormVO.getRepid();
	    gmEvalFormVO.setUserid(gmUserVO.getUserid());
	    
	    HashMap hmInput = new HashMap();
	    gmWSUtil.parseJsonToVO(strInput, gmEvalFormVO);
	    hmInput = gmWSUtil.getHashMapFromVO(gmEvalFormVO);
	     
	    listGmEvalFormVO = gmEvalFormBean.fetchformList(hmInput);
	   
	    return listGmEvalFormVO;
	     
	  }
	
	/**
	 * TO VOID OR DELETE THE FORM FROM DASHBOARD 
	 * @param strInput
	 * @return
	 * @throws AppError, JsonGenerationException, JsonMappingException, JsonParseException, IOException, Exception
	 */
	@POST
	  @Path("voidlist") 
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  public List<GmEventEvalVO> cancelEventEvalForm(String strInput) throws AppError,
	      JsonGenerationException, JsonMappingException, JsonParseException, IOException, Exception {
		 
	    GmWSUtil gmWSUtil = new GmWSUtil();
	   
	    ObjectMapper mapper = new ObjectMapper();
	    GmEventEvalVO gmEvalFormVO = mapper.readValue(strInput, GmEventEvalVO.class);
	    ArrayList alReturn = new ArrayList();
	     List<GmEventEvalVO> listGmEvalFormVO =   new ArrayList<GmEventEvalVO>();
	    validateToken(gmEvalFormVO.getToken());
	    GmEventEvalBean gmEvalFormBean = new GmEventEvalBean(gmUserVO);
	    String repId = gmEvalFormVO.getRepid();
	    gmEvalFormVO.setUserid(gmUserVO.getUserid());
	    
	    HashMap hmInput = new HashMap();
	    gmWSUtil.parseJsonToVO(strInput, gmEvalFormVO);
	    hmInput = gmWSUtil.getHashMapFromVO(gmEvalFormVO);
	    hmInput.put("REPID", repId);
	    listGmEvalFormVO = gmEvalFormBean.voidList(hmInput);
	     
	    return listGmEvalFormVO;
	     
	  }
	
	
	
	/**
	 * TO CREATE PDF FILE 
	 * @param strFile, gmJasperReport, hmParam, strJasperName
	 * @return
	 * @throws Exception
	 */
	public String createPDFFile(String strFile, GmJasperReport gmJasperReport, HashMap hmParam, String strJasperName) throws Exception {
		    String fileName = "";
		    String strFilePath = System.getProperty("ENV_EVENTEVALUPLOAD");
		    String strFileRev = GmCommonClass.getString("EVALFILEREV");
		    hmParam.put("FILEREV", strFileRev); 
		    try {		    
		    fileName = strFilePath+ strFile + ".pdf";
		    File newFile = new File(strFilePath);
		   
		    if (!newFile.exists()) {
		    	newFile.mkdirs();

			} 
	   
		    gmJasperReport.setJasperReportName(strJasperName);
		    gmJasperReport.setHmReportParameters(hmParam);
		    gmJasperReport.setReportDataList(null);
		     
		    gmJasperReport.exportJasperToPdf(fileName);
		    }  catch (Exception ex) {
		    	fileName = "";
		    	throw (ex);
	        }

		    return fileName;
		  }
	
	
	/**
	 * TO SEND EMAIL 
	 * @param strInput
	 * @return
	 * @throws AppError, Exception
	 */
	@POST
	  @Path("evalmail")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  public String sendEvalMail(String strInput) throws AppError,Exception {
		HashMap hashData = new HashMap();
	    HashMap hmSenderDetails = new HashMap();
	    String strTemplateName = "GmEventEvalEmail";	    
	    GmEmailProperties emailProps = new GmEmailProperties();
	    GmEventEvalVO gmEvalFormVO = new GmEventEvalVO();
	    GmEventEvalBean gmEvalFormBean = new GmEventEvalBean(gmEvalFormVO);  
	    String strFromMail = "";
	    String strToMail = "";
	    String strSubject = "";
	    String strName = "";
	    String strMailCC = "";
	    String strAttachmentFileName =strInput;
	    String strMessageDesc ="";
	    String strMailstatus="";
	    boolean bolResult = false;	    
	    GmWSUtil gmWSUtil = new GmWSUtil();
	    GmCRMActivityBean gmActivityBean = new GmCRMActivityBean();  
	    validateToken(gmEvalFormVO.getToken());
	    gmEvalFormVO.setUserid(gmUserVO.getUserid());
	    hmSenderDetails = gmActivityBean.getUserDetails(gmEvalFormVO.getUserid());
	    strMailCC =(String)hmSenderDetails.get("EMAILID");
	    strName = (String)hmSenderDetails.get("NAME");
	    String fileName = strAttachmentFileName.substring(strAttachmentFileName.lastIndexOf('\\') + 1);
	    String strfilename= fileName.substring(0,fileName.indexOf('.'));
	    hashData = gmEvalFormBean.fetchFormData(strfilename);
	    String strCreatedDate = GmCommonClass.parseNull((String) hashData.get("CREATEDDT"));
	    String strpartid = GmCommonClass.parseNull((String) hashData.get("PARTID"));
	    String strevalformid = GmCommonClass.parseNull((String) hashData.get("EVALFORMID"));
	    strSubject = strSubject +" - " + strName+ " - "+strpartid;
	    strSubject = GmCommonClass.getEmailProperty(strTemplateName + "."+ GmEmailProperties.SUBJECT)+strSubject + " (" + strfilename + ")";
	    strMessageDesc = GmCommonClass.getEmailProperty(strTemplateName + "."+ GmEmailProperties.MESSAGE)+strName + " on " + strCreatedDate + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <br><br>";
	    String strMailStatus = gmEvalFormVO.getMailstatus();	   
	    try {
	    	  emailProps.setSender(GmCommonClass.getEmailProperty(strTemplateName + "."+ GmEmailProperties.FROM));
		      emailProps.setRecipients(GmCommonClass.getEmailProperty(strTemplateName + "."+ GmEmailProperties.TO));
		      emailProps.setCc(strMailCC);
		      emailProps.setMessage(strMessageDesc);
		      emailProps.setMimeType(GmCommonClass.getEmailProperty(strTemplateName + "."+ GmEmailProperties.MIME_TYPE));
		      emailProps.setSubject(strSubject);
		      emailProps.setAttachment(strAttachmentFileName);
		      emailProps.setEmailHeaderName(GmCommonClass.getEmailProperty(strTemplateName + "."+ GmEmailProperties.MESSAGE_HEADER));
		      GmCommonClass.sendMail(emailProps);
		      bolResult = true;
	        } catch (Exception ex) {
        	  bolResult = false;
	          throw new AppError(ex);
	        }   

	    if (bolResult) {
	    	strMailstatus="success";
	    	gmEvalFormBean.updateEmailSent(strevalformid);
	    } else {
	    	strMailstatus="fail";
	    }
	    return strMailstatus;
	  }
	
	   
} 