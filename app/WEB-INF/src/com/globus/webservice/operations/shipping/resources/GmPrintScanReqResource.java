package com.globus.webservice.operations.shipping.resources;

import java.util.HashMap;
import java.util.MissingResourceException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.fedex.client.GmFedexShipLabelClient;
import com.globus.common.fedex.client.GmShipCarrierClient;
import com.globus.common.fedex.client.GmShipCarrierClientInterface;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.operations.shipping.beans.GmPrintPaperworkBean;
import com.globus.operations.shipping.beans.GmPrinterMapBean;
import com.globus.operations.shipping.beans.GmShipScanReqRptBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.operations.shipping.GmPrinterMapVO;
import com.globus.valueobject.operations.shipping.GmShipScanReqVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;
import com.globus.webservice.common.resources.GmResource;

/**
 * @author rshah This class used to print the packslip and ship/return labels. From SOAP/Device its
 *         called
 */
@Path("printreq")
public class GmPrintScanReqResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  private String strScanId = "";
  private String strScanType = "";
  private String strTxnId = "";
  private String strSource = "";
  private String strPrintLabel = "";
  private String strPrintRtnLabel = "";
  private String strPrintPaperwork = "";
  private String strUserId = "";
  private String strUserNm = ""; // Need to add this in UserVO or ScanReqVO.
  private String strUserShNm = ""; // Get the short name of user from GmUserVO
  private String strToteWeight = "";
  private String strLblErrMsg = "";
  private String strTrackNoLength = "";
  private String strCarrier = "";
  private String strClassName = "";
  private String strDivisionID = "";
  private String strCompanyDTFmt = "";

  // for post request
  @Path("printPaperwork")
  @POST
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  @Produces({MediaType.APPLICATION_JSON})
  public GmShipScanReqVO printScanReqDetails(String strInput) throws AppError, Exception {


    GmShipScanReqVO gmShipScanReqVO = new GmShipScanReqVO();
    parseInputAndValidateToken(strInput, gmShipScanReqVO);
    GmFedexShipLabelClient gmFedexShipLabelClient = new GmFedexShipLabelClient();

    HashMap hmPrintParam = new HashMap();
    HashMap hmTxnShipInfo = new HashMap();

    // get values from shipscanReq value object
    getValuesFromVO(gmShipScanReqVO);

    // get transaction detail, return as HashMap
    hmPrintParam = fetchTxnDetails();

    // get transaction shipping details
    fetchShippingTxnDetail(hmPrintParam);

    // print ship label (UPS, FEDEX)
    printShipCarrierLabel(hmPrintParam, gmShipScanReqVO);

    // set tracking number length (UPS, FEDEX)
    setTrackingNumberLength(gmShipScanReqVO);

    return gmShipScanReqVO;
  }

  /**
   * This method get the values form GmShipScanReqVO and set in variable.
   * 
   * @param gmShipScanReqVO
   */
  private void getValuesFromVO(GmShipScanReqVO gmShipScanReqVO) throws AppError {
    strScanId = GmCommonClass.parseNull(gmShipScanReqVO.getScanid());
    strScanType = GmCommonClass.parseNull(gmShipScanReqVO.getScantype());
    strTxnId = GmCommonClass.parseNull(gmShipScanReqVO.getTxnid());
    strSource = GmCommonClass.parseNull(gmShipScanReqVO.getSource());
    strPrintLabel = GmCommonClass.parseNull(gmShipScanReqVO.getPrintshiplabel());
    strPrintRtnLabel = GmCommonClass.parseNull(gmShipScanReqVO.getPrintreturnlabel());
    strPrintPaperwork = GmCommonClass.parseNull(gmShipScanReqVO.getPrintpaperwork());
    strUserId = GmCommonClass.parseNull(gmShipScanReqVO.getUserid());

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmShipScanReqVO.getToken());
    strUserNm = GmCommonClass.parseNull(gmUserVO.getUsernm()); // Need to add this in UserVO or
                                                               // ScanReqVO.
    strUserShNm = GmCommonClass.parseNull(gmUserVO.getShname()); // Get the short name of user from
                                                                 // GmUserVO
  }

  /**
   * This method fetch the Transaction details
   * 
   * @return HashMap object
   */
  private HashMap fetchTxnDetails() throws AppError {
    HashMap hmPrintParam = new HashMap();
    HashMap hmTxnInfo = new HashMap();

    GmShipScanReqRptBean gmShipScanReqRptBean = new GmShipScanReqRptBean();
    GmPrintPaperworkBean gmPrintPaperworkBean = new GmPrintPaperworkBean();

    if (strTxnId.equals("")) {
      hmTxnInfo =
          GmCommonClass.parseNullHashMap(gmShipScanReqRptBean.fetchShipScanTrans(strScanId,
              strScanType));
      strToteWeight =
          GmCommonClass.parseNull(gmPrintPaperworkBean.getToteBinWeight(strScanId, strScanType));
      strTxnId = GmCommonClass.parseNull((String) hmTxnInfo.get("TXNID"));
      strSource = GmCommonClass.parseNull((String) hmTxnInfo.get("SOURCE"));
      strDivisionID = GmCommonClass.parseNull((String) hmTxnInfo.get("DIVISION_ID"));
      strCompanyDTFmt = GmCommonClass.parseNull((String) hmTxnInfo.get("CMPDFMT"));
    } else { // should fetch the weight, only if the transaction id is there, other wise the above weight will be replaced
      strToteWeight = gmPrintPaperworkBean.getTxnWeight(strTxnId, strSource);
    }

    hmPrintParam.put("TXNID", strTxnId);
    hmPrintParam.put("SOURCE", strSource);
    hmPrintParam.put("SCANID", strScanId);
    hmPrintParam.put("SCANTYPE", strScanType);
    hmPrintParam.put("USERID", strUserId);
    hmPrintParam.put("USERNM", strUserNm);
    hmPrintParam.put("USERSHNM", strUserShNm);
    
    if (!strTxnId.equals("") && !strSource.equals("") && strDivisionID.equals("")){
        strDivisionID = GmCommonClass.parseNull(gmPrintPaperworkBean.getDivisionID(strTxnId, strSource));
     } 
    
    hmPrintParam.put("DIVISION_ID", strDivisionID);
    hmPrintParam.put("CMPDFMT", strCompanyDTFmt);
    log.debug("hmPrintParam=="+hmPrintParam);
    return hmPrintParam;
  }

  /**
   * This method fetch the Shipping Transaction details
   * 
   * @param hmPrintParam
   */
  private void fetchShippingTxnDetail(HashMap hmPrintParam) throws AppError {
    HashMap hmTxnShipInfo = new HashMap();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmTxnShipInfoVO gmTxnShipInfoVO = new GmTxnShipInfoVO();
    GmPrintPaperworkBean gmPrintPaperworkBean = new GmPrintPaperworkBean();
    hmTxnShipInfo =
        GmCommonClass.parseNullHashMap(gmPrintPaperworkBean.fetchShippingTxnDetails(hmPrintParam));
    log.error("hmTxnShipInfo ===" + hmTxnShipInfo);
    gmTxnShipInfoVO =
        (GmTxnShipInfoVO) gmWSUtil.getVOFromHashMap(hmTxnShipInfo, gmTxnShipInfoVO,
            gmTxnShipInfoVO.getPendingShippingProperties());
    gmTxnShipInfoVO.setUserid(strUserId);
    strCarrier = GmCommonClass.parseNull((String) hmTxnShipInfo.get("SHIPCARRIER"));
    hmPrintParam.put("SHIPINFO", gmTxnShipInfoVO);
    hmPrintParam.put("SHIPCARRIER", strCarrier);
  }

  /**
   * This method prints the Ship carrier label/Paperwork
   * 
   * @param hmTxnShipInfo , gmShipScanReqVO
   * @param gmShipScanReqVO
   * @throws AppError , Exception
   */
  private void printShipCarrierLabel(HashMap hmTxnShipInfo, GmShipScanReqVO gmShipScanReqVO)
      throws AppError {
    strCarrier = GmCommonClass.parseNull((String) hmTxnShipInfo.get("SHIPCARRIER"));
    // blException , If the mode is not UPS or FEDEX then ship label should not print
    boolean blException = false;
    log.error("strCarrier ===" + strCarrier);
    // Try and catch here added for handle the scenario if the carrier is other than UPS or FEDEX.
    // If the carrier is not UPS or FEDEX then process paperwork only.
    try {
      strClassName = GmCommonClass.parseNull(GmCommonClass.getShipCarrierString(strCarrier));
    } catch (MissingResourceException e) {
      log.error("Exception:" + e.getMessage() + " \n Cause:" + e.getCause());
      blException = true;
    }

    // check printLabel and print return label , if either one is Y then call
    // printCarrierSpecificLabel to print ship Label (UPS, FEDEX)
    if ((strPrintLabel.equals("Y") || strPrintRtnLabel.equals("Y")) && !blException) {
      printCarrierSpecificLabel(hmTxnShipInfo, gmShipScanReqVO);
    }

    // check printpaperwork , if it's Y, then call printPaperWork method to print paperwork.
    if (strPrintPaperwork.equals("Y")) {
      printPaperWork(gmShipScanReqVO, hmTxnShipInfo);
    }
  }

  /**
   * This method is used to Print carrier specific label
   * 
   * @param hmPrintParam ,gmShipScanReqVO
   * @throws AppError,Exception
   */
  private void printCarrierSpecificLabel(HashMap hmPrintParam, GmShipScanReqVO gmShipScanReqVO)
      throws AppError {
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmPrintStatus = new HashMap();

    hmPrintParam.put("PRINTSHIPLABEL", strPrintLabel);
    hmPrintParam.put("PRINTRETURNLABEL", strPrintRtnLabel);
    hmPrintParam.put("TOTEWEIGHT", strToteWeight);

    log.error("hmPrintParam ===" + hmPrintParam);

    GmShipCarrierClientInterface shipClient = GmShipCarrierClient.getInstance(strClassName);
    hmPrintStatus = shipClient.printLabel(hmPrintParam);

    // hmPrintStatus =
    // GmCommonClass.parseNullHashMap(gmFedexShipLabelClient.printLabel(hmPrintParam));
    // Following code create the JSON String for the exception. it will throw in device
    gmShipScanReqVO =
        (GmShipScanReqVO) gmWSUtil.getVOFromHashMap(hmPrintStatus, gmShipScanReqVO,
            gmShipScanReqVO.getPrintStatusMapProperties());
    strLblErrMsg = GmCommonClass.parseNull((String) hmPrintStatus.get("ERRMSG"));
    if (!strLblErrMsg.equals("")) {
      throw new AppError(strLblErrMsg, "", 'E');
    }
    // End Fedex Print
  }

  /**
   * This method used to print the pack slip
   * 
   * @param gmShipScanReqVO,hmPrintParam
   */
  private void printPaperWork(GmShipScanReqVO gmShipScanReqVO, HashMap hmPrintParam)
      throws AppError {

    GmPrintPaperworkBean gmPrintPaperworkBean = new GmPrintPaperworkBean();
    String strPaperwork = gmPrintPaperworkBean.printPaperwork(hmPrintParam);
    gmShipScanReqVO.setPrintpaperwork(strPaperwork);

  }

  /**
   * This method used to set the Tracking number length
   * 
   * @param gmShipScanReqVO
   */
  private void setTrackingNumberLength(GmShipScanReqVO gmShipScanReqVO) throws AppError {
    // try catch added, for the scenario if carrier selected not UPS or FEDEX.
    try {
      strTrackNoLength =
          GmCommonClass.parseNull(GmCommonClass.getShipCarrierString(strCarrier
              + "_TRACK_NO_LENGTH"));
      gmShipScanReqVO.setTrackingnolength(strTrackNoLength);
    } catch (Exception e) {
      log.debug("Exception:" + e.getMessage() + " \n Cause:" + e.getCause());
    }
  }


  @Path("printerMapping")
  @POST
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  @Produces({MediaType.APPLICATION_JSON})
  public GmPrinterMapVO printerMapping(String strInput) throws AppError, Exception { // This method
                                                                                     // is used to
                                                                                     // map printer
                                                                                     // to user
    GmPrinterMapVO gmPrinterMapVO = new GmPrinterMapVO();
    GmPrinterMapBean gmPrinterMapBean = new GmPrinterMapBean();

    parseInputAndValidateToken(strInput, gmPrinterMapVO); // to validate the token

    String strOpt = gmPrinterMapVO.getStropt();

    if (strOpt.equals("save")) { // if stropt= save, call below method to save the details to rules
                                 // table
      gmPrinterMapBean.savePrinterMapping(gmPrinterMapVO);
    }

    gmPrinterMapBean.fetchPrinterMapping(gmPrinterMapVO); // fetch printer mapping details of the
                                                          // user and drop down values
    return gmPrinterMapVO;
  }
}
