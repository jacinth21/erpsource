/**
 * 
 */
package com.globus.webservice.operations.shipping.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.quality.beans.GmPartAttributeBean;
import com.globus.valueobject.operations.GmTxnPartDtlVO;
import com.globus.valueobject.operations.shipping.GmShipScanReqVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.valueobject.operations.GmOperationsListVO;


/**
 * @author vprasath
 *
 */
@Path("partinfo")
public class GmPartDetailResource extends GmResource {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	 //for post request
  	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmOperationsListVO fetchPartDetails(String  strInput) throws AppError {
  		
  		GmTxnPartDtlVO gmTxnPartDtlVO = new GmTxnPartDtlVO();
  		GmShipScanReqVO gmShipScanReqVO = new GmShipScanReqVO();
  		GmPartAttributeBean gmPartAttributeBean = new GmPartAttributeBean();
  		GmOperationsListVO gmOperationsListVO = new GmOperationsListVO(); 
  		
   		GmWSUtil gmWSUtil = new GmWSUtil();
  		List<GmTxnPartDtlVO> gmPartDetailsVOList = new ArrayList<GmTxnPartDtlVO>();
  		parseInputAndValidateToken(strInput, gmShipScanReqVO);
  		String strTxnId = gmShipScanReqVO.getTxnid();
  		String strTxnType = gmShipScanReqVO.getSource();
  		gmPartDetailsVOList = gmPartAttributeBean.fetchPartDetails(strTxnId, strTxnType);	
  		
  		gmOperationsListVO.setGmTxnPartDtlVO(gmPartDetailsVOList);
  		return gmOperationsListVO ;
	}
}