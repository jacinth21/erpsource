/**
 * 
 */
package com.globus.webservice.operations.shipping.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.globus.operations.shipping.beans.GmShipScanReqTransBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.operations.shipping.beans.GmShippingInfoBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.operations.GmOperationsListVO;
import com.globus.valueobject.operations.GmTxnCountVO;
import com.globus.valueobject.operations.shipping.GmShipScanReqVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;
import com.globus.webservice.common.resources.GmResource;


/**
 * @author vprasath
 *
 */
@Path("shippinginfo")
public class GmShippingResource extends GmResource {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
  	@POST
  	@Path("fetchshippingcount")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmOperationsListVO fetchPendingShippingCount(String  strInput) throws AppError {
  		
  		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
  		GmShippingInfoBean gmShippingInfoBean = new GmShippingInfoBean();
  		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
  		GmOperationsListVO gmOperationsListVO = new GmOperationsListVO();
  		HashMap hmAccess = new HashMap();
  		GmWSUtil gmWSUtil = new GmWSUtil();
  		GmTxnCountVO gmTxnCountVO = new GmTxnCountVO();
  		
  		
  		
  		List<GmTxnCountVO> gmTxnCountVOList = new ArrayList<GmTxnCountVO>();
  		parseInputAndValidateToken(strInput, gmDataStoreVO);  		
  		
  		GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmDataStoreVO.getToken());
  		String strPartyId = gmUserVO.getPartyid();
  		
  		hmAccess = GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId, "SHIPPING_DASH")));

  		String strShippingAccess = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
  	    log.debug("Access: " + strShippingAccess);
  	    
  	    if(!strShippingAccess.equals("Y"))
  	    {
  	    	throw new AppError(AppError.APPLICATION,"1008");
  	    }
  	    
  		ArrayList alReturn =  gmShippingInfoBean.fetchPendingShippingCount(strPartyId);   // added for PMT-34028 - Shipping Device - Dashboard Filter
  		log.debug("alReturn: " + alReturn);

  		gmTxnCountVOList = gmWSUtil.getVOListFromHashMapList(alReturn, gmTxnCountVO);
  		gmOperationsListVO.setGmTxnCountVO(gmTxnCountVOList);
  		return gmOperationsListVO ;
	}
  	
  	@POST
  	@Path("fetchshippingdetails")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmOperationsListVO fetchPendingShippingDetails(String  strInput) throws AppError {
  		
  		GmShipScanReqVO gmShipScanReqVO = new GmShipScanReqVO();
  		GmShippingInfoBean gmShippingInfoBean = new GmShippingInfoBean();
  		GmWSUtil gmWSUtil = new GmWSUtil();
  		GmTxnShipInfoVO gmTxnShipInfoVO = new GmTxnShipInfoVO();
  		GmOperationsListVO gmOperationsListVO = new GmOperationsListVO();
  		List<GmTxnShipInfoVO> gmTxnShipInfoVOList = new ArrayList<GmTxnShipInfoVO>();
  		
  		
  		log.debug("strInput: " + strInput);
  		
  		List<GmTxnCountVO> gmTxnCountVOList = new ArrayList<GmTxnCountVO>();
  		parseInputAndValidateToken(strInput, gmShipScanReqVO);  		
  		
  		log.debug("fetchPendingShippingDetails gmTxnCountVO.getTxntype()  : " + gmShipScanReqVO.getSource());
  		
  		
  		
  		String strUserId =  GmCommonClass.parseNull(gmShipScanReqVO.getUserid());    // Added for PMT-34028 Shipping Device-Dashboard Filter
  		log.debug("fetchshippingdetails strUserId"+strUserId);
  		
  		HashMap hmParam = new HashMap();  		
  		hmParam.put("SOURCE", gmShipScanReqVO.getSource());
  		hmParam.put("SCANTYPE", gmShipScanReqVO.getScantype());
  		hmParam.put("STATUS", "30"); // Pending Shipping
  		hmParam.put("USERID", strUserId);   // Added for PMT-34028 Shipping Device-Dashboard Filter
  		ArrayList alReturn = gmShippingInfoBean.fetchShippingReport(hmParam);

  		log.debug("alReturn: " + alReturn);
  		gmTxnShipInfoVOList = gmWSUtil.getVOListFromHashMapList(alReturn, gmTxnShipInfoVO, gmTxnShipInfoVO.getShippingReportProperties());
  		
  		gmOperationsListVO.setGmTxnShipInfoVO(gmTxnShipInfoVOList);
  		return gmOperationsListVO ;
	}
  	
  	@POST
  	@Path("savShipRollback")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public void saveShipRollback(String  strInput) throws AppError {
  		
  		GmTxnShipInfoVO gmTxnShipInfoVO = new GmTxnShipInfoVO();
  		GmShipScanReqTransBean gmShipScanReqTransBean = new GmShipScanReqTransBean();
  		parseInputAndValidateToken(strInput, gmTxnShipInfoVO);
  		gmShipScanReqTransBean.saveShipRollback(gmTxnShipInfoVO); 		
  		
  	} 	

}