/**
 * 
 */
package com.globus.webservice.operations.shipping.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.operations.shipping.beans.GmShipScanReqRptBean;
import com.globus.operations.shipping.beans.GmShipScanReqTransBean;
import com.globus.valueobject.operations.GmOperationsListVO;
import com.globus.valueobject.operations.GmShipStationVO;
import com.globus.valueobject.operations.shipping.GmShipScanReqDtlVO;
import com.globus.valueobject.operations.shipping.GmShipScanReqVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;
import com.globus.webservice.common.resources.GmResource;
/**
 * @author rshah
 *
 */
@Path("scanreq")
public class GmShipScanReqResource extends GmResource {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	 //for post request
	@Path("fetchShipScanReq")
  	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmShipScanReqDtlVO fetchShipScanReqDetails(String  strInput) throws AppError {

		GmShipScanReqDtlVO gmShipScanReqDtlVO = new GmShipScanReqDtlVO();
		GmShipScanReqRptBean gmShipScanReqRptBean = new GmShipScanReqRptBean();

		parseInputAndValidateToken(strInput, gmShipScanReqDtlVO);
		gmShipScanReqRptBean.populateInputVO(gmShipScanReqDtlVO);
		gmShipScanReqRptBean.populateShippingDetails(gmShipScanReqDtlVO);
		gmShipScanReqRptBean.populatePartDetails(gmShipScanReqDtlVO);
		gmShipScanReqRptBean.populateRuleDetails(gmShipScanReqDtlVO);
  		
  		return gmShipScanReqDtlVO ;
	}
	
	 //for post request
	@Path("searchShippingTxnList")
  	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmOperationsListVO searchShippingTxnList(String  strInput) throws AppError {

		GmShipScanReqVO gmShipScanReqVO = new GmShipScanReqVO();
		GmTxnShipInfoVO gmTxnShipInfoVO = new GmTxnShipInfoVO();
		GmShipScanReqRptBean gmShipScanReqRptBean = new GmShipScanReqRptBean();
		GmOperationsListVO gmOperationsListVO = new GmOperationsListVO();
		List<GmTxnShipInfoVO> gmTxnShipInfoVOList = new ArrayList<GmTxnShipInfoVO>();		
		GmWSUtil gmWSUtil = new GmWSUtil();
		
		ArrayList alList = new ArrayList();
		
		parseInputAndValidateToken(strInput, gmShipScanReqVO);
		String strScanId = GmCommonClass.parseNull(gmShipScanReqVO.getScanid());
		String strScanType = GmCommonClass.parseNull(gmShipScanReqVO.getScantype());
		String strTxnId = GmCommonClass.parseNull(gmShipScanReqVO.getTxnid());
		String strSource = GmCommonClass.parseNull(gmShipScanReqVO.getSource());
		
		if (!strTxnId.equals(""))
		{
			alList = gmShipScanReqRptBean.fetchToteTxnList(strTxnId,strSource);
		}
		else if (!strScanId.equals(""))
		{
			alList = gmShipScanReqRptBean.fetchSearchTxnList(strScanId,strScanType);			
		}
		gmTxnShipInfoVOList = gmWSUtil.getVOListFromHashMapList(alList, gmTxnShipInfoVO, gmTxnShipInfoVO.getSearchTxnListProperties());  
		gmOperationsListVO.setGmTxnShipInfoVO(gmTxnShipInfoVOList);
  		return gmOperationsListVO ;
	}
	
	 //for post request
	@Path("searchShippingTxnDetail")
  	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmShipScanReqDtlVO searchShippingTxnDetail(String  strInput) throws AppError {

		GmShipScanReqDtlVO gmShipScanReqDtlVO = new GmShipScanReqDtlVO();
		GmShipScanReqRptBean gmShipScanReqRptBean = new GmShipScanReqRptBean();
		
		
		parseInputAndValidateToken(strInput, gmShipScanReqDtlVO);
		gmShipScanReqRptBean.populateInputVO(gmShipScanReqDtlVO);
		gmShipScanReqRptBean.populateShippingDetails(gmShipScanReqDtlVO);
		
		log.debug("Consign ID: " + (gmShipScanReqDtlVO.getGmTxnHeaderVO()).getTxnid());
		log.debug("EtchID: " + (gmShipScanReqDtlVO.getGmTxnHeaderVO()).getEtchid());
		
		return gmShipScanReqDtlVO ;
	}
	
	 //for post request
	@Path("searchShippingStation")
  	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmOperationsListVO searchShippingStation(String  strInput) throws AppError {

		GmShipScanReqVO gmShipScanReqVO = new GmShipScanReqVO();
		GmShipScanReqRptBean gmShipScanReqRptBean = new GmShipScanReqRptBean();
		List<GmShipStationVO> gmShipStationVOList = new ArrayList<GmShipStationVO>();
		GmShipStationVO gmShipStationVO = new GmShipStationVO();
		GmOperationsListVO gmOperationsListVO = new GmOperationsListVO();
		GmWSUtil gmWSUtil = new GmWSUtil();

		parseInputAndValidateToken(strInput, gmShipScanReqVO);
		ArrayList alList = gmShipScanReqRptBean.fetchStationDetails(gmShipScanReqVO.getScanid());
		gmShipStationVOList = gmWSUtil.getVOListFromHashMapList(alList, gmShipStationVO);
  		
		gmOperationsListVO.setGmShipStationVO(gmShipStationVOList);
  		return gmOperationsListVO ;
	}
  	
	//for post request
	@Path("saveScanStation")
  	@POST
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public void saveShipScanStation(String  strInput) throws AppError {
		
		GmTxnShipInfoVO gmTxnShipInfoVO = new GmTxnShipInfoVO();
		parseInputAndValidateToken(strInput, gmTxnShipInfoVO);
		GmShipScanReqTransBean gmShipScanReqTransBean = new GmShipScanReqTransBean();
		gmShipScanReqTransBean.saveShipStation(gmTxnShipInfoVO);
	}
	
	//for post request
	@Path("saveCompleteTote")
  	@POST  	
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public void saveCompleteTote(String  strInput) throws AppError {
		
		GmShipScanReqVO gmShipScanReqVO = new GmShipScanReqVO();
		parseInputAndValidateToken(strInput, gmShipScanReqVO);
		GmShipScanReqTransBean gmShipScanReqTransBean = new GmShipScanReqTransBean();
		gmShipScanReqTransBean.saveCompleteTote(gmShipScanReqVO);
	}
	
	//for post request
	@Path("fetchAssocToteStation")
  	@POST
  	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmOperationsListVO fetchAssocToteStation(String  strInput) throws AppError {
		
		GmShipScanReqVO gmShipScanReqVO = new GmShipScanReqVO();
		parseInputAndValidateToken(strInput, gmShipScanReqVO);
		GmShipScanReqRptBean shipScanReqRptBean = new GmShipScanReqRptBean();
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmShipStationVO gmShipStationVO = new GmShipStationVO();
		GmOperationsListVO gmOperationsListVO = new GmOperationsListVO();
		List<GmShipStationVO> gmShipStationVOList = new ArrayList<GmShipStationVO>();
		
		String strTxnId = gmShipScanReqVO.getTxnid();
		String strSource = gmShipScanReqVO.getSource();
		ArrayList alList = shipScanReqRptBean.fetchAssociatedToteStation(strTxnId, strSource);
		
		gmShipStationVOList = gmWSUtil.getVOListFromHashMapList(alList, gmShipStationVO);
		
		gmOperationsListVO.setGmShipStationVO(gmShipStationVOList);
		return gmOperationsListVO;
		
	}
}