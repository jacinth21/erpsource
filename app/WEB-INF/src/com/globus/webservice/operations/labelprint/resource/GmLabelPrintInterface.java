package com.globus.webservice.operations.labelprint.resource;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.beans.AppError;
import com.globus.valueobject.common.GmDataStoreVO;

public interface GmLabelPrintInterface {
  public ArrayList fetchLabelDataByPart(HashMap hmInData) throws AppError;
  public ArrayList fetchLabelDataByTxn(HashMap hmInData) throws AppError;
  //public ArrayList fetchPrinterStock(HashMap hmInData) throws AppError;
  public ArrayList fetchPartLabelPath(HashMap hmInData) throws AppError;

    
}
