package com.globus.webservice.operations.labelprint.resource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.globus.operations.shipping.beans.GmShipScanReqTransBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.operations.shipping.beans.GmLabelPrintInfoBean;
import com.globus.operations.shipping.beans.GmBBALabelPrintInfoBean;
import com.globus.sales.CaseManagement.beans.GmCaseBookTxnBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.operations.GmTxnCountVO;
import com.globus.valueobject.operations.GmLabelPartInfoVO;
import com.globus.valueobject.operations.GmLabelInfoVO;
import com.globus.valueobject.operations.GmLabelPrintListVO;
import com.globus.valueobject.operations.GmLabelStockInfoVO;
import com.globus.valueobject.operations.request.GmCaseAttributeVO;
import com.globus.valueobject.operations.request.GmCaseInfoVO;
import com.globus.valueobject.sales.GmConsignmentDrilldownVO;
import com.globus.valueobject.sales.GmProdRequestDetVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.operations.request.GmInvRequestResource;
import com.globus.valueobject.operations.GmOperationsListVO;
import com.globus.webservice.operations.labelprint.resource.GmLabelPrintInterface;

/**
 * @author tsekaran
 *
 */
@Path("labelprint")
public class GmLabelPrintResource extends GmResource {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
  	@POST
  	@Path("fetchtxninfo")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmLabelPrintListVO fetchPartInfo(String  strInput) throws AppError {
  		

  		GmLabelPartInfoVO gmLabelPartInfo = new GmLabelPartInfoVO();  
  		GmLabelInfoVO gmLabelInfo = new GmLabelInfoVO();
  		GmLabelPrintListVO gmLabelPrintList = new GmLabelPrintListVO();
//	  	log.debug("strInput***"+strInput);

  		parseInputAndValidateToken(strInput, gmLabelPartInfo);
	//  	log.debug("after parseInputAndValidateToken***"+gmLabelPartInfo);

	  		HashMap hmParam = new HashMap();
			GmWSUtil gmWSUtil = new GmWSUtil();
	  		List<GmLabelPartInfoVO> listGmLabelPartInfoVO = new ArrayList<GmLabelPartInfoVO>();
	  		GmLabelPrintInfoBean gmLabelPrintInfoBean= new GmLabelPrintInfoBean();
			// Instantiating the BBA Bean file
	  		GmBBALabelPrintInfoBean gmBBALabelPrintInfoBean= new GmBBALabelPrintInfoBean();
	  		
	  		hmParam = (HashMap)GmWSUtil.getHashMapFromVO(gmLabelPartInfo);
	  	   
		  	log.debug("after getHashMapFromVO***"+hmParam);

	  	String strOption = GmCommonClass.parseNull((String)gmLabelPartInfo.getStropt());
    	  String strCompanyID=GmCommonClass.parseNull((String)gmLabelPartInfo.getCompid());
	  if(strCompanyID.equals(""))
			  {
		  	     strCompanyID=GmCommonClass.getString("COMPANYID");
			  	log.debug("strCompanyID in if condition ***"+strCompanyID);
			  }
	
	  	log.debug("strOption ***"+strOption);

		//To Pass the Company Id from the LPA Rest API Reqeust.
		// String strCompanyID = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
	  	
	  	log.debug("Company id in webservice call"+strCompanyID);
		 GmLabelPrintInterface gmLabelPrintInterface = (GmLabelPrintInterface) GmBBALabelPrintInfoBean.getLabelPrintClass(strCompanyID);
		 log.debug("gmLabelPrintInterface value"+gmLabelPrintInterface);

  		
  		if(strOption.equals("PART"))
  		{
  		  log.debug("hmParam::: " + gmLabelPartInfo.getStropt()); 
		  
		   
  		  		ArrayList alReturn= gmLabelPrintInterface.fetchLabelDataByPart(hmParam);
  		  		log.debug("alReturn in main file"+alReturn);
  			// ArrayList alReturn =  gmLabelPrintInfoBean.fetchLabelDataByPart(hmParam);

	  		//Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo attributes 		
	  		listGmLabelPartInfoVO = (ArrayList)gmWSUtil.getVOListFromHashMapList(alReturn, gmLabelPartInfo, gmLabelPartInfo.getLabelDataProperties());
  		}
  	    else if(strOption.equals("TXN"))
  		{
			 ArrayList alReturn = gmLabelPrintInterface.fetchLabelDataByTxn(hmParam);
		  		log.debug("alReturn in TXN"+alReturn);
			//ArrayList alReturn =  gmLabelPrintInfoBean.fetchLabelDataByTxn(hmParam);	  		
	  		//Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo attributes 		
	  		listGmLabelPartInfoVO = (ArrayList)gmWSUtil.getVOListFromHashMapList(alReturn, gmLabelPartInfo, gmLabelPartInfo.getLabelDataProperties());
	  	//	log.debug("listGmLabelPartInfoVO in TXN"+listGmLabelPartInfoVO);

  		}
  		gmLabelPrintList.setlistGmLabelPartInfoVO(listGmLabelPartInfoVO);
  		if(listGmLabelPartInfoVO.size() > 0)
  		{
  			hmParam.clear();
  			String strInputStr ="";
  			for(int i=0;i<listGmLabelPartInfoVO.size();i++)
  			{
 		  	//	log.debug("listGmLabelPartInfoVO.size()"+listGmLabelPartInfoVO.size());

 				String strPartId 	= GmCommonClass.parseNull((String)listGmLabelPartInfoVO.get(i).getPartid());
 		  //		log.debug("strPartId"+strPartId);


 				if(i==listGmLabelPartInfoVO.size()-1)
 					strInputStr  += strPartId;
 				else
 				 strInputStr += strPartId+",";
  			}
  			hmParam.put("PARTID",strInputStr);
	  	//	log.debug("strInputStr"+strInputStr);
			
			  ArrayList alReturn = gmLabelPrintInterface.fetchPartLabelPath(hmParam);
		  	//	log.debug("alReturn in main file label"+alReturn);

  			//ArrayList alReturn =   gmLabelPrintInfoBean.fetchPartLabelPath(hmParam);
	  		
	  		//Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo attributes 		
	  		gmLabelPrintList.setListGmLabelInfoVO((ArrayList) gmWSUtil.getVOListFromHashMapList(alReturn, gmLabelInfo, gmLabelInfo.getLabelDataProperties()));
  			
  		}
  		
  		
  		return gmLabelPrintList ;
	}
	
	
	
	
	@POST
  	@Path("fetchprinterstock")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmOperationsListVO fetchPrinterStockInfo(String  strInput) throws AppError {
  		

		GmLabelStockInfoVO gmLabelStockInfo = new GmLabelStockInfoVO();  		
  		 parseInputAndValidateToken(strInput, gmLabelStockInfo);
  		 
  		  String strCompanyID=GmCommonClass.parseNull((String)gmLabelStockInfo.getCompid());
  		 
  		  if(strCompanyID.equals(""))
		  {
	  	     strCompanyID=GmCommonClass.getString("COMPANYID");
		  	log.debug("strCompanyID in if condition ***"+strCompanyID);
		  }

	  GmLabelPrintInterface gmLabelPrintInterface =
        (GmLabelPrintInterface) GmBBALabelPrintInfoBean.getLabelPrintClass(strCompanyID);
  		 
 		//validate token
 		validateToken(gmLabelStockInfo.getToken());	
  		
  		
	  		HashMap hmParam = new HashMap();
			GmWSUtil gmWSUtil = new GmWSUtil();
	  		List<GmLabelStockInfoVO> listGmLabelStockInfoVO= new ArrayList<GmLabelStockInfoVO>();
	  		GmLabelPrintInfoBean gmLabelPrintInfoBean= new GmLabelPrintInfoBean();
	  		GmOperationsListVO gmOperationsListVO = new GmOperationsListVO();
	  		
	  		hmParam = (HashMap)GmWSUtil.getHashMapFromVO(gmLabelStockInfo);
			
			
  			// ArrayList alReturn =  gmLabelPrintInterface.fetchPrinterStock();

  			ArrayList alReturn =  gmLabelPrintInfoBean.fetchPrinterStock();
	  		
	  		//Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo attributes 		
	  		listGmLabelStockInfoVO = (ArrayList) gmWSUtil.getVOListFromHashMapList(alReturn, gmLabelStockInfo, gmLabelStockInfo.getLabelDataProperties());
  		
	  		gmOperationsListVO.setGmLabelStockInfoVO(listGmLabelStockInfoVO);
  		return gmOperationsListVO ;
	}
  	@POST
  	@Path("updlblpath")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String savePartLabelPath(String  strInput) throws AppError,IOException {
  		
  		
		//Object intialization
		GmWSUtil gmWSUtil = new GmWSUtil();	
		HashMap hmParam = new HashMap();
		HashMap hmReturn = new HashMap();
		
		String strInputStr ="";
		String strPathInputStr ="";

		GmLabelPrintListVO gmLabelPrintList = new GmLabelPrintListVO();
		GmLabelInfoVO gmLabelInfo = new GmLabelInfoVO();  

		ArrayList<GmLabelInfoVO> listGmLabelInfoVO= null;  
		GmLabelPrintInfoBean  gmLabelPrintInfoBean  = new GmLabelPrintInfoBean();

		
		ObjectMapper mapper = new ObjectMapper();
		gmLabelPrintList = mapper.readValue(strInput,GmLabelPrintListVO.class);
		
		//validate token
		validateToken(gmLabelPrintList.getToken());	
  		
		 gmLabelPrintList.setUserid(gmUserVO.getUserid());
  		// parseInputAndValidateToken(strInput, gmLabelPathInfo);

  		//Convert VOs to DB input String
			GmLabelInfoVO [] arrGmLabelInfoVO =  gmLabelPrintList.getArrGmLabelInfoVO();
 		int setlength=0;
 		if(arrGmLabelInfoVO != null){
 				setlength = arrGmLabelInfoVO.length;
 		}

 		if(setlength > 0){
 			for (int i= 0;i<setlength;i++){	

 				String strPartId 	= GmCommonClass.parseNull((String)arrGmLabelInfoVO[i].getPartid());
 				String strLabelType = GmCommonClass.parseNull((String)arrGmLabelInfoVO[i].getStocktype());
 				String strLabelPath = GmCommonClass.parseNull((String)arrGmLabelInfoVO[i].getLabelpath());
 				
 				strInputStr +=strPartId+"^"+strLabelType+"^"+strLabelPath+"|";
 			}
 			
 			String strUserId = GmCommonClass.parseNull((String)gmUserVO.getUserid());
 			
 			hmParam.put("STRINPUT", strInputStr);
 		    hmParam.put("USERID", strUserId);
 		  	String strOption = GmCommonClass.parseNull((String)gmLabelPrintList.getStropt());
 	  		
 	  		if(strOption.equals("SAVE"))
 	  			gmLabelPrintInfoBean.saveLabelPath(strInputStr, strUserId);
 	  		if(strOption.equals("VOID"))
 	  		  gmLabelPrintInfoBean.voidLabelPath(strInputStr, strUserId);
 	
	}
 		return "Save Success";

  	}
  	@POST
  	@Path("voidlblpath")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String voidPartLabelPath(String  strInput) throws AppError,IOException {
  		
  		
		//Object intialization
		GmWSUtil gmWSUtil = new GmWSUtil();	
		HashMap hmParam = new HashMap();
		HashMap hmReturn = new HashMap();
		
		String strInputStr ="";
		String strPathInputStr ="";

		GmLabelPrintListVO gmLabelPrintList = new GmLabelPrintListVO();
		GmLabelInfoVO gmLabelInfo = new GmLabelInfoVO();  

		ArrayList<GmLabelInfoVO> listGmLabelInfoVO= null;  
		GmLabelPrintInfoBean  gmLabelPrintInfoBean  = new GmLabelPrintInfoBean();

		
		ObjectMapper mapper = new ObjectMapper();
		gmLabelPrintList = mapper.readValue(strInput,GmLabelPrintListVO.class);
		
		//validate token
		validateToken(gmLabelPrintList.getToken());	
  		
		 gmLabelPrintList.setUserid(gmUserVO.getUserid());
  		// parseInputAndValidateToken(strInput, gmLabelPathInfo);

  		//Convert VOs to DB input String
			GmLabelInfoVO [] arrGmLabelInfoVO =  gmLabelPrintList.getArrGmLabelInfoVO();
 		int setlength=0;
 		if(arrGmLabelInfoVO != null){
 				setlength = arrGmLabelInfoVO.length;
 		}

 		if(setlength > 0){
 			for (int i= 0;i<setlength;i++){	

 				String strPartId 	= GmCommonClass.parseNull((String)arrGmLabelInfoVO[i].getPartid());
 				String strLabelType = GmCommonClass.parseNull((String)arrGmLabelInfoVO[i].getStocktype());
 				String strLabelPath = GmCommonClass.parseNull((String)arrGmLabelInfoVO[i].getLabelpath());
 				
 				strInputStr +=strPartId+"^"+strLabelType+"^"+strLabelPath+"|";
 			}
 			
 			String strUserId = GmCommonClass.parseNull((String)gmUserVO.getUserid());
 			
 			hmParam.put("STRINPUT", strInputStr);
 		    hmParam.put("USERID", strUserId);
   	
 		   gmLabelPrintInfoBean.voidLabelPath(strInputStr, strUserId);
 		   
 	
	}
 		return "Save Success";

  	}	
  	

}