package com.globus.webservice.operations.rfid.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.operations.rfid.GmRFIDRequestVO;
import com.globus.valueobject.operations.rfid.GmRFIDResponseVO;
import com.globus.valueobject.pricing.GmGroupVO;

/**
 * @author karthiks
 *
 */
public class GmRFIDTransBean extends GmBean  {

	  Logger log = GmLogger.getInstance(this.getClass().getName());
	  
	  public GmRFIDTransBean() {
		    super(GmCommonClass.getDefaultGmDataStoreVO());
		  }

		  /**
		   * Constructor will populate company info.
		   * 
		   * @param gmDataStore
		   */
		  public GmRFIDTransBean(GmDataStoreVO gmDataStore) {
		    super(gmDataStore);
		  }
		  
	  /**
	   * saveRFIDBatchDetails - This method will save the Input JSON string in to the master table.
	   * 
	   * @param String strUsername
	   * @param String strPassword
	   * @return HashMap
	   * @exception AppError
	   */
	  public GmRFIDResponseVO saveRFIDBatchDetails(HashMap<String, String> hmParams) throws AppError {
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    GmRFIDResponseVO gmRFIDResponseVO = new GmRFIDResponseVO();
		    HashMap hmJmsParam = new HashMap();
		    String strJSONInput = GmCommonClass.parseNull(hmParams.get("JSONDATA"));
		    String strUserID = GmCommonClass.parseNull(hmParams.get("USERID"));
		    String strRFIDBatchId = "";
		    gmDBManager.setPrepareString("gm_pkg_op_rfid_process.gm_sav_batch_details", 3);
		    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
		    gmDBManager.setString(1, strJSONInput);
		    gmDBManager.setString(2, strUserID);
		    gmDBManager.execute();
		    strRFIDBatchId = GmCommonClass.parseNull(gmDBManager.getString(3));
		    log.debug("strRFIDBatchId===" + strRFIDBatchId);
		    gmDBManager.commit();

		    gmRFIDResponseVO.setGlobusbatchid(strRFIDBatchId);
		    // Calling JMS to update the tag details for RFID
		    hmJmsParam.put("USERID", hmParams.get("USERID"));
		    hmJmsParam.put("RFIDBATCHID", strRFIDBatchId);
		    saveRFIDTagByJMS(hmJmsParam);
		    
		    return gmRFIDResponseVO;
		  }
	  
	  /**
	   * saveRFIDTagByJMS - To update the tag details for RFID
	   * 
	   * @param -
	   * @return - Void
	   * @exception - AppError
	   */
	  public void saveRFIDTagByJMS(HashMap hmParam) throws AppError {

	    GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
	    String strConsumerClass =
	        GmCommonClass.parseNull(GmJMSConsumerConfigurationBean
	            .getJmsConfig("RFID_CONSUMER_CLASS"));
	    hmParam.put("HMJMSPARAM", hmParam);
	    hmParam.put("CONSUMERCLASS", strConsumerClass);
	    gmConsumerUtil.sendMessage(hmParam);

	  }
	  
		/**
		 * saveInsertDetail - This method will update or save the FRID tag table in DFID batch tag detail table.
		 * @param String
		 * @exception AppError
		 */
		public void saveRFIDTagDetails(String strRFIDBatchId ,String strUserId) throws AppError {

		  log.debug("saveRFIDTagDetails :");
		  
		  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		  gmDBManager.setPrepareString("gm_pkg_op_rfid_process.gm_sav_tag_details", 2);
		  gmDBManager.setString(1, strRFIDBatchId);
		  gmDBManager.setString(2, strUserId);
		  gmDBManager.execute();
		  gmDBManager.commit();
		}
	  
}
