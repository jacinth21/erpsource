package com.globus.webservice.operations.rfid.resources;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.operations.rfid.GmRFIDRequestVO;
import com.globus.valueobject.operations.rfid.GmRFIDResponseVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.valueobject.GmCRMPSCaseReqMapVO;
import com.globus.webservice.operations.rfid.beans.GmRFIDTransBean;

/**
 * GmRFIDResource: This class contains methods that are related to RFID related details.
 * @author karthik
 **/
@Path("globusRFIDUpdate")
public class GmRFIDResource extends GmResource {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * saveRFIDBatchDetails - This method will save the RFID Batch details
	 * 
	 * @param strInput
	 * @return log
	 * @throws AppError
	 */
	@POST
	@Path("saveRFIDBatchDetails")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmRFIDResponseVO saveRFIDBatchDetails(String strInput)throws Exception, AppError, JsonParseException,
		      JsonMappingException, IOException  {

		log.debug("======= saveRFIDBatchDetails ======" + strInput);		
		ObjectMapper mapper = new ObjectMapper();
		HashMap<String, String> hmInput = new HashMap<String, String>();
		String strToken = "";
		String strErrorMessage = "";
		String strCompAccess = "";
		String[] strErrorMessages = new String[2];		
		String strCompID = "";
		boolean blflag = false;
		// Converting JSON input string into VO
		GmRFIDRequestVO gmRFIDRequestVO = mapper.readValue(strInput, GmRFIDRequestVO.class);
		GmRFIDResponseVO gmRFIDResponseVO = new GmRFIDResponseVO();
		GmRFIDTransBean gmRFIDTransBran = new GmRFIDTransBean();
		    
		//Validate the Token
		GmWSUtil gmWSUtil = new GmWSUtil();		
		
		strCompID = GmCommonClass.parseNull(gmRFIDRequestVO.getCmpid());
		strCompID = strCompID.equals("")?"1000":strCompID;
		log.debug("======= Before ======" + strInput);
		log.debug("======= CompID ======" + strCompID);
		gmWSUtil.parseJsonToVO(strInput, gmRFIDRequestVO);
		strToken = GmCommonClass.parseNull(gmRFIDRequestVO.getToken());
		log.debug("======= strToken ======" + strToken);
		if(!strToken.equals("")){
		blflag = parseInputAndValidateRFIDToken(strToken);
		}
		log.debug("======= blflag ======" + blflag);
		
		if (!blflag){
			strErrorMessage = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("INVALID","RFIDSTATUS",strCompID));		
		}else{		
			strCompAccess = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany(strCompID,"RFIDCOMP",strCompID));
			if (!strCompAccess.equals("Y")){
				strErrorMessage = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("FAILURE","RFIDSTATUS",strCompID));
			}else{				
				//Save the input JSON string in the Master table
				hmInput.put("JSONDATA", strInput);
				gmRFIDResponseVO = gmRFIDTransBran.saveRFIDBatchDetails(hmInput);
				strErrorMessage = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SUCCESS","RFIDSTATUS",strCompID));
			}
		}
		log.debug("======= strErrorMessage ======" + strErrorMessage);
		if (strErrorMessage.indexOf("/") != -1){
			strErrorMessages = strErrorMessage.split("/");	
			gmRFIDResponseVO.setStatus(strErrorMessages[0]);
		    gmRFIDResponseVO.setStatusdesc(strErrorMessages[1]);
		}
		log.debug("======= gmRFIDResponseVO ======" + gmRFIDResponseVO);
		return gmRFIDResponseVO;
	}
	
}


