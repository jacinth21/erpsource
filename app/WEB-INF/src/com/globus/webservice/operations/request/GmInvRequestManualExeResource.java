/**
 * 
 */
package com.globus.webservice.operations.request;

import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.sales.CaseManagement.beans.GmCaseBookTxnBean;
import com.globus.webservice.common.resources.GmResource;

/*
 * The Below Method is added as part of PMT-5928. This is used to Manually Approve the
 * Item/Loaner/Set Initiated from Device. This is used to remove the Approval Time Lag between
 * Device and Portal.
 */
@Path("requestmanualsave")
public class GmInvRequestManualExeResource extends GmResource {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  @GET
  @Path("Approval")
  @Produces("application/Xml")
  public void ProcessInvRequest(@QueryParam("refid") String strRefId,
      @QueryParam("reftype") String strRefType, @QueryParam("userid") String strUserId,
      @QueryParam("operationtype") String strOperationType) throws Exception {

    GmCaseBookTxnBean gmCaseBookTxnBean = new GmCaseBookTxnBean();
    GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
    String strConsumerClass =
        GmCommonClass.parseNull(GmCommonClass.getString("INV_REQUEST_CONSUMER_CLASS"));

    HashMap hmParam = new HashMap();
    hmParam.put("CASESTATUS", "1");
    hmParam.put("CASEINFOID", strRefId);
    hmParam.put("TYPE", strRefType);
    hmParam.put("USERID", strUserId);
    hmParam.put("CONSUMERCLASS", strConsumerClass);

    if (!strOperationType.equals("resend") && !strOperationType.equals("void")) {
      throw new AppError("", "20658", 'E');
    }

    if (strOperationType.equals("void")) {
      gmCaseBookTxnBean.saveVoidInvRequest(hmParam);
    } else if (strOperationType.equals("resend")) {
      gmConsumerUtil.sendMessage(hmParam);
    }


  }
}
