/**
 * 
 */
package com.globus.webservice.operations.request;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.sales.CaseManagement.beans.GmCaseBookRptBean;
import com.globus.sales.CaseManagement.beans.GmCaseBookTxnBean;
import com.globus.sales.InvAllocation.actions.GmLoanerRequestApprlAction;
import com.globus.sales.InvAllocation.beans.GmLoanerRequestApprovalBean;
import com.globus.sales.InvAllocation.beans.GmPendAllocationBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.operations.request.GmCaseAttributeVO;
import com.globus.valueobject.operations.request.GmCaseInfoVO;
import com.globus.valueobject.operations.request.GmCaseListVO;
import com.globus.valueobject.sales.GmProdRequestDetVO;
import com.globus.webservice.common.resources.GmResource;


/**
 * MIM- Case resource - This is used to create case requests.
 * 
 * @author Elango
 * 
 */
@Path("request")
public class GmInvRequestResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * saveRequestInfo - This method is used to save the Case requests from Device.
   * 
   * @param strInput
   * @return GmCaseInfoVO
   * @throws AppError
   */
  @POST
  @Path("saverequest")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCaseInfoVO saveRequestInfo(String strInput) throws AppError, Exception, IOException,
      JsonGenerationException, JsonMappingException, JsonParseException, ParseException {

    log.debug("=======MIM - saveRequestInfo======" + strInput);

    // Object intialization
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    GmCaseInfoVO gmCaseInfoVO = new GmCaseInfoVO();
    GmCaseAttributeVO gmCaseAttributeVO = new GmCaseAttributeVO();
    ArrayList<GmCaseInfoVO> listGmCaseInfoVO = null;
   
    GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
    ObjectMapper mapper = new ObjectMapper();
    gmCaseInfoVO = mapper.readValue(strInput, GmCaseInfoVO.class);
    GmCaseBookTxnBean gmCaseBookTxnBean = new GmCaseBookTxnBean(gmCaseInfoVO);
    // validate token
    validateToken(gmCaseInfoVO.getToken());

    // check for the case request type is not empty.
    String strType = GmCommonClass.parseNull(gmCaseInfoVO.getType());
    if (strType.equals("")) {
      throw new AppError("", "20641", 'E');
    }
    if (strType.equals("1006507") || strType.equals("107286")) {//1006507--Item Consignment--107286--Account Item Consignment

      String strComments = GmCommonClass.parseNull(gmCaseInfoVO.getPrnotes());
      if (strComments.equals("")) {
        throw new AppError("", "20657", 'E');
      }
    }

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmCaseInfoVO.getToken());
    gmCaseInfoVO.setUserid(gmUserVO.getUserid());

    hmParam.put("GMCASEINFOVO", gmCaseInfoVO);
    hmParam.put("USERID", gmUserVO.getUserid());

    // Call to bean, to save the request details.

    hmReturn = GmCommonClass.parseNullHashMap(gmCaseBookTxnBean.saveRequestDetail(hmParam));
    HashMap hmApprovalData = new HashMap();
    String strConsumerClass =
        GmCommonClass.parseNull(GmCommonClass.getString("INV_REQUEST_CONSUMER_CLASS"));

    hmApprovalData.put("CASESTATUS", "1");
    hmApprovalData.put("CASEINFOID", hmReturn.get("CASEINFOIDFROMDB"));
    hmApprovalData.put("TYPE", hmReturn.get("TYPE"));
    hmApprovalData.put("CONSUMERCLASS", strConsumerClass);
    gmConsumerUtil.sendMessage(hmApprovalData);

    GmCaseInfoVO gmRtnCaseInfoVO = new GmCaseInfoVO();

    gmRtnCaseInfoVO.setCainfoid((String) hmReturn.get("CASEINFOIDFROMDB"));
    gmRtnCaseInfoVO.setCaseid((String) hmReturn.get("CASEIDFROMDB"));

    // Object nullifying
    gmCaseInfoVO = null;
    hmParam = null;
    hmReturn = null;

    return gmRtnCaseInfoVO;
  }

  /**
   * saveRequestInfo - This method is used to save the Case requests from Device.
   * 
   * @param strInput
   * @return GmCaseInfoVO
   * @throws AppError
   */
  @POST
  @Path("savereqattr")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCaseInfoVO saveRequestAttr(String strInput) throws AppError, Exception, IOException,
      JsonGenerationException, JsonMappingException, JsonParseException, ParseException {

    log.debug("=======MIM - saveRequestAttr======" + strInput);

    // Object intialization
    HashMap hmParam = new HashMap();
    GmCaseInfoVO gmCaseInfoVO = new GmCaseInfoVO();
    GmCaseBookTxnBean gmCaseBookTxnBean = new GmCaseBookTxnBean();

    ObjectMapper mapper = new ObjectMapper();
    gmCaseInfoVO = mapper.readValue(strInput, GmCaseInfoVO.class);

    // validate token
    validateToken(gmCaseInfoVO.getToken());
    gmCaseInfoVO.setUserid(gmUserVO.getUserid());

    hmParam.put("GMCASEINFOVO", gmCaseInfoVO);
    hmParam.put("USERID", gmUserVO.getUserid());

    // Call to bean, to save the request attribute details.
    gmCaseBookTxnBean.saveRequestCaseAttribute(hmParam);

    GmCaseInfoVO gmRtnCaseInfoVO = new GmCaseInfoVO();


    return gmCaseInfoVO;
  }

  /**
   * saveItemApprvlQty - This method is used to saving the approved quantity which is approved by
   * AD/PD.
   * 
   * @param strInput
   * @return GmCaseListVO
   * @throws AppError
   */
  @POST
  @Path("saveitemqty")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCaseListVO saveItemApprvlQty(String strInput) throws AppError, Exception, IOException,
      JsonGenerationException, JsonMappingException, JsonParseException, ParseException {

    // Object initialization
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    GmCaseListVO gmCaseListVO = new GmCaseListVO();
    GmProdRequestDetVO gmProdRequestDetVO = new GmProdRequestDetVO();
    ArrayList alProdReqDetails, listGmProdRequestDetVO = null;
    GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean();
    GmCaseBookTxnBean gmCaseBookTxnBean = new GmCaseBookTxnBean();
    GmPendAllocationBean gmPendAllocationBean = new GmPendAllocationBean();
    GmLoanerRequestApprlAction gmLoanerRequestApprlAction = new GmLoanerRequestApprlAction();
    GmLoanerRequestApprovalBean gmLoanerRequestApprovalBean = new GmLoanerRequestApprovalBean();
    String strInputStr = "";
    String strProdInputStr = "";
    String strPrReqDetLitePnum = "";
    String strReqDetId = "";

    HashMap hmdetails = new HashMap();
    ObjectMapper mapper = new ObjectMapper();
    gmCaseListVO = mapper.readValue(strInput, GmCaseListVO.class);

    // validate token
    validateToken(gmCaseListVO.getToken());
    gmCaseListVO.setUserid(gmUserVO.getUserid());

    // check for the strOpt is not empty.
    String strOpt = GmCommonClass.parseNull(gmCaseListVO.getStropt());
    if (strOpt.equals("") || !strOpt.equals("approve")) {
      throw new AppError("", "20643", 'E');
    }

    // Convert VOs to DB input String
    GmProdRequestDetVO[] arrGmProdRequestDetVO = gmCaseListVO.getArrGmProdRequestDetVO();
    int setlength = 0;
    if (arrGmProdRequestDetVO != null) {
      setlength = arrGmProdRequestDetVO.length;
    }

    if (setlength > 0) {
      for (int i = 0; i < setlength; i++) {

        String strProdReqId = GmCommonClass.parseNull(arrGmProdRequestDetVO[i].getPrdreqdid());
        String strApprQty = GmCommonClass.parseNull(arrGmProdRequestDetVO[i].getAprqty());
        String strPartNum = GmCommonClass.parseNull(arrGmProdRequestDetVO[i].getPnum());

        strInputStr += strProdReqId + "," + strPartNum + "," + strApprQty + "|";
        strProdInputStr += strProdReqId + ",";
      }
      String strUserId = GmCommonClass.parseNull(gmUserVO.getUserid());
      gmPendAllocationBean.saveRequestItemQty(strInputStr, strUserId);

      hmParam.put("STRREQIDS", strProdInputStr);
      hmParam.put("USERID", strUserId);
      hmParam.put("NEXTAPPRSTATUS", "5");

      hmReturn = GmCommonClass.parseNullHashMap(gmPendAllocationBean.saveLoanerReqAppv(hmParam));

      strPrReqDetLitePnum = GmCommonClass.parseNull((String) hmReturn.get("PRREQDETLITEPNUM"));
      strReqDetId = GmCommonClass.parseNull((String) hmReturn.get("PRODREQID"));
      // below method is used to send the email notification
      hmdetails.put("STRREQIDS", strProdInputStr);
      gmLoanerRequestApprovalBean.fetchADApprovalDtl(hmdetails);

      if (!strPrReqDetLitePnum.equals("")) {
        gmCaseBookTxnBean.saveRequestInfo(strPrReqDetLitePnum , "");
        hmdetails.put("STRREQIDS", strPrReqDetLitePnum);
        gmLoanerRequestApprovalBean.fetchPDApprovalDtl(hmdetails);
      }
    }
    return gmCaseListVO;
  }
}
