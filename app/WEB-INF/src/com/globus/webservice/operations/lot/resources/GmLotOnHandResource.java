package com.globus.webservice.operations.lot.resources;



import java.io.IOException; 
import java.util.HashMap;

import org.apache.log4j.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.annotation.JacksonFeatures;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.operations.lot.beans.GmLotHandReportBean;
import com.globus.valueobject.operations.GmLotOnHandVO;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.valueobject.sales.GmSalesDashResVO;
import com.globus.webservice.common.resources.GmResource;



/**
 * 
 * Pass the user vo and get required data for access filter.
 * HashMap hmParam =
 * GmCommonClass.<i>parseNullHashMap</i>(gmProdCatUtil.populateSalesParam(<font
 * color="#0000c0">gmUserVO</font>,
 *              (<font color="#7f0055"><b>new</b></font> GmSalesDashReqVO())));
 * 
 * set additional parameters into to same hashmap and pass into methods in
 * GmLotHandReportBean
 * 
 * refer: GmCRMActivityResource
 * @author aprasath
 * @version 1.0
 * @created 08-Apr-2019 9:50:43 PM
 */
@Path("lot")
public class GmLotOnHandResource extends GmResource {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	public GmLotOnHandResource(){

	}

	
	/**
	 * This method used to show the lots on hand with part number an count
	 * 
	 * 
	 * @param input
	 */
	 @POST
	  @Path("lotonhand")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public GmLotOnHandVO fetchLotsOnHand(String input) throws AppError, JsonParseException,
    JsonMappingException, IOException{
			GmLotOnHandVO gmLotOnHandVO = new GmLotOnHandVO(); 
			GmWSUtil gmWSUtil = new GmWSUtil();
			GmLotHandReportBean gmLotHandReportBean = new GmLotHandReportBean();
			ObjectMapper mapper = new ObjectMapper();
			GmLotOnHandVO gmLotOnHandVo = mapper.readValue(input, GmLotOnHandVO.class);//to read the input
			validateToken(gmLotOnHandVo.getToken()); 
			HashMap hmInput = new HashMap();
			HashMap hmAccessFilter  = fetchAccessFilter();
			hmInput.put("ACCESSFILTER",GmCommonClass.parseNullHashMap(hmAccessFilter));
			gmLotOnHandVO = gmLotHandReportBean.fetchLotsOnHand(hmInput);
			return gmLotOnHandVO;
	}

	/**
	 * This method used to show the expiring lots on hand with part number and count based on expiry date
	 * 
	 * @param input
	 */
	 
	 @POST
	  @Path("lotexpbydays")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public GmLotOnHandVO fetchLotExpByDays(String input) throws AppError, JsonParseException,
    JsonMappingException, IOException{
		
		  GmLotOnHandVO gmLotOnHandVO = new GmLotOnHandVO(); 
		  GmWSUtil gmWSUtil = new GmWSUtil();
		  GmLotHandReportBean gmLotHandReportBean = new GmLotHandReportBean();
		  ObjectMapper mapper = new ObjectMapper();
	      GmLotOnHandVO gmLotOnHandVo = mapper.readValue(input, GmLotOnHandVO.class);
	      validateToken(gmLotOnHandVo.getToken()); 
		  int expDays = gmLotOnHandVo.getExpDays();
		  HashMap hmInput = new HashMap();
		  HashMap hmAccessFilter  = fetchAccessFilter();
		  hmInput.put("ACCESSFILTER",GmCommonClass.parseNullHashMap(hmAccessFilter));
		  gmLotOnHandVO =  gmLotHandReportBean.fetchLotExpByDays(hmInput, expDays);  
		  return gmLotOnHandVO;
	}
	 

	/**
	 * This method used to show the expired lots on hand with part number and count based on expiry date
	 * 
	 * @param input
	 */
	 @POST
	  @Path("expiredlots")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public GmLotOnHandVO fetchExpiredLots(String input) throws AppError, JsonParseException,
    JsonMappingException, IOException{
			
			GmLotOnHandVO gmLotOnHandVO = new GmLotOnHandVO(); 
			GmWSUtil gmWSUtil = new GmWSUtil();
			GmLotHandReportBean gmLotHandReportBean = new GmLotHandReportBean();
			ObjectMapper mapper = new ObjectMapper();
			GmLotOnHandVO gmLotOnHandVo = mapper.readValue(input, GmLotOnHandVO.class);
			validateToken(gmLotOnHandVo.getToken()); 
		    HashMap hmInput = new HashMap();
		    HashMap hmAccessFilter  = fetchAccessFilter();
			hmInput.put("ACCESSFILTER",GmCommonClass.parseNullHashMap(hmAccessFilter));
			gmLotOnHandVO = gmLotHandReportBean.fetchExpiredLots(hmInput);
			return gmLotOnHandVO;
	}

	/**
	 * This method used to show the expiry graph based on count
	 * 
	 * @param input
	 */
	  @POST
	  @Path("expirygraph")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public GmLotOnHandVO fetchLotsExpGraph(String input) throws AppError, JsonParseException,
    JsonMappingException, IOException{
		GmLotOnHandVO gmLotOnHandVO = new GmLotOnHandVO(); 
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmLotHandReportBean gmLotHandReportBean = new GmLotHandReportBean();
		ObjectMapper mapper = new ObjectMapper();
		GmLotOnHandVO gmLotOnHandVo = mapper.readValue(input, GmLotOnHandVO.class);
		validateToken(gmLotOnHandVo.getToken()); 
		HashMap hmInput = new HashMap();
		HashMap hmAccessFilter  = fetchAccessFilter();
		hmInput.put("ACCESSFILTER",GmCommonClass.parseNullHashMap(hmAccessFilter));
		gmLotOnHandVO = gmLotHandReportBean.fetchLotsExpGraph(hmInput);
		return gmLotOnHandVO;
		
	}

	/**
	 * This method used to show the details of part number in popup for On hand qty and expired lots
	 * 
	 * @param input
	 */
	@POST
	 @Path("expiryLotDetails")
	 @Produces({MediaType.APPLICATION_JSON})
	 @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public GmLotOnHandVO fetchExpiryLotDetails(String input) throws AppError, JsonParseException,
    JsonMappingException, IOException{
		  GmLotOnHandVO gmLotOnHandVO = new GmLotOnHandVO();
		  GmWSUtil gmWSUtil = new GmWSUtil();
		  GmLotHandReportBean gmLotHandReportBean = new GmLotHandReportBean();
		  ObjectMapper mapper = new ObjectMapper();
		  HashMap hmInput = new HashMap();
		 GmLotOnHandVO gmLotOnHandVo = mapper.readValue(input, GmLotOnHandVO.class);//to read the input
	      validateToken(gmLotOnHandVo.getToken());
	       String strExpFlg = "N";
		   String strPartNum = GmCommonClass.parseNull(gmLotOnHandVo.getPartNum());
		   int expFlag = gmLotOnHandVo.getExpiryFlag();
		        if(strPartNum != ""){
			  strPartNum = strPartNum.replace(",", "','");  
		  }
		if(expFlag != 0){
			 strExpFlg = "Y"; 
		 }
		 
		HashMap hmAccessFilter  = fetchAccessFilter();
	      hmInput.put("PARTNOS", strPartNum);
		  hmInput.put("EXPIRYFLAG", strExpFlg);
		  hmInput.put("ACCESSFILTER",GmCommonClass.parseNullHashMap(hmAccessFilter));
		 
		  gmLotOnHandVO =  gmLotHandReportBean.fetchExpiryLotDetails(hmInput);
		 		  
		return gmLotOnHandVO;
	}


	/**
	 * This method used to show the details of part number in popup for 0-30 days, 31-60 days and 61-90 days
	 * 
	 * @param input
	 */
	@POST
	 @Path("expiryLotDetailsByDays")
	 @Produces({MediaType.APPLICATION_JSON})
	 @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public GmLotOnHandVO fetchExpLotDetailsByDays(String input) throws AppError, JsonParseException,
   JsonMappingException, IOException{
		 GmLotOnHandVO gmLotOnHandVO = new GmLotOnHandVO(); 
		  GmWSUtil gmWSUtil = new GmWSUtil();
		  GmLotHandReportBean gmLotHandReportBean = new GmLotHandReportBean();
		  ObjectMapper mapper = new ObjectMapper();
		  HashMap hmInput = new HashMap();
	      GmLotOnHandVO gmLotOnHandVo = (GmLotOnHandVO) mapper.readValue(input, GmLotOnHandVO.class);
	      validateToken(gmLotOnHandVo.getToken()); 
	      String strPartNum = GmCommonClass.parseNull(gmLotOnHandVo.getPartNum());
	      String strDays = "";
		  int expDays = gmLotOnHandVo.getExpDays();
		 
		 if(strPartNum != ""){
			  strPartNum = strPartNum.replace(",", "','");  
		  }
		 HashMap hmAccessFilter  = fetchAccessFilter();
		 hmInput.put("PARTNOS", strPartNum);
		 hmInput.put("ACCESSFILTER",GmCommonClass.parseNullHashMap(hmAccessFilter));
		 
		 gmLotOnHandVO =  gmLotHandReportBean.fetchExpLotDetailsByDays(hmInput,expDays);
		 		  
		return gmLotOnHandVO;
	}
	
	//Method call to get the Access filter
		 private HashMap fetchAccessFilter(){
			 GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();	
			 GmAccessFilter gmAccessFilter = new GmAccessFilter();
			 GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
			 
			    HashMap hmSalesParam =GmCommonClass.parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO,
			              (new GmSalesDashReqVO())));
			  // setting CompanyId and DivisionId to ResponseVo
			    gmSalesDashResVO.setCompanyid((String) hmSalesParam.get("COMP_FILTER"));
			    gmSalesDashResVO.setDivids((String) hmSalesParam.get("DIV_FILTER"));
			    hmSalesParam.put("RPT_REPID", gmUserVO.getUserid());
			    hmSalesParam.put("OVERRIDE_ASS_LVL", hmSalesParam.get("ACCES_LVL"));
			    hmSalesParam.put("ACCS_LVL", hmSalesParam.get("ACCES_LVL"));
			  
			   HashMap hmAccessFilter = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmSalesParam));
				
				return hmAccessFilter;
			}
}//end GmLotOnHandResource