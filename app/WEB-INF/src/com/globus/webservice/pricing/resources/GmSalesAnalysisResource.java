/**
 * FileName : GmPriceRequestResource.java
 * 
 * @Description : Resource File for Impact Analysis (PRT-R203)
 * @author matt balraj
 * @Copyright : Globus Medical Inc
 */
package com.globus.webservice.pricing.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.sales.pricing.beans.GmImpactAnalysisBean;
import com.globus.valueobject.pricing.GmPricingSalesHeaderVO;
import com.globus.valueobject.pricing.GmSalesAnalysisVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.valueobject.pricing.GmPricingListVO;

@Path("salesAnalysis")
public class GmSalesAnalysisResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * @Author : Karthik
   * @Desc This method is fetch Sales analysis data
   */
  @POST
  @Path("fetchSalesAnalysis")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmPricingListVO FetchSalesAnalysis(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {
    log.debug("fetchSalesAnalysis strInput >>>>>" + strInput);
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmReturn = new HashMap();
    ArrayList aImpactAnlyResult = new ArrayList();
    GmSalesAnalysisVO salesAnalysisVO = new GmSalesAnalysisVO();
    GmPricingListVO gmPricingListVO = new GmPricingListVO();
    
    ObjectMapper mapper = new ObjectMapper();
    GmSalesAnalysisVO gmSalesAnalysisVO = mapper.readValue(strInput, GmSalesAnalysisVO.class);
    validateToken(gmSalesAnalysisVO.getToken());
    GmImpactAnalysisBean gmImpactAnalysisbean = new GmImpactAnalysisBean(gmSalesAnalysisVO);
    String strRequestId = gmSalesAnalysisVO.getRefid();
    String strUserId = gmSalesAnalysisVO.getUserid();
    
    HashMap hmParams = new HashMap();
    hmParams.put("REQUESTID", strRequestId);
    hmParams.put("VO", salesAnalysisVO);
    hmParams.put("USERID", strUserId);
    log.debug("calling fetchSalesAnalysis (RequestId) " + strRequestId);
    hmReturn = gmImpactAnalysisbean.fetchSalesAnalysisData(hmParams);
    ArrayList salesResult = (ArrayList) hmReturn.get("SALESDATA");
    ArrayList headerResult = (ArrayList) hmReturn.get("HEADERDATA");
    
    List<GmSalesAnalysisVO> lstgmSalesAnalysisVO = new ArrayList<GmSalesAnalysisVO>();
    GmSalesAnalysisVO gmSalesDataVO = new GmSalesAnalysisVO();
       lstgmSalesAnalysisVO =
            gmWSUtil.getVOListFromHashMapList(salesResult, gmSalesDataVO,
            		gmSalesDataVO.getMappingProperties());
    GmPricingSalesHeaderVO gmPricingSalesHeaderVO = new GmPricingSalesHeaderVO();
    List<GmPricingSalesHeaderVO> gmPricingSalesHeaderListVO = new ArrayList<GmPricingSalesHeaderVO>();
    gmPricingSalesHeaderListVO =  gmWSUtil.getVOListFromHashMapList(headerResult, gmPricingSalesHeaderVO,
    		gmPricingSalesHeaderVO.getPricingHeaderProperties());
    
    gmPricingListVO.setGmsalesAnalysisVO(lstgmSalesAnalysisVO);
    gmPricingListVO.setGmPricingSalesHeaderVO(gmPricingSalesHeaderListVO);
    return gmPricingListVO;
  }
  

  /**
   * @Author : Karthik
   * @Desc This method is fetch Sales segment data
   */
  @POST
  @Path("fetchSalesSegmentdetails")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmPricingListVO FetchSalesSegmentdata(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {
    log.debug("fetchSalesSegmentdetails strInput >>>>>" + strInput);
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmReturn = new HashMap();
    ArrayList aImpactAnlyResult = new ArrayList();
    GmSalesAnalysisVO salesAnalysisVO = new GmSalesAnalysisVO();
    GmPricingListVO gmPricingListVO = new GmPricingListVO();

    List<GmSalesAnalysisVO> lstgmSalesAnalysisVO = new ArrayList<GmSalesAnalysisVO>();
    ObjectMapper mapper = new ObjectMapper();
    GmSalesAnalysisVO gmSalesAnalysisVO = mapper.readValue(strInput, GmSalesAnalysisVO.class);
    validateToken(gmSalesAnalysisVO.getToken());
    GmImpactAnalysisBean gmImpactAnalysisbean = new GmImpactAnalysisBean(gmSalesAnalysisVO);
    String strRequestId = gmSalesAnalysisVO.getRefid();
    String strUserId = gmSalesAnalysisVO.getUserid();
    
    HashMap hmParams = new HashMap();
    hmParams.put("REQUESTID", strRequestId);
    hmParams.put("VO", salesAnalysisVO);
    hmParams.put("USERID", strUserId);
    log.debug("calling fetchSalesSegmentdetails (RequestId) " + strRequestId);
    hmReturn = gmImpactAnalysisbean.fetchSalesSegmentData(hmParams);
    ArrayList segmentResult = (ArrayList) hmReturn.get("SEGMENTDATA");
    ArrayList headerResult = (ArrayList) hmReturn.get("HEADERDATA");
     lstgmSalesAnalysisVO =
            gmWSUtil.getVOListFromHashMapList(segmentResult, gmSalesAnalysisVO,
            		gmSalesAnalysisVO.getMappingProperties());
       
    GmPricingSalesHeaderVO gmPricingSalesHeaderVO = new GmPricingSalesHeaderVO();
    List<GmPricingSalesHeaderVO> gmPricingSalesHeaderListVO = new ArrayList<GmPricingSalesHeaderVO>();
    gmPricingSalesHeaderListVO =  gmWSUtil.getVOListFromHashMapList(headerResult, gmPricingSalesHeaderVO,
    		gmPricingSalesHeaderVO.getPricingHeaderProperties());
    
    gmPricingListVO.setGmsalesAnalysisVO(lstgmSalesAnalysisVO);
    gmPricingListVO.setGmPricingSalesHeaderVO(gmPricingSalesHeaderListVO);
    return gmPricingListVO;
  }

  
}
