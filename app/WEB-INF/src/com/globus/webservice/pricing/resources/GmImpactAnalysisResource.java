/**
 * FileName : GmPriceRequestResource.java
 * 
 * @Description : Resource File for Impact Analysis (PRT-R203)
 * @author matt balraj
 * @Copyright : Globus Medical Inc
 */
package com.globus.webservice.pricing.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.sales.pricing.beans.GmImpactAnalysisBean;
import com.globus.valueobject.pricing.GmImpactAnalysisVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.valueobject.pricing.GmPricingListVO;

@Path("impactAnalysis")
public class GmImpactAnalysisResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * @Author : Matt B
   * @Desc This method is fetch Impact analysis data from T7520 and T7521 tables)
   */
  @POST
  @Path("fetchImpactAnalysis")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmPricingListVO FetchImpactAnalysis(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {
    log.debug("ImpactAnalysis strInput >>>>>" + strInput);
    GmWSUtil gmWSUtil = new GmWSUtil();
    ArrayList aImpactAnlyResult = new ArrayList();
    GmImpactAnalysisVO impactAnalysisVO = new GmImpactAnalysisVO();
    GmPricingListVO gmPricingListVO = new GmPricingListVO();

    List<GmImpactAnalysisVO> lstgmImpactAnalysisVO = new ArrayList<GmImpactAnalysisVO>();
    ObjectMapper mapper = new ObjectMapper();
    GmImpactAnalysisVO gmImpactAnalysisVO = mapper.readValue(strInput, GmImpactAnalysisVO.class);
    validateToken(gmImpactAnalysisVO.getToken());
    GmImpactAnalysisBean gmImpactAnalysisbean = new GmImpactAnalysisBean(gmImpactAnalysisVO);
    String strRequestId = gmImpactAnalysisVO.getRefid();
    String strUserId = gmImpactAnalysisVO.getUserid();
    
    HashMap hmParams = new HashMap();
    hmParams.put("REQUESTID", strRequestId);
    hmParams.put("VO", impactAnalysisVO);
    hmParams.put("USERID", strUserId);
    log.debug("calling fetchImpactAnalysisData (RequestId) " + strRequestId);
    lstgmImpactAnalysisVO = gmImpactAnalysisbean.fetchImpactAnalysisData(hmParams);
    gmPricingListVO.setGmImpactAnalysisVO(lstgmImpactAnalysisVO);
    return gmPricingListVO;
  }

  /**
   * @Author : Jreddy
   * @Desc This method is fetch Impact analysis data from T7520 and T7521 tables)
   */
  @POST
  @Path("sendImpactMail")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmPricingListVO sendImpactMail(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {
    log.debug("ImpactAnalysis strInput >>>>>" + strInput);
    GmWSUtil gmWSUtil = new GmWSUtil();
    ArrayList aImpactAnlyResult = new ArrayList();
    GmImpactAnalysisVO impactAnalysisVO = new GmImpactAnalysisVO();
    GmPricingListVO gmPricingListVO = new GmPricingListVO();

    List<GmImpactAnalysisVO> lstgmImpactAnalysisVO = new ArrayList<GmImpactAnalysisVO>();
    ObjectMapper mapper = new ObjectMapper();
    GmImpactAnalysisVO gmImpactAnalysisVO = mapper.readValue(strInput, GmImpactAnalysisVO.class);
    validateToken(gmImpactAnalysisVO.getToken());
    GmImpactAnalysisBean gmImpactAnalysisbean = new GmImpactAnalysisBean(gmImpactAnalysisVO);
    String strRequestId = gmImpactAnalysisVO.getRefid();
    HashMap<String, String> hmParams = new HashMap<String, String>();
    hmParams.put("REQUESTID", strRequestId);
    hmParams.put("ACCOUNTNM", gmImpactAnalysisVO.getAccountname());
    hmParams.put("CURR", gmImpactAnalysisVO.getCurrency());
    hmParams.put("PRE12MNTHSALE", gmImpactAnalysisVO.getPre12mnthsale());
    hmParams.put("PROJPROPRICE", gmImpactAnalysisVO.getPropprice());
    hmParams.put("PROJIMPACT", gmImpactAnalysisVO.getProjimpact());
    hmParams.put("PROJIMPATPRPPRC", gmImpactAnalysisVO.getProjimpatprpprc());
    hmParams.put("ADNAME", gmImpactAnalysisVO.getAdname());
    hmParams.put("GENDATE", gmImpactAnalysisVO.getGendate());
    hmParams.put("FROM", gmImpactAnalysisVO.getFrom());
    hmParams.put("CC", gmImpactAnalysisVO.getCc());
    hmParams.put("CHECKBTN", gmImpactAnalysisVO.getCheckbtn());
    hmParams.put("USERID", gmImpactAnalysisVO.getUserid());
    ArrayList alResult = gmImpactAnalysisbean.fetchImpactEmailData(hmParams);
    gmPricingListVO.setGmImpactAnalysisVO(lstgmImpactAnalysisVO);
    return gmPricingListVO;
  }
}
