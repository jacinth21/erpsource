/**
 * @FileName : GmPricingApprovalResource.java
 * @Description :
 * @Resource File Author : Arun Date :
 * @Copyright : Globus Medical Inc
 */
package com.globus.webservice.pricing.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.simple.JSONObject;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.sales.pricing.beans.GmImpactAnalysisBean;
import com.globus.sales.pricing.beans.GmPriceApprovalRequestBean;
import com.globus.sales.pricing.beans.GmPriceApprovalTransBean;
import com.globus.sales.pricing.beans.GmPriceRequestRptBean;
import com.globus.sales.pricing.beans.GmPriceRequestTransBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.pricing.GmAccessControlVO;
import com.globus.valueobject.pricing.GmAccountPriceRequestVO;
import com.globus.valueobject.pricing.GmGroupVO;
import com.globus.valueobject.pricing.GmPricingAccessFilterVO;
import com.globus.valueobject.pricing.GmPricingApprovalHeaderVO;
import com.globus.valueobject.pricing.GmPricingReqDetailsVO;
import com.globus.valueobject.pricing.resources.GmPriceApprovalDetailsVO;
import com.globus.webservice.common.resources.GmResource;

@Path("pricingApproval")
public class GmPricingApprovalResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * 
   * This method is used to get the Accounts based on Type (Account / Group Account)
   */


  /**
   * 
   * This method is used to save the pricing details
   * 
   */
  @POST
  @Path("fetchPriceApprovalDetails")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmPricingReqDetailsVO priceApprovalDetails(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException, Exception {
    log.debug("priceApprovalDetails strInput >>>>>" + strInput);

    GmPricingAccessFilterVO gmPricingAccessFilterVO = new GmPricingAccessFilterVO();
    GmCommonBean gmCommonBean = new GmCommonBean();
    ObjectMapper mapper = new ObjectMapper();
    HashMap hmParam = new HashMap();
    GmPricingReqDetailsVO gmPricingReqDetailsVO =
        mapper.readValue(strInput, GmPricingReqDetailsVO.class);
    validateToken(gmPricingReqDetailsVO.getToken());
    String strPriceRequestId = GmCommonClass.parseNull(gmPricingReqDetailsVO.getPricerequestid());
    strPriceRequestId =
        strPriceRequestId.contains("PR-") ? strPriceRequestId : "PR-" + strPriceRequestId;
    String strUserId = gmPricingReqDetailsVO.getUserid();
    GmPriceRequestRptBean gmPriceRequestRptBean = new GmPriceRequestRptBean(gmPricingReqDetailsVO);
    GmPriceApprovalRequestBean gmPriceApprovalRequestBean =
    new GmPriceApprovalRequestBean(gmPricingReqDetailsVO);

    String strAccessFilter = gmPricingAccessFilterVO.getFiletrcondition();
    HashMap hmSalesFilters = gmCommonBean.getSalesFilterLists(strAccessFilter);
    ArrayList alRepList = new ArrayList();
    alRepList = GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters.get("REP"));
     String strFilterlRepIds = "";
     String strRepIds = "";
     HashMap hmDetails = new HashMap();
     if (alRepList != null && alRepList.size() > 0) {
     for (int i = 0; i < alRepList.size(); i++) {
     hmDetails = GmCommonClass.parseNullHashMap((HashMap) alRepList.get(i));
     strRepIds = GmCommonClass.parseNull((String) hmDetails.get("ID"));
     strFilterlRepIds += strRepIds + ",";
     }
     }
    HashMap hmParams = new HashMap();
    hmParams.put("PRICEREQUESTID", strPriceRequestId);
    hmParams.put("PRICINGREQID", strPriceRequestId);
     hmParams.put("FILTERREPS", strFilterlRepIds);
    hmParams.put("USERID", strUserId);
    log.debug("hmParams >>>" + hmParams);
    HashMap hsReturn = gmPriceRequestRptBean.fetchPriceRequestDetails(hmParams);
    ArrayList priceReqResult = (ArrayList) hsReturn.get("PRICEREQDETAILS");
    HashMap hmReturn = gmPriceApprovalRequestBean.fetchPriceRequestDetails(hmParams);
    ArrayList systemResult = (ArrayList) hmReturn.get("SYSTEMDETAILS");
     ArrayList groupResult = (ArrayList) hmReturn.get("PRICEREQDETAILS");
//    ArrayList priceReqResult = (ArrayList) hmReturn.get("PRICEREQDETAILS");
    HashMap alPriceHeaderData = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("HEADERDATA"));
    ArrayList alResult = new ArrayList();
    alResult.add(alPriceHeaderData);
    com.globus.valueobject.pricing.GmSystemVO gmSystemVO =
        new com.globus.valueobject.pricing.GmSystemVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    List<com.globus.valueobject.pricing.GmSystemVO> gmSystemVOList =
        new ArrayList<com.globus.valueobject.pricing.GmSystemVO>();
    gmSystemVOList =
        gmWSUtil.getVOListFromHashMapListWithCaseProperty(systemResult, gmSystemVO,
            gmSystemVO.getSystemProperties());

    GmPricingReqDetailsVO gmPricingReqDetailVO = new GmPricingReqDetailsVO();
    GmGroupVO gmFetchGroupsVO = new GmGroupVO();
    List<GmGroupVO> gmGroupVOList = new ArrayList<GmGroupVO>();
   
    gmGroupVOList =
        gmWSUtil.getVOListFromHashMapList(groupResult, gmFetchGroupsVO,
            gmFetchGroupsVO.getGroupProperties());

   
    GmPricingApprovalHeaderVO gmPricingApprovalHeaderVO = new GmPricingApprovalHeaderVO();
    List<GmPricingApprovalHeaderVO> gmPricingApprovalHeaderListVO = new ArrayList<GmPricingApprovalHeaderVO>();
    gmPricingApprovalHeaderListVO =  gmWSUtil.getVOListFromHashMapList(alResult, gmPricingApprovalHeaderVO,
    		gmPricingApprovalHeaderVO.getPricingApprovalRptProperties());
    log.debug("gmPricingApprovalHeaderListVO-->"+alResult);
    
    gmPricingReqDetailVO.setListGmSystemVO(gmSystemVOList);
    gmPricingReqDetailVO.setListGmGroupVO(gmGroupVOList);
    gmPricingReqDetailVO.setListgmPricingApprovalHeaderVO(gmPricingApprovalHeaderListVO);

    GmAccountPriceRequestVO gmAccountPriceRequestVO = new GmAccountPriceRequestVO();
    if (priceReqResult != null && priceReqResult.size() > 0) {
      HashMap hmapResult = (HashMap) priceReqResult.get(0);
      gmAccountPriceRequestVO.setPartyid((String) hmapResult.get("ACCOUNTID"));
      gmAccountPriceRequestVO.setPartyname((String) hmapResult.get("ACCOUNTNAME"));
      gmAccountPriceRequestVO.setPricerequestid((String) hmapResult.get("PRICEREQUESTID"));
      gmAccountPriceRequestVO.setProjected12MonthsSale((String) hmapResult.get("PROJ12MNTHSCALE"));
      gmSystemVO.setChangetype((String) hmapResult.get("CHANGETYPE"));
      gmSystemVO.setChangevalue((String) hmapResult.get("CHANGEVALUE"));
      gmAccountPriceRequestVO.setTypeid((String) hmapResult.get("TYPEID"));
      gmAccountPriceRequestVO.setQuestioncnt((String) hmapResult.get("QUESTIONCNT"));
      gmAccountPriceRequestVO.setRequestStatus((String) hmapResult.get("STATUSNAME"));
      gmAccountPriceRequestVO.setStatusid((String) hmapResult.get("STATUSID"));
    }
    gmPricingReqDetailVO.setGmAccountPriceRequestVO(gmAccountPriceRequestVO);

    return gmPricingReqDetailVO;
  }

  /**
   * setAccessPermissions - This method is retrieve access permissions
   * 
   * @param String type, String strListId, String strRefGrp
   * @return object
   * @exception AppError
   * @author Matt Balraj
   */
  @POST
  @Path("fetchAccessPermissions")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmAccessControlVO setAccessPermissions(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException, Exception {

    log.debug("setAccessPermissions strInput >>>>>" + strInput);

    GmPricingAccessFilterVO gmPricingAccessFilterVO = new GmPricingAccessFilterVO();
    GmCommonBean gmCommonBean = new GmCommonBean();
    ObjectMapper mapper = new ObjectMapper();

    GmPricingReqDetailsVO gmPricingReqDetailsVO =
        mapper.readValue(strInput, GmPricingReqDetailsVO.class);
    validateToken(gmPricingReqDetailsVO.getToken());
    String strPartyId = gmPricingReqDetailsVO.getPartyid();

    GmAccessControlVO gmAccessControlVO = new GmAccessControlVO();

    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(gmPricingReqDetailsVO);

    HashMap hmApproveAccess = new HashMap();
    HashMap hmDenyAccess = new HashMap();
    HashMap hmDeleteAccess = new HashMap();
    HashMap hmStatusUPDAccess = new HashMap();
    HashMap hmGenPriceFile = new HashMap();
    HashMap hmPrPcAccess = new HashMap();
    hmDenyAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PR_DENIED_ACCESS"));
    gmAccessControlVO.setPr_denied_access(GmCommonClass.parseNull((String) hmDenyAccess
        .get("UPDFL")));

    // Get the user access to Approve
    hmApproveAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PR_APPRVL_ACCESS"));
    gmAccessControlVO.setPr_apprvl_access(GmCommonClass.parseNull((String) hmApproveAccess
        .get("UPDFL")));


    // Get the user access to Delete the System
    hmDeleteAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PR_DELETE_ACCESS"));
    gmAccessControlVO.setPr_delete_access(GmCommonClass.parseNull((String) hmDeleteAccess
        .get("UPDFL")));


    // Get the user access stage dropdown
    hmStatusUPDAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PR_STATUS_UPD_ACCESS"));
    gmAccessControlVO.setPr_status_upd_access(GmCommonClass.parseNull((String) hmStatusUPDAccess
        .get("UPDFL")));


    // Get the Pricing Committee user access to pending pc approval
    hmPrPcAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PR_PC_ACCESS"));
    log.debug("hmPrPcAccess"+hmPrPcAccess);
    gmAccessControlVO
        .setPr_pc_access(GmCommonClass.parseNull((String) hmPrPcAccess.get("UPDFL")));

    // Access for Generating price file - Matt
    hmGenPriceFile =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PR_GEN_PRICEFILE"));
    gmAccessControlVO.setPr_gen_pricefile(GmCommonClass.parseNull((String) hmGenPriceFile
        .get("UPDFL")));

    return gmAccessControlVO;
  }
  
  /**
   * 
   * This method is used to save the pricing details
   * 
   * @throws Exception
   * 
   */
  @POST
  @Path("updatePriceApprovalDetails")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmPriceApprovalDetailsVO updatePriceApprovalDetails(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException, Exception {
    log.debug("saveOrUpdatePricingDetails strInput >>>>>" + strInput);
    ObjectMapper mapper = new ObjectMapper();
    HashMap<String, String> hmParam = new HashMap<String, String>();
    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
   
    GmPriceApprovalDetailsVO gmPriceApprovalDetailsVO =
        mapper.readValue(strInput, GmPriceApprovalDetailsVO.class);
    validateToken(gmPriceApprovalDetailsVO.getToken());
    GmPriceApprovalTransBean gmPriceApprovalTransBean =
            new GmPriceApprovalTransBean(gmPriceApprovalDetailsVO);
    GmImpactAnalysisBean gmImpactAnalysisBean = new GmImpactAnalysisBean(gmPriceApprovalDetailsVO);
    
    String strUserId = gmPriceApprovalDetailsVO.getUserid();
    String strPriceRequestId = gmPriceApprovalDetailsVO.getPricerequestid();
    String strInputStr = gmPriceApprovalDetailsVO.getInputstr();
    String strImplementString = gmPriceApprovalDetailsVO.getImplementstring();
    hmParam.put("PRICEREQUESTID", strPriceRequestId);
    hmParam.put("INPUTSTRING", strInputStr);
    hmParam.put("USERID", strUserId);
    hmParam.put("IMPLEMENTSTRING", strImplementString);
    JSONObject obj = new JSONObject();
    obj.put("cmpid",gmPriceApprovalDetailsVO.getCmpid());
    obj.put("plantid",gmPriceApprovalDetailsVO.getPlantid());
    obj.put("token",gmPriceApprovalDetailsVO.getToken());
    obj.put("partyid",gmPriceApprovalDetailsVO.getPartyid());
    obj.put("cmplangid",gmPriceApprovalDetailsVO.getCmplangid());
    hmParam.put("companyInfo", obj.toString());
    log.debug("strcompanyInfo ---> "+hmParam);
//    gmPriceApprovalTransBean.updatePriceRequestDetails(hmParam);
    String strPriceReqId = gmPriceApprovalTransBean.updatePriceRequestDetails(hmParam);
    HashMap<String, String> hmAnlysis = new HashMap<String, String>();
    hmAnlysis.put("PRICEREQUESTID", strPriceReqId);
    hmAnlysis.put("COMPANYINFO", GmCommonClass.parseNull((String) hmParam.get("companyInfo")));
    hmAnlysis.put("USERID", GmCommonClass.parseNull((String) hmParam.get("USERID")));
    /* IMPACT ANLAYSIS - Checking if price is updated creating impact analysis(PRT-R203) */
    gmImpactAnalysisBean.checkPriceUpdateFlag(hmAnlysis);
    return null;
  }

}
