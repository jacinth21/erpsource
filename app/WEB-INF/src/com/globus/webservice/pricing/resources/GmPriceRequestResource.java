/**
 * FileName : GmPriceRequestResource.java Description : Resource File Author : Jreddy Date :
 * Copyright : Globus Medical Inc
 */
package com.globus.webservice.pricing.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.annotation.JacksonFeatures;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonCancelBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.pricing.beans.GmPriceRequestRptBean;
import com.globus.sales.pricing.beans.GmPriceRequestTransBean;
import com.globus.sales.pricing.beans.GmGroupPartPricingBean;
import com.globus.sales.pricing.beans.GmPricingRequestBean;
import com.globus.valueobject.common.GmCodeLookupAccessVO;
import com.globus.valueobject.common.GmCommonCancelVO;
import com.globus.valueobject.common.GmRulesVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.pricing.GmAccountDetailsListVO;
import com.globus.valueobject.pricing.GmAccountPriceRequestVO;
import com.globus.valueobject.pricing.GmGroupPartMappingVO;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.pricing.GmGroupPartPricingVO;
import com.globus.valueobject.pricing.GmGroupVO;
import com.globus.valueobject.pricing.GmHeldOrderPriceRequestVO;
import com.globus.valueobject.pricing.GmPriceAccountTypeVO;
import com.globus.valueobject.pricing.GmPricingAccessFilterVO;
import com.globus.valueobject.pricing.GmPricingReportVO;
import com.globus.valueobject.pricing.GmPricingReqDetailsVO;
import com.globus.valueobject.pricing.GmSystemVO;
import com.globus.valueobject.pricing.GmThresholdQuestionVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.valueobject.pricing.GmPricingListVO;

@Path("pricingRequest")
public class GmPriceRequestResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * 
   * This method is used to get the Accounts based on Type (Account / Group Account)
   */
  @POST
  @Path("accountDrpDownData")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmPricingListVO fetchAllGroupAccounts(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {
    log.debug("accountDrpDownData strInput >>>>>" + strInput);
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmPriceAccountTypeVO gmPriceAccountTypeVO = new GmPriceAccountTypeVO();
    GmPricingListVO gmPricingListVO = new GmPricingListVO();
    List<GmPriceAccountTypeVO> gmAccountDrpDownVoList = new ArrayList<GmPriceAccountTypeVO>();
    GmPricingAccessFilterVO gmPricingAccessFilterVO = new GmPricingAccessFilterVO();
    ObjectMapper mapper = new ObjectMapper();
    GmPriceAccountTypeVO gmPriceAccountType =
        mapper.readValue(strInput, GmPriceAccountTypeVO.class);
    validateToken(gmPriceAccountType.getToken());
    GmPriceRequestRptBean gmPriceRequestRptBean = new GmPriceRequestRptBean(gmPriceAccountType);
    String typeId = gmPriceAccountType.getTypeid();
    String strSearch = gmPriceAccountType.getSearchtext();
    String srtCompId = gmPriceAccountType.getCmpid();
    String strUserId = gmPriceAccountType.getUserid();
    String strGPBAccId = gmPriceAccountType.getAccid();
    ArrayList alReturn = new ArrayList();
    if (typeId.equals("903107")) {
      gmPricingAccessFilterVO = fetchAccessFilters(strInput);
      alReturn =
          gmPriceRequestRptBean.loadAccountList(gmPricingAccessFilterVO.getFiletrcondition(),
              strSearch, srtCompId,strGPBAccId);
      gmAccountDrpDownVoList =
          gmWSUtil.getVOListFromHashMapList(alReturn, gmPriceAccountType,
              gmPriceAccountType.getMappingProperties());
    } else {
      alReturn = gmPriceRequestRptBean.reportPartyNameList(strUserId , "7003", strSearch,strGPBAccId);
      for (int i = 0; i < alReturn.size(); i++) {
        GmPriceAccountTypeVO gmAccountDrpDownVo = new GmPriceAccountTypeVO();
        HashMap hmReturn = (HashMap) alReturn.get(i);
        log.debug("hmReturn > " + hmReturn);
        gmAccountDrpDownVo.setPartyid((String) hmReturn.get("ID"));
        gmAccountDrpDownVo.setAccname((String) hmReturn.get("NAME"));
        gmAccountDrpDownVoList.add(gmAccountDrpDownVo);
      }
    }
    gmPricingListVO.setGmPriceAccountTypeVO(gmAccountDrpDownVoList);
    return gmPricingListVO;
  }


  /**
   * 
   * This method is used to get the Account Information once click on I icon
   * 
   */
  @POST
  @Path("accountDetailsList")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmPricingListVO fetchAccountInfo(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {
    log.debug("accountDetailsList strInput >>>>>" + strInput);
    List<GmAccountDetailsListVO> gmAccountDetailsListVO = new ArrayList<GmAccountDetailsListVO>();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmPricingListVO gmPricingListVO= new GmPricingListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmAccountDetailsListVO gmAccountDetailsVO =
        mapper.readValue(strInput, GmAccountDetailsListVO.class);
    validateToken(gmAccountDetailsVO.getToken());
    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean(gmAccountDetailsVO);
    String straccId = gmAccountDetailsVO.getAccid();
    String typeId = gmAccountDetailsVO.getTypeid();
    String strPartyId = gmAccountDetailsVO.getAccid(); //passing the GPB accountid as partyid
    if (typeId.equals("903107")) {
      HashMap hmResult = gmPricingRequestBean.loadAccountDetails(straccId, "", "");
      ArrayList alResult = new ArrayList();
      alResult.add(hmResult);
      GmAccountDetailsListVO gmAccountDetailVO = new GmAccountDetailsListVO();
      gmAccountDetailsListVO =
          gmWSUtil.getVOListFromHashMapList(alResult, gmAccountDetailsVO,
              gmAccountDetailsVO.getMappingProperties());
    }
    if (typeId.equals("903108")) {
    	 ArrayList alResult = gmPricingRequestBean.loadGPBAccountDetails(strPartyId);
        GmAccountDetailsListVO gmAccountDetailVO = new GmAccountDetailsListVO();
        gmAccountDetailsListVO =
            gmWSUtil.getVOListFromHashMapList(alResult, gmAccountDetailsVO,
                gmAccountDetailsVO.getMappingProperties());
    }
    gmPricingListVO.setGmAccountDetailsListVO(gmAccountDetailsListVO);
    return gmPricingListVO;
  }

  /**
   * 
   * This method is used to get the Account/Party Name
   * 
   */
  @POST
  @Path("accountName")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  @JacksonFeatures(serializationEnable = SerializationFeature.WRAP_ROOT_VALUE,
  deserializationEnable = DeserializationFeature.UNWRAP_ROOT_VALUE)
  public GmPriceAccountTypeVO fetchAccountName(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {
    log.debug("accountName strInput >>>>>" + strInput);
    GmWSUtil gmWSUtil = new GmWSUtil();
    String strAccName = "";
    //GmPricingListVO gmPricingListVO = new GmPricingListVO();
    List<GmPriceAccountTypeVO> gmAccountDrpDownVoList = new ArrayList<GmPriceAccountTypeVO>();
    GmPricingAccessFilterVO gmPricingAccessFilterVO = new GmPricingAccessFilterVO();
    ObjectMapper mapper = new ObjectMapper();
    GmPriceAccountTypeVO gmPriceAccountType =
        mapper.readValue(strInput, GmPriceAccountTypeVO.class);
    HashMap<String, String> hmParams = new HashMap<String, String>();
    GmPriceRequestRptBean gmPriceRequestRptBean = new GmPriceRequestRptBean(gmPriceAccountType);
    String straccId = gmPriceAccountType.getAccid();
    String strtypeId = gmPriceAccountType.getTypeid();
    String strrepId = gmPriceAccountType.getRepid();
    hmParams.put("ACCID", straccId);
    hmParams.put("TYPEID", strtypeId);
    hmParams.put("REPID", strrepId);
    gmPricingAccessFilterVO = fetchAccessFilters(strInput);
    validateToken(gmPriceAccountType.getToken());
    strAccName =
        gmPriceRequestRptBean.fetchAccountName(hmParams,
            gmPricingAccessFilterVO.getFiletrcondition());
    gmPriceAccountType.setAccname(strAccName);
    gmAccountDrpDownVoList.add(gmPriceAccountType);
   // gmPricingListVO.setGmPriceAccountTypeVO(gmAccountDrpDownVoList);
    return gmPriceAccountType;
  }


  /**
   * 
   * This method is used to get the Threshold Questions and consultant drop down Values
   * 
   */
  @POST
  @Path("thresholdQuestions")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmPricingListVO fetchThresholdQuestions(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmPricingListVO gmPricingListVO = new GmPricingListVO();
    List<GmThresholdQuestionVO> gmThrQueVoList = new ArrayList<GmThresholdQuestionVO>();

    ObjectMapper mapper = new ObjectMapper();
    GmThresholdQuestionVO gmThresholdQuestionVO =
        mapper.readValue(strInput, GmThresholdQuestionVO.class);
    validateToken(gmThresholdQuestionVO.getToken());
    String strRerqId = gmThresholdQuestionVO.getRequestid();
    strRerqId = strRerqId.toUpperCase().contains("PR-") ? strRerqId : "PR-" + strRerqId;
    GmPriceRequestRptBean gmPriceRequestRptBean = new GmPriceRequestRptBean(gmThresholdQuestionVO);
    ArrayList thQuestionDetails = gmPriceRequestRptBean.fetchThresholdQuestions(strRerqId);
    gmThrQueVoList =
        gmWSUtil.getVOListFromHashMapList(thQuestionDetails, gmThresholdQuestionVO,
            gmThresholdQuestionVO.getMappingProperties());
    
    gmPricingListVO.setGmThresholdQuestionVO(gmThrQueVoList);
    return gmPricingListVO;
  }

  /**
   * 
   * This method is used to get the Group Part Details click on P icon
   * 
   */
  @POST
  @Path("groupPartMapDetailsList")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmPricingListVO fetchPartsInfo(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {
    log.debug("groupPartMapDetailsList strInput >>>>>" + strInput);
    List<GmGroupPartMappingVO> gmGroupPartMappingVO = new ArrayList<GmGroupPartMappingVO>();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmPricingListVO gmPricingListVO = new GmPricingListVO();

    ObjectMapper mapper = new ObjectMapper();
    GmGroupPartMappingVO gmGroupPartMapVO = mapper.readValue(strInput, GmGroupPartMappingVO.class);
    String strGrpId = gmGroupPartMapVO.getGroupid();
    String strGrpName = gmGroupPartMapVO.getGroupname();
    validateToken(gmGroupPartMapVO.getToken());
    GmPriceRequestRptBean gmPriceRequestRptBean = new GmPriceRequestRptBean(gmGroupPartMapVO);
    ArrayList alResult = gmPriceRequestRptBean.fetchGroupPartMappingDetails(strGrpId);
    gmGroupPartMappingVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmGroupPartMapVO,
            gmGroupPartMapVO.getMappingProperties());
    gmPricingListVO.setGmGroupPartMappingVO(gmGroupPartMappingVO);
    return gmPricingListVO;
  }


  /**
   * 
   * This method is used to get the systems based on user enter characters
   * 
   */
  @POST
  @Path("pricingSystems")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmPricingListVO fetchAllSystems(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {
    log.debug("pricingSystems strInput >>>>>" + strInput);
    List<GmSystemVO> gmSystemVOList = new ArrayList<GmSystemVO>();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmPricingListVO gmPricingListVO = new GmPricingListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmSystemVO gmFetchSystemsVO = mapper.readValue(strInput, GmSystemVO.class);
    validateToken(gmFetchSystemsVO.getToken());
    String strSearch = gmFetchSystemsVO.getSearchtext();
    GmPriceRequestRptBean gmPriceRequestRptBean = new GmPriceRequestRptBean(gmFetchSystemsVO);
    ArrayList systemDetails = gmPriceRequestRptBean.loadSystemsList(strSearch);
    gmSystemVOList =
        gmWSUtil.getVOListFromHashMapListWithCaseProperty(systemDetails, gmFetchSystemsVO,
            gmFetchSystemsVO.getPriceSysProperties());
    
    gmPricingListVO.setGmSystemVO(gmSystemVOList);
    return gmPricingListVO;
  }

  /**
   * 
   * This method is used to get the selected system groups details
   * 
   */
  @POST
  @Path("pricingSystemGroups")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmPricingListVO fetchSystemGroupsDetails(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    log.debug("pricingSystemGroups strInput >>>>>" + strInput);
    List<GmGroupVO> gmGroupVOList = new ArrayList<GmGroupVO>();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmPricingListVO gmPricingListVO = new GmPricingListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmPricingReqDetailsVO gmPricingReqDetailsVO =
        mapper.readValue(strInput, GmPricingReqDetailsVO.class);
    validateToken(gmPricingReqDetailsVO.getToken());
    String strCompanyId =
        GmCommonClass.parseNull(gmPricingReqDetailsVO.getGmAccountPriceRequestVO().getCmpid());
    String strAccPartyId =
        GmCommonClass.parseNull(gmPricingReqDetailsVO.getGmAccountPriceRequestVO().getPartyid());
    String strSelectedSystems =
        GmCommonClass.parseNull(gmPricingReqDetailsVO.getGmAccountPriceRequestVO()
            .getSelectedSystems());
    String strTypeID =
        GmCommonClass.parseNull(gmPricingReqDetailsVO.getGmAccountPriceRequestVO().getTypeid());
    GmPriceRequestRptBean gmPriceRequestRptBean = new GmPriceRequestRptBean(gmPricingReqDetailsVO.getGmAccountPriceRequestVO());
    GmGroupVO gmFetchGroupsVO = new GmGroupVO();

    HashMap hmParams = new HashMap();
    hmParams.put("COMPANYID", strCompanyId);
    hmParams.put("ACCPARTYID", strAccPartyId);
    hmParams.put("SELECTSYSTEMS", strSelectedSystems);
    hmParams.put("TYPEID", strTypeID);
    hmParams.put("VO", gmFetchGroupsVO);

    gmGroupVOList = gmPriceRequestRptBean.fetchConstructDetails(hmParams);

    gmPricingListVO.setGmGroupVO(gmGroupVOList);
    return gmPricingListVO;
  }

  /**
   * 
   * This method is used to get the Data in Price Report Screen
   * 
   */
  @POST
  @Path("pricingReportScreen")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmPricingListVO fetchAllPricingRequests(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException, Exception {

    /**
     * To get the statusId and fromdate and todate pass the json from UI in the below format var
     * input = {"statusId":"52120","fromdate":"12/27/2016","todate":"12/27/2016"};
     * 
     **/
    List<GmPricingReportVO> gmPriceReportVOList = new ArrayList<GmPricingReportVO>();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmPricingListVO gmPricingListVO = new GmPricingListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmPricingReportVO gmPricingReport = mapper.readValue(strInput, GmPricingReportVO.class);
    GmPricingAccessFilterVO gmPricingAccessFilterVO = new GmPricingAccessFilterVO();
    GmPricingReportVO gmPricingReportVO = new GmPricingReportVO();
    GmCommonBean gmCommonBean = new GmCommonBean();
    validateToken(gmPricingReport.getToken());

    log.debug("The InPUT before calling the Bean method: " + strInput);
    String strStatusId = gmPricingReport.getStatus();
    String strFromDate = gmPricingReport.getFromdate();
    String strToDate = gmPricingReport.getTodate();
    String strCompany = gmPricingReport.getCmpid();
    String strUserId = gmPricingReport.getUserid();
    GmPriceRequestRptBean gmPriceRequestRptBean = new GmPriceRequestRptBean(gmPricingReport);
    gmPricingAccessFilterVO = fetchAccessFilters(strInput);

    String strAccessFilter = gmPricingAccessFilterVO.getFiletrcondition();
    HashMap hmSalesFilters = gmCommonBean.getSalesFilterLists(strAccessFilter);
    ArrayList alRepList = new ArrayList();
    alRepList = GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters.get("REP"));
    String strFilterlRepIds = "";
    String strRepIds = "";
    HashMap hmDetails = new HashMap();
    if (alRepList != null && alRepList.size() > 0) {
      for (int i = 0; i < alRepList.size(); i++) {
        hmDetails = GmCommonClass.parseNullHashMap((HashMap) alRepList.get(i));
        strRepIds = GmCommonClass.parseNull((String) hmDetails.get("ID"));
        strFilterlRepIds += strRepIds + ",";
      }
    }
    HashMap hmParams = new HashMap();
    hmParams.put("STATUS", strStatusId);
    hmParams.put("FROMDATE", strFromDate);
    hmParams.put("TODATE", strToDate);
    hmParams.put("COMPANY", strCompany);
    hmParams.put("FILTERREPS", strFilterlRepIds);
    hmParams.put("USERID", strUserId);
    hmParams.put("VO", gmPricingReportVO);

    gmPriceReportVOList = gmPriceRequestRptBean.fetchAllPricingRequests(hmParams);
    
    gmPricingListVO.setGmPricingReportVO(gmPriceReportVOList);
    return gmPricingListVO;
  }


  /**
   * 
   * This method is used to save the pricing details
   * 
   * @throws Exception
   * 
   */
  @POST
  @Path("saveOrUpdatePricingDetails")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmPricingReqDetailsVO saveOrUpdatePricingDetails(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException, Exception {
    log.debug("saveOrUpdatePricingDetails strInput >>>>>" + strInput);
    ObjectMapper mapper = new ObjectMapper();
    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
    GmPricingReqDetailsVO gmPricingReqDetailsVO =
        mapper.readValue(strInput, GmPricingReqDetailsVO.class);
    validateToken(gmPricingReqDetailsVO.getToken());
    String strPriceRequestId =
        GmCommonClass.parseNull(gmPricingReqDetailsVO.getGmAccountPriceRequestVO()
            .getPricerequestid());
    String strRequestStatus =
        GmCommonClass.parseNull(gmPricingReqDetailsVO.getGmAccountPriceRequestVO()
            .getRequestStatus());
    String strUserId =
        GmCommonClass.parseNull(gmPricingReqDetailsVO.getGmAccountPriceRequestVO().getUserid());
    String strSelectedSystems =
        GmCommonClass.parseNull(gmPricingReqDetailsVO.getGmAccountPriceRequestVO()
            .getSelectedSystems());
    String strAccessLvl =
        GmCommonClass.parseNull(gmPricingReqDetailsVO.getGmAccountPriceRequestVO().getStropt());
    GmPriceRequestTransBean gmPriceReqTransBean = new GmPriceRequestTransBean(gmPricingReqDetailsVO.getGmAccountPriceRequestVO());
    HashMap<String, String> hmParams = new HashMap<String, String>();
    hmParams.put("JSONDATA", strInput);
    hmParams.put("PRCREQID", strPriceRequestId);
    hmParams.put("PRCREQUSTUS", strRequestStatus);
    hmParams.put("USERID", strUserId);
    hmParams.put("ACCESSLVL", strAccessLvl);
    hmParams.put("SELECTSYSTEMS", strSelectedSystems);
    String strOutPrcReqId = gmPriceReqTransBean.saveOrUpdatePricingDetails(hmParams);
    String[] strSavedPRTIDs = strOutPrcReqId.split("@");

    gmPricingReqDetailsVO.setPriceequestid(strSavedPRTIDs[0]);
    gmPricingReqDetailsVO.setPricerequeststatus(strSavedPRTIDs[1]);

    return gmPricingReqDetailsVO;
  }

  /**
   * 
   * This method is used to get the Account Information once click on I icon
   * 
   */
  @POST
  @Path("accessFilter")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmPricingAccessFilterVO fetchAccessFilters(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {
    GmUserVO gmUserVO = new GmUserVO();
    GmPricingAccessFilterVO gmPricingAccessFilterVO = new GmPricingAccessFilterVO();

    // validate token and parse JSON into VO
    ObjectMapper mapper = new ObjectMapper();
    gmPricingAccessFilterVO = mapper.readValue(strInput, GmPricingAccessFilterVO.class);
    validateToken(gmPricingAccessFilterVO.getToken());
    HashMap hmParam = new HashMap();
    hmParam.put("USER_ID", gmPricingAccessFilterVO.getUserid());
    hmParam.put("ACCES_LVL", gmPricingAccessFilterVO.getAcid());
    hmParam.put("DEPT_ID", gmPricingAccessFilterVO.getDeptid());

    String strFilterCondition = (new GmSalesDispatchAction()).getAccessFilter(hmParam);
    gmPricingAccessFilterVO.setFiletrcondition(strFilterCondition);

    return gmPricingAccessFilterVO;
  }

  /**
   * 
   * This method is used to save the pricing details
   * 
   */
  @POST
  @Path("priceRequestDetails")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmPricingReqDetailsVO fetchPriceRequestDetails(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException, Exception {
    log.debug("priceRequestDetails strInput >>>>>" + strInput);

    GmPricingAccessFilterVO gmPricingAccessFilterVO = new GmPricingAccessFilterVO();
    GmCommonBean gmCommonBean = new GmCommonBean();
    ObjectMapper mapper = new ObjectMapper();
    HashMap hmParam = new HashMap();
    GmPricingReqDetailsVO gmPricingReqDetailsVO =
        mapper.readValue(strInput, GmPricingReqDetailsVO.class);
    validateToken(gmPricingReqDetailsVO.getToken());
    String strPriceRequestId = GmCommonClass.parseNull(gmPricingReqDetailsVO.getPricerequestid());
    strPriceRequestId =
        strPriceRequestId.contains("PR-") ? strPriceRequestId : "PR-" + strPriceRequestId;
    String strUserId = gmPricingReqDetailsVO.getUserid();

    GmPriceRequestRptBean gmPriceRequestRptBean = new GmPriceRequestRptBean(gmPricingReqDetailsVO);
    gmPricingAccessFilterVO = fetchAccessFilters(strInput);

    String strAccessFilter = gmPricingAccessFilterVO.getFiletrcondition();
    HashMap hmSalesFilters = gmCommonBean.getSalesFilterLists(strAccessFilter);
    ArrayList alRepList = new ArrayList();
    alRepList = GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters.get("REP"));
    String strFilterlRepIds = "";
    String strRepIds = "";
    HashMap hmDetails = new HashMap();
    if (alRepList != null && alRepList.size() > 0) {
      for (int i = 0; i < alRepList.size(); i++) {
        hmDetails = GmCommonClass.parseNullHashMap((HashMap) alRepList.get(i));
        strRepIds = GmCommonClass.parseNull((String) hmDetails.get("ID"));
        strFilterlRepIds += strRepIds + ",";
      }
    }
    HashMap hmParams = new HashMap();
    hmParams.put("PRICINGREQID", strPriceRequestId);
    hmParams.put("FILTERREPS", strFilterlRepIds);
    hmParams.put("USERID", strUserId);

    HashMap hmReturn = gmPriceRequestRptBean.fetchPriceRequestDetails(hmParams);
    ArrayList systemResult = (ArrayList) hmReturn.get("SYSTEMDETAILS");
    ArrayList groupResult = (ArrayList) hmReturn.get("GROUPDETAILS");
    ArrayList priceReqResult = (ArrayList) hmReturn.get("PRICEREQDETAILS");
    com.globus.valueobject.pricing.GmSystemVO gmSystemVO =
        new com.globus.valueobject.pricing.GmSystemVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    List<com.globus.valueobject.pricing.GmSystemVO> gmSystemVOList =
        new ArrayList<com.globus.valueobject.pricing.GmSystemVO>();
    gmSystemVOList =
        gmWSUtil.getVOListFromHashMapListWithCaseProperty(systemResult, gmSystemVO,
            gmSystemVO.getSystemProperties());
    GmPricingReqDetailsVO gmPricingReqDetailVO = new GmPricingReqDetailsVO();

    List<GmGroupVO> gmGroupVOList = new ArrayList<GmGroupVO>();
    GmGroupVO gmFetchGroupsVO = new GmGroupVO();
    gmGroupVOList =
        gmWSUtil.getVOListFromHashMapList(groupResult, gmFetchGroupsVO,
            gmFetchGroupsVO.getGroupProperties());

    gmPricingReqDetailVO.setListGmSystemVO(gmSystemVOList);
    gmPricingReqDetailVO.setListGmGroupVO(gmGroupVOList);

    GmAccountPriceRequestVO gmAccountPriceRequestVO = new GmAccountPriceRequestVO();
    if (priceReqResult != null && priceReqResult.size() > 0) {
      HashMap hmapResult = (HashMap) priceReqResult.get(0);
      gmAccountPriceRequestVO.setPartyid((String) hmapResult.get("ACCOUNTID"));
      gmAccountPriceRequestVO.setPartyname((String) hmapResult.get("ACCOUNTNAME"));
      gmAccountPriceRequestVO.setPricerequestid((String) hmapResult.get("PRICEREQUESTID"));
      gmAccountPriceRequestVO.setProjected12MonthsSale((String) hmapResult.get("PROJ12MNTHSCALE"));
      gmSystemVO.setChangetype((String) hmapResult.get("CHANGETYPE"));
      gmSystemVO.setChangevalue((String) hmapResult.get("CHANGEVALUE"));
      gmAccountPriceRequestVO.setTypeid((String) hmapResult.get("TYPEID"));
      gmAccountPriceRequestVO.setQuestioncnt((String) hmapResult.get("QUESTIONCNT"));
      gmAccountPriceRequestVO.setRequestStatus((String) hmapResult.get("STATUSNAME"));
      gmAccountPriceRequestVO.setStatusid((String) hmapResult.get("STATUSID"));
      gmAccountPriceRequestVO.setDocumentcount((String) hmapResult.get("DOCUMENTCOUNT"));
      gmAccountPriceRequestVO.setImpactcount((String) hmapResult.get("IMPACTCOUNT"));
      log.debug("fetchPriceRequestDetails hmapResult >>>>>" + hmapResult);
    }
    gmPricingReqDetailVO.setGmAccountPriceRequestVO(gmAccountPriceRequestVO);

    return gmPricingReqDetailVO;
  }

  /**
   * 
   * This method is used to get the Access Flag based on the logined User and restricting him to not
   * view the PRICING module if he does not belongs to the below mentioned Security Group.
   * 
   */
  @POST
  @Path("TeamAccessFlag")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmPricingAccessFilterVO fetchTeamAccessFlag(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {
    GmUserVO gmUserVO = new GmUserVO();
    String strAccessFl = "";
    GmPricingAccessFilterVO gmPricingAccessFilterVO = new GmPricingAccessFilterVO();
    HashMap hmAccess = new HashMap();
    // validate token and parse JSON into VO
    ObjectMapper mapper = new ObjectMapper();
    gmPricingAccessFilterVO = mapper.readValue(strInput, GmPricingAccessFilterVO.class);
    validateToken(gmPricingAccessFilterVO.getToken());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(gmPricingAccessFilterVO);
    String strUserPartyId = gmPricingAccessFilterVO.getPartyid();
    // User access for Pricing in IPAD
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserPartyId,
            "PRICING_APP_ACCESS"));
    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    strAccessFl = strAccessFl.equals("") ? "N" : strAccessFl;
    gmPricingAccessFilterVO.setPriceAppAccessFl(strAccessFl);
    // User access for CRM in IPAD
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserPartyId,
            "CRM_APP_ACCESS"));
    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    strAccessFl = strAccessFl.equals("") ? "N" : strAccessFl;
    gmPricingAccessFilterVO.setCrmAppAccessFl(strAccessFl);
    return gmPricingAccessFilterVO;
  }

  /**
   * 
   * This method is used to Void the Selected Price REquest ID from the IPAD Pricing Request Dash
   * borad report screen
   * 
   */
  @POST
  @Path("voidPriceRequest")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCommonCancelVO voidPriceRequest(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {
    log.debug("voidPriceRequest strInput >>>>>" + strInput);
    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
    ObjectMapper mapper = new ObjectMapper();
    GmCommonCancelVO gmCommonCancelVO = mapper.readValue(strInput, GmCommonCancelVO.class);
    validateToken(gmCommonCancelVO.getToken());
    GmCommonCancelBean gmCommonCancel = new GmCommonCancelBean(gmCommonCancelVO);

    String strPriceRequestId = GmCommonClass.parseNull(gmCommonCancelVO.getTxnids());
    String strCancelReason = GmCommonClass.parseNull(gmCommonCancelVO.getCancelreasonid());
    String strCancelComment = GmCommonClass.parseNull(gmCommonCancelVO.getComments());
    String strCancelType = "VDPRRS";
    String strCompanyId = GmCommonClass.parseNull(gmCommonCancelVO.getCmpid());
    String strUserId = GmCommonClass.parseNull(gmCommonCancelVO.getUserid());

    HashMap<String, String> hmParams = new HashMap<String, String>();
    hmParams.put("TXNID", strPriceRequestId);
    hmParams.put("CANCELTYPE", strCancelType);
    hmParams.put("CANCELREASON", strCancelReason);
    hmParams.put("COMMENTS", strCancelComment);
    hmParams.put("COMPANYID", strCompanyId);
    hmParams.put("USERID", strUserId);
    hmParams.put("SHOWSUCCESSMSG", "N");
    gmCommonCancel.UpdateCancel(hmParams);
    gmCommonCancelVO.setTxnids(strPriceRequestId);
    return gmCommonCancelVO;
  }

  /**
   * 
   * This method is used to get the default Price Status Drop-down values selected values
   * 
   */
  @POST
  @Path("fetchDefaultStatus")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmRulesVO fetchDefaultStatus(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {
    ObjectMapper mapper = new ObjectMapper();
    GmRulesVO gmRulesVO = mapper.readValue(strInput, GmRulesVO.class);
    validateToken(gmRulesVO.getToken());
    GmCommonClass gmCommonClass = new GmCommonClass();
    String strRuleId = GmCommonClass.parseNull(gmRulesVO.getRuleid());
    String strRuleGrpId = GmCommonClass.parseNull(gmRulesVO.getRulegrpid());
    String strCmpId = GmCommonClass.parseNull(gmRulesVO.getCmpid());
    String strRuleVal = gmCommonClass.getRuleValueByCompany(strRuleId, strRuleGrpId, strCmpId);
    gmRulesVO.setRulevalue(strRuleVal);
    return gmRulesVO;
  }

  /**
   * 
   * This method is used to get the Group part pricing details Name
   * 
   */
  @POST
  @Path("PartPricing")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmGroupPartPricingVO fetchPartPricingDetails(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {
	    log.debug("groupPartMapDetailsList strInput >>>>>" + strInput);
	    GmGroupPartPricingVO gmGroupPartPricingVO = new GmGroupPartPricingVO();
	    GmWSUtil gmWSUtil = new GmWSUtil();
	    HashMap hmReturn = new HashMap();
	    ObjectMapper mapper = new ObjectMapper();
	    GmGroupPartPricingVO gmGroupPartPricing = mapper.readValue(strInput, GmGroupPartPricingVO.class);
	    HashMap<String, String> hmParams = new HashMap<String, String>();
	    GmGroupPartPricingBean gmGroupPartPricingBean = new GmGroupPartPricingBean(gmGroupPartPricing);
	    String strPartNum = gmGroupPartPricing.getPartNum();
	    String strSearch = gmGroupPartPricing.getSearch();
	    String strUserId = gmGroupPartPricing.getUserid();
	    validateToken(gmGroupPartPricing.getToken());
	    hmParams.put("PARTNUMBERS", strPartNum);
	    hmParams.put("HSEARCH", strSearch);
	    hmParams.put("USERID", strUserId);
	    log.debug("strUserId>>>>>>"+strUserId);
	    hmReturn = gmGroupPartPricingBean.fetchGrpPartPriceDetails(hmParams);
	    ArrayList alResult = new ArrayList();
	    alResult = (ArrayList) hmReturn.get("alPartResult");
	    log.debug("alPartResult>>>>>> "+alResult);
	    ArrayList alGroupResult = (ArrayList) hmReturn.get("alGroupResult");
	    log.debug("alGroupResult>>>>>> "+alGroupResult);
	    com.globus.valueobject.pricing.GmGroupDetailsVO gmGroupDetailsVO =
	            new com.globus.valueobject.pricing.GmGroupDetailsVO();
	    List<com.globus.valueobject.pricing.GmGroupDetailsVO> gmGroupDetailsVOlist =
	            new ArrayList<com.globus.valueobject.pricing.GmGroupDetailsVO>();
	    gmGroupDetailsVOlist =
	            gmWSUtil.getVOListFromHashMapList(alGroupResult, gmGroupDetailsVO,
	         	gmGroupDetailsVO.getGroupPricingDetailsVO());
	    List<com.globus.valueobject.pricing.GmPartDetailsVO> gmPartDetailsVOList = new ArrayList<com.globus.valueobject.pricing.GmPartDetailsVO>();
	    com.globus.valueobject.pricing.GmPartDetailsVO gmPartDetailsVO = new com.globus.valueobject.pricing.GmPartDetailsVO();
	        gmPartDetailsVOList =
	            gmWSUtil.getVOListFromHashMapList(alResult, gmPartDetailsVO,
	            		gmPartDetailsVO.getPartPricingDetailsVO());

	        gmGroupPartPricingVO.setListGmGruouVO(gmGroupDetailsVOlist);  
	        gmGroupPartPricingVO.setListGmPartVO(gmPartDetailsVOList);
	    
	return gmGroupPartPricingVO;
 }
  
  /**
   * genereteHeldOrderPriceRequest(): This method is used to genereate the held order Price requst
   * @throws Exception
   */
  @POST
  @Path("heldOrderPriceRequest")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmPricingReqDetailsVO genereteHeldOrderPriceRequest(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException, Exception {
    log.debug("genereteHeldOrderPriceRequest strInput >>>>>" + strInput);
    ObjectMapper mapper = new ObjectMapper();
    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
    GmPricingReqDetailsVO gmPricingReqDetailsVO =
        mapper.readValue(strInput, GmPricingReqDetailsVO.class);
    validateToken(gmPricingReqDetailsVO.getToken());

    String strSelectedOrderIds = GmCommonClass.parseNull(gmPricingReqDetailsVO.getOrderids());
    String struserId = GmCommonClass.parseNull(gmPricingReqDetailsVO.getUserid());
    GmPriceRequestTransBean gmPriceReqTransBean = new GmPriceRequestTransBean(gmPricingReqDetailsVO);
    HashMap<String, String> hmParams = new HashMap<String, String>();
    hmParams.put("ORDERIDLIST", strSelectedOrderIds);
    hmParams.put("USERID", struserId);
    String strOutPrcReqId = gmPriceReqTransBean.genereteHeldOrderPriceReq(hmParams);

    gmPricingReqDetailsVO.setPriceequestid(strOutPrcReqId);

    return gmPricingReqDetailsVO;
  }
  
  /**
   * fetchHeldOrderPriceRequest(): This method is used to genereate the held order Price requst
   * @throws Exception
   */
  @POST
  @Path("fetchHeldOrderPriceRequest")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmPricingReqDetailsVO fetchHeldOrderPriceRequest(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException, Exception {
    log.debug("fetchHeldOrderPriceRequest strInput >>>>>" + strInput);
    ObjectMapper mapper = new ObjectMapper();
    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
    GmPricingReqDetailsVO gmPricingReqDetailsVO =
            mapper.readValue(strInput, GmPricingReqDetailsVO.class);
        validateToken(gmPricingReqDetailsVO.getToken());
    String strOrderId = GmCommonClass.parseNull(gmPricingReqDetailsVO.getOrderids());
    GmPriceRequestTransBean gmPriceReqTransBean = new GmPriceRequestTransBean(gmPricingReqDetailsVO);
    
    HashMap<String, String> hmParams = new HashMap<String, String>();
    hmParams.put("ORDERID", strOrderId);
    HashMap hmReturn = gmPriceReqTransBean.fetchHeldOrderPriceRequestDetails(hmParams);
    ArrayList alResult = (ArrayList) hmReturn.get("HELDORDERDETAILS");
    com.globus.valueobject.pricing.GmHeldOrderPriceRequestVO gmHeldOrderPriceRequestVO =
        new com.globus.valueobject.pricing.GmHeldOrderPriceRequestVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    
    List<com.globus.valueobject.pricing.GmHeldOrderPriceRequestVO> gmHeldOrderPriceRequestVOList =
        new ArrayList<com.globus.valueobject.pricing.GmHeldOrderPriceRequestVO>();
    gmHeldOrderPriceRequestVOList =
        gmWSUtil.getVOListFromHashMapListWithCaseProperty(alResult, gmHeldOrderPriceRequestVO,
        		gmHeldOrderPriceRequestVO.getHeldOrderPriceRequest());
    gmPricingReqDetailsVO.setListgmHeldOrderPriceRequestVO(gmHeldOrderPriceRequestVOList);
    return gmPricingReqDetailsVO;
  }
  
  
  

  /**
   * Suganthi
   * This method is used to get the Group System part pricing details Name
   * 
   */
  @POST
  @Path("SystemPartPricing")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmGroupPartPricingVO fetchPartSystemPricingDetails(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {
	    log.debug("fetchPartSystemPricingDetails strInput >>>>>" + strInput);
	    GmGroupPartPricingVO gmGroupPartPricingVO = new GmGroupPartPricingVO();
	    GmWSUtil gmWSUtil = new GmWSUtil();
	    HashMap hmReturn = new HashMap();
	    ObjectMapper mapper = new ObjectMapper();
	    String strSystem ="";
	    String strPartNum ="";
	    String strAccId = "";
	    String strUserId ="";
	    GmGroupPartPricingVO gmGroupPartPricing = mapper.readValue(strInput, GmGroupPartPricingVO.class);
	    HashMap<String, String> hmParams = new HashMap<String, String>();
	    GmGroupPartPricingBean gmGroupPartPricingBean = new GmGroupPartPricingBean(gmGroupPartPricing);
	    strPartNum = gmGroupPartPricing.getPartNum();
	    strSystem = gmGroupPartPricing.getSystem();
	    strAccId =gmGroupPartPricing.getAccId();
	    strUserId = gmGroupPartPricing.getUserid();
	    validateToken(gmGroupPartPricing.getToken());
	    hmParams.put("PARTNUMBERS", strPartNum);
	    hmParams.put("SYSTEMNAME", strSystem);
	    hmParams.put("ACCID", strAccId);
	    hmParams.put("USERID", strUserId);
	    hmReturn = gmGroupPartPricingBean.fetchPartSystemPriceDetails(hmParams);
	    ArrayList alResult = new ArrayList();
	    alResult = (ArrayList) hmReturn.get("alPartResult");
	    log.debug("alPartResult>>>>>> "+alResult);
/*	    ArrayList alGroupResult = (ArrayList) hmReturn.get("alGroupResult");
	    log.debug("alGroupResult>>>>>> "+alGroupResult);*/
	    com.globus.valueobject.pricing.GmGroupDetailsVO gmGroupDetailsVO =
	            new com.globus.valueobject.pricing.GmGroupDetailsVO();
	    List<com.globus.valueobject.pricing.GmGroupDetailsVO> gmGroupDetailsVOlist =
	            new ArrayList<com.globus.valueobject.pricing.GmGroupDetailsVO>();
	    //gmGroupDetailsVOlist =gmWSUtil.getVOListFromHashMapList(alGroupResult, gmGroupDetailsVO,gmGroupDetailsVO.getGroupPricingDetailsVO());
	    List<com.globus.valueobject.pricing.GmPartDetailsVO> gmPartDetailsVOList = new ArrayList<com.globus.valueobject.pricing.GmPartDetailsVO>();
	    com.globus.valueobject.pricing.GmPartDetailsVO gmPartDetailsVO = new com.globus.valueobject.pricing.GmPartDetailsVO();
	        gmPartDetailsVOList =
	            gmWSUtil.getVOListFromHashMapList(alResult, gmPartDetailsVO,
	            		gmPartDetailsVO.getPartPricingDetailsVO());

	        //gmGroupPartPricingVO.setListGmGruouVO(gmGroupDetailsVOlist);  
	        gmGroupPartPricingVO.setListGmPartVO(gmPartDetailsVOList);
	    
	return gmGroupPartPricingVO;
 }
}
