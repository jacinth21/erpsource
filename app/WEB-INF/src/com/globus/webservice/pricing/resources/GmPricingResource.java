package com.globus.webservice.pricing.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.sales.pricing.beans.GmGroupPartPricingBean;
import com.globus.valueobject.common.master.GmMasterDataSyncListVO;
import com.globus.valueobject.common.master.GmMasterSyncReqVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.pricing.GmAccountPriceVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.config.Compress;

/**
 * @author Manikandan
 * 
 */
@Path("accounts")
public class GmPricingResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /*
   * @POST
   * 
   * @Path("master")
   * 
   * @Produces({ MediaType.APPLICATION_JSON })
   * 
   * @Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON }) public
   * GmMasterDataSyncListVO fetchAccountPricing(String strInput) throws AppError{
   * 
   * }
   */

  /**
   * fetchAccountPricing - This method will returns Account values while calling web service. First
   * call, It will returns all Account id(Master Sync) with Account Id will get Account Details
   * after that it will returns updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @Compress
  @POST
  @Path("acctprice")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmMasterDataSyncListVO fetchAccountPricing(String strInput) throws AppError {

    HashMap hmValidate = new HashMap();
    HashMap hmParams = new HashMap();
    ArrayList alAccountpricing, alGPOpricing = null;
    List<GmAccountPriceVO> listGmAcctPartPriceVO = new ArrayList<GmAccountPriceVO>();

    GmMasterDataSyncListVO gmMasterDataSyncListVO = new GmMasterDataSyncListVO();
    GmMasterSyncReqVO gmMasterSyncReqVO = new GmMasterSyncReqVO();
   
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmAccountPriceVO gmAccountPriceVO = new GmAccountPriceVO();
    GmWSUtil gmWSUtil = new GmWSUtil();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmMasterSyncReqVO);
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID
    gmProdCatUtil.validateInput(gmMasterSyncReqVO, hmValidate);
    GmGroupPartPricingBean gmGroupPartPricingBean = new GmGroupPartPricingBean(gmMasterSyncReqVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmMasterSyncReqVO);
    // To get Page Information details all the below values are passed to procedure.
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmMasterSyncReqVO.getToken());
    hmParams.put("UUID", GmCommonClass.parseNull(gmMasterSyncReqVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmMasterSyncReqVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmMasterSyncReqVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmMasterSyncReqVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("SYNCALL", GmCommonClass.parseNull(gmMasterSyncReqVO.getSyncall()));

    String strAccId = GmCommonClass.parseNull(gmMasterSyncReqVO.getAcctid());
    String strRefType = GmCommonClass.parseNull(gmMasterSyncReqVO.getReftype());
    hmParams.put("ACCTID", strAccId);
    hmParams.put("REFTYPE", strRefType);
    hmParams.put("VO", gmAccountPriceVO);

    listGmAcctPartPriceVO = gmGroupPartPricingBean.fetchAccountPriceDetail(hmParams);
    gmAccountPriceVO =
        (GmAccountPriceVO) gmMasterDataSyncListVO.setPageProperties(listGmAcctPartPriceVO);
    gmAccountPriceVO = (gmAccountPriceVO == null) ? new GmAccountPriceVO() : gmAccountPriceVO;


    gmMasterDataSyncListVO.setReftype(strRefType);
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmMasterDataSyncListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmMasterDataSyncListVO.getPagesize()));

    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);
    gmMasterDataSyncListVO.setListGmAcctPartPriceVO(listGmAcctPartPriceVO);
    return gmMasterDataSyncListVO;

  }

  /**
   * fetchAccounts - This method will returns Account values while calling web service. First call,
   * It will returns all Account id(Master Sync) with Account Id will get Account Details after that
   * it will returns updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @Compress
  @POST
  @Path("grpprice")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmMasterDataSyncListVO fetchGPOPricing(String strInput) throws AppError {
    log.debug("comming here***** account part price");

    HashMap hmValidate = new HashMap();
    HashMap hmParams = new HashMap();
    ArrayList alAccountGPOpricing = new ArrayList();
    List<GmAccountPriceVO> listGmAcctGrpPriceVO = new ArrayList<GmAccountPriceVO>();

    GmMasterDataSyncListVO gmMasterDataSyncListVO = new GmMasterDataSyncListVO();
    GmMasterSyncReqVO gmMasterSyncReqVO = new GmMasterSyncReqVO();
    
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmAccountPriceVO gmAccountPriceVO = new GmAccountPriceVO();
    GmWSUtil gmWSUtil = new GmWSUtil();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmMasterSyncReqVO);
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    GmGroupPartPricingBean gmGroupPartPricingBean = new GmGroupPartPricingBean(gmMasterSyncReqVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmMasterSyncReqVO);
    // validate device UUID
    gmProdCatUtil.validateInput(gmMasterSyncReqVO, hmValidate);
    // To get Page Information details all the below values are passed to procedure.
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmMasterSyncReqVO.getToken());
    String strAccId = GmCommonClass.parseNull(gmMasterSyncReqVO.getAcctid());
    String strRefType = GmCommonClass.parseNull(gmMasterSyncReqVO.getReftype());

    hmParams.put("UUID", GmCommonClass.parseNull(gmMasterSyncReqVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmMasterSyncReqVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmMasterSyncReqVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmMasterSyncReqVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("SYNCALL", GmCommonClass.parseNull(gmMasterSyncReqVO.getSyncall()));
    hmParams.put("ACCTID", strAccId);
    hmParams.put("REFTYPE", strRefType);
    hmParams.put("VO", gmAccountPriceVO);
    listGmAcctGrpPriceVO = gmGroupPartPricingBean.fetchGpoPriceDetail(hmParams);
    gmAccountPriceVO =
        (GmAccountPriceVO) gmMasterDataSyncListVO.setPageProperties(listGmAcctGrpPriceVO);
    gmAccountPriceVO = (gmAccountPriceVO == null) ? new GmAccountPriceVO() : gmAccountPriceVO;


    gmMasterDataSyncListVO.setReftype(strRefType);
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmMasterDataSyncListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmMasterDataSyncListVO.getPagesize()));

    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);
    gmMasterDataSyncListVO.setListGmAcctPartPriceVO(listGmAcctGrpPriceVO);

    return gmMasterDataSyncListVO;

  }
}
