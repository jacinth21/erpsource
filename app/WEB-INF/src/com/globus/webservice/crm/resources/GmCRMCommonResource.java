package com.globus.webservice.crm.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.bean.GmCRMCommonBean;
import com.globus.webservice.crm.valueobject.GmCRMTabsVO;
import com.globus.webservice.crm.valueobject.GmCRMListVO;

@Path("crm")
public class GmCRMCommonResource extends GmResource {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  @POST
  @Path("usertabs")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listTabs(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {

    GmCRMTabsVO gmCRMTabsVO = new GmCRMTabsVO();
    List<GmCRMTabsVO> listGmCRMTabsVO = new ArrayList<GmCRMTabsVO>();
    GmWSUtil gmWSUtil = new GmWSUtil();
    ArrayList alReturn = new ArrayList();
    HashMap hmInput = new HashMap();
    GmCRMCommonBean gmCRMCommonBean = new GmCRMCommonBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    gmCRMTabsVO = mapper.readValue(strInput, GmCRMTabsVO.class);

    validateToken(gmCRMTabsVO.getToken());
    gmCRMTabsVO.setUserid(gmUserVO.getUserid());

    hmInput = gmWSUtil.getHashMapFromVO(gmCRMTabsVO);

    alReturn = gmCRMCommonBean.fetchUserFunctionList(hmInput);

    listGmCRMTabsVO = gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMTabsVO);
    gmCRMListVO.setGmCRMTabsVO(listGmCRMTabsVO);
    return gmCRMListVO;
  }
}
