package com.globus.webservice.crm.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.bean.GmCRMFieldSalesBean;
import com.globus.webservice.crm.bean.GmCriteriaBean;
import com.globus.webservice.crm.valueobject.GmAccountBasicVO;
import com.globus.webservice.crm.valueobject.GmCRMSalesRepListVO;
import com.globus.webservice.crm.valueobject.GmCriteriaVO;
import com.globus.webservice.crm.valueobject.GmCRMListVO;

@Path("fieldsales")
public class GmCRMFieldSalesResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());


  @POST
  @Path("accmapinfo")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listAccountMapInfo(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmAccountBasicVO gmAccountBasicVO = new GmAccountBasicVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMFieldSalesBean gmCRMFieldSalesBean = new GmCRMFieldSalesBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    gmAccountBasicVO = mapper.readValue(strInput, GmAccountBasicVO.class);
    ArrayList alReturn = new ArrayList();
    List<GmAccountBasicVO> listGmAccountBasicVO = new ArrayList<GmAccountBasicVO>();

    validateToken(gmAccountBasicVO.getToken());

    alReturn =
        GmCommonClass.parseNullArrayList((gmCRMFieldSalesBean.fetchAccountMap(gmAccountBasicVO
            .getRepid())));

    log.debug("alRerutn>>>" + alReturn);

    listGmAccountBasicVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmAccountBasicVO,
            gmAccountBasicVO.getMappingProperties());

    log.debug("listGmCRMSalesRepListVO>>>>" + listGmAccountBasicVO);
    gmCRMListVO.setGmAccountBasicVO(listGmAccountBasicVO);
    return gmCRMListVO;

  }

  @POST
  @Path("repinfo")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMSalesRepListVO listSalesRepInfo(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMSalesRepListVO gmCRMSalesRepListVO = new GmCRMSalesRepListVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMFieldSalesBean gmCRMFieldSalesBean = new GmCRMFieldSalesBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMSalesRepListVO gmCRMSalesRepVO = mapper.readValue(strInput, GmCRMSalesRepListVO.class);
    ArrayList alReturn = new ArrayList();
    List<GmCRMSalesRepListVO> listGmCRMSalesRepListVO = new ArrayList<GmCRMSalesRepListVO>();

    validateToken(gmCRMSalesRepVO.getToken());
    String strRepID = gmCRMSalesRepVO.getRepid();
    String strFirstnm = gmCRMSalesRepVO.getFirstnm();
    String strLastnm = gmCRMSalesRepVO.getLastnm();

    gmCRMSalesRepListVO.setUserid(gmUserVO.getUserid());

    log.debug("User ID>>>>>" + gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    HashMap hmResult = new HashMap();

    gmWSUtil.parseJsonToVO(strInput, gmCRMSalesRepListVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMSalesRepListVO);

    alReturn = GmCommonClass.parseNullArrayList((gmCRMFieldSalesBean.fetchSalesRep(hmInput)));

    log.debug("alRerutn>>>" + alReturn);

    listGmCRMSalesRepListVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMSalesRepListVO,
            gmCRMSalesRepListVO.getMappingProperties());
    
    log.debug("listGmCRMSalesRepListVO>>>>" + listGmCRMSalesRepListVO);
//    gmCRMListVO.setGmCRMSalesRepListVO(listGmCRMSalesRepListVO);
    if(listGmCRMSalesRepListVO.size() >0)
    	gmCRMSalesRepListVO =listGmCRMSalesRepListVO.get(0);
    return gmCRMSalesRepListVO;

  }



  @POST
  @Path("repfname")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_ATOM_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listSalesRepFName(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMSalesRepListVO gmCRMSalesRepListVO = new GmCRMSalesRepListVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMFieldSalesBean gmCRMFieldSalesBean = new GmCRMFieldSalesBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMSalesRepListVO gmCRMSalesRepVO = mapper.readValue(strInput, GmCRMSalesRepListVO.class);
    ArrayList alreturn = new ArrayList();
    List<GmCRMSalesRepListVO> listGmCRMSalesRepListVO = new ArrayList<GmCRMSalesRepListVO>();

    validateToken(gmCRMSalesRepVO.getToken());
    String strFirstnm = gmCRMSalesRepVO.getFirstnm();

    gmCRMSalesRepListVO.setUserid(gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    gmWSUtil.parseJsonToVO(strInput, gmCRMSalesRepListVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMSalesRepListVO);

    alreturn = GmCommonClass.parseNullArrayList((gmCRMFieldSalesBean.repFName(hmInput)));
    log.debug("alreturn>>>" + alreturn);
    listGmCRMSalesRepListVO =
        gmWSUtil.getVOListFromHashMapList(alreturn, gmCRMSalesRepListVO,
            gmCRMSalesRepListVO.getMappingProperties());
    log.debug("listGmCRMSalesRepListVO>>>>" + listGmCRMSalesRepListVO);
    gmCRMListVO.setGmCRMSalesRepListVO(listGmCRMSalesRepListVO);
    return gmCRMListVO;
  }


  @POST
  @Path("criterialist")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listFieldSalesInfoByCriteria(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMSalesRepListVO gmCRMSalesRepListVO = new GmCRMSalesRepListVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMFieldSalesBean gmCRMFieldSalesBean = new GmCRMFieldSalesBean();
    GmCRMListVO gmCRMListVO  = new GmCRMListVO();
    List<GmCRMSalesRepListVO> listGmCRMSalesRepListVO = new ArrayList<GmCRMSalesRepListVO>();

    ArrayList alCriteria = new ArrayList();
    ArrayList alReturn = new ArrayList();

    GmCriteriaBean gmCriteriaBean = new GmCriteriaBean();
    GmCriteriaVO gmCriteriaVO = new GmCriteriaVO();

    HashMap hmCriteria = new HashMap();
    HashMap hmInput = new HashMap();

    gmWSUtil.parseJsonToVO(strInput, gmCriteriaVO);

    validateToken(gmCriteriaVO.getToken());

    gmCRMSalesRepListVO.setUserid(gmUserVO.getUserid());

    log.debug("User ID>>>>>" + gmUserVO.getUserid());

    alCriteria =
        gmCriteriaBean.fetchCriteriaBy(gmCriteriaVO.getCriteriaid(), gmCriteriaVO.getCriteriatyp());

    for (int i = 0; i < alCriteria.size(); i++) {
      HashMap hmValues = GmCommonClass.parseNullHashMap((HashMap) alCriteria.get(i));
      String strKey = GmCommonClass.parseNull((String) hmValues.get("KEY"));
      String strValue = GmCommonClass.parseNull((String) hmValues.get("VALUE"));
      hmCriteria.put(strKey, strValue);
    }


    gmWSUtil.getVOFromHashMap(hmCriteria, gmCRMSalesRepListVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMSalesRepListVO);

    alReturn = GmCommonClass.parseNullArrayList((gmCRMFieldSalesBean.fetchSalesRep(hmInput)));

    log.debug("alRerutn>>>" + alReturn);

    listGmCRMSalesRepListVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMSalesRepListVO,
            gmCRMSalesRepListVO.getMappingProperties());

    log.debug("listGmCRMSalesRepListVO>>>>" + listGmCRMSalesRepListVO);
    gmCRMListVO.setGmCRMSalesRepListVO(listGmCRMSalesRepListVO);
    return gmCRMListVO;
  }

  @POST
  @Path("location")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_ATOM_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listSalesRepLocation(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMSalesRepListVO gmCRMSalesRepListVO = new GmCRMSalesRepListVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMFieldSalesBean gmCRMFieldSalesBean = new GmCRMFieldSalesBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMSalesRepListVO gmCRMSalesRepVO = mapper.readValue(strInput, GmCRMSalesRepListVO.class);
    ArrayList alreturn = new ArrayList();
    List<GmCRMSalesRepListVO> listGmCRMSalesRepListVO = new ArrayList<GmCRMSalesRepListVO>();

    validateToken(gmCRMSalesRepVO.getToken());
    String strFirstnm = gmCRMSalesRepVO.getFirstnm();

    gmCRMSalesRepListVO.setUserid(gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    gmWSUtil.parseJsonToVO(strInput, gmCRMSalesRepListVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMSalesRepListVO);

    alreturn = GmCommonClass.parseNullArrayList((gmCRMFieldSalesBean.repLocation(hmInput)));
    log.debug("alreturn>>>" + alreturn);
    listGmCRMSalesRepListVO =
        gmWSUtil.getVOListFromHashMapList(alreturn, gmCRMSalesRepListVO,
            gmCRMSalesRepListVO.getMappingProperties());
    log.debug("listGmCRMSalesRepListVO>>>>" + listGmCRMSalesRepListVO);
    gmCRMListVO.setGmCRMSalesRepListVO(listGmCRMSalesRepListVO);
    return gmCRMListVO;
  }
  
  @POST
  @Path("regterdis")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_ATOM_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listSalesRegTerDis(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMSalesRepListVO gmCRMSalesRepListVO = new GmCRMSalesRepListVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMFieldSalesBean gmCRMFieldSalesBean = new GmCRMFieldSalesBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMSalesRepListVO gmCRMSalesRepVO = mapper.readValue(strInput, GmCRMSalesRepListVO.class);
    ArrayList alreturn = new ArrayList();
    List<GmCRMSalesRepListVO> listGmCRMSalesRepListVO = new ArrayList<GmCRMSalesRepListVO>();

    validateToken(gmCRMSalesRepVO.getToken());
    
    gmCRMSalesRepListVO.setUserid(gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    gmWSUtil.parseJsonToVO(strInput, gmCRMSalesRepListVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMSalesRepListVO);

    alreturn = GmCommonClass.parseNullArrayList((gmCRMFieldSalesBean.fetchRegTerDis(hmInput)));
    log.debug("alreturn>>>" + alreturn);
    listGmCRMSalesRepListVO =
        gmWSUtil.getVOListFromHashMapList(alreturn, gmCRMSalesRepListVO,
            gmCRMSalesRepListVO.getMappingProperties());
    log.debug("listGmCRMSalesRepListVO>>>>" + listGmCRMSalesRepListVO);
    gmCRMListVO.setGmCRMSalesRepListVO(listGmCRMSalesRepListVO);
    return gmCRMListVO;
  }
  
}
