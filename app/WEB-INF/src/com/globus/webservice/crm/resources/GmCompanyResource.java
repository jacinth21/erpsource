package com.globus.webservice.crm.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.util.GmWSUtil;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.sales.GmCompanyVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.valueobject.GmCRMListVO;

/**
 * @author TMuthusamy GmCompanyResource --- Fetch company List and its details
 */
@Path("company")
public class GmCompanyResource extends GmResource {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * This method used to Fetch company list and show in dropdown(PC-2767)
	 */

	@POST
	@Path("complist")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmCRMListVO loadCompanyList(String strInput) throws AppError,
			IOException {
		log.debug("entered resource");
		GmCompanyVO gmCompanylistVO = new GmCompanyVO();
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmCRMListVO gmCRMListVO = new GmCRMListVO();
		ObjectMapper mapper = new ObjectMapper();
		ArrayList alCompanylist = new ArrayList();
		GmCompanyVO gmCompanyVO = mapper.readValue(strInput, GmCompanyVO.class);
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
		List<GmCompanyVO> listGmCompanylistVO = new ArrayList<GmCompanyVO>();

		GmCompanyBean gmCompanyBean = new GmCompanyBean(gmCompanyVO);
		GmPartyBean gmPartyBean = new GmPartyBean(gmCompanyVO);
		validateToken(gmCompanyVO.getToken());
		String strCompanyId = gmCompanyVO.getCompanyid();
		String strPartyId = gmCompanyVO.getPartyid();
		log.debug("strPartyId >>>> " + strPartyId);
		HashMap hmAccess = new HashMap();
		HashMap hmParam = new HashMap();
		hmParam.put("PARTYID", strPartyId);
		hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "CRMADMIN-SURGEON");
		
		String accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
	    if (accessFlag.equals("Y")) {
	    	alCompanylist = gmCompanyBean.loadCompanyInfo(strCompanyId);
	    }
	    else{
	    	alCompanylist = gmPartyBean.fetchPartyCompany(hmParam);
	    }
		
		// Converting list of Hashmap into list of VO and last arg for mapping
		// the hashmap key into vo
		// attributes
		listGmCompanylistVO = gmWSUtil.getVOListFromHashMapList(alCompanylist,
				gmCompanyVO, gmCompanyVO.getCompanyProperties());

		gmCRMListVO.setGmCompanyVO(listGmCompanylistVO);

		return gmCRMListVO;

	}

}
