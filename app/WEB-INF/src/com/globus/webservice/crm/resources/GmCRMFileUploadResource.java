package com.globus.webservice.crm.resources;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.beans.GmFileUploadBean;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.bean.GmCRMFileUploadBean;
import com.globus.webservice.crm.valueobject.GmCRMFileUploadVO;
import com.globus.webservice.crm.valueobject.GmCRMListVO;

@Path("file")
public class GmCRMFileUploadResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @POST
  @Path("savefile")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMFileUploadVO saveUploadInfo(String strInput) throws AppError,
      JsonGenerationException, JsonMappingException, JsonParseException, IOException, Exception {


    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMFileUploadBean gmFileUploadBean = new GmCRMFileUploadBean();

    ObjectMapper mapper = new ObjectMapper();
    GmCRMFileUploadVO gmCRMFileUploadVO = mapper.readValue(strInput, GmCRMFileUploadVO.class);

    validateToken(gmCRMFileUploadVO.getToken());
    gmCRMFileUploadVO.setUserid(gmUserVO.getUserid());


    HashMap hmInput = new HashMap();
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMFileUploadVO);


    hmInput = gmFileUploadBean.saveUploadInfo(hmInput);

    gmWSUtil.getVOFromHashMap(hmInput, gmCRMFileUploadVO);

    return gmCRMFileUploadVO;

  }

  @POST
  @Path("fetch")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO fetchUploadList(String strInput) throws AppError,
      JsonGenerationException, JsonMappingException, JsonParseException, IOException, Exception {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMFileUploadBean gmFileUploadBean = new GmCRMFileUploadBean();
    GmCRMFileUploadVO gmCRMFileUploadVO = new GmCRMFileUploadVO();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmCRMFileUploadVO> listGmCRMFileUploadVO = new ArrayList<GmCRMFileUploadVO>();

    gmWSUtil.parseJsonToVO(strInput, gmCRMFileUploadVO);
    validateToken(gmCRMFileUploadVO.getToken());
    gmCRMFileUploadVO.setUserid(gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMFileUploadVO);

    ArrayList alReturn = new ArrayList();
    String strUserId = gmUserVO.getUserid();

    alReturn = gmFileUploadBean.fetchFilesByType(gmCRMFileUploadVO.getRefid(), "105220");

    listGmCRMFileUploadVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMFileUploadVO,
            gmCRMFileUploadVO.getMappingProperties());
    gmCRMListVO.setGmCRMFileUploadVO(listGmCRMFileUploadVO);
    return gmCRMListVO;
  }

  @POST
  @Path("get")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO fetchFileList(String strInput) throws AppError,
      JsonGenerationException, JsonMappingException, JsonParseException, IOException, Exception {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMFileUploadBean gmFileUploadBean = new GmCRMFileUploadBean();
    GmCRMFileUploadVO gmCRMFileUploadVO = new GmCRMFileUploadVO();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmCRMFileUploadVO> listGmCRMFileUploadVO = new ArrayList<GmCRMFileUploadVO>();

    gmWSUtil.parseJsonToVO(strInput, gmCRMFileUploadVO);
    validateToken(gmCRMFileUploadVO.getToken());
    gmCRMFileUploadVO.setUserid(gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMFileUploadVO);

    ArrayList alReturn = new ArrayList();
    String strUserId = gmUserVO.getUserid();

    alReturn = gmFileUploadBean.fetchFiles(gmCRMFileUploadVO);

    listGmCRMFileUploadVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMFileUploadVO,
            gmCRMFileUploadVO.getMappingProperties());
    if(listGmCRMFileUploadVO.size() >0)
    	gmCRMListVO.setGmCRMFileUploadVO(listGmCRMFileUploadVO);
    else
    	gmCRMListVO = null;
    	
    return gmCRMListVO;
  }
  /*
   *  Fn To Get the url if the party has image else it returns null
   *  Pass refgroup as 103112 and refid as partyid and reftype as null , reftype for profile image is null  
   *  Sample URL:   http://localhost/resources/file/src?refgroup=103112&refid=706310&reftype=null&num=0.3136569020954372
   */
  @GET
  @Path("src")
  @Produces("application/binary")
  public Response fetchFileStream(@QueryParam("refid") String strRefId,
		  @QueryParam("refgroup") String strRefGroup, @QueryParam("reftype") String strRefType) throws AppError,
      IOException, Exception {

    GmCRMFileUploadBean gmFileUploadBean = new GmCRMFileUploadBean();
    GmCRMFileUploadVO gmCRMFileUploadVO = new GmCRMFileUploadVO();
  
    ArrayList alReturn = new ArrayList();
    HashMap hmReturn = new HashMap();
    String strFileName = "";
    String strURI = "";
    String strCRMFileServer = GmCommonClass.getString("URL_CRM_FILE_SERVER");
    URI uri =  null;
    
    gmCRMFileUploadVO.setRefid(strRefId);
    gmCRMFileUploadVO.setRefgroup(strRefGroup);
    gmCRMFileUploadVO.setReftype(strRefType);

    alReturn = gmFileUploadBean.fetchFiles(gmCRMFileUploadVO);
    if(alReturn.size() > 0){
    	hmReturn =(HashMap) alReturn.get(0);
    	strRefId = GmCommonClass.parseNull((String) hmReturn.get("REFID"));
    	strRefType = GmCommonClass.parseNull((String) hmReturn.get("REFTYPE"));
    	strRefGroup= GmCommonClass.parseNull((String) hmReturn.get("REFGROUP"));
    	strFileName = GmCommonClass.parseNull((String) hmReturn.get("FILENAME"));
    	strURI = strCRMFileServer + strRefGroup + "/" + strRefId + "/" + strFileName ;
    	uri = UriBuilder.fromUri(strURI).build();
    	return Response.seeOther(uri).build();
    }
    	return null;
  }
  
  /*
   *  Fn To upload and save the CV File for surgeon in Preceptorship module
   *  Pass file as object , refgroup as 103112, refid as partyid and reftype as 105220
   */
  
  @Path("crmfileupload")
  @POST
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  public String uploadFileInfo(@FormDataParam("file") InputStream fileInputStream,
      @FormDataParam("file") FormDataContentDisposition contentDispositionHeader,
      @FormDataParam("token") String strToken, @FormDataParam("refgrp") String strRefGrp,
      @FormDataParam("type") String strSubtype, @FormDataParam("reftype") String strRefType,
      @FormDataParam("refid") String strRefID) throws AppError, Exception {

    log.debug("the value insids fwjd");
    log.debug("strRefGrp>>>>> " + strRefGrp);
    GmFileUploadBean gmFileUploadBean = new GmFileUploadBean();

    String filePath =
        GmCommonClass.getString("CRMFILEPUSHLOC") + strRefGrp + "\\" + strRefID + "\\"
            + contentDispositionHeader.getFileName();
    String fileStatus = "File Uploaded Successfully";
    HashMap hmExpParam = new HashMap();

    log.debug("the value inside strToken ***" + strToken);
    try {
      validateToken(strToken);
      String strUserId = GmCommonClass.parseNull(gmUserVO.getUserid());
      String strfirstName = GmCommonClass.parseNull(gmUserVO.getFname());
      String strLastName = GmCommonClass.parseNull(gmUserVO.getLname());
      String struserName = strfirstName.concat(strLastName);

      hmExpParam.put("REPEMAIL", strUserId);
      hmExpParam.put("FILENAME", filePath);
      hmExpParam.put("USERNAME", struserName);

      // save the file to the server
      File outFile = new File(filePath);
      if (!outFile.exists()) {
        new File(outFile.getParent()).mkdirs();
      }
      OutputStream outputStream = new FileOutputStream(outFile);

      int read = 0;
      byte[] bytes = new byte[1024];

      while ((read = fileInputStream.read(bytes)) != -1) {
        outputStream.write(bytes, 0, read);
      }
      outputStream.flush();
      outputStream.close();
      fileInputStream.close();
      
      GmWSUtil gmWSUtil = new GmWSUtil();
      GmCRMFileUploadBean gmFileUploadDataBean = new GmCRMFileUploadBean();
      
      HashMap hmInput = new HashMap();
      
      hmInput.put("FILENAME", contentDispositionHeader.getFileName());
      hmInput.put("REFID", strRefID);
      hmInput.put("REFTYPE", strRefType);
      hmInput.put("REFGROUP", strRefGrp);
      hmInput.put("USERID", strUserId);
      
      
      hmInput = gmFileUploadDataBean.saveUploadInfo(hmInput);
      
    } catch (Exception e) {
      // log.debug(e.getMessage());
      log.debug("Coming inside exception part");
      hmExpParam.put("EXCEPTION", e.getMessage());
      gmFileUploadBean.sendFileUploadExceptionEmail(hmExpParam);
      throw new AppError("", "20642", 'E');
    }
    fileStatus = fileStatus + filePath;
    return fileStatus;
  }
  
  
}
