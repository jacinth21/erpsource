package com.globus.webservice.crm.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.bean.GmCRMTrainingBean;
import com.globus.webservice.crm.valueobject.GmCRMTrainingListVO;
import com.globus.webservice.crm.valueobject.GmCRMTrainingVO;
import com.globus.webservice.crm.valueobject.GmCRMListVO;

@Path("crmtraing")
public class GmCRMTrainingResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @POST
  @Path("traininginfo")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listTrainingInfo(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMTrainingVO gmCRMTrainingVO = new GmCRMTrainingVO();
    GmCRMTrainingListVO gmCRMTraininglist = new GmCRMTrainingListVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMTrainingBean gmCRMTrainingBean = new GmCRMTrainingBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMTrainingListVO gmCRMTrainingListVO = mapper.readValue(strInput, GmCRMTrainingListVO.class);
    ArrayList alReturn = new ArrayList();
    List<GmCRMTrainingListVO> listGmCRMTrainingVO = new ArrayList<GmCRMTrainingListVO>();

    validateToken(gmCRMTrainingListVO.getToken());

    log.debug("User ID>>>>>" + gmUserVO.getUserid());

    HashMap hmInput = new HashMap();

    gmWSUtil.parseJsonToVO(strInput, gmCRMTraininglist);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMTraininglist);

    alReturn = GmCommonClass.parseNullArrayList((gmCRMTrainingBean.fetchTrainingReport(hmInput)));

    log.debug("alRerutn>>>" + alReturn);

    listGmCRMTrainingVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMTraininglist,
            gmCRMTraininglist.getMappingProperties());

    log.debug("listGmCRMSurgeonlistVO>>>>" + listGmCRMTrainingVO);
    gmCRMListVO.setGmCRMTrainingListVO(listGmCRMTrainingVO);
    return gmCRMListVO;
  }

  @POST
  @Path("savetraining")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMTrainingListVO saveTraining(String strInput) throws Exception, JsonParseException,
      JsonMappingException, IOException {

    GmCRMTrainingVO gmCRMTrainingVO = new GmCRMTrainingVO();
    GmCRMTrainingListVO gmCRMTraininglist = new GmCRMTrainingListVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMTrainingBean gmCRMTrainingBean = new GmCRMTrainingBean();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMTrainingVO gmCRMTraining = mapper.readValue(strInput, GmCRMTrainingVO.class);
    validateToken(gmCRMTraining.getToken());
    String strPartyId = gmCRMTraining.getUserid();
    GmCRMTrainingListVO[] arrGmOrderVO = gmCRMTraining.getArrGmOrderVO();

    ArrayList alreturn =
        GmCommonClass.parseNullArrayList(gmCRMTrainingBean.populateTrainingFromJSON(arrGmOrderVO,
            gmCRMTraininglist, strPartyId));
    int size = alreturn.size();
    gmCRMTrainingBean.saveTraining(alreturn);
    return gmCRMTraininglist;

  }
}
