package com.globus.webservice.crm.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmUsageBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.bean.GmCRMSurgicalActivityBean;
import com.globus.webservice.crm.valueobject.GmCRMCalendarVO;
import com.globus.webservice.crm.valueobject.GmCRMListVO;
import com.globus.webservice.crm.valueobject.GmCRMSystemDetailsVO;

@Path("surgical")
public class GmCRMSurgicalActivityResource extends GmResource {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	/**
	 * This method used to fetch CRM Surgeon System Details. In Surgical
	 * Activity Module, the Fn Retrieves the System List when clicking the Count
	 * Event which placed in the calendar. Inputs are token, partyid,orderdate
	 */
	@POST
	@Path("sysdetails")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmCRMListVO listSurgeonSysDetailsInfo(String strInput)
			throws AppError, JsonParseException, JsonMappingException,
			IOException {
		log.debug("strInput" + strInput);

		GmWSUtil gmWSUtil = new GmWSUtil();
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		GmCRMSurgicalActivityBean gmCRMSurgicalActivityBean = new GmCRMSurgicalActivityBean();
		GmCRMListVO gmCRMListVO = new GmCRMListVO();
		ObjectMapper mapper = new ObjectMapper();
		GmCRMSystemDetailsVO gmCRMSystemDetailsVO = mapper.readValue(strInput,
				GmCRMSystemDetailsVO.class);
		ArrayList alReturn = new ArrayList();
		List<GmCRMSystemDetailsVO> listGmCRMSystemDetailsVO = new ArrayList<GmCRMSystemDetailsVO>();

		validateToken(gmCRMSystemDetailsVO.getToken());
		String strpartyid = gmCRMSystemDetailsVO.getPartyid();
		HashMap hmInput = new HashMap();

		gmWSUtil.parseJsonToVO(strInput, gmCRMSystemDetailsVO);
		hmInput = gmWSUtil.getHashMapFromVO(gmCRMSystemDetailsVO);
		
		HashMap hmParam =
	            GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
	                (new GmSalesDashReqVO())));
	    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
	    hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
	    hmInput.put("HMPARAM", hmParam);
	    hmInput.put("USERDEPTID", gmUserVO.getDeptid());   
	    
		alReturn = gmCRMSurgicalActivityBean.fetchSysDetailsInfo(hmInput);
		listGmCRMSystemDetailsVO = gmWSUtil.getVOListFromHashMapList(alReturn,
				gmCRMSystemDetailsVO,
				gmCRMSystemDetailsVO.getMappingProperties());

		gmCRMListVO.setGmCRMSystemDetailsVO((listGmCRMSystemDetailsVO));
		log.debug("gmCRMListVO>>>>" + gmCRMListVO);
		return gmCRMListVO;

	}

	/**
	 * This method used to fetch CRM Surgeon date wise DO Counts. In Surgical
	 * Activity Module, the Fn Retrieves the DO List Report when clicking the
	 * Calendar Icon. Inputs are token, partyid, fromdt, todt
	 */
	@POST
	@Path("infoclr")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmCRMListVO listSurgeonCalendarInfo(String strInput)
			throws AppError, JsonParseException, JsonMappingException,
			IOException {
		log.debug("strInput" + strInput);

		GmWSUtil gmWSUtil = new GmWSUtil();
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		GmCRMSurgicalActivityBean gmCRMSurgicalActivityBean = new GmCRMSurgicalActivityBean();
		GmCRMListVO gmCRMListVO = new GmCRMListVO();
		ObjectMapper mapper = new ObjectMapper();
		GmCRMCalendarVO gmCRMCalendarVO = mapper.readValue(strInput,
				GmCRMCalendarVO.class);
		ArrayList alReturn = new ArrayList();
		List<GmCRMCalendarVO> listGmCRMCalendarVO = new ArrayList<GmCRMCalendarVO>();

		validateToken(gmCRMCalendarVO.getToken());
		String strpartyid = gmCRMCalendarVO.getPartyid();
		HashMap hmInput = new HashMap();

		gmWSUtil.parseJsonToVO(strInput, gmCRMCalendarVO);
		hmInput = gmWSUtil.getHashMapFromVO(gmCRMCalendarVO);
		
		HashMap hmParam =
	            GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
	                (new GmSalesDashReqVO())));
	    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
	    hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
	    hmInput.put("HMPARAM", hmParam);
	    hmInput.put("USERDEPTID", gmUserVO.getDeptid());   
		
		alReturn = gmCRMSurgicalActivityBean.fetchCalendarInfo(hmInput);
		listGmCRMCalendarVO = gmWSUtil.getVOListFromHashMapList(alReturn,
				gmCRMCalendarVO, gmCRMCalendarVO.getMappingProperties());
		gmCRMListVO.setGmCRMCalendarVO((listGmCRMCalendarVO));
		log.debug("gmCRMListVO>>>>" + gmCRMListVO);
		return gmCRMListVO;
	}


}
