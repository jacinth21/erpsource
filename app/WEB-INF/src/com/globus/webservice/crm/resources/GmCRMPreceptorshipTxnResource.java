package com.globus.webservice.crm.resources;

import java.io.IOException;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.bean.GmCRMPreceptorshipTxnBean;
import com.globus.webservice.crm.valueobject.GmCRMPSCaseReqMapVO;
import com.globus.webservice.crm.valueobject.GmCRMPSCaseVO;

@Path("pstxn")
public class GmCRMPreceptorshipTxnResource extends GmResource {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * This method used to Create Case Request in Preceptorship module. Save the data from updated
   * model object in front-end as a JSONDATA.
   */

  @POST
  @Path("casereq")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMPSCaseVO saveCase(String strInput) throws Exception, AppError, JsonParseException,
      JsonMappingException, IOException {

    log.debug("strInput" + strInput);
    HashMap<String, String> hmEmailParam = new HashMap<String, String>();
    String strCaseReqInputId = "";

    ObjectMapper mapper = new ObjectMapper();
    HashMap<String, String> hmInput = new HashMap<String, String>();

    // Converting JSON input string into VO
    GmCRMPSCaseVO gmCRMPSCaseVO = mapper.readValue(strInput, GmCRMPSCaseVO.class);
    strCaseReqInputId = gmCRMPSCaseVO.getCasereqid();
    // Validate whether the user token active?
    validateToken(gmCRMPSCaseVO.getToken());

    // Declaring bean object with VO.It will pass following values
    // compDateFmt,compTimeZone,compId,compPlantId,complangid respect to company.
    GmCRMPreceptorshipTxnBean gmCRMPreceptorshipTxnBean =
        new GmCRMPreceptorshipTxnBean(gmCRMPSCaseVO);

    gmCRMPSCaseVO.setUserid(gmUserVO.getUserid());

    hmInput.put("JSONDATA", strInput);
    hmInput.put("USERID", gmUserVO.getUserid());

    String strCasereqid = gmCRMPreceptorshipTxnBean.saveCaseInfoJSON(hmInput);

    gmCRMPSCaseVO.setCasereqid(strCasereqid);
    log.debug("gmCRMPSCaseVO>>>>" + gmCRMPSCaseVO);
    
    if (strCaseReqInputId.equals("")) {
        hmEmailParam.put("CASEREQID", strCasereqid);
        hmEmailParam.put("VMFILE", "GmCRMCaseRequest.vm");
        hmEmailParam.put("EMAILPROPERTY", "GmCRMPSCaseReqEventCreate");
        gmCRMPreceptorshipTxnBean.sendCaseRequestEmail(hmEmailParam);
      }
    

    return gmCRMPSCaseVO;

  }

  /**
   * This method used to link/unlink Case Request into host case. Save the data from updated model
   * object in front-end as a JSONDATA.
   */

  @POST
  @Path("casereqmap")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMPSCaseReqMapVO saveCaseReqMapping(String strInput) throws Exception, AppError,
      JsonParseException, JsonMappingException, IOException {

    log.debug("strInput" + strInput);

    ObjectMapper mapper = new ObjectMapper();
    HashMap<String, String> hmInput = new HashMap<String, String>();

    // Converting JSON input string into VO
    GmCRMPSCaseReqMapVO gmCRMPSCaseMapVO = mapper.readValue(strInput, GmCRMPSCaseReqMapVO.class);

    // Validate whether the user token active?
    validateToken(gmCRMPSCaseMapVO.getToken());

    // Declaring bean object with VO.It will pass following values
    // compDateFmt,compTimeZone,compId,compPlantId,complangid respect to company.
    GmCRMPreceptorshipTxnBean gmCRMPreceptorshipTxnBean =
        new GmCRMPreceptorshipTxnBean(gmCRMPSCaseMapVO);

    gmCRMPSCaseMapVO.setUserid(gmUserVO.getUserid());

    hmInput.put("JSONDATA", strInput);
    hmInput.put("USERID", gmUserVO.getUserid());

    gmCRMPreceptorshipTxnBean.saveCaseRequestMappingJSON(hmInput);

    return gmCRMPSCaseMapVO;

  }

  /**
   * This method used to Create Send Email in Preceptorship module. email send at while create case
   * request
   */

  @POST
  @Path("casereqmail")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMPSCaseVO sendCaseReqMail(String strInput) throws Exception, AppError,
      JsonParseException, JsonMappingException, IOException {

    HashMap<String, String> hmEmailParam = new HashMap<String, String>();
    String strCaseReqInputId = "";

    ObjectMapper mapper = new ObjectMapper();

    // Converting JSON input string into VO
    GmCRMPSCaseVO gmCRMPSCaseVO = mapper.readValue(strInput, GmCRMPSCaseVO.class);
    strCaseReqInputId = gmCRMPSCaseVO.getCasereqid();
    // Validate whether the user token active?
    validateToken(gmCRMPSCaseVO.getToken());

    // Declaring bean object with VO.It will pass following values
    // compDateFmt,compTimeZone,compId,compPlantId,complangid respect to company.
    GmCRMPreceptorshipTxnBean gmCRMPreceptorshipTxnBean =
        new GmCRMPreceptorshipTxnBean(gmCRMPSCaseVO);

    gmCRMPSCaseVO.setUserid(gmUserVO.getUserid());

    String strCasereqid = gmCRMPSCaseVO.getCasereqid();

    gmCRMPSCaseVO.setCasereqid(strCasereqid);
    log.debug("gmCRMPSCaseVO>>>>" + gmCRMPSCaseVO);

    // send case request email
    if (!strCaseReqInputId.equals("")) {
      hmEmailParam.put("CASEREQID", strCasereqid);
      hmEmailParam.put("VMFILE", "GmCRMCaseRequest.vm");
      hmEmailParam.put("EMAILPROPERTY", "GmCRMPSCaseReqEventCreate");
      gmCRMPreceptorshipTxnBean.sendCaseRequestEmail(hmEmailParam);
    }
    return gmCRMPSCaseVO;

  }
}
