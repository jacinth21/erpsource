package com.globus.webservice.crm.resources;

import java.util.ArrayList;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmExchangeManager;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.bean.GmCRMMailBean;
import com.globus.webservice.crm.valueobject.GmCRMMailVO;

/**
 * @author jgurunathan
 * 
 */
@Path("crmmail")
public class GmCRMMailResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * This method used for event creation and email send to the respective email group and user.
   * 
   * @param strInput
   * @return
   * @throws Exception
   */
  @POST
  @Path("activity")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMMailVO sendMail(String strInput) throws AppError, Exception {

    HashMap hmEmailParams = new HashMap();
    HashMap hmParams = new HashMap();
    HashMap hmEventDetails = new HashMap();
    HashMap hmEmailDetails = new HashMap();
    HashMap hmSenderDetails = new HashMap();
    HashMap hmTemplate = new HashMap();
    ArrayList alParticipants = new ArrayList();
    ArrayList alSecurityEventUserEmails = new ArrayList();
    StringBuffer sbContent = new StringBuffer();
    GmEmailProperties gmEmailProperties = new GmEmailProperties();
    GmExchangeManager gmExchangeManager = new GmExchangeManager();
    GmEmailProperties gmInviteEmailProperties = new GmEmailProperties();

    String strFromMail = "";
    String strSubject = "";
    String strItemId = "";
    String strActcategory = "";
    String strTempFileNm = "";
    String strEmailTempCreate = "";
    String strEmailTempConfirm = "";
    String strRecipients = "";
    String strSender = "";
    String strInviteRecipients = "";
    String strAllInvtRecipients = "";


    boolean bolResult = true;
    GmCRMMailVO gmCRMMailVO = new GmCRMMailVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMMailBean gmCRMMailBean = new GmCRMMailBean();

    gmWSUtil.parseJsonToVO(strInput, gmCRMMailVO);

    validateToken(gmCRMMailVO.getToken());

    gmCRMMailVO.setUserid(gmUserVO.getUserid());

    strActcategory = gmCRMMailVO.getActcategory();

    if (strActcategory.equals("105266")) { // 105266-Physician visit (Email,Event)
      strTempFileNm = "GmCRMPhysicianVisit.vm";
      strEmailTempCreate = "GmCRMPhysicianEventCreate";
      strEmailTempConfirm = "GmCRMPhysicianEventConfirm";
      gmExchangeManager.setMailProperties("GMI_CRM_MAILBOX", "CRM_EVENT_PASSWORD");
      strRecipients = GmCommonClass.getString("CRM_EMAIL_GROUP");
      strSender = GmCommonClass.getString("CRM_EVENT_USER");
    } else if (strActcategory.equals("105272")) { // 105272-Preceptorship Host Case(Email,Event)
      strTempFileNm = "GmCRMHostCase.vm";
      strEmailTempCreate = "GmCRMPSHostEventCreate";
      strEmailTempConfirm = "GmCRMPSHostEventConfirm";
      gmExchangeManager.setMailProperties("GMI_HOSTCASE_MAILBOX", "CRM_HOST_EVENT_PASSWORD");
      strRecipients = GmCommonClass.getString("CRM_HOST_EMAIL_GROUP");
      strSender = GmCommonClass.getString("CRM_HOST_EVENT_USER");
    } else { // 105267-Sales Call (Event)
      strTempFileNm = "GmCRMSalesCall.vm";
      strEmailTempCreate = "GmCRMSalesCallCreate";
      strEmailTempConfirm = "GmCRMPSalesCallConfirm";
      gmExchangeManager.setMailProperties("GMI_SALESCALL_MAILBOX", "CRM_SALESCALL_EVENT_PASSWORD");
      strRecipients = GmCommonClass.getString("CRM_SALESCALL_EMAIL_GROUP");
      strSender = GmCommonClass.getString("CRM_SALESCALL_EVENT_USER");
    }


    hmSenderDetails = gmCRMMailBean.getUserDetails(gmCRMMailVO.getUserid());
    strFromMail = hmSenderDetails.get("NAME") + "<" + hmSenderDetails.get("EMAILID") + ">";
    hmEmailParams.put("strFrom", strFromMail);

    if (!(gmCRMMailVO.getIsevent()).equals("")) {

      hmEventDetails = gmCRMMailBean.getEventDetails(gmCRMMailVO.getActid());

      hmParams.put("ACTID", gmCRMMailVO.getActid());
      hmParams.put("ACTCATEGORY", gmCRMMailVO.getActcategory());
      hmEmailDetails = gmCRMMailBean.getPhysicianVisitEmailDtl(hmParams);
      alParticipants = (ArrayList) hmEmailDetails.get("ALPARTICIPANT");

      for (int i = 0; i < alParticipants.size(); i++) {
        HashMap hmParticipant = new HashMap();
        hmParticipant = GmCommonClass.parseNullHashMap((HashMap) alParticipants.get(i));
        strRecipients += "," + hmParticipant.get("EMAILID");
      }
      log.debug("strRecipients ------" + strRecipients);
      
      
      // PC-4769 - Ability to map users to receive VIP visit notification based on Division
      alSecurityEventUserEmails = GmCommonClass.parseNullArrayList((ArrayList) hmEmailDetails.get("ALSECURITYEVENTUSEREMAILS"));
      
      for (int i = 0; i < alSecurityEventUserEmails.size(); i++) {
          HashMap hmParticipant = new HashMap();
          hmParticipant = GmCommonClass.parseNullHashMap((HashMap) alSecurityEventUserEmails.get(i));
          strInviteRecipients += "," + GmCommonClass.parseNull((String)hmParticipant.get("EMAILID"));
        }
      
      strAllInvtRecipients += strRecipients;
      
      if(!strInviteRecipients.isEmpty())
      {
    	  strAllInvtRecipients += strInviteRecipients;
      }
      
      log.debug("strRecipients_with_SecurityEventUSer-------" + strAllInvtRecipients);

      hmEmailDetails.put("CRMFILEPATH", GmCommonClass.getString("URL_CRM_FILE_SERVER"));
      hmEmailDetails.put("CRMPORTALPATH", GmCommonClass.getString("URL_CRM_PORTAL_SERVER"));

      hmTemplate.put("TEMPLATE_NAME", strTempFileNm);

      hmTemplate.put("TEMPLATE_SUB_DIR", "prodmgmnt/templates");
      hmTemplate.put("DATA_MAP", hmEmailDetails);

      strSubject = hmEventDetails.get("ACTNM").toString();
      String strMessageBody = GmCommonClass.getXmlGridData(hmTemplate);
      if (strActcategory.equals("105272")) { 
    	  hmEventDetails.put("MSGTABLE", strMessageBody);
      }
      log.debug(strMessageBody);
      String strStartTime = hmEventDetails.get("FRMDTTIME") + " GMT";
      String strEndTime = hmEventDetails.get("TODTTIME") + " GMT";
      String strEventItemId = GmCommonClass.parseNull((String) hmEventDetails.get("EVTITEMID"));
      String strCategory = GmCommonClass.parseNull((String) hmEventDetails.get("CATEGORY"));
      String strMailStatus = gmCRMMailVO.getMailstatus();
      gmEmailProperties.setNotifyfl(gmCRMMailVO.getNotifyfl());
      gmEmailProperties.setMimeType("HTML");
      gmEmailProperties.setSender(strSender);
      gmEmailProperties.setSubject(strSubject);
      gmEmailProperties.setRecipients(strRecipients);
      gmEmailProperties.setStartTime(strStartTime);
      gmEmailProperties.setEndime(strEndTime);
      gmEmailProperties.setMessage(strMessageBody);
      gmEmailProperties.setEventItemId(strEventItemId);
      gmEmailProperties.setCategory(strCategory);
      gmEmailProperties.setLocation("TBD");
      log.debug("gmEmailProperties >>>>>>>>>>>" + gmEmailProperties);
      
      // PC-4769 - Ability to map users to receive VIP visit notification based on Division
      gmInviteEmailProperties.setNotifyfl(gmCRMMailVO.getNotifyfl());
      gmInviteEmailProperties.setMimeType("HTML");
      gmInviteEmailProperties.setSender(strSender);
      gmInviteEmailProperties.setSubject(strSubject);
      gmInviteEmailProperties.setRecipients(strAllInvtRecipients);
      gmInviteEmailProperties.setStartTime(strStartTime);
      gmInviteEmailProperties.setEndime(strEndTime);
      gmInviteEmailProperties.setMessage(strMessageBody);
      gmInviteEmailProperties.setEventItemId(strEventItemId);
      gmInviteEmailProperties.setCategory(strCategory);
      gmInviteEmailProperties.setLocation("TBD");
      log.debug("gmInviteEmailProperties >>>>>>>>>>>" + gmInviteEmailProperties);

      if (strMailStatus.equals("1") || (!strMailStatus.equals("4") && strEventItemId.equals(""))) {
        // strStatus = "REQUEST CREATE";
        if (!(gmCRMMailVO.getEventfl()).equals("")) {
          strItemId = gmExchangeManager.sendMeetingRequest(gmInviteEmailProperties);
        }
        hmParams.put("ITEMID", strItemId);
        hmParams.put("USERID", gmUserVO.getUserid());
        gmCRMMailBean.saveEventItemId(hmParams);
        bolResult = GmCommonClass.parseNull(strItemId).equals("") ? false : true;
        // Email
        if (!(gmCRMMailVO.getEmailfl()).equals("")) {
          gmCRMMailBean.sendNotification(strEmailTempCreate, gmEmailProperties, hmEventDetails);
        }
      } else if (strMailStatus.equals("2")) {
        // strStatus = "UPDATED";
        if (!(gmCRMMailVO.getEventfl()).equals("")) {
          strItemId = gmExchangeManager.updateMeetingRequest(gmInviteEmailProperties);
        }
        bolResult = GmCommonClass.parseNull(strItemId).equals("") ? false : true;
      } else if (strMailStatus.equals("3")) {
        // strStatus = "CONFIRMED";
        if (!(gmCRMMailVO.getEventfl()).equals("")) {
          strItemId = gmExchangeManager.confirmMeetingRequest(gmInviteEmailProperties);
        }
        bolResult = GmCommonClass.parseNull(strItemId).equals("") ? false : true;
        // Email
        if (!(gmCRMMailVO.getEmailfl()).equals("")) {
          gmCRMMailBean.sendNotification(strEmailTempConfirm, gmEmailProperties, hmEventDetails);
        }
      } else if (strMailStatus.equals("4") && !strEventItemId.equals("")) {
        // strStatus = "CANCELLED";
        if (!(gmCRMMailVO.getEventfl()).equals("")) {
          if (!(gmExchangeManager.cancelMeetingRequest(gmInviteEmailProperties)))
            bolResult = false;
        }
      }

      log.debug("strItemId=" + strItemId);
      log.debug("hmEmailParams >>>> " + hmEmailParams);
    }

    if (bolResult) {
      gmCRMMailVO.setMstatus("success");
    } else {
      gmCRMMailVO.setMstatus("fail");
    }

    gmCRMMailVO.setToken("");

    return gmCRMMailVO;
  }


}
