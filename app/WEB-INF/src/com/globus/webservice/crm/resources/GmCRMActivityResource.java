package com.globus.webservice.crm.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.annotation.JacksonFeatures;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmUsageBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.bean.GmCRMActivityBean;
import com.globus.webservice.crm.bean.GmCRMCalendarInterface;
import com.globus.webservice.crm.bean.GmCriteriaBean;
import com.globus.webservice.crm.valueobject.GmCRMActivityAttrVO;
import com.globus.webservice.crm.valueobject.GmCRMActivityPartyVO;
import com.globus.webservice.crm.valueobject.GmCRMActivityVO;
import com.globus.webservice.crm.valueobject.GmCRMDeviceDetailVO;
import com.globus.webservice.crm.valueobject.GmCRMListVO;
import com.globus.webservice.crm.valueobject.GmCRMLogVO;
import com.globus.webservice.crm.valueobject.GmCriteriaVO;
import com.globus.webservice.crm.valueobject.GmProjectBasicVO;

@Path("crmactivity")
public class GmCRMActivityResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());


  @POST
  @Path("info")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  @JacksonFeatures(serializationEnable = SerializationFeature.WRAP_ROOT_VALUE,
      deserializationEnable = DeserializationFeature.UNWRAP_ROOT_VALUE)
  public GmCRMListVO listActivityInfo(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {
    log.debug("entered resource");
    GmCRMActivityVO gmCRMActivitylistVO = new GmCRMActivityVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    GmCRMActivityBean gmCRMActivityBean = new GmCRMActivityBean();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMActivityVO gmCRMActivityVO = mapper.readValue(strInput, GmCRMActivityVO.class);
    ArrayList alReturn = new ArrayList();
    List<GmCRMActivityVO> listGmCRMActivitylistVO = new ArrayList<GmCRMActivityVO>();
    validateToken(gmCRMActivityVO.getToken());
    String stractid = gmCRMActivityVO.getActid();
    String struserid = gmUserVO.getUserid();
    log.debug(stractid);
    gmCRMActivitylistVO.setUserid(gmUserVO.getUserid());

    log.debug("User ID>>>>>" + gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();

    gmWSUtil.parseJsonToVO(strInput, gmCRMActivitylistVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMActivitylistVO);

    if (!stractid.equals("")) {
      String strReturnJSON = gmCRMActivityBean.fetchActivityDetailJSON(stractid, struserid);
      log.debug(strReturnJSON);
      gmCRMActivitylistVO = mapper.readValue(strReturnJSON, GmCRMActivityVO.class);
      listGmCRMActivitylistVO.add(gmCRMActivitylistVO);
    } else {

      // For Override the Access Level of Associate Sales REP to Sales Rep (Associate Sales REP
      // checks with account id)
      HashMap hmParam =
          GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
              (new GmSalesDashReqVO())));

      hmInput.put("HMPARAM", hmParam);
      hmInput.put("USERDEPTID", gmUserVO.getDeptid());
      hmInput.put("HMPARAM",
          GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam)));
      alReturn = GmCommonClass.parseNullArrayList((gmCRMActivityBean.fetchActivityList(hmInput)));


      listGmCRMActivitylistVO =
          gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMActivitylistVO,
              gmCRMActivitylistVO.getMappingProperties());

    }
    if (listGmCRMActivitylistVO.size() > 0)
      gmCRMListVO.setGmCRMActivityVO(listGmCRMActivitylistVO);
    else
      gmCRMListVO = null;
    return gmCRMListVO;

  }

  /**
   * This Resource method is used for Physician Visit, Sales Call and Perceptorship Calendar view
   * 
   * @param strInput
   * @return
   * @throws AppError
   * @throws JsonParseException
   * @throws JsonMappingException
   * @throws IOException
   */
  @POST
  @Path("scheduler")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_JSON})
  @JacksonFeatures(serializationEnable = SerializationFeature.WRAP_ROOT_VALUE,
      deserializationEnable = DeserializationFeature.UNWRAP_ROOT_VALUE)
  public GmCRMListVO loadScheduler(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {
    log.debug("Scheduler resource");
    String strActCategory = "";
    HashMap hmInput = new HashMap();
    ArrayList alReturn = new ArrayList();

    List<GmCRMActivityVO> listGmCRMActivitylistVO = new ArrayList<GmCRMActivityVO>();

    ObjectMapper mapper = new ObjectMapper();

    GmCRMActivityVO gmCRMActivitylistVO = new GmCRMActivityVO();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();


    GmCRMActivityBean gmCRMActivityBean = new GmCRMActivityBean();
    GmUsageBean gmUsageBean = new GmUsageBean(gmCRMActivitylistVO);
    GmCRMActivityVO gmCRMActivityVO = mapper.readValue(strInput, GmCRMActivityVO.class);

    validateToken(gmCRMActivityVO.getToken());
    gmCRMActivitylistVO.setUserid(gmUserVO.getUserid());
    strActCategory = gmCRMActivityVO.getActcategory();


    GmCRMCalendarInterface gmCRMCalendarInterface =
        (GmCRMCalendarInterface) GmCommonClass.getSpringBeanClass("xml/CRM_Xml_Beans.xml",
            strActCategory);

    gmWSUtil.parseJsonToVO(strInput, gmCRMActivitylistVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMActivitylistVO);

    // For Override the Access Level of Associate Sales REP to Sales Rep
    // (Associate Sales REP
    // checks with account id)
    HashMap hmParam =
        GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
            (new GmSalesDashReqVO())));

    hmInput.put("HMPARAM", hmParam);
    hmInput.put("USERDEPTID", gmUserVO.getDeptid());
    hmInput.put("USERCOMPID", gmUserVO.getCompanyid());

    log.debug(hmInput);
    // For Usage Tracking
    hmInput.put("SCREENNM", strActCategory);
    gmUsageBean.saveUsageTracking(hmInput);


    alReturn =
        GmCommonClass.parseNullArrayList((gmCRMCalendarInterface.loadCalendarViewData(hmInput)));

    listGmCRMActivitylistVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMActivitylistVO,
            gmCRMActivitylistVO.getMappingProperties());

    if (listGmCRMActivitylistVO.size() > 0)
      gmCRMListVO.setGmCRMActivityVO(listGmCRMActivitylistVO);
    else
      gmCRMListVO = null;
    return gmCRMListVO;

  }

  @POST
  @Path("infoactivitysync")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMDeviceDetailVO listActivitySyncInfo(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMDeviceDetailVO gmCRMDeviceDetailVO = new GmCRMDeviceDetailVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    gmWSUtil.parseJsonToVO(strInput, gmCRMDeviceDetailVO);
    String strUpdate = gmCRMDeviceDetailVO.getUpdate();
    log.debug("strUpdate>>>>" + strUpdate);
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmCRMDeviceDetailVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmCRMDeviceDetailVO, hmValidate);


    GmCRMActivityVO gmCRMActivitylistVO = new GmCRMActivityVO();
    GmCRMActivityBean gmCRMActitivyBean = new GmCRMActivityBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmCRMActivityVO> listGmCRMActivityVO = new ArrayList<GmCRMActivityVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmCRMDeviceDetailVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getUuid()));


    alResult =
        GmCommonClass.parseNullArrayList(gmCRMActitivyBean.fetchActivitySyncReport(hmParams));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmCRMActivityVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmCRMActivitylistVO,
            gmCRMActivitylistVO.getMappingProperties());
    // Set the page header detail from first hashmap of list.
    gmCRMDeviceDetailVO.populatePageProperties(alResult);
    gmCRMDeviceDetailVO.setReftype("10306170");// Part Number sync type
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", gmCRMDeviceDetailVO.getReftype());
    hmParams.put("PAGESIZE", gmCRMDeviceDetailVO.getPagesize());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmCRMDeviceDetailVO.setArrgmactivityvo(listGmCRMActivityVO);

    return gmCRMDeviceDetailVO;

  }

  @POST
  @Path("projectsync")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMDeviceDetailVO listProjectSync(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {

    GmCRMDeviceDetailVO gmCRMDeviceDetailVO = new GmCRMDeviceDetailVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    gmWSUtil.parseJsonToVO(strInput, gmCRMDeviceDetailVO);
    String strUpdate = gmCRMDeviceDetailVO.getUpdate();
    log.debug("strUpdate>>>>" + strUpdate);
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmCRMDeviceDetailVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmCRMDeviceDetailVO, hmValidate);


    GmProjectBasicVO gmCRMProjectBasicVO = new GmProjectBasicVO();
    GmCRMActivityBean gmCRMActitivyBean = new GmCRMActivityBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmProjectBasicVO> listGmCRMProjectBasicVO = new ArrayList<GmProjectBasicVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmCRMDeviceDetailVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getUuid()));

    alResult = GmCommonClass.parseNullArrayList(gmCRMActitivyBean.fetchProjectSyncData(hmParams));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmCRMProjectBasicVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmCRMProjectBasicVO,
            gmCRMProjectBasicVO.getMappingProperties());
    // Set the page header detail from first hashmap of list.
    gmCRMDeviceDetailVO.populatePageProperties(alResult);
    gmCRMDeviceDetailVO.setReftype("103109");// Project sync type
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", gmCRMDeviceDetailVO.getReftype());
    hmParams.put("PAGESIZE", gmCRMDeviceDetailVO.getPagesize());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmCRMDeviceDetailVO.setArrgmprojectvo(listGmCRMProjectBasicVO);

    return gmCRMDeviceDetailVO;

  }

  @POST
  @Path("actnotessync")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMDeviceDetailVO listActivityNotesSyncInfo(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMDeviceDetailVO gmCRMDeviceDetailVO = new GmCRMDeviceDetailVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    gmWSUtil.parseJsonToVO(strInput, gmCRMDeviceDetailVO);
    parseInputAndValidateToken(strInput, gmCRMDeviceDetailVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmCRMDeviceDetailVO, hmValidate);


    GmCRMLogVO gmCRMLogVO = new GmCRMLogVO();
    GmCRMActivityBean gmCRMActitivyBean = new GmCRMActivityBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmCRMLogVO> listGmCRMLogVO = new ArrayList<GmCRMLogVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmCRMDeviceDetailVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getUuid()));

    alResult = GmCommonClass.parseNullArrayList(gmCRMActitivyBean.fetchActivityNotesSync(hmParams));
    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmCRMLogVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmCRMLogVO, gmCRMLogVO.getMappingProperties());
    // Set the page header detail from first hashmap of list.
    gmCRMDeviceDetailVO.populatePageProperties(alResult);
    gmCRMDeviceDetailVO.setReftype("10306211");// Activity Notes sync type
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", gmCRMDeviceDetailVO.getReftype());
    hmParams.put("PAGESIZE", gmCRMDeviceDetailVO.getPagesize());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmCRMDeviceDetailVO.setArrgmlogvo(listGmCRMLogVO);

    return gmCRMDeviceDetailVO;

  }

  @POST
  @Path("infoactivitypartysync")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMDeviceDetailVO listActivityPartySyncInfo(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMDeviceDetailVO gmCRMDeviceDetailVO = new GmCRMDeviceDetailVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    gmWSUtil.parseJsonToVO(strInput, gmCRMDeviceDetailVO);
    String strUpdate = gmCRMDeviceDetailVO.getUpdate();
    log.debug("strUpdate>>>>" + strUpdate);
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmCRMDeviceDetailVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmCRMDeviceDetailVO, hmValidate);

    GmCRMActivityPartyVO gmCRMActivityPartyVO = new GmCRMActivityPartyVO();
    GmCRMActivityBean gmCRMActitivyBean = new GmCRMActivityBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmCRMActivityPartyVO> listGmCRMActivityPartyVO = new ArrayList<GmCRMActivityPartyVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmCRMDeviceDetailVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getUuid()));

    if (strUpdate.equals("")) {
      alResult =
          GmCommonClass
              .parseNullArrayList(gmCRMActitivyBean.fetchActivityPartySyncReport(hmParams));
    } else {
      alResult =
          GmCommonClass.parseNullArrayList(gmCRMActitivyBean
              .fetchActivityPartyUpdateReport(hmParams));
    }


    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmCRMActivityPartyVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmCRMActivityPartyVO,
            gmCRMActivityPartyVO.getMappingProperties());
    // Set the page header detail from first hashmap of list.
    gmCRMDeviceDetailVO.populatePageProperties(alResult);
    gmCRMDeviceDetailVO.setReftype("10306171");// Part Number sync type
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", gmCRMDeviceDetailVO.getReftype());
    hmParams.put("PAGESIZE", gmCRMDeviceDetailVO.getPagesize());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmCRMDeviceDetailVO.setArrgmactivitypartyvo(listGmCRMActivityPartyVO);

    return gmCRMDeviceDetailVO;

  }

  @POST
  @Path("infoactivityattrsync")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMDeviceDetailVO listActivityAttrSyncInfo(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMDeviceDetailVO gmCRMDeviceDetailVO = new GmCRMDeviceDetailVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    gmWSUtil.parseJsonToVO(strInput, gmCRMDeviceDetailVO);
    String strUpdate = gmCRMDeviceDetailVO.getUpdate();
    log.debug("strUpdate>>>>" + strUpdate);
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmCRMDeviceDetailVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmCRMDeviceDetailVO, hmValidate);

    GmCRMActivityAttrVO gmCRMActivityAttrVO = new GmCRMActivityAttrVO();
    GmCRMActivityBean gmCRMActitivyBean = new GmCRMActivityBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmCRMActivityAttrVO> listGmCRMActivityAttrVO = new ArrayList<GmCRMActivityAttrVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmCRMDeviceDetailVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getUuid()));

    if (strUpdate.equals("")) {
      alResult =
          GmCommonClass.parseNullArrayList(gmCRMActitivyBean.fetchActivityAttrSyncReport(hmParams));
    } else {
      alResult =
          GmCommonClass.parseNullArrayList(gmCRMActitivyBean
              .fetchActivityAttrUpdateReport(hmParams));
    }


    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmCRMActivityAttrVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmCRMActivityAttrVO,
            gmCRMActivityAttrVO.getMappingProperties());
    // Set the page header detail from first hashmap of list.
    gmCRMDeviceDetailVO.populatePageProperties(alResult);
    gmCRMDeviceDetailVO.setReftype("10306172");// Part Number sync type
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", gmCRMDeviceDetailVO.getReftype());
    hmParams.put("PAGESIZE", gmCRMDeviceDetailVO.getPagesize());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmCRMDeviceDetailVO.setArrgmactivityattrvo(listGmCRMActivityAttrVO);

    return gmCRMDeviceDetailVO;

  }



  @POST
  @Path("criterialist")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listActivityInfoByCriteria(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMActivityVO gmCRMActivitylistVO = new GmCRMActivityVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMActivityBean gmCRMActivityBean = new GmCRMActivityBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmCRMActivityVO> listGmCRMActivitylistVO = new ArrayList<GmCRMActivityVO>();

    ArrayList alCriteria = new ArrayList();
    ArrayList alReturn = new ArrayList();

    GmCriteriaBean gmCriteriaBean = new GmCriteriaBean();
    GmCriteriaVO gmCriteriaVO = new GmCriteriaVO();

    HashMap hmCriteria = new HashMap();
    HashMap hmInput = new HashMap();

    gmWSUtil.parseJsonToVO(strInput, gmCriteriaVO);

    validateToken(gmCriteriaVO.getToken());

    gmCRMActivitylistVO.setUserid(gmUserVO.getUserid());

    log.debug("User ID>>>>>" + gmUserVO.getUserid());

    alCriteria =
        gmCriteriaBean.fetchCriteriaBy(gmCriteriaVO.getCriteriaid(), gmCriteriaVO.getCriteriatyp());

    for (int i = 0; i < alCriteria.size(); i++) {
      HashMap hmValues = GmCommonClass.parseNullHashMap((HashMap) alCriteria.get(i));
      String strKey = GmCommonClass.parseNull((String) hmValues.get("KEY"));
      String strValue = GmCommonClass.parseNull((String) hmValues.get("VALUE"));
      hmCriteria.put(strKey, strValue);
    }


    gmWSUtil.getVOFromHashMap(hmCriteria, gmCRMActivitylistVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMActivitylistVO);

    // alReturn =
    // GmCommonClass.parseNullArrayList((gmCRMActivityBean.fetchActivityFullReport(hmInput)));

    alReturn = GmCommonClass.parseNullArrayList((gmCRMActivityBean.fetchActivityList(hmInput)));

    log.debug("alRerutn>>>" + alReturn);

    listGmCRMActivitylistVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMActivitylistVO,
            gmCRMActivitylistVO.getMappingProperties());

    log.debug("listGmCRMSurgeonlistVO>>>>" + listGmCRMActivitylistVO);
    gmCRMListVO.setGmCRMActivityVO(listGmCRMActivitylistVO);
    return gmCRMListVO;
  }

  @POST
  @Path("save")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMActivityVO saveActivity(String strInput) throws Exception, AppError,
      JsonParseException, JsonMappingException, IOException {


    GmCRMActivityBean gmActivityBean = new GmCRMActivityBean();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMActivityVO gmCRMActivityVO = mapper.readValue(strInput, GmCRMActivityVO.class);

    validateToken(gmCRMActivityVO.getToken());
    HashMap hmInput = new HashMap();
    hmInput.put("JSONDATA", strInput);
    hmInput.put("USERID", gmUserVO.getUserid());
    String strActId = gmActivityBean.saveActivityDetailJSON(hmInput);
    gmCRMActivityVO.setActid(strActId);

    // if (gmCRMActivityVO.getActcategory().equals("105267")
    // && gmCRMActivityVO.getDevicefl().equals("Y")) // Salescall with Sync calendar flag
    // sendMeetingSchedule(gmCRMActivityVO);

    return gmCRMActivityVO;

  }

  @POST
  @Path("savelog")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMLogVO savePhysicianFeedback(String strInput) throws Exception, AppError,
      JsonParseException, JsonMappingException, IOException {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMActivityBean gmActivityBean = new GmCRMActivityBean();
    ObjectMapper mapper = new ObjectMapper();
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    GmCRMLogVO gmCRMLogVO = mapper.readValue(strInput, GmCRMLogVO.class);
    String strLogId = "";

    validateToken(gmCRMLogVO.getToken());

    gmCRMLogVO.setUserid(gmUserVO.getUserid());

    strLogId = gmActivityBean.saveLog(gmDBManager, gmCRMLogVO);
    log.debug("The strLogId is in resource " + strLogId);
    gmCRMLogVO.setLogid(strLogId);
    gmDBManager.commit();

    return gmCRMLogVO;

  }

  @POST
  @Path("addattr")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO addActivityAttr(String strInput) throws Exception, AppError,
      JsonParseException, JsonMappingException, IOException {

    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParams = new HashMap();
    GmCRMActivityBean gmActivityBean = new GmCRMActivityBean();
    ObjectMapper mapper = new ObjectMapper();
    ArrayList alReturn = new ArrayList();
    GmCRMActivityAttrVO gmCRMActivityAttrVO = new GmCRMActivityAttrVO();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmCRMActivityAttrVO> listGmCRMActivityAttrVO = new ArrayList<GmCRMActivityAttrVO>();
    gmCRMActivityAttrVO = mapper.readValue(strInput, GmCRMActivityAttrVO.class);
    validateToken(gmCRMActivityAttrVO.getToken());

    gmCRMActivityAttrVO.setUserid(gmUserVO.getUserid());
    hmParams = gmWSUtil.getHashMapFromVO(gmCRMActivityAttrVO);

    GmDBManager gmDBManager = new GmDBManager();
    listGmCRMActivityAttrVO.add(gmCRMActivityAttrVO);

    gmActivityBean.saveActivityAttrInfo(hmParams, gmDBManager, listGmCRMActivityAttrVO,
        gmCRMActivityAttrVO);
    gmDBManager.commit();
    gmCRMListVO.setGmCRMActivityAttrVO(listGmCRMActivityAttrVO);
    return gmCRMListVO;

  }


  @POST
  @Path("addparty")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO addActivityParty(String strInput) throws Exception, AppError,
      JsonParseException, JsonMappingException, IOException {

    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParams = new HashMap();
    GmCRMActivityBean gmActivityBean = new GmCRMActivityBean();
    ObjectMapper mapper = new ObjectMapper();
    ArrayList alReturn = new ArrayList();
    GmCRMActivityPartyVO gmCRMActivityPartyVO = new GmCRMActivityPartyVO();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmCRMActivityPartyVO> listGmCRMActivityPartyVO = new ArrayList<GmCRMActivityPartyVO>();
    gmCRMActivityPartyVO = mapper.readValue(strInput, GmCRMActivityPartyVO.class);
    validateToken(gmCRMActivityPartyVO.getToken());

    gmCRMActivityPartyVO.setUserid(gmUserVO.getUserid());
    hmParams = gmWSUtil.getHashMapFromVO(gmCRMActivityPartyVO);

    GmDBManager gmDBManager = new GmDBManager();
    listGmCRMActivityPartyVO.add(gmCRMActivityPartyVO);

    gmActivityBean.saveActivityPartyInfo(hmParams, gmDBManager, listGmCRMActivityPartyVO,
        gmCRMActivityPartyVO);
    gmDBManager.commit();
    gmCRMListVO.setGmCRMActivityPartyVO(listGmCRMActivityPartyVO);
    return gmCRMListVO;
  }
  
  /**
   * Fetch the count of restricted Days in Activity calendar for the given dates
   * 
   * @param strInput
   * @return gmCRMActivityVO
   */
  @POST
  @Path("chkblocked")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMActivityVO restrictedDays(String strInput) throws Exception, AppError,
      JsonParseException, JsonMappingException, IOException {
    GmCRMActivityBean gmActivityBean = new GmCRMActivityBean();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMActivityVO gmCRMActivityVO = mapper.readValue(strInput, GmCRMActivityVO.class);

    validateToken(gmCRMActivityVO.getToken());
    String startdt = gmCRMActivityVO.getActstartdate();
    String enddt = gmCRMActivityVO.getActenddate();
    String actid = gmCRMActivityVO.getActid();
    String strchkFL = gmActivityBean.chkRestrictedDays(startdt, enddt, actid);
    gmCRMActivityVO.setResult(strchkFL);
    return gmCRMActivityVO;

  } 
 

}
