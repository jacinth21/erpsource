package com.globus.webservice.crm.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmUsageBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.bean.GmCRMFileUploadBean;
import com.globus.webservice.crm.bean.GmCRMSurgeonBean;
import com.globus.webservice.crm.bean.GmCRMSurgeonReportBean;
import com.globus.webservice.crm.bean.GmCriteriaBean;
import com.globus.webservice.crm.valueobject.GmAdVpVO;
import com.globus.webservice.crm.valueobject.GmCRMDeviceDetailVO;
import com.globus.webservice.crm.valueobject.GmCRMFileUploadVO;
import com.globus.webservice.crm.valueobject.GmCRMListVO;
import com.globus.webservice.crm.valueobject.GmCRMLogVO;
import com.globus.webservice.crm.valueobject.GmCRMRevenueDetailVO;
import com.globus.webservice.crm.valueobject.GmCRMRevenueHeaderVO;
import com.globus.webservice.crm.valueobject.GmCRMRevenueReportVO;
import com.globus.webservice.crm.valueobject.GmCRMSummaryReportVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonActivityVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonAddressListVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonAffiliationVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonAttrVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonContactsListVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonEducationVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonProfessionalVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonRepVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonSocialVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonSurgicalVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonlistVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgicalActivityVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgicalDOVO;
import com.globus.webservice.crm.valueobject.GmCriteriaVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgicalSystemDOVO;


@Path("surgeon")
public class GmCRMSurgeonListResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @POST
  @Path("info")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listSurgeonInfo(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {

    GmCRMSurgeonlistVO gmCRMSurgeonlistVO = new GmCRMSurgeonlistVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    GmUsageBean gmusagebean = new GmUsageBean(gmCRMSurgeonlistVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMSurgeonlistVO gmCRMSurgeonVO = mapper.readValue(strInput, GmCRMSurgeonlistVO.class);
    ArrayList alReturn = new ArrayList();
    List<GmCRMSurgeonlistVO> listGmCRMSurgeonlistVO = new ArrayList<GmCRMSurgeonlistVO>();

    validateToken(gmCRMSurgeonVO.getToken());
    String strpartyid = gmCRMSurgeonVO.getPartyid();
    String strstropt = gmCRMSurgeonVO.getStropt();
    String straccesslvl = gmCRMSurgeonVO.getAccesslvl();
    String strstatus = gmCRMSurgeonVO.getStatus();


    gmCRMSurgeonlistVO.setUserid(gmUserVO.getUserid());

    log.debug("User ID>>>>>" + gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    HashMap hmResult = new HashMap();

    gmWSUtil.parseJsonToVO(strInput, gmCRMSurgeonlistVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMSurgeonlistVO);
    String transid = (String) hmInput.get("PARTYID");
    if (!strpartyid.equals("")) {

      hmInput.put("SCREENNM", "SURGEON INFO");
      hmInput.put("TRANSID", transid);
      // For Usage Tracking
      gmusagebean.saveUsageTracking(hmInput);
      HashMap hmParam =
              GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
                  (new GmSalesDashReqVO())));
      hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
      hmInput.put("HMPARAM", hmParam);
      hmInput.put("USERDEPTID", gmUserVO.getDeptid());
      hmResult = gmCRMSurgeonBean.fetchSurgeonReport(hmInput);
      hmResult.put("SURGACSFL", gmCRMSurgeonBean.fetchSurigcalAccess(hmInput));
      gmCRMSurgeonlistVO = gmCRMSurgeonBean.populateSurgeonListVO(hmResult);
      listGmCRMSurgeonlistVO.add(gmCRMSurgeonlistVO);
    } else if (strstropt.equals("summaryRpt")) {
      // For Override the Access Level of Associate Sales REP to Sales Rep (Associate Sales REP
      // checks with account id)
      HashMap hmParam =
          GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
              (new GmSalesDashReqVO())));
      hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
      hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
      hmInput.put("HMPARAM", hmParam);
      hmInput.put("USERDEPTID", gmUserVO.getDeptid());
      alReturn =
          GmCommonClass
              .parseNullArrayList((gmCRMSurgeonBean.fetchSummarySurgeonFullReport(hmInput)));
      listGmCRMSurgeonlistVO =
          gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMSurgeonlistVO,
              gmCRMSurgeonlistVO.getMappingProperties());
    } else {
      alReturn =
          GmCommonClass.parseNullArrayList((gmCRMSurgeonBean.fetchSurgeonFullReport(hmInput)));
      listGmCRMSurgeonlistVO =
          gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMSurgeonlistVO,
              gmCRMSurgeonlistVO.getMappingProperties());
    }
    if (listGmCRMSurgeonlistVO.size() > 0)
      gmCRMListVO.setGmCRMSurgeonlistVO(listGmCRMSurgeonlistVO);
    else
      gmCRMListVO = null;
    return gmCRMListVO;

  }


  /**
   * Fetches the Surgeon Education and Professional Report list information for CRM
   * 
   * @param strInput
   * @return ArrayList
   * @throws com.globus.common.beans.AppError
   */
  @POST
  @Path("educationinfo")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listSurgeonEduInfo(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {
    GmCRMSurgeonlistVO gmCRMSurgeonlistVO = new GmCRMSurgeonlistVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMSurgeonReportBean gmCRMSurgeonReportBean = new GmCRMSurgeonReportBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMSurgeonlistVO gmCRMSurgeonVO = mapper.readValue(strInput, GmCRMSurgeonlistVO.class);
    ArrayList alReturn = new ArrayList();
    List<GmCRMSurgeonlistVO> listGmCRMSurgeonlistVO = new ArrayList<GmCRMSurgeonlistVO>();

    validateToken(gmCRMSurgeonVO.getToken());
    String strpartyid = gmCRMSurgeonVO.getPartyid();

    gmCRMSurgeonlistVO.setUserid(gmUserVO.getUserid());

    log.debug("User ID>>>>>" + gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    HashMap hmResult = new HashMap();

    gmWSUtil.parseJsonToVO(strInput, gmCRMSurgeonlistVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMSurgeonlistVO);

    if (gmCRMSurgeonVO.getStropt().equalsIgnoreCase("education")) {
      alReturn =
          GmCommonClass.parseNullArrayList((gmCRMSurgeonReportBean
              .fetchSurgeonEducationReport(hmInput)));
    } else if (gmCRMSurgeonVO.getStropt().equalsIgnoreCase("professional")) {
      alReturn =
          GmCommonClass.parseNullArrayList((gmCRMSurgeonReportBean
              .fetchSurgeonProfessionalReport(hmInput)));
    } else if (gmCRMSurgeonVO.getStropt().equalsIgnoreCase("consultant")) {
      alReturn =
          GmCommonClass
              .parseNullArrayList((gmCRMSurgeonReportBean.fetchSurgeonProjectRpt(hmInput)));
    }

    log.debug("listSurgeonEduInfo alRerutn>>>" + alReturn.size());

    listGmCRMSurgeonlistVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMSurgeonlistVO,
            gmCRMSurgeonlistVO.getMappingProperties());


    if (listGmCRMSurgeonlistVO.size() > 0)
      gmCRMListVO.setGmCRMSurgeonlistVO(listGmCRMSurgeonlistVO);
    else
      gmCRMListVO = null;
    return gmCRMListVO;

  }



  @POST
  @Path("activityinfo")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listActivityInfo(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {

    GmCRMSurgeonActivityVO gmCRMSurgeonActivityVO = new GmCRMSurgeonActivityVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMSurgeonActivityVO gmCRMSurgeonActVO =
        mapper.readValue(strInput, GmCRMSurgeonActivityVO.class);
    ArrayList alReturn = new ArrayList();
    List<GmCRMSurgeonActivityVO> listGmCRMSurgeonActivityVO =
        new ArrayList<GmCRMSurgeonActivityVO>();

    validateToken(gmCRMSurgeonActVO.getToken());
    String strpartyid = gmCRMSurgeonActVO.getPartyid();

    log.debug("User ID>>>>>" + gmUserVO.getUserid() + ">>>strpartyid>>>" + strpartyid);

    HashMap hmInput = new HashMap();

    gmWSUtil.parseJsonToVO(strInput, gmCRMSurgeonActVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMSurgeonActVO);

    alReturn = gmCRMSurgeonBean.fetchPartyActivityReport(hmInput);
    listGmCRMSurgeonActivityVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMSurgeonActVO,
            gmCRMSurgeonActVO.getMappingProperties());

    log.debug("fetchSurgeonAffiliationReport>>>>" + listGmCRMSurgeonActivityVO);

    gmCRMListVO.setGmCRMSurgeonActivityVO(listGmCRMSurgeonActivityVO);
    return gmCRMListVO;


  }

  @POST
  @Path("inforepadvpsync")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMDeviceDetailVO listSurgeonRepADVPSyncInfo(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMDeviceDetailVO gmCRMDeviceDetailVO = new GmCRMDeviceDetailVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmCRMDeviceDetailVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmCRMDeviceDetailVO, hmValidate);
    GmWSUtil gmWSUtil = new GmWSUtil();

    GmAdVpVO gmAdVpVO = new GmAdVpVO();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmAdVpVO> listGmAdVpVO = new ArrayList<GmAdVpVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmCRMDeviceDetailVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getUuid()));

    alResult =
        GmCommonClass.parseNullArrayList(gmCRMSurgeonBean.listSurgeonRepADVPSyncInfo(hmParams));
    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmAdVpVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmAdVpVO, gmAdVpVO.getMappingProperties());
    // Set the page header detail from first hashmap of list.
    gmCRMDeviceDetailVO.populatePageProperties(alResult);
    gmCRMDeviceDetailVO.setReftype("10306169");// Part Number sync type
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", gmCRMDeviceDetailVO.getReftype());
    hmParams.put("PAGESIZE", gmCRMDeviceDetailVO.getPagesize());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmCRMDeviceDetailVO.setArrgmadvpvo(listGmAdVpVO);

    return gmCRMDeviceDetailVO;

  }


  @POST
  @Path("infosurgparty")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMDeviceDetailVO listSurgeonPartySyncInfo(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMDeviceDetailVO gmCRMDeviceDetailVO = new GmCRMDeviceDetailVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    gmWSUtil.parseJsonToVO(strInput, gmCRMDeviceDetailVO);
    String strUpdate = gmCRMDeviceDetailVO.getUpdate();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmCRMDeviceDetailVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmCRMDeviceDetailVO, hmValidate);

    GmCRMSurgeonlistVO gmCRMSurgeonlistVO = new GmCRMSurgeonlistVO();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmCRMSurgeonlistVO> listGmCRMSurgeonlistVO = new ArrayList<GmCRMSurgeonlistVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmCRMDeviceDetailVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getUuid()));

    if (strUpdate.equals("")) {
      alResult =
          GmCommonClass.parseNullArrayList(gmCRMSurgeonBean.listSurgeonPartysSyncInfo(hmParams));
    } else {
      alResult =
          GmCommonClass.parseNullArrayList(gmCRMSurgeonBean.listSurgeonPartysUpdateInfo(hmParams));
    }


    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmCRMSurgeonlistVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmCRMSurgeonlistVO,
            gmCRMSurgeonlistVO.getMappingProperties());
    // Set the page header detail from first hashmap of list.
    gmCRMDeviceDetailVO.populatePageProperties(alResult);
    gmCRMDeviceDetailVO.setReftype("10306161");// Part Number sync type
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", gmCRMDeviceDetailVO.getReftype());
    hmParams.put("PAGESIZE", gmCRMDeviceDetailVO.getPagesize());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmCRMDeviceDetailVO.setArrgmsurgeonlistvo(listGmCRMSurgeonlistVO);

    return gmCRMDeviceDetailVO;

  }

  @POST
  @Path("infosurgattrparty")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMDeviceDetailVO listSurgeonAttrSyncInfo(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMDeviceDetailVO gmCRMDeviceDetailVO = new GmCRMDeviceDetailVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    gmWSUtil.parseJsonToVO(strInput, gmCRMDeviceDetailVO);
    String strUpdate = gmCRMDeviceDetailVO.getUpdate();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmCRMDeviceDetailVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmCRMDeviceDetailVO, hmValidate);

    GmCRMSurgeonAttrVO gmCRMSurgeonAttrVO = new GmCRMSurgeonAttrVO();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmCRMSurgeonAttrVO> listGmCRMSurgeonAttrVO = new ArrayList<GmCRMSurgeonAttrVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmCRMDeviceDetailVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getUuid()));

    if (strUpdate.equals("")) {
      alResult =
          GmCommonClass.parseNullArrayList(gmCRMSurgeonBean.listSurgeonAttrSyncInfo(hmParams));
    } else {
      alResult =
          GmCommonClass.parseNullArrayList(gmCRMSurgeonBean.listSurgeonAttrUpdateInfo(hmParams));
    }

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmCRMSurgeonAttrVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmCRMSurgeonAttrVO,
            gmCRMSurgeonAttrVO.getMappingProperties());
    // Set the page header detail from first hashmap of list.
    gmCRMDeviceDetailVO.populatePageProperties(alResult);
    gmCRMDeviceDetailVO.setReftype("10306162");// Part Number sync type
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", gmCRMDeviceDetailVO.getReftype());
    hmParams.put("PAGESIZE", gmCRMDeviceDetailVO.getPagesize());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmCRMDeviceDetailVO.setArrgmcrmsurgeonattrvo(listGmCRMSurgeonAttrVO);

    return gmCRMDeviceDetailVO;

  }

  @POST
  @Path("infohospaffl")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listSurgeonHospitalInfo(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {

    GmCRMSurgeonAffiliationVO gmCRMSurgeonAffiliationVO = new GmCRMSurgeonAffiliationVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMSurgeonAffiliationVO gmCRMSurgeonAffilVO =
        mapper.readValue(strInput, GmCRMSurgeonAffiliationVO.class);
    ArrayList alReturn = new ArrayList();
    List<GmCRMSurgeonAffiliationVO> listGmCRMSurgeonAffiliationVO =
        new ArrayList<GmCRMSurgeonAffiliationVO>();

    validateToken(gmCRMSurgeonAffilVO.getToken());
    String strpartyid = gmCRMSurgeonAffilVO.getPartyid();

    log.debug("User ID>>>>>" + gmUserVO.getUserid());

    HashMap hmInput = new HashMap();

    gmWSUtil.parseJsonToVO(strInput, gmCRMSurgeonAffilVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMSurgeonAffilVO);

    alReturn = gmCRMSurgeonBean.fetchSurgeonAffiliationReport(hmInput);
    listGmCRMSurgeonAffiliationVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMSurgeonAffilVO,
            gmCRMSurgeonAffilVO.getMappingProperties());

    log.debug("fetchSurgeonAffiliationReport>>>>" + listGmCRMSurgeonAffiliationVO);

    gmCRMListVO.setGmCRMSurgeonAffiliationVO(listGmCRMSurgeonAffiliationVO);
    return gmCRMListVO;


  }

  @POST
  @Path("infohospafflsync")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMDeviceDetailVO listSurgeonHospitalSyncInfo(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMDeviceDetailVO gmCRMDeviceDetailVO = new GmCRMDeviceDetailVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    gmWSUtil.parseJsonToVO(strInput, gmCRMDeviceDetailVO);
    String strUpdate = gmCRMDeviceDetailVO.getUpdate();
    log.debug("strUpdate>>>>" + strUpdate);
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmCRMDeviceDetailVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmCRMDeviceDetailVO, hmValidate);

    GmCRMSurgeonAffiliationVO gmCRMSurgeonAffiliationVO = new GmCRMSurgeonAffiliationVO();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmCRMSurgeonAffiliationVO> listGmCRMSurgeonAffiliationVO =
        new ArrayList<GmCRMSurgeonAffiliationVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmCRMDeviceDetailVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getUuid()));

    alResult =
        GmCommonClass.parseNullArrayList(gmCRMSurgeonBean
            .fetchSurgeonAffiliationSyncReport(hmParams));
    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmCRMSurgeonAffiliationVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmCRMSurgeonAffiliationVO,
            gmCRMSurgeonAffiliationVO.getMappingProperties());
    // Set the page header detail from first hashmap of list.
    gmCRMDeviceDetailVO.populatePageProperties(alResult);
    gmCRMDeviceDetailVO.setReftype("10306164");// Part Number sync type
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", gmCRMDeviceDetailVO.getReftype());
    hmParams.put("PAGESIZE", gmCRMDeviceDetailVO.getPagesize());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmCRMDeviceDetailVO.setArrgmcrmsurgeonaffiliationvo(listGmCRMSurgeonAffiliationVO);

    return gmCRMDeviceDetailVO;

  }

  @POST
  @Path("infosurgactsync")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMDeviceDetailVO listSurgeonSurgicalSyncInfo(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMDeviceDetailVO gmCRMDeviceDetailVO = new GmCRMDeviceDetailVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    gmWSUtil.parseJsonToVO(strInput, gmCRMDeviceDetailVO);
    String strUpdate = gmCRMDeviceDetailVO.getUpdate();
    log.debug("strUpdate>>>>" + strUpdate);
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmCRMDeviceDetailVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmCRMDeviceDetailVO, hmValidate);

    GmCRMSurgeonSurgicalVO gmCRMSurgeonSurgicalVO = new GmCRMSurgeonSurgicalVO();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmCRMSurgeonSurgicalVO> listGmCRMSurgeonSurgicalVO =
        new ArrayList<GmCRMSurgeonSurgicalVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmCRMDeviceDetailVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getUuid()));

    alResult =
        GmCommonClass.parseNullArrayList(gmCRMSurgeonBean.fetchSurgeonSurgicalSyncReport(hmParams));



    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmCRMSurgeonSurgicalVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmCRMSurgeonSurgicalVO,
            gmCRMSurgeonSurgicalVO.getMappingProperties());
    // Set the page header detail from first hashmap of list.
    gmCRMDeviceDetailVO.populatePageProperties(alResult);
    gmCRMDeviceDetailVO.setReftype("10306163");// Part Number sync type
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", gmCRMDeviceDetailVO.getReftype());
    hmParams.put("PAGESIZE", gmCRMDeviceDetailVO.getPagesize());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmCRMDeviceDetailVO.setArrgmcrmsurgeonsurgicalvo(listGmCRMSurgeonSurgicalVO);

    return gmCRMDeviceDetailVO;

  }

  @POST
  @Path("infosurgact")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listSurgeonSurgicalInfo(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {

    GmCRMSurgeonSurgicalVO gmCRMSurgeonSurgicalVO = new GmCRMSurgeonSurgicalVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMSurgeonSurgicalVO gmCRMSurgeonSurgicalActVO =
        mapper.readValue(strInput, GmCRMSurgeonSurgicalVO.class);
    ArrayList alReturn = new ArrayList();
    List<GmCRMSurgeonSurgicalVO> listGmCRMSurgeonSurgicalVO =
        new ArrayList<GmCRMSurgeonSurgicalVO>();

    validateToken(gmCRMSurgeonSurgicalActVO.getToken());
    String strpartyid = gmCRMSurgeonSurgicalActVO.getPartyid();

    log.debug("User ID>>>>>" + gmUserVO.getUserid());

    HashMap hmInput = new HashMap();

    gmWSUtil.parseJsonToVO(strInput, gmCRMSurgeonSurgicalActVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMSurgeonSurgicalActVO);

    alReturn = gmCRMSurgeonBean.fetchSurgeonSurgicalReport(hmInput);
    listGmCRMSurgeonSurgicalVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMSurgeonSurgicalActVO,
            gmCRMSurgeonSurgicalActVO.getMappingProperties());

    log.debug("fetchSurgeonSurgicalReport>>>>" + listGmCRMSurgeonSurgicalVO);

    gmCRMListVO.setGmCRMSurgeonSurgicalVO(listGmCRMSurgeonSurgicalVO);
    return gmCRMListVO;

  }


  @POST
  @Path("infoglobact")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listSurgeonActivityInfo(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {

    GmCRMSurgeonActivityVO gmCRMSurgeonActivityVO = new GmCRMSurgeonActivityVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMSurgeonActivityVO gmCRMSurgeonGlobusActVO =
        mapper.readValue(strInput, GmCRMSurgeonActivityVO.class);
    ArrayList alReturn = new ArrayList();
    List<GmCRMSurgeonActivityVO> listGmCRMSurgeonActivityVO =
        new ArrayList<GmCRMSurgeonActivityVO>();

    validateToken(gmCRMSurgeonGlobusActVO.getToken());
    String strpartyid = gmCRMSurgeonGlobusActVO.getPartyid();

    log.debug("User ID>>>>>" + gmUserVO.getUserid());

    HashMap hmInput = new HashMap();

    gmWSUtil.parseJsonToVO(strInput, gmCRMSurgeonGlobusActVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMSurgeonGlobusActVO);

    alReturn = gmCRMSurgeonBean.fetchSurgeonActivityReport(hmInput);
    listGmCRMSurgeonActivityVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMSurgeonGlobusActVO,
            gmCRMSurgeonGlobusActVO.getMappingProperties());

    log.debug("fetchSurgeonActivityReport>>>>" + listGmCRMSurgeonActivityVO);

    gmCRMListVO.setGmCRMSurgeonActivityVO(listGmCRMSurgeonActivityVO);
    return gmCRMListVO;

  }

  @POST
  @Path("infologsync")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMDeviceDetailVO listSurgeonCommentsSync(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMDeviceDetailVO gmCRMDeviceDetailVO = new GmCRMDeviceDetailVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    gmWSUtil.parseJsonToVO(strInput, gmCRMDeviceDetailVO);
    String strUpdate = gmCRMDeviceDetailVO.getUpdate();
    log.debug("strUpdate>>>>" + strUpdate);
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmCRMDeviceDetailVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmCRMDeviceDetailVO, hmValidate);

    GmCRMLogVO gmCRMLogVO = new GmCRMLogVO();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmCRMLogVO> listGmCRMLogVO = new ArrayList<GmCRMLogVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmCRMDeviceDetailVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getUuid()));

    alResult = GmCommonClass.parseNullArrayList(gmCRMSurgeonBean.fetchSurgeonLogSync(hmParams));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmCRMLogVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmCRMLogVO, gmCRMLogVO.getMappingProperties());
    // Set the page header detail from first hashmap of list.
    gmCRMDeviceDetailVO.populatePageProperties(alResult);
    gmCRMDeviceDetailVO.setReftype("10306173");// Part Number sync type
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", gmCRMDeviceDetailVO.getReftype());
    hmParams.put("PAGESIZE", gmCRMDeviceDetailVO.getPagesize());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmCRMDeviceDetailVO.setArrgmlogvo(listGmCRMLogVO);

    return gmCRMDeviceDetailVO;

  }

  @POST
  @Path("infodocsync")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMDeviceDetailVO listSurgeonDocumentSyncInfo(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMDeviceDetailVO gmCRMDeviceDetailVO = new GmCRMDeviceDetailVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    gmWSUtil.parseJsonToVO(strInput, gmCRMDeviceDetailVO);
    String strUpdate = gmCRMDeviceDetailVO.getUpdate();
    log.debug("strUpdate>>>>" + strUpdate);
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmCRMDeviceDetailVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmCRMDeviceDetailVO, hmValidate);

    GmCRMFileUploadVO gmCRMFileUploadVO = new GmCRMFileUploadVO();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmCRMFileUploadVO> listGmCRMFileUploadVO = new ArrayList<GmCRMFileUploadVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmCRMDeviceDetailVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getUuid()));

    if (strUpdate.equals("")) {
      alResult =
          GmCommonClass.parseNullArrayList(gmCRMSurgeonBean.fetchSurgeonDocSyncReport(hmParams));
    } else {
      alResult =
          GmCommonClass.parseNullArrayList(gmCRMSurgeonBean.fetchSurgeonDocUpdateReport(hmParams));
    }

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmCRMFileUploadVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmCRMFileUploadVO,
            gmCRMFileUploadVO.getMappingProperties());
    // Set the page header detail from first hashmap of list.
    gmCRMDeviceDetailVO.populatePageProperties(alResult);
    gmCRMDeviceDetailVO.setReftype("10306168");// Part Number sync type
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", gmCRMDeviceDetailVO.getReftype());
    hmParams.put("PAGESIZE", gmCRMDeviceDetailVO.getPagesize());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmCRMDeviceDetailVO.setListGmCRMFileUploadVO(listGmCRMFileUploadVO);

    return gmCRMDeviceDetailVO;

  }



  @POST
  @Path("infodocs")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listSurgeonDocumentInfo(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {

    GmCRMFileUploadVO gmCRMFileUploadVO = new GmCRMFileUploadVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMFileUploadBean gmFileUploadBean = new GmCRMFileUploadBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMFileUploadVO gmCRMSurgeonFileUploadVO =
        mapper.readValue(strInput, GmCRMFileUploadVO.class);
    ArrayList alReturn = new ArrayList();
    List<GmCRMFileUploadVO> listGmCRMSurgeonFileUploadVO = new ArrayList<GmCRMFileUploadVO>();

    validateToken(gmCRMSurgeonFileUploadVO.getToken());
    String strPartyID = gmCRMSurgeonFileUploadVO.getRefid();

    log.debug("User ID>>>>>" + gmUserVO.getUserid());
    gmWSUtil.parseJsonToVO(strInput, gmCRMSurgeonFileUploadVO);

    // alReturn = gmCRMSurgeonBean.fetchSurgeonActivityReport(hmInput);
    alReturn = gmFileUploadBean.fetchFilesByType(strPartyID, "105220,105221,105222");
    listGmCRMSurgeonFileUploadVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMSurgeonFileUploadVO,
            gmCRMSurgeonFileUploadVO.getMappingProperties());



    gmCRMListVO.setGmCRMFileUploadVO(listGmCRMSurgeonFileUploadVO);
    return gmCRMListVO;

  }


  @POST
  @Path("save")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMSurgeonlistVO saveSurgeon(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {
	 
	  GmCRMSurgeonSocialVO gmCRMSurgeonSocialVO = new GmCRMSurgeonSocialVO();
	  GmCRMSurgeonAddressListVO gmCRMSurgeonAddressListVO = new GmCRMSurgeonAddressListVO();	 
    GmCRMSurgeonContactsListVO gmCRMSurgeonContactsListVO = new GmCRMSurgeonContactsListVO();
    // GmCRMSurgeonAttrVO gmCRMSurgeonAttrVO = new GmCRMSurgeonAttrVO();
    GmCRMSurgeonAffiliationVO gmCRMSurgeonAffiliationVO = new GmCRMSurgeonAffiliationVO();
    GmCRMSurgeonEducationVO gmCRMSurgeonEducationVO = new GmCRMSurgeonEducationVO();
    GmCRMSurgeonProfessionalVO gmCRMSurgeonProfessionalVO = new GmCRMSurgeonProfessionalVO();
    GmCRMFileUploadVO gmCRMFileUploadVO = new GmCRMFileUploadVO();
    GmCRMSurgeonRepVO gmCRMSurgeonRepVO = new GmCRMSurgeonRepVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMSurgeonlistVO gmCRMSurgeonlistVO = mapper.readValue(strInput, GmCRMSurgeonlistVO.class);
    ArrayList alReturn = new ArrayList();
    validateToken(gmCRMSurgeonlistVO.getToken());
    List<GmCRMSurgeonAddressListVO> arrGmSurgeonAddressVO =
        gmCRMSurgeonlistVO.getArrgmsurgeonsaddressvo();
    List<GmCRMSurgeonContactsListVO> arrGmSurgeonContactsVO =
        gmCRMSurgeonlistVO.getArrgmsurgeoncontactsvo();
    // List<GmCRMSurgeonAttrVO> arrGmCRMSurgeonAttrVO =
    // gmCRMSurgeonlistVO.getArrgmcrmsurgeonattrvo();
    List<GmCRMSurgeonAffiliationVO> arrGmCRMSurgeonAffiliationVO =
        gmCRMSurgeonlistVO.getArrgmcrmsurgeonaffiliationvo();
    List<GmCRMFileUploadVO> arrGmCRMFileUploadVO = gmCRMSurgeonlistVO.getArrgmcrmfileuploadvo();
    List<GmCRMSurgeonEducationVO> arrgmcrmeducationvo = gmCRMSurgeonlistVO.getArrgmcrmeducationvo();
    List<GmCRMSurgeonProfessionalVO> arrgmcrmprofessionalvo =
        gmCRMSurgeonlistVO.getArrgmcrmprofessionalvo();

    List<GmCRMSurgeonRepVO> arrgmcrmrepvo = gmCRMSurgeonlistVO.getArrgmcrmsurgeonrepvo();
    
    List<GmCRMSurgeonSocialVO> arrGmCRMSurgeonsocialVO = gmCRMSurgeonlistVO.getArrgmcrmsurgeonsocialvo();


    gmCRMSurgeonlistVO.setUserid(gmUserVO.getUserid());
    HashMap hmInput = new HashMap();

    String strSrId = "";
    hmInput.put("PARTYID", gmCRMSurgeonlistVO.getPartyid());
    hmInput.put("FIRSTNM", gmCRMSurgeonlistVO.getFirstnm());
    hmInput.put("LASTNM", gmCRMSurgeonlistVO.getLastnm());
    hmInput.put("PARTYNM", gmCRMSurgeonlistVO.getPartynm());
    hmInput.put("MIDINITIAL", gmCRMSurgeonlistVO.getMidinitial());
    hmInput.put("USERID", gmCRMSurgeonlistVO.getUserid());
    hmInput.put("NPID", gmCRMSurgeonlistVO.getNpid());
    hmInput.put("CITY", gmCRMSurgeonlistVO.getCity());
    hmInput.put("PHONE", gmCRMSurgeonlistVO.getPhone());
    hmInput.put("EMAIL", gmCRMSurgeonlistVO.getEmail());
    hmInput.put("ASSTNM", gmCRMSurgeonlistVO.getAsstnm());
    hmInput.put("MNGRNM", gmCRMSurgeonlistVO.getMngrnm());
    hmInput.put("ACTIVEFL", gmCRMSurgeonlistVO.getActivefl());
    hmInput.put("SALUTE", gmCRMSurgeonlistVO.getSalute());
    hmInput.put("COMPANYID", gmCRMSurgeonlistVO.getCompanyid());


    strSrId =
        gmCRMSurgeonBean.saveSurgeonDetail(hmInput, arrGmSurgeonAddressVO,
            gmCRMSurgeonAddressListVO, arrGmSurgeonContactsVO, gmCRMSurgeonContactsListVO,
//            arrGmCRMSurgeonAffiliationVO, gmCRMSurgeonAffiliationVO,
            gmCRMFileUploadVO,
            arrGmCRMFileUploadVO, gmCRMSurgeonEducationVO, arrgmcrmeducationvo,
            gmCRMSurgeonProfessionalVO, arrgmcrmprofessionalvo, gmCRMSurgeonRepVO, arrgmcrmrepvo,
			gmCRMSurgeonSocialVO, arrGmCRMSurgeonsocialVO);

    gmCRMSurgeonlistVO.setPartyid(strSrId);

    return gmCRMSurgeonlistVO;

  }

  @POST
  @Path("criterialist")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listCriteriaSurgeonInfo(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {

    GmCRMSurgeonlistVO gmCRMSurgeonlistVO = new GmCRMSurgeonlistVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();

    GmCriteriaBean gmCriteriaBean = new GmCriteriaBean();
    GmCriteriaVO gmCriteriaVO = new GmCriteriaVO();

    gmWSUtil.parseJsonToVO(strInput, gmCriteriaVO);

    validateToken(gmCriteriaVO.getToken());

    gmCRMSurgeonlistVO.setUserid(gmUserVO.getUserid());

    log.debug("User ID>>>>>" + gmUserVO.getUserid());

    ArrayList alCriteria = new ArrayList();
    alCriteria = gmCriteriaBean.fetchCriteriaBy(gmCriteriaVO.getCriteriaid(), "surgeon");


    HashMap hmCriteria = new HashMap();



    for (int i = 0; i < alCriteria.size(); i++) {
      HashMap hmValues = GmCommonClass.parseNullHashMap((HashMap) alCriteria.get(i));
      String strKey = GmCommonClass.parseNull((String) hmValues.get("KEY"));
      String strValue = GmCommonClass.parseNull((String) hmValues.get("VALUE"));
      hmCriteria.put(strKey, strValue);
    }



    ArrayList alReturn = new ArrayList();
    List<GmCRMSurgeonlistVO> listGmCRMSurgeonlistVO = new ArrayList<GmCRMSurgeonlistVO>();

    HashMap hmInput = new HashMap();

    gmWSUtil.getVOFromHashMap(hmCriteria, gmCRMSurgeonlistVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMSurgeonlistVO);


    alReturn = GmCommonClass.parseNullArrayList((gmCRMSurgeonBean.fetchSurgeonFullReport(hmInput)));

    log.debug("alRerutn>>>" + alReturn);

    listGmCRMSurgeonlistVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMSurgeonlistVO,
            gmCRMSurgeonlistVO.getMappingProperties());

    log.debug("listGmCRMSurgeonlistVO>>>>" + listGmCRMSurgeonlistVO);
    gmCRMListVO.setGmCRMSurgeonlistVO(listGmCRMSurgeonlistVO);
    return gmCRMListVO;
  }

  @POST
  @Path("saveaffil")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO saveSurgeonAffil(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMSurgeonAffiliationVO gmCRMSurgeonAffiliationVO =
        mapper.readValue(strInput, GmCRMSurgeonAffiliationVO.class);
    List<GmCRMSurgeonAffiliationVO> listgmCRMSurgeonAffiliationVO =
        new ArrayList<GmCRMSurgeonAffiliationVO>();
    validateToken(gmCRMSurgeonAffiliationVO.getToken());
    gmCRMSurgeonAffiliationVO.setUserid(gmUserVO.getUserid());
    HashMap hmParams = new HashMap();

    hmParams = gmWSUtil.getHashMapFromVO(gmCRMSurgeonAffiliationVO);

    GmDBManager gmDBManager = new GmDBManager();
    listgmCRMSurgeonAffiliationVO.add(gmCRMSurgeonAffiliationVO);

//    gmCRMSurgeonBean.saveAffiliationInfo(hmParams, gmDBManager, listgmCRMSurgeonAffiliationVO,
//        gmCRMSurgeonAffiliationVO);
    gmDBManager.commit();
    gmCRMListVO.setGmCRMSurgeonAffiliationVO(listgmCRMSurgeonAffiliationVO);
    return gmCRMListVO;

  }

  @POST
  @Path("address")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listSurgeonAddress(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {

    log.debug("listGmCRMSurgeonAddressListVO>>>>");
    GmCRMSurgeonAddressListVO gmCRMSurgeonAddressListVO = new GmCRMSurgeonAddressListVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMSurgeonlistVO gmCRMSurgeonVO = mapper.readValue(strInput, GmCRMSurgeonlistVO.class);
    ArrayList alReturn = new ArrayList();
    List<GmCRMSurgeonAddressListVO> listGmCRMSurgeonAddressListVO =
        new ArrayList<GmCRMSurgeonAddressListVO>();

    validateToken(gmCRMSurgeonVO.getToken());
    String strpartyid = gmCRMSurgeonVO.getPartyid();

    // gmCRMSurgeonAddressListVO.setUserid(gmUserVO.getUserid());

    log.debug("User ID>>>>>" + gmUserVO.getUserid());

    HashMap hmInput = new HashMap();

    gmWSUtil.parseJsonToVO(strInput, gmCRMSurgeonVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMSurgeonVO);

    if (!strpartyid.equals("")) {
      alReturn = gmCRMSurgeonBean.fetchSurgeonAddressReport(hmInput);
      listGmCRMSurgeonAddressListVO =
          gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMSurgeonAddressListVO,
              gmCRMSurgeonAddressListVO.getMappingProperties());

      log.debug("listGmCRMSurgeonAddressListVO>>>>" + listGmCRMSurgeonAddressListVO);
    }
    gmCRMListVO.setGmCRMSurgeonAddressListVO(listGmCRMSurgeonAddressListVO);
    return gmCRMListVO;

  }

  @POST
  @Path("sales")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listSurgeonRepInfo(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {

    GmCRMSurgeonlistVO gmCRMSurgeonListVO = new GmCRMSurgeonlistVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMSurgeonlistVO gmCRMSurgeonVO = mapper.readValue(strInput, GmCRMSurgeonlistVO.class);
    ArrayList alReturn = new ArrayList();
    List<GmCRMSurgeonlistVO> listGmCRMSurgeonListVO = new ArrayList<GmCRMSurgeonlistVO>();

    validateToken(gmCRMSurgeonVO.getToken());
    String strpartyid = gmCRMSurgeonVO.getPartyid();

    // gmCRMSurgeonAddressListVO.setUserid(gmUserVO.getUserid());

    log.debug("User ID>>>>>" + gmUserVO.getUserid());

    HashMap hmInput = new HashMap();

    gmWSUtil.parseJsonToVO(strInput, gmCRMSurgeonVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMSurgeonVO);

    if (!strpartyid.equals("")) {
      alReturn = gmCRMSurgeonBean.fetchSurgeonRepReport(hmInput);
      listGmCRMSurgeonListVO =
          gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMSurgeonListVO,
              gmCRMSurgeonListVO.getMappingProperties());

      log.debug("listGmCRMSurgeonListVO>>>>" + listGmCRMSurgeonListVO);
    }
    gmCRMListVO.setGmCRMSurgeonlistVO(listGmCRMSurgeonListVO);
    return gmCRMListVO;

  }

  @POST
  @Path("surigicalact")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listSurgcialActivityInfo(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {

    // GmCRMSurgicalActivityVO gmCRMSurgicalActivityVO = new GmCRMSurgicalActivityVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmCRMSurgicalActivityVO gmCRMSurgicalActivityVO =
        mapper.readValue(strInput, GmCRMSurgicalActivityVO.class);
    ArrayList alReturn = new ArrayList();
    List<GmCRMSurgicalActivityVO> listGmCRMSurgicalActivityVO =
        new ArrayList<GmCRMSurgicalActivityVO>();

    validateToken(gmCRMSurgicalActivityVO.getToken());
    String strpartyid = gmCRMSurgicalActivityVO.getPartyid();
    HashMap hmInput = new HashMap();

    gmWSUtil.parseJsonToVO(strInput, gmCRMSurgicalActivityVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMSurgicalActivityVO);
    HashMap hmParam =
            GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
                (new GmSalesDashReqVO())));
    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
    hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
    hmInput.put("HMPARAM", hmParam);
    hmInput.put("USERDEPTID", gmUserVO.getDeptid());
    if (gmCRMSurgicalActivityVO.getStropt().equalsIgnoreCase("REV")) {
      alReturn = gmCRMSurgeonBean.fetchSurigcalActivityByRevenue(hmInput);
    } else if (gmCRMSurgicalActivityVO.getStropt().equalsIgnoreCase("PRC")) {
      alReturn = gmCRMSurgeonBean.fetchSurigcalActivityByProc(hmInput);
    }
    listGmCRMSurgicalActivityVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMSurgicalActivityVO,
            gmCRMSurgicalActivityVO.getMappingProperties());

    gmCRMListVO.setGmCRMSurgicalActivityVO((listGmCRMSurgicalActivityVO));
    log.debug("gmCRMListVO>>>>" + gmCRMListVO);
    return gmCRMListVO;
  }


  @POST
  @Path("surigicalactdocnt")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMSurgicalDOVO surgcialActivityDoCnt(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {
    log.debug("INPUT" + strInput);
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMSurgicalDOVO gmCRMSurgicalDOVO = mapper.readValue(strInput, GmCRMSurgicalDOVO.class);
    HashMap hmReturn = new HashMap();
    validateToken(gmCRMSurgicalDOVO.getToken());
    String strpartyid = gmCRMSurgicalDOVO.getPartyid();
    HashMap hmInput = new HashMap();
    gmWSUtil.parseJsonToVO(strInput, gmCRMSurgicalDOVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMSurgicalDOVO);
    HashMap hmParam =
            GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
                (new GmSalesDashReqVO())));
    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
    hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
    hmInput.put("HMPARAM", hmParam);
    hmInput.put("USERDEPTID", gmUserVO.getDeptid());
    hmReturn = gmCRMSurgeonBean.fetchSurgicalActivityDOCount(hmInput);
    log.debug("alReturn>>>>" + hmReturn);
    gmWSUtil.getVOFromHashMap(hmReturn, gmCRMSurgicalDOVO);
    return gmCRMSurgicalDOVO;
  }

  /**
   * This method used to fetch CRM Surgeon Summary Report. Load VP, AD , Distributor and Sales rep�s
   * surgeon detail report by status based on login user sales hierarchy.
   */
  @POST
  @Path("summaryrpt")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listSummaryReportInfo(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();

    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();

    GmCRMSummaryReportVO gmCRMSummaryReportVO =
        mapper.readValue(strInput, GmCRMSummaryReportVO.class);
    List<GmCRMSummaryReportVO> listGmCRMSummaryReportVO = new ArrayList<GmCRMSummaryReportVO>();
    validateToken(gmCRMSummaryReportVO.getToken());

    HashMap hmInput = new HashMap();
    ArrayList alReturn = new ArrayList();

    gmWSUtil.parseJsonToVO(strInput, gmCRMSummaryReportVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMSummaryReportVO);
    // For Override the Access Level of Associate Sales REP to Sales Rep (Associate Sales REP checks
    // with account id)
    HashMap hmParam =
        GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
            (new GmSalesDashReqVO())));
    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
    hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
    hmInput.put("HMPARAM", hmParam);
    hmInput.put("USERDEPTID", gmUserVO.getDeptid());
    log.debug("hmInput >>>>" + hmInput);
    alReturn = gmCRMSurgeonBean.fetchSurgeonSummaryReports(hmInput);

    listGmCRMSummaryReportVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMSummaryReportVO,
            gmCRMSummaryReportVO.getMappingProperties());

    gmCRMListVO.setGmCRMSummaryReportVO((listGmCRMSummaryReportVO));
    log.debug("gmCRMListVO>>>>" + gmCRMListVO);
    return gmCRMListVO;
  }

  /**
   * This method used to fetch CRM Surgeon Revenue Report.
   */
  @POST
  @Path("summaryrev")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMRevenueReportVO listRevenueReportInfo(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    ObjectMapper mapper = new ObjectMapper();

    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmCRMRevenueHeaderVO gmCRMRevenueHeaderVO = new GmCRMRevenueHeaderVO();
    GmCRMRevenueDetailVO gmCRMRevenueDetailVO = new GmCRMRevenueDetailVO();

    GmCRMRevenueReportVO gmCRMRevenueReportVO =
        mapper.readValue(strInput, GmCRMRevenueReportVO.class);
    List<GmCRMRevenueHeaderVO> listGmCRMRevenueHeaderVO = new ArrayList<GmCRMRevenueHeaderVO>();
    List<GmCRMRevenueDetailVO> listGmCRMRevenueDetailVO = new ArrayList<GmCRMRevenueDetailVO>();
    validateToken(gmCRMRevenueReportVO.getToken());

    HashMap hmInput = new HashMap();
    ArrayList alReturn = new ArrayList();
    List<String> alHeader = new ArrayList<String>();

    gmWSUtil.parseJsonToVO(strInput, gmCRMRevenueReportVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMRevenueReportVO);
    // For Override the Access Level of Associate Sales REP to Sales Rep (Associate Sales REP checks
    // with account id)
    HashMap hmParam =
        GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
            (new GmSalesDashReqVO())));
    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
    hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
    hmInput.put("HMPARAM", hmParam);
    hmInput.put("USERDEPTID", gmUserVO.getDeptid());
    log.debug("hmInput >>>>" + hmInput);
    alHeader = gmCRMSurgeonBean.fetchSurgeonRevenueMonths(hmInput);
    log.debug("hmInput alHeader >>>>" + alHeader);
    alReturn = gmCRMSurgeonBean.fetchSurgeonRevenueReports(hmInput);
    log.debug("hmInput >>>>" + alReturn);

    listGmCRMRevenueDetailVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMRevenueDetailVO,
            gmCRMRevenueDetailVO.getMappingProperties());
    listGmCRMRevenueHeaderVO =
        gmWSUtil.getVOListFromHashMapList(alHeader, gmCRMRevenueHeaderVO,
            gmCRMRevenueHeaderVO.getMappingProperties());
    gmCRMRevenueReportVO.setGmCRMRevenueHeaderVO((listGmCRMRevenueHeaderVO));
    gmCRMRevenueReportVO.setGmCRMRevenueDetailVO((listGmCRMRevenueDetailVO));
    log.debug("gmCRMRevenueReportVO>>>>" + gmCRMRevenueReportVO);
    return gmCRMRevenueReportVO;
  }
  
  /**
   * This method used to fetch CRM Surgeon System DO List Report. 
   * For Surgical Activity Module on clicking the System Data Plot in Chart, the Fn Retrieves the DO List Report for the System 
   * Inputs are token, partyid, systemid, fromdt, todt
   */
  @POST
  @Path("surgsystemdo")
  @Produces({ MediaType.APPLICATION_JSON })
  @Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
  public GmCRMListVO FetchSystemDOList(String strInput) throws AppError,
          JsonParseException, JsonMappingException, IOException {

      GmWSUtil gmWSUtil = new GmWSUtil();
      GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
      GmCRMListVO gmCRMListVO = new GmCRMListVO();
      ObjectMapper mapper = new ObjectMapper();
      GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
      GmCRMSurgicalSystemDOVO gmCRMSurgicalSystemDOVO = mapper.readValue(
              strInput, GmCRMSurgicalSystemDOVO.class);
      List<GmCRMSurgicalSystemDOVO> listGmCRMSurgicalSystemDOVO = new ArrayList<GmCRMSurgicalSystemDOVO>();
      validateToken(gmCRMSurgicalSystemDOVO.getToken());

      HashMap hmInput = new HashMap();
      ArrayList alReturn = new ArrayList();

      gmWSUtil.parseJsonToVO(strInput, gmCRMSurgicalSystemDOVO);
      hmInput = gmWSUtil.getHashMapFromVO(gmCRMSurgicalSystemDOVO);
      HashMap hmParam =
              GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
                  (new GmSalesDashReqVO())));
      hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
      hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
      hmInput.put("HMPARAM", hmParam);
      hmInput.put("USERDEPTID", gmUserVO.getDeptid());

      log.debug("GmCRMSurgicalSystemDOVO hmInput >>>>" + hmInput);
      alReturn = gmCRMSurgeonBean.fetchSurgeonDOLists(hmInput);

      listGmCRMSurgicalSystemDOVO = gmWSUtil.getVOListFromHashMapList(
              alReturn, gmCRMSurgicalSystemDOVO,
              gmCRMSurgicalSystemDOVO.getMappingProperties());
      gmCRMListVO.setGmCRMSurgicalSystemDOVO((listGmCRMSurgicalSystemDOVO));
      return gmCRMListVO;
  }

}
