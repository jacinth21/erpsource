package com.globus.webservice.crm.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.bean.GmCriteriaBean;
import com.globus.webservice.crm.valueobject.GmCRMDeviceDetailVO;
import com.globus.webservice.crm.valueobject.GmCriteriaAttrVO;
import com.globus.webservice.crm.valueobject.GmCriteriaVO;
import com.globus.webservice.crm.valueobject.GmCRMListVO;

@Path("criteria")
public class GmCriteriaResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @POST
  @Path("list")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO fetchCriteria(String strInput) throws AppError,
      JsonGenerationException, JsonMappingException, JsonParseException, IOException, Exception {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCriteriaBean gmCriteriaBean = new GmCriteriaBean();
    GmCriteriaVO gmCriteriaVO = new GmCriteriaVO();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmCriteriaVO> listGmCriteriaVO = new ArrayList<GmCriteriaVO>();

    gmWSUtil.parseJsonToVO(strInput, gmCriteriaVO);
    validateToken(gmCriteriaVO.getToken());
    gmCriteriaVO.setUserid(gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    hmInput = gmWSUtil.getHashMapFromVO(gmCriteriaVO);

    ArrayList alReturn = new ArrayList();
    String strUserId = gmUserVO.getUserid();

    alReturn = gmCriteriaBean.fetchCriteria(strUserId, gmCriteriaVO.getCriteriatyp());

    listGmCriteriaVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCriteriaVO,
            gmCriteriaVO.getMappingProperties());
    if(listGmCriteriaVO.size() >0)
    	gmCRMListVO.setGmCriteriaVO(listGmCriteriaVO);
    else
    	gmCRMListVO = null;
    return gmCRMListVO;
  }

  @POST
  @Path("get")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCriteriaVO getCriteria(String strInput) throws AppError, JsonGenerationException,
      JsonMappingException, JsonParseException, IOException, Exception {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCriteriaBean gmCriteriaBean = new GmCriteriaBean();
    GmCriteriaVO gmCriteriaVO = new GmCriteriaVO();
    GmCriteriaAttrVO gmCriteriaAttrVO = new GmCriteriaAttrVO();
    List<GmCriteriaAttrVO> listGmCriteriaAttrVO = new ArrayList<GmCriteriaAttrVO>();

    gmWSUtil.parseJsonToVO(strInput, gmCriteriaVO);
    validateToken(gmCriteriaVO.getToken());

    HashMap hmInput = new HashMap();
    ArrayList alReturn = new ArrayList();
    hmInput = gmWSUtil.getHashMapFromVO(gmCriteriaVO);

    HashMap hmReturn = new HashMap();
    String strUserId = gmUserVO.getUserid();

    hmReturn = gmCriteriaBean.fetchCriteriaInfo(gmCriteriaVO.getCriteriaid());
    alReturn = gmCriteriaBean.fetchCriteriaAttr(gmCriteriaVO.getCriteriaid());

    gmWSUtil.getVOFromHashMap(hmReturn, gmCriteriaVO);
    listGmCriteriaAttrVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCriteriaAttrVO,
            gmCriteriaAttrVO.getMappingProperties());

    gmCriteriaVO.setListGmCriteriaAttrVO(listGmCriteriaAttrVO);
    gmCriteriaVO.setToken("");

    return gmCriteriaVO;
  }

  @POST
  @Path("save")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCriteriaVO saveCriteia(String strInput) throws AppError, JsonGenerationException,
      JsonMappingException, JsonParseException, IOException, Exception {


    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCriteriaBean gmCriteriaBean = new GmCriteriaBean();

    ObjectMapper mapper = new ObjectMapper();
    GmCriteriaVO gmCriteriaVO = mapper.readValue(strInput, GmCriteriaVO.class);

    validateToken(gmCriteriaVO.getToken());
    gmCriteriaVO.setUserid(gmUserVO.getUserid());


    HashMap hmInput = new HashMap();
    hmInput = gmWSUtil.getHashMapFromVO(gmCriteriaVO);

    List<GmCriteriaAttrVO> arrGmCriteriaAttrVO = gmCriteriaVO.getListGmCriteriaAttrVO();

    hmInput = gmCriteriaBean.saveCriteria(hmInput, arrGmCriteriaAttrVO, gmCriteriaVO.getUserid());

    gmWSUtil.getVOFromHashMap(hmInput, gmCriteriaVO);

    return gmCriteriaVO;

  }

  @POST
  @Path("criteriasync")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMDeviceDetailVO listCriteriaSync(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {

    GmCRMDeviceDetailVO gmCRMDeviceDetailVO = new GmCRMDeviceDetailVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    gmWSUtil.parseJsonToVO(strInput, gmCRMDeviceDetailVO);
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmCRMDeviceDetailVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmCRMDeviceDetailVO, hmValidate);

    GmCriteriaVO gmCriteriaVO = new GmCriteriaVO();
    GmCriteriaBean gmCriteriaBean = new GmCriteriaBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmCriteriaVO> listCriteriaVO = new ArrayList<GmCriteriaVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmCRMDeviceDetailVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getUuid()));

    alResult = GmCommonClass.parseNullArrayList(gmCriteriaBean.fetchCriteriaSync(hmParams));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listCriteriaVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmCriteriaVO,
            gmCriteriaVO.getMappingProperties());
    // Set the page header detail from first hashmap of list.
    gmCRMDeviceDetailVO.populatePageProperties(alResult);
    gmCRMDeviceDetailVO.setReftype("10306210");// Criteria sync type
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", gmCRMDeviceDetailVO.getReftype());
    hmParams.put("PAGESIZE", gmCRMDeviceDetailVO.getPagesize());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmCRMDeviceDetailVO.setArrgmcriteriavo(listCriteriaVO);

    return gmCRMDeviceDetailVO;

  }

  @POST
  @Path("criteriaattrsync")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMDeviceDetailVO listCriteriaAttrSync(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMDeviceDetailVO gmCRMDeviceDetailVO = new GmCRMDeviceDetailVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    gmWSUtil.parseJsonToVO(strInput, gmCRMDeviceDetailVO);
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmCRMDeviceDetailVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmCRMDeviceDetailVO, hmValidate);

    GmCriteriaAttrVO gmCriteriaAttrVO = new GmCriteriaAttrVO();
    GmCriteriaBean gmCriteriaBean = new GmCriteriaBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmCriteriaAttrVO> listCriteriaAttrVO = new ArrayList<GmCriteriaAttrVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmCRMDeviceDetailVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getUuid()));

    alResult = GmCommonClass.parseNullArrayList(gmCriteriaBean.fetchCriteriaAttrSync(hmParams));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listCriteriaAttrVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmCriteriaAttrVO,
            gmCriteriaAttrVO.getMappingProperties());
    // Set the page header detail from first hashmap of list.
    gmCRMDeviceDetailVO.populatePageProperties(alResult);
    gmCRMDeviceDetailVO.setReftype("10306209");// Criteria Attr sync type
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", gmCRMDeviceDetailVO.getReftype());
    hmParams.put("PAGESIZE", gmCRMDeviceDetailVO.getPagesize());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmCRMDeviceDetailVO.setArrgmcriteriaattrvo(listCriteriaAttrVO);

    return gmCRMDeviceDetailVO;

  }

}
