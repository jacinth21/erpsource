package com.globus.webservice.crm.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmWSUtil;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.bean.GmCRMSearchBean;
import com.globus.webservice.crm.bean.GmCRMSurgeonBean;
import com.globus.webservice.crm.bean.GmCriteriaBean;
import com.globus.webservice.crm.bean.GmLeadBean;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonAddressListVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonContactsListVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonlistVO;
import com.globus.webservice.crm.valueobject.GmCriteriaVO;
import com.globus.webservice.crm.valueobject.GmLeadBasicVO;
import com.globus.webservice.crm.valueobject.GmLeadListVO;
import com.globus.webservice.crm.valueobject.GmCRMListVO;

@Path("lead")
public class GmLeadResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @POST
  @Path("info")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO fetchLeadInfo(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmLeadBean gmLeadBean = new GmLeadBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmLeadBasicVO gmLeadBasicVO = mapper.readValue(strInput, GmLeadBasicVO.class);
    List<GmLeadBasicVO> listGmLeadBasicVO = new ArrayList<GmLeadBasicVO>();
    HashMap hmOutput = new HashMap();
    HashMap hmInput = new HashMap();

    validateToken(gmLeadBasicVO.getToken());
    gmLeadBasicVO.setUserid(gmUserVO.getUserid());

    gmWSUtil.parseJsonToVO(strInput, gmLeadBasicVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmLeadBasicVO);

    listGmLeadBasicVO = gmLeadBean.fetchLeadReport(hmInput, gmLeadBasicVO);
    gmCRMListVO.setGmLeadBasicVO(listGmLeadBasicVO);
    return gmCRMListVO;
  }

  @POST
  @Path("save")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmLeadBasicVO saveLeadInfo(String strInput) throws Exception, AppError,
      JsonParseException, JsonMappingException, IOException {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmLeadBean gmLeadBean = new GmLeadBean();
    ObjectMapper mapper = new ObjectMapper();
    GmLeadBasicVO gmLeadBasicVO = mapper.readValue(strInput, GmLeadBasicVO.class);
    GmDBManager gmDBManager = new GmDBManager();
    HashMap hmInput = new HashMap();

    validateToken(gmLeadBasicVO.getToken());
    gmLeadBasicVO.setUserid(gmUserVO.getUserid());

    gmWSUtil.parseJsonToVO(strInput, gmLeadBasicVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmLeadBasicVO);

    gmLeadBasicVO.setLeadid(gmLeadBean.saveLeadInfo(hmInput, gmDBManager));
    gmDBManager.commit();

    return gmLeadBasicVO;
  }

  @POST
  @Path("multisave")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmLeadBasicVO saveMultipleLead(String strInput) throws Exception, AppError,
      JsonParseException, JsonMappingException, IOException {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmLeadBean gmLeadBean = new GmLeadBean();
    GmCRMSearchBean gmCRMSearchBean = new GmCRMSearchBean();
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    GmLeadBasicVO gmLeadBasicVO = new GmLeadBasicVO();
    ObjectMapper mapper = new ObjectMapper();
    GmLeadListVO gmLeadListVO = mapper.readValue(strInput, GmLeadListVO.class);
    GmLeadBasicVO[] arrGmLeadBasicVO = gmLeadListVO.getArrleadlist();
    GmDBManager gmDBManager = new GmDBManager();

    validateToken(gmLeadListVO.getToken());

    ArrayList alLeads = new ArrayList();
    HashMap hmInput = new HashMap();

    for (int i = 0; i < arrGmLeadBasicVO.length; i++) {
      HashMap hmLead = new HashMap();
      HashMap hmParty = new HashMap();
      ArrayList alParty = new ArrayList();
      String strPartyId = "";


      GmLeadBasicVO gmLeadSingleVO = arrGmLeadBasicVO[i];
      gmLeadSingleVO.setUserid(gmUserVO.getUserid());
      hmLead = gmWSUtil.getHashMapFromVO(gmLeadSingleVO);
      GmCRMSurgeonlistVO gmCRMSurgeonlistVO = gmLeadSingleVO.getGmCRMSurgeonlistVO();
      List<GmCRMSurgeonContactsListVO> listGmCRMSurgeonContactsListVO =
          gmCRMSurgeonlistVO.getArrgmsurgeoncontactsvo();
      List<GmCRMSurgeonAddressListVO> listGmCRMSurgeonAddressListVO =
          gmCRMSurgeonlistVO.getArrgmsurgeonsaddressvo();

      log.debug("hmLead Contact >>> "
          + gmWSUtil.getHashMapFromVO(listGmCRMSurgeonContactsListVO.get(0)));

      hmParty.put("FNAME", gmCRMSurgeonlistVO.getFirstnm());
      hmParty.put("LNAME", gmCRMSurgeonlistVO.getLastnm());

      alParty = gmCRMSearchBean.fetchPartyInfo(hmParty);

      log.debug("alParty.size() >>> " + alParty.size());
      if (alParty.size() > 0) {
        strPartyId = GmCommonClass.parseNull((String) ((HashMap) alParty.get(0)).get("PARTYID"));
        log.debug("(HashMap) alParty.get(0)).get('PARTYID') >>>" + alParty.get(0));
      } else {
        hmParty.put("FIRSTNM", gmCRMSurgeonlistVO.getFirstnm());
        hmParty.put("LASTNM", gmCRMSurgeonlistVO.getLastnm());
        hmParty.put("USERID", gmUserVO.getUserid());
        strPartyId = gmCRMSurgeonBean.savePartyInfo(hmParty, gmDBManager);
      }

      hmParty.put("PARTYID", strPartyId);

      log.debug("hmParty >>> " + hmParty);

      gmCRMSurgeonBean.saveAddressInfo(hmParty, gmDBManager, listGmCRMSurgeonAddressListVO);
      gmCRMSurgeonBean.saveContactInfo(hmParty, gmDBManager, listGmCRMSurgeonContactsListVO);

      hmLead.put("PARTYID", strPartyId);

      gmLeadSingleVO.setLeadid(gmLeadBean.saveLeadInfo(hmLead, gmDBManager));
      hmLead = gmWSUtil.getHashMapFromVO(gmLeadSingleVO);
      alLeads.add(hmLead);
    }

    gmDBManager.commit();

    log.debug("alLeads >>>> " + alLeads);

    return gmLeadBasicVO;
  }

  @POST
  @Path("assign")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmLeadBasicVO assignLead(String strInput) throws Exception, AppError, JsonParseException,
      JsonMappingException, IOException {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmLeadBean gmLeadBean = new GmLeadBean();
    ObjectMapper mapper = new ObjectMapper();
    GmLeadBasicVO gmLeadBasicVO = mapper.readValue(strInput, GmLeadBasicVO.class);
    HashMap hmInput = new HashMap();

    validateToken(gmLeadBasicVO.getToken());
    gmLeadBasicVO.setUserid(gmUserVO.getUserid());

    gmWSUtil.parseJsonToVO(strInput, gmLeadBasicVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmLeadBasicVO);

    gmLeadBasicVO.setToken("");
    gmLeadBasicVO.setRepnm(gmLeadBean.assignRep(hmInput));

    return gmLeadBasicVO;
  }

  @POST
  @Path("criterialist")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listLeadInfoByCriteria(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmLeadBasicVO gmLeadBasicVO = new GmLeadBasicVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmLeadBean gmLeadBean = new GmLeadBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmLeadBasicVO> listGmLeadBasicVO = new ArrayList<GmLeadBasicVO>();

    ArrayList alCriteria = new ArrayList();
    ArrayList alReturn = new ArrayList();

    GmCriteriaBean gmCriteriaBean = new GmCriteriaBean();
    GmCriteriaVO gmCriteriaVO = new GmCriteriaVO();

    HashMap hmCriteria = new HashMap();
    HashMap hmInput = new HashMap();

    gmWSUtil.parseJsonToVO(strInput, gmCriteriaVO);

    validateToken(gmCriteriaVO.getToken());

    gmLeadBasicVO.setUserid(gmUserVO.getUserid());

    log.debug("User ID>>>>>" + gmUserVO.getUserid());

    alCriteria =
        gmCriteriaBean.fetchCriteriaBy(gmCriteriaVO.getCriteriaid(), gmCriteriaVO.getCriteriatyp());

    for (int i = 0; i < alCriteria.size(); i++) {
      HashMap hmValues = GmCommonClass.parseNullHashMap((HashMap) alCriteria.get(i));
      String strKey = GmCommonClass.parseNull((String) hmValues.get("KEY"));
      String strValue = GmCommonClass.parseNull((String) hmValues.get("VALUE"));
      hmCriteria.put(strKey, strValue);
    }


    gmWSUtil.getVOFromHashMap(hmCriteria, gmLeadBasicVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmLeadBasicVO);

    listGmLeadBasicVO = gmLeadBean.fetchLeadReport(hmInput, gmLeadBasicVO);

    log.debug("listGmCRMSurgeonlistVO>>>>" + listGmLeadBasicVO);
    gmCRMListVO.setGmLeadBasicVO(listGmLeadBasicVO);
    return gmCRMListVO;
  }

}
