package com.globus.webservice.crm.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.webservice.crm.bean.GmCRMTradeshowRptBean;
import com.globus.webservice.crm.valueobject.GmCRMAttendeeMailVO;
import com.globus.webservice.crm.valueobject.GmCRMTradeshowVO;
import com.globus.webservice.common.resources.GmResource;

/**
 * @author TMuthusamy
 * GmCRMTradeshowRptResource ---- Tradeshow Reports
 */
@Path("tradeshow")
public class GmCRMTradeshowRptResource extends GmResource {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * This method used to fetch the tradeshow Report
	 * @param strInput
	 * @return
	 * @throws AppError, JsonParseException,JsonMappingException,IOException
	 */
	@POST
	@Path("dash")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String loadTradeshowDash(String strInput) throws AppError,
			JsonMappingException, JsonParseException, IOException, Exception {

		log.debug("strInput=>" + strInput);
		ObjectMapper mapper = new ObjectMapper();

		// Converting JSON input string into VO
		GmCRMTradeshowVO gmCRMTradeshowVO = mapper.readValue(strInput, GmCRMTradeshowVO.class);

		// Validate whether the user token active?
		validateToken(gmCRMTradeshowVO.getToken());
		GmCRMTradeshowRptBean gmCRMTradeshowRptBean = new GmCRMTradeshowRptBean(gmCRMTradeshowVO);
		String tradeDash = gmCRMTradeshowRptBean.fetchTradeshowDash(strInput);

		return tradeDash;
	}
	
	/**
	 * This method used to fetch the tradeshow Attendee Report
	 * @param strInput
	 * @return
	 * @throws AppError, JsonParseException,JsonMappingException,IOException
	 */
	@POST
	@Path("attendeerpt")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String loadTradeshowAttendeeReport(String strInput) throws AppError,
			JsonMappingException, JsonParseException, IOException, Exception {

		log.debug("strInput=>" + strInput);
		ObjectMapper mapper = new ObjectMapper();

		// Converting JSON input string into VO
		GmCRMTradeshowVO gmCRMTradeshowVO = mapper.readValue(strInput, GmCRMTradeshowVO.class);

		// Validate whether the user token active?
		validateToken(gmCRMTradeshowVO.getToken());
		GmCRMTradeshowRptBean gmCRMTradeshowRptBean = new GmCRMTradeshowRptBean(gmCRMTradeshowVO);
		
		String strReturnJSON = gmCRMTradeshowRptBean.fetchtradeshowAttendeeRpt(strInput);

		return strReturnJSON;
	}
	
	/**
	 * This method used to fetch the tradeshow Attendee Email 
	 * @param strInput
	 * @return
	 * @throws AppError, JsonParseException,JsonMappingException,IOException
	 */
	@POST
	@Path("attendeemail")
	public GmCRMAttendeeMailVO sendAttendeeMail(String strInput) throws AppError,
			JsonMappingException, JsonParseException, IOException, Exception {
		ObjectMapper mapper = new ObjectMapper();
		GmWSUtil gmWSUtil = new GmWSUtil();
		// Converting JSON input string into VO
		GmCRMAttendeeMailVO gmCRMAttendeeMailVO = mapper.readValue(strInput, GmCRMAttendeeMailVO.class);
		
		HashMap hmInput = new HashMap();
		GmCRMTradeshowRptBean gmCRMTradeshowRptBean = new GmCRMTradeshowRptBean(gmCRMAttendeeMailVO);
		hmInput = gmWSUtil.getHashMapFromVO(gmCRMAttendeeMailVO);
		String strMailResult = gmCRMTradeshowRptBean.sendTradeshowAttendeeMail(hmInput);
		gmCRMAttendeeMailVO.setMailResult(strMailResult);
		return gmCRMAttendeeMailVO;
	}


}
