package com.globus.webservice.crm.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.bean.GmCRMCRFBean;
import com.globus.webservice.crm.bean.GmCriteriaBean;
import com.globus.webservice.crm.valueobject.GmCRFApprovalVO;
import com.globus.webservice.crm.valueobject.GmCRMCRFVO;
import com.globus.webservice.crm.valueobject.GmCRMDeviceDetailVO;
import com.globus.webservice.crm.valueobject.GmCRMLogVO;
import com.globus.webservice.crm.valueobject.GmCriteriaVO;
import com.globus.webservice.crm.valueobject.GmCRMListVO;

@Path("crf")
public class GmCRMCRFResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @POST
  @Path("info")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMCRFVO fetchCRFInfo(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMCRFBean gmCRMCRFBean = new GmCRMCRFBean();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMCRFVO gmCRMCRFVO = mapper.readValue(strInput, GmCRMCRFVO.class);
    HashMap hmOutput = new HashMap();
    HashMap hmInput = new HashMap();

    validateToken(gmCRMCRFVO.getToken());
    gmCRMCRFVO.setUserid(gmUserVO.getUserid());

    gmWSUtil.parseJsonToVO(strInput, gmCRMCRFVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMCRFVO);

    gmCRMCRFBean.fetchCRFReport(hmInput, gmCRMCRFVO);

    return gmCRMCRFVO;
  }

  @POST
  @Path("save")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMCRFVO saveCRF(String strInput) throws Exception, AppError, JsonParseException,
      JsonMappingException, IOException {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMCRFBean gmCRMCRFBean = new GmCRMCRFBean();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMCRFVO gmCRMCRFVO = mapper.readValue(strInput, GmCRMCRFVO.class);
    HashMap hmInput = new HashMap();

    validateToken(gmCRMCRFVO.getToken());
    gmCRMCRFVO.setUserid(gmUserVO.getUserid());
    GmCRMLogVO gmCRMLogVO = new GmCRMLogVO();

    // gmWSUtil.parseJsonToVO(strInput, gmCRMCRFVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMCRFVO);
    List<GmCRMLogVO> arrCrmLogVO = gmCRMCRFVO.getArrgmcrmlogvo();
    gmCRMCRFVO.setCrfid(gmCRMCRFBean.saveCRFInfo(hmInput, arrCrmLogVO, gmCRMLogVO));
    // String strActId = "";
    // strActId =
    // gmCRMCRFBean.saveCRFInfo(hmInput, arrCrmLogVO, gmCRMLogVO);
    // gmCRMCRFVO.setActid(strActId);

    return gmCRMCRFVO;
  }

  @POST
  @Path("sync")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMDeviceDetailVO listSurgeonDocumentSyncInfo(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMDeviceDetailVO gmCRMDeviceDetailVO = new GmCRMDeviceDetailVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    gmWSUtil.parseJsonToVO(strInput, gmCRMDeviceDetailVO);
    String strUpdate = gmCRMDeviceDetailVO.getUpdate();
    log.debug("strUpdate>>>>" + strUpdate);
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmCRMDeviceDetailVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmCRMDeviceDetailVO, hmValidate);

    GmCRMCRFVO gmCRMCrfVO = new GmCRMCRFVO();
    GmCRMCRFBean gmCRMCrfBean = new GmCRMCRFBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmCRMCRFVO> listGmCRMCrfVO = new ArrayList<GmCRMCRFVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmCRMDeviceDetailVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getUuid()));

    if (strUpdate.equals("")) {
      alResult = GmCommonClass.parseNullArrayList(gmCRMCrfBean.fetchCRFSyncReport(hmParams));
    } else {
      alResult = GmCommonClass.parseNullArrayList(gmCRMCrfBean.fetchCRFUpdateReport(hmParams));
    }

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmCRMCrfVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmCRMCrfVO, gmCRMCrfVO.getMappingProperties());
    // Set the page header detail from first hashmap of list.
    gmCRMDeviceDetailVO.populatePageProperties(alResult);
    gmCRMDeviceDetailVO.setReftype("10306176");// Part Number sync type
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", gmCRMDeviceDetailVO.getReftype());
    hmParams.put("PAGESIZE", gmCRMDeviceDetailVO.getPagesize());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmCRMDeviceDetailVO.setArrgmcrfvo(listGmCRMCrfVO);

    return gmCRMDeviceDetailVO;

  }

  @POST
  @Path("apprsync")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMDeviceDetailVO listSurgeonCRFApproverSyncInfo(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {

    GmCRMDeviceDetailVO gmCRMDeviceDetailVO = new GmCRMDeviceDetailVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    gmWSUtil.parseJsonToVO(strInput, gmCRMDeviceDetailVO);
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmCRMDeviceDetailVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmCRMDeviceDetailVO, hmValidate);

    GmCRFApprovalVO gmCRMCrfApprvVO = new GmCRFApprovalVO();
    GmCRMCRFBean gmCRMCrfBean = new GmCRMCRFBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmCRFApprovalVO> listGmCRMCrfApprvVO = new ArrayList<GmCRFApprovalVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmCRMDeviceDetailVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmCRMDeviceDetailVO.getUuid()));


    alResult = GmCommonClass.parseNullArrayList(gmCRMCrfBean.fetchCRFApprvSyncReport(hmParams));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmCRMCrfApprvVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmCRMCrfApprvVO,
            gmCRMCrfApprvVO.getMappingProperties());
    // Set the page header detail from first hashmap of list.
    gmCRMDeviceDetailVO.populatePageProperties(alResult);
    gmCRMDeviceDetailVO.setReftype("10306206");// Approver sync type
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", gmCRMDeviceDetailVO.getReftype());
    hmParams.put("PAGESIZE", gmCRMDeviceDetailVO.getPagesize());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmCRMDeviceDetailVO.setArrgmcrfapprvvo(listGmCRMCrfApprvVO);

    return gmCRMDeviceDetailVO;

  }


  @POST
  @Path("setstatus")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRFApprovalVO setCRFStatus(String strInput) throws Exception, AppError,
      JsonParseException, JsonMappingException, IOException {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMCRFBean gmCRMCRFBean = new GmCRMCRFBean();
    ObjectMapper mapper = new ObjectMapper();
    GmCRFApprovalVO gmCRFApprovalVO = mapper.readValue(strInput, GmCRFApprovalVO.class);
    List<GmCRFApprovalVO> listGmCRFApprovalVO = new ArrayList<GmCRFApprovalVO>();
    HashMap hmInput = new HashMap();
    // ArrayList alResult = new ArrayList();

    validateToken(gmCRFApprovalVO.getToken());
    gmCRFApprovalVO.setUserid(gmUserVO.getUserid());

    gmWSUtil.parseJsonToVO(strInput, gmCRFApprovalVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRFApprovalVO);
    log.debug("setstatus form crf resource" + hmInput);
    gmCRMCRFBean.setCRFStatus(hmInput);

    // alResult = gmCRMCRFBean.fetchApprovalInfo(gmCRFApprovalVO.getCrfid());
    // listGmCRFApprovalVO =
    // gmWSUtil.getVOListFromHashMapList(alResult, gmCRFApprovalVO,
    // gmCRFApprovalVO.getMappingProperties());

    // gmCRMCRFVO.setCrfid(gmCRMCRFBean.saveCRFInfo(hmInput));

    return gmCRFApprovalVO;
  }

  @POST
  @Path("getstatus")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO getCRFStatus(String strInput) throws Exception, AppError,
      JsonParseException, JsonMappingException, IOException {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMCRFBean gmCRMCRFBean = new GmCRMCRFBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMCRFVO gmCRMCRFVO = mapper.readValue(strInput, GmCRMCRFVO.class);
    List<GmCRMCRFVO> listGmCRFVO = new ArrayList<GmCRMCRFVO>();
    HashMap hmInput = new HashMap();
    ArrayList alResult = new ArrayList();

    validateToken(gmCRMCRFVO.getToken());
    gmCRMCRFVO.setUserid(gmUserVO.getUserid());

    gmWSUtil.parseJsonToVO(strInput, gmCRMCRFVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMCRFVO);

    alResult = gmCRMCRFBean.getCRFStatus(hmInput);
    log.debug("get crf status hmInput" + hmInput);
    listGmCRFVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmCRMCRFVO, gmCRMCRFVO.getMappingProperties());
    log.debug("get crf status listGmCRFVO" + listGmCRFVO);
    // gmCRMCRFVO.setCrfid(gmCRMCRFBean.saveCRFInfo(hmInput));
    gmCRMListVO.setGmCRMCRFVO(listGmCRFVO);
    return gmCRMListVO;
  }

  @POST
  @Path("list")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO fetchCRFReport(String strInput) throws Exception, AppError,
      JsonParseException, JsonMappingException, IOException {
    HashMap hmInput = new HashMap();
    ArrayList alResult = new ArrayList();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMCRFBean gmCRMCRFBean = new GmCRMCRFBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMCRFVO gmCRMCRFVO = mapper.readValue(strInput, GmCRMCRFVO.class);
    List<GmCRMCRFVO> listGmCRMCRFVO = new ArrayList<GmCRMCRFVO>();

    validateToken(gmCRMCRFVO.getToken());
    gmCRMCRFVO.setUserid(gmUserVO.getUserid());

    gmWSUtil.parseJsonToVO(strInput, gmCRMCRFVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMCRFVO);

    alResult = gmCRMCRFBean.fetchCRFReport(hmInput);
    log.debug("fetchCRFReport alResult size =" + alResult.size());
    listGmCRMCRFVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmCRMCRFVO, gmCRMCRFVO.getMappingProperties());

    log.debug("listGmCRMCRFVO =" + listGmCRMCRFVO);
    gmCRMListVO.setGmCRMCRFVO(listGmCRMCRFVO);
    return gmCRMListVO;

  }

  @POST
  @Path("criterialist")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO listCriteriaCRFInfo(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {


    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMCRFBean gmCRMCRFBean = new GmCRMCRFBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMCRFVO gmCRMCRFVO = new GmCRMCRFVO();
    List<GmCRMCRFVO> listGmCRFVO = new ArrayList<GmCRMCRFVO>();

    GmCriteriaBean gmCriteriaBean = new GmCriteriaBean();
    GmCriteriaVO gmCriteriaVO = new GmCriteriaVO();

    gmWSUtil.parseJsonToVO(strInput, gmCriteriaVO);

    validateToken(gmCriteriaVO.getToken());

    gmCRMCRFVO.setUserid(gmUserVO.getUserid());

    log.debug("User ID>>>>>" + gmUserVO.getUserid());

    ArrayList alCriteria = new ArrayList();
    alCriteria = gmCriteriaBean.fetchCriteriaBy(gmCriteriaVO.getCriteriaid(), "crf");

    HashMap hmCriteria = new HashMap();

    for (int i = 0; i < alCriteria.size(); i++) {
      HashMap hmValues = GmCommonClass.parseNullHashMap((HashMap) alCriteria.get(i));
      String strKey = GmCommonClass.parseNull((String) hmValues.get("KEY"));
      String strValue = GmCommonClass.parseNull((String) hmValues.get("VALUE"));
      hmCriteria.put(strKey, strValue);
    }

    ArrayList alReturn = new ArrayList();
    HashMap hmInput = new HashMap();

    gmWSUtil.getVOFromHashMap(hmCriteria, gmCRMCRFVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMCRFVO);


    alReturn = GmCommonClass.parseNullArrayList(gmCRMCRFBean.fetchCRFReport(hmInput));

    log.debug("alRerutn>>>" + alReturn);

    listGmCRFVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMCRFVO, gmCRMCRFVO.getMappingProperties());

    log.debug("listGmCRMSurgeonlistVO>>>>" + listGmCRFVO);
    gmCRMListVO.setGmCRMCRFVO(listGmCRFVO);
    return gmCRMListVO;
  }

}
