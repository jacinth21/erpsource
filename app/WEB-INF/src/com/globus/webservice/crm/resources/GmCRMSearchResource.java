package com.globus.webservice.crm.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.webservice.common.bean.GmMasterRedisReportBean;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.bean.GmCRMSearchBean;
import com.globus.webservice.crm.valueobject.GmAccountBasicVO;
import com.globus.webservice.crm.valueobject.GmCRMListVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonEducationVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonProfessionalVO;
import com.globus.webservice.crm.valueobject.GmCRMUserInfoVO;
import com.globus.webservice.crm.valueobject.GmCodeLookUpVO;
import com.globus.webservice.crm.valueobject.GmPartyBasicVO;
import com.globus.webservice.crm.valueobject.GmProjectBasicVO;
import com.globus.webservice.crm.valueobject.GmSalesRepQuickVO;
import com.globus.webservice.crm.valueobject.GmSystemBasicVO;
import com.globus.webservice.crm.valueobject.GmSegmentBasicVO;

@Path("search")
public class GmCRMSearchResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @POST
  @Path("codelookup")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO searchCodeLookup(String strInput) throws AppError, JsonGenerationException,
      JsonMappingException, JsonParseException, IOException, Exception {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCodeLookUpVO gmCodeLookUpVO = new GmCodeLookUpVO();
    GmCRMSearchBean gmCRMSearchBean = new GmCRMSearchBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmCodeLookUpVO> listCodeLookUpVO = new ArrayList<GmCodeLookUpVO>();

    gmWSUtil.parseJsonToVO(strInput, gmCodeLookUpVO);

    validateToken(gmCodeLookUpVO.getToken());
    gmCodeLookUpVO.setUserid(gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    hmInput = gmWSUtil.getHashMapFromVO(gmCodeLookUpVO);

    ArrayList alReturn = new ArrayList();

    alReturn = GmCommonClass.parseNullArrayList(gmCRMSearchBean.fetchCodeLookUpValues(hmInput));

    listCodeLookUpVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCodeLookUpVO,
            gmCodeLookUpVO.getMappingProperties());
    gmCRMListVO.setGmCodeLookUpVO(listCodeLookUpVO);
    return gmCRMListVO;
  }


  @POST
  @Path("account")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO searchAccount(String strInput) throws AppError, JsonGenerationException,
      JsonMappingException, JsonParseException, IOException, Exception {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmAccountBasicVO gmAccountBasicVO = new GmAccountBasicVO();
    GmCRMSearchBean gmCRMSearchBean = new GmCRMSearchBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmAccountBasicVO> listAccountBasicVO = new ArrayList<GmAccountBasicVO>();

    gmWSUtil.parseJsonToVO(strInput, gmAccountBasicVO);

    validateToken(gmAccountBasicVO.getToken());
    gmAccountBasicVO.setUserid(gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    hmInput = gmWSUtil.getHashMapFromVO(gmAccountBasicVO);

    ArrayList alReturn = new ArrayList();
    if (gmAccountBasicVO.getStropt().equalsIgnoreCase("parent"))
    alReturn = GmCommonClass.parseNullArrayList(gmCRMSearchBean.fetchParentAccountInfo(hmInput));
    else
    alReturn = GmCommonClass.parseNullArrayList(gmCRMSearchBean.fetchAccountInfo(hmInput));

    listAccountBasicVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmAccountBasicVO,
            gmAccountBasicVO.getMappingProperties());
    gmCRMListVO.setGmAccountBasicVO(listAccountBasicVO);
    return gmCRMListVO;
  }

  /**
   * searchEducation - This method will search Education Details list in surgeon module from T1010
   * table
   * 
   * @param - HashMap
   * @return - ArrayList
   * @exception - AppError
   */

  @POST
  @Path("education")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO searchEducation(String strInput) throws AppError, JsonGenerationException,
      JsonMappingException, JsonParseException, IOException, Exception {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMSurgeonEducationVO gmCRMSurgeonEducationVO = new GmCRMSurgeonEducationVO();
    GmCRMSearchBean gmCRMSearchBean = new GmCRMSearchBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmCRMSurgeonEducationVO> listEducationVO = new ArrayList<GmCRMSurgeonEducationVO>();

    gmWSUtil.parseJsonToVO(strInput, gmCRMSurgeonEducationVO);

    validateToken(gmCRMSurgeonEducationVO.getToken());
    gmCRMSurgeonEducationVO.setUserid(gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMSurgeonEducationVO);

    ArrayList alReturn = new ArrayList();

    alReturn = GmCommonClass.parseNullArrayList(gmCRMSearchBean.fetchEducation(hmInput));

    listEducationVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMSurgeonEducationVO,
            gmCRMSurgeonEducationVO.getMappingProperties());
    gmCRMListVO.setGmCRMSurgeonEducationVO(listEducationVO);
    return gmCRMListVO;
  }

  /**
   * searchProfessional - This method will search Professional Details list in surgeon module from
   * T1020 table
   * 
   * @param - HashMap
   * @return - ArrayList
   * @exception - AppError
   */

  @POST
  @Path("professional")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO searchProfessional(String strInput) throws AppError, JsonGenerationException,
      JsonMappingException, JsonParseException, IOException, Exception {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMSurgeonProfessionalVO gmCRMSurgeonProfessionalVO = new GmCRMSurgeonProfessionalVO();
    GmCRMSearchBean gmCRMSearchBean = new GmCRMSearchBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmCRMSurgeonProfessionalVO> listProfessionalVO =
        new ArrayList<GmCRMSurgeonProfessionalVO>();

    gmWSUtil.parseJsonToVO(strInput, gmCRMSurgeonProfessionalVO);

    validateToken(gmCRMSurgeonProfessionalVO.getToken());
    gmCRMSurgeonProfessionalVO.setUserid(gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMSurgeonProfessionalVO);

    ArrayList alReturn = new ArrayList();

    alReturn = GmCommonClass.parseNullArrayList(gmCRMSearchBean.fetchProfessional(hmInput));

    listProfessionalVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmCRMSurgeonProfessionalVO,
            gmCRMSurgeonProfessionalVO.getMappingProperties());
    gmCRMListVO.setGmCRMSurgeonProfessionalVO(listProfessionalVO);
    return gmCRMListVO;
  }

  @POST
  @Path("system")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO searchSystem(String strInput) throws AppError, JsonGenerationException,
      JsonMappingException, JsonParseException, IOException, Exception {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSystemBasicVO gmSystemBasicVO = new GmSystemBasicVO();
    GmCRMSearchBean gmCRMSearchBean = new GmCRMSearchBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmSystemBasicVO> listSystemBasicVO = new ArrayList<GmSystemBasicVO>();

    gmWSUtil.parseJsonToVO(strInput, gmSystemBasicVO);

    validateToken(gmSystemBasicVO.getToken());
    gmSystemBasicVO.setUserid(gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    hmInput = gmWSUtil.getHashMapFromVO(gmSystemBasicVO);

    ArrayList alReturn = new ArrayList();
    if (gmSystemBasicVO.getStropt().equalsIgnoreCase("physicianvisit"))
      alReturn = GmCommonClass.parseNullArrayList(gmCRMSearchBean.fetchCRMVisitSystemInfo(hmInput));
    else
      alReturn = GmCommonClass.parseNullArrayList(gmCRMSearchBean.fetchSystemInfo(hmInput));

    listSystemBasicVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmSystemBasicVO,
            gmSystemBasicVO.getMappingProperties());
    gmCRMListVO.setGmSystemBasicVO(listSystemBasicVO);
    return gmCRMListVO;
  }
  
  
  /**
   * This method is to search segments or procedures by segment name as input
   * @param strInput
   * @return GmCRMListVO
   */
  @POST
  @Path("segment")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO searchSegment(String strInput) throws AppError, JsonGenerationException,
      JsonMappingException, JsonParseException, IOException, Exception {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSegmentBasicVO gmSegmentBasicVO = new GmSegmentBasicVO();
    GmCRMSearchBean gmCRMSearchBean = new GmCRMSearchBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmSegmentBasicVO> listSegmentBasicVO = new ArrayList<GmSegmentBasicVO>();

    gmWSUtil.parseJsonToVO(strInput, gmSegmentBasicVO);

    validateToken(gmSegmentBasicVO.getToken());
    gmSegmentBasicVO.setUserid(gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    hmInput = gmWSUtil.getHashMapFromVO(gmSegmentBasicVO);

    ArrayList alReturn = new ArrayList();
      alReturn = GmCommonClass.parseNullArrayList(gmCRMSearchBean.fetchSegmentInfo(hmInput));

    listSegmentBasicVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmSegmentBasicVO,
            gmSegmentBasicVO.getMappingProperties());
    gmCRMListVO.setGmSegmentBasicVO(listSegmentBasicVO);
    return gmCRMListVO;
  }


  @POST
  @Path("party")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO searchParty(String strInput) throws AppError, JsonGenerationException,
      JsonMappingException, JsonParseException, IOException, Exception {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmPartyBasicVO gmPartyBasicVO = new GmPartyBasicVO();
    GmCRMSearchBean gmCRMSearchBean = new GmCRMSearchBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmPartyBasicVO> listPartyBasicVO = new ArrayList<GmPartyBasicVO>();

    gmWSUtil.parseJsonToVO(strInput, gmPartyBasicVO);

    validateToken(gmPartyBasicVO.getToken());
    gmPartyBasicVO.setUserid(gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    hmInput = gmWSUtil.getHashMapFromVO(gmPartyBasicVO);

    ArrayList alReturn = new ArrayList();

    alReturn = GmCommonClass.parseNullArrayList(gmCRMSearchBean.fetchPartyInfo(hmInput));

    listPartyBasicVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmPartyBasicVO,
            gmPartyBasicVO.getMappingProperties());
    gmCRMListVO.setGmPartyBasicVO(listPartyBasicVO);
    return gmCRMListVO;
  }

  @POST
  @Path("rsparty")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO fetchPartyList(String strInput) throws AppError, JsonGenerationException,
      JsonMappingException, JsonParseException, IOException, Exception {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmPartyBasicVO gmPartyBasicVO = new GmPartyBasicVO();
    GmCRMSearchBean gmCRMSearchBean = new GmCRMSearchBean();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmPartyBasicVO> listPartyBasicVO = new ArrayList<GmPartyBasicVO>();

    GmMasterRedisReportBean gmMasterRedisReportBean = new GmMasterRedisReportBean(gmPartyBasicVO);
    gmWSUtil.parseJsonToVO(strInput, gmPartyBasicVO);

    validateToken(gmPartyBasicVO.getToken());
    gmPartyBasicVO.setUserid(gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    hmInput = gmWSUtil.getHashMapFromVO(gmPartyBasicVO);

    ArrayList alReturn = new ArrayList();

    String strSearch= (String) hmInput.get("PARTYNM");
    String strType= (String) hmInput.get("PARTYTYPE");
    String strKey = strType.replaceAll(",", "_");
    hmInput.put("KEY", gmMasterRedisReportBean.getAutoCompleteKeys("CRMPARTYLIST"+strKey, "N",""));
    hmInput.put("SEARCH_KEY", strSearch);
    hmInput.put("MAX_CNT", 100);
    hmInput.put("TYPE", strType);
    hmInput.put("CMPID", "");
    String strResult = GmCommonClass.parseNull(gmMasterRedisReportBean.loadSurgeonList(hmInput));
    List<GmPartyBasicVO> partyJsonList  = mapper.readValue(strResult, List.class);
    listPartyBasicVO =
        gmWSUtil.getVOListFromHashMapList(partyJsonList, gmPartyBasicVO,
            gmPartyBasicVO.getMappingProperties());
    gmCRMListVO.setGmPartyBasicVO(listPartyBasicVO);
    return gmCRMListVO;
  }
  
  @POST
  @Path("npid")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO searchNpid(String strInput) throws AppError, JsonGenerationException,
      JsonMappingException, JsonParseException, IOException, Exception {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmPartyBasicVO gmPartyBasicVO = new GmPartyBasicVO();
    GmCRMSearchBean gmCRMSearchBean = new GmCRMSearchBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmPartyBasicVO> listGmPartyBasicVO = new ArrayList<GmPartyBasicVO>();

    gmWSUtil.parseJsonToVO(strInput, gmPartyBasicVO);

    validateToken(gmPartyBasicVO.getToken());

    gmPartyBasicVO.setUserid(gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    hmInput = gmWSUtil.getHashMapFromVO(gmPartyBasicVO);

    ArrayList alReturn = new ArrayList();

    alReturn = GmCommonClass.parseNullArrayList(gmCRMSearchBean.fetchPartyID(hmInput));

    listGmPartyBasicVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmPartyBasicVO,
            gmPartyBasicVO.getMappingProperties());
    if (listGmPartyBasicVO.size() > 0)
      gmCRMListVO.setGmPartyBasicVO(listGmPartyBasicVO);
    else
      gmCRMListVO = null;
    return gmCRMListVO;
  }


  @POST
  @Path("rep")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO searchRep(String strInput) throws AppError, JsonGenerationException,
      JsonMappingException, JsonParseException, IOException, Exception {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesRepQuickVO gmSalesRepQuickVO = new GmSalesRepQuickVO();
    GmCRMSearchBean gmCRMSearchBean = new GmCRMSearchBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmSalesRepQuickVO> listSalesRepQuickVO = new ArrayList<GmSalesRepQuickVO>();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    gmWSUtil.parseJsonToVO(strInput, gmSalesRepQuickVO);

    validateToken(gmSalesRepQuickVO.getToken());
    gmSalesRepQuickVO.setUserid(gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    hmInput = gmWSUtil.getHashMapFromVO(gmSalesRepQuickVO);

    ArrayList alReturn = new ArrayList();

    HashMap hmParam =
        GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
            (new GmSalesDashReqVO())));


    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
    hmInput.put("HMPARAM", hmParam);
    hmInput.put("USERDEPTID", gmUserVO.getDeptid());
    log.debug("hmInput >>>>" + hmInput);

    alReturn = GmCommonClass.parseNullArrayList(gmCRMSearchBean.fetchRepInfo(hmInput));

    listSalesRepQuickVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmSalesRepQuickVO,
            gmSalesRepQuickVO.getMappingProperties());
    gmCRMListVO.setGmSalesRepQuickVO(listSalesRepQuickVO);
    return gmCRMListVO;
  }

  @POST
  @Path("ad")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO searchAD(String strInput) throws AppError, JsonGenerationException,
      JsonMappingException, JsonParseException, IOException, Exception {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesRepQuickVO gmSalesRepQuickVO = new GmSalesRepQuickVO();
    GmCRMSearchBean gmCRMSearchBean = new GmCRMSearchBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmSalesRepQuickVO> listSalesRepQuickVO = new ArrayList<GmSalesRepQuickVO>();

    gmWSUtil.parseJsonToVO(strInput, gmSalesRepQuickVO);

    validateToken(gmSalesRepQuickVO.getToken());
    gmSalesRepQuickVO.setUserid(gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    hmInput = gmWSUtil.getHashMapFromVO(gmSalesRepQuickVO);

    ArrayList alReturn = new ArrayList();

    alReturn = GmCommonClass.parseNullArrayList(gmCRMSearchBean.fetchADInfo(hmInput));

    listSalesRepQuickVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmSalesRepQuickVO,
            gmSalesRepQuickVO.getMappingProperties());
    gmCRMListVO.setGmSalesRepQuickVO(listSalesRepQuickVO);
    return gmCRMListVO;
  }

  @POST
  @Path("dist")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO searchDistributor(String strInput) throws AppError, JsonGenerationException,
      JsonMappingException, JsonParseException, IOException, Exception {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesRepQuickVO gmSalesRepQuickVO = new GmSalesRepQuickVO();
    GmCRMSearchBean gmCRMSearchBean = new GmCRMSearchBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmSalesRepQuickVO> listSalesRepQuickVO = new ArrayList<GmSalesRepQuickVO>();

    gmWSUtil.parseJsonToVO(strInput, gmSalesRepQuickVO);

    validateToken(gmSalesRepQuickVO.getToken());
    gmSalesRepQuickVO.setUserid(gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    hmInput = gmWSUtil.getHashMapFromVO(gmSalesRepQuickVO);

    ArrayList alReturn = new ArrayList();

    alReturn = GmCommonClass.parseNullArrayList(gmCRMSearchBean.fetchDistributorInfo(hmInput));

    listSalesRepQuickVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmSalesRepQuickVO,
            gmSalesRepQuickVO.getMappingProperties());
    gmCRMListVO.setGmSalesRepQuickVO(listSalesRepQuickVO);
    return gmCRMListVO;
  }

  @POST
  @Path("project")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO searchProject(String strInput) throws AppError, JsonGenerationException,
      JsonMappingException, JsonParseException, IOException, Exception {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmProjectBasicVO gmProjectBasicVO = new GmProjectBasicVO();
    GmCRMSearchBean gmCRMSearchBean = new GmCRMSearchBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmProjectBasicVO> listProjectBasicVO = new ArrayList<GmProjectBasicVO>();

    gmWSUtil.parseJsonToVO(strInput, gmProjectBasicVO);

    validateToken(gmProjectBasicVO.getToken());
    gmProjectBasicVO.setUserid(gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    hmInput = gmWSUtil.getHashMapFromVO(gmProjectBasicVO);

    ArrayList alReturn = new ArrayList();

    alReturn = GmCommonClass.parseNullArrayList(gmCRMSearchBean.fetchProjectInfo(hmInput));

    listProjectBasicVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmProjectBasicVO,
            gmProjectBasicVO.getMappingProperties());
    gmCRMListVO.setGmProjectBasicVO(listProjectBasicVO);
    return gmCRMListVO;
  }


  @POST
  @Path("user")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCRMListVO searchUser(String strInput) throws AppError, JsonGenerationException,
      JsonMappingException, JsonParseException, IOException, Exception {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMUserInfoVO gmUserInfoVO = new GmCRMUserInfoVO();
    GmCRMSearchBean gmCRMSearchBean = new GmCRMSearchBean();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    List<GmCRMUserInfoVO> listCrmUserInfoVO = new ArrayList<GmCRMUserInfoVO>();

    gmWSUtil.parseJsonToVO(strInput, gmUserInfoVO);

    validateToken(gmUserInfoVO.getToken());
    gmUserInfoVO.setUserid(gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    hmInput = gmWSUtil.getHashMapFromVO(gmUserInfoVO);

    ArrayList alReturn = new ArrayList();

    alReturn = GmCommonClass.parseNullArrayList(gmCRMSearchBean.fetchUserInfo(hmInput));

    listCrmUserInfoVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmUserInfoVO,
            gmUserInfoVO.getMappingProperties());
    gmCRMListVO.setGmCRMUserInfoVO(listCrmUserInfoVO);
    return gmCRMListVO;
  }
  
  /**
   * This method is to search Rep Details by using Partyid
   * @param strInput
   * @return GmPartyBasicVO
   */
  @POST
	@Path("partyrep")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String searchPartyRep(String strInput)  throws  AppError,
	JsonParseException, JsonMappingException, IOException  {
	    	GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
	    	String strReturnJSON = "";
		    GmPartyBasicVO gmPartylistVO = new GmPartyBasicVO();
		    GmWSUtil gmWSUtil = new GmWSUtil();		   
		    GmCRMSearchBean gmCRMSearchBean = new GmCRMSearchBean();
		    GmAccessFilter gmAccessFilter = new GmAccessFilter();
		    ObjectMapper mapper = new ObjectMapper();
		    HashMap hmInput = new HashMap();
		    GmPartyBasicVO gmPartyBasicVO = mapper.readValue(strInput, GmPartyBasicVO.class);		    
			 
		    validateToken(gmPartyBasicVO.getToken()); 
		    gmPartylistVO.setUserid(gmUserVO.getUserid());
		    log.debug("User ID>>>>>" + gmUserVO.getUserid()); 
		  
		    gmWSUtil.parseJsonToVO(strInput, gmPartylistVO);
		    hmInput = gmWSUtil.getHashMapFromVO(gmPartylistVO); 
		    
		    HashMap hmParam =GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
		              (new GmSalesDashReqVO())));
				   strReturnJSON = gmCRMSearchBean.fetchPartyRepInfo (hmInput);
			log.debug("strReturnJSON"+strReturnJSON.length());
		 
		    return strReturnJSON;
	 
	 
	}


}
