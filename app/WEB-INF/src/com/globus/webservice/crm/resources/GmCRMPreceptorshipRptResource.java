package com.globus.webservice.crm.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.annotation.JacksonFeatures;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.bean.GmCRMPreceptorshipRptBean;
import com.globus.webservice.crm.valueobject.GmCRMActivityVO;
import com.globus.webservice.crm.valueobject.GmCRMListVO;
import com.globus.webservice.crm.valueobject.GmCRMPSCaseVO;

@Path("psrpt")
public class GmCRMPreceptorshipRptResource extends GmResource {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * This method used to Fetch the Case Request Information. Inputs are casereqid,token
   */
  @POST
  @Path("caseinfo")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  @JacksonFeatures(serializationEnable = SerializationFeature.WRAP_ROOT_VALUE,
      deserializationEnable = DeserializationFeature.UNWRAP_ROOT_VALUE)
  public GmCRMListVO fetchCaseRequestInfo(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {
    log.debug("entered resource");
    GmCRMPSCaseVO gmCRMPSCaselistVO = new GmCRMPSCaseVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMListVO gmCRMListVO = new GmCRMListVO();
    GmCRMPreceptorshipRptBean gmCRMPreceptorshipRptBean = new GmCRMPreceptorshipRptBean();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMPSCaseVO gmCRMPSCaseVO = mapper.readValue(strInput, GmCRMPSCaseVO.class);

    List<GmCRMPSCaseVO> listGmCRMPSCaselistVO = new ArrayList<GmCRMPSCaseVO>();
    validateToken(gmCRMPSCaseVO.getToken());
    String strcasereqid = gmCRMPSCaseVO.getCasereqid();
    log.debug(strcasereqid);
    gmCRMPSCaselistVO.setUserid(gmUserVO.getUserid());

    log.debug("User ID>>>>>" + gmUserVO.getUserid());

    HashMap hmInput = new HashMap();

    gmWSUtil.parseJsonToVO(strInput, gmCRMPSCaselistVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMPSCaselistVO);

    String strReturnJSON = gmCRMPreceptorshipRptBean.fetchCaseDetailJSON(strcasereqid);
    log.debug(strReturnJSON);
    gmCRMPSCaselistVO = mapper.readValue(strReturnJSON, GmCRMPSCaseVO.class);
    listGmCRMPSCaselistVO.add(gmCRMPSCaselistVO);

    if (listGmCRMPSCaselistVO.size() > 0)
      gmCRMListVO.setGmCRMPSCaseVO(listGmCRMPSCaselistVO);
    else
      gmCRMListVO = null;
    return gmCRMListVO;

  }

  /**
   * This method used to fetch Lookup Host Case list, object in front-end as a String.
   */
  @POST
  @Path("fetchlkuphcase")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  @JacksonFeatures(serializationEnable = SerializationFeature.WRAP_ROOT_VALUE,
      deserializationEnable = DeserializationFeature.UNWRAP_ROOT_VALUE)
  public String listLkpHostCase(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {
    GmCRMActivityVO gmCRMActivitylistVO = new GmCRMActivityVO();
    GmWSUtil gmWSUtil = new GmWSUtil();

    GmCRMPreceptorshipRptBean gmCRMPreceptorshipRptBean = new GmCRMPreceptorshipRptBean();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMActivityVO gmCRMActivityVO = mapper.readValue(strInput, GmCRMActivityVO.class);
    validateToken(gmCRMActivityVO.getToken());

    String actcat = gmCRMActivityVO.getActcategory();
    gmCRMActivitylistVO.setUserid(gmUserVO.getUserid());
    log.debug("User ID>>>>>" + gmUserVO.getUserid());

    HashMap hmInput = new HashMap();
    HashMap hmResult = new HashMap();

    gmWSUtil.parseJsonToVO(strInput, gmCRMActivitylistVO);
    hmInput = gmWSUtil.getHashMapFromVO(gmCRMActivitylistVO);

    String strReturnJSON = gmCRMPreceptorshipRptBean.fetchLkupHostCaseList(hmInput);

    return strReturnJSON;

  }

  /**
   * This method used to fetch Lookup Case Request list, object in front-end as a String.
   */
  @POST
  @Path("fetchlkupcreq")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  @JacksonFeatures(serializationEnable = SerializationFeature.WRAP_ROOT_VALUE,
      deserializationEnable = DeserializationFeature.UNWRAP_ROOT_VALUE)
  public String listLkpCaseReq(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmInput = new HashMap();

    GmCRMPreceptorshipRptBean gmCRMPreceptorshipRptBean = new GmCRMPreceptorshipRptBean();
    ObjectMapper mapper = new ObjectMapper();
    GmCRMPSCaseVO gmCRMPSCaseVO = mapper.readValue(strInput, GmCRMPSCaseVO.class);

    validateToken(gmCRMPSCaseVO.getToken());

    hmInput = gmWSUtil.getHashMapFromVO(gmCRMPSCaseVO);

    hmInput.put("USERID", gmUserVO.getUserid());

    String strReturnJSON = gmCRMPreceptorshipRptBean.fetchLkupCaseReqList(hmInput);

    return strReturnJSON;

  }
  
  /**
   * This method used to fetch Lookup Hostcase list, object in front-end as a String.
   */
  
  @POST
	@Path("listhost")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String loadHostCaseReport(String strInput)  throws AppError,
  JsonParseException, JsonMappingException, IOException  {
	  		String strReturnJSON = "";
	  		String stOpt = "";
		    GmCRMActivityVO gmCRMActivitylistVO = new GmCRMActivityVO();
		    GmWSUtil gmWSUtil = new GmWSUtil();
		    GmAccessFilter gmAccessFilter = new GmAccessFilter();
		    GmCRMPreceptorshipRptBean gmCRMPreceptorshipRptBean = new GmCRMPreceptorshipRptBean();
		    ObjectMapper mapper = new ObjectMapper();
		    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		    HashMap hmInput = new HashMap();
		    GmCRMActivityVO gmCRMActivityVO = mapper.readValue(strInput, GmCRMActivityVO.class);		    
			 
		    validateToken(gmCRMActivityVO.getToken()); 
		    gmCRMActivitylistVO.setUserid(gmUserVO.getUserid());
		    log.debug("User ID>>>>>" + gmUserVO.getUserid()); 
		  
		    gmWSUtil.parseJsonToVO(strInput, gmCRMActivitylistVO);
		    hmInput = gmWSUtil.getHashMapFromVO(gmCRMActivitylistVO); 
		    
		    // For Override the Access Level of Associate Sales REP to Sales Rep (Associate Sales REP
		      // checks with account id)
		    HashMap hmParam =GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
		              (new GmSalesDashReqVO())));
		    
		      /*
		       * When Associate Rep login. His user id passed as reporting repid. 
		       */
		    hmParam.put("RPT_REPID", gmUserVO.getUserid());
		    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
		    hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));

		    hmInput.put("USERDEPTID", gmUserVO.getDeptid());
		    hmInput.put("STROPT", stOpt);
		    hmInput.put("HMPARAM",GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam)));
		    stOpt = gmCRMActivityVO.getStropt();
		    
		    // Method for fetch the data
		   if( stOpt.equals("ADMIN_UPCOME") || stOpt.equals("ADMIN_All") || stOpt.equals("pschart")){
			    
			   strReturnJSON = gmCRMPreceptorshipRptBean.loadAdminHostCaseJSON(hmInput);
		   }else{
			   
			   strReturnJSON = gmCRMPreceptorshipRptBean.loadHostCaseJSON(hmInput);
		   }

			log.debug("strReturnJSON"+strReturnJSON.length());
		 
		    return strReturnJSON;
	 
	 
	}
  
  /**
   * This method used to fetch Lookup Case Request list, object in front-end as a String.
   */
	@POST
	@Path("listcaserequest")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String loadCaseRequestReport(String strInput)  throws  AppError,
  JsonParseException, JsonMappingException, IOException  {
	    	GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
	    	String stOpt = "";
	    	String strReturnJSON = "";
		    GmCRMActivityVO gmCRMActivitylistVO = new GmCRMActivityVO();
		    GmWSUtil gmWSUtil = new GmWSUtil();		   
		    GmCRMPreceptorshipRptBean gmCRMPreceptorshipRptBean = new GmCRMPreceptorshipRptBean();
		    GmAccessFilter gmAccessFilter = new GmAccessFilter();
		    ObjectMapper mapper = new ObjectMapper();
		    HashMap hmInput = new HashMap();
		    GmCRMActivityVO gmCRMActivityVO = mapper.readValue(strInput, GmCRMActivityVO.class);		    
			 
		    validateToken(gmCRMActivityVO.getToken()); 
		    gmCRMActivitylistVO.setUserid(gmUserVO.getUserid());
		    log.debug("User ID>>>>>" + gmUserVO.getUserid()); 
		  
		    gmWSUtil.parseJsonToVO(strInput, gmCRMActivitylistVO);
		    hmInput = gmWSUtil.getHashMapFromVO(gmCRMActivitylistVO); 
		    // For Override the Access Level of Associate Sales REP to Sales Rep (Associate Sales REP
		      // checks with account id)
		    HashMap hmParam =GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
		              (new GmSalesDashReqVO())));
		    /*
		       * When Associate Rep login. His user id passed as reporting repid. 
		       */
		    hmParam.put("RPT_REPID", gmUserVO.getUserid());
		    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
		    hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));

		    hmInput.put("USERDEPTID", gmUserVO.getDeptid());
		    hmInput.put("STROPT", stOpt);
		    hmInput.put("HMPARAM",GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam)));
		    stOpt = gmCRMActivityVO.getStropt();
		    
		    // Method for fetch the data
			   if(stOpt.equals("ADMIN_All") || stOpt.equals("pschart")){
				    
				   strReturnJSON = gmCRMPreceptorshipRptBean.loadAdminCaseRequestJSON (hmInput);
			   }else{
				   
				   strReturnJSON = gmCRMPreceptorshipRptBean.loadCaseRequestJSON (hmInput);
			   }
			log.debug("strReturnJSON"+strReturnJSON.length());
		 
		    return strReturnJSON;
	 
	 
	}
	
	/**
	   * This method used to fetch Dashboard Count for preceptorship Module Open,upcoming and case request .
	   */
	
	@POST
	@Path("dashcnt")
	@Produces({MediaType.APPLICATION_JSON })
	@Consumes({MediaType.APPLICATION_JSON })
	public HashMap<String,String> fetchDashboardCount(String strInput)  throws  AppError,
  JsonParseException, JsonMappingException, IOException  {
			String strReturnJSON = "";
			
	    	GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		    GmCRMActivityVO gmCRMActivitylistVO = new GmCRMActivityVO();
		    GmWSUtil gmWSUtil = new GmWSUtil();		   
		    GmCRMPreceptorshipRptBean gmCRMPreceptorshipRptBean = new GmCRMPreceptorshipRptBean();
		    GmAccessFilter gmAccessFilter = new GmAccessFilter();
		    ObjectMapper mapper = new ObjectMapper();
		    HashMap hmInput = new HashMap();
		    HashMap<String,String> hmResult = new HashMap<String,String>();
		    GmCRMActivityVO gmCRMActivityVO = mapper.readValue(strInput, GmCRMActivityVO.class);		    
			 
		    validateToken(gmCRMActivityVO.getToken()); 
		    gmCRMActivitylistVO.setUserid(gmUserVO.getUserid());
		    log.debug("User ID>>>>>" + gmUserVO.getUserid()); 
		  
		    gmWSUtil.parseJsonToVO(strInput, gmCRMActivitylistVO);
		    hmInput = gmWSUtil.getHashMapFromVO(gmCRMActivitylistVO); 
		    // For Override the Access Level of Associate Sales REP to Sales Rep (Associate Sales REP
		      // checks with account id)
		    HashMap hmParam =GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
		              (new GmSalesDashReqVO())));
		    /*
		       * When Associate Rep login. His user id passed as reporting repid. 
		       */
		    hmParam.put("RPT_REPID", gmUserVO.getUserid());
		    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
		    hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));

		    hmInput.put("USERDEPTID", gmUserVO.getDeptid());
		    hmInput.put("HMPARAM",GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam)));
		    hmInput.put("STROPT", "COUNT");
		     
		    //Count of Upcoming Host Case
			strReturnJSON = gmCRMPreceptorshipRptBean.loadHostCaseJSON(hmInput);
			hmResult.put("UPCOMEHOST", strReturnJSON);
			//Count of Open Host Case
			hmInput.put("ACTSTATUSID", "105765");
			  
			strReturnJSON = gmCRMPreceptorshipRptBean.loadHostCaseJSON(hmInput);
			log.debug("User ID>>>>>" + gmUserVO.getUserid()); 
			hmResult.put("OPENHOST", strReturnJSON);
			//Count of Open Case Request
			hmInput.put("ACTCATEGORY", "105273");
			hmInput.put("ACTSTARTDATE", "");
			
		    strReturnJSON = gmCRMPreceptorshipRptBean.loadCaseRequestJSON(hmInput);
			hmResult.put("OPENCASEREQ", strReturnJSON);
			   
				  
		    return hmResult;
	 
	 
	}	
	
	
	  /**
	   * This method used to fetch Segment Name and System name.
	   */
	  @POST
	  @Path("fetchsegsys")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  @JacksonFeatures(serializationEnable = SerializationFeature.WRAP_ROOT_VALUE,
	      deserializationEnable = DeserializationFeature.UNWRAP_ROOT_VALUE)
	  public String listSegmentSystem(String strInput) throws AppError, JsonParseException,
	      JsonMappingException, IOException {
	    GmWSUtil gmWSUtil = new GmWSUtil();
	    HashMap hmInput = new HashMap();

	    GmCRMPreceptorshipRptBean gmCRMPreceptorshipRptBean = new GmCRMPreceptorshipRptBean();
	    ObjectMapper mapper = new ObjectMapper();
	    GmCRMPSCaseVO gmCRMPSCaseVO = mapper.readValue(strInput, GmCRMPSCaseVO.class);

	    validateToken(gmCRMPSCaseVO.getToken());

	    hmInput = gmWSUtil.getHashMapFromVO(gmCRMPSCaseVO);

	    hmInput.put("USERID", gmUserVO.getUserid());

	    String strReturnJSON = gmCRMPreceptorshipRptBean.fetchSegSysList(hmInput);

	    return strReturnJSON;

	  }
	  
	  /**
	 * @param strInput
	 * To fetch the sales Rep Zone by passing the user ID
	 * @return strReturnJSON
	 * @throws AppError
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	  @POST
	  @Path("fetchsrepzone")
	  @Produces({ MediaType.APPLICATION_JSON })
	  @Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	  public String listSalesRepZone(String strInput) throws AppError,
			JsonParseException, JsonMappingException, IOException {
		  
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		GmWSUtil gmWSUtil = new GmWSUtil();	
		GmCRMPreceptorshipRptBean gmCRMPreceptorshipRptBean = new GmCRMPreceptorshipRptBean();
		ObjectMapper mapper = new ObjectMapper();
		GmAccessFilter gmAccessFilter = new GmAccessFilter();
		GmCRMPSCaseVO gmCRMPSCaseVO = mapper.readValue(strInput,GmCRMPSCaseVO.class);
		validateToken(gmCRMPSCaseVO.getToken());
		HashMap hmInput = new HashMap();
		
	  
	    gmWSUtil.parseJsonToVO(strInput, gmCRMPSCaseVO);
	    hmInput = gmWSUtil.getHashMapFromVO(gmCRMPSCaseVO); 
	    // For Override the Access Level of Associate Sales REP to Sales Rep (Associate Sales REP
	      // checks with account id)
	    HashMap hmParam =GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
	              (new GmSalesDashReqVO())));
	    /*
	       * When Associate Rep login. His user id passed as reporting repid. 
	       */
	    hmParam.put("RPT_REPID", gmUserVO.getUserid());
	    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
	    hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));

	    hmInput.put("USERDEPTID", gmUserVO.getDeptid());
	    hmInput.put("HMPARAM",GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam)));
		
		String strReturnJSON = gmCRMPreceptorshipRptBean.fetchSalesRepZone(hmInput);
		return strReturnJSON;
	  }
	  
	  
  	  /**
	  * fetch data for host and case dash chart report
	  * @param strInput
	  * @return
	  * @throws AppError, JsonParseException,JsonMappingException,IOException
	  */
	  @POST
	  @Path("dashchart")
	  @Produces({ MediaType.APPLICATION_JSON })
	  @Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	  public String loadChartDash(String strInput) throws AppError,
  			JsonMappingException, JsonParseException, IOException, Exception {
		  	ObjectMapper mapper = new ObjectMapper();
		  	// Converting JSON input string into VO
		  	GmCRMActivityVO gmCRMActivityVO = mapper.readValue(strInput, GmCRMActivityVO.class);
		  
	  		// Validate whether the user token active?
	  		validateToken(gmCRMActivityVO.getToken());
		 	GmCRMPreceptorshipRptBean gmCRMPreceptorshipRptBean = new GmCRMPreceptorshipRptBean();
	  		String pschartdata = gmCRMPreceptorshipRptBean.loadHostCaseChartJSON();
	  
	  		return pschartdata;
	  	}
	   
	  
	  /**
	   * This method used to fetch more filters on calendar scheduler.
	   */
	  @POST
	  @Path("fetchfilters")
	  @Produces({ MediaType.APPLICATION_JSON })
	  @Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	  public String fetchFiltersList(String strInput) throws AppError,
		JsonParseException, JsonMappingException, IOException {
		  ObjectMapper mapper = new ObjectMapper();
		GmCRMPreceptorshipRptBean gmCRMPreceptorshipRptBean = new GmCRMPreceptorshipRptBean();
	    GmCRMPSCaseVO gmCRMPSCaseVO = mapper.readValue(strInput, GmCRMPSCaseVO.class);
	    validateToken(gmCRMPSCaseVO.getToken());
	    String strRegionId = gmCRMPSCaseVO.getRegion();
	    String strReturnJSON = gmCRMPreceptorshipRptBean.fetchFiltersDropdown(strRegionId);
	    return strReturnJSON;

	  }

}
