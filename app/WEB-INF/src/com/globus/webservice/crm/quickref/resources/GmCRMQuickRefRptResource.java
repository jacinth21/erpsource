package com.globus.webservice.crm.quickref.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;


import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.webservice.common.resources.GmResource;
import com.globus.crm.quickref.beans.GmCRMQuickRefRptBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author TMuthusamy GmCRMQuickRefRptResource--- fetch Quick Reference Report details
 */
@Path("quickref")
public class GmCRMQuickRefRptResource extends GmResource {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * This method used to fetch Quick Reference List as JSON String to display in CRM Dashboard
	 */

	@POST
	@Path("list")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String listQuickRef(String strInput) throws AppError,
			JsonMappingException, JsonParseException, IOException, Exception {

		log.debug("strInput=>" + strInput);
		ObjectMapper mapper = new ObjectMapper();

		// Converting JSON input string into VO
		GmDataStoreVO gmDataStoreVO = mapper.readValue(strInput, GmDataStoreVO.class);

		// Validate whether the user token active?
		validateToken(gmDataStoreVO.getToken());
		GmCRMQuickRefRptBean gmCRMQuickRefRptBean= new GmCRMQuickRefRptBean(gmDataStoreVO);
		String strReturnJSON = gmCRMQuickRefRptBean.fetchQuickRefListJSON();

		return strReturnJSON;
	}

}
