package com.globus.webservice.crm.merc.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;


import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.webservice.common.resources.GmResource;
import com.globus.crm.merc.beans.GmCRMMercRptBean;
import com.globus.valueobject.crm.merc.GmCRMMercVO;

/**
 * @author TMuthusamy GmCRMMercRptResource --- MERC Save and Fetch Details
 */
@Path("merc")
public class GmCRMMercRptResource extends GmResource {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * This method used to fetch the Merc details to display in the dashboard
	 * widget object in front-end as a JSONDATA.
	 */

	@POST
	@Path("dash")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String loadMercDash(String strInput) throws AppError,
			JsonMappingException, JsonParseException, IOException, Exception {

		log.debug("strInput=>" + strInput);
		ObjectMapper mapper = new ObjectMapper();

		// Converting JSON input string into VO
		GmCRMMercVO gmCRMMercVO = mapper.readValue(strInput, GmCRMMercVO.class);

		// Validate whether the user token active?
		validateToken(gmCRMMercVO.getToken());
		GmCRMMercRptBean gmCRMMercRptBean = new GmCRMMercRptBean(gmCRMMercVO);
		String mercDash = gmCRMMercRptBean.fetchMercDashboard(strInput);

		return mercDash;
	}

	/**
	 * This method used to fetch the Merc Attendee Report
	 * 
	 * @param strInput
	 * @return
	 * @throws AppError
	 *             , JsonMappingException, JsonParseException, IOException,
	 *             Exception
	 */
	@POST
	@Path("surgeonrpt")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String loadMercAttendeeReport(String strInput) throws AppError,
			JsonMappingException, JsonParseException, IOException, Exception {

		log.debug("strInput=>" + strInput);
		ObjectMapper mapper = new ObjectMapper();
		// Converting JSON input string into VO
		GmCRMMercVO gmCRMMercVO = mapper.readValue(strInput, GmCRMMercVO.class);

		// Validate whether the user token active?
		validateToken(gmCRMMercVO.getToken());
		String strMercid = GmCommonClass.parseNull((String) gmCRMMercVO
				.getMercid());

		GmCRMMercRptBean gmCRMMercRptBean = new GmCRMMercRptBean(gmCRMMercVO);

		String mercattendee = gmCRMMercRptBean
				.fetchMercAttendeeReport(strMercid);

		return mercattendee;
	}

	/**
	 * This method used to fetch the Merc report based on input object in
	 * front-end as a JSONDATA.
	 */

	@POST
	@Path("report")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String loadMERCDetailReport(String strInput) throws AppError,
			JsonParseException, JsonMappingException, IOException {
		log.debug("strInput" + strInput);

		GmCRMMercVO gmCRMMercVO = new GmCRMMercVO();
		GmWSUtil gmWSUtil = new GmWSUtil();

		ObjectMapper mapper = new ObjectMapper();
		gmCRMMercVO = mapper.readValue(strInput, GmCRMMercVO.class);
		validateToken(gmCRMMercVO.getToken());
		GmCRMMercRptBean gmCRMMercRptBean = new GmCRMMercRptBean(gmCRMMercVO);

		HashMap hmInput = new HashMap();

		hmInput = gmWSUtil.getHashMapFromVO(gmCRMMercVO);

		String strReturnJSON = gmCRMMercRptBean.loadMERCDetailReport(hmInput);

		return strReturnJSON;

	}

	/**
	 * This method used to Create MERC EVENT in MERC module. Save the data from
	 * updated model object in front-end as a JSONDATA.
	 */

	@POST
	@Path("save")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmCRMMercVO saveMercDetail(String strInput) throws Exception,
			AppError, JsonParseException, JsonMappingException, IOException {

		log.debug("strInput" + strInput);
		String strCaseReqInputId = "";

		ObjectMapper mapper = new ObjectMapper();
		HashMap hmInput = new HashMap();

		// Converting JSON input string into VO
		GmCRMMercVO gmCRMMercVO = mapper.readValue(strInput, GmCRMMercVO.class);
		// Validate whether the user token active?
		validateToken(gmCRMMercVO.getToken());

		// Declaring bean object with VO.It will pass following values
		// compDateFmt,compTimeZone,compId,compPlantId,complangid respect to
		// company.
		GmCRMMercRptBean gmCRMMercRptBean = new GmCRMMercRptBean(gmCRMMercVO);

		gmCRMMercVO.setUserid(gmUserVO.getUserid());

		hmInput.put("JSONDATA", strInput);
		hmInput.put("USERID", gmUserVO.getUserid());

		String strMercid = gmCRMMercRptBean.saveMercDetail(hmInput);

		gmCRMMercVO.setMercid(strMercid);
		log.debug("gmCRMMercVO>>>>" + gmCRMMercVO);

		return gmCRMMercVO;
	}

	/**
	 * This method used to fetch the Merc report based on input object in
	 * front-end as a JSONDATA.
	 */

	@POST
	@Path("info")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String fetchMercDetail(String strInput) throws AppError,
			JsonParseException, JsonMappingException, IOException {
		log.debug("strInput" + strInput);

		GmCRMMercVO gmCRMMercVO = new GmCRMMercVO();
		GmWSUtil gmWSUtil = new GmWSUtil();

		ObjectMapper mapper = new ObjectMapper();
		gmCRMMercVO = mapper.readValue(strInput, GmCRMMercVO.class);
		validateToken(gmCRMMercVO.getToken());
		GmCRMMercRptBean gmCRMMercRptBean = new GmCRMMercRptBean(gmCRMMercVO);

		HashMap hmInput = new HashMap();

		hmInput = gmWSUtil.getHashMapFromVO(gmCRMMercVO);

		String strReturnJSON = gmCRMMercRptBean.fetchMercDetail(hmInput);

		return strReturnJSON;

	}

}
