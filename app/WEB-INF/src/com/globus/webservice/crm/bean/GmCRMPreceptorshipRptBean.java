package com.globus.webservice.crm.bean;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.filters.GmAccessFilter;
import com.globus.sales.beans.GmSalesFilterConditionBean;

/**
 * @author tmuthusamy
 *
 */
public class GmCRMPreceptorshipRptBean extends GmSalesFilterConditionBean implements
    GmCRMCalendarInterface {
  /**
 * log global object declared
 */
Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * This method used to Fetch the Case Request Information. Inputs are casereqid,token
   */
  public String fetchCaseDetailJSON(String strcasereqid) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strReturn = "";
    gmDBManager.setPrepareString("gm_pkg_crm_preceptorship_rpt.gm_fch_ps_case_info_json", 2);
    gmDBManager.setString(1, strcasereqid);
    gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
    gmDBManager.execute();
    strReturn = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.close();
    return strReturn;
  }



  /**
   * This method used to fetch Lookup Host Case list
   * 
   * @param hmInput
   * @return
   * @throws AppError
   */
  public String fetchLkupHostCaseList(HashMap hmInput) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    log.debug("hmInput" + hmInput);
    String strsegid = GmCommonClass.parseNull((String) hmInput.get("SEGID"));
    String stractcategory = GmCommonClass.parseNull((String) hmInput.get("ACTCATEGORY"));
    String strReturn = "";
    gmDBManager.setPrepareString("gm_pkg_crm_preceptorship_rpt.gm_fch_ps_lkp_host_info_json", 3);
    gmDBManager.setString(1, strsegid);
    gmDBManager.setString(2, stractcategory);
    gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
    gmDBManager.execute();
    strReturn = GmCommonClass.parseNull(gmDBManager.getString(3));
    log.debug("strReturn===" + strReturn);
    gmDBManager.close();
    return strReturn;
  }


  /**
   * This method used to fetch Lookup Case Request list
   * 
   * @param hmInput
   * @return
   * @throws AppError
   */
  public String fetchLkupCaseReqList(HashMap hmInput) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strsegid = GmCommonClass.parseNull((String) hmInput.get("SEGID"));
    String stractid = GmCommonClass.parseNull((String) hmInput.get("ACTID"));
    String struserid = GmCommonClass.parseNull((String) hmInput.get("USERID"));
    String strReturn = "";
    gmDBManager.setPrepareString("gm_pkg_crm_preceptorship_rpt.gm_fch_ps_lkp_case_req_json", 4);
    gmDBManager.setString(1, strsegid);
    gmDBManager.setString(2, stractid);
    gmDBManager.setString(3, struserid);
    gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
    gmDBManager.execute();
    strReturn = GmCommonClass.parseNull(gmDBManager.getString(4));
    log.debug("strReturn===" + strReturn);
    gmDBManager.close();
    return strReturn;
  }

  /**
   * Sales rep users This method used to fetch Lookup Hostcase list, object in front-end as a
   * String.
   */


  public String loadHostCaseJSON(HashMap hmParams) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    String strUserDeptId = GmCommonClass.parseNull((String) hmParams.get("USERDEPTID"));
    HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
    String strOpt = GmCommonClass.parseNull((String) hmParams.get("STROPT"));
    String strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("AccessFilter"));
    String actstatusid = GmCommonClass.parseNull((String) hmParams.get("ACTSTATUSID"));
    String actcategory = GmCommonClass.parseNull((String) hmParams.get("ACTCATEGORY"));
    String strFrmDate = GmCommonClass.parseNull((String) hmParams.get("ACTSTARTDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParams.get("ACTENDDATE"));
    String strfirstnm = GmCommonClass.parseNull((String) hmParams.get("FIRSTNAME"));


    StringBuffer sbQuery = new StringBuffer();
    if (strOpt.equals("COUNT")) {
      sbQuery.append(" SELECT COUNT(1) ");
    } else {
      sbQuery
          .append(" SELECT JSON_ARRAYAGG (JSON_OBJECT('actstartdate' VALUE TO_CHAR(v6630.c6630_from_dt,'MM/DD/YYYY'), ");
      sbQuery.append(" 'partynm' Value v6630.surgeonnms, 'partyid' VALUE v6630.surgeonids, ");
      sbQuery
          .append(" 'actid' VALUE v6630.c6630_activity_id ,'actnm' VALUE v6630.c6630_activity_nm, ");
      sbQuery.append(" 'segnm' VALUE GET_SEGMENT_NAME(v6630.c2090_segment_id), ");
      sbQuery
          .append(" 'segid' VALUE v6630.c2090_segment_id,'pathology' VALUE v6630.c6630_pathology, ");
      sbQuery
          .append(" 'division' VALUE GET_DIVISION_NAME(v6630.C1910_DIVISION_ID), 'createdby' VALUE GET_USER_NAME(v6630.C6630_Created_By), 'region' VALUE GET_CODE_NAME(v6630.C901_REGION), 'zone' VALUE GET_CODE_NAME(v6630.C901_ZONE), 'hospital' VALUE v6630.C6630_PARTY_NM,'fieldsales' VALUE v6630.FIELDSALESNMS, ");
      sbQuery
          .append(" 'prodnm' VALUE v6630.prodnms,'actstatus' VALUE GET_CODE_NAME(v6630.c901_activity_status), ");
      sbQuery.append(" 'actstatus' VALUE GET_CODE_NAME(v6630.c901_activity_status), ");
      sbQuery
          .append(" 'actstatusid' VALUE v6630.c901_activity_status,'actnotes' VALUE v6630.c6630_note, ");
      sbQuery
          .append(" 'actenddate' VALUE v6630.c6630_to_dt,'createdate' VALUE tO_CHAR(v6630.c6630_created_date,'MM/DD/YYYY'), ");
      sbQuery
          .append(" 'acttypeid' VALUE GET_CODE_NAME(v6630.c901_activity_type)) ORDER BY v6630.c6630_from_dt,v6630.c6630_activity_id RETURNING CLOB) ");
    }
    sbQuery.append(" FROM v6630_activity_dtl v6630 ");
    if (strUserDeptId.equals("S")) {
      sbQuery
          .append(" ,(SELECT DISTINCT t6631.c6630_activity_id FROM t6631_activity_party_assoc t6631 ");
      sbQuery.append(" ,v703_rep_mapping_detail v700 WHERE t6631.c101_party_id =v700.rep_partyid ");
      sbQuery.append(" AND t6631.c6631_void_fl  IS NULL AND " + strAccessFilter + " ) V700 ");
    }

    sbQuery.append(" WHERE v6630.c901_activity_category = " + actcategory + " ");
    if (strUserDeptId.equals("S")) {
      sbQuery.append(" AND v6630.c6630_activity_id      = v700.C6630_Activity_Id ");
    }

    if (!actstatusid.equals("")) {

      sbQuery.append("AND v6630.c901_activity_status IN (" + actstatusid + ") ");
    }


    if (!strFrmDate.equals(""))
      sbQuery.append(" AND v6630.c6630_from_dt >= TO_DATE('" + strFrmDate + "','MM/DD/YYYY') ");
    if (!strToDate.equals(""))
      sbQuery.append(" AND v6630.c6630_to_dt <= TO_DATE('" + strToDate + "','MM/DD/YYYY') ");

    if (!strfirstnm.equals(""))
      sbQuery.append(" AND UPPER(V6630.surgeonnms) LIKE '%" + strfirstnm.replace("'", "''").toUpperCase() + "%' ");


    // sbQuery.append("ORDER BY v6630.c6630_from_dt,v6630.c6630_activity_id ");
    log.debug("sbQuery>>>>>" + sbQuery);

    String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());

    return strReturn;
  }



  /**
   * If Admin User This method used to fetch Lookup Hostcase list, object in front-end as a String.
   */

  public String loadAdminHostCaseJSON(HashMap hmParams) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strUserDeptId = GmCommonClass.parseNull((String) hmParams.get("USERDEPTID"));
    HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
    String strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("AccessFilter"));
    String actstatusid = GmCommonClass.parseNull((String) hmParams.get("ACTSTATUSID"));
    String actcategory = GmCommonClass.parseNull((String) hmParams.get("ACTCATEGORY"));
    String strFrmDate = GmCommonClass.parseNull((String) hmParams.get("ACTSTARTDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParams.get("ACTENDDATE"));
    String strfirstnm = GmCommonClass.parseNull((String) hmParams.get("FIRSTNAME"));
    String strDivisionid = GmCommonClass.parseNull((String) hmParams.get("DIVISION"));
    String strAdminstatus = GmCommonClass.parseNull((String) hmParams.get("ADMINSTS"));

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT JSON_ARRAYAGG (JSON_OBJECT('actstartdate' VALUE TO_CHAR(v6630.c6630_from_dt,'MM/DD/YYYY'), ");
    sbQuery.append(" 'partynm' Value v6630.surgeonnms, 'partyid' VALUE v6630.surgeonids, ");
    sbQuery
        .append(" 'actid' VALUE v6630.c6630_activity_id ,'actnm' VALUE v6630.c6630_activity_nm, ");
    sbQuery.append(" 'segnm' VALUE GET_SEGMENT_NAME(v6630.c2090_segment_id), ");
    sbQuery
        .append(" 'segid' VALUE v6630.c2090_segment_id,'pathology' VALUE v6630.c6630_pathology, ");
    sbQuery
        .append(" 'division' VALUE GET_DIVISION_NAME(v6630.C1910_DIVISION_ID),'region' VALUE GET_CODE_NAME(v6630.C901_REGION), 'zone' VALUE GET_CODE_NAME(v6630.C901_ZONE), 'hospital' VALUE v6630.C6630_PARTY_NM,'fieldsales' VALUE v6630.FIELDSALESNMS, ");
    sbQuery
        .append(" 'prodnm' VALUE v6630.prodnms,'actstatus' VALUE GET_CODE_NAME(v6630.c901_activity_status), ");
    sbQuery.append(" 'actstatus' VALUE GET_CODE_NAME(v6630.c901_activity_status), ");
    sbQuery
        .append(" 'actstatusid' VALUE v6630.c901_activity_status,'actnotes' VALUE v6630.c6630_note, ");
    sbQuery
        .append(" 'actenddate' VALUE v6630.c6630_to_dt, 'createby' VALUE GET_USER_NAME(v6630.C6630_Created_By),'createdate' VALUE tO_CHAR(v6630.c6630_created_date,'MM/DD/YYYY'), ");
    sbQuery.append(" 'acttypeid' VALUE GET_CODE_NAME(v6630.c901_activity_type) ");
    sbQuery.append(" ,'casereqid' VALUE t6660.c6660_case_request_id, ");
    sbQuery
        .append(" 'crdfl' VALUE t6660.c6660_credential_fl ,'surgnm' VALUE GET_PARTY_NAME(t6660.c101_surgeon_party_id), ");
    sbQuery
        .append(" 'hotlnm' VALUE t6660.c6660_hotel, 'hotlprice' VALUE t6660.c6660_hotel_price, ");
    sbQuery
        .append(" 'labinv' VALUE t6660.c6660_lab_invoice,'crfnm' VALUE GET_CODE_NAME(t6660.c6660_pcrf_crf_status), ");
    sbQuery
        .append(" 'trfnm' VALUE GET_CODE_NAME(t6660.c6660_trf_status), 'pcmninv' VALUE  t6660.c6660_pecimen_invoice, ");
    sbQuery.append(" 'survfl' VALUE t6660.c6660_survey_fl, 'trvlnm' VALUE t6660.c6660_travel, ");
    sbQuery.append(" 'trvlprice' Value t6660.c6660_travel_price,'adminsts' Value v6630.c901_admin_status,'adminstsnm' Value get_code_name(v6630.c901_admin_status) ");
    sbQuery
        .append(")ORDER BY v6630.c6630_from_dt, v6630.c6630_activity_id , c6660_case_request_id RETURNING CLOB) FROM v6630_activity_dtl v6630 ");
    sbQuery.append(" ,t6660_case_request t6660 ");

    if (strUserDeptId.equals("S")) {
      sbQuery
          .append(" ,(SELECT DISTINCT t6631.c6630_activity_id FROM t6631_activity_party_assoc t6631 ");
      sbQuery.append(" ,v703_rep_mapping_detail v700 WHERE t6631.c101_party_id =v700.rep_partyid ");
      sbQuery.append(" AND t6631.c6631_void_fl  IS NULL AND " + strAccessFilter + " ) V700 ");
    }

    sbQuery.append(" WHERE v6630.c901_activity_category = " + actcategory + " ");
    sbQuery.append(" AND v6630.c6630_activity_id    = t6660.c6630_activity_id(+)");
    sbQuery.append(" AND t6660.c6660_void_fl         IS NULL ");
    if (strUserDeptId.equals("S")) {
      sbQuery.append(" AND v6630.c6630_activity_id   = v700.C6630_Activity_Id ");
    }
    if (!actstatusid.equals("")) {

      sbQuery.append("AND v6630.c901_activity_status IN (" + actstatusid + ") ");
    }
    
  
    if (!strAdminstatus.equals("")) {
    	sbQuery.append("AND v6630.c1910_division_id IN (" + strDivisionid + ") ");
        sbQuery.append("AND v6630.c901_admin_status IN (" + strAdminstatus + ") ");
        sbQuery.append("AND C6630_FROM_DT >= TRUNC(CURRENT_DATE) ");
    }


    if (!strFrmDate.equals(""))
      sbQuery.append(" AND v6630.c6630_from_dt >= TO_DATE('" + strFrmDate + "','MM/DD/YYYY') ");
    if (!strToDate.equals(""))
      sbQuery.append(" AND v6630.c6630_to_dt <= TO_DATE('" + strToDate + "','MM/DD/YYYY') ");

    if (!strfirstnm.equals(""))
      sbQuery.append(" AND UPPER(V6630.surgeonnms) LIKE '%" + strfirstnm.replace("'","''").toUpperCase() + "%' ");

    // sbQuery.append("ORDER BY v6630.c6630_from_dt,v6630.c6630_activity_id ");
    // sbQuery.append(" ,c6660_case_request_id ");
    log.debug("sbQuery>>>>>" + sbQuery);

    String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());

    return strReturn;
  }


  /**
   * 
   * This method used to fetch Lookup Case request list, object in front-end as a String.
   */

  public String loadCaseRequestJSON(HashMap hmParams) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strUserDeptId = GmCommonClass.parseNull((String) hmParams.get("USERDEPTID"));
    String strOpt = GmCommonClass.parseNull((String) hmParams.get("STROPT"));
    HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
    String strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("AccessFilter"));
    String actstatusid = GmCommonClass.parseNull((String) hmParams.get("ACTSTATUSID"));
    String actcategory = GmCommonClass.parseNull((String) hmParams.get("ACTCATEGORY"));
    String strFrmDate = GmCommonClass.parseNull((String) hmParams.get("ACTSTARTDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParams.get("ACTENDDATE"));
    String strname = GmCommonClass.parseNull((String) hmParams.get("NAME"));


    StringBuffer sbQuery = new StringBuffer();
    if (strOpt.equals("COUNT")) {
      sbQuery.append(" SELECT COUNT(1) ");
    } else {
      sbQuery
          .append(" SELECT JSON_ARRAYAGG (JSON_OBJECT( 'surgid' VALUE V6660.C101_SURGEON_PARTY_ID, ");
      sbQuery.append(" 'surgnm' VALUE GET_CRM_PARTY_NAME( V6660.C101_SURGEON_PARTY_ID), ");
      sbQuery.append(" 'casereqid' VALUE V6660.C6660_CASE_REQUEST_ID, ");
      sbQuery
          .append(" 'segnm' VALUE GET_SEGMENT_NAME( V6660.C2090_SEGMENT_ID), 'region' VALUE GET_CODE_NAME( V6660.C901_REGION), 'zone' VALUE GET_CODE_NAME( V6660.C901_ZONE),");
      sbQuery
          .append(" 'status' VALUE GET_CODE_NAME(V6660.C901_STATUS), 'type' VALUE V6660.C901_TYPE, ");
      sbQuery
          .append(" 'division' VALUE GET_DIVISION_NAME(V6660.C1910_DIVISION_ID), 'ptype' VALUE GET_CODE_NAME(V6660.C901_PRECEPTOR_TYPE), 'fieldsales' VALUE V6660.assocnames,");
      sbQuery
          .append(" 'pathology' VALUE C6660_PATHOLOGY, 'actstartdate' VALUE V6660.C6660_FROM_DT, ");
      sbQuery.append(" 'assnm' VALUE V6660.assocnames,'setnm' VALUE V6660.setnames, ");
      sbQuery.append("'atndtyp' VALUE v6660.c901_attendee_type, 'atndtypnm' VALUE GET_CODE_NAME(v6660.c901_attendee_type), 'atndnm' VALUE v6660.c6630_attendee_name , 'hospital' VALUE V6660.HOSPITAL, ");
   //   sbQuery
    //      .append(" 'reqdt' VALUE TO_CHAR(V6660.C6660_FROM_DT,'MM/DD/YYYY') ||' - '|| TO_CHAR(V6660.C6660_TO_DT,'MM/DD/YYYY'), ");
      sbQuery
          .append(" 'createby' VALUE GET_USER_NAME(V6660.C6660_Created_By),'createdate' VALUE tO_CHAR(v6660.c6660_created_dt,'MM/DD/YYYY'), 'note' Value V6660.C6660_Note, ");
      sbQuery
          .append(" 'fromdt' VALUE TO_CHAR(V6660.C6660_FROM_DT,'MM/DD/YYYY') ,'todt' VALUE tO_CHAR(V6660.C6660_TO_DT,'MM/DD/YYYY')  ) ORDER BY V6660.C6660_FROM_DT, v6660.c6660_case_request_id RETURNING CLOB) ");
    }
    sbQuery.append(" FROM V6660_Case_Req_Dlt v6660 ");
    if (strUserDeptId.equals("S")) {
      sbQuery
          .append(" ,(SELECT DISTINCT c6660_case_request_id FROM T6661_Case_Request_Assoc t6661, ");
      sbQuery.append(" V703_Rep_Mapping_Detail v700  WHERE T6661.C6661_Void_Fl IS NULL ");
      sbQuery.append(" AND T6661.C101_Party_Id    =V700.Rep_Partyid AND " + strAccessFilter
          + " )V700 ");
    }
    sbQuery.append(" WHERE V6660.C901_Type =" + actcategory + " ");
    if (strUserDeptId.equals("S")) {
      sbQuery.append(" AND v6660.c6660_case_request_id = v700.c6660_case_request_id ");
    }
    if (!actstatusid.equals("")) {

      sbQuery.append(" AND v6660.C901_STATUS IN (" + actstatusid + ") ");
    }
    if (!strFrmDate.equals(""))
      sbQuery.append(" AND v6660.C6660_FROM_DT >= TO_DATE('" + strFrmDate + "','MM/DD/YYYY') ");
    if (!strToDate.equals(""))
      sbQuery.append(" AND v6660.C6660_TO_DT <= TO_DATE('" + strToDate + "','MM/DD/YYYY') ");

    if (!strname.equals(""))
      sbQuery.append(" AND UPPER(GET_CRM_PARTY_NAME(v6660.C101_SURGEON_PARTY_ID)) LIKE '%"
          + strname.replace("'","''").toUpperCase() + "%' ");


    // sbQuery.append("ORDER BY v6660.C6660_FROM_DT DESC,v6660.c6660_case_request_id ");
    log.debug("sbQuery<<<<<<<<<<<<<<<<<<" + sbQuery);

    String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());

    return strReturn;
  }



  /**
   * To get the all mail related data's for the event creation.
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */
  public HashMap fetchCaseReqEmailDtl(String strCaseReqId) throws AppError {
    HashMap hmCaseReq = new HashMap();
    ArrayList alCaseReqAsso = new ArrayList();
    ArrayList alCaseReqDtl = new ArrayList();
    ArrayList alCaseReqAttr = new ArrayList();
    ArrayList alCaseReqFile = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_crm_preceptorship_rpt.gm_fch_ps_case_info", 6);
    gmDBManager.setString(1, strCaseReqId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.execute();
    hmCaseReq =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(2)));
    alCaseReqAsso =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));
    alCaseReqDtl =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(4)));
    alCaseReqAttr =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    alCaseReqFile =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(6)));
    gmDBManager.close();
    hmCaseReq.put("ALCASEREQASSO", alCaseReqAsso);
    hmCaseReq.put("ALCASEREQDTL", alCaseReqDtl);
    hmCaseReq.put("ALCASEREQATTR", alCaseReqAttr);
    hmCaseReq.put("ALFILES", alCaseReqFile);

    return hmCaseReq;
  }

  /**
   * This method is used to load Preceptorship calendar data for Robotics Sales rep.
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */
  public ArrayList loadCalendarViewData(HashMap hmParams) throws AppError {

   // GmCRMActivityBean gmCRMActivityBean = new GmCRMActivityBean();
    ArrayList alReturn = new ArrayList();
    HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strCategory = GmCommonClass.parseNull((String) hmParams.get("ACTCATEGORY"));
    String strDivision = GmCommonClass.parseNull((String) hmParams.get("DIVISION"));
    String strStatus = GmCommonClass.parseNull((String) hmParams.get("ACTSTATUS"));
    String strZone = GmCommonClass.parseNull((String) hmParams.get("ZONE"));
    String strProcedure = GmCommonClass.parseNull((String) hmParams.get("PROCEDURE"));
    String strWorkflow = GmCommonClass.parseNull((String) hmParams.get("WORKFLOW"));
    String strType =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams
            .get("ACTTYPEID")));
    String strSetId =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("SETID")));
    String strUserDeptId = GmCommonClass.parseNull((String) hmParams.get("USERDEPTID"));
    String strDivFilter = GmCommonClass.parseNull((String) hmParam.get("DIV_FILTER"));
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    String strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("AccessFilter"));
    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT 'Y' ACCESSFL, T6630.C6630_CREATED_BY PARTYID, T6630.C6630_ACTIVITY_ID ACTID, T6630.C6630_ACTIVITY_NM ACTNM, T6630.C901_REGION REGION, T6630.C6630_PARTY_NM PARTYNM, "
            + "T6630.C6630_FROM_DT ACTSTARTDATE, T6630.C6630_TO_DT ACTENDDATE,  GET_CODE_NAME(T6630.C901_ACTIVITY_TYPE) ACTTYPE, "
            + "T6630.C901_ACTIVITY_TYPE ACTTYPEID, get_code_name(T6630.C901_ACTIVITY_STATUS) ACTSTATUS , T6630.C6630_CREATED_BY CREATEDBY, "
            + "T6630.C901_ACTIVITY_STATUS ACTSTATUSID , TO_CHAR(CAST(T6630.C6630_FROM_TM AS TIMESTAMP WITH TIME ZONE),'HH24:MI') ACTSTARTTIME , "
            + "TO_CHAR(CAST(T6630.C6630_TO_TM AS TIMESTAMP WITH TIME ZONE),'HH24:MI') ACTENDTIME, T6630.C1910_DIVISION_ID DIVISION FROM T6630_ACTIVITY T6630 ");
	if (!strSetId.equals("")) {
		  sbQuery
		      .append(",(SELECT c6630_activity_id from T6632_ACTIVITY_ATTRIBUTE where C6632_ATTRIBUTE_VALUE IN ('"
		          + strSetId + "') and C6632_VOID_FL is null ) T6632 ");
		
		}
    sbQuery.append(" WHERE T6630.C901_ACTIVITY_CATEGORY = '" + strCategory + "'");
    //sbQuery.append(" AND T6630.C901_ACTIVITY_TYPE = '107263' "); // shows only type of Premier Center 
	if (!strSetId.equals("")) {
		  sbQuery.append(" AND T6630.C6630_ACTIVITY_ID = T6632.C6630_ACTIVITY_ID ");
	}
    if(strUserDeptId.equals("S")){
        sbQuery.append(" AND T6630.C901_REGION = '" + strDivFilter + "' ");
    	sbQuery.append(" And T6630.C1910_Division_Id IN ( ");
    	sbQuery.append(" SELECT distinct T703a.C1910_Division_Id FROM T703a_Salesrep_Div_Mapping T703a, ");
    	sbQuery.append(" V703_REP_MAPPING_DETAIL v700 WHERE T703a.C703_Sales_Rep_Id =v700.rep_id ");
    	sbQuery.append(" AND t703a.c703a_void_fl      IS NULL ");
    	sbQuery.append(" AND "+ strAccessFilter + " "); 
    	sbQuery.append(" UNION SELECT C1910_DIVISION_ID FROM T703A_SALESREP_DIV_MAPPING WHERE C703A_VOID_FL IS NULL AND C703_SALES_REP_ID ='" + strUserId+ "' ) ");

    }
    
    if (!strStatus.equals("")){
    	sbQuery.append(" AND T6630.C901_ACTIVITY_STATUS IN ('" + strStatus.replace(",","','") + "') ");
    }
    
    if (!strDivision.equals("")){
    	sbQuery.append(" AND T6630.C1910_DIVISION_ID IN ('" + strDivision.replace(",","','") + "') ");
    }
    
    if (!strZone.equals("")){
    	sbQuery.append(" AND T6630.C901_ZONE IN ('" + strZone.replace(",","','") + "') ");
    }
    
    if (!strProcedure.equals("")){
    	sbQuery.append(" AND T6630.C2090_SEGMENT_ID IN ('" + strProcedure.replace(",","','") + "') ");
    }
    
    if (!strWorkflow.equals("")){
    	sbQuery.append(" AND T6630.C901_WORK_FLOW IN ('" + strWorkflow.replace(",","','") + "') ");
    }

    sbQuery.append(" AND T6630.C6630_VOID_FL IS NULL ORDER BY ACTSTARTDATE , ACTSTARTTIME ");
    
    log.debug("loadCalendarViewData Query === " + sbQuery.toString());
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
    gmDBManager.close();
    log.debug("loadCalendarViewData alReturnBean>>>>>" + alReturn.size());
    return alReturn;

  }


  /**
   * This method used to fetch Segment and System Name
   * 
   * @param hmInput
   * @return
   * @throws AppError
   */
  public String fetchSegSysList(HashMap hmInput) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT JSON_ARRAYAGG (JSON_OBJECT('segnm' VALUE SEGMENT_NAME, 'sysnm' VALUE SYSTEM_NM, 'segid' VALUE SEGMENT_ID)ORDER BY SEGMENT_NAME,SYSTEM_NM RETURNING CLOB) FROM V_Segment_System_Map ORDER BY SEGMENT_NAME, SYSTEM_NM, SEGMENT_ID ");

    log.debug("sbQuery<<<<<<<<<<<<<<<<<<" + sbQuery);

    String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());

    return strReturn;
  }
  
  
  
  /**
   * If Admin User This method used to fetch Lookup CaseRequest list, object in front-end as a String.
   */

  public String loadAdminCaseRequestJSON(HashMap hmParams) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strUserDeptId = GmCommonClass.parseNull((String) hmParams.get("USERDEPTID"));
    HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
    String strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("AccessFilter"));
    String actstatusid = GmCommonClass.parseNull((String) hmParams.get("ACTSTATUSID"));
    String actcategory = GmCommonClass.parseNull((String) hmParams.get("ACTCATEGORY"));
    String strFrmDate = GmCommonClass.parseNull((String) hmParams.get("ACTSTARTDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParams.get("ACTENDDATE"));
    String strfirstnm = GmCommonClass.parseNull((String) hmParams.get("FIRSTNAME"));
    String strDivisionid = GmCommonClass.parseNull((String) hmParams.get("DIVISION"));
    String strAdminstatus = GmCommonClass.parseNull((String) hmParams.get("ADMINSTS"));

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT JSON_ARRAYAGG (JSON_OBJECT('surgid' VALUE V6660.C101_SURGEON_PARTY_ID, 'casereqid' VALUE v6660.c6660_case_request_id, ");
    sbQuery.append(" 'surgnm' VALUE GET_PARTY_NAME( v6660.c101_surgeon_party_id), 'status' VALUE GET_CODE_NAME(v6660.c901_status), ");
    sbQuery
        .append(" 'actid' VALUE v6660.c6630_activity_id, 'region' VALUE GET_CODE_NAME(v6660.C901_REGION), 'zone' VALUE GET_CODE_NAME(v6660.C901_ZONE),'division' VALUE GET_DIVISION_NAME(v6660.C1910_DIVISION_ID), ");
    sbQuery.append(" 'pstype' VALUE GET_CODE_NAME(v6660.C901_PRECEPTOR_TYPE ),'segnm' VALUE get_segment_name(V6660.c2090_segment_id), ");
    sbQuery
        .append(" 'setnms' VALUE v6660.setnames,'fromdt' VALUE TO_CHAR(V6660.C6660_FROM_DT,'MM/DD/YYYY') ,'todt' VALUE tO_CHAR(V6660.C6660_TO_DT,'MM/DD/YYYY'),'fieldattnd' VALUE v6660.assocnames, ");
    sbQuery
        .append(" 'createdby' VALUE GET_USER_NAME(v6660.c6660_created_by),'createdate' VALUE tO_CHAR(v6660.c6660_created_dt,'MM/DD/YYYY'),'hotlnm' VALUE v6660.c6660_hotel, ");
    sbQuery
        .append(" 'hotlprice' VALUE v6660.c6660_hotel_price,'trvlnm' VALUE v6660.c6660_travel,'trvlprice' Value v6660.c6660_travel_price, ");
    sbQuery.append(" 'crfnm' VALUE GET_CODE_NAME(v6660.c6660_pcrf_crf_status), 'trfnm' VALUE GET_CODE_NAME(v6660.c6660_trf_status), ");
    sbQuery
        .append(" 'labinv' VALUE v6660.c6660_lab_invoice,'pcmninv' VALUE v6660.c6660_pecimen_invoice,'survfl' VALUE v6660.c6660_survey_fl, 'note' Value V6660.C6660_Note, ");
    sbQuery
        .append(" 'crdfl' VALUE v6660.c6660_credential_fl,'adminsts' VALUE v6660.c901_admin_status,'adminstsnm' Value get_code_name(v6660.c901_admin_status), ");
    sbQuery.append("'atndtyp' VALUE v6660.c901_attendee_type, 'atndtypnm' VALUE GET_CODE_NAME(v6660.c901_attendee_type), 'atndnm' VALUE v6660.c6630_attendee_name, 'hospital' VALUE v6660.HOSPITAL");
    sbQuery
        .append(")ORDER BY v6660.c6660_created_dt, v6660.c6660_case_request_id RETURNING CLOB ");
   
    sbQuery.append(") FROM v6660_case_req_dlt v6660 ");
    sbQuery.append(" WHERE v6660.c901_type = " + actcategory + " ");
    sbQuery.append(" AND v6660.c6660_void_fl         IS NULL ");
    
    if (!strAdminstatus.equals("")) {
    	sbQuery.append("AND v6660.c1910_division_id IN (" + strDivisionid + ") ");
        sbQuery.append("AND v6660.c901_admin_status IN (" + strAdminstatus + ") ");
        sbQuery.append("AND v6660.C901_STATUS != '105764' ");
    }

    if (!strFrmDate.equals(""))
      sbQuery.append(" AND v6660.c6660_created_dt >= TO_DATE('" + strFrmDate + "','MM/DD/YYYY') ");
    if (!strToDate.equals(""))
      sbQuery.append(" AND v6660.c6660_created_dt <= TO_DATE('" + strToDate + "','MM/DD/YYYY') ");


    log.debug("sbQuery>>>>>" + sbQuery);

    String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());

    return strReturn;
  }
  
	/**
	 * @param strUserID
	 * To fetch the sales Rep Zone by passing the user ID
	 * @return
	 * @throws AppError
	 */
	public String fetchSalesRepZone(HashMap hmParams) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
	    String strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("AccessFilter"));
	    
	    StringBuffer sbQuery = new StringBuffer();
	    sbQuery.append(" SELECT JSON_OBJECT('ID' VALUE v700s.ID, 'NAME' VALUE v700s.NM) FROM ");
        sbQuery.append(" (SELECT  DISTINCT  V700.GP_ID ID  , V700.GP_NAME || ' (' || V700.VP_NAME  || ')' NM  ");
        sbQuery.append(" FROM  V700_TERRITORY_MAPPING_DETAIL V700 ");
      
        sbQuery.append(" WHERE " + strAccessFilter + ") v700s ");

		log.debug("sbQuery>>>>>" + sbQuery);

	    String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());

		return strReturn;
	}
	
	/**
 * @param hmparam
 * @return
 * To fetch data for host and case dash chart report
 * @throws AppError
 */
   public String loadHostCaseChartJSON() throws AppError {
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_crm_preceptorship_rpt.gm_fch_hostcase_chart_json", 1);
	    gmDBManager.registerOutParameter(1, OracleTypes.CLOB);
	    gmDBManager.execute();
	    String strReturn = GmCommonClass.parseNull(gmDBManager.getString(1));
	    log.debug("strReturn===" + strReturn);
	    gmDBManager.close();
	    return strReturn;

   }
   
   /**
   * If Admin User This method used to fetch Lookup CaseRequest list, object in front-end as a String.
   */

  public String fetchFiltersDropdown(String strRegionId) throws AppError {
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	  gmDBManager.setPrepareString("gm_pkg_crm_preceptorship_rpt.gm_fch_ps_more_filter_json", 2);
	  gmDBManager.setString(1, strRegionId);
	  gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
	  gmDBManager.execute();
	  String strReturn = GmCommonClass.parseNull(gmDBManager.getString(2));
	  gmDBManager.close();
	  return strReturn;

  }

}
