package com.globus.webservice.crm.bean;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.filters.GmAccessFilter;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author jgurunathan
 * 
 */
public class GmCRMSearchBean {

  /**
 * log global variable
 */
Logger log = GmLogger.getInstance(this.getClass().getName());

  /**fetchCodeLookUpValues
 * @param hmInput
 * @return
 */
public ArrayList fetchCodeLookUpValues(HashMap hmInput) {
    String strCodeGrp =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmInput.get("CODEGRP")));
    String strCodeNm = GmCommonClass.parseNull((String) hmInput.get("CODENM"));
    String strCodeNmAlt =
        GmCommonClass
            .getStringWithQuotes(GmCommonClass.parseNull((String) hmInput.get("CODENMALT")));
    String strCodeId =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmInput.get("CODEID")));
    String strCodeAccId =
        GmCommonClass
            .getStringWithQuotes(GmCommonClass.parseNull((String) hmInput.get("CODEACCID")));
    ArrayList alReturn = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager();

    sbQuery
        .append(" SELECT T901.C901_CODE_ID CODEID, T901.C901_CODE_NM CODENM,  T901.C902_CODE_NM_ALT CODENMALT, ");
    sbQuery.append(" T901.C901_CODE_GRP CODEGRP FROM T901_CODE_LOOKUP T901 ");
    sbQuery.append(" WHERE T901.C901_ACTIVE_FL = '1' AND T901.C901_VOID_FL IS NULL ");

    if (!strCodeGrp.equals("")) {
      sbQuery.append(" AND T901.C901_CODE_GRP in ('" + strCodeGrp + "') ");
    }

    if (!strCodeNmAlt.equals("")) {
      sbQuery.append(" AND T901.C902_CODE_NM_ALT in ('" + strCodeNmAlt + "') ");
    }

    if (!strCodeNm.equals("")) {
      sbQuery.append(" AND LOWER(T901.C901_CODE_NM) LIKE '%" + strCodeNm.toLowerCase() + "%' ");
    }

    if (!strCodeId.equals("")) {
      sbQuery.append(" AND T901.C901_CODE_ID in ('" + strCodeId + "') ");
    }

    if (!strCodeAccId.equals("")) {
      sbQuery
          .append(" AND T901.C901_CODE_ID IN (SELECT T901A.C901_CODE_ID FROM T901A_LOOKUP_ACCESS T901A WHERE T901A.C901_ACCESS_CODE_ID IN ('"
              + strCodeAccId + "')) ");
    }
    sbQuery.append(" ORDER BY T901.C901_CODE_SEQ_NO ");

    log.debug("fetchCodeLookUpValues Query>>> " + sbQuery);

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    return alReturn;
  }

  /**fetchPartyID
 * @param hmInput
 * @return
 */
public ArrayList fetchPartyID(HashMap hmInput) {
    // TODO Auto-generated method stub

    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alReturn = new ArrayList();

    String strNPID = GmCommonClass.parseNull((String) hmInput.get("NPID"));
    String strPartyID = GmCommonClass.parseNull((String) hmInput.get("PARTYID"));

    log.debug("NPID>>> " + strNPID);
    log.debug("PARTYID>>> " + strPartyID);

    sbQuery
        .append(" SELECT T6600.C101_PARTY_SURGEON_ID PARTYID, C6600_SURGEON_NPI_ID NPID FROM T6600_PARTY_SURGEON T6600, T101_PARTY T101");
    sbQuery.append(" WHERE T6600.C6600_VOID_FL IS NULL ");
    sbQuery
        .append(" AND T6600.C101_PARTY_SURGEON_ID = T101.C101_PARTY_ID AND NVL(t101.c101_active_fl,'Y') <> 'N' ");

    if (!strNPID.equals(""))
      sbQuery.append(" AND C6600_SURGEON_NPI_ID = '" + strNPID + "' ");
    if (!strPartyID.equals(""))
      sbQuery.append(" AND C101_PARTY_SURGEON_ID = '" + strPartyID + "' ");

    log.debug("fetchPartyID Query>>> " + sbQuery);

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    return alReturn;
  }

  /**fetchAccountInfo
 * @param hmInput
 * @return
 */
public ArrayList fetchAccountInfo(HashMap hmInput) {
    // TODO Auto-generated method stub

    String strAccountNm = GmCommonClass.parseNull((String) hmInput.get("ACCNM"));
    String strAccountID =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmInput.get("ACCID")));

    ArrayList alReturn = new ArrayList();

    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager();

    sbQuery
        .append(" SELECT C704_ACCOUNT_ID accid, C704_ACCOUNT_NM accnm, C704_BILL_CITY acccity, GET_CODE_NAME(C704_BILL_STATE) accstate, C704_BILL_STATE accstateid ");
    sbQuery.append(" FROM T704_ACCOUNT WHERE C704_VOID_FL IS NULL AND C704_ACTIVE_FL IS NOT NULL ");
    if (!strAccountNm.equals(""))
      sbQuery.append(" AND LOWER(C704_ACCOUNT_NM) LIKE '%" + strAccountNm.toLowerCase() + "%' ");
    if (!strAccountID.equals(""))
      sbQuery.append(" AND C704_ACCOUNT_ID IN ('" + strAccountID + "') ");
    sbQuery.append(" ORDER BY C704_ACCOUNT_NM ");

    log.debug("Query>>> " + sbQuery);

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturn Before Format >>> " + alReturn);

    return alReturn;
  }


  /**
   * This Method is to fetch parent accounts, input as account name, account id
   * 
   * @param hmInput
   * @return
   */
  public ArrayList fetchParentAccountInfo(HashMap hmInput) {
    // TODO Auto-generated method stub
    String strAccountNm = GmCommonClass.parseNull((String) hmInput.get("ACCNM"));
    String strAccountID =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmInput.get("ACCID")));
    ArrayList alReturn = new ArrayList();

    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager();

    sbQuery.append(" SELECT DISTINCT(T101.C101_PARTY_NM) ACCNM, T704.C101_PARTY_ID ACCID, ");
    sbQuery.append(" T704.C704_BILL_CITY ACCCITY, GET_CODE_NAME(T704.C704_BILL_STATE) ACCSTATE, ");
    sbQuery.append(" T704.C704_BILL_STATE ACCSTATEID ");
    sbQuery.append(" FROM T704_ACCOUNT T704, T101_PARTY T101 ");
    sbQuery.append(" WHERE T704.C704_VOID_FL IS NULL ");
    sbQuery.append(" AND T101.C101_VOID_FL IS NULL ");
    sbQuery.append(" AND T704.C101_PARTY_ID  = T101.C101_PARTY_ID ");
    if (!strAccountNm.equals(""))
      sbQuery.append(" AND LOWER(T101.C101_PARTY_NM) LIKE '%" + strAccountNm.toLowerCase() + "%' ");
    if (!strAccountID.equals(""))
      sbQuery.append(" AND T704.C101_PARTY_ID = " + strAccountID + " ");
    sbQuery.append(" ORDER BY T101.C101_PARTY_NM ");

    log.debug("Query>>> " + sbQuery);
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
    log.debug("alReturn Before Format >>> " + alReturn);

    return alReturn;
  }

  /**
   * fetchEducation - This method will fetch Education Details list in surgeon module from T1010
   * table and it is differentiated by STROPT
   * 
   * @param - HashMap
   * @return - ArrayList
   * @exception - AppError
   */
  public ArrayList fetchEducation(HashMap hmInput) {
    // TODO Auto-generated method stub

    String strStropt = GmCommonClass.parseNull((String) hmInput.get("STROPT"));
    String strInstituteNm = GmCommonClass.parseNull((String) hmInput.get("INSTITUTENM"));
    String strCourse = GmCommonClass.parseNull((String) hmInput.get("COURSE"));
    String strColumn = "";
    String strColumnAliceNm = "";
    String strSearchKey = "";

    if (strStropt.equals("INSTITUTENM")) {
      strColumn = "C1010_INSTITUTE_NM";
      strColumnAliceNm = "INSTITUTENAME";
      strSearchKey = strInstituteNm;
    }
    if (strStropt.equals("COURSE")) {
      strColumn = "C1010_DEGREE_NM";
      strColumnAliceNm = "COURSE";
      strSearchKey = strCourse;
    }

    ArrayList alReturn = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager();

    sbQuery.append(" SELECT ROWNUM ID, " + strColumnAliceNm
        + " FROM ( SELECT DISTINCT TRIM(INITCAP(" + strColumn + ")) " + strColumnAliceNm
        + " FROM T1010_EDUCATION ");
    sbQuery.append(" WHERE C1010_VOID_FL IS NULL AND UPPER(" + strColumn + ") LIKE '%"
        + strSearchKey.toUpperCase() + "%' ");
    sbQuery.append(" ORDER BY UPPER(" + strColumnAliceNm + ") ) ");

    log.debug("fetchEducation Query>>> " + sbQuery);

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    return alReturn;
  }

  /**
   * fetchProfessional - This method will fetch Professional Details list in surgeon module from
   * T1020 table and it is differentiated by STROPT
   * 
   * @param - HashMap
   * @return - ArrayList
   * @exception - AppError
   */
  public ArrayList fetchProfessional(HashMap hmInput) {
    // TODO Auto-generated method stub

    String strColumn = "";
    String strColumnAliceNm = "";
    String strSearchKey = "";
    log.debug("hmInput" + hmInput);
    String strStropt = GmCommonClass.parseNull((String) hmInput.get("STROPT"));
    String strTitle = GmCommonClass.parseNull((String) hmInput.get("TITLE"));
    if (strStropt.equals("TITLE")) {
      strColumn = "C1020_TITLE";
      strColumnAliceNm = "TITLE";
      strSearchKey = strTitle;
    }
    String strCompanyname = GmCommonClass.parseNull((String) hmInput.get("COMPANYNAME"));
    if (strStropt.equals("COMPANYNAME")) {
      strColumn = "C1020_COMPANY_NM";
      strColumnAliceNm = "COMPANYNAME";
      strSearchKey = strCompanyname;
    }

    String strGroupdivision = GmCommonClass.parseNull((String) hmInput.get("GROUPDIVISION"));
    if (strStropt.equals("GROUPDIVISION")) {
      strColumn = "C1020_GROUP_DIVISION_NM";
      strColumnAliceNm = "GROUPDIVISION";
      strSearchKey = strGroupdivision;
    }

    String strCity = GmCommonClass.parseNull((String) hmInput.get("CITY"));
    if (strStropt.equals("CITY")) {
      strColumn = "C1020_CITY_NM";
      strColumnAliceNm = "CITY";
      strSearchKey = strCity;
    }

    ArrayList alReturn = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager();

    sbQuery.append(" SELECT ROWNUM ID, " + strColumnAliceNm
        + " FROM ( SELECT DISTINCT TRIM(INITCAP(" + strColumn + ")) " + strColumnAliceNm
        + " FROM T1020_CAREER ");
    sbQuery.append(" WHERE C1020_VOID_FL IS NULL AND UPPER(" + strColumn + ") LIKE '%"
        + strSearchKey.toUpperCase() + "%' ");
    sbQuery.append(" ORDER BY UPPER(" + strColumnAliceNm + ") ) ");

    log.debug("fetchProfessional Query>>> " + sbQuery);

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    return alReturn;
  }

  /**fetchSystemInfo
 * @param hmInput
 * @return
 */
public ArrayList fetchSystemInfo(HashMap hmInput) {
    String strSystemNm = GmCommonClass.parseNull((String) hmInput.get("SYSNM"));
    String strSystemId =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmInput.get("SYSID")));

    ArrayList alReturn = new ArrayList();

    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager();

    sbQuery.append(" SELECT C207_SET_ID sysid, C207_SET_NM sysnm ");
    sbQuery
        .append(" FROM T207_SET_MASTER WHERE C901_SET_GRP_TYPE = '1600' AND C207_VOID_FL IS NULL ");
    if (!strSystemNm.equals(""))
      sbQuery.append(" AND LOWER(C207_SET_NM) LIKE '%" + strSystemNm.toLowerCase() + "%' ");
    if (!strSystemId.equals(""))
      sbQuery.append(" AND C207_SET_ID IN ('" + strSystemId + "') ");
    sbQuery.append(" ORDER BY C207_SET_NM ");

    log.debug("fetchSystemInfo Query>>> " + sbQuery);

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));


    return alReturn;
  }

  /**
   * This method is to get segments info, input as id, name
   * 
   * @param hmInput
   * @return
   */
  public ArrayList fetchSegmentInfo(HashMap hmInput) {
    String strSegmentNm = GmCommonClass.parseNull((String) hmInput.get("SEGNM"));
    String strSegmentId =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmInput.get("SEGID")));

    ArrayList alReturn = new ArrayList();

    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager();

    sbQuery.append(" SELECT C2090_SEGMENT_ID SEGID, C2090_SEGMENT_NAME SEGNM ");
    sbQuery.append(" FROM T2090_SEGMENT WHERE C2090_VOID_FL IS NULL ");
    if (!strSegmentNm.equals(""))
      sbQuery.append(" AND LOWER(C2090_SEGMENT_NAME) LIKE '%" + strSegmentNm.toLowerCase() + "%' ");
    if (!strSegmentId.equals(""))
      sbQuery.append(" AND C2090_SEGMENT_ID = " + strSegmentId + " ");
    sbQuery.append(" ORDER BY C2090_SEGMENT_NAME ");


    log.debug("fetchSegmentInfo Query>>> " + sbQuery);

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));


    return alReturn;
  }

  // Get Lab system list
  /**fetchCRMVisitSystemInfo
 * @param hmInput
 * @return
 */
public ArrayList fetchCRMVisitSystemInfo(HashMap hmInput) {
    String strSystemNm = GmCommonClass.parseNull((String) hmInput.get("SYSNM"));
    String strSystemId =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmInput.get("SYSID")));
    String strSysLab = GmCommonClass.parseNull((String) hmInput.get("SYSLAB"));
    ArrayList alReturn = new ArrayList();

    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager();

    sbQuery
        .append(" SELECT DISTINCT T207.C207_SET_ID SYSID , nvl(C207_SET_ALT_NM, C207_SET_NM) SYSNM ");
    sbQuery
        .append(" FROM T207_SET_MASTER T207,  T2080_SET_COMPANY_MAPPING T2080,  t207c_set_attribute t207c WHERE ");
    sbQuery
        .append(" T207.C207_SET_ID = T2080.C207_SET_ID AND T207.C207_SET_ID = T207c.C207_SET_ID AND t207c.c901_attribute_type = '103119' AND t207c.c207c_attribute_value = '103087' ");
    if (!strSysLab.equals("")) {
      sbQuery
          .append(" AND t207.c207_set_id NOT IN (SELECT c906_rule_value FROM t906_rules WHERE c906_rule_grp_id = 'CRM_EXCLUDE' AND C906_RULE_ID ='SYSTEM' AND C906_VOID_FL IS NULL ) ");
    }
    sbQuery
        .append(" AND t207c.c207c_void_fl   IS NULL AND C901_SET_GRP_TYPE = 1600 AND T2080.C1900_COMPANY_ID = '1000' AND T2080.C2080_VOID_FL IS NULL ");

    if (!strSystemNm.equals(""))
      sbQuery.append(" AND LOWER(nvl(C207_SET_ALT_NM, C207_SET_NM)) LIKE '%"
          + strSystemNm.toLowerCase() + "%' ");
    if (!strSystemId.equals(""))
      sbQuery.append(" AND T207.C207_SET_ID IN ('" + strSystemId + "') ");
    sbQuery.append(" ORDER BY sysnm ");

    log.debug("fetchCRMVisitSystemInfo Query>>> " + sbQuery);

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    return alReturn;
  }

  /**fetchPartyInfo
 * @param hmInput
 * @return
 */
public ArrayList fetchPartyInfo(HashMap hmInput) {
    // TODO Auto-generated method stub

    String strPartyType =
        GmCommonClass
            .getStringWithQuotes(GmCommonClass.parseNull((String) hmInput.get("PARTYTYPE")));
    String strPartyNm = GmCommonClass.parseNull((String) hmInput.get("PARTYNM"));
    String strFirstNm = GmCommonClass.parseNull((String) hmInput.get("FNAME"));
    String strLastNm = GmCommonClass.parseNull((String) hmInput.get("LNAME"));
    String strNPID = GmCommonClass.parseNull((String) hmInput.get("NPID"));
    String strPartyId =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmInput.get("PARTYID")));
    ArrayList alReturn = new ArrayList();

    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager();
    sbQuery
        .append(" SELECT C101_PARTY_ID PARTYID, DECODE(C101_MIDDLE_INITIAL, null, (GET_CODE_NAME(npid.SALUTE) ||' '|| C101_FIRST_NM || ' ' || C101_LAST_NM), (GET_CODE_NAME(npid.SALUTE) ||' '|| C101_FIRST_NM || ' ' || C101_MIDDLE_INITIAL || ' ' || C101_LAST_NM)) PARTYNM, ");
    sbQuery
        .append(" npid.npid NPID, C901_PARTY_TYPE PARTYTYPE, GET_CODE_NAME(npid.SALUTE) SALUTENM,  GET_SURGEON_CV_PATH(C101_PARTY_ID) PARTYCV FROM T101_PARTY t101, (SELECT c6600_surgeon_npi_id npid , c101_party_surgeon_id partyid, C6600_PARTY_SALUTE SALUTE FROM t6600_party_surgeon WHERE c6600_void_fl is null) npid  ");
    sbQuery
        .append(" WHERE (C101_ACTIVE_FL IS NULL OR C101_ACTIVE_FL = 'Y') AND C101_VOID_FL IS NULL  and t101.c101_party_id = npid.partyid(+) ");
    if (!strPartyType.equals(""))
      sbQuery.append(" AND C901_PARTY_TYPE in ('" + strPartyType + "') ");
    if (!strPartyNm.equals(""))
      sbQuery
          .append(" AND (LOWER(NVL(C101_PARTY_NM, C101_FIRST_NM || ' ' || C101_LAST_NM)) LIKE '%"
              + strPartyNm.replace("'", "''").toLowerCase() + "%' OR npid.npid LIKE '%" + strPartyNm.replace("'", "''").toLowerCase()
              + "%') ");
    if (!strFirstNm.equals(""))
      sbQuery.append(" AND LOWER(C101_FIRST_NM ) LIKE '%" + strFirstNm.toLowerCase() + "%' ");
    if (!strLastNm.equals(""))
      sbQuery.append(" AND LOWER(C101_LAST_NM ) LIKE '%" + strLastNm.toLowerCase() + "%' ");
    if (!strNPID.equals(""))
      sbQuery.append("  and npid.npid = '" + strNPID + "' ");
    if (!strPartyId.equals(""))
      sbQuery.append(" AND C101_PARTY_ID IN ('" + strPartyId + "') ");
    sbQuery.append(" ORDER BY UPPER(PARTYNM) ");

    log.debug("Query>>> " + sbQuery);

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    return alReturn;
  }


  /**
   * @description The below query used for search by rep name and rep V703_REP_MAPPING_DETAIL table
   * @param hmParams
   * @return
   */
  public ArrayList fetchRepInfo(HashMap hmParams) {
    // TODO Auto-generated method stub

    HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    ArrayList alReturn = new ArrayList();

    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    String strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("AccessFilter"));
    String strDepId = GmCommonClass.parseNull((String) hmParams.get("USERDEPTID"));

    String strRepNm = GmCommonClass.parseNull((String) hmParams.get("REPNAME"));
    String strCategory = GmCommonClass.parseNull((String) hmParams.get("CATEGORY"));
    String strRepID = GmCommonClass.parseNull((String) hmParams.get("REPID"));

    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager();

    sbQuery
        .append(" SELECT DISTINCT(V700.REP_PARTYID) REPID, V700.REP_PARTYID PARTYID, V700.REP_NAME REPNAME, V700.AD_NAME ADNAME, "
            + " V700.VP_NAME VPNAME, V700.D_ID DID, V700.D_NAME DNAME");

    sbQuery.append(" FROM V703_REP_MAPPING_DETAIL V700");

    sbQuery.append(" WHERE " + strAccessFilter + "  ");

    if (!strRepNm.equals(""))
      sbQuery.append(" AND UPPER(V700.REP_NAME) LIKE UPPER('%" + strRepNm + "%') ");

    if (!strRepID.equals(""))
      sbQuery.append(" AND V700.REP_PARTYID IN (" + strRepID + ") ");

    sbQuery.append(" ORDER BY REPNAME ");

    log.debug("fetchRepInfo Query >>> " + sbQuery);

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    return alReturn;
  }

  /**fetchDistributorInfo
 * @param hmInput
 * @return
 */
public ArrayList fetchDistributorInfo(HashMap hmInput) {
    // TODO Auto-generated method stub
    String strRepNm = GmCommonClass.parseNull((String) hmInput.get("REPNAME"));
    String strRepID = GmCommonClass.parseNull((String) hmInput.get("REPID"));
    ArrayList alReturn = new ArrayList();

    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager();

    sbQuery
        .append(" SELECT C701_DISTRIBUTOR_ID DID, C701_DISTRIBUTOR_NAME DNAME FROM T701_DISTRIBUTOR ");
    sbQuery
        .append(" WHERE C701_VOID_FL IS NULL AND (C701_ACTIVE_FL IS NULL OR C701_ACTIVE_FL ='Y') ");
    if (!strRepNm.equals(""))
      sbQuery.append(" AND UPPER(C701_DISTRIBUTOR_NAME) LIKE UPPER('%" + strRepNm + "%') ");
    if (!strRepID.equals(""))
      sbQuery.append(" AND C701_DISTRIBUTOR_ID IN (" + strRepID + ") ");
    sbQuery.append(" ORDER BY C701_DISTRIBUTOR_NAME ");

    log.debug("Dist Query >>> " + sbQuery);

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    return alReturn;
  }

  /**fetchADInfo
 * @param hmInput
 * @return
 */
public ArrayList fetchADInfo(HashMap hmInput) {
    // TODO Auto-generated method stub
    String strRepNm = GmCommonClass.parseNull((String) hmInput.get("REPNAME"));
    String strRepID = GmCommonClass.parseNull((String) hmInput.get("REPID"));
    ArrayList alReturn = new ArrayList();

    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager();

    sbQuery.append(" SELECT DISTINCT C101_USER_ID REPID, GET_USER_NAME(C101_USER_ID) REPNAME ");
    sbQuery.append(" FROM T708_REGION_ASD_MAPPING ");
    sbQuery.append(" WHERE C708_DELETE_FL IS NULL ");
    if (!strRepNm.equals(""))
      sbQuery.append(" AND UPPER(GET_USER_NAME(C101_USER_ID)) LIKE UPPER('%" + strRepNm + "%') ");
    if (!strRepID.equals(""))
      sbQuery.append(" AND C101_USER_ID IN (" + strRepID + ") ");
    sbQuery.append(" ORDER BY GET_USER_NAME(C101_USER_ID) ");

    log.debug("AD Query >>> " + sbQuery);

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    return alReturn;
  }

  /**fetchProjectInfo
 * @param hmInput
 * @return
 */
public ArrayList fetchProjectInfo(HashMap hmInput) {
    // TODO Auto-generated method stub
    String strProNm = GmCommonClass.parseNull((String) hmInput.get("PROJECTNAME"));
    String strProId = GmCommonClass.parseNull((String) hmInput.get("PROJECTID"));
    ArrayList alReturn = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager();
    sbQuery.append(" SELECT C202_PROJECT_ID PROJECTID, C202_PROJECT_NM PROJECTNAME ");
    sbQuery.append("  FROM T202_PROJECT WHERE C202_VOID_FL IS NULL ");
    if (!strProNm.equals("")) {
      sbQuery.append(" AND UPPER(C202_PROJECT_NM) LIKE UPPER('%" + strProNm + "%') ");
      sbQuery.append(" ORDER BY C202_PROJECT_NM");
    }
    if (!strProId.equals("")) {
      sbQuery.append(" AND UPPER(C202_PROJECT_ID) LIKE UPPER('%" + strProId + "%') ");
      sbQuery.append(" ORDER BY C202_PROJECT_ID");
    }

    log.debug("Rep Query >>> " + sbQuery);

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    return alReturn;
  }

  /**fetchUserInfo
 * @param hmInput
 * @return
 */
public ArrayList fetchUserInfo(HashMap hmInput) {
    String strUserNm = GmCommonClass.parseNull((String) hmInput.get("USERNM"));
    ArrayList alReturn = new ArrayList();

    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager();

    sbQuery
        .append(" SELECT C101_USER_ID USERID, C101_USER_F_NAME || ' ' || C101_USER_L_NAME USERNM, ");
    sbQuery.append("  C101_EMAIL_ID EMAIL, C101_PARTY_ID PARTYID FROM T101_USER ");
    if (!strUserNm.equals(""))
      sbQuery
          .append(" WHERE LOWER(C101_USER_F_NAME || ' ' || C101_USER_L_NAME || ' ' || C101_EMAIL_ID) LIKE LOWER('%"
              + strUserNm + "%') ");
    // sbQuery.append(" ORDER BY C202_PROJECT_NM ");
    log.debug("Rep Query >>> " + sbQuery);
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    return alReturn;
  }
  
  /**
   * @description The below query used for Partynm T101_PARTY and Rep V703_REP_MAPPING_DETAIL table search by party id
   * @param hmParams
   * @return
   */
  
  public String fetchPartyRepInfo(HashMap hmParams) throws AppError {

	  	GmDBManager gmDBManager = new GmDBManager();
	    
	    String strPartyType = GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("PARTYTYPE")));
	    String strPartyNm = GmCommonClass.parseNull((String) hmParams.get("PARTYNM"));

	    StringBuffer sbQuery = new StringBuffer();
	    
	    sbQuery
	        .append("SELECT JSON_ARRAYAGG (JSON_OBJECT('partyid' VALUE t101.c101_party_id ,'partynm' VALUE t101.c101_first_nm ||' '||t101.c101_last_nm , ");
	    sbQuery
	        .append(" 'adname' VALUE V700.AD_NAME ,'vpname' VALUE V700.VP_NAME) ORDER BY t101.c101_first_nm ||' '||t101.c101_last_nm RETURNING CLOB) FROM T101_PARTY T101,V703_REP_MAPPING_DETAIL V700 ");
	    sbQuery
	        .append(" WHERE t101.c101_party_id = V700.REP_PARTYID(+)");
	      sbQuery.append(" AND t101.c901_party_type in (7002,7003,7004,7005,7006) ");
	    sbQuery.append("AND t101.c101_void_fl IS NULL AND (t101.C101_ACTIVE_FL IS NULL OR t101.C101_ACTIVE_FL = 'Y')");
	    sbQuery.append(" AND UPPER(t101.c101_first_nm ||' '||t101.c101_last_nm) LIKE UPPER('%" + strPartyNm + "%')");

	    log.debug("Query>>> " + sbQuery);

	    String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());

	    return strReturn;
  }



}
