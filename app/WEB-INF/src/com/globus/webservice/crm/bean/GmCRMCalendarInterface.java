package com.globus.webservice.crm.bean;

import java.util.ArrayList;
import java.util.HashMap;
import com.globus.common.beans.AppError;

public interface GmCRMCalendarInterface {
	public ArrayList loadCalendarViewData(HashMap hmParams) throws AppError;
}