package com.globus.webservice.crm.bean;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmWSUtil;
import com.globus.party.beans.GmProffessionalDetailsBean;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.webservice.crm.valueobject.GmCRMFileUploadVO;
import com.globus.webservice.crm.valueobject.GmCRMLogVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonAddressListVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonAffiliationVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonAttrVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonContactsListVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonEducationVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonProfessionalVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonRepVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonSocialVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonSurgicalVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonlistVO;
import com.globus.webservice.crm.valueobject.GmProjectBasicVO;
import com.globus.common.db.GmCacheManager;

public class GmCRMSurgeonBean extends GmSalesFilterConditionBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ArrayList fetchSurgeonFullReport(HashMap hmParams) throws AppError {

    ArrayList alReturn = new ArrayList();
    String strCity = GmCommonClass.parseNull((String) hmParams.get("CITY"));
    String strName = GmCommonClass.parseNull((String) hmParams.get("NAME"));
    String strProcedure =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams
            .get("PROCEDURE")));
    String strHospitialName = GmCommonClass.parseNull((String) hmParams.get("HOSPITALNM"));
    String strSystem =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("SYSTEM")));
    String strSystemId =
        GmCommonClass.parseNull((String) hmParams.get("SYSTEMID")).replaceAll(",", "','");
    String strState =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("STATE")));
    String strVisitDate = GmCommonClass.parseNull((String) hmParams.get("VISITDATE"));
    String strSpecality =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams
            .get("SPECALITY")));
    String strFirstnm = GmCommonClass.parseNull((String) hmParams.get("FIRSTNM"));
    String strFirstname = GmCommonClass.parseNull((String) hmParams.get("FIRSTNAME"));
    String strLastnm = GmCommonClass.parseNull((String) hmParams.get("LASTNM"));
    String strLastname = GmCommonClass.parseNull((String) hmParams.get("LASTNAME"));
    
    strFirstnm= strFirstnm.replace("'","''");
    strLastnm = strLastnm.replace("'","''");
    strFirstname= strFirstname.replace("'","''");
    strLastname= strLastname.replace("'","''");
    
    String strMiddleInitial = GmCommonClass.parseNull((String) hmParams.get("MIDDLEINITIAL"));
    String strActTyp =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("ACTTYP")));
    String strActFrmDate = GmCommonClass.parseNull((String) hmParams.get("ACTFRMDATE"));
    String strActToDate = GmCommonClass.parseNull((String) hmParams.get("ACTTODATE"));
    String strRole =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("ROLE")));
    String strRepId = GmCommonClass.parseNull((String) hmParams.get("REPID"));
    String strActNm = GmCommonClass.parseNull((String) hmParams.get("ACTNM"));
    String strKeywords =
        GmCommonClass
            .getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("KEYWORDS")));

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT UNIQUE T101.C101_PARTY_ID PARTYID, ");
    sbQuery.append(" T101.C101_FIRST_NM FIRSTNM, ");
    sbQuery.append(" T101.C101_LAST_NM LASTNM, ");
    sbQuery.append(" T101.C101_PARTY_NM PARTYNM, ");
    sbQuery.append(" T101.C101_MIDDLE_INITIAL MIDINITIAL, ");
    sbQuery.append(" T107A.EMAIL EMAIL, ");
    sbQuery.append(" T107A.PHONE PHONE ,  t106.address ,t106.state,t106.country, t106.city ");

    log.debug("REPID>>>>>" + strRepId);

    if (!strRepId.equals("")) {
      log.debug("INSIDE>>>>>" + strRepId);
      sbQuery.append(", ACC.account ");
    }
    sbQuery
        .append(" ,T903.C901_REF_GRP||'/'||  T903.C903_REF_ID||'/'||  T903.C903_FILE_NAME PICURL ");
    sbQuery
        .append(", t6600.c6600_surgeon_npi_id npid, GET_CODE_NAME(t6600.C6600_PARTY_SALUTE) salutenm FROM T101_PARTY T101 ");
    sbQuery.append(" ,t6600_party_surgeon t6600, T903_UPLOAD_FILE_LIST T903 ");
    sbQuery.append(" ,(SELECT T107.C101_PARTY_ID, ");
    sbQuery.append(" MAX (DECODE (T107.C901_MODE, 90452, T107.C107_CONTACT_VALUE,'')) EMAIL, ");
    sbQuery.append(" MAX (DECODE (T107.C901_MODE, 90450, T107.C107_CONTACT_VALUE,'')) PHONE ");
    sbQuery.append(" FROM T107_CONTACT T107 ");
    sbQuery.append(" WHERE T107.C107_PRIMARY_FL = 'Y' AND t107.c107_void_fl IS NULL ");
    sbQuery.append(" GROUP BY T107.C101_PARTY_ID ");
    sbQuery.append(" ) T107A ");
    if (!strCity.equals("") || !strState.equals("")) {
      sbQuery.append(" ,(SELECT T106.C101_PARTY_ID FROM T106_ADDRESS T106 ");
      sbQuery.append(" WHERE T106.C101_PARTY_ID IS NOT NULL ");
      if (!strCity.equals(""))
        sbQuery.append("  AND  LOWER(T106.C106_CITY) LIKE '%" + strCity.toLowerCase().trim()
            + "%' ");
      if (!strState.equals(""))
        sbQuery.append("  AND T106.C901_STATE IN ('" + strState + "') ");

      sbQuery.append(" GROUP BY T106.C101_PARTY_ID ) Add_ACT ");
    }

    if (!strActNm.equals("") || !strActTyp.equals("") || !strActFrmDate.equals("")
        || !strActToDate.equals("") || !strRole.equals("")) {
      sbQuery
          .append(", ( SELECT DISTINCT T6631.C101_PARTY_ID PARTY_ID FROM T6630_ACTIVITY T6630, T6631_ACTIVITY_PARTY_ASSOC T6631 ");
      sbQuery.append(" WHERE T6631.C6630_ACTIVITY_ID = T6630.C6630_ACTIVITY_ID ");

      if (!strActNm.equals(""))
        sbQuery.append(" AND UPPER(T6630.C6630_ACTIVITY_NM) LIKE UPPER('%" + strActNm + "%') ");

      if (!strActTyp.equals(""))
        sbQuery.append(" AND NVL(T6630.C901_ACTIVITY_TYPE, T6630.C901_ACTIVITY_CATEGORY) IN ('"
            + strActTyp + "') ");
      if (!strActFrmDate.equals("") || !strActToDate.equals("")) {
        sbQuery.append(" AND T6630.C6630_FROM_DT >= TO_DATE('" + strActFrmDate
            + "','MM/DD/YYYY') AND T6630.C6630_TO_DT <= LAST_DAY(TO_DATE('" + strActToDate
            + "','MM/DD/YYYY')) ");
      }
      if (!strRole.equals(""))
        sbQuery.append(" AND T6631.C901_ACTIVITY_PARTY_ROLE IN ('" + strRole + "') ");

      sbQuery.append("  AND T6630.C6630_VOID_FL IS NULL AND T6631.C6631_VOID_FL IS NULL ) ACT ");
    }
    if (!strRepId.equals("")) {
      sbQuery
          .append(" ,( SELECT T108.C101_FROM_PARTY_ID PARTY_ID, T108.C101_TO_PARTY_ID rep_party_id ");
      sbQuery.append(" FROM T108_PARTY_TO_PARTY_LINK T108 ");
      sbQuery.append(" WHERE T108.C108_VOID_FL     IS NULL ");
      sbQuery.append(" AND t108.C101_TO_PARTY_ID  = " + strRepId + " )t108 ");
    }
    sbQuery
        .append(" , (SELECT C106_ADD1 || ' , ' || c106_city || ' ,' || get_code_name(c901_state) address, "
            + " c106_city city, NVL(t901.c902_code_nm_alt,t901.c901_code_nm)state,nvl(t901a.c902_code_nm_alt, t901a.c901_code_nm) country, c101_party_id FROM t106_address t106, t901_code_lookup t901 ,t901_code_lookup  t901a"
            + " WHERE c106_void_fl is null  AND c106_primary_fl    = 'Y'  AND c101_party_id     IS NOT NULL  AND t106.c901_state    = t901.c901_code_id(+) AND t106.c901_country = t901a.c901_code_id(+)"
            + " AND t901.c901_void_fl IS NULL) t106  ");

    if (!strRepId.equals(""))
      sbQuery
          .append(" ,(SELECT T1014.c1014_organization_name account, T1014.c101_party_id party_id FROM T1014_PARTY_AFFILIATION T1014  "
              + "WHERE c1014_void_fl IS  NULL AND T1014.c901_affiliation_priority =1 ) ACC ");

    if (!strKeywords.equals(""))
      sbQuery
          .append(" ,   (SELECT C101_PARTY_ID FROM T1012_PARTY_OTHER_INFO WHERE C901_TYPE = '103168' AND C1012_VOID_FL IS NULL AND UPPER(C1012_DETAIL) IN ('"
              + strKeywords.toUpperCase() + "')) KEYWORD ");

    sbQuery.append(" WHERE T101.C901_PARTY_TYPE = '7000' ");
    sbQuery.append(" AND T101.C101_ACTIVE_FL   IS NULL ");
    sbQuery.append(" AND T101.C101_VOID_FL     IS NULL ");
    sbQuery
        .append(" AND T107A.C101_PARTY_ID(+) =T101.C101_PARTY_ID and t101.c101_PARTY_ID = t6600.c101_party_surgeon_id(+)  ");
    sbQuery.append(" AND t6600.c6600_void_fl IS NULL ");
    // sbQuery.append(" AND T106.C101_PARTY_ID(+)     =T101.C101_PARTY_ID ");

    /*
     * if (!strProcedure.equals("") || !strSystemId.equals("") || !strSystem.equals(""))
     * sbQuery.append(" AND SURG_ACT.C101_PARTY_ID   =T101.C101_PARTY_ID ");
     */

    if (!strCity.equals("") || !strState.equals(""))
      sbQuery.append(" AND ADD_ACT.C101_PARTY_ID   =T101.C101_PARTY_ID ");

    if (!strName.equals(""))
      sbQuery
          .append(" AND  UPPER(T101.C101_FIRST_NM)||' '|| UPPER(T101.C101_LAST_NM) LIKE UPPER('%'||'"
              + strName + "'||'%') ");
    if (!strFirstnm.equals("") || !strLastnm.equals(""))
      sbQuery.append(" AND  UPPER(T101.C101_FIRST_NM) LIKE UPPER('%'||'" + strFirstnm
          + "'||'%') AND UPPER(T101.C101_LAST_NM) LIKE UPPER('%'||'" + strLastnm + "'||'%') ");
    if (!strFirstname.equals("") || !strLastname.equals("")) {

      sbQuery.append(" AND  UPPER(T101.C101_FIRST_NM) = UPPER('" + strFirstname
          + "') AND UPPER(T101.C101_LAST_NM) = UPPER('" + strLastname + "') ");

      if (!strMiddleInitial.equals("")) {
        sbQuery
            .append(" AND  UPPER(T101.C101_MIDDLE_INITIAL) = UPPER('" + strMiddleInitial + "') ");
      } else {
        sbQuery.append(" AND  T101.C101_MIDDLE_INITIAL is null ");
      }

    }



    if (!strActNm.equals("") || !strActTyp.equals("") || !strActFrmDate.equals("")
        || !strActToDate.equals("") || !strRole.equals(""))
      sbQuery.append(" AND ACT.PARTY_ID   =T101.C101_PARTY_ID ");

    if (!strSpecality.equals(""))
      sbQuery.append(" AND t108.PARTY_ID    =T101.C101_PARTY_ID ");

    if (!strRepId.equals(""))
      sbQuery.append("  AND t108.PARTY_ID    =T101.C101_PARTY_ID ");

    sbQuery.append(" AND t101.c101_PARTY_ID = t106.c101_PARTY_ID(+) ");

    if (!strRepId.equals(""))
      sbQuery.append(" AND t101.c101_party_id = ACC.PARTY_ID(+) ");

    if (!strKeywords.equals(""))
      sbQuery.append(" AND t101.c101_party_id = KEYWORD.C101_PARTY_ID ");
    sbQuery.append(" AND C903_DELETE_FL(+) IS NULL AND C901_REF_GRP(+)      = '103112' ");
    sbQuery
        .append(" AND to_char(t101.c101_party_id) = T903.C903_REF_ID(+) AND T903.C901_REF_TYPE(+) IS NULL ORDER BY UPPER(FIRSTNM ||' '|| LASTNM) ");

    log.debug("fetchSurgeonFullReport Query 2>>>>>" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn.size());

    return alReturn;

  }

  public HashMap fetchSurgeonReport(HashMap hmParams) throws AppError {
    ArrayList alSurgeonPartyDtls = new ArrayList();
    ArrayList alSurgeonAddressDtls = new ArrayList();
    ArrayList alSurgeonContactsDtls = new ArrayList();
    // ArrayList alSurgeonAttrDtls = new ArrayList();
    ArrayList alSurgeonAffiliationDtls = new ArrayList();
    ArrayList alSurgeonEducationDtls = new ArrayList();
    ArrayList alSurgeonProffessionalDtls = new ArrayList();
    ArrayList alSurgeonFiles = new ArrayList();
    ArrayList alSurgoenProjectDtls = new ArrayList();
    HashMap hmReturn = new HashMap();
    ArrayList alSurgeonRepDtls = new ArrayList();
    ArrayList alSurgeonsociallink = new ArrayList();
    String strPartyID = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));

    log.debug("The party id is " + strPartyID);

    GmCRMFileUploadBean gmFileUploadBean = new GmCRMFileUploadBean();
    GmProffessionalDetailsBean gmProffessionalDetailsBean = new GmProffessionalDetailsBean();

    alSurgeonPartyDtls = fetchSurgeonPartyReport(hmParams);
    alSurgeonAddressDtls = fetchSurgeonAddressReport(hmParams);

    alSurgeonContactsDtls = fetchSurgeonContactsReport(hmParams);
    // alSurgeonAttrDtls = fetchSurgeonAttrReport(hmParams);
    alSurgeonAffiliationDtls = fetchSurgeonAffiliationReport(hmParams);
    alSurgeonEducationDtls = fetchSurgeonEducationInfo(hmParams);
    alSurgeonProffessionalDtls = fetchSurgeonProffessionalInfo(hmParams);
    alSurgeonFiles = gmFileUploadBean.fetchFilesByType(strPartyID, "105220,105221,105222");
    alSurgoenProjectDtls = fetchSurgeonProjectInfo(hmParams);
    alSurgeonRepDtls = fetchSurgeonRepInfo(hmParams);
    alSurgeonsociallink = fetchSurgeonlinkInfo(hmParams);
    hmReturn.put("SURGEONPARTYDTLS", alSurgeonPartyDtls);
    hmReturn.put("SURGEONADDRESSDTLS", alSurgeonAddressDtls);
    hmReturn.put("SURGEONCONTACTSDTLS", alSurgeonContactsDtls);
    // hmReturn.put("SURGEONATTRDTLS", alSurgeonAttrDtls);
    hmReturn.put("SURGEONAFFILIATIONDTLS", alSurgeonAffiliationDtls);
    hmReturn.put("SURGEONEDUCATIONDTLS", alSurgeonEducationDtls);
    hmReturn.put("SURGEONPROFFESSIONALDTLS", alSurgeonProffessionalDtls);
    hmReturn.put("SURGEONFILEDTLS", alSurgeonFiles);
    hmReturn.put("SURGEONPROJECTDTLS", alSurgoenProjectDtls);
    hmReturn.put("SURGEONREPDTLS", alSurgeonRepDtls);
    hmReturn.put("SURGEONSOCIALLINK", alSurgeonsociallink);
     


    return hmReturn;

  }

  public GmCRMSurgeonlistVO populateSurgeonListVO(HashMap hmParam) throws AppError {
    ArrayList alSurgeonPartyDtls = new ArrayList();
    ArrayList alSurgeonAddressDtls = new ArrayList();
    ArrayList alSurgeonContactsDtls = new ArrayList();
    // ArrayList alSurgeonAttrDtls = new ArrayList();
    ArrayList alSurgeonAffiliationDtls = new ArrayList();
    ArrayList alSurgeonFileDtls = new ArrayList();
    ArrayList alSurgeonEducatioDtls = new ArrayList();
    ArrayList alSurgeonProffessionalDtls = new ArrayList();
    ArrayList alSurgeonProjectlDtls = new ArrayList();
    ArrayList alSurgeonRepDtls = new ArrayList();
    ArrayList alSurgeonsociallink = new ArrayList();
    HashMap hmResult = new HashMap();

    List<GmCRMSurgeonlistVO> listGmCRMSurgeonListVO = new ArrayList<GmCRMSurgeonlistVO>();
    List<GmCRMSurgeonAddressListVO> listGmCRMSurgeonAddressListVO =
        new ArrayList<GmCRMSurgeonAddressListVO>();
    List<GmCRMSurgeonContactsListVO> listGmCRMSurgeonContactsListVO =
        new ArrayList<GmCRMSurgeonContactsListVO>();
    // List<GmCRMSurgeonAttrVO> listGmCRMSurgeonAttrVO = new ArrayList<GmCRMSurgeonAttrVO>();
    List<GmCRMSurgeonAffiliationVO> listGmCRMSurgeonAffiliationVO =
        new ArrayList<GmCRMSurgeonAffiliationVO>();
    List<GmCRMFileUploadVO> listGmCRMFileUploadVO = new ArrayList<GmCRMFileUploadVO>();

    List<GmCRMSurgeonEducationVO> listSurgeonEducationVO = new ArrayList<GmCRMSurgeonEducationVO>();
    List<GmCRMSurgeonProfessionalVO> listSurgeonProfessionalVO =
        new ArrayList<GmCRMSurgeonProfessionalVO>();

    List<GmProjectBasicVO> listSurgeonProjectBasicVO = new ArrayList<GmProjectBasicVO>();

    List<GmCRMSurgeonRepVO> listSurgeonRepVO = new ArrayList<GmCRMSurgeonRepVO>();
    
    List<GmCRMSurgeonSocialVO> listSurgeonSociallinkVO = new ArrayList<GmCRMSurgeonSocialVO>();



    GmCRMSurgeonlistVO gmCRMSurgeonListVO = new GmCRMSurgeonlistVO();
    GmCRMSurgeonAddressListVO gmCRMSurgeonAddressListVO = new GmCRMSurgeonAddressListVO();
    GmCRMSurgeonContactsListVO gmCRMSurgeonContactsListVO = new GmCRMSurgeonContactsListVO();
    // GmCRMSurgeonAttrVO gmCRMSurgeonAttrVO = new GmCRMSurgeonAttrVO();
    GmCRMSurgeonAffiliationVO gmCRMSurgeonAffiliationVO = new GmCRMSurgeonAffiliationVO();
    GmCRMFileUploadVO gmCRMFileUploadVO = new GmCRMFileUploadVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMSurgeonEducationVO gmCRMSurgeonEducationListVO = new GmCRMSurgeonEducationVO();
    GmCRMSurgeonProfessionalVO gmCRMSurgeonProfessionalVO = new GmCRMSurgeonProfessionalVO();
    GmProjectBasicVO gmProjectBasicVO = new GmProjectBasicVO();
    GmCRMSurgeonRepVO gmSurgeonRepVO = new GmCRMSurgeonRepVO();
    GmCRMSurgeonSocialVO gmSurgeonSociallinkVO = new GmCRMSurgeonSocialVO();

    alSurgeonPartyDtls = (ArrayList) hmParam.get("SURGEONPARTYDTLS");
    hmResult = GmCommonClass.parseNullHashMap((HashMap) alSurgeonPartyDtls.get(0));
    alSurgeonAddressDtls = (ArrayList) hmParam.get("SURGEONADDRESSDTLS");
    alSurgeonContactsDtls = (ArrayList) hmParam.get("SURGEONCONTACTSDTLS");
    // alSurgeonAttrDtls = (ArrayList) hmParam.get("SURGEONATTRDTLS");
    alSurgeonAffiliationDtls = (ArrayList) hmParam.get("SURGEONAFFILIATIONDTLS");
    alSurgeonFileDtls = (ArrayList) hmParam.get("SURGEONFILEDTLS");
    alSurgeonEducatioDtls = (ArrayList) hmParam.get("SURGEONEDUCATIONDTLS");
    alSurgeonProffessionalDtls = (ArrayList) hmParam.get("SURGEONPROFFESSIONALDTLS");
    alSurgeonProjectlDtls = (ArrayList) hmParam.get("SURGEONPROJECTDTLS");
    alSurgeonRepDtls = (ArrayList) hmParam.get("SURGEONREPDTLS");
    alSurgeonsociallink = (ArrayList) hmParam.get("SURGEONSOCIALLINK");

    log.debug(" alSurgeonPartyDtls >>> " + alSurgeonPartyDtls);
    log.debug(" alSurgeonAddressDtls >>> " + alSurgeonAddressDtls);
    log.debug(" alSurgeonContactsDtls >>> " + alSurgeonContactsDtls);
    // log.debug(" alSurgeonAttrDtls >>> " + alSurgeonAttrDtls);
    log.debug(" alSurgeonAffiliationDtls >>> " + alSurgeonAffiliationDtls);
    log.debug(" alSurgeonFileDtls >>> " + alSurgeonFileDtls);
    log.debug(" alEducatioDtls >>> " + alSurgeonEducatioDtls);
    log.debug(" alSurgeonProffessionalDtls >>> " + alSurgeonProffessionalDtls);
    log.debug(" alSurgeonProjectlDtls >>> " + alSurgeonProjectlDtls);
    log.debug(" alSurgeonRepDtls >>> " + alSurgeonRepDtls);
    log.debug(" alSurgeonsociallink >>> " + alSurgeonsociallink);


    listGmCRMSurgeonListVO =
        gmWSUtil.getVOListFromHashMapList(alSurgeonPartyDtls, gmCRMSurgeonListVO,
            gmCRMSurgeonListVO.getMappingProperties());


    listGmCRMSurgeonAddressListVO =
        gmWSUtil.getVOListFromHashMapList(alSurgeonAddressDtls, gmCRMSurgeonAddressListVO,
            gmCRMSurgeonAddressListVO.getMappingProperties());

    listGmCRMSurgeonContactsListVO =
        gmWSUtil.getVOListFromHashMapList(alSurgeonContactsDtls, gmCRMSurgeonContactsListVO,
            gmCRMSurgeonContactsListVO.getMappingProperties());

    /*
     * listGmCRMSurgeonAttrVO = gmWSUtil.getVOListFromHashMapList(alSurgeonAttrDtls,
     * gmCRMSurgeonAttrVO, gmCRMSurgeonAttrVO.getMappingProperties());
     */

    listGmCRMSurgeonAffiliationVO =
        gmWSUtil.getVOListFromHashMapList(alSurgeonAffiliationDtls, gmCRMSurgeonAffiliationVO,
            gmCRMSurgeonAffiliationVO.getMappingProperties());

    listGmCRMFileUploadVO =
        gmWSUtil.getVOListFromHashMapList(alSurgeonFileDtls, gmCRMFileUploadVO,
            gmCRMFileUploadVO.getMappingProperties());


    listSurgeonEducationVO =
        gmWSUtil.getVOListFromHashMapList(alSurgeonEducatioDtls, gmCRMSurgeonEducationListVO,
            gmCRMSurgeonEducationListVO.getMappingProperties());

    listSurgeonProfessionalVO =
        gmWSUtil.getVOListFromHashMapList(alSurgeonProffessionalDtls, gmCRMSurgeonProfessionalVO,
            gmCRMSurgeonProfessionalVO.getMappingProperties());

    listSurgeonProjectBasicVO =
        gmWSUtil.getVOListFromHashMapList(alSurgeonProjectlDtls, gmProjectBasicVO,
            gmProjectBasicVO.getMappingProperties());

    listSurgeonRepVO =
        gmWSUtil.getVOListFromHashMapList(alSurgeonRepDtls, gmSurgeonRepVO,
            gmSurgeonRepVO.getMappingProperties());
    
    listSurgeonSociallinkVO =gmWSUtil.getVOListFromHashMapList(alSurgeonsociallink, gmSurgeonSociallinkVO,
    		gmSurgeonSociallinkVO.getMappingProperties());
    

    gmCRMSurgeonListVO.setArrgmcrmprojectbasicvo(listSurgeonProjectBasicVO);
    gmCRMSurgeonListVO.setArrgmsurgeonsaddressvo(listGmCRMSurgeonAddressListVO);
    gmCRMSurgeonListVO.setArrgmsurgeoncontactsvo(listGmCRMSurgeonContactsListVO);
    // gmCRMSurgeonListVO.setArrgmcrmsurgeonattrvo(listGmCRMSurgeonAttrVO);
    gmCRMSurgeonListVO.setArrgmcrmsurgeonaffiliationvo(listGmCRMSurgeonAffiliationVO);
    gmCRMSurgeonListVO.setArrgmcrmeducationvo(listSurgeonEducationVO);
    gmCRMSurgeonListVO.setArrgmcrmprofessionalvo(listSurgeonProfessionalVO);
    gmCRMSurgeonListVO.setArrgmcrmfileuploadvo(listGmCRMFileUploadVO);
    gmCRMSurgeonListVO.setArrgmcrmsurgeonrepvo(listSurgeonRepVO);
    gmCRMSurgeonListVO.setArrgmcrmsurgeonsocialvo(listSurgeonSociallinkVO);
    gmCRMSurgeonListVO.setNpid(GmCommonClass.parseNull((String) hmResult.get("NPID")));
    gmCRMSurgeonListVO.setFirstnm(GmCommonClass.parseNull((String) hmResult.get("FIRSTNM")));
    gmCRMSurgeonListVO.setLastnm(GmCommonClass.parseNull((String) hmResult.get("LASTNM")));
    gmCRMSurgeonListVO.setMidinitial(GmCommonClass.parseNull((String) hmResult.get("MIDINITIAL")));
    gmCRMSurgeonListVO.setPartyid(GmCommonClass.parseNull((String) hmResult.get("PARTYID")));
    gmCRMSurgeonListVO.setAsstnm(GmCommonClass.parseNull((String) hmResult.get("ASSTNM")));
    gmCRMSurgeonListVO.setMngrnm(GmCommonClass.parseNull((String) hmResult.get("MNGRNM")));
    gmCRMSurgeonListVO.setSalute(GmCommonClass.parseNull((String) hmResult.get("SALUTE")));
    gmCRMSurgeonListVO.setSalutenm(GmCommonClass.parseNull((String) hmResult.get("SALUTENM")));
    gmCRMSurgeonListVO.setAdnm(GmCommonClass.parseNull((String) hmResult.get("ADNM")));
    gmCRMSurgeonListVO.setAdid(GmCommonClass.parseNull((String) hmResult.get("ADID")));
    gmCRMSurgeonListVO.setVpnm(GmCommonClass.parseNull((String) hmResult.get("VPNM")));
    gmCRMSurgeonListVO.setRepnm(GmCommonClass.parseNull((String) hmResult.get("REPNM")));
    gmCRMSurgeonListVO.setRepid(GmCommonClass.parseNull((String) hmResult.get("REPID")));
    gmCRMSurgeonListVO.setPicurl(GmCommonClass.parseNull((String) hmResult.get("PICURL")));
    gmCRMSurgeonListVO.setPhone(GmCommonClass.parseNull((String) hmResult.get("PHONE")));
    gmCRMSurgeonListVO.setEmail(GmCommonClass.parseNull((String) hmResult.get("EMAIL")));
    gmCRMSurgeonListVO.setAphone(GmCommonClass.parseNull((String) hmResult.get("APHONE")));
    gmCRMSurgeonListVO.setAemail(GmCommonClass.parseNull((String) hmResult.get("AEMAIL")));
    gmCRMSurgeonListVO.setBphone(GmCommonClass.parseNull((String) hmResult.get("BPHONE")));
    gmCRMSurgeonListVO.setBemail(GmCommonClass.parseNull((String) hmResult.get("BEMAIL")));
    gmCRMSurgeonListVO.setSpl(GmCommonClass.parseNull((String) hmResult.get("SPL")));
    gmCRMSurgeonListVO.setAddid(GmCommonClass.parseNull((String) hmResult.get("ADDID")));
    gmCRMSurgeonListVO.setAdd1(GmCommonClass.parseNull((String) hmResult.get("ADD1")));
    gmCRMSurgeonListVO.setAdd2(GmCommonClass.parseNull((String) hmResult.get("ADD2")));
    gmCRMSurgeonListVO.setCity(GmCommonClass.parseNull((String) hmResult.get("CITY")));
    gmCRMSurgeonListVO.setStatenm(GmCommonClass.parseNull((String) hmResult.get("STATENM")));
    gmCRMSurgeonListVO.setCountrynm(GmCommonClass.parseNull((String) hmResult.get("COUNTRYNM")));
    gmCRMSurgeonListVO.setZipcode(GmCommonClass.parseNull((String) hmResult.get("ZIPCODE")));
    gmCRMSurgeonListVO.setSplid(GmCommonClass.parseNull((String) hmResult.get("SPLID")));
    gmCRMSurgeonListVO.setKeywords(GmCommonClass.parseNull((String) hmResult.get("KEYWORDS")));
    gmCRMSurgeonListVO.setReppartyid(GmCommonClass.parseNull((String) hmResult.get("REPPARTYID")));
    gmCRMSurgeonListVO.setMerccnt(GmCommonClass.parseNull((String) hmResult.get("MERCCNT")));
    gmCRMSurgeonListVO.setPvisitcnt(GmCommonClass.parseNull((String) hmResult.get("PVISITCNT")));
    gmCRMSurgeonListVO.setScallcnt(GmCommonClass.parseNull((String) hmResult.get("SCALLCNT")));
    gmCRMSurgeonListVO.setPicfileid(GmCommonClass.parseNull((String) hmResult.get("PICFILEID")));
    gmCRMSurgeonListVO.setSurgacsfl(GmCommonClass.parseNull((String) hmParam.get("SURGACSFL")));
    gmCRMSurgeonListVO.setCompanyid(GmCommonClass.parseNull((String) hmResult.get("COMPANYID")));
    gmCRMSurgeonListVO.setCompanyname(GmCommonClass.parseNull((String) hmResult.get("COMPANYNAME")));
    return gmCRMSurgeonListVO;

  }

  // fetch surgeon info value for globus relation
  public ArrayList fetchSurgeonRepInfo(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_cm_globusinfo.gm_fch_surgeon_repinfo", 2);
    gmDBManager.setString(1, (String) hmParams.get("PARTYID"));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
 
    return alResult;

  }
  
  // fetch surgeon  Social media links like Facebook,twitter,vital,etc
   public ArrayList fetchSurgeonlinkInfo(HashMap hmParams) throws AppError {	 
	    GmDBManager gmDBManager = new GmDBManager();
	    ArrayList alResult = new ArrayList();
	    gmDBManager.setPrepareString("gm_pkg_crm_surgeon.gm_fch_surgeon_socialinfo", 2);
	    gmDBManager.setString(1, (String) hmParams.get("PARTYID"));
	    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alResult =
	        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
	            .getObject(2)));
	    gmDBManager.close();
	    log.debug("fetchSurgeonlinkInfo>>>"+alResult.size());
	    return alResult;

	  }


  public ArrayList fetchSurgeonPartyReport(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_surgeon.gm_fch_surgeon_info", 2);
    gmDBManager.setString(1, (String) hmParams.get("PARTYID"));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return alResult;
  }

  public ArrayList fetchSurgeonAddressReport(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_surgeon.gm_fch_surgeonaddr_info", 2);
    gmDBManager.setString(1, (String) hmParams.get("PARTYID"));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return alResult;
  }

  public ArrayList fetchSurgeonRepReport(HashMap hmParams) throws AppError {

    String strPartyID = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT SALESREPINFO.partyid partyid, get_rep_name(t703.c703_sales_rep_id) REPNM, get_ad_rep_name(t703.c703_sales_rep_id) ADNM, GET_AD_REP_ID(t703.c703_sales_rep_id) ADID, "
            + " get_vp_name(t703.c703_sales_rep_id) VPNM, t703.c703_sales_rep_id repid FROM (SELECT NVL( (SELECT t703.c101_party_id FROM T1014_PARTY_AFFILIATION T1014, T704_ACCOUNT T704, "
            + " t703_sales_rep t703 WHERE T704.C704_ACCOUNT_ID = T1014.C704_ACCOUNT_ID AND T1014.C1014_VOID_FL IS NULL AND T1014.C901_AFFILIATION_PRIORITY = '1' AND T1014.C101_PARTY_ID = '"
            + strPartyID
            + "' "
            + " and t703.c703_sales_rep_id = t704.c703_sales_rep_id AND ROWNUM = 1 ), (SELECT C1012_DETAIL FROM t1012_party_other_info WHERE C901_TYPE = '11509' AND c101_party_id = '"
            + strPartyID
            + "' "
            + " )) partyid FROM DUAL )SALESREPINFO, t703_sales_rep t703 where SALESREPINFO.partyid = t703.c101_party_id ");

    log.debug("Query=" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("Sales Rep alReturnBean>>>>>" + alReturn);

    return alReturn;
  }


  public ArrayList fetchSurgeonContactsReport(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_surgeon.gm_fch_surgeoncnt_info", 2);
    gmDBManager.setString(1, (String) hmParams.get("PARTYID"));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return alResult;
  }

  public ArrayList fetchSurgeonAttrReport(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_surgeon.gm_fch_surgeonattr_info", 2);
    gmDBManager.setString(1, (String) hmParams.get("PARTYID"));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return alResult;
  }

  public ArrayList fetchSurgeonAffiliationReport(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
//    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    String strPartyID = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
    sbQuery.append(" SELECT DISTINCT(T101.C101_PARTY_NM) PARENTACCTNM,  T704.C101_PARTY_ID ACCID,   T101.C101_PARTY_NM ACCNM, ");
    sbQuery.append(" T1014.C1014_CITY_NM ACCCITY, GET_CODE_NAME ( T1014.C901_STATE ) ACCSTATE, DECODE(v700.AC_ID,'','N','Y') ACCESSFLAG ");
    sbQuery.append(" FROM T1014_PARTY_AFFILIATION T1014, T704_ACCOUNT T704, T101_PARTY T101 ");
    sbQuery.append(getAccessFilterForAcc());  
    //get the V700 information with Account and apply access filter condition 
    sbQuery.append(" WHERE T1014.C704_ACCOUNT_ID = T704.C704_ACCOUNT_ID ");
    sbQuery.append(" AND T704.C101_PARTY_ID  = T101.C101_PARTY_ID ");
    sbQuery.append(" AND T1014.C704_ACCOUNT_ID  = V700.AC_ID(+) ");
    sbQuery.append(" AND T1014.C101_PARTY_ID  = '"+strPartyID+"' ");
    sbQuery.append(" AND T1014.C704_ACCOUNT_ID IS NOT NULL ");
    sbQuery.append(" AND T704.C704_VOID_FL IS NULL ");
    sbQuery.append(" AND T1014.C1014_VOID_FL IS NULL ");
    sbQuery.append(" AND T101.C101_VOID_FL IS NULL ");
     
    log.debug("New fetchSurgeonAffiliationReport Query ====>>> " + sbQuery.toString());
    alResult = GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
    gmDBManager.close();
    return alResult;

  }

  public ArrayList fetchSurgeonAffiliationSyncReport(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_update.gm_fch_all_surgeonaffl_updt", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    gmDBManager.close();
    return alResult;
  }



  public ArrayList listSurgeonAttrSyncInfo(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_update.gm_fch_all_surgeonattr_updt", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    gmDBManager.close();
    return alResult;
  }

  public ArrayList fetchSurgeonSurgicalSyncReport(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_update.gm_fch_all_surgeonsurg_updt", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    gmDBManager.close();
    return alResult;
  }

  public ArrayList listSurgeonAttrUpdateInfo(HashMap hmParams) throws AppError {

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));

    sbQuery
        .append(" SELECT C1012_PARTY_OTHER_INFO_ID ATTRID, C901_TYPE ATTRTYPE, C1012_DETAIL ATTRVALUE, NVL2(LENGTH(TRIM(TRANSLATE(C1012_DETAIL, '0123456789', ' '))), "
            + " C1012_DETAIL, (SELECT C901_CODE_NM FROM T901_CODE_LOOKUP WHERE TO_CHAR(C901_CODE_ID) = C1012_DETAIL )) ATTRNM, C1012_VOID_FL VOIDFL, "
            + " C101_PARTY_ID PARTYID FROM T1012_PARTY_OTHER_INFO WHERE C1012_VOID_FL IS NULL AND DECODE(C1012_LAST_UPDATED_DATE,null,  C1012_CREATED_DATE,C1012_LAST_UPDATED_DATE) "
            + " >= to_char((SELECT max(c9151_last_sync_date) LASTDATE FROM T9151_DEVICE_SYNC_DTL WHERE c9151_last_updated_by = '"
            + strUserID + "' )) ");

    log.debug("Query=" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;

  }

  // public ArrayList fetchSurgeonAffiliationSyncReport(HashMap hmParams)
  // throws AppError {
  //
  // ArrayList alReturn = new ArrayList();
  //
  // GmDBManager gmDBManager = new GmDBManager();
  //
  // StringBuffer sbQuery = new StringBuffer();
  //
  // sbQuery.append(" SELECT C1014_PARTY_AFFILIATION_ID AFFLID, C704_ACCOUNT_ID ACCID, C101_PARTY_ID PARTYID, C1014_ORGANIZATION_NAME ACCNM, C901_AFFILIATION_PRIORITY PRIORITY, C1014_AFFILIATION_PERCENT CASEPERCENT, "
  // +
  // " C1014_CITY_NM ACCCITY, GET_CODE_NAME(C901_STATE) ACCSTATE, C901_STATE ACCSTATEID, C1014_VOID_FL VOIDFL FROM T1014_PARTY_AFFILIATION WHERE C1014_VOID_FL "
  // +
  // " IS NULL AND DECODE(c1014_last_updated_date,null,  c1014_CREATED_DATE,c1014_last_updated_date)  > '31-DEC-15' ");
  //
  // log.debug("Query=" + sbQuery.toString());
  //
  // alReturn = GmCommonClass.parseNullArrayList(gmDBManager
  // .queryMultipleRecords(sbQuery.toString()));
  //
  // log.debug("alReturnBean>>>>>" + alReturn);
  //
  // return alReturn;
  //
  // }

  public ArrayList fetchSurgeonAffiliationUpdateReport(HashMap hmParams) throws AppError {

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));

    sbQuery
        .append(" SELECT C1014_PARTY_AFFILIATION_ID AFFLID, C704_ACCOUNT_ID ACCID, C101_PARTY_ID PARTYID, C1014_ORGANIZATION_NAME ACCNM, C901_AFFILIATION_PRIORITY PRIORITY, C1014_AFFILIATION_PERCENT CASEPERCENT, "
            + " C1014_CITY_NM ACCCITY, GET_CODE_NAME(C901_STATE) ACCSTATE, C901_STATE ACCSTATEID, C1014_VOID_FL VOIDFL FROM T1014_PARTY_AFFILIATION WHERE C1014_VOID_FL "
            + " IS NULL AND DECODE(c1014_last_updated_date,null,  c1014_CREATED_DATE,c1014_last_updated_date) >= to_char((SELECT max(c9151_last_sync_date) LASTDATE FROM T9151_DEVICE_SYNC_DTL WHERE c9151_last_updated_by "
            + " = '" + strUserID + "')) ");

    log.debug("Query=" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;

  }

  public ArrayList listSurgeonPartysSyncInfo(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_update.gm_fch_all_surgeonparty_updt", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    gmDBManager.close();
    return alResult;
  }

  public ArrayList listSurgeonPartysUpdateInfo(HashMap hmParams) throws AppError {

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));

    sbQuery
        .append(" SELECT  C6600_SURGEON_NPI_ID NPID, C101_PARTY_SURGEON_ID PARTYID, C6600_SURGEON_ASSISTANT_NM ASSTNM, C6600_SURGEON_MANAGER_NM MNGRNM, "
            + " C6600_VOID_FL VOIDFL FROM T6600_PARTY_SURGEON WHERE C6600_VOID_FL IS NULL AND DECODE(C6600_LAST_UPDATED_DATE,null,  c6600_created_date,C6600_LAST_UPDATED_DATE) >= "
            + " to_char((SELECT max(c9151_last_sync_date) LASTDATE FROM T9151_DEVICE_SYNC_DTL WHERE c9151_last_updated_by = '"
            + strUserID + "' )) ");

    log.debug("Query=" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;

  }

  public ArrayList listSurgeonRepADVPSyncInfo(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_update.gm_fch_all_ter_map", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    gmDBManager.close();
    return alResult;
  }



  public ArrayList fetchSurgeonSurgicalUpdateReport(HashMap hmParams) throws AppError {

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));

    sbQuery
        .append(" SELECT C901_PROCEDURE PROCEDUREID, GET_CODE_NAME(C901_PROCEDURE) PROCEDURENM, C6610_CASES CASES, C207_SET_ID SYSID, C6610_NON_GLOBUS_SYSTEM SYSNM, "
            + " C6610_COMPANY_NM COMPANY, C6610_SURGICAL_ACTIVITY_ID SACTID, C101_PARTY_ID PARTYID FROM T6610_SURGICAL_ACTIVITY WHERE C6610_VOID_FL IS NULL and "
            + " c6610_last_updated_date >= to_char((SELECT max(c9151_last_sync_date) LASTDATE FROM T9151_DEVICE_SYNC_DTL WHERE c9151_last_updated_by = '"
            + strUserID + "')) ");

    log.debug("fetchSurgeonSurgicalUpdateReport Query=" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;

  }

  public ArrayList fetchSurgeonDocSyncReport(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_update.gm_fch_all_surgeondocs_updt", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    gmDBManager.close();
    return alResult;
  }

  public ArrayList fetchSurgeonDocUpdateReport(HashMap hmParams) throws AppError {

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));

    sbQuery
        .append(" SELECT unique C903_UPLOAD_FILE_LIST FILEID, C901_REF_TYPE REFTYPE, C903_FILE_NAME FILENAME, C903_REF_ID REFID, C901_REF_GRP REFGROUP, C901_FILE_TITLE FILETITLE, C901_REF_TYPE REFTYPE, GET_CODE_NAME(C901_REF_TYPE) REFTYPENM, C903_DELETE_FL VOIDFL,TO_CHAR(DECODE(C903_LAST_UPDATED_DATE,NULL,C903_CREATED_DATE,C903_LAST_UPDATED_DATE),GET_RULE_VALUE('DATEFMT','DATEFORMAT')) UPDATEDDATE "
            + " , GET_USER_NAME(DECODE(C903_LAST_UPDATED_BY,NULL,C903_CREATED_BY,C903_LAST_UPDATED_BY)) UPDATEDBY FROM T903_UPLOAD_FILE_LIST WHERE "
            + " C903_DELETE_FL IS NULL AND C901_REF_GRP in ('103112','107993','107994') and DECODE(C903_LAST_UPDATED_DATE,NULL,C903_CREATED_DATE,C903_LAST_UPDATED_DATE) >= to_char((SELECT max(c9151_last_sync_date) LASTDATE FROM T9151_DEVICE_SYNC_DTL WHERE c9151_last_updated_by = '"
            + strUserID + "')) ");

    log.debug("Query=" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;

  }



  public ArrayList fetchSurgeonLogSync(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_update.gm_fch_all_surgeonlog_updt", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    gmDBManager.close();
    return alResult;
  }

  public ArrayList fetchSurgeonLogUpdate(HashMap hmParams) throws AppError {

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));

    sbQuery
        .append(" SELECT c902_log_id commentid, c902_comments comments, get_code_name(c902_comments) commentnm, c902_ref_id refid FROM t902_log WHERE c902_type IN ('105182','105183') "
            + " AND c902_void_fl IS NULL and decode(C902_LAST_UPDATED_DATE, null, C902_CREATED_DATE,C902_LAST_UPDATED_DATE) >= to_char((SELECT max(c9151_last_sync_date) LASTDATE FROM "
            + " T9151_DEVICE_SYNC_DTL WHERE c9151_last_updated_by = '" + strUserID + "')) ");

    log.debug("Query=" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;

  }

  public ArrayList fetchSurgeonSurgicalReport(HashMap hmParams) throws AppError {

    String strPartyID = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    /* This prc call should be removed after surgical activity charts done */
    gmDBManager.setPrepareString("gm_pkg_crm_surgeon.gm_fch_surgeonsurg_info", 2);
    gmDBManager.setString(1, (String) hmParams.get("PARTYID"));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();


    StringBuffer sbQuery2 = new StringBuffer();
    sbQuery2
        .append(" SELECT a.SYSID, GET_SET_NAME(a.SYSID) SYSNM, a.CASES, b.PROCEDUREID, b.PROCEDURENM FROM (SELECT T207C.C207_SET_ID SYSID, COUNT (t501.c501_order_id) CASES "
            + " FROM t6640_npi_transaction t6640, T501_ORDER T501, T501B_ORDER_BY_SYSTEM T501B, T207C_SET_ATTRIBUTE T207C WHERE t6640.c101_party_surgeon_id  = '"
            + strPartyID
            + "'  AND t6640.c6640_valid_fl = 'Y' AND t6640.c6640_void_fl IS NULL "
            + " AND t6640.c6640_ref_id = t501.c501_order_id  AND t501.c501_order_id = t501b.c501_order_id "
            + " AND t501.c501_parent_order_id IS NULL AND T501.C501_VOID_FL IS NULL "
            + " AND T501.C501_DELETE_FL IS NULL AND t501b.c501b_void_fl IS NULL "
            + " AND NVL (c901_order_type, -9999) NOT IN "
            + " (SELECT C906_RULE_VALUE FROM v901_ORDER_TYPE_GRP ) "
            + " AND T501B.C207_SYSTEM_ID  = T207C.C207_SET_ID  AND T207C.C901_ATTRIBUTE_TYPE = '103118' "
            + " AND T207C.C207C_VOID_FL      IS NULL GROUP BY T207C.C207_SET_ID ) a,"
            + " (SELECT C207_SET_ID SYSID, LISTAGG( get_code_name(C207C_ATTRIBUTE_VALUE), ' / ') WITHIN GROUP ( "
            + " ORDER BY C207C_ATTRIBUTE_VALUE) AS PROCEDURENM, LISTAGG( C207C_ATTRIBUTE_VALUE, ' / ') WITHIN GROUP ( "
            + " ORDER BY C207C_ATTRIBUTE_VALUE) AS PROCEDUREID FROM T207C_SET_ATTRIBUTE "
            + " WHERE C901_ATTRIBUTE_TYPE = '103118' AND C207C_VOID_FL IS NULL GROUP BY C207_SET_ID ) b WHERE a.SYSID = b.SYSID order by CASES desc");


    log.debug("fetchSurgeonSurgicalReport Query2 =" + sbQuery2);

    alResult.addAll(GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery2
        .toString())));

    log.debug("alReturnBean After Second Query >>>>>" + alResult.size());

    return alResult;

  }

  public ArrayList fetchSurgeonActivityReport(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_surgeon.gm_fch_surgeonactv_info", 2);
    gmDBManager.setString(1, (String) hmParams.get("PARTYID"));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return alResult;

  }

  /**
   * fetchPartyActivityReport - This method will displays globus activity list in surgeon module
   * from T6650 table
   * 
   * @param - HashMap
   * @return - ArrayList
   * @exception - AppError
   */
  public ArrayList fetchPartyActivityReport(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_surgeon.gm_fch_partyactv_info", 3);
    gmDBManager.setString(1, (String) hmParams.get("PARTYID"));
    gmDBManager.setString(2, (String) hmParams.get("PAGINATION"));
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));
    gmDBManager.close();
    return alResult;
  }

  /**
   * fetchPartyEducationReport - This method will displays Education Details list in surgeon module
   * from T1010 table
   * 
   * @param - HashMap
   * @return - ArrayList
   * @exception - AppError
   */

  public ArrayList fetchSurgeonEducationInfo(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_cm_education.gm_fch_educationinfo", 2);
    gmDBManager.setString(1, (String) hmParams.get("PARTYID"));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return alResult;

  }


  /**
   * Fetches the 'About Me' details for party
   * 
   * @param strPartyId
   * @return java.util.HashMap
   * @throws com.globus.common.beans.AppError
   * @roseuid 48FCE457017F
   */
  public ArrayList fetchSurgeonProffessionalInfo(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_cm_career.gm_fch_careerinfo", 2);
    gmDBManager.setString(1, (String) hmParams.get("PARTYID"));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return alResult;
  }

  // save the rep details for globus relation

  public String saveSurgeonDetail(HashMap hmParams, List<GmCRMSurgeonAddressListVO> alAddressVO,
      GmCRMSurgeonAddressListVO gmAddressVO, List<GmCRMSurgeonContactsListVO> alContactsVO,
      GmCRMSurgeonContactsListVO gmContactsVO, 
//      List<GmCRMSurgeonAffiliationVO> alAffiliationVO, GmCRMSurgeonAffiliationVO gmAffiliation, 
      GmCRMFileUploadVO gmCRMFileUploadVO,
      List<GmCRMFileUploadVO> alCRMFileUploadVO, GmCRMSurgeonEducationVO gmCRMSurgeonEducationVO,
      List<GmCRMSurgeonEducationVO> alEducationVO,
      GmCRMSurgeonProfessionalVO gmSurgeonProfessionalVO,
      List<GmCRMSurgeonProfessionalVO> alProfessionalVO, GmCRMSurgeonRepVO gmCRMSurgeonRepVO,
      List<GmCRMSurgeonRepVO> alarrgmcrmrepvo ,GmCRMSurgeonSocialVO gmSociallinkVO,List<GmCRMSurgeonSocialVO> alSocialLinkVO


  ) {
    // TODO Auto-generated method stub
	 
    String strPartyID = "";

    String strNPID = GmCommonClass.parseNull((String) hmParams.get("NPID"));
    String partyID = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
    HashMap hmFiles = (HashMap) hmParams.get("gmcrmfileuploadvo");

    GmDBManager gmDBManager = new GmDBManager();
    GmCacheManager gmCacheManager = new GmCacheManager();
    strPartyID = savePartyInfo(hmParams, gmDBManager);
    hmParams.put("PARTYID", strPartyID);

    savePartySurgeon(hmParams, gmDBManager);
    if (alAddressVO != null) {
      saveAddressInfo(hmParams, gmDBManager, alAddressVO);
    }
    if (alContactsVO != null) {
      saveContactInfo(hmParams, gmDBManager, alContactsVO);
    }
    // if (alAttrVO != null) {
    // saveAttrInfo(hmParams, gmDBManager, alAttrVO, gmAttrVO);
    // }
if (alSocialLinkVO != null) {
         saveSocialLinkInfo(hmParams, gmDBManager, alSocialLinkVO, gmSociallinkVO);
         }
//    if (alAffiliationVO != null) {
//      saveAffiliationInfo(hmParams, gmDBManager, alAffiliationVO, gmAffiliation);
//    }
    // saveSurgicalInfo(hmParams, gmDBManager, alSurgicalVO, gmSurgical);
    if (alCRMFileUploadVO != null) {
      saveFilesInfo(hmParams, gmDBManager, alCRMFileUploadVO, gmCRMFileUploadVO);
    }
    if (alEducationVO != null) {
      saveEducationInfo(hmParams, gmDBManager, alEducationVO, gmCRMSurgeonEducationVO);
    }
    if (alProfessionalVO != null) {
      saveProfessionalInfo(hmParams, gmDBManager, alProfessionalVO, gmSurgeonProfessionalVO);
    }
    if (alarrgmcrmrepvo != null) {
      saveRepInfo(hmParams, gmDBManager, alarrgmcrmrepvo, gmCRMSurgeonRepVO);
    }
    if (partyID.equals("")) {
    	gmCacheManager.removePatternKeys("CRMPARTYLIST*");
      }
     

    StringBuffer sbQuery = new StringBuffer();
    gmDBManager.commit();

    return strPartyID;
  }

  public void saveFilesInfo(HashMap hmParams, GmDBManager gmDBManager,
      List<GmCRMFileUploadVO> alCRMFileUploadVO, GmCRMFileUploadVO gmCRMFileUploadVO) {
    //
    // int length = alCRMFileUploadVO.size();
    //
    // for (int i = 0; i < length; i++) {
    // GmCRMFileUploadVO gmCRMFileUploadOneVO = alCRMFileUploadVO.get(i);
    // GmCommonBean gmCommonBean = new GmCommonBean();
    // log.debug("File Ref ID >>>> " + hmParams.get("PARTYID"));
    // gmDBManager.setPrepareString("gm_pkg_crm_fileupload.gm_sav_crm_fileupload",
    // 8);
    // gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    // gmDBManager.setString(1,
    // GmCommonClass.parseNull(gmCRMFileUploadOneVO.getFileid()));
    // gmDBManager.setString(2,
    // GmCommonClass.parseNull(gmCRMFileUploadOneVO.getFiletitle()));
    // gmDBManager.setString(3,
    // GmCommonClass.parseNull(gmCRMFileUploadOneVO.getFilename()));
    // gmDBManager.setString(4, GmCommonClass.parseNull((String)
    // hmParams.get("PARTYID")));
    // gmDBManager.setString(5,
    // GmCommonClass.parseNull(gmCRMFileUploadOneVO.getReftype()));
    // gmDBManager.setString(6, "107993");
    // gmDBManager.setString(7,
    // GmCommonClass.parseNull(gmCRMFileUploadOneVO.getDeletefl()));
    // gmDBManager.setString(8, GmCommonClass.parseNull((String)
    // hmParams.get("USERID")));
    //
    // gmDBManager.execute();
    // }

  }

  public String savePartyInfo(HashMap hmParams, GmDBManager gmDBManager) {

    String strPartyID = "";
    gmDBManager.setPrepareString("gm_pkg_crm_surgeon.gm_sav_surgeoninfo", 7);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("PARTYID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("FIRSTNM")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParams.get("LASTNM")));
    gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParams.get("MIDINITIAL")));
    gmDBManager.setString(5, GmCommonClass.parseNull((String) hmParams.get("ACTIVEFL")));
    gmDBManager.setString(6, GmCommonClass.parseNull((String) hmParams.get("COMPANYID")));
    gmDBManager.setString(7, GmCommonClass.parseNull((String) hmParams.get("USERID")));

    gmDBManager.execute();
    strPartyID = GmCommonClass.parseNull(gmDBManager.getString(1));

    return strPartyID;
  }

  public void savePartySurgeon(HashMap hmParams, GmDBManager gmDBManager) {

    gmDBManager.setPrepareString("gm_pkg_crm_surgeon.gm_sav_partysurgeon", 7);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("PARTYID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("NPID")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParams.get("ASSTNM")));
    gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParams.get("MNGRNM")));
    gmDBManager.setString(5, null);
    gmDBManager.setString(6, GmCommonClass.parseNull((String) hmParams.get("USERID")));
    gmDBManager.setString(7, GmCommonClass.parseNull((String) hmParams.get("SALUTE")));
    gmDBManager.execute();
  }

  public void saveAddressInfo(HashMap hmParams, GmDBManager gmDBManager,
      List<GmCRMSurgeonAddressListVO> alAddressVO) {

    GmCRMActivityBean gmCRMActivityBean = new GmCRMActivityBean();
    int length = alAddressVO.size();
    for (int i = 0; i < length; i++) {
      GmCRMSurgeonAddressListVO gmAddressVO = alAddressVO.get(i);
      HashMap hmInput = new HashMap();

      log.debug("getAddid FL>>>> " + GmCommonClass.parseNull(gmAddressVO.getAddid()));
      log.debug("Address save hmParams >>>> " + hmParams);
      gmDBManager.setPrepareString("gm_pkg_crm_address.gm_crm_sav_addressinfo", 15);
      gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
      gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
      gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("PARTYID")));
      gmDBManager.setString(2, GmCommonClass.parseNull(gmAddressVO.getAddid()));
      gmDBManager.setString(3, GmCommonClass.parseNull(gmAddressVO.getAddrtyp()));
      gmDBManager.setString(4, GmCommonClass.parseNull(gmAddressVO.getAdd1()));
      gmDBManager.setString(5, GmCommonClass.parseNull(gmAddressVO.getAdd2()));
      gmDBManager.setString(6, GmCommonClass.parseNull(gmAddressVO.getCity()));
      gmDBManager.setString(7, GmCommonClass.parseNull(gmAddressVO.getStateid()));
      gmDBManager.setString(8, GmCommonClass.parseNull(gmAddressVO.getCountryid()));
      gmDBManager.setString(9, GmCommonClass.parseNull(gmAddressVO.getZip()));
      gmDBManager.setString(10, ""); //
      gmDBManager.setString(11, GmCommonClass.parseNull((String) hmParams.get("USERID")));
      gmDBManager.setString(12, "");
      gmDBManager.setString(13, GmCommonClass.parseNull(gmAddressVO.getPrimaryfl()));
      gmDBManager.setString(14, "");
      gmDBManager.setString(15, GmCommonClass.parseNull(gmAddressVO.getVoidfl()));

      gmDBManager.execute();

      String strAddrId = GmCommonClass.parseNull(gmDBManager.getString(2));

      GmCRMLogVO gmCRMLogVO = new GmCRMLogVO();

      gmCRMLogVO.setLogid(gmAddressVO.getCommentid());
      gmCRMLogVO.setRefid(strAddrId);
      gmCRMLogVO.setLogtypeid("105180");
      gmCRMLogVO.setLogcomment(gmAddressVO.getComments());
      gmCRMLogVO.setUserid((String) hmParams.get("USERID"));

      gmCRMActivityBean.saveLog(gmDBManager, gmCRMLogVO);

    }

  }

  public void saveContactInfo(HashMap hmParams, GmDBManager gmDBManager,
      List<GmCRMSurgeonContactsListVO> alContactsVO) {

    log.debug("saveContactInfo>>>>");

    int length = alContactsVO.size();
    for (int i = 0; i < length; i++) {
      GmCRMSurgeonContactsListVO gmContactsVO = alContactsVO.get(i);
      GmCommonBean gmCommonBean = new GmCommonBean();

      gmDBManager.setPrepareString("gm_pkg_crm_contact.gm_sav_contactinfo", 10);
      gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
      gmDBManager.setString(1, GmCommonClass.parseNull(gmContactsVO.getConid()));
      gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("PARTYID")));
      gmDBManager.setString(3, GmCommonClass.parseNull(gmContactsVO.getConmode()));
      gmDBManager.setString(4, GmCommonClass.parseNull(gmContactsVO.getContype()));
      gmDBManager.setString(5, GmCommonClass.parseNull(gmContactsVO.getConvalue()));
      gmDBManager.setString(6, "");
      gmDBManager.setString(7, GmCommonClass.parseNull(gmContactsVO.getPrimaryfl()));
      gmDBManager.setString(8, ""); //
      gmDBManager.setString(9, GmCommonClass.parseNull((String) hmParams.get("USERID")));
      gmDBManager.setString(10, GmCommonClass.parseNull(gmContactsVO.getVoidfl()));
      gmDBManager.execute();
      // gmDBManager.close();
    }

  }

  public void saveAttrInfo(HashMap hmParams, GmDBManager gmDBManager,
      List<GmCRMSurgeonAttrVO> alAttrVO, GmCRMSurgeonAttrVO gmAttrVO) {

    log.debug("saveAttrInfo>>>>");
    int length = alAttrVO.size();
    for (int i = 0; i < length; i++) {
      GmCRMSurgeonAttrVO gmOrderparamVO = alAttrVO.get(i);
      GmCommonBean gmCommonBean = new GmCommonBean();

      log.debug("gm_pkg_crm_surgeon_new>>>>" + gmOrderparamVO.getAttrid());

      log.debug("Party ID>>>>>" + hmParams.get("PARTYID"));
      log.debug("Code ID>>>>>" + gmOrderparamVO.getAttrtype());

      gmDBManager.setPrepareString("gm_pkg_crm_surgeon.gm_sav_crm_surgeon_atrr", 6);
      // gmDBManager.setInt(1,
      // Integer.parseInt(GmCommonClass.parseNull(gmOrderparamVO.getAttrid())));
      gmDBManager.setString(1, GmCommonClass.parseNull(gmOrderparamVO.getAttrid()));
      gmDBManager.setInt(2,
          Integer.parseInt(GmCommonClass.parseNull((String) hmParams.get("PARTYID"))));
      gmDBManager
          .setInt(3, Integer.parseInt(GmCommonClass.parseNull(gmOrderparamVO.getAttrtype())));
      gmDBManager.setString(4, GmCommonClass.parseNull(gmOrderparamVO.getAttrvalue()));
      gmDBManager.setString(5, GmCommonClass.parseNull((String) hmParams.get("USERID")));
      gmDBManager.setString(6, GmCommonClass.parseNull(gmOrderparamVO.getVoidfl()));
      gmDBManager.execute();
      // gmDBManager.close();
    }

  }
  
  //Function for save and update the socialmedia link like,Facebook,twitter,vitals etc 
   public void saveSocialLinkInfo(HashMap hmParams, GmDBManager gmDBManager,
	      List<GmCRMSurgeonSocialVO> alSocialLinkVO, GmCRMSurgeonSocialVO  gmSociallinkVO) {	  
	    log.debug("saveSocialLinkInfo>>>>"+hmParams);
	    int length = alSocialLinkVO.size();
	    for (int i = 0; i < length; i++) {
	    	GmCRMSurgeonSocialVO gmOrderparamVO = alSocialLinkVO.get(i);
	      GmCommonBean gmCommonBean = new GmCommonBean();        
	      gmDBManager.setPrepareString ("gm_pkg_crm_surgeon.gm_sav_sociallink",9);	   
	      
	      gmDBManager.setInt(1,
		          Integer.parseInt(GmCommonClass.parseNull((String) hmParams.get("PARTYID"))));
	      gmDBManager.setString(2, GmCommonClass.parseNull(gmOrderparamVO.getFacebook()));	      
	      gmDBManager.setString(3, GmCommonClass.parseNull(gmOrderparamVO.getTwitter()));
	      gmDBManager.setString(4, GmCommonClass.parseNull(gmOrderparamVO.getGoogle()));	      
	      gmDBManager.setString(5, GmCommonClass.parseNull(gmOrderparamVO.getLinkedin()));	      
	      gmDBManager.setString(6, GmCommonClass.parseNull(gmOrderparamVO.getWebsite()));
	      gmDBManager.setString(7, GmCommonClass.parseNull(gmOrderparamVO.getOpenpayment()));
	      gmDBManager.setString(8, GmCommonClass.parseNull(gmOrderparamVO.getVitals()));
	      gmDBManager.setString(9, GmCommonClass.parseNull((String) hmParams.get("USERID")));
	     
	     
	   
	      gmDBManager.execute();
	      // gmDBManager.close();
	    }

	  }

/*  public void saveAffiliationInfo(HashMap hmParams, GmDBManager gmDBManager,
      List<GmCRMSurgeonAffiliationVO> alAffiliationVO, GmCRMSurgeonAffiliationVO gmAffiliation) {

    log.debug("saveAffiliationInfo>>>>");

    GmCRMActivityBean gmCRMActivityBean = new GmCRMActivityBean();
    int length = alAffiliationVO.size();
    for (int i = 0; i < length; i++) {
      GmCRMSurgeonAffiliationVO gmOrderparamVO = alAffiliationVO.get(i);
      GmCommonBean gmCommonBean = new GmCommonBean();

      gmDBManager.setPrepareString("gm_pkg_crm_surgeon.gm_sav_crm_surgeon_affiliation", 11);
      gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);
      gmDBManager.setInt(1,
          Integer.parseInt(GmCommonClass.parseZero((String) hmParams.get("PARTYID"))));
      gmDBManager.setString(2, GmCommonClass.parseNull(gmOrderparamVO.getAccnm()));
      gmDBManager
          .setInt(3, Integer.parseInt(GmCommonClass.parseZero(gmOrderparamVO.getPriority())));
      gmDBManager.setString(4, GmCommonClass.parseNull(gmOrderparamVO.getCasepercent()));
      gmDBManager.setString(5, GmCommonClass.parseNull((String) hmParams.get("USERID")));
      gmDBManager.setString(6, GmCommonClass.parseNull(gmOrderparamVO.getAfflid()));
      gmDBManager.setInt(7,
          Integer.parseInt(GmCommonClass.parseZero(gmOrderparamVO.getAccstateid())));
      gmDBManager.setString(8, GmCommonClass.parseNull(gmOrderparamVO.getAcccity()));
      gmDBManager.setString(9, GmCommonClass.parseNull(gmOrderparamVO.getAccid()));
      gmDBManager.setString(10, GmCommonClass.parseNull(gmOrderparamVO.getVoidfl()));
      gmDBManager.setString(11, GmCommonClass.parseNull(gmOrderparamVO.getComments()));
      gmDBManager.execute();
    }

  }*/

  public void saveSurgicalInfo(HashMap hmParams, GmDBManager gmDBManager,
      List<GmCRMSurgeonSurgicalVO> alSurgicalVO, GmCRMSurgeonSurgicalVO gmSurgical) {

    log.debug("saveSurgicalInfo>>>>" + alSurgicalVO.size());

    GmCRMActivityBean gmCRMActivityBean = new GmCRMActivityBean();

    int length = alSurgicalVO.size();
    for (int i = 0; i < length; i++) {
      GmCRMSurgeonSurgicalVO gmOrderparamVO = alSurgicalVO.get(i);
      GmCommonBean gmCommonBean = new GmCommonBean();

      gmDBManager.setPrepareString("gm_pkg_crm_surgeon.gm_sav_crm_surgeon_surgical", 11);
      gmDBManager.registerOutParameter(9, OracleTypes.VARCHAR);

      gmDBManager.setInt(1,
          Integer.parseInt(GmCommonClass.parseNull((String) hmParams.get("PARTYID"))));
      gmDBManager.setString(2, GmCommonClass.parseNull(gmOrderparamVO.getProcedureid()));
      gmDBManager.setString(3, GmCommonClass.parseNull(gmOrderparamVO.getProcedurenm()));
      gmDBManager.setString(4, GmCommonClass.parseNull(gmOrderparamVO.getSysid()));
      gmDBManager.setString(5, GmCommonClass.parseNull(gmOrderparamVO.getSysnm()));
      gmDBManager.setString(6, GmCommonClass.parseNull(gmOrderparamVO.getCompany()));
      gmDBManager.setInt(7, Integer.parseInt(GmCommonClass.parseNull(gmOrderparamVO.getCases())));
      gmDBManager.setString(8, GmCommonClass.parseNull((String) hmParams.get("USERID")));
      gmDBManager.setString(9, GmCommonClass.parseNull(gmOrderparamVO.getSactid()));
      gmDBManager.setString(10, GmCommonClass.parseNull(gmOrderparamVO.getVoidfl()));
      gmDBManager.setString(11, GmCommonClass.parseNull(gmOrderparamVO.getComments()));
      gmDBManager.execute();

    }

  }

  public void saveEducationInfo(HashMap hmParams, GmDBManager gmDBManager,
      List<GmCRMSurgeonEducationVO> alEducationVO, GmCRMSurgeonEducationVO gmEducationVO) {

    log.debug("saveSurgicalInfo>>>>" + alEducationVO.size());

    GmCRMActivityBean gmCRMActivityBean = new GmCRMActivityBean();

    int length = alEducationVO.size();
    for (int i = 0; i < length; i++) {
      GmCRMSurgeonEducationVO gmOrderparamVO = alEducationVO.get(i);

      gmDBManager.setPrepareString("gm_pkg_cm_education.gm_sav_educationinfo", 9);
      gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);

      gmDBManager.setString(1, GmCommonClass.parseNull(gmOrderparamVO.getId()));
      gmDBManager.setInt(2,
          Integer.parseInt(GmCommonClass.parseNull((String) hmParams.get("PARTYID"))));
      gmDBManager.setString(3, GmCommonClass.parseNull(gmOrderparamVO.getLevelid()));
      gmDBManager.setString(4, GmCommonClass.parseNull(gmOrderparamVO.getInstitutenm()));
      gmDBManager.setString(5, GmCommonClass.parseNull(gmOrderparamVO.getClassyear()));
      gmDBManager.setString(6, GmCommonClass.parseNull(gmOrderparamVO.getCourse()));
      gmDBManager.setString(7, GmCommonClass.parseNull(gmOrderparamVO.getDesignationid()));
      gmDBManager.setString(8, GmCommonClass.parseNull((String) hmParams.get("USERID")));
      gmDBManager.setString(9, GmCommonClass.parseNull(gmOrderparamVO.getVoidfl()));
      gmDBManager.execute();

    }

  }


  public void saveProfessionalInfo(HashMap hmParams, GmDBManager gmDBManager,
      List<GmCRMSurgeonProfessionalVO> alProfessionalVO, GmCRMSurgeonProfessionalVO gmProfessionalVO) {
    GmCRMActivityBean gmCRMActivityBean = new GmCRMActivityBean();

    int length = alProfessionalVO.size();
    for (int i = 0; i < length; i++) {
      GmCRMSurgeonProfessionalVO gmOrderparamVO = alProfessionalVO.get(i);

      gmDBManager.setPrepareString("gm_pkg_cm_career.gm_sav_careerinfo", 17);
      gmDBManager.registerOutParameter(14, OracleTypes.VARCHAR);

      gmDBManager.setInt(1,
          Integer.parseInt(GmCommonClass.parseNull((String) hmParams.get("PARTYID"))));
      gmDBManager.setString(2, GmCommonClass.parseNull(gmOrderparamVO.getTitle()));
      gmDBManager.setString(3, GmCommonClass.parseNull(gmOrderparamVO.getCompanyname()));
      gmDBManager.setString(4, GmCommonClass.parseNull(gmOrderparamVO.getCompanydesc()));
      gmDBManager.setString(5, GmCommonClass.parseNull(gmOrderparamVO.getStartmonth()));
      gmDBManager.setString(6, GmCommonClass.parseNull(gmOrderparamVO.getStartyear()));
      gmDBManager.setString(7, GmCommonClass.parseNull(gmOrderparamVO.getEndmonth()));
      gmDBManager.setString(8, GmCommonClass.parseNull(gmOrderparamVO.getEndyear()));
      gmDBManager.setString(9, GmCommonClass.parseNull(gmOrderparamVO.getStillinposition()));
      gmDBManager.setString(10, GmCommonClass.parseNull(gmOrderparamVO.getGroupdivision()));
      gmDBManager.setString(11, GmCommonClass.parseNull(gmOrderparamVO.getCountryid()));
      gmDBManager.setString(12, GmCommonClass.parseNull(gmOrderparamVO.getStateid()));
      gmDBManager.setString(13, GmCommonClass.parseNull(gmOrderparamVO.getCity()));
      gmDBManager.setString(14, GmCommonClass.parseNull(gmOrderparamVO.getIndustryid()));
      gmDBManager.setString(15, GmCommonClass.parseNull(gmOrderparamVO.getCareerid()));
      gmDBManager.setString(16, GmCommonClass.parseNull((String) hmParams.get("USERID")));
      gmDBManager.setString(17, GmCommonClass.parseNull(gmOrderparamVO.getVoidfl()));
      gmDBManager.execute();

    }

  }

  /**
   * This method used to save the repinfo for surgeon relation.
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */

  public void saveRepInfo(HashMap hmParams, GmDBManager gmDBManager,
      List<GmCRMSurgeonRepVO> alSurgeonRepVO, GmCRMSurgeonRepVO gmSurgeonRepVO) {
    GmCRMActivityBean gmCRMActivityBean = new GmCRMActivityBean();
    int length = alSurgeonRepVO.size();
    for (int i = 0; i < length; i++) {
      GmCRMSurgeonRepVO gmCRMSurgeonRepVO = alSurgeonRepVO.get(i);
      gmDBManager.setPrepareString("gm_pkg_cm_globusinfo.gm_sav_globusinfo", 7);
      gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);
      gmDBManager.setString(1, GmCommonClass.parseNull(gmCRMSurgeonRepVO.getPlinkid()));
      gmDBManager.setInt(2,
          Integer.parseInt(GmCommonClass.parseNull((String) hmParams.get("PARTYID"))));
      gmDBManager.setString(3, GmCommonClass.parseNull(gmCRMSurgeonRepVO.getRepid()));
      gmDBManager.setString(4, GmCommonClass.parseNull(gmCRMSurgeonRepVO.getPrimaryfl()));
      gmDBManager.setString(5, GmCommonClass.parseNull((String) hmParams.get("USERID")));
      gmDBManager.setString(7, GmCommonClass.parseNull(gmCRMSurgeonRepVO.getVoidfl()));
      gmDBManager.execute();

    }

  }

  /**
   * This method used to fetch the surgical activity by revenue.
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */
  public ArrayList fetchSurigcalActivityByRevenue(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    String strPartyId = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
    String strFromDate = GmCommonClass.parseNull((String) hmParams.get("FROMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParams.get("TODATE"));
    log.debug("hmParams>>> "+hmParams+"hmParam   >> "+hmParam);
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
    log.debug("strCondition>>> "+strCondition+"strPartyId   >> "+strPartyId+strFromDate+strToDate);
    StringBuffer sbQuery = new StringBuffer();
    
    sbQuery.append(" SELECT YEARMON, SUM(VALUE) VALUE, GRPID, GRPNM, GRPID2, GRPNM2, GRPID3 FROM ( ");
    sbQuery.append(" SELECT TO_CHAR(T501.C501_ORDER_DATE ,'YYYYMM')YEARMON,(T502.C502_ITEM_PRICE * T502.C502_ITEM_QTY) VALUE, ");
    sbQuery.append(" V_SYS.SEGMENT_ID GRPID, V_SYS.SEGMENT_NAME GRPNM, ");
    sbQuery.append("V_SYS.SYSTEM_ID GRPID2, V_SYS.SYSTEM_NM GRPNM2, ");
    sbQuery.append(" GET_ACCOUNT_PARTYID(T6640.C704_ACCOUNT_ID) GRPID3 ");
    sbQuery.append(" FROM T6640_NPI_TRANSACTION T6640, T501_ORDER T501, T502_ITEM_ORDER T502, ");
    sbQuery.append(" T208_SET_DETAILS T208, v_segment_system_map V_SYS ");
    sbQuery.append(" WHERE T6640.C101_PARTY_SURGEON_ID = '" + strPartyId + "' AND T6640.C6640_VALID_FL = 'Y' ");
    sbQuery.append(" AND T6640.C6640_REF_ID = T501.C501_ORDER_ID ");
    sbQuery.append(" AND T501.C501_ORDER_ID = T502.C501_ORDER_ID ");
    sbQuery.append(" AND V_SYS.SYSTEM_ID = T208.C207_SET_ID ");
    sbQuery.append(" AND T502.C205_PART_NUMBER_ID = T208.C205_PART_NUMBER_ID ");
    sbQuery.append(" AND T6640.C6640_VOID_FL IS NULL ");
    sbQuery.append(" AND T501.C501_VOID_FL IS NULL ");
    sbQuery.append(" AND T502.C502_VOID_FL IS NULL ");
    sbQuery.append(" AND T208.C208_VOID_FL IS NULL ");
    sbQuery.append(" AND T502.C502_ITEM_PRICE > 0 ");
    sbQuery.append(" AND T502.C502_ITEM_QTY > 0 ");
    sbQuery.append(" AND T501.C501_DELETE_FL IS NULL ");
    sbQuery.append(" AND NVL (C901_ORDER_TYPE, -9999) NOT IN (SELECT C906_RULE_VALUE FROM V901_ORDER_TYPE_GRP ) ");
    sbQuery.append(" AND T501.C501_ORDER_DATE >= TO_DATE('" + strFromDate + "','YYYYMM') ");
    sbQuery.append(" AND T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('" + strToDate + "','YYYYMM')) ");    
    if (!strCondition.equals(""))
            sbQuery.append(" AND " + strCondition + " ");
    sbQuery.append(" ) GROUP BY YEARMON, GRPID, GRPNM, GRPID2, GRPNM2, GRPID3 ");
    log.debug("fetchSurigcalActivityByRevenue  Query  " + sbQuery.toString());
    alResult = GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
    gmDBManager.close();
    log.debug("alResult>>> "+alResult.size());
    return alResult;

  }

  /**
   * This method used to fetch the surgical activity by procedure.
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */
  public ArrayList fetchSurigcalActivityByProc(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    String strPartyId = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
    String strFromDate = GmCommonClass.parseNull((String) hmParams.get("FROMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParams.get("TODATE"));
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
    log.debug("strCondition>>> "+strCondition+"strPartyId   >> "+strPartyId+strFromDate+strToDate);
    StringBuffer sbQuery = new StringBuffer();
    
    sbQuery.append(" SELECT ORDDATE YEARMON, COUNT(1) VALUE, SEGID GRPID, SEGNM GRPNM, ");
    sbQuery.append(" SYSID GRPID2, SYSNM GRPNM2, ACCTID GRPID3 FROM ");
    sbQuery.append(" (SELECT T6640.C6600_SURGEON_NPI_ID, ");
    sbQuery.append(" TO_CHAR(T501.C501_ORDER_DATE,'YYYYMM') ORDDATE,");
    sbQuery.append(" T501.C501_ORDER_ID ORDID, V_SYS.SEGMENT_ID SEGID, ");
    sbQuery.append(" V_SYS.SEGMENT_NAME SEGNM, V_SYS.SYSTEM_ID SYSID, ");
    sbQuery.append(" V_SYS.SYSTEM_NM SYSNM, GET_ACCOUNT_PARTYID(T6640.C704_ACCOUNT_ID) ACCTID ");
    sbQuery.append(" FROM T6640_NPI_TRANSACTION T6640, T501_ORDER T501, ");
    sbQuery.append(" T501B_ORDER_BY_SYSTEM T501B, v_segment_system_map V_SYS ");
    sbQuery.append(" WHERE T6640.C101_PARTY_SURGEON_ID     = '" + strPartyId + "' ");
    sbQuery.append(" AND T6640.C6640_VALID_FL              = 'Y' ");
    sbQuery.append(" AND T6640.C6640_REF_ID                = T501.C501_ORDER_ID ");
    sbQuery.append(" AND T501.C501_ORDER_ID                = T501B.C501_ORDER_ID ");
    sbQuery.append(" AND T501B.C207_SYSTEM_ID              = V_SYS.SYSTEM_ID ");
    sbQuery.append(" AND T6640.C6640_VOID_FL              IS NULL ");
    sbQuery.append(" AND T501.C501_VOID_FL                IS NULL ");
    sbQuery.append(" AND T501.C501_DELETE_FL              IS NULL ");
    sbQuery.append(" AND NVL (C901_ORDER_TYPE, -9999) NOT IN (SELECT C906_RULE_VALUE FROM V901_ORDER_TYPE_GRP ) ");
    sbQuery.append(" AND T501B.C501B_VOID_FL              IS NULL ");
    sbQuery.append(" AND T501.C501_ORDER_DATE >= TO_DATE('" + strFromDate + "','YYYYMM') ");
    sbQuery.append(" AND T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('" + strToDate + "','YYYYMM')) ");
    if (!strCondition.equals(""))
        sbQuery.append(" AND " + strCondition + " ");
    sbQuery.append(" ) GROUP BY ORDDATE, SYSID, SYSNM, SEGID, SEGNM, ACCTID ");
    log.debug("fetchSurigcalActivityByProc  Query  " + sbQuery.toString());
    alResult = GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
    gmDBManager.close();
    return alResult;

  }

  /**
   * This method used to fetch the surgical access.
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */

  public String fetchSurigcalAccess(HashMap hmParams) throws AppError {
    HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
    initializeParameters(hmParam);
    ArrayList alReturn = new ArrayList();
    String strPartyId = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT t108.c101_from_party_id PARTYID ");
    sbQuery.append(" FROM t108_party_to_party_link t108,  ");
    sbQuery.append(" t703_sales_rep t703  ");
    sbQuery.append(getAccessFilterClauseWithRepID());
    sbQuery.append(" WHERE t108.c101_from_party_id ='" + strPartyId + "' ");
    sbQuery.append(" AND t108.c101_to_party_id = t703.c101_party_id ");
    sbQuery.append(" AND t108.c108_void_fl IS NULL  ");
    sbQuery.append(" AND t703.c703_void_fl IS NULL AND T703.C703_SALES_REP_ID  = V700.REP_ID ");
    log.debug("loadSurigcalAccess Query  " + sbQuery.toString());
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    return (alReturn.size() > 0) ? "Y" : "N";

  }

  /**
   * This method used to fetch the DO count.
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */

  public HashMap fetchSurgicalActivityDOCount(HashMap hmParams) throws AppError {
    HashMap hmResult = new HashMap();
    
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    String strPartyId = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
    String strFromDate = GmCommonClass.parseNull((String) hmParams.get("FROMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParams.get("TODATE"));
    String strAccountID = GmCommonClass.parseNull((String) hmParams.get("ACCOUNTID"));
    String strSegmentStr = GmCommonClass.parseNull((String) hmParams.get("SEGMENTSTR"));
    String strSystemStr = GmCommonClass.parseNull((String) hmParams.get("SYSTEMSTR"));
    HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    String strCondition  = GmCommonClass.parseNull((String) hmParam.get("Condition"));
    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT COUNT(ORD) DOCOUNT ");
    sbQuery.append(" FROM ( SELECT DISTINCT T501.C501_ORDER_ID ORD ");
    sbQuery.append(" FROM T6640_NPI_TRANSACTION T6640, ");
    sbQuery
        .append(" T501_ORDER T501, T502_ITEM_ORDER T502, T208_SET_DETAILS T208, v_segment_system_map V_SYS ");
    sbQuery.append(" WHERE T6640.C101_PARTY_SURGEON_ID     = '" + strPartyId + "' ");
    sbQuery.append(" AND T6640.C6640_VALID_FL              = 'Y' ");
    sbQuery.append(" AND T6640.C6640_REF_ID                = T501.C501_ORDER_ID ");
    sbQuery.append(" AND T501.C501_ORDER_ID                = T502.C501_ORDER_ID ");
    sbQuery.append(" AND V_SYS.SYSTEM_ID                   = T208.C207_SET_ID ");
    sbQuery.append(" AND T502.C205_PART_NUMBER_ID          = T208.C205_PART_NUMBER_ID ");
    sbQuery.append(" AND T502.C502_VOID_FL				   IS NULL ");
    sbQuery.append(" AND T208.C208_VOID_FL				   IS NULL ");
    sbQuery.append(" AND T501.C501_PARENT_ORDER_ID        IS NULL ");
    sbQuery.append(" AND T6640.C6640_VOID_FL              IS NULL ");
    sbQuery.append(" AND T501.C501_VOID_FL                IS NULL ");
    sbQuery.append(" AND T501.C501_DELETE_FL              IS NULL ");
    sbQuery.append(" AND NVL (C901_ORDER_TYPE, -9999) NOT IN (SELECT C906_RULE_VALUE FROM V901_ORDER_TYPE_GRP) ");
    sbQuery.append(" AND T501.C501_ORDER_DATE >= TO_DATE('" + strFromDate + "','YYYYMM') ");
    sbQuery.append(" AND T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('" + strToDate + "','YYYYMM')) ");
    if (!strAccountID.equals(""))
      sbQuery.append(" AND T501.C704_ACCOUNT_ID IN (SELECT C704_ACCOUNT_ID FROM T704_ACCOUNT T704 WHERE T704.C704_VOID_FL IS NULL AND T704.C101_PARTY_ID   = '" + strAccountID + "') "); //For Checking the Account ID From Parent Account ID
    if (!strSystemStr.equals(""))
      sbQuery.append(" AND V_SYS.SYSTEM_ID                        IN (" + strSystemStr + ") ");
    if (!strSegmentStr.equals(""))
      sbQuery.append(" AND V_SYS.SEGMENT_ID                       IN (" + strSegmentStr + ") ");
    if (!strCondition.equals(""))
        sbQuery.append(" AND " + strCondition + " ");
    sbQuery.append(")");
    log.debug("fetchSurgicalActivityDOCount  Query  " + sbQuery.toString());
    hmResult = GmCommonClass.parseNullHashMap(gmDBManager.querySingleRecord(sbQuery.toString()));
    gmDBManager.close();
    log.debug("fetchSurgicalActivityDOCount  hmResult  " + hmResult);
    return hmResult;

  }

  /**
   * This method used to fetch CRM Surgeon Consultant Report.
   */
  public ArrayList fetchSurgeonProjectInfo(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_surgeon.gm_fch_projectinfo", 2);
    gmDBManager.setString(1, (String) hmParams.get("PARTYID"));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));

    gmDBManager.close();
    return alResult;

  }

  /**
   * This method used to fetch CRM Surgeon Summary Report. Load VP, AD , Distributor and Sales rep�s
   * surgeon detail report by status based on login user sales hierarchy.
   */
  public ArrayList fetchSurgeonSummaryReports(HashMap hmParams) throws AppError {
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();
    HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    // initializeParameters(hmParam);
    String strLogAccsLvl = (String) hmParam.get("ACCS_LVL");
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    String strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("AccessFilter"));
    String strRefId = GmCommonClass.parseNull((String) hmParams.get("REF_ID"));
    String strOpt = GmCommonClass.parseNull((String) hmParams.get("STROPT")); // For AccessLvl

    if (strOpt.equals("MYREP"))
      sbQuery.append(" SELECT REP_ID, REP_NAME, ");
    else
      sbQuery.append(" SELECT " + strOpt + "_ID, " + strOpt + "_NAME, ");
    sbQuery.append(" COUNT(DECODE(C901_STATUS,400901, C101_FROM_PARTY_ID)) ACTIVE, ");
    sbQuery.append(" COUNT(DECODE(C901_STATUS,400902, C101_FROM_PARTY_ID)) INPROGRESS, ");
    sbQuery.append(" COUNT(DECODE(C901_STATUS,400903, C101_FROM_PARTY_ID)) OPEN, ");
    sbQuery.append(" COUNT(C101_FROM_PARTY_ID) TOTAL ");
    sbQuery.append(" FROM ");
    if (strOpt.equals("MYREP"))
      sbQuery.append(" (SELECT DISTINCT REP_ID, REP_NAME, ");
    else
      sbQuery.append(" (SELECT DISTINCT " + strOpt + "_ID, " + strOpt + "_NAME, ");
    sbQuery.append(" C101_FROM_PARTY_ID, T1081.C901_STATUS ");
    sbQuery
        .append(" FROM T108_PARTY_TO_PARTY_LINK T108, T1081_PARTY_STATUS T1081, V703_REP_MAPPING_DETAIL V700");
    sbQuery.append(" WHERE T108.C108_VOID_FL IS NULL ");
    sbQuery.append(" AND T1081.C1081_VOID_FL IS NULL ");
    sbQuery.append(" AND T108.C101_FROM_PARTY_ID = T1081.C101_PARTY_ID ");
    sbQuery.append(" AND T108.C101_TO_PARTY_ID = V700.REP_PARTYID ");
    if (strOpt.equals("AD") && !strRefId.equals(""))
      sbQuery.append(" AND V700.VP_ID = " + strRefId + " ");
    if (strOpt.equals("D") && !strRefId.equals(""))
      sbQuery.append(" AND V700.AD_ID = " + strRefId + " ");
    if (strOpt.equals("REP") && !strRefId.equals(""))
      if (strLogAccsLvl.equals("2"))
        sbQuery
            .append(" AND V700.D_ID IN ( SELECT T101S.C701_DISTRIBUTOR_ID FROM T101_USER T101S WHERE C101_USER_ID = "
                + strRefId + ") ");
      else
        sbQuery.append(" AND V700.D_ID = " + strRefId + " ");
    if (strOpt.equals("MYREP") && !strRefId.equals(""))
      sbQuery.append(" AND V700.REP_ID = " + strRefId + " ");
    sbQuery.append(" AND " + strAccessFilter + " ) ");
    if (strOpt.equals("MYREP"))
      sbQuery.append(" GROUP BY REP_ID, REP_NAME ");
    else
      sbQuery.append(" GROUP BY " + strOpt + "_ID, " + strOpt + "_NAME ");

    log.debug("fetchSurgeonSummaryReports Query =" + sbQuery);
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
    return alResult;

  }

  /**
   * This method used to fetch CRM Surgeon List on Clicking the Counts of Summary Report.
   */
  public ArrayList fetchSummarySurgeonFullReport(HashMap hmParams) throws AppError {
    ArrayList alReturn = new ArrayList();
    String strRefId = GmCommonClass.parseNull((String) hmParams.get("REF_ID"));
    String strAccesslvl = GmCommonClass.parseNull((String) hmParams.get("ACCESSLVL"));
    String strStatus = GmCommonClass.parseNull((String) hmParams.get("STATUS"));
    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();
    HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    String strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("AccessFilter"));

    sbQuery
        .append(" SELECT DISTINCT T101.C101_PARTY_ID PARTYID, T101.C101_FIRST_NM FIRSTNM, T101.C101_LAST_NM LASTNM, T101.C101_PARTY_NM PARTYNM, ");
    sbQuery
        .append(" T101.C101_MIDDLE_INITIAL MIDINITIAL, T106.STATE, T106.CITY ,T106.COUNTRY,  T6600.C6600_SURGEON_NPI_ID NPID, ");
    sbQuery
        .append(" GET_CODE_NAME(T6600.C6600_PARTY_SALUTE) SALUTENM, T1081.C1081_PARTY_STATUS_ID ");
    sbQuery.append(" FROM T101_PARTY T101 , T6600_PARTY_SURGEON T6600, ");
    sbQuery
        .append(" (SELECT C106_CITY CITY, NVL(T901.C902_CODE_NM_ALT,T901.C901_CODE_NM)STATE, NVL(T901A.C902_CODE_NM_ALT, T901A.C901_CODE_NM) COUNTRY,  C101_PARTY_ID ");
    sbQuery.append(" FROM T106_ADDRESS T106, T901_CODE_LOOKUP T901, T901_CODE_LOOKUP  T901A WHERE C106_VOID_FL    IS NULL ");
    sbQuery
        .append(" AND C106_PRIMARY_FL  = 'Y' AND C101_PARTY_ID IS NOT NULL  AND T106.C901_STATE  = T901.C901_CODE_ID(+) AND T106.C901_COUNTRY = T901A.C901_CODE_ID(+) ");
    sbQuery
        .append(" AND T901.C901_VOID_FL IS NULL ) T106, T1081_PARTY_STATUS T1081, V703_REP_MAPPING_DETAIL V700, T108_PARTY_TO_PARTY_LINK T108 ");
    sbQuery
        .append(" WHERE T101.C901_PARTY_TYPE = '7000' AND T101.C101_PARTY_ID  = T1081.C101_PARTY_ID ");
    sbQuery
        .append(" AND T1081.C1081_VOID_FL  IS NULL AND T108.C108_VOID_FL IS NULL AND T101.C101_PARTY_ID  = T108.C101_FROM_PARTY_ID (+) ");
    sbQuery
        .append(" AND T108.C101_TO_PARTY_ID  = V700.REP_PARTYID AND T101.C101_ACTIVE_FL   IS NULL ");
    sbQuery
        .append(" AND T101.C101_VOID_FL     IS NULL  AND T101.C101_PARTY_ID     = T6600.C101_PARTY_SURGEON_ID(+) ");
    sbQuery.append(" AND T6600.C6600_VOID_FL   IS NULL ");
    if (!strStatus.equals("total"))
      sbQuery.append(" AND T1081.C901_STATUS =" + strStatus + " ");
    if (!strAccesslvl.equals(""))
      if (strAccesslvl.equals("MYREP"))
        sbQuery.append(" AND V700.REP_ID IN (" + strRefId + ") ");
      else
        sbQuery.append(" AND V700." + strAccesslvl + "_ID IN (" + strRefId + ") ");
    sbQuery.append(" AND " + strAccessFilter + " ");
    sbQuery
        .append(" AND T101.C101_PARTY_ID = T106.C101_PARTY_ID(+) ORDER BY UPPER(FIRSTNM ||' '|| LASTNM) ");
    log.debug("fetchSummarySurgeonFullReport SbQUERY>>>>>" + sbQuery);
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn.size());

    return alReturn;

  }


  /**
   * This method used to fetch Month Value and Year Month Fields for a given dates.
   */
  public ArrayList fetchSurgeonRevenueMonths(HashMap hmParams) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();
    String strfromdt = GmCommonClass.parseNull((String) hmParams.get("FROMDT"));
    String strtodt = GmCommonClass.parseNull((String) hmParams.get("TODT"));
    sbQuery
        .append(" SELECT TO_CHAR(MONTH_VALUE,'MON YY') MONTHVALUE, TO_CHAR(MONTH_VALUE, 'YYYYMM') YRMONTH ");
    sbQuery.append(" FROM V9001_MONTH_LIST ");
    sbQuery.append(" WHERE MONTH_VALUE >= TO_DATE(" + strfromdt + ",'YYYYMM') ");
    sbQuery.append(" AND MONTH_VALUE <= LAST_DAY(TO_DATE(" + strtodt + ",'YYYYMM')) ");

    log.debug("fetchSurgeonRevenueMonths SbQUERY>>>>>" + sbQuery);
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
    log.debug("alReturnBean>>>>>" + alReturn.size());

    return alReturn;

  }

  /**
   * This method used to fetch CRM Surgeon Revenue List on Clicking the Surgeon Icon in Summary
   * Report.
   */
  public ArrayList fetchSurgeonRevenueReports(HashMap hmParams) throws AppError {
    ArrayList alReturn = new ArrayList();
    String strRefId = GmCommonClass.parseNull((String) hmParams.get("REFID"));
    String strAccessType = GmCommonClass.parseNull((String) hmParams.get("ACCESSTYPE"));
    String strfromdt = GmCommonClass.parseNull((String) hmParams.get("FROMDT"));
    String strtodt = GmCommonClass.parseNull((String) hmParams.get("TODT"));
    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();
    HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    String strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("AccessFilter"));

    String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
    sbQuery
        .append(" SELECT T101.C101_PARTY_ID PARTYID,  T101.C101_FIRST_NM || ' ' || T101.C101_LAST_NM PARTYNM, REV.YRMONTH, REV.VALUE ");
    sbQuery.append(" FROM T101_PARTY T101, (SELECT DISTINCT T108.C101_FROM_PARTY_ID SURGEONID  ");
    sbQuery.append(" FROM V703_REP_MAPPING_DETAIL V700,  T108_PARTY_TO_PARTY_LINK T108 ");
    // -- WHERE REP_ID =480
    sbQuery
        .append(" WHERE V700.REP_PARTYID = T108.C101_TO_PARTY_ID  AND T108.C108_VOID_FL IS NULL ");
    sbQuery.append(" AND " + strAccessFilter + " ");
    if (!strAccessType.equals(""))
      if (strAccessType.equals("MYREP"))
        sbQuery.append(" AND REP_ID IN (" + strRefId + ") ");
      else
        sbQuery.append(" AND " + strAccessType + "_ID IN (" + strRefId + ") ");
    else
      sbQuery.append(" AND REP_ID IN (" + strRefId + ") ");
    sbQuery.append("  ) V700, ");
    sbQuery.append(" (SELECT TO_CHAR(T501.C501_ORDER_DATE ,'YYYYMM')YRMONTH, ");
    sbQuery
        .append(" SUM(T502.C502_ITEM_PRICE * T502.C502_ITEM_QTY) VALUE, T6640.C101_PARTY_SURGEON_ID PARTYID ");
    sbQuery.append(" FROM T6640_NPI_TRANSACTION T6640,  T501_ORDER T501,  T502_ITEM_ORDER T502 ");
    sbQuery
        .append(" WHERE T6640.C6640_VALID_FL  = 'Y' AND T6640.C6640_REF_ID   = T501.C501_ORDER_ID AND T501.C501_ORDER_ID  = T502.C501_ORDER_ID ");
    sbQuery
        .append(" AND T6640.C6640_VOID_FL IS NULL AND T501.C501_VOID_FL IS NULL ");
    sbQuery
        .append(" AND T502.C502_VOID_FL IS NULL  AND T502.C502_ITEM_PRICE > 0 AND T502.C502_ITEM_QTY  > 0 ");
    sbQuery
        .append(" AND T501.C501_DELETE_FL IS NULL AND NVL (C901_ORDER_TYPE, -9999) NOT IN (SELECT C906_RULE_VALUE FROM V901_ORDER_TYPE_GRP) ");
    sbQuery.append(" AND T501.C501_ORDER_DATE >= TO_DATE(" + strfromdt
        + ",'YYYYMM') AND T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE(" + strtodt + ",'YYYYMM')) ");
    if (!strCondition.equals("")){ // Check with sales filter access
    sbQuery.append(" AND " + strCondition + " ");
    }
    sbQuery
        .append(" GROUP BY TO_CHAR(T501.C501_ORDER_DATE ,'YYYYMM'), T6640.C101_PARTY_SURGEON_ID ) REV ");
    sbQuery
        .append(" WHERE T101.C101_PARTY_ID = REV.PARTYID AND T101.C101_PARTY_ID   = V700.SURGEONID ");
    sbQuery.append(" AND T101.C101_VOID_FL   IS NULL ORDER BY PARTYNM, YRMONTH ");
    log.debug("fetchSummarySurgeonFullReport SbQUERY>>>>>" + sbQuery);
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn.size());

    return alReturn;

  }
  
  /**
   * @param hmParams
   * @return ArrayList alReturn
   * @throws AppError
   * This method used to fetch CRM Surgeon System DO List Report.
   * For Surgical Activity Module on clicking the System Data Plot in Chart, the Fn Retrieves the DO List Report for the System
   * Inputs are token, partyid, systemid, fromdt, todt
   */
  public ArrayList fetchSurgeonDOLists(HashMap hmParams) throws AppError {
      ArrayList alReturn = new ArrayList();
      GmAccessFilter gmAccessFilter = new GmAccessFilter();
      String strPartyId = GmCommonClass.parseNull((String) hmParams
              .get("PARTYID"));
      String strSystemId = GmCommonClass.parseNull((String) hmParams
              .get("SYSTEMID"));
      String strfromdt = GmCommonClass.parseNull((String) hmParams
              .get("FROMDT"));
      String strtodt = GmCommonClass.parseNull((String) hmParams.get("TODT"));
      String strAccountID = GmCommonClass.parseNull((String) hmParams.get("ACCTID"));
      GmDBManager gmDBManager = new GmDBManager();
      StringBuffer sbQuery = new StringBuffer();
      HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
      hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
      String strCondition  = GmCommonClass.parseNull((String) hmParam.get("Condition"));
      
      sbQuery.append(" SELECT T205.C205_PART_NUM_DESC PDESC, T.*, (T.QUANTITY * T.PRICE) TOTAL ");
      sbQuery.append(" FROM (SELECT T501.C501_ORDER_ID ORDERID, ");
      sbQuery.append(" TO_CHAR(T501.C501_ORDER_DATE,'MM/DD/YYYY') ORDERDT, ");
      sbQuery.append(" T501.C501_ORDER_DATE, ");
      sbQuery.append(" T501.C704_ACCOUNT_ID ACCTID, ");
      sbQuery.append(" T501.C703_SALES_REP_ID REPID, ");
      sbQuery.append(" T502.C205_PART_NUMBER_ID PARTNUM, ");
      sbQuery.append(" SUM(T502.C502_ITEM_QTY) QUANTITY, ");
      sbQuery.append(" T502.C502_ITEM_PRICE PRICE ");
      sbQuery.append(" FROM T6640_NPI_TRANSACTION T6640, T501_ORDER T501, T502_ITEM_ORDER T502, T208_SET_DETAILS T208 ");
      sbQuery.append(" WHERE T6640.C101_PARTY_SURGEON_ID     = "+ strPartyId + " ");
      sbQuery.append(" AND T6640.C6640_VALID_FL              = 'Y' ");
      sbQuery.append(" AND T6640.C6640_REF_ID                = T501.C501_ORDER_ID ");
      sbQuery.append(" AND T501.C501_ORDER_ID                = T502.C501_ORDER_ID ");
      sbQuery.append(" AND T502.C205_PART_NUMBER_ID          = T208.C205_PART_NUMBER_ID ");
      sbQuery.append(" AND T502.C502_VOID_FL                IS NULL ");
      sbQuery.append(" AND T208.C208_VOID_FL                IS NULL ");
      sbQuery.append(" AND T6640.C6640_VOID_FL              IS NULL ");
      sbQuery.append(" AND T501.C501_VOID_FL                IS NULL ");
      sbQuery.append(" AND T501.C501_DELETE_FL              IS NULL ");
      sbQuery.append(" AND NVL (C901_ORDER_TYPE, -9999) NOT IN ");
      sbQuery.append(" (SELECT C906_RULE_VALUE FROM V901_ORDER_TYPE_GRP ) ");
      sbQuery.append(" AND T501.C501_ORDER_DATE >= TO_DATE('" + strfromdt + "','YYYYMM') ");
      sbQuery.append(" AND T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('" + strtodt + "','YYYYMM')) ");
      sbQuery.append(" AND T208.C207_SET_ID     IN ('" + strSystemId + "') ");
      if (!strAccountID.equals(""))
      sbQuery.append(" AND T501.C704_ACCOUNT_ID IN (SELECT C704_ACCOUNT_ID FROM T704_ACCOUNT T704 WHERE T704.C704_VOID_FL IS NULL AND T704.C101_PARTY_ID   = '" + strAccountID + "') ");      
      if (!strCondition.equals(""))
          sbQuery.append(" AND " + strCondition + " ");
      sbQuery.append(" GROUP BY T501.C501_ORDER_ID, T501.C501_ORDER_DATE, T501.C704_ACCOUNT_ID,  T501.C703_SALES_REP_ID, ");
      sbQuery.append(" T502.C205_PART_NUMBER_ID, T502.C502_ITEM_PRICE ) T, T205_PART_NUMBER T205 ");
      sbQuery.append(" WHERE T.PARTNUM = T205.C205_PART_NUMBER_ID ");
      sbQuery.append(" ORDER BY C501_ORDER_DATE, ORDERID, PDESC, QUANTITY ");
      
      log.debug("fetchSurgeonDOLists SbQUERY>>>>>" + sbQuery);
      alReturn = GmCommonClass.parseNullArrayList(gmDBManager
              .queryMultipleRecords(sbQuery.toString()));

      log.debug("alReturnBean>>>>>" + alReturn.size());

      return alReturn;

  }

}
