package com.globus.webservice.crm.bean;

import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmCRMPreceptorshipTxnBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmCRMPreceptorshipTxnBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  /**
   * This method used to Create Case Request in Preceptorship module. Save the data from updated
   * model object in front-end as a JSONDATA.
   * 
   * @param hmParams
   * @return String strCaseRequestId
   * @throws AppError
   */
  public String saveCaseInfoJSON(HashMap<String, String> hmParams) throws AppError {
	HashMap hmJmsParam = new HashMap();
	GmCRMActivityBean gmCRMActivityBean = new GmCRMActivityBean();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strJSONInput = GmCommonClass.parseNull(hmParams.get("JSONDATA"));
    String strUserID = GmCommonClass.parseNull(hmParams.get("USERID"));
    String strCaseRequestId = "";
    gmDBManager.setPrepareString("gm_pkg_crm_preceptorship_txn.gm_sav_crm_case_json", 3);
    gmDBManager.setString(1, strJSONInput);
    gmDBManager.setString(2, strUserID);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strCaseRequestId = GmCommonClass.parseNull(gmDBManager.getString(3));
    gmDBManager.commit();
    
    
    // Calling JMS to Sync Party Activity when Case request cancelled
    hmJmsParam.put("USERID", hmParams.get("USERID"));
    hmJmsParam.put("STROPT", "CASEREQUEST");
    gmCRMActivityBean.syncPartyActivityJMS(hmJmsParam);

    return strCaseRequestId;
  }

  /**
   * This method used to link/unlink Case Request in to host case. Save the data from updated model
   * object in front-end as a JSONDATA.
   * 
   * @param hmParams
   * @return void
   * @throws AppError
   */
  public void saveCaseRequestMappingJSON(HashMap<String, String> hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCRMActivityBean gmCRMActivityBean = new GmCRMActivityBean();
    HashMap hmJmsParam = new HashMap();
    String strJSONInput = GmCommonClass.parseNull(hmParams.get("JSONDATA"));
    String strUserID = GmCommonClass.parseNull(hmParams.get("USERID"));
    gmDBManager.setPrepareString("gm_pkg_crm_preceptorship_txn.gm_sav_case_req_map_json", 2);
    gmDBManager.setString(1, strJSONInput);
    gmDBManager.setString(2, strUserID);
    gmDBManager.execute();
    gmDBManager.commit();

    // Calling JMS to Sync Party Activity
    hmJmsParam.put("USERID", hmParams.get("USERID"));
    hmJmsParam.put("STROPT", "CASEREQUEST");
    gmCRMActivityBean.syncPartyActivityJMS(hmJmsParam);

  }


  /**
   * This method used to send Case Request creation notification email
   * 
   * @param strCaseReqId
   * @throws AppError
   */
  public void sendCaseRequestEmail(HashMap<String, String> hmParm) throws AppError {
    HashMap hmEmailDetails = new HashMap();
    HashMap hmTemplate = new HashMap();
    ArrayList alCaseReqAsso = new ArrayList();
    GmCRMPreceptorshipRptBean gmCRMPreceptorshipRptBean = new GmCRMPreceptorshipRptBean();

    String strCaseReqId = hmParm.get("CASEREQID");
    String strVMFile = hmParm.get("VMFILE");
    String strEmailTempProperty = hmParm.get("EMAILPROPERTY");
    String strSubject =
        GmCommonClass.getEmailProperty(strEmailTempProperty + "." + GmEmailProperties.SUBJECT);


    log.debug("sendCaseRequestEmail strCaseReqId>>>>>>>>>" + strCaseReqId);
    hmEmailDetails = gmCRMPreceptorshipRptBean.fetchCaseReqEmailDtl(strCaseReqId);
    hmEmailDetails.put("CRMFILEPATH", GmCommonClass.getString("URL_CRM_FILE_SERVER"));
    hmEmailDetails.put("CRMPORTALPATH", GmCommonClass.getString("URL_CRM_PORTAL_SERVER"));
    hmEmailDetails
        .put(
            "EMAILPRO_MSG_BODY",
            GmCommonClass.getEmailProperty(strEmailTempProperty + "."
                + GmEmailProperties.MESSAGE_BODY));
    hmTemplate.put("DATA_MAP", hmEmailDetails);
    hmTemplate.put("TEMPLATE_NAME", strVMFile);
    hmTemplate.put("TEMPLATE_SUB_DIR", "prodmgmnt/templates");

    String strMessageBody = GmCommonClass.getXmlGridData(hmTemplate);

    alCaseReqAsso = (ArrayList) hmEmailDetails.get("ALCASEREQASSO");
    String[] strTo = new String[alCaseReqAsso.size()];
    String[] strCC =
        GmCommonClass.StringtoArray(
            GmCommonClass.getEmailProperty(strEmailTempProperty + "." + GmEmailProperties.CC), ",");

    for (int i = 0; i < alCaseReqAsso.size(); i++) {
      HashMap hmParticipant = new HashMap();
      hmParticipant = GmCommonClass.parseNullHashMap((HashMap) alCaseReqAsso.get(i));
      strTo[i] = GmCommonClass.parseNull((String) hmParticipant.get("EMAILID"));
    }

    strSubject = strSubject.replace("[CR-ID]", strCaseReqId);

    HashMap hmEmailProperty = new HashMap();
    hmEmailProperty.put("strFrom",
        GmCommonClass.getEmailProperty(strEmailTempProperty + "." + GmEmailProperties.FROM));
    hmEmailProperty.put("strTo", strTo);
    hmEmailProperty.put("strCc", strCC);
    hmEmailProperty.put("strSubject", strSubject);
    hmEmailProperty.put("strMessageDesc", strMessageBody);
    hmEmailProperty.put("strMimeType",
        GmCommonClass.getEmailProperty(strEmailTempProperty + "." + GmEmailProperties.MIME_TYPE));
    hmEmailProperty.put(
        "strEmailHeader",
        GmCommonClass.getEmailProperty(strEmailTempProperty + "."
            + GmEmailProperties.EMAIL_HEADER_NM));

    log.debug("sendCaseRequestEmail hmEmailProperty >>> " + hmEmailProperty);

    GmCommonClass.sendEmail(hmEmailProperty);
  }

}
