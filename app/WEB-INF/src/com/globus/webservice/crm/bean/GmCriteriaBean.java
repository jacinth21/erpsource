package com.globus.webservice.crm.bean;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.webservice.crm.valueobject.GmCriteriaAttrVO;

public class GmCriteriaBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public HashMap saveCriteria(HashMap hmParams, List<GmCriteriaAttrVO> arrGmCriteriaAttrVO,
      String strUserId) throws AppError {

    String strCriteriaId = GmCommonClass.parseNull((String) hmParams.get("CRITERIAID"));

    GmDBManager gmDBManager = new GmDBManager();

    gmDBManager.setPrepareString("gm_pkg_crm_criteria.gm_sav_crm_criteria", 5);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strCriteriaId);
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("CRITERIANM")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParams.get("CRITERIATYP")));
    gmDBManager.setString(4, strUserId);
    gmDBManager.setString(5, GmCommonClass.parseNull((String) hmParams.get("VOIDFL")));

    gmDBManager.execute();
    strCriteriaId = GmCommonClass.parseNull(gmDBManager.getString(1));
    log.debug(" Log One ----------------->  ");
    saveCriteriaAttr(gmDBManager, arrGmCriteriaAttrVO, strCriteriaId);

    gmDBManager.commit();
    hmParams.put("CRITERIAID", strCriteriaId);
    return hmParams;
  }

  public void saveCriteriaAttr(GmDBManager gmDBManager, List<GmCriteriaAttrVO> arrGmCriteriaAttrVO,
      String strCriteriaId) throws AppError {
    for (int i = 0; i < arrGmCriteriaAttrVO.size(); i++) {
    	
      log.debug("arrGmCriteriaAttrVO.get(i) >>> " + arrGmCriteriaAttrVO.get(i));
      gmDBManager.setPrepareString("gm_pkg_crm_criteria.gm_sav_crm_criteria_attr", 5);
      gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
      gmDBManager.setString(1, GmCommonClass.parseNull(arrGmCriteriaAttrVO.get(i).getAttrid()));
      gmDBManager.setString(2, GmCommonClass.parseNull(arrGmCriteriaAttrVO.get(i).getKey()));
      gmDBManager.setString(3, GmCommonClass.parseNull(arrGmCriteriaAttrVO.get(i).getValue()));
      gmDBManager.setString(4, GmCommonClass.parseNull(arrGmCriteriaAttrVO.get(i).getVoidfl()));
      gmDBManager.setString(5, strCriteriaId);
      gmDBManager.execute();
      
    }
  }
  
  public ArrayList fetchCriteriaSync(HashMap hmParams) throws AppError {
	 GmDBManager gmDBManager = new GmDBManager();
	 ArrayList alResult = new ArrayList();
	 gmDBManager.setPrepareString("gm_pkg_crm_update.gm_fch_all_criteria_updt", 5);
	 gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
	 gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
	 gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
	 gmDBManager.setString(4, (String) hmParams.get("UUID"));
	 gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
	 gmDBManager.execute();
	 alResult =
	     GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
	         .getObject(5)));
	 gmDBManager.close();
	 return alResult;
  }

  public ArrayList fetchCriteriaAttrSync(HashMap hmParams) throws AppError {
	 GmDBManager gmDBManager = new GmDBManager();
	 ArrayList alResult = new ArrayList();
	 gmDBManager.setPrepareString("gm_pkg_crm_update.gm_fch_all_critattr_updt", 5);
	 gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
	 gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
	 gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
	 gmDBManager.setString(4, (String) hmParams.get("UUID"));
	 gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
	 gmDBManager.execute();
	 alResult =
	     GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
	         .getObject(5)));
	 gmDBManager.close();
	 return alResult;
  }
  
  public ArrayList fetchCriteria(String strUserId, String ModuleId) {
    // TODO Auto-generated method stub

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT T1101.C1101_CRITERIA_ID CRITERIAID, T1101.C1101_CRITERIA_NAME CRITERIANM, T1101.C2201_MODULE_ID CRITERIATYP, ");
    sbQuery
        .append(" T101.C101_USER_F_NAME || T101.C101_USER_L_NAME USERNM FROM T1101_CRITERIA T1101, T101_USER T101 ");
    sbQuery
        .append(" WHERE T1101.C1101_ACTIVE_FL IS NULL AND T101.C101_USER_ID = T1101.C101_USER_ID AND T1101.C1101_VOID_FL IS NULL ");
    sbQuery.append(" AND T1101.C101_USER_ID = '" + strUserId + "' ");
    if (!ModuleId.equals(""))
      sbQuery.append(" AND T1101.C2201_MODULE_ID = '" + ModuleId + "' ");

    log.debug("Query=" + sbQuery.toString());


    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;
  }

  public ArrayList fetchCriteriaBy(String strCriteriaId, String strModule) {
    // TODO Auto-generated method stub

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT T1101.C1101_CRITERIA_ID CRITERIAID, T1101.C1101_CRITERIA_NAME CRITERIANM, ");
    sbQuery.append(" T1101A.C1101A_CRITERIA_KEY KEY, T1101A.C1101A_CRITERIA_VALUE VALUE  ");
    sbQuery
        .append(" FROM T1101_CRITERIA T1101 JOIN T1101A_CRITERIA_ATTRIBUTE T1101A ON T1101.C1101_CRITERIA_ID = T1101A.C1101_CRITERIA_ID ");
    sbQuery
        .append(" WHERE T1101.C1101_VOID_FL IS NULL AND T1101.C1101_ACTIVE_FL IS NULL AND T1101A.C1101A_VOID_FL IS NULL ");
    sbQuery.append(" AND T1101.C1101_CRITERIA_ID = '" + strCriteriaId
        + "' AND LOWER(T1101.C2201_MODULE_ID) = '" + strModule.toLowerCase().trim() + "' ");


    log.debug("The Query is >>>>>>" + sbQuery.toString());
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("The result is alReturnBean>>>>>" + alReturn);

    return alReturn;
  }

  public HashMap fetchCriteriaInfo(String strCriteriaID) {
    // TODO Auto-generated method stub
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT C1101_CRITERIA_ID CRITERIAID, C1101_CRITERIA_NAME CRITERIANM, C2201_MODULE_ID CRITERIATYP FROM T1101_CRITERIA ");
    sbQuery.append(" WHERE C1101_VOID_FL IS NULL AND C1101_CRITERIA_ID = '" + strCriteriaID + "' ");


    log.debug("The Query is >>>>>>" + sbQuery.toString());
    hmReturn = GmCommonClass.parseNullHashMap(gmDBManager.querySingleRecord(sbQuery.toString()));

    log.debug("The result is alReturnBean>>>>>" + hmReturn);

    return hmReturn;
  }

  public ArrayList fetchCriteriaAttr(String strCriteriaID) {
    // TODO Auto-generated method stub
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT C1101_CRITERIA_ID CRITERIAID, C1101A_CRITERIA_KEY KEY, C1101A_CRITERIA_VALUE VALUE, C1101A_CRITERIA_ATTRIBUTE_ID ATTRID, C1101A_VOID_FL VOIDFL FROM T1101A_CRITERIA_ATTRIBUTE ");
    sbQuery
        .append(" WHERE C1101_CRITERIA_ID = '" + strCriteriaID + "' AND C1101A_VOID_FL IS NULL ");


    log.debug("The Query is >>>>>>" + sbQuery.toString());
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("The result is alReturnBean>>>>>" + alReturn);

    return alReturn;
  }
}
