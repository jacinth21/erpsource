package com.globus.webservice.crm.bean;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmWSUtil;
import com.globus.webservice.crm.valueobject.GmCRFApprovalVO;
import com.globus.webservice.crm.valueobject.GmCRMCRFVO;
import com.globus.webservice.crm.valueobject.GmCRMLogVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonlistVO;

public class GmCRMCRFBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public void fetchCRFReport(HashMap hmParams, GmCRMCRFVO gmCRMCRFVO) throws AppError {

    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();

    HashMap hmOutput = new HashMap();
    HashMap hmSurgeonResult = new HashMap();
    ArrayList alLogDtls = new ArrayList();
    ArrayList alApprovals = new ArrayList();
    GmCRMSurgeonlistVO gmCRMSurgeonlistVO = new GmCRMSurgeonlistVO();
    List<GmCRMLogVO> listGmCRMLogVO = new ArrayList<GmCRMLogVO>();
    GmCRMLogVO gmCRMLogVO = new GmCRMLogVO();
    List<GmCRFApprovalVO> listGmCRFApprovalVO = new ArrayList<GmCRFApprovalVO>();
    GmCRFApprovalVO gmCRFApprovalVO = new GmCRFApprovalVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    String strPartyid = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
    String strActid = GmCommonClass.parseNull((String) hmParams.get("ACTID"));
    String strCrfid = GmCommonClass.parseNull((String) hmParams.get("CRFID"));

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT C6605_CRF_ID CRFID, C101_PARTY_ID PARTYID, C6630_ACTIVITY_ID ACTID, ");
    sbQuery.append(" C901_REQUEST_TYPE REQTYPEID, GET_CODE_NAME(C901_REQUEST_TYPE) REQTYPENM, ");
    sbQuery.append(" C901_STATUS STATUSID, GET_CODE_NAME(C901_STATUS) STATUSNM, ");
    sbQuery.append(" C6605_SERVICE_TYPE SERVICETYP, C6605_REASON REASON, ");
    sbQuery
        .append(" C6605_SERVICE_DESCRIPTION SERVICEDESC, C6605_HCP_QUALIFICATION HCPQUALIFICATION, ");
    sbQuery.append(" C6605_REQUEST_DATE REQDATE , ");
    sbQuery.append(" C6605_PREP_TIME PREPTIME, ");
    sbQuery.append(" C6605_WORK_HOURS WORKHRS, C6605_TIME_FL TIMEFL, ");
    sbQuery.append(" C6605_AGREEMENT_FL AGRFL, C6605_TRAVEL_FROM TRVLFROM, ");
    sbQuery.append(" C6605_TRAVEL_TO TRVLTO, C6605_TRAVEL_TIME TRVLTIME, ");
    sbQuery.append(" C6605_TRAVEL_MILES TRVLMILE, C6605_HOUR_RATE HRRATE, ");
    sbQuery.append(" C6605_HONORARIA_STATUS HONSTATUS, C6605_HONORARIA_WORK_HRS HONWORKHRS, ");
    sbQuery
        .append(" C6605_HONORARIA_PREP_HRS HONPREPHRS, C6605_HONORARIA_COMMENT HONCOMMENT, C6605_PRIORITY_FL PRIORITYFL, GET_USER_NAME(C6605_CREATED_BY) CREATEDBY, GET_USER_NAME(C6605_LAST_UPDATED_BY) UPDATEDBY, NVL(C6605_LAST_UPDATED_DATE, '') UPDATEDON ");
    sbQuery.append(" FROM T6605_CRF WHERE C6605_VOID_FL IS NULL ");
    if (!strPartyid.equals(""))
      sbQuery.append(" AND C101_PARTY_ID = '" + strPartyid + "' ");
    if (!strActid.equals(""))
      sbQuery.append(" AND C6630_ACTIVITY_ID = '" + strActid + "' ");
    if (!strCrfid.equals(""))
      sbQuery.append(" AND C6605_CRF_ID = '" + strCrfid + "' ");


    log.debug("fetchCRFReport Query=" + sbQuery.toString());

    hmOutput = GmCommonClass.parseNullHashMap((gmDBManager.querySingleRecord(sbQuery.toString())));
    strCrfid = GmCommonClass.parseNull((String) hmOutput.get("CRFID"));


    if (strCrfid.equals("")) {
      sbQuery = new StringBuffer();
      sbQuery.append(" SELECT C101_PARTY_ID PARTYID,  ");
      sbQuery.append(" C6605_HCP_QUALIFICATION HCPQUALIFICATION ");
      sbQuery.append(" FROM T6605_CRF WHERE C6605_VOID_FL IS NULL ");
      if (!strPartyid.equals(""))
        sbQuery.append(" AND C101_PARTY_ID = '" + strPartyid + "' ");

      log.debug("fetch Exist CRFReport Query=" + sbQuery.toString());

      hmOutput =
          GmCommonClass.parseNullHashMap((gmDBManager.querySingleRecord(sbQuery.toString())));

    } else {
      alLogDtls = fetchLogReport(strCrfid);
      listGmCRMLogVO =
          gmWSUtil.getVOListFromHashMapList(alLogDtls, gmCRMLogVO,
              gmCRMLogVO.getMappingProperties());

      alApprovals = fetchApprovalInfo(strCrfid);
      listGmCRFApprovalVO =
          gmWSUtil.getVOListFromHashMapList(alApprovals, gmCRFApprovalVO,
              gmCRFApprovalVO.getMappingProperties());

    }

    log.debug("hmOutput=" + hmOutput);

    gmWSUtil.getVOFromHashMap(hmOutput, gmCRMCRFVO);

    hmSurgeonResult = gmCRMSurgeonBean.fetchSurgeonReport(hmParams);
    gmCRMSurgeonlistVO = gmCRMSurgeonBean.populateSurgeonListVO(hmSurgeonResult);
    gmCRMCRFVO.setGmCRMSurgeonlistVO(gmCRMSurgeonlistVO);
    gmCRMCRFVO.setArrgmcrmlogvo(listGmCRMLogVO);
    gmCRMCRFVO.setArrgmcrfapprovalvo(listGmCRFApprovalVO);

  }

  public ArrayList fetchApprovalInfo(String strCrfid) {
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT T6605A.C6605A_CRF_APPROVAL_ID CRFAPPRID, T6605A.C6605_CRF_ID CRFID, ");
    sbQuery
        .append(" T6605A.C101_PARTY_ID PARTYID, (T101.C101_FIRST_NM || ' ' || T101.C101_LAST_NM) PARTYNM, ");
    sbQuery.append(" T6605A.C901_STATUS STATUSID, GET_CODE_NAME(T6605A.C901_STATUS) STATUSNM, ");
    sbQuery
        .append(" T6605A.C6605A_COMMENTS COMMENTS, GET_USER_NAME(C6605A_LAST_UPDATED_BY) UPDATEDBY, C6605A_LAST_UPDATED_DATE UPDATEDON ");
    sbQuery.append(" FROM T6605A_CRF_APPROVAL T6605A, T101_PARTY T101 ");
    sbQuery.append(" WHERE T101.C101_PARTY_ID = T6605A.C101_PARTY_ID ");
    sbQuery.append(" AND T101.C101_VOID_FL IS NULL ");
    sbQuery.append(" AND T6605A.C6605_CRF_ID = '" + strCrfid + "' ");


    log.debug("fetchApprovalInfo Query=" + sbQuery.toString());
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("fetchApprovalInfo Query result >>>>>>>>>" + alReturn);
    return alReturn;
  }

  public String saveCRFInfo(HashMap hmParams, List<GmCRMLogVO> arrgmcrmlogvo, GmCRMLogVO gmCRMLogVO)
      throws Exception, AppError {

    GmDBManager gmDBManager = new GmDBManager();

    String strCrfId = new String();

    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));


    Date reqDate = GmCommonClass.getStringToDate((String) hmParams.get("REQDATE"), strApplDateFmt);

    gmDBManager.setPrepareString("gm_pkg_crm_crf.gm_crm_sav_crf", 26);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("CRFID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("PARTYID")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParams.get("ACTID")));
    gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParams.get("REQTYPEID")));
    gmDBManager.setDate(5, reqDate);
    gmDBManager.setString(6, GmCommonClass.parseNull((String) hmParams.get("STATUSID")));
    gmDBManager.setString(7, GmCommonClass.parseNull((String) hmParams.get("SERVICETYP")));
    gmDBManager.setString(8, GmCommonClass.parseNull((String) hmParams.get("REASON")));
    gmDBManager.setString(9, GmCommonClass.parseNull((String) hmParams.get("SERVICEDESC")));
    gmDBManager.setString(10, GmCommonClass.parseNull((String) hmParams.get("HCPQUALIFICATION")));
    gmDBManager.setString(11, GmCommonClass.parseNull((String) hmParams.get("PREPTIME")));
    gmDBManager.setString(12, GmCommonClass.parseNull((String) hmParams.get("WORKHRS")));
    gmDBManager.setString(13, GmCommonClass.parseNull((String) hmParams.get("TIMEFL")));
    gmDBManager.setString(14, GmCommonClass.parseNull((String) hmParams.get("AGRFL")));
    gmDBManager.setString(15, GmCommonClass.parseNull((String) hmParams.get("TRVLFROM")));
    gmDBManager.setString(16, GmCommonClass.parseNull((String) hmParams.get("TRVLTO")));
    gmDBManager.setString(17, GmCommonClass.parseNull((String) hmParams.get("TRVLTIME")));
    gmDBManager.setString(18, GmCommonClass.parseNull((String) hmParams.get("TRVLMILE")));
    gmDBManager.setString(19, GmCommonClass.parseNull((String) hmParams.get("HRRATE")));
    gmDBManager.setString(20, GmCommonClass.parseNull((String) hmParams.get("HONSTATUS")));
    gmDBManager.setString(21, GmCommonClass.parseNull((String) hmParams.get("HONWORKHRS")));
    gmDBManager.setString(22, GmCommonClass.parseNull((String) hmParams.get("HONPREPHRS")));
    gmDBManager.setString(23, GmCommonClass.parseNull((String) hmParams.get("HONCOMMENT")));
    gmDBManager.setString(24, GmCommonClass.parseNull((String) hmParams.get("VOIDFL")));
    gmDBManager.setString(25, GmCommonClass.parseNull((String) hmParams.get("PRIORITYFL")));
    gmDBManager.setString(26, GmCommonClass.parseNull((String) hmParams.get("USERID")));

    gmDBManager.execute();
    saveLogInfo(hmParams, gmDBManager, arrgmcrmlogvo, gmCRMLogVO);
    strCrfId = GmCommonClass.parseNull(gmDBManager.getString(1));

    gmDBManager.commit();
    return strCrfId;
  }

  public ArrayList getCRFStatus(HashMap hmParams) {


    String strPartyID =
        GmCommonClass
            .getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("PARTYID")));
    String strActID =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("ACTID")));
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT C101_PARTY_ID PARTYID, C6630_ACTIVITY_ID ACTID, GET_CODE_NAME(C901_STATUS) STATUSNM, C901_STATUS STATUSID FROM T6605_CRF WHERE C6605_VOID_FL IS NULL ");
    if (!strActID.equals(""))
      sbQuery.append(" AND C6630_ACTIVITY_ID IN ('" + strActID + "') ");
    if (!strPartyID.equals(""))
      sbQuery.append(" AND C101_PARTY_ID IN ('" + strPartyID + "') ");

    log.debug("getCRFStatus Query=" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("getCRFStatus alReturnBean>>>>>" + alReturn);

    return alReturn;
  }

  public ArrayList fetchCRFReport(HashMap hmParams) {

    String strActNm =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("ACTNM")));
    String strPartyid =
        GmCommonClass
            .getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("PARTYID")));
    String strApprPartyid =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams
            .get("APPRPARTYID")));
    String strFirstNm =
        GmCommonClass
            .getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("FIRSTNM")));
    String strLastNm =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("LASTNM")));
    String strStatus =
        GmCommonClass
            .getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("STATUSID")));
    String strFrmDate = GmCommonClass.parseNull((String) hmParams.get("CRFFRMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParams.get("CRFTODATE"));

    String strCreatedby = GmCommonClass.parseNull((String) hmParams.get("CREATEDBY"));

    String strUserDataCount = "";

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT COUNT(1) INT FROM T6605_CRF T6605 ");
    sbQuery.append(" WHERE T6605.C6605_CREATED_BY  IN ('" + strCreatedby + "')  ");
    sbQuery.append(" AND T6605.C6605_VOID_FL IS NULL ");

    HashMap hmResultCount = gmDBManager.querySingleRecord(sbQuery.toString());
    strUserDataCount = GmCommonClass.parseZero((String) hmResultCount.get("INT"));

    if (!strUserDataCount.equals("0")) {
      strCreatedby = strCreatedby;
    } else {
      strCreatedby = "";
    }

    sbQuery.setLength(0);

    sbQuery
        .append(" SELECT T6605.C6605_CRF_ID, T101.C101_FIRST_NM FIRSTNM, T101.C101_LAST_NM LASTNM, ");
    sbQuery
        .append(" T6605.C101_PARTY_ID PARTYID, T6605.C6630_ACTIVITY_ID ACTID, T6630.C6630_ACTIVITY_NM ACTNM, T6605.C6605_PRIORITY_FL PRIORITYFL, ");
    sbQuery.append(" GET_CODE_NAME(T6605.C901_STATUS) STATUSNM, T6605.C6605_CREATED_BY CREATEDBY ");
    sbQuery.append(" FROM T6605_CRF T6605, T6630_ACTIVITY T6630, T101_PARTY T101 ");

    if (!strApprPartyid.equals(""))
      sbQuery.append(" , T6605A_CRF_APPROVAL T6605A ");

    sbQuery
        .append(" WHERE T6605.C101_PARTY_ID   = T101.C101_PARTY_ID AND T6605.C6630_ACTIVITY_ID = T6630.C6630_ACTIVITY_ID ");
    sbQuery.append(" AND T6605.C6605_VOID_FL IS NULL AND T6630.C6630_VOID_FL IS NULL ");
    if (!strActNm.equals(""))
      sbQuery.append(" AND UPPER(T6630.C6630_ACTIVITY_NM) LIKE UPPER('%" + strActNm + "%') ");
    if (!strPartyid.equals(""))
      sbQuery.append(" AND T6605.C101_PARTY_ID IN ('" + strPartyid + "') ");

    if (!strCreatedby.equals(""))
      sbQuery.append(" AND T6605.C6605_CREATED_BY IN ('" + strCreatedby + "') ");

    if (!strApprPartyid.equals(""))
      sbQuery
          .append(" AND T6605.C6605_CRF_ID = T6605A.C6605_CRF_ID AND T6605A.C901_STATUS = '105207' AND T6605A.C101_PARTY_ID IN ('"
              + strApprPartyid + "') ");
    if (!strFirstNm.equals(""))
      sbQuery.append(" AND  UPPER(T101.C101_FIRST_NM) LIKE UPPER('%" + strFirstNm + "%') ");
    if (!strLastNm.equals(""))
      sbQuery.append(" AND UPPER(T101.C101_LAST_NM) LIKE UPPER('%" + strLastNm + "%') ");
    if (!strFrmDate.equals(""))
      sbQuery.append(" AND T6605.C6605_REQUEST_DATE >= TO_DATE('" + strFrmDate
          + "', 'MM/DD/YYYY') ");
    if (!strToDate.equals(""))
      sbQuery.append(" AND T6605.C6605_REQUEST_DATE <= TO_DATE('" + strToDate + "','MM/DD/YYYY') ");
    if (!strStatus.equals(""))
      sbQuery.append(" AND T6605.C901_STATUS IN ('" + strStatus + "') ");

    sbQuery.append(" ORDER BY PRIORITYFL ");

    log.debug("fetchCRFReport sbQuery   >>>> " + sbQuery);
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    return alReturn;
  }

  public ArrayList fetchLogReport(String strCRFId) {
    ArrayList alReturn = new ArrayList();
    strCRFId = GmCommonClass.getStringWithQuotes(strCRFId);

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT C902_LOG_ID LOGID, C902_REF_ID REFID, C902_COMMENTS LOGCOMMENT, C902_TYPE LOGTYPEID, ");
    sbQuery
        .append(" GET_CODE_NAME(C902_TYPE) LOGTYPE, C902_LAST_UPDATED_BY UPDATEDBYID, GET_USER_NAME(C902_LAST_UPDATED_BY) UPDATEDBY, ");
    sbQuery
        .append(" C902_LAST_UPDATED_DATE UPDATEDDATE,C902_VOID_FL VOIDFL FROM T902_LOG WHERE C902_VOID_FL IS NULL ");
    sbQuery.append(" AND C902_REF_ID = '" + strCRFId
        + "' AND C902_TYPE = '105181' ORDER BY UPDATEDDATE DESC, LOGID DESC");

    log.debug("fetchLogReport Query=" + sbQuery.toString());
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("fetchLogReport Query result >>>>>>>>>" + alReturn);
    return alReturn;
  }

  public void saveLogInfo(HashMap hmParams, GmDBManager gmDBManager,
      List<GmCRMLogVO> arrgmcrmlogvo, GmCRMLogVO gmLogVO) {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCRMActivityBean gmCRMActivityBean = new GmCRMActivityBean();

    int length = arrgmcrmlogvo.size();
    for (int i = 0; i < length; i++) {
      GmCRMLogVO gmCRMLogVO = arrgmcrmlogvo.get(i);

      gmCRMLogVO.setRefid((String) hmParams.get("CRFID"));
      gmCRMLogVO.setUserid((String) hmParams.get("USERID"));

      log.debug("gmOrderparamVO >>>>> " + gmWSUtil.getHashMapFromVO(gmCRMLogVO));

      gmCRMActivityBean.saveLog(gmDBManager, gmCRMLogVO);

    }
  }

  public void setCRFStatus(HashMap hmInput) {
    // ArrayList alApprovalInfo = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_crm_crf.gm_crm_set_crf_appr", 4);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmInput.get("CRFAPPRID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmInput.get("CRFID")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmInput.get("STATUSID")));
    gmDBManager.setString(4, GmCommonClass.parseNull((String) hmInput.get("COMMENTS")));
    // gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    // alApprovalInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
    gmDBManager.commit();

    // return alApprovalInfo;
  }

  public ArrayList fetchCRFSyncReport(HashMap hmParams) throws AppError {

    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_update.gm_fch_all_crf_updt", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    gmDBManager.close();
    return alResult;

    // ArrayList alReturn = new ArrayList();
    //
    // GmDBManager gmDBManager = new GmDBManager();
    //
    // StringBuffer sbQuery = new StringBuffer();
    //
    // sbQuery.append(" select C6605_CRF_ID crfid, C101_PARTY_ID partyid, C6630_ACTIVITY_ID actid, C901_REQUEST_TYPE reqtypeid , C6605_SERVICE_TYPE servicetyp , C6605_REASON reason, "
    // +
    // " C6605_SERVICE_DESCRIPTION servicedesc , C6605_HCP_QUALIFICATION hcpqualification, C6605_PREP_TIME preptime, C6605_WORK_HOURS workhrs, C6605_TIME_FL timefl, "
    // +
    // " C6605_AGREEMENT_FL agrfl, C6605_TRAVEL_FROM trvlfrom, C6605_TRAVEL_TO trvlto, C6605_TRAVEL_TIME trvltime, C6605_TRAVEL_MILES trvlmile, C6605_HOUR_RATE hrrate, "
    // +
    // " C6605_VOID_FL voidfl, C6605_CREATED_BY createdby, C901_STATUS statusid, C6605_REQUEST_DATE reqdate, C6605_HONORARIA_STATUS honstatus, C6605_HONORARIA_WORK_HRS honworkhrs, "
    // +
    // " C6605_HONORARIA_PREP_HRS honprephrs, C6605_HONORARIA_COMMENT honcomment from T6605_CRF where C6605_VOID_FL is null and decode(C6605_LAST_UPDATED_DATE,null,C6605_CREATED_DATE, "
    // + " C6605_LAST_UPDATED_DATE) >= '31-DEC-15' ");
    //
    // log.debug("Query=" + sbQuery.toString());
    //
    // alReturn = GmCommonClass.parseNullArrayList(gmDBManager
    // .queryMultipleRecords(sbQuery.toString()));
    //
    // log.debug("alReturnBean>>>>>" + alReturn);
    //
    // return alReturn;

  }

  public ArrayList fetchCRFApprvSyncReport(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_update.gm_fch_all_crfapprv_updt", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    gmDBManager.close();
    return alResult;

    // ArrayList alReturn = new ArrayList();
    //
    // GmDBManager gmDBManager = new GmDBManager();
    //
    // StringBuffer sbQuery = new StringBuffer();
    //
    // sbQuery.append(" SELECT c6605a_crf_approval_id crfapprid, c6605_crf_id CRFID, c101_party_id PARTYID, c901_status STATUSID, c6605a_comments "
    // + " COMMENTS FROM T6605A_CRF_APPROVAL ");
    //
    // log.debug("Query=" + sbQuery.toString());
    //
    // alReturn = GmCommonClass.parseNullArrayList(gmDBManager
    // .queryMultipleRecords(sbQuery.toString()));
    //
    // log.debug("alReturnBean>>>>>" + alReturn);
    //
    // return alReturn;

  }

  public ArrayList fetchCRFUpdateReport(HashMap hmParams) throws AppError {

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));

    sbQuery
        .append(" select C6605_CRF_ID crfid, C101_PARTY_ID partyid, C6630_ACTIVITY_ID actid, C901_REQUEST_TYPE reqtyp, C6605_SERVICE_TYPE servtyp, C6605_REASON reason, "
            + " C6605_SERVICE_DESCRIPTION servdesc, C6605_HCP_QUALIFICATION hcpqual, C6605_PREP_TIME preptm, C6605_WORK_HOURS workhrs, C6605_TIME_FL tmfl, "
            + " C6605_AGREEMENT_FL agreefl, C6605_TRAVEL_FROM trvlfrm, C6605_TRAVEL_TO trvlto, C6605_TRAVEL_TIME trvltm, C6605_TRAVEL_MILES trvlml, C6605_HOUR_RATE hrrate, "
            + " C6605_VOID_FL voidfl, C6605_CREATED_BY createdby, C901_STATUS status, C6605_REQUEST_DATE reqdt, C6605_HONORARIA_STATUS honstatus, C6605_HONORARIA_WORK_HRS honworkhrs, "
            + " C6605_HONORARIA_PREP_HRS honprephrs, C6605_HONORARIA_COMMENT honcmnt from T6605_CRF where decode(C6605_LAST_UPDATED_DATE,null,C6605_CREATED_DATE, "
            + " C6605_LAST_UPDATED_DATE) >= to_char((SELECT max(c9151_last_sync_date) LASTDATE FROM T9151_DEVICE_SYNC_DTL WHERE C6605_VOID_FL is null and c9151_last_updated_by = '"
            + strUserID + "')) ");

    log.debug("Query=" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;

  }

}
