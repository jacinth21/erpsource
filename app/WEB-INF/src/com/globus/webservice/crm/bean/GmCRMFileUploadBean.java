package com.globus.webservice.crm.bean;

import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.webservice.crm.valueobject.GmCRMFileUploadVO;

public class GmCRMFileUploadBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public HashMap saveUploadInfo(HashMap hmParams) throws AppError {

    GmDBManager gmDBManager = new GmDBManager();

    log.debug("RefGrp>>>> " + hmParams.get("REFGROUP"));

    gmDBManager.setPrepareString("gm_pkg_crm_fileupload.gm_sav_crm_fileupload", 8);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("FILEID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("FILETITLE")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParams.get("FILENAME")));
    gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParams.get("REFID")));
    gmDBManager.setString(5, GmCommonClass.parseNull((String) hmParams.get("REFTYPE")));
    gmDBManager.setString(6, GmCommonClass.parseNull((String) hmParams.get("REFGROUP")));
    gmDBManager.setString(7, GmCommonClass.parseNull((String) hmParams.get("DELETEFL")));
    gmDBManager.setString(8, GmCommonClass.parseNull((String) hmParams.get("USERID")));

    gmDBManager.execute();

    hmParams.put("FILEID", GmCommonClass.parseNull(gmDBManager.getString(1)));

    gmDBManager.commit();

    return hmParams;
  }

  public ArrayList fetchFiles(String RefId) {
    // TODO Auto-generated method stub

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT * FROM( SELECT C903_UPLOAD_FILE_LIST FILEID, C903_REF_ID REFID, C903_FILE_NAME FILENAME, C901_REF_TYPE REFTYPE, C901_TYPE TYPE, C901_REF_GRP REFGROUP, C901_FILE_TITLE FILETITLE, TO_CHAR(DECODE(C903_LAST_UPDATED_DATE,NULL,C903_CREATED_DATE),GET_RULE_VALUE('DATEFMT','DATEFORMAT')) UPDATEDDATE, GET_USER_NAME(DECODE(C903_LAST_UPDATED_BY,NULL,C903_CREATED_BY)) UPDATEDBY ");
    sbQuery.append(" FROM T903_UPLOAD_FILE_LIST ");
    sbQuery
        .append("  WHERE C903_REF_ID = '"
            + RefId
            + "' AND C901_REF_TYPE   = '91184'  AND c903_delete_fl IS NULL ORDER BY DECODE(C903_LAST_UPDATED_DATE,NULL,c903_created_date) DESC) UPLOADLIST WHERE ROWNUM <=5 ");
    log.debug("Query=" + sbQuery.toString());


    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;
  }

  public ArrayList fetchFilesByType(String RefId, String strRefType) {
    // TODO Auto-generated method stub

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    strRefType = GmCommonClass.getStringWithQuotes(strRefType);

    sbQuery
        .append(" SELECT C903_UPLOAD_FILE_LIST FILEID, C903_REF_ID REFID, C903_FILE_NAME FILENAME, C901_REF_TYPE REFTYPE, GET_CODE_NAME(C901_REF_TYPE) REFTYPENM, C901_TYPE TYPE, C901_REF_GRP REFGROUP, C901_FILE_TITLE FILETITLE, TO_CHAR(DECODE(C903_LAST_UPDATED_DATE,NULL,C903_CREATED_DATE),GET_RULE_VALUE('DATEFMT','DATEFORMAT')) UPDATEDDATE, GET_USER_NAME(DECODE(C903_LAST_UPDATED_BY,NULL,C903_CREATED_BY)) UPDATEDBY ");
    sbQuery.append(" FROM T903_UPLOAD_FILE_LIST ");
    sbQuery.append("  WHERE C903_REF_ID = '"+ RefId + "'");
       if(!strRefType.equals(""))  {   
            
    	   sbQuery.append(" AND C901_REF_TYPE IN ('"+ strRefType + "')");
       }   
    sbQuery.append("  AND C903_DELETE_FL IS NULL ORDER BY DECODE(C903_LAST_UPDATED_DATE,NULL,C903_CREATED_DATE) DESC");
    log.debug("Query=" + sbQuery.toString());


    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;
  }

  public ArrayList fetchFiles(GmCRMFileUploadVO gmCRMFileUploadVO) {
    // TODO Auto-generated method stub

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    String strRefId = GmCommonClass.getStringWithQuotes(gmCRMFileUploadVO.getRefid());
    String strRefType = GmCommonClass.getStringWithQuotes(gmCRMFileUploadVO.getReftype());
    String strRefGrp = GmCommonClass.getStringWithQuotes(gmCRMFileUploadVO.getRefgroup());

    sbQuery
        .append(" SELECT C903_UPLOAD_FILE_LIST FILEID, C903_REF_ID REFID, C903_FILE_NAME FILENAME, C901_REF_TYPE REFTYPE, GET_CODE_NAME(C901_REF_TYPE) REFTYPENM, C901_TYPE TYPE, C901_REF_GRP REFGROUP, C901_FILE_TITLE FILETITLE, TO_CHAR(DECODE(C903_LAST_UPDATED_DATE,NULL,C903_CREATED_DATE,C903_LAST_UPDATED_DATE),GET_RULE_VALUE('DATEFMT','DATEFORMAT')) UPDATEDDATE, GET_USER_NAME(DECODE(C903_LAST_UPDATED_BY,NULL,C903_CREATED_BY,C903_LAST_UPDATED_BY)) UPDATEDBY ");
    sbQuery.append(" FROM T903_UPLOAD_FILE_LIST ");
    sbQuery.append("  WHERE C903_DELETE_FL IS NULL ");
    if (!strRefId.equals(""))
      sbQuery.append(" AND C903_REF_ID IN ('" + strRefId + "') ");
    if (!strRefType.equals("")){
    	if(strRefType.equalsIgnoreCase("NULL"))
    		sbQuery.append(" AND C901_REF_TYPE IS NULL ");
    	else
    		sbQuery.append(" AND C901_REF_TYPE IN ('" + strRefType + "') ");
    }      
    if (!strRefGrp.equals(""))
      sbQuery.append(" AND C901_REF_GRP IN ('" + strRefGrp + "') ");
    sbQuery.append("  ORDER BY DECODE(C903_LAST_UPDATED_DATE,NULL,C903_CREATED_DATE) DESC");
    log.debug("Query=" + sbQuery.toString());


    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;
  }

}
