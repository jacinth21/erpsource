package com.globus.webservice.crm.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.webservice.crm.valueobject.GmCRMFileUploadVO;

/**
 * @author jgurunathan
 * 
 */
public class GmCRMMailBean extends GmSalesFilterConditionBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * Method used to get logged in user information to set in mail from content
   * 
   * @param strUserid
   * @return
   */
  public HashMap getUserDetails(String strUserid) {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT C101_USER_F_NAME || ' '  || C101_USER_L_NAME NAME, ");
    sbQuery.append(" C101_EMAIL_ID EMAILID ");
    sbQuery.append(" FROM T101_USER ");
    sbQuery.append(" WHERE C101_USER_ID = '" + strUserid + "' ");

    log.debug("Sender Details Query >>> " + sbQuery);

    hmReturn = GmCommonClass.parseNullHashMap((gmDBManager.querySingleRecord(sbQuery.toString())));

    return hmReturn;
  }

  /**
   * To get the all mail related data's for the event creation.
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */
  public HashMap getPhysicianVisitEmailDtl(HashMap hmParams) throws AppError {

    GmCRMFileUploadVO gmCRMFileUploadVO = new GmCRMFileUploadVO();
    HashMap hmReturn = new HashMap();
    List<HashMap> alResult = new ArrayList<HashMap>();
    List alProdReview = new ArrayList();
    List alProdLab = new ArrayList();
    List alOtherVisitor = new ArrayList();
    List alSurgeon = new ArrayList();
    List alParticipant = new ArrayList();
    List alProjects = new ArrayList();
    List alProducts = new ArrayList();
    List alAttendees = new ArrayList();
    List alSecurityEventUserEmails = new ArrayList();
    

    String strActID = GmCommonClass.parseNull((String) hmParams.get("ACTID"));
    String strActcategory = GmCommonClass.parseNull((String) hmParams.get("ACTCATEGORY"));
    String strFileRefGrp = "";
    String strDivision = "";

    GmCRMFileUploadBean gmCRMFileUploadBean = new GmCRMFileUploadBean();


      hmReturn = getEventDetails(strActID);
      alResult = fetchActivityAttrReport(hmParams);
      strFileRefGrp = "107994"; 


    for (HashMap hmTemp : alResult) {
      String strAttrType = GmCommonClass.parseNull((String) hmTemp.get("ATTRTYPE"));
      if (strAttrType.equals("105582")) {// Products to Review
        alProdReview.add(hmTemp);
      } else if (strAttrType.equals("105586")) {// Products to Use in Lab
        alProdLab.add(hmTemp);
      } else if (strAttrType.equals("105590")) {// Other Visitors
        alOtherVisitor.add(hmTemp);
      } else if (strAttrType.equals("105583")) {
        alProjects.add(hmTemp);
      }
    }

    alResult = fetchActivityPartyReport(hmParams);
    for (HashMap hmTemp : alResult) {
      String strAttrType = GmCommonClass.parseNull((String) hmTemp.get("ROLEID"));
      if (strAttrType.equals("105564")) {// Surgeons / Attendee
        alSurgeon.add(hmTemp);
      } else if (strAttrType.equals("105561")) {// Participant
        alParticipant.add(hmTemp);
      }
    }

    alResult = fetchPSProducts(hmParams);
    for (HashMap hmTemp : alResult) {
      alProducts.add(hmTemp);
    }



    gmCRMFileUploadVO.setRefid(strActID);
    gmCRMFileUploadVO.setRefgroup(strFileRefGrp);
    alResult = gmCRMFileUploadBean.fetchFiles(gmCRMFileUploadVO);// .fetchFilesByType(strActID, "");
    log.debug("getPhysicianVisitEmailDtl fetchFiles >>>>>>>>>" + alResult);
    
    // PC-4769 - Ability to map users to receive VIP visit notification based on Division
    strDivision = (String)hmReturn.get("DIVID");
    alSecurityEventUserEmails= fetchSecurityEventUserEmails(strDivision, "CRM_INVT_GRP_"+strActcategory);

    hmReturn.put("ALPRODREVIEW", alProdReview);
    hmReturn.put("ALPRODLAB", alProdLab);
    hmReturn.put("ALOTHERVISITOR", alOtherVisitor);
    hmReturn.put("ALOTHERVISITSIZE", alOtherVisitor.size());
    hmReturn.put("ALSURGEON", alSurgeon);
    hmReturn.put("ALPARTICIPANT", alParticipant);
    hmReturn.put("ALPROJECTS", alProjects);
    hmReturn.put("ALPRODUCTS", alProducts);
    hmReturn.put("ALFILES", alResult);
    hmReturn.put("ALSECURITYEVENTUSEREMAILS", alSecurityEventUserEmails);

    return hmReturn;

  }


  /**
   * Method used for get event related information for the current activity
   * 
   * @param strActID
   * @return
   */
  public HashMap getEventDetails(String strActID) {

    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT T6630.C6630_ACTIVITY_ID ACTID, TO_CHAR(T6630.C6630_CREATED_DATE,'MM/DD/YYYY') CREATEDDATE, C6630_DIETARY DIETARY, ");
    sbQuery.append(" T6630.C901_ACTIVITY_TYPE ACTTYPID, ");
    sbQuery.append(" GET_CODE_NAME(T6630.C901_ACTIVITY_TYPE) ACTTYP, ");
    sbQuery.append(" GET_RULE_VALUE(T6630.C901_ACTIVITY_TYPE,'EVENTCATEGORY') CATEGORY, ");
    sbQuery
        .append(" T6630.C6630_ACTIVITY_NM ACTNM, GET_DIVISION_NAME(C1910_DIVISION_ID) DIVISION, C1910_DIVISION_ID DIVID, ");
    sbQuery.append(" T6630.c6630_event_item_id EVTITEMID, ");
    sbQuery.append(" TO_CHAR(T6630.C6630_FROM_DT,'YYYY/MM/DD') FRMDATE, ");
    sbQuery.append(" TO_CHAR(T6630.C6630_TO_DT,'YYYY/MM/DD') TODATE, ");
    sbQuery
        .append(" TO_CHAR(CAST(T6630.C6630_FROM_TM AT TIME ZONE 'GMT' AS TIMESTAMP WITH TIME ZONE),'YYYY/MM/DD HH24:MI:SS') FRMDTTIME, ");
    sbQuery
        .append(" TO_CHAR(CAST(T6630.C6630_TO_TM AT TIME ZONE 'GMT' AS TIMESTAMP WITH TIME ZONE),'YYYY/MM/DD HH24:MI:SS') TODTTIME, ");
    sbQuery
        .append(" TO_CHAR(T6630.C6630_FROM_DT,'MM/DD/YYYY') ||' ' ||  TO_CHAR(T6630.C6630_FROM_TM,'HH12:MI AM') ");
    sbQuery
        .append(" || ' - ' || TO_CHAR(T6630.C6630_TO_TM,'HH12:MI AM') || ' ' || TO_CHAR(T6630.C6630_FROM_TM,'TZR') FULLDATE, ");
    sbQuery.append(" TO_CHAR(T6630.C6630_FROM_TM,'TZR') TZONE, ");
    sbQuery.append(" GET_USER_NAME(T6630.C6630_CREATED_BY) ORGNM, ");
    sbQuery.append(" GET_USER_EMAILID(T6630.C6630_CREATED_BY) ORGMAIL, ");
    sbQuery
        .append(" C6630_OTHER_INSTRUMENT OTHERLABINST , C6630_TOP_PRIORITY TOPPRIO, T6630.C6630_PATHOLOGY PATHLGY, ");
    sbQuery
        .append(" T6630.C6630_VOID_FL VOIDFL,T6630.C6630_NOTE NOTE,GET_SEGMENT_NAME(T6630.C2090_SEGMENT_ID) SEGNM, T6630.C6630_PARTY_NM PARTYNM ");
    sbQuery.append(" FROM T6630_ACTIVITY T6630 ");
    sbQuery.append(" WHERE T6630.C6630_ACTIVITY_ID = '" + strActID + "' ");

    log.debug("Event Details Query >>> " + sbQuery);

    hmReturn = GmCommonClass.parseNullHashMap((gmDBManager.querySingleRecord(sbQuery.toString())));

    return hmReturn;
  }

  /**
   * Method used to get activity attributes for events info
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */
  public ArrayList fetchActivityAttrReport(HashMap hmParams) throws AppError {

    String strActID = GmCommonClass.parseNull((String) hmParams.get("ACTID"));
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT C6632_ACTIVITY_ATTRIBUTE_ID ATTRID, c6632_attribute_value ATTRVAL, C6632_ATTRIBUTE_ID ATTID, C6632_ATTRIBUTE_NM ATTRNM, C901_ATTRIBUTE_TYPE ATTRTYPE, C6632_VOID_FL VOIDFL ");
    sbQuery
        .append(" FROM T6632_ACTIVITY_ATTRIBUTE WHERE C6632_VOID_FL IS NULL AND C6630_ACTIVITY_ID  = '"
            + strActID + "'");

    log.debug("Query=" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;

  }

  /**
   * Method used to get preceptorship case request attributes for events info
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */
  public ArrayList fetchPSAttrReport(HashMap hmParams) throws AppError {
    String strActID = GmCommonClass.parseNull((String) hmParams.get("ACTID"));
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT C6663_CASE_REQ_ATTR_ID ATTRID, C6663_ATTRIBUTE_VALUE ATTRVAL,C6663_ATTRIBUTE_NM ATTRNM, C901_ATTRIBUTE_TYPE ATTRTYPE, C6663_VOID_FL VOIDFL ");
    sbQuery
        .append(" FROM t6663_case_req_attr WHERE C6663_VOID_FL IS NULL AND C6660_CASE_REQUEST_ID  = '"
            + strActID + "'");
    log.debug("fetchPSAttrReport Query=" + sbQuery.toString());
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
    log.debug("alReturnBean>>>>>" + alReturn.size());
    return alReturn;
  }


  /**
   * Method used to get preceptorship case request set names for events info
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */
  public ArrayList fetchPSProducts(HashMap hmParams) throws AppError {
    String strActID = GmCommonClass.parseNull((String) hmParams.get("ACTID"));
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT C6662_SET_NM SETNM FROM T6662_Case_Request_detail WHERE C6662_VOID_FL IS NULL AND C6660_CASE_REQUEST_ID  = '"
            + strActID + "'");
    log.debug("fetchPSProducts Query=" + sbQuery.toString());
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
    log.debug("alReturnBean>>>>>" + alReturn.size());
    return alReturn;
  }



  /**
   * Method used to get all the party related information
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */
  public ArrayList fetchActivityPartyReport(HashMap hmParams) throws AppError {

    String strActID = GmCommonClass.parseNull((String) hmParams.get("ACTID"));
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append("SELECT T6631.C6631_ACTIVITY_PARTY_ID ACTPARTYID, T6631.C101_PARTY_ID PARTYID,T101U.C101_EMAIL_ID EMAILID, ");
    sbQuery
        .append(" T6631.C901_ACTIVITY_PARTY_ROLE ROLEID, GET_CODE_NAME(T6631.C901_ACTIVITY_PARTY_ROLE) ROLENM, ");
    sbQuery
        .append(" npid.npid PARTYNPID,  GET_CODE_NAME(npid.salute) || ' ' || T101.C101_FIRST_NM || ' ' || T101.C101_MIDDLE_INITIAL || ' ' || T101.C101_LAST_NM PARTYNM, ");
    sbQuery
        .append(" T6631.C901_PARTY_STATUS STATUSID, GET_CODE_NAME(T6631.C901_PARTY_STATUS) STATUSNM, T6631.C6631_VOID_FL VOIDFL, "
            + " T6631.C6630_ACTIVITY_ID ACTID , t101.c901_party_type partytype, DECODE(t101.c901_party_type,7000,get_surgeon_cv_path(T6631.C101_PARTY_ID),'') SURGEONCV ");
    sbQuery
        .append(" FROM T6631_ACTIVITY_PARTY_ASSOC T6631, T101_PARTY T101 , T101_USER T101U , "
            + "(SELECT c6600_surgeon_npi_id npid , C6600_PARTY_SALUTE salute, c101_party_surgeon_id partyid FROM t6600_party_surgeon WHERE c6600_void_fl is null) npid "
            + "WHERE T6631.C101_PARTY_ID = T101.C101_PARTY_ID(+) AND T101.C101_PARTY_ID = NPID.PARTYID(+) AND T101.C101_PARTY_ID = T101U.C101_PARTY_ID(+) AND T6631.C6631_VOID_FL IS NULL AND C6630_ACTIVITY_ID = '"
            + strActID + "'");

    log.debug("fetchActivityPartyReport Query=" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;

  }

  /**
   * To save the event item id for the activity.
   * 
   * @param hmParams
   */
  public void saveEventItemId(HashMap hmParams) {
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_crm_activity.gm_sav_crm_event_item_id", 3);

    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("ACTID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("ITEMID")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParams.get("USERID")));

    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * This method used for sending mail for physician visit users.
   * 
   * @param strTemplate,gmEmailProperties,hmEventDetails
   * @throws AppError ,Exception
   */
  public void sendNotification(String strTemplate, GmEmailProperties gmEmailProperties,
      HashMap hmEventDetails) throws AppError, Exception {
    log.debug("sendNotification " + strTemplate);
    
    String strMsgTable =  GmCommonClass.parseNull((String) hmEventDetails.get("MSGTABLE"));
    String strMessage = GmCommonClass.getEmailProperty(
            strTemplate + "." + GmEmailProperties.MESSAGE_BODY).replace("[Creator]",
                    (String) hmEventDetails.get("ORGNM"));
    
    
    gmEmailProperties.setSender(GmCommonClass.getEmailProperty(strTemplate + "."
        + GmEmailProperties.FROM));
    gmEmailProperties.setRecipients((String) hmEventDetails.get("ORGMAIL"));
    gmEmailProperties.setCc(GmCommonClass
        .getEmailProperty(strTemplate + "." + GmEmailProperties.CC));
    gmEmailProperties
        .setSubject(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.SUBJECT)
            .concat((String) hmEventDetails.get("ACTNM")));
    gmEmailProperties.setMessage(strMessage+"<BR><CENTER>"+strMsgTable+"</CENTER>");
    gmEmailProperties.setMimeType(GmCommonClass.getEmailProperty(strTemplate + "."
        + GmEmailProperties.MIME_TYPE));
    GmCommonClass.sendMail(gmEmailProperties);
  }

  
  // PC-4769 - Ability to map users to receive VIP visit notification based on Division
  
  /**
   * Method used to get recipient group email id for VIP and Preceptorship visit notification
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */
  
  public ArrayList fetchSecurityEventUserEmails(String strDivId, String strRuleGroup) throws AppError
  {
	    ArrayList alReturn = new ArrayList();
	    GmDBManager gmDBManager = new GmDBManager();
	    StringBuffer sbQuery = new StringBuffer();
	    
	    sbQuery.append(" SELECT DISTINCT t101.c101_email_id emailid");
	    sbQuery.append(" FROM t1530_access t1530, t1501_group_mapping t1501,");
	    sbQuery.append(" t1520_function_list t1520, t101_user t101 ");
	    sbQuery.append(" WHERE t1520.c1520_function_id = t1530.c1520_function_id ");
	    sbQuery.append(" AND t1530.c1500_group_id = NVL(t1501.c1500_group_id,t1501.c1500_mapped_group_id) ");
	    sbQuery.append(" AND t1501.c101_mapped_party_id = t101.c101_party_id ");
	    sbQuery.append(" AND t1520.c1520_function_id IN(SELECT c906_rule_value FROM t906_rules WHERE c906_rule_id ='"+ strDivId +"'" );
	    sbQuery.append(" AND c906_rule_grp_id = '"+ strRuleGroup + "'");
	    sbQuery.append(" AND c906_void_fl IS NULL) AND t1530.c1530_void_fl IS NULL ");
	    sbQuery.append(" AND t1501.C1501_VOID_FL IS NULL AND t1520.C1520_VOID_FL IS NULL");
	    
	    log.debug("fetchActivityPartyReport Query=" + sbQuery.toString());
	    alReturn =
	        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
	    log.debug("alReturnEmailDtls>>>>" + alReturn);
	    return alReturn;

  }
  

}
