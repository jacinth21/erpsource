package com.globus.webservice.crm.bean;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmCRMCommonBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ArrayList fetchUserFunctionList(HashMap hmParams) throws AppError {

    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();

    gmDBManager.setPrepareString("gm_pkg_crm_module.gm_crm_get_user_module", 3);

    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("USERID")));
    gmDBManager.setString(2, "CRM");
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    for (int i = 0; i < alReturn.size(); i++) {
      HashMap hmRecord = (HashMap) alReturn.get(i);
      hmRecord.remove("SEQ");
      alReturn.remove(i);
      alReturn.add(i, hmRecord);
    }

    log.debug("alReturn -->" + alReturn);
    return alReturn;

  }
}
