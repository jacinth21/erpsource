package com.globus.webservice.crm.bean;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmWSUtil;
import com.globus.party.beans.GmProffessionalDetailsBean;
import com.globus.sales.beans.GmSalesFilterConditionBean;

public class GmCRMSurgicalActivityBean extends GmSalesFilterConditionBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	/**
	 * This method used to fetch CRM Surgeon date wise DO Counts. In Surgical
	 * Activity Module, the Fn Retrieves the DO List Report when clicking the
	 * Calendar Icon. Inputs are token, partyid, fromdt, todt
	 */

	public ArrayList fetchCalendarInfo(HashMap hmParams) throws AppError {
		log.debug("hmParams>>>>>" + hmParams);
		GmDBManager gmDBManager = new GmDBManager();
		ArrayList alResult = new ArrayList();
		GmAccessFilter gmAccessFilter = new GmAccessFilter();
		HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
		
		String strPartyId = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
		String strFromDate = GmCommonClass.parseNull((String) hmParams.get("FROMDATE"));
		String strToDate = GmCommonClass.parseNull((String) hmParams.get("TODATE"));
		
		hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
		String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
	    log.debug(" Surgical Calendar Info strCondition>>>  "+strCondition);
		
		StringBuffer sbQuery = new StringBuffer();
		
		sbQuery.append(" SELECT NVL(T501.C501_Surgery_Date,T501.C501_Order_Date) SURGDT,");
		sbQuery.append(" V_SYS.SEGMENT_ID SEGID,V_SYS.SEGMENT_NAME SEGNM,V_Sys.System_Id Sysid,V_SYS.SYSTEM_NM SYSNM,");
		sbQuery.append(" COUNT(1) cnt FROM  T6640_NPI_TRANSACTION T6640,T501_ORDER T501,T501B_ORDER_BY_SYSTEM T501B,");
		sbQuery.append(" V_SEGMENT_SYSTEM_MAP V_SYS WHERE T6640.C101_PARTY_SURGEON_ID = '" + strPartyId + "'");
		sbQuery.append(" AND NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE) >= TO_DATE('" + strFromDate + "','YYYYMM')");
		sbQuery.append(" AND NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE) <= LAST_DAY(TO_DATE('" + strToDate + "','YYYYMM'))");
		sbQuery.append(" AND T6640.C6640_VALID_FL = 'Y' AND T6640.C6640_REF_ID = T501.C501_ORDER_ID AND T501.C501_ORDER_ID = T501B.C501_ORDER_ID");
		sbQuery.append(" AND T501B.C207_SYSTEM_ID = V_SYS.SYSTEM_ID AND NVL (C901_ORDER_TYPE, -9999) NOT IN");
		sbQuery.append(" (SELECT C906_RULE_VALUE FROM V901_ORDER_TYPE_GRP) AND T501.C501_PARENT_ORDER_ID IS NULL");
		sbQuery.append(" AND T6640.C6640_VOID_FL IS NULL AND T501.C501_VOID_FL IS NULL AND T501.C501_DELETE_FL IS NULL AND T501B.C501B_VOID_FL IS NULL");
		if (!strCondition.equals(""))
			sbQuery.append(" AND " + strCondition + " ");
		sbQuery.append(" Group By NVL(T501.C501_Surgery_Date,T501.C501_Order_Date) ,V_SYS.SEGMENT_ID ,V_SYS.SEGMENT_NAME ,V_Sys.System_Id ,V_SYS.SYSTEM_NM");
		sbQuery.append(" UNION ALL SELECT  NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE) SURGDT,0 SEGID,");
		sbQuery.append(" 'ALL DO' SEGNM, '0' SYSID, 'ALL DO' SYSNM, Count(1) cnt ");
		sbQuery.append(" FROM T6640_NPI_TRANSACTION T6640, T501_ORDER T501 WHERE T6640.C101_PARTY_SURGEON_ID =  '" + strPartyId + "'");
		sbQuery.append(" AND NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE) >= TO_DATE( '" + strFromDate + "','YYYYMM')");
		sbQuery.append(" AND NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE) <= LAST_DAY(TO_DATE( '" + strToDate + "','YYYYMM')) AND T6640.C6640_VALID_FL = 'Y' ");
		sbQuery.append(" AND NVL (C901_ORDER_TYPE, -9999) NOT IN (SELECT C906_RULE_VALUE FROM V901_ORDER_TYPE_GRP ) ");
		sbQuery.append(" AND T6640.C6640_REF_ID = T501.C501_ORDER_ID ");
		sbQuery.append(" AND T6640.C6640_VOID_FL IS NULL AND T501.C501_VOID_FL IS NULL AND T501.C501_DELETE_FL IS NULL");
		if (!strCondition.equals(""))
			sbQuery.append(" AND " + strCondition + " ");
		sbQuery.append(" group by NVL(T501.C501_Surgery_Date,T501.C501_Order_Date)");
		log.debug("fetchSurgicalCalendarInfo SbQUERY>>>>>" + sbQuery);
		alResult = GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));	
		gmDBManager.close();
		return alResult;

	}

	/**
	 * This method used to fetch CRM Surgeon System Details. In Surgical
	 * Activity Module, the Fn Retrieves the System List when clicking the Count
	 * Event which placed in the calendar. Inputs are token, partyid,orderdate
	 */
	
	public ArrayList fetchSysDetailsInfo(HashMap hmParams) throws AppError {
		ArrayList alReturn = new ArrayList();
		GmAccessFilter gmAccessFilter = new GmAccessFilter();
		HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
		String strPartyId = GmCommonClass.parseNull((String) hmParams
				.get("PARTYID"));
		String strOrdDate = GmCommonClass.parseNull((String) hmParams
				.get("ORDDATE"));
		String strSysId = GmCommonClass.parseNull((String) hmParams.get("SYSID"));
		
		hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
		String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
	    log.debug(" SysDO Details strCondition>>>  "+strCondition);
		
		GmDBManager gmDBManager = new GmDBManager();
		StringBuffer sbQuery = new StringBuffer();
		
		sbQuery.append(" SELECT GET_PARENT_ACCOUNT_NAME(T.ACCTID) ACCTNM , T.SURGDT, T.PARENTORD, V_SEG.SEGMENT_NAME SEGMENTNM, V_SEG.SEGMENT_ID SEGMENTID, V_SEG.SYSTEM_NM SYSNM,T.ORD,"
				+ "T.PNUM,GET_PARTNUM_DESC(T.PNUM) PARTDESC,T.QTY, T.PRICE, (T.QTY * T.PRICE) TOTAL FROM ");
		sbQuery.append(" (SELECT NVL(T501.C501_PARENT_ORDER_ID,T501.C501_ORDER_ID)PARENTORD, T501.C501_ORDER_ID ORD,");
		sbQuery.append(" NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE) SURGDT, T501.C704_ACCOUNT_ID ACCTID, GET_SET_ID_FROM_PART(T502.C205_PART_NUMBER_ID) SYSTEMID,");
		sbQuery.append(" T502.C205_PART_NUMBER_ID PNUM,T502.C502_ITEM_QTY QTY,T502.C502_ITEM_PRICE PRICE FROM");
		sbQuery.append(" T6640_NPI_TRANSACTION T6640,T501_ORDER T501, T502_ITEM_ORDER T502");
		if (!strSysId.equals("0")){
			sbQuery.append(" ,T501B_ORDER_BY_SYSTEM T501B");
		}
		sbQuery.append("  WHERE T6640.C6640_REF_ID = NVL(T501.C501_PARENT_ORDER_ID, T501.C501_ORDER_ID)");
		sbQuery.append(" AND T501.C501_ORDER_ID = T502.C501_ORDER_ID ");
		sbQuery.append(" AND NVL (C901_ORDER_TYPE, -9999) NOT IN (SELECT C906_RULE_VALUE FROM V901_ORDER_TYPE_GRP )");
		sbQuery.append(" AND T6640.C6640_VOID_FL IS NULL ");
		sbQuery.append(" AND T502.C502_VOID_FL IS NULL ");
		sbQuery.append(" AND T6640.C101_PARTY_SURGEON_ID ="+ strPartyId +"");
		sbQuery.append(" AND T6640.C6640_VALID_FL = 'Y'");
		sbQuery.append(" AND NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE) = TO_DATE("+"'"+ strOrdDate +"'"+", 'MM/DD/YYYY')");
		sbQuery.append(" AND T501.C501_VOID_FL IS NULL AND T501.C501_DELETE_FL IS NULL ");
		if (!strSysId.equals("0")){
			sbQuery.append(" AND NVL(T501.C501_PARENT_ORDER_ID, ");
			sbQuery.append(" T501.C501_ORDER_ID) = T501B.C501_ORDER_ID ");
			sbQuery.append(" AND T501B.C207_SYSTEM_ID ="+ strSysId +"");
			sbQuery.append(" AND T501B.C501B_VOID_FL IS NULL ");
		}
		if (!strCondition.equals(""))
			sbQuery.append(" AND " + strCondition + " ");
		sbQuery.append(" )T,V_SEGMENT_SYSTEM_MAP V_SEG WHERE T.SYSTEMID = V_SEG.SYSTEM_ID(+) ");
		sbQuery.append(" ORDER BY ACCTNM,PARENTORD,SEGMENTID,SEGMENTNM,SYSNM, ORD, PNUM,QTY");
		log.debug("fetchSysDetailsInfo SbQUERY>>>>>" + sbQuery);
		alReturn = GmCommonClass.parseNullArrayList(gmDBManager
				.queryMultipleRecords(sbQuery.toString()));

		log.debug("alReturnBean>>>>>" + alReturn.size());

		return alReturn;
		
//		log.debug("hmParams>>>>>" + hmParams);
//		GmDBManager gmDBManager = new GmDBManager();
//		ArrayList alResult = new ArrayList();
//		gmDBManager.setPrepareString(
//				"gm_pkg_crm_surgeon.gm_fch_sysdetails_info", 4);
//		gmDBManager.setInt(1, Integer.parseInt(GmCommonClass
//				.parseNull((String) hmParams.get("PARTYID"))));
//		gmDBManager.setString(2, (String) hmParams.get("ORDDATE"));
//		gmDBManager.setString(3, (String) hmParams.get("SYSID"));
//		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
//		gmDBManager.execute();
//		alResult = GmCommonClass.parseNullArrayList(gmDBManager
//				.returnArrayList((ResultSet) gmDBManager.getObject(4)));
//		gmDBManager.close();
//		return alResult;
	}
}
