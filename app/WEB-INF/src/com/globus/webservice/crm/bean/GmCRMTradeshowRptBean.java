package com.globus.webservice.crm.bean;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmTrackingEmailEngine;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.beans.GmBean;
import com.globus.valueobject.common.GmDataStoreVO;

import oracle.jdbc.OracleTypes;


/**
 * @author TMuthusamy
 * GmCRMTradeshowRptBean  --- Tradeshow Report Details
 */
public class GmCRMTradeshowRptBean extends GmBean {

	  Logger log = GmLogger.getInstance(this.getClass().getName());
	  
	  public GmCRMTradeshowRptBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		   super(gmDataStoreVO);
		   // TODO Auto-generated constructor stub
	   }

/**
 * This method used to fetch Tradeshow details to display in CRM Dashboard
 * @return
 * @throws AppError
 */
public String fetchTradeshowDash (String strInput) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_crm_tradeshow_rpt.gm_fch_tradeshow_dash", 2);
    gmDBManager.setString(1, strInput);
    gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
    gmDBManager.execute();
    String strTradeDetails = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.close();
    return strTradeDetails;
     
}

/**
 * This method used to fetch Tradeshow Attendee Report
 * @return
 * @throws AppError
 */
public String fetchtradeshowAttendeeRpt (String strInput) throws AppError {
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	String strReturn = "";
	gmDBManager.setPrepareString("gm_pkg_crm_tradeshow_rpt.gm_fch_tradeshow_attendee_list", 2);
	gmDBManager.setString(1, strInput);
	gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
	gmDBManager.execute();
	strReturn = GmCommonClass.parseNull(gmDBManager.getString(2));
	gmDBManager.close();
	return strReturn;
     
}

/**
 * This method used to fetch Tradeshow Attendee Email
 * @return
 * @throws AppError
 */
public String sendTradeshowAttendeeMail (HashMap<String, String> hmParm) throws AppError {
	String strTemplateName = "GmCRMTradeshowAttendeeMail.vm";
	String strEmailTempProperty = "GmCRMTradeshowAttendeeMail";

	HashMap hmEmailProperty = new HashMap();
	GmTemplateUtil templateUtil = new GmTemplateUtil();
	GmTrackingEmailEngine gmTrackingEmailEngine = new GmTrackingEmailEngine();
 		
	String tradeInfo = hmParm.get("TRADEINFO").replaceAll("\n", "<BR>");
	String msg = hmParm.get("MSG").replaceAll("\n", "<BR>");
	String attendeeMail = hmParm.get("ATTENDEEMAIL");
 	    
	hmParm.put("TRADEINFO", tradeInfo);
	hmParm.put("MSG", msg);
	templateUtil.setDataMap("hmParm", hmParm);
	templateUtil.setTemplateSubDir("operations/templates");  
	templateUtil.setTemplateName(strTemplateName);	 
	String strEmailContent = templateUtil.generateOutput();
	String[] strTo = GmCommonClass.StringtoArray(GmCommonClass.parseNull(attendeeMail),",");
	String[] strCC = GmCommonClass.StringtoArray(GmCommonClass.getEmailProperty(
			             strEmailTempProperty + "." + GmEmailProperties.CC), ",");
	
	//PC-2939
	String strUsrid = GmCommonClass.parseNull(hmParm.get("USERID"));
	String strFrom = fetchUserEmailAddress(strUsrid);
	//PC-4592
	String strSubject = GmCommonClass.parseNull(hmParm.get("SUBJECT"));
	
	hmEmailProperty.put("strFrom",strFrom);
	
	hmEmailProperty.put("strTo",strTo); 
	hmEmailProperty.put("strCc",strCC); 
	//PC-4592
	//hmEmailProperty.put("strSubject",GmCommonClass.parseNull(GmCommonClass.getEmailProperty(strEmailTempProperty  + "."+ GmEmailProperties.SUBJECT)));
	hmEmailProperty.put("strSubject",strSubject);
	hmEmailProperty.put("strMessage",GmCommonClass.parseNull(GmCommonClass.getEmailProperty(strEmailTempProperty  + "."+ GmEmailProperties.MESSAGE)));
	hmEmailProperty.put("strMessageDesc", strEmailContent);
	hmEmailProperty.put("strMimeType", GmCommonClass.parseNull(GmCommonClass.getEmailProperty(strEmailTempProperty  + "." + GmEmailProperties. MIME_TYPE)));
	log.debug("sendTradeshowAttendeeMail hmEmailProperty >>> " + hmEmailProperty);
	String strResult = gmTrackingEmailEngine.sendEmailTracking(hmEmailProperty);
	log.debug("gmTrackingEmailEngine strResult"+strResult);
	return strResult;
}

//PC-2939
/**
 * This method used to fetch Tradeshow From Email Address
 * @return
 * @throws AppError
 */

public String fetchUserEmailAddress(String strUsrid)
{
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	String strFromEmailId = "";
	gmDBManager.setPrepareString("gm_pkg_crm_tradeshow_rpt.gm_fch_rep_email_from_userid", 2);
	gmDBManager.setString(1, strUsrid);
	gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
	gmDBManager.execute();
	strFromEmailId = GmCommonClass.parseNull(gmDBManager.getString(2));
	gmDBManager.close();
	return strFromEmailId;
}


}
