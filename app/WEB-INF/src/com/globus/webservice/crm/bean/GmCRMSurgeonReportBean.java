package com.globus.webservice.crm.bean;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmCRMSurgeonReportBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * Fetches the Surgeon Education Report list information for CRM
   * 
   * @param hmParams
   * @return ArrayList
   * @throws com.globus.common.beans.AppError
   */
  public ArrayList fetchSurgeonEducationReport(HashMap hmParams) throws AppError {

    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();

    String strLevel = GmCommonClass.parseNull((String) hmParams.get("LEVEL"));
    String strInstitutenm = GmCommonClass.parseNull((String) hmParams.get("INSTITUTENM"));
    String strClassyear = GmCommonClass.parseNull((String) hmParams.get("CLASSYEAR"));
    String strCourse = GmCommonClass.parseNull((String) hmParams.get("COURSE"));
    String strDesignation = GmCommonClass.parseNull((String) hmParams.get("DESIGNATION"));

    sbQuery.append(" SELECT T101.C101_PARTY_ID PARTYID, ");
    sbQuery.append(" T101.C101_FIRST_NM FIRSTNM, ");
    sbQuery.append(" T101.C101_LAST_NM LASTNM, ");
    sbQuery.append(" T101.C101_PARTY_NM PARTYNM, ");
    sbQuery.append(" T101.C101_MIDDLE_INITIAL MIDINITIAL, ");
    sbQuery.append(" T1081.C901_STATUS STATUS, ");
    sbQuery.append(" T1010.CLASSYEAR CLASSYEAR ");
    sbQuery.append(" FROM T101_PARTY T101, ");
    sbQuery
        .append(" (SELECT C901_STATUS, C101_PARTY_ID FROM T1081_PARTY_STATUS WHERE C1081_VOID_FL IS NULL) T1081, ");
    sbQuery.append(" (SELECT C101_PARTY_ID PARTYID, ");
    sbQuery
        .append(" (RTRIM(XMLAGG(XMLELEMENT(E,get_code_name(C901_EDUCATION_LEVEL)|| ': '|| C1010_CLASS_YEAR || ',')).EXTRACT('//text()').GETCLOBVAL(),',') ) CLASSYEAR ");
    sbQuery.append(" FROM T1010_EDUCATION  WHERE C1010_VOID_FL IS NULL ");
    if (!strLevel.equals("")) {
      sbQuery.append(" AND  C901_EDUCATION_LEVEL = '" + strLevel + "' ");
    }
    if (!strInstitutenm.equals("")) {
      sbQuery.append(" AND  UPPER(C1010_INSTITUTE_NM) LIKE UPPER('%'||'" + strInstitutenm
          + "'||'%') ");
    }
    if (!strClassyear.equals("")) {
      sbQuery.append(" AND  C1010_CLASS_YEAR = '" + strClassyear + "' ");
    }
    if (!strCourse.equals("")) {
      sbQuery.append(" AND  UPPER(C1010_DEGREE_NM) LIKE UPPER('%'||'" + strCourse + "'||'%') ");
    }
    if (!strDesignation.equals("")) {
      sbQuery.append(" AND  C901_DEGREE_DESIGNATION = '" + strDesignation + "' ");
    }
    sbQuery.append(" GROUP BY C101_PARTY_ID) T1010 ");
    sbQuery.append(" WHERE T101.C901_PARTY_TYPE = '7000' ");
    sbQuery.append(" AND T101.C101_ACTIVE_FL   IS NULL ");
    sbQuery.append(" AND T101.C101_VOID_FL     IS NULL ");
    sbQuery.append(" AND T101.C101_PARTY_ID = T1081.C101_PARTY_ID(+) ");
    sbQuery.append(" AND T101.C101_PARTY_ID = T1010.PARTYID ");
    sbQuery.append(" ORDER BY UPPER(FIRSTNM ||' '|| LASTNM) ");

    log.debug("fetchSurgeonEducationReport Query>>>>>" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("fetchSurgeonEducationReport alReturnBean Size>>>>>" + alReturn.size());

    return alReturn;

  }

  /**
   * Fetches the Surgeon Professional Report list information for CRM
   * 
   * @param hmParams
   * @return ArrayList
   * @throws com.globus.common.beans.AppError
   */
  public ArrayList fetchSurgeonProfessionalReport(HashMap hmParams) throws AppError {

    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();

    String strCountry =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams
            .get("COUNTRYNM")));
    String strState =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("STATE")));
    String strCity = GmCommonClass.parseNull((String) hmParams.get("CITY"));
    String strTitle = GmCommonClass.parseNull((String) hmParams.get("TITLE"));
    String strCompanyname = GmCommonClass.parseNull((String) hmParams.get("COMPANYNAME"));
    String strStartmonth = GmCommonClass.parseNull((String) hmParams.get("STARTMONTH"));
    String strStartyear = GmCommonClass.parseNull((String) hmParams.get("STARTYEAR"));
    String strEndmonth = GmCommonClass.parseNull((String) hmParams.get("ENDMONTH"));
    String strEndyear = GmCommonClass.parseNull((String) hmParams.get("ENDYEAR"));
    String strStillinposition = GmCommonClass.parseNull((String) hmParams.get("STILLINPOSITION"));
    String strGroupdivision = GmCommonClass.parseNull((String) hmParams.get("GROUPDIVISION"));


    sbQuery.append(" SELECT T101.C101_PARTY_ID PARTYID, ");
    sbQuery.append(" T101.C101_FIRST_NM FIRSTNM, ");
    sbQuery.append(" T101.C101_LAST_NM LASTNM, ");
    sbQuery.append(" T101.C101_PARTY_NM PARTYNM, ");
    sbQuery.append(" T101.C101_MIDDLE_INITIAL MIDINITIAL, ");
    sbQuery.append(" T1081.C901_STATUS STATUS, ");
    sbQuery.append(" T1020.CNT RESULTCNT ");
    sbQuery.append(" FROM T101_PARTY T101, ");
    sbQuery
        .append(" (SELECT C901_STATUS, C101_PARTY_ID FROM T1081_PARTY_STATUS WHERE C1081_VOID_FL IS NULL) T1081, ");
    sbQuery
        .append(" (SELECT C101_PARTY_ID PARTYID, COUNT(1) CNT FROM T1020_CAREER WHERE C1020_VOID_FL IS NULL ");
    if (!strTitle.equals("")) {
      sbQuery.append(" AND  UPPER(C1020_TITLE) LIKE UPPER('%'||'" + strTitle + "'||'%') ");
    }
    if (!strCompanyname.equals("")) {
      sbQuery.append(" AND  UPPER(C1020_COMPANY_NM) LIKE UPPER('%'||'" + strCompanyname
          + "'||'%') ");
    }
    if (!strStartmonth.equals("")) {
      sbQuery.append(" AND  C901_START_MONTH = '" + strStartmonth + "' ");
    }
    if (!strStartyear.equals("")) {
      sbQuery.append(" AND  C1020_START_YEAR = '" + strStartyear + "' ");
    }
    if (!strEndmonth.equals("")) {
      sbQuery.append(" AND  C901_END_MONTH = '" + strEndmonth + "' ");
    }
    if (!strEndyear.equals("")) {
      sbQuery.append(" AND  C1020_END_YEAR = '" + strEndyear + "' ");
    }
    if (!strStillinposition.equals("")) {
      sbQuery.append(" AND  C1020_STILL_IN_THIS_POSITION = '" + strStillinposition + "' ");
    }
    if (!strGroupdivision.equals("")) {
      sbQuery.append(" AND  UPPER(C1020_GROUP_DIVISION_NM) LIKE UPPER('%'||'" + strGroupdivision
          + "'||'%') ");
    }
    if (!strCountry.equals("")) {
      sbQuery.append(" AND  C901_COUNTRY= '" + strCountry + "' ");
    }
    if (!strState.equals("")) {
      sbQuery.append(" AND  C901_STATE_NM = '" + strState + "' ");
    }
    if (!strCity.equals("")) {
      sbQuery.append(" AND  UPPER(C1020_CITY_NM) LIKE UPPER('%'||'" + strCity + "'||'%') ");
    }
    sbQuery.append(" GROUP BY C101_PARTY_ID) T1020 ");
    sbQuery.append(" WHERE T101.C901_PARTY_TYPE = '7000' ");
    sbQuery.append(" AND T101.C101_ACTIVE_FL   IS NULL ");
    sbQuery.append(" AND T101.C101_VOID_FL     IS NULL ");
    sbQuery.append(" AND T101.C101_PARTY_ID = T1081.C101_PARTY_ID(+) ");
    sbQuery.append(" AND T101.C101_PARTY_ID = T1020.PARTYID ");
    sbQuery.append(" ORDER BY UPPER(FIRSTNM ||' '|| LASTNM) ");


    log.debug("fetchSurgeonProfessionalReport Query>>>>>" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("fetchSurgeonProfessionalReport alReturnBean Size>>>>>" + alReturn.size());

    return alReturn;

  }

  /**
   * Fetches CRM Surgeon Consultant Report
   * 
   * @param hmParams
   * @return ArrayList
   * @throws com.globus.common.beans.AppError
   */

  public ArrayList fetchSurgeonProjectRpt(HashMap hmParams) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();

    String strProjectID = GmCommonClass.parseNull((String) hmParams.get("PROJECTID"));
    String strProjectNm = GmCommonClass.parseNull((String) hmParams.get("PROJECTNAME"));
    String strRoleID = GmCommonClass.parseNull((String) hmParams.get("ROLEID"));
    sbQuery.append(" SELECT T101.C101_PARTY_ID PARTYID,");
    sbQuery.append(" T101.C101_FIRST_NM FIRSTNM, ");
    sbQuery.append(" T101.C101_LAST_NM LASTNM, ");
    sbQuery.append(" T101.C101_PARTY_NM PARTYNM, ");
    sbQuery.append(" T101.C101_MIDDLE_INITIAL MIDINITIAL, ");
    sbQuery.append(" T1081.C901_STATUS STATUS, ");
    sbQuery.append(" PROJ.PROJECTNM PROJECTNAME ");
    sbQuery.append(" FROM T101_PARTY T101, ");
    sbQuery
        .append(" (SELECT C901_STATUS, C101_PARTY_ID FROM T1081_PARTY_STATUS WHERE C1081_VOID_FL IS NULL) T1081, ");
    sbQuery.append(" (SELECT T204.c101_party_id PARTYID,");
    sbQuery
        .append("(RTRIM(XMLAGG(XMLELEMENT(E,T202.C202_PROJECT_ID|| ': ' || T202.C202_PROJECT_NM || ',')).EXTRACT('//text()').GETCLOBVAL(),',') ) PROJECTNM ");
    sbQuery.append("FROM T202_PROJECT T202, T204_PROJECT_TEAM T204 ");

    sbQuery.append("  WHERE T202.C202_VOID_FL IS NULL ");
    sbQuery.append(" AND T204.C204_VOID_FL   IS NULL ");
    sbQuery.append(" AND T204.C901_STATUS_ID != 92055 ");

    sbQuery.append(" AND T204.C202_PROJECT_ID = T202.C202_PROJECT_ID ");


    if (!strProjectID.equals("")) {
      sbQuery.append(" AND T202.C202_PROJECT_ID  = '" + strProjectID + "' ");
    }
    if (!strProjectNm.equals("")) {
      sbQuery.append(" AND  UPPER(T202.c202_PROJECT_NM) LIKE UPPER('%'||'" + strProjectNm
          + "'||'%') ");
    }
    if (!strRoleID.equals("")) {

      sbQuery.append(" AND  T204.C901_ROLE_ID = '" + strRoleID + "' ");
    }


    sbQuery.append(" GROUP BY T204.C101_PARTY_ID) PROJ ");
    sbQuery.append(" WHERE T101.C901_PARTY_TYPE='7000' ");
    sbQuery.append(" AND T101.C101_ACTIVE_FL IS NULL");
    sbQuery.append(" AND T101.C101_VOID_FL IS NULL");
    sbQuery.append(" AND T101.C101_PARTY_ID = T1081.C101_PARTY_ID(+) ");
    sbQuery.append(" AND T101.C101_PARTY_ID = PROJ.PARTYID ");
    sbQuery.append(" ORDER BY UPPER(FIRSTNM ||' '  || LASTNM)");

    log.debug("fetchSurgeonProjectRpt Query>>>>>" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("fetchSurgeonProjectRpt alReturnBean Size>>>>>" + alReturn.size());

    return alReturn;


  }



}
