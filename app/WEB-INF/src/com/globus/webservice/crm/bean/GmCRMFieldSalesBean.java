package com.globus.webservice.crm.bean;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmCRMFieldSalesBean {

  private static final String String = null;
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ArrayList fetchSalesRep(HashMap hmParams) throws AppError {

    ArrayList alReturn = new ArrayList();
    String strRepID = GmCommonClass.parseNull((String) hmParams.get("REPID"));
    String strFirstnm = GmCommonClass.parseNull((String) hmParams.get("FIRSTNM"));
    String strLastnm = GmCommonClass.parseNull((String) hmParams.get("LASTNM"));
    String strDistId =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("DISTID")));
    String strRegId =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("REGID")));
    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT T101.C101_PARTY_ID PARTYID, ");
    sbQuery.append(" T101.C101_FIRST_NM FIRSTNM, ");
    sbQuery.append(" T101.C101_LAST_NM LASTNM, ");
    sbQuery.append(" T703.C703_SALES_REP_NAME PARTYNM, ");
    sbQuery.append(" T101.C101_MIDDLE_INITIAL MIDINITIAL, ");

    sbQuery.append(" T703.C703_SALES_REP_ID REPID, ");
    sbQuery.append(" T703.C701_DISTRIBUTOR_ID DISTID, ");
    sbQuery.append(" GET_DISTRIBUTOR_NAME(T703.C701_DISTRIBUTOR_ID) DISTNAME, ");
    sbQuery.append(" GET_AD_REP_NAME(T703.C703_SALES_REP_ID) ADNAME, ");
    sbQuery.append(" REG.VP_NAME VPNAME, ");
    sbQuery.append(" T107A.PHONE PHONE, ");
    sbQuery.append(" T107A.EMAIL EMAIL, ");
    sbQuery.append(" T703.C703_REP_CATEGORY CATID, ");
    sbQuery.append(" GET_CODE_NAME(T703.C703_REP_CATEGORY) CATEGORY, ");
    sbQuery.append(" T703.C702_TERRITORY_ID TERID, ");
    sbQuery.append(" GET_TERRITORY_NAME(T703.C702_TERRITORY_ID) TERNAME, ");
    sbQuery.append(" REG.REGION_ID REGID, ");
    sbQuery.append(" GET_CODE_NAME(REG.REGION_ID) REGNM, ");
    sbQuery.append(" T703.C901_DESIGNATION DESIGNATION, ");
    sbQuery.append(" GET_CODE_NAME(T703.C901_DESIGNATION) TITLE ");

    sbQuery.append(" FROM T101_PARTY T101, T703_SALES_REP T703, ");
    sbQuery
        .append(" (SELECT V700.TER_ID, MAX(V700.VP_NAME) VP_NAME, MAX(V700.REGION_ID) REGION_ID FROM V700_TERRITORY_MAPPING_DETAIL V700 ");
    if (!strRegId.equals(""))
      sbQuery.append(" WHERE V700.REGION_ID IN ('" + strRegId + "') ");
    sbQuery.append(" GROUP BY V700.TER_ID) REG ");
    
	sbQuery.append(" ,(SELECT T107.C101_PARTY_ID, ");
	sbQuery.append(" MAX (DECODE (T107.C901_MODE, 90452, T107.C107_CONTACT_VALUE,'')) EMAIL, ");
	sbQuery.append(" MAX (DECODE (T107.C901_MODE, 90450, T107.C107_CONTACT_VALUE,'')) PHONE ");
	sbQuery.append(" FROM T107_CONTACT T107 ");
	sbQuery.append(" WHERE T107.C107_PRIMARY_FL = 'Y' ");
	sbQuery.append(" GROUP BY T107.C101_PARTY_ID ");
	sbQuery.append(" ) T107A ");
	
    sbQuery.append(" WHERE T101.C101_PARTY_ID    = T703.C101_PARTY_ID ");

    if (!strRegId.equals(""))
      sbQuery.append(" AND REG.TER_ID = T703.C702_TERRITORY_ID ");
    else
      sbQuery.append(" AND REG.TER_ID(+) = T703.C702_TERRITORY_ID ");

    sbQuery.append(" AND T703.C703_VOID_FL IS NULL ");
    sbQuery.append(" AND T107A.C101_PARTY_ID(+) =T101.C101_PARTY_ID ");
    sbQuery.append(" AND T101.C901_PARTY_TYPE IN ('7005','7006') ");
    sbQuery.append(" AND T101.C101_VOID_FL IS NULL ");

    if (!strFirstnm.equals("") || !strLastnm.equals(""))
      sbQuery.append(" AND  UPPER(T101.C101_FIRST_NM) LIKE UPPER('%'||'" + strFirstnm
          + "'||'%') AND UPPER(T101.C101_LAST_NM) LIKE UPPER('%'||'" + strLastnm + "'||'%') ");
    
    if (!strRepID.equals(""))
      sbQuery.append(" AND  T703.C703_SALES_REP_ID = '" + strRepID + "' ");

    if (!strDistId.equals(""))
      sbQuery.append(" AND  T703.C701_DISTRIBUTOR_ID IN ('" + strDistId + "') ");
    sbQuery.append(" AND UPPER(T101.C101_FIRST_NM) NOT LIKE ('%INACTIVE%')"); 
    sbQuery.append(" ORDER BY T101.C101_FIRST_NM ");

    log.debug("The Query2 is >>>>>" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;
  } 
  
  public ArrayList repLocation(HashMap hmParams) throws AppError {
	    
	ArrayList alreturn = new ArrayList();
	String strZip = GmCommonClass.parseNull((String) hmParams.get("ZIP"));
	String strState = GmCommonClass.parseNull((String) hmParams.get("STATE"));
	   
	GmDBManager gmDBManager = new GmDBManager();
	StringBuffer sbQuery = new StringBuffer();
	sbQuery.append(" SELECT unique t704.c704_bill_zip_code zip, t704.c704_bill_state state, v700.region_id regid, v700.VP_ID vpid, "
	  		+ " v700.vp_name vpname, v700.ad_id adid, v700.ad_name adname, v700.rep_id repid, v700.rep_name repname "
	  		+ " FROM t704_account t704, v700_territory_mapping_detail v700 WHERE t704.c704_account_id   = v700.ac_id  ");

	if (!strZip.equals("") && !strState.equals(""))
		sbQuery.append(" AND t704.c704_bill_zip_code = ('" + strZip + "') and t704.c704_bill_state = ('" + strState + "') and rownum = 1 ");
	
	else if (!strZip.equals(""))
      sbQuery.append(" AND t704.c704_bill_zip_code = ('" + strZip + "') and rownum = 1 ");
	    
	else if (!strState.equals(""))
      sbQuery.append(" and t704.c704_bill_state = ('" + strState + "') and rownum = 1 ");
    
    
	    
    log.debug("Query for LOCATION equal" + sbQuery.toString());

    alreturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alreturn);

	return alreturn;

  }

  public ArrayList repFName(HashMap hmParams) throws AppError {
    ArrayList alreturn = new ArrayList();
    String strRepFName = GmCommonClass.parseNull((String) hmParams.get("FIRSTNM"));
    String strDistId =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("DISTID")));
    String strRegId =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("REGID")));
    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT T101.C101_PARTY_ID PARTYID,  T101.C101_FIRST_NM FIRSTNM,  T101.C101_LAST_NM LASTNM, ");
    sbQuery
        .append(" T703.C703_SALES_REP_NAME PARTYNM,  T101.C101_MIDDLE_INITIAL MIDINITIAL, T703.C703_SALES_REP_ID REPID, ");
    sbQuery
        .append(" T703.C701_DISTRIBUTOR_ID DISTID,  GET_DISTRIBUTOR_NAME(T703.C701_DISTRIBUTOR_ID) DISTNAME,  T703.C703_PHONE_NUMBER PHONE, ");
    sbQuery
        .append(" T703.C703_EMAIL_ID EMAIL, T703.C703_REP_CATEGORY CATID,  GET_CODE_NAME(T703.C703_REP_CATEGORY) CATEGORY, ");
    sbQuery
        .append(" T703.C702_TERRITORY_ID TERID,  GET_TERRITORY_NAME(T703.C702_TERRITORY_ID) TERNAME,  REG.REGION_ID REGID, ");
    sbQuery
        .append(" GET_CODE_NAME(REG.REGION_ID) REGNM, T703.C901_DESIGNATION DESIGNATION, GET_CODE_NAME(T703.C901_DESIGNATION) TITLE ");
    sbQuery
        .append(" FROM T101_PARTY T101, T703_SALES_REP T703, (SELECT V700.TER_ID, MAX(V700.REGION_ID) REGION_ID ");
    sbQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700 ");

    if (!strRegId.equals(""))
      sbQuery.append(" WHERE V700.REGION_ID IN ('" + strRegId + "') ");
    sbQuery.append(" GROUP BY V700.TER_ID) REG ");
    sbQuery.append(" WHERE T101.C101_PARTY_ID    = T703.C101_PARTY_ID ");

    if (!strRegId.equals(""))
      sbQuery.append(" AND REG.TER_ID = T703.C702_TERRITORY_ID ");
    else
      sbQuery.append(" AND REG.TER_ID(+) = T703.C702_TERRITORY_ID ");
    sbQuery
        .append(" AND T703.C703_VOID_FL IS NULL  AND T101.C901_PARTY_TYPE = '7005' AND T101.C101_VOID_FL IS NULL");
    if (!strRepFName.equals(""))
      sbQuery.append(" AND  UPPER(T101.C101_FIRST_NM) = UPPER('" + strRepFName + "') ");

    if (!strDistId.equals(""))
      sbQuery.append(" AND  T703.C701_DISTRIBUTOR_ID IN ('" + strDistId + "') ");
    log.debug("Query for first name equal" + sbQuery.toString());

    alreturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
    log.debug("alReturnBean>>>>>" + alreturn);

    return alreturn;

  }

  public ArrayList fetchAccountMap(String strRepId) {

    ArrayList alReturn = new ArrayList();
    strRepId = GmCommonClass.getStringWithQuotes(strRepId);

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT C704_ACCOUNT_ID ACCID, ");
    sbQuery.append(" C704_ACCOUNT_NM ACCNM, ");
    sbQuery.append(" C703_SALES_REP_ID REPID, ");
    sbQuery.append(" GET_REP_NAME(C703_SALES_REP_ID) REPNM, ");
    sbQuery.append(" GET_DISTRIBUTOR_NAME(GET_DISTRIBUTOR_ID(C703_SALES_REP_ID)) DISTNM, "); 
    sbQuery.append(" GET_AD_REP_NAME(C703_SALES_REP_ID) ADNM, ");
    sbQuery.append(" GET_AD_REP_ID(C703_SALES_REP_ID) ADID, ");
    sbQuery.append(" GET_VP_NAME(C703_SALES_REP_ID) VPNM, ");
    sbQuery.append(" C704_BILL_LATITUDE LAT, ");
    sbQuery.append(" C704_BILL_LONGITUDE LON ");
    sbQuery.append(" FROM T704_ACCOUNT ");
    sbQuery.append(" WHERE C704_VOID_FL IS NULL ");

    if (!strRepId.equals(""))
      sbQuery.append(" AND C703_SALES_REP_ID IN ('" + strRepId + "') ");



    log.debug("The Query2 is >>>>>" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    return alReturn;
  }
  
  public ArrayList fetchRegTerDis(HashMap hmParams) throws AppError {
	    
		ArrayList alreturn = new ArrayList();
		String strRepId = GmCommonClass.parseNull((String) hmParams.get("REPID"));
		String strRegion = GmCommonClass.parseNull((String) hmParams.get("REGID"));
		String strDist = GmCommonClass.parseNull((String) hmParams.get("DISTID"));
		String strTer = GmCommonClass.parseNull((String) hmParams.get("TERID"));
		   
		GmDBManager gmDBManager = new GmDBManager();
		StringBuffer sbQuery = new StringBuffer();
		
		if (!strRegion.equals(""))
			sbQuery.append(" SELECT UNIQUE v700.REGION_NAME REGNM, v700.REGION_ID REGID ");
		else if (!strDist.equals(""))
			sbQuery.append(" SELECT UNIQUE v700.D_NAME DISTNAME, v700.D_ID DISTID ");
		else if (!strTer.equals(""))
			sbQuery.append(" SELECT UNIQUE v700.TER_NAME TERNAME, v700.TER_ID TERID ");
		
		sbQuery.append(" FROM v700_territory_mapping_detail v700 "
				+ " WHERE (v700.AD_ID = '" + strRepId + "' or v700.VP_ID = '" + strRepId + "') "); 
		
		if (!strTer.equals("")) {
			sbQuery.append(" and V700.TER_NAME NOT IN ('^ No Territory') ");
			sbQuery.append(" and UPPER(V700.TER_NAME) NOT LIKE ('INACTIVE%') ");
			sbQuery.append(" ORDER BY V700.TER_NAME ");
		} else if (!strDist.equals("")) {
			sbQuery.append(" AND UPPER(DISTTYPENM) = 'DISTRIBUTOR' ");
			sbQuery.append(" and (UPPER(V700.D_NAME) NOT LIKE ('%INACTIVE%') AND UPPER(V700.D_NAME) NOT LIKE ('%UAD%')) ");
			sbQuery.append(" ORDER BY V700.D_NAME ");
		} else if (!strRegion.equals("")) {
			sbQuery.append(" ORDER BY V700.REGION_NAME ");
		}	    
		    
	    log.debug("Query to get Region / Territory / Distributor" + sbQuery.toString());

	    alreturn =
	        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

	    log.debug("alReturnBean>>>>>" + alreturn);

		return alreturn;

	  }

}
