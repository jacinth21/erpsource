package com.globus.webservice.crm.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmWSUtil;
import com.globus.webservice.crm.valueobject.GmCRMTrainingListVO;

public class GmCRMTrainingBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ArrayList fetchTrainingReport(HashMap hmParams) throws AppError {
    ArrayList alReturn = new ArrayList();
    String strTrainingID = GmCommonClass.parseNull((String) hmParams.get("TRID"));
    String strrepID = GmCommonClass.parseNull((String) hmParams.get("TRSALESREPID"));
    String strStatus = GmCommonClass.parseNull((String) hmParams.get("TRSTATUS"));

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append("SELECT T6650.C6650_TRAINING_ID TRID, T6650.C6650_TITLE TRTITLE, T6650.C703_SALES_REP_ID TRSALESREPID, T6650.C6650_STATUS TRSTATUS, T6650.C6650_DATE TRDATE, T6650.C6650_COMMENTS TRCOMMENTS, t703.c703_sales_rep_name TRSALESREPNAME FROM T6650_TRAINING T6650 , t703_sales_rep t703 ");
    sbQuery
        .append("WHERE t6650.c703_sales_rep_id = t703.c703_sales_rep_id AND t6650.c6650_void_fl IS NULL AND t703.c703_void_fl IS NULL");
    if (!strTrainingID.equals(""))
      sbQuery.append(" AND T6650.C6650_TRAINING_ID = '" + strTrainingID + "' ");

    if (!strrepID.equals(""))
      sbQuery.append(" AND T6650.C703_SALES_REP_ID = '" + strrepID + "' ");

    if (strStatus.equals("PENDING"))
      sbQuery.append(" AND LOWER(T6650.C6650_STATUS) NOT LIKE '%complete%' ");

    log.debug("Query=" + sbQuery.toString());


    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;

  }

  public void saveTraining(ArrayList alReturn) throws Exception {
    GmDBManager gmDBManager = new GmDBManager();
    int length = alReturn.size();
    for (int i = 0; i < length; i++) {
      HashMap hmParam = (HashMap) alReturn.get(i);
      saveTrainingDetails(hmParam, gmDBManager);
    }

    gmDBManager.commit();
  }

  public void saveTrainingDetails(HashMap hmParam, GmDBManager gmDBManager) throws Exception {
    GmCommonBean gmCommonBean = new GmCommonBean();
    String strid = GmCommonClass.parseNull((String) hmParam.get("TRID"));
    String strtitle = GmCommonClass.parseNull((String) hmParam.get("TRTITLE"));
    String strdate = GmCommonClass.parseNull((String) hmParam.get("TRDATE"));
    String strrepid = GmCommonClass.parseNull((String) hmParam.get("TRSALESREPID"));
    String strstatus = GmCommonClass.parseNull((String) hmParam.get("TRSTATUS"));
    String strpartyid = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("TRCOMMENTS"));
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));

    Date dttrainingDate = null;
    dttrainingDate = GmCommonClass.getStringToDate(strdate, strApplDateFmt);

    gmDBManager.setPrepareString("gm_pkg_crm_training.gm_sav_training", 7);
    gmDBManager.setString(1, strid);
    gmDBManager.setString(2, strtitle);
    gmDBManager.setString(3, strrepid);
    gmDBManager.setString(4, strstatus);
    gmDBManager.setDate(5, dttrainingDate);
    gmDBManager.setString(6, strComments);
    gmDBManager.setString(7, strpartyid);
    gmDBManager.execute();
  }


  public ArrayList populateTrainingFromJSON(GmCRMTrainingListVO[] alOrderVO,
      GmCRMTrainingListVO gmOrderVO, String strPartyId) throws AppError {

    ArrayList alResult = new ArrayList();
    int length = alOrderVO.length;
    GmWSUtil gmWSUtil = new GmWSUtil();
    for (int i = 0; i < length; i++) {
      GmCRMTrainingListVO gmOrderparamVO = alOrderVO[i];
      HashMap hmOrder = new HashMap();
      hmOrder.put("TRID", GmCommonClass.parseNull(gmOrderparamVO.getTrid()));
      hmOrder.put("TRTITLE", GmCommonClass.parseNull(gmOrderparamVO.getTrtitle()));
      hmOrder.put("TRDATE", GmCommonClass.parseNull(gmOrderparamVO.getTrdate()));
      hmOrder.put("TRSALESREPID", GmCommonClass.parseNull(gmOrderparamVO.getTrsalesrepid()));
      hmOrder.put("TRSALESREPNAME", GmCommonClass.parseNull(gmOrderparamVO.getTrsalesrepname()));
      hmOrder.put("TRSTATUS", GmCommonClass.parseNull(gmOrderparamVO.getTrstatus()));
      hmOrder.put("PARTYID", strPartyId);
      hmOrder.put("TRCOMMENTS", GmCommonClass.parseNull(gmOrderparamVO.getTrcomments()));
      alResult.add(hmOrder);
    }

    return alResult;
  }


}
