package com.globus.webservice.crm.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmWSUtil;
import com.globus.webservice.crm.valueobject.GmLeadBasicVO;

public class GmLeadBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public List<GmLeadBasicVO> fetchLeadReport(HashMap hmParams, GmLeadBasicVO gmLeadBasicVO)
      throws AppError {

    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    List<GmLeadBasicVO> listGmLeadBasicVO = new ArrayList<GmLeadBasicVO>();

    ArrayList alOutput = new ArrayList();
    GmWSUtil gmWSUtil = new GmWSUtil();
    String strLeadid = GmCommonClass.parseNull((String) hmParams.get("LEADID"));
    String strRepid = GmCommonClass.parseNull((String) hmParams.get("REPID"));
    String strState = GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("STATE")));
    String strProcedure = GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("PROCEDURE")));
    String strSystemid = GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("SYSTEMID")));
    String strFrmDate = GmCommonClass.parseNull((String) hmParams.get("FRMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParams.get("TODATE"));
    String strAreaUserId = GmCommonClass.parseNull((String) hmParams.get("AREAUSERID"));
    String strRegId =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("REGID")));


    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT T6640.C6640_LEAD_ID LEADID, T6640.C101_SURGEON_PARTY_ID PARTYID, T6640.C901_SOURCE SOURCEID, GET_CODE_NAME(T6640.C901_SOURCE) SOURCENM, ");
    sbQuery
        .append(" T6640.C901_RELATIONSHIP RELATIONID, GET_CODE_NAME(T6640.C901_RELATIONSHIP) RELATIONNM, T6640.C901_CHANNEL CHANNEL, T6640.C6640_LEAD_GENERATED_ON GENERATEDON, ");
    sbQuery
        .append(" T6640.C101_REP_PARTY_ID REPID, GET_REP_NAME(T6640.C101_REP_PARTY_ID) REPNM, T101.C101_FIRST_NM || ' ' || T101.C101_LAST_NM PARTYNM, T107A.EMAIL EMAIL, T107A.PHONE PHONE");
    
    if (!strAreaUserId.equals(""))
      sbQuery.append(",  address.address ");
    
    sbQuery.append(" FROM T6640_LEAD T6640, T101_PARTY T101, ");
    sbQuery
        .append(" (SELECT T107.C101_PARTY_ID, MAX (DECODE (T107.C901_MODE, 90452, T107.C107_CONTACT_VALUE)) EMAIL, MAX (DECODE (T107.C901_MODE, 90450, T107.C107_CONTACT_VALUE)) PHONE FROM T107_CONTACT T107 WHERE T107.C107_PRIMARY_FL = 'Y' GROUP BY T107.C101_PARTY_ID ) T107A ");
    if (!strState.equals(""))
      sbQuery
          .append(" ,(SELECT T106.C101_PARTY_ID FROM T106_ADDRESS T106 WHERE T106.C106_VOID_FL IS NULL AND T106.C106_PRIMARY_FL = 'Y' AND T106.C901_STATE IN ( '" + strState + "' ) ) ADDR " );
    
    if (!strProcedure.equals("")) {
        sbQuery.append(" ,(SELECT T6610.C101_PARTY_ID FROM T6610_SURGICAL_ACTIVITY T6610 ");
        sbQuery.append(" WHERE T6610.C101_PARTY_ID IS NOT NULL ");
  		 if (!strProcedure.equals(""))
          sbQuery.append("  AND T6610.C901_PROCEDURE IN ('" + strProcedure + "') ");
  		sbQuery.append(" GROUP BY T6610.C101_PARTY_ID) SURG_ACT ");
    }
    if ( !strSystemid.equals("")) {
        sbQuery.append(" ,(SELECT T6610.C101_PARTY_ID FROM T6610_SURGICAL_ACTIVITY T6610 ");
        sbQuery.append(" WHERE T6610.C101_PARTY_ID IS NOT NULL ");
              if (!strProcedure.equals(""))
          		sbQuery.append("  AND T6610.C901_PROCEDURE IN ('" + strSystemid + "') ");
       
        sbQuery.append(" GROUP BY T6610.C101_PARTY_ID) SURG_ACTT ");
      } 
    
    if (!strAreaUserId.equals("") || !strRegId.equals("")) {
      sbQuery
          .append(" , (SELECT DISTINCT T106.C101_PARTY_ID, T106.C901_STATE FROM T106_ADDRESS T106 WHERE C106_VOID_FL IS NULL AND T106.C901_STATE IN ");
      sbQuery
          .append(" ( SELECT DISTINCT T704.C704_BILL_STATE FROM v700_territory_mapping_detail v700, t704_account T704 WHERE T704.C704_ACCOUNT_ID = V700.AC_ID ");
      sbQuery.append(" AND T704.C704_VOID_FL IS NULL AND v700.region_id IN ");
      if (!strRegId.equals(""))
        sbQuery.append(" ('" + strRegId + "') ");
      
      if (!strAreaUserId.equals(""))
        sbQuery.append(" (SELECT c901_region_id  FROM T708_REGION_ASD_MAPPING WHERE c708_delete_fl IS NULL AND c708_inactive_fl IS NULL AND C101_USER_ID = '"
                + strAreaUserId + "') ");
      
      sbQuery.append(" )) AREA ");
      
      if (!strAreaUserId.equals(""))
          sbQuery.append(", (SELECT address, c101_party_id FROM (SELECT C106_ADD1 || ', ' || c106_city"
      		+ "|| ', '|| get_code_name(c901_state) address, c101_party_id, ROW_NUMBER() OVER (PARTITION BY c101_party_id ORDER BY c106_primary_fl, c106_seq_no ASC) AS rownos "
      		+ "FROM t106_address) WHERE rownos = 1  ) address");
    }
    sbQuery
        .append(" WHERE T101.C101_PARTY_ID   = T6640.C101_SURGEON_PARTY_ID AND T107A.C101_PARTY_ID(+) =T6640.C101_SURGEON_PARTY_ID ");
    if (!strAreaUserId.equals("") || !strRegId.equals(""))
      sbQuery.append(" AND AREA.C101_PARTY_ID = T6640.C101_SURGEON_PARTY_ID ");
    if (!strState.equals(""))
      sbQuery.append(" AND ADDR.C101_PARTY_ID     = T6640.C101_SURGEON_PARTY_ID ");
    if (!strProcedure.equals(""))
        sbQuery.append(" AND SURG_ACT.C101_PARTY_ID = T6640.C101_SURGEON_PARTY_ID ");
    if ( !strSystemid.equals("") )
        sbQuery.append(" AND SURG_ACTT.C101_PARTY_ID   =T101.C101_PARTY_ID ");
    if (!strRepid.equals(""))
      sbQuery.append(" AND T6640.C101_REP_PARTY_ID IN ('" + strRepid + "') ");
    if (!strFrmDate.equals(""))
      sbQuery.append(" AND T6640.C6640_LEAD_GENERATED_ON > TO_DATE('" + strFrmDate
          + "','MM/DD/YYYY') ");
    if (!strToDate.equals(""))
      sbQuery.append(" AND T6640.C6640_LEAD_GENERATED_ON < TO_DATE('" + strToDate
          + "','MM/DD/YYYY') ");

    if (!strAreaUserId.equals(""))
        sbQuery.append(" AND address.c101_party_id(+) = T101.C101_PARTY_ID ");
    
    sbQuery.append(" AND T101.C101_VOID_FL IS NULL AND T6640.C6640_VOID_FL   IS NULL ");
    if (!strLeadid.equals(""))
      sbQuery.append(" AND T6640.C6640_LEAD_ID = '" + strLeadid + "' ");

    log.debug("Query=" + sbQuery.toString());

    alOutput =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("Result data for lead" + alOutput);



    listGmLeadBasicVO =
        gmWSUtil.getVOListFromHashMapList(alOutput, gmLeadBasicVO,
            gmLeadBasicVO.getMappingProperties());



    return listGmLeadBasicVO;


  }

  public String saveLeadInfo(HashMap hmParams, GmDBManager gmDBManager) throws Exception, AppError {



    String strLeadId = new String();

    String genDate = GmCommonClass.parseNull((String) hmParams.get("GENERATEDON"));
    String strDateFmt = GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT");
    Date generatedDate = GmCommonClass.getStringToDate(genDate, strDateFmt);

    gmDBManager.setPrepareString("gm_pkg_crm_lead.gm_crm_sav_lead", 9);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("LEADID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("PARTYID")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParams.get("RELATIONID")));
    gmDBManager.setDate(4, generatedDate);
    gmDBManager.setString(5, GmCommonClass.parseNull((String) hmParams.get("SOURCEID")));
    gmDBManager.setString(6, GmCommonClass.parseNull((String) hmParams.get("CHANNEL")));
    gmDBManager.setString(7, GmCommonClass.parseNull((String) hmParams.get("REPID")));
    gmDBManager.setString(8, GmCommonClass.parseNull((String) hmParams.get("VOIDFL")));
    gmDBManager.setString(9, GmCommonClass.parseNull((String) hmParams.get("USERID")));

    gmDBManager.execute();

    strLeadId = GmCommonClass.parseNull(gmDBManager.getString(1));


    return strLeadId;
  }

  public String assignRep(HashMap hmParams) {
    GmDBManager gmDBManager = new GmDBManager();

    String strRepNm = new String();

    String strLeadID = GmCommonClass.parseNull((String) hmParams.get("LEADID"));
    String strRepId = GmCommonClass.parseNull((String) hmParams.get("REPID"));

    gmDBManager.setPrepareString("gm_pkg_crm_lead.gm_crm_assign_rep", 4);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strLeadID);
    gmDBManager.setString(2, strRepId);
    gmDBManager.setString(3, "");
    gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParams.get("USERID")));

    gmDBManager.execute();

    strRepNm = GmCommonClass.parseNull(gmDBManager.getString(3));
    log.debug("strRepNm >>> " + strRepNm);
    gmDBManager.commit();
    return strRepNm;
  }

}
