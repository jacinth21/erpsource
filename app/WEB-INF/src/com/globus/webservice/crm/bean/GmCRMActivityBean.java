package com.globus.webservice.crm.bean;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.common.util.GmWSUtil;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.webservice.crm.valueobject.GmCRMActivityAttrVO;
import com.globus.webservice.crm.valueobject.GmCRMActivityPartyVO;
import com.globus.webservice.crm.valueobject.GmCRMActivityVO;
import com.globus.webservice.crm.valueobject.GmCRMLogVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonAddressListVO;

public class GmCRMActivityBean extends GmSalesFilterConditionBean implements GmCRMCalendarInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ArrayList fetchActivityList(HashMap hmParams) throws AppError {

    ArrayList alReturn = new ArrayList();
    HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
    String strUserDeptId = GmCommonClass.parseNull((String) hmParams.get("USERDEPTID"));
    initializeParameters(hmParam);

    String strCategory = GmCommonClass.parseNull((String) hmParams.get("ACTCATEGORY"));
    String strActNm = GmCommonClass.parseNull((String) hmParams.get("ACTNM"));
    String strPartyId =
        GmCommonClass
            .getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("PARTYID")));
    String strType =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams
            .get("ACTTYPEID")));
    String strFrmDate =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams
            .get("ACTSTARTDATE")));
    String strToDate =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams
            .get("ACTENDDATE")));
    String strState =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("STATE")));
    String strProcedure =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams
            .get("PROCEDURE")));
    String strSystemid =
        GmCommonClass
            .getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("SYSTEMID")));
    String strStatus =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams
            .get("ACTSTATUS")));
    String strCrfStatus =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams
            .get("CRFSTATUS")));
    log.debug(hmParams);
    String strFirstNm = GmCommonClass.parseNull((String) hmParams.get("FIRSTNM"));
    String strLastNm = GmCommonClass.parseNull((String) hmParams.get("LASTNM"));

    String strCreatedBy = GmCommonClass.parseNull((String) hmParams.get("CREATEDBYID"));
    String strPassName = GmCommonClass.parseNull((String) hmParams.get("PASSNAME"));
    String strPartyid = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
    String strAreaUserID = GmCommonClass.parseNull((String) hmParams.get("AREAUSERID"));
    String strSurgPartyID = GmCommonClass.parseNull((String) hmParams.get("SURGPARTYID"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));


    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT DECODE(C6630_CREATED_BY,'" + strUserID
        + "','Y','N') ACCESSFL, T6630.C6630_ACTIVITY_ID ACTID, "
        + " T6630.C6630_ACTIVITY_NM ACTNM, " + " NVL(SURGEONS.NAMES, VISITORS.Names) PHYNM, "
        + " T6630.C6630_FROM_DT ACTSTARTDATE, " + " T6630.C6630_TO_DT ACTENDDATE, "
        + "T6630.C6630_CREATED_DATE CREATEDDATE, "
        + " GET_CODE_NAME(T6630.C901_ACTIVITY_TYPE) ACTTYPE, T6630.C901_ACTIVITY_TYPE ACTTYPEID, "
        + " LOCATION.LOCATION");

    sbQuery
        .append(" , get_code_name(t6630.c901_activity_status) actstatus , t6630.c901_activity_status actstatusid ");


    if (strCategory.equals("105265")) // MERC
      sbQuery.append(" ,SURGEONS.IDS PARTYID, PENDINGMERC.ATTRPARTYID ");

    if (strCategory.equals("105267") || strCategory.equals("105269")) // SALES CALL and Lead PRODUCT
      sbQuery.append(" ,PRODUCT.PRODUCT PRODUCTS");

    sbQuery
        .append(" ,TO_CHAR(CAST(T6630.C6630_FROM_TM AS TIMESTAMP WITH TIME ZONE),'HH24:MI') ACTSTARTTIME ");
    sbQuery
        .append(" ,TO_CHAR(CAST(T6630.C6630_TO_TM AS TIMESTAMP WITH TIME ZONE),'HH24:MI') ACTENDTIME ");

    if (strCategory.equals("105269") || strCategory.equals("105267"))
      sbQuery.append(" ,SURGNM.FIRSTNM, SURGNM.LASTNM, SURGNM.PARTYID ");

    if (strCategory.equals("105267"))
      sbQuery.append(" ,picinfo.picurl ");

    if (strCategory.equals("105267")) // SALES CALL RESULT
      sbQuery.append(" ,RESULT.RESULT ");

    if (strCategory.equals("105269")) // Lead
      sbQuery.append(" ,Source.c6632_attribute_NM Source, t6630.c6630_party_nm ASSIGNEE ");

    sbQuery.append("FROM T6630_ACTIVITY T6630 ");

    if (!strCrfStatus.equals(""))
      sbQuery.append(" ,T6605_CRF T6605 ");


    sbQuery
        .append(", (SELECT T6631.C6630_ACTIVITY_ID, LISTAGG(get_crm_party_name(T6631.C101_PARTY_ID), ' / ') WITHIN GROUP (ORDER BY T6631.C101_PARTY_ID) AS NAMES, LISTAGG(T6631.C101_PARTY_ID, ' / ') WITHIN GROUP "
            + " ( ORDER BY T6631.C101_PARTY_ID) AS IDS "
            + "FROM T6631_ACTIVITY_PARTY_ASSOC T6631 WHERE T6631.C901_ACTIVITY_PARTY_ROLE IN ('105564', '105562', '105563') and t6631.c6631_void_fl is null "
            + "GROUP BY T6631.C6630_ACTIVITY_ID) SURGEONS");

    sbQuery
        .append(", (SELECT T6632.C6630_ACTIVITY_ID, LISTAGG(T6632.c6632_attribute_value,' / ' ) WITHIN GROUP "
            + "(ORDER BY T6632.c6632_attribute_value) AS Names "
            + "FROM T6632_ACTIVITY_ATTRIBUTE T6632 "
            + "WHERE T6632.C6632_VOID_FL IS NULL "
            + "AND T6632.c901_attribute_type = '105590' GROUP BY T6632.C6630_ACTIVITY_ID) VISITORS ");

    if (strCategory.equals("105269") || strCategory.equals("105267")) // Lead
      sbQuery
          .append(" ,(SELECT T6631.C6630_ACTIVITY_ID ACTID,T101.C101_FIRST_NM FIRSTNM, T101.C101_LAST_NM LASTNM, T101.C101_PARTY_ID PARTYID FROM T6631_ACTIVITY_PARTY_ASSOC "
              + " T6631, T101_PARTY T101 WHERE T6631.C901_ACTIVITY_PARTY_ROLE = '105564' AND t6631.c6631_void_fl IS NULL AND T6631.C101_PARTY_ID "
              + " = T101.C101_PARTY_ID(+)) SURGNM ");

    if (strCategory.equals("105269")) // Lead
      sbQuery
          .append(", (select c6630_activity_id, c6632_attribute_NM from t6632_activity_attribute where c901_attribute_type = '105580') Source ");


    if (!strAreaUserID.equals(""))
      sbQuery
          .append(" ,(SELECT UNIQUE ADActivityMapping.actid FROM t704_account t704, v700_territory_mapping_detail v700, (SELECT unique "
              + " t6632.c6630_activity_id actid, t106.c106_zip_code zipcode, t106.c901_state state FROM t6632_activity_attribute t6632, t106_address t106 WHERE t6632.c901_attribute_type = '7777' "
              + " AND t6632.c6632_attribute_value = t106.c106_address_id) ADActivityMapping WHERE UPPER(GET_REP_NAME(t704.c703_sales_rep_id)) = UPPER(v700.rep_name) "
              + " AND (v700.AD_ID = '"
              + strAreaUserID
              + "' OR v700.VP_ID = '"
              + strAreaUserID
              + "') and (ADActivityMapping.zipcode = t704.c704_bill_zip_code OR ADActivityMapping.state "
              + " = t704.c704_ship_state)) areauser ");

    if (strCategory.equals("105265"))
      sbQuery
          .append(" ,(SELECT T6632.C6630_ACTIVITY_ID ACTID, LISTAGG(T6632.C6632_ATTRIBUTE_VALUE, ' / ') WITHIN GROUP ( ORDER BY T6632.C6632_ATTRIBUTE_VALUE) AS ATTRPARTYID "
              + " FROM t6632_activity_attribute T6632 WHERE T6632.C901_ATTRIBUTE_TYPE = '105589' AND t6632.c6632_void_fl IS NULL GROUP BY T6632.C6630_ACTIVITY_ID) PENDINGMERC ");

    if (!strSurgPartyID.equals(""))
      sbQuery
          .append(" , (select C6630_ACTIVITY_ID, c101_party_id from t6631_activity_party_assoc where c901_activity_party_role  IN ('105564', '105562', '105563') AND c6631_void_fl IS NULL) t6631 ");

    if (!strSystemid.equals(""))
      sbQuery
          .append(" ,(SELECT T6632.C6630_ACTIVITY_ID ACTID FROM t6632_activity_attribute T6632 WHERE T6632.C901_ATTRIBUTE_TYPE = '105582' AND t6632.C6632_VOID_FL IS NULL "
              + " AND T6632.C6632_ATTRIBUTE_VALUE IN ('"
              + strSystemid
              + "') GROUP BY T6632.C6630_ACTIVITY_ID) PDTS ");

    if (!strProcedure.equals(""))
      sbQuery
          .append(" ,(SELECT T6632.C6630_ACTIVITY_ID ACTID FROM t6632_activity_attribute T6632 WHERE T6632.C901_ATTRIBUTE_TYPE = '105588' AND t6632.C6632_VOID_FL IS NULL "
              + " AND T6632.C6632_ATTRIBUTE_VALUE IN ('"
              + strProcedure
              + "') GROUP BY T6632.C6630_ACTIVITY_ID) TECHS ");

    if (!strState.equals(""))
      sbQuery
          .append(" ,(SELECT t6632.c6630_activity_id actid, t106.c901_state state FROM t6632_activity_attribute t6632, t106_address t106 WHERE t6632.c901_attribute_type = '7777' "
              + " AND t106.c106_address_id = t6632.c6632_attribute_value AND t106.c901_state IN ('"
              + strState + "')) STATE ");

    if (strCategory.equals("105267")) // SALES CALL RESULT
      sbQuery
          .append(" ,(SELECT c6630_activity_id ACTID, c6632_attribute_nm RESULT FROM t6632_activity_attribute WHERE c6632_void_fl IS NULL AND c901_attribute_type = '105581') RESULT ");

    if (strCategory.equals("105267") || strCategory.equals("105269")) // SALES CALL PRODUCT
      sbQuery
          .append(" ,(SELECT RTRIM (XMLAGG (XMLELEMENT (N, c6632_attribute_nm, ', ')) .EXTRACT ('//text()'), ', ') PRODUCT, c6630_activity_id ACTID FROM t6632_activity_attribute "
              + " WHERE c901_attribute_type = '105582' and c6632_void_fl is null GROUP BY C6630_ACTIVITY_ID) PRODUCT ");

    if (strCategory.equals("105267"))
      sbQuery
          .append(" ,(SELECT  distinct C903_REF_ID REFID, C901_REF_GRP ||'/' || C903_REF_ID ||'/' || C903_FILE_NAME PICURL FROM T903_UPLOAD_FILE_LIST WHERE C903_DELETE_FL IS NULL "
              + " AND C901_REF_GRP = '103112' AND C903_DELETE_FL IS NULL AND C901_REF_TYPE IS NULL) picinfo ");

    if (!strFirstNm.equals("") || !strLastNm.equals(""))
      sbQuery
          .append(" , (select distinct t101.c101_first_nm fn, t101.c101_last_nm ln, T6631.C6630_ACTIVITY_ID id from t101_party t101, T6631_ACTIVITY_PARTY_ASSOC T6631 where t101.c101_party_id "
              + " = T6631.c101_party_id and T6631.C901_ACTIVITY_PARTY_ROLE IN ('105564', '105562', '105563') AND t6631.c6631_void_fl IS NULL) names ");

    sbQuery
        .append("  ,(SELECT t6632.c6630_activity_id ACTID,  t106.c106_city || ' ' || GET_CODE_NAME_ALT(t106.c901_state) "
            + "  LOCATION FROM t6632_activity_attribute T6632, t106_address T106 WHERE t6632.c901_attribute_type = '7777' AND t6632.c6632_attribute_value = t106.c106_address_id "
            + " AND t6632.c6632_void_fl IS NULL AND t106.c106_void_fl IS NULL) LOCATION ");
    if ((strCategory.equals("105266") || strCategory.equals("105267")) && strUserDeptId.equals("S")) { // Physician
                                                                                                       // Visit
                                                                                                       // and
                                                                                                       // Sales
      // user
      sbQuery
          .append(",( SELECT DISTINCT T6631.C6630_ACTIVITY_ID, 'Y' ACCESSFL FROM T6631_ACTIVITY_PARTY_ASSOC T6631, T703_SALES_REP T703 ");
      sbQuery.append(getAccessFilterClauseWithRepID());
      sbQuery
          .append("WHERE T703.C101_PARTY_ID = T6631.C101_PARTY_ID AND T703.C703_VOID_FL IS NULL  AND t6631.c6631_void_fl IS NULL AND T703.C703_SALES_REP_ID = V700.REP_ID ) V700 ");
    }

    sbQuery
        .append(" WHERE  T6630.C6630_VOID_FL IS NULL AND T6630.C6630_ACTIVITY_ID = VISITORS.C6630_ACTIVITY_ID(+) ");

    sbQuery.append(" AND T6630.C6630_ACTIVITY_ID = SURGEONS.C6630_ACTIVITY_ID(+) ");



    if ((strCategory.equals("105266") || strCategory.equals("105267")) && strUserDeptId.equals("S")) { // Physician
                                                                                                       // Visit
                                                                                                       // and
                                                                                                       // Sales
      // user
      sbQuery.append(" AND T6630.C6630_ACTIVITY_ID  = v700.C6630_ACTIVITY_ID ");
    }

    if (!strFirstNm.equals("") || !strLastNm.equals(""))
      sbQuery.append(" and T6630.C6630_ACTIVITY_ID = names.id(+) ");

    if (strCategory.equals("105267")) // SALES CALL RESULT
      sbQuery.append(" AND T6630.C6630_ACTIVITY_ID = RESULT.ACTID(+) ");

    if (strCategory.equals("105269") || strCategory.equals("105267"))
      sbQuery.append(" AND T6630.C6630_ACTIVITY_ID = SURGNM.ACTID(+) ");

    if (!strCategory.equals(""))
      sbQuery.append("AND T6630.C901_ACTIVITY_CATEGORY = '" + strCategory + "' ");

    if (!strType.equals(""))
      sbQuery.append("AND T6630.C901_ACTIVITY_TYPE IN ('" + strType + "') ");

    if (!strFrmDate.equals(""))
      sbQuery.append(" AND T6630.C6630_FROM_DT >= TO_DATE('" + strFrmDate + "','MM/DD/YYYY') ");
    if (!strToDate.equals(""))
      sbQuery.append(" AND T6630.C6630_TO_DT <= TO_DATE('" + strToDate + "','MM/DD/YYYY') ");

    if (!strFirstNm.equals(""))
      sbQuery.append("AND UPPER(NAMES.fn) LIKE UPPER('%' ||'" + strFirstNm.replace("'","''") + "' ||'%') ");

    if (!strLastNm.equals(""))
      sbQuery.append("AND UPPER(NAMES.ln) LIKE UPPER('%' ||'" + strLastNm.replace("'","''") + "' ||'%') ");

    if (!strPartyid.equals(""))
      sbQuery.append(" and T6630.C6630_PARTY_ID = '" + strPartyid + "' ");

    if (!strAreaUserID.equals(""))
      sbQuery.append(" AND T6630.C6630_ACTIVITY_ID = areauser.ACTID ");

    if (strCategory.equals("105267") || strCategory.equals("105269"))
      sbQuery.append(" AND T6630.C6630_ACTIVITY_ID = PRODUCT.ACTID(+) ");

    if (strCategory.equals("105269")) // Lead
      sbQuery.append(" AND T6630.C6630_ACTIVITY_ID = Source.c6630_activity_id(+) ");

    if (!strActNm.equals(""))
      sbQuery.append(" AND UPPER(T6630.C6630_ACTIVITY_NM) LIKE UPPER('%" + strActNm + "%') ");

    if (!strSystemid.equals(""))
      sbQuery.append(" AND T6630.C6630_ACTIVITY_ID = PDTS.ACTID ");

    if (!strProcedure.equals(""))
      sbQuery.append(" AND T6630.C6630_ACTIVITY_ID = TECHS.ACTID ");

    if (!strState.equals(""))
      sbQuery.append(" AND T6630.C6630_ACTIVITY_ID = STATE.ACTID ");

    if (strCategory.equals("105267"))
      sbQuery.append(" and to_char(SURGNM.PARTYID) = picinfo.REFID(+) ");

    if (strCategory.equals("105265"))
      sbQuery.append(" AND T6630.C6630_ACTIVITY_ID = PENDINGMERC.ACTID(+) ");

    if (strStatus.equals("105761") || strStatus.equals("105763")) // Physician visit scheduled
                                                                  // status ("105761")
      sbQuery.append("AND T6630.C901_ACTIVITY_STATUS IN ('105761') ");
    else if (strStatus.equals("105760") || strStatus.equals("105761','105760")) // Physician visit
                                                                                // requested status
                                                                                // ("105760")
      sbQuery.append("AND T6630.C901_ACTIVITY_STATUS IN ('" + strStatus + "') ");

    if (!strCrfStatus.equals(""))
      sbQuery.append("AND T6605.C901_STATUS IN ('" + strCrfStatus
          + "') AND T6630.C6630_ACTIVITY_ID =T6605.C6630_ACTIVITY_ID ");


    if (!strSurgPartyID.equals(""))
      sbQuery
          .append(" AND T6630.C6630_ACTIVITY_ID = T6631.C6630_ACTIVITY_ID and T6631.c101_party_id = '"
              + strSurgPartyID + "' AND T6630.C6630_FROM_DT >= TRUNC(SYSDATE) ");

    sbQuery
        .append(" AND T6630.C6630_ACTIVITY_ID = LOCATION.ACTID(+) ORDER BY T6630.C6630_FROM_DT ");


    sbQuery.append(" ,ACTSTARTTIME ");

    log.debug("fetchActivityList Query=" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn.size());

    return alReturn;

  }


  public HashMap fetchActivityReport(HashMap hmParams) throws AppError {
    ArrayList alActivityDtls = new ArrayList();
    ArrayList alActivityAttrDtls = new ArrayList();
    ArrayList alActivityPartyDtls = new ArrayList();
    ArrayList alActivityAddressDtls = new ArrayList();
    ArrayList alLogDtls = new ArrayList();
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmTemp = new HashMap();

    alActivityDtls = fetchPhysicianVisitReport(hmParams);
    if (alActivityDtls.size() > 0) {
      hmResult = GmCommonClass.parseNullHashMap((HashMap) alActivityDtls.get(0));
      hmParams.put("ACTID", GmCommonClass.parseNull((String) hmResult.get("ACTID")));
      alActivityAttrDtls = fetchActivityAttrReport(hmParams);
      alActivityPartyDtls = fetchActivityPartyReport(hmParams);

      String strAddrID = "";
      int length = alActivityAttrDtls.size();

      for (int i = 0; i < length; i++) {
        hmTemp = (HashMap) alActivityAttrDtls.get(i);
        String strAttrType = GmCommonClass.parseNull((String) hmTemp.get("ATTRTYPE"));
        if (strAttrType.equals("7777")) {
          strAddrID = GmCommonClass.parseNull((String) hmTemp.get("ATTRVAL"));
          log.debug("We Have an Address>>>>>" + strAddrID);
        }
      }


      if (strAddrID != "") {
        alActivityAddressDtls = fetchActivityAddressReport(hmParams, strAddrID);
      }


      alLogDtls = fetchLogReport(hmParams);
      hmReturn.put("ACTIVITYDTLS", alActivityDtls);
      hmReturn.put("ACTIVITYATTRDTLS", alActivityAttrDtls);
      hmReturn.put("ACTIVITYPARTYDTLS", alActivityPartyDtls);
      hmReturn.put("ACTIVITYADDRESSDTLS", alActivityAddressDtls);
      hmReturn.put("LOGDTLS", alLogDtls);

      log.debug("report full info" + hmReturn);

      return hmReturn;
    } else
      return null;

  }

  public GmCRMActivityVO populateActivityListVO(HashMap hmParam) throws AppError {

    ArrayList alActivityDtls = new ArrayList();
    ArrayList alActivityAttrDtls = new ArrayList();
    ArrayList alActivityPartyDtls = new ArrayList();
    ArrayList alActivityAddressDtls = new ArrayList();
    ArrayList alLogDtls = new ArrayList();
    HashMap hmResult = new HashMap();

    List<GmCRMActivityVO> listGmCRMActivityVO = new ArrayList<GmCRMActivityVO>();
    List<GmCRMActivityAttrVO> listGmCRMActivityAttrVO = new ArrayList<GmCRMActivityAttrVO>();
    List<GmCRMActivityPartyVO> listGmCRMActivityPartyVO = new ArrayList<GmCRMActivityPartyVO>();
    List<GmCRMSurgeonAddressListVO> listGmCRMActivityAddressListVO =
        new ArrayList<GmCRMSurgeonAddressListVO>();
    List<GmCRMLogVO> listGmCRMLogVO = new ArrayList<GmCRMLogVO>();

    GmCRMActivityVO gmCRMActivityVO = new GmCRMActivityVO();
    GmCRMActivityAttrVO gmCRMActivityAttrVO = new GmCRMActivityAttrVO();
    GmCRMActivityPartyVO gmCRMActivityPartyVO = new GmCRMActivityPartyVO();
    GmCRMSurgeonAddressListVO gmCRMActivityAddressVO = new GmCRMSurgeonAddressListVO();
    GmCRMLogVO gmCRMLogVO = new GmCRMLogVO();
    GmWSUtil gmWSUtil = new GmWSUtil();

    alActivityDtls = (ArrayList) hmParam.get("ACTIVITYDTLS");
    hmResult = GmCommonClass.parseNullHashMap((HashMap) alActivityDtls.get(0));
    alActivityAttrDtls = (ArrayList) hmParam.get("ACTIVITYATTRDTLS");
    alActivityPartyDtls = (ArrayList) hmParam.get("ACTIVITYPARTYDTLS");
    alActivityAddressDtls = (ArrayList) hmParam.get("ACTIVITYADDRESSDTLS");
    alLogDtls = (ArrayList) hmParam.get("LOGDTLS");

    listGmCRMActivityVO =
        gmWSUtil.getVOListFromHashMapList(alActivityDtls, gmCRMActivityVO,
            gmCRMActivityVO.getMappingProperties());


    listGmCRMActivityAttrVO =
        gmWSUtil.getVOListFromHashMapList(alActivityAttrDtls, gmCRMActivityAttrVO,
            gmCRMActivityAttrVO.getMappingProperties());

    listGmCRMActivityPartyVO =
        gmWSUtil.getVOListFromHashMapList(alActivityPartyDtls, gmCRMActivityPartyVO,
            gmCRMActivityPartyVO.getMappingProperties());

    listGmCRMActivityAddressListVO =
        gmWSUtil.getVOListFromHashMapList(alActivityAddressDtls, gmCRMActivityAddressVO,
            gmCRMActivityAddressVO.getMappingProperties());

    listGmCRMLogVO =
        gmWSUtil.getVOListFromHashMapList(alLogDtls, gmCRMLogVO, gmCRMLogVO.getMappingProperties());

    log.debug("log vo check >>> " + alLogDtls);

    gmCRMActivityVO.setArrgmactivityattrvo(listGmCRMActivityAttrVO);
    gmCRMActivityVO.setArrgmactivitypartyvo(listGmCRMActivityPartyVO);
    gmCRMActivityVO.setArrgmcrmlogvo(listGmCRMLogVO);
    gmCRMActivityVO.setArrgmcrmaddressvo(listGmCRMActivityAddressListVO);
    gmCRMActivityVO.setActid(GmCommonClass.parseNull((String) hmResult.get("ACTID")));
    gmCRMActivityVO.setActnm(GmCommonClass.parseNull((String) hmResult.get("ACTNM")));
    gmCRMActivityVO.setActstartdate(GmCommonClass.parseNull((String) hmResult.get("ACTSTARTDATE")));
    gmCRMActivityVO.setActenddate(GmCommonClass.parseNull((String) hmResult.get("ACTENDDATE")));
    gmCRMActivityVO.setActdesc(GmCommonClass.parseNull((String) hmResult.get("ACTDESC")));
    gmCRMActivityVO.setVoidfl(GmCommonClass.parseNull((String) hmResult.get("VOIDFL")));
    gmCRMActivityVO.setActtype(GmCommonClass.parseNull((String) hmResult.get("ACTTYPE")));
    gmCRMActivityVO.setActtypenm(GmCommonClass.parseNull((String) hmResult.get("ACTTYPENM")));
    gmCRMActivityVO.setActstatus(GmCommonClass.parseNull((String) hmResult.get("ACTSTATUS")));
    gmCRMActivityVO.setActstatusnm(GmCommonClass.parseNull((String) hmResult.get("ACTSTATUSNM")));
    gmCRMActivityVO.setActcategory(GmCommonClass.parseNull((String) hmResult.get("ACTCATEGORY")));
    gmCRMActivityVO.setSourcetypid(GmCommonClass.parseNull((String) hmResult.get("SOURCETYPID")));
    gmCRMActivityVO.setSourcetypnm(GmCommonClass.parseNull((String) hmResult.get("SOURCETYPNM")));
    gmCRMActivityVO.setSource(GmCommonClass.parseNull((String) hmResult.get("SOURCE")));
    gmCRMActivityVO.setPartyid(GmCommonClass.parseNull((String) hmResult.get("PARTYID")));
    gmCRMActivityVO.setPartynm(GmCommonClass.parseNull((String) hmResult.get("PARTYNM")));
    gmCRMActivityVO.setActstarttime(GmCommonClass.parseNull((String) hmResult.get("ACTSTARTTIME")));
    gmCRMActivityVO.setActendtime(GmCommonClass.parseNull((String) hmResult.get("ACTENDTIME")));
    gmCRMActivityVO.setCreatedbyid(GmCommonClass.parseNull((String) hmResult.get("CREATEDBYID")));
    gmCRMActivityVO.setCreatedby(GmCommonClass.parseNull((String) hmResult.get("CREATEDBY")));
    gmCRMActivityVO.setCreateddate(GmCommonClass.parseNull((String) hmResult.get("CREATEDDATE")));
    gmCRMActivityVO.setToppriority(GmCommonClass.parseNull((String) hmResult.get("TOPPRIORITY")));
    gmCRMActivityVO.setOtherinstr(GmCommonClass.parseNull((String) hmResult.get("OTHERINSTR")));
    gmCRMActivityVO.setDivision(GmCommonClass.parseNull((String) hmResult.get("DIVISION")));
    gmCRMActivityVO.setDivisionnm(GmCommonClass.parseNull((String) hmResult.get("DIVISIONNM")));
    gmCRMActivityVO.setActnotes(GmCommonClass.parseNull((String) hmResult.get("ACTNOTES")));
    gmCRMActivityVO.setDietary(GmCommonClass.parseNull((String) hmResult.get("DIETARY")));
    return gmCRMActivityVO;

  }

  public ArrayList fetchPhysicianVisitReport(HashMap hmParams) throws AppError {
    String strActID = GmCommonClass.parseNull((String) hmParams.get("ACTID"));
    String strCategory = GmCommonClass.parseNull((String) hmParams.get("ACTCATEGORY"));
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT C6630_DIETARY DIETARY ,C6630_ACTIVITY_ID ACTID, C6630_ACTIVITY_NM ACTNM, C6630_CREATED_BY CREATEDBYID, GET_USER_NAME(C6630_CREATED_BY) CREATEDBY, TO_CHAR(C6630_CREATED_DATE,'mm/dd/yyyy') CREATEDDATE,  C6630_ACTIVITY_DESC ACTDESC, C6630_VOID_FL VOIDFL, to_char(C6630_FROM_DT,'mm/dd/yyyy') ACTSTARTDATE, to_char(C6630_TO_DT,'mm/dd/yyyy') ACTENDDATE, C901_ACTIVITY_TYPE ACTTYPE, GET_CODE_NAME(C901_ACTIVITY_TYPE) ACTTYPENM, C901_ACTIVITY_STATUS ACTSTATUS, GET_CODE_NAME(C901_ACTIVITY_STATUS) ACTSTATUSNM, C901_ACTIVITY_CATEGORY ACTCATEGORY, ");
    sbQuery
        .append(" C901_SOURCE_TYPE SOURCETYPID, GET_CODE_NAME(C901_SOURCE_TYPE) SOURCETYPNM, C6630_SOURCE SOURCE, C6630_PARTY_ID PARTYID, C6630_PARTY_NM PARTYNM, TO_CHAR(C6630_FROM_TM,'HH24:MI') ACTSTARTTIME, TO_CHAR(C6630_TO_TM,'HH24:MI') ACTENDTIME,to_char(C6630_From_Tm,'TZR')TZONE, ");
    sbQuery
        .append(" C6630_OTHER_INSTRUMENT OTHERINSTR, C6630_TOP_PRIORITY TOPPRIORITY, GET_DIVISION_NAME(C1910_DIVISION_ID) DIVISIONNM, C1910_DIVISION_ID DIVISION, C6630_NOTE ACTNOTES");
    sbQuery.append(" FROM T6630_ACTIVITY WHERE C6630_VOID_FL IS NULL AND C6630_ACTIVITY_ID = '"
        + strActID + "' ");
    if (!strCategory.equals(""))
      sbQuery.append(" AND C901_ACTIVITY_CATEGORY= '" + strCategory + "' ");

    log.debug("fetchPhysicianVisitReport Query=" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;

  }

  public ArrayList fetchProjectSyncData(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_update.gm_fch_all_prj_updt", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    gmDBManager.close();
    return alResult;
  }

  public ArrayList fetchActivityNotesSync(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_update.gm_fch_all_actlog_updt", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    gmDBManager.close();
    return alResult;
  }


  public ArrayList fetchActivitySyncReport(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_update.gm_fch_all_activity_updates", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    gmDBManager.close();
    return alResult;
  }

  public ArrayList fetchActivityUpdateReport(HashMap hmParams) throws AppError {

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));

    sbQuery
        .append(" SELECT T6630.C6630_ACTIVITY_ID ACTID, GET_CODE_NAME(T6630.C901_ACTIVITY_TYPE) ACTTYPENM, T6630.C901_ACTIVITY_TYPE ACTTYPE, T6630.C6630_ACTIVITY_NM ACTNM, "
            + " TO_CHAR(T6630.C6630_FROM_DT,'MM/DD/YYYY') ACTSTARTDATE, TO_CHAR(T6630.C6630_TO_DT,'MM/DD/YYYY') ACTENDDATE, T6630.C6630_VOID_FL VOIDFL, "
            + " T6630.c901_activity_status actstatus, T6630.c6630_party_id PARTYID, T6630.c6630_party_nm PARTYNM, T6630.c6630_activity_nm ACTNM, T6630.c901_activity_category "
            + " ACTCATEGORY, get_code_name(T6630.c901_activity_category) ACTIVITYTYPE, T6630.C6630_FROM_TM ACTSTARTTIME, T6630.C6630_TO_TM ACTENDTIME, "
            + " t6630.c6630_last_updated_date FROM T6630_ACTIVITY T6630 WHERE T6630.C6630_VOID_FL IS NULL AND DECODE(t6630.c6630_last_updated_date,null,t6630.c6630_created_date,t6630.c6630_last_updated_date) >= to_char((SELECT "
            + " max(c9151_last_sync_date) LASTDATE FROM T9151_DEVICE_SYNC_DTL WHERE c9151_last_updated_by = '"
            + strUserID + "' )) ");

    log.debug("Query=" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;

  }

  public ArrayList fetchActivityPartySyncReport(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_update.gm_fch_all_activityparty_updt", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    gmDBManager.close();
    return alResult;
  }

  public ArrayList fetchActivityPartyUpdateReport(HashMap hmParams) throws AppError {

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));

    sbQuery
        .append(" SELECT c6631_activity_party_id ACTPARTYID, c6630_activity_id ACTID, c901_activity_party_role ROLEID, GET_CODE_NAME(c901_activity_party_role) ROLENM, "
            + " c101_party_id PARTYID, c6631_void_fl VOIDFL, C901_PARTY_STATUS STATUSID, GET_CODE_NAME(C901_PARTY_STATUS) STATUSNM FROM t6631_activity_party_assoc "
            + " WHERE C6631_VOID_FL IS NULL AND c6631_last_updated_date >= to_char((SELECT max(c9151_last_sync_date) LASTDATE FROM T9151_DEVICE_SYNC_DTL "
            + " WHERE c9151_last_updated_by = '" + strUserID + "' )) ");

    log.debug("Query=" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;

  }

  public ArrayList fetchActivityAttrSyncReport(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_crm_update.gm_fch_all_activityattr_updt", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    gmDBManager.close();
    return alResult;
  }

  public ArrayList fetchActivityAttrUpdateReport(HashMap hmParams) throws AppError {

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));

    sbQuery
        .append(" SELECT c6632_activity_attribute_id attrid, c6630_activity_id actid, c6632_attribute_value attrval, c6632_attribute_nm attrnm, "
            + " c901_attribute_type attrtype, c6632_void_fl voidfl FROM t6632_activity_attribute WHERE c6632_void_fl IS NULL AND c6632_last_updated_date "
            + " >= to_char((SELECT max(c9151_last_sync_date) LASTDATE FROM T9151_DEVICE_SYNC_DTL WHERE c9151_last_updated_by = '"
            + strUserID + "' )) ");

    log.debug("Query=" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;

  }

  public ArrayList fetchActivityAttrReport(HashMap hmParams) throws AppError {

    String strActID = GmCommonClass.parseNull((String) hmParams.get("ACTID"));
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT C6632_ACTIVITY_ATTRIBUTE_ID ATTRID, c6632_attribute_value ATTRVAL, C6632_ATTRIBUTE_ID ATTID, C6632_ATTRIBUTE_NM ATTRNM, C901_ATTRIBUTE_TYPE ATTRTYPE, C6632_VOID_FL VOIDFL ");
    sbQuery
        .append(" FROM T6632_ACTIVITY_ATTRIBUTE WHERE C6632_VOID_FL IS NULL AND C6630_ACTIVITY_ID  = '"
            + strActID + "'");

    log.debug("Query=" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;

  }

  public ArrayList fetchActivityAddressReport(HashMap hmParams, String strAddrID) throws AppError {


    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT C106_ADDRESS_ID ADDID, C106_ADD1 ADD1, C106_ADD2 ADD2, C106_CITY CITY, GET_CODE_NAME(C901_STATE) STATENM, C901_STATE STATEID, "
            + " GET_CODE_NAME(C901_COUNTRY) COUNTRYNM, C901_COUNTRY COUNTRYID, C106_ZIP_CODE ZIP FROM T106_ADDRESS WHERE C106_ADDRESS_ID = '"
            + strAddrID + "' " + " AND C106_VOID_FL IS NULL ");

    log.debug("Query=" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;

  }


  public ArrayList fetchLogReport(HashMap hmParams) throws AppError {

    String strActID = GmCommonClass.parseNull((String) hmParams.get("ACTID"));
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT C902_LOG_ID LOGID, C902_REF_ID REFID, C902_COMMENTS LOGCOMMENT, C902_TYPE LOGTYPEID, GET_CODE_NAME(C902_TYPE) LOGTYPE,C902_LAST_UPDATED_BY UPDATEDBYID, GET_USER_NAME(C902_LAST_UPDATED_BY) UPDATEDBY, C902_LAST_UPDATED_DATE UPDATEDDATE,C902_VOID_FL VOIDFL  ");
    sbQuery
        .append(" FROM T902_LOG WHERE C902_VOID_FL IS NULL AND C902_TYPE in ('105184','4000892','4000891','4000893') ");
    sbQuery.append(" AND C902_REF_ID = '" + strActID + "' ORDER BY UPDATEDDATE DESC, LOGID DESC");

    log.debug("fetchLogReport Query=" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;

  }


  public ArrayList fetchActivityPartyReport(HashMap hmParams) throws AppError {

    String strActID = GmCommonClass.parseNull((String) hmParams.get("ACTID"));
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append("SELECT T6631.C6631_ACTIVITY_PARTY_ID ACTPARTYID, T6631.C101_PARTY_ID PARTYID,T101U.C101_EMAIL_ID EMAILID, ");
    sbQuery
        .append(" T6631.C901_ACTIVITY_PARTY_ROLE ROLEID, GET_CODE_NAME(T6631.C901_ACTIVITY_PARTY_ROLE) ROLENM, ");
    sbQuery
        .append(" npid.npid PARTYNPID,  GET_CODE_NAME(npid.salute) || ' ' || T101.C101_FIRST_NM || ' ' || T101.C101_MIDDLE_INITIAL || ' ' || T101.C101_LAST_NM PARTYNM, ");
    sbQuery
        .append(" T6631.C901_PARTY_STATUS STATUSID, GET_CODE_NAME(T6631.C901_PARTY_STATUS) STATUSNM, T6631.C6631_VOID_FL VOIDFL, "
            + " T6631.C6630_ACTIVITY_ID ACTID , t101.c901_party_type partytype, DECODE(t101.c901_party_type,7000,get_surgeon_cv_path(T6631.C101_PARTY_ID),'') SURGEONCV ");
    sbQuery
        .append(" FROM T6631_ACTIVITY_PARTY_ASSOC T6631, T101_PARTY T101 , T101_USER T101U , "
            + "(SELECT c6600_surgeon_npi_id npid , C6600_PARTY_SALUTE salute, c101_party_surgeon_id partyid FROM t6600_party_surgeon WHERE c6600_void_fl is null) npid "
            + "WHERE T6631.C101_PARTY_ID = T101.C101_PARTY_ID(+) AND T101.C101_PARTY_ID = NPID.PARTYID(+) AND T101.C101_PARTY_ID = T101U.C101_PARTY_ID(+) AND T6631.C6631_VOID_FL IS NULL AND C6630_ACTIVITY_ID = '"
            + strActID + "'");

    log.debug("fetchActivityPartyReport Query=" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    log.debug("alReturnBean>>>>>" + alReturn);

    return alReturn;

  }

  /**
   * saveActivityDetailJSON method is for save the activity info as JSON data
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */
  public String saveActivityDetailJSON(HashMap<String, String> hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmJmsParam = new HashMap();
    String strJSONInput = GmCommonClass.parseNull(hmParams.get("JSONDATA"));
    String strUserID = GmCommonClass.parseNull(hmParams.get("USERID"));
    String strActivityId = "";
    gmDBManager.setPrepareString("gm_pkg_crm_activity.gm_sav_crm_activity_json", 3);
    gmDBManager.setString(1, strJSONInput);
    gmDBManager.setString(2, strUserID);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strActivityId = GmCommonClass.parseNull(gmDBManager.getString(3));
    gmDBManager.commit();
    // Calling JMS to Sync Party Activity
    hmJmsParam.put("USERID", hmParams.get("USERID"));
    hmJmsParam.put("STROPT", "ACTIVITY");
    syncPartyActivityJMS(hmJmsParam);

    return strActivityId;
  }

  /**
   * This Method is to fetch activity detail information as JSON
   * 
   * @param strActId
   * @return
   * @throws AppError
   */
  public String fetchActivityDetailJSON(String strActId, String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strReturn = "";
    gmDBManager.setPrepareString("gm_pkg_crm_activity.gm_fch_crm_activity_json", 3);
    gmDBManager.setString(1, strActId);
    gmDBManager.setString(2, strUserId);
    gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
    gmDBManager.execute();
    strReturn = GmCommonClass.parseNull(gmDBManager.getString(3));
    gmDBManager.close();
    return strReturn;
  }

  public String saveActivityDetail(HashMap hmParams, List<GmCRMActivityAttrVO> alActivityAttrVO,
      GmCRMActivityAttrVO gmActivityAttrVO, List<GmCRMActivityPartyVO> alActivityPartyVO,
      GmCRMActivityPartyVO gmActivityPartyVO, List<GmCRMLogVO> alCrmLogVO, GmCRMLogVO gmCRMLogVO,
      List<GmCRMSurgeonAddressListVO> alGmAddressVO, GmCRMSurgeonAddressListVO gmCRMAddressListVO)
      throws Exception, AppError {
    // TODO Auto-generated method stub
    HashMap hmJmsParam = new HashMap();
    String strActID = "";
    String strAddrID = "";

    GmDBManager gmDBManager = new GmDBManager();

    strActID = saveActivityInfo(hmParams, gmDBManager);
    hmParams.put("ACTID", strActID);
    log.debug("The hashmap activityID " + hmParams.get("ACTID"));

    // log.debug("The alGmAddressVO " + alGmAddressVO.get(0));
    log.debug("The attrvo length" + alActivityAttrVO.size());
    saveActivityAttrInfo(hmParams, gmDBManager, alActivityAttrVO, gmActivityAttrVO);
    saveActivityPartyInfo(hmParams, gmDBManager, alActivityPartyVO, gmActivityPartyVO);
    saveLogInfo(hmParams, gmDBManager, alCrmLogVO, gmCRMLogVO);
    String strtestAddrID = "";

    // log.debug("The alGmAddressVO " + alGmAddressVO.get(0));
    int len = alGmAddressVO.size();

    int length = alActivityAttrVO.size();
    if (length > 0) {
      for (int i = 0; i < length; i++) {
        String strType = alActivityAttrVO.get(i).getAttrtype();
        if (strType.equals("7777")) {
          strtestAddrID = "Y";
        }
      }
    } else {
      strtestAddrID = "";
    }


    log.debug("T strtestAddrID th " + strtestAddrID);

    log.debug("The address length " + len);
    if (len > 0) {
      strAddrID = saveActivityAddressInfo(hmParams, gmDBManager, alGmAddressVO, gmCRMAddressListVO);
      log.debug(strtestAddrID.equals(""));
      if (strtestAddrID.equals("")) {
        log.debug("New address save into attrVO");
        saveAddressAttrInfo(hmParams, gmDBManager, strAddrID);
      }
      log.debug("The strAddrID in bean is " + strAddrID);
    }


    StringBuffer sbQuery = new StringBuffer();
    gmDBManager.commit();


    log.debug("The strActID is in bean is " + strActID);

    // Calling JMS to Sync Party Activity
    hmJmsParam.put("USERID", hmParams.get("USERID"));
    hmJmsParam.put("STROPT", "ACTIVITY");
    syncPartyActivityJMS(hmJmsParam);

    return strActID;
  }

  private void saveAddressAttrInfo(HashMap hmParams, GmDBManager gmDBManager, String strAddrID) {
    gmDBManager.setPrepareString("gm_pkg_crm_activity.gm_sav_crm_activity_attr", 7);
    gmDBManager.setString(1, "");
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("ACTID")));
    gmDBManager.setString(3, "7777");
    gmDBManager.setString(4, strAddrID);
    gmDBManager.setString(5, strAddrID);
    gmDBManager.setString(6, "");
    gmDBManager.setString(7, GmCommonClass.parseNull((String) hmParams.get("USERID")));
    gmDBManager.execute();
  }


  private void updateActivitySource(HashMap hmParams, GmDBManager gmDBManager) {
    // TODO Auto-generated method stub
    gmDBManager.setPrepareString("gm_pkg_crm_activity.gm_sav_crm_update_result", 3);

    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("RESULTFOR")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("ACTID")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParams.get("USERID")));
    gmDBManager.execute();
  }

  public String saveActivityInfo(HashMap hmParams, GmDBManager gmDBManager) throws Exception,
      AppError {

    String strActID = "";
    String strFromDate = GmCommonClass.parseNull((String) hmParams.get("ACTSTARTDATE"));
    String strEndDate = GmCommonClass.parseNull((String) hmParams.get("ACTENDDATE"));
    String strDateFmt = GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT");
    Date fromdt = GmCommonClass.getStringToDate(strFromDate, strDateFmt);
    Date enddt = GmCommonClass.getStringToDate(strEndDate, strDateFmt);
    String strStartTime = GmCommonClass.parseNull((String) hmParams.get("ACTSTARTTIME"));
    String strEndTime = GmCommonClass.parseNull((String) hmParams.get("ACTENDTIME"));

    gmDBManager.setPrepareString("gm_pkg_crm_activity.gm_sav_crm_activity", 21);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("ACTID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("ACTNM")));

    gmDBManager.setDate(3, fromdt);
    gmDBManager.setDate(4, enddt);

    gmDBManager.setString(5, GmCommonClass.parseNull((String) hmParams.get("ACTDESC")));
    gmDBManager.setString(6, GmCommonClass.parseNull((String) hmParams.get("VOIDFL")));
    gmDBManager.setString(7, GmCommonClass.parseNull((String) hmParams.get("ACTTYPE")));
    gmDBManager.setString(8, GmCommonClass.parseNull((String) hmParams.get("ACTSTATUS")));
    gmDBManager.setString(9, GmCommonClass.parseNull((String) hmParams.get("ACTCATEGORY")));
    gmDBManager.setString(10, null);
    gmDBManager.setString(11, null);
    gmDBManager.setString(12, GmCommonClass.parseNull((String) hmParams.get("USERID")));
    gmDBManager.setString(13, GmCommonClass.parseNull((String) hmParams.get("PARTYID")));
    gmDBManager.setString(14, GmCommonClass.parseNull((String) hmParams.get("PARTYNM")));
    gmDBManager.setString(15, GmCommonClass.parseNull((String) hmParams.get("ACTSTARTTIME")));
    gmDBManager.setString(16, GmCommonClass.parseNull((String) hmParams.get("ACTENDTIME")));
    gmDBManager.setString(17, GmCommonClass.parseNull((String) hmParams.get("TOPPRIORITY")));
    gmDBManager.setString(18, GmCommonClass.parseNull((String) hmParams.get("OTHERINSTR")));
    gmDBManager.setString(19, GmCommonClass.parseNull((String) hmParams.get("DIVISION")));
    gmDBManager.setString(20, GmCommonClass.parseNull((String) hmParams.get("ACTNOTES")));
    gmDBManager.setString(21, GmCommonClass.parseNull((String) hmParams.get("DIETARY")));

    gmDBManager.execute();
    strActID = GmCommonClass.parseNull(gmDBManager.getString(1));
    log.debug("ActID is " + strActID);
    return strActID;
  }

  public void saveActivityAttrInfo(HashMap hmParams, GmDBManager gmDBManager,
      List<GmCRMActivityAttrVO> alActivityAttrVO, GmCRMActivityAttrVO gmActivityAttrVO) {

    int length = alActivityAttrVO.size();
    for (int i = 0; i < length; i++) {
      GmCRMActivityAttrVO gnActivityAttr = alActivityAttrVO.get(i);
      GmCommonBean gmCommonBean = new GmCommonBean();

      gmDBManager.setPrepareString("gm_pkg_crm_activity.gm_sav_crm_activity_attr", 7);
      gmDBManager.setString(1, GmCommonClass.parseNull(gnActivityAttr.getAttrid()));
      gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("ACTID")));
      gmDBManager.setString(3, GmCommonClass.parseNull(gnActivityAttr.getAttrtype()));
      gmDBManager.setString(4, GmCommonClass.parseNull(gnActivityAttr.getAttrval()));
      gmDBManager.setString(5, GmCommonClass.parseNull(gnActivityAttr.getAttrnm()));
      gmDBManager.setString(6, GmCommonClass.parseNull(gnActivityAttr.getVoidfl()));
      gmDBManager.setString(7, GmCommonClass.parseNull((String) hmParams.get("USERID")));
      gmDBManager.execute();
    }

  }

  public String saveActivityAddressInfo(HashMap hmParams, GmDBManager gmDBManager,
      List<GmCRMSurgeonAddressListVO> alGmAddressVO, GmCRMSurgeonAddressListVO gmCRMAddressListVO) {

    GmCRMSurgeonAddressListVO gnAddressVO = alGmAddressVO.get(0);
    GmCommonBean gmCommonBean = new GmCommonBean();
    log.debug("strAddrId >>>>>" + GmCommonClass.parseNull(gnAddressVO.getAddid()));
    gmDBManager.setPrepareString("gm_pkg_crm_address.gm_crm_sav_activity_addr_info", 9);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(1, GmCommonClass.parseNull(gnAddressVO.getAddid()));
    gmDBManager.setString(2, "90401");
    gmDBManager.setString(3, GmCommonClass.parseNull(gnAddressVO.getAdd1()));
    gmDBManager.setString(4, GmCommonClass.parseNull(gnAddressVO.getAdd2()));
    gmDBManager.setString(5, GmCommonClass.parseNull(gnAddressVO.getCity()));
    gmDBManager.setString(6, GmCommonClass.parseNull(gnAddressVO.getStateid()));
    gmDBManager.setString(7, GmCommonClass.parseNull(gnAddressVO.getCountryid()));
    gmDBManager.setString(8, GmCommonClass.parseNull(gnAddressVO.getZip()));
    gmDBManager.setString(9, GmCommonClass.parseNull((String) hmParams.get("USERID")));
    gmDBManager.execute();

    String strAddrId = GmCommonClass.parseNull(gmDBManager.getString(1));
    log.debug("strAddrId >>>>>" + strAddrId);
    return strAddrId;


  }

  public void saveActivityPartyInfo(HashMap hmParams, GmDBManager gmDBManager,
      List<GmCRMActivityPartyVO> alActivityPartyVO, GmCRMActivityPartyVO gmActivityPartyVO) {

    int length = alActivityPartyVO.size();

    for (int i = 0; i < length; i++) {
      GmCRMActivityPartyVO gmOrderparamVO = alActivityPartyVO.get(i);
      GmCommonBean gmCommonBean = new GmCommonBean();

      gmDBManager.setPrepareString("gm_pkg_crm_activity.gm_sav_crm_activity_party", 7);
      gmDBManager.setString(1, GmCommonClass.parseNull(gmOrderparamVO.getActpartyid()));
      gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("ACTID")));
      gmDBManager.setString(3, GmCommonClass.parseNull(gmOrderparamVO.getPartyid()));
      gmDBManager.setString(4, GmCommonClass.parseNull(gmOrderparamVO.getRoleid()));
      gmDBManager.setString(5, GmCommonClass.parseNull(gmOrderparamVO.getStatusid()));
      gmDBManager.setString(6, GmCommonClass.parseNull(gmOrderparamVO.getVoidfl()));
      gmDBManager.setString(7, GmCommonClass.parseNull((String) hmParams.get("USERID")));
      gmDBManager.execute();
    }

  }

  public void saveLogInfo(HashMap hmParams, GmDBManager gmDBManager, List<GmCRMLogVO> alLogVO,
      GmCRMLogVO gmLogVO) {

    int length = alLogVO.size();
    for (int i = 0; i < length; i++) {
      GmCRMLogVO gmCRMLogVO = alLogVO.get(i);
      GmCommonBean gmCommonBean = new GmCommonBean();

      log.debug("Last updated user check >>>  "
          + GmCommonClass.parseNull((String) hmParams.get("USERID")));

      gmCRMLogVO.setRefid((String) hmParams.get("ACTID"));
      gmCRMLogVO.setUserid((String) hmParams.get("USERID"));

      saveLog(gmDBManager, gmCRMLogVO);
    }

  }

  public String saveLog(GmDBManager gmDBManager, GmCRMLogVO gmCRMLogVO) {
    // TODO Auto-generated method stub

    String strLogID = new String();
    // GmDBManager gmDBManager = new GmDBManager();

    log.debug("gm_pkg_crm_activity.gm_sav_crm_log>>>>>");
    gmDBManager.setPrepareString("gm_pkg_crm_activity.gm_sav_crm_log", 6);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(1, GmCommonClass.parseNull(gmCRMLogVO.getLogid()));
    gmDBManager.setString(2, GmCommonClass.parseNull(gmCRMLogVO.getRefid()));
    gmDBManager.setString(3, GmCommonClass.parseNull(gmCRMLogVO.getLogtypeid()));
    gmDBManager.setString(4, GmCommonClass.parseNull(gmCRMLogVO.getLogcomment()));
    gmDBManager.setString(5, GmCommonClass.parseNull(gmCRMLogVO.getUserid()));
    gmDBManager.setString(6, GmCommonClass.parseNull(gmCRMLogVO.getVoidfl()));
    gmDBManager.execute();
    strLogID = GmCommonClass.parseNull(gmDBManager.getString(1));
    log.debug("strLogID>>>>>" + strLogID);


    return strLogID;
  }

  public HashMap getEventDetails(String strActID) {

    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT T6630.C6630_ACTIVITY_ID ACTID, TO_CHAR(T6630.C6630_CREATED_DATE,'MM/DD/YYYY') CREATEDDATE, C6630_DIETARY DIETARY, ");
    sbQuery.append(" T6630.C901_ACTIVITY_TYPE ACTTYPID, ");
    sbQuery.append(" GET_CODE_NAME(T6630.C901_ACTIVITY_TYPE) ACTTYP, ");
    sbQuery.append(" GET_RULE_VALUE(T6630.C901_ACTIVITY_TYPE,'EVENTCATEGORY') CATEGORY, ");
    sbQuery.append(" T6630.C6630_ACTIVITY_NM ACTNM, ");
    sbQuery.append(" T6630.c6630_event_item_id EVTITEMID, ");
    sbQuery.append(" TO_CHAR(T6630.C6630_FROM_DT,'YYYY/MM/DD') FRMDATE, ");
    sbQuery.append(" TO_CHAR(T6630.C6630_TO_DT,'YYYY/MM/DD') TODATE, ");
    sbQuery
        .append(" TO_CHAR(CAST(CAST(T6630.C6630_FROM_TM AS TIMESTAMP WITH TIME ZONE) AT TIME ZONE 'GMT' AS TIMESTAMP WITH TIME ZONE),'HH24:MI:SS') FRMTIME, ");
    sbQuery
        .append(" TO_CHAR(CAST(CAST(T6630.C6630_TO_TM AS TIMESTAMP WITH TIME ZONE) AT TIME ZONE 'GMT' AS TIMESTAMP WITH TIME ZONE),'HH24:MI:SS') TOTIME, ");
    sbQuery.append(" GET_USER_NAME(T6630.C6630_CREATED_BY) ORGNM, ");
    sbQuery.append(" GET_USER_EMAILID(T6630.C6630_CREATED_BY) ORGMAIL, ");
    sbQuery.append(" C6630_OTHER_INSTRUMENT OTHERLABINST , C6630_TOP_PRIORITY TOPPRIO, ");
    sbQuery.append(" T6630.C6630_VOID_FL VOIDFL,T6630.C6630_NOTE NOTE  ");
    sbQuery.append(" FROM T6630_ACTIVITY T6630 ");
    sbQuery.append(" WHERE T6630.C6630_ACTIVITY_ID = '" + strActID + "' ");

    log.debug("Event Details Query >>> " + sbQuery);

    hmReturn = GmCommonClass.parseNullHashMap((gmDBManager.querySingleRecord(sbQuery.toString())));

    return hmReturn;
  }

  public ArrayList getParticipantDetails(String strActID) {

    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT T101.C101_FIRST_NM || ' ' || T101.C101_LAST_NM NAME, T107.C107_CONTACT_VALUE EMAILID ");
    sbQuery
        .append(" FROM T6631_ACTIVITY_PARTY_ASSOC T6631, T101_PARTY T101,  T6630_ACTIVITY T6630, T107_CONTACT T107 ");
    sbQuery.append(" WHERE T101.C101_PARTY_ID = T6631.C101_PARTY_ID ");
    sbQuery.append(" AND T6630.C6630_ACTIVITY_ID = T6631.C6630_ACTIVITY_ID ");
    sbQuery.append(" AND T6631.C901_ACTIVITY_PARTY_ROLE = '105561' "); // 105561 -Participant (rep)
    sbQuery.append(" AND T107.C101_PARTY_ID(+) = T6631.C101_PARTY_ID ");
    sbQuery.append(" AND T107.C107_PRIMARY_FL(+)            = 'Y' ");
    sbQuery.append(" AND T107.C107_VOID_FL                   IS NULL ");
    sbQuery.append(" AND T107.C107_INACTIVE_FL               IS NULL ");
    sbQuery.append(" AND T107.C901_MODE(+)                 = '90452' ");
    sbQuery.append(" AND T6631.C6631_VOID_FL IS NULL ");
    sbQuery.append(" AND T6631.C6630_ACTIVITY_ID = '" + strActID + "' ");

    log.debug("Participant Details Query >>> " + sbQuery);

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    return alReturn;
  }

  public HashMap getUserDetails(String strUserid) {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT C101_USER_F_NAME || ' '  || C101_USER_L_NAME NAME, ");
    sbQuery.append(" C101_EMAIL_ID EMAILID ");
    sbQuery.append(" FROM T101_USER ");
    sbQuery.append(" WHERE C101_USER_ID = '" + strUserid + "' ");

    log.debug("Sender Details Query >>> " + sbQuery);

    hmReturn = GmCommonClass.parseNullHashMap((gmDBManager.querySingleRecord(sbQuery.toString())));

    return hmReturn;
  }

  /**
   * To save the event item id for the activity.
   * 
   * @param hmParams
   */
  /*
   * public void saveEventItemId(HashMap hmParams) { GmDBManager gmDBManager = new GmDBManager();
   * gmDBManager.setPrepareString("gm_pkg_crm_activity.gm_sav_crm_event_item_id", 3);
   * 
   * gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("ACTID")));
   * gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("ITEMID")));
   * gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParams.get("USERID")));
   * 
   * gmDBManager.execute(); gmDBManager.commit(); }
   */
  /**
   * To get the all mail related data's for the event creation.
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */
  /*
   * public HashMap getPhysicianVisitEmailDtl(HashMap hmParams) throws AppError {
   * 
   * GmCRMFileUploadVO gmCRMFileUploadVO = new GmCRMFileUploadVO(); HashMap hmReturn = new
   * HashMap(); List<HashMap> alResult = new ArrayList<HashMap>(); List alProdReview = new
   * ArrayList(); List alProdLab = new ArrayList(); List alOtherVisitor = new ArrayList(); List
   * alSurgeon = new ArrayList(); List alParticipant = new ArrayList(); List alProjects = new
   * ArrayList();
   * 
   * String strActID = GmCommonClass.parseNull((String) hmParams.get("ACTID"));
   * 
   * GmCRMFileUploadBean gmCRMFileUploadBean = new GmCRMFileUploadBean();
   * 
   * hmReturn = getEventDetails(strActID); alResult = fetchActivityAttrReport(hmParams);
   * 
   * for (HashMap hmTemp : alResult) { String strAttrType = GmCommonClass.parseNull((String)
   * hmTemp.get("ATTRTYPE")); if (strAttrType.equals("105582")) {// Products to Review
   * alProdReview.add(hmTemp); } else if (strAttrType.equals("105586")) {// Products to Use in Lab
   * alProdLab.add(hmTemp); } else if (strAttrType.equals("105590")) {// Other Visitors
   * alOtherVisitor.add(hmTemp); } else if (strAttrType.equals("105583")) { alProjects.add(hmTemp);
   * } }
   * 
   * alResult = fetchActivityPartyReport(hmParams); for (HashMap hmTemp : alResult) { String
   * strAttrType = GmCommonClass.parseNull((String) hmTemp.get("ROLEID")); if
   * (strAttrType.equals("105564")) {// Surgeons / Attendee alSurgeon.add(hmTemp); } else if
   * (strAttrType.equals("105561")) {// Participant alParticipant.add(hmTemp); } }
   * gmCRMFileUploadVO.setRefid(strActID); gmCRMFileUploadVO.setRefgroup("107994"); alResult =
   * gmCRMFileUploadBean.fetchFiles(gmCRMFileUploadVO);// .fetchFilesByType(strActID, "");
   * log.debug("===files=" + alResult);
   * 
   * hmReturn.put("ALPRODREVIEW", alProdReview); hmReturn.put("ALPRODLAB", alProdLab);
   * hmReturn.put("ALOTHERVISITOR", alOtherVisitor); hmReturn.put("ALOTHERVISITSIZE",
   * alOtherVisitor.size()); hmReturn.put("ALSURGEON", alSurgeon); hmReturn.put("ALPARTICIPANT",
   * alParticipant); hmReturn.put("ALPROJECTS", alProjects); hmReturn.put("ALFILES", alResult);
   * 
   * return hmReturn;
   * 
   * }
   */

  /**
   * To load the visit schedule informations.
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */
  public StringBuffer getCalendarViewQuery(HashMap hmParams) throws AppError {

    ArrayList alReturn = new ArrayList();
    HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
    GmAccessFilter gmAccessFilter = new GmAccessFilter();

    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    String strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("AccessFilter"));

    String strUserDeptId = GmCommonClass.parseNull((String) hmParams.get("USERDEPTID"));

    String strCategory = GmCommonClass.parseNull((String) hmParams.get("ACTCATEGORY"));
    String strPartyID =
        GmCommonClass
            .getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("PARTYID")));

    String strType =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams
            .get("ACTTYPEID")));
    String strSetId =
        GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmParams.get("SETID")));
    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT V700.ACCESSFL, V700.PARTYID, T6630.C6630_ACTIVITY_ID ACTID, T6630.C6630_ACTIVITY_NM ACTNM, T6630.C901_REGION REGION,  T6630.C6630_PARTY_NM PARTYNM, "
            + "T6630.C6630_FROM_DT ACTSTARTDATE, T6630.C6630_TO_DT ACTENDDATE,  GET_CODE_NAME(T6630.C901_ACTIVITY_TYPE) ACTTYPE, "
            + "T6630.C901_ACTIVITY_TYPE ACTTYPEID, get_code_name(T6630.C901_ACTIVITY_STATUS) ACTSTATUS , T6630.C6630_CREATED_BY CREATEDBY, "
            + "T6630.C901_ACTIVITY_STATUS ACTSTATUSID , TO_CHAR(CAST(T6630.C6630_FROM_TM AS TIMESTAMP WITH TIME ZONE),'HH24:MI') ACTSTARTTIME , "
            + "TO_CHAR(CAST(T6630.C6630_TO_TM AS TIMESTAMP WITH TIME ZONE),'HH24:MI') ACTENDTIME, C1910_DIVISION_ID DIVISION FROM T6630_ACTIVITY T6630 ");

    if (!strSetId.equals("")) {
      sbQuery
          .append(",(SELECT c6630_activity_id from T6632_ACTIVITY_ATTRIBUTE where C6632_ATTRIBUTE_VALUE IN ('"
              + strSetId + "') and C6632_VOID_FL is null ) T6632 ");

    }

    sbQuery
        .append(",( SELECT DISTINCT T6631.C6630_ACTIVITY_ID, T6631.C101_PARTY_ID PARTYID, 'Y' ACCESSFL, V700.REP_COMPID, V700.DIVID, V700.REGION_ID,"
            + "  V700.REP_PARTYID FROM T6631_ACTIVITY_PARTY_ASSOC T6631, V703_REP_MAPPING_DETAIL V700 ");

    sbQuery
        .append(" WHERE V700.REP_PARTYID = T6631.C101_PARTY_ID AND T6631.C6631_VOID_FL IS NULL  AND "
            + strAccessFilter + " ) V700 ");
    sbQuery.append(" WHERE T6630.C901_ACTIVITY_CATEGORY = '" + strCategory + "'");

    if ((strCategory.equals("105267") || strCategory.equals("105272")) && strUserDeptId.equals("S"))
      sbQuery.append(" AND T6630.C6630_ACTIVITY_ID = V700.C6630_ACTIVITY_ID ");
    else
      sbQuery.append(" AND T6630.C6630_ACTIVITY_ID = V700.C6630_ACTIVITY_ID (+) ");
    if (!strSetId.equals("")) {
      sbQuery.append(" AND T6630.C6630_ACTIVITY_ID = T6632.C6630_ACTIVITY_ID ");
    }

    if (!strType.equals(""))
      sbQuery.append(" AND T6630.C901_ACTIVITY_TYPE IN ('" + strType + "') ");

    if (!strPartyID.equals(""))
      sbQuery.append(" AND V700.REP_PARTYID IN ('" + strPartyID + "') ");

    return sbQuery;

  }
  
  /**
   * To load the visit schedule informations.
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */
  public ArrayList loadCalendarViewData(HashMap hmParams) throws AppError {

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();
    
    sbQuery.append(getCalendarViewQuery(hmParams));
    sbQuery.append(" AND T6630.C6630_VOID_FL IS NULL ORDER BY ACTSTARTDATE , ACTSTARTTIME ");

    log.debug("loadCalendarViewData Query === " + sbQuery.toString());
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
    gmDBManager.close();
    log.debug("loadCalendarViewData alReturnBean>>>>>" + alReturn.size());
    return alReturn;

  }

  /**
   * syncPartyActivityJMS - This method will form the message and send through JMS for sync the
   * party activity from CRM activity and Marketing Collateral
   * 
   * @param -
   * @return - Void
   * @exception - AppError
   */
  public void syncPartyActivityJMS(HashMap hmParam) throws AppError {


    GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
    String strConsumerClass =
        GmCommonClass.parseNull(GmJMSConsumerConfigurationBean
            .getJmsConfig("PARTY_ACTIVITY_CONSUMER_CLASS"));
    hmParam.put("HMLNPARAM", hmParam);
    hmParam.put("CONSUMERCLASS", strConsumerClass);
    gmConsumerUtil.sendMessage(hmParam);

  }

  /**
   * savePartyActivitySync - This method is calling from GmCRMPartyActivitySyncConsumerJob.java.This
   * Method will sync the party activity from CRM activity and Marketing Collateral
   * 
   * @param -
   * @return - Void
   * @exception - AppError
   */
  public void savePartyActivitySync(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    log.debug("Entering in savePartyActivitySync");
    gmDBManager.setPrepareString("gm_pkg_crm_activity.gm_sav_party_activity_sync", 2);
    gmDBManager.setString(1, (String) hmParam.get("STROPT"));
    gmDBManager.setString(2, (String) hmParam.get("USERID"));
    gmDBManager.execute();
    gmDBManager.commit();

  }

  /**
   * This method used for sending mail for physician visit users.
   * 
   * @param strTemplate,gmEmailProperties,hmEventDetails
   * @throws AppError ,Exception
   */
  /*
   * public void sendNotification(String strTemplate, GmEmailProperties gmEmailProperties, HashMap
   * hmEventDetails) throws AppError, Exception { log.debug("sendNotification " + strTemplate);
   * gmEmailProperties.setSender(GmCommonClass.getEmailProperty(strTemplate + "." +
   * GmEmailProperties.FROM)); gmEmailProperties.setRecipients((String)
   * hmEventDetails.get("ORGMAIL")); gmEmailProperties.setCc(GmCommonClass
   * .getEmailProperty(strTemplate + "." + GmEmailProperties.CC)); gmEmailProperties
   * .setSubject(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.SUBJECT)
   * .concat((String) hmEventDetails.get("ACTNM")));
   * gmEmailProperties.setMessage(GmCommonClass.getEmailProperty( strTemplate + "." +
   * GmEmailProperties.MESSAGE_BODY).replace("[Creator]", (String) hmEventDetails.get("ORGNM")));
   * gmEmailProperties.setMimeType(GmCommonClass.getEmailProperty(strTemplate + "." +
   * GmEmailProperties.MIME_TYPE)); GmCommonClass.sendMail(gmEmailProperties); }
   */
  
//Fetch the count of restricted Days in Activity calendar for the given dates

  public String chkRestrictedDays(String strStartDt,String strEndDt,String strActId) throws AppError {
	   GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	   HashMap hmJmsParam = new HashMap();
	   String strResult = "";
	   gmDBManager.setPrepareString("gm_pkg_crm_activity.gm_fch_restricted_flag", 4);
	   gmDBManager.setString(1, strStartDt);
	   gmDBManager.setString(2, strEndDt);
	   gmDBManager.setString(3, strActId);
	   gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
	   gmDBManager.execute();
	   strResult = GmCommonClass.parseNull(gmDBManager.getString(4));
	   gmDBManager.close();

	   return strResult;
 }



}
