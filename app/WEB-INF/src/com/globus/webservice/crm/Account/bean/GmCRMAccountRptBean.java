package com.globus.webservice.crm.Account.bean;

import oracle.jdbc.OracleTypes;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.beans.GmBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmCRMAccountRptBean extends GmBean {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
  public GmCRMAccountRptBean(GmDataStoreVO gmDataStoreVO) throws AppError {
	    super(gmDataStoreVO);
	    // TODO Auto-generated constructor stub
  }

	/**
	 * This method used to fetch Surgeon list for account
	 * 
	 * @param hmInput --accid
	 * @return
	 * @throws AppError
	 */
	public String fetchAccountSurgeonRpt(String strAcctId) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strAccid = GmCommonClass.parseNull((String) strAcctId);
		String strReturn = "";
		gmDBManager.setPrepareString("gm_pkg_crm_account_rpt.gm_fch_account_surgeon_rpt", 2);
		gmDBManager.setString(1, strAccid);
		gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
		gmDBManager.execute();
		strReturn = GmCommonClass.parseNull(gmDBManager.getString(2));
		gmDBManager.close();
		return strReturn;
	}
	

	  /**
	   * This method used to fetch the surgical activity by revenue.
	 * @param hmParams
	 * @return
	 * @throws AppError
	 */
	public String fetchAccountSurigcalActivityByRev(HashMap hmParams) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strResult = "";
		GmAccessFilter gmAccessFilter = new GmAccessFilter();
		HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
		String strPartyId = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
		String strFromDate = GmCommonClass.parseNull((String) hmParams.get("FROMDATE"));
		String strToDate = GmCommonClass.parseNull((String) hmParams.get("TODATE"));
		String strAccId = GmCommonClass.parseNull((String) hmParams.get("ACCID"));
		hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
		String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));		
		StringBuffer sbQuery = new StringBuffer();
		 
		sbQuery.append(" SELECT JSON_ARRAYAGG( JSON_OBJECT( 'yearmon' 	value  TO_CHAR(T501.C501_ORDER_DATE ,'YYYYMM'), ");
		sbQuery.append(" 'value' 	value  SUM(T502.C502_ITEM_PRICE * T502.C502_ITEM_QTY),  ");
		sbQuery.append(" 'grpid' 	value  V_SYS.SEGMENT_ID, ");
		sbQuery.append(" 'grpnm' 	value  V_SYS.SEGMENT_NAME,  ");
		sbQuery.append(" 'grpid2' 	value  V_SYS.SYSTEM_ID, ");
		sbQuery.append(" 'grpnm2' 	value  V_SYS.SYSTEM_NM )RETURNING CLOB) ");
		sbQuery.append(" FROM T501_ORDER T501,T502_ITEM_ORDER T502,T208_SET_DETAILS T208,v_segment_system_map V_SYS ");
		sbQuery.append(" WHERE T501.C704_ACCOUNT_ID IN ");
		sbQuery.append(" ( SELECT c704_account_id FROM t704_account WHERE C101_PARTY_ID =  '" + strAccId + "') ");
		sbQuery.append(" AND T501.C501_ORDER_ID                = T502.C501_ORDER_ID ");
//		sbQuery.append(" AND T501.C501_STATUS_FL               = 3 ");
		sbQuery.append(" AND V_SYS.SYSTEM_ID                   = T208.C207_SET_ID ");
		sbQuery.append(" AND T502.C205_PART_NUMBER_ID          = T208.C205_PART_NUMBER_ID ");
		sbQuery.append(" AND T501.C501_VOID_FL                IS NULL ");
		sbQuery.append(" AND T502.C502_VOID_FL                IS NULL ");
		sbQuery.append(" AND T208.C208_VOID_FL                IS NULL ");
		sbQuery.append(" AND T501.C501_DELETE_FL              IS NULL ");
		sbQuery.append(" AND NVL (C901_ORDER_TYPE, -9999) NOT IN ");
		sbQuery.append(" (SELECT C906_RULE_VALUE FROM V901_ORDER_TYPE_GRP ) ");
		sbQuery.append(" AND T501.C501_ORDER_DATE >= TO_DATE('" + strFromDate + "','YYYYMM') ");
		sbQuery.append(" AND ( '" + strPartyId + "'   IS NULL ");
		sbQuery.append(" OR T501.C501_ORDER_ID  IN ");
		sbQuery.append(" (SELECT c6640_ref_id FROM t6640_npi_transaction ");
		sbQuery.append(" WHERE c101_party_surgeon_id = '" + strPartyId + "' ");
		sbQuery.append(" AND c6640_void_fl IS NULL AND c6640_valid_fl ='Y' ))");
		sbQuery.append(" AND T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('" + strToDate + "','YYYYMM')) ");
		if (!strCondition.equals(""))
			sbQuery.append(" AND " + strCondition + " ");
		sbQuery.append(" GROUP BY TO_CHAR(T501.C501_ORDER_DATE ,'YYYYMM'), ");
		sbQuery.append(" V_SYS.SEGMENT_ID, V_SYS.SEGMENT_NAME, V_SYS.SYSTEM_ID, V_SYS.SYSTEM_NM ");
		
		log.debug("fetchAccountSurigcalActivityByRev  Query  " + sbQuery.toString());		
		strResult = gmDBManager.queryJSONRecord(sbQuery.toString());
	    gmDBManager.close();
	    return strResult;
	  }
	
	 /**
	   * This method used to fetch the surgical activity by Procedure.
	 * @param hmParams
	 * @return 
	 * @throws AppError
	 */
	public String fetchAccountSurigcalActivityByPrc(HashMap hmParams) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());		
		GmAccessFilter gmAccessFilter = new GmAccessFilter();
		HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
		String strPartyId = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
		String strFromDate = GmCommonClass.parseNull((String) hmParams.get("FROMDATE"));
		String strToDate = GmCommonClass.parseNull((String) hmParams.get("TODATE"));
		String strAccId = GmCommonClass.parseNull((String) hmParams.get("ACCID"));
		hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
		String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
		String strResult = "";
		
		StringBuffer sbQuery = new StringBuffer();		    
		sbQuery.append(" SELECT JSON_ARRAYAGG( JSON_OBJECT( ");
		sbQuery.append(" 'yearmon' 	value  TO_CHAR(T501.C501_ORDER_DATE,'YYYYMM'), ");
		sbQuery.append(" 'value' 	value  COUNT(T501.C501_ORDER_ID), ");
		sbQuery.append(" 'grpid' 	value  V_SYS.SEGMENT_ID, ");
		sbQuery.append(" 'grpnm' 	value  V_SYS.SEGMENT_NAME,  ");
		sbQuery.append(" 'grpid2' 	value  V_SYS.SYSTEM_ID, ");
		sbQuery.append(" 'grpnm2' 	value  V_SYS.SYSTEM_NM ) RETURNING CLOB) ");
		sbQuery.append(" FROM T501_ORDER T501,T501B_ORDER_BY_SYSTEM T501B,v_segment_system_map V_SYS ");
		sbQuery.append(" WHERE t501.C704_ACCOUNT_ID IN(SELECT c704_account_id FROM t704_account  WHERE C101_PARTY_ID = '" + strAccId + "') ");
		sbQuery.append(" AND ( '" + strPartyId + "' IS NULL OR T501.C501_ORDER_ID  IN ");
		sbQuery.append(" (SELECT c6640_ref_id FROM t6640_npi_transaction WHERE c101_party_surgeon_id = '" + strPartyId + "' ");
		sbQuery.append(" AND c6640_void_fl IS NULL AND c6640_valid_fl ='Y' )) ");
		sbQuery.append(" AND T501.C501_ORDER_ID                = T501B.C501_ORDER_ID ");
		sbQuery.append(" AND T501B.C207_SYSTEM_ID              = V_SYS.SYSTEM_ID ");
//		sbQuery.append(" AND T501.C501_STATUS_FL               = 3 ");
		sbQuery.append(" AND T501.C501_VOID_FL                IS NULL ");
		sbQuery.append(" AND T501.C501_DELETE_FL              IS NULL ");
		sbQuery.append(" AND NVL (C901_ORDER_TYPE, -9999) NOT IN ");
		sbQuery.append(" (SELECT C906_RULE_VALUE FROM V901_ORDER_TYPE_GRP ) ");
		sbQuery.append(" AND T501B.C501B_VOID_FL  IS NULL ");
		sbQuery.append(" AND T501.C501_ORDER_DATE >= TO_DATE('" + strFromDate + "','YYYYMM') ");
		sbQuery.append(" AND T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('" + strToDate + "','YYYYMM')) ");
		if (!strCondition.equals(""))
			sbQuery.append(" AND " + strCondition + " ");
		sbQuery.append(" GROUP BY TO_CHAR(T501.C501_ORDER_DATE,'YYYYMM'), ");
		sbQuery.append(" V_SYS.SEGMENT_ID, V_SYS.SEGMENT_NAME, V_SYS.SYSTEM_ID, V_SYS.SYSTEM_NM ");
		
		log.debug("fetchAccountSurigcalActivityByRev  Query  " + sbQuery.toString());
		
		strResult = gmDBManager.queryJSONRecord(sbQuery.toString());
	    gmDBManager.close();
	    return strResult;

	  }
	
	/**
	 *  This method used to fetch the DO Count based an account and Surgeon.
	 * @param hmParams
	 * @return 
	 * @throws AppError
	 */
	public String fetchAccountsurgcialActivityDOCnt(HashMap hmParams) throws AppError {
	    String hmResult = "";
	    log.debug("alReturn>>>>" + hmParams);
	    GmAccessFilter gmAccessFilter = new GmAccessFilter();
		HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
		hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
		String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
	    String strPartyId = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
	    String strFromDate = GmCommonClass.parseNull((String) hmParams.get("FROMDATE"));
	    String strToDate = GmCommonClass.parseNull((String) hmParams.get("TODATE"));
	    String strAccountID = GmCommonClass.parseNull((String) hmParams.get("ACCID"));
	    String strSegmentStr = GmCommonClass.parseNull((String) hmParams.get("SEGMENTSTR"));
	    String strSystemStr = GmCommonClass.parseNull((String) hmParams.get("SYSTEMSTR"));
	    GmDBManager gmDBManager = new GmDBManager();
	    StringBuffer sbQuery = new StringBuffer();

	    sbQuery.append(" SELECT JSON_OBJECT( 'docount' 	value COUNT(ORD)) ");
	    sbQuery.append("FROM ( SELECT DISTINCT T501.C501_ORDER_ID ORD FROM T501_ORDER T501,T502_ITEM_ORDER T502,T208_SET_DETAILS T208,v_segment_system_map V_SYS ");
	    sbQuery.append(" WHERE T501.C704_ACCOUNT_ID IN (SELECT c704_account_id FROM t704_account "); 
		sbQuery.append(" WHERE C101_PARTY_ID =  '" + strAccountID + "') AND ( '" + strPartyId + "' IS NULL OR T501.C501_ORDER_ID  IN ");
	    sbQuery.append(" (SELECT c6640_ref_id FROM t6640_npi_transaction WHERE c101_party_surgeon_id = '" + strPartyId + "' ");
	    sbQuery.append(" AND c6640_void_fl IS NULL AND c6640_valid_fl='Y')) ");
	    sbQuery.append("  AND T501.C501_ORDER_ID                = T502.C501_ORDER_ID ");
	    sbQuery.append(" AND V_SYS.SYSTEM_ID                   = T208.C207_SET_ID ");
	    sbQuery.append(" AND T502.C205_PART_NUMBER_ID          = T208.C205_PART_NUMBER_ID ");
//	    sbQuery.append(" AND T501.C501_STATUS_FL               = 3 ");
	    sbQuery.append(" AND T502.C502_VOID_FL                IS NULL ");
	    sbQuery.append(" AND T208.C208_VOID_FL                IS NULL ");
	    sbQuery.append(" AND T501.C501_PARENT_ORDER_ID        IS NULL ");
	    sbQuery.append(" AND T501.C501_VOID_FL                IS NULL ");
	    sbQuery.append(" AND T501.C501_DELETE_FL              IS NULL ");
	    sbQuery.append(" AND NVL (C901_ORDER_TYPE, -9999) NOT IN (SELECT C906_RULE_VALUE FROM V901_ORDER_TYPE_GRP) ");
	    sbQuery.append(" AND T501.C501_ORDER_DATE >= TO_DATE('" + strFromDate + "','YYYYMM') ");
	    sbQuery.append(" AND T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('" + strToDate + "','YYYYMM')) ");
	    if (!strSystemStr.equals(""))
	      sbQuery.append(" AND V_SYS.SYSTEM_ID                        IN (" + strSystemStr + ") ");
	    if (!strSegmentStr.equals(""))
	      sbQuery.append(" AND V_SYS.SEGMENT_ID                       IN (" + strSegmentStr + ") ");
	    if (!strCondition.equals(""))
			sbQuery.append(" AND " + strCondition + " ");
	    sbQuery.append(") ");
	    log.debug("fetchAccountsurgcialActivityDOCnt  Query  " + sbQuery.toString());
	    hmResult = gmDBManager.queryJSONRecord(sbQuery.toString());
	    gmDBManager.close();
	    
	    return hmResult;

	  }
	
		/**
		   * This method used to fetch the surgical activity system DO.
		 * @param hmParams
		 * @return
		 * @throws AppError
		 */
	  public String fetchAccountsurgcialActivitySysDO(HashMap hmParams) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		GmAccessFilter gmAccessFilter = new GmAccessFilter();
		HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
		
		String strPartyId = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
		String strFromDate = GmCommonClass.parseNull((String) hmParams.get("FROMDATE"));
		String strToDate = GmCommonClass.parseNull((String) hmParams.get("TODATE"));
		String strAccId = GmCommonClass.parseNull((String) hmParams.get("ACCID"));
		String strSysId = GmCommonClass.parseNull((String) hmParams.get("SYSTEMID"));
		
		hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
		String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
		    log.debug(" DOlists strCondition>>>  "+strCondition);
		String strResult = "";
		
		StringBuffer sbQuery = new StringBuffer();		
		sbQuery.append(" SELECT JSON_ARRAYAGG( JSON_OBJECT( ");
		sbQuery.append(" 'orderid' 	value T.ORDERID, ");
		sbQuery.append(" 'orderdt' 	value T.ORDERDT, ");
		sbQuery.append(" 'partnum' 	value T.PARTNUM,  ");
		sbQuery.append(" 'pdesc' 	value T205.C205_PART_NUM_DESC, ");
		sbQuery.append(" 'qty'      value T.QUANTITY,  ");
		sbQuery.append(" 'price' 	value T.PRICE, ");
		sbQuery.append(" 'total'  	value (T.QUANTITY * T.PRICE), ");
		sbQuery.append(" 'acctid'   value T.ACCTID ");
		sbQuery.append(" )ORDER BY T.ORDERDT,T.ORDERDT,T205.C205_PART_NUM_DESC,T.QUANTITY  RETURNING CLOB) ");
		sbQuery.append(" FROM (SELECT T501.C501_ORDER_ID ORDERID, ");
		sbQuery.append(" TO_CHAR(T501.C501_ORDER_DATE,'MM/DD/YYYY') ORDERDT, ");
		sbQuery.append(" T501.C501_ORDER_DATE, T501.C704_ACCOUNT_ID ACCTID, T501.C703_SALES_REP_ID REPID, ");
		sbQuery.append(" T502.C205_PART_NUMBER_ID PARTNUM, SUM(T502.C502_ITEM_QTY) QUANTITY, T502.C502_ITEM_PRICE PRICE ");
		sbQuery.append(" FROM T501_ORDER T501, T502_ITEM_ORDER T502, T208_SET_DETAILS T208 ");
		sbQuery.append(" WHERE T501.C704_ACCOUNT_ID IN(SELECT c704_account_id FROM t704_account WHERE C101_PARTY_ID = '" + strAccId + "') ");
		sbQuery.append(" AND ( '" + strPartyId + "' IS NULL OR T501.C501_ORDER_ID  IN ");
		sbQuery.append(" (SELECT c6640_ref_id FROM t6640_npi_transaction ");
		sbQuery.append(" WHERE c101_party_surgeon_id = '" + strPartyId + "' AND c6640_void_fl IS NULL ");
		sbQuery.append(" AND c6640_valid_fl          ='Y' )) ");
		sbQuery.append(" AND T501.C501_ORDER_ID                = T502.C501_ORDER_ID ");
		sbQuery.append(" AND T502.C205_PART_NUMBER_ID          = T208.C205_PART_NUMBER_ID ");
//		sbQuery.append(" AND T501.C501_STATUS_FL               = 3 ");
		sbQuery.append(" AND T502.C502_VOID_FL                IS NULL ");
		sbQuery.append(" AND T208.C208_VOID_FL                IS NULL ");
		sbQuery.append(" AND T501.C501_VOID_FL                IS NULL ");
		sbQuery.append(" AND T501.C501_DELETE_FL              IS NULL ");
		sbQuery.append(" AND NVL (C901_ORDER_TYPE, -9999) NOT IN ");
		sbQuery.append(" (SELECT C906_RULE_VALUE FROM V901_ORDER_TYPE_GRP ) ");
		sbQuery.append(" AND T501.C501_ORDER_DATE >= TO_DATE('" + strFromDate + "','YYYYMM') ");
		sbQuery.append(" AND T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('" + strToDate + "','YYYYMM')) ");
		sbQuery.append(" AND T208.C207_SET_ID     IN ('" + strSysId + "') ");
		 if (!strCondition.equals(""))
				sbQuery.append(" AND " + strCondition + " ");
		sbQuery.append(" GROUP BY T501.C501_ORDER_ID, ");
		sbQuery.append(" T501.C501_ORDER_DATE, T501.C704_ACCOUNT_ID, T501.C703_SALES_REP_ID, ");
		sbQuery.append(" T502.C205_PART_NUMBER_ID, T502.C502_ITEM_PRICE ) T, ");
		sbQuery.append(" T205_PART_NUMBER T205 WHERE T.PARTNUM = T205.C205_PART_NUMBER_ID ");
		
		log.debug("DOlist sbQuery>> "+sbQuery.toString());
		
		strResult = gmDBManager.queryJSONRecord(sbQuery.toString());
	    gmDBManager.close();
	    
	    return strResult;
	  }
	  
	  /**
	   * This method used to fetch the Account surgical calendar data.
	 * @param hmParams
	 * @return
	 * @throws AppError
	 */
	public String fetchAcctsurgcialCalInfo(HashMap hmParams) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		GmAccessFilter gmAccessFilter = new GmAccessFilter();
		HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
		log.debug("hmParams>>>" + hmParams);
		
		String strPartyId = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
		String strFromDate = GmCommonClass.parseNull((String) hmParams.get("FROMDATE"));
		String strToDate = GmCommonClass.parseNull((String) hmParams.get("TODATE"));
		
		hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
		String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
		log.debug(" CalendarInfo strCondition>>>  "+strCondition);
		String strResult = "";
	    
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("SELECT JSON_ARRAYAGG( JSON_OBJECT( ");
		sbQuery.append(" 'surgdt' VALUE TO_CHAR(T.SURGDT,'YYYY-MM-DD'), 'segid' VALUE T.SEGID, ");
		sbQuery.append(" 'segnm' VALUE T.SEGNM, 'sysid' VALUE T.SYSID, 'sysnm' VALUE T.SYSNM, 'cnt' VALUE T.CNT ) ");
		sbQuery.append("  RETURNING CLOB) FROM(SELECT NVL(T501.C501_SURGERY_DATE, T501.C501_ORDER_DATE) SURGDT, ");
		sbQuery.append("  V_SYS.SEGMENT_ID  SEGID, V_SYS.SEGMENT_NAME  SEGNM, V_SYS.SYSTEM_ID SYSID, V_SYS.SYSTEM_NM SYSNM,");
		sbQuery.append("  COUNT(1) CNT FROM   T501_ORDER T501, T501B_ORDER_BY_SYSTEM T501B, V_SEGMENT_SYSTEM_MAP V_SYS ");
	    sbQuery.append("  WHERE  T501.C704_ACCOUNT_ID IN (SELECT C704_ACCOUNT_ID FROM   T704_ACCOUNT WHERE  C101_PARTY_ID = '" + strPartyId + "')");
		sbQuery.append("  AND NVL(T501.C501_SURGERY_DATE, T501.C501_ORDER_DATE) >= TO_DATE('" + strFromDate + "', 'YYYYMM')");	
		sbQuery.append("  AND NVL(T501.C501_SURGERY_DATE, T501.C501_ORDER_DATE) <= LAST_DAY( TO_DATE('" + strToDate + "', 'YYYYMM'))");	
		sbQuery.append("  AND T501.C501_ORDER_ID = T501B.C501_ORDER_ID AND T501B.C207_SYSTEM_ID = V_SYS.SYSTEM_ID ");
		sbQuery.append("  AND NVL (C901_ORDER_TYPE, -9999) NOT IN (SELECT C906_RULE_VALUE FROM   V901_ORDER_TYPE_GRP) ");          
		sbQuery.append("  AND T501.C501_PARENT_ORDER_ID IS NULL AND T501.C501_VOID_FL IS NULL AND T501.C501_DELETE_FL IS NULL ");
	    sbQuery.append("  AND T501B.C501B_VOID_FL IS NULL");
	    if (!strCondition.equals(""))
			sbQuery.append(" AND " + strCondition + " ");
	    sbQuery.append("  GROUP BY NVL(T501.C501_SURGERY_DATE, T501.C501_ORDER_DATE),   ");      
	    sbQuery.append("  V_SYS.SEGMENT_ID, V_SYS.SEGMENT_NAME, V_SYS.SYSTEM_ID, V_SYS.SYSTEM_NM UNION ALL ");
	    sbQuery.append("  SELECT NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE) SURGDT, 0  SEGID,	'ALL DO' SEGNM, '0'  SYSID, 'ALL DO'  SYSNM, ");                    
	    sbQuery.append("  COUNT(1) CNT 	FROM   T501_ORDER T501  WHERE  T501.C704_ACCOUNT_ID IN (SELECT C704_ACCOUNT_ID FROM ");                    
	    sbQuery.append("  T704_ACCOUNT WHERE  C101_PARTY_ID = '" + strPartyId + "' )");           
	    sbQuery.append("  AND NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE) >= TO_DATE('" + strFromDate + "','YYYYMM') ");		   
	    sbQuery.append("  AND NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE) <= LAST_DAY(TO_DATE('" + strToDate + "','YYYYMM'))  ");          	   
	    sbQuery.append("  AND NVL (C901_ORDER_TYPE, -9999) NOT IN ( SELECT C906_RULE_VALUE FROM   V901_ORDER_TYPE_GRP )  ");                 
	    sbQuery.append("  AND T501.C501_VOID_FL IS NULL AND T501.C501_DELETE_FL IS NULL ");  
	    if (!strCondition.equals(""))
				sbQuery.append(" AND " + strCondition + " ");
	    sbQuery.append("  GROUP BY NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE)) T "); 
    	log.debug("Account Calendar Info sbQuery>> "+sbQuery.toString());
		
		strResult = gmDBManager.queryJSONRecord(sbQuery.toString());
	    gmDBManager.close();
	    
	    return strResult;
	  }
	
	  /**
		   * This method used to get system do details
		 * @param hmParams
		 * @return
		 * @throws AppError
	  */
	  public String fetchAcctsurgcialSysDODtl(HashMap hmParams) throws AppError {
		  String hmResult = "";
		  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		  GmAccessFilter gmAccessFilter = new GmAccessFilter();
		  HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
		  log.debug("hmParams>>>" + hmParams);
		  
		  String strPartyId = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
		  String strOrdDate = GmCommonClass.parseNull((String) hmParams.get("ORDDATE"));
		  String strSysId = GmCommonClass.parseNull((String) hmParams.get("SYSTEMID"));
		  
		  hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
		  String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
		  log.debug(" SysDO Detail strCondition>>>  "+strCondition);
			
		  
		  StringBuffer sbQuery = new StringBuffer();
		  
		  sbQuery.append("  SELECT   JSON_ARRAYAGG(JSON_OBJECT( "); 
		  sbQuery.append(" 'acctnm' value GET_PARENT_ACCOUNT_NAME(T.ACCTID), 'surgnm' 	VALUE T.SURGNM,'surgdt' VALUE TO_CHAR(T.SURGDT,'YYYY-MM-DD'), 'parentord' VALUE T.PARENTORD, 'segmentnm' VALUE V_SEG.SEGMENT_NAME,"); 
		  sbQuery.append(" 'segmentid' VALUE V_SEG.SEGMENT_ID,'sysnm' 	VALUE V_SEG.SYSTEM_NM, 'ord' 		VALUE T.ORD, "); 
		  sbQuery.append("	'pnum' 	VALUE T.PNUM, 'partdesc' 	VALUE GET_PARTNUM_DESC(T.PNUM) , 'qty' 		VALUE T.QTY, "); 
		  sbQuery.append("	'price' VALUE T.PRICE,'total' 	VALUE T.QTY * T.PRICE)"); 
		  sbQuery.append(" ORDER BY T.SURGNM, T.PARENTORD, V_SEG.SEGMENT_ID, V_SEG.SEGMENT_NAME, V_SEG.SYSTEM_NM, T.ORD, T.PNUM, T.QTY RETURNING CLOB) "); 
		  sbQuery.append("  FROM(SELECT NVL(T501.C501_PARENT_ORDER_ID, T501.C501_ORDER_ID) PARENTORD, "); 
		  sbQuery.append(" T501.C501_ORDER_ID  ORD, NVL(T501.C501_SURGERY_DATE, T501.C501_ORDER_DATE)  SURGDT, "); 
		  sbQuery.append(" T501.C704_ACCOUNT_ID ACCTID, "); 
          sbQuery.append(" GET_SET_ID_FROM_PART(T502.C205_PART_NUMBER_ID) SYSTEMID, ");
          sbQuery.append(" T502.C205_PART_NUMBER_ID  PNUM, T502.C502_ITEM_QTY QTY, ");
          sbQuery.append(" T502.C502_ITEM_PRICE PRICE,");
          sbQuery.append(" SURGEONS.SURGEONNM SURGNM ");
          sbQuery.append(" FROM  ( SELECT   T6640.C6640_REF_ID ORDERID,LISTAGG(T6640.C6640_SURGEON_NAME, ',') ");
          sbQuery.append(" WITHIN GROUP(ORDER BY T6640.C6640_SURGEON_NAME) SURGEONNM FROM T6640_NPI_TRANSACTION T6640 ");
          sbQuery.append(" WHERE    T6640.C6640_VALID_FL = 'Y' AND T6640.C6640_VOID_FL IS NULL ");
          sbQuery.append(" GROUP BY T6640.C6640_REF_ID ) SURGEONS, ");
		  sbQuery.append("  T501_ORDER T501, T502_ITEM_ORDER T502");
			if (!strSysId.equals("0")){
				sbQuery.append(" ,T501B_ORDER_BY_SYSTEM T501B");
			}
			sbQuery.append("  WHERE T501.C704_ACCOUNT_ID IN    (SELECT c704_account_id   FROM t704_account   WHERE C101_PARTY_ID = "+ strPartyId +") ");
			sbQuery.append(" AND SURGEONS.ORDERID(+) = NVL(T501.C501_PARENT_ORDER_ID, T501.C501_ORDER_ID) ");
			sbQuery.append(" AND T501.C501_ORDER_ID = T502.C501_ORDER_ID ");
			sbQuery.append(" AND NVL (C901_ORDER_TYPE, -9999) NOT IN (SELECT C906_RULE_VALUE FROM V901_ORDER_TYPE_GRP )");
			sbQuery.append(" AND T502.C502_VOID_FL IS NULL ");
			sbQuery.append(" AND NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE) = TO_DATE("+"'"+ strOrdDate +"'"+", 'MM/DD/YYYY')");
			sbQuery.append(" AND T501.C501_VOID_FL IS NULL AND T501.C501_DELETE_FL IS NULL ");
			if (!strSysId.equals("0")){
				sbQuery.append(" AND NVL(T501.C501_PARENT_ORDER_ID, ");
				sbQuery.append(" T501.C501_ORDER_ID) = T501B.C501_ORDER_ID ");
				sbQuery.append(" AND T501B.C207_SYSTEM_ID ="+ strSysId +"");
				sbQuery.append(" AND T501B.C501B_VOID_FL IS NULL ");
			}
			if (!strCondition.equals(""))
				sbQuery.append(" AND " + strCondition + " ");
			sbQuery.append(" )T,V_SEGMENT_SYSTEM_MAP V_SEG WHERE T.SYSTEMID = V_SEG.SYSTEM_ID(+) ");
			log.debug("fetchSysDetailsInfo SbQUERY>>>>>" + sbQuery);
			hmResult = gmDBManager.queryJSONRecord(sbQuery.toString());
		    gmDBManager.close();
		    return hmResult;
	  }



}
