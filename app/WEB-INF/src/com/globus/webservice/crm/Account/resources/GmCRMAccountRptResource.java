package com.globus.webservice.crm.Account.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.annotation.JacksonFeatures;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.Account.bean.GmCRMAccountRptBean;
import com.globus.webservice.crm.valueobject.GmAccountRptVO;

/**
 * @author TMuthusamy
 * GmCRMAccountRptResource ---- Surgeon Reports for Account
 */
@Path("accountrpt")
public class GmCRMAccountRptResource extends GmResource {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * This method used to fetch CRM Surgeon Report list for particular Account
	 * @param strInput
	 * @return
	 * @throws AppError, JsonParseException,JsonMappingException,IOException
	 */
	@POST
	@Path("surgeonrpt")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@JacksonFeatures(serializationEnable = SerializationFeature.WRAP_ROOT_VALUE, deserializationEnable = DeserializationFeature.UNWRAP_ROOT_VALUE)
	public String fetchAccountSurgeonReport(String strInput) throws AppError,
			JsonParseException, JsonMappingException, IOException {
		log.debug("strInput" + strInput);

		GmAccountRptVO gmAccountRptVO = new GmAccountRptVO();
		GmWSUtil gmWSUtil = new GmWSUtil();
		
		GmCRMAccountRptBean gmCRMAccountRptBean = new GmCRMAccountRptBean(gmAccountRptVO);
		ObjectMapper mapper = new ObjectMapper();
		gmAccountRptVO = mapper.readValue(strInput, GmAccountRptVO.class);
		validateToken(gmAccountRptVO.getToken());

		String strAcctId = GmCommonClass.parseNull((String) gmAccountRptVO
				.getAccid());
		String strReturnJSON = gmCRMAccountRptBean
				.fetchAccountSurgeonRpt(strAcctId);

		return strReturnJSON;

	}
	
	@POST
	@Path("surigicalact")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@JacksonFeatures(serializationEnable = SerializationFeature.WRAP_ROOT_VALUE, deserializationEnable = DeserializationFeature.UNWRAP_ROOT_VALUE)
	public String fetchAccountSurgicalActivity(String strInput) throws AppError,
				JsonParseException, JsonMappingException, IOException {
		log.debug("strInput" + strInput);
		String strResult = "";
		GmAccountRptVO gmAccountRptVO = new GmAccountRptVO();
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmCRMAccountRptBean gmCRMAccountRptBean = new GmCRMAccountRptBean(gmAccountRptVO);
		ObjectMapper mapper = new ObjectMapper();
		gmAccountRptVO = mapper.readValue(strInput, GmAccountRptVO.class);
		validateToken(gmAccountRptVO.getToken());
		String strpartyid = gmAccountRptVO.getPartyid();
		HashMap hmInput = new HashMap();

	    gmWSUtil.parseJsonToVO(strInput, gmAccountRptVO);
	    hmInput = gmWSUtil.getHashMapFromVO(gmAccountRptVO);
	    HashMap hmParam =
	            GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
	                (new GmSalesDashReqVO())));
	    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
	    hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
	    hmInput.put("HMPARAM", hmParam);
	    hmInput.put("USERDEPTID", gmUserVO.getDeptid());
	    
		if (gmAccountRptVO.getStropt().equalsIgnoreCase("REV")) {
			strResult = gmCRMAccountRptBean.fetchAccountSurigcalActivityByRev(hmInput);
		} else if (gmAccountRptVO.getStropt().equalsIgnoreCase("PRC")) {
			strResult = gmCRMAccountRptBean.fetchAccountSurigcalActivityByPrc(hmInput);
		}
		return strResult;
	}
	
	

	  /**
	 * @param strInput
	 * // To get DO count for an account
	 * @return
	 * @throws AppError
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@POST
	@Path("surigicalactdocnt")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@JacksonFeatures(serializationEnable = SerializationFeature.WRAP_ROOT_VALUE, deserializationEnable = DeserializationFeature.UNWRAP_ROOT_VALUE)
	public String fetchAccountsurgcialActivityDOCnt(String strInput)
			throws AppError, JsonParseException, JsonMappingException,
			IOException {
		log.debug("surigicalactdocnt " + strInput);
		String strResult = "";
		GmAccountRptVO gmAccountRptVO = new GmAccountRptVO();
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		GmCRMAccountRptBean gmCRMAccountRptBean = new GmCRMAccountRptBean(gmAccountRptVO);
		ObjectMapper mapper = new ObjectMapper();
		gmAccountRptVO = mapper.readValue(strInput, GmAccountRptVO.class);
		validateToken(gmAccountRptVO.getToken());
		String strpartyid = gmAccountRptVO.getPartyid();
		HashMap hmInput = new HashMap();

	    gmWSUtil.parseJsonToVO(strInput, gmAccountRptVO);
	    hmInput = gmWSUtil.getHashMapFromVO(gmAccountRptVO);
	    HashMap hmParam =
	            GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
	                (new GmSalesDashReqVO())));
	    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
	    hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
	    hmInput.put("HMPARAM", hmParam);
	    hmInput.put("USERDEPTID", gmUserVO.getDeptid());      

		strResult = gmCRMAccountRptBean.fetchAccountsurgcialActivityDOCnt(hmInput);
		return strResult;
	}
	
	@POST
	@Path("surgsystemdo")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@JacksonFeatures(serializationEnable = SerializationFeature.WRAP_ROOT_VALUE, deserializationEnable = DeserializationFeature.UNWRAP_ROOT_VALUE)
	public String fetchAccountsurgcialActivitySysDO(String strInput) throws AppError,
			JsonParseException, JsonMappingException, IOException {
		log.debug("surgsystemdo " + strInput);
		String strResult = "";
		GmAccountRptVO gmAccountRptVO = new GmAccountRptVO();
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		GmCRMAccountRptBean gmCRMAccountRptBean = new GmCRMAccountRptBean(gmAccountRptVO);
		ObjectMapper mapper = new ObjectMapper();
		gmAccountRptVO = mapper.readValue(strInput, GmAccountRptVO.class);
		validateToken(gmAccountRptVO.getToken());
		String strpartyid = gmAccountRptVO.getPartyid();
		HashMap hmInput = new HashMap();

	    gmWSUtil.parseJsonToVO(strInput, gmAccountRptVO);
	    hmInput = gmWSUtil.getHashMapFromVO(gmAccountRptVO);
	    HashMap hmParam =
	            GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
	                (new GmSalesDashReqVO())));
	    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
	    hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
	    hmInput.put("HMPARAM", hmParam);
	    hmInput.put("USERDEPTID", gmUserVO.getDeptid());

		strResult = gmCRMAccountRptBean.fetchAccountsurgcialActivitySysDO(hmInput);
		return strResult;
	}
	
	 /**
		 * @param strInput
		 * // To get Surgical Calendar Info in account surgical detail
		 * @return
		 * @throws AppError
		 * @throws JsonParseException
		 * @throws JsonMappingException
		 * @throws IOException
		 */
	@POST
	@Path("surgcalinfo")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String fetchAccountsurgcialCalInfo(String strInput) throws AppError,
			JsonParseException, JsonMappingException, IOException {
		
		log.debug("strInput=>" + strInput);
		GmAccountRptVO gmAccountRptVO = new GmAccountRptVO();
		ObjectMapper mapper = new ObjectMapper();
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		// Converting JSON input string into VO
		gmAccountRptVO = mapper.readValue(strInput, GmAccountRptVO.class);

		// Validate whether the user token active?
		validateToken(gmAccountRptVO.getToken());
		String strPartyid = gmAccountRptVO.getPartyid();

		GmCRMAccountRptBean gmCRMAccountRptBean = new GmCRMAccountRptBean(gmAccountRptVO);
		
		HashMap hmInput = new HashMap();

		hmInput = gmWSUtil.getHashMapFromVO(gmAccountRptVO);
		HashMap hmParam =
	            GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
	                (new GmSalesDashReqVO())));
	    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
	    hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
	    hmInput.put("HMPARAM", hmParam);
	    hmInput.put("USERDEPTID", gmUserVO.getDeptid());

		String strResult = gmCRMAccountRptBean.fetchAcctsurgcialCalInfo(hmInput);

		return strResult;
	}
	
	 /**
		 * @param strInput
		 * // To get System Details in account surgical detail
		 * @return
		 * @throws AppError
		 * @throws JsonParseException
		 * @throws JsonMappingException
		 * @throws IOException
		 */
	@POST
	@Path("surgsysdodtl")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String fetchAccountsurgcialSysDODtl(String strInput) throws AppError,
			JsonParseException, JsonMappingException, IOException {
		
		log.debug("strInput=>" + strInput);
		GmAccountRptVO gmAccountRptVO = new GmAccountRptVO();
		ObjectMapper mapper = new ObjectMapper();
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		// Converting JSON input string into VO
		gmAccountRptVO = mapper.readValue(strInput, GmAccountRptVO.class);

		// Validate whether the user token active?
		validateToken(gmAccountRptVO.getToken());
		String strPartyid = gmAccountRptVO.getPartyid();

		GmCRMAccountRptBean gmCRMAccountRptBean = new GmCRMAccountRptBean(gmAccountRptVO);
		
		HashMap hmInput = new HashMap();

		hmInput = gmWSUtil.getHashMapFromVO(gmAccountRptVO);
		HashMap hmParam =
	            GmCommonClass.parseNullHashMap(gmProdCatUtil.populateCRMSalesParam(gmUserVO,
	                (new GmSalesDashReqVO())));
	    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
	    hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
	    hmInput.put("HMPARAM", hmParam);
	    hmInput.put("USERDEPTID", gmUserVO.getDeptid());

		String strResult = gmCRMAccountRptBean.fetchAcctsurgcialSysDODtl(hmInput);

		return strResult;
	}

	



}
