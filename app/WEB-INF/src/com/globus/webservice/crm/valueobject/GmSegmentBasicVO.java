package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSegmentBasicVO extends GmDataStoreVO {

  private String segid = "";
  private String segnm = "";




  public String getSegid() {
    return segid;
  }

  public void setSegid(String segid) {
    this.segid = segid;
  }

  public String getSegnm() {
    return segnm;
  }

  public void setSegnm(String segnm) {
    this.segnm = segnm;
  }

  public List<GmSegmentBasicVO> getListGmSegmentBasicVO() {
    return listGmSegmentBasicVO;
  }

  public void setListGmSegmentBasicVO(List<GmSegmentBasicVO> listGmSegmentBasicVO) {
    this.listGmSegmentBasicVO = listGmSegmentBasicVO;
  }

  List<GmSegmentBasicVO> listGmSegmentBasicVO = null;

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("SEGID", "SEGID");
    properties.put("SEGNM", "SEGNM");
    return properties;
  }
}
