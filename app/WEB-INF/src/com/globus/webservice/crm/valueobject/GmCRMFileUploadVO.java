package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMFileUploadVO extends GmDataStoreVO {

  private String fileid = "";
  private String refid = "";
  private String filename = "";
  private String deletefl = "";
  private String reftype = "";
  private String reftypenm = "";
  private String type = "";
  private String refgroup = "";
  private String filetitle = "";
  private String updateddate = "";
  private String updatedby = "";
  private String path = "";
  /**
 * @return the path
 */
public String getPath() {
	return path;
}


/**
 * @param path the path to set
 */
public void setPath(String path) {
	this.path = path;
}


private List<GmCRMFileUploadVO> listGmCRMFileUploadVO = null;



  public String getReftypenm() {
    return reftypenm;
  }


  public void setReftypenm(String reftypenm) {
    this.reftypenm = reftypenm;
  }


  public String getFileid() {
    return fileid;
  }


  public void setFileid(String fileid) {
    this.fileid = fileid;
  }

  public String getRefid() {
    return refid;
  }


  public void setRefid(String refid) {
    this.refid = refid;
  }


  public String getFilename() {
    return filename;
  }


  public void setFilename(String filename) {
    this.filename = filename;
  }


  public String getDeletefl() {
    return deletefl;
  }


  public void setDeletefl(String deletefl) {
    this.deletefl = deletefl;
  }



  public String getReftype() {
    return reftype;
  }


  public void setReftype(String reftype) {
    this.reftype = reftype;
  }


  public String getType() {
    return type;
  }


  public void setType(String type) {
    this.type = type;
  }


  public String getRefgroup() {
    return refgroup;
  }


  public void setRefgroup(String refgroup) {
    this.refgroup = refgroup;
  }


  public String getFiletitle() {
    return filetitle;
  }


  public void setFiletitle(String filetitle) {
    this.filetitle = filetitle;
  }


  public String getUpdateddate() {
    return updateddate;
  }


  public void setUpdateddate(String updateddate) {
    this.updateddate = updateddate;
  }


  public String getUpdatedby() {
    return updatedby;
  }


  public void setUpdatedby(String updatedby) {
    this.updatedby = updatedby;
  }


  public List<GmCRMFileUploadVO> getListGmCRMFileUploadVO() {
    return listGmCRMFileUploadVO;
  }


  public void setListGmCRMFileUploadVO(List<GmCRMFileUploadVO> listGmCRMFileUploadVO) {
    this.listGmCRMFileUploadVO = listGmCRMFileUploadVO;
  }

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("FILEID", "FILEID");
    properties.put("REFID", "REFID");
    properties.put("FILENAME", "FILENAME");
    properties.put("DELETFL", "DELETFL");
    properties.put("REFTYPE", "REFTYPE");
    properties.put("REFTYPENM", "REFTYPENM");
    properties.put("TYPE", "TYPE");
    properties.put("REFGROUP", "REFGROUP");
    properties.put("FILETITLE", "FILETITLE");
    properties.put("UPDATEDDATE", "UPDATEDDATE");
    properties.put("UPDATEDBY", "UPDATEDBY");
    properties.put("PATH", "PATH");
    return properties;
  }

}
