package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class GmCRMAttendeeMailVO extends GmDataStoreVO{
	private String tradeInfo = "";
	private String msg = "";
	private String attendeeMail = "";
	private String mailResult = "";
	//PC-4592
	private String subject = "";
	
	
	/**
	 * @return the tradeInfo
	 */
	public String getTradeinfo() {
		return tradeInfo;
	}
	
	/**
	 * @param setTradeinfo the tradeInfo to set
	 */
	public void setTradeinfo(String tradeInfo) {
		this.tradeInfo = tradeInfo;
	}
	
	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param setMsg the setMsg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * @return the attendeeMail
	 */
	public String getAttendeemail() {
		return attendeeMail;
	}
	
	/**
	 * @param attendeeMail the attendeeMail to set
	 */
	public void setAttendeemail(String attendeeMail) {
		this.attendeeMail = attendeeMail;
	}
	
	/**
	 * @return the mailResult
	 */
	public String getMailResult() {
		return mailResult;
	}
	
	/**
	 * @param mailResult the mailResult to set
	 */
	public void setMailResult(String mailResult) {
		this.mailResult = mailResult;
	}
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}	
	@Transient
	public Properties getMappingProperties() {
		Properties properties = new Properties();
		properties.put("TRADEINFO", "TRADEINFO");
		properties.put("MSG", "MSG");
		properties.put("ATTENDEEMAIL", "ATTENDEEMAIL");
		properties.put("MAILRESULT", "MAILRESULT");
		properties.put("SUBJECT", "SUBJECT");
		return properties;
	}

	
}

