package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMSurgeonActivityVO extends GmDataStoreVO {

  private String partyid = "";
  private String activityid = "";
  private String activitytypeid = "";
  private String activitytype = "";
  private String activitydate = "";
  private String frompartyid = ""; 
  private String activitytodate = "";
  private String activitytitle = "";
  private String activityproducts = "";
  private String activityarea = "";
  private String activitycomments = "";
  private String topartyid = "";
  private String actdetail = "";
  private String pagination = "";
  private String totalsize = ""; 
  private String txnid = ""; 





public String getTxnid() {
	return txnid;
}


public void setTxnid(String txnid) {
	this.txnid = txnid;
}


public String getFrompartyid() {
	return frompartyid;
}


public void setFrompartyid(String frompartyid) {
	this.frompartyid = frompartyid;
}


public String getTotalsize() {
	return totalsize;
}


public void setTotalsize(String totalsize) {
	this.totalsize = totalsize;
}


public String getPagination() {
	return pagination;
}


public void setPagination(String pagination) {
	this.pagination = pagination;
}



public String getTopartyid() {
	return topartyid;
}



public void setTopartyid(String topartyid) {
	this.topartyid = topartyid;
}



public String getActdetail() {
	return actdetail;
}



public void setActdetail(String actdetail) {
	this.actdetail = actdetail;
}

 public String getPartyid() {
    return partyid;
  }



  public void setPartyid(String partyid) {
    this.partyid = partyid;
  }



  public String getActivityid() {
    return activityid;
  }



  public void setActivityid(String activityid) {
    this.activityid = activityid;
  }

  public String getActivitytypeid() {
	return activitytypeid;
}



public void setActivitytypeid(String activitytypeid) {
	this.activitytypeid = activitytypeid;
}

  public String getActivitytype() {
    return activitytype;
  }



  public void setActivitytype(String activitytype) {
    this.activitytype = activitytype;
  }



  public String getActivitytitle() {
    return activitytitle;
  }



  public void setActivitytitle(String activitytitle) {
    this.activitytitle = activitytitle;
  }



  public String getActivityproducts() {
    return activityproducts;
  }



  public void setActivityproducts(String activityproducts) {
    this.activityproducts = activityproducts;
  }



  public String getActivityarea() {
    return activityarea;
  }



  public void setActivityarea(String activityarea) {
    this.activityarea = activityarea;
  }



  public String getActivitycomments() {
    return activitycomments;
  }



  public void setActivitycomments(String activitycomments) {
    this.activitycomments = activitycomments;
  }



  public String getActivitydate() {
    return activitydate;
  }



  public void setActivitydate(String activitydate) {
    this.activitydate = activitydate;
  }
  
  public String getActivitytodate() {
	return activitytodate;
}


public void setActivitytodate(String activitytodate) {
	this.activitytodate = activitytodate;
}



  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("PARTYID", "PARTYID");
    properties.put("ACTIVITYID", "ACTIVITYID");
    properties.put("TXNID", "TXNID");
    properties.put("ACTIVITYTYPEID", "ACTIVITYTYPEID");
    properties.put("ACTIVITYTYPE", "ACTIVITYTYPE");
    properties.put("ACTIVITYDATE", "ACTIVITYDATE");
    properties.put("FROMPARTYID", "FROMPARTYID");
    properties.put("ACTIVITYTODATE", "ACTIVITYTODATE");
    properties.put("ACTIVITYTITLE", "ACTIVITYTITLE");
    properties.put("ACTIVITYPRODUCTS", "ACTIVITYPRODUCTS");
    properties.put("ACTIVITYAREA", "ACTIVITYAREA");
    properties.put("ACTIVITYCOMMENTS", "ACTIVITYCOMMENTS");
    properties.put("TOPARTYID", "TOPARTYID");
    properties.put("ACTDETAIL", "ACTDETAIL");
    properties.put("TOTALSIZE", "TOTALSIZE");
    
    return properties;
  }

}
