package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMSurgeonContactsListVO extends GmDataStoreVO {

  private String contype = "";
  private String conmode = "";
  private String convalue = "";
  private String conid = "";
  private String primaryfl = "";
  private String voidfl = "";



  public String getContype() {
    return contype;
  }



  public void setContype(String contype) {
    this.contype = contype;
  }



  public String getConmode() {
    return conmode;
  }



  public void setConmode(String conmode) {
    this.conmode = conmode;
  }



  public String getConvalue() {
    return convalue;
  }



  public void setConvalue(String convalue) {
    this.convalue = convalue;
  }



  public String getConid() {
    return conid;
  }



  public void setConid(String conid) {
    this.conid = conid;
  }



  public String getPrimaryfl() {
    return primaryfl;
  }



  public void setPrimaryfl(String primaryfl) {
    this.primaryfl = primaryfl;
  }


  public String getVoidfl() {
    return voidfl;
  }



  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }


  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("CONTYPE", "CONTYPE");
    properties.put("CONMODE", "CONMODE");
    properties.put("CONVALUE", "CONVALUE");
    properties.put("CONID", "CONID");
    properties.put("PRIMARYFL", "PRIMARYFL");
    properties.put("VOIDFL", "VOIDFL");
    return properties;
  }

}
