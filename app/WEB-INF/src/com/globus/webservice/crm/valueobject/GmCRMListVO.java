package com.globus.webservice.crm.valueobject;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.sales.GmCompanyVO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.globus.valueobject.common.GmDataVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMListVO extends GmDataVO {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCriteriaVO> gmCriteriaVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMActivityVO> gmCRMActivityVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMActivityAttrVO> gmCRMActivityAttrVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMActivityPartyVO> gmCRMActivityPartyVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMTabsVO> gmCRMTabsVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMCRFVO> gmCRMCRFVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMSalesRepListVO> gmCRMSalesRepListVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMFileUploadVO> gmCRMFileUploadVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCodeLookUpVO> gmCodeLookUpVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmAccountBasicVO> gmAccountBasicVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmSystemBasicVO> gmSystemBasicVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmPartyBasicVO> gmPartyBasicVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmPhysicianVisitBasicVO> gmPhysicianVisitBasicVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmSalesRepQuickVO> gmSalesRepQuickVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmProjectBasicVO> gmProjectBasicVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMUserInfoVO> gmCRMUserInfoVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMSurgeonlistVO> gmCRMSurgeonlistVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMSurgeonAffiliationVO> gmCRMSurgeonAffiliationVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMSurgeonSurgicalVO> gmCRMSurgeonSurgicalVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMSurgeonActivityVO> gmCRMSurgeonActivityVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMSurgeonAddressListVO> gmCRMSurgeonAddressListVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMTrainingListVO> gmCRMTrainingListVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmLeadBasicVO> gmLeadBasicVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMSurgeonEducationVO> gmCRMSurgeonEducationVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMSurgeonProfessionalVO> gmCRMSurgeonProfessionalVO = null;
	private List<GmCRMSurgicalActivityVO> gmCRMSurgicalActivityVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMSummaryReportVO> gmCRMSummaryReportVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMCalendarVO> gmCRMCalendarVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMSystemDetailsVO> gmCRMSystemDetailsVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMSurgicalSystemDOVO> gmCRMSurgicalSystemDOVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMSurgeonSocialVO> gmCRMSurgeonSocialVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmSegmentBasicVO> gmSegmentBasicVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCRMPSCaseVO> gmCRMPSCaseVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCompanyVO> gmCompanyVO = null;
	
	
	public List<GmCompanyVO> getGmCompanyVO() {
		return gmCompanyVO;
	}

	public void setGmCompanyVO(List<GmCompanyVO> gmCompanyVO) {
		this.gmCompanyVO = gmCompanyVO;
	}

	public List<GmCRMPSCaseVO> getGmCRMPSCaseVO() {
		return gmCRMPSCaseVO;
	}

	public void setGmCRMPSCaseVO(List<GmCRMPSCaseVO> gmCRMPSCaseVO) {
		this.gmCRMPSCaseVO = gmCRMPSCaseVO;
	}

	public List<GmSegmentBasicVO> getGmSegmentBasicVO() {
		return gmSegmentBasicVO;
	}

	public void setGmSegmentBasicVO(List<GmSegmentBasicVO> gmSegmentBasicVO) {
		this.gmSegmentBasicVO = gmSegmentBasicVO;
	}

	public List<GmCRMSurgeonSocialVO> getGmCRMSurgeonSocialVO() {
		return gmCRMSurgeonSocialVO;
	}

	public void setGmCRMSurgeonSocialVO(
			List<GmCRMSurgeonSocialVO> gmCRMSurgeonSocialVO) {
		this.gmCRMSurgeonSocialVO = gmCRMSurgeonSocialVO;
	}

	public List<GmCRMSystemDetailsVO> getGmCRMSystemDetailsVO() {
		return gmCRMSystemDetailsVO;
	}

	public void setGmCRMSystemDetailsVO(
			List<GmCRMSystemDetailsVO> gmCRMSystemDetailsVO) {
		this.gmCRMSystemDetailsVO = gmCRMSystemDetailsVO;
	}

	public List<GmCRMCalendarVO> getGmCRMCalendarVO() {
		return gmCRMCalendarVO;
	}

	public void setGmCRMCalendarVO(List<GmCRMCalendarVO> gmCRMCalendarVO) {
		this.gmCRMCalendarVO = gmCRMCalendarVO;
	}

	public List<GmCRMSurgicalSystemDOVO> getGmCRMSurgicalSystemDOVO() {
		return gmCRMSurgicalSystemDOVO;
	}

	public void setGmCRMSurgicalSystemDOVO(
			List<GmCRMSurgicalSystemDOVO> gmCRMSurgicalSystemDOVO) {
		this.gmCRMSurgicalSystemDOVO = gmCRMSurgicalSystemDOVO;
	}

	public List<GmCRMSummaryReportVO> getGmCRMSummaryReportVO() {
		return gmCRMSummaryReportVO;
	}

	public void setGmCRMSummaryReportVO(
			List<GmCRMSummaryReportVO> gmCRMSummaryReportVO) {
		this.gmCRMSummaryReportVO = gmCRMSummaryReportVO;
	}

	public List<GmCRMSurgicalActivityVO> getGmCRMSurgicalActivityVO() {
		return gmCRMSurgicalActivityVO;
	}

	public void setGmCRMSurgicalActivityVO(
			List<GmCRMSurgicalActivityVO> gmCRMSurgicalActivityVO) {
		this.gmCRMSurgicalActivityVO = gmCRMSurgicalActivityVO;
	}

	public List<GmCRMSurgeonProfessionalVO> getGmCRMSurgeonProfessionalVO() {
		return gmCRMSurgeonProfessionalVO;
	}

	public void setGmCRMSurgeonProfessionalVO(
			List<GmCRMSurgeonProfessionalVO> gmCRMSurgeonProfessionalVO) {
		this.gmCRMSurgeonProfessionalVO = gmCRMSurgeonProfessionalVO;
	}

	public List<GmCRMSurgeonEducationVO> getGmCRMSurgeonEducationVO() {
		return gmCRMSurgeonEducationVO;
	}

	public void setGmCRMSurgeonEducationVO(
			List<GmCRMSurgeonEducationVO> gmCRMSurgeonEducationVO) {
		this.gmCRMSurgeonEducationVO = gmCRMSurgeonEducationVO;
	}

	public List<GmCriteriaVO> getGmCriteriaVO() {
		return gmCriteriaVO;
	}

	public void setGmCriteriaVO(List<GmCriteriaVO> gmCriteriaVO) {
		this.gmCriteriaVO = gmCriteriaVO;
	}

	public List<GmCRMActivityVO> getGmCRMActivityVO() {
		return gmCRMActivityVO;
	}

	public void setGmCRMActivityVO(List<GmCRMActivityVO> gmCRMActivityVO) {
		this.gmCRMActivityVO = gmCRMActivityVO;
	}

	public List<GmCRMActivityAttrVO> getGmCRMActivityAttrVO() {
		return gmCRMActivityAttrVO;
	}

	public void setGmCRMActivityAttrVO(
			List<GmCRMActivityAttrVO> gmCRMActivityAttrVO) {
		this.gmCRMActivityAttrVO = gmCRMActivityAttrVO;
	}

	public List<GmCRMActivityPartyVO> getGmCRMActivityPartyVO() {
		return gmCRMActivityPartyVO;
	}

	public void setGmCRMActivityPartyVO(
			List<GmCRMActivityPartyVO> gmCRMActivityPartyVO) {
		this.gmCRMActivityPartyVO = gmCRMActivityPartyVO;
	}

	public List<GmCRMTabsVO> getGmCRMTabsVO() {
		return gmCRMTabsVO;
	}

	public void setGmCRMTabsVO(List<GmCRMTabsVO> gmCRMTabsVO) {
		this.gmCRMTabsVO = gmCRMTabsVO;
	}

	public List<GmCRMCRFVO> getGmCRMCRFVO() {
		return gmCRMCRFVO;
	}

	public void setGmCRMCRFVO(List<GmCRMCRFVO> gmCRMCRFVO) {
		this.gmCRMCRFVO = gmCRMCRFVO;
	}

	public List<GmCRMSalesRepListVO> getGmCRMSalesRepListVO() {
		return gmCRMSalesRepListVO;
	}

	public void setGmCRMSalesRepListVO(
			List<GmCRMSalesRepListVO> gmCRMSalesRepListVO) {
		this.gmCRMSalesRepListVO = gmCRMSalesRepListVO;
	}

	public List<GmCRMFileUploadVO> getGmCRMFileUploadVO() {
		return gmCRMFileUploadVO;
	}

	public void setGmCRMFileUploadVO(List<GmCRMFileUploadVO> gmCRMFileUploadVO) {
		this.gmCRMFileUploadVO = gmCRMFileUploadVO;
	}

	public List<GmSystemBasicVO> getGmSystemBasicVO() {
		return gmSystemBasicVO;
	}

	public void setGmSystemBasicVO(List<GmSystemBasicVO> gmSystemBasicVO) {
		this.gmSystemBasicVO = gmSystemBasicVO;
	}

	public List<GmPartyBasicVO> getGmPartyBasicVO() {
		return gmPartyBasicVO;
	}

	public void setGmPartyBasicVO(List<GmPartyBasicVO> gmPartyBasicVO) {
		this.gmPartyBasicVO = gmPartyBasicVO;
	}

	public List<GmPhysicianVisitBasicVO> getGmPhysicianVisitBasicVO() {
		return gmPhysicianVisitBasicVO;
	}

	public void setGmPhysicianVisitBasicVO(
			List<GmPhysicianVisitBasicVO> gmPhysicianVisitBasicVO) {
		this.gmPhysicianVisitBasicVO = gmPhysicianVisitBasicVO;
	}

	public List<GmSalesRepQuickVO> getGmSalesRepQuickVO() {
		return gmSalesRepQuickVO;
	}

	public void setGmSalesRepQuickVO(List<GmSalesRepQuickVO> gmSalesRepQuickVO) {
		this.gmSalesRepQuickVO = gmSalesRepQuickVO;
	}

	public List<GmProjectBasicVO> getGmProjectBasicVO() {
		return gmProjectBasicVO;
	}

	public void setGmProjectBasicVO(List<GmProjectBasicVO> gmProjectBasicVO) {
		this.gmProjectBasicVO = gmProjectBasicVO;
	}

	public List<GmCRMUserInfoVO> getGmCRMUserInfoVO() {
		return gmCRMUserInfoVO;
	}

	public void setGmCRMUserInfoVO(List<GmCRMUserInfoVO> gmCRMUserInfoVO) {
		this.gmCRMUserInfoVO = gmCRMUserInfoVO;
	}

	public List<GmCRMSurgeonlistVO> getGmCRMSurgeonlistVO() {
		return gmCRMSurgeonlistVO;
	}

	public void setGmCRMSurgeonlistVO(
			List<GmCRMSurgeonlistVO> gmCRMSurgeonlistVO) {
		this.gmCRMSurgeonlistVO = gmCRMSurgeonlistVO;
	}

	public List<GmCRMSurgeonAffiliationVO> getGmCRMSurgeonAffiliationVO() {
		return gmCRMSurgeonAffiliationVO;
	}

	public void setGmCRMSurgeonAffiliationVO(
			List<GmCRMSurgeonAffiliationVO> gmCRMSurgeonAffiliationVO) {
		this.gmCRMSurgeonAffiliationVO = gmCRMSurgeonAffiliationVO;
	}

	public List<GmCRMSurgeonSurgicalVO> getGmCRMSurgeonSurgicalVO() {
		return gmCRMSurgeonSurgicalVO;
	}

	public void setGmCRMSurgeonSurgicalVO(
			List<GmCRMSurgeonSurgicalVO> gmCRMSurgeonSurgicalVO) {
		this.gmCRMSurgeonSurgicalVO = gmCRMSurgeonSurgicalVO;
	}

	public List<GmCRMSurgeonActivityVO> getGmCRMSurgeonActivityVO() {
		return gmCRMSurgeonActivityVO;
	}

	public void setGmCRMSurgeonActivityVO(
			List<GmCRMSurgeonActivityVO> gmCRMSurgeonActivityVO) {
		this.gmCRMSurgeonActivityVO = gmCRMSurgeonActivityVO;
	}

	public List<GmCRMSurgeonAddressListVO> getGmCRMSurgeonAddressListVO() {
		return gmCRMSurgeonAddressListVO;
	}

	public void setGmCRMSurgeonAddressListVO(
			List<GmCRMSurgeonAddressListVO> gmCRMSurgeonAddressListVO) {
		this.gmCRMSurgeonAddressListVO = gmCRMSurgeonAddressListVO;
	}

	public List<GmCRMTrainingListVO> getGmCRMTrainingListVO() {
		return gmCRMTrainingListVO;
	}

	public void setGmCRMTrainingListVO(
			List<GmCRMTrainingListVO> gmCRMTrainingListVO) {
		this.gmCRMTrainingListVO = gmCRMTrainingListVO;
	}

	public List<GmLeadBasicVO> getGmLeadBasicVO() {
		return gmLeadBasicVO;
	}

	public void setGmLeadBasicVO(List<GmLeadBasicVO> gmLeadBasicVO) {
		this.gmLeadBasicVO = gmLeadBasicVO;
	}

	public List<GmCodeLookUpVO> getGmCodeLookUpVO() {
		return gmCodeLookUpVO;
	}

	public void setGmCodeLookUpVO(List<GmCodeLookUpVO> gmCodeLookUpVO) {
		this.gmCodeLookUpVO = gmCodeLookUpVO;
	}

	public List<GmAccountBasicVO> getGmAccountBasicVO() {
		return gmAccountBasicVO;
	}

	public void setGmAccountBasicVO(List<GmAccountBasicVO> gmAccountBasicVO) {
		this.gmAccountBasicVO = gmAccountBasicVO;
	}

}
