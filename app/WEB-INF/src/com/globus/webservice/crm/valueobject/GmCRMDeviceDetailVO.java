package com.globus.webservice.crm.valueobject;

import java.util.HashMap;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.common.beans.GmCommonClass;
import com.globus.valueobject.common.GmDeviceDetailVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonActivityVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonAffiliationVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonSurgicalVO;
import com.globus.webservice.crm.valueobject.GmCRMSurgeonlistVO;
import com.globus.webservice.crm.valueobject.GmCRMLogVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMDeviceDetailVO extends GmDeviceDetailVO {
  private List<GmCRMSurgeonAffiliationVO> arrgmcrmsurgeonaffiliationvo = null;
  private List<GmCRMSurgeonSurgicalVO> arrgmcrmsurgeonsurgicalvo = null;
  private List<GmCRMSurgeonActivityVO> arrgmcrmsurgeonactivityvo = null;
  private List<GmCRMSurgeonlistVO> arrgmsurgeonlistvo = null;
  private List<GmCRMActivityVO> arrgmactivityvo = null;
  private List<GmCRMSurgeonAttrVO> arrgmcrmsurgeonattrvo = null;
  private List<GmCRMFileUploadVO> listGmCRMFileUploadVO = null;
  private List<GmCRMActivityPartyVO> arrgmactivitypartyvo = null;
  private List<GmCRMActivityAttrVO> arrgmactivityattrvo = null;
  private List<GmCRMLogVO> arrgmlogvo = null;
  private List<GmCRMCRFVO> arrgmcrfvo = null;
  private List<GmCRFApprovalVO> arrgmcrfapprvvo = null;
  private List<GmAdVpVO> arrgmadvpvo = null;
  private List<GmCriteriaVO> arrgmcriteriavo = null;
  private List<GmCriteriaAttrVO> arrgmcriteriaattrvo = null;
  private List<GmProjectBasicVO> arrgmprojectvo = null;
  /**
 * @return the arrgmprojectvo
 */
public List<GmProjectBasicVO> getArrgmprojectvo() {
	return arrgmprojectvo;
}

/**
 * @param arrgmprojectvo the arrgmprojectvo to set
 */
public void setArrgmprojectvo(List<GmProjectBasicVO> arrgmprojectvo) {
	this.arrgmprojectvo = arrgmprojectvo;
}

/**
 * @return the arrgmcriteriaattrvo
 */
public List<GmCriteriaAttrVO> getArrgmcriteriaattrvo() {
	return arrgmcriteriaattrvo;
}

/**
 * @param arrgmcriteriaattrvo the arrgmcriteriaattrvo to set
 */
public void setArrgmcriteriaattrvo(List<GmCriteriaAttrVO> arrgmcriteriaattrvo) {
	this.arrgmcriteriaattrvo = arrgmcriteriaattrvo;
}

/**
 * @return the arrgmadvpvo
 */
public List<GmAdVpVO> getArrgmadvpvo() {
	return arrgmadvpvo;
}

/**
 * @return the arrgmcriteriavo
 */
public List<GmCriteriaVO> getArrgmcriteriavo() {
	return arrgmcriteriavo;
}

/**
 * @param arrgmcriteriavo the arrgmcriteriavo to set
 */
public void setArrgmcriteriavo(List<GmCriteriaVO> arrgmcriteriavo) {
	this.arrgmcriteriavo = arrgmcriteriavo;
}

/**
 * @param arrgmadvpvo the arrgmadvpvo to set
 */
public void setArrgmadvpvo(List<GmAdVpVO> arrgmadvpvo) {
	this.arrgmadvpvo = arrgmadvpvo;
}

/**
 * @return the arrgmcrfapprvvo
 */
public List<GmCRFApprovalVO> getArrgmcrfapprvvo() {
	return arrgmcrfapprvvo;
}

/**
 * @param arrgmcrfapprvvo the arrgmcrfapprvvo to set
 */
public void setArrgmcrfapprvvo(List<GmCRFApprovalVO> arrgmcrfapprvvo) {
	this.arrgmcrfapprvvo = arrgmcrfapprvvo;
}

/**
 * @return the arrgmcrfvo
 */
public List<GmCRMCRFVO> getArrgmcrfvo() {
	return arrgmcrfvo;
}

/**
 * @param arrgmcrfvo the arrgmcrfvo to set
 */
public void setArrgmcrfvo(List<GmCRMCRFVO> arrgmcrfvo) {
	this.arrgmcrfvo = arrgmcrfvo;
}


private String update = "";
/**
 * @return the update
 */
public String getUpdate() {
	return update;
}

/**
 * @param update the update to set
 */
public void setUpdate(String update) {
	this.update = update;
}

/**
 * @return the arrgmlogvo
 */
public List<GmCRMLogVO> getArrgmlogvo() {
	return arrgmlogvo;
}

/**
 * @param arrgmlogvo the arrgmlogvo to set
 */
public void setArrgmlogvo(List<GmCRMLogVO> arrgmlogvo) {
	this.arrgmlogvo = arrgmlogvo;
}

/**
 * @return the arrgmactivityattrvo
 */
public List<GmCRMActivityAttrVO> getArrgmactivityattrvo() {
	return arrgmactivityattrvo;
}

/**
 * @param arrgmactivityattrvo the arrgmactivityattrvo to set
 */
public void setArrgmactivityattrvo(List<GmCRMActivityAttrVO> arrgmactivityattrvo) {
	this.arrgmactivityattrvo = arrgmactivityattrvo;
}

/**
 * @return the arrgmactivitypartyvo
 */
public List<GmCRMActivityPartyVO> getArrgmactivitypartyvo() {
	return arrgmactivitypartyvo;
}

/**
 * @param arrgmactivitypartyvo the arrgmactivitypartyvo to set
 */
public void setArrgmactivitypartyvo(
		List<GmCRMActivityPartyVO> arrgmactivitypartyvo) {
	this.arrgmactivitypartyvo = arrgmactivitypartyvo;
}

/**
 * @return the arrgmactivityvo
 */
public List<GmCRMActivityVO> getArrgmactivityvo() {
	return arrgmactivityvo;
}

/**
 * @param arrgmactivityvo the arrgmactivityvo to set
 */
public void setArrgmactivityvo(List<GmCRMActivityVO> arrgmactivityvo) {
	this.arrgmactivityvo = arrgmactivityvo;
}

/**
 * @return the listGmCRMFileUploadVO
 */
public List<GmCRMFileUploadVO> getListGmCRMFileUploadVO() {
	return listGmCRMFileUploadVO;
}

/**
 * @param listGmCRMFileUploadVO the listGmCRMFileUploadVO to set
 */
public void setListGmCRMFileUploadVO(
		List<GmCRMFileUploadVO> listGmCRMFileUploadVO) {
	this.listGmCRMFileUploadVO = listGmCRMFileUploadVO;
}

/**
 * @return the arrgmcrmsurgeonattrvo
 */
public List<GmCRMSurgeonAttrVO> getArrgmcrmsurgeonattrvo() {
	return arrgmcrmsurgeonattrvo;
}

/**
 * @param arrgmcrmsurgeonattrvo the arrgmcrmsurgeonattrvo to set
 */
public void setArrgmcrmsurgeonattrvo(
		List<GmCRMSurgeonAttrVO> arrgmcrmsurgeonattrvo) {
	this.arrgmcrmsurgeonattrvo = arrgmcrmsurgeonattrvo;
}

/**
 * @return the arrgmsurgeonlistvo
 */
public List<GmCRMSurgeonlistVO> getArrgmsurgeonlistvo() {
	return arrgmsurgeonlistvo;
}

/**
 * @param arrgmsurgeonlistvo the arrgmsurgeonlistvo to set
 */
public void setArrgmsurgeonlistvo(List<GmCRMSurgeonlistVO> arrgmsurgeonlistvo) {
	this.arrgmsurgeonlistvo = arrgmsurgeonlistvo;
}

/**
 * @return the arrgmcrmsurgeonactivityvo
 */
public List<GmCRMSurgeonActivityVO> getArrgmcrmsurgeonactivityvo() {
	return arrgmcrmsurgeonactivityvo;
}

/**
 * @param arrgmcrmsurgeonactivityvo the arrgmcrmsurgeonactivityvo to set
 */
public void setArrgmcrmsurgeonactivityvo(
		List<GmCRMSurgeonActivityVO> arrgmcrmsurgeonactivityvo) {
	this.arrgmcrmsurgeonactivityvo = arrgmcrmsurgeonactivityvo;
}

/**
 * @return the arrgmcrmsurgeonsurgicalvo
 */
public List<GmCRMSurgeonSurgicalVO> getArrgmcrmsurgeonsurgicalvo() {
	return arrgmcrmsurgeonsurgicalvo;
}

/**
 * @param arrgmcrmsurgeonsurgicalvo the arrgmcrmsurgeonsurgicalvo to set
 */
public void setArrgmcrmsurgeonsurgicalvo(
		List<GmCRMSurgeonSurgicalVO> arrgmcrmsurgeonsurgicalvo) {
	this.arrgmcrmsurgeonsurgicalvo = arrgmcrmsurgeonsurgicalvo;
}

/**
 * @return the arrgmcrmsurgeonaffiliationvo
 */
public List<GmCRMSurgeonAffiliationVO> getArrgmcrmsurgeonaffiliationvo() {
	return arrgmcrmsurgeonaffiliationvo;
}

/**
 * @param arrgmcrmsurgeonaffiliationvo the arrgmcrmsurgeonaffiliationvo to set
 */
public void setArrgmcrmsurgeonaffiliationvo(
		List<GmCRMSurgeonAffiliationVO> arrgmcrmsurgeonaffiliationvo) {
	this.arrgmcrmsurgeonaffiliationvo = arrgmcrmsurgeonaffiliationvo;
}


  public void populatePageProperties(List alList) {
    int alReturnSize = alList.size();
    if (alReturnSize > 0) {
      HashMap hmPageInfo = GmCommonClass.parseNullHashMap((HashMap) alList.get(0));
      this.setPageno((String) hmPageInfo.get("PAGENO"));
      this.setTotalpages((String) hmPageInfo.get("TOTALPAGES"));
      this.setTotalsize((String) hmPageInfo.get("TOTALSIZE"));
      this.setPagesize((String) hmPageInfo.get("PAGESIZE"));
    }
  }
}
