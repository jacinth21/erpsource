package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author TMuthusamy 
 * GmAccountRptVO --- Account Surgeon Report VO
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmAccountRptVO extends GmDataStoreVO {

	private String accid = "";
	private String fromdate = "";
	private String todate = "";
	private String segmentstr = "";
	private String systemstr = "";
	private String systemid = "";
	private String orddate = "";
	

	/**
	 * @return the accid
	 */
	public String getAccid() {
		return accid;
	}

	/**
	 * @param accid
	 * the accid to set
	 */
	public void setAccid(String accid) {
		this.accid = accid;
	}
	
	/**
	 * @return the fromdate
	 */
	public String getFromdate() {
		return fromdate;
	}

	/**
	 * @param fromdate the fromdate to set
	 */
	public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}

	/**
	 * @return the todate
	 */
	public String getTodate() {
		return todate;
	}

	/**
	 * @param todate the todate to set
	 */
	public void setTodate(String todate) {
		this.todate = todate;
	}
	

	/**
	 * @return the segmentstr
	 */
	public String getSegmentstr() {
		return segmentstr;
	}

	/**
	 * @param segmentstr the segmentstr to set
	 */
	public void setSegmentstr(String segmentstr) {
		this.segmentstr = segmentstr;
	}

	/**
	 * @return the systemstr
	 */
	public String getSystemstr() {
		return systemstr;
	}

	/**
	 * @param systemstr the systemstr to set
	 */
	public void setSystemstr(String systemstr) {
		this.systemstr = systemstr;
	}
	
	/**
	 * @return the systemid
	 */
	public String getSystemid() {
		return systemid;
	}

	/**
	 * @param systemid the systemid to set
	 */
	public void setSystemid(String systemid) {
		this.systemid = systemid;
	}
	
	/**
	 * @return the orddate
	 */
	public String getOrddate() {
		return orddate;
	}

	/**
	 * @param orddate to set
	 */
	public void setOrddate(String orddate) {
		this.orddate = orddate;
	}


	@Transient
	public Properties getMappingProperties() {
		Properties properties = new Properties();
		properties.put("ACCID", "ACCID");
	    properties.put("FROMDATE", "FROMDATE");	
	    properties.put("TODATE", "TODATE");
	    properties.put("SEGMENTSTR", "SEGMENTSTR");
	    properties.put("SYSTEMSTR", "SYSTEMSTR");
	    properties.put("SYSTEMID", "SYSTEMID");
	    properties.put("ORDDATE", "ORDDATE");
		return properties;
	}

}
