package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMMailVO extends GmDataStoreVO {
  private String toaddr = "";
  private String ccaddr = "";
  private String bccaddr = "";
  private String subj = "";
  private String content = "";
  private String replyaddr = "";
  private String mstatus = "";
  private String actid = "";
  private String isevent = "";
  private String includesurgeon = "";
  private String mailstatus = "";
  private String notifyfl = "";
  private String actcategory = "";
  private String emailfl = "";
  private String eventfl = "";


  public String getNotifyfl() {
    return notifyfl;
  }

  public void setNotifyfl(String notifyfl) {
    this.notifyfl = notifyfl;
  }

  /**
   * @return the mailstatus
   */
  public String getMailstatus() {
    return mailstatus;
  }

  /**
   * @param mailstatus the mailstatus to set
   */
  public void setMailstatus(String mailstatus) {
    this.mailstatus = mailstatus;
  }

  public String getIncludesurgeon() {
    return includesurgeon;
  }

  public void setIncludesurgeon(String includesurgeon) {
    this.includesurgeon = includesurgeon;
  }

  public String getActid() {
    return actid;
  }

  public void setActid(String actid) {
    this.actid = actid;
  }


  public String getIsevent() {
    return isevent;
  }

  public void setIsevent(String isevent) {
    this.isevent = isevent;
  }


  /**
   * @return the mstatus
   */
  public String getMstatus() {
    return mstatus;
  }

  /**
   * @param mstatus the mstatus to set
   */
  public void setMstatus(String mstatus) {
    this.mstatus = mstatus;
  }

  public String getToaddr() {
    return toaddr;
  }

  public void setToaddr(String toaddr) {
    this.toaddr = toaddr;
  }

  public String getCcaddr() {
    return ccaddr;
  }

  public void setCcaddr(String ccaddr) {
    this.ccaddr = ccaddr;
  }

  public String getBccaddr() {
    return bccaddr;
  }

  public void setBccaddr(String bccaddr) {
    this.bccaddr = bccaddr;
  }

  public String getSubj() {
    return subj;
  }

  public void setSubj(String subj) {
    this.subj = subj;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getReplyaddr() {
    return replyaddr;
  }

  public void setReplyaddr(String replyaddr) {
    this.replyaddr = replyaddr;
  }

  public String getActcategory() {
    return actcategory;
  }

  public void setActcategory(String actcategory) {
    this.actcategory = actcategory;
  }
  
  public String getEmailfl() {
    return emailfl;
  }

  public void setEmailfl(String emailfl) {
    this.emailfl = emailfl;
  }

  public String getEventfl() {
    return eventfl;
  }

  public void setEventfl(String eventfl) {
    this.eventfl = eventfl;
  }

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("TOADDR", "TOADDR");
    properties.put("CCADDR", "CCADDR");
    properties.put("BCCADDR", "BCCADDR");
    properties.put("SUBJ", "SUBJ");
    properties.put("CONTENT", "CONTENT");
    properties.put("REPLYADDR", "REPLYADDR");
    properties.put("MSTATUS", "MSTATUS");
    properties.put("ACTID", "ACTID");
    properties.put("ISEVENT", "ISEVENT");
    properties.put("INCLUDESURGEON", "INCLUDESURGEON");
    properties.put("MAILSTATUS", "MAILSTATUS");
    properties.put("ACTCATEGORY", "ACTCATEGORY");
    properties.put("EMAILFL", "EMAILFL");
    properties.put("EVENTFL", "EVENTFL");
    return properties;
  }


}
