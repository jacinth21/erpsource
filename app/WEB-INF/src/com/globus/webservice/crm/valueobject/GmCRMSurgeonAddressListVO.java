package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMSurgeonAddressListVO extends GmDataStoreVO {

  private String addid = "";
  private String add1 = "";
  private String add2 = "";
  private String city = "";
  private String statenm = "";
  private String stateid = "";
  private String countrynm = "";
  private String countryid = "";
  private String addrtyp = "";
  private String addrtypnm = "";



  public String getAddrtypnm() {
    return addrtypnm;
  }



  public void setAddrtypnm(String addrtypnm) {
    this.addrtypnm = addrtypnm;
  }



  public String getAddrtyp() {
    return addrtyp;
  }



  public void setAddrtyp(String addrtyp) {
    this.addrtyp = addrtyp;
  }



  public String getCountrynm() {
    return countrynm;
  }



  public void setCountrynm(String countrynm) {
    this.countrynm = countrynm;
  }



  public String getCountryid() {
    return countryid;
  }



  public void setCountryid(String countryid) {
    this.countryid = countryid;
  }



  private String zip = "";
  private String primaryfl = "";
  private String comments = "";
  private String commentid = "";
  private String emailid = "";
  private String email = "";
  private String phoneid = "";
  private String phone = "";
  private String voidfl = "";


  public String getComments() {
    return comments;
  }



  public void setComments(String comments) {
    this.comments = comments;
  }



  public String getPrimaryfl() {
    return primaryfl;
  }



  public void setPrimaryfl(String primaryfl) {
    this.primaryfl = primaryfl;
  }



  public String getAddid() {
    return addid;
  }



  public void setAddid(String addid) {
    this.addid = addid;
  }



  public String getAdd1() {
    return add1;
  }



  public void setAdd1(String add1) {
    this.add1 = add1;
  }



  public String getAdd2() {
    return add2;
  }



  public void setAdd2(String add2) {
    this.add2 = add2;
  }



  public String getCity() {
    return city;
  }



  public void setCity(String city) {
    this.city = city;
  }



  public String getStatenm() {
    return statenm;
  }



  public void setStatenm(String statenm) {
    this.statenm = statenm;
  }


  public String getStateid() {
    return stateid;
  }


  public void setStateid(String stateid) {
    this.stateid = stateid;
  }


  public String getZip() {
    return zip;
  }


  public void setZip(String zip) {
    this.zip = zip;
  }



  public String getCommentid() {
    return commentid;
  }



  public void setCommentid(String commentid) {
    this.commentid = commentid;
  }



  public String getEmailid() {
    return emailid;
  }



  public void setEmailid(String emailid) {
    this.emailid = emailid;
  }



  public String getEmail() {
    return email;
  }



  public void setEmail(String email) {
    this.email = email;
  }



  public String getPhoneid() {
    return phoneid;
  }



  public void setPhoneid(String phoneid) {
    this.phoneid = phoneid;
  }



  public String getPhone() {
    return phone;
  }



  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getVoidfl() {
    return voidfl;
  }



  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }


  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("ADDID", "ADDID");
    properties.put("ADD1", "ADD1");
    properties.put("ADD2", "ADD2");
    properties.put("CITY", "CITY");
    properties.put("STATENM", "STATENM");
    properties.put("STATEID", "STATEID");
    properties.put("COUNTRYNM", "COUNTRYNM");
    properties.put("COUNTRYID", "COUNTRYID");
    properties.put("ZIP", "ZIP");
    properties.put("PRIMARYFL", "PRIMARYFL");
    properties.put("COMMENTS", "COMMENTS");
    properties.put("COMMENTID", "COMMENTID");
    properties.put("EMAILID", "EMAILID");
    properties.put("EMAIL", "EMAIL");
    properties.put("PHONEID", "PHONEID");
    properties.put("PHONE", "PHONE");
    properties.put("VOIDFL", "VOIDFL");
    properties.put("ADDRTYP", "ADDRTYP");
    properties.put("ADDRTYPNM", "ADDRTYPNM");
    return properties;
  }

}
