package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName("gmCRMSummaryReportVO")
public class GmCRMSummaryReportVO extends GmDataStoreVO {
	 
	 private String vp_id = "";
	 private String vp_name = "";
	 private String ad_id = "";
	 private String ad_name = "";
	 private String d_id = "";
	 private String d_name = "";
	 private String rep_id = "";
	 private String rep_name = "";
	 private String active = "";
	 private String inprogress = "";
	 private String open = "";
	 private String total = "";	
	 private String ref_id = "";		 


	public String getRef_id() {
		return ref_id;
	}




	public void setRef_id(String ref_id) {
		this.ref_id = ref_id;
	}



	public String getVp_id() {
		return vp_id;
	}




	public void setVp_id(String vp_id) {
		this.vp_id = vp_id;
	}




	public String getVp_name() {
		return vp_name;
	}




	public void setVp_name(String vp_name) {
		this.vp_name = vp_name;
	}




	public String getAd_id() {
		return ad_id;
	}




	public void setAd_id(String ad_id) {
		this.ad_id = ad_id;
	}




	public String getAd_name() {
		return ad_name;
	}




	public void setAd_name(String ad_name) {
		this.ad_name = ad_name;
	}




	public String getD_id() {
		return d_id;
	}




	public void setD_id(String d_id) {
		this.d_id = d_id;
	}




	public String getD_name() {
		return d_name;
	}




	public void setD_name(String d_name) {
		this.d_name = d_name;
	}




	public String getRep_id() {
		return rep_id;
	}




	public void setRep_id(String rep_id) {
		this.rep_id = rep_id;
	}




	public String getRep_name() {
		return rep_name;
	}




	public void setRep_name(String rep_name) {
		this.rep_name = rep_name;
	}




	public String getActive() {
		return active;
	}




	public void setActive(String active) {
		this.active = active;
	}




	public String getInprogress() {
		return inprogress;
	}




	public void setInprogress(String inprogress) {
		this.inprogress = inprogress;
	}




	public String getOpen() {
		return open;
	}




	public void setOpen(String open) {
		this.open = open;
	}




	public String getTotal() {
		return total;
	}




	public void setTotal(String total) {
		this.total = total;
	}





	

	@Transient
	  public Properties getMappingProperties() {
	    Properties properties = new Properties();	    
	    properties.put("VP_ID", "VP_ID");
	    properties.put("VP_NAME", "VP_NAME");
	    properties.put("AD_ID", "AD_ID");
	    properties.put("AD_NAME", "AD_NAME");
	    properties.put("D_ID", "D_ID");
	    properties.put("D_NAME", "D_NAME");
	    properties.put("REP_ID", "REP_ID");
	    properties.put("REP_NAME", "REP_NAME");	
	    properties.put("ACTIVE", "ACTIVE");	
	    properties.put("INPROGRESS", "INPROGRESS");
	    properties.put("OPEN", "OPEN");
	    properties.put("TOTAL", "TOTAL");
	    return properties;
	  }

}
