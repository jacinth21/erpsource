package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMPSCaseAssVO extends GmDataStoreVO {

	private String craid = "";
	private String crid = "";
	private String role = "";
	private String partyid = "";
	private String partynm = "";
	private String adnm = "";
	private String vpnm = "";
	private String voidfl = "";


	public String getCraid() {
		return craid;
	}

	public void setCraid(String craid) {
		this.craid = craid;
	}

	public String getCrid() {
		return crid;
	}

	public void setCrid(String crid) {
		this.crid = crid;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getPartyid() {
		return partyid;
	}

	public void setPartyid(String partyid) {
		this.partyid = partyid;
	}

	public String getPartynm() {
		return partynm;
	}

	public void setPartynm(String partynm) {
		this.partynm = partynm;
	}
	

	public String getAdnm() {
		return adnm;
	}

	public void setAdnm(String adnm) {
		this.adnm = adnm;
	}
	
	public String getVpnm() {
		return vpnm;
	}

	public void setVpnm(String vpnm) {
		this.vpnm = vpnm;
	}

	public String getVoidfl() {
		return voidfl;
	}

	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}

	@Transient
	public Properties getMappingProperties() {
		Properties properties = new Properties();
		properties.put("CRAID", "CRAID");
		properties.put("CRID", "CRID");
		properties.put("ROLE", "ROLE");
		properties.put("PARTYID", "PARTYID");
		properties.put("PARTYNM", "PARTYNM");
		properties.put("ADNM", "ADNM");
		properties.put("VPNM", "VPNM");
		properties.put("VOIDFL", "VOIDFL");
		return properties;
	}

}
