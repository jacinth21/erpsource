package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmAdVpVO extends GmDataStoreVO {

  private String adid = "";
  private String adnm = "";
  private String vpid = "";
  private String vpnm = "";
  private String repnm = "";
  private String repid = "";
  private String terid = "";
  private String ternm = "";
  private String regionid = "";
  private String regionnm = "";
  private String did = "";
  private String acid = "";
  private String dnm = "";
  private String disttypenm = "";



  /**
   * @return the disttypenm
   */
  public String getDisttypenm() {
    return disttypenm;
  }



  /**
   * @param disttypenm the disttypenm to set
   */
  public void setDisttypenm(String disttypenm) {
    this.disttypenm = disttypenm;
  }


  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("ADID", "ADID");
    properties.put("ADNM", "ADNM");
    properties.put("VPID", "VPID");
    properties.put("VPNM", "VPNM");
    properties.put("TERID", "TERID");
    properties.put("TERNM", "TERNM");
    properties.put("REGIONID", "REGIONID");
    properties.put("REGIONNM", "REGIONNM");
    properties.put("REPID", "REPID");
    properties.put("REPNM", "REPNM");
    properties.put("DID", "DID");
    properties.put("DNM", "DNM");
    properties.put("ACID", "ACID");
    properties.put("DISTTYPENM", "DISTTYPENM");
    return properties;
  }



  /**
   * @return the adid
   */
  public String getAdid() {
    return adid;
  }



  /**
   * @param adid the adid to set
   */
  public void setAdid(String adid) {
    this.adid = adid;
  }



  /**
   * @return the adnm
   */
  public String getAdnm() {
    return adnm;
  }



  /**
   * @param adnm the adnm to set
   */
  public void setAdnm(String adnm) {
    this.adnm = adnm;
  }



  /**
   * @return the vpid
   */
  public String getVpid() {
    return vpid;
  }



  /**
   * @param vpid the vpid to set
   */
  public void setVpid(String vpid) {
    this.vpid = vpid;
  }



  /**
   * @return the vpnm
   */
  public String getVpnm() {
    return vpnm;
  }



  /**
   * @param vpnm the vpnm to set
   */
  public void setVpnm(String vpnm) {
    this.vpnm = vpnm;
  }



  /**
   * @return the repnm
   */
  public String getRepnm() {
    return repnm;
  }



  /**
   * @param repnm the repnm to set
   */
  public void setRepnm(String repnm) {
    this.repnm = repnm;
  }



  /**
   * @return the repid
   */
  public String getRepid() {
    return repid;
  }



  /**
   * @param repid the repid to set
   */
  public void setRepid(String repid) {
    this.repid = repid;
  }



  /**
   * @return the terid
   */
  public String getTerid() {
    return terid;
  }



  /**
   * @param terid the terid to set
   */
  public void setTerid(String terid) {
    this.terid = terid;
  }



  /**
   * @return the ternm
   */
  public String getTernm() {
    return ternm;
  }



  /**
   * @param ternm the ternm to set
   */
  public void setTernm(String ternm) {
    this.ternm = ternm;
  }



  /**
   * @return the regionid
   */
  public String getRegionid() {
    return regionid;
  }



  /**
   * @param regionid the regionid to set
   */
  public void setRegionid(String regionid) {
    this.regionid = regionid;
  }



  /**
   * @return the regionnm
   */
  public String getRegionnm() {
    return regionnm;
  }



  /**
   * @param regionnm the regionnm to set
   */
  public void setRegionnm(String regionnm) {
    this.regionnm = regionnm;
  }



  /**
   * @return the did
   */
  public String getDid() {
    return did;
  }



  /**
   * @param did the did to set
   */
  public void setDid(String did) {
    this.did = did;
  }



  /**
   * @return the acid
   */
  public String getAcid() {
    return acid;
  }



  /**
   * @param acid the acid to set
   */
  public void setAcid(String acid) {
    this.acid = acid;
  }



  /**
   * @return the dnm
   */
  public String getDnm() {
    return dnm;
  }



  /**
   * @param dnm the dnm to set
   */
  public void setDnm(String dnm) {
    this.dnm = dnm;
  }

}
