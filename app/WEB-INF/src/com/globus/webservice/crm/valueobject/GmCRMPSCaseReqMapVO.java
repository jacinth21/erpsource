package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMPSCaseReqMapVO extends GmDataStoreVO {
	
	  private String actid = "";
	  private String[] linkreqs;
	  private String[] unlinkreqs;
	public String getActid() {
		return actid;
	}
	public void setActid(String actid) {
		this.actid = actid;
	}
	public String[] getLinkreqs() {
		return linkreqs;
	}
	public void setLinkreqs(String[] linkreqs) {
		this.linkreqs = linkreqs;
	}
	public String[] getUnlinkreqs() {
		return unlinkreqs;
	}
	public void setUnlinkreqs(String[] unlinkreqs) {
		this.unlinkreqs = unlinkreqs;
	}

	  
	  
}
