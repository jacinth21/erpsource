package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMSurgeonEducationVO extends GmDataStoreVO {

  private String partyid = "";
  private String id = "";
  private String level = "";
  private String levelid = "";
  private String institutenm = "";
  private String classyear = "";
  private String course = "";
  private String designation = "";
  private String designationid = "";
  private String voidfl = "";



  @Override
  public String getPartyid() {
    return partyid;
  }

  @Override
  public void setPartyid(String partyid) {
    this.partyid = partyid;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getLevel() {
    return level;
  }

  public void setLevel(String level) {
    this.level = level;
  }

  public String getLevelid() {
    return levelid;
  }

  public void setLevelid(String levelid) {
    this.levelid = levelid;
  }

  public String getInstitutenm() {
    return institutenm;
  }

  public void setInstitutenm(String institutenm) {
    this.institutenm = institutenm;
  }

  public String getClassyear() {
    return classyear;
  }

  public void setClassyear(String classyear) {
    this.classyear = classyear;
  }

  public String getCourse() {
    return course;
  }

  public void setCourse(String course) {
    this.course = course;
  }

  public String getDesignation() {
    return designation;
  }

  public void setDesignation(String designation) {
    this.designation = designation;
  }

  public String getDesignationid() {
    return designationid;
  }

  public void setDesignationid(String designationid) {
    this.designationid = designationid;
  }

  public String getVoidfl() {
    return voidfl;
  }

  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("PARTYID", "PARTYID");
    properties.put("ID", "ID");
    properties.put("EDUCATIONLEVEL", "LEVEL");
    properties.put("EDUCATIONLEVELID", "LEVELID");
    properties.put("INSTITUTENAME", "INSTITUTENM");
    properties.put("CLASSYEAR", "CLASSYEAR");
    properties.put("COURSE", "COURSE");
    properties.put("DESIGNATION", "DESIGNATION");
    properties.put("DESIGNATIONID", "DESIGNATIONID");
    properties.put("VOIDFL", "VOIDFL");
    return properties;
  }

}
