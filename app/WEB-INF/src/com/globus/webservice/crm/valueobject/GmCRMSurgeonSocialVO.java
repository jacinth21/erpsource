package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMSurgeonSocialVO extends GmDataStoreVO {

	private String partyid = "";
	private String facebook = "";
	private String twitter = "";
	private String google = "";	 
	private String linkedin = "";
	private String openpayment = "";
	private String website = "";
	private String Vitals = "";

	public String getOpenpayment() {
		return openpayment;
	}

	public void setOpenpayment(String openpayment) {
		this.openpayment = openpayment;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getVitals() {
		return Vitals;
	}

	public void setVitals(String vitals) {
		Vitals = vitals;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public String getGoogle() {
		return google;
	}

	public void setGoogle(String google) {
		this.google = google;
	}	 

	public String getLinkedin() {
		return linkedin;
	}

	public void setLinkedin(String linkedin) {
		this.linkedin = linkedin;
	}

	/**
	 * @return the partyid
	 */
	public String getPartyid() {
		return partyid;
	}

	/**
	 * @param partyid
	 *            the partyid to set
	 */
	public void setPartyid(String partyid) {
		this.partyid = partyid;
	}

	private String voidfl = "";

	public String getVoidfl() {
		return voidfl;
	}

	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}

	@Transient
	public Properties getMappingProperties() {
		Properties properties = new Properties();
		properties.put("PARTYID", "PARTYID");
		properties.put("VOIDFL", "VOIDFL");
		properties.put("FACEBOOK", "FACEBOOK");
		properties.put("TWITTER", "TWITTER");
		properties.put("GOOGLE", "GOOGLE");
		properties.put("LINKEDIN", "LINKEDIN");
		properties.put("OPENPAYMENT", "OPENPAYMENT");
		properties.put("WEBSITE", "WEBSITE");
		properties.put("VITALS", "VITALS");

		return properties;
	}

}
