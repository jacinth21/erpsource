package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMSurgeonProfessionalVO extends GmDataStoreVO {

  private String partyid = "";
  private String title = "";
  private String institute = "";
  private String desc = "";
  private String startmonth = "";
  private String startyear = "";
  private String endmonth = "";
  private String endyear = "";
  private String stillinposition = "";
  private String groupdivision = "";
  private String country = "";
  private String countryid = "";
  private String state = "";
  private String stateid = "";
  private String city = "";
  private String industry = "";
  private String industryid = "";
  private String careerid = "";
  private String companyname = "";
  private String startdate = "";
  private String enddate = "";
  private String companydesc = "";
  private String voidfl = "";



  @Override
  public String getPartyid() {
    return partyid;
  }



  @Override
  public void setPartyid(String partyid) {
    this.partyid = partyid;
  }



  public String getTitle() {
    return title;
  }



  public void setTitle(String title) {
    this.title = title;
  }



  public String getInstitute() {
    return institute;
  }



  public void setInstitute(String institute) {
    this.institute = institute;
  }



  public String getDesc() {
    return desc;
  }



  public void setDesc(String desc) {
    this.desc = desc;
  }



  public String getStartmonth() {
    return startmonth;
  }



  public void setStartmonth(String startmonth) {
    this.startmonth = startmonth;
  }



  public String getStartyear() {
    return startyear;
  }



  public void setStartyear(String startyear) {
    this.startyear = startyear;
  }



  public String getEndmonth() {
    return endmonth;
  }



  public void setEndmonth(String endmonth) {
    this.endmonth = endmonth;
  }



  public String getEndyear() {
    return endyear;
  }



  public void setEndyear(String endyear) {
    this.endyear = endyear;
  }



  public String getStillinposition() {
    return stillinposition;
  }



  public void setStillinposition(String stillinposition) {
    this.stillinposition = stillinposition;
  }



  public String getGroupdivision() {
    return groupdivision;
  }



  public void setGroupdivision(String groupdivision) {
    this.groupdivision = groupdivision;
  }



  public String getCountry() {
    return country;
  }



  public void setCountry(String country) {
    this.country = country;
  }



  public String getCountryid() {
    return countryid;
  }



  public void setCountryid(String countryid) {
    this.countryid = countryid;
  }



  public String getState() {
    return state;
  }



  public void setState(String state) {
    this.state = state;
  }



  public String getStateid() {
    return stateid;
  }



  public void setStateid(String stateid) {
    this.stateid = stateid;
  }



  public String getCity() {
    return city;
  }



  public void setCity(String city) {
    this.city = city;
  }



  public String getIndustry() {
    return industry;
  }



  public void setIndustry(String industry) {
    this.industry = industry;
  }



  public String getIndustryid() {
    return industryid;
  }



  public void setIndustryid(String industryid) {
    this.industryid = industryid;
  }



  public String getCareerid() {
    return careerid;
  }



  public void setCareerid(String careerid) {
    this.careerid = careerid;
  }



  public String getCompanyname() {
    return companyname;
  }



  public void setCompanyname(String companyname) {
    this.companyname = companyname;
  }



  public String getStartdate() {
    return startdate;
  }



  public void setStartdate(String startdate) {
    this.startdate = startdate;
  }



  public String getEnddate() {
    return enddate;
  }



  public void setEnddate(String enddate) {
    this.enddate = enddate;
  }



  public String getCompanydesc() {
    return companydesc;
  }



  public void setCompanydesc(String companydesc) {
    this.companydesc = companydesc;
  }



  public String getVoidfl() {
    return voidfl;
  }



  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }



  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("PARTYID", "PARTYID");
    properties.put("TITLE", "TITLE");
    properties.put("INSTITUTE", "INSTITUTE");
    properties.put("DESC", "DESC");
    properties.put("STARTMONTH", "STARTMONTH");
    properties.put("STARTYEAR", "STARTYEAR");
    properties.put("ENDMONTH", "ENDMONTH");
    properties.put("ENDYEAR", "ENDYEAR");
    properties.put("STILLINPOSITION", "STILLINPOSITION");
    properties.put("GROUPDIVISION", "GROUPDIVISION");
    properties.put("COUNTRY", "COUNTRY");
    properties.put("COUNTRYID", "COUNTRYID");
    properties.put("STATE", "STATE");
    properties.put("STATEID", "STATEID");
    properties.put("CITY", "CITY");
    properties.put("INDUSTRY", "INDUSTRY");
    properties.put("INDUSTRYID", "INDUSTRYID");
    properties.put("CAREERID", "CAREERID");
    properties.put("COMPANYNAME", "COMPANYNAME");
    properties.put("STARTDAY", "STARTDATE");
    properties.put("ENDDAY", "ENDDATE");
    properties.put("COMPANYDESC", "COMPANYDESC");
    properties.put("VOIDFL", "VOIDFL");
    return properties;
  }

}
