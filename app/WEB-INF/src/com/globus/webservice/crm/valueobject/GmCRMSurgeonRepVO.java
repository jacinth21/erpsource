package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import com.globus.valueobject.common.GmDataStoreVO;

public class GmCRMSurgeonRepVO extends GmDataStoreVO {
	
	private String partyid = "";
	private String repid = "";
	private String repname = ""  ;
	private String adnm  = "" ;
	private String adid  = "";
	private String vpnm  = "";
	private String vpid  = "";
	private String primaryfl = "";
	private String plinkid = "";
	private String voidfl = "";

	
	public String getVoidfl() {
		return voidfl;
	}
	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}
	public String getPartyid() {
		return partyid;
	}
	public void setPartyid(String partyid) {
		this.partyid = partyid;
	}
	public String getRepname() {
		return repname;
	}
	public void setRepname(String repname) {
		this.repname = repname;
	}
	
	public String getPlinkid() {
		return plinkid;
	}
	public void setPlinkid(String plinkid) {
		this.plinkid = plinkid;
	}
 
	public String getRepid() {
		return repid;
	}
	public void setRepid(String repid) {
		this.repid = repid;
	}
 
	public String getAdnm() {
		return adnm;
	}
	public void setAdnm(String adnm) {
		this.adnm = adnm;
	}
	public String getAdid() {
		return adid;
	}
	public void setAdid(String adid) {
		this.adid = adid;
	}
	public String getVpnm() {
		return vpnm;
	}
	public void setVpnm(String vpnm) {
		this.vpnm = vpnm;
	}
	public String getVpid() {
		return vpid;
	}
	public void setVpid(String vpid) {
		this.vpid = vpid;
	}
	public String getPrimaryfl() {
		return primaryfl;
	}
	public void setPrimaryfl(String primaryfl) {
		this.primaryfl = primaryfl;
	}
	 @Transient
	 public Properties getMappingProperties() {
		 Properties properties = new Properties();		
		 properties.put("PARTYID", "PARTYID");
		 properties.put("REPID", "REPID");
		 properties.put("REPNAME", "REPNAME");	
		 properties.put("ADNM", "ADNM");
		 properties.put("ADID", "ADID");
		 properties.put("VPNM", "VPNM");
		 properties.put("VPID", "VPID");
		 properties.put("PRIMARYFL", "PRIMARYFL");
		 properties.put("PLINKID", "PLINKID");	
		 properties.put("VOIDFL", "VOIDFL");
		 return  properties;
	 }

}
