package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMSurgeonAttrVO extends GmDataStoreVO {

  private String attrid = "";
  private String attrtype = "";
  private String attrvalue = "";
  private String attrnm = "";
  private String partyid = "";

  /**
 * @return the partyid
 */
public String getPartyid() {
	return partyid;
}


/**
 * @param partyid the partyid to set
 */
public void setPartyid(String partyid) {
	this.partyid = partyid;
}


public String getAttrid() {
    return attrid;
  }


  public void setAttrid(String attrid) {
    this.attrid = attrid;
  }


  public String getAttrnm() {
    return attrnm;
  }


  public void setAttrnm(String attrnm) {
    this.attrnm = attrnm;
  }



  private String voidfl = "";


  public String getAttrtype() {
    return attrtype;
  }


  public void setAttrtype(String attrtype) {
    this.attrtype = attrtype;
  }


  public String getAttrvalue() {
    return attrvalue;
  }


  public void setAttrvalue(String attrvalue) {
    this.attrvalue = attrvalue;
  }


  public String getVoidfl() {
    return voidfl;
  }


  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }


  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("ATTRID", "ATTRID");
    properties.put("ATTRTYPE", "ATTRTYPE");
    properties.put("ATTRVALUE", "ATTRVALUE");
    properties.put("ATTRNM", "ATTRNM");
    properties.put("VOIDFL", "VOIDFL");
    properties.put("PARTYID", "PARTYID");
    return properties;
  }

}
