package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import com.globus.valueobject.common.GmDataStoreVO;

public class GmCRMCalendarVO extends GmDataStoreVO {

	private String ord = "";
	private String surgdt = "";
	private String segid = "";
	private String segnm = "";
	private String sysid = "";
	private String sysnm = "";
	private String cnt = "";
	private String fromdate = "";
	private String todate = "";
	
	

	private List<GmCRMCalendarVO> listGmCRMCalendarVO = null;
	
	public String getOrd() {
		return ord;
	}

	public void setOrd(String ord) {
		this.ord = ord;
	}

	public String getSurgdt() {
		return surgdt;
	}

	public void setSurgdt(String surgdt) {
		this.surgdt = surgdt;
	}

	public String getSegid() {
		return segid;
	}

	public void setSegid(String segid) {
		this.segid = segid;
	}

	public String getSegnm() {
		return segnm;
	}

	public void setSegnm(String segnm) {
		this.segnm = segnm;
	}

	public String getSysid() {
		return sysid;
	}

	public void setSysid(String sysid) {
		this.sysid = sysid;
	}

	public String getSysnm() {
		return sysnm;
	}

	public void setSysnm(String sysnm) {
		this.sysnm = sysnm;
	}
	public String getCnt() {
		return cnt;
	}

	public void setCnt(String cnt) {
		this.cnt = cnt;
	}

	public String getFromdate() {
		return fromdate;
	}

	public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}

	public String getTodate() {
		return todate;
	}

	public void setTodate(String todate) {
		this.todate = todate;
	}

	

	public List<GmCRMCalendarVO> getListGmCRMCalendarVO() {
		return listGmCRMCalendarVO;
	}

	public void setListGmCRMCalendarVO(List<GmCRMCalendarVO> listGmCRMCalendarVO) {
		this.listGmCRMCalendarVO = listGmCRMCalendarVO;
	}

	@Transient
	public Properties getMappingProperties() {
		Properties properties = new Properties();
		properties.put("ORD", "ORD");
		properties.put("SURGDT", "SURGDT");
		properties.put("SEGID", "SEGID");
		properties.put("SEGNM", "SEGNM");
		properties.put("SYSID", "SYSID");
		properties.put("SYSNM", "SYSNM");
		properties.put("CNT", "CNT");
		properties.put("FROMDATE", "FROMDATE");
		properties.put("TODATE", "TODATE");
		return properties;
	}

}
