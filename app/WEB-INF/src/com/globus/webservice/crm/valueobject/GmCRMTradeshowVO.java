package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author TMuthusamy 
 * GmCRMTradeshowVO --- Tradeshow VO
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMTradeshowVO extends GmDataStoreVO {
	
	private String tradeid = "";
	private String divisionid = "";
	private String status = "";

	public String getTradeid() {
		return tradeid;
	}

	public void setTradeid(String tradeid) {
		this.tradeid = tradeid;
	}
	
	public String getDivisionid() {
		return divisionid;
	}

	public void setDivisionid(String divisionid) {
		this.divisionid = divisionid;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	@Transient
	public Properties getMappingProperties() {
		Properties properties = new Properties();
		properties.put("TRADEID", "TRADEID");
		properties.put("DIVISIONID", "DIVISIONID");
		properties.put("STATUS", "STATUS");
		return properties;
	}
}
