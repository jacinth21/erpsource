package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
import com.fasterxml.jackson.annotation.JsonRootName;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName("gmCRMSalesRepListVO")
public class GmCRMSalesRepListVO extends GmDataStoreVO {

  private String partyid = "";
  private String firstnm = "";
  private String lastnm = "";
  private String partynm = "";
  private String midinitial = "";
  private String repid = "";
  private String distid = "";
  private String distname = "";
  private String adname = "";
  private String vpname = "";
  private String phone = "";
  private String email = "";
  private String catid = "";
  private String category = "";
  private String terid = "";
  private String tername = "";
  private String regid = "";
  private String regnm = "";
  private String designation = "";
  private String title = "";
  private String adid = "";
  private String vpid = "";
  private String state = "";
  private String zip = "";
  private String repname = "";
  /**
 * @return the state
 */
public String getState() {
	return state;
}



/**
 * @param state the state to set
 */
public void setState(String state) {
	this.state = state;
}



/**
 * @return the zip
 */
public String getZip() {
	return zip;
}



/**
 * @param zip the zip to set
 */
public void setZip(String zip) {
	this.zip = zip;
}



/**
 * @return the adid
 */
public String getAdid() {
	return adid;
}



/**
 * @param adid the adid to set
 */
public void setAdid(String adid) {
	this.adid = adid;
}



/**
 * @return the vpid
 */
public String getVpid() {
	return vpid;
}



/**
 * @param vpid the vpid to set
 */
public void setVpid(String vpid) {
	this.vpid = vpid;
}



/**
 * @return the repname
 */
public String getRepname() {
	return repname;
}



/**
 * @param repname the repname to set
 */
public void setRepname(String repname) {
	this.repname = repname;
}





  public String getAdname() {
    return adname;
  }



  public void setAdname(String adname) {
    this.adname = adname;
  }



  public String getVpname() {
    return vpname;
  }



  public void setVpname(String vpname) {
    this.vpname = vpname;
  }



  public String getRegid() {
    return regid;
  }



  public void setRegid(String regid) {
    this.regid = regid;
  }



  public String getRegnm() {
    return regnm;
  }



  public void setRegnm(String regnm) {
    this.regnm = regnm;
  }



  public String getPartyid() {
    return partyid;
  }



  public void setPartyid(String partyid) {
    this.partyid = partyid;
  }



  public String getFirstnm() {
    return firstnm;
  }



  public void setFirstnm(String firstnm) {
    this.firstnm = firstnm;
  }



  public String getLastnm() {
    return lastnm;
  }



  public void setLastnm(String lastnm) {
    this.lastnm = lastnm;
  }



  public String getPartynm() {
    return partynm;
  }



  public void setPartynm(String partynm) {
    this.partynm = partynm;
  }



  public String getMidinitial() {
    return midinitial;
  }



  public void setMidinitial(String midinitial) {
    this.midinitial = midinitial;
  }



  public String getRepid() {
    return repid;
  }



  public void setRepid(String repid) {
    this.repid = repid;
  }



  public String getDistid() {
    return distid;
  }



  public void setDistid(String distid) {
    this.distid = distid;
  }



  public String getDistname() {
    return distname;
  }



  public void setDistname(String distname) {
    this.distname = distname;
  }



  public String getPhone() {
    return phone;
  }



  public void setPhone(String phone) {
    this.phone = phone;
  }



  public String getEmail() {
    return email;
  }



  public void setEmail(String email) {
    this.email = email;
  }



  public String getCatid() {
    return catid;
  }



  public void setCatid(String catid) {
    this.catid = catid;
  }



  public String getCategory() {
    return category;
  }



  public void setCategory(String category) {
    this.category = category;
  }



  public String getTerid() {
    return terid;
  }



  public void setTerid(String terid) {
    this.terid = terid;
  }



  public String getTername() {
    return tername;
  }



  public void setTername(String tername) {
    this.tername = tername;
  }



  public String getDesignation() {
    return designation;
  }



  public void setDesignation(String designation) {
    this.designation = designation;
  }



  public String getTitle() {
    return title;
  }



  public void setTitle(String title) {
    this.title = title;
  }


  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("PARTYID", "PARTYID");
    properties.put("FIRSTNM", "FIRSTNM");
    properties.put("LASTNM", "LASTNM");
    properties.put("PARTYNM", "PARTYNM");
    properties.put("MIDINITIAL", "MIDINITIAL");
    properties.put("REPID", "REPID");
    properties.put("DISTID", "DISTID");
    properties.put("DISTNAME", "DISTNAME");
    properties.put("ADNAME", "ADNAME");
    properties.put("VPNAME", "VPNAME");
    properties.put("PHONE", "PHONE");
    properties.put("EMAIL", "EMAIL");
    properties.put("CATID", "CATID");
    properties.put("CATEGORY", "CATEGORY");
    properties.put("TERID", "TERID");
    properties.put("TERNAME", "TERNAME");
    properties.put("REGID", "REGID");
    properties.put("REGNM", "REGNM");
    properties.put("DESIGNATION", "DESIGNATION");
    properties.put("TITLE", "TITLE");
    properties.put("ADID", "ADID");
    properties.put("VPID", "VPID");
    properties.put("STATE", "STATE");
    properties.put("ZIP", "ZIP");
    properties.put("REPNAME", "REPNAME");
    return properties;
  }

}
