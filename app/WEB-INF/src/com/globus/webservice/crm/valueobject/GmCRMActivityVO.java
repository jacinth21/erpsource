package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName("gmCRMActivityVO")
public class GmCRMActivityVO extends GmDataStoreVO {

  private String actid = "";
  private String actnm = "";
  private String actstartdate = "";
  private String actenddate = "";
  private String actdesc = "";
  private String voidfl = "";
  private String acttype = "";
  private String acttypenm = "";
  private String actstatus = "";
  private String actstatusid = "";
  private String actcategory = "";
  private String activitytype = "";
  private String crfstatus = "";
  private String devicefl = "";
  private String createdby = "";
  private String createddate = "";
  private String accessfl = "";
  private String toppriority = "";
  private String otherinstr = "";
  private String division = "";
  private String divisionnm = "";
  private String actnotes = "";
  private String actstatusnm = "";
  private String dietary= "";
  private String tzone= "";
  private String region = "";
  private String regionnm = "";
  private String segid= "";
  private String segnm= "";
  private String pathlgy= "";
  private String setid= "";
  private String workflow= "";   
  private String workflownm= "";  
  private String firstname="";
  private String lastname="";
  private String name="";
  private String zone= "";
  private String adminsts= ""; 
  private String zonenm= "";
  private String adminstsnm= "";

  
  
/**
 * @return the name
 */
public String getName() {
	return name;
}


/**
 * @param name the name to set
 */
public void setName(String name) {
	this.name = name;
}


public String getFirstname() {
	return firstname;
}

 
public void setFirstname(String firstname) {
	this.firstname = firstname;
}

 
public String getLastname() {
	return lastname;
}

 
public void setLastname(String lastname) {
	this.lastname = lastname;
}


  
  public String getWorkflow() {
    return workflow;
  }

  public void setWorkflow(String workflow) {
    this.workflow = workflow;
  }

  public String getWorkflownm() {
    return workflownm;
  }

  public void setWorkflownm(String workflownm) {
    this.workflownm = workflownm;
  }

  public String getSetid() {
		return setid;
	}
	 
	public void setSetid(String setid) {
		this.setid = setid;
	}

  
  public String getRegionnm() {
    return regionnm;
  }

  public void setRegionnm(String regionnm) {
    this.regionnm = regionnm;
  }

  public String getSegnm() {
    return segnm;
  }

  public void setSegnm(String segnm) {
    this.segnm = segnm;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public String getSegid() {
    return segid;
  }

  public void setSegid(String segid) {
    this.segid = segid;
  }

  public String getPathlgy() {
    return pathlgy;
  }

  public void setPathlgy(String pathlgy) {
    this.pathlgy = pathlgy;
  }

  public String getTzone() {
	return tzone;
}

public void setTzone(String tzone) {
	this.tzone = tzone;
}

public String getDietary() {
	return dietary;
}

public void setDietary(String dietary) {
	this.dietary = dietary;
}



public String getActnotes() {
    return actnotes;
  }

  public void setActnotes(String actnotes) {
    this.actnotes = actnotes;
  }


  public String getDivisionnm() {
    return divisionnm;
  }



  public void setDivisionnm(String divisionnm) {
    this.divisionnm = divisionnm;
  }



  public String getDivision() {
    return division;
  }



  public void setDivision(String division) {
    this.division = division;
  }



  public String getToppriority() {
    return toppriority;
  }



  public void setToppriority(String toppriority) {
    this.toppriority = toppriority;
  }



  public String getOtherinstr() {
    return otherinstr;
  }



  public void setOtherinstr(String otherinstr) {
    this.otherinstr = otherinstr;
  }



  public String getAccessfl() {
    return accessfl;
  }



  public void setAccessfl(String accessfl) {
    this.accessfl = accessfl;
  }



  /**
   * @return the createdby
   */
  public String getCreatedby() {
    return createdby;
  }



  /**
   * @param createdby the createdby to set
   */
  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }



  /**
   * @return the createddate
   */
  public String getCreateddate() {
    return createddate;
  }



  /**
   * @param createddate the createddate to set
   */
  public void setCreateddate(String createddate) {
    this.createddate = createddate;
  }



  /**
   * @return the crfstatus
   */
  public String getCrfstatus() {
    return crfstatus;
  }



  /**
   * @param crfstatus the crfstatus to set
   */
  public void setCrfstatus(String crfstatus) {
    this.crfstatus = crfstatus;
  }



  public String getDevicefl() {
    return devicefl;
  }



  public void setDevicefl(String devicefl) {
    this.devicefl = devicefl;
  }



  /**
   * @return the activitytype
   */
  public String getActivitytype() {
    return activitytype;
  }



  /**
   * @param activitytype the activitytype to set
   */
  public void setActivitytype(String activitytype) {
    this.activitytype = activitytype;
  }

  private String location = "";
  private String phynm = "";
  private String acttypeid = "";
  private String partyid = "";
  private String crtfrmdate = "";
  private String crttodate = "";
  private String firstnm = "";
  private String lastnm = "";
  private String sourcetypid = "";
  private String sourcetypnm = "";
  private String source = "";
  private String products = "";
  private String result = "";
  private String resultfor = "";
  private String resulttyp = "";
  private String createdbyid = "";
  private String passname = "";
  private String address = "";
  private String statenm = "";
  private String zip = "";
  private String picurl = "";
  private String state = "";
  private String procedure = "";
  private String systemid = "";
  private String assignee = "";
  private String gendate = "";
  private String partynm = "";
  private String areauserid = "";
  private String actstarttime = "";
  private String actendtime = "";
  private String surgpartyid = "";
  private String attrpartyid = "";
  private String travelstatus = "";
  private String travelcoordinator = "";
  private String notes = "";
  private String travelnotes = "";
  
  /**
   * @return the surgpartyid
   */
  public String getSurgpartyid() {
    return surgpartyid;
  }



  /**
   * @return the attrpartyid
   */
  public String getAttrpartyid() {
    return attrpartyid;
  }



  /**
   * @param attrpartyid the attrpartyid to set
   */
  public void setAttrpartyid(String attrpartyid) {
    this.attrpartyid = attrpartyid;
  }



  /**
   * @param surgpartyid the surgpartyid to set
   */
  public void setSurgpartyid(String surgpartyid) {
    this.surgpartyid = surgpartyid;
  }



  /**
   * @return the actstarttime
   */
  public String getActstarttime() {
    return actstarttime;
  }



  /**
   * @param actstarttime the actstarttime to set
   */
  public void setActstarttime(String actstarttime) {
    this.actstarttime = actstarttime;
  }



  /**
   * @return the actendtime
   */
  public String getActendtime() {
    return actendtime;
  }



  /**
   * @param actendtime the actendtime to set
   */
  public void setActendtime(String actendtime) {
    this.actendtime = actendtime;
  }



  /**
   * @return the areauserid
   */
  public String getAreauserid() {
    return areauserid;
  }



  /**
   * @param areauserid the areauserid to set
   */
  public void setAreauserid(String areauserid) {
    this.areauserid = areauserid;
  }



  /**
   * @return the partynm
   */
  public String getPartynm() {
    return partynm;
  }



  /**
   * @param partynm the partynm to set
   */
  public void setPartynm(String partynm) {
    this.partynm = partynm;
  }



  /**
   * @return the gendate
   */
  public String getGendate() {
    return gendate;
  }



  /**
   * @param gendate the gendate to set
   */
  public void setGendate(String gendate) {
    this.gendate = gendate;
  }



  /**
   * @return the assignee
   */
  public String getAssignee() {
    return assignee;
  }



  /**
   * @param assignee the assignee to set
   */
  public void setAssignee(String assignee) {
    this.assignee = assignee;
  }



  public String getState() {
    return state;
  }



  public void setState(String state) {
    this.state = state;
  }



  public String getProcedure() {
    return procedure;
  }



  public void setProcedure(String procedure) {
    this.procedure = procedure;
  }



  public String getSystemid() {
    return systemid;
  }



  public void setSystemid(String systemid) {
    this.systemid = systemid;
  }

  private List<GmCRMSurgeonAddressListVO> arrgmcrmaddressvo = null;

  /**
   * @return the arrgmcrmaddressvo
   */
  public List<GmCRMSurgeonAddressListVO> getArrgmcrmaddressvo() {
    return arrgmcrmaddressvo;
  }



  /**
   * @param arrgmcrmaddressvo the arrgmcrmaddressvo to set
   */
  public void setArrgmcrmaddressvo(List<GmCRMSurgeonAddressListVO> arrgmcrmaddressvo) {
    this.arrgmcrmaddressvo = arrgmcrmaddressvo;
  }



  public String getPicurl() {
    return picurl;
  }



  public void setPicurl(String picurl) {
    this.picurl = picurl;
  }



  public String getStatenm() {
    return statenm;
  }



  public void setStatenm(String statenm) {
    this.statenm = statenm;
  }



  public String getZip() {
    return zip;
  }



  public void setZip(String zip) {
    this.zip = zip;
  }



  public String getPassname() {
    return passname;
  }



  public String getAddress() {
    return address;
  }



  public void setAddress(String address) {
    this.address = address;
  }



  public void setPassname(String passname) {
    this.passname = passname;
  }



  public String getResulttyp() {
    return resulttyp;
  }



  public void setResulttyp(String resulttyp) {
    this.resulttyp = resulttyp;
  }



  public String getResultfor() {
    return resultfor;
  }



  public void setResultfor(String resultfor) {
    this.resultfor = resultfor;
  }



  public String getProducts() {
    return products;
  }


  public void setProducts(String products) {
    this.products = products;
  }



  public String getResult() {
    return result;
  }



  public void setResult(String result) {
    this.result = result;
  }



  public String getSourcetypid() {
    return sourcetypid;
  }



  public void setSourcetypid(String sourcetypid) {
    this.sourcetypid = sourcetypid;
  }



  public String getSourcetypnm() {
    return sourcetypnm;
  }



  public void setSourcetypnm(String sourcetypnm) {
    this.sourcetypnm = sourcetypnm;
  }



  public String getSource() {
    return source;
  }



  public void setSource(String source) {
    this.source = source;
  }



  public String getFirstnm() {
    return firstnm;
  }



  public void setFirstnm(String firstnm) {
    this.firstnm = firstnm;
  }



  public String getLastnm() {
    return lastnm;
  }



  public void setLastnm(String lastnm) {
    this.lastnm = lastnm;
  }



  public String getActtypeid() {
    return acttypeid;
  }



  public void setActtypeid(String acttypeid) {
    this.acttypeid = acttypeid;
  }



  @Override
  public String getPartyid() {
    return partyid;
  }



  @Override
  public void setPartyid(String partyid) {
    this.partyid = partyid;
  }



  public String getCrtfrmdate() {
    return crtfrmdate;
  }



  public void setCrtfrmdate(String crtfrmdate) {
    this.crtfrmdate = crtfrmdate;
  }



  public String getCrttodate() {
    return crttodate;
  }



  public void setCrttodate(String crttodate) {
    this.crttodate = crttodate;
  }



  public String getActtypenm() {
    return acttypenm;
  }



  public void setActtypenm(String acttypenm) {
    this.acttypenm = acttypenm;
  }


  public String getPhynm() {
    return phynm;
  }



  public void setPhynm(String phynm) {
    this.phynm = phynm;
  }



  private List<GmCRMActivityAttrVO> arrgmactivityattrvo = null;
  private List<GmCRMActivityPartyVO> arrgmactivitypartyvo = null;
  private List<GmCRMLogVO> arrgmcrmlogvo = null;



  public List<GmCRMLogVO> getArrgmcrmlogvo() {
    return arrgmcrmlogvo;
  }



  public void setArrgmcrmlogvo(List<GmCRMLogVO> arrgmcrmlogvo) {
    this.arrgmcrmlogvo = arrgmcrmlogvo;
  }



  public String getActid() {
    return actid;
  }



  public void setActid(String actid) {
    this.actid = actid;
  }



  public String getActnm() {
    return actnm;
  }



  public void setActnm(String actnm) {
    this.actnm = actnm;
  }



  public String getActstartdate() {
    return actstartdate;
  }



  public void setActstartdate(String actstartdate) {
    this.actstartdate = actstartdate;
  }



  public String getActenddate() {
    return actenddate;
  }



  public void setActenddate(String actenddate) {
    this.actenddate = actenddate;
  }



  public String getActdesc() {
    return actdesc;
  }



  public void setActdesc(String actdesc) {
    this.actdesc = actdesc;
  }



  public String getVoidfl() {
    return voidfl;
  }



  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }



  public String getActtype() {
    return acttype;
  }



  public void setActtype(String acttype) {
    this.acttype = acttype;
  }



  public String getActstatus() {
    return actstatus;
  }



  public void setActstatus(String actstatus) {
    this.actstatus = actstatus;
  }



  public String getActstatusid() {
    return actstatusid;
  }



  public void setActstatusid(String actstatusid) {
    this.actstatusid = actstatusid;
  }



  public String getActcategory() {
    return actcategory;
  }



  public void setActcategory(String actcategory) {
    this.actcategory = actcategory;
  }

  public String getLocation() {
    return location;
  }



  public void setLocation(String location) {
    this.location = location;
  }


  public List<GmCRMActivityAttrVO> getArrgmactivityattrvo() {
    return arrgmactivityattrvo;
  }



  public void setArrgmactivityattrvo(List<GmCRMActivityAttrVO> arrgmactivityattrvo) {
    this.arrgmactivityattrvo = arrgmactivityattrvo;
  }



  public List<GmCRMActivityPartyVO> getArrgmactivitypartyvo() {
    return arrgmactivitypartyvo;
  }



  public void setArrgmactivitypartyvo(List<GmCRMActivityPartyVO> arrgmactivitypartyvo) {
    this.arrgmactivitypartyvo = arrgmactivitypartyvo;
  }


  public String getCreatedbyid() {
    return createdbyid;
  }



  public void setCreatedbyid(String createdbyid) {
    this.createdbyid = createdbyid;
  }
  

  public String getActstatusnm() {
	return actstatusnm;
  }



  public void setActstatusnm(String actstatusnm) {
	this.actstatusnm = actstatusnm;
  }
  
  public String getZone() {
	return zone;
  }

  public void setZone(String zone) {
	this.zone = zone;
  }

  public String getAdminsts() {
	return adminsts;
  }

  public void setAdminsts(String adminsts) {
	this.adminsts = adminsts;
  }
  
  public String getZonenm() {
	return zonenm;
  }

  public void setZonenm(String zonenm) {
	this.zonenm = zonenm;
  }

  public String getAdminstsnm() {
	return adminstsnm;
  }

  public void setAdminstsnm(String adminstsnm) {
	this.adminstsnm = adminstsnm;
  }
  
   


  public String getTravelstatus() {
	return travelstatus;
}


public void setTravelstatus(String travelstatus) {
	this.travelstatus = travelstatus;
}


public String getTravelcoordinator() {
	return travelcoordinator;
}


public void setTravelcoordinator(String travelcoordinator) {
	this.travelcoordinator = travelcoordinator;
}


public String getNotes() {
	return notes;
}


public void setNotes(String notes) {
	this.notes = notes;
}


/**
 * @return the travelnotes
 */
public String getTravelnotes() {
	return travelnotes;
}


/**
 * @param travelnotes the travelnotes to set
 */
public void setTravelnotes(String travelnotes) {
	this.travelnotes = travelnotes;
}


@Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("ACTID", "ACTID");
    properties.put("ACTNM", "ACTNM");
    properties.put("ACTSTARTDATE", "ACTSTARTDATE");
    properties.put("ACTENDDATE", "ACTENDDATE");
    properties.put("ACTDESC", "ACTDESC");
    properties.put("VOIDFL", "VOIDFL");
    properties.put("ACTTYPE", "ACTTYPE");
    properties.put("ACTTYPEID", "ACTTYPEID");
    properties.put("ACTTYPENM", "ACTTYPENM");
    properties.put("ACTSTATUS", "ACTSTATUS");
    properties.put("ACTSTATUSID", "ACTSTATUSID");
    properties.put("ACTCATEGORY", "ACTCATEGORY");
    properties.put("LOCATION", "LOCATION");
    properties.put("PHYNM", "PHYNM");
    properties.put("FIRSTNM", "FIRSTNM");
    properties.put("LASTNM", "LASTNM");
    properties.put("SOURCETYPID", "SOURCETYPID");
    properties.put("SOURCETYPNM", "SOURCETYPNM");
    properties.put("SOURCE", "SOURCE");
    properties.put("PRODUCTS", "PRODUCTS");
    properties.put("RESULT", "RESULT");
    properties.put("RESULTFOR", "RESULTFOR");
    properties.put("RESULTTYP", "RESULTTYP");
    properties.put("CREATEDBYID", "CREATEDBYID");
    properties.put("CREATEDBY", "CREATEDBY");
    properties.put("CREATEDDATE", "CREATEDDATE");
    properties.put("ADDRESS", "ADDRESS");
    properties.put("STATENM", "STATENM");
    properties.put("ZIP", "ZIP");
    properties.put("PICURL", "PICURL");
    properties.put("STATE", "STATE");
    properties.put("PROCEDURE", "PROCEDURE");
    properties.put("SYSTEMID", "SYSTEMID");
    properties.put("ASSIGNEE", "ASSIGNEE");
    properties.put("PARTYID", "PARTYID");
    properties.put("GENDATE", "GENDATE");
    properties.put("PARTYNM", "PARTYNM");
    properties.put("AREAUSERID", "AREAUSERID");
    properties.put("ACTSTARTTIME", "ACTSTARTTIME");
    properties.put("ACTENDTIME", "ACTENDTIME");
    properties.put("SURGPARTYID", "SURGPARTYID");
    properties.put("ACTIVITYTYPE", "ACTIVITYTYPE");
    properties.put("CRFSTATUS", "CRFSTATUS");
    properties.put("ATTRPARTYID", "ATTRPARTYID");
    properties.put("ACCESSFL", "ACCESSFL");
    properties.put("TOPPRIORITY", "TOPPRIORITY");
    properties.put("OTHERINSTR", "OTHERINSTR");
    properties.put("DIVISION", "DIVISION");
    properties.put("DIVISIONNM", "DIVISIONNM");
    properties.put("ACTNOTES", "ACTNOTES");
    properties.put("ACTSTATUSNM", "ACTSTATUSNM");    
    properties.put("DIETARY", "DIETARY");
    properties.put("TZONE", "TZONE");   
    properties.put("SEGID", "SEGID");   
    properties.put("SEGNM", "SEGNM");      
    properties.put("REGION", "REGION"); 
    properties.put("REGIONNM", "REGIONNM"); 
	properties.put("SETID", "SETID");  
	properties.put("WORKFLOW", "WORKFLOW"); 
	properties.put("WORKFLOWNM", "WORKFLOWNM"); 
	properties.put("FIRSTNAME", "FIRSTNAME");
	properties.put("LASTNAME", "LASTNAME");
	properties.put("NAME", "NAME");
	properties.put("ZONE", "ZONE");
	properties.put("ADMINSTS", "ADMINSTS");
	properties.put("ZONENM", "ZONENM");
	properties.put("ADMINSTSNM", "ADMINSTSNM");
	properties.put("TRAVELSTATUS", "TRAVELSTATUS");
	properties.put("TRAVELCOORDINATOR", "TRAVELCOORDINATOR");
	properties.put("NOTES", "NOTES");
	properties.put("TRAVELNOTES", "TRAVELNOTES");
    return properties;
  }

}
