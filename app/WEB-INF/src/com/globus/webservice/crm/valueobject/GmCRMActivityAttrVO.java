package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMActivityAttrVO extends GmDataStoreVO {

  private String attrid = "";
  private String attid = "";

  /**
   * @return the attid
   */
  public String getAttid() {
    return attid;
  }



  /**
   * @param attid the attid to set
   */
  public void setAttid(String attid) {
    this.attid = attid;
  }



  private String attrtype = "";
  private String attrval = "";
  private String attrnm = "";
  private String voidfl = "";
  private String actid = "";


  /**
   * @return the actid
   */
  public String getActid() {
    return actid;
  }



  /**
   * @param actid the actid to set
   */
  public void setActid(String actid) {
    this.actid = actid;
  }



  public String getAttrid() {
    return attrid;
  }



  public void setAttrid(String attrid) {
    this.attrid = attrid;
  }



  public String getAttrtype() {
    return attrtype;
  }



  public void setAttrtype(String attrtype) {
    this.attrtype = attrtype;
  }



  public String getAttrval() {
    return attrval;
  }



  public void setAttrval(String attrval) {
    this.attrval = attrval;
  }



  public String getAttrnm() {
    return attrnm;
  }



  public void setAttrnm(String attrnm) {
    this.attrnm = attrnm;
  }



  public String getVoidfl() {
    return voidfl;
  }



  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }


  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("ATTRID", "ATTRID");
    properties.put("ATTRTYPE", "ATTRTYPE");
    properties.put("ATTRVAL", "ATTRVAL");
    properties.put("ATTRNM", "ATTRNM");
    properties.put("VOIDFL", "VOIDFL");
    properties.put("ACTID", "ACTID");
    properties.put("ATTID", "ATTID");
    return properties;
  }

}
