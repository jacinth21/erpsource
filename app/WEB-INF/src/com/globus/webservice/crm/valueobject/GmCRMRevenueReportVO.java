package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName("gmCRMRevenueReportVO")
public class GmCRMRevenueReportVO extends GmDataStoreVO {
	 
	
	 private String refid = "";
	 private String fromdt = "";
	 private String todt = "";
	 private String accesstype = "";	 

	 private List<GmCRMRevenueHeaderVO> gmCRMRevenueHeaderVO = null;  
	 private List<GmCRMRevenueDetailVO> gmCRMRevenueDetailVO = null;  



	public String getRefid() {
		return refid;
	}




	public void setRefid(String refid) {
		this.refid = refid;
	}




	public String getFromdt() {
		return fromdt;
	}




	public void setFromdt(String fromdt) {
		this.fromdt = fromdt;
	}




	public String getTodt() {
		return todt;
	}




	public void setTodt(String todt) {
		this.todt = todt;
	}




	public String getAccesstype() {
		return accesstype;
	}




	public void setAccesstype(String accesstype) {
		this.accesstype = accesstype;
	}




	public List<GmCRMRevenueHeaderVO> getGmCRMRevenueHeaderVO() {
		return gmCRMRevenueHeaderVO;
	}




	public void setGmCRMRevenueHeaderVO(
			List<GmCRMRevenueHeaderVO> gmCRMRevenueHeaderVO) {
		this.gmCRMRevenueHeaderVO = gmCRMRevenueHeaderVO;
	}




	public List<GmCRMRevenueDetailVO> getGmCRMRevenueDetailVO() {
		return gmCRMRevenueDetailVO;
	}




	public void setGmCRMRevenueDetailVO(
			List<GmCRMRevenueDetailVO> gmCRMRevenueDetailVO) {
		this.gmCRMRevenueDetailVO = gmCRMRevenueDetailVO;
	}


	@Transient
	  public Properties getMappingProperties() {
	    Properties properties = new Properties();
	    properties.put("ALHEADER", "MONTHVALUE");
    return properties;
  }
	
	
//	@Transient
//	  public Properties getMappingProperties() {
//	    Properties properties = new Properties();
//	    properties.put("PARTYID", "PARTYID");
//	    properties.put("PARTYNM", "PARTYNM");
//	    properties.put("MONTH1", "MONTH1");
//	    properties.put("MONTH2", "MONTH2");
//	    properties.put("MONTH3", "MONTH3");
//	    properties.put("MONTH4", "MONTH4");
//	    properties.put("MONTH5", "MONTH5");
//	    properties.put("MONTH6", "MONTH6");
//	    properties.put("TOTAL", "TOTAL");
//	    return properties;
//	  }

}
