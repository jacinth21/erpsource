package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName("gmCRMRevenueDetailVO")
public class GmCRMRevenueDetailVO extends GmDataStoreVO {
	 
	
	 private String partyid = "";
	 private String partynm = "";
	 private String yrmonth = "";
	 private String value = "";	 

	 public String getPartyid() {
			return partyid;
		}




		public void setPartyid(String partyid) {
			this.partyid = partyid;
		}
		
		 public String getPartynm() {
				return partynm;
			}




			public void setPartynm(String partynm) {
				this.partynm = partynm;
			}
			
			 public String getYrmonth() {
					return yrmonth;
				}




				public void setYrmonth(String yrmonth) {
					this.yrmonth = yrmonth;
				}

				 public String getValue() {
						return value;
					}


					public void setValue(String value) {
						this.value = value;
					}



	@Transient
	  public Properties getMappingProperties() {
	    Properties properties = new Properties();
	    properties.put("PARTYID", "PARTYID");
	    properties.put("PARTYNM", "PARTYNM");
	    properties.put("YRMONTH", "YRMONTH");
	    properties.put("VALUE", "VALUE");
	    return properties;
	  }

}
