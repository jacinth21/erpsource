package com.globus.webservice.crm.valueobject;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmLeadListVO extends GmDataStoreVO {

  private GmLeadBasicVO[] arrleadlist;

  public GmLeadBasicVO[] getArrleadlist() {
    return arrleadlist;
  }

  public void setArrleadlist(GmLeadBasicVO[] arrleadlist) {
    this.arrleadlist = arrleadlist;
  }



}
