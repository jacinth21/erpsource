package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCodeLookUpVO extends GmDataStoreVO {

  private String codeid = "";
  private String codenm = "";
  private String codegrp = "";
  private String codenmalt = "";
  private String codeaccid = "";


  public String getCodeaccid() {
    return codeaccid;
  }

  public void setCodeaccid(String codeaccid) {
    this.codeaccid = codeaccid;
  }

  public String getCodegrp() {
    return codegrp;
  }

  public void setCodegrp(String codegrp) {
    this.codegrp = codegrp;
  }

  List<GmCodeLookUpVO> listGmCodeLookUp = null;

  public String getCodeid() {
    return codeid;
  }

  public String getCodenmalt() {
    return codenmalt;
  }

  public void setCodenmalt(String codenmalt) {
    this.codenmalt = codenmalt;
  }

  public void setCodeid(String codeid) {
    this.codeid = codeid;
  }

  public String getCodenm() {
    return codenm;
  }

  public void setCodenm(String codenm) {
    this.codenm = codenm;
  }

  public List<GmCodeLookUpVO> getListGmCodeLookUp() {
    return listGmCodeLookUp;
  }

  public void setListGmCodeLookUp(List<GmCodeLookUpVO> listGmCodeLookUp) {
    this.listGmCodeLookUp = listGmCodeLookUp;
  }

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("CODEID", "CODEID");
    properties.put("CODENM", "CODENM");
    properties.put("CODEGRP", "CODEGRP");
    properties.put("CODENMALT", "CODENMALT");
    properties.put("CODEACCID", "CODEACCID");
    return properties;
  }


}
