package com.globus.webservice.crm.valueobject;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMTrainingVO extends GmDataStoreVO {

  private GmCRMTrainingListVO[] arrGmOrderVO;

  public GmCRMTrainingListVO[] getArrGmOrderVO() {
    return arrGmOrderVO;
  }

  public void setArrGmOrderVO(GmCRMTrainingListVO[] arrGmOrderVO) {
    this.arrGmOrderVO = arrGmOrderVO;
  }



}
