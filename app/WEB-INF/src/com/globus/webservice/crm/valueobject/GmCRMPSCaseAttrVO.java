package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMPSCaseAttrVO extends GmDataStoreVO {

	private String attrid = "";
	private String attrnm = "";
	private String attrval = "";
	private String attrtype = "";
	private String index = "";
	private String voidfl = "";

	public String getAttrid() {
		return attrid;
	}

	public void setAttrid(String attrid) {
		this.attrid = attrid;
	}

	public String getAttrnm() {
		return attrnm;
	}

	public void setAttrnm(String attrnm) {
		this.attrnm = attrnm;
	}

	public String getAttrval() {
		return attrval;
	}

	public void setAttrval(String attrval) {
		this.attrval = attrval;
	}

	public String getAttrtype() {
		return attrtype;
	}

	public void setAttrtype(String attrtype) {
		this.attrtype = attrtype;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getVoidfl() {
		return voidfl;
	}

	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}

	@Transient
	public Properties getMappingProperties() {
		Properties properties = new Properties();
		properties.put("ATTRID", "ATTRID");
		properties.put("ATTRNM", "ATTRNM");
		properties.put("ATTRVAL", "ATTRVAL");
		properties.put("ATTRTYPE", "ATTRTYPE");
		properties.put("INDEX", "INDEX");
		properties.put("VOIDFL", "VOIDFL");
		return properties;
	}

}
