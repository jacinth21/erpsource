package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSystemBasicVO extends GmDataStoreVO {

  private String sysid = "";
  private String sysnm = "";
  private String syslab = "";



  public String getSyslab() {
    return syslab;
  }

  public void setSyslab(String syslab) {
    this.syslab = syslab;
  }

  public String getSysid() {
    return sysid;
  }

  public void setSysid(String sysid) {
    this.sysid = sysid;
  }

  public String getSysnm() {
    return sysnm;
  }

  public void setSysnm(String sysnm) {
    this.sysnm = sysnm;
  }

  public List<GmSystemBasicVO> getListGmSystemBasicVO() {
    return listGmSystemBasicVO;
  }

  public void setListGmSystemBasicVO(List<GmSystemBasicVO> listGmSystemBasicVO) {
    this.listGmSystemBasicVO = listGmSystemBasicVO;
  }

  List<GmSystemBasicVO> listGmSystemBasicVO = null;

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("SYSID", "SYSID");
    properties.put("SYSNM", "SYSNM");
    return properties;
  }
}
