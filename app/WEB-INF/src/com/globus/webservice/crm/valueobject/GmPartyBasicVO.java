package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmPartyBasicVO extends GmDataStoreVO {

  private String partyid = "";
  private String partynm = "";
  private String fname = "";
  private String lname = "";
  private String salutenm = "";
  private String partycv = "";
  private String id = "";
  private String name = "";


  /**
   * @return the id
   */
  public String getId() {
	return id;
}
  /**
   * @param id the id to set
   */
public void setId(String id) {
	this.id = id;
}
/**
 * @return the name
 */
public String getName() {
	return name;
}
/**
 * @param name the name to set
 */
public void setName(String name) {
	this.name = name;
}

public String getFname() {
    return fname;
  }

  public void setFname(String fname) {
    this.fname = fname;
  }

  public String getLname() {
    return lname;
  }

  public void setLname(String lname) {
    this.lname = lname;
  }

  public String getSalutenm() {
    return salutenm;
  }

  public void setSalutenm(String salutenm) {
    this.salutenm = salutenm;
  }


  public String getPartycv() {
	return partycv;
}

public void setPartycv(String partycv) {
	this.partycv = partycv;
}



private String partytype = "";
  private String npid = "";

  List<GmPartyBasicVO> listGmPartyBasicVO = null;

  @Override
  public String getPartyid() {
    return partyid;
  }

  @Override
  public void setPartyid(String partyid) {
    this.partyid = partyid;
  }

  public String getPartynm() {
    return partynm;
  }

  public void setPartynm(String partynm) {
    this.partynm = partynm;
  }

  public String getPartytype() {
    return partytype;
  }

  public void setPartytype(String partytype) {
    this.partytype = partytype;
  }

  public String getNpid() {
    return npid;
  }

  public void setNpid(String npid) {
    this.npid = npid;
  }

  public List<GmPartyBasicVO> getListGmPartyBasicVO() {
    return listGmPartyBasicVO;
  }

  public void setListGmPartyBasicVO(List<GmPartyBasicVO> listGmPartyBasicVO) {
    this.listGmPartyBasicVO = listGmPartyBasicVO;
  }

@Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("PARTYID", "PARTYID");
    properties.put("PARTYNM", "PARTYNM");
    properties.put("PARTYTYPE", "PARTYTYPE");
    properties.put("NPID", "NPID");
    properties.put("SALUTENM", "SALUTENM");
    properties.put("PARTYCV", "PARTYCV");
    properties.put("ID", "ID");
    properties.put("NAME", "NAME");
    return properties;
  }

}
