package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmLeadBasicVO extends GmDataStoreVO {

  private String leadid = "";
  private String partyid = "";
  private String partynm = "";
  private String sourcenm = "";
  private String sourceid = "";
  private String relationnm = "";
  private String relationid = "";
  private String generatedon = "";
  private String channel = "";
  private String repid = "";
  private String repnm = "";
  private String email = "";
  private String phone = "";
  private String state = "";
  private String procedure = "";
  private String systemid = "";
  private String frmdate = "";
  private String todate = "";
  private String areauserid = "";
  private String voidfl = "";
  private String firstnm = "";
  private String lastnm = "";
  private String regid = "";
  private String address = "";


  public String getAddress() {
	return address;
}



	public void setAddress(String address) {
		this.address = address;
	}
	
	
	private GmCRMSurgeonlistVO gmCRMSurgeonlistVO = null;


  public String getRegid() {
    return regid;
  }



  public void setRegid(String regid) {
    this.regid = regid;
  }



  public String getAreauserid() {
    return areauserid;
  }



  public void setAreauserid(String areauserid) {
    this.areauserid = areauserid;
  }



  public GmCRMSurgeonlistVO getGmCRMSurgeonlistVO() {
    return gmCRMSurgeonlistVO;
  }



  public void setGmCRMSurgeonlistVO(GmCRMSurgeonlistVO gmCRMSurgeonlistVO) {
    this.gmCRMSurgeonlistVO = gmCRMSurgeonlistVO;
  }

  // private List<GmCRMSurgeonlistVO> listgmCRMSurgeonlistVO = null;
  //
  // public List<GmCRMSurgeonlistVO> getListgmCRMSurgeonlistVO() {
  // return listgmCRMSurgeonlistVO;
  // }
  //
  //
  //
  // public void setListgmCRMSurgeonlistVO(List<GmCRMSurgeonlistVO> listgmCRMSurgeonlistVO) {
  // this.listgmCRMSurgeonlistVO = listgmCRMSurgeonlistVO;
  // }



  public String getLeadid() {
    return leadid;
  }



  public String getState() {
    return state;
  }



  public void setProcedure(String procedure) {
    this.procedure = procedure;
  }
  
  public String getProcedure() {
	    return procedure;
	  }



	  public void setState(String state) {
	    this.state = state;
	  }

	  public void setSystemid(String systemid) {
		    this.systemid = systemid;
		  }
		  
		  public String getSystemid() {
			    return systemid;
			  }

	  
  public String getFrmdate() {
    return frmdate;
  }



  public void setFrmdate(String frmdate) {
    this.frmdate = frmdate;
  }



  public String getTodate() {
    return todate;
  }



  public void setTodate(String todate) {
    this.todate = todate;
  }



  public String getRepid() {
    return repid;
  }



  public void setRepid(String repid) {
    this.repid = repid;
  }



  public String getRepnm() {
    return repnm;
  }



  public void setRepnm(String repnm) {
    this.repnm = repnm;
  }



  public String getPartynm() {
    return partynm;
  }



  public void setPartynm(String partynm) {
    this.partynm = partynm;
  }



  public String getEmail() {
    return email;
  }



  public void setEmail(String email) {
    this.email = email;
  }



  public String getPhone() {
    return phone;
  }



  public void setPhone(String phone) {
    this.phone = phone;
  }



  public void setLeadid(String leadid) {
    this.leadid = leadid;
  }



  public String getPartyid() {
    return partyid;
  }



  public void setPartyid(String partyid) {
    this.partyid = partyid;
  }



  public String getSourcenm() {
    return sourcenm;
  }



  public void setSourcenm(String sourcenm) {
    this.sourcenm = sourcenm;
  }



  public String getSourceid() {
    return sourceid;
  }



  public void setSourceid(String sourceid) {
    this.sourceid = sourceid;
  }



  public String getRelationnm() {
    return relationnm;
  }



  public void setRelationnm(String relationnm) {
    this.relationnm = relationnm;
  }



  public String getRelationid() {
    return relationid;
  }



  public void setRelationid(String relationid) {
    this.relationid = relationid;
  }



  public String getGeneratedon() {
    return generatedon;
  }



  public void setGeneratedon(String generatedon) {
    this.generatedon = generatedon;
  }



  public String getChannel() {
    return channel;
  }



  public void setChannel(String channel) {
    this.channel = channel;
  }



  public String getVoidfl() {
    return voidfl;
  }



  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }


  public String getFirstnm() {
    return firstnm;
  }

  public void setFirstnm(String firstnm) {
    this.firstnm = firstnm;
  }



  public String getLastnm() {
    return lastnm;
  }



  public void setLastnm(String lastnm) {
    this.lastnm = lastnm;
  }


  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("LEADID", "LEADID");
    properties.put("PARTYID", "PARTYID");
    properties.put("PARTYNM", "PARTYNM");
    properties.put("SOURCENM", "SOURCENM");
    properties.put("SOURCEID", "SOURCEID");
    properties.put("RELATIONNM", "RELATIONNM");
    properties.put("RELATIONID", "RELATIONID");
    properties.put("GENERATEDON", "GENERATEDON");
    properties.put("CHANNEL", "CHANNEL");
    properties.put("REPID", "REPID");
    properties.put("REPNM", "REPNM");
    properties.put("EMAIL", "EMAIL");
    properties.put("PHONE", "PHONE");
    properties.put("STATE", "STATE");
    properties.put("PROCEDURE", "PROCEDURE");
    properties.put("SYSTEMID", "SYSTEMID");
    properties.put("FRMDATE", "FRMDATE");
    properties.put("TODATE", "TODATE");
    properties.put("VOIDFL", "VOIDFL");
    properties.put("FIRSTNM", "FIRSTNM");
    properties.put("LASTNM", "LASTNM");
    properties.put("AREAUSERID", "AREAUSERID");
    properties.put("REGID", "REGID");
    properties.put("GMCRMSURGEONLISTVO", "GMCRMSURGEONLISTVO");
    properties.put("ADDRESS", "ADDRESS");
    return properties;
  }
}
