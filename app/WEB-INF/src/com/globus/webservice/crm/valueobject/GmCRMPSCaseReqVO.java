package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMPSCaseReqVO extends GmDataStoreVO {

	private String crdid = "";
	private String crid = "";
	private String setid = "";
	private String setnm = "";
	private String voidfl = "";

	public String getCrdid() {
		return crdid;
	}

	public void setCrdid(String crdid) {
		this.crdid = crdid;
	}

	public String getCrid() {
		return crid;
	}

	public void setCrid(String crid) {
		this.crid = crid;
	}

	public String getSetid() {
		return setid;
	}

	public void setSetid(String setid) {
		this.setid = setid;
	}

	public String getSetnm() {
		return setnm;
	}

	public void setSetnm(String setnm) {
		this.setnm = setnm;
	}

	public String getVoidfl() {
		return voidfl;
	}

	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}
}
