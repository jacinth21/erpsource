package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMActivityPartyVO extends GmDataStoreVO {

  private String actpartyid = "";
  private String actid = "";
  private String partyid = "";
  private String partynpid = "";
  private String partynm = "";
  private String partytype = "";
  private String roleid = "";
  private String rolenm = "";
  private String document = "";
  private String statusid = "";
  private String statusnm = "";
  private String voidfl = "";
  private String surgeoncv = "";



  public String getActid() {
    return actid;
  }



  public void setActid(String actid) {
    this.actid = actid;
  }



  public String getPartynpid() {
    return partynpid;
  }



  public void setPartynpid(String partynpid) {
    this.partynpid = partynpid;
  }



  public String getActpartyid() {
    return actpartyid;
  }



  public void setActpartyid(String actpartyid) {
    this.actpartyid = actpartyid;
  }



  public String getPartyid() {
    return partyid;
  }



  public void setPartyid(String partyid) {
    this.partyid = partyid;
  }



  public String getPartynm() {
    return partynm;
  }



  public void setPartynm(String partynm) {
    this.partynm = partynm;
  }



  public String getPartytype() {
    return partytype;
  }



  public void setPartytype(String partytype) {
    this.partytype = partytype;
  }



  public String getRoleid() {
    return roleid;
  }



  public void setRoleid(String roleid) {
    this.roleid = roleid;
  }



  public String getRolenm() {
    return rolenm;
  }



  public void setRolenm(String rolenm) {
    this.rolenm = rolenm;
  }



  public String getDocument() {
    return document;
  }



  public void setDocument(String document) {
    this.document = document;
  }


  public String getStatusid() {
    return statusid;
  }


  public void setStatusid(String statusid) {
    this.statusid = statusid;
  }


  public String getStatusnm() {
    return statusnm;
  }


  public void setStatusnm(String statusnm) {
    this.statusnm = statusnm;
  }


  public String getVoidfl() {
    return voidfl;
  }


  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }
  
  public String getSurgeoncv() {
	    return surgeoncv;
}

  public void setSurgeoncv(String surgeoncv) {
	    this.surgeoncv = surgeoncv;
}


  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("ACTPARTYID", "ACTPARTYID");
    properties.put("ACTID", "ACTID");
    properties.put("PARTYID", "PARTYID");
    properties.put("PARTYNPID", "PARTYNPID");
    properties.put("PARTYNM", "PARTYNM");
    properties.put("PARTYTYPE", "PARTYTYPE");
    properties.put("ROLEID", "ROLEID");
    properties.put("ROLENM", "ROLENM");
    properties.put("DOCUMENT", "DOCUMENT");
    properties.put("STATUSID", "STATUSID");
    properties.put("STATUSNM", "STATUSNM");
    properties.put("VOIDFL", "VOIDFL");
    properties.put("SURGEONCV", "SURGEONCV");
    return properties;
  }

}
