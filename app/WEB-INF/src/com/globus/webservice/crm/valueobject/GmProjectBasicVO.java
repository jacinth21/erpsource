package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmProjectBasicVO extends GmDataStoreVO {

  private String projectid = "";
  private String projectname = "";
  private String voidfl = "";
  
  private String roleid = "";
  private String rolenm = "";

  
  public String getRolenm() {
	return rolenm;
}



public void setRolenm(String rolenm) {
	this.rolenm = rolenm;
}



public String getRoleid() {
	return roleid;
}



public void setRoleid(String roleid) {
	this.roleid = roleid;
}


 





  /**
 * @return the voidfl
 */
public String getVoidfl() {
	return voidfl;
}



/**
 * @param voidfl the voidfl to set
 */
public void setVoidfl(String voidfl) {
	this.voidfl = voidfl;
}



List<GmProjectBasicVO> listGmProjectBasicVO = null;



  public String getProjectid() {
    return projectid;
  }



  public void setProjectid(String projectid) {
    this.projectid = projectid;
  }



  public String getProjectname() {
    return projectname;
  }



  public void setProjectname(String projectname) {
    this.projectname = projectname;
  }



  public List<GmProjectBasicVO> getListGmProjectBasicVO() {
    return listGmProjectBasicVO;
  }



  public void setListGmProjectBasicVO(List<GmProjectBasicVO> listGmProjectBasicVO) {
    this.listGmProjectBasicVO = listGmProjectBasicVO;
  }


  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("PROJECTID", "PROJECTID");
    properties.put("PROJECTNAME", "PROJECTNAME");
    properties.put("VOIDFL", "VOIDFL");
    properties.put("ROLEID", "ROLEID");
    properties.put("ROLENM", "ROLENM");
    return properties;
  }

}
