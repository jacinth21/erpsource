package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCriteriaVO extends GmDataStoreVO {

  private String criteriaid = "";
  private String criterianm = "";
  private String criteriatyp = "";
  private String voidfl = "";
  private String usernm = "";
  private String userid = "";

  /**
 * @return the userid
 */
public String getUserid() {
	return userid;
}

/**
 * @param userid the userid to set
 */
public void setUserid(String userid) {
	this.userid = userid;
}

public String getUsernm() {
    return usernm;
  }

  public void setUsernm(String usernm) {
    this.usernm = usernm;
  }

  List<GmCriteriaAttrVO> listGmCriteriaAttrVO = null;

  public List<GmCriteriaAttrVO> getListGmCriteriaAttrVO() {
    return listGmCriteriaAttrVO;
  }

  public void setListGmCriteriaAttrVO(List<GmCriteriaAttrVO> listGmCriteriaAttrVO) {
    this.listGmCriteriaAttrVO = listGmCriteriaAttrVO;
  }

  private List<GmCriteriaVO> listGmCriteriaVO = null;

  public List<GmCriteriaVO> getListGmCriteriaVO() {
    return listGmCriteriaVO;
  }

  public void setListGmCriteriaVO(List<GmCriteriaVO> listGmCriteriaVO) {
    this.listGmCriteriaVO = listGmCriteriaVO;
  }

  public String getCriteriaid() {
    return criteriaid;
  }

  public void setCriteriaid(String criteriaid) {
    this.criteriaid = criteriaid;
  }

  public String getCriterianm() {
    return criterianm;
  }

  public void setCriterianm(String criterianm) {
    this.criterianm = criterianm;
  }

  public String getCriteriatyp() {
    return criteriatyp;
  }

  public void setCriteriatyp(String criteriatyp) {
    this.criteriatyp = criteriatyp;
  }

  public String getVoidfl() {
    return voidfl;
  }

  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("CRITERIAID", "CRITERIAID");
    properties.put("CRITERIANM", "CRITERIANM");
    properties.put("CRITERIATYP", "CRITERIATYP");
    properties.put("VOIDFL", "VOIDFL");
    properties.put("USERNM", "USERNM");
    properties.put("USERID", "USERID");
    return properties;
  }



}
