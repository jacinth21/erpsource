package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMTabsVO extends GmDataStoreVO {

  private String funcid;
  private String funcnm;
  private String readacc;
  private String updateacc;
  private String voidacc;



  public String getReadacc() {
    return readacc;
  }



  public void setReadacc(String readacc) {
    this.readacc = readacc;
  }



  public String getUpdateacc() {
    return updateacc;
  }



  public void setUpdateacc(String updateacc) {
    this.updateacc = updateacc;
  }



  public String getVoidacc() {
    return voidacc;
  }



  public void setVoidacc(String voidacc) {
    this.voidacc = voidacc;
  }



  public String getFuncid() {
    return funcid;
  }



  public void setFuncid(String funcid) {
    this.funcid = funcid;
  }



  public String getFuncnm() {
    return funcnm;
  }



  public void setFuncnm(String funcnm) {
    this.funcnm = funcnm;
  }


  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("FUNCID", "FUNCID");
    properties.put("FUNCNM", "FUNCNM");
    properties.put("READACC", "READACC");
    properties.put("UPDATEACC", "UPDATEACC");
    properties.put("VOIDACC", "VOIDACC");
    return properties;
  }

}
