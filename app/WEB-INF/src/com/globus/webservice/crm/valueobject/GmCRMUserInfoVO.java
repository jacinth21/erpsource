package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMUserInfoVO extends GmDataStoreVO {

  private String userid = "";
  private String usernm = "";
  private String email = "";
  private String partyid = "";

  @Override
  public String getUserid() {
    return userid;
  }

  @Override
  public void setUserid(String userid) {
    this.userid = userid;
  }

  public String getUsernm() {
    return usernm;
  }

  public void setUsernm(String usernm) {
    this.usernm = usernm;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPartyid() {
    return partyid;
  }

  public void setPartyid(String partyid) {
    this.partyid = partyid;
  }

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("USERID", "USERID");
    properties.put("USERNM", "USERNM");
    properties.put("EMAIL", "EMAIL");
    properties.put("PARTYID", "PARTYID");

    return properties;
  }

}
