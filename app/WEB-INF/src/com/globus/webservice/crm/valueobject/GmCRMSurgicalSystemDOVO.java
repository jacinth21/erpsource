package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName("gmCRMSurgicalSystemDOVO")
public class GmCRMSurgicalSystemDOVO extends GmDataStoreVO {
	 
	
	 private String systemid = "";
	 private String fromdt = "";
	 private String todt = "";
	 private String acctid = "";
	 private String orderdt = "";
	 private String orderid = "";
	 private String partnum = "";
	 private String pdesc = "";
	 private String qty = "";
	 private String price = "";
	 private String total = "";
	 
	 
	
	
	public String getSystemid() {
    return systemid;
  }




  public void setSystemid(String systemid) {
    this.systemid = systemid;
  }




  public String getFromdt() {
    return fromdt;
  }




  public void setFromdt(String fromdt) {
    this.fromdt = fromdt;
  }




  public String getTodt() {
    return todt;
  }




  public void setTodt(String todt) {
    this.todt = todt;
  }



  public String getAcctid() {
    return acctid;
  }




  public void setAcctid(String acctid) {
    this.acctid = acctid;
  }




  public String getOrderdt() {
    return orderdt;
  }




  public void setOrderdt(String orderdt) {
    this.orderdt = orderdt;
  }




  public String getOrderid() {
    return orderid;
  }




  public void setOrderid(String orderid) {
    this.orderid = orderid;
  }




  public String getPartnum() {
    return partnum;
  }




  public void setPartnum(String partnum) {
    this.partnum = partnum;
  }




  public String getPdesc() {
    return pdesc;
  }




  public void setPdesc(String pdesc) {
    this.pdesc = pdesc;
  }




  public String getQty() {
    return qty;
  }




  public void setQty(String qty) {
    this.qty = qty;
  }




  public String getPrice() {
    return price;
  }




  public void setPrice(String price) {
    this.price = price;
  }




  public String getTotal() {
    return total;
  }




  public void setTotal(String total) {
    this.total = total;
  }




  @Transient
	  public Properties getMappingProperties() {
	    Properties properties = new Properties();
	    properties.put("ORDERID", "ORDERID");
	    properties.put("ORDERDT", "ORDERDT");
	    properties.put("PARTNUM", "PARTNUM");
	    properties.put("PDESC", "PDESC");
	    properties.put("QUANTITY", "QTY");
	    properties.put("PRICE", "PRICE");
	    properties.put("TOTAL", "TOTAL");
	    return properties;
	  }
	
}
