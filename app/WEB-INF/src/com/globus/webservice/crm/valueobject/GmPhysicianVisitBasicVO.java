package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmPhysicianVisitBasicVO extends GmDataStoreVO {

  private String phyname = "";
  private String phyid = "";
  private String partyid = "";

  List<GmPhysicianVisitBasicVO> listGmPhysicianVisitBasicVO = null;

  public String getPhyname() {
    return phyname;
  }

  public void setPhyname(String phyname) {
    this.phyname = phyname;
  }

  public String getPhyid() {
    return phyid;
  }

  public void setPhyid(String phyid) {
    this.phyid = phyid;
  }

  public String getPartyid() {
    return partyid;
  }

  public void setPartyid(String partyid) {
    this.partyid = partyid;
  }

  public List<GmPhysicianVisitBasicVO> getListGmPhysicianVisitBasicVO() {
    return listGmPhysicianVisitBasicVO;
  }

  public void setListGmPhysicianVisitBasicVO(
      List<GmPhysicianVisitBasicVO> listGmPhysicianVisitBasicVO) {
    this.listGmPhysicianVisitBasicVO = listGmPhysicianVisitBasicVO;
  }

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("PHYNAME", "PHYNAME");
    properties.put("PHYID", "PHYID");
    properties.put("PARTYID", "PARTYID");
    return properties;
  }
}
