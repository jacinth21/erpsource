package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName("gmCRMSurgicalActivityVO")
public class GmCRMSurgicalActivityVO extends GmDataStoreVO {

	 private String yearmon = "";
	 private String value = "";
	 private String grpid = "";
	 private String grpnm = "";
	 private String grpid2 = "";
	 private String grpnm2 = "";
	 private String grpid3 = "";
	 private String grpnm3 = "";
	 private String fromdate = "";
	 private String todate = "";
	 
	 
	public String getYearmon() {
		return yearmon;
	}
	public void setYearmon(String yearmon) {
		this.yearmon = yearmon;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getGrpid() {
		return grpid;
	}
	public void setGrpid(String grpid) {
		this.grpid = grpid;
	}
	public String getGrpnm() {
		return grpnm;
	}
	public void setGrpnm(String grpnm) {
		this.grpnm = grpnm;
	}
	public String getGrpid2() {
		return grpid2;
	}
	public void setGrpid2(String grpid2) {
		this.grpid2 = grpid2;
	}
	public String getGrpnm2() {
		return grpnm2;
	}
	public void setGrpnm2(String grpnm2) {
		this.grpnm2 = grpnm2;
	}
	public String getGrpid3() {
		return grpid3;
	}
	public void setGrpid3(String grpid3) {
		this.grpid3 = grpid3;
	}
	public String getGrpnm3() {
		return grpnm3;
	}
	public void setGrpnm3(String grpnm3) {
		this.grpnm3 = grpnm3;
	}
	public String getFromdate() {
		return fromdate;
	}
	public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}
	public String getTodate() {
		return todate;
	}
	public void setTodate(String todate) {
		this.todate = todate;
	}

	@Transient
	  public Properties getMappingProperties() {
	    Properties properties = new Properties();	    
	    properties.put("YEARMON", "YEARMON");
	    properties.put("VALUE", "VALUE");
	    properties.put("GRPID", "GRPID");
	    properties.put("GRPNM", "GRPNM");
	    properties.put("GRPID2", "GRPID2");
	    properties.put("GRPNM2", "GRPNM2");
	    properties.put("GRPID3", "GRPID3");
	    properties.put("GRPNM3", "GRPNM3");	
	    properties.put("FROMDATE", "FROMDATE");	
	    properties.put("TODATE", "TODATE");
	    return properties;
	  }

}
