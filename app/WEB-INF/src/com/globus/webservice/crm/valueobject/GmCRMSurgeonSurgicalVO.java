package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMSurgeonSurgicalVO extends GmDataStoreVO {

  private String procedureid = "";
  private String procedurenm = "";
  private String cases = "";
  private String sysid = "";
  private String sysnm = "";
  private String company = "";
  private String comments = "";
  private String commentid = "";
  private String commentnm = "";
  private String partyid = "";
  /**
 * @return the partyid
 */
public String getPartyid() {
	return partyid;
}


/**
 * @param partyid the partyid to set
 */
public void setPartyid(String partyid) {
	this.partyid = partyid;
}


/**
 * @return the commentnm
 */
public String getCommentnm() {
	return commentnm;
}


/**
 * @param commentnm the commentnm to set
 */
public void setCommentnm(String commentnm) {
	this.commentnm = commentnm;
}


private String sactid = "";
  private String voidfl = "";
  
  
public String getSactid() {
	return sactid;
}


public void setSactid(String sactid) {
	this.sactid = sactid;
}


public String getCommentid() {
	return commentid;
}


public void setCommentid(String commentid) {
	this.commentid = commentid;
}


  public String getProcedureid() {
    return procedureid;
  }


  public void setProcedureid(String procedureid) {
    this.procedureid = procedureid;
  }


  public String getProcedurenm() {
    return procedurenm;
  }


  public void setProcedurenm(String procedurenm) {
    this.procedurenm = procedurenm;
  }


  public String getCases() {
    return cases;
  }


  public void setCases(String cases) {
    this.cases = cases;
  }


  public String getSysid() {
    return sysid;
  }


  public void setSysid(String sysid) {
    this.sysid = sysid;
  }


  public String getSysnm() {
    return sysnm;
  }


  public void setSysnm(String sysnm) {
    this.sysnm = sysnm;
  }


  public String getCompany() {
    return company;
  }


  public void setCompany(String company) {
    this.company = company;
  }


  public String getComments() {
    return comments;
  }


  public void setComments(String comments) {
    this.comments = comments;
  }


  public String getVoidfl() {
    return voidfl;
  }


  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("PROCEDUREID", "PROCEDUREID");
    properties.put("PROCEDURENM", "PROCEDURENM");
    properties.put("CASES", "CASES");
    properties.put("SYSID", "SYSID");
    properties.put("SYSNM", "SYSNM");
    properties.put("COMPANY", "COMPANY");
    properties.put("COMMENTS", "COMMENTS");
    properties.put("COMMENTID", "COMMENTID");
    properties.put("COMMENTNM", "COMMENTNM");
    properties.put("VOIDFL", "VOIDFL");
    properties.put("SACTID", "SACTID");
    properties.put("PARTYID", "PARTYID");
    return properties;
  }

}
