package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMTrainingListVO extends GmCRMTrainingVO {

  private String trid = "";
  private String trtitle = "";
  private String trdate = "";
  private String trsalesrepid = "";
  private String trsalesrepname = "";
  private String trstatus = "";
  private String partyid = "";
  private String trcomments = "";

  public String getTrid() {
    return trid;
  }


  public void setTrid(String trid) {
    this.trid = trid;
  }


  public String getTrtitle() {
    return trtitle;
  }


  public void setTrtitle(String trtitle) {
    this.trtitle = trtitle;
  }


  public String getTrdate() {
    return trdate;
  }


  public void setTrdate(String trdate) {
    this.trdate = trdate;
  }


  public String getTrsalesrepid() {
    return trsalesrepid;
  }


  public void setTrsalesrepid(String trsalesrepid) {
    this.trsalesrepid = trsalesrepid;
  }


  public String getTrsalesrepname() {
    return trsalesrepname;
  }


  public void setTrsalesrepname(String trsalesrepname) {
    this.trsalesrepname = trsalesrepname;
  }


  public String getTrstatus() {
    return trstatus;
  }


  public void setTrstatus(String trstatus) {
    this.trstatus = trstatus;
  }


  public String getPartyid() {
    return partyid;
  }


  public void setPartyid(String partyid) {
    this.partyid = partyid;
  }


  public String getTrcomments() {
    return trcomments;
  }


  public void setTrcomments(String trcomments) {
    this.trcomments = trcomments;
  }

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("TRID", "TRID");
    properties.put("TRTITLE", "TRTITLE");
    properties.put("TRDATE", "TRDATE");
    properties.put("TRSALESREPID", "TRSALESREPID");
    properties.put("TRSALESREPNAME", "TRSALESREPNAME");
    properties.put("TRSTATUS", "TRSTATUS");
    properties.put("PARTYID", "PARTYID");
    properties.put("TRCOMMENTS", "TRCOMMENTS");
    return properties;
  }

}
