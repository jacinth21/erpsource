package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRFApprovalVO extends GmDataStoreVO {

  private String crfapprid;
  private String crfid;
  private String partyid;
  private String partynm;
  private String statusid;
  private String statusnm;
  private String comments;
  private String updatedby;
  private String updatedon;

  public String getCrfapprid() {
    return crfapprid;
  }

  public void setCrfapprid(String crfapprid) {
    this.crfapprid = crfapprid;
  }

  public String getCrfid() {
    return crfid;
  }

  public void setCrfid(String crfid) {
    this.crfid = crfid;
  }


  @Override
  public String getPartyid() {
    return partyid;
  }


  @Override
  public void setPartyid(String partyid) {
    this.partyid = partyid;
  }

  public String getPartynm() {
    return partynm;
  }

  public void setPartynm(String partynm) {
    this.partynm = partynm;
  }

  public String getStatusid() {
    return statusid;
  }

  public void setStatusid(String statusid) {
    this.statusid = statusid;
  }

  public String getStatusnm() {
    return statusnm;
  }

  public void setStatusnm(String statusnm) {
    this.statusnm = statusnm;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public String getUpdatedby() {
    return updatedby;
  }

  public void setUpdatedby(String updatedby) {
    this.updatedby = updatedby;
  }

  public String getUpdatedon() {
    return updatedon;
  }

  public void setUpdatedon(String updatedon) {
    this.updatedon = updatedon;
  }

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("CRFAPPRID", "CRFAPPRID");
    properties.put("CRFID", "CRFID");
    properties.put("PARTYID", "PARTYID");
    properties.put("PARTYNM", "PARTYNM");
    properties.put("STATUSID", "STATUSID");
    properties.put("STATUSNM", "STATUSNM");
    properties.put("COMMENTS", "COMMENTS");
    properties.put("UPDATEDON", "UPDATEDON");
    properties.put("UPDATEDBY", "UPDATEDBY");
    return properties;
  }
}
