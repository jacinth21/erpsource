package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCriteriaAttrVO extends GmDataStoreVO {

  private String criteriaid = "";
  private String key = "";
  private String value = "";
  private String voidfl = "";
  private String attrid = ""; 

  /**
 * @return the attrid
 */
public String getAttrid() {
	return attrid;
}

/**
 * @param attrid the attrid to set
 */
public void setAttrid(String attrid) {
	this.attrid = attrid;
}

/**
 * @return the voidfl
 */
public String getVoidfl() {
	return voidfl;
}

/**
 * @param voidfl the voidfl to set
 */
public void setVoidfl(String voidfl) {
	this.voidfl = voidfl;
}

public String getCriteriaid() {
    return criteriaid;
  }

  public void setCriteriaid(String criteriaid) {
    this.criteriaid = criteriaid;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("CRITERIAID", "CRITERIAID");
    properties.put("KEY", "KEY");
    properties.put("VALUE", "VALUE");
    properties.put("ATTRID", "ATTRID");
    properties.put("VOIDFL", "VOIDFL");
    return properties;
  }

}
