package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMSurgeonlistVO extends GmDataStoreVO {

  private String partyid = "";
  private String npid = "";
  private String firstnm = "";
  private String lastnm = "";
  private String partynm = "";
  private String midinitial = "";
  private String phone = "";
  private String email = "";
  private String city = "";
  private String name = "";
  private String procedure = "";
  private String system = "";
  private String state = "";
  private String systemid = "";
  private String accid = "";
  private String visitdate = "";
  private String specality = "";
  private String actnm = "";
  private String acttyp = "";
  private String actdate = "";
  private String actfrmdate = "";
  private String acttodate = "";
  private String asstnm = "";
  private String mngrnm = "";
  private String role = "";
  private String repid = "";
  private String address = "";
  private String account = "";
  private String adnm = "";
  private String vpnm = "";
  private String repnm = "";
  private String picurl = "";
  private String picfileid = "";
  private String keywords = "";
  private String addid = "";
  private String add1 = "";
  private String add2 = "";
  private String statenm = "";
  private String countrynm = "";
  private String middleinitial = "";
  private String salute = "";
  private String salutenm = "";
  private String merccnt = "";
  private String pvisitcnt = "";
  private String scallcnt = "";


  private String level = "";
  private String levelid = "";
  private String institutenm = "";
  private String classyear = "";
  private String course = "";
  private String designation = "";
  private String designationid = "";


  private String title = "";
  private String institute = "";
  private String startmonth = "";
  private String startyear = "";
  private String endmonth = "";
  private String endyear = "";
  private String stillinposition = "";
  private String groupdivision = "";
  private String country = "";
  private String countryid = "";
  private String stateid = "";
  private String companyname = "";
  private String companyid = "";

  private String reporttype = "";
  private String resultcnt = "";
  private String surgacsfl = "";

  private String projectid = "";
  private String projectname = "";
  private String roleid = "";
  private String rolenm = "";

  private String accesslvl = "";
  private String status = "";
  private String ref_id = "";

  public String getRef_id() {
    return ref_id;
  }


  public void setRef_id(String ref_id) {
    this.ref_id = ref_id;
  }


  public String getAccesslvl() {
    return accesslvl;
  }


  public void setAccesslvl(String accesslvl) {
    this.accesslvl = accesslvl;
  }


  public String getStatus() {
    return status;
  }


  public void setStatus(String status) {
    this.status = status;
  }

  private List<GmCRMSurgeonSocialVO> arrgmcrmsurgeonsocialvo = null;
  public List<GmCRMSurgeonSocialVO> getArrgmcrmsurgeonsocialvo() {
	return arrgmcrmsurgeonsocialvo;
}


public void setArrgmcrmsurgeonsocialvo(
		List<GmCRMSurgeonSocialVO> arrgmcrmsurgeonsocialvo) {
	this.arrgmcrmsurgeonsocialvo = arrgmcrmsurgeonsocialvo;
}



  private List<GmCRMSurgeonRepVO> arrgmcrmsurgeonrepvo = null;

  public List<GmCRMSurgeonRepVO> getArrgmcrmsurgeonrepvo() {
    return arrgmcrmsurgeonrepvo;
  }


  public void setArrgmcrmsurgeonrepvo(List<GmCRMSurgeonRepVO> arrgmcrmsurgeonrepvo) {
    this.arrgmcrmsurgeonrepvo = arrgmcrmsurgeonrepvo;
  }


  public String getRolenm() {
    return rolenm;
  }


  public void setRolenm(String rolenm) {
    this.rolenm = rolenm;
  }


  private List<GmProjectBasicVO> arrgmcrmprojectbasicvo = null;

  public String getProjectid() {
    return projectid;
  }


  public void setProjectid(String projectid) {
    this.projectid = projectid;
  }


  public String getProjectname() {
    return projectname;
  }


  public void setProjectname(String projectname) {
    this.projectname = projectname;
  }


  public String getRoleid() {
    return roleid;
  }


  public void setRoleid(String roleid) {
    this.roleid = roleid;
  }



  public List<GmProjectBasicVO> getArrgmcrmprojectbasicvo() {
    return arrgmcrmprojectbasicvo;
  }


  public void setArrgmcrmprojectbasicvo(List<GmProjectBasicVO> arrgmcrmprojectbasicvo) {
    this.arrgmcrmprojectbasicvo = arrgmcrmprojectbasicvo;
  }


  public String getSurgacsfl() {
    return surgacsfl;
  }


  public void setSurgacsfl(String surgacsfl) {
    this.surgacsfl = surgacsfl;
  }


  public String getPicfileid() {
    return picfileid;
  }



  public void setPicfileid(String picfileid) {
    this.picfileid = picfileid;
  }



  public String getResultcnt() {
    return resultcnt;
  }



  public void setResultcnt(String resultcnt) {
    this.resultcnt = resultcnt;
  }



  public String getLevelid() {
    return levelid;
  }



  public void setLevelid(String levelid) {
    this.levelid = levelid;
  }



  public String getDesignationid() {
    return designationid;
  }



  public void setDesignationid(String designationid) {
    this.designationid = designationid;
  }



  public String getInstitute() {
    return institute;
  }



  public void setInstitute(String institute) {
    this.institute = institute;
  }



  public String getCountry() {
    return country;
  }



  public void setCountry(String country) {
    this.country = country;
  }



  public String getCountryid() {
    return countryid;
  }



  public void setCountryid(String countryid) {
    this.countryid = countryid;
  }



  public String getStateid() {
    return stateid;
  }



  public void setStateid(String stateid) {
    this.stateid = stateid;
  }



  public String getReporttype() {
    return reporttype;
  }



  public void setReporttype(String reporttype) {
    this.reporttype = reporttype;
  }

  public String getLevel() {
    return level;
  }



  public void setLevel(String level) {
    this.level = level;
  }



  public String getInstitutenm() {
    return institutenm;
  }



  public void setInstitutenm(String institutenm) {
    this.institutenm = institutenm;
  }



  public String getClassyear() {
    return classyear;
  }



  public void setClassyear(String classyear) {
    this.classyear = classyear;
  }



  public String getCourse() {
    return course;
  }



  public void setCourse(String course) {
    this.course = course;
  }



  public String getDesignation() {
    return designation;
  }



  public void setDesignation(String designation) {
    this.designation = designation;
  }



  public String getTitle() {
    return title;
  }



  public void setTitle(String title) {
    this.title = title;
  }


  public String getStartmonth() {
    return startmonth;
  }



  public void setStartmonth(String startmonth) {
    this.startmonth = startmonth;
  }



  public String getStartyear() {
    return startyear;
  }



  public void setStartyear(String startyear) {
    this.startyear = startyear;
  }



  public String getEndmonth() {
    return endmonth;
  }



  public void setEndmonth(String endmonth) {
    this.endmonth = endmonth;
  }



  public String getEndyear() {
    return endyear;
  }



  public void setEndyear(String endyear) {
    this.endyear = endyear;
  }



  public String getStillinposition() {
    return stillinposition;
  }



  public void setStillinposition(String stillinposition) {
    this.stillinposition = stillinposition;
  }



  public String getGroupdivision() {
    return groupdivision;
  }



  public void setGroupdivision(String groupdivision) {
    this.groupdivision = groupdivision;
  }



  public String getCompanyname() {
    return companyname;
  }



  public void setCompanyname(String companyname) {
    this.companyname = companyname;
  }

  

  public String getCompanyid() {
	return companyid;
}


public void setCompanyid(String companyid) {
	this.companyid = companyid;
}


  public String getMerccnt() {
    return merccnt;
  }



  public void setMerccnt(String merccnt) {
    this.merccnt = merccnt;
  }



  public String getPvisitcnt() {
    return pvisitcnt;
  }



  public void setPvisitcnt(String pvisitcnt) {
    this.pvisitcnt = pvisitcnt;
  }



  public String getScallcnt() {
    return scallcnt;
  }



  public void setScallcnt(String scallcnt) {
    this.scallcnt = scallcnt;
  }



  /**
   * @return the middleinitial
   */
  public String getMiddleinitial() {
    return middleinitial;
  }



  /**
   * @param middleinitial the middleinitial to set
   */
  public void setMiddleinitial(String middleinitial) {
    this.middleinitial = middleinitial;
  }



  /**
   * @return the firstname
   */
  public String getFirstname() {
    return firstname;
  }



  /**
   * @param firstname the firstname to set
   */
  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }



  /**
   * @return the lastname
   */
  public String getLastname() {
    return lastname;
  }



  /**
   * @param lastname the lastname to set
   */
  public void setLastname(String lastname) {
    this.lastname = lastname;
  }


  public String getSalute() {
    return salute;
  }



  public void setSalute(String salute) {
    this.salute = salute;
  }

  public String getSalutenm() {
    return salutenm;
  }



  public void setSalutenm(String salutenm) {
    this.salutenm = salutenm;
  }

  private String zipcode = "";
  private String adid = "";
  private String search = "";
  private String aphone = "";
  private String aemail = "";
  private String bphone = "";
  private String voidfl = "";
  private String activefl = "";
  private String firstname = "";
  private String lastname = "";

  /**
   * @return the activefl
   */
  public String getActivefl() {
    return activefl;
  }



  /**
   * @param activefl the activefl to set
   */
  public void setActivefl(String activefl) {
    this.activefl = activefl;
  }



  /**
   * @return the voidfl
   */
  public String getVoidfl() {
    return voidfl;
  }



  /**
   * @param voidfl the voidfl to set
   */
  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }



  /**
   * @return the acid
   */
  public String getAcid() {
    return acid;
  }



  /**
   * @param acid the acid to set
   */
  public void setAcid(String acid) {
    this.acid = acid;
  }


  private String bemail = "";
  private String spl = "";
  private String splid = "";
  private String reppartyid = "";
  private String did = "";
  private String dnm = "";
  private String ternm = "";
  private String terid = "";
  private String vpid = "";
  private String acid = "";

  /**
   * @return the vpid
   */
  public String getVpid() {
    return vpid;
  }



  /**
   * @param vpid the vpid to set
   */
  public void setVpid(String vpid) {
    this.vpid = vpid;
  }



  /**
   * @return the did
   */
  public String getDid() {
    return did;
  }



  /**
   * @param did the did to set
   */
  public void setDid(String did) {
    this.did = did;
  }



  /**
   * @return the dnm
   */
  public String getDnm() {
    return dnm;
  }



  /**
   * @param dnm the dnm to set
   */
  public void setDnm(String dnm) {
    this.dnm = dnm;
  }



  /**
   * @return the ternm
   */
  public String getTernm() {
    return ternm;
  }



  /**
   * @param ternm the ternm to set
   */
  public void setTernm(String ternm) {
    this.ternm = ternm;
  }



  /**
   * @return the terid
   */
  public String getTerid() {
    return terid;
  }



  /**
   * @param terid the terid to set
   */
  public void setTerid(String terid) {
    this.terid = terid;
  }



  /**
   * @return the regionnm
   */
  public String getRegionnm() {
    return regionnm;
  }



  /**
   * @param regionnm the regionnm to set
   */
  public void setRegionnm(String regionnm) {
    this.regionnm = regionnm;
  }



  /**
   * @return the regionid
   */
  public String getRegionid() {
    return regionid;
  }



  /**
   * @param regionid the regionid to set
   */
  public void setRegionid(String regionid) {
    this.regionid = regionid;
  }


  private String regionnm = "";
  private String regionid = "";

  /**
   * @return the reppartyid
   */
  public String getReppartyid() {
    return reppartyid;
  }



  /**
   * @param reppartyid the reppartyid to set
   */
  public void setReppartyid(String reppartyid) {
    this.reppartyid = reppartyid;
  }


  private List<GmCRMSurgeonAddressListVO> arrgmsurgeonsaddressvo = null;
  private List<GmCRMSurgeonContactsListVO> arrgmsurgeoncontactsvo = null;
  private List<GmCRMSurgeonAttrVO> arrgmcrmsurgeonattrvo = null;
  private List<GmCRMSurgeonAffiliationVO> arrgmcrmsurgeonaffiliationvo = null;
  private List<GmCRMSurgeonSurgicalVO> arrgmcrmsurgeonsurgicalvo = null;
  private List<GmCRMSurgeonActivityVO> arrgmcrmsurgeonactivityvo = null;
  private List<GmCRMFileUploadVO> arrgmcrmfileuploadvo = null;
  private List<GmCRMLogVO> arrgmcrmlogvo = null;
  private List<GmCRMSurgeonEducationVO> arrgmcrmeducationvo = null;
  private List<GmCRMSurgeonProfessionalVO> arrgmcrmprofessionalvo = null;



  public List<GmCRMSurgeonProfessionalVO> getArrgmcrmprofessionalvo() {
    return arrgmcrmprofessionalvo;
  }



  public void setArrgmcrmprofessionalvo(List<GmCRMSurgeonProfessionalVO> arrgmcrmprofessionalvo) {
    this.arrgmcrmprofessionalvo = arrgmcrmprofessionalvo;
  }



  public List<GmCRMSurgeonEducationVO> getArrgmcrmeducationvo() {
    return arrgmcrmeducationvo;
  }

  public void setArrgmcrmeducationvo(List<GmCRMSurgeonEducationVO> arrgmcrmeducationvo) {
    this.arrgmcrmeducationvo = arrgmcrmeducationvo;
  }



  /**
   * @return the arrgmcrmlogvo
   */
  public List<GmCRMLogVO> getArrgmcrmlogvo() {
    return arrgmcrmlogvo;
  }



  /**
   * @param arrgmcrmlogvo the arrgmcrmlogvo to set
   */
  public void setArrgmcrmlogvo(List<GmCRMLogVO> arrgmcrmlogvo) {
    this.arrgmcrmlogvo = arrgmcrmlogvo;
  }



  public List<GmCRMFileUploadVO> getArrgmcrmfileuploadvo() {
    return arrgmcrmfileuploadvo;
  }



  public void setArrgmcrmfileuploadvo(List<GmCRMFileUploadVO> arrgmcrmfileuploadvo) {
    this.arrgmcrmfileuploadvo = arrgmcrmfileuploadvo;
  }



  public List<GmCRMSurgeonAddressListVO> getArrgmsurgeonsaddressvo() {
    return arrgmsurgeonsaddressvo;
  }



  public void setArrgmsurgeonsaddressvo(List<GmCRMSurgeonAddressListVO> arrgmsurgeonsaddressvo) {
    this.arrgmsurgeonsaddressvo = arrgmsurgeonsaddressvo;
  }



  public List<GmCRMSurgeonContactsListVO> getArrgmsurgeoncontactsvo() {
    return arrgmsurgeoncontactsvo;
  }



  public void setArrgmsurgeoncontactsvo(List<GmCRMSurgeonContactsListVO> arrgmsurgeoncontactsvo) {
    this.arrgmsurgeoncontactsvo = arrgmsurgeoncontactsvo;
  }



  public List<GmCRMSurgeonAttrVO> getArrgmcrmsurgeonattrvo() {
    return arrgmcrmsurgeonattrvo;
  }



  public String getAsstnm() {
    return asstnm;
  }



  public void setAsstnm(String asstnm) {
    this.asstnm = asstnm;
  }



  public String getMngrnm() {
    return mngrnm;
  }



  public void setMngrnm(String mngrnm) {
    this.mngrnm = mngrnm;
  }



  public void setArrgmcrmsurgeonattrvo(List<GmCRMSurgeonAttrVO> arrgmcrmsurgeonattrvo) {
    this.arrgmcrmsurgeonattrvo = arrgmcrmsurgeonattrvo;
  }



  public List<GmCRMSurgeonAffiliationVO> getArrgmcrmsurgeonaffiliationvo() {
    return arrgmcrmsurgeonaffiliationvo;
  }



  public void setArrgmcrmsurgeonaffiliationvo(
      List<GmCRMSurgeonAffiliationVO> arrgmcrmsurgeonaffiliationvo) {
    this.arrgmcrmsurgeonaffiliationvo = arrgmcrmsurgeonaffiliationvo;
  }



  public List<GmCRMSurgeonSurgicalVO> getArrgmcrmsurgeonsurgicalvo() {
    return arrgmcrmsurgeonsurgicalvo;
  }



  public void setArrgmcrmsurgeonsurgicalvo(List<GmCRMSurgeonSurgicalVO> arrgmcrmsurgeonsurgicalvo) {
    this.arrgmcrmsurgeonsurgicalvo = arrgmcrmsurgeonsurgicalvo;
  }



  public List<GmCRMSurgeonActivityVO> getArrgmcrmsurgeonactivityvo() {
    return arrgmcrmsurgeonactivityvo;
  }



  public void setArrgmcrmsurgeonactivityvo(List<GmCRMSurgeonActivityVO> arrgmcrmsurgeonactivityvo) {
    this.arrgmcrmsurgeonactivityvo = arrgmcrmsurgeonactivityvo;
  }



  @Override
  public String getPartyid() {
    return partyid;
  }



  @Override
  public void setPartyid(String partyid) {
    this.partyid = partyid;
  }



  public String getNpid() {
    return npid;
  }



  public void setNpid(String npid) {
    this.npid = npid;
  }



  public String getFirstnm() {
    return firstnm;
  }



  public void setFirstnm(String firstnm) {
    this.firstnm = firstnm;
  }



  public String getLastnm() {
    return lastnm;
  }



  public void setLastnm(String lastnm) {
    this.lastnm = lastnm;
  }



  public String getPartynm() {
    return partynm;
  }



  public void setPartynm(String partynm) {
    this.partynm = partynm;
  }



  public String getMidinitial() {
    return midinitial;
  }



  public void setMidinitial(String midinitial) {
    this.midinitial = midinitial;
  }

  public String getPhone() {
    return phone;
  }



  public void setPhone(String phone) {
    this.phone = phone;
  }



  public String getEmail() {
    return email;
  }



  public void setEmail(String email) {
    this.email = email;
  }



  public String getCity() {
    return city;
  }



  public void setCity(String city) {
    this.city = city;
  }



  public String getName() {
    return name;
  }



  public void setName(String name) {
    this.name = name;
  }



  public String getProcedure() {
    return procedure;
  }



  public void setProcedure(String procedure) {
    this.procedure = procedure;
  }


  public String getSystem() {
    return system;
  }



  public void setSystem(String system) {
    this.system = system;
  }



  public String getState() {
    return state;
  }



  public void setState(String state) {
    this.state = state;
  }



  public String getSystemid() {
    return systemid;
  }



  public void setSystemid(String systemid) {
    this.systemid = systemid;
  }



  public String getAccid() {
    return accid;
  }



  public void setAccid(String accid) {
    this.accid = accid;
  }



  public String getVisitdate() {
    return visitdate;
  }



  public void setVisitdate(String visitdate) {
    this.visitdate = visitdate;
  }



  public String getSpecality() {
    return specality;
  }



  public void setSpecality(String specality) {
    this.specality = specality;
  }


  public String getActtyp() {
    return acttyp;
  }



  public String getActnm() {
    return actnm;
  }



  public void setActnm(String actnm) {
    this.actnm = actnm;
  }



  public void setActtyp(String acttyp) {
    this.acttyp = acttyp;
  }



  public String getActdate() {
    return actdate;
  }



  public void setActdate(String actdate) {
    this.actdate = actdate;
  }



  public String getActfrmdate() {
    return actfrmdate;
  }



  public void setActfrmdate(String actfrmdate) {
    this.actfrmdate = actfrmdate;
  }



  public String getActtodate() {
    return acttodate;
  }



  public void setActtodate(String acttodate) {
    this.acttodate = acttodate;
  }



  public String getRole() {
    return role;
  }



  public void setRole(String role) {
    this.role = role;
  }


  public String getRepid() {
    return repid;
  }



  public void setRepid(String repid) {
    this.repid = repid;
  }


  public String getAddress() {
    return address;
  }



  public void setAddress(String address) {
    this.address = address;
  }



  public String getAccount() {
    return account;
  }



  public void setAccount(String account) {
    this.account = account;
  }


  public String getAdnm() {
    return adnm;
  }



  public void setAdnm(String adnm) {
    this.adnm = adnm;
  }



  public String getVpnm() {
    return vpnm;
  }



  public void setVpnm(String vpnm) {
    this.vpnm = vpnm;
  }



  public String getRepnm() {
    return repnm;
  }



  public void setRepnm(String repnm) {
    this.repnm = repnm;
  }



  public String getPicurl() {
    return picurl;
  }



  public void setPicurl(String picurl) {
    this.picurl = picurl;
  }


  public String getKeywords() {
    return keywords;
  }



  public void setKeywords(String keywords) {
    this.keywords = keywords;
  }


  public String getAdid() {
    return adid;
  }



  public void setAdid(String adid) {
    this.adid = adid;
  }



  public String getSearch() {
    return search;
  }


  public void setSearch(String search) {
    this.search = search;
  }

  public String getAphone() {
    return aphone;
  }

  public void setAphone(String aphone) {
    this.aphone = aphone;
  }

  public String getAemail() {
    return aemail;
  }

  public void setAemail(String aemail) {
    this.aemail = aemail;
  }

  public String getBphone() {
    return bphone;
  }

  public void setBphone(String bphone) {
    this.bphone = bphone;
  }

  public String getBemail() {
    return bemail;
  }

  public void setBemail(String bemail) {
    this.bemail = bemail;
  }

  public String getSpl() {
    return spl;
  }

  public void setSpl(String spl) {
    this.spl = spl;
  }


  public String getAddid() {
    return addid;
  }


  public void setAddid(String addid) {
    this.addid = addid;
  }


  public String getAdd1() {
    return add1;
  }


  public void setAdd1(String add1) {
    this.add1 = add1;
  }


  public String getAdd2() {
    return add2;
  }


  public void setAdd2(String add2) {
    this.add2 = add2;
  }


  public String getStatenm() {
    return statenm;
  }


  public void setStatenm(String statenm) {
    this.statenm = statenm;
  }


  public String getCountrynm() {
    return countrynm;
  }


  public void setCountrynm(String countrynm) {
    this.countrynm = countrynm;
  }

  String getZipcode() {
    return zipcode;
  }


  public void setZipcode(String zipcode) {
    this.zipcode = zipcode;
  }

  public String getSplid() {
    return splid;
  }


  public void setSplid(String splid) {
    this.splid = splid;
  }

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("PARTYID", "PARTYID");
    properties.put("FIRSTNM", "FIRSTNM");
    properties.put("LASTNM", "LASTNM");
    properties.put("PARTYNM", "PARTYNM");
    properties.put("MIDINITIAL", "MIDINITIAL");
    properties.put("NPID", "NPID");
    properties.put("PHONE", "PHONE");
    properties.put("EMAIL", "EMAIL");
    properties.put("CITY", "CITY");
    properties.put("NAME", "NAME");
    properties.put("PROCEDURE", "PROCEDURE");
    properties.put("SYSTEM", "SYSTEM");
    properties.put("STATE", "STATE");
    properties.put("SYSTEMID", "SYSTEMID");
    properties.put("ACCID", "ACCID");
    properties.put("VISITDATE", "VISITDATE");
    properties.put("SPECALITY", "SPECALITY");
    properties.put("ACTNM", "ACTNM");
    properties.put("ACTTYP", "ACTTYP");
    properties.put("ACTDATE", "ACTDATE");
    properties.put("ACTFRMDATE", "ACTFRMDATE");
    properties.put("ACTTODATE", "ACTTODATE");
    properties.put("ASSTNM", "ASSTNM");
    properties.put("MNGRNM", "MNGRNM");
    properties.put("ROLE", "ROLE");
    properties.put("REPID", "REPID");
    properties.put("ADDRESS", "ADDRESS");
    properties.put("ACCOUNT", "ACCOUNT");
    properties.put("REPNM", "REPNM");
    properties.put("ADNM", "ADNM");
    properties.put("VPNM", "VPNM");
    properties.put("PICURL", "PICURL");
    properties.put("KEYWORDS", "KEYWORDS");
    properties.put("ADID", "ADID");
    properties.put("SEARCH", "SEARCH");
    properties.put("APHONE", "APHONE");
    properties.put("AEMAIL", "AEMAIL");
    properties.put("BPHONE", "BPHONE");
    properties.put("BEMAIL", "BEMAIL");
    properties.put("SPL", "SPL");
    properties.put("ADDID", "ADDID");
    properties.put("ADD1", "ADD1");
    properties.put("ADD2", "ADD2");
    properties.put("STATENM", "STATENM");
    properties.put("COUNTRYNM", "COUNTRYNM");
    properties.put("COUNTRY", "COUNTRY");
    properties.put("ZIPCODE", "ZIPCODE");
    properties.put("SPLID", "SPLID");
    properties.put("REPPARTYID", "REPPARTYID");
    properties.put("VPID", "VPID");
    properties.put("DID", "DID");
    properties.put("DNM", "DNM");
    properties.put("REGIONID", "REGIONID");
    properties.put("REGIONNM", "REGIONNM");
    properties.put("TERNM", "TERNM");
    properties.put("TERID", "TERID");
    properties.put("ACID", "ACID");
    properties.put("VOIDFL", "VOIDFL");
    properties.put("ACTIVEFL", "ACTIVEFL");
    properties.put("FIRSTNAME", "FIRSTNAME");
    properties.put("LASTNAME", "LASTNAME");
    properties.put("MIDDLEINITIAL", "MIDDLEINITIAL");
    properties.put("SALUTE", "SALUTE");
    properties.put("SALUTENM", "SALUTENM");
    properties.put("MERCCNT", "MERCCNT");
    properties.put("PVISITCNT", "PVISITCNT");
    properties.put("SCALLCNT", "SCALLCNT");
    properties.put("LEVEL", "LEVEL");
    properties.put("INSTITUTENM", "INSTITUTENM");
    properties.put("CLASSYEAR", "CLASSYEAR");
    properties.put("COURSE", "COURSE");
    properties.put("DESIGNATION", "DESIGNATION");
    properties.put("TITLE", "TITLE");
    properties.put("STARTMONTH", "STARTMONTH");
    properties.put("STARTYEAR", "STARTYEAR");
    properties.put("ENDMONTH", "ENDMONTH");
    properties.put("ENDYEAR", "ENDYEAR");
    properties.put("STILLINPOSITION", "STILLINPOSITION");
    properties.put("GROUPDIVISION", "GROUPDIVISION");
    properties.put("COMPANYNAME", "COMPANYNAME");
    properties.put("COMPANYID", "COMPANYID");
    properties.put("REPORTTYPE", "REPORTTYPE");
    properties.put("RESULTCNT", "RESULTCNT");
    properties.put("PICFILEID", "PICFILEID");


    properties.put("PROJECTID", "PROJECTID");
    properties.put("PROJECTNAME", "PROJECTNAME");
    properties.put("ROLEID", "ROLEID");
    properties.put("ROLENM", "ROLENM");
    properties.put("STATUS", "STATUS");

    return properties;
  }

}
