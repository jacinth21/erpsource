package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMCRFVO extends GmDataStoreVO {



  private String crfid = "";
  private String partyid = "";
  private String actid = "";
  private String reqtypeid = "";
  private String reqtypenm = "";
  private String reqdate = "";
  private String statusid = "";
  private String statusnm = "";
  private String servicetyp = "";
  private String reason = "";
  private String servicedesc = "";
  private String hcpqualification = "";
  private String preptime = "";
  private String workhrs = "";
  private String timefl = "";
  private String agrfl = "";
  private String trvlfrom = "";
  private String trvlto = "";
  private String trvltime = "";
  private String trvlmile = "";
  private String hrrate = "";
  private String voidfl = "";
  private String actnm = "";
  private String firstnm = "";
  private String lastnm = "";
  private String createdby = "";
  private String honstatus = "";
  private String honworkhrs = "";
  private String honprephrs = "";
  private String honcomment = "";
  private String crffrmdate = "";
  private String crftodate = "";
  private String apprpartyid = "";
  private String priorityfl = "";
  private String updatedby = "";
  private String updatedon = "";

  public String getPriorityfl() {
    return priorityfl;
  }

  public void setPriorityfl(String priorityfl) {
    this.priorityfl = priorityfl;
  }

  public String getApprpartyid() {
    return apprpartyid;
  }

  public void setApprpartyid(String apprpartyid) {
    this.apprpartyid = apprpartyid;
  }

  private GmCRMSurgeonlistVO gmCRMSurgeonlistVO = null;

  private List<GmCRMLogVO> arrgmcrmlogvo = null;

  private List<GmCRFApprovalVO> arrgmcrfapprovalvo = null;

  public List<GmCRFApprovalVO> getArrgmcrfapprovalvo() {
    return arrgmcrfapprovalvo;
  }

  public void setArrgmcrfapprovalvo(List<GmCRFApprovalVO> arrgmcrfapprovalvo) {
    this.arrgmcrfapprovalvo = arrgmcrfapprovalvo;
  }

  public List<GmCRMLogVO> getArrgmcrmlogvo() {
    return arrgmcrmlogvo;
  }

  public void setArrgmcrmlogvo(List<GmCRMLogVO> arrgmcrmlogvo) {
    this.arrgmcrmlogvo = arrgmcrmlogvo;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }


  public String getActnm() {
    return actnm;
  }

  public void setActnm(String actnm) {
    this.actnm = actnm;
  }

  public String getFirstnm() {
    return firstnm;
  }

  public void setFirstnm(String firstnm) {
    this.firstnm = firstnm;
  }

  public String getLastnm() {
    return lastnm;
  }

  public void setLastnm(String lastnm) {
    this.lastnm = lastnm;
  }

  public GmCRMSurgeonlistVO getGmCRMSurgeonlistVO() {
    return gmCRMSurgeonlistVO;
  }

  public void setGmCRMSurgeonlistVO(GmCRMSurgeonlistVO gmCRMSurgeonlistVO) {
    this.gmCRMSurgeonlistVO = gmCRMSurgeonlistVO;
  }

  public String getReqdate() {
    return reqdate;
  }

  public void setReqdate(String reqdate) {
    this.reqdate = reqdate;
  }

  public String getCrfid() {
    return crfid;
  }

  public void setCrfid(String crfid) {
    this.crfid = crfid;
  }

  @Override
  public String getPartyid() {
    return partyid;
  }

  @Override
  public void setPartyid(String partyid) {
    this.partyid = partyid;
  }

  public String getActid() {
    return actid;
  }

  public void setActid(String actid) {
    this.actid = actid;
  }

  public String getReqtypeid() {
    return reqtypeid;
  }

  public void setReqtypeid(String reqtypeid) {
    this.reqtypeid = reqtypeid;
  }

  public String getReqtypenm() {
    return reqtypenm;
  }

  public void setReqtypenm(String reqtypenm) {
    this.reqtypenm = reqtypenm;
  }

  public String getStatusid() {
    return statusid;
  }

  public void setStatusid(String statusid) {
    this.statusid = statusid;
  }

  public String getStatusnm() {
    return statusnm;
  }

  public void setStatusnm(String statusnm) {
    this.statusnm = statusnm;
  }

  public String getServicetyp() {
    return servicetyp;
  }

  public void setServicetyp(String servicetyp) {
    this.servicetyp = servicetyp;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public String getServicedesc() {
    return servicedesc;
  }

  public void setServicedesc(String servicedesc) {
    this.servicedesc = servicedesc;
  }

  public String getHcpqualification() {
    return hcpqualification;
  }

  public void setHcpqualification(String hcpqualification) {
    this.hcpqualification = hcpqualification;
  }

  public String getPreptime() {
    return preptime;
  }

  public void setPreptime(String preptime) {
    this.preptime = preptime;
  }

  public String getWorkhrs() {
    return workhrs;
  }

  public void setWorkhrs(String workhrs) {
    this.workhrs = workhrs;
  }

  public String getTimefl() {
    return timefl;
  }

  public void setTimefl(String timefl) {
    this.timefl = timefl;
  }

  public String getAgrfl() {
    return agrfl;
  }

  public void setAgrfl(String agrfl) {
    this.agrfl = agrfl;
  }

  public String getTrvlfrom() {
    return trvlfrom;
  }

  public void setTrvlfrom(String trvlfrom) {
    this.trvlfrom = trvlfrom;
  }

  public String getTrvlto() {
    return trvlto;
  }

  public void setTrvlto(String trvlto) {
    this.trvlto = trvlto;
  }

  public String getTrvltime() {
    return trvltime;
  }

  public void setTrvltime(String trvltime) {
    this.trvltime = trvltime;
  }

  public String getTrvlmile() {
    return trvlmile;
  }

  public void setTrvlmile(String trvlmile) {
    this.trvlmile = trvlmile;
  }

  public String getHrrate() {
    return hrrate;
  }

  public void setHrrate(String hrrate) {
    this.hrrate = hrrate;
  }

  public String getVoidfl() {
    return voidfl;
  }

  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }

  public String getHonstatus() {
    return honstatus;
  }

  public void setHonstatus(String honstatus) {
    this.honstatus = honstatus;
  }

  public String getHonworkhrs() {
    return honworkhrs;
  }

  public void setHonworkhrs(String honworkhrs) {
    this.honworkhrs = honworkhrs;
  }

  public String getHonprephrs() {
    return honprephrs;
  }

  public void setHonprephrs(String honprephrs) {
    this.honprephrs = honprephrs;
  }

  public String getHoncomment() {
    return honcomment;
  }

  public void setHoncomment(String honcomment) {
    this.honcomment = honcomment;
  }


  public String getCrffrmdate() {
    return crffrmdate;
  }

  public void setCrffrmdate(String crffrmdate) {
    this.crffrmdate = crffrmdate;
  }

  public String getCrftodate() {
    return crftodate;
  }

  public void setCrftodate(String crftodate) {
    this.crftodate = crftodate;
  }

  public String getUpdatedby() {
    return updatedby;
  }

  public void setUpdatedby(String updatedby) {
    this.updatedby = updatedby;
  }

  public String getUpdatedon() {
    return updatedon;
  }

  public void setUpdatedon(String updatedon) {
    this.updatedon = updatedon;
  }

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("CRFID", "CRFID");
    properties.put("PARTYID", "PARTYID");
    properties.put("ACTID", "ACTID");
    properties.put("REQTYPENM", "REQTYPENM");
    properties.put("REQTYPEID", "REQTYPEID");
    properties.put("STATUSID", "STATUSID");
    properties.put("STATUSNM", "STATUSNM");
    properties.put("SERVICETYP", "SERVICETYP");
    properties.put("REASON", "REASON");
    properties.put("SERVICEDESC", "SERVICEDESC");
    properties.put("HCPQUALIFICATION", "HCPQUALIFICATION");
    properties.put("PREPTIME", "PREPTIME");
    properties.put("WORKHRS", "WORKHRS");
    properties.put("TIMEFL", "TIMEFL");
    properties.put("AGRFL", "AGRFL");
    properties.put("TRVLFROM", "TRVLFROM");
    properties.put("TRVLTO", "TRVLTO");
    properties.put("TRVLTIME", "TRVLTIME");
    properties.put("TRVLMILE", "TRVLMILE");
    properties.put("HRRATE", "HRRATE");
    properties.put("VOIDFL", "VOIDFL");
    properties.put("GMCRMSURGEONLISTVO", "GMCRMSURGEONLISTVO");
    properties.put("ACTNM", "ACTNM");
    properties.put("FIRSTNM", "FIRSTNM");
    properties.put("LASTNM", "LASTNM");
    properties.put("HONSTATUS", "HONSTATUS");
    properties.put("HONWORKHRS", "HONWORKHRS");
    properties.put("HONPREPHRS", "HONPREPHRS");
    properties.put("HONCOMMENT", "HONCOMMENT");
    properties.put("ARRGMCRMLOGVO", "ARRGMCRMLOGVO");
    properties.put("CRFFRMDATE", "CRFFRMDATE");
    properties.put("CRFTODATE", "CRFTODATE");
    properties.put("PRIORITYFL", "PRIORITYFL");
    properties.put("CREATEDBY", "CREATEDBY");
    properties.put("UPDATEDBY", "UPDATEDBY");
    properties.put("UPDATEDON", "UPDATEDON");
    return properties;
  }


}
