package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMLogVO extends GmDataStoreVO {

  private String logid = "";
  private String refid = "";
  private String logtypeid = "";
  private String logtype = "";
  private String logcomment = "";
  private String updatedby = "";
  private String updatedbyid = "";
  private String updateddate = "";
  private String voidfl = "";
  private String userid = "";
  private String commentid = "";
  private String comments = "";
  private String commentnm = "";


  @Override
  public String getUserid() {
    return userid;
  }



  /**
 * @return the commentid
 */
public String getCommentid() {
	return commentid;
}



/**
 * @param commentid the commentid to set
 */
public void setCommentid(String commentid) {
	this.commentid = commentid;
}



/**
 * @return the comments
 */
public String getComments() {
	return comments;
}



/**
 * @param comments the comments to set
 */
public void setComments(String comments) {
	this.comments = comments;
}



/**
 * @return the commentnm
 */
public String getCommentnm() {
	return commentnm;
}



/**
 * @param commentnm the commentnm to set
 */
public void setCommentnm(String commentnm) {
	this.commentnm = commentnm;
}



@Override
  public void setUserid(String userid) {
    this.userid = userid;
  }



  public String getLogid() {
    return logid;
  }



  public void setLogid(String logid) {
    this.logid = logid;
  }



  public String getRefid() {
    return refid;
  }



  public void setRefid(String refid) {
    this.refid = refid;
  }



  public String getLogtypeid() {
    return logtypeid;
  }



  public void setLogtypeid(String logtypeid) {
    this.logtypeid = logtypeid;
  }



  public String getLogtype() {
    return logtype;
  }



  public void setLogtype(String logtype) {
    this.logtype = logtype;
  }



  public String getLogcomment() {
    return logcomment;
  }



  public void setLogcomment(String logcomment) {
    this.logcomment = logcomment;
  }



  public String getUpdatedby() {
    return updatedby;
  }



  public void setUpdatedby(String updatedby) {
    this.updatedby = updatedby;
  }



  public String getUpdateddate() {
    return updateddate;
  }



  public void setUpdateddate(String updateddate) {
    this.updateddate = updateddate;
  }



  public String getVoidfl() {
    return voidfl;
  }



  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }



  public String getUpdatedbyid() {
    return updatedbyid;
  }



  public void setUpdatedbyid(String updatedbyid) {
    this.updatedbyid = updatedbyid;
  }


  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("LOGID", "LOGID");
    properties.put("REFID", "REFID");
    properties.put("LOGTYPEID", "LOGTYPEID");
    properties.put("LOGTYPE", "LOGTYPE");
    properties.put("LOGCOMMENT", "LOGCOMMENT");
    properties.put("UPDATEDBY", "UPDATEDBY");
    properties.put("UPDATEDDATE", "UPDATEDDATE");
    properties.put("VOIDFL", "VOIDFL");
    properties.put("UPDATEDBYID", "UPDATEDBYID");
    properties.put("COMMENTID", "COMMENTID");
    properties.put("COMMENTS", "COMMENTS");
    properties.put("COMMENTNM", "COMMENTNM");
    return properties;
  }

}
