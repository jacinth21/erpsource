package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName("gmCRMRevenueHeaderVO")
public class GmCRMRevenueHeaderVO extends GmDataStoreVO {
	 
	private String monthvalue = "";	 
	private String yrmonth = "";	 
	
	
	public String getMonthvalue() {
		return monthvalue;
	}
	public void setMonthvalue(String monthvalue) {
		this.monthvalue = monthvalue;
	}
	
	public String getYrmonth() {
		return yrmonth;
	}
	public void setYrmonth(String yrmonth) {
		this.yrmonth = yrmonth;
	}




	@Transient
	  public Properties getMappingProperties() {
	    Properties properties = new Properties();
	    properties.put("MONTHVALUE", "MONTHVALUE");
	    properties.put("YRMONTH", "YRMONTH");
	    return properties;
	  }

}
