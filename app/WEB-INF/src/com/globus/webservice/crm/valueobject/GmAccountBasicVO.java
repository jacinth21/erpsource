package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmAccountBasicVO extends GmDataStoreVO {

  private String accid = "";
  private String accnm = "";
  private String acccity = "";
  private String accstate = "";
  private String accstateid = "";
  private String repid = "";
  private String repnm = "";
  private String reppartyid = "";
  private String lat = "";
  private String lon = "";
  private String distnm = "";
  private String adid = "";
  private String adnm = "";
  private String vpnm = "";

  
  public String getAdid() {
	return adid;
}

public void setAdid(String adid) {
	this.adid = adid;
}

public String getAdnm() {
	return adnm;
}

public void setAdnm(String adnm) {
	this.adnm = adnm;
}

public String getVpnm() {
	return vpnm;
}

public void setVpnm(String vpnm) {
	this.vpnm = vpnm;
}

public String getReppartyid() {
    return reppartyid;
  }

  public void setReppartyid(String reppartyid) {
    this.reppartyid = reppartyid;
  }

  public String getRepid() {
    return repid;
  }

  public void setRepid(String repid) {
    this.repid = repid;
  }

  public String getRepnm() {
    return repnm;
  }

  public void setRepnm(String repnm) {
    this.repnm = repnm;
  }

  public String getLat() {
    return lat;
  }

  public void setLat(String lat) {
    this.lat = lat;
  }

  public String getLon() {
    return lon;
  }

  public void setLon(String lon) {
    this.lon = lon;
  }

  public String getDistnm() {
    return distnm;
  }

  public void setDistnm(String distnm) {
    this.distnm = distnm;
  }

  public String getAccstateid() {
    return accstateid;
  }

  public void setAccstateid(String accstateid) {
    this.accstateid = accstateid;
  }

  public String getAcccity() {
    return acccity;
  }

  public void setAcccity(String acccity) {
    this.acccity = acccity;
  }

  public String getAccstate() {
    return accstate;
  }

  public void setAccstate(String accstate) {
    this.accstate = accstate;
  }

  List<GmAccountBasicVO> listGmAccountBasicVO = null;

  public List<GmAccountBasicVO> getListGmAccountBasicVO() {
    return listGmAccountBasicVO;
  }

  public void setListGmAccountBasicVO(List<GmAccountBasicVO> listGmAccountBasicVO) {
    this.listGmAccountBasicVO = listGmAccountBasicVO;
  }

  public String getAccid() {
    return accid;
  }

  public void setAccid(String accid) {
    this.accid = accid;
  }

  public String getAccnm() {
    return accnm;
  }

  public void setAccnm(String accnm) {
    this.accnm = accnm;
  }

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("ACCID", "ACCID");
    properties.put("ACCNM", "ACCNM");
    properties.put("ACCCITY", "ACCCITY");
    properties.put("ACCSTATE", "ACCSTATE");
    properties.put("ACCSTATEID", "ACCSTATEID");
    properties.put("REPID", "REPID");
    properties.put("REPPARTYID", "REPPARTYID");
    properties.put("REPNM", "REPNM");
    properties.put("LAT", "LAT");
    properties.put("LON", "LON");
    properties.put("DISTNM", "DISTNM");
    properties.put("ADID", "ADID");
    properties.put("ADNM", "ADNM");
    properties.put("VPNM", "VPNM");
    return properties;
  }

}
