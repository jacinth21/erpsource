package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSalesRepQuickVO extends GmDataStoreVO {
  private String repid = "";
  private String repname = "";
  private String did = "";
  private String dname = "";
  private String partyid = "";
  private String category = "";
  private String adname = "";
  private String vpname = "";

  public String getAdname() {
	return adname;
}

public void setAdname(String adname) {
	this.adname = adname;
}

public String getVpname() {
	return vpname;
}

public void setVpname(String vpname) {
	this.vpname = vpname;
}

List<GmSalesRepQuickVO> listGmSalesRepQuickVO = null;

  public List<GmSalesRepQuickVO> getListGmSalesRepQuickVO() {
    return listGmSalesRepQuickVO;
  }

  public void setListGmSalesRepQuickVO(List<GmSalesRepQuickVO> listGmSalesRepQuickVO) {
    this.listGmSalesRepQuickVO = listGmSalesRepQuickVO;
  }

  public String getRepid() {
    return repid;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public void setRepid(String repid) {
    this.repid = repid;
  }

  public String getRepname() {
    return repname;
  }

  public void setRepname(String repname) {
    this.repname = repname;
  }

  public String getDid() {
    return did;
  }

  public void setDid(String did) {
    this.did = did;
  }

  public String getDname() {
    return dname;
  }

  public void setDname(String dname) {
    this.dname = dname;
  }

  public String getPartyid() {
    return partyid;
  }

  public void setPartyid(String partyid) {
    this.partyid = partyid;
  }

  @Transient
  public Properties getMappingProperties() {
    Properties props = new Properties();
    props.put("REPNAME", "REPNAME");
    props.put("REPID", "REPID");
    props.put("DID", "DID");
    props.put("DID", "DID");
    props.put("DNAME", "DNAME");
    props.put("ADNAME", "ADNAME");
    props.put("VPNAME", "VPNAME");
    props.put("PARTYID", "PARTYID");
    props.put("CATEGORY", "CATEGORY");

    return props;
  }

}
