package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMSurgeonAffiliationVO extends GmDataStoreVO {

  private String afflid = "";
  private String accid = "";
  private String accnm = "";
  private String priority = "";
  private String casepercent = "";
  private String accstate = "";
  private String accstateid = "";
  private String acccity = "";
  private String comments = "";
  private String partyid = "";
  private String parentacctnm ="";
  private String accessflag = ""; 
  
  public String getParentacctnm() {
	return parentacctnm;
}


public void setParentacctnm(String parentacctnm) {
	this.parentacctnm = parentacctnm;
}


/**
 * @return the partyid
 */
public String getPartyid() {
	return partyid;
}


/**
 * @param partyid the partyid to set
 */
public void setPartyid(String partyid) {
	this.partyid = partyid;
}


/**
 * @return the commentnm
 */
public String getCommentnm() {
	return commentnm;
}


/**
 * @param commentnm the commentnm to set
 */
public void setCommentnm(String commentnm) {
	this.commentnm = commentnm;
}


/**
 * @return the accessflag
 */
public String getAccessflag() {
	return accessflag;
}


/**
 * @param accessflag the accessflag to set
 */
public void setAccessflag(String accessflag) {
	this.accessflag = accessflag;
}




private String commentid = "";
  private String commentnm = "";
  private String voidfl = "";


  public String getAfflid() {
    return afflid;
  }


  public void setAfflid(String afflid) {
    this.afflid = afflid;
  }


  public String getAccstateid() {
    return accstateid;
  }


  public void setAccstateid(String accstateid) {
    this.accstateid = accstateid;
  }



  public String getAccid() {
    return accid;
  }


  public void setAccid(String accid) {
    this.accid = accid;
  }


  public String getAccnm() {
    return accnm;
  }


  public void setAccnm(String accnm) {
    this.accnm = accnm;
  }


  public String getPriority() {
    return priority;
  }

  public void setPriority(String priority) {
    this.priority = priority;
  }


  public String getCasepercent() {
    return casepercent;
  }


  public void setCasepercent(String casepercent) {
    this.casepercent = casepercent;
  }


  public String getAccstate() {
    return accstate;
  }



  public void setAccstate(String accstate) {
    this.accstate = accstate;
  }



  public String getAcccity() {
    return acccity;
  }



  public void setAcccity(String acccity) {
    this.acccity = acccity;
  }



  public String getComments() {
    return comments;
  }



  public void setComments(String comments) {
    this.comments = comments;
  }



  public String getVoidfl() {
    return voidfl;
  }



  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }

  public String getCommentid() {
    return commentid;
  }

  public void setCommentid(String commentid) {
    this.commentid = commentid;
  }

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("AFFLID", "AFFLID");
    properties.put("ACCID", "ACCID");
    properties.put("ACCNM", "ACCNM");
    properties.put("PRIORITY", "PRIORITY");
    properties.put("CASEPERCENT", "CASEPERCENT");
    properties.put("ACCSTATE", "ACCSTATE");
    properties.put("ACCSTATEID", "ACCSTATEID");
    properties.put("ACCCITY", "ACCCITY");
    properties.put("COMMENTS", "COMMENTS");
    properties.put("COMMENTID", "COMMENTID");
    properties.put("COMMENTNM", "COMMENTNM");
    properties.put("VOIDFL", "VOIDFL");
    properties.put("PARTYID", "PARTYID");
    properties.put("PARENTACCTNM", "PARENTACCTNM");
    properties.put("ACCESSFLAG", "ACCESSFLAG");
    return properties;
  }

}
