package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import com.globus.valueobject.common.GmDataStoreVO;

public class GmCRMSystemDetailsVO<listGmCRMSystemDetailsVO> extends
		GmDataStoreVO {
	
	private String acctnm = "";
	private String surgdt = "";
	private String parentord = "";
	private String segmentnm = "";
	private String sysnm = "";
	private String ord = "";
	private String pnum = "";
	private String partdesc = "";
	private String qty = "";
	private String price = "";
	private String total = "";
	private String orddate = "";
	private String sysid = "";

	private List<GmCRMSystemDetailsVO> listGmCRMSystemDetailsVO = null;
	
	public String getAcctnm() {
		return acctnm;
	}

	public void setAcctnm(String acctnm) {
		this.acctnm = acctnm;
	}

	public String getSurgdt() {
		return surgdt;
	}

	public void setSurgdt(String surgdt) {
		this.surgdt = surgdt;
	}

	public String getParentord() {
		return parentord;
	}

	public void setParentord(String parentord) {
		this.parentord = parentord;
	}

	public String getSegmentnm() {
		return segmentnm;
	}

	public void setSegmentnm(String segmentnm) {
		this.segmentnm = segmentnm;
	}


	public String getSysnm() {
		return sysnm;
	}

	public void setSysnm(String sysnm) {
		this.sysnm = sysnm;
	}

	public String getOrd() {
		return ord;
	}

	public void setOrd(String ord) {
		this.ord = ord;
	}

	public String getPnum() {
		return pnum;
	}

	public void setPnum(String pnum) {
		this.pnum = pnum;
	}

	public String getPartdesc() {
		return partdesc;
	}

	public void setPartdesc(String partdesc) {
		this.partdesc = partdesc;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getOrddate() {
		return orddate;
	}

	public void setOrddate(String orddate) {
		this.orddate = orddate;
	}

	public String getSysid() {
		return sysid;
	}

	public void setSysid(String sysid) {
		this.sysid = sysid;
	}

	

	

	public List<GmCRMSystemDetailsVO> getListGmCRMSystemDetailsVO() {
		return listGmCRMSystemDetailsVO;
	}

	public void setListGmCRMSystemDetailsVO(
			List<GmCRMSystemDetailsVO> listGmCRMSystemDetailsVO) {
		this.listGmCRMSystemDetailsVO = listGmCRMSystemDetailsVO;
	}

	@Transient
	public Properties getMappingProperties() {
		Properties properties = new Properties();
		properties.put("ACCTNM", "ACCTNM");
		properties.put("SURGDT", "SURGDT");
		properties.put("PARENTORD", "PARENTORD");
		properties.put("SEGMENTNM", "SEGMENTNM");
		properties.put("SYSNM", "SYSNM");
		properties.put("ORD", "ORD");
		properties.put("PNUM", "PNUM");
		properties.put("PARTDESC", "PARTDESC");
		properties.put("QTY", "QTY");
		properties.put("PRICE", "PRICE");
		properties.put("TOTAL", "TOTAL");
		properties.put("ORDDATE", "ORDDATE");
		properties.put("SYSID", "SYSID");
		return properties;
	}

}
