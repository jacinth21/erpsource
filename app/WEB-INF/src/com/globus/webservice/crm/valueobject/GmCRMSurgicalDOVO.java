package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMSurgicalDOVO extends GmDataStoreVO {

  private String docount = "";
  private String fromdate = "";
  private String todate = "";
  private String accountid = "";
  private String segmentstr = "";
  private String systemstr = "";
  private String partyid = "";


public String getDocount() {
	return docount;
}

public void setDocount(String docount) {
	this.docount = docount;
}

public String getFromdate() {
	return fromdate;
}


public void setFromdate(String fromdate) {
	this.fromdate = fromdate;
}


public String getTodate() {
	return todate;
}


public void setTodate(String todate) {
	this.todate = todate;
}


public String getAccountid() {
	return accountid;
}


public void setAccountid(String accountid) {
	this.accountid = accountid;
}


public String getSegmentstr() {
	return segmentstr;
}


public void setSegmentstr(String segmentstr) {
	this.segmentstr = segmentstr;
}


public String getSystemstr() {
	return systemstr;
}


public void setSystemstr(String systemstr) {
	this.systemstr = systemstr;
}


public String getPartyid() {
	return partyid;
}


public void setPartyid(String partyid) {
	this.partyid = partyid;
}

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("FROMDATE", "FROMDATE");
    properties.put("TODATE", "TODATE");
    properties.put("ACCOUNTID", "ACCOUNTID");
    properties.put("SEGMENTSTR", "SEGMENTSTR");
    properties.put("SYSTEMSTR", "SYSTEMSTR");
    properties.put("PARTYID", "PARTYID");
    properties.put("DOCOUNT", "DOCOUNT");
    return properties;
  }

}
