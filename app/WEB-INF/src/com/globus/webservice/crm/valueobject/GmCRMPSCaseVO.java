package com.globus.webservice.crm.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName("GmCRMPSCaseVO")
public class GmCRMPSCaseVO extends GmDataStoreVO {
  private String casereqid = "";
  private String segid = "";
  private String actid = "";
  private String surgid = "";
  private String frmdt = "";
  private String todt = "";
  private String status = "";
  private String type = "";
  private String note = "";
  private String createdby = "";
  private String createdon = "";
  private String hotlnm = "";
  private String hotlprice = "";
  private String trvlnm = "";
  private String trvlprice = "";
  private String pcprcrf = "";
  private String trfstatis = "";
  private String labinv = "";
  private String labadd = "";
  private String pcmninv = "";
  private String crdfl = "";
  private String survfl = "";
  private String surgnm = "";
  private String procnm = "";
  private String crfnm = "";
  private String trfnm = "";
  private String division = "";
  private String workflow = "";
  private String pathology = "";
  private String dvnm = "";
  private String wfnm = "";
  private String statusnm = "";
  private String pstype = "";
  private String clobj = "";
  private String pstypenm = "";
  private String surgeoncv = "";
  private String createdbyid = "";
  private String region = "";
  private String regionnm = "";
  private String zone= "";
  private String zonenm= "";
  private String adminsts= "";    
  private String adminstsnm= "";
  private String atndtyp = ""; 
  private String atndtypnm = ""; 
  private String atndnm = "";  
  private String webinarfl = "";  


  public String getRegion() {
  	return region;
  }

  public void setRegion(String region) {
  	this.region = region;
  }

  public String getCreatedbyid() {
    return createdbyid;
  }

  public void setCreatedbyid(String createdbyid) {
    this.createdbyid = createdbyid;
  }

  public String getSurgeoncv() {
    return surgeoncv;
  }

  public void setSurgeoncv(String surgeoncv) {
    this.surgeoncv = surgeoncv;
  }

  private List<GmCRMPSCaseAssVO> arrcrmpscaseassvo = null;
  private List<GmCRMPSCaseReqVO> arrcrmpscasereqvo = null;
  private List<GmCRMPSCaseAttrVO> arrcrmpscaseattrvo = null;

  public List<GmCRMPSCaseAttrVO> getArrcrmpscaseattrvo() {
    return arrcrmpscaseattrvo;
  }

  public void setArrcrmpscaseattrvo(List<GmCRMPSCaseAttrVO> arrcrmpscaseattrvo) {
    this.arrcrmpscaseattrvo = arrcrmpscaseattrvo;
  }

  public List<GmCRMPSCaseAssVO> getArrcrmpscaseassvo() {
    return arrcrmpscaseassvo;
  }

  public void setArrgmpscasereqassvo(List<GmCRMPSCaseAssVO> arrcrmpscaseassvo) {
    this.arrcrmpscaseassvo = arrcrmpscaseassvo;
  }

  public List<GmCRMPSCaseReqVO> getArrcrmpscasereqvo() {
    return arrcrmpscasereqvo;
  }

  public void setarrcrmpscasereqvo(List<GmCRMPSCaseReqVO> arrcrmpscasereqvo) {
    this.arrcrmpscasereqvo = arrcrmpscasereqvo;
  }

  public String getSegid() {
    return segid;
  }

  public void setSegid(String segid) {
    this.segid = segid;
  }

  public String getCasereqid() {
    return casereqid;
  }

  public void setCasereqid(String casereqid) {
    this.casereqid = casereqid;
  }

  public String getActid() {
    return actid;
  }

  public void setActid(String actid) {
    this.actid = actid;
  }

  public String getSurgid() {
    return surgid;
  }

  public void setSurgid(String surgid) {
    this.surgid = surgid;
  }

  public String getFrmdt() {
    return frmdt;
  }

  public void setFrmdt(String frmdt) {
    this.frmdt = frmdt;
  }

  public String getTodt() {
    return todt;
  }

  public void setTodt(String todt) {
    this.todt = todt;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String gettype() {
    return type;
  }

  public void settype(String type) {
    this.type = type;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getCreatedon() {
    return createdon;
  }

  public void setCreatedon(String createdon) {
    this.createdon = createdon;
  }

  public String getHotlnm() {
    return hotlnm;
  }

  public void setHotlnm(String hotlnm) {
    this.hotlnm = hotlnm;
  }

  public String getHotlprice() {
    return hotlprice;
  }

  public void setHotlprice(String hotlprice) {
    this.hotlprice = hotlprice;
  }

  public String getTrvlnm() {
    return trvlnm;
  }

  public void setTrvlnm(String trvlnm) {
    this.trvlnm = trvlnm;
  }

  public String getTrvlprice() {
    return trvlprice;
  }

  public void setTrvlprice(String trvlprice) {
    this.trvlprice = trvlprice;
  }

  public String getPcprcrf() {
    return pcprcrf;
  }

  public void setPcprcrf(String pcprcrf) {
    this.pcprcrf = pcprcrf;
  }

  public String getTrfstatis() {
    return trfstatis;
  }

  public void setTrfstatis(String trfstatis) {
    this.trfstatis = trfstatis;
  }

  public String getLabinv() {
    return labinv;
  }

  public void setLabinv(String labinv) {
    this.labinv = labinv;
  }

  public String getLabadd() {
    return labadd;
  }

  public void setLabadd(String labadd) {
    this.labadd = labadd;
  }

  public String getPcmninv() {
    return pcmninv;
  }

  public void setPcmninv(String pcmninv) {
    this.pcmninv = pcmninv;
  }

  public String getCrdfl() {
    return crdfl;
  }

  public void setCrdfl(String crdfl) {
    this.crdfl = crdfl;
  }

  public String getSurvfl() {
    return survfl;
  }

  public void setSurvfl(String survfl) {
    this.survfl = survfl;
  }

  public String getSurgnm() {
    return surgnm;
  }

  public void setSurgnm(String surgnm) {
    this.surgnm = surgnm;
  }

  public String getProcnm() {
    return procnm;
  }

  public void setProcnm(String procnm) {
    this.procnm = procnm;
  }

  public String getCrfnm() {
    return crfnm;
  }

  public void setCrfnm(String crfnm) {
    this.crfnm = crfnm;
  }

  public String getTrfnm() {
    return trfnm;
  }

  public void setTrfnm(String trfnm) {
    this.trfnm = trfnm;
  }

  public String getDivision() {
    return division;
  }

  public void setDivision(String division) {
    this.division = division;
  }

  public String getWorkflow() {
    return workflow;
  }

  public void setWorkflow(String workflow) {
    this.workflow = workflow;
  }

  public String getPathology() {
    return pathology;
  }

  public void setPathology(String pathology) {
    this.pathology = pathology;
  }

  public String getDvnm() {
    return dvnm;
  }

  public void setDvnm(String dvnm) {
    this.dvnm = dvnm;
  }

  public String getWfnm() {
    return wfnm;
  }

  public void setWfnm(String wfnm) {
    this.wfnm = wfnm;
  }

  public String getStatusnm() {
    return statusnm;
  }

  public void setStatusnm(String statusnm) {
    this.statusnm = statusnm;
  }

  public String getPstype() {
    return pstype;
  }

  public void setPstype(String pstype) {
    this.pstype = pstype;
  }

  public String getClobj() {
    return clobj;
  }

  public void setClobj(String clobj) {
    this.clobj = clobj;
  }

  public String getPstypenm() {
    return pstypenm;
  }

  public void setPstypenm(String pstypenm) {
    this.pstypenm = pstypenm;
  }
  
  public String getRegionnm() {
	return regionnm;
  }

  public void setRegionnm(String regionnm) {
	this.regionnm = regionnm;
  }
  
  public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getZonenm() {
		return zonenm;
	}

	public void setZonenm(String zonenm) {
		this.zonenm = zonenm;
	}

	public String getAdminsts() {
		return adminsts;
	}

	public void setAdminsts(String adminsts) {
		this.adminsts = adminsts;
	}

	public String getAdminstsnm() {
		return adminstsnm;
	}

	public void setAdminstsnm(String adminstsnm) {
		this.adminstsnm = adminstsnm;
	}
	
	public String getAtndtyp() {
		return atndtyp;
	}

	public void setAtndtyp(String atndtyp) {
		this.atndtyp = atndtyp;
	}

	public String getAtndtypnm() {
		return atndtypnm;
	}

	public void setAtndtypnm(String atndtypnm) {
		this.atndtypnm = atndtypnm;
	}

	public String getAtndnm() {
		return atndnm;
	}

	public void setAtndnm(String atndnm) {
		this.atndnm = atndnm;
	}

    public String getWebinarfl() {
		return webinarfl;
	}

	public void setWebinarfl(String webinarfl) {
		this.webinarfl = webinarfl;
	}

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("CASEREQID", "CASEREQID");
    properties.put("SEGID", "SEGID");
    properties.put("ACTID", "ACTID");
    properties.put("SURGID", "SURGID");
    properties.put("FRMDT", "FRMDT");
    properties.put("TODT", "TODT");
    properties.put("STATUS", "STATUS");
    properties.put("TYPE", "TYPE");
    properties.put("CREATEDBY", "CREATEDBY");
    properties.put("CREATEDON", "CREATEDON");
    properties.put("HOTLNM", "HOTLNM");
    properties.put("HOTLPRICE", "HOTLPRICE");
    properties.put("TRVLNM", "TRVLNM");
    properties.put("TRVLPRICE", "TRVLPRICE");
    properties.put("PCPRCRF", "PCPRCRF");
    properties.put("TRFSTATIS", "TRFSTATIS");
    properties.put("LABINV", "LABINV");
    properties.put("LABADD", "LABADD");
    properties.put("PCMNINV", "PCMNINV");
    properties.put("SURVFL", "SURVFL");
    properties.put("CRDFL", "CRDFL");
    properties.put("SURGNM", "SURGNM");
    properties.put("PROCNM", "PROCNM");
    properties.put("CRFNM", "CRFNM");
    properties.put("TRFNM", "TRFNM");
    properties.put("DIVISION", "DIVISION");
    properties.put("WORKFLOW", "WORKFLOW");
    properties.put("PATHOLOGY", "PATHOLOGY");
    properties.put("DVNM", "DVNM");
    properties.put("WFNM", "WFNM");
    properties.put("STATUSNM", "STATUSNM");
    properties.put("PSTYPE", "PSTYPE");
    properties.put("CLOBJ", "CLOBJ");
    properties.put("PSTYPENM", "PSTYPENM");
    properties.put("SURGEONCV", "SURGEONCV");
    properties.put("CREATEDBYID", "CREATEDBYID");
	properties.put("REGION", "REGION");
	properties.put("REGIONNM", "REGIONNM");
	properties.put("ZONE", "ZONE");
	properties.put("ADMINSTS", "ADMINSTS");
	properties.put("ZONENM", "ZONENM");
	properties.put("ADMINSTSNM", "ADMINSTSNM");
	properties.put("ATNDTYP", "ATNDTYP");
  	properties.put("ATNDTYPNM", "ATNDTYPNM");
	properties.put("ATNDNM", "ATNDNM");
	properties.put("WEBINARFL", "WEBINARFL");
	return properties;  
	}

}
