package com.globus.webservice.sales.reports.resources;


import java.util.ArrayList;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmSalesReportUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.sales.beans.GmSalesConsignReportBean;
import com.globus.sales.beans.GmSalesYTDBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmConsignDetailByPartVO;
import com.globus.valueobject.sales.GmConsignDetailVO;
import com.globus.valueobject.sales.GmSalesReportFilterVO;
import com.globus.valueobject.sales.GmSalesReportVO;
import com.globus.webservice.common.resources.GmResource;

@Path("reports")
public class GmFieldInventoryResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchFieldInventoryRpt - This method is used to fetch the field inventory reports and it's
   * common for consignment tab,loaner tab and All tab.
   * 
   * @param strInput
   * @return JSON formatted String
   * @throws AppError
   */
  @POST
  @Path("fetchfieldinventoryrpt")
  @Produces({MediaType.TEXT_PLAIN})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public String fetchFieldInventoryRpt(String strInput) throws AppError {

    log.debug("======fetchFieldInventoryRpt========" + strInput);
    GmSalesReportFilterVO gmSalesReportFilterVO = new GmSalesReportFilterVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesReportFilterVO);

    GmSalesConsignReportBean gmSalesConsignReportBean = new GmSalesConsignReportBean();
    GmSalesYTDBean gmSalesYTDBean = new GmSalesYTDBean();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmSalesReportUtil gmSalesReportUtil = new GmSalesReportUtil();
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParam = new HashMap();
    HashMap hmFinal = new HashMap();
    HashMap hmOutput = new HashMap();

    // Take the User VO.
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesReportFilterVO.getToken());

    // Below is the common method to retrieve the more filters sales params.
    hmParam =
        GmCommonClass.parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO,
            gmSalesReportFilterVO));

    // To get the Access Level information
    hmParam.put("OVERRIDE_ASS_LVL ", hmParam.get("ACCES_LVL"));
    hmParam = GmCommonClass.parseNullHashMap((new GmAccessFilter()).getSalesAccessFilter(hmParam));
    gmSalesReportFilterVO.setAccessfilter((String) hmParam.get("AccessFilter"));
    gmSalesReportFilterVO.setCondition((String) hmParam.get("Condition"));

    String strAction = GmCommonClass.parseNull(gmSalesReportFilterVO.getAction());

    // Below is the common method to retrieve the sales Report Params.
    hmParam =
        GmCommonClass.parseNullHashMap(gmSalesReportUtil.populateSalesReportParam(strAction,
            gmSalesReportFilterVO, null, null));
    hmParam.put("COMPID", gmSalesReportFilterVO.getCompanyid());//Adding this to code for PMT-5662.When the report is loaded from device company id is null.
                                                                //To handle this company id is added into hashmap.
    if (strAction.equals("LoadDCONS")) {
      hmFinal = gmSalesConsignReportBean.reportVSetSummaryByDist(hmParam);
      hmFinal = GmCommonClass.parseNullHashMap((HashMap) hmFinal.get("CROSSVALE"));
    } else if (strAction.equals("LoadGCONS")) {
      hmFinal = gmSalesConsignReportBean.reportVSetSummaryByGroup(hmParam);
      hmFinal = GmCommonClass.parseNullHashMap((HashMap) hmFinal.get("CROSSVALE"));
    } else if (strAction.equals("LoadRep")) {
      hmFinal = gmSalesYTDBean.reportYTDBySalesRep(hmParam);
    } else if (strAction.equals("LoadPart")) {
      hmFinal = gmSalesYTDBean.reportYTDByPart(hmParam);
    } else if (strAction.equals("LoadGroup")) {
      hmFinal = gmSalesYTDBean.reportYTDByGroup(hmParam);
    } else if (strAction.equals("LoadDistributor")) {
      hmFinal = gmSalesYTDBean.reportYTDByDist(hmParam);
    } else if (strAction.equals("LoadAD")) {
      hmFinal = gmSalesConsignReportBean.reportVSetSummaryByAD(hmParam);
      hmFinal = GmCommonClass.parseNullHashMap((HashMap) hmFinal.get("CROSSVALE"));
    } else if (strAction.equals("LoadVP")) {
      hmFinal = gmSalesConsignReportBean.reportVSetSummaryByVP(hmParam);
      hmFinal = GmCommonClass.parseNullHashMap((HashMap) hmFinal.get("CROSSVALE"));
    }

    ArrayList alHeader = GmCommonClass.parseNullArrayList((ArrayList) hmFinal.get("Header"));
    ArrayList alDetails = GmCommonClass.parseNullArrayList((ArrayList) hmFinal.get("Details"));
    HashMap hmFooter = GmCommonClass.parseNullHashMap((HashMap) hmFinal.get("Footer"));

    String strHeader = gmSalesReportUtil.populateStringFromList(alHeader, "\"");

    String strDetails = gmSalesReportUtil.populateStringFromListMap(alDetails);
    ArrayList alDetail = new ArrayList();
    alDetail.add(strDetails);

    String strFooter = gmSalesReportUtil.populateStringFromMap(hmFooter);
    ArrayList alFooter = new ArrayList();
    alFooter.add(strFooter);

    hmOutput.put("header", strHeader);
    hmOutput.put("details", strDetails);
    hmOutput.put("footer", strFooter);

    String strOutput = gmSalesReportUtil.populateJSONOutput(hmOutput);

    return strOutput;
  }


  /**
   * fetchConsignDetails - This method is used to fetch the Consignment Details Report when user
   * click on Consignment tab- C Icon.
   * 
   * @param strInput
   * @return JSON formatted String
   * @throws AppError
   */
  @POST
  @Path("fetchconsigndetails")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesReportVO fetchConsignDetails(String strInput) throws AppError {
    log.debug("======fetchConsignDetails========" + strInput);

    GmConsignDetailVO gmConsignDetailVO = new GmConsignDetailVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmConsignDetailVO);

    GmSalesConsignReportBean gmSalesConsignReportBean = new GmSalesConsignReportBean();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmSalesReportUtil gmSalesReportUtil = new GmSalesReportUtil();
    GmSalesReportVO gmSalesReportVO = new GmSalesReportVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParam = new HashMap();
    ArrayList alList = new ArrayList();
    ArrayList<GmConsignDetailVO> listgmConsignDetailVO = new ArrayList();

    // Take the User VO.
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmConsignDetailVO.getToken());

    // Below is the common method to retrieve the more filters sales params.
    hmParam =
        GmCommonClass.parseNullHashMap(gmProdCatUtil
            .populateSalesParam(gmUserVO, gmConsignDetailVO));

    // To get the Access Level information
    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
    hmParam = GmCommonClass.parseNullHashMap((new GmAccessFilter()).getSalesAccessFilter(hmParam));
    gmConsignDetailVO.setAccessfilter((String) hmParam.get("AccessFilter"));
    gmConsignDetailVO.setCondition((String) hmParam.get("Condition"));

    String strAction = GmCommonClass.parseNull(gmConsignDetailVO.getAction());
    String strStrOpt = GmCommonClass.parseNull(gmConsignDetailVO.getStropt());

    // Below is the common method to retrieve the sales Report Params.
    hmParam =
        GmCommonClass.parseNullHashMap(gmSalesReportUtil.populateSalesReportParam(strAction, null,
            gmConsignDetailVO, null));

    if (strAction.equals("LoadCons")) {
      alList =
          GmCommonClass.convertDynaToArrayList(gmSalesConsignReportBean
              .reportSalesBySummary(hmParam));
    }
    if (strStrOpt.equals("BYSETPART")) {
      hmParam.put("Type", strStrOpt);
      alList =
          GmCommonClass.convertDynaToArrayList(gmSalesConsignReportBean
              .reportSalesBySummary(hmParam));
    }
    gmConsignDetailVO.setAccessfilter("");
    gmConsignDetailVO.setCondition("");

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listgmConsignDetailVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alList, gmConsignDetailVO,
            gmConsignDetailVO.getConsignDetailsProperties());

    gmSalesReportVO.setListGmConsignDetailVO(listgmConsignDetailVO);

    return gmSalesReportVO;
  }

  /**
   * fetchConsignDetailsByPart - This method is used to fetch the Consignment Details Report when
   * user click on Consignment tab- C Icon, form here to see the details by part wise(Search -Icon).
   * 
   * @param strInput
   * @return JSON formatted String
   * @throws AppError
   */
  @POST
  @Path("consigndetailbypart")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesReportVO fetchConsignDetailsByPart(String strInput) throws AppError {
    log.debug("======fetchConsignDetails========" + strInput);

    GmConsignDetailByPartVO gmConsignDetailsByPartVO = new GmConsignDetailByPartVO();
    GmSalesReportVO gmSalesReportVO = new GmSalesReportVO();
    GmConsignDetailVO gmConsignDetailVO = new GmConsignDetailVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmConsignDetailsByPartVO);

    GmSalesConsignReportBean gmSalesConsignReportBean = new GmSalesConsignReportBean();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmSalesReportUtil gmSalesReportUtil = new GmSalesReportUtil();
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParam = new HashMap();
    ArrayList alList = new ArrayList();
    ArrayList<GmConsignDetailByPartVO> listgmConsignDetailByPartVO = new ArrayList();

    // Take the User VO.
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmConsignDetailsByPartVO.getToken());

    // Below is the common method to retrieve the more filters sales params.
    hmParam =
        GmCommonClass.parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO,
            gmConsignDetailsByPartVO));
    // To get the Access Level information
    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
    hmParam = GmCommonClass.parseNullHashMap((new GmAccessFilter()).getSalesAccessFilter(hmParam));
    gmConsignDetailsByPartVO.setAccessfilter((String) hmParam.get("AccessFilter"));
    gmConsignDetailsByPartVO.setCondition((String) hmParam.get("Condition"));

    String strAction = GmCommonClass.parseNull(gmConsignDetailsByPartVO.getAction());


    // Below is the common method to retrieve the sales Report Params.
    hmParam =
        GmCommonClass.parseNullHashMap(gmSalesReportUtil.populateSalesReportParam(strAction, null,
            null, gmConsignDetailsByPartVO));
    if (strAction.equals("BYSETPART")) {

      alList =
          GmCommonClass.convertDynaToArrayList(gmSalesConsignReportBean
              .reportSearchhPartSetDetails(hmParam));
    }
    gmConsignDetailVO.setAccessfilter("");
    gmConsignDetailVO.setCondition("");

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listgmConsignDetailByPartVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alList, gmConsignDetailsByPartVO,
            gmConsignDetailsByPartVO.getConsignDetailsProperties());

    gmSalesReportVO.setListGmConsignDetailByPartVO(listgmConsignDetailByPartVO);

    return gmSalesReportVO;
  }
}
