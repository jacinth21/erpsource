package com.globus.webservice.sales.scorecard.resources;

import java.io.IOException;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.valueobject.sales.scorecard.GmScorecardRptVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.util.GmProdCatUtil;
import com.globus.sales.beans.GmSalesEfficiencyScoreCardByActualProcReportBean;
import com.globus.sales.beans.GmSalesEfficiencyScoreCardByConReportBean;
import com.globus.sales.beans.GmSalesEfficiencyScoreCardByLoanerReportBean;
import com.globus.sales.beans.GmSalesEfficiencyScoreCardRptBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;


/**
 * @author mahendrand
 *
 */
@Path("scorecard")
public class GmSalesEfficiencyScoreCardResource extends GmResource {
	/**
	 * Method to declare the log and used to debug the code
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	/** loadScorecardRptDtls is used to fetch the Scorecard Reports like By AD, By Field Sales, By System
	 * @param strInput
	 * @return
	 * @throws AppError, JsonParseException, JsonMappingException, IOException
	 */
	@POST
	@Path("report")
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String loadScorecardRptDtls(String strInput) throws AppError, JsonParseException,
	      JsonMappingException, IOException {		 
			  String stOpt = ""; 
			  String strReturnJSON = "";
			  GmWSUtil gmWSUtil = new GmWSUtil();
			  ObjectMapper mapper = new ObjectMapper();
			  HashMap hmParam = new HashMap();
			  HashMap hmInput = new HashMap();
			  GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
			  GmScorecardRptVO gmScorecardRptVO = mapper.readValue(strInput, GmScorecardRptVO.class);
			  
			  validateToken(gmScorecardRptVO.getToken());
			  GmAccessFilter gmAccessFilter = new GmAccessFilter();
			  GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
			   
			  gmDataStoreVO.setUserid(gmUserVO.getUserid());
			  gmDataStoreVO.setCmpdfmt(gmScorecardRptVO.getCmpdfmt());
			  GmSalesEfficiencyScoreCardRptBean gmSalesEfficiencyScoreCardRptBean = new GmSalesEfficiencyScoreCardRptBean(gmDataStoreVO);
			  GmSalesEfficiencyScoreCardByActualProcReportBean gmSalesEfficiencyScoreCardByActualProcReportBean = new GmSalesEfficiencyScoreCardByActualProcReportBean(gmDataStoreVO);
			  GmSalesEfficiencyScoreCardByConReportBean gmSalesEfficiencyScoreCardByConReportBean = new GmSalesEfficiencyScoreCardByConReportBean(gmDataStoreVO);
			  GmSalesEfficiencyScoreCardByLoanerReportBean gmSalesEfficiencyScoreCardByLoanerReportBean = new GmSalesEfficiencyScoreCardByLoanerReportBean(gmDataStoreVO);
			  
			  gmWSUtil.parseJsonToVO(strInput, gmScorecardRptVO);
			  hmInput = gmWSUtil.getHashMapFromVO(gmScorecardRptVO); 
			    
			  // For Override the Access Level of Associate Sales REP to Sales Rep (Associate Sales REP
			  // checks with account id)
			  hmParam =GmCommonClass.parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO,
			              (new GmSalesDashReqVO())));
			  stOpt = gmScorecardRptVO.getStropt();
			  /*
			   * When Associate Rep login. His user id passed as reporting repid. 
			   */
			  hmParam.put("RPT_REPID", gmUserVO.getUserid());
			  hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
			  hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));

			  hmInput.put("USERDEPTID", gmUserVO.getDeptid());
			  hmInput.put("STROPT", stOpt);
			  hmInput.put("HMPARAM",GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam)));
			  
			  hmInput = (HashMap) hmInput.get("HMPARAM");
			  hmInput.put("APPLNDATEFMT", gmScorecardRptVO.getCmpdfmt());
			  hmInput.put("SYSTEMID", gmScorecardRptVO.getSystemid());
			  hmInput.put("DISTRIBUTORID", gmScorecardRptVO.getDistributorid());
			  hmInput.put("ADID", gmScorecardRptVO.getAdid());
			  hmInput.put("ALLOWEDTAG", gmScorecardRptVO.getAllowedtag());
			  log.debug("loadScorecardRptDtls hmInput>>> " + hmInput);
			  // Method for fetch the data
			  if( stOpt.equals("ad") ){			   
				strReturnJSON = gmSalesEfficiencyScoreCardRptBean.fetchADScoreCardDtls(hmInput);
			  }else if( stOpt.equals("system") ){
				strReturnJSON = gmSalesEfficiencyScoreCardRptBean.fetchSystemScoreCardDtls(hmInput);
			  }
		      else if( stOpt.equals("fieldsales") ) {
				strReturnJSON = gmSalesEfficiencyScoreCardRptBean.fetchFieldSalesScoreCardDtls(hmInput);
			  } 
		      else if( stOpt.equals("actualproc") ) {
				strReturnJSON = gmSalesEfficiencyScoreCardByActualProcReportBean.fetchActualProcUsageScoreCardDtls(hmInput);
			  }
		      else if( stOpt.equals("consignment") ) {
				strReturnJSON = gmSalesEfficiencyScoreCardByConReportBean.fetchConUsageScoreCardDtls(hmInput);
			  }
		      else if( stOpt.equals("loaner") ) {
				strReturnJSON = gmSalesEfficiencyScoreCardByLoanerReportBean.fetchLoanerUsageScoreCardDtls(hmInput);
			  }
		      else if( stOpt.equals("borrowed") ) {
		    	  strReturnJSON = gmSalesEfficiencyScoreCardByActualProcReportBean.fetchBorrowTagScoreCardDtls(hmInput);
			  }
		      // PC-3677 Scorecard - Bulk DO
		      else if( stOpt.equals("bulk") ) {
		    	  strReturnJSON = gmSalesEfficiencyScoreCardByActualProcReportBean.fetchBulkDOScoreCardDtls(hmInput);
		    	  }
		      else if( stOpt.equals("overallsummary") ) {
		    	  strReturnJSON = gmSalesEfficiencyScoreCardRptBean.fetchOverallSummaryDtls(hmInput);
			  }
			  return strReturnJSON;
				  
	  }   
}

