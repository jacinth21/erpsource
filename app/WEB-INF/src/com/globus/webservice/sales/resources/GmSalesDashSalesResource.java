package com.globus.webservice.sales.resources;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.logon.beans.GmLogonBean;
import com.globus.sales.DashBoard.actions.GmSalesDashLoanerAction;
import com.globus.sales.DashBoard.beans.GmSalesDashBoardBean;
import com.globus.sales.beans.GmSalesReportBean;
import com.globus.sales.beans.GmTerritoryReportBean;
import com.globus.valueobject.common.GmCodeLookupVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.valueobject.sales.GmSalesDashResVO;
import com.globus.valueobject.sales.GmSalesDrilldownVO;
import com.globus.webservice.common.resources.GmResource;

/**
 * This class contains methods that are related to MSD - Today, MTD sales overview, Drilldown
 * details.
 * 
 * @author elango *
 */
@Path("sales")
public class GmSalesDashSalesResource extends GmResource {


  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchTodaySales - This method will fetch Today Sales details
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("todayssales")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchTodaySales(String strInput) throws AppError {

    log.debug("=======MSD - fetchTodaySales Count======" + strInput);
    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmSalesDashBoardBean gmSalesDashBoardBean = new GmSalesDashBoardBean();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmSalesDashLoanerAction gmSalesDashLoanerAction = new GmSalesDashLoanerAction();
    GmSalesReportBean gmSalesReportBean = new GmSalesReportBean(gmSalesDashReqVO);
    GmCalenderOperations gmCal = new GmCalenderOperations();
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    String strUserAccessType = "";
    String strTodaySalesType = "";
    String strTodaysDate = "";
    ArrayList alReturn = new ArrayList();
    ArrayList alUserTypeList = new ArrayList();
    HashMap hmTodaySales = new HashMap();
    List<GmSalesDashResVO> listGmSalesDashResVO = null;
    List<GmCodeLookupVO> listGmCodeLookupVO = null;

    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    String strApplCurrFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CURRFMT", "CURRFMT"));
    String strApplCurrSign =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CURRSYMBOL", "CURRSYMBOL"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // If the company id is not passing from input, we have to pass default COMP,DIV ID's based on
    // logged in User ID.
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
    hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));

    //seting company timezone
    GmCalenderOperations.setTimeZone(gmSalesDashReqVO.getCmptzone());
    // Get Todays Date
    strTodaysDate = gmCal.getCurrentDate(gmSalesDashReqVO.getCmpdfmt());
    // To get the Access Level filter information
    // hmReturn = gmAccessFilter.getSalesAccessFilter(hmParam);
    // For Today Sales, need t501 sales filter condition.
    // hmParam.put("Condition", GmCommonClass.parseNull((String) hmReturn.get("Condition")));

    // To get the Access Level filter information
    String strQuery = "";
    strQuery = GmCommonClass.parseNull((String) (new GmAction()).getSalesAccessCondition(hmParam));
    hmParam.put("STRCONDITION", strQuery);
    strQuery = (new GmAccessFilter()).getHierarchyWhereConditionT501(hmParam).toString();
    hmParam.put("Condition", (String) strQuery);

    // Taking the default FilterBy Value.
    strUserAccessType = gmSalesDashBoardBean.fetchUserType(hmParam);

    // Based on default FilterBy Value Populate the user access based Drop down options list.
    ArrayList alTypeList = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("TDSTYP"));
    alUserTypeList =
        GmCommonClass.parseNullArrayList((ArrayList) gmSalesDashLoanerAction.populateUserTypeList(
            strUserAccessType, alTypeList));
    listGmCodeLookupVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alUserTypeList, new GmCodeLookupVO(),
            (new GmCodeLookupVO()).getCodeListMappingProps());
    gmSalesDashResVO.setViewByList(listGmCodeLookupVO);

    // If filterBy value is not passed from front end then use default value.
    strTodaySalesType = GmCommonClass.parseNull((String) gmSalesDashReqVO.getFilterby());
    strUserAccessType =
        (strTodaySalesType.equals("") || strTodaySalesType.equals("0")) ? strUserAccessType
            : strTodaySalesType;
    hmParam.put("USERTYPE", strUserAccessType);
    hmParam.put("TDATE", strTodaysDate);
    hmParam.put("FORMAT", gmSalesDashReqVO.getCmpdfmt());

    hmTodaySales = gmSalesReportBean.todaysSales(hmParam); // Todays and Month to Date sales figures
    gmSalesDashResVO.setValue(GmCommonClass.parseZero((String) hmTodaySales.get("DAYSALES")));
    gmSalesDashResVO.setFilterby((String) strUserAccessType);

    alReturn =
        GmCommonClass.parseNullArrayList((ArrayList) gmSalesDashBoardBean
            .todaysSalesReport(hmParam));
    alReturn = generateSalesPerList(alReturn);
    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmSalesDashResVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alReturn, gmSalesDashResVO,
            gmSalesDashResVO.getSalesDashTodaySalesProperties());

    gmSalesDashResVO.setListGmSalesDashResListVO(listGmSalesDashResVO);

    gmSalesDashResVO.setCurrency(strApplCurrSign);
    gmSalesDashResVO.setDatefmt(strApplDateFmt);
    gmSalesDashResVO.setDay(strTodaysDate);

    alReturn = null;
    alUserTypeList = null;
    hmTodaySales = null;
    listGmSalesDashResVO = null;
    listGmCodeLookupVO = null;
    hmParam = null;
    hmReturn = null;

    return gmSalesDashResVO;
  }

  /**
   * generateSalesQuotaList - Used to Calculate the Today sales %.
   * 
   * @param altodaysSales - Contains all the Today sales item info's.
   * @return ArrayList
   * @throws AppError
   */
  private ArrayList generateSalesPerList(ArrayList altodaysSales) throws AppError {

    ArrayList alReturn = new ArrayList();
    HashMap hmTemp = null;

    double totalSales = 0.0;
    double totalItemSale = 0.0;
    double salesPercent = 0.0;

    // Caluclate the total sales value.
    for (int i = 0; i < altodaysSales.size(); i++) {
      hmTemp = GmCommonClass.parseNullHashMap((HashMap) altodaysSales.get(i));
      totalSales = totalSales + Math.abs(Double.parseDouble((String) hmTemp.get("SALES")));
    }
    // Handling the Divide by zero exception.
    totalSales = (totalSales == 0) ? 1 : totalSales;

    // Calculating the percentage value.
    for (int i = 0; i < altodaysSales.size(); i++) {
      hmTemp = GmCommonClass.parseNullHashMap((HashMap) altodaysSales.get(i));
      totalItemSale = Double.parseDouble((String) hmTemp.get("SALES"));
      salesPercent = (totalItemSale * 100) / totalSales;
      hmTemp.put("PERCENTAGE", String.valueOf((double) GmCommonClass.roundDigit(salesPercent, 2)));
      alReturn.add(hmTemp);
    }
    log.debug(alReturn);
    return alReturn;
  }

  /**
   * fetchTodaySalesDetail - This method will fetch daily Sales drilldown details based on the date
   * given
   * 
   * @param strInput - input values : stropt-detail, view-MGT/VP/AD/DIST/REP,
   *        filterby-VP/AD/FS/REP/ACC
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("todayssalesdetail")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchTodaySalesDetail(String strInput) throws AppError {

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmLogonBean gmLogonBean = new GmLogonBean();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesReportBean gmSalesReportBean = new GmSalesReportBean(gmSalesDashReqVO);
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmSalesDashBoardBean gmSalesDashBoardBean = new GmSalesDashBoardBean();
    GmCalenderOperations gmCal = new GmCalenderOperations();
    ArrayList alDailySales, listDailySalesDrilldownVO = null;
    List<GmSalesDrilldownVO> gmSalesDrilldownVOList = new ArrayList<GmSalesDrilldownVO>();
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmValidate = new HashMap();
    String strTodaySalesType = "";
    String strUserAccessType = "";
    String strOrderTotal = "";
    int alSize = 0;

    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    String strApplCurrSign =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CURRSYMBOL", "CURRSYMBOL"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass.parseNullHashMap((HashMap) gmProdCatUtil.populateSalesParam(gmUserVO,
            gmSalesDashReqVO));
	//seting company timezone
    GmCalenderOperations.setTimeZone(gmSalesDashReqVO.getCmptzone());
    // Get Todays Date
    String strTodaysDate = gmCal.getCurrentDate(gmSalesDashReqVO.getCmpdfmt());
    
    // Get strOpt value from input. This should hold "detail" and it should not be empty.
    String strStrOpt = GmCommonClass.parseNull((String) gmSalesDashReqVO.getStropt());
    hmValidate.put("stropt", "20632");
    // Get view detail ID value from input. This should hold "VP/AD/DIST/REP/ACC" id values, for
    // which the drilldown details should be populated.
    String strViewDetailID = GmCommonClass.parseNull((String) gmSalesDashReqVO.getView());
    hmValidate.put("view", "20633");
    // Get Filterby type value from input. This should hold "VP/AD/DIST/REP/ACC" id values.
    strTodaySalesType = GmCommonClass.parseNull((String) gmSalesDashReqVO.getFilterby());
    hmValidate.put("filterby", "20634");

    // validate strOpt,view and Filterby
    gmProdCatUtil.validateInput(gmSalesDashReqVO, hmValidate);

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));
    String strAccessLvlID = (String) hmParam.get("ACCES_LVL");

    hmParam.put("OVERRIDE_ASS_LVL", (String) hmParam.get("ACCES_LVL"));
    hmParam.put("ACCS_LVL", (String) hmParam.get("ACCES_LVL"));
    // To get the Access Level information
    // hmReturn = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    // hmParam.put("Condition", GmCommonClass.parseNull((String) hmReturn.get("Condition")));

    // To get the Access Level filter information
    String strQuery = "";
    strQuery = GmCommonClass.parseNull((String) (new GmAction()).getSalesAccessCondition(hmParam));
    hmParam.put("STRCONDITION", strQuery);
    strQuery = (new GmAccessFilter()).getHierarchyWhereConditionT501(hmParam).toString();
    hmParam.put("Condition", (String) strQuery);

    // Taking the default FilterBy Value for the put token user ID.
    strUserAccessType = gmSalesDashBoardBean.fetchUserType(hmParam);
    // If filterBy value is not passed from front end, use default value.
    strUserAccessType =
        (strTodaySalesType.equals("") || strTodaySalesType.equals("0")) ? strUserAccessType
            : strTodaySalesType;
    hmParam.put("USERTYPE", strUserAccessType);
    hmParam.put("FORMAT", gmSalesDashReqVO.getCmpdfmt());
    hmParam.put("STROPT", strStrOpt);
    hmParam.put("TDATE", strTodaysDate);
    hmParam.put("DETAILID", strViewDetailID);

    alDailySales =
        GmCommonClass.parseNullArrayList((ArrayList) gmSalesDashBoardBean
            .todaysSalesReport(hmParam));
    alSize = alDailySales.size();
    // This above query will contains the order total and it will be available in all the rows.
    // so taking order total value by default from the first row.
    if (alSize > 0) {
      hmParam = GmCommonClass.parseNullHashMap((HashMap) alDailySales.get(0));
      strOrderTotal = GmCommonClass.parseNull((String) hmParam.get("ORDER_TOTAL"));
    }

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listDailySalesDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alDailySales, (new GmSalesDrilldownVO()),
            new GmSalesDrilldownVO().getTodaySalesDetailsDrilldownProperties());

    gmSalesDashResVO.setListGmSalesDrilldownVO(listDailySalesDrilldownVO);
    gmSalesDashResVO.setValue(strOrderTotal);
    gmSalesDashResVO.setCurrency(strApplCurrSign);
    gmSalesDashResVO.setDatefmt(strApplDateFmt);
    gmSalesDashResVO.setDay(strTodaysDate);

    alDailySales = null;
    listDailySalesDrilldownVO = null;
    hmParam = null;
    hmReturn = null;
    hmValidate = null;

    return gmSalesDashResVO;
  }

  /**
   * fetchMTDSales - This method will fetch MTD Sales details
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("mtdsales")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchMTDSales(String strInput) throws AppError {

    log.debug("=======MSD - fetchMTDSales======" + strInput);
    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmSalesDashBoardBean gmSalesDashBoardBean = new GmSalesDashBoardBean();
    GmSalesReportBean gmSalesReportBean = new GmSalesReportBean(gmSalesDashReqVO);
    GmTerritoryReportBean gmTerritoryReportBean = new GmTerritoryReportBean();
    GmCalenderOperations gmCal = new GmCalenderOperations();
    HashMap hmParam = new HashMap();
    HashMap hmQuota = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();
    ArrayList alDetails = new ArrayList();
    HashMap hmTodaySales = new HashMap();
    List<GmSalesDashResVO> listGmSalesDashResVO = null;
    String strTodaysDate = "";
    double dbMonthSales = 0;
    double dbAvgSales = 0;
    int intWorkDays = 0;

    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    String strApplCurrFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CURRFMT", "CURRFMT"));
    String strApplCurrSign =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CURRSYMBOL", "CURRSYMBOL"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass.parseNullHashMap((HashMap) gmProdCatUtil.populateSalesParam(gmUserVO,
            gmSalesDashReqVO));

    // If the company id is not passing from input, we have to pass default COMP,DIV ID's based on
    // logged in User ID.
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));
    hmParam.put("OVERRIDE_ASS_LVL", (String) hmParam.get("ACCES_LVL"));
    hmParam.put("ACCS_LVL", (String) hmParam.get("ACCES_LVL"));
	
	//seting company timezone
    GmCalenderOperations.setTimeZone(gmSalesDashReqVO.getCmptzone());

    // Get Todays Date
    strTodaysDate = gmCal.getCurrentDate(gmSalesDashReqVO.getCmpdfmt());

    // To get the Access Level filter information
    hmReturn = gmAccessFilter.getSalesAccessFilter(hmParam);

    // For Today Sales, need t501 sales filter condition.
    // hmParam.put("Condition", GmCommonClass.parseNull((String) hmReturn.get("Condition")));

    // To get the Access Level filter information
    String strQuery = "";
    strQuery = GmCommonClass.parseNull((String) (new GmAction()).getSalesAccessCondition(hmParam));
    hmParam.put("STRCONDITION", strQuery);
    strQuery = (new GmAccessFilter()).getHierarchyWhereConditionT501(hmParam).toString();
    hmParam.put("Condition", (String) strQuery);

    hmParam.put("TDATE", strTodaysDate);
    hmParam.put("FORMAT", gmSalesDashReqVO.getCmpdfmt());

    hmTodaySales = gmSalesReportBean.todaysSales(hmParam); // Todays and Month to Date sales figures

    /* below code for get average sales for current month working days */
    dbMonthSales =
        Double.parseDouble(GmCommonClass.parseZero((String) hmTodaySales.get("MONTHSALES")));
    intWorkDays = gmSalesReportBean.fetchWorkingDays("");
    dbAvgSales = (intWorkDays == 0) ? 0 : (dbMonthSales / intWorkDays);
    gmSalesDashResVO.setAds(String.valueOf((double) GmCommonClass.roundDigit(dbAvgSales, 2)));
    gmSalesDashResVO.setValue(GmCommonClass.parseZero((String) hmTodaySales.get("MONTHSALES")));

    alReturn =
        GmCommonClass.parseNullArrayList((ArrayList) gmSalesReportBean.dailySalesReport(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmSalesDashResVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alReturn, gmSalesDashResVO,
            gmSalesDashResVO.getSalesDashMTDSalesProperties());

    gmSalesDashResVO.setListGmSalesDashResListVO(listGmSalesDashResVO);

    // To calculate the Quota.
    // For Quota need v700 condition.
    hmParam.put("Condition", GmCommonClass.parseNull((String) hmReturn.get("AccessFilter")));
    String strFilter = GmCommonClass.parseNull((String) (new GmServlet()).getSalesFilter(hmParam));
    hmParam.put("Filter", strFilter);
    String strType = GmCommonClass.parseNull((String) gmSalesDashBoardBean.fetchTypeQuota(hmParam));
    hmParam.put("TYPE", strType);
    hmParam.put("Divider", "1");
    hmParam.put("strApplCurrSign", strApplCurrSign);
    hmParam.put("strApplCurrFmt", strApplCurrFmt);
    hmParam.put("COMPID", (String) hmParam.get("COMP_FILTER"));
    hmQuota = gmTerritoryReportBean.fetchTerritoryPerformanceReport(hmParam);
    alDetails = (ArrayList) hmQuota.get("Details");
    hmQuota = gmSalesDashBoardBean.getQuotaHmapDetails(alDetails, hmParam);

    // Calculate TargetADS
    String strTgtAvgSales = calculateTargetADS(hmQuota);
    gmSalesDashResVO.setTgtads(strTgtAvgSales);

    gmSalesDashResVO.setCurrency(strApplCurrSign);
    gmSalesDashResVO.setDatefmt(strApplDateFmt);
    gmSalesDashResVO.setDay(strTodaysDate);

    hmParam = null;
    hmQuota = null;
    hmReturn = null;
    alReturn = null;
    alDetails = null;
    hmTodaySales = null;
    listGmSalesDashResVO = null;

    return gmSalesDashResVO;
  }

  /**
   * calculateTargetADS - Used to Calculate the calculate Target ADS.
   * 
   * @param altodaysSales - Contains all the Today sales item info's.
   * @return String
   * @throws AppError This method is changed to public since this is used from
   *         GmSaleDashBoardServlet.java.
   */
  public String calculateTargetADS(HashMap hmtodaysSales) throws AppError {

    ArrayList alReturn = new ArrayList();
    GmSalesReportBean gmSalesReportBean = new GmSalesReportBean();
    GmCommonBean gmCommonBean = new GmCommonBean();
    HashMap hmTemp = null;
    String strTgtAvgSales = "";
    String strTodaysDate = "";

    double dbSalesDiff = 0;
    double dbTgtAvgSales = 0;
    int intWorkDays = 0;

    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    String strApplCurrFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CURRFMT", "CURRFMT"));
    // Get Todays Date
    strTodaysDate = GmCommonClass.getStringFromDate(new java.util.Date(), strApplDateFmt);
    // This method will return the number of working days between two given dates(remaining).
    intWorkDays = gmSalesReportBean.fetchWorkingDaysByRange(strTodaysDate, strTodaysDate);

    double salesMTD = GmCommonClass.parseDouble((Double) hmtodaysSales.get("MTD"));
    double salesYTD = GmCommonClass.parseDouble((Double) hmtodaysSales.get("YTD"));
    double quotaQMTD = GmCommonClass.parseDouble((Double) hmtodaysSales.get("QMTD"));
    double quotaQYTD = GmCommonClass.parseDouble((Double) hmtodaysSales.get("QYTD"));
    String strCalDailyQuota = GmCommonClass.parseNull((String) hmtodaysSales.get("DAILYQUOTA"));

    if (strCalDailyQuota.equals("Y")) {
      // To calculate daily Quota,
      // 1. No need to consider the current month sales value.
      salesMTD = 0.00;
      // 2. The Working days should be considered for the whole month not on the remianing working
      // days.
      intWorkDays = gmSalesReportBean.fetchTotalBusinessDaysByMonth("");
    }
    dbSalesDiff = (quotaQMTD - salesMTD) * 1000;

    if (dbSalesDiff <= 0) { // If the value is -ve, [Sales is more than Quota] set the difference to
                            // 0.
      dbTgtAvgSales = 0;
    } else {
      dbTgtAvgSales = (intWorkDays == 0) ? 0 : (dbSalesDiff / intWorkDays);
      dbTgtAvgSales = GmCommonClass.roundDigit(dbTgtAvgSales, 2);

    }
    strTgtAvgSales = (new DecimalFormat("######0.00").format(dbTgtAvgSales));
    return strTgtAvgSales;
  }

  /**
   * fetchMTDSalesDetail - This method will fetch daily Sales details based on the date given
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("dailysalesdetail")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchMTDSalesDetail(String strInput) throws AppError {

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmSalesReportBean gmSalesReportBean = new GmSalesReportBean(gmSalesDashReqVO);
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmValidate = new HashMap();
    ArrayList alDailySales = null;
    List<GmSalesDrilldownVO> listDailySalesDrilldownVO = null;

    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    String strApplCurrFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CURRFMT", "CURRFMT"));
    String strApplCurrSign =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CURRSYMBOL", "CURRSYMBOL"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass.parseNullHashMap((HashMap) gmProdCatUtil.populateSalesParam(gmUserVO,
            gmSalesDashReqVO));

    String strFilterby = GmCommonClass.parseNull((String) gmSalesDashReqVO.getFilterby());
    hmValidate.put("filterby", "20630");
    // validate strOpt,view and Filterby
    gmProdCatUtil.validateInput(gmSalesDashReqVO, hmValidate);

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));
    String strAccessLvlID = (String) hmParam.get("ACCES_LVL");

    hmParam.put("OVERRIDE_ASS_LVL", (String) hmParam.get("ACCES_LVL"));
    hmParam.put("ACCS_LVL", (String) hmParam.get("ACCES_LVL"));
    // To get the Access Level information
    // hmReturn = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    // hmReturn.put("COND", (String) hmReturn.get("Condition"));

    // To get the Access Level filter information
    String strQuery = "";
    strQuery = GmCommonClass.parseNull((String) (new GmAction()).getSalesAccessCondition(hmParam));
    hmParam.put("STRCONDITION", strQuery);
    strQuery = (new GmAccessFilter()).getHierarchyWhereConditionT501(hmParam).toString();
    hmParam.put("COND", (String) strQuery);

    hmParam.put("FORMAT", gmSalesDashReqVO.getCmpdfmt());
    hmParam.put("TDATE", strFilterby);

    alDailySales = GmCommonClass.parseNullArrayList(gmSalesReportBean.todaysSalesReport(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listDailySalesDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alDailySales, (new GmSalesDrilldownVO()),
            new GmSalesDrilldownVO().getDailySalesDetailDrilldownProperties());

    gmSalesDashResVO.setListGmSalesDrilldownVO(listDailySalesDrilldownVO);
    gmSalesDashResVO.setCurrency(strApplCurrSign);
    gmSalesDashResVO.setDatefmt(strApplDateFmt);

    hmParam = null;
    hmReturn = null;
    alDailySales = null;
    listDailySalesDrilldownVO = null;

    return gmSalesDashResVO;
  }
}
