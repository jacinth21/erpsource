package com.globus.webservice.sales.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.sales.DashBoard.beans.GmSalesDashCongItemsBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmConsignmentDrilldownVO;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.valueobject.sales.GmSalesDashResVO;
import com.globus.webservice.common.resources.GmResource;

/**
 * This class contains methods that are related to MSD - Consign items overview, Drill down
 * sections.
 * 
 * @author elango
 * 
 */
@Path("consignitem")
public class GmSalesDashConsignItemResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * congItemsCount - This method will return the Sales Dash board Consignment Item count for
   * different levels.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("count")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO congItemsCount(String strInput) throws AppError {

    log.debug("=======congItems Count======" + strInput);
    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashCongItemsBean gmSalesDashCongItemsBean = new GmSalesDashCongItemsBean();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmParam = new HashMap();
    HashMap hmTemp = new HashMap();
    HashMap hmApprBoCount = new HashMap();
    ArrayList alApprBoCnt = new ArrayList();
    String strRuleDrillDownPath = "";
    String strAccItemConRptFl = "";
    ArrayList alReturn = new ArrayList();
    ArrayList alApprovedCongItems = new ArrayList();
    ArrayList alBackOrderCongItems = new ArrayList();
    ArrayList alShippedCongItems = new ArrayList();
    ArrayList alRequestedCongItems = new ArrayList();
    List<GmSalesDashResVO> listGmSalesDashResVO = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));
    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));

    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));

    hmParam.put("TOTALS", "Y");
    hmParam.put("APPLDATEFMT", strApplDateFmt);
    //To show account item consignment reports based on the below rule(PMT-29390)
    strAccItemConRptFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SAL_DASH_ACC_ITM",
            "SALEDASHTAB", strCompanyID));
    hmParam.put("ACCITEMCONRPTFL",strAccItemConRptFl);
    alRequestedCongItems =
        GmCommonClass.parseNullArrayList(gmSalesDashCongItemsBean.fetchApprovedCongItems(hmParam));
    strRuleDrillDownPath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CON_ITEM", "REQUESTED"));
    (GmCommonClass.parseNullHashMap((HashMap) alRequestedCongItems.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);
    if(strAccItemConRptFl.equals("Y")){
    //to fetch the count of both Approved and BO  in single method call(query)
      hmParam.put("PASSNAME", "Approved_BO");  
       alApprBoCnt=
          GmCommonClass.parseNullArrayList(gmSalesDashCongItemsBean.fetchCongItemsCount(hmParam));
      
      int alsize = alApprBoCnt.size();
      if (alsize < 4) {
        for (int i = 0; i < alsize; i++) {
          hmTemp = (HashMap) alApprBoCnt.get(i);
          hmApprBoCount.put(hmTemp.get("NAME"), hmTemp.get("TOTAL"));
          if(hmTemp.get("NAME").equals("Back Order")){
            //if it is matched , then setting into the corresponding arraylist
            alBackOrderCongItems.add(hmTemp);
            //setting drill down URL as per the existing flow
            strRuleDrillDownPath =
                GmCommonClass.parseNull(GmCommonClass.getRuleValue("CON_ITEM", "BACKORDER"));
          }else if(hmTemp.get("NAME").equals("Approved")){
            alApprovedCongItems.add(hmTemp);
            strRuleDrillDownPath =
                GmCommonClass.parseNull(GmCommonClass.getRuleValue("CON_ITEM", "APPROVED"));
          }
          (GmCommonClass.parseNullHashMap((HashMap) alApprBoCnt.get(i))).put("DRILLDOWNURL",
              strRuleDrillDownPath);
        }
        //Adding Back Order value as 0
        if (hmApprBoCount.get("Back Order") == null) {
          hmTemp = new HashMap();
          hmTemp.put("NAME", "Back Order");
          hmTemp.put("TOTAL", 0);
          alBackOrderCongItems.add(hmTemp);
        }
        //Adding Approved value as 0
        if (hmApprBoCount.get("Approved") == null) {
          hmTemp = new HashMap();
          hmTemp.put("NAME", "Approved");
          hmTemp.put("TOTAL", 0);
          alApprovedCongItems.add(hmTemp);
        }
      }
     
    }else{

      hmParam.put("PASSNAME", "Approved");
      alApprovedCongItems =
          GmCommonClass.parseNullArrayList(gmSalesDashCongItemsBean.fetchCongItemsCount(hmParam));
      strRuleDrillDownPath =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue("CON_ITEM", "APPROVED"));
      (GmCommonClass.parseNullHashMap((HashMap) alApprovedCongItems.get(0))).put("DRILLDOWNURL",
          strRuleDrillDownPath);
      hmParam.put("PASSNAME", "BO");
      alBackOrderCongItems =
          GmCommonClass.parseNullArrayList(gmSalesDashCongItemsBean.fetchCongItemsCount(hmParam));
      strRuleDrillDownPath =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue("CON_ITEM", "BACKORDER"));
      (GmCommonClass.parseNullHashMap((HashMap) alBackOrderCongItems.get(0))).put("DRILLDOWNURL",
          strRuleDrillDownPath);
    }
    hmParam.put("PASSNAME", "Shipped");
    alShippedCongItems =
        GmCommonClass.parseNullArrayList(gmSalesDashCongItemsBean.fetchCongItemsCount(hmParam));
    strRuleDrillDownPath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CON_ITEM", "SHIPPED"));
    (GmCommonClass.parseNullHashMap((HashMap) alShippedCongItems.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);



    alReturn.addAll(alRequestedCongItems);
    alReturn.addAll(alApprovedCongItems);
    alReturn.addAll(alShippedCongItems);
    alReturn.addAll(alBackOrderCongItems);

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmSalesDashResVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, (new GmSalesDashResVO()),
            new GmSalesDashResVO().getSalesDashBoardProperties());

    gmSalesDashResVO.setListGmSalesDashResListVO(listGmSalesDashResVO);

    hmParam = null;
    alReturn = null;
    alApprovedCongItems = null;
    alBackOrderCongItems = null;
    alShippedCongItems = null;
    listGmSalesDashResVO = null;

    return gmSalesDashResVO;
  }

  /**
   * fetchRequestedConsignmentItem - This method will returns Requested Consignment Items.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("requesteditem")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchRequestedConsignmentItem(String strInput) throws AppError {

    log.debug("=======fetch consgnitems approved======");

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashCongItemsBean gmsalesdashcongitemsbean = new GmSalesDashCongItemsBean(gmSalesDashReqVO);
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmParam = new HashMap();
    ArrayList alConsignItemsApproved, listGmConsignSetsDrilldownVO = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
    String strAccItemConRptFl = "";

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));
    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));

    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("PASSNAME", "Requested");
    //To show account item consignment reports based on the below rule(PMT-29390)
    strAccItemConRptFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SAL_DASH_ACC_ITM",
            "SALEDASHTAB", strCompanyID));
    hmParam.put("ACCITEMCONRPTFL",strAccItemConRptFl);
    alConsignItemsApproved =
        GmCommonClass.parseNullArrayList(gmsalesdashcongitemsbean.fetchApprovedCongItems(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmConsignSetsDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alConsignItemsApproved,
            (new GmConsignmentDrilldownVO()),
            new GmConsignmentDrilldownVO().getConsignItemsReqDrilldownProperties());

    gmSalesDashResVO.setListGmConsignmentDrilldownVO(listGmConsignSetsDrilldownVO);

    hmParam = null;
    alConsignItemsApproved = null;
    listGmConsignSetsDrilldownVO = null;

    return gmSalesDashResVO;
  }

  /**
   * fetchApprovedConsignmentSets - This method will returns Approved Consignment Items.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("approveditem")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchApprovedCongItems(String strInput) throws AppError {

    log.debug("=======fetch consgnitems approved======");

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    log.debug("strInput========================"+strInput);
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashCongItemsBean gmsalesdashcongitemsbean = new GmSalesDashCongItemsBean(gmSalesDashReqVO);
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmParam = new HashMap();
    ArrayList alConsignItemsApproved, listGmConsignSetsDrilldownVO = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
    String strAccItemConRptFl = "";
    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));
    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));

    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    hmParam.put("APPLDATEFMT", strApplDateFmt);
    // hmParam.put("CONGSETTYPE", "Approved");
//To show account item consignment reports based on the below rule(PMT-29390)
    strAccItemConRptFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SAL_DASH_ACC_ITM",
            "SALEDASHTAB", strCompanyID));
    hmParam.put("ACCITEMCONRPTFL",strAccItemConRptFl);
    hmParam.put("PASSNAME", "Approved");
    alConsignItemsApproved =
        GmCommonClass.parseNullArrayList(gmsalesdashcongitemsbean.fetchApprCongItems(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmConsignSetsDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alConsignItemsApproved,
            (new GmConsignmentDrilldownVO()),
            new GmConsignmentDrilldownVO().getConsignItemsDrilldownProperties());

    gmSalesDashResVO.setListGmConsignmentDrilldownVO(listGmConsignSetsDrilldownVO);

    hmParam = null;
    alConsignItemsApproved = null;
    listGmConsignSetsDrilldownVO = null;

    return gmSalesDashResVO;
  }

  /**
   * fetchShippedConsignmentSets - This method will returns Shipped Consignment Items.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("shippeditem")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchShippedCongItems(String strInput) throws AppError {

    log.debug("=======fetch Shipped drillDown======");

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashCongItemsBean gmsalesdashcongitemsbean = new GmSalesDashCongItemsBean(gmSalesDashReqVO);
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmParam = new HashMap();
    ArrayList alConsignItemsShipped, listGmConsignSetsDrilldownVO = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
    String strAccItemConRptFl = "";
    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));
    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    hmParam.put("APPLDATEFMT", strApplDateFmt);
//To show account item consignment reports based on the below rule(PMT-29390)
    strAccItemConRptFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SAL_DASH_ACC_ITM",
            "SALEDASHTAB", strCompanyID));
    hmParam.put("ACCITEMCONRPTFL",strAccItemConRptFl);
    hmParam.put("PASSNAME", "Shipped");
    alConsignItemsShipped =
        GmCommonClass.parseNullArrayList(gmsalesdashcongitemsbean.fetchShippedCongItems(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmConsignSetsDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alConsignItemsShipped,
            (new GmConsignmentDrilldownVO()),
            new GmConsignmentDrilldownVO().getConsignItemsDrilldownProperties());

    gmSalesDashResVO.setListGmConsignmentDrilldownVO(listGmConsignSetsDrilldownVO);

    hmParam = null;
    alConsignItemsShipped = null;
    listGmConsignSetsDrilldownVO = null;

    return gmSalesDashResVO;
  }

  /**
   * fetchBackOrderConsignmentSets - This method will returns Back Order Consignment Items.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("backorderitem")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchBackOrderCongItems(String strInput) throws AppError {

    log.debug("=======fetch BackOrder ConsignmentItems======");

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashCongItemsBean gmsalesdashcongitemsbean = new GmSalesDashCongItemsBean(gmSalesDashReqVO);
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmParam = new HashMap();
    ArrayList alConsignItemsBackOrder, listGmConsignSetsDrilldownVO = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
    String strAccItemConRptFl = "";
    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));
    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    hmParam.put("APPLDATEFMT", strApplDateFmt);
//To show account item consignment reports based on the below rule(PMT-29390)
    strAccItemConRptFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SAL_DASH_ACC_ITM",
            "SALEDASHTAB", strCompanyID));
    hmParam.put("ACCITEMCONRPTFL",strAccItemConRptFl);
    hmParam.put("PASSNAME", "BO");
    alConsignItemsBackOrder =
        GmCommonClass.parseNullArrayList(gmsalesdashcongitemsbean.fetchBackOrderCongItems(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmConsignSetsDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alConsignItemsBackOrder,
            (new GmConsignmentDrilldownVO()),
            new GmConsignmentDrilldownVO().getConsignItemsDrilldownProperties());

    gmSalesDashResVO.setListGmConsignmentDrilldownVO(listGmConsignSetsDrilldownVO);

    hmParam = null;
    alConsignItemsBackOrder = null;
    listGmConsignSetsDrilldownVO = null;

    return gmSalesDashResVO;
  }

}
