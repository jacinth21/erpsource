package com.globus.webservice.sales.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.sales.DashBoard.beans.GmSalesDashCongSetsBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmConsignmentDrilldownVO;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.valueobject.sales.GmSalesDashResVO;
import com.globus.webservice.common.resources.GmResource;

/**
 * This class contains methods that are related to MSD - Consign Sets overview, Drill down sections.
 * 
 * @author elango
 * 
 */
@Path("consignset")
public class GmSalesDashConsignSetsResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * congSetsCount - This method will return the Sales Dash board Consignment Set count for
   * different levels.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("count")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO congSetsCount(String strInput) throws AppError {

    log.debug("=======MSD - orders data Count======" + strInput);
    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashCongSetsBean gmSalesDashCongSetsBean = new GmSalesDashCongSetsBean();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmParam = new HashMap();
    String strRuleDrillDownPath = "";
    ArrayList alReturn = new ArrayList();
    ArrayList alRequestedSets, alApprovedSets, alShippedSets, alBackOrderSets, alAllSets, alMissingSets =
        null;
    List<GmSalesDashResVO> listGmSalesDashResVO = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));

    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));

    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("TOTALS", "Y");
    hmParam.put("CONGSETTYPE", "Approved");


    alRequestedSets =
        GmCommonClass.parseNullArrayList(gmSalesDashCongSetsBean.fetchApprovedCongSets(hmParam));
    strRuleDrillDownPath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CON_SET", "REQUESTED"));
    (GmCommonClass.parseNullHashMap((HashMap) alRequestedSets.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);

    alApprovedSets =
        GmCommonClass.parseNullArrayList(gmSalesDashCongSetsBean.fetchCongSets(hmParam));
    strRuleDrillDownPath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CON_SET", "APPROVED"));
    (GmCommonClass.parseNullHashMap((HashMap) alApprovedSets.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);

    alShippedSets =
        GmCommonClass.parseNullArrayList(gmSalesDashCongSetsBean.fetchShippedCongSets(hmParam));
    strRuleDrillDownPath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CON_SET", "SHIPPED"));
    (GmCommonClass.parseNullHashMap((HashMap) alShippedSets.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);
    hmParam.put("CONGSETTYPE", "BackOrder");

    alBackOrderSets =
        GmCommonClass.parseNullArrayList(gmSalesDashCongSetsBean.fetchCongSets(hmParam));
    strRuleDrillDownPath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CON_SET", "BACKORDER"));
    (GmCommonClass.parseNullHashMap((HashMap) alBackOrderSets.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);

    alAllSets =
        GmCommonClass.parseNullArrayList(gmSalesDashCongSetsBean.fetchAllCurrCongSets(hmParam));
    strRuleDrillDownPath = GmCommonClass.parseNull(GmCommonClass.getRuleValue("CON_SET", "ALLSET"));
    (GmCommonClass.parseNullHashMap((HashMap) alAllSets.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);

    alMissingSets =
        GmCommonClass.parseNullArrayList(gmSalesDashCongSetsBean.fetchAllMissLocCongSets(hmParam));
    strRuleDrillDownPath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CON_SET", "MISSINGLOC"));
    (GmCommonClass.parseNullHashMap((HashMap) alMissingSets.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);

    alReturn.addAll(alRequestedSets);
    alReturn.addAll(alApprovedSets);
    alReturn.addAll(alShippedSets);
    alReturn.addAll(alBackOrderSets);
    alReturn.addAll(alAllSets);
    alReturn.addAll(alMissingSets);


    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmSalesDashResVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmSalesDashResVO,
            gmSalesDashResVO.getSalesDashBoardProperties());

    gmSalesDashResVO.setListGmSalesDashResListVO(listGmSalesDashResVO);

    hmParam = null;
    alReturn = null;
    alApprovedSets = null;
    alShippedSets = null;
    alBackOrderSets = null;
    alAllSets = null;
    alMissingSets = null;
    listGmSalesDashResVO = null;

    return gmSalesDashResVO;
  }

  /**
   * fetchApprovedConsignmentSets - This method will returns Requested Consignment Sets.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("requestedset")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchRequestedCongSets(String strInput) throws AppError {

    log.debug("=======fetch drillDown======");

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashCongSetsBean gmsalesdashcongsetsbean = new GmSalesDashCongSetsBean();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmParam = new HashMap();
    ArrayList alConsignSetsApproved, listGmConsignSetsDrilldownVO = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    // validate token and parse JSON into VO
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));
    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));

    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));

    hmParam.put("PASSNAME", "Requested");
    hmParam.put("APPLDATEFMT", strApplDateFmt);
    alConsignSetsApproved =
        GmCommonClass.parseNullArrayList(gmsalesdashcongsetsbean.fetchApprovedCongSets(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmConsignSetsDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alConsignSetsApproved,
            (new GmConsignmentDrilldownVO()),
            new GmConsignmentDrilldownVO().getConsignSetsReqDrilldownProperties());

    gmSalesDashResVO.setListGmConsignmentDrilldownVO(listGmConsignSetsDrilldownVO);

    hmParam = null;
    alConsignSetsApproved = null;
    listGmConsignSetsDrilldownVO = null;

    return gmSalesDashResVO;
  }

  /**
   * fetchApprovedConsignmentSets - This method will returns Approved Consignment Sets.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("approvedset")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchApprovedCongSets(String strInput) throws AppError {

    log.debug("=======fetch drillDown======");

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashCongSetsBean gmsalesdashcongsetsbean = new GmSalesDashCongSetsBean();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmParam = new HashMap();
    ArrayList alConsignSetsApproved, listGmConsignSetsDrilldownVO = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    // validate token and parse JSON into VO
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));


    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    hmParam.put("CONGSETTYPE", "Approved");
    hmParam.put("APPLDATEFMT", strApplDateFmt);
    alConsignSetsApproved =
        GmCommonClass.parseNullArrayList(gmsalesdashcongsetsbean.fetchCongSets(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmConsignSetsDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alConsignSetsApproved,
            (new GmConsignmentDrilldownVO()),
            new GmConsignmentDrilldownVO().getConsignSetsDrilldownProperties());

    gmSalesDashResVO.setListGmConsignmentDrilldownVO(listGmConsignSetsDrilldownVO);

    hmParam = null;
    alConsignSetsApproved = null;
    listGmConsignSetsDrilldownVO = null;

    return gmSalesDashResVO;
  }

  /**
   * fetchShippedConsignmentSets - This method will returns Shipped Consignment Sets.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("shippedset")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchShippedCongSets(String strInput) throws AppError {

    log.debug("=======fetch Shipped drillDown======");

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmSalesDashCongSetsBean gmsalesdashcongsetsbean = new GmSalesDashCongSetsBean();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    HashMap hmParam = new HashMap();
    ArrayList alConsignSetsShipped, listGmConsignSetsDrilldownVO = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));
    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    hmParam.put("APPLDATEFMT", strApplDateFmt);
    alConsignSetsShipped =
        GmCommonClass.parseNullArrayList(gmsalesdashcongsetsbean.fetchShippedCongSets(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmConsignSetsDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alConsignSetsShipped,
            (new GmConsignmentDrilldownVO()),
            new GmConsignmentDrilldownVO().getConsignSetsDrilldownProperties());

    gmSalesDashResVO.setListGmConsignmentDrilldownVO(listGmConsignSetsDrilldownVO);

    hmParam = null;
    alConsignSetsShipped = null;
    listGmConsignSetsDrilldownVO = null;

    return gmSalesDashResVO;
  }

  /**
   * fetchBackOrderConsignmentSets - This method will returns Back Order Consignment Sets.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("backorderset")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchBackOrderedCongSets(String strInput) throws AppError {

    log.debug("=======fetch BackOrder drillDown======");

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashCongSetsBean gmsalesdashcongsetsbean = new GmSalesDashCongSetsBean();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmParam = new HashMap();
    ArrayList alConsignSetsBackOrder, listGmConsignSetsDrilldownVO = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    hmParam.put("CONGSETTYPE", "BackOrder");
    hmParam.put("APPLDATEFMT", strApplDateFmt);
    alConsignSetsBackOrder =
        GmCommonClass.parseNullArrayList(gmsalesdashcongsetsbean.fetchCongSets(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmConsignSetsDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alConsignSetsBackOrder,
            (new GmConsignmentDrilldownVO()),
            new GmConsignmentDrilldownVO().getConsignSetsDrilldownProperties());


    gmSalesDashResVO.setListGmConsignmentDrilldownVO(listGmConsignSetsDrilldownVO);

    hmParam = null;
    alConsignSetsBackOrder = null;
    listGmConsignSetsDrilldownVO = null;

    return gmSalesDashResVO;
  }

  /**
   * fetchAllSetsConsignmentSets - This method will returns All Current Consignment Sets.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("allset")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchAllCongSets(String strInput) throws AppError {

    log.debug("=======fetch drillDown======");

    long startTime = System.currentTimeMillis();

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashCongSetsBean gmsalesdashcongsetsbean = new GmSalesDashCongSetsBean();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmParam = new HashMap();
    ArrayList alConsignAllSets, listGmConsignSetsDrilldownVO = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // If the company id is not passing from input, we have to pass default COMP,DIV ID's based on
    // logged in User ID.
    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    hmParam.put("APPLDATEFMT", strApplDateFmt);
    alConsignAllSets =
        GmCommonClass.parseNullArrayList(gmsalesdashcongsetsbean.fetchAllCurrCongSets(hmParam));
    long endTime = System.currentTimeMillis();
    log.debug("::::" + startTime + ":::::endTime::::" + endTime
        + "::::Time taken in seconds======================>" + (endTime - startTime));

    startTime = System.currentTimeMillis();
    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmConsignSetsDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alConsignAllSets,
            (new GmConsignmentDrilldownVO()),
            new GmConsignmentDrilldownVO().getConsignSetsDrilldownProperties());

    endTime = System.currentTimeMillis();
    log.debug("::::" + startTime + ":::::endTime::::" + endTime
        + "::::Time taken in seconds======================>" + (endTime - startTime));
    gmSalesDashResVO.setListGmConsignmentDrilldownVO(listGmConsignSetsDrilldownVO);

    startTime = System.currentTimeMillis();
    hmParam = null;
    alConsignAllSets = null;
    listGmConsignSetsDrilldownVO = null;

    endTime = System.currentTimeMillis();
    log.debug("::::" + startTime + ":::::endTime::::" + endTime
        + "::::Time taken in seconds======================>" + (endTime - startTime));
    return gmSalesDashResVO;
  }

  /**
   * fetchAllMissingSetsConsignmentSets - This method will returns Missed Consignment Sets.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("missingset")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchAllMissingSetsConsignmentSets(String strInput) throws AppError {

    log.debug("=======fetch All Sets Consignment Sets drillDown======");

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashCongSetsBean gmsalesdashcongsetsbean = new GmSalesDashCongSetsBean();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmParam = new HashMap();
    ArrayList alConsignMissingSets, listGmConsignSetsDrilldownVO = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));
    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
    // hmParam.put("CONGSETTYPE", "BackOrder");
    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    hmParam.put("APPLDATEFMT", strApplDateFmt);
    alConsignMissingSets =
        GmCommonClass.parseNullArrayList(gmsalesdashcongsetsbean.fetchAllMissLocCongSets(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmConsignSetsDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alConsignMissingSets,
            (new GmConsignmentDrilldownVO()),
            new GmConsignmentDrilldownVO().getConsignSetsDrilldownProperties());

    gmSalesDashResVO.setListGmConsignmentDrilldownVO(listGmConsignSetsDrilldownVO);

    hmParam = null;
    alConsignMissingSets = null;
    listGmConsignSetsDrilldownVO = null;

    return gmSalesDashResVO;
  }

}
