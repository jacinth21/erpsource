package com.globus.webservice.sales.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.sales.DashBoard.beans.GmSalesDashReturnsBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmReturnsDrilldownVO;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.valueobject.sales.GmSalesDashResVO;
import com.globus.webservice.common.resources.GmResource;

/**
 * This class contains methods that are related to MSD - returns overview, Drilldown sections.
 * 
 * @author elango
 * 
 */
@Path("returns")
public class GmSalesDashReturnsResource extends GmResource {


  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * loanersCount - This method will return the Sales Dash board returns count for different RA
   * levels.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("count")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO returnsCount(String strInput) throws AppError {

    log.debug("=======MSD - returns data Count======" + strInput);
    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashReturnsBean gmSalesDashReturnsBean = new GmSalesDashReturnsBean();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    HashMap hmParam = new HashMap();
    HashMap hmTemp = new HashMap();
    HashMap hmClosedInitCount = new HashMap();
    String strRuleDrillDownPath = "";
    ArrayList alReturn = new ArrayList();
    ArrayList alReturnsCount = new ArrayList();
    ArrayList alReturnsInitiated = new ArrayList();
    ArrayList alReturnsClosed = new ArrayList();
    List<GmSalesDashResVO> listGmSalesDashResVO = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    String strAccItemConRptFl = "";

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    //To show account item consignment reports based on the below rule(PMT-29390)
    strAccItemConRptFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SAL_DASH_ACC_ITM",
            "SALEDASHTAB", strCompanyID));


    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("PASSNAME", "Initiated");
    if(strAccItemConRptFl.equals("Y")){
     //to fetch the count of both initiated and closed returns in single method call(query)
    
      alReturnsCount=
         GmCommonClass.parseNullArrayList(gmSalesDashReturnsBean.fetchAccReturnCount(hmParam));
     
     int alsize = alReturnsCount.size();
     if (alsize <= 2) {
       for (int i = 0; i < alsize; i++) {
         hmTemp = GmCommonClass.parseNullHashMap((HashMap) alReturnsCount.get(i));
         hmClosedInitCount.put(hmTemp.get("NAME"), hmTemp.get("TOTAL"));
         //if it is matched , then setting into the corresponding arraylist
         if(hmTemp.get("NAME").equals("RA - Initiated in Last 60 days")){
           alReturnsInitiated.add(hmTemp);
           //setting drill down URL as per the existing flow
           strRuleDrillDownPath =
               GmCommonClass.parseNull(GmCommonClass.getRuleValue("RETURNS", "Initiated"));
         }else if(hmTemp.get("NAME").equals("RA - Closed in Last 60 days")){
           alReturnsClosed.add(hmTemp);
           strRuleDrillDownPath =
               GmCommonClass.parseNull(GmCommonClass.getRuleValue("RETURNS", "Closed"));
         }
         (GmCommonClass.parseNullHashMap((HashMap) alReturnsCount.get(i))).put("DRILLDOWNURL",
             strRuleDrillDownPath);
       }
       //Adding RA - Initiated value as 0
       if(hmClosedInitCount.get("RA - Initiated in Last 60 days") == null){
         hmTemp = new HashMap();
         hmTemp.put("NAME", "RA - Initiated in Last 60 days");
         hmTemp.put("TOTAL", 0);
         alReturnsInitiated.add(hmTemp);
       }
       //Adding RA - Closed value as 0
       if (hmClosedInitCount.get("RA - Closed in Last 60 days") == null) {
         hmTemp = new HashMap();
         hmTemp.put("NAME", "RA - Closed in Last 60 days");
         hmTemp.put("TOTAL", 0);
         alReturnsClosed.add(hmTemp);
       }
     }
      
    }else{
  
    alReturnsInitiated =
        GmCommonClass.parseNullArrayList(gmSalesDashReturnsBean.fetchReturnCount(hmParam));
    
    strRuleDrillDownPath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("RETURNS", "Initiated"));
    (GmCommonClass.parseNullHashMap((HashMap) alReturnsInitiated.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);

    hmParam.put("PASSNAME", "Closed");
    alReturnsClosed =
        GmCommonClass.parseNullArrayList(gmSalesDashReturnsBean.fetchReturnCount(hmParam));

    strRuleDrillDownPath = GmCommonClass.parseNull(GmCommonClass.getRuleValue("RETURNS", "Closed"));
    (GmCommonClass.parseNullHashMap((HashMap) alReturnsClosed.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);
    }
    alReturn.addAll(alReturnsInitiated);
    alReturn.addAll(alReturnsClosed);

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmSalesDashResVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmSalesDashResVO,
            gmSalesDashResVO.getSalesDashBoardProperties());

    gmSalesDashResVO.setListGmSalesDashResListVO(listGmSalesDashResVO);

    hmParam = null;
    alReturn = null;
    alReturnsInitiated = null;
    alReturnsClosed = null;
    listGmSalesDashResVO = null;

    return gmSalesDashResVO;
  }

  /**
   * fetchPOs - This method will returns Initiated RA details.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("initiatedra")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchInitiatedReturns(String strInput) throws AppError {

    log.debug("=======fetch drillDown======");
    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashReturnsBean gmSalesDashReturnsBean = new GmSalesDashReturnsBean();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    HashMap hmParam = new HashMap();
    String strRulePassName = "";
    ArrayList alInitReturns, listReturnsDrilldownVO = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
    String strAccItemConRptFl = "";
    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    HashMap hmValidate = new HashMap();
    hmValidate.put("passname", "20625");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmSalesDashReqVO, hmValidate);

    String strPassName = GmCommonClass.parseNull(gmSalesDashReqVO.getPassname());
    if (!strPassName.equals("")) {
      strRulePassName = GmCommonClass.parseNull(GmCommonClass.getRuleValue("RETURNS", strPassName));
      if (strRulePassName.equals("")) {
        throw new AppError("", "20628", 'E');
      }
    }

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    hmParam.put("PASSNAME", strPassName);
    hmParam.put("APPLDATEFMT", strApplDateFmt);
//To show account item consignment reports based on the below rule(PMT-29390)
    strAccItemConRptFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SAL_DASH_ACC_ITM",
            "SALEDASHTAB", strCompanyID));
  
    if(strAccItemConRptFl.equals("Y")){
      alInitReturns =
          GmCommonClass.parseNullArrayList(gmSalesDashReturnsBean.fetchAccCongItemReturns(hmParam));
    }else{
    alInitReturns =
        GmCommonClass.parseNullArrayList(gmSalesDashReturnsBean.fetchAllInitReturns(hmParam));
    }
    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listReturnsDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alInitReturns, (new GmReturnsDrilldownVO()),
            new GmReturnsDrilldownVO().getReturnDrilldownProperties());

    gmSalesDashResVO.setListGmReturnsDrilldownVO(listReturnsDrilldownVO);

    hmParam = null;
    alInitReturns = null;
    listReturnsDrilldownVO = null;

    return gmSalesDashResVO;
  }

  /**
   * fetchPOs - This method will returns Closed RA details.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("closedra")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchClosedReturns(String strInput) throws AppError {

    log.debug("=======fetch drillDown======");
    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashReturnsBean gmSalesDashReturnsBean = new GmSalesDashReturnsBean();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    HashMap hmParam = new HashMap();
    String strRulePassName = "";
    ArrayList alClosedReturns, listReturnsDrilldownVO = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
    String strAccItemConRptFl = "";

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    HashMap hmValidate = new HashMap();
    hmValidate.put("passname", "20625");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmSalesDashReqVO, hmValidate);

    String strPassName = GmCommonClass.parseNull(gmSalesDashReqVO.getPassname());
    if (!strPassName.equals("")) {
      strRulePassName = GmCommonClass.parseNull(GmCommonClass.getRuleValue("RETURNS", strPassName));
      if (strRulePassName.equals("")) {
        throw new AppError("", "20628", 'E');
      }
    }

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    hmParam.put("PASSNAME", strPassName);
    hmParam.put("APPLDATEFMT", strApplDateFmt);
//To show account item consignment reports based on the below rule(PMT-29390)
    strAccItemConRptFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SAL_DASH_ACC_ITM",
            "SALEDASHTAB", strCompanyID));
    hmParam.put("ACCITEMCONRPTFL",strAccItemConRptFl);
    if(strAccItemConRptFl.equals("Y")){
      alClosedReturns =
          GmCommonClass.parseNullArrayList(gmSalesDashReturnsBean.fetchAccCongItemReturns(hmParam));
    }else{
    alClosedReturns =
        GmCommonClass.parseNullArrayList(gmSalesDashReturnsBean.fetchAllClosedReturns(hmParam));
    }

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listReturnsDrilldownVO =
        (ArrayList) gmWSUtil
            .getVOListFromHashMapList(alClosedReturns, (new GmReturnsDrilldownVO()),
                new GmReturnsDrilldownVO().getReturnDrilldownProperties());

    gmSalesDashResVO.setListGmReturnsDrilldownVO(listReturnsDrilldownVO);

    hmParam = null;
    alClosedReturns = null;
    listReturnsDrilldownVO = null;

    return gmSalesDashResVO;
  }
}
