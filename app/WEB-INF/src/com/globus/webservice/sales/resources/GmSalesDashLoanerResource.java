package com.globus.webservice.sales.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.sales.DashBoard.beans.GmSalesDashLoanerBean;
import com.globus.sales.DashBoard.beans.GmSalesDashLoanerLateFeeBean;
import com.globus.sales.Loaners.beans.GmLoanerExtBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmLoanerDrilldownVO;
import com.globus.valueobject.sales.GmLoanerRptVO;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.valueobject.sales.GmSalesDashResVO;
import com.globus.webservice.common.resources.GmResource;

/**
 * This class contains methods that are related to MSD - Loaner overview, Drilldown sections.
 * 
 * @author elango
 * 
 */
@Path("loaners")
public class GmSalesDashLoanerResource extends GmResource {



  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * loanersCount - This method will return the Sales Dash board Loaner count for different Loaner
   * levels. From the Input get the User ID. Populate sales params which will be used to get the
   * appropriate loaner details. Finally set this list details in GmSalesDashResVO. and return this
   * VO.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("count")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO loanersCount(String strInput) throws AppError, Exception {

    log.debug("=======MSD - loaners data Count======" + strInput);

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    // Object initialization
    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmSalesDashLoanerBean gmSalesDashLoanerBean = new GmSalesDashLoanerBean();
    GmSalesDashLoanerLateFeeBean gmSalesDashLoanerLateFeeBean = new GmSalesDashLoanerLateFeeBean(gmSalesDashReqVO);
    String strRuleDrillDownPath = "";
    String strLateFeeAcc = "";
    ArrayList alReturn = new ArrayList();
    ArrayList alReqLoaners, alApprLoaners, alShippedLoaners, alDue1dayLoaners, alDue2dayLoaners, alOverDueLoaners, alMissPartLoaners, alDueTodayLoaners, alLoanerLateFees =
        null;
    List<GmSalesDashResVO> listGmSalesDashResVO = null;

    // Assigning all the request IN param's into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    HashMap hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));

    // To get the Access Level filter condition information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    hmParam.put("APPLDATEFMT", strApplDateFmt);

    alReqLoaners = GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean.fetchReqLoaner(hmParam));
    strRuleDrillDownPath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("LOANERS", "Requested"));
    (GmCommonClass.parseNullHashMap((HashMap) alReqLoaners.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);

    alApprLoaners =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean.fetchApprLoaner(hmParam));
    strRuleDrillDownPath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("LOANERS", "Approved"));
    (GmCommonClass.parseNullHashMap((HashMap) alApprLoaners.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);

    alShippedLoaners =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean
            .fetchDuedayLoaner(hmParam, "Shipped"));
    strRuleDrillDownPath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("LOANERS", "Shipped"));
    (GmCommonClass.parseNullHashMap((HashMap) alShippedLoaners.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);

    alDueTodayLoaners =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean.fetchDuedayLoaner(hmParam,
            "DueToday"));
    strRuleDrillDownPath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("LOANERS", "DueToday"));
    (GmCommonClass.parseNullHashMap((HashMap) alDueTodayLoaners.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);

    alDue1dayLoaners =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean
            .fetchDuedayLoaner(hmParam, "Due1Day"));
    strRuleDrillDownPath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("LOANERS", "Due1Day"));
    (GmCommonClass.parseNullHashMap((HashMap) alDue1dayLoaners.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);

    alDue2dayLoaners =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean
            .fetchDuedayLoaner(hmParam, "Due2Day"));
    strRuleDrillDownPath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("LOANERS", "Due2Day"));
    (GmCommonClass.parseNullHashMap((HashMap) alDue2dayLoaners.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);

    alOverDueLoaners =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean
            .fetchDuedayLoaner(hmParam, "OverDue"));
    strRuleDrillDownPath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("LOANERS", "OverDue"));
    (GmCommonClass.parseNullHashMap((HashMap) alOverDueLoaners.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);

    alMissPartLoaners =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean.fetchMissingPartLoaner(hmParam));
    strRuleDrillDownPath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("LOANERS", "MissingPart"));
    (GmCommonClass.parseNullHashMap((HashMap) alMissPartLoaners.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);
    
    strLateFeeAcc = GmCommonClass.parseNull(GmCommonClass.getString("SHOW_DASH_LATE_FEE"));
    if (strLateFeeAcc.equalsIgnoreCase("YES")) {
    	hmParam.put("APPLNDATEFMT", gmSalesDashResVO.getCmpdfmt());
    	alLoanerLateFees = 
    	    	GmCommonClass.parseNullArrayList(gmSalesDashLoanerLateFeeBean.fetchLoanerAccrLateFeeTotal(hmParam));
    	strRuleDrillDownPath = GmCommonClass.parseNull(GmCommonClass.getRuleValue("LOANERS", "LATEFEEACCRSTMT"));
        (GmCommonClass.parseNullHashMap((HashMap) alLoanerLateFees .get(0))).put("DRILLDOWNURL", strRuleDrillDownPath);
        strRuleDrillDownPath = GmCommonClass.parseNull(GmCommonClass.getRuleValue("LOANERS", "RECENTLATEFEEACCR"));
        (GmCommonClass.parseNullHashMap((HashMap) alLoanerLateFees .get(1))).put("DRILLDOWNURL",strRuleDrillDownPath);
    } 
   
    alReturn.addAll(alReqLoaners);
    alReturn.addAll(alApprLoaners);
    alReturn.addAll(alShippedLoaners);
    alReturn.addAll(alDueTodayLoaners);
    alReturn.addAll(alDue1dayLoaners);
    alReturn.addAll(alDue2dayLoaners);
    alReturn.addAll(alOverDueLoaners);
    alReturn.addAll(alMissPartLoaners);
	//PC-4564 sales dashboard production issue. It was outside if-condition so it throws null pointer exception
	alReturn.addAll(GmCommonClass.parseNullArrayList(alLoanerLateFees));
	
    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmSalesDashResVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmSalesDashResVO,
            gmSalesDashResVO.getSalesDashBoardProperties());

    gmSalesDashResVO.setListGmSalesDashResListVO(listGmSalesDashResVO);

    // Nullifying the Objects.
    alReturn = null;
    hmParam = null;
    alReqLoaners = null;
    alApprLoaners = null;
    alShippedLoaners = null;
    alDue1dayLoaners = null;
    alDue2dayLoaners = null;
    alOverDueLoaners = null;
    alMissPartLoaners = null;
    alDueTodayLoaners = null;
    alLoanerLateFees = null;
    listGmSalesDashResVO = null;

    return gmSalesDashResVO;

  }

  /**
   * fetchRequestedLoaner - This method will returns Requested loaners drilldown details.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("requested")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO requestedLoaners(String strInput) throws AppError {

    log.debug("=======requested drillDown======");

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmSalesDashLoanerBean gmSalesDashLoanerBean = new GmSalesDashLoanerBean();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    HashMap hmParam = new HashMap();
    ArrayList alReqLoaners, listLoanerDrilldownVO = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
    // To get the Access Level filter condition information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));

    hmParam.put("PASSNAME", "Requested");
    hmParam.put("APPLDATEFMT", strApplDateFmt);
    alReqLoaners = GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean.fetchReqLoaner(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listLoanerDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alReqLoaners, (new GmLoanerDrilldownVO()),
            new GmLoanerDrilldownVO().getLoanerDrilldownProperties());

    gmSalesDashResVO.setListGmLoanerDrilldownVO(listLoanerDrilldownVO);

    hmParam = null;
    alReqLoaners = null;
    listLoanerDrilldownVO = null;

    return gmSalesDashResVO;
  }

  /**
   * approvedLoaners - This method will returns Approved Loaners drilldown details.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("approved")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO approvedLoaners(String strInput) throws AppError {

    log.debug("=======approved drillDown======");

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashLoanerBean gmSalesDashLoanerBean = new GmSalesDashLoanerBean();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    ArrayList alApprovLoaners, listLoanerDrilldownVO = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    HashMap hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
    // To get the Access Level filter condition information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    hmParam.put("PASSNAME", "Approved");
    hmParam.put("APPLDATEFMT", strApplDateFmt);

    alApprovLoaners =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean.fetchApprLoaner(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listLoanerDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alApprovLoaners, (new GmLoanerDrilldownVO()),
            new GmLoanerDrilldownVO().getLoanerDrilldownProperties());

    gmSalesDashResVO.setListGmLoanerDrilldownVO(listLoanerDrilldownVO);

    hmParam = null;
    alApprovLoaners = null;
    listLoanerDrilldownVO = null;
    return gmSalesDashResVO;
  }

  /**
   * dueDayLoaners - This method will returns due day Loaners drilldown details.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("dueloaners")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO dueDayLoaners(String strInput) throws AppError {

    log.debug("=======fetch drillDown======");

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
   /* if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }*/
    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashLoanerBean gmSalesDashLoanerBean = new GmSalesDashLoanerBean();
    GmLoanerExtBean gmLoanerExtBean = new GmLoanerExtBean();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    HashMap hmParam = new HashMap();
    String strPassName = "";
    String strName = "";
    String strRulePassName = "";
    String strReqDtlIDs = "";
    ArrayList allLoaners, listLoanerDrilldownVO = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    strPassName = GmCommonClass.parseNull(gmSalesDashReqVO.getPassname());
    strReqDtlIDs = GmCommonClass.parseNull(gmSalesDashReqVO.getReqdtlids());


    HashMap hmValidate = new HashMap();
    hmValidate.put("passname", "20625");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmSalesDashReqVO, hmValidate);

    if (!strPassName.equals("")) {
      strRulePassName = GmCommonClass.parseNull(GmCommonClass.getRuleValue("LOANERS", strPassName));
      if (strRulePassName.equals("")) {
        throw new AppError("", "20626", 'E');
      }
    }

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));

    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    hmParam.put("PASSNAME", strPassName);
    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("REQDTLIDS", strReqDtlIDs);

    allLoaners =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean.fetchDuedayLoaner(hmParam, strName));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listLoanerDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(allLoaners, (new GmLoanerDrilldownVO()),
            new GmLoanerDrilldownVO().getLoanerDrilldownProperties());

    gmSalesDashResVO.setListGmLoanerDrilldownVO(listLoanerDrilldownVO);

    hmParam = null;
    allLoaners = null;
    listLoanerDrilldownVO = null;
    return gmSalesDashResVO;
  }

  /**
   * missingPartLoaners - This method will returns Missing Part Loaners.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("missingparts")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO missingPartLoaners(String strInput) throws AppError {

    log.debug("=======fetch drillDown======");

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashLoanerBean gmSalesDashLoanerBean = new GmSalesDashLoanerBean();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    HashMap hmParam = new HashMap();
    ArrayList alMissingPartLoaners, listLoanerDrilldownVO = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    hmParam.put("PASSNAME", "MissingPart");
    hmParam.put("APPLDATEFMT", strApplDateFmt);

    alMissingPartLoaners =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean.fetchMissingPartLoaner(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listLoanerDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alMissingPartLoaners,
            (new GmLoanerDrilldownVO()), new GmLoanerDrilldownVO().getLoanerDrilldownProperties());

    gmSalesDashResVO.setListGmLoanerDrilldownVO(listLoanerDrilldownVO);

    hmParam = null;
    alMissingPartLoaners = null;
    listLoanerDrilldownVO = null;

    return gmSalesDashResVO;
  }

  /**
   * fetchLoanerUsageDetail - This method will be called from Device and Portal. This method is used
   * to Return Loaner usage detail for the Particular month. And This method will return Late Fee
   * for the Loaner Set which is loaned based on the Business days.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @POST
  @Path("usagedetail")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchLoanerUsageDetail(String strInput) throws AppError {

    GmLoanerRptVO gmLoanerRptVO = new GmLoanerRptVO();
    GmLoanerDrilldownVO gmLoanerDrilldownVO = new GmLoanerDrilldownVO();
    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmSalesDashLoanerBean gmSalesDashLoanerBean = new GmSalesDashLoanerBean();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    HashMap hmParam = new HashMap();
    HashMap hmfilterDetails = new HashMap();
    ArrayList alLoanerUsage = null, listLoanerDrilldownVO = null;

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmLoanerRptVO);
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmLoanerRptVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmLoanerRptVO.getToken());
    log.debug("the value inside testing purpose in resource 123 data");
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass.parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmLoanerRptVO));

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
    // To get the Access Level filter condition information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmfilterDetails = gmWSUtil.getHashMapFromVO(gmLoanerRptVO);
    hmParam.put("HMFILTERDETAILS", hmfilterDetails);

    alLoanerUsage =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean.fetchLoanerUsageDetail(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listLoanerDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alLoanerUsage, gmLoanerDrilldownVO,
            gmLoanerDrilldownVO.getLoanerUsageProperties());

    gmSalesDashResVO.setListGmLoanerDrilldownVO(listLoanerDrilldownVO);

    return gmSalesDashResVO;



  }

  /**
   * fetchLoanerUsageSummary - This method will be called from Device and Portal. This method is
   * used to Return Loaner usage Summary for the Particular month. And This method will return Count
   * of Particular Loaner Set Loaned in a period of Time.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @POST
  @Path("usagesummary")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchLoanerUsageSummary(String strInput) throws AppError {

    GmLoanerRptVO gmLoanerRptVO = new GmLoanerRptVO();
    GmLoanerDrilldownVO gmLoanerDrilldownVO = new GmLoanerDrilldownVO();
    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmSalesDashLoanerBean gmSalesDashLoanerBean = new GmSalesDashLoanerBean();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    HashMap hmParam = new HashMap();
    HashMap hmfilterDetails = new HashMap();
    ArrayList alLoanerUsage = null, listLoanerDrilldownVO = null;
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmLoanerRptVO);
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmLoanerRptVO);


    // log.debug("gmLoanerDrilldownVO.strpasname***" + gmLoanerDrilldownVO.getPassname());
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmLoanerRptVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass.parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmLoanerRptVO));
    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
    // To get the Access Level filter condition information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));


    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmfilterDetails = gmWSUtil.getHashMapFromVO(gmLoanerRptVO);
    hmParam.put("HMFILTERDETAILS", hmfilterDetails);

    alLoanerUsage =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean.fetchLoanerUsageSummary(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listLoanerDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alLoanerUsage, gmLoanerDrilldownVO,
            gmLoanerDrilldownVO.getLoanerUsageProperties());

    gmSalesDashResVO.setListGmLoanerDrilldownVO(listLoanerDrilldownVO);

    return gmSalesDashResVO;



  }


  /**
   * fetchLoanerUsage- This method will be called from Device and Portal. This method is used to
   * Return Loaner usage Summary for the Particular month.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @POST
  @Path("aggregateusage")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchLoanerUsageAggregate(String strInput) throws AppError {

    GmLoanerRptVO gmLoanerRptVO = new GmLoanerRptVO();
    GmLoanerDrilldownVO gmLoanerDrilldownVO = new GmLoanerDrilldownVO();
    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmSalesDashLoanerBean gmSalesDashLoanerBean = new GmSalesDashLoanerBean();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    HashMap hmParam = new HashMap();
    HashMap hmfilterDetails = new HashMap();
    ArrayList alLoanerUsage = null, listLoanerDrilldownVO = null;
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmLoanerRptVO);
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmLoanerRptVO);


    // log.debug("gmLoanerDrilldownVO.strpasname***" + gmLoanerDrilldownVO.getPassname());
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmLoanerRptVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass.parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmLoanerRptVO));
    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
    // To get the Access Level filter condition information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));


    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmfilterDetails = gmWSUtil.getHashMapFromVO(gmLoanerRptVO);
    hmParam.put("HMFILTERDETAILS", hmfilterDetails);

    alLoanerUsage =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean.fetchLoanerUsageAggregate(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listLoanerDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alLoanerUsage, gmLoanerDrilldownVO,
            gmLoanerDrilldownVO.getLoanerUsageProperties());

    gmSalesDashResVO.setListGmLoanerDrilldownVO(listLoanerDrilldownVO);

    return gmSalesDashResVO;
  }

}
