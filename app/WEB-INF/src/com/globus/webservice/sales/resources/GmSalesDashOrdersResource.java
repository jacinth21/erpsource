package com.globus.webservice.sales.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.sales.DashBoard.beans.GmSalesDashOrdersBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmOrdersDrilldownVO;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.valueobject.sales.GmSalesDashResVO;
import com.globus.webservice.common.resources.GmResource;

/**
 * This class contains methods that are related to MSD - Orders overview, Drilldown details.
 * 
 * @author elango
 */
@Path("orders")
public class GmSalesDashOrdersResource extends GmResource {


  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * ordersCount - This method will return the Sales Dash board Orders count for different Loaner
   * levels.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("count")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO ordersCount(String strInput) throws AppError {

    log.debug("=======MSD - orders data Count======" + strInput);
    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashOrdersBean gmSalesDashOrdersBean = new GmSalesDashOrdersBean();
    HashMap hmParam = new HashMap();
    String strRuleDrillDownPath = "";
    ArrayList alReturn = new ArrayList();
    ArrayList alSubmittedOrders, alShippedOrders, alHeldOrders, alBackOrders, alPO7Days, alPO15Days, alPO30Days =
        null;
    List<GmSalesDashResVO> listGmSalesDashResVO = null;

    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));

    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));

    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("TOTALS", "Y");

    alSubmittedOrders =
        GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchSubmittedOrders(hmParam));
    strRuleDrillDownPath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("ORDERS", "Submitted"));
    (GmCommonClass.parseNullHashMap((HashMap) alSubmittedOrders.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);

    alShippedOrders =
        GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchShippedOrders(hmParam));
    strRuleDrillDownPath = GmCommonClass.parseNull(GmCommonClass.getRuleValue("ORDERS", "Shipped"));
    (GmCommonClass.parseNullHashMap((HashMap) alShippedOrders.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);

    alHeldOrders = GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchHeldOrders(hmParam));
    strRuleDrillDownPath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("ORDERS", "HeldOrders"));
    (GmCommonClass.parseNullHashMap((HashMap) alHeldOrders.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);

    alBackOrders = GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchBackOrders(hmParam));
    strRuleDrillDownPath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("ORDERS", "BackOrder"));
    (GmCommonClass.parseNullHashMap((HashMap) alBackOrders.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);

    hmParam.put("PASSNAME", "PO14day");
    alPO7Days = GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchPOs(hmParam));
    strRuleDrillDownPath = GmCommonClass.parseNull(GmCommonClass.getRuleValue("ORDERS", "PO14day"));
    (GmCommonClass.parseNullHashMap((HashMap) alPO7Days.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);

    hmParam.put("PASSNAME", "PO14-21day");
    alPO15Days = GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchPOs(hmParam));
    strRuleDrillDownPath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("ORDERS", "PO14-21day"));
    (GmCommonClass.parseNullHashMap((HashMap) alPO15Days.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);

    hmParam.put("PASSNAME", "PO21day");
    alPO30Days = GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchPOs(hmParam));
    strRuleDrillDownPath = GmCommonClass.parseNull(GmCommonClass.getRuleValue("ORDERS", "PO21day"));
    (GmCommonClass.parseNullHashMap((HashMap) alPO30Days.get(0))).put("DRILLDOWNURL",
        strRuleDrillDownPath);

    alReturn.addAll(alSubmittedOrders);
    alReturn.addAll(alShippedOrders);
    alReturn.addAll(alHeldOrders);
    alReturn.addAll(alBackOrders);
    alReturn.addAll(alPO7Days);
    alReturn.addAll(alPO15Days);
    alReturn.addAll(alPO30Days);
    log.debug(" alReturn " + alReturn);

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmSalesDashResVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmSalesDashResVO,
            gmSalesDashResVO.getSalesDashBoardProperties());

    gmSalesDashResVO.setListGmSalesDashResListVO(listGmSalesDashResVO);

    hmParam = null;
    alReturn = null;
    alSubmittedOrders = null;
    alShippedOrders = null;
    alHeldOrders = null;
    alBackOrders = null;
    alPO7Days = null;
    alPO15Days = null;
    alPO30Days = null;
    listGmSalesDashResVO = null;

    return gmSalesDashResVO;
  }

  /**
   * fetchSubmittedOrders - This method will returns Submitted Orders.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("submitted")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchSubmittedOrders(String strInput) throws AppError {

    log.debug("=======submitted drillDown======");

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashOrdersBean gmSalesDashOrdersBean = new GmSalesDashOrdersBean();
    GmOrdersDrilldownVO gmOrdersDrilldownVO = new GmOrdersDrilldownVO();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    HashMap hmParam = new HashMap();
    ArrayList alSubmittedOrders, listOrdersDrilldownVO = null;
    Properties properties = new Properties();
    String strAccId = "";
    String strDOId = "";
    String strFromDate = "";
    String strToDate = "";
    String strStrOpt = "";
    String strLotNumber = "";
    String strSurgeonName = "";


    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    String strApplCurrSign =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CURRSYMBOL", "CURRSYMBOL"));
    properties = gmOrdersDrilldownVO.getOrderDrilldownProperties();
    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));
    strStrOpt = GmCommonClass.parseNull(gmSalesDashReqVO.getStropt());
    String strPassName = GmCommonClass.parseNull(gmSalesDashReqVO.getPassname());
    strAccId = GmCommonClass.parseNull(gmSalesDashReqVO.getAccountid());

    // stropt - DOREPORT only for to load do reports. Through salesdashboard stropt value is null
    if (strStrOpt.equals("DOREPORT")) {
      strDOId = GmCommonClass.parseNull(gmSalesDashReqVO.getDoid());
      strFromDate = GmCommonClass.parseNull(gmSalesDashReqVO.getFromdate());
      strToDate = GmCommonClass.parseNull(gmSalesDashReqVO.getTodate());
      //Getting Lot Number and Surgeon From Input
      strLotNumber = GmCommonClass.parseNull(gmSalesDashReqVO.getLotnumber());
      strSurgeonName = GmCommonClass.parseNull(gmSalesDashReqVO.getSurgeonname());
      properties = gmOrdersDrilldownVO.getDOReportProperties();
    }

    // when input contains multiple DOIds, to separate those Ids with with quotes
    strDOId = GmCommonClass.getStringWithQuotes(strDOId);
    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));

    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("ACCOUNTID", strAccId);
    hmParam.put("DOID", strDOId);
    hmParam.put("FROMDT", strFromDate);
    hmParam.put("TODT", strToDate);
    hmParam.put("STROPT", strStrOpt);
    hmParam.put("PASSNAME", strPassName.trim());
    hmParam.put("USERID", gmUserVO.getUserid());
    // For DO Report Based On Lot Number and Surgeon Name
    hmParam.put("LOTNUMBER", strLotNumber);
    hmParam.put("SURGEONNAME", strSurgeonName);
    alSubmittedOrders =
        GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchSubmittedOrders(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listOrdersDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alSubmittedOrders, gmOrdersDrilldownVO,
            properties);

    gmSalesDashResVO.setCurrency(strApplCurrSign);
    gmSalesDashResVO.setListGmOrdersDrilldownVO(listOrdersDrilldownVO);

    hmParam = null;
    alSubmittedOrders = null;
    listOrdersDrilldownVO = null;

    return gmSalesDashResVO;
  }

  /**
   * fetchShippedOrders - This method will returns Shipped Orders.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("shipped")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchShippedOrders(String strInput) throws AppError {

    log.debug("=======shipped drillDown======");

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmSalesDashOrdersBean gmSalesDashOrdersBean = new GmSalesDashOrdersBean();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    HashMap hmParam = new HashMap();
    StringBuffer strFilterCondition = new StringBuffer();
    ArrayList alReturn = new ArrayList();
    ArrayList alShippedOrders, listOrdersDrilldownVO = null;

    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    String strApplCurrSign =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CURRSYMBOL", "CURRSYMBOL"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));

    hmParam.put("APPLDATEFMT", strApplDateFmt);
    alShippedOrders =
        GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchShippedOrders(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listOrdersDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alShippedOrders, (new GmOrdersDrilldownVO()),
            new GmOrdersDrilldownVO().getOrderDrilldownProperties());

    gmSalesDashResVO.setCurrency(strApplCurrSign);
    gmSalesDashResVO.setListGmOrdersDrilldownVO(listOrdersDrilldownVO);

    hmParam = null;
    alShippedOrders = null;
    listOrdersDrilldownVO = null;

    return gmSalesDashResVO;
  }

  /**
   * fetchHeldOrders - This method will returns Held Orders drilldown details.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("heldorders")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchHeldOrders(String strInput) throws AppError {

    log.debug("=======heldorders drillDown======");

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmSalesDashOrdersBean gmSalesDashOrdersBean = new GmSalesDashOrdersBean();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    HashMap hmParam = new HashMap();
    ArrayList alReturn = new ArrayList();
    ArrayList alHeldOrders, listOrdersDrilldownVO = null;

    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    String strApplCurrSign =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CURRSYMBOL", "CURRSYMBOL"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));

    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));

    hmParam.put("APPLDATEFMT", strApplDateFmt);
    alHeldOrders = GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchHeldOrders(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listOrdersDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alHeldOrders, (new GmOrdersDrilldownVO()),
            new GmOrdersDrilldownVO().getOrderDrilldownProperties());

    gmSalesDashResVO.setCurrency(strApplCurrSign);
    gmSalesDashResVO.setListGmOrdersDrilldownVO(listOrdersDrilldownVO);

    hmParam = null;
    alHeldOrders = null;
    listOrdersDrilldownVO = null;

    return gmSalesDashResVO;
  }

  /**
   * fetchBackOrders - This method will returns Back Orders drilldown details.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("backorder")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchBackOrders(String strInput) throws AppError {

    log.debug("=======backorder drillDown======");

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashOrdersBean gmSalesDashOrdersBean = new GmSalesDashOrdersBean();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmParam = new HashMap();
    ArrayList alReturn = new ArrayList();
    ArrayList alBackOrders, listOrdersDrilldownVO = null;

    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    String strApplCurrSign =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CURRSYMBOL", "CURRSYMBOL"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));

    hmParam.put("APPLDATEFMT", strApplDateFmt);
    alBackOrders = GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchBackOrders(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listOrdersDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alBackOrders, (new GmOrdersDrilldownVO()),
            new GmOrdersDrilldownVO().getOrderDrilldownProperties());

    gmSalesDashResVO.setCurrency(strApplCurrSign);
    gmSalesDashResVO.setListGmOrdersDrilldownVO(listOrdersDrilldownVO);

    hmParam = null;
    alBackOrders = null;
    listOrdersDrilldownVO = null;

    return gmSalesDashResVO;
  }

  /**
   * fetchPOs - This method will returns Pending POs drilldown details depends on date.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("pendingpo")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchPendingOrders(String strInput) throws AppError {

    log.debug("=======pendingpo drillDown======");

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashOrdersBean gmSalesDashOrdersBean = new GmSalesDashOrdersBean();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmParam = new HashMap();
    String strRulePassName = "";
    ArrayList alPendingPOs, listOrdersDrilldownVO = null;

    String strPassName = GmCommonClass.parseNull(gmSalesDashReqVO.getPassname());

    HashMap hmValidate = new HashMap();
    hmValidate.put("passname", "20625");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmSalesDashReqVO, hmValidate);

    if (!strPassName.equals("")) {
      strRulePassName = GmCommonClass.parseNull(GmCommonClass.getRuleValue("ORDERS", strPassName));
      if (strRulePassName.equals("")) {
        throw new AppError("", "20627", 'E');
      }
    }

    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    String strApplCurrSign =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CURRSYMBOL", "CURRSYMBOL"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));

    hmParam.put("PASSNAME", strPassName.trim());
    hmParam.put("APPLDATEFMT", strApplDateFmt);

    alPendingPOs = GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchPOs(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listOrdersDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alPendingPOs, (new GmOrdersDrilldownVO()),
            new GmOrdersDrilldownVO().getOrderDrilldownProperties());

    gmSalesDashResVO.setCurrency(strApplCurrSign);
    gmSalesDashResVO.setListGmOrdersDrilldownVO(listOrdersDrilldownVO);

    hmParam = null;
    alPendingPOs = null;
    listOrdersDrilldownVO = null;

    return gmSalesDashResVO;
  }
}
