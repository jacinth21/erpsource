/**
 * 
 * @FileName : GmOrderPOStatusResource.java
 * @Copyright : Globus Medical Inc
 * @Requirement : PMT-53741
 * @author tmuthusamy
 * @Date : June 2020
 */
package com.globus.webservice.sales.resources;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmDODocumentUploadBean;
import com.globus.sales.DashBoard.beans.GmOrderPOStatusBean;
import com.globus.sales.DashBoard.beans.GmSalesDashOrdersBean;
import com.globus.valueobject.common.GmCommonFileUploadVO;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmOrderCustPOVO;
import com.globus.valueobject.sales.GmOrdersDrilldownVO;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.valueobject.sales.GmSalesDashResVO;
import com.globus.webservice.common.resources.GmResource;

/**
 * This class contains methods that are related to sales order PO status.
 * 
 * @author tmuthusamy
 */
@Path("po")
public class GmOrderPOStatusResource extends GmResource {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	/** This method used to fetch Pending Order Count and amount from t501 order table
	 *  for PO < 14, PO14 - 21, PO > 21, Pending approval and Pending Discrepancy status
	 * 
	 * @param strInput
	 * @return gmSalesDashResVO
	 * @throws AppError
	 */
	@POST
	@Path("podash")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmSalesDashResVO listPODashboard(String strInput) throws AppError {
		log.debug("pending orders dash" + strInput);
	    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
	    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();

	    // validate token and parse JSON into VO
	    parseInputAndValidateToken(strInput, gmSalesDashReqVO);
	    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
	    GmAccessFilter gmAccessFilter = new GmAccessFilter();
	    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
	    GmWSUtil gmWSUtil = new GmWSUtil();
	    GmOrderPOStatusBean gmOrderPOStatusBean = new GmOrderPOStatusBean(gmSalesDashResVO);
	    HashMap hmParam = new HashMap();
	    String strRuleDrillDownPath = "";
	    ArrayList alReturn = new ArrayList();
	    ArrayList alPO7Days, alPO15Days, alPO30Days,alPOApproval,alPODiscrepancy =
	        null;
	    List<GmSalesDashResVO> listGmSalesDashResVO = null;

	    String strApplDateFmt =
	    		GmCommonClass.parseNull(gmDataStoreVO.getCmpdfmt());

	    // Assigning all the request params into Response VO
	    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

	    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
	    // Below is the common method to retrieve the sales params.
	    hmParam =
	        GmCommonClass
	            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

	    // setting CompanyId and DivisionId to ResponseVo
	    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
	    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

	    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));

	    // To get the Access Level information
	    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));

	    hmParam.put("APPLDATEFMT", strApplDateFmt);
	    hmParam.put("TOTALS", "Y");
	    hmParam.put("CURRTYPE", GmCommonClass.parseNull(gmSalesDashReqVO.getSalesdbcurrtype()));

		// Method for fetch the data
		hmParam.put("PASSNAME", "PO14day");
		alPO7Days = GmCommonClass.parseNullArrayList(gmOrderPOStatusBean
				.fetchPODashboard(hmParam));
	    (GmCommonClass.parseNullHashMap((HashMap) alPO7Days.get(0))).put("DRILLDOWNURL",
	    		"PO14day");
		hmParam.put("PASSNAME", "PO14-21day");
		alPO15Days = GmCommonClass.parseNullArrayList(gmOrderPOStatusBean
				.fetchPODashboard(hmParam));
	    (GmCommonClass.parseNullHashMap((HashMap) alPO15Days.get(0))).put("DRILLDOWNURL",
	    		"PO14-21day");
		hmParam.put("PASSNAME", "PO21day");
		alPO30Days = GmCommonClass.parseNullArrayList(gmOrderPOStatusBean
				.fetchPODashboard(hmParam));
	    (GmCommonClass.parseNullHashMap((HashMap) alPO30Days.get(0))).put("DRILLDOWNURL",
	    		"PO21day");
		hmParam.put("PASSNAME", "PendingApproval");
		alPOApproval = GmCommonClass.parseNullArrayList(gmOrderPOStatusBean
				.fetchPODashboard(hmParam));
	    (GmCommonClass.parseNullHashMap((HashMap) alPOApproval.get(0))).put("DRILLDOWNURL",
	        "PendingApproval");
		hmParam.put("PASSNAME", "PendingDiscrepancy");
		alPODiscrepancy = GmCommonClass.parseNullArrayList(gmOrderPOStatusBean
				.fetchPODashboard(hmParam));
	    (GmCommonClass.parseNullHashMap((HashMap) alPODiscrepancy.get(0))).put("DRILLDOWNURL",
	    		"PendingDiscrepancy");

		alReturn.addAll(alPO7Days);
		alReturn.addAll(alPO15Days);
		alReturn.addAll(alPO30Days);
		alReturn.addAll(alPOApproval);
		alReturn.addAll(alPODiscrepancy);
		
		log.debug(" alReturn " + alReturn);

	    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
	    // attributes
	    listGmSalesDashResVO =
	        gmWSUtil.getVOListFromHashMapList(alReturn, gmSalesDashResVO,
	            gmSalesDashResVO.getSalesDashBoardProperties());

	    gmSalesDashResVO.setListGmSalesDashResListVO(listGmSalesDashResVO);

	    hmParam = null;
	    alReturn = null;
	    alPO7Days = null;
	    alPO15Days = null;
	    alPO30Days = null;
	    alPOApproval = null;
	    alPODiscrepancy = null;
	    listGmSalesDashResVO = null;

	    return gmSalesDashResVO;

	}
	
	/** This method used to fetch Pending Order Report for PO < 14, PO14 - 21, PO > 21,
	 *  Pending approval and Pending Discrepancy status
	 * 
	 * @param strInput
	 * @return gmSalesDashResVO
	 * @throws AppError
	 */
	
	@POST
	@Path("poreport")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmSalesDashResVO listPOReports(String strInput) throws AppError {
		log.debug("pending orders Report" + strInput);
	    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();

	    // validate token and parse JSON into VO
	    parseInputAndValidateToken(strInput, gmSalesDashReqVO);
	    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
	    GmAccessFilter gmAccessFilter = new GmAccessFilter();
	    GmOrdersDrilldownVO gmOrdersDrilldownVO = new GmOrdersDrilldownVO();
	    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
	    GmWSUtil gmWSUtil = new GmWSUtil();
	    GmOrderPOStatusBean gmOrderPOStatusBean = new GmOrderPOStatusBean(gmSalesDashResVO);
	    HashMap hmParam = new HashMap();
	    ArrayList alReturn = new ArrayList();
	    ArrayList alReturnOutput = null;
	    List<GmOrdersDrilldownVO> listGmOrdersDrilldownVO = null;

	    String strApplDateFmt =
	        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));

	    // Assigning all the request params into Response VO
	    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

	    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
	    
	    String strPassName=GmCommonClass.parseNull(gmSalesDashReqVO.getPassname());
	    // Below is the common method to retrieve the sales params.
	    hmParam =
	        GmCommonClass
	            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

	    // setting CompanyId and DivisionId to ResponseVo
	    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
	    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

	    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));

	    // To get the Access Level information
	    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));

	    hmParam.put("APPLDATEFMT", strApplDateFmt);
	    hmParam.put("PASSNAME", strPassName);
	    hmParam.put("CURRTYPE", GmCommonClass.parseNull(gmSalesDashReqVO.getSalesdbcurrtype()));
	    alReturnOutput = GmCommonClass.parseNullArrayList(gmOrderPOStatusBean
				.fetchPOReport(hmParam));
	    
	    alReturn.addAll(alReturnOutput);
		
		log.debug(" alReturn " + alReturn);

	    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
	    // attributes
		listGmOrdersDrilldownVO =
	        gmWSUtil.getVOListFromHashMapList(alReturn, gmOrdersDrilldownVO,
	        		gmOrdersDrilldownVO.getOrderDrilldownProperties());

	    gmSalesDashResVO.setListGmOrdersDrilldownVO(listGmOrdersDrilldownVO);

	    hmParam = null;
	    alReturn = null;
	    alReturnOutput = null;
	    listGmOrdersDrilldownVO = null;

	    return gmSalesDashResVO;
	}
	
	/**
	   * updatePO - Method used to update PO number and PO Amount in T501_order table
	   * 
	   * @param strInput
	   * @return JSON string
	   * @throws AppError
	   * @Author tmuthusamy
	   */
	@POST
	@Path("updatepo")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmOrderCustPOVO updatePO(String strInput)  throws AppError, IOException{
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmCustomerBean gmCustomerBean = new GmCustomerBean();

		ObjectMapper mapper = new ObjectMapper();
		GmOrderCustPOVO gmOrderCustPOVO = mapper.readValue(strInput,GmOrderCustPOVO.class);

		validateToken(gmOrderCustPOVO.getToken());
		gmOrderCustPOVO.setUserid(gmUserVO.getUserid());

		HashMap hmInput = new HashMap();
		hmInput = gmWSUtil.getHashMapFromVO(gmOrderCustPOVO);
		hmInput = gmCustomerBean.saveCustPO(hmInput);

		gmWSUtil.getVOFromHashMap(hmInput, gmOrderCustPOVO);
		return gmOrderCustPOVO;

	}
	
	/**
	   * uploadFile - Method to upload file to server path
	   * 
	   * @param strInput
	   * @return JSON string
	   * @throws AppError
	   * @Author tmuthusamy
	   */
	  @Path("uploadFile")
	  @POST
	  @Consumes(MediaType.MULTIPART_FORM_DATA)
	  public String uploadFileInfo(@FormDataParam("file") InputStream fileInputStream,
		  @FormDataParam("file") FormDataContentDisposition contentDispositionHeader,
	      @FormDataParam("token") String strToken,@FormDataParam("uploadDir") String uploadPath,
	      @FormDataParam("strRefId") String strRefID,
	      @FormDataParam("locale") String localeNm) throws AppError, Exception {

	    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();
		  String filePath =
				  uploadPath + localeNm + "\\" + strRefID + "\\"
			            + contentDispositionHeader.getFileName();
		  log.debug("filePath Check>> " + filePath);
	    String fileStatus = "File Uploaded Successfully";
	    log.debug("fileStatus ***" + fileStatus);

	    HashMap hmExpParam = new HashMap();

	    // Validating Token
	    log.debug("the value inside strToken ***" + strToken);
	    try {
	      boolean bflag = validateToken(strToken);
	      log.debug("bflag ***" + bflag);
	      String strUserId = GmCommonClass.parseNull(gmUserVO.getUserid());
	      String strfirstName = GmCommonClass.parseNull(gmUserVO.getFname());
	      String strLastName = GmCommonClass.parseNull(gmUserVO.getLname());
	      String struserName = strfirstName.concat(strLastName);
	      String filename = contentDispositionHeader.getFileName();

	      hmExpParam.put("REPEMAIL", strUserId);
	      hmExpParam.put("FILENAME", filePath);
	      hmExpParam.put("USERNAME", struserName);

	      // save the file to the server
	      File outFile = new File(filePath);
	      log.debug("outFile>>>> " + outFile);
	      log.debug("outFile>>>> " + outFile.getParent());
	      if (!outFile.exists()) {
	    	  new File(outFile.getParent()).mkdirs();
	      }
	      OutputStream outputStream = new FileOutputStream(outFile);

	      int read = 0;
	      byte[] bytes = new byte[1024];

	      while ((read = fileInputStream.read(bytes)) != -1) {
	        outputStream.write(bytes, 0, read);
	      }
	      outputStream.flush();
	      outputStream.close();
	      fileInputStream.close();

	    } catch (Exception e) {

	      log.debug("Raising exception >>>>>>>>>>>>>>>>>>>");
	      hmExpParam.put("EXCEPTION", e.getMessage());
	      throw new AppError("", "20642", 'E');
	    }
	    fileStatus = fileStatus + filePath;
	    return "" ;
	  }
	  
	  /**
	   * uploadFile - Method to save the data in T903_Upload_File_list
	   * table
	   * 
	   * @param strInput
	   * @return JSON string
	   * @throws AppError
	   * @Author tmuthusamy
	   */ 
	@POST
	@Path("saveUploadedFile")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmCommonFileUploadVO saveFileUpload(String strInput) throws AppError, IOException {

		GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();

		ObjectMapper mapper = new ObjectMapper();
		GmCommonFileUploadVO gmCommonFileUploadVO = mapper.readValue(strInput,
				GmCommonFileUploadVO.class);

		validateToken(gmCommonFileUploadVO.getToken());
		
		String strRefID = GmCommonClass.parseNull(gmCommonFileUploadVO.getRefid());
		String strRefGrp = GmCommonClass.parseNull(gmCommonFileUploadVO.getRefgroup());
		String strFileName = GmCommonClass.parseNull(gmCommonFileUploadVO.getFilename());
		String strSubtype = GmCommonClass.parseNull(gmCommonFileUploadVO.getType());
		String strUserId = GmCommonClass.parseNull(gmCommonFileUploadVO.getUserid());
		String strRefType = GmCommonClass.parseNull(gmCommonFileUploadVO.getReftype());
		String strFilesize = GmCommonClass.parseNull(gmCommonFileUploadVO.getFilesize());

		HashMap hmparams = new HashMap();
		hmparams.put("REFID", strRefID);
		hmparams.put("REFGRP", strRefGrp);
		hmparams.put("FILENAME", strFileName);
		hmparams.put("SUBTYPE", strSubtype);
		hmparams.put("USERID", strUserId);
		hmparams.put("REFTYPE", strRefType);
		hmparams.put("FILESIZE", strFilesize);

		log.debug("uploadprFile==========>>>>>>>" + hmparams);
		// calling the saveUpload method
		String strFileId = gmCommonUploadBean.saveUploadUniqueInfo(hmparams);

		gmCommonFileUploadVO.setFileid(strFileId);
	    log.debug("gmCommonFileUploadVO>>>>" + gmCommonFileUploadVO);
	    

	    return gmCommonFileUploadVO;

	}
	
	/**
	 * This method used to fetch the length of customer PO 
	 * 
     * @param strInput
     * @return JSON string
     * @throws AppError
     * @Author tmuthusamy
	 */

	@POST
	@Path("checkPoNumber")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String fetchCheckPoNumber(String strInput) throws AppError,IOException {
		log.debug("strInput" + strInput);
		String strReturnJSON = "";
		GmWSUtil gmWSUtil = new GmWSUtil();

		ObjectMapper mapper = new ObjectMapper();
		GmOrderCustPOVO gmOrderCustPOVO = mapper.readValue(strInput,GmOrderCustPOVO.class);
		GmOrderPOStatusBean gmOrderPOStatusBean = new GmOrderPOStatusBean(gmOrderCustPOVO);

		validateToken(gmOrderCustPOVO.getToken());
		gmOrderCustPOVO.setUserid(gmUserVO.getUserid());
		HashMap hmParam = new HashMap();
		
		gmWSUtil.parseJsonToVO(strInput, gmOrderCustPOVO);
		hmParam = gmWSUtil.getHashMapFromVO(gmOrderCustPOVO);
		strReturnJSON = gmOrderPOStatusBean.checkCustomerPO(hmParam);
		
		return strReturnJSON;

	}
	
	
	/**
	 * This method used to update the flag when file save for the order
	 * 
     * @param strInput
     * @return string
     * @throws AppError
     * @Author tmuthusamy
	 */
	@POST
	@Path("updateorderfl")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String updateOrderFlag(String strInput)  throws AppError, IOException{
		
		ObjectMapper mapper = new ObjectMapper();
		GmCommonFileUploadVO gmCommonFileUploadVO = mapper.readValue(strInput,
				GmCommonFileUploadVO.class);

		validateToken(gmCommonFileUploadVO.getToken());
		
		GmDODocumentUploadBean gmDODocumentUploadBean = new GmDODocumentUploadBean(gmCommonFileUploadVO);
		HashMap hmParam = new HashMap();
		hmParam.put("DO_DOC_UPLOAD_FL", GmCommonClass.parseNull(gmCommonFileUploadVO.getDeletefl()));
		hmParam.put("STRORDERID", GmCommonClass.parseNull(gmCommonFileUploadVO.getRefid()));
		hmParam.put("USERID", GmCommonClass.parseNull(gmCommonFileUploadVO.getUserid()));
		log.debug("hmParam>>>> " + hmParam);
        gmDODocumentUploadBean.updateDODocumentUploadFlag(hmParam);
		return "";

	}
	
	/**
	 * This method used to fetch the order list of having PO Number which pass into service
	 * 
     * @param strInput
     * @return strReturnJSON
     * @throws AppError
     * @Author tmuthusamy
	 */
	@POST
	@Path("orderlist")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String fetchOrderList(String strInput) throws AppError,IOException {
		log.debug("strInput" + strInput);
		String strReturnJSON = "";
		GmWSUtil gmWSUtil = new GmWSUtil();

		ObjectMapper mapper = new ObjectMapper();
		GmOrderCustPOVO gmOrderCustPOVO = mapper.readValue(strInput,GmOrderCustPOVO.class);
		GmOrderPOStatusBean gmOrderPOStatusBean = new GmOrderPOStatusBean(gmOrderCustPOVO);

		validateToken(gmOrderCustPOVO.getToken());
		gmOrderCustPOVO.setUserid(gmUserVO.getUserid());
		HashMap hmParam = new HashMap();
		
		gmWSUtil.parseJsonToVO(strInput, gmOrderCustPOVO);
		hmParam = gmWSUtil.getHashMapFromVO(gmOrderCustPOVO);
		
		strReturnJSON = gmOrderPOStatusBean.fetchOrderPoList(hmParam);
		
		return strReturnJSON;

	}
	
	
	/**
	 * This method used to fetch the Access flag information for the user have a permission access to uploaded file
	 * 
     * @param strInput
     * @return strReturnJSON
     * @throws AppError
     * @Author tmuthusamy
	 */
	@POST
	@Path("accessfl")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String loadFetchAccessFlag(String strInput) throws AppError,IOException {
		log.debug("strInput" + strInput);
		String strReturnJSON = "";
		GmWSUtil gmWSUtil = new GmWSUtil();

		ObjectMapper mapper = new ObjectMapper();
		GmOrderCustPOVO gmOrderCustPOVO = mapper.readValue(strInput,GmOrderCustPOVO.class);
		GmOrderPOStatusBean gmOrderPOStatusBean = new GmOrderPOStatusBean(gmOrderCustPOVO);

		validateToken(gmOrderCustPOVO.getToken());
		gmOrderCustPOVO.setUserid(gmUserVO.getUserid());
		HashMap hmParam = new HashMap();
		
		gmWSUtil.parseJsonToVO(strInput, gmOrderCustPOVO);
		hmParam = gmWSUtil.getHashMapFromVO(gmOrderCustPOVO);
		
		strReturnJSON = gmOrderPOStatusBean.fetchAccessFlInfo(hmParam);
		
		return strReturnJSON;

	}

}
