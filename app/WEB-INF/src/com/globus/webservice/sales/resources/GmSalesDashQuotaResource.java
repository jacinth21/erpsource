package com.globus.webservice.sales.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.sales.DashBoard.beans.GmSalesDashBoardBean;
import com.globus.sales.beans.GmTerritoryReportBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmSalesDashGrowthResVO;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.valueobject.sales.GmSalesDashResVO;
import com.globus.webservice.common.resources.GmResource;

/**
 * This class contains methods that are related to MSD - quota overview, Drilldown sections.
 * 
 * @author elango
 * 
 */
@Path("quota")
public class GmSalesDashQuotaResource extends GmResource {


  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * quotaPerformance - This method will calculate the Quota.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("count")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO quotaPerformance(String strInput) throws AppError {

    log.debug("=======MSD - quota Performance  Count======" + strInput);
    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmSalesDashBoardBean gmSalesDashBoardBean = new GmSalesDashBoardBean();
    GmTerritoryReportBean gmTerritoryReportBean = new GmTerritoryReportBean();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alDetails, alReturn = null;
    List<GmSalesDashGrowthResVO> listGmSalesDashGrowthResVO = null;

    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    String strApplCurrFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CURRFMT", "CURRFMT"));
    String strApplCurrSign =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CURRSYMBOL", "CURRSYMBOL"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // If the company id is not passing from input, we have to pass default COMP,DIV ID's based on
    // logged in User ID.
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));

    // To get the Access Level filter information
    hmReturn = gmAccessFilter.getSalesAccessFilter(hmParam);
    
    // For Quota, we need v700 sales filter condition. so here replacing it with t501 filter cond.
    hmParam.put("Condition", GmCommonClass.parseNull((String) hmReturn.get("AccessFilter")));
    
    String strFilter = GmCommonClass.parseNull((new GmServlet()).getSalesFilter(hmParam));
    hmParam.put("Filter", strFilter);

    String strType = GmCommonClass.parseNull(gmSalesDashBoardBean.fetchTypeQuota(hmParam));
    hmParam.put("TYPE", strType);
    hmParam.put("Divider", "1");
    hmParam.put("strApplCurrSign", strApplCurrSign);
    hmParam.put("strApplCurrFmt", strApplCurrFmt);
    hmParam.put("COMPID", hmParam.get("COMP_FILTER"));
    //33183 Quota Performance Growth Widget
    hmParam.put("HistCondition", GmCommonClass.parseNull((String) hmReturn.get("Condition"))); 
    hmParam.put("ReportType", "Historical");
    //33183 Quota Performance Growth Widget

    hmReturn = gmTerritoryReportBean.fetchTerritoryPerformanceReport(hmParam);
    alDetails = (ArrayList) hmReturn.get("Details");
    hmReturn = gmSalesDashBoardBean.getQuotaHmapDetails(alDetails, hmParam);

    // To populate the Sales Quota List
    alReturn = generateSalesQuotaList(hmReturn);

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmSalesDashGrowthResVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, (new GmSalesDashGrowthResVO()),
            new GmSalesDashGrowthResVO().getSalesDashGrowthProperties());

    gmSalesDashResVO.setCurrency(strApplCurrSign);
    gmSalesDashResVO.setDatefmt(strApplDateFmt);
    gmSalesDashResVO.setListGmSalesDashGrowthResVO(listGmSalesDashGrowthResVO);

    hmParam = null;
    hmReturn = null;
    alDetails = null;
    alReturn = null;
    listGmSalesDashGrowthResVO = null;

    return gmSalesDashResVO;
  }


  /**
   * generateSalesQuotaList - Used to populate the Final Quota list.
   * 
   * @param hmSalesDetails - Contains all the info's of Quota/Growth Quadrant.
   * @return ArrayList
   * @throws AppError
   */
  private ArrayList generateSalesQuotaList(HashMap hmSalesDetails) throws AppError {

    ArrayList alReturn = new ArrayList();
    HashMap hmTemp = null;

    hmTemp = new HashMap();
    hmTemp.put("TYPE", "Sales");
    hmTemp.put("MTD", GmCommonClass.parseDouble((Double) hmSalesDetails.get("MTD")) + "");
    hmTemp.put("YTD", GmCommonClass.parseDouble((Double) hmSalesDetails.get("YTD")) + "");
    alReturn.add(hmTemp);

    hmTemp = new HashMap();
    hmTemp.put("TYPE", "Quota");
    hmTemp.put("MTD", GmCommonClass.parseDouble((Double) hmSalesDetails.get("QMTD")) + "");
    hmTemp.put("YTD", GmCommonClass.parseDouble((Double) hmSalesDetails.get("QYTD")) + "");
    alReturn.add(hmTemp);

    hmTemp = new HashMap();
    hmTemp.put("TYPE", "(%) To Quota ");
    hmTemp.put("MTD", GmCommonClass.parseDouble((Double) hmSalesDetails.get("MQPER")) + "");
    hmTemp.put("YTD", GmCommonClass.parseDouble((Double) hmSalesDetails.get("YQPER")) + "");
    alReturn.add(hmTemp);

    hmTemp = new HashMap();
    hmTemp.put("TYPE", "Previous Sale");
    hmTemp.put("MTD", GmCommonClass.parseDouble((Double) hmSalesDetails.get("PMTD")) + "");
    hmTemp.put("YTD", GmCommonClass.parseDouble((Double) hmSalesDetails.get("PYTD")) + "");
    alReturn.add(hmTemp);

    hmTemp = new HashMap();
    hmTemp.put("TYPE", "Growth");
    hmTemp.put("MTD", GmCommonClass.parseDouble((Double) hmSalesDetails.get("MTDGTHDLR")) + "");
    hmTemp.put("YTD", GmCommonClass.parseDouble((Double) hmSalesDetails.get("YTDGTHDLR")) + "");
    alReturn.add(hmTemp);

    hmTemp = new HashMap();
    hmTemp.put("TYPE", "(%) To Growth");
    hmTemp.put("MTD", GmCommonClass.parseDouble((Double) hmSalesDetails.get("MTDGTHPER")) + "");
    hmTemp.put("YTD", GmCommonClass.parseDouble((Double) hmSalesDetails.get("YTDGTHPER")) + "");
    alReturn.add(hmTemp);

    hmTemp = new HashMap();
    hmTemp.put("TYPE", "Projected");
    hmTemp.put("MTD", GmCommonClass.parseDouble((Double) hmSalesDetails.get("EMTD")) + "");
    hmTemp.put("YTD", GmCommonClass.parseDouble((Double) hmSalesDetails.get("EYTD")) + "");
    alReturn.add(hmTemp);

    return alReturn;
  }
}
