package com.globus.webservice.sales.resources;

import java.util.ArrayList;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.accounts.physicalaudit.beans.GmTagBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.operations.logistics.beans.GmSetPropertyBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmSetInvRptReqVO;
import com.globus.valueobject.sales.GmSetInvRptVO;
import com.globus.webservice.common.resources.GmResource;

/**
 * This class contains methods that are related to MIM - Set Inventory Report.
 * @author Anilkumar  
 */
@Path("setinvrpt")
public class GmSetInvRptResource extends GmResource{

Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**fetchSetInvReport - This method will return the Sets Information based on Sets and Sales Hierarchy.
	 * @param strInput
	 * @return GmSetInvRptReqVO
	 * @throws AppError
	 */
	@POST
	@Path("report")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmSetInvRptReqVO fetchSetInvReport(String  strInput) throws AppError{
		log.debug("============= calling fetchSetInvReport ===============");
		GmSetInvRptReqVO gmSetInvRptReqVO = new GmSetInvRptReqVO();
		GmSetInvRptVO gmSetInvRptVO = new GmSetInvRptVO();
		
		//validate token and parse JSON into VO
		parseInputAndValidateToken(strInput, gmSetInvRptReqVO);
		
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		GmWSUtil gmWSUtil = new GmWSUtil();	
		GmAccessFilter gmAccessFilter = new GmAccessFilter();
		GmTagBean gmTagBean  = new GmTagBean();
		HashMap hmParam = new HashMap();
		ArrayList alSetInvRptVO, listSetInvRptVO= null;		
		String strSetId = "";
		String strSystem = "";
		String strDistid = "";
		String strType = "";		
		
		//Assigning all the request params into Response VO
		gmWSUtil.parseJsonToVO(strInput, gmSetInvRptReqVO);
		
		GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSetInvRptReqVO.getToken());
		//Below is the common method to retrieve the sales params.
		hmParam = GmCommonClass.parseNullHashMap((HashMap)gmProdCatUtil.populateSalesParam(gmUserVO, gmSetInvRptReqVO));		
		strSetId = GmCommonClass.parseNull((String)gmSetInvRptReqVO.getSetid());
		strSystem = GmCommonClass.parseNull((String)gmSetInvRptReqVO.getSystem());
		strDistid = GmCommonClass.parseNull((String)gmSetInvRptReqVO.getDistid());
		strType = GmCommonClass.parseNull((String)gmSetInvRptReqVO.getType());		
		
		gmSetInvRptReqVO.setCompanyid((String)hmParam.get("COMP_FILTER"));
		gmSetInvRptReqVO.setDivids((String)hmParam.get("DIV_FILTER"));
		//Below lines of code is added to open the wide access to pull all the sets. For this report we do not need access filter.
		hmParam.put("DEPT_ID","");
		//To get the Access Level filter condition information
		hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
		hmParam.put("CONDITION", (String) hmParam.get("AccessFilter"));		
		hmParam.put("SETID",strSetId);
		hmParam.put("SYSTEM",strSystem);
		hmParam.put("TYPEID",strType);		
		hmParam.put("DISTRIBUTORID",strDistid);
		
		alSetInvRptVO = GmCommonClass.parseNullArrayList(gmTagBean.fetchSetInvReport(hmParam));
		
		//Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo attributes 		
		listSetInvRptVO = (ArrayList) gmWSUtil.getVOListFromHashMapList(alSetInvRptVO, (new GmSetInvRptVO()),
																		new GmSetInvRptVO().getSetInvRptProperties());

		gmSetInvRptReqVO.setListGmSetInvRptVO(listSetInvRptVO);
		
		return gmSetInvRptReqVO;
	}
	
	/**fetchSetTissueFl - This method will return the Sets Tissue Flag.
	 * @param strInput
	 * @return strReturn
	 * @throws AppError
	 */
	  @POST
	  @Path("fetchsettfl")
	  public String fetchSetTissueFl(String strInput) {

	    
	    GmSetInvRptVO gmSetInvRptVO = new GmSetInvRptVO();   
	    GmSetPropertyBean gmSetPropertyBean = new GmSetPropertyBean();

	    // validate input
	    parseInputAndValidateToken(strInput,gmSetInvRptVO);   
	    // validate token
	    validateToken(gmSetInvRptVO.getToken());      

	    String strSetInput = GmCommonClass.parseNull((String) gmSetInvRptVO.getSetid());

	    // Call to bean, to save the request details.
	    String strReturn = GmCommonClass.parseNull((String) gmSetPropertyBean.fetchSetTissueFlList(strSetInput));
	    return strReturn;
	  }

}
