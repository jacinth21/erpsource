package com.globus.webservice.sales.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.custservice.beans.GmDOBean;
import com.globus.custservice.beans.GmDORptBean;
import com.globus.custservice.beans.GmDOTxnBean;
import com.globus.sales.beans.GmSalesMappingBean;
import com.globus.valueobject.sales.GmBookDOVO;
import com.globus.valueobject.sales.GmDORecordTagIDVO;
import com.globus.valueobject.sales.GmOrderVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.valueobject.common.GmDeviceInfoVO;  


@Path("DeliveredOrder")
public class GmDeliveredOrderResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());
  Logger logOrder = GmLogger.getInstance("globusapporder");

  /**
   * SyncDeliveredOrder - This method will Save Order Values in to Db while calling web service.
   * Needs to save information for order and while saving if there is any exception, it should be
   * sent as an E-mail.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError,Exception
   */
  @POST
  @Path("syncdo")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmOrderVO syncDeliveredOrder(String strInput) throws AppError, JsonGenerationException,
      JsonMappingException, JsonParseException, IOException, Exception {

	  
	log.debug("coming inside syncDeliveredOrder");  
   
	HashMap hmParam = new HashMap();
    HashMap hmParamStatus = new HashMap();
    String strfirstName = "";
    String strLastName = "";
    String struserName = "";
    String strRepEmailId = "";

    GmBookDOVO gmBookDOVO = new GmBookDOVO();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmOrderVO gmOrderVO = new GmOrderVO();
    // PMT-38262
    GmDeviceInfoVO gmDeviceInfoVO = new GmDeviceInfoVO();
  

    ObjectMapper mapper = new ObjectMapper();
    log.debug("DeliveredOrder input string...." + strInput);
    logOrder.error("DeliveredOrder errror input string...." + strInput);
 
    GmBookDOVO gmBookDO = mapper.readValue(strInput, GmBookDOVO.class);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmBookDO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    // Validating Token
    validateToken(gmBookDO.getToken());
    GmOrderVO[] arrGmOrderVO = gmBookDO.getArrGmOrderVO();
    gmOrderVO.setUserid(gmUserVO.getUserid());
    GmDOBean gmDoBean = new GmDOBean(gmBookDO);
    GmSalesMappingBean gmSalesMappingBean = new GmSalesMappingBean();

    // The Below Code is added to display
    GmOrderVO gmOrderparamVO = arrGmOrderVO[0];

   
    strfirstName = GmCommonClass.parseNull(gmUserVO.getFname());
    strLastName = GmCommonClass.parseNull(gmUserVO.getLname());
    struserName = strfirstName.concat(strLastName);
    hmParam.put("USERTYPE", "4121");// 4121 - Type is for Sales Rep
    hmParam.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    log.debug("the value inside USERID****" + GmCommonClass.parseNull(gmUserVO.getUserid()));
    strRepEmailId = GmCommonClass.parseNull(gmSalesMappingBean.getCSShipEmail(hmParam));

    log.debug("the value inside strRepEmailId****" + strRepEmailId);

    String strAccountId = GmCommonClass.parseNull(gmOrderparamVO.getAcctid());
    String strOrderid = GmCommonClass.parseNull(gmOrderparamVO.getOrderid());
    String strOrdertotal = GmCommonClass.parseNull(gmOrderparamVO.getTotal());
    String strSkipValidate = GmCommonClass.parseNull(gmOrderparamVO.getSkipvalidatefl());
    // hmParam.put("ACCOUNTID", gmOrderVO.getAcctid());
    // PMT-38262
    String strUserid = GmCommonClass.parseNull(gmUserVO.getUserid());
    String strUuid = GmCommonClass.parseNull(gmBookDO.getUuid());
   
  
    
    try {
      ArrayList alreturn =
          GmCommonClass.parseNullArrayList(gmDoBean.populateDOFromJSON(arrGmOrderVO, gmOrderVO));
      log.debug("the value inside array list in test*****" + alreturn);
      logOrder.error("Array list and Part details while booking DO from IPAD GmDeliveredOrderResource.syncDeliveredOrder()*****" + alreturn);
      logOrder.debug("debug Array list and Part details while booking DO from IPAD GmDeliveredOrderResource.syncDeliveredOrder()*****" + alreturn);
      int size = alreturn.size();
      hmParam = (HashMap) alreturn.get(0);
      // Getting ArrayList and sending to Save Method.
      hmParamStatus.put("SKIPVALIDATEFL", strSkipValidate);
      hmParamStatus.put("ORDERID", strOrderid);
      hmParamStatus.put("ACCOUNTID", strAccountId);
      // PMT-38262 - Put values inside hashmap
      hmParamStatus.put("USERID", strUserid);
      hmParamStatus.put("UUID", strUuid);
      gmDoBean.saveDeliveredOrder(alreturn, hmParamStatus);
    
     
      return gmOrderparamVO;
    } catch (Exception e) {
      // If any Exception Thrown,There will be an E-mail Sent.The below method is called to send an
      // Exception email
      // HashMap hmParams = new HashMap();
      log.debug("Exception == " + e.getMessage());
      hmParam.put("EXCEPTION", e.getMessage());
      hmParam.put("REPEMAIL", strRepEmailId);
      hmParam.put("REPNAME", struserName);
      hmParam.put("ACCOUNTID", strAccountId);
      hmParam.put("ORDERID", strOrderid);
      hmParam.put("TOTAL", strOrdertotal);
      gmDoBean.sendOrderExceptionEmail(hmParam);
      throw e;
    }

  }

  /**
   * fetchLatestDO - This method used to - device will call this service to get the very latest DO
   * ID for the �Account, Order Date (sysdate), Rep ID, prefix orderid� combination
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError,Exception
   */
  @POST
  @Path("getlatestdo")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmOrderVO fetchLatestDO(String strInput) throws AppError {

    GmOrderVO gmOrderVO = new GmOrderVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmOrderVO);

    GmDOBean gmDoBean = new GmDOBean();
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParam = new HashMap();
    String strLatestDo = "";

    // Assigning all the request IN param's into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmOrderVO);
    hmParam = gmWSUtil.getHashMapFromVO(gmOrderVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmOrderVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    // below fields are mandatrory, if it's not coming in input then thowing apperror
    if (GmCommonClass.parseNull(gmOrderVO.getOrderdate()).equals("")) {
      throw new AppError("", "20655", 'E');
    }
    if (GmCommonClass.parseNull(gmOrderVO.getPrefixordid()).equals("")) {
      throw new AppError("", "20656", 'E');
    }

    // below method will return latest DoId based on the input given
    strLatestDo = gmDoBean.fetchLatestDO(hmParam);
    gmOrderVO.setOrderid(strLatestDo);

    hmParam = null;
    return gmOrderVO;
  }
  
  /**
   * fetchDOTagList - This method used to get the tag list
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError,Exception
   */
  @POST
  @Path("getDOTagList")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmDORecordTagIDVO fetchDOTagList(String strInput) throws AppError {

	GmDORecordTagIDVO gmDORecordTagIDVO = new GmDORecordTagIDVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmDORecordTagIDVO);

    GmDORptBean gmDORptBean  = new GmDORptBean(gmDORecordTagIDVO);
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();

    // Assigning all the request IN param's into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmDORecordTagIDVO);
    hmParam = gmWSUtil.getHashMapFromVO(gmDORecordTagIDVO);

    hmReturn = gmDORptBean.fetchDOTagList(hmParam);
    gmDORecordTagIDVO.setTagid((String)hmReturn.get("TAGID"));
    gmDORecordTagIDVO.setSetid((String)hmReturn.get("SETID"));
    gmDORecordTagIDVO.setSetnm((String)hmReturn.get("SETNM"));

    return gmDORecordTagIDVO;
  }
  /**
   * saveDOTags - This method used to save the DO tags
   * @author ppandiyan
   * @param strInput
   * @return JSON String
   * @throws AppError,Exception
   */
  @POST
  @Path("saveDOTags")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public void saveDOTags(String strInput) throws AppError {

	GmDORecordTagIDVO gmDORecordTagIDVO = new GmDORecordTagIDVO();
    GmDOTxnBean gmDOTxnBean  = new GmDOTxnBean(gmDORecordTagIDVO);
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParam = new HashMap();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmDORecordTagIDVO);

    // Assigning all the request IN param's into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmDORecordTagIDVO);
    hmParam = gmWSUtil.getHashMapFromVO(gmDORecordTagIDVO);
    log.debug("saveDOTags****"+hmParam);
    gmDOTxnBean.saveDOTags(hmParam);

  }
  
}
