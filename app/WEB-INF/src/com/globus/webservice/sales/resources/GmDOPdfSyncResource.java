package com.globus.webservice.sales.resources;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.sales.GmSyncPDFDetailsVO;
import com.globus.valueobject.sales.GmSyncPDFInfoVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.valueobject.sales.GmSalesListVO;

@Path("DOPdf")
public class GmDOPdfSyncResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());


  /**
   * fetchLatestDO - This method used to - device will call this service to get the very latest DO
   */
  @POST
  @Path("sync")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesListVO updateDOPdfFlag(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {
    ObjectMapper mapper = new ObjectMapper();
    GmSalesListVO gmSalesListVO = new GmSalesListVO();
    log.debug("strInput::" + strInput);
    GmSyncPDFDetailsVO gmSyncPDFDetailsVO = mapper.readValue(strInput, GmSyncPDFDetailsVO.class);
    // validate token and parse JSON into VO
    validateToken(gmSyncPDFDetailsVO.getToken());
    GmSyncPDFInfoVO[] gmSyncPDFInfoVO = gmSyncPDFDetailsVO.getGmSyncPDFInfoVO();
    List<GmSyncPDFInfoVO> listgmpdf = new ArrayList<GmSyncPDFInfoVO>();
    String strCompanyID = GmCommonClass.parseNull(gmSyncPDFDetailsVO.getCmpid());
    String strCompanyLocale = GmCommonClass.getCompanyLocale(strCompanyID);
    strCompanyLocale = strCompanyLocale.equals("") ? "US" : strCompanyLocale;
    log.debug("strCompanyLocale in updateDOPdfFlag ::" + strCompanyLocale);

    String strorderID = "";
    String strpdfNM = "";
    String strpdfFL = "";
    for (int i = 0; i < gmSyncPDFInfoVO.length; i++) {
      GmSyncPDFInfoVO gmpdfparamVO = gmSyncPDFInfoVO[i];

      strorderID = gmpdfparamVO.getDoid();
      strpdfNM = gmpdfparamVO.getPdfname();
      strpdfFL = gmpdfparamVO.getPdffl();
      if (strpdfFL.equals("N")) {
        String strFilePath = GmCommonClass.getString("DOFILEPUSHLOC");
        strFilePath = strFilePath + strCompanyLocale + "\\" + strpdfNM;
        File f = new File(strFilePath);
        if (f.exists()) {
          strpdfFL = "Y";
        } else {
          strpdfFL = "N";
        }
        GmSyncPDFInfoVO gmsync = new GmSyncPDFInfoVO();
        gmsync.setDoid(strorderID);
        gmsync.setPdfname(strpdfNM);
        gmsync.setPdffl(strpdfFL);
        listgmpdf.add(gmsync);
      }
    }
    gmSalesListVO.setGmSyncPDFInfoVO(listgmpdf);
    return gmSalesListVO;
  }
}
