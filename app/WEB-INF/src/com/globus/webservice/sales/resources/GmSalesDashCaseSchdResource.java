package com.globus.webservice.sales.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.sales.CaseManagement.beans.GmCaseBookRptBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmCaseDrilldownVO;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.valueobject.sales.GmSalesDashResVO;
import com.globus.webservice.common.resources.GmResource;

/**
 * This class contains methods that are related to MSD - Case overview, Drilldown sections.
 * 
 * @author elango
 * 
 */
@Path("casescheduled")
public class GmSalesDashCaseSchdResource extends GmResource {


  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * casesCount - This method will return the Sales Dash board cases count for different case
   * levels.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("count")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO casesScheduledCount(String strInput) throws AppError {

    log.debug("=======MSD - case scheduled data Count======" + strInput);

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmParam = new HashMap();
    ArrayList alReturn = new ArrayList();
    ArrayList alScheduledCases = null;
    ArrayList alTodayCase = null;
    ArrayList alTomoCase = null;
    ArrayList alNext5dayCase = null;
    List<GmSalesDashResVO> listGmSalesDashResVO = null;
    HashMap hmScheduledCases = null;
    HashMap hmTempDetails = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));

    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
    hmParam.put("APPLDATEFMT", strApplDateFmt);
    // For cases, we need v700 sales filter condition. so here replacing it with t501 filter cond.
    hmParam.put("Condition", hmParam.get("AccessFilter"));
    alScheduledCases =
        GmCommonClass.parseNullArrayList(gmCaseBookRptBean.fetchCasesScheduled(hmParam));

    if (alScheduledCases.size() > 0) {
      hmScheduledCases = GmCommonClass.parseNullHashMap((HashMap) alScheduledCases.get(0));
      hmTempDetails = new HashMap();
      hmTempDetails.put("NAME", "Today");
      hmTempDetails.put("TOTAL",
          GmCommonClass.parseZero((String) hmScheduledCases.get("TODAY_CNT")));
      hmTempDetails.put("DRILLDOWNURL",
          GmCommonClass.parseNull(GmCommonClass.getRuleValue("CASES", "Today")));
      alReturn.add(hmTempDetails);

      hmTempDetails = new HashMap();
      hmTempDetails.put("NAME", "Tomorrow");
      hmTempDetails.put("TOTAL",
          GmCommonClass.parseZero((String) hmScheduledCases.get("TOMORROW_CNT")));
      hmTempDetails.put("DRILLDOWNURL",
          GmCommonClass.parseNull(GmCommonClass.getRuleValue("CASES", "Tomorrow")));
      alReturn.add(hmTempDetails);

      hmTempDetails = new HashMap();
      hmTempDetails.put("NAME", "In next 5 days");
      hmTempDetails.put("TOTAL",
          GmCommonClass.parseZero((String) hmScheduledCases.get("NEXTXDAY_CNT")));
      hmTempDetails.put("DRILLDOWNURL",
          GmCommonClass.parseNull(GmCommonClass.getRuleValue("CASES", "NextXDays")));
      alReturn.add(hmTempDetails);
    }


    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmSalesDashResVO =
        gmWSUtil.getVOListFromHashMapList(alReturn, gmSalesDashResVO,
            gmSalesDashResVO.getSalesDashBoardProperties());

    log.debug(" listGmSalesDashResVO " + listGmSalesDashResVO);

    gmSalesDashResVO.setListGmSalesDashResListVO(listGmSalesDashResVO);

    hmParam = null;
    alReturn = null;
    alScheduledCases = null;
    alTodayCase = null;
    alTomoCase = null;
    alNext5dayCase = null;
    listGmSalesDashResVO = null;
    hmScheduledCases = null;
    hmTempDetails = null;

    return gmSalesDashResVO;
  }

  /**
   * fetchCasesScheduled - This method will returns cases already booked based on day given.
   * 
   * @param strInput
   * @return GmSalesDashResVO
   * @throws AppError
   */
  @POST
  @Path("cases")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashResVO fetchCasesScheduled(String strInput) throws AppError {

    log.debug("=======fetch Cases Scheduled======");

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmParam = new HashMap();
    HashMap hmValidate = new HashMap();
    String strRulePassName = "";
    ArrayList alCasesSchedule, listCasesDrilldownVO = null;
    String strApplDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));

    // Assigning all the request params into Response VO
    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);

    String strPassName = GmCommonClass.parseNull(gmSalesDashReqVO.getPassname());
    hmValidate.put("passname", "20625");
    // validate strOpt,view and Filterby
    gmProdCatUtil.validateInput(gmSalesDashReqVO, hmValidate);

    if (!strPassName.equals("")) {
      strRulePassName = GmCommonClass.parseNull(GmCommonClass.getRuleValue("CASES", strPassName));
      if (strRulePassName.equals("")) {
        throw new AppError("", "20629", 'E');
      }
    }

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

    // setting CompanyId and DivisionId to ResponseVo
    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));

    // To get the Access Level information
    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));

    hmParam.put("Condition", hmParam.get("AccessFilter"));
    hmParam.put("PASSNAME", strPassName);
    hmParam.put("APPLDATEFMT", strApplDateFmt);
    alCasesSchedule =
        GmCommonClass.parseNullArrayList(gmCaseBookRptBean.fetchCasesScheduled(hmParam));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listCasesDrilldownVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alCasesSchedule, (new GmCaseDrilldownVO()),
            new GmCaseDrilldownVO().getCaseDrilldownProperties());

    gmSalesDashResVO.setListGmCaseDrilldownVO(listCasesDrilldownVO);

    hmParam = null;
    alCasesSchedule = null;
    listCasesDrilldownVO = null;

    return gmSalesDashResVO;
  }
}
