/**
 * FileName : GmSalesDashBoardCurrencyResource.java Description : This class is used to get the
 * sales dashboard currecy type details Author : Tejeswara Reddy Date : April 18, 2017 Copyright :
 * Globus Medical Inc
 */
package com.globus.webservice.sales.resources;

import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmDeviceDataStoreVO;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.webservice.common.resources.GmResource;

/**
 * 
 * @author Tejeswara Reddy
 * 
 */
@Path("salesDashBoardCurrency")
public class GmSalesDashCurrencyResource extends GmResource {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * quotaPerformance - This method will give the Sales Dashboard currency type and currency symbol.
   * 
   * @param strInput
   * @return GmSalesVO
   * @throws AppError
   */
  @POST
  @Path("currencySymbolType")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmDeviceDataStoreVO fetchCurrencySymbolType(String strInput) throws AppError {
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
    log.debug("strCompanyID in ssales" + strCompanyID);
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass
            .parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));
    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));

    String strCondition = (new GmSalesDispatchAction()).getAccessFilter(hmParam);
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    hmReturn = gmAccessFilter.getCurrSymbolByCompany(hmParam, null, strCondition, null, null, "$");

    GmDeviceDataStoreVO gmDeviceDataStoreVO = new GmDeviceDataStoreVO();
    gmDeviceDataStoreVO.setSalesdbcurrsymbol((String) hmReturn.get("CURR_SYMBOL"));
    gmDeviceDataStoreVO.setSalesdbcurrtype((String) hmReturn.get("CURR_TYPE"));
    return gmDeviceDataStoreVO;
  }
}
