package com.globus.webservice.sales.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.globus.accounts.physicalaudit.beans.GmTagBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.sales.beans.GmInvSetReqBorrowRptBean;
import com.globus.sales.beans.GmInvTagShareOptionRptBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmInvSetReqBorrowRptDetailVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.valueobject.sales.GmInvSetReqBorrowEmailVO;
import com.globus.valueobject.sales.GmInvTagShareOptDetailVO;
import com.globus.valueobject.sales.GmSetDetlVO;
import com.globus.valueobject.sales.GmReceiverEmailVO;

/**
 * This class contains methods that are related to Tag Share Option Report screen
 * 
 * @author karthik somanathan
 * 
 */
@Path("invTagShareOption")
public class GmInvTagShareOptionRptResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());
  
   /**fetchTagShareReportDetails - This method used to fetch the details of sharing report
	 * @param strInput
	 * @return String
	 * @throws AppError
	 */
	@POST
	@Path("loadTagDtl")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String fetchTagShareReportDetails(String strInput) throws AppError,
			JsonParseException, JsonMappingException, IOException {
		log.debug("fetchTagShareReportDetails strInput ==" + strInput);

		GmInvTagShareOptDetailVO gmInvTagShareOptDetailVO = new GmInvTagShareOptDetailVO();

		//validate token and parse JSON into VO
		parseInputAndValidateToken(strInput, gmInvTagShareOptDetailVO);
		
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		GmWSUtil gmWSUtil = new GmWSUtil();	
		GmAccessFilter gmAccessFilter = new GmAccessFilter();
		GmInvTagShareOptionRptBean gmInvTagShareOptionRptBean  = new GmInvTagShareOptionRptBean();
		HashMap hmParam = new HashMap();
		String strSetId = "";
		String strSystem = "";	
		
		//Assigning all the request params into Response VO
		gmWSUtil.parseJsonToVO(strInput, gmInvTagShareOptDetailVO);
		
		GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmInvTagShareOptDetailVO.getToken());
		//Below is the common method to retrieve the sales params.
		hmParam = GmCommonClass.parseNullHashMap((HashMap)gmProdCatUtil.populateSalesParam(gmUserVO, gmInvTagShareOptDetailVO));		
		strSetId = GmCommonClass.parseNull((String)gmInvTagShareOptDetailVO.getSetid());
		strSystem = GmCommonClass.parseNull((String)gmInvTagShareOptDetailVO.getSystem());
		
		gmInvTagShareOptDetailVO.setCompanyid((String)hmParam.get("COMP_FILTER"));
		gmInvTagShareOptDetailVO.setDivids((String)hmParam.get("DIV_FILTER"));
		
		//Below lines of code is added to open the wide access to pull all the sets. For this report we do not need access filter.
		hmParam.put("DEPT_ID","");
		//To get the Access Level filter condition information
		hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
		hmParam.put("CONDITION", (String) hmParam.get("AccessFilter"));		
		hmParam.put("SETID",strSetId);
		hmParam.put("SYSTEM",strSystem);

		String strReturnJSON = gmInvTagShareOptionRptBean.loadTagShareReportDetails(hmParam);
		return strReturnJSON;
	}
	
   /**updateTagShareOptions - This method used to update the tag share options from tag sharing report
	 * @param strInput
	 * @return String
	 * @throws AppError
	 */
	@POST
	@Path("updateTagShareOption")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmInvTagShareOptDetailVO updateTagShareOptions(String  strInput)throws AppError, JsonGenerationException,
    JsonMappingException, JsonParseException, IOException, Exception {
		 log.debug(" **  updateTagShareOptions strInput ** " + strInput);
		 GmInvTagShareOptDetailVO gmInvTagShareOptDetailVO = new GmInvTagShareOptDetailVO();

			//validate token and parse JSON into VO
			parseInputAndValidateToken(strInput, gmInvTagShareOptDetailVO);
			
			GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
			GmWSUtil gmWSUtil = new GmWSUtil();	
			GmInvTagShareOptionRptBean gmInvTagShareOptionRptBean  = new GmInvTagShareOptionRptBean();
			HashMap hmParam = new HashMap();
			String strTagId = "";
			String strShareOptionId = "";
			String strUserId = "";
			
			//Assigning all the request params into Response VO
			gmWSUtil.parseJsonToVO(strInput, gmInvTagShareOptDetailVO);
			GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmInvTagShareOptDetailVO.getToken());
			
		    //Below is the common method to retrieve the sales params.
			hmParam = GmCommonClass.parseNullHashMap((HashMap)gmProdCatUtil.populateSalesParam(gmUserVO, gmInvTagShareOptDetailVO));		
			strTagId = GmCommonClass.parseNull((String)gmInvTagShareOptDetailVO.getTagid());
			strShareOptionId = GmCommonClass.parseNull((String)gmInvTagShareOptDetailVO.getTagshareoptionid());
			strUserId = GmCommonClass.parseNull(gmUserVO.getUserid());
			
			hmParam.put("TAGID",strTagId);
			hmParam.put("SHAREOPTIONID",strShareOptionId);
			hmParam.put("USERID",strUserId);
	    
		    String strReturnMsg = gmInvTagShareOptionRptBean.updateTagShareOptions(hmParam);
		    gmInvTagShareOptDetailVO.setMessage(strReturnMsg);
		    
		return gmInvTagShareOptDetailVO;
	}

}
