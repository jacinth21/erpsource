package com.globus.webservice.sales.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.sales.DashBoard.beans.GmSalesDashLoanerBean;
import com.globus.sales.DashBoard.beans.GmSalesDashLoanerLateFeeBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmLoanerDrilldownVO;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.valueobject.sales.GmSalesDashResVO;
import com.globus.webservice.common.resources.GmResource;


/**
 * GmSalesDashLoanerLateFeeResources to get Loaner Late fee Details
 * @author dmishra
 *
 */
@Path("latefee")
public class GmSalesDashLoanerLateFeeResources extends GmResource {

	  /**
	 *  This the logger variable to write the log 
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	  
	  /**
	   * This Method returns the Past Month Accural Late Fee Drilldown Report Details
	 * @param strInput
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	  @POST
	  @Path("accrlatefeespastmon")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  public GmSalesDashResVO fetchLateFeeAccrStmt(String strInput) throws AppError, Exception {

	    log.debug("=======LateFee Past Month drillDown======");	    

	    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
	    // validate token and parse JSON into VO
	    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

	    GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
	    GmSalesDashLoanerLateFeeBean gmSalesDashLoanerLateFeeBean = new GmSalesDashLoanerLateFeeBean();
	    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
	    GmWSUtil gmWSUtil = new GmWSUtil();
	    GmAccessFilter gmAccessFilter = new GmAccessFilter();
	    HashMap hmParam = new HashMap();
	    HashMap hmDateRange = new HashMap();
	    ArrayList alPreLateFee, listLoanerDrilldownVO = null;
	    

	    // Assigning all the request params into Response VO
	    gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);
	    String strApplDateFmt =
		        GmCommonClass.parseNull(gmSalesDashResVO.getCmpdfmt());
	    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
	    // Below is the common method to retrieve the sales params.
	    hmParam =
	        GmCommonClass.parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));

	    // setting CompanyId and DivisionId to ResponseVo
	    gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
	    gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));

	    hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
	    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
	    // To get the Access Level filter condition information
	    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));

	    hmParam.put("APPLNDATEFMT", strApplDateFmt);
	    hmDateRange = gmSalesDashLoanerLateFeeBean.getLoanerLateFeeDateRange();
	    hmParam.put("STARTDATE", GmCommonClass.parseNull((String) hmDateRange.get("PREMONTHSTARTDATE")));
	    hmParam.put("ENDDATE", GmCommonClass.parseNull((String) hmDateRange.get("PREMONTHENDDATE")));

	    alPreLateFee = GmCommonClass.parseNullArrayList(gmSalesDashLoanerLateFeeBean.fetchLoanerAccrLateFeeDetail(hmParam));
	    
	    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
	    // attributes
	    listLoanerDrilldownVO =
	        (ArrayList) gmWSUtil.getVOListFromHashMapList(alPreLateFee, (new GmLoanerDrilldownVO()),
	            new GmLoanerDrilldownVO().getLoanerDrilldownProperties());

	    gmSalesDashResVO.setListGmLoanerDrilldownVO(listLoanerDrilldownVO);

	    hmParam = null;
	    hmDateRange = null;
	    alPreLateFee = null;
	    listLoanerDrilldownVO = null;

	    return gmSalesDashResVO;
	  }

 
	  /**
	   * This Method returns the Recent Month Accural Late Fee Drilldown Report Details
	 * @param strInput
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	  @POST
	  @Path("accrlatefeescurr")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  public GmSalesDashResVO fetchRecentLateFeeAccr(String strInput) throws AppError, Exception {

		  log.debug("=======LateFee Past Month drillDown======");	    
		  GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
		  // validate token and parse JSON into VO
		  parseInputAndValidateToken(strInput, gmSalesDashReqVO);
	
		  GmSalesDashResVO gmSalesDashResVO = new GmSalesDashResVO();
		  GmSalesDashLoanerLateFeeBean gmSalesDashLoanerLateFeeBean = new GmSalesDashLoanerLateFeeBean();
		  GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		  GmWSUtil gmWSUtil = new GmWSUtil();
		  GmAccessFilter gmAccessFilter = new GmAccessFilter();
		  HashMap hmParam = new HashMap();
		  HashMap hmDateRange = new HashMap();
		  ArrayList alLateFee, listLoanerDrilldownVO = null;
		  
		  // Assigning all the request params into Response VO
		  gmWSUtil.parseJsonToVO(strInput, gmSalesDashResVO);
		  String strApplDateFmt =
			        GmCommonClass.parseNull(gmSalesDashResVO.getCmpdfmt());
		  GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesDashReqVO.getToken());
		  // Below is the common method to retrieve the sales params.
		  hmParam =
		        GmCommonClass.parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesDashReqVO));
	
		  // setting CompanyId and DivisionId to ResponseVo
		  gmSalesDashResVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
		  gmSalesDashResVO.setDivids((String) hmParam.get("DIV_FILTER"));
	
		  hmParam.put("OVERRIDE_ACCS_LVL", hmParam.get("ACCES_LVL"));
		  hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
		  // To get the Access Level filter condition information
		  hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
	
		  hmParam.put("APPLNDATEFMT", strApplDateFmt);
		  hmDateRange = gmSalesDashLoanerLateFeeBean.getLoanerLateFeeDateRange();
		  hmParam.put("STARTDATE", GmCommonClass.parseNull((String) hmDateRange.get("CURRMONTHSTARTDATE")));
		  hmParam.put("ENDDATE", GmCommonClass.parseNull((String) hmDateRange.get("CURRMONTHENDDATE")));
	
		  alLateFee = GmCommonClass.parseNullArrayList(gmSalesDashLoanerLateFeeBean.fetchLoanerAccrLateFeeDetail(hmParam));
		  // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
		  // attributes
		  listLoanerDrilldownVO =
		        (ArrayList) gmWSUtil.getVOListFromHashMapList(alLateFee, (new GmLoanerDrilldownVO()),
		            new GmLoanerDrilldownVO().getLoanerDrilldownProperties());
	
		  gmSalesDashResVO.setListGmLoanerDrilldownVO(listLoanerDrilldownVO);
	
		  hmParam = null;
		  hmDateRange = null;
		  alLateFee = null;
		  listLoanerDrilldownVO = null;
	
		  return gmSalesDashResVO;
	  }
}

