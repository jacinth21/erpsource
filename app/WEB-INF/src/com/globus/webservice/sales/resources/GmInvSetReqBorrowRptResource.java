package com.globus.webservice.sales.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.globus.accounts.physicalaudit.beans.GmTagBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.sales.beans.GmInvSetReqBorrowRptBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmInvSetReqBorrowRptDetailVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.valueobject.sales.GmInvSetReqBorrowEmailVO;
import com.globus.valueobject.sales.GmSetDetlVO;
import com.globus.valueobject.sales.GmReceiverEmailVO;

/**
 * This class contains methods that are related to MSD - Loaner overview, Drilldown sections.
 * 
 * @author tmuthusamy
 * 
 */
@Path("InvSetReqBorrow")
public class GmInvSetReqBorrowRptResource extends GmResource {



  Logger log = GmLogger.getInstance(this.getClass().getName());
  
	@POST
	@Path("setReqBorrowDetail")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String fetchSetRequestBorrowDetails(String strInput) throws AppError,
			JsonParseException, JsonMappingException, IOException {
		log.debug("strInput" + strInput);

		GmInvSetReqBorrowRptDetailVO gmInvSetReqBorrowRptDetailVO = new GmInvSetReqBorrowRptDetailVO();

		//validate token and parse JSON into VO
		parseInputAndValidateToken(strInput, gmInvSetReqBorrowRptDetailVO);
		
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		GmWSUtil gmWSUtil = new GmWSUtil();	
		GmAccessFilter gmAccessFilter = new GmAccessFilter();
		GmInvSetReqBorrowRptBean gmInvSetReqBorrowRptBean  = new GmInvSetReqBorrowRptBean();
		HashMap hmParam = new HashMap();
		String strSetId = "";
		String strSystem = "";	
		
		//Assigning all the request params into Response VO
		gmWSUtil.parseJsonToVO(strInput, gmInvSetReqBorrowRptDetailVO);
		
		GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmInvSetReqBorrowRptDetailVO.getToken());
		//Below is the common method to retrieve the sales params.
		hmParam = GmCommonClass.parseNullHashMap((HashMap)gmProdCatUtil.populateSalesParam(gmUserVO, gmInvSetReqBorrowRptDetailVO));		
		strSetId = GmCommonClass.parseNull((String)gmInvSetReqBorrowRptDetailVO.getSetid());
		strSystem = GmCommonClass.parseNull((String)gmInvSetReqBorrowRptDetailVO.getSystem());
		
		gmInvSetReqBorrowRptDetailVO.setCompanyid((String)hmParam.get("COMP_FILTER"));
		gmInvSetReqBorrowRptDetailVO.setDivids((String)hmParam.get("DIV_FILTER"));
		//Below lines of code is added to open the wide access to pull all the sets. For this report we do not need access filter.
		hmParam.put("DEPT_ID","");
		//To get the Access Level filter condition information
		hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
		hmParam.put("CONDITION", (String) hmParam.get("AccessFilter"));		
		hmParam.put("SETID",strSetId);
		hmParam.put("SYSTEM",strSystem);

		String strReturnJSON = gmInvSetReqBorrowRptBean.loadSetRequestBorrowDetails(hmParam);
		return strReturnJSON;
	}
	
		/**sendRequestToBorrowEmail - This method used to send mail request for requestor.
	 * @param strInput
	 * @return String
	 * @throws AppError
	 */
	@POST
	@Path("sendRequestToBorrowEmail")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmInvSetReqBorrowEmailVO sendRequestToBorrowEmail(String  strInput)throws AppError, JsonGenerationException,
    JsonMappingException, JsonParseException, IOException, Exception {
		 log.debug(" **  sendRequestToBorrowEmail strInput ** " + strInput);
		 GmWSUtil gmWSUtil = new GmWSUtil();
		 HashMap hmParams = new HashMap();
		 String strReturnMsg = "";
		 ObjectMapper mapper = new ObjectMapper();
		 GmInvSetReqBorrowEmailVO gmInvSetReqBorrowEmailVO = new GmInvSetReqBorrowEmailVO();
		 
	    gmInvSetReqBorrowEmailVO = mapper.readValue(strInput, GmInvSetReqBorrowEmailVO.class);
	    validateToken(gmInvSetReqBorrowEmailVO.getToken());
	    GmInvSetReqBorrowRptBean gmInvSetReqBorrowRptBean = new GmInvSetReqBorrowRptBean(gmInvSetReqBorrowEmailVO);
	    hmParams = gmWSUtil.getHashMapFromVO(gmInvSetReqBorrowEmailVO);
	    log.debug("hmParams ========" + hmParams);
	    
	    GmReceiverEmailVO[] gmReceiverEmailVO = gmInvSetReqBorrowEmailVO.getArrReceiverEmailVO();
	    hmParams.put("GMRECEIVEREMAILVO", gmReceiverEmailVO);
	    
	    strReturnMsg = gmInvSetReqBorrowRptBean.progressSetRequestBorrowDtls(hmParams);
	    gmInvSetReqBorrowEmailVO.setStrReturnMessage(strReturnMsg);
		return gmInvSetReqBorrowEmailVO;
	}

}
