package com.globus.webservice.sales.resources;

import java.util.ArrayList;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.sales.DashBoard.beans.GmSalesDashConfigDrilldownBean;
import com.globus.valueobject.sales.GmSalesDashConfColumnVO;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.webservice.common.resources.GmResource;

/**
 * @author Manikandan
 * 
 */
@Path("configuredrilldown")
public class GmSalesDashConfigDrilldownResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchDrilldownGroup - This method will fetch the Column Name Which will be displayed in the
   * drill down for Sales Dash Board.
   * 
   * @param strInput
   * @return GmSalesDashConfColumnVO
   * @throws AppError
   */
  @POST
  @Path("ddgroup")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashConfColumnVO fetchDrilldownGroup(String strInput) throws AppError {

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashConfColumnVO gmsalesdashconfcolumnvo = new GmSalesDashConfColumnVO();
    GmSalesDashConfigDrilldownBean gmsalesdashconfigdrilldownbean =
        new GmSalesDashConfigDrilldownBean();
    HashMap hmParam = new HashMap();
    ArrayList alConfigddgrp, listGmSalesDashConfColumnVO = null;

    alConfigddgrp =
        GmCommonClass.parseNullArrayList(gmsalesdashconfigdrilldownbean
            .fetchDrilldownGroup(hmParam));
    listGmSalesDashConfColumnVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alConfigddgrp, gmsalesdashconfcolumnvo,
            gmsalesdashconfcolumnvo.getDrilldowngroupProperties());

    gmsalesdashconfcolumnvo.setListGmSalesDashConfColumnVO(listGmSalesDashConfColumnVO);

    hmParam = null;
    alConfigddgrp = null;
    listGmSalesDashConfColumnVO = null;

    return gmsalesdashconfcolumnvo;
  }


  /**
   * fetchDrilldownGroup - This method will fetch the Details Of The Column Name Which will be
   * displayed in the drill down for Sales Dash Board.
   * 
   * @param strInput
   * @return GmSalesDashConfColumnVO
   * @throws AppError
   */
  @POST
  @Path("dddetails")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesDashConfColumnVO fetchDrilldownDetails(String strInput) throws AppError {

    GmSalesDashReqVO gmSalesDashReqVO = new GmSalesDashReqVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesDashReqVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmSalesDashReqVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    GmSalesDashConfColumnVO gmsalesdashconfcolumnvo = new GmSalesDashConfColumnVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSalesDashConfigDrilldownBean gmsalesdashconfigdrilldownbean =
        new GmSalesDashConfigDrilldownBean();
    HashMap hmParam = new HashMap();
    ArrayList alConfigdddetails, listGmSalesDashConfColumnVO = null;

    String strPassName = "";
    strPassName = GmCommonClass.parseNull(gmSalesDashReqVO.getPassname());
    hmParam.put("PASSNAME", strPassName);
    alConfigdddetails =
        GmCommonClass.parseNullArrayList(gmsalesdashconfigdrilldownbean
            .fetchDrilldownDetails(hmParam));
    listGmSalesDashConfColumnVO =
        (ArrayList) gmWSUtil.getVOListFromHashMapList(alConfigdddetails, gmsalesdashconfcolumnvo,
            gmsalesdashconfcolumnvo.getDrilldowndetailsProperties());

    gmsalesdashconfcolumnvo.setListGmSalesDashConfColumnVO(listGmSalesDashConfColumnVO);

    hmParam = null;
    alConfigdddetails = null;
    listGmSalesDashConfColumnVO = null;

    return gmsalesdashconfcolumnvo;
  }
}
