package com.globus.webservice.sales.resources;


import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.sales.Loaners.beans.GmLoanerExtBean;
import com.globus.valueobject.sales.GmLoanerDrilldownVO;
import com.globus.valueobject.sales.GmLoanerExtVO;
import com.globus.webservice.common.resources.GmResource;

@Path("loanerext")
public class GmSalesDashLoanerExtResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * saveloanerextension - This method is used to save loaner extension from Device.
   * 
   * @param strInput
   * @return void
   * @throws AppError
   */
  @POST
  @Path("saveloanerext")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmLoanerDrilldownVO saveloanerextension(String strInput) throws AppError, IOException,
      JsonGenerationException, JsonMappingException, JsonParseException, ParseException {

    GmLoanerExtVO gmLoanerExtVO = new GmLoanerExtVO();

    GmLoanerDrilldownVO gmLoanerDrilldownVO = new GmLoanerDrilldownVO();

    ObjectMapper mapper = new ObjectMapper();
    gmLoanerDrilldownVO = mapper.readValue(strInput, GmLoanerDrilldownVO.class);

    // parseInputAndValidateToken(strInput, gmLoanerExtVO);
    validateToken(gmLoanerDrilldownVO.getToken());
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmLoanerDrilldownVO.getCmpid());
   /* if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }*/

    HashMap hmParam = new HashMap();
    GmWSUtil gmWSUtil = new GmWSUtil();
    gmLoanerDrilldownVO.setUserid(gmUserVO.getUserid());
    hmParam = gmWSUtil.getHashMapFromVO(gmLoanerDrilldownVO);

    // Convert email VOs to DB input String
    GmLoanerExtVO[] arrGmLoanerExtVO = gmLoanerDrilldownVO.getGmLoanerExtVO();
    String strInputStr = "";

    for (int i = 0; i < arrGmLoanerExtVO.length; i++) {
      gmLoanerExtVO = arrGmLoanerExtVO[i];
      strInputStr = strInputStr + gmLoanerExtVO;
    }

    hmParam.put("EXTENDSTRING", strInputStr);
    log.debug("=======  hmParam======" + hmParam);
    GmLoanerExtBean gmLoanerExtBean = new GmLoanerExtBean(gmLoanerDrilldownVO);
    gmLoanerExtBean.saveLoanerExt(hmParam);
    return gmLoanerDrilldownVO;

  }
}
