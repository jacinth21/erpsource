package com.globus.webservice.sales.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.logon.beans.GmLogonBean;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmCompanyVO;
import com.globus.valueobject.sales.GmFieldSalesVO;
import com.globus.valueobject.sales.GmSalesFilterVO;
import com.globus.valueobject.sales.GmSlsDivisionVO;
import com.globus.valueobject.sales.GmSlsRegionVO;
import com.globus.valueobject.sales.GmSlsZoneVO;
import com.globus.webservice.common.resources.GmResource;

/**
 * @author Rajeshwaran
 * 
 */
@Path("salesfilter")
public class GmSalesFilterResource extends GmResource {


  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchSalesFilters - This method will return the Sales Filter. From the Input get the User ID.
   * Call the after that it will returns updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @POST
  @Path("morefilter")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmSalesFilterVO fetchSalesFilters(String strInput) throws AppError {
    log.debug("=======More filters======");

    GmSalesFilterVO gmSalesFilterVO = new GmSalesFilterVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmSalesFilterVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmSalesFilterVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }

    GmLogonBean gmLogonBean = new GmLogonBean();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmParam = new HashMap();
    HashMap hmSalesFilters = null;
    String strAccessLvlID = "";
    String strNeedMoreFilter = "";
    String strFilterCondition = "";
    String strPassName = "";

    ArrayList alTemp = null;
    ArrayList alCompany = null;
    ArrayList alDivision = null;
    ArrayList alZone = null;
    ArrayList alRegion = null;
    ArrayList alFieldSales = null;
    List listGmCompanyVO = null;
    List listGmSlsDivisionVO = null;
    List listGmSlsZoneVO = null;
    List listGmSlsRegionVO = null;
    List listGmFieldSalesVO = null;

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmSalesFilterVO.getToken());
    // Below is the common method to retrieve the sales params.
    hmParam =
        GmCommonClass.parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, gmSalesFilterVO));

    // If the company id is not passing from input, we have to pass default COMP,DIV ID's based on
    // logged in User ID.
    gmSalesFilterVO.setCompanyid((String) hmParam.get("COMP_FILTER"));
    gmSalesFilterVO.setDivids((String) hmParam.get("DIV_FILTER"));

    strAccessLvlID = GmCommonClass.parseNull((String) hmParam.get("ACCES_LVL"));
    strNeedMoreFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strAccessLvlID, "SALES_DASH"));
    strNeedMoreFilter = "YES";
    strPassName = GmCommonClass.parseNull(gmSalesFilterVO.getPassname());
    // Below lines of code is added to open the wide access to pull all the sets. For this report we
    // do not need access filter.
    if (strPassName.equals("1")) {
      hmParam.put("DEPT_ID", "");
    }
    // Based on the Access Level check if the More Filters is required.
    if (strNeedMoreFilter.equalsIgnoreCase("YES")) {

      strFilterCondition = (new GmSalesDispatchAction()).getAccessFilter(hmParam);
      hmSalesFilters = (new GmCommonBean()).getSalesFilterLists(strFilterCondition);

      alCompany = (ArrayList) hmSalesFilters.get("COMP");
      alDivision = (ArrayList) hmSalesFilters.get("DIV");
      alZone = (ArrayList) hmSalesFilters.get("ZONEBYCOMPDIV");
      alRegion = (ArrayList) hmSalesFilters.get("REGNBYZONE");
      alFieldSales = (ArrayList) hmSalesFilters.get("DIST");

      // Removing Bone Bank Allograft(4000643) from Company list in More Filter
      for (int i = 0; i < alCompany.size(); i++) {
        HashMap hmTemp = GmCommonClass.parseNullHashMap((HashMap) alCompany.get(i));
        String strCodeId = GmCommonClass.parseNull((String) hmTemp.get("CODEID"));

        if (strCodeId.equals("4000643")) {
          alCompany.remove(i);
        }
      }

      // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
      // attributes
      listGmCompanyVO =
          gmWSUtil.getVOListFromHashMapList(alCompany, (new GmCompanyVO()),
              new GmCompanyVO().getCompanyProperties());

      listGmSlsDivisionVO =
          gmWSUtil.getVOListFromHashMapList(alDivision, (new GmSlsDivisionVO()),
              new GmSlsDivisionVO().getDivisionProperties());

      listGmSlsZoneVO =
          gmWSUtil.getVOListFromHashMapList(alZone, (new GmSlsZoneVO()),
              new GmSlsZoneVO().getZoneProperties());

      listGmSlsRegionVO =
          gmWSUtil.getVOListFromHashMapList(alRegion, (new GmSlsRegionVO()),
              new GmSlsRegionVO().getRegionProperties());

      listGmFieldSalesVO =
          gmWSUtil.getVOListFromHashMapList(alFieldSales, (new GmFieldSalesVO()),
              new GmFieldSalesVO().getFieldSalesProperties());

      gmSalesFilterVO.setListGmCompanyListVO(listGmCompanyVO);
      gmSalesFilterVO.setListGmSlsDivisionListVO(listGmSlsDivisionVO);
      gmSalesFilterVO.setListGmSlsZoneListVO(listGmSlsZoneVO);
      gmSalesFilterVO.setListGmSlsRegionListVO(listGmSlsRegionVO);
      gmSalesFilterVO.setListGmFieldSalesListVO(listGmFieldSalesVO);

    } else {
      gmSalesFilterVO = null;
    }



    return gmSalesFilterVO;


  }

}
