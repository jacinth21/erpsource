package com.globus.webservice.sales.kit.bean;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

import oracle.jdbc.driver.OracleTypes;
public class GmKitMappingReportBean extends GmBean{
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * Constructor will populate company info.
	 * 
	 * @param gmDataStore
	 */
	public GmKitMappingReportBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	}

	/**
	 * fetchKitNameAvailability - This method is used to validate the kit name
	 * 
	 * @param strPartyId
	 *            , strKitName
	 * @exception AppError
	 */
	public String fetchKitNameAvailability(String strPartyId, String strKitName)
			throws AppError {

		log.debug("fetchKitNameAvailability :" + strPartyId + " strKitName"+ strKitName);

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_kit_mapping_rpt.gm_fch_kit_nm_avl", 3);
		gmDBManager.setString(1, strPartyId);
		gmDBManager.setString(2, strKitName);
		gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
		gmDBManager.execute();

		String strResults = GmCommonClass.parseNull(gmDBManager.getString(3));
		log.debug("strResults==>" + strResults);
		gmDBManager.close();

		return strResults;
	}

	/**
	 * fetchKitMapping - This method is used to fetch the kit details
	 * 
	 * @param strKitId
	 * @return String Json data
	 * @exception AppError
	 */
	public String fetchKitMapping(String strKitId, String strCmpId,
			String strPartyId, String strCompFmt) throws AppError {

		log.debug("fetchKitMapping :" + strKitId + "strPartyId==>" + strPartyId+ "strCompFmt==> "+strCompFmt);
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_kit_mapping_rpt.gm_fch_kit_dtl", 5);
		gmDBManager.setString(1, strKitId);
		gmDBManager.setString(2, strCmpId);
		gmDBManager.setString(3, strPartyId);
		gmDBManager.setString(4, strCompFmt);
		gmDBManager.registerOutParameter(5, OracleTypes.CLOB);
		gmDBManager.execute();

		String strKitDetails = GmCommonClass.parseNull(gmDBManager.getString(5));
		gmDBManager.close();

		return strKitDetails;
	}

	/**
	 * fetchkitReports - This method is used to fetch the kit details to show
	 * report
	 * 
	 * @param hmInput
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList fetchkitReports(HashMap hmInput) throws AppError {

		log.debug("fetchKitreport report hash :" + hmInput);

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		ArrayList alReturn = new ArrayList();

		String repid = GmCommonClass.parseNull((String) hmInput.get("REPID"));
		String partyid = GmCommonClass.parseNull((String) hmInput.get("PARTYID"));
		String kitname = GmCommonClass.parseNull((String) hmInput.get("KITNM"));
		String stropt = GmCommonClass.parseNull((String) hmInput.get("STROPT"));

		StringBuffer sbQuery = new StringBuffer();

		sbQuery.append("SELECT   KITID,KITNAME, sum(NVL(KITSET,0)) KITSET,sum(NVL(KITPART,0)) KITPART from (");
		sbQuery.append("Select  T2078.C2078_KIT_MAP_ID KITID, T2078.C2078_KIT_NM KITNAME,to_char(count(T2079.C207_SET_ID)) KITSET,null KITPART ");
		sbQuery.append("from t2079_kit_map_details T2079, T2078_KIT_MAPPING T2078 ");
		if (!stropt.equals("mykit"))
			sbQuery.append(", T2079B_KIT_REP_MAP T2079B ");

		sbQuery.append("where T2078.C2078_KIT_MAP_ID = T2079.C2078_KIT_MAP_ID AND t2078.C2078_ACTIVE_FL='Y' AND t2078.c901_type = '107921' and t2079.c2079_void_fl is null ");
		
		if (!stropt.equals("mykit"))
			sbQuery.append(" AND C2079B_VOID_FL is null ");
		
		if (!kitname.equals(""))
			sbQuery.append(" and lower(C2078_KIT_NM) like lower('%" + kitname+ "%')");

		if (stropt.equals("mykit"))
			sbQuery.append("AND T2078.C2078_OWNER_PARTY_ID ='" + partyid + "'");
		else if (stropt.equals("sharewme")) {
			sbQuery.append(" AND T2079B.C2078_KIT_MAP_ID = T2078.C2078_KIT_MAP_ID");
			sbQuery.append(" AND T2079B.C2079B_REP_PARTY_ID  = '" + partyid+ "'");
		}

		sbQuery.append(" GROUP BY T2078.C2078_KIT_NM, T2078.C2078_KIT_MAP_ID ");
		sbQuery.append("UNION ");
		sbQuery.append("select  T2078.C2078_KIT_MAP_ID KITID, T2078.C2078_KIT_NM KITNAME,null KITSET,to_char(COUNT(T2079A.C205_PART_NUMBER_ID)) KITPART ");
		sbQuery.append("from T2079A_KIT_PART_MAP_DTL T2079A,T2078_KIT_MAPPING T2078 ");
		if (!stropt.equals("mykit"))
			sbQuery.append(", T2079B_KIT_REP_MAP T2079B ");

		sbQuery.append("WHERE T2078.C2078_KIT_MAP_ID  = T2079A.C2078_KIT_MAP_ID AND t2078.C2078_ACTIVE_FL='Y'  AND t2078.c901_type = '107921' and T2079A.C2079A_void_fl is null ");
		if (!stropt.equals("mykit"))
			sbQuery.append(" AND C2079B_VOID_FL is null ");

		if (!kitname.equals(""))
			sbQuery.append(" and lower(C2078_KIT_NM) like lower('%" + kitname+ "%')");

		if (stropt.equals("mykit"))
			sbQuery.append("AND T2078.C2078_OWNER_PARTY_ID ='" + partyid + "'");
		else if (stropt.equals("sharewme")) {
			sbQuery.append(" AND T2079B.C2078_KIT_MAP_ID = T2078.C2078_KIT_MAP_ID");
			sbQuery.append(" AND T2079B.C2079B_REP_PARTY_ID  = '" + partyid+ "'");
		}

		sbQuery.append(" GROUP BY T2078.C2078_KIT_NM, T2078.C2078_KIT_MAP_ID) ");
		sbQuery.append("group by KITID,KITNAME ");
		sbQuery.append("order by upper(KITNAME) ");

		log.debug("fetchkitReports " + sbQuery);
		alReturn = GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

		return alReturn;

	}

	/**
	 * fetchKitinActiveReports - This method is used to fetch the inactive kit
	 * details to show report
	 * 
	 * @param hmInput
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList fetchKitinActiveReports(HashMap hmInput) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		ArrayList alReturn = new ArrayList();

		String statusfl = GmCommonClass.parseNull((String) hmInput.get("VIODFL"));
		String repid = GmCommonClass.parseNull((String) hmInput.get("REPID"));
		String partyid = GmCommonClass.parseNull((String) hmInput.get("PARTYID"));
		String kitname = GmCommonClass.parseNull((String) hmInput.get("KITNM"));
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" select C2078_KIT_MAP_ID KITID,C2078_KIT_NM KITNM,C2078_DEACTIVATE_DATE LSDATE  from T2078_KIT_MAPPING T2078 where T2078.C2078_ACTIVE_FL = 'N' ");
		sbQuery.append(" and T2078.C2078_VOID_FL is null and T2078.C2078_OWNER_PARTY_ID = '"+ partyid + "'");

		if (!kitname.equals(""))
			sbQuery.append(" and lower(C2078_KIT_NM) like lower('%" + kitname+ "%')");

		sbQuery.append(" order by C2078_DEACTIVATE_DATE DESC ");
		log.debug("fetchKitinActiveReports " + sbQuery.toString());

		alReturn = GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

		return alReturn;
	}

	/**
	 * fetchPartDetails - This method is used to fetch the part details based on
	 * set id when P icon clicked
	 * 
	 * @param strSetId
	 * @return String Json data
	 * @exception AppError
	 */
	public String fetchPartDetails(String strSetId) throws AppError {

		log.debug("fetchPartDetails: " + strSetId);
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_kit_mapping_rpt.gm_fch_part_dtl",2);
		gmDBManager.setString(1, strSetId);
		gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
		gmDBManager.execute();

		String strPartDetails = GmCommonClass.parseNull(gmDBManager.getString(2));
		gmDBManager.close();

		return strPartDetails;
	}

	/**
	 * deactivateKit - This method is used to active and deactive the kit
	 * 
	 * @param strSetId
	 * @return String Json data
	 * @exception AppError
	 */
	public ArrayList saveKitActiveSts(HashMap hmInput) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		ArrayList alReturn = new ArrayList();

		String strKitid = GmCommonClass.parseNull((String) hmInput.get("KITID"));
		String strRepid = GmCommonClass.parseNull((String) hmInput.get("REPID"));
		String strOpt = GmCommonClass.parseNull((String) hmInput.get("STROPT"));
		String strActfl = GmCommonClass.parseNull((String) hmInput.get("ACTIVEFL"));
		log.debug("strActfl = "+strActfl);

		gmDBManager.setPrepareString("gm_pkg_kit_mapping_rpt.gm_sav_kit_toggle", 3);
		gmDBManager.setString(1, GmCommonClass.parseNull(strKitid));
		gmDBManager.setString(2, GmCommonClass.parseNull(strRepid));
		gmDBManager.setString(3, GmCommonClass.parseNull(strActfl));
		gmDBManager.execute();
		gmDBManager.commit();
		return alReturn;
	}
		      
	/**
	 * validate - This method is used to validate the tag id based on loaner
	 * 
	 * @param tagId
	 * @return String
	 * @exception AppError
	 */
	public String validTag(HashMap hmInput) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		ArrayList alReturn = new ArrayList();

		String tagid = GmCommonClass.parseNull((String) hmInput.get("TAGID"));
		String repid = GmCommonClass.parseNull((String) hmInput.get("REPID"));

		gmDBManager.setPrepareString("gm_pkg_kit_mapping_rpt.gm_fch_tag_validate", 2);
		gmDBManager.setString(1, GmCommonClass.parseNull(tagid));
		gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
		gmDBManager.execute();

		String strResults = GmCommonClass.parseNull(gmDBManager.getString(2));
		gmDBManager.close();
		log.debug("strResults>>>>>>" + strResults);
		return strResults;
	}

	/**
	 * getDistributorID - This method is used to get the distributor id based on
	 * sales rep
	 * 
	 * @param strUserId
	 * @return String
	 * @exception AppError
	 */
	public String getDistributorID(String strUserId) throws AppError {
		log.debug("strUserId==>" + strUserId);
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strDId = "";

		StringBuffer sbQuery = new StringBuffer();
		gmDBManager.setFunctionString("gm_pkg_kit_mapping_rpt.get_distributor_id", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();

		strDId = GmCommonClass.parseNull((String) gmDBManager.getString(1));
		gmDBManager.close();

		return strDId;
	}
	
	/**
	 * fetchSharedBymeReports - This method is used to fetch the kit fetch
	 * Shared By me Report
	 * 
	 * @param hmInput
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList fetchSharedBymeReports(HashMap hmInput) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		ArrayList alReturn = new ArrayList();

		String repid = GmCommonClass.parseNull((String) hmInput.get("REPID"));
		String partyid = GmCommonClass.parseNull((String) hmInput.get("PARTYID"));
		String kitname = GmCommonClass.parseNull((String) hmInput.get("KITNM"));
		String stropt = GmCommonClass.parseNull((String) hmInput.get("STROPT"));

		StringBuffer sbQuery = new StringBuffer();

		sbQuery.append(" SELECT KITID, KITNAME, SUM(NVL(KITSET,0)) KITSET ,  SUM(NVL(KITPART,0)) KITPART FROM(");
		sbQuery.append(" SELECT KITPART.KITID, KITPART.KITNAME,TO_CHAR(COUNT(T2079A.C205_PART_NUMBER_ID)) KITPART, NULL KITSET FROM T2079A_KIT_PART_MAP_DTL T2079A, ");
		sbQuery.append(" (SELECT DISTINCT T2079B.C2078_KIT_MAP_ID KITID, T2078.C2078_KIT_NM KITNAME  FROM T2079B_KIT_REP_MAP T2079B, T2078_KIT_MAPPING T2078 WHERE T2078.C2078_KIT_MAP_ID  = T2079B.C2078_KIT_MAP_ID ");
		sbQuery.append(" AND T2079B.C2079B_REP_PARTY_ID !=" + partyid
				+ "  AND T2078.C2078_OWNER_PARTY_ID  = " + partyid
				+ "  AND T2078.C2078_VOID_FL  IS NULL ");
		sbQuery.append("AND T2079B.C2079B_VOID_FL IS NULL AND t2078.C2078_ACTIVE_FL ='Y' ");
		if (!kitname.equals(""))
			sbQuery.append(" and lower(C2078_KIT_NM) like lower('%" + kitname
					+ "%')");
		sbQuery.append(" )KITPART WHERE KITPART.KITID = t2079a.c2078_kit_map_id AND t2079a.c2079a_void_fl IS NULL ");
		sbQuery.append(" GROUP BY KITPART.KITID, KITPART.KITNAME ");
		sbQuery.append(" UNION ");
		sbQuery.append(" SELECT KITSHARE.KITID, KITSHARE.KITNAME, NULL KITPART,  TO_CHAR(COUNT(T2079.C207_SET_ID)) KITSET ");
		sbQuery.append(" FROM t2079_kit_map_details T2079, (SELECT DISTINCT T2079B.C2078_KIT_MAP_ID KITID , T2078.C2078_KIT_NM KITNAME ");
		sbQuery.append(" FROM T2079B_KIT_REP_MAP T2079B, T2078_KIT_MAPPING T2078 WHERE T2078.C2078_KIT_MAP_ID = T2079B.C2078_KIT_MAP_ID ");
		sbQuery.append(" AND T2079B.C2079B_REP_PARTY_ID !=" + partyid
				+ " AND T2078.C2078_OWNER_PARTY_ID  =" + partyid + " ");
		sbQuery.append(" AND T2078.C2078_VOID_FL IS NULL  AND T2079B.C2079B_VOID_FL IS NULL AND t2078.C2078_ACTIVE_FL='Y' ");
		if (!kitname.equals(""))
			sbQuery.append(" and lower(C2078_KIT_NM) like lower('%" + kitname
					+ "%')");
		sbQuery.append(" )KITSHARE WHERE KITSHARE.KITID = t2079.c2078_kit_map_id AND t2079.c2079_void_fl IS NULL");
		sbQuery.append(" GROUP BY KITSHARE.KITID, KITSHARE.KITNAME ) GROUP BY KITID, KITNAME ORDER BY UPPER(KITNAME)");

		log.debug("fetchSharedBymeReports " + sbQuery);
		alReturn = GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

		return alReturn;

	}
	/**
	 * fetchSetDetails - This method will be used fetch the set details based on
	 * refid
	 * 
	 * @param strRefId
	 * @exception AppError
	 */
	public HashMap fetchSetDetails(String strRefId, String strUserId)throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_kit_mapping_rpt.gm_fch_set_id", 3);
		gmDBManager.setString(1, strRefId);
		gmDBManager.setString(2, strUserId);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();
		HashMap hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		return hmReturn;
	}

	/**
	 * fetchTagDetails - This method will be used fetch the tag details based on
	 * refid
	 * 
	 * @param strRefId
	 * @exception AppError
	 */
	public HashMap fetchTagDetails(String strRefId, String strUserId)
			throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_kit_mapping_rpt.gm_fch_teg_set_id", 3);
		gmDBManager.setString(1, strRefId);
		gmDBManager.setString(2, strUserId);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();
		HashMap hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		return hmReturn;
	}

	/**
	 * fetchPartDetails - This method will fetch the part num details based on part num
	 * 
	 * @param String strPartNum, strCompId
	 * @return HashMap
	 * @exception AppError
	 */
	public HashMap fetchPartDetails(String strPartNum, String strCompId)throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_kit_mapping_rpt.gm_fch_part_num",3);
		gmDBManager.setString(1, strPartNum);
		gmDBManager.setString(2, strCompId);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();
		HashMap hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		return hmReturn;
	}
}
