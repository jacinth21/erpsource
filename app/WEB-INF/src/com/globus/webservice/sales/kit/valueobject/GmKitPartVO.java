package com.globus.webservice.sales.kit.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class GmKitPartVO extends GmDataStoreVO{
	private String partnum = "";
	private String partdesc = "";
	private String qty = "";
	private String kitid = "";
	private String voidfl = "";
	
	public String getPartnum() {
		return partnum;
	}
	public void setPartnum(String partnum) {
		this.partnum = partnum;
	}
	public String getPartdesc() {
		return partdesc;
	}
	public void setPartdesc(String partdesc) {
		this.partdesc = partdesc;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getKitid() {
		return kitid;
	}
	public void setKitid(String kitid) {
		this.kitid = kitid;
	}
	public String getVoidfl() {
		return voidfl;
	}
	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}
	
}
