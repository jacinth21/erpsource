package com.globus.webservice.sales.kit.resources;

import java.io.IOException;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmAutoCompleteTransBean;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.operations.logistics.beans.GmKitMappingBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.sales.GmCompanyVO;
import com.globus.webservice.common.bean.GmMasterRedisTransBean;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.valueobject.GmCRMPSCaseVO;
import com.globus.webservice.sales.kit.valueobject.GmKitInputsVO;
import com.globus.webservice.sales.kit.bean.*;
import com.globus.webservice.sales.casemanagement.bean.*;
import com.globus.webservice.sales.casemanagement.valueobject.GmCaseVO;

@Path("kit")
public class GmKitResource extends GmResource {
	  Logger log = GmLogger.getInstance(this.getClass().getName());
/**
  * This method used to Validate the kit name based on kit name and party id
  * object in front-end as a JSONDATA.
  */  
      @POST
	  @Path("validatekitnm")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  
	  public GmKitInputsVO validateKitName(String strInput) throws AppError, JsonGenerationException,
	      JsonMappingException, JsonParseException, IOException, Exception {
		  
	    log.debug("strInput=>"+strInput);
	    ObjectMapper mapper = new ObjectMapper();
	    String strPartyId = "";
	    String strKitName = "";
	    String strKitNameavl = "";
	 
	    GmKitInputsVO gmKitInputsVO = mapper.readValue(strInput, GmKitInputsVO.class);	 
	    validateToken(gmKitInputsVO.getToken());
	    GmKitMappingReportBean gmKitMappingReportBean = new GmKitMappingReportBean(gmKitInputsVO);
	    
	    strPartyId = GmCommonClass.parseNull((String) gmKitInputsVO.getPartyid());
	    strKitName = GmCommonClass.parseNull((String) gmKitInputsVO.getKitnm());
	    strKitNameavl = gmKitMappingReportBean.fetchKitNameAvailability(strPartyId,strKitName);
	    gmKitInputsVO.setKitNameCheck(strKitNameavl);
	    log.debug("strKitNameavl=>"+strKitNameavl);
	    return gmKitInputsVO;	    
	  }
      
/**
  * This method used to save and edit kit details 
  * object in front-end as a JSONDATA.
  */
	  @POST
	  @Path("save")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  
	  public GmKitInputsVO saveKitMapping(String strInput) throws AppError, JsonGenerationException,
	      JsonMappingException, JsonParseException, IOException, Exception {
		  
		ObjectMapper mapper = new ObjectMapper();
		String strKitId = "";
		String strKitType="";
		String strActiveFl="";
		String strUserId = "";
		HashMap hmParam = new HashMap();
	    log.debug("strInput->"+strInput);
	    GmKitInputsVO gmKitInputsVO = mapper.readValue(strInput, GmKitInputsVO.class);
	    GmKitMappingWrapperBean gmKitMappingWrapperBean = new GmKitMappingWrapperBean(gmKitInputsVO);
	  
	    validateToken(gmKitInputsVO.getToken());	    
	    strUserId = gmKitInputsVO.getUserid();
	    hmParam = gmKitMappingWrapperBean.saveKitMapping(strInput,strUserId);
	    strKitId =GmCommonClass.parseNull((String)hmParam.get("KITID"));
	    
	    strKitType =GmCommonClass.parseNull((String)hmParam.get("KITTYPE"));
	    String strcmpId = GmCommonClass.parseNull((String)gmKitInputsVO.getCmpid());
	    String strKitNm = GmCommonClass.parseNull((String)gmKitInputsVO.getKitnm());
		strActiveFl = GmCommonClass.parseNull((String)gmKitInputsVO.getActivefl());
	    String strCompanyInfo ="{\"cmpid\":\""+strcmpId+"\",\"partyid\":\""+gmKitInputsVO.getPartyid()+"\",\"plantid\":\""+gmKitInputsVO.getPlantid()+"\"}";
	    syncKitNameList(strKitId,strKitId,strKitNm,"syncKitNameList",strCompanyInfo,gmKitInputsVO.getPartyid(),gmKitInputsVO,strKitType,strActiveFl);
	    
	    gmKitInputsVO.setKitid(strKitId);
	    log.debug("strKitId==>>"+strKitId);
	
	    return gmKitInputsVO;	    
	  } 

  /**
    * This method used to fetch the kit details based on kit id
    * object in front-end as a JSONDATA.
    */	  
	  @POST
	  @Path("fetchkit")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  
	  public String fetchKitMapping(String strInput) throws AppError, JsonGenerationException,
	      JsonMappingException, JsonParseException, IOException, Exception {
		  
		ObjectMapper mapper = new ObjectMapper();
	    log.debug("strInput-->"+strInput);
	    String strKitId = "";
	    String strKitDtlJSON = "";
	    String strCmpId = "";
	    String strPartyId = "";
	    String strCompFmt = "";
	    
	    GmKitInputsVO gmKitInputsVO = mapper.readValue(strInput, GmKitInputsVO.class);
	    validateToken(gmKitInputsVO.getToken());
	    GmKitMappingReportBean gmKitMappingReportBean = new GmKitMappingReportBean(gmKitInputsVO);
	    
	    strKitId = GmCommonClass.parseNull((String)gmKitInputsVO.getKitid());
	    strCmpId = GmCommonClass.parseNull((String)gmKitInputsVO.getCmpid());
	    strPartyId = GmCommonClass.parseNull((String)gmKitInputsVO.getPartyid());
	    strCompFmt = GmCommonClass.parseNull((String)gmKitInputsVO.getCmpdfmt());
	    strKitDtlJSON = gmKitMappingReportBean.fetchKitMapping(strKitId,strCmpId,strPartyId, strCompFmt);
	    log.debug("strKitDtlJSON-->"+strKitDtlJSON);
	    return strKitDtlJSON;
	    
	  } 
 /**
   * This method used to fetch the part details based on set id when P icon clicked
   * object in front-end as a JSONDATA.
   */	  
   	  @POST
   	  @Path("fetchpart")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  
	  public String fetchPartDetails(String strInput) throws AppError, JsonGenerationException,
	      JsonMappingException, JsonParseException, IOException, Exception {
   		  
   		String strPartDtls = "";
		ObjectMapper mapper = new ObjectMapper();
	    GmKitInputsVO gmKitInputsVO = mapper.readValue(strInput, GmKitInputsVO.class);
	    validateToken(gmKitInputsVO.getToken());
		GmKitMappingReportBean gmKitMappingReportBean = new GmKitMappingReportBean(gmKitInputsVO);
    
	    String strSetID = GmCommonClass.parseNull((String)gmKitInputsVO.getSetid());
	    strPartDtls = gmKitMappingReportBean.fetchPartDetails(strSetID);
	    
	    return strPartDtls;	    
	  } 
/**
  * This method used to fetch the kit report details based on user id
  *
  */	
   	 @POST
	 @Path("report")
	 @Produces({MediaType.APPLICATION_JSON})
	 @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  
	 public List<GmKitInputsVO> fetchkitReports(String strInput) throws AppError, JsonGenerationException,
	      JsonMappingException, JsonParseException, IOException, Exception {
		  
	    List<GmKitInputsVO> reportData =   new ArrayList<GmKitInputsVO>();
	    GmWSUtil gmWSUtil = new GmWSUtil();
	    GmKitInputsVO gmKitInputsVO = new GmKitInputsVO();
	    gmWSUtil.parseJsonToVO(strInput, gmKitInputsVO);
	    validateToken(gmKitInputsVO.getToken());
	    HashMap hmInput = gmWSUtil.getHashMapFromVO(gmKitInputsVO);
	    String strUserId = gmKitInputsVO.getUserid();
	    String strOpt = gmKitInputsVO.getStropt();
	    
	    GmKitMappingReportBean gmKitMappingReportBean = new GmKitMappingReportBean(gmKitInputsVO);
    
	    if(strOpt.equals("inactiveKit")) {
	    	reportData = gmKitMappingReportBean.fetchKitinActiveReports(hmInput);
	    } else if (strOpt.equals("sharebme")) {
	    	reportData = gmKitMappingReportBean.fetchSharedBymeReports(hmInput);
	    } else {
		   reportData = gmKitMappingReportBean.fetchkitReports(hmInput);
	    }
	   return reportData;     
	 }
    
/**
  * This method used to deactivate and re activate kit  
  */      
      @POST
      @Path("toggleactivate")
      @Produces({MediaType.APPLICATION_JSON})
      @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
      
      public List<GmKitInputsVO> deactivateKit(String strInput) throws AppError, JsonGenerationException,
          JsonMappingException, JsonParseException, IOException, Exception {
            
        log.debug("Deactivate kit");
        List<GmKitInputsVO> reportData =   new ArrayList<GmKitInputsVO>();
        GmWSUtil gmWSUtil = new GmWSUtil();
        GmKitInputsVO gmKitInputsVO = new GmKitInputsVO();
        gmWSUtil.parseJsonToVO(strInput, gmKitInputsVO);
        validateToken(gmKitInputsVO.getToken());
        GmKitMappingReportBean gmKitMappingReportBean = new GmKitMappingReportBean(gmKitInputsVO);
                      
        HashMap hmInput = gmWSUtil.getHashMapFromVO(gmKitInputsVO);
        String strUserId = GmCommonClass.parseNull((String) gmKitInputsVO.getUserid());
        String strKitType = GmCommonClass.parseNull((String) gmKitInputsVO.getKittype());
        String strActiveFl = GmCommonClass.parseNull((String)gmKitInputsVO.getActivefl());
        String strcmpId = GmCommonClass.parseNull((String)gmKitInputsVO.getCmpid());
        String strKitNm = GmCommonClass.parseNull((String)gmKitInputsVO.getKitnm());
        String strKitId = GmCommonClass.parseNull((String)gmKitInputsVO.getKitid());
        
        reportData = gmKitMappingReportBean.saveKitActiveSts(hmInput);
        
	    String strCompanyInfo ="{\"cmpid\":\""+strcmpId+"\",\"partyid\":\""+gmKitInputsVO.getPartyid()+"\",\"plantid\":\""+gmKitInputsVO.getPlantid()+"\"}";
	    syncKitNameList(strKitId,strKitId,strKitNm,"syncKitNameList",strCompanyInfo,gmKitInputsVO.getPartyid(),gmKitInputsVO,strKitType,strActiveFl);
        
        return reportData; 
        
      }
      
 /**
   * This method used to save the share kit details 
   */
	  @POST
	  @Path("sharekit")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  
	  public GmKitInputsVO saveShareKit(String strInput) throws AppError, JsonGenerationException,
	      JsonMappingException, JsonParseException, IOException, Exception {
		  
		ObjectMapper mapper = new ObjectMapper();
		HashMap hmJmsParam = new HashMap();
		String strKitId = "";
		String strUserId = "";
	    log.debug("strInput->"+strInput);
	    GmKitInputsVO gmKitInputsVO = mapper.readValue(strInput, GmKitInputsVO.class);
	    validateToken(gmKitInputsVO.getToken());
	    GmKitMappingWrapperBean gmKitMappingWrapperBean = new GmKitMappingWrapperBean(gmKitInputsVO);
	    GmCaseTxnBean gmCaseTxnBean = new GmCaseTxnBean(gmKitInputsVO);
	    String strOpt = GmCommonClass.parseNull((String) gmKitInputsVO.getStropt());
	    String strAccId = GmCommonClass.parseNull((String) gmKitInputsVO.getAccid());
	    log.debug("stropt==>"+strOpt);
	    strUserId = gmKitInputsVO.getUserid();
	    String strCompanyInfo ="{\"cmpid\":\""+gmKitInputsVO.getCmpid()+"\",\"partyid\":\""+gmKitInputsVO.getPartyid()+"\",\"plantid\":\""+gmKitInputsVO.getPlantid()+"\"}";
	    strKitId = gmKitMappingWrapperBean.saveShareKitMapping(strInput,strUserId);
	    gmKitInputsVO.setKitid(strKitId);
	    if(strOpt.equals("CASE")){	   
	        	hmJmsParam.put("USERID", strUserId);
	 		    hmJmsParam.put("CASEID", strKitId);
	 		    hmJmsParam.put("MAILSTATUS", "2");
	 		    hmJmsParam.put("ACCID", strAccId);
	 		    hmJmsParam.put("MSTAT", "E");
	 		   hmJmsParam.put("COMPANY_INFO", strCompanyInfo);
	 		    gmCaseTxnBean.UpdateEventCalendar(hmJmsParam);
	    }
	
	    return gmKitInputsVO;	    
	  } 
	  
	  
/**
  * This method used to validate loaner tag id
  */
	  @POST
	  @Path("validateTag")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  
	  public GmKitInputsVO validateLn(String strInput) throws AppError, JsonGenerationException,
      JsonMappingException, JsonParseException, IOException, Exception {
        
		log.debug("Validate Tagid");
		String reportData = "";
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmKitInputsVO gmKitInputsVO = new GmKitInputsVO();
		gmWSUtil.parseJsonToVO(strInput, gmKitInputsVO);
		validateToken(gmKitInputsVO.getToken());
		GmKitMappingReportBean gmKitMappingReportBean = new GmKitMappingReportBean(gmKitInputsVO);
                  
		HashMap hmInput = gmWSUtil.getHashMapFromVO(gmKitInputsVO);
		String strUserId = gmKitInputsVO.getUserid();
		reportData = gmKitMappingReportBean.validTag(hmInput);
		gmKitInputsVO.setTagidVaidate(reportData);
	    
	    return gmKitInputsVO;   
      }
	  
/**
  * synKitNameList - To save the kit name details to fetch from catch
  * @param String
  * @return 
  * @throws Exception
  */
	  public void syncKitNameList(String strkitId,String strKitnm,String strKitname,String strMethod,
			  String strCompanyInfo,String strPartyID,GmKitInputsVO gmKitInputsVO,String strKitType, String strActiveFl) throws Exception {

		  GmMasterRedisTransBean gmMasterRedisTransBean = new GmMasterRedisTransBean(gmKitInputsVO);
		  HashMap hmJmsParam = new HashMap();
		  hmJmsParam.put("ID", strkitId);
		  hmJmsParam.put("OLD_NM", strKitnm);
		  hmJmsParam.put("NEW_NM", strKitname);
		  hmJmsParam.put("METHOD", strMethod);
		  hmJmsParam.put("PARTYID", strPartyID);
		  hmJmsParam.put("KITTYPE", strKitType);
		  hmJmsParam.put("ACTIVEFL", strActiveFl);
		  hmJmsParam.put("companyInfo", strCompanyInfo);
		  gmMasterRedisTransBean.saveDetailsToCache(hmJmsParam);
	  }	  	        
}

