package com.globus.webservice.sales.kit.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.webservice.crm.valueobject.GmCRMPSCaseAssVO;
import com.globus.webservice.crm.valueobject.GmCRMPSCaseReqVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmKitInputsVO extends GmDataStoreVO {

  private String kitnm = "";
  private String setid = "";
  private String partnum = "";
  private String activefl = "";
  private String KitNameCheck = "";
  private String tagid = "";
  private String kitid = "";
  private String voidfl = "";
  private String kittype = "";
  private String deactivedate = "";
  private String sharedby = "";
  private String tagidVaidate = "";
  private String accid = "";
  
  private List<GmKitSetVO> arrkitsetvo = null;
  private List<GmKitPartVO> arrkitpartvo = null;
  private List<GmKitRepVO> arrrepdtl = null;
  
public String getKitnm() {
	return kitnm;
}

public void setKitnm(String kitnm) {
	this.kitnm = kitnm;
}

public String getKitNameCheck() {
	return KitNameCheck;
}

public void setKitNameCheck(String kitNameCheck) {
	KitNameCheck = kitNameCheck;
}

public String getSetid() {
	return setid;
}

public void setSetid(String setid) {
	this.setid = setid;
}

public String getPartnum() {
	return partnum;
}

public void setPartnum(String partnum) {
	this.partnum = partnum;
}

public String getTagid() {
	return tagid;
}

public void setTagid(String tagid) {
	this.tagid = tagid;
}

public String getKitid() {
	return kitid;
}

public void setKitid(String kitid) {
	this.kitid = kitid;
}

public String getVoidfl() {
	return voidfl;
}

public void setVoidfl(String voidfl) {
	this.voidfl = voidfl;
}

public String getKittype() {
	return kittype;
}

public void setKittype(String kittype) {
	this.kittype = kittype;
}

public List<GmKitSetVO> getArrkitsetvo() {
	return arrkitsetvo;
}

public void setArrkitsetvo(List<GmKitSetVO> arrkitsetvo) {
	this.arrkitsetvo = arrkitsetvo;
}

public List<GmKitPartVO> getArrkitpartvo() {
	return arrkitpartvo;
}

public void setArrkitpartvo(List<GmKitPartVO> arrkitpartvo) {
	this.arrkitpartvo = arrkitpartvo;
}

public String getActivefl() {
	return activefl;
}

public void setActivefl(String activefl) {
	this.activefl = activefl;
}

public String getDeactivedate() {
	return deactivedate;
}

public void setDeactivedate(String deactivedate) {
	this.deactivedate = deactivedate;
}

public List<GmKitRepVO> getArrrepdtl() {
	return arrrepdtl;
}

public void setArrrepdtl(List<GmKitRepVO> arrrepdtl) {
	this.arrrepdtl = arrrepdtl;
}

public String getSharedby() {
	return sharedby;
}

public void setSharedby(String sharedby) {
	this.sharedby = sharedby;
}

public String getTagidVaidate() {
	return tagidVaidate;
}

public void setTagidVaidate(String tagidVaidate) {
	this.tagidVaidate = tagidVaidate;
}

public String getAccid() {
	return accid;
}

public void setAccid(String accid) {
	this.accid = accid;
}

}