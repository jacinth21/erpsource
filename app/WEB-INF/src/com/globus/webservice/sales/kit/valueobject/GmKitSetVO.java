package com.globus.webservice.sales.kit.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmKitSetVO extends GmDataStoreVO{
	private String setid = "";
	private String setdesc = "";
	private String kitid = "";
	private String tagid = "";
	private String voidfl = "";
	private String consignto = "";
	
	public String getSetid() {
		return setid;
	}
	public void setSetid(String setid) {
		this.setid = setid;
	}
	public String getSetdesc() {
		return setdesc;
	}
	public void setSetdesc(String setdesc) {
		this.setdesc = setdesc;
	}
	public String getKitid() {
		return  kitid;
	}
	public void setKitid(String kitid) {
		this.kitid = kitid;
	}
	public String getTagid() {
		return tagid;
	}
	public void setTagid(String tagid) {
		this.tagid = tagid;
	}
	public String getVoidfl() {
		return voidfl;
	}
	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}
	public String getConsignto() {
		return consignto;
	}
	public void setConsignto(String consignto) {
		this.consignto = consignto;
	}
	
}
