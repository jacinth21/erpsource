package com.globus.webservice.sales.kit.valueobject;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class GmKitRepVO extends GmDataStoreVO{
	
	private String kitid = "";
	private String partyid = "";
	private String partynm = "";
	private String voidfl = "";
	
	public String getKitid() {
		return kitid;
	}
	public void setKitid(String kitid) {
		this.kitid = kitid;
	}
	public String getPartyid() {
		return partyid;
	}
	public void setPartyid(String partyid) {
		this.partyid = partyid;
	}
	public String getVoidfl() {
		return voidfl;
	}
	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}
	public String getPartynm() {
		return partynm;
	}
	public void setPartynm(String partynm) {
		this.partynm = partynm;
	}
	
	
}

