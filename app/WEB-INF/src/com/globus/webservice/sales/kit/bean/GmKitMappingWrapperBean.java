package com.globus.webservice.sales.kit.bean;

import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.webservice.common.bean.GmMasterRedisTransBean;
import com.globus.webservice.crm.bean.GmCRMActivityBean;

public class GmKitMappingWrapperBean extends GmBean{
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * Constructor will populate company info.
	 * 
	 * @param gmDataStore
	 */
	public GmKitMappingWrapperBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	}

	/**
	 * This method used to save the kit details model object in front-end as a
	 * JSONDATA.
	 * 
	 * @param strJSONInput
	 * @return String strKitId
	 * @throws AppError
	 */
	public HashMap saveKitMapping(String strJSONInput, String strUserID) throws AppError {
		HashMap hmJmsParam = new HashMap();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strKitId = "";
		String strKitType ="";

		gmDBManager.setPrepareString("gm_pkg_kit_mapping_wrap.gm_sav_kit_dtl",4);
		gmDBManager.setString(1, strJSONInput);
		gmDBManager.setString(2, strUserID);
		gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
		gmDBManager.execute();

		strKitId = GmCommonClass.parseNull(gmDBManager.getString(3));
		strKitType = GmCommonClass.parseNull(gmDBManager.getString(4));
		gmDBManager.commit();
		log.debug("strKitId===>" + strKitId);
		hmJmsParam.put("KITID", strKitId);
		hmJmsParam.put("KITTYPE", strKitType);
		return hmJmsParam;
	}
		  
	/**
	 * This method used to save the share kit details model object in front-end
	 * as a JSONDATA.
	 * 
	 * @param strJSONInput
	 * @return String strKitId
	 * @throws AppError
	 */
	public String saveShareKitMapping(String strJSONInput, String strUserID) throws AppError {
		log.debug("saveShareKitMapping==>");
		HashMap hmJmsParam = new HashMap();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strKitId = "";

		gmDBManager.setPrepareString("gm_pkg_kit_mapping_wrap.gm_sav_kit_rep_dtl", 3);
		gmDBManager.setString(1, strJSONInput);
		gmDBManager.setString(2, strUserID);
		gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
		gmDBManager.execute();

		strKitId = GmCommonClass.parseNull(gmDBManager.getString(3));
		gmDBManager.commit();
		log.debug("strKitId===>" + strKitId);
		return strKitId;
	}

	/**
	 * This method used to save the set details to cache
	 * 
	 * @param
	 * @return void
	 * 
	 */
	public void syncSetTagList(String strId, String strOldNm, String strNewNm,
			String strMethod, String strCompanyInfo, String strDistId) {

		GmMasterRedisTransBean gmMasterRedisTransBean = new GmMasterRedisTransBean(getGmDataStoreVO());
		HashMap hmJmsParam = new HashMap();
		hmJmsParam.put("ID", strId);
		hmJmsParam.put("OLD_NM", strOldNm);
		hmJmsParam.put("NEW_NM", strNewNm);
		hmJmsParam.put("METHOD", strMethod);
		hmJmsParam.put("DISTID", strDistId);
		hmJmsParam.put("companyInfo", strCompanyInfo);
		gmMasterRedisTransBean.saveDetailsToCache(hmJmsParam);
	}

	/**
	 * This method used to save the set details to cache based on tag
	 * 
	 * @param
	 * @return void
	 * 
	 */
	public void syncTagList(String strId, String strOldNm, String strNewNm,
			String strMethod, String strCompanyInfo, String strDistId) {
		
		GmMasterRedisTransBean gmMasterRedisTransBean = new GmMasterRedisTransBean(getGmDataStoreVO());
		HashMap hmJmsParam = new HashMap();
		hmJmsParam.put("ID", strId);
		hmJmsParam.put("OLD_NM", strOldNm);
		hmJmsParam.put("NEW_NM", strNewNm);
		hmJmsParam.put("METHOD", strMethod);
		hmJmsParam.put("DISTID", strDistId);
		hmJmsParam.put("companyInfo", strCompanyInfo);

		gmMasterRedisTransBean.saveDetailsToCache(hmJmsParam);
	}

	/**
	 * This method used to save the part details to cache
	 * 
	 * @param
	 * @return void
	 * 
	 */
	public void syncPartNumList(String strId, String strOldNm, String strNewNm,
			String strMethod, String strCompanyInfo) {
		log.debug("syncPartNumList==>" + strMethod);
		GmMasterRedisTransBean gmMasterRedisTransBean = new GmMasterRedisTransBean(
				getGmDataStoreVO());
		HashMap hmJmsParam = new HashMap();
		hmJmsParam.put("ID", strId);
		hmJmsParam.put("OLD_NM", strOldNm);
		hmJmsParam.put("NEW_NM", strNewNm);
		hmJmsParam.put("METHOD", strMethod);
		hmJmsParam.put("companyInfo", strCompanyInfo);
		gmMasterRedisTransBean.saveDetailsToCache(hmJmsParam);
	}
} 
