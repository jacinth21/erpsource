package com.globus.webservice.sales.casemanagement.valueobject;

import com.globus.valueobject.common.GmDataStoreVO;

public class GmCaseSetVO extends GmDataStoreVO{
	
	private String caseinfoid="";
	private String kitid="";
	private String kitnm="";
	private String setid="";
	private String setdesc="";
	private String tagid="";
	private String qty="";
	private String voidfl="";
	
	public String getCaseinfoid() {
		return caseinfoid;
	}
	public void setCaseinfoid(String caseinfoid) {
		this.caseinfoid = caseinfoid;
	}
	public String getKitid() {
		return kitid;
	}
	public void setKitid(String kitid) {
		this.kitid = kitid;
	}
	public String getKitnm() {
		return kitnm;
	}
	public void setKitnm(String kitnm) {
		this.kitnm = kitnm;
	}
	public String getSetid() {
		return setid;
	}
	public void setSetid(String setid) {
		this.setid = setid;
	}
	public String getSetdesc() {
		return setdesc;
	}
	public void setSetdesc(String setdesc) {
		this.setdesc = setdesc;
	}
	public String getTagid() {
		return tagid;
	}
	public void setTagid(String tagid) {
		this.tagid = tagid;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getVoidfl() {
		return voidfl;
	}
	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}

}
