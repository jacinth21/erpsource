package com.globus.webservice.sales.casemanagement.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmExchangeManager;
import com.globus.common.beans.GmLogger;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.sales.casemanagement.bean.GmCaseTxnBean;
import com.globus.webservice.sales.casemanagement.bean.GmCaseRptBean;
import com.globus.webservice.sales.casemanagement.valueobject.GmCaseVO;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

@Path("casetxn")
public class GmCaseTxnResource extends GmResource {
	  Logger log = GmLogger.getInstance(this.getClass().getName());

	  /**
       * This method used to save and edit case details 
       * object in front-end as a JSONDATA.
       */
	  @POST
	  @Path("savecase")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  
	  public GmCaseVO saveCaseDtl(String strInput) throws AppError, JsonGenerationException,
	      JsonMappingException, JsonParseException, IOException, Exception {
	
		 ObjectMapper mapper = new ObjectMapper();
		 HashMap hmJmsParam = new HashMap();
		 ArrayList alParticipants = new ArrayList();
		 String strCaseInfoId = "";
		 String strUserId = "";
		 String strRecipients = "";
		 String strShareRecipients = "";
		 String strSurgeon ="";
		 String strcompid = "";
		 String strCmpdFmt = "";
	     log.debug("strInput->"+strInput);
	    
	     GmCaseVO gmCaseVO = mapper.readValue(strInput, GmCaseVO.class);	
	     validateToken(gmCaseVO.getToken());
	     GmCaseTxnBean gmCaseTxnBean = new GmCaseTxnBean(gmCaseVO);
	     strUserId = GmCommonClass.parseNull((String)gmCaseVO.getUserid());
	     String strMailStatus = GmCommonClass.parseNull((String)gmCaseVO.getMailstatus());
	     String strAccId = GmCommonClass.parseNull((String)gmCaseVO.getAccid());
	     strCaseInfoId = gmCaseTxnBean.saveCaseDetail(strInput,strUserId);
	     strCmpdFmt = GmCommonClass.parseNull((String)gmCaseVO.getCmpdfmt());
	     String strCompanyInfo ="{\"cmpid\":\""+gmCaseVO.getCmpid()+"\",\"partyid\":\""+gmCaseVO.getPartyid()+"\",\"plantid\":\""+gmCaseVO.getPlantid()+"\"}";
	     gmCaseVO.setCaseinfoid(strCaseInfoId);
	     
	    
	     //TO UPDATE THE CASE SCHEDULAR CALENDAR EVENT NOTIFICATION EMAIL
	     
	        hmJmsParam.put("USERID", strUserId);
		    hmJmsParam.put("CASEID", strCaseInfoId);
		    hmJmsParam.put("MAILSTATUS", strMailStatus);
		    hmJmsParam.put("ACCID", strAccId);
		    hmJmsParam.put("CMPDFMT", strCmpdFmt);
		    hmJmsParam.put("MSTAT", "E");
		    hmJmsParam.put("COMPANY_INFO", strCompanyInfo);
		    gmCaseTxnBean.UpdateEventCalendar(hmJmsParam);
	  
	     return gmCaseVO;	    
	  }
	  
	  /**
       * This method used to update case status as cancelled
       * object in front-end as a JSONDATA.
       */
	  @POST
	  @Path("cancelcase")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  
	  public GmCaseVO cancelCase(String strInput) throws AppError, JsonGenerationException,
	      JsonMappingException, JsonParseException, IOException, Exception {
	
		 ObjectMapper mapper = new ObjectMapper();
		 HashMap hmJmsParam = new HashMap();
		 String strCaseInfoId = "";
		 String strUserId = "";
	     log.debug("strInput->"+strInput);
	   
	     GmCaseVO gmCaseVO = mapper.readValue(strInput, GmCaseVO.class);	
	     validateToken(gmCaseVO.getToken());	
	     GmCaseTxnBean gmCaseTxnBean = new GmCaseTxnBean(gmCaseVO);
	     strUserId = GmCommonClass.parseNull((String)gmCaseVO.getUserid());
	     String strMailStatus = GmCommonClass.parseNull((String)gmCaseVO.getMailstatus());
	     String strAccId = GmCommonClass.parseNull((String)gmCaseVO.getAccid());
	     String strCompanyInfo ="{\"cmpid\":\""+gmCaseVO.getCmpid()+"\",\"partyid\":\""+gmCaseVO.getPartyid()+"\",\"plantid\":\""+gmCaseVO.getPlantid()+"\"}";
	     log.debug("strUserId--->"+strUserId);
	     strCaseInfoId = gmCaseTxnBean.saveCancelCase(strInput,strUserId);
	     
	     gmCaseVO.setCaseinfoid(strCaseInfoId);
	     
	     hmJmsParam.put("USERID", strUserId);
		 hmJmsParam.put("CASEID", strCaseInfoId);
		 hmJmsParam.put("MAILSTATUS", strMailStatus);
		 hmJmsParam.put("ACCID", strAccId);
		 hmJmsParam.put("MSTAT", "C");
		 hmJmsParam.put("COMPANY_INFO", strCompanyInfo);
		 gmCaseTxnBean.UpdateEventCalendar(hmJmsParam);
		 
	     return gmCaseVO;	
	  } 
	
}
