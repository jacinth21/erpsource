package com.globus.webservice.sales.casemanagement.valueobject;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class GmCaseVO extends GmDataStoreVO{
	
	private String caseinfoid="";
	private String caseid="";
	private String surgdt="";
	private String starttm="";
	private String endtm="";
	private String accid="";
	private String accnm="";
	private String surgtypeid="";
	private String surgtype="";
	private String surglvlid="";
	private String surglvl="";
	private String statusid="";
	private String status="";
	private String notes="";
	private String shareid="";
	private String sharenm="";
	private String sbid="";
	private String tagid="";
	private String setid="";
	private String tagfl="";
	private String voidfl="";
	private String mailstatus="";
	private String createddt="";
	private String createdby="";
	private String updateddt="";
	private String updatedby="";
	private String compid="";
	
	public String getMailstatus() {
		return mailstatus;
	}
	public void setMailstatus(String mailstatus) {
		this.mailstatus = mailstatus;
	}
	public String getCreateddt() {
		return createddt;
	}
	public void setCreateddt(String createddt) {
		this.createddt = createddt;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public String getUpdateddt() {
		return updateddt;
	}
	public void setUpdateddt(String updateddt) {
		this.updateddt = updateddt;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public String getVoidfl() {
		return voidfl;
	}
	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}
	private List<GmCasePartyVO> arrcasepartyvo = null;
	private List<GmCaseShareVO> arrcasesharevo = null;
	private List<GmCaseSetVO> arrcasesetvo = null;
	private List<GmCasePartVO> arrcasepartvo = null;
	public String getCaseinfoid() {
		return caseinfoid;
	}
	public void setCaseinfoid(String caseinfoid) {
		this.caseinfoid = caseinfoid;
	}
	public String getCaseid() {
		return caseid;
	}
	public void setCaseid(String caseid) {
		this.caseid = caseid;
	}
	public String getSurgdt() {
		return surgdt;
	}
	public void setSurgdt(String surgdt) {
		this.surgdt = surgdt;
	}
	public String getStarttm() {
		return starttm;
	}
	public void setStarttm(String starttm) {
		this.starttm = starttm;
	}
	public String getEndtm() {
		return endtm;
	}
	public void setEndtm(String endtm) {
		this.endtm = endtm;
	}
	public String getAccid() {
		return accid;
	}
	public void setAccid(String accid) {
		this.accid = accid;
	}
	public String getAccnm() {
		return accnm;
	}
	public void setAccnm(String accnm) {
		this.accnm = accnm;
	}
	public String getSurgtypeid() {
		return surgtypeid;
	}
	public void setSurgtypeid(String surgtypeid) {
		this.surgtypeid = surgtypeid;
	}
	public String getSurgtype() {
		return surgtype;
	}
	public void setSurgtype(String surgtype) {
		this.surgtype = surgtype;
	}
	public String getSurglvlid() {
		return surglvlid;
	}
	public void setSurglvlid(String surglvlid) {
		this.surglvlid = surglvlid;
	}
	public String getSurglvl() {
		return surglvl;
	}
	public void setSurglvl(String surglvl) {
		this.surglvl = surglvl;
	}
	public String getStatusid() {
		return statusid;
	}
	public void setStatusid(String statusid) {
		this.statusid = statusid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getShareid() {
		return shareid;
	}
	public void setShareid(String shareid) {
		this.shareid = shareid;
	}
	public String getSharenm() {
		return sharenm;
	}
	public void setSharenm(String sharenm) {
		this.sharenm = sharenm;
	}
	public List<GmCasePartyVO> getArrcasepartyvo() {
		return arrcasepartyvo;
	}
	public void setArrcasepartyvo(List<GmCasePartyVO> arrcasepartyvo) {
		this.arrcasepartyvo = arrcasepartyvo;
	}
	public List<GmCaseShareVO> getArrcasesharevo() {
		return arrcasesharevo;
	}
	public void setArrcasesharevo(List<GmCaseShareVO> arrcasesharevo) {
		this.arrcasesharevo = arrcasesharevo;
	}
	public List<GmCaseSetVO> getArrcasesetvo() {
		return arrcasesetvo;
	}
	public void setArrcasesetvo(List<GmCaseSetVO> arrcasesetvo) {
		this.arrcasesetvo = arrcasesetvo;
	}
	public List<GmCasePartVO> getArrcasepartvo() {
		return arrcasepartvo;
	}
	public void setArrcasepartvo(List<GmCasePartVO> arrcasepartvo) {
		this.arrcasepartvo = arrcasepartvo;
	}
	public String getSbid() {
		return sbid;
	}
	public void setSbid(String sbid) {
		this.sbid = sbid;
	}
	public String getTagid() {
		return tagid;
	}
	public void setTagid(String tagid) {
		this.tagid = tagid;
	}
	public String getSetid() {
		return setid;
	}
	public void setSetid(String setid) {
		this.setid = setid;
	}
	public String getTagfl() {
		return tagfl;
	}
	public void setTagfl(String tagfl) {
		this.tagfl = tagfl;
	}
	public String getCompid() {
		return compid;
	}
	public void setCompid(String compid) {
		this.compid = compid;
	}
		
}
