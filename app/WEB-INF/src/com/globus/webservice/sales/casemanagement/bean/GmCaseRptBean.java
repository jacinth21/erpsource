package com.globus.webservice.sales.casemanagement.bean;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.filters.GmAccessFilter;
import com.globus.valueobject.common.GmDataStoreVO;


	public class GmCaseRptBean extends GmBean{
	 Logger log = GmLogger.getInstance(this.getClass().getName());
	  
	public GmCaseRptBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	}

	/**
	 * Constructor will populate company info.
	 * 
	 * @param gmDataStore
	 */
	public GmCaseRptBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	}

	/**
	 * fetchAccountList - To fetch the Account list based on rep id
	 * 
	 * @param String strRepId
	 * @return String
	 * @throws AppError
	 */
	public String fetchAccountList(HashMap hmParams) throws AppError{

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		log.debug("hmParams>>>>"+hmParams);
		 HashMap hmParam = (HashMap) hmParams.get("HMPARAM");
		    GmAccessFilter gmAccessFilter = new GmAccessFilter();
		    String strLogAccsLvl = (String) hmParam.get("ACCS_LVL");
		    hmParam = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
		    String strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("AccessFilter"));

		    
		    log.debug("strAccessFilter>>>>"+strAccessFilter);
		    String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));
		    StringBuffer sbQuery = new StringBuffer();
		    sbQuery.append(" SELECT JSON_ARRAYAGG(JSON_OBJECT( 'ID' value V700.AC_ID, 'Name' value NVL(V700.AC_NAME,V700.AC_NAME_EN) )  ORDER BY ");
		    sbQuery.append(" V700.AC_ID RETURNING CLOB) FROM ");
	    	sbQuery.append("(select Distinct ac_id, ac_name,ac_name_en, COMPID, COMPNAME,DIVID,DIVNAME,GP_ID,GP_NAME,REGION_ID,REGION_NAME,VP_ID, REP_COMPID,REP_ID,D_ID,AD_ID");
	        sbQuery.append(" from v700_territory_mapping_detail) v700  WHERE V700.AC_ID IS NOT NULL");
		    sbQuery.append(" AND " + strAccessFilter + " " );
		    log.debug("sbQuery>>>>>" + sbQuery);

		    String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());
		return strReturn;
	}

	/**
	 * fetchSetDetailsFromSB - To fetch set details from set bundle id
	 * 
	 * @param String strSBId
	 * @return String
	 * @throws AppError
	 */
	public String fetchSetDetailsFromSB(String strSBId) throws AppError{

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_case_rpt.gm_fch_set_dtl_Sb", 2);
		gmDBManager.setString(1, strSBId);
		gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
		gmDBManager.execute();
		String strAccList = GmCommonClass.parseNull(gmDBManager.getString(2));
		gmDBManager.close();
		return strAccList;
	}

	/**
	 * validateTag - To validate the tag in case inventory details tab
	 * 
	 * @param String strTagNum, strSetId
	 * @return String
	 * @throws AppError
	 */
	public String validateTag(String strTagNum, String strSetId)throws AppError,Exception {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_case_rpt.gm_fch_validate_tag", 3);
		gmDBManager.setString(1, strSetId);
		gmDBManager.setString(2, strTagNum);
		gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
		gmDBManager.execute();
		String strTagVal = GmCommonClass.parseNull(gmDBManager.getString(3));
		log.debug("strTagVal==>" + strTagVal);
		gmDBManager.close();
		return strTagVal;
	}
	
	/**
	 * generateCashId - To generate cash id when surgery date and account selected on case screen
	 * 
	 * @param String strSurgDt, strRepId, strCmpdtfmt
	 * @return String
	 * @throws Exception
	 */
	public String generateCashId(String strSurgDt, String strRepId,String strCmpdtfmt) throws Exception {
		
		log.debug("strSurgDt==>" + strSurgDt + " strRepId==>" + strRepId+ " strCmpdtfmt==>" + strCmpdtfmt);

		Date strSurDate = GmCommonClass.getStringToDate(strSurgDt, strCmpdtfmt);
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_case_rpt.gm_fch_case_id", 3);
		gmDBManager.setDate(1, strSurDate);
		gmDBManager.setString(2, strRepId);
		gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
		gmDBManager.execute();
		String strCashId = GmCommonClass.parseNull(gmDBManager.getString(3));
		log.debug("strCashId==>" + strCashId);
		gmDBManager.close();
		return strCashId;
	}
 
	/**
	 * fetchCaseDashboardData - This method is used to fetch the case dashboard data
	 * 
	 * @param strPartyId
	 * @exception AppError
	 */
	public String fetchCaseDashboardData(String strPartyId,String strCmpdfmt) throws AppError {

		log.debug("fetchCasedashboardData :" + strPartyId);

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_case_rpt.gm_fch_case_dsh_data", 3);
		gmDBManager.setString(1, strPartyId);
		gmDBManager.setString(2, strCmpdfmt);
		gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
		gmDBManager.execute();
		String strCaseDtl = GmCommonClass.parseNull(gmDBManager.getString(3));
		gmDBManager.close();
		log.debug("strCaseDtl==>" + strCaseDtl);

		return strCaseDtl;
	}
	   
	/**
	 * fetchCaseMapping - This method is used to fetch the case details
	 * 
	 * @param strCaseinfoId
	 * @return String Json data
	 * @exception AppError
	 */
	public String fetchCaseMapping(String strCaseinfoId,String strPartyId, String strCmpdfmt) throws AppError {

		log.debug("fetchCaseMapping :" + strCaseinfoId);
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_case_rpt.gm_fch_case_dtl", 4);
		gmDBManager.setString(1, strCaseinfoId);
		gmDBManager.setString(2, strPartyId);
		gmDBManager.setString(3, strCmpdfmt);
		gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
		gmDBManager.execute();

		String strCaseDetails = GmCommonClass.parseNull(gmDBManager.getString(4));
		gmDBManager.close();

		return strCaseDetails;
	}

	/**
	 * fetchCaseDtlData - This method is used to fetch the case details data
	 * 
	 * @param strPartyId
	 * @exception AppError
	 */
	public String fetchCaseDtlData(String strPartyId, String strCmpdfmt) throws AppError {

		log.debug("strPartyId=>"+strPartyId);

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_case_rpt.gm_fch_case_shared_dtl", 3);
		gmDBManager.setString(1, strPartyId);
		gmDBManager.setString(2, strCmpdfmt);
		gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
		gmDBManager.execute();
		String strCaseDtl =  GmCommonClass.parseNull(gmDBManager.getString(3));
		gmDBManager.close();
		log.debug("strCaseDtl==>" + strCaseDtl);

		return strCaseDtl;
	}
	
	/**
	 * fetchCaseCalendarData - This method is used to fetch the calendar
	 * data  
	 * @param strPartyId
	 * @exception AppError
	 */
	public String fetchCaseCalendarData(String strPartyId, String strCmpdfmt) throws AppError {

		log.debug("strPartyId=>"+strPartyId);
		String strCaseDtl = "";
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_case_rpt.gm_fch_case_calendar_data", 3);
		gmDBManager.setString(1, strPartyId);
		gmDBManager.setString(2, strCmpdfmt);
		gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
		gmDBManager.execute();
		strCaseDtl =  GmCommonClass.parseNull(gmDBManager.getString(3));
		gmDBManager.close();
		log.debug("strCaseDtl==>" + strCaseDtl);

		return strCaseDtl;
	}
	
	/**
	 * fetchCaseData - This method is used to fetch the dashboard
	 * data  
	 * @param strPartyId
	 * @exception AppError
	 */
	public String fetchCaseData(String strPartyId,String strCmpdfmt) throws AppError {

		log.debug("strPartyId=>"+strPartyId +"strCmpdfmt=>"+strCmpdfmt);

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_case_rpt.gm_fch_case_dashborad", 3);
		gmDBManager.setString(1, strPartyId);
		gmDBManager.setString(2, strCmpdfmt);
		gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
		gmDBManager.execute();
		String strCaseDtl =  GmCommonClass.parseNull(gmDBManager.getString(3));
		gmDBManager.close();
		log.debug("strCaseDtl==>" + strCaseDtl);

		return strCaseDtl;
	}
	/**
	 * fetchCaseEventCal - This method is used to fetch case event update
	 * details based on case info id
	 * 
	 * @param - strCaseInfoId
	 * @return - ArrayList
	 * @exception - AppError, Exception
	 */
	public ArrayList fetchCaseEventCal(String strCaseInfoId,String strRepId, String strCmpdFmt) throws AppError,Exception {

		ArrayList alListVal = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		GmEmailProperties gmEmailProperties = new GmEmailProperties();

		log.debug("strCaseInfoId==>" + strCaseInfoId +"strCmpdFmt>> "+strCmpdFmt);

		gmDBManager.setPrepareString("gm_pkg_case_rpt.gm_fch_case_book_dtl", 4);
		gmDBManager.setString(1, strCaseInfoId);
		gmDBManager.setString(2, strRepId);
		gmDBManager.setString(3, strCmpdFmt);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.execute();
		alListVal = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
		gmDBManager.close();
		log.debug("alListVal==>" + alListVal.size());
		return alListVal;

	}
	
	/**
	 * fetchAssoRepAcc - This method is used to fetch the Associate rep based on
	 * account id
	 * 
	 * @param - strAccId
	 * @return - ArrayList
	 * @exception - AppError, Exception
	 */
	public ArrayList fetchAssoRepAcc(String strAccId) throws Exception {

		log.debug("getAssoRepAcc==>" + strAccId);
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		ArrayList alList = new ArrayList();
		gmDBManager.setPrepareString("gm_pkg_case_rpt.gm_fch_asso_rep_acc",2);
		gmDBManager.setString(1, strAccId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();

		return alList;
	}

	/**
	 * fetchCaseEventCal - This method is used to fetch case event update
	 * details based on case info id
	 * 
	 * @param - strCaseInfoId
	 * @return - ArrayList
	 * @exception - AppError, Exception
	 */
	public ArrayList fetchShareRepList(String strCaseInfoId) throws AppError,Exception {

		ArrayList alListVal = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		GmEmailProperties gmEmailProperties = new GmEmailProperties();

		log.debug("strCaseInfoId==>" + strCaseInfoId);

		gmDBManager.setPrepareString("gm_pkg_case_rpt.gm_fch_case_share_mail", 2);
		gmDBManager.setString(1, strCaseInfoId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alListVal = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		log.debug("alListVal==>" + alListVal.size());
		return alListVal;

	}
	
	
}
