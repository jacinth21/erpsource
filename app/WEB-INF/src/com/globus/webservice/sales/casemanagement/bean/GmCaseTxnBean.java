package com.globus.webservice.sales.casemanagement.bean;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmExchangeManager;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.webservice.sales.casemanagement.valueobject.GmCaseVO;

public class GmCaseTxnBean extends GmBean{
	 Logger log = GmLogger.getInstance(this.getClass().getName());
	  
	  public GmCaseTxnBean() {
		    super(GmCommonClass.getDefaultGmDataStoreVO());
		  }

	/**
	 * Constructor will populate company info.
	 * 
	 * @param gmDataStore
	 */
	public GmCaseTxnBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	}

	/**
	 * This method used to save the case details model object in front-end as a
	 * JSONDATA.
	 * 
	 * @param strJSONInput
	 * @return String strCaseId
	 * @throws AppError
	 */
	public String saveCaseDetail(String strJSONInput, String strUserID)
			throws AppError {
		HashMap hmJmsParam = new HashMap();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strCaseInfoId = "";

		gmDBManager.setPrepareString("gm_pkg_case_wrap.gm_sav_case_dtl", 3);
		gmDBManager.setString(1, strJSONInput);
		gmDBManager.setString(2, strUserID);
		gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
		gmDBManager.execute();

		strCaseInfoId = GmCommonClass.parseNull(gmDBManager.getString(3));
		gmDBManager.commit();
		log.debug("strCaseInfoId===>" + strCaseInfoId);

		return strCaseInfoId;
	}
	/**
	 * saveCancelCase This method used to update case status as cancelled 
	 * JSONDATA.
	 * 
	 * @param strJSONInput
	 * @return String strUserID
	 * @throws AppError
	 */
	public String saveCancelCase(String strJSONInput, String strUserID)
			throws AppError {
		HashMap hmJmsParam = new HashMap();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strCaseInfoId = "";

		gmDBManager.setPrepareString("gm_pkg_case_txn.gm_sav_cancel_case", 3);
		gmDBManager.setString(1, strJSONInput);
		gmDBManager.setString(2, strUserID);
		gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
		gmDBManager.execute();

		strCaseInfoId = GmCommonClass.parseNull(gmDBManager.getString(3));
		gmDBManager.commit();
		
		return strCaseInfoId;
	}
	
	/**
	 * checkDirectRepdtl This method used to check login rep is direct sales rep or not
	 * JSONDATA.
	 * 
	 * @param strRepId
	 * @return String 
	 * @throws AppError
	 */
	public HashMap checkDirectRepdtl(String strRepId) throws AppError{

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		HashMap hmParam = new HashMap();
		gmDBManager.setPrepareString("gm_pkg_case_rpt.gm_check_dir_rep", 2);
		gmDBManager.setString(1, strRepId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();

		hmParam = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		
		return hmParam;
	}
	/**
	 * updateCaseBookingCalJMS - This method will form the message and send
	 * through JMS for update Event on calendar
	 * 
	 * @param -hmParam
	 * @return - Void
	 * @exception - AppError
	 */
	public void updateCaseBookingCalJMS(HashMap hmParam) throws AppError {
		log.debug("updateCaseBookingCalJMS==>"+hmParam);
		GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
		String strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean
				.getJmsConfig("CASE_BOOKING_CAL_CONSUMER_CLASS"));
		hmParam.put("HMLNPARAM", hmParam);
		hmParam.put("CONSUMERCLASS", strConsumerClass);
		gmConsumerUtil.sendMessage(hmParam);

	}
	
	/**
	 * To save the event item id for the case booking.
	 * 
	 * @param hmParams
	 */
	public void saveEventItemId(HashMap hmParams) {
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_case_txn.gm_sav_case_event_item_id", 3);

		gmDBManager.setString(1,GmCommonClass.parseNull((String) hmParams.get("CASEINFOID")));
		gmDBManager.setString(2,GmCommonClass.parseNull((String) hmParams.get("ITEMID")));
		gmDBManager.setString(3,GmCommonClass.parseNull((String) hmParams.get("USERID")));

		gmDBManager.execute();
		gmDBManager.commit();
	}
	  /**
     * UpdateEventCalendar - This method used pass the detail of update AD calendar data
     * @param hmParam
     * @return 
     * @throws AppError, Exception
     */  
	  public void UpdateEventCalendar(HashMap hmParam) throws AppError,Exception{
		  
			 HashMap hmJmsParam = new HashMap();
			 ArrayList alParticipants = new ArrayList();
			 String strCaseInfoId = GmCommonClass.parseNull((String)hmParam.get("CASEID"));
			 String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
			 String strCmpdFmt = GmCommonClass.parseNull((String) hmParam.get("CMPDFMT"));
			 String strAccId = GmCommonClass.parseNull((String)hmParam.get("ACCID"));
			 String strMailStatus = GmCommonClass.parseNull((String)hmParam.get("MAILSTATUS"));
			 String strMStat = GmCommonClass.parseNull((String)hmParam.get("MSTAT")); 
			 String strRecipients = "";
			 String strShareRecipients = "";
			 String strSurgeon ="";
			 String strMessage = "";
			 String strRepNm = "";
			 GmCaseTxnBean gmCaseTxnBean = new GmCaseTxnBean(getGmDataStoreVO());
		     GmCaseRptBean gmCaseRptbean = new GmCaseRptBean(getGmDataStoreVO());
		     HashMap hmReturn = GmCommonClass.parseNullHashMap((HashMap)gmCaseTxnBean.checkDirectRepdtl(strUserId));
		     String strRepCat = GmCommonClass.parseNull((String)hmReturn.get("CAT"));
		     String strRepEmail = GmCommonClass.parseNull((String)hmReturn.get("REPEMAILID"));
		     if(strRepCat.equals("4021")){    //Direct sales rep	
		    	ArrayList alList = gmCaseRptbean.fetchCaseEventCal(strCaseInfoId,strUserId,strCmpdFmt);
		    		for (int i = 0; i < alList.size(); i++) {
		    			HashMap hmdata = new HashMap();
		    			hmdata = GmCommonClass.parseNullHashMap((HashMap) alList.get(i));
		    			strSurgeon += "," + hmdata.get("SURNM");
		    			strRepNm = GmCommonClass.parseNull((String)hmdata.get("REPNAME"));
		    		}
		    	 String strSubjects = "Surgery with "+strSurgeon.substring(strSurgeon.indexOf(',') + 1);
		    	 alParticipants = gmCaseRptbean.fetchAssoRepAcc(strAccId);
		 		 	for (int i = 0; i < alParticipants.size(); i++) {
		 		 		HashMap hmParticipant = new HashMap();
		 		 		hmParticipant = GmCommonClass.parseNullHashMap((HashMap) alParticipants.get(i));
		 		 		strRecipients += "," + hmParticipant.get("EMAILID");
		 		 	}
		 		 strRecipients = strRecipients + "," + strRepEmail;
		 		
		 		  //SHARD USER EMAIL ADDRESS
		 		 ArrayList alShareList = GmCommonClass.parseNullArrayList( (ArrayList) gmCaseRptbean.fetchShareRepList(strCaseInfoId));
					if(alShareList.size() > 0){
						for (int i = 0; i < alShareList.size(); i++) {
							HashMap hmParticipant = new HashMap();
						 	hmParticipant = GmCommonClass.parseNullHashMap((HashMap) alShareList.get(i));
						 	strShareRecipients += "," + hmParticipant.get("SHAREEMAIL");
						}
					strRecipients = strRecipients + strShareRecipients;
					//strMessage = " <b>Case Shared by "+strRepNm+"</b><br/><br/>";
					}
					    
		    	 hmJmsParam.put("USERID", strUserId);
				 hmJmsParam.put("CASEID", strCaseInfoId);
				 hmJmsParam.put("MAILSTATUS", strMailStatus);
				 hmJmsParam.put("SUBJECT", strSubjects);
				 hmJmsParam.put("MESSAGE", strMessage);
				 hmJmsParam.put("RECIPIENTS", strRecipients.substring(strRecipients.indexOf(',') + 1));
				 hmJmsParam.put("COMPANY_INFO", GmCommonClass.parseNull((String)hmParam.get("COMPANY_INFO")));
				 gmCaseTxnBean.updateCaseBookingCalJMS(hmJmsParam);		   
		     }
	  }
}
