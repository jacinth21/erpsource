package com.globus.webservice.sales.casemanagement.valueobject;

import com.globus.valueobject.common.GmDataStoreVO;

public class GmCasePartyVO extends GmDataStoreVO{
	
	private String caseinfoid="";
	private String npiid="";
	private String partyid="";
	private String partynm="";
	private String voidfl="";
	
	public String getCaseinfoid() {
		return caseinfoid;
	}
	public void setCaseinfoid(String caseinfoid) {
		this.caseinfoid = caseinfoid;
	}
	public String getNpiid() {
		return npiid;
	}
	public void setNpiid(String npiid) {
		this.npiid = npiid;
	}
	public String getPartyid() {
		return partyid;
	}
	public void setPartyid(String partyid) {
		this.partyid = partyid;
	}
	public String getPartynm() {
		return partynm;
	}
	public void setPartynm(String partynm) {
		this.partynm = partynm;
	}
	public String getVoidfl() {
		return voidfl;
	}
	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}
	

}
