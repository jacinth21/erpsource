package com.globus.webservice.sales.casemanagement.resources;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.sales.CaseManagement.beans.GmCaseBookRptBean;
import com.globus.sales.InvAllocation.beans.GmPendAllocationBean;
import com.globus.valueobject.common.GmDeviceDetailVO;
import com.globus.valueobject.operations.request.GmCaseAttributeVO;
import com.globus.valueobject.operations.request.GmCaseInfoListVO;
import com.globus.valueobject.operations.request.GmCaseInfoVO;
import com.globus.valueobject.operations.request.GmCaseListVO;
import com.globus.valueobject.sales.GmProdRequestDetVO;
import com.globus.webservice.common.resources.GmResource;

@Path("caseschdl")
public class GmCaseSchdlResource extends GmResource {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**syncCaseSchedule - This method is used to sync the case and case attribute from Device.
	 * @param strInput
	 * @return GmCaseInfoVO
	 * @throws AppError
	 */
	@POST
	@Path("sync")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })

	public GmCaseListVO syncCaseSchedule(String strInput) throws AppError,
			IOException, JsonGenerationException, JsonMappingException,
			JsonParseException, ParseException {
		log.debug("======syncCaseSchedule========");
	
		GmCaseListVO gmCaseListVO = new GmCaseListVO();
		GmCaseInfoVO gmCaseInfoVO = new GmCaseInfoVO();
		GmCaseAttributeVO gmCaseAttributeVO = new GmCaseAttributeVO();
		GmDeviceDetailVO gmDeviceDetailVO = new GmDeviceDetailVO();
		GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean();
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmProdCatBean gmProdCatBean = new GmProdCatBean();
		
		//validate token and parse JSON into VO
		parseInputAndValidateToken(strInput, gmDeviceDetailVO);
		
		ArrayList alCaseDtls  = new ArrayList();
		HashMap hmParam= new HashMap();
		
		List<GmCaseInfoVO> listGmCaseInfoVO   =  new ArrayList<GmCaseInfoVO>();
		List<GmCaseAttributeVO> listGmCaseAttributeVO   =  new ArrayList<GmCaseAttributeVO>();
		
		hmParam = gmWSUtil.getHashMapFromVO(gmDeviceDetailVO);
		hmParam.put("USERID", gmUserVO.getUserid());
		hmParam.put("LANGUAGE","103097"); //English
		alCaseDtls = gmCaseBookRptBean.fetchCaseMaster(hmParam);

		
		listGmCaseInfoVO = gmWSUtil.getVOListFromHashMapList(alCaseDtls,
				gmCaseInfoVO, gmCaseInfoVO.getCaseInfoProperties());
		
		//Set the page header detail from first hashmap of list.
		gmCaseListVO.populatePageProperties(alCaseDtls);
		gmCaseListVO.setReftype("4000533");
		hmParam.put("PAGESIZE",gmCaseListVO.getPagesize());
		hmParam.put("REFTYPE","4000533"); //Case Sync type
		hmParam.put("STATUSID","103120"); //Initiate
		
		// To save details of device sync details and sync log.
		gmProdCatBean.saveDeviceSyncDetail(hmParam);
		
		gmCaseListVO.setListGmCaseInfoVO(listGmCaseInfoVO);
		//gmCaseListVO.setListGmCaseAttributeVO(listGmCaseAttributeVO);
		
		alCaseDtls  = null;
		hmParam= null;
		
		return gmCaseListVO;
	}
	/**syncCaseSchedule - This method is used to sync the case and case attribute from Device.
	 * @param strInput
	 * @return GmCaseInfoVO
	 * @throws AppError
	 */
	@POST
	@Path("syncattr")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })

	public GmCaseListVO syncCaseAttribute(String strInput) throws AppError,
			IOException, JsonGenerationException, JsonMappingException,
			JsonParseException, ParseException {
		log.debug("======syncCaseSchedule========");
		String strPageSize = "";
		GmCaseListVO gmCaseListVO = new GmCaseListVO();
		GmCaseInfoVO gmCaseInfoVO = new GmCaseInfoVO();
		GmCaseAttributeVO gmCaseAttributeVO = new GmCaseAttributeVO();
		GmDeviceDetailVO gmDeviceDetailVO = new GmDeviceDetailVO();
		GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean();
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmProdCatBean gmProdCatBean = new GmProdCatBean();
		
		//validate token and parse JSON into VO
		parseInputAndValidateToken(strInput, gmDeviceDetailVO);
		
		ArrayList alCaseAttrDtls  = new ArrayList();
		HashMap hmParam= new HashMap();
		
		List<GmCaseInfoVO> listGmCaseInfoVO   =  new ArrayList<GmCaseInfoVO>();
		List<GmCaseAttributeVO> listGmCaseAttributeVO   =  new ArrayList<GmCaseAttributeVO>();
		
		hmParam = gmWSUtil.getHashMapFromVO(gmDeviceDetailVO);
		hmParam.put("USERID", gmUserVO.getUserid());
		hmParam.put("LANGUAGE","103097"); //English
		alCaseAttrDtls = gmCaseBookRptBean.fetchCaseAttribute(hmParam);

		listGmCaseAttributeVO = gmWSUtil.getVOListFromHashMapList(
				alCaseAttrDtls, gmCaseAttributeVO,
				gmCaseAttributeVO.getCaseAttributeProperties());
		strPageSize +=listGmCaseInfoVO.size();
		
		//Set the page header detail from first hashmap of list.
		gmCaseListVO.populatePageProperties(alCaseAttrDtls);
		gmCaseListVO.setReftype("4000538");
		hmParam.put("PAGESIZE",gmCaseListVO.getPagesize());
		hmParam.put("REFTYPE","4000538"); //Case Attribute Sync type
		hmParam.put("STATUSID","103120"); //Initiate
		
		// To save details of device sync details and sync log.
		gmProdCatBean.saveDeviceSyncDetail(hmParam);
		
		//gmCaseListVO.setListGmCaseInfoVO(listGmCaseInfoVO);
		gmCaseListVO.setListGmCaseAttributeVO(listGmCaseAttributeVO);
		
		alCaseAttrDtls  = null;
		hmParam= null;
		
		return gmCaseListVO;
	}	
	/**loadCaseScheduleReport 
	 * This method is used to fetch the case info for online case schedule report from Device.
	 * @param strInput
	 * @return GmCaseInfoVO
	 * @throws AppError
	 */
	@POST
	@Path("report")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })

	public GmCaseListVO fetcCaseReport(String strInput) throws AppError,
			IOException, JsonGenerationException, JsonMappingException,
			JsonParseException, ParseException {		
		GmCaseListVO gmCaseListVO = new GmCaseListVO();
		GmCaseInfoVO gmCaseInfoVO = new GmCaseInfoVO();
		GmCaseInfoListVO gmCaseInfoListVO = new GmCaseInfoListVO();
		
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean();
		GmAccessFilter gmAccessFilter =new GmAccessFilter();
		GmWSUtil gmWSUtil = new GmWSUtil();
		
		//validate token and parse JSON into VO
		parseInputAndValidateToken(strInput, gmCaseInfoListVO);
		
		ArrayList alResult  = new ArrayList();
		HashMap hmParam= new HashMap();
		HashMap hmReturn= new HashMap();
		
		List<GmCaseInfoVO> listGmCaseInfoVO   =  new ArrayList<GmCaseInfoVO>();
		
		hmParam = GmCommonClass.parseNullHashMap((HashMap) gmProdCatUtil
				.populateSalesParam(gmUserVO, gmCaseInfoListVO));
		//To get the Access Level information 		
		hmReturn = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
		
		hmParam = gmWSUtil.getHashMapFromVO(gmCaseInfoListVO);
		hmParam.put("AccessFilter", hmReturn.get("AccessFilter"));
		hmParam.put("USERID", gmUserVO.getUserid());

		alResult = gmCaseBookRptBean.fetchCaseScheduleRpt(hmParam);
		
		listGmCaseInfoVO = gmWSUtil.getVOListFromHashMapList(alResult,
				gmCaseInfoVO, gmCaseInfoVO.getCaseInfoProperties());
		
		gmCaseListVO.setListGmCaseInfoVO(listGmCaseInfoVO);
		
		alResult  = null;
		hmParam = null;
		hmReturn = null;
		
		return gmCaseListVO;
	}	
	
	/**fetchItemRequestForApproval 
	 * This method is used to fetch the case info for online case schedule report from Device.
	 * @param strInput
	 * @return GmCaseListVO
	 * @throws AppError
	 */
	@POST
	@Path("fetchItemApprvl")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmCaseListVO fetchItemRequestForApproval(String strInput) throws AppError,
			IOException, JsonGenerationException, JsonMappingException,
			JsonParseException, ParseException {
		
		GmCaseListVO gmCaseListVO = new GmCaseListVO();
		GmCaseInfoListVO gmCaseInfoListVO = new GmCaseInfoListVO();
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		GmPendAllocationBean gmPendAllocationBean = new GmPendAllocationBean();
		GmAccessFilter gmAccessFilter =new GmAccessFilter();
		GmWSUtil gmWSUtil = new GmWSUtil();
		ArrayList alProdReqDetails, listGmProdRequestDetVO = null;
		HashMap hmParam = new HashMap();
		HashMap hmReturn = new HashMap();
		
		//validate token and parse JSON into VO
		parseInputAndValidateToken(strInput, gmCaseInfoListVO);

		gmCaseInfoListVO.setUserid(gmUserVO.getUserid());
		String strApplDateFmt = GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT","DATEFORMAT"));
		
		//check for the StrOpt is not empty.
		String strOpt = GmCommonClass.parseNull((String)gmCaseInfoListVO.getStropt());
		if(strOpt.equals("") || !strOpt.equals("400088")){
			throw new AppError("" , "20643",'E');
		}
		hmParam = GmCommonClass.parseNullHashMap((HashMap) gmProdCatUtil
						.populateSalesParam(gmUserVO, gmCaseInfoListVO));
		//To get the Access Level information 		
		hmReturn = GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam));
		
		hmParam = gmWSUtil.getHashMapFromVO(gmCaseInfoListVO);
		String strFrmDate = GmCommonClass.parseNull((String)hmParam.get("FROMDT"));
		String strToDate = GmCommonClass.parseNull((String)hmParam.get("TODT"));
		String strProdReqId = GmCommonClass.parseNull((String)hmParam.get("PRDREQDID"));
		String strUserId = GmCommonClass.parseNull(gmCaseInfoListVO.getUserid());
		String strFieldSales = GmCommonClass.parseNull(gmCaseInfoListVO.getFsids());
		String strAccessFilter = GmCommonClass.parseNull((String)hmReturn.get("AccessFilter"));

		hmParam.put("ACCESSFILTER",strAccessFilter);
		hmParam.put("USERID", strUserId);
		hmParam.put("STARTDT", strFrmDate);
		hmParam.put("ENDDT", strToDate);
		hmParam.put("REQUESTID", strProdReqId);
		hmParam.put("STROPT", strOpt);
		hmParam.put("REQ_STATUS", "2");
		hmParam.put("APPLNDATEFMT", strApplDateFmt);
		hmParam.put("FIELDSALES", strFieldSales);

		alProdReqDetails = GmCommonClass.parseNullArrayList(gmPendAllocationBean.fetchPendingApprlLoanerReqDtl(hmParam));
		//Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo attributes 		
		listGmProdRequestDetVO = (ArrayList) gmWSUtil.getVOListFromHashMapList(alProdReqDetails, (new GmProdRequestDetVO()),
																	new GmProdRequestDetVO().getProdRequestDetailProperties());

		gmCaseListVO.setListGmProdRequestDetVO(listGmProdRequestDetVO);
		
		return gmCaseListVO;
	}
}