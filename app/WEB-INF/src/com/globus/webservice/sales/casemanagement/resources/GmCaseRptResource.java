package com.globus.webservice.sales.casemanagement.resources;

import java.io.IOException;
import java.util.HashMap;

import org.apache.log4j.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.sales.casemanagement.bean.GmCaseRptBean;
import com.globus.webservice.sales.casemanagement.valueobject.GmCaseVO;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

@Path("case")
public class GmCaseRptResource extends GmResource {
	  Logger log = GmLogger.getInstance(this.getClass().getName());

/**
  * This method used to get the account list based on login rep
  * 
  */  
	@POST
	@Path("account")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
		  
	public String fetchAccountList(String strInput) throws AppError, JsonGenerationException,
	JsonMappingException, JsonParseException, IOException, Exception {
	  
	  log.debug("strInput=>"+strInput);
	  ObjectMapper mapper = new ObjectMapper();
	  GmWSUtil gmWSUtil = new GmWSUtil();
	  GmAccessFilter gmAccessFilter = new GmAccessFilter();
	  HashMap hmInput = new HashMap();
	  GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
	  GmCaseVO gmCaseVO = mapper.readValue(strInput, GmCaseVO.class);
	  GmCaseRptBean gmCaseRptBean = new GmCaseRptBean(gmCaseVO);
		validateToken(gmCaseVO.getToken());
		
		gmCaseVO.setUserid(gmUserVO.getUserid());
	
	  gmWSUtil.parseJsonToVO(strInput, gmCaseVO);
	  hmInput = gmWSUtil.getHashMapFromVO(gmCaseVO); 

	  
	// For Override the Access Level of Associate Sales REP to Sales Rep (Associate Sales REP
	// checks with account id)
	  HashMap hmParam =GmCommonClass.parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO,
	            (new GmSalesDashReqVO())));
	  
	    /*
	 * When Associate Rep login. His user id passed as reporting repid. 
	 */
	  hmParam.put("RPT_REPID", gmUserVO.getUserid());
	  hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
	  hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
	  hmInput.put("HMPARAM", hmParam);
	  hmInput.put("USERDEPTID", gmUserVO.getDeptid());
	  log.debug("hmInput>>>>>" + hmInput); 
		
	  String strAccList = gmCaseRptBean.fetchAccountList(hmInput);
	  return strAccList;
	}
	
/**
  * This method used to generate case id when account is selected
  * 
  */
	 @POST
	 @Path("caseidgen")
	 @Produces({MediaType.APPLICATION_JSON})
	 @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
			  
	 public GmCaseVO generateCaseId(String strInput) throws AppError, JsonGenerationException,
		  JsonMappingException, JsonParseException, IOException, Exception {
		    	  
		 log.debug("strInput=>"+strInput);
		 ObjectMapper mapper = new ObjectMapper();
		 GmCaseVO gmCaseVO = mapper.readValue(strInput, GmCaseVO.class);
		 GmCaseRptBean gmCaseRptBean = new GmCaseRptBean(gmCaseVO);
		 validateToken(gmCaseVO.getToken());
		  	      
		 String strSurgDt = GmCommonClass.parseNull((String)gmCaseVO.getSurgdt());
		 String strRepId = GmCommonClass.parseNull((String)gmCaseVO.getUserid());
		 String strCmpdtfmt = GmCommonClass.parseNull((String)gmCaseVO.getCmpdfmt());
		 log.debug("strSurgDt==>"+strSurgDt+"strRepId==>"+strRepId);
		 String strCaseId = gmCaseRptBean.generateCashId(strSurgDt,strRepId,strCmpdtfmt);
		 gmCaseVO.setCaseid(strCaseId);
		 return gmCaseVO;
	 }
		          
/**
  * This method used to fetch set details based on set bundle id
  * 
  */			  
	 @POST
	 @Path("fetchsetbn")
	 @Produces({MediaType.APPLICATION_JSON})
	 @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
			  
	 public String fetchSetDetailsFromSB(String strInput) throws AppError, JsonGenerationException,
		 JsonMappingException, JsonParseException, IOException, Exception {
		    	  
		  log.debug("strInput=>"+strInput);
		  ObjectMapper mapper = new ObjectMapper();
		  GmCaseVO gmCaseVO = mapper.readValue(strInput, GmCaseVO.class);
		  GmCaseRptBean gmCaseRptBean = new GmCaseRptBean(gmCaseVO);
		  validateToken(gmCaseVO.getToken());
		  	      
		  String strSBId = GmCommonClass.parseNull((String) gmCaseVO.getSbid());
		  log.debug("strSBId==>"+strSBId);
		  String strAccList = gmCaseRptBean.fetchSetDetailsFromSB(strSBId);
		  return strAccList;
	 }	
/**
  * This method used to fetch set details based on set bundle id
  * 
  */			  
	 @POST
	 @Path("validatetag")
	 @Produces({MediaType.APPLICATION_JSON})
	 @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
				  
	 public GmCaseVO validateTag(String strInput) throws AppError, JsonGenerationException,
		JsonMappingException, JsonParseException, IOException, Exception {
			    	  
		  log.debug("strInput=>"+strInput);
		  ObjectMapper mapper = new ObjectMapper();
		  GmCaseVO gmCaseVO = mapper.readValue(strInput, GmCaseVO.class);
		  GmCaseRptBean gmCaseRptBean = new GmCaseRptBean(gmCaseVO);	 
		  validateToken(gmCaseVO.getToken());
			  	      
		  String strTagNum = GmCommonClass.parseNull((String) gmCaseVO.getTagid());
		  String strSetId = GmCommonClass.parseNull((String) gmCaseVO.getSetid());
		  log.debug("strTagNum==>"+strTagNum +" strSetId==>"+strSetId);
		  String strTagVal = gmCaseRptBean.validateTag(strTagNum,strSetId);
		  gmCaseVO.setTagfl(strTagVal);
		  log.debug("strTagVal==>"+strTagVal);
		  return gmCaseVO;
	 }	
			      	
/**
  * fetchCaseDashboardData - This method is used to display dashboard data
  *
  */
	@POST
	@Path("dashboard")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
			    		  
	public String fetchCaseDashboardData (String strInput) throws AppError, JsonGenerationException,
		JsonMappingException, JsonParseException, IOException, Exception {
		
		log.debug("dashboard"+strInput);
		ObjectMapper mapper = new ObjectMapper();
		GmCaseVO gmCaseVO = mapper.readValue(strInput, GmCaseVO.class);	 
		validateToken(gmCaseVO.getToken());
		GmCaseRptBean gmCaseRptbean = new GmCaseRptBean(gmCaseVO);
		String strPartyId = GmCommonClass.parseNull((String) gmCaseVO.getPartyid());
		String strCmpdfmt = GmCommonClass.parseNull((String) gmCaseVO.getCmpdfmt());

		String reportData = gmCaseRptbean.fetchCaseDashboardData(strPartyId,strCmpdfmt);
		log.debug("reportData>>>>"+reportData);
		return reportData; 			    		    
	}
	
/**
  * This method used to fetch the case details based on case info id
  * object in front-end as a JSONDATA.
  */	  
	@POST
	@Path("fetchcase")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
			    			  
	public String fetchCaseMapping(String strInput) throws AppError, JsonGenerationException,
		JsonMappingException, JsonParseException, IOException, Exception {
			    				  
		ObjectMapper mapper = new ObjectMapper();
		log.debug("strInput-->"+strInput);
		String strCaseinfoId = "";
		String strCaseDtlJSON = "";
		String strPartyId= "";
		String strCmpdfmt= "";
			    			    
		GmCaseVO gmCaseVO = mapper.readValue(strInput, GmCaseVO.class);
		GmCaseRptBean gmCaseRptbean = new GmCaseRptBean(gmCaseVO);
		validateToken(gmCaseVO.getToken());
			    			    
		strCaseinfoId = GmCommonClass.parseNull((String)gmCaseVO.getCaseinfoid());
		strPartyId = GmCommonClass.parseNull((String)gmCaseVO.getPartyid());
		strCmpdfmt = GmCommonClass.parseNull((String) gmCaseVO.getCmpdfmt());
		log.debug("strCmpdfmt-->"+strCmpdfmt);
		strCaseDtlJSON = gmCaseRptbean.fetchCaseMapping(strCaseinfoId,strPartyId,strCmpdfmt);
		

		return strCaseDtlJSON;
	} 
	/**
	  * This method used to fetch the shared case details based on party id
	  * object in front-end as a JSONDATA.
	  */
		@POST
		@Path("casedtl")
		@Produces({MediaType.APPLICATION_JSON})
		@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
				    		  
		public String fetchCaseDtl (String strInput) throws AppError, JsonGenerationException,
			JsonMappingException, JsonParseException, IOException, Exception {
			
			log.debug("dashboard"+strInput);
			ObjectMapper mapper = new ObjectMapper();
			GmCaseVO gmCaseVO = mapper.readValue(strInput, GmCaseVO.class);	 
			validateToken(gmCaseVO.getToken());
			GmCaseRptBean gmCaseRptbean = new GmCaseRptBean(gmCaseVO);
			String strPartyId = GmCommonClass.parseNull((String) gmCaseVO.getPartyid());
			String strCmpdfmt = GmCommonClass.parseNull((String) gmCaseVO.getCmpdfmt());

			String reportData = gmCaseRptbean.fetchCaseDtlData(strPartyId,strCmpdfmt);
			log.debug("reportData>>>>"+reportData);
			return reportData; 			    		    
		}
		
	/**
	  * This method used to fetch the calendar details based on party id
	  * object in front-end as a JSONDATA.
	  */
		@POST
		@Path("casecalendar")
		@Produces({MediaType.APPLICATION_JSON})
		@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
				    		  
		public String fetchCaseCalendarData (String strInput) throws AppError, JsonGenerationException,
			JsonMappingException, JsonParseException, IOException, Exception {
			
			log.debug("casecalendar"+strInput);
			ObjectMapper mapper = new ObjectMapper();
			GmCaseVO gmCaseVO = mapper.readValue(strInput, GmCaseVO.class);	 
			validateToken(gmCaseVO.getToken());
			GmCaseRptBean gmCaseRptbean = new GmCaseRptBean(gmCaseVO);
			String strPartyId = GmCommonClass.parseNull((String) gmCaseVO.getPartyid());
			String strCmpdfmt = GmCommonClass.parseNull((String) gmCaseVO.getCmpdfmt());

			String reportData = gmCaseRptbean.fetchCaseCalendarData(strPartyId, strCmpdfmt);
			log.debug("casecalendar>>>>"+reportData);
			return reportData; 			    		    
		}
		
	/**
	  * This method used to fetch the calendar details based on party id
	  * object in front-end as a JSONDATA.
	  */
		@POST
		@Path("dashdata")
		@Produces({MediaType.APPLICATION_JSON})
		@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
				    		  
		public String fetchCaseData (String strInput) throws AppError, JsonGenerationException,
			JsonMappingException, JsonParseException, IOException, Exception {
			
			log.debug("fetchCaseData"+strInput);
			ObjectMapper mapper = new ObjectMapper();
			GmCaseVO gmCaseVO = mapper.readValue(strInput, GmCaseVO.class);	 
			validateToken(gmCaseVO.getToken());
			GmCaseRptBean gmCaseRptbean = new GmCaseRptBean(gmCaseVO);
			String strPartyId = GmCommonClass.parseNull((String) gmCaseVO.getPartyid());
			String strCmpdfmt = GmCommonClass.parseNull((String) gmCaseVO.getCmpdfmt());

			String reportData = gmCaseRptbean.fetchCaseData(strPartyId,strCmpdfmt);
			log.debug("fetchCaseData>>>>"+reportData);
			return reportData; 			    		    
		}
}
