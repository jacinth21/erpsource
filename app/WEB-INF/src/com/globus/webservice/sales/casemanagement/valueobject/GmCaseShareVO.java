package com.globus.webservice.sales.casemanagement.valueobject;

import com.globus.valueobject.common.GmDataStoreVO;

public class GmCaseShareVO extends GmDataStoreVO{
	
	private String caseinfoid="";
	private String repid="";
	private String repnm="";
	private String voidfl="";
	
	public String getCaseinfoid() {
		return caseinfoid;
	}
	public void setCaseinfoid(String caseinfoid) {
		this.caseinfoid = caseinfoid;
	}
	public String getRepid() {
		return repid;
	}
	public void setRepid(String repid) {
		this.repid = repid;
	}
	public String getRepnm() {
		return repnm;
	}
	public void setRepnm(String repnm) {
		this.repnm = repnm;
	}
	public String getVoidfl() {
		return voidfl;
	}
	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}
	

}
