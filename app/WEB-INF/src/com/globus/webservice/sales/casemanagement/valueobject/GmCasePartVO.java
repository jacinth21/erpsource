package com.globus.webservice.sales.casemanagement.valueobject;

import com.globus.valueobject.common.GmDataStoreVO;

public class GmCasePartVO extends GmDataStoreVO{
	
	private String caseinfoid="";
	private String kitid="";
	private String kitnm="";
	private String partnum="";
	private String partdesc="";
	private String qty="";
	private String voidfl="";
	
	public String getCaseinfoid() {
		return caseinfoid;
	}
	public void setCaseinfoid(String caseinfoid) {
		this.caseinfoid = caseinfoid;
	}
	public String getKitid() {
		return kitid;
	}
	public void setKitid(String kitid) {
		this.kitid = kitid;
	}
	public String getKitnm() {
		return kitnm;
	}
	public void setKitnm(String kitnm) {
		this.kitnm = kitnm;
	}
	public String getPartnum() {
		return partnum;
	}
	public void setPartnum(String partnum) {
		this.partnum = partnum;
	}
	public String getPartdesc() {
		return partdesc;
	}
	public void setPartdesc(String partdesc) {
		this.partdesc = partdesc;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getVoidfl() {
		return voidfl;
	}
	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}
}
