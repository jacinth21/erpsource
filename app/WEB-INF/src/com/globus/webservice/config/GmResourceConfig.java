package com.globus.webservice.config;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.globus.common.beans.GmLogger;

public class GmResourceConfig extends ResourceConfig {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	public GmResourceConfig() {
		ObjectMapper mapper = new ObjectMapper();
		//mapper.enable(SerializationFeature.INDENT_OUTPUT);
		mapper.setSerializationInclusion(Include.NON_NULL);//null variable will not available in JSON serialization
		JacksonJaxbJsonProvider provider = new JacksonJaxbJsonProvider();
		provider.setMapper(mapper);
		register(provider);
		register(MultiPartFeature.class); //enabling multiplart request (file upload functionality) 
		packages("com.globus.webservice"); //Register Resource class package

	}
}
