package com.globus.webservice.common.master;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.master.beans.GmMasterDataBean;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.valueobject.common.GmDeviceDetailVO;
import com.globus.valueobject.common.master.GmAccountsVO;
import com.globus.valueobject.common.master.GmAcctGpoMapVO;
import com.globus.valueobject.common.master.GmAssocRepMapVO;
import com.globus.valueobject.common.master.GmDistributorVO;
import com.globus.valueobject.common.master.GmMasterDataSyncListVO;
import com.globus.valueobject.common.master.GmSalesRepVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.config.Compress;


/**
 * @author Manikandan
 * 
 */
@Path("master")
public class GmMasterDataResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchAccounts - This method will returns Account values while calling web service. First call,
   * It will returns all Account id(Master Sync) with Account Id will get Account Details after that
   * it will returns updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @Compress
  @POST
  @Path("accounts")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmMasterDataSyncListVO fetchAccounts(String strInput) throws AppError {

    HashMap hmValidate = new HashMap();
    HashMap hmParams = new HashMap();
    ArrayList alAccounts = new ArrayList();
    List<GmAccountsVO> listGmAccountVO = new ArrayList<GmAccountsVO>();

    GmMasterDataSyncListVO gmMasterDataSyncListVO = new GmMasterDataSyncListVO();
    GmDeviceDetailVO gmDeviceDetailVO = new GmDeviceDetailVO();

    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmAccountsVO gmAccountsVO = new GmAccountsVO();
    GmWSUtil gmWSUtil = new GmWSUtil();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmDeviceDetailVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmDeviceDetailVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID
    gmProdCatUtil.validateInput(gmDeviceDetailVO, hmValidate);
    // To get Page Information details all the below values are passed to procedure.
    // op changess start
    GmMasterDataBean gmMasterDataBean = new GmMasterDataBean(gmDeviceDetailVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmDeviceDetailVO);
    // op changess end
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmDeviceDetailVO.getToken());
    hmParams.put("UUID", GmCommonClass.parseNull(gmDeviceDetailVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));

    hmParams.put("VO", gmAccountsVO);
    listGmAccountVO = gmMasterDataBean.fetchAccountsDetail(hmParams);
    gmAccountsVO = (GmAccountsVO) gmMasterDataSyncListVO.setPageProperties(listGmAccountVO);
    gmAccountsVO = (gmAccountsVO == null) ? new GmAccountsVO() : gmAccountsVO;

    gmMasterDataSyncListVO.setReftype("4000519"); // Address sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmMasterDataSyncListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmMasterDataSyncListVO.getPagesize()));
    hmParams.put("LASTREC", gmAccountsVO.getAcctid()); // ACCTID

    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);
    gmMasterDataSyncListVO.setListGmAccountVO(listGmAccountVO);

    return gmMasterDataSyncListVO;

  }

  /**
   * fetchSalesReps - This method will returns Sales Rep values while calling web service. First
   * call, It will returns all Sales Rep values(Master Sync) which are Active based on Sales Rep Id
   * after that it will returns updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @Compress
  @POST
  @Path("salesreps")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmMasterDataSyncListVO fetchSalesReps(String strInput) throws AppError {
    HashMap hmValidate = new HashMap();
    HashMap hmParams = new HashMap();
    ArrayList alSalesRep = new ArrayList();
    List<GmSalesRepVO> listGmSalesRepVO = new ArrayList<GmSalesRepVO>();

    GmMasterDataSyncListVO gmMasterDataSyncListVO = new GmMasterDataSyncListVO();
    GmDeviceDetailVO gmDeviceDetailVO = new GmDeviceDetailVO();

    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmSalesRepVO gmSalesRepVO = new GmSalesRepVO();
    GmWSUtil gmWSUtil = new GmWSUtil();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmDeviceDetailVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmDeviceDetailVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID
    gmProdCatUtil.validateInput(gmDeviceDetailVO, hmValidate);
    // op changess start
    GmMasterDataBean gmMasterDataBean = new GmMasterDataBean(gmDeviceDetailVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmDeviceDetailVO);
    // op changess end

    // To get Page Information details all the below values are passed to procedure.
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmDeviceDetailVO.getToken());
    hmParams.put("UUID", GmCommonClass.parseNull(gmDeviceDetailVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("VO", gmSalesRepVO);
    listGmSalesRepVO = gmMasterDataBean.fetchSalesRepsDetail(hmParams);
    gmSalesRepVO = (GmSalesRepVO) gmMasterDataSyncListVO.setPageProperties(listGmSalesRepVO);
    gmSalesRepVO = (gmSalesRepVO == null) ? new GmSalesRepVO() : gmSalesRepVO;

    gmMasterDataSyncListVO.setReftype("4000520"); // Sales Rep sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmMasterDataSyncListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmMasterDataSyncListVO.getPagesize()));
    hmParams.put("LASTREC", gmSalesRepVO.getRepid()); // REPID

    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);
    gmMasterDataSyncListVO.setListGmSalesRepVO(listGmSalesRepVO);

    return gmMasterDataSyncListVO;

  }


  /**
   * fetchDistributors - This method will returns Distributor values while calling web service.
   * First call, It will returns all Keyword values(Master Sync) which are Active based on
   * Distributor id. after that it will returns updated values only based on last sync for the
   * device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @Compress
  @POST
  @Path("distributors")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmMasterDataSyncListVO fetchDistributors(String strInput) throws AppError {

    HashMap hmValidate = new HashMap();
    HashMap hmParams = new HashMap();
    ArrayList alDistributor = new ArrayList();
    List<GmDistributorVO> listGmDistributorVO = new ArrayList<GmDistributorVO>();

    GmMasterDataSyncListVO gmMasterDataSyncListVO = new GmMasterDataSyncListVO();
    GmDeviceDetailVO gmDeviceDetailVO = new GmDeviceDetailVO();

    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmDistributorVO gmDistributorVO = new GmDistributorVO();
    GmWSUtil gmWSUtil = new GmWSUtil();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmDeviceDetailVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmDeviceDetailVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID
    gmProdCatUtil.validateInput(gmDeviceDetailVO, hmValidate);
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmDeviceDetailVO.getToken());
    // op changess start
    GmMasterDataBean gmMasterDataBean = new GmMasterDataBean(gmDeviceDetailVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmDeviceDetailVO);
    // op changess end
    // To get Page Information details all the below values are passed to procedure.
    hmParams.put("UUID", GmCommonClass.parseNull(gmDeviceDetailVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("VO", gmDistributorVO);
    listGmDistributorVO = gmMasterDataBean.fetchDistributorsDetail(hmParams);
    gmDistributorVO =
        (GmDistributorVO) gmMasterDataSyncListVO.setPageProperties(listGmDistributorVO);
    gmDistributorVO = (gmDistributorVO == null) ? new GmDistributorVO() : gmDistributorVO;

    gmMasterDataSyncListVO.setReftype("4000521"); // Distributor sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmMasterDataSyncListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmMasterDataSyncListVO.getPagesize()));
    hmParams.put("LASTREC", gmDistributorVO.getDid()); // DID

    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);
    gmMasterDataSyncListVO.setListGmDistributorVO(listGmDistributorVO);

    return gmMasterDataSyncListVO;
  }

  /**
   * fetchSalesReps - This method will returns Keywords values while calling web service. First
   * call, It will returns all Keyword values(Master Sync) which are published to product catalog
   * after that it will returns updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @Compress
  @POST
  @Path("acctgpomap")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmMasterDataSyncListVO fetchAcctGpoMapping(String strInput) throws AppError {

    HashMap hmValidate = new HashMap();
    HashMap hmParams = new HashMap();
    ArrayList alResult = new ArrayList();
    List<GmAcctGpoMapVO> listGmAcctGpoMapVO = new ArrayList<GmAcctGpoMapVO>();

    GmMasterDataSyncListVO gmMasterDataSyncListVO = new GmMasterDataSyncListVO();
    GmDeviceDetailVO gmDeviceDetailVO = new GmDeviceDetailVO();

    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmAcctGpoMapVO gmAcctGpoMapVO = new GmAcctGpoMapVO();
    GmWSUtil gmWSUtil = new GmWSUtil();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmDeviceDetailVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmDeviceDetailVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID
    gmProdCatUtil.validateInput(gmDeviceDetailVO, hmValidate);
    // op changess start
    GmMasterDataBean gmMasterDataBean = new GmMasterDataBean(gmDeviceDetailVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmDeviceDetailVO);
    // op changess end

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmDeviceDetailVO.getToken());
    // To get Page Information details all the below values are passed to procedure.
    hmParams.put("UUID", GmCommonClass.parseNull(gmDeviceDetailVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));

    hmParams.put("VO", gmAcctGpoMapVO);
    listGmAcctGpoMapVO = gmMasterDataBean.fetchAcctGpoMapDetail(hmParams);
    gmAcctGpoMapVO = (GmAcctGpoMapVO) gmMasterDataSyncListVO.setPageProperties(listGmAcctGpoMapVO);
    gmAcctGpoMapVO = (gmAcctGpoMapVO == null) ? new GmAcctGpoMapVO() : gmAcctGpoMapVO;

    gmMasterDataSyncListVO.setReftype("4000530"); // Account Gpo Map sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmMasterDataSyncListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmMasterDataSyncListVO.getPagesize()));
    hmParams.put("LASTREC", gmAcctGpoMapVO.getAcctgpomapid()); // ACCTGPOMAPID

    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);
    gmMasterDataSyncListVO.setListGmAcctGpoMapVO(listGmAcctGpoMapVO);

    return gmMasterDataSyncListVO;

  }

  /**
   * fetchSalesReps - This method will returns User values while calling web service. First call, It
   * will returns all User values(Master Sync) which are Active based on User Id after that it will
   * returns updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @Compress
  @POST
  @Path("users")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmMasterDataSyncListVO fetchUsers(String strInput) throws AppError {
    HashMap hmValidate = new HashMap();
    HashMap hmParams = new HashMap();
    ArrayList alUser = new ArrayList();
    List<GmUserVO> listGmUserVO = new ArrayList<GmUserVO>();

    GmMasterDataSyncListVO gmMasterDataSyncListVO = new GmMasterDataSyncListVO();
    GmDeviceDetailVO gmDeviceDetailVO = new GmDeviceDetailVO();

    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmUserVO gmUserVOTemp = new GmUserVO();
    GmWSUtil gmWSUtil = new GmWSUtil();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmDeviceDetailVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmDeviceDetailVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID
    gmProdCatUtil.validateInput(gmDeviceDetailVO, hmValidate);
    // op changess start
    GmMasterDataBean gmMasterDataBean = new GmMasterDataBean(gmDeviceDetailVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmDeviceDetailVO);
    // op changess end
    // To get Page Information details all the below values are passed to procedure.
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmDeviceDetailVO.getToken());
    hmParams.put("UUID", GmCommonClass.parseNull(gmDeviceDetailVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));

    alUser = GmCommonClass.parseNullArrayList(gmMasterDataBean.fetchUserDetail(hmParams));
    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmUserVO =
        gmWSUtil
            .getVOListFromHashMapList(alUser, gmUserVOTemp, gmUserVOTemp.getMappingProperties());
    // Set the page header detail from first hashmap of list.
    gmMasterDataSyncListVO.populatePageProperties(alUser, "USERID");
    gmMasterDataSyncListVO.setReftype("4000535"); // User sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmMasterDataSyncListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmMasterDataSyncListVO.getPagesize()));
    hmParams.put("LASTREC", gmMasterDataSyncListVO.getLastrec());

    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);
    gmMasterDataSyncListVO.setListGmUserVO(listGmUserVO);

    return gmMasterDataSyncListVO;

  }

  /**
   * fetchASSalesReps - This method will returns User values while calling web service. First call,
   * It will returns all User values(Master Sync) which are Active based on User Id after that it
   * will returns updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @Compress
  @POST
  @Path("assocreps")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmMasterDataSyncListVO fetchAssocReps(String strInput) throws AppError {
    HashMap hmValidate = new HashMap();
    HashMap hmParams = new HashMap();
    ArrayList alAssocRep = new ArrayList();
    List<GmAssocRepMapVO> listGmAssocRepMapVO = new ArrayList<GmAssocRepMapVO>();

    GmMasterDataSyncListVO gmMasterDataSyncListVO = new GmMasterDataSyncListVO();
    GmDeviceDetailVO gmDeviceDetailVO = new GmDeviceDetailVO();

    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmAssocRepMapVO gmAssocRepMapVO = new GmAssocRepMapVO();
    GmWSUtil gmWSUtil = new GmWSUtil();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmDeviceDetailVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmDeviceDetailVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID
    gmProdCatUtil.validateInput(gmDeviceDetailVO, hmValidate);
    // op changess start
    GmMasterDataBean gmMasterDataBean = new GmMasterDataBean(gmDeviceDetailVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmDeviceDetailVO);
    // op changess end
    // To get Page Information details all the below values are passed to procedure.
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmDeviceDetailVO.getToken());
    hmParams.put("UUID", GmCommonClass.parseNull(gmDeviceDetailVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("VO", gmAssocRepMapVO);
    listGmAssocRepMapVO = gmMasterDataBean.fetchAssocRepDetail(hmParams);
    gmAssocRepMapVO =
        (GmAssocRepMapVO) gmMasterDataSyncListVO.setPageProperties(listGmAssocRepMapVO);
    gmAssocRepMapVO = (gmAssocRepMapVO == null) ? new GmAssocRepMapVO() : gmAssocRepMapVO;
    gmMasterDataSyncListVO.setReftype("4000629"); // User sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmMasterDataSyncListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmMasterDataSyncListVO.getPagesize()));
    hmParams.put("LASTREC", gmAssocRepMapVO.getAccasrepmapid()); // ACCASREPMAPID

    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);
    gmMasterDataSyncListVO.setListGmAssocRepMapVO(listGmAssocRepMapVO);

    return gmMasterDataSyncListVO;

  }
}
