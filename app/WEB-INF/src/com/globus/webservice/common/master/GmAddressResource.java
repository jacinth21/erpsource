package com.globus.webservice.common.master;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.party.beans.GmAddressBean;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.sales.beans.GmSalesMappingBean;
import com.globus.valueobject.common.GmDeviceDetailVO;
import com.globus.valueobject.common.master.GmAddressVO;
import com.globus.valueobject.common.master.GmMasterDataSyncListVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.config.Compress;

/**
 * @author Manikandan
 * 
 */
@Path("address")
public class GmAddressResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchAddress - This method will returns Address values while calling web service. First call,
   * It will returns all Keyword values(Master Sync) which are Active with respect to Account ID
   * after that it will returns updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @Compress
  @POST
  @Path("master")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmMasterDataSyncListVO fetchAddress(String strInput) throws AppError {

    HashMap hmValidate = new HashMap();
    HashMap hmParams = new HashMap();
    ArrayList alAddress = new ArrayList();
    List<GmAddressVO> listGmAddressVO = new ArrayList<GmAddressVO>();

    GmMasterDataSyncListVO gmMasterDataSyncListVO = new GmMasterDataSyncListVO();
    GmDeviceDetailVO gmDeviceDetailVO = new GmDeviceDetailVO();

    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmAddressVO gmAddressVO = new GmAddressVO();
    GmWSUtil gmWSUtil = new GmWSUtil();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmDeviceDetailVO);
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID
    gmProdCatUtil.validateInput(gmDeviceDetailVO, hmValidate);
    // To get Page Information details all the below values are passed to procedure.
    // op changess start
    GmAddressBean gmAddressBean = new GmAddressBean(gmDeviceDetailVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmDeviceDetailVO);
    // op changess end
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmDeviceDetailVO.getToken());
    hmParams.put("UUID", GmCommonClass.parseNull(gmDeviceDetailVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmDeviceDetailVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmDeviceDetailVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmDeviceDetailVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    alAddress = GmCommonClass.parseNullArrayList(gmAddressBean.fetchAddressDetail(hmParams));
    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmAddressVO =
        gmWSUtil.getVOListFromHashMapList(alAddress, gmAddressVO,
            gmAddressVO.getAddressProperties());
    // Set the page header detail from first hashmap of list.
    gmMasterDataSyncListVO.populatePageProperties(alAddress, "ADDRID");
    gmMasterDataSyncListVO.setReftype("4000522"); // Address sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmMasterDataSyncListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmMasterDataSyncListVO.getPagesize()));
    hmParams.put("LASTREC", gmMasterDataSyncListVO.getLastrec());

    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);
    gmMasterDataSyncListVO.setListGmAddressVO(listGmAddressVO);

    return gmMasterDataSyncListVO;

  }

  /**
   * SaveAddress - This method will Save Address values in to Db while calling web service. After
   * Saving , it will send a success mail to Customer Service Associate and Sales Admin and a copy
   * will sent to Sales Rep If any Exception is thrown a mail will sent to Sales Rep.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError,Exception
   */
  @Compress
  @POST
  @Path("syncaddr")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmAddressVO SaveAddress(String strInput) throws AppError, Exception {

    // HashMap hmValidate = new HashMap();
    HashMap hmParams = new HashMap();
    ArrayList alAddress = new ArrayList();
    List<GmAddressVO> listGmAddressVO = new ArrayList<GmAddressVO>();
    GmSalesMappingBean gmSalesMappingBean = new GmSalesMappingBean();
    GmAddressBean gmAddressBean = new GmAddressBean();
    GmAddressVO gmAddressVO = new GmAddressVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmAddressVO);
    String struserName = "";
    String struserid = "";
    String strfirstName = "";
    String strLastName = "";
    String strRepEmailId = "";

    struserid = GmCommonClass.parseNull(gmUserVO.getUserid());
    strfirstName = GmCommonClass.parseNull(gmUserVO.getFname());
    strLastName = GmCommonClass.parseNull(gmUserVO.getLname());
    struserName = strfirstName.concat(strLastName);
    // The Below method is called for Converting VO into HashMap.
    hmParams = gmWSUtil.getHashMapFromVO(gmAddressVO);
    hmParams.put("USERNAME", struserName);
    hmParams.put("USERTYPE", "4121");// 4121 - Type is for Sales Rep
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("HADDRESSID", GmCommonClass.parseNull(gmAddressVO.getAddrid()));
    strRepEmailId = GmCommonClass.parseNull(gmSalesMappingBean.getCSShipEmail(hmParams));

    hmParams.put("REPEMAIL", strRepEmailId);
    try {
      String strAddressId = gmAddressBean.saveAddressInfo(hmParams);
      gmAddressVO.setAddrid(strAddressId);
      hmParams.put("ADDRID", strAddressId);
      // If the data's are successfully saved into server, Customer Service,Sales Admin and Sales
      // Rep Will be notified by an E-mail.The below method is called to send an email
      gmAddressBean.sendNewAddressEmail(hmParams);
      return gmAddressVO;
    } catch (Exception e) {
      // If any Exception Thrown,Sales Rep Will be notified by an E-mail.The below method is called
      // to send an Exception email
      hmParams.put("EXCEPTION", e.getMessage());
      gmAddressBean.sendAddressExceptionEmail(hmParams);
      throw e;
    }
  }
}
