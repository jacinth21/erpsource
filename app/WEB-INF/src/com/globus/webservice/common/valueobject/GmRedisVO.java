package com.globus.webservice.common.valueobject;


import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmRedisVO extends GmDataStoreVO {

  private String srchkey = "";
  private String maxcount = "200";
  private String activefl = "";
  private String acid = "";
  private String type = "";
public String getType() {
	return type;
}
public void setType(String type) {
	this.type = type;
}
public String getSrchkey() {
	return srchkey;
}
public void setSrchkey(String srchkey) {
	this.srchkey = srchkey;
}
public String getMaxcount() {
	return maxcount;
}
public void setMaxcount(String maxcount) {
	this.maxcount = maxcount;
}
public String getActivefl() {
	return activefl;
}
public void setActivefl(String activefl) {
	this.activefl = activefl;
}
public String getAcid() {
	return acid;
}
public void setAcid(String acid) {
	this.acid = acid;
}
 
}