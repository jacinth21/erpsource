package com.globus.webservice.common.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.valueobject.common.GmPartyContactVO;
import com.globus.valueobject.common.GmPartyVO;
import com.globus.valueobject.common.GmProdCatlReqVO;
import com.globus.valueobject.common.GmProdCatlResListVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.webservice.config.Compress;

@Path("party")
public class GmPartyResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchParties - This method will returns party id values while calling web service. First call,
   * It will returns all party id which are mapped to Rule Group ID PARTY_TYPE (7000) after that it
   * will returns updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  // for party id
  @Compress
  @Path("master")
  @POST
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlResListVO fetchParties(String strInput) throws AppError {

    HashMap hmParams = new HashMap();
    HashMap hmValidate = new HashMap();
    ArrayList alResult = new ArrayList();
    List<GmPartyVO> listGmPartyVO = new ArrayList<GmPartyVO>();

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmPartyVO gmPartyVO = new GmPartyVO();


    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmProdCatlReqVO gmProdCatlReqVO = new GmProdCatlReqVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlReqVO);
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlReqVO, hmValidate);
    GmPartyBean gmPartyBean = new GmPartyBean(gmProdCatlReqVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmProdCatlReqVO);
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlReqVO.getToken());

    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlReqVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlReqVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlReqVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlReqVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("VO", gmPartyVO);
    listGmPartyVO = gmPartyBean.fetchParties(hmParams);
    gmPartyVO = (GmPartyVO) gmProdCatlResListVO.setPageProperties(listGmPartyVO);
    gmPartyVO = (gmPartyVO == null) ? new GmPartyVO() : gmPartyVO;

    gmProdCatlResListVO.setReftype("4000445");// Party List sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmProdCatlResListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmProdCatlResListVO.getPagesize()));
    hmParams.put("LASTREC", gmPartyVO.getPartyid()); // PARTYID

    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmProdCatlResListVO.setListGmPartyVO(listGmPartyVO);

    return gmProdCatlResListVO;
  }

  /**
   * fetchPartyContact - This method will returns party id values while calling web service. First
   * call, It will returns all party id which are mapped to Rule Group ID CONTACT_MODE (90452) after
   * that it will returns updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  // for contact
  @Compress
  @Path("contact")
  @POST
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlResListVO fetchPartyContact(String strInput) throws AppError {

    HashMap hmParams = new HashMap();
    HashMap hmValidate = new HashMap();
    ArrayList alResult = new ArrayList();
    List<GmPartyContactVO> listGmPartyContactVO = new ArrayList<GmPartyContactVO>();

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmPartyContactVO gmPartyContactVO = new GmPartyContactVO();

    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmProdCatlReqVO gmProdCatlReqVO = new GmProdCatlReqVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlReqVO);
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");

    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlReqVO, hmValidate);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmProdCatlReqVO);
    GmPartyBean gmPartyBean = new GmPartyBean(gmProdCatlReqVO);
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlReqVO.getToken());

    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlReqVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlReqVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlReqVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlReqVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("VO", gmPartyContactVO);
    listGmPartyContactVO = gmPartyBean.fetchPartyContacts(hmParams);
    gmPartyContactVO =
        (GmPartyContactVO) gmProdCatlResListVO.setPageProperties(listGmPartyContactVO);
    gmPartyContactVO = (gmPartyContactVO == null) ? new GmPartyContactVO() : gmPartyContactVO;


    gmProdCatlResListVO.setReftype("4000448");// Party Contact sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmProdCatlResListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmProdCatlResListVO.getPagesize()));
    hmParams.put("LASTREC", gmPartyContactVO.getContactid()); // CONTACTID

    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmProdCatlResListVO.setListGmPartyContactVO(listGmPartyContactVO);

    return gmProdCatlResListVO;
  }
}
