package com.globus.webservice.common.resources;

import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmCommonCancelServlet;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.common.GmCommonCancelVO;

@Path("cancel")
public class GmCommonCancelResource extends GmResource{
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**cancelTransaction - This method will cancel/reject the transaction by taking the input as proper comments
	 * ,cancel reason and transaction id's.   
	 * @param strInput
	 * @return GmCommonCancelVO
	 * @throws AppError
	 */
	@POST
	@Path("canceltransaction")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmCommonCancelVO  cancelTransaction(String strInput) throws AppError,Exception{
		
		GmCommonCancelVO gmCommonCancelVO = new GmCommonCancelVO();
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmCommonCancelServlet  gmCommonCancelServlet = new GmCommonCancelServlet();
		HashMap hmParam = new HashMap();
		String strOpt = "";
		String strCancelReason = "";
		String strCancelGrp = "";
		String strTxnIds = "";
		String strComments = "";
		String strUserId = "";
		log.debug("===============cancelTransaction================");
		//validate token and parse JSON into VO
		parseInputAndValidateToken(strInput, gmCommonCancelVO);
		gmCommonCancelVO.setUserid(gmUserVO.getUserid());
		log.debug("===============validate===============");
		strOpt = GmCommonClass.parseNull((String)gmCommonCancelVO.getStropt());
		strCancelReason = GmCommonClass.parseNull((String)gmCommonCancelVO.getCancelreasonid());
		strCancelGrp = GmCommonClass.parseNull((String)gmCommonCancelVO.getCancelgrp());
		strTxnIds = GmCommonClass.parseNull((String)gmCommonCancelVO.getTxnids());
		strComments = GmCommonClass.parseNull((String)gmCommonCancelVO.getComments());
		
		//check for the StrOpt is not empty.
		if(strOpt.equals("") || !strOpt.equals("cancel")){
			throw new AppError("" , "20643",'E');
		}
		//check for the strCancelReason is not empty.
		if(strCancelReason.equals("")){
			throw new AppError("" , "20648",'E');
		}
		//check for the strCancelGrp is not empty.
		if(strCancelGrp.equals("")){
			throw new AppError("" , "20645",'E');
		}
		//check for the strTxnIds is not empty.
		if(strTxnIds.equals("")){
			throw new AppError("" , "20646",'E');
		}
		//check for the strComments is not empty.
		if(strComments.equals("")){
			throw new AppError("" , "20647",'E');
		}
		
		hmParam = gmWSUtil.getHashMapFromVO(gmCommonCancelVO);
		
		strCancelReason = GmCommonClass.parseNull((String)hmParam.get("CANCELREASONID"));
		strCancelGrp = GmCommonClass.parseNull((String)hmParam.get("CANCELGRP"));
		strTxnIds = GmCommonClass.parseNull((String)hmParam.get("TXNIDS"));
		strComments = GmCommonClass.parseNull((String)hmParam.get("COMMENTS"));       
		strUserId = GmCommonClass.parseNull(gmCommonCancelVO.getUserid());
		
		hmParam.put("TXNID",strTxnIds);
        hmParam.put("CANCELREASON",strCancelReason);
        hmParam.put("CANCELTYPE",strCancelGrp);
        hmParam.put("COMMENTS",strComments);
        hmParam.put("USERID",strUserId);
		
        gmCommonCancelServlet.processCancel(hmParam);
		
		return gmCommonCancelVO;
	}
}
