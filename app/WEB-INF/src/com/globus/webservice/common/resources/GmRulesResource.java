/**
 * 
 */
package com.globus.webservice.common.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmRuleBean;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.operations.shipping.beans.GmShipScanReqRptBean;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.valueobject.common.GmCommonListVO;
import com.globus.valueobject.common.GmProdCatlReqVO;
import com.globus.valueobject.common.GmProdCatlResListVO;
import com.globus.valueobject.common.GmRulesVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.operations.GmTxnRulesVO;
import com.globus.valueobject.operations.shipping.GmShipScanReqVO;
import com.globus.webservice.config.Compress;

/**
 * @author vprasath
 * 
 */
@Path("rules")
public class GmRulesResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  // for post request
  @Path("fetchrules")
  @POST
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCommonListVO fetchRuleConsequences(String strInput) throws AppError {
	  GmCommonListVO gmCommonListVO = new GmCommonListVO();
    Map hmMap = new HashMap();
    GmWSUtil gmWSUtil = new GmWSUtil();
    List<GmTxnRulesVO> gmTxnRulesVOList = new ArrayList<GmTxnRulesVO>();
    GmTxnRulesVO gmTxnRulesVO = new GmTxnRulesVO();

    hmMap = gmWSUtil.parseJsonToMap(strInput);
    GmRuleBean gmRuleBean = new GmRuleBean();
    ArrayList alList = gmRuleBean.fetchRulesForTransaction(hmMap);
    gmTxnRulesVOList =
        gmWSUtil
            .getVOListFromHashMapList(alList, gmTxnRulesVO, gmTxnRulesVO.getMappingProperties());

    log.debug("alReturnList: " + alList);
    gmCommonListVO.setGmTxnRulesVO(gmTxnRulesVOList);
    return gmCommonListVO;
  }

  // for post request
  @Path("printshippingrules")
  @POST
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public void printShippingRules(String strInput) throws AppError {

    GmShipScanReqVO gmShipScanReqVO = new GmShipScanReqVO();
    parseInputAndValidateToken(strInput, gmShipScanReqVO);
    GmRuleBean gmRuleBean = new GmRuleBean();
    GmShipScanReqRptBean gmShipScanReqRptBean = new GmShipScanReqRptBean();

    HashMap hmRuleParam = new HashMap();
    String strTxnId = gmShipScanReqVO.getTxnid();
    String strSource = gmShipScanReqVO.getSource();
    String strUserId = gmShipScanReqVO.getUserid();
    hmRuleParam = gmShipScanReqRptBean.fetchShippingRuleParams(strTxnId, strSource, "VERIFY");
    hmRuleParam.put("USERID", strUserId);
    gmRuleBean.printRulesForTransaction(hmRuleParam);

  }

  /**
   * fetchSystems - This method will returns RULE values while calling web service. First call, It
   * will returns all RULE values which are mapped to Rule Group ID SHAREGROUP after that it will
   * returns updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  // for Rule Lookup
  @Compress
  @Path("rulelookup")
  @POST
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlResListVO fetchRules(String strInput) throws AppError {

    HashMap hmParams = new HashMap();
    HashMap hmValidate = new HashMap();
    ArrayList alResult = new ArrayList();
    List<GmRulesVO> listGmRulesVO = new ArrayList<GmRulesVO>();

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmRulesVO gmRulesVO = new GmRulesVO();
    GmRuleBean gmRuleBean = new GmRuleBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();
    GmProdCatlReqVO gmProdCatlReqVO = new GmProdCatlReqVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlReqVO);
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlReqVO, hmValidate);
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlReqVO.getToken());
    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlReqVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlReqVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlReqVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlReqVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));

    alResult = GmCommonClass.parseNullArrayList(gmRuleBean.fetchRules(hmParams));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmRulesVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmRulesVO, gmRulesVO.getRulesProperties());

    // Set the page header detail from first hashmap of list.
    gmProdCatlResListVO.populatePageProperties(alResult, "RULESEQID");

    gmProdCatlResListVO.setReftype("4000443");// RULE GROUP sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmProdCatlResListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmProdCatlResListVO.getPagesize()));
    hmParams.put("LASTREC", gmProdCatlResListVO.getLastrec());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmProdCatlResListVO.setListGmRulesVO(listGmRulesVO);

    return gmProdCatlResListVO;
  }

}
