package com.globus.webservice.common.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCodeLookupBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.valueobject.common.GmCodeLookupAccessVO;
import com.globus.valueobject.common.GmCodeLookupExclusionVO;
import com.globus.valueobject.common.GmCodeLookupVO;
import com.globus.valueobject.common.GmProdCatlResListVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.webservice.config.Compress;

@Path("codelookup")
public class GmCodeLookupResource extends GmResource {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchCodeLookup - This method will returns code lookup values while calling web service. First
   * call
   * 
   * @param strInput
   * @return JSON string
   * @throws AppError
   */
  @Compress
  @POST
  @Path("master")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlResListVO fetchCodeLookup(String strInput) throws AppError {
    log.debug("=======fetchCodeLookup======");
    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    // validate device UUID and language is not empty
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("uuid", "20608");
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCodeLookupVO gmCodeLookupVO = new GmCodeLookupVO();
    GmCodeLookupBean gmCodeLookupBean = new GmCodeLookupBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmCodeLookupVO> alGmCodeLookupVO = new ArrayList<GmCodeLookupVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", "103097"); // For CodeLookup language should be English only.
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));

    alResult = GmCommonClass.parseNullArrayList(gmCodeLookupBean.fetchCodeLookup(hmParams));

    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    alGmCodeLookupVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmCodeLookupVO,
            gmCodeLookupVO.getCodeLookupProperties());

    // Set the page header detail from first hashmap of list.
    gmProdCatlResListVO.populatePageProperties(alResult, "CODEID");
    gmProdCatlResListVO.setReftype("103107");// CodeLookup sync type

    hmParams.put("REFTYPE", gmProdCatlResListVO.getReftype());
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", gmProdCatlResListVO.getPagesize());
    hmParams.put("LASTREC", gmProdCatlResListVO.getLastrec());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);
    gmProdCatlResListVO.setListGmCodeLookupVO(alGmCodeLookupVO);

    return gmProdCatlResListVO;
  }


  /**
   * fetchCodeLookupExclusion - This method will returns code lookup exclusion values while calling
   * web service. First call, It will returns all code lookup values which are mapped to product
   * catalog after that it will returns updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON string
   * @throws AppError
   */
  @Compress
  @POST
  @Path("codelookupexcl")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlResListVO fetchCodeLookupExclusion(String strInput) throws AppError {
    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    // validate device UUID and language is not empty
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("uuid", "20608");
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCodeLookupBean gmCodeLookupBean = new GmCodeLookupBean(gmProdCatlResListVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean();
    GmCodeLookupExclusionVO gmCodeLookupExclusionVO = new GmCodeLookupExclusionVO();
    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());
    List<GmCodeLookupExclusionVO> listCodeLookupExclusionVO =
        new ArrayList<GmCodeLookupExclusionVO>();

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", "103097"); // For CodeLookup language should be English only.
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));

    alResult =
        GmCommonClass.parseNullArrayList(gmCodeLookupBean.fetchCodeLookupExclusion(hmParams));
    log.debug("alResult in codelookupexcl -------->" + alResult);
    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listCodeLookupExclusionVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmCodeLookupExclusionVO,
            gmCodeLookupExclusionVO.getCodeLookupExclusionProperties());
    // Set the page header detail from first hashmap of list.
    gmProdCatlResListVO.populatePageProperties(alResult, "CODEID");
    gmProdCatlResListVO.setReftype("26230802");// CodeLookup Exclusion sync type

    hmParams.put("REFTYPE", gmProdCatlResListVO.getReftype());
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", gmProdCatlResListVO.getPagesize());
    hmParams.put("LASTREC", gmProdCatlResListVO.getLastrec());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);
    gmProdCatlResListVO.setListGmCodeLookupExclusionVO(listCodeLookupExclusionVO);

    return gmProdCatlResListVO;
  }

  /**
   * fetchCodeLookupExclusion - This method will returns code lookup exclusion values while calling
   * web service. First call, It will returns all code lookup values which are mapped to product
   * catalog after that it will returns updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON string
   * @throws AppError
   */
  @Compress
  @POST
  @Path("codelookupaccess")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlResListVO fetchCodeLookupAccessList(String strInput) throws AppError {
    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    // validate device UUID and language is not empty
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("uuid", "20608");
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCodeLookupBean gmCodeLookupBean = new GmCodeLookupBean(gmProdCatlResListVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean();
    GmCodeLookupAccessVO gmCodeLookupAccessVO = new GmCodeLookupAccessVO();
    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());
    List<GmCodeLookupAccessVO> listGmCodeLkpAccessVO = new ArrayList<GmCodeLookupAccessVO>();

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", "103097"); // For CodeLookup language should be English only.
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));

    alResult =
        GmCommonClass.parseNullArrayList(gmCodeLookupBean.fetchCodeLookupAccessList(hmParams));
    log.debug("alResult in codelookupAccess -------->" + alResult);
    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmCodeLkpAccessVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmCodeLookupAccessVO,
            gmCodeLookupAccessVO.getCodeLookupAccessProperties());
    // Set the page header detail from first hashmap of list.
    gmProdCatlResListVO.populatePageProperties(alResult, "CODEID");
    gmProdCatlResListVO.setReftype("26230804");// 26230804:CodeLookup Access sync type

    hmParams.put("REFTYPE", gmProdCatlResListVO.getReftype());
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", gmProdCatlResListVO.getPagesize());
    hmParams.put("LASTREC", gmProdCatlResListVO.getLastrec());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);
    gmProdCatlResListVO.setListGmCodeLkpAccessVO(listGmCodeLkpAccessVO);

    return gmProdCatlResListVO;
  }
}
