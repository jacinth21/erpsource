package com.globus.webservice.common.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCodeLookupBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.logon.beans.GmLogonBean;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.valueobject.common.GmCodeLookupVO;
import com.globus.valueobject.common.GmProdCatlResListVO;
import com.globus.valueobject.logon.GmSecurityEventVO;
import com.globus.valueobject.logon.GmUserVO;

@Path("securityevent")
public class GmSecurityEventResource extends GmResource{
	
	 Logger log = GmLogger.getInstance(this.getClass().getName());
	 
	  @POST
	  @Path("securityeventlist")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public GmProdCatlResListVO fetchSecurityEvents(String strInput)
			throws AppError {
		log.debug("=======fetchCodeLookup======");
		GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

		// validate token and parse JSON into VO
		parseInputAndValidateToken(strInput, gmProdCatlResListVO);
		// validate device UUID and language is not empty
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		HashMap hmValidate = new HashMap();
		hmValidate.put("uuid", "20608");
		gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);

		GmWSUtil gmWSUtil = new GmWSUtil();
		GmSecurityEventVO gmSecurityEventVO = new GmSecurityEventVO();
		GmLogonBean gmLogonBean = new GmLogonBean();
		GmProdCatBean gmProdCatBean = new GmProdCatBean();

		HashMap hmParams = new HashMap();
		List alResult = new ArrayList();
		List<GmSecurityEventVO> listGmSecurityEvent = new ArrayList<GmSecurityEventVO>();
		GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO
				.getToken());

		hmParams.put("TOKEN",
				GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
		hmParams.put("LANGUAGE", "103097"); // For CodeLookup language should be
											// English only.
		hmParams.put("PAGENO",
				GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
		hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
		hmParams.put("UUID",
				GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
		hmParams.put("UUID",
				GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
		hmParams.put("PARTYID",
				GmCommonClass.parseNull(gmUserVO.getPartyid()));

		
		GmCodeLookupBean gmCodeLookupBean = new GmCodeLookupBean(gmProdCatlResListVO);
		/*alResult = GmCommonClass.parseNullArrayList(gmLogonBean
				.getSecurityEvents(gmUserVO.getPartyid()));		*/
		
		alResult = GmCommonClass.parseNullArrayList(gmCodeLookupBean.fetchSecurityEvents(hmParams));	
		
		listGmSecurityEvent =
	        gmWSUtil.getVOListFromHashMapList(alResult,gmSecurityEventVO,gmSecurityEventVO.getMappingProperties()); 
	    
		// Set the page header detail from first hashmap of list.
		gmProdCatlResListVO.populatePageProperties(alResult, "CODEID");
		gmProdCatlResListVO.setReftype("7000443");// Security Event sync type

		hmParams.put("REFTYPE", gmProdCatlResListVO.getReftype());
		hmParams.put("STATUSID", "103120"); // Initiated status
		hmParams.put("PAGESIZE", gmProdCatlResListVO.getPagesize());
		hmParams.put("LASTREC", gmProdCatlResListVO.getLastrec());
		// To save details of device sync details and sync log.
		gmProdCatBean.saveDeviceSyncDetail(hmParams);
		gmProdCatlResListVO.setListGmSecurityEventVO(listGmSecurityEvent);
		

		return gmProdCatlResListVO;
	}
}
