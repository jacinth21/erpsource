/**
 * 
 * @FileName : GmCommonFileUploadResource.java
 * @Copyright : Globus Medical Inc
 * @Requirement : PRT-R202
 * @author matt balraj
 * @Date : May 2017
 */
package com.globus.webservice.common.resources;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.common.GmCommonFileListVO;
import com.globus.valueobject.common.GmCommonFileUploadVO;
import com.globus.valueobject.common.GmCommonListVO;

@Path("fileUpload")
public class GmCommonFileUploadResource extends GmResource {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchFileList (This method will return list of files for a Refid from T903_upload_file_list
   * 
   * @param strInput
   * @return JSON string
   * @throws AppError
   * @Author Matt B
   */
  @POST
  @Path("fetchFileList")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCommonListVO FetchListOfFiles(String strInput) throws AppError,
      JsonParseException, JsonMappingException, IOException {
    log.debug("FetchListOfFiles strInput >>>>>" + strInput);
    GmWSUtil gmWSUtil = new GmWSUtil();
    ArrayList aiUploadFileList = new ArrayList();
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(gmUserVO);
    GmCommonListVO gmCommonListVO = new GmCommonListVO();
    List<GmCommonFileUploadVO> lstCommonFileUploadVO = new ArrayList<GmCommonFileUploadVO>();
    ObjectMapper mapper = new ObjectMapper();
    GmCommonFileUploadVO gmCommonFileUploadVO =
        mapper.readValue(strInput, GmCommonFileUploadVO.class);
    validateToken(gmCommonFileUploadVO.getToken());
    String strRequestId = gmCommonFileUploadVO.getRefid();
    String strRefType = gmCommonFileUploadVO.getReftype();
    String strType = gmCommonFileUploadVO.getType();
    strRefType = strRefType.equals("0") ? "" : strRefType;
    HashMap hmParams = new HashMap();
    hmParams.put("ARTIFACTSTYPE", ""); // c901_type not used since not included in save
    hmParams.put("REFID", strRequestId);
    hmParams.put("UPLOADTYPE", "");
    hmParams.put("SUBTYPE", strType); // c901_ref_type
    log.debug("hmParams " + hmParams);
    aiUploadFileList = gmCommonUploadBean.fetchUploadInfo(hmParams);
    log.debug("aiUploadFileList=====>" + aiUploadFileList);
    lstCommonFileUploadVO =
        gmWSUtil.getVOListFromHashMapList(aiUploadFileList, gmCommonFileUploadVO,
            gmCommonFileUploadVO.getMappingProperties());
    
    gmCommonListVO.setGmCommonFileUploadVO(lstCommonFileUploadVO);
    return gmCommonListVO;
  }

  /**
   * uploadprFile - Method to upload file to server path and save the data in T903_Upload_File_list
   * table
   * 
   * @param strInput
   * @return JSON string
   * @throws AppError
   * @Author Matt B
   */
  @Path("uploadFileInfo")
  @POST
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  public String uploadFileInfo(@FormDataParam("file") InputStream fileInputStream,
      @FormDataParam("file") FormDataContentDisposition contentDispositionHeader,
      @FormDataParam("token") String strToken, @FormDataParam("refgrp") String strRefGrp,
      @FormDataParam("type") String strSubtype, @FormDataParam("reftype") String strRefType,
      @FormDataParam("refid") String strRefID) throws AppError, Exception {

    log.debug("strRefGrp>>>>> " + strRefGrp);
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();
    String filePath =
        GmCommonClass.getString("PRICING_DOCS") + strRefID + "\\"
            + contentDispositionHeader.getFileName();
    String fileStatus = "File Uploaded Successfully";
    log.debug("fileStatus ***" + fileStatus);

    HashMap hmExpParam = new HashMap();

    // Validating Token
    log.debug("the value inside strToken ***" + strToken);
    try {
      boolean bflag = validateToken(strToken);
      log.debug("bflag ***" + bflag);
      String strUserId = GmCommonClass.parseNull(gmUserVO.getUserid());
      String strfirstName = GmCommonClass.parseNull(gmUserVO.getFname());
      String strLastName = GmCommonClass.parseNull(gmUserVO.getLname());
      String struserName = strfirstName.concat(strLastName);
      String filename = contentDispositionHeader.getFileName();

      hmExpParam.put("REPEMAIL", strUserId);
      hmExpParam.put("FILENAME", filePath);
      hmExpParam.put("USERNAME", struserName);

      // save the file to the server
      File outFile = new File(filePath);
      if (!outFile.exists()) {
        new File(outFile.getParent()).mkdirs();
      }
      OutputStream outputStream = new FileOutputStream(outFile);

      int read = 0;
      byte[] bytes = new byte[1024];

      while ((read = fileInputStream.read(bytes)) != -1) {
        outputStream.write(bytes, 0, read);
      }
      outputStream.flush();
      outputStream.close();
      fileInputStream.close();
      log.debug("filename>>>>" + filename);

      HashMap hmparams = new HashMap();
      hmparams.put("REFID", strRefID);
      hmparams.put("REFGRP", strRefGrp);
      hmparams.put("FILENAME", filename);
      hmparams.put("SUBTYPE", strSubtype);
      hmparams.put("USERID", strUserId);
      hmparams.put("REFTYPE", strRefType);

      log.debug("uploadprFile==========>>>>>>>" + hmparams);
      // calling the saveUpload method
      gmCommonUploadBean.saveUploadUniqueInfo(hmparams);

    } catch (Exception e) {

      log.debug("Raising exception >>>>>>>>>>>>>>>>>>>");
      hmExpParam.put("EXCEPTION", e.getMessage());
      throw new AppError("", "20642", 'E');
    }
    fileStatus = fileStatus + filePath;
    return fileStatus;
  }

  /**
   * deleteUploadFileDetails (Method to mark delete_fl of selected files
   * 
   * @param strInput
   * @return JSON string
   * @throws AppError
   * @Author Matt B
   */
  @POST
  @Path("deleteUploadedFiles")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public void deleteUploadFileDetails(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {
    log.debug("deleteUploadFileDetails strInput >>>>>" + strInput);
    ObjectMapper mapper = new ObjectMapper();

    GmCommonFileListVO gmCommonFileListVO = mapper.readValue(strInput, GmCommonFileListVO.class);

    validateToken(gmCommonFileListVO.getToken());
    GmCommonFileUploadVO[] gmCommonFileUploadVO = gmCommonFileListVO.getGmCommonFileUploadVO();

    String strFileIds = "";
    String strUserId = "";
    for (int i = 0; i < gmCommonFileUploadVO.length; i++) {
      GmCommonFileUploadVO gmFileUploadVO = gmCommonFileUploadVO[i];

      strFileIds = strFileIds + gmFileUploadVO.getFileid();
      strFileIds = strFileIds + "|";
      strUserId = gmFileUploadVO.getUpdatedby();
    }
    log.debug("List of File Ids >>>>>>>>>>>>>>>>" + strFileIds);
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();
    gmCommonUploadBean.deleteUploadInfo(strFileIds, strUserId);
    return;
  }

  /**
   * checkFileExists (This method will return list of files for a Refid from T903_upload_file_list
   * 
   * @param strInput
   * @return JSON string
   * @throws AppError
   * @Author Matt B
   */
  @POST
  @Path("checkFileExists")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCommonFileUploadVO FetchFileSts(String strInput) throws AppError, JsonParseException,
      JsonMappingException, IOException {
    log.debug("FetchFileSts strInput >>>>>" + strInput);
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(gmUserVO);

    GmCommonFileUploadVO retCommonFileUploadVO = new GmCommonFileUploadVO();
    ObjectMapper mapper = new ObjectMapper();
    GmCommonFileUploadVO gmCommonFileUploadVO =
        mapper.readValue(strInput, GmCommonFileUploadVO.class);
    validateToken(gmCommonFileUploadVO.getToken());
    String strRequestId = gmCommonFileUploadVO.getRefid();
    String strRefType = gmCommonFileUploadVO.getReftype();
    String strFileName = gmCommonFileUploadVO.getFilename();

    String strStatus =
        gmCommonUploadBean.validateFileNameSts(strRequestId, strRefType, strFileName);
    retCommonFileUploadVO.setMessage(strStatus);
    log.debug("strStatus=====>" + strStatus);
    return retCommonFileUploadVO;
  }


}
