package com.globus.webservice.common.resources;

import java.io.IOException;
import java.util.HashMap;

import org.apache.log4j.Logger;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmAutoCompleteForm;
import com.globus.common.util.GmWSUtil;
import com.globus.operations.logistics.beans.GmKitMappingBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.webservice.common.bean.GmMasterRedisReportBean;
import com.globus.webservice.common.valueobject.GmRedisVO;
import com.globus.webservice.sales.kit.bean.GmKitMappingReportBean;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

@Path("cmsearch")
public class GmCommonSearchResource extends GmResource {

	  Logger log = GmLogger.getInstance(this.getClass().getName());
	  
/**
  * This method used to fetch set list based on distributor id
  */	
	  @POST
	  @Path("setlist")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  public String searchTag(String strInput) throws AppError, JsonGenerationException,
	      JsonMappingException, JsonParseException, IOException, Exception {
		  
		log.debug("strInput=>"+strInput);
		ObjectMapper mapper = new ObjectMapper();
	    HashMap hmParam = new HashMap(); 
	    GmRedisVO gmRedisVO = mapper.readValue(strInput, GmRedisVO.class);
	    validateToken(gmRedisVO.getToken());
	    GmMasterRedisReportBean gmMasterRedisReportBean = new GmMasterRedisReportBean(gmRedisVO);
	    GmKitMappingReportBean gmKitMappingReportBean = new GmKitMappingReportBean(gmRedisVO);
        
	    String strType = "";
		String strKey = "";
		String strSearchKey = GmCommonClass.parseNull((String) gmRedisVO.getSrchkey());
		String strMaxCount = GmCommonClass.parseNull((String) gmRedisVO.getMaxcount());
		String strTerm = GmCommonClass.parseNull((String) gmRedisVO.getSrchkey());
		String strActiveFl = GmCommonClass.parseNull((String) gmRedisVO.getActivefl());
		String strCompanyId = GmCommonClass.parseNull((String) gmRedisVO.getCmpid());
		String strUserId = GmCommonClass.parseNull((String) gmRedisVO.getUserid());			
		String strDistId = gmKitMappingReportBean.getDistributorID(strUserId);
		   
		strKey = gmMasterRedisReportBean.getAutoCompleteKeys("SET_TAG_LIST", "Y",strDistId);
		hmParam.put("KEY", strKey);
		hmParam.put("SEARCH_KEY", strSearchKey);
		hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
		hmParam.put("TYPE", strType);
		hmParam.put("ACTIVEFL", strActiveFl);
		hmParam.put("COMPANYID", strCompanyId);
		hmParam.put("DISTID", strDistId);
		String strResults = gmMasterRedisReportBean.loadSetTagDistList(hmParam);
		
		if(!strResults.contains("ID")){
			strKey = gmMasterRedisReportBean.getAutoCompleteKeys("TAG_LIST", "Y",strDistId);
			hmParam.put("KEY", strKey);
			hmParam.put("SEARCH_KEY", strSearchKey);
			hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
			hmParam.put("TYPE", strType);
			hmParam.put("ACTIVEFL", strActiveFl);
			hmParam.put("COMPANYID", strCompanyId);
			hmParam.put("DISTID", strDistId);
			strResults = gmMasterRedisReportBean.loadTagDistList(hmParam);
		}
	    return strResults;
	    
	  }
		  
/**
  * This method used to fetch Part list based on company 
  */		  
	  @POST
	  @Path("partlist")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  public String searchPart(String strInput) throws AppError, JsonGenerationException,
	      JsonMappingException, JsonParseException, IOException, Exception {
		  
	    log.debug("strInput===>"+strInput);
	    ObjectMapper mapper = new ObjectMapper();
	    GmRedisVO gmRedisVO = mapper.readValue(strInput, GmRedisVO.class);
	    validateToken(gmRedisVO.getToken());
	    GmMasterRedisReportBean gmMasterRedisReportBean = new GmMasterRedisReportBean(gmRedisVO);   
	    
	    String strType = "";
		String strKey = "";
		String strSearchKey = GmCommonClass.parseNull((String) gmRedisVO.getSrchkey());
		String strMaxCount = GmCommonClass.parseNull((String) gmRedisVO.getMaxcount());
		String strTerm = GmCommonClass.parseNull((String) gmRedisVO.getSrchkey());
		String strActiveFl = GmCommonClass.parseNull((String) gmRedisVO.getActivefl());
		String strCompanyId = GmCommonClass.parseNull((String) gmRedisVO.getCmpid());
	    HashMap hmParam = new HashMap();    
		strKey = gmMasterRedisReportBean.getAutoCompleteKeys("PART_LIST", "Y",strCompanyId);
		hmParam.put("KEY", strKey);
		hmParam.put("SEARCH_KEY", strSearchKey);
		hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
		hmParam.put("TYPE", strType);
		hmParam.put("ACTIVEFL", strActiveFl);
		hmParam.put("COMAPNYID", strCompanyId);
		String strResults = gmMasterRedisReportBean.loadPartNumList(hmParam);
	    return strResults;
	    
	  }
 /**
   * This method used to fetch Sales Rep list based on login company
   */	
	  @POST
	  @Path("party")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  public String searchPartyList(String strInput) throws AppError, JsonGenerationException,
	      JsonMappingException, JsonParseException, IOException, Exception {
		  
	    log.debug("strInput===>"+strInput);
	    ObjectMapper mapper = new ObjectMapper();
	    GmRedisVO gmRedisVO = mapper.readValue(strInput, GmRedisVO.class);
	    validateToken(gmRedisVO.getToken());
	    GmMasterRedisReportBean gmMasterRedisReportBean = new GmMasterRedisReportBean(gmRedisVO);
	        
	    String strType = "";
		String strKey = "";
		String strSearchKey = GmCommonClass.parseNull((String) gmRedisVO.getSrchkey());
		String strMaxCount = GmCommonClass.parseNull((String) gmRedisVO.getMaxcount());
		String strTerm = GmCommonClass.parseNull((String) gmRedisVO.getSrchkey());
		String strCompanyId = GmCommonClass.parseNull((String) gmRedisVO.getCmpid());
	    HashMap hmParam = new HashMap();
	
		strKey = gmMasterRedisReportBean.getAutoCompleteKeys("REP_PARTY_LIST", "Y",strCompanyId);
		hmParam.put("KEY", strKey);
		hmParam.put("SEARCH_KEY", strSearchKey);
		hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
		hmParam.put("TYPE", strType);
		hmParam.put("COMPANYID", strCompanyId);
		String strResults = gmMasterRedisReportBean.loadSalesRepList(strKey, strSearchKey,Integer.parseInt(strMaxCount), hmParam);
	    return strResults;
	    
	  } 
/**
  * This method used to fetch kit name list based on party id
  * 
  */	
	  @POST
	  @Path("kitname")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  public String fetchKitList(String strInput) throws AppError, JsonGenerationException,
	      JsonMappingException, JsonParseException, IOException, Exception {
		  
		log.debug("strInput===>"+strInput);
	    ObjectMapper mapper = new ObjectMapper();
		GmRedisVO gmRedisVO = mapper.readValue(strInput, GmRedisVO.class);
		validateToken(gmRedisVO.getToken());  
		GmMasterRedisReportBean gmMasterRedisReportBean = new GmMasterRedisReportBean(gmRedisVO);
	    
		String strType = "";
	    String strKey = "";
	    String strSearchKey = GmCommonClass.parseNull((String) gmRedisVO.getSrchkey());
		String strMaxCount = GmCommonClass.parseNull((String) gmRedisVO.getMaxcount());
		String strTerm = GmCommonClass.parseNull((String) gmRedisVO.getSrchkey());
		String strPartyId = GmCommonClass.parseNull((String) gmRedisVO.getPartyid());
		String strKitType = GmCommonClass.parseNull((String) gmRedisVO.getType());
		HashMap hmParam = new HashMap();
		
		if (strKitType.equals("107922")){
			strKey = gmMasterRedisReportBean.getAutoCompleteKeys("KIT_NAME_LIST", "Y",strPartyId);
		}else if(strKitType.equals("107923")){
			strKey = gmMasterRedisReportBean.getAutoCompleteKeys("PART_KIT_NAME_LIST", "Y", strPartyId);
		}else{
			strKey = gmMasterRedisReportBean.getAutoCompleteKeys("KIT_NAME_LIST", "Y", strPartyId);
		}
		hmParam.put("KEY", strKey);
		hmParam.put("SEARCH_KEY", strSearchKey);
		hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
		hmParam.put("TYPE", strType);
		hmParam.put("PARTYID", strPartyId);
		hmParam.put("KITTYPE", strKitType);
		String strResults = gmMasterRedisReportBean.fetchKitNmList(hmParam);
		return strResults;		    
	  }

/**
 * Fetch the Set bundle name list from Redis server.
 *  
 */
	@POST
	@Path("setbundle")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public String loadSetBundleNameList(String strInput) throws AppError, JsonGenerationException,
		JsonMappingException, JsonParseException, IOException, Exception {
		
		ObjectMapper mapper = new ObjectMapper();
		HashMap hmParam = new HashMap();
		GmRedisVO gmRedisVO = mapper.readValue(strInput, GmRedisVO.class);
		validateToken(gmRedisVO.getToken());
		
		GmMasterRedisReportBean gmMasterRedisReportBean = new GmMasterRedisReportBean(gmRedisVO);
		String strSearchKey = GmCommonClass.parseNull((String) gmRedisVO.getSrchkey());
		String strMaxCount = GmCommonClass.parseNull((String) gmRedisVO.getMaxcount());
		String strCmpId = GmCommonClass.parseNull((String) gmRedisVO.getCmpid());
		String strKey = gmMasterRedisReportBean.getAutoCompleteKeys("SET_BUNDLE_NM_LIST", "Y",strCmpId);
			  
		hmParam.put("KEY", strKey);
		hmParam.put("SEARCH_KEY", strSearchKey);
		hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
		hmParam.put("COMPANYID", strCmpId);
			log.debug("strCmpId==>"+strCmpId);
		String strResults = gmMasterRedisReportBean.fetchSetBundleNameList(hmParam);
		return strResults;
	}

/**
 * This method used to fetch set details based on logged in company
 * 
 */	
	@POST
	@Path("setbasedcmp")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public String fetchSetList(String strInput) throws AppError, JsonGenerationException,
		JsonMappingException, JsonParseException, IOException, Exception {
			  
		log.debug("strInput===>"+strInput);
		ObjectMapper mapper = new ObjectMapper();
		GmRedisVO gmRedisVO = mapper.readValue(strInput, GmRedisVO.class);
		validateToken(gmRedisVO.getToken());  
		GmMasterRedisReportBean gmMasterRedisReportBean = new GmMasterRedisReportBean(gmRedisVO);
		    
		String strType = "";
		String strKey = "";
		String strSearchKey = GmCommonClass.parseNull((String) gmRedisVO.getSrchkey());
		String strMaxCount = GmCommonClass.parseNull((String) gmRedisVO.getMaxcount());
		String strTerm = GmCommonClass.parseNull((String) gmRedisVO.getSrchkey());
		String strCmpId = GmCommonClass.parseNull((String) gmRedisVO.getCmpid());			
		HashMap hmParam = new HashMap();		
		strKey = gmMasterRedisReportBean.getAutoCompleteKeys("CASE_MAPPING_SET", "Y",strCmpId);
		hmParam.put("KEY", strKey);
		hmParam.put("SEARCH_KEY", strSearchKey);
		hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
		hmParam.put("TYPE", strType);
		hmParam.put("CMPID", strCmpId);

		String strResults = gmMasterRedisReportBean.fetchSetNmList(hmParam);

		return strResults;			
	}  
	@POST
	  @Path("replist")
	  @Produces({MediaType.APPLICATION_JSON})
	  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	  public String searchRepList(String strInput) throws AppError, JsonGenerationException,
	  JsonMappingException, JsonParseException, IOException, Exception {
		  
		log.debug("strInput===>"+strInput);
		ObjectMapper mapper = new ObjectMapper();

		GmRedisVO gmRedisVO = mapper.readValue(strInput, GmRedisVO.class);
		validateToken(gmRedisVO.getToken());
		GmMasterRedisReportBean gmMasterRedisReportBean = new GmMasterRedisReportBean(gmRedisVO);

		    
		String strType = "";
		String strKey = "";
		String strSearchKey = GmCommonClass.parseNull((String) gmRedisVO.getSrchkey());
		String strMaxCount = GmCommonClass.parseNull((String) gmRedisVO.getMaxcount());
		String strTerm = GmCommonClass.parseNull((String) gmRedisVO.getSrchkey());
		String strCompanyId = GmCommonClass.parseNull((String) gmRedisVO.getCmpid());
		HashMap hmParam = new HashMap();
		
		strKey = gmMasterRedisReportBean.getAutoCompleteKeys("CRM_REP_LIST", "N", strCompanyId);
		hmParam.put("KEY", strKey);
		hmParam.put("SEARCH_KEY", strSearchKey);
		hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
		hmParam.put("TYPE", strType);
		hmParam.put("COMPANYID", strCompanyId);
		String strResults = gmMasterRedisReportBean.loadRepList(strKey, strSearchKey,Integer.parseInt(strMaxCount), hmParam);
		
		return strResults;

	  }
}
