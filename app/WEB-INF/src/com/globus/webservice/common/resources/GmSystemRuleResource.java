/**
 * 
 */
package com.globus.webservice.common.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;


import com.globus.common.beans.AppError;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.util.GmTokenManager;
import com.globus.valueobject.operations.GmRuleVO;
import com.globus.common.beans.GmCommonBean;

/**
 * @author HReddy
 *
 */
@Path("systemrules")
public class GmSystemRuleResource extends GmResource {
	Logger log = GmLogger.getInstance(this.getClass().getName());  	
    @POST
    @Path("fetchRuleByGroup")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public GmRuleVO fetchRuleByGroup(String  strInput) throws AppError{    	 
    	GmRuleVO gmRuleVO = new GmRuleVO();
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmCommonBean gmCommonBean = new GmCommonBean();		
		HashMap hmUserInfo = null;
		HashMap hmParam = new HashMap();
		ArrayList alUDIRuledetails, listGmRuleVO = null;	
		
		//validate token and parse JSON into VO
		parseInputAndValidateToken(strInput, gmRuleVO);
		//Assigning all the request params into Response VO
				
		GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmRuleVO.getToken());
		String strUserID = (String)gmUserVO.getUserid();
		String strRuleID = "";
		strRuleID = GmCommonClass.parseNull((String)gmRuleVO.getRulegroup());
		hmParam.put("CODEGRPID", strRuleID);
		alUDIRuledetails = GmCommonClass.parseNullArrayList(gmCommonBean.fetchUDIRuleParams(hmParam)); 
		listGmRuleVO = (ArrayList) gmWSUtil.getVOListFromHashMapList(alUDIRuledetails, (new GmRuleVO()),
				new GmRuleVO().getUDIRuleProperties());
		gmRuleVO.setListGmRuleVO(listGmRuleVO);
		return gmRuleVO;
	}    
}