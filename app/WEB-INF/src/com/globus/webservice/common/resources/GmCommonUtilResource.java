/**
 * 
 */
package com.globus.webservice.common.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import sun.util.logging.resources.logging;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.common.GmCodeLookupVO;
import com.globus.valueobject.common.GmCommonListVO;
import com.globus.valueobject.common.GmCommonLogVO;
import com.globus.valueobject.common.GmRulesVO;

/**
 * @author vprasath
 * 
 */

@Path("common")
public class GmCommonUtilResource extends GmResource {
	Logger log = GmLogger.getInstance(this.getClass().getName());
  // for post request
  @Path("codelookuplist")
  @POST
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCommonListVO fetchCodeLookupList(String strInput) throws AppError {

	GmCommonListVO gmCommonListVO =new GmCommonListVO();
    GmCodeLookupVO gmCodeLookupVO = new GmCodeLookupVO();
    parseInputAndValidateToken(strInput, gmCodeLookupVO);

    List<GmCodeLookupVO> gmCodeLookUpList = new ArrayList<GmCodeLookupVO>();
    GmWSUtil gmWSUtil = new GmWSUtil();

    ArrayList alCodeList = GmCommonClass.getCodeList(gmCodeLookupVO.getCodegrp());
    gmCodeLookUpList =
        gmWSUtil.getVOListFromHashMapList(alCodeList, gmCodeLookupVO,
            gmCodeLookupVO.getCodeListMappingProps());
    gmCommonListVO.setGmCodeLookupVO(gmCodeLookUpList);
    return gmCommonListVO;
  }

  /**
   * fetchCommonLogDetails - Common resource to fetch Log details
   * 
   * @param strInput
   * @return List<GmCommonLogVO>
   * @throws AppError
   */
  @Path("fetchlogdetails")
  @POST
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCommonListVO fetchCommonLogDetails(String strInput) throws AppError {
	 GmCommonListVO gmCommonListVO = new GmCommonListVO();
    GmCommonLogVO gmCommonLogVO = new GmCommonLogVO();
    // Validate the input
    parseInputAndValidateToken(strInput, gmCommonLogVO);

    // Object Instantiation
    GmCommonBean gmCommonBean = new GmCommonBean();
    HashMap hmParams = new HashMap();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    List<GmCommonLogVO> gmCommonLogList = new ArrayList<GmCommonLogVO>();

    // Validate Txnid and Txntype is not empty
    hmParams.put("txnid", "20636");
    hmParams.put("txntype", "20637");
    gmProdCatUtil.validateInput(gmCommonLogVO, hmParams);

    hmParams.put("TXNIDS", GmCommonClass.parseNull(gmCommonLogVO.getTxnid()));
    hmParams.put("TXNTYPE", GmCommonClass.parseNull(gmCommonLogVO.getTxntype()));

    // Fetch Log details
    ArrayList alLogList =
        GmCommonClass.parseNullArrayList(gmCommonBean.fetchCommonLogDetails(hmParams));

    gmCommonLogList =
        gmWSUtil.getVOListFromHashMapList(alLogList, gmCommonLogVO,
            gmCommonLogVO.getCommonLogProperties());

    // Nullifying the objects
    alLogList = null;
    hmParams = null;
    gmCommonListVO.setGmCommonLogVO(gmCommonLogList);
    return gmCommonListVO;
  }

  /**
   * fetchCommonLogDetails - Common resource to fetch Log details
   * 
   * @param strInput
   * @return List<GmCommonLogVO>
   * @throws AppError
   */
  @Path("fetchComments")
  @POST
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCommonListVO fetchCommentLog(String strInput) throws AppError {
	  GmCommonListVO gmCommonListVO = new GmCommonListVO();
    GmCommonLogVO gmCommonLogVO = new GmCommonLogVO();
    // Validate the input
    parseInputAndValidateToken(strInput, gmCommonLogVO);
    // Object Instantiation
    GmCommonBean gmCommonBean = new GmCommonBean();
    HashMap hmParams = new HashMap();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    List<GmCommonLogVO> gmCommonLogList = new ArrayList<GmCommonLogVO>();
    // Validate Txnid and Txntype is not empty
    hmParams.put("txnid", "20636");
    hmParams.put("txntype", "20637");
    gmProdCatUtil.validateInput(gmCommonLogVO, hmParams);

    String strTxnId = GmCommonClass.parseNull(gmCommonLogVO.getTxnid());
    String strType = GmCommonClass.parseNull(gmCommonLogVO.getTxntype());
    // Fetch Log details
    ArrayList alLog = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strTxnId, strType));

    gmCommonLogList =
        gmWSUtil.getVOListFromHashMapList(alLog, gmCommonLogVO,
            gmCommonLogVO.getCommentLogProperties());

    gmCommonListVO.setGmCommonLogVO(gmCommonLogList);
    return gmCommonListVO;
  }

  /**
   * saveCommentLog - Common resource to Save Log details
   * 
   * @param strInput
   * @return List<GmCommonLogVO>
   * @throws AppError
   */
  @Path("saveComments")
  @POST
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmCommonLogVO saveCommentLog(String strInput) throws AppError {

    GmCommonLogVO gmCommonLogVO = new GmCommonLogVO();
    // Validate the input
    parseInputAndValidateToken(strInput, gmCommonLogVO);
    // Object Instantiation
    GmCommonBean gmCommonBean = new GmCommonBean();
    HashMap hmParams = new HashMap();
    GmWSUtil gmWSUtil = new GmWSUtil();
    ArrayList alLog = new ArrayList();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    List<GmCommonLogVO> gmCommonLogList = new ArrayList<GmCommonLogVO>();
    // Validate Txnid and Txntype is not empty
    hmParams.put("txnid", "20636");
    hmParams.put("txntype", "20637");
    gmProdCatUtil.validateInput(gmCommonLogVO, hmParams);

    String strTxnId = GmCommonClass.parseNull(gmCommonLogVO.getTxnid());
    String strType = GmCommonClass.parseNull(gmCommonLogVO.getTxntype());
    String strComments = GmCommonClass.parseNull(gmCommonLogVO.getComments());
    String strUserId = GmCommonClass.parseNull(gmUserVO.getUserid());

    gmCommonBean.saveLog(strTxnId, strComments, strUserId, strType);

    return gmCommonLogVO;
  }
  
  /**
   * fetchRuleValue - Fetch the rule value based on the compnay, ruleid and rulegroupid
   * 
   * @param strInput
   * @return List<GmRulesVO>
   * @throws AppError
   */
  
  @Path("fetchRuleValue")
  @POST
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmRulesVO fetchRulelist(String strInput) throws AppError {
	  
	GmRulesVO gmRulesVO = new GmRulesVO();
    parseInputAndValidateToken(strInput, gmRulesVO);
    String strrulevalue= GmCommonClass.getRuleValueByCompany(gmRulesVO.getRuleid() ,gmRulesVO.getRulegrpid(),gmRulesVO.getCmpid());
    gmRulesVO.setRulevalue(strrulevalue);
    return gmRulesVO;
  }
  
}
