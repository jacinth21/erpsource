package com.globus.webservice.common.resources;

import java.util.HashMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.logon.beans.GmLogonBean;
import org.apache.log4j.Logger;
import com.globus.common.beans.GmLogger;

public class GmResource {
	
	  Logger log = GmLogger.getInstance(this.getClass().getName());


	protected GmUserVO gmUserVO = new GmUserVO();
	protected boolean parseInputAndValidateToken(String strInput, GmDataStoreVO gmDataStoreVO)
	{
		GmWSUtil gmWSUtil = new GmWSUtil();
		String strToken = "";
		gmWSUtil.parseJsonToVO(strInput, gmDataStoreVO);
		strToken = gmDataStoreVO.getToken();
		return validateToken(strToken);	
	}
	
	/**
	 * Method to validate token
	 * This method first check the token is available in static HashMap.
	 * If token is not exist then it goes to DB and check token exist in DB with active status.
	 * If token exist in DB then add userVO detail in static HashMap.
	 * If token is not exist in DB as well then it throws error message.
	 * @param strToken
	 * @return
	 */
	public boolean validateToken(String strToken) {

		GmWSUtil gmWSUtil = new GmWSUtil();
		GmLogonBean gmLogonBean = new GmLogonBean();

		HashMap hmUserToken = new HashMap();
		String strDbToken = "";
		boolean bflag = true;
		bflag = GmTokenManager.verify(strToken);
		

		if (bflag) {//if token exist in static HashMap
			gmUserVO = GmTokenManager.getTokenValue(strToken);
		} else {//if token exist in DB
			hmUserToken = (HashMap) gmLogonBean.fetchAllDBTokens(strToken);// getting Token value from DB
			gmUserVO = (GmUserVO) gmWSUtil.getVOFromHashMap(hmUserToken,gmUserVO, gmUserVO.getMappingProperties());
			strDbToken = GmCommonClass.parseNull(gmUserVO.getToken());

			if (strDbToken.equals("")) {
				throw new AppError(AppError.APPLICATION, "1002.3");
			}
			GmTokenManager.addToken(strDbToken, gmUserVO);
		} 
		
		return bflag;	
	}
	
	public boolean parseInputAndValidateRFIDToken(String strToken) {		
		GmLogonBean gmLogonBean = new GmLogonBean();
		HashMap hmUserToken = new HashMap();		
		boolean bflag = false;	
		
		if (!strToken.equals("")){
		hmUserToken = GmCommonClass.parseNullHashMap((HashMap) gmLogonBean.fetchAllDBTokens(strToken));// getting Token value from DB
		bflag = hmUserToken.size() > 0 ?true:false; 
		}
		return bflag;
	}
	
}


