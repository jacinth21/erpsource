package com.globus.webservice.common.exception;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAppErrorBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.webservice.common.exception.api.GmErrorResponse;


/**
 * @version 1.0
 * @author: Iain Porter iain.porter@porterhead.com
 * @since 19/10/2012
 */
public abstract class GmBaseWebApplicationException extends WebApplicationException {

    private final int status;
    private final String errorCode;
    private final String errorType;
    private  String developerMessage;
    private  String reason;
    private  String action;
    
    /* List of static constants that are used to identify the
     * context of the error message
     */
     public static String APPLICATION  = "APP";
    
    /** String for Appending to Oracle Related Errors */
    protected static String ORA = "ORA";
    
    /**String for replacing the msg content */
    protected String strMsgTransId ="";
    protected Exception exp;

    protected Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    
    /** The context(module in the project) of the error occured*/
    protected String strContext = "";

    /** The Message ID associated with the message */
    protected String strMsgId = "";

    /** The message text associated to the error*/
    protected String strMsgText = "";

    /** To work on custom Error message*/
    protected String cType = "E";
    
    public static ResourceBundle rbApp = ResourceBundle.getBundle("AppError");
    
    /** The time stamp at which the error occured*/
    protected Date datDateTime ;
    
    public GmBaseWebApplicationException(int status, Exception ex) {
    	
    	processException(ex);
        this.status = status;
        this.errorCode = strMsgId;
        this.errorType = cType;
        this.developerMessage = strMsgText;
        setReason();
        
        if(GmCommonClass.parseNull(developerMessage).equals(""))
		{
			this.developerMessage = rbApp.getString("dbError");
		}
    }
    
    public GmBaseWebApplicationException(String astrContext, String astrMsgId)
    {
    	
		  this.status = 500;	
		  this.errorCode = astrMsgId;
		  this.errorType = cType;
		  
		  try
		  {
			  this.developerMessage = rbApp.getString(astrContext+astrMsgId);
		  }
		  catch(MissingResourceException ex)
		  {
			  this.developerMessage = astrContext;
		  }
		 
		  setReason();
		  
		  if(GmCommonClass.parseNull(developerMessage).equals(""))
			{
				this.developerMessage = rbApp.getString("dbError");
			}
    }
    
    public GmBaseWebApplicationException(String astrContext, String astrMsgId, char CLType)
    {
    	  this.status = 500;	
		  this.errorCode = astrMsgId;
		  this.errorType = ""+CLType;
		 
	   try{
			  this.developerMessage = rbApp.getString(astrContext+astrMsgId);
		  }
	   catch(MissingResourceException ex)
		  {
			  this.developerMessage = astrContext;
		  }
	   
		  setReason();
		  
	  if(GmCommonClass.parseNull(developerMessage).equals(""))
		{
			this.developerMessage = rbApp.getString("dbError");
		}
    }

    @Override
    public Response getResponse() {
        return Response.status(status).type(MediaType.APPLICATION_JSON_TYPE).entity(getErrorResponse()).build();
    }

    private GmErrorResponse getErrorResponse() {
    	if(developerMessage.indexOf("^") != -1)
    		developerMessage = developerMessage.substring(developerMessage.indexOf("^") +1);
    	
    	setReason();
    	/* the below change is for web service error message are not getting display 
    	 * if the error message is defined in apperror.xml
    	 */
    	if(strMsgText.indexOf("^") != -1)
    		strMsgText = strMsgText.substring(strMsgText.indexOf("^") +1);
    	
    	String strMessage = strMsgText.trim().equals("")?developerMessage:strMsgText;
    	
    	GmErrorResponse response = new GmErrorResponse();
    	response.setErrorType(errorType);
        response.setErrorCode(errorCode);
        response.setMessage(strMessage);
        response.setAction(action);
        response.setReason(reason);
        return response;
    }
    
    private void processException(Exception ex)
    {
    	 // Time Stamp
    	datDateTime = new Date();
        /* in case of SQL exception fetch the database error message
         * from the REXDBStrings properties file
         */

    	if (ex instanceof SQLException)
        {     
    		SQLException sqlExp = (SQLException)ex;
    		
            if (sqlExp.getErrorCode() <= 20000 && ex.getMessage().equals(""))
            	strMsgText = rbApp.getString("dbError");
            else 
            {
            	log.debug(" INSIDE SQL Error " + ORA.concat(String.valueOf(sqlExp.getErrorCode())) + "  StrMsgText is " + strMsgText);
            			try{
            					strMsgText = String.valueOf(sqlExp.getErrorCode()) + "^" + GmCommonClass.parseNull(rbApp.getString(ORA.concat(String.valueOf(sqlExp.getErrorCode()))));
            				}
            			catch (MissingResourceException mreExp){            				
	                        	strMsgText = String.valueOf(sqlExp.getErrorCode()) + "^" + parseAndGetMessage(sqlExp);
            				}
            			strMsgId = String.valueOf(sqlExp.getErrorCode());
            	if (strMsgText.equals(""))
            	{
            		strMsgText =  ex.getMessage() + ": " + rbApp.getString("dbError");
            	}
				log.fatal(" Value of Error Message in App Error is " +strMsgText );
            }            	
        }
        // If exception is AppError itself then use the information in the exception
        else if (ex instanceof AppError)
        {

            AppError ap = (AppError)ex;
            strContext = ap.getContext();
            strMsgId = ap.getMessageId();
            strMsgText = ap.getMessage();
            datDateTime = ap.getTimeStamp();
       }
        // For any other exception get the exception message
        else
        {
            strMsgText = rbApp.getString("dbError");

        }
    }
    
    protected String parseAndGetMessage(SQLException sqlException)
	{
		String strMessage = sqlException.getMessage();
		int iErrorCode = sqlException.getErrorCode();
		String strORANumber = "ORA-" + iErrorCode + ":";
		strMessage = strMessage.replaceAll(strORANumber, "");	
		int index = strMessage.indexOf("\n");
		if (index != -1) {
			strMessage = strMessage.substring(0,index);
		}
		return strMessage;
	}
    
    private void setReason() 
    {
    	try {
    	GmAppErrorBean appErrorBean = new GmAppErrorBean();
    	HashMap hmMap = appErrorBean.getApperrorMsg(GmCommonClass.getString("GMAPPERROR"), "ERR"+strMsgText);
    	if(strMsgText == null || strMsgText.trim().equals(""))
     	{
     		strMsgText = GmCommonClass.parseNull((String)hmMap.get("MESSAGE"));
     	}
     	  
     	reason = GmCommonClass.parseNull((String)hmMap.get("REASON")); 
     	action = GmCommonClass.parseNull((String)hmMap.get("ACTION"));
    	}
     	
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    	}
    }

}
