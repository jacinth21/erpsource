package com.globus.webservice.common.exception;


public class GmApplicationRuntimeException extends GmBaseWebApplicationException {
	private String strErroCode = ""; 
	private String strType= "";
	private String strErrorMessage = "";
	
    public GmApplicationRuntimeException(Exception ex) {
        super(500, ex);
    }   
  
    public GmApplicationRuntimeException(String astrContext, String astrMsgId)
    {
    	super(astrContext,astrMsgId);
    }
    
    public GmApplicationRuntimeException(String astrContext, String astrMsgId, char CLType)
    {
    	super(astrContext, astrMsgId, CLType  );
    }
    
}
