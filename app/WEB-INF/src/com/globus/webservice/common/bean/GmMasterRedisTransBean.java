package com.globus.webservice.common.bean;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import oracle.jdbc.driver.OracleTypes;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.operations.logistics.beans.GmKitMappingBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.webservice.common.bean.GmMasterRedisReportBean;
public class GmMasterRedisTransBean extends GmBean{
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * Constructor will populate company info.
	 * 
	 * @param gmDataStore
	 */
	public GmMasterRedisTransBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	}

	/**
	 * 
	 * saveDetailsToCache - This method used to save the details to cache server
	 * 
	 * @param hmCacheDetails
	 * @throws AppError
	 */
	public void saveDetailsToCache(HashMap hmCacheDetails) throws AppError {

		String strCacheQueueName = GmCommonClass.getJmsConfig("JMS_CACHE_QUEUE");
		log.debug("strCacheQueueName>>" + strCacheQueueName);
		// to call the cache (in JMS)
		GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
		String strCacheConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("REDIS_CONSUMER_CLASS"));
		hmCacheDetails.put("CONSUMERCLASS", strCacheConsumerClass);
		hmCacheDetails.put("QUEUE_NAME", strCacheQueueName);
		gmConsumerUtil.sendMessage(hmCacheDetails);
	}

	/**
	 * syncSetTagList - This method used to sync the set list to redis
	 * 
	 * @param hmParam
	 * @throws AppError
	 */
	public void syncSetTagList(HashMap hmParam) {

		GmMasterRedisReportBean gmMasterRedisReportBean = new GmMasterRedisReportBean(getGmDataStoreVO());
		String strId = GmCommonClass.parseNull((String) hmParam.get("ID"));
		String strOldName = GmCommonClass.parseNull((String) hmParam.get("OLD_NM"));
		String strNewName = GmCommonClass.parseNull((String) hmParam.get("NEW_NM"));
		String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
		String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
		GmCacheManager gmCacheManager = new GmCacheManager();
		log.debug("strPartyId=>>" + strPartyId);
		String strKey = gmMasterRedisReportBean.getAutoCompleteKeys("SET_TAG_LIST", "Y", strDistId);
		String strNewDataFl = gmCacheManager.updateSortedSetInCache(strKey,strOldName, strNewName, strId);

		if (strNewDataFl.equalsIgnoreCase("Y")) {
			GmCacheManager gmTmpCacheManager = new GmCacheManager();
			gmTmpCacheManager.setKey(strKey);
			gmTmpCacheManager.setSearchPrefix("Glo");
			gmTmpCacheManager.setMaxFetchCount(100);
			gmTmpCacheManager.returnJsonString(gmMasterRedisReportBean.loadSetTagDistList(hmParam));
		}
	}

	/**
	 * syncTagList - This method used to sync the set list based on tag
	 * 
	 * @param hmParam
	 * @throws AppError
	 */
	public void syncTagList(HashMap hmParam) {

		GmMasterRedisReportBean gmMasterRedisReportBean = new GmMasterRedisReportBean(getGmDataStoreVO());
		String strId = GmCommonClass.parseNull((String) hmParam.get("ID"));
		String strOldName = GmCommonClass.parseNull((String) hmParam.get("OLD_NM"));
		String strNewName = GmCommonClass.parseNull((String) hmParam.get("NEW_NM"));
		String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
		GmCacheManager gmCacheManager = new GmCacheManager();
		log.debug("strCompanyId=>" + getGmDataStoreVO().getCmpid());
		String strKey = gmMasterRedisReportBean.getAutoCompleteKeys("TAG_LIST","Y", strDistId);
		String strNewDataFl = gmCacheManager.updateSortedSetInCache(strKey,strOldName, strNewName, strId);

		if (strNewDataFl.equalsIgnoreCase("Y")) {
			GmCacheManager gmTmpCacheManager = new GmCacheManager();
			gmTmpCacheManager.setKey(strKey);
			gmTmpCacheManager.setSearchPrefix("Glo");
			gmTmpCacheManager.setMaxFetchCount(100);
			gmTmpCacheManager.returnJsonString(gmMasterRedisReportBean.loadTagDistList(hmParam));
		}
	}

	/**
	 * syncPartNumList - This method used to sync the part list to redis
	 * 
	 * @param hmParam
	 * @throws AppError
	 */
	public void syncPartNumList(HashMap hmParam) {
		log.debug("syncPartNumList=>");
		GmMasterRedisReportBean gmMasterRedisReportBean = new GmMasterRedisReportBean(getGmDataStoreVO());
		String strId = GmCommonClass.parseNull((String) hmParam.get("ID"));
		String strOldName = GmCommonClass.parseNull((String) hmParam.get("OLD_NM"));
		String strNewName = GmCommonClass.parseNull((String) hmParam.get("NEW_NM"));
		GmCacheManager gmCacheManager = new GmCacheManager();
		String strKey = gmMasterRedisReportBean.getAutoCompleteKeys("PART_LIST", "Y", getCompId());
		log.debug("strKey==>" + strKey);
		String strNewDataFl = gmCacheManager.updateSortedSetInCache(strKey,strOldName, strNewName, strId);

		if (strNewDataFl.equalsIgnoreCase("Y")) {
			GmCacheManager gmTmpCacheManager = new GmCacheManager();
			gmTmpCacheManager.setKey(strKey);
			gmTmpCacheManager.setSearchPrefix("Glo");
			gmTmpCacheManager.setMaxFetchCount(100);
			gmTmpCacheManager.returnJsonString(gmMasterRedisReportBean.loadPartNumList(hmParam));
		}
	}

	/**
	 * fetchSyncSetDetails to fetch the data and update based on Job
	 * 
	 * @author
	 * @param
	 * @return ArrayList
	 */
	public ArrayList fetchSyncSetDetails(String strCompanyId) throws AppError {
		ArrayList alResult = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		log.debug("strCompanyId---->"+strCompanyId);
		//String strCompanyId = GmCommonClass.parseNull(getCompId());
		gmDBManager.setPrepareString("gm_pkg_kit_mapping_rpt.gm_fch_sync_set",2);
		gmDBManager.setString(1, strCompanyId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();

		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		log.debug("alResult.size() -->" + alResult.size());

		return alResult;
	}

	/**
	 * fetchSyncTagDetails to fetch the data and update based on Job
	 * 
	 * @author
	 * @param
	 * @return ArrayList
	 */
	public ArrayList fetchSyncTagDetails(String strDistId) throws AppError {
		ArrayList alResult = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strCompanyId = GmCommonClass.parseNull(getCompId());
		log.debug("strCompanyId===>>" + strCompanyId);

		gmDBManager.setPrepareString("gm_pkg_kit_mapping_rpt.gm_fch_sync_tag_set", 3);
		gmDBManager.setString(1, strCompanyId);
		gmDBManager.setString(2, strDistId);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();

		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		log.debug("alResult.size() -->" + alResult.size());

		return alResult;
	}
	  /**
	   * fetchSyncPartDetails to fetch the part num details data based on job
	   * 
	   * @author
	   * @param
	   * @return ArrayList
	   */
	  public ArrayList fetchSyncPartDetails() throws AppError {
	    ArrayList alResult = new ArrayList();
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strCompanyId = GmCommonClass.parseNull(getCompId());
	    gmDBManager.setPrepareString("gm_pkg_kit_mapping_rpt.gm_fch_sync_part", 2);
	    gmDBManager.setString(1, strCompanyId);
	    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
	    gmDBManager.close();
	    log.debug("alResult.size() -->" + alResult.size());

	    return alResult;
	  } 
	/**
	 * syncKitNameList - This method used to sync kit name list for case screen
	 * 
	 * @param hmParam
	 * @throws AppError
	 */
	public void syncKitNameList(HashMap hmParam) {

		GmMasterRedisReportBean gmMasterRedisReportBean = new GmMasterRedisReportBean(getGmDataStoreVO());
		GmCacheManager gmCacheManager = new GmCacheManager();

		String strId = GmCommonClass.parseNull((String) hmParam.get("ID"));
		String strOldName = GmCommonClass.parseNull((String) hmParam.get("OLD_NM"));
		String strNewName = GmCommonClass.parseNull((String) hmParam.get("NEW_NM"));
		String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
		String strKitType = GmCommonClass.parseNull((String) hmParam.get("KITTYPE"));
		String strActiveFl = GmCommonClass.parseNull((String) hmParam.get("ACTIVEFL"));
		String strKey = "";
		/*Based on kittype update key on redis 107923-Part Kit, 107922-Set Kit*/
		if(strKitType.equals("107922")){
			strKey = gmMasterRedisReportBean.getAutoCompleteKeys("SET_KIT_NAME_LIST", "Y", strPartyId);
		}
		else if(strKitType.equals("107923")){
			strKey = gmMasterRedisReportBean.getAutoCompleteKeys("PART_KIT_NAME_LIST", "Y", strPartyId);
		}else{
			strKey = gmMasterRedisReportBean.getAutoCompleteKeys("KIT_NAME_LIST", "Y", strPartyId);
		}
		String strNewDataFl = gmCacheManager.updateSortedSetInCache(strKey,strId, strNewName, strId);

		if (strNewDataFl.equalsIgnoreCase("Y")) {
			GmCacheManager gmTmpCacheManager = new GmCacheManager();
			gmTmpCacheManager.setKey(strKey);
			gmTmpCacheManager.setSearchPrefix("Glo");
			gmTmpCacheManager.setMaxFetchCount(100);
			gmTmpCacheManager.returnJsonString(gmMasterRedisReportBean.fetchKitNmList(hmParam));
		}
		/*Remove Key from redis when activeFl is N*/
		if(strActiveFl.equals("N")){
	        GmCacheManager gmTmpCacheManager = new GmCacheManager();
	        gmCacheManager.removeKey(strKey);
	        gmTmpCacheManager.returnJsonString(gmMasterRedisReportBean.fetchKitNmList(hmParam));
	   }
	}
}
