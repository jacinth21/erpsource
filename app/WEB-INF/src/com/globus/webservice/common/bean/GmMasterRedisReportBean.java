package com.globus.webservice.common.bean;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmMasterRedisReportBean extends GmBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * Constructor will populate company info.
	 * 
	 * @param gmDataStore
	 */
	public GmMasterRedisReportBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	}

	/**
	 * @param strKey
	 * @param StrCompanyFl
	 * @return
	 * @throws AppError
	 */
	public String getAutoCompleteKeys(String strKey, String StrCompanyFl,
			String strId) throws AppError {
		
		String strReturnKey = "";
		log.debug(" before strReturnKey ==> " + strKey);
		String strLocale = "";
		if (getComplangid().equals("10306122")) { // 10306122 Japanese
			strLocale = "JP";
		}
		// based on locale to get the Redis key
		GmResourceBundleBean rbRedis = GmCommonClass.getResourceBundleBean("properties.redis.GmRedis", strLocale);
		String strPropertiesValue = GmCommonClass.parseNull(rbRedis.getProperty(strKey));
		strPropertiesValue = strPropertiesValue.equals("") ? strKey: strPropertiesValue;
		if (StrCompanyFl.equalsIgnoreCase("Y")) {
			strPropertiesValue = strPropertiesValue + ":" + strId;
		}
		strReturnKey = strPropertiesValue + ":AUTO";
		log.debug(" After strReturnKey ==> " + strReturnKey);
		return strReturnKey;
	}

	/**
	 * loadSetTagDistList - To fetch the load the list of set details to JSON to
	 * cache
	 * 
	 * @param HashMap
	 * @return String
	 * @throws AppError
	 */
	public String loadSetTagDistList(HashMap hmParam) throws AppError {

		String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
		String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
		int iMaxCount = (Integer) hmParam.get("MAX_CNT");
		String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
		String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
		gmCacheManager.setKey(strKey);
		gmCacheManager.setSearchPrefix(searchPrefix);
		gmCacheManager.setMaxFetchCount(iMaxCount);
		String strReturn = gmCacheManager.returnJsonString(getSetTagDistQuery(hmParam)); // getSetTagDistQuery(hmParam);
		log.debug("strReturn===>" + strReturn.length());
		return strReturn;
	}

	/**
	 * getSetTagDistQuery - To fetch the set List details
	 * 
	 * @param HashMap
	 * @return String
	 * @throws AppError
	 */
	public String getSetTagDistQuery(HashMap hmParam) throws AppError {

		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
		String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
		String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
		log.debug("strCompanyId==>" + strCompanyId + " strDistId==>"+ strDistId);
		//gmCommonBean.saveMyTemp(strDistId);
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT t.* from (");
		sbQuery.append(" SELECT TAG.TAGID,TAG.TRANSID, T207.C207_SET_ID SETID, T701.C701_DISTRIBUTOR_NAME DNAME, T701.C701_DISTRIBUTOR_ID DID, ");
		sbQuery.append(" RANK()  OVER (partition BY TRANSID ORDER BY TAGID) H_RANK, ");
		sbQuery.append(" TAG.TAGID ||'-' || T207.C207_SET_ID ID, TAG.TAGID ALTNAME, DECODE(TAG.TAGID,NULL,'*Missing Tag*',TAG.TAGID) || '-' || T207.C207_SET_ID ||'-' || T207.C207_SET_NM NAME ");
		sbQuery.append(" FROM T207_SET_MASTER T207, T2080_SET_COMPANY_MAPPING T2080, T701_DISTRIBUTOR T701, ");
		sbQuery.append(" (SELECT T5010.C207_SET_ID SETID, T5010.C5010_TAG_ID TAGID, C5010_LOCATION_ID DID, nvl(T921.C921_REF_ID,T5010.C5010_LAST_UPDATED_TRANS_ID) TRANSID ");
		sbQuery.append(" FROM T5010_TAG T5010, T921_TRANSFER_DETAIL T921 ");
		sbQuery.append(" WHERE T5010.C5010_LAST_UPDATED_TRANS_ID = T921.C920_TRANSFER_ID(+) AND T5010.C901_LOCATION_TYPE = 4120 ");
		sbQuery.append(" AND  C901_LINK_TYPE(+) = 90360 ");
		sbQuery.append(" AND T5010.C5010_LOCATION_ID = '" + strDistId + "' ");
		sbQuery.append(" AND T5010.c901_inventory_type = '10003'"); // Loose Item/Sales Consignment  
		sbQuery.append(" AND C207_SET_ID   IS NOT NULL ");
		sbQuery.append(" AND C1900_COMPANY_ID = "+strCompanyId);
		sbQuery.append(" AND T5010.C5010_VOID_FL IS NULL AND T921.C921_VOID_FL IS NULL");
		sbQuery.append(" UNION ALL ");	
		
		sbQuery.append(" SELECT DISTINCT T504.C207_SET_ID, NULL TAGID,T504.C701_DISTRIBUTOR_ID DID, ");
		sbQuery.append(" T504.C504_CONSIGNMENT_ID TRNASID ");
		sbQuery.append(" FROM T5053_LOCATION_PART_MAPPING T5053, T5051_INV_WAREHOUSE T5051, T5052_LOCATION_MASTER T5052,");
		sbQuery.append(" T505_ITEM_CONSIGNMENT T505 , T504_CONSIGNMENT T504 ");
		sbQuery.append(" WHERE T5051.C5051_INV_WAREHOUSE_ID = T5052.C5051_INV_WAREHOUSE_ID ");
		sbQuery.append(" AND T505.C205_PART_NUMBER_ID       = T5053.C205_PART_NUMBER_ID "); 
		sbQuery.append(" AND T504.C504_CONSIGNMENT_ID = T505.C504_CONSIGNMENT_ID ");
		sbQuery.append(" AND REGEXP_LIKE (T5053.C205_PART_NUMBER_ID, NVL (NULL, T5053.C205_PART_NUMBER_ID)) ");
		sbQuery.append(" AND T5052.C5052_LOCATION_ID      = NVL (NULL, T5052.C5052_LOCATION_ID) ");
		sbQuery.append(" AND T5052.C5052_REF_ID           = DECODE('" + strDistId + "','0',T5052.C5052_REF_ID,'" + strDistId + "') ");
		sbQuery.append(" AND T5052.C901_LOCATION_TYPE     = DECODE('4000338','0',T5052.C901_LOCATION_TYPE,'4000338') ");
		sbQuery.append(" AND T5051.C5051_INV_WAREHOUSE_ID = '3' ");
		sbQuery.append(" AND T504.C701_DISTRIBUTOR_ID  ='" + strDistId + "' ");
		sbQuery.append(" AND t504.C504_STATUS_FL          = '4' ");
		sbQuery.append(" AND t504.C504_TYPE               = 4110 ");
		sbQuery.append(" AND t504.C1900_COMPANY_ID        = "+strCompanyId);
		sbQuery.append(" AND t504.c504_consignment_id NOT LIKE 'TFCN%' AND t5053.c5053_curr_qty > 0 "); 
		sbQuery.append(" AND T5053.C5052_LOCATION_ID      = T5052.C5052_LOCATION_ID ");
		sbQuery.append(" AND T5052.C1900_COMPANY_ID       = "+strCompanyId);
		sbQuery.append(" AND T504.C207_SET_ID IS NOT NULL ");
		sbQuery.append(" AND T5052.C5052_VOID_FL         IS NULL ");
		sbQuery.append(" AND C504_VOID_FL                IS NULL ");
		sbQuery.append(" AND C505_VOID_FL IS NULL ");
		sbQuery.append(" MINUS ");
		sbQuery.append(" SELECT C207_SET_ID SETID, ");
		sbQuery.append(" NULL TAGID, ");
		sbQuery.append(" C5011_LOCATION_ID DID, ");
		sbQuery.append(" C5011_LAST_UPDATED_TRANS_ID TRANSID ");
		sbQuery.append(" FROM T5011_TAG_LOG ");
		sbQuery.append(" WHERE C5011_LOCATION_ID = '" + strDistId + "' ");
		sbQuery.append(" AND C901_LOCATION_TYPE  = 4120 ");	
		sbQuery.append(" AND c901_inventory_type = '10003'");	//Loose Item/Sales Consignment
		sbQuery.append(" AND C207_SET_ID   IS NOT NULL ");
		sbQuery.append(" AND C5011_VOID_FL IS NULL)TAG ");
		
		sbQuery.append(" WHERE T207.C207_SET_ID = TAG.SETID AND T701.C701_DISTRIBUTOR_ID = TAG.DID ");
		sbQuery.append(" AND T2080.C207_SET_ID = T207.C207_SET_ID " );
		sbQuery.append(" AND T207.C901_SET_GRP_TYPE = 1601" );
		sbQuery.append(" AND T207.C207_TYPE = '4070' ");
		sbQuery.append(" AND C207_VOID_FL IS NULL AND T2080.C2080_VOID_FL IS NULL ");
		sbQuery.append(" AND T2080.C1900_COMPANY_ID =  "+strCompanyId);
		sbQuery.append(" ORDER BY UPPER(T207.C207_Set_Id)) t WHERE H_RANK = 1 ");
		log.debug(" Query for loading set list " + sbQuery.toString());
		//gmCommonBean.saveDeleteMyTemp(gmDBManager);
		return sbQuery.toString();
	}

	/**
	 * loadTagDistList - To fetch the load the list of set details based on tag
	 * 
	 * @param HashMap
	 * @return String
	 * @throws AppError
	 */
	public String loadTagDistList(HashMap hmParam) throws AppError {

		String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
		String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
		int iMaxCount = (Integer) hmParam.get("MAX_CNT");
		String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
		String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
		gmCacheManager.setKey(strKey);
		gmCacheManager.setSearchPrefix(searchPrefix);
		gmCacheManager.setMaxFetchCount(iMaxCount);
		String strReturn = gmCacheManager.returnJsonString(getTagDistQuery(hmParam)); // getSetTagDistQuery(hmParam);
		log.debug("strReturn===>" + strReturn.length());
		return strReturn;
	}

	/**
	 * getTagDistQuery - To fetch the set List details based on tag
	 * 
	 * @param HashMap
	 * @return String
	 * @throws AppError
	 */
	public String getTagDistQuery(HashMap hmParam) throws AppError {

		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
		String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
		String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
		String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
		log.debug("strCompanyId==>" + strCompanyId);

		StringBuffer sbQuery = new StringBuffer();

		sbQuery.append(" SELECT T5010.C5010_Tag_Id tagid, ");
		sbQuery.append(" t5010.c5010_last_updated_trans_id txnid, ");
		sbQuery.append(" T5010.C207_SET_ID SETID, GET_SET_NAME(T5010.C207_SET_ID) SETNM, ");
		sbQuery.append(" T701.C701_DISTRIBUTOR_NAME DNAME, T701.C701_DISTRIBUTOR_ID DID, ");
		sbQuery.append(" T5010.C5010_TAG_ID ||'-'||T5010.C207_SET_ID ID, ");
		sbQuery.append(" T5010.C5010_TAG_ID || '-'|| T5010.C207_SET_ID ||'-'|| GET_SET_NAME(T5010.C207_SET_ID) ||'*Consigned to '||t701.c701_distributor_name||'*' NAME ");
		sbQuery.append(" FROM T5010_Tag T5010, t701_distributor t701, t207_set_master t207 ");
		sbQuery.append(" WHERE t5010.c5010_location_id = t701.c701_distributor_id ");
		sbQuery.append(" AND t207.C207_SET_ID = T5010.C207_SET_ID ");
		sbQuery.append(" AND t207.c207_type = 4070 ");
		sbQuery.append(" AND t701.C701_DISTRIBUTOR_ID != "+strDistId);
		sbQuery.append(" AND T5010.C901_LOCATION_TYPE  = 4120 ");
		sbQuery.append(" AND T5010.C1900_COMPANY_ID    = " + strCompanyId);
		sbQuery.append(" AND t701.c701_void_fl IS NULL ");
		sbQuery.append(" AND T5010.C5010_VOID_FL IS NULL AND T5010.C207_SET_ID IS NOT NULL  AND t207.C207_VOID_FL IS NULL");
		sbQuery.append(" ORDER BY UPPER(T5010.C5010_Tag_Id) ");

		log.debug(" Query for loading set list " + sbQuery.toString());
		return sbQuery.toString();
	}

	/**
	 * loadPartNumList - To load the list of part details to JSON to cache
	 * 
	 * @param HashMap
	 * @return String
	 * @throws AppError
	 */
	public String loadPartNumList(HashMap hmParam) throws AppError {

		String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
		String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
		int iMaxCount = (Integer) hmParam.get("MAX_CNT");
		String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
		GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
		gmCacheManager.setKey(strKey);
		gmCacheManager.setSearchPrefix(searchPrefix);
		gmCacheManager.setMaxFetchCount(iMaxCount);
		hmParam.put("TYPE", strType);
		String strReturn = gmCacheManager.returnJsonString(getPartNumQueryStr(hmParam)); // getPartNumQueryStr(hmParam);
		return strReturn;
	}

	/**
	 * getPartNumQueryStr - To fetch the part number List details
	 * 
	 * @param HashMap
	 * @return String
	 * @throws AppError
	 */
	public String getPartNumQueryStr(HashMap hmParam) throws AppError {

		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
		String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMAPNYID"));
		ArrayList alReturn = new ArrayList();
		HashMap hmRuleData = new HashMap();

		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT T205.C205_PART_NUMBER_ID ID, T205.C205_PART_NUMBER_ID || ' - ' || T205.C205_PART_NUM_DESC NAME ");
		sbQuery.append(" FROM T205_PART_NUMBER T205, T205D_PART_ATTRIBUTE T205D, ");
		sbQuery.append(" (SELECT C901_RFS_ID RFSID, C1900_COMPANY_ID CMPID FROM T901C_RFS_COMPANY_MAPPING WHERE C901C_VOID_FL IS NULL UNION SELECT 80120 RFSID, "
				+ strCompanyId
				+ " CMPID FROM DUAL)RFS "
				+ " WHERE T205.C205_PART_NUMBER_ID = T205D.C205_PART_NUMBER_ID and t205d.c205d_void_fl is null "
				+ " and t205.c205_active_fl = 'Y' "
				+ "  and t205d.c205d_attribute_value = to_char(RFS.Rfsid) and RFS.cmpid = '"
				+ strCompanyId + "' and t205d.c901_attribute_type=80180 ");
		sbQuery.append(" ORDER BY UPPER(T205.C205_PART_NUMBER_ID) ");
		log.debug(" Query for loading set list " + sbQuery.toString());
		return sbQuery.toString();
	}

	/**
	 * loadSalesRepList - This method to fetch the sales rep setup information
	 * based on user search
	 * 
	 * @param strKey
	 * @param searchPrefix
	 * @param iMaxCount
	 * @return
	 * @throws AppError
	 */
	public String loadSalesRepList(String strKey, String searchPrefix,
			int iMaxCount, HashMap hmParam) throws AppError {
		GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
		gmCacheManager.setKey(strKey);
		gmCacheManager.setSearchPrefix(searchPrefix);
		gmCacheManager.setMaxFetchCount(iMaxCount);
		gmCacheManager.setStrSearchType(GmCommonClass.parseNull((String) hmParam.get("SEARCHTYPE")));
		String strReturn = gmCacheManager.returnJsonString(getSalesRepQueryStr(new HashMap()));
		return strReturn;
	}

	public String getSalesRepQueryStr(HashMap hmParam) throws AppError {

		log.debug(" hmParam ::: >>>" + hmParam);

		ArrayList alReturn = new ArrayList();
		String strFilter = GmCommonClass.parseNull((String) hmParam.get("FILTER"));
		String strShipTyp = GmCommonClass.parseNull((String) hmParam.get("SHIPTYPE"));
		String strRepID = GmCommonClass.parseNull((String) hmParam.get("REPID"));
		String strDivID = GmCommonClass.parseNull((String) hmParam.get("DIVID"));
		String strAccountSrc = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTSRC"));
		String strDirectRepType = GmCommonClass.parseNull((String) hmParam.get("DIRECTREPTYPE"));
		String strRepFilter = GmCommonClass.parseNull((String) hmParam.get("REPFILTER"));
		String strCompanyID = GmCommonClass.parseNull((String) hmParam.get("COMPANY_ID"));

		/*
		 * When Company ID is passed in along with the HashMap then we have to
		 * filter the Rep based on the passed in company . Use the Company from
		 * Context only when the HashMap does not have the Company ID.
		 */
		strCompanyID = strCompanyID.equals("") ? getCompId() : strCompanyID;

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT C703_SALES_REP_ID REPID,T703.C101_PARTY_ID ID, 'S' || C703_SALES_REP_ID SID, t701.C701_DISTRIBUTOR_ID DID,  DECODE('"
				+ getComplangid()
				+ "' , '103097',NVL(C703_SALES_REP_NAME_EN, C703_SALES_REP_NAME), C703_SALES_REP_NAME) NAME,  ");
		sbQuery.append(" t701.c701_distributor_name DNAME ");
		sbQuery.append(" , T101.C101_PARTY_ID  PARTYID ,  T101.C101_FIRST_NM || ' ' || T101.C101_LAST_NM PARTYNAME  "); // Party
		sbQuery.append(" , lower( DECODE ('"
				+ getComplangid()
				+ "', '103097', NVL (C703_SALES_REP_NAME_EN, C703_SALES_REP_NAME),  C703_SALES_REP_NAME)) repname ");
		sbQuery.append(" FROM T703_SALES_REP  T703 , T101_PARTY T101, t701_distributor t701 ");
		sbQuery.append(" WHERE  T703.C101_PARTY_ID = T101.C101_PARTY_ID  AND  C703_VOID_FL IS NULL ");
		sbQuery.append(" AND t703.c701_distributor_id = t701.c701_distributor_id ");
		sbQuery.append(" AND t701.c701_void_fl IS NULL ");
		sbQuery.append(" AND T703.C1900_COMPANY_ID = " + strCompanyID);
		sbQuery.append(" AND T703.C703_ACTIVE_FL = 'Y' ");
		sbQuery.append(" ORDER BY repname ");
		log.debug(" Query to fetch Rep Details is GmAutoCompleteBean >>>>  "
				+ sbQuery.toString());
		return sbQuery.toString();
	}

	/**
	 * fetchKitNmList - To fetch the kit name for autocomplete dropdown kit list
	 * 
	 * @param HashMap
	 * @return String
	 * @throws AppError
	 */
	public String fetchKitNmList(HashMap hmParam) throws AppError {

		String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
		String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
		int iMaxCount = (Integer) hmParam.get("MAX_CNT");
		String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));

		GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
		gmCacheManager.setKey(strKey);
		gmCacheManager.setSearchPrefix(searchPrefix);
		gmCacheManager.setMaxFetchCount(iMaxCount);
		gmCacheManager.setMaxFetchCount(iMaxCount);
		HashMap hmdata = new HashMap();
		hmdata.put("TYPE", strType);

		String strReturn = gmCacheManager.returnJsonString(loadKitNmList(hmParam));

		return strReturn;
	}

	/**
	 * loadKitNmList - To fetch the kit name for autocomplete dropdown kit name
	 * list
	 * 
	 * @param HashMap
	 * @return String
	 * @throws AppError
	 */
	public String loadKitNmList(HashMap hmParam) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		StringBuffer sbQuery = new StringBuffer();

		String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
		String strActiveFl = GmCommonClass.parseNull((String) hmParam.get("ACTIVEFL"));
		String strKitType = GmCommonClass.parseNull((String) hmParam.get("KITTYPE"));

		sbQuery.append(" SELECT t2078.C2078_KIT_MAP_ID ID, t2078.C2078_KIT_NM NAME");
		sbQuery.append(" FROM T2078_KIT_MAPPING t2078 ");
		sbQuery.append(" WHERE C2078_VOID_FL is NULL ");
		sbQuery.append(" AND C2078_ACTIVE_FL ='Y' ");
		sbQuery.append(" AND T2078.c2078_OWNER_PARTY_ID = "+strPartyId);
		
		/*Based on kittype fetch kit from Redis*/
		if (strKitType.equals("107922")){
			sbQuery.append(" AND T2078.C901_KIT_TYPE IN (107924,107922)");
		}else if (strKitType.equals("107923")){
			sbQuery.append(" AND T2078.C901_KIT_TYPE IN (107924,107923)");
		}
		
	
		sbQuery.append(" UNION ");
		
		sbQuery.append(" SELECT t2078.C2078_KIT_MAP_ID ID, t2078.C2078_KIT_NM NAME ");
		sbQuery.append(" FROM T2079B_KIT_REP_MAP T2079B,T2078_KIT_MAPPING t2078 ");
		sbQuery.append(" WHERE T2079B.C2078_KIT_MAP_ID = T2078.C2078_KIT_MAP_ID ");
		sbQuery.append(" AND T2079B.C2079B_REP_PARTY_ID = "+strPartyId);
		/*Based on kittype fetch kit from Redis*/
		if (strKitType.equals("107922")){
			sbQuery.append(" AND T2078.C901_KIT_TYPE IN (107924,107922)");
		}else if (strKitType.equals("107923")){
			sbQuery.append(" AND T2078.C901_KIT_TYPE IN (107924,107923)");
		}
		sbQuery.append(" AND C2079B_VOID_FL IS NULL ORDER BY NAME ASC");
		
		log.debug("sbQuery.toString() --> " + sbQuery.toString());

		return sbQuery.toString();
	}
/**
* fetchSetBundleNameList - To fetch the set Bundle Name.
* 
* @param HashMap
* @return String
* @throws AppError
*/
public String fetchSetBundleNameList(HashMap hmParam) throws AppError {
	 
	  String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
	  String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
	  int iMaxCount = (Integer) hmParam.get("MAX_CNT");
	  String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
	  
	  GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
	  gmCacheManager.setKey(strKey);
	  gmCacheManager.setSearchPrefix(searchPrefix);
	  gmCacheManager.setMaxFetchCount(iMaxCount);
	  HashMap hmdata = new HashMap();
	  hmdata.put("TYPE", strType);
	  
	  String strReturn = gmCacheManager.returnJsonString(loadSetBndNmList(hmParam));
	
	  return strReturn; 
}

/**
* loadSetBndNmList - To fetch the set Bundle Name details
* 
* @param HashMap
* @return String
* @throws AppError
*/
public String loadSetBndNmList(HashMap hmParam) throws AppError {
	  
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());	
	  StringBuffer sbQuery = new StringBuffer();
	  String strCmpId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
log.debug("strCmpId=========>"+strCmpId);
	  sbQuery.append(" select t.* from (SELECT t2075.C2075_SET_BUNDLE_ID ID, ");
	  sbQuery.append(" t2075.c2075_set_bundle_nm NAME, ");
	  sbQuery.append(" NVL(t2075A.c2075a_unque_keyword,'NO_KEYWORDS') ALTNAME ");
	  sbQuery.append(" FROM t2075_set_bundle t2075, T2075A_SET_BUNDLE_KEYWORD T2075A, t2081_set_bundle_comp_mapping t2081, T2076_SET_BUNDLE_DETAILS t2076 ");		  
	  sbQuery.append(" WHERE t2075.C2075_SET_BUNDLE_ID = T2075A.C2075_SET_BUNDLE_ID(+) ");
	  sbQuery.append(" AND T2075.C2075_SET_BUNDLE_ID = T2081.C2075_SET_BUNDLE_ID ");
	  sbQuery.append(" AND T2076.C2075_SET_BUNDLE_ID = T2081.C2075_SET_BUNDLE_ID ");
	  sbQuery.append(" AND T2076.C207_SET_ID IN (SELECT DISTINCT c207_set_id from T207_SET_MASTER WHERE c207_type = '4070' AND c207_void_fl IS NULL) ");
	  sbQuery.append(" AND t2081.c1900_company_id ="+strCmpId);
	  sbQuery.append(" AND t2075.c2075_void_fl IS NULL ");
	  sbQuery.append(" AND T2075A.C2075A_VOID_FL IS NULL ");
	  sbQuery.append(" AND t2081.C2081_VOID_FL IS NULL) t GROUP BY t.ID,t.NAME,t.ALTNAME ");
	  
	  log.debug("loadSetBndNmList==>"+sbQuery.toString());
	  return sbQuery.toString();

} 	  	
/**
* fetchSetNmList - To fetch the set name for autocomplete dropdown 
* 
* @param HashMap
* @return String
* @throws AppError
*/
public String fetchSetNmList(HashMap hmParam) throws AppError {

	String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
	String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
	int iMaxCount = (Integer) hmParam.get("MAX_CNT");
	String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));

	GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
	gmCacheManager.setKey(strKey);
	gmCacheManager.setSearchPrefix(searchPrefix);
	gmCacheManager.setMaxFetchCount(iMaxCount);
	HashMap hmdata = new HashMap();
	hmdata.put("TYPE", strType);

	String strReturn = gmCacheManager.returnJsonString(loadSetNmList(hmParam));

	return strReturn;
}

/**
* loadSetNmList - To fetch the set name for autocomplete dropdown
* list
* 
* @param HashMap
* @return String
* @throws AppError
*/
public String loadSetNmList(HashMap hmParam) throws AppError {

	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	StringBuffer sbQuery = new StringBuffer();

	String strCmpId = GmCommonClass.parseNull((String) hmParam.get("CMPID"));
	String strActiveFl = GmCommonClass.parseNull((String) hmParam.get("ACTIVEFL"));

	sbQuery.append(" SELECT T5010.C5010_Tag_Id tagid, ");
	sbQuery.append(" t5010.c5010_last_updated_trans_id txnid, ");
	sbQuery.append(" T5010.C207_SET_ID SETID, GET_SET_NAME(T5010.C207_SET_ID) SETNM, ");
	sbQuery.append(" T701.C701_DISTRIBUTOR_NAME DNAME, T701.C701_DISTRIBUTOR_ID DID, ");
	sbQuery.append(" T5010.C5010_TAG_ID ||'-'||T5010.C207_SET_ID ID, ");
	sbQuery.append(" T5010.C5010_TAG_ID || '-'|| T5010.C207_SET_ID ||'-'|| GET_SET_NAME(T5010.C207_SET_ID) NAME ");
	sbQuery.append(" FROM T5010_Tag T5010, t701_distributor t701, t207_set_master t207 ");
	sbQuery.append(" WHERE t5010.c5010_location_id = t701.c701_distributor_id ");
	sbQuery.append(" AND t207.C207_SET_ID = T5010.C207_SET_ID ");
	sbQuery.append(" AND t207.c207_type = 4070 ");
	sbQuery.append(" AND T5010.C901_LOCATION_TYPE  = 4120 ");
	sbQuery.append(" AND T5010.C1900_COMPANY_ID    = " + strCmpId);
	sbQuery.append(" AND t701.c701_void_fl IS NULL ");
	sbQuery.append(" AND T5010.C5010_VOID_FL IS NULL AND T5010.C207_SET_ID IS NOT NULL  AND t207.C207_VOID_FL IS NULL");
	sbQuery.append(" ORDER BY UPPER(T5010.C5010_Tag_Id) ");
	
	log.debug("sbQuery.toString() --> " + sbQuery.toString());

	return sbQuery.toString();
}
/**
* loadSurgeonList - To fetch the Surgeon list for autocomplete dropdown
* list
* 
* @param HashMap
* @return String
* @throws AppError
*/
public String loadSurgeonList(HashMap hmParam) throws AppError {

	String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
	String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
	int iMaxCount = (Integer) hmParam.get("MAX_CNT");
	String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
	String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
	gmCacheManager.setKey(strKey);
	gmCacheManager.setSearchPrefix(searchPrefix);
	gmCacheManager.setMaxFetchCount(iMaxCount);
	String strReturn = gmCacheManager.returnJsonString(getSurgeonListQuery(hmParam));
	return strReturn;
}
/**
* getSurgeonListQuery - To fetch the Surgeon list if search key is not present in redis for autocomplete dropdown
* list
* 
* @param HashMap
* @return String
* @throws AppError
*/
public String getSurgeonListQuery(HashMap hmParam) throws AppError {

	String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
	strType = strType.replaceAll(",", "','");
	StringBuffer sbQuery = new StringBuffer();
	sbQuery.append(" SELECT c101_party_id     ID,decode(c101_middle_initial, NULL,(get_code_name(npid.salute) ");
	sbQuery.append("|| ' ' || c101_first_nm|| ' '|| c101_last_nm),(get_code_name(npid.salute)|| ' '|| c101_first_nm ");
	sbQuery.append(" || ' ' || c101_middle_initial|| ' ' || c101_last_nm)) NAME FROM t101_party t101, ");                                                                                                                            
	sbQuery.append(" (SELECT c6600_surgeon_npi_id    npid, c101_party_surgeon_id   partyid, c6600_party_salute  salute FROM ");
	sbQuery.append("  t6600_party_surgeon WHERE c6600_void_fl IS NULL ) npid WHERE ( c101_active_fl IS NULL ");
	sbQuery.append("  OR c101_active_fl = 'Y' ) AND c101_void_fl IS NULL AND t101.c101_party_id = npid.partyid (+) ");
	sbQuery.append(" AND c901_party_type IN ( '"+strType+"' ) ORDER BY     upper(NAME) ");
	log.debug("query----->"+sbQuery.toString());
	return sbQuery.toString();
}


/**
 * loadRepList - This method to fetch the sales rep setup information
 * based on user search
 * 
 * @param strKey
 * @param searchPrefix
 * @param iMaxCount
 * @return
 * @throws AppError
 */
public String loadRepList(String strKey, String searchPrefix,
		int iMaxCount, HashMap hmParam) throws AppError {
	GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
	GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
	gmCacheManager.setKey(strKey);
	gmCacheManager.setSearchPrefix(searchPrefix);
	gmCacheManager.setMaxFetchCount(iMaxCount);
	gmCacheManager.setStrSearchType(GmCommonClass.parseNull((String) hmParam.get("SEARCHTYPE")));
	String strReturn = gmCacheManager.returnJsonString(getRepListQueryStr(new HashMap()));
	return strReturn;
}

public String getRepListQueryStr(HashMap hmParam) throws AppError {

	log.debug(" hmParam ::: >>>" + hmParam);

	ArrayList alReturn = new ArrayList();
	String strFilter = GmCommonClass.parseNull((String) hmParam.get("FILTER"));
	String strShipTyp = GmCommonClass.parseNull((String) hmParam.get("SHIPTYPE"));
	String strRepID = GmCommonClass.parseNull((String) hmParam.get("REPID"));
	String strDivID = GmCommonClass.parseNull((String) hmParam.get("DIVID"));
	String strAccountSrc = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTSRC"));
	String strDirectRepType = GmCommonClass.parseNull((String) hmParam.get("DIRECTREPTYPE"));
	String strRepFilter = GmCommonClass.parseNull((String) hmParam.get("REPFILTER"));
	String strCompanyID = GmCommonClass.parseNull((String) hmParam.get("COMPANY_ID"));

	/*
	 * When Company ID is passed in along with the HashMap then we have to
	 * filter the Rep based on the passed in company . Use the Company from
	 * Context only when the HashMap does not have the Company ID.
	 */
	strCompanyID = strCompanyID.equals("") ? getCompId() : strCompanyID;


	StringBuffer sbQuery = new StringBuffer();
	sbQuery.append(" SELECT C703_SALES_REP_ID ID,T703.C101_PARTY_ID reppartyid, 'S' || C703_SALES_REP_ID SID, t701.C701_DISTRIBUTOR_ID DID,  DECODE('"
			+ getComplangid()
			+ "' , '103097',NVL(C703_SALES_REP_NAME_EN, C703_SALES_REP_NAME), C703_SALES_REP_NAME) NAME,  ");
	sbQuery.append(" t701.c701_distributor_name DNAME ");
	sbQuery.append(" , T101.C101_PARTY_ID  PARTYID ,  T101.C101_FIRST_NM || ' ' || T101.C101_LAST_NM PARTYNAME  "); // Party
	sbQuery.append(" , lower( DECODE ('"
			+ getComplangid()
			+ "', '103097', NVL (C703_SALES_REP_NAME_EN, C703_SALES_REP_NAME),  C703_SALES_REP_NAME)) repname ");
	sbQuery.append(" FROM T703_SALES_REP  T703 , T101_PARTY T101, t701_distributor t701 ");
	sbQuery.append(" WHERE  T703.C101_PARTY_ID = T101.C101_PARTY_ID  AND  C703_VOID_FL IS NULL ");
	sbQuery.append(" AND t703.c701_distributor_id = t701.c701_distributor_id ");
	sbQuery.append(" AND t701.c701_void_fl IS NULL ");
	sbQuery.append(" AND T703.C1900_COMPANY_ID = " + strCompanyID);
	sbQuery.append(" AND T703.C703_ACTIVE_FL = 'Y' ");
	sbQuery.append(" ORDER BY repname ");
	log.debug(" Query to fetch Rep Details is GmAutoCompleteBean >>>>  "
			+ sbQuery.toString());
	return sbQuery.toString();
}

}
