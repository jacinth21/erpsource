package com.globus.webservice.prodmgmnt.mrktcollateral.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.mrktcollateral.beans.GmArtifactBean;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.valueobject.common.GmProdCatlReqVO;
import com.globus.valueobject.common.GmProdCatlResListVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.prodmgmnt.GmKeywordRefVO;
import com.globus.webservice.common.resources.GmResource;

/**
 * @author Anilkumar
 * 
 */
@Path("keyword")
public class GmKeywordResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchSystems - This method will returns Keywords values while calling web service. First call,
   * It will returns all Keyword values(Master Sync) which are published to product catalog after
   * that it will returns updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @POST
  @Path("master")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlResListVO fetchKeywords(String strInput) throws AppError {

    HashMap hmValidate = new HashMap();
    HashMap hmParams = new HashMap();
    ArrayList alResult = new ArrayList();
    List<GmKeywordRefVO> listGmKeywordRefVO = new ArrayList<GmKeywordRefVO>();

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();
    GmProdCatlReqVO gmProdcatlReqVO = new GmProdCatlReqVO();
    GmKeywordRefVO gmKeywordRefVO = new GmKeywordRefVO();
    GmArtifactBean gmArtifactBean = new GmArtifactBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmWSUtil gmWSUtil = new GmWSUtil();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdcatlReqVO);
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID
    gmProdCatUtil.validateInput(gmProdcatlReqVO, hmValidate);
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdcatlReqVO.getToken());
    hmParams.put("UUID", GmCommonClass.parseNull(gmProdcatlReqVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdcatlReqVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdcatlReqVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdcatlReqVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("VO", gmKeywordRefVO);
    listGmKeywordRefVO = gmArtifactBean.fetchKeywords(hmParams);
    gmKeywordRefVO = (GmKeywordRefVO) gmProdCatlResListVO.setPageProperties(listGmKeywordRefVO);
    gmKeywordRefVO = (gmKeywordRefVO == null) ? new GmKeywordRefVO() : gmKeywordRefVO;

    gmProdCatlResListVO.setReftype("4000442"); // Keyword sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmProdCatlResListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmProdCatlResListVO.getPagesize()));
    hmParams.put("LASTREC", gmKeywordRefVO.getKeywordid()); // KEYWORDID

    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);
    gmProdCatlResListVO.setListGmKeywordRefVO(listGmKeywordRefVO);

    return gmProdCatlResListVO;

  }

}
