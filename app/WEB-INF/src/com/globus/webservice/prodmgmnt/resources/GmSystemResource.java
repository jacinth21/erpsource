package com.globus.webservice.prodmgmnt.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.beans.GmSystemServiceBean;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.valueobject.common.GmProdCatlResListVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.prodmgmnt.GmSetAttributeVO;
import com.globus.valueobject.prodmgmnt.GmSetMasterVO;
import com.globus.valueobject.prodmgmnt.GmSetPartVO;
import com.globus.valueobject.prodmgmnt.GmSystemAttributeVO;
import com.globus.valueobject.prodmgmnt.GmSystemVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.config.Compress;

/**
 * @author elango
 * 
 */
@Path("system")
public class GmSystemResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchSystems - This method will returns SYSTEM values while calling web service. First call, It
   * will returns all SYSTEM values(Master Sync) which are published to product catalog after that
   * it will returns updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @Compress
  @POST
  @Path("master")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlResListVO fetchSystems(String strInput) throws AppError {
    log.debug("=======fetchSystems======");

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }

    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSystemVO gmSystemVO = new GmSystemVO();
    GmSystemServiceBean gmSystemServiceBean = new GmSystemServiceBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmSystemVO> listGmSystemVO = new ArrayList<GmSystemVO>();

    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("VO", gmSystemVO);
    listGmSystemVO = gmSystemServiceBean.fetchSystems(hmParams);
    gmSystemVO = (GmSystemVO) gmProdCatlResListVO.setPageProperties(listGmSystemVO);

    gmSystemVO = (gmSystemVO == null) ? new GmSystemVO() : gmSystemVO;

    gmProdCatlResListVO.setReftype("103108");// SYSTEM sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmProdCatlResListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmProdCatlResListVO.getPagesize()));
    hmParams.put("LASTREC", gmSystemVO.getSystemid()); // SYSTEMID

    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmProdCatlResListVO.setListGmSystemVO(listGmSystemVO);

    return gmProdCatlResListVO;
  }
  
//PMT#37595 - System availability in iPad based on System Release
  @Compress
  @POST
  @Path("masterjson")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public String fetchJsonSystems(String strInput) throws AppError {
    log.debug("=======New fetchSystems======");

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }

    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSystemVO gmSystemVO = new GmSystemVO();
    GmSystemServiceBean gmSystemServiceBean = new GmSystemServiceBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmSystemVO> listGmSystemVO = new ArrayList<GmSystemVO>();

    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("COMPID", GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid()));
    hmParams.put("VO", gmSystemVO);
     String strReturnJSON = gmSystemServiceBean.fetchJsonSystems(hmParams);
       log.debug("strReturnJSON Master?? "+strReturnJSON.length());
    gmSystemVO = (GmSystemVO) gmProdCatlResListVO.setPageProperties(listGmSystemVO);

    gmSystemVO = (gmSystemVO == null) ? new GmSystemVO() : gmSystemVO;

    gmProdCatlResListVO.setReftype("103108");// SYSTEM sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmProdCatlResListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmProdCatlResListVO.getPagesize()));
    hmParams.put("LASTREC", gmSystemVO.getSystemid()); // SYSTEMID
    hmParams.put("LASTREC", "SYNCJSON"); 

    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmProdCatlResListVO.setListGmSystemVO(listGmSystemVO);

    return strReturnJSON;
  }
  

  /**
   * fetchSetMaster - This method will returns SET values while calling web service. First call, It
   * will returns all SET values(Master Sync) which are published to product catalog after that it
   * will returns updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @Compress
  @POST
  @Path("setmaster")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlResListVO fetchSetMaster(String strInput) throws AppError {
    log.debug("=======fetchSetMaster======");

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSetMasterVO gmSetMasterVO = new GmSetMasterVO();
    // op changess start
    GmSystemServiceBean gmSystemServiceBean = new GmSystemServiceBean(gmProdCatlResListVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmProdCatlResListVO);
    // op changess end
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmSetMasterVO> listGmSetMasterVO = new ArrayList<GmSetMasterVO>();

    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("VO", gmSetMasterVO);
    listGmSetMasterVO = gmSystemServiceBean.fetchSetMaster(hmParams);


    gmSetMasterVO = (GmSetMasterVO) gmProdCatlResListVO.setPageProperties(listGmSetMasterVO);

    gmSetMasterVO = (gmSetMasterVO == null) ? new GmSetMasterVO() : gmSetMasterVO;

    gmProdCatlResListVO.setReftype("103111"); // SET sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmProdCatlResListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmProdCatlResListVO.getPagesize()));
    hmParams.put("LASTREC", gmSetMasterVO.getSetid()); // SETID
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);
    gmProdCatlResListVO.setListGmSetMasterVO(listGmSetMasterVO);

    return gmProdCatlResListVO;
  }
  
  
  
//PMT#37773 - Set availability in iPad based on System Release
  @Compress
  @POST
  @Path("setmasterjson")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public String fetchJsonSetMaster(String strInput) throws AppError {
    log.debug("=======fetchSetMaster======");

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSetMasterVO gmSetMasterVO = new GmSetMasterVO();
    // op changess start
    GmSystemServiceBean gmSystemServiceBean = new GmSystemServiceBean(gmProdCatlResListVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmProdCatlResListVO);
    // op changess end
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmSetMasterVO> listGmSetMasterVO = new ArrayList<GmSetMasterVO>();

    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("COMPID", GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid()));
    hmParams.put("VO", gmSetMasterVO);
    String strReturnJSON = gmSystemServiceBean.fetchJsonSetMaster(hmParams);
    log.debug("strReturnJSON SetMaster?? "+strReturnJSON);

    gmSetMasterVO = (GmSetMasterVO) gmProdCatlResListVO.setPageProperties(listGmSetMasterVO);

    gmSetMasterVO = (gmSetMasterVO == null) ? new GmSetMasterVO() : gmSetMasterVO;

    gmProdCatlResListVO.setReftype("103111"); // SET sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmProdCatlResListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmProdCatlResListVO.getPagesize()));
    hmParams.put("LASTREC", gmSetMasterVO.getSetid()); // SETID
    hmParams.put("LASTREC", "SYNCJSON");
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);
    gmProdCatlResListVO.setListGmSetMasterVO(listGmSetMasterVO);

    return strReturnJSON;
  }

  /**
   * fetchSetPartDetails - This method will returns SET Parts values while calling web service.
   * First call, It will returns all SET Parts values(Master Sync) which are published to product
   * catalog after that it will returns updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @Compress
  @POST
  @Path("setpart")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlResListVO fetchSetPartDetails(String strInput) throws AppError {
    log.debug("=======fetchSetPartDetails======");

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSetPartVO gmSetPartVO = new GmSetPartVO();
    // op changess start
    GmSystemServiceBean gmSystemServiceBean = new GmSystemServiceBean(gmProdCatlResListVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmProdCatlResListVO);
    // op changess end
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmSetPartVO> listGmSetPartVO = new ArrayList<GmSetPartVO>();

    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("VO", gmSetPartVO);
    listGmSetPartVO = gmSystemServiceBean.fetchSetPartDetails(hmParams);


    gmSetPartVO = (GmSetPartVO) gmProdCatlResListVO.setPageProperties(listGmSetPartVO);

    gmSetPartVO = (gmSetPartVO == null) ? new GmSetPartVO() : gmSetPartVO;

    gmProdCatlResListVO.setReftype("4000409"); // SET PART sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmProdCatlResListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmProdCatlResListVO.getPagesize()));
    hmParams.put("LASTREC", gmSetPartVO.getSetdtlid()); // SETDTLID

    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmProdCatlResListVO.setListGmSetPartVO(listGmSetPartVO);

    return gmProdCatlResListVO;
  }
  
//PMT#37773 - Set availability in iPad based on System Release
  @Compress
  @POST
  @Path("setpartjson")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public String fetchSetJsonPartDetails(String strInput) throws AppError {
    log.debug("=======fetchSetPartDetails======");

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSetPartVO gmSetPartVO = new GmSetPartVO();
    // op changess start
    GmSystemServiceBean gmSystemServiceBean = new GmSystemServiceBean(gmProdCatlResListVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmProdCatlResListVO);
    // op changess end
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmSetPartVO> listGmSetPartVO = new ArrayList<GmSetPartVO>();

    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("COMPID", GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid()));
    hmParams.put("VO", gmSetPartVO);
    String strReturnJSON = gmSystemServiceBean.fetchJsonSetPartDetails(hmParams);
    log.debug("strReturnJSON Part?? "+strReturnJSON);

    gmSetPartVO = (GmSetPartVO) gmProdCatlResListVO.setPageProperties(listGmSetPartVO);

    gmSetPartVO = (gmSetPartVO == null) ? new GmSetPartVO() : gmSetPartVO;

    gmProdCatlResListVO.setReftype("4000409"); // SET PART sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmProdCatlResListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmProdCatlResListVO.getPagesize()));
    hmParams.put("LASTREC", gmSetPartVO.getSetdtlid()); // SETDTLID
    hmParams.put("LASTREC", "SYNCJSON");

    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmProdCatlResListVO.setListGmSetPartVO(listGmSetPartVO);

    return strReturnJSON;
  }

  /**
   * fetchSystemAttributes - This method will returns SYSTEM Attribute values while calling web
   * service. First call, It will returns all SYSTEM Attribute values(Master Sync) which are
   * published to product catalog after that it will returns updated values only based on last sync
   * for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @Compress
  @POST
  @Path("systemattrib")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlResListVO fetchSystemAttributes(String strInput) throws AppError {
    log.debug("=======fetchSystemAttributes======");

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSystemAttributeVO gmSystemAttributeVO = new GmSystemAttributeVO();
    GmSystemServiceBean gmSystemServiceBean = new GmSystemServiceBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmSystemAttributeVO> listGmSystemAttributeVO = new ArrayList<GmSystemAttributeVO>();

    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("VO", gmSystemAttributeVO);

    listGmSystemAttributeVO = gmSystemServiceBean.fetchSystemAttributes(hmParams);
    gmSystemAttributeVO =
        (GmSystemAttributeVO) gmProdCatlResListVO.setPageProperties(listGmSystemAttributeVO);

    gmSystemAttributeVO =
        (gmSystemAttributeVO == null) ? new GmSystemAttributeVO() : gmSystemAttributeVO;

    gmProdCatlResListVO.setReftype("4000416");// SYSTEM Attribute sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmProdCatlResListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmProdCatlResListVO.getPagesize()));
    hmParams.put("LASTREC", gmSystemAttributeVO.getSetattrid()); // SETATTRID
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmProdCatlResListVO.setListGmSystemAttributeVO(listGmSystemAttributeVO);

    return gmProdCatlResListVO;
  }
  
  
//PMT#37595 - System availability in iPad based on System Release
  @Compress
  @POST
  @Path("systemattribjson")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public String fetchJsonSystemAttributes(String strInput) throws AppError {
    log.debug("=======new fetchSystemAttributes======");

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSystemAttributeVO gmSystemAttributeVO = new GmSystemAttributeVO();
    GmSystemServiceBean gmSystemServiceBean = new GmSystemServiceBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmSystemAttributeVO> listGmSystemAttributeVO = new ArrayList<GmSystemAttributeVO>();

    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("COMPID", GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid()));
    hmParams.put("VO", gmSystemAttributeVO);

    String strReturnJSON = gmSystemServiceBean.fetchSystemJsonAttributes(hmParams);
    log.debug("strReturnJSON systemattribjson >>>>>> "+strReturnJSON.length());
    gmSystemAttributeVO =
        (GmSystemAttributeVO) gmProdCatlResListVO.setPageProperties(listGmSystemAttributeVO);

    gmSystemAttributeVO =
        (gmSystemAttributeVO == null) ? new GmSystemAttributeVO() : gmSystemAttributeVO;

    gmProdCatlResListVO.setReftype("4000416");// SYSTEM Attribute sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmProdCatlResListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmProdCatlResListVO.getPagesize()));
    hmParams.put("LASTREC", gmSystemAttributeVO.getSetattrid()); // SETATTRID
    hmParams.put("LASTREC", "SYNCJSON");
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmProdCatlResListVO.setListGmSystemAttributeVO(listGmSystemAttributeVO);

    return strReturnJSON;
  }

  /**
   * fetchSetsAttribute - This method will returns SET Attribute values while calling web service.
   * First call, It will returns all SET Attribute values(Master Sync) . after that it will returns
   * updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @Compress
  @POST
  @Path("setAttr")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlResListVO fetchSetsAttribute(String strInput) throws AppError {

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSetAttributeVO gmSetAttributeVO = new GmSetAttributeVO();

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmSetAttributeVO> listGmSetAttributeVO = new ArrayList<GmSetAttributeVO>();
    // op changess start
    GmSystemServiceBean gmSystemServiceBean = new GmSystemServiceBean(gmProdCatlResListVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmProdCatlResListVO);
    // op changess end
    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("VO", gmSetAttributeVO);
    listGmSetAttributeVO = gmSystemServiceBean.fetchSetAttributes(hmParams);
    gmSetAttributeVO.setSetattrid("");
    gmSetAttributeVO =
        (GmSetAttributeVO) gmProdCatlResListVO.setPageProperties(listGmSetAttributeVO);

    gmSetAttributeVO = (gmSetAttributeVO == null) ? new GmSetAttributeVO() : gmSetAttributeVO;

    gmProdCatlResListVO.setReftype("4000736");// SET Attribute sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmProdCatlResListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmProdCatlResListVO.getPagesize()));
    hmParams.put("LASTREC", gmSetAttributeVO.getSetattrid());
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmProdCatlResListVO.setListGmSetAttributeVO(listGmSetAttributeVO);


    return gmProdCatlResListVO;
  }
  
//PMT#37773 - Set availability in iPad based on System Release
  @Compress
  @POST
  @Path("setAttrjson")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public String fetchJsonSetsAttribute(String strInput) throws AppError {

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmSetAttributeVO gmSetAttributeVO = new GmSetAttributeVO();

    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmSetAttributeVO> listGmSetAttributeVO = new ArrayList<GmSetAttributeVO>();
    // op changess start
    GmSystemServiceBean gmSystemServiceBean = new GmSystemServiceBean(gmProdCatlResListVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmProdCatlResListVO);
    // op changess end
    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("COMPID", GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid()));
    hmParams.put("VO", gmSetAttributeVO);
    String strReturnJSON = gmSystemServiceBean.fetchJsonSetAttributes(hmParams);
    log.debug("strReturnJSON ATTR ?? "+strReturnJSON.length());
    gmSetAttributeVO.setSetattrid("");
    gmSetAttributeVO =
        (GmSetAttributeVO) gmProdCatlResListVO.setPageProperties(listGmSetAttributeVO);

    gmSetAttributeVO = (gmSetAttributeVO == null) ? new GmSetAttributeVO() : gmSetAttributeVO;

    gmProdCatlResListVO.setReftype("4000736");// SET Attribute sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmProdCatlResListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmProdCatlResListVO.getPagesize()));
    hmParams.put("LASTREC", gmSetAttributeVO.getSetattrid());
    hmParams.put("LASTREC", "SYNCJSON");
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmProdCatlResListVO.setListGmSetAttributeVO(listGmSetAttributeVO);


    return strReturnJSON;
  }
}
