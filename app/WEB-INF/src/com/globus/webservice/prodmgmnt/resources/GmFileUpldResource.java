package com.globus.webservice.prodmgmnt.resources;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.beans.GmFileUploadBean;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.valueobject.common.GmProdCatlResListVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.prodmgmnt.GmFileVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.config.Compress;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

@Path("uploadedFile")
public class GmFileUpldResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchAllFilesInfo - This method will returns all file values while calling web service. First
   * call, It will returns all files which belongs to images Type-4000410, and which are published
   * to product catalog after that it will returns updated values only based on last sync for the
   * device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @Compress
  @POST
  @Path("master")  
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlResListVO fetchAllFilesInfo(String strInput) throws AppError {

    log.debug("=======fetchAllFilesInfo======");
    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmFileVO gmFileVO = new GmFileVO();
    GmFileUploadBean gmFileUploadBean = new GmFileUploadBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmFileVO> listGmFileVO = new ArrayList<GmFileVO>();

    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("VO", gmFileVO);
    listGmFileVO = gmFileUploadBean.fetchUploadedFileInfo(hmParams);
    gmFileVO = (GmFileVO) gmProdCatlResListVO.setPageProperties(listGmFileVO);
    gmFileVO = (gmFileVO == null) ? new GmFileVO() : gmFileVO;

    gmProdCatlResListVO.setReftype("4000410");// Files Device sync type
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmProdCatlResListVO.getReftype()));
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmProdCatlResListVO.getPagesize()));
    hmParams.put("LASTREC", gmFileVO.getFileid()); // FILEID

    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmProdCatlResListVO.setListGmFileVO(listGmFileVO);

    return gmProdCatlResListVO;
  }

  @Path("singlefileupload")
  @POST
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  public String uploadSingleFile(@FormDataParam("file") InputStream fileInputStream,
      @FormDataParam("file") FormDataContentDisposition contentDispositionHeader,
      @FormDataParam("token") String strToken, @FormDataParam("cmpid") String strCmpid,
      @FormDataParam("ordid") String strodrid, @FormDataParam("filename") String strfilenm)
      throws AppError, Exception {

    GmFileUploadBean gmFileUploadBean = new GmFileUploadBean();
    GmCommonUploadBean gmCommonUpldBean = new GmCommonUploadBean();

    String strCompanyLocale = GmCommonClass.getCompanyLocale(strCmpid);
    strCompanyLocale = strCompanyLocale.equals("") ? "US" : strCompanyLocale;
    log.debug("strCompanyLocale ***" + strCompanyLocale);
    // Filepath change according to the COmpany of the USER
    String filePath =
        GmCommonClass.getString("DOFILEPUSHLOC") + strCompanyLocale + "\\"
            + contentDispositionHeader.getFileName();
    // Thread.sleep(5000);

    log.debug("filePath*" + filePath);
    String fileStatus = "File Uploaded Successfully";
    HashMap hmExpParam = new HashMap();

    log.debug("the value inside strToken ***" + strToken);
    String strUserId = GmCommonClass.parseNull(gmUserVO.getUserid());
    // Validating Token
    try {

      validateToken(strToken);

      String strfirstName = GmCommonClass.parseNull(gmUserVO.getFname());
      String strLastName = GmCommonClass.parseNull(gmUserVO.getLname());
      String struserName = strfirstName.concat(strLastName);

      hmExpParam.put("REPEMAIL", strUserId);
      hmExpParam.put("FILENAME", filePath);
      hmExpParam.put("USERNAME", struserName);

      // save the file to the server

      File outFile = new File(filePath);
      if (!outFile.exists()) {
        new File(outFile.getParent()).mkdirs();
      }
      OutputStream outputStream = new FileOutputStream(outFile);

      int read = 0;
      byte[] bytes = new byte[1024];

      while ((read = fileInputStream.read(bytes)) != -1) {
        outputStream.write(bytes, 0, read);
      }
      outputStream.flush();
      outputStream.close();
      fileInputStream.close();
    } catch (Exception e) {
      log.debug("Coming inside exception part");
      hmExpParam.put("EXCEPTION", e.getMessage());
      gmFileUploadBean.sendFileUploadExceptionEmail(hmExpParam);
      throw new AppError("", "20642", 'E');
    }
    // Save Uploaded PDF infop Into T903_upload_file_list
    gmCommonUpldBean.saveUploadInfo(strodrid, "26230807", strfilenm, strUserId); // 26230807
    // DOPDF
    fileStatus = fileStatus + filePath;
    return fileStatus;
  }

  @Path("crmfileupload")
  @POST
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  public String uploadCRMFile(@FormDataParam("file") InputStream fileInputStream,
      @FormDataParam("file") FormDataContentDisposition contentDispositionHeader,
      @FormDataParam("token") String strToken, @FormDataParam("refgrp") String strRefGrp,
      @FormDataParam("refid") String strRefID) throws AppError, Exception {

    log.debug("the value insids fwjd");
    log.debug("strRefGrp>>>>> " + strRefGrp);
    GmFileUploadBean gmFileUploadBean = new GmFileUploadBean();
    // String filePath =
    // "C:\\Apache2.2\\htdocs\\files" + "\\" + strRefGrp + "\\" + strRefID + "\\"
    // + contentDispositionHeader.getFileName();

    String filePath =
        GmCommonClass.getString("CRMFILEPUSHLOC") + strRefGrp + "\\" + strRefID + "\\"
            + contentDispositionHeader.getFileName();
    String fileStatus = "File Uploaded Successfully";
    HashMap hmExpParam = new HashMap();

    log.debug("the value inside strToken ***" + strToken);
    // Validating Token
    try {
      validateToken(strToken);
      String strUserId = GmCommonClass.parseNull(gmUserVO.getUserid());
      String strfirstName = GmCommonClass.parseNull(gmUserVO.getFname());
      String strLastName = GmCommonClass.parseNull(gmUserVO.getLname());
      String struserName = strfirstName.concat(strLastName);

      hmExpParam.put("REPEMAIL", strUserId);
      hmExpParam.put("FILENAME", filePath);
      hmExpParam.put("USERNAME", struserName);

      // save the file to the server
      File outFile = new File(filePath);
      if (!outFile.exists()) {
        new File(outFile.getParent()).mkdirs();
      }
      OutputStream outputStream = new FileOutputStream(outFile);

      int read = 0;
      byte[] bytes = new byte[1024];

      while ((read = fileInputStream.read(bytes)) != -1) {
        outputStream.write(bytes, 0, read);
      }
      outputStream.flush();
      outputStream.close();
      fileInputStream.close();
      //
    } catch (Exception e) {
      // log.debug(e.getMessage());
      log.debug("Coming inside exception part");
      hmExpParam.put("EXCEPTION", e.getMessage());
      gmFileUploadBean.sendFileUploadExceptionEmail(hmExpParam);
      throw new AppError("", "20642", 'E');
    }
    fileStatus = fileStatus + filePath;
    return fileStatus;
  }
  
  /**
   * fetchJsonAllFilesInfo - This method will returns all file values while calling web service. First
   * call, It will returns all files which belongs to images Type-4000410, and which are published
   * to product catalog after that it will returns updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return
   * @throws AppError
   */
  @Compress
  @POST
  @Path("masterjson")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public String fetchJsonAllFilesInfo(String strInput) throws AppError {
	    
	    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();
	    parseInputAndValidateToken(strInput, gmProdCatlResListVO);  // validate token and parse JSON into VO
	    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
	    
	    HashMap hmValidate = new HashMap();
	    hmValidate.put("langid", "20607");
	    hmValidate.put("uuid", "20608");
	    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);  // validate device UUID and language is not empty

	    GmFileVO gmFileVO = new GmFileVO();
	    GmFileUploadBean gmFileUploadBean = new GmFileUploadBean();
	    GmProdCatBean gmProdCatBean = new GmProdCatBean();
	    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

	    HashMap hmParams = new HashMap();
	    List alResult = new ArrayList();
	    List<GmFileVO> listGmFileVO = new ArrayList<GmFileVO>();

	    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
	    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
	    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
	    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
	    hmParams.put("CMPID", GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid()));
	    
	    String strReturnJSON = gmFileUploadBean.fetchJsonUploadedFileInfo(hmParams);
	    
	     gmFileVO = (GmFileVO) gmProdCatlResListVO.setPageProperties(listGmFileVO);
	     gmFileVO = (gmFileVO == null) ? new GmFileVO() : gmFileVO;

	    gmProdCatlResListVO.setReftype("4000410");// Files Device sync type
	    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmProdCatlResListVO.getReftype()));
	    hmParams.put("STATUSID", "103120"); // Initiated status
	    hmParams.put("PAGESIZE", GmCommonClass.parseNull(gmProdCatlResListVO.getPagesize()));
	    hmParams.put("LASTREC", "SYNCJSON"); // FILEID

	    // To save details of device sync details and sync log.
	    gmProdCatBean.saveDeviceSyncDetail(hmParams);
	    gmProdCatlResListVO.setListGmFileVO(listGmFileVO);

	    return strReturnJSON;
  }

}
