package com.globus.webservice.prodmgmnt.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.custservice.beans.GmDOTagDtlBean;
import com.globus.valueobject.sales.GmDORecordTagIDVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.config.Compress;

/**
 * @author ppandiyan
 * 
 */
@Path("DOTag")
public class GmDOTagDetailResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchDOTagDtls - This method will be called from device. This is used to fetch the Tag Related
   * Information for the Respective Order ID.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @POST
  @Path("fetchDOTagDtls")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public String fetchTagReqDetails(String strInput) throws AppError {
	  log.debug("fetchDOTagDtls");
	GmDORecordTagIDVO gmDORecordTagIDVO = new GmDORecordTagIDVO();
    GmDOTagDtlBean gmDOTagDtlBean = new GmDOTagDtlBean();
    String strReturnJSON="";

    parseInputAndValidateToken(strInput, gmDORecordTagIDVO);
    String strOrderID = GmCommonClass.parseNull(gmDORecordTagIDVO.getOrderid());
    strReturnJSON = gmDOTagDtlBean.fetchDOTagDetails(strOrderID);
    log.debug("fetchDOTagDtls::"+strReturnJSON);
    return strReturnJSON;
  }


  
}
