package com.globus.webservice.prodmgmnt.resources;

import com.globus.common.beans.AppError; 
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.sales.GmSalesDashReqVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.bean.GmCRMPreceptorshipRptBean;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import java.io.IOException;
import java.util.HashMap;
import org.apache.log4j.Logger;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import com.globus.valueobject.common.GmDataStoreVO;

import com.globus.valueobject.prodmgmnt.GmCollateralEmailTrackVO;
import com.globus.prodmgmnt.beans.GmCollateralEmailTrackBean;

/**
 * @author mselvamani
 *
 */

@Path("emailtrack")
public class GmCollateralEmailTrackResource extends GmResource {

	/**
	 * Method to declare the log and used to debug the code
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * fetchCollateralActivitityChartDetails is used to fetch the total and
	 * delivered email Reports like By AD, By Field Sales
	 * 
	 * @param strInput
	 * @return
	 * @throws AppError, JsonParseException, JsonMappingException,
	 *                   IOException
	 */
	@POST
	@Path("fetchCollateralActivitityChartDetails")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String fetchCollateralActivitityChartDetails(String strInput)
			throws AppError, JsonParseException, JsonMappingException, IOException {
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		String strReturnJSON = "";
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmAccessFilter gmAccessFilter = new GmAccessFilter();
		ObjectMapper mapper = new ObjectMapper();
		HashMap hmInput = new HashMap();
		GmCollateralEmailTrackVO gmCollateralEmailTrackVO = mapper.readValue(strInput, GmCollateralEmailTrackVO.class);

		validateToken(gmCollateralEmailTrackVO.getToken());
		gmCollateralEmailTrackVO.setUserid(gmUserVO.getUserid());
		gmDataStoreVO.setCmpdfmt(gmCollateralEmailTrackVO.getCmpdfmt());
		GmCollateralEmailTrackBean gmCollateralEmailTrackBean = new GmCollateralEmailTrackBean(gmDataStoreVO);

		gmWSUtil.parseJsonToVO(strInput, gmCollateralEmailTrackVO);
		hmInput = gmWSUtil.getHashMapFromVO(gmCollateralEmailTrackVO);
		HashMap hmParam = GmCommonClass
				.parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, (new GmSalesDashReqVO())));

		/*
		 * When Associate Rep login. His user id passed as reporting repid.
		 */

		hmParam.put("RPT_REPID", gmUserVO.getUserid());
		hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
		hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));

		hmInput.put("HMPARAM", GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam)));
		hmInput = (HashMap) hmInput.get("HMPARAM");
		hmInput.put("APPLNDATEFMT", gmCollateralEmailTrackVO.getCmpdfmt());
		hmInput.put("STARTDATE", gmCollateralEmailTrackVO.getStartdate());
		hmInput.put("ENDDATE", gmCollateralEmailTrackVO.getEnddate());
		hmInput.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
		hmInput.put("USERID", gmUserVO.getUserid());
		strReturnJSON = gmCollateralEmailTrackBean.fetchCollateralActivitityChartDetails(hmInput);
		log.debug("strReturnJSON" + strReturnJSON);

		return strReturnJSON;

	}

	/**
	 * fetchCollateralOpenChartDetails is used to fetch the open and unique open
	 * email Reports like By AD, By Field Sales
	 * 
	 * @param strInput
	 * @return
	 * @throws AppError, JsonParseException, JsonMappingException,
	 *                   IOException
	 */
	@POST
	@Path("fetchCollateralOpenChartDetails")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String fetchCollateralOpenChartDetails(String strInput)
			throws AppError, JsonParseException, JsonMappingException, IOException {
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		String strReturnJSON = "";
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmAccessFilter gmAccessFilter = new GmAccessFilter();
		ObjectMapper mapper = new ObjectMapper();
		HashMap hmInput = new HashMap();
		GmCollateralEmailTrackVO gmCollateralEmailTrackVO = mapper.readValue(strInput, GmCollateralEmailTrackVO.class);

		validateToken(gmCollateralEmailTrackVO.getToken());
		gmCollateralEmailTrackVO.setUserid(gmUserVO.getUserid());
		gmDataStoreVO.setCmpdfmt(gmCollateralEmailTrackVO.getCmpdfmt());
		GmCollateralEmailTrackBean gmCollateralEmailTrackBean = new GmCollateralEmailTrackBean(gmDataStoreVO);

		gmWSUtil.parseJsonToVO(strInput, gmCollateralEmailTrackVO);
		hmInput = gmWSUtil.getHashMapFromVO(gmCollateralEmailTrackVO);
		HashMap hmParam = GmCommonClass
				.parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, (new GmSalesDashReqVO())));

		/*
		 * When Associate Rep login. His user id passed as reporting repid.
		 */
		hmParam.put("RPT_REPID", gmUserVO.getUserid());
		hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
		hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));

		hmInput.put("HMPARAM", GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam)));
		hmInput = (HashMap) hmInput.get("HMPARAM");
		hmInput.put("APPLNDATEFMT", gmCollateralEmailTrackVO.getCmpdfmt());
		hmInput.put("STARTDATE", gmCollateralEmailTrackVO.getStartdate());
		hmInput.put("ENDDATE", gmCollateralEmailTrackVO.getEnddate());
		hmInput.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
		hmInput.put("USERID", gmUserVO.getUserid());
		strReturnJSON = gmCollateralEmailTrackBean.fetchCollateralOpenChartDetails(hmInput);
		log.debug("strReturnJSON" + strReturnJSON);
		return strReturnJSON;

	}

	/**
	 * fetchCollateralClickChartDetails is used to fetch the open and unique clicks
	 * email Reports like By AD, By Field Sales
	 * 
	 * @param strInput
	 * @return
	 * @throws AppError, JsonParseException, JsonMappingException,
	 *                   IOException
	 */
	@POST
	@Path("fetchCollateralClickChartDetails")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String fetchCollateralClickChartDetails(String strInput)
			throws AppError, JsonParseException, JsonMappingException, IOException {
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		String strReturnJSON = "";
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmAccessFilter gmAccessFilter = new GmAccessFilter();
		ObjectMapper mapper = new ObjectMapper();
		HashMap hmInput = new HashMap();
		GmCollateralEmailTrackVO gmCollateralEmailTrackVO = mapper.readValue(strInput, GmCollateralEmailTrackVO.class);

		validateToken(gmCollateralEmailTrackVO.getToken());
		gmCollateralEmailTrackVO.setUserid(gmUserVO.getUserid());
		gmDataStoreVO.setCmpdfmt(gmCollateralEmailTrackVO.getCmpdfmt());
		GmCollateralEmailTrackBean gmCollateralEmailTrackBean = new GmCollateralEmailTrackBean(gmDataStoreVO);

		gmWSUtil.parseJsonToVO(strInput, gmCollateralEmailTrackVO);
		hmInput = gmWSUtil.getHashMapFromVO(gmCollateralEmailTrackVO);
		HashMap hmParam = GmCommonClass
				.parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, (new GmSalesDashReqVO())));

		/*
		 * When Associate Rep login. His user id passed as reporting repid.
		 */

		hmParam.put("RPT_REPID", gmUserVO.getUserid());
		hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
		hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));

		hmInput.put("HMPARAM", GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam)));
		hmInput = (HashMap) hmInput.get("HMPARAM");
		hmInput.put("APPLNDATEFMT", gmCollateralEmailTrackVO.getCmpdfmt());
		hmInput.put("STARTDATE", gmCollateralEmailTrackVO.getStartdate());
		hmInput.put("ENDDATE", gmCollateralEmailTrackVO.getEnddate());
		hmInput.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
		hmInput.put("USERID", gmUserVO.getUserid());
		strReturnJSON = gmCollateralEmailTrackBean.fetchCollateralClickChartDetails(hmInput);
		log.debug("strReturnJSON" + strReturnJSON);
		return strReturnJSON;

	}
	
	/**
	 * fetchTotalLinkSends is used to fetch total link sends
	 *  like By AD, By Field Sales
	 * 
	 * @param strInput
	 * @return
	 * @throws AppError, JsonParseException, JsonMappingException,
	 *                   IOException
	 */
	@POST
	@Path("fetchTotalLinkSends")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String fetchTotalLinkSends(String strInput)
			throws AppError, JsonParseException, JsonMappingException, IOException {
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		String strReturnJSON = "";
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmAccessFilter gmAccessFilter = new GmAccessFilter();
		ObjectMapper mapper = new ObjectMapper();
		HashMap hmInput = new HashMap();
		GmCollateralEmailTrackVO gmCollateralEmailTrackVO = mapper.readValue(strInput, GmCollateralEmailTrackVO.class);

		validateToken(gmCollateralEmailTrackVO.getToken());
		gmCollateralEmailTrackVO.setUserid(gmUserVO.getUserid());
		gmDataStoreVO.setCmpdfmt(gmCollateralEmailTrackVO.getCmpdfmt());
		GmCollateralEmailTrackBean gmCollateralEmailTrackBean = new GmCollateralEmailTrackBean(gmDataStoreVO);

		gmWSUtil.parseJsonToVO(strInput, gmCollateralEmailTrackVO);
		hmInput = gmWSUtil.getHashMapFromVO(gmCollateralEmailTrackVO);
		HashMap hmParam = GmCommonClass
				.parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, (new GmSalesDashReqVO())));

		/*
		 * When Associate Rep login. His user id passed as reporting repid.
		 */

		hmParam.put("RPT_REPID", gmUserVO.getUserid());
		hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
		hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));

		hmInput.put("HMPARAM", GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam)));
		hmInput = (HashMap) hmInput.get("HMPARAM");
		hmInput.put("APPLNDATEFMT", gmCollateralEmailTrackVO.getCmpdfmt());
		hmInput.put("STARTDATE", gmCollateralEmailTrackVO.getStartdate());
		hmInput.put("ENDDATE", gmCollateralEmailTrackVO.getEnddate());
		hmInput.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
		hmInput.put("USERID", gmUserVO.getUserid());
		strReturnJSON = gmCollateralEmailTrackBean.fetchTotalLinkSends(hmInput);
		log.debug("strReturnJSON" + strReturnJSON);
		return strReturnJSON;

	}

	/**
	 * fetchCollateralEmailTrackDetail - This method used to show the Email Track
	 * Detailed View Report by AD, by Field Sales
	 * 
	 * @param strInput
	 * @return
	 * @throws AppError, JsonParseException, JsonMappingException,
	 *                   IOException
	 */
	@POST
	@Path("emailTrackDetailReport")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String fetchCollateralEmailTrackDetail(String strInput)
			throws AppError, JsonParseException, JsonMappingException, IOException {
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		GmAccessFilter gmAccessFilter = new GmAccessFilter();
		GmWSUtil gmWSUtil = new GmWSUtil();
		ObjectMapper mapper = new ObjectMapper();
		HashMap hmInput = new HashMap();
		String strReturnJSON = "";
		GmCollateralEmailTrackVO gmCollateralEmailTrackVO = mapper.readValue(strInput, GmCollateralEmailTrackVO.class);// to
																														// read
																														// the
																														// input
		validateToken(gmCollateralEmailTrackVO.getToken());

		gmCollateralEmailTrackVO.setUserid(gmUserVO.getUserid());
		gmDataStoreVO.setCmpdfmt(gmCollateralEmailTrackVO.getCmpdfmt());
		GmCollateralEmailTrackBean gmCollateralEmailTrackBean = new GmCollateralEmailTrackBean(gmDataStoreVO);
		// For Override the Access Level of Associate Sales REP to Sales Rep (Associate
		// Sales REP
		// checks with account id)
		HashMap hmParam = GmCommonClass
				.parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, (new GmSalesDashReqVO())));
		/*
		 * When Associate Rep login. His user id passed as reporting repid.
		 */
		hmParam.put("RPT_REPID", gmUserVO.getUserid());
		hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
		hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));

		hmInput.put("USERDEPTID", gmUserVO.getDeptid());
		hmInput.put("HMPARAM", GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam)));
		hmInput = (HashMap) hmInput.get("HMPARAM");
		hmInput.put("STARTDATE", gmCollateralEmailTrackVO.getStartdate());
		hmInput.put("ENDDATE", gmCollateralEmailTrackVO.getEnddate());
		hmInput.put("EMAILID", gmCollateralEmailTrackVO.getEmailid());
		hmInput.put("EVENTTYPE", gmCollateralEmailTrackVO.getEventtype());
		hmInput.put("CONTENTITLE", gmCollateralEmailTrackVO.getContenttitle());
		hmInput.put("USERID", gmUserVO.getUserid());
		hmInput.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
		strReturnJSON = gmCollateralEmailTrackBean.fetchCollateralEmailTrackDetail(hmInput);
		log.debug("fetchCollateralEmailTrackDetail " + gmCollateralEmailTrackVO);
		return strReturnJSON;
	}

	/**
	 * fetchEventTimeStamp - This method used to show the TimeStamp in Popup when
	 * Opens/Clicks count clicked in Detailed Report
	 * 
	 * @param strInput
	 * @return
	 * @throws AppError, JsonParseException, JsonMappingException,
	 *                   IOException
	 */
	@POST
	@Path("fetchEventTimeStamp")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String fetchEventTimeStamp(String strInput)
			throws AppError, JsonParseException, JsonMappingException, IOException {
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		GmAccessFilter gmAccessFilter = new GmAccessFilter();
		GmWSUtil gmWSUtil = new GmWSUtil();
		ObjectMapper mapper = new ObjectMapper();
		HashMap hmInput = new HashMap();
		String strReturnJSON = "";
		GmCollateralEmailTrackVO gmCollateralEmailTrackVO = mapper.readValue(strInput, GmCollateralEmailTrackVO.class);// to
																														// read
																														// the
																														// input
		validateToken(gmCollateralEmailTrackVO.getToken());

		gmCollateralEmailTrackVO.setUserid(gmUserVO.getUserid());
		gmDataStoreVO.setCmpdfmt(gmCollateralEmailTrackVO.getCmpdfmt());
		GmCollateralEmailTrackBean gmCollateralEmailTrackBean = new GmCollateralEmailTrackBean(gmDataStoreVO);

		hmInput.put("EVENTYPE", gmCollateralEmailTrackVO.getEventtype());
		hmInput.put("EMAILDETAILSID", gmCollateralEmailTrackVO.getEmaildetailsid());
		hmInput.put("USERID", gmUserVO.getUserid());
		strReturnJSON = gmCollateralEmailTrackBean.fetchEventTimeStamp(hmInput);
		log.debug("fetchCollateralEmailTrackDetail " + gmCollateralEmailTrackVO);
		return strReturnJSON;
	}

	/**
	 * fetchCollateralFrequentContacts is used to fetch the frequent contacts for
	 * AD, Field Sales, Distributor
	 * 
	 * @param strInput
	 * @return
	 * @throws AppError, JsonParseException, JsonMappingException,
	 *                   IOException
	 */
	@POST
	@Path("fetchCollateralFrequentContacts")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String fetchCollateralFrequentContacts(String strInput)
			throws AppError, JsonParseException, JsonMappingException, IOException {
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		GmAccessFilter gmAccessFilter = new GmAccessFilter();
		GmWSUtil gmWSUtil = new GmWSUtil();
		ObjectMapper mapper = new ObjectMapper();
		HashMap hmInput = new HashMap();
		String strReturnJSON = "";
		GmCollateralEmailTrackVO gmCollateralEmailTrackVO = mapper.readValue(strInput, GmCollateralEmailTrackVO.class);// to
																														// read
																														// the
																														// input
		validateToken(gmCollateralEmailTrackVO.getToken());

		gmCollateralEmailTrackVO.setUserid(gmUserVO.getUserid());
		gmDataStoreVO.setCmpdfmt(gmCollateralEmailTrackVO.getCmpdfmt());
		GmCollateralEmailTrackBean gmCollateralEmailTrackBean = new GmCollateralEmailTrackBean(gmDataStoreVO);
		// For Override the Access Level of Associate Sales REP to Sales Rep (Associate
		// Sales REP
		// checks with account id)
		HashMap hmParam = GmCommonClass
				.parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, (new GmSalesDashReqVO())));
		/*
		 * When Associate Rep login. His user id passed as reporting repid.
		 */
		hmParam.put("RPT_REPID", gmUserVO.getUserid());
		hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
		hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));

		hmInput.put("USERDEPTID", gmUserVO.getDeptid());
		hmInput.put("HMPARAM", GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam)));
		hmInput = (HashMap) hmInput.get("HMPARAM");
		hmInput.put("STARTDATE", gmCollateralEmailTrackVO.getStartdate());
		hmInput.put("ENDDATE", gmCollateralEmailTrackVO.getEnddate());
		hmInput.put("USERID", gmUserVO.getUserid());
		hmInput.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
		strReturnJSON = gmCollateralEmailTrackBean.fetchCollateralFrequentContacts(hmInput);
		log.debug("strReturnJSON" + strReturnJSON);
		return strReturnJSON;

	}

	/**
	 * fetchCollateralFrequentDoc - This method used to show Frequently used
	 * Collateral for Summary Reports
	 * 
	 * @param strInput
	 * @return
	 * @throws AppError, JsonParseException, JsonMappingException,
	 *                   IOException
	 */
	@POST
	@Path("fetchCollateralFrequentDoc")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String fetchCollateralFrequentDoc(String strInput)
			throws AppError, JsonParseException, JsonMappingException, IOException {
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		GmAccessFilter gmAccessFilter = new GmAccessFilter();
		GmWSUtil gmWSUtil = new GmWSUtil();
		ObjectMapper mapper = new ObjectMapper();
		HashMap hmInput = new HashMap();
		String strReturnJSON = "";
		GmCollateralEmailTrackVO gmCollateralEmailTrackVO = mapper.readValue(strInput, GmCollateralEmailTrackVO.class);// to
																														// read
																														// the
																														// input
		validateToken(gmCollateralEmailTrackVO.getToken());

		gmCollateralEmailTrackVO.setUserid(gmUserVO.getUserid());
		gmDataStoreVO.setCmpdfmt(gmCollateralEmailTrackVO.getCmpdfmt());
		GmCollateralEmailTrackBean gmCollateralEmailTrackBean = new GmCollateralEmailTrackBean(gmDataStoreVO);
		// For Override the Access Level of Associate Sales REP to Sales Rep (Associate
		// Sales REP
		// checks with account id)
		HashMap hmParam = GmCommonClass
				.parseNullHashMap(gmProdCatUtil.populateSalesParam(gmUserVO, (new GmSalesDashReqVO())));
		/*
		 * When Associate Rep login. His user id passed as reporting repid.
		 */
		hmParam.put("RPT_REPID", gmUserVO.getUserid());
		hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
		hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));

		hmInput.put("USERDEPTID", gmUserVO.getDeptid());
		hmInput.put("HMPARAM", GmCommonClass.parseNullHashMap(gmAccessFilter.getSalesAccessFilter(hmParam)));
		hmInput = (HashMap) hmInput.get("HMPARAM");
		hmInput.put("STARTDATE", gmCollateralEmailTrackVO.getStartdate());
		hmInput.put("ENDDATE", gmCollateralEmailTrackVO.getEnddate());
		hmInput.put("USERID", gmUserVO.getUserid());
		hmInput.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
		strReturnJSON = gmCollateralEmailTrackBean.fetchCollateralFrequentDoc(hmInput);
		log.debug("fetchCollateralFrequentDoc " + gmCollateralEmailTrackVO);
		return strReturnJSON;
	}

	/**
	 * fetchCollateralGlobalFrequentDoc - This method used to show most shared content for All Globus Reps
	 * @param strInput
	 * @return
	 * @throws AppError, JsonParseException, JsonMappingException, IOException
	 */
	@POST
	@Path("fetchCollateralGlobalFrequentDoc")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public String fetchCollateralGlobalFrequentDoc(String strInput)
			throws AppError, JsonParseException, JsonMappingException, IOException {
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		GmWSUtil gmWSUtil = new GmWSUtil();
		ObjectMapper mapper = new ObjectMapper();
		HashMap hmInput = new HashMap();
		String strReturnJSON = "";
		GmCollateralEmailTrackVO gmCollateralEmailTrackVO = mapper.readValue(strInput, GmCollateralEmailTrackVO.class);// to
																														// read
																														// the
																														// input
		validateToken(gmCollateralEmailTrackVO.getToken());

		gmCollateralEmailTrackVO.setUserid(gmUserVO.getUserid());
		gmDataStoreVO.setCmpdfmt(gmCollateralEmailTrackVO.getCmpdfmt());
		gmDataStoreVO.setCmpid(gmCollateralEmailTrackVO.getCmpid());
		GmCollateralEmailTrackBean gmCollateralEmailTrackBean = new GmCollateralEmailTrackBean(gmDataStoreVO);

		hmInput.put("STARTDATE", gmCollateralEmailTrackVO.getStartdate());
		hmInput.put("ENDDATE", gmCollateralEmailTrackVO.getEnddate());
		hmInput.put("USERID", gmUserVO.getUserid());
		strReturnJSON = gmCollateralEmailTrackBean.fetchCollateralGlobalFrequentDoc(hmInput);
		log.debug("fetchCollateralGlobalFrequentDoc " + gmCollateralEmailTrackVO);
		return strReturnJSON;
	}

}
