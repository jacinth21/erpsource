package com.globus.webservice.prodmgmnt.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.logon.beans.GmLogonBean;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.valueobject.common.GmDeviceDetailVO;
import com.globus.valueobject.common.GmProdCatlReqVO;
import com.globus.valueobject.common.GmProdCatlResListVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.prodmgmnt.GmDataUpdateVO;
import com.globus.webservice.common.resources.GmResource;

/**
 * @author elango
 * 
 */
@Path("devicesync")
public class GmProdCatResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * saveDeviceSyncDetail - This method will save status of every sync, while calling web service.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @POST
  @Path("status")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlReqVO saveDeviceSyncDetail(String strInput) throws AppError {
    log.debug("=======update  Status======");

    GmProdCatlReqVO gmProdCatlReqVO = new GmProdCatlReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlReqVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    HashMap hmReturn = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlReqVO, hmValidate);
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlReqVO.getToken());
    // op changess start
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmProdCatlReqVO);
    // op changess end
    HashMap hmParams = new HashMap();
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlReqVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlReqVO.getLangid()));
    hmParams.put("REFTYPE", GmCommonClass.parseNull(gmProdCatlReqVO.getReftype()));
    hmParams.put("STATUSID", GmCommonClass.parseNull(gmProdCatlReqVO.getStatusid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlReqVO.getUuid()));
    hmParams.put("TOTALSIZE", GmCommonClass.parseNull(gmProdCatlReqVO.getTotalsize()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("COMPID", GmCommonClass.parseNull(gmProdCatlReqVO.getCmpid()));  // PMT-37595 - need company id
    hmParams.put("LASTREC", GmCommonClass.parseNull(gmProdCatlReqVO.getLastrec()));
    // To save details of device sync details and sync log.
    hmReturn = gmProdCatBean.saveDeviceSyncDetail(hmParams);
    gmProdCatlReqVO.setStatusid((String) hmReturn.get("STATUS"));
    gmProdCatlReqVO.setSvrcnt((String) hmReturn.get("SERVERCNT"));
    return gmProdCatlReqVO;
  }

  /**
   * fetchAllDataUpdates - This method will fetch All Data Updates to be sync with the device, while
   * calling web service.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */

  @POST
  @Path("updates")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlReqVO fetchAllDataUpdates(String strInput) throws AppError {
    log.debug("=======fetchAllDataUpdates======");

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

    GmWSUtil gmWSUtil = new GmWSUtil();

    GmDataUpdateVO gmDataUpdateVO = new GmDataUpdateVO();
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmProdCatlResListVO);
    GmLogonBean gmLogonBean = new GmLogonBean(gmProdCatlResListVO);

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    String strPartyID = "";
    List<GmDataUpdateVO> listGmDataUpdateVO = new ArrayList<GmDataUpdateVO>();

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));

    // Passing partyid as input to get the updates based on the party
    strPartyID =
        GmCommonClass.parseNull(gmLogonBean.fetchPartyId(GmCommonClass.parseNull(gmUserVO
            .getUserid())));
    strPartyID =
        strPartyID.equals("") ? GmCommonClass.parseNull(gmUserVO.getPartyid()) : strPartyID;
    hmParams.put("PARTYID", strPartyID);
    alResult = gmProdCatBean.fetchDataSyncDtls(hmParams);
    log.debug("=======alResult======>" + alResult);
    // Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo
    // attributes
    listGmDataUpdateVO =
        gmWSUtil.getVOListFromHashMapList(alResult, gmDataUpdateVO,
            gmDataUpdateVO.getDataUpdateProperties());
    // Set the page header detail from first hashmap of list.

    gmProdCatlResListVO.populatePageProperties(alResult);
    gmProdCatlResListVO.setListGmDataUpdateVO(listGmDataUpdateVO);
    log.debug("=======gmProdCatlResListVO======>" + gmProdCatlResListVO);
    return gmProdCatlResListVO;
  }

  /**
   * fetchAllDataUpdatesCount - This method will fetch All Data Updates Count to be sync with the
   * device, while calling web service.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @POST
  @Path("updatescnt")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlReqVO fetchAllDataUpdatesCount(String strInput) throws AppError {
    log.debug("=======fetchAllDataUpdates======");

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

    GmWSUtil gmWSUtil = new GmWSUtil();

    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmProdCatlResListVO);
    GmLogonBean gmLogonBean = new GmLogonBean(gmProdCatlResListVO);
    GmDeviceDetailVO gmDeviceDetailVO = new GmDeviceDetailVO();

    HashMap hmParams = new HashMap();
    HashMap hmReturn = new HashMap();
    List alResult = new ArrayList();
    List alAutoUpdates = new ArrayList();
    String strPartyID = "";
    List<GmDataUpdateVO> listGmDataUpdateVO = new ArrayList<GmDataUpdateVO>();
    List<GmDeviceDetailVO> listAutoUpdates = new ArrayList<GmDeviceDetailVO>();

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));

    // Passing partyid as input to get the updates based on the party
    strPartyID =
        GmCommonClass.parseNull(gmLogonBean.fetchPartyId(GmCommonClass.parseNull(gmUserVO
            .getUserid())));
    strPartyID =
        strPartyID.equals("") ? GmCommonClass.parseNull(gmUserVO.getPartyid()) : strPartyID;
    hmParams.put("PARTYID", strPartyID);
    alResult = gmProdCatBean.fetchDataSyncDtls(hmParams);

    // Set the page header detail from first hashmap of list.
    gmProdCatlResListVO.populatePageProperties(alResult);

    // Get Auto update list [ex. case schedule]
    alAutoUpdates = gmProdCatBean.fetchAutoUpdate(hmParams);
    if (alAutoUpdates.size() > 0) {
      listAutoUpdates = gmWSUtil.getVOListFromHashMapList(alAutoUpdates, gmDeviceDetailVO);
      gmProdCatlResListVO.setListAutoUpdates(listAutoUpdates);
    }

    return gmProdCatlResListVO;
  }

  /*
   * saveDeviceSyncClearDtl is used to void the sync data based on the token and UUID passed.
   */
  @POST
  @Path("clear")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlReqVO saveDeviceSyncClearDtl(String strInput) throws AppError {

    GmProdCatlReqVO gmProdCatlReqVO = new GmProdCatlReqVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlReqVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("uuid", "20608");
    // validate device UUID is not empty
    gmProdCatUtil.validateInput(gmProdCatlReqVO, hmValidate);

    GmProdCatBean gmProdCatBean = new GmProdCatBean();
    HashMap hmParams = new HashMap();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlReqVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlReqVO.getToken()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlReqVO.getUuid()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("SYNCTYPE", GmCommonClass.parseNull(gmProdCatlReqVO.getReftype()));


    // To clear details of device sync details and sync log.
    gmProdCatBean.saveDeviceClearSyncDetail(hmParams);

    return gmProdCatlReqVO;

  }

}
