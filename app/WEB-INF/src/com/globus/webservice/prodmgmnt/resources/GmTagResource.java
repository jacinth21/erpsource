package com.globus.webservice.prodmgmnt.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.accounts.physicalaudit.beans.GmTagBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.beans.GmSystemServiceBean;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.valueobject.common.GmProdCatlResListVO;
import com.globus.valueobject.common.GmTagDetailVO;
import com.globus.valueobject.common.GmTagInfoVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.config.Compress;

/**
 * @author Manikandan Rajasekaran
 * 
 */
@Path("tag")
public class GmTagResource extends GmResource {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchTagReqDetails - This method will be called from device when we scan a Tag ID or when click
   * on Load Button in Device After Entering Tag ID. This is used to fetch the Tag Related
   * Information for the Respective Tag ID and It will return the Part Details Scetion for the
   * Respective Consignment ID.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @POST
  @Path("fetchTagReq")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmTagInfoVO fetchTagReqDetails(String strInput) throws AppError {

    GmTagInfoVO gmTagInfoVO = new GmTagInfoVO();
    GmSystemServiceBean gmSystemServiceBean = new GmSystemServiceBean();
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();

    parseInputAndValidateToken(strInput, gmTagInfoVO);

    hmValidate.put("countryid", "20659");
    hmValidate.put("tagid", "20660");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmTagInfoVO, hmValidate);
    gmSystemServiceBean.populateTagDetailsVO(gmTagInfoVO);

    return gmTagInfoVO;
  }

  /**
   * fetchTagInfo - This method will be called from device tag sync
   * Save the Tag Details in Device Storage
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  
  @Compress
  @POST
  @Path("fetchTagInfo")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public String fetchTagDetailsSync(String strInput) throws AppError {
    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    // validate device UUID and language is not empty
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("uuid", "20608");
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmTagBean gmTagBean = new GmTagBean(gmProdCatlResListVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean();
    GmTagDetailVO gmTagInfoVO = new GmTagDetailVO();
    HashMap hmParams = new HashMap();
    List<GmTagDetailVO> listGmTagInfoVO = new ArrayList<GmTagDetailVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());
    List alResult = new ArrayList();
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", "103097"); // For CodeLookup language should be English only.
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
    hmParams.put("VO", gmTagInfoVO);
    String strReturnJSON = gmTagBean.fetchTagDetailsSync(hmParams);
    log.debug("strReturnJSON ?? "+strReturnJSON);
    gmTagInfoVO = (GmTagDetailVO) gmProdCatlResListVO.setPageProperties(listGmTagInfoVO);
    gmTagInfoVO = (gmTagInfoVO == null) ? new GmTagDetailVO() : gmTagInfoVO;

    gmProdCatlResListVO.setReftype("4001234");// Tag sync type

    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", "4001234");
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);
    gmProdCatlResListVO.setListGmTagInfoVO(listGmTagInfoVO);

    return strReturnJSON;
  }
  
}
