package com.globus.webservice.prodmgmnt.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.accounts.physicalaudit.beans.GmTagBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.beans.GmPartNumberServiceBean;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.valueobject.common.GmProdCatlResListVO;
import com.globus.valueobject.common.GmTagDetailVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.prodmgmnt.GmPartAttributeVO;
import com.globus.valueobject.prodmgmnt.GmPartNumberVO;
import com.globus.valueobject.prodmgmnt.GmPartRFSVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.config.Compress;


@Path("parts")
public class GmPartNumberResource extends GmResource {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchPartNumbers - This method will returns values while calling web service. First call, It
   * will returns all parts which are published to product catalog after that it will returns
   * updated parts only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @Compress
  @POST
  @Path("master")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlResListVO fetchPartNumbers(String strInput) throws AppError {
    log.debug("=======fetchPartNumbers======");

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);
    GmWSUtil gmWSUtil = new GmWSUtil();

    GmPartNumberVO gmPartNumberVO = new GmPartNumberVO();

    // op changess start
    GmPartNumberServiceBean gmPartNumberServiceBean =
        new GmPartNumberServiceBean(gmProdCatlResListVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmProdCatlResListVO);
    // op changess end
    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmPartNumberVO> listGmPartNumberVO = new ArrayList<GmPartNumberVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
    hmParams.put("VO", gmPartNumberVO);
    listGmPartNumberVO = gmPartNumberServiceBean.fetchPartNumbers(hmParams);
    gmPartNumberVO = (GmPartNumberVO) gmProdCatlResListVO.setPageProperties(listGmPartNumberVO);
    gmPartNumberVO = (gmPartNumberVO == null) ? new GmPartNumberVO() : gmPartNumberVO;

    gmProdCatlResListVO.setReftype("4000411");// Part Number sync type

    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", gmProdCatlResListVO.getReftype());
    hmParams.put("PAGESIZE", gmProdCatlResListVO.getPagesize());
    hmParams.put("LASTREC", gmPartNumberVO.getPartnum()); // PARTNUM

    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmProdCatlResListVO.setListGmPartNumberVO(listGmPartNumberVO);

    return gmProdCatlResListVO;
  }
  
  
 
 // PMT#40762-part-availability-in-ipad
  @Compress
  @POST
  @Path("masterjson")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public String fetchJsonPartNumbers(String strInput) throws AppError {
    log.debug("=======fetchPartNumbersNew======");

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);
    GmWSUtil gmWSUtil = new GmWSUtil();

    GmPartNumberVO gmPartNumberVO = new GmPartNumberVO();

    // op changess start
    GmPartNumberServiceBean gmPartNumberServiceBean =
        new GmPartNumberServiceBean(gmProdCatlResListVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmProdCatlResListVO);
    // op changess end
    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmPartNumberVO> listGmPartNumberVO = new ArrayList<GmPartNumberVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());
    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
    hmParams.put("COMPID", GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid()));
    hmParams.put("VO", gmPartNumberVO);
    
    String strReturnJSON = gmPartNumberServiceBean.fetchJsonPartNumbers(hmParams); 
    log.debug("strReturnJSON Part?? "+strReturnJSON.length());
    
   // listGmPartNumberVO = gmPartNumberServiceBean.fetchPartNumbers(hmParams);
    gmPartNumberVO = (GmPartNumberVO) gmProdCatlResListVO.setPageProperties(listGmPartNumberVO);
    gmPartNumberVO = (gmPartNumberVO == null) ? new GmPartNumberVO() : gmPartNumberVO;

    gmProdCatlResListVO.setReftype("4000411");// Part Number sync type

    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", gmProdCatlResListVO.getReftype());
    hmParams.put("PAGESIZE", gmProdCatlResListVO.getPagesize());
    hmParams.put("LASTREC", "SYNCJSON"); // PARTNUM

    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmProdCatlResListVO.setListGmPartNumberVO(listGmPartNumberVO);

    return strReturnJSON;
  }
  
  
  @Compress
  @POST
  @Path("partattr")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlResListVO fetchPartAttributes(String strInput) throws AppError {
    log.debug("=======fetchPartNumbers attr======");

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();
    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmPartAttributeVO gmPartAttributeVO = new GmPartAttributeVO();
    // op changess start
    GmPartNumberServiceBean gmPartNumberServiceBean =
        new GmPartNumberServiceBean(gmProdCatlResListVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmProdCatlResListVO);
    // op changess end
    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmPartAttributeVO> listGmPartAttributeVO = new ArrayList<GmPartAttributeVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
    hmParams.put("COUNTRYID", GmCommonClass.parseNull(gmProdCatlResListVO.getCountryid()));
    hmParams.put("VO", gmPartAttributeVO);

    listGmPartAttributeVO = gmPartNumberServiceBean.fetchPartAttributes(hmParams);
    gmPartAttributeVO =
        (GmPartAttributeVO) gmProdCatlResListVO.setPageProperties(listGmPartAttributeVO);
    gmPartAttributeVO = (gmPartAttributeVO == null) ? new GmPartAttributeVO() : gmPartAttributeVO;

    gmProdCatlResListVO.setReftype("4000827");// Part Number sync type

    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", gmProdCatlResListVO.getReftype());
    hmParams.put("PAGESIZE", gmProdCatlResListVO.getPagesize());
    hmParams.put("LASTREC", gmPartAttributeVO.getPartattrid()); // PARTATTRID
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmProdCatlResListVO.setListGmPartAttributeVO(listGmPartAttributeVO);

    return gmProdCatlResListVO;
  }
  
  @Compress
  @POST
  @Path("partattrjson")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public String fetchPartJSON(String strInput) throws AppError {
	  log.debug("=======fetchPartNumbers attr======");

	    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();
	    // validate token and parse JSON into VO
	    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
	    // check for company id -Mobile App
	    String strCompanyID = GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid());
	    if (strCompanyID.equals("")) {
	      throw new AppError("Please update the App", "", 'E');
	    }
	    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
	    HashMap hmValidate = new HashMap();
	    hmValidate.put("langid", "20607");
	    hmValidate.put("uuid", "20608");
	    // validate device UUID and language is not empty
	    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);
	    GmWSUtil gmWSUtil = new GmWSUtil();
	    GmPartAttributeVO gmPartAttributeVO = new GmPartAttributeVO();
	    // op changess start
	    GmPartNumberServiceBean gmPartNumberServiceBean =
	        new GmPartNumberServiceBean(gmProdCatlResListVO);
	    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmProdCatlResListVO);
	    // op changess end
	    HashMap hmParams = new HashMap();
	    List alResult = new ArrayList();
	    List<GmPartAttributeVO> listGmPartAttributeVO = new ArrayList<GmPartAttributeVO>();
	    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

	    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
	    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
	    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
	    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
	    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
	    hmParams.put("COMPID", GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid()));
	    hmParams.put("VO", gmPartAttributeVO);

	    String strReturnJSON = gmPartNumberServiceBean.fetchJsonPartAttributes(hmParams);
	    log.debug("strReturnJSON PartAttr?? "+strReturnJSON.length());
	    gmPartAttributeVO =
	        (GmPartAttributeVO) gmProdCatlResListVO.setPageProperties(listGmPartAttributeVO);
	    gmPartAttributeVO = (gmPartAttributeVO == null) ? new GmPartAttributeVO() : gmPartAttributeVO;

	    gmProdCatlResListVO.setReftype("4000827");// Part Number sync type

	    hmParams.put("STATUSID", "103120"); // Initiated status
	    hmParams.put("REFTYPE", gmProdCatlResListVO.getReftype());
	    hmParams.put("PAGESIZE", gmProdCatlResListVO.getPagesize());
	    hmParams.put("LASTREC", "SYNCJSON"); // PARTATTRID
	    // To save details of device sync details and sync log.
	    gmProdCatBean.saveDeviceSyncDetail(hmParams);

	    gmProdCatlResListVO.setListGmPartAttributeVO(listGmPartAttributeVO);

    return strReturnJSON;
  }


  @Compress
  @POST
  @Path("partRFS")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlResListVO fetchPartRFS(String strInput) throws AppError {
    log.debug("=======fetchPartRFS======");

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    // check for company id -Mobile App
    String strCompanyID = GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid());
    if (strCompanyID.equals("")) {
      throw new AppError("Please update the App", "", 'E');
    }
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmPartRFSVO gmPartRFSVO = new GmPartRFSVO();
    GmPartNumberServiceBean gmPartNumberServiceBean =
        new GmPartNumberServiceBean(gmProdCatlResListVO);
    GmProdCatBean gmProdCatBean = new GmProdCatBean(gmProdCatlResListVO);
    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmPartRFSVO> listGmPartRFSVO = new ArrayList<GmPartRFSVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
    hmParams.put("COUNTRYID", GmCommonClass.parseNull(gmProdCatlResListVO.getCountryid()));
    hmParams.put("VO", gmPartRFSVO);
    listGmPartRFSVO = gmPartNumberServiceBean.fetchPartRFSMapping(hmParams);
    gmPartRFSVO = (GmPartRFSVO) gmProdCatlResListVO.setPageProperties(listGmPartRFSVO);
    gmPartRFSVO = (gmPartRFSVO == null) ? new GmPartRFSVO() : gmPartRFSVO;
    gmProdCatlResListVO.setReftype("26230803");// 26230803 -Part Number RFS sync type

    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("REFTYPE", gmProdCatlResListVO.getReftype());
    hmParams.put("PAGESIZE", gmProdCatlResListVO.getPagesize());
    hmParams.put("LASTREC", gmPartRFSVO.getRfsmapid()); // RFSMAPID
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmProdCatlResListVO.setListGmRFSpartVO(listGmPartRFSVO);

    return gmProdCatlResListVO;
  }
}
