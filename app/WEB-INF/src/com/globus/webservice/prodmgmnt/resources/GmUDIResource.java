package com.globus.webservice.prodmgmnt.resources;


import java.util.ArrayList;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.beans.GmUDIDataBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.operations.GmRuleVO;
import com.globus.valueobject.prodmgmnt.GmUDIDataVO;
import com.globus.webservice.common.resources.GmResource;

/**
 * @author Yoga
 *
 */
@Path("udiinfo")
public class GmUDIResource extends GmResource {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	 //Resource to get UDI data for given mrf
	@Path("fetchUDIData")
 	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	
	public GmUDIDataVO fetchUDIData(String str_input) throws AppError {

			GmUDIDataVO gmUDIDataVO = new GmUDIDataVO();
			GmUDIDataBean gmUDIDataBean = new GmUDIDataBean();
			GmWSUtil gmWSUtil = new GmWSUtil();

			//validate token and parse JSON into VO
			parseInputAndValidateToken(str_input, gmUDIDataVO);
			//Assigning all the request params into Response VO

			GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmUDIDataVO.getToken());
			
			gmUDIDataVO = gmUDIDataBean.populateInputVO(gmUDIDataVO.getMrf());
	  		return gmUDIDataVO ;
  		
		}
 	
 	
}