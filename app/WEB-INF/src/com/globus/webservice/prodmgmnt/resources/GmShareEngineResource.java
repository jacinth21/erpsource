package com.globus.webservice.prodmgmnt.resources;

import java.io.IOException;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.crm.bean.GmCRMActivityBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.beans.GmShareEngineRptBean;
import com.globus.prodmgmnt.beans.GmShareEngineTransBean;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.prodmgmnt.GmShareEngineVO;
import com.globus.valueobject.prodmgmnt.GmShareListVO;
import com.globus.valueobject.prodmgmnt.GmSharedEmailVO;
import com.globus.valueobject.prodmgmnt.GmSharedFileVO;

/**
 * @author Anilkumar
 *
 */
@Path("share")
public class GmShareEngineResource extends GmResource {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**fetchSharedStatusDtls - This method is used to fetch all the Shared info Status details
	 * @param strInput
	 * @return JSON String
	 * @throws AppError
	 */	
	
	@POST
	@Path("sharedStatus")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	
	public GmShareListVO fetchSharedStatusDtls(String  strInput) throws AppError{
		
		HashMap hmParam= new HashMap();
		HashMap hmResult= new HashMap();
		
		GmShareListVO gmShareListVO = new GmShareListVO();
		GmShareEngineRptBean gmShareEngineRptBean = new GmShareEngineRptBean();
		GmWSUtil gmWSUtil=new GmWSUtil();
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		HashMap hmValidate = new HashMap();
		
		//validate token and parse JSON into VO
		parseInputAndValidateToken(strInput, gmShareListVO);
		hmValidate.put("partytype", "20615");
		//validate Party Type
		gmProdCatUtil.validateInput(gmShareListVO, hmValidate);
		GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmShareListVO.getToken());
		
		hmParam = gmWSUtil.getHashMapFromVO(gmShareListVO);
		hmParam.put("USERID",gmUserVO.getUserid());
		//  This method used to get the list of Shared Information
		hmResult = gmShareEngineRptBean.fetchSharedStatusDtls(hmParam);
		//This method will be used to populate shareListVo from shareEngineVO
		gmShareListVO = gmShareEngineRptBean.populateShareListVO(hmResult);
		
		return gmShareListVO;
	}

	@POST
	@Path("shareEmail")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	
	public GmShareEngineVO saveSharedInfo(String  strInput) throws AppError, JsonGenerationException, JsonMappingException, JsonParseException, IOException{

		HashMap hmParam= new HashMap();
		HashMap hmResult= new HashMap();
		HashMap hmParamStatus= new HashMap();

		GmShareEngineTransBean gmShareEngineTransBean = new GmShareEngineTransBean();
		GmCRMActivityBean gmCRMActivityBean = new GmCRMActivityBean();

		GmShareEngineVO gmShareEngineVO = new GmShareEngineVO();
		GmShareEngineRptBean gmShareEngineRptBean = new GmShareEngineRptBean();
		GmWSUtil gmWSUtil=new GmWSUtil();
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		
		if(strInput.contains("\\") && strInput.startsWith("\"") && strInput.endsWith("\"")){
			strInput= strInput.replaceAll("\\\\", "");
			strInput= strInput.substring(1, strInput.length() - 1);
		}
		
		ObjectMapper mapper = new ObjectMapper();
		 log.debug(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(mapper.readValue(strInput, Object.class)));
		 gmShareEngineVO = mapper.readValue(strInput,GmShareEngineVO.class);
		
		//validate token and parse JSON into VO
		//parseInputAndValidateToken(strInput, gmShareEngineVO);
		validateToken(gmShareEngineVO.getToken());
		
		HashMap hmValidate = new HashMap();
		hmValidate.put("shextrefid", "20620");
		//validate Party Type
		gmProdCatUtil.validateInput(gmShareEngineVO, hmValidate);
		
		GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmShareEngineVO.getToken());
		log.debug("gmUserVO:"+gmUserVO.getUserid());
		
		 //Convert file to DB input string
		// String strShFileInputstr = gmWSUtil.convertVOToCommaString(gmShareEngineVO);
		GmSharedFileVO [] arrGmSharedFileVO =  gmShareEngineVO.getGmSharedFileVO();
		String strShFileInputstr = gmWSUtil.getDbInputStringFromVO(arrGmSharedFileVO,",",",",GmSharedFileVO.getSaveSharedFileProperties(),"getSaveSharedFileProperties");
		strShFileInputstr = strShFileInputstr.substring(0, strShFileInputstr.length()-1);
		 log.debug("strShFileInputstr:"+strShFileInputstr);
		 
		 
		 //Convert  email VOs to DB input String
		 GmSharedEmailVO [] arrGmSharedEmailVO =  gmShareEngineVO.getGmSharedEmailVO();
		 String strEmailInputStr = gmWSUtil.getDbInputStringFromVO(arrGmSharedEmailVO,"^","|",GmSharedEmailVO.getSaveSharedEmailProperties(),"getSaveSharedEmailProperties");
		 
		 
		 hmParam = gmWSUtil.getHashMapFromVO(gmShareEngineVO);
		 
		 hmParam.put("FILEINPUTSTR", strShFileInputstr);
		 hmParam.put("EMAILINPUTSTR", strEmailInputStr);
		 hmParam.put("USERID", gmUserVO.getUserid());//gmUserVO.getUserid()
		 
		 log.debug("saveSharedInfo:::::"+hmParam);
		 hmParamStatus = gmShareEngineTransBean.saveSharedInfo(hmParam);
		 
		 //Send Message using JMS to process share
		 gmShareEngineTransBean.saveShareIdJms(hmParamStatus);
		 
		 HashMap hmJmsParam = new HashMap();
		 hmJmsParam.put("USERID",gmUserVO.getUserid());
		 hmJmsParam.put("STROPT","FILESHARE");
		 gmCRMActivityBean.syncPartyActivityJMS(hmJmsParam);
		    
		 GmShareEngineVO gmRtnShareEngineVO = new GmShareEngineVO();
		 
		 gmRtnShareEngineVO.setShareid((String)hmParamStatus.get("strShareID"));
		 gmRtnShareEngineVO.setSharestatus((String)hmParamStatus.get("strShareStatus"));
		 gmRtnShareEngineVO.setShextrefid(gmShareEngineVO.getShextrefid());
		 gmShareEngineVO = null;
		return gmRtnShareEngineVO;
	}
	
}
	
	