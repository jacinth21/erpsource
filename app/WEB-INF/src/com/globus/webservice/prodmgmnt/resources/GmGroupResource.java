package com.globus.webservice.prodmgmnt.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTokenManager;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.beans.GmGroupServiceBean;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.valueobject.common.GmProdCatlResListVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.prodmgmnt.GmGroupPartVO;
import com.globus.valueobject.prodmgmnt.GmGroupVO;
import com.globus.webservice.common.resources.GmResource;
import com.globus.webservice.config.Compress;

@Path("gopgroup")
public class GmGroupResource extends GmResource {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchGroups - This method will returns group values while calling web service. First call, It
   * will returns all group values which are published to product catalog after that it will returns
   * updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  @Compress
  @POST
  @Path("master")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlResListVO fetchGroups(String strInput) throws AppError {
    log.debug("=======fetchGroups======");

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);

    GmWSUtil gmWSUtil = new GmWSUtil();

    GmGroupVO gmGroupVO = new GmGroupVO();
    GmGroupServiceBean gmPartNumberServiceBean = new GmGroupServiceBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmGroupVO> listGmGroupVO = new ArrayList<GmGroupVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
    hmParams.put("VO", gmGroupVO);
    listGmGroupVO = gmPartNumberServiceBean.fetchGroups(hmParams);
    gmGroupVO = (GmGroupVO) gmProdCatlResListVO.setPageProperties(listGmGroupVO);

    gmGroupVO = (gmGroupVO == null) ? new GmGroupVO() : gmGroupVO;

    gmProdCatlResListVO.setReftype("103110"); // Group ref type


    hmParams.put("REFTYPE", gmProdCatlResListVO.getReftype());
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", gmProdCatlResListVO.getPagesize());
    hmParams.put("LASTREC", gmGroupVO.getGroupid()); // GROUPID
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmProdCatlResListVO.setListGmGroupVO(listGmGroupVO);

    return gmProdCatlResListVO;
  }

  
  
  
  /**
   * fetchGroups - This method will returns group values while calling web service.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  
  // PMT# Group sync in iPad with Interantional Changes
  
  @Compress
  @POST
  @Path("masterjson")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public String fetchJasonGroups(String strInput) throws AppError {
	  
	  log.debug("=======fetchJasonGroupsNew======");
	  
	  GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

	    // validate token and parse JSON into VO
	    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
	    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
	    HashMap hmValidate = new HashMap();
	    hmValidate.put("langid", "20607");
	    hmValidate.put("uuid", "20608");
	    // validate device UUID and language is not empty
	    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);

	    GmWSUtil gmWSUtil = new GmWSUtil();

	    GmGroupVO gmGroupVO = new GmGroupVO();
	    GmGroupServiceBean gmPartNumberServiceBean = new GmGroupServiceBean();
	    GmProdCatBean gmProdCatBean = new GmProdCatBean();

	    HashMap hmParams = new HashMap();
	    List alResult = new ArrayList();
	    List<GmGroupVO> listGmGroupVO = new ArrayList<GmGroupVO>();
	    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

	    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
	    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
	    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
	    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
	    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
	    hmParams.put("COMPID", GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid()));
	    hmParams.put("VO", gmGroupVO);
	    String strReturnJSON = gmPartNumberServiceBean.fetchJasonGroups(hmParams);
	    log.debug("strReturnJSON Group ?? "+strReturnJSON.length());
	    
	    gmGroupVO = (GmGroupVO) gmProdCatlResListVO.setPageProperties(listGmGroupVO);

	    gmGroupVO = (gmGroupVO == null) ? new GmGroupVO() : gmGroupVO;

	    gmProdCatlResListVO.setReftype("103110"); // Group ref type


	    hmParams.put("REFTYPE", gmProdCatlResListVO.getReftype());
	    hmParams.put("STATUSID", "103120"); // Initiated status
	    hmParams.put("PAGESIZE", gmProdCatlResListVO.getPagesize());
	    hmParams.put("LASTREC", "SYNCJSON"); // GROUPID
	    // To save details of device sync details and sync log.
	    gmProdCatBean.saveDeviceSyncDetail(hmParams);

	    gmProdCatlResListVO.setListGmGroupVO(listGmGroupVO);
	  
	    return strReturnJSON;
	  
  }
  
  
  /**
   * fetchGroupParts - This method will returns group parts values while calling web service. First
   * call, It will returns all group parts values which are published to product catalog after that
   * it will returns updated values only based on last sync for the device.
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  
  
  @Compress
  @POST
  @Path("detail")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public GmProdCatlResListVO fetchGroupParts(String strInput) throws AppError {
    log.debug("=======fetchGroupParts======");

    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

    // validate token and parse JSON into VO
    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    HashMap hmValidate = new HashMap();
    hmValidate.put("langid", "20607");
    hmValidate.put("uuid", "20608");
    // validate device UUID and language is not empty
    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);

    GmWSUtil gmWSUtil = new GmWSUtil();

    GmGroupPartVO gmGroupPartVO = new GmGroupPartVO();
    GmGroupServiceBean gmPartNumberServiceBean = new GmGroupServiceBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();

    HashMap hmParams = new HashMap();
    List alResult = new ArrayList();
    List<GmGroupPartVO> listGmGroupPartVO = new ArrayList<GmGroupPartVO>();
    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
    hmParams.put("VO", gmGroupPartVO);
    listGmGroupPartVO = gmPartNumberServiceBean.fetchGroupParts(hmParams);
    gmGroupPartVO = (GmGroupPartVO) gmProdCatlResListVO.setPageProperties(listGmGroupPartVO);

    gmGroupPartVO = (gmGroupPartVO == null) ? new GmGroupPartVO() : gmGroupPartVO;

    gmProdCatlResListVO.setReftype("4000408"); // Group Part ref type


    hmParams.put("REFTYPE", gmProdCatlResListVO.getReftype());
    hmParams.put("STATUSID", "103120"); // Initiated status
    hmParams.put("PAGESIZE", gmProdCatlResListVO.getPagesize());
    hmParams.put("LASTREC", gmGroupPartVO.getGrpdtlid()); // GRPDTLID
    // To save details of device sync details and sync log.
    gmProdCatBean.saveDeviceSyncDetail(hmParams);

    gmProdCatlResListVO.setListGmGroupPartVO(listGmGroupPartVO);

    return gmProdCatlResListVO;
  }
  
  
  // PMT# Group sync in iPad with Interantional Changes
  
  /**
   * fetchGroupParts - This method will returns group parts values while calling web service. 
   * 
   * @param strInput
   * @return JSON String
   * @throws AppError
   */
  	@Compress
  	@POST
  	@Path("detailjson")
  	@Produces({MediaType.APPLICATION_JSON})
  	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})

  
  	public String fetchJsonGroupParts(String strInput) throws AppError {
  	    log.debug("=======fetchGroupPartsNew======");

  	    GmProdCatlResListVO gmProdCatlResListVO = new GmProdCatlResListVO();

  	    // validate token and parse JSON into VO
  	    parseInputAndValidateToken(strInput, gmProdCatlResListVO);
  	    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
  	    HashMap hmValidate = new HashMap();
  	    hmValidate.put("langid", "20607");
  	    hmValidate.put("uuid", "20608");
  	    // validate device UUID and language is not empty
  	    gmProdCatUtil.validateInput(gmProdCatlResListVO, hmValidate);

  	    GmWSUtil gmWSUtil = new GmWSUtil();

  	    GmGroupPartVO gmGroupPartVO = new GmGroupPartVO();
  	    GmGroupServiceBean gmPartNumberServiceBean = new GmGroupServiceBean();
  	    GmProdCatBean gmProdCatBean = new GmProdCatBean();

  	    HashMap hmParams = new HashMap();
  	    List alResult = new ArrayList();
  	    List<GmGroupPartVO> listGmGroupPartVO = new ArrayList<GmGroupPartVO>();
  	    GmUserVO gmUserVO = GmTokenManager.getTokenValue(gmProdCatlResListVO.getToken());

  	    hmParams.put("TOKEN", GmCommonClass.parseNull(gmProdCatlResListVO.getToken()));
  	    hmParams.put("LANGUAGE", GmCommonClass.parseNull(gmProdCatlResListVO.getLangid()));
  	    hmParams.put("PAGENO", GmCommonClass.parseNull(gmProdCatlResListVO.getPageno()));
  	    hmParams.put("USERID", GmCommonClass.parseNull(gmUserVO.getUserid()));
  	    hmParams.put("UUID", GmCommonClass.parseNull(gmProdCatlResListVO.getUuid()));
  	    hmParams.put("COMPID", GmCommonClass.parseNull(gmProdCatlResListVO.getCmpid()));
  	    hmParams.put("VO", gmGroupPartVO);
  	    String strReturnJSON = gmPartNumberServiceBean.fetchJsonGroupParts(hmParams);
  	    log.debug("strReturnJSON Group Part?? "+strReturnJSON.length());
  	    
  	    gmGroupPartVO = (GmGroupPartVO) gmProdCatlResListVO.setPageProperties(listGmGroupPartVO);

  	    gmGroupPartVO = (gmGroupPartVO == null) ? new GmGroupPartVO() : gmGroupPartVO;

  	    gmProdCatlResListVO.setReftype("4000408"); // Group Part ref type


  	    hmParams.put("REFTYPE", gmProdCatlResListVO.getReftype());
  	    hmParams.put("STATUSID", "103120"); // Initiated status
  	    hmParams.put("PAGESIZE", gmProdCatlResListVO.getPagesize());
  	    hmParams.put("LASTREC", "SYNCJSON"); // GRPDTLID
  	    // To save details of device sync details and sync log.
  	    gmProdCatBean.saveDeviceSyncDetail(hmParams);

  	    gmProdCatlResListVO.setListGmGroupPartVO(listGmGroupPartVO);

  	    return strReturnJSON;
  
  	}
}
