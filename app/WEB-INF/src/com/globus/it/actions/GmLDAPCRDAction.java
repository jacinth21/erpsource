/**
 * ClassName : GmLDAPCRDAction.java Description: This class is used to configure AD Author : Version
 * : 1 Date: //2012 Copyright : Globus Medical Inc Change History : Created
 */
package com.globus.it.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSecurityBean;
import com.globus.it.beans.GmLdapBean;
import com.globus.it.forms.GmLDAPCRDForm;
import com.globus.logon.auth.GmLDAPManager;


public class GmLDAPCRDAction extends GmAction {
  /*
   * The below method is used to configure AD to portal
   */
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                 // Class.

    GmLDAPCRDForm gmLDAPCRDForm = (GmLDAPCRDForm) form;
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmSecurityBean gmSecurityBean = new GmSecurityBean();
    GmLdapBean gmLdapBean = new GmLdapBean(getGmDataStoreVO());

    gmLDAPCRDForm.loadSessionParameters(request);

    String strGroupID = "";
    String strOpt = "";
    String strPwd = "";
    String strUserName = "";
    String strMessage = "";

    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();

    strOpt = gmLDAPCRDForm.getStrOpt();
    strGroupID = gmLDAPCRDForm.getGroupID();

    hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmLDAPCRDForm));
    String strLdapId = GmCommonClass.parseNull((String) hmParam.get("LDAPID"));
    if (strLdapId.equals("")) {
      strLdapId = GmCommonClass.getRuleValue("DEFAULT_LDAP", "LDAP");
    }
    hmParam.put("LDAPID", strLdapId);
    ArrayList alLdap = GmCommonClass.parseNullArrayList(gmLdapBean.fetchLdapDetails("", "Y"));
    if (strOpt.equals("test")) {
      strMessage = GmCommonClass.parseNull(gmCommonBean.testLDAPConnection(hmParam));
      gmLDAPCRDForm.setMessage(strMessage);
      strOpt = "init";
    }

    if (strOpt.equals("save")) {
      gmCommonBean.saveSystemRules(hmParam);
      gmCommonBean.fetchLdapUser();// get system rule values from t906b table and store in static
                                   // HashMap
      strOpt = "reload";
    }

    /*
     * init - While test connecting LDAP may fail due to incorrect login credential. So, LDAP
     * reconnected with static HashMap values reload - While saving LDAP still connected with old
     * login credential. So, LDAP reconnected with new static HashMap values
     */
    if (strOpt.equals("init") || strOpt.equals("reload")) {
      GmLDAPManager gmLDAPManager = new GmLDAPManager(strLdapId);
      gmLDAPManager.initializeLDAP(strLdapId); // reconnected LDAP
    }

    if (strOpt.equals("load") || strOpt.equals("reload")) {

      hmReturn =
          GmCommonClass.parseNullHashMap(gmCommonBean.fetchSystemRules(strGroupID, strLdapId));
      log.debug(" hmReturn is " + hmReturn);
      // strUserName = GmCommonClass.parseNull((String) hmReturn.get("USERNAME"));
      // strPwd = GmCommonClass.parseNull((String) hmReturn.get("PASSWORD"));
      // strPwd = gmSecurityBean.decrypt(strPwd, strUserName);
      hmReturn = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get(strLdapId));
      hmReturn.put("PASSWORD", "");
      GmCommonClass.getFormFromHashMap(gmLDAPCRDForm, hmReturn);
    }
    gmLDAPCRDForm.setAlLdap(alLdap);
    return mapping.findForward("GmLDAPCRDSetup");
  }

}
