package com.globus.it.actions;


import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.it.beans.GmAssetBean;
import com.globus.it.forms.GmAssetMasterForm;


public class GmAssetMasterAction extends GmAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)
	throws Exception {

		Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.

		GmAssetMasterForm gmAssetMasterForm = (GmAssetMasterForm) form;
		gmAssetMasterForm.loadSessionParameters(request);
		GmAssetBean gmAssetBean = new GmAssetBean();
		GmCommonBean gmCommonBean = new GmCommonBean();
		String strOpt = gmAssetMasterForm.getStrOpt();
		String strAssetId = "";
		String strForward = "GmAssetMaster";

		HashMap hmParam = new HashMap();
		HashMap hmReturn = new HashMap();
		String strActiveFl = "";
		ArrayList alLogReasons = new ArrayList();
		ArrayList alResult = new ArrayList();

		hmParam =(HashMap)GmCommonClass.getHashMapFromForm(gmAssetMasterForm);

		if (strOpt.equals("save") || strOpt.equals("edit")) {
			log.debug("strOpt############"+strOpt);
			strAssetId = GmCommonClass.parseNull(gmAssetBean.saveAssetDetails(hmParam));
			strOpt = "edit";
			alLogReasons = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strAssetId, "1261"));
			gmAssetMasterForm.setAlLogReasons(alLogReasons);
			
		}else if (strOpt.equals("editload")) {
			
			strAssetId = GmCommonClass.parseNull((String)hmParam.get("ASSETID"));
			hmReturn = GmCommonClass.parseNullHashMap(gmAssetBean.fetchAssetDetails(strAssetId));
			GmCommonClass.getFormFromHashMap(gmAssetMasterForm, hmReturn);
			alLogReasons = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strAssetId, "1261"));
			gmAssetMasterForm.setAlLogReasons(alLogReasons);
			strOpt = "edit";
			
		}else if(strOpt.equals("report")){

			strForward = "GmAssetMasterReport";

		}else if (strOpt.equals("reload")){
			
			alResult = GmCommonClass.parseNullArrayList(gmAssetBean.fetchAssetMasterReport(hmParam));
			log.debug(" alResult " + alResult);
			gmAssetMasterForm.setAlResult(alResult);
			gmAssetMasterForm.setGridXmlData(getXmlGridData(alResult));
			strForward = "GmAssetMasterReport";
			
		}

		if (strOpt.equals("")) {
			strOpt = "save";
		}
		log.debug("Inside action GmAssetMasterAction##########################"+strForward+"###"+strOpt);
		gmAssetMasterForm.setStrOpt(strOpt);
		gmAssetMasterForm.setAssetID(strAssetId);
		return mapping.findForward(strForward);
	}
	public String getXmlGridData(ArrayList alParam) throws AppError {

		GmTemplateUtil templateUtil = new GmTemplateUtil();
		templateUtil.setDataList("alResult", alParam);
		templateUtil.setTemplateName("GmAssetMasterReport.vm");
		templateUtil.setTemplateSubDir("/it/templates");
		return templateUtil.generateOutput();
	}

}
