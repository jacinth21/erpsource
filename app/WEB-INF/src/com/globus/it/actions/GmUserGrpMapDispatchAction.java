package com.globus.it.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmCacheManager;
import com.globus.it.beans.GmUserGroupMapBean;
import com.globus.it.forms.GmUserGroupMapForm;



public class GmUserGrpMapDispatchAction extends GmDispatchAction{
	 Logger log = GmLogger.getInstance(this.getClass().getName());
	 
	 /**
		 * fetchAddUserGroup - This method retrieves active users and group 
		 * 
		 * @param actionMapping
		 * @param actionForm
		 * @param request
		 * @param response
		 * @return org.apache.struts.action.ActionForward
		 */
	 	public ActionForward fetchAddUserGroup(ActionMapping actionMapping,
				ActionForm form, HttpServletRequest request,
				HttpServletResponse response) throws AppError {
			ArrayList alResult = new ArrayList();	
			HashMap hmXmlParams = new HashMap();
			HashMap hmParams = new HashMap();
			
			String strXmlString = "";
			String strOpt = "";
			String strGrpType="";
			GmUserGroupMapBean gmUserGroupMapBean = new GmUserGroupMapBean();
			GmUserGroupMapForm gmUserGroupMapForm=(GmUserGroupMapForm) form;
			gmUserGroupMapForm.loadSessionParameters(request);
			
			strOpt = gmUserGroupMapForm.getStrOpt();
			strGrpType = gmUserGroupMapForm.getGrpType();
			
			if(strOpt.equals("fetch")){
				hmParams = GmCommonClass.getHashMapFromForm(gmUserGroupMapForm);
				log.debug("hmParams==>>"+hmParams);
				if(strGrpType.equals("92264") || strGrpType.equals("92265")){
					log.debug("strGrpType==>>"+strGrpType);
					alResult = GmCommonClass.parseNullArrayList(gmUserGroupMapBean.fetchModSecGroup(hmParams));
					log.debug("alResult==>>"+alResult);
				}else{
					alResult = GmCommonClass.parseNullArrayList(gmUserGroupMapBean.fetchAddUserGroup(hmParams));
					log.debug("alResult==>"+alResult);
				}
				hmXmlParams.put("TEMPLATE", "GmAddUserGroup.vm");
				hmXmlParams.put("TEMPLATEPATH", "it/templates");
				hmXmlParams.put("ALRESULT", alResult);
				strXmlString = GmCommonClass.parseNull(gmUserGroupMapBean.getXmlGridData(hmXmlParams,""));
				gmUserGroupMapForm.setXmlString(strXmlString);
			}
			return actionMapping.findForward("GmAddUserGroup"); 
		}
	 	
	 	
	 	 /**
		 * fchFunctDetails - This method retrieves  users and group details from access table 
		 * 
		 * @param actionMapping
		 * @param actionForm
		 * @param request
		 * @param response
		 * @return org.apache.struts.action.ActionForward
		 */
	 	
		public ActionForward fchFunctDetails(ActionMapping actionMapping,
				ActionForm form, HttpServletRequest request,
				HttpServletResponse response) throws AppError {
			
			
			GmUserGroupMapForm gmUserGroupMapForm=(GmUserGroupMapForm) form;
			gmUserGroupMapForm.loadSessionParameters(request);
			GmUserGroupMapBean gmUserGroupMappingBean = new GmUserGroupMapBean();
			
	        String strOpt = gmUserGroupMapForm.getStrOpt();
			//ArrayList alFunction = new ArrayList();			
			         
	        HashMap hmParam = GmCommonClass.getHashMapFromForm(gmUserGroupMapForm);
	      
	      	       		
	    	//alFunction = GmCommonClass.parseNullArrayList(gmUserGroupMappingBean.fetchAddUserGroup());
	    	//gmAddUserGroupForm.setAlFunction(alFunction);
			
			
	        if (strOpt.equals("OnFuncLoad") || strOpt.equals("OnSave"))
	        {
				   HashMap hmXmlParams=new HashMap();
				   HashMap hmResult=new HashMap();
				   HashMap hmValues=new HashMap();
				   HashMap hmStatus=new HashMap();
				   
				   ArrayList alFuncDetails= new ArrayList();
				   ArrayList alStatus= new ArrayList();
				   GmCommonClass gmCommonClass = new GmCommonClass();
				   alStatus = gmCommonClass.getCodeList("CERTR");
				   alStatus.remove(alStatus.size()-1);
				   String strGrpType = GmCommonClass.parseNull((String)gmUserGroupMapForm.getGrpType());
				   
				   if(strOpt.equals("OnSave"))
				   {
					   String group_id=GmCommonClass.parseNull((String)request.getParameter("hTaskNameStr"));
					   gmUserGroupMappingBean.saveGrpDetails(hmParam);
					   //to remove the module from cache server
					   GmCacheManager gmCacheManager=new GmCacheManager();		
					   StringTokenizer strToken = new StringTokenizer(group_id, "|");
					   while (strToken.hasMoreElements()) {
					      String  strGrpId = strToken.nextToken();
					      gmCacheManager.removePatternKeys("fetchUserNavigationLink:"+ strGrpId);    
					 }	                     
				   }
				   
				   hmResult = GmCommonClass.parseNullHashMap(gmUserGroupMappingBean.fetchFnGrpDetails(hmParam));
				   alFuncDetails=GmCommonClass.parseNullArrayList((ArrayList)hmResult.get("FUNCTION"));
				   if(alFuncDetails.size()>0)
				   {
				   hmValues=(HashMap)alFuncDetails.get(0);
				   gmUserGroupMapForm.setFuncID(GmCommonClass.parseNull((String)hmValues.get("FID")));
				   gmUserGroupMapForm.setFuncName(GmCommonClass.parseNull((String)hmValues.get("FNAME")));
				   gmUserGroupMapForm.setFuncDesc(GmCommonClass.parseNull((String)hmValues.get("FDESC")));
				   gmUserGroupMapForm.setFuncType(GmCommonClass.parseNull((String)hmValues.get("FTYPE")));
				   gmUserGroupMapForm.setFunType(GmCommonClass.parseNull((String)hmValues.get("CTYPE")));
				   }
				   hmValues.put("SESSDATEFMT", (String)request.getSession().getAttribute("strSessApplDateFmt"));
				   hmValues.put("GRPTYPE", strGrpType);
		    	   hmXmlParams.put("TEMPLATE", "GmUserGroupMap.vm");
				   hmXmlParams.put("TEMPLATEPATH", "it/templates");
				   hmXmlParams.put("ALRESULT", GmCommonClass.parseNullArrayList((ArrayList)hmResult.get("GROUP")));
				   hmXmlParams.put("ALSTATUS", alStatus);
				   hmXmlParams.put("HMSEQSTATUS", hmValues);
				   String strXmlString = gmUserGroupMappingBean.getXmlGridData(hmXmlParams,"ADD");
				   gmUserGroupMapForm.setXmlData(strXmlString);
			 }else if(strOpt.equals("SGMAP")){
				   HashMap hmParams=new HashMap();
				   hmParams.put("GRPID", GmCommonClass.parseNull(gmUserGroupMapForm.getGrpId()));
				   hmParams.put("GRPTYPE", GmCommonClass.parseNull(gmUserGroupMapForm.getGrpType()));
				   ArrayList alResult=GmCommonClass.parseNullArrayList(gmUserGroupMappingBean.fetchSecGrp(hmParams));
				   ArrayList alStatus= new ArrayList();
				   alStatus = GmCommonClass.getCodeList("CERTR");
				   HashMap hmValues=new HashMap();
				   HashMap hmXmlParams=new HashMap();
				   HashMap hmOpt=new HashMap();
				   hmOpt.put("STROPT", strOpt);
				   hmValues.put("SESSDATEFMT", (String)request.getSession().getAttribute("strSessApplDateFmt"));
				   hmValues.put("GRPTYPE", GmCommonClass.parseNull(gmUserGroupMapForm.getGrpType()));
		    	   hmXmlParams.put("TEMPLATE", "GmUserGroupMap.vm");
				   hmXmlParams.put("TEMPLATEPATH", "it/templates");
				   hmXmlParams.put("ALRESULT", alResult);
				   hmXmlParams.put("ALSTATUS", alStatus);
				   hmXmlParams.put("HMSEQSTATUS", hmValues);
				   hmXmlParams.put("HMOPT", hmOpt);
				   String strXmlString = gmUserGroupMappingBean.getXmlGridData(hmXmlParams,"ADD");
				   gmUserGroupMapForm.setXmlData(strXmlString);
				   HashMap hmParamVal=GmCommonClass.parseNullHashMap(gmUserGroupMappingBean.fetchGroupDetail(GmCommonClass.parseNull(gmUserGroupMapForm.getGrpId())));
				   String strGrpNm=GmCommonClass.parseNull((String)hmParamVal.get("GRPNM"));
				   String strGrpDesc=GmCommonClass.parseNull((String)hmParamVal.get("GRPDESC"));
				   gmUserGroupMapForm.setFuncName(strGrpNm);
				   gmUserGroupMapForm.setFuncDesc(strGrpDesc);
			 }

	       return actionMapping.findForward("GmUserGroupMap"); 
		}

		public ActionForward loadFunList(ActionMapping mapping, ActionForm form,HttpServletRequest request,HttpServletResponse response)throws AppError{
			
			GmUserGroupMapBean gmUserGroupMapBean = new GmUserGroupMapBean();
			GmUserGroupMapForm gmUserGroupMapForm=(GmUserGroupMapForm)form;
			HashMap hmXmlParams=new HashMap();
			HashMap hmValues=new HashMap();
			HashMap hmOpt=new HashMap();			
			ArrayList alResult = new ArrayList();
			String strOpt=GmCommonClass.parseNull((String)gmUserGroupMapForm.getStrOpt());
			String strAction = GmCommonClass.parseNull((String)gmUserGroupMapForm.getHaction());
			String strType="92262";
			if(strOpt.equals("SESG")){
				strType="92267";
			}
			alResult = gmUserGroupMapBean.fetchFunctionList(strType);
			if(strAction.equals("SECGRP_SECEVN")){
				HashMap hmParams=new HashMap();
				hmParams.put("GRPID", GmCommonClass.parseNull(gmUserGroupMapForm.getGrpId()));
				hmParams.put("GRPTYPE", GmCommonClass.parseNull(gmUserGroupMapForm.getGrpType()));
				alResult = GmCommonClass.parseNullArrayList(gmUserGroupMapBean.fetchSecEvent(hmParams));
			}
			hmOpt.put("STROPT", strOpt);
			hmValues.put("SESSDATEFMT", (String)request.getSession().getAttribute("strSessApplDateFmt"));
			
			ArrayList alReasons = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("VDTSK"));
			ArrayList alTaskType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LMTYP"));
			hmXmlParams.put("TEMPLATE", "GmFunctionListReport.vm");
			hmXmlParams.put("TEMPLATEPATH", "it/templates");
			hmXmlParams.put("ALRESULT", alResult);
			hmXmlParams.put("ALREASON", alReasons);
			hmXmlParams.put("ALTASKTYPE", alTaskType);
			hmXmlParams.put("HMSEQSTATUS", hmValues);
			hmXmlParams.put("HMOPT", hmOpt);
			String  strXmlString = GmCommonClass.parseNull(gmUserGroupMapBean.getXmlGridData(hmXmlParams,"ADD"));
			gmUserGroupMapForm.setXmlData(strXmlString);
			return mapping.findForward("GmFunctionListReport");
		}
		public ActionForward fetchGroupUserLookup(ActionMapping mapping, ActionForm form,HttpServletRequest request,HttpServletResponse response)throws AppError{
			GmAccessControlBean gmAccessControlBean=new GmAccessControlBean();
			GmUserGroupMapBean gmUserGroupMapBean = new GmUserGroupMapBean();
			GmUserGroupMapForm gmUserGroupMapForm=(GmUserGroupMapForm)form;
			HashMap hmParam=new HashMap();
			HashMap hmXmlParams=new HashMap();			
			String strGrpId=gmUserGroupMapForm.getGrpId();
			log.debug("strGrpId==> "+strGrpId);
			hmParam.put("GRPMAPPINGID","");
			hmParam.put("USERNM","0");
			hmParam.put("GROUPNAME",strGrpId);
			hmParam.put("DEFAULTGRP","");
			hmParam.put("USERID","0");
			hmParam.put("USERDEPT","0");			
			ArrayList alResult=GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchGroupMappingDetails(hmParam));
			hmXmlParams.put("TEMPLATE", "GmUserMapping.vm");
			hmXmlParams.put("TEMPLATEPATH", "user/templates");
			hmXmlParams.put("ALRESULT", alResult);
			String strGridXmlData = gmUserGroupMapBean.getXmlGridData(hmXmlParams,"");
			gmUserGroupMapForm.setXmlData(strGridXmlData);
			return mapping.findForward("GmUserGroupMapped");
		}
		public ActionForward saveFunctDetails(ActionMapping mapping, ActionForm form,HttpServletRequest request,HttpServletResponse response)throws AppError{
			GmUserGroupMapBean gmUserGroupMapBean = new GmUserGroupMapBean();
			GmUserGroupMapForm gmUserGroupMapForm=(GmUserGroupMapForm)form;
			gmUserGroupMapForm.loadSessionParameters(request);
			
			HashMap hmParam = GmCommonClass.getHashMapFromForm(gmUserGroupMapForm);
			log.debug("hmParam ::: " + hmParam);
			String strOpt = gmUserGroupMapForm.getStrOpt();
			
			GmCacheManager gmCacheManager=new GmCacheManager();
			gmCacheManager.removePatternKeys("fetchUserNavigationLink:");
			gmUserGroupMapBean.saveTaskDetails(hmParam);
			return mapping.findForward(strOpt);
		}
		
}
