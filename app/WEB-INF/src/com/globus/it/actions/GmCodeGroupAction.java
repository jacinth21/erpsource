package com.globus.it.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.it.beans.GmCodeGroupBean;
import com.globus.it.forms.GmCodeGroupForm;


public class GmCodeGroupAction extends GmAction {

public ActionForward execute(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response)
		throws Exception {
		
	Logger log = GmLogger.getInstance(this.getClass().getName());
	ArrayList alReturn = new ArrayList();
	HashMap hmParam = new HashMap();
	HashMap hmReturn = new HashMap();
	String strOpt = "";
	String strFrdPage = "GmCodeGroupSetup";
	String strcodeGrpID = "";
	int inChk = 0;
	GmCodeGroupBean gmCodeGroupBean = new GmCodeGroupBean();
	GmCodeGroupForm gmCodeGroupForm = (GmCodeGroupForm) form;
	gmCodeGroupForm.loadSessionParameters(request);
	hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmCodeGroupForm));
	strOpt = GmCommonClass.parseNull(gmCodeGroupForm.getStrOpt());
	if(strOpt.equals("check")){
		String strChk = "";
		inChk = Integer.parseInt(GmCommonClass.parseZero(gmCodeGroupBean.checkCodeGroup(gmCodeGroupForm.getCodeGrp())));
		if(inChk>0){
			strChk = "yes";
			strOpt = "check";
		}else{
			strChk = "no";
			strOpt = "edit";
		}
		gmCodeGroupForm.setStrChk(strChk);
	}else if(strOpt.equals("save")){
		strcodeGrpID =  GmCommonClass.parseNull(gmCodeGroupBean.saveCodeGroup(hmParam));
		gmCodeGroupForm.setCodeGrpID(strcodeGrpID);
		strOpt = "edit";
	}else if(strOpt.equals("edit")){
		hmReturn = GmCommonClass.parseNullHashMap(gmCodeGroupBean.fetchCodeGroup(hmParam));
		log.debug("hmReturn "+hmReturn.size());
		log.debug("hmReturn values"+hmReturn);
		gmCodeGroupForm = (GmCodeGroupForm)GmCommonClass.getFormFromHashMap(gmCodeGroupForm, hmReturn);
	}else if(strOpt.equals("load")){
		ArrayList alReport =GmCommonClass.parseNullArrayList(gmCodeGroupBean.fetchCodeGroupList(hmParam));
		String strXmlData = generateOutPut(alReport);
		gmCodeGroupForm.setGridXmlData(strXmlData);
		strFrdPage="GmCodeGroupReport";
	}else if(strOpt.equals("report")){
		strFrdPage="GmCodeGroupReport";
	}
	gmCodeGroupForm.setStrOpt(strOpt);
	return mapping.findForward(strFrdPage);
}
private String generateOutPut(ArrayList alGridData) throws AppError {
	GmTemplateUtil templateUtil = new GmTemplateUtil();
	templateUtil.setDataList("alGridData", alGridData);
	templateUtil.setTemplateSubDir("it/templates");
	templateUtil.setTemplateName("GmCodeGroup.vm");
	return templateUtil.generateOutput();
}

}
