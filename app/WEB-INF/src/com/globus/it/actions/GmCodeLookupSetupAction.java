package com.globus.it.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAutoCompleteTransBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.it.beans.GmCodeGroupBean;
import com.globus.it.forms.GmCodeLookupSetupForm;

public class GmCodeLookupSetupAction extends GmAction {
  // @Override
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    // TODO Auto-generated method stub

    Logger log = GmLogger.getInstance(this.getClass().getName());
    String strCodeGrp = "";
    String strOpt = "";
    ArrayList alResult = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();
    GmCodeGroupBean gmCodeGroupBean = new GmCodeGroupBean();
    GmCodeLookupSetupForm gmCodeLookupSetupForm = (GmCodeLookupSetupForm) form;
    gmCodeLookupSetupForm.loadSessionParameters(request);
    hmParam =
        GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmCodeLookupSetupForm));
    strOpt = gmCodeLookupSetupForm.getStrOpt();
    strCodeGrp = gmCodeLookupSetupForm.getCodeGrp();

    log.debug("strOpt Before : " + strOpt);


    if (strOpt.equals("save") || strOpt.equals("vsave")) {

      gmCodeGroupBean.saveCodeLookupValue(hmParam);
      gmCodeLookupSetupForm.setInputString("");
      strOpt = strOpt.equals("save") ? "report" : "void";
      // to update the details to cache server (using JMS)
      GmAutoCompleteTransBean gmAutoCompleteTransBean =
          new GmAutoCompleteTransBean(getGmDataStoreVO());
      HashMap hmCacheDtls = new HashMap();
      hmCacheDtls.put("CODE_GRP", strCodeGrp);
      hmCacheDtls.put("METHOD", "CodeLookUp");
      hmCacheDtls.put("companyInfo", GmCommonClass.parseNull(request.getParameter("companyInfo")));
      gmAutoCompleteTransBean.saveDetailsToCache(hmCacheDtls);


    }
    hmResult.put("STROPT", strOpt);

    log.debug("strOpt: " + strOpt);
    log.debug("strCodeGrp: " + strCodeGrp);
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    hmResult.put("COMPANYLOCALE", strSessCompanyLocale);
    alResult =
        GmCommonClass.parseNullArrayList(gmCodeGroupBean.fetchCodeLookupGroupList(strCodeGrp));
    HashMap hmCodeGrpDetail = new HashMap();
    hmCodeGrpDetail =GmCommonClass.parseNullHashMap(gmCodeGroupBean.fetchCodeGroupId(strCodeGrp));//fetch Code Group Details
    request.setAttribute("HMCODEGRPDETAIL",hmCodeGrpDetail);
    gmCodeLookupSetupForm.setGridXmlData(generateOutPut(alResult, hmResult));
    gmCodeLookupSetupForm.setStrOpt(strOpt);
    return mapping.findForward("GmCodeLookupSetup");
  }

  /**
   * getXmlGridData - This method will be used to generate Output for grid data.
   * 
   * @return String - will be used by dhtmlx grid
   * @exception AppError
   */
  private String generateOutPut(ArrayList alParam, HashMap hmParam) throws Exception {
    
	String strSessCompanyLocale = GmCommonClass.parseNull((String) hmParam.get("COMPANYLOCALE"));
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alParam);
    templateUtil.setDataMap("hmResult", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.it.GmCodeLookup", strSessCompanyLocale));
    templateUtil.setTemplateName("GmCodeLookup.vm");
    templateUtil.setTemplateSubDir("it/templates");
    return templateUtil.generateOutput();
  }

}
