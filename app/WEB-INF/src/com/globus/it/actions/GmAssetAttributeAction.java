package com.globus.it.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.beans.AppError;

//import com.globus.it.beans.GmAssetAttributeBean;
import com.globus.it.beans.GmAssetBean;
import com.globus.it.forms.GmAssetAttributeForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
public class GmAssetAttributeAction extends GmAction{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		// TODO Auto-generated method stub

		Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.

		String strOpt = "";
		String attId = "";
		String strAssetID = "";
		String strForward = "GmAssetAttribute";
		ArrayList alAssTypeList = new ArrayList();
		ArrayList alResult = new ArrayList();
		HashMap hmParam = new HashMap();
		HashMap hmTemp = new HashMap();

		GmAssetAttributeForm gmAssetAttributeForm=(GmAssetAttributeForm)form;
		gmAssetAttributeForm.loadSessionParameters(request);

		GmAssetBean gmAssetBean=new GmAssetBean();
		hmParam = GmCommonClass.getHashMapFromForm(gmAssetAttributeForm);
		strOpt=gmAssetAttributeForm.getStrOpt();

		if (strOpt.equals("save")) {

			attId = GmCommonClass.parseNull(gmAssetBean.saveAssetAtt(hmParam));
			gmAssetAttributeForm.setAttId(attId);
			strOpt = "edit";
			
		}else if (strOpt.equals("load")) {

			hmTemp = GmCommonClass.parseNullHashMap(gmAssetBean.getHashMapAssetAtt(hmParam));
			GmCommonClass.getFormFromHashMap(gmAssetAttributeForm,hmTemp);
			strOpt = "edit";

		}else if (strOpt.equals("editload")) {

			attId = GmCommonClass.parseNull((String)hmParam.get("ATTID"));
			hmTemp = GmCommonClass.parseNullHashMap(gmAssetBean.fetchAssetAttDetails(attId));
			GmCommonClass.getFormFromHashMap(gmAssetAttributeForm, hmTemp);
			strOpt = "edit";
			
		}else if (strOpt.equals("reset")) {
			
			gmAssetAttributeForm.setAssetID("");
			gmAssetAttributeForm.setAttId("");
			gmAssetAttributeForm.setAttributeValue("");
			gmAssetAttributeForm.setActiveflag("");
			gmAssetAttributeForm.setAttTyp("0");
			strOpt="load";
			
		}else if(strOpt.equals("report")){
			
			strForward = "GmAssetAttributeReport";
			
		}else if (strOpt.equals("reload")){
			
			alResult = GmCommonClass.parseNullArrayList(gmAssetBean.fetchAstAttributeReport(hmParam));
			gmAssetAttributeForm.setGridXmlData(getXmlGridData(alResult));
			strForward = "GmAssetAttributeReport";
			
		}

		alAssTypeList =  GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("ASSTYP"));
		gmAssetAttributeForm.setAlAssTyp(alAssTypeList);

		if(strOpt.equals(""))
		{
			strOpt = "load";
		}
		gmAssetAttributeForm.setStrOpt(strOpt);
		return mapping.findForward(strForward);
	}
	public String getXmlGridData(ArrayList alParam) throws AppError {

		GmTemplateUtil templateUtil = new GmTemplateUtil();
		templateUtil.setDataList("alResult", alParam);
		templateUtil.setTemplateName("GmAssetAttributeReport.vm");
		templateUtil.setTemplateSubDir("it/templates");

		return templateUtil.generateOutput();
	}
}
