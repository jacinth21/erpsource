package com.globus.it.actions;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.util.GmTemplateUtil;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAutoCompleteTransBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.it.beans.GmCodeGroupBean;
import com.globus.it.forms.GmCodeGroupForm;







import java.util.HashMap;

public class GmCodeLookupExlnAction extends GmDispatchAction{	
 Logger log = GmLogger.getInstance(this.getClass().getName());
	
 
 
	/**
	   * saveCodeLookupExclnValues - This method is used to save the codelookup Exclusion values by using . It will 
	   * get the company & type value display as a success message
	   * 
	   * @param actionMapping
	   * @param actionForm, company locale, hmParam
	   * @param request
	   * @param response
	   * @return org.apache.struts.action.ActionForward
	   * @throws IOException
	   */
	
	public ActionForward saveCodeLookupExclnValues(ActionMapping actionMapping, ActionForm actionForm,
		      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
			
		 instantiate(request, response);
		 ArrayList alCompanyList = new ArrayList();
		 ArrayList altype = new ArrayList();
		 HashMap hmParam = new HashMap();
		 
		GmCodeGroupForm gmCodeGroupForm = (GmCodeGroupForm)actionForm;		    
	    gmCodeGroupForm.loadSessionParameters(request);
		String strSessCompanyLocale= GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
	    GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.it.GmCodelookupExcln", strSessCompanyLocale);
	    GmCodeGroupBean gmCodeGroupBean = new GmCodeGroupBean(getGmDataStoreVO());
	    GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
	    String strMessage = "";
		String strOpt ="";
		String strCodeId = "";
		String strType="";
		String companystr="";
		String userid="";
		String companyListstr="";
		String strmessageComp="";
		String strCompanyID = "";
		String substring="";
		String strCodeGrp="";
		
		hmParam = GmCommonClass.getHashMapFromForm(gmCodeGroupForm);
		alCompanyList = GmCommonClass.parseNullArrayList(gmCompanyBean.loadCompanyInfo(""));
      	gmCodeGroupForm.setAlcompany(alCompanyList);
		altype = GmCommonClass.getCodeList("CETYPE");
		gmCodeGroupForm.setAltype(altype);
		
		strOpt = GmCommonClass.parseNull(gmCodeGroupForm.getStrOpt());
	    
		if(strOpt.equals("save")){
			
			 strCodeGrp=  GmCommonClass.parseNull(gmCodeGroupBean.saveCodeLookupExlnValue(hmParam));
			// to update the details to cache server (using JMS)   	  
     	      GmAutoCompleteTransBean gmAutoCompleteTransBean =
     	          new GmAutoCompleteTransBean(getGmDataStoreVO());
     	      HashMap hmCacheDtls = new HashMap();
     	      hmCacheDtls.put("CODE_GRP", strCodeGrp);
     	      hmCacheDtls.put("METHOD", "CodeLookUp");
     	      hmCacheDtls.put("companyInfo", GmCommonClass.parseNull(request.getParameter("companyInfo")));
     	      gmAutoCompleteTransBean.saveDetailsToCache(hmCacheDtls);

		    strCodeId = GmCommonClass.parseNull((String) hmParam.get("CODEID"));
		    strCodeId = strCodeId.substring(0, strCodeId.length() - 1);
		    strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
		    companystr= GmCommonClass.parseNull((String) hmParam.get("COMPANYSTR"));
		    String[] alcompanyList = companystr.split(",");
		 /*
		  * The for loop is using the strCompanyID get the strcompName ,showing in the success message 
	     */
		for (int i = 0; i < alcompanyList.length; i++) 
		{
			
	    strCompanyID= GmCommonClass.parseNull(alcompanyList[i]);
		String strcompName = GmCommonClass.getCompanyName(strCompanyID);
	    strmessageComp += strcompName + ",";
		}
		
		strmessageComp = strmessageComp.substring(0, strmessageComp.length() - 1);
		String strFollowing =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_THE_FOLLOWING_CODE"));
		String strSuccess =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ARE_SUCCESSFULLY_EXCLUDED_INCLUDED"));
		
		if(!strType.equals("") && strType.equals("106060")){
			strMessage=strFollowing+" "+ strCodeId+" " + strSuccess+" " + "Excluded for"+" " + strmessageComp;
		}else{
			strMessage=strFollowing+" "+ strCodeId+" " + strSuccess+" " + "Included for"+" " + strmessageComp;
		}
		
		
		request.setAttribute("hType", "S");
		throw new AppError(strMessage, "", 'S'); 
		}
			
			return actionMapping.findForward("gmCodeLookupExcln");
	}
	/**
	   * fetchCodeLookupExclnDtls - This method is used to fetch code lookup Exclusion details
	   * 
	   * @param actionMapping
	   * @param actionForm
	   * @param request
	   * @param response
	   * @return org.apache.struts.action.ActionForward
	   * @throws IOException
	   */
	
	public ActionForward fetchCodeLookupExclnDtls(ActionMapping actionMapping, ActionForm actionForm,
		      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
		    instantiate(request, response);
		    ArrayList alResult = new ArrayList();
		    
		    ArrayList alExclude = new ArrayList();
		    String strSessCompanyLocale =GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale")); 
		    GmCodeGroupForm gmCodeGroupForm = (GmCodeGroupForm)actionForm;		    
		    gmCodeGroupForm.loadSessionParameters(request);
		    GmCodeGroupBean gmCodeGroupBean = new GmCodeGroupBean(getGmDataStoreVO());
		    HashMap hmParam = new HashMap();
		    HashMap hmResults = new HashMap();
		    hmParam = GmCommonClass.getHashMapFromForm(gmCodeGroupForm);
		     
		    hmResults = gmCodeGroupBean.fetchCodeLookupExclnDtls(hmParam);
		    String strCodeName = GmCommonClass.parseNull((String)hmResults.get("CODENM"));
		    gmCodeGroupForm.setCodenm(strCodeName);
		    		    
		    hmResults.put("LABLESPATH","properties.labels.it.GmCodelookupExclnReport");
		    hmResults.put("TEMPLATE","GmCodeCompanyMapRpt.vm");
		    hmResults.put("TEMPLATEPATH","it/templates");
		   
		    
		    
		    String strXmlString =GmCommonClass.parseNull(getXmlGridData(hmResults,strSessCompanyLocale));
		    gmCodeGroupForm.setStrXmlString(strXmlString);
		    return actionMapping.findForward("GmCodeCompMapReport");
	 } 
	/**
      * Code Lookup Exclusion - This method will get the string datas value
      * @return String
      * @param  hmParam, company locale
	  */
	 public String getXmlGridData(HashMap hmParam,String strSessCompanyLocale) throws AppError {
		    ArrayList alInclude = new ArrayList();
		    GmTemplateUtil templateUtil = new GmTemplateUtil();
		    String strTemplateName = GmCommonClass.parseNull((String) hmParam.get("TEMPLATE"));
		    String strTemplatePath = GmCommonClass.parseNull((String) hmParam.get("TEMPLATEPATH"));
		    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean("properties.labels.it.GmCodelookupExclnReport", strSessCompanyLocale));
		    alInclude = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("INCLEXCLDETAILS"));
		    templateUtil.setDataList("alInclude", alInclude);
		    templateUtil.setTemplateSubDir(strTemplatePath);
		    templateUtil.setTemplateName(strTemplateName);
		    return templateUtil.generateOutput();
		  }
	 

}