package com.globus.it.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmCodeGroupForm extends GmCommonForm{
	private String codeGrpID = "";
	private String codeGrp = "";
	private String codeDes = "";
	private String lookupNeeded = "10";
	private String strChk = "";
	private String gridXmlData = "";
	private String codeFrm = "";
	private String codeTo = "";
	private String strXmlString = "";
	
	private String codeidstr="";
	private String companystr="";
	private String type="";
	private String codeid = "";
	private String codenm = "";
	
	
	

	private String selectAll = "";	
	private ArrayList altype = new ArrayList();
	private String comment="";
	private ArrayList alcompany = new ArrayList();
	private String hsetstatusNotcheck = "";
	private String checkSelectedCompany = "";
	
	public ArrayList getAlcompany() {
		return alcompany;
	}

	public void setAlcompany(ArrayList alcompany) {
		this.alcompany = alcompany;
	}


	public String getCheckSelectedCompany() {
		return checkSelectedCompany;
	}

	public void setCheckSelectedCompany(String checkSelectedCompany) {
		this.checkSelectedCompany = checkSelectedCompany;
	}

	public String getSelectAll() {
		return selectAll;
	}

	public void setSelectAll(String selectAll) {
		this.selectAll = selectAll;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getHsetstatusNotcheck() {
		return hsetstatusNotcheck;
	}

	public void setHsetstatusNotcheck(String hsetstatusNotcheck) {
		this.hsetstatusNotcheck = hsetstatusNotcheck;
	}

	public String getCompanyStr() {
		return companystr;
	}

	public void setCompanyStr(String companystr) {
		this.companystr = companystr;
	}
	
	
	public ArrayList getAltype() {
		return altype;
	}

	public void setAltype(ArrayList altype) {
		this.altype = altype;
	}

	public String getType() {
		return type;
	}
	public String getCodenm() {
		return codenm;
	}
	public void setCodenm(String codenm) {
		this.codenm = codenm;
	}

	public String getCodeid() {
		return codeid;
	}

	public void setCodeid(String codeid) {
		this.codeid = codeid;
	}

	
	
	public String getcodeidstr(){
		return codeidstr;
	}
	
	public void setcodeidstr(String codeidstr)
	{
		this.codeidstr = codeidstr;
	}
	
	public String getcompanystr(){
		return companystr;
	}
	
	public void setcompanystr(String companystr)
	{
		this.companystr = companystr;
	}
	
	public String gettype(){
		return type;
	}
	
	public void settype(String type)
	{
		this.type = type;
	}
	
	
	
	/**
	 * @return the codeFrm
	 */
	public String getCodeFrm() {
		return codeFrm;
	}
	/**
	 * @param codeFrm the codeFrm to set
	 */
	public void setCodeFrm(String codeFrm) {
		this.codeFrm = codeFrm;
	}
	/**
	 * @return the codeTo
	 */
	public String getCodeTo() {
		return codeTo;
	}
	/**
	 * @param codeTo the codeTo to set
	 */
	public void setCodeTo(String codeTo) {
		this.codeTo = codeTo;
	}
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	/**
	 * @return the codeGrpID
	 */
	public String getCodeGrpID() {
		return codeGrpID;
	}
	/**
	 * @param codeGrpID the codeGrpID to set
	 */
	public void setCodeGrpID(String codeGrpID) {
		this.codeGrpID = codeGrpID;
	}
	/**
	 * @return the codeGrp
	 */
	public String getCodeGrp() {
		return codeGrp;
	}
	/**
	 * @param codeGrp the codeGrp to set
	 */
	public void setCodeGrp(String codeGrp) {
		this.codeGrp = codeGrp;
	}
	/**
	 * @return the codeDes
	 */
	public String getCodeDes() {
		return codeDes;
	}
	/**
	 * @param codeDes the codeDes to set
	 */
	public void setCodeDes(String codeDes) {
		this.codeDes = codeDes;
	}
	/**
	 * @return the lookupNeeded
	 */
	public String getLookupNeeded() {
		return lookupNeeded;
	}
	/**
	 * @param lookupNeeded the lookupNeeded to set
	 */
	public void setLookupNeeded(String lookupNeeded) {
		this.lookupNeeded = lookupNeeded;
	}
	/**
	 * @return the strChk
	 */
	public String getStrChk() {
		return strChk;
	}
	/**
	 * @param strChk the strChk to set
	 */
	public void setStrChk(String strChk) {
		this.strChk = strChk;
	}
	public String getStrXmlString() {
		return strXmlString;
	}

	public void setStrXmlString(String strXmlString) {
		this.strXmlString = strXmlString;
	}
	
}
