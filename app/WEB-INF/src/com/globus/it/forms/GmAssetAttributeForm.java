package com.globus.it.forms;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;



import com.globus.common.forms.GmCommonForm;

public class GmAssetAttributeForm extends GmCommonForm{
	private String assetID="";
	private String attId="";
	private String attributeValue="";
	private String activeflag="";
	private String gridXmlData = "";
	private String attTyp="";
	private ArrayList alAssTyp=new ArrayList();
	
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	
	/**
	 * @return the attTyp
	 */
	public String getAttTyp() {
		return attTyp;
	}
	/**
	 * @param attTyp the attTyp to set
	 */
	public void setAttTyp(String attTyp) {
		this.attTyp = attTyp;
	}
		
	/**
	 * @return the alAttriType
	 */

	/**
	 * @return the alAssTyp
	 */
	public ArrayList getAlAssTyp() {
		return alAssTyp;
	}
	/**
	 * @param alAssTyp the alAssTyp to set
	 */
	public void setAlAssTyp(ArrayList alAssTyp) {
		this.alAssTyp = alAssTyp;
	}
	/**
	 * @return the assetID
	 */
	public String getAssetID() {
		return assetID;
	}
	/**
	 * @param assetID the assetID to set
	 */
	public void setAssetID(String assetID) {
		this.assetID = assetID;
	}
	
	
	/**
	 * @return the attId
	 */
	public String getAttId() {
		return attId;
	}
	/**
	 * @param attId the attId to set
	 */
	public void setAttId(String attId) {
		this.attId = attId;
	}
	/**
	 * @return the attributeValue
	 */
	public String getAttributeValue() {
		return attributeValue;
	}
	/**
	 * @param attributeValue the attributeValue to set
	 */
	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}
	/**
	 * @return the activeflag
	 */
	public String getActiveflag() {
		return activeflag;
	}
	/**
	 * @param activeflag the activeflag to set
	 */
	public void setActiveflag(String activeflag) {
		if(activeflag.equals("Y"))
			this.activeflag ="on";
		else
			this.activeflag = activeflag;
	}
	
}
