package com.globus.it.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmUserGroupMapForm extends GmCommonForm{
	
	private ArrayList alFunction = new ArrayList();
	private String filterData = "";
	private String xmlString = "";
	private String funcID = "";
	private String funcName = "";
	private String funcDesc = "";
	private String funcType = "";
	private String xmlData = "";
	private String hinputStr = "";
	private String rowId = "";
	private String grpId = "";
	private String funType = "";
	private String grpType="";
	
	private String hVoidStr = "";
	private String hTaskNameStr ="";
	private String hTaskDescStr ="";
	private String hTaskTypeStr ="";
	
	public String gethVoidStr() {
		return hVoidStr;
	}
	public void sethVoidStr(String hVoidStr) {
		this.hVoidStr = hVoidStr;
	}
	public String gethTaskNameStr() {
		return hTaskNameStr;
	}
	public void sethTaskNameStr(String hTaskNameStr) {
		this.hTaskNameStr = hTaskNameStr;
	}
	public String gethTaskDescStr() {
		return hTaskDescStr;
	}
	public void sethTaskDescStr(String hTaskDescStr) {
		this.hTaskDescStr = hTaskDescStr;
	}
	public String gethTaskTypeStr() {
		return hTaskTypeStr;
	}
	public void sethTaskTypeStr(String hTaskTypeStr) {
		this.hTaskTypeStr = hTaskTypeStr;
	}
	/**
	 * @return the grpType
	 */
	public String getGrpType() {
		return grpType;
	}
	/**
	 * @param grpType the grpType to set
	 */
	public void setGrpType(String grpType) {
		this.grpType = grpType;
	}
	/**
	 * @return the funType
	 */
	public String getFunType() {
		return funType;
	}
	/**
	 * @param funType the funType to set
	 */
	public void setFunType(String funType) {
		this.funType = funType;
	}
	/**
	 * @return the grpId
	 */
	public String getGrpId() {
		return grpId;
	}
	/**
	 * @param grpId the grpId to set
	 */
	
	public void setGrpId(String grpId) {
		this.grpId = grpId;
	}
	
	public ArrayList getAlFunction() {
		return alFunction;
	}
	
	public void setAlFunction(ArrayList alFunction) {
		this.alFunction = alFunction;
	}
	public String getFilterData() {
		return filterData;
	}
	public void setFilterData(String filterData) {
		this.filterData = filterData;
	}
	public String getXmlString() {
		return xmlString;
	}
	public void setXmlString(String xmlString) {
		this.xmlString = xmlString;
	}
	public String getFuncID() {
		return funcID;
	}
	public void setFuncID(String funcID) {
		this.funcID = funcID;
	}
	public String getFuncName() {
		return funcName;
	}
	public void setFuncName(String funcName) {
		this.funcName = funcName;
	}
	public String getFuncDesc() {
		return funcDesc;
	}
	public void setFuncDesc(String funcDesc) {
		this.funcDesc = funcDesc;
	}
	public String getFuncType() {
		return funcType;
	}
	public void setFuncType(String funcType) {
		this.funcType = funcType;
	}
	public String getXmlData() {
		return xmlData;
	}
	public void setXmlData(String xmlData) {
		this.xmlData = xmlData;
	}
	public String getHinputStr() {
		return hinputStr;
	}
	public void setHinputStr(String hinputStr) {
		this.hinputStr = hinputStr;
	}
	public String getRowId() {
		return rowId;
	}
	
	public void setRowId(String rowId) {
		this.rowId = rowId;
	}
}
