package com.globus.it.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmLogForm;

public class GmAssetMasterForm extends GmLogForm {

	private static final long serialVersionUID = 1234567890L;

	private String assetID;
	private String assetName;
	private String assetDesc;
	private String activeFlag;
	private ArrayList alResult = new ArrayList();
	private String gridXmlData = "";
	/**
	 * @return the alResult
	 */
	public ArrayList getAlResult() {
		return alResult;
	}
	/**
	 * @param alResult the alResult to set
	 */
	public void setAlResult(ArrayList alResult) {
		this.alResult = alResult;
	}
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	/**
	 * @return the assetID
	 */
	public String getAssetID() {
		return assetID;
	}

	/**
	 * @param assetID
	 *            the assetID to set
	 */
	public void setAssetID(String assetID) {
		this.assetID = assetID;
	}

	/**
	 * @return the assetName
	 */
	public String getAssetName() {
		return assetName;
	}

	/**
	 * @param assetName
	 *            the assetName to set
	 */
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	/**
	 * @return the assetDesc
	 */
	public String getAssetDesc() {
		return assetDesc;
	}

	/**
	 * @param assetDesc
	 *            the assetDesc to set
	 */
	public void setAssetDesc(String assetDesc) {
		this.assetDesc = assetDesc;
	}

	/**
	 * @return the activeFlag
	 */
	public String getActiveFlag() {
		return activeFlag;
	}

	/**
	 * @param activeFlag
	 *            the activeFlag to set
	 */
	public void setActiveFlag(String activeFlag) {
		if(activeFlag.equals("Y")){
			this.activeFlag = "on";
		}else{
			this.activeFlag = activeFlag;
		}
	}

}
