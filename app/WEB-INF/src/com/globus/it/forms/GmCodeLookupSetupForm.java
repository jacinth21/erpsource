package com.globus.it.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmCodeLookupSetupForm extends GmCommonForm {
	private String gridXmlData="";
	private String inputString="";
	private String codeGrp="";

	/**
	 * @return the codeGrp
	 */
	public String getCodeGrp() {
		return codeGrp;
	}

	/**
	 * @param codeGrp the codeGrp to set
	 */
	public void setCodeGrp(String codeGrp) {
		this.codeGrp = codeGrp;
	}

	private ArrayList alReturn=new ArrayList();

	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}

	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}

	/**
	 * @return the inputString
	 */
	public String getInputString() {
		return inputString;
	}

	/**
	 * @param inputString the inputString to set
	 */
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}

	/**
	 * @return the alReturn
	 */
	public ArrayList getAlReturn() {
		return alReturn;
	}

	/**
	 * @param alReturn the alReturn to set
	 */
	public void setAlReturn(ArrayList alReturn) {
		this.alReturn = alReturn;
	}
		

}
