/**
 * FileName : GmLDAPCRDForm.java Description: This class is used to handle AD configuration
 * properties Author : Version : 1 Date: //2012 Copyright : Globus Medical Inc Change History :
 * Created
 */
package com.globus.it.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmLDAPCRDForm extends GmCommonForm {

  private String name = "";
  private String userName = "";
  private String password = "";
  private String hostName = "";
  private String port = "";
  private String userdn = "";
  private String groupID = "";
  private String message = "";
  private String ldapID = "";
  private String base = "";
  private ArrayList alLdap = new ArrayList();

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the userName
   */
  public String getUserName() {
    return userName;
  }

  /**
   * @param userName the userName to set
   */
  public void setUserName(String userName) {
    this.userName = userName;
  }

  /**
   * @return the password
   */
  public String getPassword() {
    return password;
  }

  /**
   * @param password the password to set
   */
  public void setPassword(String password) {
    this.password = password;
  }

  /**
   * @return the hostName
   */
  public String getHostName() {
    return hostName;
  }

  /**
   * @param hostName the hostName to set
   */
  public void setHostName(String hostName) {
    this.hostName = hostName;
  }

  /**
   * @return the port
   */
  public String getPort() {
    return port;
  }

  /**
   * @param port the port to set
   */
  public void setPort(String port) {
    this.port = port;
  }

  /**
   * @return the userdn
   */
  public String getUserdn() {
    return userdn;
  }

  /**
   * @param userdn the userdn to set
   */
  public void setUserdn(String userdn) {
    this.userdn = userdn;
  }

  /**
   * @return the groupID
   */
  public String getGroupID() {
    return groupID;
  }

  /**
   * @param groupID the groupID to set
   */
  public void setGroupID(String groupID) {
    this.groupID = groupID;
  }

  /**
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  /**
   * @param message the message to set
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * @return the ldapID
   */
  public String getLdapID() {
    return ldapID;
  }

  /**
   * @param ldapID the ldapID to set
   */
  public void setLdapID(String ldapID) {
    this.ldapID = ldapID;
  }

  /**
   * @return the alLdap
   */
  public ArrayList getAlLdap() {
    return alLdap;
  }

  /**
   * @param alLdap the alLdap to set
   */
  public void setAlLdap(ArrayList alLdap) {
    this.alLdap = alLdap;
  }

  /**
   * @return the base
   */
  public String getBase() {
    return base;
  }

  /**
   * @param base the base to set
   */
  public void setBase(String base) {
    this.base = base;
  }

}
