package com.globus.it.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmLdapBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * Default Constructor
   */
  public GmLdapBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * @param gmDataStoreVO
   * @throws AppError
   */
  public GmLdapBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    // TODO Auto-generated constructor stub
    super(gmDataStoreVO);
  }

  /**
   * This method used to fetch the LDAP master details
   * 
   * @param strLdapId
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchLdapDetails(String strLdapId, String strActiveFl) throws AppError {
    ArrayList alLdapDtls = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_ldap.gm_fch_ldap_dtls", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLdapId);
    gmDBManager.setString(2, strActiveFl);
    gmDBManager.execute();
    alLdapDtls = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    log.debug(" LDAP Master size " + alLdapDtls.size());
    return alLdapDtls;
  }

  /**
   * This method used to fetch LDAP ID based on User name
   * 
   * @param strUserName
   * @return String
   * @throws AppError
   */
  public String getLdapIdFromUserName(String strUserName) throws AppError {
    String strLdapId = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_it_ldap.get_ldap_id_from_user_name", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strUserName);
    gmDBManager.execute();
    strLdapId = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    log.debug(" User name LDAP ID --> " + strLdapId);
    return strLdapId;

  }

  /**
   * This method used to fetch LDAP ID based on User ID
   * 
   * @param strUserId
   * @return String
   * @throws AppError
   */
  public String getLdapId(String strUserId) throws AppError {
    String strLdapId = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_it_ldap.get_ldap_id_from_user_id", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    strLdapId = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    log.debug(" User LDAP ID " + strLdapId);
    return strLdapId;
  }

  /**
   * This method used to fetch the data based on user name and password
   * 
   * @param strUserName
   * @param strPassword
   * @return HashMap
   * @throws AppError
   */
  public HashMap fetchDBUserDetails(String strUserName, String strPassword) throws AppError {
    HashMap hmDbUserDtls = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_ldap.gm_fch_db_user_ldap_id", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strUserName);
    gmDBManager.setString(2, strPassword);
    gmDBManager.execute();
    hmDbUserDtls = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    log.debug(" DB User deatils -->" + hmDbUserDtls);
    return hmDbUserDtls;

  }
}
