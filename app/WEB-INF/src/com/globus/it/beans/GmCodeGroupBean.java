package com.globus.it.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmCodeGroupBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmCodeGroupBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * @param gmDataStoreVO
   */
  public GmCodeGroupBean(GmDataStoreVO gmDataStoreVO) {
    super(gmDataStoreVO);
  }

  /**
   * getCodeList - This method returns a list of code names for code group based on company
   * 
   * @param strCodeGrp - Code group name
   * @param strDeptId - strDeptId
   * @param gmDataStoreVO - Contains the company details
   * @exception AppError
   * @return ArrayList
   */

  public ArrayList getCodeList(String strCodeGrp, String strDeptId, GmDataStoreVO gmDataStoreVO)
      throws AppError {

    ArrayList arList = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager();
    String strCodeGrpParsed = GmCommonClass.getStringWithQuotes(strCodeGrp);
    String strCompanyID = "";

    if (gmDataStoreVO != null) {
      strCompanyID = GmCommonClass.parseNull(gmDataStoreVO.getCmpid());
    }
    return getCodeList(strCodeGrp, strDeptId, strCompanyID);
  }

  /**
   * getCodeList - This method returns a list of code names for code group based on company
   * 
   * @param strCodeGrp - Code group name
   * @param strDeptId - strDeptId
   * @param strCompanyId - company id
   * @exception AppError
   * @return ArrayList
   */

  public ArrayList getCodeList(String strCodeGrp, String strDeptId, String strCompanyId)
      throws AppError {

    ArrayList arList = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    GmCacheManager gmCacheManager = new GmCacheManager();
    String strCodeGrpParsed = GmCommonClass.getStringWithQuotes(strCodeGrp);

    sbQuery.append("SELECT  T901.C901_CODE_ID CODEID, T901.C901_CODE_NM CODENM ");
    sbQuery
        .append(",T901.C902_CODE_NM_ALT CODENMALT, T901.C901_CONTROL_TYPE CONTROLTYP,T901.C901_CODE_SEQ_NO CODESEQNO");
    sbQuery.append(" FROM  T901_CODE_LOOKUP T901 ");
    sbQuery.append(" WHERE  T901.C901_CODE_GRP IN ('" + strCodeGrpParsed + "')");
    sbQuery.append(" AND T901.C901_ACTIVE_FL = '1' ");
    if (!strDeptId.equals("")) {
      sbQuery
          .append(" AND T901.c901_code_id IN (SELECT T901A.C901_CODE_ID FROM T901A_LOOKUP_ACCESS T901A WHERE T901A.C901_ACCESS_CODE_ID IN (");
      sbQuery.append(strDeptId);
      sbQuery.append("))");
    }
    sbQuery.append(" AND C901_VOID_FL          IS NULL ");

    // Below Query will filter the Code ID that are in the Exclusion for the Company.
    if (!strCompanyId.equals("")) {
      sbQuery.append(" AND T901.C901_CODE_ID NOT IN ");
      sbQuery.append(" ( ");
      sbQuery.append(" SELECT T901D.C901_CODE_ID ");
      sbQuery.append(" FROM T901D_CODE_LKP_COMPANY_EXCLN T901D, T901_CODE_LOOKUP T901 ");
      sbQuery.append(" WHERE T901D.C901D_VOID_FL   IS NULL ");
      sbQuery.append(" AND T901D.C1900_COMPANY_ID = " + strCompanyId + "");
      sbQuery.append(" AND T901D.C901_CODE_ID     = T901.C901_CODE_ID ");
      sbQuery.append(" AND T901.C901_CODE_GRP    IN ('" + strCodeGrpParsed + "')");
      sbQuery.append(" AND T901.C901_ACTIVE_FL    = '1' ");
      sbQuery.append(" AND T901.C901_VOID_FL     IS NULL ");
      sbQuery.append(" ) ");
    }
    // sbQuery.append(" AND  ( C1900_COMPANY_ID = '" + strCompanyID +
    // "' OR C1900_COMPANY_ID IS NULL  )");
    sbQuery.append(" ORDER BY T901.C901_CODE_SEQ_NO, UPPER(T901.C901_CODE_NM) ");
    log.debug("sbQuery.toString()" + sbQuery.toString());
    gmCacheManager.setKey("getCodeList:" + strCompanyId + ":" + "DeptId:" + strDeptId + ":"
        + strCodeGrpParsed);
    arList = gmCacheManager.queryMultipleRecords(sbQuery.toString());

    if (arList.size() < 0) {
      throw new AppError(AppError.APPLICATION, "1002");
    }
    return arList;
  }

  /**
   * saveCodeGroup - This method returns a code group ID
   * 
   * @param HashMap hmParam Holds Code Group detail
   * 
   * @exception AppError
   * @return String object
   */
  public String saveCodeGroup(HashMap hmParam) throws AppError {
    String strCodeGrpID = GmCommonClass.parseNull((String) hmParam.get("CODEGRPID"));
    String strCodeGrp = GmCommonClass.parseNull((String) hmParam.get("CODEGRP"));
    String strCodeDes = GmCommonClass.parseNull((String) hmParam.get("CODEDES"));
    String strLookupNeeded = GmCommonClass.parseNull((String) hmParam.get("LOOKUPNEEDED"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_code_lookup.gm_sav_code_group ", 5);
    gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
    gmDBManager.setString(1, strCodeGrpID);
    gmDBManager.setString(2, strCodeGrp.toUpperCase());
    gmDBManager.setString(3, strCodeDes);
    gmDBManager.setString(4, strLookupNeeded);
    gmDBManager.setString(5, strUserId);
    gmDBManager.execute();
    strCodeGrpID = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.commit();
    return strCodeGrpID;
  }

  /**
   * checkCodeGroup - This method returns number of specific code Group
   * 
   * @param String strCodeGrp Holds Code Group ID
   * 
   * @exception AppError
   * @return String object
   */
  public String checkCodeGroup(String strCodeGrp) throws AppError {
    String strResult = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_code_lookup.gm_chk_code_group", 1);
    gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
    gmDBManager.setString(1, strCodeGrp.toUpperCase());
    gmDBManager.execute();
    strResult = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    return strResult;
  }

  /**
   * fetchCodeGroup - This method returns a HashMap
   * 
   * @param HashMap hmParam Holds Code Group detail
   * 
   * @exception AppError
   * @return HashMap object
   */
  public HashMap fetchCodeGroup(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    String strCode_Grp_ID = GmCommonClass.parseNull((String) hmParam.get("CODEGRPID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_code_lookup.gm_fch_code_group", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strCode_Grp_ID);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * fetchCodeGroupList - This method returns an ArrayList
   * 
   * @param HashMap hmParam Holds Code Group and code ID values
   * 
   * @exception AppError
   * @return ArrayList object
   */
  public ArrayList fetchCodeGroupList(HashMap hmParams) throws AppError {
    GmDBManager db = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    db.setPrepareString("gm_pkg_cm_code_lookup.gm_fetch_code_group", 4);
    db.registerOutParameter(4, OracleTypes.CURSOR);
    db.setString(1, (String) hmParams.get("CODEGRP"));
    db.setString(2, (String) hmParams.get("CODEIDFRM"));
    db.setString(3, (String) hmParams.get("CODEIDTO"));
    db.execute();
    alResult = db.returnArrayList((ResultSet) db.getObject(4));
    db.close();
    return alResult;
  }

  /**
   * saveCodeLookupValue - This method is used to save code lookup values.
   * 
   * @param HashMap hmParam Holds code lookup values as string
   * 
   * @exception AppError
   * 
   */
  public void saveCodeLookupValue(HashMap hmParam) throws AppError {
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_cm_code_lookup.gm_sav_code_lookup", 2);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strUserID);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * saveCodeLookupExlnValue - This method is used to save code lookup exxclusion values.
   * 
   * @param hmParam
   * 
   * @exception AppError
   * 
   */

  public String saveCodeLookupExlnValue(HashMap hmParam) throws AppError {
    String strCodeGrp = "";
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_cm_code_lookup.gm_sav_code_lookup_excln", 5);
    gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("CODEID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("COMPANYSTR")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("TYPE")));
    gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParam.get("USERID")));
    gmDBManager.execute();
    strCodeGrp = GmCommonClass.parseNull(gmDBManager.getString(5));
    gmDBManager.commit();
    return strCodeGrp;
  }


  /**
   * fetchCodeLookupGroupList - This method returns an ArrayList
   * 
   * @param HashMap hmParam Holds Code lookups values
   * 
   * @exception AppError
   * @return ArrayList object
   */
  public ArrayList fetchCodeLookupGroupList(String strCode_Grp) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_cm_code_lookup.gm_fch_code_group_list", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strCode_Grp);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alResult;
  }

  /**
   * fetchCodeLookupExclnDtls - This method returns an ArrayList
   * 
   * @param HashMap hmParam Holds Code lookups values
   * 
   * @exception AppError
   * @return include & exclude details
   */

public HashMap fetchCodeLookupExclnDtls(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    String strCodeId = GmCommonClass.parseNull((String) hmParam.get("CODEID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    ArrayList alInclExclCompany = new ArrayList();

    gmDBManager.setPrepareString("gm_pkg_cm_code_lookup.gm_fch_code_lookup_excln_dtls", 3);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strCodeId);
    gmDBManager.execute();

    hmReturn =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(2)));
    alInclExclCompany =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));

    hmReturn.put("INCLEXCLDETAILS", alInclExclCompany);

    gmDBManager.close();
    return hmReturn;


  }
 
  /**
   * fetchCodeGroupId from Code Group Master Table - This method returns an ArrayList
   * 
   * @param strCodeGrp
   * @return
   * @throws AppError
 */
public HashMap fetchCodeGroupId(String strCodeGrp) throws AppError {
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    HashMap hmCodeGrpDetail = new HashMap();
	    gmDBManager.setPrepareString("gm_pkg_cm_code_lookup.gm_fch_code_group_id", 2);
	    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	    gmDBManager.setString(1, strCodeGrp);
	    gmDBManager.execute();
	    hmCodeGrpDetail = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
	    gmDBManager.close();
	    return hmCodeGrpDetail;
	  }
  
}
