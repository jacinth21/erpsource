package com.globus.it.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.it.forms.GmAssetMasterForm;


public class GmAssetBean {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	GmCommonBean gmCommonBean = new GmCommonBean();

	public String saveAssetDetails(HashMap hmParam) throws AppError {

		GmDBManager gmDBManager = new GmDBManager();
		String strAssetFromDB = "";
		String strAssetId = GmCommonClass.parseNull((String) hmParam.get("ASSETID"));
		String strAssetName = GmCommonClass.parseNull((String) hmParam.get("ASSETNAME"));
		String strAssetDesc = GmCommonClass.parseNull((String) hmParam.get("ASSETDESC"));
		String strActiveFlag = GmCommonClass.getCheckBoxValue((String) hmParam.get("ACTIVEFLAG"));
		String strStrOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
		String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		
		gmDBManager.setPrepareString("gm_pkg_it_asset.gm_ac_save_assdetails", 5);

		gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
		gmDBManager.setString(1, strAssetId);
		gmDBManager.setString(2, strAssetName);
		gmDBManager.setString(3, strAssetDesc);
		gmDBManager.setString(4, strActiveFlag);
		gmDBManager.setString(5, strUserID);
		gmDBManager.execute();
		strAssetFromDB = GmCommonClass.parseNull(gmDBManager.getString(1));
		// For Saving Comments Log
		if (!strLogReason.equals("")) {
			gmCommonBean.saveLog(gmDBManager, strAssetFromDB, strLogReason,strUserID, "1261");
		}
		gmDBManager.commit();
		return strAssetFromDB;
	}
		
	public HashMap fetchAssetDetails(String strAssetId) throws AppError {

		GmDBManager gmDBManager = new GmDBManager();
		HashMap hmReturn = new HashMap();
		gmDBManager.setPrepareString("gm_pkg_it_asset.gm_it_fch_assdetails", 2);
		gmDBManager.setString(1, strAssetId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		
		hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		
		return hmReturn;
	}
	public ArrayList fetchAssetMasterReport(HashMap hmValues) throws AppError
	{
		GmDBManager gmDBManager = new GmDBManager ();
		ArrayList alReturn = new ArrayList();
		StringBuffer sbQuery = new StringBuffer();
		
		String strAssetId = GmCommonClass.parseNull((String) hmValues.get("ASSETID"));
		String strAssetName = GmCommonClass.parseNull((String) hmValues.get("ASSETNAME"));
		String strAssetDesc = GmCommonClass.parseNull((String) hmValues.get("ASSETDESC"));
		String strActiveFlag = GmCommonClass.getCheckBoxValue((String) hmValues.get("ACTIVEFLAG"));
		
		sbQuery.append("select c221_asset_id assetid, c221_asset_nm assetname, ");
		sbQuery.append(" c221_asset_desc assetdesc,c221_active_fl active ");
		sbQuery.append(" from t221_asset_master where c221_void_fl is null ");

		
		if (!strAssetId.equals(""))
		{
			sbQuery.append(" AND c221_asset_id like '%");
			sbQuery.append(strAssetId);
			sbQuery.append("%'");
		}
		
		if (!strAssetName.equals(""))
		{
			sbQuery.append(" AND c221_asset_nm like '%");
			sbQuery.append(strAssetName);
			sbQuery.append("%'");
		}
		
		if (!strAssetDesc.equals(""))
		{
			sbQuery.append(" AND c221_asset_desc like '%");
			sbQuery.append(strAssetDesc);
			sbQuery.append("%'");
		}	
		
		if (!strActiveFlag.equals(""))
		{
			sbQuery.append(" AND c221_active_fl = 'Y'");
		}			
		log.debug("sbQuery#######"+sbQuery.toString());		
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		gmDBManager.close();
	    
		return alReturn;
	}
	
	public ArrayList fetchAstAttributeReport(HashMap hmValues) throws AppError
	{
		GmDBManager gmDBManager = new GmDBManager ();
		ArrayList alReturn = new ArrayList();
		StringBuffer sbQuery = new StringBuffer();
		String strAssetId = GmCommonClass.parseNull((String)hmValues.get("ASSETID"));
		String strAttTyp = GmCommonClass.parseNull((String)hmValues.get("ATTTYP"));
		String strAttributeValue = GmCommonClass.parseNull((String)hmValues.get("ATTRIBUTEVALUE"));
		String strActFl= GmCommonClass.getCheckBoxValue((String)hmValues.get("ACTIVEFLAG"));
		sbQuery.append("SELECT c222_asset_attrib_seq_id ATTID, c221_asset_id ASSETID,get_code_name(c901_attribute_type) ATTTYPNM, ");
		sbQuery.append(" c901_attribute_type ATTTYP,c222_attribute_value ATTRIBUTEVALUE, c222_active_fl ACTIVEFLAG ");
		sbQuery.append(" FROM t222_asset_attribute where c222_void_fl is null ");
		if (!strAssetId.equals("")){
			sbQuery.append(" AND c221_asset_id like '%");
			sbQuery.append(strAssetId);
			sbQuery.append("%'");
		}
		if (!strAttTyp.equals("0")){
			sbQuery.append(" AND c901_attribute_type=");
			sbQuery.append(strAttTyp);
			sbQuery.append("");
		}
		if (!strAttributeValue.equals("")){
			sbQuery.append(" AND c222_attribute_value like '%");
			sbQuery.append(strAttributeValue);
			sbQuery.append("%'");
		}	
		if (!strActFl.equals("")){
			sbQuery.append(" AND c222_active_fl = 'Y'");
		}
		log.debug("sbQuery.toString()##########"+sbQuery.toString());
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		gmDBManager.close();
		return alReturn;
	}
	public String saveAssetAtt(HashMap hmParams)throws AppError {
		String attidFrmDB="";
		String strAssetID = GmCommonClass.parseNull((String) hmParams.get("ASSETID"));
		String strAttType = GmCommonClass.parseNull((String) hmParams.get("ATTTYP"));		
		String strAttValue = GmCommonClass.parseNull((String) hmParams.get("ATTRIBUTEVALUE"));
		String strActFl = GmCommonClass.getCheckBoxValue((String) hmParams.get("ACTIVEFLAG"));
		String strUserId = GmCommonClass.parseNull((String)hmParams.get("USERID"));
		String strAttid=GmCommonClass.parseNull((String)hmParams.get("ATTID"));
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("GM_PKG_IT_ASSET.GM_SAV_ASS_ATT", 6);
		gmDBManager.registerOutParameter(6, java.sql.Types.CHAR);
		gmDBManager.setString(1, strAssetID);
		gmDBManager.setInt(2,Integer.parseInt(strAttType));					
		gmDBManager.setString(3,strAttValue);
		gmDBManager.setString(4, strActFl);
		gmDBManager.setString(5, strUserId);
		gmDBManager.setString(6, strAttid);
		gmDBManager.execute();
		attidFrmDB = GmCommonClass.parseNull(gmDBManager.getString(6));
		gmDBManager.commit();
		return attidFrmDB; 	
	}


	public HashMap getHashMapAssetAtt(HashMap hmParams)throws AppError {
		HashMap hmresult=new HashMap();
		String strAssetID = GmCommonClass.parseNull((String) hmParams.get("ASSETID"));
		String strAttType = GmCommonClass.parseNull((String) hmParams.get("ATTTYP"));		
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("GM_PKG_IT_ASSET.GM_CHK_ASS_ATT",3);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.setString(1, strAssetID);
		gmDBManager.setInt(2,Integer.parseInt(strAttType));					
		gmDBManager.execute();
		hmresult=gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		String strAssID=GmCommonClass.parseNull((String)hmresult.get("ASSETID"));
		if(strAssID.equals("")){
			hmresult.put("ASSETID", strAssetID);
			hmresult.put("ATTTYP",strAttType);
			hmresult.put("ATTRIBUTEVALUE","");
			hmresult.put("ACTIVEFLAG","");
			hmresult.put("ATTID","");
		}
		return hmresult; 	
	}
	
	public HashMap fetchAssetAttDetails(String strAttId) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		HashMap hmReturn = new HashMap();
		gmDBManager.setPrepareString("GM_PKG_IT_ASSET.GM_FTCH_ASS_ATT", 2);
		gmDBManager.setString(1, strAttId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		return hmReturn;
	}
}