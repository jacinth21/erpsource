package com.globus.it.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;

public class GmUserGroupMapBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	/**	 
	 * fetchAddUserGroup() This method retrieves active users and group based on the argument *  	
	 * @return ArrayList -  user/group records
	 */
	public ArrayList fetchAddUserGroup(HashMap hmParam) throws AppError {

		ArrayList alUserList = new ArrayList();
		String strFuncID = GmCommonClass.parseNull((String)hmParam.get("FUNCID"));
		String strFilterTxt = GmCommonClass.parseNull((String)hmParam.get("FILTERDATA"));
		GmDBManager gmDBManager = new GmDBManager();		
		gmDBManager.setPrepareString("gm_pkg_it_grp_user_map.gm_fch_add_group_user",3);
		gmDBManager.setString(1, strFuncID);
		gmDBManager.setString(2, strFilterTxt);
        gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
        gmDBManager.execute();
        alUserList = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3)));
		log.debug("alUserList   "+alUserList);
		gmDBManager.close();
		return alUserList;
	}
	/**
	 * @param hmParam
	 * @return ArrayList
	 * @throws AppError
	 * 
	 */
	public ArrayList fetchModSecGroup(HashMap hmParam) throws AppError {
		ArrayList alUserList = new ArrayList();
		String strGrpType = GmCommonClass.parseNull((String)hmParam.get("GRPTYPE"));
		String strFilterTxt = GmCommonClass.parseNull((String)hmParam.get("FILTERDATA"));
		GmDBManager gmDBManager = new GmDBManager();		
		gmDBManager.setPrepareString("gm_pkg_it_grp_user_map.gm_fch_grp_list",3);
		gmDBManager.setString(1, strGrpType);
		gmDBManager.setString(2, strFilterTxt);
        gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
        gmDBManager.execute();
        alUserList = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3)));
		log.debug("alUserList   "+alUserList);
		gmDBManager.close();
		return alUserList;
	}
	
	public ArrayList fetchFunction() throws AppError {

		ArrayList alFuncList = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();		
		gmDBManager.setPrepareString("gm_pkg_it_grp_user_map.gm_it_fch_fndetails",1);
		gmDBManager.registerOutParameter(1,OracleTypes.CURSOR);
        gmDBManager.execute();
        alFuncList = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(1)));
        gmDBManager.close();
		return alFuncList;
	}
	
	
	public HashMap fetchFnGrpDetails(HashMap hmParam) throws AppError {
		
		HashMap hmGroup = new HashMap();
		hmGroup.put("FID", "");
		hmGroup.put("ACCESSID", "");
		hmGroup.put("GROUPID", "");
		hmGroup.put("GROUPNM", "");
		hmGroup.put("GROUPDESC", "");
		hmGroup.put("ACCESSID", "");
		hmGroup.put("SEQNO", "");
		hmGroup.put("READFL", "");
		hmGroup.put("UPDATEFL", "");
		hmGroup.put("VOIDFL", "");
		hmGroup.put("CREATBY", "");
		ArrayList alGroup = new ArrayList();
		//algroup.add(goup);
		HashMap hmResult = new HashMap();
		String strGrpTypeId="";
		GmDBManager gmDBManager = new GmDBManager();	
		String grpType=GmCommonClass.parseNull((String)hmParam.get("GRPTYPE"));
		if(grpType.equals("TSMD")){
			strGrpTypeId="92265";
		}else if(grpType.equals("TSSG")||grpType.equals("SESG")){
			strGrpTypeId="92264";
		}
		gmDBManager.setPrepareString("gm_pkg_it_grp_user_map.gm_it_fch_fngrpdetails",4);
		gmDBManager.setString(1, GmCommonClass.parseNull((String)hmParam.get("FUNCID")));
		gmDBManager.setString(2,strGrpTypeId);
		gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
        gmDBManager.execute();
        alGroup=GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(4)));
        if(alGroup.isEmpty()){
        	alGroup.add(hmGroup);
        }
        hmResult.put("FUNCTION", GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3))));
        hmResult.put("GROUP",alGroup);//GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(4))));
        gmDBManager.close();
		return hmResult;
	}
	
	public void saveGrpDetails(HashMap hmParam) throws AppError {

		
		GmDBManager gmDBManager = new GmDBManager();		
		gmDBManager.setPrepareString("gm_pkg_it_grp_user_map.gm_it_sav_grpdetails",2);
		gmDBManager.setString(1, GmCommonClass.parseNull((String)hmParam.get("HINPUTSTR")));
		gmDBManager.setString(2, GmCommonClass.parseNull((String)hmParam.get("USERID")));
		gmDBManager.execute();
		gmDBManager.commit();     	
	}
	
	 /**
     * getXmlGridData - This method will return the Xml String
     * @param hmXmlParams - contains the Template and Template path 
     * @return String XML String
     * @exception AppError
     */
	 public String getXmlGridData(HashMap hmXmlParams,String strStatus) throws AppError {
 		
 		GmTemplateUtil templateUtil = new GmTemplateUtil();
 		templateUtil.setDataList("alResult", (ArrayList)hmXmlParams.get("ALRESULT"));
 		templateUtil.setDataList("alReason", (ArrayList)hmXmlParams.get("ALREASON"));
 		templateUtil.setDataList("alTaskType", (ArrayList)hmXmlParams.get("ALTASKTYPE"));
 		templateUtil.setDataMap("hmOpt",(HashMap)hmXmlParams.get("HMOPT"));
 		if(strStatus.equals("ADD"))
 		{
 		templateUtil.setDataList("alStatus", (ArrayList)hmXmlParams.get("ALSTATUS"));
 		templateUtil.setDataMap("hmSeqStatus" ,(HashMap)hmXmlParams.get("HMSEQSTATUS"));
 		}
 		templateUtil.setTemplateName((String)hmXmlParams.get("TEMPLATE"));
 		templateUtil.setTemplateSubDir((String)hmXmlParams.get("TEMPLATEPATH"));
 		return templateUtil.generateOutput();
 	}
	 
	 /** @Description This procedure will fetch all the function list 
	  * @author Rajkumar
	  * @return {@link ArrayList} 
	  * 
	  */
	 	
	 	public ArrayList fetchFunctionList(String strType) throws AppError {

	 		GmDBManager gmDBManager = new GmDBManager();
	 		ArrayList alReturn = new ArrayList();
	 		gmDBManager.setPrepareString("gm_pkg_it_grp_user_map.gm_fch_function_list", 2);
	 		gmDBManager.setString(1, strType);
	 		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	 		gmDBManager.execute();
	 		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
	 		gmDBManager.close();
	 		return alReturn;
	 		
	 	}
	 	/** @Description Procedure to fetch Group Name 
		  * @author Rajkumar
		  * @return {@link String} 
		  * 
		  */
	 	public HashMap fetchGroupDetail(String strGrpId)throws AppError{
	 		StringBuffer sbQuery = new StringBuffer();
	 		GmDBManager gmDBManager = new GmDBManager();
			sbQuery.append(" SELECT gm_pkg_it_user.get_group_nm('" + strGrpId + "') GRPNM, gm_pkg_it_user.get_group_desc('" + strGrpId + "') GRPDESC ");
			sbQuery.append(" FROM DUAL ");
			HashMap hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
	 		return hmResult;
	 	}

		 /** @Description Procedure to fetch the Task ans Security Events for Security Group 
		  * @author Rajkumar
		  * @return {@link ArrayList} 
		  * 
		  */
	 	public ArrayList fetchSecGrp(HashMap hmParams) throws AppError {
	 		GmDBManager gmDBManager = new GmDBManager();
	 		String strGrpId=GmCommonClass.parseNull((String)hmParams.get("GRPID"));
	 		String strGrpType=GmCommonClass.parseNull((String)hmParams.get("GRPTYPE"));
	 		ArrayList alReturn = new ArrayList();
	 		gmDBManager.setPrepareString("gm_pkg_it_grp_user_map.gm_fch_sec_group_map", 3);
	 		gmDBManager.setString(1, strGrpId);
	 		gmDBManager.setString(2, strGrpType);
	 		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
	 		gmDBManager.execute();
	 		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
	 		gmDBManager.close();
	 		return alReturn;
	 	}
	 	
		 /** @Description Procedure to save the Task, Security Event to Security Group Details 
		  * @author Rajkumar
		  * @return {@link Void} 
		  * 
		  */
	 	public void saveTaskDetails(HashMap hmParam) throws AppError{
	 		GmDBManager gmDBManager = new GmDBManager();		
			gmDBManager.setPrepareString("gm_pkg_it_grp_user_map.gm_sav_task_details",5);
			
			String strVoidStr = GmCommonClass.parseNull((String)hmParam.get("HVOIDSTR"));
			String strTaskName = GmCommonClass.parseNull((String)hmParam.get("HTASKNAMESTR"));
			String strTaskDesc = GmCommonClass.parseNull((String)hmParam.get("HTASKDESCSTR"));
			String strTaskType = GmCommonClass.parseNull((String)hmParam.get("HTASKTYPESTR"));
			String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
			gmDBManager.setString(1, strVoidStr);
			gmDBManager.setString(2, strTaskName);
			gmDBManager.setString(3, strTaskDesc);
			gmDBManager.setString(4, strTaskType);
			gmDBManager.setString(5, strUserId);
			gmDBManager.execute();
			gmDBManager.commit();  
	 	}
	 	
	 	/** @Description Procedure to fetch the Security Events for Security Group 
		* @author Jignesh
		* @return {@link ArrayList} 
		* 
		*/
	 	public ArrayList fetchSecEvent(HashMap hmParams) throws AppError {
	 		GmDBManager gmDBManager = new GmDBManager();
            ArrayList alReturn = new ArrayList();
	 		String strGrpId=GmCommonClass.parseNull((String)hmParams.get("GRPID"));
	 		String strGrpType=GmCommonClass.parseNull((String)hmParams.get("GRPTYPE"));
	 			 		
	 		gmDBManager.setPrepareString("gm_pkg_it_grp_user_map.gm_fch_sec_event_map", 3);
	 		gmDBManager.setString(1, strGrpId);
	 		gmDBManager.setString(2, strGrpType);
	 		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
	 		gmDBManager.execute();
	 		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
	 		gmDBManager.close();
	 		return alReturn;
	 	}
}
