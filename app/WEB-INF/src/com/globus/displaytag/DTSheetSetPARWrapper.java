package com.globus.displaytag;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;

public class DTSheetSetPARWrapper extends TableDecorator{
    private String strPar;
    private String strSetID;
    private String strDmdMasterID;
    private String strType = "";
    private String strHistory = "";
    private DynaBean db ;
    private int intCount = 0;
    private String strDtlID = "";
    private String strComments = "";
    public StringBuffer  strValue = new StringBuffer();
    String strImagePath = GmCommonClass.getString("GMIMAGES");
    
    /**
     * Returns the PAR value with Text Filed and History Icon.
     * @return String
     */
    public String getPARVALUE(){
        db =    (DynaBean) this.getCurrentRowObject();
        strHistory = String.valueOf(db.get("HISTORY_FL"));       
        strValue.setLength(0);
        strPar = String.valueOf(db.get("PARVALUE"));
        strPar = strPar.equals("null")?"":strPar;
        strHistory = String.valueOf(db.get("HISTORY_FL"));
        strDtlID   = String.valueOf(db.get("DTLID"));
        strSetID   = (String)db.get("SETID");        
        strValue.setLength(0);
        strValue.append("<input style='text-align:center' type = text name = parvalue");
        strValue.append(String.valueOf(intCount) );
        strValue.append(" align = center size = 7  maxlength = 2 value='");
        strValue.append(strPar);
        strValue.append("' onBlur ='fnPARValidate(this)'>");  
        strValue.append("<input type = 'hidden' name = setid");
        strValue.append(String.valueOf(intCount) );
        strValue.append(" value='");
        strValue.append(strSetID);
        strValue.append("'> ");
        strValue.append("<input type=hidden name ='hPARVal");
		strValue.append(intCount);
		strValue.append("' value='");
		strValue.append(strPar);
		strValue.append("'> ");
        
        if (strHistory.equals("Y"))  {
        	strValue.append(" <img style='cursor:hand' src='/images/icon_History.gif'" +"border='0'" + " onClick=\"fnParHistory('"+ strDtlID +"','" + 1005 + "') \" />" ); 
        }
        intCount++;
        return strValue.toString();        
    }    
    
    /**
     * Returns the Image for Comments Column.
     * @return String
     */
    public String getCOMMENTS(){
        db =    (DynaBean) this.getCurrentRowObject();
        strSetID   = (String)db.get("SETID");
        strDmdMasterID = (String)db.get("DMDMASTERID");
        strType = db.get("RLOG").toString();
        strSetID = strSetID + "-" + strDmdMasterID;
        if (strType.equals("Y")){
            return "<img id='imgEditA' style='cursor:hand' src='/images/phone-icon_ans.gif'" +  
            " width='20' height='17'" + 
                "onClick=\"javascript:fnLog( '"+4000115+"','" + strSetID + "') \" />"; 
        }else{
            return "<img id='imgEditA' style='cursor:hand' src='/images/phone_icon.jpg'" +  
            " width='20' height='17' " + 
                "onClick=\"javascript:fnLog( '"+4000115+"','" + strSetID + "') \" />";
        }
    }
    
    /**
     * Returns the string formatted Set ID while downloading the report to Excel.
     * @return String
     */
    public String getEXCELSETID(){
        db = (DynaBean) this.getCurrentRowObject();
        strSetID   = (String)db.get("SETID");   
        strValue.setLength(0);
        strValue.append("=\""+strSetID+"\"");
        return strValue.toString();
    }  
    
    /**
     * Returns the only PAR value without Textbox and History Icon while downloading the report to Excel. .
     * @return String
     */
    public String getEXCELPAR(){
        db     =   (DynaBean) this.getCurrentRowObject();
        strPar = String.valueOf(db.get("PARVALUE"));
        return strPar;
    }
    
    /**
     * Returns Empty String while downloading the report to Excel. .
     * @return String
     */
    /*
    public String getEXCELCOMMENTS(){
        db = (DynaBean) this.getCurrentRowObject();
        strComments = "";
        return strComments;
    }
    */
}


