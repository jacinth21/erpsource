package com.globus.displaytag.beans.dashboard;

import java.util.HashMap;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;

public class DTConsignedSetsItemsProcessingWrapper extends TableDecorator{
	private HashMap db;
	private String strSHIPREQFL;
	private String strCONSFL;
	private String strCID;

	/*
	  public String getCONSFL() { 
		  
		db = (HashMap) this.getCurrentRowObject();   
		
		strSHIPREQFL = (String)db.get("SHIPREQFL");
		strCONSFL = (String)db.get("CONSFL");
		strCID =(String) db.get("CID"); 
		String strCntrlFl = strCONSFL.equals("Y")?"<a class=RightTextRed alt='Click to Consign this Set' href= javascript:fnCallEditItems('"+strCID+"');>Y</a>":"--"; 
		return strCntrlFl; 
	  
	  }
	  
	  */
/*
	public String getSHIPREQFL() {


		db = (HashMap) this.getCurrentRowObject();   
		strSHIPREQFL = (String)db.get("SHIPREQFL");
		strCONSFL = (String)db.get("CONSFL");
		strCID =(String) db.get("CID"); 
		String strShipFl;
		if (strSHIPREQFL.equals("1")) {
			strShipFl = strCONSFL.equals("Y")?"<a class=RightTextRed>Y</a>":"<a class=RightTextRed href= javascript:fnCallSetShip('"+strCID+"');>Y</a>";
		} else {
			strShipFl = "--";
		}
		return strShipFl;
	}
	*/
	public String getCID() { 

		db = 	(HashMap) this.getCurrentRowObject();
    	strCID = (String) db.get("CID");
    	String strConsignType = (String)db.get("CTYPE");
    	String strImagePath = GmCommonClass.getString("GMIMAGES");
    	strCONSFL = (String)db.get("CONSFL");
    	StringBuffer strBufVal = new StringBuffer();
					        
		strBufVal.append("<img style='cursor:hand' src='");
   		strBufVal.append(strImagePath+"/packslip.gif'");
   		strBufVal.append(" title='Click to View ' width='12' height='17' id ='" +strCID+ "','"+strConsignType+"'"); 
	   	strBufVal.append(" onClick =fnPicSlip('"+strCID+"','"+strConsignType+"');> </img> ");
	   	strBufVal.append("<img style='cursor:hand' src='");
	   	strBufVal.append(strImagePath+"/detail_icon.gif'");
	   	strBufVal.append(" title='Click to add comments ' width='12' height='17' id ='" +strCID+"' "); 
		strBufVal.append(" onClick =fnOpenOrdLogcsg('" + strCID + "');> </img> ");
		if (strCONSFL.equals("Y")) {
			strBufVal.append(" <a class=RightTextRed href= javascript:fnCallEditItems('"+strCID+"');>");		
		}		
		strBufVal.append(strCID);
		if (strCONSFL.equals("Y")) {
			strBufVal.append("</a>");
		}
    	return strBufVal.toString();
    	
	  }
	public String getSNAME() { 
	
		db = (HashMap) this.getCurrentRowObject();   
		
		strCID =GmCommonClass.parseNull((String)db.get("SNAME")); 
		String strCntrlFl =strCID.equals("")?"Item Consignment":GmCommonClass.getStringWithTM(strCID);
		return strCntrlFl;
	  }
	

}
