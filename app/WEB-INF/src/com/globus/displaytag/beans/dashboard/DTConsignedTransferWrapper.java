/**
 * FileName    : DTConsignedTransferWrapper.java 
 * Description :
 * Author      : tchandure
 * Date        : Jun 11, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.displaytag.beans.dashboard;

import java.util.HashMap;

import org.displaytag.decorator.TableDecorator;



/**
 * @author tchandure
 *
 */
public class DTConsignedTransferWrapper  extends TableDecorator{
	private HashMap db;
	private String strTSFID;
	

	
	  public String getTSFID() { 
		  
		db = (HashMap) this.getCurrentRowObject();   
		
		strTSFID = (String)db.get("TSFID");
		
		String strCntrlFl = "<a class=RightText href= javascript:fnCallEditTransfer('"+strTSFID+"');>"+strTSFID; 
		return strCntrlFl; 
	  
	  }

	
	
}
