/*
 * Created on Apr 20, 2006
 * 
 * TODO To change the template for this generated file go to Window - Preferences - Java - Code
 * Style - Code Templates
 */
package com.globus.displaytag.beans.dashboard;



import java.util.Date;
import java.util.HashMap;

import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.valueobject.common.GmDataStoreVO;

public class DTDHRDashboardWrapper extends TableDecorator {


  private HashMap db;
  private String strID;
  private String strFlag;
  private String strBackOrderFl;
  private String strForeColor;

  /**
   * Creates a new Wrapper decorator who's job is to reformat some of the data located in our
   * TestObject's.
   */

  
  public String getFL() {

    db = (HashMap) this.getCurrentRowObject();
    strFlag = (String) db.get("FL");
    strID = (String) db.get("ID");
    String strControlFl = null;
    int intConCount = 0;
    int intVeriCount = 0;
    int intPackCount = 0;
    int intInspCount = 0;
    if (strFlag.equals("0")) {
      strControlFl =
          "<a class=RightTextRed href= javascript:fnUpdateDHR('" + strID + "','C');>Y</a>";
      intConCount++;
    } else if (strFlag.equals("1")) {
      strControlFl = "-";
      intInspCount++;
    } else if (strFlag.equals("2")) {
      strControlFl = "-";
      intPackCount++;
    } else if (strFlag.equals("3")) {
      strControlFl = "-";
      intVeriCount++;
    }

    return strControlFl;
  }

  public String getVID() {

    db = (HashMap) this.getCurrentRowObject();
    strFlag = (String) db.get("FL");
    strID = (String) db.get("ID");
    String strInspFl = null;
    int intConCount = 0;
    int intVeriCount = 0;
    int intPackCount = 0;
    int intInspCount = 0;
    if (strFlag.equals("0")) {
      strInspFl = "-";
      intConCount++;
    } else if (strFlag.equals("1")) {
      strInspFl = "<a class=RightTextRed href= javascript:fnUpdateDHR('" + strID + "','I');>Y</a>";
      intInspCount++;
    } else if (strFlag.equals("2")) {
      strInspFl = "-";
      intPackCount++;
    } else if (strFlag.equals("3")) {
      strInspFl = "-";
      intVeriCount++;
    }

    return strInspFl;
  }

  public String getCNUM() {

    db = (HashMap) this.getCurrentRowObject();
    strFlag = (String) db.get("FL");
    strID = (String) db.get("ID");
    String strPackFl = null;
    int intConCount = 0;
    int intVeriCount = 0;
    int intPackCount = 0;
    int intInspCount = 0;
    if (strFlag.equals("0")) {
      strPackFl = "-";
      intConCount++;
    } else if (strFlag.equals("1")) {
      strPackFl = "-";
      intInspCount++;
    } else if (strFlag.equals("2")) {
      strPackFl = "<a class=RightTextRed href= javascript:fnUpdateDHR('" + strID + "','P');>Y</a>";
      intPackCount++;
    } else if (strFlag.equals("3")) {
      strPackFl = "-";
      intVeriCount++;
    }

    return strPackFl;
  }

  public String getLDATE() {

    db = (HashMap) this.getCurrentRowObject();
    strFlag = (String) db.get("FL");
    strID = (String) db.get("ID");
    String strVeriFl = null;
    int intConCount = 0;
    int intVeriCount = 0;
    int intPackCount = 0;
    int intInspCount = 0;
    if (strFlag.equals("0")) {
      strVeriFl = "-";
      intConCount++;
    } else if (strFlag.equals("1")) {
      strVeriFl = "-";
      intInspCount++;
    } else if (strFlag.equals("2")) {
      strVeriFl = "-";
      intPackCount++;
    } else if (strFlag.equals("3")) {
      strVeriFl = "<a class=RightTextRed href= javascript:fnUpdateDHR('" + strID + "','V');>Y</a>";
      intVeriCount++;
    }

    return strVeriFl;
  }

  public String getVNAME() {

    db = (HashMap) this.getCurrentRowObject();
    String strVname = "";
   strBackOrderFl = (String) db.get("DHR_PRIORITY");
    if (strBackOrderFl.trim().equals("0")) {
      strForeColor = "<span style=\"color:red;\">";
    } else {
      strForeColor = "";
    }
    
   strVname = strForeColor;
   
    strVname += (String) db.get("VNAME") + "</span>";

    return strVname;

  }
  
  public String getDHRPRIORITY() {

	    db = (HashMap) this.getCurrentRowObject();
	    String strDHRPriority = "";
	    strDHRPriority = strForeColor;
	    strDHRPriority += (String) db.get("DHR_PRIORITY") + "</span>";

	    return strDHRPriority;

	  }

  public String getID() {

    db = (HashMap) this.getCurrentRowObject();
    String strID = "";
    String strClogFlag = "";
    String strImgSrc = "";
    StringBuffer strBufVal = new StringBuffer();
    strBackOrderFl = (String) db.get("DHR_PRIORITY");
    if (strBackOrderFl.trim().equals("0")) {
      strForeColor = "<span style=\"color:red;\">";
    } else {
      strForeColor = "<span>";
    }
   
    strID = (String) db.get("ID");
    strClogFlag = (String) db.get("CLOG");
    if (strBackOrderFl.trim().equals("0")) {
        strForeColor = "<span style=\"color:red;\">";
      } else {
        strForeColor = "<span>";
      }
    if (strClogFlag.equals("N")) {
      strImgSrc = "/images/phone_icon.jpg";
    } else {
      strImgSrc = "/images/phone-icon_ans.gif";
    }
    strBufVal.append("<a href=javascript:fnOpenTransferLog('" + strID
        + "')> <img id=imgEdit style=cursor:hand src =" + strImgSrc
        + " title=Click to add comments width=17 height=12/> </a>");
    strBufVal.append("</span>");

    strBufVal.append(strID);
    return strBufVal.toString();
  }

  public String getPARTNUM() {

    db = (HashMap) this.getCurrentRowObject();
    String strPARTNUM = "";
   strBackOrderFl = (String) db.get("DHR_PRIORITY");
    if (strBackOrderFl.trim().equals("0")) {
      strForeColor = "<span style=\"color:red;\">";
    } else {
      strForeColor = "";
    }
 
    strPARTNUM = strForeColor;
  
    strPARTNUM += (String) db.get("PARTNUM") + "</span>";

    return strPARTNUM;
  }

  public String getPDESC() {

    db = (HashMap) this.getCurrentRowObject();
    String strPDESC = "";
    strBackOrderFl = (String) db.get("DHR_PRIORITY");
    if (strBackOrderFl.trim().equals("0")) {
      strForeColor = "<span style=\"color:red;\">";
    } else {
      strForeColor = "";
    }
  
    strPDESC = strForeColor;
   
    strPDESC += (String) db.get("PDESC") + "</span>";

    return strPDESC;
  }

  public String getCDATEDIS() {
    GmDataStoreVO gmDataStoreVO =
        (GmDataStoreVO) getPageContext().getRequest().getAttribute("gmDataStoreVO");
    gmDataStoreVO =
        ((gmDataStoreVO == null) || ((gmDataStoreVO.getCmpdfmt()).equals(""))) ? GmCommonClass
            .getDefaultGmDataStoreVO() : gmDataStoreVO;

    db = (HashMap) this.getCurrentRowObject();
    String strCDATE = "";
    strBackOrderFl = (String) db.get("DHR_PRIORITY");
    if (strBackOrderFl.trim().equals("0")) {
      strForeColor = "<span style=\"color:red;\">";
    } else {
      strForeColor = "";
    }
   
   strCDATE = strForeColor;
 
    strCDATE +=
        GmCommonClass.getStringFromDate((Date) db.get("CDATE"), gmDataStoreVO.getCmpdfmt())
            + "</span>";

    return strCDATE;
  }

  public String getQTY() {

    db = (HashMap) this.getCurrentRowObject();
    String strQTY = "";
    strBackOrderFl = (String) db.get("DHR_PRIORITY");
    if (strBackOrderFl.trim().equals("Y")) {
      strForeColor = "<span style=\"color:red;\">";
    } else {
      strForeColor = "";
    }
 
 //   strQTY = strForeColor;
    strQTY += (String) db.get("QTY") + "</span>";

    return strQTY;
  }
}
