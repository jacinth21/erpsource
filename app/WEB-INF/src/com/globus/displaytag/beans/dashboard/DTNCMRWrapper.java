/**
 * FileName    : DTNCMRWrapper.java 
 * Description :
 * Author      : tchandure
 * Date        : Jun 17, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.displaytag.beans.dashboard;

import java.util.HashMap;

import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;

/**
 * @author tchandure
 *
 */
public class DTNCMRWrapper extends TableDecorator{
	private HashMap db;
    private String strID;
    private String strFlag;
    private String strEvalID;
    	    	  
    /**
     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
     */
    
    public String getNID()
    {

    	db = 	(HashMap) this.getCurrentRowObject();
    	
    	strID = (String) db.get("NID");
    	
    	String strInspFl = null;
    	if(strID.toString().startsWith("GM-EVAL"))
    	{
    		strInspFl="<A class=RightText  href=javascript:fnShowEVAL('"+strID+"')>"+strID+"</a>";
    	}else{
    	strInspFl="<A class=RightText  href=javascript:fnShowNCMR('"+strID+"')>"+strID+"</a>";
    	}
    	return strInspFl;
    }
    

    public String getSFL()
    {

    	db = 	(HashMap) this.getCurrentRowObject();
    	strFlag = (String) db.get("SFL");
    	strID = (String) db.get("NID");
    	String strInspFl = null;
		
		int intPendCnt = 0;
		int intCloseCnt = 0;
		if (strFlag.equals("1"))
		{
			strInspFl = "<a class=RightTextRed href= javascript:fnUpdateNCMR('"+strID+"','A');>Y</a>";
			intPendCnt++;
		}
		else if (strFlag.equals("2"))
		{
			strInspFl = "-";
			intCloseCnt++;
		}
    	return strInspFl;
    }
    public String getVID()
    {

    	db = 	(HashMap) this.getCurrentRowObject();
    	strFlag = (String) db.get("SFL");
    	strID = (String) db.get("NID");

		String strPackFl = null;
		int intPendCnt = 0;
		int intCloseCnt = 0;
		if (strFlag.equals("1"))
		{
			strPackFl = "";
			intPendCnt++;
		}
		else if (strFlag.equals("2"))
		{
			strPackFl = "<a class=RightTextRed href= javascript:fnUpdateNCMR('"+strID+"','C');>Y</a>";
			intCloseCnt++;
		}
    	return strPackFl;
    }
    public String getEID()
    {

    	db = 	(HashMap) this.getCurrentRowObject();
    	strFlag = (String) db.get("SFL");
    	strEvalID = (String) db.get("NID");
    	String strEvalFl = null;
		
		 
		if (strFlag.equals("0"))
		{
			strEvalFl = "<a class=RightTextRed href= javascript:fnUpdateNCMREval('"+ strEvalID +"','E');>Y</a>";
		}
		else if (strFlag.equals("1"))
		{
			strEvalFl = "-";
		}
			
		else if (strFlag.equals("2"))
		{
			strEvalFl = "-";
		}

    	return strEvalFl;
    }
    
    public String getTICKETID()
    {
    	
    	db = (HashMap) this.getCurrentRowObject();
    	
    	strID = (String) db.get("TICKETID");
    	
    	String strValue = null;
    	String strTicketUrl = GmCommonClass.parseNull(GmCommonClass.getJiraConfig("TICKETS_URL"));
    	strTicketUrl = strTicketUrl + strID;
    
    	strValue="<A class=RightText href='"+strTicketUrl+"' target=_blank>"+strID+"</a>";
    	
    	return strValue;
    }

}
