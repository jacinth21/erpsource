/**
 * FileName    : DTWIPOrdersWrapper.java 
 * Description :
 * Author      : tchandure
 * Date        : Jun 10, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.displaytag.beans.dashboard;


import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;


/**
 * @author tchandure
 * 
 */
public class DTWIPOrdersWrapper extends TableDecorator {

    private DynaBean db;
	private String strCFL;
	private String strSFL;
	private String strID;
	private String strTOTAL;
	
	public String getID() { 
		  
		db = (DynaBean) this.getCurrentRowObject();   
		String strImagePath = GmCommonClass.getString("GMIMAGES");
		strCFL =(String) db.get("CFL");
		strID =(String) db.get("ID"); 
		String strCntrlFl = "<a href=javascript:fnPrintPick('"+strID+"');><img src="+strImagePath+"/packslip.gif border=0></a>&nbsp"
		+"<a href=javascript:fnViewOrder('"+strID+"');><img src="+strImagePath+"/ordsum.gif border=0></a>&nbsp"+ "<a class=RightTextRed href=javascript:fnCallControl('"+strID+"');>"+strID+"</a>"; 
		return strCntrlFl; 
	  
	  }
	public String getTOTAL() { 
		  
		db = (DynaBean) this.getCurrentRowObject();   
		
		strTOTAL =(String) db.get("TOTAL"); 
		String strCntrlFl = "$ "+ GmCommonClass.getStringWithCommas(strTOTAL); 
		return strCntrlFl; 
	  
	  }
	
	/*  public String getCFL() { 
		  
		db = (DynaBean) this.getCurrentRowObject();   
		strCFL =(String) db.get("CFL");
		strID =(String) db.get("ID"); 
		String strCntrlFl = strCFL.equals("Y")?"<a class=RightTextRed href=javascript:fnCallControl('"+strID+"');>Y</a>":"--"; 
		return strCntrlFl; 
	  
	  } 

	 public String getSFL() {


		db = (DynaBean) this.getCurrentRowObject();
		strCFL = (String) db.get("CFL");
		strSFL = ((String) db.get("SFL"));
		strID = (String) db.get("ID");
		String strShipFl;
		if (strCFL.equals("Y")) {
			strShipFl = "<a class=RightTextRed>Y</a>";
		} else {
			strShipFl = strSFL.equals("Y") ? "<a class=RightTextRed href= javascript:fnCallShip('" + strID + "');>Y</a>" : "--";
		}
		return strShipFl;
	}*/

}
