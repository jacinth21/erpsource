package com.globus.displaytag.beans.dashboard;

import java.util.HashMap;

import org.displaytag.decorator.TotalTableDecorator;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;

public class DTConsignedItemsWrapper extends TotalTableDecorator{
	private DynaBean db;

    private String strCID;
    private String strConsignType;
    private int iCType;
    private String strCONTROLFL;
    private String strSHIPREQFL;
    private String strRefId;
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
    /**
     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
     */
    public String getCID()
    {
    	String strInspFl = null;
    
    	db = 	(DynaBean) this.getCurrentRowObject();
    	strCID = (String) db.get("CID");
    	iCType = Integer.parseInt((db.get("CTYPE")).toString());	
    	strRefId = (String) db.get("TXNID");
    	String strImagePath = GmCommonClass.getString("GMIMAGES");
    	strInspFl = "<a href=javascript:fnPicSlip('"+strCID+"','"+iCType+"','"+strRefId+"');><img src="+strImagePath+"/packslip.gif border=0></a> "+strCID;

    	return strInspFl;
    }
    public String getCONTROLFL()
    {
    	String strCntrlFl = null;

    	db = 	(DynaBean) this.getCurrentRowObject();
    	strCONTROLFL = (String) db.get("CONTROLFL");
    	strSHIPREQFL = (String)db.get("SHIPREQFL");
    	strCID = (String) db.get("CID");
    	iCType = Integer.parseInt((db.get("CTYPE")).toString());		
    
	String strRedTextShip = "<a class=RightTextRed href= javascript:fnCallItemsShip('"+strCID+"','"+iCType+"');>Y</a>";
	String strRedTextShipNoLink = "<a class=RightTextRed>Y</a>";
	String strRedTextControl = "<a class=RightTextRed href= javascript:fnCallEditItems('"+strCID+"','"+iCType+"');>Y</a>";

	String strShipFl = strSHIPREQFL.equals("1")?strRedTextShip:"--";
	if (strSHIPREQFL.equals("1"))
	{
		strShipFl = strCONTROLFL.equals("Y")?strRedTextShipNoLink:strRedTextShip;
	}
	else
	{
		strShipFl = "--";
	}
	strCntrlFl = strCONTROLFL.equals("Y")?strRedTextControl:"--";
    	
	return strCntrlFl;
    }
    
    public String getSHIPREQFL()
    {
    	String strShipFl = null;
    	db = 	(DynaBean) this.getCurrentRowObject();
    	strCONTROLFL = (String) db.get("CONTROLFL");
    	strSHIPREQFL = (String)db.get("SHIPREQFL");
    	strCID = (String) db.get("CID");
    	iCType = Integer.parseInt((db.get("CTYPE")).toString());	
    
	String strRedTextShip = "<a class=RightTextRed href= javascript:fnCallItemsShip('"+strCID+"','"+iCType+"');>Y</a>";
	String strRedTextShipNoLink = "<a class=RightTextRed>Y</a>";
	String strRedTextControl = "<a class=RightTextRed href= javascript:fnCallEditItems('"+strCID+"','"+iCType+"');>Y</a>";

	strShipFl = strSHIPREQFL.equals("1")?strRedTextShip:"--";
	if (strSHIPREQFL.equals("1"))
	{
		strShipFl = strCONTROLFL.equals("Y")?strRedTextShipNoLink:strRedTextShip;
	}
	else
	{
		strShipFl = "--";
	}
	String strCntrlFl = strCONTROLFL.equals("Y")?strRedTextControl:"--";
    
	return strShipFl;
    }
}
