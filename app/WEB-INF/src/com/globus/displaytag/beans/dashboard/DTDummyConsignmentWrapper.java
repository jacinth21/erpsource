/**
 * FileName    : DTDummyConsignmentWrapper.java 
 * Description :
 * Author      : tchandure
 * Date        : Jun 12, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.displaytag.beans.dashboard;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;


/**
 * @author tchandure
 *
 */
public class DTDummyConsignmentWrapper extends TableDecorator {
	 private DynaBean db;
		private String strCONDID;

		
		public String getCONDID() { 
			  
			db = (DynaBean) this.getCurrentRowObject();   
			strCONDID =(String) db.get("CONDID"); 
			String strCntrlFl = "<a class=RightText href= javascript:fnCallEditDummyConsignment('"+strCONDID+"');>"+strCONDID; 
			return strCntrlFl; 
		  
		  }
		
}
