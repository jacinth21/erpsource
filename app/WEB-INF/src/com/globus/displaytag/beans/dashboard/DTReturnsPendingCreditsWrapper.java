/**
 * FileName    : DTReturnsPendingCreditsWrapper.java 
 * Description :
 * Author      : tchandure
 * Date        : Jun 16, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.displaytag.beans.dashboard;

import java.util.HashMap;
import com.globus.common.beans.GmCommonClass;

import org.displaytag.decorator.TableDecorator;

/**
 * @author tchandure
 *
 */
public class DTReturnsPendingCreditsWrapper extends TableDecorator{
	private HashMap db;
	private String strRAId;
	private String strTYPE;
	private String strSTATUS_FL;

	
	  public String getRAID() { 
		  
		db = (HashMap) this.getCurrentRowObject();   
		
		strRAId = (String)db.get("RAID");

		String strCntrlFl = "<a class=RightText href= javascript:fnViewReturns('"+strRAId+"');>"+strRAId+"</a>"; 
		return strCntrlFl; 
	  
	  }
	  public String getTYPEID() { 
		  db = (HashMap) this.getCurrentRowObject();  
		  String strCntrlFl;
		  strRAId = (String)db.get("RAID");
		  strTYPE = (String)db.get("TYPEID");
		  String strStatus = GmCommonClass.parseNull((String)db.get("STATUS_FL"));
	  if (strStatus.equals("Y")) 
		{
		  strCntrlFl=strTYPE;
		  strCntrlFl = "<a class=RightText href= javascript:fnCallReturn('"+strRAId+"');>Y</a>";	
		}else{
		  strCntrlFl = "&nbsp;-";					
		}
	  return strCntrlFl;
	  }
	  public String getSTATUS_FL() { 
		  db = (HashMap) this.getCurrentRowObject();  
		  strRAId = (String)db.get("RAID");
		  String strCntrlFl;
		  String strStatus = GmCommonClass.parseNull((String)db.get("STATUS_FL"));
	  if (strStatus.equals("Y")) 
		{
		  strCntrlFl = "&nbsp;-";	
		}else{
		  strCntrlFl = "<a class=RightText href= javascript:fnCallEditReturn('"+strRAId+"');>Y</a>";					
		}
	  return strCntrlFl;
	  }
}
