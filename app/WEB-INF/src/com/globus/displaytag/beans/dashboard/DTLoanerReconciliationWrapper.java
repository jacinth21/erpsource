/**
 * FileName    : DTLoanerReconciliationWrapper.java 
 * Description :
 * Author      : tchandure
 * Date        : Jun 11, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.displaytag.beans.dashboard;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonConstants;
import com.globus.common.beans.GmLogger;

/**
 * @author tchandure
 * 
 */
public class DTLoanerReconciliationWrapper extends TableDecorator {
	private DynaBean db;
	private String strINHID;
	private String strSFL;
	private String strConsFl = "";
	private int intCount = 0;
	Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);

	String strImagePath = GmCommonClass.getString("GMIMAGES");

	public String getINHID() {
		db = (DynaBean) this.getCurrentRowObject();
		strINHID = (String) db.get("INHID");
		String strProcessLog = (String) db.get("PROCESSLOG");
		String strCntrlFl = strINHID;
		if (!strINHID.equals("-")) {
			strCntrlFl += "<a class=RightText href=javascript:fnOpenLog('" + strINHID + "',1246);>";
			if (strProcessLog.equals("N")) {
				strCntrlFl += "<img src=" + strImagePath + "/phone_icon.jpg border=0 width=15 height=13 title='Click to add/view Comments'></a>&nbsp;";
			} else {
				strCntrlFl += "<img src=" + strImagePath + "/phone-icon_ans.gif border=0 width=15 height=13 title='Click to add/view Comments'></a>&nbsp;";
			}
			// strCntrlFl+=strINHID;
			strCntrlFl += "&nbsp;<a class=RightText href=javascript:fnInHousePicSlip('" + strINHID + "');><img src=" + strImagePath
					+ "/packslip.gif border=0></a>";

		}
		return strCntrlFl;
	}

	public String getCONSFL() {
		db = (DynaBean) this.getCurrentRowObject();
		String strCntrlFl = "";
		strINHID = (String) db.get("INHID");
		strConsFl = GmCommonClass.parseNull((String) db.get("CONSFL"));
		if (strConsFl.equals("Y")) {
			strCntrlFl = " <a class=RightTextRed href= javascript:fnCallRecon('" + strINHID + "');>" + strConsFl + "</a>";
		} else
			strCntrlFl = strConsFl;
		/*
		 * strSFL = GmCommonClass.parseNull((String) db.get("SFL")); if
		 * (!strINHID.equals("-") && strSFL.equals("3")){ strCntrlFl = " <a
		 * class=RightTextRed href= javascript:fnCallRecon('"+strINHID+"');>Y</a>"; }
		 * else if (strSFL.equals("4")){ strCntrlFl = "R"; }
		 */
		return strCntrlFl;
	}

	public String getETCHID() {
		db = (DynaBean) this.getCurrentRowObject();
		String strETCHID = String.valueOf(db.get("ETCHID"));
		String strLnTransId = String.valueOf(db.get("LNTRANSID"));
		String strAcceptlog = String.valueOf(db.get("ACCEPTLOG"));
		String strReturn = strETCHID;
		strReturn += " <a class=RightText href=javascript:fnOpenLog('" + strLnTransId + "',1245);>";
		if (strAcceptlog.equals("N")) {
			strReturn += "<img src=" + strImagePath + "/phone_icon.jpg border=0 width=15 height=13 title='Click to add/view Comments'></a>&nbsp;";
		} else {
			strReturn += "<img src=" + strImagePath + "/phone-icon_ans.gif border=0 width=15 height=13 title='Click to add/view Comments'></a>&nbsp;";
		}
		return strReturn;
	}

	public String getDAYSLATE() {
		db = (DynaBean) this.getCurrentRowObject();
		int strDays = Integer.parseInt(String.valueOf(db.get("DAYSLATE")));
		if (strDays < 0) {
			return String.valueOf(Math.abs(strDays));
		} else
			return null;
	}

	public String getREQID() {
		db = (DynaBean) this.getCurrentRowObject();
		StringBuffer strValue = new StringBuffer();
		int strReqId = Integer.parseInt(String.valueOf(db.get("REQID")));
		String strPendingReconFlg = String.valueOf(db.get("PENDINGRECON"));
		String strRequestLog = (String) db.get("REQUESTLOG");

		if (strReqId == 0) {
			return "";
		}
		int strReqDetId = Integer.parseInt(GmCommonClass.parseZero(String.valueOf(db.get("REQDETID"))));
		strValue.append(strReqId);
		if (strPendingReconFlg.equals("Y")) {
			strValue.append("<input type=checkbox value=");
			strValue.append(strReqDetId);
			strValue.append(" name='Chk_reqid");		
			strValue.append("'> ");
		}
		strValue.append(" <a class=RightText href=javascript:fnOpenLog('" + strReqId + "',1247);> ");
		if (strRequestLog.equals("N")) {
			strValue.append(" <img src=" + strImagePath + "/phone_icon.jpg border=0 width=15 height=13 title='Click to add/view Comments'></a>&nbsp;");
		} else {
			strValue.append(" <img src=" + strImagePath + "/phone-icon_ans.gif border=0 width=15 height=13 title='Click to add/view Comments'></a>&nbsp;");
		}
		// strValue.append(strReqId);
		intCount++;
		return strValue.toString();

	}

}
