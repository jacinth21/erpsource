package com.globus.displaytag.beans;

import java.text.DecimalFormat;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;

public class DTMADetailsWrapper extends TableDecorator {

  private DynaBean db;
  private DecimalFormat moneyFormat;
  private double dblCurVal;
  private String formatCurVal;

  public DTMADetailsWrapper() {
    super();
    this.moneyFormat = new DecimalFormat("#,###,###.00");
  }



  /**
   * Returns negative Amount as a red string in bracket.
   * 
   * @return String
   **/

  public String getEXTCOST() {
    db = (DynaBean) this.getCurrentRowObject();
    if (db.get("EXTCOST") == null)
      return "";
    else {
      dblCurVal = Double.parseDouble((db.get("EXTCOST")).toString());
      formatCurVal = moneyFormat.format(dblCurVal);

      return GmCommonClass.getRedText(formatCurVal);
    }
  }

  public String getMAQTY() {
    db = (DynaBean) this.getCurrentRowObject();
    if (db.get("MAQTY") == null)
      return "";
    else {
      return GmCommonClass.getRedText((db.get("MAQTY")).toString());
    }
  }


}
