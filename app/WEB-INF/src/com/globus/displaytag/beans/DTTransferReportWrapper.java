package com.globus.displaytag.beans;

/**
 * @author jkumar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */


import java.text.DecimalFormat;
import java.util.HashMap;

import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.TableDecorator;
import org.apache.commons.beanutils.DynaBean;

import com.globus.common.beans.GmLogger; 
import org.apache.log4j.Logger; 
import com.globus.common.beans.GmCommonClass;


public class DTTransferReportWrapper  extends TableDecorator{


     /**
     * DecimalFormat used to format money in getMoney().
     */
    private DynaBean db ;
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    
    
    /**
     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
     */
    public DTTransferReportWrapper()
    {
        super();
    }

    public String getTSFID()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        String strTransferId = (String) db.get("TSFID"); 

            
        /*
         * The transaction type can be either "Consignment" or "Return"
         * If the transaction is consignment, then a specific javascript needs to be called for showing the details of the consignment in a pop-up
         * If the transaction is return, then a specific javascript needs to be called for showing the details of the return in a pop-up
         */
       
        return "<a href=javascript:fnCallEditTransfer('" + strTransferId + "')> "  + strTransferId + "</a>" ;
    }
    
}
