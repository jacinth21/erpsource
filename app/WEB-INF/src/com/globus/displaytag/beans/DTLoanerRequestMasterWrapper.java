package com.globus.displaytag.beans;

import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.TableDecorator;
import org.apache.commons.beanutils.DynaBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmCommonClass;
import java.util.HashMap;

public class DTLoanerRequestMasterWrapper extends TableDecorator{
	
	HashMap hmSearchResult = new HashMap ();
	GmLogger log = GmLogger.getInstance(); // Gets an instance of Logger class
	
	public DTLoanerRequestMasterWrapper()
	    {
	        super();
	    	
	    }

	public String getPDTREQID()
	    {
		 	hmSearchResult = 	(HashMap) this.getCurrentRowObject();
			 
		 	StringBuffer strValue = new StringBuffer(); 
		 	String strReqId =  ((String)hmSearchResult.get("PDTREQID"));
		 	String strDistNm = ((String)hmSearchResult.get("REQFORID"));
		 	String strShipTo = ((String)hmSearchResult.get("SHIPTO"));
		 	String strReqDetId =  ((String)hmSearchResult.get("REQDETAILID"));
		 	String strShiptoid =  ((String)hmSearchResult.get("SHIPTOID"));
		 	String strShipcarr =  ((String)hmSearchResult.get("SHIPCARR"));
		 	String strShipmode =  ((String)hmSearchResult.get("SHIPMODE"));
		 	String strShipaddid =  ((String)hmSearchResult.get("SHIPADDID"));
	        
		 	// Code to add the checkbox value
			strValue.append("<input class=RightText type='radio' name='radPart' id='");
			strValue.append(strReqId );
			strValue.append("' value='") ;
			strValue.append(strReqId) ;
			strValue.append("' onClick='javascript:fnSelectPart(this");
			strValue.append(")' > ") ;
			strValue.append(strReqId) ;
			strValue.append("<input type='hidden' name='reqforid"+strReqId+"' value='");
			strValue.append(strDistNm);
			strValue.append("'> ");
			strValue.append("<input type='hidden' name='shipto"+strReqId+"' value='");
			strValue.append(strShipTo);
			strValue.append("'> ");
			strValue.append("<input type='hidden' name='reqdetid"+strReqId+"' value='");
			strValue.append(strReqDetId);
			strValue.append("'> ");
			strValue.append("<input type='hidden' name='shiptoid"+strReqId+"' value='");
			strValue.append(strShiptoid);
			strValue.append("'> ");
			strValue.append("<input type='hidden' name='shipcarr"+strReqId+"' value='");
			strValue.append(strShipcarr);
			strValue.append("'> ");
			strValue.append("<input type='hidden' name='shipmode"+strReqId+"' value='");
			strValue.append(strShipmode);
			strValue.append("'> ");
			strValue.append("<input type='hidden' name='shipaddid"+strReqId+"' value='");
			strValue.append(strShipaddid);
			strValue.append("'> ");
			
			
			
			//log.debug("String value which will be returned is " + strValue.toString());
			return strValue.toString();
			
	    }
	 
}
