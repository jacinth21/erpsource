package com.globus.displaytag.beans;

/**
     * @author richardk
     *
     * Ths bean is used for VirtualSetWrapped screen format and for hyperlinks
     * 
     */

    import org.displaytag.decorator.TableDecorator;
    import org.apache.commons.beanutils.DynaBean;
    import com.globus.common.beans.GmLogger;
    import org.apache.log4j.Logger;



    public class DTSalesDetailListWrapper  extends TableDecorator
    {
        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

        /**
         * DecimalFormat used to format money in getMoney().
         */
        private DynaBean db ;        
        private String strPartID ;        
        private String strTemp;
        
        /**
         * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
         */
        public DTSalesDetailListWrapper()
        {
            
           // super();
            log.debug(" DTConsignmentDetailListWrapper inside ");
         
        }

        public String getPNUM()
        {
            db =    (DynaBean) this.getCurrentRowObject();
            strPartID   = db.get("PNUM").toString();
            
           // log.debug("<a  href=\"javascript:fnViewPartActual( '" + strPartID + "' )\" > "  + strTemp + "</a>");
            
            return "&nbsp;<a class='RightText' title='Click to view Shipment details for this part' href=\"javascript:fnViewPartActual( '" + strPartID + "' )\" >"  + strPartID + "</a>" ;
        }
    }

