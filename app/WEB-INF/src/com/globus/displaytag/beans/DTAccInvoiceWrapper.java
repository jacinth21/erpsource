/*
 * Created on Apr 27, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.globus.displaytag.beans;

import java.text.DecimalFormat;
import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.TableDecorator;
import org.apache.commons.beanutils.DynaBean;
import com.globus.common.beans.GmCommonClass;

/**
 * @author jkumar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class DTAccInvoiceWrapper extends TableDecorator{
    /**
	     * FastDateFormat used to format dates in getDate().
	     */
	    private FastDateFormat dateFormat;

	    /**
	     * DecimalFormat used to format money in getMoney().
	     */
	    private DecimalFormat moneyFormat;
	    private StringBuffer  strBufVal;
	    private DynaBean db;
	    private String strValue;
	   	private String strCallFlag;
	   	private String strRes;
	   	String strImagePath;
	   	private String strEmailReqFl = "";
		private String strEmailVersion = "";
		private String strPDFFileID = "";
		private String strCSVFileID = "";
		private String strInvoicedYear = "";
		private String strDisplayImage = "";

	    /**
	     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
	     */
	    public DTAccInvoiceWrapper()
	    {
	        super();

	        // Formats for displaying dates and money.
	        this.dateFormat = FastDateFormat.getInstance("MM/dd/yy"); //$NON-NLS-1$
	        this.moneyFormat = new DecimalFormat("#,###,###.00"); //$NON-NLS-1$ // removed the hardcoded value of $ as currency type is shown in the header
	        strBufVal = new StringBuffer();
	        strImagePath = GmCommonClass.getString("GMIMAGES");
	    }

	    /**
	     * Returns the date as a String in MM/dd/yy format.
	     * @return formatted date
	     */
	    /* public String getINV_DATE()
	    {
	        return this.dateFormat.format(((DynaBean) this.getCurrentRowObject()).get("INV_DATE"));
	    }*/


	    /**
	     * Returns the Invoice Amount as a String in $ #,###,###.00 format.
	     * @return String
	     */
	    public String getINV_AMT()
	    {
	    	String strAmt = this.moneyFormat.format(((DynaBean) this.getCurrentRowObject()).get("INV_AMT"));
	        return GmCommonClass.getRedText(strAmt);
	    }

	    /**
	     * Returns the Payment Amount  as a String in $ #,###,###.00 format.
	     * @return String
	     */
	    public String getPAYMENT_AMT()
	    {
	        
	        db =  	(DynaBean) this.getCurrentRowObject();
	        if (db.get("PAYMENT_AMT") == null)
	        {
	            return "-";
	        }
	        else
	        {
	            return this.moneyFormat.format(db.get("PAYMENT_AMT") );
	        }
	    }
	    
	    public String getORDER_VALUE()
	    {
	        return this.moneyFormat.format(((DynaBean) this.getCurrentRowObject()).get("ORDER_VALUE"));       
	    }    
	    
	    public String getSHIP_COST()
	    {
	        
	        db =  	(DynaBean) this.getCurrentRowObject();
	        if (db.get("SHIP_COST") == null)
	        {
	            return "-";
	        }
	        else
	        {
	            return this.moneyFormat.format(db.get("SHIP_COST") );
	        }
	    }
	    
	    public String getTOT_COST()
	    {
	        return this.moneyFormat.format(((DynaBean) this.getCurrentRowObject()).get("TOT_COST"));
	    }    

	    
	    
	    /**
	     * Returns the Payment Amount  as a String in $ #,###,###.00 format.
	     * @return String
	     */
	    public String getINV_ID()
	    {
	        db =  	(DynaBean) this.getCurrentRowObject();
	        if (db.get("INV_ID") == null )
	        {
	            return "";
	        }
	        else
	        {
	        	 strCallFlag = (String)db.get("CALL_FLAG");
	        	 strEmailReqFl = GmCommonClass.parseNull(String.valueOf(db.get("EMAILREQ")));
	        	 strEmailVersion = GmCommonClass.parseNull(String.valueOf(db.get("EVERSION")));
				 strPDFFileID = GmCommonClass.parseNull(String.valueOf(db.get("PDFFILEID")));
				 strCSVFileID = GmCommonClass.parseNull(String.valueOf(db.get("CSVFILEID")));
				 strInvoicedYear = GmCommonClass.parseNull(String.valueOf(db.get("INVYEAR")));

	        	 strBufVal.setLength(0);
	        	
	        	 strBufVal.append("<img id='imgEdit' style='cursor:hand' src='");
	        	if (strCallFlag.equals("N")){
	        		strBufVal.append(strImagePath+"/phone_icon.jpg'");
	        	}
	        	else {
	        		strBufVal.append(strImagePath+"/phone-icon_ans.gif'");
	        	}
	        	 
    	   		strBufVal.append("title='Click to Enter Call log' width='22' height='20' " ); 
    	   		strBufVal.append("onClick=fnOpenLog('"+db.get("INV_ID")+"') </img>&nbsp;");
        		strBufVal.append("<a href= javascript:fnPrintInvoice('" +db.get("INV_ID") + "')><U>" + db.get("INV_ID") + "</U></a>");

				if(!strPDFFileID.equals("") && !strPDFFileID.equals("null")){ // check the PDF file is stored 
					strBufVal.append("&nbsp;<img id='imgPDF' style='cursor:hand;vertical-align:middle;' src='"+strImagePath+"/pdf_icon.gif' onclick=fnExportFile('"+strPDFFileID+"'); title='Click to Open PDF file'>&nbsp;");
				}
				if(!strCSVFileID.equals("")&& !strCSVFileID.equals("null")){ // check the CSV file is stored
					strBufVal.append("<img id='imgCSV' style='cursor:hand;vertical-align:middle;' src='"+strImagePath+"/csv_icon.gif' onclick=fnExportFile('"+strCSVFileID+"'); title='Click to Open CSV file'>&nbsp;");
				}

        		strRes = strBufVal.toString(); 
	        	
        		return strRes;
	        }
	    }

	    public String getORDER_ID()
	    {
	        db =  	(DynaBean) this.getCurrentRowObject();
	        strValue = (String)db.get("ORDER_ID");
	        
	        //Check if the Orded is a Return or Duplicate Order -- redirect based on the type
	        if ((strValue.charAt(strValue.length()-1)== 'R') ||(strValue.charAt(strValue.length()-1)== 'D') )
	        {
	            return "<a href=javascript:fnPrintCreditMemo('" +
	            		strValue + "')> "  + strValue + "</a>" ;
	        }else if (strValue.charAt(strValue.length()-1)== 'A')
	        {
	            return "<a href=javascript:fnPrintCashAdjustMemo('" + 
	            	strValue + "')> "  + strValue + "</a>" ;
			}else
			    return "<a href=javascript:fnPrintPack('" +  
			    strValue + "')> "  + strValue + "</a>" ;
	    	}
	}

