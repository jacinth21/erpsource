/*
 * Created on Apr 27, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.globus.displaytag.beans;

import java.text.DecimalFormat;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmCommonClass;


public class DTPartListPriceWrapper extends TableDecorator{
 
	 	private DecimalFormat moneyFormat;
	 	HashMap hmResult = new HashMap ();
		double dbLprice = 0.0;
		Double dblObj;
		String strSessCurrSymbol = "";
	    	    
	    String strDb = "";
	    String strCostId = "";
	    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

	    /**
	     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
	     */
	    public DTPartListPriceWrapper()
	    {
	        super();
	        // Formats for displaying dates and money.
	        this.moneyFormat = new DecimalFormat("#,###,###.00"); //$NON-NLS-1$
	    }


	     public String getLPRICE()
	    {
	    	 hmResult = (HashMap) this.getCurrentRowObject();
	    	 dbLprice = Double.parseDouble((hmResult.get("LPRICE")).toString());
	    	 dblObj = Double.valueOf(String.valueOf(dbLprice));
	 		 strSessCurrSymbol = GmCommonClass.parseNull((String) getPageContext().getSession().getAttribute("strSessCurrSymbol")) ;
	    	 
	    	 if (dbLprice == 0)
		        {
		            return "-";
		        }
		        else
		        {
		              	return strSessCurrSymbol+this.moneyFormat.format(dblObj);
		        }
	    	 
	    }
	}
	    /**
	     * Returns the List Price as a String in $ #,###,###.00 format.
	     * @return String
	     */
 