/**
 * Licensed under the Artistic License; you may not use this file
 * except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://displaytag.sourceforge.net/license.html
 *
 * THIS PACKAGE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */
package com.globus.displaytag.beans;

import java.util.HashMap;

import org.displaytag.decorator.TableDecorator;
import com.globus.common.beans.GmLogger;
import org.apache.log4j.Logger;


/**
 * This class is a decorator of the TestObjects that we keep in our List. This class provides a number of methods for
 * formatting data, creating dynamic links, and exercising some aspects of the display:table API functionality.
 * @author epesh
 * @author Fabrizio Giustina
 * @version $Revision$ ($Author$)
 */
public class DTProjectReportWrapper extends TableDecorator
{

    private String strId;
    private String strName;
    private HashMap hmRow = new HashMap ();
    /**
     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
     */
    public DTProjectReportWrapper()
    {
        super();
    }

    /**
     * Returns the Payment Amount  as a String in $ #,###,###.00 format.
     * @return String
     */
    public String getIDLINK()
    {
        hmRow =    (HashMap) this.getCurrentRowObject();
        strId = String.valueOf(hmRow.get("ID"));
        
        return "<a href=javascript:fnCallEdit('" + strId + "')> "+ strId+ " </a>" ;
    }
    
    public String getNAMELINK(){
        hmRow =    (HashMap) this.getCurrentRowObject();
        strId = String.valueOf(hmRow.get("ID"));
        strName = String.valueOf(hmRow.get("NAME"));
        
        return "<a href=javascript:fnCallPart('" + strId + "')> "+ strName+ " </a>" ;
        }

	public String getGROUPNAMEEXCEL() {
		hmRow = (HashMap) this.getCurrentRowObject();
		return String.valueOf(hmRow.get("GROUPNAME")).replaceAll("<BR>", "\n").replaceAll("<br>", "\n");
	}

	public String getSEGMENTEXCEL() {
		hmRow = (HashMap) this.getCurrentRowObject();
		return String.valueOf(hmRow.get("SEGMENT")).replaceAll("<BR>", "\n").replaceAll("<br>", "\n");
	}

	public String getTECHNIQUEEXCEL() {
		hmRow = (HashMap) this.getCurrentRowObject();
		return String.valueOf(hmRow.get("TECHNIQUE")).replaceAll("<BR>", "\n").replaceAll("<br>", "\n");
	}  
   }


