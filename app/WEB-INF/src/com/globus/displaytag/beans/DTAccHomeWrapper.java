package com.globus.displaytag.beans;
	
import java.text.DecimalFormat;

import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;
import org.apache.commons.beanutils.DynaBean;

import javax.servlet.jsp.PageContext;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger; 
import org.apache.log4j.Logger; 

	
public class DTAccHomeWrapper  extends TableDecorator{
		
		
		/**
		* DecimalFormat used to format money in getMoney().
		*/
		private DecimalFormat moneyFormat;
		private DynaBean db ;
		private String formatVal;
		double dblVal ;
		String strImagePath = GmCommonClass.getString("GMIMAGES");
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
		String strRes = "";
		String strScreenType = "";
		
		/**
		* Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
		*/
		public DTAccHomeWrapper()
		{
			super();
		}
		public void init(PageContext context, Object decorated, TableModel tableModel) {
			// TODO Auto-generated method stub
			super.init(context, decorated, tableModel);
			strScreenType = GmCommonClass.parseNull((String) getPageContext().getAttribute("screenType", PageContext.REQUEST_SCOPE));
		}
		
		/**
		*
		* @return String
		*/
		
		public String getRAID()
		{
			db =  	(DynaBean) this.getCurrentRowObject();
			if (db.get("RAID") == null )
			{
				return "";
			}
			else
			{
				String  strRaid = db.get("RAID").toString();
				return  "<a href= javascript:fnViewReturns('" + strRaid + "')>" +strRaid+ "</a>" ;
			}
		}
		
		public String getPENDCREDIT()
		{
			db =  	(DynaBean) this.getCurrentRowObject();
			String  strRaid = db.get("RAID").toString();
			return  "<a href= javascript:fnCallEditReturn('" + strRaid + "')>" + "Y" + "</a>" ;
		}
		
		public String getID()
		{
			//log.debug(" Inside getID ");
			db =  	(DynaBean) this.getCurrentRowObject();
			String  strPO = db.get("PO").toString();
			String  strId = db.get("ID").toString();
			String  strHoldFl = GmCommonClass.parseNull(db.get("HOLDFL").toString());
			
			StringBuffer strBufVal = new StringBuffer();
			if (strHoldFl.equals("Y"))
			{
				strBufVal.append("<img style=cursor:hand src=images/hold-icon.png title='DO on Hold Status' >");
			}
			strBufVal.append("<img style='cursor:hand' src='");
     	
       		strBufVal.append(strImagePath+"/detail_icon.gif'");
	   		strBufVal.append(" title='Click to View Invoice Details' width='12' height='17' id ='" +strPO+ "' " ); 
 	   		strBufVal.append(" onClick =fnViewInv('" + strId + "',this); </img> ");
	   		//strBufVal.append(" onClick =fnCallInv('" + strId + "',this); </img> ");
     		//strBufVal.append("<a href= javascript:fnPrintInvoice('" +db.get("CUSTOMER_PO") + "','" + db.get("ACCT_ID") +"')> <U>" + db.get("INV_ID") + "</U> </a> ");
 	   		strRes = strBufVal.toString(); 
			
			//return  "<a href= javascript:fnViewInv('" + strPO + "','" + strId + "')>" + "Y" + "</a>" ;
     		return strRes;
		}
		
		public String getPO()
		{
			db =  	(DynaBean) this.getCurrentRowObject();
			String  strPO = db.get("PO").toString();
			String  strId = db.get("ID").toString();
			String strRes = "";
			if(strScreenType.equals("")){
				String strReplacePO = strPO.replaceAll("\\\\", "\\\\\\\\"); //PC-5283 Unable to create invoice for the backslash symbol customer PO's
				strRes = "<a href= javascript:fnCallInv('" + strId + "','"+ strReplacePO + "')>" + strPO + "</a>" ;
			}else if(strScreenType.equals("INV_BATCH")){
				strRes = strPO;
			}
			
			 return  strRes;
			//return  "<a id ='" +strPO+ "' href= javascript:fnCallInv('" + strId + "',this)>" + strPO + "</a>" ;
     		
		}
		
		public String getPARENTOID()
		{
			db =  	(DynaBean) this.getCurrentRowObject();
			String  strParentOID = db.get("PARENTOID").toString();
						
			 return  "<a href= javascript:fnViewOrder('" + strParentOID + "')>" + strParentOID + "</a>" ;
			//return  "<a id ='" +strPO+ "' href= javascript:fnCallInv('" + strId + "',this)>" + strPO + "</a>" ;
     		
		}
			
}
