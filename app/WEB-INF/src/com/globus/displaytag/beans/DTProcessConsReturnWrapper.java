/**
 * 
 */
package com.globus.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * @author vprasath
 *
 */
public class DTProcessConsReturnWrapper extends TableDecorator
{

    private DynaBean db ;
    private String strPnum = "";
    private StringBuffer  strValue = new StringBuffer();
    private int intCount = 0;
    Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j    
    
    
    /**
     * 
     */
    public DTProcessConsReturnWrapper()
    {
        super();

    }
    /**
     * Returns the link to view part information  
     * @return String
     */
    
    public String getPNUM ()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strPnum   = String.valueOf(db.get("PNUM"));
        return "<a  href=\"javascript:fnViewPartActual( '" + strPnum + "') \" />" 
        + strPnum + "</a>" +"<input type='hidden' name='hPartNo"+String.valueOf(intCount)+"' value='"+strPnum+"' >";
                
    }
    
    public String getCHKBOX()
    {
        log.debug("Enter");
        db =   (DynaBean) this.getCurrentRowObject();
        String strPartNum =(String) db.get("PNUM");
        String strConsQty = db.get("PENDING_QTY") == null ? "0" :  GmCommonClass.parseZero(String.valueOf(db.get("PENDING_QTY")));

        String strIsChecked = "checked";
        if (getSET_QTY().equals("0") || strConsQty.equals("0"))
        {
            strIsChecked = "";
        }
        
        strValue.setLength(0);
        strValue.append("<input class=RightText type='checkbox' ");
        strValue.append(strIsChecked);
        strValue.append(" name='rad");
        strValue.append(String.valueOf(intCount) );
        strValue.append("' value='") ;
        strValue.append(strPartNum) ;
        strValue.append("' id='") ;
        strValue.append(strConsQty) ; 
        strValue.append("' onClick=javascript:fnOnCheckRad('"+intCount+"'); > ") ;
        
        log.debug("Exit");
        return strValue.toString();        
    }
    
    public String getPENDING_QTY()
    {
        db =   (DynaBean) this.getCurrentRowObject();       
        String strPendingQty =  String.valueOf(db.get("PENDING_QTY"));
        
        strValue.setLength(0);
        strValue.append("<input type='hidden' name='hPendingQty");
        strValue.append(String.valueOf(intCount) );
        strValue.append("' value='") ;
        strValue.append(strPendingQty) ;
        strValue.append("' > ") ;
        strValue.append(strPendingQty);
        return strValue.toString();    
    }
    
    public String getRETQTY()
    {
        String strSetQty = new String(getSET_QTY());
        strSetQty = GmCommonClass.parseZero(strSetQty);
        
        strValue.setLength(0);
        strValue.append("<input class=RightText type='input' size='4' name='Txt_RetQty");
        strValue.append(String.valueOf(intCount) );
        strValue.append("' value='") ;
        strValue.append(strSetQty) ;
        strValue.append("' > ") ;
        
        intCount++;
        return strValue.toString();
    }
    
    public String getSET_QTY()
    {
        db =   (DynaBean) this.getCurrentRowObject();       
        String strRetQty =  String.valueOf(db.get("SET_QTY"));
        strRetQty = (strRetQty == null || strRetQty.equals("") || strRetQty.equals("null")) ? "" : strRetQty;                
        return strRetQty;
    }
    
}
