package com.globus.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;
import com.globus.common.beans.GmLogger; 
import org.apache.log4j.Logger; 

public class DTRowHighlightWrapper extends TableDecorator
{
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    
    private DynaBean db ;
    String strCount = "0";
            public String addRowClass()
            {
                db = (DynaBean)this.getCurrentRowObject();
                strCount = String.valueOf(db.get("GROUPINGCOUNT"));
                    if ( strCount != null && !strCount.equals("")) 
                    {
                     //  log.debug("ShadeLevel"+strCount);   
                     return "DTShadeLevel"+strCount;
                    }                    
                    return "";
            }
}
