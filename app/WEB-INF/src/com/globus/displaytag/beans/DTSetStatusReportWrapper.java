package com.globus.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
 

public class DTSetStatusReportWrapper extends TableDecorator
{ 
    private DynaBean db ;
    GmLogger log = GmLogger.getInstance();
	StringBuffer strValue = new StringBuffer();  
    
    public String getREQID()
    {
    	db =    (DynaBean) this.getCurrentRowObject();
        String strReqId = (String)db.get("REQID");
        String strConId = (String)db.get("CONID");
        
		 
        strValue.setLength(0); 
		
		strValue.append("  <a href= javascript:fnViewDetails('" +strReqId +"','" + strConId +"')>" + strReqId + "</a>") ;
	//	strValue.append(strReqId) ;
				
		//log.debug("String value which will be returned is " + strValue.toString());
		return strValue.toString();
		
    } 
    
    public String getCONID()
    { 	 
	 
	 	 
	 	String strConId =  GmCommonClass.parseNull(((String)db.get("CONID")));
	 	if(strConId.equals(""))
	 		return strConId;
	 		
	 	strValue.setLength(0);
		strValue.append("  <a href= javascript:fnPrintVer('" + strConId +"')>" + strConId + "</a>") ;
	//	 log.debug("String vGalue which will be returned is " + strValue.toString());
	 	return strValue.toString();
		
    } 
    
    
    public String getSETID()
    {
    	db =    (DynaBean) this.getCurrentRowObject();
        String strSetId = (String)db.get("SETID");
        String strSetNm = (String)db.get("SETNAME");
        strSetNm = strSetNm.replaceAll(" ","-");
        
        strValue.setLength(0);
		 
		
		strValue.append("  <a href= javascript:fnCallSetMap('" +strSetId +"','" + strSetNm +"')>" + strSetId + "</a>") ;
	//	strValue.append(strReqId) ;
				
		// log.debug("String value which will be returned is " + strValue.toString());
		return strValue.toString();
		
    } 
    
    public String getPNUM()
    { 	 
	 
    	db =    (DynaBean) this.getCurrentRowObject();	// Was missing in prev version, added this as a fix for 3241
	 	String strPNUM =  GmCommonClass.parseNull(((String)db.get("PNUM")));
	  
	 	strValue.setLength(0);
		strValue.append("  <a href= javascript:fnPartReport()>" + strPNUM + "</a>") ;
	//	 log.debug("String vGalue which will be returned is " + strValue.toString());
	 	return strValue.toString();
		
    } 
    
}


