package com.globus.displaytag.beans;

import com.globus.common.beans.GmLogger;
import org.apache.log4j.Logger;
import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

public class DTParentPartQtyDetailWrapper extends TableDecorator {
	private String strValue;
	private String strDisplay;
	DynaBean db;
	/**
	 * Creates a new Wrapper decorator who's job is to reformat some of the data
	 * located in our TestObject's.
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
																	// Initialize
																	// the
																	// Logger
																	// Class.

	public DTParentPartQtyDetailWrapper() {
		super();
	}

	public String getINSTOCK() {
		return getColumnName("INSTOCK");
	}

	public String getOPENDHR() {
		return getColumnName("OPENDHR");
	}

	public String getTOTAL_INSTOCK() {
		return getColumnName("TOTAL_INSTOCK");
	}

	public String getTOTAL() {
		return getColumnName("TOTAL");
	}

	public String getTOTAL_OPENDHR() {
		return getColumnName("TOTAL_OPENDHR");
	}

	public String getSUB_PART_QTY() {
		return getColumnName("SUB_PART_QTY");
	}

	private String getColumnName(String columnName) {
		db = (DynaBean) this.getCurrentRowObject();
		strValue = String.valueOf(db.get(columnName));

		if (strValue == null || strValue.equals("") || strValue.equals("null")) {
			strDisplay = "-  ";
		} else {
			strDisplay = strValue;
		}
		return strDisplay;
	}

}
