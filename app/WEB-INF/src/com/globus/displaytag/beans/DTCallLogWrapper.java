package com.globus.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;

public class DTCallLogWrapper extends TableDecorator
{

    public DTCallLogWrapper()
    {
        super();
        // TODO Auto-generated constructor stub
    }
    private String strId;
    private String strClogFlag;
    private String strImgSrc;
    private DynaBean db ;
    String strImagePath = GmCommonClass.getString("GMIMAGES");
    
    /**
     * Returns the Payment Amount  as a String in $ #,###,###.00 format.
     * @return String
     */
    public String getCLOG()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strId = String.valueOf(db.get("ID"));
        strClogFlag = String.valueOf(db.get("CLOG"));
        strImgSrc = "";
        
        if(strClogFlag.equals("N"))
        {
            strImgSrc = strImagePath+"/phone_icon.jpg";
        }
        else
        {
            strImgSrc = strImagePath+"/phone-icon_ans.gif";
        }
        return "<a href=javascript:fnOpenLog('" + strId + "')>  <img id='imgEdit' style='cursor:hand' src = "+ strImgSrc+ " title='Click to Enter Call log' width='20' height='17' />  </a>" ;                                     
    }

}


