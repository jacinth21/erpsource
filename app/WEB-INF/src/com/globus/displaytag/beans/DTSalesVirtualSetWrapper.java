package com.globus.displaytag.beans;

/**
 * @author richardk
 *
 * Ths bean is used for VirtualSetWrapped screen format and for hyperlinks
 * 
 */


import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTSalesVirtualSetWrapper  extends TableDecorator{


     /**
     * DecimalFormat used to format money in getMoney().
     */
    private DynaBean db ;
    private String strID = "";
    private String strSID = "";
    private String strDID = ""; 
    private String strType = "";
    private String strVSetCount = "";
    private String  strSharedStatus = "";
	private String strRowClass = "RightText";
	private String strSalesType = "";
	
    Logger log = GmLogger.getInstance(this.getClass().getName());
    /**
     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
     */
    public DTSalesVirtualSetWrapper()
    {
    	super();
     }
    
    public void init(PageContext context, Object decorated) {
    	// TODO Auto-generated method stub
    	strSalesType = context.getRequest().getParameter("Cbo_SalesType");
        log.debug (" Sales Type is " + strSalesType);
    }
    
    public String getDNAME(){
        db =  	(DynaBean) this.getCurrentRowObject();
    	
    	
    	log.debug(" Sales type getDNAME " +strSalesType);
    	String strDistributorName = (String)db.get("DNAME");
    	return strDistributorName;
    }
    
    /**
     * Returns the link to view part details for the selected sets group details 
     * @return String
     */
    public String getSGDIMAGE ()
    {
    	db =  	(DynaBean) this.getCurrentRowObject();
    	strDID = (String)db.get("D_ID");
    	
       	if (strSalesType.equals("50301")){
       		return "";
       	}
       	
        strID  = (String)db.get("VSETID");
        strType = (String)db.get("TYPE");
        String strGroupID = (String)db.get("GROUPID");
        
        
        return "<img id='imgEditA' style='cursor:hand' src='/images/ordsum.gif'" +  
                " width='12' height='13' alt='Click to view all consigned parts'" +
                "onClick=\"javascript:fnViewGroupPart( '" + strGroupID + "', '" + strDID + "' , '" + strType +  "' ) \" />";
    }

    
    /**
     * Returns the link to view set details by consignment details
     * @return String
     */
    public String getVSETID()
    {
    	db =  	(DynaBean) this.getCurrentRowObject();
        log.debug(" Sales type " +strSalesType);
        double dblScount = 0;
        String strSetName = "";
        
        try{
        if (strSalesType.equals("50301")){
        	strID  = (String)db.get("VSETID");
        }
        else{
        	strID  = (String)db.get("LSETID");
        }
        log.debug(" strID type " +strID);
        
        strSetName = GmCommonClass.parseNull((String)db.get("VSETNM"));
        dblScount = Double.parseDouble(db.get("VSETIDCOUNT").toString());
        log.debug(" dblScount type " +dblScount);
        }
        catch(Exception exo) {
        	exo.printStackTrace();
        }
        
        if (dblScount > 1)
        {
            return "-";
        }
        if (strID.startsWith("AA")|| strID.startsWith("AI")) 
        {
            return "-";
        }
        else
        {
        	return "<a  href=\"javascript:fnViewSetID('" + strID + "','" +strSetName+ "')\"> "  + strID + "</a>" ;
        }
        
        	
    }

    
    public String getVSETCOUNT ()
    {
    	db =  	(DynaBean) this.getCurrentRowObject();
        String strTemp  = db.get("VCONSRPTID").toString();
        strID = GmCommonClass.parseNull(db.get("VSETID").toString());
        String strVSetIdCount = GmCommonClass.parseNull(db.get("VSETIDCOUNT").toString());
        
    	if (strSalesType.equals("50301")){
    		strVSetCount = db.get("VSETCOUNT").toString();
    		if (strVSetCount.equals("0") || !strVSetIdCount.equals("0"))
    		            return "-";
    		        	else return strVSetCount;

       	}
        
        if (strTemp.equals("20100"))
        {
            return db.get("VASETCOUNT").toString();
        }
        else
        {
        	strVSetCount = db.get("VSETCOUNT").toString();
        	if (strVSetCount.equals("0") || strID.startsWith("AA"))
            return "-";
        	else return strVSetCount;
        }
    	
    }
    
    
    /**
     * Returns the link to view part details for the selected sets group details 
     * @return String
     */
    public String getSDIMAGE ()
    {
        double dblScount;
        String strMCount ;
        String strType ;
        db =  	(DynaBean) this.getCurrentRowObject();
        strDID = (String)db.get("D_ID");
        strID  = (String)db.get("VSETID");
        dblScount = Double.parseDouble(db.get("VSETIDCOUNT").toString());
        String strVSetCost = db.get("VSETCOST").toString();
        
        log.debug(" count is " +dblScount + " dist is " +strDID);
        
        if (strSalesType.equals("50301")){
        	
        	if (strVSetCount.equals("0")){
        		return "-";
        		}
        	
        	if (dblScount > 0){
        		return "<img id='imgEditA' style='cursor:hand' src='/images/icon_info.gif'" +  
        			                " width='13' height='13' alt='Click to view Additional Inventory'" +
        			                "onClick=\"javascript:fnViewSetDetails( '" + strID + "', '" + strDID + "' , '' ) \" />";
        		}
        	else{
        		return "<img id='imgEditA' style='cursor:hand' src='/images/action-detail.gif'" +  
        		                " width='12' height='13' alt='Click to view Loaner Details'" +
        		                "onClick=\"javascript: fnShowLoanerDetails('" + strDID + "','" + strID + "')\" />";
        		}
        }
        else{
            strSID = (String)db.get("LSETID");
            strType = (String)db.get("TYPE");
        if (strVSetCost.equals("0"))
    	{
        	return "-";
    	}
        if (strSID.startsWith("AI") || strSID.startsWith("AA")) 
        {
        	 /*return "<img id='imgEditA' style='cursor:hand' src='/images/action-detail.gif'" +  
             " width='12' height='13' alt='Click to view consignment detail'" +  
             "onClick=\"javascript:fnViewPartByGroup( '" + strSID  + "', '" + strDID + "' , '" + strType +  "' ) \" />";*/
        	 return "<img id='imgEditA' style='cursor:hand' src='/images/action-detail.gif'" +  
             " width='12' height='13' alt='Click to view consignment breakdown'" +
             "onClick=\"javascript:fnViewSetDetails( '" + strSID + "', '" + strDID + "' , 'BYSETPART' ) \" />";
        }
        else
        {
            if (dblScount > 1)
            {
	            return "<img id='imgEditA' style='cursor:hand' src='/images/icon_info.gif'" +  
	                " width='13' height='13' alt='Click to view Additional Inventory'" +
	                "onClick=\"javascript:fnViewSetDetails( '" + strSID + "', '" + strDID + "' , '" + strType +  "' ) \" />";
            }
            else
            {
	            return "<img id='imgEditA' style='cursor:hand' src='/images/action-detail.gif'" +  
                " width='12' height='13' alt='Click to view consignment breakdown'" +
                "onClick=\"javascript:fnViewSetDetails( '" + strSID + "', '" + strDID + "' , 'BYSETPART' ) \" />";
                
            }
        }
        }
    }

    public String getVCONSRPTID ()
    {   
    	   String strValue = "";
    	   db =  	(DynaBean) this.getCurrentRowObject();
    	try {
        String strTemp  = db.get("VCONSRPTID").toString();
        strSharedStatus = GmCommonClass.parseZero((db.get("SHAREDSTATUS")).toString());
     
        
        if (strTemp.equals("20100"))
        {
        	strValue =  "<font color=red>* </font>";
        }
        
        if (!strSharedStatus.equals("0")){
        	strValue = strValue + "<font color=blue> ** </font>";
        }
    	}
    	catch(Exception exp ){
    		exp.printStackTrace();
    	}
        return strValue;
    }
    
    public String getVSETNM ()
    {   
    	db =  	(DynaBean) this.getCurrentRowObject();
    	String strSetNm  = db.get("VSETNM").toString();
    	String strTrade = GmCommonClass.getStringWithTM(strSetNm);
        return strTrade;
        
    }
    
    /**
     * Returns the link to view set details by consignment details
     * @return String
     */
/*     public String getAIMAGE()
    {
        //db =  	(DynaBean) this.getCurrentRowObject();
        //strID  = (String)db.get("VSETID");
        
        //String strDID = (String)db.get("D_ID");

        if (strID.startsWith("UNS")) 
        {
            return "";
        }
        else
        {
            return "<img id='imgEditA' style='cursor:hand' src='/images/actual.gif'" +  
                " width='12' height='13' " +  
               " onClick=\"javascript:fnViewSetActual( '" + strID + "', '" + strDID + "' )\" />"; 
        }
    }
*/
    
}