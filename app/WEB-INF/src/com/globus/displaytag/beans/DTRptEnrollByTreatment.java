package com.globus.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;

public class DTRptEnrollByTreatment extends TableDecorator
{
    private String strId;
    private String strClogFlag;
    private String strImgSrc;
    private DynaBean db ;
    String strImagePath = GmCommonClass.getString("GMIMAGES");
    
    /**
     * Returns the Payment Amount  as a String in $ #,###,###.00 format.
     * @return String
     */
    public String getSITEID()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strId = String.valueOf(db.get("SITEID"));        
        strImgSrc = strImagePath+"/HospitalIcon.gif";        
        return "<a href=javascript:fnCallAccount('" + strId + "')>  <img id='imgEdit' style='cursor:hand' src = "+ strImgSrc+ " />  </a> " +strId ;                                     
    }
}
