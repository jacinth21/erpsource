package com.globus.displaytag.beans;

/**
 * @author richardk
 *
 * Ths bean is used for VirtualSetWrapped screen format and for hyperlinks
 * 
 */


import java.text.DecimalFormat;

import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.TableDecorator;
import org.apache.commons.beanutils.DynaBean;

public class DTSalesVirtualActualWrapper  extends TableDecorator{


     /**
     * DecimalFormat used to format money in getMoney().
     */
    private DynaBean db ;
    private String strID = "";
    private String strSID = "";
    private String strDID = ""; 
    private String strType = "";
    
    /**
     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
     */
    public DTSalesVirtualActualWrapper()
    {
        super();
       //this.moneyFormat = new DecimalFormat("$ #,###,###.00"); //$NON-NLS-1$
        //this.moneyFormat = new DecimalFormat("$ {0,number,.00}"); 
    }

    /**
     * Returns the link to view call log information  
     * @return String
     */
    public String getLOG ()
    {
        db =  	(DynaBean) this.getCurrentRowObject();
        strID  	= (String)db.get("SID");
        strDID 	= db.get("DID").toString();
        strType = db.get("LOG").toString();

        if (strType.equals("Y") )
        {
            return "<img id='imgEditA' style='cursor:hand' src='/images/phone-icon_ans.gif'" +  
            " width='20' height='17'" + 
                "onClick=\"javascript:fnLog( '" + strDID + "R" + strID + "') \" />"; 
        }
        else
        {
            return "<img id='imgEditA' style='cursor:hand' src='/images/phone_icon.jpg'" +  
            " width='20' height='17' " + 
                "onClick=\"javascript:fnLog( '" + strDID + "R" + strID + "') \" />";
		}
                
    }
    
    public String getDIFFFLAG ()
    {
        db =  	(DynaBean) this.getCurrentRowObject();
        double dblVcount = Double.parseDouble(db.get("VCOUNT").toString());
        double dblFcount = Double.parseDouble(db.get("FCOUNT").toString());

        if (dblVcount > dblFcount ) 
        {
            return "<img id='imgEditA' style='cursor:hand' src='/images/arrow_up.gif'/>" ;  

        }
        else if (dblVcount < dblFcount)
        {
            return "<img id='imgEditA' style='cursor:hand' src='/images/arrow_down.gif' />";   
        }
        else
        {
            return "";            
        }
                
    }

    
    /**
     * Returns the link to view part details for the selected sets group details 
     * @return String
     */
    public String getVCOUNT ()
    {
        db =  	(DynaBean) this.getCurrentRowObject();
        String strCount ;
        strID  = (String)db.get("SID");
        strDID = db.get("DID").toString();
        strCount = db.get("VCOUNT").toString();

       return "<a  href=\"javascript:fnViewSetDetails( '" + strID + "', '" + strDID + "' , 'BYSETPART' ) \" />"
       + strCount + "</a>" ;
                
    }

    /**
     * Returns the link to view set actual consignment 
     * @return String
     */
    public String getFCOUNT ()
    {
        db =  	(DynaBean) this.getCurrentRowObject();
        String strCount ;
        strID  = (String)db.get("SID");
        strDID = db.get("DID").toString();
        strCount = db.get("FCOUNT").toString();

       return "<a  href=\"javascript:fnViewSetActual( '" + strID + "', '" + strDID + "'  ) \" />"
       + strCount + "</a>" ;
                
    }
    
}