package com.globus.displaytag.beans;
	
import org.displaytag.decorator.TableDecorator;
import org.apache.commons.beanutils.DynaBean;

public class DTFlagDHRWrapper  extends TableDecorator{
		
		
		/**
		* DecimalFormat used to format money in getMoney().
		*/
		private DynaBean db ;
		
		/**
		* Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
		*/
		public DTFlagDHRWrapper()
		{
			super();
		}
		
		public String getIDHLINK()
		{
			db =  	(DynaBean) this.getCurrentRowObject();
			if (db.get("ID") == null )
			{
				return "";
			}
			else
			{
				String  strDHRId = db.get("ID").toString();
				return  "<a href= javascript:fnViewDHR('" + strDHRId + "')>" +strDHRId+ "</a>" ;
			}
		}

		public String getPOIDHLINK()
		{
			db =  	(DynaBean) this.getCurrentRowObject();
			String  strPO = db.get("POID").toString();
						
			 return  "<a href= javascript:fnViewPO('" + strPO + "')>" + strPO + "</a>" ;
		}
		
		public String getINVID()
		{
			String  strpymntid = "";
			String  strInvoiceID= "";
			String strOutput = "";
			db =  	(DynaBean) this.getCurrentRowObject();
			if(db.get("PID") != null && db.get("INVID") != null ){
				  strpymntid = db.get("PID").toString();
			  strInvoiceID = db.get("INVID").toString();
			  strOutput = "<a href= javascript:fnInvcDetails('" + strpymntid + "')>" +strInvoiceID+ "</a>" ;
			}
			else 
				strOutput="";
			
			 return strOutput;
		}
		
		public String getSFL()
		{
			db =  	(DynaBean) this.getCurrentRowObject(); 
			String  strStatFl = db.get("SFL").toString();
			String strPayFl = db.get("PFL").toString();
		//	String strPaymentID = db.get("PID").toString();
		//	System.out.println("strPaymentID>>>>>>>>>>>>>>>>>>>>>>>>>>" + strPaymentID);
			if (strStatFl.equals("4") &&  strPayFl.equals("Y"))
			{
				strStatFl = "DHR Verified & INV";//"Flag for Payment";
			}
			else if (strStatFl.equals("4") && strPayFl.equals("N"))
			{
				strStatFl = "DHR Verified No INV";//"Open DHR";
			}
			else
			{
				strStatFl = "DHR Not Verified";
			}
						
			 return  strStatFl;
		}

		
}
