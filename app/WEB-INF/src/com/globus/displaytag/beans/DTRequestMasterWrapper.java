package com.globus.displaytag.beans;


import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.TableDecorator;
import org.apache.commons.beanutils.DynaBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmCommonClass;
import java.util.HashMap;

public class DTRequestMasterWrapper extends TableDecorator{
	
	//HashMap hmSearchResult = new HashMap ();
	GmLogger log = GmLogger.getInstance(); // Gets an instance of Logger class
	private DynaBean db ;
	private String strParNumber;
	private String strType = "";
	private String strClogFlag = "";
	private String strInHousePurpId = "";
	private String strImgSrc;
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	
	public DTRequestMasterWrapper()
	    {
	        super();
	    	
	    }


	public String getREQID()
	    {
		 	//hmSearchResult = 	(HashMap) this.getCurrentRowObject();
			DynaBean db =    (DynaBean) this.getCurrentRowObject();   
			 strClogFlag = String.valueOf(db.get("CLOG"));
		 
		 	// <A HREF='#' onClick="self.opener.document.frmAccount.Txt_FromDate.value='06/30/2006'; self.opener.document.frmAccount.Txt_FromDate.focus(); window.close();">
		 	
		 	StringBuffer strValue = new StringBuffer(); 
		 	String strReqId =  ((String)db.get("REQID"));
		 	String strConId =  ((String)db.get("CONID"));
		 	String strReqFor = GmCommonClass.parseNull(((String)db.get("REQUESTFORID")));
 			String strPurpose = GmCommonClass.parseNull(String.valueOf(db.get("PURPOSE"))) ;
		 	int Status = Integer.parseInt(db.get("STFLG").toString());	 
		 	 strImgSrc = "";
		 	String strReqSourceId =  GmCommonClass.parseNull(String.valueOf(db.get("REQSRCCID")));	        		 	
		 	String strCNStatusfl =  GmCommonClass.parseNull(String.valueOf(db.get("CNSTFL")));
		 	// Code to add the checkbox value
			strValue.append("<input class=RightText type='radio' name='radPart' id='");
			strValue.append(strReqId );
			strValue.append("' value='") ;
			strValue.append(strReqId) ;			
			strValue.append("' onClick=javascript:fnSelectPart('");
			strValue.append(strReqId) ;
			strValue.append("','");
			strValue.append(Status);
			strValue.append("','");
			strValue.append(strReqFor);
			strValue.append("','");
			strValue.append(strConId);
			strValue.append("') > ") ;
			if(strClogFlag.equals("N"))
	        {
	            strImgSrc = strImagePath+"/phone_icon.jpg";
	        }
	        else
	        {
	            strImgSrc = strImagePath+"/phone-icon_ans.gif";
	        }
			strValue.append("<a href=javascript:fnOpenOrdLogInv('" + strReqId + "')>  <img id='imgEdit' style='cursor:hand' src = "+ strImgSrc+ " title='Click to add comments' width='17' height='12' />  </a>" );
			strValue.append("  <a href= javascript:fnViewDetails('" +strReqId +"','" + strConId +"')>" + strReqId + "</a>") ;
			strInHousePurpId = strReqId;
			strInHousePurpId = strInHousePurpId.replaceAll("-","_");
			strValue.append("<input type='hidden' name='InHousePurose"+strInHousePurpId+"' value='");
			strValue.append(strPurpose);
			strValue.append("'> ");
			strValue.append("<input type='hidden' name='hreqsourceid"+strInHousePurpId+"' value='");
			strValue.append(strReqSourceId);
			strValue.append("'> ");
			strValue.append("<input type='hidden' name='hCNStatusfl"+strInHousePurpId+"' value='");
			strValue.append(strCNStatusfl);
			strValue.append("'> ");
			
			return strValue.toString();
			
	    }
	public String getCONID()
    {
	 	//hmSearchResult = 	(HashMap) this.getCurrentRowObject();
		DynaBean db =    (DynaBean) this.getCurrentRowObject();   
	 
	 	StringBuffer strValue = new StringBuffer(); 
	 	String strReqId =  ((String)db.get("REQID"));
	 	String strConId =  ((String)db.get("CONID"));
	 	String strTag =  GmCommonClass.parseNull(((String)db.get("TAG")));
	 	if(strTag.equals("Y"))
	 	{
	 		strValue.append("  <a href= javascript:fnOpenTag('" + strConId +"')><img id='imgTag' style='cursor:hand' src = '/images/tag.jpg' title='Click to add Tag' /> </a>") ;
	 	}
		strValue.append("  <a href= javascript:fnPrintVer('" + strConId +"')>" + strConId + "</a>") ;
	 	return strValue.toString();
		
    }
	

	public String getPDTREQID()
    {
	 	//hmSearchResult = 	(HashMap) this.getCurrentRowObject();
		DynaBean db =    (DynaBean) this.getCurrentRowObject();   
	 
	 	StringBuffer strValue = new StringBuffer(); 
	 	String strReqId =  ((String)db.get("PDTREQID"));
	 	String strDistNm = ((String)db.get("REQFORID"));
	 	String strShipTo = ((String)db.get("SHIPTO"));
	 	String strReqDetId =  ((String)db.get("REQDETAILID"));
        
	 	// Code to add the checkbox value
		strValue.append("<input class=RightText type='radio' name='radPart' id='");
		strValue.append(strReqId );
		strValue.append("' value='") ;
		strValue.append(strReqId) ;
		strValue.append("' onClick='javascript:fnSelectPart(this");
		strValue.append(")' > ") ;
		strValue.append(strReqId) ;
		strValue.append("<input type='hidden' name='reqforid"+strReqId+"' value='");
		strValue.append(strDistNm);
		strValue.append("'> ");
		strValue.append("<input type='hidden' name='shipto"+strReqId+"' value='");
		strValue.append(strShipTo);
		strValue.append("'> ");
		strValue.append("<input type='hidden' name='reqdetid"+strReqId+"' value='");
		strValue.append(strReqDetId);
		strValue.append("'> ");
		
		
		//log.debug("String value which will be returned is " + strValue.toString());
		return strValue.toString();
		
    }
	
	
}
