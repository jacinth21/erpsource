package com.globus.displaytag.beans;


import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.jsp.PageContext;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;
import com.globus.clinical.forms.GmClinicalDownloadForm;
import com.globus.common.beans.GmLogger;

public class DTClinicalDownloadWrapper extends TableDecorator{

		private HashMap hmValues = new HashMap();
	  
		
		private GmClinicalDownloadForm gmClinicalDownloadForm;
		private List alCancelList;
		private Iterator alCancelListIter;
		private String strFileId = "";
		private String strLockFl = "";
		private String strComments = "";
	    
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
		
		public void init(PageContext pageContext, Object decorated, TableModel tableModel) {
			super.init(pageContext, decorated, tableModel);
			gmClinicalDownloadForm = (GmClinicalDownloadForm)getPageContext().getAttribute("frmClinicalDownload", PageContext.REQUEST_SCOPE);			
			alCancelList = gmClinicalDownloadForm.getAlCancelReasonList();		
		}

	    public String getLINK()
	    {	    	
	    	hmValues =   (HashMap) this.getCurrentRowObject();
	        strFileId   = (String)hmValues.get("ID");	        
	        StringBuffer sbHtml = new StringBuffer();
	        sbHtml.append(" <a href=javascript:fnOpenForm(");
	        sbHtml.append(strFileId);
	        sbHtml.append(");>");
	        sbHtml.append("<img border='0' src=/images/csv_icon.gif></a>");	     
	        return sbHtml.toString();
	    }  	   
	    
	    public String getCANCEL()
	    {	    	
	    	hmValues =   (HashMap) this.getCurrentRowObject();
	    	strLockFl   = (String)hmValues.get("LOCK_FL");
	    	strFileId   = (String)hmValues.get("ID");	
	        StringBuffer sbHtml = new StringBuffer();
	        if(strLockFl.equalsIgnoreCase("Y")){
	        	gmClinicalDownloadForm.setStrLockExist("true");
	        	sbHtml.append(" <INPUT type='checkbox' id='cancelCheck' name='cancelCheck' value='");
	        	sbHtml.append(strFileId);
	        	sbHtml.append("' onClick='fnShowReason()'/>");	        	
	        }
	        else{
	        	sbHtml.append("&nbsp;");
	        }
	        return sbHtml.toString();
	    }
	    
	    public String getCOMMENTS()
	    {	    	
	    	hmValues =   (HashMap) this.getCurrentRowObject();
	    	strLockFl   = (String)hmValues.get("LOCK_FL");
	    	strFileId   = (String)hmValues.get("ID");	
	    	strComments   = (String)hmValues.get("COMMENTS");	
	    	
	        StringBuffer sbHtml = new StringBuffer();
	        if(strLockFl.equalsIgnoreCase("Y")){
	        	gmClinicalDownloadForm.setStrLockExist("true");
	        	sbHtml.append(" <div id='cancelDiv' style='display:none' >");
	        	alCancelListIter = alCancelList.iterator();
	        	sbHtml.append("<select name='cancelId");	    		
	        	sbHtml.append("' class='RightText' tabindex=0 style='width:150px'> ");
	        	sbHtml.append(" <option value='0'>[Choose One] </option>");
	    		while (alCancelListIter.hasNext()) {
	    			HashMap hmCancelList = (HashMap) alCancelListIter.next();
	    			String strOptKey = (String) hmCancelList.get("CODEID");
	    			String strOptValue = (String) hmCancelList.get("CODENM");
	    			
	    			sbHtml.append(" <option value= " + strOptKey + " > " + strOptValue + "</option>");
	    		}

	    		sbHtml.append(" </select>");
	    		sbHtml.append(" </div>");	        		        	
	        }
	        else{
	        	sbHtml.append(strComments);
	        }
	        return sbHtml.toString();
	    }
}
