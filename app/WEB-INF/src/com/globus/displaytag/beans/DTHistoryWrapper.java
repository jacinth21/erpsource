package com.globus.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;

public class DTHistoryWrapper extends TableDecorator
{
    private String strPar; 
    private String strCom;
    
    private String strParNumber;
    private String strCSS = "";
    
    private String strUby = "";
    private String strUdate = "";
    private String strHistory = "";
    private DynaBean db ;
    private int intCount = 0;
    public StringBuffer  strValue = new StringBuffer();
    String strImagePath = GmCommonClass.getString("GMIMAGES");
    
    /**
     * Returns the string for text field.
     * @return String
     */
    public String getPVALUE()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strPar = String.valueOf(db.get("PVALUE"));
        strHistory = String.valueOf(db.get("HISTORYFL"));
           
        
        if (strHistory.equals("Y") )
        strCSS = "strikethruRedTD";
        else 
        	strCSS = "ShadeWhite";
        
        strValue.setLength(0);
        strValue.append("<td class='");
        
        strValue.append(strCSS);
        
        strValue.append("'>");
      
        strValue.append(strPar);
        strValue.append("</td>");
        return strValue.toString();
        //return "<input type = text name = parvalue size = 10 value="+strPar+">";
        
    }
    
   /* public String getCOMMENTS()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strCom = GmCommonClass.parseNull(String.valueOf(db.get("COMMENTS")));
       
        System.out.println("COMMENTS : "+ strCom);        
        return "<textarea name = txt_LogReason  class=InputArea rows=3 cols=30>" + strCom + "</textarea>";
    }
    */
    
    public String getUBY()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strUby = String.valueOf(db.get("UBY"));
        strHistory = String.valueOf(db.get("HISTORYFL"));
           
        
        if (strHistory.equals("Y") )
        strCSS = "strikethruRedTD";
        else 
        	strCSS = "ShadeWhite";
        
        strValue.setLength(0);
        strValue.append("<td class='");
        
        strValue.append(strCSS);
        
        strValue.append("'>");

        strValue.append(strUby);
        strValue.append("</td>");
         
        return strValue.toString();

                       
    }
    
    public String getUDATE()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strUdate = String.valueOf(db.get("UDATE"));
        strHistory = String.valueOf(db.get("HISTORYFL"));
           
        
        if (strHistory.equals("Y") )
        strCSS = "strikethruRedTD";
        else 
        	strCSS = "ShadeWhite";
        
        strValue.setLength(0);
        strValue.append("<td class='");
        
        strValue.append(strCSS);
        
        strValue.append("'>");

        strValue.append(strUdate);
        strValue.append("</td>");
         
        return strValue.toString();

                       
    }
}


