/**
 * Licensed under the Artistic License; you may not use this file
 * except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://displaytag.sourceforge.net/license.html
 *
 * THIS PACKAGE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */
package com.globus.displaytag.beans;

import java.text.DecimalFormat;
import java.util.HashMap;

import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.TableDecorator;
import org.apache.commons.beanutils.DynaBean;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger; 
import org.apache.log4j.Logger; 


/**
 * This class is a decorator of the TestObjects that we keep in our List. This class provides a number of methods for
 * formatting data, creating dynamic links, and exercising some aspects of the display:table API functionality.
 * @author epesh
 * @author Fabrizio Giustina
 * @version $Revision$ ($Author$)
 */
public class DTMfgInitateWOWrapper extends TableDecorator
{

    private DynaBean db;
    private String strValue;
    private String strValueFormatted;
    private int i = 0;
    private String strPnum;
    private String strSetQty;
    private String strInvQty;
    
    private String strForceBO;
    private String strReqQty;
    private StringBuffer strValuebuff = new StringBuffer();
	private int intCount = 0;
	private int intCount1 = 0;
	private String strUnitName = "";
	private String strUnitType = "";
	private String strdisabled = "";
	private String strQtyEdit = "";
	
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
    HashMap hmSearchResult;
	
    /**
     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
     */
    public DTMfgInitateWOWrapper()
    {
        super();
    }

    /**
     * Returns the Payment Amount  as a String in $ #,###,###.00 format.
     * @return String
     */
    public String getPNUM()
    {
    	i++;
    	hmSearchResult = (HashMap) this.getCurrentRowObject();
        strValue = GmCommonClass.parseNull((String)hmSearchResult.get("PNUM"));
        strPnum = "<input type = 'hidden' name='hPartNum"+i+"' value='"+strValue+"'>&nbsp;"+strValue; 
        return strPnum;
    }

    public String getSETQTY()
    {
    	hmSearchResult = (HashMap) this.getCurrentRowObject();
        strValue = GmCommonClass.parseNull((String)hmSearchResult.get("SETQTY"));
        strSetQty = "<input type = 'hidden' name='hSetQty"+i+"' value='"+strValue+"'>&nbsp;"+strValue; 
        return strSetQty;
    }
    
    public String getINVQTY()
    {
    	hmSearchResult = (HashMap) this.getCurrentRowObject();
        strValue = GmCommonClass.parseNull((String)hmSearchResult.get("INVQTY"));
        strInvQty = "<input type = 'hidden' name='hInvQty"+i+"' value='"+strValue+"'>&nbsp;"+strValue; 
        return strInvQty;
    }
     
	public String getFORCEBO() { 
		hmSearchResult = (HashMap) this.getCurrentRowObject(); 
		//strUnitName =  (String)hmSearchResult.get("UNITNM"); 
		strUnitType =  GmCommonClass.parseNull((String)hmSearchResult.get("UNITTYPE")); 
	 	if(strUnitType.equals("40012")) strdisabled="disabled";  else strdisabled="";
		/*
		strValuebuff.setLength(0); 
		strValuebuff.append("<input class=RightText type='checkbox' "); 
		strValuebuff.append(strdisabled);
		strValuebuff.append(" name='Chk_BO");
		strValuebuff.append(String.valueOf(intCount)); 
		strValuebuff.append("' > ");*/
		strForceBO = "<input class=RightText " + strdisabled + " type='checkbox' name='Chk_BO"+intCount+"'>&nbsp;"; 
		intCount++; 
		return strForceBO;
	}
	
	 public String getREQQTY()
	    {  
		//strValue = (String)hmSearchResult.get("UNITNM"); 
		 	strUnitType =  GmCommonClass.parseNull((String)hmSearchResult.get("UNITTYPE"));
		 	strQtyEdit =  GmCommonClass.parseNull((String)hmSearchResult.get("QTYEDIT")); 
		 	if( strUnitType.equals("40012"))	strdisabled="disabled";  else strdisabled="";
	        strReqQty = "<input type = 'hidden' name='hUnitType"+intCount1+"' value='"+strUnitType+"'>&nbsp;"+"<input class=RightText " + strdisabled + " type='text' name='reqQty"+intCount1+"' size = 10 >&nbsp;"; 
	        intCount1++;
	        return strReqQty; 
	    }
}
