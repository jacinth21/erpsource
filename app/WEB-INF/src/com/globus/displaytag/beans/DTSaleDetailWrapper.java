package com.globus.displaytag.beans;

/**
 * @author jkumar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */


import java.text.DecimalFormat;

import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.TableDecorator;
import org.apache.commons.beanutils.DynaBean;

public class DTSaleDetailWrapper  extends TableDecorator{


     /**
     * DecimalFormat used to format money in getMoney().
     */
    private DecimalFormat moneyFormat;
    private DynaBean db ;
    private String formatVal;
    double dblVal ;
    private StringBuffer strIDValue;
    
    /**
     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
     */
    public DTSaleDetailWrapper()
    {
        super();
       this.moneyFormat = new DecimalFormat("$ #,###,###.00"); //$NON-NLS-1$
        //this.moneyFormat = new DecimalFormat("$ {0,number,.00}"); 
    }

    /**
     * Returns the Payment Amount  as a String in $ #,###,###.00 format.
     * @return String
     */
    
    public String getID()
    {
     db =  	(DynaBean) this.getCurrentRowObject();
        if (db.get("ID") == null )
        {
            return "";
        }
        else
        {
        	 return "<a href= javascript:fnPrintOrder('" + 
	    		db.get("ID") + "')> " + db.get("ID") + "</a>" ;
        	
		}
    }
    
    public String getTRACK()
    {
       	db =  	(DynaBean) this.getCurrentRowObject();
        if (db.get("TRACK") == null )
        {
            return "";
        }
        else
        {
        	 return "<a href= javascript:fnOpenTrack('" + 
	    	db.get("TRACK") + "')> " + db.get("TRACK") + "</a>" ;
        }
        
    }
    
    public String getINVID()
    {
    
    	db =  	(DynaBean) this.getCurrentRowObject();
        if (db.get("INVID") == null )
        {
            return "";
        }
        else
        {
        	 return "<a href= javascript:fnPrintInvoice('" + 
	    		db.get("INVID") + "')> " + db.get("INVID") + "</a>" ;
    }
        
    }
   
    public String getCOST()
    {
    	db =  	(DynaBean) this.getCurrentRowObject();
        if (db.get("COST") == null )
        {
            return "";
        }
        else
        {
         // return this.moneyFormat.format(db.get("COST"));
        	double dblCost = Double.parseDouble((db.get("COST")).toString());
            String strCost = this.moneyFormat.format(dblCost);
           return strCost;
        }
    }   
    
    public String getSHIPCOST()
    {
    	db =  	(DynaBean) this.getCurrentRowObject();
        if (db.get("SHIPCOST") == null )
        {
            return "";
        }
        else {  
        // return this.moneyFormat.format(db.get("SHIPCOST"));
        	double dblShipCost = Double.parseDouble((db.get("SHIPCOST")).toString());
            String strShipCost = this.moneyFormat.format(dblShipCost);
           return strShipCost;
        }
    }    
    
    
}