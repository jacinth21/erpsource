package com.globus.displaytag.beans.sales;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;
import com.globus.common.beans.GmLogger;

public class DTDistributorReportWrapper extends TableDecorator {
	private DynaBean db;
	private String DISTID = "";
	GmLogger log = GmLogger.getInstance();

	public DTDistributorReportWrapper() 
	{
		super();
	}

	public String getDISTID() 
	{
		db = (DynaBean) this.getCurrentRowObject();
		DISTID = (String) db.get("DISTID");

		StringBuffer strValue = new StringBuffer();
		strValue.append(" <a href= javascript:fnCallAccountRpt('");
		strValue.append(DISTID);
		strValue.append("');> <img src=/images/a.gif height='14' width='12' alt='Detail'> </a>");
		strValue.append(" <a href= javascript:fnCallRepRpt('");
		strValue.append(DISTID);
		strValue.append("');> <img src=/images/icon_sales.gif height='14' width='12' alt='Receive' > </a>");

	//	log.debug("String value which will be returned is " + strValue.toString());
		return strValue.toString();
	}

}
