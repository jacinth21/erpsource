package com.globus.displaytag.beans.sales;

import java.util.HashMap;

import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTCaseSetsWrapper extends TableDecorator{
	

	private HashMap db;
    private String strSetId; 
    private String strSetNM; 
    private String strTAGID = "";  
    public StringBuffer strValue = new StringBuffer(); 
	
public String getSETID(){
	db = 	(HashMap) this.getCurrentRowObject();
	strSetId=GmCommonClass.parseNull((String)db.get("SETID")); 
	strSetNM=GmCommonClass.parseNull((String)db.get("SETNM"));
	strSetNM =  strSetNM.replaceAll(" ", "-"); 
	strSetId = "<a href =\"#\" onClick=\"return doOnLoad('"+strSetId+"','" +strSetNM+"'"+")\">"+strSetId+"</a>";
	
return strSetId;

}


public String getTAGID() {
	db = (HashMap) this.getCurrentRowObject();
	strTAGID = GmCommonClass.parseNull((String) db.get("TAGID")); 
	
	strValue.setLength(0);
	if(!strTAGID.equals("TBD"))
	{
		strValue.append("<img src='/images/jpg_icon.jpg' alt='Tag' width='16' height='16' id='imgEdit' style='cursor:hand' title='View Image' onclick=\"return viewImg('"+strTAGID+"')\" /> ");
		strValue.append("<img src='/images/detail_icon.gif' alt='Set' width='16' height='16'  style='cursor:hand' title='Set Override' onclick=\"return fnSetOverride('"+strTAGID+"')\" />");
	}
	strValue.append("  " + strTAGID);
	
	return strValue.toString();
}
}
