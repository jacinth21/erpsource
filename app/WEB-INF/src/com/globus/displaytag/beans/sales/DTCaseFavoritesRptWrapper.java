package com.globus.displaytag.beans.sales;

import java.util.HashMap;

import org.displaytag.decorator.TableDecorator;

public class DTCaseFavoritesRptWrapper extends TableDecorator{
	

	private HashMap db;
    private String strFavoriteCaseId;
    String strFavoriteCaseNm=null;
    String strCatg = null;
    
public String getCASENM(){
	db = 	(HashMap) this.getCurrentRowObject();
	strFavoriteCaseId=(String)db.get("FCASEID");
	strFavoriteCaseNm =(String)db.get("CASENM");
	strFavoriteCaseNm = "<a href= javascript:fnCaseFavorite('"+strFavoriteCaseId+"');>"+strFavoriteCaseNm+"</a>";
	
return strFavoriteCaseNm;

}
public String getCATEGORY(){
	db = 	(HashMap) this.getCurrentRowObject();
	strCatg=(String)db.get("CATEGORY");
	strCatg = strCatg.replaceAll(",", "<BR>");
	return strCatg;

}
}
