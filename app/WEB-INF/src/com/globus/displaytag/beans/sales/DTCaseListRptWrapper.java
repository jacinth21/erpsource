package com.globus.displaytag.beans.sales;

import java.util.HashMap;

import org.displaytag.decorator.TableDecorator;

public class DTCaseListRptWrapper extends TableDecorator{
	
	private HashMap db;
    private String strCaseInfoId;
    String strCaseid=null;
    String strCaseStsID=null;
    String strReqSet=null;
    String strAloSet=null;
    String strCatg = null;
    
public String getCASEID(){
	db = 	(HashMap) this.getCurrentRowObject();
	strCaseInfoId=(String)db.get("CASEINFOID");
	strCaseid =(String)db.get("CASEID");
	strCaseid = "<a href=\"/gmCasePost.do?caseInfoID=" + strCaseInfoId + "\">"+strCaseid+"</a>";
	
return strCaseid;

}
public String getREQSET(){
	db = 	(HashMap) this.getCurrentRowObject();
	strCaseStsID=(String)db.get("CASESTS");
	strReqSet=(String)db.get("REQSET");
	//case status is cancelled then return N/A
	if(strCaseStsID.equals("11093"))
	{
		strReqSet="N/A";
	}
	
return strReqSet;

}
public String getALOCSET(){
	db = 	(HashMap) this.getCurrentRowObject();
	strCaseInfoId=(String)db.get("CASESTS");
	strAloSet =(String)db.get("ALOCSET");
	//case status is cancelled then return N/A
	if(strCaseStsID.equals("11093"))
	{
		strAloSet="N/A";
	}
	
return strAloSet;

}
public String getCATEGORY(){
	db = 	(HashMap) this.getCurrentRowObject();
	strCatg=(String)db.get("CATEGORY");
	strCatg = strCatg.replaceAll(",", "<BR>");
	return strCatg;

}
}