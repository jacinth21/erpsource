/**
 * Licensed under the Artistic License; you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * 
 * http://displaytag.sourceforge.net/license.html
 * 
 * THIS PACKAGE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.
 */
package com.globus.displaytag.beans.sales;

import java.text.DecimalFormat;
import java.util.HashMap;

import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;


/**
 * This class is a decorator of the TestObjects that we keep in our List. This class provides a
 * number of methods for formatting data, creating dynamic links, and exercising some aspects of the
 * display:table API functionality.
 * 
 * @author epesh
 * @author Fabrizio Giustina
 * @version $Revision$ ($Author$)
 */
public class DTDetailedSalesWrapper extends TableDecorator {

  private DecimalFormat moneyFormat;
  HashMap hmResult = new HashMap();
  double dbTotal = 0.0;
  double dbGrandTotal = 0.0;
  double dbPercent = 0.0;
  String strOrderId = "";
  String strParentId = "";
  String strFormat = "";
  Double dblObj;
  String strCurrSymbol = "";

  /**
   * Creates a new Wrapper decorator who's job is to reformat some of the data located in our
   * TestObject's.
   */
  public DTDetailedSalesWrapper() {
    super();
    // Formats for displaying dates and money.
    this.moneyFormat = new DecimalFormat("#,###,###.00"); //$NON-NLS-1$
  }

  public String getORDID() {
    hmResult = (HashMap) this.getCurrentRowObject();
    strOrderId = GmCommonClass.parseNull((String) hmResult.get("ORDID"));
    strParentId = GmCommonClass.parseNull((String) hmResult.get("PARENTID"));

    // Check if the Order is a Return or Duplicate Order -- redirect based on the type
    if ((strOrderId.charAt(strOrderId.length() - 1) == 'R')
        || (strOrderId.charAt(strOrderId.length() - 1) == 'D')) {
      strFormat = "<a href=javascript:fnPrintCreditMemo('" + strOrderId + "');";
    } else if (strOrderId.charAt(strOrderId.length() - 1) == 'A') {
      strFormat = "<a href=javascript:fnPrintCashAdjustMemo('" + strOrderId + "');";

    } else {
      strFormat =
          "<a title='View Packing Slip'  href=javascript:fnPrintPick('" + strOrderId + "');";
    }

    strFormat =
        strFormat + " ><img src='images/packslip.gif' title='View Pack Slip'  border=0></a>";
    if (strParentId.equals("")) {
      strParentId = strOrderId;
    }

    strFormat =
        strFormat + "&nbsp;<a href=javascript:fnViewOrder('" + strParentId
            + "');><img src='images/ordsum.gif' title='View Order Summary'  border=0></a>";

    return strFormat + "&nbsp;" + strOrderId;
  }


  public String getSALES() {
    hmResult = (HashMap) this.getCurrentRowObject();
    dbTotal = Double.parseDouble((hmResult.get("SALES")).toString());
    dblObj = Double.valueOf(String.valueOf(dbTotal));
    strCurrSymbol =
        GmCommonClass.parseNull((String) getPageContext().getRequest().getAttribute("hCurrSymb"))
            + " ";

    if (dbTotal == 0) {
      return "-";
    } else {
      return strCurrSymbol + this.moneyFormat.format(dblObj);
    }
  }


  public String getGRANDTOTAL() {
    hmResult = (HashMap) this.getCurrentRowObject();
    dbGrandTotal = Double.parseDouble((hmResult.get("GRANDTOTAL")).toString());
    dblObj = Double.valueOf(String.valueOf(dbGrandTotal));

    if (dbGrandTotal == 0) {
      return "-";
    } else {
      return strCurrSymbol + this.moneyFormat.format(dblObj);
    }
  }


  public String getPERCENT() {
    hmResult = (HashMap) this.getCurrentRowObject();
    dbTotal = Double.parseDouble((hmResult.get("SALES")).toString());
    dbGrandTotal = Double.parseDouble((hmResult.get("GRANDTOTAL")).toString());
    dbPercent = 100 - ((dbGrandTotal - dbTotal) / dbGrandTotal) * 100;

    return GmCommonClass.getStringWithCommas("" + dbPercent, 2) + "%";
  }

}
