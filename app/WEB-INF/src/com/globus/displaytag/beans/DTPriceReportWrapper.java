/*
 * Created on Apr 27, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.globus.displaytag.beans;

import java.text.DecimalFormat;
import java.util.HashMap;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.forms.GmCommonForm;

public class DTPriceReportWrapper extends TableDecorator{
 
	 	private DecimalFormat moneyFormat;
	 	HashMap hmResult = new HashMap ();
		double dbLprice = 0.0;
		Double dblObj;
		private DynaBean db = null; 	    
	    String strDb = "";
	    String strId = "";
	    String strHisFl = "";
	    String strGrpType = "";
	    private int intCount = 0;
	    String strHistIntImgValue = "<img id=imgEdit style=cursor:hand src=images/icon_History.gif " + 
		  "  align='bottom' title='Click to view Pricing history' width=18 height=18 ";	    
	    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

	    /**
	     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
	     */
	    public DTPriceReportWrapper()
	    {
	        super();
	        // Formats for displaying dates and money.
	        //this.moneyFormat = new DecimalFormat("$#,###,###.00"); //$NON-NLS-1$
	    }

	    /**
	     * Returns the List Price as a String in $ #,###,###.00 format.
	     * @return String
	     */
		/*public String getPRICE()
		{
			 hmResult = (HashMap) this.getCurrentRowObject();
			 
			 dbLprice = Double.parseDouble((hmResult.get("PRICE")).toString());
			 //dblObj = Double.valueOf(String.valueOf(dbLprice));
		 
			if (dbLprice == 0)
			{
			    return "-";
			}
			else
			{
				System.out.println("gmCommonForm.getApplnCurrFmt():::::::"+gmCommonForm.getApplnCurrFmt());
				System.out.println("gmCommonForm.getApplnCurrSign():::::::"+gmCommonForm.getApplnCurrSign());
			     String str = GmCommonClass.getStringWithCommas((hmResult.get("PRICE")).toString(), gmCommonForm.getApplnCurrFmt());
			     str = gmCommonForm.getApplnCurrSign()+str;
			     System.out.println("str-------------->"+str);
			     return str;
			}
			     	 
	    }*/
		
		public String getID()
		{
			StringBuffer sbHtml = new StringBuffer();
			db =  	(DynaBean) this.getCurrentRowObject();  
			strId = GmCommonClass.parseNull(String.valueOf(db.get("ID")));
			strGrpType = GmCommonClass.parseNull(String.valueOf(db.get("GROUPTYPE")));
			String strVoidFl   = GmCommonClass.parseNull(String.valueOf(db.get("VOID_FL")));
	    	String strAPId = GmCommonClass.parseNull(String.valueOf(db.get("APID")));
	    	String strUnitPrice = GmCommonClass.parseNull(String.valueOf(db.get("PRICE")));
	    	String strDisabled = "";
	    	if(strVoidFl.equals("Y"))
	    	{
	    		strDisabled = "disabled";
	    	}
	    	sbHtml.setLength(0);
	    	sbHtml.append("<input type='hidden' name='hAPPrice");
	    	sbHtml.append(String.valueOf(intCount));
	    	sbHtml.append("' value='");
	    	sbHtml.append(strAPId);
	    	sbHtml.append("'>");
	    	sbHtml.append("<input type='hidden' name='hUPrice");
			sbHtml.append(String.valueOf(intCount));
			sbHtml.append("' value='");
			sbHtml.append(strUnitPrice);
			sbHtml.append("'>");
			sbHtml.append("<input type='checkbox'" + strDisabled + " onClick=\"javascript:fnClickCheckBox('"+intCount+"','"+strAPId+"','"+strUnitPrice+"')\" name='chk_rowwise"); 
			sbHtml.append(String.valueOf(intCount));
			sbHtml.append("'> ");
			
			if(!strGrpType.equals("40045"))
				sbHtml.append("<a href=\"javascript:fnCallEdit('" + strId + "')\";> "  + strId + "</a>") ;
			else{
				sbHtml.append("<a href='#' onClick=\"return fnGroupDetail('");
				sbHtml.append(strId);
				sbHtml.append("')\";> <img style='border:none' title='Click here to Part Details' style='cursor:hand' src='/images/product_icon.gif'/> </a>"+strId);
				
			}
			
			intCount++;
			return sbHtml.toString();
		}
		
		public String getHISFL()
		{
			db =  	(DynaBean) this.getCurrentRowObject();  
			strHisFl = GmCommonClass.parseNull(String.valueOf(db.get("HISFL")));
			
			String strAuditID = "";
			if (strHisFl.equals("Y"))
			{
				strId = GmCommonClass.parseNull(String.valueOf(db.get("APID")));
				strAuditID = GmCommonClass.parseNull(String.valueOf(db.get("AUDIT_ID")));
				strHisFl = strHistIntImgValue + "onClick=\"javascript:fnCallHistory('" +strId+"')\"/>";
			}
			return strHisFl; 
		}
		 /**getCHECKBOX - this method used to set the check box column in report
		 * @return
		 */
		public String getCHECKBOX()
		    {
		    	StringBuffer  strValue = new StringBuffer();
		    	DynaBean db =    (DynaBean) this.getCurrentRowObject();               
		    	String strPartNum   = GmCommonClass.parseNull(String.valueOf(db.get("ID")));
		    	String strVoidFl   = GmCommonClass.parseNull(String.valueOf(db.get("VOID_FL")));
		    	strId = GmCommonClass.parseNull(String.valueOf(db.get("APID")));
		    	String strUnitPrice = GmCommonClass.parseNull(String.valueOf(db.get("PRICE")));
		    	String strDisabled = "";
		    	
		    	if(strVoidFl.equals("Y"))
		    	{
		    		strDisabled = "disabled";
		    	}
		    	strValue.setLength(0);
		    	strValue.append("<input type='hidden' name='hAPPrice");
				strValue.append(String.valueOf(intCount));
				strValue.append("' value='");
				strValue.append(strId);
				strValue.append("'>");
				strValue.append("<input type='hidden' name='hUPrice");
				strValue.append(String.valueOf(intCount));
				strValue.append("' value='");
				strValue.append(strUnitPrice);
				strValue.append("'>");
		    	strValue.append("<input type='checkbox'" + strDisabled + " onClick=\"javascript:fnSetEditCharge('"+intCount+"','"+strPartNum+"')\" name='chk_rowwise"); 
		    	 strValue.append(String.valueOf(intCount));
		    	 strValue.append("'> ");
		    	 intCount++;
		       	return strValue.toString();               
		    } 
		
	}

 