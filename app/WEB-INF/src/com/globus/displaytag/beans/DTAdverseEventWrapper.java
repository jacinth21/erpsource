package com.globus.displaytag.beans;

import java.util.HashMap;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

public class DTAdverseEventWrapper extends TableDecorator
{
    private HashMap hmValues = new HashMap();
    private String strPatientId ="";
    private String strFormDate = "";
    private int count = 0;
	private String strEventId = "";
    
    public DTAdverseEventWrapper()
    {
        super();
    }
    
    public String getEVENT_ID()
    {
        hmValues =   (HashMap) this.getCurrentRowObject();
        //db =    (DynaBean) this.getCurrentRowObject();
        strPatientId   = (String)hmValues.get("PID");
        strFormDate =(String)hmValues.get("EXAMDT");
        strEventId =(String)hmValues.get("EVENT_ID");            
        
        StringBuffer sbHtml = new StringBuffer();
        sbHtml.append(" <a href= javascript:fnOpenForm('");
        sbHtml.append(strPatientId);
        sbHtml.append("','");
        sbHtml.append(strEventId);
        sbHtml.append("');> <img src=/images/pr.gif> </a> ");
        sbHtml.append(strEventId );
        
        return sbHtml.toString();
    }

}
