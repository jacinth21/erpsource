package com.globus.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;

public class DTExsReturnDetailsWrapper extends TableDecorator
{
    private DynaBean db ;
    private String strPartNum = "";
    private String strQty = "";
    private int intCount = 0;
    
    public String getPNUM()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strPartNum   = (String)db.get("PNUM");
        strQty   = GmCommonClass.parseZero(String.valueOf(db.get("QTY")));
        
        StringBuffer strValue = new StringBuffer();
        strValue.setLength(0);
        strValue.append("<input class=RightText type='checkbox' ");
        strValue.append(" name='chk");
        strValue.append(String.valueOf(intCount) );
        strValue.append("' value='") ;
        strValue.append(strPartNum) ;
       strValue.append("' id='") ;
        strValue.append(strQty) ;  
        strValue.append("'> &nbsp; ");
        strValue.append(strPartNum);
        
       intCount++;
        
        return strValue.toString();
    }
    
}
