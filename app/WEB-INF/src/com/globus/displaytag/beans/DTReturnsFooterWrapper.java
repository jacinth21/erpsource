
package com.globus.displaytag.beans;

import java.text.DecimalFormat;

import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.TableDecorator;
import org.apache.commons.beanutils.DynaBean;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger; 
import org.apache.log4j.Logger; 

    
public class DTReturnsFooterWrapper  extends TableDecorator{
        
        
        /**
        * DecimalFormat used to format money in getMoney().
        */

        private DynaBean db ;
        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
        
        /**
        * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
        */
        public DTReturnsFooterWrapper()
        {
            super();
        }
        
        /**
        *
        * @return String
        */
        
        public String getRID()
        {
            db =    (DynaBean) this.getCurrentRowObject();
            if (db.get("RID") == null )
            {
                return "";
            }
            else
            {
                String  strRaid = db.get("RID").toString();
                return  "<a href= javascript:fnViewReturns('" + strRaid + "')>" +strRaid+ "</a>" ;
            }
        }
}
