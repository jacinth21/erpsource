package com.globus.displaytag.beans;

/**
 * @author richardk
 *
 * Ths bean is used for VirtualSetWrapped screen format and for hyperlinks
 * 
 */

import java.text.DecimalFormat;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTSalesVPartDetailWrapper  extends TableDecorator{


     /**
     * DecimalFormat used to format money in getMoney().
     */
    private DynaBean db ;
    private DecimalFormat moneyFormat;
    private String strPartID ;
    private String strDistID ;
    private double dblFinal;
    private String strTemp;
    
    Logger log = GmLogger.getInstance(this.getClass().getName());
	
    
    /**
     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
     */
    public DTSalesVPartDetailWrapper()
    {
       super();
       this.moneyFormat = new DecimalFormat("###"); //$NON-NLS-1$
        //this.moneyFormat = new DecimalFormat("$ {0,number,.00}"); 
    }
    
    public String getPID()
    {
        db =  	(DynaBean) this.getCurrentRowObject();
        strDistID 	= db.get("DID").toString();
        strPartID 	= db.get("PID").toString();
        return "&nbsp;<a  href=\"javascript:fnViewPartActual( '" + strPartID + "', '" + strDistID + "' )\" >"   
        		+ strPartID + "</a>" ;
    }
    
    public String getSQTY()
    {
        db =  	(DynaBean) this.getCurrentRowObject();
        String strSetQty = GmCommonClass.parseZero(db.get("SQTY").toString());
        String strCrtFl = db.get("CRTFL").toString();
        
        if (strSetQty.equals("0")){
        	strSetQty = "-";
        }
        
        if(strCrtFl.equals("Y"))
        	return "&nbsp;"+"<font color=red>*</font>" + strSetQty  ;
        else
        	return "&nbsp;"+ strSetQty;
    }
    public String getTCOUNT()
    {
        db =  	(DynaBean) this.getCurrentRowObject();
        strDistID 	= db.get("DID").toString();
        strPartID 	= db.get("PID").toString();
        strTemp 	= db.get("TCOUNT").toString();
        
        return "&nbsp;<a  href=\"javascript:fnViewPartActual( '" + strPartID + "', '" + strDistID + "' )\" >"   
        		+ strTemp + "</a>" ;
    }
    
    
    
    /**
     * Returns the link to view set details by consignment details
     * @return String
     */
  /*  public double getPCONS()
    {
        db =  	(DynaBean) this.getCurrentRowObject();
        double dblUsage  	= Double.parseDouble(db.get("SETUSAGE").toString());
        double dblunUsed  	= Double.parseDouble(db.get("PUNUSED").toString());
        double dblSetQTY  	= Double.parseDouble(db.get("CQTY").toString());
        
        if (dblSetQTY == 0 )
        {
            return 0;
        }
        
        dblFinal =  (dblUsage + dblunUsed)/dblSetQTY;
        
        
        return Math.floor(dblFinal) ; //this.moneyFormat.format(dblFinal).toString();	
    }*/

    /**
     * Returns the link to view set details by consignment details
     * @return String
     */
    public String getCFL()
    {
        db =  	(DynaBean) this.getCurrentRowObject();
        String strCFlag 	= db.get("CFL").toString();
        strPartID 	= db.get("PID").toString();
        
        if (strCFlag.equals("Y") )
        {
            return "<a  href=\"javascript:fnViewPartCrossOver( '" + strPartID + "' )\" > "   
    		+ strCFlag + "</a>" ;

        }
        else
        {
            return strCFlag;
        }
    }
    public String getPDESC ()
    {   
    	String strPartDesc  = db.get("PDESC").toString(); 
    	String strTrade = GmCommonClass.getStringWithTM(strPartDesc);
    	return strTrade;
        
    }
    
}