package com.globus.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTRequestSwapWrapper extends TableDecorator {

	GmLogger log = GmLogger.getInstance(); // Gets an instance of Logger class
	private String strImagePath = GmCommonClass.getString("GMIMAGES");
	private String strClogFlag = "";
	private String strImgSrc;

	public String getREQID() {
		DynaBean db = (DynaBean) this.getCurrentRowObject();

		StringBuffer strValue = new StringBuffer();
		String strReqId = ((String) db.get("REQID"));
		String strConId = ((String) db.get("CONSID"));
		strClogFlag = String.valueOf(db.get("CLOG"));
		 
	 	 strImgSrc = "";
	 	if(strClogFlag.equals("N"))
        {
            strImgSrc = strImagePath+"/phone_icon.jpg";
        } 
        else
        {
            strImgSrc = strImagePath+"/phone-icon_ans.gif";
        }
	 	strValue.setLength(0);
	 	strValue.append("<a href=javascript:fnOpenOrdLogInv('" + strReqId + "')>  <img id='imgEdit' style='cursor:hand' src = "+ strImgSrc+ " title='Click to add comments' width='17' height='12' />  </a>" );
			 
		strValue.append("  <a href= javascript:fnViewDetails('" + strReqId + "','" + strConId + "')>" + strReqId + "</a>");

		return strValue.toString();

	}
	public String getCONSID() {
		DynaBean db = (DynaBean) this.getCurrentRowObject();

		StringBuffer strValue = new StringBuffer();
		String strConId = ((String) db.get("CONSID"));
		// if(strConId.equals("null")) strConId ="";
		log.debug("strConId is  " + strConId);

		strValue.setLength(0);
		if (!strConId.equals("A"))
			strValue.append("  <a href= javascript:fnPrintVer('" + strConId + "')>" + strConId + "</a>");
		else
			strValue.append("");
		return strValue.toString();

	}
}
