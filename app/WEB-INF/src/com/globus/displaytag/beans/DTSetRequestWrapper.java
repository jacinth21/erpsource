package com.globus.displaytag.beans;

import org.displaytag.decorator.TableDecorator;
import org.apache.commons.beanutils.DynaBean;
import com.globus.common.beans.GmLogger; 
import org.apache.log4j.Logger; 

	
public class DTSetRequestWrapper  extends TableDecorator{
		private DynaBean db ;
		
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
		public String getID()
		{
			db =  	(DynaBean) this.getCurrentRowObject();				
				String strRequestID = db.get("REQUEST_ID").toString();
				return  "<input type=radio name=request id="+strRequestID+">"  ;
		}
}
