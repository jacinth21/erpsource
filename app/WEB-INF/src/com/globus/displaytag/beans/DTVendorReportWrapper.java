package com.globus.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import com.globus.common.beans.GmCommonClass;

import org.apache.commons.beanutils.DynaBean;

import org.apache.commons.lang.StringEscapeUtils;

public class DTVendorReportWrapper extends TableDecorator {

	//private String strName;
	private String strValueFormatted;
	HashMap hmVendorDetails = new HashMap ();
	private DynaBean db;
	//private String strForeColor ="<span style=\"color:red;\">"+"(";
	//The below method name is changed for PMT-5022 which is to avoid special characters in excel.
	public String getVENDNAMEXLS() {
		db = (DynaBean) this.getCurrentRowObject();
		String strName = String.valueOf(db.get("NAME"));
		String strVendId = String.valueOf(db.get("ID"));
		return "<a href=javascript:fnCallEdit('" + strVendId + "')> " + strName + "</a>";
		
	}

	public String getPOID() {
		db = (DynaBean) this.getCurrentRowObject();
		//hmVendorDetails = (HashMap) this.getCurrentRowObject();
		//String strPOId = GmCommonClass.parseNull((String)hmVendorDetails.get("PO"));
		String strPOId = GmCommonClass.parseNull(String.valueOf(db.get("PO")));
		
		return "<a href=javascript:fnCallOpenPO('" + strPOId + "')> " + strPOId + "</a>";
	}

	public String getLOG() {
		db = (DynaBean) this.getCurrentRowObject();
		//hmVendorDetails = (HashMap) this.getCurrentRowObject();
		//String strCallFlag = GmCommonClass.parseNull((String) hmVendorDetails.get("LOG"));
		//String strWOId = GmCommonClass.parseNull((String) hmVendorDetails.get("WOID"));
		String strWOId = GmCommonClass.parseNull(String.valueOf(db.get("WOID")));
		String strCallFlag = GmCommonClass.parseNull(String.valueOf(db.get("LOG")));
		
		String strImg = "<img id=imgEdit style=cursor:hand src=images/phone_icon.jpg title='Click to Enter Call log'	width=22 height=20 onClick=javascript:fnOpenLog('"
				+ strWOId + "')>";
		if (strCallFlag.equals("Y")) {
			strImg = "<img id=imgEdit style=cursor:hand src=images/phone-icon_ans.gif title='Click to Enter Call log'	width=22 height=20 onClick=javascript:fnOpenLog('"
					+ strWOId + "')>";
		}
		return strImg;
	}

	public String getLASTCOMMENTSEXCL() {
		db = (DynaBean) this.getCurrentRowObject();
		//hmVendorDetails = (HashMap) this.getCurrentRowObject();
		//String strCommnets = GmCommonClass.parseNull(String.valueOf(hmVendorDetails.get("LASTCOMMENTS")));
		
		String strCommnets = GmCommonClass.parseNull(String.valueOf(db.get("LASTCOMMENTS")));
		if(!strCommnets.equals("null")){
			strCommnets = strCommnets.replaceAll("\\<[^\\>]*\\>","");
			strCommnets = strCommnets.replaceAll("&lt;","<").replaceAll("&gt;",">").replaceAll("&amp;","&");
    	}else{
    		strCommnets = "";
    	}
		return strCommnets.toString(); 
	}
	
	public String getDAYSVIEW() {
		db = (DynaBean) this.getCurrentRowObject();
		Integer intDays = 0;
		StringBuffer strValue = new StringBuffer();
		//hmVendorDetails = (HashMap) this.getCurrentRowObject();
		intDays = Integer.parseInt(GmCommonClass.parseNull(String.valueOf(db.get("DAYS"))));
		//intDays = Integer.parseInt(GmCommonClass.parseNull(String.valueOf(hmVendorDetails.get("DAYS"))));
		strValue.setLength(0);
		
		if (intDays < 0){
			intDays = Math.abs(intDays);
			strValue.append("<table><tr><td class=RightTextRed align=right> (" + intDays + ")</td></tr></table>");

		}else{
			strValue.append("<table><tr><td class=RightText align=right>  " + intDays + " </td></tr></table>");
		}
		return strValue.toString();
		
	}
	//The Below Code is added for PMT-5022,This code is added to fix the issue when the HTML code is rendered to wrapper class.
	//In DB for currency � is saved as &pound;,when clicking on Excel � is symbol is shown as &pound;.
	public String getCURRENCYXLS() {
		db = (DynaBean) this.getCurrentRowObject();
			
		String strCurrency = String.valueOf(db.get("CURRENCY"));
		strCurrency = StringEscapeUtils.unescapeHtml(strCurrency);
		return strCurrency.toString(); 
	}
	
	
}
