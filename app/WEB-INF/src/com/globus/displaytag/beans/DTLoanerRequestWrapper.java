package com.globus.displaytag.beans;

import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.TableDecorator;
import org.apache.commons.beanutils.DynaBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmCommonClass;
import java.util.HashMap;

public class DTLoanerRequestWrapper extends TableDecorator{
	
	HashMap hmSearchResult = new HashMap ();
	GmLogger log = GmLogger.getInstance(); // Gets an instance of Logger class
	String strImagePath;
	public DTLoanerRequestWrapper()
	    {
	        super();
	        strImagePath = GmCommonClass.getString("GMIMAGES");
	    }

	public String getREQDETAILID()
	    {
		 	hmSearchResult = 	(HashMap) this.getCurrentRowObject();
		// log.debug("hmSearchResult"+hmSearchResult);
		 	// <A HREF='#' onClick="self.opener.document.frmAccount.Txt_FromDate.value='06/30/2006'; self.opener.document.frmAccount.Txt_FromDate.focus(); window.close();">
		 	
		 	StringBuffer strValue = new StringBuffer(); 
		 	String strReqId =  ((String)hmSearchResult.get("REQDETAILID"));
		 	String strprdReqId =  ((String)hmSearchResult.get("PDTREQID"));
		 	String strStatus =  ((String)hmSearchResult.get("STSID"));
		 	String strHoldFl =  ((String)hmSearchResult.get("HOLDFL"));
		 	// Code to add the checkbox value
		 	strValue.append("<input type='hidden' value='");
			strValue.append(strStatus) ;
			strValue.append("' name='hstatus_");
			strValue.append(strReqId);
			strValue.append("'>");
			
			strValue.append("<input class=RightText type='radio' name='radPart' id='");
			strValue.append(strReqId );
			strValue.append("' value='") ;
			strValue.append(strReqId) ;
			strValue.append("' onClick=javascript:fnSelectPart('");
			strValue.append(strReqId+"','") ;
			strValue.append(strprdReqId) ;
			strValue.append("') > ") ;			
			strValue.append(strReqId) ;
			
			//adding the hold image if the status is 'Allocated'
			
	        	if (strStatus.equals("20") && strHoldFl.equalsIgnoreCase("Y")){
	        		strValue.append("<img id='imgHold' style='cursor:hand' src='");
	        		strValue.append(strImagePath+"/hold-icon.png'");
	        		strValue.append("title='Loaner on Hold' width='12' height='12' </img>");
	        	}
	        	 
    	   		
			
					
			//log.debug("String value which will be returned is " + strValue.toString());
			return strValue.toString();
			
	    }
	 
}
