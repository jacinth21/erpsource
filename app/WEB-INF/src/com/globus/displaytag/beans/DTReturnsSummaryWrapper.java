package com.globus.displaytag.beans;

import java.util.HashMap;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;

public class DTReturnsSummaryWrapper extends TableDecorator
{
    private DynaBean db ;
    private String strRAID = "";
    private String strValue = "";
    private String strValueFormatted = ""; 
    private String strType = "";
    
    
    public String getRAID()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strRAID   = (String)db.get("RAID");
        
        StringBuffer sbHtml = new StringBuffer();
        sbHtml.append(" <a href= javascript:fnReceiveReturns('");
        sbHtml.append(strRAID);
        sbHtml.append("');> <img src=/images/d.gif  alt=' View Detail'> </a>");
        sbHtml.append(" <a href= javascript:fnCallEditReturn('");
        sbHtml.append(strRAID);
        sbHtml.append("');> <img src=/images/icon_sales.gif alt='Receive' > </a>");
        sbHtml.append(" <a href= javascript:fnReconfigReturn('");
        sbHtml.append(strRAID);
        sbHtml.append("');> <img src=/images/consignment.gif alt='Reconfigure' > </a>");
        sbHtml.append(" <a href= javascript:fnPrintRA('");
        sbHtml.append(strRAID);
        sbHtml.append("');> <img src=/images/product_icon.gif alt='Print Version'> </a>");
        sbHtml.append(strRAID);
        
        return sbHtml.toString();
    }
    /**
     * Returns the link to view call log information  
     * @return String
     */
    public String getLOG ()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strRAID   = (String)db.get("RAID");
        strType = db.get("LOG").toString();

        if (strType.equals("Y") )
        {
            return "<img id='imgEditA' style='cursor:hand' src='/images/phone-icon_ans.gif'" +  
            " width='20' height='17'" + 
                "onClick=\"javascript:fnLog( '" + strRAID + "') \" />"; 
        }
        else
        {
            return "<img id='imgEditA' style='cursor:hand' src='/images/phone_icon.jpg'" +  
            " width='20' height='17' " + 
                "onClick=\"javascript:fnLog( '" + strRAID+ "') \" />";
        }
                
    }
    
    /**
     * Returns the link to view call log information  
     * @return String
     */
    public String getPARENTRMAID()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        if (db.get("PARENTRMAID") == null )
        {
            return "";
        }
        else
        {
            String  strRaid = db.get("PARENTRMAID").toString();
            return  "<a href= javascript:fnViewReturns('" + strRaid + "')>" +strRaid+ "</a>" ;
        }
    }
}
