package com.globus.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;

public class DTDemandGroupPartWrapper extends TableDecorator
{
    private DynaBean db ;
    private String strPartNum = "";
    private int intCount = 0;
    
    public String getPNUM()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strPartNum   = (String)db.get("PNUM");
        String strFormFieldName = "checkPartNumbers";
      
        StringBuffer strValue = new StringBuffer();
        strValue.setLength(0);
        strValue.append(" <html:multibox property=\"");
        strValue.append(strFormFieldName);
        strValue.append("\" value=\"");
        strValue.append(strPartNum);
        strValue.append("\" /> ");
        
        intCount++;
        
        return strValue.toString(); 
    }
    
}
