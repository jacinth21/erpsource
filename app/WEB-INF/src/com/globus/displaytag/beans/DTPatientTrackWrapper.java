package com.globus.displaytag.beans;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTPatientTrackWrapper extends TableDecorator{

		private HashMap hmValues = new HashMap();
	    private String strPatientId ="";
	    private String strPatientIde ="";
	    private String strStudyListId = "";
		private String strCheckSiteName = "";
	    
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.	    
	    public String getLINK()
	    {
	        hmValues =   (HashMap) this.getCurrentRowObject();
	        //db =    (DynaBean) this.getCurrentRowObject();
	        strPatientId   = (String)hmValues.get("ID");
	        strPatientIde   = (String)hmValues.get("IDENO");
	        strStudyListId =(String)hmValues.get("STUDYID");
	        strCheckSiteName =(String)hmValues.get("SITEID");
	        
	        
	        StringBuffer sbHtml = new StringBuffer();
	        sbHtml.append(" <a href=/gmRptPatientTracking.do?method=reportPatientTrackInfo&strOpt=Report&patient_Id=");
	        sbHtml.append(strPatientId);
	        sbHtml.append("&");
	        sbHtml.append("patient_Ide="+strPatientIde);
	        sbHtml.append("&");
	        sbHtml.append("studyListId="+strStudyListId);
	        sbHtml.append("&");	        
	        sbHtml.append("checkSiteName="+strCheckSiteName);
	        sbHtml.append("> <img src=/images/packslip.gif alt='Patient Tracker'> </a> ");
	        sbHtml.append(" <a href=/gmRptPatientTracking.do?method=reportPatientTrackForm&strOpt=Report&patient_Id=");
	        sbHtml.append(strPatientId);
	        sbHtml.append("&");
	        sbHtml.append("patient_Ide="+strPatientIde);
	        sbHtml.append("&");
	        sbHtml.append("studyListId="+strStudyListId);
	        sbHtml.append("&");	        
	        sbHtml.append("checkSiteName="+strCheckSiteName);
	        sbHtml.append("> <img src=/images/icon-letter.png alt='Form Tracker'> </a> ");

	        sbHtml.append((String)hmValues.get("IDENO") );
	        
	        return sbHtml.toString();
	    }
	    
	    
	    public String getWOPEN()
	    {
	        hmValues =   (HashMap) this.getCurrentRowObject();
	        //db =    (DynaBean) this.getCurrentRowObject();
	        strPatientId   = (String)hmValues.get("ID");
	        strPatientIde   = (String)hmValues.get("NAME");
	        strStudyListId =(String)hmValues.get("STUDYID");
	        strCheckSiteName =(String)hmValues.get("SITEID");
	        String strWOpenDate = (String)hmValues.get("W_OPEN");
	        String strWCloseDate = (String)hmValues.get("W_CLOSE");
	        
	        int month  = Integer.parseInt(strWOpenDate.substring(0, strWOpenDate.indexOf("/")));
	        int day  = Integer.parseInt(strWOpenDate.substring(strWOpenDate.indexOf("/")+1,strWOpenDate.lastIndexOf("/")))-1;
	        int year  = Integer.parseInt(strWOpenDate.substring(strWOpenDate.lastIndexOf("/")+1));
	        

	        Calendar clWOpenDate = Calendar.getInstance();
	        clWOpenDate.set(year, month, day);
	        
	        Calendar clCurrentDate = Calendar.getInstance();
	        String sbHtml = new String();
	        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy"); 
	        
	        Calendar clNewCurrentDate = Calendar.getInstance();
	        clNewCurrentDate.add(Calendar.MONTH, 1);

	        if(clCurrentDate.after(clWOpenDate)){
	        	sbHtml = (" ----- ");
	        }else if(clCurrentDate.equals(clWOpenDate)){
		        sbHtml = "<span style='color:green; font-weight: bold;'>"+sdf.format(clWOpenDate.getTime())+"</span>";
	        }else if (clCurrentDate.before(clWOpenDate) && clNewCurrentDate.after(clWOpenDate)){
	        	sbHtml = "<span style='color:orange; font-weight: bold;'>"+sdf.format(clWOpenDate.getTime())+"</span>";
	        }else{
	        	sbHtml = sdf.format(clWOpenDate.getTime());
	        } 
	        return sbHtml.toString();
	    }
	    
	    public String getWCLOSE()
	    {
	        hmValues =   (HashMap) this.getCurrentRowObject();
	        //db =    (DynaBean) this.getCurrentRowObject();
	        strPatientId   = (String)hmValues.get("ID");
	        strPatientIde   = (String)hmValues.get("NAME");
	        strStudyListId =(String)hmValues.get("STUDYID");
	        strCheckSiteName =(String)hmValues.get("SITEID");
	        
	        
	        String strWCloseDate = (String)hmValues.get("W_CLOSE");
	        String strMissingCurFl = GmCommonClass.parseNull((String)hmValues.get("MISSING_CUR_FL"));

	        int month  = Integer.parseInt(strWCloseDate.substring(0, strWCloseDate.indexOf("/")));
	        int day  = Integer.parseInt(strWCloseDate.substring(strWCloseDate.indexOf("/")+1,strWCloseDate.lastIndexOf("/")))-1;
	        int year  = Integer.parseInt(strWCloseDate.substring(strWCloseDate.lastIndexOf("/")+1));

	        Calendar clWCloseDate = Calendar.getInstance();
	        clWCloseDate.set(year, month, day);
	        
	        Calendar clCurrentDate = Calendar.getInstance();
	        String sbHtml = new String();
	        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy"); 
	        
	        Calendar clNewCurrentDate = Calendar.getInstance();
	        clNewCurrentDate.add(Calendar.MONTH, 1);

	        if(clCurrentDate.after(clWCloseDate)){
	        	if(strMissingCurFl.equalsIgnoreCase("Y")){
	        		sbHtml = "<span style='color:red; font-weight: bold;'>"+sdf.format(clWCloseDate.getTime())+"</span>";
	        	}else{
	        		sbHtml = sdf.format(clWCloseDate.getTime());
	        	}	
	        }else if(clCurrentDate.equals(clWCloseDate)){
		        sbHtml = "<span style='color:green; font-weight: bold;'>"+sdf.format(clWCloseDate.getTime())+"</span>";
	        }else if (clCurrentDate.before(clWCloseDate) && clNewCurrentDate.after(clWCloseDate)){
	        	sbHtml = "<span style='color:orange; font-weight: bold;'>"+sdf.format(clWCloseDate.getTime())+"</span>";
	        }else{
	        	sbHtml = sdf.format(clWCloseDate.getTime());
	        } 
	        return sbHtml.toString();
	    }

	    public String getWOPENEXP()
	    {
	        hmValues =   (HashMap) this.getCurrentRowObject();
	        //db =    (DynaBean) this.getCurrentRowObject();
	        strPatientId   = (String)hmValues.get("ID");
	        strPatientIde   = (String)hmValues.get("NAME");
	        strStudyListId =(String)hmValues.get("STUDYID");
	        strCheckSiteName =(String)hmValues.get("SITEID");
	        String strWOpenDate = (String)hmValues.get("W_OPEN");

	        int month  = Integer.parseInt(strWOpenDate.substring(0, strWOpenDate.indexOf("/")));
	        int day  = Integer.parseInt(strWOpenDate.substring(strWOpenDate.indexOf("/")+1,strWOpenDate.lastIndexOf("-")))-1;
	        int year  = Integer.parseInt(strWOpenDate.substring(strWOpenDate.lastIndexOf("/")+1));

	        Calendar clWOpenDate = Calendar.getInstance();
	        clWOpenDate.set(year, month, day);
	        
	        Calendar clCurrentDate = Calendar.getInstance();
	        String sbHtml = new String();
	        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy"); 
	        
	        Calendar clNewCurrentDate = Calendar.getInstance();
	        clNewCurrentDate.add(Calendar.MONTH, 1);

	        if(clCurrentDate.after(clWOpenDate)){
	        	sbHtml = (" ----- ");
	        }else if(clCurrentDate.equals(clWOpenDate)){
		        //sbHtml = "<span style='color:darkgreen; font-weight: bold;'>"+sdf.format(clWOpenDate.getTime())+"</span>";
	        	sbHtml = sdf.format(clWOpenDate.getTime());
	        }else if (clCurrentDate.before(clWOpenDate) && clNewCurrentDate.after(clWOpenDate)){
	        	//sbHtml = "<span style='color:darkorange; font-weight: bold;'>"+sdf.format(clWOpenDate.getTime())+"</span>";
	        	sbHtml = sdf.format(clWOpenDate.getTime());
	        }else{
	        	sbHtml = sdf.format(clWOpenDate.getTime());
	        } 
	        return sbHtml.toString();
	    }

	    public String getWCLOSEEXP()
	    {
	        hmValues =   (HashMap) this.getCurrentRowObject();
	        //db =    (DynaBean) this.getCurrentRowObject();
	        strPatientId   = (String)hmValues.get("ID");
	        strPatientIde   = (String)hmValues.get("NAME");
	        strStudyListId =(String)hmValues.get("STUDYID");
	        strCheckSiteName =(String)hmValues.get("SITEID");
	        
	        
	        String strWCloseDate = (String)hmValues.get("W_CLOSE");
	        String strMissingCurFl = GmCommonClass.parseNull((String)hmValues.get("MISSING_CUR_FL"));

	        int month  = Integer.parseInt(strWCloseDate.substring(0, strWCloseDate.indexOf("/")));
	        int day  = Integer.parseInt(strWCloseDate.substring(strWCloseDate.indexOf("/")+1,strWCloseDate.lastIndexOf("/")))-1;
	        int year  = Integer.parseInt(strWCloseDate.substring(strWCloseDate.lastIndexOf("/")+1));

	        Calendar clWCloseDate = Calendar.getInstance();
	        clWCloseDate.set(year, month, day);
	        
	        Calendar clCurrentDate = Calendar.getInstance();
	        String sbHtml = new String();
	        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy"); 
	        
	        Calendar clNewCurrentDate = Calendar.getInstance();
	        clNewCurrentDate.add(Calendar.MONTH, 1);

	        if(clCurrentDate.after(clWCloseDate)){
	        	if(strMissingCurFl.equalsIgnoreCase("Y")){
	        		//sbHtml = "<span style='color:darkred; font-weight: bold;'>"+sdf.format(clWCloseDate.getTime())+"</span>";
	        		sbHtml = sdf.format(clWCloseDate.getTime());
	        	}else{
	        		sbHtml = sdf.format(clWCloseDate.getTime());
	        	}	
	        }else if(clCurrentDate.equals(clWCloseDate)){
		        //sbHtml = "<span style='color:darkgreen; font-weight: bold;'>"+sdf.format(clWCloseDate.getTime())+"</span>";
	        	sbHtml = sdf.format(clWCloseDate.getTime());
	        }else if (clCurrentDate.before(clWCloseDate) && clNewCurrentDate.after(clWCloseDate)){
	        	//sbHtml = "<span style='color:darkorange; font-weight: bold;'>"+sdf.format(clWCloseDate.getTime())+"</span>";
	        	sbHtml = sdf.format(clWCloseDate.getTime());
	        }else{
	        	sbHtml = sdf.format(clWCloseDate.getTime());
	        } 
	        return sbHtml.toString();
	    }
	    
	    public String getNAME()
	    {
	        hmValues =   (HashMap) this.getCurrentRowObject();
	        //db =    (DynaBean) this.getCurrentRowObject();
	        strPatientId   = (String)hmValues.get("ID");
	        strPatientIde   = (String)hmValues.get("NAME");
	        String strStudyId =(String)hmValues.get("STUDYID");
	        String strPrdId =(String)hmValues.get("PERIOD_ID");
	        //log.debug("strPatientId========="+strPatientId);
	        //log.debug("strStudyId========="+strStudyId);
	        //log.debug("strPrdId========="+strPrdId);
	        StringBuffer sbHtml = new StringBuffer();
	        sbHtml.append(" <a href=/GmFormDataEntryServlet?strAction=Reload");
	        sbHtml.append("&");
	        sbHtml.append("Cbo_Patient="+strPatientId);
	        sbHtml.append("&");
	        sbHtml.append("Cbo_Study="+strStudyId);
	        sbHtml.append("&");	        
	        sbHtml.append("Cbo_Period="+strPrdId);
	        sbHtml.append("> <img src=/images/icon-letter.png alt='Form Data Entry'> </a> ");

	        sbHtml.append((String)hmValues.get("NAME") );
	        
	        return sbHtml.toString();
	    }
	    
}
