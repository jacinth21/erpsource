package com.globus.displaytag.beans;

import java.text.DecimalFormat;
import java.util.HashMap;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

public class DTProcessReturnWrapper extends TableDecorator
{
    /**
     * DecimalFormat used to format money in getMoney().
     */
    private DynaBean db ;
    private String strPnum = "";
    private double dblVal = 0;
    private DecimalFormat moneyFormat;
    private String formatVal;
    
    /**
     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
     */
    public DTProcessReturnWrapper()
    {
        super();
        this.moneyFormat = new DecimalFormat("$ #,###,###.00"); //$NON-NLS-1$
        //this.moneyFormat = new DecimalFormat("$ {0,number,.00}"); 
    }

    /**
     * Returns the link to view part information  
     * @return String
     */
    public String getPNUM ()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strPnum   = String.valueOf(db.get("PNUM"));
        return "<a  href=\"javascript:fnViewPartActual( '" + strPnum + "') \" />" 
        + strPnum + "</a>" ;                
    }
    
    public Double getTOTAL_VALUE()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        if (db.get("TOTAL_VALUE") == null )
        {
            dblVal = 0;
        }
        else
        {
        dblVal = Double.parseDouble((db.get("TOTAL_VALUE")).toString());        
        }        
       return  new Double(dblVal);
        
    }

}
