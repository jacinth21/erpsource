package com.globus.displaytag.beans;

/**
 * @author jkumar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */


import java.text.DecimalFormat;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;


public class DTFullTransferWrapper  extends TableDecorator{


     /**
     * DecimalFormat used to format money in getMoney().
     */
    private DecimalFormat moneyFormat;
    private HashMap hmFullTransfer ;
    private String formatVal;
    public StringBuffer  strValue = new StringBuffer();;
    private int intCount = 0;
    double dblVal ;
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    
    
    /**
     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
     */
    public DTFullTransferWrapper()
    {
        super();
    }

    public String getTXNID()
    {
        hmFullTransfer =   (HashMap) this.getCurrentRowObject();
        String strTxnId =(String) hmFullTransfer.get("TXNID");
        String strTxnType =  ((String)hmFullTransfer.get("TYPE"));
            
        /*
         * The transaction type can be either "Consignment" or "Return"
         * If the transaction is consignment, then a specific javascript needs to be called for showing the details of the consignment in a pop-up
         * If the transaction is return, then a specific javascript needs to be called for showing the details of the return in a pop-up
         */
               
        if (strTxnType.equals("Consignment"))
        {
            return  "<a href= javascript:fnCallCSGInfo('" + strTxnId + "')>" + strTxnId + "</a>" ;
        }
        else
        {
            return  "<a href= javascript:fnCallRMAInfo('" + strTxnId + "')>" + strTxnId + "</a>" ;        
         }
    }
    
    public String getCHKBOX()
    {
        hmFullTransfer =   (HashMap) this.getCurrentRowObject();
        String strTxnId =(String) hmFullTransfer.get("TXNID");
        String strTxnType =  ((String)hmFullTransfer.get("TYPE"));
        
        if (strTxnType.equals("Consignment"))
            strTxnType = "90360"; // 90360 refers to the link type COnsignment
        else
            strTxnType = "90361"; // 90361 refers to the link type Returns
        
        strValue.setLength(0);
        strValue.append("<input class=RightText type='checkbox' checked = 'true' name='rad");
        strValue.append(String.valueOf(intCount) );
        strValue.append("' value='") ;
        strValue.append(strTxnId) ;
        strValue.append("' id='") ;
        strValue.append(strTxnType) ; 
        strValue.append("' onClick=javascript:fnOnCheckRad();> ") ; 
        
        intCount++;
        return strValue.toString();
    }
    
    
    public String getSETID()
    {
        hmFullTransfer =   (HashMap) this.getCurrentRowObject();
        String strSetId =(String) hmFullTransfer.get("SETID");
        
        if (strSetId.equals("/"))
        {
            return "";
        }
        else {
            return GmCommonClass.getStringWithTM(strSetId);
        }
    }
    
    
}
