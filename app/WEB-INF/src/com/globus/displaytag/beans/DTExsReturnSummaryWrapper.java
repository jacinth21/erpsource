package com.globus.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;
import com.globus.common.beans.GmCommonClass;

public class DTExsReturnSummaryWrapper extends TableDecorator
{
    private DynaBean db ;
    private String strRAID = "";
    private String strConsignId = "";
    private String strValue = "";
    private String strValueFormatted = ""; 
    private String strType = "";
    private String strTID= "";
    
    public String getCONSIGNID()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strConsignId   = (String)db.get("CONSIGNID");
        
        
        StringBuffer sbHtml = new StringBuffer();
        sbHtml.append(" <a href= javascript:fnCallCSGExcess('");
        sbHtml.append(strConsignId);
        sbHtml.append("');> <img src=/images/icon_sales.gif> </a>");
        sbHtml.append(" <a href= javascript:fnPrintConsignVer('");
        sbHtml.append(strConsignId);
        sbHtml.append("');> ");
        sbHtml.append(strConsignId);
        sbHtml.append("</a> ");
        
        return sbHtml.toString();
    }
    
        
    /**
     * Returns the link to view call log information  
     * @return String
     */
    public String getCLOG ()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strConsignId   = (String)db.get("CONSIGNID");
        strType = db.get("CLOG").toString();

        if (strType.equals("Y") )
        {
            return "<img id='imgEditA' style='cursor:hand' src='/images/phone-icon_ans.gif'" +  
            " width='20' height='17'" + 
                "onClick=\"javascript:fnLog( '"+1221+"','" + strConsignId + "') \" />"; 
        }
        else
        {
            return "<img id='imgEditA' style='cursor:hand' src='/images/phone_icon.jpg'" +  
            " width='20' height='17' " + 
                "onClick=\"javascript:fnLog( '"+1221+"','" + strConsignId+ "') \" />";
        }
                
    }
    
    /**
     * Returns the link to view call log information  
     * @return String
     */
    public String getRMAID()
    {
        db =    (DynaBean) this.getCurrentRowObject();
     
        String  strRaid = db.get("RMAID").toString();
        return  "<a href= javascript:fnPrintReturnVer('" + strRaid + "')>" +strRaid+ "</a>" ;
     }

    /**
     * Returns the link to view call log information  
     * @return String
     */
    public String getRLOG()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strRAID   = (String)db.get("RMAID");
        strType = db.get("RLOG").toString();

        if (strType.equals("Y") )
        {
            return "<img id='imgEditA' style='cursor:hand' src='/images/phone-icon_ans.gif'" +  
            " width='20' height='17'" + 
                "onClick=\"javascript:fnLog( '"+1216+"','" + strRAID + "') \" />"; 
        }
        else
        {
            return "<img id='imgEditA' style='cursor:hand' src='/images/phone_icon.jpg'" +  
            " width='20' height='17' " + 
                "onClick=\"javascript:fnLog( '"+1216+"','" + strRAID+ "') \" />";
        }
                
    }
    
    /**
	 * Returns the link to call edit transfer  
	 * @return String
	 */
	public String getTID() {
		db = (DynaBean) this.getCurrentRowObject();

		String strTid = GmCommonClass.parseNull((String)db.get("TID"));
		return "<a href= javascript:fnCallEditTransfer('" + strTid + "')>" + strTid + "</a>";
	}

}
