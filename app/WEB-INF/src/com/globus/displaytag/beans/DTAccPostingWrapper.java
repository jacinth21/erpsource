/*
 * Created on Apr 27, 2006
 * 
 * TODO To change the template for this generated file go to Window - Preferences - Java - Code
 * Style - Code Templates
 */
package com.globus.displaytag.beans;

import java.text.DecimalFormat;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmLogger;

/**
 * @author jkumar
 *
 *         TODO To change the template for this generated type comment go to Window - Preferences -
 *         Java - Code Style - Code Templates
 */
public class DTAccPostingWrapper extends TableDecorator {
  /**
   * DecimalFormat used to format money in getMoney().
   */
  private DecimalFormat moneyFormat;
  private DynaBean db;
  double dbDBamt = 0.0;
  double dbCRamt = 0.0;

  String strDb = "";
  String strCostId = "";
  String strTXNId = "";
  String strTXNTypeId = "";
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  /**
   * Creates a new Wrapper decorator who's job is to reformat some of the data located in our
   * TestObject's.
   */
  public DTAccPostingWrapper() {
    super();
    // Formats for displaying dates and money.
    this.moneyFormat = new DecimalFormat("#,###,##0.00#"); //$NON-NLS-1$
  }

  /**
   * Returns the date as a String in MM/dd/yy format.
   * 
   * @return formatted date
   */
  /*
   * public String getINV_DATE() { return this.dateFormat.format(((DynaBean)
   * this.getCurrentRowObject()).get("INV_DATE")); }
   */

  /**
   * Returns the Payment Amount as a String in $ #,###,###.00 format.
   * 
   * @return String
   */
  public String getDRAMT() {

    db = (DynaBean) this.getCurrentRowObject();
    dbDBamt = Double.parseDouble((db.get("DRAMT")).toString());
    strCostId = db.get("COSTID").toString();

    if (strCostId.equals("0") && dbDBamt == 0) {
      return "";
    } else if (dbDBamt == 0) {
      return "-";
    } else {
      return this.moneyFormat.format(db.get("DRAMT"));
    }

  }

  /**
   * Returns the Payment Amount as a String in $ #,###,###.00 format.
   * 
   * @return String
   */
  public String getCRAMT() {

    db = (DynaBean) this.getCurrentRowObject();
    dbCRamt = Double.parseDouble((db.get("CRAMT")).toString());
    strCostId = db.get("COSTID").toString();

    if (strCostId.equals("0") && dbCRamt == 0) {
      return "";
    } else if (dbCRamt == 0) {
      return "-";
    } else {
      return this.moneyFormat.format(db.get("CRAMT"));
    }
  }

  public String getTXNID() {
    db = (DynaBean) this.getCurrentRowObject();
    // strTXNTypeId = String.valueOf(db.get("TXNTYPEID"));
    strTXNId = String.valueOf(db.get("TXNID"));
    return "&nbsp;<a href=javascript:fnTxnDetail('" + strTXNId + "')> " + strTXNId + "</a>";
  }

  /**
   * Returns the Payment Amount as a String in $ #,###,###.00 format.
   * 
   * @return String
   */
  public String getCORP_DR_AMT() {

    db = (DynaBean) this.getCurrentRowObject();
    dbDBamt = Double.parseDouble((db.get("CORP_DR_AMT")).toString());
    strCostId = db.get("COSTID").toString();

    if (strCostId.equals("0") && dbDBamt == 0) {
      return "";
    } else if (dbDBamt == 0) {
      return "-";
    } else {
      return this.moneyFormat.format(db.get("CORP_DR_AMT"));
    }


  }

  /**
   * Returns the Payment Amount as a String in $ #,###,###.00 format.
   * 
   * @return String
   */
  public String getCORP_CR_AMT() {

    db = (DynaBean) this.getCurrentRowObject();
    dbCRamt = Double.parseDouble((db.get("CORP_CR_AMT")).toString());
    strCostId = db.get("COSTID").toString();

    if (strCostId.equals("0") && dbCRamt == 0) {
      return "";
    } else if (dbCRamt == 0) {
      return "-";
    } else {
      return this.moneyFormat.format(db.get("CORP_CR_AMT"));
    }
  }


}
