package com.globus.displaytag.beans;
import java.text.DecimalFormat;

import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.TableDecorator;
import org.apache.commons.beanutils.DynaBean;

import com.globus.common.beans.GmCommonClass;

public class DTParDetailWrapper extends TableDecorator{

	
	 private String strParNumber;
	    private String strType = "";
	    private String strSETID = "";
	    private String strSETDESC = "";
	    private DynaBean db ;
	    
	    public StringBuffer  strValue = new StringBuffer();
	   
	    public String getSETID()
	    {
	    	String strLimitedAccess= GmCommonClass.parseNull((String)getPageContext().getRequest().getAttribute("DMNDSHT_LIMITED_ACC"));
	    	String strImgFun = "#";
	    	String strLinkFun = "#";
	    	if(!strLimitedAccess.equalsIgnoreCase("true")){
	    		strImgFun="javascript:fnViewC('" + strSETID + "')";
	    		strLinkFun="javascript:fnViewDetails('" +strSETID +"','" + strSETDESC +"')";
	    	}
	    	
	        db =    (DynaBean) this.getCurrentRowObject();
	        
	        strSETID = String.valueOf(db.get("SETID"));
	        strSETDESC = String.valueOf(db.get("SETDESC"));
	        strSETDESC =  strSETDESC.replaceAll(" ", "-"); 
	        
	       // System.out.println("PARVALUE : "+strPar);        
	        strValue.setLength(0);
	         
	       
	        	strValue.append("    <img style='cursor:hand' src='/images/consignment.gif'" +  
	             "border='0'" + 
	                 "onClick="+strImgFun +" />" ); 
	        	
	         strValue.append("  <a href= "+strLinkFun + ">"+strSETID+"</a>") ;
	        
	        return strValue.toString();
	        //return "<input type = text name = parvalue size = 10 value="+strPar+">";
	        
	    }
}
