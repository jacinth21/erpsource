    /**
     * Licensed under the Artistic License; you may not use this file
     * except in compliance with the License.
     * You may obtain a copy of the License at
     *
     *      http://displaytag.sourceforge.net/license.html
     *
     * THIS PACKAGE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
     * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
     * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
     */
    package com.globus.displaytag.beans;

    import org.displaytag.decorator.TableDecorator;
    import org.apache.commons.beanutils.DynaBean;
    import com.globus.common.beans.GmLogger;
    import org.apache.log4j.Logger;


    /**
     * This class is a decorator of the TestObjects that we keep in our List. This class provides a number of methods for
     * formatting data, creating dynamic links, and exercising some aspects of the display:table API functionality.
     * @author 
     * @author Vamsi Nagavarapu
     * @version $Revision$ ($Author$)
     */
    public class GmClinicalSurgeonSetupWrapper extends TableDecorator
    {

        private String strId;
        private DynaBean db ;
        /**
         * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
         */
        public GmClinicalSurgeonSetupWrapper()
        {
            super();
        }

        /**
         * Returns the Payment Amount  as a String in $ #,###,###.00 format.
         * @return String
         */
        public String getSID()
        {
            db =    (DynaBean) this.getCurrentRowObject();
            strId = String.valueOf(db.get("SID"));
            
            return "<a href=javascript:fnEditId('" + strId + "')>  <img width='39' height='19' title='Click here to edit details' style='cursor:hand' src='/images/edit.jpg'/>  </a>" ;
        }

    }
