package com.globus.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;

public class DTParWrapper extends TableDecorator
{
    private String strPar; 
    private String strCom;
    
    private String strParNumber;
    private String strType = "";
    private String strHistory = "";
    private DynaBean db ;
    private int intCount = 0;
    public StringBuffer  strValue = new StringBuffer();
    String strImagePath = GmCommonClass.getString("GMIMAGES");
    
    /**
     * Returns the string for text field.
     * @return String
     */
    public String getPARVALUE()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strPar = String.valueOf(db.get("PARVALUE"));
        strHistory = String.valueOf(db.get("HISTORY_FL"));
        strParNumber   = (String)db.get("PNUM");
        
       // System.out.println("PARVALUE : "+strPar);        
        strValue.setLength(0);
        strValue.append("<input type = text name = parvalue");
        strValue.append(String.valueOf(intCount) );
        strValue.append(" size = 10 value='");
        strValue.append(strPar);
        strValue.append("'>");
       
        intCount++;
        return strValue.toString();
        //return "<input type = text name = parvalue size = 10 value="+strPar+">";
        
    }
    
    public String getHISTORY()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        
        strHistory = String.valueOf(db.get("HISTORY_FL"));
        
        
       // System.out.println("PARVALUE : "+strPar);        
        strValue.setLength(0);
         
      
        if (strHistory.equals("Y") )
        {
        	strValue.append("    <img style='cursor:hand' src='/images/icon_History.gif'" +  
             "border='0'" + 
                 "onClick=\"javascript:fnParHistory( '"+ strParNumber +"','" + 1231 + "') \" />" ); 
        }
        
        return strValue.toString();
        //return "<input type = text name = parvalue size = 10 value="+strPar+">";
        
    }
    
   /* public String getCOMMENTS()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strCom = GmCommonClass.parseNull(String.valueOf(db.get("COMMENTS")));
       
        System.out.println("COMMENTS : "+ strCom);        
        return "<textarea name = txt_LogReason  class=InputArea rows=3 cols=30>" + strCom + "</textarea>";
    }
    */
    
    public String getCOMMENTS()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strParNumber   = (String)db.get("PNUM");
        strType = db.get("RLOG").toString();

                 
        if (strType.equals("Y") )
        {
            return "<img id='imgEditA' style='cursor:hand' src='/images/phone-icon_ans.gif'" +  
            " width='20' height='17'" + 
                "onClick=\"javascript:fnLog( '"+1231+"','" + strParNumber + "') \" />"; 
        }
        else
        {
            return "<img id='imgEditA' style='cursor:hand' src='/images/phone_icon.jpg'" +  
            " width='20' height='17' " + 
                "onClick=\"javascript:fnLog( '"+1231+"','" + strParNumber + "') \" />";
        }
                
    }
}


