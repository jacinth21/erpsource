package com.globus.displaytag.beans;

/**
 * @author jkumar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */


import java.text.DecimalFormat;
import java.util.HashMap;

import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.TableDecorator;
import org.apache.commons.beanutils.DynaBean;

import com.globus.common.beans.GmLogger; 
import org.apache.log4j.Logger; 
import com.globus.common.beans.GmCommonClass;


public class DTSetTransferWrapper  extends TableDecorator{


     /**
     * DecimalFormat used to format money in getMoney().
     */
    private DecimalFormat moneyFormat;
    private HashMap hmFullTransfer ;
    private String formatVal;
    public StringBuffer  strValue = new StringBuffer();;
    private int intCount = 0;
    double dblVal ;
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    
    
    /**
     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
     */
    public DTSetTransferWrapper()
    {
        super();
    }

    public String getPNUM()
    {
        hmFullTransfer =   (HashMap) this.getCurrentRowObject();
        String strPartNum = (String) hmFullTransfer.get("PNUM"); 

            
        /*
         * The transaction type can be either "Consignment" or "Return"
         * If the transaction is consignment, then a specific javascript needs to be called for showing the details of the consignment in a pop-up
         * If the transaction is return, then a specific javascript needs to be called for showing the details of the return in a pop-up
         */
       
            return  "<a href=\"javascript:fnViewPartActual('" + strPartNum + "')\";>" + strPartNum + "</a>" ;
    }
    
    public String getCHKBOX()
    {
        hmFullTransfer =   (HashMap) this.getCurrentRowObject();
        String strPartNum =(String) hmFullTransfer.get("PNUM");
        String strQtyOnHand =  GmCommonClass.parseZero((String)hmFullTransfer.get("QTY"));
        String strSetQty = GmCommonClass.parseZero((String)hmFullTransfer.get("SETQTY"));
        int intQty = Integer.parseInt(strQtyOnHand);
        String strIsChecked = "checked";

        if (strSetQty.equals("0") || intQty <=0)
        {
            strIsChecked = "";
        }
        
        strValue.setLength(0);
        strValue.append("<input class=RightText type='checkbox' ");
        strValue.append(strIsChecked);
        strValue.append(" name='rad");
        strValue.append(String.valueOf(intCount) );
        strValue.append("' value='") ;
        strValue.append(strPartNum) ;
        strValue.append("' id='") ;
        strValue.append(strQtyOnHand) ; 
        strValue.append("' onClick=javascript:fnOnCheckRad(); > ") ;
        
        return strValue.toString();
    }
    
    public String getTSFQTY()
    {
        hmFullTransfer =   (HashMap) this.getCurrentRowObject();
        String strPartNum =GmCommonClass.parseNull((String) hmFullTransfer.get("PNUM"));
        String strTsfQty =  GmCommonClass.parseZero((String)hmFullTransfer.get("TSFQTY"));
        int intTsfQty = Integer.parseInt(strTsfQty);
        // check the transfer Qty is greater than zero, then put as 0 value.
        if(intTsfQty <0){
        	intTsfQty = 0;
        }
        strValue.setLength(0);
        strValue.append("<input class=RightText type='input' size='4' name='Txt_TsfQty");
        strValue.append(String.valueOf(intCount) );
        strValue.append("' value='") ;
        strValue.append(intTsfQty) ;
        strValue.append("' > ") ;
        
        intCount++;
        return strValue.toString();
    }
    
    
}
