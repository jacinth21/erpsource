 
package com.globus.displaytag.beans;

import java.util.HashMap;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;


 
public class DTAccountGroupPriceWrapper extends TableDecorator
{

    private DynaBean db;
    private String strValue =""; 
    
   
    private String strSetNM = ""; 
    private String strSetid = ""; 
	 
	 
	
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
      
     

    public String getSETNM()
    {
    	 
    	db =    (DynaBean) this.getCurrentRowObject();
    	strSetid = String.valueOf(db.get("SETID"));
    	strSetNM = String.valueOf(db.get("SETNM"));
    	  
     
    	 
    		strValue =  "&nbsp;<a title='Click to open part history'><img id='imgEdit' style='cursor:hand' src='/images/icon_info.gif'" +  
	        "onClick=\"javascript:fnInitPriceReq( '"+ strSetid  + "') \" /></a>&nbsp;" + strSetNM; 
        	
        
        return strValue;
    }

     
}
