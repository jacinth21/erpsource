/**
 * Licensed under the Artistic License; you may not use this file
 * except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://displaytag.sourceforge.net/license.html
 *
 * THIS PACKAGE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */
package com.globus.displaytag.beans;

import java.text.DecimalFormat;
import java.util.HashMap;

import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.TableDecorator;
import org.apache.commons.beanutils.DynaBean;
import com.globus.common.beans.GmCommonClass;


/**
 * This class is a decorator of the TestObjects that we keep in our List. This class provides a number of methods for
 * formatting data, creating dynamic links, and exercising some aspects of the display:table API functionality.
 * @author epesh
 * @author Fabrizio Giustina
 * @version $Revision$ ($Author$)
 */
public class DTProdSetListWrapper extends TableDecorator
{

    private String strId;
    private String strName;
    HashMap hmSetListReq = new HashMap ();
    /**
     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
     */
    public DTProdSetListWrapper()
    {
        super();
    }

    /**
     * Returns the Payment Amount  as a String in $ #,###,###.00 format.
     * @return String
     */
    public String getID()
    {
	 	hmSetListReq = 	(HashMap) this.getCurrentRowObject();
    	strId = (String) hmSetListReq.get("ID");
    	strName = (String)hmSetListReq.get("NAME");
    	strName = strName.replaceAll(" ","&nbsp;");
    	return "<a href=javascript:fnCallSetMap('" + 
         strId + "','" + strName + "')> "  + strId + "</a>" ;
    }

}
