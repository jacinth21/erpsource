
/**
 * Licensed under the Artistic License; you may not use this file
 * except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://displaytag.sourceforge.net/license.html
 *
 * THIS PACKAGE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */
package com.globus.displaytag.beans;
import org.displaytag.decorator.TableDecorator;
import java.util.HashMap;
import com.globus.common.beans.*;


/**
 * This class is a decorator of the TestObjects that we keep in our List. This class provides a number of methods for
 * formatting data, creating dynamic links, and exercising some aspects of the display:table API functionality.
 * @author epesh
 * @author Fabrizio Giustina
 * @version $Revision$ ($Author$)
 */
public class DTPDSubAsmSearchWrapper extends TableDecorator
{

    private HashMap hmMap;
    private String strValue;
    private String strValueFormatted;

    /**
     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
     */
    public DTPDSubAsmSearchWrapper()
    {
        super();
    }

    /**
     * Returns the Payment Amount  as a String in $ #,###,###.00 format.
     * @return String
     */
    public String getPARTDESC()
    {
    	hmMap 	 =	(HashMap) this.getCurrentRowObject();
        strValue = (String)hmMap.get("PARTDESC");
        strValueFormatted = GmCommonClass.getStringWithTM(strValue);
	    return strValueFormatted;
    }

    public String getPROJECTNAME()
    {
    	hmMap =  	(HashMap) this.getCurrentRowObject();
        strValue = (String)hmMap.get("PROJECTNAME");
        strValueFormatted = GmCommonClass.getStringWithTM(strValue);
	    return strValueFormatted;
    }
}
