package com.globus.displaytag.beans;

import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.TableDecorator;
import org.apache.commons.beanutils.DynaBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmCommonClass;
import java.util.HashMap;

public class DTCustSvcSearchWrapper extends TableDecorator{
	
	HashMap hmSearchResult = new HashMap ();
	GmLogger log = GmLogger.getInstance(); // Gets an instance of Logger class
	
	public DTCustSvcSearchWrapper()
	    {
	        super();
	    	
	    }

	 public String getACCRADBUTTON()
	    {
		 	hmSearchResult = 	(HashMap) this.getCurrentRowObject();
		 
		 	// <A HREF='#' onClick="self.opener.document.frmAccount.Txt_FromDate.value='06/30/2006'; self.opener.document.frmAccount.Txt_FromDate.focus(); window.close();">
		 	
		 	StringBuffer strValue = new StringBuffer(); 
		 	String strValueFormatted =  ((String)hmSearchResult.get("ACCID"));
		 	String strAccName = ((String)hmSearchResult.get("ACCNAME"));
	        
		 	// Code to add the checkbox value
			strValue.append("<input class=RightText type='radio' name='rad' id='");
			strValue.append(strValueFormatted );
			strValue.append("' value='") ;
			strValue.append(strValueFormatted) ;
			strValue.append("' onClick='javascript:fnSelectAcc(") ;
			strValue.append(strValueFormatted) ;
			strValue.append(" , \" " ) ;
			strValue.append(strAccName) ;
			strValue.append(" \"  )' > ") ;
					
			//log.debug("String value which will be returned is " + strValue.toString());
			return strValue.toString();
			
	    }
	 
	 public String getREPRADBUTTON()
	    {
		 	hmSearchResult = 	(HashMap) this.getCurrentRowObject();
		 
		 	// <A HREF='#' onClick="self.opener.document.frmAccount.Txt_FromDate.value='06/30/2006'; self.opener.document.frmAccount.Txt_FromDate.focus(); window.close();">
		 	
		 	StringBuffer strValue = new StringBuffer(); 
		 	String strSRepId =  ((String)hmSearchResult.get("SREPID"));
		 	String strSRepName =  ((String)hmSearchResult.get("SREPNAME"));
	        
		 	// Code to add the checkbox value
			strValue.append("<input class=RightText type='radio' name='radSRep' id='");
			strValue.append(strSRepId );
			strValue.append("' value='") ;
			strValue.append(strSRepName) ;
			strValue.append("' onClick='javascript:fnSelectRep(this");
			//strValue.append("' onClick='javascript:fnSelectRep( ");
			//strValue.append(strSRepId) ;
			//strValue.append(" , ") ;
			//strValue.append(strSRepName) ;
			strValue.append(")' > ") ;
					
			//log.debug("String value which will be returned is " + strValue.toString());
			return strValue.toString();
			
	    }
	 
	 public String getPARTRADBUTTON()
	    {
		 	hmSearchResult = 	(HashMap) this.getCurrentRowObject();
		 
		 	// <A HREF='#' onClick="self.opener.document.frmAccount.Txt_FromDate.value='06/30/2006'; self.opener.document.frmAccount.Txt_FromDate.focus(); window.close();">
		 	
		 	StringBuffer strValue = new StringBuffer(); 
		 	String strPartNum =  ((String)hmSearchResult.get("PNUM"));
		 	String strPartDesc =  ((String)hmSearchResult.get("PDESC"));
	        
		 	// Code to add the checkbox value
			strValue.append("<input class=RightText type='radio' name='radPart' id='");
			strValue.append(strPartNum );
			strValue.append("' value='") ;
			strValue.append(strPartDesc) ;
			strValue.append("' onClick='javascript:fnSelectPart(this");
			strValue.append(")' > ") ;
					
			//log.debug("String value which will be returned is " + strValue.toString());
			return strValue.toString();
			
	    }
	 
	 public String getPDESC()
	 {
		 hmSearchResult = 	(HashMap) this.getCurrentRowObject();
		 String strPartDescTM =  GmCommonClass.getStringWithTM(((String)hmSearchResult.get("PDESC")));
		 return strPartDescTM;
	 }
}
