/*
 * Created on Apr 20, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.globus.displaytag.beans;

/**
 * @author jkumar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;
import java.text.DecimalFormat;
import org.apache.commons.beanutils.DynaBean;

	/**
	 * This class is a decorator of the TestObjects that we keep in our List. This class provides a number of methods for
	 * formatting data, creating dynamic links, and exercising some aspects of the display:table API functionality.
	 * @author epesh
	 * @author Fabrizio Giustina
	 * @version $Revision$ ($Author$)
	 */
	public class DTDHRWrapper extends TableDecorator
	{

	   //  private ArrayList al;
		private DynaBean db;
	    private StringBuffer  strValue;
	    private double dblTot;
	    private String strDHRid;
	    private String strValueFormatted;
	    private DecimalFormat moneyFormat;
	    	    	  
	    /**
	     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
	     */
	    public DTDHRWrapper()
	    {
	    	super();
	    	strValue	= new StringBuffer();
	    	this.moneyFormat = new DecimalFormat("$#,###,###.00"); //$NON-NLS-1$
	    }

	    public String getID()
	    {
	    	db = 	(DynaBean) this.getCurrentRowObject();
	    	if (db.get("ID").equals(null))
	        {
	    		//System.out.println(" Inside NUll Cost");
	            return "-";
	        }
	        else {
	        	strDHRid =(String) db.get("ID");
	         return "<a href=javascript:fnCallDHR('" + 
	         strDHRid + "')> "  + strDHRid + "</a>" ;
	        }
	    }
	    
		public String getCNUM()
	    {
	       	db = 	(DynaBean) this.getCurrentRowObject();
	       	
	       	dblTot = Double.parseDouble((db.get("TCOST")).toString());
	       	strDHRid = (String)db.get("ID");
	        strValueFormatted =  ((String)db.get("ID")).replaceAll("-","");
	        
	        // hidden value to fetch the amount 
	        strValue.setLength(0);
	       strValue.append("<input type='hidden' value='");
			strValue.append(dblTot) ;
			strValue.append("'name='hDHRAmt");
			strValue.append(strValueFormatted);
			strValue.append("'>");
			
	        // Hidded value to fetch the ID
	        strValue.append("<input type='hidden' value='");
			strValue.append(strDHRid) ;
			strValue.append("' name='hDHRID_");
			strValue.append(strValueFormatted);
			strValue.append("'>");

			
			// Code to add the checkbox value
			strValue.append("<input class=RightText type='checkbox' name='rad' id='");
			strValue.append(strValueFormatted );
			strValue.append("'' value='") ;
			strValue.append(strValueFormatted) ;
			strValue.append("' onClick='fnCalculateAmount(this);'>&nbsp;") ;
					
			//System.out.println(strValue.toString());
			return strValue.toString();
			}
		
		public String getPAYAMT(){
			String strPaymentAmt = "-";
			double dbPaymentAmt = 0;
			db = 	(DynaBean) this.getCurrentRowObject();
			Object objPaymentAmt =(db.get("PAYAMT"));
			
			if(objPaymentAmt != null){
				dbPaymentAmt = Double.parseDouble(objPaymentAmt.toString());
				strPaymentAmt = moneyFormat.format(dbPaymentAmt);
			}

			return strPaymentAmt;
		}
	}


