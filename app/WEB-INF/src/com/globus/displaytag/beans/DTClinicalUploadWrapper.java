package com.globus.displaytag.beans;


import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.jsp.PageContext;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;
import com.globus.clinical.forms.GmClinicalDownloadForm;
import com.globus.common.beans.GmLogger;

public class DTClinicalUploadWrapper extends TableDecorator{

		private HashMap hmValues = new HashMap();
		private String strFileId = "";
	    
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
		
	    public String getID()
	    {	    	
	    	hmValues =   (HashMap) this.getCurrentRowObject();
	        strFileId   = (String)hmValues.get("ID");	  
	        StringBuffer sbHtml = new StringBuffer();
	        sbHtml.append(" <a href=javascript:fnOpenForm(");
	        sbHtml.append(strFileId);
	        sbHtml.append(");>");
	        sbHtml.append("<img border='0' src=/images/csv_icon.gif></a>");	     
	        return sbHtml.toString();
	    }  	   
	    
	    
}
