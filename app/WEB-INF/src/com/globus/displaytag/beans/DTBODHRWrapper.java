package com.globus.displaytag.beans;

import java.util.HashMap;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;
import  com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonConstants;
import com.globus.common.beans.GmLogger;
 

public class DTBODHRWrapper extends TableDecorator
{
   Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS); 
   String strWiki = GmCommonClass.getString("GWIKI");
   String strWikiTitle = "";
    /**
     * Returns the string for RequestId field icon.
     * @return String
     */
    public String getPNUM()
    {
    	StringBuffer  strValue = new StringBuffer();
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();               
    	String strPNUM   = GmCommonClass.parseNull((String)db.get("PNUM"));
        strValue.setLength(0);     
        strValue.append("<a href=\"javascript:fnopenGmPartInvReport('"+strPNUM+"')\"> <img src='/images/icon_sales.gif' /> </a>" );   
       	strValue.append(strPNUM);
    	return strValue.toString();
                
    }
    public String getDHRQTY()
    {
    	StringBuffer  strValue = new StringBuffer();
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();               
        String strDCT   = GmCommonClass.parseNull(String.valueOf(db.get("DHRQTY")));
        String strPNUM   = GmCommonClass.parseNull((String)db.get("PNUM"));
        strValue.setLength(0);     
        strValue.append("<a href=\"javascript:fnopener('"+strPNUM+"')\">"+strDCT+"</a>");
    	return strValue.toString();
                
    }
    public String getTO_SHELF()
    {
    	StringBuffer  strValue = new StringBuffer();
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();               
        String strShelf   = GmCommonClass.parseNull(String.valueOf(db.get("TO_SHELF")));
        strWikiTitle = GmCommonClass.getWikiTitle("TO_SHELF");
        strValue.setLength(0);     
        strValue.append("<a href=\"javascript:fnHelp('" + strWiki + "','" + strWikiTitle + "');\">"+strShelf+"</a>");
    	return strValue.toString();
    }
    public String getTO_BULK()
    {
    	StringBuffer  strValue = new StringBuffer();
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();               
        String strBulk   = GmCommonClass.parseNull(String.valueOf(db.get("TO_BULK")));
        strWikiTitle = GmCommonClass.getWikiTitle("TO_BULK");
        strValue.setLength(0);     
        strValue.append("<a href=\"javascript:fnHelp('" + strWiki + "','" + strWikiTitle + "');\">"+strBulk+"</a>");
    	return strValue.toString();
    }      /* 
    public String getTO_RM()
    {
    	StringBuffer  strValue = new StringBuffer();
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();               
        String strRM   = GmCommonClass.parseNull(String.valueOf(db.get("TO_RM")));
        strWikiTitle = GmCommonClass.getWikiTitle("TO_RM");
        strValue.setLength(0);     
        strValue.append("<a href=\"javascript:fnHelp('" + strWiki + "','" + strWikiTitle + "');\">"+strRM+"</a>");
    	return strValue.toString();
    }    */
}