 
package com.globus.displaytag.beans;

import java.util.HashMap;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;


 
public class DTGroupsReportWrapper extends TableDecorator
{

    private DynaBean db;
    private String strValue =""; 
    
    private String strGroupNM = ""; 
    private String strGroupid = ""; 
    
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
      
    public String getGROUPNM()
    {
    	 
    	db =    (DynaBean) this.getCurrentRowObject();
    	strGroupid = String.valueOf(db.get("GROUPID"));
    	strGroupNM = String.valueOf(db.get("GROUPNM")) ; 
	 
		strValue =  "&nbsp;<a title='Click to open part history'><img id='imgEdit' style='cursor:hand' src='/images/product_icon.gif'" +  
        "onClick=\"javascript:fnGroupDetail( '"+ strGroupid +"','" + strGroupNM + "') \" /></a>&nbsp;" + strGroupNM; 
        	
        
        return strValue;
    }
    
     
}
