package com.globus.displaytag.beans;

/**
 * @author jkumar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */


import java.text.DecimalFormat;

import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.TableDecorator;
import org.apache.commons.beanutils.DynaBean;

public class DTAccARWrapper  extends TableDecorator{


     /**
     * DecimalFormat used to format money in getMoney().
     */
    private DecimalFormat moneyFormat;
    private DynaBean db ;
    private String formatVal;
    double dblVal ;
    
    /**
     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
     */
    public DTAccARWrapper()
    {
        super();
       this.moneyFormat = new DecimalFormat("#,###,###"); //$NON-NLS-1$
        //this.moneyFormat = new DecimalFormat("$ {0,number,.00}"); 
    }

    /**
     * Returns the Payment Amount  as a String in $ #,###,###.00 format.
     * @return String
     */
    
    public String getCURRENTVALUE()
    {
     db =  	(DynaBean) this.getCurrentRowObject();
        if (db.get("CURRENTVALUE") == null )
        {
            return "";
        }
        else
        {
        double dblCurVal = Double.parseDouble((db.get("CURRENTVALUE")).toString());
        String formatCurVal = this.moneyFormat.format(dblCurVal);
        return  "<a href= javascript:fnViewDetails('" + db.get("ID") + "','" + "129" +"')>" +formatCurVal+ "</a>" ;
		}
    }
    
     public String getONE_THIRTY()
    {
          db =  	(DynaBean) this.getCurrentRowObject();
        if (db.get("ONE_THIRTY") == null )
        {
            return "";
        }
        else
        {
        dblVal = Double.parseDouble((db.get("ONE_THIRTY")).toString());
        formatVal = this.moneyFormat.format(dblVal);
        return  "<a href= javascript:fnViewDetails('" + db.get("ID") + "','" + "3160" +"')>" +formatVal+ "</a>" ;
		}
    }
  
    public String getTHIRTYONE_SIXTY()
    {
    	 db =  	(DynaBean) this.getCurrentRowObject();
         if (db.get("THIRTYONE_SIXTY") == null )
         {
             return "";
         }
         else
         {
         dblVal = Double.parseDouble((db.get("THIRTYONE_SIXTY")).toString());
         formatVal = this.moneyFormat.format(dblVal);
         return  "<a href= javascript:fnViewDetails('" + db.get("ID") + "','" + "6190" +"')>" +formatVal+ "</a>" ;
 		}
    }
  
    public String getSIXTYONE_NINETY()
    {
    	 db =  	(DynaBean) this.getCurrentRowObject();
         if (db.get("SIXTYONE_NINETY") == null )
         {
             return "";
         }
         else
         {
         dblVal = Double.parseDouble((db.get("SIXTYONE_NINETY")).toString());
         formatVal = this.moneyFormat.format(dblVal);
         return  "<a href= javascript:fnViewDetails('" + db.get("ID") + "','" + "91120" +"')>" +formatVal+ "</a>" ;
 		}
    }
  
    public String getGREATER_NINETY()
    {
    	db =  	(DynaBean) this.getCurrentRowObject();
         if (db.get("GREATER_NINETY") == null )
         {
             return "";
         }
         else
         {
         dblVal = Double.parseDouble((db.get("GREATER_NINETY")).toString());
         formatVal = this.moneyFormat.format(dblVal);
         return  "<a href= javascript:fnViewDetails('" + db.get("ID") + "','" + "121" +"')>" +formatVal+ "</a>" ;
 		}
    } 
    
    public String getROW_TOTAL()
    {
    	db =  	(DynaBean) this.getCurrentRowObject();
         if (db.get("ROW_TOTAL") == null )
         {
             return "";
         }
         else
         {
         dblVal = Double.parseDouble((db.get("ROW_TOTAL")).toString());
         formatVal = this.moneyFormat.format(dblVal);
         return  formatVal;
 		}
    } 
  
}
