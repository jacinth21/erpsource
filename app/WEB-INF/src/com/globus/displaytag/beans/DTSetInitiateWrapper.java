package com.globus.displaytag.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTSetInitiateWrapper extends TableDecorator{
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

	private HashMap hmValue = new HashMap();  
	private double dblSetQty;
	private double dblBulkQty;
	private String strRowClass = "RightText";
	
	public DTSetInitiateWrapper()
	    {
	        super();
	    }

	public String addRowClass()
	    {
			hmValue =  	(HashMap) this.getCurrentRowObject();
			
			dblSetQty = Double.parseDouble(GmCommonClass.parseZero((hmValue.get("QTY")).toString()));
			dblBulkQty = Double.parseDouble(GmCommonClass.parseZero((hmValue.get("QTYBULK")).toString()));
			strRowClass = dblSetQty > dblBulkQty ? "RightTextRed" : "RightText";

			return strRowClass;
	    }

}
