package com.globus.displaytag.beans;

/**
 * @author richardk
 * 
 *         Ths bean is used for VirtualSetWrapped screen format and for hyperlinks
 * 
 */

import java.math.BigDecimal;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TotalTableDecorator;

import com.globus.common.beans.GmCommonClass;

public class DTSalesVirtualDetailWrapper extends TotalTableDecorator {


  /**
   * DecimalFormat used to format money in getMoney().
   */
  private DynaBean db;
  private String strType;
  private String strID;
  private String strTrackID;
  private String strCarrier;

  /**
   * Creates a new Wrapper decorator who's job is to reformat some of the data located in our
   * TestObject's.
   */
  public DTSalesVirtualDetailWrapper() {
    super();
    //this.moneyFormat = new DecimalFormat("$ #,###,###.00"); //$NON-NLS-1$
    // this.moneyFormat = new DecimalFormat("$ {0,number,.00}");
  }


  /**
   * Returns the link to view set details by consignment details
   * 
   * @return String
   */
  public String getDIMAGE() {
    db = (DynaBean) this.getCurrentRowObject();
    strType = (String) db.get("CTYPE");
    strID = (String) db.get("ID");
    strTrackID = (String) db.get("TRACK");

    if (strType.equals("R")) {
      return "<img id='imgEditA' style='cursor:hand' src='/images/return_black.gif'"
          + " width='12' height='13' />";
      // "onClick=\"javascript:fnViewUnassignedID( '" + strID + "', '" + strDID + "') \" />";
    } else {
      return "";
    }
  }

  /**
   * Returns the link to view ID
   * 
   * @return String
   */
  public String getID() {
    /** Below code added because if we sort using this column its getting failed */
    db = (DynaBean) this.getCurrentRowObject();
    strType = (String) db.get("CTYPE");
    strID = (String) db.get("ID");
    /***/
    if (strType.equals("R")) {
      return "&nbsp;<a class='RightTextRed' href=javascript:fnPrintRA('" + strID + "')>" + strID
          + "</a>";
    } else {
      return "&nbsp;<a href=javascript:fnPrintConsignVer('" + strID + "')>" + strID + "</a>";
    }
  }

  /**
   * Returns the link to view TRACK ID
   * 
   * @return String
   */
  public String getTRACK() {
    db = (DynaBean) this.getCurrentRowObject();
    strTrackID = GmCommonClass.parseNull((String) db.get("TRACK"));
    strCarrier = GmCommonClass.parseNull((BigDecimal) db.get("CARRIER") + "");
    /** Above code added because if we sort using this column its getting failed */
    if (!strTrackID.equals("-")) {
      if (strCarrier.equals("5001")) {// Fedex
        return "<a  href=\"javascript:fnOpenTrack('" + strTrackID + "')\"> " + strTrackID + "</a>";
      } else {
        return strTrackID;
      }
    } else {
      return "-";
    }

  }

  public String addRowClass() {
    db = (DynaBean) this.getCurrentRowObject();
    strType = (String) db.get("CTYPE");
    String strRowClass = strType.equals("R") ? "RightTextRed" : "RightText";
    return strRowClass;
  }

}
