package com.globus.displaytag.beans;

import org.displaytag.decorator.TableDecorator;

public class GmCommonDecorator extends TableDecorator {
	
	 /**
     * Returns the Payment Amount  as a String in $ #,###,###.00 format.
     * @return String
     */
    public String getIDLink(String strFunctionName, String strKey)
    {        
        return "<a href=javascript:"+strFunctionName+"('" + strKey + "')>  <img width='39' height='19' title='Click here to edit details' style='cursor:hand' src='/images/edit.jpg'/>  </a>" ;
    }

}
