package com.globus.displaytag.beans;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmLogger;

public class DTRequestGrowthWrapper extends TableDecorator
{
    private String strMonth; 
   
    private int intRequest  ;
    private int intGrowth  ;
    private int intMonth;
    private DynaBean db ;
    private String strStyleClass;
    private String strFirstDayOfMonth;
    private String strLastDayOfMonth;
    private String strSetId;
    private String strRefType;
    public StringBuffer  strValue = new StringBuffer();
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

    
    /**
     * Returns the string for text field.
     * @return String
     */
    public String getREQUIRED_MONTH()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strMonth = (String)db.get("REQUIRED_MONTH");
        intGrowth = Integer.parseInt(db.get("GROWTH_QTY").toString());
        intRequest   = Integer.parseInt(db.get("REQUEST_QTY").toString());
        strValue.setLength(0);
       
         if(intGrowth < intRequest)    
          	 strValue.append("<font color=red>"+ strMonth +"</font>");
         else         	 
        	 strValue.append(strMonth);
         return strValue.toString();
    }
    
    public String getGROWTH_QTY()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strMonth = (String)db.get("REQUIRED_MONTH");
        intGrowth = Integer.parseInt(db.get("GROWTH_QTY").toString());
        intRequest   = Integer.parseInt(db.get("REQUEST_QTY").toString());
        strValue.setLength(0);      
   //     return strValue.toString();
        if(intGrowth < intRequest)    
         	 strValue.append("<font color=red>"+ intGrowth +"</font>");
        else         	 
       	 strValue.append(intGrowth);
        return strValue.toString();
    }
    
    public String getREQUEST_QTY()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strMonth = (String)db.get("REQUIRED_MONTH");
        strRefType = db.get("REF_TYPE").toString();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");        
        try{
        	//intMonth = Integer.parseInt(new SimpleDateFormat("MM").format(new SimpleDateFormat("MMM-yy").parse(strMonth)));
        	//intMonth--; // because Calendar class considers Jan as 0 and Dec as 11
        	strFirstDayOfMonth = df.format(new SimpleDateFormat("MMM-yy").parse(strMonth));
        	calendar.setTime(new SimpleDateFormat("MMM-yy").parse(strMonth));
        	calendar.add(Calendar.MONTH, 1);
        	calendar.add(Calendar.DATE, -1);
        	strLastDayOfMonth = df.format(calendar.getTime());
        	
        }
        catch(Exception exp){
        	exp.printStackTrace();
        }
        
        intGrowth = Integer.parseInt(db.get("GROWTH_QTY").toString());
        intRequest   = Integer.parseInt(db.get("REQUEST_QTY").toString());
        strStyleClass = "RightText";
        strSetId = (String)db.get("REF_ID");
        strValue.setLength(0);
   //     return strValue.toString();
        if(intGrowth < intRequest)    
        	strStyleClass =  "RightTextRed";
        
       // strFirstDayOfMonth = GmCalenderOperations.getFirstDayOfMonth(intMonth);
        //strLastDayOfMonth = GmCalenderOperations.getLastDayOfMonth(intMonth);
//        log.debug(" intMonth " + intMonth+ " strMonth " +strMonth + " firstdayof month " + strFirstDayOfMonth + " last day " + strLastDayOfMonth);
        
        strValue.append("  <a style='cursor:hand' class="); 
        strValue.append(strStyleClass);
        strValue.append(" onClick=javascript:fnRequestGrowthDrillDown('");
    	strValue.append(strFirstDayOfMonth);
    	strValue.append("','");
    	strValue.append(strLastDayOfMonth);
    	strValue.append("','");
    	strValue.append(strSetId);
    	strValue.append("','");
    	strValue.append(strRefType);
    	strValue.append("'); />" );
    	strValue.append(String.valueOf(intRequest));
    	strValue.append("</a>");

        //strValue.append("<table><tr><td class=RightTextRed align=center>"+ intRequest +"</td></tr></table>");
        return strValue.toString();
    }
     
}


