package com.globus.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TotalTableDecorator;

public class DTPartDrillDownWrapper extends TotalTableDecorator
{
    private DynaBean db ;
    private String strType = "";
    private String strID = "";
    
    
    public String getCSGID()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strType     = (String)db.get("STYPE");
        strID      = (String)db.get("CSGID");
 
        if (strType.equals("40020") )
        {
             return "&nbsp;<a class='red' href=javascript:fnPrintPack('" + 
             strID + "')> "  + strID + "</a>" ;
        }
        else if (strType.equals("40022") )
        {
             return "&nbsp;<a href=javascript:fnPrintConsignVer('" + strID + "')> "  + strID + "</a>" ;
        } 
        else {
        	return "&nbsp;"+strID;
        }
    }
}
