/**
 * Licensed under the Artistic License; you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * 
 * http://displaytag.sourceforge.net/license.html
 * 
 * THIS PACKAGE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE.
 */
package com.globus.displaytag.beans;

import java.text.DecimalFormat;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.lang.time.FastDateFormat;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * This class is a decorator of the TestObjects that we keep in our List. This class provides a
 * number of methods for formatting data, creating dynamic links, and exercising some aspects of the
 * display:table API functionality.
 * 
 * @author epesh
 * @author Fabrizio Giustina
 * @version $Revision$ ($Author$)
 */
public class DTInvoiceOrderWrapper extends TableDecorator {

  /**
   * FastDateFormat used to format dates in getDate().
   */
  private final FastDateFormat dateFormat;
  private final String strForeColor = "<span style=\"color:red;\">";
  double dbTotal = 0.0;
  Double dblObj;
  String strSessCurrSymbol = "";

  /**
   * DecimalFormat used to format money in getMoney().
   */
  private final DecimalFormat moneyFormat;

  private DynaBean db;
  private String strValue;
  private String strImg;
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  /**
   * Creates a new Wrapper decorator who's job is to reformat some of the data located in our
   * TestObject's.
   */
  public DTInvoiceOrderWrapper() {
    super();

    // Formats for displaying dates and money.
    this.dateFormat = FastDateFormat.getInstance("MM/dd/yy"); //$NON-NLS-1$
    this.moneyFormat = new DecimalFormat("#,###,##0.00"); //$NON-NLS-1$
  }

  /**
   * Returns the date as a String in MM/dd/yy format.
   * 
   * @return formatted date
   */
  /*
   * public String getINV_DATE() { return this.dateFormat.format(((DynaBean)
   * this.getCurrentRowObject()).get("INV_DATE")); }
   */

  /**
   * Returns the Payment Amount as a String in $ #,###,###.00 format.
   * 
   * @return String
   */

  public String getINV_ID() {
    db = (DynaBean) this.getCurrentRowObject();
    // log.debug("INV_ID wrapper called");
    if (db.get("INV_ID") == null) {
      return "";
    } else {
      return "<a href= javascript:fnPrintInvoice('" + db.get("INV_ID") + "')> " + db.get("INV_ID")
          + "</a>";
    }

  }
  
  
  public String getTAX_AMT() {
  db = (DynaBean) this.getCurrentRowObject();
 //  log.debug("TAX_AMT wrapper called");  
  dbTotal = Double.parseDouble((db.get("TAX_AMT")).toString());
  dblObj = Double.valueOf(String.valueOf(dbTotal));
   String strInvid = GmCommonClass.parseNull((String) db.get("INV_ID"));
  
  if (strInvid.equals("")) {	
    return "";
  } else {
	  if(dblObj < 0)
	  {
		  return strForeColor + "(" + this.moneyFormat.format(dblObj * -1) + ")</span>";   
	  }
	  else
	  {
	  return this.moneyFormat.format(dblObj);
	  }
  }

}
  
  
  
  
  public String getINV_AMT() {
  db = (DynaBean) this.getCurrentRowObject();
 //  log.debug("TAX_AMT wrapper called");
  
  dbTotal = Double.parseDouble((db.get("INV_AMT")).toString());
  dblObj = Double.valueOf(String.valueOf(dbTotal));
   String strInvid = GmCommonClass.parseNull((String) db.get("INV_ID"));
  
  if (strInvid.equals("")) {	
    return "";
  } else {
	  if(dblObj < 0)
	  {
		  return strForeColor + "(" + this.moneyFormat.format(dblObj * -1) + ")</span>";   
	  }
	  else
	  {
	  return this.moneyFormat.format(dblObj);
	  }
  }

}
  
  
  public String getPARENT_ORDER_ID() {
	    db = (DynaBean) this.getCurrentRowObject();
	    strValue = GmCommonClass.parseNull((String) db.get("PARENT_ORDER_ID"));
	    if (strValue.equals("")) {
	      strImg = "";
	    } else {
	      strImg =
	          "<a href=javascript:fnPrintPack('" + strValue + "')> " + strValue + "</a>&nbsp;";
	    }
	    return strImg;
  }
  
  public String getORDER_ID() {
    db = (DynaBean) this.getCurrentRowObject();
    strValue = (String) db.get("ORDER_ID");
    // to show the "Document" icon
    String strDoFlag = GmCommonClass.parseNull((String) db.get("DO_FLAG"));
    if (strDoFlag.equals("")) {
      strImg = "";
    } else {
      strImg =
          "<a href=javascript:fnOpenDOFile('" + strValue
              + "')><img  src = '/images/d.gif' ></a>&nbsp;";
    }
    // Check if the Orded is a Return or Duplicate Order -- redirect based
    // on the type
    if ((strValue.charAt(strValue.length() - 1) == 'R')
        || (strValue.charAt(strValue.length() - 1) == 'D')) {
      return strImg + "<a href=javascript:fnPrintCreditMemo('" + strValue + "')> " + strValue
          + "</a>";
    } else if (strValue.charAt(strValue.length() - 1) == 'A') {
      return strImg + "<a href=javascript:fnPrintCashAdjustMemo('" + strValue + "')> " + strValue
          + "</a>";
    } else
      return strImg + "<a href=javascript:fnPrintPack('" + strValue + "')> " + strValue + "</a>";
  }

  public String getPNUM() {
    db = (DynaBean) this.getCurrentRowObject();
    if (db.get("PNUM") == null) {
      return "";
    } else {
      return "<a href= javascript:fnOpenPartDet('" + db.get("PNUM") + "')> " + db.get("PNUM")
          + "</a>";
    }

  }

  
  public String getORDER_VALUE() {
    db = (DynaBean) this.getCurrentRowObject();
    dbTotal = Double.parseDouble((db.get("ORDER_VALUE")).toString());
    dblObj = Double.valueOf(String.valueOf(dbTotal));

    if (dbTotal < 0) {
      return strForeColor + "(" + this.moneyFormat.format(dblObj * -1) + ")</span>";
    } else {
      return this.moneyFormat.format(dblObj);
    }
  }

  public String getSHIP_COST() {

    db = (DynaBean) this.getCurrentRowObject();
    dbTotal = Double.parseDouble((db.get("SHIP_COST")).toString());
    dblObj = Double.valueOf(String.valueOf(dbTotal));
    if (dbTotal < 0) {
      return strForeColor + "(" + this.moneyFormat.format(dblObj * -1) + ")</span>";
    } else {
      return this.moneyFormat.format(dblObj);
    }
  }

  public String getTOT_COST() {

    db = (DynaBean) this.getCurrentRowObject();
    dbTotal = Double.parseDouble((db.get("TOT_COST")).toString());
    dblObj = Double.valueOf(String.valueOf(dbTotal));
    if (dbTotal < 0) {
      return strForeColor + "(" + this.moneyFormat.format(dblObj * -1) + ")</span>";
    } else {
      return this.moneyFormat.format(dblObj);
    }
  }

  public String getPART_COGS_VAL() {

    db = (DynaBean) this.getCurrentRowObject();
    dbTotal = Double.parseDouble((db.get("PART_COGS_VAL")).toString());
    dblObj = Double.valueOf(String.valueOf(dbTotal));
    if (dbTotal < 0) {
      return strForeColor + "(" + this.moneyFormat.format(dblObj * -1) + ")</span>";
    } else {
      return this.moneyFormat.format(dblObj);
    }
  }

}
