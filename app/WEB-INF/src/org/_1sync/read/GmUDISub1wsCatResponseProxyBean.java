package org._1sync.read;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;

import org._1sync.read.CatalogueResponseType.DocumentAcknowledgement;
import org._1sync.read.CatalogueResponseType.DocumentException;
import org._1sync.read.CatalogueResponseType.Header;
import org._1sync.read.CatalogueResponseType.MessageException;
import org._1sync.read.ItemAuthorizationResponseType.DocumentNotification;
import org.apache.log4j.Logger;

import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmUDIUtil;

/**
 * The Class GmUDISub1wsCatResponseProxyBean.
 * 
 * @author mpatel
 */

public class GmUDISub1wsCatResponseProxyBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());



  /**
   * Populate1ws response vo.
   * 
   * @param hmCollectResParam the hm collect res param
   * @throws JAXBException the JAXB exception
   */
  public void populate1wsResponseVO(HashMap hmCollectResParam) throws JAXBException {
    String strPathTempDir = (String) hmCollectResParam.get("strPathTempDir");

    Envelope env = readPopulatedEnvVO(strPathTempDir);
    readPopulatedEnvHeader(env, hmCollectResParam);

    Date date = GmUDIUtil.toDate(env.getHeader().getCreationDateTime());
    // System.out.println("java.util.Date from XMLGregorianCalendar in Java : " + date);
    // readPopulatedEnvRes (env);
    envResponseRouter(env, hmCollectResParam);


  }



  /**
   * Read populated env vo.
   * 
   * @param strPathTempDir the str path temp dir
   * @return the envelope
   * @throws JAXBException the JAXB exception
   */
  public Envelope readPopulatedEnvVO(String strPathTempDir) throws JAXBException {
    File file = new File(strPathTempDir);
    JAXBContext jaxbContext = JAXBContext.newInstance(Envelope.class);

    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
    Envelope env = (Envelope) jaxbUnmarshaller.unmarshal(file);
    return env;
  }

  /**
   * Read populated env header.
   * 
   * @param env the env
   * @param hmCollectResParam the hm collect res param
   */
  public void readPopulatedEnvHeader(Envelope env, HashMap hmCollectResParam) {
    HeaderType htype = env.getHeader();
    hmCollectResParam.put("strMsgId", htype.getMessageId());


  }

  /**
   * Env response router.
   * 
   * @param env the env
   * @param hmCollectResParam the hm collect res param
   * @throws JAXBException the JAXB exception
   */
  public void envResponseRouter(Envelope env, HashMap hmCollectResParam) throws JAXBException {

    if (env.getCatalogueResponse() instanceof CatalogueResponseType) {
      readPopulatedEnvCatRes(env, hmCollectResParam);
    } else if (env.getGdsnItemRegistryResponse() instanceof GdsnItemRegistryResponseType) {
      readPopulatedEnvItemRegRes(env, hmCollectResParam);
      hmCollectResParam.put("XMLTYPE", "104863");// 104863 GDSN Item Registry Xml Response
    } else if (env.getItemSyncResponse() instanceof ItemSyncResponseType) {
      readPopulatedEnvItemSyncRes(env, hmCollectResParam);
      hmCollectResParam.put("XMLTYPE", "104864");// 104864 Item Sync Xml Response
    } else if (env.getItemAuthorizationResponse() instanceof ItemAuthorizationResponseType) {
      readPopulatedEnvItemAuthRes(env, hmCollectResParam);//
      hmCollectResParam.put("XMLTYPE", "104865");// 104865 CIC XML Response

    }
  }



  /**
   * Read populated env cat res.
   * 
   * @param env the env
   * @param hmCollectResParam the hm collect res param
   */
  public void readPopulatedEnvCatRes(Envelope env, HashMap hmCollectResParam) {
    String str = "";
    StringBuffer sbDBInputStatus = new StringBuffer();
    StringBuffer sbDBInputMsgExc = new StringBuffer();
    StringBuffer sbDBInputDesc = new StringBuffer();
    StringBuffer sbDBInputSrcSys = new StringBuffer();

    readpopulatedCatResHeader(env, hmCollectResParam);


    // Route to Item,publication,Message Exception File
    envCatResRouter(env, hmCollectResParam, sbDBInputStatus, sbDBInputMsgExc, sbDBInputDesc,
        sbDBInputSrcSys);

    hmCollectResParam.put("strDBInputMsgExc", sbDBInputMsgExc.toString());
    hmCollectResParam.put("sbDBInputStatus", sbDBInputStatus.toString());
    hmCollectResParam.put("sbDBInputDesc", sbDBInputDesc.toString());
    hmCollectResParam.put("sbDBInputSrcSys", sbDBInputSrcSys.toString());
    System.out.println(" readPopulatedEnvCatRes strDBInputMsgExc :" + sbDBInputMsgExc.toString());
    System.out.println(" readPopulatedEnvCatRes sbDBInputStatus:" + sbDBInputStatus.toString());
    System.out.println("readPopulatedEnvCatRes sbDBInputDesc:" + sbDBInputDesc.toString());
  }

  /**
   * Env cat res router.
   * 
   * @param env the env
   * @param hmCollectResParam the hm collect res param
   * @param sbDBInputStatus the sb db input status
   * @param sbDBInputMsgExc the sb db input msg exc
   * @param sbDBInputDesc the sb db input desc
   * @param sbDBInputSrcSys the sb db input src sys
   */
  public void envCatResRouter(Envelope env, HashMap hmCollectResParam,
      StringBuffer sbDBInputStatus, StringBuffer sbDBInputMsgExc, StringBuffer sbDBInputDesc,
      StringBuffer sbDBInputSrcSys) {

    CatalogueResponseType catTyp = env.getCatalogueResponse();
    MessageException docMsgExc = catTyp.getMessageException();

    List<Object> docAckExcList = catTyp.getDocumentExceptionOrDocumentAcknowledgement();
    System.out.println("catTyp.getDocumentExceptionOrDocumentAcknowledgement().size(): "
        + catTyp.getDocumentExceptionOrDocumentAcknowledgement().size());
    System.out.println("docMsgExc: " + docMsgExc);
    System.out.println("docAckExcList: " + docAckExcList);

    if (docMsgExc != null) {

      readpopulatedCatResMsgExc(docMsgExc, sbDBInputMsgExc);
      hmCollectResParam.put("XMLTYPE", "104862");// 104862 Message Exception Xml Response

    }

    if (docAckExcList.size() > 0) {
      String strXMLTYPE = "";
      for (Object doc : docAckExcList) {
        DocumentAcknowledgement docAck = null;
        DocumentException docExc = null;

        if (doc instanceof DocumentAcknowledgement) {
          docAck = (DocumentAcknowledgement) doc;
          System.out
              .println("****************************DocumentAcknowledgement***********************");

          if (docAck.getItem() instanceof ItemAcknowledgementType) {
            readpopulatedDocAckItem(docAck, sbDBInputStatus, sbDBInputSrcSys);
            strXMLTYPE = "104860";// 104860 Item Xml Response
          } else if (docAck.getPublication() instanceof PublicationAcknowledgementType) {
            readpopulatedDocAckPublication(docAck, sbDBInputStatus, sbDBInputSrcSys);
            strXMLTYPE = "104861";// 104861 Publication Xml Response
          }
        } else if (doc instanceof DocumentException) {
          docExc = (DocumentException) doc;
          System.out
              .println("****************************DocumentException***********************");

          if (docExc.getItem() instanceof ItemExceptionType) {
            readpopulatedDocExcItem(docExc, sbDBInputStatus, sbDBInputDesc, sbDBInputSrcSys);
            strXMLTYPE = "104860";// 104860 Item Xml Response
          } else if (docExc.getPublication() instanceof PublicationExceptionType) {
            readpopulatedDocExcPublication(docExc, sbDBInputStatus, sbDBInputDesc, sbDBInputSrcSys);
            strXMLTYPE = "104861";// 104861 Publication Xml Response
          }
        }
      }

      hmCollectResParam.put("XMLTYPE", strXMLTYPE);
    }
  }


  /**
   * Readpopulated cat res header.
   * 
   * @param env the env
   * @param hmCollectResParam the hm collect res param
   */
  public void readpopulatedCatResHeader(Envelope env, HashMap hmCollectResParam) {
    CatalogueResponseType catTyp = env.getCatalogueResponse();
    Header head = catTyp.getHeader();
    System.out.println("head.getUserGLN() : " + head.getUserGLN());
    System.out.println("head.getOriginatingMessageId() : " + head.getOriginatingMessageId());
    hmCollectResParam.put("strOrgMsgId", head.getOriginatingMessageId());

  }

  /**
   * Readpopulated cat res msg exc.
   * 
   * @param docMsgExc the doc msg exc
   * @param sbDBInputMsgExc the sb db input msg exc
   */
  public void readpopulatedCatResMsgExc(MessageException docMsgExc, StringBuffer sbDBInputMsgExc) {
    // select all DI numbers using this message Id and mark the status to Rework Item from 1ws or
    // Rework Publication from 1ws
    List<DescriptionType> listDocMsgExc = docMsgExc.getDescription();
    for (DescriptionType doc : listDocMsgExc) {
      sbDBInputMsgExc =
          sbDBInputMsgExc.append(doc.getCode()).append("^").append(doc.getValue()).append("^")
              .append("").append("|");
    }
  }



  /**
   * Readpopulated doc ack item.
   * 
   * @param docAck the doc ack
   * @param sbDBInputStatus the sb db input status
   * @param sbDBInputSrcSys the sb db input src sys
   */
  public void readpopulatedDocAckItem(DocumentAcknowledgement docAck, StringBuffer sbDBInputStatus,
      StringBuffer sbDBInputSrcSys) {
    ItemAcknowledgementType itemAck = docAck.getItem();


    // 104826 Success-Uploaded Item to 1ws ,
    // gtin^DocId^GDSNItemSuccessflag^GDSNPublicationSuccessFlag^CICFlag^GUDIDPublishFlag^InternalStatus^ExternalStatus|
    sbDBInputStatus =
        sbDBInputStatus.append(itemAck.getGtin()).append("^")
            .append(docAck.getOriginatingDocumentId()).append("^").append("Y").append("^")
            .append("").append("^").append("").append("^").append("").append("^").append("")
            .append("^").append("104826").append("|");

    // item+ pub+registry+ItemSync+itemAuthorization
    // operation^gtin^informationProviderGLN^targetMarket^dataRecipientGLN^type^AdditionalInformation^registrationDate^itemSyncErrorDate^authorizationDate^authorizationState^confirmationStatusDetail|
    sbDBInputSrcSys.append("operation").append("^").append(itemAck.getOperation()).append("^")
        .append(itemAck.getGtin()).append("|").append("gtin").append("^").append(itemAck.getGtin())
        .append("^").append(itemAck.getGtin()).append("|").append("informationProviderGLN")
        .append("^").append(itemAck.getInformationProviderGLN()).append("^")
        .append(itemAck.getGtin()).append("|").append("targetMarket").append("^")
        .append(itemAck.getTargetMarket()).append("^").append(itemAck.getGtin()).append("|")
        .append("").append("^").append("").append("^").append("").append("|").append("")
        .append("^").append("").append("^").append("").append("|").append("").append("^")
        .append("").append("^").append("").append("|").append("").append("^").append("")
        .append("^").append("").append("|").append("").append("^").append("").append("^")
        .append("").append("|").append("").append("^").append("").append("^").append("")
        .append("|").append("").append("^").append("").append("^").append("").append("|")
        .append("").append("^").append("").append("^").append("").append("|");

  }

  /**
   * Readpopulated doc exc item.
   * 
   * @param docExc the doc exc
   * @param sbDBInputStatus the sb db input status
   * @param sbDBInputDesc the sb db input desc
   * @param sbDBInputSrcSys the sb db input src sys
   */
  public void readpopulatedDocExcItem(DocumentException docExc, StringBuffer sbDBInputStatus,
      StringBuffer sbDBInputDesc, StringBuffer sbDBInputSrcSys) {

    ItemExceptionType itemExc = docExc.getItem();

    List<DescriptionType> listDocExc = itemExc.getDescription();

    // codeid^valueText^gtin|
    for (DescriptionType doc : listDocExc) {
      sbDBInputDesc =
          sbDBInputDesc.append(doc.getCode()).append("^").append(doc.getValue()).append("^")
              .append(itemExc.getGtin()).append("|");
    }

    // 104827 Rework Item from 1ws ,
    // gtin^DocId^GDSNItemSuccessflag^GDSNPublicationSuccessFlag^CICFlag^GUDIDPublishFlag^InternalStatus^ExternalStatus|
    sbDBInputStatus =
        sbDBInputStatus.append(itemExc.getGtin()).append("^")
            .append(docExc.getOriginatingDocumentId()).append("^").append("").append("^")
            .append("").append("^").append("").append("^").append("").append("^").append("")
            .append("^").append("104827").append("|");

    // item+ pub+registry+ItemSync+itemAuthorization
    // operation^gtin^informationProviderGLN^targetMarket^dataRecipientGLN^type^AdditionalInformation^registrationDate^itemSyncErrorDate^authorizationDate^authorizationState^confirmationStatusDetail|
    sbDBInputSrcSys.append("operation").append("^").append(itemExc.getOperation()).append("^")
        .append(itemExc.getGtin()).append("|").append("gtin").append("^").append(itemExc.getGtin())
        .append("^").append(itemExc.getGtin()).append("|").append("informationProviderGLN")
        .append("^").append(itemExc.getInformationProviderGLN()).append("^")
        .append(itemExc.getGtin()).append("|").append("targetMarket").append("^")
        .append(itemExc.getTargetMarket()).append("^").append(itemExc.getGtin()).append("|")
        .append("").append("^").append("").append("^").append("").append("|").append("")
        .append("^").append("").append("^").append("").append("|").append("").append("^")
        .append("").append("^").append("").append("|").append("").append("^").append("")
        .append("^").append("").append("|").append("").append("^").append("").append("^")
        .append("").append("|").append("").append("^").append("").append("^").append("")
        .append("|").append("").append("^").append("").append("^").append("").append("|")
        .append("").append("^").append("").append("^").append("").append("|");


  }



  /**
   * Readpopulated doc ack publication.
   * 
   * @param docAck the doc ack
   * @param sbDBInputStatus the sb db input status
   * @param sbDBInputSrcSys the sb db input src sys
   */
  public void readpopulatedDocAckPublication(DocumentAcknowledgement docAck,
      StringBuffer sbDBInputStatus, StringBuffer sbDBInputSrcSys) {

    PublicationAcknowledgementType pubAck = docAck.getPublication();


    // 104830 Success-Uploaded Publication to 1ws
    // gtin^DocId^GDSNItemSuccessflag^GDSNPublicationSuccessFlag^CICFlag^GUDIDPublishFlag^InternalStatus^ExternalStatus|
    sbDBInputStatus =
        sbDBInputStatus.append(pubAck.getGtin()).append("^")
            .append(docAck.getOriginatingDocumentId()).append("^").append("").append("^")
            .append("Y").append("^").append("").append("^").append("").append("^").append("")
            .append("^").append("104830").append("|");

    // item+ pub+registry+ItemSync+itemAuthorization
    // operation^gtin^informationProviderGLN^targetMarket^dataRecipientGLN^type^AdditionalInformation^registrationDate^itemSyncErrorDate^authorizationDate^authorizationState^confirmationStatusDetail|
    sbDBInputSrcSys.append("operation").append("^").append(pubAck.getOperation()).append("^")
        .append(pubAck.getGtin()).append("|").append("gtin").append("^").append(pubAck.getGtin())
        .append("^").append(pubAck.getGtin()).append("|").append("informationProviderGLN")
        .append("^").append(pubAck.getInformationProviderGLN()).append("^")
        .append(pubAck.getGtin()).append("|").append("targetMarket").append("^")
        .append(pubAck.getTargetMarket()).append("^").append(pubAck.getGtin()).append("|")
        .append("dataRecipientGLN").append("^").append("").append("^").append("").append("|")
        .append("type").append("^").append(pubAck.getType()).append("^").append("").append("|")
        .append("").append("^").append("").append("^").append("").append("|").append("")
        .append("^").append("").append("^").append("").append("|").append("").append("^")
        .append("").append("^").append("").append("|").append("").append("^").append("")
        .append("^").append("").append("|").append("").append("^").append("").append("^")
        .append("").append("|").append("").append("^").append("").append("^").append("")
        .append("|");
  }



  /**
   * Readpopulated doc exc publication.
   * 
   * @param docExc the doc exc
   * @param sbDBInputStatus the sb db input status
   * @param sbDBInputDesc the sb db input desc
   * @param sbDBInputSrcSys the sb db input src sys
   */
  public void readpopulatedDocExcPublication(DocumentException docExc,
      StringBuffer sbDBInputStatus, StringBuffer sbDBInputDesc, StringBuffer sbDBInputSrcSys) {

    PublicationExceptionType pubExc = docExc.getPublication();
    List<DescriptionType> listDocExc = pubExc.getDescription();
    // codeid^valueText^gtin|
    for (DescriptionType doc : listDocExc) {
      sbDBInputDesc =
          sbDBInputDesc.append(doc.getCode()).append("^").append(doc.getValue()).append("^")
              .append(pubExc.getGtin()).append("|");
    }



    // 104831 Rework Publication from 1ws ,
    // gtin^DocId^GDSNItemSuccessflag^GDSNPublicationSuccessFlag^CICFlag^GUDIDPublishFlag^InternalStatus^ExternalStatus|
    sbDBInputStatus =
        sbDBInputStatus.append(pubExc.getGtin()).append("^")
            .append(docExc.getOriginatingDocumentId()).append("^").append("").append("^")
            .append("").append("^").append("").append("^").append("").append("^").append("")
            .append("^").append("104831").append("|");

    // item+ pub+registry+ItemSync+itemAuthorization
    // operation^gtin^informationProviderGLN^targetMarket^dataRecipientGLN^type^AdditionalInformation^registrationDate^itemSyncErrorDate^authorizationDate^authorizationState^confirmationStatusDetail|
    sbDBInputSrcSys.append("operation").append("^").append(pubExc.getOperation()).append("^")
        .append(pubExc.getGtin()).append("|").append("gtin").append("^").append(pubExc.getGtin())
        .append("^").append(pubExc.getGtin()).append("|").append("informationProviderGLN")
        .append("^").append(pubExc.getInformationProviderGLN()).append("^")
        .append(pubExc.getGtin()).append("|").append("targetMarket").append("^")
        .append(pubExc.getTargetMarket()).append("^").append(pubExc.getGtin()).append("|")
        .append("dataRecipientGLN").append("^").append("").append("^").append("").append("|")
        .append("type").append("^").append(pubExc.getType()).append("^").append("").append("|")
        .append("").append("^").append("").append("^").append("").append("|").append("")
        .append("^").append("").append("^").append("").append("|").append("").append("^")
        .append("").append("^").append("").append("|").append("").append("^").append("")
        .append("^").append("").append("|").append("").append("^").append("").append("^")
        .append("").append("|").append("").append("^").append("").append("^").append("")
        .append("|");
  }


  /**
   * Read populated env item reg res.
   * 
   * @param env the env
   * @param hmCollectResParam the hm collect res param
   */
  public void readPopulatedEnvItemRegRes(Envelope env, HashMap hmCollectResParam) {
    StringBuffer sbDBInputStatus = new StringBuffer();
    StringBuffer sbDBInputDesc = new StringBuffer();
    StringBuffer sbDBInputSrcSys = new StringBuffer();

    GdsnItemRegistryResponseType gdsnItemRegRes = env.getGdsnItemRegistryResponse();
    List listDocExcAck = gdsnItemRegRes.getDocumentExceptionOrDocumentAcknowledgement();
    if (listDocExcAck.size() > 0) {

      for (Object doc : listDocExcAck) {
        org._1sync.read.GdsnItemRegistryResponseType.DocumentAcknowledgement docAck = null;
        org._1sync.read.GdsnItemRegistryResponseType.DocumentException docExc = null;

        if (doc instanceof org._1sync.read.GdsnItemRegistryResponseType.DocumentAcknowledgement) {
          docAck = (org._1sync.read.GdsnItemRegistryResponseType.DocumentAcknowledgement) doc;
          readpopulatedItemRegDocAck(docAck, sbDBInputStatus, sbDBInputSrcSys);

        } else if (doc instanceof org._1sync.read.GdsnItemRegistryResponseType.DocumentException) {
          docExc = (org._1sync.read.GdsnItemRegistryResponseType.DocumentException) doc;
          readpopulatedItemRegDocException(docExc, sbDBInputStatus, sbDBInputDesc, sbDBInputSrcSys);
        }
      }
      hmCollectResParam.put("sbDBInputStatus", sbDBInputStatus.toString());
      hmCollectResParam.put("sbDBInputDesc", sbDBInputDesc.toString());
      hmCollectResParam.put("sbDBInputSrcSys", sbDBInputSrcSys.toString());
      System.out.println("readPopulatedEnvItemRegRes sbDBInputStatus: "
          + sbDBInputStatus.toString());
      System.out.println("readPopulatedEnvItemRegRes sbDBInputDesc: " + sbDBInputDesc.toString());
    }

  }

  /**
   * Readpopulated item reg doc ack.
   * 
   * @param docAck the doc ack
   * @param sbDBInputStatus the sb db input status
   * @param sbDBInputSrcSys the sb db input src sys
   */
  public void readpopulatedItemRegDocAck(
      org._1sync.read.GdsnItemRegistryResponseType.DocumentAcknowledgement docAck,
      StringBuffer sbDBInputStatus, StringBuffer sbDBInputSrcSys) {


    // 4000822 Success-Uploaded Item to Item Registry ,
    // gtin^DocId^GDSNItemSuccessflag^GDSNPublicationSuccessFlag^CICFlag^GUDIDPublishFlag^InternalStatus^ExternalStatus|
    sbDBInputStatus =
        sbDBInputStatus.append(docAck.getGtin()).append("^").append(docAck.getDocumentId())
            .append("^").append("").append("^").append("").append("^").append("").append("^")
            .append("").append("^").append("").append("^").append("4000822").append("|");

    // item+ pub+registry+ItemSync+itemAuthorization
    // operation^gtin^informationProviderGLN^targetMarket^dataRecipientGLN^type^AdditionalInformation^registrationDate^itemSyncErrorDate^authorizationDate^authorizationState^confirmationStatusDetail|
    sbDBInputSrcSys.append("operation").append("^").append(docAck.getOperation()).append("^")
        .append(docAck.getGtin()).append("|").append("gtin").append("^").append(docAck.getGtin())
        .append("^").append(docAck.getGtin()).append("|").append("informationProviderGLN")
        .append("^").append(docAck.getInformationProviderGLN()).append("^")
        .append(docAck.getGtin()).append("|").append("targetMarket").append("^")
        .append(docAck.getTargetMarket()).append("^").append(docAck.getGtin()).append("|")
        .append("").append("^").append("").append("^").append("").append("|").append("")
        .append("^").append("").append("^").append("").append("|").append("additionalInformation")
        .append("^").append(docAck.getAdditionalInformation()).append("^").append(docAck.getGtin())
        .append("|").append("registrationDate").append("^").append(docAck.getRegistrationDate())
        .append("^").append(docAck.getGtin()).append("|").append("").append("^").append("")
        .append("^").append("").append("|").append("").append("^").append("").append("^")
        .append("").append("|").append("").append("^").append("").append("^").append("")
        .append("|").append("").append("^").append("").append("^").append("").append("|");

  }

  /**
   * Readpopulated item reg doc exception.
   * 
   * @param docExc the doc exc
   * @param sbDBInputStatus the sb db input status
   * @param sbDBInputDesc the sb db input desc
   * @param sbDBInputSrcSys the sb db input src sys
   */
  public void readpopulatedItemRegDocException(
      org._1sync.read.GdsnItemRegistryResponseType.DocumentException docExc,
      StringBuffer sbDBInputStatus, StringBuffer sbDBInputDesc, StringBuffer sbDBInputSrcSys) {

    List<DescriptionType> listDocExc = docExc.getDescription();
    // codeid^valueText^gtin|
    for (DescriptionType doc : listDocExc) {
      sbDBInputDesc =
          sbDBInputDesc.append(doc.getCode()).append("^").append(doc.getValue()).append("^")
              .append(docExc.getGtin()).append("|");
    }



    // 4000818 Rework Item from 1ws Item Registry ,
    // gtin^DocId^GDSNItemSuccessflag^GDSNPublicationSuccessFlag^CICFlag^GUDIDPublishFlag^InternalStatus^ExternalStatus|
    sbDBInputStatus =
        sbDBInputStatus.append(docExc.getGtin()).append("^").append(docExc.getDocumentId())
            .append("^").append("").append("^").append("").append("^").append("").append("^")
            .append("").append("^").append("").append("^").append("4000818").append("|");


    // item+ pub+registry+ItemSync+itemAuthorization
    // operation^gtin^informationProviderGLN^targetMarket^dataRecipientGLN^type^AdditionalInformation^registrationDate^itemSyncErrorDate^authorizationDate^authorizationState^confirmationStatusDetail|
    sbDBInputSrcSys.append("operation").append("^").append(docExc.getOperation()).append("^")
        .append(docExc.getGtin()).append("|").append("gtin").append("^").append(docExc.getGtin())
        .append("^").append(docExc.getGtin()).append("|").append("informationProviderGLN")
        .append("^").append(docExc.getInformationProviderGLN()).append("^")
        .append(docExc.getGtin()).append("|").append("targetMarket").append("^")
        .append(docExc.getTargetMarket()).append("^").append(docExc.getGtin()).append("|")
        .append("").append("^").append("").append("^").append("").append("|").append("")
        .append("^").append("").append("^").append("").append("|").append("additionalInformation")
        .append("^").append(docExc.getAdditionalInformation()).append("^").append(docExc.getGtin())
        .append("|").append("").append("^").append("").append("^").append("").append("|")
        .append("").append("^").append("").append("^").append("").append("|").append("")
        .append("^").append("").append("^").append("").append("|").append("").append("^")
        .append("").append("^").append("").append("|").append("").append("^").append("")
        .append("^").append("").append("|");

  }

  /**
   * Read populated env item sync res.
   * 
   * @param env the env
   * @param hmCollectResParam the hm collect res param
   */
  public void readPopulatedEnvItemSyncRes(Envelope env, HashMap hmCollectResParam) {
    StringBuffer sbDBInputStatus = new StringBuffer();
    StringBuffer sbDBInputDesc = new StringBuffer();
    StringBuffer sbDBInputSrcSys = new StringBuffer();

    ItemSyncResponseType itemSyncRes = env.getItemSyncResponse();

    List listitemSyncDocAck = itemSyncRes.getDocumentException();
    if (listitemSyncDocAck.size() > 0) {

      for (Object doc : listitemSyncDocAck) {
        org._1sync.read.ItemSyncResponseType.DocumentAcknowledgement docAck = null;
        org._1sync.read.ItemSyncResponseType.DocumentException docExc = null;

        if (doc instanceof org._1sync.read.ItemSyncResponseType.DocumentAcknowledgement) {
          docAck = (org._1sync.read.ItemSyncResponseType.DocumentAcknowledgement) doc;
          // readpopulatedItemSyncDocAck( docAck,sbDBInputStatus);
          System.out.println("org._1sync.read.ItemSyncResponseType.DocumentAcknowledgement:: "
              + docAck.getGtin());


        } else if (doc instanceof org._1sync.read.ItemSyncResponseType.DocumentException) {
          docExc = (org._1sync.read.ItemSyncResponseType.DocumentException) doc;
          readpopulatedItemSyncDocException(docExc, sbDBInputStatus, sbDBInputDesc, sbDBInputSrcSys);
        }
      }
      hmCollectResParam.put("sbDBInputStatus", sbDBInputStatus.toString());
      hmCollectResParam.put("sbDBInputDesc", sbDBInputDesc.toString());
      hmCollectResParam.put("sbDBInputSrcSys", sbDBInputSrcSys.toString());
      System.out.println("readPopulatedEnvItemSyncRes sbDBInputStatus : "
          + sbDBInputStatus.toString());
      System.out.println("readPopulatedEnvItemSyncRes sbDBInputDesc : " + sbDBInputDesc.toString());

    }

  }

  /**
   * Readpopulated item sync doc exception.
   * 
   * @param docExc the doc exc
   * @param sbDBInputStatus the sb db input status
   * @param sbDBInputDesc the sb db input desc
   * @param sbDBInputSrcSys the sb db input src sys
   */
  public void readpopulatedItemSyncDocException(
      org._1sync.read.ItemSyncResponseType.DocumentException docExc, StringBuffer sbDBInputStatus,
      StringBuffer sbDBInputDesc, StringBuffer sbDBInputSrcSys) {

    List<DescriptionType> listDocExc = docExc.getDescription();
    // codeid^valueText^gtin|
    for (DescriptionType doc : listDocExc) {
      sbDBInputDesc =
          sbDBInputDesc.append(doc.getCode()).append("^").append(doc.getValue()).append("^")
              .append(docExc.getGtin()).append("|");
    }



    // 104832 Rework from 1ws-GUDID Sync ,
    // gtin^DocId^GDSNItemSuccessflag^GDSNPublicationSuccessFlag^CICFlag^GUDIDPublishFlag^InternalStatus^ExternalStatus|
    sbDBInputStatus =
        sbDBInputStatus.append(docExc.getGtin()).append("^").append(docExc.getDocumentId())
            .append("^").append("").append("^").append("").append("^").append("").append("^")
            .append("").append("^").append("").append("^").append("104832").append("|");

    // item+ pub+registry+ItemSync+itemAuthorization
    // operation^gtin^informationProviderGLN^targetMarket^dataRecipientGLN^type^AdditionalInformation^registrationDate^itemSyncErrorDate^authorizationDate^authorizationState^confirmationStatusDetail|
    sbDBInputSrcSys.append("operation").append("^").append(docExc.getOperation()).append("^")
        .append(docExc.getGtin()).append("|").append("gtin").append("^").append(docExc.getGtin())
        .append("^").append(docExc.getGtin()).append("|").append("informationProviderGLN")
        .append("^").append(docExc.getInformationProviderGLN()).append("^")
        .append(docExc.getGtin()).append("|").append("targetMarket").append("^")
        .append(docExc.getTargetMarket()).append("^").append(docExc.getGtin()).append("|")
        .append("dataRecipientGLN").append("^").append(docExc.getDataRecipientGLN()).append("^")
        .append(docExc.getGtin()).append("|").append("").append("^").append("").append("^")
        .append("").append("|").append("additionalInformation").append("^")
        .append(docExc.getAdditionalInformation()).append("^").append(docExc.getGtin()).append("|")
        .append("").append("^").append("").append("^").append("").append("|")
        .append("itemSyncErrorDate").append("^").append(docExc.getItemSyncErrorDate()).append("^")
        .append(docExc.getGtin()).append("|").append("").append("^").append("").append("^")
        .append("").append("|").append("").append("^").append("").append("^").append("")
        .append("|").append("").append("^").append("").append("^").append("").append("|");


  }


  /**
   * Read populated env item auth res.
   * 
   * @param env the env
   * @param hmCollectResParam the hm collect res param
   */
  public void readPopulatedEnvItemAuthRes(Envelope env, HashMap hmCollectResParam) {

    StringBuffer sbDBInputStatus = new StringBuffer();
    StringBuffer sbDBInputDesc = new StringBuffer();
    StringBuffer sbDBInputSrcSys = new StringBuffer();

    ItemAuthorizationResponseType itemAuthRes = env.getItemAuthorizationResponse();
    List<DocumentNotification> listDocNotif = itemAuthRes.getDocumentNotification();
    for (DocumentNotification docNoti : listDocNotif) {
      readpopulatedItemAuthResDocNoti(docNoti, sbDBInputStatus, sbDBInputDesc, sbDBInputSrcSys);
    }
    hmCollectResParam.put("sbDBInputStatus", sbDBInputStatus.toString());
    hmCollectResParam.put("sbDBInputDesc", sbDBInputDesc.toString());
    hmCollectResParam.put("sbDBInputSrcSys", sbDBInputSrcSys.toString());
    System.out.println("readPopulatedEnvItemAuthRes sbDBInputStatus : "
        + sbDBInputStatus.toString());
    System.out.println("readPopulatedEnvItemAuthRes sbDBInputDesc : " + sbDBInputDesc.toString());
    System.out.println("readPopulatedEnvItemAuthRes sbDBInputSrcSys : "
        + sbDBInputSrcSys.toString());


  }

  /**
   * Readpopulated item auth res doc noti.
   * 
   * @param docNoti the doc noti
   * @param sbDBInputStatus the sb db input status
   * @param sbDBInputDesc the sb db input desc
   * @param sbDBInputSrcSys the sb db input src sys
   */
  public void readpopulatedItemAuthResDocNoti(DocumentNotification docNoti,
      StringBuffer sbDBInputStatus, StringBuffer sbDBInputDesc, StringBuffer sbDBInputSrcSys) {

    String strGtin = docNoti.getGtin();
    String strDocId = docNoti.getDocumentId();
    String strInfoGLN = docNoti.getInformationProviderGLN();
    String strTrgMkt = docNoti.getTargetMarket();
    String strRecGLN = docNoti.getDataRecipientGLN();
    XMLGregorianCalendar xmlGdt = docNoti.getAuthorizationDate();
    String strAuthState = docNoti.getAuthorizationState();
    String strConfirmStatDet = docNoti.getConfirmationStatusDetail();



    if (strAuthState.equals("SYNCHRONISED")) {
      // 104834 6b-Successfully published to GUDID
      // gtin^DocId^GDSNItemSuccessflag^GDSNPublicationSuccessFlag^CICFlag^GUDIDPublishFlag^InternalStatus^ExternalStatus|
      sbDBInputStatus =
          sbDBInputStatus.append(strGtin).append("^").append(strDocId).append("^").append("")
              .append("^").append("").append("^").append("").append("^").append("Y").append("^")
              .append("").append("^").append("104834").append("|");

    } else if (strAuthState.equals("REVIEW")) {
      // 104833 a-Error publishing to GUDID
      // gtin^DocId^GDSNItemSuccessflag^GDSNPublicationSuccessFlag^CICFlag^GUDIDPublishFlag^InternalStatus^ExternalStatus|
      sbDBInputStatus =
          sbDBInputStatus.append(strGtin).append("^").append(strDocId).append("^").append("")
              .append("^").append("").append("^").append("").append("^").append("").append("^")
              .append("").append("^").append("104833").append("|");

      // codeid^valueText^gtin|
      sbDBInputDesc =
          sbDBInputDesc.append("").append("^").append(strConfirmStatDet).append("^")
              .append(strGtin).append("|");
    }

    // item+ pub+registry+ItemSync+itemAuthorization
    // operation^gtin^informationProviderGLN^targetMarket^dataRecipientGLN^type^AdditionalInformation^registrationDate^itemSyncErrorDate^authorizationDate^authorizationState^confirmationStatusDetail|
    sbDBInputSrcSys.append("documentId").append("^").append(strDocId).append("^").append(strGtin)
        .append("|").append("gtin").append("^").append(strGtin).append("^").append(strGtin)
        .append("|").append("informationProviderGLN").append("^").append(strInfoGLN).append("^")
        .append(strGtin).append("|").append("targetMarket").append("^").append(strTrgMkt)
        .append("^").append(strGtin).append("|").append("dataRecipientGLN").append("^")
        .append(strRecGLN).append("^").append(strGtin).append("|").append("").append("^")
        .append("").append("^").append("").append("|").append("").append("^").append("")
        .append("^").append("").append("|").append("").append("^").append("").append("^")
        .append("").append("|").append("").append("^").append("").append("^").append("")
        .append("|").append("authorizationDate").append("^").append(xmlGdt).append("^")
        .append(strGtin).append("|").append("authorizationState").append("^").append(strAuthState)
        .append("^").append(strGtin).append("|").append("confirmationStatusDetail").append("^")
        .append(strConfirmStatDet).append("^").append(strGtin).append("|");



  }

}
