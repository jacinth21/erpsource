package org._1sync.create;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

import org._1sync.create.CatalogueRequestType.Document;
import org._1sync.create.GlobalType.GtinName;
import org._1sync.create.GlobalType.NetContent;
import org._1sync.create.TargetType.AlternateItemIdentification;
import org._1sync.create.TargetType.FunctionalName;
import org._1sync.create.TargetType.IsDispatchUnit;
import org._1sync.create.TargetType.IsInvoiceUnit;
import org._1sync.create.TargetType.IsOrderableUnit;
import org._1sync.create.TargetType.StartAvailabilityDate;
import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmUDIUtil;



/**
 * The Class GmUDISub1wsCatRequestProxyBean. The below is the XMl Structure to be created. Pls refer
 * the class diagram Envelope Header Catalog request Header Document Item Global Attributes Target
 * market Attributes Core Attributes Flex Attributes Group Many attributes Publication
 * 
 * @author mpatel
 */

public class GmUDISub1wsCatRequestProxyBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());
  public static final String XMLDATEFORMAT = "yyyy-MM-dd'T'HH:mm:ss";

  /**
   * Populate cat request proxy xsd.
   * 
   * @param alTempList the al temp list
   * @param hmGeneralParam the hm general param
   * @param strDestFileName the str dest file name
   * @throws JAXBException the JAXB exception
   */
  public void populateCatRequestProxyXsd(ArrayList alTempList, HashMap hmGeneralParam,
      String strDestFileName) throws JAXBException {


    HashMap hmAllHeaderAttr = (HashMap) alTempList.get(0);
    populateEnv(hmAllHeaderAttr, alTempList, hmGeneralParam, strDestFileName);
  }

  /**
   * Populate env.
   * 
   * @param hmAllHeaderAttr the hm all header attr
   * @param alTempList the al temp list
   * @param hmGeneralParam the hm general param
   * @param strDestFileName the str dest file name
   * @throws JAXBException the JAXB exception
   */
  public void populateEnv(HashMap<String, Object> hmAllHeaderAttr, ArrayList alTempList,
      HashMap hmGeneralParam, String strDestFileName) throws JAXBException {


    GmUDIUtil gmUDIUtil = new GmUDIUtil();
    Envelope env = new Envelope();

    populateEnvHeader(hmAllHeaderAttr, env, alTempList, hmGeneralParam);
    populateEnvCatRequest(hmAllHeaderAttr, env, alTempList, hmGeneralParam);

    // Create XML FILE
    gmUDIUtil.createXML(strDestFileName, env);

  }

  /**
   * Populate env header.
   * 
   * @param hmAllHeaderAttr the hm all header attr
   * @param env the env
   * @param alTempList the al temp list
   * @param hmGeneralParam the hm general param
   */
  public void populateEnvHeader(HashMap<String, Object> hmAllHeaderAttr, Envelope env,
      ArrayList alTempList, HashMap hmGeneralParam) {


    // Header

    HeaderType headType = new HeaderType();
    headType
        .setVersion(GmCommonClass.parseNull(GmCommonClass.getUDIFdaXmlConfig("SCHEMA_VERSION")));
    headType.setSender((String) hmAllHeaderAttr.get("ME2_INFOPROVIDER"));
    headType.setRepresentedParty((String) hmAllHeaderAttr.get("XML_REPRESENTEDPARTY"));
    headType.setReceiver((String) hmAllHeaderAttr.get("XML_RECEIVER"));
    headType.setMessageId((String) hmGeneralParam.get("strMsgId"));
    Date crDt = new Date();
    crDt =
        GmUDIUtil.getStringToDate((String) hmAllHeaderAttr.get("XML_CREATIONDATE"), XMLDATEFORMAT);
    headType.setCreationDateTime(GmUDIUtil.dateToXmlDate(crDt));

    // Set Header
    env.setHeader(headType);
  }

  /**
   * Populate env cat request.
   * 
   * @param hmAllHeaderAttr the hm all header attr
   * @param env the env
   * @param alTempList the al temp list
   * @param hmGeneralParam the hm general param
   */
  public void populateEnvCatRequest(HashMap<String, Object> hmAllHeaderAttr, Envelope env,
      ArrayList alTempList, HashMap hmGeneralParam) {

    CatalogueRequestType catalogueRequest = new CatalogueRequestType();
    catalogueRequest.setVersion(GmCommonClass.parseNull(GmCommonClass
        .getUDIFdaXmlConfig("SCHEMA_VERSION")));
    // Header
    populateCatReqHeader(hmAllHeaderAttr, catalogueRequest, hmGeneralParam);
    log.debug("populateCatReqHeader:");
    // Document
    populateCatReqDoc(catalogueRequest, alTempList, hmGeneralParam);

    // Set Cat Request
    env.setCatalogueRequest(catalogueRequest);

  }


  /**
   * Populate cat req header.
   * 
   * @param hmAllHeaderAttr the hm all header attr
   * @param catalogueRequest the catalogue request
   * @param hmGeneralParam the hm general param
   */
  public void populateCatReqHeader(HashMap hmAllHeaderAttr, CatalogueRequestType catalogueRequest,
      HashMap hmGeneralParam) {

    DocumentHeaderType documentHeaderType = new DocumentHeaderType();
    documentHeaderType.setUserGLN((String) hmAllHeaderAttr.get("ME2_INFOPROVIDER"));
    documentHeaderType.setUserId((String) hmAllHeaderAttr.get("XML_BATCHUSERID"));

    // Set Cat Req Header
    catalogueRequest.setHeader(documentHeaderType);
    log.debug("populateCatReqHeader:::");


  }

  /**
   * Populate cat req doc.
   * 
   * @param catalogueRequest the catalogue request
   * @param alTempList the al temp list
   * @param hmGeneralParam the hm general param
   */
  public void populateCatReqDoc(CatalogueRequestType catalogueRequest, ArrayList alTempList,
      HashMap hmGeneralParam) {

    String strXMLType = (String) hmGeneralParam.get("XMLTYPE");


    List<CatalogueRequestType.Document> listCatalogueRequestDoc = new ArrayList<Document>();

    // Loop for each DI
    for (int dataCnt = 0; dataCnt < alTempList.size(); dataCnt++) {
      ArrayList alGtinMultiAttrs = null;
      HashMap hmDataAttrs = (HashMap) alTempList.get(dataCnt);
      // log.debug("hmeachDiAttr: " + hmDataAttrs);
      CatalogueRequestType.Document catreqDoc = new CatalogueRequestType.Document();
      catreqDoc.setDocumentId((String) hmDataAttrs.get("DOCID"));//


      if (strXMLType.equals("ITEMXML")) {
        ArrayList alMultiAttr =
            GmCommonClass.parseNullArrayList((ArrayList) hmGeneralParam.get("alMultiAttr"));
        alGtinMultiAttrs = extractSpecificMultiGtinAttr(hmDataAttrs, alMultiAttr);
        // Populate Item
        populateDocItem(hmDataAttrs, catreqDoc, alGtinMultiAttrs);
      } else if (strXMLType.equals("PUBXML")) {
        // Populate Publication
        populateDocPublication(hmDataAttrs, catreqDoc);
      }

      listCatalogueRequestDoc.add(catreqDoc);
    }// End Out Loop
    catalogueRequest.document = listCatalogueRequestDoc;

  }

  /**
   * Extract specific multi gtin attr.
   * 
   * @param hmDataAttrs the hm data attrs
   * @param alMultiAttr the al multi attr
   * @return the array list
   */
  public ArrayList extractSpecificMultiGtinAttr(HashMap hmDataAttrs, ArrayList alMultiAttr) {

    String strDataGtin = (String) hmDataAttrs.get("DE2_PRIMARY_DI_NUMBER");

    ArrayList alGtinMultiAttrs = new ArrayList();
    for (int multiCnt = 0; multiCnt < alMultiAttr.size(); multiCnt++) {
      HashMap hmMultiAttrs = (HashMap) alMultiAttr.get(multiCnt);
      String strMultiGtin = (String) hmMultiAttrs.get("C2060_DI_NUMBER");
      if (strMultiGtin.equals(strDataGtin)) {

        // Multi Attributes for this DI only
        alGtinMultiAttrs.add(hmMultiAttrs);
      }
    }// End Inner Loop
    return alGtinMultiAttrs;
  }

  /**
   * Populate doc item.
   * 
   * @param hmDataAttrs the hm data attrs
   * @param catreqDoc the catreq doc
   * @param alGtinMultiAttrs the al gtin multi attrs
   */
  public void populateDocItem(HashMap hmDataAttrs, CatalogueRequestType.Document catreqDoc,
      ArrayList alGtinMultiAttrs) {


    ItemCrType itemCrType = new ItemCrType();
    // XMLGregorianCalendar xcal1 = new XMLGregorianCalendar();
    // xcal.setTime(15, 20, 59);

    itemCrType.setOperation((String) hmDataAttrs.get("GDSN_ITEM_UPLOAD_OPERATION"));
    itemCrType.setGtin((String) hmDataAttrs.get("DE2_PRIMARY_DI_NUMBER"));
    itemCrType.setInformationProviderGLN((String) hmDataAttrs.get("ME2_INFOPROVIDER"));
    Date effDt = new Date();
    effDt = GmUDIUtil.getStringToDate((String) hmDataAttrs.get("XML_EFFECTIVEDATE"), XMLDATEFORMAT);
    itemCrType.setEffectiveDate(GmUDIUtil.dateToXmlDate(effDt));

    catreqDoc.setItem(itemCrType);

    // call Global
    populateGlobalAttr(hmDataAttrs, itemCrType, alGtinMultiAttrs);



    // Call Target Mkt
    populateTargetMktAttr(hmDataAttrs, itemCrType, alGtinMultiAttrs);
    log.debug("populateDocItem:::");

  }

  /**
   * Populate doc publication.
   * 
   * @param hmDataAttrs the hm data attrs
   * @param catreqDoc the catreq doc
   */
  public void populateDocPublication(HashMap hmDataAttrs, CatalogueRequestType.Document catreqDoc) {
    PublicationCrType pubCrType = new PublicationCrType();


    // As of now since we have 1 subscriber we don't need XML_MARKETGROUP
    List<JAXBElement<String>> alMktGrpOrDataRecGLN = new ArrayList<JAXBElement<String>>();

    QName dataRecGLN = new QName(null, "dataRecipientGLN");
    JAXBElement<String> strDataRecGLN =
        new JAXBElement<String>(dataRecGLN, String.class,
            (String) hmDataAttrs.get("XML_MARKETGROUP"));

    alMktGrpOrDataRecGLN.add(strDataRecGLN);
    pubCrType.marketGroupOrDataRecipientGLN = alMktGrpOrDataRecGLN;

    pubCrType.setOperation((String) hmDataAttrs.get("GDSN_PUBLISH_UPLOAD_OPERATION"));
    pubCrType.setGtin((String) hmDataAttrs.get("DE2_PRIMARY_DI_NUMBER"));
    pubCrType.setInformationProviderGLN((String) hmDataAttrs.get("ME2_INFOPROVIDER"));
    pubCrType.setTargetMarket((String) hmDataAttrs.get("ME4_TGT_MKT_COUNTRY_CODE"));
    // pubCrType.marketGroupOrDataRecipientGLN
    pubCrType.setType((String) hmDataAttrs.get("XML_TYPE"));
    Date pubDate = new Date();
    pubDate = GmUDIUtil.getStringToDate((String) hmDataAttrs.get("XML_PUBLISHDATE"), XMLDATEFORMAT);
    pubCrType.setPublishDate(GmUDIUtil.dateToXmlDate(pubDate));

    catreqDoc.setPublication(pubCrType);
    log.debug("populateDocPublication: ");

  }

  /**
   * Populate global attr.
   * 
   * @param hmDataAttrs the hm data attrs
   * @param itemCrType the item cr type
   * @param alGtinMultiAttrs the al gtin multi attrs
   */
  public void populateGlobalAttr(HashMap hmDataAttrs, ItemCrType itemCrType,
      ArrayList alGtinMultiAttrs) {

    // Global Attributes
    GlobalType globalType = new GlobalType();

    String strProductType =
        GmCommonClass.parseNull((String) hmDataAttrs.get("ME5_TRADE_ITEM_UNIT"));
    if (!strProductType.equals("")) {
      globalType.setProductType(strProductType);
    }


    String strGlobalCalssCode =
        GmCommonClass.parseNull((String) hmDataAttrs.get("ME22_CLASSIFICATION_CODE"));
    if (!strGlobalCalssCode.equals("")) {
      GlobalType.GlobalClassificationCategory globalClassificationCategory =
          new GlobalType.GlobalClassificationCategory();
      globalClassificationCategory.setCode(strGlobalCalssCode);
      globalType.setGlobalClassificationCategory(globalClassificationCategory);
    }

    String strGtinName = GmCommonClass.parseNull((String) hmDataAttrs.get("DE2_GTIN_NAME"));
    if (!strGtinName.equals("")) {
      GlobalType.GtinName gTypeGtinName = new GlobalType.GtinName();
      gTypeGtinName.setLang("en");
      gTypeGtinName.setValue(strGtinName);
      List<GlobalType.GtinName> listGlTypeGtinName = new ArrayList<GtinName>();
      listGlTypeGtinName.add(gTypeGtinName);
      globalType.gtinName = listGlTypeGtinName;
    }


    String strBrandName = GmCommonClass.parseNull((String) hmDataAttrs.get("DE8_BRAND_NUM"));
    if (!strBrandName.equals("")) {
      globalType.setBrandName(strBrandName);
    }

    String strBrandOwnGLN = GmCommonClass.parseNull((String) hmDataAttrs.get("ME2_BRANDOWNER_GLN"));
    if (!strBrandOwnGLN.equals("")) {
      globalType.setBrandOwnerGLN(strBrandOwnGLN);
    }

    String strNetContent = GmCommonClass.parseNull((String) hmDataAttrs.get("ME19_NET_CONTENT"));
    if (!strNetContent.equals("")) {
      GlobalType.NetContent gTypeNetContent = new GlobalType.NetContent();
      gTypeNetContent.setUom((String) hmDataAttrs.get("ME20_NET_CONTENT_UOM"));
      gTypeNetContent.setValue(new BigDecimal(strNetContent));
      List<GlobalType.NetContent> listNetContent = new ArrayList<NetContent>();
      listNetContent.add(gTypeNetContent);
      globalType.netContent = listNetContent;
    }

    String strIsBaseUnit = GmCommonClass.parseNull((String) hmDataAttrs.get("ME6_IS_BASE_UNIT"));
    if (!strIsBaseUnit.equals("")) {
      globalType.setIsBaseUnit(GmUDIUtil.getXmlBooleanTypeVal(strIsBaseUnit));
    }
    // Enum
    // What is globalType.setCancelDate(value);
    String strIsConUnit = GmCommonClass.parseNull((String) hmDataAttrs.get("ME7_IS_CONSUMER_UNIT"));
    if (!strIsConUnit.equals("")) {
      globalType.setIsConsumerUnit(GmUDIUtil.getXmlBooleanTypeVal(strIsConUnit));
    }


    // What is globalType.setCancelDate(value);
    String strQtyofNextLowertradeItem =
        GmCommonClass.parseNull((String) hmDataAttrs.get("DE21_QUANTITY_PER_PACKAGE"));
    if (!strQtyofNextLowertradeItem.equals("")) {
      globalType.setTotalQuantityOfNextLowerTradeItem(new BigInteger(strQtyofNextLowertradeItem));
    }

    // Set Object in Parent
    itemCrType.setGlobalAttributes(globalType);

  }


  /**
   * Populate target mkt attr.
   * 
   * @param hmDataAttrs the hm data attrs
   * @param itemCrType the item cr type
   * @param alGtinMultiAttrs the al gtin multi attrs
   */
  // @SuppressWarnings("deprecation")
  public void populateTargetMktAttr(HashMap hmDataAttrs, ItemCrType itemCrType,
      ArrayList alGtinMultiAttrs) {

    // Main Object
    TargetType trgType = new TargetType();

    // populate Core Attr
    populateCoreAttr(hmDataAttrs, trgType);
    // Populate Flex and
    populateFlexAttr(hmDataAttrs, trgType, alGtinMultiAttrs);

    // Set Object in Parent
    itemCrType.setTargetMarketAttributes(trgType);
    log.debug("populateTargetMktAttr:::");
  }


  /**
   * Populate core attr.
   * 
   * @param hmDataAttrs the hm data attrs
   * @param trgType the trg type
   */
  public void populateCoreAttr(HashMap hmDataAttrs, TargetType trgType) {

    String strTragetTp =
        GmCommonClass.parseNull((String) hmDataAttrs.get("ME4_TGT_MKT_COUNTRY_CODE"));
    if (!strTragetTp.equals("")) {
      trgType.setTargetMarket(strTragetTp);
    }


    String strBatchNumber = GmCommonClass.parseNull((String) hmDataAttrs.get("DE41_LOT_BATCH"));
    if (!strBatchNumber.equals("")) {
      trgType.setHasBatchNumber(GmUDIUtil.getXmlBooleanTypeVal(strBatchNumber));
    }


    // productDescription
    String strProdDesc =
        GmCommonClass.parseNull((String) hmDataAttrs.get("ME24_PRODUCT_DESCRIPTION"));
    if (!strProdDesc.equals("")) {
      TargetType.ProductDescription tTypeprodDesc = new TargetType.ProductDescription();
      tTypeprodDesc.setLang("en");
      tTypeprodDesc.setValue(strProdDesc);
      List<TargetType.ProductDescription> listProdDesc =
          new ArrayList<TargetType.ProductDescription>();
      listProdDesc.add(tTypeprodDesc);
      trgType.productDescription = listProdDesc;
    }


    // Required Only if doesTradeItemContainLatex attribute is True
    String strMarkFreeFrom =
        GmCommonClass.parseNull((String) hmDataAttrs.get("ODE47_DEVICE_CONTAIN_LATEX"));
    if (!strMarkFreeFrom.equals("")) {
      List<String> alMarkFreeFrom = new ArrayList<String>();
      alMarkFreeFrom.add(strMarkFreeFrom);
      trgType.packageMarksFreeFrom = alMarkFreeFrom;
    }


    String strItemIden_S = GmCommonClass.parseNull((String) hmDataAttrs.get("DE9_MODEL_NUMBER_S"));
    if (!strItemIden_S.equals("")) {
      TargetType.AlternateItemIdentification tTypeAltItemId =
          new TargetType.AlternateItemIdentification();
      tTypeAltItemId.setAgency((String) hmDataAttrs.get("DE9_MODEL_NUMBER_P"));
      tTypeAltItemId.setId(strItemIden_S);
      List<TargetType.AlternateItemIdentification> listAltItemId =
          new ArrayList<AlternateItemIdentification>();
      listAltItemId.add(tTypeAltItemId);
      trgType.alternateItemIdentification = listAltItemId;
    }

    String strFuncName = GmCommonClass.parseNull((String) hmDataAttrs.get("ME21_FUNCTIONAL_NAME"));
    if (!strFuncName.equals("")) {
      TargetType.FunctionalName tTypeFuncName = new TargetType.FunctionalName();
      tTypeFuncName.setLang("en");
      tTypeFuncName.setValue(strFuncName);
      List<TargetType.FunctionalName> listFuncName = new ArrayList<FunctionalName>();
      listFuncName.add(tTypeFuncName);
      trgType.functionalName = listFuncName;
    }



    String strISDispatchUnit =
        GmCommonClass.parseNull((String) hmDataAttrs.get("ME10_IS_DESPATCH_UNIT"));
    if (!strISDispatchUnit.equals("")) {
      TargetType.IsDispatchUnit tTypeISDispatchUnit = new TargetType.IsDispatchUnit();
      // Attribute not required
      // tTypeISDispatchUnit.setDataRecipientGLN((String) hmDataAttrs.get("ME2_INFOPROVIDER"));
      tTypeISDispatchUnit.setValue(GmUDIUtil.getXmlBooleanTypeVal(strISDispatchUnit));
      List<TargetType.IsDispatchUnit> listDispUnit = new ArrayList<IsDispatchUnit>();
      listDispUnit.add(tTypeISDispatchUnit);
      trgType.isDispatchUnit = listDispUnit;
    }



    String strIsInvoiceUnit =
        GmCommonClass.parseNull((String) hmDataAttrs.get("ME9_IS_INVOICE_UNIT"));
    if (!strIsInvoiceUnit.equals("")) {
      TargetType.IsInvoiceUnit tTypeIsInvoiceUnit = new TargetType.IsInvoiceUnit();
      // Attribute not required
      // tTypeIsInvoiceUnit.setDataRecipientGLN((String) hmDataAttrs.get("ME2_INFOPROVIDER"));
      tTypeIsInvoiceUnit.setValue(GmUDIUtil.getXmlBooleanTypeVal(strIsInvoiceUnit));
      List<TargetType.IsInvoiceUnit> listInvoiceUnit = new ArrayList<IsInvoiceUnit>();
      listInvoiceUnit.add(tTypeIsInvoiceUnit);
      trgType.isInvoiceUnit = listInvoiceUnit;
    }

    String strIsOrderUnit =
        GmCommonClass.parseNull((String) hmDataAttrs.get("ME8_IS_ORDERABLE_UNIT"));
    if (!strIsOrderUnit.equals("")) {
      TargetType.IsOrderableUnit tTypeIsOrderUnit = new TargetType.IsOrderableUnit();
      // Attribute not required
      // tTypeIsOrderUnit.setDataRecipientGLN((String) hmDataAttrs.get("ME2_INFOPROVIDER"));
      tTypeIsOrderUnit.setValue(GmUDIUtil.getXmlBooleanTypeVal(strIsOrderUnit));
      List<TargetType.IsOrderableUnit> listOrdUnit = new ArrayList<IsOrderableUnit>();
      listOrdUnit.add(tTypeIsOrderUnit);
      trgType.isOrderableUnit = listOrdUnit;
    }

    // PMT-7655 MJR3 change: this attributes are not mandatory hence remove from submission
    // height,width,depth and grossWeight
    /*
     * String strGrossWeig = GmCommonClass.parseNull((String) hmDataAttrs.get("ME11_GROSS_WEIGHT"));
     * if (!strGrossWeig.equals("")) { TargetType.GrossWeight tTypeGrossWgt = new
     * TargetType.GrossWeight(); tTypeGrossWgt.setUom((String)
     * hmDataAttrs.get("ME12_GROSS_WEIGHT_UOM")); tTypeGrossWgt.setValue(new
     * BigDecimal(strGrossWeig)); trgType.setGrossWeight(tTypeGrossWgt);
     * 
     * }
     */


    String strStAvailDate =
        GmCommonClass.parseNull((String) hmDataAttrs.get("ME23_START_AVAILABILITY_DATE"));
    if (!strStAvailDate.equals("")) {
      TargetType.StartAvailabilityDate tTypeStAvailDate = new TargetType.StartAvailabilityDate();
      // Attribute in XML not Required
      // tTypeStAvailDate.setDataRecipientGLN((String) hmDataAttrs.get("ME2_INFOPROVIDER"));
      Date StAvailDate = new Date();
      StAvailDate = GmUDIUtil.getStringToDate(strStAvailDate, XMLDATEFORMAT);
      tTypeStAvailDate.setValue(GmUDIUtil.dateToXmlDate(StAvailDate));
      List<TargetType.StartAvailabilityDate> listStAvailDt = new ArrayList<StartAvailabilityDate>();
      listStAvailDt.add(tTypeStAvailDate);
      trgType.startAvailabilityDate = listStAvailDt;
    }

    // Added
    String strDisContDate =
        GmCommonClass.parseNull((String) hmDataAttrs.get("DE24_PACKAGE_DISCONTINUE_DATE"));
    if (!strDisContDate.equals("")) {

      Date DisContDate = new Date();
      DisContDate = GmUDIUtil.getStringToDate(strDisContDate, XMLDATEFORMAT);
      trgType.setDiscontinueDate(GmUDIUtil.dateToXmlDate(DisContDate));
    }

  }

  /**
   * Populate flex attr.
   * 
   * @param hmDataAttrs the hm data attrs
   * @param trgType the trg type
   */
  public void populateFlexAttr(HashMap hmDataAttrs, TargetType trgType, ArrayList alGtinMultiAttrs) {

    // Set Object in AttrSupplierTypes Flex
    List<Object> listFlex = new ArrayList<Object>();

    // Flex Start
    AttrSupplierTypes attrSuppTypes = new AttrSupplierTypes();

    String strDCnt = GmCommonClass.parseNull((String) hmDataAttrs.get("DE3_DEVICE_COUNT"));
    if (!strDCnt.equals("")) {
      AttrRddType attrRAddRtypeDCnt = new AttrRddType();
      attrRAddRtypeDCnt.setName("uDIDDeviceCount");
      attrRAddRtypeDCnt.setValue(strDCnt);
      listFlex.add(attrRAddRtypeDCnt);
    }



    String strfDAUnitOfUseGTINVal =
        GmCommonClass.parseNull((String) hmDataAttrs.get("DE4_UNIT_DI_NUMBER"));
    if (!strfDAUnitOfUseGTINVal.equals("")) {
      AttrRddManyType attrRddManyTypeUseGtin = new AttrRddManyType();
      attrRddManyTypeUseGtin.setName("fDAUnitOfUseGTIN");
      List<String> listfDAUnitOfUseGTINVal = new ArrayList<String>();
      listfDAUnitOfUseGTINVal.add(strfDAUnitOfUseGTINVal);
      attrRddManyTypeUseGtin.value = listfDAUnitOfUseGTINVal;
      listFlex.add(attrRddManyTypeUseGtin);
    }


    String strfDAMedicalDeviceListing =
        GmCommonClass.parseNull((String) hmDataAttrs.get("DE36_FDA_LISTING_NUMBER"));
    if (!strfDAMedicalDeviceListing.equals("")) {
      AttrRddManyType attrRddManyTypeDivList = new AttrRddManyType();
      attrRddManyTypeDivList.setName("fDAMedicalDeviceListing");
      List<String> listfDAMedicalDeviceListing = new ArrayList<String>();
      listfDAMedicalDeviceListing.add(strfDAMedicalDeviceListing);
      attrRddManyTypeDivList.value = listfDAMedicalDeviceListing;
      listFlex.add(attrRddManyTypeDivList);

    }

    String strserialNumberLocationCode =
        GmCommonClass.parseNull((String) hmDataAttrs.get("DE43_SERIAL_NUMBER"));
    if (!strserialNumberLocationCode.equals("")) {
      AttrRddManyType attrRddManyTypeSerNoLCode = new AttrRddManyType();
      attrRddManyTypeSerNoLCode.setName("serialNumberLocationCode");
      List<String> listserialNumberLocationCode = new ArrayList<String>();
      listserialNumberLocationCode.add(strserialNumberLocationCode);
      attrRddManyTypeSerNoLCode.value = listserialNumberLocationCode;
      listFlex.add(attrRddManyTypeSerNoLCode);
    }


    // MJR3 Change PMT-7655: rename initialManufacturerSterilisation to
    // initialManufacturerSterilisationCode
    String strManufStr =
        GmCommonClass.parseNull((String) hmDataAttrs.get("DE60_DEVICE_PACKAGED_STERILE"));
    if (!strManufStr.equals("")) {
      AttrRddManyType attrRddManyTypeManufStr = new AttrRddManyType();
      attrRddManyTypeManufStr.setName("initialManufacturerSterilisationCode");
      List<String> listinitialManufacturerSterilisation = new ArrayList<String>();
      listinitialManufacturerSterilisation.add(strManufStr);
      attrRddManyTypeManufStr.value = listinitialManufacturerSterilisation;
      listFlex.add(attrRddManyTypeManufStr);
    }

    // MJR3 Change PMT-7655: rename initialSterilisationPriorToUse to
    // initialSterilisationPriorToUseCode
    String strManufStrPriorUse =
        GmCommonClass.parseNull((String) hmDataAttrs.get("DE62_STERILIZATION_METHOD"));
    if (!strManufStrPriorUse.equals("")) {
      AttrRddManyType attrRddManyTypeManufStrPriorUse = new AttrRddManyType();
      attrRddManyTypeManufStrPriorUse.setName("initialSterilisationPriorToUseCode");
      List<String> listAttrRddManyTypeManufStrPriorUse = new ArrayList<String>();
      listAttrRddManyTypeManufStrPriorUse.add(strManufStrPriorUse);
      attrRddManyTypeManufStrPriorUse.value = listAttrRddManyTypeManufStrPriorUse;
      listFlex.add(attrRddManyTypeManufStrPriorUse);
    }



    // Clinical Dimensions
    String strClinicalSzTp = GmCommonClass.parseNull((String) hmDataAttrs.get("DE51_TYPE1"));
    if (!strClinicalSzTp.equals("")) {
      AttrRddType attrRAddRtypeClinicalSzTp = new AttrRddType();
      attrRAddRtypeClinicalSzTp.setName("clinicalSizeType");
      attrRAddRtypeClinicalSzTp.setValue(strClinicalSzTp);
      listFlex.add(attrRAddRtypeClinicalSzTp);
    }

    String strClSzVal = GmCommonClass.parseNull((String) hmDataAttrs.get("DE52_VALUE1"));
    if (!strClSzVal.equals("")) {
      AttrQualRddType attrQualRaddTpClSzVal = new AttrQualRddType();
      attrQualRaddTpClSzVal.setName("clinicalSizeValue");
      attrQualRaddTpClSzVal.setQual((String) hmDataAttrs.get("DE53_UOM1"));
      attrQualRaddTpClSzVal.setValue(strClSzVal);
      listFlex.add(attrQualRaddTpClSzVal);
    }

    String strClinicalSzText =
        GmCommonClass.parseNull((String) hmDataAttrs.get("DE54_SIZE_TYPE_TEXT1"));
    if (!strClinicalSzText.equals("")) {
      AttrRddType attrRAddRtypeClinicalSzText = new AttrRddType();
      attrRAddRtypeClinicalSzText.setName("clinicalSizeText");
      attrRAddRtypeClinicalSzText.setValue(strClinicalSzText);
      listFlex.add(attrRAddRtypeClinicalSzText);
    }


    String strClinicalSzTp1 = GmCommonClass.parseNull((String) hmDataAttrs.get("DE51_TYPE2"));
    if (!strClinicalSzTp1.equals("")) {
      AttrRddType attrRAddRtypeClinicalSzTp1 = new AttrRddType();
      attrRAddRtypeClinicalSzTp1.setName("clinicalSizeType2");
      attrRAddRtypeClinicalSzTp1.setValue(strClinicalSzTp1);
      listFlex.add(attrRAddRtypeClinicalSzTp1);
    }


    String strClSzVal1 = GmCommonClass.parseNull((String) hmDataAttrs.get("DE52_VALUE2"));
    if (!strClSzVal1.equals("")) {
      AttrQualRddType attrQualRaddTpClSzVal1 = new AttrQualRddType();
      attrQualRaddTpClSzVal1.setName("clinicalSizeValue2");
      attrQualRaddTpClSzVal1.setQual((String) hmDataAttrs.get("DE53_UOM2"));
      attrQualRaddTpClSzVal1.setValue(strClSzVal1);
      listFlex.add(attrQualRaddTpClSzVal1);
    }

    String strClinicalSzText1 =
        GmCommonClass.parseNull((String) hmDataAttrs.get("DE54_SIZE_TYPE_TEXT2"));
    if (!strClinicalSzText1.equals("")) {
      AttrRddType attrRAddRtypeClinicalSzText1 = new AttrRddType();
      attrRAddRtypeClinicalSzText1.setName("clinicalSizeText2");
      attrRAddRtypeClinicalSzText1.setValue(strClinicalSzText1);
      listFlex.add(attrRAddRtypeClinicalSzText1);
    }

    // /////
    String strClinicalSzTp2 = GmCommonClass.parseNull((String) hmDataAttrs.get("DE51_TYPE3"));
    if (!strClinicalSzTp2.equals("")) {
      AttrRddType attrRAddRtypeClinicalSzTp2 = new AttrRddType();
      attrRAddRtypeClinicalSzTp2.setName("clinicalSizeType3");
      attrRAddRtypeClinicalSzTp2.setValue(strClinicalSzTp2);
      listFlex.add(attrRAddRtypeClinicalSzTp2);
    }

    String strClSzVal2 = GmCommonClass.parseNull((String) hmDataAttrs.get("DE52_VALUE3"));
    if (!strClSzVal2.equals("")) {
      AttrQualRddType attrQualRaddTpClSzVal2 = new AttrQualRddType();
      attrQualRaddTpClSzVal2.setName("clinicalSizeValue3");
      attrQualRaddTpClSzVal2.setQual((String) hmDataAttrs.get("DE53_UOM3"));
      attrQualRaddTpClSzVal2.setValue(strClSzVal2);
      listFlex.add(attrQualRaddTpClSzVal2);
    }


    String strClinicalSzText2 =
        GmCommonClass.parseNull((String) hmDataAttrs.get("DE54_SIZE_TYPE_TEXT3"));
    if (!strClinicalSzText2.equals("")) {
      AttrRddType attrRAddRtypeClinicalSzText2 = new AttrRddType();
      attrRAddRtypeClinicalSzText2.setName("clinicalSizeText3");
      attrRAddRtypeClinicalSzText2.setValue(strClinicalSzText2);
      listFlex.add(attrRAddRtypeClinicalSzText2);
    }


    String strClinicalSzTp3 = GmCommonClass.parseNull((String) hmDataAttrs.get("DE51_TYPE4"));
    if (!strClinicalSzTp3.equals("")) {
      AttrRddType attrRAddRtypeClinicalSzTp3 = new AttrRddType();
      attrRAddRtypeClinicalSzTp3.setName("clinicalSizeType4");
      attrRAddRtypeClinicalSzTp3.setValue(strClinicalSzTp3);
      listFlex.add(attrRAddRtypeClinicalSzTp3);
    }

    String strClSzVal3 = GmCommonClass.parseNull((String) hmDataAttrs.get("DE52_VALUE4"));
    if (!strClSzVal3.equals("")) {
      AttrQualRddType attrQualRaddTpClSzVal3 = new AttrQualRddType();
      attrQualRaddTpClSzVal3.setName("clinicalSizeValue4");
      attrQualRaddTpClSzVal3.setQual((String) hmDataAttrs.get("DE53_UOM4"));
      attrQualRaddTpClSzVal3.setValue(strClSzVal3);
      listFlex.add(attrQualRaddTpClSzVal3);
    }

    String strClinicalSzText3 =
        GmCommonClass.parseNull((String) hmDataAttrs.get("DE54_SIZE_TYPE_TEXT4"));
    if (!strClinicalSzText3.equals("")) {
      AttrRddType attrRAddRtypeClinicalSzText3 = new AttrRddType();
      attrRAddRtypeClinicalSzText3.setName("clinicalSizeText4");
      attrRAddRtypeClinicalSzText3.setValue(strClinicalSzText3);
      listFlex.add(attrRAddRtypeClinicalSzText3);
    }
    // Clinical Dimensions

    String strFdaGUDIDPubDate =
        GmCommonClass.parseNull((String) hmDataAttrs.get("DE12_FDAGUDIDPUBLISHDATE"));
    if (!strFdaGUDIDPubDate.equals("")) {
      AttrRddType attrRAddRtypeFdaGUDIDPubDate = new AttrRddType();
      //PMT 14581
      // attrRAddRtypeFdaGUDIDPubDate.setName("fDAGUDIDPublishDate");
      attrRAddRtypeFdaGUDIDPubDate.setName("udidFirstPublicationDateTime");
      Date fDaGudidPubDate = new Date();
      fDaGudidPubDate = GmUDIUtil.getStringToDate(strFdaGUDIDPubDate, XMLDATEFORMAT);
      attrRAddRtypeFdaGUDIDPubDate.setValue(GmUDIUtil.dateToXmlDate(fDaGudidPubDate).toString());
      listFlex.add(attrRAddRtypeFdaGUDIDPubDate);
    }

    String strExeMkt = GmCommonClass.parseNull((String) hmDataAttrs.get("DE15_DEVICE_SUBJECT_DM"));
    if (!strExeMkt.equals("")) {
      AttrRddType attrRAddRtypeExeMkt = new AttrRddType();
      attrRAddRtypeExeMkt.setName("isTradeItemExemptFromDirectPartMarking");
      attrRAddRtypeExeMkt.setValue(GmUDIUtil.getXmlYorN(strExeMkt));
      listFlex.add(attrRAddRtypeExeMkt);
    }



    String strDirectMakt = GmCommonClass.parseNull((String) hmDataAttrs.get("DE17_DM_DI_NUMBER"));
    if (!strDirectMakt.equals("")) {
      AttrRddType attrRAddRtypeDirMkt = new AttrRddType();
      attrRAddRtypeDirMkt.setName("directPartMarking");
      attrRAddRtypeDirMkt.setValue(strDirectMakt);
      listFlex.add(attrRAddRtypeDirMkt);
    }


    String strHumTissue = GmCommonClass.parseNull((String) hmDataAttrs.get("DE28_HCTP"));
    if (!strHumTissue.equals("")) {
      AttrRddType attrRAddRtypeHumTissue = new AttrRddType();
      attrRAddRtypeHumTissue.setName("doesTradeItemContainHumanTissue");
      attrRAddRtypeHumTissue.setValue(GmUDIUtil.getXmlStringVal(strHumTissue));
      listFlex.add(attrRAddRtypeHumTissue);
    }


    String strFDAMktAuth =
        GmCommonClass.parseNull((String) hmDataAttrs.get("DE31_DEV_EXEMPT_FROM_PREMARKET"));
    if (!strFDAMktAuth.equals("")) {
      AttrRddType attrRAddRtypeFDAMktAuth = new AttrRddType();
      attrRAddRtypeFDAMktAuth.setName("exemptFromFDAPreMarketAuthorization");
      attrRAddRtypeFDAMktAuth.setValue(GmUDIUtil.getXmlStringVal(strFDAMktAuth));
      listFlex.add(attrRAddRtypeFDAMktAuth);
    }


    String strMfgReuse = GmCommonClass.parseNull((String) hmDataAttrs.get("DE40_FOR_SINGLE_USE"));
    if (!strMfgReuse.equals("")) {
      AttrRddType attrRAddRtypeMfgReuse = new AttrRddType();
      attrRAddRtypeMfgReuse.setName("manufacturerDeclaredReusabilityType");
      attrRAddRtypeMfgReuse.setValue(strMfgReuse);
      listFlex.add(attrRAddRtypeMfgReuse);
    }



    String strDonIDNo =
        GmCommonClass.parseNull((String) hmDataAttrs.get("DE45_DONATION_IDENTIFICATION"));
    if (!strDonIDNo.equals("")) {
      AttrRddType attrRAddRtypeDonIDNo = new AttrRddType();
      attrRAddRtypeDonIDNo.setName("donationIdentificationNumberMarked");
      attrRAddRtypeDonIDNo.setValue(GmUDIUtil.getXmlStringVal(strDonIDNo));
      listFlex.add(attrRAddRtypeDonIDNo);
    }


    String strContLatex =
        GmCommonClass.parseNull((String) hmDataAttrs.get("DE46_NATURAL_RUBBER_LATEX"));
    if (!strContLatex.equals("")) {
      AttrRddType attrRAddRtypeContLatex = new AttrRddType();
      attrRAddRtypeContLatex.setName("doesTradeItemContainLatex");
      attrRAddRtypeContLatex.setValue(GmUDIUtil.getXmlStringVal(strContLatex));
      listFlex.add(attrRAddRtypeContLatex);
    }

    String strMRICode = GmCommonClass.parseNull((String) hmDataAttrs.get("DE50_MRI_SAFETY_STATUS"));
    if (!strMRICode.equals("")) {
      AttrRddType attrRAddRtypeMRICode = new AttrRddType();
      attrRAddRtypeMRICode.setName("mRICompatibilityCode");
      attrRAddRtypeMRICode.setValue(strMRICode);
      listFlex.add(attrRAddRtypeMRICode);
    }

    populateGroupMany(hmDataAttrs, listFlex, alGtinMultiAttrs);
    attrSuppTypes.attrOrAttrManyOrAttrQual = listFlex;

    // Set AttrSupplierTypes Flex In Target
    trgType.setFlex(attrSuppTypes);
    // Flex End

  }


  /**
   * Populate group many.
   * 
   * @param hmDataAttrs the hm data attrs
   * @param listFlex the list flex
   */
  public void populateGroupMany(HashMap hmDataAttrs, List<Object> listFlex,
      ArrayList alGtinMultiAttrs) {
    // PMT-7655 MJR3
    // Move alternateClassification to flex group
    populateMultiAttr(hmDataAttrs, listFlex, alGtinMultiAttrs);

    // PMT-7655 MJR3
    // Start attrGroupMany 0.1 dataCarrier
    AttrGroupRddManyType attrGrpRddManyTypeDataCarr = new AttrGroupRddManyType();
    attrGrpRddManyTypeDataCarr.setName("dataCarrier");

    AttrTypes attrTpsDataCarr = new AttrTypes();

    AttrType attrTpDataCarr = new AttrType();
    attrTpDataCarr.setName("dataCarrierTypeCode");
    attrTpDataCarr.setValue((String) hmDataAttrs.get("ME25_DATA_CARRIER"));


    List<Object> listAttrTypeDataCarr = new ArrayList<Object>();
    listAttrTypeDataCarr.add(attrTpDataCarr);

    attrTpsDataCarr.attrOrAttrManyOrAttrQual = listAttrTypeDataCarr;

    List<AttrTypes> alAttrTypesDataCarr = new ArrayList<AttrTypes>();
    alAttrTypesDataCarr.add(attrTpsDataCarr);
    attrGrpRddManyTypeDataCarr.row = alAttrTypesDataCarr;
    // End attrGroupMany 0.1


    // PMT-7655 MJR3
    // Start attrGroupMany 0.2 packagingDate for Expiration date
    String strExpDate = GmCommonClass.parseNull((String) hmDataAttrs.get("DE44_EXPIRATION_DATE"));
    if (!strExpDate.equals("")) {
      AttrGroupRddManyType attrGrpRddManyTypePkgDate = new AttrGroupRddManyType();
      attrGrpRddManyTypePkgDate.setName("packagingDate");

      AttrTypes attrTpsPkgDate = new AttrTypes();

      AttrType attrTpPkgDate = new AttrType();
      attrTpPkgDate.setName("tradeItemDateOnPackagingTypeCode");
      attrTpPkgDate.setValue((String) hmDataAttrs.get("DE44_EXPIRATION_DATE"));


      List<Object> listAttrTypePkgDate = new ArrayList<Object>();
      listAttrTypePkgDate.add(attrTpPkgDate);

      attrTpsPkgDate.attrOrAttrManyOrAttrQual = listAttrTypePkgDate;

      List<AttrTypes> alAttrTypesPkgDate = new ArrayList<AttrTypes>();
      alAttrTypesPkgDate.add(attrTpsPkgDate);
      attrGrpRddManyTypePkgDate.row = alAttrTypesPkgDate;
      listFlex.add(attrGrpRddManyTypePkgDate);// attrGroupMany to for packagingDate
    }
    // End attrGroupMany 0.2


    /*
     * --PMT-7655 MJR3 change:If NO, this attributure should not be submitted, If YES
     * 
     * // If isPackageMarkedWithManufactureDate = true,
     * 
     * then create a new loop of packagingDate with tradeItemDateOnPackagingTypeCode =
     * PRODUCTION_DATE .If isPackageMarkedWithManufactureDate =false, then no migration (left
     * blank).
     * 
     * String strMfgDate = GmCommonClass.parseNull((String)
     * hmDataAttrs.get("DE42_MANUFACTURING_DATE"));
     * 
     * String strMfgDateYOrN =GmUDIUtil.getXmlYorN(strMfgDate);
     * 
     * if (!strMfgDate.equals("") && strMfgDateYOrN.equals("YES")) { AttrRddType
     * attrRAddRtypeMfgDate = new AttrRddType();
     * attrRAddRtypeMfgDate.setName("isPackageMarkedWithManufactureDate");
     * attrRAddRtypeMfgDate.setValue(strMfgDateYOrN); listFlex.add(attrRAddRtypeMfgDate); }
     */

    // Start attrGroupMany 0.3 packagingDate for Manufacture date
    String strMfgDate =
        GmCommonClass.parseNull((String) hmDataAttrs.get("DE42_MANUFACTURING_DATE"));

    if (!strMfgDate.equals("")) {

      AttrGroupRddManyType attrGrpRddManyTypeMfgDate = new AttrGroupRddManyType();
      attrGrpRddManyTypeMfgDate.setName("packagingDate");

      AttrTypes attrTpsMfgDate = new AttrTypes();

      AttrType attrTpMfgDate = new AttrType();
      attrTpMfgDate.setName("tradeItemDateOnPackagingTypeCode");
      attrTpMfgDate.setValue((String) hmDataAttrs.get("DE42_MANUFACTURING_DATE"));


      List<Object> listAttrTypeMfgDate = new ArrayList<Object>();
      listAttrTypeMfgDate.add(attrTpMfgDate);

      attrTpsMfgDate.attrOrAttrManyOrAttrQual = listAttrTypeMfgDate;

      List<AttrTypes> alAttrTypesMfgDate = new ArrayList<AttrTypes>();
      alAttrTypesMfgDate.add(attrTpsMfgDate);
      attrGrpRddManyTypeMfgDate.row = alAttrTypesMfgDate;
      listFlex.add(attrGrpRddManyTypeMfgDate);// attrGroupMany to for packagingDate

      // AttrRddType attrRAddRtypeMfgDate = new AttrRddType();
      // attrRAddRtypeMfgDate.setName("isPackageMarkedWithManufactureDate");
      // attrRAddRtypeMfgDate.setValue(GmUDIUtil.getXmlYorN(strMfgDate));
      // listFlex.add(attrRAddRtypeMfgDate);
    }// END attrGroupMany 0.3


    // Start attrGroupMany 1
    AttrGroupRddManyType attrGrpRddManyType = new AttrGroupRddManyType();
    attrGrpRddManyType.setName("brandOwnerAdditionalTradeItemIdentification");

    AttrTypes attrTps = new AttrTypes();

    AttrType attrTpDUNS = new AttrType();
    attrTpDUNS.setName("brandOwnerAdditionalIdType");
    attrTpDUNS.setValue((String) hmDataAttrs.get("LBL_LABELER_DUNS"));

    AttrType attrTpDUNSNo = new AttrType();
    attrTpDUNSNo.setName("brandOwnerAdditionalIdValue");
    attrTpDUNSNo.setValue((String) hmDataAttrs.get("DE5_LABELER_DUNS_NUMBER"));

    List<Object> listAttrType = new ArrayList<Object>();
    listAttrType.add(attrTpDUNS);
    listAttrType.add(attrTpDUNSNo);

    attrTps.attrOrAttrManyOrAttrQual = listAttrType;

    List<AttrTypes> alAttrTypes = new ArrayList<AttrTypes>();
    alAttrTypes.add(attrTps);
    attrGrpRddManyType.row = alAttrTypes;
    // End attrGroupMany 1



    // Start attrGroupMany 2
    // OuterMost Group Many Trade Item
    AttrGroupRddManyType attrGManyTypeTradeItemContactInfo = new AttrGroupRddManyType();
    attrGManyTypeTradeItemContactInfo.setName("tradeItemContactInfo");

    // Row OuterMost Trade Item
    AttrTypes attrTpsTradeItemContactInfoR0 = new AttrTypes();

    List<Object> alAttrTpsTradeItemContactInfoR0 = new ArrayList<Object>();
    // Added Attr contactType to Outermost row
    String strContactType = GmCommonClass.parseNull((String) hmDataAttrs.get("CONTACT_TYPE"));
    if (!strContactType.equals("")) {
      AttrRddType attrRAddRtypeContactType = new AttrRddType();
      attrRAddRtypeContactType.setName("contactType");
      attrRAddRtypeContactType.setValue(strContactType);
      alAttrTpsTradeItemContactInfoR0.add(attrRAddRtypeContactType);// Add CUSTOMER_SUPPORT

    }


    // Outer attrGroupMany End TargetMarket
    AttrGroupRddManyType attrGManyTypeTrgMktComCh = new AttrGroupRddManyType();
    attrGManyTypeTrgMktComCh.setName("targetMarketCommunicationChannel");
    // Row for Target Mkt
    AttrTypes attrTpsTagMktCommR1 = new AttrTypes();


    // nested attrGroupMany
    AttrGroupRddManyType attrGManyTypeComCh = new AttrGroupRddManyType();
    attrGManyTypeComCh.setName("communicationChannel");

    // row
    AttrTypes attrTpsComChRow1 = new AttrTypes();
    AttrTypes attrTpsComChRow2 = new AttrTypes();

    // attr
    AttrType attrTpEmailLbl = new AttrType();
    attrTpEmailLbl.setName("communicationChannelCode");
    attrTpEmailLbl.setValue((String) hmDataAttrs.get("LBL_COM_CH11_EMAIL"));

    AttrType attrTpEmailID = new AttrType();
    attrTpEmailID.setName("communicationChannelNumber");
    attrTpEmailID.setValue((String) hmDataAttrs.get("DE27_CUSTOMER_CONTACT_EMAIL"));

    AttrType attrTpTelLBL = new AttrType();
    attrTpTelLBL.setName("communicationChannelCode");
    attrTpTelLBL.setValue((String) hmDataAttrs.get("LBL_COM_CH1_PHONE"));

    AttrType attrTpTelNo = new AttrType();
    attrTpTelNo.setName("communicationChannelNumber");
    attrTpTelNo.setValue((String) hmDataAttrs.get("DE26_CUSTOMER_CONTACT_PHONE"));

    // ROW 1
    List<Object> listAttrTypeEmail = new ArrayList<Object>();
    listAttrTypeEmail.add(attrTpEmailLbl);
    listAttrTypeEmail.add(attrTpEmailID);

    // ROW 2
    List<Object> listAttrTypePhone = new ArrayList<Object>();
    listAttrTypePhone.add(attrTpTelLBL);
    listAttrTypePhone.add(attrTpTelNo);

    attrTpsComChRow1.attrOrAttrManyOrAttrQual = listAttrTypeEmail;
    attrTpsComChRow2.attrOrAttrManyOrAttrQual = listAttrTypePhone;

    List<AttrTypes> alAttrGManyTypeComCh = new ArrayList<AttrTypes>();
    alAttrGManyTypeComCh.add(attrTpsComChRow1);
    alAttrGManyTypeComCh.add(attrTpsComChRow2);
    attrGManyTypeComCh.row = alAttrGManyTypeComCh;// nested attrGroupMany End

    List<Object> alAttrTpsTagMktCommR1 = new ArrayList<Object>();
    alAttrTpsTagMktCommR1.add(attrGManyTypeComCh);
    attrTpsTagMktCommR1.attrOrAttrManyOrAttrQual = alAttrTpsTagMktCommR1;

    List<AttrTypes> alAttrGManyTypeTrgMktComCh = new ArrayList<AttrTypes>();
    alAttrGManyTypeTrgMktComCh.add(attrTpsTagMktCommR1);
    attrGManyTypeTrgMktComCh.row = alAttrGManyTypeTrgMktComCh;
    // Outer attrGroupMany End

    alAttrTpsTradeItemContactInfoR0.add(attrGManyTypeTrgMktComCh);// Add entire
                                                                  // targetMarketCommunicationChannel
    attrTpsTradeItemContactInfoR0.attrOrAttrManyOrAttrQual = alAttrTpsTradeItemContactInfoR0;

    List<AttrTypes> alAttrGManyTypeTradeItemContactInfo = new ArrayList<AttrTypes>();
    alAttrGManyTypeTradeItemContactInfo.add(attrTpsTradeItemContactInfoR0);
    attrGManyTypeTradeItemContactInfo.row = alAttrGManyTypeTradeItemContactInfo;// Outer Most
                                                                                // attrGroupMany End
    // End attrGroupMany 2


    listFlex.add(attrGrpRddManyTypeDataCarr);// attrGroupMany to for data carrier
    listFlex.add(attrGrpRddManyType);// attrGroupMany to Flex

    listFlex.add(attrGManyTypeTradeItemContactInfo);


  }

  /**
   * Main call for the overloaded method. As Part of PMT-7655 multi attributes ahould be moved to
   * flex group
   * 
   * @param hmDataAttrs the hm data attrs
   * @param listFlex the list flex
   * @param alGtinMultiAttrs the al gtin multi attrs
   */
  public void populateMultiAttr(HashMap hmDataAttrs, List<Object> listFlex,
      ArrayList alGtinMultiAttrs) {
    String altClassGMDNSch = "";
    String altClassGMDNCode = "";

    // Start attrGroupMany 0
    AttrGroupRddManyType attrGrpRddManyTypealtclass = new AttrGroupRddManyType();
    attrGrpRddManyTypealtclass.setName("alternateClassification");

    List<AttrTypes> alAttrTypes = new ArrayList<AttrTypes>();

    populateMultiAttr(alGtinMultiAttrs, "DE32_FDA_SUB_NUM", "FDAPSN", alAttrTypes);

    populateMultiAttr(alGtinMultiAttrs, "DE34_FDA_PRODUCTCODE", "ProdCodeClassDB", alAttrTypes);

    // Add GMDN
    altClassGMDNSch = (String) hmDataAttrs.get("DE37_GMDNCODE_L");
    altClassGMDNCode = (String) hmDataAttrs.get("DE37_GMDNCODE");
    populateMultiAttr(altClassGMDNSch, altClassGMDNCode, alAttrTypes);

    // alAttrTypes.add(attrTps);
    attrGrpRddManyTypealtclass.row = alAttrTypes;
    // End attrGroupMany 0

    listFlex.add(attrGrpRddManyTypealtclass);// attrGroupMany to Flex


  }


  /**
   * Overloaded method
   * 
   * @param alGtinMultiAttrs the al gtin multi attrs
   * @param strMultiGrpType the str multi grp type
   * @param strRuleID the str rule id
   * @param alAttrTypes the al attr types
   */
  public void populateMultiAttr(ArrayList alGtinMultiAttrs, String strMultiGrpType,
      String strRuleID, List<AttrTypes> alAttrTypes) {
    HashMap hmMultiGrp = null;
    int intsize = alGtinMultiAttrs.size();

    for (int multiCnt = 0; multiCnt < intsize; multiCnt++) {

      hmMultiGrp = (HashMap) alGtinMultiAttrs.get(multiCnt);

      if (((String) hmMultiGrp.get("C906_RULE_GRP_ID")).equals(strMultiGrpType)) {

        String strAttrVal = (String) hmMultiGrp.get("C205D_ATTRIBUTE_VALUE");
        populateMultiAttr(strRuleID, strAttrVal, alAttrTypes);



      }
    }
  }

  /**
   * Overloaded common method method.
   * 
   * @param strRuleID the str rule id
   * @param strAttrVal the str attr val
   * @param alAttrTypes the al attr types
   */
  public void populateMultiAttr(String strRuleID, String strAttrVal, List<AttrTypes> alAttrTypes) {

    AttrTypes attrTps = new AttrTypes();

    AttrType attrTpScheme = new AttrType();
    attrTpScheme.setName("scheme");
    attrTpScheme.setValue(strRuleID);

    AttrType attrTpCode = new AttrType();
    attrTpCode.setName("code");
    attrTpCode.setValue(strAttrVal);

    List<Object> listAttrType = new ArrayList<Object>();
    listAttrType.add(attrTpScheme);
    listAttrType.add(attrTpCode);

    attrTps.attrOrAttrManyOrAttrQual = listAttrType;

    alAttrTypes.add(attrTps);

  }



}
