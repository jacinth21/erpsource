
<%
	/**********************************************************************************
	 * File			 	: GmRegnDistTerrMappingReport.jsp
	 * Desc		 		: 
	 * Version	 		: 1.0
	 * author			: Dhinakaran James
	 * Modified By to DHTMLX : Rajkumar Jayakumar Aug 20, 2010
	 ************************************************************************************/
%>
<%@ page language="java"%>
<%@ page
	import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>

<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@include file="/common/GmHeader.inc"%>
<%@ page buffer="16kb" autoFlush="true"%>
<%@ taglib prefix="fmtRegnDistTerrMappingReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!--sales\GmRegnDistTerrMappingReport.jsp  -->
<fmtRegnDistTerrMappingReport:setLocale value="<%=strLocale%>"/>
<fmtRegnDistTerrMappingReport:setBundle basename="properties.labels.sales.GmRegnDistTerrMappingReport"/>

<%
	response.setHeader("Cache-Control", "no-cache");
%>
<%
	try {
		GmServlet gm = new GmServlet();
		gm.checkSession(response, session);
		
		String gridData="";
		if(request.getAttribute("strXmlData")!=null){
			gridData=request.getAttribute("strXmlData").toString();
		}
%>

<HTML>
<HEAD>

<TITLE>Globus Medical: Region Distributor Territory Account Mapping Report</TITLE>

<style type="text/css" media="all">
div.exportlinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	margin: 2px 0 10px 0;
	width: 40%;
}
</style>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/fonts/font_roboto/roboto.css"/>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/sources/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/skins/skyblue/dhtmlx.css">
<%-- <script language="JavaScript" src="<%=strJsPath%>/Error.js"></script> --%>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<%-- PC-5633 new suite has been added--%>
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx_deprecated.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_link.js"></script>
<%-- <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script> --%>
<%-- <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script> --%>
<%-- <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script> --%>
<style type="text/css">
.even{
background-color:#ffffff !important;
}
.uneven{
background-color:#dcedfc !important;
}
div.gridbox table.hdr td {
color: #000000 !important;
background-color: #d2e9fc !important;
border: 1px solid white;
}
div.gridbox_material.gridbox .xhdr {
background:#dcedfc !important;
}
</style> 
 
 <script>
var gridObjData='<%=gridData%>';
var gridObj;
function fnExcelDown(){
gridObj.toExcel('/phpapp/excel/generate.php');
}

//PC-5633 Filters have been added and excel export is downloading.
function fnOnPageLoad() {
if(gridObjData!='')
{
gridObj = new dhtmlXGridObject('dataGridDiv');
gridObj.init();
gridObj.enableAlterCss("even","uneven");
gridObj.attachHeader('#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#text_filter,#numeric_filter,#text_filter,#select_filter,#text_filter');
gridObj.loadXMLString(gridObjData);
}
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">

<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmTerritoryServlet">

<input type="hidden" name="hAction" value=""/>

<table border="0" style="width=1250px;" cellspacing="0" cellpadding="0">
	<tr>
		<td height="25" class="RightDashBoardHeader"><fmtRegnDistTerrMappingReport:message key="LBL_REGN_DIS"/>
		<fmtRegnDistTerrMappingReport:message key="LBL_TER_ACC"/></td>
	</tr>
	
	<tr>
		<td>
			<div id="dataGridDiv" height="600px" width="1250px"></div>
		</td>
	</tr>
	
	<tr>
		<td align="center">
			<div class='exportlinks'><fmtRegnDistTerrMappingReport:message key="LBL_EXPORT"/> <img src='img/ico_file_excel.png' />&nbsp;
				<a href="#" onclick="fnExcelDown();"> <fmtRegnDistTerrMappingReport:message key="LBL_EXCEL"/> </a>
			</div>
		</td>
	</tr>
	
</table>

</FORM>
<%
	} catch (Exception e) {
		e.printStackTrace();
	}
%>

</BODY>

</HTML>