 <%
/**********************************************************************************
 * File		 		: GmDetailedSalesReport.jsp
 * Desc		 		: This screen is used for the Account Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>

<%@page import="com.globus.valueobject.common.GmDataStoreVO"%> 
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.sql.Date" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib uri="com.corda.taglib" prefix="ctl" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>

<!-- Removed "/WEB-INF/gmjsp-taglib.tld" since GmHeader.inc is included-->

<%@ taglib prefix="fmtDetailedSalesReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\GmDetailedSalesReport.jsp -->

<fmtDetailedSalesReport:setLocale value="<%=strLocale%>"/>
<fmtDetailedSalesReport:setBundle basename="properties.labels.sales.GmDetailedSalesReport"/>

<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strTimeZone = strGCompTimeZone;
	String strApplDateFmt = strGCompDateFmt;
	String strWikiTitle = GmCommonClass.getWikiTitle("DETAILED_MONTHLY_SALES");
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	ArrayList alReturn = (ArrayList)hmReturn.get("SALES");
	String strType = (String)request.getAttribute("hType")==null?"":(String)request.getAttribute("hType");
	
	java.util.Date dtFrmDate=(java.util.Date)request.getAttribute("hFrom");
	java.util.Date dtToDate=(java.util.Date)request.getAttribute("hTo");
	
	String strRegnFilter = GmCommonClass.parseNull((String)request.getAttribute("hUSFilter"));
	strRegnFilter = strRegnFilter.equals("") ? "0" : strRegnFilter;

	String strReportType = GmCommonClass.parseNull((String)request.getAttribute("hRptType"));
	
	ArrayList alRegion=(ArrayList)request.getAttribute("alRegion");
	strReportType = strReportType.equals("")?"S":strReportType;
	int intLength = alReturn.size();

	GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.sales.GmDetailedSalesReport", strSessCompanyLocale);
	String strGraphTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_DETAILED_MONTHLY"));
	int intSize = 0;
	HashMap hmLoop = new HashMap();

	String strId = "";
	String strNextAccId = "";
	String strName = "";
	String strDistId = "";
	String strDistName = "";
	String strDate = "";
	String strSales = "";
	String strDay = "";
	String strOrdId= "";
	String strParentOrdId = "";
	String strPrice = "";
	String strOrdSumId = "";
	
	String strShade = "";
	String strNextId = "";
	int intCount = 0;
	String strLine = "";
	String strTotal = "";
	String strAcctTotal = "";
	String strAvgDaily = "";
	String strRepName = "";
	
	boolean blFlag = false;
	double dbSales = 0.0;
	double dbAcctTotal = 0.0;
	double dbTotal = 0.0;
	double dbAvgDaily = 0.0;
	double dbTempTotal = 0.0;
	double dbDistTotal = 0.0;
	String strOrdDate = "";
	Date dtOrdDate=null;
	
	ArrayList alDist = new ArrayList();
	ArrayList alSales = new ArrayList();
	ArrayList alDate = new ArrayList();
	
	String strDeptId = (String)session.getAttribute("strSessDeptId")==null?"":(String)session.getAttribute("strSessDeptId");
	String strAccessLvl = (String)session.getAttribute("strSessAccLvl")==null?"":(String)session.getAttribute("strSessAccLvl");
	int intAccessLvl = Integer.parseInt(strAccessLvl);
	boolean bolAccess = true;
	if ((strDeptId.equals("S") && intAccessLvl < 5))
	{
		bolAccess = false;
	}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Sales Report </TITLE>

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script>

function fnSubmit()
{
	fnReloadSales(); // Call the Action in the Filters jsp
	//document.frmMain.submit();
}

function Toggle(val)
{
	
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className="";
		tabobj.style.background="#ffffff";
	}
}

function fnPrintPick(val)
{
	windowOpener('<%=strServletPath%>/GmEditOrderServlet?hMode=PrintPack&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
}

function fnViewOrder(val)
{
	windowOpener('/GmEditOrderServlet?hMode=PrintPrice&hidePartPrice=Y&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=810,height=450");	
}

function fnPrintCreditMemo(val)
{
	windowOpener('<%=strServletPath%>/GmReportCreditsServlet?hAction=PrintVersion&hOrderId='+val,"Credit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintCashAdjustMemo(val)
{
	windowOpener('<%=strServletPath%>/GmReportCreditsServlet?hAction=PrintCashAdj&hOrderId='+val,"Credit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnLoad()
{
    document.frmMain.Cbo_Report.value = '<%=strReportType%>';
}
function fnGo() 
{
document.frmMain.hAction.value = 'Reload';
document.frmMain.submit();
}


</script>
</HEAD>

<BODY leftmargin="20" topmargin="10"  onLoad="fnLoad();">
<FORM name="frmMain" method="POST" action="<%=strServletPath%>/GmDetailedSalesServlet">
<input type="hidden" name="hAction" value="Reload">

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table class="DtTable800" cellspacing="0" cellpadding="0">
		<tr>
			<td width="798" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="7">
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td height="25" class="RightDashBoardHeader" colspan="7"><fmtDetailedSalesReport:message key="LBL_DAILY_MONTHLY_SALES"/></td>
								<td  height="25" class="RightDashBoardHeader">
								<fmtDetailedSalesReport:message key="IMG_ALT_HELP" var = "varHelp"/>
								<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			
					<tr><td class="Line" colspan="7"></td></tr>
					<tr>
						<td height="25" class="RightTextBlue" colspan="7">&nbsp;&nbsp;&nbsp;<fmtDetailedSalesReport:message key="LBL_FROM_DATE" var="varFrmDt"/><gmjsp:label type="RegularText"  SFLblControlName="${varFrmDt}" td="false"/> &nbsp;
 				<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtFrmDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
				&nbsp;&nbsp;<fmtDetailedSalesReport:message key="LBL_TO_DATE" var="varToDt"/><gmjsp:label type="RegularText"  SFLblControlName="${varToDt}" td="false"/>&nbsp; 
				<gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=dtToDate==null?null:new java.sql.Date(dtToDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;
<%
			if (bolAccess){
%>
					<fmtDetailedSalesReport:message key="LBL_REPORT_TYPE" var="varReportType"/>
				&nbsp;&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varReportType}:" td="false"/>
				<select name="Cbo_Report" class="RightText">
					<option value="S"><fmtDetailedSalesReport:message key="LBL_SUMMARY"/>
					<option value="D"><fmtDetailedSalesReport:message key="LBL_DETAIL"/>
					<option value="G"><fmtDetailedSalesReport:message key="LBL_GROUP_BREAKUP"/>
				</select>
<%
			}
%>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<fmtDetailedSalesReport:message key="BTN_GO" var="varGo"/><gmjsp:button value="&nbsp;${varGo}&nbsp;" name="Btn_Go" gmClass="button" buttonType="Load" onClick="javascript:fnReloadSales('Reload');" />
						</td>
					</tr>
					<tr><td colspan="7" height="20">
						<jsp:include page="/sales/GmSalesFilters.jsp" >
						<jsp:param name="FRMNAME" value="frmMain" />
						<jsp:param name="TERRITORIES" value="DISPLAY" />
						<jsp:param name="HACTION" value="Reload" />
						</jsp:include>
					</td></tr>

<%
	if (strReportType.equals("S"))
	{
%>
					<tr><td class="Line" colspan="7"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="100"><fmtDetailedSalesReport:message key="LBL_FIELD_SALES"/></td>
						<td width="150"><fmtDetailedSalesReport:message key="LBL_ACCOUNT_NAME"/></td>
						<td width="300"><fmtDetailedSalesReport:message key="LBL_PACK_SLIP"/></td>
						<td width="200"><fmtDetailedSalesReport:message key="LBL_DO_ID"/></td>
						<td width="200"><fmtDetailedSalesReport:message key="LBL_REP"/></td>
						<td width="100"><fmtDetailedSalesReport:message key="LBL_ORDER_DATE"/></td>
						<td width="100"><fmtDetailedSalesReport:message key="LBL_AMOUNT"/></td>
					</tr>
<%
						int intAccCount = 0;
						boolean bolAcFl = false;
						if (intLength > 0)
						{
							HashMap hmTempLoop = new HashMap();

							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alReturn.get(i);
								if (i<intLength-1)
								{
									hmTempLoop = (HashMap)alReturn.get(i+1);
									strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("DID"));
									strNextAccId = GmCommonClass.parseNull((String)hmTempLoop.get("ID"));
								}

								strDistId = GmCommonClass.parseNull((String)hmLoop.get("DID"));
								strDistName = GmCommonClass.parseNull((String)hmLoop.get("DNAME"));
								strId	= GmCommonClass.parseNull((String)hmLoop.get("ID"));
								strName = GmCommonClass.parseNull((String)hmLoop.get("NAME"));
								strPrice = GmCommonClass.parseZero((String)hmLoop.get("SALES"));
								strOrdId = GmCommonClass.parseZero((String)hmLoop.get("ORDID"));
								//dtOrdDate=(java.sql.Date)hmLoop.get("ORDDATE");
								//strOrdDate = GmCommonClass.getStringFromDate(dtOrdDate,strApplDateFmt);
								strOrdDate = GmCommonClass.parseZero((String)hmLoop.get("ORDDATE"));
								strParentOrdId = GmCommonClass.parseNull((String)hmLoop.get("PARENTID"));
								strOrdSumId = strParentOrdId.equals("")?strOrdId:strParentOrdId;
								strRepName = GmCommonClass.parseNull((String)hmLoop.get("RNAME"));
								
								dbSales = Double.parseDouble(strPrice);
								dbAcctTotal = dbAcctTotal + dbSales;
								dbTotal = dbTotal + dbSales;
								dbDistTotal = dbDistTotal + dbSales;
								
								if (strDistId.equals(strNextId))
								{
									intCount++;
									strLine = "<TR><TD colspan=7 height=1 class=Line></TD></TR>";
								}
								else
								{
									strLine = "<TR><TD colspan=7 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
										strDistName = "";
										strLine = "";
									}
									else
									{
										blFlag = true;
									}
									intCount = 0;
								}
								
								if (strId.equals(strNextAccId))
								{
									intAccCount++;
								}
								else
								{
									if (intAccCount > 0)
									{
										strName = "";
										strLine = "";
									}
									else
									{
										bolAcFl = true;
									}
									intAccCount = 0;
								}
																
								strAcctTotal = ""+dbAcctTotal;

								if (intCount > 1)
								{
									strDistName = "";
									strLine = "";
								}
						
								strShade = (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows

								out.print(strLine);
								strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
								if (intCount == 1 || blFlag)
								{
								alDist.add(strDistName);
%>
					<tr id="tr<%=strDistId%>">
						<fmtDetailedSalesReport:message key="LBL_CLICK_TO_EXPAND" var="varClickToExpand"/>
						<td colspan="6" height="20" class="RightText">&nbsp;<A class="RightText" title="${varClickToExpand}" href="javascript:Toggle('<%=strDistId%>')"><%=strDistName%></a></td>
						<td class="RightText" id="td<%=strDistId%>" align="right"></td>
					</tr>
					<tr>
						<td colspan="7"><div style="display:none" id="div<%=strDistId%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tab<%=strDistId%>">
<%
								blFlag = false;
								}
								
								if (intAccCount == 1 || bolAcFl)
								{
%>
								<tr id="trAcc<%=strDistId%><%=strId%>">
									<td width="100">&nbsp;</td>
									<fmtDetailedSalesReport:message key="LBL_CLICK_TO_EXPAND" var="varClickToExpand"/>
									<td width="400" height="20" class="RightText">&nbsp;<A class="RightText" title="${varClickToExpand}" href="javascript:Toggle('Acc<%=strDistId%><%=strId%>')"><%=strName%></a></td>
									<td class="RightText" id="tdAcc<%=strDistId%><%=strId%>" align="right"></td>
								</tr>
								<tr>
									<td colspan="5"><div style="display:none" id="divAcc<%=strDistId%><%=strId%>">
										<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tabAcc<%=strDistId%><%=strId%>">
<%
								bolAcFl = false;
								}
%>

											<tr <%=strShade%>>
												<td width="100" class="RightText" height="17">&nbsp;</td>
												<td width="180" class="RightText">&nbsp;&nbsp;</td>
												<td width="300">
<%						// Check if the Orded is a Return or Duplicate Order -- redirect based on the type 						
						if ((strOrdId.charAt(strOrdId.length()-1)== 'R') ||(strOrdId.charAt(strOrdId.length()-1)== 'D') )
						{%><a href="javascript:fnPrintCreditMemo('<%=strOrdId%>');" >
<%						}else if (strOrdId.charAt(strOrdId.length()-1)== 'A') 
						{%><a href="javascript:fnPrintCashAdjustMemo('<%=strOrdId%>');" >
<%						}else
						{%>	<a href="javascript:fnPrintPick('<%=strOrdId%>');" title="View Packing Slip" >
<%						}%>	
						<fmtDetailedSalesReport:message key="IMG_ALT_VIEW" var="varView"/>	
						<img src="<%=strImagePath%>/packslip.gif" title="${varView}"  border=0></a>
						<a href="javascript:fnViewOrder('<%=strOrdSumId%>');"><fmtDetailedSalesReport:message key="IMG_ALT_ORDER" var="varOrder"/><img src="<%=strImagePath%>/ordsum.gif" title="${varOrder}"  border=0></a>&nbsp;<%=strOrdId%></td>
												<td width="200">&nbsp;<%=strParentOrdId%></td>
												<td width="200">&nbsp;<%=strRepName%></td>
												<td width="150" class="RightText">&nbsp;<%=strOrdDate%></td>
												<gmjsp:currency type="CurrText"  textValue="<%=strPrice%>"/>
											</tr>
											<tr><td colspan="7" bgcolor="#cccccc"></td></tr>
<%					
								if (intAccCount == 0 ||  i == intLength-1)
								{
%>
						<script>
							 document.all.tdAcc<%=strDistId%><%=strId%>.innerHTML = "<gmjsp:currency type="CurrText"  textValue="<%=strAcctTotal%>" td="false"/>&nbsp;"; 
						</script>
							
<%	
									if ( intAccCount == 0)
									{
%>
						</table></div></td></tr>
<%
									}
								dbAcctTotal = 0.0;
								}
%>
					
<%					
								if ( intCount == 0 || i == intLength-1)
								{
									strAcctTotal = ""+dbDistTotal;
									alSales.add(strAcctTotal);									
%>
							</table></div>
						</td>
					</tr>
					<script>
						 document.all.td<%=strDistId%>.innerHTML = "<gmjsp:currency type="CurrTextSign"  textValue="<%=strAcctTotal%>" td="false"/>&nbsp;";
					</script>
<%
								dbDistTotal = 0.0;					
								}
								strTotal = ""+dbTotal;
							}// end of FOR
%>
							</table></div>
						</td>
					</tr>
					<tr><td colspan="7" class="Line"></td></tr>
					<tr>
						<td align="right"  class="RightTableCaption" height="20" colspan="6"><fmtDetailedSalesReport:message key="LBL_GRAND_TOTAL"/> &nbsp;&nbsp;&nbsp;&nbsp;
						<gmjsp:currency type="CurrTextSign"  textValue="<%=strTotal%>"/></td>
					</tr>
					<tr><td colspan="7" class="Line"></td></tr>
										
<%
						} // End of IF
						else{
%>
					<tr><td colspan="7" class="Line"></td></tr>					
					<tr><td colspan="7" class="RightTextRed" height="50" align=center><fmtDetailedSalesReport:message key="LBL_NOSALES"/> ! </td></tr>
<%					
						}
	}else{
%>
				<tr>
					<td colspan="7">
						<display:table name="alReturn" class="its" id="currentRowObject" decorator="com.globus.displaytag.beans.sales.DTDetailedSalesWrapper" >
							<fmtDetailedSalesReport:message key="DT_PACK_SLIP" var="varPckSlip"/>
							<display:column property="ORDID" title="${varPckSlip}" class="alignleft" style="white-space: nowrap" sortable="true" group="1"/>
							<fmtDetailedSalesReport:message key="DT_DO_ID" var="varDoId"/>
							<display:column property="PARENTID" title="${varDoId}" sortable="true" class="alignleft" style="white-space: nowrap" />
							<fmtDetailedSalesReport:message key="DT_ORDER_DATE" var="varOrdDate"/>
							<display:column property="ORDDATE" title="${varOrdDate}" class="alignleft" sortable="true" />
							<fmtDetailedSalesReport:message key="DT_ACC_ID" var="varAccId"/>
							<display:column property="ID" title="${varAccId}" class="aligncenter" sortable="true"  />
							<fmtDetailedSalesReport:message key="DT_ACCOUNT" var="varAccount"/>
							<display:column property="NAME" title="${varAccount}" class="alignleft"  sortable="true" maxLength="13" />
							<fmtDetailedSalesReport:message key="DT_REP_NAME" var="varRepName"/>
							<display:column property="RNAME" title="${varRepName}" class="alignleft"  sortable="true" />
							<fmtDetailedSalesReport:message key="DT_DIST_ID" var="varDistId"/>
							<display:column property="DID" title="${varDistId}" class="alignleft" sortable="true"  />
							<fmtDetailedSalesReport:message key="DT_DISTRIBUTOR_NAME" var="varDisName"/>
							<display:column property="DNAME" title="${varDisName}" class="alignleft"  sortable="true" />
							<fmtDetailedSalesReport:message key="DT_SUB_TOTAL" var="varSubTot"/>
							<display:column property="SALES" title="${varSubTot}" sortable="true" class="alignright" />
							<fmtDetailedSalesReport:message key="DT_GRAND_TOTAL" var="varGrandTot"/>
							<display:column property="GRANDTOTAL" title="${varGrandTot}" class="alignright" sortable="true" />
<%
	if (strReportType.equals("G"))
	{
%>							
							<fmtDetailedSalesReport:message key="DT_PERCENT" var="varPercent"/>
							<display:column property="PERCENT" title="${varPercent}" class="alignright" sortable="true" />
							<fmtDetailedSalesReport:message key="DT_GROUP" var="varGrp"/>
							<display:column property="GROUPNM" title="${varGrp}" class="alignright" sortable="true" />
<%
	}
%>							
							
						</display:table>
					</td>
				</tr>
	
<%				
		}
%>

				</table>
			</td>
		</tr>
    </table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
