 <%
/**********************************************************************************
 * File		 		: GmSalesConsignPartSearch.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Richard 
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<!-- GmSalesVirtualActual.jsp -->
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<!-- Imports for Logger -->
<%@ taglib prefix="fmtSalesVirtualActual" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtSalesVirtualActual:setLocale value="<%=strLocale%>"/>
<fmtSalesVirtualActual:setBundle basename="properties.labels.sales.GmSalesVirtualActual"/>


<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.labels.sales.GmSalesVirtualActual", strSessCompanyLocale);
	
	String strAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	HashMap hmDropDown = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HMDROPDOWN"));
	String strPartNum = GmCommonClass.parseNull((String)request.getAttribute("hPartNum"));
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
	String colNumber="";
	
	ArrayList alSystem   = GmCommonClass.parseNullArrayList((ArrayList)hmDropDown.get("SYSTEM"));
	ArrayList alHierarchy   = GmCommonClass.parseNullArrayList((ArrayList)hmDropDown.get("HIERARCHY"));
	ArrayList alCategory   = GmCommonClass.parseNullArrayList((ArrayList)hmDropDown.get("CATEGORY"));
	ArrayList alType   = GmCommonClass.parseNullArrayList((ArrayList)hmDropDown.get("SETTYPE"));
	
	HashMap hmParam = (HashMap)request.getAttribute("hmParam");
	//String strSystem = GmCommonClass.parseNull((String)hmParam.get("SYSTEM"));
	//String strHierarchy = GmCommonClass.parseNull((String)hmParam.get("HIERARCHY"));
	//String strCategory = GmCommonClass.parseNull((String)hmParam.get("CATEGORY"));
	
	String strSystem =  GmCommonClass.parseNull( request.getParameter("Cbo_System")) ;
	String strHierarchy =  GmCommonClass.parseNull( request.getParameter("Cbo_Hierarchy")) ;
	String strCategory =	GmCommonClass.parseNull( request.getParameter("Cbo_Category")) ;
	String strType= GmCommonClass.parseNull( request.getParameter("Cbo_Type")) ;
	//String strCategory = "";
	log.debug(" hmParam is  " +hmParam);
	
	String strhSetIds 	= "";
	String strDist 		= "";
	String strName 		= "";
	String strSetId = "";
	String strLikeOption = "";
	String strTotalVCountColSpan = "4";
	int tdColSpan = 3;

	ArrayList alList = new ArrayList();
	ArrayList alSets = new ArrayList();
	ArrayList alLikeOptions = new ArrayList();

	strDist = GmCommonClass.parseNull( request.getParameter("Cbo_Id")) ;
	strLikeOption = GmCommonClass.parseNull( request.getParameter("Cbo_Search")) ;
	strSetId = GmCommonClass.parseNull( request.getParameter("Txt_PartNum")) ;
	
	int intSetLength = 0;
	int intDistLength = 0;
	log.debug(" Dist Id " + strDist);

	if (hmReturn != null) 
	{
		alList = (ArrayList)hmReturn.get("LIST");
		alLikeOptions = (ArrayList)hmReturn.get("LIKEOPTIONS");
	}

	if (strOpt.equals("BYDIST") || strOpt.equals("BYCDIST") || strOpt.equals("BYCDISTSALES"))
	{
		strName = "NM";
	}
	else
	{
		strName = "NAME";
	}	
	HashMap hmLoop = new HashMap();

	String strConId = "";
	String strSetName = "";
	//String strCategory = "";
	String strShipDate = "";
	String strTrack = "";
	String strVal = "";

	String strShade = "";

	String strVCount = "";
	String strFCount = "";
	String strCCount = "";
	String strRCount = "";
	String strTitleTag = "";
	String strColumnName = "";
	String strTitleForLogical = "Logical";
	
	if(strOpt.equals("BYPART"))
	{
		strTitleTag = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_CONS_LOGI_ACTUAL_PART"));
		strColumnName  = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_PART"));
	}
	else if (strOpt.equals("BYSET"))
	{
		strTitleTag = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_CONS_LOGI_ACTUAL_SET"));
		strColumnName  = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_ACTUAL"));
	}
	else if (strOpt.equals("BYDIST"))
	{
		strTitleTag = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_CONS_LOGI_ACTUAL_DIST"));
		strColumnName  = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_ACTUAL"));
	}
	else if (strOpt.equals("BYCDIST") || strOpt.equals("BYCDISTSALES") )
	{
		strTitleTag = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_CONS_NET_RPT_FIELD_SALES"));
		strTitleForLogical = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_SET"));
	}
	else if (strOpt.equals("BYCSET") || strOpt.equals("BYCSETSALES"))
	{
		strTitleTag =GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_CONS_NET_RPT_SET"));
		strTitleForLogical = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_SET"));
	}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Report- Sales Consignments Logical vs Actual</TITLE>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>


 

<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>

<script>

function fnViewSetActual(val, did)
{
	var varSearchTextId =  document.frmMain.Txt_PartNum.id;
	
	if(varSearchTextId == 1 )
	{
		document.frmMain.Cbo_DistId.value = did;
		document.frmMain.hSetIds.value = '';
		document.frmMain.hOpt.value = 'BYPARTDETAIL';
		
		document.frmMain.action ="<%=strServletPath%>/GmSalesConsignSearchServlet";
		document.frmMain.hAction.value = "Reload";
		document.frmMain.submit();		
	}
	else {
		document.frmMain.Cbo_DistId.value = did;
		document.frmMain.hSetIds.value = val;
		document.frmMain.hOpt.value = 'BYSET';
		
		document.frmMain.action ="<%=strServletPath%>/GmSalesConsignSearchServlet";
		document.frmMain.hAction.value = "Reload";
		document.frmMain.submit();	
	}
}


function fnViewSetDetails(val, did, type)
{
	
	document.frmMain.DistributorID.value = did;
	document.frmMain.SetNumber.value = val;
	document.frmMain.Cbo_DistId.value = did;
	document.frmMain.hSetIds.value = val;	
	document.frmMain.hOpt.value = 'BYSETPART';
	document.frmMain.action ="<%=strServletPath%>/GmSalesConsignSearchServlet";
	document.frmMain.hAction.value = "Reload";
	document.frmMain.submit();	
}

function fnReload()
{
	var varSearchTextId =  document.frmMain.Txt_PartNum.id;
	var varOpt = '<%=strOpt%>';

	if(varSearchTextId == 1 )
	{
		var varPartNum = document.frmMain.Txt_PartNum.value;
		if (varPartNum == '' && varOpt == 'BYPART')
		{
			Error_Details(message[201]);
		}
	}
	document.frmMain.hOpt.value = '<%=strOpt%>';
	document.frmMain.hAction.value = 'Reload';
	fnStartProgress('Y');
	fnSubmit();	
	
}
function fnEnter()
{
		if (event.keyCode == 13)
		{ 
			fnReload();
		}
}

function fnLog(id)
{
	windowOpener("/GmCommonLogServlet?hType=1207&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnSubmit()
{
if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	
	document.frmMain.submit();
}

</script>

</HEAD>

<BODY onkeyup="fnEnter();" leftmargin="20" topmargin="10" >
<FORM name="frmMain" method="POST" action="<%=strServletPath%>/GmSalesVirtualActualServlet">
<input type="hidden" name="hOpt" value="<%=strOpt%>">
<input type="hidden" name="hSetIds" value="<%=strhSetIds%>">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="Cbo_DistId" value="<%=strAction%>">
<input type="hidden" name="hPartID" value="<%=strPartNum%>">
<input type="hidden" name="DistributorID" >
<input type="hidden" name="SetNumber">

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="dttable900" cellspacing="0" cellpadding="0" >
		<tr>
			<td colspan="2" height="25" class="RightDashBoardHeader"> <%=strTitleTag%></span>			
			</td>
		</tr>
		<tr><td colspan="2"  class="Line" height="1"></td></tr>
		<tr><td colspan="2"  height=5>  </td></tr>
			<fmtSalesVirtualActual:message key="LBL_NAME" var="varName"/>	
			<tr class="shade"><td class="RightTableCaption">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <gmjsp:label type="RegularText"  SFLblControlName="${varName}:" td="false"/>
				
				&nbsp;<gmjsp:dropdown controlName="Cbo_Id"  seletedValue="<%= strDist%>" 	
					tabIndex="1"  width="350" value="<%=alList%>" codeId = "ID" codeName = "<%=strName%>" 
					defaultValue= "[Choose One]"  /> </td>
					
<%
				 if (strOpt.equals("BYPART"))
					{
 %>					<fmtSalesVirtualActual:message key="LBL_PART_NUM" var="varPnum"/>
 					&nbsp;&nbsp;&nbsp;&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varPnum}:" td="false"/>	
 					&nbsp;&nbsp;<input type="text" size="15" value="<%=strPartNum%>" name="Txt_PartNum" class="InputArea"  id="1" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=2>
 <%
				} else if (strOpt.equals("BYDIST") || strOpt.equals("BYCDIST") || strOpt.equals("BYCDISTSALES"))
					{
				  
 %>				 	<fmtSalesVirtualActual:message key="LBL_SET_NM" var="varSetNm"/>
 					<td class="RightTableCaption" align="Left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varSetNm}:" td="false"/> 	
 					&nbsp;&nbsp;<input type="text" size="25" value="<%=strSetId%>" name="Txt_PartNum" class="InputArea"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=2> 					
 					
 <% }  else { %>
 						<input type="hidden" name="Txt_PartNum">
<% }
			if(strOpt.equals("BYCDISTSALES")||strOpt.equals("BYCDIST"))
			{
%>				
			<tr><td colspan="2"   height="1" ></td></tr>
			<tr><td colspan="2"></td></tr>
			<tr><fmtSalesVirtualActual:message key="LBL_SYSTEM" var="varSystem"/>
				<td class="RightTableCaption" align="left"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <gmjsp:label type="RegularText"  SFLblControlName="${varSystem}: " td="false"/>					
				&nbsp;<gmjsp:dropdown controlName="Cbo_System"  seletedValue="<%= strSystem%>" 	
					tabIndex="1"   value="<%=alSystem%>" codeId = "ID" codeName = "NAME" 
					defaultValue= "[Choose One]"  /> </td>
				<fmtSalesVirtualActual:message key="LBL_HIERARCHY" var="varHierarcy"/>
				<td class="RightTableCaption" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<gmjsp:label type="RegularText"  SFLblControlName="${varHierarcy}:" td="false"/>
				 &nbsp;&nbsp;<gmjsp:dropdown controlName="Cbo_Hierarchy"  seletedValue="<%= strHierarchy%>" 	
					tabIndex="2"   value="<%=alHierarchy%>" codeId = "CODEID" codeName = "CODENMALT" 
					defaultValue= "[Choose One]"  /> </td>
				
			</tr>
			<tr><td colspan="2"   height="1" ></td></tr>
			<tr><td colspan="2"></td></tr>
			<tr class="shade">
				<fmtSalesVirtualActual:message key="LBL_CATEGORY" var="varCategory"/>
				<td class="RightTableCaption" align="left"> &nbsp;&nbsp; <gmjsp:label type="RegularText"  SFLblControlName="${varCategory}: " td="false"/>
			 		&nbsp;<gmjsp:dropdown controlName="Cbo_Category"  seletedValue="<%= strCategory%>" 	
					tabIndex="1"   value="<%=alCategory%>" codeId = "CODEID" codeName = "CODENM" 
					defaultValue= "[Choose One]"  /> </td>
					<fmtSalesVirtualActual:message key="LBL_TYPE" var="varType"/>
					<fmtSalesVirtualActual:message key="BTN_LOAD" var="varLoad"/>
					<td  height="25" class="RightTableCaption" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varType}:" td="false"/>
					&nbsp;&nbsp;<gmjsp:dropdown controlName="Cbo_Type"  seletedValue="<%= strType%>" 	
					tabIndex="1"   value="<%=alType%>" codeId = "CODEID" codeName = "CODENM" 
					defaultValue= "[Choose One]"  /> 
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" buttonType="Load" onClick="fnReload();" tabindex="4" />
				</td>
			</tr>
					
<% 			}else
				{
%>
				<fmtSalesVirtualActual:message key="BTN_LOAD" var="varLoad"/>
<td>
					
				&nbsp;&nbsp;<gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" buttonType="Load" onClick="fnReload();" tabindex="4" /></td>
<% }%>
				<p></p>
			</td></tr>	
			<tr><td colspan="2"  height=5>  </td></tr>	
			<tr><td colspan="2">			
				 <display:table name="requestScope.results.rows" class="its"  id="currentRowObject" export="true" varTotals="totals" cellpadding="0" cellspacing="0"  freezeHeader="true" 
					decorator="com.globus.displaytag.beans.DTSalesVirtualActualWrapper" > 
			<% if (!strOpt.equals("BYCDISTSALES") && !strOpt.equals("BYCSETSALES") ) {%>					
				    <display:column property="LOG" title="" media="html" style="text-align: Right;" sortable="true"/>	
			<% }  %>
					<fmtSalesVirtualActual:message key="DT_SET_ID" var="varSetId"/>				    
				    <display:column property="SID" title="${varSetId}" style="text-align: left;"  sortable="true" />
				    <fmtSalesVirtualActual:message key="DT_SET_NM" var="varSetNm"/>	
				    <display:column property="SNAME" title="${varSetNm}" style="text-align: left;"  sortable="true" />
			<%	    if(strOpt.equals("BYCDISTSALES")||strOpt.equals("BYCDIST"))
					{
			%>
					<fmtSalesVirtualActual:message key="DT_SYS_NM" var="varSysNm"/>
					<display:column property="SYSTEMNAME" title="${varSysNm}" style="text-align: left;"  sortable="true" />
					<fmtSalesVirtualActual:message key="DT_HIERARC" var="varHircy"/>
					<display:column property="HIERARCHY" title="&nbsp;${varHircy}&nbsp;&nbsp;" style="text-align: left;"  sortable="true" />
					<fmtSalesVirtualActual:message key="DT_CATEG" var="varCateg"/>
					<display:column property="CATEG" title="${varCateg}"     style="text-align: Left;"  sortable="true" />
					<fmtSalesVirtualActual:message key="DT_TYP" var="varType"/>
					<display:column property="TYPE" title="${varType}"   style="text-align: Left;" sortable="true" />
			<% }  %>
			<% if (!strOpt.equals("BYCDIST") && !strOpt.equals("BYCSET") && !strOpt.equals("BYCDISTSALES") && !strOpt.equals("BYCSETSALES")) {%>				   
					<display:column property="DIFFFLAG" title="" style="text-align: center;"  sortable="true"/>
			<% } %>											
				    <display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
					<display:column property="VCOUNT" title="<%=strTitleForLogical%>"   total="true" style="text-align: center;" />
					<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>	
			<% if (!strOpt.equals("BYCDIST")&& !strOpt.equals("BYCSET") && !strOpt.equals("BYCDISTSALES") && !strOpt.equals("BYCSETSALES")) {%>					
					<display:column property="FCOUNT" title="<%=strColumnName%>"   total="true" style="text-align: center;" />
				    <display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				    <fmtSalesVirtualActual:message key="DT_CONSIGN" var="varCons"/>				    
					<display:column property="COUNT" title="${varCons}"   total="true" style="text-align: center;"  sortable="true"/>
					<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
					 <fmtSalesVirtualActual:message key="DT_RETURN" var="varRtns"/>						    	
					<display:column property="RCOUNT" title="${varRtns}"   total="true"  style="text-align: center;"  sortable="true"/>					
			<% } %>				
				<display:footer media="html"> 
						<tr><td class="Line" colspan="12"></td></tr>
						<%						
							if (!strOpt.equals("BYCDIST") && !strOpt.equals("BYCSET") && !strOpt.equals("BYCDISTSALES") && !strOpt.equals("BYCSETSALES")) {
								strVCount = ((HashMap)pageContext.getAttribute("totals")).get("column6").toString();
								strFCount = ((HashMap)pageContext.getAttribute("totals")).get("column8").toString();
								strCCount = ((HashMap)pageContext.getAttribute("totals")).get("column10").toString();
								strRCount = ((HashMap)pageContext.getAttribute("totals")).get("column12").toString();
							}
							else {	
							if(strOpt.equals("BYCDIST"))
								{		
								strTotalVCountColSpan = "7";	
								colNumber="column9";
								}
							else if(strOpt.equals("BYCDISTSALES"))
							{		
							strTotalVCountColSpan = "6";	
							colNumber="column8";
							}
							else if( strOpt.equals("BYCSETSALES"))
							 {
								strTotalVCountColSpan = "2";		
								colNumber="column4";
							 }
							 else
							 {
							 	strTotalVCountColSpan = "3";						
							 	colNumber="column5";
							 }	
								if(((HashMap)pageContext.getAttribute("totals")) != null && ((HashMap)pageContext.getAttribute("totals")).get(colNumber) != null)
								{
									strVCount = ((HashMap)pageContext.getAttribute("totals")).get(colNumber).toString();
								}	
							}
							
						%>
						
					<tr>
				
					    <td height="20"  colspan=<%=strTotalVCountColSpan%>> </td>						
					    <td class="Line" width="1" > </td>
						<td  style="text-align: center;" ><B><%= GmCommonClass.getStringWithCommas(strVCount,0) %></B></td>
						<td class="Line" width="1" > </td>
											    
				<% 
					if (!strOpt.equals("BYCDIST") && !strOpt.equals("BYCSET") && !strOpt.equals("BYCDISTSALES") && !strOpt.equals("BYCSETSALES")) {
				%>											
						<td  style="text-align: center;"><B><%= GmCommonClass.getStringWithCommas(strFCount,0) %></B></td>
						<td class="Line"  width="1"> </td>
						<td  style="text-align: center;"><B><%= GmCommonClass.getStringWithCommas(strCCount,0) %></B></td>
						<td class="Line"  width="1"> </td>
						<td  style="text-align: center;"><B><%= GmCommonClass.getStringWithCommas(strRCount,0) %></B></td>
				<% } %>																	
					</tr>
					</display:footer>				
				 </display:table> 					
			</td></tr>
		<tr><td colspan="2" class="Line" height="1"></td></tr>
		<script> fnstripedbyid('myHighlightTable','lightBrown') </script>
	</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
