 <%
/**********************************************************************************
 * File			 	: GmSalesVirtualSummary.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Richardk
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*,java.text.DecimalFormat" %>

<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page buffer="16kb" autoFlush="true" %>
 <!-- \sales\AreaSet\GmAddInfo.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%
	response.setHeader("Cache-Control", "no-cache, post-check=0, pre-check=0"); //HTTP 1.1
	response.setHeader("Pragma", "no-cache");  //HTTP 1.0
	response.setDateHeader ("Expires", 0); //prevents caching at the proxy server 
%>


<%@ page import="com.globus.common.beans.GmCommonClass"%> 
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<%@ taglib prefix="fmtSalesVirtualDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!--sales\GmSalesVirtualDetails.jsp  -->
<fmtSalesVirtualDetails:setLocale value="<%=strLocale%>"/>
<fmtSalesVirtualDetails:setBundle basename="properties.labels.sales.GmSalesVirtualDetails"/>
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	int intSize = 0;
	int intLength = 0;


	String strCodeID = "";
	String strSelected = "";
	String strVendId = "";
	String strCurDate = "";
	
	String strCount ; 
	String strVal ; 
	String strGrowth ;
	String strPVal ;
	String strPercent ;
	
	HashMap hmLoadReturn = new HashMap();
	HashMap hcboVal = new HashMap();
  
  	String strOpt = (String)request.getAttribute("hOpt")==null?"":(String)request.getAttribute("hOpt");
  	String strHeader = (String)request.getAttribute("hHeader")==null?"":(String)request.getAttribute("hHeader");
	double dbTemp = 0;
  	DecimalFormat df = new DecimalFormat("#%");
  	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Sales Growth Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">


<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     textarea {
	width: 60%;
	height: 60px;
	}
	
	table.DtTable1000 {
	width: 1100px;
	}
</style>

<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>

<script>

function fnViewPartActual(val)
{
	document.frmMain.hPartID.value = val;
	document.frmMain.Cbo_DistId.value = '<%=request.getParameter("hDistributorID")%>';
	document.frmMain.hOpt.value = 'BYPARTDETAIL';
	
	document.frmMain.action ="<%=strServletPath%>/GmSalesConsignSearchServlet";
	document.frmMain.hAction.value = "Reload";
	document.frmMain.submit();	
}



function fnGo()
{
	fnSetSelections();
	document.frmMain.submit();
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmMain" method="POST" action="<%=strServletPath%>/GmSalesGrowthServlet">
	<table class="DtTable850" cellspacing="0" cellpadding="0">
		<input type="hidden" name="hAction" value="">
		<input type="hidden" name="hId" value="">
		<input type="hidden" name="strSessOpt" value="<%=strOpt%>">

		<!-- The following hidden fields are for Sales Drilldown (Additional Drill down)-->
		<input type="hidden" name="hDistributorID" value="<%=request.getParameter("hDistributorID")%>">
		<input type="hidden" name="hAccountID" value="<%=request.getParameter("hAccountID")%>">
		<input type="hidden" name="hSetNumber" value="<%=request.getParameter("hSetNumber")%>">
		<input type="hidden" name="hRepID" value="<%=request.getParameter("hRepID")%>">
		<input type="hidden" name="hTerritory" value="<%=request.getParameter("hTerritory")%>">
		<input type="hidden" name="hPartNumber" value="">
		<input type="hidden" name="hPartID" value="">
		<input type="hidden" name="hAreaDirector" value="<%=request.getParameter("hAreaDirector")%>">
		<input type="hidden" name="Cbo_DistId" value="<%=request.getParameter("hDistributorID")%>">
		<input type="hidden" name="hOpt" value="">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtSalesVirtualDetails:message key="LBL_CONSIGMENT"/> <%=strHeader%></td>
		</tr>
		<tr><td class="Line"></td></tr>
		
	</FORM>
	<FORM name ="frmDtag"> 
		<tr><td class="Line"></td></tr>
		<tr><td>
			<table border="0" width="100%"  export="false" class="its" varTotals="totals" 
					cellpadding="0" cellspacing="0" >
			<tr><td valign="top" class="aligncenter" >			
			<display:table name="requestScope.results.rows" export="false"  class="its" varTotals="totals" 
					cellpadding="0" cellspacing="0" decorator="com.globus.displaytag.beans.DTSalesDetailListWrapper">
				<fmtSalesVirtualDetails:message key="DT_PART_NUM" var="varPartNum"/><display:column property="PNUM" title="${varPartNum}"  sortable="true" style="text-align: center;"
					class="ShadeWhite" headerClass="ShadeLightGrayTD" />
				<fmtSalesVirtualDetails:message key="DT_PART_DESC" var="varPartDesc"/><display:column property="PDESC" title="${varPartDesc}" style="text-align: left;"
					class="ShadeWhite" headerClass="ShadeLightGrayTD" />
				<fmtSalesVirtualDetails:message key="DT_PROD_FAMILY" var="varProdFamily"/><display:column property="PRODFAMILY" title="${varProdFamily}" style="text-align: left;"
					class="ShadeWhite" headerClass="ShadeLightGrayTD" />
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>				
				<fmtSalesVirtualDetails:message key="DT_QTY" var="varQty"/><display:column property="QTY" title="${varQty}" sortable="true" total="true" 
					style="text-align: center;" class="ShadeMedGreenTD"  headerClass="ShadeDarkGrayTD"/>
				<fmtSalesVirtualDetails:message key="DT_PART_VALUE" var="varPartValue"/><display:column property="PRICE" title="${varPartValue}" sortable="true" format="{0,number,$#,###,###.00}"
				style="text-align: right;" class="ShadeMedGreenTD"  headerClass="ShadeDarkGrayTD"/>
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				<fmtSalesVirtualDetails:message key="DT_TOT_VALUE" var="varTotValue"/><display:column property="TOTAL" title="${varTotValue}" sortable="true" format="{0,number,$#,###,###.00}"
				style="text-align: right;" class="ShadeMedGreenTD"  total="true" headerClass="ShadeDarkGrayTD"/>
				
				<display:footer media="html"> 
					<tr><td class="Line" colspan="8"></td></tr>
					<tr>
					<td class = "ShadeDarkGrayTD" colspan = "3"> <B> <fmtSalesVirtualDetails:message key="LBL_TOTAL"/> </B></td>
					<td class="Line" ></td>
					<%
						strCount 	= ((HashMap)pageContext.getAttribute("totals")).get("column5").toString();
						strVal 	= ((HashMap)pageContext.getAttribute("totals")).get("column8").toString();
					%>
					<td class = "ShadeDarkGreenTD" style="text-align: center;" ><B><%= GmCommonClass.getStringWithCommas(strCount,0) %></B></td>
					<td class = "ShadeDarkGreenTD" > 
					<td class="Line" ></td>
					<td class = "ShadeDarkGreenTD" style="text-align: right;" ><B><%= GmCommonClass.getStringWithCommas(strVal, 2) %></B></td>
				</display:footer>				
				</display:table>			
		</td></tr>
	</FORM>	
		</table></td></tr>
	</table>
<%
	}catch(Exception e)
		{
			e.printStackTrace();
		}
%>

</BODY>
</HTML>
