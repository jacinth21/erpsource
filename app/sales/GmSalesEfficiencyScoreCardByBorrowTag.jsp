 <!-- sales\GmSalesEfficiencyScoreCardByBorrowTag.jsp -->
<%
	/**********************************************************************************
	  * File		 		: GmSalesEfficiencyScoreCardByBorrowTag.jsp.jsp
      * Desc		 		: This screen is used to Scorecard - Borrow Tag Information 
      * author			    : MKosalram
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java"%>
<%@page import="java.util.Date"%> 
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.net.URLEncoder"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%> 
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.net.URLEncoder" %>
<%@ taglib prefix="fmtActualProceduresReportScoreCard" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtActualProceduresReportScoreCard:setLocale value="<%=strLocale%>"/>
<fmtActualProceduresReportScoreCard:setBundle basename="properties.labels.sales.GmActualProceduresReportScoreCard"/> 
 
<bean:define id="screenName" name="frmActualProceduresReportScoreCard" property="screenName" type="java.lang.String"></bean:define>
<%

 String strsalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");

 String strWikiTitle = "";

%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Scorecard - Borrow Tag Information</TITLE>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<!-- Method allows user to add new row from clipboard. New row is being added to the last position in the grid. -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<!-- paste content of clipboard into block selection of grid -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script language="JavaScript" src="<%=strsalesJsPath%>/GmSalesEfficiencyScoreCardByBorrowTag.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/jquery-latest.js"></script>

<style type="text/css" media="all">
@import url("styles/screen.css"); 
</style>
</HEAD>
<body leftmargin="15" topmargin="10">
<body leftmargin="15" topmargin="10" onLoad="fnBorrowTagOnPageLoad(); ">
<html:form> 
<html:hidden property="systemId" />
<html:hidden property="distributorId" />
<html:hidden property="adId" />
<!-- PC-5101: Scorecard DO classification changes -->
<html:hidden property="allowedTag" />
 <table class="DtTable1055" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="9" height="25" class="RightDashBoardHeader">&nbsp; <%=screenName%> </td>
				<td height="25" align="right" class=RightDashBoardHeader><fmtActualProceduresReportScoreCard:message key="IMG_ALT_HELP" var="varHelp"/>				    
				 <img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
             </tr>
             
		   <tr class="oddshade"> 
				<td colspan="10"><div id="dataGridDiv" height="400px"></div></td>
		  </tr>
		  
		  <tr id="DivExportExcel" height="25">
			<td colspan="6" align="center">
			    <div class='exportlinks'><fmtActualProceduresReportScoreCard:message key="EXPORT_OPTS"/> :<img
					src='img/ico_file_excel.png' onclick="fnExportDtls();" />&nbsp;<a href="#"
					onclick="fnExportDtls();"><fmtActualProceduresReportScoreCard:message key="EXCEL"/> 
				</a></div>
			</td>
		 </tr>
		 
		 <tr><td colspan="4" align="center"  class="RegularText" height="20"><div  id="DivNothingMessage"><fmtActualProceduresReportScoreCard:message key="NO_DATA_FOUND"/></div></td></tr>
       </table> 
 </html:form> 
  <%@ include file="/common/GmFooter.inc" %> 
</BODY>
</HTML>