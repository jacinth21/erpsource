<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- sales\GmSalesFilterDetails. -->
<%
	/*********************************
	 * Filter details 
	 *********************************/ 
	HashMap hmSalesFiltersDetails = new HashMap();
	String strName = "";
	GmServlet gm = new GmServlet();
	
	hmSalesFiltersDetails = (HashMap)request.getAttribute("hFilterDetails");
	System.out.println("hmSalesFiltersDetails"+hmSalesFiltersDetails + " User id " + (String)session.getAttribute("strSessUserId"));
	StringBuffer sbValue = new StringBuffer();
	if( hmSalesFiltersDetails.size() != 0 ) 
	{
%>	
<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tr height="16" bgcolor="beige"> <td class=RightTextSmall> 
		<% 
			strName = GmCommonClass.parseNull((String)hmSalesFiltersDetails.get("VPNAME"));
			if (!strName.equals(""))
			{
			sbValue.append("(V)- "); 
			sbValue.append("<FONT Color='blue'> <u>"); 
			sbValue.append(strName);
			sbValue.append("</u></FONT>");
			}
			
			strName =  GmCommonClass.parseNull((String)hmSalesFiltersDetails.get("TERRITORY"));
			if (!strName.equals(""))
			{
				sbValue.append("(T)- "); 
				sbValue.append("<FONT Color='blue'> <u>"); 
				sbValue.append(strName);
				sbValue.append("</u></FONT>");
			}

			strName =  GmCommonClass.parseNull((String)hmSalesFiltersDetails.get("ADNAME"));
			if (!strName.equals(""))
			{
				if (sbValue.length()!=0) 
				{
					sbValue.append(", " );
				}
				sbValue.append("(AD)- "); 
				sbValue.append("<FONT Color='blue'> <u>"); 
				sbValue.append(strName);
				sbValue.append("</u></FONT>");
			}
			
			strName =  GmCommonClass.parseNull((String)hmSalesFiltersDetails.get("FIELDSALESNAME"));
			if (!strName.equals(""))
			{
				if (sbValue.length()!=0) 
				{
					sbValue.append(", " );
				}
				sbValue.append("(D)- "); 
				sbValue.append("<FONT Color='blue'> <u>"); 
				sbValue.append(strName);
				sbValue.append("</u></FONT>");
			}

			
			strName =  GmCommonClass.parseNull((String)hmSalesFiltersDetails.get("SET"));
			if (!strName.equals(""))
			{
				if (sbValue.length()!=0) 
				{
					sbValue.append(", " );
				}
				sbValue.append("(S)- "); 
				sbValue.append("<FONT Color='blue'> <u>"); 
				sbValue.append(strName);
				sbValue.append("</u></FONT>");
			}
				
			strName =  GmCommonClass.parseNull((String)hmSalesFiltersDetails.get("ACCOUNT"));
			if (!strName.equals(""))
			{
				if (sbValue.length()!=0)   
				{
					sbValue.append(", " );
				}
				sbValue.append("(A)- "); 
				sbValue.append("<FONT Color='blue'> <u>"); 
				sbValue.append(strName);
				sbValue.append("</u></FONT>");
			}
			strName =  GmCommonClass.parseNull((String)hmSalesFiltersDetails.get("REP"));
			if (!strName.equals(""))
			{
				if (sbValue.length()!=0)   
				{
					sbValue.append(", " );
				}
				sbValue.append("(R)- "); 
				sbValue.append("<FONT Color='blue'> <u>"); 
				sbValue.append(strName);
				sbValue.append("</u></FONT>");
			}

			strName =  GmCommonClass.parseNull((String)hmSalesFiltersDetails.get("PART"));
			if (!strName.equals(""))
			{
				if (sbValue.length()!=0)   
				{
					sbValue.append(", " );
				}
				sbValue.append("(P)- "); 
				sbValue.append("<FONT Color='blue'> <u>"); 
				sbValue.append(strName);
				sbValue.append("</u></FONT>");
			}
			// MNTTASK-6864:While click the S icon in Pricing Request screen will display group pricing data
			// and click Go button that time is should not display account name with group pricing name. So we have to check condition.
			strName =  GmCommonClass.parseNull((String)hmSalesFiltersDetails.get("ACCOUNT"));
			if(strName.equals("")){
				strName =  GmCommonClass.parseNull((String)hmSalesFiltersDetails.get("GRPNAME"));
				if (!strName.equals(""))
				{
					if (sbValue.length()!=0)   
					{
						sbValue.append(", " );
					}
					sbValue.append("(G)- "); 
					sbValue.append("<FONT Color='blue'> <u>"); 
					sbValue.append(strName);
					sbValue.append("</u></FONT>");
				}
			}
			out.println(sbValue.toString());
			
		%>
</td></tr>
</table>

<%	} %>