 <%
/**********************************************************************************
 * File		 		: GmSalesConsignSearch.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
 <%@page import="java.util.Date"%>
<%@page import="com.globus.valueobject.common.GmDataStoreVO"%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="java.util.Date"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<!-- sales\GmSalesConsignSearch.jsp -->
<%@ taglib prefix="fmtSaleConsSearch" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtSaleConsSearch:setLocale value="<%=strLocale%>"/>
<fmtSaleConsSearch:setBundle basename="properties.labels.sales.GmSalesConsignSearch"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	

	String strTimeZone = strGCompTimeZone;
	String strApplDateFmt = strGCompDateFmt;
	String strDTDateFmt = "{0,date,"+strApplDateFmt+"}";
	String strAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strOpt = (String)request.getAttribute("hOpt")==null?"":(String)request.getAttribute("hOpt");
	HashMap hmParam = (HashMap)request.getAttribute("hmParam");
	
	String strhSetIds = "";
	String strDist = "";
	String strFrmDt = "";
	String strToDt = "";
	String strType = "";
	String strCategory = "";
	String strStatus = "";
	String strPartNum = "";
	String strTransId = "";
	Date dtFromDate= null;
	Date dtToDate = null;

	if (hmParam != null) 
	{
		strhSetIds = GmCommonClass.parseNull((String)hmParam.get("SETID"));
		strDist = GmCommonClass.parseNull((String)hmParam.get("DISTID"));
		strFrmDt = GmCommonClass.parseNull((String)hmParam.get("FRMDT"));	
		strToDt = GmCommonClass.parseNull((String)hmParam.get("TODT"));
		strType = GmCommonClass.parseNull((String)hmParam.get("TYPE"));
		strCategory = GmCommonClass.parseNull((String)hmParam.get("CATEGORY"));
		strStatus = GmCommonClass.parseNull((String)hmParam.get("STATUS"));
		strPartNum = GmCommonClass.parseNull((String)hmParam.get("PNUM"));
		strTransId = GmCommonClass.parseNull((String)hmParam.get("TRANSID"));
		
		dtFromDate = GmCommonClass.getStringToDate(strFrmDt,strApplDateFmt);
		dtToDate = GmCommonClass.getStringToDate(strToDt,strApplDateFmt);
	}	

	ArrayList alDistributor = new ArrayList();
	ArrayList alSets = new ArrayList();
	ArrayList alSearchTypes = new ArrayList();
	ArrayList alSearchKinds = new ArrayList();
	ArrayList alSearchStatus = new ArrayList();
	
	if (hmReturn != null) 
	{
		alSets = (ArrayList)hmReturn.get("SETMASTERLIST");
		alDistributor = (ArrayList)hmReturn.get("DISTRIBUTORLIST");
		alSearchTypes = (ArrayList)hmReturn.get("SEARCHTYPES");
		alSearchKinds = (ArrayList)hmReturn.get("SEARCHKIND");
		alSearchStatus = (ArrayList)hmReturn.get("SEARCHSTATUS");
	}
	
	HashMap hmMapSets = new HashMap();
	hmMapSets.put("ID","");
	hmMapSets.put("PID","ID");
	hmMapSets.put("NM","NAME");	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Report- Sales Consignments Search</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     textarea {
	width: 60%;
	height: 60px;
	}
	
	table.DtTable1000 {
	width: 1100px;
	}
	
	a.red {
	FONT-SIZE: 11px; 
	COLOR: red; 
	FONT-FAMILY: verdana, arial, sans-serif;
	}
</style>

<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
function fnReload()
{
	var objSet = document.frmAccount.Chk_GrpSet;
	var setlen = objSet.length;
	var setstr = '';
	
	var temp = '';
		
	for (var i=0;i<setlen;i++)
	{
		if (objSet[i].checked == true)
		{
			setstr = setstr + objSet[i].value + ',';
		}
		document.frmAccount.hSetIds.value = setstr.substr(0,setstr.length-1);
	}

	var objSet = document.frmAccount.Chk_GrpDist;
	var setlen = objSet.length;
	var setstr = '';
	
	var temp = '';
		
	for (var i=0;i<setlen;i++)
	{
		if (objSet[i].checked == true)
		{
			setstr = setstr + objSet[i].id + ',';
		}
		document.frmAccount.hDistIds.value = setstr.substr(0,setstr.length-1);
	}
		
	document.frmAccount.hAction.value = 'Reload';
	fnStartProgress();
	document.frmAccount.submit();
}


function fnCallDiv(val,flag,etch)
{
	document.frmAccount.hConsignId.value = val;
	document.frmAccount.hFlagId.value = flag;
	document.frmAccount.hEtchId.value = etch;
}

function fnOpenTrack(val)
{
	windowOpener('https://www.fedex.com/fedextrack?ascend_header=1&clienttype=dotcom&cntry_code=us&language=english&tracknumbers='+val,"Track","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
}
function fnPrintConsignVer(val)
{
	windowOpener("/GmConsignSetServlet?hAction=LoadConsign&hId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintReturnVer(val)
{
	fnPrintRA(val);
}

function fnLoad()
{
	var setids = document.frmAccount.hSetIds.value;
	var objgrp = '';
	var distids = document.frmAccount.hDistIds.value;
	
	if (setids != '')
	{
		objgrp = document.frmAccount.Chk_GrpSet;
		fnCheckSelections(setids,objgrp);
	}

	if (distids != '')
	{
		objgrp = document.frmAccount.Chk_GrpDist;
		fnCheckSelections(distids,objgrp);
	}	
	
}

// Marks the checkboxes as 'checked'
function fnCheckSelections(val,objgrp)
{
	var valobj = val.split(",");
	var sval = '';
	var arrlen = objgrp.length;
	
	if (arrlen > 0)
	{
		for (var j=0;j< valobj.length;j++ )
		{
			sval = valobj[j];
			for (var i=0;i< arrlen;i++ )
			{
				if (objgrp[i].value == sval || objgrp[i].id == sval)
				{
					objgrp[i].checked = true;
					break;
				}
			}
		}
	}
	else
	{
		objgrp.checked = true;
	}
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="javascript:fnLoad();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmConsignSearchServlet">
<input type="hidden" name="hOpt" value="<%=strOpt%>">
<input type="hidden" name="hSetIds" value="<%=strhSetIds%>">
<input type="hidden" name="hDistIds" value="<%=strDist%>">
<input type="hidden" name="hAction" value="<%=strAction%>">

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtSaleConsSearch:message key="TD_SALE_CONS_SET_HEADER"/></span>			
			</td>
		</tr>
		<tr><td class="Line" height="1"></td></tr>
		<tr>
			<td height="35" class="RightText">
				<table class="Border" cellspacing="0" width="100%">
					<tr height="18" bgcolor="#eeeeee" class="RightTableCaption">
						<td width="250">&nbsp;<fmtSaleConsSearch:message key="LBL_DIST"/></td>
						<td>&nbsp;<fmtSaleConsSearch:message key="LBL_SET"/></td>
						<td colspan="2"></td>
					</tr>
					<tr>
						<td valign="top"><%=GmCommonControls.getChkBoxGroup("",alDistributor,"Dist")%></td>
						<td><%=GmCommonControls.getChkBoxGroup("",alSets,"Set",hmMapSets,"IDNM")%></td>
						<td>
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr height="26">
									<td class="RightText" HEIGHT="24" align="right"><fmtSaleConsSearch:message key="LBL_CATEGORY"/>: </td>
									<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Category"  seletedValue="<%=strCategory%>" 	
												tabIndex="1"   value="<%= alSearchKinds%>" codeId = "CODEID"  codeName = "CODENM"  />
									</td>
									<td rowspan="3" width="100" valign="middle" align="center">
										<fmtSaleConsSearch:message key="BTN_LOAD" var="varLoad"/>
										<fmtSaleConsSearch:message key="BTN_RESET" var="varReset"/>
										<gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" buttonType="Load" onClick="fnReload();" tabindex="25" />&nbsp;
										<BR><BR><gmjsp:button value="&nbsp;${varReset}&nbsp;" gmClass="button" buttonType="Load" onClick="document.frmAccount.reset();" tabindex="25" />&nbsp;
									</td>
								</tr>
								<tr height="26">
									<td align="right"><fmtSaleConsSearch:message key="LBL_TYPE"/>: </td>
									<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Type"  seletedValue="<%=strType%>" 	
												tabIndex="1"   value="<%= alSearchTypes%>" codeId = "CODEID"  codeName = "CODENM" />
									</td>
								</tr>
								<tr height="26">
									<td align="right"><fmtSaleConsSearch:message key="LBL_STATUS"/>:</td>
									<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Status"  seletedValue="<%=strStatus%>" 	
												tabIndex="1"   value="<%= alSearchStatus%>" codeId = "CODEID"  codeName = "CODENM" />
									</td>
								</tr>
								<tr>
									<td><fmtSaleConsSearch:message key="LBL_INC_INHOUSE"/>:</td>
									<td><input type="checkbox" name="Chk_InHouse"></td>
								</tr>
							</table>
						</td>
						<td width="50"></td>
					</tr>
					<tr><td class="Line" colspan="5" height="1"></td></tr>
					<tr height="28">
						<td colspan="3">&nbsp;<fmtSaleConsSearch:message key="LBL_PART_NUM"/>:&nbsp;<input type="text" size="30" value="<%=strPartNum%>" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;&nbsp;
						&nbsp;<fmtSaleConsSearch:message key="LBL_TRANS_ID"/>:&nbsp;<input type="text" size="15" value="<%=strTransId%>" name="Txt_TransId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;&nbsp;
						</td>
					</tr>
					<tr>
						<td colspan="3" height="28">&nbsp;&nbsp;&nbsp;&nbsp;<fmtSaleConsSearch:message key="LBL_FROM_DATE"/>:&nbsp;<gmjsp:calendar textControlName="Txt_FrmDt" textValue="<%=(dtFromDate==null)?null:new java.sql.Date(dtFromDate.getTime())%>" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>	
						&nbsp;&nbsp;&nbsp;<fmtSaleConsSearch:message key="LBL_TO_DATE"/>:&nbsp;<gmjsp:calendar textControlName="Txt_ToDt" textValue="<%=(dtToDate==null)?null:new java.sql.Date(dtToDate.getTime())%>" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>						
						</td>
					</tr>
					
				</table>			
			</td>
		</tr>
		<tr><td class="Line" height="1"></td></tr>
<%
		if (strAction.equals("Reload"))
		{		
%>
		<tr>
			<td>
				 <display:table name="requestScope.results.rows" export="false"  varTotals="totals" freezeHeader="true" paneSize="450"
					cellpadding="0" cellspacing="0" decorator="com.globus.displaytag.beans.DTSalesVirtualDetailWrapper">
					<fmtSaleConsSearch:message key="DT_CONS_ID" var="varConsId"/> 
				    <display:column property="ID" title="${varConsId}" sortable="true"  
				    style="text-align: left;" class="ShadeWhite" headerClass="ShadeLightGrayTD" />
				    <fmtSaleConsSearch:message key="DT_SET_ID" var="varSetId"/>
				    <display:column property="SETID" title="${varSetId}" sortable="true"  
				    style="text-align: center;" class="ShadeWhite" headerClass="ShadeLightGrayTD" />
				     <fmtSaleConsSearch:message key="DT_DIST_ID" var="varDistId"/>
				   <display:column property="DISTID" title="${varDistId}" sortable="true"
					style="text-align: left;" class="ShadeWhite" headerClass="ShadeLightGrayTD" />
				    <display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				    <fmtSaleConsSearch:message key="DT_SHIP_CREDIT_DATE" var="varShipDate"/>
				    <display:column property="DDATE" title="${varShipDate}" sortable="true" 
				    style="text-align: center;" class="ShadeMedGreenTD"  headerClass="ShadeDarkGrayTD" format="<%=strDTDateFmt%>"/> 
				    <fmtSaleConsSearch:message key="DT_TRACK_ID" var="varTrackId"/>
					<display:column property="TRACK" title="${varTrackId}" sortable="true"  
					style="text-align: center;" class="ShadeMedGreenTD"  headerClass="ShadeDarkGrayTD"/>
				 </display:table> 
			</td>
		</tr>
<%
		}
%>
	</table>


</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
