<%
/**********************************************************************************
 * File		 		: GmPendAllocRpt.jsp
 * Desc		 		: For Pending allocation report.
 * Version	 		: 1.0
 * author			: Rshah
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %> 

<%@ taglib prefix="fmtPendAllocRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\InvAllocation\GmPendAllocRpt.jsp -->

<fmtPendAllocRpt:setLocale value="<%=strLocale%>"/>
<fmtPendAllocRpt:setBundle basename="properties.labels.sales.InvAllocation.GmPendAllocRpt"/> 
<%
java.sql.Date dtSurDate = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Pending Allocation </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"  type="text/javascript"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js" type="text/javascript"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js" type="text/javascript"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script> 

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>

var dhxWins,w1;

function loadPopup() {
	  
    dhxWins = new dhtmlXWindows();
    dhxWins.enableAutoViewport(true);
    dhxWins.attachViewportTo("winVP");
 
    w1 = dhxWins.createWindow("w1", 30, 40, 900, 400);

    dhxWins.attachEvent("onClose", function(win){
    	var w11 = dhxWins.window("w1");
    	//alert('test');
    	return true;
    });
 	   
    w1.setText(message_sales[189]); 
	w1.attachURL("/sales/InvAllocation/GmSetOverrideRpt.jsp");
	}

	var myCalendar;
	function calendarLoad(vdate) {
		myCalendar = new dhtmlXCalendarObject([ "surgDate", "loanDate" ]);
		myCalendar.setDateFormat("%m/%d/%Y");
	}
	dhtmlxEvent(window, "load", calendarLoad);
</script>  
  
</HEAD>
 
<BODY leftmargin="20" topmargin="10" >
<FORM name="frmPendAllocRpt" method="POST" action=""> 

<!-- Custom tag lib code modified for JBOSS migration changes --> 
	<table class="DtTable800" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td >
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr >
						<td height="25" colspan="2" class="RightDashBoardHeader">&nbsp;<fmtPendAllocRpt:message key="LBL_PENDING_ALLOCATION"/></td>						 
					</tr>
					<tr><td class="Line" height="1" colspan="2"></td></tr>						
                    <tr>
                    	<td width="900" height="100" valign="top" colspan="2" >
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
				            	<tr class="evenshade">
				                	<fmtPendAllocRpt:message key="LBL_FIELD_SALES" var="varFieldSal"/>
				                	<gmjsp:label type="BoldText"  SFLblControlName="${varFieldSal}" td="true"/>            
									<td>&nbsp;<select name="fieldSales" id="" class="RightText">
											 <option value=0 id=0 ><fmtPendAllocRpt:message key="LBL_CHOOSE_ONE"/></option><option  value= 222 id=null selected="selected">Metro Med</option> 	
											 <option  value= 303  id=null><fmtPendAllocRpt:message key="LBL_SKY_SURGICAL"/></option>
											 <option  value= 6  id=null><fmtPendAllocRpt:message key="LBL_ROCK_SURGICAL_ASSOCIATES"/></option>										 
										</select>&nbsp;				    		            				
					                </td>
					                <fmtPendAllocRpt:message key="LBL_SALES_REP" var="varSalesRep"/>
					                <gmjsp:label type="BoldText"  SFLblControlName="${varSalesRep}" td="true"/>
									<td align ="left">&nbsp;<select name="salesRep" class="RightText">
											<option value="4011"><fmtPendAllocRpt:message key="LBL_CHOOSE_ONE"/></option>
											<option value="836"><fmtPendAllocRpt:message key="LBL_GRANT_NOEL"/></option>
											<option value="836"><fmtPendAllocRpt:message key="LBL_MICHAEL_MELCHIONNI"/></option>										
										</select>
									</td>			                    
			                    </tr>
				                <tr><td class="LLine" height="1" colspan="4" ></td></tr>
				                <tr class="oddshade">
				                	<fmtPendAllocRpt:message key="LBL_CASE_ID" var="varCaseId"/>
				                	<gmjsp:label type="BoldText"  SFLblControlName="${varCaseId}" td="true"/>
				                	<td>&nbsp;<input type="text" size="20"  id="caseid" name="caseid" class="InputArea" onFocus="changeBgColor(this,'#AACCE8'); " onBlur="changeBgColor(this,'#ffffff'); " value="%case%">&nbsp;
				                	<fmtPendAllocRpt:message key="LBL_SURGERY_DATE" var="varSurgeryDt"/>
				                	<gmjsp:label type="BoldText"  SFLblControlName="${varSurgeryDt}" td="true"/>
				                	<td height="25" class="RightText" align="left">
				                		&nbsp;<input type="text" id="surgDate" size="10">
				                	</td>
				                </tr>
				                <tr><td class="LLine" height="1" colspan="4" ></td></tr>
				                <tr class="evenshade">
				                	<fmtPendAllocRpt:message key="LBL_LOAN_DATE" var="varLoanDate"/>
				                	<gmjsp:label type="BoldText"  SFLblControlName="${varLoanDate}" td="true"/>
				                	<td class="RightTableButton" class="RightText" align="left">
				                		&nbsp;<input type="text" id="loanDate" size="10">
				                	</td>
				                	<td height="25" align="left"></td>
				                	<td class="RightTableButton" align="left">
				                		&nbsp;<fmtPendAllocRpt:message key="BTN_LOAD" var="varLoad"/>
				                		<gmjsp:button style="width: 8em" value="${varLoad}" />&nbsp;
				                	</td>
				                </tr>
				                <tr>
									<td colspan="4">
										<table border="0" style="margin-bottom: 0px;margin-left: 0px;margin-right: 0px;margin-top: 0px;border: 0px solid  #676767" width="100%" cellspacing="0" cellpadding="0"  >
											<tr><td class="Line" height="1" colspan="7"></td></tr>
											<tr  class="ShadeRightTableCaption">
												<td class="RightTableCaption" align="right" HEIGHT="24"   ><fmtPendAllocRpt:message key="LBL_SET_ID"/>&nbsp;</td>
												<td class="RightTableCaption" align="left" HEIGHT="24"   ><fmtPendAllocRpt:message key="LBL_SET_NAME"/>	</td>
												<td class="RightTableCaption" align="left" HEIGHT="24"   ><fmtPendAllocRpt:message key="LBL_SET_TYPE"/></td>
												<td class="RightTableCaption" align="left" HEIGHT="24"   ><fmtPendAllocRpt:message key="LBL_FIELD_SALES"/></td>
												<td class="RightTableCaption" align="left" HEIGHT="24"   ><fmtPendAllocRpt:message key="LBL_SALES_REP"/></td>
												<td class="RightTableCaption" align="left" HEIGHT="24"   ><fmtPendAllocRpt:message key="LBL_LOCK_FROM"/></td>
												<td class="RightTableCaption" align="left" HEIGHT="24"   ><fmtPendAllocRpt:message key="LBL_LOCK_TO"/></td>
											</tr>
											<tr><td class="Line" height="1" colspan="7"></td></tr>
											<tr class="evenshade">
												<td   align="right" HEIGHT="24"   >&nbsp;<input type=radio value="961.901" name="setid" checked="checked"><fmtPendAllocRpt:message key="LBL_961.901"/></input>&nbsp;</td>
												<td   align="left" HEIGHT="24"   ><fmtPendAllocRpt:message key="LBL_XTEND_INSTRUMENT_SET"/></td> 
												<td   align="left" HEIGHT="24"   ><fmtPendAllocRpt:message key="LBL_STANDARD"/></td>
												<td   align="left" HEIGHT="24"    ><fmtPendAllocRpt:message key="LBL_METRO_MED"/></td>
												<td   align="left" HEIGHT="24"   ><fmtPendAllocRpt:message key="LBL_GRANT_NOEL"/></td>												
												<td   align="left" HEIGHT="24" ><fmtPendAllocRpt:message key="LBL_10/21/2011"/></td>
												<td   align="left" HEIGHT="24"   ><fmtPendAllocRpt:message key="LBL_10/25/2011"/></td>																			 
											</tr>
											<tr><td class="LLine" height="1" colspan="7"></td></tr>
											<tr class="oddshade">
												<td   align="right" HEIGHT="24"   >&nbsp;<input type=radio value="961.906" name="setid" checked="checked"><fmtPendAllocRpt:message key="LBL_961.906"/></input>&nbsp;</td>
												<td   align="left" HEIGHT="24"   ><fmtPendAllocRpt:message key="LBL_EXTENDED_PLATE_SET"/></td> 
												<td   align="left" HEIGHT="24"   ><fmtPendAllocRpt:message key="LBL_ADDITIONAL"/></td>
												<td   align="left" HEIGHT="24"    ><fmtPendAllocRpt:message key="LBL_MICHAEL_MELCHIONNI"/></td>
												<td   align="left" HEIGHT="24"   ><fmtPendAllocRpt:message key="LBL_MICHAEL_MELCHIONNI"/></td>												
												<td   align="left" HEIGHT="24" ><fmtPendAllocRpt:message key="LBL_10/22/2011"/></td>
												<td   align="left" HEIGHT="24"   ><fmtPendAllocRpt:message key="LBL_10/26/2011"/></td>																												 
											</tr>
											<tr><td class="LLine" height="1" colspan="7"></td></tr>
											<tr class="evenshade">
												<td   align="right" HEIGHT="24"   ></td>
												<td   align="left" HEIGHT="24"    ></td>
												<td   align="left" HEIGHT="24"   ></td>
												<td   align="right" HEIGHT="24"   ></td> 
												<td   align="left" HEIGHT="24"   ></td>
												<td   align="left" HEIGHT="24" ></td>
												<td   align="left" HEIGHT="24"   ></td>																			 
											</tr>
											<tr><td class="LLine" height="1" colspan="7"></td></tr>
											<tr class="oddshade">
												<td   align="right" HEIGHT="24"   ></td>
												<td   align="left" HEIGHT="24"    ></td>
												<td   align="left" HEIGHT="24"   ></td>
												<td   align="right" HEIGHT="24"   ></td> 
												<td   align="left" HEIGHT="24"   ></td>
												<td   align="left" HEIGHT="24" ></td>
												<td   align="left" HEIGHT="24"   ></td>																			 
											</tr>													 					
										</table>
									</td>
								</tr>								 
								<tr><td class="LLine" height="1" colspan="4"></td></tr>
											                   
				         	 </table>
			       	  	</td>
			   	  	</tr>
			   	  	<tr>
			         	<td class="RightTableButton" align="center" colspan="2">
			        		<fmtPendAllocRpt:message key="BTN_SET_OVERRIDE" var="varOverride"/>
			        		<gmjsp:button value="${varOverride}"  gmClass="button"  buttonType="Save" onClick="window.location='/sales/InvAllocation/GmSetOverrideRpt.jsp'"/>&nbsp;
			        		<%-- <gmjsp:button value="Set Override" gmClass="button" buttonType="Save" onClick="loadPopup();" />&nbsp;--%>			            	
			            </td>			                				                    
		            </tr>
				</table>
			</td>
		</tr> 
    </table>
</FORM>
<%@ include file="/common/GmFooter.inc"%> 
 
 
</BODY>
 
</HTML>
 

