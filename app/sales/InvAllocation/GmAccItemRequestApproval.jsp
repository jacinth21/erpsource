<%
	/**********************************************************************************
	 * File		 		: GmAccItemRequestApproval.jsp
	 * Desc		 		: For Account Item Request Approval.
	 * Version	 		: 1.0
	 * author			: Paveethra
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc" %>  
<%@ taglib prefix="fmtLoanerRequestApproval" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmAccItemRequestApproval.jsp -->
<fmtLoanerRequestApproval:setLocale value="<%=strLocale%>"/>
<fmtLoanerRequestApproval:setBundle basename="properties.labels.custservice.ProcessRequest.GmLoanerProcessRequest"/>
<%
String strSalesInvallocationJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_INVALLOCATION");
String strApplDateFmt = strGCompDateFmt;//GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
String strDtFmt = "{0,date,"+strApplDateFmt+"}";
String strHaction="";
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.ProcessRequest.GmLoanerProcessRequest", strSessCompanyLocale);
%>
<bean:define id="xmlGridData" name="frmAccItemRequestApproval" property="xmlGridData" type="java.lang.String"></bean:define>
<bean:define id="strTxnType" name="frmAccItemRequestApproval" property="strTxnType" type="java.lang.String"></bean:define>
<bean:define id="appvlAccessFl" name="frmAccItemRequestApproval" property="appvlAccessFl" type="java.lang.String"></bean:define>
<bean:define id="strUserType" name="frmAccItemRequestApproval" property="userType" type="java.lang.String"></bean:define>
<bean:define id="strOpt" name="frmAccItemRequestApproval" property="strOpt" type="java.lang.String"></bean:define>

 <bean:define id="alSystemList" name="frmAccItemRequestApproval" property="alSystemList" type="java.util.ArrayList"> </bean:define>
<bean:define id="strAccountID" name="frmAccItemRequestApproval" property="accountID" type="java.lang.String"></bean:define>
<bean:define id="strAccountName" name="frmAccItemRequestApproval" property="searchaccountID" type="java.lang.String"></bean:define>
<bean:define id="userType" name="frmAccItemRequestApproval" property="userType" type="java.lang.String"></bean:define>

<%
request.setCharacterEncoding("UTF-8");
response.setContentType("text/html; charset=UTF-8");



	String strHeaderLbl =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ACC_ITEM_REQUEST_APPROVAL_DASHBOARD"));
	String strWikiProp = "ITEM_REQUEST_APPVL";
	String strShipDtLbl =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("VM_REQUESTED_DATE"));


%>
<HTML>
<HEAD>
<TITLE> Globus Medical: <%=strHeaderLbl%> </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strSalesInvallocationJsPath%>/GmAccItemRequestApproval.js"></script>

<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 
<script>
var objGridData = '';
objGridData = '<%=xmlGridData%>';
var gObj='';
var strTxnType ='<%=strTxnType%>';
var strOptVal ='<%=strOpt%>';
var appvlAccessFl = '<%=appvlAccessFl%>';
var format = '<%=strApplDateFmt%>';
var userType = '<%=userType%>';

</script>
</HEAD>
 
<BODY leftmargin="20" topmargin="10" onload="fnOnload()" onkeydown="javaScript:return fnLoadRequset();">
<html:form action="/gmAccItemRequestApproval.do?method=accItemPendingApprRequests">
<html:hidden property="strOpt" value="load"/>
<html:hidden property="strReqIds"/>
<html:hidden property="strTxnType"/>
<html:hidden property="haction" value="<%=strHaction%>"/>
<html:hidden property="useyType" value="<%=strUserType%>"/>
<html:hidden property="hApproveInputString"/>
<html:hidden property="partString"/>
<html:hidden property="raqtyString"/>
<html:hidden property="userType"/>
<html:hidden property="requestor_name"/>


<!-- Custom tag lib code modified for JBOSS migration changes -->
<table class="DtTable1100" border="0" cellspacing="0" cellpadding="0" style="width: 1250px;">
	<tr >
		<td height="30" class="RightDashBoardHeader" colspan="4">&nbsp;<%=strHeaderLbl%></td>
		<td align="right" class=RightDashBoardHeader colspan="4">
			<fmtLoanerRequestApproval:message key="LBL_HELP" var="varHelp"/>
			<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle(strWikiProp)%>');" />
		</td>
		</tr>
		<tr><td class="Line" height="1" colspan="8"></td></tr>	
		<tr>
			<td colspan="8" height="20">
				<jsp:include page="/sales/GmSalesFilters.jsp" >
					<jsp:param name="HIDE" value="SYSTEM" />
					<jsp:param name="FRMNAME" value="frmAccItemRequestApproval" />
					<jsp:param name="HACTION" value="" />
				</jsp:include> 
			</td>
		</tr>	
		<tr class="evenshade">
			<td class="RightRedCaption" height="25" align="Right" width="20%"><fmtLoanerRequestApproval:message key="LBL_PARENTREQID"/>:</td>
			
			<td>&nbsp;<html:text property="requestId" maxlength="20" size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" onkeypress="fnEnter(event);" />
			</td>
			<td class="RightRedCaption" height="25" align="Right" width="20%">&nbsp;<fmtLoanerRequestApproval:message key="LBL_SYSTEMLIST"/>:</td>
				<td>&nbsp;<gmjsp:autolist  comboType="DhtmlXCombo"    SFFormName="frmLoanerRequestApproval" SFSeletedValue="systemid"   controlName="systemid" value="<%=alSystemList%>"  codeId="ID" codeName="NAME" 
				defaultValue="${varChooseone}" tabIndex="1" width="300" />
								
		</tr>

		<tr><td class="LLine" height="1" colspan="9"></td></tr>
		<tr class="oddshade">
						<td  height="30" class="RightTableCaption" align="right" width="13%"><fmtLoanerRequestApproval:message key="LBL_ACCOUNT"/>:</td>
						<td><table cellpadding="0" cellspacing="0" border="0"><tr><td width="10px"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
										<jsp:param name="CONTROL_NAME" value="accountID" />
										<jsp:param name="METHOD_LOAD" value="loadAccountList" />
										<jsp:param name="WIDTH" value="320" />
										<jsp:param name="CSS_CLASS" value="search" />
										<jsp:param name="TAB_INDEX" value="1"/>
										<jsp:param name="CONTROL_NM_VALUE" value="<%=strAccountName %>" /> 
										<jsp:param name="CONTROL_ID_VALUE" value="<%=strAccountID %>" />
										<jsp:param name="SHOW_DATA" value="100" />
										<jsp:param name="AUTO_RELOAD" value="fnDisplayAccID(this);" />					
									</jsp:include></td></tr></table>	
						</td>
			
				<td class="RightRedCaption" height="25" align="Right" >&nbsp;<%=strShipDtLbl%>:</td>
				<td class="RightRedCaption" colspan="2">&nbsp;&nbsp;
				<table>
				<tr>
					<td colspan="8"><fmtLoanerRequestApproval:message key="LBL_FROMDATE"/>:&nbsp;<gmjsp:calendar SFFormName="frmAccItemRequestApproval" 
				controlName="startDt"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;<fmtLoanerRequestApproval:message key="LBL_TODATE"/>:&nbsp;<gmjsp:calendar SFFormName="frmAccItemRequestApproval" 
				controlName="endDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/></td>
				<td class="oddshade" colspan="8">&nbsp;<gmjsp:button value="&nbsp;Load&nbsp;"  name="Btn_Submit" gmClass="Button" buttonType="Load"  onClick="fnLoad();"/>
			</td>
			</tr>	
				</table>	</td>
		
			</tr>
		<tr><td class="Line" height="1" colspan="8"></td></tr>
		<tr class="evenshade" style="display: none;" id="pricenote">
		   <td height="25" colspan="8" ><fmtLoanerRequestApproval:message key="LBL_SETTIERPRICE"/></td>
		</tr>
		<tr><td class="LLine" height="1" colspan="8"></td></tr>
		<%if(xmlGridData.indexOf("cell") != -1){%>
		<tr>
			<td colspan="9"><div id="loanerPendingRpt" height="500px" width="1120"></div></td>					
		</tr>
		<%if(strUserType.equals("PD")){ %>
		<tr><td colspan="8" align="center" height="35">&nbsp;<input type="checkbox" size="30"  name="chk_ShowArrowFl" value=1 
		     class=RightTableCaption align="left">&nbsp;<fmtLoanerRequestApproval:message key="LBL_INHOUSECOMMENTS"/>&nbsp;&nbsp;
		</td></tr>
			<%} %>

		<tr>
			
			<td align="center" colspan="8" height="35">
				<logic:equal name="frmAccItemRequestApproval" property="appvlAccessFl" value="Y">
						<fmtLoanerRequestApproval:message key="BTN_APPROVE" var="varApprove"/>
				      	<gmjsp:button name="approve" controlId="approve" value="${varApprove}"  onClick="fnApprove('Request');"/>&nbsp;&nbsp;
				</logic:equal>
				<logic:equal name="frmAccItemRequestApproval" property="rejectAccessFl" value="Y">
						<fmtLoanerRequestApproval:message key="BTN_REJECT" var="varReject"/>
				       	<gmjsp:button value="${varReject}"  onClick="fnReject();"/>&nbsp;
				</logic:equal>
				<fmtLoanerRequestApproval:message key="BTN_INVENTORY" var="varInventory"/>
				<fmtLoanerRequestApproval:message key="BTN_ACCNET" var="varAccNet"/>
				<gmjsp:button value="${varInventory}" gmClass="Button" onClick="fnInventoryLookup();" tabindex="13" buttonType="Load" />&nbsp;
				<gmjsp:button value="${varAccNet}" gmClass="Button" onClick="fnAccNetRpt();" tabindex="13" buttonType="Load" />&nbsp;
				
			</td>
		</tr>
		<%}else{%>
		<tr>
		<td colspan="8" height="20" align="center" class="RightText"><fmtLoanerRequestApproval:message key="LBL_NODATA"/></td> 
		</tr>
		<%}%>
		<%if(xmlGridData.indexOf("cell") != -1) {%>
		<tr>
			<td align="center" colspan="6">
				<logic:equal name="frmAccItemRequestApproval" property="appvlAccessFl" value="Y">
					<div class='exportlinks'><fmtLoanerRequestApproval:message key="LBL_EXPORTOOPTION"/> : <img src='img/ico_file_excel.png' />&nbsp; <a href="#" onclick="fnExportToExcel();"> <fmtLoanerRequestApproval:message key="LBL_EXCEL"/> </a></div>
				</logic:equal>
			</td>
		</tr>
		<%}%>	
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>


