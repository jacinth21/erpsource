<%
	/**********************************************************************************
	 * File		 		: GmLoanerRequestApproval.jsp
	 * Desc		 		: For Loaner Request Approval.
	 * Version	 		: 1.0
	 * author			: Gopinathan
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc" %>  
<%@ taglib prefix="fmtLoanerRequestApproval" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmLoanerRequestApproval.jsp -->
<fmtLoanerRequestApproval:setLocale value="<%=strLocale%>"/>
<fmtLoanerRequestApproval:setBundle basename="properties.labels.custservice.ProcessRequest.GmLoanerProcessRequest"/>
<%
String strSalesInvallocationJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_INVALLOCATION");
String strApplDateFmt = strGCompDateFmt;//GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
String strDtFmt = "{0,date,"+strApplDateFmt+"}";
String strHaction="";
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.ProcessRequest.GmLoanerProcessRequest", strSessCompanyLocale);
%>
<bean:define id="xmlGridData" name="frmLoanerRequestApproval" property="xmlGridData" type="java.lang.String"></bean:define>
<bean:define id="strTxnType" name="frmLoanerRequestApproval" property="strTxnType" type="java.lang.String"></bean:define>
<bean:define id="appvlAccessFl" name="frmLoanerRequestApproval" property="appvlAccessFl" type="java.lang.String"></bean:define>
<bean:define id="strUserType" name="frmLoanerRequestApproval" property="userType" type="java.lang.String"></bean:define>
<bean:define id="strOpt" name="frmLoanerRequestApproval" property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="alFieldSales" name="frmLoanerRequestApproval" property="alFieldSales" type="java.util.ArrayList"> </bean:define>
<bean:define id="alSystemList" name="frmLoanerRequestApproval" property="alSystemList" type="java.util.ArrayList"> </bean:define>
<bean:define id="alSalesReps" name="frmLoanerRequestApproval" property="alSalesReps" type="java.util.ArrayList"> </bean:define>
<bean:define id="alDSOwner" name="frmLoanerRequestApproval" property="alDSOwner" type="java.util.ArrayList"> </bean:define>
<%
request.setCharacterEncoding("UTF-8");
response.setContentType("text/html; charset=UTF-8");

String 	strHeaderLbl =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SET_REQUEST_APPROVAL"));
String strWikiProp = "SET_REQUEST_APPVL" ;
String strShipDtLbl =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("VM_PLANNED_SHIP_DATE"));

if(strTxnType.equals("4119")){
	strHeaderLbl =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_IN_HOUSE_LOANER_REQUEST"));
	strWikiProp = "LOANER_REQUEST_APPVL";
}
if( strOpt.equals("PDREJLNREQAPPR") || strTxnType.equals("4127")){
	strHeaderLbl =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_LOANER_REQUEST_APPROVAL"));
	strWikiProp = "LOANER_REQUEST_APPVL";
	strHaction ="PDREJLNREQAPPR";
}
if(strTxnType.equals("400088")){
	strHeaderLbl =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ITEM_REQUEST_APPROVAL_DASHBOARD"));
	strWikiProp = "ITEM_REQUEST_APPVL";
	strShipDtLbl =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("VM_REQUESTED_DATE"));
}
if(strTxnType.equals("400087")){
	strShipDtLbl =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("VM_REQUESTED_DATE"));	
}
log.debug("the value inside strTxnType *** in jsp"+strTxnType);
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: <%=strHeaderLbl%> </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strSalesInvallocationJsPath%>/GmLoanerRequestApproval.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxCombo.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 
<script>
var objGridData = '';
objGridData = '<%=xmlGridData%>';
var gObj='';
var strTxnType ='<%=strTxnType%>';
var strOptVal ='<%=strOpt%>';
var appvlAccessFl = '<%=appvlAccessFl%>';
var format = '<%=strApplDateFmt%>';
var repLen = <%=alSalesReps.size()%>;

<%
HashMap hmRepVal = new HashMap();
for (int i=0;i<alSalesReps.size();i++)
{
	hmRepVal = (HashMap)alSalesReps.get(i);
%>
var	RepArr<%=i%> = "<%=hmRepVal.get("DID")%>^<%=hmRepVal.get("DNAME")%>^<%=hmRepVal.get("ID")%>^<%=hmRepVal.get("NM")%>" ;
<%
}
%>


function fnOpenAdd(p_ShipTo,p_ShipToId,p_addid) 
{


if (p_ShipTo == '0')
{
		alert(message_operations[720]);
		return;
}
if (p_ShipTo == '4121' && p_ShipToId =='0')
{
		alert(message_operations[718]);
		return;
}
if (p_ShipTo == '4121')
{
	p_ShipToId = p_addid;
}
windowOpener("/GmOrderItemServlet?hAction=SAddress&hShip="+p_ShipTo+"&hShipId="+p_ShipToId,"Address","resizable=yes,scrollbars=yes,top=340,left=420,width=300,height=135");
//windowOpener("/gmLoanerRequestApproval.do?strOpt=Address&hShip="+p_ShipTo+"&hShipId="+p_ShipToId,"Address","resizable=yes,scrollbars=yes,top=340,left=420,width=300,height=135");
}
</script>
</HEAD>
 
<BODY leftmargin="20" topmargin="10" onload="fnOnload()" onkeydown="javaScript:return fnLoadRequset();">
<html:form action="/gmLoanerRequestApproval.do?">
<html:hidden property="strOpt" value="load"/>
<html:hidden property="strReqIds"/>
<html:hidden property="strTxnType"/>
<html:hidden property="haction" value="<%=strHaction%>"/>
<html:hidden property="useyType" value="<%=strUserType%>"/>
<html:hidden property="hApproveInputString"/>
<html:hidden property="partString"/>
<html:hidden property="raqtyString"/>
<html:hidden property="userType"/>
<html:hidden property="requestor_name"/>


<!-- Custom tag lib code modified for JBOSS migration changes -->
<table class="DtTable1100" border="0" cellspacing="0" cellpadding="0" style="width: 1250px;">
	<tr >
		<td height="30" class="RightDashBoardHeader" colspan="4">&nbsp;<%=strHeaderLbl%></td>
		<td align="right" class=RightDashBoardHeader colspan="4">
			<fmtLoanerRequestApproval:message key="LBL_HELP" var="varHelp"/>
			<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle(strWikiProp)%>');" />
		</td>
		</tr>
		<tr><td class="Line" height="1" colspan="8"></td></tr>	
		<tr>
			<td colspan="8" height="20">
				<jsp:include page="/sales/GmSalesFilters.jsp" >
					<jsp:param name="HIDE" value="SYSTEM" />
					<jsp:param name="FRMNAME" value="frmLoanerRequestApproval" />
					<jsp:param name="HACTION" value="" />
				</jsp:include> 
			</td>
		</tr>	
		<tr class="evenshade">
			<td class="RightRedCaption" height="25" align="Right" width="20%"><fmtLoanerRequestApproval:message key="LBL_PARENTREQID"/>:</td>
			<td>&nbsp;<html:text property="requestId" maxlength="20" size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" onkeypress="fnEnter(event);" />
			</td>
			<fmtLoanerRequestApproval:message key="LBL_CHOOSEONE" var="varChooseone"/>
			<%if(strTxnType.equals("400088")){%>
				<td class="RightRedCaption" height="25" align="Right" width="20%">&nbsp;<fmtLoanerRequestApproval:message key="LBL_SYSTEMLIST"/>:</td>
				<td>&nbsp;<gmjsp:autolist  comboType="DhtmlXCombo"    SFFormName="frmLoanerRequestApproval" SFSeletedValue="systemid"   controlName="systemid" value="<%=alSystemList%>"  codeId="ID" codeName="NAME" 
				defaultValue="${varChooseone}" tabIndex="1" width="300" />
			</td>
			<%} else if(strTxnType.equals("400087")){%>	
			<td class="RightRedCaption" height="25" align="Right" width="15%">&nbsp;<fmtLoanerRequestApproval:message key="LBL_FIELDSALES"/>:</td>
				<td>&nbsp;<gmjsp:autolist  comboType="DhtmlXCombo" controlName="fieldSales" SFFormName="frmLoanerRequestApproval"  SFSeletedValue="fieldSales"   value="<%=alFieldSales%>"  codeId="ID" codeName="NM" 
				defaultValue="${varChooseone}" tabIndex="1" onChange="fnDistFilterReps" width="300" /></td>
				<%}else  { %>
				<td class="RightRedCaption" height="25" align="Right" width="15%">&nbsp;<fmtLoanerRequestApproval:message key="LBL_EVENTNAME"/>:</td>
				<td>&nbsp;<gmjsp:dropdown controlName="eventName"  SFFormName='frmLoanerRequestApproval' SFSeletedValue="eventName"  defaultValue= "${varChooseone}"	
					SFValue="alEventName" codeId="CID" codeName="ENAME" width="300" />
				</td><%}%>	
		</tr>
		<%if(strTxnType.equals("400088") ){%>					
			<tr><td class="LLine" height="1" colspan="9"></td></tr>
				<tr class="oddshade">
				<td class="RightRedCaption" height="25" align="Right" width="15%">&nbsp;<fmtLoanerRequestApproval:message key="LBL_FIELDSALES"/>:</td>
				<td>&nbsp;<gmjsp:autolist  comboType="DhtmlXCombo" controlName="fieldSales" SFFormName="frmLoanerRequestApproval"  SFSeletedValue="fieldSales"   value="<%=alFieldSales%>"  codeId="ID" codeName="NM" 
				defaultValue="${varChooseone}" tabIndex="1" onChange="fnDistFilterReps" width="300" /></td>
				<td class="RightRedCaption" height="25" align="Right" width="15%">&nbsp;<fmtLoanerRequestApproval:message key="LBL_SALESREPS"/>:</td>
				<td colspan="5">&nbsp;<gmjsp:autolist  comboType="DhtmlXCombo"  SFFormName="frmLoanerRequestApproval" SFSeletedValue="salesRep"   controlName="salesRep"  value="<%=alSalesReps%>"  codeId="ID" codeName="NM" 
					defaultValue="${varChooseone}" tabIndex="1" width="300" />
				</td><%}%>
		</tr>
		<tr><td class="LLine" height="1" colspan="9"></td></tr>
		<%if(strTxnType.equals("400088")){%>
		<tr class="evenshade">
		<%}else { %>
		<tr class="oddshade">
		<%}%>
		<%if(strTxnType.equals("400088")){%>
			<td class="RightRedCaption" height="25" align="Right" width="15%">&nbsp;<fmtLoanerRequestApproval:message key="LBL_SHEETOWNER"/>:</td>
			<td>&nbsp;<gmjsp:autolist  comboType="DhtmlXCombo"  SFFormName="frmLoanerRequestApproval" SFSeletedValue="sheetOwner"   controlName="sheetOwner"  value="<%=alDSOwner%>"  codeId="ID" codeName="NAME" 
					defaultValue="${varChooseone}" tabIndex="1" width="300" /></td>
	    <%}%>
			<td class="RightRedCaption" height="25" align="Right" >&nbsp;<%=strShipDtLbl%>:</td>
			<td class="RightRedCaption" >&nbsp;&nbsp;
			<%if(strTxnType.equals("400088")){%>
			<table>
				<tr>
					<td colspan="8">From Date:&nbsp;<gmjsp:calendar SFFormName="frmLoanerRequestApproval" 
				controlName="startDt"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;<fmtLoanerRequestApproval:message key="LBL_TODATE"/>:&nbsp;<gmjsp:calendar SFFormName="frmLoanerRequestApproval" 
				controlName="endDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/></td>
				<td class="oddshade" colspan="8">&nbsp;<gmjsp:button value="&nbsp;Load&nbsp;"  name="Btn_Submit" gmClass="Button" buttonType="Load"  onClick="fnLoad();"/>
			</td>		</tr></table><% } else {%>
			
			 <fmtLoanerRequestApproval:message key="LBL_FROMDATE"/>:&nbsp;<gmjsp:calendar SFFormName="frmLoanerRequestApproval" 
				controlName="startDt"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtLoanerRequestApproval:message key="LBL_TODATE"/>:&nbsp;<gmjsp:calendar SFFormName="frmLoanerRequestApproval" 
				controlName="endDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			</td>			
			<fmtLoanerRequestApproval:message key="BTN_LOAD" var="varLoad"/>
			<td colspan="6" align="right" width="175">&nbsp;<gmjsp:button value="&nbsp;${varLoad}&nbsp;"  name="Btn_Submit" gmClass="Button" buttonType="Load"  onClick="fnLoad();"/></td><%} %>
		</tr>
		<tr><td class="Line" height="1" colspan="8"></td></tr>
		<tr class="evenshade" style="display: none;" id="pricenote">
		   <td height="25" colspan="8" ><fmtLoanerRequestApproval:message key="LBL_SETTIERPRICE"/></td>
		</tr>
		<tr><td class="LLine" height="1" colspan="8"></td></tr>
		<%if(xmlGridData.indexOf("cell") != -1){%>
		<tr>
			<td colspan="9"><div id="loanerPendingRpt" height="500px" width="1120"></div></td>					
		</tr>
		<%if(strUserType.equals("PD") && strTxnType.equals("400088")){ %>
		<tr><td colspan="8" align="center" height="35">&nbsp;<input type="checkbox" size="30"  name="chk_ShowArrowFl" value=1 
		     class=RightTableCaption align="left">&nbsp;<fmtLoanerRequestApproval:message key="LBL_INHOUSECOMMENTS"/>&nbsp;&nbsp;
		</td></tr>
			<%} %>
		<%if(strTxnType.equals("400087") || (strOpt.equals("PDREJLNREQAPPR") && strTxnType.equals("4127"))){%>	
		<tr class="Shade">
		<td colspan="9"> 
	    <jsp:include page="/common/GmIncludeLog.jsp" >
	    <jsp:param name="FORMNAME" value="frmLoanerRequestApproval" />
		<jsp:param name="ALNAME" value="alLogReasons" />
		<jsp:param name="LogMode" value="Edit" />					
		</jsp:include>
		</td></tr>
			<%} %>
		<tr>
			
			<td align="center" colspan="8" height="35">
				<logic:equal name="frmLoanerRequestApproval" property="appvlAccessFl" value="Y">
						<fmtLoanerRequestApproval:message key="BTN_APPROVE" var="varApprove"/>
				      	<gmjsp:button name="approve" controlId="approve" value="${varApprove}"  onClick="fnApprove('Request');"/>&nbsp;&nbsp;
				</logic:equal>
				<logic:equal name="frmLoanerRequestApproval" property="rejectAccessFl" value="Y">
						<fmtLoanerRequestApproval:message key="BTN_REJECT" var="varReject"/>
				       	<gmjsp:button value="${varReject}"  onClick="fnReject();"/>&nbsp;
				</logic:equal>
				<%if(strTxnType.equals("400088")){%>
				<logic:equal name="frmLoanerRequestApproval" property="appvlAccessFl" value="Y">
				<fmtLoanerRequestApproval:message key="BTN_INVENTORY" var="varInventory"/>
				<fmtLoanerRequestApproval:message key="BTN_TREND" var="varTrend"/>
				<fmtLoanerRequestApproval:message key="BTN_CONSIGNMENT" var="varConsignment"/>
				<gmjsp:button value="${varInventory}" gmClass="Button" onClick="fnInventoryLookup();" tabindex="13" buttonType="Load" />&nbsp;
				<gmjsp:button value="${varTrend}" gmClass="Button" onClick="fnTrendLookup();" tabindex="13" buttonType="Load" />&nbsp;
				<gmjsp:button value="${varConsignment}" gmClass="Button" onClick="fnConsignmentByPart();" tabindex="13" buttonType="Load" />&nbsp;
				</logic:equal>
				<% } %>
			</td>
		</tr>
		<%}else{%>
		<tr>
			<td colspan="8" height="20" align="center" class="RightText"><fmtLoanerRequestApproval:message key="LBL_NODATA"/></td>
		</tr>
		<%}%>
		<%if(xmlGridData.indexOf("cell") != -1) {%>
		<tr>
			<td align="center" colspan="6">
				<logic:equal name="frmLoanerRequestApproval" property="appvlAccessFl" value="Y">
					<div class='exportlinks'><fmtLoanerRequestApproval:message key="LBL_EXPORTOOPTION"/> : <img src='img/ico_file_excel.png' />&nbsp; <a href="#" onclick="fnExportToExcel();"> <fmtLoanerRequestApproval:message key="LBL_EXCEL"/> </a></div>
				</logic:equal>
			</td>
		</tr>
		<%}%>	
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>


