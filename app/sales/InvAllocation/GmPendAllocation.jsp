<%
	/**********************************************************************************
	 * File		 		: GmPendAllocation.jsp
	 * Desc		 		: For Pending allocation report.
	 * Version	 		: 1.0
	 * author			: Rshah
	 ************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@page import="org.apache.commons.beanutils.DynaClass"%>
<%@page import="org.apache.commons.beanutils.BasicDynaBean"%>

<%@ taglib prefix="fmtPendAllocation" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- sales\InvAllocation\GmPendAllocation.jsp -->

<fmtPendAllocation:setLocale value="<%=strLocale%>"/>
<fmtPendAllocation:setBundle basename="properties.labels.sales.InvAllocation.GmPendAllocation"/>

<%
String strSalesInvallocationJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_INVALLOCATION");
String strWikiTitle = GmCommonClass.getWikiTitle("PENDING_ALLOCATION");
String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
String strDtFmt = "{0,date,"+strApplDateFmt+"}";
String strTodaysDate = GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate"));

boolean strExport = true;

if (strBrowserType == "SAFARI" || strBrowserType == "")
{
	strExport = false;
	
}

%>
<bean:define id="returnList" name="frmPendAllocation" property="returnList" type="java.util.List"></bean:define>
<HTML>
<HEAD>
<TITLE>Globus Medical: Pending Allocation</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>


<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
	<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js" type="text/javascript"></script>
	<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js" type="text/javascript"></script>
	<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"  type="text/javascript"></script>
	<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">


<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
var dhxWins,w1;
var strToday = "<%=strTodaysDate%>";
 </script> 
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strSalesInvallocationJsPath%>/GmPendAllocation.js"></script>
</HEAD>
<BODY leftmargin="20" topmargin="10" >
<html:form method="post" action="/gmPendAllocation?method=loadPendingAllocationRpt">
<html:hidden property="hSetId" value=""/>
<html:hidden property="strOpt"/>
<html:hidden property="hsetName" value=""/>
<html:hidden property="hcaseDistId" value=""/>
<html:hidden property="hcaseSetId" value=""/>
<html:hidden property="hcaseInfoId" value=""/>

<!-- Custom tag lib code modified for JBOSS migration changes -->
		<table class="DtTable950" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" colspan="3" class="RightDashBoardHeader">&nbsp;<fmtPendAllocation:message key="LBL_PENDING_ALLOCATION"/></td>
				<td  height="25" class="RightDashBoardHeader" align="right">
					<fmtPendAllocation:message key="IMG_ALT_HELP" var = "varHelp"/>
					<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       		</td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="4"></td>
			</tr>
			
			<tr>
				<fmtPendAllocation:message key="LBL_FIELD_SALES" var="varFieldSal"/>
				<gmjsp:label type="BoldText" SFLblControlName="&nbsp;${varFieldSal}" td="true"/>
				<td class="RightRedCaption">&nbsp;<gmjsp:dropdown controlName="dist" SFFormName='frmPendAllocation' SFSeletedValue="dist"
						SFValue="alDist" codeId="ID" codeName="NM" defaultValue="[Choose One]" />
				</td>
				<fmtPendAllocation:message key="LBL_SALES_REP" var="varSalesRep"/>
				<gmjsp:label type="BoldText" SFLblControlName="${varSalesRep}" td="true"/>
				<td>&nbsp;<gmjsp:dropdown controlName="salesRep" SFFormName='frmPendAllocation' SFSeletedValue="salesRep"
						defaultValue="[Choose One]" SFValue="alRepList" codeId="ID" codeName="NM" />
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="Shade">
				<fmtPendAllocation:message key="LBL_CASE_ID" var="varCaseId"/>
				<gmjsp:label type="BoldText" SFLblControlName="&nbsp;${varCaseId}" td="true"/>
				<td class="RightRedCaption">&nbsp;<html:text property="caseId" size="20" onfocus="changeBgColor(this,'#AACCE8');" 
				styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" />&nbsp;
				</td>
				<fmtPendAllocation:message key="LBL_SURGERY_DATE" var="varSurgeryDt"/>
				<gmjsp:label type="BoldText" SFLblControlName="${varSurgeryDt}" td="true" />
				<td width="320">&nbsp;<gmjsp:calendar SFFormName="frmPendAllocation" SFDtTextControlName="sugeryDate"
						gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr>
				<fmtPendAllocation:message key="LBL_LOAD_DATE" var="varLoadDate"/>
				<gmjsp:label type="BoldText" SFLblControlName="&nbsp;${varLoadDate}"/>				
				<td width="320">&nbsp;<gmjsp:calendar
						SFFormName="frmPendAllocation" SFDtTextControlName="loanDate"
						gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" /></td>
				<td>&nbsp;</td>
				<td class="RightRedCaption" align="left" >&nbsp;
				<fmtPendAllocation:message key="LBL_LOAD" var="varLoad"/>
				<gmjsp:button value="&nbsp;${varLoad}&nbsp;" buttonType="Load" onClick="fnLoad();"/></td>
			</tr>
			
			<tr>
				<td class="Line" height="1" colspan="4"></td>
			</tr>
			<%
				if (returnList.size() > 0)
				{
			%>
			<tr>
				<td colspan="4" width="100%">
		 
					<display:table cellspacing="0" cellpadding="0" name="requestScope.frmPendAllocation.returnList"  pagesize="20" 	export="<%=strExport %>"  class="its" requestURI="/gmPendAllocation.do" id="currentRowObject" decorator="com.globus.sales.InvAllocation.displaytag.DTPendAllocationWrapper">
					 
						<fmtPendAllocation:message key="DT_CASE_ID" var="varCaseId"/>
						<display:column property="CASE_ID" title="${varCaseId}"  class="alignleft" style="width=145"/>
						<fmtPendAllocation:message key="DT_SURGERY_DATE" var="varSurDt"/>
						<display:column property="SURGERYDT" title="${varSurDt}"  sortable="true" class="alignleft" format="<%=strDtFmt%>"/>
						<fmtPendAllocation:message key="DT_SET_ID" var="varSetId"/>
						<display:column property="SETID" title="${varSetId}"  sortable="true" class="alignleft" style="width=65"/>
						<fmtPendAllocation:message key="DT_SET_NAME" var="varSetName"/>
						<display:column property="SETNAME" title="${varSetName}" class="alignleft" maxLength="80"  sortable="true"/>
						<fmtPendAllocation:message key="DT_FIELD_SALES" var="varFieldSal"/>
						<display:column property="DISTNAME" title="${varFieldSal}" class="alignleft" sortable="true" />
						<fmtPendAllocation:message key="DT_SALES_REP" var="varSalesRep"/>
						<display:column property="REPNAME" title="${varSalesRep}" class="alignleft"  sortable="true"/>
						<fmtPendAllocation:message key="DT_LOCK_FROM" var="varLockFrm"/>
						<display:column property="LOCKFROMDT" title="${varLockFrm}" class="alignleft"  format="<%=strDtFmt%>" sortable="true" style="width=70"/>
						<fmtPendAllocation:message key="DT_LOCK_TO" var="varLockTo"/>
						<display:column property="LOCKTODT" title="${varLockTo}" class="alignleft" format="<%=strDtFmt%>" sortable="true" style="width=70"/>
					</display:table>
			 
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
								
			<tr>
				<td class="RightRedCaption" align="center" colspan="4">
				<fmtPendAllocation:message key="BTN_OVERRIDE" var="varOverride"/>
				<gmjsp:button value="${varOverride}" buttonType="Save" onClick="fnSetOverride();"/>&nbsp;
					
				</td>
			</tr>
			<%} %>
		</table>

	</html:form>
<%@ include file="/common/GmFooter.inc" %>	
</BODY>
</HTML>


