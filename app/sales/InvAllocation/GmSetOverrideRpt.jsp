<%
/**********************************************************************************
 * File		 		: GmSetOverrideRpt.jsp
 * Desc		 		: For Override of tags.
 * Version	 		: 1.0
 * author			: Gopinathan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %> 

<%@ taglib prefix="fmtSetOverrideRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<!-- sales\InvAllocation\GmSetOverrideRpt.jsp -->

<fmtSetOverrideRpt:setLocale value="<%=strLocale%>"/>
<fmtSetOverrideRpt:setBundle basename="properties.labels.sales.InvAllocation.GmSetOverrideRpt"/>

<%
String strSalesInvallocationJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_INVALLOCATION");
String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
String strDtFmt = "{0,date,"+strApplDateFmt+"}";
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Set Override </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
	<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js" type="text/javascript"></script>
	<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js" type="text/javascript"></script>
	<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"  type="text/javascript"></script>
	<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strSalesInvallocationJsPath%>/GmSetOverrideRpt.js"></script>  
</HEAD>
 
<BODY leftmargin="20" topmargin="10" onload="fnPageLoad();">
<html:form action="/gmSetOverrideRpt.do?">
<html:hidden property="caseSetId"/>
<html:hidden property="caseInfoId"/>
<html:hidden property="setId"/>
<html:hidden property="setName"/>
<html:hidden property="caseDistId"/>
<html:hidden property="tagDistId"/>
<html:hidden property="tagId"/>
<html:hidden property="lockFromDt"/>
<html:hidden property="lockToDt"/>
<html:hidden property="strOpt"/> 
<html:hidden property="parentFrmName"/> 
<html:hidden property="mapFlag"/> 

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table class="DtTable950" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td >
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr >
						<td height="25" class="RightDashBoardHeader">&nbsp;<fmtSetOverrideRpt:message key="LBL_SET_OVERRIDE"/> </td>
										 
					</tr>
					<tr><td class="Line" height="1" colspan="2"></td></tr>
		
						
                    <tr>
                    	<td width="100%" height="100%" valign="top"  >
						<table border="0" width="100%" cellspacing="0" cellpadding="0" >
			            	<tr class="evenshade">
			                	<fmtSetOverrideRpt:message key="LBL_SET_ID" var="varSetId"/>
			                	<gmjsp:label type="BoldText"  SFLblControlName="${varSetId} " td="true"/> 
			                    <td class="RightRedCaption" width="30%">&nbsp;
			                    	<bean:write name="frmSetOverrideRpt" property="setId"/>
			                    </td>
			                    <fmtSetOverrideRpt:message key="LBL_SET_NAME" var="varSetName"/>
			                    <gmjsp:label type="BoldText"  SFLblControlName="${varSetName} " td="true"/> 
			                    <td class="RightRedCaption" width="40%">&nbsp;
			                   		<bean:write name="frmSetOverrideRpt" property="setName"/>
			                    </td>
			                </tr>
			                <tr><td class="LLine" height="1" colspan="4"></td></tr>
			                <tr class="oddShade">     
			                    <fmtSetOverrideRpt:message key="LBL_FIELD_SALES" var="varFieldSales"/>
			                    <gmjsp:label type="BoldText"  SFLblControlName="${varFieldSales}Field Sales: " td="true"/>           
								<td width="40%">&nbsp;<gmjsp:dropdown	controlName="distId" SFFormName="frmSetOverrideRpt" SFSeletedValue="distId" SFValue="alDistributor" codeId="ID" codeName="NM" defaultValue="[Choose One]" width="300" onChange="" />	    		            				
				                </td>
				                <td height="25" align="left"></td>
				                <td class="RightRedCaption" align="center">
				                		&nbsp;<fmtSetOverrideRpt:message key="BTN_LOAD" var="varLoad"/><gmjsp:button buttonType="Load" name="load" style="width: 5em"  value="${varLoad}"  onClick="displayDiv(); fnLoad();"/>&nbsp;&nbsp;&nbsp;
				                		&nbsp;<fmtSetOverrideRpt:message key="BTN_MAP" var="varMap"/><gmjsp:button name="btn_Map" buttonType="Save" style="width: 5em" value="${varMap}"   onClick="displayDiv(); fnLoadMap();"/>&nbsp;
				                </td>
				                
				               <%-- <a href='/gmMap.do?method=getMap&strOpt=load'>Google Map</a> --%>
		                    </tr>
			                <tr><td class="LLine" height="1" colspan="4"></td></tr>
					        <tr>
								<td colspan="4">
									<table border="0" width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td>
											<div id = "load" >
												<display:table name="requestScope.frmSetOverrideRpt.alResult" class="its" id="currentRowObject"  pagesize="5"
												export = "false" decorator="com.globus.sales.InvAllocation.displaytag.DTTagSetOverrideWrapper" requestURI="/gmSetOverrideRpt.do?"> 
													<fmtSetOverrideRpt:message key="DT_TAG" var="varTag"/>
													<display:column property="TAGID" title="${varTag}" style="width:120;" class="alignleft" sortable="true"/>
													<fmtSetOverrideRpt:message key="DT_FIELD_SALES" var="varFieldSales"/>
													<display:column property="DISTNAME" title="${varFieldSales}" style="width:100;" class="alignleft" sortable="true"/> 
													<fmtSetOverrideRpt:message key="DT_SALES_REP" var="varSalesRep"/>
													<display:column property="REPNAME" title="${varSalesRep}Sales Rep"  style="width:100;" class="alignleft" sortable="true"/>							
													<fmtSetOverrideRpt:message key="DT_CURRENT_LOACTION" var="varCurLoc"/>
													<display:column property="CURADDR" title="${varCurLoc}"  style="width:280;" class="alignleft" sortable="true"/>
													<fmtSetOverrideRpt:message key="DT_DISTANCE" var="varDistance"/>
													<display:column property="DIST" title="${varDistance}" class="aligncenter" sortable="true"></display:column>
													<fmtSetOverrideRpt:message key="DT_NEXT_CASE" var="varNxtCase"/>
													<display:column property="CASE_DATE" title="${varNxtCase}" style="width:80;"  class="aligncenter" sortable="true"  format="<%=strDtFmt%>"/>
													<fmtSetOverrideRpt:message key="DT_STATUS" var="varStatus"/>
													<display:column property="DECOMSIN" title="${varStatus}" style="width:50;" class="aligncenter" />
													<fmtSetOverrideRpt:message key="DT_LOCK" var="varLock"/>
													<display:column property="LOCKINV" title="${varLock}" style="width:50;" class="aligncenter" />
												</display:table>
											</div>
											<div id = "map" ></div>
											</td>
										</tr>
									</table> 
								</td>
							</tr>
							<tr><td class="LLine" height="1" colspan="4"></td></tr>
			   	  			<tr>
			                	<td align="center" colspan="4" class="RightRedCaption">
			                			<fmtSetOverrideRpt:message key="BTN_SUBMIT" var="varSubmit"/>
			                			<gmjsp:button  value="${varSubmit}" buttonType="Save" onClick="fnSubmit();"/>&nbsp;
			                			<fmtSetOverrideRpt:message key="BTN_CLOSE" var="varClose"/>
			                			<gmjsp:button  value="${varClose}" buttonType="Load" onClick="fnClose();"/>&nbsp;
			                	</td>			                				                    
		                    </tr>
				</table>
			</td>
		</tr> 
    </table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
 

