<%
	/**********************************************************************************
	 * File		 		: GmItemApprovalComment.jsp
	 * Desc		 		: For Approving With Comments.
	 * Version	 		: 1.0
	 * author			: Mahavishnu
	 ************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %> 

<%@ taglib prefix="fmtApprovalComment" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- sales\InvAllocation\GmItemApprovalComment.jsp -->

<fmtApprovalComment:setLocale value="<%=strLocale%>"/>
<fmtApprovalComment:setBundle basename="properties.labels.sales.InvAllocation.GmApprovalComment"/>
 
<bean:define id="email_subject" name="frmAccItemRequestApproval" property="email_subject" type="java.lang.String"></bean:define>
<bean:define id="RAID" name="frmAccItemRequestApproval" property="raId" type="java.lang.String"></bean:define>
<bean:define id="RequestId" name="frmAccItemRequestApproval" property="request_id" type="java.lang.String"></bean:define>
<bean:define id="message" name="frmAccItemRequestApproval" property="message" type="java.lang.String"></bean:define>

<%
String strSalesInvallocationJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_INVALLOCATION");
request.setCharacterEncoding("UTF-8");
response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Approval With Comments </TITLE>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strSalesInvallocationJsPath%>/GmAccItemRequestApproval.js"></script>


</HEAD>
<BODY leftmargin="20" topmargin="10">
<html:form action="/gmAccItemRequestApproval.do?">
<html:hidden property="strOpt"/>
<html:hidden property="strReqIds"/>
<html:hidden property="strTxnType"/>
<html:hidden property="haction"/>
<html:hidden property="hApproveInputString"/>
<html:hidden property="partString"/>
<html:hidden property="raqtyString"/>
<html:hidden property="userType"/>
<html:hidden property="chkbox_display"/>
<html:hidden property="requestor_name"/>
<table  class="DtTable700" border="0" cellspacing="0" cellpadding="0">
<%if(RequestId.equals("")){%>
	<tr >
		<td  colspan="2" height="30" class="RightDashBoardHeader">&nbsp;<fmtApprovalComment:message key="LBL_APPROVE_WITH_COMMENTS"/></td>
	</tr>
	<tr>
	<tr>
		<td class="LLine" colspan="2"></td>
	</tr>
	<tr>
		<td class="RightTableCaption" align="right" ><fmtApprovalComment:message key="LBL_TO"/>:</td>
		<td >&nbsp;<html:text  property="email_to"     
		styleClass="InputArea" size="75"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="3" /></td>
	</tr>
	<tr>
		<td class="LLine" colspan="2"></td>
	</tr>
	<tr class="oddshade">
		<td class="RightTableCaption" align="right" width="20.5%"><fmtApprovalComment:message key="LBL_CC"/>:</td>
		<td >&nbsp;<html:text  property="email_cc"    
		styleClass="InputArea" size="75" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="4" /></td>
	</tr>
	<tr>
		<td class="LLine" colspan="2"></td>
	</tr>
	<tr class="evenshade">
		<td class="RightTableCaption" align="right" width="20.5%"><fmtApprovalComment:message key="LBL_SUBJECT"/>:</td>
		<td >&nbsp;<html:text  property="email_subject"    
		styleClass="InputArea" size="75" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="5" /></td>
	</tr>
	<tr>
		<td class="LLine" colspan="2"></td>
	</tr>
	<tr class="oddshade">
		<td class="RightTableCaption" align="right" width="20.5%"><fmtApprovalComment:message key="LBL_COMMENTS"/>:</td>
		<td width="200">&nbsp;<html:textarea  property="email_comments"  cols="75" style="height:70px" tabindex="6"
		styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
	</tr>
	
	<tr>
			
		<td align="center" colspan="2" height="35">
		<fmtApprovalComment:message key="BTN_SEND" var="varSend"/><gmjsp:button value="${varSend}" gmClass="button" onClick="fnApprove('Comments',this.form);" tabindex="7" buttonType="Save" />&nbsp;
		<fmtApprovalComment:message key="BTN_CANCEL" var="varCancel"/><gmjsp:button value="${varCancel}" gmClass="button" onClick="fnCancel();" tabindex="8" buttonType="Load" />&nbsp;
		</td>
	</tr>
	

 <% }else{%>
 
 	<tr>
		<td colspan="5" align="center" height="25">&nbsp;<font style="color: Blue;"><b><bean:write name="frmAccItemRequestApproval" property="message"/></b></font></td>
	</tr>
	<tr>
		<td colspan="5" align="center" height="25">&nbsp;<fmtApprovalComment:message key="LBL_NEW_REQ"/>:&nbsp;<%=RequestId%></td>
	</tr>
	<tr>
		<td colspan="5" align="center" height="25">&nbsp;<fmtApprovalComment:message key="LBL_NEW_RA"/>:&nbsp;<%=RAID%></td>
	</tr>
	<tr>
			
		<td align="center" colspan="2" height="35">
		<fmtApprovalComment:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="${varClose}" gmClass="button" onClick="fnClose();" tabindex="8" buttonType="Load" />&nbsp;
		</td>
	</tr>
 <% } %>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>



