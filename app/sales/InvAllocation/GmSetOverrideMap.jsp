 <%
/**********************************************************************************
 * File		 		: GmSetOverrideMap.jsp
 * Desc		 		: This screen is used for to track distributor. 
 * Version	 		: 1.0
 * author			: Jignesh Shah
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>


<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<%@ taglib prefix="fmtSetOverrideMap" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\InvAllocation\GmSetOverrideMap.jsp -->

<fmtSetOverrideMap:setLocale value="<%=strLocale%>"/>
<fmtSetOverrideMap:setBundle basename="properties.labels.sales.InvAllocation.GmSetOverrideMap"/>

<bean:define id="xmlData" name="frmSetOverrideRpt" property="xmlData" type="java.lang.String"></bean:define>
<%
String strSalesInvallocationJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_INVALLOCATION");

%>
<HTML>
<HEAD>
<TITLE>SetOverride Map</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strSalesInvallocationJsPath%>/GmSetOverrideRpt.js"></script>
<!--<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?sensor=false"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
--> <script src="http://maps.google.com/maps?file=api&key=" type="text/javascript"></script>

<script language="JavaScript" src="<%=strJsPath%>/GmLabeledMarker.js"></script>  
<script language="JavaScript" src="<%=strJsPath%>/GmMap.js"></script>
<script src="<%=strJsPath%>/GmMarkerManager.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script>

var strXmlData = '<%=xmlData%>';

</script>

</HEAD>
<BODY leftmargin="20" topmargin="10" onload="LoadMap();" >
<html:form action="/gmSetOverrideRpt.do?">
<html:hidden property="tagId"/>
<html:hidden property="caseSetId"/>
<html:hidden property="tagDistId"/>
<html:hidden property="parentFrmName"/> 
<html:hidden property="strOpt"/>
	<table border="1"cellspacing="0" cellpadding="0" >
		
		<tr>
			<td height="25" class="RightDashBoardHeader" >
				<fmtSetOverrideMap:message key="LBL_DISTRIBUTOR_TRACKER"/>
			</td>
		</tr>
		<tr><td class="Line" height="1"></td></tr>
		<tr valign="top">
			<td>
				<table cellpadding="0" cellspacing="0" border="0" bordercolor="brown">
				<tr>
					<td><div >
						<div id="map" style="width: 920px; height: 500px"></div><br/>
					</div></td>
				</tr>
				</table>
			</td>
			
		</tr>
		<tr height="40">
				<td align="center"><!-- Custom tag lib code modified for JBOSS migration changes -->
				<fmtSetOverrideMap:message key="BTN_BACK" var="varBck"/>
				<gmjsp:button name="back" buttonType="Load" value="${varBck}" onClick="history.back();"/>
				</td>
		</tr>
	</table>
</html:form>

</BODY>

</HTML>
