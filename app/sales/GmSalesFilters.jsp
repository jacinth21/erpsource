
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@ page import="com.globus.common.beans.GmCommonControls"%>

<!-- WEB-INF path corrected for JBOSS migration changes -->
	<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtSalesFilters" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmSalesFilters.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
}
%>
<fmtSalesFilters:setLocale value="<%=strLocale%>"/>
<fmtSalesFilters:setBundle basename="properties.labels.sales.GmSalesFilters"/>	
			
<%
	/*********************************
	 * Reason for Modification
	 *********************************/
	HashMap hmSalesFilters = new HashMap();
	HashMap hmFilterAccess = new HashMap();
	ArrayList alRegion = new ArrayList();
	ArrayList alDistributor = new ArrayList();
	ArrayList alTerritory = new ArrayList();
	ArrayList alPdtGroup = new ArrayList();
	ArrayList alZone = new ArrayList();
	ArrayList alComp = new ArrayList();
	ArrayList alDiv = new ArrayList();
	ArrayList alZonesByCompDiv = new ArrayList();
	ArrayList alRegnByZone = new ArrayList();
	ArrayList alCompCurrTyp = new ArrayList();
    ArrayList alTemp = new ArrayList();

	String strFilterType = GmCommonClass.parseNull(request.getParameter("FILTER"));
	String strFormName = GmCommonClass.parseNull(request.getParameter("FRMNAME"));
	String strHaction = GmCommonClass.parseNull(request.getParameter("HACTION"));
	String strUrl = GmCommonClass.parseNull(request.getParameter("URL"));

	String strDeptId = (String) session.getAttribute("strSessDeptId") == null? "": (String) session.getAttribute("strSessDeptId");
	int intAccessLvl = Integer.parseInt(GmCommonClass.parseZero((String) session.getAttribute("strSessAccLvl")));
	String strPrimaryCmpyId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));
	String strBBASalesAcsFl = GmCommonClass.parseNull((String) session.getAttribute("strSessBBASalesAcsFl"));
	
	if (strDeptId.equals("S")&& (intAccessLvl == 1 || intAccessLvl == 2 || intAccessLvl == 7)) {
		strFilterType = "TYPE1";
	} else if (strDeptId.equals("S") && (intAccessLvl == 3)) {
		strFilterType = "TYPE2";
	} else if (strDeptId.equals("S")&& (intAccessLvl == 4 || intAccessLvl == 6)) {
		strFilterType = "TYPE3";
	} else if (!strDeptId.equals("S")|| (strDeptId.equals("S") && (intAccessLvl == 5))) {
		strFilterType = "TYPE4";
	}
	strFilterType = "TYPE4";
	String strRegnFilter = (String) request.getAttribute("hRegnFilter") == null? "": (String) request.getAttribute("hRegnFilter");
	String strDistFilter = (String) request.getAttribute("hDistFilter") == null? "": (String) request.getAttribute("hDistFilter");
	String strTerrFilter = "";
	String strPgrpFilter = "";
	String strCompFilter = (String) request.getAttribute("hCompFilter") == null? "": (String) request.getAttribute("hCompFilter");
	String strDivFilter = (String) request.getAttribute("hDivFilter") == null? "": (String) request.getAttribute("hDivFilter");
	String strZoneFilter = (String) request.getAttribute("hZoneFilter") == null? "": (String) request.getAttribute("hZoneFilter");
	String strGrpAllDiv= (String) request.getAttribute("strGrpAllDiv") == null? "": (String) request.getAttribute("strGrpAllDiv");
	String strGrpAllZone= (String) request.getAttribute("strGrpAllZone") == null? "": (String) request.getAttribute("strGrpAllZone");
	String strGrpAllRegn= (String) request.getAttribute("strGrpAllRegn") == null? "": (String) request.getAttribute("strGrpAllRegn");
	String strGrpDistUnChk = (String) request.getAttribute("hDistFilterUnChk") == null? "": (String) request.getAttribute("hDistFilterUnChk");
	String strGrpAllDist= (String) request.getAttribute("strGrpAllDist") == null? "": (String) request.getAttribute("strGrpAllDist");
	String strCurrType = GmCommonClass.parseNull((String) request.getAttribute("hCurrType"));
	String strFilterHide = GmCommonClass.parseNull((String) request.getParameter("HIDE"));
	String strFilterHideGo = GmCommonClass.parseNull((String) request.getParameter("HIDEBUTTON"));
	String strTerrDisplay = GmCommonClass.parseNull((String) request.getParameter("TERRITORIES"));
	strCompFilter = strCompFilter.equals("")? GmCommonClass.parseNull((String) session.getAttribute("strSessCompId")): strCompFilter;
	strDivFilter = strDivFilter.equals("")? GmCommonClass.parseNull((String) session.getAttribute("strSessDivId")): strDivFilter;
	String strHiddenCompanyfl = GmCommonClass.parseNull((String) request.getParameter("hideCompany"));
	//PMT-33183 QUOTA PERFORMANCE SALES REPORT
	strHiddenCompanyfl = strHiddenCompanyfl.equals("")?GmCommonClass.parseNull((String) request.getAttribute("hideCompany")):strHiddenCompanyfl;
	

	strCurrType = strCurrType.equals("")?GmCommonClass.parseNull((String) session.getAttribute("strSessCurrType")) : strCurrType;
	String strCompFilterNm = GmCommonClass.parseNull((String) session.getAttribute("COMPANYNM"));
	//String strDivFilterNm = GmCommonClass.parseNull((String) session.getAttribute("DIVISIONNM"));

	hmSalesFilters = (HashMap) request.getAttribute("hmSalesFilters");
	if (hmSalesFilters != null) {
		alComp = (ArrayList) hmSalesFilters.get("COMP");
		alDiv = (ArrayList) hmSalesFilters.get("DIV");
		alZone = (ArrayList) hmSalesFilters.get("ZONE");
		alRegion = (ArrayList) hmSalesFilters.get("REGN");
		alDistributor = (ArrayList) hmSalesFilters.get("DIST");
		alTerritory = (ArrayList) hmSalesFilters.get("TERR");
		alPdtGroup = (ArrayList) hmSalesFilters.get("PGRP");
		alZonesByCompDiv = (ArrayList) hmSalesFilters.get("ZONEBYCOMPDIV");
		alRegnByZone = (ArrayList) hmSalesFilters.get("REGNBYZONE");
		alCompCurrTyp  = (ArrayList) hmSalesFilters.get("CURRTYP");
		//hmFilterAccess = (HashMap)hmSalesFilters.get("FilterAccess");

	}
 
	// Loading the company dropdown based on the users.
	// If the users primary company is 1001(BBA) then loading ALL(1008003) and BBA(400643) only into the Company Dropdown.
	// Else if the user having the access to see BBA sales load Company dropdown with all the values.
	// Else if the user is not belongs to BBA and also not having the access to view BBA sale then load company dropdown without BBA(400643)
    for (int i = 0; i < alComp.size(); i++) {
        HashMap hmTemp = GmCommonClass.parseNullHashMap((HashMap) alComp.get(i));
        String strCodeId = GmCommonClass.parseNull((String) hmTemp.get("CODEID"));
        if (strPrimaryCmpyId.equals("1001")) {
        	if (strCodeId.equals("100803") || strCodeId.equals("4000643")) {
          		alTemp.add(alComp.get(i));
        	}
        } else if(strBBASalesAcsFl.equals("Y")){
                alTemp.add(alComp.get(i));
        }else {
           if(!strCodeId.equals("4000643")){
                alTemp.add(alComp.get(i));
           }
        }
    }
	
    alComp.clear();
    alComp = GmCommonClass.parseNullArrayList((ArrayList) alTemp.clone());
	
	
	int intDistSize = 0;
	int intTerrSize = 0;
	int intZoneByCompDiv = 0;
	int intRegnByZone = 0;
	int intDiv = 0;
	int intComp = 0;

	if (alDistributor != null) {
		intDistSize = alDistributor.size();
	}
	if (alTerritory != null) {
		intTerrSize = alTerritory.size();
	}

	if (alZonesByCompDiv != null) {
		intZoneByCompDiv = alZonesByCompDiv.size();
	}
	if (alRegnByZone != null) {
		intRegnByZone = alRegnByZone.size();
	}
	if (alDiv != null) {
		intDiv = alDiv.size();
	}
	if (alComp != null) {
		intComp = alComp.size();
	}
	HashMap hmLoop = new HashMap();
					
	
	// PMT-25353 : Sales More filete taking long time to load issue fix
	// to remove the empty line - So, that memory will be reduce
	
	String strTerritoryHtml = "";
	String strDistHtml = "";
	String strZoneHtml = "";
	String strRegionHtml = "";
	String strDivHtml = "";

	// Div

	hmLoop = new HashMap();
	if (intDiv > 0) {
	  for (int i = 0; i < intDiv; i++) {
	    hmLoop = (HashMap) alDiv.get(i);
	    strDivHtml +=
	        "DivArr[" + i + "] = new Array(\"" + hmLoop.get("ID") + "\",\"" + hmLoop.get("NM")
	            + "\",\"" + hmLoop.get("COMPID") + "\",\"" + hmLoop.get("COMPNAME") + "\");";
	  }
	}

	// Zone
	
	hmLoop = new HashMap();
	if (intZoneByCompDiv > 0) {
	  for (int i = 0; i < intZoneByCompDiv; i++) {
	    hmLoop = (HashMap) alZonesByCompDiv.get(i);
	    strZoneHtml +=
	        "ZAr[" + i + "] = new Array(\"" + hmLoop.get("ID") + "\",\"" + hmLoop.get("NM")
	            + "\",\"" + hmLoop.get("COMPDIVID") + "\",\"" + hmLoop.get("COMPID") + "\");";
	  }
	}
	
	// Region
	
	hmLoop = new HashMap();
	if (intRegnByZone > 0) {
	  for (int i = 0; i < intRegnByZone; i++) {
	    hmLoop = (HashMap) alRegnByZone.get(i);
	    strRegionHtml +=
	        "RAr[" + i + "] = new Array(\"" + hmLoop.get("ID") + "\",\"" + hmLoop.get("NM")
	            + "\",\"" + hmLoop.get("ZONEID") + "\",\"" + hmLoop.get("COMPID") + "\");";
	  }
	}

	// Distributor
	
	if (intDistSize > 0) {
	  for (int i = 0; i < intDistSize; i++) {
	    hmLoop = (HashMap) alDistributor.get(i);
	    strDistHtml +=
	        "DAr[" + i + "] = new Array(\"" + hmLoop.get("ID") + "\",\"" + hmLoop.get("NM")
	            + "\",\"" + hmLoop.get("PID") + "\",\"" + hmLoop.get("COMPID") + "\");";

	  }
	}

	if(strTerrDisplay.equals("DISPLAY")){
	  
	  // When Territory check box available then only get the System values
	  strTerrFilter = GmCommonClass.parseNull((String) request.getAttribute("hTerrFilter"));
		// Terrirtory 

		hmLoop = new HashMap();
		if (intTerrSize > 0) {
		  for (int i = 0; i < intTerrSize; i++) {
		    hmLoop = (HashMap) alTerritory.get(i);
		    strTerritoryHtml +=
		        "TAr[" + i + "] = new Array(\"" + hmLoop.get("ID") + "\",\"" + hmLoop.get("NM")
		            + "\",\"" + hmLoop.get("PID") + "\",\"" + hmLoop.get("COMPID") + "\");";
		  }
		}

	  
	} // end strTerrDisplay
	
	
	// When System checkbox available then only get the System values 
	if(!strFilterHide.equals("SYSTEM")){
	  
	  strPgrpFilter = GmCommonClass.parseNull((String) request.getAttribute("hPgrpFilter"));
	  
	}
	
				%>
<script>
	var vFilterShowFl = false;
	
	// when expend more filter time we set the flag.
	//based on the flag - to avoid reset the hidden filed 
	var expendMoreFilterFl = false;

	var TotalRows = 0;
	var DAr = new Array(1);
	var TAr = new Array(1);
	var ZAr = new Array(1);
	var DivArr = new Array(1);
	var RAr = new Array(1);
	var FilterType = '<%=strFilterType%>'

	var regn = '<%=strRegnFilter%>';
	var dist = '<%=strDistFilter%>';
	var terr = '<%=strTerrFilter%>';
	var pgrp = '<%=strPgrpFilter%>';
	var comp = '<%=strCompFilter%>';
	var div = '<%=strDivFilter%>';
	var zone = '<%=strZoneFilter%>';	
	var divAll= '<%=strGrpAllDiv%>';
	var zoneAll= '<%=strGrpAllZone%>';
	var regnAll= '<%=strGrpAllRegn%>';
	var distAll= '<%=strGrpAllDist%>';
	var distUnChk = '<%=strGrpDistUnChk%>';

	var frmNm = '<%= strFormName%>';
    var frmObj = eval("document."+frmNm);
    
    var currType = '<%=strCurrType%>';
    var deptid = '<%=strDeptId%>';
    var acess = '<%=intAccessLvl%>';
	
    <%=strDivHtml%>
    
    <%=strZoneHtml%>
    
    <%=strRegionHtml%>
    
    <%=strDistHtml%>

	<%=strTerritoryHtml%>
	
</script>
<script src="<%=strJsPath%>/sales/GmSalesFilter.js"></script>
<script src="<%=strJsPath%>/sales/GmFlashinglinks.js"></script>
<input type="hidden" name="hCookieFlag" value="">
<input type="hidden" name="hDivInnerHtml" value="">
<input type="hidden" name="hZoneInnerHtml" value="">
<input type="hidden" name="hRegnInnerHtml" value="">
<input type="hidden" name="hDistInnerHtml" value="">
<input type="hidden" name="hTerrInnerHtml" value="">
<input type="hidden" name="hAcctInnerHtml" value="">
<input type="hidden" name="hPgrpInnerHtml" value="">

<input type="hidden" name="hZoneFilter" value="<%=strZoneFilter%>">
<input type="hidden" name="hRegnFilter" value="<%=strRegnFilter%>">
<input type="hidden" name="hDistFilter" value="<%=strDistFilter%>">
<input type="hidden" name="hTerrFilter" value="<%=strTerrFilter%>">
<input type="hidden" name="hPgrpFilter" value="<%=strPgrpFilter%>">
<input type="hidden" name="hCompFilter" value="<%=strCompFilter%>">
<input type="hidden" name="hDivFilter" value="<%=strDivFilter%>">
<input type="hidden" name="hCurrType" value="<%=strCurrType%>">
<input type="hidden" name="hideCompany" value="<%=strHiddenCompanyfl%>">
<input type="hidden" name="hDistFilterUnChk" value="<%=strGrpDistUnChk%>">
<%
if (!strFilterType.equals("TYPE1") || (strFilterType.equals("TYPE1") && !strFilterHide.equals("SYSTEM")))
{
%>
<%
if (!strDistFilter.equals("") || !strZoneFilter.equals("") || !strRegnFilter.equals(""))
{%>
	<a href="javascript:fnShowFilters();" class="RightText" rel="flashfg[red]"><fmtSalesFilters:message key="LBL_MOREFILTERS"/></a>
	>>
<%} else {%>
	<a href="javascript:fnShowFilters();" class="RightText"><fmtSalesFilters:message key="LBL_MOREFILTERS"/></a>
	>>
<%}%>

<!-- Custom tag lib code modified for JBOSS migration changes -->

<table class="Border"  cellspacing="0">
	<tr>
		<td>

	<table id="tabFilter" style="display:none" cellspacing="0">
		<tr height="18" bgcolor='<%=strFilterType.equals("TYPE4")?"":"#eeeeee"%>' class="RightTableCaption">
<%
		if(strFilterType.equals("TYPE1"))
		{
%>		
			<gmjsp:label type="BoldText" align="left" SFLblControlName="&nbsp;Systems&nbsp;" td="true"/>
			<%
			if(strTerrDisplay.equals("DISPLAY")){
%>
			<gmjsp:label type="BoldText"  align="left" SFLblControlName="&nbsp;Territories&nbsp;" td="true"/>
<%
			}
	
%>	
<%
		}else if(strFilterType.equals("TYPE2"))
		{
%>		
			<gmjsp:label type="BoldText" align="left" SFLblControlName="&nbsp;Field&nbsp;Sales&nbsp;" td="true"/>
<%
			if(strTerrDisplay.equals("DISPLAY")){
%>
			<gmjsp:label type="BoldText"  align="left" SFLblControlName="&nbsp;Territories&nbsp;" td="true"/>
<%
			}
			if(!strFilterHide.equals("SYSTEM")){
%>	
			<gmjsp:label type="BoldText" align="left"  SFLblControlName="&nbsp;Systems&nbsp;" td="true"/>
<%
			}
%>
<%
		}else if(strFilterType.equals("TYPE3"))
		{
%>		
			
			<gmjsp:label type="BoldText" align="left" SFLblControlName="&nbsp;Regions&nbsp;" td="true"/>
			<gmjsp:label type="BoldText" align="left" SFLblControlName="&nbsp;Field&nbsp;Sales&nbsp;" td="true"/>
<%
			if(strTerrDisplay.equals("DISPLAY")){
%>
			<gmjsp:label type="BoldText"  align="left" SFLblControlName="&nbsp;Territories&nbsp;" td="true"/>
<%
			}
			if(!strFilterHide.equals("SYSTEM")){
%>	
				<gmjsp:label type="BoldText" align="left" SFLblControlName="&nbsp;Systems&nbsp;" td="true"/>
<%
			}
%>
<%
		}else if(strFilterType.equals("TYPE4"))
		{
%>		
			<td>
				<table border="0" cellspacing="0" cellpadding="0">
						<tr> 
							<td colspan="1">
							<fmtSalesFilters:message key="LBL_COMPANY" var="varCompany"/>
							<gmjsp:label type="BoldText"  SFLblControlName="&nbsp;${varCompany}:&nbsp;" td="false"/>							
								<gmjsp:dropdown controlName="Comp" seletedValue="<%=strCompFilter %>"
								value="<%=alComp%>" codeId="CODEID" codeName="CODENM"
								onChange="fnLoadDiv(this);" tabIndex="1" disabled="false" />
							</td>
					 <% if (strPrimaryCmpyId.equals("1000") && (!strDeptId.equals("S"))&&(strHiddenCompanyfl.equals("Y"))) { %> 
							<td colspan="1">
							<fmtSalesFilters:message key="LBL_REPORTING_CURRENCY" var="varReportingCurrency"/>
							<gmjsp:label type="BoldText"  SFLblControlName="&nbsp;${varReportingCurrency}:&nbsp;" td="false"/>							
								<gmjsp:dropdown controlName="CurrType" seletedValue="<%=strCurrType%>"
								value="<%=alCompCurrTyp%>" codeId="CODEID" codeName="CODENM"
								 tabIndex="1" disabled="false"/>
							</td>
					<% } %> 
						</tr>
						<tr bgcolor="#eeeeee">
							<fmtSalesFilters:message key="LBL_DIVISION" var="varDivision"/>
							<fmtSalesFilters:message key="LBL_ZONE" var="varZone"/>
							<td class="RightTableCaption"><input type="checkbox" name="Chk_Grp_ALL_Div" value="Y" onClick="javascript:fnLoadAllCont('Div')"/>
							<gmjsp:label type="BoldText" align="left"  SFLblControlName="&nbsp;${varDivision}&nbsp;" td="false" /></td>
							<td class="RightTableCaption"><input type="checkbox" name="Chk_Grp_ALL_Zone" value="Y" onClick="javascript:fnLoadAllCont('Zone')"/>
							<gmjsp:label type="BoldText" align="left"  SFLblControlName="&nbsp;${varZone}&nbsp;" td="false"/></td>
<%
					if(strTerrDisplay.equals("DISPLAY") && !strFilterHide.equals("SYSTEM")){
%>								
							<fmtSalesFilters:message key="LBL_REGION" var="varRegions"/>
							<td class="RightTableCaption"><input type="checkbox" name="Chk_Grp_ALL_Regn" value="Y" onClick="javascript:fnLoadAllCont('Regn')"/>
							<gmjsp:label type="BoldText"  align="left" SFLblControlName="&nbsp;${varRegions}&nbsp;" td="false"/></td>
<%
					}
%>
						</tr>
						<tr>
							<td width="20"><DIV class='gmFilter' id='Div_Div'  ><ul style="list-style-type: none; padding: 0; margin: 0;"><li><input class='RightInput' type='checkbox' name='Chk_GrpDiv'></li></ul></DIV></td>
							<td><DIV class='gmFilter' id='Div_Zone'  ><ul style="list-style-type: none; padding: 0; margin: 0;"><li><input class='RightInput' type='checkbox' name='Chk_GrpZone'></li></ul></DIV></td>
<%
					if(strTerrDisplay.equals("DISPLAY") && !strFilterHide.equals("SYSTEM")){
%>
						<td><DIV class='gmFilter' id='Div_Regn'><ul style="list-style-type: none; padding: 0; margin: 0;"><li><input class='RightInput' type='checkbox' name='Chk_GrpRegn'></li></ul></DIV></td>
<%
					}
%>
						</tr>
				</table>
			</td>			
<%
		}
%>
			<% String StrReloadSales = "fnReloadSales('"+strHaction+"','"+strUrl+"')"; %>
			<td bgcolor="white" width="100%" rowspan="2" valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;
			<fmtSalesFilters:message key="BTN_RESET" var="varReset"/>
				<gmjsp:button value="${varReset}" gmClass="Button" buttonType="Load" onClick="fnResetSales();" tabindex="2" />&nbsp;&nbsp;
				<%
			if(!strFilterHideGo.equals("HIDEGO")){
				%>
				<fmtSalesFilters:message key="BTN_GO" var="varGo"/>
				<gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" gmClass="Button" buttonType="Load" onClick="<%=StrReloadSales%>" tabindex="2" />
				<%
							}
				%>
			</td>
		</tr>
		<tr>
<%
		if(strFilterType.equals("TYPE1"))
		{
%>		
				<td id ="PG"><%=GmCommonControls.getChkBoxGroup("",alPdtGroup,"Pgrp")%></td>
<%
			if(strTerrDisplay.equals("DISPLAY")){
%>
			<td><%=GmCommonControls.getChkBoxGroup("fnLoadAcct(this)",alTerritory,"Terr")%></td>
<%
			}			
%>			
<%
		}else if(strFilterType.equals("TYPE2"))
		{
%>		
			<td><DIV class='gmFilter' id='Div_Dist'><ul style="list-style-type: none; padding: 0; margin: 0;"><li><input class='RightInput' type='checkbox' name='Chk_GrpDist'></li></ul></DIV></td>
<%
			if(strTerrDisplay.equals("DISPLAY")){
%>
			<td><%=GmCommonControls.getChkBoxGroup("fnLoadAcct(this)",alTerritory,"Terr")%></td>
<%
			}
			if(!strFilterHide.equals("SYSTEM")){
%>	
				<td id ="PG"><%=GmCommonControls.getChkBoxGroup("",alPdtGroup,"Pgrp")%></td>
<%
			}
%>
<%
		}else if(strFilterType.equals("TYPE3"))
		{
%>		
			<td><DIV class='gmFilter' id='Div_Regn'><ul style="list-style-type: none; padding: 0; margin: 0;"><li><input class='RightInput' type='checkbox' name='Chk_GrpRegn'></li></ul></DIV></td>
			<td><DIV class='gmFilter' id='Div_Dist'><ul style="list-style-type: none; padding: 0; margin: 0;"><li><input class='RightInput' type='checkbox' name='Chk_GrpDist'></li></ul></DIV></td>
<%
			if(strTerrDisplay.equals("DISPLAY")){
%>
			<td><%=GmCommonControls.getChkBoxGroup("fnLoadAcct(this)",alTerritory,"Terr")%></td>
<%
			}
			if(!strFilterHide.equals("SYSTEM")){
%>				
				<td id ="PG"><%=GmCommonControls.getChkBoxGroup("",alPdtGroup,"Pgrp")%></td>
<%
			}
%>
<%
		}else if(strFilterType.equals("TYPE4"))
		{
%>		
			<td>
				<table border="0" cellspacing="0" cellpadding="0" >
					<tr bgcolor="#eeeeee">
<%
					if(!(strTerrDisplay.equals("DISPLAY") && !strFilterHide.equals("SYSTEM"))){
%>						
						<fmtSalesFilters:message key="LBL_REGION" var="varRegions"/>
						<td class="RightTableCaption"><input type="checkbox" name="Chk_Grp_ALL_Regn" value="Y" onClick="javascript:fnLoadAllCont('Regn')"/>
							<gmjsp:label type="BoldText"  align="left" SFLblControlName="&nbsp;${varRegions}&nbsp;" td="false"/></td>
					<%
					}
%>
						<fmtSalesFilters:message key="LBL_FIELD_SALES" var="varFieldSales"/>
							<td class="RightTableCaption"><input type="checkbox" name="Chk_Grp_ALL_Dist" value="Y" onClick="javascript:fnLoadAllCont('Dist')"/>
						<gmjsp:label type="BoldText"  align="left" SFLblControlName="&nbsp;${varFieldSales}&nbsp;" td="false"/></td>
<%
					if(strTerrDisplay.equals("DISPLAY")){
%>
						<fmtSalesFilters:message key="LBL_TERRITORIES" var="varTerritories"/>
						<gmjsp:label type="BoldText"  align="left" SFLblControlName="&nbsp;${varTerritories}&nbsp;" td="true"/>
<%
					}
					if(!strFilterHide.equals("SYSTEM")){
%>	
							<fmtSalesFilters:message key="LBL_SYSTEMS" var="varSystem"/>
							<gmjsp:label type="BoldText"  align="left" SFLblControlName="&nbsp;${varSystem}&nbsp;" td="true"/>
<%
						}
%>
					</tr>
					<tr>
<%
					if(!(strTerrDisplay.equals("DISPLAY") && !strFilterHide.equals("SYSTEM"))){
%>
						<td><DIV class='gmFilter' id='Div_Regn'  ><ul style="list-style-type: none; padding: 0; margin: 0;"><li><input class='RightInput' type='checkbox' name='Chk_GrpRegn'></li></ul></DIV></td>
<%
					}
%>
						<td><DIV class='gmFilter' id='Div_Dist'  ><ul style="list-style-type: none; padding: 0; margin: 0;"><li><input class='RightInput' type='checkbox' name='Chk_GrpDist'></li></ul></DIV></td>
						
<%
						if(strTerrDisplay.equals("DISPLAY")){
%>
							<td><%=GmCommonControls.getChkBoxGroup("fnLoadAcct(this)",alTerritory,"Terr")%></td>
<%
						}
						if(!strFilterHide.equals("SYSTEM")){
%>						
							<td id ="PG"><%=GmCommonControls.getChkBoxGroup("",alPdtGroup,"Pgrp")%></td>
<%
						}
%>
					</tr>
				</table>
			</td>
<%
		}else{
%>			
			<td></td>
<%
		}
%>		

		</tr>
	</table>
</td>
</tr>
</table>
<%} %>
<script>
	if (document.all.Div_Div){
		frmObj.hDivInnerHtml.value = document.all.Div_Div.innerHTML;
	}
	if (document.all.Div_Zone){
		frmObj.hZoneInnerHtml.value = document.all.Div_Zone.innerHTML;
	}
	if (document.all.Div_Regn){
		frmObj.hRegnInnerHtml.value = document.all.Div_Regn.innerHTML;
	}
	if (document.all.Div_Dist){
		frmObj.hDistInnerHtml.value = document.all.Div_Dist.innerHTML;
	}
	if (document.all.Div_Terr){
		frmObj.hTerrInnerHtml.value = document.all.Div_Terr.innerHTML;
	}
	if (document.all.Div_Pgrp){
		frmObj.hPgrpInnerHtml.value = document.all.Div_Pgrp.innerHTML;
	}
	
	 
</script>
