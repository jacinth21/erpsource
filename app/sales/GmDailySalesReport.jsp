<%@ taglib uri="com.corda.taglib" prefix="ctl" %>
 <%
/**********************************************************************************
 * File		 		: GmDailySalesReport.jsp
 * Desc		 		: This screen is used for the Account Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
 <%@page import="java.util.Date"%>
<%@page import="com.globus.valueobject.common.GmDataStoreVO"%> 
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtDailySalesReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\GmDailySalesReport.jsp -->

<fmtDailySalesReport:setLocale value="<%=strLocale%>"/>
<fmtDailySalesReport:setBundle basename="properties.labels.sales.GmDailySalesReport"/>



<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strSalesDashBoardJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_DASHBOARD");
	String strTimeZone = strGCompTimeZone;
	String strApplDateFmt =strGCompDateFmt;
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	ArrayList alReturn = (ArrayList)hmReturn.get("DAILYSALES");
	String strType = (String)request.getAttribute("hType")==null?"":(String)request.getAttribute("hType");
	java.util.Date dtFrmDate=(java.util.Date)request.getAttribute("hFrom");
	String strRegnFilter = GmCommonClass.parseNull((String)request.getAttribute("hUSFilter"));
	
	ArrayList alRegion=(ArrayList)request.getAttribute("alRegion");
	ArrayList alColorsList = new ArrayList();
	String strOthersCode = "";
	String strLegendsCnt = "";
	
	
	int intLength = alReturn.size();
	
	GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.sales.GmDailySalesReport", strSessCompanyLocale);


	String strGraphTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_DAILY_SALES_REPORT"));

	String strWikiTitle = GmCommonClass.parseNull((String)GmCommonClass.getWikiTitle("DAILY_SALES"));
	int intSize = 0;
	HashMap hmLoop = new HashMap();

	String strId = "";
	String strName = "";
	String strDistId = "";
	String strDistName = "";
	String strPrice = "";

	String strShade = "";
	String strNextId = "";
	int intCount = 0;
	String strLine = "";
	String strTotal = "";
	String strAcctTotal = "";
	
	boolean blFlag = false;
	double dbSales = 0.0;
	double dbAcctTotal = 0.0;
	double dbTotal = 0.0;
	
	ArrayList alDist = new ArrayList();
	ArrayList alSales = new ArrayList();
	ArrayList alDate = new ArrayList();

	alColorsList = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALCOLORSLIST"));
	strOthersCode = GmCommonClass.parseNull((String)hmReturn.get("STROTHERSCOLORCODE"));
	strLegendsCnt = GmCommonClass.parseNull((String)hmReturn.get("STRLEGENDSCNT"));

	String strDeptId = (String)session.getAttribute("strSessDeptId")==null?"":(String)session.getAttribute("strSessDeptId");
	String strAccessLvl = (String)session.getAttribute("strSessAccLvl")==null?"":(String)session.getAttribute("strSessAccLvl");
	int intAccessLvl = Integer.parseInt(strAccessLvl);
	boolean bolAccess = true;
	if ((strDeptId.equals("S") && intAccessLvl < 5))
	{
		bolAccess = false;
	}
	String strDate = GmCommonClass.getStringFromDate(dtFrmDate,strGCompDateFmt); 
    String strDoughnutData =GmCommonClass.parseNull((String) request.getAttribute("DOUGHNUTJSON"));
	String strCurrSign = GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb"));
	String strFCExportServer = GmCommonClass.parseNull((String) GmCommonClass.getString("FUSIONCHART_EXPORT_SERVER"));   
	//String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));  
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Sales Report </TITLE>

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css"></link>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/fusionchart/fusioncharts.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script type="text/javascript"	src="<%=strSalesDashBoardJsPath%>/GmDailySalesReport.js"></script>
<style type="text/css">
.fa-rectangle {
  font-size: inherit;
   width: 7px;
}
</style>

<script>

var doughnutDataArray = eval(<%=strDoughnutData%>);
var doughnutTotalSales = doughnutDataArray.totalamt;
doughnutDataArray = eval(doughnutDataArray.data);
var date = '<%=strDate%>';
var currSymbol = '<%=strCurrSign%>';
var fcExportServer = '<%=strFCExportServer%>';
var caption = message_sales[298]+""+date;
var width ='650';
var dateFormat = '<%=strApplDateFmt%>';

function fnSubmit()
{
	//Since Date is mandatory, Date validation is added.
	var frmDate = TRIM(document.frmMain.hFromDate.value);
    if(frmDate == ''){
    	Error_Details(message_sales[190]);
    }else{
    	CommonDateValidation(document.frmMain.hFromDate, dateFormat, Error_Details_Trans(message_sales[191],dateFormat));
    }	
	if (ErrorCount > 0){
		Error_Show();
	    Error_Clear();
		return false;
	}
	
	fnStartProgress('Y');
	document.frmMain.submit();
}
function formatCurrency(num,decimalplace) {
	decimalplace = (decimalplace=='')?0:decimalplace;
	return currSymbol + parseFloat(num).toFixed(decimalplace).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function fnLoad(){
	fnDoughnut();
}
function Toggle(val)
{
	
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className="";
		tabobj.style.background="#ffffff";
	}
}
function fnClose(){
	parent.dhxWins.window("popupWindow").close();
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad()">
<FORM name="frmMain" method="POST" action="<%=strServletPath%>/GmDetailedSalesServlet">
<input type="hidden" name="hType" value="<%=strType%>">
<input type="hidden" name="hAction" value="">
<%if(strClientSysType.equals("IPAD")){ %>
<div style="width: 100%; height: 100%; overflow:auto">
<%}%>

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table class="border" width="650" cellspacing="0" cellpadding="0">
		<tr>
			<td width="650" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" class="RightDashBoardHeader" colspan="2">
							<fmtDailySalesReport:message key="LBL_DAILY_SALES_REPORT"/>
						</td>
						<td height="25" class="RightDashBoardHeader">
						<fmtDailySalesReport:message key="IMG_ALT_HELP" var="varHelp"/>
						<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
						</td>
					</tr>
					<tr><td class="Line" colspan="3"></td></tr>
					<tr>
						<td height="25" class="RightTextBlue" colspan="3">&nbsp;
						<table border="0" align="left" cellspacing="0" cellpadding="0"><tr>					
						<td>&nbsp;&nbsp;&nbsp;<fmtDailySalesReport:message key="LBL_CHOOSE_DATE" var="varChDate"/><gmjsp:label type="MandatoryText"  SFLblControlName="${varChDate}" td="false" />&nbsp;</td>
						<td><gmjsp:calendar textControlName="hFromDate" textValue="<%=new java.sql.Date(dtFrmDate.getTime()) %>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;&nbsp;&nbsp;</td>
						<td><fmtDailySalesReport:message key="BTN_GO" var="varGo"/><gmjsp:button value="&nbsp;${varGo}&nbsp;" name="Btn_Go" gmClass="button" buttonType="Load"  onClick="javascript:fnSubmit();" /></td></tr>
						</table>
						</td>
					</tr>
<%
			if (bolAccess && !strType.equals("SalesDashDetail")){
%>		
		<tr>
			<td colspan="3" height="20">
			<jsp:include page="/sales/GmSalesFilters.jsp" >
					<jsp:param name="HIDE" value="SYSTEM" />
					<jsp:param name="FRMNAME" value="frmMain" />
			</jsp:include>
			</td>
		</tr>		
<%
			}
%>					
					<tr><td class="Line" colspan="3"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="200"><fmtDailySalesReport:message key="LBL_FIELD_SALES"/></td>
						<td width="450"><fmtDailySalesReport:message key="LBL_ACCOUNT_NAME"/></td>
						<td width="150"><fmtDailySalesReport:message key="LBL_AMOUNT"/></td>
					</tr>
<%
						if (intLength > 0)
						{
							HashMap hmTempLoop = new HashMap();
							int colorCnt =0;
							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alReturn.get(i);
								if (i<intLength-1)
								{
									hmTempLoop = (HashMap)alReturn.get(i+1);
									strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("DID"));
								}

								strDistId = GmCommonClass.parseNull((String)hmLoop.get("DID"));
								strDistName = GmCommonClass.parseNull((String)hmLoop.get("DNAME"));
								strId	= GmCommonClass.parseNull((String)hmLoop.get("ID"));
								strName = GmCommonClass.parseNull((String)hmLoop.get("NAME"));
								strPrice = GmCommonClass.parseZero((String)hmLoop.get("SALES"));

								dbSales = Double.parseDouble(strPrice);
								dbAcctTotal = dbAcctTotal + dbSales;
								dbTotal = dbTotal + dbSales;
								if (strDistId.equals(strNextId))
								{
									intCount++;
									strLine = "<TR><TD colspan=3 height=1 class=Line></TD></TR>";
								}
								else
								{
									strLine = "<TR><TD colspan=3 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
										strDistName = "";
										strLine = "";
									}
									else
									{
										blFlag = true;
									}
									intCount = 0;
									
								}
								strAcctTotal = ""+dbAcctTotal;

								if (intCount > 1)
								{
									strDistName = "";
									strLine = "";
								}
							
								strShade = (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows

								out.print(strLine);
								strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
								if (intCount == 1 || blFlag)
								{
									alDist.add(strDistName);
									String strLegendColor = "";
									if(colorCnt < Integer.parseInt(strLegendsCnt) ){
									 	strLegendColor = GmCommonClass.parseNull((String)alColorsList.get(colorCnt++));
									}else{
									  	strLegendColor = strOthersCode;
									}	
%>
					<tr id="tr<%=strDistId%>">
						<td colspan="2" height="20" class="RightText">&nbsp;<span class="fa-rectangle" style="background-color:<%=strLegendColor%>; " ></span>
						<a class="RightText" title="Click to Expand" href="javascript:Toggle('<%=strDistId%>')"><%=strDistName%></a></td>
						<td class="RightText" id="td<%=strDistId%>" align="right"></td>
					</tr>
					<tr>
						<td colspan="5"><div style="display:none" id="div<%=strDistId%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tab<%=strDistId%>">
<%
								blFlag = false;
								}
%>
					<tr <%=strShade%>>
						<td width="200" class="RightText" height="20">&nbsp;</td>
						<td width="450" class="RightText">&nbsp;<%=strName%></td>
						<gmjsp:currency type="CurrText"  textValue="<%=strPrice%>" />
					</tr>
					<tr><td colspan="3" bgcolor="#cccccc"></td></tr>
<%					
					if ( intCount == 0 || i == intLength-1)
					{
						alSales.add(GmCommonClass.getStringWithCommas(strAcctTotal));
%>
							</table></div>
						</td>
					</tr>
					<script>
						document.all.td<%=strDistId%>.innerHTML = " <gmjsp:currency type="CurrTextSign"  textValue="<%=strAcctTotal%>" td="false"/>&nbsp";
					</script>
<%
						dbAcctTotal = 0.0;					
					}
				strTotal = ""+GmCommonClass.roundDigit(dbTotal,2);
				}// end of FOR
				//below for loop for avoid calendar popup window hide behind the chart issue
				for(int i=0;i<(5-intLength);i++){
%>
				<tr><td colspan="3">&nbsp;</td></tr>

<%} %>
					<tr><td colspan="3" class="Line"></td></tr>
					<tr>
						<td align="right" class="RightTableCaption" height="20" colspan="2"><fmtDailySalesReport:message key="LBL_GRAND_TOTAL"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;<gmjsp:currency  type="CurrTextSign"  textValue="<%=strTotal%>"/>
						<input type="hidden" name="hGrandTotal" value="<%=strTotal%>"></td>
					</tr>
					<tr><td colspan="3" class="Line"></td></tr>
					<tr>
						<td colspan="3" align="center">
						<div id="divDonutContainer"></div>
						</td>
					</tr>
<%
						} // End of IF
						else{
%>
					<tr><td colspan="3" class="Line"></td></tr>					
					<tr><td colspan="3" class="RightTextRed" height="50" align=center><fmtDailySalesReport:message key="LBL_NO_DATA"/> </td></tr>
<%					
						}
%>
				</table>
			</td>
		</tr>
<%
if(strType.equals("SalesDashDetail")){
%>
					<tr><td colspan="3" class="Line"></td></tr>	
					<tr height="30"><td colspan="3" align="center"><fmtDailySalesReport:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="&nbsp;${varClose}&nbsp;" buttonType="Load" onClick="fnClose();"></gmjsp:button></td></tr>
<%} %>		
    </table>
<%if(strClientSysType.equals("IPAD")){ %>
</div>
<%}%>
</FORM>

<%@ include file="/common/GmFooter.inc" %>
<script>var total= '<%=strTotal%>';</script>
</BODY>

</HTML>
