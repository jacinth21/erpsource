 <%
/**********************************************************************************
 * File		 		: GmSalesReportYTDByAccountPer.jsp
 * Desc		 		: This screen is used to display Sales Rep By Account
 * Version	 		: 1.0
 * author			: RichardK
************************************************************************************/
%>
<%@ page language="java" %>
<%@include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtSalesReportMultiYTD" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!--sales\GmSalesReportMultiYTD.jsp  -->
<fmtSalesReportMultiYTD:setLocale value="<%=strLocale%>"/>
<fmtSalesReportMultiYTD:setBundle basename="properties.labels.sales.GmSalesReportMultiYTD"/>
<%
try {

	GmServlet gm = new GmServlet();
	String strWikiTitle = GmCommonClass.getWikiTitle("TERRITORY_SUMMARY_REPORT");
	HashMap hmReturn = new HashMap();
	
	ArrayList alProjList = new ArrayList();
	ArrayList alStyle 		= new ArrayList();
	ArrayList alDrillDown 	= new ArrayList();
	
	String strSelected = "";
	String strCodeID = "";
	String strProjNm = "";
	String strProjId = "";
	
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	
	gm.checkSession(response,session);

	String strShowArrow = GmCommonClass.parseZero((String)request.getAttribute("Chk_ShowArrowFl"));
	String strShowPercent = GmCommonClass.parseZero((String)request.getAttribute("Chk_ShowPercentFl"));
	String strHideAmount = GmCommonClass.parseZero((String)request.getAttribute("Chk_HideAmountFl"));
	
	// Combobox default value
	String strHeader = (String)request.getAttribute("strHeader");
	//String strProj = (String)request.getAttribute("Cbo_Proj");
	String strProj = (String)request.getAttribute("hSetNumber");

	
	
	String strType = (String)request.getAttribute("Cbo_Type");
	
	//Below code is to track down the visits on the report.
	String strUserId = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
	String strShUserName = GmCommonClass.parseNull((String)session.getAttribute("strSessShName"));
	String strUserName = strUserId + " - " + strShUserName;
	//Visit code ends

	strProj = (strProj == null)?"0":strProj; // If no value selected will set the combo box value to [Choose One]
	strProj = (strProj.equals("null"))?"0":strProj; // If no value selected will set the combo box value to [Choose One]
	strType = (strType == null)?"0":strType; // Setting combo box value
	String strhAction = (String)request.getAttribute("hAction");


	
	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}
	
	strhAction = (strhAction == null)?"Load":strhAction;
	
	// To get the Project Combo Box value information 
	HashMap hmComonValue = (HashMap)request.getAttribute("hmComonValue");
	if (hmComonValue != null)
	{
		alProjList = (ArrayList)hmComonValue.get("PROJECTS");
	}
		
	// To Generate the Crosstab report
	gmCrossTab.setHeader("Sales By Distributor By Month");
	
	gmCrossTab.setLinkType(strhAction);
	gmCrossTab.setLinkRequired(true);	// To Specify the link
	
	
	// To display percent changed information
	if (strShowPercent.equals("1"))
	{
		gmCrossTab.setReportType(gmCrossTab.CROSSTAB_SALE_PER);	
	}	
	if (strShowArrow.equals("1") )
	{
		gmCrossTab.setShowArrow(true);; // To Display % arrow information
	}

	if (strHideAmount.equals("1"))
	{
		gmCrossTab.setHideAmount(true);	
	}	
	// To specify if its unit or valye 
	gmCrossTab.setValueType(Integer.parseInt(strType));
	
	// Add Lines
	gmCrossTab.setColumnLineStyle("borderDark");
	gmCrossTab.addLine("Name");
	
	// Setting Display Parameter
	gmCrossTab.setAmountWidth(65);
	gmCrossTab.setNameWidth(220);
	gmCrossTab.setRowHeight(22);
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	
	// Page Body Style
	gmCrossTab.setGeneralHeaderStyle("aaTopHeader");
	gmCrossTab.addStyle("Name","ShadeMedGrayTD","aaTopHeader");
	gmCrossTab.addStyle("Total","ShadeMedBlueTD","ShadeDarkBlueTD") ;
	
	
	// Setting for hierarchy color
	alStyle.add("ShadeLightBlueTD");
	alStyle.add("ShadeLevelI");
	alStyle.add("ShadeLevelII");
	gmCrossTab.setMultiColumnStyle(alStyle);
		
	//Setting drilldown information 
	alDrillDown.add("Account"); // Maps to Account drilldown	
	alDrillDown.add("Group"); 	// Maps to Group drilldown	
	alDrillDown.add("Part"); 	// Maps to Part drilldown
	gmCrossTab.setDrillDownDetails(alDrillDown);
	gmCrossTab.displayZeros(true);

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: YTD #</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">

<script>

function fnSubmit()
{
	document.frmOrder.submit();
}

function fnCallAccount(val,Hierarchy )
{
	if (Hierarchy=='2') 
	{
		val = val.replace("L3","");	
		//The AD Level will have data like 100480_789528 [REGIONID_ADID], we are substring the data and pass only AD ID. 
		if(val.indexOf("_") != -1)
			val = val.substring(val.indexOf("_") +1);
		
		document.frmOrder.hAreaDirector.value = val;
	}
	if (Hierarchy=='1') 
	{
		val = val.replace("L2","");
		document.frmOrder.DistributorID.value = val;
	}
	if (Hierarchy=='0') 
	{
		val = val.replace("L1","");
		document.frmOrder.hTerritory.value = val;
	}
	
	//document.frmOrder.hAction.value = "LoadAccount";
	//document.frmOrder.submit();
	url='<%=strServletPath%>/GmSalesYTDReportServlet?&hAction=LoadAccount';
	fnReloadSales('',url);
}


function fnCallPart(val, Hierarchy)
{
	if (Hierarchy=='2') 
	{
		val = val.replace("L3","");	
		//The AD Level will have data like 100480_789528 [REGIONID_ADID], we are substring the data and pass only AD ID.
		if(val.indexOf("_") != -1)
			val = val.substring(val.indexOf("_") +1);
		
		document.frmOrder.hAreaDirector.value = val;
	}
	
	if (Hierarchy=='1') 
	{
		val = val.replace("L2","");
		document.frmOrder.DistributorID.value = val;
	}

	if (Hierarchy=='0') 
	{
		val = val.replace("L1","");
		document.frmOrder.hTerritory.value = val;
	}

	//document.frmOrder.hAction.value = "LoadPart";
	//document.frmOrder.submit();
	url='<%=strServletPath%>/GmSalesYTDReportServlet?&hAction=LoadPart';
	fnReloadSales('',url);
}

function fnCallSetNumber(val,Hierarchy )
{
	if (Hierarchy=='2') 
	{
		val = val.replace("L3","");
		
		//The AD Level will have data like 100480_789528 [REGIONID_ADID], we are substring the data and pass only AD ID.	
		if(val.indexOf("_") != -1)
			val = val.substring(val.indexOf("_") +1);
		
		document.frmOrder.hAreaDirector.value = val;
	}

	if (Hierarchy=='1') 
	{
		val = val.replace("L2","");	
		document.frmOrder.DistributorID.value = val;
	}

	if (Hierarchy=='0') 
	{
		val = val.replace("L1","");
		document.frmOrder.hTerritory.value = val;
	}

	//document.frmOrder.action ="<%=strServletPath%>/GmSetYTDReportServlet";
	//document.frmOrder.hAction.value = "LoadGroup";
	//document.frmOrder.submit();
	url='<%=strServletPath%>/GmSetYTDReportServlet?&hAction=LoadGroup&SetNumber=null';
	fnReloadSales('',url);
}

function fnGo(haction)
{
	document.frmOrder.SetNumber.value = document.frmOrder.Cbo_Proj.value;
	fnReloadSales(haction);
	//document.frmOrder.submit();
}

function fnLoad()
{
	document.frmOrder.Cbo_FromMonth.value = '<%=request.getAttribute("Cbo_FromMonth")%>';
	document.frmOrder.Cbo_ToMonth.value = '<%=request.getAttribute("Cbo_ToMonth")%>';
	document.frmOrder.Cbo_FromYear.value = '<%=request.getAttribute("Cbo_FromYear")%>';
	document.frmOrder.Cbo_ToYear.value = '<%=request.getAttribute("Cbo_ToYear")%>';
	
	document.frmOrder.Chk_ShowPercentFl.checked = <%=(strShowPercent == "0")?"false":"true"%>;
	document.frmOrder.Chk_ShowArrowFl.checked = <%=(strShowArrow == "0")?"false":"true"%>;
	document.frmOrder.Chk_HideAmountFl.checked = <%=(strHideAmount == "0")?"false":"true"%>;

	document.frmOrder.Cbo_Proj.value = '<%=strProj%>';
	document.frmOrder.Cbo_Type.value = '<%=strType%>';
	
	document.frmOrder.hAreaDirector.value = "null";
	document.frmOrder.DistributorID.value = "null";
	document.frmOrder.hTerritory.value = "null";
}

function Toggle(val)
{
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);
	
	if (obj.style.display == 'none')
	{
		obj.style.display = '';
	}
	else
	{
		obj.style.display = 'none';
	}
}

function fnDownloadVer()
{
	hact = document.frmOrder.hAction.value;
	hFromMonth = document.frmOrder.Cbo_FromMonth.value;
	hFromYear = document.frmOrder.Cbo_FromYear.value;
	hToMonth = document.frmOrder.Cbo_ToMonth.value;
	hToYear = document.frmOrder.Cbo_ToYear.value;
	cShowArrowFl =  document.frmOrder.Chk_ShowArrowFl.checked;
	cShowPercentFl = document.frmOrder.Chk_ShowPercentFl.checked; 
	cHideAmount = document.frmOrder.Chk_HideAmountFl.checked;
	hDistributorID = document.frmOrder.DistributorID.value;

	// Default selected value
	hProjectID = document.frmOrder.Cbo_Proj.value;
	hType = document.frmOrder.Cbo_Type.value;
	cboName = 	document.frmOrder.Cbo_Proj[document.frmOrder.Cbo_Proj.selectedIndex].text;
	
	windowOpener("<%=strServletPath%>/GmSalesYTDReportServlet?hExcel=Excel&hAction="+hact+"&Cbo_FromMonth="+hFromMonth+"&Cbo_FromYear="+hFromYear+"&Cbo_ToMonth="+hToMonth+"&Cbo_ToYear="+hToYear+"&Chk_ShowArrowFl="+cShowArrowFl+"&Chk_ShowPercentFl="+cShowPercentFl+"&Chk_HideAmountFl="+cHideAmount+"&Cbo_Proj="+hProjectID+"&Cbo_Type="+hType+"&DistributorID="+hDistributorID+"&hcboName="+cboName,"INVEXCEL","resizable=yes,scrollbars=yes,top=300,left=200,width=800,height=490");
}


</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad()">
<FORM name="frmOrder" method="get" action = "<%=strServletPath%>/GmSalesYTDReportServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="DistributorID" value="null">
<input type="hidden" name="hAccountID" value="null">
<input type="hidden" name="SetNumber" value="<%=request.getAttribute("hSetNumber")%>">
<input type="hidden" name="hRepID" value="null">
<input type="hidden" name="hPartNumber" value="null">
<input type="hidden" name="hTerritory" value="null">
<input type="hidden" name="hAreaDirector" value="null">

<input type="hidden" name="Cbo_Name" value="">
<TABLE cellSpacing=0 cellPadding=0  border=0 width="100%">
<TBODY>
	<tr>
		<td rowspan="10" class=Line noWrap width=1></td>
		<td class=Line noWrap height=1></td>
		<td rowspan="10" class=Line noWrap width=1></td>
	</tr>
	<tr>
		<td>
			<table cellSpacing=0 cellPadding=0 width="100%">
				<tr class=Line>
					<td height=25 class=RightDashBoardHeader><%=strHeader %><fmtSalesReportMultiYTD:message key="LBL_IN"/></td>
					<td  height="25" class="RightDashBoardHeader">
					<fmtSalesReportMultiYTD:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr class=Line><td noWrap height=1 colspan="4"></td></tr>
	<!-- Addition Filter Information -->
	<tr><td>
			<jsp:include page="/sales/GmSalesFilters.jsp" >
			<jsp:param name="FRMNAME" value="frmOrder" />
			<jsp:param name="HACTION" value="<%=strhAction%>" />
			<jsp:param name="HIDE" value="SYSTEM" />
			<jsp:param name="HIDEBUTTON" value="HIDEGO" />
			</jsp:include>
	</td></tr>
	<tr>
		<td><jsp:include page="/common/GmCrossTabFilterInclude.jsp" > 
		<jsp:param name="HACTION" value="<%=strhAction%>" />
		</jsp:include> </td>
	</tr>
		<!-- Screen Level Filter	-->
	<tr class=Line><td noWrap height=1></td></tr>	
	<tr>
	<td class="RightTableCaption" colspan="2" HEIGHT="25">
		<!--Apply Group Filter
		<input type="checkbox" size="30"  name="Chk_ShowPercentFl" value=1 tabindex=6>
		-->
		&nbsp;&nbsp;&nbsp;<fmtSalesReportMultiYTD:message key="LBL_PRO_GRP_NAME"/>
		&nbsp;<select name="Cbo_Proj" id="Region" class="RightText" tabindex="9"><option value="0" ><fmtSalesReportMultiYTD:message key="LBL_ALL_PRO"/>
<%		HashMap hcboVal = null;
			  		
		int intSize = alProjList.size();
		hcboVal = new HashMap();
		for (int i=0;i<intSize;i++)
		{
			hcboVal = (HashMap)alProjList.get(i);
			strCodeID = (String)hcboVal.get("ID");
			strProjNm = GmCommonClass.getStringWithTM((String)hcboVal.get("NAME"));
%>			<option <%=strSelected%> value="<%=strCodeID%>"><%=strProjNm%></option>
<%		}%>
		</select>
		&nbsp;<fmtSalesReportMultiYTD:message key="LBL_TYPE"/> 
		&nbsp;&nbsp;<select name="Cbo_Type" id="Type" class="RightText" tabindex="10">
			 <option value="0" ><fmtSalesReportMultiYTD:message key="LBL_UNIT"/></option>
			 <option value="1" ><fmtSalesReportMultiYTD:message key="LBL_AMOUNT"/></option>
		 </select>&nbsp;&nbsp;</td>
	</tr>	
	
	<tr class=borderDark>
		<td noWrap height=1>  </td>
	</tr>
	<tr>
		<td noWrap height=3>  </td>
	</tr>	
	<tr>
		<td align=center> <%=gmCrossTab.PrintMultiLevelCrossTabReport(hmReturn) %></td>
	</tr>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>
<%@ include file="/common/GmFooter.inc"%>
</HTML>
