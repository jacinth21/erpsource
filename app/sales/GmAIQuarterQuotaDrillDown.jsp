
<%
	/**********************************************************************************
	 * File		 		: GmAIQuarterQuotaDrillDown.jsp
	 * Desc		 		: This screen is used for the 
	 * Version	 		: 1.0
	 * author			: Joe P Kumar
	 ************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc"%>
<%@ page import="org.displaytag.decorator.TotalTableDecorator"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel"%>

<%@ taglib prefix="fmtAIQuarterQuotaDrillDown" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\GmAIQuarterQuotaDrillDown.jsp -->

<fmtAIQuarterQuotaDrillDown:setLocale value="<%=strLocale%>"/>
<fmtAIQuarterQuotaDrillDown:setBundle basename="properties.labels.sales.GmAIQuarterQuotaDrillDown"/>

<%
	String strWikiTitle = GmCommonClass.getWikiTitle("REQUEST_MODIFY_VIEW");
	request.setAttribute("dynadecorator", new org.displaytag.decorator.TotalTableDecorator() {
		DynaBean db;
		double dblTot = 0;
		String strTxnType = "";
		String strTxnId = "";

		public String addRowClass() {
			db = (DynaBean) this.getCurrentRowObject();
			dblTot = Double.parseDouble((db.get("TXNQTY")).toString());
			return dblTot > 0 ? "RightText" : "RightTextRed";
		}

	});
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Additional Inventory Quota Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>

<script>
 function fnShowDetails(){
 	document.frmAIQuotaReportForm.strOpt.value = 'showdetails';
 	document.frmAIQuotaReportForm.submit();
 }
 
 function fnGo(){
 	document.frmAIQuotaReportForm.strOpt.value = 'report';
 	document.frmAIQuotaReportForm.action = "/gmAIQuotaReportAction.do?method=quarterlyQuotaDrillDownByAD&showDetailsFlag=on";
 	fnStartProgress('Y');
 	document.frmAIQuotaReportForm.submit();
 }
 
 function fnViewFieldSales(varfsId, varfsName){
 	document.frmAIQuotaReportForm.action = "/gmAIQuotaReportAction.do?method=quarterlyQuotaDrillDownByAD&showDetailsFlag=on";
 	document.frmAIQuotaReportForm.fieldSales.value = varfsId;
 	document.frmAIQuotaReportForm.fieldSalesName.value = varfsName;
 	document.frmAIQuotaReportForm.submit();
 }
 
 function fnPrintConsignVer(val)
{
	windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId="+val,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnSelectAll(varCmd)
{
				objSelAll = document.all.selectAll;
				if (objSelAll ) {
				objCheckSiteArr = document.all.checkFieldSales;

				if ((objSelAll.checked || varCmd == 'selectAll') && objCheckSiteArr)
				{
					objSelAll.checked = true;
					objCheckSiteArrLen = objCheckSiteArr.length;
					for(i = 0; i < objCheckSiteArrLen; i ++) 
					{	
						objCheckSiteArr[i].checked = true;
					}
				}
				else if (!objSelAll.checked  && objCheckSiteArr ){
					objCheckSiteArrLen = objCheckSiteArr.length;
					for(i = 0; i < objCheckSiteArrLen; i ++) 
					{	
						objCheckSiteArr[i].checked = false;
					}
				}
				}
	}

function fnDeselect()
{
objSelAll = document.all.selectAll;
if (objSelAll) {
	objSelAll.checked = false;
	}
}
	
</script>

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>

<html:form action="/gmAIQuotaReportAction.do?method=quarterlyQuotaDrillDownByAD">
	<html:hidden property="strOpt" />
	<html:hidden property="adId" />
	<html:hidden property="adName" />
	<html:hidden property="vpId" />
	<html:hidden property="vpName" />
	<html:hidden property="hfromPage" />
	<html:hidden property="haction" />
	<html:hidden property="fieldSales" value="" />
	<html:hidden property="fieldSalesName" value="" />
	
<!-- Struts tag lib code modified for JBOSS migration changes -->
	<BODY leftmargin="20" topmargin="10">
	<table border="0" class="DtTable800" width="800" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4"><fmtAIQuarterQuotaDrillDown:message key="LBL_ADDITIONALINVENTORY_RPT"/></td>
		</tr>
		<tr>
			<td class="Line" height="1" colspan="4"></td>
		</tr>
		<logic:equal name="frmAIQuotaReportForm" property="hfromPage" value="REPORT">
			<tr>
				<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtAIQuarterQuotaDrillDown:message key="LBL_"/>Quota Date From :</td>
				<td>&nbsp;<html:text property="quotaFromDate" size="10" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
					onblur="changeBgColor(this,'#ffffff');" />&nbsp; <img id="Img_Date" style="cursor: hand"
					onclick="javascript:show_calendar('frmAIQuotaReportForm.quotaFromDate');" title="Click to open Calendar" src="<%=strImagePath%>/nav_calendar.gif" border=0
					align="absmiddle" height=18 width=19 /></td>
				<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtAIQuarterQuotaDrillDown:message key="LBL_QUOTA_DATE_TO"/> :</td>
				<td>&nbsp;<html:text property="quotaToDate" size="10" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
					onblur="changeBgColor(this,'#ffffff');" />&nbsp; <fmtAIQuarterQuotaDrillDown:message key="IMG_ALT_OPEN" var = "varOpen"/>
					<img id="Img_Date" style="cursor: hand"
					onclick="javascript:show_calendar('frmAIQuotaReportForm.quotaToDate');" title="${varOpen}" src="<%=strImagePath%>/nav_calendar.gif" border=0
					align="absmiddle" height=18 width=19 /></td>

			</tr>
			<tr>
				<td class="Line" height="1" colspan="4"></td>
			</tr>
			<tr class="shade">
				<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtAIQuarterQuotaDrillDown:message key="LBL_PART_NUM_ID"/> :</td>
				<td>&nbsp;<html:text property="partNumberId" size="15" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
					onblur="changeBgColor(this,'#ffffff');" />&nbsp;</td>
				<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtAIQuarterQuotaDrillDown:message key="LBL_PART_NUM_DESC"/> :</td>
				<td>&nbsp;<html:text property="partNumberDescription" size="40" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
					onblur="changeBgColor(this,'#ffffff');" />&nbsp;</td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="4"></td>
			</tr>
			<tr>
				<td class="RightTableCaption" align="right" HEIGHT="112">&nbsp;<fmtAIQuarterQuotaDrillDown:message key="LBL_FIELD_SALES"/> :</td>
				<td colspan="2">
				<DIV
					style="display: visible; height: 96%; overflow: auto; border-width: 1px; border-style: solid; margin-left: 5px; margin-right: 8px; margin-top: 2px; margin-bottom: 2px;">
				<table cellspacing="0" cellpadding="0">
					<tr bgcolor="gainsboro">
						<td>&nbsp;<html:checkbox property="selectAll" onclick="fnSelectAll('toggle');" /><B>&nbsp;<fmtAIQuarterQuotaDrillDown:message key="LBL_SELECT_ALL"/> </B></td>
					</tr>
				</table>
				<table>
					<logic:iterate id="fieldSalesList" name="frmAIQuotaReportForm" property="alFieldSales">
						<tr>
							<td><htmlel:multibox property="checkFieldSales" value="${fieldSalesList.D_ID}" onclick="fnDeselect();" /> <bean:write name="fieldSalesList"
								property="D_NAME" /></td>
						</tr>
					</logic:iterate>
				</table>
				</DIV>
				</td>
				<td><fmtAIQuarterQuotaDrillDown:message key="BTN_GO" var="varGo"/><gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" gmClass="button" buttonType="Save" onClick="fnGo();" /></td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="4"></td>
			</tr>
		</logic:equal>
		<tr class="shade">
			<td colspan="4" class="RightTableCaption" HEIGHT="24"><fmtAIQuarterQuotaDrillDown:message key="LBL_SHOW_DETAILS"/>: &nbsp;<html:checkbox property="showDetailsFlag"
				onclick="javascript:fnShowDetails();" /></td>
		</tr>
		<tr>
			<td class="Line" height="1" colspan="4"></td>
		</tr>
		<logic:notEqual name="frmAIQuotaReportForm" property="hfromPage" value="REPORT">
			<tr>
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
				<td colspan="4"><jsp:include page="/sales/GmSalesFilterDetails.jsp"/>
				</td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="4"></td>
			</tr>
		</logic:notEqual>
		<tr>
			<td colspan="4"><display:table name="requestScope.frmAIQuotaReportForm.rdQuarterlyQuotaDrillDownByAD"
				requestURI="/gmAIQuotaReportAction.do?method=quarterlyQuotaDrillDownByAD" export="true" class="its" id="currentRowObject"
				decorator="com.globus.sales.displaytag.beans.DTAIQuotaReportWrapper">
				<logic:equal name="frmAIQuotaReportForm" property="haction" value="LoadVP">
					<fmtAIQuarterQuotaDrillDown:message key="DT_AD_NAME" var="varADName"/>
					<display:column property="ADNAME" title="${varADName}" sortable="true" maxLength="20" group="1" />
				</logic:equal>
				<logic:notEqual name="frmAIQuotaReportForm" property="showDetailsFlag" value="on">
					<fmtAIQuarterQuotaDrillDown:message key="DT_FIELD_SALES" var="varFieldSales"/>
					<display:column property="FIELDSALES" title="${varFieldSales}" sortable="true" maxLength="20" />
				</logic:notEqual>
				<logic:equal name="frmAIQuotaReportForm" property="showDetailsFlag" value="on">
					<fmtAIQuarterQuotaDrillDown:message key="DT_FIELD_SALES" var="varFieldSales"/>
					<display:column property="FIELDSALES" title="${varFieldSales}" sortable="true" maxLength="20" group="1" />
					<fmtAIQuarterQuotaDrillDown:message key="DT_PART_NUMBER" var="varPrtNum"/>
					<display:column property="PNUM" title="${varPrtNum}" sortable="true" style="height:20px" />
					<fmtAIQuarterQuotaDrillDown:message key="DT_PRT_NUM_DESC" var="varPrtNumDesc"/>
					<display:column property="PNUMDESC" title="${varPrtNumDesc}" sortable="true" maxLength="20" />
					<fmtAIQuarterQuotaDrillDown:message key="DT_TXN_ID" var="varTxnId"/>
					<display:column property="TXNID" title="${varTxnId}" sortable="true" style="width:100px" />
					<fmtAIQuarterQuotaDrillDown:message key="DT_TXN_DATE" var="varTxnDate"/>
					<display:column property="TXNDATE" title="${varTxnDate}" sortable="true" style="width:60px" format="{0,date,short}" />
				</logic:equal>
				<fmtAIQuarterQuotaDrillDown:message key="DT_TXN_QTY" var="varTxnQty"/>
				<display:column property="TXNQTY" title="${varTxnQty}" sortable="true" total="true" class="alignright" />
				<fmtAIQuarterQuotaDrillDown:message key="DT_LIST_PRICE" var="varListPrice"/>
				<display:column property="PRICE" title="${varListPrice}" sortable="true" total="true" class="alignright" format="{0,number,$#,###,###.00}" />
				<fmtAIQuarterQuotaDrillDown:message key="DT_TOTAL_PRICE" var="varTotalPrice"/>
				<display:column property="TOTALPRICE" title="${varTotalPrice}" sortable="true" total="true" class="alignright" format="{0,number,$#,###,###.00}" />
			</display:table></td>
		</tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
