 <%
/**********************************************************************************
 * File		 		: GmProjectSalesReport.jsp
 * Desc		 		: This screen is used to display Sales By State
 * Version	 		: 1.0
 * author			: RichardK
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.PopChartUtils"%>
<%@include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtProjectSalesReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- sales\GmProjectSalesReport.jsp -->
<fmtProjectSalesReport:setLocale value="<%=strLocale%>"/>
<fmtProjectSalesReport:setBundle basename="properties.labels.sales.GmProjectSalesReport"/>
<%
	GmServlet gm = new GmServlet();
	HashMap hmReturn = new HashMap();
	HashMap hmSalesByMonth = new HashMap();

	ArrayList alProjectSales 	= new ArrayList();	
	
	gm.checkSession(response,session);

	String strWikiTitle = GmCommonClass.getWikiTitle("SALES_BY_PROJECT");
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	
	alProjectSales = (ArrayList)hmReturn.get("PROJECTSALES");
	hmSalesByMonth = (HashMap)hmReturn.get("PROJECTBYMONTH");
	
	// Other values
	
	StringBuffer sbSateScript = new StringBuffer();	
	StringBuffer sbTopAgentGrp = new StringBuffer();	
	StringBuffer sbTopAgentItem = new StringBuffer();	
	StringBuffer sbTopAccountGrp = new StringBuffer();	
	StringBuffer sbTopAccountItem = new StringBuffer();	
	
	StringBuffer  sbpcScript = new StringBuffer();	


%>
<HTML>
<HEAD>
<TITLE> Globus Medical: YTD #</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script>

function fnSubmit()
{
	document.frmOrder.submit();
}

function fnCallDetail(val)
{
	document.frmOrder.DistributorID.value = val;
	document.frmOrder.hAction.value = "LoadAccount";
	document.frmOrder.submit();
}

function fnCountryName(val)
{
  
}

function fnLoad()
{
	document.frmOrder.Cbo_FromMonth.value = '<%=request.getAttribute("Cbo_FromMonth")%>';
	document.frmOrder.Cbo_ToMonth.value = '<%=request.getAttribute("Cbo_ToMonth")%>';
	document.frmOrder.Cbo_FromYear.value = '<%=request.getAttribute("Cbo_FromYear")%>';
	document.frmOrder.Cbo_ToYear.value = '<%=request.getAttribute("Cbo_ToYear")%>';
}


</script>
</HEAD>

<BODY leftmargin="20" topmargin="3" onLoad="javascript:fnLoad()">
<FORM name="frmOrder" method="post" action = "<%=strServletPath%>/GmSalesByProjectServlet">
<input type="hidden" name="hAction" value="">
<TABLE cellSpacing=0 cellPadding=0  border="0"  width="800" >
<TBODY>
	<tr>
		<td rowspan="10" class=Line noWrap width=1></td>
		<td class=Line noWrap height=1></td>
		<td rowspan="10" class=Line noWrap width=1></td>
	</tr>
	<tr>
		<td>
			<TABLE cellSpacing=0 cellPadding=0  border="0"   width="100%" >
				<tr class=Line>
					<td height=23 class=RightDashBoardHeader><fmtProjectSalesReport:message key="LBL_SALES"/> </td>
					<td  height="25" class="RightDashBoardHeader">
					<fmtProjectSalesReport:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
				</tr>
			</TABLE>
		</td>
	</tr>
	<tr class=Line><td noWrap height=1></td></tr>
	<tr class=borderDark><td noWrap height=1> </td></tr>
	<tr>
		<td height=30 align="left">&nbsp;
			<jsp:include page="/common/GmMonthYearFilter.jsp" >
				<jsp:param name="HACTION" value="LoadProjectSales" />
			</jsp:include> 
		 </td>
	</tr>
	<tr>
		<td colspan="7" height="20">
			<jsp:include page="/sales/GmSalesFilters.jsp" >
				<jsp:param name="FRMNAME" value="frmOrder" />
				<jsp:param name="HACTION" value="LoadProjectSales" />
			</jsp:include> 
		</td>
	</tr>
	<tr class=Line><td noWrap height=1></td></tr>
	<tr>
	<td height=245  align="center" >
		<!-- Begin Embedder Code -->
		<%		PopChartUtils myPopChart = new PopChartUtils();
		
				// Sales By Project 
				sbpcScript.append(myPopChart.buildBar_Row_ChartScript(alProjectSales,"graph1",1,1));
				
				myPopChart.setPopFileName("BarChart.pcxml");
				myPopChart.setPCScript(sbpcScript.toString());
				myPopChart.setWidth(745);
				myPopChart.setHeight(251);			
		%>
		<%= myPopChart.getPopChartHTML() %>	
	</td></tr>
	<tr class=borderDark><td noWrap height=1>  </td></tr>

	<tr><td height=251 align="center" >
		<!-- Begin Embedder Code -->
		<%	PopChartUtils myPopChart2 = new PopChartUtils();
		
			// Sales By Project BY Month  
			sbpcScript = new StringBuffer();
			sbpcScript.append(myPopChart2.buildBar_RowStack_ChartScript(hmSalesByMonth,"graph",1,1));
			
			myPopChart2.setPopFileName("BarChart3DStack.pcxml");
			myPopChart2.setPCScript(sbpcScript.toString());
			myPopChart2.setWidth(745);
			myPopChart2.setHeight(251);			
		%>
		<%= myPopChart2.getPopChartHTML() %>	
	</td></tr>
	<tr class=borderDark><td noWrap height=1>  </td></tr>
	</TBODY></TABLE>

</FORM>
</BODY>
<%@include file="/common/GmFooter.inc"%>
</HTML>
