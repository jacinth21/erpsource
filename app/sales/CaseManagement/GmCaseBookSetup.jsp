<!-- \sales\CaseManagement\GmCaseBookSetup.jsp -->
<%
	/**********************************************************************************
	 * File		 		: GmCaseBookSetup.jsp
	 * Desc		 		: This screen is used for Case Book Setup
	 * Version	 		: 1.0
	 * author			: Xun
	 * 
	 ************************************************************************************/
%> 
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%> 
<%@ taglib prefix="fmtCaseBookSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtCaseBookSetup:setLocale value="<%=strLocale%>"/>
<fmtCaseBookSetup:setBundle basename="properties.labels.sales.CaseManagement.GmCaseBookSetup"/>

 
<bean:define id="alAcctList" name="frmCaseBookSetup" property="alAcctList" type="java.util.List"></bean:define>
<bean:define id="alDistList" name="frmCaseBookSetup" property="alDistList" type="java.util.List"></bean:define>
<bean:define id="alRepList" name="frmCaseBookSetup" property="alRepList" type="java.util.List"></bean:define>
<bean:define id="caseInfoID" name="frmCaseBookSetup" property="caseInfoID" type="java.lang.String"></bean:define> 
<bean:define id="caseID" name="frmCaseBookSetup" property="caseID" type="java.lang.String"></bean:define>
<%
String strSalesCaseMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_CASEMANAGEMENT");
String strTodaysDate = (String)session.getAttribute("strSessTodaysDate");

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Case Book </TITLE> 
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/party/GmParty.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strSalesCaseMgmntJsPath%>/GmCaseBookSetup.js"></script>
<script language="JavaScript">  

var lblSurgeryDate = '<fmtCaseBookSetup:message key="LBL_SURGERY_DATE"/>';
var lblFieldSalesName = '<fmtCaseBookSetup:message key="LBL_FIELD_SALES_NAME"/>';
var lblSalesRep = '<fmtCaseBookSetup:message key="LBL_SALES_REP"/>';

 var distLen = <%=alDistList.size()%>;
 var repLen = <%=alRepList.size()%>;
 var acctLen = <%=alAcctList.size()%>;
 var strCaseInfoID = "<%=caseInfoID%>";
 var strcaseID = "<%=caseID%>";
 var strToday = "<%=strTodaysDate%>";
<%
	HashMap hmDistVal = new HashMap();
	for (int i=0;i<alDistList.size();i++)
	{
		hmDistVal = (HashMap)alDistList.get(i);
%>
var	 DistArr<%=i%> =  "<%=hmDistVal.get("ID")%>,<%=hmDistVal.get("NM")%>" ;
<%
	}
%> 


<%
	HashMap hmRepVal = new HashMap();
	for (int i=0;i<alRepList.size();i++)
	{
		hmRepVal = (HashMap)alRepList.get(i);
%>
var	RepArr<%=i%> = "<%=hmRepVal.get("DID")%>,<%=hmRepVal.get("DNAME")%>,<%=hmRepVal.get("ID")%>,<%=hmRepVal.get("NM")%>" ;
<%
	}
%>

<%
	HashMap hmAcctVal = new HashMap();
	for (int i=0;i<alAcctList.size();i++)
	{
		hmAcctVal = (HashMap)alAcctList.get(i);
%>
var	AcctArr<%=i%> =  "<%=hmAcctVal.get("DID")%>,<%=hmAcctVal.get("REPID")%>,<%=hmAcctVal.get("ID")%>,<%=hmAcctVal.get("ACNAME")%>" ;
<%
	}
%>


function fnCaseVoid(){
	 
            document.frmCaseBookSetup.hCancelType.value = 'VDCASE';           
            document.frmCaseBookSetup.hTxnId.value = strCaseInfoID; 
            document.frmCaseBookSetup.hTxnName.value = strcaseID; 
            document.frmCaseBookSetup.action ="/GmCommonCancelServlet";            
            document.frmCaseBookSetup.hAction.value = "Load";                     
            document.frmCaseBookSetup.submit();
      }
 </script>
</HEAD>
 
<BODY leftmargin="20" topmargin="10"  onload="fnPageLoad();" >
<html:form action="/gmCaseBookSetup.do">
<html:hidden property="strOpt" value=""/>
 <html:hidden property="caseInfoID"  /> 
 
<input type="hidden" name="hTxnId" value=""/>
<input type="hidden" name="hTxnName" value=""/>
<input type= "hidden" name="hAction" value=""/>
<input type= "hidden"  name="hCancelType" value=""/>
 
	<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr > <td colspan="2">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25"  class="RightDashBoardHeader" width=60%>&nbsp;<fmtCaseBookSetup:message key="LBL_CASE_BOOKING"/></td>
						 <td align="right" class=RightDashBoardHeader>
							
<fmtCaseBookSetup:message key="IMG_ALT_HELP" var="varHelp"/>
							<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("CASE_BOOK_SETUP1")%>');" />
						</td>
					</tr>
					</table>
						</td>
					</tr>
					<tr><td class="Line" height="1" colspan="2"></td></tr>
					 
					<tr class="evenshade">
						  <td  class="RightTableCaption" align="Right" width="30%"><font color="red">*</font>&nbsp;<fmtCaseBookSetup:message key="LBL_SURGERY_DATE"/>:</td>
						<td width="320">&nbsp; 
						 <gmjsp:calendar SFFormName="frmCaseBookSetup" SFDtTextControlName="surgeryDate"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
					   </td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="oddshade">
						  <td  class="RightTableCaption" align="Right"><font color="red">*</font>&nbsp;<fmtCaseBookSetup:message key="LBL_SURGERY_TIME"/>:</td>
						<td width="320">&nbsp;
						<gmjsp:dropdown controlName="surgeryTimeHour" SFFormName="frmCaseBookSetup" SFSeletedValue="surgeryTimeHour"
							SFValue="alSurgeryTimeHour" codeId = "CODEID"  codeName = "CODENM"   defaultValue= "" />													
					
						&nbsp;<gmjsp:dropdown controlName="surgeryTimeMin" SFFormName="frmCaseBookSetup" SFSeletedValue="surgeryTimeMin"
							SFValue="alSurgeryTimeMin" codeId = "CODEID"  codeName = "CODENM"   defaultValue= "" />													
					
						&nbsp;<gmjsp:dropdown controlName="surgeryAMPM" SFFormName="frmCaseBookSetup" SFSeletedValue="surgeryAMPM"
							SFValue="alSurgeryAMPM" codeId = "CODEID"  codeName = "CODENM"   defaultValue= "" />													
					
						</td>
					</tr>					
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="evenshade">
						  <td  class="RightTableCaption" align="Right"><font color="red">*</font>&nbsp;<fmtCaseBookSetup:message key="LBL_FIELD_SALES_NAME"/>:</td>
						<td width="320">&nbsp;
						<gmjsp:dropdown controlName="distributorId" SFFormName="frmCaseBookSetup" SFSeletedValue="distributorId" onChange="javascript:fnDistFilterReps(this, 'DIST');"
							SFValue="alDistList" codeId = "ID"  codeName = "NM"   defaultValue= "[Choose One]" />													
					
						</td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="oddshade">
						  <td  class="RightTableCaption" align="Right"><font color="red">*</font>&nbsp;<fmtCaseBookSetup:message key="LBL_SALES_REP"/>:</td>
						<td width="320">&nbsp;
						<gmjsp:dropdown controlName="repId" SFFormName="frmCaseBookSetup" SFSeletedValue="repId" onChange="javascript:fnFilterAccts(this, 'REP');"
							SFValue="alRepList" codeId = "ID"  codeName = "NM"   defaultValue= "[Choose One]" />													
									</td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="evenshade">
						  <td  class="RightTableCaption" align="Right"><fmtCaseBookSetup:message key="LBL_ACCOUNT"/>:</td>
						<td>&nbsp;
						       
						        <gmjsp:dropdown controlName="accountId" SFFormName="frmCaseBookSetup" SFSeletedValue="accountId"
							           SFValue="alAcctList" codeId = "ID"  codeName = "ACNAME"   defaultValue= "[Choose One]" />
					&nbsp;	
					<input type="checkbox" name="checkSelectedAll" value=" "  onClick = "javascript:fnShowAllFilterAccts();"><fmtCaseBookSetup:message key="LBL_SHOW_ALL"/></td>
					
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="oddshade">
						  <td  class="RightTableCaption" align="Right"> &nbsp;<fmtCaseBookSetup:message key="LBL_PERSONAL_CASE_NOTES"/>:</td>
							 
	    
						<td  height="80"  >&nbsp;
						 <html:textarea property="personalNote" rows="5" cols="50" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/>
    		           
    		           </td>
					</tr>			
					 
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="evenshade">
						  <td  class="RightTableCaption" align="Right"><fmtCaseBookSetup:message key="LBL_CASE_STATUS"/>:</td>
						<td width="320">&nbsp;<bean:write name="frmCaseBookSetup" property="caseStatus"/>   </td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					 
					<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
					 
					 
					<tr class="evenshade">
						<td colspan="2">  
  
 
						</td>
					</tr>
				   <tr>
						<td  height="40" colspan="2" align="center">&nbsp;
						 <logic:notEqual name="frmCaseBookSetup" property="caseInfoID" value=""> 
							<fmtCaseBookSetup:message key="BTN_DELETE" var="varDelete"/><gmjsp:button style="width: 7em" value="${varDelete}" buttonType="Save" onClick="fnCaseVoid();" />&nbsp;
						</logic:notEqual>		
							<fmtCaseBookSetup:message key="BTN_SAVE_DRAFT" var="varSaveDraft"/><gmjsp:button name="" style="width: 10em" buttonType="Save" value="${varSaveDraft}"  onClick="fnSubmit();" />&nbsp;
							<fmtCaseBookSetup:message key="BTN_NEXT" var="varNext"/><gmjsp:button name="" style="width: 5em" buttonType="Save" value="${varNext}"  onClick="fnNext();" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
 
    </table>
</html:form>
     
 
<%@ include file="/common/GmFooter.inc" %>
</BODY>
 
</HTML>
 

