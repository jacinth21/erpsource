<!--\sales\CaseManagement\GmPartDescriptionSearch.jsp  -->
<%
/**********************************************************************************
 * File		 		: GmPartDescriptionSearch.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Dhana Reddy
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtPartDescriptionSearch" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartDescriptionSearch:setLocale value="<%=strLocale%>"/>
<fmtPartDescriptionSearch:setBundle basename="properties.labels.sales.CaseManagement.GmPartDescriptionSearch"/>

<bean:define id="alResult" name="frmOrderDetails" property="alResult" type="java.util.ArrayList"></bean:define>
<HTML>
<HEAD>
<TITLE> Globus Medical: Case List Report  </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
function fnSubmit(){
	document.frmOrderDetails.strOpt.value="load";
	fnStartProgress('Y');
	document.frmOrderDetails.submit();
}
function fnClose(){
	   
	   parent.dhxWins.window("w1").close();
}
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10">
<html:form action="/gmOrderDetails.do?method=searchPartDescription">
<html:hidden property="strOpt" name="frmOrderDetails"/>
<table border="0" class="DtTable680" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader" ><fmtPartDescriptionSearch:message key="LBL_PART_AND_DESCRIPTION"/></td>
			<td class="RightDashBoardHeader" colspan="2"></td>
			<td class="RightDashBoardHeader" colspan="2"></td>
		</tr>
		<tr class="Shade">
			<td   class="RightTableCaption" align="Right" >&nbsp;<fmtPartDescriptionSearch:message key="LBL_PART_NUMBER"/>:</td>
			<td > &nbsp;<html:text property="partNum" size="15" onfocus="changeBgColor(this,'#AACCE8');" 
							name="frmOrderDetails" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" />&nbsp;</td>
				
			<td height="25" class="RightTableCaption" align="Right">&nbsp;<fmtPartDescriptionSearch:message key="LBL_PART_DESCRIPTION"/>:</td>
			<td >&nbsp;
			<html:text property="partDesc" size="20" onfocus="changeBgColor(this,'#AACCE8');" 
							name="frmOrderDetails" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" />&nbsp;
			</td>
			<td  class="aligncenter" align="center" height="30" colspan="2">
			<fmtPartDescriptionSearch:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" gmClass="button" buttonType="Load" onClick="fnSubmit();" />&nbsp;
			</td>			
		</tr>
		<tr>
			<td colspan="6"> <display:table 
					name="requestScope.frmOrderDetails.alResult" 
					export="false" class="its" id="currentRowObject" 
					freezeHeader="true"  paneSize="300"
					requestURI="/gmOrderDetails.do?method=searchPartDescription">
					<fmtPartDescriptionSearch:message key="LBL_PART_NUMBER" var="varPartNumber"/><display:column property="PNUM" title="${varPartNumber}" style="width:100" />
					<fmtPartDescriptionSearch:message key="LBL_PART_DESCRIPTION" var="varPartDescription"/><display:column property="PDESC" title="${varPartDescription}" style="width:200" />
				</display:table> 
			</td>
		</tr>
		<tr>
			<td colspan="6" align="center">&nbsp;<fmtPartDescriptionSearch:message key="BTN_CLOSE" var="varClose"/> <gmjsp:button value="&nbsp;${varClose}&nbsp;" buttonType="Load" name="Btn_Close" gmClass="button" onClick="fnClose();" /></td>
        </tr>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>