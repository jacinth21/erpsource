<!--\sales\CaseManagement\GmCaseCalendarView.jsp  -->
<%
/**********************************************************************************
 * File		 		: GmCaseCalendarView.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			:Dhana Reddy
************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtCaseCalendarView" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtCaseCalendarView:setLocale value="<%=strLocale%>"/>
<fmtCaseCalendarView:setBundle basename="properties.labels.sales.CaseManagement.GmCaseCalendarView"/>

 <%String strTodaysDate = (String)session.getAttribute("strSessTodaysDate")==null?"":(String)session.getAttribute("strSessTodaysDate"); %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0">

<bean:define id="gridData" name="frmCaseCalendar" property="gridData" type="java.lang.String"> </bean:define>
<bean:define id="fromDt" name="frmCaseCalendar" property="fromDt" type="java.util.Date"> </bean:define>
<bean:define id="toDt" name="frmCaseCalendar" property="toDt" type="java.util.Date"> </bean:define>
<bean:define id="applnDateFmt" name="frmCaseCalendar" property="applnDateFmt" type="java.lang.String"> </bean:define>
<%
String strSalesCaseMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_CASEMANAGEMENT");
String strFromDt = GmCommonClass.parseNull((String)GmCommonClass.getStringFromDate(fromDt,applnDateFmt));
String strToDt = GmCommonClass.parseNull((String)GmCommonClass.getStringFromDate(toDt,applnDateFmt));
%>
<title>Case Calendar View</title>
	<script language="JavaScript" src='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler.js'></script>
	<script language="JavaScript" src='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler_minical.js'></script>
	<script language="JavaScript" src='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler_readonly.js'></script>
	<script language="JavaScript" src='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler_tooltip.js'></script>
		<script language="JavaScript" src='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler_expand.js'></script>
		
	<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js" type="text/javascript"></script>
	<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js" type="text/javascript"></script>
	<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"  type="text/javascript"></script>
	
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">
	<link rel='STYLESHEET' type='text/css' href='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler_ext.css'>
	<link rel='STYLESHEET' type='text/css' href='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler.css'>
	<link rel='STYLESHEET' type='text/css' href='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler_glossy.css'>
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
	<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
	<script language="JavaScript" src="<%=strSalesCaseMgmntJsPath%>/GmCaseCalendarView.js"></script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
	
<script type="text/javascript" charset="utf-8">
	var gridData ='<%=gridData%>';
	var fromDate = '<%=strFromDt%>';
</script>

</head>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<html:form action="/gmCaseCalendarAction.do?method=caseCalendar" >
<html:hidden property="strOpt" name="frmCaseCalendar" />
<html:hidden property="fromDt" name="frmCaseCalendar" value="<%=strFromDt %>" />
<html:hidden property="toDt" name="frmCaseCalendar"  value="<%=strToDt %>"/>
<html:hidden property="ccMonth" name="frmCaseCalendar" />
<html:hidden property="ccYear" name="frmCaseCalendar" />
<html:hidden property="haction" name="frmCaseCalendar" />
	<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
			
			<tr><td colspan="5" height="25" class="RightDashBoardHeader"><fmtCaseCalendarView:message key="LBL_CASE_CALENDER_VIEW"/></td>
			<td align="right" class=RightDashBoardHeader>
				<fmtCaseCalendarView:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("CASE_CALENDAR_VIEW")%>');" />
			</td> 
			</tr>
			<tr> <td colspan="6" bgcolor="#666666"></td>
			</tr>
	<% if(!gridData.equals("")){
	%>
	<tr><td colspan="6">
	<div id="caseCalender" class="dhx_cal_container" style='width:800; height:1030; '>
		<div class="dhx_cal_navline">
			<div class="dhx_cal_prev_button">&nbsp;</div>
			<div class="dhx_cal_next_button">&nbsp;</div>
			<div class="dhx_cal_today_button"></div>
			<div class="dhx_cal_date"></div>
			<div class="dhx_minical_icon" id="dhx_minical_icon" onclick="show_minical()">&nbsp;</div>
			<div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>
			<div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div>
			<div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div>
		</div> 
		
		<div class="dhx_cal_header"></div>
		<div class="dhx_cal_data"></div>
		
	</div>
	</td></tr>
	<tr><td colspan="1" height="25" class="RightTableCaption" ></td>
	<td><table border="0" cellspacing="0" cellpadding="0"  >
	 <tr><td height="25" width="50%" class="RightTableCaption" align="center" bgcolor ="#FF0000"><font color="#FFFFFF"><fmtCaseCalendarView:message key="LBL_DRAFT"/></font></td>
	<td height="25" class="RightTableCaption" align="center" bgcolor ="#000000"><font color="#FFFFFF"><fmtCaseCalendarView:message key="LBL_CANCELLED"/></font></td></tr>
	<tr><td height="25" width="50%" class="RightTableCaption" align="center" bgcolor ="#008000"><font color="#FFFFFF"><fmtCaseCalendarView:message key="LBL_ACTIVE"/></font></td>
	<td height="25" class="RightTableCaption" align="center" bgcolor ="#FFFF00"><fmtCaseCalendarView:message key="LBL_CLOSED"/></td></tr>
	</table>
	</td>
	<td><table border="0" cellspacing="0" cellpadding="0" >
	 <tr><td height="25" width="50%" class="RightTableCaption" align="center" bgcolor ="#00FFFF"><fmtCaseCalendarView:message key="LBL_RESCHEDULED"/></td>
	<td height="25" width="50%" class="RightTableCaption" >&nbsp; </td></tr>
	<tr><td height="25" width="50%" class="RightTableCaption" > </td>
	<td height="25" class="RightTableCaption" > </td></tr>
	</table>
	</td>
	<td colspan="2" height="25" class="RightTableCaption" ></td>
	</tr>
	<%} %>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>