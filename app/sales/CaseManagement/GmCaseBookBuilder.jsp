<!-- \sales\CaseManagement\GmCaseBookBuilder.jsp -->

  <%@ include file="/common/GmHeader.inc"%>
  <%@ taglib prefix="fmtCaseBookBuilder" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtCaseBookBuilder:setLocale value="<%=strLocale%>"/>
<fmtCaseBookBuilder:setBundle basename="properties.labels.sales.CaseManagement.GmCaseBookBuilder"/>
 <%
 String strPartyJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PARTY");

 %>
<HTML>
<HEAD>
<TITLE> Globus Medical: Case Book </TITLE>
 <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strPartyJsPath%>/GmParty.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
  
 
</HEAD>
 
<BODY leftmargin="20" topmargin="10"  >
<FORM name="frmVendor" method="POST" action=" ">
 
 
	<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr ><td colspan="2">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" colspan="1" class="RightDashBoardHeader" width=70%>&nbsp;<fmtCaseBookBuilder:message key="LBL_CASE_BOOKING_STEP2_CASE_BUILDER"/></td>
						  <td align="right" class=RightDashBoardHeader>
							<fmtCaseBookBuilder:message key="IMG_HELP" var="varHelp"/><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${Help}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("CASE_BOOK_SETUP2")%>');" />
						</td>
						</tr>
					</table>
					</td>
					</tr>
					<tr><td class="Line" height="1" colspan="2"></td></tr>
					 
					<tr height="25">
						  <td  class="RightTableCaption" align="Right"><fmtCaseBookBuilder:message key="LBL_PICK_FROM_FAVOURITE"/>:</td>
						<td width="460">&nbsp;<select name="Cbo_CaseId"  class="RightText" tabindex=0  > <option value=0 id=0 > </option><option value=1 id=1 ><fmtCaseBookBuilder:message key="LBL_ROCKINGHAM_MEMORIAL_HOSPITAL"/></option> </select>
						 
						</td>
					</tr>
					<tr>
                        <td class="RightTextBlue" colspan="2" align="center"><fmtCaseBookBuilder:message key="LBL_SELECT_FROM_LIST_TO_EDIT"/></td> 
                    </tr>
                    <tr><td colspan="2" class="Line"></td></tr>
					
				 
					<tr class="evenshade">
						  <td  class="RightTableCaption" align="Right"><fmtCaseBookBuilder:message key="LBL_CASE_ID"/>:</td>
						<td>&nbsp;  
						
						 <fmtCaseBookBuilder:message key="LBL_D999201010181"/>
					 
						</td>
					</tr>	
					 
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="oddshade">
						  <td  class="RightTableCaption" align="Right"><font color="red">*</font>&nbsp;<fmtCaseBookBuilder:message key="LBL_APPROACH"/>:&nbsp;</td>
						<td   width="180">&nbsp; 
					 <DIV style="display:visible;height: 80px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
      					 
            	<table>
                	
                       	<tr>
                       		<td>
						    	<input type="checkbox" name="checkSelectedReq" value="10"  >
							    <fmtCaseBookBuilder:message key="LBL_ANTERIOR"/>
							</td>
						</tr>	    
					
                       	<tr>
                       		<td>
						    	<input type="checkbox" name="checkSelectedReq" value="15"  >
							    <fmtCaseBookBuilder:message key="LBL_POSTERIOR"/>
							</td>
						</tr>	    
					
                       	<tr>
                       		<td>
						    	<input type="checkbox" name="checkSelectedReq" value="20"  >
							    <fmtCaseBookBuilder:message key="LBL_LATERAL"/>
							</td>
						</tr>	    
					
				</table>
			</DIV> <BR>
						</td>
					</tr>					
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="evenshade">
						  <td  class="RightTableCaption" align="Right"><font color="red">*</font>&nbsp;<fmtCaseBookBuilder:message key="LBL_REGION"/>:&nbsp;</td>
						<td   width="280">&nbsp; 
						
				 <DIV style="display:visible;height: 60px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
     			 
            	<table>
                	
                       	<tr>
                       		<td>
						    	<input type="checkbox" name="checkSelectedReq" value="10" >
							    <fmtCaseBookBuilder:message key="LBL_CERVICAL"/>&nbsp; 
							   <input type="checkbox" name="checkSelectedReq" value="10" >
							    <fmtCaseBookBuilder:message key="LBL_LUMBAR"/>&nbsp; 
							    <input type="checkbox" name="checkSelectedReq" value="10" >
							    <fmtCaseBookBuilder:message key="LBL_OCCIPUT"/>&nbsp; 
							</td>
						</tr>	    
					
                       	<tr>
                       		<td>
						    	<input type="checkbox" name="checkSelectedReq" value="10"  >
							    <fmtCaseBookBuilder:message key="LBL_THORIAC"/>&nbsp; 
							   <input type="checkbox" name="checkSelectedReq" value="10" >
							    <fmtCaseBookBuilder:message key="LBL_SACRAL"/>&nbsp; &nbsp; 
							    <input type="checkbox" name="checkSelectedReq" value="10"  >
							    <fmtCaseBookBuilder:message key="LBL_ILIUM"/>&nbsp; 
							</td>
						</tr>	   
                           
					
				</table>
			</DIV> <BR>
						</td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="oddshade">
						  <td  class="RightTableCaption" align="Right">&nbsp;<fmtCaseBookBuilder:message key="LBL_INTERBODY"/>:</td>
						<td  >&nbsp; 
						
						<input type=radio value="Yes" name="InterBody" checked="checked"><fmtCaseBookBuilder:message key="LBL_YES"/></input>&nbsp;
						<input type=radio value="No" name="InterBody"><fmtCaseBookBuilder:message key="LBL_NO"/></input>&nbsp;
										</td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="evenshade">
						  <td  class="RightTableCaption" align="Right"><fmtCaseBookBuilder:message key="LBL_BIOLOGIC"/>:</td>
						<td>&nbsp;  
						
						<input type=radio value="Yes" name="Biologic" checked="checked"><fmtCaseBookBuilder:message key="LBL_YES"/></input>&nbsp;
						<input type=radio value="No" name="Biologic"><fmtCaseBookBuilder:message key="LBL_NO"/></input>&nbsp;
						</td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="oddshade">
						  <td  class="RightTableCaption" align="Right"><fmtCaseBookBuilder:message key="LBL_FIXATION"/>:</td>
						<td>&nbsp;  
						
						<input type=radio value="Yes" name="Fixation" checked="checked"><fmtCaseBookBuilder:message key="LBL_YES"/></input>&nbsp;
						<input type=radio value="No" name="Fixation"><fmtCaseBookBuilder:message key="LBL_NO"/></input>&nbsp;
						</td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="evenshade">
						  <td  class="RightTableCaption" align="Right"> &nbsp;<fmtCaseBookBuilder:message key="LBL_LEVEL_TREATED"/>:</td>
							  
						<td  height="25"  >&nbsp;  <fmtCaseBookBuilder:message key="LBL_FROM"/> 
						<select name="Cbo_Cfrom" class="RightText" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');"> 
  						 <option  value= 'C1'  id=null><fmtCaseBookBuilder:message key="LBL_C1"/></option>
						 <option  value= 'C2'   id=null><fmtCaseBookBuilder:message key="LBL_C2"/></option>
						 <option  value= 'C3'   id=null><fmtCaseBookBuilder:message key="LBL_C3"/></option>
						 <option  value= 'C4'   id=null><fmtCaseBookBuilder:message key="LBL_C4"/></option>
						 <option  value= 'C5'   id=null><fmtCaseBookBuilder:message key="LBL_C5"/></option>
						 <option  value= 'C6'   id=null><fmtCaseBookBuilder:message key="LBL_C6"/></option>
						 <option  value= 'C7'   id=null><fmtCaseBookBuilder:message key="LBL_C7"/></option>
						 <option  value= 'C8'   id=null><fmtCaseBookBuilder:message key="LBL_C8"/></option> 
						</select>
						&nbsp; <fmtCaseBookBuilder:message key="LBL_TO"/>To 
						<select name="Cbo_Cfrom" class="RightText" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');"> 
  						 <option  value= 'C1'  id=null><fmtCaseBookBuilder:message key="LBL_C1"/></option>
						 <option  value= 'C2'   id=null selected><fmtCaseBookBuilder:message key="LBL_C2"/></option>
						 <option  value= 'C3'   id=null><fmtCaseBookBuilder:message key="LBL_C3"/></option>
						 <option  value= 'C4'   id=null><fmtCaseBookBuilder:message key="LBL_C4"/></option>
						 <option  value= 'C5'   id=null><fmtCaseBookBuilder:message key="LBL_C5"/></option>
						 <option  value= 'C6'   id=null><fmtCaseBookBuilder:message key="LBL_C6"/></option>
						 <option  value= 'C7'   id=null><fmtCaseBookBuilder:message key="LBL_C7"/></option>
						 <option  value= 'C8'   id=null><fmtCaseBookBuilder:message key="LBL_C8"/></option> 
						</select> 
						</td>
					</tr>			
					 
					<tr><td class="LLine" height="1" colspan="2"></td></tr>

					<tr class="oddshade">
						  
						 <td  class="RightTableCaption" align="Right"> &nbsp;<fmtCaseBookBuilder:message key="LBL_NUMBER_OF_LEVEL"/>:</td>	  
						<td  class="RightTableCaption"    > 
						 
						&nbsp;   <input type="text" size="3"  id="Level" name="Level" class="InputArea" onFocus="changeBgColor(this,'#AACCE8'); " onBlur="changeBgColor(this,'#ffffff'); " value="1">
						</td>
					</tr>			
					 	<tr><td class="LLine" height="1" colspan="2"></td></tr>
				   <tr><!-- Custom tag lib code modified for JBOSS migration changes -->
						<td  height="40" colspan="2" align="center">&nbsp;
							<fmt:message key="LBL_BACK" var="varBack"/>
							<gmjsp:button name="" style="width: 5em" value="${varBack}"  buttonType="Load" onClick="window.location='/sales/CaseManagement/GmCaseBookSetup.jsp'"/>&nbsp;
							<fmt:message key="LBL_DELETE" var="varDelete"/>
							<gmjsp:button name="" style="width: 5em" value="${varDelete}" buttonType="Save"  onClick="fnSubmit();" />&nbsp;
							<fmt:message key="LBL_SAVE_DRAFT" var="varSaveDraft"/>
							<gmjsp:button name="" style="width: 8em" value="${varSaveDraft}" buttonType="Save" onClick="fnSubmit();" />&nbsp;
							<fmt:message key="LBL_NEXT" var="varNext"/>
							<gmjsp:button name="" style="width: 5em" value="${varNext}" buttonType="Load"  onClick="window.location='/sales/CaseManagement/GmCaseBookSetDetails.jsp'" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
 
    </table>
</FORM>
     
 <%@ include file="/common/GmFooter.inc" %> 
 
</BODY>
 
</HTML>
 

