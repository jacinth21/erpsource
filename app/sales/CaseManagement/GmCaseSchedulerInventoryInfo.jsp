<!-- \sales\CaseManagement\GmCaseSchedulerInventoryInfo.jsp -->
<%
	/**********************************************************************************
	 * File		 		: GmCaseSchedulerInventoryInfo.jsp
	 * Desc		 		: Case InventoryInformation view Page
	 * Version	 		: 1.0
	 * author			: N Raja
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="frmtCaseListRpt"	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import="com.globus.common.beans.GmCommonClass"%>
<frmtCaseListRpt:setLocale value="<%=strLocale%>" />
<frmtCaseListRpt:setBundle basename="properties.labels.sales.CaseManagement.GmCaseInventoryInfo" />
<%
String strSalesCaseMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_CASEMANAGEMENT");
String strGridXmlInvData = GmCommonClass.parseNull((String)request.getAttribute("XMLGRIDINVDATA"));  
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Case Scheduler List Report</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strSalesCaseMgmntJsPath%>/GmCaseSchedulerInventoryInfo.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/jquery-latest.js"></script> 

<script>
var objGridInvData;
var gridInvObj;
objGridInvData = '<%=strGridXmlInvData%>';
</script>

<style>
.autocompleteINV .search-container {
	display: inline-block;
}
</style>

</HEAD>
<BODY leftmargin="0" topmargin="0" onload="fnOnPageLoad_Case();">
		<table border="0" class="DtTable785" cellspacing="0" cellpadding="0">
	      <%if (strGridXmlInvData.indexOf("cell") != -1) {%>  	
         <tr> <td colspan="5">
          <div id="caseInventoryInfo" style="height: 347px; width: 785px; display:table-caption;"></div>
 	 	 </td>
 	    </tr>
       <tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>
       <% }else{ %>
		 <tr>
		 <td height="30" colspan="5" align="center" class="RightTextBlue"><frmtCaseListRpt:message key="LBL_NO_DIS"/></td>
		</tr>
		<%} %>	
	  </table>
	
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
