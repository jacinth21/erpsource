<!--\sales\DashBoard\GmSalesDashBoard.jsp  -->
<%
/**********************************************************************************
 * File		 		: GmSalesDashBoard.jsp
 * Desc		 		: This screen is used for the Sales DashBoard
 * Version	 		: 1.0
 * author			: Gopinathan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtSalesDashBoard" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.*,java.io.*"%>
<%@ page import="java.sql.Date"%>
<%-- <%@ page import ="com.globus.common.beans.PopChartUtils"%> --%>
<fmtSalesDashBoard:setLocale value="<%=strLocale%>"/>
<fmtSalesDashBoard:setBundle basename="properties.labels.sales.Dashboard.GmCaseScheDashBoard"/>
<bean:define id="hmKitShipDsh" name="frmGmCaseSchedulerDash" property="hmKitShipDsh" scope="request" type="java.util.HashMap"></bean:define>
<bean:define id="hmKitReturnDsh" name="frmGmCaseSchedulerDash" property="hmKitReturnDsh" scope="request" type="java.util.HashMap"></bean:define>
<bean:define id="hmKitShipDshup" name="frmGmCaseSchedulerDash" property="hmKitShipDshup" scope="request" type="java.util.HashMap"></bean:define>
<bean:define id="hmKitRetDshup" name="frmGmCaseSchedulerDash" property="hmKitRetDshup" scope="request" type="java.util.HashMap"></bean:define>
<%
String strSalesCaseMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_CASEMANAGEMENT");
String strWikiTitle = GmCommonClass.getWikiTitle("CASE_SCHEDULER_DASHBOARD");

HashMap hmshipover = GmCommonClass.parseNullHashMap((HashMap)hmKitShipDsh.get("SHIPOVERDUE"));
HashMap hmshiptoday = GmCommonClass.parseNullHashMap((HashMap)hmKitShipDsh.get("SHIPTODAY"));
HashMap hmshiptmw = GmCommonClass.parseNullHashMap((HashMap)hmKitShipDsh.get("SHIPTMW"));

String strShipOverDue = GmCommonClass.parseNull((String)hmshipover.get("SHIPOVERDUECNT"));
String strShipoverstdt = GmCommonClass.parseNull((String) hmshipover.get("STARTDATE")); 
String strShipoverenddt = GmCommonClass.parseNull((String) hmshipover.get("ENDDATE"));  

String strShipToday = GmCommonClass.parseNull((String)hmshiptoday.get("SHIPOVERDUECNT"));
String strShiptodaystdt = GmCommonClass.parseNull((String) hmshiptoday.get("STARTDATE")); 
String strShiptodayenddt = GmCommonClass.parseNull((String) hmshiptoday.get("ENDDATE"));

String strShipTmw = GmCommonClass.parseNull((String)hmshiptmw.get("SHIPOVERDUECNT"));
String strShiptmwstdt = GmCommonClass.parseNull((String)hmshiptmw.get("STARTDATE")); 
String strShiptmwenddt = GmCommonClass.parseNull((String)hmshiptmw.get("ENDDATE"));

HashMap hmretover = GmCommonClass.parseNullHashMap((HashMap)hmKitReturnDsh.get("RETOVERDUE"));
HashMap hmrettoday = GmCommonClass.parseNullHashMap((HashMap)hmKitReturnDsh.get("RETTODAY"));
HashMap hmrettmw = GmCommonClass.parseNullHashMap((HashMap)hmKitReturnDsh.get("RETTMW"));

String strRetOverDue = GmCommonClass.parseNull((String)hmretover.get("RETURNOVERDUECNT"));
String strRetoverstdt = GmCommonClass.parseNull((String) hmretover.get("STARTDATE")); 
String strRetoverenddt = GmCommonClass.parseNull((String) hmretover.get("ENDDATE"));

String strRetToday = GmCommonClass.parseNull((String)hmrettoday.get("RETURNTODAYCNT"));
String strRettodaystdt = GmCommonClass.parseNull((String) hmrettoday.get("STARTDATE")); 
String strRettodayenddt = GmCommonClass.parseNull((String) hmrettoday.get("ENDDATE"));

String strRetTmw = GmCommonClass.parseNull((String)hmrettmw.get("RETURNTOMORROWCNT")); 
String strRettmwstdt = GmCommonClass.parseNull((String)hmrettmw.get("STARTDATE")); 
String strRettmwenddt = GmCommonClass.parseNull((String)hmrettmw.get("ENDDATE"));

String strKitShipTW = GmCommonClass.parseNull((String)hmKitShipDshup.get("TW_CNT"));
String strKitTWStDt = GmCommonClass.parseNull((String)hmKitShipDshup.get("TW_STARTDATE"));
String strKitTWendDt = GmCommonClass.parseNull((String)hmKitShipDshup.get("TW_ENDDATE"));

String strKitShipNW = GmCommonClass.parseNull((String)hmKitShipDshup.get("NW_CNT"));
String strKitNWStDt = GmCommonClass.parseNull((String)hmKitShipDshup.get("NW_STARTDATE"));
String strKitNWendDt = GmCommonClass.parseNull((String)hmKitShipDshup.get("NW_ENDDATE"));

String strKitShipTM = GmCommonClass.parseNull((String)hmKitShipDshup.get("TM_CNT")); 
String strKitTMStDt = GmCommonClass.parseNull((String)hmKitShipDshup.get("TM_STARTDATE"));
String strKitTMendDt = GmCommonClass.parseNull((String)hmKitShipDshup.get("TM_ENDDATE"));

String strKitRetTW = GmCommonClass.parseNull((String)hmKitRetDshup.get("TW_CNT"));
String strKitRetTWStDt = GmCommonClass.parseNull((String)hmKitRetDshup.get("TW_STARTDATE"));
String strKitRetTWendDt = GmCommonClass.parseNull((String)hmKitRetDshup.get("TW_ENDDATE"));

String strKitRetNW = GmCommonClass.parseNull((String)hmKitRetDshup.get("NW_CNT"));
String strKitRetNWStDt = GmCommonClass.parseNull((String)hmKitRetDshup.get("NW_STARTDATE"));
String strKitRetNWendDt = GmCommonClass.parseNull((String)hmKitRetDshup.get("NW_ENDDATE"));

String strKitRetTM = GmCommonClass.parseNull((String)hmKitRetDshup.get("TM_CNT")); 
String strKitRetTMStDt = GmCommonClass.parseNull((String)hmKitRetDshup.get("TM_STARTDATE"));
String strKitRetTMendDt = GmCommonClass.parseNull((String)hmKitRetDshup.get("TM_ENDDATE"));
%>

<HTML>
	<Head>
	<title> Case Scheduler Dashboard</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxChart/dhtmlxchart.css"/>  
    <script src="<%=strExtWebPath%>/dhtmlx/dhtmlxChart/dhtmlxchart.js" type="text/javascript"></script>
	<script language="JavaScript" src="<%=strSalesCaseMgmntJsPath%>/GmCaseSchDashboard.js"></script> 
    <style type="text/css" media="all">
		   @import url("<%=strCssPath%>/screen.css");
	</style>

<style>
.box {
  width: 280px;
  border: 1px solid #000;
  margin: auto;
  height: 220px;
  border-radius: 5px;
  margin-bottom: 20px;
}
.head{
  background-color:#cee4ff;
  border-radius: 5px;
  border-bottom: 1px solid #000;
  height: 30px;
}
.headlabel{
    padding-bottom: 15px;
}
.content{
padding-top: 50px;
}
</style>
	<script>
	// Below values set for kit shipping upcoming details
	var strKitTWStDt = '<%=strKitTWStDt%>';
	var strKitTWendDt = '<%=strKitTWendDt%>';
	var strKitNWStDt = '<%=strKitNWStDt%>';
	var strKitNWendDt = '<%=strKitNWendDt%>';
	var strKitTMStDt = '<%=strKitTMStDt%>';
	var strKitTMendDt = '<%=strKitTMendDt%>';
	var Kit_ShipUpcoming_dataset = [	
	{ count:"<%=strKitShipTW%>", month:"1-7 days", color: "#4472C4" , id:"KITSHIPTW"},
	{ count:"<%=strKitShipNW%>", month:"8-14 days", color: "#ED7D31" , id:"KITSHIPNW"},  
	{ count:"<%=strKitShipTM%>", month:"15-30 days", color: "#D3D3D3" , id:"KITSHIPTM"}
	];
	// Below values set for kit returns upcoming details
	var strKitRetTWStDt = '<%=strKitRetTWStDt%>';
	var strKitRetTWendDt = '<%=strKitRetTWendDt%>';
	var strKitRetNWStDt = '<%=strKitRetNWStDt%>';
	var strKitRetNWendDt = '<%=strKitRetNWendDt%>';
	var strKitRetTMStDt = '<%=strKitRetTMStDt%>';
	var strKitRetTMendDt = '<%=strKitRetTMendDt%>';
	var Kit_RetUpcoming_dataset = [	
	{ count:"<%=strKitRetTW%>", month:"1-7 days", color: "#4472C4" , id:"KITRETTW"},
	{ count:"<%=strKitRetNW%>", month:"8-14 days", color: "#ED7D31" , id:"KITRETNW"},  
	{ count:"<%=strKitRetTM%>", month:"15-30 days", color: "#D3D3D3" , id:"KITRETTM"}
	];
	</script>
	</Head>
  
	<BODY leftmargin="0" topmargin="0" onload="fnKitShipUpLoad();fnKitRetUpLoad();">

			<html:form  action="/gmCaseSchedulerDash.do?method=fetchKitData">				
			<table>
			<tr><td colspan ="4"><div class="headlabel"><font size="5"><center><b><fmtSalesDashBoard:message key="LBL_CASE_SCH_DASH_SHIPPING"/></b></center></font></div></td></tr>
	<tr><td style="padding-left:10px">	
	<div class="box">
		<div class="RightTableCaption head"><font color="red" size="5"><center><fmtSalesDashBoard:message key="LBL_OVERDUE"/></center></font></div>
		<div class="content" onclick="fnKitShipOver('<%=strShipoverstdt%>','<%=strShipoverenddt%>');"><font color="red" size="10"><center><%=strShipOverDue%></center></font></div>
	</div></td>
	
	<td style="padding-left:10px"><div class="box">
		<div class="RightTableCaption head"><font size="5"><center><fmtSalesDashBoard:message key="LBL_TODAY"/></center></font></div>
		<div class="content" onclick="fnKitShiptoday('<%=strShiptodaystdt%>','<%=strShiptodayenddt%>');"><font size="10"><center><%=strShipToday%></center></font></div></div>
	</td>
		
	<td style="padding-left:10px"><div class="box">
		<div class="RightTableCaption head"><font size="5"><center><fmtSalesDashBoard:message key="LBL_TOMORROW"/></center></font></div>
		<div class="content" onclick="fnKitShiptmw('<%=strShiptmwstdt%>','<%=strShiptmwenddt%>');"><font size="10"><center><%=strShipTmw%></center></font></div></div>
	</td>
		
	<td style="padding-left:10px;padding-right:10px;"><div class="box">
		<div class="RightTableCaption head"><font size="5"><center><fmtSalesDashBoard:message key="LBL_UPCOMING"/></center></font></div>
	<div id="KitShipUpcoming" style="width:330px;height:200px;" ></div></div>
	</td>
	</tr>
	
		<tr bgcolor="#000">
			<td colspan="6" height="1"></td>
		</tr>
			<tr >	<td colspan ="4"><div class="headlabel"><font size="5"><center><b><fmtSalesDashBoard:message key="LBL_CASE_SCH_DASH_RETURN"/></b></center></font></div></td></tr>
		<tr>
	<td style="padding-left:10px">
	<div class="box">
	<div class="RightTableCaption head"><font color="red" size="5"><center><fmtSalesDashBoard:message key="LBL_OVERDUE"/></center></font></div>
	<div class="content" onclick="fnKitRetover('<%=strRetoverstdt%>','<%=strRetoverenddt%>');"><font color="red" size="10"><center><%=strRetOverDue%></center></font></div>
	</div>

	</td>
		<td style="padding-left:10px">
	<div class="box">
		<div class="RightTableCaption head"><font size="5"><center><fmtSalesDashBoard:message key="LBL_TODAY"/></center></font></div>
	<div class="content" onclick="fnKitRettoday('<%=strRettodaystdt%>','<%=strRettodayenddt%>');"><font size="10"><center><%=strRetToday%></center></font></div></div>
	</td>
		<td style="padding-left:10px">
	<div class="box">
		<div class="RightTableCaption head"><font size="5"><center><fmtSalesDashBoard:message key="LBL_TOMORROW"/></center></font></div>
	<div class="content" onclick="fnKitRettmw('<%=strRettmwstdt%>','<%=strRettmwenddt%>');"><font size="10"><center><%=strRetTmw%></center></font></div></div>
	</td>
		<td style="padding-left:10px;padding-right:10px;">
	<div class="box">
		<div class="RightTableCaption head"><font size="5"><center><fmtSalesDashBoard:message key="LBL_UPCOMING"/></center></font></div>
	<div id="KitRetUpcoming" style="width:330px;height:200px;"></div></div>
	</td>
	</tr>
			</table>
			
			</html:form>
			<%@ include file="/common/GmFooter.inc" %>
	</BODY>
</HTML>

