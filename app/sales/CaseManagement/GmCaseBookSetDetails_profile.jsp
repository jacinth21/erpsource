<!--\sales\CaseManagement\GmCaseBookSetDetails_profile.jsp   -->
 <%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtCaseBookSetDetails_profile" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtCaseBookSetDetails_profile:setLocale value="<%=strLocale%>"/>
<fmtCaseBookSetDetails_profile:setBundle basename="properties.labels.sales.CaseManagement.GmCaseBookSetDetails_profile"/>
<%
String strPartyJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PARTY");
String strCustServiceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Case Book </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strPartyJsPath%>/GmParty.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
 
 
<script language="JavaScript" src="<%=strCustServiceJsPath%>/GmSalesRepSetup.js"></script>

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"  type="text/javascript"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js" type="text/javascript"></script>
 
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js" type="text/javascript"></script>
 
<script language="JavaScript" src="<%=strCustServiceJsPath%>/GmSalesRepSetup.js"></script>

<script>

var dhxWins,w1;

function doOnLoad(setid,setnm) {
 
    dhxWins = new dhtmlXWindows();
    dhxWins.enableAutoViewport(false);
    dhxWins.attachViewportTo("winVP");
 
    w1 = dhxWins.createWindow("w1", 30, 40, 600, 1000);
    w1.setText(message_sales[242]); 
    var ajaxUrl = fnAjaxURLAppendCmpInfo("/GmSetReportServlet?hOpt=SETRPT&hAction=Drilldown&hSetId="+setid+ "&hSetNm="+setnm);
	w1.attachURL(ajaxUrl);
}
 </script>
</HEAD>
 
<BODY leftmargin="20" topmargin="10" >
<FORM name="frmVendor" method="POST" action="">
   <div id="winVP" style="position: relative;  height: 1500px;   margin: 10px;">
	<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr >
						<td height="25" colspan="2" class="RightDashBoardHeader">&nbsp;<fmtCaseBookSetDetails_profile:message key="LBL_CASE_BOOKING"/></td>
						 
					</tr>
					<tr><td class="Line" height="1" colspan="2"></td></tr>
					 
					<tr class="evenshade">
						  <td  class="RightTableCaption" align="left" colspan="2"><font color="red"><fmtCaseBookSetDetails_profile:message key="LBL_SELECT_PRODUCT_SYSTEM"/></font></td>
						 
					</tr>
					
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="oddshade">
						  <td  class="RightTableCaption" align="left">
						       	<input type="checkbox" name="checkSelectedReq" value="10" >
							    <fmtCaseBookSetDetails_profile:message key="LBL_ASSURE_SYSTEM"/>&nbsp; 
						 </td>
						<td width="320">&nbsp; 
						</td>
					</tr>		
					 <tr><td class="LLine" height="1" colspan="2"></td></tr>
					  
					<tr class="evenshade">
						<td colspan="2"> 
							
				<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="oddshade">
						  <td  class="RightTableCaption" align="left">
						       	<input type="checkbox" name="checkSelectedReq" value="10" >
							    <fmtCaseBookSetDetails_profile:message key="LBL_PROVIDENCE_SYSTEM"/>&nbsp; 
						 </td>
						<td width="320">&nbsp; 
						</td>
					</tr>					
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
				 <tr  >
						  <td  class="RightTableCaption" align="left" height="25" colspan="2">
						       
						 </td>
						 
					</tr> 

<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="oddshade">
						  <td  class="RightTableCaption" align="left">
						       	<input type="checkbox" name="checkSelectedReq" value="10" >
							    <fmtCaseBookSetDetails_profile:message key="LBL_VIP_SYSTEM"/>&nbsp; 
						 </td>
						<td width="320">&nbsp; 
						</td>
					</tr>					
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr  >
						  <td  class="RightTableCaption" align="left" height="25" colspan="2">
						       
						 </td>
						 
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr  class="oddshade">
						  <td  class="RightTableCaption" align="left">
						       	<input type="checkbox" name="checkSelected" value="10" onclick="fnShowFilters('tabRecv');">
							    <fmtCaseBookSetDetails_profile:message key="LBL_XTEND_SYSTEM"/>&nbsp; 
						 </td>
						<td width="320">&nbsp; 
						</td>
					</tr>		
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					 
					 <tr>
						<td colspan="6">
							<table border="0" width="100%" cellspacing="0" cellpadding="0" id="tabRecv" style="display:none">
							<tr><td colspan="9" height="1" bgcolor="#cccccc"></td></tr>
							<tr   bgcolor="#e4e6f2" >
									<td class="RightTableCaption" align="left" HEIGHT="24"   ><fmtCaseBookSetDetails_profile:message key="LBL_#"/></td>
									<td class="RightTableCaption" align="center" HEIGHT="24"   width="20%"><fmtCaseBookSetDetails_profile:message key="LBL_SET_ID"/></td>
									<td class="RightTableCaption" align="center" HEIGHT="24" colspan="3" width="35%"><fmtCaseBookSetDetails_profile:message key="LBL_SET_NAME"/></td>
									<td class="RightTableCaption" align="left" HEIGHT="24"   width="15%"><fmtCaseBookSetDetails_profile:message key="LBL_FAMILY"/></td>
									<td class="RightTableCaption" align="left" HEIGHT="24"   width="10%"><fmtCaseBookSetDetails_profile:message key="LBL_TYPE"/></td>
									<td class="RightTableCaption" align="center" HEIGHT="24"   ><fmtCaseBookSetDetails_profile:message key="LBL_QTY"/></td>
									<td class="RightTableCaption" align="center" HEIGHT="24"   ><fmtCaseBookSetDetails_profile:message key="LBL_SELECT"/></td>
									 	</tr>
								<tr><td class="LLine" height="1" colspan="9"></td></tr>
								 
								<tr class="Shade">
									<td   align="left" HEIGHT="24"   ><fmtCaseBookSetDetails_profile:message key="LBL_1"/></td>
									<td   align="center" HEIGHT="24"   width="15%"><a href ="javascript:doOnLoad('961.901','XTEND Instrument Set')"><fmtCaseBookSetDetails_profile:message key="LBL_961.901"/></a></td>
									<td   align="left" HEIGHT="24" colspan="3" width="35%"><fmtCaseBookSetDetails_profile:message key="LBL_XTEND_INSTRUMENT_SET"/></td>
									<td   align="left" HEIGHT="24"  width="15%"  ><fmtCaseBookSetDetails_profile:message key="LBL_INSTRUMENTS"/></td>
									<td   align="left" HEIGHT="24"   ><fmtCaseBookSetDetails_profile:message key="LBL_STANDARD"/></td>
									<td   align="center" HEIGHT="24"  width="15%" ><select name="Cbo_RepId" id="" class="RightText"    > 
									<option value=1 id=0 ><fmtCaseBookSetDetails_profile:message key="LBL_1"/></option>
									<option  value= 2  id=null><fmtCaseBookSetDetails_profile:message key="LBL_2"/></option> 	
									 <option  value= 3  id=null><fmtCaseBookSetDetails_profile:message key="LBL_3"/></option>
									 <option  value= 4  id=null><fmtCaseBookSetDetails_profile:message key="LBL_4"/></option>
									 <option  value= 5  id=null><fmtCaseBookSetDetails_profile:message key="LBL_5"/></option>
									 <option  value= 6  id=null><fmtCaseBookSetDetails_profile:message key="LBL_6"/></option> 
									</select>	  </td> 
									<td class="RightTableCaption" align="center" HEIGHT="24" width="10%"  > 	<input type="checkbox" name="check1" value="10" checked>  </td>
									 	</tr>	
								
								<tr><td colspan="9" height="1" bgcolor="#cccccc"></td></tr>
								<tr  >
									<td  align="left" HEIGHT="24"   ><fmtCaseBookSetDetails_profile:message key="LBL_2"/></td>
									<td   align="center" HEIGHT="24"   width="15%"><a href ="javascript:doOnLoad('961.906','XTEND Screw Set')"><fmtCaseBookSetDetails_profile:message key="LBL_961.906"/> </a></td>
									<td   align="left" HEIGHT="24" colspan="3" width="35%"><fmtCaseBookSetDetails_profile:message key="LBL_XTEND_SCREW_SET"/></td>
									<td   align="left" HEIGHT="24"  width="10%"  ><fmtCaseBookSetDetails_profile:message key="LBL_IMPLANTS"/></td>
									<td   align="left" HEIGHT="24"   ><fmtCaseBookSetDetails_profile:message key="LBL_STANDARD"/></td>
									<td   align="center" HEIGHT="24"  width="10%"    ><select name="Cbo_RepId" id="" class="RightText"    > 
									<option value=1 id=0 ><fmtCaseBookSetDetails_profile:message key="LBL_1"/></option>
									<option  value= 2  id=null><fmtCaseBookSetDetails_profile:message key="LBL_2"/></option> 	
									 <option  value= 3  id=null><fmtCaseBookSetDetails_profile:message key="LBL_3"/></option>
									 <option  value= 4  id=null><fmtCaseBookSetDetails_profile:message key="LBL_4"/></option>
									 <option  value= 5  id=null><fmtCaseBookSetDetails_profile:message key="LBL_5"/></option>
									 <option  value= 6  id=null><fmtCaseBookSetDetails_profile:message key="LBL_6"/></option> 
									</select>	 </td>
									<td class="RightTableCaption" align="center" HEIGHT="24"    width="15%"  >  <input type="checkbox" name="check2" value="10"  checked>  </td>
									 	</tr>
									 	
								<tr><td colspan="9" height="1" bgcolor="#cccccc"></td></tr>
								<tr class="Shade">
									<td   align="left" HEIGHT="24"   >3</td>
									<td   align="center" HEIGHT="24"   width="15%"><a href ="javascript:doOnLoad('961.902','XTEND Cervical Place Set')"><fmtCaseBookSetDetails_profile:message key="LBL_961.902"/></a></td>
									<td   align="left" HEIGHT="24" colspan="3" width="35%"><fmtCaseBookSetDetails_profile:message key="LBL_XTEND_CERVICAL_PLACE_SET"/></td>
									<td   align="left" HEIGHT="24"  width="15%"  ><fmtCaseBookSetDetails_profile:message key="LBL_IMPLANTS"/></td>
									<td   align="left" HEIGHT="24"   ><fmtCaseBookSetDetails_profile:message key="LBL_STANDARD"/></td>
									<td   align="center" HEIGHT="24"  width="10%"    ><select name="Cbo_RepId" id="" class="RightText"    > 
									<option value=1 id=0 ><fmtCaseBookSetDetails_profile:message key="LBL_1"/></option>
									<option  value= 2  id=null><fmtCaseBookSetDetails_profile:message key="LBL_2"/></option> 	
									 <option  value= 3  id=null><fmtCaseBookSetDetails_profile:message key="LBL_3"/></option>
									 <option  value= 4  id=null><fmtCaseBookSetDetails_profile:message key="LBL_4"/></option>
									 <option  value= 5  id=null><fmtCaseBookSetDetails_profile:message key="LBL_5"/></option>
									 <option  value= 6  id=null><fmtCaseBookSetDetails_profile:message key="LBL_6"/></option> 
									</select>	 </td>
									<td class="RightTableCaption" align="center" HEIGHT="24"   width="15%"   ><input type="checkbox" name="check3" value="10" checked>  </td>   
									 	</tr>
								
								<tr><td class="LLine" height="1" colspan="9"></td></tr>
								 
								<tr class="Shade">
									<td   align="left" HEIGHT="24"   ><fmtCaseBookSetDetails_profile:message key="LBL_4"/></td>
									<td   align="center" HEIGHT="24"   width="15%"><a href ="javascript:doOnLoad('961.912','Extender Plate Se')"><fmtCaseBookSetDetails_profile:message key="LBL_961_912"/> </a></td>
									<td   align="left" HEIGHT="24" colspan="3" width="35%"><fmtCaseBookSetDetails_profile:message key="LBL_EXTENDER_PLATE_SET"/></td>
									<td   align="left" HEIGHT="24"   width="15%" ><fmtCaseBookSetDetails_profile:message key="LBL_IMPLANTS"/></td>
									<td   align="left" HEIGHT="24"   ><fmtCaseBookSetDetails_profile:message key="LBL_ADDITIONAL"/></td>
									<td   align="center" HEIGHT="24"  width="10%"  ><select name="Cbo_RepId" id="" class="RightText"    > 
									<option value=1 id=0 ><fmtCaseBookSetDetails_profile:message key="LBL_1"/></option>
									<option  value= 2  id=null><fmtCaseBookSetDetails_profile:message key="LBL_2"/></option> 	
									 <option  value= 3  id=null><fmtCaseBookSetDetails_profile:message key="LBL_3"/></option>
									 <option  value= 4  id=null><fmtCaseBookSetDetails_profile:message key="LBL_4"/></option>
									 <option  value= 5  id=null><fmtCaseBookSetDetails_profile:message key="LBL_5"/></option>
									 <option  value= 6  id=null><fmtCaseBookSetDetails_profile:message key="LBL_6"/></option> 
									</select>	  </td> 
									<td class="RightTableCaption" align="center" HEIGHT="24" width="15%"   > 	<input type="checkbox" name="check1" value="10"  >  </td>
									 	</tr>
								<tr > 
								 
									<td width="300">
									<div id="processimg"><IMG border=0 height="20" width="20" src="<%=strImagePath%>/process.gif"></img></div>
									<div id="DivShowUNMYes"><IMG border=0 height="20" width="20" src="<%=strImagePath%>/success.gif"></img></div>
									<div id="DivShowUNMExists"><b> </b></div>
									</td>
								</tr>
							 
								
								
							</table>
						</td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr  >
						  <td  class="RightTableCaption" align="left" height="25" colspan="2">
						       
						 </td>
						 
					</tr> 
				
				
 
						</td>
					</tr>
				   <tr>
						<td  height="40" colspan="2" align="center">&nbsp;
						<fmtCaseBookSetDetails_profile:message key="BTN_BACK" var="varBack"/><gmjsp:button style="width: 5em" value="${varBack}" buttonType="Load" onClick="window.location='/sales/CaseManagement/GmCasePorfileBuilder.jsp'" />&nbsp;
							<fmtCaseBookSetDetails_profile:message key="BTN_DELETE" var="varDelete"/><gmjsp:button style="width: 5em" value="${varDelete}" buttonType="Save"  onClick="fnSubmit();" />&nbsp;
							<fmtCaseBookSetDetails_profile:message key="BTN_SAVE_DRAFT" var="varSaveDraft"/><gmjsp:button style="width: 8em" value="${varSaveDraft}" buttonType="Save" onClick="fnSubmit();" />&nbsp;
							
							<fmtCaseBookSetDetails_profile:message key="BTN_NEXT" var="varNext"/><gmjsp:button style="width: 5em" value="${varNext}" buttonType="Load" onClick="window.location='/sales/CaseManagement/GmCaseBookConfirm_profile.jsp'" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
 
    </table></div>
</FORM>
     
 <%@ include file="/common/GmFooter.inc" %>
 
</BODY>
 
</HTML>
 

