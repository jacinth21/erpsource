<!-- \sales\CaseManagement\GmCaseListRpt.jsp -->
<%
/**********************************************************************************
 * File		 		: GmCaseListRpt.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Dhana Reddy
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtCaseListRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtCaseListRpt:setLocale value="<%=strLocale%>"/>
<fmtCaseListRpt:setBundle basename="properties.labels.sales.CaseManagement.GmCaseListRpt"/>

<%
String strSalesCaseMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_CASEMANAGEMENT");
String strApplDateFmt = strGCompDateFmt;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Case List Report  </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strSalesCaseMgmntJsPath%>/GmCaseListRpt.js"></script>

<script>
var format = '<%=strApplDateFmt%>';
var lblFromDate = '<fmtCaseListRpt:message key="LBL_FROM_DATE"/>';
var lblToDate = '<fmtCaseListRpt:message key="LBL_TO_DATE"/>';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" >

<html:form  action="/gmCaseListRptAction.do?method=caseListRpt">
<html:hidden property="strOpt" name="frmCaseListRpt"/>
<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader" ><fmtCaseListRpt:message key="LBL_CASE_LIST_REPORT"/></td>
			<td  class="RightDashBoardHeader" align="right" colspan="3">  
		    </td>
		     <td align="right" class=RightDashBoardHeader>
				<fmtCaseListRpt:message key="IMG_HELP" var="varHelp"/><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("CASE_LIST_REPORT")%>');" />
			</td> 
		</tr>
		<tr><td class="Line" height="1" colspan="6"></td></tr>	
		<tr class="evenshade">
			
			<td class="RightRedCaption" align="Right">&nbsp;<fmtCaseListRpt:message key="LBL_FROM_DATE"/>:</td>
			<td width="180">&nbsp;<gmjsp:calendar SFFormName="frmCaseListRpt" SFDtTextControlName="fromDt"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			</td>
			
			<td class="RightRedCaption" align="Right"><fmtCaseListRpt:message key="LBL_TO_DATE"/>:</td>
			<td width="180">&nbsp;<gmjsp:calendar SFFormName="frmCaseListRpt" SFDtTextControlName="toDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			</td>
			<td class="RightRedCaption" align="Right"><fmtCaseListRpt:message key="LBL_SALES"/>&nbsp;<fmtCaseListRpt:message key="LBL_REP"/>:</td>	
			<td Width="230">&nbsp;<gmjsp:dropdown controlName="rep"  SFFormName='frmCaseListRpt' SFSeletedValue="rep"  defaultValue= "[Choose One]"	
				width="200" SFValue="alRep" codeId="ID" codeName="NM"/></td>
				

	  </tr>
		<tr><td class="LLine" height="1" colspan="6"></td></tr>
		<tr class="oddshade">
			<td  class="RightRedCaption" align="Right" width=7%>&nbsp;<fmtCaseListRpt:message key="LBL_CASE_#"/>:</td>
				<td width="180"> &nbsp;<html:text property="caseId" name="frmCaseListRpt" size="20" onchange="fnUpperCase(this);"/></td>
			<td class="RightRedCaption" align="Right">&nbsp;<fmtCaseListRpt:message key="LBL_FIELD"/>&nbsp;<fmtCaseListRpt:message key="LBL_SALES"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="dist"  SFFormName='frmCaseListRpt' SFSeletedValue="dist"  defaultValue= "[Choose One]"	
				width="250" SFValue="alDist" codeId="ID" codeName="NM" /></td>
			<td class="RightRedCaption" align="Right" >&nbsp;<fmtCaseListRpt:message key="LBL_STATUS"/>:</td>
			<td Width="230">&nbsp;<gmjsp:dropdown controlName="status"  SFFormName='frmCaseListRpt' SFSeletedValue="status"  defaultValue= "[Choose One]"	
				SFValue="alStatus" codeId="CODEID" codeName="CODENM" width="200"/></td>			
			
			<td colspan="2"></td>	
	  </tr>
	  <tr><td class="LLine" height="1" colspan="6"></td></tr>
	  <tr class="evenshade">
						<td class="RightRedCaption" align="Right">&nbsp;<fmtCaseListRpt:message key="LBL_CATEGORIES"/>:</td>
						<td width="180">&nbsp;<gmjsp:dropdown controlName="category"  SFFormName='frmCaseListRpt' SFSeletedValue="category"  defaultValue= "[Choose One]"	
							SFValue="alCategory" codeId="CODEID" codeName="CODENM" width="150"/></td>
						<td   class="RightRedCaption" align="Right" >&nbsp;<fmtCaseListRpt:message key="LBL_ACCOUNT"/>:</td>
						<td width="270">&nbsp;<gmjsp:dropdown controlName="account" width="250" SFFormName='frmCaseListRpt' SFSeletedValue="account"  defaultValue= "[Choose One]"	
							SFValue="alAccount" codeId="ID" codeName="ACNAME"/></td>
						<td ></td>						
						<td align="left">&nbsp;<fmtCaseListRpt:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;${varLoad}&nbsp;"  name="Btn_Submit"   buttonType="Load" onClick=" fnLoad();"/></td>
					
		</tr>
	  
	  <tr><td class="Line" height="1" colspan="6"></td></tr>
		<tr>
			<td colspan="6"> <display:table 
					name="frmCaseListRpt.alResult"  requestURI="/gmCaseListRptAction.do?method=caseListRpt" 
					export="false" class="its" id="currentRowObject"  paneSize="420"
					decorator="com.globus.displaytag.beans.sales.DTCaseListRptWrapper">
					 <fmtCaseListRpt:message key="DT_CASE_#" var="varCase"/><display:column property="CASEID" title="${varCase}" style="width:130"/>
					<fmtCaseListRpt:message key="DT_SURGERY_DATE" var="varSurgeryDate"/><display:column property="SURGERY_TIME" title="${varSurgeryDate}" sortable="true" style="width:110"/>
					<fmtCaseListRpt:message key="DT_CATEGORY" var="varCategory"/><display:column property="CATEGORY" title="${varCategory}" />
					<fmtCaseListRpt:message key="DT_FIELD_SALES" var="varFieldSales"/><display:column property="DISTNM" title="${varFieldSales}"  sortable="true" maxLength="20"/>
					<fmtCaseListRpt:message key="DT_SALES_REP" var="varSalesRep"/><display:column property="REPNM" title="${varSalesRep}"  sortable="true" maxLength="20"/>
					<fmtCaseListRpt:message key="DT_ACCOUNT" var="varAccount"/><display:column property="ACCOUNTNM" title="${varAccount}" maxLength="20"/>
					<fmtCaseListRpt:message key="DT_STATUS" var="varStatus"/><display:column property="STATUS" title="${varStatus}" sortable="true" />
					<fmtCaseListRpt:message key="DT_REQ_SETS" var="varReqSets"/><display:column property="REQSET" title="${varReqSets}"  style="width:40" class="alignright"  />
					<fmtCaseListRpt:message key="DT_ALLOC_SETS" var="varAllocSets"/><display:column property="ALOCSET" title="${varAllocSets}" style="width:40"  class="alignright"  />				
				</display:table> 
			</td>
		</tr>
</table>	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

