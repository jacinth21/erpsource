<!--\sales\CaseManagement\GmCaseScheduleCalendarView.jsp -->
<%
/**********************************************************************************
 * File		 		: GmCaseScheduleCalendarView.jsp
 * Desc		 		: Case Scheduler Calendar page 
 * Version	 		: 1.0
 * Author			: N Raja
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ taglib prefix="frmCaseScheduleCalendar" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<frmCaseScheduleCalendar:setLocale value="<%=strLocale%>"/>
<frmCaseScheduleCalendar:setBundle basename="properties.labels.sales.CaseManagement.GmCaseScheduleCalendarView"/>

 <%String strTodaysDate = (String)session.getAttribute("strSessTodaysDate")==null?"":(String)session.getAttribute("strSessTodaysDate"); 
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();;
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
 %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0">

<bean:define id="fromDt" name="frmCaseScheduleCalendar" property="fromDt" type="java.util.Date"> </bean:define>
<bean:define id="toDt" name="frmCaseScheduleCalendar" property="toDt" type="java.util.Date"> </bean:define>
<bean:define id="applnDateFmt" name="frmCaseScheduleCalendar" property="applnDateFmt" type="java.lang.String"> </bean:define>
<%
String strSalesCaseMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_CASEMANAGEMENT");
String strFromDt = GmCommonClass.parseNull((String)GmCommonClass.getStringFromDate(fromDt,applnDateFmt));
String strToDt = GmCommonClass.parseNull((String)GmCommonClass.getStringFromDate(toDt,applnDateFmt));
%>
<title>Case Scheduler Calendar View</title>
  <script language="JavaScript" src='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler.js'></script>
  <script language="JavaScript" src='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler_ext.js'></script>
  <script language="JavaScript" src='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler_minical.js'></script>
  <script language="JavaScript" src='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler_readonly.js'></script>
  <script language="JavaScript" src='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler_expand.js'></script>  
  <script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js" type="text/javascript"></script>
  <script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js" type="text/javascript"></script>
  <script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"  type="text/javascript"></script>
  <link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
  <link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">
  <link rel='STYLESHEET' type='text/css' href='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler_ext.css'>
  <link rel='STYLESHEET' type='text/css' href='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler.css'>
  <link rel='STYLESHEET' type='text/css' href='<%=strJsPath%>/dhtmlx/dhtmlxCalendar/caseSchedulerCalendar/dhtmlxscheduler_glossy.css'>  
  <link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css"> 
  <script language="JavaScript" src='<%=strJsPath%>/dhtmlx/dhtmlxCalendar/caseSchedulerCalendar/dhtmlxscheduler.js'></script> 
  <script language="JavaScript" src='<%=strJsPath%>/dhtmlx/dhtmlxCalendar/caseSchedulerCalendar/dhtmlxscheduler_tooltip.js'></script> 
  <script language="JavaScript" src='<%=strJsPath%>/dhtmlx/dhtmlxCalendar/caseSchedulerCalendar/dhtmlxscheduler_minical.js'></script> 
  <script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
  <script language="JavaScript" src="<%=strSalesCaseMgmntJsPath%>/GmCaseScheduleCalendarView.js"></script> 
  <script language="JavaScript" src="<%=strJsPath%>/jquery-latest.js"></script> 

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<style>
.span-bg{
    width:10px;
    height:10px;
    display:inline-block;
}
 .dhtmlXTooltip.tooltip{
    right:inherit !important;
    background-color: white;
    border-left: 1px dotted #887A2E;
    border-top: 1px dotted #887A2E;
    color: #000000 !important;
    cursor: default;
    z-index: 500;
    position: absolute;
    width: 30em;
    height: 20em; 
 }
 .dhx_minical_popup{
 position: absolute; z-index: 10100; width: 251px; height: 175px;
 }
 
 .btn-group{
     top: 1px;
     right: 210px;
 }
 
.btn-group a{
    background-color: #fff;
    font-family: Trebuchet MS, arial, sans-serif;
    font-size: 12px;
    font-weight: bold;
    COLOR: #2E73AC;
    cursor: pointer;
    padding: 2px 10px;
    border: 1px solid #2E73AC;
    float: left;
 }
 
 
.btn-group a.actives{
 background-color: #2E73AC;
 color:  #fff;
 }
 
 .btn-group .actives, .btn-group a:hover {
  background-color: #2E73AC;
  color:  #fff;
}
.btn-group  a:hover, a.actives:visited ,  a.actives:hover {
 text-decoration: none; 
 }
.btn-group a:first-child{
  border-radius: 9px 0px 0px 9px !important;
}
.btn-group a:last-child{
  border-radius: 0px 9px 9px 0px !important;
}
 
</style>
	
<script type="text/javascript" charset="utf-8">
	var fromDate = '<%=strFromDt%>';
	var toDate = '<%=strToDt%>';
	var sessApplDateFmt = '<%=applnDateFmt%>';
	var companyId ='<%=strCompanyId%>';
</script>

</head>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad(sessApplDateFmt,'shortTerm');">
 <html:form action="/gmCaseScheduleCalendar.do?method=fatchCaseSchedularCalendar"> 
 <html:hidden property="strOpt" name="frmCaseScheduleCalendar" /> 
<html:hidden property="fromDt" name="frmCaseScheduleCalendar" value="'<%=strFromDt%>'" />
<html:hidden property="toDt" name="frmCaseScheduleCalendar"  value="'<%=strFromDt%>'" />
<html:hidden property="ccMonth" name="frmCaseScheduleCalendar" />
<html:hidden property="ccYear" name="frmCaseScheduleCalendar" />
<html:hidden property="haction" name="frmCaseScheduleCalendar" /> 
	<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
			<tr><td colspan="5" height="25" class="RightDashBoardHeader"><frmCaseScheduleCalendar:message key="LBL_CASE_CALENDER"/></td>
			<td align="right" class=RightDashBoardHeader>
		  <frmCaseScheduleCalendar:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("CASE_SCHEDULER_CALENDAR")%>');" /> 
			</td> 
			</tr>
			<tr> <td colspan="6" bgcolor="#666666"></td>
			</tr>
	<tr><td colspan="6">
	<div id="caseScheduleCalender" class="dhx_cal_container" style='width:800; height:690;'>
		<div class="dhx_cal_navline">
		    <div class="dhx_cal_today_button"></div>
			<div class="dhx_cal_prev_button">&nbsp;</div>
			<div class="dhx_cal_next_button">&nbsp;</div>
			<div class="dhx_cal_date"></div>
			<div class="dhx_minical_icon" id="dhx_minical_icon" onclick="show_minical()">&nbsp;</div>
			<div class="btn-group" id="groupBtn">
	          <a class="span-btn actives" onclick="fnBasedOnTerm(sessApplDateFmt,'shortTerm'); fnActiveButton(event);"><frmCaseScheduleCalendar:message key="LBL_SHORT_TERM"/></a>
	          <a class="span-btn" onclick="fnBasedOnTerm(sessApplDateFmt,'viewAll'); fnActiveButton(event);"><frmCaseScheduleCalendar:message key="LBL_VIEW_ALL"/></a>
	          <a class="span-btn" onclick="fnBasedOnTerm(sessApplDateFmt,'longTerm'); fnActiveButton(event);"><frmCaseScheduleCalendar:message key="LBL_LONG_TERM"/></a>
            </div>
			<div class="dhx_cal_tab" name="month_tab" style="right:132px;"></div> 
			<div class="dhx_cal_tab" name="week_tab" style="right:69px;"></div>
			<div class="dhx_cal_tab" name="day_tab" style="right:5px;"></div> 
		</div> 
		<div class="dhx_cal_header"></div>
		<div class="dhx_cal_data"></div>
	</div>
	</td>
	</tr>
	
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>