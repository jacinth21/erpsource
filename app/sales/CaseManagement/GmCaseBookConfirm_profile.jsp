<!-- \sales\CaseManagement\GmCaseBookConfirm_profile.jsp  -->
 
 
 <%@ include file="/common/GmHeader.inc" %>
 <%@ taglib prefix="fmtCaseBookConfirm_profile" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtCaseBookConfirm_profile:setLocale value="<%=strLocale%>"/>
<fmtCaseBookConfirm_profile:setBundle basename="properties.labels.sales.CaseManagement.GmCaseBookConfirm_profile"/>
 <%
 String strPartyJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PARTY");
 %>
<HTML>
<HEAD>
<TITLE> Globus Medical: Case Book </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strPartyJsPath%>/GmParty.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>

 <link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"  type="text/javascript"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js" type="text/javascript"></script>
 
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js" type="text/javascript"></script> 

 <script>

var dhxWins,w1;

function doOnLoad(setid,setnm) {
 
    dhxWins = new dhtmlXWindows();
    dhxWins.enableAutoViewport(false);
    dhxWins.attachViewportTo("winVP");
 
    w1 = dhxWins.createWindow("w1", 30, 40, 600, 1000);
    w1.setText(message_sales[242]); 
    var ajaxUrl = fnAjaxURLAppendCmpInfo("/GmSetReportServlet?hOpt=SETRPT&hAction=Drilldown&hSetId="+setid+ "&hSetNm="+setnm);
	w1.attachURL(ajaxUrl);
}
 </script>
</HEAD>
 
<BODY leftmargin="20" topmargin="10" >
<FORM name="frmVendor" method="POST" action="">
 
 
 <div id="winVP" style="position: relative;  height: 1500px;   margin: 10px;">
	<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr >
						<td height="25" colspan="2" class="RightDashBoardHeader">&nbsp;<fmtCaseBookConfirm_profile:message key="LBL_CASE_BOOKING_CONFIRM"/></td>
						 
					</tr>
					<tr><td class="Line" height="1" colspan="2"></td></tr>
					 
					<tr class="evenshade">
						  <td  class="RightTableCaption" align="left" colspan="2"><font color="red"><fmtCaseBookConfirm_profile:message key="LBL_REVIEW_THE_INFORMATION"/>.<br> <fmtCaseBookConfirm_profile:message key="LBL_ALL_INFORMATION_IS_CORRECT"/>,<fmtCaseBookConfirm_profile:message key="LBL_CLICK_SAVE"/> </font></td>
						 
					</tr>
				 	
					 
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="ShadeRightTableCaption" HEIGHT="24">
						  <td  class="RightTableCaption" align="left"> 
							     <fmtCaseBookConfirm_profile:message key="LBL_ATTRIBUTE_DETAILS"/>&nbsp; 
						 </td>
						<td width="320">&nbsp; 
						</td>
					</tr>					
					<tr><td class="LLine" height="1" colspan="4"></td></tr>
					<tr  >
						  <td  class="RightTableCaption" align="right" height="25"  colspan="2">
						  <table border="0" width="100%" cellspacing="0" cellpadding="0"  >
						  <tr  ><td class="RightTableCaption" align="right" height="25" width="18%">
						       <fmtCaseBookConfirm_profile:message key="LBL_APPROACH"/>:
						 </td>
						   <td    align="left" height="25"  width="32%">
						        &nbsp; <fmtCaseBookConfirm_profile:message key="LBL_ANTERIOR"/>
						 </td>
						  <td  class="RightTableCaption" align="right" height="25"  width="25%">
						       <fmtCaseBookConfirm_profile:message key="LBL_REGION"/>: 
						 </td>
						  <td   align="left" height="25"  width="25%">
						       &nbsp; <fmtCaseBookConfirm_profile:message key="LBL_CERVICAL"/>
						 </td>
						 
						</tr> </table>
					 </td>
						 
					</tr> 	
					 			
					<tr><td class="LLine" height="1" colspan="4"></td></tr>
					<tr class="oddshade" >
						  <td  class="RightTableCaption" align="right" height="25"  colspan="2">
						  <table border="0" width="100%" cellspacing="0" cellpadding="0"  >
						  <tr  ><td class="RightTableCaption" align="right" height="25" width="18%">
						       <fmtCaseBookConfirm_profile:message key="LBL_INTERBODY"/>:
						 </td>
						   <td  class="RightText" align="left" height="25"  width="32%">
						        &nbsp; <fmtCaseBookConfirm_profile:message key="LBL_YES"/>
						 </td>
						  <td  class="RightTableCaption" align="right" height="25"  width="25%">
						       <fmtCaseBookConfirm_profile:message key="LBL_BOLOGIC"/>: 
						 </td>
						  <td  class="RightText" align="left" height="25"  width="25%">
						       &nbsp; <fmtCaseBookConfirm_profile:message key="LBL_NO"/>
						 </td>
						 
						</tr> </table>
					 </td>
						 
					</tr>	

					<tr><td class="LLine" height="1" colspan="4"></td></tr>
					<tr  >
						  <td  class="RightTableCaption" align="right" height="25"  colspan="2">
						  <table border="0" width="100%" cellspacing="0" cellpadding="0"  >
						  <tr  ><td class="RightTableCaption" align="right" height="25" width="18%">
						       <fmtCaseBookConfirm_profile:message key="LBL_FIXATION"/>:
						 </td>
						   <td  class="RightText" align="left" height="25"  width="32%">
						        &nbsp; <fmtCaseBookConfirm_profile:message key="LBL_YES"/>
						 </td>
						  <td  class="RightTableCaption" align="right" height="25"  width="25%">
						      <fmtCaseBookConfirm_profile:message key="LBL_LEVELS_TREATED"/>:
						 </td>
						  <td  class="RightText" align="left" height="25"  width="25%">
						       &nbsp; <fmtCaseBookConfirm_profile:message key="LBL_C1_TO_C2(1)"/>
						 </td>
						 
						</tr> </table>
					 </td>
						 
					</tr>	
				 	
						  
					<tr><td class="LLine" height="1" colspan="4"></td></tr>
					<tr   class="ShadeRightTableCaption" HEIGHT="24" >
						  <td  align="left"> 
							    <fmtCaseBookConfirm_profile:message key="LBL_PRODUCT_SYSTEM"/>&nbsp; 
						 </td>
						<td width="320">&nbsp; 
						</td>
					</tr>				
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					 
					 <tr>
						<td colspan="6">
							<table border="0" width="100%" cellspacing="0" cellpadding="0" id="tabRecv"  >
							<tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>
							<tr  bgcolor="#e4e6f2" >
								 
									<td class="RightTableCaption" align="center" HEIGHT="24"   width="20%"><fmtCaseBookConfirm_profile:message key="LBL_SET_ID"/></td>
									<td class="RightTableCaption" align="center" HEIGHT="24" colspan="2" width="35%"><fmtCaseBookConfirm_profile:message key="LBL_SET_NAME"/></td>
									<td class="RightTableCaption" align="left" HEIGHT="24"   width="15%"><fmtCaseBookConfirm_profile:message key="LBL_FAMILY"/></td>
									<td class="RightTableCaption" align="left" HEIGHT="24"   width="15%"><fmtCaseBookConfirm_profile:message key="LBL_TYPE"/></td>
									<td class="RightTableCaption" align="left" HEIGHT="24"   width="15%"><fmtCaseBookConfirm_profile:message key="LBL_QTY"/></td>
								 
									 	</tr>
								<tr><td class="LLine" height="1" colspan="6"></td></tr>
								 
								<tr  >
									 
									<td  align="center" HEIGHT="24"   width="20%"><a href ="javascript:doOnLoad('961.901', 'XTEND Instrument Set')"><fmtCaseBookConfirm_profile:message key="LBL_961.901"/> </a> </td>
									<td    align="left" HEIGHT="24" colspan="2" width="35%"><fmtCaseBookConfirm_profile:message key="LBL_XTEND_INSTRUMENT_SET"/></td>
									<td   align="left" HEIGHT="24"   ><fmtCaseBookConfirm_profile:message key="LBL_INSTRUMENT"/></td>
									<td   align="left" HEIGHT="24"   ><fmtCaseBookConfirm_profile:message key="LBL_STANDARD"/></td>
									<td   align="left" HEIGHT="24"   ><fmtCaseBookConfirm_profile:message key="LBL_1"/></td>
								</tr>	
								
								<tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>
								<tr class="Shade" >
									 
									<td  align="center" HEIGHT="24"   width="20%"><a href ="javascript:doOnLoad('961.906', 'XTEND Screw Set')"><fmtCaseBookConfirm_profile:message key="LBL_961.906"/></a> </td>
									<td   align="left" HEIGHT="24" colspan="2" width="35%"><fmtCaseBookConfirm_profile:message key="LBL_XTEND_SCREW_SET"/></td>
									<td   align="left" HEIGHT="24"   ><fmtCaseBookConfirm_profile:message key="LBL_IMPLANTS"/></td>
									<td   align="left" HEIGHT="24"   ><fmtCaseBookConfirm_profile:message key="LBL_STANDARD"/></td>
									<td   align="left" HEIGHT="24"   ><fmtCaseBookConfirm_profile:message key="LBL_1"/></td>
							 	</tr>
									 	
								<tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>
								<tr > 
									<td   align="center" HEIGHT="24"   width="15%"><a href ="javascript:doOnLoad('961.902', 'XTEND Cervical Place Set')"><fmtCaseBookConfirm_profile:message key="LBL_961.902"/></a> </td>
									<td   align="left" HEIGHT="24" colspan="2" width="35%"><fmtCaseBookConfirm_profile:message key="LBL_XTEND_CERVICAL_PLACE_SET"/></td>
									<td   align="left" HEIGHT="24"   ><fmtCaseBookConfirm_profile:message key="LBL_IMPLANTS"/></td>
									<td   align="left" HEIGHT="24"   ><fmtCaseBookConfirm_profile:message key="LBL_STANDARD"/></td>
									<td   align="left" HEIGHT="24"   ><fmtCaseBookConfirm_profile:message key="LBL_1"/></td>
							 	</tr>
								
								<tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>
								<tr class="Shade"> 
									<td   align="center" HEIGHT="24"   width="15%"><a href ="javascript:doOnLoad('961.912', 'Extender Plate Set')"><fmtCaseBookConfirm_profile:message key="LBL_961.912"/></a></td>
									<td  align="left" HEIGHT="24" colspan="2" width="35%"><fmtCaseBookConfirm_profile:message key="LBL_EXTENDER_PLATE_SET"/></td>
									<td   align="left" HEIGHT="24"   ><fmtCaseBookConfirm_profile:message key="LBL_IMPLANTS"/></td>
									<td   align="left" HEIGHT="24"   ><fmtCaseBookConfirm_profile:message key="LBL_ADDITIONAL"/></td>
									<td   align="left" HEIGHT="24"   ><fmtCaseBookConfirm_profile:message key="LBL_1"/></td>
							 	</tr>  
								
							</table>
						</td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr  >
						  <td  class="RightTableCaption" align="left" height="25" colspan="2">
						       
						 </td>
						 
					</tr> 
					 
					 <tr><td class="LLine" height="1" colspan="2"></td></tr>
					  
					<tr class="evenshade">
						<td colspan="2"> 
							
 
 
  
 
						</td>
					</tr>
				   <tr>
						<td  height="40" colspan="2" align="center">&nbsp;<fmtCaseBookConfirm_profile:message key="BTN_BACK" var="varBack"/>
							<gmjsp:button style="width: 5em" value="${varBack}" buttonType="Load" onClick="window.location='/sales/CaseManagement/GmCaseBookSetDetails_profile.jsp'"/>&nbsp;
							<fmtCaseBookConfirm_profile:message key="BTN_SAVE" var="varSave"/><gmjsp:button style="width: 8em" value="${varSave}" buttonType="Save" />&nbsp;
						 
						</td>
					</tr>
				</table>
			</td>
		</tr>
 
    </table></div>
</FORM>
     
 
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
 
</HTML>
 

