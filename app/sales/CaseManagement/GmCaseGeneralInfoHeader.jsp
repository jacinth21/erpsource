<!-- \sales\CaseManagement\GmCaseGeneralInfoHeader.jsp -->
<%
/**********************************************************************************
 * File		 		: GmCaseGeneralInfoHeader.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Xun
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%> 
<%@ taglib prefix="fmtCaseGeneralInfoHeader" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtCaseGeneralInfoHeader:setLocale value="<%=strLocale%>"/>
<fmtCaseGeneralInfoHeader:setBundle basename="properties.labels.sales.CaseManagement.GmCaseGeneralInfoHeader"/>

<%    
 String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>
<bean:define id="hmGenerlInfo" name="<%=strFormName%>"  property="hmGenerlInfo" type="java.util.HashMap"></bean:define> 
<%     
 String strDist = GmCommonClass.parseNull((String) hmGenerlInfo.get("DISTNM"));
 String strRep = GmCommonClass.parseNull((String) hmGenerlInfo.get("REPNM"));
 String strSurgeryDT = GmCommonClass.parseNull((String) hmGenerlInfo.get("SURGERY_TIME"));
 String strCaseStatus	 = GmCommonClass.parseNull((String) hmGenerlInfo.get("STATUS"));
 String strAcct	 = GmCommonClass.parseNull((String) hmGenerlInfo.get("ACCOUNTNM")); 
 String strCaseID	 = GmCommonClass.parseNull((String) hmGenerlInfo.get("CASEID"));
 String strAMPM	 = GmCommonClass.parseNull((String) hmGenerlInfo.get("SURGERYAMPM"));
 String strParentCaseID	 = GmCommonClass.parseNull((String) hmGenerlInfo.get("PARENTCASEID")); 
 String strDOID	 = GmCommonClass.parseNull((String) hmGenerlInfo.get("DOID")); 
  
%>  
 <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
 
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
 	 	
		<tr class="ShadeRightTableCaption" HEIGHT="24">
						  <td  class="RightTableCaption" align="left" colspan="2"> 
							    <fmtCaseGeneralInfoHeader:message key="LBL_GENERAL_INFORMATION"/>&nbsp; 
						 </td>
						 
					</tr>					
					<tr><td class="LLine" height="1" colspan="4"></td></tr>
					<tr  >
						  <td  class="RightTableCaption" align="right" height="25"  colspan="2">
						  <table border="0" width="100%" cellspacing="0" cellpadding="0"  >
						  <tr  ><td class="RightTableCaption" align="right" height="25" width="18%" >
						       <fmtCaseGeneralInfoHeader:message key="LBL_FIELD_SALES"/> :
						 </td>
						   <td    align="left" height="25"  width="32%" >
						        &nbsp; <%=strDist%>	 
						 </td>
						  <td  class="RightTableCaption" align="right" height="25"  width="25%" >
						       <fmtCaseGeneralInfoHeader:message key="LBL_SALES_REP"/> : 
						 </td>
						  <td    align="left" height="25"  width="25%" >
						       &nbsp; <%=strRep%>
						 </td>
						 
						</tr> </table>
					 </td>
						 
					</tr> 	
					 			
					<tr><td class="LLine" height="1" colspan="4"></td></tr>
					<tr class="oddshade" >
						  <td  class="RightTableCaption" align="right" height="25"  colspan="2">
						  <table border="0" width="100%" cellspacing="0" cellpadding="0"  >
						  <tr  ><td class="RightTableCaption" align="right" height="25" width="18%">
						       <fmtCaseGeneralInfoHeader:message key="LBL_SURGERY_DATE"/> :
						 </td>
						   <td   align="left" height="25"  width="32%">
						        &nbsp; <%=strSurgeryDT%>&nbsp; <%=strAMPM%>
						 </td>
						  <td  class="RightTableCaption" align="right" height="25"  width="25%">
						       <fmtCaseGeneralInfoHeader:message key="LBL_CASE_STATUS"/> : 
						 </td>
						  <td    align="left" height="25"  width="25%">
						       &nbsp;  <%=strCaseStatus%> 
						 </td>
						 
						</tr> </table>
					 </td>
						 
					</tr>	
					<tr><td class="LLine" height="1" colspan="4"></td></tr>
					<tr  >
						  <td  class="RightTableCaption" align="right" height="25"  colspan="2">
						  <table border="0" width="100%" cellspacing="0" cellpadding="0"  >
						  <tr  ><td class="RightTableCaption" align="right" height="25" width="18%">
						       <fmtCaseGeneralInfoHeader:message key="LBL_ACCOUNT"/>:
						 </td>
						   <td   align="left" height="25"  width="32%">
						        &nbsp;  <%=strAcct%>
						 </td>
						  <td  class="RightTableCaption" align="right" height="25"  width="25%">
						       <fmtCaseGeneralInfoHeader:message key="LBL_CASE_ID"/> : 
						 </td>
						  <td    align="left" height="25"  width="25%">
						       &nbsp;  <%=strCaseID%>
						 </td>
						 
						</tr> </table>
					 </td>
						 
					</tr>
					
					<tr><td class="LLine" height="1" colspan="4"></td></tr>
					<tr class="oddshade" >
						  <td  class="RightTableCaption" align="right" height="25"  colspan="2">
						  <table border="0" width="100%" cellspacing="0" cellpadding="0"  >
						  <tr  ><td class="RightTableCaption" align="right" height="25" width="18%">
						       <fmtCaseGeneralInfoHeader:message key="LBL_DO_ID_#"/> :
						 </td>
						   <td   align="left" height="25"  width="32%">&nbsp;<a href="#" onClick ="fnFetchDOSummary('<%=strDOID %>')";><%=strDOID %></a>
						 </td>
						  <td  class="RightTableCaption" align="right" height="25"  width="25%">
						       <fmtCaseGeneralInfoHeader:message key="LBL_PARENT_CASE_ID"/> : 
						 </td>
						  <td    align="left" height="25"  width="25%">
						       &nbsp;  <%=strParentCaseID%> 
						 </td>
						 
						</tr> </table>
					 </td>	 
  
 <%@ include file="/common/GmFooter.inc" %>

