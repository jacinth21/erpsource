<!-- \sales\CaseManagement\GmCaseBookSetDetails.jsp  -->
 <%
	/**********************************************************************************
	 * File		 		: GmCaseBookSetDetails.jsp
	 * Desc		 		: This screen is used for Case Book Set Details
	 * Version	 		: 1.0
	 * author			: Xun
	 * 
	 ************************************************************************************/
%> 
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%> 
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib prefix="fmtCaseBookSetDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtCaseBookSetDetails:setLocale value="<%=strLocale%>"/>
<fmtCaseBookSetDetails:setBundle basename="properties.labels.sales.CaseManagement.GmCaseBookSetDetails"/>

<bean:define id="alSystemSets" name="frmCaseBookSetDtls" property="alSystemSets" type="java.util.List"></bean:define>
<bean:define id="alBookedSets" name="frmCaseBookSetDtls" property="alBookedSets" type="java.util.List"></bean:define>
<bean:define id="caseInfoID" name="frmCaseBookSetDtls" property="caseInfoID" type="java.lang.String"></bean:define>
<bean:define id="productionType" name="frmCaseBookSetDtls" property="productionType" type="java.lang.String"></bean:define>
<bean:define id="fwdProductType" name="frmCaseBookSetDtls" property="fwdProductType" type="java.lang.String"></bean:define>
<bean:define id="caseID" name="frmCaseBookSetDtls" property="caseID" type="java.lang.String"></bean:define> 
<bean:define id="favouriteCaseID" name="frmCaseBookSetDtls" property="favouriteCaseID" type="java.lang.String"></bean:define>
<bean:define id="screenTitle" name="frmCaseBookSetDtls" property="screenTitle" type="java.lang.String"></bean:define>
<bean:define id="fromscreen" name="frmCaseBookSetDtls" property="fromscreen" type="java.lang.String"></bean:define>
<bean:define id="screenNote" name="frmCaseBookSetDtls" property="screenNote" type="java.lang.String"></bean:define>

<%
String strSalesCaseMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_CASEMANAGEMENT");
GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.sales.CaseManagement.GmCaseBookSetDetails", strSessCompanyLocale);
	ArrayList alListSys = new ArrayList();
	ArrayList alListBookedSet = new ArrayList();	
	 
	alListSys = GmCommonClass.parseNullArrayList((ArrayList) alSystemSets);  //System list
	alListBookedSet = GmCommonClass.parseNullArrayList((ArrayList) alBookedSets);  //Set list
	int rowsize1 = alListSys.size();
	int rowsize2 = alListBookedSet.size(); 
	
	HashMap hmSysLoop = new HashMap();
	HashMap hmSetLoop = new HashMap();
	HashMap hmSysLoopNxt = new HashMap();
	
	String strSystemId = "";
	String strSystemNm = "";
	String strSysSetId = "";
	String strSetNm = ""; 
	String strFamily = "";
	String strType = "";
	String strSetId = "";
	String tempsystem ="";
	String strNextSys =""; 
	String title ="";
    String wikiTitle ="";
    String header="";
    String headerInfo = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PROCEDURES"));
    String fwtscreenNote = screenNote;
    int qtyMaxCount=3;
  //PC-5609 - company information not passing in edge browser after submit or redirect the page, so get company info and form companyInfo JSON string in JSP ANd set into form variable
    String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\",\"cmplangid\":\""+gmDataStoreVO.getCmplangid()+"\",\"cmpdfmt\":\""+strGCompDateFmt+"\"}";
  
    if(screenNote.equals("")){
    	fwtscreenNote = "";
    }
   
    if(fromscreen.equals("eventsetup"))
    {
    	title = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_EVENT_BOOKING_RESOURCE"));
		wikiTitle ="EVENT_BOOK_RESOURCE_SELECTION";
		header=GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PRODUCT_SYSTEM_MANDATORY"));
		headerInfo = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_EVENT"));
		qtyMaxCount=10;
    }
    else 
    	{
    	header=GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PRODUCT_SYSTEM_MANDATORY_CASE"));
    	if(!caseInfoID.equals(""))
    	{
		title = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_CASE_BOOK_PRODUCT_SELECTION"));
		wikiTitle ="CASE_BOOK_PRODUCT_SELECTION";
		}
		else
		{
		title = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PROFILE_SELECTION"));
		wikiTitle = "PROFILE_PRODUCT_SELECTION";
		}
    	}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Case Book </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
 
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script> 

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"  type="text/javascript"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js" type="text/javascript"></script>
 
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js" type="text/javascript"></script> 
<script language="JavaScript" src="<%=strSalesCaseMgmntJsPath%>/GmCaseBookSetDetails.js"></script>
 

<script>
var size ="<%=rowsize1 %>";
var setLen = <%=alBookedSets.size()%>;
var type = "<%=fwdProductType%>";
var strCaseInfoID = "<%=caseInfoID%>";
var strcaseID = "<%=caseID%>";
var strFavcaseID = "<%=favouriteCaseID%>";
var fromscreen = "<%=fromscreen%>";
<%
HashMap hmSetVal = new HashMap();
for (int i=0;i<alBookedSets.size();i++)
{
	hmSetVal = (HashMap)alBookedSets.get(i);
%>
var	SetArr<%=i%> = "<%=hmSetVal.get("SETID")%>,<%=hmSetVal.get("QTY")%> " ;
<%
}
%>
function doOnLoad(setid,setnm) {	
	var setText = '';
	if(fromscreen == "eventsetup"){
		setText = message_sales[243];
	}else{
		setText = message_sales[244];
	}	
	w1 = createDhtmlxWindow(930,680);
	w1.setText(setText);   
	var ajaxUrl = fnAjaxURLAppendCmpInfo("/GmSetReportServlet?hOpt=SETRPT&hAction=Drilldown&hSetId="+setid+ "&hSetNm="+setnm);
	w1.attachURL(ajaxUrl);
	return false;
}


function fnCaseVoid(){
	 
            document.frmCaseBookSetDtls.hCancelType.value = 'VDCASE';           
            document.frmCaseBookSetDtls.hTxnId.value = strCaseInfoID; 
            document.frmCaseBookSetDtls.hTxnName.value = strcaseID; 
            document.frmCaseBookSetDtls.action ="/GmCommonCancelServlet";            
            document.frmCaseBookSetDtls.hAction.value = "Load";                     
            document.frmCaseBookSetDtls.submit();
      }

 </script>
</HEAD>
 
<BODY leftmargin="20" topmargin="10"  onload="fnCheckSelections();">
<html:form action="/gmCaseBookSetDtls.do" >
<html:hidden property="strOpt"  />
 <html:hidden property="caseInfoID"  />
 <html:hidden property="caseID"  />
 <html:hidden property="favouriteCaseID"   />
 <html:hidden property="hinputStr"   />
 <html:hidden property="fromscreen"   value="<%=fromscreen%>" />
 <html:hidden property="productionType" value="<%=fwdProductType%>"  />
 <html:hidden property="categoryList"   />
 <html:hidden property="fwdProductType"   />
 <!--  PC-5609 - set the companyInfo in hidden variable  -->
 <html:hidden property="companyInfo" value="<%=strCompanyInfo%>"/> 
 <input type="hidden" name="hTxnId" value=""/>
<input type="hidden" name="hTxnName" value=""/>
<input type= "hidden" name="hAction" value=""/>
<input type= "hidden"  name="hCancelType" value=""/>
<html:hidden property="backScreen" />

   <div id="winVP" style="position: relative;  height: 1500px;   margin: 10px;">
	<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr >
						<td height="25" colspan="1" class="RightDashBoardHeader">&nbsp;<%=title %> <%=screenTitle%></td>
						 <td align="right" class=RightDashBoardHeader>
						<fmtCaseBookSetDetails:message key="IMG_HELP" var="varHelp"/><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle(wikiTitle)%>');" />
						</td> 
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<logic:notEqual name="frmCaseBookSetDtls" property="caseInfoID" value=""> 
					<tr class="evenshade">
						  <td  class="RightTableCaption" align="left" colspan="2"><font color="red"><%=header%></font></td>
						 
					</tr>
					</logic:notEqual>
					<tr class="evenshade">
						  <td  class="RightTableCaption" align="left" colspan="2"><font color="red"><fmtCaseBookSetDetails:message key="LBL_SELECT_PRODUCT"/> <%=headerInfo%></font></td>
						 
					</tr>
					 
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					
				 
					 <tr>
						<td colspan="6">
							<table border="0" width="100%" cellspacing="0" cellpadding="0"   >
					<%
					int k = 0, j = 0, syscount = 0 ;
						for (int i = 0;i < rowsize1 ;i++ )
								{ 	
									hmSysLoop = (HashMap)alListSys.get(i);
									 
									 if (i< (rowsize1 -1))
									 {
										 hmSysLoopNxt = (HashMap)alListSys.get(i+1);
										 strNextSys = GmCommonClass.parseNull((String)hmSysLoopNxt.get("SYSTEMID")); 
										}
									  
									strSystemNm = GmCommonClass.parseNull((String)hmSysLoop.get("SYSTEMNM"));
									strSystemId = GmCommonClass.parseNull((String)hmSysLoop.get("SYSTEMID"));
									strSysSetId = GmCommonClass.parseNull((String)hmSysLoop.get("SETID")); 
									strSetNm = GmCommonClass.parseNull((String)hmSysLoop.get("SETNM"));
									strFamily = GmCommonClass.parseNull((String)hmSysLoop.get("FAMILY"));
									strType = GmCommonClass.parseNull((String)hmSysLoop.get("STYPE")); 
									strSetId = GmCommonClass.parseNull((String)hmSysLoop.get("SETID"));  
									
									if (!tempsystem.equals(strSystemId))
									{  j = 1;
										syscount++;
									%>
							<tr  class="oddshade" >
							<%if(!fromscreen.equals("eventsetup")){%>
								<td  class="RightTableCaption" colspan="8" align="left">  
							       	<input type="checkbox" name='sysCheck<%=strSystemId.replace(".","")%>'  onclick="fnShow('div<%=strSystemId %>','<%=strSystemId %>', 'click');">
								    <%=strSystemNm %> &nbsp; 
								</td>	   
							<%}else{ %>
								<td  class="RightTableCaption" colspan="8" align="left">  
							       	<input type="checkbox" name='sysCheck<%=strSystemId.replace("-","")%>'  onclick="fnShow('div<%=strSystemId %>','<%=strSystemId %>', 'click');">
								    <%=strSystemNm %> &nbsp; 
								</td>	   
							<%} %>
								
									<td  >&nbsp; 
									</td>
								</tr>		
							<tr><td colspan="9" height="1" bgcolor="#cccccc"></td></tr>
							<tr    >
							<td colspan="9">
							<table border="0" width="100%" cellspacing="0" cellpadding="0"  id="div<%=strSystemId %>"  style="display:none"><tr  bgcolor="#e4e6f2" >
									<td class="RightTableCaption" align="left" HEIGHT="24" width="5%"  ><fmtCaseBookSetDetails:message key="LBL_#"/></td>
									<td class="RightTableCaption" align="left" HEIGHT="24"   width="15%"><fmtCaseBookSetDetails:message key="LBL_SET_ID"/></td>
									<td class="RightTableCaption" align="left" HEIGHT="24" colspan="3" width="35%"><fmtCaseBookSetDetails:message key="LBL_SET_NAME"/></td>
									 <%if(!fromscreen.equals("eventsetup"))
    								{%>
									<td class="RightTableCaption" align="left" HEIGHT="24"   width="20%"><fmtCaseBookSetDetails:message key="LBL_FAMILY"/></td>
									<td class="RightTableCaption" align="left" HEIGHT="24"   width="10%"><fmtCaseBookSetDetails:message key="LBL_TYPE"/></td>
									<%}%>
									<td class="RightTableCaption" align="center" HEIGHT="24"   ><fmtCaseBookSetDetails:message key="LBL_QTY"/></td>
									<td class="RightTableCaption" align="center" HEIGHT="24"   ><fmtCaseBookSetDetails:message key="LBL_SELECT"/></td>
									 	</tr>
								<tr  ><td class="LLine" height="1" colspan="9"></td></tr>
						<%
									}
									
							 %>	
							 
								<tr   id="tr<%=i%>"  onMouseover="fnProcessCheckRowHighlight(tr<%=i%>,<%=i%>)"  onMouseout="fnProcessCheckRestoreBgColor(tr<%=i%>,<%=i%>)"   >
									<td   align="left" HEIGHT="24"  width="5%" ><%=j%>. </td>
									<td   align="left" HEIGHT="24"   width="15%">  <a href ="#" onClick="return doOnLoad('<%=strSetId%>','<%=strSetNm%>')">  
									<input type=hidden value='<%=strSetId%>' name="Set<%=k%>"> <%=strSetId%></a>
									<input type=hidden value='<%=strSystemId%>' name="System<%=k%>">
									<input type=hidden value='<%=strType%>' name="Systype<%=k%>"></td>
									<td   align="left" HEIGHT="24" colspan="3" width="35%"> <%=strSetNm%></td>
									 <%if(!fromscreen.equals("eventsetup"))
    								{%>
									<td   align="left" HEIGHT="24"  width="15%"  ><%=strFamily%></td>
									<td   align="left" HEIGHT="24"   ><%=strType%></td>
									<%}%>
									<td   align="center" HEIGHT="24"  width="15%" ><select name="cbo_setQty<%=k%>" id="" class="RightText"    onChange="return fnQtyCheckSelections('<%=fromscreen%>','<%=k%>')"> 
									<% 
									for (int x = 1; x<=qtyMaxCount ;x++ ) {%>
									<option value=<%=x%> id=1><%=x%></option>
									<%} %>
									</select>	
									  </td> 
									<td class="RightTableCaption" align="center" HEIGHT="24" width="10%"  > 	<input type="checkbox" name="check<%=k%>" onclick="fnUnCheckSelections('<%=strSystemId%>');" >  </td>
									 	</tr>	
								
								<tr><td colspan="9" height="1" bgcolor="#cccccc"></td></tr>
								
						<%		 	 
							if ((tempsystem.equals(strSystemId)&&!strNextSys.equals(strSystemId))||(i== rowsize1 -1)||(!tempsystem.equals(strSystemId)&&!strNextSys.equals(strSystemId)))
							{ 
						%>
								</table>
							</td>
						</tr>
						
						<% 
							}
						tempsystem = strSystemId;
						k ++;
						j ++;
						}
						%>		
							  
								
							</table>
						</td>
					</tr>					
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr> <td class="RightTableCaption" align="left" height="10" colspan="2"> </td>						 
					</tr> 
				<% if((fromscreen.equals("eventsetup")) && (!screenNote.equals(""))) {%>
				<tr>
					<td> <font color="red"><fmtCaseBookSetDetails:message key="LBL_NOTE"/> <b> <%=fwtscreenNote%> </b><fmtCaseBookSetDetails:message key="LBL_SELECTION_SCREEN"/></font></td>
				</tr>
				<%} %>  
				   <tr>
						<td  height="40" colspan="2" align="center">&nbsp;
						 <logic:notEqual name="frmCaseBookSetDtls" property="caseInfoID" value="">  	
							<fmtCaseBookSetDetails:message key="BTN_BACK" var="varBack"/><gmjsp:button style="width: 7em" value="${varBack}" buttonType="Load" onClick="fnBack();" />&nbsp;
							 <logic:notEqual name="frmCaseBookSetDtls" property="fromscreen" value="eventsetup"> 
							<fmtCaseBookSetDetails:message key="BTN_DELETE" var="varDelete"/><gmjsp:button style="width: 7em" value="${varDelete}" buttonType="Save" onClick="fnCaseVoid();" />&nbsp;
							<fmtCaseBookSetDetails:message key="BTN_SAVE_DRAFT" var="varSaveDraft"/><gmjsp:button style="width: 10em" value="${varSaveDraft}" buttonType="Save"  onClick="fnSubmit();" />&nbsp;
							 </logic:notEqual>
							 </logic:notEqual>
							 <logic:equal name="frmCaseBookSetDtls" property="caseInfoID" value="">  	
							 <fmtCaseBookSetDetails:message key="BTN_BACK" var="varBack"/><gmjsp:button style="width: 7em" value="${varBack}" buttonType="Load"  onClick="fnProfileBack();" />
							 </logic:equal>						
							 <logic:notEqual name="frmCaseBookSetDtls" property="caseInfoID" value="">  	
							 <fmtCaseBookSetDetails:message key="BTN_NEXT" var="varNext"/><gmjsp:button style="width: 7em" value="${varNext}" buttonType="Save" onClick="fnNext();" />
							 </logic:notEqual>
							 <logic:equal name="frmCaseBookSetDtls" property="caseInfoID" value="">  	
							 <fmtCaseBookSetDetails:message key="BTN_NEXT" var="varNext"/><gmjsp:button style="width: 7em" value="${varNext}" buttonType="Save" onClick="fnProfileNext();" />
							 </logic:equal>
						</td>
					</tr>
				</table>
			</td>
		</tr>
     </table></div>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
