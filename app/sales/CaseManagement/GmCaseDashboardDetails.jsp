<!--\sales\DashBoard\GmSalesDashBoard.jsp  -->
<%
/**********************************************************************************
 * File		 		: GmSalesDashBoard.jsp
 * Desc		 		: This screen is used for the Sales DashBoard
 * Version	 		: 1.0
 * author			: Gopinathan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtSalesDashBoard" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtSalesDashBoard:setLocale value="<%=strLocale%>"/>
<fmtSalesDashBoard:setBundle basename="properties.labels.sales.Dashboard.GmCaseScheDashBoard"/>
<bean:define id="hmCaseShipDsh" name="frmGmCaseSchedulerDash" property="hmCaseShipDsh" scope="request" type="java.util.HashMap"></bean:define>
<bean:define id="hmCaseReturnDsh" name="frmGmCaseSchedulerDash" property="hmCaseReturnDsh" scope="request" type="java.util.HashMap"></bean:define>
<bean:define id="hmCaseShipDshup" name="frmGmCaseSchedulerDash" property="hmCaseShipDshup" scope="request" type="java.util.HashMap"></bean:define>
<bean:define id="hmCaseRetDshup" name="frmGmCaseSchedulerDash" property="hmCaseRetDshup" scope="request" type="java.util.HashMap"></bean:define>
<%
String strSalesCaseMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_CASEMANAGEMENT");
String strWikiTitle = GmCommonClass.getWikiTitle("CASE_SCHEDULER_DASHBOARD");

HashMap hmshipover = GmCommonClass.parseNullHashMap((HashMap)hmCaseShipDsh.get("SHIPOVERDUE"));
HashMap hmshiptoday = GmCommonClass.parseNullHashMap((HashMap)hmCaseShipDsh.get("SHIPTODAY"));
HashMap hmshiptmw = GmCommonClass.parseNullHashMap((HashMap)hmCaseShipDsh.get("SHIPTMW"));

String strShipOverDue = GmCommonClass.parseNull((String)hmshipover.get("SHIPOVERDUECNT"));
String strShipoverstdt = GmCommonClass.parseNull((String) hmshipover.get("STARTDATE")); 
String strShipoverenddt = GmCommonClass.parseNull((String) hmshipover.get("ENDDATE"));  

String strShipToday = GmCommonClass.parseNull((String)hmshiptoday.get("SHIPOVERDUECNT"));
String strShiptodaystdt = GmCommonClass.parseNull((String) hmshiptoday.get("STARTDATE")); 
String strShiptodayenddt = GmCommonClass.parseNull((String) hmshiptoday.get("ENDDATE"));

String strShipTmw = GmCommonClass.parseNull((String)hmshiptmw.get("SHIPOVERDUECNT"));
String strShiptmwstdt = GmCommonClass.parseNull((String)hmshiptmw.get("STARTDATE")); 
String strShiptmwenddt = GmCommonClass.parseNull((String)hmshiptmw.get("ENDDATE"));

HashMap hmretover = GmCommonClass.parseNullHashMap((HashMap)hmCaseReturnDsh.get("RETOVERDUE"));
HashMap hmrettoday = GmCommonClass.parseNullHashMap((HashMap)hmCaseReturnDsh.get("RETTODAY"));
HashMap hmrettmw = GmCommonClass.parseNullHashMap((HashMap)hmCaseReturnDsh.get("RETTMW"));

String strRetOverDue = GmCommonClass.parseNull((String)hmretover.get("RETURNOVERDUECNT"));
String strRetoverstdt = GmCommonClass.parseNull((String) hmretover.get("STARTDATE")); 
String strRetoverenddt = GmCommonClass.parseNull((String) hmretover.get("ENDDATE"));

String strRetToday = GmCommonClass.parseNull((String)hmrettoday.get("RETURNTODAYCNT"));
String strRettodaystdt = GmCommonClass.parseNull((String) hmrettoday.get("STARTDATE")); 
String strRettodayenddt = GmCommonClass.parseNull((String) hmrettoday.get("ENDDATE"));

String strRetTmw = GmCommonClass.parseNull((String)hmrettmw.get("RETURNTOMORROWCNT")); 
String strRettmwstdt = GmCommonClass.parseNull((String)hmrettmw.get("STARTDATE")); 
String strRettmwenddt = GmCommonClass.parseNull((String)hmrettmw.get("ENDDATE"));

String strCaseShipTW = GmCommonClass.parseNull((String)hmCaseShipDshup.get("TW_CNT"));
String strCaseTWStDt = GmCommonClass.parseNull((String)hmCaseShipDshup.get("TW_STARTDATE"));
String strCaseTWendDt = GmCommonClass.parseNull((String)hmCaseShipDshup.get("TW_ENDDATE"));

String strCaseShipNW = GmCommonClass.parseNull((String)hmCaseShipDshup.get("NW_CNT"));
String strCaseNWStDt = GmCommonClass.parseNull((String)hmCaseShipDshup.get("NW_STARTDATE"));
String strCaseNWendDt = GmCommonClass.parseNull((String)hmCaseShipDshup.get("NW_ENDDATE"));

String strCaseShipTM = GmCommonClass.parseNull((String)hmCaseShipDshup.get("TM_CNT")); 
String strCaseTMStDt = GmCommonClass.parseNull((String)hmCaseShipDshup.get("TM_STARTDATE"));
String strCaseTMendDt = GmCommonClass.parseNull((String)hmCaseShipDshup.get("TM_ENDDATE"));

String strCaseRetTW = GmCommonClass.parseNull((String)hmCaseRetDshup.get("TW_CNT"));
String strCaseRetTWStDt = GmCommonClass.parseNull((String)hmCaseRetDshup.get("TW_STARTDATE"));
String strCaseRetTWendDt = GmCommonClass.parseNull((String)hmCaseRetDshup.get("TW_ENDDATE"));

String strCaseRetNW = GmCommonClass.parseNull((String)hmCaseRetDshup.get("NW_CNT"));
String strCaseRetNWStDt = GmCommonClass.parseNull((String)hmCaseRetDshup.get("NW_STARTDATE"));
String strCaseRetNWendDt = GmCommonClass.parseNull((String)hmCaseRetDshup.get("NW_ENDDATE"));

String strCaseRetTM = GmCommonClass.parseNull((String)hmCaseRetDshup.get("TM_CNT")); 
String strCaseRetTMStDt = GmCommonClass.parseNull((String)hmCaseRetDshup.get("TM_STARTDATE"));
String strCaseRetTMendDt = GmCommonClass.parseNull((String)hmCaseRetDshup.get("TM_ENDDATE"));
%>

<HTML>
	<Head>
		<title> Case Scheduler Dashboard</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	  	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxChart/dhtmlxchart.css"/>
 		<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxChart/dhtmlxchart.js" type="text/javascript"></script>
        <script language="JavaScript" src="<%=strSalesCaseMgmntJsPath%>/GmCaseSchDashboard.js"></script>  	

        <style type="text/css" media="all">
		     @import url("<%=strCssPath%>/screen.css");
		</style>
        <style>
.box {
  width: 280px;
  border: 1px solid #000;
  margin: auto;
  height: 220px;
  border-radius: 5px;
  margin-bottom: 20px;
}
.head{
  background-color:#cee4ff;
  border-radius: 5px;
  border-bottom: 1px solid #000;
  height: 30px;
}
.headlabel{
    padding-bottom: 15px;
}
.content{
padding-top: 50px;
}
</style>
<script> 
	// Below values set for case shipping upcoming details
	var strCaseTWStDt = '<%=strCaseTWStDt%>';
	var strCaseTWendDt = '<%=strCaseTWendDt%>';
	var strCaseNWStDt = '<%=strCaseNWStDt%>';
	var strCaseNWendDt = '<%=strCaseNWendDt%>';
	var strCaseTMStDt = '<%=strCaseTMStDt%>';
	var strCaseTMendDt = '<%=strCaseTMendDt%>';
	var Case_ShipUpcoming_dataset = [	
	{ count:"<%=strCaseShipTW%>", month:"1-7 days", color: "#4472C4" , id:"CASESHIPTW"},
	{ count:"<%=strCaseShipNW%>", month:"8-14 days", color: "#ED7D31" , id:"CASESHIPNW"},  
	{ count:"<%=strCaseShipTM%>", month:"15-30 days", color: "#D3D3D3" , id:"CASESHIPTM"}
	];
	// Below values set for case returns upcoming details
	var strCaseRetTWStDt = '<%=strCaseRetTWStDt%>';
	var strCaseRetTWendDt = '<%=strCaseRetTWendDt%>';
	var strCaseRetNWStDt = '<%=strCaseRetNWStDt%>';
	var strCaseRetNWendDt = '<%=strCaseRetNWendDt%>';
	var strCaseRetTMStDt = '<%=strCaseRetTMStDt%>';
	var strCaseRetTMendDt = '<%=strCaseRetTMendDt%>';
	var Case_RetUpcoming_dataset = [	
	{ count:"<%=strCaseRetTW%>", month:"1-7 days", color: "#4472C4" , id:"CASERETTW"},
	{ count:"<%=strCaseRetNW%>", month:"8-14 days", color: "#ED7D31" , id:"CASERETNW"},  
	{ count:"<%=strCaseRetTM%>", month:"15-30 days", color: "#D3D3D3" , id:"CASERETTM"}
	];
	</script>
	</Head>
  
	<BODY leftmargin="0" topmargin="0" onload="fnCaseShipUpLoad();fnCaseRetUpLoad();">

			<html:form  action="/gmCaseSchedulerDash.do?method=fetchKitData">				
			<table>
			<tr><td colspan ="4" ><div class="headlabel"><font size="5"><center><b><fmtSalesDashBoard:message key="LBL_CASE_SCH_DASH_SHIPPING"/></b></center></font></div></td></tr>
	<tr><td style="padding-left:10px">	
	<div class="box">
		<div class="RightTableCaption head"><font color="red" size="5"><center><fmtSalesDashBoard:message key="LBL_OVERDUE"/></center></font></div>
		<div class="content" onclick="fnCaseShipOver('<%=strShipoverstdt%>','<%=strShipoverenddt%>');"><font color="red" size="10"><center><%=strShipOverDue%></center></font></div>
	</div></td>
	
	<td style="padding-left:10px"><div class="box">
		<div class="RightTableCaption head" ><font size="5"><center><fmtSalesDashBoard:message key="LBL_TODAY"/></center></font></div>
		<div class="content" onclick="fnCaseShiptoday('<%=strShiptodaystdt%>','<%=strShiptodayenddt%>');"><font size="10"><center><%=strShipToday%></center></font></div></div>
	</td>
		
	<td style="padding-left:10px"><div class="box">
		<div class="RightTableCaption head"><font size="5"><center><fmtSalesDashBoard:message key="LBL_TOMORROW"/></center></font></div>
		<div class="content" onclick="fnCaseShiptmw('<%=strShiptmwstdt%>','<%=strShiptmwenddt%>');"><font size="10"><center><%=strShipTmw%></center></font></div></div>
	</td>
		
	<td style="padding-left:10px;padding-right:10px;"><div class="box">
		<div class="RightTableCaption head"><font size="5"><center><fmtSalesDashBoard:message key="LBL_UPCOMING"/></center></font></div>
		<div id="CaseShipUpcoming" style="width:330px;height:200px;" ></div></div>
	</td>
	</tr>
	
		<tr bgcolor="#000">
			<td colspan="6" height="1"></td>
		</tr>
			<tr >	<td colspan ="4"><div class="headlabel"><font size="5"><center><b><fmtSalesDashBoard:message key="LBL_CASE_SCH_DASH_RETURN"/></b></center></font></div></td></tr>
		<tr>
	<td style="padding-left:10px">
	<div class="box">
	<div class="RightTableCaption head"><font color="red" size="5"><center><b><fmtSalesDashBoard:message key="LBL_OVERDUE"/></b></center></font></div>
	<div class="content" onclick="fnCaseRetover('<%=strRetoverstdt%>','<%=strRetoverenddt%>');"><font color="red" size="10"><center><%=strRetOverDue%></center></font></div>
	</div>
	
	</td>
		<td style="padding-left:10px">
	<div class="box">
		<div class="RightTableCaption head"><font size="5"><center><b><fmtSalesDashBoard:message key="LBL_TODAY"/></b></center></font></div>
	<div class="content" onclick="fnCaseRettoday('<%=strRettodaystdt%>','<%=strRettodayenddt%>');"><font size="10"><center><%=strRetToday%></center></font></div></div>
	</td>
		<td style="padding-left:10px">
	<div class="box">
		<div class="RightTableCaption head"><font size="5"><center><b><fmtSalesDashBoard:message key="LBL_TOMORROW"/></b></center></font></div>
	<div class="content" onclick="fnCaseRettmw('<%=strRettmwstdt%>','<%=strRettmwenddt%>');"><font size="10"><center><%=strRetTmw%></center></font></div></div>
	</td>
		<td style="padding-left:10px;padding-right:10px;">
	<div class="box">
		<div class="RightTableCaption head"><font size="5"><center><b><fmtSalesDashBoard:message key="LBL_UPCOMING"/></b></center></font></div>
	<div id="CaseRetUpcoming" style="width:330px;height:200px;" ></div></div>
	</td>
	</tr>

			</table>		
			</html:form>
			<%@ include file="/common/GmFooter.inc" %>
	</BODY>
</HTML>

