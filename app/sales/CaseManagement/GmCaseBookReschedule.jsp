<!-- \sales\CaseManagement\GmCaseBookReschedule.jsp -->
<%
	/**********************************************************************************
	 * File		 		: GmCaseBookSetup.jsp
	 * Desc		 		: This screen is used for Case Book Setup
	 * Version	 		: 1.0
	 * author			: Xun
	 * 
	 ************************************************************************************/
%> 
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtCaseBookReschedule" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtCaseBookReschedule:setLocale value="<%=strLocale%>"/>
<fmtCaseBookReschedule:setBundle basename="properties.labels.sales.CaseManagement.GmCaseBookReschedule"/> 
 <bean:define id="caseInfoID" name="frmCaseBookReschedule" property="caseInfoID" type="java.lang.String"></bean:define>
 <%
 String strSalesCaseMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_CASEMANAGEMENT");
String strTodaysDate = (String)session.getAttribute("strSessTodaysDate");

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Case Book Reschedule </TITLE> 
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/party/GmParty.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strSalesCaseMgmntJsPath%>/GmCaseBookReschedule.js"></script>
<script>
var strCaseInfoID = "<%=caseInfoID%>";
var strToday = "<%=strTodaysDate%>";
var lblSurgeryDate='<fmtCaseBookReschedule:message key="LBL_SURGERY_DATE"/>';
var lblReason='<fmtCaseBookReschedule:message key="LBL_REASON"/>';
</script>
</HEAD>
 
<BODY leftmargin="20" topmargin="10"  >
<html:form action="/gmCaseBookReschedule.do">
<html:hidden property="strOpt" />
 <html:hidden property="caseInfoID" />
 
	<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr >
						<td height="25" colspan="1" class="RightDashBoardHeader" width="40%">&nbsp;<fmtCaseBookReschedule:message key="LBL_CASE_BOOKING_RESCHEDULE"/></td>
						<td align="right" class=RightDashBoardHeader colspan="1">&nbsp;<td>
						 
					</tr>
					<tr><td class="Line" height="1" colspan="2"></td></tr>
					<!-- Custom tag lib code modified for JBOSS migration changes --> 
					<tr class="evenshade">
						  <td  class="RightTableCaption" align="Right"><font color="red">*</font>&nbsp;<fmtCaseBookReschedule:message key="LBL_SURGERY_DATE"/>:</td>
						<td width="320">&nbsp;
						 <gmjsp:calendar SFFormName="frmCaseBookReschedule" SFDtTextControlName="surgeryDate"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
					   </td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="oddshade">
						  <td  class="RightTableCaption" align="Right">&nbsp;<fmtCaseBookReschedule:message key="LBL_SURGERY_TIME"/>:</td>
						<td width="320">&nbsp;
						<gmjsp:dropdown controlName="surgeryTimeHour" SFFormName="frmCaseBookReschedule" SFSeletedValue="surgeryTimeHour"
							SFValue="alSurgeryTimeHour" codeId = "CODEID"  codeName = "CODENM"   defaultValue= "" />													
					
						&nbsp;<gmjsp:dropdown controlName="surgeryTimeMin" SFFormName="frmCaseBookReschedule" SFSeletedValue="surgeryTimeMin"
							SFValue="alSurgeryTimeMin" codeId = "CODEID"  codeName = "CODENM"   defaultValue= "" />													
					
						&nbsp;<gmjsp:dropdown controlName="surgeryAMPM" SFFormName="frmCaseBookReschedule" SFSeletedValue="surgeryAMPM"
							SFValue="alSurgeryAMPM" codeId = "CODEID"  codeName = "CODENM"   defaultValue= "" />													
					
						</td>
					</tr>					
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="evenshade">
						  <td  class="RightTableCaption" align="Right"><font color="red">*</font>&nbsp;<fmtCaseBookReschedule:message key="LBL_REASON"/>:</td>
						<td width="320">&nbsp;
						<gmjsp:dropdown controlName="reasons" SFFormName="frmCaseBookReschedule" SFSeletedValue="reasons" 
							SFValue="alReasons" codeId = "CODEID"  codeName = "CODENM"   defaultValue= "[Choose One]" />													
					
						</td>
					</tr>
					<tr>
							<td colspan="2" align="center" height="24">&nbsp; 
								<jsp:include
									page="/common/GmIncludeLog.jsp">
									<jsp:param name="FORMNAME" value="frmCaseBookReschedule" />
									<jsp:param name="ALNAME" value="alLogReasons" />
									<jsp:param name="LogMode" value="Edit" />
								</jsp:include></td>
						</tr>
				   <tr>
						<td  height="40" colspan="2" align="center">&nbsp;		
							<fmtCaseBookReschedule:message key="BTN_RESCHEDULE_CLASS" var="varRescheduleCase"/><gmjsp:button style="width: 14em" buttonType="Save" value="${varRescheduleCase}"  onClick="fnSubmit();" />&nbsp;
							<fmtCaseBookReschedule:message key="BTN_BACK" var="varBack"/><gmjsp:button style="width: 5em" value="${varBack}" buttonType="Load" onClick="fnBack();" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
 
    </table>
</html:form>
 <%@ include file="/common/GmFooter.inc" %>   
</BODY>
 
</HTML>
 

