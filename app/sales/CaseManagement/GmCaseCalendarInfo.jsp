<!-- \sales\CaseManagement\GmCaseCalendarInfo.jsp -->
<%
/**********************************************************************************
 * File		 		: GmCaseCalendarInfo.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Dhana Reddy
************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtCaseCalendarInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtCaseCalendarInfo:setLocale value="<%=strLocale%>"/>
<fmtCaseCalendarInfo:setBundle basename="properties.labels.sales.CaseManagement.GmCaseCalendarInfo"/>


<bean:define id="gridData" name="frmCaseCalendar" property="gridData" type="java.lang.String"> </bean:define>
<HTML>
<HEAD>
<TITLE> Globus Medical: Case Calendar Info</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/date-picker.js"></script>
<script type="text/javascript">
   var objGridData;
   objGridData ='<%=gridData%>';
   function fnOnPageLoad()
   {
   	if(objGridData != '')
   	{
   		gridObj = initGrid('grpData',objGridData);
   	}
   	
   }
   function fnClose(){
	   
	   parent.dhxWins.window("w1").close();
   }
</script>
</HEAD>


<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">

<html:form action="/gmCaseCalendarAction.do?method=calendarCaseInfo">
<table border="0" class="GtTable900" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader" ><fmtCaseCalendarInfo:message key="LBL_CALENDER_CASE_INFO"/></td>
			
			<td class="RightDashBoardHeader"></td>
			<td  class="RightDashBoardHeader" align="right" colspan="3">
			   
		    </td>
		</tr>

			<%if(!gridData.equals("")){%>
		
			<tr>  <td><div id="grpData"  style="height:400;width :900" ></div></td> </tr>
			<tr>
			      <td colspan="6" align="center">
			          <div class='exportlinks'><fmtCaseCalendarInfo:message key="DIV_EXPORT_OPT"/>: <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
			                              onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> <fmtCaseCalendarInfo:message key="DIV_EXCEL"/> </a>&nbsp;
			          </div>
			     </td>

			</tr>
			
			<tr>
			<fmt:message key="BTN_CLOSE" var="varClose"/>
			<td colspan="6" align="center"><p>&nbsp;</p> <gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" buttonType="Load" onClick="fnClose();" /></td>
            </tr>
			
			<%} %>		
</table>	
<%@ include file="/common/GmFooter.inc" %>
</html:form>
</BODY>
</HTML>

