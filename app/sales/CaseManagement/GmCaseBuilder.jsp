<!-- \sales\CaseManagement\GmCaseBuilder.jsp -->
<%
	/**********************************************************************************
	 * File		 		: GmCaseBuilder.jsp
	 * Desc		 		: This screen is used for Case Book Builder
	 * Version	 		: 1.0
	 * author			: Xun
	 * 
	 ************************************************************************************/
%> 
<%@ include file="/common/GmHeader.inc"%> 
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%> 
<%@ taglib prefix="fmtCaseBuilder" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtCaseBuilder:setLocale value="<%=strLocale%>"/>
<fmtCaseBuilder:setBundle basename="properties.labels.sales.CaseManagement.GmCaseBuilder"/>
 
<bean:define id="alCategories" name="frmCaseBuilder" property="alCategories" type="java.util.List"></bean:define>
<bean:define id="caseInfoID" name="frmCaseBuilder" property="caseInfoID" type="java.lang.String"></bean:define>
<bean:define id="caseID" name="frmCaseBuilder" property="caseID" type="java.lang.String"></bean:define>
<bean:define id="selectedCategories" name="frmCaseBuilder" property="selectedCategories" type="java.lang.String"></bean:define>
<bean:define id="favouriteCaseID" name="frmCaseBuilder" property="favouriteCaseID" type="java.lang.String"></bean:define>
<%
Object bean = pageContext.getAttribute("frmCaseBuilder", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
String strSalesCaseMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_CASEMANAGEMENT");
String title ="";
String strWikiTitle ="";
if(!caseInfoID.equals(""))
{
	title = "Case Booking - Step 2 - Case ";
	strWikiTitle = "CASE_BOOK_SETUP2";
}
else
{
	title = "Profile ";
	strWikiTitle = "PROFILE_BUILDER";
}

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Case Book Builder</TITLE>
 <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
 
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

  
<script language="JavaScript" src="<%=strSalesCaseMgmntJsPath%>/GmCaseBuilder.js"></script>
 <script>  
 var lblFavoriteName='<fmtCaseBuilder:message key="LBL_FAVOURITE_NAME"/>';
 var sCategories = "<%=selectedCategories%>";
 
 var strCaseInfoID = "<%=caseInfoID%>";
 var strcaseID = "<%=caseID%>";
 var strFavcaseID = "<%=favouriteCaseID%>";
 
 var categoriesLen = <%=alCategories.size()%>;
 
function fnCaseVoid(){
	 
            document.frmCaseBuilder.hCancelType.value = 'VDCASE';           
            document.frmCaseBuilder.hTxnId.value = strCaseInfoID; 
            document.frmCaseBuilder.hTxnName.value = strcaseID; 
            document.frmCaseBuilder.action ="/GmCommonCancelServlet";            
            document.frmCaseBuilder.hAction.value = "Load";                     
            document.frmCaseBuilder.submit();
      }

function fnFetch()
{
	var favouriteCaseID = document.frmCaseBuilder.favouriteCaseID.value;
	if(favouriteCaseID == 0){
		document.frmCaseBuilder.favouriteCaseNM.value = "";
		fnReset();
	}else{
	document.frmCaseBuilder.strOpt.value = "fetchFavorite";
	document.frmCaseBuilder.submit();
	}
}
</script>
</HEAD>
 
<BODY leftmargin="20" topmargin="10"  onload="fnCheckSelections();">
<FORM name="frmCaseBuilder" method="POST" action="gmCaseBuilder.do">
<html:hidden property="caseID"  />
<html:hidden property="strOpt"  />
<html:hidden property="caseInfoID"  />
<html:hidden property="repId"  />
<input type="hidden" name="hTxnId" value=""/>
<input type="hidden" name="hTxnName" value=""/>
<input type= "hidden" name="hAction" value=""/>
<input type= "hidden"  name="hCancelType" value=""/>
	<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr > <td colspan="2">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" colspan="1" class="RightDashBoardHeader" width=70%>&nbsp;<%=title %><fmtCaseBuilder:message key="LBL_BUILDER"/> </td>
						 <td align="right" class=RightDashBoardHeader>
							<fmtCaseBuilder:message key="IMG_HELP" var="varHelp"/><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${Help}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle(strWikiTitle)%>');" />
						</td>
						</tr>
					</table>
					</td>
					</tr>
					<tr><td class="Line" height="1" colspan="2"></td></tr>
					<logic:notEqual name="frmCaseBuilder" property="caseInfoID" value=""> 
					<tr class="evenshade">
						  <td  class="RightTableCaption" align="left" colspan="2"><font color="red"><fmtCaseBuilder:message key="LBL_CATEGORY_SELECTION"/></font></td>
						 
					</tr>
					</logic:notEqual>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr height="25">
						  <td  class="RightTableCaption" align="Right"><fmtCaseBuilder:message key="LBL_PICK_FROM_FAVOURITE"/>:</td>
						<td width="460">&nbsp;
						<gmjsp:dropdown controlName="favouriteCaseID" SFFormName="frmCaseBuilder" SFSeletedValue="favouriteCaseID"
							SFValue="alFavouriteCases" codeId = "CODEID"  codeName = "CODENM" onChange="fnFetch();" defaultValue= "[Choose One]" />	
						</td>
					</tr>
					<tr>
                        <td class="RightTextBlue" colspan="2" align="center"><fmtCaseBuilder:message key="LBL_SELECT_fROM_LIST"/></td> 
                    </tr>
                    <tr ><td colspan="2" class="Line"></td></tr>
							 <logic:notEqual name="frmCaseBuilder" property="strOpt" value="fetchFavorite"> 
							 <logic:notEqual name="frmCaseBuilder" property="caseInfoID" value=""> 			
										<tr class="oddshade">
									  <td  class="RightTableCaption" align="Right"><fmtCaseBuilder:message key="LBL_CASE_ID"/> :</td>
									<td>&nbsp;  
									
									 <bean:write name="frmCaseBuilder" property="caseID" /> 
								 
									</td>
								</tr>	
							</logic:notEqual>	
							</logic:notEqual>
							<logic:equal name="frmCaseBuilder" property="strOpt" value="fetchFavorite"> 
							 <logic:notEqual name="frmCaseBuilder" property="caseInfoID" value=""> 			
										<tr class="oddshade">
									  <td  class="RightTableCaption" align="Right"><fmtCaseBuilder:message key="LBL_CASE_ID"/>:</td>
									<td>&nbsp;  
									
									 <bean:write name="frmCaseBuilder" property="caseID" /> 
								 
									</td>
								</tr>	
							</logic:notEqual>	
							</logic:equal>
							 <logic:equal name="frmCaseBuilder" property="strOpt" value="fetchFavorite"> 	
							 <logic:equal name="frmCaseBuilder" property="caseInfoID" value=""> 		
								<tr class="evenshade">
										  <td  class="RightTableCaption" align="Right"><font color="red">*</font>&nbsp;<fmtCaseBuilder:message key="LBL_FAVOURITE_NAME"/> :</td>
										<td>&nbsp;  
										
										 <html:text property="favouriteCaseNM"  size="15" maxlength="30"  />
									 
										</td>
									</tr>	
							</logic:equal>
							</logic:equal> 
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="evenshade">
						<td  class="RightTableCaption" align="Right">
						<logic:equal name="frmCaseBuilder" property="caseInfoID" value=""> 
						<font color="red">*</font>
						</logic:equal>
						&nbsp;<fmtCaseBuilder:message key="LBL_CATEGORIES"/> :</td>
						<td   width="180">&nbsp;&nbsp;<input type="checkbox" name="checkSelectedAll" value=" "  onClick = "javascript:fnSelectAll(this);"><fmtCaseBuilder:message key="LBL_SELECT_ALL"/> </td>
					</tr>
					<tr class="evenshade">
						<td align="Right">&nbsp;</td>
						<td width="180">
     			  <DIV style="display:visible;height: 175px;width:280px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
                         <table>
                        	<logic:iterate id="selectedCategorieslist" name="frmCaseBuilder" property="alCategories">
                        	<tr>
                        		<td>
								    <htmlel:multibox property="checkCategories" value="${selectedCategorieslist.CODEID}"  onclick="fnSelectCategories();" />
								    <bean:write name="selectedCategorieslist" property="CODENM" />
	     						</td>
							</tr>	    
							</logic:iterate>
							</table>
							</DIV><BR>
						</td>
					</tr>
					 	<tr><td class="LLine" height="1" colspan="2"></td></tr>
				   <tr>
						<td  height="40" colspan="2" align="center">&nbsp;
							 <logic:notEqual name="frmCaseBuilder" property="caseInfoID" value=""> 
								 <logic:notEqual name="frmCaseBuilder" property="caseStatus" value="11091">	
									<fmtCaseBuilder:message key="BTN_BACK" var="varBack"/><gmjsp:button style="width: 5em" value="${varBack}" buttonType="Load"  onClick="fnBack();"/>&nbsp;
								 </logic:notEqual>	
							 </logic:notEqual>	
							 <logic:notEqual name="frmCaseBuilder" property="caseInfoID" value=""> 
							<fmtCaseBuilder:message key="BTN_DELETE" var="varDelete"/><gmjsp:button style="width: 7em" value="${varDelete}" buttonType="Save" onClick="fnCaseVoid();" />&nbsp;
							</logic:notEqual>	
							<logic:notEqual name="frmCaseBuilder" property="caseInfoID" value=""> 
							<fmtCaseBuilder:message key="BTN_SAVE_DRAFT" var="varSaveDraft"/><gmjsp:button style="width: 10em" value="${varSaveDraft}" buttonType="Save" onClick="fnSubmit();" />&nbsp;
							</logic:notEqual>
							<logic:notEqual name="frmCaseBuilder" property="caseInfoID" value="">  			
							<fmtCaseBuilder:message key="BTN_NEXT" var="varNext"/><gmjsp:button style="width: 7em" value="${varNext}" buttonType="Load"  onClick="fnNext('next');" />
							</logic:notEqual>
							 	
							 <logic:equal name="frmCaseBuilder" property="caseInfoID" value=""> 		
							<fmtCaseBuilder:message key="BTN_NEXT" var="varNext"/><gmjsp:button style="width: 5em" value="${varNext}" buttonType="Load" onClick="fnNext('ProfileNext');" />
							</logic:equal>
							 
					</tr>
				</table>
			</td>
		</tr>
 
    </table>
</FORM>
     
 <%@ include file="/common/GmFooter.inc" %>
 
</BODY>
 
</HTML>
 

