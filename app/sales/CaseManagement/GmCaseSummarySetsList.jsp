<!-- \sales\CaseManagement\GmCaseSummarySetsList.jsp -->
<%
/**********************************************************************************
 * File		 		: GmCaseSummarySetsList.jsp
 * Desc		 		: This screen is used to display Set list on case book confirm page
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%> 
  
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtCaseSummarySetsList" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtCaseSummarySetsList:setLocale value="<%=strLocale%>"/>
<fmtCaseSummarySetsList:setBundle basename="properties.labels.sales.CaseManagement.GmCaseSummarySetsList"/>
  
 <bean:define id="alAllBookedSet" name="frmCasePost" property="alAllBookedSets" type="java.util.ArrayList"></bean:define>
 <bean:define id="parentForm" name="frmCasePost" property="parentForm" type="java.lang.String"></bean:define> 
		<tr>
			<td  >
			<tr   class="ShadeRightTableCaption" HEIGHT="24" >
						  <td  align="left"> 
							   <fmtCaseSummarySetsList:message key="LBL_PRODUCT_SYSTEM"/> &nbsp; 
						 </td>
						<td width="320">&nbsp; 
						</td>
					</tr>				
					<tr><td class="Line" height="1" colspan="2"></td></tr>
<%
if(alAllBookedSet.size()>0){
%>	
				<display:table name="requestScope.frmCasePost.alAllBookedSets" requestURI="/gmCasePost.do"  decorator="com.globus.sales.CaseManagement.displaytag.DTCaseSetsWrapper"   class="its" id="currentRowObject"   
				  >  
			 		 		<%if (parentForm.equals("")) {%>
			 		 		<display:column property="INITLN" title="<span id='spnSelAll'></span>" class="aligncenter"/>
			 		 		<% } %>
			 		 		<fmtCaseSummarySetsList:message key="DT_SET_ID" var="varSetID"/><display:column property="SETID" title="${varSetID}" sortProperty="SORTSETID" sortable="true" />
							<fmtCaseSummarySetsList:message key="DT_SET_NAME" var="varSetName"/><display:column property="SETNM" title="${varSetName}"  class="alignleft" sortable="true" /> 
							<fmtCaseSummarySetsList:message key="DT_SET" var="varSet"/><display:column property="SETALLOC" title="${varSet}"  class="alignleft" sortable="true" />
							<fmtCaseSummarySetsList:message key="DT_TAG_ID" var="varTagID"/><display:column property="TAGID" title="${varTagID}" style="width:90;" class="alignleft" sortProperty="SORTTAGID" sortable="true" /> 
							<fmtCaseSummarySetsList:message key="DT_SET_LOCK_FROM" var="varSetLockfrom"/><display:column property="LOCKFROMDT" title="${varSetLockfrom}"  class="alignleft" /> 
							<fmtCaseSummarySetsList:message key="DT_SET_LOCK_TO" var="varSetLockTo"/><display:column property="LOCKTODT" title="${varSetLockTo}"  class="alignleft" /> 
							<fmtCaseSummarySetsList:message key="DT_CURRENT_LOCATION" var="varCurrentLocation"/><display:column property="CURRENTLOC" title="${varCurrentLocation}"  class="alignleft" /> 
							<fmtCaseSummarySetsList:message key="DT_STATUS" var="varStatus"/><display:column property="STATUS" title="${varStatus}"  class="alignleft" sortable="true" /> 
							<fmtCaseSummarySetsList:message key="DT_SHIP_IN" var="varShipIn"/><display:column property="SHIPIN" title="${varShipIn}"  class="alignleft" /> 
							<fmtCaseSummarySetsList:message key="DT_SHIP_OUT" var="varShipOut"/><display:column property="SHIPOUT" title="${varShipOut}"  class="alignleft"  /> 
						</display:table>
<%
}else{
%>	
					<tr class="evenshade">
						<td colspan="2">		
							&nbsp;&nbsp;<font color="red"> <fmtCaseSummarySetsList:message key="LBL_NO_SYSTEMS_ARE_SELECTED"/> -</font> 
						</td>
					</tr>
<%
}
%>						
			</td>
		</tr> 
 
 <%@ include file="/common/GmFooter.inc" %>