<!-- \sales\CaseManagement\GmCaseBookConfirm.jsp -->

 <%
	/**********************************************************************************
	 * File		 		: GmCaseBookSetDetails.jsp
	 * Desc		 		: This screen is used for Case Book Set Details
	 * Version	 		: 1.0
	 * author			: Xun
	 * 
	 ************************************************************************************/
%> 

<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%> 
<%@ taglib prefix="fmtCaseBookConfirm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtCaseBookConfirm:setLocale value="<%=strLocale%>"/>
<fmtCaseBookConfirm:setBundle basename="properties.labels.sales.CaseManagement.GmCaseBookConfirm"/>
<bean:define id="caseInfoID" name="frmCaseBookSetDtls" property="caseInfoID" type="java.lang.String"></bean:define>
<bean:define id="hmGenerlInfo" name="frmCaseBookSetDtls"  property="hmGenerlInfo" type="java.util.HashMap"></bean:define> 
<bean:define id="alCaseAttrDtls" name="frmCaseBookSetDtls"  property="alCaseAttrDtls" type="java.util.List"></bean:define>
<bean:define id="alAllBookedSets" name="frmCaseBookSetDtls" property="alAllBookedSets" type="java.util.List"></bean:define> 
<bean:define id="caseID" name="frmCaseBookSetDtls" property="caseID" type="java.lang.String"></bean:define> 
<bean:define id="favouriteCaseID" name="frmCaseBookSetDtls" property="favouriteCaseID" type="java.lang.String"></bean:define>
<bean:define id="hmGenerlInfo" name="frmCaseBookSetDtls"  property="hmGenerlInfo" type="java.util.HashMap"></bean:define> 
<% 
String title ="";
String strWikiTitle ="";

if(!caseInfoID.equals(""))
{
	GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.sales.CaseManagement.GmCaseBookConfirm", strSessCompanyLocale);
	title = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_CASE_BOOKING_STEP"));
	strWikiTitle = "CASE_BOOK_SETUP_CONFIRM";
}
else
{
	GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.sales.CaseManagement.GmCaseBookConfirm", strSessCompanyLocale);
	title = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PROFILE"));
	strWikiTitle = "PROFILE_CONFIRM";
}
String strAcct	 = GmCommonClass.parseNull((String) hmGenerlInfo.get("ACCOUNTNM")); 
String strSalesCaseMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_CASEMANAGEMENT");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Case Book </TITLE>  
 
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
 <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">   
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script> 

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css"> 
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js" type="text/javascript"></script> 
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js" type="text/javascript"></script> 
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strSalesCaseMgmntJsPath%>/GmCaseBookConfirm.js"></script>
 <script>

var strCaseInfoID = "<%=caseInfoID%>";
var strcaseID = "<%=caseID%>";
var strAccount = "<%=strAcct%>";

 </script>
</HEAD>
 
<BODY leftmargin="20" topmargin="10" > 
<html:form action="/gmCaseBookSetDtls.do" >
<html:hidden property="strOpt"  />
 <html:hidden property="caseInfoID"  />
 <html:hidden property="caseID"  />
 <html:hidden property="categoryList"/>
 <html:hidden property="favouriteCaseID"   />
 <html:hidden property="hinputStr"   />
 <html:hidden property="fromscreen"   /> 
<input type="hidden" name="hTxnId" value=""/>
<input type="hidden" name="hTxnName" value=""/>
<input type= "hidden" name="hAction" value=""/>
<input type= "hidden"  name="hCancelType" value=""/>
 
  <div id="winVP" style="position: relative;  height: 1500px;   margin: 10px;">
	<table class="DtTable1100" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr >
						<td height="25" colspan="1" class="RightDashBoardHeader">&nbsp;<%=title %> <fmtCaseBookConfirm:message key="LBL_CONFIRM"/></td>
						 <td align="right" class=RightDashBoardHeader>
							<fmtCaseBookConfirm:message key="IMG_HELP" var="varHelp"/><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle(strWikiTitle)%>');" />
						</td>
					</tr>
					<tr><td class="Line" height="1" colspan="4"></td></tr>
					 <logic:notEqual name="frmCaseBookSetDtls" property="caseInfoID" value="">  	 
					<tr class="evenshade">
						  <td  class="RightTableCaption" align="left" colspan="2"><font color="red"><fmtCaseBookConfirm:message key="LBL_REVIEW_THE_INFORMATION"/><br> <fmtCaseBookConfirm:message key="LBL_IF_ALL_INFORMATION_IS_CORRECT"/>
						   <fmtCaseBookConfirm:message key="LBL_CLICK_POST_CASE"/></font></td>
						 
					</tr>
					<tr><td class="LLine" height="1" colspan="4"></td></tr>
					
					
						<jsp:include page= "/sales/CaseManagement/GmCaseGeneralInfoHeader.jsp" >
						<jsp:param name="FORMNAME" value="frmCaseBookSetDtls" />
						</jsp:include>
					  </logic:notEqual>
					
			  		<jsp:include page= "/sales/CaseManagement/GmCaseAttributeDtls.jsp" >
			  		<jsp:param name="FORMNAME" value="frmCaseBookSetDtls" />
			  		</jsp:include>
				 
			 
						  
					<tr><td class="Line" height="1" colspan="4"></td></tr>
					
					  
							<jsp:include page= "/sales/CaseManagement/GmCaseConfirmSetsList.jsp" >
							<jsp:param name="FORMNAME" value="frmCaseBookSetDtls" />
							</jsp:include>
					 
					 <tr><td class="Line" height="1" colspan="2"></td></tr>
					  
					 
				   <tr>
						<td  height="40" colspan="2" align="center">&nbsp;
							 <logic:notEqual name="frmCaseBookSetDtls" property="caseInfoID" value="">  	
							<fmtCaseBookConfirm:message key="BTN_BACK" var="varBack"/><gmjsp:button style="width: 7em" value="${varBack}" buttonType="Load" onClick="fnBack();" />&nbsp;
							 </logic:notEqual>
							 <logic:equal name="frmCaseBookSetDtls" property="caseInfoID" value="">  	
							 <fmtCaseBookConfirm:message key="BTN_BACK" var="varBack"/><gmjsp:button style="width: 7em" value="${varBack}" buttonType="Load" onClick="fnProfileBack();" />
							 </logic:equal>
						 
							 <logic:notEqual name="frmCaseBookSetDtls" property="caseInfoID" value=""> 
							<fmtCaseBookConfirm:message key="BTN_POST_CASE" var="varPostCase"/><gmjsp:button style="width: 10em" value="${varPostCase}" buttonType="Save" onClick="fnPost();" />&nbsp;
							<fmtCaseBookConfirm:message key="BTN_DELETE" var="varDelete"/><gmjsp:button style="width: 7em" value="${varDelete}" buttonType="Save"  onClick="fnCaseVoid()" />
							 </logic:notEqual>
						</td>
					</tr>
					<logic:equal name="frmCaseBookSetDtls" property="caseInfoID" value=""> 
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr>
						<td colspan="2" height="25" align="center" class="RightTextBlue">
							<b><fmtCaseBookConfirm:message key="LBL_PROFILE_SAVED_SUCCESSFULLY"/></b>
						</td>
					</tr>
					</logic:equal>
				</table>
			</td>
		</tr>
 
    </table></div>
</html:form>
     
 
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
 
</HTML>
 

