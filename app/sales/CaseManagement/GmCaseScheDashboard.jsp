<!--\sales\CaseManagement\GmCaseScheDashboard.jsp -->
<%
/**********************************************************************************
 * File		 		: GmSalesDashBoard.jsp
 * Desc		 		: This screen is used for the Case Scheduler DashBoard
 * Version	 		: 1.0
 * author			: Agilan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtSalesDashBoard" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtSalesDashBoard:setLocale value="<%=strLocale%>"/>
<fmtSalesDashBoard:setBundle basename="properties.labels.sales.Dashboard.GmCaseScheDashBoard"/>
<bean:define id="strName" name="frmGmCaseSchedulerDash" property="strName" scope="request" type="java.lang.String"></bean:define>
<%
String strSalesCaseMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_CASEMANAGEMENT");
String strWikiTitle = GmCommonClass.getWikiTitle("CASE_SCHEDULER_DASHBOARD");
%>
<HTML>
	<Head>
		<title> Case Scheduler Dashboard</title>	
		<meta charset="utf-8">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
      	<script language="JavaScript" src="<%=strSalesCaseMgmntJsPath%>/GmCaseSchDashboard.js"></script>  	
        <style type="text/css" media="all">
		     @import url("<%=strCssPath%>/screen.css");
		</style>
        <style>
        	table.DtSalesTable1000 {
			margin:10px 0 0 0px;
			width: 1000px;
			border: 1px solid  #676767;
			}

.btn-group button {
  border: 1px solid #4472C4; 
  color:#3172B9; 
  padding:5px; 
  cursor: pointer; 
  float: left; 
  background:#FFF;
}  
.lblbtn:first-child{border-radius: 10px 0px 0px 10px !important; }
.lblbtn:last-child{border-radius: 0px 10px 10px 0px !important; }
.lblbtn{width:70px; }
.lblbtn.active{  
background-color: #4472C4; 
color:#FFFFFF;
}
</style>
	</Head>
	<BODY leftmargin="20" topmargin="10" >	
			<FORM name="frmGmCaseSchedulerDash" method="POST" action="/gmCaseSchedulerDash.do?method=fetchKitData" >		
			<table border="0" cellspacing="0" cellpadding="0" width="950px" height="650px" class="DtSalesTable1000" >
				<tr>
					<td height="30" colspan="3" class="RightDashBoardHeader">&nbsp;<fmtSalesDashBoard:message key="LBL_CASE_SCHEDULER_DASHBOARD"/></td>
					<td  height="30" class="RightDashBoardHeader">
						<fmtSalesDashBoard:message key="IMG_ALT_HELP" var="varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>			
				</tr>
				<tr>
			<td colspan="4">		
			<div class="btn-group" style="padding-left:87%;padding-top:10px;">
			<% if(strName.equals("CASE")){ %> 
         		<button class="lblbtn"  onclick="fnKitLoad();"><fmtSalesDashBoard:message key="LBL_KIT"/></button>
         		<button class="lblbtn active"  onclick="fnCaseLoad();" ><fmtSalesDashBoard:message key="LBL_CASE"/></button>
     		<%}else{ %> 
      	  		<button class="lblbtn active" onclick="fnKitLoad()" ><fmtSalesDashBoard:message key="LBL_KIT"/></button>
         		<button class="lblbtn" onclick="fnCaseLoad()"><fmtSalesDashBoard:message key="LBL_CASE"/></button>
      		<%} %> 
      		</div>
			</td></tr>
			<tr>
				<td width="950px" colspan="4">
					<% if(strName.equals("CASE")){ %>
						<jsp:include page="/sales/CaseManagement/GmCaseDashboardDetails.jsp" >
							<jsp:param name="HIDE" value="SYSTEM" />
							<jsp:param name="FRMNAME" value="frmGmCaseSchedulerDash" />
							<jsp:param name="HACTION" value="" />
						</jsp:include>
						<%}else{ %> 
						<jsp:include page="/sales/CaseManagement/GmKitDetailsDashboard.jsp" >
							<jsp:param name="HIDE" value="SYSTEM" />
							<jsp:param name="FRMNAME" value="frmGmCaseSchedulerDash" />
							<jsp:param name="HACTION" value="" />
						</jsp:include>
						<%} %>
					</td>
				</tr>		
			</table>			
			</FORM>
	<%@ include file="/common/GmFooter.inc" %>
	</BODY>
</HTML>

