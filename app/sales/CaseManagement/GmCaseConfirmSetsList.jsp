<!--\sales\CaseManagement\GmCaseConfirmSetsList.jsp  -->
<%
/**********************************************************************************
 * File		 		: GmCaseConfirmSetList.jsp
 * Desc		 		: This screen is used to display Set list on case book confirm page
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%> 
 
<%@ include file="/common/GmHeader.inc"%>  
<%@ taglib prefix="fmtCaseConfirmSetsList" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtCaseConfirmSetsList:setLocale value="<%=strLocale%>"/>
<fmtCaseConfirmSetsList:setBundle basename="properties.labels.sales.CaseManagement.GmCaseConfirmSetsList"/>

 <bean:define id="alAllBookedSet" name="frmCaseBookSetDtls" property="alAllBookedSets" type="java.util.ArrayList"></bean:define>
  
		<tr>
			<td>
					<tr   class="ShadeRightTableCaption" HEIGHT="24" >
						  <td  align="left"> 
							    <fmtCaseConfirmSetsList:message key="LBL_PRODUCT_SYSTEM"/>&nbsp; 
						 </td>
						<td width="320">&nbsp; 
						</td>
					</tr>	
					<tr><td class="Line" height="1" colspan="2"></td></tr>
<%
if(alAllBookedSet.size()>0){
%>						
					<display:table name="requestScope.frmCaseBookSetDtls.alAllBookedSets" requestURI="/gmCaseBookSetDtls.do"  decorator="com.globus.displaytag.beans.sales.DTCaseSetsWrapper"  class="its" id="currentRowObject"    > 
				 		 	<fmtCaseConfirmSetsList:message key="LBL_SET_ID" var="varSetID"/>	<display:column property="SETID" title="${varSetID}"   />
								<fmtCaseConfirmSetsList:message key="LBL_SET_NAME" var="varSetName"/>	<display:column property="SETNM" title="${varSetName}"  class="alignleft" /> 
								<fmtCaseConfirmSetsList:message key="LBL_FAMILY" var="varFamily"/>	<display:column property="FAMILY" title="${varFamily}"  class="alignleft" />
								<fmtCaseConfirmSetsList:message key="LBL_TYPE" var="varType"/>	<display:column property="STYPE" title="${varType}"  class="alignleft" /> 
								<fmtCaseConfirmSetsList:message key="LBL_QTY" var="varQty"/>	<display:column property="QTY" title="${varQty}"  class="alignright"  /> 
					</display:table>
<%
}else{
%>	
					<tr class="evenshade">
						<td colspan="2">		
							&nbsp;&nbsp;<font color="red"><fmtCaseConfirmSetsList:message key="LBL_NO_SYSTEMS_ARE_SELECTED"/>- </font> 
						</td>
					</tr>
<%
}
%>
			</td>
		</tr> 
 
 