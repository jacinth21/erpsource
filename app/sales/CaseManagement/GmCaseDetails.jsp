<%
/**********************************************************************************
 * File		 		: GmCaseDetails.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Agilan S
************************************************************************************/
%>
<!--\Sales\CaseManagement\GmCaseDetails.jsp -->
<%@ page language="java"%>
<%@page import="java.util.Date"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmCommonClass"%>
<%@ page import="java.util.*,java.io.*"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtCaseSchedule" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<fmtCaseSchedule:setLocale value="<%=strLocale%>" />
<fmtCaseSchedule:setBundle basename="properties.labels.sales.CaseManagement.GmCaseScheduler" />
<bean:define id="caseId" name="frmCaseSchedularForm" property="caseId" scope="request" type="java.lang.String"></bean:define>
<bean:define id="distributor" name="frmCaseSchedularForm" property="distributor" scope="request" type="java.lang.String"></bean:define>
<bean:define id="salesrep" name="frmCaseSchedularForm" property="salesrep" scope="request" type="java.lang.String"></bean:define>
<bean:define id="account" name="frmCaseSchedularForm" property="account" scope="request" type="java.lang.String"></bean:define>
<bean:define id="status" name="frmCaseSchedularForm" property="status" scope="request" type="java.lang.String"></bean:define>
<bean:define id="longTerm" name="frmCaseSchedularForm" property="longTerm" scope="request" type="java.lang.String"></bean:define>
<bean:define id="searchdistributor" name="frmCaseSchedularForm" property="searchdistributor" scope="request" type="java.lang.String"></bean:define>
<bean:define id="searchsalesrep" name="frmCaseSchedularForm" property="searchsalesrep" scope="request" type="java.lang.String"></bean:define>
<bean:define id="searchaccount" name="frmCaseSchedularForm" property="searchaccount" scope="request" type="java.lang.String"></bean:define>
<bean:define id="alListAcc" name="frmCaseSchedularForm" property="alListAcc" scope="request" type="java.util.ArrayList"></bean:define>
<bean:define id="alDisList" name="frmCaseSchedularForm" property="alDisList" scope="request" type="java.util.ArrayList"></bean:define> 
<bean:define id="alSalesRepList" name="frmCaseSchedularForm" property="alSalesRepList" scope="request" type="java.util.ArrayList"></bean:define>
<bean:define id="alListAcc" name="frmCaseSchedularForm" property="alListAcc" scope="request" type="java.util.ArrayList"></bean:define>
<%
String strSucMsg="";
int intLog = 0;	
boolean blHideComments = false;
String strSalesCaseMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_CASEMANAGEMENT");
String strSessApplDateFmt= strGCompDateFmt;
String strCompanyId = gmDataStoreVO.getCmpid();
String strPlantId = gmDataStoreVO.getPlantid();
String strPartyId = gmDataStoreVO.getPartyid();
String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
int RepSize = alSalesRepList.size();
int Accsize = alListAcc.size();
String strLongTerm = GmCommonClass.parseNull((String)request.getAttribute("STRLONGTERM"));
ArrayList alLogReasons = (ArrayList)request.getAttribute("hmLog");
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Case Details</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/party/GmParty.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strSalesCaseMgmntJsPath%>/GmCaseSchedular.js"></script>
<script>
var gCmpDateFmt = "<%=strGCompDateFmt%>";
var companyId = '<%=strCompanyId%>';
var status = "<%=status%>";
var compInfObj = '<%=strCompanyInfo%>'; 
var RepLen = '<%=RepSize%>';
var AccLen = '<%=Accsize%>'; 
var strLongTerm = '<%=strLongTerm%>'
<%-- <% 
HashMap hcboVal = new HashMap();
for (int i=0;i<alSalesRepList.size();i++)
{
	hcboVal = (HashMap)alSalesRepList.get(i);
%>
var RepArr<%=i%> ="<%=hcboVal.get("REPID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("PARTYID")%>,<%=hcboVal.get("DID")%>,<%=hcboVal.get("DDID")%>";
<%
}
%> --%>
<% 
HashMap hcboVal = new HashMap();
for (int i=0;i<alSalesRepList.size();i++)
{
	hcboVal = (HashMap)alSalesRepList.get(i);
%>
var RepArr<%=i%> ="<%=hcboVal.get("REPID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("PARTYID")%>,<%=hcboVal.get("DID")%>,<%=hcboVal.get("DDID")%>";
<%
}
%>
<%	
HashMap hcboVals = new HashMap();
for (int i=0;i<alListAcc.size();i++)
{
	hcboVals = (HashMap)alListAcc.get(i);
%>
var AccArr<%=i%> ="<%=hcboVals.get("ID")%>,<%=hcboVals.get("NAME")%>,<%=hcboVals.get("DID")%>";
<%
}
%>

</script>
<style type="text/css" media="all">
@import url("styles/screen.css");
</style>
</HEAD>
<BODY leftmargin="0" rightmargin="0" topmargin="0" onload="fnLoad();">

	<html:form action="/gmCaseSchedular.do">
		<html:hidden styleId="strOpt" property="strOpt" />
		<table border="0" width="100%" cellspacing="0" cellpadding="0">

			<tr height="30">
				<td width="100" class="RightTableCaption" HEIGHT="24" align="right"><font color="red">*</font>&nbsp;<fmtCaseSchedule:message key="LBL_SURGERY_DATE" />:&nbsp;</td>
				<td>
					<gmjsp:calendar  SFFormName="frmCaseSchedularForm" SFDtTextControlName="surgeryDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');fncheckdate();" />
				</td>
				<td class="RightTableCaption" HEIGHT="24" align="right" width="100"><font color="red">*</font>&nbsp;<fmtCaseSchedule:message key="LBL_SHIP_DATE" />:</td>
				<td>&nbsp;<gmjsp:calendar SFFormName="frmCaseSchedularForm" SFDtTextControlName="shipDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" /></td>
			</tr>
			<tr>
				<td colspan="4" height="1" class="LLine"></td>
			</tr>
			<tr height="30" class="Shade">
				<td class="RightTableCaption" HEIGHT="24" align="right"><font color="red">*</font>&nbsp;<fmtCaseSchedule:message key="LBL_RETRUN_DATE" />:</td>
				<td id="RetDt">&nbsp;<gmjsp:calendar SFFormName="frmCaseSchedularForm" SFDtTextControlName="retDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" /></td>
				<td class="RightTableCaption" align="right" height="30" width="150"><font color="red">*</font>&nbsp;<fmtCaseSchedule:message key="LBL_DIVISION" />:&nbsp;</td>
				<td>&nbsp;<gmjsp:dropdown  controlName="division" SFFormName="frmCaseSchedularForm" SFSeletedValue="division" SFValue="alDivList" codeId="DIVISION_ID" codeName="DIVISION_NAME" defaultValue="[Choose one]" /></td>
			</tr>
			<tr>
				<td colspan="4" height="1" class="LLine"></td>
			</tr>
			<tr height="30">
				<td class="RightTableCaption" HEIGHT="24" align="right"><font color="red">*</font>&nbsp;<fmtCaseSchedule:message key="LBL_FIELD_SALES" />:</td>
				<td>&nbsp;<gmjsp:dropdown  width="150" controlName="distributor" SFFormName="frmCaseSchedularForm" SFSeletedValue="distributor" SFValue="alDisList" codeId="ID" codeName="NAME"  
				            defaultValue="[Choose one]" onChange="javascript:fnSetShipTo();"/></td>
				<td class="RightTableCaption" align="right" height="30" width="150"><font color="red">*</font>&nbsp;<fmtCaseSchedule:message key="LBL_SALES_REP" />:&nbsp;</td>
				<td>&nbsp;<gmjsp:dropdown  width="150" controlName="salesrep" SFFormName="frmCaseSchedularForm" SFSeletedValue="salesrep" SFValue="alSalesRepList" codeId="ID" codeName="NAME" 
				defaultValue="[Choose one]" onChange="javascript:fnSetAssocReps(this.value);" /></td>
			</tr>

			<tr>
				<td colspan="4" height="1" class="LLine"></td>
			</tr>
			<tr height="30" class="Shade">
				<td class="RightTableCaption" HEIGHT="24" align="right" width="150" colspan="1"><font color="red">*</font>&nbsp;<fmtCaseSchedule:message key="LBL_ACCOUNT" />:</td>
				<td  align="left" colspan="1">&nbsp;<gmjsp:dropdown  width="300" controlName="account" SFFormName="frmCaseSchedularForm" SFSeletedValue="account" SFValue="alListAcc" codeId="ID" codeName="NAME"  
				            defaultValue="[Choose one]" onChange=""/></td>
				 <td class="RightTableCaption" align="right" height="24"><fmtCaseSchedule:message key="LBL_LONG_TERM"/>:</td>	
				 <td><html:checkbox name="frmCaseSchedularForm" property="longTerm" title="off" onclick="check();"></html:checkbox> </td> 
				</tr>
			<tr style="background: #000000;">
				<td colspan="4" height="1"></td>
			</tr>
			

<tr class="ShadeRightTableCaption">
	<td Height="24" colspan="4" style="font-size:9pt;"><font color="red">*</font>&nbsp;<fmtCaseSchedule:message key="LBL_COMMENTS"/></td>
</tr>

	<tr><td height="1" colspan="4" bgcolor="#666666"></td></tr>		
	<tr>
	    <td colspan="4" height="50"  ><input type="hidden" name="hShowCam" value="">&nbsp;<textarea id="comments" name="" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
		onBlur="changeBgColor(this,'#ffffff');"  rows=3 cols=100></textarea></td>
	</tr>

	<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
	<tr><td colspan="4">
	<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" id="CASELOG">
	<TR bgcolor="#EEEEEE" class="RightTableCaption">
		<TD class="RightTableCaption" width="60" height="25"><fmtCaseSchedule:message key="LBL_DATE"/></TD>
		<TD class="RightTableCaption" width="140"><fmtCaseSchedule:message key="LBL_UNAME"/></TD>
		<TD  class="RightTableCaption" width="500"><fmtCaseSchedule:message key="LBL_COMMENTS"/></TD>
		
<!-- 		
		
		<td width="60" height="25">&nbsp;DATE</td>
		<td width="140">NAME </td>
	    <td colspan="2" width="500">Commtens</td>  -->
	</TR>
	<TR><TD colspan=4 height=1 class=Line></TD></TR> 
<%
if (alLogReasons != null){
	intLog = alLogReasons.size();
}
if(intLog>3){
	blHideComments  = true;
}
if (intLog > 0)
{
						HashMap hmLog = new HashMap();
						HashMap hmTempLoop = new HashMap();
						intLog = alLogReasons.size();
						for (int i = 0;i < intLog ;i++ )
						{
							hmLog = (HashMap)alLogReasons.get(i); %>
			<tr class="TRLOG">
				<td id="DTC<%=i%>" class="RightText" height="20">&nbsp;<%=(String)hmLog.get("DT")%></td>
				<td id="UNAMEC<%=i%>" class="RightText">&nbsp;<%=(String)hmLog.get("UNAME")%></td>
				<td id="COMMENTSC<%=i%>" style="width: 500px;" width="500"><%=(String)hmLog.get("COMMENTS")%></td></tr>
			<%}}else {
	%>		<tr class="NODATA"><td colspan="4" height="25" align="center" class="RightTextBlue">
	<BR><fmtCaseSchedule:message key="TD_NO_DATA"/></td></tr>
<%

}
%><tr>
				<td colspan="4" height="5"></td>
			</tr>
		</TABLE></td></tr>		
			
	<tr style="background: #000000;">
				<td colspan="4" style="height:0.5px"></td>
			</tr> 
		
			<tr>
				<td colspan="4" height="5"></td>
			</tr>
			<tr>
				<td colspan="4" align="center" height="24">
					<fmtCaseSchedule:message key="BTN_SUBMIT" var="varSubmit" /> 
					<fmtCaseSchedule:message key="BTN_RESET" var="varReset" /> 
					<% if(status.equals("107162") || status.equals("107163")) {%>
					<gmjsp:button value="${varReset}" name="caseRes" gmClass="button" buttonType="Save" onClick="fnCaseReset(this.form)" disabled="true" />&nbsp; 
					<gmjsp:button value="${varSubmit}" name="caseSub" gmClass="button" buttonType="Save" onClick="fnCaseSubmit(this.form)" disabled="true" />&nbsp;
					<%}else{ %>
					<gmjsp:button value="${varReset}" name="caseRes" gmClass="button" buttonType="Save" onClick="fnCaseReset(this.form)" disabled="flase" />&nbsp; 
					<gmjsp:button value="${varSubmit}" name="caseSub" gmClass="button" tabindex="" buttonType="Save" onClick="fnCaseSubmit(this.form)" disabled="flase" />&nbsp;
					<%} %>
					</td>
			</tr>
			<tr>
				<td colspan="4" height="5"></td>
			</tr>
				
			<tr style="background: #000000;">
				<td colspan="4" style="height:0.5px"></td>
			</tr> 
			
			<tr>
				<td height="25" colspan="4" align="center" class="" id="SucMsg" style="color: green"><b><%=strSucMsg%></b></td>
			</tr>
		</table>
	</html:form>
</BODY>
</HTML>