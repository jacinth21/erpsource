<!-- \sales\CaseManagement\GmFavouriteCase.jsp -->
 <%
	/**********************************************************************************
	 * File		 		: GmFavouriteCase.jsp
	 * Desc		 		: This screen is used to save Favourite case
	 * Version	 		: 1.0
	 * author			: DhanaReddy
	 * 
	 ************************************************************************************/
%>  
 <%@ include file="/common/GmHeader.inc"%>
 <%@ taglib prefix="fmtFavouriteCase" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtFavouriteCase:setLocale value="<%=strLocale%>"/>
<fmtFavouriteCase:setBundle basename="properties.labels.sales.CaseManagement.GmFavouriteCase"/>
 
 <bean:define id="caseInfoId" name="frmFavouriteCase" property="caseInfoID" type="java.lang.String"></bean:define>
 <bean:define id="caseId" name="frmFavouriteCase" property="caseID" type="java.lang.String"></bean:define>
 <bean:define id="strOpt" name="frmFavouriteCase" property="strOpt" scope="request" type="java.lang.String"></bean:define>
<%
String strSalesCaseMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_CASEMANAGEMENT");
GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.sales.CaseManagement.GmFavouriteCase", strSessCompanyLocale);
String strFavFrom =GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_FAVOURITE_CASE"));
String strFavID =GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_ID"));
String strFavNm =GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_NAME"));
if(strOpt.equals("eventfav")){
	strFavFrom =GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_FAVOURITE_EVENT"));
	strFavID =GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_EVENT_NAME"));
	strFavNm =GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_FAVOURITE_NAME"));
}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Favorite Case Book </TITLE>
 <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strSalesCaseMgmntJsPath%>/GmFavouriteCase.js"></script>
<script>
var strCaseInfoId= "<%=caseInfoId%>";
var strCaseId= "<%=caseId%>";
var strOpt = "<%=strOpt%>";
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10"  >
<html:form action="/gmFavouriteCase.do">
 <html:hidden name="frmFavouriteCase" property="caseInfoID" />
 <html:hidden name="frmFavouriteCase" property="caseID" />
 <html:hidden name="frmFavouriteCase" property="strOpt" />
	<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr >
					     <td height="25" colspan="1" class="RightDashBoardHeader">&nbsp;<%=strFavFrom%></td>
						 <td align="right" class=RightDashBoardHeader>
						<fmtFavouriteCase:message key="IMG_HELP" var="varHelp"/><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${Help}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("FAVORITE_CASE")%>');" />
						</td>
					</tr>
					<tr><td class="Line" height="1" colspan="2"></td></tr>
					 
					<tr>
						<td height="25" class="RightTableCaption" align="Right"> &nbsp;<%=strFavID%></td>
						<td width="70%">&nbsp; <bean:write name="frmFavouriteCase" property="caseID" /> </td>
					</tr>
				 
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="Shade">
						<td height="25" class="RightTableCaption" align="Right"><font color="red">*</font>&nbsp;<%=strFavNm%></td>
						<td width="70%">&nbsp; <html:text property="favouriteName" name="frmFavouriteCase" size="25" maxlength="55" onfocus="changeBgColor(this,'#AACCE8'); " onblur="changeBgColor(this,'#ffffff'); "/>
						</td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
				   <tr>
						<td class="RightRedCaption" colspan="2" align="center">&nbsp;
							<% if (!strClientSysType.equals("IPAD") ){%>
								<fmtFavouriteCase:message key="BTN_BACK" var="varBack"/><gmjsp:button style="width: 6em" value="${varBack}" buttonType="Load" onClick="fnBack();" />&nbsp;
							<%}else{ %>
								<fmtFavouriteCase:message key="BTN_BACK" var="varBack"/><gmjsp:button style="width: 5em" value="${varBack}" buttonType="Load"  onClick="fnCaseActionBack();"/>&nbsp;
							<%} %>
								<fmtFavouriteCase:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button style="width: 8em" value="${varSubmit}" buttonType="Save" onClick="fnSubmit();"/>
							
						</td>
					</tr>
				</table>
			</td>
		</tr>
 
    </table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
 

