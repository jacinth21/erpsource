<%
/**********************************************************************************
 * File		 		: GmCaseScheduler.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Agilan S
************************************************************************************/
%>
<!--\Sales\CaseManagement\GmCaseScheduler.jsp -->

<%@ page language="java"%>
<%@page import="java.util.Date"%> 
<%@page import="java.net.URLEncoder"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmCommonClass"%>
<%@ page import = "java.util.*,java.io.*"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtCaseSchedule" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmtCaseSchedule:setLocale value="<%=strLocale%>"/>
<fmtCaseSchedule:setBundle basename="properties.labels.sales.CaseManagement.GmCaseScheduler"/>
<bean:define id="caseId" name="frmCaseSchedularForm" property="caseId" scope="request" type="java.lang.String"></bean:define>
<bean:define id="status" name="frmCaseSchedularForm" property="status" scope="request" type="java.lang.String"></bean:define>
<% 
String strSalesCaseMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_CASEMANAGEMENT");
String strTDHeight = "500px";  
String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Case Schedule </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxlayout.css">
<link rel="STYLESHEET" type="text/css"href="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.css">
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script> 
<script language="JavaScript" src="<%=strSalesCaseMgmntJsPath%>/GmCaseSchedular.js"></script>
<style>
table.DtTable800 {
	margin: 0 0 0 0;
	width: 800px;
	border: 1px solid  #676767;
}
.dhx_tabcontent_zone {
width:792px !important;
}
</style>

<script type="text/javascript">
var strCompanyInfo ='<%=strCompanyInfo%>';
var global_param ="";
var href;//To give the path
//Make the Details tab active on page load
</script>
<style type="text/css" media="all">
@import url("styles/screen.css");
</style>
<BODY leftmargin="20" topmargin="10" onload="fnOnloadpage();"> 
<html:form  action="/gmCaseSchedular.do?" >  
  
    <table  border="0" class="DtTable800" cellspacing="0" cellpadding="0" >
			<tr>
				<td height="25" colspan="3" class="RightDashBoardHeader">&nbsp;<fmtCaseSchedule:message key="LBL_HEADING" /></td>
				<td align="right" class=RightDashBoardHeader>
				<fmtCaseSchedule:message
						key="IMG_ALT_HELP" var="varHelp" /><img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("CASE_SCHEDULER_NEW_CASE")%>');" />
				</td>
			</tr>
			<tr>
				<td class="RightTableCaption" align="right" height="30" width="150"><b><fmtCaseSchedule:message key="LBL_CASE" />:&nbsp;</b></td>
				<td width="150" align="left" id="caseId"><%=caseId%></td>
				<td class="RightTableCaption" align="right" height="30" width="150"><b><fmtCaseSchedule:message key="LBL_STATUS" />:&nbsp;</b></td>
				<td><gmjsp:dropdown controlName="status"
						SFFormName="frmCaseSchedularForm" SFSeletedValue="status"
						SFValue="alSetStatus" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose one]" /></td>
			</tr>

			<tr>
				<td colspan="4">
					<table cellpadding="0">
						<tr>
							<td id="my_tabbar"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
 
  </html:form>

<%@ include file="/common/GmFooter.inc"%>
</body>
</html>