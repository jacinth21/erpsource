<% 
/**********************************************************************************
 * File		 		: GmInvSelection.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Agilan S
************************************************************************************/
%>
<!--\Sales\CaseManagement\GmInvSelection.jsp -->
<%@ page language="java"%>
<%@page import="java.util.Date"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="com.globus.common.beans.GmCommonClass"%>
<%@ page import="java.util.*,java.io.*"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.net.URLEncoder"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="fmtCaseSchedule" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmtCaseSchedule:setLocale value="<%=strLocale%>" />
<fmtCaseSchedule:setBundle basename="properties.labels.sales.CaseManagement.GmCaseScheduler" />
<bean:define id="caseId" name="frmCaseSchedularForm" property="caseId" scope="request" type="java.lang.String"></bean:define>
<bean:define id="searchkitId" name="frmCaseSchedularForm" property="searchkitId" scope="request" type="java.lang.String"></bean:define>
<bean:define id="alList" name="frmCaseSchedularForm" property="alList" scope="request" type="java.util.ArrayList"></bean:define>
<bean:define id="status" name="frmCaseSchedularForm" property="status" scope="request" type="java.lang.String"></bean:define>
<%
String strSalesCaseMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_CASEMANAGEMENT");
int intSize=0;
int intCount = 0;
String strLine = "";
boolean blFlag = false;
String strKitName = "";
String strSetId = "";
String strSetNm = "";
String strQty = "";
String strSts = "";
String strTagNum = "";
String strKitNm = "";
String strShade = "";
String strKitId = "";
String strStatus = "";
String strCompanyId = gmDataStoreVO.getCmpid();
String strPlantId = gmDataStoreVO.getPlantid();
String strPartyId = gmDataStoreVO.getPartyid();
String strSessApplDateFmt= strGCompDateFmt;
String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
strCompanyInfo = URLEncoder.encode(strCompanyInfo);
int count = alList.size();
int intLog = 0;	
boolean blHideComments = false;
ArrayList alLogReasons = (ArrayList)request.getAttribute("hmLog");
intLog = alLogReasons.size();
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Inventory Selection</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<link rel="STYLESHEET" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxlayout.css">
<link rel="STYLESHEET" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.css">
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strSalesCaseMgmntJsPath%>/GmCaseSchedular.js"></script>
<script>
var countval = '<%=count%>';
var compInfObj = '<%=strCompanyInfo%>'; 
var companyId = '<%=strCompanyId%>';
var gCompDateFmt = "<%=strGCompDateFmt%>";
</script>
<style type="text/css" media="all">
@import url("styles/screen.css");
</style>
</HEAD>
<BODY leftmargin="0" rightmargin="0" topmargin="10" onload="fnOnLoad();">

	<html:form action="/gmCaseSchedular.do">
		<html:hidden styleId="strOpt" property="strOpt" />
		<input type="hidden" id="hRowCnt" value="<%=count%>" />
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td width="140" class="RightTableCaption" HEIGHT="24" align="right"><fmtCaseSchedule:message key="LBL_INVENTORY_SELECTION" />:&nbsp;</td>
				<td width="70" align="left"><select><option value="0">By Kit</option></select> 
				</td>
				<td align="left" style="margin-top: 3.5px; position:absolute;"><jsp:include page="/common/GmAutoCompleteInclude.jsp">
						<jsp:param name="CONTROL_NAME" value="kitID" />
						<jsp:param name="METHOD_LOAD" value="loadKitNameList&activeFl=Y" />
						<jsp:param name="WIDTH" value="400" />
						<jsp:param name="CSS_CLASS" value="search" />
						<jsp:param name="TAB_INDEX" value="4" />
						<jsp:param name="CONTROL_NM_VALUE" value="<%=searchkitId%>" />
						<jsp:param name="CONTROL_ID_VALUE" value="" />
						<jsp:param name="SHOW_DATA" value="10" />
						<jsp:param name="AUTO_RELOAD" value="fnFetechKitDetails()" />
					</jsp:include>
					<span id="validSetError" style=" display: none; float: right; position: relative;  top: -15px; right: -18px;">
					<img tabindex="-1"  id = "msg" title="Kit Name is not available for this time, please select different inventory or reschedule the case" height=13 width=13 src="<%=strImagePath%>/error.gif"></img>
						</span>
					</td>
			</tr>
			<tr style="background: #000000;">
				<td colspan="4" height="1"></td>
			</tr>
			<tr>
				<td colspan="4">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" id="KitMapping">
									<TR class="Shade" height="25">
										<TD class="ShadeRightTableCaption" width="150" align="left"><fmtCaseSchedule:message key="LBL_KIT_NAME_SET_ID" /></TD>
										<TD class="ShadeRightTableCaption" width="300" align="left"><fmtCaseSchedule:message key="LBL_DESCRIPTION" /></TD>
										<TD class="ShadeRightTableCaption" width="100" align="center"><fmtCaseSchedule:message key="LBL_QTY" /></TD>
										<TD class="ShadeRightTableCaption" width="150" align="center"><fmtCaseSchedule:message key="LBL_TAG_NUMBER" /></TD>

									</TR>

									<tr style="background: #000000;">
										<td colspan="4" height="1"></td>
									</tr>
									<%			
					if (alList != null)
					{
						HashMap hmLoop = new HashMap();
						HashMap hmTempLoop = new HashMap();
						intSize = alList.size();

						hmTempLoop = new HashMap();
						strLine = "";
						blFlag = false;

						for (int i = 0;i < intSize ;i++ )
						{
							hmLoop = (HashMap)alList.get(i);

							if (i<intSize-1)
							{
								hmTempLoop = (HashMap)alList.get(i+1);
								strKitName = GmCommonClass.parseNull((String)hmTempLoop.get("KITNM"));
							}

							strSetId = GmCommonClass.parseNull((String)hmLoop.get("SETID"));
							strKitNm = GmCommonClass.parseNull((String)hmLoop.get("KITNM"));
							strKitId = GmCommonClass.parseNull((String)hmLoop.get("KITID"));
							strSetNm = GmCommonClass.parseNull((String)hmLoop.get("SETNM"));
							strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
							strTagNum = GmCommonClass.parseNull((String)hmLoop.get("TAGID"));
							strStatus = GmCommonClass.parseNull((String)hmLoop.get("STATUS"));
							if (strKitName.equals(strKitNm))
							{
								intCount++;
								strLine = "<TR><TD colspan=4 height=1 class=Line></TD></TR>";
							}
							else
							{
								strLine = "<TR><TD colspan=4 height=1 class=Line></TD></TR>";
								if (intCount > 0)
								{
									strSetNm = "";
									strLine = "";
								}
								else
								{
									blFlag = true;
								}
								intCount = 0;
							}

							if (intCount > 1)
							{
								strKitNm = "";
								strLine = "";
							}
						
							strShade	= (i%2 == 0)?"class=Shade":""; //For alternate Shading of rows

							out.print(strLine);
							if (intCount == 1 || blFlag)
							{
%>
									<tr style="background: #e6e6e6;" class="RightTableCaption"
										id="tr<%=strKitNm%>">

										<td colspan="4" width="80" height="20" class="RightText">&nbsp;
											<%if((strStatus.equals("107160"))|| (strStatus.equals(""))){ %>
											<img border=0 Alt=Remove title=Remove valign=left
											src=/images/red_btn_minus.gif height=10 width=9
											onclick="javascript:fnRemoveItem('<%=strKitId%>','<%=caseId%>','<%=strKitNm%>')"></img>
											<%} %>
											&nbsp;<%=strKitNm%></td>
									</tr>
									<tr>
										<td colspan="4"><div style="display: inline"
												id="div<%=strKitNm%>">
												<table width="100%" cellpadding="0" cellspacing="0"
													border="0" id="invselect">

													<%
								blFlag = false;
								}
%>
													<TR <%=strShade%> id="TR<%=i%>">

														<td id="setid<%=i%>" width="150" height="20" align="left"><%=strSetId%></td>
														<td id="setnm<%=i%>" width="300" class="RightText" align="left"><%=strSetNm%></td>
														<td id="setqty<%=i%>" width="100" class="RightText" align="center">&nbsp;<%=strQty%></td>
														<td id="tagnum<%=i%>" width="150" class="RightText" align="center">&nbsp;<%=strTagNum%></td>
													<TR>
												
													<tr>
														<td colspan="4" height="1" bgcolor="#eeeeee"></td>
													</tr>
													<%					
							if ( intCount == 0 || i == intSize-1)
							{
%>
												</table>
											</div></td>
									</tr>
									<%
							}
										

						} // End of For
					}
						
%>
								</TABLE>
							</TD>
						</TR>
					</TABLE>
				</TD>
			</tr>
		</table>
		<tr><td colspan="4">

	<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" id="CASELOG">
	<%if(intLog != 0) {%> 
	<tr class="theaderLine"><td colspan="3" height="70"></td></tr>
	<tr style="background: #000000;"><td colspan=3 height=1 class=Line></td></tr>
	<TR bgcolor="#EEEEEE" class="RightTableCaption theader">
		<TD class="RightTableCaption" width="60" height="25"><fmtCaseSchedule:message key="LBL_DATE"/></TD>
		<TD class="RightTableCaption" width="140"><fmtCaseSchedule:message key="LBL_UNAME"/></TD>
		<TD  class="RightTableCaption" width="500"><fmtCaseSchedule:message key="LBL_COMMENTS"/></TD>
			
	</TR>
 	<%} %> 
	<TR><TD colspan=4 height=1 class=Line></TD></TR> 
<%
if (alLogReasons != null){
	intLog = alLogReasons.size();
}
if(intLog>3){
	blHideComments  = true;
}
if (intLog > 0)
{
						HashMap hmLog = new HashMap();
						HashMap hmTempLoop = new HashMap();
						intLog = alLogReasons.size();
						for (int i = 0;i < intLog ;i++ )
						{
							hmLog = (HashMap)alLogReasons.get(i); %>
			<tr class="TRLOG">
				<td id="DTC<%=i%>" class="RightText" height="20">&nbsp;<%=(String)hmLog.get("DT")%></td>
				<td id="UNAMEC<%=i%>" class="RightText">&nbsp;<%=(String)hmLog.get("UNAME")%></td>
				<td id="COMMENTSC<%=i%>" style="width: 500px;" width="500"><%=(String)hmLog.get("COMMENTS")%></td></tr>

					 <%}}	%><%-- else {
		<tr class="NODATA"><td colspan="4" height="25" align="center" class="RightTextBlue">
	<BR><fmtCaseSchedule:message key="TD_NO_DATA"/></td></tr>
<%}%> --%>

		</TABLE>	
		</td></tr>		
				
	</html:form>
	<script>
	cnt = document.frmCaseSchedularForm.hRowCnt.value;
	var status ='<%=strStatus%>';
	</script> 
</BODY>
</HTML>