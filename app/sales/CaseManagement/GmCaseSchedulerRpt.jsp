<!-- \sales\CaseManagement\GmCaseSchedulerRpt.jsp -->
<%
	/**********************************************************************************
	 * File		 		: GmCaseSchedulerRpt.jsp
	 * Desc		 		: Case Scheduler Report Page
	 * Version	 		: 1.0
	 * author			: N Raja
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="frmtCaseListRpt"	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<frmtCaseListRpt:setLocale value="<%=strLocale%>" />
<frmtCaseListRpt:setBundle basename="properties.labels.sales.CaseManagement.GmCaseSchedulerRpt" />

<bean:define id="gridXmlData" name="frmCaseScheduleRpt" property="gridXmlData" type="java.lang.String"></bean:define>
<bean:define id="dateSelection" name="frmCaseScheduleRpt" property="dateSelection" type="java.lang.String"></bean:define>
<bean:define id="division" name="frmCaseScheduleRpt" property="division" type="java.lang.String"></bean:define>
<bean:define id="account" name="frmCaseScheduleRpt" property="account" type="java.lang.String"></bean:define>
<bean:define id="fieldSales" name="frmCaseScheduleRpt" property="fieldSales" type="java.lang.String"></bean:define>
<bean:define id="caseInventory" name="frmCaseScheduleRpt" property="caseInventory" type="java.lang.String"></bean:define>
<bean:define id="salesRep" name="frmCaseScheduleRpt" property="salesRep" type="java.lang.String"></bean:define>
<bean:define id="status" name="frmCaseScheduleRpt" property="status" type="java.lang.String"></bean:define>
<bean:define id="searchaccount" name="frmCaseScheduleRpt" property="searchaccount" type="java.lang.String"></bean:define>
<bean:define id="searchsalesRep" name="frmCaseScheduleRpt" property="searchsalesRep" type="java.lang.String"></bean:define>  
<bean:define id="searchfieldSales" name="frmCaseScheduleRpt" property="searchfieldSales" type="java.lang.String"></bean:define>  
<bean:define id="kitMap" name="frmCaseScheduleRpt" property="kitMap" type="java.lang.String"></bean:define> 
<bean:define id="kitMapId" name="frmCaseScheduleRpt" property="kitMapId" type="java.lang.String"></bean:define> 
<bean:define id="searchkitMapId" name="frmCaseScheduleRpt" property="searchkitMapId" type="java.lang.String"></bean:define> 
<bean:define id="alStatus" name="frmCaseScheduleRpt" property="alStatus" type="java.util.ArrayList"></bean:define>
<%
String strSalesCaseMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_CASEMANAGEMENT");
String strGridXmlData = GmCommonClass.parseNull((String)request.getAttribute("XMLGRIDDATA"));  
String strFromDt = GmCommonClass.parseNull((String)request.getAttribute("hFrmDt"));  
String strToDt = GmCommonClass.parseNull((String)request.getAttribute("hToDt"));  
String strSessApplDateFmt= strGCompDateFmt;
HashMap hmSystemMap = new HashMap();
hmSystemMap.put("ID","CODEID");
hmSystemMap.put("PID","CODEID");
hmSystemMap.put("NM","CODENM");

%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Case Scheduler Report</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strSalesCaseMgmntJsPath%>/GmCaseSchedulerRpt.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/jquery-latest.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
var objGridData;
objGridData = '<%=strGridXmlData%>'; 
var gCmpDateFmt = "<%=strGCompDateFmt%>";
var gridObj;
</script>

<style>
.autocompleteINV .search-container {
	display: inline-block;
}
div#Div_Status{
width: 100px;
height: 80px;
}
</style>

</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
	<html:form action="/gmCaseScheduleReport.do?method=caseSchedulerListRpt">
	<html:hidden name="frmCaseScheduleRpt"  property="strOpt" value="" /> 
	<html:hidden name="frmCaseScheduleRpt"  property="status" value="" /> 
	<input type="hidden" name="hStatusGrp" value="<%=status%>">
		<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" colspan="2" class="RightDashBoardHeader"><frmtCaseListRpt:message
						key="LBL_CASE_SCHEDULER_REPORT" /></td>
				<td class="RightDashBoardHeader" align="right" colspan="2"></td>
				<td align="right" class=RightDashBoardHeader><frmtCaseListRpt:message
						key="IMG_HELP" var="varHelp" /><img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("CASE_SCHEDULER_REPORT")%>');" />
				</td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="5"></td>
			</tr>
			<tr class="evenshade">
				<td class="RightRedCaption" align="Right"><font color="red">*</font>
				<frmtCaseListRpt:message key="LBL_DATE_SELECTION" />:</td> 
		 		 <td class="RightTableCaption" width="250">&nbsp;<gmjsp:dropdown controlName="dateSelection" SFFormName="frmCaseScheduleRpt" SFSeletedValue="surgerydate"
					SFValue="aldateSelection" codeId="CODEID"  codeName="CODENM" />
				  <gmjsp:calendar SFFormName="frmCaseScheduleRpt" SFDtTextControlName="fromDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" textValue="<%=(strFromDt==null?null: (new java.sql.Date(GmCommonClass.getStringToDate(strFromDt,strSessApplDateFmt).getTime())))%>"/>
						</td>
					<td>
					<gmjsp:calendar	SFFormName="frmCaseScheduleRpt" SFDtTextControlName="toDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" textValue="<%=(strToDt.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strToDt,strSessApplDateFmt).getTime())))%>" /></td>
				<td class="RightRedCaption" align="Right" colspan=""><frmtCaseListRpt:message key="LBL_DIVISION" />:</td>
				<td>&nbsp;<gmjsp:dropdown controlName="division" SFFormName="frmCaseScheduleRpt" SFSeletedValue="spineDivision" SFValue="alDivision" codeId="DIVISION_ID" codeName="DIVISION_NAME" defaultValue="[Choose one]"  />
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="5"></td>
			</tr>
			<tr class="oddshade">
				<td class="RightRedCaption" align="Right"><frmtCaseListRpt:message
						key="LBL_FIELD_SALES" />:</td>
				<td><jsp:include page="/common/GmAutoCompleteInclude.jsp">
						<jsp:param name="CONTROL_NAME" value="fieldSales" />
						<jsp:param name="METHOD_LOAD" value="loadFieldSalesList" />
						<jsp:param name="WIDTH" value="200" />
						<jsp:param name="CSS_CLASS" value="search" />
						<jsp:param name="TAB_INDEX" value="4" />
						<jsp:param name="CONTROL_NM_VALUE" value="<%=searchfieldSales%>" />
						<jsp:param name="CONTROL_ID_VALUE" value="<%=fieldSales%>" />
						<jsp:param name="SHOW_DATA" value="10" />
						<jsp:param name="ON_BLUR" value=" fnAcBlur(this);" />
					</jsp:include></td>
					<td></td>
					
				<td class="RightRedCaption" align="Right"><frmtCaseListRpt:message
						key="LBL_SALES_REP" />:</td>
				<td colspan="2"><jsp:include page="/common/GmAutoCompleteInclude.jsp">
						<jsp:param name="CONTROL_NAME" value="salesRep" />
						<jsp:param name="METHOD_LOAD" value="loadSalesRepList" />
						<jsp:param name="WIDTH" value="200" />
						<jsp:param name="CSS_CLASS" value="search" />
						<jsp:param name="TAB_INDEX" value="4" />
						<jsp:param name="CONTROL_NM_VALUE" value="<%=searchsalesRep%>" />
						<jsp:param name="CONTROL_ID_VALUE" value="<%=salesRep%>" />  
						<jsp:param name="SHOW_DATA" value="10" />
						<jsp:param name="ON_BLUR" value=" fnAcBlur(this);" />
					</jsp:include></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="5"></td>
			</tr>
			<tr class="evenshade">
				<td class="RightRedCaption" align="Right"><frmtCaseListRpt:message
						key="LBL_ACCOUNT" />:</td>
				<td><jsp:include page="/common/GmAutoCompleteInclude.jsp">
						<jsp:param name="CONTROL_NAME" value="account" />
						<jsp:param name="METHOD_LOAD" value="loadAccountList&searchType=LikeSearch" />
						<jsp:param name="WIDTH" value="200" />
						<jsp:param name="CSS_CLASS" value="search" />
						<jsp:param name="TAB_INDEX" value="4" />
						<jsp:param name="CONTROL_NM_VALUE" value="<%=searchaccount%>"/>
						<jsp:param name="CONTROL_ID_VALUE" value="<%=account%>" />
						<jsp:param name="SHOW_DATA" value="10" />
						<jsp:param name="ON_BLUR" value=" fnAcBlur(this);" />
						</jsp:include>
                </td>
				<td class="RightRedCaption" align="Right" colspan="2"><frmtCaseListRpt:message
						key="LBL_CASE_INVENTORY" />:&nbsp;<select name="kitMap" id="kitMap" class="RightText" tabindex="20">
					 	<option value="0" >By Kit</option>
				 	</select></td>
				 	<td><jsp:include page="/common/GmAutoCompleteInclude.jsp">
						<jsp:param name="CONTROL_NAME" value="kitMapId" />
						<jsp:param name="METHOD_LOAD" value="loadKitNameList" />
						<jsp:param name="WIDTH" value="200" />
						<jsp:param name="CSS_CLASS" value="search" />
						<jsp:param name="TAB_INDEX" value="4" />
						<jsp:param name="CONTROL_NM_VALUE" value="<%=searchkitMapId%>" />
						<jsp:param name="CONTROL_ID_VALUE" value="<%=kitMapId%>" />
						<jsp:param name="SHOW_DATA" value="10" />
						<jsp:param name="AUTO_RELOAD"	value="" />
					</jsp:include> </td>
					
			</tr>
					<tr class="oddshade">
						<td class="RightRedCaption" align="Right"><frmtCaseListRpt:message key="LBL_STATUS" />:&nbsp;</td>
						<td valign="middle">&nbsp;<%=GmCommonControls.getChkBoxGroup("",alStatus,"Status",hmSystemMap)%>&nbsp;</td>					
				       <%--  <td class="RightTableCaption">&nbsp;<gmjsp:dropdown controlName="status" SFFormName="frmCaseScheduleRpt" SFSeletedValue="status" SFValue="alStatus" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]" /></td> --%> 
				       <td></td> 
				       <td class="RightTableCaption" align="right" height="24"><frmtCaseListRpt:message key="LBL_CASE_LENGTH"/>:</td>			       
				       <td class="RightTableCaption">&nbsp;<gmjsp:dropdown controlName="term" SFFormName="frmCaseScheduleRpt" SFSeletedValue="longTerm" SFValue="alTerm" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]" />	 
				 		<%-- <td><html:checkbox name="frmCaseScheduleRpt" property="longTerm" title="off" onclick="check();"></html:checkbox>--%>	 
						<frmtCaseListRpt:message key="BTN_LOAD" var="varLoad"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
						<gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" buttonType="Load" onClick="fnLoadCaseRpt();"/> </td>
			        </tr>
		<tr>	
       <tr><td colspan="5" height="1" bgcolor="#040404"></td></tr>	
       <%if (gridXmlData.indexOf("cell") != -1) {%>  
      <tr> <td colspan="5">
          <div id="caseListRpt" style="height: 500px; width:995px; display:table-caption;"></div>
 	 	 </td>
 	  </tr>
     <tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>
     <tr>
	<td colspan="5" align="center">
	    <div class='exportlinks'><frmtCaseListRpt:message key="LBL_EXPORT_OPTS"/> : <img src='img/ico_file_excel.png' onclick='fnExcel();'/>&nbsp;<a href="#" onclick="fnExcel();"><frmtCaseListRpt:message key="LBL_EXCEL"/></a></div>
	</td>
	</tr> 
		 <% }else{ %>
		 <tr>
		 <td height="30" colspan="5" align="center" class="RightTextBlue"><frmtCaseListRpt:message key="LBL_NO_DIS"/></td>
		</tr>
		<%} %>	

			</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
