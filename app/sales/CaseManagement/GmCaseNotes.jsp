<%
/**********************************************************************************
 * File		 		: GmCaseNotes.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Agilan S
************************************************************************************/
%>
<!--\Sales\CaseManagement\GmCaseNotes.jsp -->
<%@ page language="java"%>
<%@page import="java.util.Date"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmCommonClass"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.*,java.io.*"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="fmtCaseSchedule" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<fmtCaseSchedule:setLocale value="<%=strLocale%>" />
<fmtCaseSchedule:setBundle basename="properties.labels.sales.CaseManagement.GmCaseScheduler" />
<bean:define id="caseId" name="frmCaseSchedularForm" property="caseId" scope="request" type="java.lang.String"></bean:define>
<%
String strSalesCaseMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_CASEMANAGEMENT");
String strSessApplDateFmt= strGCompDateFmt;
String strCompanyId = gmDataStoreVO.getCmpid();
String strPlantId = gmDataStoreVO.getPlantid();
String strPartyId = gmDataStoreVO.getPartyid();
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Case Notes</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<link rel="STYLESHEET" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxlayout.css">
<link rel="STYLESHEET" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strSalesCaseMgmntJsPath%>/GmCaseSchedular.js"></script>
<script>
var companyId = '<%=strCompanyId%>';
var gCompDateFmt = "<%=strGCompDateFmt%>";
</script>
<style type="text/css" media="all">
@import url("styles/screen.css");
</style>
</HEAD>
<BODY leftmargin="0" rightmargin="0" topmargin="0" onload="">

	<html:form action="/gmCaseSchedular.do">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td width="250" class="RightTableCaption" HEIGHT="24" align="left"><fmtCaseSchedule:message key="LBL_NOTES" />:&nbsp;</td>
			</tr>
			<tr>
				<td colspan="4" height="3"></td>
			</tr>
			<tr>
				<td colspan="4"><jsp:include page="/sales/CaseManagement/GmIncludeNotes.jsp">
						<jsp:param name="FORMNAME" value="frmCaseSchedularForm" />
						<jsp:param name="ALNAME" value="alLogReasons" />
						<jsp:param name="LogMode" value="Edit" />
						<jsp:param name="Mandatory" value="yes" />
					</jsp:include></td>
			</tr>
			<tr>
				<td colspan="4" height="100"></td>
			</tr>
			<tr style="background: #000000;">
				<td colspan="4" height="1"></td>
			</tr>
			<tr>
				<td colspan="4" height="12"></td>
			</tr>
			<tr>
				<td colspan="4" align="center" height="24">
					<fmtCaseSchedule:message key="BTN_SUBMIT" var="varSubmit" /> 
					<fmtCaseSchedule:message key="BTN_RESET" var="varReset" /> 
					<gmjsp:button value="${varReset}" gmClass="button" buttonType="Save" onClick="fnNoteReset(this.form)" />&nbsp; 
					<gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnCaseNoteSubmit(this.form)" />&nbsp;</td>
			</tr>
		</table>

	</html:form>
</BODY>
</HTML>