<!--\sales\CaseManagement\GmCaseAttributeDtls.jsp  -->

<%@page import="java.util.HashMap"%>
<%
/**********************************************************************************
 * File		 		: GmCaseAttributeDtls.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Xun
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%> 
<%@ taglib prefix="fmtCaseAttributeDtls" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtCaseAttributeDtls:setLocale value="<%=strLocale%>"/>
<fmtCaseAttributeDtls:setBundle basename="properties.labels.sales.CaseManagement.GmCaseAttributeDtls"/>

<%    
 String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>
<bean:define id="alCaseAttrDtls" name="<%=strFormName%>"  property="alCaseAttrDtls" type="java.util.List"></bean:define> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
 
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<tr><td class="Line" height="1" colspan="2"></td></tr>
<tr class="ShadeRightTableCaption" HEIGHT="24">
	<td  class="RightTableCaption" align="left" colspan="2"> 
	    <fmtCaseAttributeDtls:message key="LBL_ATTRIBUTE_DETAILS"/>&nbsp; 
	</td>	
</tr>					
<tr><td class="Line" height="1" colspan="2"></td></tr>
<%
		int intListSize = alCaseAttrDtls.size();
		if(intListSize > 0){
		HashMap hmValues = new HashMap();
		for(int j=0;j<alCaseAttrDtls.size();j++)
		{
			hmValues=(HashMap)alCaseAttrDtls.get(j);
			String strCatg = GmCommonClass.parseNull((String)hmValues.get("CATEGORY"));
			String strClassShade = "oddshade"; 
			if (j%2 == 0)
			{
				strClassShade = "evenshade"; 
			}
%>
<tr class="<%=strClassShade%>">
	<td colspan="2"><!-- Custom tag lib code modified for JBOSS migration changes -->		
		&nbsp;&nbsp;- &nbsp;<gmjsp:label type="RegularText"  SFLblControlName="<%=strCatg%>" td="false"/>
	</td>
</tr>				 
		<% 	
		}
		}else{
		%>	 
<tr class="evenshade">
	<td colspan="2">		
		&nbsp;&nbsp;<font color="red"> <fmtCaseAttributeDtls:message key="LBL_NO_ATTRIBUTES_ARE_SELECTED"/>-</font> 
	</td>
</tr>				 
<% 	
		}					
%>