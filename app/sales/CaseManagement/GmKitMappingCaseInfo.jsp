<!-- \sales\CaseManagement\GmKitMappingCaseInfo.jsp -->
<%
	/**********************************************************************************
	 * File		 		: GmKitMappingCaseInfo.jsp
	 * Desc		 		: Kit Mapping CaseInfo Page
	 * Version	 		: 1.0
	 * author			: N Raja
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%> 
<%@ taglib prefix="frmCaseSchedularForm" uri="http://java.sun.com/jsp/jstl/fmt"%>  
<%@ page isELIgnored="false"%>
<frmCaseSchedularForm:setLocale value="<%=strLocale%>" />
<frmCaseSchedularForm:setBundle basename="properties.labels.sales.CaseManagement.GmKitMappingCaseInfo" />
<%
String strLogisticsJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_LOGISTICS");
String strLongTerm = GmCommonClass.parseNull((String)request.getAttribute("STRLONGTERM"));
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Kit Mapping Case Info</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strLogisticsJsPath%>/GmKitMapping.js"></script>
<script>
var strLongTerm = '<%=strLongTerm%>';
</script>
</HEAD>
<BODY leftmargin="2" topmargin="2" onload="fnLoadCaseInfo();">
		<table border="0" class="DtTable730" cellspacing="0" cellpadding="0">
			
			<tr class="evenshade">
				<td class="RightRedCaption" colspan="2">
				<frmCaseSchedularForm:message key="LBL_CASEID" /> :
		 		 &nbsp;<bean:write name="frmCaseSchedularForm" property="caseId"/></td>  
				<td class="RightRedCaption" colspan="2"><frmCaseSchedularForm:message key="LBL_SURGERY_DATE" />:
				&nbsp;<bean:write name="frmCaseSchedularForm" property="strSurgeryDt"/></td>  
			</tr>
			
			<tr class="oddshade">
				<td class="RightRedCaption" colspan="2"><frmCaseSchedularForm:message
						key="LBL_SHIP_DATE" /> :
				&nbsp;<bean:write name="frmCaseSchedularForm" property="strShipDt"/></td>
				<td class="RightRedCaption" colspan="2"><frmCaseSchedularForm:message
						key="LBL_RETURN_DATE" /> :
				<bean:write name="frmCaseSchedularForm" property="strRetDt"/></td>
			</tr>
			
			<tr class="evenshade">
				<td class="RightRedCaption" colspan="2"><frmCaseSchedularForm:message
						key="LBL_ACCOUNT" /> :
					 <bean:write name="frmCaseSchedularForm" property="account"/> &#45; <bean:write name="frmCaseSchedularForm" property="searchaccount"/></td> 	
               <td class="RightRedCaption" colspan="2"><frmCaseSchedularForm:message
						key="LBL_DIVISION" /> :
						&nbsp;<bean:write name="frmCaseSchedularForm" property="division"/></td> 	
			</tr>
			<tr class="oddshade">
				<td class="RightRedCaption" colspan="2"><frmCaseSchedularForm:message
						key="LBL_FIELD_SALES" /> :
				&nbsp;<bean:write name="frmCaseSchedularForm" property="searchdistributor"/></td> 	
               <td class="RightRedCaption" colspan="2"><frmCaseSchedularForm:message
						key="LBL_SALES_REP" /> :
			   &nbsp;<bean:write name="frmCaseSchedularForm" property="searchsalesrep"/></td> 	
			</tr>
			<tr class="evenshade">
			 <td colspan="3" class="RightTableCaption" height="24"><frmCaseSchedularForm:message key="LBL_LONG_TERM"/> :&nbsp;<%=strLongTerm%>			 
			</tr>
			</table>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>