<!-- \sales\CaseManagement\GmCaseFavoritesRpt.jsp -->
<%
/**********************************************************************************
 * File		 		: GmCaseFavoritesRpt.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: 
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtCaseFavoritesRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtCaseFavoritesRpt:setLocale value="<%=strLocale%>"/>
<fmtCaseFavoritesRpt:setBundle basename="properties.labels.sales.CaseManagement.GmCaseFavoritesRpt"/>
<%
String strSalesCaseMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_CASEMANAGEMENT");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Case Favorites List Report  </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strSalesCaseMgmntJsPath%>/GmCaseFavoritesRpt.js"></script>

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>

</HEAD>
<BODY leftmargin="20" topmargin="10" >
<html:form  action="/gmCaseFavoritesRptAction.do?method=caseFavoritesRpt">
<html:hidden property="strOpt" name="frmCaseFavoritesRpt"/>
<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader" ><fmtCaseFavoritesRpt:message key="LBL_CASE_FAVOURITES_REPORT"/></td>
			<td  class="RightDashBoardHeader" align="right" colspan="3"></td>
			 <td align="right" class=RightDashBoardHeader>
			<fmtCaseFavoritesRpt:message key="IMG_ALT_HELP" var="varHelp"/><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("CASE_FAVORITE_REPORT")%>');" />
			</td> 
		</tr>
		<tr><td class="Line" height="1" colspan="6"></td></tr>
		<tr class="evenshade">
			<td class="RightRedCaption" align="Right" width=15%>&nbsp;<fmtCaseFavoritesRpt:message key="LBL_FAVORITE_NAME"/>:</td>
			<td > &nbsp;<html:text property="favoriteNm" name="frmCaseFavoritesRpt" size="15" /></td>
					
			<td class="RightTableRightRedCaptionCaption" align="Right" width=15%><fmtCaseFavoritesRpt:message key="LBL_SALES_REP"/>:</td>	
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<td >&nbsp;<gmjsp:dropdown controlName="rep"  SFFormName='frmCaseFavoritesRpt' SFSeletedValue="rep"  defaultValue= "[Choose One]"	
				SFValue="alRep" codeId="ID" codeName="NM"/></td>
				
			<td class="RightRedCaption" colspan="2" align="center"> <fmtCaseFavoritesRpt:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;${varLoad}&nbsp;" buttonType="Load" name="Btn_Submit"   onClick=" fnLoad();"/></td>
		</tr>
		<tr><td class="Line" height="1" colspan="6"></td></tr>
		<tr>
			<td colspan="6">
			    <display:table 
					name="frmCaseFavoritesRpt.alResult" 
					export="false" class="its" id="currentRowObject" 
					decorator="com.globus.displaytag.beans.sales.DTCaseFavoritesRptWrapper"
					freezeHeader="true"  paneSize="450" requestURI="/gmCaseFavoritesRptAction.do?method=caseFavoritesRpt">
					<fmtCaseFavoritesRpt:message key="DT_CASE_NAME" var="varCaseName"/><display:column property="CASENM" title="${varCaseName}" group="1" sortable="false"/>
					<fmtCaseFavoritesRpt:message key="DT_CATEGORY" var="varCategory"/><display:column property="CATEGORY" title="${varCategory}" class="alignleft" sortable="true"/>
					<fmtCaseFavoritesRpt:message key="DT_FIELD_SALES" var="varFieldSales"/><display:column property="DISTNM" title="${varFieldSales}" sortable="true"/>
					<fmtCaseFavoritesRpt:message key="DT_SALES_REP" var="varSalesRep"/><display:column property="REPNM" title="${varSalesRep}" sortable="true"/>
				</display:table>
			</td>
		</tr>

</table>	

</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>