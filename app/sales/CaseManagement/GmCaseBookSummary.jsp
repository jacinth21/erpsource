<!--  \sales\CaseManagement\GmCaseBookSummary.jsp -->
 <%
	/**********************************************************************************
	 * File		 		: GmCaseBookSummary.jsp
	 * Desc		 		: This screen is used for Case Book Set Summary
	 * Version	 		: 1.0
	 * author			: Xun
	 * 
	 ************************************************************************************/
%> 
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>  
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%>
<%@ page import ="java.util.Date"%>  
<%@ taglib prefix="fmtCaseBookSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtCaseBookSummary:setLocale value="<%=strLocale%>"/>
<fmtCaseBookSummary:setBundle basename="properties.labels.sales.CaseManagement.GmCaseBookSummary"/>

<bean:define id="caseInfoID" name="frmCasePost" property="caseInfoID" type="java.lang.String"></bean:define>
<bean:define id="hmGenerlInfo" name="frmCasePost"  property="hmGenerlInfo" type="java.util.HashMap"></bean:define> 
<bean:define id="alCaseAttrDtls" name="frmCasePost"  property="alCaseAttrDtls" type="java.util.List"></bean:define> 
<bean:define id="alAllBookedSets" name="frmCasePost" property="alAllBookedSets" type="java.util.List"></bean:define> 
<bean:define id="caseID" name="frmCasePost" property="caseID" type="java.lang.String"></bean:define>
<bean:define id="parentForm" name="frmCasePost" property="parentForm" type="java.lang.String"></bean:define>
<bean:define id="strPhoneOrderAccess" name="frmCasePost" property="phoneOrderAccess" type="java.lang.String"></bean:define> 


<% 
String strSalesCaseMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_CASEMANAGEMENT");
String strCaseID	 = GmCommonClass.parseNull((String) hmGenerlInfo.get("CASEID")); 
   String strStatusID	 = GmCommonClass.parseNull((String) hmGenerlInfo.get("STATUSID"));
   String strDistID	 = GmCommonClass.parseNull((String) hmGenerlInfo.get("DISTID"));
   String strRepID	 = GmCommonClass.parseNull((String) hmGenerlInfo.get("REPID"));
   String strAccountID	 = GmCommonClass.parseNull((String) hmGenerlInfo.get("ACCID"));
   String strAccountRepID	 = GmCommonClass.parseNull((String) hmGenerlInfo.get("ACCREPID"));
   String strCaseInfoId	 = GmCommonClass.parseNull((String) hmGenerlInfo.get("CASEINFOID"));
   String strAccountNm	 = GmCommonClass.parseNull((String) hmGenerlInfo.get("ACCOUNTNM"));
   String surgeryDt	 = GmCommonClass.getStringFromDate((Date)hmGenerlInfo.get("SURGERY_DATE"),strGCompDateFmt);
   String strDOID	 = GmCommonClass.parseNull((String) hmGenerlInfo.get("DOID")); 
   String strDOFL	 = GmCommonClass.parseNull((String) hmGenerlInfo.get("DOFL")); 
   String strDateFormat = strGCompDateFmt;
   String strTodaysDate = GmCalenderOperations.getCurrentDate(strDateFormat);

   String strTableClass = "DtTable1100"; 
   boolean bValueFound = GmCalenderOperations.isLessThan(surgeryDt, strTodaysDate,strDateFormat) || GmCalenderOperations.isEqual(surgeryDt, strTodaysDate, strDateFormat);
   
   boolean bPastCase = GmCalenderOperations.isLessThan(surgeryDt, strTodaysDate, strDateFormat);

   if (!parentForm.equals("")) {
	   strTableClass = "DtTable850"; 
   }
%>    
<HTML>
<HEAD>
<TITLE> Globus Medical: Case Book </TITLE> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css"> 
   
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>  
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script> 
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css"> 
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js" type="text/javascript"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js" type="text/javascript"></script> 
<script language="JavaScript" src="<%=strSalesCaseMgmntJsPath%>/GmCaseBookSummary.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
 <script>
 var strCaseInfoID = "<%=caseInfoID%>";
 var strCaseID="<%=strCaseID%>";
 var distID="<%=strDistID%>";
 var repID="<%=strRepID%>";
 var accountID="<%=strAccountID%>";
 var accountRepID="<%=strAccountRepID%>";
 var accountNm="<%=strAccountNm%>";
 var surgeryDt="<%=surgeryDt%>";
 var caseInfoID = "<%=strCaseInfoId%>"
 var strStatusID = "<%=strStatusID%>"
 var strPhoneOrder = "<%=strPhoneOrderAccess%>";
var dhxWins,w1; 

 </script>
 <script>
function fnClosePopup()
{
	parent.dhxWins.window("w1").close();
}

</script>
</HEAD>
 
<BODY leftmargin="20" topmargin="10" onload="fnPageLoad()">
<html:form action="/gmCasePost.do" >
<html:hidden property="strOpt"/>
<input type="hidden" name="hTxnId" value=""/>
<input type="hidden" name="hTxnName" value=""/>
<input type= "hidden" name="hAction" value=""/>
<input type= "hidden"  name="hCancelType" value=""/>
<input type= "hidden"  name="caseInfoID" value="<%=caseInfoID%>"/>
<input type= "hidden"  name="parentForm" value="<%=parentForm%>"/>
<input type= "hidden"  name="inputString" value=""/> 
   <div id="winVP" style="position: relative;  height: 1500px;   margin: 10px;">
	<table class="<%=strTableClass%>" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td >
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr >
						<td height="25" colspan="1" class="RightDashBoardHeader">&nbsp;<fmtCaseBookSummary:message key="LBL_CASE_BOOKING_SUMMARY_INFORMATION"/></td>
						  <td align="right" class=RightDashBoardHeader>
							<fmtCaseBookSummary:message key="IMG_HELP" var = "varHelp"/><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("CASE_BOOK_SUMMARY_INFO")%>');" />
						 </td>
					</tr>
					<tr><td class="Line" height="1" colspan="2"></td></tr>
					  
					<jsp:include page= "/sales/CaseManagement/GmCaseGeneralInfoHeader.jsp" >
					<jsp:param name="FORMNAME" value="frmCasePost" />
					</jsp:include>
					
			  		<jsp:include page= "/sales/CaseManagement/GmCaseAttributeDtls.jsp" >
			  		<jsp:param name="FORMNAME" value="frmCasePost" />
			  		</jsp:include>
				 	
					<tr><td class="Line" height="1" colspan="2"></td></tr>
					 
					 <jsp:include page= "/sales/CaseManagement/GmCaseSummarySetsList.jsp"/> 
			  		<%if (alAllBookedSets.size()> 0)  { %>	
 					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr  >
						  <td  class="RightTableCaption" align="left" height="25" colspan="2">
						       <span id="spnMsg"></span>
						 </td>
						 
					</tr> 
					 <%} %>
					 <tr><td class="Line" height="1" colspan="2"></td></tr>
					 
				   <tr>
						<td  height="40" colspan="2" align="center">&nbsp;
							<%if (parentForm.equals("")) {%>
							<% if (strStatusID.equals("11090"))  {%>
							<fmtCaseBookSummary:message key="BTN_EDIT_CASE" var="varEditCase"/><gmjsp:button style="width: 9em" value="${varEditCase}" buttonType="Save" onClick="fnEdit()" />&nbsp;
							 <% } 
							if (strStatusID.equals("11091") && !bPastCase)  { %>
							<fmtCaseBookSummary:message key="BTN_EDIT_CASE" var="varEditCase"/><gmjsp:button style="width: 9em" value="${varEditCase}" buttonType="Save" onClick="fnActiveEdit()" />&nbsp;
							<%if (strDOID.equals("")) { %>							
							<fmtCaseBookSummary:message key="BTN_RESCHEDULE_CASE" var="varRescheduleCase"/><gmjsp:button style="width: 14em" value="${varRescheduleCase}" buttonType="Save" onClick="fnReschedule()" />&nbsp;
							<%}%>
							<fmtCaseBookSummary:message key="BTN_CANCEL_CASE" var="varCancelCase"/><gmjsp:button style="width: 10em" buttonType="Save" value="${varCancelCase}"  onClick="fnCaseVoid()"  />&nbsp;
							<fmtCaseBookSummary:message key="BTN_INITIATE_LOANER" var="varInitiateLoaner"/><gmjsp:button style="width: 14em" buttonType="Save" value="${varInitiateLoaner}" name="btnInitLoan" onClick="fnInitiateLoaner();"  />&nbsp;
							<%}
							if ((strStatusID.equals("11091")&& bValueFound && strDOID.equals("")||(!strDOID.equals("")&&strDOFL.equals("0")))) { %>
							
							<fmtCaseBookSummary:message key="BTN_BOOK_NEW_DO" var="varBookNewDO"/><gmjsp:button style="width: 11em" value="${varBookNewDO}" buttonType="Save" onClick="fnBookNewDO()"/>&nbsp;
							<% } %>
							
							<fmtCaseBookSummary:message key="BTN_FAVOURITE" var="varFavorite"/><gmjsp:button style="width: 9em" value="${varFavorite}" buttonType="Save" onClick="fnFavourite()"  />&nbsp;
							<%} else { %>
							<fmtCaseBookSummary:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="&nbsp;${varClose}&nbsp;" buttonType="Load" onClick="javascript:fnClosePopup();"></gmjsp:button>
							<%}%>
						</td>
					</tr>
				</table>
			</td>
		</tr>
 
    </table></div>
</html:form>

 
<%@ include file="/common/GmFooter.inc" %>

</BODY>
 
</HTML>
 

