 <%
/**********************************************************************************
 * File		 		: GmTerritoryReport.jsp
 * Desc		 		: This screen is used for Territory / Quota Details
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<!-- sales\GmTerritoryReport.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<%@ taglib prefix="fmtTerritoryReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtTerritoryReport:setLocale value="<%=strLocale%>"/>
<fmtTerritoryReport:setBundle basename="properties.labels.sales.GmTerritoryReport"/>
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);



	String strDeptId = (String)session.getAttribute("strSessDeptId")==null?"":(String)session.getAttribute("strSessDeptId");
	String strAccessLvl = (String)session.getAttribute("strSessAccLvl")==null?"":(String)session.getAttribute("strSessAccLvl");

	int intAccessLvl = Integer.parseInt(strAccessLvl);
	boolean bolAccess = false;

	if ((strDeptId.equals("O") && intAccessLvl > 3))
	{
		bolAccess = true;
	}


	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	
	HashMap hmSales = new HashMap();

	ArrayList alTerritory = new ArrayList();
	ArrayList alMonthSales = new ArrayList();
	ArrayList alDate = new ArrayList();

	HashMap hmTempLoop = new HashMap();
	HashMap hmLoop = new HashMap();
	String strId = "";
	String strName = "";
	String strCount = "";
	String strSum = "";

	String strDistName = "";
	String strDaySales = "";
	String strMonthSales = "";	

    if (hmReturn != null)
    {								
		alTerritory = (ArrayList)hmReturn.get("REPORT");
	}
	
	String strShade = "";
	String strLine = "";
	String strRAId = "";
	String strReturnType = "";
	String strReturnReason = "";
	String strExpectedDate = "";
	String strPerson = "";
	String strComments = "";
	String strDateInitiated = "";
	String strAccName = "";
	
	int intLength = 0;
	String strNextId = "";
	String strDistId = "";
	String strPrice = "";
	double dbSales = 0.0;
	double dbAcctTotal = 0.0;
	double dbTotal = 0.0;
	int intCount = 0;
	boolean blFlag = false;
	String strAcctTotal = "";
	ArrayList alDist = new ArrayList();
	ArrayList alSales = new ArrayList();
	String strTotal = "";
	GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.sales.GmTerritoryReport", strSessCompanyLocale);
	String strGraphTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_TODAY_SALES"));
	
	String strDate = "";
	String strDay = "";
	String strSales = "";
	String strAvgDaily = "";
	double dbAvgDaily = 0.0;
	double dbTempTotal = 0.0;

	StringBuffer sbPcScript = new StringBuffer();
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Accounts Dashboard </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script>


function Toggle(val)
{
	
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className="";
		tabobj.style.background="#ffffff";
	}
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmAcctFrameServlet">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hPgToLoad" value="GmEditOrderServlet">
<input type="hidden" name="hSubMenu" value="Trans">
<input type="hidden" name="hMode" value="">
<input type="hidden" name="hFrom" value="Dashboard">
<input type="hidden" name="hPO" value="">

	<table border="0" width="550" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="6" width="1" class="Line"></td>
			<td bgcolor="black"><img src="<%=strImagePath%>/spacer.gif" height="1"></td>
			<td rowspan="6" width="1" class="Line"></td>
		</tr>
		<tr><td height="25" class="RightDashBoardHeader"><fmtTerritoryReport:message key="LBL_TERRITORY"/></td></tr>
		<tr><td class="Line"></td></tr>		
		<tr>
			<td valign="top" align="center">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="20" width="200"><fmtTerritoryReport:message key="LBL_DISTRIBUTOR"/></td>
						<td width="450">Territory</td>
						<td width="150">&nbsp;&nbsp;&nbsp;<fmtTerritoryReport:message key="LBL_QUOTA"/></td>
					</tr>	
<%
						intLength = alTerritory.size();
						if (intLength > 0)
						{
							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alTerritory.get(i);
								if (i<intLength-1)
								{
									hmTempLoop = (HashMap)alTerritory.get(i+1);
									strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("DID"));
								}

								strDistId = GmCommonClass.parseNull((String)hmLoop.get("DID"));
								strDistName = GmCommonClass.parseNull((String)hmLoop.get("DNAME"));
								strId	= GmCommonClass.parseNull((String)hmLoop.get("TID"));
								strName = GmCommonClass.parseNull((String)hmLoop.get("TNAME"));
								strPrice = GmCommonClass.parseZero((String)hmLoop.get("TQUOTA"));

								dbSales = Double.parseDouble(strPrice);
								dbAcctTotal = dbAcctTotal + dbSales;
								dbTotal = dbTotal + dbSales;
								if (strDistId.equals(strNextId))
								{
									intCount++;
									strLine = "<TR><TD colspan=3 height=1 class=Line></TD></TR>";
								}
								else
								{
									strLine = "<TR><TD colspan=3 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
										strDistName = "";
										strLine = "";
									}
									else
									{
										blFlag = true;
									}
									intCount = 0;
									
								}
								strAcctTotal = ""+dbAcctTotal;

								if (intCount > 1)
								{
									strDistName = "";
									strLine = "";
								}
							
								strShade = (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows

								out.print(strLine);
								strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
								if (intCount == 1 || blFlag)
								{
																		alDist.add(strDistName);
%>
						<tr id="tr<%=strDistId%>">
							<td colspan="2" height="19" class="RightText">&nbsp;<A class="RightText" title="Click to Expand" href="javascript:Toggle('<%=strDistId%>')"><%=strDistName%></a></td>
							<td class="RightText" id="td<%=strDistId%>" align="right"></td>
						</tr>
						<tr>
							<td colspan="3"><div style="display:none" id="div<%=strDistId%>">
								<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tab<%=strDistId%>">
<%
								blFlag = false;
								}
%>
						<tr <%=strShade%>>
							<td width="200" class="RightText" height="18">&nbsp;</td>
							<td width="450" class="RightText">&nbsp;<%=strName%></td>
							<td width="150" class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strPrice)%>&nbsp;</td>
						</tr>
						<tr><td colspan="3" bgcolor="#cccccc"></td></tr>
<%					
								if ( intCount == 0 || i == intLength-1)
								{
									alSales.add(GmCommonClass.getStringWithCommas(strAcctTotal));
%>
								</table></div>
							</td>
						</tr>
					<script>
						document.all.td<%=strDistId%>.innerHTML = "$<%=GmCommonClass.getStringWithCommas(strAcctTotal)%>&nbsp;";
					</script>
<%
									dbAcctTotal = 0.0;					
								}
							strTotal = ""+dbTotal;
							}// end of FOR
%>
						<tr><td colspan="3" class="Line"></td></tr>
						<tr>
							<td align="right"  class="RightTableCaption" height="20" colspan="2"><fmtTerritoryReport:message key="LBL_GRAND_TOTAL"/>&nbsp;</td>
							<td class="RightTableCaption" align="right">$<%=GmCommonClass.getStringWithCommas(strTotal)%>&nbsp;</td>
						</tr>
						<tr><td colspan="3" class="Line"></td></tr>
<%
						} // End of IF
						else{
%>
					<tr><td class="Line"></td></tr>					
					<tr><td class="RightTextRed" height="50" align=center><fmtTerritoryReport:message key="LBL_NO_SALES"/> ! </td></tr>
<%					
						}
%>
				</table>
			</td>
		</tr>
		<tr>
			<td height="1" bgcolor="black"></td>
		</tr>
	</table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
