 <%
/**********************************************************************************
 * File		 		: GmVanGuardList.jsp
 * Desc		 		: This screen is used for the Setting primary and secondary project list
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- sales\GmVanGuardList.jsp -->
<%@ taglib prefix="fmtVanGuardList" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtVanGuardList:setLocale value="<%=strLocale%>"/>
<fmtVanGuardList:setBundle basename="properties.labels.sales.GmVanGuardList"/>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%

try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	ArrayList alVGuardType = new ArrayList();
	alVGuardType = (ArrayList)request.getAttribute("alVGuardType");
	log.debug(" alVGuardType is " +alVGuardType);
	ArrayList alVanguardParameters = request.getAttribute("alVanguardParameters") == null?new ArrayList():(ArrayList)request.getAttribute("alVanguardParameters");
	String strTitle= "Set Group Mapping";
	String strOpt= request.getAttribute("strOpt") == null ?"":(String) request.getAttribute("strOpt");
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmLoad = new HashMap();

	String strParaID="";
	String strParaName="";
	String strParaValue="";
	String strCodeID = "";
	String strSelected = "";
		
	int intSize = 0;
	int intVGuardTypeSize = 0;
	int count=0;
	HashMap hcboVal = null;
	HashMap hcboValVGuard = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: VanGuard List </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script>

function fnSubmit(frm)
{
 Error_Clear();
 fnPopulateHiddenValues();
 frm.hVguardParameter.value =  getInputString(frm); 	
  //alert(frm.hVguardParameter.value);
  if (ErrorCount > 0)
	{
		Error_Show();
		str = '';
		return false;
	}else{
		fnStartProgress('Y');
	document.frmvanguard.submit();
	  	}
}

function fnPopulateHiddenValues()
{
	var theinputs=document.getElementsByTagName('select');
	var varcount = theinputs.length;
	var objhParamValue;
	var objcbo_vguardtype;
	
	for(i = 0; i < varcount; i++)
	{
		objhParamValue = eval("document.frmvanguard.hParamVale_man"+i+"_seq1");
		objcbo_vguardtype = eval("document.frmvanguard.Cbo_vguardtype"+i);
		objhParamValue.value = objcbo_vguardtype.value;
	}
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmvanguard" method="POST" action="<%=strServletPath%>/GmVanGuardSetupServlet">
<input type="hidden" name="hAction" value="Save">
<input type="hidden" name="hVguardParameter" value="" >
<input type="hidden" name="hCount" value="<%=count%>" >

	<table border="0" width="500" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="498" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td class="Line" colspan="2"></td></tr>
					<tr>
						<td colspan="2" height="25" class="RightDashBoardHeader">&nbsp;<%=strTitle%></td>
					</tr>
					<tr><td class="Line" colspan="2"></td></tr>
<%
		
		intSize = alVanguardParameters.size();
  		for(count=0;count<intSize;count++) 
  					{   
  					hcboVal = (HashMap)alVanguardParameters.get(count);
  					strParaID = GmCommonClass.parseNull((String)hcboVal.get("ID"));
					strParaName = GmCommonClass.parseNull((String)hcboVal.get("NAME"));
					strParaValue = (String)hcboVal.get("VALUE");
					strParaValue = (strParaValue == null)?"20161":strParaValue;
%>					
					
					<tr>
						<td class="RightText" align="right" width="300" HEIGHT="24"><%=strParaName%>:</td>
						<input type="hidden" name=<%="strSetID"+count+"_seq0"%> value="<%=strParaID%>">
						<td width="320">&nbsp;
						<input type="hidden" name=<%="hParamVale_man"+count+"_seq1"%> value="">						
							<select name="Cbo_vguardtype<%=count%>" class="RightText"  onFocus="changeBgColor(this,'#AACCE8');"  onBlur="changeBgColor(this,'#ffffff');">
														<%
																		  		intVGuardTypeSize = alVGuardType.size();
																		  		hcboValVGuard = new HashMap();
																		  		for (int i=0;i<intVGuardTypeSize;i++)
																		  		{
																		  			hcboValVGuard = (HashMap)alVGuardType.get(i);
																		  			strCodeID = (String)hcboValVGuard.get("CODEID");
																					strSelected = strParaValue.equals(strCodeID)?"selected":"";
															%>
																						<option <%=strSelected%> value=<%=strCodeID%> > <%=hcboValVGuard.get("CODENM")%></option>
															<%
																		  		}
															%>
							</select>							
						</td>
					</tr>
<%
  					}
%>					
				   <tr>
						<td colspan="2" align="center"  height="30">&nbsp;
						<fmtVanGuardList:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnSubmit(this.form);" />&nbsp;
						</td>
				  </tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
