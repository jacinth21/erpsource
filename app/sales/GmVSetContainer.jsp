<%
/**********************************************************************************
 * File		 		: GmVSetContainer.jsp
 * Desc		 		: This screen is used to display Loaner Usage details 
 * Version	 		: 1.0
 * author			: Joe
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtVSetContainer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- sales\GmVSetContainer.jsp -->
<fmtVSetContainer:setLocale value="<%=strLocale%>"/>
<fmtVSetContainer:setBundle basename="properties.labels.sales.GmVSetContainer"/>
<%
String strhAction = (String) request.getAttribute("hAction");
String strhSalesConsign = GmCommonClass.parseNull((String) request.getAttribute("strSalesConsign"));
log.debug(" strhSalesConsign ----------------  " + strhSalesConsign);

if (strhAction == null) {
	strhAction = (String) session.getAttribute("hAction");

	log.debug(" actions " + strhAction);
}
//Below code is to track down the visits on the report.
String strUserId = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
String strShUserName = GmCommonClass.parseNull((String)session.getAttribute("strSessShName"));
String strUserName = strUserId + " - " + strShUserName;
//Visit code ends

%>
<HTML>
<HEAD>
<TITLE> New Document </TITLE>
<META NAME="Generator" CONTENT="EditPlus">
<META NAME="Author" CONTENT="">
<META NAME="Keywords" CONTENT="">
<META NAME="Description" CONTENT="">
</HEAD>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />

<script type="text/javascript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script> downloadPiwikJS();</script>
<script> fnTracker('Sales Rep Performance', '<%=strUserName%>');</script>
<script language="javascript" type="text/javascript">
//Function will call on page load. It will append companyInfo in each tab href and initiate ajaxtab.
function fnAjaxTabLoad(){
	fnAjaxTabAppendCmpInfo("#constabs");
	fnMainTab();
}

//initiate ajaxtab
function fnMainTab(){
	var rules=new ddajaxtabs("constabs", "consdivcontainer");
	rules.setpersist(false);
	rules.init();			
}
</script>
<BODY leftmargin="20" topmargin="10" onload="fnAjaxTabLoad();" >
<TABLE cellSpacing=0 cellPadding=0 border=0 width="1000" class="border" >
<tr class=Line>
			<td height=25 class=RightDashBoardHeader>
			<table width="100%">
				<tr>
					<td class=RightDashBoardHeader><fmtVSetContainer:message key="LBL_FEILD_INV"/></td>
					<td align="right" class=RightDashBoardHeader>
					<fmtVSetContainer:message key="IMG_ALT_HELP" var="varHelp"/>
<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
    height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("FIELD_INV_REPORT")%>');"/>
</td>
						</tr>
			</table>
			</td>
		</tr>
		<tr >
			<td noWrap height=2></td>
		</tr>
<tr><td>
<ul id="constabs" class="shadetabs">
<li><a href="/GmSalesConsignmentServlet?hAction=<%=strhAction%>&Cbo_SalesType=50300&hPage=SalesConsignment&Cbo_TypeCon=<%=GmCommonClass.parseNull((String) request.getAttribute("Cbo_TypeCon"))%>" rel="#iframe"><fmtVSetContainer:message key="LBL_CONSIGNMENT"/></a></li>
<li><a href="/GmSalesConsignmentServlet?hAction=<%=strhAction%>&Cbo_SalesType=50301&hPage=Loaner&Cbo_LoanerType=70131&Cbo_TypeCon=<%=GmCommonClass.parseNull((String) request.getAttribute("Cbo_TypeCon"))%>" rel="#iframe"><fmtVSetContainer:message key="LBL_LOANER"/></a></li>
<li><a href="/GmSalesConsignmentServlet?hAction=<%=strhAction%>&Cbo_SalesType=50302&hPage=Loaner&Cbo_TypeCon=<%=GmCommonClass.parseNull((String) request.getAttribute("Cbo_TypeCon"))%>" rel="#iframe"><fmtVSetContainer:message key="LBL_ALL"/></a></li>
</ul><div id="consdivcontainer" style="width:100%;height:630"></div>
</td> </tr>
</TABLE>
</BODY>

</HTML>