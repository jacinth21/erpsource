<!-- \sales\DashBoard\GmSalesDashDtlPopup.jsp -->
 <%
/*****************************************************************************************************************************
 * File		 		: GmSalesDashDtlPopup.jsp
 * Desc		 		: This screen is used to show the Detailed information after Click on the count value in Sales Dash Board.
 * Version	 		: 1.0
 * author			: Gopinathan
******************************************************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtSalesDashDtlPopup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtSalesDashDtlPopup:setLocale value="<%=strLocale%>"/>
<fmtSalesDashDtlPopup:setBundle basename="properties.labels.sales.Dashboard.GmSalesDashDtlPopup"/>

<%
String strSalesDashBoardJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_DASHBOARD");
%>
<html>
<head>

		<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxlayout.css">
        <link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/skins/dhtmlxlayout_dhx_skyblue.css">
        <script src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
        <script src="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxcontainer.js"></script>
        <script src="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxlayout.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css"></link>
        <link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css"></link>
		<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
		<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
        <script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js"></script>
        <script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js"></script>
        <script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"></script>
        <script src="<%=strSalesDashBoardJsPath%>/GmSalesDashboard.js"></script>

<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<TITLE> Globus Medical:&nbsp;</TITLE>
<%
String strIncludeURL = request.getQueryString();
String strTableClass = ""; 
 if(strIncludeURL.indexOf("GmEditOrderServlet")!=-1){
	 strTableClass = "DtTable700";
 }else if(strIncludeURL.indexOf("GmPrintCreditServle")!=-1){
	 strTableClass = "DtTable500";
 }else{
	 strTableClass = "DtTable800";
 }
%>
<script>
function fnClosePopup()
{
	parent.dhxWins.window("w1").close();
}

</script>
</head>

<body leftmargin="10" topmargin="10" style="overflow: auto">
	<div style="width: 100%; height: 100%; overflow:auto" align="center">
		<table cellspacing="0" cellpadding="0" class="<%=strTableClass%>" >
			<tr>
				<td ><!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					<jsp:include page="<%=strIncludeURL%>"/>
				</td>
			</tr>
			<tr>
				<td align="center" height="40"><fmtSalesDashDtlPopup:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="&nbsp;${varClose}&nbsp;" buttonType="Load" onClick="javascript:fnClosePopup();"></gmjsp:button></td>
			</tr>
		</table>
	</div>	
</body>
</html>
<%@ include file="/common/GmFooter.inc" %>

