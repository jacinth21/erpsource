<!-- \sales\DashBoard\GmSalesDashLoanerExt.jsp -->
<%
	/*****************************************************************************************************************
	 * File		 		: GmSalesDashLoanerExt.jsp
	 * Desc		 		: This screen is used to providew the option to Sales Dept to extend the Loaner Extn Reqiuest
	 * Version	 		: 1.0
	 * author			: Harinadh Reddi
	 *****************************************************************************************************************/
%>
	<%@page import="com.globus.common.beans.GmCalenderOperations"%>
	<%@ include file="/common/GmHeader.inc" %>
	<%@ page language="java" %>
	<%@ page import ="com.globus.common.beans.GmCommonClass"%>
	<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
	<%@ page import ="org.apache.commons.beanutils.PropertyUtils"%>
	<%@ taglib prefix="fmtSalesDashLoanerExt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtSalesDashLoanerExt:setLocale value="<%=strLocale%>"/>
<fmtSalesDashLoanerExt:setBundle basename="properties.labels.sales.Dashboard.GmSalesDashLoanerExt"/>
	
	<bean:define id="xmlGridData" name="frmSalesDashLoanerExt" property="xmlGridData" type="java.lang.String"></bean:define>
	<bean:define id="alNewExtnDateValues" name="frmSalesDashLoanerExt" property="alNewExtnDateValues" type="java.util.ArrayList"></bean:define>
	
<HTML>
<HEAD>	
	<% 
	String strSalesDashBoardJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_DASHBOARD");
		String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
		HashMap hcboVal = null;
		String strTodaysDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt);
		String strCompDateFmt = strGCompDateFmt;
	%>
	<TITLE> Globus Medical: Loaner Extension Request</TITLE>
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
	<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
	<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/skins/dhtmlxcalendar_yahoolike.css">
	<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strLocale%>.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
	<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
	<script language="JavaScript" src="<%=strSalesDashBoardJsPath%>/GmSalesDashLoanerExtnDetails.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_dhxcalendar.js"></script>

	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
	<script>
		var objGridData = '';
		objGridData = '<%=xmlGridData%>';
		var gObj='';
		var format = '<%=strApplDateFmt%>';
		var newExtnDateSize = <%=alNewExtnDateValues.size()%>;
		var today_dt = '<%=strTodaysDate%>';
		var dateFormat = '<%=strCompDateFmt%>';
		<% hcboVal = new HashMap();
		for (int i=0;i<alNewExtnDateValues.size();i++){
			hcboVal = (HashMap)alNewExtnDateValues.get(i); %>
		var newExtnDate<%=i%> ="<%=hcboVal.get("REQDTLID")%>,<%=hcboVal.get("DAYEXTEND")%>,<%=hcboVal.get("NEWEXTNDATE")%>";
		<% } %>
		
	</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
	<html:form action="/gmSalesDashLoanerExt.do" >
	<html:hidden property="strOpt"/>
	<html:hidden property="inputString"/>
	<html:hidden property="consignId"/>
	<html:hidden property="reqiestDtlID"/>

	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable800"  cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtSalesDashLoanerExt:message key="LBL_PROCESS_LOANER_EXTENSION"/></td>
			<td align="right" class=RightDashBoardHeader>
				<fmtSalesDashLoanerExt:message key="IMG_HELP" var="varHelp"/><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${Help}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("LOANER_EXTN_REQUEST")%>');" />
			</td>
		</tr>
		<tr><td colspan="2" class="Line" height="1"></td></tr>
		<%if(xmlGridData.indexOf("cell") != -1){%>
		<tr>
			<td  colspan="6" width="100%">
				<div id="dataGridDiv"  height="300px" width="800px"></div>
			</td>
		</tr>
		<%}else if(!xmlGridData.equals("")){%>
			<tr><td colspan="5" align="center" height="30" class="RightText"><fmtSalesDashLoanerExt:message key="MSG_NO_DATA"/></td></tr>
		<%}else{%>
			<tr><td colspan="5" align="center" height="30" class="RightText"><fmtSalesDashLoanerExt:message key="NOTHING_FOUND_TO_DISPLAY"/></td></tr>
		<%}%>
		<tr>
		<td colspan="5">
			<jsp:include page="/common/GmIncludeLog.jsp" >
			                    	<jsp:param name="FORMNAME" value="frmSalesDashLoanerExt" />
									<jsp:param name="ALNAME" value="alLogReasons" />
									<jsp:param name="LogMode" value="Edit" />
									<jsp:param name="Mandatory" value="yes" />
								</jsp:include>
								</td>
		</tr>
		<%if(xmlGridData.indexOf("cell") != -1){%>
		<tr>
			<tr><td colspan="2" class="Line" height="1"></td></tr>
			<tr>
				<td align="center" colspan="4" height="35">				
					 <fmtSalesDashLoanerExt:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" name="Btn_Submit" buttonType="Save" gmClass="button" onClick="javascript:fnExtend();" />&nbsp;&nbsp;
					 <fmtSalesDashLoanerExt:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="${varClose}" name="Btn_Close" buttonType="Save" gmClass="button" onClick="javascript:fnClose();" />&nbsp;&nbsp;
				</td>
			</tr>
		<%}%>			
	</table>
</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>