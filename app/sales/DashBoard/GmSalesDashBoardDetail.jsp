<!-- \sales\DashBoard\GmSalesDashBoardDetail.jsp -->
 <%
/*****************************************************************************************************************************
 * File		 		: GmSalesDashBoardDetail.jsp
 * Desc		 		: This screen is used to show the Detailed information after Click on the count value in Sales Dash Board.
 * Version	 		: 1.0
 * author			: Gopinathan
******************************************************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtSalesDashBoardDetail" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtSalesDashBoardDetail:setLocale value="<%=strLocale%>"/>
<fmtSalesDashBoardDetail:setBundle basename="properties.labels.sales.Dashboard.GmSalesDashBoardDetail"/>

<bean:define id="gridData" name="frmGmSalesDash" property="strXMLGrid" type="java.lang.String"> </bean:define>
<bean:define id="strOpt" name="frmGmSalesDash" property="strOpt" type="java.lang.String"> </bean:define>
<bean:define id="passName" name="frmGmSalesDash" property="passName" type="java.lang.String"> </bean:define>
<bean:define id="transType" name="frmGmSalesDash" property="transType" type="java.lang.String"> </bean:define>
<bean:define id="strAccessFlag" name="frmGmSalesDash" property="accessFlag" type="java.lang.String"> </bean:define>
<%
String strSalesDashBoardJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_DASHBOARD");
String  strApplnCurrSign  = GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb"));
%>
<html>
<head>

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css"></link>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css"> 

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script src="<%=strSalesDashBoardJsPath%>/GmSalesDashboardDetail.js"></script>

<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<TITLE> Globus Medical:&nbsp;</TITLE>
<script>
var objGridData;
objGridData ='<%=gridData%>';
var strOpt ='<%=strOpt%>';
var currSign = '<%=strApplnCurrSign%>'; 
var passname = '<%=passName%>';
</script>
</head>

<body onload="javascript:fnOnPageLoad();" leftmargin="20" topmargin="10" style="overflow: auto">
<input type="hidden" name="passName" />
	<div style="width: 100%; height: 100%; overflow:auto">
		<table cellspacing="0" cellpadding="0" class="DtTable800">
			<tr>
				<td id="gridData">
					<div id="GridLayout" style="position: relative; height:<%=strOpt.equals("todaysales")?"250px":"365px"%>; width:<%=strOpt.equals("todaysales")?"865px":"920px"%>; aborder: #B5CDE4 1px solid;"></div>
				</td>
			</tr>
			<tr>
				<td>
					<div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div>
				</td>
			</tr>
		</table>
		<br>
		<table cellspacing="0" cellpadding="0">
		<tr id="DivExportExcel" height="25">
			<td colspan="6" align="center">
			    <div class='exportlinks'><fmtSalesDashBoardDetail:message key="EXPORT_OPTS"/> :<img
					src='img/ico_file_excel.png' onclick="fnExcelExport();" />&nbsp;<a href="#"
					onclick="fnExcelExport();"><fmtSalesDashBoardDetail:message key="EXCEL"/> 
				</a></div>
			</td>
		 </tr>
			<tr>
				<td align="center">				
					<fmtSalesDashBoardDetail:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="&nbsp;${varClose}&nbsp;" buttonType="Load" onClick="javascript:fnClose();"/>&nbsp;&nbsp;&nbsp;
					<%if (strAccessFlag.equals("Y") && ((passName.equals("Requested") || passName.equals("Approved") || passName.equals("Shipped") || passName.equals("DueToday") || passName.equals("Due1Day") || passName.equals("Due2Day") || passName.equals("OverDue")) && transType.equals("Loaners"))){ %>
						<fmtSalesDashBoardDetail:message key="LBL_EXTEND" var="varExtend"/><gmjsp:button value="&nbsp;${varExtend}&nbsp;" name="Btn_Extend" buttonType="Save" onClick="javascript:fnExtend();"/>&nbsp;&nbsp;
					<%} %>	
				</td>	
			</tr>
		</table>
	</div>	
</body>
</html>
<%@ include file="/common/GmFooter.inc" %>

