<!--\sales\DashBoard\GmSalesDashBoard.jsp  -->
<%
/**********************************************************************************
 * File		 		: GmSalesDashBoard.jsp
 * Desc		 		: This screen is used for the Sales DashBoard
 * Version	 		: 1.0
 * author			: Gopinathan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtSalesDashBoard" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtSalesDashBoard:setLocale value="<%=strLocale%>"/>
<fmtSalesDashBoard:setBundle basename="properties.labels.sales.Dashboard.GmSalesDashBoard"/>


<%
String strSalesDashBoardJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_DASHBOARD");
String strWikiTitle = GmCommonClass.getWikiTitle("SALES_DASHBOARD");
String strUserId = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
String strShUserName = GmCommonClass.parseNull((String)session.getAttribute("strSessShName"));
String strSessCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
String strUserName = strUserId + " - " + strShUserName;
%>

<HTML>
	<Head>
		<title> Sales Dashboard</title>
		<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxlayout.css">
        <link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/skins/dhtmlxlayout_dhx_skyblue.css">
        <script src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
        <script src="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxcontainer.js"></script>
        <script src="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxlayout.js"></script>
        <script src="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/patterns/dhtmlxlayout_pattern4w.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css"></link>
        <link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css"></link>
        <script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js"></script>
        <script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js"></script>
        <script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"></script>
		<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
		<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
        <script src="<%=strSalesDashBoardJsPath%>/GmSalesDashboard.js"></script>        	
		<script> downloadPiwikJS();</script>
		<script> fnTracker('Sales Dashboard', '<%=strUserName%>');</script>

        <style type="text/css" media="all">
		     @import url("<%=strCssPath%>/screen.css");
		</style>
        <style>
        	table.DtSalesTable955 {
			margin: 0 0 0 0;
			width: 955px;
			border: 1px solid  #676767;
			}
        </style>
	</Head>
    <bean:define id="strDaySales" name="frmGmSalesDash" property="todaySales" type="java.lang.String"></bean:define>
	<bean:define id="strMonthSales" name="frmGmSalesDash" property="monthlySales" type="java.lang.String"></bean:define>
	<bean:define id="strTodaySalesType" name="frmGmSalesDash" property="todaySalesType" type="java.lang.String"></bean:define>
	<bean:define id="strTodaySalesTab" name="frmGmSalesDash" property="todaySalesTab" type="java.lang.String"></bean:define>
	<bean:define id="strMonthSalesTab" name="frmGmSalesDash" property="monthSalesTab" type="java.lang.String"></bean:define>
	<bean:define id="accItemConRptFl" name="frmGmSalesDash" property="accItemConRptFl" type="java.lang.String"></bean:define>
	
	<script>
	var dhxLayout_1;
	var dhxLayout_2;
	var dhxLayout_3;
	var dhxWins,popupWindow,w1;
	var todaySalesSltVal = '<%=strTodaySalesType%>'; 
	var sessCurrSign = '<%=strSessCurrSign%>'; 
	var strAccItemConRptFl = '<%=accItemConRptFl%>';
	</script>
	
	<BODY leftmargin="20" topmargin="10" onload="doOnLoad();" style="overflow: auto">
		<% 
			if (!strClientSysType.equals("IPAD"))
			{
		%>
			<div style="width: 100%; height: 100%; overflow:auto">
		<% } else {%>	
		<div style="width: 100%; overflow:auto">
		<% } %>
			<FORM name="frmGmSalesDash" method="POST" action="/gmSalesDashLoaner.do?method=loadSalesDashBoard">
			<input type="hidden" name="strDaySales" value="<%=strDaySales%>">
			<input type="hidden" name="strMonthSales" value="<%=strMonthSales%>">
			<input type="hidden" name="todaySalesTab" value="<%=strTodaySalesTab%>">
			<input type="hidden" name="monthSalesTab" value="<%=strMonthSalesTab%>">				
			<table border="0" width="950px" height="730px" class="DtSalesTable955" cellspacing="0" cellpadding="0">
				<div id="userType" style="display:none" ><gmjsp:dropdown controlName="todaySalesType" SFFormName="frmGmSalesDash" SFSeletedValue="todaySalesType" SFValue="alUserTyp" codeId="CODENMALT" codeName="CODENM" defaultValue="[Choose One]" onChange="fnFetchTodaySales(this.value);" /></div>
				<tr>
					<td height="25" class="RightDashBoardHeader"><fmtSalesDashBoard:message key="LBL_SALES_DASHBOARD"/></td>
					<td  height="25" class="RightDashBoardHeader">
						<fmtSalesDashBoard:message key="IMG_ALT_HELP" var="varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>			
				</tr>
				<tr>
					
					<td width="950px" colspan="4">
						<jsp:include page="/sales/GmSalesFilters.jsp" >
							<jsp:param name="HIDE" value="SYSTEM" />
							<jsp:param name="FRMNAME" value="frmGmSalesDash" />
							<jsp:param name="URL" value="/gmSalesDashLoaner.do?method=loadSalesDashBoard" />
						</jsp:include> 
					</td>
				</tr>
				<tr>
					<td width="950px" colspan="4">
						<div id="salesDash_1" style="position: relative; height: 202px; aborder: #B5CDE4 1px solid; margin: 0px;0px;0px;0px;"></div>	
						<div id="salesDash_2" style="position: relative; height: 202px; aborder: #B5CDE4 1px solid; margin: 2px;0px;0px;0px;"></div>
						<div id="salesDash_3" style="position: relative; height: 321px; aborder: #B5CDE4 1px solid; margin: 2px;0px;0px;0px;"></div>
					</td>
				</tr>	
			</table>
			
			</FORM>
		</div>	
	</BODY>
</HTML>
<%@ include file="/common/GmFooter.inc" %>
