<!--\sales\Event\GmEventShippingDetails.jsp  -->
<%
	/**********************************************************************************
	 * File		 		: GmCaseBookSetup.jsp
	 * Desc		 		: This screen is used for Case Book Setup
	 * Version	 		: 1.0
	 * author			: Xun
	 * 
	 ************************************************************************************/
%> 
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtEventShippingDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<fmtEventShippingDetails:setLocale value="<%=strLocale%>"/>
<fmtEventShippingDetails:setBundle basename="properties.labels.sales.Event.GmEventShippingDetails"/>

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%> 
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>
<HTML>
<HEAD>
<%
String strsalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");
String strWikiTitle = GmCommonClass.getWikiTitle("EVENT_SHIPPING_INFO");
String strSessApplDateFmt =  strGCompDateFmt;
GmCalenderOperations.setTimeZone(strGCompTimeZone);
String currentdate = GmCalenderOperations.getCurrentDate(strSessApplDateFmt);
//PC-5609 - company information not passing in edge browser after submit or redirect the page, so get company info and form companyInfo JSON string in JSP ANd set into form variable
String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\",\"cmplangid\":\""+gmDataStoreVO.getCmplangid()+"\",\"cmpdfmt\":\""+strSessApplDateFmt+"\"}";

%>
<TITLE> Globus Medical: Case Book </TITLE> 
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strsalesJsPath%>/event/GmEventShippingDetails.js"></script>

<script language="JavaScript"> 
var format = '<%=strSessApplDateFmt%>';
var lblNames = '<fmtEventShippingDetails:message key="LBL_EVENT_NAMES"/>'; 
</script>
</HEAD>

<!-- To show the selected address in Address field, need to pass the selected address id to the function --> 
<BODY leftmargin="20" topmargin="10" onload="fnGetAddressList(document.all.names,'<bean:write name="frmShipDetails" property="addressid"/>','Y');">
<html:form action="/gmCaseBookSetDtls.do">
<input type="hidden" name="RE_FORWARD" value="gmEventShipAction">
<html:hidden property="strOpt"  /><html:hidden property="haction"  />
<input type="hidden" name="currentdate" value="<%=currentdate %>"/>
<html:hidden property="caseInfoID"  />
<html:hidden property="caseID"  />
<html:hidden property="categoryList"/>
<html:hidden property="favouriteCaseID"   />
<html:hidden property="hinputStr"   />
<html:hidden property="fromscreen"   /> 
<html:hidden property="eventStartDate"   />
<html:hidden property="eventEndDate"   />
<html:hidden property="backScreen" />
<html:hidden property="companyInfo" value="<%=strCompanyInfo%>"/>  <!--  PC-5609 - set the companyInfo in hidden variable  -->
<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">	
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="3" >&nbsp;<fmtEventShippingDetails:message key="LBL_EVENT_BOOKING_SHIPPING_DETAILS"/></td>		
			<td class="RightDashBoardHeader" colspan="1" ><fmtEventShippingDetails:message key="IMG_HELP" var="varHelp"/>
			<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
		</tr>
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		<tr class="ShadeRightTableCaption">
			<td height="25"  colspan="4"><fmtEventShippingDetails:message key="LBL_EVENT_INFORMATION"/></td>			
		</tr>			
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		<tr class="evenshade">
			<td class="RightTableCaption" align="right" HEIGHT="25" width="190"><fmtEventShippingDetails:message key="LBL_EVENT_NAME"/></td>
			<td width="145" align="left">&nbsp;<bean:write name="frmCaseBookSetDtls" property="eventName"/></td>			
			<td class="RightTableCaption" align="right" HEIGHT="25" width="220">&nbsp;<fmtEventShippingDetails:message key="LBL_PURPOSE_OF_EVENT"/></td>
            <td width="230" align="left">&nbsp;<bean:write name="frmCaseBookSetDtls" property="purpose"/></td>
        </tr>
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		<tr class="oddshade">
			<td class="RightTableCaption" align="right" HEIGHT="25" width="190">&nbsp;<fmtEventShippingDetails:message key="LBL_START_DATE"/></td>
			<td width="145" align="left">&nbsp;<bean:write name="frmCaseBookSetDtls" property="eventStartDate"/>
            <td class="RightTableCaption" align="right" HEIGHT="25" width="220">&nbsp;<fmtEventShippingDetails:message key="LBL_END_DATE"/></td>
            <td width="230" align="left">&nbsp;<bean:write name="frmCaseBookSetDtls" property="eventEndDate"/></td>
        </tr>		
		<tr><td colspan="4" class="LLine" height="1"></td></tr>		
		<tr class="evenshade">
			<td class="RightTableCaption" align="right" HEIGHT="25" width="190"><font color="red">*</font>&nbsp;<fmtEventShippingDetails:message key="LBL_PLANNED_SHIP_DATE"/></td>
			<td align="left" width="145">&nbsp;<gmjsp:calendar  SFFormName="frmCaseBookSetDtls" controlName="plannedShipDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" /></td>
            <td class="RightTableCaption" align="right" HEIGHT="25" width="220"><font color="red">*</font>&nbsp;<fmtEventShippingDetails:message key="LBL_EXPECTED_RETURN_DATE"/></td>
		    <td width="230" align="left">&nbsp;<gmjsp:calendar  SFFormName="frmCaseBookSetDtls" controlName="expReturnDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" /></td>		
		</tr>
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		<tr class="oddshade">
			<td class="RightTableCaption" align="right" HEIGHT="25" width="190">&nbsp;<fmtEventShippingDetails:message key="LBL_STATUS"/>:</td>
			<td align="left" width="145">&nbsp;<bean:write name="frmCaseBookSetDtls" property="status"/></td>
            <td class="RightTableCaption" align="right" HEIGHT="25" width="220">&nbsp;</td>
		    <td width="250" align="left">&nbsp;</td>		
        </tr>	
		<tr><td colspan="4" class="LLine" height="1"></td></tr>		
		<tr>
			<td colspan="4">
				<jsp:include page="/operations/shipping/GmIncShipDetails.jsp">	
						<jsp:param name="hType" value="InhouseLoaner" />
					</jsp:include>			
			</td>
		</tr>
		<logic:notEqual name="frmCaseBookSetDtls" property="eventEditSts" value="N" >
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		<tr>
			<td height="25"  colspan="4"><font color="red"><fmtEventShippingDetails:message key="LBL_PLANNED_SHIP"/> </font></td>			
		</tr>
		</logic:notEqual>	
		<tr><td colspan="4" class="LLine" height="1"></td></tr>		
		<tr valign="middle" height="35">
			<td  colspan="1" ></td>
			<td  colspan="2" align="center" >&nbsp;
			<fmtEventShippingDetails:message key="BTN_BACK" var="varBack"/><gmjsp:button value="&nbsp;${varBack}&nbsp;" onClick="javascript:fnBack();" buttonType="Load" />&nbsp;
			<fmtEventShippingDetails:message key="BTN_NEXT" var="varNext"/><gmjsp:button value="&nbsp;${varNext}&nbsp;" onClick="fnNext();" buttonType="Load" /></td>
			<td  colspan="1" ></td>
		</tr>	
	</table>
</html:form>
</BODY>
 <%@ include file="/common/GmFooter.inc" %> 
</HTML>
 

