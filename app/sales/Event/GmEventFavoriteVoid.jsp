<!-- sales\Event\GmEventFavoriteVoid.jsp -->
<%
   /**********************************************************************************
	 * File		 		: GmEventFavoriteVoid.jsp
	 * Desc		 		: This screen is used for Voiding the Favorite
	 * Version	 		: 1.0
	 * author			: Hreddi 
   ************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtEventFavoriteVoid" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<fmtEventFavoriteVoid:setLocale value="<%=strLocale%>"/>
<fmtEventFavoriteVoid:setBundle basename="properties.labels.sales.Event.GmEventFavoriteVoid"/>

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%> 
<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");
	String strWikiTitle = GmCommonClass.getWikiTitle("FAVORITE_VOID");
	String strsalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Case Book </TITLE> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strsalesJsPath%>/event/GmEventFavoriteVoid.js"></script>
<script>
var lblFavoriteList = <fmtEventFavoriteVoid:message key="LBL_FAVOURITE_LIST"/>
</script>
</HEAD>


 
<BODY leftmargin="20" topmargin="10">
<html:form action="/gmEventSetup.do">
<html:hidden property="strOpt" />
<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">	
		<tr>
		   <td colspan="2">
		   	<table  border="0" cellspacing="0" cellpadding="0"> 
		   	<tr>
		    	<td height="25" width="75%" class="RightDashBoardHeader" colspan="1">&nbsp;<fmtEventFavoriteVoid:message key="LBL_VOID_FAVOURITE"/></td>
			    <td class="RightDashBoardHeader" width="25%" colspan="1">
			    	<fmtEventFavoriteVoid:message key="varHelp" var="varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' 
			                        title='${Help}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			    </td>
	    	</tr> 
	        </table>
	       </td>
		</tr>			
		<tr><td colspan="4" class="LLine" height="1"></td></tr>				
		<tr>
			<td colspan="4">
				<table border="0" cellspacing="0" cellpadding="0" >
					<tr>
						<td class="RightTableCaption" align="Right" height="30" width="20%">&nbsp;<fmtEventFavoriteVoid:message key="LBL_FAVOURITE_LIST"/>:</td>		
						<!-- Custom tag lib code modified for JBOSS migration changes -->
						<td>&nbsp;<gmjsp:dropdown controlName="favouriteCaseID" SFFormName="frmEventSetup" SFSeletedValue="favouriteCaseID" 
										defaultValue= "[Choose from Favorites]"	SFValue="alFavoriteEventList" width="475" codeId="CODEID" codeName="CODENM"/></td>						
					</tr>
					<tr><td class="LLine" height="1" colspan="4"></td></tr>
					<tr>
						<td colspan="4" align="center" height="30"><fmtEventFavoriteVoid:message key="BTN_VOID" var="varVoid"/><gmjsp:button gmClass="button" name="Void" value="&nbsp;${varVoid}&nbsp;" buttonType="Save" onClick="fnVoidFavorite();" />&nbsp;</td>
					</tr>
				</table>
			</td> 						
		</tr>							
    </table>	
</html:form>   
</BODY>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
 