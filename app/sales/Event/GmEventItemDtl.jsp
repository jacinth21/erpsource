<!-- \sales\Event\GmEventItemDtl.jsp -->
<%
/**********************************************************************************
 * File		 		: GmEventItemDtl.jsp
 * Desc		 		: This jsp will add item to the event
 * Version	 		: 1.0
 * author			: Gopinathan Saravanan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtEventItemDtl" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<fmtEventItemDtl:setLocale value="<%=strLocale%>"/>
<fmtEventItemDtl:setBundle basename="properties.labels.sales.Event.GmEventItemDtl"/>

<HTML>
<bean:define id="gridData" name="frmEventItemDtl" property="gridData" type="java.lang.String" />
<bean:define id="strFromScreen" name="frmEventItemDtl" property="fromscreen" type="java.lang.String" />

<%
String strsalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");
String header="";
String headerInfo = "";
String title ="";
String wikiTitle = "";
GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.sales.Event.GmEventItemDtl", strSessCompanyLocale);
if(strFromScreen.equals("eventsetup"))
{
	
	title = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_EVENT_BOOKING"));
	header= GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_ITEMS_SELECTION_MANDATORY"));
	headerInfo = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PROCEDURES"));
	wikiTitle = "EVENT_BOOK_RESOURCE_SELECTION";
}
else{
	title =" Case Booking - Step - Product Selection - Item";
	header=GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PRODUCT_SELECTION_MANDATORY"));
	headerInfo = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PROCEDURES"));
	wikiTitle ="CASE_BOOK_PRODUCT_SELECTION";
}
%>
<head>
<TITLE> Globus Medical: Event List Report  </TITLE>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strsalesJsPath%>/event/GmEventItemDtl.js"></script>
<script type="text/javascript">
var gridObj;
var objGridData = '<%=gridData%>';
</script>
</head>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<html:form action="/gmEventItemDtl.do">
<html:hidden property="strOpt"  />
<html:hidden property="partNumInputString" />
<html:hidden property="inputString" />
<html:hidden property="productionType" />
 <html:hidden property="caseInfoID"  />
 <html:hidden property="fromscreen" />
 <html:hidden property="categoryList"/>
 <html:hidden property="fwdProductType"/>
 <html:hidden property="backScreen"/>
<table class="DtTable700" border="0" cellspacing="0" cellpadding="0" >
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader">&nbsp;<%=title%></td>
			<td align="right" colspan="1" class=RightDashBoardHeader>
				<fmtEventItemDtl:message key="IMG_HELP" var ="varHelp"/>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle(wikiTitle)%>');" />
			</td> 
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#CCCCCC"></td></tr>
		<tr class="evenshade">
				<td  class="RightTableCaption" align="left" colspan="3"><font color="red"><%=header%></font></td>
		</tr>
		<tr class="evenshade">
				<td  class="RightTableCaption" align="left" colspan="3"><font color="red"><fmtEventItemDtl:message key="LBL_SELECT_ITEM"/><%=headerInfo%></font></td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#CCCCCC"></td></tr>
		<tr class="shade" height="30">
			<td width="15%" class="RightRedCaption" height="25" align="Right" >&nbsp;<fmtEventItemDtl:message key="LBL_PART_NUMBER"/>:</td>
			<td>
				&nbsp;<input type="text" size="50" value="" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
			</td>
			<td>
			<fmtEventItemDtl:message key="BTN_ADD_TO_CART" var="varAddtoCart"/><gmjsp:button value="${varAddtoCart}" onClick="fnAddtoCart();" buttonType="Load" />&nbsp;
			<fmtEventItemDtl:message key="BTN_PART_LOOKUP" var="varPartLookUp"/><gmjsp:button value="${varPartLookUp}" onClick="fnOpenPart();" buttonType="Load" />&nbsp;
			</td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#CCCCCC"></td></tr>
		<tr>
			<td colspan="3">
				<table>	
					<tr>
						<td class="RightTableCaption"><!-- Custom tag lib code modified for JBOSS migration changes -->							
							<fmtEventItemDtl:message key="LBL_DEL" var="varDel"/><gmjsp:label type="BoldText"  SFLblControlName="${varDel}" td="false"/>
							&nbsp;<img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="Remove Row" style="border: none;cursor: hand;" onClick="javascript:removeSelectedRow()" height="14">
						</td>
					</tr>
			</table>
		</td>
		</tr>
		<tr>
			<td colspan="3">
				<div id="dataGrid" style="height: 400px;width: 718px;"></div>
			</td>
		</tr>
				<tr><td colspan="3" height="1" bgcolor="#CCCCCC"></td></tr>
		<tr>

		</tr>
		
		<tr>
			<td  height="40" colspan="3" align="center">&nbsp;&nbsp;
				<fmtEventItemDtl:message key="BTN_BACK" var="varBack"/><gmjsp:button style="width: 7em" value="${varBack}"  onClick="fnBack();" buttonType="Load"/>
				<fmtEventItemDtl:message key="BTN_NEXT" var="varNext"/><gmjsp:button style="width: 7em" value="${varNext}"  onClick="javascript:fnSubmit();" buttonType="Load"/>
			</td>
		</tr>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>