<!-- \sales\Event\GmEventSummary.jsp -->
<%
	/**********************************************************************************
	 * File		 		: GmEventSummary.jsp
	 * Desc		 		: This screen is used for Event Summary Info
	 * author			: Elango
	 ************************************************************************************/
%> 
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtEventSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtEventSummary:setLocale value="<%=strLocale%>"/>
<fmtEventSummary:setBundle basename="properties.labels.sales.Event.GmEventSummary"/>

<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<bean:define id="hmEventHeaderInfo" name="frmEventRpt" property="hmEventHeaderInfo" type="java.util.HashMap"></bean:define>
<bean:define id="alSummaryAction" name="frmEventRpt" property="alSummaryAction" type="java.util.ArrayList"></bean:define>
<bean:define id="caseInfoID" name="frmEventRpt" property="caseInfoID" type="java.lang.String"></bean:define>
<bean:define id="strRequestID" name="frmEventRpt" property="strRequestID" type="java.lang.String"></bean:define>
<bean:define id="strStstusID" name="frmEventRpt" property="strStstusID" type="java.lang.String"></bean:define>
<bean:define id="eventRequestedMeterial" name="frmEventRpt" property="eventRequestedMeterial" type="java.lang.String"></bean:define>
<%
String strsalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");
	String strHtmlJasperRpt = "";
	//PC-5609 - company information not passing in edge browser after submit or redirect the page, so get company info and form companyInfo JSON string in JSP ANd set into form variable
	String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\",\"cmplangid\":\""+gmDataStoreVO.getCmplangid()+"\",\"cmpdfmt\":\""+strGCompDateFmt+"\"}";
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Event Summary Information</TITLE> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strsalesJsPath%>/event/GmEventSummary.js"></script>
<script>
var strCaseInfoID = '<%=caseInfoID%>';
var strRequestID ='<%=strRequestID%>';
var selectedCategories = '<%=eventRequestedMeterial%>';

</script>
</HEAD>
<BODY leftmargin="8">

<html:form  action="/gmEventReport.do?method=downloadSummaryAsPDF" >
<html:hidden name="frmEventRpt" property="strOpt"  />
<html:hidden  name="frmEventRpt" property="caseInfoID"  />
<html:hidden  name="frmEventRpt" property="eventName"  />
<html:hidden  name="frmEventRpt" property="strStstusID"  />
<html:hidden  name="frmEventRpt" property="eventID"  />
<html:hidden  name="frmEventRpt" property="accessFl"  />
<html:hidden property="companyInfo" value="<%=strCompanyInfo%>"/> <!--  PC-5609 - set the companyInfo in hidden variable  -->
<table  cellspacing="0" cellpadding="0" >
	   <tr >	
			<td >
		         <%
		        String strJasperName 	= "/GmEventSummary.jasper";
				GmJasperReport gmJasperReport = new GmJasperReport();			
				gmJasperReport.setRequest(request);
				gmJasperReport.setResponse(response);
				gmJasperReport.setJasperReportName(strJasperName);
				gmJasperReport.setHmReportParameters(hmEventHeaderInfo);
				gmJasperReport.setReportDataList(null);
				gmJasperReport.setPageHeight(200);
				gmJasperReport.setBlDisplayImage(true);
				strHtmlJasperRpt = gmJasperReport.getHtmlReport();
		      %>
		      <%=strHtmlJasperRpt %>		
			</td>		
		</tr>
		<tr><td>&nbsp;</td></tr>	
		<tr >	<!-- Custom tag lib code modified for JBOSS migration changes -->
		     <td align ="center" class="RightTableCaption">&nbsp;&nbsp; <fmtEventSummary:message key="LBL_CHOOSE_ACTION"/>:&nbsp; 
				<gmjsp:dropdown	controlName="dnEventAction" SFFormName="frmEventRpt" SFSeletedValue="dnEventAction" SFValue="alSummaryAction" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" />&nbsp;&nbsp;
		        <fmtEventSummary:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="Submit"  value="&nbsp;&nbsp;${varSubmit}&nbsp;&nbsp;" onClick="fnSubmit();" buttonType="Save"/>&nbsp;&nbsp;
		     </td>
	    </tr>
	    <tr><td>&nbsp;</td></tr>	   
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>