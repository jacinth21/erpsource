

<%

/**********************************************************************************
 * File		 		: GmEventCalendar.jsp
 * Desc		 		: This jsp is used to give the Overview of an Event at one shot per a day/week/month
 * Version	 		: 1.0
 * author			: Harinadh Reddy
************************************************************************************/
%>
<!-- \sales\Event\GmEventCalendar.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtEventCalendar" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<fmtEventCalendar:setLocale value="<%=strLocale%>"/>
<fmtEventCalendar:setBundle basename="properties.labels.sales.Event.GmEventCalendar"/>

<%@ page import="com.globus.common.beans.GmCalenderOperations"%>
 <%
 GmCalenderOperations.setTimeZone(strGCompDateFmt);
 String strTodaysDate =  GmCalenderOperations.getCurrentDate(strGCompDateFmt); 
 String strsalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");
 %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0">
<bean:define id="gridCalendar" name="frmEventRpt" property="gridCalendar" type="java.lang.String"> </bean:define>
<bean:define id="eventStartDt" name="frmEventRpt" property="eventStartDt" type="java.util.Date"> </bean:define>
<bean:define id="eventEndDt" name="frmEventRpt" property="eventEndDt" type="java.util.Date"> </bean:define>
<bean:define id="applnDateFmt" name="frmEventRpt" property="applnDateFmt" type="java.lang.String"> </bean:define>
<%
String strFromDt = GmCommonClass.parseNull((String)GmCommonClass.getStringFromDate(eventStartDt,strGCompDateFmt));
String strToDt = GmCommonClass.parseNull((String)GmCommonClass.getStringFromDate(eventEndDt,strGCompDateFmt));
%>
<title>Case Calendar View</title>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">
<link rel='STYLESHEET' type='text/css' href='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler_ext.css'>
<link rel='STYLESHEET' type='text/css' href='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler.css'>
<link rel='STYLESHEET' type='text/css' href='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler_glossy.css'>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
	
<script language="JavaScript" src='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler.js'></script>
<script language="JavaScript" src='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler_minical.js'></script>
<script language="JavaScript" src='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler_readonly.js'></script>
<script language="JavaScript" src='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler_tooltip.js'></script>
<script language="JavaScript" src='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler_expand.js'></script>
		
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js" type="text/javascript"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js" type="text/javascript"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"  type="text/javascript"></script>	
	
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strsalesJsPath%>/event/GmEventCalendar.js"></script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>	
<script type="text/javascript" charset="utf-8">
	var gridData ='<%=gridCalendar%>';
	var fromDate = '<%=strFromDt%>';
</script>
</head>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<html:form action="/gmEventReport.do?method=viewEventCalendar" >
<html:hidden property="strOpt" name="frmEventRpt" />
<html:hidden property="eventStartDt" name="frmEventRpt" value="<%=strFromDt %>" />
<html:hidden property="eventEndDt" name="frmEventRpt"  value="<%=strToDt %>"/>
<html:hidden property="ecMonth" name="frmEventRpt" />
<html:hidden property="ecYear" name="frmEventRpt" />
<html:hidden property="haction" name="frmEventRpt" />
	<table border="0" class="DtTable950" cellspacing="0" cellpadding="0">			
		<tr>
			<td colspan="5" height="25" class="RightDashBoardHeader"><fmtEventCalendar:message key="LBL_EVENT_CALENDER_VIEW"/></td>
			<td align="right" class=RightDashBoardHeader>
				<fmtEventCalendar:message key="IMG_ALT_HELP" var="varHelp"/><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("EVENT_CALENDAR_VIEW")%>');" />
			</td> 
		</tr>
		<tr> 
			<td colspan="6" bgcolor="#666666"></td>
		</tr>
	<% if(!gridCalendar.equals("")){%>
		<tr>
			<td colspan="6">
				<div id="eventCalender" class="dhx_cal_container" style='width:950; height:1200; '>
					<div class="dhx_cal_navline">
						<div class="dhx_cal_prev_button">&nbsp;</div>
						<div class="dhx_cal_next_button">&nbsp;</div>
						<div class="dhx_cal_today_button"></div>
						<div class="dhx_cal_date"></div>
						<div class="dhx_minical_icon" id="dhx_minical_icon" onclick="show_minical()">&nbsp;</div>
						<div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>
						<div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div>
						<div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div>
					</div> 		
					<div class="dhx_cal_header"></div>
					<div class="dhx_cal_data"></div>		
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="1" height="25" class="RightTableCaption" ></td>
			<td>
				<div id="footer-sites" align="center" style="background:">
                        <span style="color:red;font-size:500%">&#9632;</span>&nbsp; <b><fmtEventCalendar:message key="LBL_IN_COMPLETE"/></b> &nbsp;
                        <span style="color:green;font-size:500%">&#9632;</span>&nbsp;<b><fmtEventCalendar:message key="LBL_ACTIVE"/> </b>   &nbsp;
                        <span style="color:black;font-size:500%">&#9632;</span>&nbsp;<b> <fmtEventCalendar:message key="LBL_CANCELLED"/></b> &nbsp;
                        <span style="color:yellow;font-size:500%">&#9632;</span>&nbsp; <b><fmtEventCalendar:message key="LBL_CLOSED"/></b> &nbsp;
                	</div>
			</td>		
			<td colspan="2" height="25" class="RightTableCaption" ></td>
		</tr>
	<%}%>
	</table>
</html:form>
</body>
 <%@ include file="/common/GmFooter.inc" %>
</html>