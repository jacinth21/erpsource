<!--  \sales\Event\GmEventListReport.jsp-->
<%
/**********************************************************************************
 * File		 		: GmEventListReport.jsp
 * Desc		 		: This jsp will show the Event List report
 * Version	 		: 1.0
 * author			: Jignesh Shah
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtEventListReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtEventListReport:setLocale value="<%=strLocale%>"/>
<fmtEventListReport:setBundle basename="properties.labels.sales.Event.GmEventListReport"/>

<bean:define id="xmlGridData" name="frmEventRpt" property="xmlGridData" type="java.lang.String"></bean:define>
<bean:define id="loanTo" name="frmEventRpt" property="loanTo" type="java.lang.String"></bean:define>
<bean:define id="alRepList" name="frmEventRpt" property="alRepList" type="java.util.ArrayList"></bean:define>
<bean:define id="alEmpList" name="frmEventRpt" property="alEmpList" type="java.util.ArrayList"></bean:define>
<bean:define id="alOUSList" name="frmEventRpt" property="alOUSList" type="java.util.ArrayList"></bean:define>
<bean:define id="dtType" name="frmEventRpt" property="dtType" type="java.lang.String"></bean:define>
<%
HashMap hcboVal = null;

String strsalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");
String strDisableDpdwn = "";
String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
if(loanTo.equals("0") || loanTo.equals("")){
	strDisableDpdwn = "disabled";
}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Event List Report  </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strsalesJsPath%>/event/GmEventListReport.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script type="text/javascript">
var objGridData = '<%=xmlGridData%>';

var RepLen = <%=alRepList.size()%>;
var EmpLen = <%=alEmpList.size()%>;
var OUSLen = <%=alOUSList.size()%>;
var strDtType = '<%=dtType%>';
var format = '<%=strApplDateFmt%>';
<%
hcboVal = new HashMap();
for (int i=0;i<alRepList.size();i++)
{
	hcboVal = (HashMap)alRepList.get(i);
%>
var alRepListArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>

<%
hcboVal = new HashMap();
for (int i=0;i<alEmpList.size();i++)
{
	hcboVal = (HashMap)alEmpList.get(i);
%>
var alEmpListArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>
<%
hcboVal = new HashMap();
for (int i=0;i<alOUSList.size();i++)
{
	hcboVal = (HashMap)alOUSList.get(i);
%>
var alOUSListArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form  action="/gmEventReport.do?method=listEvent">
<html:hidden property="strOpt" name="frmEventRpt"/>
<!-- Custom tag lib code modified for JBOSS migration changes -->
<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader" ><fmtEventListReport:message key="LBL_EVENT_LIST_REPORT"/></td>
			<td  class="RightDashBoardHeader" align="right" colspan="4"></td>
		    <td align="right" class=RightDashBoardHeader>
				<fmtEventListReport:message key="IMG_ALT_HELP" var="varHelp"/><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("EVENT_LIST_REPORT")%>');" />
			</td> 
		</tr>
		<tr><td class="Line" height="1" colspan="7"></td></tr>	
		<tr class="evenshade">
			<td class="RightRedCaption" height="25" align="Right" width="140">&nbsp;<fmtEventListReport:message key="LBL_EVENT_NAME"/>:</td>
			<td width="230">&nbsp;<gmjsp:dropdown controlName="eventName"  SFFormName='frmEventRpt' SFSeletedValue="eventName"  defaultValue= "[Choose One]"	
				SFValue="alEventName" codeId="CID" codeName="ENAME" width="220"/>
			</td>
			<td class="RightRedCaption" height="25" align="Right" width="145"><fmtEventListReport:message key="LBL_PURPOSE_OF_EVENT"/>:</td>
			<td width="150">&nbsp;<gmjsp:dropdown controlName="pupEvent"  SFFormName='frmEventRpt' SFSeletedValue="pupEvent"  defaultValue= "[Choose One]"	
				SFValue="alPurEvent" codeId="CODEID" codeName="CODENM" width="120"/>
			</td>
			<td class="RightRedCaption" height="25" align="Right" width="150"><fmtEventListReport:message key="LBL_LOAN_TO"/>:</td>	
			<td Width="120">&nbsp;<gmjsp:dropdown controlName="loanTo"  SFFormName='frmEventRpt' SFSeletedValue="loanTo"  defaultValue= "[Choose One]"	
				SFValue="alLoanTo" codeId="CODEID" codeName="CODENM" width="100" onChange="javascript:fnLoadLoanToNames(this);"/>	
			</td>
			<td Width="100">&nbsp;	
			</td>
		</tr>
		<tr><td class="LLine" height="1" colspan="7"></td></tr>
		<tr class="oddshade">
			<td class="RightRedCaption" height="25" align="Right" width="140">&nbsp;<fmtEventListReport:message key="LBL_LOAN_TO_NAME"/>:</td>
			<% if(loanTo.equals("19518") ){%>
			<td width="230">&nbsp;<gmjsp:dropdown controlName="loanToName"  SFFormName='frmEventRpt' SFSeletedValue="loanToName"  defaultValue= "[Choose One]"	
				SFValue="alRepList" codeId="ID" codeName="NAME" width="220" disabled="<%=strDisableDpdwn%>"/>
			</td>
			<%}else if(loanTo.equals("19523") ){%>
			<td width="230">&nbsp;<gmjsp:dropdown controlName="loanToName"  SFFormName='frmEventRpt' SFSeletedValue="loanToName"  defaultValue= "[Choose One]"	
				SFValue="alOUSList" codeId="ID" codeName="NAME" width="220" disabled="<%=strDisableDpdwn%>"/>
			</td>			
			<%}else{%>
			<td width="230">&nbsp;<gmjsp:dropdown controlName="loanToName"  SFFormName='frmEventRpt' SFSeletedValue="loanToName"  defaultValue= "[Choose One]"	
				SFValue="alEmpList" codeId="ID" codeName="NAME" width="220" disabled="<%=strDisableDpdwn%>"/>
			</td>
			<%} %>
			<td class="RightRedCaption" align="Right" width="150"><fmtEventListReport:message key="LBL_DATE_RANGE"/>:</td>
			<td class="RightRedCaption" colspan="3">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightRedCaption" align="Right"><fmtEventListReport:message key="LBL_TYPE"/>:</td>
						<td>&nbsp;<select name="dtType">
							<option value='0' ><fmtEventListReport:message key="LBL_CHOOSE_ONE"/></option>
							<option value='ESD'><fmtEventListReport:message key="LBL_EVENT_START_DATE"/></option>
							<option value='EED'><fmtEventListReport:message key="LBL_EVENT_END_DATE"/></option>
							</select>
						</td>
						<td></td><td></td>
					</tr>
					<tr>
						<td class="RightRedCaption" align="Right"><fmtEventListReport:message key="LBL_FROM_DATE"/>:</td>
						<td>&nbsp;<gmjsp:calendar SFFormName="frmEventRpt" controlName="startDt"  gmClass="InputArea" 
						onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
						</td>
						<td class="RightRedCaption" align="Right">&nbsp;<fmtEventListReport:message key="LBL_TO_DATE"/>:</td>
						<td>&nbsp;<gmjsp:calendar SFFormName="frmEventRpt" controlName="endDt" gmClass="InputArea" 
							onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
						</td>
					</tr> 
				</table>
			</td>
			
			<td Width="30" align="center">&nbsp;<fmtEventListReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;${varLoad}&nbsp;" name="Btn_Submit" buttonType="Load" onClick="fnLoad();"/></td>
		</tr>
	  	<tr><td class="LLine" height="1" colspan="7"></td></tr>
	  		<%if(!xmlGridData.equals("")){%>
		<tr>
	    	<td colspan="7" align="center">
				<div id="GridData" style="grid" height="500px" width="1000px"></div>
		   	</td>	
		</tr>
		<%}else{%>
		<tr>
			<td colspan="7" align="center" height="30" class="RightText"><fmtEventListReport:message key="MSG_NO_DATA"/></td>
		</tr>
		<%}%>
		<tr>
			<td height="1" colspan="7" class="LLine"></td>
		</tr>
		<%if(xmlGridData.indexOf("cell") != -1) {%>
		<tr>
			<td colspan="7" align="center">
			    <div class='exportlinks'><fmtEventListReport:message key="DIV_EXPORT_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnDownloadXLS();"><fmtEventListReport:message key="DIV_EXCEL"/>
			    </a></div>
			</td>
		</tr>
		<%} %>
</table>	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

