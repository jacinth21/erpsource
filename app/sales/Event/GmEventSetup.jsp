<!--  \sales\Event\GmEventSetup.jsp-->
<%
	/**********************************************************************************
	 * File		 		: GmEventSetup.jsp
	 * Desc		 		: This screen is used for Event Setup
	 * Version	 		: 1.0
	 * author			: Hreddi
	 * 
	 ************************************************************************************/
%> 
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>  
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%> 
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtEventSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtEventSetup:setLocale value="<%=strLocale%>"/>
<fmtEventSetup:setBundle basename="properties.labels.sales.Event.GmEventSetup"/>

<bean:define id="eventName" name="frmEventSetup" property="eventName" type="java.lang.String"></bean:define>
<bean:define id="alEventMaterialReqList" name="frmEventSetup" property="alEventMaterialReqList" type="java.util.List"></bean:define>
<bean:define id="caseInfoID" name="frmEventSetup" property="caseInfoID" type="java.lang.String"></bean:define>
<bean:define id="selectedCategories" name="frmEventSetup" property="selectedCategories" type="java.lang.String"></bean:define>
<bean:define id="eventLoanTo" name="frmEventSetup" property="eventLoanTo" type="java.lang.String"></bean:define>
<bean:define id="strOpt" name="frmEventSetup" property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="eventStartDate" name="frmEventSetup" property="eventStartDate" type="java.lang.String"></bean:define>
<bean:define id="eventEndDate" name="frmEventSetup" property="eventEndDate" type="java.lang.String"></bean:define>
<bean:define id="eventID" name="frmEventSetup" property="eventID" type="java.lang.String"></bean:define>
<bean:define id="plannedshippedDateFl" name="frmEventSetup" property="plannedshippedDateFl" type="java.lang.String"></bean:define>
<bean:define id="alEmployeeList" name="frmEventSetup" property="alEmployeeList" type="java.util.ArrayList"></bean:define>
<bean:define id="alRepList" name="frmEventSetup" property="alRepList" type="java.util.ArrayList"></bean:define>
<bean:define id="alOUSList" name="frmEventSetup" property="alOUSList" type="java.util.ArrayList"></bean:define>
<%
log.debug("alOUSList>>>>>>>>>>"+alOUSList);
GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.sales.Event.GmEventSetup", strSessCompanyLocale);
request.setCharacterEncoding("UTF-8");
response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8");
HashMap hcboVal = null;
String strWikiTitle = "";
String strSessApplDateFmt =  strGCompDateFmt;
GmCalenderOperations.setTimeZone(strGCompTimeZone);
if(strOpt.equals("reschedule")){
	 strWikiTitle = GmCommonClass.getWikiTitle("EVENT_RESCHEDULE");
}else if(strOpt.equals("edit")){
	 strWikiTitle = GmCommonClass.getWikiTitle("EVENT_EDIT");
}else{
	 strWikiTitle = GmCommonClass.getWikiTitle("EVENT_SETUP");
}
String strSalesEventJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_EVENT");
String strDisableDropdown = "";
String strFieldsDisable = "";
String strLableDisable  = "";
String strHeader		= GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_GENERAL_INFO"));
String currentdate = GmCalenderOperations.getCurrentDate(strSessApplDateFmt);
if(eventLoanTo.equals("0") || eventLoanTo.equals("")){
	strDisableDropdown = "disabled";
}
if(strOpt.equals("edit")){
	strFieldsDisable =  "disabled";
}
if(strOpt.equals("reschedule")){
	strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_RESCHEDULE_EVENT"));
}else if(strOpt.equals("edit")){
	strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_EDIT_GENERAL_INFORMATION")); 
}
//PC-5609 - company information not passing in edge browser after submit or redirect the page, so get company info and form companyInfo JSON string in JSP ANd set into form variable
String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\",\"cmplangid\":\""+gmDataStoreVO.getCmplangid()+"\",\"cmpdfmt\":\""+strSessApplDateFmt+"\"}";

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Case Book </TITLE> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strSalesEventJsPath %>/GmEventSetup.js"></script>

<script language="JavaScript"> 
var categoriesLen = <%=alEventMaterialReqList.size()%>;
var strCaseInfoID = "<%=caseInfoID%>";
var sCategories   = "<%=selectedCategories%>";
var caseInfoID    = "<%=caseInfoID%>"; 
var planshipFl    = "<%=plannedshippedDateFl%>";

var RepLen = <%=alRepList.size()%>;
var EmpLen = <%=alEmployeeList.size()%>;
var OUSLen = <%=alOUSList.size()%>;
var format = "<%=strSessApplDateFmt%>";
var lblEventName = '<fmtEventSetup:message key="LBL_EVENT_NAME"/>';
var lblEventVenue= '<fmtEventSetup:message key="LBL_VENUE"/>';
var lblPurposeofEvent= '<fmtEventSetup:message key="LBL_PURPOSE_OF_EVENT"/>';
var lblLoanTo= '<fmtEventSetup:message key="LBL_LOAN_TO"/>';
var lblLoanToName='<fmtEventSetup:message key="LBL_LOAN_TO_NAME"/>';
<%
hcboVal = new HashMap();
for (int i=0;i<alRepList.size();i++)
{
	hcboVal = (HashMap)alRepList.get(i);
%>
var alRepListArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>

<%
hcboVal = new HashMap();
for (int i=0;i<alEmployeeList.size();i++)
{
	hcboVal = (HashMap)alEmployeeList.get(i);
%>
var alEmpListArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>

<%
hcboVal = new HashMap();
for (int i=0;i<alOUSList.size();i++)
{
	hcboVal = (HashMap)alOUSList.get(i);
%>
var alOUSListArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>
</script>
</HEAD>
 
<BODY leftmargin="20" topmargin="10" onload="fnCheckSelections();">
<html:form action="/gmEventSetup.do">
<html:hidden property="strOpt" />
<html:hidden property="caseInfoID" />
<html:hidden property="eventID" />
<html:hidden property="applnDateFmt" />
<html:hidden property="backScreen"/>
<input type="hidden" name="currentdate" value="<%=currentdate %>"/>
<%if(strOpt.equals("edit")){ %>
<html:hidden property="eventStartDate" />
<html:hidden property="eventEndDate" />
<%} %>
<%if(strOpt.equals("reschedule")){ %>
<html:hidden property="eventName" />
<html:hidden property="purpose" />
<%} %>
<%if((!plannedshippedDateFl.equals("Y")) && (strOpt.equals("reschedule"))){%>
<html:hidden property="plannedShipeddate" />
<html:hidden property="expectedReturnDt" />
<%} %>
 <html:hidden property="companyInfo" value="<%=strCompanyInfo%>"/> <!--  PC-5609 - set the companyInfo in hidden variable  -->
<!-- Custom tag lib code modified for JBOSS migration changes -->
<!-- Struts tag lib code modified for JBOSS migration changes -->

	<table border="0" class="DtTable500" cellspacing="0" cellpadding="0">	
		<tr>
		   <td colspan="2">
		   <table  border="0" cellspacing="0" cellpadding="0"> 
		   <tr>
		        <td height="25" width="75%" class="RightDashBoardHeader" colspan="1">&nbsp;<fmtEventSetup:message key="LBL_EVENT_BOOKING"/> <%=strHeader%></td>
			    <td class="RightDashBoardHeader" width="25%" colspan="1"><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
	    	</tr> 
	    	</table>
	    	</td>
		</tr>			
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<% if(strOpt.equals("edit")){ %>
					<tr class="shade">
						<td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;<fmtEventSetup:message key="LBL_EVENT_ID"/>:</td>
						<td>&nbsp;<bean:write name="frmEventSetup" property="eventID"/></td>
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<%} %>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;<fmtEventSetup:message key="LBL_EVENT_NAME"/></td>
						<%if(!strOpt.equals("reschedule")){%>
						<td align="left" >&nbsp;<html:text maxlength="50" property="eventName" size="25"   
							styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
						</td>	
						<%}else{ %>
						<td>&nbsp;<bean:write name="frmEventSetup" property="eventName"/></td>
						<%} %>
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right" HEIGHT="25" ><font color="red">*</font>&nbsp;<fmtEventSetup:message key="LBL_PURPOSE_OF_EVENT"/>:</td>
						<%if(!strOpt.equals("reschedule")){%>
						<td >&nbsp;<gmjsp:dropdown controlName="eventPurpose" SFFormName="frmEventSetup" SFSeletedValue="eventPurpose" defaultValue= "[Choose One]" 
								SFValue="alEventPurposeList" codeId="CODEID" codeName="CODENM"/>
						</td>
						<%}else{ %>	
						<td>&nbsp;<bean:write name="frmEventSetup" property="purpose"/></td>
						<%} %>
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr class="">
						<td class="RightTableCaption" align="right" HEIGHT="25" width="40%" ><font color="red">*</font>&nbsp;<fmtEventSetup:message key="LBL_START_DATE"/>:</td>						
						<% if(strOpt.equals("edit")){ %>
							<td>&nbsp;<bean:write name="frmEventSetup" property="eventStartDate"/></td>
						<% }
						else{ %>
						<td >&nbsp;<gmjsp:calendar SFFormName="frmEventSetup" controlName="eventStartDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" /></td>
						<%} %>
					</tr>	
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right" HEIGHT="25" ><font color="red">*</font>&nbsp;<fmtEventSetup:message key="LBL_END_DATE"/>:</td>
						<% if(strOpt.equals("edit")){ %>
							<td>&nbsp;<bean:write name="frmEventSetup" property="eventEndDate"/></td>
						<% }
						else{ %>
						<td>&nbsp;<gmjsp:calendar  SFFormName="frmEventSetup" controlName="eventEndDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" /></td>
						<%} %>	
					</tr>
					<%if((strOpt.equals("reschedule"))&& (plannedshippedDateFl.equals("Y"))){%>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="25" ><font color="red">*</font>&nbsp;<fmtEventSetup:message key="LBL_PLANNED_SHIP_DATE"/>:</td>
						<td>&nbsp;<gmjsp:calendar  SFFormName="frmEventSetup" controlName="plannedShipeddate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" /></td>
					</tr>
					<tr ><td colspan="4" class="LLine" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right" HEIGHT="25" ><font color="red">*</font>&nbsp;<fmtEventSetup:message key="LBL_EXPECTED_RETURN_DATE"/>:</td>
						<td>&nbsp;<gmjsp:calendar  SFFormName="frmEventSetup" controlName="expectedReturnDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" /></td>
					</tr>
					<%} %>
					<% if(strOpt.equals("reschedule")){%>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="25" ><font color="red">*</font>&nbsp;<fmtEventSetup:message key="LBL_REASON"/>:</td>
						<td >&nbsp;<gmjsp:dropdown controlName="rescheduleReason" SFFormName="frmEventSetup" SFSeletedValue="rescheduleReason" defaultValue= "[Choose One]" 
								SFValue="alrescheduleReasonList" codeId="CODEID" codeName="CODENM"/>
						</td>	
					</tr>	
					<%}%>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<% if(!strOpt.equals("reschedule")){ %>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="25" ><font color="red">*</font>&nbsp;<fmtEventSetup:message key="LBL_VENUE"/>:</td>
						<td align="left" >&nbsp;<html:text maxlength="100" property="eventVenue"  size="25"    
							styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
						</td>	
					</tr>					
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right" HEIGHT="25" ><font color="red">*</font>&nbsp;<fmtEventSetup:message key="LBL_LOAN_TO"/>:</td>
						<td >&nbsp;<gmjsp:dropdown controlName="eventLoanTo" SFFormName="frmEventSetup" SFSeletedValue="eventLoanTo" defaultValue= "[Choose One]" 
								SFValue="alEventLoanToList" codeId="CODEID" codeName="CODENM" onChange="javascript:fnLoadLoanToNames(this);"/>
						</td>	
					</tr>	
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr class="">
					<td class="RightTableCaption" align="right" HEIGHT="25" ><font color="red">*</font>&nbsp;<fmtEventSetup:message key="LBL_LOAN_TO_NAME"/>:</td>
					<% if(eventLoanTo.equals("19518") ){%>
						
						<td >&nbsp;<gmjsp:dropdown  controlName="eventLoanToName" SFFormName="frmEventSetup" SFSeletedValue="eventLoanToName" defaultValue= "[Choose One]" 
								SFValue="alRepList" codeId="ID" codeName="NAME"/>
						</td>
					<%}else if(eventLoanTo.equals("19523") ){%>
					<td >&nbsp;<gmjsp:dropdown  controlName="eventLoanToName" SFFormName="frmEventSetup" SFSeletedValue="eventLoanToName" defaultValue= "[Choose One]" 
								SFValue="alOUSList" codeId="ID" codeName="NAME"/>
						</td>
					<%}else{%>
						<td >&nbsp;<gmjsp:dropdown disabled="<%=strDisableDropdown%>" controlName="eventLoanToName" SFFormName="frmEventSetup" SFSeletedValue="eventLoanToName" defaultValue= "[Choose One]" 
								SFValue="alEmployeeList" codeId="ID" codeName="NAME"/> 
						</td>	
					<%} %>
					</tr>					
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr class="shade">
						<td  class="RightTableCaption" align="Right">						
						&nbsp;<fmtEventSetup:message key="LBL_MATERIALS_REQUIRED"/>:</td>		
							<td>&nbsp;<gmjsp:dropdown controlName="favouriteCaseID" SFFormName="frmEventSetup" SFSeletedValue="favouriteCaseID" defaultValue= "[Choose from Favorites]" 
								SFValue="alFavoriteEventList" disabled="<%=strFieldsDisable%>" onChange="fnFavorite()" codeId="CODEID" codeName="CODENM"/>	</td>						
					</tr>
					<tr class="shade">
						<td align="left">&nbsp;</td>
						<td> <DIV style="display:visible;height: 80px;width:185px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
     			     <table>
                        	<logic:iterate id="selectedCategorieslist" name="frmEventSetup" property="alEventMaterialReqList">
                        	<tr>
                        		<td>
								    <htmlel:multibox property="eventRequestedMeterial" value="${selectedCategorieslist.CODEID}"  onclick="fnSelectedMaterialslist();" />
								    <bean:write name="selectedCategorieslist" property="CODENM" />
	     						</td>
							</tr>	    
							</logic:iterate>
							</table>												
							</DIV>
							
						</td>
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<%}%>
					<tr>
						<td colspan="4"> 
							<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="FORMNAME" value="frmEventSetup" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="Edit" />
							</jsp:include>
						</td>
					</tr>
					<tr >
						<td colspan="4" height="30" align="center">&nbsp;
							<%if(strOpt.equals("reschedule")){ %>
								<fmtEventSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" name="Btn_Submit" onClick="fnReschedule();" buttonType="Save"/>&nbsp;	
							<%}else{ %>	
								<fmtEventSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" name="Btn_Submit" onClick="fnSubmit();" buttonType="Save"/>&nbsp;
								<fmtEventSetup:message key="BTN_NEXT" var="varNext"/><gmjsp:button value="&nbsp;${varNext}&nbsp;"  onClick="javascript:fnNext();" buttonType="Save"/>
							<%} %>
														
						</td>
					</tr>				
    </table>	
</html:form>   
</BODY>
 <%@ include file="/common/GmFooter.inc" %> 
</HTML>