 <%
/**********************************************************************************
 * File		 			: GmSalesOrderDetails.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.1
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- \sales\GmSalesOrderDetails.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<%@ taglib prefix="fmtSalesOrderDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!--sales\GmSalesOrderDetails.jsp  -->
<fmtSalesOrderDetails:setLocale value="<%=strLocale%>"/>
<fmtSalesOrderDetails:setBundle basename="properties.labels.sales.GmSalesOrderDetails"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	
	String strAccId = (String)request.getAttribute("strAccId");


%>
<HTML>
<TITLE>Sales Detail Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>

function fnSubmit()
{
	document.frmAccount.hAction.value = "Go";
	document.frmAccount.submit();
}

function fnPrintOrder(val)
{
	windowOpener('/GmEditOrderServlet?hMode=PrintPrice&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=760,height=410");	
}

function fnOpenTrack(val)
{
windowOpener('https://www.fedex.com/fedextrack?ascend_header=1&clienttype=dotcom&cntry_code=us&language=english&tracknumbers='+val,"Track","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
}

function fnPrintInvoice(varinv)
{
windowOpener('/GmInvoiceInfoServlet?hAction=Print&hInv='+varinv,"PrntInv","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=500");
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmSales" >
<input type="hidden" name ="hAccId" value ="<%=strAccId%>">

<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" width = 100% class="RightDashBoardHeader">
		<fmtSalesOrderDetails:message key="LBL_SALES_DET"/>
			</td>
		</tr>
		<tr><td class="Line" height="1" colspan="2"></td></tr>
</table> 

		<display:table name="requestScope.results.rows" export="false" class="its" id="currentRowObject" varTotals="totals" decorator="com.globus.displaytag.beans.DTSaleDetailWrapper">
					<fmtSalesOrderDetails:message key="DT_ACC_NAME" var="varAccName"/><display:column property="NAME" title="${varAccName}" class="alignleft" sortable="true"  />
					<fmtSalesOrderDetails:message key="DT_ORDER_ID" var="varOrderId"/><display:column property="ID" title="${varOrderId}" class="aligncenter"  sortable="true" />
					<fmtSalesOrderDetails:message key="DT_TOT_COST" var="varTotCost"/><display:column property="COST" title="${varTotCost}" class="alignright"  sortable="true" total="true"/>
					<fmtSalesOrderDetails:message key="DT_SHIP_COST" var="varShipCost"/><display:column property="SHIPCOST" title="${varShipCost}" class="alignright"  total="true"/>
					<fmtSalesOrderDetails:message key="DT_ORDER_STATUS" var="varOrderStatus"/><display:column property="ORDER_STATUS" title="${varOrderStatus}" sortable="true" class="alignleft"  />
					<fmtSalesOrderDetails:message key="DT_REP_NAME" var="varRepName"/><display:column property="REP_NAME" title="${varRepName}" sortable="true" class="alignleft"  />
					<fmtSalesOrderDetails:message key="DT_TRACKING" var="varTracking"/><display:column property="TRACK" title="${varTracking}" class="alignright"  />
					<fmtSalesOrderDetails:message key="DT_CUST_PO" var="varCustPo"/><display:column property="PO" title="${varCustPo}" class="alignright"  />
					<fmtSalesOrderDetails:message key="DT_INV_ID" var="varInvId"/><display:column property="INVID" title="${varInvId}" class="aligncenter"  />
					<fmtSalesOrderDetails:message key="DT_ORDER_TYPE" var="varOrderType"/><display:column property="ORDERTYPE" title="${varOrderType}" class="alignleft"  />
					
		<display:footer media="html"> 

<% 
		String strVal;
		strVal = ((HashMap)pageContext.getAttribute("totals")).get("column3").toString();
		String strTotCost =  " $ " + GmCommonClass.getStringWithCommas(strVal);
		strVal = ((HashMap)pageContext.getAttribute("totals")).get("column4").toString();
		String strShipCost =  " $ " + GmCommonClass.getStringWithCommas(strVal);
%>
		<tr class="shade">
			<td colspan="2"><B><fmtSalesOrderDetails:message key="LBL_TOTAL"/></B></td>
		  	<td class = "alignright"> <B><%=strTotCost%></B></td>
  		  	<td class = "alignright"> <B><%=strShipCost%></B></td>
  		  	<td colspan="6"> </td>
		</tr>
 </display:footer>		
		</display:table>
					
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</FORM>
</BODY>

</HTML>
