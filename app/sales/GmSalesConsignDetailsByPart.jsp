 <%
/**********************************************************************************
 * File		 		: GmSalesConsignDetailsByPart.jsp
 * Desc		 		: This screen is used to report on Consignment by Part / Distributor
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- Imports for Logger -->

<%@ page import="com.globus.common.beans.GmResourceBundleBean" %> 

<%@ taglib prefix="fmtSalesConsign" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- globusMedApp\sales\GmSalesConsignDetailsByPart.jsp -->
<fmtSalesConsign:setLocale value="<%=strLocale%>"/>
<fmtSalesConsign:setBundle basename="properties.labels.sales.GmSalesConsignDetailsByPart"/>

<%
try {

	GmServlet gm = new GmServlet();
	HashMap hmReturn = new HashMap();
	HashMap hmParam = new HashMap();
	HashMap hmFilterDetails = new HashMap();
	ArrayList alProjList = new ArrayList();
	ArrayList alDistributor = new ArrayList();
	ArrayList alSets = new ArrayList();
	ArrayList alStates = new ArrayList();
	ArrayList alRegions = new ArrayList();
	ArrayList alDrillDown 	= new ArrayList();
	
	String strSelected = "";
	String strCodeID = "";
	String strProjNm = "";
	String strProjId = "";
	String strFromMonth 	= "";
	String strFromYear		= "";
	String strToMonth		= "";
	String strToYear		= "";
	String strCondition		= "";
	String strType	 = "";
	String strSetNumber		= "";
	String strDistributorID = "";
	String strAccountID 	= "";
	String strRepID 		= "";
	String strPartNumber	= "";
	String strADID 			= "";
	String strTerritoryID	= "";
	String strAccessFilter	= "";
	String strSetId = "";
	String strAction = "";
	String strPageHeader = "";
	String strRegion = "";
	String strState = "";
	String strWikiTitle = "";
	int intSortColumn;
	int intSortOrder;
	
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	gmCrossTab.setExport(true, pageContext, "/GmSalesConsignDetailsServlet", "SalesConsignDetailbyPartReport", "frmOrder");
	gm.checkSession(response,session);
	
	String strShowArrow = GmCommonClass.parseZero((String)request.getAttribute("Chk_ShowArrowFl"));
	String strShowPercent = GmCommonClass.parseZero((String)request.getAttribute("Chk_ShowPercentFl"));
	String strHideAmount = GmCommonClass.parseZero((String)request.getAttribute("Chk_HideAmountFl"));
	
	hmParam = (HashMap)request.getAttribute("PARAM");
	hmFilterDetails = (HashMap)request.getAttribute("FILTERDETAILS");
	strState = GmCommonClass.parseNull((String)request.getAttribute("strState"));
	strRegion = GmCommonClass.parseNull((String)request.getAttribute("strRegion"));
	
	// log.debug(" HMPARAM " + hmParam);
	
	if (hmFilterDetails != null) 
	{
		alSets = (ArrayList)hmFilterDetails.get("SETLIST");
		alDistributor = (ArrayList)hmFilterDetails.get("DISTRIBUTORLIST");
		alStates = (ArrayList)hmFilterDetails.get("STATES");
	    alRegions  = (ArrayList)hmFilterDetails.get("REGIONS");
	}
	
	if (hmParam != null)
	{
			//log.debug(" pnum " + GmCommonClass.parseNull((String) hmParam.get("PartNumber")));
			strFromMonth 	= GmCommonClass.parseNull((String) hmParam.get("FromMonth"));
			strFromYear		= GmCommonClass.parseNull((String) hmParam.get("FromYear"));
			strToMonth			= GmCommonClass.parseNull((String) hmParam.get("ToMonth")); 
			strToYear				= GmCommonClass.parseNull((String) hmParam.get("ToYear"));
			strCondition		= GmCommonClass.parseNull((String) hmParam.get("Condition")) ;
			strType					= GmCommonClass.parseNull((String) hmParam.get("Type")); 
			strDistributorID = GmCommonClass.parseNull((String) hmParam.get("DistributorID"));
			strAccountID 		= GmCommonClass.parseNull((String) hmParam.get("AccountID"));
			strRepID 				= GmCommonClass.parseNull((String) hmParam.get("RepID"));
			strPartNumber	= GmCommonClass.parseNull((String) hmParam.get("PartNumber"));
			strADID 				= GmCommonClass.parseNull((String) hmParam.get("ADID"));
			strTerritoryID		= GmCommonClass.parseNull((String) hmParam.get("TerritoryID"));
			strAccessFilter	= GmCommonClass.parseNull((String) hmParam.get("AccessFilter"));	
			strAction 	= GmCommonClass.parseNull((String) hmParam.get("hAction"));	
	}

	intSortColumn =  request.getParameter("hSortColumn") == null ? -1 : Integer.parseInt((String)request.getParameter("hSortColumn"));
	
	//log.debug(" Int strSortColumn " + intSortColumn);
	intSortOrder = Integer.parseInt(GmCommonClass.parseZero((String)request.getParameter("hSortOrder")));	
	
	strSetId = GmCommonClass.parseNull((String)request.getAttribute("SetId"));
	strSetNumber = GmCommonClass.parseNull((String)request.getAttribute("hSetNumber"));
	strPartNumber = GmCommonClass.parseNull((String)request.getAttribute("hPartNumber"));
	
	// Combobox default value
	String strHeader = (String)request.getAttribute("strHeader");
	String strProj = (String)request.getAttribute("hSetNumber");
	
	strProj = (strProj == null)?"0":strProj; // If no value selected will set the combo box value to [Choose One]
	strProj = (strProj.equals("null"))?"0":strProj; // If no value selected will set the combo box value to [Choose One]
	strType = (strType.equals(""))?"1":strType; // Setting combo box value
	
	HashMap hmMapSets = new HashMap();
	hmMapSets.put("ID","");
	hmMapSets.put("PID","ID");
	hmMapSets.put("NM","NAME");	
	
	HashMap hmMapDistributor = new HashMap();
	hmMapDistributor.put("ID","");
	hmMapDistributor.put("PID","CODEID");
	hmMapDistributor.put("NM","CODENM");	
	
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.labels.sales.GmSalesConsignDetailsByPart", strSessCompanyLocale);
	//log.debug(" size of alDistributor  " + alDistributor.size());
	if(strAction.equals("LoadPart"))
	{
		strPageHeader = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_DTLS_BY_PART_HEADER"));
		strWikiTitle = GmCommonClass.getWikiTitle("REPORT_DISTRIBUTOR");
	}
	
	else if (strAction.equals("LoadDistributor"))
	{
		strPageHeader = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_CONSIGN_SALES_HEADER"));
		strWikiTitle = GmCommonClass.getWikiTitle("COSIGNMENT_SALES_REPORT");
	}
	
	
%>
<HTML>
<HEAD> 
<TITLE> Globus Medical: Consignment Vs Sales by Part/Distributor </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script>
 
 
function fnGo()
{ 
	var objSet = document.frmOrder.Chk_GrpSet;
//	var objDist = document.frmOrder.Chk_GrpDistributor;
	var setlen = '';
	var Action = document.frmOrder.hAction.value;
	var region = document.frmOrder.cbo_Region.value;
	var state = document.frmOrder.cbo_State.value;
	
	if (Action == 'LoadDistributor')
	{
		setlen = 0;
	}		
	else {
		setlen = objSet.length;
	}
	
//	var distlen = objDist.length;
	var catstr = '';
	var setstr = '';
	var diststr = '';
	var distgrpstr = '';
	var distgrpstrfinal = '';
	var setstrfinal = '';
	
	var temp = '';
	for (var i=0;i<setlen;i++)
	{
		if (objSet[i].status == true)
		{
			setstr = setstr + objSet[i].value + ',';
		}
	}
	setstrfinal = setstr.substr(0,setstr.length-1);
	document.frmOrder.SetNumber.value = setstrfinal;
	  
/*	for (var i=0;i<distlen;i++)
	{
		if (objDist[i].status == true)
		{
			temp = objDist[i].value;
			distgrpstr = distgrpstr + objDist[i].value + ',';
		}
	}
	
	distgrpstrfinal = distgrpstr.substr(0,distgrpstr.length-1);*/
//	document.frmOrder.DistributorID.value = distgrpstrfinal;
		
	var partnum = document.frmOrder.hPartNumber.value;
	var setid =	document.frmOrder.hSetId.value;
	
	if (setstrfinal == '' && partnum == '' && setid == '' && distgrpstrfinal == '')
	{
		if (Action !='LoadDistributor' )
		Error_Details(message[403]);
	}
	
/*	if (distgrpstrfinal == '')
	{
		if (Action !='LoadDistributor')
		Error_Details(message[404]);
	}*/
	
	if (Action == 'LoadDistributor' && partnum == '')
	{
		Error_Details(message[405]);
	}
	if (ErrorCount > 0)
	 {
		Error_Show();
		Error_Clear();
		return false;
	}
	 fnReloadSales('<%=strAction%>','<%=strServletPath%>/GmSalesConsignDetailsServlet');
	
	// fnSubmit();
}

function fnLoad()
{	 
	//alert('<%=strAction%>');
	document.frmOrder.Cbo_FromMonth.value = '<%=request.getAttribute("Cbo_FromMonth")%>';
	document.frmOrder.Cbo_ToMonth.value = '<%=request.getAttribute("Cbo_ToMonth")%>';
	document.frmOrder.Cbo_FromYear.value = '<%=request.getAttribute("Cbo_FromYear")%>';
	document.frmOrder.Cbo_ToYear.value = '<%=request.getAttribute("Cbo_ToYear")%>';
	document.frmOrder.hAction.value = '<%=strAction%>';	
	var setids = document.frmOrder.SetNumber.value;
	//var dist = document.frmOrder.DistributorID.value;
	 
	var objgrp = '';
	
	if (setids != '')
	{
		objgrp = document.frmOrder.Chk_GrpSet;
		fnCheckSelections(setids,objgrp);
	}

	 
	document.frmOrder.Cbo_Type.value = '<%=strType%>';

}


// Marks the checkboxes as 'checked'
function fnCheckSelections(val,objgrp)
{
	var valobj = val.split(",");
	var sval = '';
	var arrlen = objgrp.length;
	
	if (arrlen > 0)
	{
		for (var j=0;j< valobj.length;j++ )
		{
			sval = valobj[j];
			for (var i=0;i< arrlen;i++ )
			{
				if (objgrp[i].value == sval)
				{
					objgrp[i].status = true;
					break;
				}
			}
		}
	}
	else
	{
		objgrp.status = true;
	}
}

function fnSort(varColumnName, varSortOrder)
{
	document.frmOrder.hSortColumn.value = varColumnName;
	document.frmOrder.hSortOrder.value = varSortOrder;
	fnGo();
}

// Filter by distributor consignment 
function fnCallCons(val, Type)
{
	document.frmOrder.hAction.value = "LoadDistributor";
	document.frmOrder.hPartNumber.value = val;
	//document.frmOrder.action ="<%=strServletPath%>/GmSalesConsignDetailsServlet";
	//document.frmOrder.submit();
	url='<%=strServletPath%>/GmSalesConsignDetailsServlet?&hAction=LoadDistributor';
	fnReloadSales('',url);
}

function fnSubmit()
{
//var adID = document.frmOrder.cbo_Region.value;

	if (ErrorCount > 0)
	 {
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmOrder.submit();	
}
		
	

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad()">
<FORM name="frmOrder" method="post" action = "<%=strServletPath%>/GmSalesConsignDetailsServlet">
<input type="hidden" name="hAreaDirector" value="<%=request.getAttribute("hAreaDirector")%>">
<input type="hidden" name="DistributorID" value="<%=request.getAttribute("hDistributorID")%>">
<input type="hidden" name="hAccountID" value="<%=request.getAttribute("hAccountID")%>">
<input type="hidden" name="SetNumber" value="<%=request.getAttribute("hSetNumber")%>">
<input type="hidden" name="hRepID" value="<%=request.getAttribute("hRepID")%>">
<input type="hidden" name="Cbo_Name" value="">
<input type="hidden" name="hSortColumn" value="<%=intSortColumn%>">
<input type="hidden" name="hSortOrder" value="<%=intSortOrder%>">
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="hTerritory" value="<%=request.getAttribute("hTerritory")%>">
<input type="hidden" name="hAction" value="<%=strAction%>">
<%
	if(strAction.equals("LoadDistributor"))
	{
%>	
<input type="hidden" name="hSetId" value="<%=strSetId%>">
<%		
	}		
%>	
<TABLE class="DtTable800" cellSpacing=0 cellPadding=0  border="0">
	<tr>
		<td height=25 class=RightDashBoardHeader colspan="1"><%=strPageHeader%></td>
		<td  height="25" class="RightDashBoardHeader">
		<fmtSalesConsign:message key="IMG_ALT_HELP" var = "varHelp"/>
		<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		</td>
		
	</tr>
	<tr>
		<td height="20">
			<jsp:include page="/sales/GmSalesFilters.jsp" >
				<jsp:param name="HIDE" value="SYSTEM" />
				<jsp:param name="FRMNAME" value="frmOrder" />
				<jsp:param name="HACTION" value="<%=strAction%>" />
				<jsp:param name="URL" value="/GmSalesConsignDetailsServlet?hOpt=Report"/>
				<jsp:param name="HIDEBUTTON" value="HIDEGO" />
			</jsp:include> 
		</td>
	</tr>
	<tr><td class=Line height=1 colspan="2"></td></tr>
	<tr>
		<td colspan="2">
			<table cellspacing="0" cellpadding="0" width="100%" border="0">		
				<tr height="18" bgcolor="#eeeeee" class="RightTableCaption">
				<td width="1">&nbsp; </td>
<%					if (!strAction.equals("LoadDistributor")) {
%>
					<td>&nbsp;<fmtSalesConsign:message key="LBL_GROUP"/></td>
<% } %>					
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td> </td> 
<%					if (!strAction.equals("LoadDistributor")) {
%>
					<td><%=GmCommonControls.getChkBoxGroup("",alSets,"Set",hmMapSets,"IDNM")%></td>
<% } %>										
					<td>
						<table cellspacing="0" cellpadding="0" width="100%" border="0">		
						<%
							if(strAction.equals("LoadPart"))
							{
						%>	
								<tr>
									<td height="25" align="right" class="RightTableCaption"> <fmtSalesConsign:message key="LBL_SET_ID"/>:</td>
									<td>&nbsp;&nbsp;<input type="text" name="hSetId" value="<%=strSetId%>" class="RightText" size="40"> </td>
								</tr>
					   <%		
						    }		
					   %>		
							<tr>
								<td height="25" align="right" class="RightTableCaption" > <fmtSalesConsign:message key="LBL_PART_NUMS"/> :</td>
								<td>&nbsp;&nbsp;<input type="text" name="hPartNumber" value="<%=strPartNumber%>" class="RightText" size="40"> </td>
							</tr>							 
							 <tr>
							<td height="25" align="right" class="RightTableCaption"> <fmtSalesConsign:message key="LBL_STATE"/>:</td>
	                             <!-- Custom tag lib code modified for JBOSS migration changes -->
	                             <td>&nbsp;
			                        <gmjsp:dropdown controlName="cbo_State"  seletedValue="<%= strState %>" 	
									 value="<%= alStates %>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />                                                                  
	        	                </td>
            		        </tr>
            		         <tr>
	                        	<td height="25" align="right" class="RightTableCaption"> <fmtSalesConsign:message key="LBL_AREA_DIR"/>: </td>
	                             <!-- Custom tag lib code modified for JBOSS migration changes -->
	                             <td>&nbsp;
			                        <gmjsp:dropdown controlName="cbo_Region"  seletedValue="<%= strRegion %>" 	
									 value="<%= alRegions %>" codeId = "ID"  codeName = "NM"  defaultValue= "[Choose One]" />                                                                  
	        	                </td>
            		        </tr>
							<tr>
								<td height="25" align="right" class="RightTableCaption"><fmtSalesConsign:message key="LBL_UNIT_DOLLAR"/>:</td>
								<td>&nbsp;&nbsp;<select name="Cbo_Type" id="Type" class="RightText" >
									 <option value="0" ><fmtSalesConsign:message key="OPT_UNIT"/></option>
									 <option value="1" ><fmtSalesConsign:message key="OPT_DOLLAR"/></option>
									</select>									 
								</td>
							</tr>
						</table>					
					</td>					
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table cellspacing="0" cellpadding="0" width="100%" border="0">		
				<tr>
					<td colspan="2"> <jsp:include page="/include/GmFromToDateConsign.jsp" />  </td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<table cellspacing="0" cellpadding="0" width="100%" border="0">	
				<tr>	
<% 
	/* //Cross Tab column headers from Properties
	String strLblName = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("CT_NAME"));
	String strLblTotal = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("CT_TOTAL"));
	String strLblCons = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("CT_CONS"));
	String strLblListValue = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("CT_LIST_VAL"));
	String strLblSales = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("CT_SALES"));
	String strLblUnit = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("CT_UNIT"));
	String strLblAvgSales = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("CT_AVG_SALES"));
	String strLblAvgUsgae = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("CT_AVG_USAGE"));
 */
	gmCrossTab.setLinkRequired(true);	// To Specify the link
	
	// To specify if its unit or valye 
	gmCrossTab.setValueType(Integer.parseInt(strType));
	
	// Add Lines
	gmCrossTab.setColumnLineStyle("borderDark");
	gmCrossTab.addLine("Name");
	gmCrossTab.addLine("List Value");
	gmCrossTab.addLine("Avg. Usage");
	gmCrossTab.displayZeros(true);
	// Setting Display Parameter
	gmCrossTab.setAmountWidth(70);
	gmCrossTab.setNameWidth(350);
	gmCrossTab.setRowHeight(22);
	gmCrossTab.setLinkRequired(true);
	gmCrossTab.setNoDivision(true);
	gmCrossTab.setTotalRequired(false);
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	gmCrossTab.setFixedHeader(true);
	
	//Setting drilldown information 
	gmCrossTab.setLinkType(strAction);
	alDrillDown.add("LoadGCONS");	
	if(strAction.equals("LoadDistributor"))	
	 {
        gmCrossTab.setLinkRequired(false);
	   	alDrillDown.remove("LoadGCONS");	
	 }
	gmCrossTab.setDrillDownDetails(alDrillDown);
	
	// Page Body Style
	gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD");
	gmCrossTab.addStyle("Name","ShadeMedGrayTD","ShadeDarkGrayTD");
	gmCrossTab.addStyle("Total","ShadeMedBlueTD","ShadeDarkBlueTD") ;
	gmCrossTab.addStyle("Cons # ","ShadeMedGreenTD","ShadeDarkGreenTD");
	gmCrossTab.addStyle("List Value","ShadeLightGreenTD","ShadeDarkGreenTD") ;
	gmCrossTab.addStyle("Sales","ShadeMedOrangeTD","ShadeDarkOrangeTD");
	gmCrossTab.addStyle("Unit","ShadeLightOrangeTD","ShadeDarkOrangeTD") ;
	gmCrossTab.addStyle("Avg. Sales","ShadeMedOrangeTD","ShadeDarkOrangeTD");
	gmCrossTab.addStyle("Avg. Usage","ShadeLightOrangeTD","ShadeDarkOrangeTD") ;
	
	gmCrossTab.addSkip_Div_Round("Cons # ");
	gmCrossTab.addSkip_Div_Round("Unit");
	
	gmCrossTab.addDivMandatory("Sales");
	gmCrossTab.addDivMandatory("List Value");
	gmCrossTab.addDivMandatory("Avg. Sales");

	gmCrossTab.add_RoundOff("Sales","2");
	gmCrossTab.add_RoundOff("List Value","2");
	gmCrossTab.addSkip_Div("Avg. Usage");
	gmCrossTab.add_RoundOff("Avg. Usage","1");
	gmCrossTab.add_RoundOff("Avg. Sales","2");
	
	gmCrossTab.setSortRequired(true);
	gmCrossTab.addSortType("Name","String");
	
	gmCrossTab.setSortColumn(intSortColumn);
	gmCrossTab.setSortOrder(intSortOrder);
	gmCrossTab.setRowHighlightRequired(true);
	
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	

%>	
	
					<td align=center colspan="4"> <%=gmCrossTab.PrintCrossTabReport(hmReturn) %></td>
				</tr>
			</table>	
		</td>
	</tr>
	<tr><td class="Line" height=1 colspan="2"></td></tr>
</table>
	
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>
<%@ include file="/common/GmFooter.inc" %> 
</HTML>

