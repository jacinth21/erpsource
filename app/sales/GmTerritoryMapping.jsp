 <%
/**********************************************************************************
 * File		 		: sales/GmTerritoryMapping.jsp
 * Desc		 		: This screen is used to display Set Rep By Account now modified 
 *					  to display group report
 * Version	 		: 1.1
 * author			: RichardK
************************************************************************************/
%>
<%@ page language="java" %>
<%@include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- sales\GmTerritoryMapping.jsp -->
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtTerritoryMapping" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtTerritoryMapping:setLocale value="<%=strLocale%>"/>
<fmtTerritoryMapping:setBundle basename="properties.labels.sales.GmTerritoryMapping"/>
<%
String strSalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");
try {
	String strWikiTitle = GmCommonClass.getWikiTitle("QUOTAREPORT_BREAKUP");

	GmServlet gm 			= new GmServlet();
	HashMap hmReturn 		= new HashMap();
	ArrayList alProjList 	= new ArrayList();
	ArrayList alStyle 		= new ArrayList();
	ArrayList alDrillDown 	= new ArrayList();
	
	String strSelected = "";
	String strCodeID = "";
	String strProjNm = "";
	String strProjId = "";
	
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	
	boolean bolAccess = true;
	
	gm.checkSession(response,session);

	String strShowArrow = GmCommonClass.parseZero((String)request.getAttribute("Chk_ShowArrowFl"));
	String strShowPercent = GmCommonClass.parseZero((String)request.getAttribute("Chk_ShowPercentFl"));
	String strHideAmount = GmCommonClass.parseZero((String)request.getAttribute("Chk_HideAmountFl"));
	
	// Combobox default value
	String strHeader = (String)request.getAttribute("strHeader");
	String strhAction = (String)request.getAttribute("hAction");
	
	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}
	
	strhAction = (strhAction == null)?"Load":strhAction;
	
	//Below code is to track down the visits on the report.
	String strUserId = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
	String strShUserName = GmCommonClass.parseNull((String)session.getAttribute("strSessShName"));
	String strUserName = strUserId + " - " + strShUserName;
	//Visit code ends
	
	// To get the Project Combo Box value information 
	HashMap hmComonValue = (HashMap)request.getAttribute("hmComonValue");
	if (hmComonValue != null)
	{
		alProjList = (ArrayList)hmComonValue.get("PROJECTS");
	}
		
	// To Generate the Crosstab report
	gmCrossTab.setHeader("Sales By Distributor By Month");
	
	gmCrossTab.setLinkType(strhAction);
	gmCrossTab.setLinkRequired(true);	// To Specify the link
	
	// To display percent changed information
	if (strShowPercent.equals("1"))
	{
		gmCrossTab.setReportType(gmCrossTab.CROSSTAB_SALE_PER);	
	}	
	if (strShowArrow.equals("1") )
	{
		gmCrossTab.setShowArrow(true);; // To Display % arrow information
	}

	if (strHideAmount.equals("1"))
	{
		gmCrossTab.setHideAmount(true);	
	}	

	// To specify if its unit or value defaults maps to dollar 
	gmCrossTab.setValueType(1);
	// Setting Line before print
	gmCrossTab.addLine("Name");	
	
	
	// Line Style information
	gmCrossTab.setColumnLineStyle("borderDark");


	
		// Setting Display Parameter
	gmCrossTab.setAmountWidth(65);
	gmCrossTab.setNameWidth(320);
	gmCrossTab.setRowHeight(22);
	gmCrossTab.displayZeros(true);
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	
	// Setting the cross tab style 
	// Page Body Style
	gmCrossTab.setGeneralHeaderStyle("aaTopHeader");
	gmCrossTab.addStyle("Name","ShadeMedGrayTD","aaTopHeader");
	gmCrossTab.addStyle("Total","ShadeMedBlueTD","ShadeDarkBlueTD") ;
	
	
	// Setting for hierarchy color
	alStyle.add("ShadeLightBlueTD");
	alStyle.add("ShadeLevelI");
	alStyle.add("ShadeLevelII");
	gmCrossTab.setMultiColumnStyle(alStyle);
	
	//Setting drilldown information 
	alDrillDown.add("Account"); // Maps to Account drilldown
	gmCrossTab.setDrillDownDetails(alDrillDown);
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: YTD #</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strSalesJsPath%>/GmSalesFilter.js"></script>
<script> downloadPiwikJS();</script>
<script> fnTracker('<%=strHeader %>', '<%=strUserName%>');</script>
<script>
var vaction = '<%=strhAction%>';
function fnSubmit()
{
	document.frmOrder.submit();
}

function fnGo()
{
	var haction = document.frmOrder.hAction.value;
	var actionURL = '<%=strServletPath%>/GmTerritoryMappingServlet?strOpt='+haction;
	fnReloadSales(haction,actionURL);
	//document.frmOrder.submit();
}
function fnCallAccount(id, Hierarchy, LinkDetails)
{
	
	if(vaction == 'LoadQuotaVP'){
		Hierarchy++;
	}
	
    if (Hierarchy=='3') 
	{
		windowOpener("<%=strServletPath%>/GmTerritoryServlet?hAction=ShowAcc&hType=VP&hId="+id,"TerrAcc","resizable=yes,scrollbars=yes,top=250,left=250,width=600,height=300");	
	}
	if (Hierarchy=='2') 
	{
		windowOpener("<%=strServletPath%>/GmTerritoryServlet?hAction=ShowAcc&hType=AD&hId="+id,"TerrAcc","resizable=yes,scrollbars=yes,top=250,left=250,width=600,height=300");	
	}
	if (Hierarchy=='1') 
	{
		windowOpener("<%=strServletPath%>/GmTerritoryServlet?hAction=ShowAcc&hType=DIST&hId="+id,"TerrAcc","resizable=yes,scrollbars=yes,top=250,left=250,width=600,height=300");	
	}
	if (Hierarchy=='0') 
	{
		windowOpener("<%=strServletPath%>/GmTerritoryServlet?hAction=ShowAcc&hType=TERR&hId="+id,"TerrAcc","resizable=yes,scrollbars=yes,top=250,left=250,width=600,height=300");	
	}	
}

function fnCallDetail(val)
{
	document.frmOrder.SetNumber.value = val;
	document.frmOrder.action = "<%=strServletPath%>/GmSalesYTDReportServlet";
	document.frmOrder.hAction.value = "LoadAccount";
	document.frmOrder.submit();
}
function fnLoad()
{
	document.frmOrder.Cbo_FromMonth.value = '<%=request.getAttribute("Cbo_FromMonth")%>';
	document.frmOrder.Cbo_ToMonth.value = '<%=request.getAttribute("Cbo_ToMonth")%>';
	document.frmOrder.Cbo_FromYear.value = '<%=request.getAttribute("Cbo_FromYear")%>';
	document.frmOrder.Cbo_ToYear.value = '<%=request.getAttribute("Cbo_ToYear")%>';
	
	document.frmOrder.Chk_ShowPercentFl.checked = <%=(strShowPercent == "0")?"false":"true"%>;
	document.frmOrder.Chk_ShowArrowFl.checked = <%=(strShowArrow == "0")?"false":"true"%>;
	document.frmOrder.Chk_HideAmountFl.checked = <%=(strHideAmount == "0")?"false":"true"%>;
}

function Toggle(val)
{
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);
	
	if (obj.style.display == 'none')
	{
		obj.style.display = '';
	}
	else
	{
		obj.style.display = 'none';
	}
}


</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad()">
<FORM name="frmOrder" method="post" action = "<%=strServletPath%>/GmTerritoryMappingServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<TABLE cellSpacing=0 cellPadding=0  border=0 width="100%">
<TBODY>
	<tr>
		<td rowspan="10" class=Line noWrap width=1></td>
		<td class=Line noWrap height=1></td>
		<td rowspan="10" class=Line noWrap width=1></td>
	</tr>
	<tr>
		<td>
			<TABLE cellSpacing=0 cellPadding=0  border=0 width="100%">
				<tr class=Line>
					<td height=25 class=RightDashBoardHeader><%=strHeader %> (in 000's)</td>
					<td  height="25" class="RightDashBoardHeader">
						<fmtTerritoryMapping:message key="IMG_ALT_HELP" var = "varHelp"/>
						<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
				</tr>
			</TABLE>
		</td>
	</tr>	
	
		<tr>
			<td colspan="5" height="20">
			<jsp:include page="/sales/GmSalesFilters.jsp" >
				<jsp:param name="FRMNAME" value="frmOrder" />
				<jsp:param name="HACTION" value="<%=strhAction%>"/>
				<jsp:param name="HIDE" value="SYSTEM" />				
			</jsp:include>
			</td>
		</tr>		

	<tr class=Line><td noWrap height=1></td></tr>
	<!-- Addition Filter Information -->
	<tr>
		<td><jsp:include page="/common/GmCrossTabFilterInclude.jsp">
				<jsp:param name="HACTION" value="<%=strhAction%>"/>
			</jsp:include>
		  </td>
	</tr>
		<!-- Screen Level Filter	-->
	<tr class=borderDark>
		<td noWrap height=1>  </td>
	</tr>
	<tr>
		<td noWrap height=3>  </td>
	</tr>	
	<tr>
		<td align=center> <%=gmCrossTab.PrintMultiLevelCrossTabReport(hmReturn) %></td>
	</tr>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>
<%@ include file="/common/GmFooter.inc"%>
</HTML>
