 <%
/**********************************************************************************
 * File		 		: GmSalesConsignPartSearch.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Richard 
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.Math" %>

<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<!-- WEB-INF path corrected for JBOSS migration changes -->

 <!-- \sales\AreaSet\GmAddInfo.jsp -->

<%@ include file="/common/GmHeader.inc" %>


<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<%@ taglib prefix="fmtSalesConsignPartSearch" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!--sales\GmSalesConsignPartSearch.jsp  -->
<fmtSalesConsignPartSearch:setLocale value="<%=strLocale%>"/>
<fmtSalesConsignPartSearch:setBundle basename="properties.labels.sales.GmSalesConsignPartSearch"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	
	String strAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strOpt = (String)request.getAttribute("hOpt")==null?"":(String)request.getAttribute("hOpt");
	HashMap hmParam = (HashMap)request.getAttribute("hmParam");
	
	
	
	String strhSetIds 	= "";
	String strDist 		= "";
	String strFrmDt 	= "";
	String strToDt 		= "";

	ArrayList alDistributor = new ArrayList();
	ArrayList alSets = new ArrayList();

	strDist = GmCommonClass.parseNull( request.getParameter("Cbo_DistId")) ;
	strhSetIds = GmCommonClass.parseNull( request.getParameter("hSetIds")) ;
	int intSetLength = 0;
	int intDistLength = 0;

	if (hmReturn != null) 
	{
		alSets = (ArrayList)hmReturn.get("SETMASTERLIST");
		alDistributor = (ArrayList)hmReturn.get("DISTRIBUTORLIST");

		if (!alSets.isEmpty())
		{
			intSetLength = alSets.size();
		}

	}

	HashMap hmLoop = new HashMap();

	String strConId = "";
	String strSetName = "";
	String strCategory = "";
	String strShipDate = "";
	String strTrack = "";
	String strVal = "";

	String strShade = "";

	HashMap hmMapSets = new HashMap();
	hmMapSets.put("ID","");
	hmMapSets.put("PID","ID");
	hmMapSets.put("NM","NAME");	
	
	String strType = GmCommonClass.parseZero(request.getParameter("Cbo_Type"));
	
	String strQtyConsgn="" ; 
	String strListVal="";
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Report- Sales Consignments Search</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>
function fnSetUtilization()
{
	document.frmAccount.hAction.value = 'LoadPart';
	var hSetId = document.frmAccount.hSetIds.value;
	var distributorID = document.frmAccount.Cbo_DistId.value;
	document.frmAccount.action ="<%=strServletPath%>/GmSalesConsignDetailsServlet?hSetId="+hSetId+"&DistributorID="+distributorID;
	document.frmAccount.submit();
}

function fnViewPartActual(val, did)
{
	document.frmAccount.hPartID.value = val;
	document.frmAccount.Cbo_DistId.value = did;
	document.frmAccount.hOpt.value = 'BYPARTDETAIL';
	
	document.frmAccount.action ="<%=strServletPath%>/GmSalesConsignSearchServlet";
	document.frmAccount.hAction.value = "Reload";
	document.frmAccount.submit();
	
}
function fnLoadPart(val)
{
	document.frmAccount.hOpt.value = 'BYSETPART';
	document.frmAccount.action ="<%=strServletPath%>/GmSalesConsignSearchServlet";
	document.frmAccount.submit();
}
function fnViewPartCrossOver(Pval)
{
	document.frmAccount.hPartID.value = Pval;
	document.frmAccount.hOpt.value = 'BYPARTCROSSOVER';
	
	document.frmAccount.action ="<%=strServletPath%>/GmSalesConsignSearchServlet";
	document.frmAccount.hAction.value = "Reload";
	document.frmAccount.submit();
	
}

function fnCallDiv(val,flag,etch)
{
	document.frmAccount.hConsignId.value = val;
	document.frmAccount.hFlagId.value = flag;
	document.frmAccount.hEtchId.value = etch;
}

function fnOpenTrack(val)
{
	windowOpener('https://www.fedex.com/fedextrack?ascend_header=1&clienttype=dotcom&cntry_code=us&language=english&tracknumbers='+val,"Track","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
}
function fnPrintConsignVer(val)
{
	windowOpener("/GmConsignSetServlet?hAction=LoadConsign&hId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintReturnVer(val)
{
	windowOpener("/GmReportCreditsServlet?hAction=PrintVersion&hId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}



// Marks the checkboxes as 'checked'
function fnCheckSelections(val,objgrp)
{
	var valobj = val.split(",");
	var sval = '';
	var arrlen = objgrp.length;
	
	if (arrlen > 0)
	{
		for (var j=0;j< valobj.length;j++ )
		{
			sval = valobj[j];
			for (var i=0;i< arrlen;i++ )
			{
				if (objgrp[i].value == sval)
				{
					objgrp[i].checked = true;
					break;
				}
			}
		}
	}
	else
	{
		objgrp.checked = true;
	}
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmSalesConsignSearchServlet">
<input type="hidden" name="hOpt" value="<%=strOpt%>">
<input type="hidden" name="hSetIds" value="<%=strhSetIds%>">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="Cbo_DistId" value="<%=strDist%>">
<input type="hidden" name="hPartID" value="">

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtSalesConsignPartSearch:message key="LBL_CONSIG_DETAILS"/></span>			
			</td>
		</tr>
		<tr><td class="Line" height="1"></td></tr>
		<tr><td height=5>  </td></tr>	
		<tr>
			<td>
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
				<jsp:include page="/sales/GmSalesFilterDetails.jsp" />		
			</td>		
		</tr>
		<tr><td height=5>  </td></tr>	
		<tr><td class="Line" height="1"></td></tr>
		<tr>
			<td>
			<%  if ( strOpt.equals("BYPART") ) 
				{
			%>
				 <display:table name="requestScope.results.rows" class="its" export="false"  varTotals="totals" 
					cellpadding="0" cellspacing="0" decorator="com.globus.displaytag.beans.DTSalesVPartDetailWrapper" > 
				    <fmtSalesConsignPartSearch:message key="DT_PART" var="varPart"/><display:column property="PID" title="${varPart}" sortable="true" style="text-align: left;height=20"  />
				   <fmtSalesConsignPartSearch:message key="DT_PART_DESC" var="varPartDesc"/> <display:column property="PDESC" title="${varPartDesc}" sortable="true" />
				   <display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				  <fmtSalesConsignPartSearch:message key="DT_QTY" var="varQty"/> <display:column property="TCOUNT" title="${varQty}" sortable="true" total="true" style="text-align: right;" />
				  <fmtSalesConsignPartSearch:message key="DT_PRICE" var="varPrice"/>  <display:column property="PRICE" title="${varPrice}" sortable="true" style="text-align: right;" 
				    format="{0,number,$#,###,###.00}" /> 
					<fmtSalesConsignPartSearch:message key="DT_TOT_PRICE" var="varTotPrice"/><display:column property="TPRICE" title="${varTotPrice}" sortable="true"  format="{0,number,$#,###,###.00}" 					
						style="text-align: right;" />
				<display:footer media="html"> 
					<tr><td class="Line" colspan="6"></td></tr>
					<tr>
						<td height="20" colspan="2"></td>
						<td width="1"></td>
						<td colspan="3"></td>
					</tr>
				</display:footer>				
				 </display:table> 
			<% }else if ( strOpt.equals("BYPARTDETAIL") ) 
			   {
			%>
				 <display:table name="requestScope.results.rows" class="its" export="false"  varTotals="totals" 
					cellpadding="0" cellspacing="0" decorator="com.globus.displaytag.beans.DTSalesVirtualDetailWrapper" defaultsort="4"> 
				    <display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				    <fmtSalesConsignPartSearch:message key="DT_CONSIG_ID" var="varConsigId"/><display:column property="ID" title="${varConsigId}" sortable="true" style="text-align: left;height=20" />
				    <fmtSalesConsignPartSearch:message key="DT_SET_ID" var="varSetId"/><display:column property="SETID" title="${varSetId}" sortable="true" style="text-align: center;" />	
   				   <fmtSalesConsignPartSearch:message key="DT_CLASS" var="varClass"/> <display:column property="SETORITEM" title="${varClass}" group="1" sortable="true" style="text-align: center;" />
				    <fmtSalesConsignPartSearch:message key="DT_QTY_CONSIG" var="varQtyConsig"/><display:column property="QTY" title="${varQtyConsig}" sortable="true" total="true" style="text-align: center;" />
					<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				   <fmtSalesConsignPartSearch:message key="DT_SHIP_DT" var="varShipDt"/> <display:column property="DDATE" title="${varShipDt}" sortable="true" style="text-align: left;" />				     	
					<fmtSalesConsignPartSearch:message key="DT_TRACKING" var="varTracking"/><display:column property="TRACK" title="${varTracking}" sortable="true" style="text-align: left;" />
				 </display:table> 					

				 
			<% } else if ( strOpt.equals("BYSETPART") )
			{ %>	 
				 <display:table name="requestScope.results.rows" class="its" export="false" freezeHeader="true" varTotals="totals"   
					cellpadding="0" cellspacing="0" decorator="com.globus.displaytag.beans.DTSalesVPartDetailWrapper">
					<fmtSalesConsignPartSearch:message key="DT_PART" var="varPart"/><display:column property="PID" title="${varPart}" sortable="true" style="text-align: left; height=20"  />
				    <fmtSalesConsignPartSearch:message key="DT_PART_DESC" var="varPartDesc"/><display:column property="PDESC" title="${varPartDesc}" sortable="true" style="text-align: left;" />
				    <fmtSalesConsignPartSearch:message key="DT_PROD_FAM" var="varProdFam"/><display:column property="PRODFAMILY" title="${varProdFam}" sortable="true" style="text-align: left;"  />
				   <fmtSalesConsignPartSearch:message key="DT_SET_QTY" var="varSetQty"/> <display:column property="SQTY" title="${varSetQty}" sortable="true" style="text-align: right;" />
					<fmtSalesConsignPartSearch:message key="DT_QTY_CONSIG" var="varQtyConsig"/><display:column property="SETUSAGE" title="${varQtyConsig}" sortable="true" total="true" style="text-align: center;" />
				    <fmtSalesConsignPartSearch:message key="DT_LIST_VAL" var="varListVal"/><display:column property="LISTVALUE" title="${varListVal}" sortable="true"  total="true" style="text-align: right;"  
						format="{0,number,$#,###,###.00}" />												
				<display:footer media="html"> 
					<tr><td class="Line" colspan="6"></td></tr>
					<tr>
						<td class = "ShadeDarkGrayTD" colspan = "1"></td>
						<td class = "ShadeDarkGrayTD" ></td>
						<td class = "ShadeDarkGrayTD" ></td>
						<td class = "ShadeDarkGrayTD" align = "right"><B>&nbsp;&nbsp;<fmtSalesConsignPartSearch:message key="LBL_TOTAL"/> </B></td>
						<%
							strQtyConsgn 	= ((HashMap)pageContext.getAttribute("totals")).get("column5").toString();
						 	strListVal 	= ((HashMap)pageContext.getAttribute("totals")).get("column6").toString();
						%>
						<td class = "ShadeDarkGrayTD" style="text-align: center;" ><B><%= Math.round(Double.parseDouble(strQtyConsgn))%></B></td>
						<td class = "ShadeDarkGrayTD" style="text-align: right;" ><B>$<%= GmCommonClass.getStringWithCommas(strListVal, 2) %></B></td>						
					</tr>
					<tr><td class="Line" colspan="6"></td></tr>
				</display:footer>								
			</display:table> 
			<%
			} else if ( strOpt.equals("BYPARTCROSSOVER") ) 
			{ %>
				 <display:table name="requestScope.results.rows" class="its" export="false" cellpadding="0" cellspacing="0"> 
				    <fmtSalesConsignPartSearch:message key="DT_SET_ID" var="varSetId"/><display:column property="SID" title="${varSetId}" sortable="true" style="text-align: left;height=20" />
				    <fmtSalesConsignPartSearch:message key="DT_SET_NAME" var="varSetName"/><display:column property="SNM" title="${varSetName}" sortable="true" style="text-align: left;" />
				   <display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				   <fmtSalesConsignPartSearch:message key="DT_SET_QTY" var="varSetQty"/><display:column property="SQTY" title="${varSetQty}" sortable="true" style="text-align: right;" />
				   <fmtSalesConsignPartSearch:message key="DT_SET_COUNT" var="varSetCount"/><display:column property="SETCOUNT" title="${varSetCount}" sortable="true" style="text-align: right;" />
				   <fmtSalesConsignPartSearch:message key="DT_SET_PART_COUNT" var="varSetPartCount"/><display:column property="SETPARTCOUNT" title="${varSetPartCount}" sortable="true" style="text-align: right;" />				 
				<display:footer media="html"> 
					<tr><td class="Line" colspan="6"></td></tr>
					<tr>
						<td height="20" colspan="2"></td>
						<td width="1"></td>
						<td colspan="3"></td>
					</tr>
				</display:footer>				
				</display:table> 
			<% }%>
			</td>
		</tr>
		<tr><td class="Line" height="1"></td></tr>
		
	</table>


</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
	