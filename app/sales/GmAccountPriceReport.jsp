<%@page import="java.util.Calendar"%>
<%
/**********************************************************************************
 * File		 		: GmAccountPriceReport.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
 <%@page import="java.util.Date"%>
<%@page import="com.globus.valueobject.common.GmDataStoreVO"%> 
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@ page import ="com.globus.common.beans.GmCommonClass,org.apache.commons.beanutils.RowSetDynaClass"%>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtAccountPriceReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>


<!-- sales\GmAccountPriceReport.jsp -->

<fmtAccountPriceReport:setLocale value="<%=strLocale%>"/>
<fmtAccountPriceReport:setBundle basename="properties.labels.sales.GmAccountPriceReport"/>


<%

	
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	log = GmLogger.getInstance(GmCommonConstants.SALES);
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");
	//String strTimeZone = strGCompTimeZone;
	//String strApplCurrFmt = strGCompDateFmt;
	//Cannot  use the above since the format from session is #,###,##0.00
	String  strApplCurrFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplCurrFmt"));
	// Need to be display dynamic Year in XLS button.
	Calendar cal = Calendar.getInstance(); 
	int intCurrYear = cal.get(Calendar.YEAR);
	String strOpt = (String)request.getAttribute("hOpt");
	String strWikiTitle = GmCommonClass.getWikiTitle("ACCOUNTS_PRICING_LIST");
	String strCurrSign = GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb"));
	String strCurrPos = GmCommonClass.parseNull(GmCommonClass.getString("CURRPOS"));
	String strCurrFmt = "";
	String strAccCurrSymb = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_SYMB"));
	strCurrSign = strAccCurrSymb.equals("")?strCurrSign:strAccCurrSymb;
	GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.sales.GmAccountPriceReport", strSessCompanyLocale);
	if(strCurrPos.equalsIgnoreCase("RIGHT")){
		strCurrFmt = "{0,number,"+(strApplCurrFmt+" "+ strCurrSign)+"}";
	}else{
		strCurrFmt = "{0,number,"+(strCurrSign+" "+strApplCurrFmt)+"}";
	}
	String strPriceIncrTitle = "";
	String strDiscount = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_DISC"));
	String strTitleT1 = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_T1"))+ strCurrSign;  
	String strTitleT2 = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_T2"))+ strCurrSign;
	String strTitleT3 = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_T3"))+ strCurrSign;
	String strTitleT4 = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_T4"))+ strCurrSign;
	String strListPriceTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_LIST_PRICE"))+" " +strCurrSign;
	String strUnitPriceTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_UNIT_PRICE"))+ " " +strCurrSign;
	if(strOpt.equals("CurrentPrice")){
		strUnitPriceTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PRICE"))+ " " +strCurrSign;
	}else if(strOpt.equals("PriceIncrease")){
		strUnitPriceTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_CURRENT_PRICE"))+ " " +strCurrSign;
		strPriceIncrTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PRICE_INCREASE"))+ " " +strCurrSign;
	}
	int intSize = 0;
	HashMap hmLoop = new HashMap();
	HashMap hcboVal = null;
	String strCustServiceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strsalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");
	String strPartNum = "";
	String strLikeOption = "";
	String strCodeID = "";
	String strDistId = "";
	String strSelected = "";
	String strAccId =  GmCommonClass.parseNull((String)request.getAttribute("hAccId"));
	String strProjectIds = GmCommonClass.parseNull((String)request.getAttribute("hProjectIds"));
	String strHAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strChkSelectAll = GmCommonClass.parseNull((String)request.getAttribute("CHKSELECTALL"));
	String strQueryBy = GmCommonClass.parseNull((String)request.getAttribute("hQueryBy"));
	String strChkAll =strChkSelectAll.equals("")?"":"checked";
	String strAccNm =  GmCommonClass.parseNull((String)request.getAttribute("hAccNm"));
	//String strAccNm ="";
	String strOrdId = "";
	String strFormattedOrderId = "";
	String strOrdDt = "";
	String strPrice = "";
	String strOrdBy = "";
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strItems = "";
	String strStatusFl = "";
	String strTrackNum = "";
	String strPO = "";
	String strInvId = "";
	String strShipToDate = "";
	String strOrderType = "";
	String strFrmDate = (String)request.getAttribute("hFrom")==null?"":(String)request.getAttribute("hFrom");
	String strToDate = (String)request.getAttribute("hTo")==null?"":(String)request.getAttribute("hTo");
	String strParentOId = "";
	
	String strPercentIncrease="6"  ;
	int   intDefaultColSpan = 6 ;
	String strDefaultTableClass = "DtTable1000";
	
	String strShade = "";
	
	String strProjId = "";
	String strOrdCallFlag = "";
	String strInvCallFlag = "";
	String strParam = (String)request.getAttribute("hMode");
	ArrayList alProject = new ArrayList();
	ArrayList alSystem = new ArrayList();
	ArrayList alDistributor = new ArrayList();
	ArrayList alAccount = new ArrayList();
	ArrayList alAccountOrder = new ArrayList();
	ArrayList alProjSys = new ArrayList();
	ArrayList alLikeOptions = new ArrayList();

	int intLength = 0;
	int intSetLength = 0;

	HashMap hmReturn = new HashMap();
	HashMap hmAccList = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	alAccount = (ArrayList)hmReturn.get("ACCOUNTLIST");
	alLikeOptions = (ArrayList)hmReturn.get("LIKEOPTIONS");
	
	if (hmReturn != null) 
	{
		alProject = (ArrayList)hmReturn.get("PROJECTLIST");
		alSystem = (ArrayList)hmReturn.get("SYSTEMLIST");
		hmAccList = (HashMap)hmReturn.get("ACCDETAIL");
		strPartNum = GmCommonClass.parseNull((String)request.getAttribute("hPartNum"));
		strLikeOption = GmCommonClass.parseNull((String)request.getAttribute("hLikeOption"));
		if(strQueryBy.equals("Project Name"))
			alProjSys = alProject;
		else
			alProjSys = alSystem;
		if (!alProject.isEmpty())
		{
			intSetLength = alProject.size();
		}

	}
	
	
	RowSetDynaClass resultSet = (RowSetDynaClass) request.getAttribute("ACCPRICEREPORT");
	int intNRows=0;
	
	if(resultSet != null) {
		intNRows = ( (ArrayList)resultSet.getRows() ).size();
	}
	

	HashMap hmMapProject = new HashMap();
	hmMapProject.put("ID","");
	hmMapProject.put("PID","ID");
	hmMapProject.put("NM","NAME");	
	
	
	intDefaultColSpan = ( strOpt.equals("PriceIncrease") ? 6 : intDefaultColSpan  );
	strDefaultTableClass = ( strOpt.equals("PriceIncrease") ? "DtTable850" : strDefaultTableClass  );
	
	strPercentIncrease = (String)request.getAttribute("hPerIncrease");
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Account Pricing Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/PhoneOrder.js"></script>
<script language="JavaScript" src="<%=strCustServiceJsPath%>/GmParentRepInfo.js"></script>
<script language="javascript" src="<%=strsalesJsPath%>/GmAccountPriceReport.js"></script>

<script>
var totalProject = 0;
var strServletPath = '<%=strServletPath%>';
var intSetLen = <%=intSetLength%>;
var projectIds = "<%= strProjectIds.replaceAll("'","")%>" ;
var type = "Account";
var enablePriceLabel = 'N';
var projLen = <%=alProject.size()%>;
var systemLen = <%=alSystem.size()%>;

<%
 intSize = alProject.size();
hcboVal = new HashMap();
for (int i=0;i<intSize;i++)
{
	hcboVal = (HashMap)alProject.get(i);
%>
var projArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>

<%
intSize = alSystem.size();
hcboVal = new HashMap();
for (int i=0;i<intSize;i++)
{
	hcboVal = (HashMap)alSystem.get(i);
%>
var systemArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>
</script>
 
    <style>
        .AcctPricingTab tbody{
          width: 100%;
          display: table;  
        }  
 </style>
</HEAD>

<BODY leftmargin="20" topmargin="10"   onload="javascript:fnLoad();" onkeyup="fnEnter();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmSalesAcctPriceReportServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hMode" value="">
<input type="hidden" name="hProjIds" value="">
<input type="hidden" name="hOpt" value="<%=strOpt%>">
<input type="hidden" name="hAccId" value="<%=strAccId %>">
<input type="hidden" name="hLen" value="<%=intLength%>">
<input type="hidden" name="hQueryBy" value="<%=strQueryBy %>">
<input type="hidden" name="hAccNm" id="hAccNm" value="<%=strAccNm %>">



<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table  class="<%=strDefaultTableClass %>" cellspacing="0" cellpadding="0">
		<tr>
		<!--	<td rowspan="6" width="1" class="Line"></td>
			<td rowspan="6" width="1" class="Line"></td> -->
			<td colspan="<%= intDefaultColSpan%>" class="line"></td>
		</tr>
		<tr>
			<td colspan="<%= intDefaultColSpan%>"  >
				<table cellspacing="0" cellpadding="0">
				<tr>
				<td height="20" colspan = "<%= intDefaultColSpan%>" class="RightDashBoardHeader"><fmtAccountPriceReport:message key="LBL_ACCOUNTS_PRICING_LIST"/></td>
				<td align="right" class=RightDashBoardHeader style="display: none;"> 	
				<fmtAccountPriceReport:message key="IMG_ALT_HELP" var = "varHelp"/>
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
				</tr>
				</table>
			</td>	
		</tr>
		<tr><td colspan="<%= intDefaultColSpan%>" width="1" class="Line"></td></tr>
		<tr>
			<td width="375" class="RightTableCaption" HEIGHT="24">&nbsp;<fmtAccountPriceReport:message key="LBL_ACCOUNT_ID"/>:&nbsp;<input type="text" size="8" value="" name="Txt_AccId" class="InputArea" tabindex=1 onFocus="changeBgColor(this,'#AACCE8');" onBlur="javascript:fnCallAcIdBlur(this)"> &nbsp;&nbsp;<span class="RightTextBlue"></span></td>
			<td width="375"></td>
		</tr>
		<tr><td colspan="<%= intDefaultColSpan%>" class="LLine"></td></tr>
		<tr>
			<td colspan="<%= intDefaultColSpan%>">
				<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" id="tabAcSum" class= "AcctPricingTab" style="display:none">
					<tr class="Shade" height="25">
						<td class="RightTableCaption" align="Right"><fmtAccountPriceReport:message key="LBL_REGION"/>:</td>
						<td id="Lbl_Regn" class="RightText"></td>
						<td class="RightTableCaption" align="Right"><fmtAccountPriceReport:message key="LBL_AREA_DIRECTOR"/>:</td>
						<td id="Lbl_AdName" class="RightText"></td>
						<td class="RightTableCaption" align="Right"><fmtAccountPriceReport:message key="LBL_DISTRIBUTOR"/>:</td>
						<td id="Lbl_DName" class="RightText"></td>
					</tr>
					<tr><td colspan="6" class="LLine"></td></tr>
					<tr height="25">
						<td class="RightTableCaption" align="Right"><fmtAccountPriceReport:message key="LBL_TERITORY"/>:</td>
						<td id="Lbl_TName" class="RightText"></td>
						<td class="RightTableCaption" align="Right"><fmtAccountPriceReport:message key="LBL_REP"/>:</td>
						<td id="Lbl_RName" class="RightText"></td>
						<td class="RightTableCaption" align="Right"><fmtAccountPriceReport:message key="LBL_GRP_PRICE_BOOK"/>:</td>
						<td id="Lbl_PName" class="RightText"></td>
					</tr>
					<tr><td colspan="6" class="LLine"></td></tr>
					 <!-- MNTTASK:6074 - Account Affiliation Start code --> 
					  <tr class="Shade" height="25">
						<td class="RightTableCaption" align="Right"><fmtAccountPriceReport:message key="LBL_IDN" var="varIdn"/><gmjsp:label type="RegularText"  SFLblControlName="${varIdn}" td="false"/></td>
						<td id="Lbl_GPRAffName" class="RightText"></td>
						<td class="RightTableCaption" align="Right"><fmtAccountPriceReport:message key="LBL_GPO" var="varGpo"/><gmjsp:label type="RegularText"  SFLblControlName="${varGpo}" td="false"/></td>
						<td id="Lbl_HlthCrAffName" class="RightText"></td>
						<td class="RightTableCaption" align="Right"><fmtAccountPriceReport:message key="LBL_CONTRACT" var="varContract"/><gmjsp:label type="RegularText"  SFLblControlName="${varContract}" td="false"/></td>
						<td id="Lbl_ContractType" class="RightText"></td>
					</tr>
					<!-- Account Affiliation End code -->  
					<tr><td colspan="<%= intDefaultColSpan%>" width="1" class="LLine"></td></tr>
				</table>
			</td>
		</tr>
		
		<tr  >
			<td colspan="6">
				<div id="parentDiv"></div>
			</td>
		</tr>
		<tr class="Shade" >
		
			<td height="25" class="RightTableCaption">&nbsp;<fmtAccountPriceReport:message key="LBL_ACCOUNTS"/>&nbsp; </td>	
			<td class="RightTableCaption">&nbsp;<fmtAccountPriceReport:message key="LBL_QUERY_BY"/>&nbsp;<select name="Cbo_QueryBy" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" onChange="javascript:fnCallType(this);">
					<option value="System Name" ><fmtAccountPriceReport:message key="LBL_SYSTEM_NAME"/>
					<option value="Project Name"><fmtAccountPriceReport:message key="LBL_PRJ_NAME"/></option>
				</select>  </td>		
			<td> </td>
			
			<% if(strOpt.equals("PriceIncrease") || strOpt.equals("CurrentPrice")) {  %>
				<td colspan=3></td>
			<% } %>
						 	
		</tr>

		<tr><td colspan="<%= intDefaultColSpan%>" width="1" class="LLine"></td></tr>
		<tr><td height="3"></td></tr>		
		<tr  >
			<td width="375" class="RightText" >
					 &nbsp;<%--<gmjsp:dropdown controlName="Cbo_AccId"  seletedValue="<%= strAccId %>" 	
					width="350" value="<%= alAccount%>" defaultValue= "[Choose One]"  /> --%>
					 <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
										<jsp:param name="CONTROL_NAME" value="Cbo_AccId" />
										<jsp:param name="METHOD_LOAD" value="loadAccountList&searchType=LikeSearch" />
										<jsp:param name="WIDTH" value="300" />
										<jsp:param name="CSS_CLASS" value="search" />
										<jsp:param name="TAB_INDEX" value="1"/>
										<jsp:param name="CONTROL_NM_VALUE" value="<%=strAccNm %>" /> 
										<jsp:param name="CONTROL_ID_VALUE" value="<%=strAccId %>" />
										<jsp:param name="SHOW_DATA" value="200" />
										<jsp:param name="AUTO_RELOAD" value="fnDisplayAccID(this);" />					
									</jsp:include> 
			</td>
			<td width="325"> <% GmCommonControls.setWidth(275); %> <input type="checkbox" name="Chk_selectAll" <%=strChkAll%> onClick="fnSelectAll(this);">Select All
				<%=GmCommonControls.getChkBoxGroup("",alProjSys,"Proj",hmMapProject,"NM")%> 
			</td>
			
			
			
			
			<%  if(strOpt.equals("PriceIncrease")  )  { %>
				
				<td width=200>&nbsp;&nbsp;&nbsp; <fmtAccountPriceReport:message key="LBL_INCREASE"/></td>				 
				<td>  
				 <input type="text" size="4" value="<%=strPercentIncrease%>%" name="Txt_PerIncrease" class="InputArea" 
				 	onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3>
				</td>
				<td width=100></td>
				
			<% } else {%>
				<input type="hidden" name="Txt_PerIncrease" value="">
			<% }%>	
			
			
			
			<td width="300" align="left"> <fmtAccountPriceReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" buttonType="Load" onClick="fnGo();" tabindex="6" />&nbsp;	</td>
		</tr>
		<tr><td colspan="<%= intDefaultColSpan%>" width="1" class="LLine"></td></tr>
		<tr class="Shade">
			<td class="RightTableCaption" colspan="<%= intDefaultColSpan%>"> &nbsp;<fmtAccountPriceReport:message key="LBL_PART_NUMBER"/>
				<input type="text" size="30" value="<%=strPartNum%>" name="hPartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=4>
				&nbsp;&nbsp; <gmjsp:dropdown controlName="Cbo_Search"  seletedValue="<%= strLikeOption%>" tabIndex="5"  value="<%=alLikeOptions%>" codeId = "CODEID" codeName = "CODENM" /> 
			</td>
     	</tr>
		<tr><td height="2" colspan="<%= intDefaultColSpan%>"></td></tr>
		<tr><td colspan="<%= intDefaultColSpan%>" width="1" class="LLine"></td></tr>
		
		<%
		if (strAction.equals("Reload"))
		{		
%>
		<tr>
			<td colspan = "<%= intDefaultColSpan%>">
			
			<%  if (strOpt.equals("PriceIncrease") ) {  %>
			
				<display:table name="requestScope.ACCPRICEREPORT.rows" class="its" id="currentRowObject" 	 cellpadding="0" cellspacing="0" freezeHeader="true" paneSize="400"> 
				<display:caption media="excel" class="ShadeDarkGrayTD" ><fmtAccountPriceReport:message key="LBL_ACCOUNT_NAME"/> :- <%= hmAccList.get("NAME") %> <fmtAccountPriceReport:message key="LBL_ACCOUNT_ID"/> :- <%= hmAccList.get("ID") %>   <fmtAccountPriceReport:message key="LBL_DISTRIBUTOR"/>  :- <%= hmAccList.get("DISTRIBUTOR") %>   <fmtAccountPriceReport:message key="LBL_DATE"/> :- <%= hmAccList.get("DT") %></display:caption>

				<%  if (strQueryBy.equals("System Name") ) {  %>
				<fmtAccountPriceReport:message key="DT_SYSTEM_NAME" var="varSystemName"/>
				<display:column property="SYSNM" title="${varSystemName}" sortable="true" 	headerClass="ShadeDarkGrayTD" class="ShadeWhite" style="text-align:left"/>
				<%  } else   { %>   
				<fmtAccountPriceReport:message key="DT_PRJ_NAME" var="varPrjName"/>
				<display:column property="PJNM" title="${varPrjName}" sortable="true" 	headerClass="ShadeDarkGrayTD" class="ShadeWhite" style="text-align:left"/>
     			<%	 }  		%>
				
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				<fmtAccountPriceReport:message key="DT_PART" var="varPart"/>
				<display:column property="PID" title="${varPart}" sortable="true"  	headerClass="ShadeDarkGrayTD" class="ShadeWhite" style="text-align: center;"  />
				
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				<fmtAccountPriceReport:message key="DT_PART_DESCRIPTION" var="varPrtDesc"/>
				<display:column property="PDESC" escapeXml="flase" title="${varPrtDesc}" sortable="true" maxWords="30"  headerClass="ShadeDarkGrayTD" class="ShadeWhite" style="text-align:left;width:300px" />
				
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				
				<display:column property="UPRICE" title="<%=strUnitPriceTitle%>" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD" 
					sortable="true" style="text-align: right;" format="<%=strCurrFmt%>" />
			
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				<fmtAccountPriceReport:message key="DT_" var="var"/>
				<display:column property="GPO" title="Group Price Book" media="html" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD" sortable="true" style="text-align: center;"/>
		
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				
				<display:column property="PRICE2008" title="<%=strPriceIncrTitle%>" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD" 
					sortable="true" style="text-align: right;" format="<%=strCurrFmt%>" />
    			
				<display:footer media="html"> 
					<tr><td class="Line" colspan="11"></td></tr>
				</display:footer>
				
				</display:table>
				
			<% } else if (strOpt.equals("CurrentPrice") ) { %>	
			
				<display:table name="requestScope.ACCPRICEREPORT.rows" class="its" id="currentRowObject" 	 cellpadding="0" cellspacing="0" freezeHeader="true" paneSize="400"> 
				<display:caption media="excel" class="ShadeDarkGrayTD" ><fmtAccountPriceReport:message key="LBL_ACCOUNT_NAME"/> :- <%= hmAccList.get("NAME") %> <fmtAccountPriceReport:message key="LBL_ACCOUNT_ID"/> :- <%= hmAccList.get("ID") %>    <fmtAccountPriceReport:message key="LBL_DISTRIBUTOR"/> :- <%= hmAccList.get("DISTRIBUTOR") %>   <fmtAccountPriceReport:message key="LBL_DATE"/> :- <%= hmAccList.get("DT") %></display:caption>
                <%  if (strQueryBy.equals("System Name") ) {  %>
				<fmtAccountPriceReport:message key="DT_SYSTEM_NAME" var="varSysName"/>
				<display:column property="SYSNM" title="${varSysName}" sortable="true" 	headerClass="ShadeDarkGrayTD" class="ShadeWhite" style="text-align:left;width:300px"/>
				<%  } else   { %>   
				<fmtAccountPriceReport:message key="DT_PRJ_NAME" var="varPrjName"/>
				<display:column property="PJNM" title="${varPrjName}" sortable="true" 	headerClass="ShadeDarkGrayTD" class="ShadeWhite" style="text-align:left;width:300px"/>
     			<%	 }  		%>
				
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				<fmtAccountPriceReport:message key="DT_PART" var="varPart"/>
				<display:column property="PID" title="${varPart}" sortable="true"  	headerClass="ShadeDarkGrayTD" class="ShadeWhite" style="text-align: center;"  />
				
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				
				<display:column property="PDESC" escapeXml="false"  title="Part Description" sortable="true" maxWords="30"  headerClass="ShadeDarkGrayTD" class="ShadeWhite" style="text-align:left;width:350px" />
				
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				
				<display:column property="UPRICE" title="<%=strUnitPriceTitle%>" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD" 
					sortable="true" style="text-align: right;" format="<%=strCurrFmt%>" />
				
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
    			<fmtAccountPriceReport:message key="DT_GRP_PRICE_BOOK" var="varGrpBook"/>
    			<display:column property="GPO" title="${varGrpBook}" media="html" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD" sortable="true" style="text-align: center;" />
				<display:footer media="html"> 
					<tr><td class="Line" colspan="11"></td></tr>
				</display:footer>
				</display:table>
			<% } 
			// if thru 'Setup of Account Pricing'
			else if(strOpt.equals("STP")) 
			{
			%> 
				<display:table name="requestScope.ACCPRICEREPORT.rows" class="its" id="currentRowObject" export="true" cellpadding="0" cellspacing="0" > 
				<display:setProperty name="export.excel.filename" value="ICT By Project.xls" />
				<display:caption media="excel" class="ShadeDarkGrayTD" ><fmtAccountPriceReport:message key="LBL_ACCOUNT_NAME"/> :- <%= hmAccList.get("NAME") %> <fmtAccountPriceReport:message key="LBL_ACCOUNT_ID"/> :- <%= hmAccList.get("ID") %> <fmtAccountPriceReport:message key="LBL_DATE"/> :- <%= hmAccList.get("DT") %></display:caption>
				<fmtAccountPriceReport:message key="DT_PRJ_NAME" var="varPrjName"/>
				<display:column property="PJNM" title="${varPrjName}" sortable="true" 
					headerClass="ShadeDarkGrayTD" class="ShadeWhite" />
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				<fmtAccountPriceReport:message key="DT_PART" var="varPart"/>
				<display:column property="PID" title="${varPart}" sortable="true" 
					headerClass="ShadeDarkGrayTD" class="ShadeWhite" style="text-align: center;"  />
				<fmtAccountPriceReport:message key="DT_PART_DESCRIPTION" var="varPrtDesc"/>
				<display:column property="PDESC" escapeXml="false"  title="${varPrtDesc}" sortable="true" maxWords="30"
					headerClass="ShadeDarkGrayTD" class="ShadeWhite" />
				
				<display:column property="TIERI" title="<%=strTitleT1%>"  headerClass="ShadeDarkBlueTD" class="ShadeMedBlueTD" 
					sortable="true" style="text-align: right;" format="<%=strCurrFmt%>" />
				
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				
				<display:column property="TIERII" title="<%=strTitleT2%>" headerClass="ShadeDarkBlueTD" class="ShadeMedBlueTD" 
					sortable="true" style="text-align: right;"  format="<%=strCurrFmt%>" />
				<fmtAccountPriceReport:message key="DT_T2" var="varT"/>
				<display:column property="TIERIID" title="${varT}" headerClass="ShadeDarkBlueTD" class="ShadeLightBlueTD" 
					sortable="true"  style="text-align: right;" format="{0,number,###.##}" />
				
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				
				<display:column property="TIERIII" title="<%=strTitleT3%>" headerClass="ShadeDarkBlueTD"  class="ShadeMedBlueTD"  
					sortable="true" style="text-align: right;" format="<%=strCurrFmt%>" />
				<fmtAccountPriceReport:message key="DT_T3" var="varT"/>
				<display:column property="TIERIIID" title="${varT}" headerClass="ShadeDarkBlueTD"  class="ShadeLightBlueTD" 
					sortable="true" style="text-align: right;" format="{0,number,###.##} "/>
				
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				
				<display:column property="TIERIV" title="<%=strTitleT4%>" headerClass="ShadeDarkBlueTD" class="ShadeMedBlueTD" 
					sortable="true" style="text-align: right;" format="<%=strCurrFmt%>" />
				<fmtAccountPriceReport:message key="DT_T4" var="varT"/>
				<display:column property="TIERIVD" title="${varT}" headerClass="ShadeDarkBlueTD" class="ShadeLightBlueTD" 
					sortable="true" style="text-align: right;" format="{0,number,###.##}" />
				
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				
				<display:column property="TLIST" title="Tier" headerClass="ShadeDarkGrayTD" class="ShadeWhite"
					sortable="true" style="text-align: center;"  />
				
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				<fmtAccountPriceReport:message key="DT_UNIT_PRICE" var="varUnitPrice"/>
				<display:column property="UPRICE" title="${varUnitPrice}" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD" 
					sortable="true" style="text-align: right;"  format="<%=strCurrFmt%>"  />
				<fmtAccountPriceReport:message key="DT_DISC" var="varDisc"/>
				<display:column property="DOFFERED" title="${varDisc}" headerClass="ShadeDarkGreenTD" class="ShadeLightGreenTD" 
					sortable="true" style="text-align: right;" format="{0,number,###.##%}" />
				
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				<fmtAccountPriceReport:message key="DT_GRP_PRICE_BOOK" var="varGrpBook"/>
				<display:column property="GPO" title="${varGrpBook}" media="html" headerClass="ShadeDarkGreenTD" class="ShadeLightGreenTD"  sortable="true" style="text-align: right;"/>
				<display:footer media="html"> 
					<tr><td class="Line" colspan="21"></td></tr>
				</display:footer>
				</display:table> 
			<% } else {%>	
				<display:table name="requestScope.ACCPRICEREPORT.rows" class="its" id="currentRowObject" cellpadding="0" cellspacing="0" freezeHeader="true" paneSize="400" > 
				
				<display:caption media="excel" class="ShadeDarkGrayTD" >
					<fmtAccountPriceReport:message key="LBL_ACCOUNT_NAME"/> :- <%= hmAccList.get("NAME") %> <fmtAccountPriceReport:message key="LBL_ACCOUNT_ID"/> :- <%= hmAccList.get("ID") %><fmtAccountPriceReport:message key="LBL_DATE"/>  :- <%= hmAccList.get("DT") %>
				</display:caption>
				<%  if (strQueryBy.equals("System Name") ) {  %>
				<fmtAccountPriceReport:message key="DT_SYSTEM_NAME" var="varSysName"/>
				<display:column property="SYSNM" title="${varSysName}" sortable="true" 
					headerClass="ShadeDarkGrayTD" class="ShadeWhite" style="text-align:left"/>
				<%  } else   { %>   
                <fmtAccountPriceReport:message key="DT_PRJ_NAME" var="varPrjname"/>
                <display:column property="PJNM" title="${varPrjname}" sortable="true" 
					headerClass="ShadeDarkGrayTD" class="ShadeWhite" style="text-align:left"/>
				
				<%	 }  		%>
				
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				<fmtAccountPriceReport:message key="DT_PART" var="varPart"/>
				<display:column property="PID" title="${varPart}" sortable="true" 
					headerClass="ShadeDarkGrayTD" class="ShadeWhite" style="text-align: center;"  />
				
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				<fmtAccountPriceReport:message key="DT_PART_DESCRIPTION" var="varPrtDesc"/>
				<display:column property="PDESC" escapeXml="false"  title="${varPrtDesc}" sortable="true" maxWords="30"
					headerClass="ShadeDarkGrayTD" class="ShadeWhite" style="text-align:left;width:350px"/>
					

				
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				
				<display:column property="TIERI" title="<%=strListPriceTitle%>"  headerClass="ShadeDarkBlueTD" class="ShadeMedBlueTD" 
					sortable="true" style="text-align: right;" format="<%=strCurrFmt%>" />
				
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				
					
				
				<display:column property="UPRICE" title="<%=strUnitPriceTitle%>" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD" 
					sortable="true" style="text-align: right;" format="<%=strCurrFmt%>" />
			
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>	
				
				<display:column property="DOFFERED" title="<%=strDiscount%>" headerClass="ShadeDarkGreenTD" class="ShadeLightGreenTD" 
					sortable="true" style="text-align: right;" format="{0,number,###%}" />
				
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				<fmtAccountPriceReport:message key="DT_GRP_PRICE_BOOK" var="varGrpBook"/>
				<display:column property="GPO" title="${varGrpBook}" media="html" headerClass="ShadeDarkGreenTD" class="ShadeLightGreenTD" 
					sortable="true" style="text-align: center;"/>
				<display:footer media="html"> 
					<tr><td class="Line" colspan="13"></td></tr>
				</display:footer>
				
				</display:table> 
			
			<% }
			if ( strOpt.equals("PriceIncrease") || strOpt.equals("CurrentPrice") ||  strOpt.equals("DTL") )
			  {
				if ( intNRows > 0 ) 
				{  
			%> 
					<tr><td colspan = "<%= intDefaultColSpan%>" align="center"><br><fmtAccountPriceReport:message key="BTN_EXCEL" var="varExcel"/><gmjsp:button value="${varExcel}" name="Btn_Print" gmClass="button" buttonType="Load" onClick="fnExportExcel()"/></td></tr>
					 <tr><td colspan='<%= intDefaultColSpan%>' height='20' align='center' id='button'> 
					<!--<input type='button' value='&nbsp;Print HTML&nbsp;' name='Btn_Print' class='button' onClick='fnPrint("HTML");' >
					<input type='button' value='&nbsp;Print PDF&nbsp;' name='Btn_Print' class='button' onClick='fnPrint("PDF");' >						
					 <input type='button' value='&nbsp;Email&nbsp;' name='Btn_Print' class='button' onClick='fnPrint("EMAIL");' >
					<input type='button' value='&nbsp;PDF Email&nbsp;' name='Btn_Print' class='button' onClick='fnPrint("EMAILPDF");' > 
					<input type='button' value='&nbsp;RTF&nbsp;' name='Btn_Print' class='button' onClick='fnPrint("RTF");' >-->
					<%-- To download the formatted <%=intCurrYear%> Price List that was sent to the Account, please click here --> 
					<input type='button' value='&nbsp;<%=intCurrYear%> XLS&nbsp;' name='Btn_Print' class='button' onClick='fnPrint("XLS");' >
					</td> </tr>--%>
				<% 	}  %>
			 				
			<% } 
			
			%>
			</td>
		</tr>
<%
		}
%>
		
    </table>
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
