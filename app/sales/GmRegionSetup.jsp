<%
/**********************************************************************************
 * File		 		: GmRegionSetup.jsp
 * Desc		 		: Region creation need to be automated and it should be through screens. Sales department should be able to create/edit
 						zone through those screens.
 * Version	 		: 2.0
 * author			: Jayaprasanth Gurunathan
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtRegionSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%
String strsalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES"); 
%>
<!--sales\GmRegionSetup.jsp  -->
<fmtRegionSetup:setLocale value="<%=strLocale%>"/>
<fmtRegionSetup:setBundle basename="properties.labels.sales.GmRegionSetup"/>
<bean:define id="strRegionID" name="frmRegionSetupForm" property="regName" type="java.lang.String"> </bean:define>
<bean:define id="adType" name="frmRegionSetupForm" property="adType" type="java.lang.String"> </bean:define>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Asset Activity</title>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script type="text/javascript" src="<%=strsalesJsPath%>/GmSalesMapping.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
</head>
<body leftmargin="20" topmargin="10" onLoad="fnDisable();">
<html:form action="/gmRegionMappingAction.do" method="post" styleId="form">
<input type="hidden" name="regHiddenName" value="<%=strRegionID%>">
<input type="hidden" name="regHiddenName" value="<%=adType%>">

<!-- Custom tag lib code modified for JBOSS migration changes --> 
	<table border="0" cellspacing="0" cellpadding="0" class="DtTable680">
	<tr>
	<td class="RightDashBoardHeader" height="20" align="left"><fmtRegionSetup:message key="LBL_REGION_AD"/></td>
	<td height="25" class="RightDashBoardHeader" align="right"></td>
	</tr>
		<tr height="30">
			<td align="right" class="RightTableCaption" ><span style="color:red;font-weight:bold;">*</span><fmtRegionSetup:message key="LBL_REGION_NAME"/>&nbsp;</td>
			<td>
			<html:text property="regName" name="frmRegionSetupForm" styleId="regName" size="22" />
			<html:hidden property="reg_id" styleId="reg_id" name="frmRegionSetupForm"/>
			<html:hidden property="strOpt" styleId="strOpt"/>
			</td>
		</tr>
		<tr class="shade" height="30">
		<td align="right" class="RightTableCaption" ><span style="color:red;font-weight:bold;">*</span><fmtRegionSetup:message key="LBL_ZONE"/>&nbsp;</td>
		<td><gmjsp:dropdown controlId="zoneType" 
			controlName="zoneType" SFFormName="frmRegionSetupForm"
			SFSeletedValue="zoneType" width="150" SFValue="alZone"
			codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" />
		</td>
		</tr>
		<tr class="shade" height="30">
		<td align="right" class="RightTableCaption" ><span style="color:red;font-weight:bold;">*</span><fmtRegionSetup:message key="LBL_DIVISION_NAME"/>&nbsp;</td>
		<td><gmjsp:dropdown controlId="divisionType" 
			controlName="divisionType" SFFormName="frmRegionSetupForm"
			SFSeletedValue="divisionType" width="150" SFValue="alDivision"
			codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" />
		</td>
		</tr>
		<tr class="shade" height="30">
		<td align="right" class="RightTableCaption" ><span style="color:red;font-weight:bold;">*</span><fmtRegionSetup:message key="LBL_COUNTRY_NAME"/>&nbsp;</td>
		<td><gmjsp:dropdown controlId="countryType" 
			controlName="countryType" SFFormName="frmRegionSetupForm"
			SFSeletedValue="countryType" width="150" SFValue="alCountry"
			codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" />
		</td>
		</tr>
		<tr class="shade" height="30">
		<td align="right" class="RightTableCaption" ><span style="color:red;font-weight:bold;">*</span><fmtRegionSetup:message key="LBL_COMPANY_NAME"/>&nbsp;</td>
		<td><gmjsp:dropdown controlId="companyType" 
			controlName="companyType" SFFormName="frmRegionSetupForm"
			SFSeletedValue="companyType" width="150" SFValue="alCompany"
			codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" />
		</td>
		</tr>
		<tr>
			<td class="RightTableCaption" align="right" HEIGHT="30"><span style="color:red;font-weight:bold;">*</span><fmtRegionSetup:message key="LBL_VP"/>&nbsp;</td>
			<td><gmjsp:dropdown	controlId="vpID" 
			controlName="vpID" SFFormName="frmRegionSetupForm" 
			SFSeletedValue="vpID" width="150" SFValue="alVP" 
			codeId="ID" codeName="NAME" defaultValue="[Choose One]" />
			<input type="checkbox" name="newVPFL" value="" onClick="fnNewVPUser(this);"/>
			<fmtRegionSetup:message key="LBL_CREATE_OPENVP"/></td>
		</tr>
		<tr class="shade" height="30">
			<td align="right" class="RightTableCaption"><span style="color:red;font-weight:bold;">*</span><fmtRegionSetup:message key="LBL_AD"/>&nbsp;</td>
		<td>
		 <gmjsp:dropdown controlId="adType" 
			controlName="adType" SFFormName="frmRegionSetupForm"
			SFSeletedValue="adType" tabIndex="3" width="150" SFValue="alAD"
			codeId="ID" codeName="NAME" defaultValue="[Choose One]" /> 
			<input type="checkbox" name="newADFL" value="" onClick="fnNewADUser(this);"/>
			<fmtRegionSetup:message key="LBL_CREATE_OPENAD"/></td>
		</tr>
			<tr class="shade" height="30">
			<td align="right" class="RightTableCaption"><fmtRegionSetup:message key="LBL_AD_PLUS"/>&nbsp;</td>
		<td>
		 <gmjsp:dropdown controlId="adPlus" 
			controlName="adPlus" SFFormName="frmRegionSetupForm"
			SFSeletedValue="adPlus" tabIndex="3" width="150" SFValue="alADPlus"
			codeId="ID" codeName="NAME" defaultValue="[Choose One]" /> 
		&nbsp;<bean:write name="frmRegionSetupForm"  property="adPlusNames"/></td>
		
		</tr>
		<tr height="30">
		<td colspan="2">
		<center><fmtRegionSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="saveBut" buttonType="Save" value="${varSubmit}" gmClass="Button" onClick="fnSubmit();"/>&nbsp;&nbsp;&nbsp;&nbsp;		 
		<!--
		<logic:notEqual name="frmRegionSetupForm" property="reg_id" value=""> 
			<fmtRegionSetup:message key="BTN_VOID" var="varVoid"/><gmjsp:button name="voidButton" buttonType="Save" value="${varVoid}" gmClass="Button" onClick="fnVoid(document.getElementById('reg_id').value);"/>
		</logic:notEqual>
		--> 
		 </center>
		</td>
		</tr>
	</table> 
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>