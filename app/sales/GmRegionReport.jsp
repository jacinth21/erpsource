<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtRegionReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<% 
String strsalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");
%>
<!-- sales\GmRegionReport.jsp -->
<fmtRegionReport:setLocale value="<%=strLocale%>"/>
<fmtRegionReport:setBundle basename="properties.labels.sales.GmRegionReport"/>
<head>
<title>Insert title here</title>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script type="text/javascript" src="<%=strsalesJsPath%>/GmSalesMapping.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

<bean:define id="gridData" name="frmRegionSetupForm" property="gridXmlData" type="java.lang.String">
</bean:define>

<script>
var gridObjData ='<%=gridData%>';
var mygrid; 
</script>
</head>

<body leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form action="/gmRegionMappingAction.do" method="post" styleId="form">

<html:hidden property="strOpt" name="frmRegionSetupForm"/>
<table cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="5" height="25" class="RightDashBoardHeader"><fmtRegionReport:message key="LBL_REGION"/></td>
	</tr>
	<tr>
		<td colspan="4">
		<div id="dataGridDiv" width="1200px" height="450px"></div>
		</td>
	</tr>
	<tr>
         <td colspan="6" align="center">
          <div class='exportlinks'><fmtRegionReport:message key="LBL_EXPORT"/> <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
               onclick="mygrid.toExcel('/phpapp/excel/generate.php');"><fmtRegionReport:message key="LBL_EXCEL"/> </a></div>
           </td>
	</tr>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>