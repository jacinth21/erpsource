 <%
/**********************************************************************************
 * File			 	: GmSalesGrowthReport.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page buffer="16kb" autoFlush="true" %>

<!--sales\GmTerrSalesToQuota.jsp  --> 
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<%@ taglib prefix="fmtTerrSalesToQuota" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtTerrSalesToQuota:setLocale value="<%=strLocale%>"/>
<fmtTerrSalesToQuota:setBundle basename="properties.labels.sales.GmTerrSalesToQuota"/>
<%
response.setHeader("Cache-Control", "no-cache");
%>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	int intSize = 0;
	int intLength = 0;


	String strCodeID = "";
	String strSelected = "";
	String strVendId = "";
	String strCurDate = "";
	
	HashMap hmLoadReturn = new HashMap();
	HashMap hcboVal = new HashMap();
  
  	String strOpt = (String)request.getAttribute("hOpt")==null?"":(String)request.getAttribute("hOpt");
  	String strHeader = (String)request.getAttribute("hHeader")==null?"":(String)request.getAttribute("hHeader");

  	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Sales Growth Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     textarea {
	width: 60%;
	height: 60px;
}
</style>

<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>

<script>
function fnCallHosp(obj)
{
	fnSetSelections();
	document.frmMain.hAction.value = "Reload";
	document.frmMain.strSessOpt.value = "ACCT";
	document.frmMain.hId.value = obj.id;
	document.frmMain.submit();
}

function fnCallGrp(obj)
{
	fnSetSelections();
	document.frmMain.hAction.value = "Reload";
	document.frmMain.strSessOpt.value = "GRP";
	document.frmMain.hId.value = obj.id;
	document.frmMain.submit();
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">


	<table class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtTerrSalesToQuota:message key="LBL_TERRITORY_SALES"/></td>
		</tr>
		<tr><td class="Line"></td></tr>
		<tr>
			<td><!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					<jsp:include page="/sales/GmSalesFilters.jsp" />
			</td>
		</tr>
		<tr><td class="Line"></td></tr>
		<tr>
			<td>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td valign="top" class="aligncenter" height="30">
						<FORM name="frmMain" method="POST" action="<%=strServletPath%>/GmTerritoryServlet">
							<input type="hidden" name="hAction" value="">
							<input type="hidden" name="hId" value="">
							<input type="hidden" name="strSessOpt" value="<%=strOpt%>">
							<!-- The following 4 hidden fields are for Sales Filters -->
							<input type="hidden" name="hRegnFilter" value="">
							<input type="hidden" name="hDistFilter" value="">
							<input type="hidden" name="hTerrFilter" value="">
							<input type="hidden" name="hPgrpFilter" value="">

							<display:table name="alReturn" export="false" class="its">
							<fmtTerrSalesToQuota:message key="DT_NAME" var="varName"/><display:column property="NAME" title="${varName}" class="alignleft" sortable="true" />
							<fmtTerrSalesToQuota:message key="DT_REP_NAME" var="varRepName"/><display:column property="RNAME" title="${varRepName}" class="alignleft" sortable="true" />
							<fmtTerrSalesToQuota:message key="DT_MTD_SALES" var="varMtdSales"/><display:column property="MTD" title="${varMtdSales}" class="alignright" sortable="true" format="{0,number,$#,###,###.##}" />
							<fmtTerrSalesToQuota:message key="DT_MONTH_QUOTA" var="varMonthQuota"/><display:column property="QMTD" title="${varMonthQuota}" class="alignright" sortable="true" format="{0,number,$#,###,###.##}" />
							<fmtTerrSalesToQuota:message key="DT_QUOTA" var="varQuota"/><display:column property="MQPER" title="${varQuota}" class="alignright" sortable="true" />
							<fmtTerrSalesToQuota:message key="DT_QTD_SALES" var="varQtdSales"/><display:column property="QTD" title="${varQtdSales}" class="alignright" sortable="true" format="{0,number,$#,###,###.##}" />
							<fmtTerrSalesToQuota:message key="DT_QUATER_QUOTA" var="varQuateQuota"/><display:column property="QQTD" title="${varQuateQuota}" class="alignright" sortable="true" format="{0,number,$#,###,###.##}" />
							<fmtTerrSalesToQuota:message key="DT_QUOTAQ" var="varQuotaq"/><display:column property="QQPER" title="${varQuotaq}" class="alignright" sortable="true" />
							<fmtTerrSalesToQuota:message key="DT_YTD_SALES" var="varYtdSales"/><display:column property="YTD" title="${varYtdSales}" class="alignright" sortable="true" format="{0,number,$#,###,###.##}" />
							<fmtTerrSalesToQuota:message key="DT_YEAR_QUOTA" var="varYearQuota"/><display:column property="QYTD" title="${varYearQuota}" class="alignright" sortable="true" format="{0,number,$#,###,###.##}" />
							<fmtTerrSalesToQuota:message key="DT_YQUOTA" var="varYquota"/><display:column property="YQPER" title="${varYquota}" class="alignright" sortable="true" />
							</display:table>

							</FORM>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

<%
	}catch(Exception e)
		{
			e.printStackTrace();
		}
%>

</BODY>

</HTML>
