<%
/*********************************************************************************************************
 * File		 		: GmSetInvReport.jsp
 * Desc		 		: This screen is used to load Sets Report.
 * Version	 		: 1.0
 * author			: Anilkumar
**********************************************************************************************************/
%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%> 
<%@ page import="com.globus.common.beans.GmCommonControls"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtSetInvReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\InvManagement\GmSetInvReport.jsp -->

<fmtSetInvReport:setLocale value="<%=strLocale%>"/>
<fmtSetInvReport:setBundle basename="properties.labels.sales.InvManagement.GmSetInvReport"/> 

<bean:define id="alDistList" name="frmGmTagSetInvMgt" property="alDistributor" type="java.util.List"></bean:define>
<bean:define id="alType" name="frmGmTagSetInvMgt" property="alType" type="java.util.List"></bean:define>
<bean:define id="alSystem" name="frmGmTagSetInvMgt" property="alSystem" type="java.util.ArrayList"></bean:define>
<bean:define id="alSet" name="frmGmTagSetInvMgt" property="alSet" type="java.util.ArrayList"></bean:define>
<bean:define id="xmlGridData" name="frmGmTagSetInvMgt" property="gridData" type="java.lang.String"></bean:define>
<bean:define id="strSetId" name="frmGmTagSetInvMgt" property="setId" type="java.lang.String"></bean:define>
 <bean:define id="strSystem" name="frmGmTagSetInvMgt" property="system" type="java.lang.String"></bean:define>
 <%

String strSalesInvMgntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_INVMANAGEMENT");
 %>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Inventory Report</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script> 
<script language="JavaScript" src="<%=strSalesInvMgntJsPath%>/GmSetInvReport.js"></script>
<script type="text/javascript">

var objGridData = '<%=xmlGridData%>';
var SetArr = new Array(1);
var selectsetval = '<%=strSetId%>';
var selectedSys = '<%=strSystem%>';
<%

	HashMap hmSetVal = new HashMap();
	for (int i=0,j=0;i<alSet.size();i++)
	{
		hmSetVal = (HashMap)alSet.get(i);
		String strType = GmCommonClass.parseNull((String)hmSetVal.get("TYPEID"));
		if(strType.equals("4070") || strType.equals("4074")){		
		
%>
SetArr[<%=j++%>] = new Array("<%=hmSetVal.get("ID")%>","<%=hmSetVal.get("IDNAME")%>","<%=hmSetVal.get("TYPEID")%>","<%=hmSetVal.get("SYSTEMID")%>");
<%
		}
		}
%>

</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageload()">
<html:form action="/gmTagSetInvMgt.do?method=fetchSetInvReport">
<html:hidden property="strOpt"/>

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table  border="0"  class="DtTable1000"  cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" width="1000" colspan="5">
				<fmtSetInvReport:message key="LBL_SET_INVENTORY_REPORT"/>
			</td>
			<td align="right" class=RightDashBoardHeader>
			<fmtSetInvReport:message key="IMG_ALT_HELP" var = "varHelp"/>			
			<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("SET_INVENT_REPORT_ALL")%>');" />
			</td>
		</tr>
		<tr>
			<td colspan="7">
				<jsp:include page="/sales/GmSalesFilters.jsp" >
					<jsp:param name="HIDE" value="SYSTEM" />
					<jsp:param name="HIDEBUTTON" value="HIDEGO" />
					<jsp:param name="FRMNAME" value="frmGmTagSetInvMgt" />
					<jsp:param name="URL" value="/gmTagSetInvMgt.do?method=fetchSetInvReport" />
				</jsp:include> 
			</td>
	   </tr>
		
		<tr class="shade">
			<td class="RightRedCaption" align="right" height="25"  ><fmtSetInvReport:message key="LBL_SYSTEM_NAME"/>:&nbsp;</td>				
				<td height="23"><gmjsp:autolist comboType="DhtmlXCombo" controlName="system" SFFormName="frmGmTagSetInvMgt" SFSeletedValue="system"
					SFValue="alSystem" codeId="ID"  codeName="NAME"  onChange="loadSets"  tabIndex="1" defaultValue="[Choose One]" width="200" />
			</td>
		
			<td class="RightRedCaption" align="right" height="25" ><fmtSetInvReport:message key="LBL_SET"/>:&nbsp;</td>				
			<td colspan="3"><gmjsp:dropdown controlName="setId" SFFormName="frmGmTagSetInvMgt"
				SFSeletedValue="setId" SFValue="alSet" codeId="ID"  codeName="IDNAME"   defaultValue="[Choose One]" width="230" />
			</td>
		</tr>
		<tr><td colspan="7" class="LLine" height="1"></td></tr>
		<tr>
		<td class="RightRedCaption" align="right" height="25"  ><fmtSetInvReport:message key="LBL_FIELD_SALES"/>:&nbsp;</td>				
		<td><gmjsp:dropdown controlName="distributorId" SFFormName="frmGmTagSetInvMgt"
				SFSeletedValue="distributorId" SFValue="alDistributor" codeId="ID"  codeName="NM"   defaultValue="[Choose One]" width="200" />
		</td>
		<td class="RightRedCaption" align="right" height="25"><fmtSetInvReport:message key="LBL_TYPE"/>:&nbsp;</td>
	    <td> <gmjsp:dropdown controlName="typeid"  SFFormName="frmGmTagSetInvMgt" SFSeletedValue="typeid" SFValue="alType"
		       codeId="CODENMALT"  codeName="CODENM"  defaultValue="[Choose One]"  width="230"/>
        </td>
        <td colspan="0" align="left">
				<fmtSetInvReport:message key="BTN_LOAD" var="varLoad"/>
				<gmjsp:button  name="Load"   value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" buttonType="Load" onClick="fnLoad();"/>&nbsp;&nbsp;
			</td>
		</tr>		
		<tr><td class="Line" height="1" colspan="7"></td></tr>
		<%if(xmlGridData.indexOf("cell") != -1){%>		
		<tr>
			<td colspan="7">
				<div id="setInventoryRpt" style="height: 300px; width: 1000px"></div>
			</td>
		</tr>		
		<%}else{%>
		<tr>
			<td colspan="7" height="20" align="center" class="RightText"><fmtSetInvReport:message key="MSG_NO_DATA"/>
			</td>
		</tr>
		<%}%>
		<%if(xmlGridData.indexOf("cell") != -1) {%>
		<tr>
			<td align="center" colspan="6">
				<div class='exportlinks'><fmtSetInvReport:message key="DIV_EXPORT_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp; <a href="#" onclick="fnExportToExcel();"> <fmtSetInvReport:message key="DIV_EXCEL"/> </a></div>
			</td>
		</tr>
		<%}%>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>