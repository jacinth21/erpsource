<%
/*********************************************************************************************************
 * File		 		: GmTagSetAttribute.jsp
 * Desc		 		: This screen is used to edit tag attribute
 * Version	 		: 1.0
 * author			: Gopinathan
**********************************************************************************************************/
%> 
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtTagSetAttribute" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\InvManagement\GmTagSetAttribute.jsp -->

<fmtTagSetAttribute:setLocale value="<%=strLocale%>"/>
<fmtTagSetAttribute:setBundle basename="properties.labels.sales.InvManagement.GmTagSetAttribute"/>


<HTML>
<HEAD>

<bean:define id="strTagAttrStatus" name="frmGmTagSetInvMgt" property="tagAttrStatus" type="java.lang.String"></bean:define>
<bean:define id="strTagAttrType" name="frmGmTagSetInvMgt" property="tagAttrType" type="java.lang.String"></bean:define>
<bean:define id="strOption" name="frmGmTagSetInvMgt" property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="strMultiTags" name="frmGmTagSetInvMgt" property="strTagIds" type="java.lang.String"></bean:define>
<%
String strSalesInvMgntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_INVMANAGEMENT");
String strAttLabel = "";
if(strTagAttrType.equals("11305")){
	strAttLabel = strTagAttrStatus.equals("Y")?"Unlock":"Lock";
}else if(strTagAttrType.equals("11310")){
	strAttLabel = strTagAttrStatus.equals("Y")?"Active":"Decommission";
}
strMultiTags = strMultiTags.replace(",",", ");
%>
<TITLE> Globus Medical: Tag Attribute</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
<script language="JavaScript" src="<%=strSalesInvMgntJsPath%>/GmTagSetAttribute.js"></script>
</HEAD>
<BODY leftmargin="20" topmargin="10">
<html:form action="/gmTagSetInvMgt.do?method=tagSetLocation" >
<html:hidden property="strOpt"/>
<html:hidden property="strTagIds"/>
<html:hidden property="tagAttrStatus"/>
<html:hidden property="tagAttrType"/>
<html:hidden property="tagAttrValue"/>
<html:hidden property="cancelType"/>
<html:hidden property="zone"/>
<html:hidden property="region"/>
<html:hidden property="distributorId"/>
<html:hidden property="salesRepId"/>
<html:hidden property="accountId"/>
<html:hidden property="setId"/>
<html:hidden property="tagIdFrom"/>
<html:hidden property="tagIdTo"/>
<html:hidden property="currentLoc"/>
<html:hidden property="lockInv"/>
<html:hidden property="decommission"/>
<html:hidden property="system"/>

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable800"  cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;
				<%=strAttLabel%> <fmtTagSetAttribute:message key="LBL_INVENTORY"/>
			</td>
			<td align="right" class=RightDashBoardHeader>
			<fmtTagSetAttribute:message key="IMG_ALT_HELP" var = "varHelp"/>			
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("EDIT_TAG_LOCATION")%>');" />
			</td>
		</tr>
		<tr><td colspan="2" class="Line" height="1"></td></tr>
		<tr>
		     <fmtTagSetAttribute:message key="LBL_TAG" var="varTag"/>
			<gmjsp:label type="BoldText"  SFLblControlName="${varTag} :&nbsp;" td="true"/>
			<td width="65%">
				<%=strMultiTags%>
			</td>
		</tr>
<%
if(strTagAttrType.equals("11305")){
%>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr  class="shade">
		    <fmtTagSetAttribute:message key="LBL_LOCK_ACC" var="varLock"/>
			<gmjsp:label type='<%=(!strAttLabel.equals("Unlock"))?"MandatoryText":"BoldText"%>'  SFLblControlName="${varLock} :&nbsp;" td="true"/>
			<td align ="left">
				<gmjsp:dropdown	controlName="lockAccId" disabled='<%=(strAttLabel.equals("Unlock"))?"Disabled":""%>'  SFFormName="frmGmTagSetInvMgt" SFSeletedValue="lockAccId" SFValue="alAccount" codeId="ID" codeName="ACNAME" defaultValue="[Choose One]" onChange="" />
			</td>
		</tr>
<%
}
%>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr>
		    <fmtTagSetAttribute:message key="LBL_REASON" var="varReason"/>
			<gmjsp:label type="MandatoryText"  SFLblControlName="${varReason} :&nbsp;" td="true"/>
			<td align ="left">
				<gmjsp:dropdown	controlName="tagAttrReason" SFFormName="frmGmTagSetInvMgt" SFSeletedValue="tagAttrReason" SFValue="alTagAttrReason" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" onChange="" />
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr class="shade">
		     <fmtTagSetAttribute:message key="LBL_COMMENTS" var="varComments"/>
			<gmjsp:label type="MandatoryText"  SFLblControlName="${varComments} :&nbsp;" td="true"/>
			<td align ="left">
			<html:textarea property="tagAttrCmt" cols="60" rows="3"/>
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr>
			<td colspan="2">
			<jsp:include page="/sales/InvManagement/GmIncludeTagCommentsLog.jsp">
				<jsp:param name="FORMNAME" value="frmGmTagSetInvMgt" />
				<jsp:param name="ALNAME" value="alCommentsLog" />					
			</jsp:include>
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		
		<tr height="40">
			<td colspan="2" align="center">
				<% String strDisableFl = "" ;
				if(strOption.equals("save")){
					strDisableFl = "true";
				}
				%>
				
				<gmjsp:button name="save"  disabled ="<%=strDisableFl%>" value="<%=strAttLabel%>" buttonType="Save" onClick="fnSaveAttribute();" />&nbsp;&nbsp;
				<fmtTagSetAttribute:message key="BTN_BACK" var="varBack"/>
				<gmjsp:button name="back"   value="&nbsp;&nbsp;${varBack}&nbsp;&nbsp;" buttonType="Save" onClick="fnSetInvRpt();"/>&nbsp;&nbsp;
			</td>
		</tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>