<%
/*********************************************************************************************************
 * File		 		: GmTagSetLocation.jsp
 * Desc		 		: This screen is used to edit tag current location
 * Version	 		: 1.0
 * author			: Gopinathan
**********************************************************************************************************/
%> 
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>

<%@ taglib prefix="fmtTagSetLocation" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- sales\InvManagement\GmTagSetLocation.jsp -->

<fmtTagSetLocation:setLocale value="<%=strLocale%>"/>
<fmtTagSetLocation:setBundle basename="properties.labels.sales.InvManagement.GmTagSetLocation"/>
 

<bean:define id="alDistList" name="frmGmTagSetInvMgt" property="alDistributor" type="java.util.List"></bean:define>
<bean:define id="alRepList" name="frmGmTagSetInvMgt" property="alSalesRep" type="java.util.List"></bean:define>
<bean:define id="alAcctList" name="frmGmTagSetInvMgt" property="alAccount" type="java.util.List"></bean:define>
<bean:define id="StrAddressId" name="frmGmTagSetInvMgt" property="addressId" type="java.lang.String"></bean:define>
<bean:define id="strMultiTags" name="frmGmTagSetInvMgt" property="strTagIds" type="java.lang.String"></bean:define>
<bean:define id="strSubLoc" name="frmGmTagSetInvMgt" property="subLocId" type="java.lang.String"></bean:define>

<%
String strSalesInvMgntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_INVMANAGEMENT");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Tag Current Location</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
<script language="JavaScript" src="<%=strSalesInvMgntJsPath%>/GmTagSetLocation.js"></script>
<script type="text/javascript">

var lblEntity = '<fmtTagSetLocation:message key="LBL_ENTITY"/>';
var lblName = '<fmtTagSetLocation:message key="LBL_NAME"/>';
var lblLocation = '<fmtTagSetLocation:message key="LBL_LOCATION"/>';

<% 
StrAddressId = GmCommonClass.parseZero(StrAddressId);
strSubLoc = GmCommonClass.parseZero(strSubLoc); 
strMultiTags = strMultiTags.replace(",",", ");
%>

var ajax = new Array();
var vAddId = '<%=StrAddressId%>';

var distLen = <%=alDistList.size()%>;
var repLen = <%=alRepList.size()%>;
var acctLen = <%=alAcctList.size()%>;

<%
HashMap hmDistVal = new HashMap();
for (int i=0;i<alDistList.size();i++)
{
	hmDistVal = (HashMap)alDistList.get(i);
%>
var	 DistArr<%=i%> =  "<%=hmDistVal.get("ID")%>,<%=hmDistVal.get("NAME")%>" ;
<%
}
%> 

<%
	HashMap hmRepVal = new HashMap();
	for (int i=0;i<alRepList.size();i++)
	{
		hmRepVal = (HashMap)alRepList.get(i);
%>
var	RepArr<%=i%> = "<%=hmRepVal.get("ID")%>,<%=hmRepVal.get("NAME")%>" ;
<%
	}
%>

<%
	HashMap hmAcctVal = new HashMap();
	for (int i=0;i<alAcctList.size();i++)
	{
		hmAcctVal = (HashMap)alAcctList.get(i);
%>
var	AcctArr<%=i%> =  "<%=hmAcctVal.get("ID")%>,<%=hmAcctVal.get("ACNAME")%>" ;
<%
	}
%>

</script>
 
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnChangeLoc('<%=strSubLoc%>');">
<html:form action="/gmTagSetInvMgt.do?method=tagSetLocation" >
<html:hidden property="strOpt"/>
<html:hidden property="strTagIds"/>
<html:hidden property="zone"/>
<html:hidden property="region"/>
<html:hidden property="distributorId"/>
<html:hidden property="salesRepId"/>
<html:hidden property="accountId"/>
<html:hidden property="setId"/>
<html:hidden property="tagIdFrom"/>
<html:hidden property="tagIdTo"/>
<html:hidden property="currentLoc"/>
<html:hidden property="lockInv"/>
<html:hidden property="decommission"/>
<html:hidden property="system"/>
<html:hidden property="distId"/>
<html:hidden property="parentFrmName"/>

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable700"  cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" width="272">
				<fmtTagSetLocation:message key="LBL_TAG_CURRENT_LOCATION"/>
			</td>
			<td align="right" class=RightDashBoardHeader>
			<fmtTagSetLocation:message key="IMG_ALT_HELP" var = "varHelp"/>
			<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("EDIT_TAG_LOCATION")%>');" />
			</td>
		</tr>
		<tr><td colspan="2" class="Line" height="1"></td></tr>
		<tr>
		     <fmtTagSetLocation:message key="LBL_TAG" var="varTag"/>
			<gmjsp:label type="BoldText"  SFLblControlName="${varTag} :&nbsp;" td="true"/>
			<td width="400" height="22">
				<%=strMultiTags%>
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr class="shade" >
		    <fmtTagSetLocation:message key="LBL_ENTITY" var="varEntity"/>
			<gmjsp:label type="MandatoryText"  SFLblControlName="${varEntity} :&nbsp;" td="true"/>
			<td align ="left" height="22">
				<gmjsp:dropdown	controlName="curLocId" SFFormName="frmGmTagSetInvMgt" SFSeletedValue="curLocId" SFValue="alCurrentLoc" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" onChange="fnChangeLoc('0');fnEmptyDrpDwn(document.frmGmTagSetInvMgt.addressId);" />
			</td>
		</tr>

		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr id="subLocRow">
		<fmtTagSetLocation:message key="LBL_NAME" var="varName"/>
			<gmjsp:label type="MandatoryText"  SFLblControlName="${varName} :&nbsp;" td="true"/>
			<td align ="left" height="22">
				<gmjsp:dropdown	controlName="subLocId" SFFormName="frmGmTagSetInvMgt" SFSeletedValue="subLocId" SFValue="alSalesRep" width="300" codeId="ID" codeName="NAME" defaultValue="[Choose One]" onChange="fnChangeName();" />
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr id="addRow" class="shade">
		<fmtTagSetLocation:message key="LBL_LOCATION" var="varLocation"/>
			<gmjsp:label type="MandatoryText"  SFLblControlName="${varLocation} :&nbsp;" td="true"/>
			<td align ="left" height="22">
				<gmjsp:dropdown	controlName="addressId" SFFormName="frmGmTagSetInvMgt" SFSeletedValue="addressId" width="370" codeId="ID" codeName="NM" defaultValue="[Choose One]" onChange="" />
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr height="40">
			<td colspan="2" align="center">
			<fmtTagSetLocation:message key="BTN_SAVE" var="varSave"/>
				<gmjsp:button  name="Load"   value="&nbsp;&nbsp;${varSave}&nbsp;&nbsp;" buttonType="Save" onClick="fnSaveLoction();" />&nbsp;&nbsp;
				<fmtTagSetLocation:message key="LBL_BACK" var="varBack"/>
				<gmjsp:button  name="Load"   value="&nbsp;&nbsp;${varBack}&nbsp;&nbsp;" buttonType="Load" onClick="fnSetInvRpt();"/>&nbsp;&nbsp;
			</td>
		</tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>