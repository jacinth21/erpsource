<%
	/**********************************************************************************
	 * File		 		: GmIncludeTagCommentsLog.jsp
	 * Desc		 		: This screen is used as an include Jsp in GmTagSetAttribute.jsp
	 * Version	 		: 1.0
	 * author			: Gopinathan
	 ************************************************************************************/
%>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="org.apache.commons.beanutils.PropertyUtils"%>
 
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtIncludeTagCommentsLog" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\InvManagement\GmIncludeTagCommentsLog.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtIncludeTagCommentsLog:setLocale value="<%=strLocale%>"/>
<fmtIncludeTagCommentsLog:setBundle basename="properties.labels.sales.InvManagement.GmIncludeTagCommentsLog"/>
<%	
	HashMap hmLog = new HashMap();
	ArrayList alLog = new ArrayList();
	int intLog = 0;	 
	String strFormName = "";
	String strArrayListName = "";
	boolean blHideComments = false;
	
	strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
	if(!strFormName.equals(""))
	{
	
		strArrayListName = GmCommonClass.parseNull(request.getParameter("ALNAME"));
		Object objTemp = pageContext.getRequest().getAttribute(strFormName);
		alLog  = GmCommonClass.parseNullArrayList((ArrayList)PropertyUtils.getProperty(objTemp,strArrayListName));
	}
	else {
	
	}
	
%>
	  
	  
<script type="text/javascript">
function fnShowAll(){
	var vshow = (navigator.userAgent.indexOf("Safari")!=-1)?'table-row':'block';
	 var tblHideComments = document.getElementById("tblHideComments");
	 var divShow = document.getElementById("divShow");
      if(tblHideComments.style.display == vshow){
	        tblHideComments.style.display = 'none';
	        divShow.innerHTML = "&nbsp;<a href=\"javascript:fnShowDivTag()\"><img alt=\"Click to view all comments\" border=\"0\" src=\"
	    <%=strImagePath%>/plus.gif\"/>&nbsp;Show More</a>";
       }else{
       	tblHideComments.style.display = vshow;
	        divShow.innerHTML = "&nbsp;<a href=\"javascript:fnShowDivTag()\"><img alt=\"Click to hide comments\" border=\"0\" src=\"
	        	<%=strImagePath%>/minus.gif\"/>&nbsp;Show Less</a>";
       }
}
</script>

<table border=0 Width="100%" cellspacing=0 cellpadding=0>
	<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
	<tr bgcolor="#EEEEEE" class="RightTableCaption">
		<td width="60" height="25">&nbsp;<fmtIncludeTagCommentsLog:message key="LBL_TAGID"/></td>
		<td width="400">&nbsp;<fmtIncludeTagCommentsLog:message key="LBL_COMMENTS"/></td>
		<td width="140">&nbsp;<fmtIncludeTagCommentsLog:message key="LBL_REASON"/></td>
		<td width="140">&nbsp;<fmtIncludeTagCommentsLog:message key="LBL_CREATED_BY"/></td>
		<td width="60" height="25">&nbsp;<fmtIncludeTagCommentsLog:message key="LBL_DATE"/></td>
	</tr>
	<TR><TD colspan=5 height=1 class=Line></TD></TR>
	
	<%
	if (alLog != null){
		intLog = alLog.size();
	}
	if(intLog>3){
		blHideComments  = true;
	}
	if (intLog > 0)
	{
		
		for (int i=0;i<intLog;i++)
		{	hmLog = (HashMap)alLog.get(i);
%>
			<tr>
				<td class="RightText" height="20">&nbsp;<%=(String)hmLog.get("TXNID")%></td>
				<td class="RightText">&nbsp;<%=(String)hmLog.get("COMMENTS")%></td>
				<td class="RightText"><%=(String)hmLog.get("CANCELREASON")%></td>
				<td class="RightText">&nbsp;<%=(String)hmLog.get("CREATEDBY")%></td>
				<td class="RightText"><%=(String)hmLog.get("CANCELDATE")%></td>
			</tr>
			<%if(blHideComments && i==2){%>
					<tr><td  colspan=5><div id="divShow">
					&nbsp;<a href="javascript:fnShowDivTag()"><fmtIncludeTagCommentsLog:message key="IMG_ALT_VIEW" var = "varView"/><img alt="${varView}" border="0" src="
<%=strImagePath%>/plus.gif"/>&nbsp;<fmtIncludeTagCommentsLog:message key="LBL_SHOW_MORE"/></a></div></td></tr>
				 	<tr><td colspan=5><table id="tblHideComments" border="0" style="display:none" width="100%" cellspacing=0 cellpadding=0>
			 		<tr>
						<td width="60" ></td>
						<td width="400"></td>
						<td width="140"></td>
						<td width="140"></td>
						<td width="60"></td>
					</tr>
			<%} // End of Div flag 
			if(blHideComments && i==(intLog-1)){
			%>
				</table></td></tr> 
			<%} 
		}//End of FOR Loop
	}else {
%>		<tr><td colspan="5" height="25" align="center" class="RightTextBlue">
		<BR><fmtIncludeTagCommentsLog:message key="MSG_NO_DATA"/></td></tr>
<%

}
	%>
<TR><TD colspan=5 height=1 class=Line></TD></TR>
</table>
