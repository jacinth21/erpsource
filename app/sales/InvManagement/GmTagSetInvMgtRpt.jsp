<%
/*********************************************************************************************************
 * File		 		: GmTagSetInvMgtRpt.jsp
 * Desc		 		: This screen is used to sales rep's set inventory management
 * Version	 		: 1.0
 * author			: Gopinathan
**********************************************************************************************************/
%> 
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>

<%@ taglib prefix="fmtTagSetInvMgtRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- sales\InvManagement\GmTagSetInvMgtRpt.jsp -->

<fmtTagSetInvMgtRpt:setLocale value="<%=strLocale%>"/>
<fmtTagSetInvMgtRpt:setBundle basename="properties.labels.sales.InvManagement.GmTagSetInvMgtRpt"/>

 
<bean:define id="alRegionList" name="frmGmTagSetInvMgt" property="alRegion" type="java.util.List"></bean:define>
<bean:define id="alDistList" name="frmGmTagSetInvMgt" property="alDistributor" type="java.util.List"></bean:define>
<bean:define id="alAcctList" name="frmGmTagSetInvMgt" property="alAccount" type="java.util.List"></bean:define>
<bean:define id="alRepList" name="frmGmTagSetInvMgt" property="alSalesRep" type="java.util.List"></bean:define>
<bean:define id="gridData" name="frmGmTagSetInvMgt" property="gridData" type="java.lang.String"></bean:define>
<bean:define id="alDivList" name="frmGmTagSetInvMgt" property="alDivison" type="java.util.List"></bean:define>
<%
String strSalesInvMgntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_INVMANAGEMENT");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Inventory Management</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strSalesInvMgntJsPath%>/GmTagSetInvMgtRpt.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js" ></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js" ></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">
<script language="JavaScript"	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<%
int intAccLevel 	    = Integer.parseInt(GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
String strDepartMentID	= GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
if(strDepartMentID.equals("C")){
	
}
String strGridData = "";
if(GmCommonClass.parseNull(gridData).indexOf("cell")!=-1){
	strGridData = gridData;
}


%>

<script>
var dhxWins,w1;
var mygrid;
var gridObjData=TRIM('<%=strGridData%>');


var regionLen = <%=alRegionList.size()%>;
var distLen = <%=alDistList.size()%>;
var repLen = <%=alRepList.size()%>;
var acctLen = <%=alAcctList.size()%>;

<%
	HashMap hmRegionVal = new HashMap();
	for (int i=0;i<alRegionList.size();i++)
	{
		hmRegionVal = (HashMap)alRegionList.get(i);
%>
var	 RegionArr<%=i%> =  "<%=hmRegionVal.get("VPID")%>,<%=hmRegionVal.get("ADID")%>,<%=hmRegionVal.get("ID")%>,<%=hmRegionVal.get("NM")%>" ;
<%
	}
%> 


<%
HashMap hmDistVal = new HashMap();
for (int i=0;i<alDistList.size();i++)
{
	hmDistVal = (HashMap)alDistList.get(i);
%>
var	 DistArr<%=i%> =  "<%=hmDistVal.get("VPID")%>,<%=hmDistVal.get("ADID")%>,<%=hmDistVal.get("ID")%>,<%=hmDistVal.get("NM")%>" ;
<%
}
%> 

<%
	HashMap hmRepVal = new HashMap();
	for (int i=0;i<alRepList.size();i++)
	{
		hmRepVal = (HashMap)alRepList.get(i);
%>
var	RepArr<%=i%> = "<%=hmRepVal.get("DID")%>,<%=hmRepVal.get("DNAME")%>,<%=hmRepVal.get("ID")%>,<%=hmRepVal.get("NM")%>" ;
<%
	}
%>

<%
	HashMap hmAcctVal = new HashMap();
	for (int i=0;i<alAcctList.size();i++)
	{
		hmAcctVal = (HashMap)alAcctList.get(i);
%>
var	AcctArr<%=i%> =  "<%=hmAcctVal.get("DID")%>,<%=hmAcctVal.get("REPID")%>,<%=hmAcctVal.get("ID")%>,<%=hmAcctVal.get("ACNAME")%>" ;
<%
	}
%>

</script>
<style>
    .oddshade td, .evenshade td{
        white-space: nowrap;
    }

    </style>
 
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnPageLoad();" onkeyup="fnEnter();">
<html:form action="/gmTagSetInvMgt.do?method=tagSetInvReport" >
<html:hidden property="strOpt"/>
<html:hidden property="strTagIds"/>
<html:hidden property="tagAttrStatus"/>
<html:hidden property="tagAttrType"/>
<html:hidden property="cancelType"/>
<html:hidden property="pageType"/>

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable1100"  cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="6" height="25" class="RightDashBoardHeader">&nbsp;
				<fmtTagSetInvMgtRpt:message key="LBL_SET_IVENTORY_REPORT"/>
			</td>
			<td align="right" class=RightDashBoardHeader>
			<fmtTagSetInvMgtRpt:message key="IMG_ALT_HELP" var = "varHelp"/>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("SET_INVENTORY_REPORT")%>');" />
			</td>
		</tr>
		<tr><td colspan="7" class="Line" height="1"></td></tr>
		<tr class="evenshade" style="white-space: nowrap;">
			<fmtTagSetInvMgtRpt:message key="LBL_TAG_RANGE" var="varTag"/>
			<gmjsp:label type="BoldText"  SFLblControlName="${varTag} :" td="true"/>
			<td class="RightRedCaption">&nbsp;
				<html:text
					property="tagIdFrom" size="12" onkeypress="return isNumberKey(event);"
					onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
					onblur="changeBgColor(this,'#ffffff');" />&nbsp;&nbsp;
				<html:text
					property="tagIdTo" size="12" onkeypress="return isNumberKey(event);"
					onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
					onblur="changeBgColor(this,'#ffffff');" />
			</td>
			<fmtTagSetInvMgtRpt:message key="LBL_SET_ID" var="varSetId"/>
			<gmjsp:label type="BoldText"  SFLblControlName="&nbsp;${varSetId} :" td="true"/>
			<td class="RightRedCaption">&nbsp;
					<html:text
					property="setId" size="15"
					onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
					onblur="changeBgColor(this,'#ffffff');" />
			</td>
			<fmtTagSetInvMgtRpt:message key="LBL_CUR_LOCATION" var="varLocation"/>
			<gmjsp:label type="BoldText"  SFLblControlName="&nbsp;${varLocation} :" td="true"/>
			<td align ="left">&nbsp;
				<gmjsp:dropdown	controlName="currentLoc" SFFormName="frmGmTagSetInvMgt" SFSeletedValue="currentLoc" SFValue="alCurrentLoc" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" width="150" onChange="" />
			</td>
		</tr>
		<tr><td colspan="7" class="LLine" height="1"></td></tr>
		<tr  class="oddshade">
		<%--
			<gmjsp:label type="BoldText"  SFLblControlName="System :" td="true"/>
			<td height="25" align ="left">&nbsp;
				<gmjsp:dropdown	controlName="system" SFFormName="frmGmTagSetInvMgt" SFSeletedValue="system" SFValue="alSystem" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" onChange="" />
			</td>
 		--%>
 		<fmtTagSetInvMgtRpt:message key="LBL_FIELD_SALES" var="varFieldSales"/>
 			<gmjsp:label type="BoldText" SFLblControlName="${varFieldSales} :" td="true"/>
			<td class="RightRedCaption" style="padding-left: 4px;">
				<gmjsp:dropdown	controlName="distributorId" SFFormName="frmGmTagSetInvMgt" SFSeletedValue="distributorId" SFValue="alDistributor" codeId="ID" codeName="NM" defaultValue="[Choose One]" width="300" onChange="javascript:fnDistFilterReps(this, 'DIST');" />
			</td>
			<fmtTagSetInvMgtRpt:message key="LBL_ACCOUNT" var="varAccount"/>
			<gmjsp:label type="BoldText"  SFLblControlName="${varAccount} :" td="true"/>
			<td class="RightRedCaption" align ="left" colspan="1" style="padding-left: 4px;">
				<gmjsp:dropdown	controlName="accountId" SFFormName="frmGmTagSetInvMgt" SFSeletedValue="accountId" SFValue="alAccount" disabled="disabled" codeId="ID" codeName="ACNAME" defaultValue="[Choose One]" width="370" onChange="" />
			</td>
			<td class="RightRedCaption" align ="right">
				<div id="btnLoad">
					&nbsp;<fmtTagSetInvMgtRpt:message key="BTN_LOAD" var="varLoad"/>
					<gmjsp:button buttonType="Load" name="Load"  value="&nbsp;${varLoad}&nbsp;" onClick="fnLoad();"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</div>
			</td>
		</tr>
		<tr><td colspan="7" class="LLine" height="1"></td></tr>
		<tr class="evenshade">
			<td  colspan=7 height="25">
				<div id="divShow">
					&nbsp;<a href="javascript:fnShowMoreFilter()">
<fmtTagSetInvMgtRpt:message key="IMG_ALT_VIEW" var = "varView"/>
<img alt="${varView}" border="0" src="<%=strImagePath%>/plus.gif"/>&nbsp;<fmtTagSetInvMgtRpt:message key="LBL_SHOW_FILTER"/></a>
				</div>
			</td>
		</tr>
		<tr><td colspan="7" class="LLine" height="1"></td></tr>
		<tr class="oddshade" id="rowF1" style="display:none">
		<fmtTagSetInvMgtRpt:message key="LBL_ZONE" var="varZone"/>
			<gmjsp:label type="BoldText"  SFLblControlName="${varZone} :" td="true"/>
			<td height="25" >&nbsp;
				<gmjsp:dropdown	controlName="zone" SFFormName="frmGmTagSetInvMgt" SFSeletedValue="zone" SFValue="alZone" codeId="ID" codeName="NM" defaultValue="[Choose One]" width="250" onChange="javascript:fnFilterRegion(this, 'VP');" />
			</td>
			<fmtTagSetInvMgtRpt:message key="LBL_REGION" var="varRegion"/>
			<gmjsp:label type="BoldText"  SFLblControlName="${varRegion} :" td="true"/>
			<td align ="left">&nbsp;
				<gmjsp:dropdown	controlName="region" SFFormName="frmGmTagSetInvMgt" SFSeletedValue="region" SFValue="alRegion" codeId="ID" codeName="NM" defaultValue="[Choose One]" width="270" onChange="javascript:fnRegnFilterDist(this, 'AD');" />
			</td>
			<fmtTagSetInvMgtRpt:message key="LBL_STATUS" var="varStatus"/>
			<gmjsp:label type="BoldText"  SFLblControlName="${varStatus} :" td="true"/>
			<td align ="left">&nbsp;
				<gmjsp:dropdown	controlName="decommission" SFFormName="frmGmTagSetInvMgt" SFSeletedValue="decommission" SFValue="alDecommission"  codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" width="150" onChange="" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr id="rowL" style="display:none"><td colspan="7" class="LLine" height="1"></td></tr>
		<tr  class="evenshade" id="rowF2" style="display:none">
		<fmtTagSetInvMgtRpt:message key="LBL_SALES_REP" var="varSalesRep"/>
			<gmjsp:label type="BoldText"  SFLblControlName="&nbsp;${varSalesRep} :" td="true"/>
			<td class="RightRedCaption" align ="left">&nbsp;
				<gmjsp:dropdown	controlName="salesRepId" SFFormName="frmGmTagSetInvMgt" SFSeletedValue="salesRepId" disabled="disabled" SFValue="alSalesRep" codeId="ID" codeName="NM" defaultValue="[Choose One]" width="270" onChange="javascript:fnFilterAccts(this, 'REP');" />
			</td>
			<fmtTagSetInvMgtRpt:message key="LBL_SET_LOCK" var="varSetLock"/>
			<td class="RightRedCaption" align ="left">&nbsp;
				<gmjsp:dropdown	controlName="lockInv" SFFormName="frmGmTagSetInvMgt" SFSeletedValue="lockInv" SFValue="alLockInv" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" width="150"  />
			</td>
			<fmtTagSetInvMgtRpt:message key="LBL_DIVISION" var="varDivison"/>
			<gmjsp:label type="BoldText"  SFLblControlName="${varDivison} :" td="true"/>
			<td class="RightRedCaption" align ="left" style="padding-left: 2px;">
				<gmjsp:dropdown	controlName="divison" SFFormName="frmGmTagSetInvMgt" SFSeletedValue="divison" SFValue="alDivison" codeId="ID" codeName="NM" defaultValue="[Choose One]" width="180"  />
			</td>
			
			<td class="RightRedCaption" align ="right" colspan="3">&nbsp;
					<fmtTagSetInvMgtRpt:message key="BTN_LOAD" var="varLoad"/>
					<gmjsp:button buttonType="Load" name="Load"  value="&nbsp;${varLoad}&nbsp;" onClick="fnLoad();"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
		</tr>
		<tr><td colspan="7" class="LLine" height="1"></td></tr>
			<logic:notEqual name="frmGmTagSetInvMgt" property="pageType" value="report">
			<tr>	
				<td>
					<jsp:include page="/sales/InvManagement/GmTagSetInvInclude.jsp" />
				</td>	
			</tr>
			</logic:notEqual>
			<logic:equal name="frmGmTagSetInvMgt" property="pageType" value="report">
				<tr>
					<td colspan="7" >
						<div id="dataGridDiv" height="500px" width="1100px"></div>
					</td>
				</tr>
		<%if(GmCommonClass.parseNull(gridData).indexOf("cell")!=-1){%>
				<tr>
					<td colspan="7" align="center">
						<div class='exportlinks'><fmtTagSetInvMgtRpt:message key="DIV_EXPORT_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp;
							<a href="#" onclick="fnExcelExport();"> <fmtTagSetInvMgtRpt:message key="DIV_EXCEL"/> </a>
						</div>
					</td>
				</tr>
				<%} %>
			</logic:equal>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
<script>
 var loadInnerHtml = document.all.btnLoad.innerHTML;
</script>
</HTML>