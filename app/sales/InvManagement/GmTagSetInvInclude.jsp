 
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtTagSetInvInclude" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\InvManagement\GmTagSetInvInclude.jsp -->





<%

if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
} %>
<fmtTagSetInvInclude:setLocale value="<%=strLocale%>"/>
<fmtTagSetInvInclude:setBundle basename="properties.labels.sales.InvManagement.GmTagSetInvInclude"/>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<%

boolean strExport =true;
if(strClientSysType.equals("IPAD")){
	strExport =false;
}else{
	strExport =true;
}
%><tr>
			<td colspan="7">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<display:table name="requestScope.frmGmTagSetInvMgt.alResult" class="its" id="currentRowObject"  pagesize="5"
							export = "<%=strExport%>" decorator="com.globus.sales.InvManagement.displaytag.DTTagSetInvMgtRtpWrapper" requestURI="/gmTagSetInvMgt.do?method=tagSetInvReport" >
								<display:setProperty name="export.excel.decorator" value="com.globus.sales.InvManagement.displaytag.DTTagSetInvMgtRtpWrapper" /> 
								<fmtTagSetInvInclude:message key="DT_TAG" var="varTag"/>
								<display:column property="TAGID" title="${varTag}" style="width:9%" class="alignleft" sortable="true" media="html"/>
								<display:column property="TAG_ID" title="${varTag}" style="width:9%" class="alignleft" media="excel"/>
							 	<fmtTagSetInvInclude:message key="DT_SET_ID" var="varSetId"/>
							 	<display:column property="SET_ID" title="${varSetId}" style="width:5%" class="alignleft" sortable="true"/> 
								<fmtTagSetInvInclude:message key="DT_SET_DESCRIPTION" var="varSetDesc"/>
								<display:column property="SET_DESC" title="${varSetDesc}" style="width:30%"  class="alignleft" sortable="true"/>							
								<fmtTagSetInvInclude:message key="DT_CONSIGNED_TO" var="varConsign"/>
								<display:column property="OWNER" title="${varConsign}" style="width:10%" class="alignleft" sortable="true"/>
								<fmtTagSetInvInclude:message key="DT_REP_NAME" var="varRep"/>
								<display:column property="REP_NAME" title="${varRep}" style="width:10%" class="alignleft" sortable="true"/>
						<%--	<display:column property="CUR_LOCATION" title="Current<BR>Location" style="width:100;"  class="alignleft" sortable="true"/> --%>
								<fmtTagSetInvInclude:message key="DT_CURRENT_ADDRESS" var="varCurrAdd"/>
								<display:column property="CURADDR" title="${varCurrAdd}" style="width:15%" class="alignleft" sortable="true" media="html"/>								
								<display:column property="EXLCURADDR" title="${varCurrAdd}" style="width:15%" class="alignleft" media="excel"/>
								<fmtTagSetInvInclude:message key="DT_STATUS" var="varStatus"/>
								<display:column property="DECOMSIN" title="${varStatus}" style="width:3%" class="aligncenter" media="html"/>								
								<display:column property="EXLDECOMSIN" title="${varStatus}" style="width:3%" class="aligncenter" media="excel"/>
								<fmtTagSetInvInclude:message key="DT_LOCK" var="varLock"/>
								<display:column property="LOCKINV" title="${varLock}" style="width:3%" class="aligncenter" media="html"/> 								
								<display:column property="EXLOCKINV" title="${varLock}" style="width:3%" class="aligncenter" media="excel"/> 
							</display:table> 
						</td>
					</tr>
			</table> 
			</td>
		</tr>
		<logic:equal name="frmGmTagSetInvMgt" property="strOpt" value="Reload">
		<tr><td colspan="7" class="LLine" height="1"></td></tr>
		<tr height="40">
			<td colspan="7" align="center">
			<!-- Custom tag lib code modified for JBOSS migration changes -->
				<!--  When the Request is from the Report Link, the Edit Location,Lock,Decomission button should not be displayed. -->
				<logic:notEqual name="frmGmTagSetInvMgt" property="pageType" value="report">
				<fmtTagSetInvInclude:message key="BTN_EDIT_LOCATION" var="varEdit"/>
					<gmjsp:button buttonType="Load" name="Load"   value="${varEdit}" onClick="fnEditLoction();"/>&nbsp;&nbsp;
				
				
					<logic:equal name="frmGmTagSetInvMgt" property="lockAccessFl" value="Y">
					<fmtTagSetInvInclude:message key="BTN_LOCK_INVENTORY" var="varLock"/>
						<gmjsp:button buttonType="Load" name="Load"   value="${varLock}" onClick="fnLockInv();"/>&nbsp;&nbsp;
					</logic:equal>
					<logic:equal name="frmGmTagSetInvMgt" property="decomsAccessFl" value="Y">
						<fmtTagSetInvInclude:message key="BTN_DECOMMISSION" var="varDecommission"/>
						<gmjsp:button buttonType="Load" name="Load"   value="${varDecommission}" onClick="fnDecomission();"/>&nbsp;&nbsp;
					</logic:equal>
				
				</logic:notEqual>
			</td>
		</tr>
		</logic:equal>