 
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap" %>
<%@ page import="java.util.Date" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtUploadTagSetImage" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\InvManagement\GmUploadTagSetImage.jsp -->

<fmtUploadTagSetImage:setLocale value="<%=strLocale%>"/>
<fmtUploadTagSetImage:setBundle basename="properties.labels.sales.InvManagement.GmUploadTagSetImage"/>


<%
String strSalesInvMgntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_INVMANAGEMENT");
String strWikiTitle = GmCommonClass.getWikiTitle("UPLOAD_TAG_IMAGE");

String strAction = GmCommonClass.parseNull((String) request.getAttribute("hAction"));
String strFwdAction = GmCommonClass.parseNull((String) request.getAttribute("hFwdAction"));
String strTagID = GmCommonClass.parseNull((String) request.getAttribute("TAGID"));
String strImageID = GmCommonClass.parseNull((String) request.getAttribute("hImgId"));
String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
ArrayList alUploadinfo = GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("alUploadinfo"));

HashMap hmReturn = new HashMap();
String strImgPath = GmCommonClass.getString("SETIMGDIR");
String strTagDIR = "/GmCommonFileOpenServlet?uploadString=SETIMGDIR&appendPath=";
String strHeader="";
String strImgName = "";
String strImgID = "";
String strLastUpdatedBy = "";
String strLastUpdatedDate = "";
String strTypeName = "";
String strFileName ="";

int inUploadSize = alUploadinfo.size();
GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.sales.InvManagement.GmUploadTagSetImage", strSessCompanyLocale);
if(strAction.equals("Load")){
	strHeader =GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_UPLOAD_TAG_IMAGE")); 
}
if(strAction.equals("Large")){
	strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_VIEW_TAG_IMAGE")); 
	for(int i=0;i < inUploadSize;i++){
	hmReturn = (HashMap) alUploadinfo.get(i);
	 String strID = GmCommonClass.parseNull((String) hmReturn.get("ID"));
	 if(strID.equals(strImageID) || inUploadSize ==1){
	 strImgID = GmCommonClass.parseNull((String) hmReturn.get("ID"));	 
	 strImgName = GmCommonClass.parseNull((String) hmReturn.get("NAME"));
	 strLastUpdatedBy = GmCommonClass.parseNull((String) hmReturn.get("UNAME"));
	 strLastUpdatedDate = GmCommonClass.parseNull((String) hmReturn.get("CDATE"));
	 strTypeName = GmCommonClass.parseNull((String) hmReturn.get("MATYPE"));
	 strImgPath = "/GmCommonFileOpenServlet?uploadString=SETIMGDIR&appendPath="+strTypeName.replaceAll("/", "").toUpperCase()+"&sId="+strImgID;
	 }
	}
}
if(strAction.equals("View")){
	strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_VIEW_TAG_IMAGE"));
}
%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<TITLE> Globus Medical:Tag Image view</TITLE>
 <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strSalesInvMgntJsPath%>/GmUploadTagSetImage.js"></script>
<script>
var totalsize = '<%=alUploadinfo.size()%>';
</script>
</HEAD>
<BODY leftmargin="10" topmargin="10" onLoad="fnPageLoad();">
<form name="frmUploadTag" method="post" action = "<%=strServletPath%>/GmSetImgMgtServlet" enctype="multipart/form-data">
<input type="hidden" name ="hAction" value="<%=strAction%>" />
<input type="hidden" name ="hFwdAction" value="<%=strFwdAction%>" />
<input type="hidden" name ="hImgId" value=""/>
<input type="hidden" name ="TAGID" value="<%=strTagID%>"/>
<input type="hidden" name ="hInputStr" value=""/>
<input type="hidden" name ="hupDate" value=""/>
<input type="hidden" name ="hupUsername" value=""/>
<input type="hidden" name ="fileName" value="<%=strImgName%>" />
<input type="hidden" name="todo" value="upload"> 
<input type="hidden" name ="hRefType" value="11341" />

<!-- Custom tag lib code modified for JBOSS migration changes -->
<table border="0" class="DtTable800"  cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="3" class="RightDashBoardHeader">&nbsp;
				<%=strHeader%>
			</td>
			<td align="right" class=RightDashBoardHeader  > 	
					<fmtUploadTagSetImage:message key="IMG_ALT_HELP" var = "varHelp"/>
					<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
		</tr>
     <tr><td>&nbsp; </td></tr>					
						<%if(strAction.equals("Load") || strAction.equals("View")){
							if(alUploadinfo.size() > 0){
					for (int i = 0; i < inUploadSize; i++) {
						if(i%4==0){ 
							if(i==0){
							%>
							<tr>
						<%}else{
							%>
						</tr><tr height="40"><td colspan="4" height="40"> </tr><tr>
						<%} }
						HashMap hmLoop = (HashMap) alUploadinfo.get(i);
						 strImgName = GmCommonClass.parseNull((String) hmLoop.get("NAME"));
						 strImgID = GmCommonClass.parseNull((String) hmLoop.get("ID"));
						 strTypeName = GmCommonClass.parseNull((String) hmLoop.get("MATYPE"));
						 strLastUpdatedBy = GmCommonClass.parseNull((String) hmLoop.get("UNAME"));
						 strLastUpdatedDate = GmCommonClass.parseNull((String)hmLoop.get("CDATE"));
 						String source = strTagDIR+strTypeName.replaceAll("/", "").toUpperCase()+"&sId="+strImgID;
				%>
					<td width="150" >&nbsp;&nbsp;
						<img border="0" src="<%=source%>" height="100" width="80" onclick="javascript:fnOpenLarge(<%=strImgID%>);">
					<%if(strAction.equals("Load")){ %>
						<br>&nbsp;&nbsp;<input  name="chk<%=i%>" id="<%=strImgID%>" type="checkbox"/>
						<br>&nbsp;<b><fmtUploadTagSetImage:message key="LBL_UPLOADED_BY"/>:</b>&nbsp;<%=strLastUpdatedBy.replace(" ","&nbsp;")%>
						&nbsp;<b><fmtUploadTagSetImage:message key="LBL_UPLOADED_DATE"/>:</b>&nbsp;<%=strLastUpdatedDate%>
					<%} %> 
					</td>
				<% } %>
					</tr>
			<% }else
				{ %>
					<tr><td colspan="4" align="center"> <B><fmtUploadTagSetImage:message key="LBL_NO_TAG_IMAGE"/> </B></td></tr> 
			  <%} %>
			  	<tr height="20"><td colspan="4"></td></tr>
			<%if(strAction.equals("Load")){ %>
			<tr><td colspan="4" class="Line" height="1"></td></tr>
				<tr id="imageUpload" height="40" style="display:none">
				<td align="center" colspan="4" valign="middle">&nbsp;&nbsp;<b><fmtUploadTagSetImage:message key="LBL_"/>Select a file to upload :</b>
					&nbsp;<input type="file" name="uploadfile" size="50" " class="multi" accept="jpg" >&nbsp;&nbsp;&nbsp;&nbsp;
					<fmtUploadTagSetImage:message key="BTN_UPLOAD" var="varUpload"/>
					<gmjsp:button  name="Submit" value="${varUpload}"  buttonType="Save" onClick="fnUpload();"/>
				</td>
			</tr>
			<tr id="ipadFunction" height="40" style="display:none">
			<td colspan="4" valign="middle">&nbsp;
			<fmtUploadTagSetImage:message key="LBL_TAG_IMAGE_UPLOAD"/>
			</td>
			</tr>
			<tr><td colspan="4" class="Line" height="1"></td></tr>
				<tr height="40">
					<td colspan="4" align="center">
					<fmtUploadTagSetImage:message key="BTN_DELETE" var="varDelete"/>
						<gmjsp:button   value="${varDelete}" buttonType="Save" onClick="fnVoid();"/>&nbsp;&nbsp;
						<fmtUploadTagSetImage:message key="BTN_CLOSE" var="varClose"/>
						<gmjsp:button   value="${varClose}" buttonType="Load" onClick="fnClose();" />
					</td>
				</tr>
			<%} else { %>
			<tr height="40">
					<td colspan="4" align="center">
					<fmtUploadTagSetImage:message key="BTN_CLOSE" var="varClose"/>
					<gmjsp:button   value="${varClose}" buttonType="Load" onClick="fnClose();" />&nbsp;&nbsp;
					</td>
				</tr>
			
			<%    }
			  } %>
			<%if(strAction.equals("Large")){ %>
			
			<tr>
				 <td >&nbsp;&nbsp;
					<b><fmtUploadTagSetImage:message key="LBL_UPLOADED_BY"/>:</b> <%=strLastUpdatedBy%>
				</td>
				 <td >
					
				</td>
				 <td align="right">
					<b><fmtUploadTagSetImage:message key="LBL_UPLOADED_DATE"/>:</b> <%=strLastUpdatedDate%>
				</td>
			</tr>	
			<tr>			
				<td align="center" colspan="4">
				<br>
					<img   src="<%=strImgPath%>"  border="0" align="middle" height="350" width="350" />&nbsp;
				</td>
			</tr>
				<tr height="40"><td colspan="4"></td></tr>
		 <tr><td colspan="4" class="Line" height="1"></td></tr>
			<tr height="40">
				<td colspan="4" align="center">
				<fmtUploadTagSetImage:message key="BTN_BACK" var="varBack"/>
				<gmjsp:button name="back"  buttonType="Load"  value="${varBack}" onClick="fnBack();"/>
				</td>
			</tr>
						<%} %>
					
			
		</table>
</form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>
