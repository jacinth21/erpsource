
<%@ include file="/common/GmHeader.inc" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Globus Medical: Party Summary</title>
<script language="JavaScript" src="<%=strJsPath%>/party/GmParty.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>

<script>
function fnViewSetID(val, name)
{
	
	document.frmMain.hSetNumber.value = val;
	document.frmMain.hSetName.value = name;
	
	document.frmMain.action ="<%=strServletPath%>/GmVirtualSetMapServlet";
	document.frmMain.hAction.value = "Drilldown";
	document.frmMain.submit();
	
	//windowOpener('/GmVirtualSetMapServlet?hAction=Drilldown&hSetId='+val+'&hSetNm='+name,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=760,height=410");	
}

function fnViewPartByGroup(val, did, type )
{
	document.frmMain.hDistributorID.value = did;
	document.frmMain.hSetNumber.value = val;

	if (type == "BYSETPART") 	
	{
		document.frmMain.hAction.value = "LoadUnACons";
	}
	else
	{
		document.frmMain.hAction.value = "LoadGCons";
	}
		
	document.frmMain.action ="<%=strServletPath%>/GmVirtualSetMapServlet";
	document.frmMain.submit();
	
	//windowOpener('/GmVirtualSetMapServlet?hAction=LoadUnACons&hSetId='+val+'&hDID='+did,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=760,height=410");	
}

function fnViewSetActual(val, did)
{
	document.frmMain.Cbo_DistId.value = did;
	document.frmMain.hSetIds.value = val;
	document.frmMain.hOpt.value = 'BYSET';
	
	document.frmMain.action ="<%=strServletPath%>/GmSalesConsignSearchServlet";
	document.frmMain.hAction.value = "Reload";
	document.frmMain.submit();
	
}

function fnViewGroupPart(val, did, type)
{
	document.frmMain.Cbo_DistId.value = did;
	document.frmMain.hSetIds.value = val;

	if (type == "BYSETPART") 	
	{
		document.frmMain.hOpt.value = 'BYPART';
	}
	else
	{
		document.frmMain.hOpt.value = 'BYGPART';
	}
	
	document.frmMain.action ="<%=strServletPath%>/GmSalesConsignSearchServlet";
	document.frmMain.hAction.value = "Reload";
	document.frmMain.submit();
	
}

function fnViewSetDetails(val, did, type)
{

	document.frmMain.DistributorID.value = did;
	document.frmMain.SetNumber.value = val;
	document.frmMain.Cbo_DistId.value = did;
	document.frmMain.hSetIds.value = val;	
	document.frmMain.hOpt.value = 'BYSETPART';

	if (type == "BYSETPART") 	
	{
		document.frmMain.action ="<%=strServletPath%>/GmSalesConsignSearchServlet";
		document.frmMain.hAction.value = "Reload";		
	}
	else
	{
		document.frmMain.action ="<%=strServletPath%>/GmSalesVirtualCDetailServlet";
		document.frmMain.hAction.value = "CheckSharedSet";		
	}

	document.frmMain.submit();
	
}

function fnViewSetPartDetails(val, did)
{
	document.frmMain.Cbo_DistId.value = did;
	document.frmMain.hSetIds.value = val;
	document.frmMain.hOpt.value = 'BYSETPART';
	
	document.frmMain.action ="<%=strServletPath%>/GmSalesConsignSearchServlet";
	document.frmMain.hAction.value = "Reload";
	document.frmMain.submit();
	
}

function fnLoad()
{	//document.frmMain.Cbo_ToMonth.value = '<%=request.getAttribute("Cbo_ToMonth")%>';
	//document.frmMain.Cbo_ToYear.value = '<%=request.getAttribute("Cbo_ToYear")%>';
}


function fnGo()
{
	fnSetSelections();
	document.frmMain.submit();
}

function fnShowLoanerDetails(distid, id){
	var turnsmonth = document.frmMain.Cbo_Turns.value;
	windowOpener('/GmInHouseSetServlet?hAction=ViewUsageHistory&hTurnsMonth='+turnsmonth +'&distributorId='+distid+'&setId='+id, 'UsageDetails', 'resizable=yes,scrollbars=yes,top=40,left=50,width=900,height=600,align=center');
}


</script>

<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
</head>
<%
   ArrayList alSet = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALSHAREDSET"));
   String strDistributorId = GmCommonClass.parseNull((String)request.getAttribute("DISTRIBUTORID"));
System.out.println(" arraylist  " + alSet);
   int size = alSet.size();
   String strID = "basicinfo";
   String strRel = "basicinfocontentarea";
   String strSetId = "";
   String strHeader = "";
	HashMap hmSharedSetDtls = new HashMap();
 
%>
<body leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmMain" method="POST" action="<%=strServletPath%>/GmSalesGrowthServlet">
		<input type="hidden" name="hAction" value="">
		<input type="hidden" name="hOpt" value="">
		<input type="hidden" name="hDistributorID" value="<%=request.getParameter("hDistributorID")%>">
		<input type="hidden" name="DistributorID" >
		<input type="hidden" name="hRepID" value="<%=request.getParameter("hRepID")%>">
		<input type="hidden" name="hAreaDirector" value="<%=request.getParameter("hAreaDirector")%>">
		<input type="hidden" name="Cbo_DistId" value="">
		<input type="hidden" name="SetNumber">
		<input type="hidden" name="Cbo_SalesType" value="<%=request.getParameter("Cbo_SalesType")%>">
		<input type="hidden" name="Cbo_Turns" value="<%=request.getParameter("Cbo_Turns")%>">
		
		<!--   goes to GmSalesConsignSearchServlet -->
		<input type="hidden" name="hSetIds" value="">
		<input type="hidden" name="hId" value="">
		
		<!-- The following hidden fields are for Sales Drilldown (Additional Drill down)-->
		<input type="hidden" name="hSetNumber" value="<%=request.getParameter("hSetNumber")%>">
		<input type="hidden" name="hSetName" value="">
		<input type="hidden" name="hPartNumber" value="">
		

<%
   for(int i=0; i<size;i++)
   {
	   strID += i;
	   strRel += i;
	   hmSharedSetDtls = (HashMap)alSet.get(i);
	   strSetId = (String)hmSharedSetDtls.get("MAINID");
	   strHeader = (String)hmSharedSetDtls.get("TITLE");
	   
%>

<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="50%">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2">
					<jsp:include page="/GmSalesVirtualCDetailServlet" >
					<jsp:param name="hAction" value="LoadCons" />
					<jsp:param name="FromPage" value="CONTAINER" />					
					<jsp:param name="SetNumber" value="<%=strSetId%>" />
					<jsp:param name="DistributorID" value="<%= strDistributorId%>" />
					<jsp:param name="HEADER" value="<%=strHeader%>" />					
					</jsp:include>
					</td>
				</tr>				
			</table>			
		</td>
	  </tr>
</table>
<BR>
<%
   }
%>	
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>