<%
/**********************************************************************************
 * File		 		: GmGroupPartPricingSetup.jsp
 * Desc		 		: Setup the construct for a system
 * Version	 		: 1.0
 * author			: Ramdas Mayya
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import="java.util.ArrayList,java.util.*" %>
<% 
String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");
String strWikiTitle = GmCommonClass.getWikiTitle("OP_GROUP_PART_MAP");
%>

<%@ taglib prefix="fmtGroupPartPriceSetUp" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmGroupPartPriceSetUp.jsp -->

<fmtGroupPartPriceSetUp:setLocale value="<%=strLocale%>"/>
<fmtGroupPartPriceSetUp:setBundle basename="properties.labels.sales.pricing.GmGroupPartPriceSetUp"/>

<HTML>
<HEAD>
<TITLE> Globus Medical: Construct Builder </TITLE>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxCombo.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_form.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_markers.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_undo.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<link rel="STYLESHEET" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCombo/dhtmlxcombo.css">
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxCombo/ext/dhtmlxcombo_extra.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmGroupPartPricing.js"></script>
<!-- The Below JS file is imported for DHTMLX Popup Window  -->
<script src="<%=strJsPath%>/sales/pricing/GmPricingCommon.js"></script>
<bean:define id="gridData" name="frmGroupPartPricingForm" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="groupData" name="frmGroupPartPricingForm" property="gridXmlGroupData" type="java.lang.String"> </bean:define>
<bean:define id="sysData" name="frmGroupPartPricingForm" property="gridXmlSysData" type="java.lang.String"> </bean:define>
<bean:define id="groupTypeData" name="frmGroupPartPricingForm" property="gridXmlGroupTypeData" type="java.lang.String"> </bean:define>
<bean:define id="alGroupListDef" name="frmGroupPartPricingForm" property="alGroupList" type="java.util.List"></bean:define>
<bean:define id="advpViewAccess" name="frmGroupPartPricingForm" property="advpViewAccess" type="java.lang.String"> </bean:define>
<bean:define id="sysId" name="frmGroupPartPricingForm" property="systemListId" type="java.lang.String"> </bean:define>
<bean:define id="groupId" name="frmGroupPartPricingForm" property="groupId" type="java.lang.String"></bean:define>
<bean:define id="groupTypeId" name="frmGroupPartPricingForm" property="groupTypeId" type="java.lang.String"> </bean:define>
<bean:define id="strOpt" name="frmGroupPartPricingForm" property="strOpt" type="java.lang.String"> </bean:define>
<bean:define id="hsearch" name="frmGroupPartPricingForm" property="hsearch" type="java.lang.String"> </bean:define>
<bean:define id="strErrorGrpIds" name="frmGroupPartPricingForm" property="strErrorGrpIds" type="java.lang.String"> </bean:define>
<bean:define id="strNoData" name="frmGroupPartPricingForm" property="strNoData" type="java.lang.String"> </bean:define>
<bean:define id="alGroupList" name="frmGroupPartPricingForm" property="alGroupList" type="java.util.List"></bean:define>
<bean:define id="alSystemList" name="frmGroupPartPricingForm" property="alSystemList" type="java.util.List"></bean:define>
<bean:define id="alGroupTypeList" name="frmGroupPartPricingForm" property="alGroupTypeList" type="java.util.List"></bean:define>
<bean:define id="hsysID" name="frmGroupPartPricingForm" property="hsysID" type="java.lang.String"></bean:define>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/displaytag.css");
      div.gridbox_dhx_skyblue table.obj tr td{
     border-width:0px 1px 0px 0px;
     border-color : #A4BED4;
}
</style>
<script>

var grpCombo,sysCombo,grpTypCombo,mygrid;

var groupData = '<%=groupData%>';
var advpaccess = '<%=advpViewAccess%>';
var sysId = '<%=sysId%>';
var groupId = '<%=groupId%>';
var groupTypeId = '<%=groupTypeId%>';
var strOpt =  '<%=strOpt%>';
var hsearch = '<%=hsearch%>';
var strErrorGrpIds = '<%=strErrorGrpIds%>';
var noDataFound = '<%=strNoData%>';
var groupID = '<%=groupId%>';
var groupTypeID = '<%=groupTypeId%>';
var hsysID = '<%=hsysID%>';

function fnOnPageLoad() {
	var gridData = '<%=gridData%>';
	if(gridData.length>0)
	{
		initializeGrid(gridData,advpaccess);
	}
	/* var objSelect = document.frmGroupPartPricingForm.systemListId;
	  setSelectedValue(objSelect,hsysID); */
	
	  if(hsysID !=''){
		  document.frmGroupPartPricingForm.systemListId.value=hsysID;
		  fnReload();
	  }
	//Below is to carry the selected values for the  system list, Group type, Group type combo's.
	if(sysId){
		systemListId.setComboValue(sysId);}
	if(groupTypeId){
		groupTypeId.setComboValue(groupTypeID);}
	if(groupId){
		groupId.setComboValue(groupID);}
	if(hsearch != ''){
		document.frmGroupPartPricingForm.Cbo_Search.value = hsearch;}
	
	//When strOpt is empty, the grid has no data and it displays only the headers.Below is to hide the Header section of Grid. 
	if(strOpt == ''){
		document.getElementById('price_header').style.display = "none";
		document.getElementById('price_detail').style.display = "none";
		document.getElementById('filterDetails').style.height = "50";
	}else if(noDataFound == 'YES' && strOpt != ''){	
		document.getElementById('nodatafound').style.display = "block";
		document.getElementById('price_detail').style.display = "none";
	}
		systemListId.attachEvent("onOpen", onOpenComboFunc);
		groupTypeId.attachEvent("onOpen", onOpenComboFunc);
		groupId.attachEvent("onOpen", onOpenComboFunc);
				

	//Below function is to set the User notification alert message. 
	fnUserNotification();
}
</script>

</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad()"  onkeypress="fnEnter();" >
<html:form action="/gmGroupPartPricingDAction.do">
<html:hidden property="strOpt" />
<html:hidden property="hsearch" />
<html:hidden property="haction" value=""/>
<html:hidden property="hinputStringPart" value=""/>
<html:hidden property="hinputStringGroup" value=""/>

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtGroupPartPriceSetUp:message key="LBL_GRP_PART_PRICE_SETUP"/></td>
			<td align="right" class=RightDashBoardHeader > 	
				<fmtGroupPartPriceSetUp:message key="IMG_ALT_HELP" var = "varHelp"/>
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
		</tr>
		<tr><td colspan="4" height="1" class="line"></td></tr>
		<tr>
			<td height="150" valign="top" colspan="4" id="filterDetails">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					 <tr>
                     <td height="24" class="RightTableCaption" align="right" ><fmtGroupPartPriceSetUp:message key="LBL_SYSTEMS"/>  :</td>
						<td align="left">&nbsp;<gmjsp:autolist comboType= "DhtmlXCombo" controlName="systemListId"
										SFFormName="frmGroupPartPricingForm" SFSeletedValue="systemListId"
										SFValue="alSystemList" codeId="ID" codeName="NAME"
										defaultValue="[Choose One]" width="300" onBlur="onBlur"/> </td>	
						<td class="RightTableCaption" align="right" HEIGHT="28"></font><fmtGroupPartPriceSetUp:message key="LBL_GROUPS"/> :</td>
						<td align="left">&nbsp;<gmjsp:autolist comboType= "DhtmlXCombo" controlName="groupId"
										SFFormName="frmGroupPartPricingForm" SFSeletedValue="groupId"
										SFValue="alGroupList" codeId="CODEID" codeName="CODENM"
										defaultValue="[Choose One]" width="300" onBlur="onBlur"/> </td>
                   	
                     </tr>
                      <tr class="Shade">
                	  <td height="24" class="RightTableCaption" align="right"><fmtGroupPartPriceSetUp:message key="LBL_PRICED_BY"/> :</td>
						<td> &nbsp;<gmjsp:dropdown controlName="pricedTypeId" SFFormName="frmGroupPartPricingForm" SFSeletedValue="pricedTypeId"
										SFValue="alPricedTypeList" codeId = "CODEID"  codeName = "CODENM"   defaultValue= "[Choose One]" /> 		
						</td>
                     	<td height="24" class="RightTableCaption" align="right" > <fmtGroupPartPriceSetUp:message key="LBL_GROUP_TYPE"/> :</td>
						<td align="left">&nbsp;<gmjsp:autolist comboType= "DhtmlXCombo" controlName="groupTypeId"
										SFFormName="frmGroupPartPricingForm" SFSeletedValue="groupTypeId"
										SFValue="alGroupTypeList" codeId="CODEID" codeName="CODENM"
										defaultValue="[Choose One]" width="300" onBlur="onBlur"/> </td>

                    </tr>   
                     <tr>
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtGroupPartPriceSetUp:message key="LBL_PART"/> :</td> 
                        <td colspan="2">&nbsp;<html:text property="partNumbers"  size="20" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>&nbsp; 
                        	 <select name="Cbo_Search" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
								  	<option value="0" ><fmtGroupPartPriceSetUp:message key="LBL_CHOOSE_ONE"/>
								  	<option value="LIT" ><fmtGroupPartPriceSetUp:message key="LBL_LITERAL"/>
								  	<option value="LIKEPRE" ><fmtGroupPartPriceSetUp:message key="LBL_PREFIX"/>
								  	<option value="LIKESUF" ><fmtGroupPartPriceSetUp:message key="LBL_SUFFIX"/>
							 </select>	&nbsp;&nbsp; &nbsp;&nbsp;   </td><td>&nbsp;
								  <!-- Below Filter button is commented for PMT-2479. --> 		
			 				<!-- <gmjsp:button value="Filter" gmClass="Button" buttonType="Load"  onClick="fnFilter();" /> -->
							<fmtGroupPartPriceSetUp:message key="BTN_LOAD" var="varLoad"/>
							<gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" controlId="Btn_Load" style="width: 6em"  buttonType="Load"  gmClass="Button"   onClick="fnReload();" /></td>
	                        <!-- MNTTASK-7037 - Active Status Filter Not Working Properly So we have used Filter similar Pricing Dashboard  -->
	                        
						<%--  <td height="24" class="RightTableCaption" align="right">&nbsp;Status :</td> 
						<td> &nbsp;&nbsp; <gmjsp:dropdown controlName="statusList" SFFormName="frmGroupPartPricingForm" SFSeletedValue="statusList"
										SFValue="alStatusList" codeId = "CODEID"  codeName = "CODENM"   defaultValue= "[Choose One]" onChange="fnFilter();" />  		
						&nbsp; <gmjsp:button value="Filter" gmClass="Button" buttonType="Load"  onClick="fnFilter();" />
						</td>	 --%>			
                     </tr>                 
                    <tr><td colspan="4" class="ELine"></td></tr>
                    <tr class="ShadeRightTableCaption" id="price_header">
							<td Height="24" colspan="4">&nbsp;<fmtGroupPartPriceSetUp:message key="LBL_PRICE_DETAIL"/></td>
					</tr>
					<tr><td><div id="nodatafound" style="display: none;"><font color=blue size=1>&nbsp;<fmtGroupPartPriceSetUp:message key="MSG_NO_DATA"/>!</font></div></td></tr>
                    <tr id="price_detail" >
                    <td colspan="4">
                    <table  border="0" cellspacing="0" cellpadding="0" >
                    <tr><td colspan="4" class="lline"></td></tr>
					<tr>
					<td  align="left" colspan="4" height="20">
						<table cellpadding="1" cellspacing="1" border="0"  width="470" >
							<TR>
									<td>
									&nbsp;<fmtGroupPartPriceSetUp:message key="IMG_ALT_COPY" var = "varCopy"/><img src="<%=strImagePath%>/dhtmlxGrid/copy.gif" onClick="javascript:docopy()" alt="${varCopy}" style="border: none;cursor:hand;" height="16">	
									&nbsp;<fmtGroupPartPriceSetUp:message key="IMG_ALT_PASTE" var = "varPaste"/><img src="<%=strImagePath%>/dhtmlxGrid/paste.gif" alt="${varPaste}" style="border: none;cursor:hand;" onClick="javascript:pasteToGrid()" height="16">	
									&nbsp;<fmtGroupPartPriceSetUp:message key="IMG_ALT_REDO" var = "varRedo"/><img src="<%=strImagePath%>/dhtmlxGrid/redo.gif" alt="${varRedo}" onClick="javascript:doredo();" style="border: none;cursor:hand;" height="16">
									&nbsp; <fmtGroupPartPriceSetUp:message key="IMG_ALT_UNDO" var = "varUndo"/><img src="<%=strImagePath%>/dhtmlxGrid/undo.gif" alt="${varUndo}" onClick="javascript:doundo();" style="border: none;cursor:hand;" height="16">									
									&nbsp; <fmtGroupPartPriceSetUp:message key="IMG_ALT_ROWS" var = "varRows"/><img src="<%=strImagePath%>/dhtmlxGrid/row_add_after_1.gif" alt="${varRows}" style="border: none;cursor:hand;" onClick="javascript:addRowFromClipboard()" height="16">
									&nbsp; <fmtGroupPartPriceSetUp:message key="IMG_ALT_ENABLE" var = "varEnable"/><img src="<%=strImagePath%>/dhtmlxGrid/highlight.gif" alt="${varEnable}" style="border: none;cursor:hand;" onClick="javascript:enableMarker()"  height="16">
									&nbsp;<fmtGroupPartPriceSetUp:message key="IMG_ALT_DISABLE" var = "varDisable"/> <img src="<%=strImagePath%>/dhtmlxGrid/disable-highlight.gif" alt="${varDisable}" style="border: none;cursor:hand;" onClick="javascript:disableMarker()" height="16">
									&nbsp;<input type="text"  id="applyToMarked" size="8" maxlength="10"  onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="9"/>
										&nbsp;&nbsp;<fmtGroupPartPriceSetUp:message key="BTN_APPLY" var="varApply"/>
										<gmjsp:button value="${varApply}" tabindex="10" gmClass="Button" buttonType="Save" onClick="javascript:applyToMarkedCell();" />										
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div id="progress" style="display:none"><img id="status" src=""  style="border: none;" height="16"> </div><div id="msg" style="display:none;color:blue;font-weight:bold;"></div>
									</td>										
								</TR>
					
						</table>
					</td>				
			 </tr>
			 	   <tr><td colspan="4" class="lline"></td></tr>
				   <tr>
			            <td colspan="4" valign="top">
			            <div id="mygrid_container" style="height:500px;width:1100px;"></div>
						</td>
	    	       </tr> 
    			   <tr><td colspan="4" class="ELine"></td></tr> 
    			   <tr class="ShadeRightTableCaption">
    			   	   <td Height="24" colspan="3">&nbsp;<font color="red">*</font><fmtGroupPartPriceSetUp:message key="LBL_COMMENTS"/></td>
				   </tr>
				   <tr><td colspan="4" class="ELine"></td></tr> 
				   <tr>
	   				   <td colspan="4" height="50" >
	   				   <textarea id="comments" name="comments" value="" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');if(TRIM(this.value)=='')this.value='';" onBlur="changeBgColor(this,'#ffffff');"  rows=3 cols=100>
	   				   </textarea></td>
				   </tr>
    			   <tr><td colspan="4" class="ELine"></td></tr>
              	   <tr height="30">
              		 	<td colspan="4" align="center">
              		 	<fmtGroupPartPriceSetUp:message key="BTN_UPDATE" var="varUpdate"/>
              		 	<gmjsp:button controlId="idSubmit" value="${varUpdate}" gmClass="Button" buttonType="Save" onClick="fnSubmit();" />
              		 	</td>
	               </tr></table></td></tr>
	  		 	</table>
  			   </td>
  		  </tr>	
    </table>	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>

