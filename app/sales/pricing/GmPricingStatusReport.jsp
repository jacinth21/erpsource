

<%
	/**********************************************************************************
	 * File		 		: GmPricingStatusReport.jsp
	 * Desc		 		: fetch all the request by selected filters
	 * Version	 		: 1.0
	 * author			: Xun
	 ************************************************************************************/
%>

<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtPricingStatusReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmPricingStatusReport.jsp -->

<fmtPricingStatusReport:setLocale value="<%=strLocale%>"/>
<fmtPricingStatusReport:setBundle basename="properties.labels.sales.pricing.GmPricingStatusReport"/>



<bean:define id="ldtResult" name="frmPricingStatusReport" property="ldtResult" type="java.util.List"></bean:define>
<bean:define id="alVPId" name="frmPricingStatusReport" property="alVPId" type="java.util.List"></bean:define>
<bean:define id="gridPriceRequestStatus" name="frmPricingStatusReport" property="priceRequestStatusXML" type="java.lang.String"></bean:define>
<bean:define id="selectedVP" name="frmPricingStatusReport" property="selectedVP" type="java.lang.String"></bean:define>
<%
String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");
	String strWikiTitle = GmCommonClass.getWikiTitle("PRICING_STATUS_REPORT");
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Pricing Status Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmPricingStatusReport.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

<%-- <link rel="STYLESHEET" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.css"> 
<script src="<%=strJsPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.js"></script> --%>
 
 <!-- dhtmlxGrid -->
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid_form.js "></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<style type="text/css" media="all">
@import url("<%=strCssPath%>/displaytag.css");

.grid_hover {
	background-color: #E6E6FA;
	font-size: 20px;
}

div.gridbox_dhx_skyblue table.obj tr td {
	border-width: 0px 1px 0px 0px;
	border-color: #A4BED4;
}
</style>

<%

	String strAccessLvl = GmCommonClass.parseNull((String)session.getAttribute("strSessAccLvl"));
	int intAccessLvl = Integer.parseInt(GmCommonClass.parseZero(strAccessLvl));
%>

<script>
 
var previousCheckedVP = '<%=selectedVP%>';
<%-- var previousCheckedAD = '<%=selectedAD%>';
var previousCheckedFS = '<%=selectedFS%>';
var previousCheckedAC = '<%=selectedAC%>';
 --%>
 var opt = '<bean:write name="frmPricingStatusReport" property="strOpt"/>';
 
var arrAccountIn = new Array(1);
var ADArr = new Array(1);
var DistArr = new Array(1);
var accessLevel= '<%=intAccessLvl%>'

 function doOnRowSelect(rowId,cellIndex){
		  if (cellIndex==1){
			fnOpenPricingInitiate(rowId);
			}
		}  

function fnOpenPricingInitiate(reqid,strAccType,StrGroupAcc,StrGroupAccID){
	if(strAccType == undefined)
		strAccType = 0;
	if(StrGroupAccID == undefined)
		StrGroupAccID = "";
	//alert(reqid+"::::"+strAccType+"::::"+StrGroupAcc+":::"+StrGroupAccID);
	this.location.href="/gmPricingRequestDAction.do?method=openPriceRequest&fchAllSysFl=Y&strRequestId="+reqid+"&strAccType="+strAccType+"&strGroupAcc="+StrGroupAccID;
}

function fnOpenSystemPricingInitiate(reqid,accid,systemid,groupid){
var varFchAllSysFl = ''; 
if(reqid != '' ){
varFchAllSysFl = 'Y';
}
if(groupid==undefined){
	groupid = '';
}
	this.location.href="/gmPricingRequestDAction.do?method=openPriceRequest&sysGrpId="+groupid+"&fchAllSysFl="+varFchAllSysFl+"&sysTranId="+systemid+"&strAccountId="+accid+"&strRequestId="+reqid;
}
 function fnOnPageLoad() {		
	var gridPriceRequestStatusData = '';
	var gridPendingSystemAccountPriceData = '';
	var gridPendingGroupAccountPriceData = '';
	/* var pendsys = '<bean:write name="frmPricingStatusReport" property="pendingSystemAccountCount"/>';
	var pendGroup = '<bean:write name="frmPricingStatusReport" property="pendingGroupAccountCount"/>';
	 */
	 if(opt != 'reload'){	
	
		fnSelectAll(document.frmPricingStatusReport.selectVPAll,'checkVPs','selectAll');
		fnSelectStatusOnLoad();
		// Commented the below condition for MNTTASK-2632. should not load the data before select any account.
		/*if(opt==''||opt==null){
			fnLoad();
		}*/
		  return;
	}
	opt ='';		
	
var gridPendingGroupAccountPriceData = '<%=gridPriceRequestStatus%>';
	
	if (gridPendingGroupAccountPriceData != ''){
		
		gridObj = initGrid('dataGridDiv',gridPendingGroupAccountPriceData);
		gridObj.enableTooltips("true,true,true,true,true,true,true,true,true,true,true,true");
		// Added fillter Type,Account Id,Account Name,Contract,Group Account,Required Approval,Status,	Sales Rep Name,	AD Name,VP Name,Initiated By,Created Date
		gridObj.attachHeader('#select_filter,#text_filter,#text_filter,#select_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#rspan');
		//gridObj.sortRows(3, 'str','des');				

		
		
	}
	
}
 
 

	
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad()">
<html:form action="/gmPricingStatusReport.do?method=pricingStatusReport">

	<html:hidden property="strOpt" />
	<html:hidden property="strDetailFlag" />
	<html:hidden property="hinputstring" />

	<table border="0" class="DtTable950" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtPricingStatusReport:message key="LBL_PRICING_DASHBOARD"/></td>

			<td align="right" class=RightDashBoardHeader><fmtPricingStatusReport:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>

		</tr>
		<tr>
			<td colspan="4" height="1" class="line"></td>
		</tr>
		<tr>
			<td width="950" height="100" valign="top" colspan="2">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">

				<tr>
					<td colspan="5" class="Line"></td>
				</tr>

				<tr>

					<td height="24" class="RightTableCaption" align="right"><fmtPricingStatusReport:message key="LBL_VP"/>:&nbsp;</td>
					<td>
						<DIV class="gmFilter"  id="Div_VP"  style="width:280px;height:85px">
						  <table> <tr><td bgcolor="gainsboro" > <html:checkbox property="selectVPAll" onclick="fnSelectAll(this,'checkVPs','toggle');" /><B><fmtPricingStatusReport:message key="LBL_SELECT_ALL"/> </B> </td>
							</tr></table> 
							 <table cellspacing="0" cellpadding="0">
								
								 <logic:iterate id="allVP" name="frmPricingStatusReport" property="alVPId">
								<tr>
									<td><htmlel:multibox property="checkVPs" value="${allVP.ID}" onclick="fnSelectVP();"  />
										 <bean:write name="allVP" property="NM" /></td>
								</tr>
								</logic:iterate>
							
								 </table>
								</div>
						</td>
					<td height="24" class="RightTableCaption" align="right"><fmtPricingStatusReport:message key="LBL_STATUS"/>:&nbsp;</td>					
					<td>
						<DIV class="gmFilter" id="Div_Status" style="width: 280px; height: 85px">
							<table>
								<tr>
									<td bgcolor="gainsboro"><html:checkbox property="selectStatusAll" onclick="fnSelectAll(this,'checkStatus','toggle');" /><B><fmtPricingStatusReport:message key="LBL_SELECT_ALL"/> </B></td>
								</tr>
							</table>
							<table cellspacing="0" cellpadding="0">
		
								<logic:iterate id="allStatus" name="frmPricingStatusReport" property="alStatus">
									<tr>
										<td><htmlel:multibox property="checkStatus" value="${allStatus.CODEID}" onclick="fnSelectAllToggle('selectStatusAll','checkStatus');" />
										<bean:write name="allStatus" property="CODENM" /></td>
									</tr>
								</logic:iterate>
		
							</table>
						</div>					
					</td>						
					<td><fmtPricingStatusReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button style="width: 5em; height: 2em; font-size: 13px" value="${varLoad}" name="loadbutton" buttonType="Load" gmClass="button" onClick="fnLoad();" /></td>
				</tr>				
				
                <tr>
					<td colspan="4" >&nbsp;</td>
				</tr>                     
				<logic:equal name="frmPricingStatusReport" property="strOpt" value="reload">
				<tr>
					<td height="25" class="RightDashBoardHeader" colspan="7"><fmtPricingStatusReport:message key="LBL_APPROVAL_PENDING"/></td>
				</tr>
					<tr>
						<td colspan="7" width="100%">
							<div id="dataGridDiv" style="height: 500px;"></div>
						</td>
					</tr>
				</logic:equal>
			</table>
			</td>
		</tr>
		
		<tr>
			<td colspan="6" align="center" height="25">&nbsp;
				<% if(!gridPriceRequestStatus.equals("") && strClientSysType.equals("PC")){%>
				<div class='exportlinks'><fmtPricingStatusReport:message key="DIV_EXPORT_OPT"/> : <img	src='img/ico_file_excel.png'/>&nbsp;<a href="#" onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> <fmtPricingStatusReport:message key="DIV_EXCEL"/>
				 </a></div>
				<% }%>
			</td>
		</tr>
		
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>

