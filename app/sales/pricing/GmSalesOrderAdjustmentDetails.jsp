 <%@page import="com.globus.common.util.GmCookieManager"%>
<%
/***************************************************************************************
 * File		 		: GmSalesOrderAdjustmentDetails.jsp
 * Desc		 		: This screen is used for displayimg Sales Order Adjustment Details
 * Version	 		: 1.0
 * author			: HReddi
*****************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.sales.pricing.forms.GmPartPriceAdjRptForm"%>
<!-- sales\pricing\GmSalesOrderAdjustmentDetails.jsp -->

<%@ taglib prefix="fmtSalesOrdAdjDtls" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtSalesOrdAdjDtls:setLocale value="<%=strLocale%>"/>
<fmtSalesOrdAdjDtls:setBundle basename="properties.labels.sales.pricing.GmSalesOrderAdjustmentDetails"/>

<bean:define id="xmlGridData" name="frmPartPriceAdjRpt" property="xmlGridData" type="java.lang.String"> </bean:define>
<%
String strBeforeTotal = "";
String strAdjustAmount = "";
String strAfterTotal = "";
double dbBeforeItemTotal = 0.0;
double dbBeforePriceTotal = 0.0;
double dbAfterItemTotal = 0.0;
double dbAfterPriceTotal = 0.0;

double dbPortalPriceTotal = 0.0;
double dbPortalPrice = 0.0;

double dbBeforePrice = 0.0;
double dbAfterPrice = 0.0;
double dbAdjustPrice = 0.0;

String strBeforeItemTotal = "";
String strAfterItemTotal = "";
String strAfterPrice = "";
String strItemQty = "";
int intQty = 0;
int intPreQty = 0;
HashMap hmReturn = new HashMap();
hmReturn =  GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HEADERDETAILS"));
strBeforeTotal = GmCommonClass.parseZero((String)hmReturn.get("BEFORETOTAL"));
strAfterTotal = GmCommonClass.parseZero((String)hmReturn.get("AFTERTOTAL"));
strAdjustAmount = GmCommonClass.parseZero((String)hmReturn.get("ADJUSTMENTAMUNT"));
ArrayList alOrderSummary = new ArrayList();
alOrderSummary = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ORDERSUMMARY"));

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Sales Order Adjustment Details </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script>
var objGridData;
objGridData = '<%=xmlGridData%>';

function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableTooltips("true,true,true,true,true,true,true,true,true,true,true");
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}

function fnOnPageLoad() {
	if (objGridData != '') {
		gridObj = initGridData('AdjustmentReport',objGridData);
	}
}

</script>
</HEAD>
<%
String strWikiTitle = GmCommonClass.getWikiTitle("ADJUSTMENT_DETAILS"); 
%>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();" >
<html:form action="/gmPartPriceAdjustmentRptAction.do?method=fetchAdjustmentDetails">
	<table class="DtTable1000" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="7">&nbsp;<fmtSalesOrdAdjDtls:message key="LBL_ADJ_DTLS"/></td>
			<fmtSalesOrdAdjDtls:message key="IMG_ALT_HELP" var = "varHelp"/>
			<td class="RightDashBoardHeader"><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
		</tr>	
			<%if(!xmlGridData.equals("")){%>
				<tr>
					<td colspan="8">
						<div  id="AdjustmentReport" style="" height="300px" width="1004px"></div>						
					</td>
				</tr>
				<tr>
					<td height="1" colspan="8" class="LLine"></td>
				</tr>			
			<%}else{%>
				<tr>
					<td colspan="8" height="1" class="LLine"></td></tr>
				<tr>
					<td colspan="8" align="center" height="30" class="RightText">
					<fmtSalesOrdAdjDtls:message key="LBL_NO_DATA"/>
					</td>
				</tr>
			<%}%>
			
			<%	
					int intSize = 0;
				    HashMap hcboVal = null;
				    String strQty = "";
				    String strPrice = "";
				    String strUnitPrice = "";
				    String strNetUnitPrice = "";
				    String strPortalPrice = "";
			  		intSize = alOrderSummary.size();
					hcboVal = new HashMap();					
			  		for (int i=0;i<intSize;i++){
			  			hcboVal = (HashMap)alOrderSummary.get(i);
			  			strQty = GmCommonClass.parseZero((String)hcboVal.get("QTY"));
			  			strPrice = GmCommonClass.parseZero((String)hcboVal.get("PRICE"));
						strUnitPrice = GmCommonClass.parseZero((String)hcboVal.get("UPRICE"));												
						strNetUnitPrice = GmCommonClass.parseZero((String)hcboVal.get("NETUNITPRICE"));
						strPortalPrice =  GmCommonClass.parseZero((String)hcboVal.get("CURRPORTPRICE"));
						
						intQty = Integer.parseInt(strQty)+ intPreQty;
						strItemQty = ""+intQty;

						dbAfterItemTotal = Double.parseDouble(strPrice);
						dbAfterItemTotal = intQty * dbAfterItemTotal; // Multiply by Qty
						strAfterItemTotal = ""+dbAfterItemTotal;
						dbAfterPriceTotal = dbAfterPriceTotal + dbAfterItemTotal;
						strAfterPrice = "" + 	dbAfterPriceTotal;	
						
						dbBeforeItemTotal = Double.parseDouble(strUnitPrice);
						dbBeforeItemTotal = intQty * dbBeforeItemTotal; // Multiply by Qty
						strBeforeItemTotal = ""+dbBeforeItemTotal;
						dbBeforePriceTotal = dbBeforePriceTotal + dbBeforeItemTotal;
						strBeforeTotal = "" + 	dbBeforePriceTotal;
						
						dbBeforePrice = Double.parseDouble(strBeforeTotal);
						dbAfterPrice = Double.parseDouble(strAfterPrice);						
						dbAdjustPrice = dbAfterPrice - dbBeforePrice;						
						strAdjustAmount = ""+dbAdjustPrice;				
						
						dbPortalPrice = Double.parseDouble(strPortalPrice);
						dbPortalPriceTotal =dbPortalPriceTotal +( intQty * dbPortalPrice);
						strPortalPrice =""+dbPortalPriceTotal; 
						
					}
						
%>
<tr>
			<td width="20%" colspan="5"> </td>
			<td  height="20" colspan="3">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="Right" class="RightTableCaption" width="80%"><fmtSalesOrdAdjDtls:message key="LBL_TOTAL_PORTAL_PRICE"/>:&nbsp;&nbsp;</td>
						<td  Height="25"  class="RightTableCaption" align="right" >&nbsp;<gmjsp:currency td="false" textValue="<%=strPortalPrice%>" width="15%"  gmClass="RightTableCaption"  type="BoldCurrTextSign"/>
						</td>						
					</tr>
				</table>
			</td>				
		</tr>
		
		<tr>
			<td width="20%" colspan="5"> </td>
			<td  height="20" colspan="3">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="Right" class="RightTableCaption" width="80%"><fmtSalesOrdAdjDtls:message key="LBL_TOTAL_BEFORE_ADJ"/>:&nbsp;&nbsp;</td>
						<td  Height="25"  class="RightTableCaption" align="right" >&nbsp;<gmjsp:currency td="false" textValue="<%=strBeforeTotal%>" width="15%"  gmClass="RightTableCaption"  type="BoldCurrTextSign"/>
						</td>						
					</tr>
				</table>
			</td>				
		</tr>
			<tr>
			<td width="20%" colspan="5"></td>
			<td  height="20" colspan="3">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="Right" class="RightTableCaption" width="80%"><fmtSalesOrdAdjDtls:message key="LBL_ADJS"/>:&nbsp;&nbsp;</td>
						<td  Height="25"  class="RightTableCaption" align="right" >&nbsp;<gmjsp:currency td="false" textValue="<%=strAdjustAmount%>" width="15%"  gmClass="RightTableCaption"  type="BoldCurrTextSign"/>
						</td>						
					</tr>
				</table>
			</td>				
		</tr>
			<tr>
			<td width="20%" colspan="5"></td>
			<td  height="20" colspan="3">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="Right" class="RightTableCaption" width="80%"><fmtSalesOrdAdjDtls:message key="LBL_TOTAL_AFTER_ADJ"/>:&nbsp;&nbsp;</td>
						<td  Height="25"  class="RightTableCaption" align="right" >&nbsp;<gmjsp:currency td="false" textValue="<%=strAfterPrice%>" width="15%"  gmClass="RightTableCaption"  type="BoldCurrTextSign"/>
						</td>						
					</tr>
				</table>
			</td>				
		</tr>			
		<tr>
			<td height="1" colspan="8" class="LLine"></td>
		</tr>
		<tr>
			<fmtSalesOrdAdjDtls:message key="BTN_CLOSE" var = "varClose"/>
			<td colspan="8" height="30" align="center"><gmjsp:button value="${varClose}"  name="Btn_Close"  gmClass="Button" buttonType="Load"  onClick="window.close();" />&nbsp;</td>
		</tr>					
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
