
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!-- \accounts\GmIncRebateTierSetup.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtrebateMgnt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page isELIgnored="false" %>
<fmtrebateMgnt:setBundle basename="properties.labels.sales.pricing.GmRebateSetup"/>
<%
String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");	
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"
	href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmRebateSetup.js"></script>	

<title>Rebate Management Tier Setup</title>
</head>
<body leftmargin="20" topmargin="10" >
	<html:form action="/gmRebateSetup.do?method=loadRebateManagmentDtls">
	
	
		<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
			    <tr class="Shade">
			   <td class="RightTableCaption" align="left" height="30">&nbsp;<fmtrebateMgnt:message key="LBL_LAYER"/></td>
			     <td class="RightTableCaption" align="left" height="30"><fmtrebateMgnt:message key="LBL_PERCENTAGE" /></td>
			      <td class="RightTableCaption" align="left" height="30"><fmtrebateMgnt:message key="LBL_FROM_AMOUNT"/></td>
			       <td class="RightTableCaption" align="left" height="30"><fmtrebateMgnt:message key="LBL_TO_AMOUNT"/></td>
			    </tr>
			    
			  	<tr>
					<td class="LLine" colspan="4" height="1"></td>
				</tr>
					
			    <tr >
			    <td>&nbsp;<fmtrebateMgnt:message key="LBL_TIER1"/>:</td>
			     <td class="RightTableCaption" align="left" height="30"><html:text name="frmRebateSetup"  property="tier1Percent" tabindex="7" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/> </td>
			    <td class="RightTableCaption" align="left" height="30"><html:text name="frmRebateSetup"  property="tier1FromAmt" tabindex="8" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/></td>
			     <td class="RightTableCaption" align="left" height="30"><html:text name="frmRebateSetup"  property="tier1ToAmt" tabindex="9" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/></td>
			    </tr>
			    
			   	<tr>
					<td class="LLine" colspan="4" height="1"></td>
				</tr>
				
			      <tr class="Shade">
			     <td>&nbsp;<fmtrebateMgnt:message key="LBL_TIER2"/>:</td>
			      <td class="RightTableCaption" align="left" height="30"><html:text name="frmRebateSetup"  property="tier2Percent" tabindex="10" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/> </td>
			    <td class="RightTableCaption" align="left" height="30"><html:text name="frmRebateSetup"  property="tier2FromAmt" tabindex="11" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/></td>
			     <td class="RightTableCaption" align="left" height="30"><html:text name="frmRebateSetup"  property="tier2ToAmt" tabindex="12" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/></td>
			    </tr>
			     
			     <tr>
					<td class="LLine" colspan="4" height="1"></td>
				</tr>
			      <tr>
			    <td>&nbsp;<fmtrebateMgnt:message key="LBL_TIER3"/></td>
			      <td class="RightTableCaption" align="left" height="30"><html:text name="frmRebateSetup"  property="tier3Percent" tabindex="13" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/> </td>
			    <td class="RightTableCaption" align="left" height="30"><html:text name="frmRebateSetup"  property="tier3FromAmt" tabindex="14" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/></td>
			     <td class="RightTableCaption" align="left" height="30"><html:text name="frmRebateSetup"  property="tier3ToAmt" tabindex="15" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/></td>
			    </tr> 
			    <tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr> 
			 </table>
			


	</html:form>
</body>

</html>