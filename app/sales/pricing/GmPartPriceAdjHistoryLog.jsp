<%
/**********************************************************************************
 * File		 		: GmAuditTrailDetail.jsp
 * Desc		 		: display required date history info
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ taglib prefix="fmtPartPriceAdjHistoryLog" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmPartPriceAdjHistoryLog.jsp -->

<fmtPartPriceAdjHistoryLog:setLocale value="<%=strLocale%>"/>
<fmtPartPriceAdjHistoryLog:setBundle basename="properties.labels.sales.pricing.GmPartPriceAdjHistoryLog"/>
  
<bean:define id="ldtHistoryResult" name="frmPartPriceAdjRpt" property="ldtHistoryResult" type="java.util.List"></bean:define>
 
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Mapping </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
 
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<%

String value="Value";
String transId="Transaction ID";
String strScreenHeader = "Part Price Adjustment History Log";
String strAlign ="alignleft";

String strTimeZone = strGCompTimeZone; 
String strDTDateFmt = "{0,date,"+strGCompDateFmt+"}"; 


boolean exportValue=false;

%>
 

</HEAD>

<BODY leftmargin="20" topmargin="10">
 
	<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
	
	 <tr>
			<td height="25" colspan="2" width="45%" class="RightDashBoardHeader">&nbsp;<fmtPartPriceAdjHistoryLog:message key="LBL_PART_PRICE"/></td>
			<td align="right" class=RightDashBoardHeader>
				<fmtPartPriceAdjHistoryLog:message key="IMG_ALT_HELP" var = "varHelp"/>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("PART_PRICE_ADJ_HISTORY")%>');" />
			</td>
		</tr>	
	 
		 <tr>
	                     <td colspan="6">
	                     	<display:table export="<%=exportValue%>" name="requestScope.frmPartPriceAdjRpt.ldtHistoryResult" class="its" id="currentRowObject" requestURI="/gmPartPriceAdjustmentRptAction.do?method=fetchPartPriceHystoryDetails" >							
							<fmtPartPriceAdjHistoryLog:message key="DT_TRANSACTION_ID" var="varTransId"/>
							<display:column property="TRANSID" title="${varTransId}"    class="<%=strAlign %>"/>
							<fmtPartPriceAdjHistoryLog:message key="DT_PART_NUMBER" var="varPartNumber"/>
							<display:column property="PARTNUM" title="${varPartNumber}"    class="<%=strAlign %>"/>
							<fmtPartPriceAdjHistoryLog:message key="DT_PART_NUM_DESCRIPTION" var="varDescription"/>
							<display:column property="PDESC" title="${varDescription}"    class="<%=strAlign %>"/>
							<fmtPartPriceAdjHistoryLog:message key="DT_UNIT_PRICE" var="varUnitPrice"/>
							<display:column property="UNITPRICE" title="${varUnitPrice}"  format="{0,number,#,###,###.00}"   class="<%=strAlign %>"/>
							<fmtPartPriceAdjHistoryLog:message key="DT_ADJ_CODE" var="varAdjCode"/>
							<display:column property="ADJCODENAME" title="${varAdjCode}"    class="<%=strAlign %>"/>
							<fmtPartPriceAdjHistoryLog:message key="DT_ADJ_VALUE" var="varAdjValue"/>
							<display:column property="ADJVALUE" title="${varAdjValue}"    class="<%=strAlign %>"/>
							<fmtPartPriceAdjHistoryLog:message key="DT_NET_UNIT_PRICE" var="varNetPrice"/>
							<display:column property="NETUNITPRICE" title="${varNetPrice}"  format="{0,number,#,###,###.00}"   class="<%=strAlign %>"/>								
							<fmtPartPriceAdjHistoryLog:message key="DT_INITIATED_DATE" var="varInitiatedDate"/>
							<display:column property="INITDATE" title="${varInitiatedDate}" class="alignleft"  format="<%=strDTDateFmt%>" />							 
							<fmtPartPriceAdjHistoryLog:message key="DT_INITIATED_BY" var="varIniBy"/>
							<display:column property="INITBY" title="${varIniBy}"   class="alignleft"/>
							<fmtPartPriceAdjHistoryLog:message key="DT_IMPLEMENT_BY" var="varImpBy"/>
							<display:column property="IMPLEMBY" title="${varImpBy}"   class="alignleft"/> 
							<fmtPartPriceAdjHistoryLog:message key="DT_IMPLEMENT_DATE" var="varImpDate"/>
							<display:column property="IMPLEMDATE" title="${varImpDate}" class="alignleft"  format="<%=strDTDateFmt%>"/>						
							</display:table>

    		             </td>
                    </tr>
          <tr><td colspan="6" class="LLine" height="1"></td> </tr>
          <tr>
          	<td colspan="6" align="center" height="25"><fmtPartPriceAdjHistoryLog:message key="BTN_CLOSE" var="varClose"/><gmjsp:button  style="height:23px" value="&nbsp;${varClose}&nbsp;" gmClass="button" onClick="window.close();" buttonType="Load" /></td>
          </tr>     
		 
    </table>		     	
 
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

