 <%
/**********************************************************************************
 * File		 		: GmSalesPricingFileGen.jsp
 * Desc		 		: This screen is used to generate Pdfs and Excels for Sales Price Increase
 * author			: Mihir Patel
 ************************************************************************************/
%>
  <%@ include file="/common/GmHeader.inc" %>
  <%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap, java.util.Calendar" %> 
  <%@ page import ="com.globus.common.beans.GmCommonControls"%>
  <%@ page import = "com.globus.sales.pricing.forms.GmSalesPricingFileGenForm" %>
 
 <%@ taglib prefix="fmtSalesPricingFileGen" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmSalesPricingFileGen.jsp -->

<fmtSalesPricingFileGen:setLocale value="<%=strLocale%>"/>
<fmtSalesPricingFileGen:setBundle basename="properties.labels.sales.pricing.GmSalesPricingFileGen"/>
 
 <%
String strWikiTitle = GmCommonClass.getWikiTitle("MANUAL_GENERATE_TAGS");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Price Increase PDF and Excels</TITLE>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>	

<script> 

function fnConfirm()
 {
   return confirm(message_sales[192]);   
 }
function fnGenerate()
{
    
    
   var genPdf = document.frmSalesPricingFileGen.genPdf.checked;
   var genExcel = document.frmSalesPricingFileGen.genExcel.checked;
   var inputStringAccId =  document.frmSalesPricingFileGen.inputStringAccId.value; 
   
    if (inputStringAccId == '')
    {
    	Error_Details(message_sales[193]);
    }else if (inputStringAccId != '' && genPdf == '' && genExcel =='' )
    {
    	Error_Details(message_sales[194]);
    }
    else if(inputStringAccId.length >= 125 )
    {
	  Error_Details(message_sales[195]);
	}
     
    if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	
	 //document.frmSalesPricingFileGen.genPdf.value = document.frmSalesPricingFileGen.genPdf.checked;
	 //document.frmSalesPricingFileGen.genExcel.value = document.frmSalesPricingFileGen.genExcel.checked;	 		
	document.frmSalesPricingFileGen.strOpt.value = "RELOAD";
	if(fnConfirm())
		 {
		 	fnStartProgress();
			document.frmSalesPricingFileGen.submit();
		 }		
	
}
</script>

</HEAD>
<BODY leftmargin="20" topmargin="10"  >
 
<html:form action="/gmGenerateFiles.do"   >
<html:hidden property="strOpt" value=""/> 


 <table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
	<tr>
			<td height="25" class="RightDashBoardHeader" ><fmtSalesPricingFileGen:message key="LBL_GENERATE_PRICE_INC_FILES"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
		  	<fmtSalesPricingFileGen:message key="IMG_ALT_HELP" var = "varHelp"/>
		  	<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />  
	       </td>
	</tr>
		
		<tr><td class="LLine" height="1" colspan="2"></td></tr>

		<tr>
			<td height="30" class="RightTableCaption" align="right"><fmtSalesPricingFileGen:message key="LBL_ACCOUNT_ID"/>:</td>
			<td><!-- Struts tag lib code modified for JBOSS migration changes -->
			&nbsp;<html:textarea property="inputStringAccId" rows="5" cols="45" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
			&nbsp;&nbsp;
			</td>		
		</tr>		

		<tr><td colspan="2" class="ELine"></td></tr>
			
		<tr >
			<td height="30" class="RightTableCaption" align="right"><html:checkbox property="genPdf" /></td>
			<td>&nbsp;<fmtSalesPricingFileGen:message key="LBL_GENERATE_PDF"/></td>
		</tr>				  

		 <tr><td colspan="2" class="ELine"></td></tr>	
		 <tr>					
			<td height="30" class="RightTableCaption" align="right"><html:checkbox property="genExcel" /></td>
			<td>&nbsp;<fmtSalesPricingFileGen:message key="LBL_GENERATE_EXCEL"/> &nbsp;&nbsp;<fmtSalesPricingFileGen:message key="BTN_GENERATE" var="varGenerate"/><gmjsp:button value="&nbsp;&nbsp;${varGenerate}&nbsp;&nbsp;" name="Btn_Go" buttonType="Save" gmClass="button" onClick="javascript:fnGenerate();" /></td>
		</tr>			

		
	</table>		
 </html:form>
 <%@ include file="/common/GmFooter.inc" %>
 </BODY>
 </HTML>
