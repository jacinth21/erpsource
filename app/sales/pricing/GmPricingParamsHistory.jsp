<%
/********************************************************************************************************
 * File		 		: GmPricingParamsHistory.jsp
 * Desc		 		: Display the Price Adjustment Code history for the selected Account/Group Price Book
 * Version	 		: 1.0
 * author			: HReddi
*********************************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ taglib prefix="fmtPricingParamsHistory" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmPricingParamsHistory.jsp -->

<fmtPricingParamsHistory:setLocale value="<%=strLocale%>"/>
<fmtPricingParamsHistory:setBundle basename="properties.labels.sales.pricing.GmPricingParamsHistory"/>
  
<bean:define id="ldtHistoryResult" name="frmPricingParamsSetup" property="ldtHistoryResult" type="java.util.List"></bean:define>
 
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Mapping </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
 
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<%
boolean exportValue=false;
%>
</HEAD>

<BODY leftmargin="20" topmargin="10"> 
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">	
	  
		<tr>
			<td height="25" colspan="2" width="35%" class="RightDashBoardHeader">&nbsp;<fmtPricingParamsHistory:message key="LBL_PRICE_ADJ_CODE_HIST"/></td>
			<td align="right" class=RightDashBoardHeader>
				<fmtPricingParamsHistory:message key="IMG_ALT_HELP" var = "varHelp"/>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("PRICE_PARAM_ADJ_HISTORY")%>');" />
			</td>
		</tr>
	 
	 
		 <tr>
	          <td colspan="3">
	          	<display:table export="<%=exportValue%>" name="requestScope.frmPricingParamsSetup.ldtHistoryResult" class="its" id="currentRowObject" requestURI="/gmPricingParamsSetup.do?method=fetchPriceParamHystory" >							
				<fmtPricingParamsHistory:message key="DT_ADJ_CODE" var="varAdjCode"/>
				<display:column property="ADJCODE" title="${varAdjCode}"    class="aligncenter"/>	
				<fmtPricingParamsHistory:message key="DT_ADJ_VALUE" var="varAdjValue"/>
				<display:column property="RVALUE" title="${varAdjValue}"    class="aligncenter"/>	
				<fmtPricingParamsHistory:message key="DT_PRICE_LIST" var="varPriceList"/>
				<display:column property="PRICEVAL" title="${varPriceList}"    class="alignleft"/>							
				<fmtPricingParamsHistory:message key="DT_UPDATE_DATE" var="varUpdatedDate"/>
				<display:column property="UPDATE_DATE" title="${varUpdatedDate}" class="alignleft"/>							 
				<fmtPricingParamsHistory:message key="DT_UPDATE_BY" var="varUpdatedBy"/>
				<display:column property="UPDATEDBY" title="${varUpdatedBy}"   class="alignleft"/> 
				</display:table>
			  </td>
         </tr>
          <tr><td colspan="3" class="LLine" height="1"></td> </tr>
          <tr>
          	<td colspan="3" align="center" height="25"><fmtPricingParamsHistory:message key="BTN_CLOSE" var="varClose"/><gmjsp:button  style="height:23px" value="&nbsp;${varClose}&nbsp;" gmClass="button" onClick="window.close();" buttonType="Load" /></td>
          </tr>		 
    </table> 
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

