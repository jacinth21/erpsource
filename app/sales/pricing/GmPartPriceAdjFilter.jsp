 <%
/*******************************************************************************************************
 * File		 		: GmPartPriceAdjFilter.jsp
 * Desc		 		: This JSP is to show the Filter part in Part Price adjustment screen
 * Version	 		: 1.0
 * author			: arajan
*********************************************************************************************************/
%>

<%@ page language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtPartPriceAdjFilter" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmPartPriceAdjFilter.jsp -->

<fmtPartPriceAdjFilter:setLocale value="<%=strLocale%>"/>
<fmtPartPriceAdjFilter:setBundle basename="properties.labels.sales.pricing.GmPartPriceAdjFilter"/>

<%
String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
String strScreen = GmCommonClass.parseNull(request.getParameter("SCREEN"));
String strLabeltyp = "MandatoryText"; 
if(strScreen.equals("Dashboard")){
	strLabeltyp = "RegularText";
}

String strJsFucntion = "";
String strAccFunction = "";
if(strScreen.equals("REPORT") || strScreen.equals("Dashboard")){
  strJsFucntion = "javascript:fnLoadAcctId(this);";
  strAccFunction = "changeBgColor(this,'#ffffff');fnLoadAccountName(this);";
}else{
  strJsFucntion = "javascript:fnLoadAcctId(this);fnLoad();";
  strAccFunction = "changeBgColor(this,'#ffffff');fnLoadAccountName(this);fnLoad();";
}


%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: Part Price Adjustment Initiate</title>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmPartPriceAdjFilter.js"></script>
<script>
var screen = '<%=strScreen%>';
var lblType='<fmtPartPriceAdjFilter:message key="LBL_TYPE"/>';
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnChangeDisplay();">

	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td height="27" class="RightTableCaption" align="right" width="8%"><fmtPartPriceAdjFilter:message key="LBL_TYPE" var="varType"/><gmjsp:label type="<%=strLabeltyp%>" SFLblControlName="${varType}" td="false" /> </td>
			<%if(!strScreen.equals("Dashboard")){%>
			<td width="17%">&nbsp;<gmjsp:dropdown controlName="type" SFFormName="<%=strFormName %>" SFSeletedValue="type" SFValue="alType" codeId="CODEID" codeName="CODENM" onChange="javascript:fnChangeDisplay(this);" /></td>
			<%}else{ %>
			<td width="17%">&nbsp;<gmjsp:dropdown controlName="type" SFFormName="<%=strFormName %>" SFSeletedValue="type" SFValue="alType" codeId="CODEID" codeName="CODENM" onChange="javascript:fnChangeDisplay(this);"  defaultValue= "[Choose One]"/></td>
			<%} %>
			
			<%-- <td height="24" class="RightTableCaption" align="right" width="5%"><gmjsp:label type="MandatoryText" SFLblControlName="Name:" td="false" /> </td> --%>
			<td class=RightTableCaption align="right" height="24" width="7%"> <div id="header"></div> </td>
			<td>
				<table  border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td id="acc_name" style="display: none" width="455px">&nbsp;<gmjsp:dropdown controlName="accName" width="450" SFFormName="<%=strFormName %>" SFSeletedValue="accName" SFValue="alAccName" codeId="ID" codeName="NAME" onChange="<%=strJsFucntion %>"
								defaultValue= "[Choose One]" /></td>
						<td id="grp_acc_name" style="display: none" width="455px">&nbsp;<gmjsp:dropdown controlName="grpAccName" width="450" SFFormName="<%=strFormName %>" SFSeletedValue="grpAccName" SFValue="alGrpAccName" codeId="ID" codeName="NAME" onChange="<%=strJsFucntion %>"
								defaultValue= "[Choose One]" /></td>
						<td id="empty_list" style="display: none" width="455px">&nbsp;<gmjsp:dropdown controlName="emptyAcc" width="450" SFFormName="<%=strFormName %>" SFSeletedValue="emptyAcc" SFValue="alEmptyAcc"
								defaultValue= "[Choose One]"/></td>						
					</tr>
				</table>
			</td>
			<td height="24" class="RightTableCaption" align="right" width="5%"><fmtPartPriceAdjFilter:message key="LBL_ID" var="varId"/><gmjsp:label type="RegularText" SFLblControlName="${varId}" td="false" /></td>
				<td width="10%">&nbsp;<html:text property="accId" styleClass="InputArea" name="<%=strFormName %>" size="8" onfocus="changeBgColor(this,'#AACCE8');" onblur="<%=strAccFunction %>" />
			</td>
			<%if(!strScreen.equals("REPORT") && !strScreen.equals("Dashboard")){ %>
				<td width="10%" height="23">&nbsp;
					<fmtPartPriceAdjFilter:message key="BTN_LOAD" var="varLoad"/>
					<gmjsp:button name="Btn_Load" value="${varLoad}" gmClass="button" style="width:5em;height:24px;cursor:pointer"  onClick="javascript:fnLoad(this);" buttonType="Load" />
				</td>
			<%} %>
		</tr>		
	</table>	
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>