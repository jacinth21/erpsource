<%
/**********************************************************************************
 * File		 		: GmPricingStatusDetail.jsp 
 * Desc		 		: display Pricing Status detail
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

 <%@ taglib prefix="fmtPricingStatusDetail" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmPricingStatusDetail.jsp -->

<fmtPricingStatusDetail:setLocale value="<%=strLocale%>"/>
<fmtPricingStatusDetail:setBundle basename="properties.labels.sales.pricing.GmPricingStatusDetail"/>
  
 
<HTML>
<HEAD> 
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css"> 
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>  

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script> 
function fnViewPriceRequest (reqid)
{
	 
	if(window.parent.document.getElementById('StatusDetailFrame')){ 
	window.parent.location.href="/gmPricingRequestDAction.do?method=openPriceRequest&strRequestId="+reqid+"&fchAllSysFl=Y";
	} 
	
}	 
  
 
</script>
</HEAD>

<BODY  > 
<html:form> 
	<table border="0"   width="100%" cellspacing="0" cellpadding="0">
	  
            <tr>      
             
            <td  >
          
            <display:table name="requestScope.frmPricingStatusReport.ldtResult" requestURI="/gmPricingStatusReport.do?method=pricingStatusReport"   class="its" id="currentRowObject" freezeHeader="true" paneSize="250" export="true" decorator="com.globus.displaytag.beans.DTGroupStatusReportWrapper">
			 
			 		<fmtPricingStatusDetail:message key="DT_REQ_ID" var="varReqId"/>
			 		<display:column property="REQID" title="${varReqId}" class="alignleft"  sortable="true" />
					<fmtPricingStatusDetail:message key="DT_ACC_ID" var="varAccId"/>
					<display:column property="ACCOUNTID" title="${varAccId}" class="alignleft" sortable="true"/>
					<fmtPricingStatusDetail:message key="DT_ACC_NAME" var="varAccName"/>
					<display:column property="ACCOUNTNM" title="${varAccName}" class="alignleft" sortable="true"/>
					<fmtPricingStatusDetail:message key="DT_STATUS" var="varStatus"/>
					<display:column property="STATUS" title="${varStatus}" class="alignleft" />
					<fmtPricingStatusDetail:message key="DT_EFFECTIVE_DATE" var="varEffDate"/>
					<display:column property="EFFECTIVEDATE" title="${varEffDate}" class="alignleft" />
					<fmtPricingStatusDetail:message key="DT_CREATED_BY" var="varCreateBy"/>
					<display:column property="CREATEDBY" title="${varCreateBy}" class="alignleft" />
					<fmtPricingStatusDetail:message key="DT_CREATED_DATE" var="varCreateDate"/>
					<display:column property="CREATEDDATE" title="${varCreateDate}" class="alignleft" />
				 
		</display:table>  
			</td>
			</tr>
	     
		 
    </table>
    </html:form> 
    <%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>

