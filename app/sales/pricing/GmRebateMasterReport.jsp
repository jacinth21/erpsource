<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- sales\pricing\GmRebateManagemnetReport.jsp -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtRebateReport"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>

<fmtRebateReport:setLocale value="<%=strLocale%>" />
<fmtRebateReport:setBundle
	basename="properties.labels.sales.pricing.GmRebateMasterReport" />
<bean:define id="partyId" name="frmRebateReport" property="partyId"
	type="java.lang.String">
</bean:define>
<bean:define id="searchpartyId" name="frmRebateReport"
	property="searchpartyId" type="java.lang.String">
</bean:define>
<bean:define id="groupTypeId" name="frmRebateReport"
	property="groupTypeId" type="java.lang.String">
</bean:define>

<%
	String strSalesPricingJsPath = GmFilePathConfigurationBean
			.getFilePathConfig("JS_SALES_PRICING");
	String strWikiTitle = GmCommonClass
			.getWikiTitle("REBATE_MANAGEMENT_REPORT");
	String strApplDateFmt = strGCompDateFmt;
%>
<html>
<head>
<title>Rebate Management Report</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css"> 
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script> 
<script language="javascript" src="<%=strJsPath%>/GmGrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_fast.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript"
	src="<%=strSalesPricingJsPath%>/GmRebateMasterReport.js"></script>

<script type="text/javascript">
var format = '<%=strApplDateFmt%>';
</script>
</head>

<body leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();" onkeyup= "fnEnter();">
	<html:form action="/gmRebateReport.do?method=loadRebateManagmentReport">
		<html:hidden property="strOpt" />
		<html:hidden property="rebateId" />

		<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="5" height="25" class="RightDashBoardHeader"><fmtRebateReport:message
						key="LBL_REBATE_MANAGEMENT_REPORT" /></td>
				<td height="25" class="RightDashBoardHeader"><fmtRebateReport:message
						key="IMG_HELP" var="varHelp" /><img align="right" id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="6" height="1"></td>
			</tr>
			<tr class="Shade">
				<td class="RightTableCaption" align="right" height="30"><fmtRebateReport:message
						key="LBL_ACCOUNT_TYPE" var="varAccountType" /> <gmjsp:label
						type="RegularText" SFLblControlName="${varAccountType}:"
						td="false" /></td>
				<td class="RightText">&nbsp; <gmjsp:dropdown
						controlName="groupTypeId" SFFormName="frmRebateReport"
						SFSeletedValue="groupTypeId" SFValue="alAccountType"
						codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"
						tabIndex="1"  onChange="fnAccType(this.value);"/>
				</td>
				<td class="RightTableCaption" align="right" height="30"><fmtRebateReport:message
						key="LBL_NAME" var="varName" /> <gmjsp:label type="RegularText"
						SFLblControlName="${varName}:" td="false" /></td>
				<td><jsp:include page="/common/GmAutoCompleteInclude.jsp">
						<jsp:param name="CONTROL_NAME" value="partyId" />
						<jsp:param name="METHOD_LOAD"
							value="loadPartyNameList&searchType=PrefixSearch" />
						<jsp:param name="WIDTH" value="300" />
						<jsp:param name="CSS_CLASS" value="search" />
						<jsp:param name="TAB_INDEX" value="2" />
						<jsp:param name="CONTROL_NM_VALUE" value="<%=searchpartyId%>" />
						<jsp:param name="CONTROL_ID_VALUE" value="<%=partyId%>" />
						<jsp:param name="SHOW_DATA" value="100" />
						<jsp:param name="AUTO_RELOAD" value="" />
						<jsp:param name="DYNAMIC_OBJ" value="#groupTypeId^" />
					</jsp:include></td>
				<td class="RightTableCaption" align="right" height="30"><fmtRebateReport:message
						key="LBL_REABTE_TYPE" var="varRebateType" /> <gmjsp:label
						type="RegularText" SFLblControlName="${varRebateType}:" td="false" />
				</td>
				<td>&nbsp; <gmjsp:dropdown controlName="rebateTypeId"
						SFFormName="frmRebateReport" SFSeletedValue="rebateTypeId"
						SFValue="alRebateType" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" tabIndex="3" />
				</td>

			</tr>
			<tr>
				<td class="LLine" colspan="6" height="1"></td>
			</tr>
			<tr>
				<td class="RightTableCaption" align="right" height="30"><fmtRebateReport:message
						key="LBL_REBATE_CATEGORY" var="varRebateCategory" /> <gmjsp:label
						type="RegularText" SFLblControlName="${varRebateCategory}:"
						td="false" /></td>
				<td>&nbsp; <gmjsp:dropdown controlName="rebateCategoryId"
						SFFormName="frmRebateReport" SFSeletedValue="rebateCategoryId"
						SFValue="alRebateCategory" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" tabIndex="4"
						onChange="fnChange(this.value)" /></td>
				<td class="RightTableCaption" align="right" height="30"><fmtRebateReport:message
						key="LBL_REBATE_RATE" var="varRebateRate" /> <gmjsp:label
						type="RegularText" SFLblControlName="${varRebateRate}:" td="false" />
				</td>
				<td>&nbsp;<html:text name="frmRebateReport"
						property="rebateFromRate" styleClass="InputArea"
						onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" tabindex="5" size="7" maxlength="5"/> <html:text
						name="frmRebateReport" property="rebateToRate"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" tabindex="6" size="7" maxlength="5"/>
				</td>
				<td class="RightTableCaption" align="right" height="30"><fmtRebateReport:message
						key="LBL_REBATE_PARTS" var="varRebateParts" /> <gmjsp:label
						type="RegularText" SFLblControlName="${varRebateParts}:"
						td="false" /></td>
				<td>&nbsp; <gmjsp:dropdown controlName="rebatePartTypeId"
						SFFormName="frmRebateReport" SFSeletedValue="rebatePartTypeId"
						SFValue="alRebateParts" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" tabIndex="7" /></td>
			</tr>
			<tr>
				<td class="LLine" colspan="6" height="1"></td>
			</tr>
			<tr class="Shade">
				<td class="RightTableCaption" align="right">&nbsp; <fmtRebateReport:message
						key="LBL_DATE_TYPE" />:
				</td>
				<td class="RightTableCaption">&nbsp;
				<gmjsp:dropdown controlName="dateType"
						SFFormName="frmRebateReport" SFSeletedValue="dateType"
						SFValue="alDateType" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"
						tabIndex="8"/></td>
					<td colspan="2" class="RightTableCaption">
				    <fmtRebateReport:message key="LBL_FROM" var="varFrom"/>
					 <fmtRebateReport:message key="LBL_TO" var="varTo"/>
					<gmjsp:label type="RegularText"  SFLblControlName="${varFrom}:" td="false"/>&nbsp;
					<gmjsp:calendar SFFormName="frmRebateReport" 
							controlName="rebateFromDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="9" />&nbsp;
							&nbsp;&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varTo}:" td="false"/>&nbsp;
					<gmjsp:calendar SFFormName="frmRebateReport" 
							controlName="rebateToDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="10"/>&nbsp;
			
				
				</td>

				<fmtRebateReport:message key="BTN_LOAD" var="varLoad" />
				<td align="left" colspan="2"><gmjsp:button
						value="&nbsp;${varLoad}&nbsp;" gmClass="button"
						onClick="fnLoad();" buttonType="Load" tabindex="11" /></td>

			</tr>

			<tr>
				<td colspan="6">
					<div id="dataGridDiv" height="500px" width="1100"></div>
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="6" height="1"></td>
			</tr>

			<tr>
				<td colspan="6" align="center">
					<div class='exportlinks' id="DivExportExcel">
						<fmtRebateReport:message key="LBL_EXPORT_OPTIONS" />
						:<img src='img/ico_file_excel.png' />&nbsp; <a href="#"
							onclick="fnDownloadXLS();"><fmtRebateReport:message
								key="LBL_EXCEL" /></a> 
					</div>
				</td>
			</tr>

			<tr height="30">
				<td colspan="6" align="center"><div id="DivNothingMessage">
						<fmtRebateReport:message key="LBL_NO_DATA_AVAILABLE" />
					</div></td>
			</tr>
		</table>
	</html:form>
</body>
<%@ include file="/common/GmFooter.inc"%>
</html>