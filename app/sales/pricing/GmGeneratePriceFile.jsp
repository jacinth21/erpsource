 <%
/**********************************************************************************
 * File		 		: GmGeneratePriceFile.jsp
 * Desc		 		: This screen is used to display the generate price file Report
 * Version	 		: 1.0
 * author			: dsandeep
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtGeneratePriceFile" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmGeneratePriceFile.jsp -->
<fmtGeneratePriceFile:setLocale value="<%=strLocale%>"/>
<fmtGeneratePriceFile:setBundle basename="properties.labels.sales.pricing.GmGeneratePriceFile"/>

<bean:define id="xmlGridData" name="frmPriceApprovalRequest" property="xmlGridData" type="java.lang.String"> </bean:define>
<bean:define id="gpbid" name="frmPriceApprovalRequest" property="gpbid" type="java.lang.String"> </bean:define>
<bean:define id="gpbName" name="frmPriceApprovalRequest" property="gpbName" type="java.lang.String"> </bean:define>
<bean:define id="strhlcaff" name="frmPriceApprovalRequest" property="strhlcaff" type="java.lang.String"> </bean:define>
<bean:define id="strgpb" name="frmPriceApprovalRequest" property="strgpb" type="java.lang.String"> </bean:define>

<HTML>
<HEAD>
<TITLE> Globus Medical: Generate price file </TITLE>
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script>
var gridObj = "";
var headerAcc ='';
var headerGpo = '';
var accID='<%=gpbid%>';
var accName="<%=gpbName%>";
var gpoAffiliation="<%=strhlcaff%>";
var groupPricBook="<%=strgpb%>";
var gridData = '<%=xmlGridData%>';
headerAcc = 'Account/GPB ID : '+accID+',#cspan,#cspan,Account/GPB Name : '+accName+',#cspan,#cspan,#cspan';
headerGpo = 'GPO Affiliation : '+gpoAffiliation+',#cspan,#cspan,Group Price Book : '+groupPricBook+',#cspan,#cspan,#cspan';
function fnOnLoad(){

if (gridData != '') {
	gridObj = initGridData('mygrid_container', gridData);
	gridObj.attachHeader(headerAcc);
	gridObj.attachHeader(headerGpo);
	gridObj.attachHeader("System Name,Part#,Part Description, List Price $,Unit Price $,Disc%,Status");
	gridObj.enablePaging(true, 100, 10, "pagingArea", true);
	gridObj.setPagingSkin("bricks");
}

}
//This Function used to Initiate the grid
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}

function fnExportExcel() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}
</script>
</HEAD>
<BODY onload="fnOnLoad();">
<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
<tr>
<td height="25"></td>
</tr>
<tr><td class="LLine" colspan="8" height="1"></td></tr>
<tr>
  <td height="25" class="RightDashBoardHeader" colspan="5" align = "center"> <fmtGeneratePriceFile:message key="LBL_CONFIDENTIAL"/></td>
</tr>
<tr><td class="LLine" colspan="8" height="1"></td></tr>
<tr>
  <td height="25" class="RightDashBoardHeader" colspan="5" align = "center"> <fmtGeneratePriceFile:message key="LBL_ACCOUNT_PRICE_REPORT"/></td>
</tr>
 
 
 
               <%
				if (xmlGridData.indexOf("cell") != -1) {
               %>
               
           <tr>
				<td colspan="10">
					<div id="mygrid_container" style="height:400px;width:1000px;"></div>
					<div id="pagingArea" style="width: 1000px"></div>
				</td>
			</tr>
         <tr>
				<td colspan="10" align="center" height="25" width="100%">
					<div class='exportlinks'> <fmtGeneratePriceFile:message key="DIV_EXPORT_OPT"/>: <img src='img/ico_file_excel.png' /> <a href="#"
							onclick="javascript:fnExportExcel();"><fmtGeneratePriceFile:message key="DIV_EXCEL"/></a>
				</td>
			</tr>
           <%
           } else if (!xmlGridData.equals("")) {
           %>		
			
			<tr>
				<td colspan="8" height="1" class="LLine">
				<div id="mygrid_container" style="height:100px;width:1000px;"></div>
				</td>
			</tr>
			<tr>
			<tr>
				<td colspan="8" align="center" class="RightText"><fmtGeneratePriceFile:message key="LBL_NO_DATA"/>
					</td>
			</tr>
			</tr>
			<%}%>
			<tr>
            <td height="25" class="RightDashBoardHeader" colspan="5" align = "center"> <fmtGeneratePriceFile:message key="LBL_CONFIDENTIAL"/> </td>
            </tr>
			
 </table>
 <%@ include file="/common/GmFooter.inc"%>
 </BODY>
 </HTML>
   





