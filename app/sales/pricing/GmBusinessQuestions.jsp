<%
/**********************************************************************************************
 * File		 		: GmBusinessQuestions.jsp
 * Desc		 		: This screen is used to review the Business Questions and Answers Details
 * Version	 		: 1.0
 * author			: Jagadeesh Reddy N
 **********************************************************************************************/
%>
 <!-- \sales\pricing\GmBusinessQuestions.jsp -->

	<%@ include file="/common/GmHeader.inc" %>
	<%@ page language="java" %>
	
	<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
	<%@ page import ="org.apache.commons.beanutils.PropertyUtils"%>
	<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
	
 	<bean:define id="priceRequestId" name="frmPriceApprovalRequest" property="priceRequestId" type="java.lang.String"></bean:define>
 	<bean:define id="alQuestion" name="frmPriceApprovalRequest"	property="alQuestion" type="java.util.ArrayList"></bean:define>
	<bean:define id="alAnswer" name="frmPriceApprovalRequest"	property="alAnswer" type="java.util.ArrayList"></bean:define>
	<bean:define id="alAnswerGroup" name="frmPriceApprovalRequest"	property="alAnswerGroup" type="java.util.ArrayList"></bean:define>
	<bean:define id="alQuestionAnswer" name="frmPriceApprovalRequest"	property="alQuestionAnswer" type="java.util.ArrayList"></bean:define>
 	<%
 	String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");
 	%>
<HTML>
<HEAD>	
	<TITLE> Globus Medical: Business Questions</TITLE>
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">	
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
		
	<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
	<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
	<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmPriceApprovalRequest.js"></script>
		
	<style type="text/css" media="all">
	@import url("<%=strCssPath%>/displaytag.css");
	  @import url("<%=strCssPath%>/screen.css");
	</style>
	<script>
		
	</script>
	<%	

	HashMap hmQuestion = new HashMap();
	HashMap hmAnswer = new HashMap();
	HashMap hmAnsGroup = new HashMap();
	HashMap hmQuestionAnswer = new HashMap();
	
	int intQuestion = 0;
	int intAnserGroup = 0; 
	
	String strAnswerGrpId = "";
	String strBRValue = "";
	String strAnsQuestionID = "";
	String strAnsGrpNm = "";
	String strControl = "";
	String strAnswerGrpTemp = "";
	String strOptionNm = "";
	String strOptionId = ""; 
	String strAnswerGrpIds ="";
	
	String strAnsDs ="";
	String strListID ="";
	
	String strListPt1 = "<select type=select class=RightText style='vertical-align:top;' id='' name=";
	String strListPt2 = ">";
	String strListPt3 = "</select>";
	String strTextPt1 = "<textarea style='white-space: pre-wrap; margin-bottom:10px;' rows=3 cols=125 name=";
	String strTextPt2 = "<textarea rows=3 cols=125 readonly='true'  name=";
	
	String strControlType = "";
	int intQuesSize = 0;
	int intAnsSize =0;
	int intAnsGrpSize =0;
	 
	String strFormName = ""; 
	 
	String strQuestionNM = "";
	String strQuestionID = ""; 
	String strArrayListQuesName = "";
	String strArrayListAnsName = "";
	String strArrayListAnsGrpName = "";
	String strArrayListQuesAnsName ="";
		
	// Check if the page is in struts
	strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
	
	if(!strFormName.equals(""))
	{
		strArrayListQuesName = GmCommonClass.parseNull(request.getParameter("alQuestion"));
		strArrayListAnsName = GmCommonClass.parseNull(request.getParameter("alAnswer"));
		strArrayListAnsGrpName = GmCommonClass.parseNull(request.getParameter("alAnswerGroup"));
		strArrayListQuesAnsName = GmCommonClass.parseNull(request.getParameter("alQuestionAnswer"));
		Object objTemp = pageContext.getRequest().getAttribute(strFormName);
		alQuestion  = GmCommonClass.parseNullArrayList((ArrayList)PropertyUtils.getProperty(objTemp,strArrayListQuesName));
		alAnswer  = GmCommonClass.parseNullArrayList((ArrayList)PropertyUtils.getProperty(objTemp,strArrayListAnsName));
		alAnswerGroup  = GmCommonClass.parseNullArrayList((ArrayList)PropertyUtils.getProperty(objTemp,strArrayListAnsGrpName));
		alQuestionAnswer  = GmCommonClass.parseNullArrayList((ArrayList)PropertyUtils.getProperty(objTemp,strArrayListQuesAnsName));
	}

	String strMode = GmCommonClass.parseNull(request.getParameter("Mode"));	  
	
%>		
</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
	<html:form action="/gmPriceApprovalRequestAction.do?" >
	<html:hidden property="strOpt"/>
	<html:hidden property="hiddenPriceReqID" value="<%=priceRequestId%>"/>	
	<input type="hidden" name="sysProjectString">

	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" bordercolor="red" style="white-space: nowrap;" class="DtTable850"  cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;Business Questions</td>
			<td align="right" colspan="4" class=RightDashBoardHeader>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("BUSINESS_QUESTIONS")%>');" />
			</td>
		</tr>
		
		<%
	if 	(!strMode.equals(""))
	{	
%>	
	<tr><td height="1" colspan="3" bgcolor="#666666"></td></tr>		
	<tr>
		<td colspan="3" height="50" >&nbsp; </td>
	</tr>
<%
	}
%>	
	<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
	 
<%	

 	if (alQuestion != null)
	{
		intQuesSize = alQuestion.size();
	}
	
	if (intQuesSize > 0)
	{   
		intAnsSize = alAnswer.size();
		intAnsGrpSize = alAnswerGroup.size(); 
		
		for (int i=0;i<intQuesSize;i++)
		{
			hmQuestion = (HashMap)alQuestion.get(i);
			strQuestionNM = (String)hmQuestion.get("QUESTIONNAME");
			strQuestionID = (String)hmQuestion.get("QUESTIONID");

		 	
			strAnswerGrpId = ""; 
			String strShade = "Shade";
			if(i%2 == 0)
			{
				strShade = "";
			}
			
%>			<tr class="<%=strShade %>">
				<td  colspan="3"  class="RightText" height="22"   valign="top">&nbsp;<b><%=strQuestionID%>)</b>&nbsp; 
				 <b><%=strQuestionNM%></b> </td>
			</tr>	
			<tr class="<%=strShade %>"><td colspan="3" height="24" class="RightText">
				<span class="RightTextBlue" style="vertical-align:top;">&nbsp;&nbsp;<b>A.</b></span>
<%	
			//Below code loops thru for each answers		
			strBRValue = "";
			for (int j=0;j<intAnsSize;j++)
			{
				hmAnswer = (HashMap)alAnswer.get(j);
				strAnsQuestionID = (String)hmAnswer.get("QUESTIONID");
				strAnswerGrpId = (String)hmAnswer.get("QUESTIONID");
				strControlType = (String)hmAnswer.get("CONTROLTYPE");
				strAnswerGrpIds = "hAnsGrpId".concat(strAnswerGrpId); 
				
				if (alQuestionAnswer.size()> 0)
				{
					hmQuestionAnswer = (HashMap)alQuestionAnswer.get(j);
					strListID = (String)hmQuestionAnswer.get("LISTID");
					strAnsDs = (String)hmQuestionAnswer.get("ANSWERDESC");
				}
				if (strAnsQuestionID.equals(strQuestionID))
				{
 
				// If the question is not associated with with combo box then will display below val
					
						if (strControlType.equals("26240001") || strControlType.equals("26240002"))
						{
							strControl = strTextPt1.concat(strAnswerGrpIds).concat(" id=").concat(""+strAnsQuestionID).concat("'>").concat(strAnsDs+"</textarea>");
							out.println( strBRValue +  strControl + "&nbsp;&nbsp;" ); 
							out.println( "<input type=hidden  name=hQuestionID" + strAnswerGrpId + " value = \"" + strAnsQuestionID + "\" >" );
						} 
				
					else{
						strControl = "";
						intAnsGrpSize = alAnswerGroup.size();
						hmAnsGroup = new HashMap(); 
						for (int k=0;k<intAnsGrpSize;k++)
						{
							hmAnsGroup = (HashMap)alAnswerGroup.get(k);
							strAnswerGrpTemp = (String)hmAnsGroup.get("GROUPNAME");
							
							if (strAnswerGrpTemp.equals(strAnswerGrpTemp))
							{
								strOptionNm = (String)hmAnsGroup.get("LISTNAME");
								strOptionId = (String)hmAnsGroup.get("LISTID");								
								
						 		strOptionId = strOptionId.equals(strListID)?strOptionId.concat(" selected"):strOptionId;
								
								strOptionId = "<option value=".concat(strOptionId).concat(">");
								if(strListID.equals(""))
								{
								  strControl = strControl.concat(strOptionId).concat("Choose One").concat("</option>");
								}
								else
								{
									strControl = strControl.concat(strOptionId).concat(strOptionNm).concat("</option>");
								}
							}
						}	
						
						strControl = strListPt1.concat(strAnswerGrpIds).concat(strListPt2).concat(strControl).concat(strListPt3); 
						out.println( strBRValue + strControl + "&nbsp;&nbsp;") ;
						
						strControl = strTextPt1.concat(strAnswerGrpIds).concat(" id=").concat(""+strAnsQuestionID).concat("'>").concat(strAnsDs+"</textarea>");
						out.println( strBRValue +  strControl + "&nbsp;&nbsp;" ); 
						  
						out.println( "<input type=hidden name=hQuestionID" + strAnswerGrpId + " value = \"" + strAnsQuestionID + "\" >" );
						 
					}
				}
%>				  				
<%
			} // end of Inner FOR LOOP Answer Loop 
%> 
			<BR></td></tr>
			<tr><td colspan="3" height="1" class="Line"></td></tr>
<%		} // END of Outer FOR Loop
%>		 

<%  		}
	else {
%>		<tr><td colspan="3" height="25" align="center" class="RightTextBlue">
		<BR><fmtQuestion:message key="LBL_NO_DATA_AVAILABLE"/></td></tr>
<%		}
		
%>						
		</table>					
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>