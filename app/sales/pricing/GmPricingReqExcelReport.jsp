<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@page import="java.util.Locale"%>

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtPricingReqExcelReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmPricingReqExcelReport.jsp --> 
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}


%>
<fmtPricingReqExcelReport:setLocale value="<%=strLocale%>"/>
<fmtPricingReqExcelReport:setBundle basename="properties.labels.sales.pricing.GmPricingReqExcelReport"/>


<bean:define id="reqId" name="frmPricingRequestForm" property="hreqid" type="java.lang.String"> </bean:define>
<%

response.setContentType("application/vnd.ms-excel");
String reqid=request.getParameter("anlData");
response.addHeader("content-disposition", "attachment; filename = Request-"+reqId+".xls");
String currentTime = GmCommonClass.getCurrentDate("MM/dd/yyyy HH:mm:ss aaa").toLocaleString();
String buf=request.getParameter("anlData");
//try{response.getWriter().println(buf);}catch(Exception e){}
%>
<html>
<head>
<TITLE> Globus Medical: Account Price Change Request </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style>
.RightTableCaption {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
}

.Line{
	background-color: #DCDCDC;
	font-weight:bold;
}

.RightText {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
}
</style>
</head>

<body topmargin="10" leftmargin="10">
<br>
<h3 align="center"><fmtPricingReqExcelReport:message key="LBL_PRICE_CHANGE_REQ"/> <%= reqId%></h3>
<table border="1" cellspacing="0" cellpadding="0">
  <tr><td colspan="4" class="RightTableCaption" ><fmtPricingReqExcelReport:message key="LBL_REPORT_DOWNLOAD"/> <%= currentTime%> </td></tr>
  <tr><td colspan="4" class="Line" ><fmtPricingReqExcelReport:message key="LBL_REQUEST_DETAILS"/></td></tr>
   <tr class="Shade">				
  	 <td height="24" class="RightTableCaption"  align="right" width="25%"> <fmtPricingReqExcelReport:message key="LBL_ACC_NAME"/> :</td>
	 <td> &nbsp;&nbsp;<bean:write name="frmPricingRequestForm" property="strAccountName"/></td>	
 	 <td height="24" class="RightTableCaption" align="right"> <fmtPricingReqExcelReport:message key="LBL_ACC_ID"/> :</td>
	 <td>&nbsp;&nbsp;<bean:write name="frmPricingRequestForm" property="strAccountId"/></td>
     </tr>
     <tr>
     <td height="24" class="RightTableCaption" align="right" width="25%"> <fmtPricingReqExcelReport:message key="LBL_STATUS"/> :</td>
	<td width="25%"> &nbsp;&nbsp;<bean:write name="frmPricingRequestForm" property="strStatusName"/>	
	</td>	
	<td height="24" class="RightTableCaption" align="right" width="25%"> </td>
	<td width="25%"> &nbsp;&nbsp;</td>	
     </tr>
    <tr>
     <td height="24" class="RightTableCaption" align="right" width="25%"> <fmtPricingReqExcelReport:message key="LBL_INITIATED_BY"/> :</td>
	<td width="25%"> &nbsp;&nbsp;<bean:write name="frmPricingRequestForm" property="strInitiateByName"/>	
	</td>	
	<td height="24" class="RightTableCaption" align="right" width="25%"> <fmtPricingReqExcelReport:message key="LBL_INITIATED_DATE"/> :</td>
	<td width="25%"> &nbsp;&nbsp;<bean:write name="frmPricingRequestForm" property="strInitiateDate"/></td>	
     </tr>
 </table>
<br>
 <br>
<table border="1" cellspacing="0" cellpadding="0">
 	<tr><td colspan="4" class="Line" ><fmtPricingReqExcelReport:message key="LBL_ACCOUNT_DETAILS"/></td></tr>
   <tr class="Shade">				
  	 <td height="24" class="RightTableCaption" align="right" width="25%"> <fmtPricingReqExcelReport:message key="LBL_REGION"/> :</td>
	 <td width="25%"> &nbsp;&nbsp;<bean:write name="frmPricingRequestForm" property="strRegName"/></td>	
 	 <td height="24" class="RightTableCaption" align="right" width="25%"> <fmtPricingReqExcelReport:message key="LBL_INITIATE_BY"/> :</td>
	 <td width="25%">&nbsp;&nbsp;<bean:write name="frmPricingRequestForm" property="strRepName"/></td>
     </tr>
     <tr>
     <td height="24" class="RightTableCaption" align="right" width="25%"> <fmtPricingReqExcelReport:message key="LBL_TERRITORY"/> :</td>
	<td width="25%"> &nbsp;&nbsp;<bean:write name="frmPricingRequestForm" property="strTerName"/>	
	</td>	
	<td height="24" class="RightTableCaption" align="right" width="25%"> <fmtPricingReqExcelReport:message key="LBL_DISTRIBUTOR"/> :</td>
	<td width="25%"> &nbsp;&nbsp;<bean:write name="frmPricingRequestForm" property="strDName"/></td>	
     </tr>
     <tr class="Shade">				
     <td height="24" class="RightTableCaption" align="right"> <fmtPricingReqExcelReport:message key="LBL_AREA_DIRECTOR"/> :</td>
	 <td>&nbsp;&nbsp;<bean:write name="frmPricingRequestForm" property="strAdName"/></td>	
	 <td height="24" class="RightTableCaption" align="right"> <fmtPricingReqExcelReport:message key="LBL_VP_NAME"/> :</td>
	<td> &nbsp;&nbsp;<bean:write name="frmPricingRequestForm" property="strVpName"/></td>
 	</tr> 	
 </table>
 <br>
 <br>
<%=buf%>
</body>
<html>