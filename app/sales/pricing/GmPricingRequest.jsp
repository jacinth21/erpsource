
<%
/**********************************************************************************
 * File		 		: GmPricingRequest.jsp
 * Desc		 		: Account Group - Price Change Request Screen
 * Version	 		: 1.0
 * author			: Ramdas Mayya
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import="java.util.ArrayList,java.util.*"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtPricingRequest" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmPricingRequest.jsp -->

<fmtPricingRequest:setLocale value="<%=strLocale%>"/>
<fmtPricingRequest:setBundle basename="properties.labels.sales.pricing.GmPricingRequest"/>

<% 
String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");
String strWikiTitle = GmCommonClass.getWikiTitle("OP_GROUP_PART_MAP");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Price Change Request</TITLE>
<meta http-equiv="Content-Typ content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script src="<%=strSalesPricingJsPath%>/GmPricingCommon.js"></script>
<script src="<%=strJsPath%>/date-picker.js"></script>
<script src="<%=strSalesPricingJsPath%>/GmPricingRequest.js"></script>

 <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxlayout.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/skins/dhtmlxlayout_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxToolbar/skins/dhtmlxtoolbar_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/skins/dhtmlxcalendar_yahoolike.css">
 <link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<%-- <link rel="STYLESHEET" type="text/css"	href="<%=strJsPath%>/dhtmlx/dhtmlxCombo/dhtmlxcombo.css"> --%>
<%-- <link rel="STYLESHEET" type="text/css"	href="<%=strJsPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strJsPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">
 --%>
<!-- MNTTASK-3518 Stack Overflow commented bolow line -->

<%-- <script src="<%=strJsPath%>/dhtmlx/dhtmlxcommon.js"></script> --%>
<%-- <script src="<%=strJsPath%>/dhtmlx/dhtmlxLayout/dhtmlxcontainer.js"></script> --%>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxlayout.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_ssc.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>

<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_validation.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_sub_row.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_acheck_a.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_dhxcalendar.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxToolbar/dhtmlxtoolbar.js"></script>
<!-- <script src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script> -->
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<%-- <script src="<%=strJsPath%>/dhtmlx/dhtmlxCombo/dhtmlxcombo.js"></script> --%>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxTreeGrid/dhtmlxtreegrid.js"></script> 
 <script src="<%=strExtWebPath%>/dhtmlx/dhtmlxTreeGrid/ext/dhtmlxtreegrid_filter.js"></script> 
<%--  <script src="<%=strJsPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js"></script>  --%>
 <script src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script> 
 <script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script> 

<%-- <script src="<%=strJsPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js"></script>
<script src="<%=strJsPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"></script> --%>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/patterns/dhtmlxlayout_pattern4e.js"></script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/displaytag.css");

.grid_hover {
	background-color: #E6E6FA;
	font-size: 20px;
}

div.gridbox_dhx_skyblue table.obj tr td {
	border-width: 0px 1px 0px 0px;
	border-color: #A4BED4;
}
</style>


<bean:define id="gridAnalysisDataView" name="frmPricingRequestForm"	property="gridXmlAnalysisView" type="java.lang.String"></bean:define>
<bean:define id="gridGroupData" name="frmPricingRequestForm" property="gridXmlGroupData" type="java.lang.String"></bean:define>
<bean:define id="gridAnalysisData" name="frmPricingRequestForm"	property="gridXmlAnalysisData" type="java.lang.String"></bean:define>
<!--<bean:define id="gridXmlWorkflowData" name="frmPricingRequestForm"	property="gridXmlWorkflowData" type="java.lang.String"></bean:define>-->
<bean:define id="gridXmlActivityData" name="frmPricingRequestForm"	property="gridXmlActivityData" type="java.lang.String"></bean:define>
<bean:define id="hstatusId" name="frmPricingRequestForm"	property="hstatusId" type="java.lang.String"></bean:define>
<bean:define id="alQuestion" name="frmPricingRequestForm"	property="alQuestion" type="java.util.ArrayList"></bean:define>
<bean:define id="alAnswer" name="frmPricingRequestForm"	property="alAnswer" type="java.util.ArrayList"></bean:define>
<bean:define id="alAnswerGroup" name="frmPricingRequestForm"	property="alAnswerGroup" type="java.util.ArrayList"></bean:define>
<bean:define id="alQuestionAnswer" name="frmPricingRequestForm"	property="alQuestionAnswer" type="java.util.ArrayList"></bean:define>
<bean:define id="fmDate" name="frmPricingRequestForm"	property="effectiveDate" type="java.lang.String"></bean:define>
<bean:define id="flgReqId" name="frmPricingRequestForm"	property="flgReqId" type="java.lang.String"></bean:define>
<bean:define id="strAccountId" name="frmPricingRequestForm"	property="strAccountId" type="java.lang.String"></bean:define>
<bean:define id="strContractFl" name="frmPricingRequestForm"	property="strContractFl" type="java.lang.String"></bean:define>
<bean:define id="strRequestId" name="frmPricingRequestForm"	property="strRequestId" type="java.lang.String"></bean:define>
<bean:define id="strErrorMsg" name="frmPricingRequestForm"	property="errorMsg" type="java.lang.String"></bean:define>
<bean:define id="strApprLvl" name="frmPricingRequestForm"	property="apprlvl" type="java.lang.String"></bean:define>
<bean:define id="strAccGPO" name="frmPricingRequestForm"	property="strAccGPO" type="java.lang.String"></bean:define>
<bean:define id="strAccType" name="frmPricingRequestForm"	property="strAccType" type="java.lang.String"></bean:define>
<bean:define id="strDispFlag" name="frmPricingRequestForm"	property="strDispFlg" type="java.lang.String"></bean:define>
<bean:define id="strGroupAcc" name="frmPricingRequestForm"	property="strGroupAcc" type="java.lang.String"></bean:define>
<bean:define id="strGroupName" name="frmPricingRequestForm"	property="strGroupName" type="java.lang.String"></bean:define>
<bean:define id="strPublOvrRide" name="frmPricingRequestForm"	property="strPublOvrRide" type="java.lang.String"></bean:define>

<%
	boolean isSysEnabled = (!(hstatusId.equals("52189") || hstatusId.equals("52190"))) ;
	String strDeptId = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
	GmCalenderOperations gmCalenderOperations = new GmCalenderOperations();
	String currDate = gmCalenderOperations.getCurrentDate("MM/dd/yyyy");
	String strAnaView = GmCommonClass.parseNull((String) session.getAttribute("strAnaView"));
	String strActivityLog = GmCommonClass.parseNull((String) session.getAttribute("strActivityLog"));
	System.out.println("strGroupAcc::"+strGroupAcc);
	strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	
%>
<script>
// For general Declaration 
var gpoID = '<%=strGroupAcc%>';
var dispflg = '<%=strDispFlag%>';
var reqType = '<%=strAccType%>';
var mCal;
var fmDate='<%=fmDate%>';
var flgReqId ='<%=flgReqId%>';
var contfl = '<%=strContractFl%>';
var reqstatus = '';
var reqid = '';
var grpGridChanged = false;
var ansSize = <%=alAnswer.size()%>;
var QuestionSize=<%=alQuestion.size()%>;
var userdataacl = '';
var userdesc = '';
var flgActChange =false;
var statuId = '<%=hstatusId%>';
var strAccountId ='<%=strAccountId%>';
var strRequestId = '<%=strRequestId%>';
var strDeptId = '<%=strDeptId%>';
var currDate ='<%=currDate%>';
var allowPrompt = true;
var flgChkSubmit = false;
var submitClicked = false;
var gridObjAnalysis ='';
var errorMsg = '<%=strErrorMsg%>';
var errorMsg_fl = false;
var apprlvl_stat = '';
var strApprLvl = '<%=strApprLvl%>';
var ApprLvl_fl = false;
var strPublOvrRide = '<%=strPublOvrRide%>';

// Declare index id in every column in vm file and we get easy index no here. I have declared variables for get index. 

// For Pricing Group Tab 
var remove_0_g = '';
var status_1_g = '';
var select_2_g = '';
var name_3_g = '';
var list_4_g = '';
var current_5_g = '';
var type_6_g = '';
var change_7_g = '';
var adjusted_8_g = '';
var comment_9_g = '';
var tw_10_g = '';
var consfl_11_g = '';
var dtype_12_g = '';
var effDate_13_g = '';
var DemoFl_14_g = '';
var dtype_15_g = '';
var dchange_16_g = '';
var dprice_17_g = '';
var fDate_18_g = '';
var tDate_19_g = '';
var ActiveFl_20_g = '';
var discount_9_g = ''

// For Approval View Tab
var approve_0_a = '';
var name_1_a = '';
var qty_2_a = '';
var list_3_a = '';
var current_4_a = '';
var adjusted_5_a = '';
var tw_6_a = '';
var twcomp_7_a = '';
var rule_8_a = '';
var lvlstrid_9_a = '';
var apprby_10_a = '';
var asp_11_a = '';
var ad_12_a = '';
var vp_13_a = '';
var pricons_14_a = '';
var prigrp_15_a = '';
var C_16_a = '';
var efftype_17_a = '';
var effdate_18_a = '';
var DemoFL_19_a = '';
var dfromdt_20_a = '';
var dtodt_21_a = '';
var dprice_22_a = '';
var ActiveFlg_23_a = '';
var pricedType = '';
var strGroupName = '<%=strGroupName%>';

// For Combo when we click the Status, it will dipslay folowing status.
var AD_AD = '52150,52151'; //'Submit for VP, Submit for PC';
var AD_PC = '52028,52151'; //'Approved,Submit for PC';
var VP_AD = '52150,52151'; //'Submit for VP,Submit for PC';
var VP_PC = '52028'; //'Approved;
var PC_AD = '52150,52151'; //'Submit For VP,Submit for PC';
var PC_PC = '52151'; //'Submit For VP';

var gridValueChanged = false;
var gridrows = '';
var removeSysIds = '';
var strAnaView = '<%=strAnaView%>';
var strActivityLog = '<%=strActivityLog%>';
var columnnameType = false;
var columnnameChange = false;
var setValuefromClipboard = '';
var setChangeValuefromClipboard = '';
window.onbeforeunload = WarnUser;
function WarnUser(){  
	approve_0_a = gridObjAnalysis.getColIndexById("approve");
	gridObjAnalysis.forEachRow( function(id){
		if(gridObjAnalysis.getLevel(id)>=1)
		{	
			if(gridObjAnalysis.cellById(id,approve_0_a).getValue()==1)
			{
				flgChkSubmit = true;
			}
			
		}
	});
		if(flgChkSubmit && !submitClicked)   {      
			event.returnValue = message_sales[291];  
			 }  
		  else   {    
		// Reset the flag to its default value.      
		allowPrompt = true;  
		 }
	flgChkSubmit = false;
}

var gridObjAnalysisView;
/*
doLayoutCellExpand - is used to collapse the other headers if any Layout header is opened.
*/
function doLayoutCellExpand(id){
	if(id=='a'){	
		dhxLayout.cells("a").expand();
		dhxLayout.cells("b").setHeight(230);
		dhxLayout.cells("b").collapse();
		dhxLayout.cells("d").collapse();
	}
	if (id=='b'){	
		dhxLayout.cells("b").expand(); 
		dhxLayout.cells("b").setHeight(230);
		dhxLayout.cells("a").collapse();
		dhxLayout.cells("d").collapse();
	}
	if (id=='d'){	
		dhxLayout.cells("d").expand();
		dhxLayout.cells("b").setHeight(230);
		dhxLayout.cells("a").collapse();
		dhxLayout.cells("b").collapse();
	}
}
/*
 * fnDynamicHeight is used to set div based on screen height.
 */
function fnDynamicHeight(){
	var windowHeight = $(window).height();
	var divHeight = (windowHeight * 86)/100;
				
	$("#parentId").css("height",divHeight);
	$("#parentTable").css("height",divHeight);
}
function fnOnPageLoad()
{
	fnDynamicHeight();
	dhxLayout = new dhtmlXLayoutObject("parentId", "4E");
	progressOff(true);
	//document.frmPricingRequestForm.idSave.disabled=false;
	if(document.getElementById("processimg")!= undefined)
		document.getElementById("processimg").style.display='none';
	if(statuId=='52186' || statuId==''){
		//document.frmPricingRequestForm.idSaveNew.disabled=false;
	}
		if(strRequestId!="")// && statuId==""){
			document.frmPricingRequestForm.strAccType.disabled = true;
		if(strRequestId!=""  && document.frmPricingRequestForm.strGroupAcc!=undefined)// && statuId==""){
			document.frmPricingRequestForm.strGroupAcc.disabled = true;
		
	/*		document.frmPricingRequestForm.idSave.disabled=true;
			document.frmPricingRequestForm.idSaveNew.disabled=true;
			document.frmPricingRequestForm.idExport.disabled=true;*/
		//}
		
	if(strApprLvl == 'PC'){
		ApprLvl_fl = true;
	}else if(strApprLvl == 'AD'){
		ApprLvl_fl = false;
	}
		
	 document.frmPricingRequestForm.hstatusId.value ='<bean:write name="frmPricingRequestForm" property="hstatusId"/>';
     document.frmPricingRequestForm.hreqid.value ='<bean:write name="frmPricingRequestForm" property="strRequestId"/>';
     reqstatus = document.frmPricingRequestForm.hstatusId.value;
     reqid = document.frmPricingRequestForm.hreqid.value;
     fnSelectAll(document.frmPricingRequestForm.selectSystemAll,'checkSystems','unselectAll');     
     //Tab bar rendering
	 dhtmlXLayoutObject.prototype.extendClick = function() {
		 var t = this;
		 for (var a in this.polyObj) {
		 this.polyObj[a].firstChild.firstChild.onclick = function() {
		 t[t._isCollapsed(this._dockCell)?"_expand":"_collapse"](this._dockCell,"hide");
		 }
		 this.polyObj[a].firstChild.firstChild.childNodes[4].onclick = null;
		 }
		 }
	 dhxLayout.extendClick();
	 dhxLayout.cells("a").attachObject("Div_dhtmlxPanel0");
	 dhxLayout.cells("b").attachObject("Div_dhtmlxPanel1");
	//Attaching the Expand event with Layout object.
	 dhxLayout.attachEvent("onExpand", doLayoutCellExpand);
			
	 tabbar = dhxLayout.cells("c").attachTabbar();
	 dhxLayout.cells("d").attachObject("Div_dhtmlxPanel3");
 	 dhxLayout.cells("a").setHeight(160);
  /* dhxLayout.cells("b").setHeight(250);
	 dhxLayout.cells("d").setHeight(250); */
	 dhxLayout.cells("c").hideHeader();
	 
	 dhxLayout.cells("a").setText(message_sales[290]);
	 dhxLayout.cells("b").setText(message_sales[289]);
	 dhxLayout.cells("d").setText(message_sales[288]);
	 dhxLayout.cells("b").collapse();
	 dhxLayout.cells("d").collapse();
	 dhxLayout.setCollapsedText("a", message_sales[290]);
	 dhxLayout.setCollapsedText("b", message_sales[289]);
	 dhxLayout.setCollapsedText("d", message_sales[288]);
		 

     
	//tabbar = new dhtmlXTabBar("pricing_tabbar", "top");
	
	tabbar.setSkin("winscarf");
	tabbar.setImagePath("<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/imgs/");
	tabbar.addTab("group", message_sales[292], "150px");
	tabbar.addTab("analysis", message_sales[293], "150px");
	tabbar.addTab("analysisView", message_sales[294], "150px");
	//tabbar.addTab("workflow", "Workflow Summary", "150px");
	tabbar.addTab("activity", message_sales[295], "150px");
	tabbar.setTabActive("group");
	 
	//Price by group grid
	
	prlyt1 = tabbar.cells("group").attachLayout("1C","dhx_skyblue");

	prlyt1.cells("a").hideHeader();
	anltb1 = prlyt1.cells("a").attachToolbar();
	anltb1.setIconPath("<%=strExtWebPath%>/dhtmlx/dhtmlxToolbar/imgs/");
	anltb1.loadXML("<%=strJsPath%>/sales/pricing/groupButtonbar.xml");
	anltb1.attachEvent("onStateChange", onAnlToolBar1);
	gridObjGroup = prlyt1.cells("a").attachGrid();
	gridObjGroup.setImagePath("<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/imgs/");
	gridObjGroup.selMultiRows = true;
	gridObjGroup.setSkin("dhx_skyblue");
	gridObjGroup.setImagePath("<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/imgs/");
	gridObjGroup.enableTooltips("true,true,false,false,true,false,false,false,true,false,false,false");
	
	var editEventId = gridObjGroup.attachEvent("onEditCell",doOnCellEdit);
	
	// MNTTASK-5171. I commented below line for Drag & Drop. We cannot do edit in single click in combo or text box
	//gridObjGroup.enableEditEvents(true, false, false,false,false,false,false,false,false,false,false,false,false);

	gridObjGroup.attachEvent("onRowSelect" ,function(rowId,cellIndex){
		remove_0_g = gridObjGroup.getColIndexById("Remove");
		name_3_g = gridObjGroup.getColIndexById("Name");
		comment_9_g = gridObjGroup.getColIndexById("Comments");
		dtype_12_g = gridObjGroup.getColIndexById("dtype");
		type_6_g = gridObjGroup.getColIndexById("type");
		 effDate_13_g = gridObjGroup.getColIndexById("effDate");
		 pricedType = gridObjGroup.getUserData(rowId,"price_type");
		 select_2_g = gridObjGroup.getColIndexById("select");
		if (cellIndex== remove_0_g && gridObjGroup.getLevel(rowId)==0){
			if(statuId!='52190'){
				 if(confirm("Any Changes made will be Lost. Are you sure you want to Remove this System?")){
	          			fnDeleteSystem(rowId);
				 }/*else if(cellIndex == name_3_g && gridObjGroup.getLevel(rowId)==1 && pricedType == '52111'){
					fnGetPartDetails(rowId,cellIndex);
				 }*/else if (cellIndex == comment_9_g && gridObjGroup.getLevel(rowId)==0){
					fnOpenSysComments( document.frmPricingRequestForm.hreqid.value+'-'+rowId);
          		}
			}
		 }else if (cellIndex==comment_9_g && gridObjGroup.getLevel(rowId)==1){
          	fnOpenGroupComments(rowId);
         }else if(cellIndex==dtype_12_g || cellIndex==effDate_13_g){
         	disenDates(rowId,cellIndex);
         }
	});
	
	// MNTTASK-5171. Drag & Drop We cannot do edit in single click in combo or text box
	gridObjGroup.enableBlockSelection(true);
	gridObjGroup.attachEvent("onKeyPress",onKeyPressed);
	
	gridObjGroup.attachEvent("onOpenStart", onOpenStartTreeItem);
	gridObjGroup.init();
	gridObjGroup.loadXMLString('<%=gridGroupData%>');
	gridObjGroup.attachHeader("<label><input type='checkbox' value='no'  onClick='javascript:fnChangedFilter(this);'/>"+message_sales[287]+"</label>," + "#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan," + "#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan");
	if(GLOBAL_BrowserType == 'SAFARI' && GLOBAL_ClientSysType != 'PC'){
		gridObjGroup.enableValidation(true);
	}
	
	userdataacl = gridObjGroup.getUserData("","acl");
	userdesc = gridObjGroup.getUserData("","des");
	//Analysis tab with layout
	prlyt = tabbar.cells("analysis").attachLayout("1C","dhx_skyblue");

	gridObjAnalysis = prlyt.cells("a").attachGrid();
	prlyt.cells("a").hideHeader();
	anltb = prlyt.cells("a").attachToolbar();
	anltb.setIconPath("<%=strExtWebPath%>/dhtmlx/dhtmlxToolbar/imgs/");
	var des = gridObjGroup.getUserData("","des");
	if(des!=undefined && des!=''){
		anltb.loadXML("<%=strJsPath%>/sales/pricing/analysisButtonbar.xml");
	}else{
		anltb.loadXML("<%=strJsPath%>/sales/pricing/analysisButtonbarRep.xml");
	}
	
	anltb.attachEvent("onStateChange", onAnlToolBar);
	
	gridObjAnalysis.selMultiRows = true;
	gridObjAnalysis.setSkin("dhx_skyblue");
	gridObjAnalysis.setImagePath("<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/imgs/");
	gridObjAnalysis.enableTooltips("false,false,false,false,flase,false,false,false,true,false,false");
	
	gridObjAnalysis.enableEditEvents(true,false,false,false,false,false,false,false,false,false,false,false,false,false,false);
	//gridObjAnalysis.setEditable(false);
	gridObjAnalysis.enableTreeCellEdit(false);

	var editAnalEventId = gridObjAnalysis.attachEvent("onEditCell",doOnCellEditDisable);
	approve_0_a = gridObjAnalysis.getColIndexById("approve");
	DemoFL_19_a = gridObjAnalysis.getColIndexById("DemoFL");
	//Once Approved we should not changed anything.
	gridObjAnalysis.attachEvent("onRowSelect" ,function(rowId,cellIndex){
			 if (cellIndex== DemoFL_19_a){
	    	       gridObjAnalysis.cellById(rowId,cellIndex).setDisabled(true);
			}
			var Status = gridObjAnalysis.cellById(rowId,approve_0_a).getValue();
			//alert(cellIndex+":::"+approve_0_a);
			if(cellIndex == approve_0_a){
				//fnCallDeleteStatus(rowId,cellIndex,gridObjAnalysis);
			}
	});

	gridObjAnalysis.init();
	gridObjAnalysis.loadXMLString('<%=gridAnalysisData%>');
	
	gridObjAnalysis.attachEvent("onCheckbox",onClickSelect);
	//Analysis view data added by velu
	gridObjAnalysisView = tabbar.cells("analysisView").attachGrid();
	gridObjAnalysisView.selMultiRows = true;
	gridObjAnalysisView.setSkin("dhx_skyblue");
	gridObjAnalysisView.setImagePath("<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/imgs/");
	gridObjAnalysisView.enableTooltips("true,true,true,true,true,true,true,true,true,true,true,true,true");
	
	<%-- gridObjAnalysisView.setHeader("System Name,Group Name,Qty,Proposed Price,#cspan,Tripwire,#cspan,#cspan,Current Pricing,#cspan,2011 List,#cspan,#cspan",null,
									["text-align:left;","text-align:left;","text-align:left;","text-align:center;","text-align:center;","text-align:center;","text-align:center;","text-align:center;"]);
	gridObjAnalysisView.attachHeader("#rspan,#rspan,#rspan,Part,Construct,Trip,Construct,chk,Price,Construct,%Off,List,Construct");	--%>
	
	gridObjAnalysisView.init();
	gridObjAnalysisView.loadXMLString('<%=gridAnalysisDataView%>');	
		          
	//Work flow tab
<%--	gridObjWorkflow = tabbar.cells("workflow").attachGrid();
	gridObjWorkflow.enableEditEvents(false,false,false);
	gridObjWorkflow.setSkin("dhx_skyblue");
	gridObjWorkflow.setImagePath("<%=strJsPath%>/dhtmlx/dhtmlxGrid/imgs/");
	gridObjWorkflow.enableTooltips("false,false,false");
	gridObjWorkflow.setHeader("Entity,Pending,Approved");
	gridObjWorkflow.setColWidth(0,"150");
	gridObjWorkflow.setColWidth(1,"150");
	gridObjWorkflow.setColWidth(2,"150");
	
	gridObjWorkflow.init();
	gridObjWorkflow.loadXMLString('<%=gridXmlWorkflowData%>');--%>
	//Activity Log Tab	
	
	gridObjActivity = tabbar.cells("activity").attachGrid();
	gridObjActivity.setSkin("dhx_skyblue");
	gridObjActivity.setHeader("Activity Details");	
	gridObjActivity.init();
	gridObjActivity.loadXMLString('<%=gridXmlActivityData%>');
	gridObjActivity.setImagePath("<%=strJsPath%>/dhtmlx/dhtmlxGrid/imgs/");
	gridObjGroup.enableRowsHover(true,'grid_hover');
	gridObjAnalysis.enableRowsHover(true,'grid_hover');
 	//52186-Initiated,52187-Pending AD Approval,52122-Pending VP Approval,52123-Pending PC Approval
	//52189-Approved Not Implemented,52190-Approved Implemented
	//alert("Status:"+reqstatus+"::"+userdesc);
	
    /*if(reqstatus=='52186' || reqstatus=='52187'){
    		document.getElementById("idVoid").disabled=false;
    }else{
    		document.getElementById("idVoid").disabled=true;
    }*/
    

       // gridObjAnalysis.setEditable(false);
    //if therequest status is approved the report is read only
	document.getElementById("idDntImplm_tmp").style.display='none';
	if(reqstatus=='52190')
	{
		gridObjAnalysis.setEditable(false);	
		gridObjAnalysis.enableEditEvents(false,false,false);
    	document.getElementById("idSave").style.display='none';
    	document.getElementById("idVoid").style.display='none';
    	document.getElementById("idDntImplm").style.display='none';
    	document.getElementById("idDntImplm_tmp").style.display='block';
	}

	if(strPublOvrRide=='Y' && document.frmPricingRequestForm.chk_DntImplm!=undefined){
		
		document.frmPricingRequestForm.chk_DntImplm.checked=true;
	}

	//Hide the Checkbox [Do not implement Price] when the Account is having Contract .
	if(TRIM(contfl)=='YES')
	{
		document.getElementById("idDntImplm").style.display='none';
		document.getElementById("idDntImplm_tmp").style.display='block';
	}
	
    //to persist the view type-analysis/groupby
     document.frmPricingRequestForm.hviewType.value ='<bean:write name="frmPricingRequestForm" property="hviewType"/>';
     if(document.frmPricingRequestForm.hviewType.value=="analysis")
     {
     	 tabbar.setTabActive("analysis");
     }
     else 
     {
     	gridObjGroup.loadOpenStates("grp");
     }
   
       
    fnUpdateGrpConstPrice();
    
    //on select fuction for tab selection
    tabbar.attachEvent("onSelect", function(id,last_id){
    	if(id=='analysis')
		{
			fnShowHideButtons(id);// All button should be hide when click the Analysis view tab.
			//on analysis tab click if there any chnages in priced by Grop Grid need to save and fecth
			gridObjGroup.saveOpenStates("grp");
			var ids = gridObjGroup.getChangedRows(true);
			document.frmPricingRequestForm.hviewType.value="analysis";
			if(((gridObjGroup.getRowsNum()>0 && reqstatus=='') || (reqstatus!=''))&&
			  (gridObjGroup.getChangedRows(true).length>0 || grpGridChanged) )
			  {
				//fnOnSave(reqstatus, false, false);		
				grpGridChanged= false;
			  }
	    }
	    else if (id=="group")
	    {
	    	fnShowHideButtons(id);
	    	gridObjAnalysis.saveOpenStates("anl");
	    	gridObjGroup.loadOpenStates("grp");
	    	document.frmPricingRequestForm.hviewType.value="group";
	    	 return true;
	    }
	  /*else if(id == "workflow")
	    {
	    	fnShowHideButtons(id);
	    	fnLoadWorkflowData();
	    }*/else if(id == "analysisView"){
	    	fnShowHideButtons(id);
	    	fnGetAnalysisViewDetails(); //Ajax call - to get the Analysis View data 
	    }
	    else if(id == "activity"){
	    	fnGetActivityLogDetails(); //Ajax call - to get the Activity Log data 
	    }
    	
	    return true;
     });
     
    function fnShowHideButtons(id){
    	// Void,Submit,Save and Export Excel should be hide when click the Analysis View Tab other wise show and only should display the Excel Export link.
    	if(id == "analysisView"){
    		if(statuId!='' && statuId==52186)
    		document.frmPricingRequestForm.idSaveNew.disabled = true;
    		
    		document.frmPricingRequestForm.idVoid.disabled = true;
    		document.frmPricingRequestForm.idExport.disabled = true;
    		document.frmPricingRequestForm.idSave.disabled = true;
	    	document.getElementById("showAnalysisView").style.display = 'inline';
	    	document.getElementById("idDntImplm").style.display='none';
	    	document.getElementById("idDntImplm_tmp").style.display='block';
	    	
    	}else{
    		if(statuId!='' && statuId==52186 && SwitchUserFl != 'Y')
        		document.frmPricingRequestForm.idSaveNew.disabled = false;
    		if(SwitchUserFl != 'Y'){
    		document.frmPricingRequestForm.idVoid.disabled = false;
    		document.frmPricingRequestForm.idSave.disabled = false;
    		}
    		document.frmPricingRequestForm.idExport.disabled = false;
	    	document.getElementById("showAnalysisView").style.display = 'none';

	    	if(reqstatus != '52190' && TRIM(contfl)!='YES')
	    	{
	    		document.getElementById("idDntImplm").style.display='block';
	    		document.getElementById("idDntImplm_tmp").style.display='none';
	    	}
	    	
	    	if((reqstatus=='52122' || reqstatus=='52123')&& userdesc=='AD'){
	    	   	document.frmPricingRequestForm.idVoid.disabled = true;
	    	   	document.frmPricingRequestForm.idSave.disabled = true;
	    	}else if(reqstatus=='52123' && userdesc=='VP'){
	    		document.frmPricingRequestForm.idVoid.disabled = true;
	    		document.frmPricingRequestForm.idSave.disabled = true;
	    	}	
    	}
     }
    
    document.getElementById("showAnalysisView").style.display='none';
    
    if(errorMsg !=''){
    	document.getElementById("msg").innerHTML = errorMsg;
    	dhxLayout.cells("d").expand();
    }
    //Code for rendering the calendar component
 /*
    mCal = new dhtmlxCalendarObject("dhtmlxCalendar", false, {
		isMonthEditable: true,
		isYeaRitable: true
	});
	mCal.disableIESelectFix(true);
	mCal.setOnClickHandler(function(date,obj,type){
		document.getElementById("effectiveDate").value = mCal.getFormatedDate("%m/%d/%Y", date);
		
		var objC=document.getElementById("dhtmlxCalendar"); 
		if(objC.style.display=="none"){
		 objC.style.display="inline";
		}
		else{ 
			objC.style.display="none";	
		}
	});
	
	mCal.draw();
	document.getElementById("dhtmlxCalendar").style.display="none";
	*/
	if((reqstatus=='52122' || reqstatus=='52123')&& userdesc=='AD'){
	   	document.frmPricingRequestForm.idVoid.disabled = true;
	   	document.frmPricingRequestForm.idSave.disabled = true;
	}else if(reqstatus=='52123' && userdesc=='VP'){
		document.frmPricingRequestForm.idVoid.disabled = true;
		document.frmPricingRequestForm.idSave.disabled = true;
	}
	if( strAccountId!='' && dispflg == 'Y'){
		dhxLayout.cells("a").collapse();
		dhxLayout.cells("b").expand();
	}
	
	if(document.frmPricingRequestForm.strGroupAcc == undefined)
		document.getElementById("HistoricalGrpAccDiv").style.display = 'none';
		
	// While select Group Account in Type drop down , will be displayed H & S Icon.	
	if(document.frmPricingRequestForm.strGroupAcc!=undefined && document.frmPricingRequestForm.strGroupAcc.value!=0){
		document.getElementById("HistoricalGrpAccDiv").style.display = 'inline';
	}else if(document.frmPricingRequestForm.strGroupAcc != undefined){
		document.getElementById("HistoricalGrpAccDiv").style.display = 'none';
	}
	strGroupName = strGroupName.substring(0,strGroupName.indexOf('('));
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad()">
<html:form action="/gmPricingRequestDAction.do?method=savePriceRequest">
	<html:hidden property="strOpt" />
	<html:hidden property="hTxnId" value="" />
	<html:hidden property="hTxnName" value="" />
	<html:hidden property="hCancelType" value="VDPRRQ" />
	<html:hidden property="hRedirectURL" value="" />
	<html:hidden property="haction" value="" />
	<html:hidden property="hreqid" value="" />
	<html:hidden property="hstatusId" value="" />
	<html:hidden property="hinputStr" value="" />
	<html:hidden property="hviewType" value="" />
	<html:hidden property="hinputAnswer" value="" />
	<html:hidden property="anlData" value="" />
	<html:hidden property="hDeniedinputStr" value="" />
	<html:hidden property="strRemoveSysIds" value="" />
	<input type="hidden" name="hSkipCmnCnl" value="">
	<input type="hidden" name="hDisplayNm" value="">
	<html:hidden property="apprlvl" value="" />
	<html:hidden property="strPublOvrRide" value="" />
	<html:hidden property="strAccGPO" value="<%=strAccGPO%>" />
	
	<table border="0" bordercolor="green" class="DtTable1200" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtPricingRequest:message key="LBL_PRICE_REQ"/></td>
			<td align="right" class=RightDashBoardHeader><fmtPricingRequest:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
		</tr>
		<tr>
			<td valign="top" colspan="4">
			<table border="0" valign="top" width="100%" cellspacing="0" cellpadding="0">
				<tr>
				<td colspan="4">
				<div id="parentId" style="width: 1198px; height: 780px; position: relative;">
				<table id="parentTable" border="0" cellspacing="0" cellpadding="0" style="width: 1198px; height: 780px; position: relative;">
				<tr>
							<td colspan="4">
				<div id="Div_dhtmlxPanel0"style="height:100%;overflow:auto;">
				<table border="0" bordercolor="red" cellspacing="0" cellpadding="0">
					<tr>
						<td height="24" class="RightTableCaption" align="right"><font color="red">*</font> <fmtPricingRequest:message key="LBL_TYPE"/> :</td>
						<td>&nbsp;&nbsp;<gmjsp:dropdown controlName="strAccType" SFFormName="frmPricingRequestForm"	SFSeletedValue="strAccType" SFValue="alAccountType" codeId="CODEID" codeName="CODENM" onChange="javascript:fnLoadAcctType(this);"
								defaultValue="" /></td>
								<logic:equal name="frmPricingRequestForm" property="strAccType"	value="903108">
						<td height="24" class="RightTableCaption" align="right"> <fmtPricingRequest:message key="LBL_GRP_ACC_LIST"/> : </td>	
						<td width=25%>&nbsp;&nbsp;<gmjsp:dropdown controlName="strGroupAcc" SFFormName="frmPricingRequestForm"	SFSeletedValue="strGroupAcc" SFValue="alGroupList" codeId="ID" codeName="NAME" onChange="javascript:fnLoadGroupAcct(this);"
								defaultValue="[Choose One]" /></td>
								</logic:equal>
					</tr>
					<tr class="Shade">
						<logic:equal name="frmPricingRequestForm" property="hstatusId"	value="">
							<td height="24" class="RightTableCaption" align="right"><font color="red">*</font> <fmtPricingRequest:message key="LBL_ACC_LIST"/> :</td>
							<td width=35%>&nbsp;&nbsp;<gmjsp:dropdown controlName="strAccountId" SFFormName="frmPricingRequestForm"	SFSeletedValue="strAccountId" SFValue="alAccountList" codeId="ID" codeName="NM" onChange="javascript:fnLoadAcct(this);"
								width="300" defaultValue="[Choose One]" />
								</td>
						</logic:equal>
						<logic:notEqual name="frmPricingRequestForm" property="hstatusId" value="">
							<td height="24" class="RightTableCaption" align="right"><fmtPricingRequest:message key="LBL_ACC_NAME"/> :</td>
							<td>&nbsp;&nbsp;<bean:write
								name="frmPricingRequestForm" property="strAccountName" />
								<a href="#" onClick="javascript:fnHistoricalSalesAccRpt()" ><fmtPricingRequest:message key="IMG_ALT_SALES" var = "varSales"/><img id='imgEdit' height="15" width="15" align="middle" style='cursor:hand' border="0" src='<%=strImagePath%>/ordsum.gif' title='${varSales}'"/></a>
								</td>
						</logic:notEqual>
						<td height="24" class="RightTableCaption" align="right"><fmtPricingRequest:message key="LBL_ID"/> :</td>
						<td id="strAccountId" width=25%>&nbsp;&nbsp;
						<logic:equal name="frmPricingRequestForm" property="strAccType" value="903107">
							<bean:write	name="frmPricingRequestForm" property="strAccountId" /></td>
						</logic:equal>
						<logic:equal name="frmPricingRequestForm" property="strAccType" value="903108">
							<bean:write	name="frmPricingRequestForm" property="strGroupAcc" /></td>
						</logic:equal>
					</tr>
					<tr>
						<td height="24" class="RightTableCaption" align="right">
						<fmtPricingRequest:message key="LBL_INITIATED_BY"/> :</td>
						<td>&nbsp;&nbsp;<bean:write
							name="frmPricingRequestForm" property="strInitiateByName" /></td>
						<td height="24" class="RightTableCaption" align="right"> <fmtPricingRequest:message key="LBL_INITIATEED_DATE"/> :</td>
						<td width=25%>&nbsp;&nbsp;<bean:write name="frmPricingRequestForm" property="strInitiateDate" /></td>
					</tr>
					<tr class="Shade">
						<td height="24" class="RightTableCaption" align="right"><fmtPricingRequest:message key="LBL_CONTRACT_FLAG"/> :</td>
						<td width=25%>
						<logic:equal name="frmPricingRequestForm" property="strContractFl" value="NO">
						&nbsp;&nbsp;<bean:write name="frmPricingRequestForm" property="strContractFl" />
						</logic:equal>
						<logic:equal name="frmPricingRequestForm" property="strContractFl" value="YES">
						&nbsp;&nbsp;<font color="red"><b> <bean:write name="frmPricingRequestForm" property="strContractFl" /></b></font>
						</logic:equal>
						</td>
						<td height="24" class="RightTableCaption" align="right"> <fmtPricingRequest:message key="LBL_GRP_PRICE_BOOK"/> :</td>
						<td width=25%>
						<%-- <logic:equal name="frmPricingRequestForm" property="strAccType"	value="903108"> --%>
						&nbsp;&nbsp;<a href="#" onClick="javascript:fnGroupAccountPopUp();" ><bean:write
							name="frmPricingRequestForm" property="strGroupName" />
							</a>
								<div id="HistoricalGrpAccDiv">
								<fmtPricingRequest:message key="IMG_ALT_SALES_GRP_ACC" var = "varSalGrp"/><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/ordsum.gif' height="15" width="15" title='${varSalGrp} ' onClick="javascript:fnHistoricalSalesGrpAccSysRpt()"/>
								<fmtPricingRequest:message key="IMG_ALT_SALES_ACC" var = "varSalesAcc"/><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/HospitalIcon.gif' height="15" width="15" title='${varSalesAcc} ' onClick="javascript:fnHistoricalSalesGrpAccRpt()"/>
								</div>
							 
						<%-- </logic:equal> --%>
						</td>
						</tr>
					<!-- MNTTASK:6074 - Account Affiliation Start code --> 
						<tr>
						<td class="RightTableCaption" align="right"><fmtPricingRequest:message key="LBL_IDN"/> :</td>
						<td width=25%>&nbsp;&nbsp;<bean:write name="frmPricingRequestForm" property="strGprAff" /></td>
						<td  class="RightTableCaption" align="right"><fmtPricingRequest:message key="LBL_GPO"/> :</td>
						<td width=25%>&nbsp;&nbsp;<bean:write name="frmPricingRequestForm" property="strHlcAff" /></td>
						</tr> 
						<!-- Account Affiliation End code -->
					<tr>
						<td colspan="1"></td>
						<td colspan="3">
						<div id="dhtmlxCalendar" style="position: absolute; z-index: 10;"></div>
						</td>
					</tr>
					<tr class="Shade">
					<td colspan="4">&nbsp;&nbsp;<div id="msg" style="color:red;font-weight:bold;text-align:center"></div></td>
					</tr>
				</table>
				</div>
				</td>
				</tr>
						<tr>
							<td colspan="4">
							<!-- Layout cell a  starts -->
							<div id="Div_dhtmlxPanel1"style="height:100%;overflow:auto;">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr class="Shade">
									<td height="24" class="RightTableCaption" align="right" width="18%"><fmtPricingRequest:message key="LBL_REGION"/> :</td>
									<td width="25%">&nbsp;&nbsp;<bean:write name="frmPricingRequestForm" property="strRegName" /></td>
									<td height="24" class="RightTableCaption" align="right" width="25%"><fmtPricingRequest:message key="LBL_SALES_REP"/> :</td>
									<td width="25%">&nbsp;&nbsp;<bean:write name="frmPricingRequestForm" property="strRepName" /></td>
								</tr>
								<tr>
									<td height="24" class="RightTableCaption" align="right" width="25%"><fmtPricingRequest:message key="LBL_TERRITORY"/> :</td>
									<td width="25%">&nbsp;&nbsp;<bean:write name="frmPricingRequestForm" property="strTerName" /></td>
									<td height="24" class="RightTableCaption" align="right" width="25%"><fmtPricingRequest:message key="LBL_DISTRIBUTOR"/> :</td>
									<td width="25%">&nbsp;&nbsp;<bean:write name="frmPricingRequestForm" property="strDName" /></td>
								</tr>
								<tr class="Shade">
									<td height="24" class="RightTableCaption" align="right"><fmtPricingRequest:message key="LBL_AREA_DIRECTOR"/> :</td>
									<td>&nbsp;&nbsp;<bean:write name="frmPricingRequestForm"property="strAdName" /></td>
									<td height="24" class="RightTableCaption" align="right"><fmtPricingRequest:message key="LBL_VP_NAME"/> :</td>
									<td>&nbsp;&nbsp;<bean:write name="frmPricingRequestForm"property="strVpName" /></td>
								</tr>

								<%if(isSysEnabled){%>
								<tr>
									<td colspan="4" class="Line"></td>
								</tr>
								<tr>
									<table border="0">
									<tr>
										<td style="width: 260px"></td>
										<td>
										<div class="gmFilter" id="Div_System" style="width: 380px; height: 150px">
										<table> <tr><td bgcolor="gainsboro" ><input type="checkbox" name="selectSystemAll" onclick="fnSelectAll(this,'checkSystems','toggle');">  <B><fmtPricingRequest:message key="LBL_SELECT_ALL"/> </B> </td>
										</tr></table>
										<table cellspacing="0" cellpadding="0">
											<logic:iterate id="alSystems" name="frmPricingRequestForm"
												property="alSystemList">
												<tr>
													<td><htmlel:multibox property="checkSystems" value="${alSystems.ID}" /> <bean:define id="sysNm"	name="alSystems" property="NAME" type="java.lang.String">
													</bean:define><%=sysNm%></td>
												</tr>
											</logic:iterate>
										</table>
										</div>
										</td>
										<td>
										<table>
											<tr>
												<td align="left"><fmtPricingRequest:message key="BTN_ADD" var="varAdd"/><gmjsp:button style="width: 5em; height: 2em; font-size: 13px" value="${varAdd}&nbsp;" buttonType="Save" controlId="Btn_Add" onClick="javascript:fnAddSytems();" name="Btn_Add" gmClass="Button" /></td>
												<td align="left"><fmtPricingRequest:message key="BTN_SYSTEM" var="varSystem"/><gmjsp:button style="width: 12em; height: 2em; font-size: 13px" value="&nbsp;${varSystem}&nbsp;" buttonType="Save" onClick="javascript:fnUnpricedSystem();" controlId="Btn_UnPrcSys" gmClass="Button" /></td>
												<td align="left"><fmtPricingRequest:message key="BTN_GROUP" var="varGroup"/><gmjsp:button style="width: 12em; height: 2em; font-size: 13px" value="&nbsp;${varGroup}&nbsp;" buttonType="Save" onClick="javascript:fnUnpricedGroup();" controlId="Btn_UnPrcGrp" gmClass="Button" /></td>
											</tr>
											<tr>
												<td></td>
												<td><div id="processimg" align="middle"><IMG border=0 height="25" width="20" align="middle"  src="<%=strImagePath%>/process.gif"></img><font color="red" align="middle"><b>&nbsp;&nbsp;<fmtPricingRequest:message key="LBL_LOADING"/></b></font></div></td>
											</tr>
										</table>
										</td>
									</tr>
								</table>
								</tr>
								<%}%>
							</table>
							</div>
							<!-- Layout Cell a Ends -->
							</td>
						</tr>
						<!-- Layout Cell b we are rendering Tab bar -->
						<tr>
							<td colspan="4">
							<!-- Layout Cell c Starts -->
							<div id="Div_dhtmlxPanel3" style="height:100%;overflow:auto;">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td colspan="4" align="center" height="30"><jsp:include
										page="/common/GmQuestions.jsp">
										<jsp:param name="FORMNAME" value="frmPricingRequestForm" />
										<jsp:param name="ALQUESTION" value="alQuestion" />
										<jsp:param name="ALANSWER" value="alAnswer" />
										<jsp:param name="ALANSWERGROUP" value="alAnswerGroup" />
										<jsp:param name="ALQUESTIONANSWER" value="alQuestionAnswer" />
									</jsp:include></td>
								</tr>
								<tr>
									<td colspan="4" align="center" height="30"><jsp:include	page="/common/GmIncludeLog.jsp">
										<jsp:param name="FORMNAME" value="frmPricingRequestForm" />
										<jsp:param name="ALNAME" value="alLogReasons" />
										<jsp:param name="LogMode" value="Edit" />
									</jsp:include></td>
								</tr>
							</table>
							</div>
							<!-- Layout Cell c Ends -->
							</td>
						</tr>
					</table>
					</div>
					<!-- Main Layout Ends -->
					</td>
				</tr>	
				
				<tr>
					<td><div style=" position: relative; "><table ><tr>
					<td align="right" id="idDntImplm" width="370px" ><div><input type="checkbox" name="chk_DntImplm"> <B><fmtPricingRequest:message key="LBL_NOT_TO_IMPLEMENT"/></B></div></td>
					<td align="right" id="idDntImplm_tmp" width="370px">&nbsp;</td>
					
					<td colspan="4" align="left">
								
					<fmtPricingRequest:message key="BTN_VOID" var="varVoid"/><gmjsp:button style="width: 5em; height: 2em; font-size: 13px" controlId="idVoid" value="${varVoid}" buttonType="Save" gmClass="Button" onClick="javascript:fnVoid();" />&nbsp;
					<% if(hstatusId.equals("") || hstatusId.equals("52186")){%>
					<fmtPricingRequest:message key="BTN_SAVE" var="varSave"/><gmjsp:button	controlId="idSaveNew" style="width: 8em; height: 2em; font-size: 13px" value="${varSave}" gmClass="Button" buttonType="Save"	onClick="fnOnSave('Save');" />&nbsp;
					<%}%>
					<fmtPricingRequest:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button	controlId="idSave" style="width: 8em; height: 2em; font-size: 13px" value="${varSubmit}" gmClass="Button" buttonType="Save"	onClick="fnOnSave('Submit');" />&nbsp;
					<% if(strClientSysType.equals("PC")) {%>
					<fmtPricingRequest:message key="BTN_EXPORT_EXCEL" var="varExpExcel"/><gmjsp:button style="width: 10em; height: 2em; font-size: 13px" controlId="idExport"	value="${varExpExcel}"	gmClass="Button" buttonType="Load" onClick="javascript:fnExport();"/>
					<% }else{%>
					<input type="hidden"  id="idExport"	value="Export to Excel"	"/>
						<%}%>
					</td></tr></table></div></td>
				</tr>
				<tr>
				<td colspan="6" align="center">
					<div id="showAnalysisView">
					<% if(strClientSysType.equals("PC")) {%>
					<div class='exportlinks'><fmtPricingRequest:message key="DIV_EXPORT_OPT"/> : <img	src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="gridObjAnalysisView.toExcel('/phpapp/excel/generate.php');"> <fmtPricingRequest:message key="DIV_EXCEL"/> </a></div>
					<% }%>
					</div>
				</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
