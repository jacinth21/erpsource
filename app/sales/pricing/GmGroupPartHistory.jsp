<%
/**********************************************************************************
 * File		 		: GmGroupPartHistory.jsp
 * Desc		 		: display Group part history
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ taglib prefix="fmtGroupPartHistory" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmGroupPartHistory.jsp -->

<fmtGroupPartHistory:setLocale value="<%=strLocale%>"/>
<fmtGroupPartHistory:setBundle basename="properties.labels.sales.pricing.GmGroupPartHistory"/>
  
<bean:define id="ldtResult" name="frmGroupPartMapping" property="ldtResult" type="java.util.List"></bean:define>
 
<HTML>
<HEAD>
<TITLE> Globus Medical: Multiple Part Group </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
 
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

 

</HEAD>
<html:form action="/gmGroupPartMap.do">
<html:hidden property="hpnum" />
<BODY leftmargin="20" topmargin="10">
 
	<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
	 
	 	<tr>
			<td colspan="5" height="25" class="RightDashBoardHeader">
			<logic:notEqual  name="frmGroupPartMapping" property="hpnum" value=""><fmtGroupPartHistory:message key="LBL_PART_HISTORY"/></logic:notEqual>
			<logic:equal  name="frmGroupPartMapping" property="hpnum" value=""><fmtGroupPartHistory:message key="LBL_ALL_PART_HISTORY"/></logic:equal>
			
			</td> 
			
		</tr>
		<tr><td colspan="5" height="1" class="line"></td></tr>
		
		 <tr>
	                     <td colspan="5">
	                     	<display:table  name="requestScope.frmGroupPartMapping.ldtResult" class="its" id="currentRowObject" requestURI="/gmGroupPartHistory.do"  > 
							<fmtGroupPartHistory:message key="DT_GROUP_NAME" var="varGrpName"/>
							<display:column property="GROUPNM" title="${varGrpName}"   group="1"  class="alignleft"/>
							<fmtGroupPartHistory:message key="DT_PART_NUMBER" var="varPartNumber"/>
							<display:column property="PARTNUM" title="${varPartNumber}"   group="2"  class="alignleft"/>
							<fmtGroupPartHistory:message key="DT_PRIMARY_SECONDARY" var="varPrimarySec"/>
							<display:column property="PRIMARY_SECONDARY" title="${varPrimarySec}"    class="alignleft"/>
							<fmtGroupPartHistory:message key="DT_LIST_PRICE" var="varListPrice"/>
							<display:column property="LISTPRICE" title="${varListPrice}"    class="alignright"/>
							<fmtGroupPartHistory:message key="DT_AD_LIMIT" var="varAdLimit"/>
							<display:column property="ADLIMIT" title="${varAdLimit}"    class="alignright"/>
							<fmtGroupPartHistory:message key="DT_VP_LIMIT" var="varVpLimit"/>
							<display:column property="VPLIMIT" title="${varVpLimit}"    class="alignright"/> 							
							<fmtGroupPartHistory:message key="DT_TRIP_WIRE" var="varTripWire"/>
							<display:column property="TRIPWIRE" title="${varTripWire}"    class="alignright"/> 							
							<fmtGroupPartHistory:message key="DT_CREATED_BY" var="varCreatedBy"/>
							<display:column property="CREATED_BY" title="${varCreatedBy}"    class="alignleft"/>
							<fmtGroupPartHistory:message key="DT_CREATED_DATE" var="varCreatedDate"/>
							<display:column property="CREATED_DATE" title="${varCreatedDate}"    class="alignleft"/>
							<fmtGroupPartHistory:message key="DT_ACTION" var="varAction"/>
							<display:column property="LOGTYPE" title="${varAction}"    class="alignleft"/>
							<fmtGroupPartHistory:message key="DT_ACTION_BY" var="varActionBy"/>
							<display:column property="ACTION_BY" title="${varActionBy}"    class="alignleft"/>
							<fmtGroupPartHistory:message key="DT_ACTION_DATE" var="varActionDate"/>
							<display:column property="ACTION_DATE" title="${varActionDate}"    class="alignleft"/>
							</display:table>

    		             </td>
                    </tr> 
        
		 
    </table>		     	
 
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</html:form>
</HTML>

