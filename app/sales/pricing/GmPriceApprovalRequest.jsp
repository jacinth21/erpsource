<%
	/**********************************************************************************************
	 * File		 		: GmPriceApprovalRequest.jsp
	 * Desc		 		: This screen is used to review and update the saved Price Request Details
	 * Version	 		: 1.0
	 * author			: Harinadh Reddi
	 **********************************************************************************************/
%>

 <!-- \sales\pricing\GmPriceApprovalRequest.jsp --> 
	<%@ include file="/common/GmHeader.inc" %>
	<%@ page language="java" %>
	<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
	
	<%@ page import ="com.globus.common.beans.GmCommonControls"%>	
	<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
	
 	<bean:define id="xmlGridData" name="frmPriceApprovalRequest" property="xmlGridData" type="java.lang.String"> </bean:define>
 	<bean:define id="priceRequestId" name="frmPriceApprovalRequest" property="priceRequestId" type="java.lang.String"></bean:define>
 	<bean:define id="statusId" name="frmPriceApprovalRequest" property="statusId" type="java.lang.String"></bean:define>
 	<bean:define id="denyAccesFl" name="frmPriceApprovalRequest" property="denyAccesFl" type="java.lang.String"></bean:define>
 	<bean:define id="approveAccFl" name="frmPriceApprovalRequest" property="approveAccFl" type="java.lang.String"></bean:define>
 	<bean:define id="statusUpdAccFl" name="frmPriceApprovalRequest" property="statusUpdAccFl" type="java.lang.String"></bean:define>
 	<bean:define id="pcAccFl" name="frmPriceApprovalRequest" property="pcAccFl" type="java.lang.String"></bean:define>
 	<bean:define id="advpAccFl" name="frmPriceApprovalRequest" property="advpAccFl" type="java.lang.String"></bean:define>
 	<bean:define id="last12MonthSales" name="frmPriceApprovalRequest" property="last12MonthSales" type="java.lang.String"></bean:define>
 	<bean:define id="accountId" name="frmPriceApprovalRequest" property="accountId" type="java.lang.String"></bean:define>
 	<bean:define id="groupPriceId" name="frmPriceApprovalRequest" property="groupPriceId" type="java.lang.String"></bean:define>
 	<bean:define id="recountCount" name="frmPriceApprovalRequest" property="recountCount" type="java.lang.String"></bean:define> 
 	<bean:define id="checkFlag" name="frmPriceApprovalRequest" property="checkFlag" type="java.lang.String"></bean:define>	 	
 	<bean:define id="genPrtFileFl" name="frmPriceApprovalRequest" property="genPrtFileFl" type="java.lang.String"></bean:define>
 	<bean:define id="count" name="frmPriceApprovalRequest" property="count" type="java.lang.String"></bean:define> 
 	<bean:define id="idnUpdAccess" name="frmPriceApprovalRequest" property="idnUpdAccess" type="java.lang.String"></bean:define>
    <bean:define id="strDisabled" name="frmPriceApprovalRequest" property="strDisabled" type="java.lang.String"></bean:define>
 	
 	<%
 	String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");
 	String strCurrencySymbl = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
 	String strCurrencyFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplCurrFmt"));
 
	%>
<HTML>
<HEAD>	
	<TITLE> Globus Medical: Price Request - Request Details</TITLE>
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">	
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
	<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
	
	<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
	<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
	<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
	<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>	
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>	
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxTreeGrid/dhtmlxtreegrid.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxTreeGrid/ext/dhtmlxtreegrid_filter.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
	<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmPriceApprovalRequest.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>	
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>	
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pivot.js"></script>	
	<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_markers.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
	
	<style type="text/css" media="all">
	@import url("<%=strCssPath%>/displaytag.css");
	  @import url("<%=strCssPath%>/screen.css");
	table.DtTable1400 {
		margin: 0 0 0 0;
		width: 1400px;
		border: 1px solid  #676767;
	}

	.grid_hover {
		background-color: #E6E6FA;
		font-size: 20px;
	}

	/* div.gridbox_dhx_skyblue table.obj tr td {
		border-width: 0px 1px 0px 0px;
		border-color: #A4BED4;
	} */
	
	div.gridbox_dhx_skyblue table.obj tr td{
		
		border-width: 1px 1px;
		border-color : #FDFDFD #A4BED4 #A4BED4 #FDFDFD;
		background-color:transparent;
}
	
</style>
	<script>
	    
		var objGridData;
		objGridData = '<%=xmlGridData%>';
		var gridObj ='';
		var reqStatus = '<%=statusId%>';
		var denyAccess = '<%=denyAccesFl%>';
		var apprvlAccess = '<%=approveAccFl%>';
		var statUpdAccFl = '<%=statusUpdAccFl%>';
		var pcAccess = '<%=pcAccFl%>';
		var advpAccess = '<%=advpAccFl%>';
		var totrowCount = '<%=recountCount%>';
		var genPrtFile = '<%=genPrtFileFl%>';
	</script>	
</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();" onkeydown="javaScript:return fnLoadPriceReqDetails();">
	<html:form action="/gmPriceApprovalRequestAction.do?" >
	<html:hidden property="strOpt"/>
	<html:hidden property="inputString"/>
	<html:hidden property="implementString"/>
	<html:hidden property="hiddenPriceReqID" value="<%=priceRequestId%>"/>	
	<input type="hidden" name="sysProjectString">
    <input type="hidden" name="hAction" value="">
    <input type="hidden" name="hTxnId" value="">
    <input type="hidden" name="hCancelType" value=""/>
    <input type="hidden" name="rebate" value=""/>
    <input type="hidden" name="groupPriceId" value=""/>
    <input type="hidden" name="priceInstruments" value=""/>
    

	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" bordercolor="red" style="white-space: nowrap;" class="DtTable1400"  cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;Price Request - Request Details</td>
			<td align="right" colspan="4" class=RightDashBoardHeader>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("PRICE_REQUEST_DETAILS")%>');" />
			</td>
		</tr>
		<TR>
			<td class="RightTableCaption" align="right" HEIGHT="25" ><font color="red">*</font>&nbsp;Pricing Request ID:</td>
			<td align="left">
			<table><tr>
			<td width ="225px">
			&nbsp;<html:text maxlength="50" property="priceRequestId" size="25"   
			styleClass="InputArea" style="text-transform: uppercase;" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
			&nbsp;<gmjsp:button value="&nbsp;Load&nbsp;" style="height: 22px; width: 4em;"  name="Btn_Submit" buttonType="Load" onClick="fnLoad();"/> 			
			</td>	
			<%if(checkFlag == "Y") {%>
			<td align="left" style="padding-top: 3px;">
				<a href="javascript:fnBusinessQuestions();"><img src="<%=strImagePath%>/b.jpg" title="Business Questions"  border=0></a>
				<a href="javascript:fnViewAccountDetails();"><img src="<%=strImagePath%>/icon_info.gif" title="Account/Group details"  border=0></a>
			</td>
			<%} %>
			</tr></table>
			</td>
			<td class="RightTableCaption" align="right" HEIGHT="25" >&nbsp;Status:</td>
			<td align="left" colspan="4">&nbsp;<bean:write name="frmPriceApprovalRequest" property="statusName"></bean:write></td>
		</TR>
		<tr><td class="LLine" colspan="8" height="1"></td></tr>
		<tr class="shade">
<td class="RightTableCaption" align="right" HEIGHT="25" >&nbsp;Account/GPB ID:</td>	
<td align="left">&nbsp;<bean:write name="frmPriceApprovalRequest" property="gpbid"></bean:write></td>&nbsp;&nbsp;
<td class="RightTableCaption" align="right" HEIGHT="25" >&nbsp;Account/GPB Name:</td>	
<td align="left">&nbsp;<bean:write name="frmPriceApprovalRequest" property="gpbName"></bean:write></td>

</tr>
<tr><td class="LLine" colspan="8" height="1"></td></tr>
<tr>
<td class="RightTableCaption" align="right" HEIGHT="25" >&nbsp;GPO Affiliation:</td>	
		<td align="left">&nbsp;<bean:write name="frmPriceApprovalRequest" property="strhlcaff"></bean:write></td>&nbsp;&nbsp;
<td class="RightTableCaption" align="right" HEIGHT="25" >&nbsp;Group Price Book:</td>
		<td align="left">&nbsp;<bean:write name="frmPriceApprovalRequest" property="strgpb"></bean:write></td>
</tr>
		<tr><td class="LLine" colspan="8" height="1"></td></tr>
		<tr class="shade">			
			<td height="25" class="RightTableCaption" align="Right">Initiated By:</td>
			<td align="left" >&nbsp;<bean:write name="frmPriceApprovalRequest" property="initiatedBy"></bean:write></td>				
			<td height="25" class="RightTableCaption" align="Right">Initiated Date:</td>
			<td align="left"  colspan="4">&nbsp;<bean:write name="frmPriceApprovalRequest" property="initiatedDate"></bean:write></td>		
		</tr>
		<tr><td class="LLine" colspan="8" height="1"></td></tr>
		<tr>			
			<td height="25" class="RightTableCaption" align="Right" >Currency:</td>	
			<td align="left" >&nbsp;<bean:write name="frmPriceApprovalRequest" property="currency"></bean:write></td>		
			<td height="25"  class="RightTableCaption" align="Right">Last 12 Months Sales:</td>			
			<td align="left" colspan="4">&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(last12MonthSales,strCurrencyFmt))%>
			<% if(!last12MonthSales.equals("0") && !last12MonthSales.equals("") && !accountId.equals("")){%>
			 <a href="javascript:fnViewAccountSales('<%=accountId%>');"><img src="<%=strImagePath%>/ordsum.gif" title="Last 12 Month Sales"  border=0></a>			
			<%} %>
			</td>			
		</tr>		
		<%if(xmlGridData.indexOf("cell") != -1){%>
			<tr>
				<td colspan="8"><div  id="PriceReqDetails" style="" height="550px" width="1390px"></div></td>
			</tr>
			<% String strbutton = "Documents"+"("+count+")"; %>
			<tr><td colspan="8" align="center">
			<gmjsp:button name ="idVoid" value="Submit" gmClass="button" onClick="fnSubmit();" buttonType="Save" disabled = "<%=strDisabled %>"/>
			<gmjsp:button name ="fileUpload" value="<%=strbutton%>"  gmClass="button" onClick="fnFileUpload();" buttonType="Save" />
			</td>
				</tr>
			<tr><td class="LLine" colspan="8" height="1"></td></tr>
			<tr>
			<td align="center" class="RightTableCaption" colspan="8">&nbsp;
			        Choose Action: &nbsp;<select name="Cbo_Action1" class="RightText"
					onFocus="changeBgColor(this,'#AACCE8');"
					onBlur="changeBgColor(this,'#ffffff');"><option value="0">[ChooseOne]
						<option value="GPOREF">View GPO Ref</option>
						<option value="EXPEXCEL">Export to Excel</option>
						<option value="EXPEXCELOUS">Export to Excel-OUS</option>
						<option value="GRTPRICE">Generate Price File</option>
						<option value="INTSTATUS">Rollback to Initiated status</option>
						<option value="GPBCOMPARISON">GPB Comparison Map</option>
						<option value="VOIDPRICEREQUEST">Void Price Request</option>
				</select> <gmjsp:button name ="idVoidNew" value="Go" gmClass="button" onClick="fnGo();" buttonType="Save" disabled = "<%=strDisabled %>"/>&nbsp;</td>
		</tr>		
		<%}else if(!xmlGridData.equals("")){%>
			<tr><td colspan="8" align="center" class="RightText">Nothing found to display</td></tr>
		<%}else{%>
			<tr><td colspan="8" align="center" class="RightText">No data available</td></tr>
		<%} %>
		</table>					
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>