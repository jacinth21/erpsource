<%
/**********************************************************************************
 * File		 		: GmConstructBuilder.jsp
 * Desc		 		: Setup the construct for a system
 * Version	 		: 1.0
 * author			: Ramdas Mayya
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import="java.util.ArrayList,java.util.*" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtConstructBuilder" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmConstructBuilder.jsp -->

<fmtConstructBuilder:setLocale value="<%=strLocale%>"/>
<fmtConstructBuilder:setBundle basename="properties.labels.sales.pricing.GmConstructBuilder"/>
<% 
String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");
String strWikiTitle = GmCommonClass.getWikiTitle("OP_GROUP_PART_MAP");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Construct Builder </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">  

<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmPricingCommon.js"></script>
<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmConstructBuilder.js"></script>
<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmCommonBuilder.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/displaytag.css"); 
      div.gridbox_dhx_skyblue table.obj tr td{
     border-width:0px 1px 0px 0px;
     border-color : #A4BED4;
}
</style>

<bean:define id="alGroupListDef" name="frmConstructBuilder" property="alGroupList" type="java.util.List"></bean:define>
<bean:define id="gridData" name="frmConstructBuilder" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="strEnable" name="frmConstructBuilder" property="strEnable" type="java.lang.String"></bean:define>
<bean:define id="strVoidAccess" name="frmConstructBuilder" property="voidAccess" type="java.lang.String"></bean:define>
<%
String strDisabled = (strVoidAccess.equals("Y")) ? "false":"true";
%>
<script>
var lblSystem = '<fmtConstructBuilder:message key="LBL_SYSTEM"/>';
var lblLevel = '<fmtConstructBuilder:message key="LBL_LEVEL"/>';
var lblStatus = '<fmtConstructBuilder:message key="LBL_LEVEL"/>';
var arrGroupInp = new Array(1);
var strVoidAcessFl='<%=strVoidAccess%>';
<%
	ArrayList alGroupList = GmCommonClass.parseNullArrayList((ArrayList) alGroupListDef);
	int intGrpSize = alGroupList.size();
	HashMap hmLoop = null;
	if (intGrpSize > 0)
	{
	   for (int i=0;i<intGrpSize;i++)
	   {
			hmLoop = (HashMap)alGroupList.get(i);
	%>
	arrGroupInp[<%=i%>] = new Array("<%=hmLoop.get("CODEID")%>","<%=hmLoop.get("CODENM")%>","<%=hmLoop.get("PID")%>","<%=hmLoop.get("SETID")%>");
	<% }
}%>

function fnOnPageLoad()
{
	    gridObj = initGrid('dataGridDiv','<%=gridData%>');
		gridObj.enableTooltips("true,false,false,false");
		gridObj.enableEditEvents(true, false, false);
		
	 	gridObj.customGroupFormat = function(grpTypeId, count) {
		    return fnGetGroupTypeName(grpTypeId);
		};
/*
		gridObj.groupBy(5,["#title","#cspan","#cspan","#cspan","#cspan","#cspan"]);
*/
		gridObj.attachEvent("onEditCell",doOnCellEdit);
		gridObj.attachEvent("onCellChanged",doOnCellChange);
		gridObj.attachEvent("onRowSelect" ,function(rowId,cellIndex){
          if (cellIndex==0){
          		gridObj.deleteRow(rowId);
          		fnCaluclateConstructPrice();
			}
		});
		
		gridObj.attachEvent("onMouseOver",function(id,ind)
		{
	    	 if (ind==0)
	    	 {
	              gridObj.cells(id,ind).setAttribute("title","Click here to remove");
	              gridObj.setCellTextStyle(id,ind,"cursor:hand;");
	         }
	         else
	         {
	         	return false;
	         }
	    	return true;
		})
	 	gridObj.collapseAllGroups();
	
		allGroupsHtml = document.getElementById("Div_Group").innerHTML;
		fnCaluclateConstructPrice();
		fnSystemFilterGroups();
}
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad()">
<html:form action="/gmConstructBuilder.do">
<html:hidden property="strOpt" />
<html:hidden property="hTxnId" value=""/>
<html:hidden property="haction" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="SYSCON"/>
<html:hidden property="hinputString" value=""/>
<html:hidden property="hconstructName" value=""/>

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable725" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtConstructBuilder:message key="LBL_SYSTEM_CONSTRUCT_BUILDER"/></td>
			
			<td align="right" class=RightDashBoardHeader > 	
				<fmtConstructBuilder:message key="IMG_ALT_HELP" var = "varHelp"/>				
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
			
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					 <tr>
					 <td style="width:100px"></td>
                     <td height="24" class="RightTableCaption" align="right"><font color="red">*</font> <fmtConstructBuilder:message key="LBL_SYSTEM"/> :</td>
						<td> &nbsp;&nbsp;<gmjsp:dropdown controlName="systemListId" SFFormName="frmConstructBuilder" SFSeletedValue="systemListId"
										SFValue="alSystemList"  codeId = "ID"  codeName = "NAME" onChange="fnFetchConstructs();fnSystemFilterGroups();fnClearSelections()"   defaultValue= "[Choose One]"/> 		
						</td>	
                     </tr>
                     
                      <tr class="Shade">
                      <td style="width:100px"></td>
                     <td height="24" class="RightTableCaption" align="right"><font color="red">*</font> <fmtConstructBuilder:message key="LBL_LEVEL"/> :</td>
						<td> &nbsp;&nbsp;<gmjsp:dropdown controlName="constLevelId" SFFormName="frmConstructBuilder" SFSeletedValue="constLevelId"
										SFValue="alConstLevelList" codeId = "CODEID"  codeName = "CODENM"   defaultValue= "[Choose One]" onChange="fnFillConstructName();" /> 		
						</td>	
                     </tr>
                    <tr>
                	<td style="width:100px"></td>
                     <td height="24" class="RightTableCaption" align="right"><html:checkbox property="newConstructFlag" onclick="fnShowAddConstruct();">&nbsp;<fmtConstructBuilder:message key="LBL_ADD_NEW"/>&nbsp;&nbsp;&nbsp;</html:checkbox><font color="red">*</font> <fmtConstructBuilder:message key="LBL_NAME"/> :</td>
                     <td>
                     <div id="combo_constructName">
                     &nbsp;&nbsp;<gmjsp:dropdown controlName="constructId" SFFormName="frmConstructBuilder" SFSeletedValue="constructId"
										SFValue="alConstructList"  codeId = "CONSTRUCTID"  codeName = "CONSTRUCTNAME" onChange="fnFetchDetails();"   defaultValue= "[Choose One]"/>
                      </div>
                      <div id="add_Construct" style="display:none;padding-left: 8px;">
                        <input style="font-size:11px;height:18px;" type="text" id="conStaticName" size="28"   readonly="readonly">
                        <input class="InputArea" type="text" id="conStaticName2" size="12">
                      </div>
                      	</td>
                    </tr>
                    <tr class="Shade">
                    <td style="width:100px"></td>
                       <td height="24" class="RightTableCaption" align="right"><font   color="red">*</font> <fmtConstructBuilder:message key="LBL_STATUS"/> :</td>
						<td> &nbsp;&nbsp;<gmjsp:dropdown controlName="statusId" SFFormName="frmConstructBuilder" SFSeletedValue="statusId"
										SFValue="alStatusList" codeId = "CODEID"  codeName = "CODENM"   defaultValue= "[Choose One]"/> 		
						</td>	
                    </tr>
                     <tr >
                    <td style="width:100px"></td>
                       <td height="24" class="RightTableCaption" align="right"><fmtConstructBuilder:message key="LBL_PRINCIPAL_CONSTRUCT"/>:</td>
						<td> &nbsp;<html:checkbox property="principalConstruct" />		
						</td>	
                    </tr>
                    
                     
                    
                    <tr><td colspan="4" class="Line"></td></tr>
                    <tr>
                    <td colspan="3">
                    	<table>
	                    	<td>
							<table>
							
							<tr>
							 <td height="24" class="RightTableCaption" align="right">  <fmtConstructBuilder:message key="LBL_SHOW_ALL_GROUPS"/> :</td>
								<td> &nbsp;<input type="checkbox" id="showAllGroups" onclick="fnFilterGroups()">&nbsp; 		
								</td>		 
						 	 </tr>
							
							<tr>
								 <td height="24" class="RightTableCaption" align="right"> <fmtConstructBuilder:message key="LBL_GROUP_TYPE"/> :</td>
						<td> &nbsp;&nbsp;<gmjsp:dropdown controlName="groupTypeId" SFFormName="frmConstructBuilder" SFSeletedValue="groupTypeId" 
										SFValue="alGrpTypeList" codeId = "CODEID"  codeName = "CODENM" onChange="fnFilterGroups()" defaultValue= "[Choose One]"/> 		
						</td>	
							</tr>
											
							</table>
							</td>
							 <td>
							  <DIV class="gmFilter" id="Div_Group"  style="width:350px;height:150px">
							 <table cellspacing="0" cellpadding="0">
								
								 <logic:iterate id="alGroup" name="frmConstructBuilder" property="alGroupList">
								<tr>
									<td><htmlel:multibox property="checkGroup"  value="${alGroup.CODEID}"   />
										 <bean:write name="alGroup" property="CODENM" />
										 &nbsp;<a title='Click to open part details'><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/product_icon.gif' onClick='javascript:fnGroupDetail("<bean:write name="alGroup" property="CODEID" />","<bean:write name="alGroup" property="CODENM" />");'/></a>
										 </td>
								</tr>
								</logic:iterate>
							
								 </table>
								</div>
							</td>
							<%
								String strDisableBtnVal = strEnable;
								if(strDisableBtnVal.equalsIgnoreCase("disabled"))
								{
									strDisableBtnVal = "true";
								}
							%>
                       	 <td align="left" style=" width:120px;"><fmtConstructBuilder:message key="BTN_ADD" var="varAdd"/><gmjsp:button disabled="<%=strDisableBtnVal%>" buttonType="Save" value="&nbsp;${varAdd}&nbsp;" onClick="javascript:fnAddGroup();"  gmClass="button" /></td>
                      </table>
					</tr>				
                   <tr><td colspan="3" class="Line"></td></tr>
		             <tr class="ShadeRightTableCaption">
								<td Height="24" colspan="3">&nbsp;<fmtConstructBuilder:message key="LBL_CONSTRUCT_DETAIL"/></td>
					</tr>
                <table cellpadding="0"  cellspacing="0" width="100%"  border="0" bordercolor="gainsboro"  >   
				<TR class="RightTableCaption" height=24 >
					<TH class="RightText" width="280" align="center"></TH>
					<TH class="RightText" width="110" align="center"><fmtConstructBuilder:message key="LBL_LIST"/></TH>
					<TH class="RightText" width="110" align="center">&nbsp; <fmtConstructBuilder:message key="LBL_TRIPWIRE"/>   </TH>
				
				</TR>	
				<tr><th class="Line" height="1" colspan="11"></th></tr>
                 	<tr height="30">
                 	<td  align="center" width=280></td>
						<td  align="center"><input type="text" size="13" id="idListPrice" readonly="readonly" class="RightInputCaption"></td>
					<td align="center"><input type="text" size="13"   id="idTwLimit" readonly="readonly" class="RightInputCaption"></td>
					
				</tr>
				
				
				</table>
				
			<tr>
            <td colspan="3" valign="top">
            <div id="dataGridDiv" style="height:230px;width:724px;"></div>
			</td>
    	</tr> 
    	<tr><td colspan="2" class="ELine"></td></tr> 
               <tr>
               	<td colspan="2" align="center">
               	<fmtConstructBuilder:message key="BTN_VOID" var="varVoid"/>
               	<gmjsp:button disabled="<%=strDisabled%>"  buttonType="Save"  controlId="idVoid" value="&nbsp;&nbsp;${varVoid}&nbsp;&nbsp;&nbsp;" gmClass="button" onClick="javascript:fnVoid();" />
                 <fmtConstructBuilder:message key="BTN_SUBMIT" var="varSubmit"/>
                 <gmjsp:button disabled="<%=strDisableBtnVal %>" buttonType="Save"  controlId="idSubmit" value="${varSubmit}" gmClass="button" onClick="fnValidateOnSubmit();" /> 
             </td>
               </tr>
  		 	</table>
  			   </td>
  		  </tr>	
    </table>	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>

