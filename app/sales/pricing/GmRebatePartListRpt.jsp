
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtRebatePartReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmRebateManagemnetReport.jsp -->
<fmtRebatePartReport:setLocale value="<%=strLocale%>"/>
<fmtRebatePartReport:setBundle basename="properties.labels.sales.pricing.GmRebatePartListRpt"/>
<bean:define id="partyId" name="frmRebateReport" property="partyId" type="java.lang.String"> </bean:define>
<bean:define id="searchpartyId" name="frmRebateReport" property="searchpartyId" type="java.lang.String"> </bean:define> 
<%
String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");
String strWikiTitle = GmCommonClass.getWikiTitle("REBATE_PART_LIST_REPORT");
String strApplDateFmt = strGCompDateFmt;
%>
<html>
<head>
<title>Rebate Management Report</title>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css"> 
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script> 
<script language="javascript" src="<%=strJsPath%>/GmGrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmRebatePartListRpt.js"></script>	

<script type="text/javascript">
var format = '<%=strApplDateFmt%>';
</script>
</head>

<body leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();" onkeyup= "fnEnter();">
	<html:form action="/gmRebateReport.do?method=loadRebatePartReportsDtls">
	<html:hidden property="strOpt" />
	<html:hidden property="rebateId" />
	<table border="0" class="DtTable950" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" colspan="3" class="RightDashBoardHeader"><fmtRebatePartReport:message
						key="LBL_REBATE_MANAGEMENT_REPORT" /></td>
				<td height="25" class="RightDashBoardHeader"><fmtRebatePartReport:message
						key="IMG_HELP" var="varHelp" /><img align="right" id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr class="Shade">
				<td class="RightTableCaption" align="right" height="25" >
				<fmtRebatePartReport:message key="LBL_ACCOUNT_TYPE" var="varAccountType"/><gmjsp:label
						type="RegularText" SFLblControlName="${varAccountType}:" td="false"/>
				</td>
				<td class="RightText">&nbsp;<gmjsp:dropdown controlName="groupTypeId"
						SFFormName="frmRebateReport" SFSeletedValue="groupTypeId"
						SFValue="alAccountType" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" tabIndex="1" onChange="fnOnChange(this.value);"/></td>
	     <td class="RightTableCaption" align="right" height="25">
			<fmtRebatePartReport:message key="LBL_NAME"  var="varName"/>
               <gmjsp:label type="RegularText" SFLblControlName="${varName}:" td="false"/>
			<td colspan="">
					<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="partyId" />
					<jsp:param name="METHOD_LOAD" value="loadPartyNameList&searchType=PrefixSearch" />
					<jsp:param name="WIDTH" value="300" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value="2"/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%=searchpartyId%>" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="<%=partyId%>" />
					<jsp:param name="SHOW_DATA" value="100" />
					<jsp:param name="AUTO_RELOAD" value="" />
					<jsp:param name="DYNAMIC_OBJ" value="#groupTypeId^" />				
							</jsp:include>
					</td>
						
					
			</tr>
			 <tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
	
				 <tr  height="35">
			 <td class="RightTableCaption"  align="right">&nbsp; <fmtRebatePartReport:message key="LBL_DATE_TYPE"/>:</td>
			<td colspan="3">&nbsp;<gmjsp:dropdown controlName="dateType" SFFormName="frmRebateReport" SFSeletedValue="dateType" defaultValue="[Choose One]"
	                SFValue="alDateType" codeId="CODEID" codeName="CODENM"   tabIndex="3"/>
	     &nbsp;<gmjsp:calendar SFFormName="frmRebateReport"
						controlName="rebateFromDate" gmClass="InputArea"
						onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" tabIndex="4" />
			<gmjsp:calendar SFFormName="frmRebateReport"
						controlName="rebateToDate" gmClass="InputArea"
						onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" tabIndex="5" /></td>
						
						
			</tr>
			 <tr>
				<td class="LLine" colspan=4 height="1"></td>
			</tr>
			<tr class="Shade">
			<td class="RightTableCaption" align="right" height="25"><fmtRebatePartReport:message key="LBL_PARTS" />:</td>
	<td >&nbsp;<html:text size="30" name="frmRebateReport" property="batchPartNum" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="6" />
	<html:select property ="pnumSuffix" name="frmRebateReport"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="7" >
		<html:option value="0" ><fmtRebatePartReport:message key="LBL_CHOOSEONE"/></html:option>
		<html:option value="LIT" ><fmtRebatePartReport:message key="LBL_LITERAL"/></html:option>
		<html:option value="LIKEPRE" ><fmtRebatePartReport:message key="LBL_LIKE_PREFIX"/></html:option>
		<html:option value="LIKESUF" ><fmtRebatePartReport:message key="LBL_LIKE_SUFFIX"/></html:option>
	</html:select>
	</td>
	<td align="center" colspan="2" height="25">
	     <gmjsp:button name="Btn_Load" value="Load" gmClass="button" onClick="fnLoad();" buttonType="Load" tabindex="8" /> 
	 </td>
			</tr>
			<tr>
				<td class="LLine" colspan=4 height="1"></td>
			</tr>
  <tr>
    <td colspan="4">
	<div id="dataGridDiv" height="500px" width="950"></div>	
 <div id="pagingArea" height="500px" width="950"></div>	
    </td>	
  </tr>
 
  <tr> 
	  <td colspan="4" align="center" >
			<div class='exportlinks' id="DivExportExcel"><fmtRebatePartReport:message key="LBL_EXPORT_OPTIONS" />:<img src='img/ico_file_excel.png' />&nbsp;
			  <a href="#"onclick="fnDownloadXLS();"><fmtRebatePartReport:message key="LBL_EXCEL" /></a>
			</div>
	  </td>
  </tr>

	<tr height="30" >
		<td colspan="4" align="center"><div id="DivNothingMessage"><fmtRebatePartReport:message key="LBL_NO_DATA_AVAILABLE" /></div></td>
	</tr>	
		</table>
	</html:form>
</body>
<%@ include file="/common/GmFooter.inc" %>
</html>
