<%
/**********************************************************************************
 * File		 		: GmAccountGroupPriceReport.jsp
 * Desc		 		: fetch Account Group Price
 * Version	 		: 1.0
 * author			: Xun
************************************************************************************/
%>

<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>

<%@ taglib prefix="fmtAccountGroupPriceReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmAccountGroupPriceReport.jsp -->

<fmtAccountGroupPriceReport:setLocale value="<%=strLocale%>"/>
<fmtAccountGroupPriceReport:setBundle basename="properties.labels.sales.pricing.GmAccountGroupPriceReport"/>



<bean:define id="alGroupList" name="frmAccountGroupPriceReport" property="alGroupList" type="java.util.List"></bean:define>
<bean:define id="ldtResult" name="frmAccountGroupPriceReport" property="ldtResult" type="java.util.List"></bean:define> 

<%  
String strWikiTitle = GmCommonClass.getWikiTitle("ACCOUNT_GROUP_PRICE_REPORT");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Pricing Status Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script> 

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
 

 var lblAccountId = '<fmtAccountGroupPriceReport:message key="DT_ACCOUNT_ID"/>';
 var lblSystem = '<fmtAccountGroupPriceReport:message key="LBL_SYSTEM"/>';
 var arrGroupIn = new Array(1);
 var allGroupsHtml;
<%
	ArrayList alGroup = GmCommonClass.parseNullArrayList((ArrayList) alGroupList);
	int intGrpSize = alGroup.size();
	 
	HashMap hmLoop = null;
	if (intGrpSize > 0)
	{
	    for (int i=0;i< intGrpSize;i++)
	    {
		hmLoop = (HashMap) alGroup.get(i);
		%>
		arrGroupIn[<%=i%>] = new Array("<%=hmLoop.get("SETID")%>","<%=hmLoop.get("CODENM")%>", "<%=hmLoop.get("CODEID")%>");
		<% }
	}%>
 
	
	
	function fnFilterGroups(obj)
	{
		  
		var val = obj.value;
		if(val==0)
		{
			document.all.Div_Group.innerHTML = allGroupsHtml;
			return;
		}
		document.all.Div_Group.innerHTML = '';
		var newhtml = '';
		var arrGrpLen = arrGroupIn.length;
			
		newhtml =	"<input type='checkbox' name='selectGrpAll'  bgcolor='gainsboro'  onclick=fnSelectGrpAll('toggle');>   <B>Select All </B><br>"; 
		for (var j=0;j<arrGrpLen;j++ )
		{		
			if (arrGroupIn[j][0] == val)
			{
				newhtml = newhtml + fnCreateChkBox(arrGroupIn[j][2], arrGroupIn[j][1]);
				 
			}
		}
		 	document.all.Div_Group.innerHTML = newhtml; 
		 
	}
	
	
	// To create checkboxes dynamically 
	function fnCreateChkBox(id,nm )
	{
		var html = "<input type='checkbox' name='checkGroups'  value='"+id+"'>"+nm+"<br>";
		return html;
	}
 
function fnLoad()
{	
	 
		fnValidateDropDn('accountID',lblAccountId);
		fnValidateDropDn('setID', lblSystem);
		 
		if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	 
	document.frmAccountGroupPriceReport.haction.value = 'reload';
	  
  	document.frmAccountGroupPriceReport.submit();
	 
}

 
  
function fnPriceHistory(detailID)
{ 
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=1030&txnId="+detailID,"","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
  }


function fnSelectGrpAll(varCmd)
{	 
				objSelAll = document.all.selectGrpAll;
				if (objSelAll ) {
				objCheckSheetArr = document.all.checkGroups; 
					
				if ((objSelAll.checked || varCmd == 'selectGrpAll') && objCheckSheetArr)
				{
					objSelAll.checked = true;
					objCheckSheetArrLen = objCheckSheetArr.length;
					
					objCheckSheetArr1 = document.frmAccountGroupPriceReport.checkGroups;
					if (objCheckSheetArr1.type == 'checkbox')
						{
							objCheckSheetArr1.checked = true;
						}
													
					for(i = 0; i < objCheckSheetArrLen; i ++) 
					{	
						objCheckSheetArr[i].checked = true;
					}
				 
				 
				}
				else if (!objSelAll.checked  && objCheckSheetArr ){
					objCheckSheetArrLen = objCheckSheetArr.length;
					
					objCheckSheetArr1 = document.frmAccountGroupPriceReport.checkGroups;
					if (objCheckSheetArr1.type == 'checkbox')
						{
							objCheckSheetArr1.checked = false;
						}
					
					for(i = 0; i < objCheckSheetArrLen; i ++) 
					{	
						objCheckSheetArr[i].checked = false;
					}
			
					}
				}
			 	 
	}
	
	
	function fnLoadGroups(obj)
	{
	//	 alert(<%=intGrpSize%>);		 
			document.frmAccountGroupPriceReport.haction.value ='onchange';
			document.frmAccountGroupPriceReport.setID.value  = obj.value;
			document.frmAccountGroupPriceReport.submit();
	}
			
function  fnOnPageLoad()
{
	if(document.frmAccountGroupPriceReport.haction.value =='onchange')
	{
		  
		 objCheckSheetArr = document.all.checkGroups; 
				 
		objCheckSheetArrLen = <%=intGrpSize%>;
		 
			 if(objCheckSheetArrLen >0)
			 {	 
			 	document.frmAccountGroupPriceReport.selectGrpAll.checked = true;
			objCheckSheetArr1 = document.frmAccountGroupPriceReport.checkGroups;
					if (objCheckSheetArr1.type == 'checkbox')
						{
							objCheckSheetArr1.checked = true;
						}				
					for(i = 0; i < objCheckSheetArrLen; i ++) 
					{	
						objCheckSheetArr[i].checked = true;
					}
					}
					else {
					document.frmAccountGroupPriceReport.selectGrpAll.checked = false;
					}
	}
	
}


function fnInitPriceReq (setid)
{
	 
	document.frmPricingStatusReport.action="/gmPricingRequestDAction.do?method=loadPriceRequest";
 	document.frmPricingStatusReport.submit();
}	
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10"  onLoad="fnOnPageLoad()" >
<html:form action="/gmAccountGroupPriceReport.do?method=accountGroupPriceReport">
 
<html:hidden property="haction" /> 
<html:hidden property="strOpt" value="40045" /> 

	<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtAccountGroupPriceReport:message key="LBL_ACCOUNT_GROUP"/></td>
			
			<td align="right" class=RightDashBoardHeader > <fmtAccountGroupPriceReport:message key="IMG_ALT_HELP" var = "varHelp"/>
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				 	 	
				 	</td>
			
		</tr>
		<tr><td colspan="4" height="1" class="line"></td></tr>
		<tr>
			<td width="1100" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					 
                    <tr><td colspan="6" class="Line"></td></tr>
                     
                      <tr>
                     <td height="24" class="RightTableCaption" align="right"><font color="red">*</font> <fmtAccountGroupPriceReport:message key="LBL_ACCOUNT"/> :</td>
                     <!-- Custom tag lib code modified for JBOSS migration changes -->
						<td> &nbsp;&nbsp;<gmjsp:dropdown controlName="accountID" SFFormName="frmAccountGroupPriceReport" SFSeletedValue="accountID"
										SFValue="alAccount" codeId = "ID"  codeName = "NM"  defaultValue= "[Choose One]"/> 		
						</td>	
						
					  <td height="24" class="RightTableCaption" align="center"><font color="red">*</font> <fmtAccountGroupPriceReport:message key="LBL_SYSTEM"/> :</td>
					  <!-- Custom tag lib code modified for JBOSS migration changes -->
						<td align="left">&nbsp;<gmjsp:dropdown controlName="setID" SFFormName="frmAccountGroupPriceReport" SFSeletedValue="setID" onChange="javascript:fnLoadGroups(this);"  
										SFValue="alSets" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]"/> 		
						</td>
						<td height="24" class="RightTableCaption" align="right"> <fmtAccountGroupPriceReport:message key="LBL_PART"/> :&nbsp;</td>
						<!-- Struts tag lib code modified for JBOSS migration changes -->
						<td>  <html:text property="pnum"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;	
						</td>	
                     </tr>
                     <tr><td colspan="6" class="LLine"></td></tr> 
                   
                    <tr>
                       <td height="24" class="RightTableCaption" align="right"> <fmtAccountGroupPriceReport:message key="LBL_GROUP"/> :&nbsp;</td>
						 
						<td>  
						  <DIV class="gmFilter"  id="Div_Group"   style="width:320px;height:150px">
						  <table> <tr><td bgcolor="gainsboro" > <html:checkbox property="selectGrpAll" onclick="fnSelectGrpAll('toggle');" /><B><fmtAccountGroupPriceReport:message key="LBL_SELECT_ALL"/> </B> </td>
							</tr></table> 
							 <table cellspacing="0" cellpadding="0">
								
								 <logic:iterate id="SelectedGroups" name="frmAccountGroupPriceReport" property="alGroupList">
								<tr>
									<td><htmlel:multibox property="checkGroups"  value="${SelectedGroups.CODEID}"   />
										 <bean:write name="SelectedGroups" property="CODENM" /></td>
								</tr>
								</logic:iterate>
							
								 </table>
								</div>
						
						
							</td>	
						
					  <td height="24" class="RightTableCaption" align="right"> </td>
						<td> &nbsp;&nbsp;<fmtAccountGroupPriceReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" name ="loadbutton" gmClass="button" buttonType="Load" onClick="fnLoad();"  />&nbsp;&nbsp; 		
						</td>		
						
					 <td height="24" colspan="2" > </td>
                    </tr> 
                 <tr><td colspan="6" class="LLine"></td></tr> 
                  
				<tr>
		<td  colspan="7" width="100%">
		<display:table name="requestScope.frmAccountGroupPriceReport.ldtResult" export="true" class="its" freezeHeader="true" paneSize="400" requestURI="/gmAccountGroupPriceReport.do?method=accountGroupPriceReport" id="currentRowObject"  >
					
					<fmtAccountGroupPriceReport:message key="DT_ACCOUNT_ID" var="varAccountId"/>
					<display:column property="ACCOUNTID1" title="${varAccountId}"  group="1" class="alignleft" sortable="true"/>
					<fmtAccountGroupPriceReport:message key="DT_ACCOUNT_NAME" var="varAccName"/>
					<display:column property="ACCOUNTNM" title="${varAccName}" group="2" class="alignleft" sortable="true"/>
					<fmtAccountGroupPriceReport:message key="LBL_SYSTEM" var="varSystem"/>
					<display:column property="SETNM" title="${varSystem}" class="alignleft" group="3" sortable="true" />
					<fmtAccountGroupPriceReport:message key="DT_GROUP" var="varGroup"/>
					<display:column property="GROUPNAME" title="${varGroup}" group="4" maxlength="25"  class="alignleft" />
					<fmtAccountGroupPriceReport:message key="DT_PART" var="varPart"/>
					<display:column property="PARTNUM" title="${varPart}" class="alignleft" />
					<fmtAccountGroupPriceReport:message key="DT_PART_DESCRIPTION" var="varPartDesc"/>
					<display:column property="PDESC" title="${varPartDesc}" maxlength="25"  class="alignleft"  /> 
					<fmtAccountGroupPriceReport:message key="DT_LIST_PRICE" var="varListPrice"/>
					<display:column property="LIST_PRICE" title="${varListPrice}"  class="alignright" format="{0,number,$#,###,###.00}" />
					<fmtAccountGroupPriceReport:message key="DT_CURRENT_PRICE" var="varCurrentPrice"/>
					<display:column property="CURRENT_PRICE" title="${varCurrentPrice}"   class="alignright"  format="{0,number,$#,###,###.00}" />
					<fmtAccountGroupPriceReport:message key="DT_LIST_PRICE_DATE" var="varListDate"/>
					<display:column property="LIST_CREATEDDATE" title="${varListDate}" class="aligncenter" />
					<fmtAccountGroupPriceReport:message key="DT_CURRENT_PRICE" var="varCurrentPrice"/>
					<display:column property="CURR_CREATEDDATE" title="${varCurrentPrice}" class="aligncenter" />
				 
		</display:table>
		</td>
		</tr> 	
				   
                   
   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>

