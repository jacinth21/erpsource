<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@page import="java.util.Locale"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ page import="com.globus.common.beans.GmCommonClass"%>

<%@ page import ="org.apache.commons.beanutils.PropertyUtils"%>
<%@ page import ="com.globus.common.beans.GmCommonBean"%>
<%@ taglib prefix="fmtPricingDetailsExportToExcel" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>


<bean:define id="last12MonthSales" name="frmPriceApprovalRequest" property="last12MonthSales" type="java.lang.String"></bean:define>
<bean:define id="projected12MonthSales" name="frmPriceApprovalRequest" property="projected12MonthSales" type="java.lang.String"></bean:define>
<bean:define id="priceRequestId" name="frmPriceApprovalRequest" property="priceRequestId" type="java.lang.String"></bean:define>
<bean:define id="alQuestion" name="frmPriceApprovalRequest"	property="alQuestion" type="java.util.ArrayList"></bean:define>
<bean:define id="alAnswer" name="frmPriceApprovalRequest"	property="alAnswer" type="java.util.ArrayList"></bean:define>
<bean:define id="alAnswerGroup" name="frmPriceApprovalRequest"	property="alAnswerGroup" type="java.util.ArrayList"></bean:define>
<bean:define id="alQuestionAnswer" name="frmPriceApprovalRequest"	property="alQuestionAnswer" type="java.util.ArrayList"></bean:define> 

<!-- sales\pricing\GmPricingDetailsExportToExcel.jsp --> 
<%
System.out.println("projected12MonthSales>>>>>" + projected12MonthSales);
Locale locale = null;
String strLocale = "";

String strJSLocale = "";

String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));

if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}

String strCurrencySymbl = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
String strCurrencyFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplCurrFmt"));

String strAction = "";
HashMap hmResult = new HashMap();
HashMap hmResults = new HashMap();
ArrayList alReqDetails = new ArrayList();
hmResult = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturn"));
hmResults = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmResult"));
alReqDetails = GmCommonClass.parseNullArrayList((ArrayList)hmResult.get("PRICEREQDETAILS"));

ArrayList alGpbHeader = GmCommonClass.parseNullArrayList((ArrayList)hmResults.get("ALGPBHEADER"));
ArrayList alPriceDtls = GmCommonClass.parseNullArrayList((ArrayList)hmResults.get("ALPRICEDTLS"));
ArrayList alSystemDtls = GmCommonClass.parseNullArrayList((ArrayList)hmResults.get("ALSYSTEMDTLS"));

int intLength = alSystemDtls.size();
int intLngth = intLength-1;
System.out.println("array list size"+intLength);
int intalPriceDtls = alPriceDtls.size();
int intalSystemDtls = alSystemDtls.size();
int intSize = 0;
String strSetHeadName ="";
String strQty = "";
String strGroupName = "";
String strUnitListPrice = "";
String strUnitTripWire = "";
String strTripWireExetensionPrice = "";
String strUnitCurrentPrice = "";
String strProposedUnitPrice = "";
String strPricipalFlag = "";
String strListPriceOffList = "";
String strName = "";
String strSetName = "";
String strHeaderName ="";
String strStatusName = "";
double totalProposedPrice = 0;
double totalPrpsdPrice= 0;
double totalTripWirePrice = 0;
double totaTripPrice= 0;
double totalCurrentPrice = 0;
double totalCrrtPrice= 0;
double totalListPrice = 0;
double totalLstPrice= 0;

String strGbpPriceDtlId ="";
String strPriceDtlId ="";
String strGpbName = "";
int intLng = alGpbHeader.size();
String strCurrVal = GmCommonClass.parseNull((String)hmResult.get("CURRVAL"));
strAction = GmCommonClass.parseNull((String)hmResult.get("HACTION"));
if (strAction.equals("EXPEXCELOUS"))
{
	strCurrencySymbl = GmCommonClass.parseNull((String)session.getAttribute("strSessRptCurrSymbol"));;
	
}
if((last12MonthSales.equals("") ||last12MonthSales == null))
{
	last12MonthSales = "0";
}
if(projected12MonthSales.equals("") ||projected12MonthSales == null)
{
	projected12MonthSales = "0";
}
double intCurrVal = Double.parseDouble(strCurrVal) ;
double intlast12MonthSales = Double.parseDouble(last12MonthSales)/intCurrVal;
double intprojected12MonthSales = Double.parseDouble(projected12MonthSales)/intCurrVal;
last12MonthSales = Double.toString(intlast12MonthSales);
projected12MonthSales = Double.toString(intprojected12MonthSales);
//GPB Comparision changes

HashMap hmGpbName = new HashMap();             
%>

<fmtPricingDetailsExportToExcel:setLocale value="<%=strLocale%>"/>
<fmtPricingDetailsExportToExcel:setBundle basename="properties.labels.sales.pricing.GmPricingDetailsExportToExcel"/>

<%
//String strCssPath = GmCommonClass.getString("GMSTYLES");
response.setContentType("application/vnd.ms-excel");
response.addHeader("content-disposition", "attachment; filename = Report.xls");
String currentTime = GmCommonClass.getCurrentDate("MM/dd/yyyy HH:mm:ss aaa").toLocaleString(); 
//try{response.getWriter().println(buf);}catch(Exception e){}
%>
<html>
<head>
<TITLE> Globus Medical: Price request details</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>

.RightTableCaption {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
}

.Line{
	background-color: #DCDCDC;
	font-weight:bold;
}

</style>
</head>

<body topmargin="10" leftmargin="10">
<form method="post" action="/gmPriceApprovalRequestAction.do?">
<br>
<h1 align="center"><fmtPricingDetailsExportToExcel:message key="LBL_PRICE_CONFIDENTIAL_MSG"/></h1>
<h4 align="center"><fmtPricingDetailsExportToExcel:message key="LBL_PRICE_CHANGE_REQ"/><%=priceRequestId%></h4>
<table border="1" cellspacing="0" cellpadding="0">
  <tr><td colspan="5" class="RightTableCaption" style = "font-size:11px"><fmtPricingDetailsExportToExcel:message key="LBL_REPORT_DOWNLOAD"/> <%=currentTime%> </td></tr>
  <tr><td colspan="5" class="Line" style = "font-size:11px"><fmtPricingDetailsExportToExcel:message key="LBL_REQUEST_DETAILS"/></td></tr>
   <tr class="Shade">				
  	 <td height="24" class="RightTableCaption"  align="right" width="25%" style = "font-size:11px"> <fmtPricingDetailsExportToExcel:message key="LBL_ACC_NAME"/> :</td>
	 <td style="font-size:10px"> &nbsp;&nbsp;<bean:write name="frmPriceApprovalRequest" property="gpbName"></bean:write></td>
	 <td></td>
 	 <td height="24" class="RightTableCaption" align="right" style = "font-size:11px" width="25%"> <fmtPricingDetailsExportToExcel:message key="LBL_ACC_ID"/> :</td>
	 <td style="font-size:10px">&nbsp;&nbsp;<bean:write name="frmPriceApprovalRequest" property="gpbid"></bean:write></td>
     </tr>
     <tr>
     <td height="24" class="RightTableCaption" align="right" width="25%" style = "font-size:11px"> <fmtPricingDetailsExportToExcel:message key="LBL_STATUS"/> :</td>
	<td width="25%" style="font-size:10px"> &nbsp;&nbsp;<bean:write name="frmPriceApprovalRequest" property="statusName"></bean:write></td>
	<td></td>
	<td height="24" class="RightTableCaption" align="right" width="25%"> </td>
	<td width="25%"> &nbsp;&nbsp;</td>	
     </tr>
    <tr>
     <td height="24" class="RightTableCaption" align="right" width="25%" style = "font-size:11px"> <fmtPricingDetailsExportToExcel:message key="LBL_INITIATED_BY"/> :</td>
	<td width="25%" style="font-size:10px"> &nbsp;&nbsp;<bean:write name="frmPriceApprovalRequest" property="initiatedBy"></bean:write>
	</td>
	<td></td>	
	<td height="24" class="RightTableCaption" align="right" width="25%" style = "font-size:11px"> <fmtPricingDetailsExportToExcel:message key="LBL_INITIATED_DATE"/> :</td>
	<td width="25%" style="font-size:10px"> &nbsp;&nbsp;<bean:write name="frmPriceApprovalRequest" property="initiatedDate"></bean:write></td>	
     </tr>
     
    <tr>
     <td height="24" class="RightTableCaption" align="right" width="25%" style = "font-size:11px"> <fmtPricingDetailsExportToExcel:message key="LBL_GPO_AFFILIATION"/> :</td>
	 <td width="25%" style="font-size:10px"> &nbsp;&nbsp;<bean:write name="frmPriceApprovalRequest" property="strhlcaff"></bean:write></td>
	 <td></td>
	 <td height="24" class="RightTableCaption" align="right" width="25%" style = "font-size:11px"> <fmtPricingDetailsExportToExcel:message key="LBL_GPO_PRICEBOOK"/> :</td>
	 <td width="25%" style="font-size:10px"> &nbsp;&nbsp;<bean:write name="frmPriceApprovalRequest" property="strgpb"></bean:write></td>
	 </tr>
	<tr>
     <td height="24" class="RightTableCaption" align="right" width="25%" style = "font-size:11px"> <fmtPricingDetailsExportToExcel:message key="LBL_12MONTHS_SALES"/> :</td>
	 <td align="left" style="font-size:10px" >&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(last12MonthSales,strCurrencyFmt))%></td>
	 <td></td>
     <td height="24" class="RightTableCaption" align="right" width="25%" style = "font-size:11px"> <fmtPricingDetailsExportToExcel:message key="LBL_PROJECT_12MONTHS_SALES"/> :</td>
     <td align="left" style="font-size:10px" >&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(projected12MonthSales,strCurrencyFmt))%></td>
    </tr>
    <tr>
	<td height="24" class="RightTableCaption" align="right" width="25%" style = "font-size:11px"> <fmtPricingDetailsExportToExcel:message key="LBL_CONTRACT_ACCOUNT"/> :</td>
	<td width="25%" style="font-size:10px"> &nbsp;&nbsp;<bean:write name="frmPriceApprovalRequest" property="contractflname"></bean:write></td>
	<td></td>
    <td height="24" class="RightTableCaption" align="right" width="25%" style = "font-size:11px"> <fmtPricingDetailsExportToExcel:message key="LBL_REBATE"/> :</td>
    <td width="25%"> &nbsp;&nbsp;</td>
    </tr>
    <tr>
    <td height="24" class="RightTableCaption" align="right" width="25%" style = "font-size:11px"> <fmtPricingDetailsExportToExcel:message key="LBL_MATRIX"/> :</td>
    <td width="25%"> &nbsp;&nbsp;</td>
    <td></td>
    <td height="24" class="RightTableCaption" align="right" width="25%"> </td>
	<td width="25%"> &nbsp;&nbsp;</td>
    </tr>
</table>
<br>
 <br>
<table border="1" cellspacing="0" cellpadding="0">
 	<tr><td colspan="5" class="Line" style = "font-size:11px"><fmtPricingDetailsExportToExcel:message key="LBL_ACCOUNT_DETAILS"/></td></tr>
   <tr class="Shade">				
  	 <td height="24" class="RightTableCaption" align="right" width="25%" style = "font-size:11px"> <fmtPricingDetailsExportToExcel:message key="LBL_REGION"/> :</td>
	 <td width="25%" style="font-size:10px"> &nbsp;&nbsp;<bean:write name="frmPriceApprovalRequest" property="strregname"></bean:write></td>
	 <td></td>	
 	 <td height="24" class="RightTableCaption" align="right" width="25%" style = "font-size:11px"> <fmtPricingDetailsExportToExcel:message key="LBL_INITIATE_BY"/> :</td>
	 <td width="25%" style="font-size:10px">&nbsp;&nbsp;<bean:write name="frmPriceApprovalRequest" property="initiatedBy"></bean:write></td>
     </tr>
     <tr>
     <td height="24" class="RightTableCaption" align="right" width="25%" style = "font-size:11px"> <fmtPricingDetailsExportToExcel:message key="LBL_TERRITORY"/> :</td>
	<td width="25%" style="font-size:10px"> &nbsp;&nbsp;<bean:write name="frmPriceApprovalRequest" property="strtername"></bean:write>	
	</td>
	<td></td>
	<td height="24" class="RightTableCaption" align="right" width="25%" style = "font-size:11px"> <fmtPricingDetailsExportToExcel:message key="LBL_DISTRIBUTOR"/> :</td>
	<td width="25%" style="font-size:10px"> &nbsp;&nbsp;<bean:write name="frmPriceApprovalRequest" property="strdname"></bean:write></td>	
     </tr>
     <tr class="Shade">				
     <td height="24" class="RightTableCaption" align="right" style = "font-size:11px"> <fmtPricingDetailsExportToExcel:message key="LBL_AREA_DIRECTOR"/>:</td>
	 <td style="font-size:10px">&nbsp;&nbsp;<bean:write name="frmPriceApprovalRequest" property="stradname"></bean:write></td>
	 <td></td>
	 <td height="24" class="RightTableCaption" align="right" style = "font-size:11px"> <fmtPricingDetailsExportToExcel:message key="LBL_VP_NAME"/>:</td>
	<td style="font-size:10px"> &nbsp;&nbsp;<bean:write name="frmPriceApprovalRequest" property="strvpname"></bean:write></td>
 	</tr> 	
 </table>
 <br>
 <br>
 <table border = "1" cellspacing="0" cellpadding="0">
         <tr class="Line">
           <td height="24" class="RightTableCaption" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_SYSTEM"/></td>
           <td height="24" class="RightTableCaption" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_CONSTRUCT_GROUP_PART"/></td>
           <td height="24" class="RightTableCaption" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_QTY"/></td>
           <td height="24" class="RightTableCaption" colspan = "2" align = "center" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_PROPOSED"/></td>
           <td height="24" class="RightTableCaption"colspan = "3" align = "center" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_TRIP_WIRE"/></td>
           <td height="24" class="RightTableCaption" colspan = "2" align = "center" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_CURRENT"/></td>
           <td height="24" class="RightTableCaption" colspan = "3" align = "center" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_LIST"/></td>
           
           <!-- GPB Comparision changes -->
           <%for (int i = 0;i < intLng ;i++){
             hmGpbName = (HashMap)alGpbHeader.get(i);	  
	  		 strGpbName = GmCommonClass.parseNull((String)hmGpbName.get("GPBNAME"));
	  		 String strColorCode = GmCommonClass.parseNull((String)hmGpbName.get("COLORCODE"));
	  		 String strCheckFlag = GmCommonClass.parseNull((String)hmGpbName.get("CHECKFLAG"));
      		%>
      		<%if(strCheckFlag.equals("Y")){%>
	   		<td height="24" class="RightTableCaption" colspan = "3" align = "center" style = "font-size:10px; background-color:<%=strColorCode%>"> <%=strGpbName%></td>
	   		<%}else {%>
	   		<td height="24" class="RightTableCaption" colspan = "2" align = "center" style = "font-size:10px; background-color:<%=strColorCode%>"> <%=strGpbName%></td>
	   		<%}%>  
    	    <% }%>           
        </tr>
        <tr class="Line">
          <td height="24" class="RightTableCaption" ></td>
          <td height="24" class="RightTableCaption" ></td>
          <td height="24" class="RightTableCaption" ></td>
          <td height="24" class="RightTableCaption" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_PART"/></td>
          <td height="24" class="RightTableCaption" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_CONSTRUCT"/></td>
          <td height="24" class="RightTableCaption" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_PART"/></td>
          <td height="24" class="RightTableCaption" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_CONSTRUCT"/></td>
          <td height="24" class="RightTableCaption" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_CHK"/></td>
          <td height="24" class="RightTableCaption" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_PART"/></td>
          <td height="24" class="RightTableCaption" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_CONSTRUCT"/></td>
          <td height="24" class="RightTableCaption" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_OFF"/></td>
          <td height="24" class="RightTableCaption" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_PART"/></td>
          <td height="24" class="RightTableCaption" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_CONSTRUCT"/></td>
          
          <!-- GPB Comparision changes -->
          <% for (int i = 0;i < intLng ;i++) {
          hmGpbName = (HashMap)alGpbHeader.get(i);
          String strCheckFlag = GmCommonClass.parseNull((String)hmGpbName.get("CHECKFLAG"));
          %>
       		<%if(strCheckFlag.equals("Y")){%>
       		<td height="24" class="RightTableCaption" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_PART"/></td>
       		<td height="24" class="RightTableCaption" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_CONSTRUCT"/></td>
       		<td height="24" class="RightTableCaption" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_CHK"/></td>
       		<%}else {%>
       		<td height="24" class="RightTableCaption" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_PART"/></td>
       		<td height="24" class="RightTableCaption" style = "font-size:10px"> <fmtPricingDetailsExportToExcel:message key="LBL_CONSTRUCT"/></td>
       		<%}%>   
    	  <% }%>            
        </tr>
                 
        <%
       if (intLength > 0)
		{
			HashMap hmTempLoop = new HashMap();
			for (int i = 0;i < intLength ;i++ )
			{
				int intQty;
				int h=0;
				int count = 0;
				double intGpbConstructPrice=0;				
				double intTripWirePrice = 0;
				double  intProposedPrice= 0;
				double intListConstructPrice=0;
				double intProposedConstructPrice=0;
				double intCurrentConstructPrice=0;
				double intTripWireConstructPrice = 0;
				String strGpbConstructPrice ="";
				String strListConstructPrice = "";
				String strProposedConstructPrice = "";
				String strCurrentConstructPrice = "";
				String strApprove = "";
				String strApproveBy = "";
				String strApprovedBy = "";
				String strChkFlag = "";
				String strChkTotalFlag = "";
				String strGpbHeader ="";				
				String strGpbConstruct ="";				
				hmTempLoop = (HashMap)alSystemDtls.get(i);
				double intProposedUnitPrice =0;
				double intListPriceOffList = 0;
				double douUnitListPrice=0;
				double intUnitCurrentPrice = 0;
				strPriceDtlId = GmCommonClass.parseNull((String)hmTempLoop.get("PRCDETLID"));
				strSetHeadName = GmCommonClass.parseNull((String)hmTempLoop.get("SET_NAME"));
				strSetName = GmCommonClass.parseNull((String)hmTempLoop.get("SETNAME"));
				strGroupName = GmCommonClass.parseNull((String)hmTempLoop.get("GROUPNAME"));
				strQty = GmCommonClass.parseNull((String)hmTempLoop.get("QUANTITY"));
				intQty = Integer.parseInt(strQty);
				strProposedUnitPrice = GmCommonClass.parseNull((String)hmTempLoop.get("PROPOSEDUNITPRICE"));				
				if(!strProposedUnitPrice.equals(""))
				{
					intProposedPrice = Double.parseDouble(strProposedUnitPrice);
					intProposedUnitPrice = intProposedPrice/intCurrVal;
					intProposedConstructPrice = intQty*intProposedPrice;
					intProposedConstructPrice = intProposedConstructPrice/intCurrVal;
					totalProposedPrice = totalProposedPrice + intProposedConstructPrice;
					strProposedUnitPrice =Double.toString(intProposedUnitPrice); 
					strProposedConstructPrice = Double.toString(intProposedConstructPrice);
					intProposedPrice = intProposedPrice/intCurrVal;
				}
				strUnitTripWire = GmCommonClass.parseNull((String)hmTempLoop.get("UNITTRIPWIRE"));
				if(!strUnitTripWire.equals(""))
				{
				intTripWirePrice=Double.parseDouble(strUnitTripWire);
				intTripWireConstructPrice = intQty*intTripWirePrice;
				intTripWireConstructPrice = intTripWireConstructPrice/intCurrVal;
				totalTripWirePrice  = totalTripWirePrice  + intTripWireConstructPrice;
				intTripWirePrice = intTripWirePrice/intCurrVal;
				strUnitTripWire = Double.toString(intTripWirePrice);
				}
				strTripWireExetensionPrice = GmCommonClass.parseNull((String)hmTempLoop.get("TRIPWIREEXTENSIONPRICE"));
				if(!strTripWireExetensionPrice.equals(""))
				{
				intListPriceOffList = Double.parseDouble(strTripWireExetensionPrice);
				intListPriceOffList = intListPriceOffList/intCurrVal;
				strTripWireExetensionPrice = Double.toString(intListPriceOffList);
				}
				strUnitCurrentPrice = GmCommonClass.parseNull((String)hmTempLoop.get("UNITCURRENTPRICE"));
				if(!strUnitCurrentPrice.equals(""))
				{
					intUnitCurrentPrice = Double.parseDouble(strUnitCurrentPrice);
					intCurrentConstructPrice = intQty*intUnitCurrentPrice;
					intCurrentConstructPrice = intCurrentConstructPrice/intCurrVal;
					totalCurrentPrice = totalCurrentPrice + intCurrentConstructPrice;
					totalCurrentPrice = totalCurrentPrice/intCurrVal;
					strCurrentConstructPrice = Double.toString(intCurrentConstructPrice);
					intUnitCurrentPrice = intUnitCurrentPrice /intCurrVal;
					strUnitCurrentPrice = Double.toString(intUnitCurrentPrice);
				}
				strListPriceOffList = GmCommonClass.parseNull((String)hmTempLoop.get("LISTPRICEOFFLIST"));
				
				
				
				strUnitListPrice = GmCommonClass.parseNull((String)hmTempLoop.get("UNITLISTPRICE"));
				if(!strUnitListPrice.equals(""))
				{
					douUnitListPrice = Double.parseDouble(strUnitListPrice);
					intListConstructPrice = intQty*douUnitListPrice;
					intListConstructPrice = intListConstructPrice/intCurrVal;
					douUnitListPrice = douUnitListPrice/intCurrVal;
					totalListPrice  = totalListPrice  + intListConstructPrice;
					strListConstructPrice = Double.toString(intListConstructPrice);
					strUnitListPrice = Double.toString(douUnitListPrice);
				}
				
				strStatusName = GmCommonClass.parseNull((String)hmTempLoop.get("STATUSNAME"));
				strApprovedBy = GmCommonClass.parseNull((String)hmTempLoop.get("APPROVEDBY"));
				if(!strProposedUnitPrice.equals(""))
				{
				if(intProposedPrice<intTripWirePrice)
				{
					strChkFlag = "NO";
				}
				if(intProposedPrice>intTripWirePrice)
				{
					strChkFlag = "YES";
				}
				}			
				
				if(strStatusName.equals("Approved"))
				{
					strApprove = strStatusName;
					
					GmCommonBean gmCommonBean = new GmCommonBean();
					strApproveBy = gmCommonBean.getUserName(strApprovedBy);
				}
				
			    if(i==0) 
				{
			    	strHeaderName = strSetHeadName;
				%>
					<tr>
					<td style="font-size:10px"><b><%=strSetHeadName%></b></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					 <%for (int k = 0;k < intLng ;k++){ 
					   hmGpbName = (HashMap)alGpbHeader.get(k);
				          String strCheckFlag = GmCommonClass.parseNull((String)hmGpbName.get("CHECKFLAG"));
				          %>
	   					
	   					<%if(strCheckFlag.equals("Y")){%>
       						<td></td><td></td><td></td>
       					<%}else{%>
       						<td></td><td></td>
       					<%}%>
    				<% }%>   
					</tr>
					
				<%}
				%>
				<%if(i==0)
				{ 
				strName=strSetName;
				
				}%>
				
				<%if(!strName.equals(strSetName))
				{ 
					
					
					totalPrpsdPrice = totalProposedPrice- intProposedConstructPrice;
					totalCrrtPrice = totalCurrentPrice-intCurrentConstructPrice;
					totalLstPrice = totalListPrice- intListConstructPrice;
					totaTripPrice= totalTripWirePrice - intTripWireConstructPrice;
					if(totalPrpsdPrice<totaTripPrice)
					{
						strChkTotalFlag = "";
					}
				%> 
				<tr>
					<td></td><td style="font-size:10px"><b><%=strName%></b></td><td></td><td></td><td style="font-size:10px" align=right><b>&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(Double.toString(totalPrpsdPrice),strCurrencyFmt))%></b></td><td></td>
					<td style="font-size:10px" align=right><b>&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(Double.toString(totaTripPrice),strCurrencyFmt))%></b></td>
					
					
					<td style = "color:white;font-size:10px" align=center><b><%=strChkTotalFlag%></b></td>
				
					
					<td></td><td style="font-size:10px" align=right><b>&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(Double.toString(totalCrrtPrice),strCurrencyFmt))%></b></td><td></td><td></td>
					<td style="font-size:10px" align=right><b>&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(Double.toString(totalLstPrice),strCurrencyFmt))%></b></td>
					<%for (int k = 0;k < intLng ;k++){ 
					   hmGpbName = (HashMap)alGpbHeader.get(k);
				          String strCheckFlag = GmCommonClass.parseNull((String)hmGpbName.get("CHECKFLAG"));
				     %>
	   					<%if(strCheckFlag.equals("Y")){%>
       						<td></td><td></td><td></td>
       					<%}else{%>
       						<td></td><td></td>
       					<%}%>
    				<% }%>  
					</tr>
					<%if(!strHeaderName.equals(strSetHeadName))
				    {
						
			    	strHeaderName = strSetHeadName;
				    %>
					<tr>
					<td style="font-size:10px"><b><%=strSetHeadName%></b></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					<%for (int k = 0;k < intLng ;k++){ 
					   hmGpbName = (HashMap)alGpbHeader.get(k);
				          String strCheckFlag = GmCommonClass.parseNull((String)hmGpbName.get("CHECKFLAG"));
				     %>
	   					<%if(strCheckFlag.equals("Y")){%>
       						<td></td><td></td><td></td>
       					<%}else{%>
       						<td></td><td></td>
       					<%}%>
    				<% }%>  
					</tr>
				    <%}%>
					
					
				   <%totalProposedPrice = intProposedConstructPrice;
				     totalCurrentPrice = intCurrentConstructPrice;
				     totalListPrice = intListConstructPrice;
				     totalTripWirePrice = intTripWireConstructPrice;
				     strName=strSetName;
				     
				   %>
				   
					
				<%}%>
					<tr>
					<td style="font-size:10px"><b><%=strSetHeadName%></b></td><td style="font-size:10px"><%=strGroupName%></td><td align = "center" style="font-size:10px"><%=strQty%></td>					
					<%if(!strProposedUnitPrice.equals("")){%>
					<td style="font-size:10px" align=right><b>&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strProposedUnitPrice,strCurrencyFmt))%></b></td>
					<%}else{%>
					<td style="font-size:10px" align=right><%=strProposedUnitPrice%></td>
					<% }%>
					<%if(!strProposedConstructPrice.equals("")){%>
					<td style="font-size:10px" align=right><b>&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strProposedConstructPrice,strCurrencyFmt))%></b></td>
					<%}else{%>
					<td style="font-size:10px" align=right><%=strProposedConstructPrice%></td>
					<% }%>
					<%if(!strUnitTripWire.equals("")){%>
					<td style="font-size:10px" align=right>&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strUnitTripWire,strCurrencyFmt))%></td>
					<%}else{%>
					<td style="font-size:10px" align=right><%=strUnitTripWire%></td>
					<% }%>
					<%if(!strTripWireExetensionPrice.equals("")){%>
					<td style="font-size:10px" align=right>&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strTripWireExetensionPrice,strCurrencyFmt))%></td>
					<%}else{%>
					<td style="font-size:10px" align=right><%=strTripWireExetensionPrice%></td>
					<% }%>
					
					
		 <%if(strChkFlag.equals("NO"))
			 {%>
				    <td style = "color:red;font-size:10px" align=center><%=strChkFlag%></td>
			<%}else{%>
					<td style = "color:green;font-size:10px" align=center><%=strChkFlag%></td>
				<% }%>
				<%if(!strUnitCurrentPrice.equals("")){%>
					<td style="font-size:10px" align=right>&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strUnitCurrentPrice,strCurrencyFmt))%></td>
					<%}else{%>
					<td style="font-size:10px" align=right><%=strUnitCurrentPrice%></td>
					<% }%>
					<%if(!strCurrentConstructPrice.equals("")){%>
					<td style="font-size:10px" align=right>&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strCurrentConstructPrice,strCurrencyFmt))%></td>
					<%}else{%>
					<td style="font-size:10px" align=right><%=strCurrentConstructPrice%></td>
					<% }%>
					<td style="font-size:10px" align=right><%=strListPriceOffList%></td>
					<%if(!strUnitListPrice.equals("")){%>
					<td style="font-size:10px" align=right>&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strUnitListPrice,strCurrencyFmt))%></td>
					<%}else{%>
					<td style="font-size:10px" align=right><%=strUnitListPrice%></td>
					<% }%>
					<%if(!strListConstructPrice.equals("")){%>
					<td style="font-size:10px" align=right>&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strListConstructPrice,strCurrencyFmt))%></td>
					<%}else{%>
					<td style="font-size:10px" align=right><%=strListConstructPrice%></td>
					<% }%>
				<!-- for GPB Comparision -->
				<% 
				
				for (int j = 0; j < intalPriceDtls; j++ ){
				  HashMap hmAlPriceDtls = new HashMap();
				  hmAlPriceDtls = (HashMap)alPriceDtls.get(j);
				  strGbpPriceDtlId = GmCommonClass.parseNull((String)hmAlPriceDtls.get("REQDTLID"));
				  String strGbpDtlId = GmCommonClass.parseNull((String)hmAlPriceDtls.get("GPBID"));
				  String strGbpUnitPrice = GmCommonClass.parseNull((String)hmAlPriceDtls.get("UNITPRICE"));
				  String strColorCd = GmCommonClass.parseNull((String)hmAlPriceDtls.get("COLORCD"));
				  String strCheckFl = GmCommonClass.parseNull((String)hmAlPriceDtls.get("CHECKFL"));
				  String strChekFlag = "";
				 
				  if(strPriceDtlId.equals(strGbpPriceDtlId)){
				    count++;
				   while (h < intLng){
				     
			             hmGpbName = (HashMap)alGpbHeader.get(h);	  
				  		String strGpbId = GmCommonClass.parseNull((String)hmGpbName.get("GPBID"));
				  		
				  		 if(!strGpbId.equals(strGbpDtlId))
				  		 {%>
       						<td></td><td></td>
	       					<%  h++;	
				  		 }
				  		 else
				  		 {
				      strGpbHeader = strGbpUnitPrice;
				      if(!strGbpUnitPrice.equals("")){
				      intGpbConstructPrice = intQty * Double.parseDouble(strGbpUnitPrice);
				      strGpbConstructPrice = Double.toString(intGpbConstructPrice);
				      //Checking YES/NO based on proposed price
				      if(intProposedPrice != 0){
				      if(intProposedPrice<intGpbConstructPrice)
						{
							strChekFlag = "NO";
						}
						if(intProposedPrice>intGpbConstructPrice)
						{
							strChekFlag = "YES";
						}		
				      }
				  }
				        h++;    
				        
				  %>
				  <!-- Add CHK column based on checkflag -->			  
				  <%if(strCheckFl.equals("Y")){%>
				  <%if(!strGpbHeader.equals("")){%>
					<td style="font-size:10px;background-color:<%=strColorCd%>" align=right>&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strGpbHeader,strCurrencyFmt))%></td>
					<%}else{%>
					<td style="font-size:10px;background-color:<%=strColorCd%>" align=right><%=strGpbHeader%></td>
					<% }%>
					<%if(!strGpbConstructPrice.equals("")){%>
					<td style="font-size:10px;background-color:<%=strColorCd%>" align=right>&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strGpbConstructPrice,strCurrencyFmt))%></td>
					<%}else{%>
					<td style="font-size:10px;background-color:<%=strColorCd%>" align=right><%=strGpbConstructPrice%></td>
					<% }%>
				    <%if(strChekFlag.equals("NO")){%>
				    <td style="font-size:10px;color:red" align=center><%=strChekFlag%></td>
				    <%}else{%>
				    <td style="font-size:10px;color:green" align=center><%=strChekFlag%></td>
				   <%}%>
       			  <%}else{%>
       				<%if(!strGpbHeader.equals("")){%>
					<td style="font-size:10px;background-color:<%=strColorCd%>" align=right>&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strGpbHeader,strCurrencyFmt))%></td>
					<%}else{%>
					<td style="font-size:10px;background-color:<%=strColorCd%>" align=right><%=strGpbHeader%></td>
					<% }%>
					<%if(!strGpbConstructPrice.equals("")){%>
					<td style="font-size:10px;background-color:<%=strColorCd%>" align=right>&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strGpbConstructPrice,strCurrencyFmt))%></td>
					<%}else{%>
					<td style="font-size:10px;background-color:<%=strColorCd%>" align=right><%=strGpbConstructPrice%></td>
					<% }%>
       			<%}
       			
       			           break;
			           }
				  		
		          }
			  }
	   }%>
			   <!-- For Empty GPB results -->
			   <%for (int k = 0;k < intLng ;k++){ 
					   hmGpbName = (HashMap)alGpbHeader.get(k);
				          String strCheckFlag = GmCommonClass.parseNull((String)hmGpbName.get("CHECKFLAG"));
				     if(count==0){%>
	   					<%if(strCheckFlag.equals("Y")){%>
       						<td></td><td></td><td></td>
       					<%}else{%>
       						<td></td><td></td>
       					<%}%>
       					<%}%>
    				<% }%> 
				 </tr>
			 <%
			 if(intLngth==i)
			 {
				 System.out.println("intLength>>>>"+intLngth);
				 
				 if(totalProposedPrice<totalTripWirePrice)
					{
						strChkTotalFlag = "";
					}
			 %>
			 
				 <tr>
				 <td></td><td style="font-size:10px"><b><%=strName%></b></td><td></td><td></td><td style="font-size:10px" align=right><b>&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(Double.toString(totalProposedPrice),strCurrencyFmt))%></b></td><td></td>
				 <td style="font-size:10px" align=right><b>&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(Double.toString(totalTripWirePrice),strCurrencyFmt))%></b></td>
				 
					<td style = "color:white;font-size:8px;font-size:10px" align=center><b><%=strChkTotalFlag%></b></td>
				
				<td></td><td style="font-size:10px" align=right><b>&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(Double.toString(totalCurrentPrice),strCurrencyFmt))%></b></td><td></td><td></td>
				<td style="font-size:10px" align=right><b>&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(Double.toString(totalListPrice),strCurrencyFmt))%></b></td>
				<%for (int k = 0;k < intLng ;k++){ %>
	   					<td></td><td></td>
    				<% }%>
				 </tr>
			<%}
			}
			}
         else	
         {%>
         
        	<tr><td colspan="10" class="Line"></td></tr>
        	<tr><td colspan="10" class="RightTextRed" height="50" align=center><fmtPricingDetailsExportToExcel:message key="LBL_NO_DATA_AVAILABLE"/></td></tr>
    <%}%>
         </table>
         
 <!-- Business Questions -->
 <br>
 <br>
 <br>
  <table border="1" cellspacing="0" cellpadding="0">  
  <tr><td colspan="12" class="Line" style = "font-size:11px"><fmtPricingDetailsExportToExcel:message key="LBL_BUSINESS_QUESTIONS"/></td></tr>
     
  <%   
     int intQuesSize = 0;
     int intAnsSize = 0;
       String strQuestionNM ="";
       String strAns = "";
       String strQustnNm = "";
       String strAnswer = "";
       String strListId = "";
       String strLstId = "";
       intQuesSize = alQuestion.size();
       intAnsSize = alQuestionAnswer.size();
       HashMap hmQuestion = new HashMap();
       HashMap hmAnswer = new HashMap();
       HashMap hmAnsGroup = new HashMap();
       
       if (intQuesSize > 0)
       { %>  
      <%for (int i=0;i<intQuesSize;i++)
             {
                    hmQuestion = (HashMap)alQuestion.get(i);
                    strQuestionNM = (String)hmQuestion.get("QUESTIONNAME");            
                    strQustnNm = i+1+") "+strQuestionNM;
                    if(intAnsSize!=0){
                           hmAnswer = (HashMap)alQuestionAnswer.get(i);
                           hmAnsGroup = (HashMap)alQuestionAnswer.get(0);
                           strAns = GmCommonClass.parseNull((String)hmAnswer.get("ANSWERDESC"));
                           strListId = GmCommonClass.parseNull((String)hmAnswer.get("LISTID"));
                           strAnswer = "A. "+strAns;
                           
                        }
                    else
                    {
                    strAnswer = "A.";
                    }
                    if(strListId.equals(""))
                    {
                           strLstId = "A."+strAns;
                    }
                    else
                    {
                           strLstId = "A."+GmCommonClass.getCodeNameFromCodeId(strListId)+"  "+strAns;
                    }
                    
                    %>     
                    <tr><td style="font-size:10px" colspan="12"><b><%=strQustnNm%></b></td></tr>
                    <%if(i==0)
                    {%>
                    <tr><td style="font-size:10px" colspan="12"><%=strLstId%></td></tr>
                           
                           <%}else{%>
                                 <tr><td style="font-size:10px" colspan="12"><%=strAnswer%></td></tr>
                                 <%}%>
                    
                    
                    <%}
       }
                    
%>     
     
 </table>
 
 <h1 align="center"><fmtPricingDetailsExportToExcel:message key="LBL_PRICE_CONFIDENTIAL_MSG"/></h1>
     
</form>
</body>
<html>