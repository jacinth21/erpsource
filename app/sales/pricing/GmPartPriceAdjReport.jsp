<%
	/***************************************************************************************
	 * File		 		: GmPartPriceAdjReport.jsp
	 * Desc		 		: This screen is used to providew the Part Price Adjustment Report
	 * Version	 		: 1.0
	 * author			: Harinadh Reddi
	 ****************************************************************************************/
%>
	<%@ include file="/common/GmHeader.inc" %>
	<%@ page language="java" %>
	
	<%@ page import ="com.globus.common.beans.GmCommonControls"%>	
	<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
	<%@ page import ="org.apache.commons.beanutils.PropertyUtils"%>	
	<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
	<%@ taglib prefix="fmtPartPriceAdjReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
    <%@ page isELIgnored="false" %>
    <%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
    
    <!-- sales\pricing\GmPartPriceAdjReport.jsp -->

<fmtPartPriceAdjReport:setLocale value="<%=strLocale%>"/>
<fmtPartPriceAdjReport:setBundle basename="properties.labels.sales.pricing.GmPartPriceAdjReport"/>

 	<bean:define id="partNum" name="frmPartPriceAdjRpt" property="partNum" type="java.lang.String"></bean:define>
 	<bean:define id="alProjectList" name="frmPartPriceAdjRpt" property="alProjectList" type="java.util.ArrayList"></bean:define>
 	<bean:define id="alSystemList" name="frmPartPriceAdjRpt" property="alSystemList" type="java.util.ArrayList"></bean:define> 
 	<bean:define id="ldtResult" name="frmPartPriceAdjRpt" property="ldtResult" type="java.util.List"></bean:define>
 	<bean:define id="projectList" name="frmPartPriceAdjRpt" property="projectList" type="java.lang.String"></bean:define>
 	<bean:define id="systemList" name="frmPartPriceAdjRpt" property="systemList" type="java.lang.String"></bean:define>
 	<bean:define id="accName" name="frmPartPriceAdjRpt" property="accName" type="java.lang.String"></bean:define>
 	<bean:define id="queryByType" name="frmPartPriceAdjRpt" property="queryByType" type="java.lang.String"></bean:define>
 	<bean:define id="xmlGridData" name="frmPartPriceAdjRpt" property="xmlGridData" type="java.lang.String"> </bean:define>
 	
<HTML>
<HEAD>	
	<% 
	String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");
		String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
		HashMap hcboVal = null;
		HashMap hmQueryValues = new HashMap();
		hmQueryValues.put("ID","ID");
		hmQueryValues.put("PID","ID");
		hmQueryValues.put("NM","NAME");	
		int rowsize = ldtResult.size();
		GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.sales.pricing.GmPartPriceAdjReport", strSessCompanyLocale);
		String strQueryType = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_SYSTEM_NAME"));
		if(queryByType.equals("120002")){
			
			 strQueryType = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PRO_NAME")); 
		}
		
		
	%>
	<TITLE> Globus Medical: Loaner Extension Request</TITLE>
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">	
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
	<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
	<link rel="STYLESHEET" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
	
	<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
    <script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
	<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
	<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmPartPriceAdjReport.js"></script>
	<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
	<script>
		var objGridData;
		objGridData = '<%=xmlGridData%>';
		var gridObj ='';
		var projectString = '<%=projectList%>';
		var systemString = '<%=systemList%>';
	</script>	
</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
	<html:form action="/gmPartPriceAdjustmentRptAction.do?" >
	<html:hidden property="strOpt"/>
	<input type="hidden" name="sysProjectString">

	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" bordercolor="red" class="DtTable1100"  cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="6" width="25%" class="RightDashBoardHeader">&nbsp;<fmtPartPriceAdjReport:message key="LBL_PART_PRICE_ADJ_REPORT"/></td>
			<td align="right" class=RightDashBoardHeader>
				<fmtPartPriceAdjReport:message key="IMG_ALT_HELP" var = "varHelp"/>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("PART_PRICE_ADJ_REPORT")%>');" />
			</td>
		</tr>		
		<tr>
			<td colspan="8" >
           		<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr height="30">
						<td height="24" width="8%" class="RightTableCaption" align="right"><fmtPartPriceAdjReport:message key="LBL_TYPE" var="varType"/><gmjsp:label type="MandatoryText" SFLblControlName="${varType}" td="false" /> </td>
						<td width="18%">&nbsp;<gmjsp:dropdown controlName="type" SFFormName="frmPartPriceAdjRpt" SFSeletedValue="type" SFValue="alType" codeId="CODEID" codeName="CODENM" onChange="javascript:fnChangeDisplay(this);"/></td>						
						<td class=RightTableCaption align="right" height="24" width="10%"> <div id="header"></div> </td>
						<td id="acc_name" style="display: none" width="35%">&nbsp;<gmjsp:dropdown controlName="accName" SFFormName="frmPartPriceAdjRpt" SFSeletedValue="accName" SFValue="alAccName" codeId="ID" codeName="NAME" onChange="javascript:fnLoadAcctType(this);"
											defaultValue= "[Choose One]" /></td>
					
						<td id="grp_acc_name" style="display: none" width="35%">&nbsp;<gmjsp:dropdown controlName="grpAccName" SFFormName="frmPartPriceAdjRpt" SFSeletedValue="grpAccName" SFValue="alGrpAccName" codeId="ID" codeName="NAME" onChange="javascript:fnLoadAcctType(this);"
											defaultValue= "[Choose One]" /></td>	
						
							<td height="24" class="RightTableCaption" align="right" width="5%"><fmtPartPriceAdjReport:message key="LBL_ID" var="varId"/><gmjsp:label type="RegularText" SFLblControlName="${varId}" td="false" /></td>
							<td width="15%">&nbsp;<html:text property="accId" styleClass="InputArea" name="frmPartPriceAdjRpt" size="8" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');fnLoadAccDetails(this)" />
							</td>					
					</tr>					
		<tr height="25" id="headerDetails" style="display: none">
			<td colspan="8">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr><td  colspan="8" class="LLine" height="1"></td></tr>
					<tr height="25" class="shade">
						<td class="RightTableCaption" align="right" width="8%"><fmtPartPriceAdjReport:message key="LBL_REGION"/>:</td>
						<td width="19%" class="RightText" id="Lbl_Regn" ></td>
						<td class="RightTableCaption" align="right" width="10%"><fmtPartPriceAdjReport:message key="LBL_AREA_DIRECTOR"/>:</td>
						<td width="23%" class="RightText" id="Lbl_AdName"></td>
						<td class="RightTableCaption" align="right" width="15%"><fmtPartPriceAdjReport:message key="LBL_DISTRIBUTOR"/>:</td>
						<td width="20%"  class="RightText" id="Lbl_DName"></td>
						<td class="RightTableCaption" align="right" width="15%"></td>
					</tr>
					<tr><td  colspan="8" class="LLine" height="1"></td></tr>
					<tr height="25">
						<td class="RightTableCaption" align="right" width="8%"><fmtPartPriceAdjReport:message key="LBL_TERITORY"/>:</td>
						<td class="RightText" width="19%" id="Lbl_TName"></td>
						<td class="RightTableCaption" align="right" width="10%"><fmtPartPriceAdjReport:message key="LBL_REP"/>:</td>
						<td class="RightText" width="23%" id="Lbl_RName"></td>
						<td class="RightTableCaption" align="right" width="15%"><fmtPartPriceAdjReport:message key="LBL_GRP_PRICE_BOOK"/>:</td>
						<td class="RightText" width="20%" id="Lbl_PName"></td>
						<td class="RightTableCaption" align="right" width="15%"></td>
					</tr>
					<tr><td  colspan="8" class="LLine" height="1"></td></tr>
					<tr height="25" class="shade">
						<td class="RightTableCaption" align="right" width="8%"><fmtPartPriceAdjReport:message key="LBL_IDN"/>:</td>
						<td class="RightText" width="19%" id="Lbl_GPRAffName"></td>
						<td class="RightTableCaption" align="right" width="10%"><fmtPartPriceAdjReport:message key="LBL_GPO"/>:</td>
						<td class="RightText" width="23%" id="Lbl_HlthCrAffName"></td>
						<td class="RightTableCaption" align="right" width="15%"></td>
						<td class="RightText"></td>
						<td class="RightTableCaption" align="right" width="15%"></td>
					</tr>
				</table>
			</td>
		</tr>	
				</table> 
       		</td>   
		</tr>		
		<tr><td colspan="8" class="LLine" height="1"></td></tr>
		<tr></tr>
		<tr>
			<td height="100"  class="RightTableCaption" width="37%" align="right"><fmtPartPriceAdjReport:message key="LBL_QUERY_BY"/>:</td>
			<td colspan="6">
				<table border="0" bordercolor="red" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td width="20%">&nbsp;<gmjsp:dropdown controlName="queryByType" SFFormName="frmPartPriceAdjRpt" SFSeletedValue="queryByType" SFValue="alQueryTypeList" codeId="CODEID" codeName="CODENM"  onChange="fnCallType();" /></td>
						<td id="systemList" style="display: block;">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">								
								<tr>
									<td width="60%"><input type="checkbox" name="selectSystemQueryList" onclick="fnSelectAllSystemLists(this,'Chk_GrpQuerySystemList','toggle');" >&nbsp;<B><fmtPartPriceAdjReport:message key="LBL_SELECT_ALL"/> </B></td>
							    </tr>
							   	<tr>
									<td align="left" width="60%"><%=GmCommonControls.getChkBoxGroup("fnSelectSystemName()",alSystemList,"QuerySystemList",hmQueryValues,"")%></td>
									<td align="center"><fmtPartPriceAdjReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnLoadPartPriceData();" buttonType="Load" /></td>
									<td width="11%"></td>
							   	</tr>	
							</table>	
						</td>
						<td id="projectlist" style="display: none;">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">					
								<tr>
									<td width="60%"><input type="checkbox" name="selectProjectQueryList" onclick="fnSelectAllProjectLists(this,'Chk_GrpQueryProjectList','toggle');" >&nbsp;<B><fmtPartPriceAdjReport:message key="LBL_SELECT_ALL"/> </B></td>
							    </tr>
							   	<tr>
									<td align="left" width="60%"><%=GmCommonControls.getChkBoxGroup("fnSelectProjectName()",alProjectList,"QueryProjectList",hmQueryValues,"")%></td>
									<td align="center"><fmtPartPriceAdjReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnLoadPartPriceData();" buttonType="Load" /></td>
									<td width="11%"></td>
							   	</tr>
							   	<tr></tr>
							</table>	
						</td>
					</tr>
					</table>
			</td>				
		</tr>
		<tr><td colspan="8" class="LLine" height="1"></td></tr>
			<tr class="shade" height="25">
			<td class="RightTableCaption" width="25%" align = "right"><fmtPartPriceAdjReport:message key="LBL_PART_NUMBER"/>:</td>
			<td colspan="6">&nbsp;<html:text property="partNum" styleClass="InputArea" name="frmPartPriceAdjRpt" size="28" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/>&nbsp;<gmjsp:dropdown controlName="partLike" SFFormName="frmPartPriceAdjRpt" SFSeletedValue="partLike" SFValue="alLikeOptions" codeId="CODEID" codeName="CODENM" defaultValue= "[Choose One]" /></td>
     	</tr>     	 	
	</table>
	<table border="0" class="DtTable1100"  cellspacing="0" cellpadding="0">
	<%if(!xmlGridData.equals("")){%>
			<tr>
				<td colspan="7">					
						<div  id="PartPriceAdjReport" height="450px" width="1090px"></div>
						<div id="pagingArea"  width="1095px"></div>
				</td>
				<td></td>
			</tr>
			<tr><td height="1" colspan="8" class="LLine"></td></tr>			
		<%}else{%>
			<tr>
				<td colspan="8" align="center" class="RightText"><fmtPartPriceAdjReport:message key="MSG_NO_DATA"/>
				</td>
			</tr>
		<%}%>
		<tr><td height="1" colspan="8" class="LLine"></td></tr>
		<%if( xmlGridData.indexOf("cell") != -1) {%>
		<tr>
			<td colspan="8" align="center">
			    <div class='exportlinks'><fmtPartPriceAdjReport:message key="DIV_EXPORT_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnDownloadXLS();"> <fmtPartPriceAdjReport:message key="DIV_EXCEL"/> </a></div>
			</td>
		</tr>
		<%} %>    
	</table>
</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>