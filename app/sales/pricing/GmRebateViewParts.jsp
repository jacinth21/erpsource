
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!-- \sales\pricing\GmRebateViewParts.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtrebateMgnt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtrebateMgnt:setBundle
	basename="properties.labels.sales.pricing.GmRebateViewParts" />
<bean:define id="gridData" name="frmRebateReport" property="gridData" type="java.lang.String"> </bean:define>


<%
String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");
String strWikiTitle = GmCommonClass.getWikiTitle("VIEW_PARTS_REBATE");	
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"
	href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmRebateViewParts.js"></script>	
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script>
objGridData = '<%=gridData%>';
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
	<html:form
		action="/gmRebateReport.do?method=viewRebateparts">
	
		<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" width="150" class="RightDashBoardHeader"><fmtrebateMgnt:message
						key="VIEW_PARTS_REBATE" /></td>
				<td height="25" class="RightDashBoardHeader"><fmtrebateMgnt:message
						key="IMG_HELP" var="varHelp" /><img align="right" id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr class="Shade">
				<td class="RightTableCaption" align="Right" height="25" width="100">&nbsp;<fmtrebateMgnt:message
						key="LBL_ACCOUNT_TYPE" />:
				</td>
				<td colspan="2">&nbsp;&nbsp;<bean:write property="groupTypeName"
						name="frmRebateReport" /></td>
			</tr>
				<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr>
				<td class="RightTableCaption" align="right" height="25" >&nbsp;<fmtrebateMgnt:message
						key="LBL_NAME" />:
				</td>
				<td>&nbsp;&nbsp;<bean:write property="partyName"
						name="frmRebateReport" /></td>
			</tr>
				<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr class="Shade">
				<td class="RightTableCaption" align="right" height="25">&nbsp;<fmtrebateMgnt:message
						key="LBL_EFFECTIVE_DATE" />:
				</td>
			 <td>&nbsp;&nbsp;<bean:write property="effectiveDate" name="frmRebateReport" />-
			 	 <bean:write property="contractToDate" name="frmRebateReport" /></td>
			</tr>
				<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<%
					if (gridData.indexOf("cell") != -1) {
			%>
			<tr>
				<td colspan="4">
					<div id="dataGridDiv" class="" height="400px"></div>
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr>
			<td colspan="4" align="center">
			    <div class='exportlinks'><fmtrebateMgnt:message key="DIV_EXPORT_OPT"/> : <img
					src='img/ico_file_excel.png' />&nbsp;<a href="#"
					onclick="fnExport();"><fmtrebateMgnt:message key="DIV_EXCEL"/> 
				</a></div>
			</td>
		</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
		<%
					}else{
		%>
		<tr>
			<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr>
			<td colspan="4" align="center" height="25" class="RegularText"><fmtrebateMgnt:message key="NO_DATA_FOUND"/></td>
			</tr>
			<%} %>
			<tr>
				 <td colspan="4" height="25" align="center"><gmjsp:button value="Close"
							name="BTN_CLOSE" gmClass="button" buttonType="Load"
							onClick="fnClose();" />&nbsp;</td>
			</tr>
			<tr>
				<td colspan="4" class="LLine" height="1"></td>
			</tr>	
		</table>
	</html:form>
</body>
<%@ include file="/common/GmFooter.inc" %>
</html>