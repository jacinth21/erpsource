 <%
/*******************************************************************************************************
 * File		 		: GmPartPriceAdjDtls.jsp
 * Desc		 		: This JSP is used to inclued the grid portion of Part Price Adjustment Initiate screen
 * Version	 		: 1.0
 * author			: arajan
*********************************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.HashMap" %>

<%@ taglib prefix="fmtPartPriceAdjDtls" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmPartPriceAdjDtls.jsp -->

<fmtPartPriceAdjDtls:setLocale value="<%=strLocale%>"/>
<fmtPartPriceAdjDtls:setBundle basename="properties.labels.sales.pricing.GmPartPriceAdjDtls"/>
<%
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
HashMap hmAdjCode = null;
%>
<html>
<head>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
 <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_form.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_markers.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxMenu/dhtmlxmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_validation.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_fast.js"></script>
<!-- The library provides support for different keyboard commands that let users to navigate through the grid. -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<bean:define id="gridData" name="<%=strFormName%>" property="strXMLGrid" type="java.lang.String"> </bean:define>
<bean:define id="alAdjCodeLst" name="<%=strFormName%>" property="alAdjCodeLst" type="java.util.ArrayList" ></bean:define>
<script>
var objGridData;
objGridData ='<%=gridData%>';
var adjCodeListLen = <%= alAdjCodeLst.size()%>;

<%	
hmAdjCode = new HashMap();	
for (int i=0;i< alAdjCodeLst.size();i++)
{
  hmAdjCode = (HashMap) alAdjCodeLst.get(i); 
%>
var adjCodeArr<%=i%> ="<%=GmCommonClass.parseNull((String)hmAdjCode.get("CODEID"))%>";
<%
}
%>
</script>

</head>
<body leftmargin="20" topmargin="10">
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr height="30">
			<td class="RightTableCaption" align="left" height="30">
				<div id="rfs_grid" style="display:inline" >
					<table cellpadding="1" cellspacing="1" border="0">
						<tr>
							<td class="RightTableCaption">
								&nbsp;<a href="#"><fmtPartPriceAdjDtls:message key="IMG_ALT_COPY" var = "varCopy"/><img src="<%=strImagePath%>/dhtmlxGrid/copy.gif" onClick="javascript:docopy()" alt="${varCopy}" style="border: none;" height="14"></a>
								&nbsp;<a href="#"><fmtPartPriceAdjDtls:message key="IMG_ALT_ADDROWS" var = "varAddRows"/><img src="<%=strImagePath%>/dhtmlxGrid/row_add_after_1.gif" alt="${varAddRows}" style="border: none;" onClick="javascript:addRowFromClipboard()" height="14"></a>
								&nbsp;<a href="#"><fmtPartPriceAdjDtls:message key="IMG_ALT_ADD" var = "varAdd"/><img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="${varAdd}" style="border: none;" onClick="javascript:addRow()"height="14"></a>
								&nbsp;<a href="#"><fmtPartPriceAdjDtls:message key="IMG_ALT_REMOVE" var = "varRemove"/><img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="${varRemove}" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a>
								&nbsp;<a href="#"><fmtPartPriceAdjDtls:message key="IMG_ALT_PASTE" var = "varPaste"/><img src="<%=strImagePath%>/dhtmlxGrid/paste.gif" alt="${varPaste}" style="border: none;" onClick="javascript:pasteToGrid()" height="14"></a>
							</td>									
						</tr>
						<tr>
							<td height="17"><font color="#800000">&nbsp;<fmtPartPriceAdjDtls:message key="LBL_MAXIMUM"/></font></td>
						</tr>
					</table>
				</div>
			</td>									
		</tr>
		
		<tr><td ><div id="dataGridDiv"  style="height:450px"></div></td></tr>
		<tr><td>
			<table cellpadding="1" cellspacing="1" border="0" width="100%">
				<tr height="50">
					<td  width="3%"><p style="height: 25px; width: 25px; border-bottom: #000000 1px solid; border-left: #000000 1px solid; border-top: #000000 1px solid; border-right: #000000 1px solid; background: #e5e500;"></td>
					<td width="10%"><b><fmtPartPriceAdjDtls:message key="LBL_ADJUSTMENT"/>  </b></td>
					<td width="3%"><p style="height: 25px; width: 25px; border-bottom: #000000 1px solid; border-left: #000000 1px solid; border-top: #000000 1px solid; border-right: #000000 1px solid; background: #ff4c4c;"></td>
					<td width="12%"><b><fmtPartPriceAdjDtls:message key="LBL_MOUSE_OVER"/> </b></td>
					<td width="72%"></td>
				</tr>
			</table>
		</td></tr>
		<%-- <tr height="30">
		<td align="center">
			<gmjsp:button name="Btn_Save" value="Save" gmClass="button" style="width:5em" onClick="javascript:fnSave(this);" tabindex="2" buttonType="Save" />
			<gmjsp:button name="Btn_Submit" value="Submit" gmClass="button" style="width:5em" onClick="javascript:fnSubmit(this);" tabindex="2" buttonType="Save" />
			<gmjsp:button name="Btn_Void" value="Void" gmClass="button" style="width:5em" onClick="javascript:fnVoid(this);" tabindex="2" buttonType="Save" />
			<gmjsp:button name="Btn_Excel" value="Excel Download" gmClass="button" onClick="fnExcelExport();" tabindex="2" buttonType="Load" />
		</td>
		</tr> --%>
		<%-- <tr>
    		<td height="25" class="ShadeRightTableCaption" width="100%">&nbsp;Comments <font color="red" >* Required for Denied Action</font></td>     		
    		</tr>
    		<tr><td colspan="8" height="1" bgcolor="#cccccc"></td></tr>
    		<tr>
			<td height="70px">&nbsp;<html:textarea property="deniedComments"  cols="130" style="height:50px"
					styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
			</td>
		</tr> --%>
		
		<tr><td colspan="8" height="1" bgcolor="#676767"></td></tr>
		<tr>
		<td colspan="5"> 
			<jsp:include page="/common/GmIncludeLog.jsp" >
				<jsp:param name="FORMNAME" value="frmPartPriceAdj" />
				<jsp:param name="ALNAME" value="alLogReasons" />
				<jsp:param name="LogMode" value="Edit" />
				<jsp:param name="Mandatory" value="yes" />
			</jsp:include>
		</td>
		</tr>
		<tr><td colspan="8" height="1" bgcolor="#cccccc"></td></tr>
		<tr height="30" class="Shade">
			<td  class="RightTableCaption" colspan="9" align="center">
			<fmtPartPriceAdjDtls:message key="LBL_CHOOSE_ACTION" var="varAction"/><gmjsp:label type="RegularText" SFLblControlName="${varAction}" td="false" />&nbsp;
			<gmjsp:dropdown controlName="actions" SFFormName="frmPartPriceAdj" SFSeletedValue="actions" SFValue="alActions" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
			&nbsp;<fmtPartPriceAdjDtls:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="Btn_Save" value="${varSubmit}" gmClass="button" style="width:5em;cursor:pointer" onClick="javascript:fnSubmitAction(this);" tabindex="2" buttonType="Load" />
			</td>
		</tr>
		<tr><td colspan="8" align="center" class="RightTableCaption"><font color="green"><bean:write name="frmPartPriceAdj" property="successMessage"></bean:write></font></td></tr>
	</table>
</body>
</html>