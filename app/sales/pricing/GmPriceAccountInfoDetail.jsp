<%
	/**********************************************************************************************
	 * File		 		: GmPriceAccountInfoDetail.jsp
	 * Desc		 		: This screen is used to review the account information Details
	 * Version	 		: 1.0
	 * author			: Jagadeesh Reddy N
	 **********************************************************************************************/
%>

 <!-- \sales\pricing\GmPriceAccountInfoDetail.jsp -->
	<%@ include file="/common/GmHeader.inc" %>
	<%@ page language="java" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

	<%@ page import ="com.globus.common.beans.GmCommonControls"%>	
	<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
	<%@ page import ="org.apache.commons.beanutils.PropertyUtils"%>
	
 	<bean:define id="priceRequestId" name="frmPriceApprovalRequest" property="priceRequestId" type="java.lang.String"></bean:define>
 	<bean:define id="last12MonthSales" name="frmPriceApprovalRequest" property="last12MonthSales" type="java.lang.String"></bean:define>
 	<bean:define id="accid" name="frmPriceApprovalRequest" property="accid" type="java.lang.String"></bean:define>
 	
 	<%
	String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");
 	String strCurrencySymbl = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
 	String strCurrencyFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplCurrFmt"));
	%>
 	
<HTML>
<HEAD>	
	<TITLE> Globus Medical: Account Information</TITLE>
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">	
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
	
		
	<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
	<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
	<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmPriceApprovalRequest.js"></script>
		
	<style type="text/css" media="all">
	@import url("<%=strCssPath%>/displaytag.css");
	  @import url("<%=strCssPath%>/screen.css");
	  table.DtTable583 {
		margin: 0 0 0 0;
		width: 583px;
		border: 1px solid  #676767;
	}  
	</style>
	<script>
	
	</script>	
</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
	<html:form action="/gmPriceApprovalRequestAction.do?" >
	<html:hidden property="strOpt"/>
	<html:hidden property="hiddenPriceReqID" value="<%=priceRequestId%>"/>	
	<input type="hidden" name="sysProjectString">

	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" bordercolor="red" style="white-space: nowrap;" class="DtTable583"  cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;Account/Group Information</td>
			<td align="right" class=RightDashBoardHeader>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("PRICE_ACCOUNT_INFORMATION")%>');" />
			</td>
		</tr>
	<%
	if 	(!accid.equals(""))
	{	
	%>	
		<tr>
		<td class="RightTableCaption" width="40%" align="right" HEIGHT="25" >&nbsp;Account Id:</td>	
		<td align="left">&nbsp;<bean:write name="frmPriceApprovalRequest" property="accid"></bean:write></td>
		</tr>
		<tr class="shade">
		<td class="RightTableCaption" align="right" HEIGHT="25" >&nbsp;Account Name:</td>	
		<td align="left">&nbsp;<bean:write name="frmPriceApprovalRequest" property="strparentacc"></bean:write></td>
		</tr>
		<tr>
		<td class="RightTableCaption" align="right" HEIGHT="25" >&nbsp;Contract Account:</td>	
		<td align="left">&nbsp;<bean:write name="frmPriceApprovalRequest" property="contractflname"></bean:write></td>
		</tr>
		<tr class="shade">
		<td class="RightTableCaption" align="right" HEIGHT="25" >&nbsp;Currency:</td>	
		<td align="left">&nbsp;<bean:write name="frmPriceApprovalRequest" property="strcurrency"></bean:write></td>
		</tr>
		<tr>
		<td class="RightTableCaption" align="right" HEIGHT="25" >&nbsp;GPO Affiliation:</td>	
		<td align="left">&nbsp;<bean:write name="frmPriceApprovalRequest" property="strhlcaff"></bean:write></td>
		</tr>
		<tr class="shade">
		<td class="RightTableCaption" align="right" HEIGHT="25" >&nbsp;Group Price Book:</td>	
		<td align="left">&nbsp;<bean:write name="frmPriceApprovalRequest" property="strgpb"></bean:write></td>
		</tr>
		<tr>
		<td class="RightTableCaption" align="right" HEIGHT="25" >&nbsp;IDN:</td>	
		<td align="left">&nbsp;<bean:write name="frmPriceApprovalRequest" property="strgpraff"></bean:write></td>
		</tr>
		<tr class="shade">
		<td class="RightTableCaption" align="right" HEIGHT="25" >&nbsp;Rep Accts For Parent Account:</td>	
		<td align="left">&nbsp;<bean:write name="frmPriceApprovalRequest" property="strparentacc"></bean:write></td>
		</tr>
		<tr>
		<td class="RightTableCaption" align="right" HEIGHT="25" >&nbsp;Distibutor:</td>	
		<td align="left">&nbsp;<bean:write name="frmPriceApprovalRequest" property="strdname"></bean:write></td>
		</tr>
		<tr class="shade">
		<td class="RightTableCaption" align="right" HEIGHT="25" >&nbsp;Sales Rep:</td>	
		<td align="left">&nbsp;<bean:write name="frmPriceApprovalRequest" property="strrepname"></bean:write></td>
		</tr>
		<tr>
		<td class="RightTableCaption" align="right" HEIGHT="25" >&nbsp;Area Director:</td>	
		<td align="left">&nbsp;<bean:write name="frmPriceApprovalRequest" property="stradname"></bean:write></td>
		</tr>
		<tr class="shade">
		<td class="RightTableCaption" align="right" HEIGHT="25" >&nbsp;Vice President:</td>	
		<td align="left">&nbsp;<bean:write name="frmPriceApprovalRequest" property="strvpname"></bean:write></td>
		</tr>
		<tr>
		<td class="RightTableCaption" align="right" HEIGHT="25" >&nbsp;Region:</td>	
		<td align="left">&nbsp;<bean:write name="frmPriceApprovalRequest" property="strregname"></bean:write></td>
		</tr>
		<tr class="shade">
		<td class="RightTableCaption" align="right" HEIGHT="25" >&nbsp;Territory:</td>	
		<td align="left">&nbsp;<bean:write name="frmPriceApprovalRequest" property="strtername"></bean:write></td>
		</tr>
		<tr>
		<td class="RightTableCaption" align="right" HEIGHT="25" >&nbsp;12 Months Sale:</td>	
		<td align="left" >&nbsp;<%=strCurrencySymbl%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(last12MonthSales,strCurrencyFmt))%></td>
		</tr>
	<%
	}else{
	%>	<tr><td colspan="3" height="25" align="center" class="RightTextBlue">
		<BR>No Data Available</td></tr>
	<%	}

 	%>
		</table>					
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>