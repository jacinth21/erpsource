<%
/**********************************************************************************
 * File		 		: GmPricingAnnualChange.jsp
 * Desc		 		: Accounts annual change and initiate multiple price requests for selected accounts
 * Version	 		: 1.0
 * author			: Xun
************************************************************************************/
%>

<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>

<%@ taglib prefix="fmtPricingAnnualChange" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- sales\pricing\GmPricingAnnualChange.jsp -->

<fmtPricingAnnualChange:setLocale value="<%=strLocale%>"/>
<fmtPricingAnnualChange:setBundle basename="properties.labels.sales.pricing.GmPricingAnnualChange"/>

<bean:define id="alAccount" name="frmPricingAnnualChange" property="alAccount" type="java.util.List"></bean:define>
<bean:define id="alDistributor" name="frmPricingAnnualChange" property="alDistributor" type="java.util.List"></bean:define> 
 

<%
String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");
String strWikiTitle = GmCommonClass.getWikiTitle("ACCOUNT_ANNUAL_PRICE_CHANGE_SETUP");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Pricing Status Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmPricingAnnualChange.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
 
 

 var arrAccountIn = new Array(1);
 var allAccountsHtml;
<%
	ArrayList alAccountList = GmCommonClass.parseNullArrayList((ArrayList) alAccount);
	int intAcctSize = alAccountList.size();
	 
	HashMap hmLoop = null;
	if (intAcctSize > 0)
	{
	    for (int i=0;i< intAcctSize;i++)
	    {
		hmLoop = (HashMap) alAccountList.get(i);
		%>
		arrAccountIn[<%=i%>] = new Array("<%=hmLoop.get("ADID")%>","<%=hmLoop.get("NM")%>", "<%=hmLoop.get("ID")%>", "<%=hmLoop.get("DID")%>");
		<% }
	}%>

 
 
 var distLen = <%=alDistributor.size()%>;

<%
	HashMap hcboVal = new HashMap();
	for (int i=0;i<alDistributor.size();i++)
	{
		hcboVal = (HashMap)alDistributor.get(i);
%>
	var DistArr<%=i%> ="<%=hcboVal.get("ADID")%>, <%=hcboVal.get("ID")%>,<%=hcboVal.get("NM")%>";
<%
	}
%>
 
 
	
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10"  >
<html:form action="/gmPricingAnnualChange.do">

<html:hidden property="strOpt" /> 

<html:hidden property="haccounts" /> 
 
<!-- Custom tag lib code modified for JBOSS migration changes -->
<!-- Struts tag lib code modified for JBOSS migration changes -->


	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtPricingAnnualChange:message key="LBL_ACC_ANNUAL_PRICE"/></td>
			
			<td align="right" class=RightDashBoardHeader > 	<fmtPricingAnnualChange:message key="IMG_ALT_HELP" var = "varHelp"/> <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp} ' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				 	
				 	</td>
			
		</tr>
		<tr><td colspan="4" height="1" class="line"></td></tr>
		<tr>
			<td width="848" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					 
                    <tr><td colspan="4" class="Line"></td></tr>
                     
                      <tr class="Shade">
                     <td height="24" class="RightTableCaption" align="right"> <fmtPricingAnnualChange:message key="LBL_AD"/> :</td>
						<td> &nbsp;&nbsp;<gmjsp:dropdown controlName="adID" SFFormName="frmPricingAnnualChange" SFSeletedValue="adID" onChange="javascript:fnFilterAccounts(this);" 
										SFValue="alADId" codeId = "ADID"  codeName = "NM"  defaultValue= "[Choose One]"/> 		
						</td>	
						
					   <td height="24" class="RightTableCaption" align="right"> <fmtPricingAnnualChange:message key="LBL_DISTRIBUTOR"/> :</td>
						<td> &nbsp;&nbsp;<gmjsp:dropdown controlName="distributorID" SFFormName="frmPricingAnnualChange" SFSeletedValue="distributorID" onChange="javascript:fnDistFilterAccounts(this);" 
										SFValue="alDistributor" codeId = "ID"  codeName = "NM"  defaultValue= "[Choose One]"/> 		
						</td>	
                     </tr>
                     <tr><td colspan="4" class="LLine"></td></tr> 
                    <tr >
                       <td height="24" class="RightTableCaption" align="right"> <fmtPricingAnnualChange:message key="LBL_ACCOUNTS"/> :&nbsp;&nbsp;</td>
						<td>  
						  <DIV class="gmFilter" id="Div_Account"  style="width:280px;height:150px">
						  <table> <tr><td bgcolor="gainsboro" > <html:checkbox property="selectAcctAll" onclick="fnSelectAcctAll('toggle');" /><B><fmtPricingAnnualChange:message key="LBL_SELECT_ALL"/> </B> </td>
							</tr></table> 
							 <table cellspacing="0" cellpadding="0">
								
								 <logic:iterate id="SelectedAccounts" name="frmPricingAnnualChange" property="alAccount">
								<tr>
									<td><htmlel:multibox property="checkAccounts"  value="${SelectedAccounts.ID}"   />
										 <bean:write name="SelectedAccounts" property="NM" /></td>
								</tr>
								</logic:iterate>
							
								 </table>
								</div>
						
						
							</td>	
						
					    <td height="24" class="RightTableCaption" align="right"> <fmtPricingAnnualChange:message key="LBL_SYSTEM"/> :&nbsp;&nbsp;</td>
						<td>  
						  <DIV class="gmFilter"    style="width:280px;height:150px">
						  <table> <tr><td bgcolor="gainsboro" > <html:checkbox property="selectSetAll" onclick="fnSelectSetAll('toggle');" /><B><fmtPricingAnnualChange:message key="LBL_"/>Select All </B> </td>
							</tr></table> 
									<table >
		                        	<logic:iterate id="SelectedSetlist" name="frmPricingAnnualChange" property="alSets">
		                        	<tr>
		                        		<td>
									    	<htmlel:multibox property="checkSelectedSets" value="${SelectedSetlist.ID}" />
										    
										    <bean:define id="sysNm" name="SelectedSetlist" property="NAME" type="java.lang.String"> </bean:define>
										 &nbsp;<%=sysNm%> 
										</td>
									</tr>	    
									</logic:iterate>
								</table>
								</div>
						
						
							</td>	
                    </tr>
                    
                    <tr><td colspan="4" class="LLine"></td></tr>
                    <tr class="Shade">
                       <td height="24"  colspan="2" class="RightTableCaption" align="left">&nbsp;&nbsp;<fmtPricingAnnualChange:message key="LBL_DEFAULT_VALUES"/> :</td>
                       <td height="24" colspan="2"  > </td>
						  
                    </tr> 
                 <tr><td colspan="4" class="LLine"></td></tr> 
                 
                   <tr>
                     <td height="24" class="RightTableCaption" align="right"><font color="red">*</font> <fmtPricingAnnualChange:message key="LBL_CHANGE"/> :</td>
						<td> &nbsp;&nbsp;<gmjsp:dropdown controlName="changeType" SFFormName="frmPricingAnnualChange" SFSeletedValue="changeType"
										SFValue="alChangeType" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"/> 
							<html:text property="changeValue"  size="5" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;					
						</td>	
						
						   <td height="24" class="RightTableCaption" align="right"><font color="red">*</font> <fmtPricingAnnualChange:message key="LBL_EFFECTIVE_DATE"/> :&nbsp;</td>
						<td>  <html:text property="effectiveDate"  size="10" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;
				                     <fmtPricingAnnualChange:message key="IMG_ALT_OPEN" var = "varOpen"/><img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmPricingAnnualChange.effectiveDate');" title="${varOpen}"  
							src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />
						</td>	
                     </tr>
                     <tr><td colspan="4" class="Line"></td></tr>   
                      <tr  > 
                       <td height="24"  colspan="4" class="RightTableCaption" align="center">&nbsp;&nbsp;
                        <logic:equal name="frmPricingAnnualChange" property="strEnableSubmit" value="false"> 
	                   <fmtPricingAnnualChange:message key="BTN_SUBMIT" var="varSubmit"/>
	                   <gmjsp:button value="${varSubmit}" gmClass="button" disabled="true" buttonType="Save"  onClick="fnSubmit();" />
	                   </logic:equal>
	                   <logic:equal name="frmPricingAnnualChange" property="strEnableSubmit" value="true"> 
                        <fmtPricingAnnualChange:message key="BTN_SUBMIT" var="varSubmit"/>
                        <gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnSubmit();" /> 
                        </logic:equal>
                        </td>
                       
                    </tr>                    
   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>

