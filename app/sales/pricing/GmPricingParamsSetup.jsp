<%
/**********************************************************************************
* File                    : GmPricingParamsSetup.jsp 
* Desc                    : Pricing parameter details
* author                  : Karthik
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtPricingParamsSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmPricingParamsSetup.jsp -->

<fmtPricingParamsSetup:setLocale value="<%=strLocale%>"/>
<fmtPricingParamsSetup:setBundle basename="properties.labels.sales.pricing.GmPricingParamsSetup"/>

<%-- <bean:define id="strContract" name="frmPricingParamsSetup" property="strContract" type="java.lang.String"> </bean:define> --%>
<bean:define id="alWastePriceList" name="frmPricingParamsSetup" property="alWastePriceList" type="java.util.ArrayList"></bean:define>
<bean:define id="alRevisionPriceList" name="frmPricingParamsSetup" property="alRevisionPriceList" type="java.util.ArrayList"></bean:define>
<bean:define id="accid" name="frmPricingParamsSetup" property="accid" type="java.lang.String"></bean:define>
<bean:define id="showWasedHistory" name="frmPricingParamsSetup" property="showWasedHistory" type="java.lang.String"></bean:define>
<bean:define id="showRevisionHistory" name="frmPricingParamsSetup" property="showRevisionHistory" type="java.lang.String"></bean:define>
<bean:define id="hScreenType" name="frmPricingParamsSetup" property="hScreenType" type="java.lang.String"></bean:define>
<bean:define id="partyId" name="frmPricingParamsSetup" property="partyId" type="java.lang.String"></bean:define>
<bean:define id="wastePrcAdjID" name="frmPricingParamsSetup" property="wastePrcAdjID" type="java.lang.String"></bean:define>
<bean:define id="revisionPrcAdjID" name="frmPricingParamsSetup" property="revisionPrcAdjID" type="java.lang.String"></bean:define>

<%
String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");
String strAttrId = "";
String strScreenType = hScreenType;

String strSubmitAccess = GmCommonClass.parseNull((String)request.getAttribute("SUBMITACCFL"));
String strBtnDisable = "false";
if(!strSubmitAccess.equals("Y")){
	strBtnDisable = "true";
}


%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Party Setup </TITLE>
<meta http-equiv="Content-Type"content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmPricingParamsSetup.js?v=1"></script>

<script>
var accountID = '<%=accid%>';
var partyID = '<%=partyId%>';
var screenType = '<%=strScreenType%>';
var wstPrcID = '<%=wastePrcAdjID%>';
var reivPrcID = '<%=revisionPrcAdjID%>';
</script>
<style>
table.DtTable770{	
	margin: 0 0 0 0;
	width: 780px;
	border: 1px solid  #676767;
}
</style>
</HEAD>

<BODY leftmargin="10" topmargin="20">
<html:form  action="/gmPricingParamsSetup.do">
<input type="hidden" name="hScreenType"> 
<input type="hidden" name="hAccAttInputStr">
<input type="hidden" name="hPriceAdjInputStr">
<table class="DtTable770" border="0" cellspacing="0" cellpadding="0">

	 <% if(!strScreenType.equals("groupAccountSetup")){ %>
		 <tr><td colspan="4" class="LLine" height="1" ></td></tr>
		 <tr>
       <td colspan="4" >
           <jsp:include page="/custservice/GmAccountAffiliation.jsp" >
           <jsp:param name="FORMNAME" value="frmPricingParamsSetup" />                                                                                                                                       
           </jsp:include>       
       </td>                
    </tr>   
	<%} %>
		  <tr>
       	<td colspan="4" class="ShadeRightTableCaption" height="22"><fmtPricingParamsSetup:message key="LBL_PRICE_ADJ_CODE"/></td>
       </tr> 
       <tr><td class="LLine" height="1" colspan="4"></td></tr>
       <tr>
       	<td colspan="4">
       		<table width="100%" border="0" cellspacing="0" cellpadding="0">
       			<tr>
       				<td class="RightTableCaption" align="right" HEIGHT="24" width="15%">&nbsp;<fmtPricingParamsSetup:message key="LBL_ADJ_CODE"/>:</td>
					<td class="RightTableCaption" align="center" HEIGHT="24" width="15%"> <fmtPricingParamsSetup:message key="LBL_ADJ_VALUE"/> </td>
					<td class="RightTableCaption" align="left" HEIGHT="24" width="25%">&nbsp;<fmtPricingParamsSetup:message key="LBL_PRICE_LIST"/></td>
       			</tr>
       			<tr><td class="LLine" height="1" colspan="3"></td></tr>
       			<tr class="oddshade">
	              <td class="RightTableCaption" align="right" HEIGHT="24"  width="15%">&nbsp;<fmtPricingParamsSetup:message key="LBL_WASTED"/>:</td>
	              <td align="center" width="15%">&nbsp;<html:text styleId="wasted" property="wasted" styleClass="InputArea" name="frmPricingParamsSetup"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
	              <td align="left"  width="25%">&nbsp;<gmjsp:dropdown width="220" controlName="wastedPrice" SFFormName="frmPricingParamsSetup" SFSeletedValue="wastedPrice" SFValue="alWastePriceList" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" />
	              &nbsp;<%if(showWasedHistory.equals("Y")){ %>
						<a href="#" title="Click to view the history" onclick="javascript:fnWasteHistory()"><fmtPricingParamsSetup:message key="IMG_ALT_VIEW" var = "varView"/><img alt="${varView}" src="<%=strImagePath%>/icon_History.gif" width="18" height="18" border="0"></a>
				  <%} %>
     			</td>
              	</tr> 
              	<tr><td class="LLine" height="1" colspan="3"></td></tr>   
              	<tr class="evenshade">
	              <td class="RightTableCaption" align="right" HEIGHT="24"  width="15%">&nbsp;<fmtPricingParamsSetup:message key="LBL_REVISION"/>:</td>
	              <td align="center" width="15%">&nbsp;<html:text styleId="revision" property="revision" styleClass="InputArea" name="frmPricingParamsSetup"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
	              <td align="left"  width="25%">&nbsp;<gmjsp:dropdown width="220" controlName="revisionPrice" SFFormName="frmPricingParamsSetup" SFSeletedValue="revisionPrice" SFValue="alRevisionPriceList" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"  />
	               &nbsp;<%if(showRevisionHistory.equals("Y")){ %>
						<a href="#" title="Click to view the history" onclick="javascript:fnRevisionHistory()"><fmtPricingParamsSetup:message key="IMG_ALT_VIEW" var = "varView"/><img alt="${varView}" src="<%=strImagePath%>/icon_History.gif" width="18" height="18" border="0"></a>
				  <%} %>
	              </td>
              	</tr>    			
       		</table>      
         </td>
	</tr> 
	<tr><td class="LLine" height="1" colspan="4"></td></tr>
	<tr>
       <td colspan="4" align="left" height="25">
           <jsp:include page="/common/GmIncludeLog.jsp" >
           <jsp:param name="FORMNAME" value="frmPricingParamsSetup" />
           <jsp:param name="ALNAME" value="alLogReasons" />
           <jsp:param name="LogMode" value="Edit" />     
           <jsp:param name="Mandatory" value="yes" />                                                                                                                                     
           </jsp:include>       
       </td>                
    </tr>                
    <tr><td class="LLine" height="1" colspan="4"></td></tr>
    <tr class="evenshade" height="30px">
    <% 
		String strSubmit = "fnSubmit('"+strScreenType+"')";
	%>
    	<td align="center" colspan="4"><fmtPricingParamsSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" style="width: 6em;height: 23"  disabled="<%=strBtnDisable %>" gmClass="Button" onClick="<%=strSubmit%>" buttonType="save" />
        </td>
    </tr> 
                     
</table>                          
</html:form>
              <%@ include file="/common/GmFooter.inc"%>
       </BODY>
</HTML>
