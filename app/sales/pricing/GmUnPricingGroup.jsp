

<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>

<%@ taglib prefix="fmtUnPricingGroup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmUnPricingGroup.jsp -->

<fmtUnPricingGroup:setLocale value="<%=strLocale%>"/>
<fmtUnPricingGroup:setBundle basename="properties.labels.sales.pricing.GmUnPricingGroup"/>

<bean:define id="gridPendingGroupAccountPrice" name="frmPricingRequestForm" property="pendingGroupAccountPriceGridXML" type="java.lang.String"></bean:define>
<bean:define id="pendingGroupAccountCount" name="frmPricingRequestForm" property="pendingGroupAccountCount" type="java.lang.String"></bean:define>
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("PRICING_STATUS_REPORT");
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Pricing -Unpriced Group Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>


<style type="text/css" media="all">
@import url("<%=strCssPath%>/displaytag.css");

.grid_hover {
	background-color: #E6E6FA;
	font-size: 20px;
}

div.gridbox_dhx_skyblue table.obj tr td {
	border-width: 0px 1px 0px 0px;
	border-color: #A4BED4;
}
</style>

<script type="text/javascript">
function fnOnPageLoad() {		
	var gridPendingGroupAccountPriceData = '<%=gridPendingGroupAccountPrice%>';
	
	if (gridPendingGroupAccountPriceData != ''){
		
		gridObj = initGrid('dataGridDiv',gridPendingGroupAccountPriceData);
		gridObj.enableTooltips("true,true,true,true");
	}
	
}
/*function fnOpenSystemPricingInitiate(reqid,accid,systemid,groupid){
	window.opener.fnAjaxPopUpRst(reqid,accid,systemid,groupid);
	window.close();
	}
*/
function fnOpenSystemPricingInitiate(reqid,accid,systemid,groupid){
	window.parent.fnAjaxPopUpRst(reqid,accid,systemid,groupid);
	CloseDhtmlxWindow();
	return false;
	}
	
function fnClose()
{
	CloseDhtmlxWindow();
	return false;
}

</script>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad()">
<html:form action="/gmPricingRequestDAction.do?method=UnPricingGroupReport">

	<html:hidden property="strOpt" />
	

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><%=pendingGroupAccountCount %></td>
			</tr>
			
			
			<tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>
			
			
			<tr id="gridData"> 
			
				<td><div id="dataGridDiv" style="grid" height="550px" width="850">&nbsp;</div></td> 
			</tr>
			<tr>
			<td height="50" align="center"><fmtUnPricingGroup:message key="BTN_CLOSE" var="varClose"/><gmjsp:button style="width: 8em; height: 2em; font-size: 13px" name="Close_button" value="${varClose}" buttonType="Load" gmClass="button" onClick="fnClose();" /> </td>
			</tr>
			</table>
			</html:form>
			</BODY></HTML>
			
