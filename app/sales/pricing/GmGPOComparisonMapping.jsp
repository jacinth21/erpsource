 <%
/**********************************************************************************
 * File		 		: GmGPOComparisonMapping.jsp
 * Desc		 		: This screen is used to display the generate price file Report
 * Version	 		: 1.0
 * author			: dsandeep
************************************************************************************/
%>


<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtGPOComparisonMapping" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmGPOComparisonMapping.jsp -->
<fmtGPOComparisonMapping:setLocale value="<%=strLocale%>"/>
<fmtGPOComparisonMapping:setBundle basename="properties.labels.sales.pricing.GmGPOComparisonMapping"/>

<bean:define id="priceRequestId" name="frmPriceApprovalRequest" property="priceRequestId" type="java.lang.String"></bean:define>
<bean:define id="alSystemList" name="frmPriceApprovalRequest" property="alSystemList" type="java.util.List"></bean:define>
<bean:define id="saveSys" name="frmPriceApprovalRequest" property="saveSys" type="java.lang.String"></bean:define>
<HTML>

<%
String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");
String strSetName = "";
String strSetId="";
String strGRPFlag = "";
HashMap hmResult = new HashMap();
HashMap hmDetails = new HashMap();
ArrayList alSystemDetails = new ArrayList();
ArrayList alGpbDetails = new ArrayList();
hmResult = (HashMap)request.getAttribute("SYSTEMLIST");
hmDetails = (HashMap)request.getAttribute("GPBLIST");
alSystemDetails = GmCommonClass.parseNullArrayList((ArrayList)hmResult.get("SYSTEMLIST"));
alGpbDetails = GmCommonClass.parseNullArrayList((ArrayList)hmDetails.get("GPBLIST"));
String strSaveSys = GmCommonClass.parseNull((String)hmResult.get("SAVESYS"));
int intLength = alSystemDetails.size();
int intGpbLength = alGpbDetails.size();
String dupChk="";
%>
<HEAD>
<TITLE> Globus Medical: Pricing GPO Comparison</TITLE>
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");

</style>
<style>
.Table_border {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 6px;
}
div.scroll {
    height: 300px;
    overflow: scroll;
}

</style>

<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxCombo.js"></script>
<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmGPOComparisonMapping.js"></script>
<script type="text/javascript">

var inputstr ='';
var setid='';
var i='<%=intLength%>';
var priceRequestId="<%=priceRequestId%>";
var saveSys = "<%=saveSys%>";
var remove = "<%=strImagePath%>";
var strSaveSys= "<%=strSaveSys%>";
var intGpbLength= "<%=intGpbLength%>";
</script>
</HEAD>
<BODY>
<html:form action="/gmPriceApprovalRequestAction.do">
<input type="hidden" name="hsetId" id ="hsetId" value="">
<input type="hidden" name="inputString" id ="inputString" value="">

<table border="0" class="DtTable1000"cellspacing="0" cellpadding="0" height="650">
<tr>
 <td height="25" class="RightDashBoardHeader" colspan = "3">&nbsp;Pricing GPO Comparison</td>
  <td align="right" class=RightDashBoardHeader colspan = "3">
<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("PRICE_REQUEST_DETAILS")%>');" />
</td>
<td colspan="4"></td>
</tr>
<tr style="background-color: green;">
<td height="20" colspan ="6" id="successCont" style="display: none;color: #FFFFFF;"></td>
</tr>
<tr>
<td class="RightTableCaption" height="35"  align = "right" colspan="2">Price Request ID</td>&nbsp;
<td class="RightTableCaption" height="35"  align = "left" colspan="2">:&nbsp;<%=priceRequestId%></td>
<td colspan="2"></td>
</tr>
<tr>	
  <td height="26px" class="RightTableCaption" align="right" colspan="2" style="min-width: 127px">Additional System:</td>
  <td style="height: 26px" colspan="2">&nbsp;
  <gmjsp:autolist comboType= "DhtmlXCombo" controlName="systemListId"
										SFFormName="frmPriceApprovalRequest" SFSeletedValue="systemListId"
										SFValue="alSystemList" codeId="ID" codeName="NAME"
										defaultValue="[Choose One]" width="300"/> 
 <gmjsp:button value="&nbsp;Add&nbsp;" style="vertical-align: text-bottom;height: 26px; width: 5em;"  buttonType="Load" onClick="javascript:fnAddSystem(this);"/>
</td>
<td colspan="2"></td>
</tr>
<tr>
<td height="30"></td>
</tr>

<tr height = "30">
<td align = "left" colspan = "4">
<table width = "30%" align ="center" style = "background-color:#FFFFFF;min-width: 551px;margin-left: 20px;" border="0" class ="Table_border"  height = "150px" resizable="yes" scrollbars= "yes">
<tr class ="Table_border RightDashBoardHeader" bgcolor = "#3172AA" height = "30"  border-collapse: collapse;>
<td width = "11%" class ="Table_border RightDashBoardHeader" align = "center">Delete</td>
<td width = "58%" class ="Table_border RightDashBoardHeader" align = "center">System</td>
<td width = "35%" class ="Table_border RightDashBoardHeader" align = "center">Selected</td>
</tr>

<td align = "center" colspan = "3" height = "1" >
<div class = "scroll">
<table height="1" class ="Table_border"  id="gpoCompTable" border="0">
<tr>
<td align = "center" colspan = "3" height = "1" >
<%
if(intLength>0)
{
	 for(int i =0;i<intLength;i++)
	 {
		 int j=i+1;
		 HashMap hmList = new HashMap();
		 hmList = (HashMap)alSystemDetails.get(i);
		 strSetName = GmCommonClass.parseNull((String)hmList.get("SETNAME"));
		 strSetId = GmCommonClass.parseNull((String)hmList.get("SYSID"));
		 strGRPFlag = GmCommonClass.parseNull((String)hmList.get("GRPFL"));
		 if(strGRPFlag.equals("Y"))
		 {%>
		 <tr id="delSystemId<%=j%>"><td width ="11.4%" class ="Table_border" style ="font-size:14px;background-color:none;" align = "center">
		 <span style="color:red" onclick="javascript:fndelete('<%=j%>');"><img src="<%=strImagePath%>/delete.gif" title="Remove"  border=0></span></td>
		 <td class ="Table_border" height="20" width ="60%"><%=strSetName%><input type="hidden" id="hSetId<%=j%>" value="<%=strSetId%>" > 
		 </td><td class ="Table_border" height="20" width ="30%"  align ="center"><input type="checkbox" id="chk<%=j%>" name="allGroups" value="Y" checked></td></tr>
		 
		<%}
		 else
		 {%>
		 <tr id="delSystemId<%=j%>"><td width ="11.4%" class ="Table_border" style ="font-size:14px;background-color:none;" align = "center">
		 <span style="color:red" onclick="javascript:fndelete('<%=j%>');"><img src="<%=strImagePath%>/delete.gif" title="Remove"  border=0></span></td>
		 <td class ="Table_border" height="20" width ="60%"><%=strSetName%><input type="hidden" id="hSetId<%=j%>" value="<%=strSetId%>" > 
		 </td><td class ="Table_border" height="20" width ="30%"  align ="center"><input type="checkbox" id="chk<%=j%>" name="allGroups" value="Y" ></td></tr>
		<%}
	 }
}

%>
</tr>
</table>

</td>
<tr>
</table>

</td>
<td style = "min-width: 260px;float: right;margin-left: 50px;">
<b>Mapped GPB:</b>
<table height="1" class ="Table_border"  id="gpoCompTable" border="0" >
<tr class ="Table_border RightDashBoardHeader" bgcolor = "#3172AA" height = "30"  border-collapse: collapse;>
<td class ="Table_border RightDashBoardHeader" height="20" width ="30%" align = "center">GPB Name</td>
<td class ="Table_border RightDashBoardHeader" height="20" width ="30%" align = "center">Selected</td>
</tr>
<tr>
<td align = "center" colspan = "3" height = "1" >
<%
if(intGpbLength>0)
{
	 for(int i =0;i<intGpbLength;i++)
	 {
		 int j=i+1;
		 HashMap hmGpbList = new HashMap();
		 hmGpbList = (HashMap)alGpbDetails.get(i);
		String strGpbId = GmCommonClass.parseNull((String)hmGpbList.get("GPB_ID"));
		String strGpbName = GmCommonClass.parseNull((String)hmGpbList.get("GPB_NAME"));
		String strGpbFl = GmCommonClass.parseNull((String)hmGpbList.get("MAPPED_GPB"));
		 if(strGpbFl.equals("Y"))
		 {
		   dupChk = strGpbId+'|';
		 %>
		 <tr>
		 <td class ="Table_border" height="20" width ="30%"><%=strGpbName%><input type="hidden" id="hGpbId<%=j%>" value="<%=strGpbId%>" > 
		 </td><td class ="Table_border" height="20" width ="10%"  align ="center"><input type="checkbox" id="chkd<%=j%>" name="allGpbs" value="Y" checked></td>
		 </tr>
		 
		<%}
		 else
		 {%>
		 <tr>
		 <td class ="Table_border" height="20" width ="30%"><%=strGpbName%><input type="hidden" id="hGpbId<%=j%>" value="<%=strGpbId%>" > 
		 </td><td class ="Table_border" height="20" width ="10%"  align ="center"><input type="checkbox" id="chkd<%=j%>" name="allGpbs" value="Y" ></td>
		 </tr>
		<%}
	 }
}

%>
</tr>
</table>
<input type="hidden" id="dupChkVal" name="dupChkVal" value="<%=dupChk%>"/>


</td>
<td height="30" width = "30%"></td>
</tr>
</div>
<tr height="30">
<td align  = "center" colspan ="4"><gmjsp:button value="&nbsp;Save&nbsp;" style="margin-top:2px;height: 30px; width: 7em;" name="Btn_Submit" buttonType="Load" onClick="javascript:fnSave(this);"/>
<td align  = "center" colspan ="2"><gmjsp:button value="&nbsp;Save&nbsp;" style="margin-left:-35px;height: 30px; width: 7em;" name="Btn_Submit" buttonType="Load" onClick="javascript:fnSaveGpb(this);"/></td>

</td>
</tr>
<tr height="250">
<td align  = "center" colspan ="6"><gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_Close" gmClass="button"  style="margin: 10px 0px 0px 0px;height: 30px; width: 7em;" buttonType="Load" onClick="javaScript:fnClose();"/></td>
</tr>
 </table> 
 
 </html:form>
 </BODY>
 <%@ include file="/common/GmFooter.inc"%>
 </HTML>