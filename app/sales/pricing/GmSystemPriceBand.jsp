<%
/**********************************************************************************
 * File		 		: GmSystemPriceBand.jsp
 * Desc		 		: AD/VP Price Band for a system
 * Version	 		: 1.0
 * author			: Xun
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import="java.util.ArrayList,java.util.*" %>

<%@ taglib prefix="fmtSystemPriceBand" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmSystemPriceBand.jsp -->

<fmtSystemPriceBand:setLocale value="<%=strLocale%>"/>
<fmtSystemPriceBand:setBundle basename="properties.labels.sales.pricing.GmSystemPriceBand"/>


<% String strWikiTitle = GmCommonClass.getWikiTitle("OP_GROUP_PART_MAP");%>


<HTML>
<HEAD>
<TITLE> Globus Medical: Construct Builder </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_drag.js"></script>
 
<bean:define id="gridData" name="frmSystemPriceBand" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="enableADVPChange" name="frmSystemPriceBand" property="enableADVPChange" type="java.lang.String"> </bean:define> 

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/displaytag.css");
      div.gridbox_dhx_skyblue table.obj tr td{
     border-width:0px 1px 0px 0px;
     border-color : #A4BED4;
}
</style>
<script>
var arrGroupInp = new Array(1);
var mygrid;
 
function fnOnPageLoad() {

	var gridObjData = '<%=gridData%>';
	if(gridObjData!="")
	{
	   	mygrid = initMyGrid('dataGridDiv',gridObjData);
	   	mygrid.enableEditEvents(true, false, false);
	   	mygrid.attachEvent("onEditCell",doOnCellEdit);
	   
	 }

}

function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {		
	if(cellInd==1 || cellInd==2)
	{
		if (stage==1){
			var c=this.editor.obj;
			if(c!=null) c.select();
			return true;
		}		
		
		return true;
	}
	else 
	{
		return false;
	}
}


function fnSubmit() {

	mygrid.editStop();
	var gridrows =mygrid.getChangedRows(",");	
	if(gridrows!=''){	
	//{alert(' No change(s) to Update.');return;};
		var gridrowsarr = gridrows.toString().split(",");
		var arrLen = gridrowsarr.length;
		var isInValidPr =false;
	//	var rowIds = gridObj.getAllRowIds();
	//	var idsArr = rowIds.split(",");
	//	var arrLen = idsArr.length;
		var strInputString = '';
		for (var i=0;i<arrLen;i++)
		{
	        rowId=gridrowsarr[i];
	        systemid=mygrid.cellById(rowId,0).getValue();
			adp=mygrid.cellById(rowId,1).getValue();		
			vpp=mygrid.cellById(rowId,2).getValue();
	    
	        if(isNaN(adp) || isNaN(vpp))
			{
				isInValidPr=true;
				mygrid.setRowTextStyle(rowId,'background-color:#FF9595');
			}
	
		//	strInputString = strInputString+ systemid +'^'+ adp +'^'+ vpp +'|';
			strInputString = strInputString+ rowId +'^'+ adp +'^'+ '20179' +'|'+ rowId +'^'+ vpp +'^'+ '20180' +'|';
		}
	        if(isInValidPr)
		 	{
				alert(message_sales[196]);
				return;
			}
		   document.frmSystemPriceBand.hinputString.value =  strInputString;
	}	
	   strInputString = '';
	   var allgridrows =mygrid.getAllRowIds(",");
	   var allgridrowsarr = allgridrows.toString().split(",");
	   var arrAllLen = allgridrowsarr.length;
	
		for (i=0;i<arrAllLen;i++)
		{
	        rowId=allgridrowsarr[i];
	        //systemid=mygrid.cellById(rowId,0).getValue();	
			strInputString = strInputString+ rowId +'^'+ i +'|';
		}
	
	
	document.frmSystemPriceBand.hsystemPriorityString.value =  strInputString;
	document.frmSystemPriceBand.haction.value =  'save'; 
	fnStartProgress("Y");
	document.frmSystemPriceBand.submit();
}
function moveUp()
{	
	if(mygrid.getSelectedId()){
		mygrid.moveRow(mygrid.getSelectedId(),"up")
	}
}

function moveDown()
{
	if(mygrid.getSelectedId()){
		mygrid.moveRow(mygrid.getSelectedId(),"down")
	}
}
function initMyGrid(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");	
	gObj.enableDragAndDrop(true);
	gObj.init();		
	gObj.loadXMLString(gridData);		
	return gObj;
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad()">
<html:form action="/gmPriceBandSetup.do">
<html:hidden property="strOpt" />
<html:hidden property="haction" value=""/>
<html:hidden property="hinputString" value=""/>
<html:hidden property="hsystemPriorityString" value=""/> 

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtSystemPriceBand:message key="LBL_AD_VP_PRICE_BRAND_SETUP"/></td>
			<td align="right" class=RightDashBoardHeader > 	
				<fmtSystemPriceBand:message key="IMG_ALT_HELP" var = "varHelp"/>				
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
		</tr>
		<tr><td colspan="4" height="1" class="line"></td></tr>
		<tr>
			<td  valign="top" colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0"> 
				
				<tr><td align="left"><a href='#' onclick='moveUp()'><fmtSystemPriceBand:message key="IMG_ALT_INCREASE" var = "varIncrease"/>	<img width="16" height="16" alt="${varIncrease}" src="<%=strImagePath%>/arrow_up.gif" border="0"></a>
	&nbsp;<a href='#' onclick='moveDown()'><fmtSystemPriceBand:message key="IMG_ALT_DECREASE" var = "varDecrease"/>	<img width="16" height="16" alt="${varDecrease}" src="<%=strImagePath%>/arrow_down.gif" border="0"></a>
				</td></tr>
                    
			 	 <tr><td colspan="4" class="lline"></td></tr>
				   <tr>
			            <td colspan="4" valign="top">
			            <div id="dataGridDiv" style="height:500px;width:698px;"></div>
						</td>
	    	       </tr> 
    			 	<tr><td colspan="4" class="ELine"></td></tr> 
    			 	<% String strDisableBtnVal = enableADVPChange;
						if(strDisableBtnVal.equals("disabled")){
							strDisableBtnVal ="true";
						}
					 %>
              		 <tr height="30">
               	<td colspan="4" align="center">
                 <fmtSystemPriceBand:message key="BTN_UPDATE" var="varUpdate"/>
                 <gmjsp:button disabled="<%=strDisableBtnVal%>"  controlId="idSubmit" value="${varUpdate}" gmClass="button" buttonType="Save" onClick="fnSubmit();" />  
	             </td>
	               </tr>
	  		 	</table>
  			   </td>
  		  </tr>	
    </table>	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>

