 <%
/*******************************************************************************************************
 * File		 		: GmPartPriceAdjInitiate.jsp
 * Desc		 		: This JSP is like a container jsp for the screen Part Peice Adjustment Initiate,
 						to include the jsp for different sections
 * Version	 		: 1.0
 * author			: arajan
*********************************************************************************************************/
%>

<%@ page language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtPartPriceAdjInitiate" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- sales\pricing\GmPartPriceAdjInitiate.jsp -->

<fmtPartPriceAdjInitiate:setLocale value="<%=strLocale%>"/>
<fmtPartPriceAdjInitiate:setBundle basename="properties.labels.sales.pricing.GmPartPriceAdjInitiate"/>


<% 
String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");
response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8");
String strWikiTitle = GmCommonClass.getWikiTitle("PART_PRICE_ADJ_INIT");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: Part Price Adjustment Initiate</title>
<style type="text/css">
table.DtTable1050 {
	margin: 0 0 0 0;
	width: 1050px;
	border: 1px solid  #676767;
}
</style>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmPartPriceAdjInitiate.js"></script>
<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmPartPriceAdjFilter.js"></script>

<bean:define id="accId" name="frmPartPriceAdj" property="accId" type="java.lang.String"> </bean:define>

<script>
var prevAccId = '<%=accId%>';
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form action="/gmPartPriceAdjLoad.do?method=loadPartPriceDtls">
<html:hidden property="hInputString" name="frmPartPriceAdj" />
<html:hidden property="hRemString" name="frmPartPriceAdj" />
<html:hidden property="hSaveAction" name="frmPartPriceAdj" />
<html:hidden property="partyId" name="frmPartPriceAdj" />
<html:hidden property="hDisplayNm" name="frmPartPriceAdj" />
<html:hidden property="hRedirectURL" name="frmPartPriceAdj" />
	<table border="0" class="DtTable1050" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" width="90%">&nbsp;<fmtPartPriceAdjInitiate:message key="LBL_PART_PRICE_ADJUSTMENT"/></td>
			<td  align="right" class="RightDashBoardHeader" width="10%">
				<fmtPartPriceAdjInitiate:message key="IMG_ALT_HELP" var = "varHelp"/>
				<img align="right" id='imgEdit' style='cursor:pointer' src='<%=strImagePath%>/help.gif' title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"/>
			</td>
		</tr>
		<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
		<!-- To include the Filter Section-->
		<tr>
			<td colspan="2">
				<jsp:include page="/sales/pricing/GmPartPriceAdjFilter.jsp" >
				<jsp:param name="FORMNAME" value="frmPartPriceAdj" />
				</jsp:include>
			</td>
     	</tr>
     	<logic:notEmpty property="accId" name="frmPartPriceAdj">
	     	<!-- To include the Header Section-->
			<tr>
				<td colspan="2">
					<jsp:include page="/sales/pricing/GmPartPriceAdjHeader.jsp" >
					<jsp:param name="FORMNAME" value="frmPartPriceAdj" />
					</jsp:include>
				</td>
	     	</tr>
	     	<!-- To include the Grid Section-->
			<tr>
				<td colspan="2">
					<jsp:include page="/sales/pricing/GmPartPriceAdjDtls.jsp" >
					<jsp:param name="FORMNAME" value="frmPartPriceAdj" />
					</jsp:include>
				</td>
	     	</tr>
     	</logic:notEmpty>
	</table>
<%@ include file="/common/GmFooter.inc"%>
</html:form>
</body>
</html>