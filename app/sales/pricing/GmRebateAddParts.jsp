<%
	/**********************************************************************************
	 * File		 		: GmRebateAddPart.jsp
	 * Desc		 		: This screen is used to Add parts for Rebate
	 * Version	 		: 
	 * author			: Prabhu Vigneshwaran M D
	 
	 *****************************************************************************/
%>
 <!-- \sales\pricingGmRebateAddParts.jsp -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import = "java.util.*,java.io.*"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="fmtrebateAddParts" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%>
<%@ page isELIgnored="false" %>
<fmtrebateAddParts:setBundle basename="properties.labels.sales.pricing.GmRebateAddParts" />
<bean:define id="accessFl" name="frmRebateSetup" property="accessFl" type="java.lang.String"></bean:define>
<% 
String strWikiTitle = GmCommonClass.getWikiTitle("REBATE_ADD_NEW_PARTS"); 
String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");
GmCalenderOperations.setTimeZone(strGCompTimeZone);
String strTodaysDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt);
String strApplDateFmt = strGCompDateFmt;
String strSubmitDisable="true";	
if(accessFl.equals("Y")){
	strSubmitDisable="false";
}
%>  

<HTML>
<HEAD>
<title>GlobusOne Enterprise Portal: Rebate Add New Parts Setup screen</title>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmRebateAddParts.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script>
var today_dt = '<%=strTodaysDate%>';
var format = '<%=strApplDateFmt%>';
</script>
<BODY leftmargin="20" topmargin="10">
<html:form action="/gmRebateSetup.do?method=loadRebateMappingDtls">
<html:hidden property="hcontractFromDate" name="frmRebateSetup"/>
<html:hidden property="hcontractToDate" name="frmRebateSetup"/>
<html:hidden property="hRebateId" name="frmRebateSetup"/>
<html:hidden property="hAccName" name="frmRebateSetup"/>
<html:hidden property="hpartyName" name="frmRebateSetup"/>
<html:hidden property="partInputString"name="frmRebateSetup" />
<html:hidden property="strOpt" />
<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
	<tr>
	<td colspan="2" height="25" class="RightDashBoardHeader">&nbsp;<fmtrebateAddParts:message
						key="LBL_REBATE_ADD_PARTS" /></td>
	<td align="right" class="RightDashBoardHeader">&nbsp;<img
					id='imgEdit' align="right" style='cursor: hand'
					src='<%=strImagePath%>/help.gif' title='Help' width='16'
					height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	</td>			
	</tr>
	        <tr>
				<td class="LLine" colspan="3" height="1"></td>
			</tr>	
	<tr>
			<td height="25" width="100"  class="RightTableCaption" align="right">&nbsp;<fmtrebateAddParts:message
						key="LBL_ACCOUNT_TYPE" var="varAccounType" /><gmjsp:label
				type="RegularText" SFLblControlName="${varAccounType}:" td="false" />
				<td colspan="2">&nbsp;&nbsp;<bean:write
				property="haccountTypeName" name="frmRebateSetup"/></td>
	</tr>
	
	        <tr>
				<td class="LLine" colspan="3" height="1"></td>
			</tr>
	
	<tr class="Shade">
		
				<td height="25" class="RightTableCaption" align="right">&nbsp;<fmtrebateAddParts:message
						key="LBL_NAME" var="varName" /><gmjsp:label
				type="RegularText" SFLblControlName="${varName}:" td="false" />
				</td>
				<td colspan="2">&nbsp;&nbsp;<bean:write
				property="searchpartyId" name="frmRebateSetup" /></td>
					
				</tr>
			
		    <tr>
				<td class="LLine" colspan="3" height="1"></td>
			</tr>
			
		
	      <tr>
				<td height="25" class="RightTableCaption" align="Right">&nbsp;<fmtrebateAddParts:message
						key="LBL_EFFECTIVE_DATE" var="varEffectiveDate" /><gmjsp:label
							type="RegularText" SFLblControlName="${varEffectiveDate}:" td="false" /></td>
				<td colspan="2">&nbsp;&nbsp;<bean:write
						property="rebateEffectiveDate" name="frmRebateSetup" />-<bean:write
						property="rebateToDate" name="frmRebateSetup" /></td>
	    </tr>
			 <tr>
				<td class="LLine" colspan="3" height="1"></td>
			</tr>
			
			
			
			    <tr class="Shade">
				<td class="RightTableCaption" align="Right">&nbsp;<font color="red">*</font>
					<fmtrebateAddParts:message
						key="LBL_PART_NUMBER"/>:</td>
				<td colspan="2" valign="absmiddle">&nbsp;
				<textarea name="batchPartNum"  id="batchPartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
		          onBlur="changeBgColor(this,'#ffffff');"  rows=20 cols=40 ></textarea>
				</td>
					
			</tr>
		
		
			
			 <tr>
				<td class="LLine" colspan="3" height="1"></td>
			</tr>
			
			
			<tr>
			<td class="RightTableCaption" align="right" height="25">&nbsp;<font color="red">*</font>&nbsp;<fmtrebateAddParts:message
						key="LBL_EFFECTIVE_DATE"/>:</td>
			<td colspan="2">&nbsp;&nbsp;<gmjsp:calendar SFFormName="frmRebateSetup"
						controlName="effectivePartDate" gmClass="InputArea"
						onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" tabIndex="" />
			</td>
			
		    </tr>
		

	
		     <tr>
				<td class="LLine" colspan="3" height="1"></td>
			</tr>
				
			<tr>
			<td colspan="3">
				<jsp:include page="/common/GmIncludeLog.jsp" >
				<jsp:param name="FORMNAME" value="frmRebateSetup" />
					<jsp:param name="ALNAME" value="alLogReasons" />
					<jsp:param name="LogMode" value="txt_LogReason" />
					<jsp:param name="Mandatory" value="yes" />
					<jsp:param name="TabIndex" value="13" />
				</jsp:include>
			</td>	
			
			</tr>
			<tr>
				<td class="LLine" colspan="3" height="1"></td>
			</tr>
		
			<tr>
			 <td colspan="3"  align="center" height="50">
				 <gmjsp:button name="BTN_SUBMIT" value="Submit" gmClass="button"  buttonType="Save" onClick="fnSubmit()" disabled="<%=strSubmitDisable%>" tabindex="6" />
                 <gmjsp:button name="BTN_RESET" value="Reset" gmClass="button" onClick="fnReset()" buttonType="Reset" tabindex="6" />
			 </td>
			</tr>
			<tr>
				<td class="LLine" colspan="3" height="1"></td>
			</tr>
			</tr>
			
		</table>
</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>

