<%
/**********************************************************************************
 * File		 		: GmMultiPartGroup.jsp
 * Desc		 		: display Part belong to Multiple Groups info
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ taglib prefix="fmtMultiPartGroup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmMultiPartGroup.jsp -->

<fmtMultiPartGroup:setLocale value="<%=strLocale%>"/>
<fmtMultiPartGroup:setBundle basename="properties.labels.sales.pricing.GmMultiPartGroup"/>
  
<bean:define id="ldtResult" name="frmGroupPartMapping" property="ldtResult" type="java.util.List"></bean:define>
 
<HTML>
<HEAD>
<TITLE> Globus Medical: Multiple Part Group </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
  

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

 

</HEAD>

<BODY leftmargin="20" topmargin="10">
 
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0"> 
		<tr><td colspan="6" height="1" class="line"></td></tr>
		
		 <tr>
	                     <td colspan="6">
	                     	<display:table  name="requestScope.frmGroupPartMapping.ldtResult" class="its" id="currentRowObject" requestURI="/gmMultiPartGroup.do"  > 
							<fmtMultiPartGroup:message key="DT_PART_NUMBER" var="varPartNumber"/>
							<display:column property="PNUM" title="${varPartNumber}"  group="1"  class="alignleft"/>
							<fmtMultiPartGroup:message key="DT_GROUP_NAME" var="varGroupName"/>
							<display:column property="GROUPNAME" title="${varGroupName}"  group="2"  class="alignleft"/> 
							<fmtMultiPartGroup:message key="DT_PRI_SEC" var="varPriSec"/>
							<display:column property="PARTTYPE" title="${varPriSec}"    class="alignleft"/>
							<fmtMultiPartGroup:message key="DT_LIST_PRICE" var="varListPrice"/>
							<display:column property="LISTPRICE" title="${varListPrice}"    class="alignright"/>
							<fmtMultiPartGroup:message key="DT_AD_LIMIT" var="varADLimit"/>
							<display:column property="ADLIMIT" title="${varADLimit}"    class="alignright"/>
							<fmtMultiPartGroup:message key="DT_VP_LIMIT" var="varVPLimit"/>
							<display:column property="VPLIMIT" title="${varVPLimit}"    class="alignright"/> 							
							<fmtMultiPartGroup:message key="DT_TRIP_WIRE" var="varTripWire"/>
							<display:column property="TRIPWIRE" title="${varTripWire}"    class="alignright"/> 
							
							</display:table>

    		             </td>
                    </tr> 
        
		 
    </table>		     	
 
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

