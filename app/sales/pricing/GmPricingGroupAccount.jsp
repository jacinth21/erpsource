<%@ include file="/common/GmHeader.inc"%>

<%@ page import="java.util.ArrayList,java.util.*"%>

<%@ taglib prefix="fmtPricingGroupAccount" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmPricingGroupAccount.jsp -->

<fmtPricingGroupAccount:setLocale value="<%=strLocale%>"/>
<fmtPricingGroupAccount:setBundle basename="properties.labels.sales.pricing.GmPricingGroupAccount"/>

<% String strWikiTitle = GmCommonClass.getWikiTitle("OP_GROUP_PART_MAP");
String strGroupData = GmCommonClass.parseNull((String)request.getAttribute("GroupData"));
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Pricing Group Account Details</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strExtWebPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>

<style type="text/css" media="all">
@import url("<%=strCssPath%>/displaytag.css");

.grid_hover {
	background-color: #E6E6FA;
	font-size: 20px;
}

div.gridbox_dhx_skyblue table.obj tr td {
	border-width: 0px 1px 0px 0px;
	border-color: #A4BED4;
}
</style>

<script type="text/javascript">

//MNTTASK-3518 - Stack Over Flow
var split_At = '';
var w1,dhxWins;
// End Code MNTTASK-3518 - Stack Over Flow

function fnOnPageLoad() {
	var gridGroupAccountDetails = '<%=strGroupData%>';
	
	if (gridGroupAccountDetails != ''){
		gridObj = initGrid('dataGridDiv',gridGroupAccountDetails);
		gridObj.enableTooltips("true,true,true,false,false,true,true,true");
	}
	
}
function fnClose()
{ 	
	CloseDhtmlxWindow();
	return false;
}

// MNTTASK-3518 - Stack Over Flow 

function setDefalutGridProperties(divRef,footerArry)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	if(footerArry != undefined && footerArry.length >0)
	gObj.attachFooter(footerArry);
	gObj.enableMultiline(true);	
	return gObj;
}

function initializeDefaultGrid(gObj, gridData, split_At)
{	
	gObj.init();
	if(split_At !='' || split_At != 'undefined')
	{
		gObj.splitAt(split_At);
	}
	gObj.loadXMLString(gridData);	
	return gObj;
}

function initGrid(divRef,gridData,split_At,footerArry){
	var gObj = setDefalutGridProperties(divRef,footerArry);
	return initializeDefaultGrid(gObj,gridData,split_At);
}
// End code MNTTASK-3518 - Stack Over Flow

</script>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad()">
<html:form action="/gmPricingRequestDAction.do?method=GroupAccountDetails">

	<table border="0" class="DtTable900" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtPricingGroupAccount:message key="LBL_GRP_ACC_DETAILS"/></td>
			</tr>
			
			
			<tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>
			
			
			<tr id="gridData"> 
			
				<td><div id="dataGridDiv" style="grid" height="300px" width="870px">&nbsp;</div></td> 
			</tr>
			<tr>
			<td height="50" align="center"><fmtPricingGroupAccount:message key="BTN_CLOSE" var="varClose"/><gmjsp:button style="width: 8em; height: 2em; font-size: 13px" name="Close_button" value="${varClose}" buttonType="Load" gmClass="button" onClick="fnClose();" /> </td>
			</tr>
			</table>
			</html:form>
</BODY>
</HTML>

