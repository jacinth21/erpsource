 <%
/*******************************************************************************************************
 * File		 		: GmPartPriceAdjHeader.jsp
 * Desc		 		: This JSP is to show the header part in Part Price adjustment screen
 * Version	 		: 1.0
 * author			: arajan
*********************************************************************************************************/
%>

<%@ page language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Date" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtPartPriceAdjHeader" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmPartPriceAdjHeader.jsp -->

<fmtPartPriceAdjHeader:setLocale value="<%=strLocale%>"/>
<fmtPartPriceAdjHeader:setBundle basename="properties.labels.sales.pricing.GmPartPriceAdjHeader"/>
<%
String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
 
String strTimeZone = strGCompTimeZone; 
String strDTDateFmt = "{0,date,"+strGCompTimeZone+"}"; 

Date dtimpDate = (Date) request.getAttribute("IMPDATE");
Date dtiniDate = (Date) request.getAttribute("INITDATE"); 
 
%>
 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: Part Price Adjustment Initiate</title>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmPartPriceAdjInitiate.js"></script>

</head>
<body leftmargin="20" topmargin="10">
<html:hidden property="hTxnId" name="<%=strFormName %>" />
<html:hidden property="txnId" name="<%=strFormName %>" />
<html:hidden property="prid" name="<%=strFormName %>" />
<html:hidden property="statusid" name="<%=strFormName %>" />

	<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr><td colspan="9" height="1" bgcolor="#cccccc"></td></tr>
		<tr class="Shade">
			<td height="24" class="RightTableCaption" align="right" width="15%"><fmtPartPriceAdjHeader:message key="LBL_STATUS" var="varStatus"/><gmjsp:label type="RegularText" SFLblControlName="${varStatus}" td="false" /> </td>
			<td width="15%">&nbsp;<bean:write name="<%=strFormName %>" property="status"/></td>
			
			<td height="24" class="RightTableCaption" align="right" width="15%"><fmtPartPriceAdjHeader:message key="LBL_IMPLEMENTED_DATE" var="varImpDate"/><gmjsp:label type="RegularText" SFLblControlName="${varImpDate}" td="false" /> </td>
			<td width="15%">&nbsp; <%=GmCommonClass.getStringFromDate(dtimpDate, strGCompDateFmt )%></td>
		
			<td height="24" class="RightTableCaption" align="right" width="15%"><fmtPartPriceAdjHeader:message key="LBL_REGION" var="varRegion"/><gmjsp:label type="RegularText" SFLblControlName="${varRegion}" td="false" /> </td>
			<td width="25%">&nbsp;<bean:write name="<%=strFormName %>" property="rgname"/></td>
		</tr>
		<tr><td colspan="9" height="1" bgcolor="#cccccc"></td></tr>
		<tr>
			<td height="24" class="RightTableCaption" align="right"><fmtPartPriceAdjHeader:message key="LBL_TRANSACTION_ID" var="varTransId"/><gmjsp:label type="RegularText" SFLblControlName="${varTransId}" td="false" /> </td>
			<td>&nbsp;<bean:write name="<%=strFormName %>" property="txnId"/></td>
			
			<td height="24" class="RightTableCaption" align="right"><fmtPartPriceAdjHeader:message key="LBL_CONTRACT" var="varContract"/><gmjsp:label type="RegularText" SFLblControlName="${varContract}" td="false" /> </td>
			<td>&nbsp;<bean:write name="<%=strFormName %>" property="contract"/></td>
		
			<td height="24" class="RightTableCaption" align="right"><fmtPartPriceAdjHeader:message key="LBL_TERRITORY" var="varTerritory"/><gmjsp:label type="RegularText" SFLblControlName="${varTerritory}" td="false" /> </td>
			<td>&nbsp;<bean:write name="<%=strFormName %>" property="trname"/></td>
		</tr>
		<tr><td colspan="9" height="1" bgcolor="#cccccc"></td></tr>
		<tr class="Shade">
			<td height="24" class="RightTableCaption" align="right"><fmtPartPriceAdjHeader:message key="LBL_INITIATED_BY" var="varInitiatedBy"/><gmjsp:label type="RegularText" SFLblControlName="${varInitiatedBy}" td="false" /> </td>
			<td>&nbsp;<bean:write name="<%=strFormName %>" property="initiatedBy"/></td>
			
			<td height="24" class="RightTableCaption" align="right"><fmtPartPriceAdjHeader:message key="LBL_IDN" var="varIdn"/><gmjsp:label type="RegularText" SFLblControlName="${varIdn}" td="false" /> </td>
			<td>&nbsp;<bean:write name="<%=strFormName %>" property="gpraff"/></td>
		
			<td height="24" class="RightTableCaption" align="right"><fmtPartPriceAdjHeader:message key="LBL_AREA_DIRECTOR" var="varAreaDir"/><gmjsp:label type="RegularText" SFLblControlName="${varAreaDir}:" td="false" /> </td>
			<td>&nbsp;<bean:write name="<%=strFormName %>" property="adname"/></td>
		</tr>
		<tr><td colspan="9" height="1" bgcolor="#cccccc"></td></tr>
		<tr>
			<td height="24" class="RightTableCaption" align="right"><fmtPartPriceAdjHeader:message key="LBL_INITIATED_DATE" var="varIniDate"/><gmjsp:label type="RegularText" SFLblControlName="${varIniDate}" td="false" /> </td>
			<td>&nbsp; <%=GmCommonClass.getStringFromDate(dtiniDate, strGCompDateFmt )%></td>
			
			<td height="24" class="RightTableCaption" align="right"><fmtPartPriceAdjHeader:message key="LBL_GRP_PRICE_BOOK" var="varPriBook"/><gmjsp:label type="RegularText" SFLblControlName="${varPriBook}" td="false" /> </td>
			<td>&nbsp;<bean:write name="<%=strFormName %>" property="prName"/></td>
		
			<td height="24" class="RightTableCaption" align="right"><fmtPartPriceAdjHeader:message key="LBL_REP" var="varRep"/><gmjsp:label type="RegularText" SFLblControlName="${varRep}" td="false" /> </td>
			<td>&nbsp;<bean:write name="<%=strFormName %>" property="rpname"/></td>
		</tr>
		<tr><td colspan="9" height="1" bgcolor="#cccccc"></td></tr>
		<tr class="Shade">
			<td height="24" class="RightTableCaption" align="right"><fmtPartPriceAdjHeader:message key="LBL_IMPLEMENTED_BY" var="varImpBy"/><gmjsp:label type="RegularText" SFLblControlName="${varImpBy}" td="false" /> </td>
			<td>&nbsp;<bean:write name="<%=strFormName %>" property="implementedBy"/></td>
			
			<td height="24" class="RightTableCaption" align="right"><fmtPartPriceAdjHeader:message key="LBL_GPO" var="varGpo"/><gmjsp:label type="RegularText" SFLblControlName="${varGpo}" td="false" /> </td>
			<td>&nbsp;<bean:write name="<%=strFormName %>" property="hlcaff"/></td>
		
			<td height="24" class="RightTableCaption" align="right"><fmtPartPriceAdjHeader:message key="LBL_DISTRIBUTOR" var="varDistributor"/><gmjsp:label type="RegularText" SFLblControlName="${varDistributor}" td="false" /> </td>
			<td>&nbsp;<bean:write name="<%=strFormName %>" property="dname"/></td>
		</tr>
		<tr><td colspan="9" height="1" bgcolor="#cccccc"></td></tr>
		
	</table>
<%@ include file="/common/GmFooter.inc"%>

</body>
</html>