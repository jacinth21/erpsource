<%
/**********************************************************************************
 * File		 		: GmGroupPartDetail.jsp 
 * Desc		 		: display Group part detail
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<!-- Removed taglib, since GmHeader.inc is included -->
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtGroupPartDetail" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmGroupPartDetail.jsp -->

<fmtGroupPartDetail:setLocale value="<%=strLocale%>"/>
<fmtGroupPartDetail:setBundle basename="properties.labels.sales.pricing.GmGroupPartDetail"/>

<bean:define id="groupId" name="frmGroupPartMapping" property="groupId" type="java.lang.String"></bean:define>  
<bean:define id="strHPartPriceFl" name="frmGroupPartMapping" property="strHPartPriceFl" type="java.lang.String"></bean:define>
<bean:define id="groupNM" name="frmGroupPartMapping" property="groupNM" type="java.lang.String"></bean:define>
<bean:define id="strDisableBtn" name="frmGroupPartMapping" property="strDisableBtn" type="java.lang.String"></bean:define>  
 <%  
 String strProdmgntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
ArrayList alReturn = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("RDGROUPPARTMAP"));
int intParts = alReturn.size();
%>
 
<HTML>
<HEAD>
<TITLE> Globus Medical: Multiple Part Group </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
 <script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script> 
<script language="JavaScript" src="<%=strProdmgntJsPath%>/GmGroupPartDetail.js"></script> 

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
var groupid = <%=groupId%>; 

var rowsize = <%=intParts%>;  
var disableBtn = '<%=strDisableBtn%>';
 
 
</script>
</HEAD>

<BODY  onload="fnCheckSelections();">
<html:form> 
<html:hidden property="haction" />

<html:hidden property="hpnum" />
<html:hidden property="groupNM" />
<html:hidden property="hviewEdit" />

	<table border="0"  class="DtTable725" cellspacing="0" cellpadding="0">
	
	<logic:equal  name="frmGroupPartMapping" property="hviewEdit" value="view">
		<tr>
			<td colspan="4" height="25" class="RightDashBoardHeader"><fmtGroupPartDetail:message key="LBL_GROUP_PART_MAPPING"/>
		<!--  	  <bean:write name="frmGroupPartMapping" property="groupNM"></bean:write> -->
			</td> 
			
		</tr>
		
	</logic:equal>	
	  <logic:notEqual  name="frmGroupPartMapping" property="hviewEdit" value="view">
             <tr class="ShadeRightTableCaption">
						<td Height="24" colspan="3">&nbsp;<fmtGroupPartDetail:message key="LBL_GROUP_PART_DETAIL"/></td>
					</tr>
             <tr><td colspan="4" class="Line"></td></tr>
                    <tr>
                    	<td>
                    		<table> <!-- Changed for allignment issue -->
                    			<tr>
			                        <td class="RightTableCaption" align="right" HEIGHT="24" width="15%">&nbsp;<fmtGroupPartDetail:message key="LBL_PART_NUMBERS"/>:</td>
								<td>&nbsp;<html:text property="partNumbers" size="50"
										onfocus="changeBgColor(this,'#AACCE8');"
										styleClass="InputArea"
										onkeypress="javascript:fnChangeFocus('submit');"
										onblur="changeBgColor(this,'#ffffff');" />&nbsp;<a
									title="You can perform wildcard search. For eg: type 101 to pull up all parts that have 101 in it"><img	src=<%=strImagePath%>/question.gif border=0</a>
								</td>
								<td width="30%">    
				                        <fmtGroupPartDetail:message key="BTN_LOAD" var="varLoad"/>
				                        <gmjsp:button style="width: 5em; height: 2em; font-size: 13px" value="${varLoad}" gmClass="button"  buttonType="Load" onClick="fnLoad(this.form);" />&nbsp;&nbsp;&nbsp;&nbsp;
											<A class="RightText" title="Click to see all group part history" href="javascript:fnAllPartHistory()"><fmtGroupPartDetail:message key="LBL_VIEW_ALL_PART_HISTORY"/></a>	
			                        </td>
			                    </tr>
	                        </table>
                        </td>
                    </tr>    </logic:notEqual> 
           <tr><td colspan="4" height="1" class="line"></td></tr>          
            <tr>      
             
            <td colspan="4">
          	<!-- Changed paneSize for allignment issue -->
            <display:table name="RDGROUPPARTMAP" requestURI="/gmGroupPartMap.do" excludedParams="haction" class="its" id="currentRowObject" freezeHeader="true" paneSize="240" decorator="com.globus.displaytag.beans.DTGroupPartMappingWrapper"> 
			 <logic:notEqual  name="frmGroupPartMapping" property="hviewEdit" value="view">
			<display:column media="html"  title="<input type='checkbox' name='selectAll' onclick='fnSelectAll();' />" style="width:5px" >  
						<htmlel:multibox property="checkPartNumbers" styleId="checkPartNumbers"  value="${currentRowObject.PNUM}" > </htmlel:multibox>  
			 </display:column>
			 </logic:notEqual>
			 <logic:equal  name="frmGroupPartMapping" property="hviewEdit" value="view">
			<fmtGroupPartDetail:message key="DT_GROUP_NAME" var="varGrpName"/> 
			<display:column property="GROUPNM" title="${varGrpName}" class="alignleft" group="1"  >						 
			 </display:column>
			 </logic:equal> 
			 <fmtGroupPartDetail:message key="DT_PART_NUMBER" var="varPartNumber"/>
			 <display:column property="PNUM" title="${varPartNumber}" sortable="true" class="alignleft" style="width:100px" /> 
			  <logic:notEqual  name="frmGroupPartMapping" property="hviewEdit" value="view">
			 <fmtGroupPartDetail:message key="DT_PRIMARY" var="varPrimary"/>
			 <display:column property="PRICING_TYPE" title=" ${varPrimary}&nbsp;" class="aligncenter" style="width:5px" >						 
			 </display:column>
			 </logic:notEqual>
			 
			 <logic:notEqual  name="frmGroupPartMapping" property="hviewEdit" value="view">
			 <fmtGroupPartDetail:message key="DT_PRIMARY_LOCK" var="varPrimaryLock"/>
			 <display:column property="PRIMARYLOCK" title=" ${varPrimaryLock}" class="aligncenter" style="width:5px" >						 
			 </display:column>
			 </logic:notEqual>
			 <!-- PMT-758 - When click P icon in Group Part Pricing screen, should be display List & TW price. -->
			 <fmtGroupPartDetail:message key="DT_PART_DESCRIPTION" var="varPartDesc"/>
			 <display:column property="PDESC" title="${varPartDesc}" sortable="true" class="alignleft"/> 
			 <logic:equal  name="frmGroupPartMapping" property="strHPartPriceFl" value="Y">
			<fmtGroupPartDetail:message key="DT_LIST_PRICE" var="varListPrice"/>
			 <display:column property="LISTP" title="${varListPrice}" sortable="true" class="alignright" style="width:100px" />
			<fmtGroupPartDetail:message key="DT_TRIPWIRE" var="varTripwire"/>
			 <display:column property="TW" title="${varTripwire}" sortable="true" class="alignright" style="width:100px"/>						 
			 </logic:equal>
			</display:table>  
			  
			</td>
			</tr>
		<%if(!strDisableBtn.equals("Y")) {%><!-- To disable the button, if calling from Group Part Mapping screen[not a popup window]  -->
	     	<tr><td class="LLine" height="1" colspan="4"></td></tr>
			<tr>
			    <td align="center" colspan="4" class="RightRedCaption">
           			<fmtGroupPartDetail:message key="BTN_CLOSE" var="varClose"/>
           			<gmjsp:button style="width: 8em; height: 2em; font-size: 13px" value="${varClose}" gmClass="button" buttonType="Load" onClick="fnClose();"/>&nbsp;
			    </td>			                				                    
		    </tr>
		<%} %>
    </table>		     	
 </html:form>
 <%@ include file="/common/GmFooter.inc"%> 
</BODY>
</HTML>

