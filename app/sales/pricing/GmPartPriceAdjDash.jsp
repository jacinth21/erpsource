 <%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%
/*******************************************************************************************************
 * File		 		: GmPartPriceAdjInitiate.jsp
 * Desc		 		: This JSP is like a container jsp for the screen Part Peice Adjustment Initiate,
 						to include the jsp for different sections
 * Version	 		: 1.0
 * author			: arajan
*********************************************************************************************************/
%>

<%@ page language="java"%>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtPartPriceAdjDash" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmPartPriceAdjDash.jsp -->

<fmtPartPriceAdjDash:setLocale value="<%=strLocale%>"/>
<fmtPartPriceAdjDash:setBundle basename="properties.labels.sales.pricing.GmPartPriceAdjDash"/>

<bean:define id="alStatus" name="frmPartPriceAdj" property="alStatus" type="java.util.ArrayList"></bean:define>
<bean:define id="status" name="frmPartPriceAdj" property="status" type="java.lang.String"></bean:define>
<bean:define id="successMessage" name="frmPartPriceAdj" property="successMessage" type="java.lang.String"></bean:define>


<%    
response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8");
String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");
String strWikiTitle = GmCommonClass.getWikiTitle("PART_PRICE_ADJ_DASH");
String strSessApplDateFmt = strGCompDateFmt;
String strTodaysDate = GmCalenderOperations.getCurrentDate(strSessApplDateFmt);
HashMap hmQueryValues = new HashMap();
hmQueryValues.put("ID","CODEID");
hmQueryValues.put("PID","CODEID");
hmQueryValues.put("NM","CODENM");	
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: Part Price Adjustment Dashboard</title>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<style type="text/css">
table.DtTable1030 {
	margin: 0 0 0 0;
	width: 1030px;
	border: 1px solid  #676767;
}
</style>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmPartPriceAdjDash.js"></script>
<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmPartPriceAdjFilter.js"></script>
<script>
var format = "<%=strSessApplDateFmt%>";
var todaysDate 	= '<%=strTodaysDate%>';
var statusString = '<%=status%>';
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnPageLoad();">
<html:form action="/gmPartPriceAdjDash.do?method=loadPartPriceAdjDtls">
<html:hidden property="hInputString" name="frmPartPriceAdj" />
<html:hidden property="hSaveAction" name="frmPartPriceAdj" />
<html:hidden property="partyId" name="frmPartPriceAdj" />
<html:hidden property="strOpt" name="frmPartPriceAdj" />
<html:hidden property="hStatus" name="frmPartPriceAdj" value="<%=status%>" />
	<table border="0" class="DtTable1030" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" width="90%">&nbsp;<fmtPartPriceAdjDash:message key="LBL_PART_PRICE_ADJUSTMENT"/></td>
			<td  align="right" class="RightDashBoardHeader" width="10%">
				<fmtPartPriceAdjDash:message key="IMG_ALT_HELP" var = "varHelp"/>
				<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"/>
			</td>
		</tr>
		<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
		<!-- To include the Filter Section-->
		<tr>
			<td colspan="2">
				<jsp:include page="/sales/pricing/GmPartPriceAdjFilter.jsp" >
				<jsp:param name="FORMNAME" value="frmPartPriceAdj" />
				<jsp:param name="SCREEN" value="Dashboard" />
				</jsp:include>
			</td>
     	</tr>
     	<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>     	
     	<tr><td colspan="2">
     	<table border="0" width="100%" cellspacing="0" cellpadding="0">
	     	<tr class="Shade" height="25">
	     		<td height="25" class="RightTableCaption" align="right" width="8%"><fmtPartPriceAdjDash:message key="LBL_INITIATED_BY" var="varInitiatedBy"/><gmjsp:label type="RegularText" SFLblControlName="${varInitiatedBy}" td="false" /></td>
				<td width="24%">&nbsp;<gmjsp:dropdown controlName="initBy" SFFormName="frmPartPriceAdj" SFSeletedValue="initBy" SFValue="alInitBy" codeId="ID" codeName="NAME" defaultValue="[Choose One]"/></td>
				<td width="20%"><B>&nbsp;<gmjsp:dropdown boldValues="true" controlName="dateFld" SFFormName="frmPartPriceAdj" SFSeletedValue="dateFld" SFValue="alDateFld" codeId="CODEID" codeName="CODENM"/></B></td>
				<td width="38%"><b><gmjsp:calendar SFFormName="frmPartPriceAdj" controlName="startDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="10"/></b>
				&nbsp;<gmjsp:calendar SFFormName="frmPartPriceAdj" controlName="endDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="10"/></td>
				<td height="30" align="left">&nbsp;<fmtPartPriceAdjDash:message key="BTN_LOAD" var="varLoad"/><gmjsp:button name="Btn_Load" value="${varLoad}" gmClass="button" style="width:4em"  onClick="javascript:fnLoadDash();" tabindex="2" buttonType="Load" />
				</td>				
	     	</tr>
	     	<tr><td colspan="8" height="1" bgcolor="#cccccc"></td></tr>
	     	<tr>
		     	<td class="RightTableCaption" align="right" colspan="2"><fmtPartPriceAdjDash:message key="LBL_STATUS" var="varStatus"/><gmjsp:label type="RegularText" SFLblControlName="${varStatus}" td="false" /></td>
		     	<td>
		     		<table border="0" width="100%" cellspacing="0" cellpadding="0">
		     			<tr></tr>		     			
		     			<tr>
		     				<td width="20%"></td>
		     				<td style="height: 50px"><%=GmCommonControls.getChkBoxGroup("fnSelectInv()",alStatus,"Status",hmQueryValues,"")%></td>
		     			</tr>
		     		</table>
		     	</td>	     	
	     	</tr>
	     	<tr></tr>
     	</table>
     	</td></tr>
     	<!-- To include the Header Section-->
		<tr>
			<td colspan="2">
				<jsp:include page="/sales/pricing/GmPartPriceAdjDetails.jsp" >
				<jsp:param name="FORMNAME" value="frmPartPriceAdj" />
				</jsp:include>
			</td>
     	</tr>
	</table>
<%@ include file="/common/GmFooter.inc"%>
</html:form>
</body>
</html>