

<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>

<%@ taglib prefix="fmtUnPricingSystem" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmUnPricingSystem.jsp -->

<fmtUnPricingSystem:setLocale value="<%=strLocale%>"/>
<fmtUnPricingSystem:setBundle basename="properties.labels.sales.pricing.GmUnPricingSystem"/>

<bean:define id="gridPendingSystemAccountPrice" name="frmPricingRequestForm" property="pendingSystemAccountPriceGridXML" type="java.lang.String"></bean:define>
<bean:define id="pendingSystemAccountCount" name="frmPricingRequestForm" property="pendingSystemAccountCount" type="java.lang.String"></bean:define>
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("PRICING_STATUS_REPORT");
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Pricing- Unpriced System Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>



<style type="text/css" media="all">
@import url("<%=strCssPath%>/displaytag.css");

.grid_hover {
	background-color: #E6E6FA;
	font-size: 20px;
}

div.gridbox_dhx_skyblue table.obj tr td {
	border-width: 0px 1px 0px 0px;
	border-color: #A4BED4;
}
</style>

<script type="text/javascript">
function fnOnPageLoad() {		
	var gridPendingSystemAccountPriceData = '<%=gridPendingSystemAccountPrice%>';
	
	if (gridPendingSystemAccountPriceData != ''){
		
		gridObj = initGrid('dataGridDiv',gridPendingSystemAccountPriceData);
		gridObj.enableTooltips("true,true,true");
	}
	
}
/*function fnOpenSystemPricingInitiate(reqid,accid,systemid,groupid){
	window.opener.fnAjaxPopUpRst(reqid,accid,systemid,groupid);
	window.close();
	}
*/
function fnOpenSystemPricingInitiate(reqid,accid,systemid,groupid){
	window.parent.fnAjaxPopUpRst(reqid,accid,systemid,groupid);
	CloseDhtmlxWindow();
	return false;
	}

function fnClose()
{ 	
	CloseDhtmlxWindow();
	return false;
}
</script>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<html:form action="/gmPricingRequestDAction.do?method=UnPricingSystemReport">

	<html:hidden property="strOpt" />

	<table border="0" class="DtTable500" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><%=pendingSystemAccountCount %></td>
			</tr>
			
			
			<tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>
			
			
			<tr id="gridData"> 
			
				<td><div id="dataGridDiv" style="grid" height="550px" width="650">&nbsp;</div></td> 
			</tr>
			<tr>
			<td height="50" align="center"><fmtUnPricingSystem:message key="BTN_CLOSE" var="varClose"/><gmjsp:button style="width: 8em; height: 2em; font-size: 13px" name="Close_button" buttonType="Load" value="${varClose}" gmClass="button" onClick="fnClose();" /> </td>
			</tr>
			</table>
			</html:form>
</BODY>
</HTML>
			
