 <%
/*******************************************************************************************************
 * File		 		: GmPartPriceAdjDetails.jsp
 * Desc		 		: This JSP an included jsp to show the detiails section of Part Price Adjustment Dashboard
 * Version	 		: 1.0
 * author			: arajan
*********************************************************************************************************/
%>

<%@ page language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.Date"%>
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtPartPriceAdjDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmPartPriceAdjDetails.jsp -->

<fmtPartPriceAdjDetails:setLocale value="<%=strLocale%>"/>
<fmtPartPriceAdjDetails:setBundle basename="properties.labels.sales.pricing.GmPartPriceAdjDetails"/>

<bean:define id="alAdjDetails" name="frmPartPriceAdj" property="alAdjDetails" type="java.util.List"> </bean:define>
<bean:define id="successMessage" name="frmPartPriceAdj" property="successMessage" type="java.lang.String"></bean:define>

<%
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
int intLength = alAdjDetails.size();
int intSize = 0;
int intCount = 0;
String strLine = "";
String strNextAccId = "";
String strTxnId = "";
String strInitBy = "";
String strInitDt = "";
Date dtInitDt = null;
String strImpBy = "";
Date dtImpDt = null;
String strImpDt = "";
String strStatus = "";
//String strCount = "";
String strAccID = "";
String strShade = "";
String strCallFlag = "";
int intAllCnt = 0;
String strAccName = "";
String strAccountIds = "";
boolean blFlag = false;

String strSubmitAccess = GmCommonClass.parseNull((String)request.getAttribute("SUBMITACCFL"));
String strBtnDisable = "false";
if(!strSubmitAccess.equals("Y")){
	strBtnDisable = "true";
}
 

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: Part Price Adjustment Initiate</title>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
</head>
<body leftmargin="20" topmargin="10">

	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr><td colspan="8" height="1" bgcolor="#cccccc"></td></tr>
			<tr>
				<td colspan="9">
					<table border="0"  width="100%" cellspacing="0" cellpadding="0">
						<tr class="ShadeRightTableCaption">
							<!-- <td width="5%" height="24">Select</td> -->
							<td width="10%"><fmtPartPriceAdjDetails:message key="LBL_ACC_GRP_NAME"/></td>
							<td width="12%"><fmtPartPriceAdjDetails:message key="LBL_TRANSACTION_ID"/></td>
							<td width="12%" align="center"><fmtPartPriceAdjDetails:message key="LBL_INITIATED_DATE"/></td>
							<td width="10%"><fmtPartPriceAdjDetails:message key="LBL_INITIATED_BY"/></td>							
							<td width="14%" align="center"><fmtPartPriceAdjDetails:message key="LBL_IMPLEMENTED_DATE"/></td>
							<td width="13%"><fmtPartPriceAdjDetails:message key="LBL_IMPLEMENTED_BY"/></td>
							<!-- <td width="10%"># of Parts</td> -->
							<td width="10%"><fmtPartPriceAdjDetails:message key="LBL_STATUS"/></td>
							<td width="8%"><fmtPartPriceAdjDetails:message key="LBL_COMMENTS"/></td>
						</tr>
<%
						if (intLength > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();						
							intSize = alAdjDetails.size();
							hmTempLoop = new HashMap();
							strLine = "";
							blFlag = false;
	
							for (int i = 0;i < intSize ;i++ ){
								hmLoop = (HashMap)alAdjDetails.get(i);
								if (i<intSize-1){
									hmTempLoop = (HashMap)alAdjDetails.get(i+1);
									strNextAccId = GmCommonClass.parseNull((String)hmTempLoop.get("PARTYID"));
								}
	
								strTxnId = GmCommonClass.parseNull((String)hmLoop.get("TXNID"));
								strInitBy = GmCommonClass.parseNull((String)hmLoop.get("INITBY"));
								//strInitDt = GmCommonClass.parseNull((String)hmLoop.get("INITDATE"));
								dtInitDt = (Date)hmLoop.get("INITDATE");
								strInitDt =dtInitDt==null?"":GmCommonClass.getStringFromDate(dtInitDt,strGCompDateFmt);								
								strImpBy = GmCommonClass.parseNull((String)hmLoop.get("APPRBY"));
								//strImpDt = GmCommonClass.parseNull((String)hmLoop.get("APPRDATE"));
								dtImpDt = (Date)hmLoop.get("APPRDATE");
								strImpDt = dtImpDt==null?"":GmCommonClass.getStringFromDate(dtImpDt,strGCompDateFmt);	
								strStatus = GmCommonClass.parseNull((String)hmLoop.get("STATUS"));
								//strCount = GmCommonClass.parseNull((String)hmLoop.get("PARTCNT"));
								strAccID = GmCommonClass.parseNull((String)hmLoop.get("PARTYID"));	
								strAccName = GmCommonClass.parseNull((String)hmLoop.get("PARTYNAME"));
								strCallFlag = GmCommonClass.parseNull((String)hmLoop.get("CALL_FLAG"));
													
								if (strAccID.equals(strNextAccId)){
									intCount++;
									strLine = "<TR><TD colspan=10 height=1 class=Line></TD></TR>";
								}else{
									strAccountIds = strAccountIds+strAccID+',';
									strLine = "<TR><TD colspan=10 height=1 class=Line></TD></TR>";
									if (intCount > 0){
										strLine = "";
									}else{
										blFlag = true;
									}
									intCount = 0;
								}
								
								if(i == intSize-1 && intSize != 1){ // To append the last account id
								  strAccountIds = strAccountIds+strAccID+',';
								}
	
								if (intCount > 1){
									//strRepNm = "";
									strLine = "";
								}
							
								strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
		
								out.print(strLine);	
								if (intCount == 1 || blFlag){
%>
	
						<tr id="tr<%=strAccID%>">		
							<td class="RightText" colspan="10" height="20">
							<fmtPartPriceAdjDetails:message key="LBL_CLICK_TO_EXPAND" var="varClickToExpand"/>
							&nbsp;<A class="RightText" title="${varClickToExpand}" href="javascript:Toggle('<%=strAccID%>')"><%=strAccName%></td>
						</tr>
							<tr>
								<td colspan="10"><div style="display:none" id="div<%=strAccID%>">
									<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tab<%=strAccID%>">
<%
									intAllCnt++;
									blFlag = false;
									}
%>					<TR <%=strShade%>>
							
						<!-- <td width="5%"></td> -->
						<td width="9%"></td>	
						<fmtPartPriceAdjDetails:message key="LBL_CLICK_TO_CHANGE" var="varClickToChange"/>
						<td width="12%" class="RightText" align="left">&nbsp;<A class="RightText" title="${varClickToChange}" href="javascript:fnEditTxn('<%=strTxnId%>','<%=strAccID%>')"><%=strTxnId%></td>	
						<td width="12%" class="RightText" align="center">&nbsp;<%=strInitDt%></td>
						<td width="10%" class="RightText" align="left">&nbsp;<%=strInitBy%></td>	
						<td width="14%" class="RightText" align="center">&nbsp;<%=strImpDt%></td>						
						<td width="13%" class="RightText" align="left">&nbsp;<%=strImpBy%></td>	
						<%-- <td width="10%" class="RightText" align="center">&nbsp;<%=strCount%></td> --%>	
						<td width="10%" class="RightText" align="left">&nbsp;<%=strStatus%></td>	
						<td width="8%" align="center">
						<fmtPartPriceAdjDetails:message key="LBL_ENTER_LOG" var="varClickToEnterLog"/>
							<img id="imgEdit" style="cursor:pointer"  
									<% if (strCallFlag.equals("N"))
	 								{ %>
		 							src="<%=strImagePath%>/phone_icon.jpg" 
		 							<%} else {%>	
		 							src="<%=strImagePath%>/phone-icon_ans.gif" 
			 						<% }%>
			 						title="${varClickToEnterLog}" 
			 						width="22" height="20" 
			 						onClick="javascript:fnOpenLog('<%=strTxnId%>' )"/>
						</td>	
								
					</TR>
									<tr><td colspan="10" height="1" bgcolor="#eeeeee"></td></tr>
<%					
								if ( intCount == 0 || i == intSize-1)
								{
%>
								</table></div>
							</td>
						</tr>
<%
								}
								
							} // End of For
								 
						}else{
%>									<tr>
										<td colspan="10" align="center" class="RightTextRed"><fmtPartPriceAdjDetails:message key="LBL_NO_RECORDS"/> !</td>
									</tr>
<%
						} 
%>
					</TABLE>
				</TD>
			</tr>
			<input type="hidden" value="<%=strAccountIds%>" id="hAccIDs" />
			<input type="hidden" value="<%=intLength%>" id="hRecCount" />
			
	</table>
	
<%@ include file="/common/GmFooter.inc"%>

</body>
</html>