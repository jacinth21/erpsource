
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!-- sales\pricing\GmRebateSetup.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtrebateMgnt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%> 
<bean:define id="rebateId" name="frmRebateSetup" property="rebateId" type="java.lang.String"> </bean:define>
<%@ page isELIgnored="false" %>
<fmtrebateMgnt:setLocale value="<%=strLocale%>"/>
<fmtrebateMgnt:setBundle basename="properties.labels.sales.pricing.GmRebateSetup" />
<bean:define id="partyId" name="frmRebateSetup" property="partyId" type="java.lang.String"> </bean:define>
<bean:define id="searchpartyId" name="frmRebateSetup" property="searchpartyId" type="java.lang.String"> </bean:define>
<bean:define id="partyName" name="frmRebateSetup" property="partyName" type="java.lang.String"> </bean:define>
<bean:define id="groupTypeId" name="frmRebateSetup" property="groupTypeId" type="java.lang.String"> </bean:define>
<bean:define id="accessFl" name="frmRebateSetup" property="accessFl" type="java.lang.String"></bean:define>
<bean:define id="rebateId" name="frmRebateSetup" property="rebateId" type="java.lang.String"></bean:define>
<bean:define id="activeFl" name="frmRebateSetup" property="activeFl" type="java.lang.String"></bean:define>
<bean:define id="rebateCategoryFl" name="frmRebateSetup" property="rebateCategoryFl" type="java.lang.String"></bean:define>
<bean:define id="rebateRateFl" name="frmRebateSetup" property="rebateRateFl" type="java.lang.String"></bean:define>
<bean:define id="rebatePartFl" name="frmRebateSetup" property="rebatePartFl" type="java.lang.String"></bean:define>
<bean:define id="rebateEffectiveFl" name="frmRebateSetup" property="rebateEffectiveFl" type="java.lang.String"></bean:define>
<bean:define id="rebateEffectiveDate" name="frmRebateSetup" property="rebateEffectiveDate" type="java.lang.String"></bean:define>
<%
String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");
String strWikiTitle = GmCommonClass.getWikiTitle("REBATE_MANAGMENT");
GmCalenderOperations.setTimeZone(strGCompTimeZone);
String strTodaysDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt);
String strApplDateFmt = strGCompDateFmt;
String strSaveDisable = "true";

if(accessFl.equals("Y")){
	strSaveDisable="false";
}
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"
	href="<%=strCssPath%>/displaytag.css">

<script language="JavaScript" src="<%=strJsPath%>/dhtmlxcommon.js"></script>

<script language="JavaScript"
	src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmRebateSetup.js"></script>	

<script type="text/javascript">
var format = '<%=strApplDateFmt%>';
var rebateId ='<%=rebateId%>';
var todayDate = '<%=strTodaysDate%>';
var activeFlag = '<%=activeFl%>';
var rebateCategoryFl ='<%=rebateCategoryFl%>';
var rebateRateFl ='<%=rebateRateFl%>';
var rebatePartFl ='<%=rebatePartFl%>';
var rebateEffectiveFl ='<%=rebateEffectiveFl%>';
var rebateEffectiveDt ='<%=rebateEffectiveDate%>';
</script>

<title>Rebate Management</title>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad();" >
	<html:form
		action="/gmRebateSetup.do?method=loadRebateManagmentDtls">
		<html:hidden property="strOpt" />
			<html:hidden property="rebateId" />
		<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="3" height="25" class="RightDashBoardHeader"><fmtrebateMgnt:message
						key="LBL_REBATE_MANAGEMENT" /></td>
				<td height="25" class="RightDashBoardHeader"><fmtrebateMgnt:message
						key="IMG_HELP" var="varHelp" /><img align="right" id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr class="Shade">
				<td class="RightTableCaption" align="right" height="25" >
				<fmtrebateMgnt:message key="LBL_ACCOUNT_TYPE" var="varAccountType"/><gmjsp:label
						type="RegularText" SFLblControlName="${varAccountType}:" td="false"/>
				</td>
				<td class="RightText">&nbsp;<gmjsp:dropdown controlName="groupTypeId"
						SFFormName="frmRebateSetup" SFSeletedValue="groupTypeId"
						SFValue="alAccountType" codeId="CODEID" codeName="CODENM"
						tabIndex="1" onChange="fnChangeAccSource(this.value);"/></td>
						<td colspan="2" >&nbsp;</td>
			</tr>
			 <tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			
			<tr>
			<td class="RightTableCaption" align="right" height="25">
			<fmtrebateMgnt:message key="LBL_NAME"  var="varName"/>
               <gmjsp:label type="MandatoryText" SFLblControlName="${varName}:" td="false"/>
			</td>
					<td ><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="partyId" />
					<jsp:param name="METHOD_LOAD" value="loadPartyNameList&searchType=PrefixSearch" />
					<jsp:param name="WIDTH" value="400" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value="2"/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%=searchpartyId%>" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="<%=partyId%>" />
					<jsp:param name="SHOW_DATA" value="100" />
					<jsp:param name="AUTO_RELOAD" value="fnLoadPartDtls();" />
					<jsp:param name="DYNAMIC_OBJ" value="#groupTypeId^" />				
							</jsp:include>
							</td>
			
	
			<td align="right" class="RightTableCaption" width="150"><fmtrebateMgnt:message
						key="LBL_REBATE_ID"/>:
	      </td>
			<td align="left" id="rebateID" width="100">&nbsp;<bean:write property="rebateId" name="frmRebateSetup"/></td>
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr class="Shade">
			    <td class="RightTableCaption" align="right" height="25">
			  <fmtrebateMgnt:message key="LBL_REABTE_TYPE" var="varRebateType" />
			   <gmjsp:label type="RegularText" SFLblControlName="${varRebateType}:" td="false"/>
			  </td>
			  <td>&nbsp;<gmjsp:dropdown controlName="rebateTypeId"
						SFFormName="frmRebateSetup" SFSeletedValue="rebateTypeId"
						SFValue="alRebateType" codeId="CODEID" codeName="CODENM"
						 tabIndex="3"/>	
					
				</td>
						<td align="right" class="RightTableCaption">&nbsp;<fmtrebateMgnt:message
						key="LBL_PART_NUMBER" />:</td>
						<td align="left" id="rebatePartNumber">&nbsp;<bean:write property="rebatePartNumber"
						name="frmRebateSetup" /></td>
						
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr>
			  <td class="RightTableCaption" align="right" height="25">
			  <fmtrebateMgnt:message key="LBL_REBATE_CATEGORY" var="varRebateCategory" />
			   <gmjsp:label type="RegularText" SFLblControlName="${varRebateCategory}:" td="false"/>
			  </td>
			  <td>&nbsp;<gmjsp:dropdown controlName="rebateCategoryId"
						SFFormName="frmRebateSetup" SFSeletedValue="rebateCategoryId"
						SFValue="alRebateCategory" codeId="CODEID" codeName="CODENM"
						tabIndex="4" onChange="fnChange(this.value)"/>
						<%  if(rebateCategoryFl.equals("Y") ) { %>
						<a href="#" onClick="fnHistory(107703);" id="rebateCategoryIcon"><img alt="View Qty Update" src="<%=strImagePath%>/icon_History.gif" border="0"></a>
						<%} %>
						</td>
						<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr class="Shade">
			<td class="RightTableCaption" align="right" height="25">
			<fmtrebateMgnt:message key="LBL_REBATE_RATE" var="varRebateRate"/>
			 <gmjsp:label type="MandatoryText" SFLblControlName="${varRebateRate}:" td="false"/>
			</td>
			<td>&nbsp;<html:text maxlength="5" name="frmRebateSetup" property="rebateRate"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" tabindex="5" />
							<%  if(rebateRateFl.equals("Y") ) { %>
						<a href="#" onClick="fnHistory(107704)" id="rebateRateIcon"><img alt="View Qty Update" src="<%=strImagePath%>/icon_History.gif" border="0"></a>
						<%} %>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
				<tr>
				 <td class="RightTableCaption" align="right" height="25">
			  <fmtrebateMgnt:message key="LBL_REBATE_PARTS" var="varRebateParts"/>
			  <gmjsp:label type="RegularText" SFLblControlName="${varRebateParts}:" td="false"/>
			</td>
			  <td>&nbsp;<gmjsp:dropdown controlName="rebatePartTypeId"
						SFFormName="frmRebateSetup" SFSeletedValue="rebatePartTypeId"
						SFValue="alRebateParts" codeId="CODEID" codeName="CODENM"
						tabIndex="6"  onChange="fnRebateParts()"/>
						<%if(rebatePartFl.equals("Y") ) { %>
						<a href="#" onClick="fnHistory(107702);" id="rebatePartsIcon"><img alt="View Qty Update" src="<%=strImagePath%>/icon_History.gif" border="0"></a>
					<%} %>
						</td>
				
				<td colspan="2">&nbsp;</td>
						
			</tr>
			 <tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
		
		<tr>
				<td colspan="4"><div id="divTierSetup" style="display:none;">
				<jsp:include page="/sales/pricing/GmIncRebateTierSetup.jsp" >
				<jsp:param value="FORMNAME" name="frmRebateSetup"/>
				</jsp:include>
				</div>
				</td>
			</tr> 
		<tr class="Shade">
			<td class="RightTableCaption" align="right" height="25">
			<fmtrebateMgnt:message key="LBL_CONTRACT_DATE" var="varContractdt"/>
			 <gmjsp:label type="MandatoryText" SFLblControlName="${varContractdt}:" td="false"/>
			</td>
			<td>&nbsp;<gmjsp:calendar SFFormName="frmRebateSetup"
						controlName="rebateFromDate" gmClass="InputArea"
						onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" tabIndex="16"/>
			<gmjsp:calendar SFFormName="frmRebateSetup"
						controlName="rebateToDate" gmClass="InputArea"
						onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" tabIndex="17" /></td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr >
			<td class="RightTableCaption" align="right" height="25">
			<fmtrebateMgnt:message key="LBL_EFFECTIVE_DATE" var="vareffectiveDt"/>
			 <gmjsp:label type="MandatoryText" SFLblControlName="${vareffectiveDt}:" td="false"/>
			</td>
			<td>&nbsp;<gmjsp:calendar SFFormName="frmRebateSetup"
						controlName="rebateEffectiveDate" gmClass="InputArea"
						onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" tabIndex="18" />
						<% if(rebateEffectiveFl.equals("Y") ) { %>
						<a href="#" onClick="fnHistory(107705)" id="effDateIcon"><img alt="View Qty Update" src="<%=strImagePath%>/icon_History.gif" border="0"></a>
						<%} %>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
	
			
			<tr class="Shade">
			  <td class="RightTableCaption" align="right" height="25">&nbsp;<fmtrebateMgnt:message
						key="LBL_PAYMENT" />:</td>
			  <td>&nbsp;<gmjsp:dropdown controlName="rebatePaymentId"
						SFFormName="frmRebateSetup" SFSeletedValue="rebatePaymentId"
						SFValue="alPayment" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" tabIndex="19"/></td>
						<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr >
			   <td class="RightTableCaption" align="right" height="25">&nbsp;<fmtrebateMgnt:message
						key="LBL_CONTRACT_INFO" />:</td>
			<td class="RightTableCaption">&nbsp;<html:text size="70" name="frmRebateSetup" property="rebateContractInfo"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" tabindex="20" />
			</td>
				<td colspan="2">&nbsp;</td>	
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr class="Shade" height="80">
			 <td class="RightTableCaption" align="right">&nbsp;<fmtrebateMgnt:message
						key="LBL_REABTE_COMMENTS" />:</td>
			<td colspan="3">&nbsp;<html:textarea property="rebateComments" rows="5" cols="80"  name="frmRebateSetup"
				styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="21" />
			</td>
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
		
			<tr>
			 <td class="RightTableCaption" align="right">&nbsp;<fmtrebateMgnt:message
						key="LBL_ACTIVE" />:</td>
			  <td><html:checkbox property="activeFl" name="frmRebateSetup" tabindex="22"></html:checkbox> </td>
			 <td colspan="2">&nbsp;</td>	
			</tr>
	          <tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr>
			<td colspan="4">
				<jsp:include page="/common/GmIncludeLog.jsp" >
				<jsp:param name="FORMNAME" value="frmRebateSetup" />
					<jsp:param name="ALNAME" value="alLogReasons" />
					<jsp:param name="LogMode" value="txt_LogReason" />
					<jsp:param name="Mandatory" value="yes" />
					<jsp:param name="TabIndex" value="23" />
				</jsp:include>
			</td>	
			
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr>
			 <td colspan="4"  align="center" height="30">
				 <gmjsp:button name="BTN_SUBMIT" value="Submit" gmClass="button"  buttonType="Save" onClick="fnSubmit()" tabindex="24" disabled="<%=strSaveDisable%>"/>
                 <gmjsp:button name="BTN_RESET" value="Reset" gmClass="button" onClick="fnReset(this)" buttonType="Reset" tabindex="25" />
                  <gmjsp:button name="BTN_ADD_PARTS" value="Add Parts" gmClass="button" onClick="fnAddNewParts();" buttonType="Save" tabindex="26" disabled="<%=strSaveDisable%>"/>
                 <gmjsp:button name="BTN_VIEW_PARTS" value="View Parts" gmClass="button" onClick="fnViewParts()" buttonType="Load" tabindex="27" /> 
				
			 </td>
			</tr> 
		</table>
	</html:form>
</body>
<%@ include file="/common/GmFooter.inc" %>
</html>