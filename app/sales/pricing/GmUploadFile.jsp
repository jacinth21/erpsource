 <%
/**********************************************************************************
 * File		 		: GmUploadFile.jsp
 * Desc		 		: This screen is used to display the generate price file Report
 * Version	 		: 1.0
 * author			: dsandeep
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtUploadFile" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmUploadFile.jsp -->
<fmtUploadFile:setLocale value="<%=strLocale%>"/>
<fmtUploadFile:setBundle basename="properties.labels.sales.pricing.GmUploadFile"/>

<bean:define id="xmlGridData" name="frmPriceApprovalRequest" property="xmlGridData" type="java.lang.String"> </bean:define>
<bean:define id="priceRequestId" name="frmPriceApprovalRequest" property="priceRequestId" type="java.lang.String"></bean:define>
<%
String strSalesPricingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_PRICING");
%>


<HTML>
<HEAD>
<TITLE> Globus Medical: Upload price file </TITLE>
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strSalesPricingJsPath%>/GmUploadFile.js"></script>

<script>

var gridObj = "";
var gridData = '<%=xmlGridData%>';

function fnOnLoad(){

if (gridData != '') {
	gridObj = initGridData('mygrid_container', gridData);
	//chk_emailReq_rowID = gridObj.getColIndexById("upload_id");
	//alert(chk_emailReq_rowID);
	gridObj.enablePaging(true, 100, 10, "pagingArea", true);
	gridObj.setPagingSkin("bricks");
}

}
//This Function used to Initiate the grid
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.init();
	gObj.loadXMLString(gridData);
	return gObj;
}

function fnExportExcel() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}
</script>

<script> 

var priceRequestId = '<%=priceRequestId%>';

</script>


</HEAD>
<BODY onload="fnOnLoad();"> 
<html:form method="post" action="/gmPriceApprovalRequestAction.do?" enctype="multipart/form-data">
<html:hidden property="haction"/>
<input type="hidden" name="haction" value="" />
<table  class="DtTable1000" cellspacing="0" >
<tr>
  <td height="25" class="RightDashBoardHeader" colspan="5" align = "left"><fmtUploadFile:message key="LBL_DOCUMENTS"/>:<%=priceRequestId%></td>
</tr>
<tr height="20">
</tr>
<tr>
<td colspan="3" align="center">
<!-- <input type="file" name="file" id ="fileUpload"/> -->
<html:file property="file" name="frmPriceApprovalRequest"/> 
<fmtUploadFile:message key="BTN_UPLOAD" var="varUpload"/><gmjsp:button style="width: 5em; height: 2em; font-size: 13px" controlId="fileUploads" value="${varUpload}" buttonType="Save" gmClass="Button" onClick="fnFileUpload(this.form);"/>
</td>
</tr>


				<%if (xmlGridData.indexOf("cell") != -1) {
               %>
           <tr>
				<td colspan="10">
					<div id="mygrid_container" style="height:400px;width:1000px;"></div>
					<div id="pagingArea" style="width: 1000px"></div>
				</td>
			</tr> 
			<%}else if (!xmlGridData.equals("")) {
				%>
				<tr>
					<td colspan="8" height="1" class="LLine">
					<div id="mygrid_container" style="height:100px;width:1000px;"></div>
					</td>
				</tr>				
				<tr>
					<td colspan="8" align="center" class="RightText"><fmtUploadFile:message key="LBL_NO_DATA"/>
						</td>
				</tr>
				
				<%}%> 
			<tr height="20">
			</tr>
			<tr>
			<td colspan="3" align="center">
			<fmtUploadFile:message key="BTN_VOID" var="varVoid"/><gmjsp:button value="${varVoid}" gmClass="Button" name="btn_void" style="width: 6em" buttonType="Save" onClick="fnVoid(this.form);"/>
			
			<fmtUploadFile:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="javaScript:fnClose();" buttonType="Load" />
			
            </td>
			</tr>
			
			
</table>


      </html:form>  
 <%@ include file="/common/GmFooter.inc"%>
 </BODY>
 </HTML>
   




