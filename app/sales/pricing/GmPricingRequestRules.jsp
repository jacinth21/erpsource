

<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>

<%@ taglib prefix="fmtPricingRequestRules" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\pricing\GmPricingRequestRules.jsp -->

<fmtPricingRequestRules:setLocale value="<%=strLocale%>"/>
<fmtPricingRequestRules:setBundle basename="properties.labels.sales.pricing.GmPricingRequestRules"/>

<bean:define id="pricingRules" name="frmPricingRequestForm" property="pricingRequestRules" type="java.lang.String"></bean:define>

<%
	String strWikiTitle = GmCommonClass.getWikiTitle("PRICING_STATUS_REPORT");
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Pricing -Request Rules</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>


<style type="text/css" media="all">
@import url("<%=strCssPath%>/displaytag.css");

.grid_hover {
	background-color: #E6E6FA;
	font-size: 20px;
}

div.gridbox_dhx_skyblue table.obj tr td {
	border-width: 0px 1px 0px 0px;
	border-color: #A4BED4;
}
</style>

<script type="text/javascript">
function fnOnPageLoad() {		
	var pricingRules = '<%=pricingRules%>';
	
	if (pricingRules != ''){
		
		gridObj = initGrid('dataGridDiv',pricingRules);
		gridObj.enableTooltips("true,true,true,true");
	}
	
}
</script>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad()">
<html:form action="/gmPricingRequestDAction.do?method=loadPricingRequestRules">

	<html:hidden property="strOpt" />
	

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		
			<tr id="gridData"> 
			
				<td><div id="dataGridDiv" style="grid" height="150px" width="702">&nbsp;</div></td> 
			</tr>
			<tr>
			<td height="50" align="center"><fmtPricingRequestRules:message key="BTN_CLOSE" var="varClose"/><gmjsp:button style="width: 8em; height: 2em; font-size: 13px" name="Close_button" value="${varClose}" buttonType="Load" gmClass="button" onClick="javascript:window.close();" /> </td>
			</tr>
			</table>
			</html:form>
			</BODY></HTML>
			
