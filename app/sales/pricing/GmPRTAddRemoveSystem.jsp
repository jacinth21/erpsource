<!-- prodmgmnt\GmPRTAddRemoveSystem.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="fmtAddRemoveSystem"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>

<fmtAddRemoveSystem:setLocale value="<%=strLocale%>" />
<bean:define id="systemId" name="frmPriceRequestApprovalRequest"
	property="systemId" type="java.lang.String"></bean:define>
<%
	String strSalesPricingJsPath = GmFilePathConfigurationBean
			.getFilePathConfig("JS_SALES_PRICING");
	String strSetId = "";
	String strHeaderTitle = "Add/Remove System in PRT";
	String strWikiTitle = "ADD_OR_REMOVESYSTEM_PRICING";
%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="STYLESHEET" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxlayout.css">
<link rel="STYLESHEET" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.css">

<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlxcommon.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript"
	src="<%=strSalesPricingJsPath%>/GmPRTAddRemoveSystem.js"></script>
<script language="javascript" type="text/javascript"
	src="<%=strExtWebPath%>/tiny_mce/tiny_mce.js"></script>
<style>
#searchsystemId{
width:480px!important;
margin-top:4px;
}
 #segmentId{
width:370px;
}
</style>

</HEAD>
<BODY leftmargin="20" topmargin="10">
	<FORM name="frmPriceRequestApprovalRequest" method="POST">
		<input type="hidden" name="submitFl" value="" /> <input type="hidden"
			name="sys_AccId" id="sys_AccId" value="" />
		<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">

			<tr>
				<td height="25" class="RightDashBoardHeader" width="250">&nbsp;<%=strHeaderTitle%></td>
				<td align="right" class=RightDashBoardHeader colspan="2"><img
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='Help' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
				<tr>
					<td class="LLine" height="1" colspan="2"></td>
				</tr>
				<tr height="30" class="shade">
					<td align="right" width="180px"><font color="red"><b>*</font>System
						Name:&nbsp;</b></td>
					<td width="600px"><div style="float: left">
							<jsp:include page="/common/GmAutoCompleteInclude.jsp">
								<jsp:param name="CONTROL_NAME" value="systemId" />
								<jsp:param name="METHOD_LOAD"
									value="loadSystemList&searchType=PrefixSearch" />
								<jsp:param name="WIDTH" value="300" />
								<jsp:param name="CSS_CLASS" value="search" />
								<jsp:param name="CONTROL_NM_VALUE" value="" />
								<jsp:param name="CONTROL_ID_VALUE" value="<%=systemId%>" />
								<jsp:param name="SHOW_DATA" value="50" />
								<jsp:param name="AUTO_RELOAD" value="fnGetSystemId();" />

							</jsp:include>
						</div>
						<div style="float: left">
							&nbsp;&nbsp;&nbsp;
							<gmjsp:button value="GPP" onClick="fnOpenGP();" buttonType="Load"
								gmClass="button" />
						</div></td>
					<td></td>
				</tr>
				</td>
				</tr>
				<tr>
					<td height="1" colspan="2"></td>
				</tr>
				<tr height="30">
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<td align="right" width="180px"><b>Segment Name:&nbsp;</b></td>
					<td>&nbsp;<gmjsp:dropdown controlName="segmentId"
							SFFormName="frmPriceRequestApprovalRequest"
							SFSeletedValue="segmentId" SFValue="alSegNameList"
							codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
					</td>
				</tr>
				<tr>
					<td height="1" colspan="2"></td>
				</tr>
				<tr height="30" class="shade">
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<td align="right" width="180px"><b>Add/Remove
							System:&nbsp;</b></td>
					<td>&nbsp;<select name="releaseFl" class="RightText"
						tabindex="2" onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');">
							<option value="Y">Add in Pricing Tool</option>
							<option value="N">Remove From Pricing Tool</option>
					</select>
					</td>
				</tr>
			</table>
			<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<%-- <tr class="<%=strShade%>" >
						<td align="Right" width="180px" valign="top"><font color="red">*</font><b>User List:&nbsp;</b></td> 
						<td>
     			  		<%=GmCommonControls.getChkBoxGroup("",alUserList,"user",hmUser)%>
						</td>
						</tr> --%>
				<tr>
					<td align="center" height="1" colspan="2"><b><div
								id="successMsg" class="RegularText" style="color: red"></div></b></td>
				</tr>
				<tr>
					<td height="50" align="center" colspan="2">&nbsp; <gmjsp:button
							value="Submit" buttonType="Save" onClick="fnSubmit();"
							gmClass="button" />&nbsp; <gmjsp:button value="Reset"
							onClick="fnClear();" buttonType="Load" gmClass="button" />&nbsp;
					
				</tr>
				<tr>
					<td height="1" colspan="2"></td>
				</tr>
				</td>
				</tr>
			</table>
		</table>
	</FORM>
	<%@ include file="/common/GmFooter.inc"%>
</body>
</html>