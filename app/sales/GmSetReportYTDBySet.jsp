 <%
/**********************************************************************************
 * File		 		: GmSetReportYTDByAccountPer.jsp
 * Desc		 		: This screen is used to display Set Rep By Account now modified 
 *					  to display group report
 * Version	 		: 1.1
 * author			: RichardK
************************************************************************************/
%>
<%@ page language="java" %>
<%@include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtSetReportYTDBySet" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!--  sales\GmSetReportYTDBySet.jsp-->
<fmtSetReportYTDBySet:setLocale value="<%=strLocale%>"/>
<fmtSetReportYTDBySet:setBundle basename="properties.labels.sales.GmSetReportYTDBySet"/>

<%
try {

	GmServlet gm = new GmServlet();
	HashMap hmReturn = new HashMap();
	ArrayList alProjList = new ArrayList();
	String strWikiTitle = GmCommonClass.getWikiTitle("SALES_SYSTEM_REPORT");
	String strSelected = "";
	String strCodeID = "";
	String strProjNm = "";
	String strProjId = "";
	
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	gmCrossTab.setExport(true, pageContext, "/GmSetYTDReportServlet", "SetYTDReport", "frmOrder");
	gm.checkSession(response,session);

	String strShowArrow = GmCommonClass.parseZero((String)request.getAttribute("Chk_ShowArrowFl"));
	String strShowPercent = GmCommonClass.parseZero((String)request.getAttribute("Chk_ShowPercentFl"));
	String strHideAmount = GmCommonClass.parseZero((String)request.getAttribute("Chk_HideAmountFl"));
	String strGroupAccId = GmCommonClass.parseNull((String)request.getAttribute("GroupAccId"));
	String strGrpPricingName = GmCommonClass.parseNull((String)request.getAttribute("GrpPricingName"));

	// Combobox default value
	String strHeader = (String)request.getAttribute("strHeader");
	String strFilterID = (String)request.getAttribute("Cbo_Name");
	String strType = (String)request.getAttribute("Cbo_Type");
	
	String strDispTemp = (strFilterID.equals("0"))?null:strFilterID;
	
	strFilterID = (strFilterID == null)?"0":strFilterID; // If no value selected will set the combo box value to [Choose One]
	strType = (strType == null)?"0":strType; // Setting combo box value
	String strhAction = (String)request.getAttribute("hAction");
	
	

	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}
	

	gmCrossTab.setLinkType(strhAction);
	strhAction = (strhAction == null)?"Load":strhAction;
	
	// To get the Project Combo Box value information 
	HashMap hmComonValue = (HashMap)request.getAttribute("hmComonValue");
	if (hmComonValue != null)
	{
		alProjList = (ArrayList)hmComonValue.get("PROJECTS");
	}
		
	// To Generate the Crosstab report
	gmCrossTab.setHeader("Sales By Distributor By Month");
	
	gmCrossTab.setLinkRequired(true);	// To Specify the link
	
	// To display percent changed information
	if (strShowPercent.equals("1"))
	{
		gmCrossTab.setReportType(gmCrossTab.CROSSTAB_SALE_PER);	
	}	
	if (strShowArrow.equals("1") )
	{
		gmCrossTab.setShowArrow(true);; // To Display % arrow information
	}

	if (strHideAmount.equals("1"))
	{
		gmCrossTab.setHideAmount(true);	
	}	
	// To specify if its unit or valye 
	gmCrossTab.setValueType(Integer.parseInt(strType));
	
	gmCrossTab.setRowHighlightRequired(true);
	
	// Setting Display Parameter
	gmCrossTab.setAmountWidth(70);
	gmCrossTab.setNameWidth(400);
	gmCrossTab.setRowHeight(18);
	
	// Include the column the need to skipped 
	gmCrossTab.addSkip_Div_Round("Sets");
	gmCrossTab.addSkip_Div("Turns");
	
	// Line Style information
	gmCrossTab.setColumnDivider(true);
	gmCrossTab.setColumnLineStyle("borderDark");
	// Setting Line before print
	gmCrossTab.addLine("Name");
	gmCrossTab.addLine("Total");
	gmCrossTab.addLine("Value");
	
	// To Skip Additional Calculation
	gmCrossTab.addSkip_Calculation("Sets");
	gmCrossTab.addSkip_Calculation("Value");
	gmCrossTab.addSkip_Calculation("Turns");
	
	// Setting the cross tab style 
	//gmCrossTab.setGeneralStyle("ShadeLightBlueTD");
	gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD");
	gmCrossTab.addStyle("Name","ShadeMedGrayTD","ShadeDarkGrayTD");
	gmCrossTab.addStyle("Total","ShadeMedBlueTD","ShadeDarkBlueTD") ;
	gmCrossTab.addStyle("Sets","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
	gmCrossTab.addStyle("Value","ShadeLightOrangeTD","ShadeDarkOrangeTD") ;
	gmCrossTab.addStyle("Turns","ShadeMedGreenTD","ShadeDarkGreenTD") ;
	
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	
/*************************************************************  SORT COLUMNS *********************************************************/	
	
	int intSortColumn =  request.getParameter("hSortColumn") == null ? -1 : Integer.parseInt((String)request.getParameter("hSortColumn"));
	int intSortOrder = Integer.parseInt(GmCommonClass.parseZero((String)request.getParameter("hSortOrder")));	
    
	gmCrossTab.setSortRequired(true);
	gmCrossTab.addSortType("Name","String");
	
	gmCrossTab.setSortColumn(intSortColumn);
	gmCrossTab.setSortOrder(intSortOrder);
	gmCrossTab.displayZeros(true);
			
	/*************************************************************************************************************************************/
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: YTD #</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script>
var strGroupAccId = '<%=strGroupAccId%>';
var strGrpPricingName = '<%=strGrpPricingName%>';
function fnSort(varColumnName, varSortOrder)
{
	document.frmOrder.hSortColumn.value = varColumnName;
	document.frmOrder.hSortOrder.value = varSortOrder;
	fnGo("<%=strhAction%>");
}


function fnSubmit()
{
	document.frmOrder.submit();
}

function fnGo(haction)
{
	if (document.frmOrder.Cbo_Name.value != '0') 
	{
		document.frmOrder.hAccountID.value = document.frmOrder.Cbo_Name.value;
	}	
	document.frmOrder.hAction.value ="<%=strhAction%>";
	
	if ('LoadGroup' == '<%=strhAction%>' || 'LoadGroupA' == '<%=strhAction%>' || 'LoadSet' == '<%=strhAction%>' ) 
	{
		document.frmOrder.SetNumber.value = 'null';
	}
	var val = document.frmOrder.SetNumber.value
	url='<%=strServletPath%>/GmSetYTDReportServlet?SetNumber='+val;
	url=url+'&hAction='+document.frmOrder.hAction.value+'&hGroupAccId='+strGroupAccId+'&hGroupName='+strGrpPricingName; // MNTTASK-6864. Added strGroupAccId from Pricing Request Screen. While select group account,set GPO id.
	fnReloadSales('',url);
	 
	//document.frmOrder.submit();
}

function fnCallAccount(val,Type )
{ 
	var ShowType = 	Type.substring(Type.length,Type.length -1 );
	if (ShowType == 'A')
	{
		document.frmOrder.hAction.value = "LoadAccountA";	
	}else {
		document.frmOrder.hAction.value = "LoadAccount";
	}
	
	url='<%=strServletPath%>/GmSalesYTDReportServlet?SetNumber='+val;
	url=url+'&hAction='+document.frmOrder.hAction.value;
	fnReloadSales('',url);
}

function fnCallRepDetails(val, Type)
{
	var ShowType = 	Type.substring(Type.length,Type.length -1 );
	if (ShowType == 'A')
	{
		document.frmOrder.hAction.value = "LoadRepA";	
	}else {
		document.frmOrder.hAction.value = "LoadRep";
	}

	url='<%=strServletPath%>/GmSalesYTDReportServlet?SetNumber='+val;
	url=url+'&hAction='+document.frmOrder.hAction.value;
	fnReloadSales('',url);
}

function fnCallDisDetails(val, Type)
{
	var ShowType = 	Type.substring(Type.length,Type.length -1 );
	if (ShowType == 'A')
	{
		document.frmOrder.hAction.value = "LoadDistributorA";	
	}else {
		document.frmOrder.hAction.value = "LoadDistributor";
	}


	url='<%=strServletPath%>/GmSalesYTDReportServlet?SetNumber='+val;
	url=url+'&hAction='+document.frmOrder.hAction.value;
	fnReloadSales('',url);
}

function fnCallPart(val, Type)
{
	
	var ShowType = 	Type.substring(Type.length,Type.length -1 );
	if (ShowType == 'A')
	{
		document.frmOrder.hAction.value = "LoadPartA";	
	}else {
		document.frmOrder.hAction.value = "LoadPart";
	}

	
	document.frmOrder.SetNumber.value = val;
	url='<%=strServletPath%>/GmSalesYTDReportServlet?SetNumber='+val;
	url=url+'&hAction='+document.frmOrder.hAction.value;
	fnReloadSales('',url);
	
}

function fnCallDetail(val)
{
	document.frmOrder.SetNumber.value = val;
	document.frmOrder.action = "<%=strServletPath%>/GmSalesYTDReportServlet";
	document.frmOrder.hAction.value = "LoadAccount";
	document.frmOrder.submit();
}
function fnLoad()
{
	document.frmOrder.Cbo_FromMonth.value = '<%=request.getAttribute("Cbo_FromMonth")%>';
	document.frmOrder.Cbo_ToMonth.value = '<%=request.getAttribute("Cbo_ToMonth")%>';
	document.frmOrder.Cbo_FromYear.value = '<%=request.getAttribute("Cbo_FromYear")%>';
	document.frmOrder.Cbo_ToYear.value = '<%=request.getAttribute("Cbo_ToYear")%>';
	
	document.frmOrder.Chk_ShowPercentFl.checked = <%=(strShowPercent == "0")?"false":"true"%>;
	document.frmOrder.Chk_ShowArrowFl.checked = <%=(strShowArrow == "0")?"false":"true"%>;
	document.frmOrder.Chk_HideAmountFl.checked = <%=(strHideAmount == "0")?"false":"true"%>;

	document.frmOrder.Cbo_Name.value = '<%=strFilterID%>';
	document.frmOrder.Cbo_Type.value = '<%=strType%>';
}
 
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad()">
<FORM name="frmOrder" method="post" action = "<%=strServletPath%>/GmSetYTDReportServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">

<input type="hidden" name="hAreaDirector" value="<%=request.getAttribute("hAreaDirector")%>">
<input type="hidden" name="DistributorID" value="<%=request.getAttribute("hDistributorID")%>">
<input type="hidden" name="hAccountID" value="<%=request.getAttribute("hAccountID")%>">
<input type="hidden" name="SetNumber" value="<%=request.getAttribute("hSetNumber")%>">
<input type="hidden" name="hRepID" value="<%=request.getAttribute("hRepID")%>">
<input type="hidden" name="hPartNumber" value="<%=request.getAttribute("hPartNumber")%>">
<input type="hidden" name="hTerritory" value="<%=request.getAttribute("hTerritory")%>">
<!-- ********************Included for sorting****************** -->
  <input type="hidden" name="hSortColumn" value="<%=intSortColumn%>">
  <input type="hidden" name="hSortOrder" value="<%=intSortOrder%>">
<!-- ********************************************************** -->
<TABLE cellSpacing=0 cellPadding=0  border=0 width="1000">
	<tr>
		<td rowspan="11" class=Line noWrap width=1></td>
		<td class=Line noWrap height=1></td>
		<td rowspan="11" class=Line noWrap width=1></td>
	</tr>
	<tr>
		<td>
			<TABLE cellSpacing=0 cellPadding=0  border="0"  width="100%">
				<tr class=Line>
					<td height=25 class=RightDashBoardHeader><%=strHeader %><fmtSetReportYTDBySet:message key="LBL_IN"/></td>
					<td  height="25" class="RightDashBoardHeader">
						<fmtSetReportYTDBySet:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
	
				</tr>
			</TABLE>
		</td>
	</tr>
	<tr class=Line><td noWrap height=1></td></tr>
	<!-- Addition Filter Information -->
	<tr>
		<td>
						<jsp:include page="/sales/GmSalesFilters.jsp" >
						<jsp:param name="FRMNAME" value="frmOrder" />
						<jsp:param name="HACTION" value="<%=strhAction%>" />
						<jsp:param name="HIDEBUTTON" value="HIDEGO" />
						</jsp:include>
		</td>
	</tr>
	<tr>
		<td><jsp:include page="/common/GmCrossTabFilterInclude.jsp" > 
		<jsp:param name="HACTION" value="<%=strhAction%>" /></jsp:include> </td>
	</tr>
		<!-- Screen Level Filter	-->
	<tr class=Line><td noWrap height=1></td></tr>	
	<tr>
	<% if (strhAction.equals("LoadGroup")|| strhAction.equals("LoadGroupA")) {%>
		<td class="RightTableCaption" colspan="2" HEIGHT="25">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<fmtSetReportYTDBySet:message key="LBL_ACC_NAME"/>
	<%} else {%>
		<td class="RightTableCaption" colspan="2" HEIGHT="25"><fmtSetReportYTDBySet:message key="LBL_DIS_NAME"/>
	<%} %>
		&nbsp;<select name="Cbo_Name" id="Region" class="RightText" tabindex="9">
		<option value="0" ><fmtSetReportYTDBySet:message key="LBL_ALL_VALUES"/>			
<%		HashMap hcboVal = null;	  		
		int intSize = alProjList.size();
		hcboVal = new HashMap();
		for (int i=0;i<intSize;i++)
		{
			hcboVal = (HashMap)alProjList.get(i);
			strCodeID = (String)hcboVal.get("ID");
			strProjNm = GmCommonClass.getStringWithTM((String)hcboVal.get("NAME"));
%>
			<option <%=strSelected%> value="<%=strCodeID%>"><%=strProjNm%></option>
<%
		}
%>
		</select>
		&nbsp;&nbsp;<fmtSetReportYTDBySet:message key="LBL_TYPE"/> 
		&nbsp;<select name="Cbo_Type" id="Type" class="RightText" tabindex="10">
			 <option value="0" ><fmtSetReportYTDBySet:message key="LBL_UNIT"/></option>
			 <option value="1" ><fmtSetReportYTDBySet:message key="LBL_AMOUNT"/></option>
		 </select>				
		 
	</td>
	</tr>	
	<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
	<tr><td>
		<jsp:include page="/sales/GmSalesFilterDetails.jsp"/>
	</td></tr>
	<tr class=Line><td noWrap height=1></td></tr>	
	
	<tr>
		<td> <%=gmCrossTab.PrintCrossTabReport(hmReturn)%></td>
	</tr>
</TABLE>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>
<%@ include file="/common/GmSalesFooter.inc"%><!--PMT-53328 - Implement jquery.freezeheader.js file for freeze the header information -->
<%@ include file="/common/GmFooter.inc"%>
</HTML>
