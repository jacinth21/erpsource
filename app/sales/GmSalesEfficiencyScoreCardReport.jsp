<!-- sales\GmSalesEfficiencyScoreCardReport.jsp -->
<%
	/**********************************************************************************
	  * File		 		: GmSalesEfficiencyScoreCardReport.jsp
      * Desc		 		: This screen is used to Scorecard - Sales report by System, Field Sales, Area Director (AD)
      * author			    : Raja
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java"%>
<%@page import="java.util.Date"%> 
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.net.URLEncoder"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%> 
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.net.URLEncoder" %>
<%@ taglib prefix="fmtGmSalesEfficiencyScoreCard" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtGmSalesEfficiencyScoreCard:setLocale value="<%=strLocale%>"/>
<fmtGmSalesEfficiencyScoreCard:setBundle basename="properties.labels.sales.GmSalesEfficiencyScoreCardReport"/> 
<bean:define id="screenName" name="frmSalesEfficiencyScoreCard" property="screenName" type="java.lang.String"></bean:define>
<bean:define id="screenType" name="frmSalesEfficiencyScoreCard" property="screenType" type="java.lang.String"></bean:define>
<bean:define id="strOpt" name="frmSalesEfficiencyScoreCard" property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="drillDownName" name="frmSalesEfficiencyScoreCard" property="drillDownName" type="java.lang.String"></bean:define>

<%

String strsalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");

String strWikiTitle = GmCommonClass.parseNull((String)GmCommonClass.getWikiTitle("SCORE_CARD_BY_"+screenType));

//PC-3528: getting the access level
String strAccessLvl = GmCommonClass.parseZero((String)session.getAttribute("strSessAccLvl"));
int intAccessLvl = Integer.parseInt(strAccessLvl);

%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Scorecard - Sales report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<!-- Method allows user to add new row from clipboard. New row is being added to the last position in the grid. -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>

<!-- paste content of clipboard into block selection of grid -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script language="JavaScript" src="<%=strsalesJsPath%>/GmSalesEfficiencyScoreCardReport.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/jquery-latest.js"></script>
<script type="text/javascript">
var screenTypeVal = '<%=screenType%>';
var hDrillDownVal = '<%=drillDownName %>';
var hUserAccessLevel = '<%=intAccessLvl %>';
</script>

<style type="text/css" media="all">
@import url("styles/screen.css"); 
</style>
<style> div.gridbox_dhx_skyblue table.hdr td { text-align: center; vertical-align: middle !important; } </style>
<style type="text/css">
         table.DtTable1550 {
             margin: 0 0 0 0;
             width: 1550px;
             border: 1px solid #676767;
             overflow:auto;
         }
     </style>
</HEAD>
<body leftmargin="15" topmargin="10" onLoad="fnOnPageLoad('<%=screenType%>'); ">
<html:form action="<%=strOpt %>" > 
<html:hidden property="systemId"/>
<html:hidden property="adId"/>
<html:hidden property="distId"/>
<html:hidden property="drillDownName"/>

 <table class="DtTable1300" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="9" height="25" class="RightDashBoardHeader">&nbsp;<%=screenName%> </td>
				<td height="25" align="right" class=RightDashBoardHeader><fmtGmSalesEfficiencyScoreCard:message key="IMG_ALT_HELP" var="varHelp"/>				    
				 <img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
             </tr>
             
           <tr>
				<td colspan="10">
					    <jsp:include page="/sales/GmSalesFilters.jsp" >
					    <jsp:param name="HIDE" value="SYSTEM" />
						<jsp:param name="FRMNAME" value="frmSalesEfficiencyScoreCard" />
						<jsp:param name="URL" value="<%=strOpt %>" />	
						
					</jsp:include>
				</td>
			</tr>
		
		<tr>
				<td colspan="10" height="30" align="center">
				<div id="btn_scoreCard">
						<fmtGmSalesEfficiencyScoreCard:message key="SCORE_CARD_ONLY" var="varScorecardOnly"/>
					    <gmjsp:button gmClass="button" name="Btn_ScoreCard" value="${varScorecardOnly}" onClick="fnDisableProjectData ();" buttonType="Load" controlId="Btn_ScoreCard"/>
					    
					    <fmtGmSalesEfficiencyScoreCard:message key="SCORE_CARD_PROJECTION" var="varScorecardProj"/>
					    <gmjsp:button gmClass="button" name="Btn_ScoreCard_With_Proj" value="${varScorecardProj}" onClick="fnEnableProjectData ();" buttonType="Load" controlId="Btn_ScoreCard_With_Proj"/>
						<!-- PC-5101: Scorecard DO classification changes -->
						&nbsp;&nbsp;&nbsp;<b>Allowed Tag:</b>
						<select name="allowedTag" id="allowedTag" tabindex="3" class="RightText" onchange="fnChangeAllowedTags()"> <option id="0" value="0">All<option value="Y" selected>Y</option><option value="N">N</option></select>
				</div>
				</td>
		</tr>
		<logic:notEmpty name="frmSalesEfficiencyScoreCard" property="drillDownName">
		<tr height="30">
		<td colspan="10" class="RightTableCaption">&nbsp;<bean:write name="drillDownName"/></td>
		</tr>
		</logic:notEmpty>
		<tr>	
		   <tr class="oddshade"> 
				<td colspan="10"><div id="dataGridDiv" height="400px"></div></td>
		  </tr>
		  <tr id="DivExportExcel" height="25">
			<td colspan="6" align="center">
			    <div class='exportlinks'><fmtGmSalesEfficiencyScoreCard:message key="EXPORT_OPTS"/> :<img
					src='img/ico_file_excel.png' onclick="fnExportDtls();" />&nbsp;<a href="#"
					onclick="fnExportDtls();"><fmtGmSalesEfficiencyScoreCard:message key="EXCEL"/> 
				</a></div>
			</td>
		 </tr>
		 <tr><td colspan="4" align="center"  class="RegularText" height="20"><div  id="DivNothingMessage"><fmtGmSalesEfficiencyScoreCard:message key="NO_DATA_FOUND"/></div></td></tr>
       </table> 
 </html:form> 
  <%@ include file="/common/GmFooter.inc" %> 
</BODY>
</HTML>