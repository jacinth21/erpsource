 <%
/**********************************************************************************
 * File		 		: GmReportYTDExcel.jsp
 * Desc		 		: This screen is used to download the same in Excel format
 * Version	 		: 1.0
 * author			: RichardK
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<!-- \sales\GmReportYTDExcel.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%
try {

	GmServlet gm = new GmServlet();
	HashMap hmReturn = new HashMap();
	ArrayList alProjList = new ArrayList();
	
	String strSelected = "";
	String strCodeID = "";
	String strProjNm = "";
	String strProjId = "";
	
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	
	gm.checkSession(response,session);

	
	
	String strShowArrow = GmCommonClass.parseZero((String)request.getAttribute("Chk_ShowArrowFl"));
	String strShowPercent = GmCommonClass.parseZero((String)request.getAttribute("Chk_ShowPercentFl"));
	String strHideAmount = GmCommonClass.parseZero((String)request.getAttribute("Chk_HideAmountFl"));
	
	// Combobox default value
	String strHeader = (String)request.getAttribute("strHeader");
	String strProj = (String)request.getAttribute("Cbo_Proj");
	String strType = (String)request.getAttribute("Cbo_Type");
	

	strProj = (strProj == null)?"0":strProj; // If no value selected will set the combo box value to [Choose One]
	strType = (strType == null)?"0":strType; // Setting combo box value
	String strhAction = (String)request.getAttribute("hAction");
	
	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}
	
	strhAction = (strhAction == null)?"Load":strhAction;
	
	// To get the Project Combo Box value information 
	HashMap hmComonValue = (HashMap)request.getAttribute("hmComonValue");
	if (hmComonValue != null)
	{
		alProjList = (ArrayList)hmComonValue.get("PROJECTS");
	}
		
	// To Generate the Crosstab report
	gmCrossTab.setHeader("Sales By Distributor By Month");
	
	gmCrossTab.setLinkRequired(false);	// To Specify the link
	
	// To display percent changed information
	if (strShowPercent.equals("1"))
	{
		gmCrossTab.setReportType(gmCrossTab.CROSSTAB_SALE_PER);	
	}	
	if (strShowArrow.equals("1") )
	{
		gmCrossTab.setShowArrow(true);; // To Display % arrow information
	}

	if (strHideAmount.equals("1"))
	{
		gmCrossTab.setHideAmount(true);	
	}	
	// To specify if its unit or valye 
	gmCrossTab.setValueType(Integer.parseInt(strType));
	
	// Add Lines
	gmCrossTab.setColumnLineStyle("borderDark");
	//gmCrossTab.addLine("Name");
	
	// Setting Display Parameter
	gmCrossTab.setAmountWidth(70);
	gmCrossTab.setNameWidth(250);
	gmCrossTab.setRowHeight(22);
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	
	// Page Body Style
	gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD");
	gmCrossTab.addStyle("Name","ShadeMedGrayTD","ShadeDarkGrayTD");
	gmCrossTab.addStyle("Total","ShadeMedBlueTD","ShadeDarkBlueTD") ;
	
	gmCrossTab.setDisableLine(true);
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: YTD #</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style>
.borderDark {
	FONT-SIZE: 12px; 
	FONT-FAMILY: Helvetica,sans-serif; 
	BACKGROUND-COLOR: #7c816a
}

.borderLight {
	FONT-SIZE: 12px; 
	FONT-FAMILY: Helvetica,sans-serif; 
	BACKGROUND-COLOR: #cccccc
}

.ShadeDarkGrayTD{
	background-color: #c3c5b8;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeLightGrayTD{
	background-color: #dee0d2;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeMedGrayTD{
	background-color: #dee0d2;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeDarkBlueTD{
	background-color: #a0b3de;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}
.ShadeLightBlueTD{
	background-color: #f1f2f7;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeMedBlueTD{
	background-color: #e4e6f2;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeDarkGreenTD{
	background-color: #ccdd99;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeMedGreenTD{
	background-color: #e1f3a6;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeDarkOrangeTD{
	background-color: #feb469;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeMedOrangeTD{
	background-color: #ffecb9;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeLightOrangeTD{
	background-color: #fef2cc;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}
.ShadeBlueTD{
	background-color: #ecf6fe;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeRightTableCaptionBlueTD{
	FONT-SIZE: 9px; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
	background-color: #AACCE8;
}

.ShadeBlueTDSmall{
	background-color: #ecf6fe;
	COLOR : Black;
	font-family: sans-serif;
	font-size : 11px;
	font-style: normal;
	height: 20px;
}

.CrossRightText {
	FONT-SIZE: 9px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
}

.RightTextRedSmall { 
	FONT-SIZE: 11px; 
	COLOR: #FF0000; 
	FONT-FAMILY:sans-serif;
	text-decoration: None;
}

.RightTextSmall {
	FONT-SIZE: 9px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
}


	background-color: #dee0d2;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad()">
<FORM name="frmOrder" method="post" action = "<%=strServletPath%>/GmSalesYTDReportServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="DistributorID" value="<%=request.getAttribute("strDistributorID")%>">
<input type="hidden" name="Cbo_Name" value="">
<TABLE cellSpacing=0 cellPadding=0  border=0 width="1000">
<TBODY>
	<tr>
		<td align=center> <%=gmCrossTab.PrintCrossTabReport(hmReturn) %></td>
	</tr>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
