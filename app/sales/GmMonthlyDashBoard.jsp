 <%
/**********************************************************************************
 * File		 		: GmMonthlyDashBoard.jsp
 * Desc		 		: This screen is used to display Monthly Sales Dashboard
 * Version	 		: 1.0
 * author			: RichardK
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.PopChartUtils"%>
<%@ taglib prefix="fmtMonthlyDashBoard" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- sales\GmMonthlyDashBoard.jsp -->

<fmtMonthlyDashBoard:setLocale value="<%=strLocale%>"/>
<fmtMonthlyDashBoard:setBundle basename="properties.labels.sales.GmMonthlyDashBoard"/>

<%
String strSalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");
try {

	GmServlet gm = new GmServlet();
	HashMap hmReturn = new HashMap();
	

 
	String strWikiTitle = GmCommonClass.getWikiTitle("MONTHLY_SALES_DASHBOARD");
	String strCurrSign = GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb"));
	ArrayList alStateSales 	= new ArrayList();	
	ArrayList alTopAgent 	= new ArrayList();	
	ArrayList alTopAccount 	= new ArrayList();	
	ArrayList alMonthlySales = new ArrayList();	
	ArrayList alProjectSales = new ArrayList();	
	ArrayList alMonthDropDown = new ArrayList();
	ArrayList alYearDropDown = new ArrayList();
	
	gm.checkSession(response,session);

	
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	
	
	alStateSales = (ArrayList)hmReturn.get("STATESALE");
	alTopAgent = (ArrayList)hmReturn.get("TOPAGENT");
	alTopAccount = (ArrayList)hmReturn.get("TOPACCOUNT");
	alMonthlySales = (ArrayList)hmReturn.get("MONTHLYSALES");
	alProjectSales = (ArrayList)hmReturn.get("PROJECTSALES");
	alMonthDropDown = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALMONTHDROPDOWN"));
	alYearDropDown = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALYEARDROPDOWN"));
	int intWorkingDays = (Integer) hmReturn.get("WORKING_DAYS");
	
	// Other values
	StringBuffer sbSateScript = new StringBuffer();	
	StringBuffer sbTopAgentGrp = new StringBuffer();	
	StringBuffer sbTopAgentItem = new StringBuffer();	
	StringBuffer sbTopAccountGrp = new StringBuffer();	
	StringBuffer sbTopAccountItem = new StringBuffer();	
	
	StringBuffer  sbpcScript = new StringBuffer();	
	
	String strToMonth = GmCommonClass.parseNull((String)request.getAttribute("Cbo_ToMonth"));
	String strToYear = GmCommonClass.parseNull((String)request.getAttribute("Cbo_ToYear"));
	
	//Below code is to track down the visits on the report.
	String strUserId = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
	String strShUserName = GmCommonClass.parseNull((String)session.getAttribute("strSessShName"));
	String strUserName = strUserId + " - " + strShUserName;
	//Visit code ends
	
	String strFCExportServer = GmCommonClass.parseNull((String) GmCommonClass.getString("FUSIONCHART_EXPORT_SERVER")); 
	HashMap hmCharts = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HMCHARTS"));
	String strTopDistJSON =  GmCommonClass.parseNull((String)hmCharts.get("TOPDISTJSON"));
	String strTopAcctJSON =  GmCommonClass.parseNull((String)hmCharts.get("TOPACCTJSON"));
	String strDailyJSON =  GmCommonClass.parseNull((String)hmCharts.get("DAILYSALESJSON"));
	String strProductJSON =  GmCommonClass.parseNull((String)hmCharts.get("PRODUCTJSON"));
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: YTD #</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script type="text/javascript" src="<%=strExtWebPath%>/fusionchart/fusioncharts.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/fusionchart/fusioncharts.maps.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/fusionchart/fusioncharts.usa.js"></script>
<script type="text/javascript" src="<%=strSalesJsPath%>/GmMonthlyDashboard.js"></script>
<script> downloadPiwikJS();</script>
<script> fnTracker('Monthly Sales Dashboard', '<%=strUserName%>');</script>
<script>
var topDistBarDataArray =eval(<%=strTopDistJSON%>);
var highestDistVal = parseInt(topDistBarDataArray.yaxismaxvalue);
var topAcctBarDataArray =eval(<%=strTopAcctJSON%>);
var highestAcctVal = parseInt(topDistBarDataArray.yaxismaxvalue);
var topDailyBarDataArray =eval(<%=strDailyJSON%>);
var highestDailyVal = parseInt(topDailyBarDataArray.yaxismaxvalue);
var totalSales = parseFloat(topDailyBarDataArray.totalamount);
var workingDays = parseInt(topDailyBarDataArray.workdays);

var productDataArray =eval(<%=strProductJSON%>); 


var currSymbol ="<%=strCurrSign%>";
var fcExportServer = '<%=strFCExportServer%>';

function fnSubmit()
{
	document.frmOrder.submit();
}

function fnCallDetail(val)
{
	document.frmOrder.DistributorID.value = val;
	document.frmOrder.hAction.value = "LoadAccount";
	document.frmOrder.submit();
}

function fnCountryName(val)
{
  
}

function fnLoad()
{	document.frmOrder.Cbo_ToMonth.value = '<%=request.getAttribute("Cbo_ToMonth")%>';
	document.frmOrder.Cbo_ToYear.value = '<%=request.getAttribute("Cbo_ToYear")%>';
	fnTotalAvg();
	fnDailyBarChart();
	fnProductPieChart();
	fnTopDistBarChart();
	fnTopAcctBarChart();
}


</script>
</HEAD>

<BODY leftmargin="20" topmargin="5" onLoad="javascript:fnLoad();">
<FORM name="frmOrder" method="post" action = "<%=strServletPath%>/GmMonthlyDashboardServlet">
<input type="hidden" name="hAction" value="">
<TABLE cellSpacing=0 cellPadding=0  border=0 width="800">
<TBODY>
	<tr>
		<td rowspan="10" class=Line noWrap width=1></td>
		<td class=Line noWrap height=1></td>
		<td rowspan="10" class=Line noWrap width=1></td>
	</tr>
	<tr>
		<td>
			<TABLE cellSpacing=0 cellPadding=0  border=0 width="100%">
				<tr class=Line>
					<td height=25 class=RightDashBoardHeader><fmtMonthlyDashBoard:message key="LBL_MONTHLY_SALES"/></td>
					<td  height="25" class="RightDashBoardHeader">
					<fmtMonthlyDashBoard:message key="IMG_ALT_HELP" var = "varHelp"/>
					<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
				</tr>
			</TABLE>
		</td>
	</tr>
	<tr class=Line><td noWrap height=1></td></tr>
	<tr class=borderDark>
		<td noWrap height=1>  </td>
	</tr>
	<tr align="left"><td>&nbsp;
		<!-- Combo Filter -->
		<table cellSpacing=0 cellPadding=0  border=0>
		<tr height=30>
			<TD class=RightText width=100><fmtMonthlyDashBoard:message key="LBL_FROM_DATE"/>:</TD>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<TD class=RightText width=105><gmjsp:dropdown controlName="Cbo_ToMonth"  seletedValue="<%=strToMonth%>" value="<%=alMonthDropDown%>" 
			codeId = "CODENMALT"  codeName = "CODENM"  tabIndex="1" />
			</TD>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<TD class=RightText width=75><gmjsp:dropdown controlName="Cbo_ToYear"  seletedValue="<%=strToYear%>" value="<%=alYearDropDown%>" 
			codeId= "CODENM"  codeName = "CODENM"  tabIndex="2" />
			</TD>
			<TD> <fmtMonthlyDashBoard:message key="BTN_GO" var="varGo"/><gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" gmClass="button" buttonType="Load" tabindex="5" onClick="fnReloadSales('LoadStateSales');"/></TD>
		</TR>
		<tr>
			<td colspan="7" height="20">
				<jsp:include page="/sales/GmSalesFilters.jsp" >
					<jsp:param name="HIDE" value="SYSTEM" />
					<jsp:param name="FRMNAME" value="frmOrder" />
					<jsp:param name="HACTION" value="LoadStateSales" />
				</jsp:include> 
			</td>
		</tr>
		</TABLE>
	</td></tr>
	<tr><td noWrap height=3></td></tr>
				<tr>
					<td>
						<table>
							<tr>
								<td height="30" class="RightDashBoardHeader" align="center">
									<fmtMonthlyDashBoard:message key="LBL_TOTAL_SALES"/> - &nbsp;&nbsp;<span id="spnTotal"></span></td>
								<td height="30" class="RightDashBoardHeader" align="center">
									<fmtMonthlyDashBoard:message key="LBL_AVERAGE_SALES"/> - &nbsp;&nbsp;<span id="spnAvg"></span></td>
							</tr>
							<tr>
								<td>
									<div id="divDailyContainer"></div>
								</td>
								<td>
									<div id="divProductContainer"></div>
								</td>
							</tr>
							<tr>
								<td>
									<div id="divTopDistBarContainer"></div>
								</td>
								<td>
									<div id="divTopAcctBarContainer"></div>
								</td>
							</tr>

						</table>
					</td>
				</tr>
				<tr><td noWrap height=5></td></tr>
	<tr class=borderDark><td noWrap height=1>  </td></tr>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>
<%@ include file="/common/GmFooter.inc" %> 
</HTML>
