<%@page import="java.util.Locale"%>
<%
/**********************************************************************************
 * File		 		: GmAccountPriceReportExce.jsp
 * Desc		 		: This screen is used for the Account Price Report to Excel
 * Version	 		: 1.0
 * author			: Velu 
************************************************************************************/
%>


<%@ page import="java.util.ArrayList,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>

<%@ taglib prefix="fmtAccountPriceReportExcel" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\GmAccountPriceReportExcel.jsp -->
<%
Locale locale = null;
String strLocale = "";

String strJSLocale = "";

String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));

if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtAccountPriceReportExcel:setLocale value="<%=strLocale%>"/>
<fmtAccountPriceReportExcel:setBundle basename="properties.labels.sales.GmAccountPriceReportExcel"/>

<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");
	String strcurrSymbol = GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb"));
	String strAccCurrSymb = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_SYMB"));
	strcurrSymbol = strAccCurrSymb.equals("")?strcurrSymbol:strAccCurrSymb;
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strAccId  = GmCommonClass.parseNull((String)request.getAttribute("hAccId"));
	String strOpt  = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
	String strQueryBy = GmCommonClass.parseNull((String)request.getAttribute("hQueryBy"));
	
	ArrayList alReturn = new ArrayList();
	
	HashMap hmLoop = new HashMap();
	HashMap hmResult = new HashMap();
	
	String strLblPrice = "";
	String strLblDiscount = "";
	String strLblUnitPrice = "";
	String strPartNum = "";
	String strPrice = "";
	String strProjetName = "";
	String strPartDesc = "";
	String strGPO = "";
	String strUnitPrice = "";
	String strDiscount = "";
	String strIncrPrice = "";
	int intLength = 0;
	String strColsPan = "4"; 
	hmResult = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("ACCDETAIL"));
	alReturn = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("PRICINGLISTREPORT"));
	GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.sales.GmAccountPriceReportExcel", strSessCompanyLocale);
	if(alReturn.size() > 0 )
		 intLength = alReturn.size();
	
	if(strOpt.equals("CurrentPrice")){
		strLblPrice 	= 	GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PRICE"))+" " +strcurrSymbol;
		strLblUnitPrice = 	"";
		strLblDiscount 	= 	"";
		strColsPan = "5";
	}else if(strOpt.equals("PriceIncrease")){
		strLblPrice 	= 	GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_CURRENT_PRICE"))+" " +strcurrSymbol;
		strLblUnitPrice	=	GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PRICE_INCREASE"))+" " +strcurrSymbol;
		strLblDiscount	= 	"";
		strColsPan = "6";
	}else{
		strLblPrice 	= 	GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_LIST_PRICE"))+" " +strcurrSymbol;
		strLblUnitPrice	=	GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_UNIT_PRICE"))+" " +strcurrSymbol;
		strLblDiscount 	= 	GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_DISC"));
		strColsPan = "7";
	}
 
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Account Price Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.RightTableCaption {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
}
.Line{
	background-color: #676767;
}
.RightText {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
}
</style>
</HEAD>
 
<BODY leftmargin="20" topmargin="10">
				<table border="0"  width="100%" cellspacing="0" cellpadding="0">
				    <tr class="RightTableCaption">
						<td height="25" align="center" colspan="<%=strColsPan%>"><font size="+2"><fmtAccountPriceReportExcel:message key="LBL_CONFIDENTIAL"/></font></td>
					</tr>
					<tr class="RightTableCaption">
						<td height="25" align="center" colspan="<%=strColsPan%>"><font size="+2"><fmtAccountPriceReportExcel:message key="LBL_ACC_PRICE_REPORT"/></font></td>
					</tr>
					<%-- <tr><td class="Line" height="1" colspan="<%=strColsPan%>"></td></tr> --%>
					<tr>
						<% if(strQueryBy.equals("System Name"))
								{
						%>
						<td width="300"><b><fmtAccountPriceReportExcel:message key="LBL_SYS_NAME"/></b></td>
						<% 	
								}
								else
								{
						%>
						<td width="300"><b>Project Name</b></td>			
						<% 
								}
						%>
     					<td width="80"><b><fmtAccountPriceReportExcel:message key="LBL_PART"/></b></td>
						<td width="350"><b><fmtAccountPriceReportExcel:message key="LBL_PART_DESC"/></b></td>
						<td width="100" align="center"><b><%=strLblPrice%></b></td>
						<%if(!strLblUnitPrice.equals("")){ %>
							<td width="100" align="center"><b><%=strLblUnitPrice%></b></td>
						<%} %>
						<%if(!strLblDiscount.equals("")){ %>
							<td width="70" align="center"><b><%=strLblDiscount%></b></td>
						<%} %>
						<td width="60" align="center"><b><fmtAccountPriceReportExcel:message key="LBL_GRP_PRICE_BOOK"/></b></td>
					</tr>
					<%-- <tr><td class="Line" height="1" colspan="<%=strColsPan%>"></td></tr> --%>
        <%					if (intLength > 0){
							long intDiscount = 0;
							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alReturn.get(i);
								if(strQueryBy.equals("System Name"))
								{
								strProjetName = GmCommonClass.parseNull((String)hmLoop.get("SYSNM"));
								}
								else
								{
								strProjetName = GmCommonClass.parseNull((String)hmLoop.get("PJNM"));
								}
 								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PID"));
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strGPO = GmCommonClass.parseNull((String)hmLoop.get("GPO"));
								strPrice = Double.parseDouble((String)hmLoop.get("TIERI").toString())+ "";
								strUnitPrice = Double.parseDouble((String)hmLoop.get("UPRICE").toString()) + "";
								strIncrPrice = Double.parseDouble((String)hmLoop.get("PRICE2008").toString()) + "";
								intDiscount = Math.round(Double.parseDouble((String)hmLoop.get("DOFFERED").toString())*100);
								strDiscount = intDiscount + "%";
								if(strOpt.equals("PriceIncrease")){
									strPrice = 	strUnitPrice;
									strUnitPrice = strIncrPrice;
								}else if(strOpt.equals("CurrentPrice")){
									strPrice = 	strUnitPrice;
								}
								
 							%>
					<tr>
						<td><%=strProjetName%></td>
						<td align="left"><%=strPartNum%></td>
						<td><%=strPartDesc%></td>
						<td class="RightText"><%=strcurrSymbol%><%=GmCommonClass.getStringWithCommas(strPrice)%></td>
						<%if(!strLblUnitPrice.equals("")){ %>
						<td class="RightText"><%=strcurrSymbol%><%=GmCommonClass.getStringWithCommas(strUnitPrice)%></td>
						<%} %>
						<%if(!strLblDiscount.equals("")){ %>
							<td class="RightText"><%=strDiscount%></td>
						<%}%>
						<td align="center"><%=strGPO%></td>
					</tr>
		<%
				}
				}// end of FOR
		%> 
				<tr><td colspan="<%=strColsPan%>" height="1" class="Line"></td></tr>
			    <tr class="RightTableCaption">
					<td height="25" align="center" colspan="<%=strColsPan%>"><font size="+2"><fmtAccountPriceReportExcel:message key="LBL_CONFIDENTIAL"/></font></td>
				</tr>
				</table>
</BODY>
</HTML> 
