 <%
/**********************************************************************************
 * File		 		: GmRegnDistTerrMapping.jsp
 * Desc		 		: This screen is used to display the mapping between 
 * 					  Region, Distrobutor and Territory 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ include file="/common/GmHeader.inc" %>
<% 
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<%@ taglib prefix="fmtRegnDistTerrMapping" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!--  sales\GmRegnDistTerrMapping.jsp-->
<fmtRegnDistTerrMapping:setLocale value="<%=strLocale%>"/>
<fmtRegnDistTerrMapping:setBundle basename="properties.labels.sales.GmRegnDistTerrMapping"/>


<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	String strCommonPath = GmCommonClass.getString("GMCOMMON");
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	ArrayList alReturn = (ArrayList)hmReturn.get("REPORT");
	int intLength = alReturn.size();

	String strGraphTitle = "";
	int intSize = 0;
	HashMap hmLoop = new HashMap();

	String strId = "";
	String strNextAccId = "";
	String strName = "";
	String strDistId = "";
	String strDistName = "";
	String strDate = "";
	String strSales = "";
	String strDay = "";
	String strOrdId= "";
	String strADName = "";

	String strTerrId = "";

	String strShade = "";
	String strNextId = "";
	int intCount = 0;
	String strLine = "";
	String strTotal = "";
	String strAcctTotal = "";
	String strAvgDaily = "";
	
	boolean blFlag = false;
	double dbSales = 0.0;
	double dbAcctTotal = 0.0;
	double dbTotal = 0.0;
	double dbAvgDaily = 0.0;
	double dbTempTotal = 0.0;
	double dbDistTotal = 0.0;
	String strOrdDate = "";
	
	ArrayList alDist = new ArrayList();
	ArrayList alSales = new ArrayList();
	ArrayList alDate = new ArrayList();

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Sales Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>

function fnSubmit()
{
	document.frmAccount.submit();
}

function Toggle(val)
{
	
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className="ShadeRightTableCaptionBlue";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className="";
		tabobj.style.background="#ffffff";
	}
}

function fnShowAccounts(val)
{
	windowOpener("<%=strServletPath%>/GmTerritoryServlet?hAction=ShowAcc&hType=REP&hId="+val,"TerrAcc","resizable=yes,scrollbars=yes,top=250,left=250,width=600,height=300");	
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST">
<input type="hidden" name="hAction" value="Reload">

	<table border="0" width="650" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="648" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" class="RightDashBoardHeader" colspan="5"><fmtRegnDistTerrMapping:message key="LBL_REGION"/></td></tr>
					<tr><td class="Line" colspan="4"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="100"><fmtRegnDistTerrMapping:message key="LBL_RGN"/></td>
						<td width="350"><fmtRegnDistTerrMapping:message key="LBL_DIST"/></td>
						<td width="200"><fmtRegnDistTerrMapping:message key="LBL_TERRITORY"/></td>
						<td width="200"><fmtRegnDistTerrMapping:message key="LBL_SALES_REP"/></td>
					</tr>
<%
						int intAccCount = 0;
						boolean bolAcFl = false;
						if (intLength > 0)
						{
							HashMap hmTempLoop = new HashMap();

							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alReturn.get(i);
								if (i<intLength-1)
								{
									hmTempLoop = (HashMap)alReturn.get(i+1);
									strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("RID"));
									strNextAccId = GmCommonClass.parseNull((String)hmTempLoop.get("DID"));
								}
								strADName = GmCommonClass.parseNull((String)hmLoop.get("ADNAME"));
								strDistId = GmCommonClass.parseNull((String)hmLoop.get("RID"));
								strDistName = GmCommonClass.parseNull((String)hmLoop.get("RGNAME")).concat("&nbsp;(").concat(strADName).concat(")");
								strId	= GmCommonClass.parseNull((String)hmLoop.get("DID"));
								strName = GmCommonClass.parseNull((String)hmLoop.get("DNAME"));
								strTerrId = GmCommonClass.parseZero((String)hmLoop.get("REPID"));
								strOrdId = GmCommonClass.parseZero((String)hmLoop.get("TNAME"));
								strOrdDate = GmCommonClass.parseZero((String)hmLoop.get("RNAME"));
								
								if (strDistId.equals(strNextId))
								{
									intCount++;
									strLine = "<TR><TD colspan=4 height=1 class=Line></TD></TR>";
								}
								else
								{
									strLine = "<TR><TD colspan=4 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
										strDistName = "";
										strLine = "";
									}
									else
									{
										blFlag = true;
									}
									intCount = 0;
								}
								
								if (strId.equals(strNextAccId))
								{
									intAccCount++;
								}
								else
								{
									if (intAccCount > 0)
									{
										strName = "";
										strLine = "";
									}
									else
									{
										bolAcFl = true;
									}
									intAccCount = 0;
								}
																
								strAcctTotal = ""+dbAcctTotal;

								if (intCount > 1)
								{
									strDistName = "";
									strLine = "";
								}
						
								strShade = (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows

								out.print(strLine);
								strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
								if (intCount == 1 || blFlag)
								{
%>
					<tr id="tr<%=strDistId%>">
						<td colspan="4" height="20" class="RightText">&nbsp;<A class="RightText" title="Click to Expand" href="javascript:Toggle('<%=strDistId%>')"><%=strDistName%></a></td>
					</tr>
					<tr>
						<td colspan="4"><div style="display:none" id="div<%=strDistId%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tab<%=strDistId%>">
<%
								blFlag = false;
								}
								
								if (intAccCount == 1 || bolAcFl)
								{
%>
								<tr id="trAcc<%=strDistId%><%=strId%>">
									<td width="100">&nbsp;</td>
									<td width="300" height="20" class="RightText">&nbsp;<A class="RightText" title="Click to Expand" href="javascript:Toggle('Acc<%=strDistId%><%=strId%>')"><%=strName%></a></td>
								</tr>
								<tr>
									<td colspan="3"><div style="display:none" id="divAcc<%=strDistId%><%=strId%>">
										<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tabAcc<%=strDistId%><%=strId%>">
<%
								bolAcFl = false;
								}
%>

											<tr <%=strShade%>>
												<td width="100" class="RightText" height="17">&nbsp;</td>
												<td width="250" class="RightText">&nbsp;&nbsp;</td>
												<td width="200">&nbsp;<a class=RightText href= javascript:fnShowAccounts(<%=strTerrId%>)><%=strOrdId%></a></td>
												<td width="200" class="RightText">&nbsp;<%=strOrdDate%></td>
											</tr>
											<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
<%					
								if (intAccCount == 0 ||  i == intLength-1)
								{
%>
						<script>
							//document.all.tdAcc<%=strDistId%><%=strId%>.innerHTML = "$<%=GmCommonClass.getStringWithCommas(strAcctTotal)%>&nbsp;";
						</script>
							
<%	
									if ( intAccCount == 0)
									{
%>
						</table></div></td></tr>
<%
									}
								dbAcctTotal = 0.0;
								}
%>
					
<%					
								if ( intCount == 0 || i == intLength-1)
								{
									strAcctTotal = ""+dbDistTotal;
%>
							</table></div>
						</td>
					</tr>
					<script>
						//document.all.td<%=strDistId%>.innerHTML = "$<%=GmCommonClass.getStringWithCommas(strAcctTotal)%>&nbsp;";
					</script>
<%
									dbDistTotal = 0.0;					
								}
								strTotal = ""+dbTotal;
							}// end of FOR
%>
							</table></div>
						</td>
					</tr>
<%
						} // End of IF
						else{
%>
					<tr><td colspan="4" class="Line"></td></tr>					
					<tr><td colspan="4" class="RightTextRed" height="50" align=center><fmtRegnDistTerrMapping:message key="LBL_NO_DATA"/> ! </td></tr>
<%					
						}
%>
				</table>
			</td>
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif"></td>
		</tr>
		<tr>
			<td colspan="5" height="1" bgcolor="#666666"></td>
		</tr>
    </table>

</FORM>


<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
