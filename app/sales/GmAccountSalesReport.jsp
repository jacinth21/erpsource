
<%
	/**********************************************************************************
	 * File		 		: GmAccountSalesReport.jsp
	 * Desc		 		: This screen is used to display Accounts detail by Sales
	 * Version	 		: 1.0
	 * author			: Dhinakaran James
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtAccountSalesReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\GmAccountSalesReport.jsp -->

<fmtAccountSalesReport:setLocale value="<%=strLocale%>"/>
<fmtAccountSalesReport:setBundle basename="properties.labels.sales.GmAccountSalesReport"/>


<%
String strsalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");
	String gridData = "";
String strWikiTitle = GmCommonClass.getWikiTitle("FIELDSALES_REPORT");
	if (request.getAttribute("strXmlData") != null) {
		gridData = request.getAttribute("strXmlData").toString();
	}
	String strCurrSymbol = GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb"));
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Account Report</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<%-- <script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script> --%>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strsalesJsPath%>/GmSalesFilter.js"></script>
	
<script>

var gridObjData ='<%=gridData%>';
var mygrid;
var currSymb = '<%=strCurrSymbol%>';

eXcell_ron.prototype.setValue=function(val){ 
	   if (!val||val.toString()._dhx_trim() == ""){
	       this.cell._clearCell=true;
	       val = 0;
	   } else
	       this.cell._clearCell=false;
	   this.setCValue(this.grid._aplNF(val, this.cell._cellIndex));
	}

function fnOnPageLoad() {	
	if(gridObjData!='')
	{
			mygrid = setDefalutGridProperties('dataGridDiv');	
			mygrid.setNumberFormat("" +currSymb+ " 0,000.00",5,".",",");
			mygrid.setNumberFormat("" +currSymb+ " 0,000.00",6,".",",");
			mygrid.setNumberFormat("" +currSymb+ " 0,000.00",7,".",",");
			mygrid.enableMultiline(false);	
			mygrid = initializeDefaultGrid(mygrid,gridObjData);
			mygrid.enableDistributedParsing(true);
		   	mygrid.groupBy(1,["#title","","","","","#stat_total","#stat_total","#stat_total"]);			   	  	
     		mygrid.collapseAllGroups();  
			mygrid.expandAllGroups(); 	 	   			   			   
		   	mygrid.setColumnHidden(1,true);
		   	mygrid.attachHeader('#rspan,#text_filter,#text_filter,#text_filter,#select_filter,#numeric_filter,#numeric_filter,#numeric_filter');
		   	document.getElementById('xls').style.visibility = 'visible';
	}else
	{
		document.getElementById('xls').style.visibility = 'hidden'; 
	}
}

function fnExportTest(str){
	mygrid.setColumnHidden(0,true);
	mygrid.setColumnHidden(1,false);
	mygrid.expandAllGroups();
	if(str=="excel"){
		mygrid.toExcel('/phpapp/excel/generate.php');
	}else{
		mygrid.toExcel('/phpapp/pdf/generate.php');
	}
	mygrid.setColumnHidden(0,false);
	mygrid.setColumnHidden(1,true);
	mygrid.collapseAllGroups();  
}

function fnSubmit(val)
{
	document.frmAccount.hColumn.value = val;
	document.frmAccount.submit();
}

function fnCallEdit(val)
{
	document.frmAccount.hId.value = val;
	document.frmAccount.action = "<%=strServletPath%>/GmCustFrameServlet?hFrom=AccountReport";
	document.frmAccount.submit();
}

function Toggle(val)
{
	
	var obj = eval("document.all.div"+val);
	
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className= "ShadeRightTableCaptionBlue";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className= "";
		tabobj.style.background="#ffffff";
	}
}
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<FORM name="frmAccount" method="POST"
	action="<%=strServletPath%>/GmAccountReportServlet"><input
	type="hidden" name="hColumn" value=""> <input type="hidden"
	name="hId" value=""> <input type="hidden" name="hPgToLoad"
	value="GmAccountServlet"> <input type="hidden" name="hSubMenu"
	value="Setup">
<table class="border" width="900" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="3" bgcolor="#666666"></td>
	</tr>
	<tr>
		<td bgcolor="#666666" width="1"></td>
		<td width="900" valign="top">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="RightDashBoardHeader" colspan="7">
				<fmtAccountSalesReport:message key="LBL_REPT_ACC_SALES"/></td>
				<td  height="25" class="RightDashBoardHeader" style="display: none;">
						<fmtAccountSalesReport:message key="IMG_ALT_HELP" var = "varHelp"/>
						<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
			</tr>
			<tr>
			<td colspan="10">
			<table>
			<tr>
				<td>
					<jsp:include page="/sales/GmSalesFilters.jsp" >
						<jsp:param name="FRMNAME" value="frmAccount" />
						<jsp:param name="HIDE" value="SYSTEM" />				
					</jsp:include>
				</td>
			</tr>
			<tr>
				<td>
				<div id="dataGridDiv" width="900px" height="450px"></div>
				</td>
			</tr>
			<tr align="center">
				<td  >
				<div id="xls" class='exportlinks'><fmtAccountSalesReport:message key="DIV_EXPORT_OPT"/> : <img
					src='img/ico_file_excel.png' />&nbsp;<a href="#"
					onclick="fnExportTest('excel');"> <fmtAccountSalesReport:message key="DIV_EXCEL"/> </a></div>
				</td>
			</tr>
		</table>
		</td>
		<td bgcolor="#666666" width="1"></td>
	</tr>
	
</table>
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>