 <%
/**********************************************************************************
 * File		 		: GmSalesConsignmentReport.jsp
 * Desc		 		: This screen is used to display Sales Consignment report based on 
 *					  virtual load  	
 * Version	 		: 1.0
 * author			: RichardK
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ include file="/common/GmHeader.inc" %>


<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<%@ taglib prefix="fmtSalesConsignmentReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!--sales\GmSalesConsignmentReport.jsp  -->
<fmtSalesConsignmentReport:setLocale value="<%=strLocale%>"/>
<fmtSalesConsignmentReport:setBundle basename="properties.labels.sales.GmSalesConsignmentReport"/>
<HTML>
<HEAD>
<TITLE> Globus Medical: Consignment Report #</TITLE>
<%
try {

	GmServlet gm = new GmServlet();
	
	HashMap hmReturn = new HashMap();
	
	ArrayList alProjList = new ArrayList();
	ArrayList alStyle 		= new ArrayList();
	ArrayList alDrillDown 	= new ArrayList();
	
	String strSelected = "";
	String strCodeID = "";
	String strProjNm = "";
	String strProjId = "";
	
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	
	gm.checkSession(response,session);

	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	String strCommonPath = GmCommonClass.getString("GMCOMMON");
	
	// Combobox default value
	String strHeader = (String)request.getAttribute("strHeader");
	//String strProj = (String)request.getAttribute("Cbo_Proj");
	String strProj = (String)request.getAttribute("hSetNumber");
    String hAreaDirector = GmCommonClass.parseNull((String)request.getAttribute("hAreaDirector"));
	
	String strType = (String)request.getAttribute("Cbo_Type");
	

	
	String strhAction = (String)request.getAttribute("hAction");
	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}
	
	ArrayList alConTypes = (ArrayList)request.getAttribute("alConTypes");
	ArrayList alPeriods = (ArrayList)request.getAttribute("alPeriods");
	ArrayList alSetTypes = (ArrayList)request.getAttribute("alSetTypes");
	
	String strConType = GmCommonClass.parseNull((String)request.getAttribute("strConType"));
	String strPeriod  =  GmCommonClass.parseNull((String)request.getAttribute("strPeriod"));
	String strSetType = GmCommonClass.parseNull((String)request.getAttribute("strSetType"));	
	strType = (strType == null || strType.equals(""))?"1":strType; // Setting combo box value
	%>
	
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script>

function fnSort(varColumnName, varSortOrder)
{
	document.frmOrder.hSortColumn.value = varColumnName;
	document.frmOrder.hSortOrder.value = varSortOrder;
	fnGo();
}

function fnSubmit()
{
	document.frmOrder.submit();
}

function fnCallAccount(val,Hierarchy )
{
	if (Hierarchy=='1') 
	{
		document.frmOrder.hAreaDirector.value = val;
	}
	if (Hierarchy=='0') 
	{
		document.frmOrder.DistributorID.value = val;
	}
	
	document.frmOrder.hAction.value = "LoadAccount";
	document.frmOrder.submit();
}

// Filter by distributor consignment 
function fnCallCons(val, Type)
{
	document.frmOrder.hAction.value = "LoadDistSummary";
	document.frmOrder.hAreaDirector.value = val;
	document.frmOrder.action ="<%=strServletPath%>/GmSalesConsignmentServlet";
	document.frmOrder.submit();
}



function fnCallSetNumber(val,Hierarchy )
{
	if (Hierarchy=='1') 
	{
		document.frmOrder.hAreaDirector.value = val;
	}

	if (Hierarchy=='0') 
	{
		document.frmOrder.DistributorID.value = val;
	}


	document.frmOrder.action ="<%=strServletPath%>/GmSetYTDReportServlet";
	document.frmOrder.hAction.value = "LoadGroup";
	document.frmOrder.submit();
}



function fnLoad()
{
	document.frmOrder.Cbo_FromMonth.value = '<%=request.getAttribute("Cbo_FromMonth")%>';
	document.frmOrder.Cbo_ToMonth.value = '<%=request.getAttribute("Cbo_ToMonth")%>';
	document.frmOrder.Cbo_FromYear.value = '<%=request.getAttribute("Cbo_FromYear")%>';
	document.frmOrder.Cbo_ToYear.value = '<%=request.getAttribute("Cbo_ToYear")%>';
	document.frmOrder.hAreaDirector.value = '<%=hAreaDirector%>';
	document.frmOrder.Cbo_Type.value = "<%=strType%>";	
}

function Toggle(val)
{
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);
	
	if (obj.style.display == 'none')
	{
		obj.style.display = '';
	}
	else
	{
		obj.style.display = 'none';
	}
}

function fnGo()
{  	
	document.frmOrder.action = "<%=strServletPath%>/GmSalesConsignmentServlet";
	document.frmOrder.submit();
}

function fnDownloadVer()
{
	
	windowOpener("<%=strServletPath%>/GmSalesYTDReportServlet?hExcel=Excel&hAction="+hact+"&Cbo_FromMonth="+hFromMonth+"&Cbo_FromYear="+hFromYear+"&Cbo_ToMonth="+hToMonth+"&Cbo_ToYear="+hToYear+"&Chk_ShowArrowFl="+cShowArrowFl+"&Chk_ShowPercentFl="+cShowPercentFl+"&Chk_HideAmountFl="+cHideAmount+"&Cbo_Proj="+hProjectID+"&Cbo_Type="+hType+"&DistributorID="+hDistributorID+"&hcboName="+cboName,"INVEXCEL","resizable=yes,scrollbars=yes,top=300,left=200,width=800,height=490");
}


</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad()">
<FORM name="frmOrder" method="post" action = "<%=strServletPath%>/GmSalesYTDReportServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="DistributorID" value="null">
<input type="hidden" name="hAccountID" value="null">
<input type="hidden" name="SetNumber" value="<%=request.getAttribute("hSetNumber")%>">
<input type="hidden" name="hRepID" value="null">
<input type="hidden" name="hPartNumber" value="null">
<input type="hidden" name="hTerritory" value="null">
<input type="hidden" name="hAreaDirector" value="null">

<input type="hidden" name="Cbo_FromMonth" value="">
<input type="hidden" name="Cbo_ToMonth" value="">
<input type="hidden" name="Cbo_FromYear" value="">
<input type="hidden" name="Cbo_ToYear" value="">

<input type="hidden" name="Cbo_Name" value="">

<!-- Custom tag lib code modified for JBOSS migration changes -->
<TABLE cellSpacing=0 cellPadding=0  border=0 width="1000">
<TBODY>
	<tr>
		<td rowspan="10" class=Line noWrap width=1></td>
		<td class=Line noWrap height=1></td>
		<td rowspan="10" class=Line noWrap width=1></td>
	</tr>
	<tr class=Line><td height=25 class=RightDashBoardHeader><%=strHeader %> </td></tr>
	<tr class=Line><td noWrap height=1></td></tr>
	<tr>
	  <td>
	   <table cellSpacing=0 cellPadding=0  border=0>			
			<TR height=30>
			    <td class=RightText align="right"><fmtSalesConsignmentReport:message key="LBL_TYPE"/>
				</td>
				<td>
					<gmjsp:dropdown controlName="Cbo_Con_Type"  seletedValue="<%= strConType %>" 	
					tabIndex="1"  value="<%= alConTypes%>" codeId="CODEID" codeName="CODENM"/>
				</td>	
				
				<td class=RightText align="right"><fmtSalesConsignmentReport:message key="LBL_PERIOD"/></td>
				
				<td>
					<gmjsp:dropdown controlName="Cbo_Period"  seletedValue="<%= strPeriod %>" 	
					tabIndex="2"  width="150" value="<%= alPeriods%>" codeId="CODEID" codeName="CODENM" />
				</td>
			 </tr>	
			 <tr>
				<td class=RightText align="right" width="100" ><fmtSalesConsignmentReport:message key="LBL_UNIT_DOL"/></td>	
				<td width="150"><select name=Cbo_Type class=RightText tabIndex="3">
					<option value=1><fmtSalesConsignmentReport:message key="LBL_DOLLAR"/></option> 
					<option value=0><fmtSalesConsignmentReport:message key="LBL_UNIT"/></option>
					</select> </td>		
				<td class=RightText align="right" width="100" ><fmtSalesConsignmentReport:message key="LBL_SET_TYPE"/></td>	
				<td>
					<gmjsp:dropdown controlName="Cbo_SetType"  seletedValue="<%= strSetType %>" 	
					tabIndex="4"  width="150" value="<%= alSetTypes%>" defaultValue="All" codeId="CODEID"  codeName="CODENM" />
				</td>			
				<TD width=100 align="center"> 
				<fmtSalesConsignmentReport:message key="BTN_GO" var="varGo"/><gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" onClick="javascript:fnGo();" gmClass="button" tabindex="5" buttonType="Load" />
				</TD>		
			</TR>
			<tr><td>&nbsp;</td></tr>
		</TABLE>
   	   </td>
	</tr>
	<!-- Addition Filter Information -->
	<tr class=Line><td noWrap height=1></td></tr>
<tr><td>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
		<jsp:include page="/sales/GmSalesFilterDetails.jsp" />
	</td></tr>	

	<%
	// To Generate the Crosstab report
	gmCrossTab.setHeader("Sales By Distributor By Month");
	
	gmCrossTab.setLinkType(strhAction);
	gmCrossTab.setLinkRequired(true);	// To Specify the link
	
	
	// To specify if its unit or valye 
	gmCrossTab.setValueType(Integer.parseInt(strType));
	
	// Add Lines
	gmCrossTab.setColumnLineStyle("borderDark");
	gmCrossTab.addLine("Name");
	
	// Setting Display Parameter
	gmCrossTab.setAmountWidth(30);
	gmCrossTab.setNameWidth(220);
	gmCrossTab.setRowHeight(22);
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	
	// Page Body Style
	gmCrossTab.setGeneralHeaderStyle("ShadeWhite");
	gmCrossTab.addStyle("Name","ShadeMedGrayTD","aaTopHeader");
	gmCrossTab.addStyle("Total","ShadeMedBlueTD","ShadeDarkBlueTD") ;
	
	gmCrossTab.setHeaderImage(true);
	
	// Setting for hierarchy color
	alStyle.add("ShadeLightBlueTD");
	//alStyle.add("ShadeLightBlueTD");
	//alStyle.add("ShadeLightBlueTD");
	//alStyle.add("ShadeLevelI");
	//alStyle.add("ShadeLevelII");
	gmCrossTab.setMultiColumnStyle(alStyle);

		
	//Setting drilldown information 
	if(hAreaDirector.equals("")) 
	{
	   alDrillDown.add("LoadGCONS"); // Maps to Account drilldown
	}
	else
	{
	   alDrillDown.add("");	
	}
	gmCrossTab.setDrillDownDetails(alDrillDown);
	gmCrossTab.setRowHighlightRequired(true);
	
	/******************************************* SORT COLUMNS **************************************************************************/	
	
	int intSortColumn =  request.getParameter("hSortColumn") == null ? -1 : Integer.parseInt((String)request.getParameter("hSortColumn"));
	int intSortOrder = Integer.parseInt(GmCommonClass.parseZero((String)request.getParameter("hSortOrder")));	
    
	gmCrossTab.setSortRequired(true);
	gmCrossTab.addSortType("Name","String");
	
	gmCrossTab.setSortColumn(intSortColumn);
	gmCrossTab.setSortOrder(intSortOrder);
			
	/*************************************************************************************************************************************/

%>
	
	<tr>
		<td align=center> <%=gmCrossTab.PrintCrossTabReport(hmReturn) %></td>
	</tr>	
	
	</TBODY>
  </TABLE>	
  <!-- Included for sorting -->
  <input type="hidden" name="hSortColumn" value="<%=intSortColumn%>">
<input type="hidden" name="hSortOrder" value="<%=intSortOrder%>">
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
