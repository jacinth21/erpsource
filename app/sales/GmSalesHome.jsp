 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmSalesHome.jsp
 * Desc		 		: This screen is used for the DashBoard Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@page import="com.globus.valueobject.common.GmDataStoreVO"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib uri="com.corda.taglib" prefix="ctl" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="java.sql.Date" %>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%>						
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ taglib prefix="fmtSalesHome" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!--sales\GmSalesHome.jsp  -->
<fmtSalesHome:setLocale value="<%=strLocale%>"/>
<fmtSalesHome:setBundle basename="properties.labels.sales.GmSalesHome"/>
<%
String strSalesDashBoardJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_DASHBOARD");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strTimeZone = strGCompTimeZone;
	String strApplCurrFmt = strGCompDateFmt;
	String strWikiTitle = GmCommonClass.getWikiTitle("DAILY_SALES");


	String strDeptId = (String)session.getAttribute("strSessDeptId")==null?"":(String)session.getAttribute("strSessDeptId");
	String strAccessLvl = (String)session.getAttribute("strSessAccLvl")==null?"":(String)session.getAttribute("strSessAccLvl");
	
	String strUserId = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
	String strShUserName = GmCommonClass.parseNull((String)session.getAttribute("strSessShName"));
	String strUserName = strUserId + " - " + strShUserName;
	
	GmResourceBundleBean gmResourceBundleBeanlbl = 
	    GmCommonClass.getResourceBundleBean("properties.labels.sales.GmSalesHome", strSessCompanyLocale);
	ArrayList alRegion=(ArrayList)request.getAttribute("alRegion");
	
	//System.out.println("strDeptId : " + strDeptId + " strAccessLvl : " + strAccessLvl);
	
	int intAccessLvl = Integer.parseInt(strAccessLvl);
	boolean bolAccess = true;
	/*	
	if ((strDeptId.equals("S") && intAccessLvl < 5))
	{
		bolAccess = false;
	}
	*/
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	//out.print(hmReturn);
	
	HashMap hmSales = new HashMap();

	ArrayList alTodaySales = new ArrayList();
	ArrayList alMonthSales = new ArrayList();
	ArrayList alDate = new ArrayList();
	ArrayList alColorsList = new ArrayList();

	HashMap hmTempLoop = new HashMap();
	HashMap hmLoop = new HashMap();
	String strId = "";
	String strName = "";
	String strCount = "";
	String strSum = "";
	String strOthersCode = "";
	String strLegendsCnt = "";

	String strDistName = "";
	String strDaySales = "";
	String strMonthSales = "";	
	int intWorkingDays = 0;
	
    if (hmReturn != null)
    {								
		hmSales = (HashMap)hmReturn.get("SALES");
		alTodaySales = (ArrayList)hmReturn.get("TODAY");
		alMonthSales = (ArrayList)hmReturn.get("MONTH");
		intWorkingDays = (Integer) hmReturn.get("WORKING_DAYS");
		
		alColorsList = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALCOLORSLIST"));
		strOthersCode = GmCommonClass.parseNull((String)hmReturn.get("STROTHERSCOLORCODE"));
		strLegendsCnt = GmCommonClass.parseNull((String)hmReturn.get("STRLEGENDSCNT"));
		
		if (hmSales != null)
		{
			strDaySales = (String)hmSales.get("DAYSALES");
			strMonthSales = (String)hmSales.get("MONTHSALES");
		}
	}
	
	String strShade = "";
	String strLine = "";
	String strRAId = "";
	String strReturnType = "";
	String strReturnReason = "";
	String strExpectedDate = "";
	String strPerson = "";
	String strComments = "";
	String strDateInitiated = "";
	String strAccName = "";
	
	int intLength = 0;
	String strNextId = "";
	String strDistId = "";
	String strPrice = "";
	double dbSales = 0.0;
	double dbAcctTotal = 0.0;
	double dbTotal = 0.0;
	int intCount = 0;
	boolean blFlag = false;
	String strAcctTotal = "";
	ArrayList alDist = new ArrayList();
	ArrayList alSales = new ArrayList();
	String strTotal = "";
	String strGraphTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_TODAY"));
	
	String strDate = "";
	java.sql.Date dtDate=null;
	String strDay = "";
	String strSales = "";
	String strAvgDaily = "";
	double dbAvgDaily = 0.0;
	double dbTempTotal = 0.0;
	String strDayOfWeek = "";

	StringBuffer sbPcScript = new StringBuffer();
	String strCurrSign = GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb"));
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	String strTodaysDate =  GmCalenderOperations.getCurrentDate(strApplCurrFmt);
	Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(strGCompTimeZone));
	String strMontoDate = (new java.text.SimpleDateFormat("MM/yyyy")).format(cal.getTime());

	String strDoughnutData =GmCommonClass.parseNull((String) request.getAttribute("DOUGHNUTJSON"));
	String strSalesBarData = GmCommonClass.parseNull((String) request.getAttribute("BARJSON"));  
	String strToAchieveQuota = GmCommonClass.parseNull((String) request.getAttribute("TOACHIEVEQUOTA"));  
	String strFCExportServer = GmCommonClass.parseNull((String) GmCommonClass.getString("FUSIONCHART_EXPORT_SERVER"));   
	String strProjSales = GmCommonClass.parseNull((String) request.getAttribute("PROJECTEDSALES"));  
	String strQuotaTypePer = GmCommonClass.parseNull((String) request.getAttribute("QUOTATYPEPER"));  
	String strPrjQuotaTypePer = GmCommonClass.parseNull((String) request.getAttribute("PRJQUOTAPERTYPE"));  
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Sales Dashboard </TITLE>
<script type="text/javascript" src="<%=strExtWebPath%>/fusionchart/fusioncharts.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script type="text/javascript"	src="<%=strSalesDashBoardJsPath%>/GmDailySalesReport.js"></script>
<script>


var doughnutDataArray = eval(<%=strDoughnutData%>);
var doughnutTotalSales = doughnutDataArray.totalamt;
doughnutDataArray = eval(doughnutDataArray.data);
var salesBarDataArray = eval(<%=strSalesBarData%>);
var currSymbol = '<%=strCurrSign%>';
var todayDate =  '<%=strTodaysDate%>';
var monToDate =  '<%=strMontoDate%>';
var dailyQuota =  '<%=strToAchieveQuota%>';
var projSales =  '<%=strProjSales%>';
var quotaTypePer =  '<%=strQuotaTypePer%>';
var prjQuotaTypePer =  '<%=strPrjQuotaTypePer%>';
var total ='<%=strDaySales%>';
var fcExportServer = '<%=strFCExportServer%>';
var lblTodaySales = "<fmtSalesHome:message key='LBL_TODAY'/>";
var caption = lblTodaySales+" ("+todayDate+")";
var width ='478';
var highestVal = parseInt(salesBarDataArray.yaxismaxvalue);
salesBarDataArray = salesBarDataArray.data;


function fnCallDailyReport(val)
{
	document.frmMain.hFromDate.value = val;
	document.frmMain.hType.value = "DAILY";
	document.frmMain.action = "/GmDetailedSalesServlet";
    if (document.frmMain.Cbo_RegnFilter)
     {
		document.frmMain.hUSFilter.value =  document.frmMain.Cbo_RegnFilter.value; ;
	}
	document.frmMain.submit();
}

function Toggle(val)
{
	
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className="";
		tabobj.style.background="#ffffff";
	}
}

function fnCallReload(val)
{
	 if (val == "0" )
	 {
		  val = "";
	 }
	document.frmMain.hAction.value = "Load";
	document.frmMain.hRegnFilter.value = val;
	
	document.frmMain.action = "/GmSaleDashBoardServlet";
	document.frmMain.submit();
}


function formatNumber(num,decimalplace) {
	decimalplace = (decimalplace=='')?0:decimalplace;
	return  parseFloat(num).toFixed(decimalplace).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


function formatCurrency(num) {
	num = num.toString();
	if(isNaN(num))
		num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
		cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3));
	if(sign){
		return currSymbol+'' + num+ '.' + cents;
	}else{
		return currSymbol+'(' + num+ '.' + cents+')'; //(((sign)?'':'-') +
	}
}

function fnBarChart(){

	var avgSales = 0;
	if(document.frmMain.hAvgSales){
		avgSales= document.frmMain.hAvgSales.value;
	}
	dailyQuota = parseFloat(dailyQuota);
	if(highestVal < dailyQuota ){
		highestVal = dailyQuota;
	}
	highestVal += (highestVal/20);
	
	var diffPerDay = avgSales-dailyQuota;
	diffPerDayColor= '#000000';
	diffPerDayColor = (diffPerDay <0) ?'#FF0000':diffPerDayColor;

	//commenting for BUG-5435
	//Previously we are taking EMTD & this value is divided by 1000 in GmSalesDashBoardBean.getQuotaHmapDetails,
	//Now we are taking the EMTDACTUAL. so we dont need this multiple by 1000	
	//projSales = projSales * 1000;
	projSalesColor= '#000000';
	projSalesColor = (projSales <0) ?'#FF0000':projSalesColor;
	
	//When the data is empty dont render the chart.
	if(salesBarDataArray != ''){
	//Chart type declaration
	var salesBarObj={
			type: 'column2d',
	        renderAt: 'divBarContainer',
	        width: '478',
	        height: '449',
	        dataFormat: 'json'
	};
	var salesBarDataSrc={};
	
	//Chart parameters declaration
	salesBarDataSrc.chart={ 
			"caption": message_sales[266]+"("+monToDate+")",
			 "captionFontColor":"#000000",
             "captionFont":"verdana, arial, sans-serif",
            //"subCaption": "Harry's SuperMart",
            //"xAxisName": "Month",
            //"yAxisName": "Revenues (In USD)",
             "numberPrefix": currSymbol,
             "paletteColors": "#45abf5",
             "bgColor": "#eeeeee",
             //"canvasbgColor": "#eeeeee",
             "borderAlpha": "20", 
             "valueFontColor": "#000000",
             "labelfontcolor":"#000000", 
             "outCnvBaseFontColor": "#000000",
             "canvasBorderAlpha": "0",
             "usePlotGradientColor": "0",
             "plotBorderAlpha": "10",
             "placevaluesInside": "0",
             "rotatevalues": "1",
             "showValues":"0",
             
             "formatNumberScale": "1",
             //"decimals": "2",
             //"forceDecimals": "1",
             
              "chartBottomMargin":"60",
              
             "toolTipColor":"#ffffff",
             "toolTipBorderThickness":"0",
             "toolTipBgColor":"#000000",
             "toolTipBgAlpha":"80",
             "toolTipBorderRadius":"2",
             "toolTipPadding":"5", 
             "yAxisMaxValue":highestVal,//adjust y axis max val range for highest trend line
             "showTrendlineLabels":"0",
             "showBorder": "0",
             "labelDisplay":"auto",
             "adjustDiv": "0",  //adjust y axis devision range for highest trend line values.
			 //"numDivLines": "5",             
             "showXAxisLine": "1",
             "xAxisLineColor": "#999999",
             "divlineColor": "#999999",               
             "divLineIsDashed": "0",
             "showAlternateHGridColor": "0",
             "subcaptionFontBold": "0",
             "subcaptionFontSize": "14",
     	     "showPercentValues":"1",
     	    "showPercentInToolTip":"1",
     		 "exportenabled" : "1",
     		 "exportatclient" : "0",
     		 "showHoverEffect": "1",
     		 "exporthandler" : fcExportServer,
     		 "html5exporthandler" : fcExportServer
    };
	salesBarDataSrc.annotations={
        
        "groups": [{

        "items": [{
                "id": "actlabelBackground",
                "type": "line",
                "thickness":"3",
                "fillColor": "#006633",
                "x": "75",
                "y": "400",
                "tox": "85"
         },{
                "id": "jennifer-user-icon",
                "type": "text",
                "text": "ADS:" ,
                "color":"#000000",
                "align": "left",
                "x": "90",
                "y": "400",
                "bold":"0"
         },{
                "id": "jennifer-user-icon",
                "type": "text",
                "align": "left",
                "text": formatCurrency(avgSales),
                "color":"#000000",
                "x": "120",
                "y": "400",
                "bold":"0"
         },{
            	"id": "actlabelBackground",
                "type": "line",
                "thickness": "3",
                "fillColor": "#CC3333",
                "align": "left",
                "x": "285",
                "y": "400",
                "tox": "295"
         },{
            	"id": "Quota-user-icon",
                "type": "text",
                "text": "Daily Quota:" , 
                "align": "left",
                "color":"#000000",
                "x": "300",
                "y": "400",
                "bold":"0"
	     },{
				"id": "Quota-user-icon",
				"type": "text",
				"text": formatCurrency(dailyQuota),
				"color":projSalesColor,
                "align": "left",
				"x": "366",
				"y": "400",
				"bold":"0"
 		 },{
                "id": "jennifer-user-icon",
                "type": "text",
                "text": "Difference Per Day: " ,
                "color":"#000000",
                "align": "left",
                "x": "74",
                "y": "420",
                "bold":"0"
         },{
	            "id": "jennifer-user-icon",
	            "type": "text",
	            "text": formatCurrency( diffPerDay ),
	            "color":diffPerDayColor,
                "align": "left",
	            "x": "175",
	            "y": "420",
	            "bold":"0"
    	 },{ 
                "id": "jennifer-user-icon",
                "type": "text",
                "text": "Projected ("+currSymbol+"): " ,
                "color":"#000000",
                "align": "left",
                "x": "284",
                "y": "420",
                "bold":"0"
   		 },{
				"id": "jennifer-user-icon",
				"type": "text",
				"text": formatCurrency(projSales),
				"color":projSalesColor,
                "align": "left",
				"x": "357",
				"y": "420",
				"bold":"0"
 		 },{
               "id": "jennifer-user-icon",
               "type": "text",
               "text": " (%) To Quota: ",
               "color":"#000000",
                "align": "left",
               "x": "74",
               "y": "439",
               "bold":"0"
    	},{
	            "id": "jennifer-user-icon",
	            "type": "text",
	            "text": formatNumber(quotaTypePer,2 )+" %",
	            "color":"#000000",
                "align": "left",
	            "x": "150",
	            "y": "439",
	            "bold":"0"
 		},{
            	"id": "pquota-user-icon",
           		"type": "text",
            	"text": " Projected (%) To Quota: ",
	            "color":"#000000",
	            "align": "left",
	            "x": "284",
	            "y": "439",
	            "bold":"0"
 		},{
	            "id": "pquotaval-user-icon",
	            "type": "text",
	            "text": formatNumber(prjQuotaTypePer,2 )+" %",
	            "color":"#000000",
             	"align": "left",
	            "x": "408",
	            "y": "439",
	            "bold":"0"
		}]
	    }]
	};
	//Assigning the data array values
	salesBarDataSrc.data=salesBarDataArray;
	
	//Assigning the trend line values
	salesBarDataSrc.trendlines =  [
							        {
							        	"line": [
							                {
							                    "startvalue": avgSales,
							                    "color": "#006633",//"FBB917",
							                    "tooltext": "ADS \n" + formatCurrency(avgSales),
							                    "thickness": "2",
							                    "showontop" : "1",
							                    "valueonright" : "0"
							                },{
							                    "startvalue": dailyQuota,
							                    "color": "#CC3333",
							                    "tooltext": "Daily Quota \n"  + formatCurrency( dailyQuota),
							                    "thickness": "2",
							                    "showontop" : "1",
							                    "valueonright" : "0"
							                }
										]
							        }
							      ];	
	salesBarObj.dataSource=salesBarDataSrc;	
		
	//Fusion chart rendering.
    var salesBarChart = new FusionCharts(salesBarObj);
    salesBarChart.render();   
    }
}

function fnLoad(){
	fnDoughnut();
	fnBarChart();
}

</script>
<script> downloadPiwikJS();</script>
<script> fnTracker(message_sales[268], '<%=strUserName%>');</script>
<style type="text/css">
.fa-rectangle {
  font-size: inherit;
  width: 7px;
}
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad()">
<FORM name="frmMain" method="POST" action="<%=strServletPath%>/GmSaleDashBoardServlet">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hPgToLoad" value="">
<input type="hidden" name="hSubMenu" value="">
<input type="hidden" name="hType" value="">
<input type="hidden" name="hFrom" value="Dashboard">
<input type="hidden" name="hFromDate" value="">
<input type="hidden" name="hAction" value="">

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" width="960" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="5" bgcolor="black"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
			<td colspan="5" bgcolor="black"><img src="<%=strImagePath%>/spacer.gif" height="1"></td>
			<td rowspan="5" bgcolor="black"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
		</tr>
		<tr>
			<td colspan="5">
				<table border="0"  width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" class="RightDashBoardHeader" align="left" colspan="5"><fmtSalesHome:message key="LBL_SALES_DASH"/></td>
						<td   height="25" class="RightDashBoardHeader">
						<fmtSalesHome:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
<%
			if (bolAccess){
%>		
		<tr>
			<td colspan="5" height="20">
			<jsp:include page="/sales/GmSalesFilters.jsp" >
				<jsp:param name="HIDE" value="SYSTEM" />
				<jsp:param name="FRMNAME" value="frmMain" />
				<jsp:param name="HACTION" value="Reload" />
			</jsp:include>
			</td>
		</tr>		
<%
			}
%>
		<tr>
			<td width="480" valign="top" align="center">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" class="RightDashBoardHeader" align="center" colspan="3">
							<fmtSalesHome:message key="LBL_TODAY"/> -&nbsp;&nbsp;&nbsp;<gmjsp:currency type="CurrTextSign"  textValue="<%=strDaySales%>" td="false"/></td> 
						
					</tr>
					<tr><td class="Line" colspan="3"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="20" width="200"><fmtSalesHome:message key="LBL_FEILD_SALES"/></td>
						<td width="450"><fmtSalesHome:message key="LBL_ACC_NAME"/></td>
						<td width="150">&nbsp;&nbsp;&nbsp;<fmtSalesHome:message key="LBL_AMOUNT"/></td>
					</tr>					
				</table>
				<div style="display:visible; height: 201px; overflow: auto; overflow-x:hidden; ">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
<%
						intLength = alTodaySales.size();
						int colorCnt =0;
						if (intLength > 0)
						{
							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alTodaySales.get(i);
								if (i<intLength-1)
								{
									hmTempLoop = (HashMap)alTodaySales.get(i+1);
									strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("DID"));
								}

								strDistId = GmCommonClass.parseNull((String)hmLoop.get("DID"));
								strDistName = GmCommonClass.parseNull((String)hmLoop.get("DNAME"));
								strId	= GmCommonClass.parseNull((String)hmLoop.get("ID"));
								strName = GmCommonClass.parseNull((String)hmLoop.get("NAME"));
								strPrice = GmCommonClass.parseZero((String)hmLoop.get("SALES"));

								dbSales = Double.parseDouble(strPrice);
								dbAcctTotal = dbAcctTotal + dbSales;
								dbTotal = dbTotal + dbSales;
								if (strDistId.equals(strNextId))
								{
									intCount++;
									strLine = "<TR><TD colspan=3 height=1 class=Line></TD></TR>";
								}
								else
								{
									strLine = "<TR><TD colspan=3 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
										strDistName = "";
										strLine = "";
									}
									else
									{
										blFlag = true;
									}
									intCount = 0;
									
								}
								strAcctTotal = ""+dbAcctTotal;

								if (intCount > 1)
								{
									strDistName = "";
									strLine = "";
								}
							
								strShade = (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows

								out.print(strLine);
								strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
								if (intCount == 1 || blFlag)
								{
										alDist.add(strDistName);
										String strLegendColor = "";
										if(colorCnt < Integer.parseInt(strLegendsCnt) ){
										 	strLegendColor = GmCommonClass.parseNull((String)alColorsList.get(colorCnt++));
										}else{
										  	strLegendColor = strOthersCode;
										}											
%>
					<tr id="tr<%=strDistId%>">
						<td colspan="2" height="19" class="RightText">&nbsp;<span class="fa-rectangle" style="background-color:<%=strLegendColor%>; " ></span>
						<a class="RightText" title="Click to Expand" href="javascript:Toggle('<%=strDistId%>')"><%=strDistName%></a></td>
						<td class="RightText" id="td<%=strDistId%>" align="right"></td>
					</tr>
					<tr>
						<td colspan="5"><div style="display:none" id="div<%=strDistId%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tab<%=strDistId%>">
<%
								blFlag = false;
								}
%>
					<tr <%=strShade%>>
						<td width="200" class="RightText" height="18">&nbsp;</td>
						<td width="450" class="RightText">&nbsp;<%=strName%></td>
						<gmjsp:currency type="CurrTextSign"  textValue="<%=strPrice%>"/>
					</tr>
					<tr><td colspan="3" bgcolor="#cccccc"></td></tr>
<%					
					if ( intCount == 0 || i == intLength-1)
					{
						alSales.add(strAcctTotal);
%>
							</table></div>
						</td>
					</tr>
					<script>
						document.all.td<%=strDistId%>.innerHTML = "<gmjsp:currency type="CurrTextSign"  textValue="<%=strAcctTotal%>" td="false"/>&nbsp;";
					</script>
<%
						dbAcctTotal = 0.0;					
					}
				strTotal = ""+dbTotal;
				}// end of FOR
%>
					<tr><td colspan="3" class="Line"></td></tr>
					<tr>
						<td align="right"  class="RightTableCaption" height="20" colspan="2"><fmtSalesHome:message key="LBL_GRAND_TOTAL"/>&nbsp;</td>
						<gmjsp:currency type="CurrTextSign"  textValue="<%=strTotal%>"/>
					</tr>
					</tr>
				</table>
				</div>
				<table border="0" width="98%" cellspacing="0" cellpadding="0">
					<tr><td class="Line"></td></tr>	
					<tr>
						<td  align="center">
							<div id="divDonutContainer"></div>
						</td>
					</tr>
<%
						} // End of IF
						else{
%>
					<tr><td class="Line"></td></tr>					
					<tr><td class="RightTextRed" height="50" align=center><fmtSalesHome:message key="LBL_NO_SALES"/> </td></tr>
<%					
						}
%>
					</tr>
				</table>				
				
			</td>
			<td bgcolor="black" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
			<td width="480" valign="top" align="center">

				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" colspan="3" class="RightDashBoardHeader" align="center">
							<fmtSalesHome:message key="LBL_MONTH_TO"/> - &nbsp;&nbsp;&nbsp;<gmjsp:currency type="CurrTextSign"  textValue="<%=strMonthSales%>" td="false"/></td>
						
					</tr>
					<tr><td class="Line" colspan="3"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="20" width="200">&nbsp;<fmtSalesHome:message key="LBL_DATE"/></td>
						<td width="450"><fmtSalesHome:message key="LBL_DAY"/></td>
						<td width="150"><fmtSalesHome:message key="LBL_AMOUNT"/></td>
					</tr>					
					<tr><td colspan="3" class="Line"></td></tr>
				</table>
				<div style="display:visible; height: 200px; overflow: auto; overflow-x:hidden;">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
<%
						intLength = alMonthSales.size();
						alSales = new ArrayList();
						hmLoop = new HashMap();
						dbSales = 0.0;
						dbTotal = 0.0;
						dbTempTotal = 0.0;
						strSales = "";
						int intDOF = 0;
						int intId = 0;
						if (intLength > 0)
						{
							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alMonthSales.get(i);
								if (i<intLength-1)
								{
									hmTempLoop = (HashMap)alMonthSales.get(i+1);
									strNextId = GmCommonClass.parseZero((String)hmTempLoop.get("DOF"));
									intId = Integer.parseInt(strNextId);
								}
								
								//dtDate=(java.sql.Date)hmLoop.get("SDATE");
							
								//strDate =  GmCalenderOperations.getCurrentDate(strApplCurrFmt);
								strDate = (String)hmLoop.get("SDATEFMT");
								strDay = GmCommonClass.parseZero((String)hmLoop.get("DAY"));
								strSales = GmCommonClass.parseZero((String)hmLoop.get("SALES"));
								strDayOfWeek = GmCommonClass.parseZero((String)hmLoop.get("DOF"));
								intDOF = Integer.parseInt(strDayOfWeek);
								dbSales = Double.parseDouble(strSales);
								dbTotal = dbTotal + dbSales;
								dbTempTotal = dbTempTotal + dbSales;
								alDate.add(strDate);
								alSales.add(strSales);
%>
					<tr class="RightText">
						<td HEIGHT="19" >&nbsp;<a href="javascript:fnCallDailyReport('<%=strDate%>')" class="RightText"><%=strDate%></a></td>
						<td ><%=strDay%></td>
						 <gmjsp:currency type="CurrTextSign"  textValue="<%=strSales%>"/>
					</tr>
					<tr><td colspan="3" bgcolor="#cccccc"></td></tr>
					
<%
								if (intDOF > intId)
								{
									strTotal = ""+dbTempTotal;
%>
					<tr bgcolor="#ecf6fe">
						<td class="RightTableCaption" align="right" height="18" colspan="2"><fmtSalesHome:message key="LBL_WEEK_TOT"/>&nbsp;</td>
						<gmjsp:currency type="CurrTextSign"  textValue="<%=strTotal%>"/>
					</tr>
					<tr><td colspan="3" class="Line"></td></tr>
<%								
									dbTempTotal = 0.0;
								}
							}
						strTotal = ""+dbTotal;
						dbAvgDaily = (intWorkingDays == 0)? 0:(dbTotal / intWorkingDays);
						strAvgDaily = ""+GmCommonClass.roundDigit(dbAvgDaily,2);
%>
					<tr class="ShadeRightTableCaptionBlue">
						<td align="right"  class="RightTableCaption" height="20" colspan="2"><fmtSalesHome:message key="LBL_GRAND_TOTAL"/>&nbsp;&nbsp;&nbsp;</td>
						<gmjsp:currency type="CurrTextSign"  textValue="<%=strTotal%>"/>
					</tr>
					<tr><td colspan="3" class="Line"></td></tr>
					<tr>
						<td align="right"  class="RightTableCaption" height="20" colspan="2"><fmtSalesHome:message key="LBL_AVG_DAILY"/>&nbsp;&nbsp;&nbsp;</td>
						<gmjsp:currency type="CurrTextSign"  textValue="<%=strAvgDaily%>"/>
						<input type="hidden" name="hAvgSales" value="<%=strAvgDaily%>">
					</tr>
					<tr><td colspan="3" class="Line"></td></tr>
					<tr>
						<td align="right"  class="RightTableCaption" height="20" colspan="2"><fmtSalesHome:message key="LBL_DAILY_QUOTA"/>&nbsp;&nbsp;&nbsp;</td>
						<gmjsp:currency type="CurrTextSign"  textValue="<%=strToAchieveQuota%>"/>
					</tr>
				</table>
				</DIV>
				<table border="0" width="98%" cellspacing="0" cellpadding="0">
					<tr><td class="Line"></td></tr>					
					<tr>
						<td>
							<div id="divBarContainer"></div>
						</td>
					</tr>				
<%
						}
						else
						{
%>
					<tr><td class="RightTextRed" height="50" align=center><fmtSalesHome:message key="LBL_NO_SALES"/> </td></tr>
<%
						}					
%>
				</table>
								
			</td>
		</tr>
		<tr>
			<td colspan="5" height="1" bgcolor="black"></td>
		</tr>
	</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
