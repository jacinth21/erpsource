<!-- sales\GmSalesEfficiencyScoreCardByLoanerReport.jsp -->
<%
	/**********************************************************************************
	  * File		 		: GmSalesEfficiencyScoreCardByLoanerReport.jsp
      * Desc		 		: This screen is used to Scorecard - Exp. Loaner Usage report
      * author			    : Raja
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java"%>
<%@page import="java.util.Date"%> 
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%> 
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.net.URLEncoder" %>
<%@ taglib prefix="fmtGmSalesEfficiencyScoreCardByLoaner" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtGmSalesEfficiencyScoreCardByLoaner:setLocale value="<%=strLocale%>"/>
<fmtGmSalesEfficiencyScoreCardByLoaner:setBundle basename="properties.labels.sales.GmSalesEfficiencyScoreCardByLoanerReport"/> 

<%

String strsalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");

String strWikiTitle = GmCommonClass.parseNull((String)GmCommonClass.getWikiTitle("SCORE_CARD_BY_LOANER"));

%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Scorecard - Exp. Loaner usage report</TITLE>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<!-- Method allows user to add new row from clipboard. New row is being added to the last position in the grid. -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<!-- paste content of clipboard into block selection of grid -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script language="JavaScript" src="<%=strsalesJsPath%>/GmSalesEfficiencyScoreCardByLoanerReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/jquery-latest.js"></script>

<style type="text/css" media="all">
@import url("styles/screen.css"); 
</style>
<style> div.gridbox_dhx_skyblue table.hdr td { text-align: center; vertical-align: middle !important; } </style>
<style type="text/css">
         table.DtTable1550 {
             margin: 0 0 0 0;
             width: 1300px;
             border: 1px solid #676767;
             overflow:auto;
         }
     </style>
</HEAD>
<body leftmargin="15" topmargin="10" onLoad="fnOnPageLoad(); ">
<html:form action="/gmSalesEfficiencyScoreCardByLoaner.do?method=loadLoanerUsageScoreCardDtls"> 
<html:hidden property="systemId" />
<html:hidden property="distributorId" />
<html:hidden property="adId" />
 <table class="DtTable1500" border="0" cellspacing="0" cellpadding="0">
			<tr>
				 <td colspan="9" height="25" class="RightDashBoardHeader">&nbsp;<fmtGmSalesEfficiencyScoreCardByLoaner:message key="LBL_EXP_LOANER_RPT_HEADER"/> </td>
				<td height="25" align="right" class=RightDashBoardHeader><fmtGmSalesEfficiencyScoreCardByLoaner:message key="IMG_ALT_HELP" var="varHelp"/>				    
				 <img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
             </tr>
             
           <tr>
				 <td colspan="10">
					    <jsp:include page="/sales/GmSalesFilters.jsp" >
					    <jsp:param name="HIDE" value="SYSTEM" />
						<jsp:param name="FRMNAME" value="frmSalesEfficiencyScoreCardByLoaner" />
						<jsp:param name="URL" value="/gmSalesEfficiencyScoreCardByLoaner.do?method=loadLoanerUsageScoreCardDtls" />	
						
					</jsp:include>
				</td> 
			</tr>
		
		   <tr class="oddshade"> 
				<td colspan="10"><div id="dataGridDiv" height="400px"></div></td>
		  </tr>
		  <tr id="DivExportExcel" height="25">
			<td colspan="6" align="center">
			    <div class='exportlinks'><fmtGmSalesEfficiencyScoreCardByLoaner:message key="EXPORT_OPTS"/> :<img
					src='img/ico_file_excel.png' onclick="fnExportDtls();" />&nbsp;<a href="#"
					onclick="fnExportDtls();"><fmtGmSalesEfficiencyScoreCardByLoaner:message key="EXCEL"/> 
				</a></div>
			</td>
		 </tr>
		 <tr><td colspan="4" align="center"  class="RegularText" height="20"><div  id="DivNothingMessage"><fmtGmSalesEfficiencyScoreCardByLoaner:message key="NO_DATA_FOUND"/></div></td></tr>
       </table> 
 </html:form> 
  <%@ include file="/common/GmFooter.inc" %> 
</BODY>
</HTML>