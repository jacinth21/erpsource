<%
/**********************************************************************************
 * File		 		: GmADReplacement.jsp
 * Desc		 		: AD Replacement
 * Version	 		: 1.0
************************************************************************************/
%>
 <%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ taglib prefix="fmtSalesManage" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtSalesManage:setLocale value="<%=strLocale%>"/>
<fmtSalesManage:setBundle basename="properties.labels.sales.GmSalesManageAD"/>


<% 
String strSalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");	
String strWikiTitle = GmCommonClass.getWikiTitle("AD_REPLACEMENT");  
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: AD Replacement </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">


<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/jquery.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/json2.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/libCompiler/export/1345731336/dhtmlx.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strSalesJsPath%>/GmSalesmanage.js"></script>
<style type="text/css" media="all">
     @import url("styles/screen.css");
     div#winVP {
			position: relative;
			height: 350px;
			border: 1px solid #dfdfdf;
			margin: 10px;
		}
		div.objId {
			position: relative;
			width: 100%;
			height: 100%;
			font-family: Roboto, Arial, Helvetica;
			font-size: 14px;
			color: #404040;
			overflow: auto;
		}
	
</style>

</HEAD>

<BODY leftmargin="20" topmargin="10"  >
<html:form action="/gmSalesManage.do?"  >
<html:hidden property="strOpt" />
<html:hidden property="oldadid" />
<html:hidden property="hAction"/>
<html:hidden property="hRegionNm"/>
<html:hidden property="hZoneNm"/>
<html:hidden property="hUserNm"/>


<html:hidden property="checkedFL" name="frmSalesManageForm"/> 
 
<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr>
				<td height="25" class="RightDashBoardHeader" colspan="4"><fmtSalesManage:message key="LBL_AD_REPLACEMENT"/></td>
					<fmtSalesManage:message key="IMG_ALT_HELP" var="VarHelp"/>
				<td align="right" class=RightDashBoardHeader><img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${VarHelp}' width='16'
					height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
				</td>	
			</tr>
			<tr>
				<td class="Line" height="1" colspan="5"></td>
			</tr>
			<tr>
				<td align="right" class="RightTableCaption" HEIGHT="23"><fmtSalesManage:message key="LBL_ZONE"/>:</td>
				<td colspan="4" class="RightText">&nbsp;<gmjsp:dropdown controlName="zoneid" SFFormName="frmSalesManageForm" SFSeletedValue="zoneid"
								SFValue="alZone" codeId = "GPID"  codeName = "GRPNAME" onChange="fnChangeAcctType();" defaultValue= "[Select]" />	</td>
			</tr>
			<tr>
				<td colspan="5" class="LLine"></td>
			</tr>
			<tr class="Shade">
				<td align="right" class="RightTableCaption" HEIGHT="23"><fmtSalesManage:message key="LBL_REGION"/>:</td>
				<td class="RightText" colspan="4">&nbsp;<gmjsp:dropdown controlName="regionid" SFFormName="frmSalesManageForm" SFSeletedValue="regionid"
								SFValue="alRegion" codeId = "ADID"  codeName = "REGNAME"  defaultValue= "[Select]" />	</td>
			</tr>
			<tr>
				<td colspan="5" class="LLine"></td>
			</tr>
			<tr>
				<td align="right" class="RightTableCaption" HEIGHT="23"><fmtSalesManage:message key="LBL_USER"/>: </td>
				<td class="RightText" width="80px" style="padding-left: 5px;"><gmjsp:dropdown controlName="newadid" SFFormName="frmSalesManageForm" SFSeletedValue="newadid"
								SFValue="alNewAD" codeId = "USERID"  codeName = "USERNAME"  defaultValue= "[Select]"  onBlur="fnShowDropdown();"/></td>
				<td  align="right"><input type="checkbox" name="Create_OpenAD" value="" onClick="fnShowHideUser(this);"/></td>
				<td  align="left" HEIGHT="30" ><fmtSalesManage:message key="LBL_CREATE_OPENAD"/></td>
			</tr>
			<tr>
				<td colspan="5" class="LLine"></td>
			</tr><tr height="30">
				<fmtSalesManage:message key="BTN_SUBMIT" var="varSubmit"/>
				
				<td align="center" colspan="3">
				<gmjsp:button value="${varSubmit}" gmClass="button" name="btn_submit" style="width: 6em" buttonType="Save" onClick="fnReplaceAD();" />&nbsp;&nbsp;&nbsp; 
			
				</td>
			</tr>
		
    </table>		     	
</html:form>
<b><a style="color: red">*</a><fmtSalesManage:message key="LBL_HINT"/></b>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
