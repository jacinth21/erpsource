<%
/**********************************************************************************
 * File                      : GmTerrGrowthQuotaReport.jsp
 * Desc                      : This screen is used for the displaying Territory Growth and To Quota Info
 *                            : grouped by Region and Distributor
 * Version             : 1.0
 * author               : Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!--sales\GmTerrGrowthQuotaReport.jsp  -->
<!-- WEB-INF path corrected for JBOSS migration changes -->
					
<%@ taglib prefix="fmtTerrGrowthQuotaReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtTerrGrowthQuotaReport:setLocale value="<%=strLocale%>"/>
<fmtTerrGrowthQuotaReport:setBundle basename="properties.labels.sales.GmTerrGrowthQuotaReport"/>
<%
      response.setHeader("Cache-Control", "no-cache, post-check=0, pre-check=0"); //HTTP 1.1
      response.setHeader("Pragma", "no-cache");  //HTTP 1.0
      response.setDateHeader ("Expires", 0); //prevents caching at the proxy server 
%>
<%
      String strSalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");
      GmServlet gm = new GmServlet();
      gm.checkSession(response,session);

      String strCurrSign=GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb"));
    //Below code is to track down the visits on the report.
  	String strUserId = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
  	String strShUserName = GmCommonClass.parseNull((String)session.getAttribute("strSessShName"));
  	String strUserName = strUserId + " - " + strShUserName;
  	//Visit code ends
      
      HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
      ArrayList alReturn = (ArrayList)hmReturn.get("REPORT");
      ArrayList alMonthDropDown = new ArrayList();
      ArrayList alYearDropDown = new ArrayList();
      
      alMonthDropDown = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get ("ALMONTHDROPDOWN"));
      alYearDropDown = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get ("ALYEARDROPDOWN"));

      int intLength = alReturn.size();

      int intSize = 0;
      HashMap hmLoop = new HashMap();

      String strRegnId  = "";
      String strRegnNm  = "";
      String strNextId  = "";
      String strDistId  = "";
      String strNextDistId = "";
      String strDistNm  = "";
      String strTerrID  = "";
      String strTerrNm  = "";
      String strRepNm    = "";
      String strADID          = "";
      
      Double dbMonthSales = new Double(0);
      Double dbMonthQuota = new Double(0);
      Double dbMonthPercQuota = new Double(0);
      Double dbMonthPrevSales = new Double(0);
      
      Double dbQuartSales = new Double(0);
      Double dbQuartQuota = new Double(0);
      Double dbQuartPercQuota = new Double(0);
      Double dbQuartPrevSales = new Double(0);
      
      Double dbYearSales = new Double(0);
      Double dbYearQuota = new Double(0);
      Double dbYearPercQuota = new Double(0);
      Double dbYearPrevSales = new Double(0);
            
      String strOrdId= "";

      String strShade = "";
      int intCount = 0;
      String strLine = "";

      Double dbMonthGrowth = new Double(0);
      Double dbMonthGrowthPerc = new Double(0);
      Double dbQuartGrowth = new Double(0);
      Double dbQuartGrowthPerc = new Double(0);
      Double dbYearGrowth = new Double(0);
      Double dbYearGrowthPerc = new Double(0);
            
      boolean blFlag = false;
      double dbSales = 0.0;
      double dbAcctTotal = 0.0;
      double dbTotal = 0.0;
      double dbAvgDaily = 0.0;
      double dbTempTotal = 0.0;
      double dbDistTotal = 0.0;
      String strOrdDate = "";

      double dbLev1MonthSales = 0.0;
      double dbLev2MonthSales = 0.0;
      double dbLev3MonthSales = 0.0;
      double dbLev1MonthS = 0.0;
      double dbLev2MonthS = 0.0;
      double dbLev3MonthS = 0.0;
      double dbLev1MonthPrevS = 0.0;
      double dbLev2MonthPrevS = 0.0;      
      double dbLev3MonthPrevS = 0.0;      
      double dbLev1MonthPrevSales = 0.0;
      double dbLev2MonthPrevSales = 0.0;  
      double dbLev3MonthPrevSales = 0.0;  
            
      double dbLev1QuartSales = 0.0;
      double dbLev2QuartSales = 0.0;
      double dbLev3QuartSales = 0.0;
      double dbLev1QuartS = 0.0;
      double dbLev2QuartS = 0.0;
      double dbLev3QuartS = 0.0;
      double dbLev1QuartPrevS = 0.0;
      double dbLev2QuartPrevS = 0.0;      
      double dbLev3QuartPrevS = 0.0;      
      double dbLev1QuartPrevSales = 0.0;
      double dbLev2QuartPrevSales = 0.0;
      double dbLev3QuartPrevSales = 0.0;
            
      double dbLev1YearSales = 0.0;
      double dbLev2YearSales = 0.0;
      double dbLev3YearSales = 0.0;
      double dbLev1YearS = 0.0;
      double dbLev2YearS = 0.0;
      double dbLev3YearS = 0.0;
      double dbLev1YearPrevS = 0.0;
      double dbLev2YearPrevS = 0.0;
      double dbLev3YearPrevS = 0.0;
      double dbLev1YearPrevSales = 0.0;
      double dbLev2YearPrevSales = 0.0;
      double dbLev3YearPrevSales = 0.0;
            
      double dbLev1MonthQuota = 0.0;
      double dbLev2MonthQuota = 0.0;
      double dbLev3MonthQuota = 0.0;
      double dbLev1MonthQ     = 0.0;
      double dbLev2MonthQ     = 0.0;
      double dbLev3MonthQ     = 0.0;
            
      double dbLev1QuartQuota = 0.0;
      double dbLev2QuartQuota = 0.0;
      double dbLev3QuartQuota = 0.0;
      double dbLev1QuartQ     = 0.0;
      double dbLev2QuartQ     = 0.0;
      double dbLev3QuartQ     = 0.0;
      
      double dbLev1YearQuota = 0.0;
      double dbLev2YearQuota = 0.0;
      double dbLev3YearQuota = 0.0;
      double dbLev1YearQ            = 0.0;
      double dbLev2YearQ            = 0.0;
      double dbLev3YearQ            = 0.0;
      
      double  dbLev1MonthPQ   = 0.0;
      double  dbLev2MonthPQ   = 0.0;
      double  dbLev3MonthPQ   = 0.0;
      double  dbLev1QuartPQ   = 0.0;
      double  dbLev2QuartPQ   = 0.0;
      double  dbLev3QuartPQ   = 0.0;
      double  dbLev1YearPQ    = 0.0;
      double  dbLev2YearPQ    = 0.0;
      double  dbLev3YearPQ    = 0.0;
      
      double  dbLev1MonthGD   = 0.0;
      double  dbLev2MonthGD   = 0.0;
      double  dbLev3MonthGD   = 0.0;
      double  dbLev1QuartGD   = 0.0;
      double  dbLev2QuartGD   = 0.0;
      double  dbLev3QuartGD   = 0.0;
      double  dbLev1YearGD    = 0.0;
      double  dbLev2YearGD    = 0.0;
      double  dbLev3YearGD    = 0.0;
      
      double  dbLev1MonthGP   = 0.0;
      double  dbLev2MonthGP   = 0.0;
      double  dbLev3MonthGP   = 0.0;
      double  dbLev1QuartGP   = 0.0;
      double  dbLev2QuartGP   = 0.0;
      double  dbLev3QuartGP   = 0.0;
      double  dbLev1YearGP    = 0.0;
      double  dbLev2YearGP    = 0.0;
      double  dbLev3YearGP    = 0.0;
      
      
      double totalLev1MonthSales = 0.0;
      double totalLev1QuartSales = 0.0;
      double totalLev1YearSales = 0.0;
      double totalLev1MonthQuota = 0.0;
      double totalLev1QuartQuota = 0.0;
      double totalLev1YearQuota = 0.0;
      double totalLev1MonthPQ = 0.0;
      double totalLev1QuartPQ = 0.0;      
      double totalLev1YearPQ = 0.0;      
      double totalLev1MonthGD = 0.0;
      double totalLev1QuartGD = 0.0;  
      double totalLev1YearGD = 0.0;  
            
      double totalLev1MonthGP = 0.0;
      double totalLev1QuartGP = 0.0;
      double totalLev1YearGP = 0.0;
      double totalLev1MonthPrevSales = 0.0;
      double totalLev1QuartPrevSales = 0.0;
      double totalLev1YearPrevSales = 0.0;
       
      
      Double dbLev1MonthGrowthPerc = new Double(0);
      Double dbLev2MonthGrowthPerc = new Double(0);

      // Other required variable 
      String strOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt")) ;
      String strRegnFilter = GmCommonClass.parseNull((String)request.getAttribute("hRegnFilter"));
      String strToMonth = "";
      String strToYear ="";

      
      strToMonth = GmCommonClass.parseNull((String)request.getAttribute("Cbo_ToMonth"));
      strToYear = GmCommonClass.parseNull((String)request.getAttribute("Cbo_ToYear"));
      String strBtnClick="fnGo('"+strOpt+"');";
      
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Territory Growth & Quota Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strSalesJsPath%>/GmSalesFilter.js"></script>
<script> downloadPiwikJS();</script>
<script> fnTracker('Summary Growth Report (in 000s)', '<%=strUserName%>');</script>
<script>

function fnCallAccount(val,Hierarchy )
{
	url='<%=strServletPath%>/GmSalesGrowthServlet?';
      if (Hierarchy=='2') 
      {
            document.frmMain.hAreaDirector.value = val;
            url='<%=strServletPath%>/GmSalesGrowthServlet?hAreaDirector='+val;
      }
      if (Hierarchy=='1') 
      {
            document.frmMain.hDistributorID.value = val;
            url='<%=strServletPath%>/GmSalesGrowthServlet?hDistributorID='+val;
      }
      if (Hierarchy=='0') 
      {
            document.frmMain.hTerritory.value = val;
            url='<%=strServletPath%>/GmSalesGrowthServlet?hTerritory='+val;
      }
      
      document.frmMain.hAction.value = "Reload";
      document.frmMain.strSessOpt.value = "ACCT";
    
	url=url+'&strSessOpt=ACCT&hAction='+document.frmMain.hAction.value;
		 
	fnReloadSales('',url);
   
}


function fnCallPart(val, Hierarchy)
{
	url='<%=strServletPath%>/GmSalesGrowthServlet?';
      if (Hierarchy=='2') 
      {
            document.frmMain.hAreaDirector.value = val;
            url='<%=strServletPath%>/GmSalesGrowthServlet?hAreaDirector='+val;
      }
      if (Hierarchy=='1') 
      {
            document.frmMain.hDistributorID.value = val;
            url='<%=strServletPath%>/GmSalesGrowthServlet?hDistributorID='+val;
      }
      if (Hierarchy=='0') 
      {
            document.frmMain.hTerritory.value = val;
            url='<%=strServletPath%>/GmSalesGrowthServlet?hTerritory='+val;
      }

      document.frmMain.hAction.value = "Reload";
      document.frmMain.strSessOpt.value = "PART";
      
	url=url+'&strSessOpt=PART&hAction='+document.frmMain.hAction.value;
		 
	fnReloadSales('',url);
}

function fnCallSetNumber(val,Hierarchy )
{ 
   	url='<%=strServletPath%>/GmSalesGrowthServlet?';
      if (Hierarchy=='2') 
      {
            document.frmMain.hAreaDirector.value = val;
            url='<%=strServletPath%>/GmSalesGrowthServlet?hAreaDirector='+val;
      }
      if (Hierarchy=='1') 
      {
            document.frmMain.hDistributorID.value = val;
            url='<%=strServletPath%>/GmSalesGrowthServlet?hDistributorID='+val;
      }
      if (Hierarchy=='0') 
      {
            document.frmMain.hTerritory.value = val;
            url='<%=strServletPath%>/GmSalesGrowthServlet?hTerritory='+val;
      }

      document.frmMain.hAction.value = "Reload";
      document.frmMain.strSessOpt.value = "PGRP";
     
	url=url+'&strSessOpt=PGRP&hAction='+document.frmMain.hAction.value;
		 
	fnReloadSales('',url);
}

function fnGo(haction)
{
      //fnSetSelections();
      fnReloadSales(haction,null);
      //document.frmMain.hAction.value = 'Reload';
      //document.frmMain.submit();
}

function Toggle(val)
{
      var obj = eval("document.all.tab"+val);

      if (obj.style.display == 'none')
      {
            obj.style.display = '';
      }
      else
      {
            obj.style.display = 'none';
      }
}

function fnLoad()
{     
      document.frmMain.Cbo_ToMonth.value = '<%=request.getAttribute("Cbo_ToMonth")%>';
      document.frmMain.Cbo_ToYear.value = '<%=request.getAttribute("Cbo_ToYear")%>';
      
      document.frmMain.hAreaDirector.value      = "null";
      document.frmMain.hDistributorID.value     = "null";
      document.frmMain.hTerritory.value         = "null";
    //  document.frmMain.Cbo_RegnFilter.value = '<%=strRegnFilter%>';
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad()">
<FORM name="frmMain" method="POST" action="<%=strServletPath%>/GmQuotaTrackingServlet">
      <input type="hidden" name="hAction" value="Load">
      <input type="hidden" name="hOpt" value="QuotaTrack">
      <input type="hidden" name="strSessOpt" value="<%=strOpt%>">
      
      <!-- The following hidden fields are for Sales Drilldown (Additional Drill down)-->
      <input type="hidden" name="hDistributorID" value="null">
      <input type="hidden" name="hAccountID" value="null">
      <input type="hidden" name="hSetNumber" value="<%=request.getParameter("hSetNumber")%>">
      <input type="hidden" name="hRepID" value="null">
      <input type="hidden" name="hTerritory" value="null">
      <input type="hidden" name="hPartNumber" value="null">
      <input type="hidden" name="hAreaDirector" value="null">
      
            
      <table border="0" width="1527" cellspacing="0" cellpadding="0">
            <tr>
                  <td colspan="3" ></td>
            </tr>
            <tr>
                  <td  width="1"></td>
                  <td width="1525" valign="top">
                        <table border="0" width="100%" cellspacing="0" cellpadding="0">
                              <tr>
                                    <td height="25" class="RightDashBoardHeader" colspan="23"><fmtTerrGrowthQuotaReport:message key="LBL_SUMMARY_GROWTH"/></td>
                              </tr>
                              <tr><td class="Line" colspan="23"></td></tr>
                              <tr>
									<td colspan="10">
									<jsp:include page="/sales/GmSalesFilters.jsp" >
										<jsp:param name="FRMNAME" value="frmMain" />
										<jsp:param name="HACTION" value="<%=strOpt%>"/>
										<jsp:param name="HIDE" value="SYSTEM" />				
									</jsp:include>
									</td>
							 </tr>		
		
                              <tr>
                                    <td colspan="23">
                                          <table cellSpacing="0" cellPadding="0"  border="0">
                                                <tr height=30>
                                                      <TD class=RightText align="right" width="100"><fmtTerrGrowthQuotaReport:message key="LBL_PERIOD"/></TD>
                                                      <TD class=RightText width=105>&nbsp;&nbsp;<gmjsp:dropdown controlName="Cbo_ToMonth"  seletedValue="<%=strToMonth %>" value="<%=alMonthDropDown%>" 
															codeId = "CODENMALT"  codeName = "CODENM"  tabIndex="1"  />
													  </TD>
                                                      <TD class=RightText width=75>
                                                      <gmjsp:dropdown controlName="Cbo_ToYear"  seletedValue="<%=strToYear%>" value="<%=alYearDropDown%>" 
														codeId = "CODENM"  codeName = "CODENM"  tabIndex="2" />
														</TD>
                                                   <!--    <TD class=RightText align="right" width="60">Regions:</TD>
                                                      <td>&nbsp;&nbsp;
                                                            <select name="Cbo_RegnFilter" class="RightText">
                                                                  <option value="">All
                                                                  <option value="East">East
                                                                  <option value="West">West
                                                            </select>
                                                      </td>	--> 
                                                      <TD>&nbsp;&nbsp; <fmtTerrGrowthQuotaReport:message key="BTN_GO" var="varGo"/><gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" gmClass="button" onClick="<%=strBtnClick%>" tabindex="5" buttonType="Load" /></TD>
                                                </TR>
                                          </TABLE>
                                    </td>
                              </tr>
                              <%/* Will be used later 
                              	/* <tr>
                                    <td colspan="23">
                                                <jsp:include page="/sales/GmSalesFilters.jsp" >
                                                </jsp:include>
                                    </td>
                              </tr>  */%>
                              <tr><td class="Line" colspan="23"></td></tr>
                              <tr class="ShadeRightTableCaption">
                                    <td HEIGHT="24" width="160" rowspan="3" colspan="3"><fmtTerrGrowthQuotaReport:message key="LBL_REGION_FEILD"/><BR>&nbsp;&nbsp;&nbsp;&nbsp;<fmtTerrGrowthQuotaReport:message key="LBL_TERRI"/></td>
                                    <td width="150" align="center" rowspan="3"><fmtTerrGrowthQuotaReport:message key="LBL_SPINE_SPCL"/></td>
                                    <td width="45" rowspan="3">&nbsp;</td>
                                    <td align="center" width="390" colspan="6" class="ShadeDarkGreenTD"><fmtTerrGrowthQuotaReport:message key="LBL_MONTH"/></td>
                                    <td align="center" width="390" colspan="6" class="ShadeDarkBlueTD"><fmtTerrGrowthQuotaReport:message key="LBL_QUARTER"/></td>
                                    <td align="center" width="390" colspan="6" class="ShadeDarkOrangeTD"><fmtTerrGrowthQuotaReport:message key="LBL_YEAR"/></td>
                              </tr>                         
                              <tr><td class="Line" colspan="23"></td></tr>
                              <tr class="ShadeRightTableCaption" height="18">
                                    <td width="65" align="center" class="ShadeDarkGreenTD"><fmtTerrGrowthQuotaReport:message key="LBL_CMTD"/></td>
                                    <td width="65" align="center" class="ShadeDarkGreenTD"><fmtTerrGrowthQuotaReport:message key="LBL_PMTD"/></td>
                                    <td width="65" align="center" class="ShadeDarkGreenTD"><fmtTerrGrowthQuotaReport:message key="LBL_QUOTAAMT"/></td>
                                    <td width="65" align="center" class="ShadeDarkGreenTD"><fmtTerrGrowthQuotaReport:message key="LBL_QUOTA"/></td>
                                    <td width="65" align="center" class="ShadeDarkGreenTD"><fmtTerrGrowthQuotaReport:message key="LBL_GROWTHAMT"/></td>
                                    <td width="65" align="center" class="ShadeDarkGreenTD"><fmtTerrGrowthQuotaReport:message key="LBL_GROWTH"/></td>
                                    <td width="65" align="center" class="ShadeDarkBlueTD"><fmtTerrGrowthQuotaReport:message key="LBL_CQTD"/></td>
                                    <td width="65" align="center" class="ShadeDarkBlueTD"><fmtTerrGrowthQuotaReport:message key="LBL_PQTD"/></td>
                                    <td width="65" align="center" class="ShadeDarkBlueTD"><fmtTerrGrowthQuotaReport:message key="LBL_QUOTAAMT"/></td>
                                    <td width="65" align="center" class="ShadeDarkBlueTD"><fmtTerrGrowthQuotaReport:message key="LBL_QUOTA"/></td>
                                    <td width="65" align="center" class="ShadeDarkBlueTD"><fmtTerrGrowthQuotaReport:message key="LBL_GROWTHAMT"/></td>
                                    <td width="65" align="center" class="ShadeDarkBlueTD"><fmtTerrGrowthQuotaReport:message key="LBL_GROWTH"/></td>
                                    <td width="65" align="center" class="ShadeDarkOrangeTD"><fmtTerrGrowthQuotaReport:message key="LBL_CYTD"/></td>
                                    <td width="65" align="center" class="ShadeDarkOrangeTD"><fmtTerrGrowthQuotaReport:message key="LBL_PYTD"/></td>
                                    <td width="65" align="center" class="ShadeDarkOrangeTD"><fmtTerrGrowthQuotaReport:message key="LBL_QUOTAAMT"/></td>
                                    <td width="65" align="center" class="ShadeDarkOrangeTD"><fmtTerrGrowthQuotaReport:message key="LBL_QUOTA"/></td>
                                    <td width="65" align="center" class="ShadeDarkOrangeTD"><fmtTerrGrowthQuotaReport:message key="LBL_GROWTHAMT"/></td>
                                    <td width="65" align="center" class="ShadeDarkOrangeTD"><fmtTerrGrowthQuotaReport:message key="LBL_GROWTH"/></td>
                              </tr>
<%
          int intDist = 0;
          int intRegn = 0;
          int intAccCount = 0;
          boolean bolAcFl = false;
          StringBuffer strLevel1 = new StringBuffer();
          StringBuffer strLevel2 = new StringBuffer();
          StringBuffer strLevel3 = new StringBuffer();
		  
		  //**************************************************
		  // if value fetched then system moved to below code 
		  //**************************************************
		  	
		  if (intLength > 0)
          {
                                          HashMap hmTempLoop = new HashMap();

                                          for (int i = 0;i < intLength ;i++ )
                                          {
                                                hmLoop = (HashMap)alReturn.get(i);
                                                if (i<intLength-1)
                                                {
                                                      hmTempLoop = (HashMap)alReturn.get(i+1);
                                                      strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("RGID"));
                                                      strNextDistId = GmCommonClass.parseNull((String)hmTempLoop.get("DID"));
                                                }

                                                strRegnId = GmCommonClass.parseNull((String)hmLoop.get("RGID"));
                                                strRegnNm = GmCommonClass.parseNull((String)hmLoop.get("RGNAME"));
                                                
                                                strADID   = GmCommonClass.parseNull((String)hmLoop.get("AD_ID"));
                                                
                                                strDistId = GmCommonClass.parseNull((String)hmLoop.get("DID"));
                                                strDistNm = GmCommonClass.parseNull((String)hmLoop.get("DNAME"));
                                                strTerrID = GmCommonClass.parseNull((String)hmLoop.get("ID"));    
                                                strTerrNm = GmCommonClass.parseNull((String)hmLoop.get("NAME"));
                                                strRepNm  = GmCommonClass.parseNull((String)hmLoop.get("RNAME"));

                                                dbMonthSales = GmCommonClass.parseDouble((Double)hmLoop.get("MTD"));
                                                dbMonthQuota = GmCommonClass.parseDouble((Double)hmLoop.get("QMTD"));
                                                dbMonthPercQuota = GmCommonClass.parseDouble((Double)hmLoop.get("MQPER"));
                                                dbMonthPrevSales = GmCommonClass.parseDouble((Double)hmLoop.get("PMTD"));

                                                dbQuartSales = GmCommonClass.parseDouble((Double)hmLoop.get("QTD"));
                                                dbQuartQuota = GmCommonClass.parseDouble((Double)hmLoop.get("QQTD"));
                                                dbQuartPercQuota = GmCommonClass.parseDouble((Double)hmLoop.get("QQPER"));
                                                dbQuartPrevSales = GmCommonClass.parseDouble((Double)hmLoop.get("PQTD"));

                                                dbYearSales = GmCommonClass.parseDouble((Double)hmLoop.get("YTD"));
                                                dbYearQuota = GmCommonClass.parseDouble((Double)hmLoop.get("QYTD"));
                                                dbYearPercQuota = GmCommonClass.parseDouble((Double)hmLoop.get("YQPER"));
                                                dbYearPrevSales = GmCommonClass.parseDouble((Double)hmLoop.get("PYTD"));
                                                
                                                dbMonthGrowth = GmCommonClass.parseDouble((Double)hmLoop.get("MTDGTHDLR"));
                                                dbMonthGrowthPerc = GmCommonClass.parseDouble((Double)hmLoop.get("MTDGTHPER"));
                                                
                                                dbQuartGrowth = GmCommonClass.parseDouble((Double)hmLoop.get("QTDGTHDLR"));
                                                dbQuartGrowthPerc = GmCommonClass.parseDouble((Double)hmLoop.get("QTDGTHPER"));
                                                
                                                dbYearGrowth = GmCommonClass.parseDouble((Double)hmLoop.get("YTDGTHDLR"));
                                                dbYearGrowthPerc = GmCommonClass.parseDouble((Double)hmLoop.get("YTDGTHPER"));
                                                
                                                if (strRegnId.equals(strNextId))
                                                {
                                                      intCount++;
                                                      if (i > 0)
                                                      {
                                                            strLine = "<TR><TD colspan=23 height=1 class='Line'></TD></TR>";
                                                      }
                                                }
                                                else
                                                {
                                                	strLine = "<TR><TD colspan=23 height=1 class='Line'></TD></TR>";
                                                      
                                                      if (intCount > 0)
                                                      {
                                                            strRegnNm = "";
                                                            strLine = "";
                                                      }
                                                      else
                                                      {
                                                            blFlag = true;
                                                            strLine = "<TR><TD colspan=23 height=1 class='Line'></TD></TR>";
                                                      }
                                                      intCount = 0;
                                                      
                                                }
                                                intDist++;
                                                if (strDistId.equals(strNextDistId))
                                                {
                                                      intAccCount++;
                                                }
                                                else
                                                {
                                                      if (intAccCount > 0)
                                                      {
                                                            strDistNm = "";
                                                            strLine = "";
                                                      }
                                                      else
                                                      {
                                                            bolAcFl = true;
                                                      }
                                                      intAccCount = 0;
                                                }

                                                
                                                // SALES SECTION
                                                dbLev1MonthS = Double.parseDouble(dbMonthSales.toString());
                                                dbLev1MonthSales = dbLev1MonthSales + dbLev1MonthS;
                                                
                                                dbLev2MonthS = Double.parseDouble(dbMonthSales.toString());
                                                dbLev2MonthSales = dbLev2MonthSales + dbLev2MonthS;
                                                
                                                dbLev1QuartS = Double.parseDouble(dbQuartSales.toString());
                                                dbLev1QuartSales = dbLev1QuartSales + dbLev1QuartS;
                                                
                                                dbLev2QuartS = Double.parseDouble(dbQuartSales.toString());
                                                dbLev2QuartSales = dbLev2QuartSales + dbLev2QuartS;

                                                dbLev1YearS = Double.parseDouble(dbYearSales.toString());
                                                dbLev1YearSales = dbLev1YearSales + dbLev1YearS;
                                                
                                                dbLev2YearS = Double.parseDouble(dbYearSales.toString());
                                                dbLev2YearSales = dbLev2YearSales + dbLev2YearS;
                                                
                                                //QUOTA SECTION
                                                dbLev1MonthQ = Double.parseDouble(dbMonthQuota.toString());
                                                dbLev1MonthQuota = dbLev1MonthQuota + dbLev1MonthQ;
                                                
                                                dbLev2MonthQ = Double.parseDouble(dbMonthQuota.toString());
                                                dbLev2MonthQuota = dbLev2MonthQuota + dbLev2MonthQ;
                                                
                                                dbLev1QuartQ = Double.parseDouble(dbQuartQuota.toString());
                                                dbLev1QuartQuota = dbLev1QuartQuota + dbLev1QuartQ;
                                                
                                                dbLev2QuartQ = Double.parseDouble(dbQuartQuota.toString());
                                                dbLev2QuartQuota = dbLev2QuartQuota + dbLev2QuartQ;

                                                dbLev1YearQ = Double.parseDouble(dbYearQuota.toString());
                                                dbLev1YearQuota = dbLev1YearQuota + dbLev1YearQ;
                                                
                                                dbLev2YearQ = Double.parseDouble(dbYearQuota.toString());
                                                dbLev2YearQuota = dbLev2YearQuota + dbLev2YearQ;
                                                
                                                // GROWTH SECTION
                                                dbLev1MonthGD = dbLev1MonthGD + Double.parseDouble(dbMonthGrowth.toString());
                                                dbLev2MonthGD = dbLev2MonthGD + Double.parseDouble(dbMonthGrowth.toString());
                                                
                                                dbLev1QuartGD = dbLev1QuartGD + Double.parseDouble(dbQuartGrowth.toString());
                                                dbLev2QuartGD = dbLev2QuartGD + Double.parseDouble(dbQuartGrowth.toString());
                                                
                                                dbLev1YearGD = dbLev1YearGD + Double.parseDouble(dbYearGrowth.toString());
                                                dbLev2YearGD = dbLev2YearGD + Double.parseDouble(dbYearGrowth.toString());
                                                
                                                dbLev1MonthPrevS = dbMonthPrevSales.doubleValue();
                                                dbLev1MonthPrevSales = dbLev1MonthPrevSales + dbLev1MonthPrevS;
                                                
                                                dbLev2MonthPrevS = dbMonthPrevSales.doubleValue();
                                                dbLev2MonthPrevSales = dbLev2MonthPrevSales + dbLev2MonthPrevS;
                                                
                                                dbLev1QuartPrevS = dbQuartPrevSales.doubleValue();
                                                dbLev1QuartPrevSales = dbLev1QuartPrevSales + dbLev1QuartPrevS;
                                                
                                                dbLev2QuartPrevS = dbQuartPrevSales.doubleValue();
                                                dbLev2QuartPrevSales = dbLev2QuartPrevSales + dbLev2QuartPrevS;

                                                dbLev1YearPrevS = dbYearPrevSales.doubleValue();
                                                dbLev1YearPrevSales = dbLev1YearPrevSales + dbLev1YearPrevS;
                                                
                                                dbLev2YearPrevS = dbYearPrevSales.doubleValue();
                                                dbLev2YearPrevSales = dbLev2YearPrevSales + dbLev2YearPrevS;


                                                //intCount used to see if it's same region or not
                                                if (intCount > 1)
                                                {
                                                      strRegnNm = "";
                                                      strLine = "";
                                                }
                                                //out.print(strLine);
                                               
                                               // ****************************************************	                         
                                               //intAccCount used to see if it's the same distributor
                                               // ****************************************************	
                                                if (intAccCount == 1 || bolAcFl)
                                                {                                                     
                                                      strLevel2.append("<tr id=trAcc" + strRegnId + strDistId + " class='ShadeLevelIII'>");
                                                      strLevel2.append("<td class='RightText' width='10'>&nbsp;</td>");
                                                      strLevel2.append("<td height='23' width='300' class='RightText' colspan='3'>&nbsp;<A class='RightText' ");
                                                      strLevel2.append(" title='Click to Expand'  href=javascript:Toggle('Acc" + strRegnId + strDistId + "') >" + strDistNm +"</a></td>");

                                               		  bolAcFl = false;
                                                }
                                                
                                                //strLevel3.setLength(0);     
                                                strLevel3.append("<tr><td width='10' class='RightText' bgcolor='#ffffff' height='17'></td>" );
                                                strLevel3.append("<td width='20' class='RightText' bgcolor='#ffffff'></td>" );
                                                strLevel3.append("<td width='140' bgcolor='#ffffff' class='RightText' colspan='2'>&nbsp;" + strTerrNm + "</td>" ); 
                                                strLevel3.append("<td width='140' bgcolor='#ffffff' class='RightText' colspan='2'>&nbsp;" + strRepNm + "</td>" );
                                                strLevel3.append("<td width='45' bgcolor='#ffffff'  align='right' >" + printDrillLink(strTerrNm,strTerrID,0)  + "</td>" );         
                                                strLevel3.append("<td width='65' class=ShadeMedGreenTD align='right'>"+strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbMonthSales.toString())) + "</td>" );
                                                strLevel3.append("<td width='65' class=ShadeLightGreenTD align='right'>"+strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbMonthPrevSales.toString())) + "</td>" );
                                                strLevel3.append("<td width='65' class=ShadeMedGreenTD align='right'>"+strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbMonthQuota.toString())) + "</td>" );
                                                strLevel3.append("<td width='65' class=ShadeLightGreenTD align='right'>" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbMonthPercQuota.doubleValue())) + "</td>" );
                                                strLevel3.append("<td width='65' class=ShadeMedGreenTD align='right'>"+strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbMonthGrowth.toString())) + "</td>" );
                                                strLevel3.append("<td width='65' class=ShadeLightGreenTD align='right'>" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbMonthGrowthPerc.doubleValue() )) + "</td>" );
                                                strLevel3.append("<td width='65' class=ShadeMedBlueTD align='right'>"+strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbQuartSales.toString())) + "</td>" );
                                                strLevel3.append("<td width='65' class=ShadeLightBlueTD align='right'>"+strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbQuartPrevSales.toString())) + "</td>" );
                                                strLevel3.append("<td width='65' class=ShadeMedBlueTD align='right'>"+strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbQuartQuota.toString())) + "</td>" );
                                                strLevel3.append("<td width='65' class=ShadeLightBlueTD align='right'>" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbQuartPercQuota.doubleValue() )) + "</td>" );
                                                strLevel3.append("<td width='65' class=ShadeMedBlueTD align='right'>"+strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbQuartGrowth.toString())) + "</td>" );
                                                strLevel3.append("<td width='65' class=ShadeLightBlueTD align='right'>" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbQuartGrowthPerc.doubleValue() )) + "</td>" );
                                                strLevel3.append("<td width='65' class=ShadeMedOrangeTD align='right'>"+strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbYearSales.toString())) + "</td>" );
                                                strLevel3.append("<td width='65' class=ShadeLightOrangeTD align='right'>"+strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbYearPrevSales.toString())) + "</td>" );
                                                strLevel3.append("<td width='65' class=ShadeMedOrangeTD align='right'>"+strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbYearQuota.toString())) + "</td>" );
                                                strLevel3.append("<td width='65' class=ShadeLightOrangeTD align='right'>" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbYearPercQuota.doubleValue() )) + "</td>" );
                                                strLevel3.append("<td width='65' class=ShadeMedOrangeTD align='right'>"+strCurrSign+ GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbYearGrowth.toString())) + "</td>" );
                                                strLevel3.append("<td width='65' class=ShadeLightOrangeTD align='right'>" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbYearGrowthPerc.doubleValue())) + "</td></tr>");
                                                strLevel3.append("<tr><td colspan=23 bgcolor=#CCCCCC></td></tr>");
                                                //out.println(strLevel3);

	   										// ****************************************************	
 											//for the first region or different region display
											// ****************************************************	
												if (intCount == 1 || blFlag)
												{	
												
												      strLevel1.append("<tr id='tr" + strRegnId + "' class='ShadeLevelI'>");
												      strLevel1.append("<td colspan='4' width='310' height='23' style=\"font-style: normal;\" >&nbsp;<a class='RightText'");
												      strLevel1.append(" title='Click to Expand Region22 Information' href=javascript:Toggle('"+ strRegnId +"')>"+ strRegnNm +"</a></td>");
												      strLevel1.append("<td width='45' align='right'> " + printDrillLink(strRegnNm,strADID,2)  + "</td>" );     
												      strLevel1.append("<td width='65' class='ShadeMedGreenTD'  align='Right'  id=tdMonthLvl1" + strRegnId + ">&nbsp; </td>" );
												      strLevel1.append("<td width='65' class='ShadeLightGreenTD' align='Right' id=tdPMonthLvl1" + strRegnId + " >&nbsp; </td>" );
												      strLevel1.append("<td width='65' class='ShadeMedGreenTD'  align='Right' id=tdMonthLvl1Q" + strRegnId + " >&nbsp;</td>" );
												      
												      strLevel1.append("<td width='65' class=ShadeLightGreenTD align='Right' id=tdMonthLvl1PQ" + strRegnId + ">&nbsp;</td>" );
												      strLevel1.append("<td width='65' class=ShadeMedGreenTD     align='Right' id=tdMonthLvl1GD" + strRegnId + ">&nbsp;</td>" ); 
												      strLevel1.append("<td width='65' class=ShadeLightGreenTD align='Right' id=tdMonthLvl1GP" + strRegnId + ">&nbsp;</td>" ); 
												      strLevel1.append("<td width='65' class=ShadeMedBlueTD      align='Right' id=tdQuartLvl1" + strRegnId + ">&nbsp;</td>" ); 
												      strLevel1.append("<td width='65' class=ShadeLightBlueTD  align='Right' id=tdPQuartLvl1" + strRegnId + ">&nbsp;</td>" ); 
												      strLevel1.append("<td width='65' class=ShadeMedBlueTD      align='Right' id=tdQuartLvl1Q" + strRegnId + ">&nbsp;</td>" ); 
												      strLevel1.append("<td width='65' class=ShadeLightBlueTD  align='Right' id=tdQuartLvl1PQ" + strRegnId + ">&nbsp;</td>" ); 
												      strLevel1.append("<td width='65' class=ShadeMedBlueTD      align='Right' id=tdQuartLvl1GD" + strRegnId + ">&nbsp;</td>" ); 
												      strLevel1.append("<td width='65' class=ShadeLightBlueTD  align='Right' id=tdQuartLvl1GP" + strRegnId + ">&nbsp;</td>" ); 
												      strLevel1.append("<td width='65' class=ShadeMedOrangeTD  align='Right' id=tdYearLvl1" + strRegnId + ">&nbsp;</td>" ); 
												      strLevel1.append("<td width='65' class=ShadeLightOrangeTD  align='Right' id=tdPYearLvl1" + strRegnId + ">&nbsp;</td>" ); 
												      strLevel1.append("<td width='65' class=ShadeMedOrangeTD  align='Right' id=tdYearLvl1Q" + strRegnId + ">&nbsp;</td>" ); 
												      strLevel1.append("<td width='65' class=ShadeLightOrangeTD align='Right' id=tdYearLvl1PQ" + strRegnId + ">&nbsp;</td>" ); 
												      strLevel1.append("<td width='65' class=ShadeMedOrangeTD  align='Right' id=tdYearLvl1GD" + strRegnId + ">&nbsp;</td>" ); 
												      strLevel1.append("<td width='65' class=ShadeLightOrangeTD align='Right' id=tdYearLvl1GP" + strRegnId + ">&nbsp;</td>");
												      strLevel1.append("</tr>");
												      strLevel1.append("<tr>");
												      strLevel1.append("<td colspan=23 bgcolor=#cccccc>");
												      strLevel1.append("<table style=display:none width=100% cellpadding=0 cellspacing=0 border=0 id=tab"+strRegnId+">");
												      blFlag = false;
												}
												// ****************************************************	
												//intAccCount used to see if it's the same distributor and if it's the last one
												// ****************************************************	
                                                if (intAccCount == 0 ||  i == intLength-1)
                                                {
                                                      dbLev2MonthPQ = dbLev2MonthQuota == 0?0:(dbLev2MonthSales / dbLev2MonthQuota);
                                                      dbLev2QuartPQ = dbLev2QuartQuota == 0?0:(dbLev2QuartSales / dbLev2QuartQuota);
                                                      dbLev2YearPQ = dbLev2YearQuota == 0?0:(dbLev2YearSales / dbLev2YearQuota);
                                                      
                                                      dbLev2MonthGP = dbLev2MonthPrevSales == 0?0:((dbLev2MonthSales-dbLev2MonthPrevSales)/dbLev2MonthPrevSales);
                                                      dbLev2QuartGP = dbLev2QuartPrevSales == 0?0:((dbLev2QuartSales-dbLev2QuartPrevSales)/dbLev2QuartPrevSales);
                                                      dbLev2YearGP = dbLev2YearPrevSales == 0?0:((dbLev2YearSales-dbLev2YearPrevSales)/dbLev2YearPrevSales);
                                                      
                                                     
                                                      strLevel2.append("<td width='45' align='right'> " + printDrillLink(strDistNm,strDistId,1)  + "</td>" );
                                                      strLevel2.append("<td width='65' class=ShadeMedGreenTD align='Right' id=tdMonthLvl2" + strRegnId  + strDistId +   " style=font-style: italic;>"+strCurrSign+GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev2MonthSales))+"</td>");
                                                      strLevel2.append("<td width='65' class=ShadeLightGreenTD align='Right' id=tdPMonthLvl2" + strRegnId  + strDistId +  " style=font-style: italic;>"+strCurrSign+GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev2MonthPrevSales))+"</td>" );
                                                      strLevel2.append("<td width='65' class=ShadeMedGreenTD align='Right' id=tdMonthLvl2Q" + strRegnId  + strDistId +   " style=font-style: italic;>"+strCurrSign+GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev2MonthQuota))+"</td>" );
                                                      strLevel2.append("<td width='65' class=ShadeLightGreenTD align='Right' id=tdMonthLvl2PQ" + strRegnId  + strDistId +  " style=font-style: italic;>"+GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbLev2MonthPQ))+"</td>" );
                                                      strLevel2.append("<td width='65' class=ShadeMedGreenTD align='Right' id=tdMonthLvl2GD" + strRegnId  + strDistId +  " style=font-style: italic;>"+strCurrSign+GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev2MonthGD))+"</td>" );
                                                      strLevel2.append("<td width='65' class=ShadeLightGreenTD align='Right' id=tdMonthLvl2GP" + strRegnId + strDistId +  " style=font-style: italic;>"+GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbLev2MonthGP))+"</td>" );
                                                      strLevel2.append("<td width='65' class=ShadeMedBlueTD align='Right' id=tdQuartLvl2" + strRegnId  + strDistId +   " style=font-style: italic;>"+strCurrSign+GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev2QuartSales))+"</td>" );
                                                      strLevel2.append("<td width='65' class=ShadeLightBlueTD align='Right' id=tdPQuartLvl2" + strRegnId  + strDistId +  " style=font-style: italic;>"+strCurrSign+GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev2QuartPrevSales))+"</td>" );
                                                      strLevel2.append("<td width='65' class=ShadeMedBlueTD align='Right' id=tdQuartLvl2Q" + strRegnId  + strDistId +   " style=font-style: italic;>"+strCurrSign+GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev2QuartQuota))+"</td>" );
                                                      strLevel2.append("<td width='65' class=ShadeLightBlueTD align='Right' id=tdQuartLvl2PQ" + strRegnId  + strDistId +  " style=font-style: italic;>"+GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbLev2QuartPQ))+"</td>" );
                                                      strLevel2.append("<td width='65' class=ShadeMedBlueTD align='Right' id=tdQuartLvl2GD" + strRegnId  + strDistId +  " style=font-style: italic;>"+strCurrSign+GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev2QuartGD))+"</td>" );
                                                      strLevel2.append("<td width='65' class=ShadeLightBlueTD align='Right' id=tdQuartLvl2GP" + strRegnId  + strDistId +  " style=font-style: italic;>"+GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbLev2QuartGP))+"</td>" );
                                                      strLevel2.append("<td width='65' class=ShadeMedOrangeTD align='Right' id=tdYearLvl2" + strRegnId + strDistId +   " style=font-style: italic;>"+strCurrSign+GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev2YearSales))+"</td>" );
                                                      strLevel2.append("<td width='65' class=ShadeLightOrangeTD align='Right' id=tdPYearLvl2" + strRegnId  + strDistId +  " style=font-style: italic;>"+strCurrSign+GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev2YearPrevSales))+"</td>" );
                                                      strLevel2.append("<td width='65' class=ShadeMedOrangeTD align='Right' id=tdYearLvl2Q" + strRegnId + strDistId +   " style=font-style: italic;>"+strCurrSign+GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev2YearQuota))+"</td>" );
                                                      strLevel2.append("<td width='65' class=ShadeLightOrangeTD align='Right' id=tdYearLvl2PQ" + strRegnId + strDistId + " style=font-style: italic;>"+GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbLev2YearPQ))+"</td>" );
                                                      strLevel2.append("<td width='65' class=ShadeMedOrangeTD align='Right' id=tdYearLvl2GD" + strRegnId  + strDistId + " style=font-style: italic;>"+strCurrSign+GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev2YearGD))+"</td>" );
                                                      strLevel2.append("<td width='65' class=ShadeLightOrangeTD align='Right' id=tdYearLvl2GP" + strRegnId + strDistId + " style=font-style: italic;>"+GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbLev2YearGP))+"</td>");
                                                      strLevel2.append("</tr>");  
                                                      strLevel2.append("<tr>");
                                                      strLevel2.append("<td colspan=23 bgcolor=#cccccc>");
                                                      strLevel2.append("<table style=display:none width=100% cellpadding=0 cellspacing=0 border=0 id=tabAcc"+strRegnId+strDistId+">");
                                             
                                                     out.print(strLevel1);
                                                     out.print(strLevel2);
                                                     out.print(strLevel3);
                                                     out.println("</table></td></tr>"); //closing level 2 
                                                     out.flush();
                                                     
                                                 
                                                      strLevel1.setLength(0);
                                                      strLevel2.setLength(0);
                                                      strLevel3.setLength(0); 
                                                      
                                                     
                                                      
                                                      dbAcctTotal = 0.0;
                                                      dbLev2MonthSales = 0.0;
                                                      dbLev2QuartSales = 0.0;
                                                      dbLev2YearSales = 0.0;
                                                      dbLev2MonthQuota = 0.0;
                                                      dbLev2QuartQuota = 0.0;
                                                      dbLev2YearQuota = 0.0;
                                                      dbLev2MonthPQ = 0.0;
                                                      dbLev2QuartPQ = 0.0;
                                                      dbLev2YearPQ = 0.0;
                                                      dbLev2MonthGD = 0.0;
                                                      dbLev2QuartGD = 0.0;
                                                      dbLev2YearGD = 0.0;
                                                      dbLev2MonthPrevSales = 0.0;
                                                      dbLev2QuartPrevSales = 0.0;
                                                      dbLev2YearPrevSales = 0.0;
                                                      
                                                      

                                                }
                                               
                                                // Below execute region total 
                                                if ( intCount == 0 || i == intLength-1)
                                                {                                                	 
                                                      out.println("</table></td></tr>"); //closing level 1
                                                      
                                                      dbLev1MonthPQ = dbLev1MonthQuota ==0?0:(dbLev1MonthSales / dbLev1MonthQuota);
                                                      dbLev1QuartPQ = dbLev1QuartQuota == 0?0:(dbLev1QuartSales / dbLev1QuartQuota);
                                                      dbLev1YearPQ = dbLev1YearQuota == 0?0:(dbLev1YearSales / dbLev1YearQuota);
                                                      
                                                      dbLev1MonthGP = dbLev1MonthPrevSales == 0?0:((dbLev1MonthSales-dbLev1MonthPrevSales)/dbLev1MonthPrevSales);
                                                      dbLev1QuartGP = dbLev1QuartPrevSales == 0?0:((dbLev1QuartSales-dbLev1QuartPrevSales)/dbLev1QuartPrevSales);
                                                      dbLev1YearGP = dbLev1YearPrevSales == 0?0:((dbLev1YearSales-dbLev1YearPrevSales)/dbLev1YearPrevSales);
                                                      
                                                      // calculating the value to be displayed in grand total
                                                      dbLev3MonthSales  = dbLev3MonthSales + dbLev1MonthSales;
                                                      dbLev3MonthPrevSales = dbLev3MonthPrevSales + dbLev1MonthPrevSales;
                                                      dbLev3QuartSales  = dbLev3QuartSales + dbLev1QuartSales;
                                                      dbLev3QuartPrevSales = dbLev3QuartPrevSales + dbLev1QuartPrevSales;
                                                      dbLev3YearSales    = dbLev3YearSales + dbLev1YearSales;
                                                      dbLev3YearPrevSales     = dbLev3YearPrevSales + dbLev1YearPrevSales;
                                                      dbLev3MonthQuota  = dbLev3MonthQuota + dbLev1MonthQuota;
                                                      dbLev3QuartQuota  = dbLev3QuartQuota + dbLev1QuartQuota;
                                                      dbLev3YearQuota    = dbLev3YearQuota + dbLev1YearQuota;
                                                      dbLev3MonthGD            = dbLev3MonthGD  + dbLev1MonthGD;
                                                      dbLev3QuartGD            = dbLev3QuartGD + dbLev1QuartGD;
                                                      dbLev3YearGD             = dbLev3YearGD  + dbLev1YearGD;
                                                      
                                                      
                                                      
                                                      
                                              /*      totalLev1MonthSales = dbLev1MonthSales;
                      						        totalLev1QuartSales = dbLev1QuartSales;
                      						        totalLev1YearSales = dbLev1YearSales;
                      						        totalLev1MonthQuota = dbLev1MonthQuota;
                      						        totalLev1QuartQuota = dbLev1QuartQuota;
                      						        totalLev1YearQuota = dbLev1YearQuota;
                      						        totalLev1MonthPQ = dbLev1MonthPQ;
                      						        totalLev1QuartPQ = dbLev1QuartPQ;      
                      						        totalLev1YearPQ = dbLev1YearPQ;      
                      						        totalLev1MonthGD = dbLev1MonthGD;
                      						        totalLev1QuartGD = dbLev1QuartGD;  
                      						        totalLev1YearGD = dbLev1YearGD;  
                      						            
                      						        totalLev1MonthGP = dbLev1MonthGP;
                      						        totalLev1QuartGP = dbLev1QuartGP;
                      						        totalLev1YearGP = dbLev1YearGP;
                      						        totalLev1MonthPrevSales = dbLev1MonthPrevSales;
                      						        totalLev1QuartPrevSales = dbLev1QuartPrevSales;
                      						        totalLev1YearPrevSales = dbLev1YearPrevSales;*/

 // **************************************************
   // Below code to calcualte First level value 
   // **************************************************
%>
<script>
	document.all.tdMonthLvl1<%=strRegnId%>.innerHTML = "<%=strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev1MonthSales))%>";
	document.all.tdQuartLvl1<%=strRegnId%>.innerHTML = "<%=strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev1QuartSales))%>";
	document.all.tdYearLvl1<%=strRegnId%>.innerHTML = "<%=strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev1YearSales))%>";
	document.all.tdMonthLvl1Q<%=strRegnId%>.innerHTML = "<%=strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev1MonthQuota))%>";
	document.all.tdQuartLvl1Q<%=strRegnId%>.innerHTML = "<%=strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev1QuartQuota))%>";
	document.all.tdYearLvl1Q<%=strRegnId%>.innerHTML = "<%=strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev1YearQuota))%>";

	// Start HERE
	document.all.tdMonthLvl1PQ<%=strRegnId%>.innerHTML = "<%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbLev1MonthPQ))%>";
    // *************
	
	document.all.tdQuartLvl1PQ<%=strRegnId%>.innerHTML = "<%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbLev1QuartPQ))%>";
	document.all.tdYearLvl1PQ<%=strRegnId%>.innerHTML = "<%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbLev1YearPQ))%>";
	document.all.tdMonthLvl1GD<%=strRegnId%>.innerHTML = "<%=strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev1MonthGD))%>";
	document.all.tdQuartLvl1GD<%=strRegnId%>.innerHTML = "<%=strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev1QuartGD))%>";
	document.all.tdYearLvl1GD<%=strRegnId%>.innerHTML = "<%=strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev1YearGD))%>";
	document.all.tdMonthLvl1GP<%=strRegnId%>.innerHTML = "<%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbLev1MonthGP))%>";
	document.all.tdQuartLvl1GP<%=strRegnId%>.innerHTML = "<%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbLev1QuartGP))%>";
	document.all.tdYearLvl1GP<%=strRegnId%>.innerHTML = "<%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbLev1YearGP))%>";
	document.all.tdPMonthLvl1<%=strRegnId%>.innerHTML = "<%=strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev1MonthPrevSales))%>";
	document.all.tdPQuartLvl1<%=strRegnId%>.innerHTML = "<%=strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev1QuartPrevSales))%>";
	document.all.tdPYearLvl1<%=strRegnId%>.innerHTML = "<%=strCurrSign + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev1YearPrevSales))%>";
</script>
<%
                              dbLev1MonthSales = 0.0;
                              dbLev1QuartSales = 0.0;
                              dbLev1YearSales = 0.0;
                              dbLev1MonthQuota = 0.0;
                              dbLev1QuartQuota = 0.0;
                              dbLev1YearQuota = 0.0;
                              dbLev1MonthPQ = 0.0;
                              dbLev1QuartPQ = 0.0;
                              dbLev1YearPQ = 0.0;
                              dbLev1MonthGD = 0.0;
                              dbLev1QuartGD = 0.0;
                              dbLev1YearGD = 0.0;
                              dbLev1MonthPrevSales = 0.0;
                              dbLev1QuartPrevSales = 0.0;
                              dbLev1YearPrevSales = 0.0;
                              
                        }

                  }// end of FOR
				  
				  
                  // Calculate grand total % and growth to be displayed in the system 
                  dbLev3MonthPQ = dbLev3MonthQuota ==0?0:(dbLev3MonthSales / dbLev3MonthQuota);
                  dbLev3QuartPQ = dbLev3QuartQuota == 0?0:(dbLev3QuartSales / dbLev3QuartQuota);
                  dbLev3YearPQ = dbLev3YearQuota == 0?0:(dbLev3YearSales / dbLev3YearQuota);
                  
                  dbLev3MonthGP = dbLev3MonthPrevSales == 0?0:((dbLev3MonthSales-dbLev3MonthPrevSales)/dbLev3MonthPrevSales);
                  dbLev3QuartGP = dbLev3QuartPrevSales == 0?0:((dbLev3QuartSales-dbLev3QuartPrevSales)/dbLev3QuartPrevSales);
                  dbLev3YearGP = dbLev3YearPrevSales == 0?0:((dbLev3YearSales-dbLev3YearPrevSales)/dbLev3YearPrevSales);
%>
<% //********************************************************
   // Below is grand total logic 
   //********************************************************
%>           
    <tr><td colspan="23" class="Line"></td></tr>

  <tr ><td colspan="23" >
  <table cellpadding=0 cellspacing=0 >
 	 <tr  class="ShadeRightTableCaption">
        <td colspan="4" width="310" height="23">&nbsp;<fmtTerrGrowthQuotaReport:message key="LBL_GRAND_TOTAL"/> </td>
        <td width="45" align="right"></td>  
        <td width="65" class="ShadeDarkGreenTD"  align="Right" ><%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev3MonthSales))%></td>
        <td width="65" class="ShadeDarkGreenTD"  align="Right" ><%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev3MonthPrevSales))%></td>
        <td width="65" class="ShadeDarkGreenTD"  align="Right" ><%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev3MonthQuota))%></td>
        <td width="65" class="ShadeDarkGreenTD"  align="Right" ><%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbLev3MonthPQ))%></td>
        <td width="65" class="ShadeDarkGreenTD"  align="Right" ><%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev3MonthGD))%></td>
        <td width="65" class="ShadeDarkGreenTD"  align="Right" ><%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbLev3MonthGP))%></td>
        <td width="65" class="ShadeDarkBlueTD"     align="Right" ><%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev3QuartSales))%></td>
        <td width="65" class="ShadeDarkBlueTD"     align="Right" ><%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev3QuartPrevSales))%></td>
        <td width="65" class="ShadeDarkBlueTD"     align="Right" ><%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev3QuartQuota))%></td>
        <td width="65" class="ShadeDarkBlueTD"     align="Right" ><%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbLev3QuartPQ))%></td>
        <td width="65" class="ShadeDarkBlueTD"   align="Right" ><%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev3QuartGD))%></td>
        <td width="65" class="ShadeDarkBlueTD"   align="Right" ><%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbLev3QuartGP))%></td>
        <td width="65" class="ShadeDarkOrangeTD" align="Right" ><%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev3YearSales))%></td>
        <td width="65" class="ShadeDarkOrangeTD" align="Right" ><%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev3YearPrevSales))%></td>
        <td width="65" class="ShadeDarkOrangeTD" align="Right" ><%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev3YearQuota))%></td>
        <td width="65" class="ShadeDarkOrangeTD" align="Right" ><%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbLev3YearPQ))%></td>
        <td width="65" class="ShadeDarkOrangeTD" align="Right"><%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(dbLev3YearGD))%></td>
        <td width="65" class="ShadeDarkOrangeTD" align="Right"><%=GmCommonClass.getRedTextSmall(GmCommonClass.getStringInPerFormat(dbLev3YearGP))%></td>
        </tr>
   </table></td></tr>
	<tr><td colspan="23" ></td></tr>
                              
<%
   } // End of IF
else{
%>
    <tr><td colspan="23" class="Line"></td></tr>                            
    <tr><td colspan="23" class="RightTextRed" height="50" align=center><fmtTerrGrowthQuotaReport:message key="LBL_NO_SALES"/> </td></tr>
<%                            
    }
%>
</table>

</td>
<td  width="1"><img src="<%=strImagePath%>/spacer.gif"></td>
</tr>

<tr>
<td colspan="5" height="1" ></td>
</tr>

</table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

<%!
      // To fetch the link information 
      private String printDrillLink(String Value, String ID, int intHierarchy)
      {
          StringBuffer strValue = new StringBuffer();

        // Account drilldown
        strValue.append("<img id='imgEditA' style='cursor:hand' ");  
        strValue.append("src='images/HospitalIcon.gif' "); 
        strValue.append("title='Click to View Account information' width='12' height='13' ") ;
        strValue.append("onClick=\"javascript:fnCallAccount('" );
        strValue.append(ID);
        strValue.append("', '");
        strValue.append(intHierarchy);
        strValue.append("' , '" + Value + "' )\"/> ");

          // Group drilldown
          strValue.append("<img id='imgEditG' style='cursor:hand' ");  
        strValue.append("src='/images/ordsum.gif' "); 
        strValue.append("title='Click to View System Information' width='12' height='13' ") ;
        strValue.append("onClick=\"javascript:fnCallSetNumber('" );
        strValue.append(ID);
        strValue.append("', '");
        strValue.append(intHierarchy);
        strValue.append("' , '" + Value + "' )\"/> ");

            //Part Number drilldown
        strValue.append("<img id='imgEditP' style='cursor:hand' ");  
        strValue.append("src='/images/product_icon.jpg' "); 
        strValue.append("title='Click to View Part # Details' width='12' height='13' ") ;
        strValue.append("onClick=\"javascript:fnCallPart('" );
        strValue.append(ID);
        strValue.append("', '");
        strValue.append(intHierarchy);
        strValue.append("' , '" + Value + "' )\"/> ");                                    

          return strValue.toString();
      }
%>


