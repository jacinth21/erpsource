 <%
/**********************************************************************************
 * File			 	: GmSalesGrowthReport.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*,java.text.DecimalFormat" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtSalesGrowthReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- sales\GmSalesGrowthReport.jsp -->
<fmtSalesGrowthReport:setLocale value="<%=strLocale%>"/>
<fmtSalesGrowthReport:setBundle basename="properties.labels.sales.GmSalesGrowthReport"/>
<%
String strsalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	int intSize = 0;
	int intLength = 0;

	String strWikiTitle = GmCommonClass.getWikiTitle("QUOTA_PERFORMANCE_REPORT");
	String strCodeID = "";
	String strSelected = "";
	String strVendId = "";
	String strCurDate = "";
	
	String strVal ="0.0" ; 
	String strGrowth ="0.0" ;
	String strQuota ="0.0";
	String strPVal ="0.0";
	String strPercent ="0.0";
	String strPercentQuota ="0.0" ;
	String strExpQuota ="0.0";
	String strFooterClass1 = "ShadeDarkGreenTD";
	String strFooterClass2 = "ShadeDarkBlueTD";
	String strForeColor ="<span style=\"color:red;\">";
	
	HashMap hmLoadReturn = new HashMap();
	HashMap hcboVal = new HashMap();
  
	ArrayList alMonthDropDown = new ArrayList();
	ArrayList alYearDropDown = new ArrayList();

  	String strOpt = (String)request.getAttribute("hOpt")==null?"":(String)request.getAttribute("hOpt");
  	String strHeader = (String)request.getAttribute("hHeader")==null?"":(String)request.getAttribute("hHeader");
  	
  	
  	
  	String monthDisplay   = (String)request.getParameter("hMonth")==null?"":(String)request.getParameter("hMonth");
  	String quarterDisplay = (String)request.getParameter("hQuarter")==null?"":(String)request.getParameter("hQuarter");
  	String yearDisplay 	  = (String)request.getParameter("hYear")==null?"":(String)request.getParameter("hYear");
  	String PreYearDisplay = (String)request.getParameter("hPreYear")==null?"":(String)request.getParameter("hPreYear");
  	
	monthDisplay   = monthDisplay.equals("")?(String)request.getAttribute("hMonth"):monthDisplay;
  	quarterDisplay   = quarterDisplay.equals("")?(String)request.getAttribute("hQuarter"):quarterDisplay;
  	yearDisplay   = yearDisplay.equals("")?(String)request.getAttribute("hYear"):yearDisplay;
  	PreYearDisplay   = PreYearDisplay.equals("")?(String)request.getAttribute("hPreYear"):PreYearDisplay;
  	
  	monthDisplay   = monthDisplay.equals("")?"off":monthDisplay;
  	quarterDisplay   = quarterDisplay.equals("")?"off":quarterDisplay;
  	yearDisplay   = yearDisplay.equals("")?"off":yearDisplay;
  	PreYearDisplay   = PreYearDisplay.equals("")?"off":PreYearDisplay;
  	
  	if (monthDisplay.equals("off")!=true) 
	{	
		strFooterClass1 = "ShadeDarkGreenTD";
	}else if (quarterDisplay.equals("off")!=true) 
	{	
		strFooterClass1 = "ShadeDarkBlueTD";
	}else if (yearDisplay.equals("off")!=true) 
	{	
		strFooterClass1 = "ShadeDarkOrangeTD";
	}
	if (monthDisplay.equals("off")!=true && quarterDisplay.equals("off")!=true ) 
	{	
		strFooterClass2 = "ShadeDarkBlueTD";
	}else
	{	
		strFooterClass2 = "ShadeDarkOrangeTD";
	}	
  	
  	String strDisplay ="pdf";
  	
  	
  	String strApplCurrFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplCurrFmt"));
  	//String strCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
  	//String strCurrFmt = "{0,number,"+strCurrSign+strApplCurrFmt+"}";
  	String strCurrFmt = "{0,number,"+strApplCurrFmt+"}";
  	String strCurrSign = "";

    //Below code is to track down the visits on the report.
	String strUserId = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
	String strShUserName = GmCommonClass.parseNull((String)session.getAttribute("strSessShName"));
	String strUserName = strUserId + " - " + strShUserName;
	//Visit code ends
  	
  	 if (strOpt.equals("VP")||strOpt.equals("AD") || strOpt.equals("TERR") || strOpt.equals("DIST"))  {  		 
  		 strDisplay = "html csv xml pdf";
  	 }
  	
  	
  	alMonthDropDown = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALMONTHDROPDOWN"));
  	alYearDropDown = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALYEARDROPDOWN"));
  	
  	String strToMonth = GmCommonClass.parseNull((String)request.getAttribute("Cbo_ToMonth"));
  	String strToYear = GmCommonClass.parseNull((String)request.getAttribute("Cbo_ToYear"));

	double dbTemp = 0;
	double dbTempQ = 0;
  	DecimalFormat df = new DecimalFormat("#0.00 %");

	             String strColumn4 =  "";
				 String strColumn5 =  "";
				 String strColumn6 =  "";
				 String strColumn7 =  "";
				 String strColumn8 =  "";
				 String strColumn9 =  "";
				 String strColumn10 =  "";
				 String strColumn11 =  "";				
				 String strColumn12 =  "";				
				 String strColumn13 =  "";
				 String strColumn14 =  "";
				 String strColumn15 =  "";
				 String strColumn16 =  "";
				 String strColumn17 =  "";
				 String strColumn18 =  "";
				 String strColumn19 =  "";
				 String strColumn20 =  "";
				 String strColumn21 =  "";
				 String strColumn22 =  "";
				 String strColumn23 =  "";
				 String strColumn24 =  "";
				 String strColumn25 =  "";
				 String strColumn26 =  "";
				 String strColumn27 =  "";	
	String strBtnClick ="fnGo('"+strOpt+"');";
  	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Sales Growth Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     textarea {
	width: 60%;
	height: 60px;
	}
	
	table.DtTable1000 {
	width: 1100px;
	}
</style>

<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strsalesJsPath%>/GmSalesFilter.js"></script>
<script> downloadPiwikJS();</script>
<script> fnTracker('<%=strHeader %>', '<%=strUserName%>');</script>
<script>
function fnCallHosp(obj, Type)
{
	//fnSetSelections();
	document.frmMain.hAction.value = "Reload";
	document.frmMain.strSessOpt.value = "ACCT";
	if (Type=='TERR') 
	{
		document.frmMain.hTerritory.value = obj.id;
	}
	else if (Type=='A') 
	{
		document.frmMain.hAccountID.value = obj.id;
	}
	else if (Type=='R') 
	{
		document.frmMain.hRepID.value = obj.id;
	}
	else if (Type=='G') 
	{
		document.frmMain.hSetNumber.value = obj.id;
	}
	else if (Type=='D') 
	{
		document.frmMain.hDistributorID.value = obj.id;
	}

	//document.frmMain.submit();
	url='<%=strServletPath%>/GmSalesGrowthServlet?&hAction=Reload';
	fnReloadSales('',url);
}

function fnCallGrp(obj, Type)
{
	//fnSetSelections();
	document.frmMain.hAction.value = "Reload";
	document.frmMain.strSessOpt.value = "PGRP";
	
	if (Type=='T') 
	{
		document.frmMain.hTerritory.value = obj.id;
	}
	else if (Type=='A') 
	{
		document.frmMain.hAccountID.value = obj.id;
	}
	else if (Type=='R') 
	{
		document.frmMain.hRepID.value = obj.id;
	}
	else if (Type=='G') 
	{
		document.frmMain.hSetNumber.value = obj.id;
	}
	else if (Type=='D') 
	{
		document.frmMain.hDistributorID.value = obj.id;
	}
	//document.frmMain.submit();
	url='<%=strServletPath%>/GmSalesGrowthServlet?&hAction=Reload';
	fnReloadSales('',url);
}

function fnCallSpineS(obj, Type)
{
	//fnSetSelections();
	document.frmMain.hAction.value = "Reload";
	document.frmMain.strSessOpt.value = "SREP";
	
	if (Type=='T') 
	{
		document.frmMain.hTerritory.value = obj.id;
	}
	else if (Type=='A') 
	{
		document.frmMain.hAccountID.value = obj.id;
	}
	else if (Type=='R') 
	{
		document.frmMain.hRepID.value = obj.id;
	}
	else if (Type=='G') 
	{
		document.frmMain.hSetNumber.value = obj.id;
	}
	else if (Type=='D') 
	{
		document.frmMain.hDistributorID.value = obj.id;
	}	

	//document.frmMain.submit();
	url='<%=strServletPath%>/GmSalesGrowthServlet?&hAction=Reload';
	fnReloadSales('',url);
}
function fnCallFieldSalesBy(obj,Type,TypeTo,reportType)
{
	//fnSetSelections();
	document.frmMain.hAction.value = "Reload";
	document.frmMain.strSessOpt.value = TypeTo;
	if (Type=='VP') 
	{
		document.frmMain.hVPID.value = obj.id; 
	}
	else if (Type=='DIST')
	{
		document.frmMain.hDistributorID.value = obj.id;
		document.frmMain.hReportType.value = reportType;//33147 Quota Performance
	}
		else if (Type=='AD')
	{

		//document.frmMain.hAreaDirector.value = obj.id;
		document.frmMain.hRegnID.value = obj.id;
		document.frmMain.hReportType.value = reportType;
	}
		else if (Type=='TERR')
	{
		document.frmMain.hTerrFilter.value = obj.id;
	}
	//document.frmMain.submit();	
	url='<%=strServletPath%>/GmSalesGrowthServlet?&hAction=Reload';
	fnReloadSales('',url);
}
function fnCallPart(obj, Type)
{
	//fnSetSelections();
	document.frmMain.hAction.value = "Reload";
	document.frmMain.strSessOpt.value = "PART";
	
	if (Type=='T') 
	{
		document.frmMain.hTerritory.value = obj.id;
	}
	else if (Type=='A') 
	{
		document.frmMain.hAccountID.value = obj.id;
	}
	else if (Type=='R') 
	{
		document.frmMain.hRepID.value = obj.id;
	}
	else if (Type=='G') 
	{
		document.frmMain.hSetNumber.value = obj.id;
	}
	else if (Type=='D') 
	{
		document.frmMain.hDistributorID.value = obj.id;
	}
		
	//document.frmMain.submit();
	url='<%=strServletPath%>/GmSalesGrowthServlet?&hAction=Reload';
	fnReloadSales('',url);
}

function fnCallFieldSales(obj, Type)
{
	document.frmMain.hAction.value = "Reload";
	document.frmMain.strSessOpt.value = "DIST";
	
	if (Type=='AD') 
	{
		document.frmMain.hRegnFilter.value = obj.id;
	}
	//document.frmMain.submit();
	url='<%=strServletPath%>/GmSalesGrowthServlet?&hAction=Reload';
	fnReloadSales('',url);
}

function fnLoad()
{	

	document.frmMain.Cbo_ToMonth.value = '<%=request.getAttribute("Cbo_ToMonth")%>';
	document.frmMain.Cbo_ToYear.value = '<%=request.getAttribute("Cbo_ToYear")%>';

	var month ='<%=request.getAttribute("hMonth")%>';
	var quarter='<%=request.getAttribute("hQuarter")%>';
	var year ='<%=request.getAttribute("hYear")%>';
	var preYear ='<%=request.getAttribute("hPreYear")%>';
	var varWidth=0;
	var type=document.frmMain.strSessOpt.value;

	/*pgobj=document.getElementById("PG");
		pgobj.style.display='none';
	pgHObj=document.getElementById("pgH");
		pgHObj.style.display='none';*/
		
	var cnt=0;	
	
	if(type=='VP')
	{
		 varWidth=900;
	}
	else if (type=='AD')
	{
		 varWidth=800;
		
	}
	else 
	{
		varWidth=900;
	}
	tableObj=document.getElementById("mTable");
	dtableObj=document.getElementById("dtable");

	if (month != 'off' && month != '')
	{
		document.frmMain.Month.checked=true;
		
		varWidth=varWidth+120;
		cnt++
	
		
	}

	if (quarter != 'off' && quarter != '')
	{
		document.frmMain.Quarter.checked=true;
		varWidth=varWidth+120;
		cnt++
	}
	if (year != 'off' && year != '')
	{
		document.frmMain.Year.checked= true;
		varWidth=varWidth+120;
		cnt++
	}
	
	if (preYear == 'on')
	{	
		
		document.frmMain.PreYear.checked= true;
		varWidth=varWidth+300;
		if (type=='AD')
		{
			varWidth=varWidth+200;
		}
		if(type=='VP')
		{
		 varWidth=varWidth+100;
		}
	}
	if((type=='VP'&& cnt<2))
		{
		 	varWidth=varWidth-100;
		}
	tableObj.style.width=varWidth;
	

}

function fnEnter()
{
		if (event.keyCode == 13)
		{ 
			fnGo('<%=strOpt%>');
		}
}
function fnGo(haction)
{
	//fnSetSelections();
	fnStatusChange();
	fnReloadSales(haction,null);
	//document.frmMain.submit();
}
function fnStatusChange()
{
// month value is selected by defult.
	

	box = eval("document.frmMain.Month" ); 
if (box.checked != true){
	document.frmMain.hMonth.value="off";
}
else 
{
	document.frmMain.hMonth.value="on";
}
	box = eval("document.frmMain.Quarter" ); 
if (box.checked != true){
	document.frmMain.hQuarter.value="off";
}
else 
{
	document.frmMain.hQuarter.value="on";
}

box = eval("document.frmMain.Year" ); 

if (box.checked != true){
	document.frmMain.hYear.value="off";
}
else 
{
	document.frmMain.hYear.value="on";
}

boxPreYear = eval("document.frmMain.PreYear" ); 
 
if (boxPreYear.checked != true){
	document.frmMain.hPreYear.value="off";
}
else 
{
	document.frmMain.hPreYear.value="on";
}

/*
else
{
document.frmMain.byMonth.value="off";
}
box = eval("document.frmMain.Quarter" ); 
if (box.checked == true){
document.frmMain.byQuarter.value="Display";
}
box = eval("document.frmMain.Year" ); 
if (box.checked == true){
document.frmMain.byYear.value="Display";
}*/
//document.frmMain.submit();
	
}

</script>
</HEAD>

<BODY leftmargin="20" onkeyup="fnEnter();" topmargin="10" onLoad="javascript:fnLoad()">
<FORM name="frmMain" method="POST" action="<%=strServletPath%>/GmSalesGrowthServlet">
	<table id ="mTable"  style="margin: 0 0 0 0; width: 1400; border: 1px solid  #676767;" cellspacing="0" cellpadding="0">
		<input type="hidden" name="hAction" value="">
		<input type="hidden" name="hId" value="">
		<input type="hidden" name="strSessOpt" value="<%=strOpt%>">
		<!-- The following hidden fields are for Sales Drilldown (Additional Drill down)-->
		<input type="hidden" name="hDistributorID" value="<%=request.getParameter("hDistributorID")%>">
		<input type="hidden" name="hAccountID" value="<%=request.getParameter("hAccountID")%>">
		<input type="hidden" name="hSetNumber" value="<%=request.getParameter("hSetNumber")%>">
		<input type="hidden" name="hRepID" value="<%=request.getParameter("hRepID")%>">
		<input type="hidden" name="hTerritory" value="<%=request.getParameter("hTerritory")%>">
		<input type="hidden" name="hPartNumber" value="">
		<input type="hidden" name="hAreaDirector" value="<%=request.getParameter("hAreaDirector")%>">
		<input type="hidden" name="hRegnID" value="<%=request.getParameter("hRegnID")%>">
		<input type="hidden" name="hVPID" value="<%=request.getParameter("hVPID")%>">
		<input type="hidden" name="hMonth" value="<%=request.getParameter("hMonth")%>">
		<input type="hidden" name="hQuarter" value="<%=request.getParameter("hQuarter")%>">
		<input type="hidden" name="hYear" value="<%=request.getParameter("hYear")%>">
		<input type="hidden" name="hPreYear" value="<%=request.getParameter("hPreYear")%>">
		<input type="hidden" name="hReportType" value="<%=request.getParameter("hReportType")%>">		
		<tr>
			<td  height="25" class="RightDashBoardHeader"> <fmtSalesGrowthReport:message key="LBL_QUOTA_PERFORMANCE"/> <%=strHeader%> <fmtSalesGrowthReport:message key="LBL_IN"/></td>
			<td  height="25" class="RightDashBoardHeader">
			<fmtSalesGrowthReport:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>		
		</tr>
		<tr><td colspan="2" class="Line"></td></tr>
		
		<tr>
			<td colspan="10">
			<jsp:include page="/sales/GmSalesFilters.jsp" >
				<jsp:param name="FRMNAME" value="frmMain" />
				<jsp:param name="HACTION" value="<%=strOpt%>"/>
				<jsp:param name="HIDE" value="SYSTEM" />
				<jsp:param name="HIDEBUTTON" value="HIDEGO" />				
			</jsp:include>
			</td>
		</tr>		
		
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr   align="center"><td>
			<!-- Combo Filter -->
			<table cellSpacing=0 cellPadding=0  border=0>
			<tr  height=30>
				<TD class=RightText align="right" width=100>&nbsp;&nbsp;&nbsp;&nbsp;<fmtSalesGrowthReport:message key="LBL_PERIOD"/>:&nbsp;&nbsp;</TD>
				<TD class=RightText width=105>
				<gmjsp:dropdown controlName="Cbo_ToMonth"  seletedValue="<%=strToMonth %>" value="<%=alMonthDropDown%>" 
	  			codeId = "CODENMALT"  codeName = "CODENM"  tabIndex="1" />
				</TD>
				<TD class=RightText width=75>
				<gmjsp:dropdown controlName="Cbo_ToYear"  seletedValue="<%=strToYear %>" value="<%=alYearDropDown%>" 
				codeId = "CODENM"  codeName = "CODENM"  tabIndex="2" />
				</TD>
						
					<td>
					<table cellSpacing=0 cellPadding=0  border=0 width="100%">
					<INPUT TYPE=CHECKBOX NAME="Month" onclick="fnStatusChange()" /><fmtSalesGrowthReport:message key="LBL_MONTH"/> 
					
					<INPUT TYPE=CHECKBOX NAME="Quarter" /> <fmtSalesGrowthReport:message key="LBL_QUARTER"/>
					<INPUT TYPE=CHECKBOX NAME="Year" /> <fmtSalesGrowthReport:message key="LBL_YEAR"/>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<INPUT TYPE=CHECKBOX NAME="PreYear"  onclick="fnStatusChange()" /> <fmtSalesGrowthReport:message key="LBL_INCLUDE_PREV"/>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<fmtSalesGrowthReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button  controlId="go" value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button" onClick="<%=strBtnClick%>" tabindex="5" buttonType="Load" />
					</table>
					</td>

				
			</TR>
			</TABLE>
		</td></tr>
		<tr><td>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
			<jsp:include page="/sales/GmSalesFilterDetails.jsp" />
		</td></tr>
		<tr height="10"><td></td></tr>
</FORM>

<FORM name ="frmDtag"> 
	<tr><td colspan="2" class="Line"></td></tr>
	<tr>
		<td colspan="2">
			<table border="0" width="100%"  cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top" class="aligncenter" height="30" colspan="15">
					<% if (strOpt.equals("VP")||strOpt.equals("AD")) 
				   			{ %>
						<display:table name="requestScope.alReturn"  id ="dtable" export="true"  cellpadding="0" cellspacing="0" varTotals="totals"
						 decorator="com.globus.sales.displaytag.beans.DTSalesGrowthReportWrapper">
						 <display:setProperty name="export.excel.decorator" value="com.globus.sales.displaytag.beans.DTSalesGrowthReportWrapper" />
                            <display:setProperty name="export.excel.filename" value="Quota Performance Export.xls" />
							<display:column property="ID" title="" headerClass="ShadeDarkGrayTD" media="html" class="ShadeLightGrayTD" 
					 		style="text-align: center;" />

							<% if (strOpt.equals("VP")) 
				   			{ %>
				   			<fmtSalesGrowthReport:message key="DT_NAME" var="varName"/><display:column property="NAME" title="${varName}"  style="align:left;width:500;text-align: left;"  headerClass="ShadeDarkGrayTD"   class="ShadeLightGrayTD" sortable="true" />
				   			<fmtSalesGrowthReport:message key="DT_GEOGR" var="varGeogr"/><display:column property="GNAME" title="${varGeogr}" headerClass="ShadeDarkGrayTD"  class="ShadeLightGrayTD"  sortable="true" style="text-align:left;"/>
				   			
							<% } 
							else {%>
							<fmtSalesGrowthReport:message key="DT_NAME" var="varName"/><display:column property="NAME" title="${varName}"  headerClass="ShadeDarkGrayTD"  class="ShadeLightGrayTD" sortable="true" style="text-align: left;"/>
							<% } if (strOpt.equals("AD")) 
				   			{ %>
				   			<fmtSalesGrowthReport:message key="DT_REGION" var="varRegion"/><display:column property="RGNAME" title="${varRegion}" headerClass="ShadeDarkGrayTD" style="text-align:left;" class="ShadeLightGrayTD" sortable="true" />
							<% }
							if (monthDisplay.equals("off")!=true) 
				   				{
				   			%>							
							<%-- Month Growth Report  --%>
						
							<display:column  property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
							
								<fmtSalesGrowthReport:message key="DT_CMTD" var="varCmtd"/><display:column property="MTD" title="${varCmtd}" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD" sortable="true"
								 	sortProperty="SMTD" format="<%=strCurrFmt%>"   style="font-size: 9px;text-align: right;" total="true" media="<%=strDisplay%>"/>
								<fmtSalesGrowthReport:message key="DT_CMTD" var="varCmtd"/><display:column property="EXLMTD" title="${varCmtd}" style="font-size: 9px;text-align: right;" media="excel"/>
							<% 
							if (PreYearDisplay.equals("on")) 
				   				{
				   			%>	
						
								<fmtSalesGrowthReport:message key="DT_PMTD" var="varPmtd"/><display:column property="PMTD"   title="${varPmtd}" headerClass="ShadeDarkGreenTD"  total="true" class="ShadeLightGreenTD"
									sortable="true" sortProperty="SPMTD" format="<%=strCurrFmt%>"  style="text-align: right;" media="<%=strDisplay%>"/>
								<fmtSalesGrowthReport:message key="DT_PMTD" var="varPmtd"/><display:column property="EXLPMTD" title="${varPmtd}" style="font-size: 9px;text-align: right;"  media="excel"/> 
							<% }%>		
							<fmtSalesGrowthReport:message key="DT_QUO_AMT" var="varQuoAmt"/><display:column property="QMTD"  title="${varQuoAmt}" headerClass="ShadeDarkGreenTD" total="true"  media="<%=strDisplay%>"
								class="ShadeMedGreenTD" sortProperty="SQMTD" format="<%=strCurrFmt%>"  sortable="true" style="text-align: right;" />
							<fmtSalesGrowthReport:message key="DT_QUO_AMT" var="varQuoAmt"/><display:column property="EXLQMTD" title="${varQuoAmt}" style="font-size: 9px;text-align: right;" media="excel"/>
							<fmtSalesGrowthReport:message key="DT_QUOTA" var="varQuota"/><display:column property="MQPER" title="${varQuota}" headerClass="ShadeDarkGreenTD"  media="<%=strDisplay%>"
								class="ShadeLightGreenTD" sortProperty="SMQPER" sortable="true" format="{0,number,##0.00%}" style="text-align: right;" />
							<fmtSalesGrowthReport:message key="DT_QUOTA" var="varQuota"/><display:column property="EXLMQPER" title="${varQuota}" style="font-size: 9px;text-align: right;" media="excel"/>	
							<% 
							if (PreYearDisplay.equals("on")) 
				   				{
				   			%>	
							<fmtSalesGrowthReport:message key="DT_GROWTH_AMT" var="varGrowthAmt"/><display:column property="MTDGTHDLR" title="${varGrowthAmt}" headerClass="ShadeDarkGreenTD" total="true" class="ShadeMedGreenTD"
								sortProperty="SMTDGTHDLR" format="<%=strCurrFmt%>" sortable="true" style="text-align: right;" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_GROWTH_AMT" var="varGrowthAmt"/><display:column property="EXLMTDGTHDLR" title="${varGrowthAmt}" style="font-size: 9px;text-align: right;" media="excel"/>
							<fmtSalesGrowthReport:message key="DT_GROWTH" var="varGrowth"/><display:column property="MTDGTHPER" title="${varGrowth}" headerClass="ShadeDarkGreenTD" class="ShadeLightGreenTD"
								sortProperty="SMTDGTHPER" sortable="true" format="{0,number,##0.00%}" style="text-align: right;" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_GROWTH" var="varGrowth"/><display:column property="EXLMTDGTHPER" title="${varGrowth}" style="font-size: 9px;text-align: right;" media="excel"/>
							<% }%>	
							<fmtSalesGrowthReport:message key="DT_PROJECTED" var="varProjected"/><display:column property="EMTD" title="${varProjected}" headerClass="ShadeDarkGreenTD" total="true" class="ShadeMedGreenTD"
								sortProperty="SEMTD" sortable="true" format="<%=strCurrFmt%>" style="text-align: right;" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_PROJECTED" var="varProjected"/><display:column  property="EXLEMTD" title="${varProjected}" style="font-size: 9px;text-align: right;" media="excel"/>					
							<% 	
				   				}
							
								if (quarterDisplay.equals("off")!=true) 
			   					{
			   					
							%>
							<%-- Quarter Growth Report  --%>
				
							<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
							<fmtSalesGrowthReport:message key="DT_CQTD" var="varCqtd"/><display:column property="QTD" title="${varCqtd}"  headerClass="ShadeDarkBlueTD" class="ShadeMedBlueTD"  sortProperty="SQTD"
								sortable="true" format="<%=strCurrFmt%>" style="text-align: right;" total="true" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_CQTD" var="varCqtd"/><display:column property="EXLQTD" title="${varCqtd}" style="font-size: 9px;text-align: right;" media="excel"/>	
							<% 
								if (PreYearDisplay.equals("on")) 
				   					{
				   			%>	
							<fmtSalesGrowthReport:message key="DT_PQTD" var="varPqtd"/><display:column property="PQTD" title="${varPqtd}"  headerClass="ShadeDarkBlueTD" class="ShadeLightBlueTD" sortProperty="SPQTD"
								sortable="true" format="<%=strCurrFmt%>" style="text-align: right;" total="true" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_PQTD" var="varPqtd"/><display:column property="EXLPQTD" title="${varPqtd}" style="font-size: 9px;text-align: right;" media="excel"/>
								<% }%>	
							<fmtSalesGrowthReport:message key="DT_QUO_AMT" var="varQuoAmt"/><display:column property="QQTD" title="${varQuoAmt}" headerClass="ShadeDarkBlueTD" total="true"  media="<%=strDisplay%>"
								class="ShadeMedBlueTD" sortProperty="SQQTD" sortable="true" format="<%=strCurrFmt%>" style="text-align: right;" />
							<fmtSalesGrowthReport:message key="DT_QUO_AMT" var="varQuoAmt"/><display:column property="EXLQQTD" title="${varQuoAmt}" style="font-size: 9px;text-align: right;" media="excel"/>
							<fmtSalesGrowthReport:message key="DT_QUOTA" var="varQuota"/><display:column property="QQPER" title="${varQuota}" headerClass="ShadeDarkBlueTD" media="<%=strDisplay%>"
								class="ShadeLightBlueTD" sortProperty="SQQPER" sortable="true" format="{0,number,##0.00%}" style="text-align: right;" />
							<fmtSalesGrowthReport:message key="DT_QUOTA" var="varQuota"/><display:column property="EXLQQPER" title="${varQuota}" style="font-size: 9px;text-align: right;" media="excel"/>
										<% 
							if (PreYearDisplay.equals("on")) 
				   				{
				   			%>
							<fmtSalesGrowthReport:message key="DT_GROWTH_AMT" var="varGrowthAmt"/><display:column property="QTDGTHDLR" title="${varGrowthAmt}" headerClass="ShadeDarkBlueTD" class="ShadeMedBlueTD" sortable="true"
								sortProperty="SQTDGTHDLR" style="text-align: right;" format="<%=strCurrFmt%>" total="true" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_GROWTH_AMT" var="varGrowthAmt"/><display:column property="EXLQTDGTHDLR" title="${varGrowthAmt}" style="font-size: 9px;text-align: right;" media="excel"/>
							<fmtSalesGrowthReport:message key="DT_GROWTH" var="varGrowth"/><display:column property="QTDGTHPER" title="${varGrowth}"  headerClass="ShadeDarkBlueTD" class="ShadeLightBlueTD"
								sortProperty="SQTDGTHPER" sortable="true"  format="{0,number,##0.00%}" style="text-align: right;" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_GROWTH" var="varGrowth"/><display:column property="EXLQTDGTHPER" title="${varGrowth}" style="font-size: 9px;text-align: right;" media="excel"/>
								
							<% }%>
							<fmtSalesGrowthReport:message key="DT_PROJECTED" var="varProjected"/><display:column property="EQTD" title="${varProjected}" headerClass="ShadeDarkBlueTD" total="true" class="ShadeMedBlueTD"
								sortProperty="SEQTD" sortable="true" format="<%=strCurrFmt%>" style="text-align: right;" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_PROJECTED" var="varProjected"/><display:column property="EXLEQTD" title="${varProjected}" style="font-size: 9px;text-align: right;" media="excel"/>	
								<% 	
				   				}
								if (yearDisplay.equals("off")!=true) 
			   					{
								%>
							<%--  Year Growth Report --%>
							<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
							<fmtSalesGrowthReport:message key="DT_CYTD" var="varCytd"/><display:column property="YTD" title="${varCytd}" headerClass="ShadeDarkOrangeTD"  class="ShadeMedOrangeTD" sortable="true" 
								sortProperty="SYTD" format="<%=strCurrFmt%>" style="text-align: right;" total="true" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_CYTD" var="varCytd"/><display:column property="EXLYTD" title="${varCytd}" style="font-size: 9px;text-align: right;" media="excel"/>
											<% 
							if (PreYearDisplay.equals("on")) 
				   				{
				   			%>
							<fmtSalesGrowthReport:message key="DT_PYTD" var="varPytd"/><display:column property="PYTD" title="${varPytd}" headerClass="ShadeDarkOrangeTD"  class="ShadeLightOrangeTD" sortable="true" 
								sortProperty="SPYTD" format="<%=strCurrFmt%>" style="text-align: right;" total="true" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_PYTD" var="varvarPytd"/><display:column property="EXLPYTD" title="${varPytd}" style="font-size: 9px;text-align: right;" media="excel"/>
								<% }%>
							<fmtSalesGrowthReport:message key="DT_QUO_AMT" var="varQuoAmt"/><display:column property="QYTD" title="${varQuoAmt}" headerClass="ShadeDarkOrangeTD" total="true"  media="<%=strDisplay%>"
								class="ShadeMedOrangeTD" sortProperty="SQYTD" sortable="true" format="<%=strCurrFmt%>" style="text-align: right;" />
							<fmtSalesGrowthReport:message key="DT_QUO_AMT" var="varQuoAmt"/><display:column property="EXLQYTD" title="${varQuoAmt}" style="font-size: 9px;text-align: right;" media="excel"/>
							<fmtSalesGrowthReport:message key="DT_QUOTA" var="varQuota"/><display:column property="YQPER" title="${varQuota}" headerClass="ShadeDarkOrangeTD" media="<%=strDisplay%>"
								class="ShadeLightOrangeTD" sortProperty="SYQPER" sortable="true" format="{0,number,##0.00%}" style="text-align: right;" />
							<fmtSalesGrowthReport:message key="DT_QUOTA" var="varQuota"/><display:column property="EXLYQPER" title="${varQuota}" style="font-size: 9px;text-align: right;" media="excel"/>
													<% 
							if (PreYearDisplay.equals("on")) 
				   				{
				   			%>
							<fmtSalesGrowthReport:message key="DT_GROWTH_AMT" var="varGrowthAmt"/><display:column property="YTDGTHDLR" title="${varGrowthAmt}" headerClass="ShadeDarkOrangeTD" class="ShadeMedOrangeTD" media="<%=strDisplay%>"
								sortProperty="SYTDGTHDLR" sortable="true" format="<%=strCurrFmt%>" style="text-align: right;" total="true"/>
							<fmtSalesGrowthReport:message key="DT_" var="var"/><display:column property="EXLYTDGTHDLR" title="${varGrowthAmt}" style="font-size: 9px;text-align: right;" media="excel"/>
							<fmtSalesGrowthReport:message key="DT_GROWTH" var="varGrowth"/><display:column property="YTDGTHPER" title="${varGrowth}" headerClass="ShadeDarkOrangeTD" class="ShadeLightOrangeTD"
								sortProperty="SYTDGTHPER" format="{0,number,##0.00%}" sortable="true" style="text-align: right;" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_" var="var"/><display:column property="EXLYTDGTHPER" title="${varGrowth}" style="font-size: 9px;text-align: right;" media="excel"/>
									<% 	
				   				}
								%>
							<fmtSalesGrowthReport:message key="DT_PROJECTED" var="varProjected"/><display:column property="EYTD" title="${varProjected}" headerClass="ShadeDarkOrangeTD" total="true" class="ShadeMedOrangeTD"
								sortProperty="SEYTD" sortable="true"  format="<%=strCurrFmt%>" style="text-align: right;" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_PROJECTED" var="varProjected"/><display:column property="EXLEYTD" title="${varProjected}" style="font-size: 9px;text-align: right;" media="excel"/>	
								<% 	
				   				}
								%>
 							<%--<display:column property="ICON"  title="" headerClass="ShadeDarkGrayTD" class="ShadeMedGrayTD" media="html"/> --%>
				<display:footer media="html"> 
				<%
				 HashMap hmColumns = (HashMap)pageContext.getAttribute("totals");
				 strColumn4 =  hmColumns.get("column4") != null ? String.valueOf(hmColumns.get("column4")) : "0";
				 strColumn5 =  hmColumns.get("column5") != null ? String.valueOf( hmColumns.get("column5")) : "0";
				 strColumn6 =  hmColumns.get("column6") != null ? String.valueOf( hmColumns.get("column6")) : "0";
				 strColumn7 =  hmColumns.get("column7") != null ? String.valueOf( hmColumns.get("column7")) : "0";
				 strColumn8 =  hmColumns.get("column8") != null ? String.valueOf( hmColumns.get("column8")) : "0";
				 strColumn9 =  hmColumns.get("column9") != null ? String.valueOf( hmColumns.get("column9")) : "0";
				 strColumn10 =  hmColumns.get("column10") != null ? String.valueOf( hmColumns.get("column10")) : "0";
				 strColumn11 =  hmColumns.get("column11") != null ? String.valueOf( hmColumns.get("column11")) : "0";
				 strColumn12 =  hmColumns.get("column12") != null ? String.valueOf( hmColumns.get("column12")) : "0";
				 strColumn13 =  hmColumns.get("column13") != null ? String.valueOf( hmColumns.get("column13")) : "0";
				 strColumn14 =  hmColumns.get("column14") != null ? String.valueOf( hmColumns.get("column14")) : "0";
				 strColumn15 =  hmColumns.get("column15") != null ? String.valueOf( hmColumns.get("column15")) : "0";
				 strColumn16 =  hmColumns.get("column16") != null ? String.valueOf( hmColumns.get("column16")) : "0";
				 strColumn17 =  hmColumns.get("column17") != null ? String.valueOf( hmColumns.get("column17")) : "0";
				 strColumn18 =  hmColumns.get("column18") != null ? String.valueOf( hmColumns.get("column18")) : "0";
				 strColumn19 =  hmColumns.get("column19") != null ? String.valueOf( hmColumns.get("column19")) : "0";
				 strColumn20 =  hmColumns.get("column20") != null ? String.valueOf( hmColumns.get("column20")) : "0";
				 strColumn21 =  hmColumns.get("column21") != null ? String.valueOf( hmColumns.get("column21")) : "0";
				 strColumn22 =  hmColumns.get("column22") != null ? String.valueOf( hmColumns.get("column22")) : "0";
				 strColumn23 =  hmColumns.get("column23") != null ? String.valueOf( hmColumns.get("column23")) : "0";
				 strColumn24 =  hmColumns.get("column24") != null ? String.valueOf( hmColumns.get("column24")) : "0";
				 strColumn25 =  hmColumns.get("column25") != null ? String.valueOf( hmColumns.get("column25")) : "0";
				 strColumn26 =  hmColumns.get("column26") != null ? String.valueOf( hmColumns.get("column26")) : "0";
				 strColumn27 =  hmColumns.get("column27") != null ? String.valueOf( hmColumns.get("column27")) : "0";
				 %>

				<%	if (strOpt.equals("AD")||strOpt.equals("VP"))
					{ %>
					<tr><td class="Line" colspan="27"></td></tr>
					<tr><td class = "ShadeDarkGrayTD" colspan="2"> <B><fmtSalesGrowthReport:message key="LBL_TOTAL"/></B></td>
					<td class = "ShadeDarkGrayTD" style="text-align: right;" ></td>
					<%	}
					else
						{ %>
					<tr><td class="Line" colspan="26"></td></tr>
					<tr><td class = "ShadeDarkGrayTD" colspan="2"> <B><fmtSalesGrowthReport:message key="LBL_TOTAL"/></B></td>
					<% }
				
				if (PreYearDisplay.equals("on")) 
	   			{
							if ( strOpt.equals("AD")||strOpt.equals("VP"))
							{
							strVal 		= strColumn5;
							strPVal 	= strColumn6;
							strQuota	= strColumn7;
							strGrowth = strColumn9;
							strExpQuota = strColumn11;
							}
							else
							{
							strVal 		= strColumn4;
							strPVal 	= strColumn5;
							strQuota	= strColumn6;
							strGrowth = strColumn8;
							strExpQuota = strColumn10;
							}	
	   			}
				

						if (PreYearDisplay.equals("on")!=true) 
			   			{
								if (strOpt.equals("TERR") || strOpt.equals("AD")||strOpt.equals("VP"))
								{
								strVal 		= strColumn5;								
								strQuota	= strColumn6;
								strExpQuota = strColumn8;								
								}
								else
								{
								strVal 		= strColumn4;								
								strQuota	= strColumn5;								
								strExpQuota = strColumn7;
								}	
			   			}

						if (PreYearDisplay.equals("on")) 
			   			{
							dbTemp = ((Double.parseDouble(strVal)- Double.parseDouble(strPVal))  /Double.parseDouble(strPVal) ) ;
			   			}
						if(Double.isNaN(dbTemp)||Double.isInfinite(dbTemp))
						{
							dbTemp=0;
						}
						dbTempQ = (Double.parseDouble(strVal)/Double.parseDouble(strQuota)) ;
						if(Double.isNaN(dbTempQ)||Double.isInfinite(dbTempQ))
						{
							dbTempQ=0;
						}
						strVal 		= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strVal));
						strVal		= (strVal.indexOf("(")!=-1)?strForeColor + strVal:strVal; // In Total row to give red color for $ symbol as well as negative value.
						
						strPVal 	= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strPVal));
						strPVal		= (strPVal.indexOf("(")!=-1)?strForeColor + strPVal:strPVal;
						
						strGrowth 	= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strGrowth));
						strGrowth 	= (strGrowth.indexOf("(")!=-1)?strForeColor + strGrowth:strGrowth;
						
						strQuota 	= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strQuota));
						strQuota	= (strQuota.indexOf("(")!=-1)?strForeColor + strQuota:strQuota;
						
						strExpQuota = strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strExpQuota));
						strExpQuota = (strExpQuota.indexOf("(")!=-1)?strForeColor + strExpQuota:strExpQuota;
						
						strPercent 	= GmCommonClass.getRedTextSmall(df.format(dbTemp));
						strPercentQuota = GmCommonClass.getRedTextSmall(df.format(dbTempQ));
						//strPercent = "";
		   				//if (monthDisplay.equals("Display")) 
		   				//{
					%>
					<td class="Line"></td>
					<td class = "<%=strFooterClass1%>" style="text-align: right;" ><B><%=strVal%></B></td>
					<% 
						if (PreYearDisplay.equals("on")) 
				   		{
				   	%>	
							<td class = "<%=strFooterClass1%>" style="text-align: right;" ><B><%=strPVal%></B></td>
					<% 	
				   				}
					%>
					<% if (strDisplay.equals("html csv xml pdf")) { %>
					<td class = "<%=strFooterClass1%>" style="text-align: right;" ><B><%=strQuota%></B></td>
					<td class = "<%=strFooterClass1%>" style="text-align: right;" ><B><%=strPercentQuota%></B></td>
					<% } %>
					<% 
						if (PreYearDisplay.equals("on")) 
				   		{
				   	%>
					<td class = "<%=strFooterClass1%>" style="text-align: right;" ><B><%=strGrowth%></B></td>
					<td class = "<%=strFooterClass1%>" style="text-align: right;" ><B><%=strPercent%></B></td>
					<% 	
				   				}
					%>
					
					<% if (strDisplay.equals("html csv xml pdf")) { %>
					<td class = "<%=strFooterClass1%>" style="text-align: right;" ><B><%=strExpQuota%></B></td>
					<% } %>

					<%
					strVal = "0";
					strPVal = "0";
					strQuota = "0";
					strGrowth = "0";
					strExpQuota = "0";
					strPercent = "0";
					strPercentQuota = "0";
					
					if ((monthDisplay.equals("off")!=true && quarterDisplay.equals("off")!=true)||(monthDisplay.equals("off")!=true&&yearDisplay.equals("off")!=true)
						||(quarterDisplay.equals("off")!=true&& yearDisplay.equals("off")!=true)) 
   					{
						if (PreYearDisplay.equals("on")) 
			   			{
   			
							if ( strOpt.equals("AD")||strOpt.equals("VP"))
							{
								strVal 		= strColumn13;
								strPVal 	= strColumn14;
								strQuota	= strColumn15;
							
								strGrowth 	= strColumn17;
								strExpQuota = strColumn19;
							}
							else
							{
								strVal 		= strColumn12;
								strPVal 	= strColumn13;										
								strQuota 	= strColumn14;
							
								strGrowth	= strColumn16;
								strExpQuota = strColumn18;
							}	
			   			}
						
						if (PreYearDisplay.equals("on")!=true) 
			   			{
								if (strOpt.equals("AD")||strOpt.equals("VP"))
								{
								strVal 		= strColumn10;
								strQuota	= strColumn11;
								strExpQuota = strColumn13;
								}
								else
								{
									strVal 		= strColumn9;									
									strQuota 	= strColumn10;
									strExpQuota = strColumn12;
								}	
			   			}
						
						if (PreYearDisplay.equals("on")) 
			   			{
							dbTemp = ( ( Double.parseDouble(strVal)- Double.parseDouble(strPVal))  /Double.parseDouble(strPVal) ) ;
			   			}
						if(Double.isNaN(dbTemp)||Double.isInfinite(dbTemp))
						{
							dbTemp=0;
						}
						dbTempQ = (Double.parseDouble(strVal)/Double.parseDouble(strQuota)) ;
						if(Double.isNaN(dbTempQ)||Double.isInfinite(dbTempQ))
						{
							dbTempQ=0;
						}
						
						strVal 		= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strVal));
						strVal		= (strVal.indexOf("(")!=-1)?strForeColor + strVal:strVal; // In Total row to give red color for $ symbol as well as negative value.
						
						strPVal 	= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strPVal));
						strPVal		= (strPVal.indexOf("(")!=-1)?strForeColor + strPVal:strPVal;
						
						strGrowth 	= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strGrowth));
						strGrowth 	= (strGrowth.indexOf("(")!=-1)?strForeColor + strGrowth:strGrowth;
						
						strQuota 	= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strQuota));
						strQuota	= (strQuota.indexOf("(")!=-1)?strForeColor + strQuota:strQuota;
						
						strExpQuota = strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strExpQuota));
						strExpQuota = (strExpQuota.indexOf("(")!=-1)?strForeColor + strExpQuota:strExpQuota;
						
						strPercent 	= GmCommonClass.getRedTextSmall(df.format(dbTemp));
						strPercentQuota = GmCommonClass.getRedTextSmall(df.format(dbTempQ));

					%>
					<td class="Line"></td>
					<td class = "<%=strFooterClass2%>" style="text-align: right;" ><B><%=strVal%></B></td>
					<% 
						if (PreYearDisplay.equals("on")) 
				   		{
				   	%>	
							<td class = "<%=strFooterClass2%>" style="text-align: right;" ><B><%=strPVal%></B></td>
					<% 	
				   				}
					%>
					<% if (strDisplay.equals("html csv xml pdf")) { %>
					<td class = "<%=strFooterClass2%>" style="text-align: right;" ><B><%=strQuota%></B></td>
					<td class = "<%=strFooterClass2%>" style="text-align: right;" ><B><%=strPercentQuota%></B></td>
					<% } %>
					<% 
						if (PreYearDisplay.equals("on")) 
				   		{
				   	%>
					<td class = "<%=strFooterClass2%>" style="text-align: right;" ><B><%=strGrowth%></B></td>
					<td class = "<%=strFooterClass2%>" style="text-align: right;" ><B><%=strPercent%></B></td>
					<% 	
				   				}
					%>
					<% if (strDisplay.equals("html csv xml pdf")) { %>
					<td class = "<%=strFooterClass2%>" style="text-align: right;" ><B><%=strExpQuota%></B></td>
					<% } %>
					<%
					}
					strVal = "0";
					strPVal = "0";
					strQuota = "0";
					strGrowth = "0";
					strExpQuota = "0";
					strPercent = "0";
					strPercentQuota = "0";
					
					if (monthDisplay.equals("off")!=true && quarterDisplay.equals("off")!=true && yearDisplay.equals("off")!=true) 
	   				{
						
						if (PreYearDisplay.equals("on")) 
			   			{
							if (strOpt.equals("TERR") || strOpt.equals("AD")||strOpt.equals("VP"))
							{
								strVal 		= strColumn21;
								strPVal 	= strColumn22;							
								strQuota  	= strColumn23;
							
								strGrowth = strColumn25;
								strExpQuota = strColumn27;
							}
							else
							{
								strVal 		= strColumn20;
								strPVal 	= strColumn21;
								strQuota 	= strColumn22;
							
								strGrowth	= strColumn24;
								strExpQuota = strColumn26;
							}	
			   			}

						if (PreYearDisplay.equals("on")!=true) 
			   			{
								if (strOpt.equals("TERR") || strOpt.equals("AD")||strOpt.equals("VP"))
								{
								strVal 		= strColumn15;								
								strQuota	= strColumn16;
								strExpQuota = strColumn18;
								}
								else
								{
									strVal 		= strColumn14;									
									strQuota 	= strColumn15;								
									strExpQuota = strColumn17;
								}	
			   			}
						if (PreYearDisplay.equals("on")) 
			   			{
							dbTemp = ( ( Double.parseDouble(strVal)- Double.parseDouble(strPVal))  /Double.parseDouble(strPVal) ) ;
			   			}
						if(Double.isNaN(dbTemp)||Double.isInfinite(dbTemp))
						{
							dbTemp=0;
						}
						dbTempQ = (Double.parseDouble(strVal)/Double.parseDouble(strQuota)) ;
						if(Double.isNaN(dbTempQ)||Double.isInfinite(dbTempQ))
						{
							dbTempQ=0;
						}
						strVal 		= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strVal));
						strVal		= (strVal.indexOf("(")!=-1)?strForeColor + strVal:strVal; // In Total row to give red color for $ symbol as well as negative value.
						
						strPVal 	= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strPVal));
						strPVal		= (strPVal.indexOf("(")!=-1)?strForeColor + strPVal:strPVal;
						
						strGrowth 	= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strGrowth));
						strGrowth 	= (strGrowth.indexOf("(")!=-1)?strForeColor + strGrowth:strGrowth;
						
						strQuota 	= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strQuota));
						strQuota	= (strQuota.indexOf("(")!=-1)?strForeColor + strQuota:strQuota;
						
						strExpQuota = strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strExpQuota));
						strExpQuota = (strExpQuota.indexOf("(")!=-1)?strForeColor + strExpQuota:strExpQuota;
						
						strPercent = GmCommonClass.getRedTextSmall(df.format(dbTemp));
						strPercentQuota = GmCommonClass.getRedTextSmall(df.format(dbTempQ));
		   				
					%>
					<td class="Line"></td>
					<td class = "ShadeDarkOrangeTD" style="text-align: right;" ><B><%=strVal%></B></td>
					<% 
						if (PreYearDisplay.equals("on")) 
				   		{
				   	%>	
							<td class = "ShadeDarkOrangeTD" style="text-align: right;" ><B><%=strPVal%></B></td>
					<% 	
				   				}
					%>
					<% if (strDisplay.equals("html csv xml pdf")) { %>
					<td class = "ShadeDarkOrangeTD" style="text-align: right;" ><B><%=strQuota%></B></td>
					<td class = "ShadeDarkOrangeTD" style="text-align: right;" ><B><%=strPercentQuota%></B></td>
					<% } %>
					<% 
						if (PreYearDisplay.equals("on")) 
				   		{
				   	%>
					<td class = "ShadeDarkOrangeTD" style="text-align: right;" ><B><%=strGrowth%></B></td>
					<td class = "ShadeDarkOrangeTD" style="text-align: right;" ><B><%=strPercent%></B></td>
					<% 	
				   				}
					%>
					<% if (strDisplay.equals("html csv xml pdf")) { %>
					<td class = "ShadeDarkOrangeTD" style="text-align: right;" ><B><%=strExpQuota%></B></td>	
					<%} %>
					<%} %>
				</tr>
				</display:footer>
				
				</display:table>
				<% } 
				else{%>
				<display:table name="alReturn"  id ="dtable" export="true"   cellpadding="0" cellspacing="0" varTotals="totals"
					decorator="com.globus.sales.displaytag.beans.DTSalesGrowthReportWrapper">	
					<display:setProperty name="export.excel.decorator" value="com.globus.sales.displaytag.beans.DTSalesGrowthReportWrapper" />
                     <display:setProperty name="export.excel.filename" value="Quota Performance Export.xls" />
							<display:column  property="ID" title="" headerClass="ShadeDarkGrayTD" media="html" class="ShadeLightGrayTD" 
					 		style="text-align: center;" />
							<% if (!strOpt.equals("TERR")) {%>
							<fmtSalesGrowthReport:message key="DT_NAME" var="varName"/><display:column property="NAME" title="${varName}"  headerClass="ShadeDarkGrayTD"  class="ShadeLightGrayTD" sortable="true" style="text-align: left;"/>
							<% } %>
							<% if (strOpt.equals("TERR")) 
				   			{ %>
				   				<fmtSalesGrowthReport:message key="DT_REP_NAME" var="varRepName"/><display:column property="RNAME" title="${varRepName}" headerClass="ShadeDarkGrayTD"  class="ShadeLightGrayTD" sortable="true" />
				   				<fmtSalesGrowthReport:message key="DT_NAME" var="varName"/><display:column property="NAME" title="${varName}"  headerClass="ShadeDarkGrayTD"  class="ShadeLightGrayTD" sortable="true" style="text-align: left;"/>
							<% } %>
							<%  if (strOpt.equals("AD")) 
				   			{ %>
				   				<fmtSalesGrowthReport:message key="DT_REGION" var="varRegion"/><display:column property="RGNAME" title="${varRegion}" headerClass="ShadeDarkGrayTD" style="text-align:left;" class="ShadeLightGrayTD" sortable="true" />
							<% } 
							if (monthDisplay.equals("off")!=true) 
				   				{
				   			%>							
							<%-- Month Growth Report  --%>
						
							<display:column  property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
							
								<fmtSalesGrowthReport:message key="DT_CMTD" var="varCmtd"/><display:column property="MTD" title="${varCmtd}" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD" sortable="true"	sortProperty="SMTD" format="<%=strCurrFmt%>" style="font-size: 9px;text-align: right;" total="true" media="<%=strDisplay%>"/>
								<fmtSalesGrowthReport:message key="DT_CMTD" var="varCmtd"/><display:column property="EXLMTD" title="${varCmtd}" style="font-size: 9px;text-align: right;" media="excel"/>
							<% 
							if (PreYearDisplay.equals("on")) 
				   				{
				   			%>	
						
								<fmtSalesGrowthReport:message key="DT_PMTD" var="varPmtd"/><display:column property="PMTD" title="${varPmtd}" headerClass="ShadeDarkGreenTD" total="true" class="ShadeLightGreenTD"
									sortable="true" sortProperty="SPMTD" format="<%=strCurrFmt%>"  style="text-align: right;" media="<%=strDisplay%>"/>
								 <fmtSalesGrowthReport:message key="DT_PMTD" var="varPmtd"/><display:column property="EXLPMTD" title="${varPmtd}" style="font-size: 9px;text-align: right;" media="excel"/> 
							<% }%>		
							<fmtSalesGrowthReport:message key="DT_QUO_AMT" var="varQuoAmt"/><display:column property="QMTD" title="${varQuoAmt}" headerClass="ShadeDarkGreenTD" total="true" media="<%=strDisplay%>"
								class="ShadeMedGreenTD" sortProperty="SQMTD" sortable="true" format="<%=strCurrFmt%>" style="text-align: right;" />
							<fmtSalesGrowthReport:message key="DT_QUO_AMT" var="varQuoAmt"/><display:column property="EXLQMTD" title="${varQuoAmt}" style="font-size: 9px;text-align: right;" media="excel"/>
							<fmtSalesGrowthReport:message key="DT_QUOTA" var="varQuota"/><display:column property="MQPER" title="${varQuota}" headerClass="ShadeDarkGreenTD" media="<%=strDisplay%>" 
								class="ShadeLightGreenTD" sortProperty="SMQPER" sortable="true" format="{0,number,##0.00%}" style="text-align: right;" />
							<fmtSalesGrowthReport:message key="DT_QUOTA" var="varQuota"/><display:column property="EXLMQPER" title="${varQuota}" style="font-size: 9px;text-align: right;" media="excel"/>	
							<% 
							if (PreYearDisplay.equals("on")) 
				   				{
				   			%>	
							<fmtSalesGrowthReport:message key="DT_GROWTH_AMT" var="varGrowthAmt"/><display:column property="MTDGTHDLR" title="${varGrowthAmt}" headerClass="ShadeDarkGreenTD" total="true" class="ShadeMedGreenTD"
								sortProperty="SMTDGTHDLR" sortable="true" format="<%=strCurrFmt%>" style="text-align: right;" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_GROWTH_AMT" var="varGrowthAmt"/><display:column property="EXLMTDGTHDLR" title="${varGrowthAmt}" style="font-size: 9px;text-align: right;" media="excel"/>
							<fmtSalesGrowthReport:message key="DT_GROWTH" var="varGrowth"/><display:column property="MTDGTHPER" title="Growth(%)" headerClass="ShadeDarkGreenTD" class="ShadeLightGreenTD"
								 sortProperty="SMTDGTHPER" sortable="true" format="{0,number,##0.00%}" style="text-align: right;" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_GROWTH" var="varGrowth"/><display:column property="EXLMTDGTHPER" title="${varGrowth}" style="font-size: 9px;text-align: right;" media="excel"/>
							<% }%>	
							<fmtSalesGrowthReport:message key="DT_PROJECTED" var="varProjected"/><display:column property="EMTD" title="${varProjected}" headerClass="ShadeDarkGreenTD" total="true" class="ShadeMedGreenTD"
								sortProperty="SEMTD" sortable="true" format="<%=strCurrFmt%>" style="text-align: right;" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_PROJECTED" var="varProjected"/><display:column property="EXLEMTD" title="${varProjected}" style="font-size: 9px;text-align: right;" media="excel"/>					
							<% 	
				   				}
							
								if (quarterDisplay.equals("off")!=true) 
			   					{
			   					
							%>
							<%-- Quarter Growth Report  --%>
				
							<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
							<fmtSalesGrowthReport:message key="DT_CQTD" var="varCqtd"/><display:column property="QTD" title="${varCqtd}"  headerClass="ShadeDarkBlueTD" class="ShadeMedBlueTD"  sortProperty="SQTD"
								sortable="true" format="<%=strCurrFmt%>" style="text-align: right;" total="true" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_CQTD" var="varCqtd"/><display:column property="EXLQTD" title="${varCqtd}" style="font-size: 9px;text-align: right;" media="excel"/>
							<% 
								if (PreYearDisplay.equals("on")) 
				   					{
				   			%>	
							<fmtSalesGrowthReport:message key="DT_PQTD" var="varPqtd"/><display:column property="PQTD" title="${varPqtd}"  headerClass="ShadeDarkBlueTD" class="ShadeLightBlueTD" sortProperty="SPQTD"
								sortable="true" format="<%=strCurrFmt%>" style="text-align: right;" total="true" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_PQTD" var="varPqtd"/><display:column property="EXLPQTD" title="${varPqtd}" style="font-size: 9px;text-align: right;" media="excel"/>
								<% }%>	
							<fmtSalesGrowthReport:message key="DT_QUO_AMT" var="varQuoAmt"/><display:column property="QQTD" title="${varQuoAmt}" headerClass="ShadeDarkBlueTD" total="true" media="<%=strDisplay%>"
								class="ShadeMedBlueTD" sortProperty="SQQTD" sortable="true" format="<%=strCurrFmt%>" style="text-align: right;" />
							<fmtSalesGrowthReport:message key="DT_QUO_AMT" var="varQuoAmt"/><display:column property="EXLQQTD" title="${varQuoAmt}" style="font-size: 9px;text-align: right;" media="excel"/>
							<fmtSalesGrowthReport:message key="DT_QUOTA" var="varQuota"/><display:column property="QQPER" title="${varQuota}" headerClass="ShadeDarkBlueTD" media="<%=strDisplay%>"
								class="ShadeLightBlueTD" sortProperty="SQQPER" sortable="true" format="{0,number,##0.00%}" style="text-align: right;" />
							<fmtSalesGrowthReport:message key="DT_QUOTA" var="varQuota"/><display:column property="EXLQQPER" title="${varQuota}" style="font-size: 9px;text-align: right;" media="excel"/>
										<% 
							if (PreYearDisplay.equals("on")) 
				   				{
				   			%>
							<fmtSalesGrowthReport:message key="DT_GROWTH_AMT" var="varGrowthAmt"/><display:column property="QTDGTHDLR" title="${varGrowthAmt}" headerClass="ShadeDarkBlueTD" class="ShadeMedBlueTD" sortable="true"
								sortProperty="SQTDGTHDLR" style="text-align: right;" format="<%=strCurrFmt%>" total="true" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_GROWTH_AMT" var="varGrowthAmt"/><display:column property="EXLQTDGTHDLR" title="${varGrowthAmt}" style="font-size: 9px;text-align: right;" media="excel"/>
							<fmtSalesGrowthReport:message key="DT_GROWTH" var="varGrowth"/><display:column property="QTDGTHPER" title="${varGrowth}" headerClass="ShadeDarkBlueTD" class="ShadeLightBlueTD"
								sortProperty="SQTDGTHPER" sortable="true" format="{0,number,##0.00%}"  style="text-align: right;" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_GROWTH" var="varGrowth"/><display:column property="EXLQTDGTHPER" title="${varGrowth}" style="font-size: 9px;text-align: right;" media="excel"/>	
							<% }%>
							<fmtSalesGrowthReport:message key="DT_PROJECTED" var="varProjected"/><display:column property="EQTD" title="${varProjected}" headerClass="ShadeDarkBlueTD" total="true" class="ShadeMedBlueTD"
								sortProperty="SEQTD" sortable="true" format="<%=strCurrFmt%>" style="text-align: right;" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_PROJECTED" var="varProjected"/><display:column property="EXLEQTD" title="${varProjected}" style="font-size: 9px;text-align: right;" media="excel"/>	
								<% 	
				   				}
								if (yearDisplay.equals("off")!=true) 
			   					{
								%>
							<%--  Year Growth Report --%>
							<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
							<fmtSalesGrowthReport:message key="DT_CYTD" var="varCytd"/><display:column property="YTD" title="${varCytd}" headerClass="ShadeDarkOrangeTD"  class="ShadeMedOrangeTD" sortable="true" 
								sortProperty="SYTD" format="<%=strCurrFmt%>" style="text-align: right;" total="true" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_CYTD" var="varCytd"/><display:column property="EXLYTD" title="${varCytd}" style="font-size: 9px;text-align: right;" media="excel"/>
											<% 
							if (PreYearDisplay.equals("on")) 
				   				{
				   			%>
							<fmtSalesGrowthReport:message key="DT_PYTD" var="varPytd"/><display:column property="PYTD" title="${varPytd}" headerClass="ShadeDarkOrangeTD"  class="ShadeLightOrangeTD" sortable="true" 
								sortProperty="SPYTD" format="<%=strCurrFmt%>" style="text-align: right;" total="true" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_PYTD" var="varPytd"/><display:column property="EXLPYTD" title="${varPytd}" style="font-size: 9px;text-align: right;" media="excel"/>
								<% }%>
							<fmtSalesGrowthReport:message key="DT_QUO_AMT" var="varQuoAmt"/><display:column property="QYTD" title="${varQuoAmt}" headerClass="ShadeDarkOrangeTD" total="true" media="<%=strDisplay%>"
								class="ShadeMedOrangeTD" sortProperty="SQYTD" sortable="true" format="<%=strCurrFmt%>" style="text-align: right;" />
							<fmtSalesGrowthReport:message key="DT_QUO_AMT" var="varQuoAmt"/><display:column property="EXLQYTD" title="${varQuoAmt}" style="font-size: 9px;text-align: right;" media="excel"/>
							<fmtSalesGrowthReport:message key="DT_QUOTA" var="varQuota"/><display:column property="YQPER" title="${varQuota}" headerClass="ShadeDarkOrangeTD"  media="<%=strDisplay%>"
								class="ShadeLightOrangeTD" sortProperty="SYQPER" sortable="true" format="{0,number,##0.00%}" style="text-align: right;" />
							<fmtSalesGrowthReport:message key="DT_QUOTA" var="varQuota"/><display:column property="EXLYQPER" title="${varQuota}" style="font-size: 9px;text-align: right;" media="excel"/>
													<% 
							if (PreYearDisplay.equals("on")) 
				   				{
				   			%>
							<fmtSalesGrowthReport:message key="DT_GROWTH_AMT" var="varGrowthAmt"/><display:column property="YTDGTHDLR" title="${varGrowthAmt}" headerClass="ShadeDarkOrangeTD" class="ShadeMedOrangeTD" media="<%=strDisplay%>"
								sortProperty="SYTDGTHDLR" sortable="true" format="<%=strCurrFmt%>" style="text-align: right;" total="true"/>
							<fmtSalesGrowthReport:message key="DT_GROWTH_AMT" var="varGrowthAmt"/><display:column property="EXLYTDGTHDLR" title="${varGrowthAmt}" style="font-size: 9px;text-align: right;" media="excel"/>
							<fmtSalesGrowthReport:message key="DT_GROWTH" var="varGrowth"/><display:column property="YTDGTHPER" title="${varGrowth}" headerClass="ShadeDarkOrangeTD" class="ShadeLightOrangeTD"
								sortProperty="SYTDGTHPER" format="{0,number,##0.00%}" sortable="true" style="text-align: right;" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_GROWTH" var="varGrowth"/><display:column property="EXLYTDGTHPER" title="${varGrowth}" style="font-size: 9px;text-align: right;" media="excel"/>
									<% 	
				   				}
								%>
							<fmtSalesGrowthReport:message key="DT_PROJECTED" var="varProjected"/><display:column property="EYTD" title="${varProjected}" headerClass="ShadeDarkOrangeTD" total="true" class="ShadeMedOrangeTD"
								sortProperty="SEYTD" sortable="true"  format="<%=strCurrFmt%>" style="text-align: right;" media="<%=strDisplay%>"/>
							<fmtSalesGrowthReport:message key="DT_PROJECTED" var="varProjected"/><display:column property="EXLEYTD" title="${varProjected}" style="font-size: 9px;text-align: right;" media="excel"/>	
								<% 	
				   				}
								%>
 							<%--<display:column property="ICON"  title="" headerClass="ShadeDarkGrayTD" class="ShadeMedGrayTD" media="html"/> --%>
 				<% if (strDisplay.equals("html csv xml pdf")) { %>
				<display:footer media="html"> 
				<%
				 HashMap hmColumns = (HashMap)pageContext.getAttribute("totals");
				// //System.out.println("hmColumns : " + hmColumns);
				 strColumn4 =  hmColumns.get("column4") != null ? String.valueOf(hmColumns.get("column4")) : "0";
				 strColumn5 =  hmColumns.get("column5") != null ? String.valueOf( hmColumns.get("column5")) : "0";
				 strColumn6 =  hmColumns.get("column6") != null ? String.valueOf( hmColumns.get("column6")) : "0";
				 strColumn7 =  hmColumns.get("column7") != null ? String.valueOf( hmColumns.get("column7")) : "0";
				 strColumn8 =  hmColumns.get("column8") != null ? String.valueOf( hmColumns.get("column8")) : "0";
				 strColumn9 =  hmColumns.get("column9") != null ? String.valueOf( hmColumns.get("column9")) : "0";
				 strColumn10 =  hmColumns.get("column10") != null ? String.valueOf( hmColumns.get("column10")) : "0";
				 strColumn11 =  hmColumns.get("column11") != null ? String.valueOf( hmColumns.get("column11")) : "0";
				 strColumn12 =  hmColumns.get("column12") != null ? String.valueOf( hmColumns.get("column12")) : "0";
				 strColumn13 =  hmColumns.get("column13") != null ? String.valueOf( hmColumns.get("column13")) : "0";
				 strColumn14 =  hmColumns.get("column14") != null ? String.valueOf( hmColumns.get("column14")) : "0";
				 strColumn15 =  hmColumns.get("column15") != null ? String.valueOf( hmColumns.get("column15")) : "0";
				 strColumn16 =  hmColumns.get("column16") != null ? String.valueOf( hmColumns.get("column16")) : "0";
				 strColumn17 =  hmColumns.get("column17") != null ? String.valueOf( hmColumns.get("column17")) : "0";
				 strColumn18 =  hmColumns.get("column18") != null ? String.valueOf( hmColumns.get("column18")) : "0";
				 strColumn19 =  hmColumns.get("column19") != null ? String.valueOf( hmColumns.get("column19")) : "0";
				 strColumn20 =  hmColumns.get("column20") != null ? String.valueOf( hmColumns.get("column20")) : "0";
				 strColumn21 =  hmColumns.get("column21") != null ? String.valueOf( hmColumns.get("column21")) : "0";
				 strColumn22 =  hmColumns.get("column22") != null ? String.valueOf( hmColumns.get("column22")) : "0";
				 strColumn23 =  hmColumns.get("column23") != null ? String.valueOf( hmColumns.get("column23")) : "0";
				 strColumn24 =  hmColumns.get("column24") != null ? String.valueOf( hmColumns.get("column24")) : "0";
				 strColumn25 =  hmColumns.get("column25") != null ? String.valueOf( hmColumns.get("column25")) : "0";
				 strColumn26 =  hmColumns.get("column26") != null ? String.valueOf( hmColumns.get("column26")) : "0";
				 strColumn27 =  hmColumns.get("column27") != null ? String.valueOf( hmColumns.get("column27")) : "0";
				 
				 %>
				<%	if (strOpt.equals("TERR") )
					{ %>
					
					<tr><td class="Line" colspan="27"></td></tr>
					<tr><td class = "ShadeDarkGrayTD" colspan="2"> <B><fmtSalesGrowthReport:message key="LBL_TOTAL"/> </B></td>
					<td class = "ShadeDarkGrayTD" style="text-align: right;" ></td>
					<%	}
					else
						{ %>
					<tr><td class="Line" colspan="26"></td></tr>
					<tr><td class = "ShadeDarkGrayTD" colspan="2"> <B><fmtSalesGrowthReport:message key="LBL_TOTAL"/> </B></td>
					<% }
				
				if (PreYearDisplay.equals("on")) 
	   			{
							if (strOpt.equals("TERR") )
							{
							strVal 		= strColumn5;
							strPVal 	= strColumn6;
							strQuota	= strColumn7;
							strGrowth 	= strColumn9;
							strExpQuota = strColumn11;
							}
							else
							{
							strVal 		= strColumn4;
							strPVal 	= strColumn5;
							strQuota	= strColumn6;
							strGrowth 	= strColumn8;
							strExpQuota = strColumn10;
							}	
	   			}

						if (PreYearDisplay.equals("on")!=true) 
			   			{
								if (strOpt.equals("TERR") )
								{
								strVal 		= strColumn5;								
								strQuota	= strColumn6;
								strExpQuota = strColumn8;
								
								}
								else
								{
								strVal 		= strColumn4;								
								strQuota	= strColumn5;								
								strExpQuota = strColumn7;
								}	
			   			}

						if (PreYearDisplay.equals("on")) 
			   			{
							dbTemp = ((Double.parseDouble(strVal)- Double.parseDouble(strPVal))  /Double.parseDouble(strPVal) ) ;
			   			}
						if(Double.isNaN(dbTemp)||Double.isInfinite(dbTemp))
						{
							dbTemp=0;
						}
						dbTempQ = (Double.parseDouble(strVal)/Double.parseDouble(strQuota)) ;
						if(Double.isNaN(dbTempQ)||Double.isInfinite(dbTempQ))
						{
							dbTempQ=0;
						}
						
						strVal 		= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strVal));
						strVal		= (strVal.indexOf("(")!=-1)?strForeColor + strVal:strVal; // In Total row to give red color for $ symbol as well as negative value.
						
						strPVal 	= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strPVal));
						strPVal		= (strPVal.indexOf("(")!=-1)?strForeColor + strPVal:strPVal;
						
						strGrowth 	= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strGrowth));
						strGrowth 	= (strGrowth.indexOf("(")!=-1)?strForeColor + strGrowth:strGrowth;
						
						strQuota 	= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strQuota));
						strQuota	= (strQuota.indexOf("(")!=-1)?strForeColor + strQuota:strQuota;
						
						strExpQuota = strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strExpQuota));
						strExpQuota = (strExpQuota.indexOf("(")!=-1)?strForeColor + strExpQuota:strExpQuota;
						
						strPercent 	= GmCommonClass.getRedTextSmall(df.format(dbTemp));
						strPercentQuota = GmCommonClass.getRedTextSmall(df.format(dbTempQ));
						//strPercent = "";
		   				//if (monthDisplay.equals("Display")) 
		   				//{
					%>
					<td class="Line"></td>
					<td class = "<%=strFooterClass1%>" style="text-align: right;" ><B><%=strVal%></B></td>
					<% 
						if (PreYearDisplay.equals("on")) 
				   		{
				   	%>	
							<td class = "<%=strFooterClass1%>" style="text-align: right;" ><B><%=strPVal%></B></td>
					<% 	
				   				}
					%>
					<% if (strDisplay.equals("html csv xml pdf") ) { %>
					<td class = "<%=strFooterClass1%>" style="text-align: right;" ><B><%=strQuota%></B></td>
					<td class = "<%=strFooterClass1%>" style="text-align: right;" ><B><%=strPercentQuota%></B></td>
					<% } %>
					<% 
						if (PreYearDisplay.equals("on")) 
				   		{
				   	%>
					<td class = "<%=strFooterClass1%>" style="text-align: right;" ><B><%=strGrowth%></B></td>
					<td class = "<%=strFooterClass1%>" style="text-align: right;" ><B><%=strPercent%></B></td>
					<% 	
				   				}
					%>
				<% if (strDisplay.equals("html csv xml pdf") ) { %>
					<td class = "<%=strFooterClass1%>" style="text-align: right;" ><B><%=strExpQuota%></B></td>
				<% } %>			
					<%
					strVal = "0";
					strPVal = "0";
					strQuota = "0";
					strGrowth = "0";
					strExpQuota = "0";
					strPercent = "0";
					strPercentQuota = "0";
					
					if ((monthDisplay.equals("off")!=true && quarterDisplay.equals("off")!=true)||(monthDisplay.equals("off")!=true&&yearDisplay.equals("off")!=true)
						||(quarterDisplay.equals("off")!=true&& yearDisplay.equals("off")!=true)) 
   					{
						if (PreYearDisplay.equals("on")) 
			   			{
							
   			
							if (strOpt.equals("TERR") )
							{
								strVal 		= strColumn13;
								strPVal 	= strColumn14;
								strQuota	= strColumn15;							
								strGrowth 	= strColumn17;
								strExpQuota = strColumn19;
							}
							else
							{
								strVal 		= strColumn12;
								strPVal 	= strColumn13;										
								strQuota 	= strColumn14;							
								strGrowth	= strColumn16;
								strExpQuota = strColumn18;
							}	
			   			}
						
						if (PreYearDisplay.equals("on")!=true) 
			   			{
								if (strOpt.equals("TERR") || strOpt.equals("AD")||strOpt.equals("VP"))
								{
								strVal 		= strColumn10;
								strQuota	= strColumn11;
								strExpQuota = strColumn13;
								}
								else
								{
									strVal 		= strColumn9;									
									strQuota 	= strColumn10;
									strExpQuota = strColumn12;
								}	
			   			}
						
						if (PreYearDisplay.equals("on")) 
			   			{
							dbTemp = ( ( Double.parseDouble(strVal)- Double.parseDouble(strPVal))  /Double.parseDouble(strPVal) ) ;
			   			}
						if(Double.isNaN(dbTemp)||Double.isInfinite(dbTemp))
						{
							dbTemp=0;
						}
						dbTempQ = (Double.parseDouble(strVal)/Double.parseDouble(strQuota)) ;
						if(Double.isNaN(dbTempQ)||Double.isInfinite(dbTempQ))
						{
							dbTempQ=0;
						}
						
						strVal 		= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strVal));
						strVal		= (strVal.indexOf("(")!=-1)?strForeColor + strVal:strVal; // In Total row to give red color for $ symbol as well as negative value.
						
						strPVal 	= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strPVal));
						strPVal		= (strPVal.indexOf("(")!=-1)?strForeColor + strPVal:strPVal;
						
						strGrowth 	= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strGrowth));
						strGrowth 	= (strGrowth.indexOf("(")!=-1)?strForeColor + strGrowth:strGrowth;
						
						strQuota 	= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strQuota));
						strQuota	= (strQuota.indexOf("(")!=-1)?strForeColor + strQuota:strQuota;
						
						strExpQuota = strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strExpQuota));
						strExpQuota = (strExpQuota.indexOf("(")!=-1)?strForeColor + strExpQuota:strExpQuota;
						
						strPercent 	= GmCommonClass.getRedTextSmall(df.format(dbTemp));
						strPercentQuota = GmCommonClass.getRedTextSmall(df.format(dbTempQ));

					%>
					<td class="Line"></td>
					<td class = "<%=strFooterClass2%>" style="text-align: right;" ><B><%=strVal%></B></td>
					<% 
						if (PreYearDisplay.equals("on")) 
				   		{
				   	%>	
							<td class = "<%=strFooterClass2%>" style="text-align: right;" ><B><%=strPVal%></B></td>
					<% 	
				   				}
					%>
					<% if (strDisplay.equals("html csv xml pdf")) { %>
					<td class = "<%=strFooterClass2%>" style="text-align: right;" ><B><%=strQuota%></B></td>
					<td class = "<%=strFooterClass2%>" style="text-align: right;" ><B><%=strPercentQuota%></B></td>
					<% } %>
					<% 
						if (PreYearDisplay.equals("on")) 
				   		{
				   	%>
					<td class = "<%=strFooterClass2%>" style="text-align: right;" ><B><%=strGrowth%></B></td>
					<td class = "<%=strFooterClass2%>" style="text-align: right;" ><B><%=strPercent%></B></td>
					<% 	
				   				}
					%>
					<% if (strDisplay.equals("html csv xml pdf")) { %>
					<td class = "<%=strFooterClass2%>" style="text-align: right;" ><B><%=strExpQuota%></B></td>
					<% } %>
					<%
					}
					strVal = "0";
					strPVal = "0";
					strQuota = "0";
					strGrowth = "0";
					strExpQuota = "0";
					strPercent = "0";
					strPercentQuota = "0";
					
					if (monthDisplay.equals("off")!=true && quarterDisplay.equals("off")!=true && yearDisplay.equals("off")!=true) 
	   				{
						
						if (PreYearDisplay.equals("on")) 
			   			{
							if (strOpt.equals("TERR") )
							{
								strVal 		= strColumn21;
								strPVal 	= strColumn22;							
								strQuota  	= strColumn23;							
								strGrowth   = strColumn25;
								strExpQuota = strColumn27;
							}
							else
							{
								strVal 		= strColumn20;
								strPVal 	= strColumn21;
								strQuota 	= strColumn22;							
								strGrowth	= strColumn24;
								strExpQuota = strColumn26;
							}	
			   			}

						if (PreYearDisplay.equals("on")!=true) 
			   			{
								if (strOpt.equals("TERR") || strOpt.equals("AD")||strOpt.equals("VP"))
								{
								strVal 		= strColumn15;								
								strQuota	= strColumn16;
								strExpQuota = strColumn18;
								}
								else
								{
									strVal 		= strColumn14;									
									strQuota 	= strColumn15;								
									strExpQuota = strColumn17;
								}	
			   			}
						if (PreYearDisplay.equals("on")) 
			   			{
							dbTemp = ( ( Double.parseDouble(strVal)- Double.parseDouble(strPVal))  /Double.parseDouble(strPVal) ) ;
			   			}
						if(Double.isNaN(dbTemp)||Double.isInfinite(dbTemp))
						{
							dbTemp=0;
						}
						dbTempQ = (Double.parseDouble(strVal)/Double.parseDouble(strQuota)) ;
						if(Double.isNaN(dbTempQ)||Double.isInfinite(dbTempQ))
						{
							dbTempQ=0;
						}
						
						strVal 		= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strVal));
						strVal		= (strVal.indexOf("(")!=-1)?strForeColor + strVal:strVal; // In Total row to give red color for $ symbol as well as negative value.
						
						strPVal 	= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strPVal));
						strPVal		= (strPVal.indexOf("(")!=-1)?strForeColor + strPVal:strPVal;
						
						strGrowth 	= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strGrowth));
						strGrowth 	= (strGrowth.indexOf("(")!=-1)?strForeColor + strGrowth:strGrowth;
						
						strQuota 	= strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strQuota));
						strQuota	= (strQuota.indexOf("(")!=-1)?strForeColor + strQuota:strQuota;
						
						strExpQuota = strCurrSign + "&nbsp;" + GmCommonClass.getRedTextSmall(GmCommonClass.getStringInThousands(strExpQuota));
						strExpQuota = (strExpQuota.indexOf("(")!=-1)?strForeColor + strExpQuota:strExpQuota;
						
						strPercent = GmCommonClass.getRedTextSmall(df.format(dbTemp));
						strPercentQuota = GmCommonClass.getRedTextSmall(df.format(dbTempQ));
		   				
					%>
					<td class="Line"></td>
					<td class = "ShadeDarkOrangeTD" style="text-align: right;" ><B><%=strVal%></B></td>
					<% 
						if (PreYearDisplay.equals("on")) 
				   		{
				   	%>	
							<td class = "ShadeDarkOrangeTD" style="text-align: right;" ><B><%=strPVal%></B></td>
					<% 	
				   				}
					%>
					<% if (strDisplay.equals("html csv xml pdf")) { %>
					<td class = "ShadeDarkOrangeTD" style="text-align: right;" ><B><%=strQuota%></B></td>
					<td class = "ShadeDarkOrangeTD" style="text-align: right;" ><B><%=strPercentQuota%></B></td>
					<% } %>
					<% 
						if (PreYearDisplay.equals("on")) 
				   		{
				   	%>
					<td class = "ShadeDarkOrangeTD" style="text-align: right;" ><B><%=strGrowth%></B></td>
					<td class = "ShadeDarkOrangeTD" style="text-align: right;" ><B><%=strPercent%></B></td>
					<% 	
				   				}
					%>
					<% if (strDisplay.equals("html csv xml pdf")) { %>
					<td class = "ShadeDarkOrangeTD" style="text-align: right;" ><B><%=strExpQuota%></B></td>	
					<% } %>
					<%} %>					
				</tr>
				</display:footer>
				<% } else {					
				%>			
				 <display:footer media="html">
				 <%
					 HashMap hmColumns = (HashMap)pageContext.getAttribute("totals");
					 //System.out.println("hmColumns : " + hmColumns);	
					 int noCols = hmColumns.keySet().size();
					 java.util.Iterator keys = hmColumns.keySet().iterator();
					 int tdSize = ( noCols * 2 ) + 2;
					 int startColumn = 4;
					 String strStartKey = "column";
					 String strValue = "";
					 String strCurrentKey = "";
					 String strCheckKey = "";
					 int previousColumn = 4;
					 int colSpan =0;
					 int index =1;
				 %>				 
				 	<tr><td class="Line" colspan= <%=tdSize%>></td></tr>
					<tr><td class = "ShadeDarkGrayTD" colspan="3"> <B> Total </B></td>
					<%
					   while(index <= noCols)
					   {						   
						   strCheckKey = strStartKey+startColumn;
						   Object o = hmColumns.get(strCheckKey);
							//System.out.println("strCheckKey : " + strCheckKey);	
							
						  while(o == null && startColumn <=27 ) 
						   {
							  startColumn = startColumn + 1;
							  strCheckKey = strStartKey+startColumn;
							  o = hmColumns.get(strCheckKey);
						   }
						  
						  strValue =  strValue != null ? String.valueOf(o) : "0";
						  //System.out.println("strValue : " + strValue);
						   colSpan = startColumn - previousColumn;
						   //System.out.println("colSpan : " + colSpan);
						  previousColumn = startColumn;  
						  
						  %>						   
						   <td class = "ShadeDarkGrayTD" style="text-align: right;" colspan = <%=colSpan%>><B><%=strCurrSign + GmCommonClass.getRedText(GmCommonClass.getStringInThousands(strValue))%></B></td>						   	
				     <% 
				     	  
					     startColumn ++;
				     	index++;
					   }
					%>	
					</tr>				
				 </display:footer>	
				<% } %>
				</display:table>
				<%} %>
			</td></FORM></tr>
		</table></td></tr>
	</table>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
