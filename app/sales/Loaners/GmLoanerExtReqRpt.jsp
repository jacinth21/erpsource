<%
	/*********************************************************************************************
	 * File		 		: GmLoanerExtReqRpt.jsp
	 * Desc		 		: This screen is used to show the Loaner Extension Requests Report
	 * Version	 		: 1.0
	 * author			: Agilan
	 *********************************************************************************************/
%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<%@ include file="/common/GmHeader.inc" %>
	<%@ page language="java" %>
	
	<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
	<%@ page import ="org.apache.commons.beanutils.PropertyUtils"%>
	
	<%@ taglib prefix="fmtLoanerExtReqReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
	<%@ page isELIgnored="false" %>
	<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
	<%@ page import ="com.globus.common.beans.GmCommonControls"%>
	<%@ page import="com.globus.common.beans.GmCalenderOperations"%>

<!-- \sales\Loaners\GmLoanerExtReqRpt.jsp -->

<fmtLoanerExtReqReport:setLocale value="<%=strLocale%>"/>
<fmtLoanerExtReqReport:setBundle basename="properties.labels.sales.Loaners.GmLoanerExtReqReport"/>
	
	<bean:define id="strZone" name="frmLoanerExtReqRpt" property="strZone" type="java.lang.String"></bean:define>
	<bean:define id="strRegion" name="frmLoanerExtReqRpt" property="strRegion" type="java.lang.String"></bean:define>
	<bean:define id="alZone" name="frmLoanerExtReqRpt" property="alZone" type="java.util.ArrayList"></bean:define>
	<bean:define id="alRegions" name="frmLoanerExtReqRpt" property="alRegions" type="java.util.ArrayList"></bean:define>
	<bean:define id="fromDt" name="frmLoanerExtReqRpt" property="fromDt" type="java.lang.String"> </bean:define>
	<bean:define id="toDt" name="frmLoanerExtReqRpt" property="toDt" type="java.lang.String"> </bean:define>
	<bean:define id="strFieldSales" name="frmLoanerExtReqRpt" property="strFieldSales" type="java.lang.String"></bean:define>
	<bean:define id="alFieldSales" name="frmLoanerExtReqRpt" property="alFieldSales" type="java.util.ArrayList"></bean:define>
	
<HTML>
<HEAD>	
	<% 
	String strSalesLoanersJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_LOANERS");
	String strApplDateFmt = strGCompDateFmt;

	String strWikiTitle = GmCommonClass.getWikiTitle("LOANER_EXTN_REQUEST_REPORT");
%>
	<TITLE> Globus Medical: Loaner Extension Request Report</TITLE>

	<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
 	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
 	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/daterangepicker/GmDateRangePicker.css">
 	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
	<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
	<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
    <link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/skins/dhtmlxcalendar_yahoolike.css"> 
    <script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
    <script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
    <script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
	<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
    <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
	<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_dhxcalendar.js"></script> 
	<script language="JavaScript" src="<%=strSalesLoanersJsPath%>/GmLoanerExtApproval.js"></script>
	<script language="JavaScript" src="<%=strSalesLoanersJsPath%>/GmLoanerExtReqRpt.js"></script>
	
	<script>
	var format = '<%=strApplDateFmt%>';
	var zone = '<%=strZone%>';
	var region = '<%=strRegion%>';
	var fieldSales = '<%=strFieldSales%>';
	var gCmpDateFmt = "<%=strGCompDateFmt%>";
	gCmpDateFmt = gCmpDateFmt.toUpperCase();
	var date = new Date();
	var frmdt = new Date(date.getFullYear(), date.getMonth(), 1);
	var todt = date;
	</script>
	
	<style>
	
	 ul {
        margin-left : 5px;
	}
	table.DtTable1420 {
		margin: 0 0 0 0;
		width: 1420px;
		border: 1px solid  #676767;
	}
	.lblSpan {
  		float: left;
  		display: inline-block;
  		padding-top: 38px;
  		margin-right: 15px;
  		padding-left: 10px;
	}
	.dtSpan {
  		display: inline-block;
  		
	}
	.calSpan{
		position: relative;
		padding: 10px 10px 10px 10px;
		border:1px solid  #676767;	
	}
	#divtoDt{
	   left:180px;
	}
	
   .divSpan{
	   float:left;
	   margin-right: 20px;

	} 
	</style>

</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
	<html:form action="/gmLoanerExtReqRpt.do" method="post">
	<input type="hidden" name="hZoneInnerHtml" value="">
    <input type="hidden" name="hRegnInnerHtml" value="">
	<input type="hidden" name="hZoneGrp" value="<%=strZone%>">	
	<input type="hidden" name="hRegionGrp" value="<%=strRegion%>">
	<input type="hidden" name="hFieldSales" value="<%=strFieldSales%>">
	<input type="hidden" name="strFieldSales" value="">
	<input type="hidden" name="strZone" value="">
	<input type="hidden" name="strRegion" value="">
	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable1420"  cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4">&nbsp;<fmtLoanerExtReqReport:message key="LBL_LOANER_EXTENSION_REQUEST"/></td>
			
			<td align="right" class=RightDashBoardHeader>
				<fmtLoanerExtReqReport:message key="IMG_ALT_HELP" var = "varHelp"/>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr><td colspan="5" height="5"></td></tr>
		<tr>
			<td class="RightTableCaption" colspan="5">
			<jsp:include page="/sales/Loaners/GmIncludeLoanerFilters.jsp" >
			<jsp:param value="frmLoanerExtReqRpt" name="FORMNAME"/>
			<jsp:param value="alZone" name="ALZONE"/>
			<jsp:param value="alRegions" name="ALREGIONS"/>
			<jsp:param value="alFieldSales" name="ALFIELDSALES"/>
			</jsp:include>
			<div style="padding-top:40px;" ><span style="padding-left:20px;" >
			<fmtLoanerExtReqReport:message key="LBL_EXTN_REQ_DATE" />:</span>
					<!-- <span class="dtSpan"><span class="calSpan"><fmtLoanerExtReqReport:message key="LBL_FROM"/>:&nbsp; -->
					<gmjsp:calendar SFFormName="frmLoanerExtReqRpt"  controlName="fromDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
					<!--<fmtLoanerExtReqReport:message key="LBL_TO"/>:&nbsp;-->
					<gmjsp:calendar SFFormName="frmLoanerExtReqRpt"  controlName="toDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
					<input type="text" id="dateRangeId" name="dateRangeNm" value="" readonly="readonly"/> 
					&nbsp;&nbsp;<fmtLoanerExtReqReport:message key="BTN_LOAD" var="varLoad"/>
					<gmjsp:button value="&nbsp;${varLoad}&nbsp;" name="Btn_Load" buttonType="Load" gmClass="Button" onClick="javascript:fnLoad();" /> 
				</div>
			</td>
		</tr>
		<tr><td colspan="5" height="1" class="Line"></td></tr>
		<tr>
            <td align="left" colspan="6">
              <div id="dataGridDiv" style="width: 100%;" height="400px" width="1420px"></div>
               <div id="pagingArea" height="400px" width="1420px"></div>	
           </td>
        </tr> 
        <tr><td align="center" colspan="6">
            <div class='exportlinks' style="display: none" id="DivExportExcel"><fmtLoanerExtReqReport:message key="DIV_EXPORT_OPT" />:<img src='img/ico_file_excel.png' onclick="fnExport();"/>&nbsp; 
            <a href="#" onclick="fnExport();"><fmtLoanerExtReqReport:message key="DIV_EXCEL" /></a></div>
 	      </td>
       </tr>    
	<tr><td colspan="6" align="center"  class="RegularText"><div  id="DivNothingMessage"><fmtLoanerExtReqReport:message key="LBL_NO_DATA_AVAILABLE"/></div></td></tr>
   
	</table>
</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
<script>
if (document.all.Div_Zone){
	document.frmLoanerExtReqRpt.hZoneInnerHtml.value = document.all.Div_Zone.innerHTML;
}	
</script>
<script language="JavaScript" src="<%=strJsPath%>/moment/moment.min.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/daterangepicker/daterangepicker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/common/GmDaterange.js"></script>
<script>$(document).ready(function() {fnDaterange(frmdt,todt,'#dateRangeId');});</script> 
</HTML>