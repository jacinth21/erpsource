<%
/**********************************************************************************
 * File		 		: GmLoanerLogDtl.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Angeline Sophia .A
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--sales\Loaners\GmLoanerLogDtl.jsp -->
<%@ page language="java"%>

<%@page import="java.util.Date"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmCommonClass"%>
<%@ page import="java.util.*,java.io.*"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtLoanerExtReqReport" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<fmtLoanerExtReqReport:setLocale value="<%=strLocale%>"/>
<fmtLoanerExtReqReport:setBundle basename="properties.labels.sales.Loaners.GmLoanerExtReqReport"/>
<bean:define id="hmHistoryDtls" name="frmLoanerExtReqRpt" property="hmHistoryDtls" type="java.util.HashMap"></bean:define>
<bean:define id="hmHistoryDtl" name="frmLoanerExtReqRpt" property="hmHistoryDtl" type="java.util.ArrayList"></bean:define>
<bean:define id="strfl" name="frmLoanerExtReqRpt" property="strfl" type="java.lang.String"></bean:define>
<%
String strSalesLoanersJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_LOANERS");
int intLog = 0;	
HashMap hmLog =new HashMap();
String strcmt = "";
String stract = "";
String strLnrdt = "";
String strExtreqretdt = "";
String strExtreqdt = "";
String strSurgydt = "";
String strApprrtndt = "";
String strExprtdt = "";
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: History Details</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strSalesLoanersJsPath%>/GmLoanerExtReqRpt.js"></script>

</HEAD>
 <BODY leftmargin="20" topmargin="10" onload="fnLoad();">
	<html:form action="/gmLoanerExtReqRpt.do" method="post">
	<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
	
	<tr><td class="RightDashBoardHeader" colspan="8"><fmtLoanerExtReqReport:message key="LBL_HIS_DETAILS"/></td></tr>
	 <tr class="Shade">
		<td class="RightTableCaption" width="120px"><fmtLoanerExtReqReport:message key="LBL_REQUESTOR_NM"/></td>
		<td class="RightTableCaption" width="130px"><fmtLoanerExtReqReport:message key="LBL_COMMENTS"/></td>
		<td  class="RightTableCaption" width="250px" style="padding-left: 18px;"><fmtLoanerExtReqReport:message key="LBL_LOANED_ON"/></td>
		<td  class="RightTableCaption" width="250px"><fmtLoanerExtReqReport:message key="LBL_EPT_RETURN_DATE"/></td>
		<td  class="RightTableCaption" width="250px"><fmtLoanerExtReqReport:message key="LBL_EXTN_REQUESTED_DATE"/></td>
		<td  class="RightTableCaption" width="350px" style="padding-left: 25px;"><fmtLoanerExtReqReport:message key="LBL_SURGERY_DATE"/></td>
		<td  class="RightTableCaption" width="130px"><fmtLoanerExtReqReport:message key="LBL_EXTN_REQ_RETURN_DT"/></td>
		<td  class="RightTableCaption" width="250px" style="padding-left: 18px;"><fmtLoanerExtReqReport:message key="LBL_APPV_RETURN_DT"/></td>
	</tr> 
	
	<tr><td colspan="8" height=1 class=Line></td></tr> 
	
	<% if (hmHistoryDtl != null){
		intLog = hmHistoryDtl.size();
	}	
	
	
	
	if (intLog > 0)
	{	
		for (int i=0;i<intLog;i++)
		{	
			hmLog = (HashMap)hmHistoryDtl.get(i);
			String strComments = GmCommonClass.parseNull((String)hmLog.get("COMMENTS"));
			String strAccName = GmCommonClass.parseNull((String)hmLog.get("REQUESTORNM"));
			String strLoanerDt = GmCommonClass.parseNull((String)hmLog.get("LOANERDT"));
			String strExtnreqretdt = GmCommonClass.parseNull((String)hmLog.get("EXTNREQRETDT"));
			String strExtnreqdt = GmCommonClass.parseNull((String)hmLog.get("EXTNREQDT"));
			String strSurgerydt = GmCommonClass.parseNull((String)hmLog.get("SURGDT"));
			String strApprretdt = GmCommonClass.parseNull((String)hmLog.get("APPRRETDT"));
			String strExpretdt = GmCommonClass.parseNull((String)hmLog.get("EXPRETDT"));
			
			
			if(strfl.equals("Y")){ 
			 String[] sttCmnts = strComments.split("~");
			 String[] sttAccNm = strAccName.split("~");
			 String[] sttLnrdt = strLoanerDt.split("~");
			 String[] sttExtreqretdt = strExtnreqretdt.split("~");
			 String[] sttExtreqdt = strExtnreqdt.split("~");
			 String[] sttSurgydt = strSurgerydt.split("~");
			 String[] sttApprrtndt = strApprretdt.split("~");
			 String[] sttExpretdt = strExpretdt.split("~"); 
			
			 strcmt = sttCmnts[1]; 
			 stract = sttAccNm[1]; 
			 strLnrdt = sttLnrdt[1]; 
			 strExtreqretdt = sttExtreqretdt[1]; 
			 strExtreqdt = sttExtreqdt[1]; 
			 strSurgydt = sttSurgydt[1]; 
			 strApprrtndt = sttApprrtndt[1]; 
			 strExprtdt = sttExpretdt[1]; 
			}
			%>
			
			<tr height="20px">
				<% if(strfl.equals("Y")){ %>
				<td ><%=GmCommonClass.parseNull((String)hmLog.get("NAME"))%></td>
				<td ><%=GmCommonClass.parseNull((String)hmLog.get("DT"))%></td>
				<td style="word-wrap:break-word;"><%=strcmt%></td> 
				<td style="word-wrap:break-word;"><%=stract%></td>
				<td style="word-wrap:break-word;"><%=strLnrdt%></td>
				<td style="word-wrap:break-word;"><%=strExtreqretdt%></td>
				<td style="word-wrap:break-word;"><%=strExtreqdt%></td>
				<td style="word-wrap:break-word;"><%=strSurgydt%></td>
				<td style="word-wrap:break-word;"><%=strApprrtndt%></td>
				<td style="word-wrap:break-word;"><%=strExprtdt%></td> 
				<% }else{ %>
				<td style="word-wrap:break-word;"><%=strAccName%></td> 
				<td style="word-wrap:break-word;"><%=strComments%></td> 
				<td style="padding-left: 18px;"><%=strLoanerDt%></td> 
				<td style="word-wrap:break-word;"><%=strExpretdt%></td> 
				<td style="word-wrap:break-word;"><%=strExtnreqdt%></td> 
				<td style="padding-left: 25px;"><%=strSurgerydt%></td> 
				<td style="word-wrap:break-word;"><%=strExtnreqretdt%></td> 
				<td style="padding-left: 18px;"><%=strApprretdt%></td> 
				
				<%} %>				
		<%}}else{ %>
	   <tr ><td colspan="8" height="25" align="center" class="RightTextBlue">
	             <fmtLoanerExtReqReport:message key="TD_NO_DATA"/></td></tr>
	
		<%} %>	

		<% if(intLog>0) { %>
			<tr><td colspan="8" height=1 class=Line></td></tr> 
			<tr height="30"><td colspan="4" class="aligncenter" align="right"><fmtLoanerExtReqReport:message key="BTN_CLOSE" var="varClose"/>
			<gmjsp:button  value="${varClose}" gmClass="button" onClick="fnClose();" buttonType="Load" />&nbsp;</td></tr> 
		<%} %>
	</table>
	</html:form>
</BODY> 
</HTML>