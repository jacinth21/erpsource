<%
/**********************************************************************************
 * File		 		: GmLoanerCallLogDetails.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Agilan S
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--sales\Loaners\GmLoanerCallLogDetails.jsp -->
<%@ page language="java"%>

<%@page import="java.util.Date"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmCommonClass"%>
<%@ page import="java.util.*,java.io.*"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtLoanerExtReqReport" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<fmtLoanerExtReqReport:setLocale value="<%=strLocale%>"/>
<fmtLoanerExtReqReport:setBundle basename="properties.labels.sales.Loaners.GmLoanerExtReqReport"/>
<bean:define id="hmHistoryDtls" name="frmLoanerExtReqRpt" property="hmHistoryDtls" type="java.util.HashMap"></bean:define>
<bean:define id="hmHistoryDtl" name="frmLoanerExtReqRpt" property="hmHistoryDtl" type="java.util.ArrayList"></bean:define>
<bean:define id="strfl" name="frmLoanerExtReqRpt" property="strfl" type="java.lang.String"></bean:define>
<%
String strSalesLoanersJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_LOANERS");
int intLog = 0;	
HashMap hmLog =new HashMap();
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Log Details</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strSalesLoanersJsPath%>/GmLoanerExtReqRpt.js"></script>

</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnLoad();">
	<html:form action="/gmLoanerExtReqRpt.do" method="post">
	<table border="0" class="DtTable500" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
	<% if(strfl.equals("Y")){ %>
	<tr><td class="RightDashBoardHeader" colspan="3"><fmtLoanerExtReqReport:message key="LBL_HIS_DETAILS"/></td></tr>
	<tr class="Shade">
		<td class="RightTableCaption" width="120px">&nbsp;<fmtLoanerExtReqReport:message key="LBL_REQUESTOR_NM"/></td>
		<td class="RightTableCaption" width="130px">&nbsp;<fmtLoanerExtReqReport:message key="LBL_EXTN_REQUTESTED"/></td>
		<td  class="RightTableCaption" width="250px"><fmtLoanerExtReqReport:message key="LBL_EXTN_REQ_RETURN_DT"/></td>
	</tr>
	<% }else { %>
	<tr><td class="RightDashBoardHeader" colspan="3"><fmtLoanerExtReqReport:message key="LBL_LOG_DETAILS"/></td></tr>
	<tr class="Shade">
		<td class="RightTableCaption" width="120px">&nbsp;<fmtLoanerExtReqReport:message key="LBL_UNAME"/></td>
		<td class="RightTableCaption" width="130px">&nbsp;<fmtLoanerExtReqReport:message key="LBL_DATE"/></td>
		<td  class="RightTableCaption" width="250px"><fmtLoanerExtReqReport:message key="LBL_COMMENTS"/></td>
	</tr>
	<% } %>
	<tr><td colspan="3" height=1 class=Line></td></tr> 
	<% if (hmHistoryDtl != null){
		intLog = hmHistoryDtl.size();
	}	
	String strcmt = "";
	if (intLog > 0)
	{	
		for (int i=0;i<intLog;i++)
		{	
			hmLog = (HashMap)hmHistoryDtl.get(i);
			String strComments = GmCommonClass.parseNull((String)hmLog.get("COMMENTS"));
			if(strfl.equals("Y")){ 
			String[] sttCmnts = strComments.split("~");
			strcmt = sttCmnts[1]; 
			}
			%>
			
			<tr height="20px">
				<% if(strfl.equals("Y")){ %>
				<td ><%=GmCommonClass.parseNull((String)hmLog.get("NAME"))%></td>
				<td ><%=GmCommonClass.parseNull((String)hmLog.get("DT"))%></td>
				<td style="word-wrap:break-word;"><%=strcmt%></td> 
				<% }else{ %>
				<td ><%=GmCommonClass.parseNull((String)hmLog.get("NAME"))%></td>
				<td ><%=GmCommonClass.parseNull((String)hmLog.get("DT"))%></td>
				<td style="word-wrap:break-word;"><%=strComments%></td> 
				<%} %>				
		<%}}else{ %>
	    <tr ><td colspan="3" height="25" align="center" class="RightTextBlue">
	             <fmtLoanerExtReqReport:message key="TD_NO_DATA"/></td></tr>
	
		<%} %>	

		<% if(intLog>0) { %>
			<tr><td colspan="3" height=1 class=Line></td></tr> 
			<tr height="30"><td colspan="3" class="aligncenter" align="center"><fmtLoanerExtReqReport:message key="BTN_CLOSE" var="varClose"/>
			<gmjsp:button  value="${varClose}" gmClass="button" onClick="fnClose();" buttonType="Load" />&nbsp;</td></tr> 
		<%} %>
	</table>
	</html:form>
</BODY>
</HTML>