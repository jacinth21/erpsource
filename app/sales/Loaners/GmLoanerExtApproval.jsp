<%
	/*********************************************************************************************
	 * File		 		: GmLoanerExtApproval.jsp
	 * Desc		 		: This screen is used to Approve and Reject the Loaner Extension Requests
	 * Version	 		: 1.0
	 * author			: Harinadh Reddi
	 *********************************************************************************************/
%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<%@ include file="/common/GmHeader.inc" %>
	<%@ page language="java" %>
	
	<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
	<%@ page import ="org.apache.commons.beanutils.PropertyUtils"%>
	
	<%@ taglib prefix="fmtLoanerExtApproval" uri="http://java.sun.com/jsp/jstl/fmt" %>
	<%@ page isELIgnored="false" %>
	<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
	<%@ page import ="com.globus.common.beans.GmCommonControls"%>
	<%@page import="com.globus.common.beans.GmCalenderOperations"%>

<!-- sales\Loaners\GmLoanerExtApproval.jsp -->

<fmtLoanerExtApproval:setLocale value="<%=strLocale%>"/>
<fmtLoanerExtApproval:setBundle basename="properties.labels.sales.Loaners.GmLoanerExtApproval"/>

	<bean:define id="strZone" name="frmLoanerExtApprl" property="strZone" type="java.lang.String"></bean:define>
	<bean:define id="strRegion" name="frmLoanerExtApprl" property="strRegion" type="java.lang.String"></bean:define>
	<bean:define id="alZone" name="frmLoanerExtApprl" property="alZone" type="java.util.ArrayList"></bean:define>
	<bean:define id="alRegions" name="frmLoanerExtApprl" property="alRegions" type="java.util.ArrayList"></bean:define>
	<bean:define id="strFieldSales" name="frmLoanerExtApprl" property="strFieldSales" type="java.lang.String"></bean:define>
	<bean:define id="alFieldSales" name="frmLoanerExtApprl" property="alFieldSales" type="java.util.ArrayList"></bean:define>
<HTML>
<HEAD>	
	<% 
	
	String strSalesLoanersJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_LOANERS");
	/* String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt")); */
	String strApplDateFmt = strGCompDateFmt;
	
	%>
	<TITLE> Globus Medical: Loaner Extension Request</TITLE>

	<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
 	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
	<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
	<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
    <link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/skins/dhtmlxcalendar_yahoolike.css"> 
    <script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
    <script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
	<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
    <script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
    <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
	<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_dhxcalendar.js"></script> 
	<script language="JavaScript" src="<%=strSalesLoanersJsPath%>/GmLoanerExtApproval.js"></script>
	<script>

		var format = '<%=strApplDateFmt%>';
		var zone = '<%=strZone%>';
		var region = '<%=strRegion%>';
		var fieldSales = '<%=strFieldSales%>';
	</script>
	<style>
	 ul {
     margin-left : 5px;
	}
	</style>
	<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
	table.DtTable1450 {
		margin: 0 0 0 0;
		width: 1450px;
		border: 1px solid  #676767;
	}
	.lblSpan {
  		float: left;
  		display: inline-block;
  		padding-top: 38px;
  		margin-right: 15px;
	}
	.divSpan{
	float:left;  
	margin-right: 100px;
	}
</style>

</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
	<html:form action="/gmLoanerExtApprl.do" method="post">
	<input type="hidden" name="hZoneGrp" value="<%=strZone%>">	
	<input type="hidden" name="hRegionGrp" value="<%=strRegion%>">
	<input type="hidden" name="hFieldSales" value="<%=strFieldSales%>">
	<input type="hidden" name="strZone" value="">
	<input type="hidden" name="strRegion" value="">
	<input type="hidden" name="strFieldSales" value="">
	<input type="hidden" name="hZoneInnerHtml" value="">
<input type="hidden" name="hRegnInnerHtml" value="">
<input type="hidden" name="hFSInnerHtml" value="">
	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable1450"  cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4">&nbsp;<fmtLoanerExtApproval:message key="LBL_LOANER_EXTENSION_REQUEST"/></td>
			
			<td align="right" class=RightDashBoardHeader>
				<fmtLoanerExtApproval:message key="IMG_ALT_HELP" var = "varHelp"/>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("LOANER_EXTN_REQUEST")%>');" />
			</td>
		</tr>
		<tr><td colspan="5" height="5"></td></tr>
		<tr>
		<!-- <td>&nbsp;&nbsp;</td> -->
			<td class="RightRedCaption" colspan="5">
			<jsp:include page="/sales/Loaners/GmIncludeLoanerFilters.jsp" >
			<jsp:param value="frmLoanerExtApprl" name="FORMNAME"/>
			<jsp:param value="alZone" name="ALZONE"/>
			<jsp:param value="alRegions" name="ALREGIONS"/>
			<jsp:param value="alFieldSales" name="ALFIELDSALES"/>
			</jsp:include>
	    <span style="float: left;display: inline-block;padding-top: 38px;margin-right: 15px;">
		<fmtLoanerExtApproval:message key="BTN_LOAD" var="varLoad"/>
					<gmjsp:button value="&nbsp;${varLoad}&nbsp;" name="Btn_Load" buttonType="Load" gmClass="Button" onClick="javascript:fnLoad();" /> 
					</span></td>
		</tr>
       <tr> <td colspan="5" class="Line" height="1"></td></tr> 
		<tr>
			<td  colspan="5" width="100%">
				<div id="dataGridDiv"  height="300px" width="1450px"></div>
				 <div id="pagingArea" height="300px" width="1450px"></div>	
			</td>
		</tr>
		<tr> <td colspan="5" class="Line" height="1"></td></tr>
		<tr class="ShadeRightTableCaption" >
	      <td  id="cmts" colspan="5" style="font-size:9pt;height:24px;"><div id="comments" ><font color="red">*</font>&nbsp;<fmtLoanerExtApproval:message key="LBL_COMMENTS"/></div></td>
        </tr>
        <div id="hideline1">
	    <tr><td height="1" colspan="5" class="Line" ></td></tr>	</div>	
	    <tr height="60" id="hideTextarea">
		    <td colspan="5"><input type="hidden" name="hShowCam" value="">&nbsp;<textarea id="strApprovalComments" name="strApprovalComments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
			onBlur="changeBgColor(this,'#ffffff');"  rows=3 cols=100></textarea></td>
	     </tr>
		 <div id="hideline2">
			<tr><td colspan="5" class="Line" height="1"></td></tr></div>
			<tr height="30" id="hideButtons">
				<td style="padding-left:600px" align="center" colspan="5" height="10">
					 <fmtLoanerExtApproval:message key="BTN_APPROVE" var="varApprove"/>
					 <gmjsp:button value="${varApprove}" name="Btn_Approve" buttonType="Save" gmClass="Button" onClick="javascript:fnApproval();" />&nbsp;&nbsp;
					 <fmtLoanerExtApproval:message key="BTN_REJECT" var="varReject"/>
					 <gmjsp:button value="${varReject}" name="Btn_Reject" buttonType="Save" gmClass="Button" onClick="javascript:fnReject();" />
				</td>
			</tr>
			<tr ><td colspan="5" align="center"><div  id="hideExcel" class='exportlinks'><fmtLoanerExtApproval:message key="DIV_EXPORT_OPT"/>: <img src='img/ico_file_excel.png' onclick="fnExport();"/>&nbsp;<a href="#" onclick="fnExport();"> <fmtLoanerExtApproval:message key="DIV_EXCEL"/>
			</a></div></td></tr>
			<tr><td colspan="5" align="center"  class="RegularText"><div  id="DivNothingMessage"><fmtLoanerExtApproval:message key="LBL_NOTHING_FOUND_DISPLAY"/></div></td></tr>		
	</table>
</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</script>
</HTML>