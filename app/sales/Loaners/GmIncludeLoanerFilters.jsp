<!--sales\Loaners\GmIncludeLoanerFilters.jsp  -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@page import="org.apache.commons.beanutils.PropertyUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="fmtLoanerExtFilters" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmtLoanerExtFilters:requestEncoding value="UTF-8" />
<fmtLoanerExtFilters:setLocale value="<%=strLocale%>"/>
<fmtLoanerExtFilters:setBundle basename="properties.labels.sales.Loaners.GmLoanerExtApproval"/>
<%

String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));

String strArrayListZone ="";
String strArrayListRegion ="";
String strArrayListFS ="";
String strZoneHtml = "";
String strRegionHtml = "";
String strDistHtml= "";
ArrayList alZone = new ArrayList();
ArrayList alRegions = new ArrayList();
ArrayList alDistributor = new ArrayList();
if(!strFormName.equals(""))
{ 
	Object objTemp = pageContext.getRequest().getAttribute(strFormName);
	strArrayListZone = GmCommonClass.parseNull(request.getParameter("ALZONE"));
	alZone  = GmCommonClass.parseNullArrayList((ArrayList)PropertyUtils.getProperty(objTemp,strArrayListZone));
	
	strArrayListRegion = GmCommonClass.parseNull(request.getParameter("ALREGIONS"));
	alRegions  = GmCommonClass.parseNullArrayList((ArrayList)PropertyUtils.getProperty(objTemp,strArrayListRegion));
	
	strArrayListFS = GmCommonClass.parseNull(request.getParameter("ALFIELDSALES"));
	alDistributor  = GmCommonClass.parseNullArrayList((ArrayList)PropertyUtils.getProperty(objTemp,strArrayListFS));
	log.debug("alDistributor==="+alDistributor);
	int intZoneByCompDiv = 0;
	int intRegnByZone = 0;
	HashMap hmLoop = new HashMap();
	int intDistSize=0;
	
	if (alZone != null) {
		intZoneByCompDiv = alZone.size();
	}
	if (alRegions != null) {
		intRegnByZone = alRegions.size();
	}
	if (alDistributor != null) {
		intDistSize = alDistributor.size();
	}
	// Zone	
	hmLoop = new HashMap();
	if (intZoneByCompDiv > 0) {
	  for (int i = 0; i < intZoneByCompDiv; i++) {
	    hmLoop = (HashMap) alZone.get(i);
	    strZoneHtml +=
	        "ZAr[" + i + "] = new Array(\"" + hmLoop.get("CODEID") + "\",\"" + hmLoop.get("CODENM")
	            + "\",\"" + hmLoop.get("COMPDIVID") + "\",\"" + hmLoop.get("COMPID") + "\");";
	  }
	}
	
	// Region	
	hmLoop = new HashMap();
	if (intRegnByZone > 0) {
	  for (int i = 0; i < intRegnByZone; i++) {
	    hmLoop = (HashMap) alRegions.get(i);
	    strRegionHtml +=
	        "RAr[" + i + "] = new Array(\"" + hmLoop.get("CODEID") + "\",\"" + hmLoop.get("CODENM")
	            + "\",\"" + hmLoop.get("ZONEID") + "\",\"" + hmLoop.get("COMPID") + "\");";
	  }
	}
	//Distributor
	hmLoop = new HashMap();
		if (intDistSize > 0) {
		  for (int i = 0; i < intDistSize; i++) {
		    hmLoop = (HashMap) alDistributor.get(i);
		    strDistHtml +=
		        "DAr[" + i + "] = new Array(\"" + hmLoop.get("ID") + "\",\"" + hmLoop.get("NM")
		            + "\",\"" + hmLoop.get("PID") + "\",\"" + hmLoop.get("COMPID") + "\");";

		  }
		} 
}
 
%>
<html>
<head>
<script>
		var ZAr = new Array(1);
		var RAr = new Array(1);
		var DAr = new Array(1);
		
		var frmNm = '<%= strFormName%>';
	    var frmObj = eval("document."+frmNm);
		<%=strZoneHtml%>
		<%=strRegionHtml%>
		<%=strDistHtml%>
		var  vFilterShowFl = false; 
</script>
<input type="hidden" name="hZoneInnerHtml" value="">
<input type="hidden" name="hRegnInnerHtml" value="">
<input type="hidden" name="hDistInnerHtml" value="">
</head>
<body>
<div class="divSpan">
			<span class="lblSpan"><fmtLoanerExtFilters:message key="LBL_ZONE" />:</span>
			<div style="width: 200px;float: left; height: 130px;">
			<input class="RightInput" type="checkbox" name="Chk_Grp_ALL_Zone" value="" onclick="javascript:fnSelectAll('Zone');">
			<span class="RightTextAS" style="position: relative;top: -2px;">Select All</span>
			<DIV class='gmFilter' id='Div_Zone'  ><ul style="list-style-type: none; padding: 0; margin: 0;">
			<li><input class='RightInput' type='checkbox' name='Chk_GrpZone'></li></ul></DIV>
			</div>
			</div>	
			<div class="divSpan"><span class="lblSpan"><fmtLoanerExtFilters:message key="LBL_REGION" />:</span>
			<div style="width: 200px;float: left; height: 130px;">
			<input class="RightInput" type="checkbox" name="Chk_Grp_ALL_Regn" value="" onclick="javascript:fnSelectAll('Regn');">
			<span class="RightTextAS" style="position: relative;top: -2px;">Select All</span>
			<DIV class='gmFilter' id='Div_Regn'><ul style="list-style-type: none; padding: 0; margin: 0;">
			<li><input class='RightInput' type='checkbox' name='Chk_GrpRegn'></li></ul></DIV>&nbsp;</div></div>
			<div  class="divSpan"><span class="lblSpan"><fmtLoanerExtFilters:message key="LBL_FIELD_SALES" />:</span>
			<div style="width: 200px;float: left; height: 130px;">
			<input class="RightInput" type="checkbox" name="Chk_Grp_ALL_Dist" value="" onclick="javascript:fnSelectAll('Dist');">
			<span class="RightTextAS" style="position: relative;top: -2px;">Select All</span>
			<DIV class='gmFilter' id='Div_Dist'><ul style="list-style-type: none; padding: 0; margin: 0;">
			<li><input class='RightInput' type="checkbox" name="Chk_GrpDist"/></li></ul></DIV>&nbsp;</div></div>

</body>
</html>
<script>
	if (document.all.Div_Zone){
		frmObj.hZoneInnerHtml.value = document.all.Div_Zone.innerHTML;
	}
	if (document.all.Div_Regn){
		frmObj.hRegnInnerHtml.value = document.all.Div_Regn.innerHTML;
	}
	if (document.all.Div_Dist){
		frmObj.hDistInnerHtml.value = document.all.Div_Dist.innerHTML;
	}
</script>