
<%
	/**********************************************************************************
	 * File		 		: GmSalesRunRateReport.jsp
	 * Desc		 		: This screen is used for the 
	 * Version	 		: 1.0
	 * author			: Joe P Kumar
	 ************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtSalesRunRateReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!--sales\GmSalesRunRateReport.jsp  -->
<fmtSalesRunRateReport:setLocale value="<%=strLocale%>"/>
<fmtSalesRunRateReport:setBundle basename="properties.labels.sales.GmSalesRunRateReport"/>

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel"%>
<%@ page import="com.globus.common.beans.GmCrossTabFormat"%>
<%@ page import="java.util.ArrayList,java.util.HashMap"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<bean:define id="strSortColumn" name="frmSalesRunRateReportForm"  property="hsortColumn" type="java.lang.String"></bean:define>
<bean:define id="strSortOrder" name="frmSalesRunRateReportForm"  property="hsortOrder" type="java.lang.String"></bean:define>
<bean:define id="strMonthRR" name="frmSalesRunRateReportForm"  property="monthRR" type="java.lang.String"></bean:define>
<bean:define id="strMonthRRColumn" name="frmSalesRunRateReportForm"  property="xmonthRRColumn" type="java.lang.String"></bean:define>
<bean:define id="hmReturn" name="frmSalesRunRateReportForm"  property="hmReportData" type="java.util.HashMap"></bean:define>
<bean:define id="strOpt" name="frmSalesRunRateReportForm"  property="strOpt" type="java.lang.String"></bean:define>

<%
    String strsalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");
	//Below code is to track down the visits on the report.
	String strUserId = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
	String strShUserName = GmCommonClass.parseNull((String)session.getAttribute("strSessShName"));
	String strUserName = strUserId + " - " + strShUserName;
	String strWikiTitle = GmCommonClass.getWikiTitle("EPICRR_REPORT");
	//Visit code ends
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Additional Inventory Quota Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strsalesJsPath%>/GmSalesFilter.js"></script>
<script> downloadPiwikJS();</script>
<script> fnTracker('Sales Rep Performance', '<%=strUserName%>');</script>
<script>
function fnGo(haction){
	//document.frmSalesRunRateReportForm.haction.value='report';
	fnReloadSales(haction,null);
	//document.frmSalesRunRateReportForm.submit();
}

function fnSort(varColumnName, varSortOrder)
{	
	document.frmSalesRunRateReportForm.hsortColumn.value = varColumnName;
	document.frmSalesRunRateReportForm.hsortOrder.value = varSortOrder;
	fnGo('<%=strOpt%>');
}

// This selects all the checkboxes for each of the varControl.
// Also When the VarControl is Zone, it selects all the area and if the varControl is FieldSalesType, it selects all the designation
function fnSelectAll(varControl,varControlToSelect,varCmd)
{	
				objSelAll = varControl;
				//alert(varControl.name);
				if (objSelAll ) {
				//objCheckSiteArr = document.all.checkFieldSales;
				var objCheckSiteArr = eval("document.frmSalesRunRateReportForm."+varControlToSelect);

				if ((objSelAll.checked || varCmd == 'selectAll') && objCheckSiteArr)
				{
					objSelAll.checked = true;
					objCheckSiteArr.checked = true;
					objCheckSiteArrLen = objCheckSiteArr.length;
					for(i = 0; i < objCheckSiteArrLen; i ++) 
					{	
						objCheckSiteArr[i].checked = true;
					}
				}
				else if (!objSelAll.checked  && objCheckSiteArr ){
					objCheckSiteArrLen = objCheckSiteArr.length;
					objCheckSiteArr.checked = false;
					for(i = 0; i < objCheckSiteArrLen; i ++) 
					{	
						objCheckSiteArr[i].checked = false;
					}
				}
				}

				if (varControl.name == 'selectAllZone'){
					document.frmSalesRunRateReportForm.selectAllArea.checked = document.frmSalesRunRateReportForm.selectAllZone.checked;
					fnSelectAll(document.frmSalesRunRateReportForm.selectAllArea,'checkArea','toggle');
				}

				if (varControl.name == 'selectAllFieldSalesType'){
					document.frmSalesRunRateReportForm.selectAllRepDesignation.checked = document.frmSalesRunRateReportForm.selectAllFieldSalesType.checked;
					fnSelectAll(document.frmSalesRunRateReportForm.selectAllRepDesignation,'checkRepDesignation','toggle');
				}

	}

// This toggles the "Select All" checkbox. If one of the checkboxes is off, then the "select all" is checked off
function fnSelectAllToggle( varControl, varControlToSelect)
	{
	var objCheckSiteArr = eval("document.frmSalesRunRateReportForm."+varControlToSelect);
	var objControl = eval("document.frmSalesRunRateReportForm."+varControl);
	var blShoulcheck = true; 	

	objCheckSiteArrLen = objCheckSiteArr.length;

	if(!objCheckSiteArr.checked && !objCheckSiteArrLen){
		objControl.checked = false;
		blShoulcheck = false;
	}
	
		for(i = 0; i < objCheckSiteArrLen; i ++) 
			{	
				if(!objCheckSiteArr[i].checked){
					objControl.checked = false;
					blShoulcheck = false;
				}
			}

			if(blShoulcheck){
				objControl.checked = true;
			}
	}


// Load method - this selects all the components by default
function fnLoad(){
	var varhaction = document.frmSalesRunRateReportForm.haction.value;

	if (varhaction != 'report'){
		fnSelectAll(document.frmSalesRunRateReportForm.selectAllZone,'checkZone','selectAll');
		fnSelectAll(document.frmSalesRunRateReportForm.selectAllArea,'checkArea','selectAll');
		fnSelectAll(document.frmSalesRunRateReportForm.selectAllFieldSalesType,'checkFieldSalesType','selectAll');
		fnSelectAll(document.frmSalesRunRateReportForm.selectAllRepDesignation,'checkRepDesignation','selectAll');
	}
	
	fnSelectRepDesignation(document.frmSalesRunRateReportForm.checkFieldSalesType[0]);
}	

// When a Zone is passed, this selects the corresponding Regions
function fnSelectRegion(objZoneId){

	var varZoneId = objZoneId.value;
	var objCheckAreaArr = document.frmSalesRunRateReportForm.checkArea;
	var varCheckAreaArrLen = objCheckAreaArr.length;

	//alert(objZoneId.checked + " varZoneId " + varZoneId);

	for(i = 0; i < varCheckAreaArrLen; i ++) 
		{	
			var varArea = objCheckAreaArr[i];
			var split = new Array();
			split = varArea.alt.split('_');
			if (split[0] == varZoneId && objZoneId.checked){
				varArea.checked = true;
			}	
			else if (split[0] == varZoneId && !objZoneId.checked){
				varArea.checked = false;
			}

		}
		// below () to toggle the "select All" of Zone and Area
	fnToggleSelectAreaZone();
	
}

// When the "select all" of area is clicked, "select all" of Zone has to be modified.
function fnSelectAllArea(){
		document.frmSalesRunRateReportForm.selectAllZone.checked = document.frmSalesRunRateReportForm.selectAllArea.checked;
		fnSelectAll(document.frmSalesRunRateReportForm.selectAllZone,'checkZone','toggle');
}

// When a region is selected, corresponding Zone has to be selected (if all the regions of tat zone are checked)
function fnDeSelectZone(varRegionId){

	var split = new Array();
	split = varRegionId.alt.split('_');
	var varZoneFromRegion = split[0];

	// Look through all regions to see if all the regions for this zone are checked. If so check it.
	var objCheckAreaArr = document.frmSalesRunRateReportForm.checkArea;
	var varCheckAreaArrLen = objCheckAreaArr.length;
	var blShoulcheck = true;
	
	for(i = 0; i < varCheckAreaArrLen; i ++) 
		{	
			var varArea = objCheckAreaArr[i];
			var split = new Array();
			split = varArea.alt.split('_');
//			alert(' varArea. ' +varArea.alt+ ' status ' +varArea.checked);
			if (split[0] == varZoneFromRegion && !varArea.checked){
				blShoulcheck = false;
			}	
		}

	//alert(blShoulcheck);			
	var objCheckZoneArr = document.frmSalesRunRateReportForm.checkZone;
	var varCheckZoneArrLen = objCheckZoneArr.length;
	// For single zone
	if(!varCheckZoneArrLen){
		if (!blShoulcheck){
			objCheckZoneArr.checked = false;
		}
		else{
			objCheckZoneArr.checked = true;
		}
	}

	
//	alert(varZoneFromRegion + " varRegionId.checked " +varRegionId.checked);
	for(i = 0; i < varCheckZoneArrLen; i ++) 
		{	
//		alert(objCheckZoneArr[i].value);
			if (varZoneFromRegion == objCheckZoneArr[i].value && !blShoulcheck){
				objCheckZoneArr[i].checked = false;
			}	
			else if (varZoneFromRegion == objCheckZoneArr[i].value && blShoulcheck){
				objCheckZoneArr[i].checked = true;
			}
		}
			fnToggleSelectAreaZone();
}

// When a fs type is clicked, the designation has to be selected
function fnSelectRepDesignation(objFieldSalesType){
	var varFieldSalesTypeId = objFieldSalesType.value;
	var varFieldSalesTypeStatus = objFieldSalesType.checked;
	// alert(varFieldSalesTypeId + " status " + varFieldSalesTypeStatus);
	var objFieldSalesTypeArr = document.frmSalesRunRateReportForm.checkFieldSalesType;
	var isDistributorRepEnabled = false;
	var isDirectRepEnabled = false;

	for(i = 0; i < objFieldSalesTypeArr.length; i ++) 
		{	
			var varFieldSales = objFieldSalesTypeArr[i];
			// distributor rep shud be checked and direct rep shud be unchecked
			if (varFieldSales.value == 4020 && varFieldSales.checked){
				isDistributorRepEnabled = true;
			}	
			
			if (varFieldSales.value == 4021 && varFieldSales.checked){
				isDirectRepEnabled = true;
				}
		}

	var objCheckRepDesignationArr = document.frmSalesRunRateReportForm.checkRepDesignation;
	var varCheckRepDesignationArrLen = objCheckRepDesignationArr.length;
	var objSelectAllDesignation = document.frmSalesRunRateReportForm.selectAllRepDesignation;
	var varcheckedrepdesignationcount = 0;
	// alert(objZoneId.checked + " varZoneId " + varZoneId);
	
		for(i = 0; i < varCheckRepDesignationArrLen; i ++) 
			{	
				var varRepDesignation = objCheckRepDesignationArr[i];
				
				if (varRepDesignation.checked){
					varcheckedrepdesignationcount++;
				}
				
				if(varFieldSalesTypeId == 4020 && varFieldSalesTypeStatus && isDistributorRepEnabled && !isDirectRepEnabled){
					varRepDesignation.disabled = true;
					objSelectAllDesignation.disabled = true;
					}
				else if (varFieldSalesTypeId == 4021 && varFieldSalesTypeStatus){
					varRepDesignation.disabled = false;
					objSelectAllDesignation.disabled = false;
					varRepDesignation.checked = true;
					}
				else if(varFieldSalesTypeId == 4020 && !varFieldSalesTypeStatus && isDirectRepEnabled){
					varRepDesignation.disabled = false;
					objSelectAllDesignation.disabled = false;
				}
				else if (varFieldSalesTypeId == 4021 && !varFieldSalesTypeStatus){
					varRepDesignation.checked = false;
					varRepDesignation.disabled = true;
					objSelectAllDesignation.disabled = true;
				}
			}
	fnToggleSelectFieldSalesRepDesignation();
	
	// If not all designation is checked, then onload we dont select any Field sales type
	if(varcheckedrepdesignationcount !=  varCheckRepDesignationArrLen){
	fnDeselectFieldSalesType();
	}
}


// When FS is unchecked, designation has to be changed accordingly
function fnDeselectFieldSalesType(){
	
	fnSelectAllToggle('selectAllRepDesignation','checkRepDesignation');
	var varSelectAllRepDesignation = document.frmSalesRunRateReportForm.selectAllRepDesignation.checked;
	var objFieldSalesTypeArr = document.frmSalesRunRateReportForm.checkFieldSalesType;
	var isDirectRepEnabled = false;

	for(i = 0; i < objFieldSalesTypeArr.length; i ++) 
		{	
			var varFieldSales = objFieldSalesTypeArr[i];

			if (varFieldSales.value == 4021 && varSelectAllRepDesignation){
				varFieldSales.checked = true;
			}	
			
			if (!varSelectAllRepDesignation){
				varFieldSales.checked = false;
			}
		}

	fnSelectAllToggle('selectAllFieldSalesType','checkFieldSalesType');
}

function fnToggleSelectAreaZone(){
	fnSelectAllToggle('selectAllZone','checkZone');
	fnSelectAllToggle('selectAllArea','checkArea');
	}

function fnToggleSelectFieldSalesRepDesignation(){
	fnSelectAllToggle('selectAllFieldSalesType','checkFieldSalesType');
	fnSelectAllToggle('selectAllRepDesignation','checkRepDesignation');
}


</script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>

<html:form action="/gmSalesRunReportDAction.do?method=salesRepRunRate">
    
	<html:hidden property="strOpt" />
	<html:hidden property="adId" />
	<html:hidden property="adName" />
	<html:hidden property="vpId" />
	<html:hidden property="vpName" />
	<html:hidden property="hfromPage" />
	<html:hidden property="haction" />
	<html:hidden property="fieldSales" value="" />
	<html:hidden property="fieldSalesName" value="" />
	<html:hidden property="hsortColumn"/>
	<html:hidden property="hsortOrder"/>
	<html:hidden property="hAction"  value="" />
	
<!-- Custom tag lib code modified for JBOSS migration changes -->	
	<BODY leftmargin="20" topmargin="10" onload="fnLoad();" >
	<table border="0" class="DtTable1000" width="800" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="6">
				<table border="0"  width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" class="RightDashBoardHeader" colspan="4"><fmtSalesRunRateReport:message key="LBL_SALESREP"/></td>
						<td  height="25" class="RightDashBoardHeader">
						<fmtSalesRunRateReport:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="Line" height="1" colspan="4"></td>
		</tr>
		<tr>
			<td colspan="10">
			<jsp:include page="/sales/GmSalesFilters.jsp" >
				<jsp:param name="FRMNAME" value="frmSalesRunRateReportForm" />
				<jsp:param name="HACTION" value="<%=strOpt%>"/>
				<jsp:param name="HIDE" value="SYSTEM" />				
			</jsp:include>
			</td>
		</tr>		
		
			<tr width = 850 height="18" bgcolor="#eeeeee" class="RightTableCaption">					
					<td width="250" align="center"><fmtSalesRunRateReport:message key="LBL_FEILDSALES"/></td>
					<td width="250" align="center"><fmtSalesRunRateReport:message key="LBL_REPDESIGN"/></td>
					<td width="250" align="center"></td>
					<td width="250" align="center"></td>					
					<td> </td>
				</tr>
				<tr>					
					<td width="250">
					<DIV
						style="display: visible; height: 100px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 8px; margin-right: 8px;">
					<table cellspacing="0" cellpadding="0">
						<tr bgcolor="gainsboro">
							<td><html:checkbox property="selectAllFieldSalesType" onclick="fnSelectAll(this,'checkFieldSalesType','toggle');" /><B>&nbsp;<fmtSalesRunRateReport:message key="LBL_SELECTALL"/> </B></td>
						</tr>
					</table>	
					<table cellspacing="0" cellpadding="0">
						<logic:iterate id="alFieldSalesType" name="frmSalesRunRateReportForm" property="alFieldSalesType">
							<tr>
								<td><htmlel:multibox property="checkFieldSalesType" value="${alFieldSalesType.CODEID}" onclick="fnSelectRepDesignation(this);"/> 
								<bean:write name="alFieldSalesType" property="CODENM" /></td>
							</tr>
						</logic:iterate>
					</table>
					</DIV>	
					<BR>				
					</td>
					<td width="250">
					<DIV
						style="display: visible; height: 100px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 8px; margin-right: 8px;">
					<table cellspacing="0" cellpadding="0">
						<tr bgcolor="gainsboro">
							<td><html:checkbox property="selectAllRepDesignation" onclick="fnSelectAll(this,'checkRepDesignation','toggle');" /><B>&nbsp;<fmtSalesRunRateReport:message key="LBL_SELECTALL"/></B></td>
						</tr>
					</table>	
					<table cellspacing="0" cellpadding="0">
						<logic:iterate id="alRepDesignation" name="frmSalesRunRateReportForm" property="alRepDesignation">
							<!-- When the Rep Designation is : Territory Assistant, AD, ASS do not show the option. Done for PMT-8802  -->
							<logic:notEqual name="alRepDesignation" property="CODEID" value="70120" >
								<logic:notEqual name="alRepDesignation" property="CODEID" value="70115" >
									<logic:notEqual name="alRepDesignation" property="CODEID" value="70119" >
							<tr>
								<td><htmlel:multibox property="checkRepDesignation" value="${alRepDesignation.CODEID}" onclick="fnDeselectFieldSalesType();" /> 
								<bean:write name="alRepDesignation" property="CODENM" /></td>
							</tr>
									</logic:notEqual>		
								</logic:notEqual>		
							</logic:notEqual>	
						</logic:iterate>
					</table>
					</DIV>	
					<BR>				
					</td>
				</tr>
			</td>
		</tr>
		<tr height="24">
				<td colspan="4">
					<table cellSpacing="0" cellPadding="0"  border="0">
					<tr height="20">
						<TD class=RightTableCaption align="right" ><fmtSalesRunRateReport:message key="LBL_SALES_DATE"/>&nbsp;</TD>
						<TD class=RightText ><gmjsp:dropdown controlName="salesFromMonth"  SFFormName='frmSalesRunRateReportForm' SFSeletedValue="salesFromMonth"
				       	SFValue="alMonthList" width="105"	 codeId="CODENMALT" codeName="CODENM"  defaultValue= "[Choose One]"/>&nbsp;&nbsp;<gmjsp:dropdown controlName="salesFromYear"  SFFormName='frmSalesRunRateReportForm' SFSeletedValue="salesFromYear"
						SFValue="alYearList" width="105" 	 codeId="CODENM" codeName="CODENM"  defaultValue= "[Choose One]"/></TD>
						<TD class=RightTableCaption align="center"><fmtSalesRunRateReport:message key="LBL_TO_DATE"/>&nbsp;</TD>
						<TD class=RightText ><gmjsp:dropdown controlName="salesToMonth"  SFFormName='frmSalesRunRateReportForm' SFSeletedValue="salesToMonth"
						SFValue="alMonthList"  width="105"	 codeId="CODENMALT" codeName="CODENM"  defaultValue= "[Choose One]" />&nbsp;&nbsp;<gmjsp:dropdown controlName="salesToYear"  SFFormName='frmSalesRunRateReportForm' SFSeletedValue="salesToYear"
						SFValue="alYearList"  width="105"	 codeId="CODENM" codeName="CODENM"  defaultValue= "[Choose One]" />&nbsp;&nbsp;&nbsp;&nbsp;</TD>	
						<td class="RightTableCaption">&nbsp;<html:checkbox property="showInActiveRepsFlag" /><fmtSalesRunRateReport:message key="LBL_SHOW_INACTIVE"/>                                                                  
    		            </td>
					</TR>
					</table>
				</td>
		</tr>		
					<%
						String strBtnClick="javascript:fnGo('"+strOpt+"');";
					%>
		<tr height="24">
				<td colspan="4">
					<table cellSpacing="0" cellPadding="0"  border="0">
					<tr height="20">
						<TD class=RightTableCaption align="right" ><fmtSalesRunRateReport:message key="LBL_HIRE_DATE"/>&nbsp;</TD>
						<TD class=RightText ><gmjsp:dropdown controlName="hireFromMonth"  SFFormName='frmSalesRunRateReportForm' SFSeletedValue="hireFromMonth"
				       	SFValue="alMonthList" width="105"	 codeId="CODENMALT" codeName="CODENM" defaultValue= "[Choose One]" />&nbsp;&nbsp;<gmjsp:dropdown controlName="hireFromYear"  SFFormName='frmSalesRunRateReportForm' SFSeletedValue="hireFromYear"
						SFValue="alYearList" width="105" 	 codeId="CODENM" codeName="CODENM" defaultValue= "[Choose One]" /></TD>
						<TD class=RightTableCaption align="center" ><fmtSalesRunRateReport:message key="LBL_TO_DATE"/>&nbsp;</TD>
						<TD class=RightText ><gmjsp:dropdown controlName="hireToMonth"  SFFormName='frmSalesRunRateReportForm' SFSeletedValue="hireToMonth"
						SFValue="alMonthList"  width="105"	 codeId="CODENMALT" codeName="CODENM" defaultValue= "[Choose One]" />&nbsp;&nbsp;<gmjsp:dropdown controlName="hireToYear"  SFFormName='frmSalesRunRateReportForm' SFSeletedValue="hireToYear"
						SFValue="alYearList"  width="105"	 codeId="CODENM" codeName="CODENM" defaultValue= "[Choose One]" />&nbsp;&nbsp;&nbsp;&nbsp;</TD>	
                        <td class="RightTableCaption">&nbsp;
    		               <html:text property="monthRR"  size="3" maxlength="2" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>  &nbsp;<fmtSalesRunRateReport:message key="LBL_MONTH_RR"/>
    		               &nbsp;&nbsp;&nbsp;&nbsp;<fmtSalesRunRateReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" onClick="<%=strBtnClick%>" gmClass="button" buttonType="Load" />                                                                 
    		            </td>
					</TR>
					</table>
				</td>
		</tr>		
		
<% 
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	
	//To Generate the Crosstab report
	gmCrossTab.setHeader("Sales By Distributor By Month");
	
	// Setting Display Parameter
	gmCrossTab.setAmountWidth(70);
	gmCrossTab.setNameWidth(400);
	gmCrossTab.setRowHeight(21);
	gmCrossTab.setColumnWidth("Territory",200);
	
	// Line Style information
	gmCrossTab.setColumnDivider(false);
	gmCrossTab.setColumnLineStyle("borderDark");
	
	// Setting Line before print
	gmCrossTab.addLine("Name");
	gmCrossTab.addLine("Territory");
	gmCrossTab.addLine("Type");
	gmCrossTab.addLine("Date of Hire");
	gmCrossTab.addLine("Total");
	gmCrossTab.addLine("Last Month<BR> Sales RR");
	gmCrossTab.addLine(" MM 10-12 RR");
	gmCrossTab.addLine(strMonthRRColumn);
	gmCrossTab.addLine("Last 3 Mon RR");
	
	gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD");

	gmCrossTab.addStyle("Name", "ShadeMedGrayTD", "ShadeDarkGrayTD");
	gmCrossTab.addStyle("Territory", "ShadeMedGrayTD", "ShadeDarkGrayTD");
	gmCrossTab.addStyle("Type", "ShadeMedGrayTD", "ShadeDarkGrayTD");
	gmCrossTab.addStyle("Date of Hire", "ShadeMedGrayTD", "ShadeDarkGrayTD");

	gmCrossTab.addStyle("Total", "ShadeMedBlueTD", "ShadeDarkBlueTD");

	gmCrossTab.addStyle("Last Month<BR> Sales RR", "ShadeMedBrownTD", "ShadeDarkBrownTD");
	gmCrossTab.addStyle(" MM 10-12 RR", "ShadeLightBrownTD", "ShadeDarkBrownTD");
	gmCrossTab.addStyle(strMonthRRColumn, "ShadeMedGreenTD", "ShadeDarkGreenTD");
	gmCrossTab.addStyle("Last 3 Mon RR", "ShadeLightGreenTD", "ShadeDarkGreenTD");

	// Sort Functionality 
	gmCrossTab.setSortRequired(true);
	gmCrossTab.addSortType("Name", "String");

	int intSortColumn = strSortColumn.equals("") ? -1 : Integer.parseInt(strSortColumn);
	int intSortOrder = Integer.parseInt(GmCommonClass.parseZero(strSortOrder));
	gmCrossTab.setSortColumn(intSortColumn);
	gmCrossTab.setSortOrder(intSortOrder);
	gmCrossTab.addSortType("Territory","String");
	gmCrossTab.addSortType("Type","String");
	gmCrossTab.addSortType("Date of Hire","Date");
	gmCrossTab.displayZeros(true);
	gmCrossTab.setHideColumn("Last Month<BR> Sales RR");
	//gmCrossTab.setValueType(0); -- so that we dont divide by 1000
	
	// Loop through and round off all the columns from 4 (Date of Hire till total
	ArrayList alHeader = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("Header"));
	int endIndex = 16; // safe initalization to end of total
	for (int k = 0; k < alHeader.size();k++){
            			if (alHeader.get(k).equals("Total")){
            				endIndex = k;
            				break;
            			}
            		}
	
	for (int i = 4; i <= endIndex; i++){
		String strColumnName = (String)alHeader.get(i);
		gmCrossTab.add_RoundOff(strColumnName, "0");
	}
	
	gmCrossTab.add_RoundOff("Total", "0");
	gmCrossTab.add_RoundOff("Last 3 Mon RR", "0");
	
	gmCrossTab.setRowHighlightRequired(true);

	gmCrossTab.setExport(true, pageContext, "/gmSalesRunReportDAction.do?method=salesRepRunRate", "SalesRepPerformanceReport");
	
%>		
		<tr>
			<td colspan="4" align=center><%=gmCrossTab.PrintCrossTabReport(hmReturn)%></td>
		</tr>
		<tr>
			<td class="RightTableCaption" colspan="4" align=left><font color="red">*</font> <fmtSalesRunRateReport:message key="LBL_ALL_SALES"/> </td>
		</tr>		
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
