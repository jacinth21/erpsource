
<%
	/**********************************************************************************
	 * File		 		: GmVSetReport.jsp
	 * Desc		 		: This screen is used to display Set Rep By Account now modified 
	 *					  to display group report
	 * Version	 		: 1.1
	 * author			: RichardK
	 ************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.HashMap"%>
<%@ page import="com.globus.common.beans.GmCrossTabFormat"%>

<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtVsetReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmVSetReport.jsp -->
<fmtVsetReport:setLocale value="<%=strLocale%>"/>
<fmtVsetReport:setBundle basename="properties.labels.custservice.ProcessRequest.GmLoanerProcessRequest"/>
<%
try {
GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	HashMap hmReturn = new HashMap();
	ArrayList alProjList = new ArrayList();
	ArrayList alDrillDown = new ArrayList();


		GmServlet gm = new GmServlet();
				gm.checkSession(response, session);
		String strHtmlPath = GmCommonClass.getString("GMHTML");

	String strSelected = "";
	String strCodeID = "";
	String strProjNm = "";
	String strProjId = "";
	
	String strShowArrow = GmCommonClass.parseZero((String) request.getAttribute("Chk_ShowArrowFl"));
	String strShowPercent = GmCommonClass.parseZero((String) request.getAttribute("Chk_ShowPercentFl"));
	String strHideAmount = GmCommonClass.parseZero((String) request.getAttribute("Chk_HideAmountFl"));
	
	// Combobox default value
	String strHeader = (String) request.getAttribute("strHeader");
	String strFilterID = (String) request.getAttribute("Cbo_Name");
	String strType = (String) request.getAttribute("Cbo_Type");
	String strSetID = GmCommonClass.parseNull((String) request.getAttribute("hSetNumber"));
	String strDistributorID = GmCommonClass.parseNull((String) request.getAttribute("hDistributorID"));
	String strAD = GmCommonClass.parseNull((String) request.getAttribute("hAreaDirector"));
	String strLoanerType = GmCommonClass.parseNull((String) request.getAttribute("Cbo_LoanerType"));
	String strLoanerTypeDisable = GmCommonClass.parseNull((String) request.getAttribute("LOANERTYPEDISABLED"));
	String strShowAI = GmCommonClass.parseNull((String) request.getAttribute("SHOWAI")).equals("Y")?"CHECKED":"";
	String strBaselineName = GmCommonClass.parseNull((String) request.getAttribute("HBASELINENAME"));
	
	//When strTurnsLabel/strBreakUpLabel get changed then the GmSalesConsignReportBean and GmSalesSystemDecorator has to be changed.
	String strBreakUpLabel = "Case<BR>BreakUp";
	String strTurnsLabel = GmCommonClass.parseNull((String) request.getAttribute("HTURNSLABEL"));
	//------
	log.debug(" show AI is " +strShowAI);
	String strShowAIDisable = GmCommonClass.parseNull((String) request.getAttribute("SHOWAIDISABLED"));
	
	strFilterID = (strFilterID == null) ? "0" : strFilterID; // If no value selected will set the combo box value to [Choose One]
	String strhAction = (String) request.getAttribute("hAction");
	ArrayList alSaleTypes = (ArrayList) request.getAttribute("alSaleTypes");
	ArrayList alConsignmentTypes = (ArrayList) request.getAttribute("alConsignmentTypes");
	ArrayList alStatus = (ArrayList) request.getAttribute("alStatus");
	ArrayList alTurns = (ArrayList) request.getAttribute("alTurns");
	ArrayList alLoanerType = (ArrayList) request.getAttribute("alLoanerType");
	
	String strSalesConsignType = GmCommonClass.parseNull((String) request.getAttribute("strSalesConsign"));
	String strStatus = GmCommonClass.parseNull((String) request.getAttribute("strStatus"));
	String strTurns = GmCommonClass.parseNull((String) request.getAttribute("strTurns"));
	strType = (strType == null || "50400".equals(strSalesConsignType)) ? "1" : "0"; // Setting combo box value
	
	HashMap hcboVal = new HashMap();
	int intSize = 0;
	int intSortColumn;
	int intSortOrder;
	
	String strSalesConsign = GmCommonClass.parseNull((String) request.getAttribute("strSalesConsign"));
	String strSaleType = GmCommonClass.parseNull((String) request.getAttribute("strSaleType"));
	
	intSortColumn = request.getParameter("hSortColumn") == null ? -1 : Integer.parseInt((String) request.getParameter("hSortColumn"));
	intSortOrder = Integer.parseInt(GmCommonClass.parseZero((String) request.getParameter("hSortOrder")));
	
	if (strhAction == null) {
		strhAction = (String) session.getAttribute("hAction");
	}
	
	strhAction = (strhAction == null) ? "Load" : strhAction;
	
	// To get the Project Combo Box value information 
	HashMap hmComonValue = (HashMap) request.getAttribute("hmComonValue");
	if (hmComonValue != null) {
		alProjList = (ArrayList) hmComonValue.get("PROJECTS");
	}

%>
<HTML>
<HEAD>
<TITLE>Globus Medical: YTD #</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
</HEAD>
<script>
function fnHelp()
{
  windowOpener("<%=strHtmlPath%>/GmHelp.html");	
}

function fnSubmit()
{
	document.frmOrder.submit();
}

function fnGo()
{
  fnClear();  
  fnReloadSales('<%=strhAction%>','<%=strServletPath%>/GmSalesConsignmentServlet');
 //document.frmOrder.action = "<%=strServletPath%>/GmSalesConsignmentServlet";
 //document.frmOrder.submit();

}

function fnCallAccount(val,Type )
{  
	url='<%=strServletPath%>/GmSalesYTDReportServlet?';
	var ShowType = 	Type.substring(Type.length,Type.length -1 );
	if (ShowType == 'A')
	{
		document.frmOrder.hAction.value = "LoadAccountA";	
	}else {
		document.frmOrder.hAction.value = "LoadAccount";
	}

	if (Type == 'LoadDCONS') 
	{
		//alert('Inside the loop');
		document.frmOrder.DistributorID.value = val;
		  url='<%=strServletPath%>/GmSalesYTDReportServlet?hDistributorID='+val;
	}
	else if(Type == "LoadAD")
	{
	    document.frmOrder.hAreaDirector.value = val;
		// PC-5196 - The data could not be fetched since the AD Id sent to the backend was incorrect(<Region ID>-<AD ID>). 
		// Its failing wherever the AD used on "Where" condition. 
		// In this PC, the AD Id and Region Id have been split and only the AD Id is being passed to the server.
		
	      var adVal = val;
	      var indexOfVal = val.indexOf("-");
	      if(indexOfVal >= 0){
	          var splitVal = val.split("-");
	          adVal = splitVal[1].trim();
	      }

	      console.log("fnCallAccount >>>>>>>" + adVal);
	    url='<%=strServletPath%>/GmSalesYTDReportServlet?hAreaDirector='+adVal;	
	      
	}
	else
	{
		document.frmOrder.SetNumber.value = val;
		  url='<%=strServletPath%>/GmSalesYTDReportServlet?SetNumber='+val;	
	} 
	  
	url=url+'&hAction='+document.frmOrder.hAction.value;
	
	fnReloadSales('',url);
}


function fnCallRepDetails(val, Type)
{  
	url='<%=strServletPath%>/GmSalesYTDReportServlet?';
	var ShowType = 	Type.substring(Type.length,Type.length -1 );
	if (ShowType == 'A')
	{
		document.frmOrder.hAction.value = "LoadRepA";	
	}else {
		document.frmOrder.hAction.value = "LoadRep";
	}

	if (Type =='LoadDCONS') 
	{
		document.frmOrder.DistributorID.value = val;
		  url='<%=strServletPath%>/GmSalesYTDReportServlet?hDistributorID='+val;
	}
	else if(Type == "LoadAD")
	{
	    document.frmOrder.hAreaDirector.value = val;	
	      var adVal = val;
	      var indexOfVal = val.indexOf("-");
	      if(indexOfVal >= 0){
	          var splitVal = val.split("-");
	          adVal = splitVal[1].trim();
	      }
	      console.log("fnCallRepDetails >>>>>>>" + adVal);
	    url='<%=strServletPath%>/GmSalesYTDReportServlet?hAreaDirector='+adVal;		  
	}
	else
	{
		document.frmOrder.SetNumber.value = val;
		 url='<%=strServletPath%>/GmSalesYTDReportServlet?SetNumber='+val;	
	}
  
	url=url+'&hAction='+document.frmOrder.hAction.value;
	fnReloadSales('',url);
}

function fnCallDisDetails(val, Type)
{ 
	var ShowType = 	Type.substring(Type.length,Type.length -1 );
	if (ShowType == 'A')
	{
		document.frmOrder.hAction.value = "LoadDistributorA";	
	}else {
		document.frmOrder.hAction.value = "LoadDistributor";
	}


	document.frmOrder.SetNumber.value = val;
	//document.frmOrder.action ="<%=strServletPath%>/GmSalesYTDReportServlet";
	//document.frmOrder.submit();
	url='<%=strServletPath%>/GmSalesYTDReportServlet?&hAction='+document.frmOrder.hAction.value;
	fnReloadSales('',url);
}

function fnCallPart(val, Type)
{ 
	var ShowType = 	Type.substring(Type.length,Type.length -1 );
	if (ShowType == 'A')
	{
		document.frmOrder.hAction.value = "LoadPartA";	
	}else {
		document.frmOrder.hAction.value = "LoadPart";
	}


	document.frmOrder.SetNumber.value = val;
	//document.frmOrder.action ="<%=strServletPath%>/GmSalesYTDReportServlet";
	//document.frmOrder.submit();
	url='<%=strServletPath%>/GmSalesYTDReportServlet?&hAction='+document.frmOrder.hAction.value;
	fnReloadSales('',url);
}

// Filter by distributor consignment 
function fnCallDisp(val, Type)
{ 
	document.frmOrder.hAction.value = "LoadPart";
	fnSetValue (val,Type );
//	document.frmOrder.SetNumber.value = val;
	//document.frmOrder.action ="<%=strServletPath%>/GmSalesConsignDetailsServlet";
	//document.frmOrder.submit();
	url='<%=strServletPath%>/GmSalesConsignDetailsServlet?&hAction='+document.frmOrder.hAction.value;
	fnReloadSales('',url);
}

// Filter by distributor consignment 
function fnCallCons(val, Type)
{   
	url='<%=strServletPath%>/GmSalesConsignmentServlet?';
	document.frmOrder.hAction.value = "LoadDCONS";

	if(Type == "LoadAD")
    {
      document.frmOrder.hAreaDirector.value = val;   
      var adVal = val;
      var indexOfVal = val.indexOf("-");
      if(indexOfVal >= 0){
          var splitVal = val.split("-");
          adVal = splitVal[1].trim();
      }
      console.log("fnCallCons >>>>>>>" + adVal);
      url='<%=strServletPath%>/GmSalesConsignmentServlet?hAreaDirector='+adVal;         
    }
		else if(Type == "LoadVP")
		{
	      document.frmOrder.hVpRegionID.value = val;
	      url='<%=strServletPath%>/GmSalesConsignmentServlet?hVpRegionID='+val;			  
		} else {
	  	   document.frmOrder.SetNumber.value = val;
	  	 url='<%=strServletPath%>/GmSalesConsignmentServlet?SetNumber='+val;	
		}
	 
		url=url+'&hAction='+document.frmOrder.hAction.value;
		fnReloadSales('',url);
}

// Filter by Group (For the selected distributor) 
function fnCallSetNumber(val, Type)
{ 
	url='<%=strServletPath%>/GmSalesConsignmentServlet?';
	document.frmOrder.hAction.value = "LoadGCONS";

		if(Type == "LoadAD")
		{
	      document.frmOrder.hAreaDirector.value = val;	

	      var adVal = val;
	      var indexOfVal = val.indexOf("-");
	      if(indexOfVal >= 0){
	          var splitVal = val.split("-");
	          adVal = splitVal[1].trim();
	      }
	      console.log("fnCallSetNumber >>>>" + adVal);
	      url='<%=strServletPath%>/GmSalesConsignmentServlet?hAreaDirector='+adVal;			  
		} 
		else if(Type == "LoadVP")
		{
	      document.frmOrder.hVpRegionID.value = val;	
	      url='<%=strServletPath%>/GmSalesConsignmentServlet?hVpRegionID='+val;			  
		} else {
	  	  document.frmOrder.DistributorID.value = val;
	  	url='<%=strServletPath%>/GmSalesConsignmentServlet?DistributorID='+val;	
		}
  
		url=url+'&hAction='+document.frmOrder.hAction.value;
		fnReloadSales('',url);
}

// Consignment details 
function fnCallConsD(val, Type)
{ 
	//document.frmOrder.DistributorID.value = val;
	varSalesType = document.frmOrder.Cbo_SalesType.value;
	
	fnSetValue (val,Type );
	//document.frmOrder.action ="<%=strServletPath%>/GmSalesVirtualCDetailServlet";
	//document.frmOrder.hAction.value = "LoadCons";
	//document.frmOrder.hAction.value = "CheckSharedSet";
	//document.frmOrder.submit();
	url='<%=strServletPath%>/GmSalesVirtualCDetailServlet?&hAction=CheckSharedSet';
	fnReloadSales('',url);
}

function fnCallDetail(val, Type)
{ 
	document.frmOrder.SetNumber.value = val;
	//document.frmOrder.action = "<%=strServletPath%>/GmSalesYTDReportServlet";
	//document.frmOrder.hAction.value = "LoadAccount";
	//document.frmOrder.submit();
	url='<%=strServletPath%>/GmSalesYTDReportServlet?hAction=LoadAccount&SetNumber'+val;
	fnReloadSales('',url);
}

function fnCallAD(val, Type)
{ 
	url='<%=strServletPath%>/GmSalesConsignmentServlet?';
	if (Type == 'LoadVP'){
		document.frmOrder.hVpRegionID.value = val;	
		  url='<%=strServletPath%>/GmSalesConsignmentServlet?hVpRegionID='+val;		
	}
	else{
		document.frmOrder.SetNumber.value = val;
		url='<%=strServletPath%>/GmSalesConsignmentServlet?SetNumber='+val;	
	}
	url=url+'&hAction=LoadAD';
	fnReloadSales('',url);
}

function fnSetValue (val,Type )
{
	switch (Type) 
	{
		case 'LoadGCONS':  
			document.frmOrder.SetNumber.value = val;
			break;
		case 'LoadDCONS' :  
			document.frmOrder.DistributorID.value = val;
			break;
	}	
	
}

function fnClear()
{
var action = document.frmOrder.hAction.value;
	var distID = document.frmOrder.DistributorID.value;
	
	if(action == 'LoadGCONS')
	{	 
       document.frmOrder.SetNumber.value="";	
	}
	else if(action == 'LoadDCONS')
	{	   
	   document.frmOrder.DistributorID.value="";
	}
	else if(action == 'LoadAD')
	{	   
	   document.frmOrder.hAreaDirector.value="";
	}
}
function fnSort(varColumnName, varSortOrder)
{	
	document.frmOrder.hSortColumn.value = varColumnName;
	document.frmOrder.hSortOrder.value = varSortOrder;
	fnGo();
}

function fnLoad()
{
	//alert("Set No : "+document.frmOrder.SetNumber.value +" , Dist: "+document.frmOrder.DistributorID.value+" ,  AD: "+document.frmOrder.hAreaDirector.value);
	
	document.frmOrder.Cbo_FromMonth.value = '<%=request.getAttribute("Cbo_FromMonth")%>';
	document.frmOrder.Cbo_ToMonth.value = '<%=request.getAttribute("Cbo_ToMonth")%>';
	document.frmOrder.Cbo_FromYear.value = '<%=request.getAttribute("Cbo_FromYear")%>';
	document.frmOrder.Cbo_ToYear.value = '<%=request.getAttribute("Cbo_ToYear")%>';
	
	fnChangeLoanerTypeStatus();
}

function fnDownloadVer()
{
	//hact = document.frmOrder.hAction.value;
	//hFromMonth = document.frmOrder.Cbo_FromMonth.value;
	//hFromYear = document.frmOrder.Cbo_FromYear.value;
	//hToMonth = document.frmOrder.Cbo_ToMonth.value;
	//hToYear = document.frmOrder.Cbo_ToYear.value;
	//cShowArrowFl =  document.frmOrder.Chk_ShowArrowFl.checked;
	//cShowPercentFl = document.frmOrder.Chk_ShowPercentFl.checked; 
	//cHideAmount = document.frmOrder.Chk_HideAmountFl.checked;
	//hDistributorID = document.frmOrder.DistributorID.value;

	// Default selected value
	//hCName = document.frmOrder.Cbo_Name.value;
	//hType = document.frmOrder.Cbo_Type.value;
	//cboName = 	document.frmOrder.Cbo_Name[document.frmOrder.Cbo_Name.selectedIndex].text;
	
	//windowOpener("<%=strServletPath%>/GmSetYTDReportServlet?hExcel=Excel<%=GmCommonClass.getWindowOperner(request,response).replaceAll("'","").replaceAll("\n","") %>","INVEXCEL","resizable=yes,scrollbars=yes,top=300,left=200,width=800,height=490");
	//windowOpener("<%=strServletPath%>/GmSetYTDReportServlet?hExcel=Excel&hAction="+hact+"&Cbo_FromMonth="+hFromMonth+"&Cbo_FromYear="+hFromYear+"&Cbo_ToMonth="+hToMonth+"&Cbo_ToYear="+hToYear+"&Chk_ShowArrowFl="+cShowArrowFl+"&Chk_ShowPercentFl="+cShowPercentFl+"&Chk_HideAmountFl="+cHideAmount+"&Cbo_Name="+hCName+"&Cbo_Type="+hType+"&DistributorID="+hDistributorID+"&hcboName="+cboName,"INVEXCEL","resizable=yes,scrollbars=yes,top=300,left=200,width=800,height=490");
}

function fnViewAIQuotaDrillDown(varId,varName){
	var action = document.frmOrder.hAction.value;
	if(action == 'LoadAD')
	{	 
		windowOpener("/gmAIQuotaReportAction.do?method=quarterlyQuotaDrillDownByAD&haction=LoadAD&&adId="+varId+"&adName="+varName,"QUOTA","resizable=yes,scrollbars=yes,top=300,left=100,width=800,height=490");	
	}
	else if(action == 'LoadVP')
	{
		windowOpener("/gmAIQuotaReportAction.do?method=quarterlyQuotaDrillDownByAD&haction=LoadVP&vpId="+varId+"&vpName="+varName,"QUOTA","resizable=yes,scrollbars=yes,top=300,left=100,width=800,height=490");
	}
	
	
} 

// Not being used due to tabs. Will be removed later
function fnChangeLoanerTypeStatus(){

	varSalesType = document.frmOrder.Cbo_SalesType.value;

	if (varSalesType == 50301){
		document.frmOrder.Cbo_LoanerType.disabled  = false;
		document.frmOrder.Cbo_LoanerType.value = 70131;
		document.frmOrder.Chk_ShowAI.checked = false;
		document.frmOrder.Chk_ShowAI.disabled = true;
	}
	else if (varSalesType == 50300){
		document.frmOrder.Chk_ShowAI.disabled = false;
		document.frmOrder.Cbo_LoanerType.disabled  = true;
	}
}

// Not being used due to tabs. Will be removed later
function fnValidateLoanerStatus(varLoanerType){
	varSalesType = document.frmOrder.Cbo_SalesType.value; 
	
	if (varSalesType == 50301 && varLoanerType == 0){
		//Error_Details("  If the <B>Sales Type</B> is Loaner, Please choose one of the valid <B>Loaner Type</B>  <LI> <i>Loaner Sets </i></LI>  <LI> <i>Field Loaner Usage</i></LI>  ");
		Error_Details(message[10186]);
	}
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			document.frmOrder.Cbo_LoanerType.value = 70131;
			return false;
	}
}


function fnLoad()
{
	//alert("Set No : "+document.frmOrder.SetNumber.value +" , Dist: "+document.frmOrder.DistributorID.value+" ,  AD: "+document.frmOrder.hAreaDirector.value);
	if(parent.dhxWins != undefined){
		document.getElementById("Bt_Close").style.visibility="visible";	
	}else{
		document.getElementById("Bt_Close").style.visibility="hidden";
	}
	document.frmOrder.Cbo_FromMonth.value = '<%=request.getAttribute("Cbo_FromMonth")%>';
	document.frmOrder.Cbo_ToMonth.value = '<%=request.getAttribute("Cbo_ToMonth")%>';
	document.frmOrder.Cbo_FromYear.value = '<%=request.getAttribute("Cbo_FromYear")%>';
	document.frmOrder.Cbo_ToYear.value = '<%=request.getAttribute("Cbo_ToYear")%>';
	//document.frmOrder.Cbo_Type.value = '<%=strType%>';
	document.frmOrder.hAction.value='<%=strhAction%>';
	
}

document.onkeypress = function(){
if(event.keyCode == 13){
	fnGo();
	}
}
function fnClose(){
	parent.dhxWins.window("w1").close();	
}

</script>
<BODY  onLoad="javascript:fnLoad()">
<FORM name="frmOrder" method="post" action="<%=strServletPath%>/GmSetYTDReportServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hAreaDirector" value="<%=request.getAttribute("hAreaDirector")%>"> 
<input type="hidden" name="DistributorID" value="<%=request.getAttribute("hDistributorID")%>"> 
<input type="hidden" name="hAccountID" value="<%=request.getAttribute("hAccountID")%>">
<input type="hidden" name="SetNumber" value="<%=request.getAttribute("hSetNumber")%>"> 
<input type="hidden" name="hRepID"	value="<%=request.getAttribute("hRepID")%>"> 
<input type="hidden" name="hPartNumber" value="<%=request.getAttribute("hPartNumber")%>"> 
<input type="hidden" name="hTerritory" value="<%=request.getAttribute("hTerritory")%>"> 
<input type="hidden" name="hVpRegionID"	value="<%=request.getAttribute("hVpRegionID")%>"> 
<input type="hidden" name="Cbo_SalesType"	value="<%=request.getAttribute("strSaleType")%>">
<input type="hidden" name="hSortColumn" value="<%=intSortColumn%>"> 
<input	type="hidden" name="hSortOrder" value="<%=intSortOrder%>"> 
<input type="hidden" name="Cbo_Status" value="0">
<input type="hidden" name="hFromPage" value="FIELDINVENTORYREPORT">


<TABLE cellSpacing=0 cellPadding=0 border=0 width="978">
	<TBODY>
		<tr class=Line>
			<td noWrap height=1></td>
		</tr>
		<tr>
			<td colspan="2">
			<table cellSpacing=0 cellPadding=0 border=0 width="100%">
				<TR height=30>
				<!-- Custom tag lib code modified for JBOSS migration changes -->
					<td class="RightTableCaption" align="right" width="85"><fmtVsetReport:message key="LBL_TYPE"/>:&nbsp;</td>
					<td width="100"><gmjsp:dropdown controlName="Cbo_TypeCon" seletedValue="<%= strSalesConsign %>" tabIndex="1" width="90" value="<%= alConsignmentTypes%>"
						codeId="CODEID" codeName="CODENM" /></td>
					<td class="RightTableCaption" align="right" width="145">&nbsp;<fmtVsetReport:message key="LBL_PERIOD"/>:&nbsp;</td>
					<td width="80"><gmjsp:dropdown controlName="Cbo_Turns" seletedValue="<%= strTurns %>" tabIndex="2" value="<%= alTurns%>" codeId="CODENMALT"
						codeName="CODENM" /></td>
					<td class="RightTableCaption"  align="right" HEIGHT="24" width="115"><fmtVsetReport:message key="LBL_SHOEAIDATA"/>:&nbsp;</td>
				<td >&nbsp;<input type="checkbox" <%=strShowAIDisable%>  name="Chk_ShowAI" <%=strShowAI%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=25></td>
				</TR>
			</TABLE>
			</td>
		</tr>
		<tr height=30>
			<td>
			<table border="0">
				<tr>
				<TD><jsp:include page="/include/GmFromToDateConsign.jsp">
						<jsp:param name="tabStartCount" value="5" />
					</jsp:include></TD>
				</tr>	
			</table>
		</tr>
		<tr>
		<td colspan="7" height="20">
			<jsp:include page="/sales/GmSalesFilters.jsp" >
				<jsp:param name="HIDE" value="SYSTEM" />
				<jsp:param name="FRMNAME" value="frmOrder" />
				<jsp:param name="HACTION" value="<%=strhAction%>" />
				<jsp:param name="URL" value="/GmSalesConsignmentServlet"/>
			</jsp:include> 
		</td>
		</tr>
		<!-- Addition Filter Information -->
		<tr class=borderDark>
			<td noWrap></td>
		</tr>

		<tr>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
			<td><jsp:include page="/sales/GmSalesFilterDetails.jsp" /></td>
		</tr>
		<%
			// To Generate the Crosstab report
				gmCrossTab.setHeader("Sales By Distributor By Month");
				

				// To display percent changed information
				if (strShowPercent.equals("1")) {
					gmCrossTab.setReportType(gmCrossTab.CROSSTAB_SALE_PER);
				}
				if (strShowArrow.equals("1")) {
					gmCrossTab.setShowArrow(true); // To Display % arrow information
				}

				if (strHideAmount.equals("1")) {
					gmCrossTab.setHideAmount(true);
				}

				// Setting the Action to form the link
				gmCrossTab.setLinkType(strhAction);

				// To specify if its unit or valye 
				gmCrossTab.setValueType(Integer.parseInt(strType));

				// Include the column the need to skipped
				gmCrossTab.addSkip_Div_Round("Loaner Sets");
				gmCrossTab.addSkip_Div_Round("Consign Sets");
				gmCrossTab.addSkip_Div_Round("Loaner Usage");
				gmCrossTab.addSkip_Div_Round("Total Sets");
				gmCrossTab.addSkip_Div_Round(strBaselineName);
				gmCrossTab.addSkip_Div_Round("Loaner case in period");
				gmCrossTab.addSkip_Div_Round("Loaner <BR>Utilization %");
				
				//gmCrossTab.addSkip_Div_Round(strBaselineName);
				//gmCrossTab.addSkip_Div_Round("Annualized Value");
				gmCrossTab.addSkip_Div_Round("#Cases<BR> per Month");
				gmCrossTab.addSkip_Div(strTurnsLabel);
				gmCrossTab.addSkip_Div("$ Turns");
				gmCrossTab.addSkip_Div("#C/M/S<br> Target");
				gmCrossTab.addSkip_Div("Turns Target");
				gmCrossTab.addSkip_Div("%Exp Cases");
				gmCrossTab.addSkip_Div("@Target<br>#Sets");
				gmCrossTab.addSkip_Div("Excess<br>#Sets");
				gmCrossTab.addSkip_Div("% Additional");
				gmCrossTab.addSkip_Div("% AAI Budget");
				

				gmCrossTab.addDivMandatory("Annualized Value");
				gmCrossTab.add_RoundOff("Annualized Value", "1");

				gmCrossTab.addDivMandatory("$ Value");
				gmCrossTab.add_RoundOff("$ Value", "1");
				
				gmCrossTab.add_RoundOff("% AAI Budget","1");
				gmCrossTab.add_RoundOff("#Cases<BR> per Month","1");
				gmCrossTab.add_RoundOff("#Cases<BR>/Month", "1");
				gmCrossTab.add_RoundOff(strTurnsLabel, "1");
				gmCrossTab.add_RoundOff("$ Turns", "1");
				gmCrossTab.add_RoundOff("#C/M/S<br> Target", "1");
				gmCrossTab.add_RoundOff("Turns Target", "1");
				gmCrossTab.add_RoundOff("%Exp Cases", "0");
				gmCrossTab.add_RoundOff("@Target<br>#Sets", "0");
				gmCrossTab.add_RoundOff("Excess<br>#Sets", "0");

				// Setting Display Parameter
				gmCrossTab.setAmountWidth(50);
				gmCrossTab.setNameWidth(300);
				gmCrossTab.setRowHeight(18);

				// Line Style information
				gmCrossTab.setColumnDivider(false);
				gmCrossTab.setColumnLineStyle("borderDark");

				// Setting Line before print
				gmCrossTab.addLine("Name");
				gmCrossTab.addLine("Total");
				gmCrossTab.addLine("Total <BR>$Value");
				gmCrossTab.addLine(strTurnsLabel);
				gmCrossTab.addLine("% Additional");
				gmCrossTab.addLine("AAI Budget");
				gmCrossTab.addLine("% AAI Budget");
				gmCrossTab.addLine(strBaselineName);
				gmCrossTab.addLine("Total Sets");
				
				gmCrossTab.setHideColumn("Budget");
				gmCrossTab.setHideColumn("AI Usage<BR>(Quarter)");
				gmCrossTab.setHideColumn("% to <BR>AI Budget");
				gmCrossTab.setHideColumn(strBreakUpLabel);
				if (!strLoanerTypeDisable.equals("disabled") || !strShowAI.equals("CHECKED")){	
				gmCrossTab.setHideColumn("AAS");
				gmCrossTab.setHideColumn("STD");
				gmCrossTab.setHideColumn("AAI");
				gmCrossTab.setHideColumn("% Additional");
				gmCrossTab.setHideColumn("AAI Budget");
				gmCrossTab.setHideColumn("% AAI Budget");				
				}
				if(!strhAction.equals("LoadDCONS")&&!strhAction.equals("LoadAD")&&!strhAction.equals("LoadVP"))
				{
					gmCrossTab.setHideColumn("AAI Budget");
					gmCrossTab.setHideColumn("% AAI Budget");
				}

				//Setting drilldown information 
				/*
				 **********************  The following if coditions has to be consolidated and should be modified **************************************
				 */

				if ((strhAction.equals("LoadGCONS") || strhAction.equals("LoadVP")) && strDistributorID.equals("") && strAD.equals("")) {
					alDrillDown.add("AD");
				}
				if (strDistributorID.equals("") && strSetID.equals("")) {
					alDrillDown.add(strhAction);
				}
				log.debug(" strSalesConsignType " + strSalesConsignType);
				if (strhAction.equals("LoadAD") || strhAction.equals("LoadVP")) {
					alDrillDown.add("LoadGCONS");
					if (strSetID.equals("")) {
						alDrillDown.add("Group");
					}
				}
				// If the consignment type is COGS / the report is for ALL , dont display Account and Rep
				if (!strSalesConsignType.equals("50420") && !strhAction.equals("LoadVP") && !strSaleType.equals("50302")) 
				{
					alDrillDown.add("Account"); // Maps to Account drilldown		
					alDrillDown.add("Rep");
				}
				if (!strSalesConsignType.equals("50420") && !strhAction.equals("LoadAD") && !strhAction.equals("LoadVP") && (!strDistributorID.equals("") || !strSetID.equals(""))&& !strSaleType.equals("50302")) {
					alDrillDown.add("Disp"); // Maps to Disp drilldown
					alDrillDown.add("Cons"); // Maps to Cons drilldown
					alDrillDown.remove(strhAction);
				}

				if (alDrillDown.size() == 0) {
					alDrillDown.add("");
				}
				
				/*
				 *******************************************************************************************************************************
				 */
				 log.debug (" Loabner Type " +strLoanerType);
					// If the Loaner Type is "Loaner Sets" dont display any drill down
				if (!strLoanerType.equals("70130")){
					gmCrossTab.setLinkRequired(true); // To Specify the link
					gmCrossTab.setDrillDownDetails(alDrillDown);
				}

				// To Skip Additional Calculation
				//gmCrossTab.addSkip_Calculation(strBaselineName);
				//gmCrossTab.addSkip_Calculation("Value");
				gmCrossTab.addSkip_Calculation("% Additional");
				//gmCrossTab.addSkip_Calculation("Quota");
				//gmCrossTab.addSkip_Calculation("AI Usage<BR>(Quarter)");
				gmCrossTab.addSkip_Calculation("% to <BR>AI Budget");

				//gmCrossTab.addSkip_Div("Quota");
				//gmCrossTab.addSkip_Div("AI Usage<BR>(Quarter)");
				gmCrossTab.addSkip_Div("% to <BR>AI Budget");
				gmCrossTab.setTotalRequired(false);

				// Setting the cross tab style 
				//gmCrossTab.setGeneralStyle("ShadeLightBlueTD");
				gmCrossTab.setColumnWidth("Baseline Sets <BR>Breakup", 95);
				gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD");
                gmCrossTab.addStyle("Name", "ShadeMedGrayTD", "ShadeDarkGrayTD");
                gmCrossTab.addStyle("Total", "ShadeMedBlueTD", "ShadeDarkBlueTD");	
                gmCrossTab.addStyle("Loaner Sets", "ShadeLightBlueTD", "ShadeDarkBlueTD");
				gmCrossTab.addStyle("Consign Sets", "ShadeMedBlueTD", "ShadeDarkBlueTD");
				gmCrossTab.addStyle("Loaner Usage", "ShadeLightBlueTD", "ShadeDarkBlueTD");
				gmCrossTab.addStyle(strBaselineName, "ShadeMedBlueTD", "ShadeDarkBlueTD");
				gmCrossTab.addStyle("Baseline Sets <BR>Breakup", "ShadeLightBlueTD", "ShadeDarkBlueTD");
				gmCrossTab.addStyle("Total <BR>$Value", "ShadeLightBlueTD", "ShadeDarkBlueTD");
				gmCrossTab.addStyle("#Cases<BR> per Month", "ShadeVeryLightBlueGrayTD", "ShadeDarkBlueTD");
				gmCrossTab.addStyle("Loaner case in period", "ShadeMedBlueGrayTD", "ShadeDarkBlueTD");
				gmCrossTab.addStyle(strTurnsLabel, "ShadeLightBlueGrayTD", "ShadeDarkBlueTD");
				gmCrossTab.addStyle("Loaner <BR>Utilization %", "ShadeMedBlueGrayTD", "ShadeDarkBlueTD");
				// gmCrossTab.addStyle("Annualized Value", "ShadeLightBrownTD", "ShadeDarkBrownTD");
				// gmCrossTab.addStyle("$ Turns", "ShadeLightYellowTD", "ShadeDarkYellowTD");
				// gmCrossTab.addStyle("#C/M/S<br> Target", "ShadeMedGreenTD", "ShadeDarkGreenTD");
				// gmCrossTab.addStyle("Turns Target", "ShadeLightGreenTD", "ShadeDarkGreenTD");
				// gmCrossTab.addStyle("%Exp Cases", "ShadeMedOrangeTD", "ShadeDarkOrangeTD");
				// gmCrossTab.addStyle("@Target<br>#Sets", "ShadeLightOrangeTD", "ShadeDarkOrangeTD");
				// gmCrossTab.addStyle("Excess<br>#Sets", "ShadeLightOrangeTD", "ShadeDarkOrangeTD"); 

				gmCrossTab.addStyle("AAS", "ShadeLightOrangeTD", "ShadeDarkOrangeTD");
				gmCrossTab.addStyle("STD", "ShadeMedOrangeTD", "ShadeDarkOrangeTD");
				gmCrossTab.addStyle("AAI", "ShadeLightOrangeTD", "ShadeDarkOrangeTD");
				gmCrossTab.addStyle("% Additional", "ShadeMedOrangeTD", "ShadeDarkOrangeTD");
                gmCrossTab.addStyle("AAI Budget", "ShadeLightOrangeTD", "ShadeDarkOrangeTD");
				gmCrossTab.addStyle("% AAI Budget","ShadeMedOrangeTD", "ShadeDarkOrangeTD");
				gmCrossTab.addStyle("Budget", "ShadeLightBlueTD", "ShadeDarkBlueTD");
				gmCrossTab.addStyle("AI Usage<BR>(Quarter)", "ShadeMedBlueTD", "ShadeDarkBlueTD");
				gmCrossTab.addStyle("% to <BR>AI Budget", "ShadeLightBlueTD", "ShadeDarkBlueTD");  
                gmCrossTab.setDecorator("com.globus.crosstab.beans.GmSalesSystemDecorator");
                
				// PMT-27002: Sorting always by Name (AD/VP/System/Distributor)  So, remove the Seq No code              
				gmCrossTab.setDefaultSortColumn("Name");

				// Sort Functionality 
				gmCrossTab.setSortRequired(true);
				gmCrossTab.addSortType("Name", "String");
				gmCrossTab.addSortType("Baseline Sets <BR>Breakup","String");
				gmCrossTab.setSortColumn(intSortColumn);
				gmCrossTab.setSortOrder(intSortOrder);
				gmCrossTab.setRowHighlightRequired(true);
				gmCrossTab.displayZeros(true);
				gmCrossTab.setExport(true, pageContext, "/GmSalesConsignmentServlet", "SalesReport", "frmOrder");
				//Hide the Column
				gmCrossTab.setHideColumn("Seq No");
				// gmCrossTab.setHideColumn(strBaselineName);

				hmReturn = (HashMap) request.getAttribute("hmReturn");
				//log.debug(" Table is " +hmReturn);
		%>
		<tr>
			<td align=center><%=gmCrossTab.PrintCrossTabReport(hmReturn)%></td>
		</tr>
		<tr>
			<td colspan="14">
<% out.println(GmCommonClass.parseNull((String) request.getAttribute("CALCULATIONNOTES"))); %>			
			</td>
		</tr>
		<fmtVsetReport:message key="BTN_CLOSE" var="varClose"/>
		<tr><td align="center" colspan="14"> <gmjsp:button controlId="Bt_Close" name="Bt_Close" value="${varClose}" style="width: 5em" gmClass="button" buttonType="Load" onClick="fnClose();" /> &nbsp;</td></tr>
</table>

</FORM>
<%
	} catch (Exception e) {
		e.printStackTrace();
	}
%>
</BODY>
<%@ include file="/common/GmFooter.inc" %>
</HTML>
