<!-- /sales/AreaSet/GmAreaSetPendingReturn.jsp -->

 <%@ include file="/common/GmHeader.inc" %>
 <%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
 
 <%@ taglib prefix="fmtAreaSetPendingReturn" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtAreaSetPendingReturn:setLocale value="<%=strLocale%>"/>
<fmtAreaSetPendingReturn:setBundle basename="properties.labels.sales.AreaSet.GmAreaSetPendingReturn"/>
 <%
 String strSalesAreasetJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_AREASET"); 
 %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
 <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
	<script language="JavaScript" src='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler.js'></script>
	<script language="JavaScript" src='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler_minical.js'></script>
	<script language="JavaScript" src='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler_readonly.js'></script>
	<script language="JavaScript" src='<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxscheduler_tooltip.js'></script>
	<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
	<script language="javascript" src="<%=strSalesAreasetJsPath%>/GmAreaSetShipping.js"></script>
	<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js" type="text/javascript"></script>
	<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js" type="text/javascript"></script>
	<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"  type="text/javascript"></script>
	
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">
<style type="text/css" media="all"> 

.Column {
	FONT-SIZE: 9pt; 
	COLOR: #000000; 
	font-weight: normal;
	border: 1px solid  #d3e2e9;
	FONT-FAMILY: Trebuchet MS, arial, sans-serif;
}

.InnerColumn {
	FONT-SIZE: 9pt; 
	COLOR: #000000; 
	font-weight: normal;
	FONT-FAMILY: Trebuchet MS, arial, sans-serif;
}

.Header { 
	background: #3172AA;
	FONT-SIZE: 9pt; 
	COLOR: #FFFFFF; 
	font-weight: bold;
	FONT-FAMILY: Trebuchet MS, arial, sans-serif;
}

</style>

<script>

var dhxWins,w1;

function fnCalendarCaseInfo(infoid) {
	  
    dhxWins = new dhtmlXWindows();
    dhxWins.enableAutoViewport(true);
   // dhxWins.attachViewportTo("winVP");
    w1 = dhxWins.createWindow("w1", 30, 40, 760, 400);
    w1.setText(message_sales[241]); 
	w1.attachURL("/sales/AreaSet/GmAddInfo.jsp?infoid="+infoid);
}

function popitup(url) {
	newwindow=window.open(url,'name','height=300,width=850');
	if (window.focus) {newwindow.focus()}
	return false;
}


 </script>  
</head>

<body>

<!-- Custom tag lib code modified for JBOSS migration changes -->
<table width="950" border=1  cellpadding="0" cellspacing="0"><tr><td>
<table width="954" border=0  cellpadding="0" cellspacing="0">
  <tr class="RightDashBoardHeader">
    <td height="24" colspan="19"><fmtAreaSetPendingReturn:message key="LBL_PENDING_RETURN"/></td>
  </tr>
  <tr>
    <td height="30" class="RightRedCaption">&nbsp;<fmtAreaSetPendingReturn:message key="LBL_STATUS"/>&nbsp;</td>
    <td class="RightRedCaption"><select name="addressType" id="select" class="RightText" tabindex="0"   onchange="" >
      <option value="90400" selected="selected"><fmtAreaSetPendingReturn:message key="LBL_PENDING_SHIPPING"/></option>
      <option value="90401"><fmtAreaSetPendingReturn:message key="LBL_SHIPPED"/></option>
    </select></td>
    <td class="RightRedCaption">&nbsp;<fmtAreaSetPendingReturn:message key="LBL_TAG"/>&nbsp;</td>
    <td class="RightRedCaption"><fmtAreaSetPendingReturn:message key="LBL_FROM"/> 
      <input name="preference" type="text" class="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" value="" size="12" maxlength="12" />
    <fmtAreaSetPendingReturn:message key="LBL_TO"/>
    <input name="preference2" type="text" class="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" value="" size="12" maxlength="12" /></td>
    <td class="RightRedCaption">&nbsp;<fmtAreaSetPendingReturn:message key="LBL_CARRIER"/>&nbsp;</td>
    <td colspan="14" height="28" class="RightTableCaption"><select name="select" id="select2" class="RightText" tabindex="0"   onchange="" >
      <option value="0" id="0" ><fmtAreaSetPendingReturn:message key="LBL_CHOOSE_ONE"/></option>
      <option value="90400"><fmtAreaSetPendingReturn:message key="LBL_FEDEX"/></option>
    </select></td>
  </tr>
  <tr class="Shade">
    <td width="116" class="RightRedCaption">&nbsp;<fmtAreaSetPendingReturn:message key="LBL_ACTUAL_STATUS"/>&nbsp; </td>
    <td width="173" class="RightRedCaption"><select name="select2" id="select3" class="RightText" tabindex="0"   onchange="" >
      <option value="0" id="0" ><fmtAreaSetPendingReturn:message key="LBL_CHOOSE_ONE"/></option>
      <option value="90400"><fmtAreaSetPendingReturn:message key="LBL_RECEIVED"/></option>
      <option value="90401"><fmtAreaSetPendingReturn:message key="LBL_REJECTED"/></option>
    </select></td>
    <td width="134" class="RightRedCaption">&nbsp;
      <select name="select3" id="select4" class="RightText" tabindex="0"   onchange="" >
        <option value="90401"><fmtAreaSetPendingReturn:message key="LBL_LOAN_START_DATE"/></option>
        <option value="90403"><fmtAreaSetPendingReturn:message key="LBL_LOAN_END_DATE"/></option>
        <option value="90400"><fmtAreaSetPendingReturn:message key="LBL_SURGERY_DATE"/></option>
      </select>      &nbsp;</td>
    <td width="269" class="RightRedCaption"><fmtAreaSetPendingReturn:message key="LBL_FROM"/> 
       	<gmjsp:calendar textControlName="Txt_FromDate" SFFormName='frmAreaSet'   onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
      <fmtAreaSetPendingReturn:message key="LBL_TO"/>
       <gmjsp:calendar textControlName="Txt_ToDate" SFFormName='frmAreaSet'   onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;</td>
    <td width="84" class="RightRedCaption">&nbsp;
      <fmtAreaSetPendingReturn:message key="LBL_LOAD" var="varLoad"/><gmjsp:button name="button" value="${varLoad}" buttonType="Load" onClick="window.location='/sales/AreaSet/GmAreaSetPendingReturn.jsp'"/>&nbsp;</td>
    <td colspan="14" height="34" width="174">&nbsp;</td>
  </tr>
</table>
<table border=0 cellspacing="0" cellpadding="0" width="954" >
  <tr height="52" >
    <td width="103" height="52" class="Header"><div align="center"><fmtAreaSetPendingReturn:message key="LBL_TAG#"/></div></td>
    <td width="112" class="Header"><div align="center"><fmtAreaSetPendingReturn:message key="LBL_SET_DESC"/></div></td>
    <td width="108" class="Header"><div align="center"><fmtAreaSetPendingReturn:message key="LBL_STATUS"/></div></td>
    <td width="184" class="Header"><div align="center"><fmtAreaSetPendingReturn:message key="LBL_CURRENT_LOCATION"/></div></td>
    <td width="72" class="Header"><div align="center"><fmtAreaSetPendingReturn:message key="LBL_LOAN"/> <br /><fmtAreaSetPendingReturn:message key="LBL_START_DATE"/></div></td>
    <td width="72" class="Header"><div align="center"><fmtAreaSetPendingReturn:message key="LBL_LOAN"/> <br /><fmtAreaSetPendingReturn:message key="LBL_END_DATE"/></div></td>
    <td width="241" class="Header"><div align="center"><fmtAreaSetPendingReturn:message key="LBL_SHIP_ADDRESS"/></div></td>
    <td width="62" class="Header"><div align="center"><fmtAreaSetPendingReturn:message key="LBL_ADD_INFO"/></div></td>
  </tr>
  <tr height="74">
    <td width="103" class="Column"><table width="94" >
      <tr>
        <td width="25"><input name="checkbox222" type="checkbox"/></td>
        <td width="6"><img src='<%=strImagePath%>/jpg_icon.jpg' alt="Tag" width='16' height='16' id='imgEdit' style='cursor:hand' title='View Image' onclick="return popitup('/sales/InvManagement/GmViewTagSetImageGallery.jsp')" /></td>
        <td width="41" class="InnerColumn"><fmtAreaSetPendingReturn:message key="LBL_0021283"/></td>
      </tr>
    </table></td>
    <td width="112" class="Column"><fmtAreaSetPendingReturn:message key="LBL_REVEREADDITION"/></td>
    <td width="108" class="Column"><div align="left"><fmtAreaSetPendingReturn:message key="LBL_PENDING_SHIPPING"/></div></td>
    <td rowspan="2" width="184" class="Column"><br />
      <strong><fmtAreaSetPendingReturn:message key="LBL_JOHN_CASTEEL"/></strong><br />
<fmtAreaSetPendingReturn:message key="LBL_1010-S_MAIN_STREET"/>
				<fmtAreaSetPendingReturn:message key="LBL_HARRISONBURG_VIRGINA_22801"/><br />
                <strong><fmtAreaSetPendingReturn:message key="LBL_PH"/></strong><fmtAreaSetPendingReturn:message key="LBL_324_547_8956"/><br />
                <strong><fmtAreaSetPendingReturn:message key="LBL_EMAIL"/>:-</strong><fmtAreaSetPendingReturn:message key="LBL_JCASTEEL@GLOBUS"/>&nbsp;</td>
    <td rowspan="2" width="72" class="Column"><fmtAreaSetPendingReturn:message key="LBL_10_13_2011"/></td>
    <td rowspan="2" width="72" class="Column"><fmtAreaSetPendingReturn:message key="LBL_10_17_2011"/></td>
    <td rowspan="2" height="133" width="241" class="Column">
             <strong><fmtAreaSetPendingReturn:message key="LBL_MIKE_CASTELLNO"/>,</strong><br />
                <fmtAreaSetPendingReturn:message key="LBL_3600JOSEPH"/><br />
                <strong><fmtAreaSetPendingReturn:message key="LBL_PH_324_547_8956"/></strong><br />
          <strong><fmtAreaSetPendingReturn:message key="LBL_EMAIL_MCASTELLAN"/></strong></td>
    <td rowspan="2" height="133" width="62" align="center"  class="Column"><fmtAreaSetPendingReturn:message key="LBL_INFO" var="varInfo"/><gmjsp:button name="button23"  buttonType="Load" value="${varInfo}" onClick="fnCalendarCaseInfo(3)"/></td>
  </tr>
  
  <tr height="59">
    <td width="103" height="59" class="Column"><table width="94" >
      <tr>
        <td width="25"><input name="checkbox23" type="checkbox"/></td>
        <td width="6"><fmtAreaSetPendingReturn:message key="IMG_VIEW_IMAGE" var="varViewImage"/><img src='<%=strImagePath%>/jpg_icon.jpg' alt="Tag" width='16' height='16' id='imgEdit' style='cursor:hand' title='${ViewImage}' onclick="return popitup('/sales/InvManagement/GmViewTagSetImageGallery.jsp')"></td>
        <td width="41" class="InnerColumn"><fmtAreaSetPendingReturn:message key="LBL_00212111"/> </td>
      </tr>
    </table></td>
    <td width="112" class="Column"><fmtAreaSetPendingReturn:message key="LBL_XTEND_SCREW_SET"/></td>
    <td width="108" class="Column"><div align="left"><fmtAreaSetPendingReturn:message key="LBL_PENDING_SHIPPING"/> </div></td>
    </tr>
</table>
</td></tr></table>
<table width="950" cellpadding="0" cellspacing="0">
  <tr>
    <td width="75">&nbsp;</td>
    <td width="75">&nbsp;</td>
    <td width="141">&nbsp;</td>
    <td width="718">&nbsp;</td>
    <td width="120" align="center"> <fmtAreaSetPendingReturn:message key="BTN_HAND_DELIVER" var="varHandDeliver"/><gmjsp:button name="button2"  buttonType="Save" onClick="javascript:confirm('Do you want to ship out the select tags?','Ok','Cancel');" value="${HandDeliver}" /></td>
    <td width="150" height="50" colspan="14" align="center"> <fmtAreaSetPendingReturn:message key="BTN_SHIP_TO" var="varShipTo"/><gmjsp:button name="button3"  buttonType="Save" onClick="window.location='/sales/AreaSet/GmEditShipping.jsp'" value="${varShipTo}" /></td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
