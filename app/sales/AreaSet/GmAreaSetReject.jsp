<!-- \sales\AreaSet\GmAreaSetReject.jsp -->
  
  <%@ include file="/common/GmHeader.inc" %>
  <%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap, java.util.Calendar" %> 
  <%@ page import ="com.globus.common.beans.GmCommonControls"%>
  <%@ page import = "com.globus.sales.areaset.forms.GmAreaSetForm" %>
  
  
<%@ taglib prefix="fmtAreaSetReject" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtAreaSetReject:setLocale value="<%=strLocale%>"/>
<fmtAreaSetReject:setBundle basename="properties.labels.sales.AreaSet.GmAreaSetReject"/>

 <bean:define id="hTxnVal" name="frmAreaSet" property="hTxnVal" type="java.lang.String"> </bean:define>
 <bean:define id="returnList" name="frmAreaSet" property="alResult" type="java.util.List"></bean:define>
 <% 
 String strSalesAreasetJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_AREASET");
 String[] strTotalVal=new String[0];
 String[] strCaseInfo=new String[0];
 HashMap hcboVal =new HashMap(); 
 String strCodeID="",strDistRepID="";
 if (hTxnVal.length()>0)
 {
	
	 strTotalVal=hTxnVal.split("\\|");
		 
 }%>
<HTML>
<HEAD>
 
<TITLE>Globus Medical: Modify Shipping Details</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="GlobusCommonScript.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js" type="text/javascript"></script>
	<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js" type="text/javascript"></script>
	<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"  type="text/javascript"></script>
		<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">
<script>

var dhxWins,w1;
var TotalRows = <%=strTotalVal.length%>;



 </script> 
 <script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="javascript" src="<%=strSalesAreasetJsPath%>/GmRejectShipping.js"></script>
</head>


<body leftmargin="20" topmargin="10" onLoad="fnOnLoad();">

<html:form action="/gmAreaSetShipping.do">

	<html:hidden name="frmAreaSet" property="hTxnVal" />
<html:hidden name="frmAreaSet" property="strOpt" />	
<html:hidden name="frmAreaSet" property="caseID" />	
		
		<table   border="0" cellpadding="0" cellspacing="0" class="DtTable700">
			<tr class="RightDashBoardHeader">
				<td height="25"  colspan="3"><fmtAreaSetReject:message key="LBL_REJECT_SET_DETAILS"/></td>
				<td width="196" height="25" align="right">			</td>
  </tr>
 
			<tr>
				<td colspan="4" height="1" bgcolor="#cccccc"></td>
			</tr>
			<tr>
				<td class="line" colspan="4"></td>
			</tr>
			<tr>
			<td width="351"  align="right" class="RightTableCaption" ><fmtAreaSetReject:message key="LBL_CASE_ID"/>		&nbsp;:&nbsp; 	</td>
				<td height="33" colspan="4" ><bean:write name="frmAreaSet" property="caseID" /></td>
			</tr>
			
			<tr class="Shade">
			<td width="351"  align="right" class="RightTableCaption"><fmtAreaSetReject:message key="LBL_REASON_FOR_REJECTION"/>		&nbsp;:&nbsp;	</td>
			  <!-- Custom tag lib code modified for JBOSS migration changes -->
			  <td height="36" colspan="4" >   <gmjsp:dropdown controlName="status"  SFFormName='frmAreaSet' SFSeletedValue="status"  	
						tabIndex="1"  SFValue="alSetStatus" codeId="CODEID" codeName="CODENM" onChange="fnModify(this.value)" defaultValue= "[Choose One]"/></td>
			</tr>
			
			
			<tr>
				<td colspan="4" align="center">
 


 
 <br>
 
 
 
 
 
 
 

 
<table align="center" border="0" class="DtTable500" id="tblHideComments" style="display:block" cellspacing="0" cellpadding="0">	
		
		
		<tr class="ShadeRightTableCaption" height="30">
		  <td align="center" class="RightTableCaption" width="10%"></td>
		  <td align="center" class="RightTableCaption" width="34%"><fmtAreaSetReject:message key="LBL_ASSIGNED_TAG_ID"/></td>
		  <td align="center" class="RightTableCaption" width="11%"></td>
		  <td align="center" class="RightTableCaption" width="45%"><fmtAreaSetReject:message key="LBL_ACTUAL_TAG_IDS"/></td>
	    </tr>
		<%if(strTotalVal.length>0){
			for(int i=0; i<strTotalVal.length;i++)
			{
				
			strCaseInfo=strTotalVal[i].split("\\^");
			if(strCaseInfo.length>0)
			{%>
			
		<tr class="<%=(i+1)%2==0?"Shade":""%>">
			<td width="10%" align="center"> <input type="checkbox" id="Chk_tagid<%=i%>" name="Chk_tagid<%=i%>" value="<%=strCaseInfo[0] %>"/></td>
			<td width="34%" align="center"><%=strCaseInfo[0] %> </td>
			<input type="hidden" value="<%=strCaseInfo[1] %>"  name="Hdn_shipid<%=i%>"  />
			<input type="hidden" value="<%=strCaseInfo[2] %>"  name="Hdn_refid<%=i%>"  />
			<input type="hidden" value="<%=strCaseInfo[3] %>"  name="Hdn_shipfromid<%=i%>"  />
			<td width="11%" align="center"></td>
		    <td width="45%" align="center">&nbsp;
		  <select name="rTagid<%=i%>"  id="rTagid<%=i%>" class="RightText" onChange="fnSelectTag(this.value,'<%=i%>')">  
		  <option  value="0"> [Choose One]</option>
		  <% int intSize =  returnList.size();
		 
           for (int j=0;j<intSize;j++)
           {
        	   hcboVal = (HashMap)returnList.get(j);
        	   
				strCodeID = (String)hcboVal.get("CODEID");
				strDistRepID = (String)hcboVal.get("DISTREPID");
				if(strCaseInfo[3].equals(strDistRepID)){
				%>
				<option  value="<%=strCodeID%>"> <%=strCodeID%></option>
				
				
				<% }}%>
           </select>
           
                <input name="rtTagid<%=i%>" id="rtTagid<%=i%>" type="text" class="InputArea" onkeypress="return isNumberKey(event);" onFocus="changeBgColor(this,'#AACCE8');" onBlur="fnTextTag(this.value,'<%=i%>');"   size="8" maxlength="8"></td>
		</tr>
		
 	
			<%}
			}
			
		}%>
			 </table>	
			</td>	
			</tr>
			<tr>
				<td colspan="4" height="50" align="center"><fmtAreaSetReject:message key="BTN_SUBMIT" var="varSubmit"/>
				<gmjsp:button controlId="Btn_Submit" value="${varSubmit}" buttonType="Save" onClick="fnGo();" />
				&nbsp;&nbsp;&nbsp;<fmtAreaSetReject:message key="BTN_CLOSE" var="varClose"/>
				<gmjsp:button value="${varClose}" buttonType="Load" onClick="fnClose()" />&nbsp;&nbsp;&nbsp;  
				</td>
		</tr>
  </table>	

	</html:form>
	<%@ include file="/common/GmFooter.inc"%>    
 
</body>
</html>
