  
   <!-- \sales\AreaSet\GmEditShipping.jsp -->
  <%@ include file="/common/GmHeader.inc" %>
  <%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
  <%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap, java.util.Calendar" %> 
  <%@ page import ="com.globus.common.beans.GmCommonControls"%>
  <%@ page import = "com.globus.sales.areaset.forms.GmAreaSetForm" %>
  <bean:define id="hTxnVal" name="frmAreaSet" property="hTxnVal" type="java.lang.String"> </bean:define>
   
  <bean:define id="strOpt" name="frmAreaSet" property="strOpt" type="java.lang.String"> </bean:define>
  <%
  String strSalesAreasetJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_AREASET");
  %>
 
<HTML>
<HEAD>
 
<TITLE>Globus Medical: Modify Shipping Details</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>

<script>

var hTxnVal = '<%=hTxnVal%>';

var strOpt = '<%=strOpt%>';

</script>
<script language="javascript" src="<%=strSalesAreasetJsPath%>/GmEditShipping.js"></script>
</head>


<body leftmargin="20" topmargin="10" >

<html:form action="/gmAreaSetShipping.do">

		<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
			<tr><td class="line" colspan="4"></td></tr>
			<tr><td colspan="2"> 
						<jsp:include page="/gmIncShipDetails.do">
				<jsp:param name="FORMNAME" value="frmShipDetails" />
				<jsp:param name="DISPLAYFL" value="S"/>
				<jsp:param name="strOpt" value="AreaSet"/>
		 		</jsp:include>		
					
				</td></tr>
				
			
				
		</table>

	</html:form>
	    
 
</body>
</html>
