<!-- \sales\AreaSet\GmAreaSetShipping.jsp -->

<%@page import="java.sql.Date"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtAreaSetShipping" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtAreaSetShipping:setLocale value="<%=strLocale%>"/>
<fmtAreaSetShipping:setBundle basename="properties.labels.sales.AreaSet.GmAreaSetShipping"/>




 <bean:define id="returnList" name="frmAreaSet" property="alResult" type="java.util.List"></bean:define>
 <bean:define id="txtFromDate" name="frmAreaSet" property="txtFromDate" type="java.lang.String"></bean:define>
 <bean:define id="txtToDate" name="frmAreaSet" property="txtToDate" type="java.lang.String"></bean:define>
<%
String strSalesAreasetJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_AREASET");
	ArrayList alList = new ArrayList();
	 
	alList = GmCommonClass.parseNullArrayList((ArrayList) returnList);
	 
	int totalRows = alList.size();
	
	String strSessApplDateFmt=(String)request.getSession().getAttribute("strSessApplDateFmt");
	
	%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>AreaSet</title>


<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>	
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>


<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
 <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
	<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js" type="text/javascript"></script>
	<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js" type="text/javascript"></script>
	<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"  type="text/javascript"></script>
	<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>

var dhxWins,w1;
var TotalRows = <%=totalRows%>;



 </script> 
<script language="javascript" src="<%=strSalesAreasetJsPath%>/GmAreaSetShipping.js"></script>
</head>



 <logic:equal name="frmAreaSet" property="popup" value="close">
 <body leftmargin="20" topmargin="10" onload="fnClose();"></body>
</logic:equal>
 <logic:equal name="frmAreaSet" property="popup" value="">
<body leftmargin="20" topmargin="10">



<html:form action="/gmAreaSetShipping.do?method=load" >
<html:hidden name="frmAreaSet" property="hTxnVal" />
<html:hidden name="frmAreaSet" property="strOpt" />
<html:hidden name="frmAreaSet" property="caseID" />

<!-- Custom tag lib code modified for JBOSS migration changes -->
<table  cellpadding="0" cellspacing="0" class="DtTable950">
  <tr><td>
<table  border=0  cellpadding="0" cellspacing="0">
  <tr>
    <td height="24" colspan="19" class="RightDashBoardHeader"><bean:write name="frmAreaSet" property="type" /></td>
    <td align="right" class=RightDashBoardHeader>
				<fmtAreaSetShipping:message key="IMG_HELP" var="varHelp"/><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${Help}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("AREASET_FLOW")%>');" />
	</td>
  </tr>
  <tr>
    <td class="RightRedCaption" align="right">&nbsp;<fmtAreaSetShipping:message key="LBL_STATUS"/>&nbsp;:&nbsp;</td>
    <td class="RightRedCaption"><gmjsp:dropdown controlName="status"  SFFormName='frmAreaSet' SFSeletedValue="status"  	
						tabIndex="1"  SFValue="alSetStatus" codeId="CODEID" codeName="CODENM" onChange="" defaultValue= "[Choose One]"/></td>
    <td class="RightRedCaption" align="right"><fmtAreaSetShipping:message key="LBL_TAG"/>&nbsp;:&nbsp; <fmtAreaSetShipping:message key="LBL_FROM"/></td>
    <td class="RightRedCaption"> 
      &nbsp; <html:text name="frmAreaSet" property="tagFrom"  onkeypress="return isNumberKey(event);" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"  size="8" maxlength="8" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtAreaSetShipping:message key="LBL_TO"/> 
    &nbsp;  <html:text name="frmAreaSet" property="tagTo"  onkeypress="return isNumberKey(event);" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"  size="8" maxlength="8" /></td>
    <td class="RightRedCaption" align="right"><fmtAreaSetShipping:message key="LBL_CARRIER"/> :&nbsp;</td>
    <td colspan="14" class="RightRedCaption"><gmjsp:dropdown controlName="carrier"  SFFormName='frmAreaSet' SFSeletedValue="carrier"  	
						tabIndex="1"  SFValue="alShipCarr" codeId="CODEID" codeName="CODENM" onChange="" defaultValue= "[Choose One]"/></td>
  </tr>
  <tr><td class="Line" height="1" colspan="20"></td></tr>
  <tr class="Shade">
    <td width="133" class="RightRedCaption" align="right">&nbsp;<fmtAreaSetShipping:message key="LBL_ACCEPT_STATUS"/>&nbsp;:&nbsp; </td>
    <td width="126" class="RightRedCaption">
      <gmjsp:dropdown controlName="actualStatus"  SFFormName='frmAreaSet' SFSeletedValue="actualStatus"  	
						tabIndex="1"  SFValue="alSetActualStatus" codeId="CODEID" codeName="CODENM" onChange="" defaultValue= "[Choose One]"/></td>
    <td width="156" class="RightRedCaption" align="right">
      <gmjsp:dropdown controlName="dates"  SFFormName='frmAreaSet' SFSeletedValue="dates"  	
						tabIndex="1"  SFValue="alDates" codeId="CODEID" codeName="CODENM" onChange="" defaultValue= "[Choose One]"/>&nbsp;:&nbsp;</td>
    <td width="305" class="RightRedCaption"><fmtAreaSetShipping:message key="LBL_FROM"/>
       	&nbsp;
       	<gmjsp:calendar textControlName="txtFromDate" SFFormName='frmAreaSet'  textValue="<%=(txtFromDate.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(txtFromDate,strSessApplDateFmt).getTime())))%>"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
      <fmtAreaSetShipping:message key="LBL_TO"/>
      &nbsp; <gmjsp:calendar textControlName="txtToDate" SFFormName='frmAreaSet' textValue="<%=(txtToDate.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(txtToDate,strSessApplDateFmt).getTime())))%>"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;</td>
    <td class="RightRedCaption" width="97"><fmtAreaSetShipping:message key="BTN_LOAD" var="varLoad"/><gmjsp:button name="button" style="width: 5em" buttonType="Load" value="${Load}" onClick="return fnLoad()"/></td>
    <td colspan="16" height="26" width="133">&nbsp;</td>
  </tr>
</table>

</td>
</tr>
<tr><td class="Line" height="1" colspan="19"></td></tr>
<tr >
  <td >
		<display:table name="requestScope.frmAreaSet.alResult" class="its"
			id="currentRowObject" requestURI="/gmAreaSetShipping.do?method=load" style="height:35" export="false"  decorator="com.globus.sales.areaset.displaytag.beans.DTASPendingShippingWrapper">
				<fmtAreaSetShipping:message key="LBL_TAG_#" var="varTag"/><display:column property="TAGID" title="${varTag}" style="width:95" sortable="true" />							
				<fmtAreaSetShipping:message key="LBL_SET_DESC" var="varSetDesc"/><display:column property="SETDESC" title="${varSetDesc}"  style="width:120" sortable="true" />
				<fmtAreaSetShipping:message key="LBL_STATUS" var="varStatus"/><display:column property="STATUS" title="${varStatus}"  style="width:80" sortable="true" />
				
				<logic:notEqual name="frmAreaSet" property="strOpt" value="11403">
				<fmtAreaSetShipping:message key="LBL_CURRENT_LOCATION" var="varCurrentLocation"/><display:column property="CURR_LOCATION" title="${CurrentLocation}" style="width:206" class="alignleft" sortable="true"/>
				</logic:notEqual>
				
				<logic:equal name="frmAreaSet" property="strOpt" value="11403">
				<fmtAreaSetShipping:message key="LBL_CURRENT_LOCATION" var="varCurrentLocation"/><display:column property="CURR_LOCATION" title="${CurrentLocation}" style="width:406" class="alignleft" sortable="true"/>
				</logic:equal>
				
				
				<fmtAreaSetShipping:message key="LBL_START_DATE" var="varStartDate"/><display:column property="STARTDATE" title="${varStartDate}" style="width:70" class="alignright" sortable="true" />							
				<fmtAreaSetShipping:message key="LBL_END_DATE" var="varEndDate"/><display:column property="ENDDATE" title="${varEndDate}" style="width:70" class="alignright"  sortable="true" />
				
				<logic:equal name="frmAreaSet" property="strOpt" value="11400">
				
				<fmtAreaSetShipping:message key="LBL_SHIP_ADDRESS" var="varShipAddress"/><display:column property="SHIPADD" title="${ShipAddress}" style="width:206" class="alignleft" sortable="true"  />
				
				</logic:equal>
				
				<logic:equal name="frmAreaSet" property="strOpt" value="11402">
				
				<fmtAreaSetShipping:message key="LBL_SHIP_ADDRESS" var="varShipAddress"/><display:column property="SHIPADD" title="${ShipAddress}" style="width:206" class="alignleft" sortable="true"  />
				
				</logic:equal>
				
				<logic:equal name="frmAreaSet" property="strOpt" value="11401">
				
				<fmtAreaSetShipping:message key="LBL_RETURN_ADDRESS" var="varReturnAddress"/><display:column property="RETURNADD" title="${ReturnAddress}" style="width:206" class="alignleft" sortable="true"  />
				
				</logic:equal>

				
				<fmtAreaSetShipping:message key="LBL_MORE_INFO" var="varMoreInfo"/><display:column property="SOURCE" title="${MoreInfo}" style="width:50" class="alignright"  sortable="true" />
				
		
		</display:table> 
  </td>
</tr>
					
  	
 <tr >
  <td >

<table height="50" width="950" cellpadding="0" cellspacing="0">
  <tr>

    <logic:equal name="frmAreaSet" property="strOpt" value="11400">
     <td align="right">
       <fmtAreaSetShipping:message key="BTN_HAND_DELIVER" var="varHandDeliver"/><gmjsp:button name="button2"  onClick="return fnStatusUpdate(' shipout ')" value="${varHandDeliver}" buttonType="Save" />&nbsp;
       <fmtAreaSetShipping:message key="BTN_SHIP_TO" var="varShipTo"/><gmjsp:button name="button3"  onClick="return fnShipTo()" value="${varShipTo}" buttonType="Save" />&nbsp;
     </td>
    </logic:equal>
    <logic:equal name="frmAreaSet" property="strOpt" value="11402">
      <td align="right">
        <fmtAreaSetShipping:message key="BTN_HAND_DELIVER" var="var"/><gmjsp:button name="button2"  onClick="return fnStatusUpdate(' shipout ')" value="${varHandDeliver}" buttonType="Save" />&nbsp;
        <fmtAreaSetShipping:message key="BTN_SHIP_TO" var="var"/><gmjsp:button name="button3"  onClick="return fnShipTo()" value="${varShipTo}" buttonType="Save" />&nbsp;
      </td>
    </logic:equal>
    
    <logic:equal name="frmAreaSet" property="strOpt" value="11401">
      <td align="right">
        <fmtAreaSetShipping:message key="BTN_ACCEPT" var="varAccept"/><gmjsp:button name="button2"  onClick="return fnStatusUpdate(' accept ')" value="${varAccept}" buttonType="Save"/>&nbsp;
        <fmtAreaSetShipping:message key="BTN_REJECT" var="varReject"/><gmjsp:button name="button3"  onClick="return fnReject()" value="${varReject}" buttonType="Save" />&nbsp;
      </td>
    </logic:equal>
    
    <logic:equal name="frmAreaSet" property="strOpt" value="11403">
      <td align="right"><fmtAreaSetShipping:message key="BTN_ACCEPT" var="varAccept"/><gmjsp:button name="button2"  onClick="return fnStatusUpdate(' accept ')" value="${varAccept}" buttonType="Save"/>
      <fmtAreaSetShipping:message key="BTN_REJECT" var="varReject"/><gmjsp:button name="button3"  onClick="return fnReject()" value="${varReject}" buttonType="Save"/>&nbsp;
    </td>
    </logic:equal>

  </tr>
</table>
</td></tr>	
  </table>

<p>&nbsp;</p>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</logic:equal>
</html>
