 
 <!-- \sales\AreaSet\GmAddInfo.jsp -->
 <%@ include file="/common/GmHeader.inc" %>
 
 
<fmtAddInfo:setLocale value="<%=strLocale%>"/>
<fmtAddInfo:setBundle basename="properties.labels.sales.AreaSet.GmAddInfo"/>

<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>AreaSet - More Info</title>

<style type="text/css" media="all"> 
.style1 {FONT-SIZE: 8pt; COLOR: #000000; font-weight: bold; border: 1px solid #d3e2e9; font-family: verdana, arial, sans-serif; }
</style>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>	
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>

<script type="text/javascript">
 
   function fnClose(){
	   CloseDhtmlxWindow();
	   return false;
   }
   function fnAddress(){
	   var repID = document.frmAreaSet.repID.value;
	   var addID = document.frmAreaSet.addID.value;
	   var refID = document.frmAreaSet.refID.value;
	   var stropt =document.frmAreaSet.strOpt.value; 
	   var shipType =document.frmAreaSet.shipType.value;
	   document.frmAreaSet.action = "/gmAreaSetShipping.do?method=EditAddress&repID="+repID+"&addID="+addID+"&refID="+refID+"&strOpt="+stropt+"&shipType="+shipType;
	   document.frmAreaSet.submit();
	   return false;
   }
   
</script>
</head>






 <logic:equal name="frmAreaSet" property="strOpt" value="">
 <body onload="fnClose();"></body>
</logic:equal>

 <logic:notEqual name="frmAreaSet" property="strOpt" value="">
<body >
<html:form action="/gmAreaSetShipping.do">
<input type="hidden" name="repID" value="<bean:write name="frmAreaSet" property="repID" />"/>
<input type="hidden" name="addID" value="<bean:write name="frmAreaSet" property="addID" />"/>
<input type="hidden" name="refID" value="<bean:write name="frmAreaSet" property="refID" />"/>
<input type="hidden" name="shipType" value="<bean:write name="frmAreaSet" property="shipType" />"/>
<input type="hidden" name="strOpt" value="<bean:write name="frmAreaSet" property="strOpt" />"/>

<!-- Custom tag lib code modified for JBOSS migration changes -->
<table width="648" border="0">
  <tr>
    <td width="638"><div align="center">
      <table width="411" border="1"  cellpadding="0" cellspacing="0">
          <tr>
            <td width="438"><table width="409" border="0"  cellpadding="0" cellspacing="0">
                <tr class="RightDashBoardHeader">
                  <td width="241" height="24" colspan="19"><fmtAddInfo:message key="LBL_ADDITIONAL_INFO"/></td>
                </tr>
              </table>
              <table border="0" cellspacing="0" cellpadding="0" width="408" >
			
                <tr height="20">
                  <td width="215" height="35" class="style1"><fmtAddInfo:message key="LBL_CASE#"/></td>
                  <td width="193" class="Column" align="left"><bean:write name="frmAreaSet" property="caseID" /></td>
                </tr>
                <tr height="20" class="Shade">
                  <td height="28" class="style1"><fmtAddInfo:message key="LBL_CARRIER_MODE"/></td>
                  <td class="Column" align="left"><bean:write name="frmAreaSet" property="delCarrier" />
                    <logic:equal name="frmAreaSet" property="statusfl" value="40">
                   /
                   </logic:equal>
                    <bean:write name="frmAreaSet" property="delMode" /></td>
                </tr>
                <tr height="20">
                  <td height="26" class="style1"><fmtAddInfo:message key="LBL_TRACKING_NUMBER"/></td>
                  <td class="Column" align="left"><bean:write name="frmAreaSet" property="tNo" /></td>
                </tr>
                <tr height="20" class="Shade">
                  <td height="23" class="style1"><fmtAddInfo:message key="LBL_ACCOUNT"/></td>
                  <td class="Column" align="left"><bean:write name="frmAreaSet" property="account" /></td>
                </tr>
                <tr height="20">
                  <td height="23" class="style1"><fmtAddInfo:message key="LBL_TAG#"/></td>
                  <td class="Column" align="left"> <bean:write name="frmAreaSet" property="tagID" /></td>
                </tr>
                <tr height="20" class="Shade">
                  <td height="23" class="style1"><fmtAddInfo:message key="LBL_CONSIGNED_TO"/></td>
                  <td class="Column" align="left"><bean:write name="frmAreaSet" property="consignTo" /></td>
                </tr>
                <tr height="20">
                  <td height="23" class="style1"><fmtAddInfo:message key="LBL_REQUESTED_SURGERY_DATE"/></td>
                  <td class="Column" align="left"><bean:write name="frmAreaSet" property="surgeryDate" /></td>
                </tr>
                <logic:notEqual name="frmAreaSet" property="strOpt" value="11402">
                 
                <tr height="20" class="Shade">
                  <td height="20" class="Column" align="left"><strong>
                 
                    <logic:equal name="frmAreaSet" property="strOpt" value="11400">
                  <fmtAddInfo:message key="LBL_RETURN_ADDRESS"/>
                  </strong><span class="InnerColumn">
                    <fmtAddInfo:message key="BTN_E" var="varE"/><gmjsp:button name="button22"  buttonType="Save" value="${varE}" onClick="return fnAddress();"/>
                    </span>
                  </logic:equal>
                  
                   <logic:equal name="frmAreaSet" property="strOpt" value="11401">
                   <fmtAddInfo:message key="LBL_SHIPPING_ADDRESS"/>
                 <logic:notEqual name="frmAreaSet" property="statusfl" value="40">
                   </strong><span class="InnerColumn">
                    <fmtAddInfo:message key="BTN_E" var="varE"/><gmjsp:button name="button22" buttonType="Save" value="${varE}" onClick="return fnAddress();"/>
                    </span>
                  </logic:notEqual>
                   </logic:equal>
                   
                    <logic:equal name="frmAreaSet" property="strOpt" value="11403">
                    <fmtAddInfo:message key="LBL_SHIPPING_ADDRESS"/>
                  <logic:notEqual name="frmAreaSet" property="statusfl" value="40">
                   </strong><span class="InnerColumn">
                    <fmtAddInfo:message key="BTN_E" var="varE"/><gmjsp:button name="button22" value="${varE}" buttonType="Save" onClick="return fnAddress();"/>
                    </span>
                  </logic:notEqual>
                   </logic:equal>
                  
                 
                  </td>
                  <td class="Column" align="left">
                   <bean:define id="address" name="frmAreaSet" property="address" type="java.lang.String"></bean:define>
                  <%=address%>
                 </td>
                </tr>
				
			</logic:notEqual>
				
              </table></td>
          </tr>
      </table>
      <br />
      <table width="414" cellpadding="0" cellspacing="0">
          <tr>
            <td width="442" align="center"><fmtAddInfo:message key="BTN_CLOSE" var="varClose"/><gmjsp:button  name="button2"  buttonType="Save" onClick="return fnClose();" value="${varClose}" />            </td>
          </tr>
              </table>
    </div></td>
  </tr>
</table>
<p>&nbsp;</p>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</logic:notEqual>
</html>
