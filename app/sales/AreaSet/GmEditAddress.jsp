  
   <!-- \sales\AreaSet\GmEditAddress.jsp -->
  
  <%@ include file="/common/GmHeader.inc" %>
  <%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
  <%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap, java.util.Calendar" %> 
  <%@ page import ="com.globus.common.beans.GmCommonControls"%>
  <%@ page import = "com.globus.sales.areaset.forms.GmAreaSetForm" %>
  <bean:define id="repID" name="frmAreaSet" property="repID" type="java.lang.String"> </bean:define>
 
  <bean:define id="addID" name="frmAreaSet" property="addID" type="java.lang.String"> </bean:define>
  
  <bean:define id="refID" name="frmAreaSet" property="refID" type="java.lang.String"> </bean:define>
  
  <bean:define id="strOpt" name="frmAreaSet" property="strOpt" type="java.lang.String"> </bean:define>
  
  <bean:define id="shipType" name="frmAreaSet" property="shipType" type="java.lang.String"> </bean:define>
  
  <bean:define id="oldAddID" name="frmAreaSet" property="oldAddID" type="java.lang.String"> </bean:define>
  <bean:define id="haction" name="frmAreaSet" property="haction" type="java.lang.String"> </bean:define>  
  
  <bean:define id="strCaseDistID" name="frmAreaSet" property="caseDistID" type="java.lang.String"> </bean:define> 
  <bean:define id="strCaseRepID" name="frmAreaSet" property="caseRepID" type="java.lang.String"> </bean:define>   
  <bean:define id="strCaseAccID" name="frmAreaSet" property="caseAccID" type="java.lang.String"> </bean:define>
  <bean:define id="caseAccNm" name="frmAreaSet" property="caseAccNm" type="java.lang.String"> </bean:define>   
  <bean:define id="caseRepNm" name="frmAreaSet" property="caseRepNm" type="java.lang.String"> </bean:define>
  <bean:define id="addIdReturned" name="frmAreaSet" property="addIdReturned" type="java.lang.String"> </bean:define>
  <bean:define id="hAddrId" name="frmAreaSet" property="hAddrId" type="java.lang.String"> </bean:define> 
  <%
  String strSalesAreasetJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES_AREASET");
  %>
<HTML>
<HEAD>
 
<TITLE>Globus Medical: Areaset Edit Address</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
var distid = '<%=strCaseDistID%>';
var repid = '<%=strCaseRepID%>';
var accid = '<%=strCaseAccID%>';
var repname='<%=caseRepNm%>';
var acname='<%=caseAccNm%>';
var repID = '<%=repID%>';


var addID = '<%=addID%>';

var refID = '<%=refID%>';

var strOpt = '<%=strOpt%>';

var shipType = '<%=shipType%>';

var oldAddID = '<%=oldAddID%>';
var haction = '<%=haction%>';
var addIdReturned ='<%=addIdReturned%>';
</script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="javascript" src="<%=strSalesAreasetJsPath%>/GmEditAddress.js"></script>
</head>


<body leftmargin="20" topmargin="10" onload="SetDefaultValues();">

<html:form action="/gmAreaSetShipping.do">
<html:hidden property="parentFrmName"/> 

		<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
			<tr><td class="line" colspan="4"></td></tr>
			<tr><td colspan="2"> 
						<jsp:include page="/gmIncShipDetails.do" flush="true">
				 <jsp:param name="FORMNAME" value="frmShipDetails" />
				<jsp:param name="DISPLAYFL" value="N"/>
				<jsp:param name="names" value="<%=repID%>"/>
				<jsp:param name="shipTo" value="<%=shipType%>"/>
				<jsp:param name="screenType" value="Case"/>
				<jsp:param name="hAddrId" value="<%=hAddrId%>"/>
				</jsp:include>		
					
				</td></tr>
				
		</table>

	</html:form>
	    
 <%@ include file="/common/GmFooter.inc" %>
</body>
</html>
