
<!-- \sales\AreaSet\GmAddressLookup.jsp  -->
 
 <%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtAddressLookup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtAddressLookup:setLocale value="<%=strLocale%>"/>
<fmtAddressLookup:setBundle basename="properties.labels.salesAreaSet.GmAddressLookup.jsp "/> 
 
 
 
 <%
 String strPartyJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PARTY");
 %>
 
 
 
 
 
 
<html>
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
	<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
</html>
 
 
<HTML>
<HEAD>
<TITLE> Globus Medical: Address Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
	
tr.odd {
	background-color: #fff
}

tr.even {
	background-color: #fea
}
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
 <script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strPartyJsPath%>/GmParty.js"></script>
 

 
</HEAD>
 
<BODY leftmargin="20" topmargin="10" onLoad="refeshParentPage()">
<form name="frmAddressSetup" method="post" action="/gmAddressSetup.do">
<input type="hidden" name="strOpt" value="">
<input type="hidden" name="haddressID" value="">
<input type="hidden" name="partyId" value="713">
<input type="hidden" name="includedPage" value="false">
<input type="hidden" name="transactPage" value="true">
<input type="hidden" name="addIdReturned" value="">
 
<input type="hidden" name="hTxnId"/>
<input type="hidden" name="hTxnName"/>
<input type="hidden" name="hCancelType" value="VDADD"/>
<input type="hidden" name="hAction" />
 
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtAddressLookup:message key="LBL_ADDRESS_SETUP"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtAddressLookup:message key="IMG_HELP" var="varHelp"/><img id='imgEdit' align="right" style='cursor:hand' src='
<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('http://gwiki.intranet/wiki','Address_Setup');" />
		    </td>
		</tr>
		<tr><td colspan="2" height="0" bgcolor="#666666"></td></tr>
		
		<tr>
			<td width="698" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">		     
                    <tr><td colspan="2" height="15"></td></tr> 
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr class="Shade">
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="24" ></font><font color="red">*</font>&nbsp;<fmtAddressLookup:message key="LBL_ADDRESS_TYPE"/>:</td> 
                        <td>&nbsp;
							<select name="addressType" id="" class="RightText" tabindex=0   onChange="" > <option value=0 id=0 ><fmtAddressLookup:message key="LBL_CHOOSE_ONE"/><option  value= 90400  id=null><fmtAddressLookup:message key="LBL_HOME"/></option><option  value= 90401  id=null><fmtAddressLookup:message key="LBL_OFFICE"/></option><option  value= 90403  id=null><fmtAddressLookup:message key="LBL_HFPU"/></option><option  value= 90402  id=null><fmtAddressLookup:message key="LBL_OTHERS"/></option></select>
                        </td>
                    </tr>                    
                                     
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr>
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtAddressLookup:message key="LBL_ADDRESS"/>:</td>
                        <td>&nbsp; <input type="text" name="billAdd1" size="50" value="" onBlur="changeBgColor(this,'#ffffff');" onFocus="changeBgColor(this,'#AACCE8');" class="InputArea">
                        </td> 
                    </tr>
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr class="Shade">
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="24"><fmtAddressLookup:message key="LBL_ADDRESS"/>:</td> 
                        <td>&nbsp; <input type="text" name="billAdd2" size="50" value="" onBlur="changeBgColor(this,'#ffffff');" onFocus="changeBgColor(this,'#AACCE8');" class="InputArea">
                        </td> 
                    </tr>
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr>
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtAddressLookup:message key="LBL_CITY"/>:</td>                         
                        <td>&nbsp; <input type="text" name="billCity" size="30" value="" onBlur="changeBgColor(this,'#ffffff');" onFocus="changeBgColor(this,'#AACCE8');" class="InputArea">
                        </td> 
                    </tr>
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr class="Shade">
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtAddressLookup:message key="LBL_STATE"/>:</td> 
                        <td>&nbsp;
                			<select name="state" id="" class="RightText" tabindex=0   onChange="" > <option value=0 id=0 ><fmtAddressLookup:message key="LBL_CHOOSE_ONE"/><option  value= 1007  id=null><fmtAddressLookup:message key="LBL_ALABAMA"/></option><option  value= 1020  id=null><fmtAddressLookup:message key="LBL_ALASKA"/></option><option  value= 1021  id=null><fmtAddressLookup:message key="LBL_ARIZONA"/></option><option  value= 1022  id=null><fmtAddressLookup:message key="LBL_ARKANSAS"/></option><option  value= 1017  id=null><fmtAddressLookup:message key="LBL_CALIFORNIA"/></option><option  value= 1023  id=null><fmtAddressLookup:message key="LBL_COLORADO"/></option><option  value= 1024  id=null><fmtAddressLookup:message key="LBL_CONNECTICUT"/></option><option  value= 1025  id=null><fmtAddressLookup:message key="LBL_DELAWARE"/></option><option  value= 1005  id=null><fmtAddressLookup:message key="LBL_FLORIDA"/></option><option  value= 1011  id=null><fmtAddressLookup:message key="LBL_GEORGIA"/></option><option  value= 1026  id=null><fmtAddressLookup:message key="LBL_HAWAII"/></option><option  value= 1014  id=null><fmtAddressLookup:message key="LBL_IDAHO"/></option><option  value= 1027  id=null><fmtAddressLookup:message key="LBL_ILLINOIS"/></option><option  value= 1028  id=null><fmtAddressLookup:message key="LBL_INDIANA"/></option><option  value= 1029  id=null><fmtAddressLookup:message key="LBL_IOWA"/></option><option  value= 1030  id=null><fmtAddressLookup:message key="LBL_KANSAS"/></option><option  value= 1031  id=null><fmtAddressLookup:message key="LBL_KENTUCKY"/></option><option  value= 1032  id=null><fmtAddressLookup:message key="LBL_LOUISIANA"/></option><option  value= 1033  id=null><fmtAddressLookup:message key="LBL_MAINE"/></option><option  value= 1012  id=null><fmtAddressLookup:message key="LBL_MARYLAND"/></option><option  value= 1034  id=null><fmtAddressLookup:message key="LBL_MASSCHUSETTS"/></option><option  value= 1010  id=null><fmtAddressLookup:message key="LBL_MICHIGAN"/></option><option  value= 1018  id=null><fmtAddressLookup:message key="LBL_MINNESOTA"/></option><option  value= 1035  id=null><fmtAddressLookup:message key="LBL_MISSISIPPI"/></option><option  value= 1036  id=null><fmtAddressLookup:message key="LBL_MISSOURI"/></option><option  value= 1037  id=null><fmtAddressLookup:message key="LBL_MONTANA"/></option><option  value= 1038  id=null><fmtAddressLookup:message key="LBL_NEBRESKA"/></option><option  value= 1039  id=null><fmtAddressLookup:message key="LBL_NEVADA"/></option><option  value= 1019  id=null><fmtAddressLookup:message key="LBL_NEW_HAMPSHIRE"/></option><option  value= 1004  id=null><fmtAddressLookup:message key="LBL_NEW_JERSEY"/></option><option  value= 1040  id=null><fmtAddressLookup:message key="LBL_NEW_MEXICO"/></option><option  value= 1041  id=null><fmtAddressLookup:message key="LBL_NEW_YORK"/></option><option  value= 1002  id=null><fmtAddressLookup:message key="LBL_NORTH_CAROLINA"/></option><option  value= 1008  id=null><fmtAddressLookup:message key="LBL_NORTH_DAKOTA"/></option><option  value= 1016  id=null><fmtAddressLookup:message key="LBL_OHIO"/></option><option  value= 1042  id=null><fmtAddressLookup:message key="LBL_OKLAHOMA"/></option><option  value= 1043  id=null><fmtAddressLookup:message key="LBL_OREGON"/></option><option  value= 1001  id=null><fmtAddressLookup:message key="LBL_PENNSYLVANIA"/></option><option  value= 1107  id=null><fmtAddressLookup:message key="LBL_PUERTO_RICO"/></option><option  value= 1044  id=null><fmtAddressLookup:message key="LBL_RHODE_ISLAND"/></option><option  value= 1006  id=null><fmtAddressLookup:message key="LBL_SOUTH_CAROLINA"/></option><option  value= 1045  id=null><fmtAddressLookup:message key="LBL_SOUTH_DAKOTA"/></option><option  value= 1046  id=null><fmtAddressLookup:message key="LBL_TENNESSEE"/></option><option  value= 1015  id=null><fmtAddressLookup:message key="LBL_TEXAS"/></option><option  value= 1047  id=null><fmtAddressLookup:message key="LBL_UTAH"/></option><option  value= 1048  id=null><fmtAddressLookup:message key="LBL_VERMONT"/></option><option  value= 1049  id=null><fmtAddressLookup:message key="LBL_VIRGINIA"/></option><option  value= 1050  id=null><fmtAddressLookup:message key="LBL_WASHINGTON"/></option><option  value= 1051  id=null><fmtAddressLookup:message key="LBL_WEST_VIRGINIA"/></option><option  value= 1009  id=null><fmtAddressLookup:message key="LBL_WISCONSIN"/></option><option  value= 1052  id=null><fmtAddressLookup:message key="LBL_WYOMING"/></option><option  value= 1053  id=null><fmtAddressLookup:message key="LBL_DISTRICT_OF_COLUMBIA"/></option><option  value= 1054  id=null><fmtAddressLookup:message key="LBL_TAMIL_NADU"/></option><option  value= 1057  id=null><fmtAddressLookup:message key="LBL_NA"/></option><option  value= 1056  id=null><fmtAddressLookup:message key="LBL_HAMPSHIRE"/></option><option  value= 1058  id=null><fmtAddressLookup:message key="LBL_JAMAICA"/></option><option  value= 1059  id=null><fmtAddressLookup:message key="LBL_TBE"/></option></select>
				        </td>
                    </tr>
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr>
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtAddressLookup:message key="LBL_ZIP"/>:</td> 
                        <td>&nbsp; <input type="text" name="zipCode" size="10" value="" onBlur="changeBgColor(this,'#ffffff');" onFocus="changeBgColor(this,'#AACCE8');" class="InputArea">
                        </td> 
                    </tr>
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr class="Shade">
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtAddressLookup:message key="LBL_COUNTRY"/>:</td> 
                        <td>&nbsp;
	                         <select name="country" id="" class="RightText" tabindex=0   onChange="" > <option value=0 id=0 ><fmtAddressLookup:message key="LBL_CHOOSE_ONE"/><option  value= 1101  id=null><fmtAddressLookup:message key="LBL_UNITED_STATES"/></option><option  value= 1104  id=null><fmtAddressLookup:message key="LBL_BERMUDA"/></option><option  value= 1108  id=null><fmtAddressLookup:message key="LBL_CYPRUS"/></option><option  value= 1102  id=null><fmtAddressLookup:message key="LBL_GERMANY"/></option><option  value= 1103  id=null><fmtAddressLookup:message key="LBL_INDIA"/></option><option  value= 1106  id=null><fmtAddressLookup:message key="LBL_MALAYSIA"/></option><option  value= 1105  id=null><fmtAddressLookup:message key="LBL_SRILANKA"/></option><option  value= 1110  id=null><fmtAddressLookup:message key="LBL_UNITED_KINGDOM"/></option><option  value= 1100  id=null><fmtAddressLookup:message key="LBL_ETHIOPIA"/></option><option  value= 1112  id=null><fmtAddressLookup:message key="LBL_KOREA"/></option><option  value= 1113  id=null><fmtAddressLookup:message key="LBL_TAIWAN"/></option><option  value= 1114  id=null><fmtAddressLookup:message key="LBL_AUSTRALIA"/></option><option  value= 1115  id=null><fmtAddressLookup:message key="LBL_BELGIUM"/></option><option  value= 1116  id=null><fmtAddressLookup:message key="LBL_PARAGUAY"/></option><option  value= 1117  id=null><fmtAddressLookup:message key="LBL_SOUTH_AFRICA"/></option><option  value= 1118  id=null><fmtAddressLookup:message key="LBL_CANADA"/></option><option  value= 1120  id=null><fmtAddressLookup:message key="LBL_IRAQ"/></option><option  value= 1121  id=null><fmtAddressLookup:message key="LBL_VENEZUELA"/></option><option  value= 1122  id=null><fmtAddressLookup:message key="LBL_COLOMBIA"/></option><option  value= 1123  id=null><fmtAddressLookup:message key="LBL_SAUDI_ARABIA"/></option><option  value= 1124  id=null><fmtAddressLookup:message key="LBL_POLAND"/></option><option  value= 1125  id=null><fmtAddressLookup:message key="LBL_KUWAIT"/></option><option  value= 1126  id=null><fmtAddressLookup:message key="LBL_HONG_KONG"/></option><option  value= 1127  id=null><fmtAddressLookup:message key="LBL_BRAZIL"/></option><option  value= 1128  id=null><fmtAddressLookup:message key="LBL_SOUTH_KOREA"/></option><option  value= 1132  id=null><fmtAddressLookup:message key="LBL_IRAN"/></option><option  value= 1133  id=null><fmtAddressLookup:message key="LBL_JAPAN"/></option><option  value= 1129  id=null><fmtAddressLookup:message key="LBL_ITALY"/></option><option  value= 1134  id=null><fmtAddressLookup:message key="LBL_EGYPT"/></option><option  value= 1130  id=null><fmtAddressLookup:message key="LBL_SWEDEN"/></option><option  value= 1135  id=null><fmtAddressLookup:message key="LBL_CHINA"/></option><option  value= 1131  id=null><fmtAddressLookup:message key="LBL_RUSSIA"/></option><option  value= 1136  id=null><fmtAddressLookup:message key="LBL_SPAIN"/></option><option  value= 1137  id=null><fmtAddressLookup:message key="LBL_ROMANIA"/></option><option  value= 1138  id=null><fmtAddressLookup:message key="LBL_NEPAL"/></option><option  value= 1139  id=null><fmtAddressLookup:message key="LBL_FRANCE"/></option><option  value= 1140  id=null><fmtAddressLookup:message key="LBL_ISRAEL"/></option><option  value= 1141  id=null><fmtAddressLookup:message key="LBL_SWITZERLAND"/></option><option  value= 1142  id=null><fmtAddressLookup:message key="LBL_GREECE"/></option><option  value= 1143  id=null><fmtAddressLookup:message key="LBL_SCOTLAND"/></option><option  value= 1144  id=null><fmtAddressLookup:message key="LBL_UGANDA"/></option><option  value= 1145  id=null><fmtAddressLookup:message key="LBL_WEST_INDIES"/></option><option  value= 1146  id=null><fmtAddressLookup:message key="LBL_MAURITIUS"/></option><option  value= 1147  id=null><fmtAddressLookup:message key="LBL_DENMARK"/></option><option  value= 100180  id=null><fmtAddressLookup:message key="LBL_JAMAICA"/></option><option  value= 100241  id=null><fmtAddressLookup:message key="LBL_CHILE"/></option></select>
                        </td>
                    </tr>
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr>
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="30">&nbsp;<fmtAddressLookup:message key="LBL_PREFERENCE"/>:</td>
                        <td>&nbsp;
							<input type="text" name="preference" size="15" value="" onBlur="changeBgColor(this,'#ffffff');" onFocus="changeBgColor(this,'#AACCE8');" class="InputArea">
						</td>
                    </tr>   
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr class="Shade">
                        <td width="40%" class="RightTableCaption" align="right" HEIGHT="24"><fmtAddressLookup:message key="LBL_INACTIVE_FLAG"/>:</td> 
                        <td>&nbsp;<input type="checkbox" name="activeFlag" value="on">
                         </td>
                    </tr>
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    
                    
                    	<tr>
                        	<td width="40%" class="RightTableCaption" align="right" HEIGHT="24"><fmtAddressLookup:message key="LBL_PRIMARY_FLAG"/></td> 
                        	<td>&nbsp;<input type="checkbox" name="primaryFlag" value="on"></td>
                    	</tr>
                                        
                    <tr>
                    	<td colspan="2">
                    		
 
 
 
 
<table border=0 Width="100%" cellspacing=0 cellpadding=0>
<tr class="ShadeRightTableCaption">
	<td Height="24" colspan="3">&nbsp;<fmtAddressLookup:message key="LBL_COMMENTS"/></td>
</tr>
 
	
	
	<tr><td height="1" colspan="3" bgcolor="#666666"></td></tr>		
	<tr>
	    
		<td colspan="3" height="50"  >&nbsp;<textarea name="txt_LogReason" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
		onBlur="changeBgColor(this,'#ffffff');"  rows=3 cols=100></textarea></td>
	</tr>
	
	<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
	<tr bgcolor="#EEEEEE" class="RightTableCaption">
		<td width="60" height="25">&nbsp;<fmtAddressLookup:message key="LBL_DATE"/></td>
		<td width="140"><fmtAddressLookup:message key="LBL_USER_NAME"/> </td>
		<td width="500"><fmtAddressLookup:message key="LBL_DETAILS"/></td>
	</tr>
	<TR><TD colspan=3 height=1 class=Line></TD></TR>
	
			<tr><td colspan="3" height="25" align="center" class="RightTextBlue">
		<BR><fmtAddressLookup:message key="LBL_NO_DATA_AVAILABLE"/></td></tr>
 
<TR><TD colspan=3 height=1 class=Line></TD></TR>
</table>
 
						</td>
                    </tr>
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr><td colspan="2" height="15"></td></tr>                    
                    <tr> <!-- Custom tag lib code modified for JBOSS migration changes -->                        
                        <td colspan="2" align="center" height="30">&nbsp;
                        	<fmtAddressLookup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" buttonType="save"  onClick="javascript:history.back(1);" />
                        	<fmtAddressLookup:message key="BTN_ADD_NEW" var="varAddNew"/><gmjsp:button value="${varAddNew}" buttonType="save" onClick="javascript:history.back(1);" /> 
                        	<fmtAddressLookup:message key="BTN_VOID" var="varVoid"/><gmjsp:button value="${varVoid}"   buttonType="save" onClick="javascript:history.back(1);" />
							<fmtAddressLookup:message key="BTN_CANCEL" var="varCancel"/><gmjsp:button value="${varCancel}"  buttonType="load" onClick="javascript:history.back(1);" />
                        </td>
                    </tr>
                    <tr><td colspan="2" height="15"></td></tr>
                    <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
                    <tr>
                    	<td colspan="2">
    						
 
 
 
 
 
 
 
 
 
 
 
 
 
<html>
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
	<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
</html>
 
<style type="text/css" media="all"> 
	@import url("<%=strCssPath%>/screen.css");
</style>
 
 
 
<table id="currentRowObject" class="gmits">
<tr class="ShadeRightTableCaption">

<th></th>
<th><fmtAddressLookup:message key="LBL_TYPE"/></th>
<th><fmtAddressLookup:message key="LBL_ADDRESS"/></th>
<th><fmtAddressLookup:message key="LBL_CITY"/></th>
<th><fmtAddressLookup:message key="LBL_STATE"/></th>
<th><fmtAddressLookup:message key="LBL_ZIP"/></th>
<th><fmtAddressLookup:message key="LBL_COUNTRY"/></th>
<th><fmtAddressLookup:message key="LBL_STATUS"/></th>
<th><fmtAddressLookup:message key="LBL_PRIMARY"/></th></tr></thead>
<tbody>
<tr class="odd">
<td class="alignleft"><a href=javascript:fnAddressEditId('390')>  <img width='39' height='19' title='Click here to edit details' style='cursor:hand' src='<%=strImagePath%>/edit.jpg'/>  </a></td>
<td class="aligncenter"><fmtAddressLookup:message key="LBL_HOME"/></td>
<td class="alignleft"><fmtAddressLookup:message key="LBL_CALLE_24_AD20"/></td>
<td class="alignleft"><fmtAddressLookup:message key="LBL_RIO_GRANDE"/></td>
<td class="alignleft"><fmtAddressLookup:message key="LBL_PUERTO_RICO"/></td>
<td class="alignright"><fmtAddressLookup:message key="LBL_00745"/></td>
<td class="alignleft"><fmtAddressLookup:message key="LBL_UNITED_STATES"/></td>
<td class="aligncenter"><fmtAddressLookup:message key="LBL_ACTIVE"/></td>
<td class="aligncenter"><fmtAddressLookup:message key="LBL_Y"/></td></tr>
<tr class="Shade">
<td class="alignleft"><a href=javascript:fnAddressEditId('2376')>  <fmtAddressLookup:message key="IMG_CLICK_HERE_TO_EDIT_DETAILS" var="varClickheretoeditdetails"/><img width='39' height='19' title='Click here to edit details' style='cursor:hand' src='
<%=strImagePath%>/edit.jpg'/>  </a></td>
<td class="aligncenter"><fmtAddressLookup:message key="LBL_OTHERS"/></td>
<td class="alignleft"><fmtAddressLookup:message key="LBL_MACRO_ISLAND_MARRIOTT"/></td>
<td class="alignleft"><fmtAddressLookup:message key="LBL_MACRO_ISLAND"/></td>
<td class="alignleft"><fmtAddressLookup:message key="LBL_FLORIDA"/></td>
<td class="alignright"><fmtAddressLookup:message key="LBL_34145"/></td>
<td class="alignleft"><fmtAddressLookup:message key="LBL_UNITED_STATES"/></td>
<td class="aligncenter"><fmtAddressLookup:message key="LBL_ACTIVE"/></td>
<td class="aligncenter"><fmtAddressLookup:message key="LBL_N"/></td></tr></tbody></table>  
    
    
 
						</td>
                    </tr>              
 			  		   
  		  		</tr>	
      			</table>
  			  </td>
  		  </tr>	
    </table>		     	
</form>
<%@ include file="/common/GmFooter.inc"%>
 
</BODY>
 
</HTML>
 

