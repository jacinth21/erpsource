 <%
/**********************************************************************************
 * File		 		: GmTerritorySetup.jsp
 * Desc		 		: This screen is used to display Accounts in a Territory
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<!-- sales\GmTerrAccountReport.jsp -->
<%@ taglib prefix="fmtTerrAccountReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtTerrAccountReport:setLocale value="<%=strLocale%>"/>
<fmtTerrAccountReport:setBundle basename="properties.labels.sales.GmTerrAccountReport"/>


<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	ArrayList alAccounts = new ArrayList();

	String strTerrNm = "";

	if ( hmReturn != null)
	{
		alAccounts = (ArrayList)hmReturn.get("ACCLIST");
	}

	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Territory-Account Mapping </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmTerritory" method="POST">
	<table border="0" width="500" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="498" height="100%" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td class="Line" colspan="2"></td></tr>
					<tr>
						<td colspan="2" height="25" class="RightDashBoardHeader">&nbsp;<fmtTerrAccountReport:message key="LBL_ACC_LIST"/></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="2"></td></tr>
<%		  	intSize = alAccounts.size();
			if (intSize > 0)	
			{		
%>				<tr>
					<td class="RightText" align="center" HEIGHT="24"><fmtTerrAccountReport:message key="LBL_ACC"/></td>
					<td class="RightText">
<%					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alAccounts.get(i);
%>						&nbsp;<%=i + 1%>.
						&nbsp;<%=hcboVal.get("ANAME")%><BR>
<%					}
%>					</td></tr>
<%			} else 
			{ %>
				<tr><td class="RightText" align="center" HEIGHT="24"><B><fmtTerrAccountReport:message key="LBL_NO_ACC"/></B></td></tr>
<%			}
%>
	</table>
	</td><td bgcolor="#666666" width="1"></td></tr>
	<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
    </table>

</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
