 <%
/**********************************************************************************
 * File		 		: GmStateSalesReport.jsp
 * Desc		 		: This screen is used to display Sales By State
 * Version	 		: 1.0
 * author			: RichardK
************************************************************************************/
%>
<%@ page language="java" %>
<%@include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.PopChartUtils"%>
<%@ taglib prefix="fmtStateSalesReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- sales\GmStateSalesReport.jsp -->
<fmtStateSalesReport:setLocale value="<%=strLocale%>"/>
<fmtStateSalesReport:setBundle basename="properties.labels.sales.GmStateSalesReport"/>

<%
try {

	GmServlet gm = new GmServlet();
	HashMap hmReturn = new HashMap();

	String strsalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");
	String strWikiTitle = GmCommonClass.getWikiTitle("SALES_BY_STATE");
	ArrayList alStateSales 	= new ArrayList();	
	ArrayList alTopAgent 	= new ArrayList();	
	ArrayList alTopAccount 	= new ArrayList();	
	
	gm.checkSession(response,session);
	
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	
	alStateSales = (ArrayList)hmReturn.get("STATESALE");
	alTopAgent = (ArrayList)hmReturn.get("TOPAGENT");
	alTopAccount = (ArrayList)hmReturn.get("TOPACCOUNT");
	
	// Other values
	
	StringBuffer sbSateScript = new StringBuffer();	
	StringBuffer sbTopAgentGrp = new StringBuffer();	
	StringBuffer sbTopAgentItem = new StringBuffer();	
	StringBuffer sbTopAccountGrp = new StringBuffer();	
	StringBuffer sbTopAccountItem = new StringBuffer();	
	
	StringBuffer  sbpcScript = new StringBuffer();	
	//Below code is to track down the visits on the report.
	String strUserId = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
	String strShUserName = GmCommonClass.parseNull((String)session.getAttribute("strSessShName"));
	String strUserName = strUserId + " - " + strShUserName;
	//Visit code ends
	
	HashMap hmTemp= null;	
	String strCurrSign = GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb"));
	String strStateSalesJsonData = GmCommonClass.parseNull((String) request.getAttribute("STATESALESJSON"));  
	String strFCExportServer = GmCommonClass.parseNull((String) GmCommonClass.getString("FUSIONCHART_EXPORT_SERVER")); 
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: YTD #</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/fusionchart/fusioncharts.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/fusionchart/fusioncharts.maps.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/fusionchart/fusioncharts.usa.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script type="text/javascript" src="<%=strsalesJsPath%>/GmStateSalesReport.js"></script>

<script> downloadPiwikJS();</script>
<script> 
var lblSalesBystate = '<fmtStateSalesReport:message key="LBL_SALES_STATE"/>';
fnTracker(lblSalesBystate, '<%=strUserName%>');

</script>
<script>

var currSymbol = '<%=strCurrSign%>';
var fcExportServer = '<%=strFCExportServer%>';
var mapchartDataArray = eval(<%=strStateSalesJsonData%>);
var totalSales = mapchartDataArray.totalamt;
var fromMonth = '<%=request.getAttribute("Cbo_FromMonth")%>';
var toMonth = '<%=request.getAttribute("Cbo_ToMonth")%>';
var fromYear = '<%=request.getAttribute("Cbo_FromYear")%>';
var toYear = '<%=request.getAttribute("Cbo_ToYear")%>';

var subCaptionFontcolor ="#000000";
if(totalSales < 0){
	subCaptionFontcolor = "#FF0000"; 
	totalSales = "("+formatCurrency(totalSales)+")";
}else{
	totalSales = formatCurrency(totalSales);
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="5" onLoad="javascript:fnLoad()">
<FORM name="frmMain" method="post" action = "<%=strServletPath%>/GmDetailStateSalesServlet">
<input type="hidden" name="hAction" value="">
<TABLE cellSpacing=0 cellPadding=0  border=0 width="980">
<TBODY>
	<tr>
		<td rowspan="10" class=Line noWrap width=1></td>
		<td class=Line noWrap height=1></td>
		<td rowspan="10" class=Line noWrap width=1></td>
	</tr>
	<tr>
		<td>
			<TABLE cellSpacing=0 cellPadding=0  border=0 width="100%">
				<tr class=Line>
					<td height=25 class=RightDashBoardHeader><fmtStateSalesReport:message key="LBL_SALES_STATE"/></td>
					<td  height="25" class="RightDashBoardHeader">
						<fmtStateSalesReport:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
				</tr>
			</TABLE>
	</td>
	
	</tr>
	<tr class=Line><td noWrap height=1></td></tr>
	<tr class=borderDark>
		<td noWrap height=1>  </td>
	</tr>
	<tr>
		<td align="left">&nbsp;
			<jsp:include page="/common/GmMonthYearFilter.jsp"> 
				<jsp:param name="HACTION" value="LoadStateSales" />
			</jsp:include> 
		 </td>
	</tr>
	<tr>
		<td colspan="7" height="20">
			<jsp:include page="/sales/GmSalesFilters.jsp" >
				<jsp:param name="HIDE" value="SYSTEM" />
				<jsp:param name="FRMNAME" value="frmMain" />
				<jsp:param name="HACTION" value="LoadStateSales" />
			</jsp:include> 
		</td>
	</tr> 
	<tr class=Line><td noWrap height=1></td></tr>
	<tr>
		<td noWrap height=3>  </td>
	</tr>	
	<tr>
	<td height=98%  align="center" colspan="7">
	<div id="chartContainer" height="400" width="950" style="border: 1px;border-color: black;"></div>
	</td>
	</tr>
	<tr class=Line><td noWrap height=1></td></tr>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>
<%@include file="/common/GmFooter.inc"%>
</HTML>
