<%
/**********************************************************************************
 * File		 		: GmZoneSetup.jsp
 * Desc		 		: Setup the CodeLookup values
 * Version	 		: 1.0
 * author			: Gopinathan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtZoneReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!--sales\GmZoneReport.jsp  -->
<fmtZoneReport:setLocale value="<%=strLocale%>"/>
<fmtZoneReport:setBundle basename="properties.labels.sales.GmZoneReport"/>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<bean:define id="gridData" name="frmSalesMapping" property="gridXmlData" type="java.lang.String"> </bean:define>
<%
String strsalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");
%>
<html>
<head>
<title>Code Name Change</title>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strsalesJsPath%>/GmSalesZoneMapping.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script>
	var objGridData;
	objGridData = '<%=gridData%>';
		
</script>
</head>
<body leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoad()">
<html:form action="/gmSalesZoneMapping.do">
<html:hidden name="frmSalesMapping" property="strOpt" /> 
<html:hidden name="frmSalesMapping" property="zoneID" />
<table border="0" cellspacing="0" cellpadding="0">
	<tr >
		<td  height="25"  class="RightDashBoardHeader" >&nbsp;<fmtZoneReport:message key="LBL_ZONE_REPORT"/></td>
		
	</tr>	
	<tr>
           	<td  height="400" width="890">
				<div id="mygrid_container" style="grid" height="400px" width="890px"></div>
		    </td>	
	</tr>	
	<tr>
                <td colspan="2" align="center">
                <div class='exportlinks'><fmtZoneReport:message key="LBL_EXPORT_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
                                onclick="mygrid.toExcel('/phpapp/excel/generate.php');"> <fmtZoneReport:message key="LBL_EXCEL"/> </a></div>
                </td>
		</tr>
</table>
</html:form>
</body>
</html>