<%
/**********************************************************************************
 * File		 		: GmAssocRepAcctMap.jsp
 * Desc		 		: This screen is used to Map Rep to Account
 * author			: Maikandan Rajasekaran
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.Date"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtAssocRepAcctMap" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\GmAssocRepAcctMap.jsp -->

<fmtAssocRepAcctMap:setLocale value="<%=strLocale%>"/>
<fmtAssocRepAcctMap:setBundle basename="properties.labels.sales.GmAssocRepAcctMap"/>
<!-- Imports for Logger -->


<%@ page buffer="16kb" autoFlush="true" %>
<%@ page import="java.util.StringTokenizer" %>

<%
 String strWikiTitle = GmCommonClass.getWikiTitle("ASSOC_REP_MAPPING");
 String strCustServiceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");

 ArrayList alRepDetails = new ArrayList();
 ArrayList alAssocRepList = new ArrayList();
 HashMap hmAssocRepData = new HashMap();
 HashMap hmDetails = new HashMap();
 HashMap hmData = new HashMap();
 hmAssocRepData = (HashMap)request.getAttribute("HMASSOCREPDATA");
 
 hmDetails = GmCommonClass.parseNullHashMap((HashMap)hmAssocRepData.get("HMACCOUNT"));
 
 hmData = GmCommonClass.parseNullHashMap((HashMap)hmDetails.get("ACCSUM"));
 
 String strAccountId =GmCommonClass.parseNull((String)hmData.get("ACCID"));
 String strAccountName = GmCommonClass.parseNull((String)hmData.get("ANAME"));
 String strRepName = GmCommonClass.parseNull((String)hmData.get("RPNAME"));
 String strDistName = GmCommonClass.parseNull((String)hmData.get("DNAME"));
 String strRepId = GmCommonClass.parseNull((String)hmData.get("REPID"));
 alRepDetails = GmCommonClass.parseNullArrayList((ArrayList)hmAssocRepData.get("ALREPLIST"));
 alAssocRepList = GmCommonClass.parseNullArrayList((ArrayList)hmAssocRepData.get("ALASSOCREP"));
 String strXmlGridData = (String)request.getAttribute("GRIDDATA");
 
 int AssocRepListsize = alAssocRepList.size();

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Associate Rep Mapping </TITLE>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel = "stylesheet" type = "text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strCustServiceJsPath%>/GmAssocRepAcctMap.js"></script>

<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
	<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
	<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>	
	<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxCombo.js"></script>
<script>
var objGridData;
objGridData = '<%=strXmlGridData%>';
var acctid = '<%=strAccountId%>';
var assocrepsize = <%=AssocRepListsize%>;
var repid = '<%=strRepId%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload= "fnOnLoad();" onkeypress="fnEnter();">

<FORM name="frmAccountRepMap" method="POST" action="<%=strServletPath%>/GmAccountServlet">
<input type="hidden" name="hInputString" value="">
<input type="hidden" name="hstrOpt" value="">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hAccountId" value="">
<input type="hidden" name="hAssocRepId" value="">
<input type="hidden" name="hVoidString" value="">

	<table border="0"  class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="100" valign="top">
				<table border="0"  class="DtTable700" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" class="RightDashBoardHeader" colspan="3">&nbsp;<fmtAssocRepAcctMap:message key="LBL_ASSOCIATE_REP_MAPPING"/></td>
					<td  height="25" class="RightDashBoardHeader">
						<fmtAssocRepAcctMap:message key="IMG_ALT_HELP" var = "varHelp"/>
						<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
					</tr>
					<tr class="evenshade">
						<fmtAssocRepAcctMap:message key="LBL_ACC_NAME" var="varAccName"/><gmjsp:label width="20%"  type="BoldText"  SFLblControlName="${varAccName}" td="true"/>
						<td width="35%">&nbsp;<%=strAccountName%></td>
						<fmtAssocRepAcctMap:message key="LBL_ACC_ID" var="varAccId"/><gmjsp:label width="15%" type="BoldText"  SFLblControlName="${varAccId}" td="true"/>
						<td width="35%">&nbsp;<%=strAccountId%></td>
					</tr>
					<tr><td class="LLine" height="1" colspan="4"></td></tr>
					<tr class="oddshade">
						<fmtAssocRepAcctMap:message key="LBL_DISTRIBUTOR_NAME" var="varDisName"/><gmjsp:label type="BoldText" SFLblControlName="${varDisName}" td="true"/>
						<td>&nbsp;<%=strDistName%></td>
						<fmtAssocRepAcctMap:message key="LBL_SALES_REP" var="varSalesRep"/><gmjsp:label type="BoldText" SFLblControlName="${varSalesRep}" td="true"/>
						<td>&nbsp;<%=strRepName%></td>
					</tr>
					<tr><td class="LLine" height="1" colspan="4"></td></tr>
					<tr class="ShadeRightTableCaptionBlue">
								<fmtAssocRepAcctMap:message key="LBL_MAPPING" var="varMapping"/><gmjsp:label type="BoldText" SFLblControlName="${varMapping}" td="true" align="left" />
								<td height="23"></td><td></td><td></td>
					</tr>
					<tr><td class="LLine" height="1" colspan="4"></td></tr>
					<tr colspan ="2" class="evenshade">
						<fmtAssocRepAcctMap:message key="LBL_ASS_REPLIST" var="varAssReplist"/><gmjsp:label type="BoldText"  SFLblControlName="${varAssReplist}" td="true"/>
						<td class="RightText">&nbsp;
							
							<gmjsp:autolist  comboType="DhtmlXCombo" controlName="Cbo_AssocRepId"  value="<%=alRepDetails%>" codeId="ID" codeName="NAME" 
				        									defaultValue="[Choose One]" tabIndex="-1" width="350"
				        									   onChange="fnRepMap"/>
				        </td>							
					</tr>							
				
					<tr><td class="LLine" height="1" colspan="4"></td></tr>
					<tr >
						<td colspan="4">
							<div  id="ASSOCREPDATA" style="height: 400px;width: 700px"></div>
						</td>
					</tr>	
					<tr>
					<td align="center" height="30" colspan="4">
						<fmtAssocRepAcctMap:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="Btn_Submit" value="${varSubmit}" buttonType="Save" controlId="Submit" gmClass="Button" style="width: 6em" onClick="fnSaveAssocRepMap();" />
					</td>
					</tr>
					
					
			</table>
		</td>
		</tr>
	</table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>