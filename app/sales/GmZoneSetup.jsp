<%
/**********************************************************************************
 * File		 		: GmZoneSetup.jsp
 * Desc		 		: This screen is used for Zone and VP mapping 
 * Version	 		: 1.0
 * author			: Gopinathan Saravanan
************************************************************************************/
%>

<!--sales\GmZoneSetup.jsp -->

<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtZoneSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- sales\GmZoneSetup.jsp -->
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtZoneSetup:setLocale value="<%=strLocale%>"/>
<fmtZoneSetup:setBundle basename="properties.labels.sales.GmZoneSetup"/>
<%
String strsalesJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_SALES");
%>

<HTML>
<HEAD>

<TITLE>Globus Medical: Zone - VP Setup</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strsalesJsPath%>/GmSalesZoneMapping.js"></script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>

</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnDisable();">
<form name="frmSalesMapping" method="post" action="/gmSalesZoneMapping.do">
<html:hidden name="frmSalesMapping" property="strOpt" /> 
<html:hidden name="frmSalesMapping" property="zoneID" />

<table border="0" class="DtTable680" cellspacing="0" cellpadding="0">
	<tr>
		<td  colspan="2" height="25" class="RightDashBoardHeader">&nbsp;<fmtZoneSetup:message key="LBL_ZONE_VP"/></td>
		
	</tr>
		
	<tr>
		<td class="RightTableCaption" align="right" HEIGHT="30"><font color="red">*</font><fmtZoneSetup:message key="LBL_ZONE"/>:&nbsp;</td>
		<td width="60%" align="left">&nbsp;<html:text name="frmSalesMapping" property="zoneNM"  size="31" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" /></td>
	</tr>
	<tr class="Line"></tr>
<%--
	<tr class="shade">
		<td class="RightTableCaption" align="right" HEIGHT="30">VP:&nbsp;</td>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<td width="60%" align="left">&nbsp;<gmjsp:dropdown	width="200" controlName="vpID" SFFormName="frmSalesMapping" SFSeletedValue="vpID" SFValue="alVP" codeId="ID" codeName="NAME" defaultValue="[Choose One]" />
		</td>
	</tr>
	 --%>
	<tr class="shade">
		<td colspan="2" align="center" height="30">
		<logic:equal name="frmSalesMapping" property="zoneID" value="">
		<fmtZoneSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" buttonType="Save" onClick="fnSubmit();"/>
		</logic:equal>
		<logic:notEqual name="frmSalesMapping" property="zoneID" value=""> 
			<fmtZoneSetup:message key="BTN_VOID" var="varVoid"/><gmjsp:button value="&nbsp;${varVoid}&nbsp;" gmClass="button" buttonType="Save" onClick="fnVoid();"/>
		</logic:notEqual>
		</td>
	</tr>
	
</table>

</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
