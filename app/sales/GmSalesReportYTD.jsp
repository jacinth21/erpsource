 <%
/**********************************************************************************
 * File		 		: GmSalesReportYTD.jsp
 * Desc		 		: This screen is used to display Sales Rep By Account
 * Version	 		: 1.0
 * author			: RichardK
************************************************************************************/
%>
<%@ page language="java" %>
<%@include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtSalesReportYTD" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!--sales\GmSalesReportYTD.jsp  -->
<fmtSalesReportYTD:setLocale value="<%=strLocale%>"/>
<fmtSalesReportYTD:setBundle basename="properties.labels.sales.GmSalesReportYTD"/>


<%
try {
	String strWikiTitle = GmCommonClass.getWikiTitle("HISTORICAL_SALES");
	GmServlet gm = new GmServlet();
	HashMap hmReturn = new HashMap();
	ArrayList alProjList = new ArrayList();
	
	String strSelected = "";
	String strCodeID = "";
	String strProjNm = "";
	String strProjId = "";
	
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	gmCrossTab.setExport(true, pageContext, "/GmSalesYTDReportServlet", "SalesYTDReport");
	gm.checkSession(response,session);

	/*Below three values are assigned to checkboxes.
	checkbox value should be either 0 or 1.
	So,GmCommonClass.parseZero is used here.*/
	String strShowArrow = GmCommonClass.parseZero((String)request.getAttribute("Chk_ShowArrowFl"));
	String strShowPercent = GmCommonClass.parseZero((String)request.getAttribute("Chk_ShowPercentFl"));
	String strHideAmount = GmCommonClass.parseZero((String)request.getAttribute("Chk_HideAmountFl"));
	
	String strFromMonth = (String)request.getAttribute("Cbo_FromMonth");
	String strFromYear = (String)request.getAttribute("Cbo_FromYear");
	String strToMonth = (String)request.getAttribute("Cbo_ToMonth");
	String strToYear = (String)request.getAttribute("Cbo_ToYear");
	
	// MNTTASK-6864 - Historical Sales Report displayed in Pricing Request Screen
	String strGroupAccId = GmCommonClass.parseNull((String)request.getAttribute("GroupAccId"));
	
	// Combobox default value
	String strHeader = (String)request.getAttribute("strHeader");
	//String strProj = (String)request.getAttribute("Cbo_Proj");
	String strProj = (String)request.getAttribute("hSetNumber");
	
	String strType = (String)request.getAttribute("Cbo_Type");
	String strSalesType = GmCommonClass.parseNull((String)request.getAttribute("Cbo_SalesType"));
	String strFromPage = GmCommonClass.parseNull((String)request.getAttribute("HFROMPAGE"));
	ArrayList alSalesType = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("SALESTYPE"));

	strProj = (strProj == null)?"0":strProj; // If no value selected will set the combo box value to [Choose One]
	strProj = (strProj.equals("null"))?"0":strProj; // If no value selected will set the combo box value to [Choose One]
	strType = (strType == null)?"0":strType; // Setting combo box value
	String strhAction = (String)request.getAttribute("hAction");
	
	String strPageLoad = (String)request.getAttribute("pageLoad");

	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}
	
	strhAction = (strhAction == null)?"Load":strhAction;
	
	// To get the Project Combo Box value information 
	HashMap hmComonValue = (HashMap)request.getAttribute("hmComonValue");
	if (hmComonValue != null)
	{
		alProjList = (ArrayList)hmComonValue.get("PROJECTS");
	}
		
	if (!strhAction.equals("LoadAD") && !strhAction.equals("LoadVP")) 
	{
		gmCrossTab.setLinkType(strhAction);
		gmCrossTab.setLinkRequired(true);	// To Specify the link
	}
	
	
	// To display percent changed information
	if (strShowPercent.equals("1"))
	{
		gmCrossTab.setReportType(gmCrossTab.CROSSTAB_SALE_PER);	
	}	
	if (strShowArrow.equals("1") )
	{
		gmCrossTab.setShowArrow(true);; // To Display % arrow information
	}

	if (strHideAmount.equals("1"))
	{
		gmCrossTab.setHideAmount(true);	
	}	
	// To specify if its unit or valye 
	gmCrossTab.setValueType(Integer.parseInt(strType));
	gmCrossTab.setRowHighlightRequired(true);
	// Add Lines
	gmCrossTab.setColumnLineStyle("borderDark");
	gmCrossTab.addLine("Name");
	
	// Setting Display Parameter
	gmCrossTab.setAmountWidth(70);
	gmCrossTab.setNameWidth(250);
	gmCrossTab.setRowHeight(22);
	gmCrossTab.displayZeros(true);
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	
	// Page Body Style
	gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD");
	gmCrossTab.addStyle("Name","ShadeMedGrayTD","ShadeDarkGrayTD");
	gmCrossTab.addStyle("Total","ShadeMedBlueTD","ShadeDarkBlueTD") ;
/*************************************************************  SORT COLUMNS *********************************************************/	
	
	int intSortColumn =  request.getParameter("hSortColumn") == null ? -1 : Integer.parseInt((String)request.getParameter("hSortColumn"));
	int intSortOrder = Integer.parseInt(GmCommonClass.parseZero((String)request.getParameter("hSortOrder")));	
    
	gmCrossTab.setSortRequired(true);
	gmCrossTab.addSortType("Name","String");
	
	gmCrossTab.setSortColumn(intSortColumn);
	gmCrossTab.setSortOrder(intSortOrder);

	/*************************************************************************************************************************************/
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: YTD #</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script>
 
var strGroupAccId = '<%=strGroupAccId%>';
var salesType = '<%=strSalesType%>';
var hfrom_page = '<%=strFromPage%>';
function fnSort(varColumnName, varSortOrder)
{
	document.frmMain.hSortColumn.value = varColumnName;
	document.frmMain.hSortOrder.value = varSortOrder;
	fnGo("<%=strhAction%>");
}

function fnSubmit()
{
	document.frmMain.submit();
}

function fnCallAccount(val,Type )
{ 
	var ShowType = 	Type.substring(Type.length,Type.length -1 );
	
	if (ShowType == 'A')
	{
		document.frmMain.hAction.value = "LoadAccountA";	
	}else {
		document.frmMain.hAction.value = "LoadAccount";
	}
 
	fnSetValue (val,Type );
	 
	url='<%=strServletPath%>/GmSalesYTDReportServlet?';
	url=url+'hAction='+document.frmMain.hAction.value;
	 
	fnReloadSales('',url);
//	document.frmMain.submit();
}

function fnCallRepDetails(val, Type)
{ 
	var ShowType = 	Type.substring(Type.length,Type.length -1 );
	
	if (ShowType == 'A')
	{
		document.frmMain.hAction.value = "LoadRepA";
	}else {
		document.frmMain.hAction.value = "LoadRep";
	}
 
	fnSetValue (val,Type );	
	 
	url='<%=strServletPath%>/GmSalesYTDReportServlet?';
	url=url+'hAction='+document.frmMain.hAction.value;
	fnReloadSales('',url);
//	document.frmMain.submit();
}

function fnCallPart(val, Type)
{   
	var ShowType = 	Type.substring(Type.length,Type.length -1 );
	
	if (ShowType == 'A')
	{
		document.frmMain.hAction.value = "LoadPartA";	
	}else {
		document.frmMain.hAction.value = "LoadPart";
	}
	 
	fnSetValue (val,Type );

	url='<%=strServletPath%>/GmSalesYTDReportServlet?';
	url=url+'hAction='+document.frmMain.hAction.value;
	fnReloadSales('',url);
//	document.frmMain.submit();
}

function fnCallSetNumber(val,Type )
{ 
    var ShowType = 	Type.substring(Type.length,Type.length -1 );
	
	if (ShowType == 'A')
	{
		document.frmMain.hAction.value = "LoadGroupA";	
	}else {
		document.frmMain.hAction.value = "LoadGroup";
	}
	document.frmMain.pageLoad.value = "Go";		
	
	fnSetValue (val,Type );	
	document.frmMain.action ="<%=strServletPath%>/GmSetYTDReportServlet";
	
 	url='<%=strServletPath%>/GmSetYTDReportServlet?';
	url=url+'hAction='+document.frmMain.hAction.value;
	url=url+'&SetNumber=null';  
	fnReloadSales('',url);
//	document.frmMain.submit();
}

function fnSetValue (val,Type )
{  
	switch (Type) 
	{
		case 'LoadDistributor':  
			document.frmMain.DistributorID.value = val;
			break;
		case 'LoadDistributorA' :  
			document.frmMain.DistributorID.value = val;
			break;
		case "LoadRep":  		
			document.frmMain.hRepID.value = val;
			break;
		case "LoadRepA": 		
			document.frmMain.hRepID.value = val;
			break;
		case 'LoadAccount':		
			document.frmMain.hAccountID.value = val;
			break;
		case 'LoadAccountA':		
			document.frmMain.hAccountID.value = val;
			break;
		case 'LoadPart':
			document.frmMain.hPartNumber.value = val;
			break;
		case 'LoadPartA':
			document.frmMain.hPartNumber.value = val;
			break;
		case 'LoadTerritory':
			document.frmMain.hTerritory.value = val;		
			break;	
	}	
	
}
function fnGo(haction)
{ 
	document.frmMain.hGroupAccId.value = strGroupAccId;
	document.frmMain.SetNumber.value = document.frmMain.Cbo_Proj.value;
	if (hfrom_page == 'FIELDINVENTORYREPORT')
	{
		document.frmMain.hSalesType.value = salesType;
	}
	fnReloadSales(haction);
	
	
//	document.frmMain.submit(); 
	
	document.frmMain.hAction.value ="<%=strhAction%>";
	
	if ('LoadDistributor' == '<%=strhAction%>' || 'LoadDistributorA' == '<%=strhAction%>' ) 
	{
		document.frmMain.DistributorID.value = '';
	}
	else if ('LoadRep' == '<%=strhAction%>' || 'LoadRepA' == '<%=strhAction%>' ) 
	{
		document.frmMain.hRepID.value = '';
	}	
	else if ('LoadAccount' == '<%=strhAction%>' || 'LoadAccountA' == '<%=strhAction%>' ) 
	{
		document.frmMain.hAccountID.value = '';
	}	
	else if ('LoadPart' == '<%=strhAction%>' || 'LoadPartA' == '<%=strhAction%>' ) 
	{
		document.frmMain.hPartNumber.value = '';
	}	
	else if ('LoadTerritory' == '<%=strhAction%>' || 'LoadPartA' == '<%=strhAction%>' ) 
	{
		document.frmMain.hTerritory.value = '';
	}	
			
}

function fnLoad()
{ 
	document.frmMain.Cbo_FromMonth.value = '<%=request.getAttribute("Cbo_FromMonth")%>';
	document.frmMain.Cbo_ToMonth.value = '<%=request.getAttribute("Cbo_ToMonth")%>';
	document.frmMain.Cbo_FromYear.value = '<%=request.getAttribute("Cbo_FromYear")%>';
	document.frmMain.Cbo_ToYear.value = '<%=request.getAttribute("Cbo_ToYear")%>';

	 
	
	document.frmMain.Chk_ShowPercentFl.checked = <%=(strShowPercent == "0")?"false":"true"%>;
	document.frmMain.Chk_ShowArrowFl.checked = <%=(strShowArrow == "0")?"false":"true"%>;
	document.frmMain.Chk_HideAmountFl.checked = <%=(strHideAmount == "0")?"false":"true"%>;

	document.frmMain.Cbo_Proj.value = '<%=strProj%>';
	document.frmMain.Cbo_Type.value = '<%=strType%>'; 
	
	
}

</script>
</HEAD>
<%if(strFromPage.equals("FIELDINVENTORYREPORT")) { %>
<BODY onLoad="javascript:fnLoad()">
<% } else { %>
<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad()">
<%} %>
<FORM name="frmMain" method="get" action = "<%=strServletPath%>/GmSalesYTDReportServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">

<input type="hidden" name="hAreaDirector" value="<%=request.getAttribute("hAreaDirector")%>">
<input type="hidden" name="DistributorID" value="<%=request.getAttribute("hDistributorID")%>">
<input type="hidden" name="hAccountID" value="<%=request.getAttribute("hAccountID")%>">
<input type="hidden" name="SetNumber" value="<%=request.getAttribute("hSetNumber")%>">
<input type="hidden" name="hRepID" value="<%=request.getAttribute("hRepID")%>">
<input type="hidden" name="hPartNumber" value="<%=request.getAttribute("hPartNumber")%>">
<input type="hidden" name="Cbo_Name" value="">
<input type="hidden" name="hTerritory" value="<%=request.getAttribute("hTerritory")%>">
<input type="hidden" name="hFromPage" value="<%=request.getAttribute("HFROMPAGE")%>">
<input type="hidden" name="hSalesType" value="<%=request.getAttribute("Cbo_SalesType")%>">

<!-- ********************Pricing Request Screen****************** -->
 <input type="hidden" name="hGroupAccId" value="<%=strGroupAccId %>">
 
<!-- ********************Included for sorting****************** -->
  <input type="hidden" name="hSortColumn" value="<%=intSortColumn%>">
  <input type="hidden" name="hSortOrder" value="<%=intSortOrder%>">
   <input type="hidden" name="pageLoad" value="<%=strPageLoad %>">
<!-- ********************************************************** -->

<% String strRowspan =  (strhAction.equals("LoadPart") || strhAction.equals("LoadPartA"))? "12":"11";%>
<TABLE cellSpacing=0 cellPadding=0  border=0 width="1000">
<TBODY>
	<tr>
		<td rowspan="<%=strRowspan%>" class=Line noWrap width=1></td>
		<td class=Line noWrap height=1></td>
		<td rowspan="<%=strRowspan%>" class=Line noWrap width=1></td>
	</tr>
	<tr>
		<td>
			<TABLE cellSpacing=0 cellPadding=0  border="0"  width="100%">
				<tr class=Line>
					<td height=25 class=RightDashBoardHeader ><%=strHeader %><fmtSalesReportYTD:message key="LBL_IN"/></td>
					<td  height="25" class="RightDashBoardHeader">
					<fmtSalesReportYTD:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
	
				</tr>
			</TABLE>
		</td>
	</tr>
	<tr class=Line ><td noWrap height=1 ></td></tr>
	<!-- Addition Filter Information -->
	<tr><td  >
						<jsp:include page="/sales/GmSalesFilters.jsp" >
						<jsp:param name="HIDE" value="SYSTEM" />
						<jsp:param name="FRMNAME" value="frmMain" />
						<jsp:param name="HACTION" value="<%=strhAction%>" />
						<jsp:param name="HIDEBUTTON" value="HIDEGO" />
						</jsp:include>
	</td></tr>
	<tr>
		<td><jsp:include page="/common/GmCrossTabFilterInclude.jsp" > 
		<jsp:param name="HACTION" value="<%=strhAction%>" />
		</jsp:include> </td>
	</tr>
	
		<!-- Screen Level Filter	-->
	<tr class=Line><td noWrap height=1></td></tr>	
	<tr>
	<td class="RightTableCaption" HEIGHT="30"  >
		<!--Apply Group Filter
		<input type="checkbox" size="30"  name="Chk_ShowPercentFl" value=1 tabindex=6>
		-->
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtSalesReportYTD:message key="LBL_SYSTEM_NAME"/>
		&nbsp;<select name="Cbo_Proj" id="Region" class="RightText" tabindex="9"><option value="0" ><fmtSalesReportYTD:message key="LBL_ALL_SYSTEM"/>
<%		HashMap hcboVal = null;
			  		
		int intSize = alProjList.size();
		hcboVal = new HashMap();
		for (int i=0;i<intSize;i++)
		{
			hcboVal = (HashMap)alProjList.get(i);
			strCodeID = (String)hcboVal.get("ID");
		//	strProjNm = GmCommonClass.getStringWithTM((String)hcboVal.get("NAME"));
			strProjNm = (String)hcboVal.get("NAME");
%>			<option <%=strSelected%> value="<%=strCodeID%>"><%=strProjNm%></option>
<%		}%>
		</select>
		&nbsp;&nbsp;<fmtSalesReportYTD:message key="LBL_TYPE"/> 
		&nbsp;<select name="Cbo_Type" id="Type" class="RightText" tabindex="10">
			 <option value="0" ><fmtSalesReportYTD:message key="LBL_UNIT"/></option>
			 <option value="1" ><fmtSalesReportYTD:message key="LBL_AMOUNT"/></option>
		 </select>&nbsp;&nbsp;
<%if(!strFromPage.equals("FIELDINVENTORYREPORT")) { %>
<!-- Custom tag lib code modified for JBOSS migration changes -->
		&nbsp;<fmtSalesReportYTD:message key="LBL_SALES_TYPE"/>: 
		&nbsp;<gmjsp:dropdown controlName="Cbo_SalesType"  seletedValue="<%= strSalesType %>" 	
					 width="150" value="<%= alSalesType%>" defaultValue= "All" codeId="CODEID" codeName="CODENM" />
		 </select>&nbsp;&nbsp;
<% }%>		 
	</tr>	
	<% if (strhAction.equals("LoadPart") || strhAction.equals("LoadPartA")) 
	   {	%>
			<tr><td><span class="RightTextBlue"><fmtSalesReportYTD:message key="LBL_PLEASE_SELECT"/></span>
			</td></tr>
	<% }%>	
				
	<tr><td>
	
	<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
		<jsp:include page="/sales/GmSalesFilterDetails.jsp" />
	</td></tr>
	
	<tr class=borderDark><td noWrap height=1></td></tr>
	<tr>
		<td> <%=gmCrossTab.PrintCrossTabReport(hmReturn) %></td>
	</tr>
	<tr class=borderDark><td noWrap height=1></td></tr>	
</TBODY>
</TABLE>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>
<%@ include file="/common/GmSalesFooter.inc"%><!--PMT-53328 - Implement jquery.freezeheader.js file for freeze the header information -->
<%@ include file="/common/GmFooter.inc"%>
</HTML>
