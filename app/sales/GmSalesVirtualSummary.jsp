 <%
/**********************************************************************************
 * File			 	: GmSalesVirtualSummary.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Richardk
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*,java.text.DecimalFormat" %>

<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page buffer="16kb" autoFlush="true" %>

<%@ taglib prefix="fmtSalesVirtualSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!--sales\GmSalesVirtualSummary.jsp  -->
<fmtSalesVirtualSummary:setLocale value="<%=strLocale%>"/>
<fmtSalesVirtualSummary:setBundle basename="properties.labels.sales.GmSalesVirtualSummary"/>

<%
	response.setHeader("Cache-Control", "no-cache, post-check=0, pre-check=0"); //HTTP 1.1
	response.setHeader("Pragma", "no-cache");  //HTTP 1.0
	response.setDateHeader ("Expires", 0); //prevents caching at the proxy server 
%>


<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	int intSize = 0;
	int intLength = 0;

	String strCodeID = "";
	String strSelected = "";
	String strVendId = "";
	String strCurDate = "";
	
	String strCount ; 
	String strVal ; 
	String strGrowth ;
	String strPVal ;
	String strPercent ;
	
	HashMap hmLoadReturn = new HashMap();
	HashMap hcboVal = new HashMap();
  
  	String strSalesType = GmCommonClass.parseNull((String)request.getAttribute("Cbo_SalesType"));
  	log.debug(" Cbo_SalesType " +strSalesType);
  	String strOpt = (String)request.getAttribute("hOpt")==null?"":(String)request.getAttribute("hOpt");
  	String strSort = (String)request.getAttribute("hSort")==null?"true":(String)request.getAttribute("hSort");
  	String strHeader = (String)request.getAttribute("strHeader")==null?"Consignment Details":(String)request.getAttribute("strHeader");

	double dbTemp = 0;
  	DecimalFormat df = new DecimalFormat("#%");
  	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Consignment Summary List</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>



</script>

<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad()">
	<table class="DtTable850" cellspacing="0" cellpadding="0">
		<input type="hidden" name="strSessOpt" value="<%=strOpt%>">

		<!-- The following hidden fields are for Sales Drilldown (Additional Drill down)-->
		<tr>
			<td height="25" class="RightDashBoardHeader"><%=strHeader%></td>
		</tr>

		<tr><td class="Line"></td></tr>
		<!-- Holds the drill down filter information -->
		<tr><td height="10"></td></tr>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
		<tr><td><jsp:include page="/sales/GmSalesFilterDetails.jsp" />
		</td></tr>
		<tr><td height="5"></td></tr>
		<tr><td>
			<table border="0" width="100%"  export="false" varTotals="totals" cellpadding="0" cellspacing="0" >
			<tr><td valign="top" class="aligncenter" >
			<display:table name="requestScope.results.rows" export="false"  class="its" varTotals="totals" 
					cellpadding="0" cellspacing="0" decorator="com.globus.displaytag.beans.DTSalesVirtualSetWrapper" requestURI="/GmSalesVirtualCDetailServlet?hSort=123" >
				<fmtSalesVirtualSummary:message key="DT_DIST" var="varDist"/><display:column property="DNAME" title="${varDist}"   group="1" style="text-align: left; height=20"/>
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				<fmtSalesVirtualSummary:message key="DT_SYSTEM" var="varSystem"/><display:column property="GROUPNM" title="${varSystem}"  group="1" style="text-align: left;" />
				<display:column property="SGDIMAGE" title="" group="1" style="text-align: center;" />
				<display:column property="DIMAGE" title="" style="text-align: center;" />
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				<fmtSalesVirtualSummary:message key="DT_SET_ID" var="varSetId"/><display:column property="VSETID" title="${varSetId}"  style="text-align: center;width:70px"/>
				<fmtSalesVirtualSummary:message key="DT_SET_NAME" var="varSetName"/><display:column property="VSETNM" title="${varSetName}"  style="text-align: left;"/>
				<display:column property="SDIMAGE" title="" style="text-align: center;" />
				<fmtSalesVirtualSummary:message key="DT_TYPE" var="varType"/><display:column property="HCH" title="${varType}"  style="text-align: center;" />
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
				<fmtSalesVirtualSummary:message key="DT_SET" var="varSet"/><display:column property="VSETCOUNT" title="${varSet}"  total="true" style="text-align: center;" />
				<display:column property="VCONSRPTID" title="" style="text-align: left;" />
<% if(!"50301".equals(strSalesType)) { %>				
				<fmtSalesVirtualSummary:message key="DT_SET_VALUE" var="varSetValue"/><display:column property="VSETCOST" title="${varSetValue}" format="{0,number,$#,###,###.00}" style="text-align: right;"   total="true" />
				<display:footer media="html"> 
					<tr><td class="Line" colspan="14"></td></tr>
					
					<tr>
					<td class = "ShadeDarkGrayTD" colspan = "9"></td>
					<td class = "ShadeDarkGrayTD"> </td>
					<td class="Line" ></td>
					
					<%
						strCount 	= ((HashMap)pageContext.getAttribute("totals")).get("column12").toString();
						strVal 	= ((HashMap)pageContext.getAttribute("totals")).get("column14").toString();
					%>
					<td class = "ShadeDarkGrayTD" style="text-align: center;" ></td>
					<td class = "ShadeDarkGrayTD" style="text-align: left;" ><B><fmtSalesVirtualSummary:message key=""/>Total </B></td>
					<td class = "ShadeDarkGrayTD" style="text-align: right;" ><B>$<%= GmCommonClass.getStringWithCommas(strVal, 2) %></B></td>
				</display:footer>			
<% } else{  %>
				<display:footer media="html"> 
				<tr><td class="Line" colspan="14"></td></tr>
				</display:footer>			
<% }   %>
				</display:table>			
		</td></tr>
					<tr>
						<td colspan="14">
							<fmtSalesVirtualSummary:message key="LBL_AI"/>
							<fmtSalesVirtualSummary:message key="LBL_AAS"/>
							<fmtSalesVirtualSummary:message key="LBL_STD"/>
							<fmtSalesVirtualSummary:message key="LBL_AAI"/> 
						</td>
					</tr>	
		</table></td></tr>
	</table>
<%
	}catch(Exception e)
		{
			e.printStackTrace();
		}
%>

</BODY>
<%@ include file="/common/GmFooter.inc"%>
</HTML>
