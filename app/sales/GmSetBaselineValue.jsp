 <%
/**********************************************************************************
 * File		 		: GmSetBaselineValue.jsp
 * Desc		 		: This screen is used for the Setting Baseline Value
 * Version	 		: 1.0
 * author			: V Prasath
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtSetBaselineValue" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!--sales\GmSetBaselineValue.jsp  -->
<fmtSetBaselineValue:setLocale value="<%=strLocale%>"/>
<fmtSetBaselineValue:setBundle basename="properties.labels.sales.GmSetBaselineValue"/>
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	ArrayList alBaselineParameters = request.getAttribute("alBaselineParameters") == null?new ArrayList():(ArrayList)request.getAttribute("alBaselineParameters");
	String strTitle= request.getAttribute("strTitle") == null ?"":(String) request.getAttribute("strTitle");
	String strOpt= request.getAttribute("strOpt") == null ?"":(String) request.getAttribute("strOpt");
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmLoad = new HashMap();

	String strParaID="";
	String strParaName="";
	String strParaValue="";
		
	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Baseline Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script>

function fnSubmit(frm)
{
 Error_Clear();
 checkIsNumeric(frm)
  frm.hBaselineParameter.value =  getInputString(frm); 	
  //alert(frm.hBaselineParameter.value);
  if (ErrorCount > 0)
	{
		Error_Show();
		str = '';
		return false;
	}else{
		fnStartProgress('Y');
	document.frmBaseline.submit();
	  	}
}
function checkIsNumeric(x) {

var Form = x;
var Elements = Form.getElementsByTagName("input");
var Element;
	for (i = 0; i < Elements.length; i++){
	Element = Elements[i]; 			
	 if(Element.type == "text") {
	   if (Element.value != "" && isNaN(Element.value))
    	{
    	// alert(Element.name+" : "+Element.value);
    	Error_Details(message[400]);
		return false;
   		 }
   		 else if(parseFloat(Element.value) > 99.9) 
   		 {
   		   Error_Details(message[401]);
		   return false;
   		 } 
   		 else {
   		    var str = Element.value;
   		     if(str.indexOf(".") != -1) {
	   		    var subStr = str.substring(str.indexOf(".") +1);
	   		    if(subStr.length > 1)
	   		     {
	   		    	 Error_Details(message[402]);
		 			 return false;
	   		     }
	   		   }  
   		 }
	   } 
	}
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmBaseline" method="POST" action="<%=strServletPath%>/GmSetBaselineValueServlet">
<input type="hidden" name="hAction" value="Save">
<input type="hidden" name="hBaselineParameter" value="" >
<input type="hidden" name="hOpt" value="<%=strOpt%>">

	<table border="0" width="500" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="498" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td class="Line" colspan="2"></td></tr>
					<tr>
						<td colspan="2" height="25" class="RightDashBoardHeader">&nbsp;<%=strTitle%></td>
					</tr>
					<tr><td class="Line" colspan="2"></td></tr>
<%
		intSize = alBaselineParameters.size();
  		for(int count=0;count<intSize;count++) 
  					{   
  					hcboVal = (HashMap)alBaselineParameters.get(count);
  					strParaID = GmCommonClass.parseNull((String)hcboVal.get("ID"));
					strParaName = GmCommonClass.parseNull((String)hcboVal.get("NAME"));
					strParaValue = GmCommonClass.parseNull((String)hcboVal.get("VALUE"));
%>					
					
					<tr>
						<td class="RightText" align="right" width="300" HEIGHT="24"><%=strParaName%>:</td>
						<input type="hidden" name=<%="strSetID"+count+"_seq0"%> value="<%=strParaID%>">
						<td width="320">&nbsp;
							<input type="text" size="15" value="<%=strParaValue%>" name=<%="Txt_Parameter_Value_man"+count+"_seq1"%> class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=10></td>
					</tr>
<%
  					}
%>					
				   <tr>
						
						<td colspan="2" align="center"  height="30">&nbsp;
						<fmtSetBaselineValue:message key="BTN_SUBMIT" var="varSubmit"/>
						<gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnSubmit(this.form);" tabindex="13" />&nbsp;
						</td>
					</tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
