 
<% 
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.StringTokenizer" %>

<%@ page import ="com.globus.common.beans.GmCommonControls"%>

<%@ page import ="com.globus.common.servlets.GmServlet"%>

<!-- WEB-INF path corrected for JBOSS migration changes -->

 <!-- \sales\GmPrintAcctPriceIncrease.jsp-->

<%@ include file="/common/GmHeader.inc" %>
<!-- Imports for Logger -->

<%@ page import="com.globus.common.beans.GmCommonConstants,org.apache.commons.beanutils.RowSetDynaClass,org.apache.commons.beanutils.DynaBean"%> 
 

<%@ taglib prefix="fmtPrintAcctPriceIncrease" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\GmPrintAcctPriceIncrease.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtPrintAcctPriceIncrease:setLocale value="<%=strLocale%>"/>
<fmtPrintAcctPriceIncrease:setBundle basename="properties.labels.sales.GmPrintAcctPriceIncrease"/>



<%
try {
	
	
	
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	Logger log = GmLogger.getInstance(GmCommonConstants.SALES);
	

	

		
	String strAccId 	 =  GmCommonClass.parseNull((String)request.getAttribute("hAccId"));
	String strProjectIds =  GmCommonClass.parseNull((String)request.getAttribute("hProjectIds"));
	
	
	String strOpt 		 =  GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
	
	
	String strLine = "";
	
	
	String strDefaultTableClass = "DtTable1000";
	int   intDefaultColSpan     = 3 ;
	
	  int pageSize = 29; // Need to be a prime number
	final int projNRows = 24 ; 
	
	int pageCount = 1;
	boolean bFlag = false;
	
	
	

	HashMap    hmAccList=null;
	ArrayList  alReportData = new ArrayList();
	
	String     strProjName, strPartNumber, strPartDescription, strPrice2007, strPrice2008 ;
	
	intDefaultColSpan = ( strOpt.equals("PriceIncrease") ? 6 : intDefaultColSpan  );
	strDefaultTableClass = ( strOpt.equals("PriceIncrease") ? "DtTable850" : strDefaultTableClass  );
	
	HashMap hmStartedProjects = new HashMap();
	
	
	HashMap  hmReturn = (HashMap)request.getAttribute("hmReturn");
	
	if (hmReturn != null) 
	{	
		hmAccList = (HashMap)hmReturn.get("ACCDETAIL");
	}
	
	
	RowSetDynaClass resultSet = (RowSetDynaClass) request.getAttribute("ACCPRICEREPORT");
	
	if(resultSet != null)
		alReportData = (ArrayList)resultSet.getRows();
	
	
	
	
	//log.debug("From GMC: " + GmCommonClass.getString("GMADDRESS"));
	String gmAddress =  GmCommonClass.getString("GMADDRESS");
	//gmAddress = gmAddress.replaceAll("\\","<br>");
	StringTokenizer stk = new StringTokenizer(gmAddress,"/");
	gmAddress = "<b>" ;
	String token;
	int j  = 0 ;
	final int nTokens = stk.countTokens();
	
	while( stk.hasMoreTokens() )
	{
		 
		gmAddress +=  (String)stk.nextToken() + "<br>" ;  
		
		if(j == 0 )
		   gmAddress += "</b>";
		
		if ( j == nTokens - 2 )
			gmAddress += "<font size=-2>" ;
			
		j++ ;
	}
	
	gmAddress += "</font>";
	
	StringBuffer sbHtmlCode = new StringBuffer();
	
	//
	  sbHtmlCode.append("<table> <tr> <td width='880'>"); //outer table added
	
	
	  sbHtmlCode.append("<table border='0' width='880' cellspacing='0' cellpadding='0' align='center'>");
	  sbHtmlCode.append("<tr><td bgcolor='#666666' colspan='3'></td></tr>");
	  sbHtmlCode.append("<tr>");
	  sbHtmlCode.append("<td bgcolor='#666666' width='1' rowspan='6'></td>");
	  sbHtmlCode.append("<td>");
	  sbHtmlCode.append("<table border='0' cellpadding='0' cellspacing='0' border='0' >"); //width='100%'
	  sbHtmlCode.append("<tr>");
	  sbHtmlCode.append("<td class='RightText' valign='top' width=180>");
	  sbHtmlCode.append("<img src='images/s_logo.gif' width='160' height='60'>");
	  sbHtmlCode.append("</td>");
	  sbHtmlCode.append("<td align='left'  > ");
	  sbHtmlCode.append(gmAddress);
	  
      sbHtmlCode.append("</td>	");		
      sbHtmlCode.append("</tr>");
      sbHtmlCode.append("</table>");
      sbHtmlCode.append("</td>");
      sbHtmlCode.append("<td bgcolor='#666666' width='1' rowspan='6'></td></tr>");
      sbHtmlCode.append("<tr><td bgcolor='#666666'></td></tr>");
      sbHtmlCode.append("<tr>");
   
      sbHtmlCode.append("<!-- middle table -->");
      sbHtmlCode.append("<td width='878' height='1' valign='top'>");
  
      sbHtmlCode.append("<table   cellspacing='0' cellpadding='0'>"); // width='100%'

      sbHtmlCode.append("<tr bgcolor='#eeeeee' class='RightTableCaption' font=7> ");
      sbHtmlCode.append("<td height='25' align='left' width=36% >&nbsp;Account Name: "+hmAccList.get("NAME")+" </td>"); 

	  //sbHtmlCode.append("<td height='25' align='center'>&nbsp;"+ hmAccList.get("NAME")+" </td> ");
										
	  sbHtmlCode.append("		 <td  align='center'  width='30%'>&nbsp;Account ID: "+  hmAccList.get("ID") +"</td> ");
	  
	  sbHtmlCode.append("		 <td  align='center' width='33%'>&nbsp;Report Date: "+  hmAccList.get("DT")  +"</td> ");
	  
	     
										
	  //sbHtmlCode.append("	 <td  align='center'>&nbsp;id</td> ");
	  sbHtmlCode.append("	</tr> ");
 
	  sbHtmlCode.append("	 </table>");
 
	  sbHtmlCode.append("	 <table   cellspacing='0' cellpadding='0'>"); //width='100%'
	  sbHtmlCode.append("	 <tr> ");
      sbHtmlCode.append("	 <td align='center' rowspan='1' valign='top'  width='350'>"); 

      sbHtmlCode.append("		Region:" + hmAccList.get("REGION"));
      sbHtmlCode.append("	 </td> ");
      sbHtmlCode.append("<td class='RightText' align='center'  width='350'>&nbsp;Area Director: " + hmAccList.get("AREADIR") +" </td>"); 
      sbHtmlCode.append("<td height='25' class='RightText' align='center'  width='350'>&nbsp; Distributor: "+ hmAccList.get("DISTRIBUTOR")+" </td>"); 

       sbHtmlCode.append("</tr> ");

       sbHtmlCode.append("<tr>"); 
       sbHtmlCode.append("<td align='center' rowspan='1' valign='top'  width='350'>"); 

       sbHtmlCode.append("Territory:" + hmAccList.get("TERRITORY"));
       sbHtmlCode.append("</td> ");
       sbHtmlCode.append("<td class='RightText' align='center'  width='350'>&nbsp;Rep: "+hmAccList.get("REP")+"</td>"); 
       sbHtmlCode.append("<td height='25' class='RightText' align='center'  width='350'>&nbsp;   </td>");
       sbHtmlCode.append(" </tr> ");
       sbHtmlCode.append(" </table> "); 
       sbHtmlCode.append("<!-- middle table -->");
       sbHtmlCode.append("</td>");
       sbHtmlCode.append("</tr>");
	   sbHtmlCode.append("<tr>  <td width='880' height='100' valign='top'>");
	   sbHtmlCode.append("<table border='0' width='100%' cellspacing='0' cellpadding='0'>");
	   sbHtmlCode.append("<tr><td>");		
	   sbHtmlCode.append("<table  border='0'  cellspacing='0' cellpadding='0' id='myTable'><tr>");
	   sbHtmlCode.append("<td colspan='7' height='1' bgcolor='#666666'></td></tr><tr bgcolor='#eeeeee' class='RightTableCaption'>");
	   
	   //sbHtmlCode.append("<td height='25' width='250'  align='center'>Project Name</td>");
	   
       sbHtmlCode.append("					<td width='100'>Part #</td>");
       sbHtmlCode.append("<td width='400' align='center'>Part Description</td>");
       sbHtmlCode.append("<td align='center' width='100'>2007 Price</td>");
       sbHtmlCode.append("<td align='center' width='100'>2008 Price</td>");
       sbHtmlCode.append("</tr>");
       sbHtmlCode.append("<tr><td colspan='7' height='1' bgcolor='#666666'></td></tr>"); 
	
       sbHtmlCode.append("<!--");
       sbHtmlCode.append("your data rows");
       sbHtmlCode.append("-->");

     //
	

     
     StringBuffer sbHtmlInnerTable = new StringBuffer();
	
	//
	  sbHtmlInnerTable.append("<table> <tr> <td width='880'>"); //outer table added
	
	
	  sbHtmlInnerTable.append("<table border='0' width='880' cellspacing='0' cellpadding='0' align='center'>");
	  
	  //sbHtmlInnerTable.append("<tr><td bgcolor='#666666' colspan='3'></td></tr>");
	  
	  sbHtmlInnerTable.append("<tr>");
	  
	  
	  sbHtmlInnerTable.append("<td bgcolor='#666666' width='1' rowspan='6'></td>");
	  
	   sbHtmlInnerTable.append("<td>");
	  
	  /*sbHtmlInnerTable.append("<table border='0' cellpadding='0' cellspacing='0' border='0' >"); //width='100%'
	  sbHtmlInnerTable.append("<tr>");
	  sbHtmlInnerTable.append("<td class='RightText' valign='top' width=180>");
	  sbHtmlInnerTable.append("<img src='images/s_logo.gif' width='160' height='60'>");
	  sbHtmlInnerTable.append("</td>");
	  sbHtmlInnerTable.append("<td align='left'  > ");
	  sbHtmlInnerTable.append(gmAddress);
	  
	  sbHtmlInnerTable.append("</td>	");		
	  sbHtmlInnerTable.append("</tr>");
	  sbHtmlInnerTable.append("</table>");
	  
	  */
	  
	  
	  sbHtmlInnerTable.append("</td>");
	  
	  
	  
	  sbHtmlInnerTable.append("<td bgcolor='#666666' width='1' rowspan='6'></td>");
	  
	  sbHtmlInnerTable.append("</tr>");
	  
	  
	  
	  sbHtmlInnerTable.append("<tr><td bgcolor='#666666'></td></tr>");
	  sbHtmlInnerTable.append("<tr>");
   
	  sbHtmlInnerTable.append("<!-- middle table -->");
	  sbHtmlInnerTable.append("<td width='878' height='1' valign='top'>");
  
	  sbHtmlInnerTable.append("<table   cellspacing='0' cellpadding='0'>"); // width='100%'

	  sbHtmlInnerTable.append("<tr bgcolor='#eeeeee' class='RightTableCaption'> ");
	  	  
	  
	  sbHtmlInnerTable.append("<td height='25' align='left' width=36% font=7 >&nbsp;Account Name: "+hmAccList.get("NAME")+" </td>"); 

										
	  sbHtmlInnerTable.append("<td  align='center' width='30%'   >&nbsp;Account ID: "+  hmAccList.get("ID") +"</td> ");
	  
	  sbHtmlInnerTable.append("		 <td  align='center' width='33%'  >&nbsp;Report Date: "+  hmAccList.get("DT")  +"</td> ");
										
	  sbHtmlInnerTable.append("	</tr> ");
 
	  sbHtmlInnerTable.append("	 </table>");
 
	  
	  sbHtmlInnerTable.append("<!-- middle table -->");
	  sbHtmlInnerTable.append("</td>");
	  sbHtmlInnerTable.append("</tr>");
	  sbHtmlInnerTable.append("<tr>  <td width='880' height='100' valign='top'>");
	  sbHtmlInnerTable.append("<table border='0' width='100%' cellspacing='0' cellpadding='0'>");
	  sbHtmlInnerTable.append("<tr><td>");		
	  sbHtmlInnerTable.append("<table  border='0'  cellspacing='0' cellpadding='0' id='myTable'><tr>");
	  sbHtmlInnerTable.append("<td colspan='7' height='1' bgcolor='#666666'></td></tr><tr bgcolor='#eeeeee' class='RightTableCaption'>");
	   
	   
	   
      sbHtmlInnerTable.append("<td width='100'>Part #</td>");
      sbHtmlInnerTable.append("<td width='400' align='center'>Part Description</td>");
      sbHtmlInnerTable.append("<td align='center' width='100'>2007 Price</td>");
      sbHtmlInnerTable.append("<td align='center' width='100'>2008 Price</td>");
      sbHtmlInnerTable.append("</tr>");
      sbHtmlInnerTable.append("<tr><td colspan='7' height='1' bgcolor='#666666'></td></tr>"); 
	
      sbHtmlInnerTable.append("<!--");
      sbHtmlInnerTable.append("your data rows");
      sbHtmlInnerTable.append("-->");

	
       
       
       
       
	
	
	//
	
	StringBuffer sbTableEnd = new StringBuffer();
		
	sbTableEnd.append("<!-- end part -->");	
	sbTableEnd.append("	<tr><td colspan='6' bgcolor='#666666'></td></tr>");
	sbTableEnd.append(" <!-- Must part below -->");
	sbTableEnd.append(" </table>");
	sbTableEnd.append("	</td>");
	sbTableEnd.append("	</tr>");
	sbTableEnd.append(" </table>");
	sbTableEnd.append(" </td>");
	sbTableEnd.append(" </tr>");
	sbTableEnd.append(" </table>");
		
    //end of newly adde outer table
    sbTableEnd.append("</td></tr></table>");		
	sbTableEnd.append(" <p STYLE=\"page-break-after: always\"></p>");								


	sbTableEnd.append("<br><br>");
	
	
	
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Account Pricing Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>

<script>

var tdinnner = "";
var tdinnner2 = "";

function hidePrint()
{
	strObject = eval("document.all.button1");	
	tdinnner = strObject.innerHTML;	
	strObject.innerHTML = "";
	
	
	strObject = eval("document.all.button2");	
	tdinnner2 = strObject.innerHTML;	
	strObject.innerHTML = "";
	
	
}

function showPrint()
{
	strObject = eval("document.all.button1");
	strObject.innerHTML = tdinnner ;
	
	
	strObject = eval("document.all.button2");
	strObject.innerHTML = tdinnner2 ;
	
	
}

function fnPrint()
{
	window.print();
}

</script>


</HEAD>

<BODY leftmargin="20" topmargin="10"  onbeforeprint="hidePrint();" onafterprint="showPrint();" >
<FORM name="frmSales" method="POST" action="<%=strServletPath%>/GmSalesAcctPriceReportServlet">


	

	<table width='880'>  
						 
						 <tr> <td colspan='<%= intDefaultColSpan%>' height='30' align='center' id='button1'>
							<fmtPrintAcctPriceIncrease:message key="BTN_PRINT" var="varPrint"/>
							 <gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" buttonType="Load" onClick="fnPrint();" />
							 &nbsp;&nbsp;
							 <fmtPrintAcctPriceIncrease:message key="BTN_CLOSE" var="varClose"/>
							 <gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" buttonType="Load" onClick="window.close();"/>
							 
							  </td> </tr>		
						 
    </table>   


<!--  Display data in multiple pages with logo at top -->

<%=sbHtmlCode.toString()%>

 <%
 					   final int intSize = alReportData.size();
 					   DynaBean dynaBean;
 					   int intPageNRows=0;
 					   String strLastProject="";
			
 					   for ( int i = 0 ; i < intSize ; i++ )
						{
 						    
 						    dynaBean = (DynaBean)alReportData.get(i);							
							
							strProjName 		= 	 GmCommonClass.parseNull((String)dynaBean.get("PJNM"));
							strPartNumber 		= 	 GmCommonClass.parseNull((String)dynaBean.get("PID"));
							strPartDescription  = 	 GmCommonClass.parseNull((String)dynaBean.get("PDESC"));
							strPrice2007		= 	 String.valueOf(dynaBean.get("UPRICE"));
							strPrice2008		= 	 String.valueOf(dynaBean.get("PRICE2008")); 
						
							
							out.print(strLine);
							
							if( !strProjName.equals(strLastProject) ) {
								
								//// New project should not start near the end of page
								//if ((i > 0) && (i % projNRows) == 0)
								if( intPageNRows > 	projNRows )
								{					
									pageSize = 35;
								    pageCount++;
								    intPageNRows = 0 ; 
									out.println("	<tr><td colspan='6' height='1' bgcolor='#666666'></td></tr>");
									out.println("	<tr><td colspan='6' height='20' class='RightText' align='right'>Cont. on Next Page -></td></tr>");															
									out.println(sbTableEnd);																
									//out.println(sbHtmlCode);
									out.println(sbHtmlInnerTable);
									
									

								}//End  Page Over if i % pageSize=0		
								
								/////
							%>
							
								<tr><td colspan='6' height='1' bgcolor='#666666'></td></tr>
								<tr>  <td align="center" colspan=5 height='25' bgcolor='#dddddd'><fmtPrintAcctPriceIncrease:message key="LBL_PRODUCT_NAME"/> :&nbsp;  <%=strProjName%> </td>
								</tr><tr><td colspan='6' height='1' bgcolor='#666666'></td></tr>
								
							<% 	strLastProject = strProjName; 							     
								} 
							%>
							
							
							<%  //If project continues on next page, display project name continue row.
								// if 1st row and project started.  
							
								if ( intPageNRows == 0  && hmStartedProjects.containsKey(strProjName) )
								{
									out.println("<tr><td colspan='6' height='1' bgcolor='#666666'></td></tr>");
									out.println("<tr>  <td align='center' colspan=5 height='25' bgcolor='#dddddd'>Product Name :&nbsp; " + strProjName +  "(continued) </td> ");
									out.println("</tr><tr><td colspan='6' height='1' bgcolor='#666666'></td></tr>");
								}
							
							%>
							
							
						    <%   //if (  (Double.valueOf(strPrice2007) ).doubleValue() != 0 ) { 		%> 
							
 					  		<tr  height='22' >									
								<!--  td class="RightText" height="20">&nbsp;strProjName</td-->   
								<td class="RightText">&nbsp;<%=strPartNumber%></td>
								<td class="RightText" align="left"><%=strPartDescription%></td>
								<td class="RightText" align="right">$<%=GmCommonClass.getStringWithCommas(strPrice2007)%>&nbsp;&nbsp;</td>								
								<td class="RightText" align="right">$<%=GmCommonClass.getStringWithCommas(strPrice2008)%>&nbsp;&nbsp;&nbsp;</td>	
																										
							</tr>
							<tr><td colspan='6' height='1' bgcolor='#999999'></td></tr>
							
							
							
							<% //}
							
							    intPageNRows++ ; 
							
								hmStartedProjects.put(strProjName,strProjName);
							
							%>
							
							
							
							<%  //If Page Over
							
							//!Double check if ((i > 0) && (i % pageSize) == 0)
							if ( intPageNRows == pageSize ) 
							{
								pageSize = 35; 	
								pageCount++;			
								intPageNRows = 0 ; 
								if ( i != (intSize-1) )
								{
									out.println("	<tr><td colspan='6' height='1' bgcolor='#666666'></td></tr>");
									out.println("	<tr><td colspan='6' height='20' class='RightText' align='right'>Cont. on Next Page -></td></tr>");		
								}							
								
								//!!out.println(sbTableEnd);
								if ( i < (intSize-1) ) {
									out.println(sbTableEnd);
									out.println(sbHtmlInnerTable);
								}

							}//End  Page Over if i % pageSize=0
							     
							
							//If bflag
							
							
							//if(i > 0 && i % pageSize == 0 && i == (intSize -1))	
							  if( intPageNRows == pageSize && i == (intSize -1))
	  						  {
							   						
								
	   						   bFlag = true;						     
	%>
							   <table width='880'>
							  
	<%							     
	    					  }  //end if of bflag
							
					}//End of FOR Loop
					
 %>
							

<% 

if(!bFlag)
{
		
		
%>
								
					</td>
				</tr>
			
		
<%  out.println("</table> </td> </tr></table>");  //trial error   
   } //End  if!bFlag %>

							
							 
						 <!-- table width='880' -->	
						 <tr><td width='880' colspan='8' height='1' bgcolor='#666666'></td></tr>
						 <tr> <td colspan='<%= intDefaultColSpan%>' height='30' align='center' id='button2'>
							 <fmtPrintAcctPriceIncrease:message key="BTN_PRINT" var="varPrint"/>
							 <gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrint();" />
							 &nbsp;&nbsp;
							 <fmtPrintAcctPriceIncrease:message key="BTN_CLOSE" var="varClose"/>
							 <gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" />
							 
							  </td> </tr>		
						 <tr><td width='880' colspan='7' height='1' bgcolor='#666666'></td></tr>
						 </table>   
						
						 
									
			 					        
			        
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
