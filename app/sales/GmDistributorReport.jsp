<%
/**********************************************************************************
 * File		 				: GmRequestMasterAction.jsp
 * Desc		 				: REquest Master report
 * Version	 				: 1.0
 * author					: D James
 * Modified to DHTMLX By 	: RAJKUMAR JAYAKUMAR
************************************************************************************/
%>

<%@include file="/common/GmHeader.inc"%> 
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<bean:define id="gridData" name="frmDistributorReport" property="strXmlString" type="java.lang.String"></bean:define>

<%@ taglib prefix="fmtDistributorReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- sales\GmDistributorReport.jsp -->

<fmtDistributorReport:setLocale value="<%=strLocale%>"/>
<fmtDistributorReport:setBundle basename="properties.labels.sales.GmDistributorReport"/>

<% 

String strWikiTitle = GmCommonClass.getWikiTitle("FIELD_SALES_REPORT");
String strDistId = "0";

//Below code is to track down the visits on the report.
String strUserId = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
String strShUserName = GmCommonClass.parseNull((String)session.getAttribute("strSessShName"));
String strUserName = strUserId + " - " + strShUserName;
//Visit code ends
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Distributor Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />


<script type="text/javascript">

var gridObjData='<%=gridData%>';
var mygrid;

function fnOnPageLoad() {
	if(gridObjData!='')
	{
		mygrid = initGrid('dataGridDiv',gridObjData);  
	}
}


function fnSubmit()
{
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmDistributorReport.strOpt.value = 'selectfilters';
	//document.frmDistributorReport.submit();
	fnReloadSales();
}
function fnCallAccountRpt(val)
{
	document.frmDistributorReport.action = "<%=strServletPath%>/GmAccountReportServlet";
	document.frmDistributorReport.hDistId.value = val;
	document.frmDistributorReport.hOpt.value = "ALL";
	//document.frmDistributorReport.submit();
	fnReloadSales();
}
function fnCallRepRpt(val)
{
	document.frmDistributorReport.action = "<%=strServletPath%>/GmAccountReportServlet";
	document.frmDistributorReport.hDistId.value = val;
	document.frmDistributorReport.hOpt.value = "REPALL";
	//document.frmDistributorReport.submit();
	fnReloadSales();
}


</script>
<script> downloadPiwikJS();</script>
<script> fnTracker('Field Sales Report', '<%=strUserName%>');</script>

</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();" >

<html:form action="/gmDistributorReport.do">
	
 	<input type="hidden" name="hDistId" value="">
 	<input type="hidden" name="hOpt" value="">
	<html:hidden property="strOpt" value="" />
	<table border="0" class="GtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" height="25" class="RightDashBoardHeader">&nbsp;<fmtDistributorReport:message key="LBL_FIELD_SALES_REPORT"/></td>
			<td align="right" class=RightDashBoardHeader > 	
				<fmtDistributorReport:message key="IMG_ALT_HELP" var = "varHelp"/>
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
		</tr>	
				<!-- Addition Filter Information -->
		<tr><td colspan="4">
				<jsp:include page="/sales/GmSalesFilters.jsp" >
				<jsp:param name="FRMNAME" value="frmDistributorReport" />
				<jsp:param name="HIDE" value="SYSTEM" />
				<jsp:param name="HIDEBUTTON" value="HIDEGO" />
				</jsp:include>
		</td></tr>
				<tr width = 850 height="18" bgcolor="#eeeeee" class="RightTableCaption">
					<td >&nbsp;<fmtDistributorReport:message key="LBL_CATEGORY"/></td>
					<td >&nbsp;<fmtDistributorReport:message key="LBL_STATE"/></td>
					<td colspan="2"> </td>
				</tr>
				<tr>
					<td >
					<br>
					<DIV
						style="display: visible; height: 100px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 8px; margin-right: 8px;">
					<table>
						<logic:iterate id="allCategorylist" name="frmDistributorReport" property="allCategories">
							<tr>
								<td><htmlel:multibox property="selectedCategories" value="${allCategorylist.CODEID}" />
									 <bean:write name="allCategorylist" property="CODENM" /></td>
							</tr>
						</logic:iterate>
					</table>
					</DIV>
					<BR>
					</td>
					<td>
					<DIV
						style="display: visible; height: 100px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 8px; margin-right: 8px;">
					<table>
						<logic:iterate id="allStatelist" name="frmDistributorReport" property="allStates">
							<tr>
								<td><htmlel:multibox property="selectedStates" value="${allStatelist.CODEID}" /> 
								<bean:write name="allStatelist" property="CODENM" /></td>
							</tr>
						</logic:iterate>
					</table>
					</DIV>	
					
		           <td>&nbsp;<fmtDistributorReport:message key="LBL_ACTIVE"/>: <html:checkbox property="selectedActive" />
		               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtDistributorReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button" buttonType="Load" onClick="fnSubmit();" />
		           </td>
		        </tr>
				<tr>
			<td  colspan="4" width="100%">
				<div id="dataGridDiv" style=" height: 500px; width: 848px;"></div>
			</td>
		</tr>
		<tr>
				<td colspan="6" align="center" width="850">
				<div class='exportlinks'><fmtDistributorReport:message key="DIV_EXPORT_OPT"/> : <img
					src='img/ico_file_excel.png' />&nbsp;<a href="#"
					onclick="mygrid.toExcel('/phpapp/excel/generate.php');"> <fmtDistributorReport:message key="DIV_EXCEL"/>
				</a></div>
				</td>
			</tr>
	</table>
	
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>

