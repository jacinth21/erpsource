<%
/**********************************************************************************
 * File		 		: GmControlNumberCartInclude.jsp
 * Desc		 		: This screen is used to edit the control number details
                      used in O.R.
 * Version	 		: 1.0
 * author			: Venkata Prasath
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@page import="java.util.Date"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib prefix="fmtUsedLotNumberInclude" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- custservice\GmUsedLotNumberInclude.jsp -->
<fmtUsedLotNumberInclude:setLocale value="<%=strLocale%>"/>
<fmtUsedLotNumberInclude:setBundle basename="properties.labels.custservice.GmUsedLotNumberInclude"/>
<%
    String strCustomerServiceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	ArrayList alUsageDetails = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALUSEDLOTDTLS"));
	String strSkpUsdLotValid =  GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SKP_USED_LOT_VALID"));
	String strInvoiceId = GmCommonClass.parseNull(request.getParameter("InvoiceId"));
	
%>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strCustomerServiceJsPath%>/GmUsedLotNumberInclude.js"></script>
<script>
//members 
this.keyArray = new Array(); // Location Keys 
this.valArray = new Array(); // Location Values 
         
// methods 
this.put = put; 
this.get = get; 
this.findIt = findIt;

var count = 0;
var usedLotSize = <%=alUsageDetails.size()%>;
</script>
<%	
// When click the add rows - at the time to show the below string 
HashMap hmControlNumDet = new HashMap();
String strChooseOne = "<option value=\"0\" >[Choose One]</option>";
String strOptionString = "";
for (int i=0;i< alUsageDetails.size();i++)
{
	hmControlNumDet = (HashMap) alUsageDetails.get(i);
	String strPnum = GmCommonClass.parseNull((String) hmControlNumDet.get("PNUM"));
	ArrayList alLot = GmCommonClass.parseNullArrayList((ArrayList)hmControlNumDet.get("LOT_DTLS"));
	strOptionString = "";
	for(int j=0;j< alLot.size(); j++){
	  HashMap hmTemp = (HashMap) alLot.get(j); 
	  String strId = GmCommonClass.parseNull((String) hmTemp.get("ID"));
	  String strName = GmCommonClass.parseNull((String)hmTemp.get("NAME"));
	  strOptionString += " <option value=\""+strId+"\">"+strName+"</option>";
	}
	strOptionString = strChooseOne + strOptionString;
	%>
	<script>put('<%=strPnum%>','<%=strOptionString%>');</script>
	<%} %>

<table cellpadding="0" cellspacing="0" width="100%" border="1" bordercolor="gainsboro" id="EditControlNumber">
			  <thead>
				<TR class="Shade" class="RightTableCaption" style="position:relative;">
				<%-- <% if (strFormName.equals("frmVendor")) { %>
					<TH class="RightText" width="40" align="center">Remove</TH>
				<% } %>	 --%>
					<TH class="RightText" width="110" align="center" height="25"><fmtUsedLotNumberInclude:message key="LBL_PART"/></TH>
					<TH class="RightText" width="330" align ="left">&nbsp;<fmtUsedLotNumberInclude:message key="LBL_PART_DESCRIPTION"/></TH>
					<TH class="RightText" width="50" align="center"><fmtUsedLotNumberInclude:message key="LBL_ORDER"/><br><fmtUsedLotNumberInclude:message key="LBL_QTY"/></TH>
					<TH class="RightText" width="50" align="center"><fmtUsedLotNumberInclude:message key="LBL_QTY"/></TH>
					<TH class="RightText" width="210" align="center"><fmtUsedLotNumberInclude:message key="LBL_CONTROL"/></TH>
				</TR>	
			  </thead>
			  <TBODY>
<%
					int intSize  = alUsageDetails.size();

					if ( intSize == 0 )
					{
						%>
						<tr >
						<td  height="20" class="RightText" colspan = 6>
							No data found.
						</td>
						</tr>
					<%	
					}

					for (int i=0;i<intSize;i++)
			  		{
						HashMap hmControlNum =(HashMap)alUsageDetails.get(i);
						String strDesc ="";
						String strPartNum ="";
						String strQty = "";
						String strItemQty = "";
						String strControlNum ="";
						String strPkey = "";
						String strCnumNeedFl = "";
						String strUsageDetailId = "";
						String strDDTNum = "";
						String strItemOrderId = "";
						strPartNum =  GmCommonClass.parseNull((String)hmControlNum.get("PNUM"));
						strDesc =  GmCommonClass.parseNull((String)hmControlNum.get("PDESC"));
						strQty =  GmCommonClass.parseNull((String)hmControlNum.get("QTY"));
						ArrayList alList = GmCommonClass.parseNullArrayList((ArrayList)hmControlNum.get("LOT_DTLS"));
						int intAlListSize = alList.size();
						String strSelected = "";
						String strDisabled = strQty.equals("1")?"disabled":"";
						if(intAlListSize==0){
						  strDisabled = "disabled";
						}
						String StrDisableQty = (!strInvoiceId.equals(""))?"disabled":"";
						
						String strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
%>
								<tr <%=strShade %>>
										<%-- <% if (strFormName.equals("frmVendor")) { %>
											<td><input type="checkbox" name="chkVoidOrder<%=i+1%>"></input> </td>
										<% } %>	 --%>
									<td  height="20" class="RightText">
									<% if(!strQty.equals("1")){ %>
										&nbsp;<a href="javascript:fnAddNewRow(<%=i+1%>,'<%=strPartNum %>');" ><IMG id="partInc<%=i+1%>" border=0 src="<%=strImagePath%>/plus.gif"></a> <%} %>&nbsp;&nbsp;<%=strPartNum%>
										<input type="hidden" name="hUsagePartNum<%=i+1%>" tabindex="-1" value="<%=strPartNum%>" />
								   </td>
									<td id="CellRemove<%=i+1%>" class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%>
								    </td>
								    <td class="RightText" align="center">&nbsp;<%=strQty %></td>
									<td align="center" id="Qty<%=i+1%>" class="RightText">&nbsp;<input type="text" <%=StrDisableQty%> name="txt_usage_qty<%=i+1%>" value="" size="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" /> 
									<input type="hidden" name="hLotQty<%=i+1%>" value="<%=strQty%>" tabindex="1" size="2">
									</td>
									<td id="controlNum<%=i+1%>" class="RightText" align="left">&nbsp;
									<select name="Cbo_controlNum<%=i+1%>" id="Cbo_controlNum<%=i+1%>" style="width: 150px" <%=strDisabled%>><option value="0" id="0"><fmtUsedLotNumberInclude:message key="LBL_CHOOSE_ONE"/></option>
									<%
									strOptionString = "";
									int intTempSize = alList.size();
									strSelected = "";
									if(intTempSize ==1){
									  strSelected = "selected";
									}
									for(int j=0;j< alList.size(); j++){
									  HashMap hmTemp = (HashMap) alList.get(j); 
									  String strId = GmCommonClass.parseNull((String) hmTemp.get("ID"));
									  String strName = GmCommonClass.parseNull((String)hmTemp.get("NAME"));
									  
									  strOptionString += " <option "+strSelected+" value=\""+strId+"\">"+strName+"</option>";
									}
									%>
									<%=strOptionString %></select>
									<input type="hidden" name="hcontrolnum<%=i+1%>"   id="hcontrolnum<%=i+1%>"  tabindex="2"/>
									<fmtUsedLotNumberInclude:message key="LBL_CONTROL_AVAILABLE" var="varControlAvailable"/>
									<fmtUsedLotNumberInclude:message key="LBL_CONTROL_DOESNOT_EXIST" var="varControlDoesNotExist"/>
									<span id="DivShowCnumAvl<%=i+1%>" style=" vertical-align:middle; display: none;"> <img height="16" width="19" title="${varControlAvailable}" src="<%=strImagePath%>/success.gif"></img></span>
						 			<span id="DivShowCnumNotExists<%=i+1%>" style=" vertical-align:middle; display: none;"> <img height="12" width="12" title="${varControlDoesNotExist}" src="<%=strImagePath%>/delete.gif"></img>&nbsp;<a href="javascript:fnViewLotReport('<%=i+1 %>')"><img height="15" border="0" title="Lot Code Report" src="<%=strImagePath%>/location.png"></img></a></span>
									</td>
								</tr>
<%								} // For Loop ends here					
%>
			  </TBODY>
			</table>
			<INPUT type="hidden" name="hTextboxCnt" value="<%=intSize%>" />