 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmOrderItemControl.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%> 
<%@ page import="java.util.Date"%>
<%@ taglib prefix="fmtOrderItemControl" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmOrderItemControl.jsp -->
<fmtOrderItemControl:setLocale value="<%=strLocale%>"/>
<fmtOrderItemControl:setBundle basename="properties.labels.custservice.GmOrderItemControl"/>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	GmCalenderOperations gmCal = new GmCalenderOperations();
	String strCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmOrderItemControl", strSessCompanyLocale);
	String strAccCurrSymb ="";
	// to setting the time zone
	gmCal.setTimeZone(strGCompTimeZone);
	String strApplnDateFmt = strGCompDateFmt;
	String strTOdayDate = gmCal.getCurrentDate(strGCompDateFmt);
	Date dtFrmDate = null;
	Date dtTodayDate = null;
	ArrayList alOrdrTpeList = new ArrayList();
	String strWikiTitle = "";
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strMode = (String)request.getAttribute("hMode")==null?"":(String)request.getAttribute("hMode");
	String strShipFl = (String)request.getAttribute("hShipFl")==null?"":(String)request.getAttribute("hShipFl");
	String strShowResLot = GmCommonClass.parseNull((String)request.getAttribute("ShowReserveLot"));
	HashMap hmParam = (HashMap)request.getAttribute("hmParam");
	alOrdrTpeList = (ArrayList)hmParam.get("ALORDRTYPELIST");
	String strChecked = "";
	int intSize= 0;
	strChecked = strShipFl.equals("2")?"checked":"";
	log.debug("strMode inside OrderItem Control is " +strMode + " Action is " + strhAction );
	String strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CONTROL_NUMBER"));
	if (strhAction.equals("EditShip"))
	{
		strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SHIPPIG_DETAILS"));
		strWikiTitle = GmCommonClass.getWikiTitle("ORDER_ITEM_EDIT_SHIP");
	}
	else if (strhAction.equals("EditPO"))
	{
		strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PO_DETAILS"));
		strWikiTitle = GmCommonClass.getWikiTitle("ORDER_ITEM_EDIT_PO");
	}else if (strhAction.equals("ACKORD")){
		strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ACKNOWLEDGEMENT_ORDER"));
		strWikiTitle = GmCommonClass.getWikiTitle("ACKNOWLEDGEMENT_ORD_EDIT");
	}
	String strOrderId = "";
	String strAccName = "";
	String strUserName = "";
	//Date dtOrdDate = null;
	String strComments = "";
	String strShade = "";
	String strItemQty = "";
	String strControl = "";
	String strAccid = "";
	String strCodeID = "";
	String strSelected = "";
	String strShipDate = "";
	String strShipCarr = "";
	String strShipMode = "";
	String strTrack = "";
	String strShipCost = "";
	String strPO = "";
	String strItemOrdType = "";
	String strItemOrdDesc = "";
	String strDisabled = "";
	String strOrderType = "";
	String strReadOnly = "";
	String strCapFlag = "";
	String strRAReason = "";
	String strInvoiceId = "";
	String strParentOrdId="";
	String strOrdDate = "";
	String strRequiredDt = "";
	String strOrdAtbID = "";
	String strOrdCodeID = "";
	String strOrdAtbVal = "";
	boolean blLoanerFl = false;
	long diff = 0;
	
	String strTodaysDate = strTOdayDate==null?"":strTOdayDate;
	String strDeptId = (String)session.getAttribute("strSessDeptId")==null?"":(String)session.getAttribute("strSessDeptId");

	ArrayList alCart = new ArrayList();
	ArrayList alOrdAtbDetails = new ArrayList();	
	HashMap hmOrderDetails = new HashMap();
	HashMap hcboVal = new HashMap();

	int intPriceSize = 0;
	if (hmReturn != null)
	{
		hmOrderDetails = (HashMap)hmReturn.get("ORDERDETAILS");
		alOrdAtbDetails	= GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ORDATBDETAILS"));
		strOrderId = (String)hmOrderDetails.get("ID");
		strInvoiceId = (String)hmOrderDetails.get("INVOICEID");
		log.debug("invoice id is:"+strInvoiceId);
		
		strAccName = (String)hmOrderDetails.get("ANAME");
		strAccid = (String)hmOrderDetails.get("ACCID");
	    strUserName = (String)hmOrderDetails.get("UNAME");
		//dtOrdDate = (Date)hmOrderDetails.get("ODT");
		strOrdDate = GmCommonClass.getStringFromDate((java.sql.Date)hmOrderDetails.get("ODT"),strApplnDateFmt);
		strComments = (String)hmOrderDetails.get("COMMENTS");
		strPO = (String)hmOrderDetails.get("PO");
		strOrderType = GmCommonClass.parseNull((String)hmOrderDetails.get("ORDERTYPE"));
		strReadOnly = strOrderType.equals("2530")?"disabled":"";
		strRAReason =	GmCommonClass.parseNull((String)hmOrderDetails.get("RAREASON"));
		strParentOrdId =	GmCommonClass.parseNull((String)hmOrderDetails.get("PARENTORDID"));
		strRequiredDt = GmCommonClass.parseNull((String)hmOrderDetails.get("REQDATE"));	
		strAccCurrSymb = GmCommonClass.parseNull((String)hmOrderDetails.get("ACC_CURR_SYMB"));
		
		strCurrSign = strAccCurrSymb.equals("")?strCurrSign:strAccCurrSymb; 
		alCart = (ArrayList)hmReturn.get("CARTDETAILS");

		HashMap hmShipDetails = (HashMap)hmReturn.get("SHIPDETAILS");

		if (hmShipDetails != null)
		{
			strShipDate = GmCommonClass.getStringFromDate((java.sql.Date)hmOrderDetails.get("SDT"),strApplnDateFmt);
			strShipCarr = (String)hmShipDetails.get("SCAR");
			strShipMode = (String)hmShipDetails.get("SMODE");
			strTrack = (String)hmShipDetails.get("STRK");
			strShipCost = GmCommonClass.parseZero((String)hmShipDetails.get("SCOST"));
			//strShipCarrNm = (String)hmShipDetails.get("SCARNM");
			//strShipModeNm = (String)hmShipDetails.get("SMODENM");
			
			strShipDate = strShipDate.equals("")?strTodaysDate:strShipDate;
			strShipCarr = strShipCarr.equals("")?"5001":strShipCarr;
			strShipMode = strShipMode.equals("")?"5005":strShipMode;
		}

		intSize = alOrdAtbDetails.size();
		hcboVal = new HashMap();
		for (int i = 0; i < intSize; i++){
			hcboVal = (HashMap) alOrdAtbDetails.get(i);
			strOrdAtbID = (String) hcboVal.get("OAID");
			strOrdCodeID = (String) hcboVal.get("TRTYPEID");
			strOrdAtbVal = (String) hcboVal.get("ATTRVAL");
			//103926: Required Date
			if(strOrdCodeID.equals("103926")){
				dtFrmDate = (Date)GmCommonClass.getStringToDate(strOrdAtbVal,strApplnDateFmt); // Required Date
				dtTodayDate = (Date)GmCommonClass.getStringToDate(strTOdayDate,strApplnDateFmt);// Today Date
				// Calculating Date diff
				try{
					diff = dtTodayDate.getTime() - dtFrmDate.getTime();
					diff = diff / (1000 * 60 * 60 * 24);
				 	if(diff >= 0){
						dtFrmDate = dtTodayDate;
				    }
				}catch(Exception e){
				    e.printStackTrace();
				}
			}
		}
	}

	HashMap hmShip = new HashMap();
	ArrayList alCarrier = new ArrayList();
	ArrayList alMode = new ArrayList();
	ArrayList alShipTo = new ArrayList();
	ArrayList alRepList = new ArrayList();

	hmShip = (HashMap)request.getAttribute("hmShipLoad");
	String strShipTo = "";
	String strShipToId = "";
	String strShipLable = "";
	String strOrdLabel = "Order ID:";
	if (hmShip != null)
	{
		alCarrier = (ArrayList)hmShip.get("CARRIER");
		alMode = (ArrayList)hmShip.get("MODE");
		alShipTo = (ArrayList)hmShip.get("SHIPLIST");
		alRepList = (ArrayList)hmShip.get("REPLIST");
		strShipTo = (String)hmOrderDetails.get("SHIPTO");
		strShipToId = (String)hmOrderDetails.get("SHIPTOID");
	}
	if (!strhAction.equals("EditShip") && !strhAction.equals("EditPO") && !strRAReason.equals("3316") && !strhAction.equals("ACKORD"))
	{
		strShipLable = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_RELEASE_FOR_SHIPPING"));
	}
	if (!strhAction.equals("EditShip") && !strhAction.equals("EditPO") && strRAReason.equals("3316"))
	{
		strShipLable = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_COMPLETE"));
	}
	if(strOrderType.equals("101260")){
		strOrdLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ACK_ID"));
	}
	String strValidateFlag = (String)request.getAttribute("VALIDATEFLAG")==null?"":(String)request.getAttribute("VALIDATEFLAG");
	//PMT-30867 -  ACk Screen Chnages
	String strReleaseFlag = (String)request.getAttribute("RELEASEQTYFL")==null?"":(String)request.getAttribute("RELEASEQTYFL");
	String strReleaseQtyChecked = strReleaseFlag.equals("Y")?"checked":"";
	%>

<%@page import="java.util.Date"%><HTML>
<HEAD>
<TITLE> Globus Medical: Enter Control Numbers </TITLE>
<link rel="STYLESHEET" type="text/css"	href="<%=strJsPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/customerservice/GmOrderItemControl.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/prodmgmnt/GmRule.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxcommon.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>	
<script>
var servletPath = '<%=strServletPath%>';
var ShipToId = '<%=strShipToId%>';
var custpo = '<%=strPO%>';
var POMessage = '';
var DeptId = '<%=strDeptId%>';
var ParentOrdId = '<%=strParentOrdId%>';
var strComments = '<%=strComments%>';
var strOrdAtbId = '<%=strOrdAtbID%>';
var strOrdCodeId = '<%=strOrdCodeID%>';
var format = '<%=strApplnDateFmt%>';
var currDate = '<%=strTodaysDate%>';
var orderType = '<%=strOrderType%>';
var ousAccId = '<%=strAccid%>';
var validatFlag = '<%=strValidateFlag%>';
var releaseQtyFlag ='<%=strReleaseFlag%>';
</script>


</HEAD>

<BODY leftmargin="20" topmargin="10" onkeydown="fnEnter();" >
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmOrderItemServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hOrderId" value="<%=strOrderId%>">
<input type="hidden" id="Txt_OrdId" name="Txt_OrdId" value="<%=strOrderId%>">
<input type="hidden" name="hAccName" value="<%=strAccName%>">
<input type="hidden" name="hAccId" value="<%=strAccid%>">
<input type="hidden" id="Txt_AccId" name="Txt_AccId" value="<%=strAccid%>">
<input type="hidden" name="hRaReason" value="<%=strRAReason%>">
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="RE_FORWARD" value="gmOrderItemServlet">
<input type="hidden" name="txnStatus" value="PROCESS">
<input type="hidden" name="RE_TXN" value="50925">
<input type="hidden" name="hInvoiceId" value="<%=strInvoiceId%>">
<input type="hidden" name="hInputString" value="">
<input type="hidden" name="hReleaseOrder" value="">
<input type="hidden" name="hOusAccId" value="">

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0" style="border-collapse:separate;">
		<tr>
			<td>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td height="25" class="RightDashBoardHeader" colspan="3"><fmtOrderItemControl:message key="LBL_ORDER_ITEM"/> - <%=strTitle%> <fmtOrderItemControl:message key="LBL_EDIT"/></td>
					<fmtOrderItemControl:message key="LBL_HELP" var="varHelp"/>
					<td height="25" class="RightDashBoardHeader" align="right"><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
				</tr>
				</table>
			</td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="748" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
						<td class="RightText" HEIGHT="20" align="right" width="20%">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="<%=strOrdLabel%>" td="false"/></td>
						<td class="RightText">&nbsp;<b><%=strOrderId%></b></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr class="Shade">
					<fmtOrderItemControl:message key="LBL_ACCOUNT_NAME" var="varAccountName"/>
						<td class="RightText" HEIGHT="20" align="right" width="20%">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varAccountName}:" td="false"/></td>
						<td class="RightText">&nbsp;<b><%=strAccName%></b></td>
						
					</tr>
					<tr class="Shade"> 
					<fmtOrderItemControl:message key="LBL_ACCOUNT_ID" var="varAccountId"/>
						<td class="RightText" HEIGHT="20" align="right" width="20%">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varAccountId}:" td="false"/></td>
						<td class="RightText">&nbsp;<b><%=strAccid%></b></td>
					</tr>					
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					 <tr>
					 <fmtOrderItemControl:message key="LBL_ORDER_ENTERED_BY" var="varOrderEnteredBy"/>
						<td class="RightText" HEIGHT="20" align="right" width="20%">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varOrderEnteredBy}:" td="false"/></td>
						<td class="RightText">&nbsp;<b><%=strUserName%></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtOrderItemControl:message key="LBL_DATE"/> :&nbsp;<b><%=strOrdDate%></b></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr class="Shade">  
					<fmtOrderItemControl:message key="LBL_COMMENTS" var="varComments"/>
						<td class="RightText" valign="top" align="right" HEIGHT="60" width="20%"><gmjsp:label type="RegularText"  SFLblControlName="${varComments}:" td="false"/></td>
						<td class="RightText" valign="top" >&nbsp;<%=strComments%></td>
					</tr>
					
					<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
					<tr  height="20">
						<td class="RightTableCaption" HEIGHT="20" align="right"><fmtOrderItemControl:message key="LBL_NPI"/>:</td>
						<td class="RightText">
							<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="Txt_Npi" />
							<jsp:param name="METHOD_LOAD" value="loadSurgeonName&strOpt=npilist" />
							<jsp:param name="WIDTH" value="155" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="CONTROL_NM_VALUE" value="" />
							<jsp:param name="CONTROL_ID_VALUE" value="" />
							<jsp:param name="SHOW_DATA" value="10" />
							<jsp:param name="TAB_INDEX" value="2" />
							<jsp:param name="AUTO_RELOAD" value="fnSetSurgeonNm(this);" />
							<jsp:param name="ON_BLUR" value="fnAutoFocus(this);" />
							</jsp:include>	
						</td> </tr>
							<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
					<tr class="shade" height="20">
						<td class="RightTableCaption" HEIGHT="20" align="right"><fmtOrderItemControl:message key="LBL_SURGEON"/>:</td>
						<!-- <td class="RightText" HEIGHT="24" >&nbsp;<input type="text" size="20" value="" name="Txt_Surgeon" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"> -->
						<td>
							<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="Txt_Surgeon" />
							<jsp:param name="METHOD_LOAD" value="loadSurgeonName&strOpt=surgeonlist" />
							<jsp:param name="WIDTH" value="200" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="CONTROL_NM_VALUE" value="" />
							<jsp:param name="CONTROL_ID_VALUE" value="" />
							<jsp:param name="SHOW_DATA" value="10" />
							<jsp:param name="TAB_INDEX" value="3" />
							<jsp:param name="AUTO_RELOAD" value="fnSetNPINum(this);" />
							<jsp:param name="ON_BLUR" value="fnAutoFocus(this);" />
							</jsp:include>	
						<div id="npidtls" style="display: inline;">&nbsp;
						<jsp:include page="/custservice/GmNPIProcess.jsp" >															 
							<jsp:param name="npiTxnId" value='' />
						</jsp:include>
						</div> 
						</td>
					</tr>
					
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr>
					<fmtOrderItemControl:message key="LBL_CUSOTMER_PO" var="varCustomerPo"/>
						<td align="right" HEIGHT="24" class="RightText"><gmjsp:label type="RegularText"  SFLblControlName="${varCustomerPo}:" td="false"/></td>
<%
				if (strhAction.equals("EditPO") || strhAction.equals("ACKORD"))
				{
%>
						<fmtOrderItemControl:message key="LBL_PO_ID_AVAILABLE" var="varPoIDAvail"/>
						<fmtOrderItemControl:message key="LBL_PO_ID_ALREADY_EXIST" var="varPoIdExist"/>
						<td class="RightText">&nbsp;<input type="text" size="15" value="<%=strPO%>" name="Txt_PO" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');fnClearDiv();" onBlur="changeBgColor(this,'#ffffff');fnCheckPO('CHECKPO');fnEditStatus();" tabindex=4>
						<span id="DivShowPOIDAvail" style="vertical-align:middle; display: none;"> <img height="24" width="25" title="${varPoIDAvail}" src="<%=strImagePath%>/success.gif"></img></span>
						<span id="DivShowPOIDExists" style="vertical-align:middle; display: none;"><img title="${varPoIdExist}" src="<%=strImagePath%>/delete.gif"></img>&nbsp;<a href="javascript:fnModifyOrder()"><img height="15" title="Order Details" src="<%=strImagePath%>/location.png"></img></a></span>
						<!-- <span id="DivShowLocation" style="vertical-align:middle; display: none;"> --><!-- </span> -->
						</td>
<%
				}else{
%>
						<td class="RightText">&nbsp;<%=strPO%></td>
<%
				}

					// Only for Ack Order we will show the Text Box to enter the Comments which will be stored in the T501_Order.c501_Comments column.
					if (strhAction.equals("ACKORD"))
					{
	%>
				<tr><td class="LLine" colspan="2" height="1"></td></tr>
				<tr class="Shade">
				<fmtOrderItemControl:message key="LBL_ADDITIONAL_INFO" var="varAdditionalInfo"/>
						<td align="right" HEIGHT="24" class="RightText"><b><gmjsp:label type="RegularText"  SFLblControlName="${varAdditionalInfo}:" td="false"/></b></td>

				<td class="RightText">&nbsp;<input type="text" size="35"  maxLength="450" name="Txt_OrderComments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
				 onBlur="changeBgColor(this,'#ffffff');" tabindex=4 value="<%=strComments%>"></td>
				 </tr>
				<tr><td class="LLine" colspan="2" height="1"></td></tr>
				 <tr>
						<td class="RightTableCaption" HEIGHT="24"  align="right"><fmtOrderItemControl:message key="LBL_REQUIRED_DATE"/>:</td>
						<td class="RightText" HEIGHT="24">&nbsp;<gmjsp:calendar textControlName="Txt_ReqDate" 
						              textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtFrmDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="2"/>
						&nbsp;&nbsp;<fmtOrderItemControl:message key="LBL_AUTOFILL_QTY"/>: 
						<input type="checkbox" size="30" name="Chk_ReleaseQty_Fl" value="" <%=strReleaseQtyChecked%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" onclick="fnRealeseQty();">
						</td>
				</tr>
<%
				}

				if (strParentOrdId.equals("") && strDeptId.equals("J") && !strhAction.equals("ACKORD")) // only a/r should have access
				{ 
%>
					</tr>
					<tr>
					<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
						<td colspan="2">
							<jsp:include page="/custservice/GmIncludeRevenueTrack.jsp"/>
						</td>
					</tr>
			<%
				}
			%>
<%
				if (!strhAction.equals("EditShip") && !strhAction.equals("EditPO") && !strhAction.equals("ACKORD"))
				{
%>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24"><%=strShipLable%></td>
						<td width="320">&nbsp;<input type="checkbox" size="30" name="Chk_ShipFl" value="" <%=strChecked%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=25></td>
					</tr>
<%
				}	
                if(strhAction.equals("ACKORD")){
	            %>
	            <tr>
					<td colspan="2">
						<jsp:include page="/custservice/GmAckOrdDetailsInclude.jsp" />	
					</td>
	          	</tr>
<%				if(strShowResLot.equals("YES")){ %>
	            <tr>
	            	<td colspan="3" class="RightDashBoardHeader" height="25"><fmtOrderItemControl:message key="LBL_PART_RESERVE_DETAILS"/></td></tr>
	            <tr>
					<td colspan="2">
						<jsp:include page="/custservice/GmAckPartReserveDetails.jsp" />	
					</td>
	          </tr>
<%				} %>
	          <tr><td colspan="2" height="1" class="LLine"></td></tr>
	<% 
}
				if (strhAction.equals("LoadSet")|| strhAction.equals("EditControl") || strhAction.equals("EditShip")|| strhAction.equals("EditPO"))
				{
%>
					<tr>
						<td colspan="2" width="748">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr><td colspan="8" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td width="70" height="25">&nbsp;<fmtOrderItemControl:message key="LBL_PART_NUMBER"/></td>
									<td width="360"><fmtOrderItemControl:message key="LBL_DESCRIPTION"/></td>
									<td width="60" align="center"><fmtOrderItemControl:message key="LBL_ORDER_QTY"/></td>
									<td width="60" align="center"><fmtOrderItemControl:message key="LBL_ORDER_TYPE"/></td>
									<td width="70" >&nbsp;<fmtOrderItemControl:message key="LBL_QTY"/></td>
									<td width="170"><fmtOrderItemControl:message key="LBL_CONTROL"/> <br>
									Number</td>
<%
									if (strhAction.equals("EditShip")|| strhAction.equals("EditPO"))
									{
%>
									<td align="center"><fmtOrderItemControl:message key="LBL_PRICE_EACH"/></td>
									<td align="center" width="60"><fmtOrderItemControl:message key="LBL_AMOUNT"/></td>
<%
									}
%>									
								</tr>
								<TR><TD colspan=8 height=1 class=Line></TD></TR>
<%
						intSize = alCart.size();
						StringBuffer sbPartNum = new StringBuffer();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();

							String strNextPartNum = "";
							int intCount = 0;
							String strColor = "";
							String strLine = "";
							String strPartNumHidden = "";
							String strPartDesc = "";
							String strPrice = "";
							String strTemp = "";
							String strPartNum = "";
							String strDispQty = "";
							String strRefId = "";
							int intQty = 0;
							double dbItemTotal = 0.0;
							double dbTotal = 0.0;
							String strItemTotal = "";
							String strTotal = "";
							double dblShip = 0.0;

							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alCart.get(i);
								if (i<intSize-1)
								{
									hmTempLoop = (HashMap)alCart.get(i+1);
									strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("ID"));
								}
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("ID"));
								sbPartNum.append(strPartNum);
								sbPartNum.append(",");
								strPartNumHidden = strPartNum;
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
								strDispQty = GmCommonClass.parseZero((String)hmLoop.get("PARTQTY"));
								strTemp = strPartNum;
								strRefId = GmCommonClass.parseNull((String)hmLoop.get("REFID"));
								if (strPartNum.equals(strNextPartNum))
								{
									intCount++;
									strLine = "<TR><TD colspan=8 height=1 bgcolor=#eeeeee></TD></TR>";
								}

								else
								{
									strLine = "<TR><TD colspan=8 height=1 bgcolor=#eeeeee></TD></TR>";
									if (intCount > 0)
									{
										strPartNum = "";
										strPartDesc = "";
										//strColor = "bgcolor=white";
										strLine = "";
										strDispQty = "";
									}
									else
									{
										//strColor = "";
									}
									intCount = 0;
								}

								if (intCount > 1)
								{
									strPartNum = "";
									strPartDesc = "";
									//strColor = "bgcolor=white";
									strLine = "";
									strDispQty = "";
								}
							
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
								strItemQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								strItemOrdType = GmCommonClass.parseNull((String)hmLoop.get("ITEMTYPE"));
								strItemOrdDesc = GmCommonClass.parseNull((String)hmLoop.get("ITEMTYPEDESC"));
								strCapFlag = GmCommonClass.parseNull((String)hmLoop.get("CONSFL"));
																
								intQty = Integer.parseInt(strItemQty);
								dbItemTotal = Double.parseDouble(strPrice);
								dbItemTotal = intQty * dbItemTotal; // Multiply by Qty
								strItemTotal = ""+dbItemTotal;
								dbTotal = dbTotal + dbItemTotal;
								//strTotal = ""+dbTotal;
								out.print(strLine);
								
								if (strItemOrdType.equals("50301"))
								{
									strDisabled = "readonly";
									strControl = "NOC#";
									blLoanerFl = true;
								}
								else
								{
									blLoanerFl = false;
									strDisabled = "";
								}
								if (strRAReason.equals("3316")) 				
								{
									strDisabled = "readonly";
								}

%>
								<tr>
									<input type="hidden" name="hPartNum<%=i%>" value="<%=strPartNumHidden%>">
									<input type="hidden" name="hRefId<%=i%>" value="<%=strRefId%>">
									<input type="hidden" name="hQty<%=strPartNum.replaceAll("\\.","")%>" value="<%=strDispQty%>">
									<input type="hidden" name="hPrice<%=i%>" value="<%=strPrice%>">
									<input type="hidden" name="hType<%=i%>" value="<%=strItemOrdType%>">
									<input type="hidden" name="hCapFl<%=i%>" value="<%=strCapFlag%>">
<%
								if (strhAction.equals("EditControl") && !blLoanerFl)
								{
%>
									<td class="RightText" height="20">&nbsp;<a class="RightText" tabindex=-1 href="javascript:fnSplit('<%=i%>','<%=strPartNum%>');"><%=strPartNum%></a></td>
<%
								}else{
%>
									<td class="RightText" height="20">&nbsp;<%=strPartNum%></td>
<%
								}
%>
									<td class="RightText"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td class="RightText" align="center">&nbsp;<%=strDispQty%></td>
									<td class="RightText" align="center">&nbsp;<%=strItemOrdDesc%></td>
<%
								if (strhAction.equals("EditControl"))
								{
%>
									<td id="Cell<%=i%>" class="RightText" ><input type="text" size="3" value="<%=strItemQty%>" name="Txt_Qty<%=i%>" <%=strDisabled%> class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3></td>
									<td><input type="text" size="9" value="<%=strControl%>" name="Txt_CNum<%=i%>" class="InputArea" maxlength="20"  <%=strDisabled%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" onChange="fnFetchMessages(this,'<%=strPartNum%>','PROCESS', '50925');" tabindex=3>
									</td>
<%
								}else{
%>
								<td class="RightText" align="center"><%=strItemQty%></td>
									<td><%=strControl%></td>
							
								<gmjsp:currency currSymbol="<%=strCurrSign%>" type="CurrTextSign"  textValue="<%=strPrice%>"/>
								<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strItemTotal)%>&nbsp;</td>
<%
								}
%>
								</tr>
<%
							}//End of FOR Loop
							out.print("<input type=hidden name=pnums value="+sbPartNum.toString().substring(0,sbPartNum.length()-1)+">");
							if (!strhAction.equals("EditControl"))
							{
								dblShip = Double.parseDouble(strShipCost);
								dbTotal = dbTotal + dblShip;
								strTotal = ""+dbTotal;
%>
								<tr><td colspan="8" height="1" bgcolor="#eeeeee"></td></tr>
								<tr>
									<td>&nbsp;</td>
									<td  height="20" colspan="6" class="RightText">&nbsp;<fmtOrderItemControl:message key="LBL_SHIPPING_CHARGES"/></td>
									<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strShipCost)%>&nbsp;</td>
								</tr>
								<tr><td colspan="8" height="1" bgcolor="#eeeeee"></td></tr>
								<tr>
									<td colspan="7" height="30" align="right" class="RightTableCaption"><fmtOrderItemControl:message key="LBL_TOTAL"/>&nbsp;&nbsp;</td>
									<gmjsp:currency currSymbol="<%=strCurrSign%>" type="CurrTextSign"  textValue="<%=strTotal%>"/>
									<input type="hidden" name="hOrderAmt" value="<%=strTotal%>"></td>
								</tr>
<%
								}
							}else {
%>
						<tr><td colspan="7" height="50" align="center" class="RightTextRed"><BR><fmtOrderItemControl:message key="LBL_NO_PART_NUMBER_IN_THIS_ORDER"/></td></tr>
<%
							}		
%>
							</table>
							<input type="hidden" name="hCnt" value="<%=intSize-1%>">
						</td>
					</tr>	
				</table>
			</td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
    </table>
<%
		if (strhAction.equals("EditControl") && blLoanerFl)
		{
%>
			<BR><span class="RightText"><b><fmtOrderItemControl:message key="LBL_NOTE1"/></span><BR>
<%
		}
		else if (strhAction.equals("EditShip") && blLoanerFl)
		{
%>
			<BR><span class="RightText"><fmtOrderItemControl:message key="LBL_NOTE2"/></span><BR>
<%
		}	
%>
<BR>
<%
		if (strhAction.equals("EditShip"))
		{
%>
<span class="RightTextBlue"><fmtOrderItemControl:message key="LBL_EDIT_SHIP_TO_DETAILS"/></span>
				<table border=0 width=750 cellspacing=0 cellpadding=0>
					<tr><td bgcolor=#666666 colspan=4></td></tr>
					<tr>
						<td bgcolor=#666666 width=1 rowspan="3"></td>
						<td colspan="2"></td>
						<td bgcolor=#666666 width=1 rowspan="3"></td>
					</tr>
					<tr>
						<td class="RightText" width="250" HEIGHT="23" align="right">&nbsp;<fmtOrderItemControl:message key="LBL_SHIP_TO"/>:</td>
						<td class="RightText" >&nbsp;<select <%=strReadOnly%> name="Cbo_ShipTo" id="Region" class="RightText" tabindex="1" onChange="javascript:fnCallShip();" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
<%
			  		intSize = 3; // Hardcoding to remove the 'Employee' value from list !
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alShipTo.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strShipTo.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
							</select>&nbsp;&nbsp;&nbsp;&nbsp; <fmtOrderItemControl:message key="LBL_VALUES"/>:<select name="Cbo_Rep" disabled class="RightText" tabindex="2" onChange="javascript:fnCallShip();" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
<%
			  		intSize = alRepList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alRepList.get(i);
			  			strCodeID = (String)hcboVal.get("REPID");
						strSelected = strShipTo.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%
			  		}
%>
							</select></td>
					</tr>
					<tr><td bgcolor=#666666 colspan=4></td></tr>
				</table>
					<script>
						fnSetValue();
					</script>
<span class="RightTextBlue"><fmtOrderItemControl:message key="LBL_EDIT_SHIPPING_DETAILS"/></span>
	<table border=0 width=750 cellspacing=0 cellpadding=0>
		<tr><td bgcolor=#666666 colspan=3></td></tr>
		<tr><td bgcolor=#666666 width=1></td>
			<td>
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr class="Shade">
						<td align="right" class="RightText" HEIGHT="24" width="250"><fmtOrderItemControl:message key="LBL_SHIPPING_DATE"/>:</td>
						<td class="RightText">&nbsp<input <%=strReadOnly%> type="text" size="10" value="<%=strShipDate%>" name="Txt_SDate" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3>&nbsp;<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmVendor.Txt_SDate');" title="Click to open Calendar"  src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 /></td>
					</tr>
					<tr>
						<td HEIGHT="24" align="right" class="RightText"><fmtOrderItemControl:message key="LBL_SHIPPING_CARRIER"/>:</td>
						<td class="RightText">&nbsp;<select <%=strReadOnly%> name="Cbo_ShipCarr" class="RightText" tabindex="4" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
<%
			  		intSize = alCarrier.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alCarrier.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strShipCarr.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>

													</select>
							</td>
					</tr>
					<tr class="Shade">
						<td align="right" HEIGHT="24" class="RightText"><fmtOrderItemControl:message key="LBL_SHIPPING_MODE"/>:</td>
						<td class="RightText">&nbsp;<select <%=strReadOnly%> name="Cbo_ShipMode" class="RightText" tabindex="5" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
<%
			  		intSize = alMode.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alMode.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strShipMode.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>

													</select>
							</td>
					</tr>
					<tr>
						<td align="right" HEIGHT="24" class="RightText"><fmtOrderItemControl:message key="LBL_SHIPPING_CHARGES"/>:</td>
						<td class="RightText">&nbsp<input <%=strReadOnly%> type="text" size="3" value="<%=strShipCost%>" name="Txt_SCost" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=6></td>
					</tr>
					<tr class="shade">
						<td align="right" HEIGHT="24" class="RightText"><fmtOrderItemControl:message key="LBL_TRACKING_NUMBER"/>:</td>
						<td class="RightText">&nbsp<input <%=strReadOnly%> type="text" size="40" value="<%=strTrack%>" name="Txt_Track" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="fnChangeTrack(this)" tabindex=7>
						<br>&nbsp;<fmtOrderItemControl:message key="LBL_USE_BARCODE"/></td>
					</tr>
				</table>
			</td>
			<td bgcolor=#666666 width=1></td>
		</tr>
	</table>

<%
		}
	}
%>
	<table border="0" width="100%" cellspacing="0" cellpadding="0" >
		<tr>
			<td  width=250></td>
			<td align="center" height="30" id="button">
<%-- <%if (!strhAction.equals("EditShip")){%>
		<gmjsp:button value="&nbsp;Save Comments&nbsp;" name="Btn_Submit" gmClass="button" onClick="javascript:fnSaveComments();" buttonType="Save" />&nbsp;&nbsp;
<%}%> --%>
<%	if (strMode.equals("SaveShip"))
	{
%>
				<fmtOrderItemControl:message key="BTN_PCKING_SLIP" var="varPackingSlip"/>
				<gmjsp:button value="&nbsp;${varPackingSlip}&nbsp;" name="Btn_Pack" gmClass="button" onClick="fnPrintPack();" buttonType="Save" />
<%
	}
%>
			</td>
			
		<tr>
		
	</table>
	<script>
<%
	if (strMode.equals("SaveShip"))
	{
%>
	alert(message[5532]);
<%
	}
%>
</script>
<%
	if (!strhAction.equals("EditShip"))
	{
%>
<table border="0" width="100%" cellspacing="0" cellpadding="0" >
	<!-- <tr><td bgcolor=#666666 colspan=3></td></tr> -->
	<tr> 
		<!-- <td bgcolor=#666666 width=1></td> -->
		<td>
			<jsp:include page="/common/GmIncludeLog.jsp" >
				<jsp:param name="LogType" value="" />
				<jsp:param name="LogMode" value="Edit" />
			</jsp:include>
		</td>
		<!-- <td bgcolor=#666666 width=1></td> -->
	</tr>
	<tr><td colspan="5" height="2" bgcolor="#eeeeee"></td></tr>
	<tr>
				<td align="center" colspan="5">
				<jsp:include page="/accounts/GmCreditHoldInclude.jsp" >
					<jsp:param name="ALERT" value="N"/>
					<jsp:param name="SUBTYPE" value="104965"/>
					</jsp:include>
				</td>
	</tr>
	<tr><td colspan="5" align="center">
	<fmtOrderItemControl:message key="LBL_CHOOSE_ACTION"/>:
	<fmtOrderItemControl:message key="BTN_SUBMIT" var="varSubmit"/>
	<%
	if (strhAction.equals("ACKORD")){
	%>
    <gmjsp:dropdown controlName="cbo_ack_action" value="<%=alOrdrTpeList%>" codeId="CODENMALT"  codeName="CODENM" defaultValue="[Choose One]" onChange="javascript:fnCreditHold();"/> 
	<input type="button" value="&nbsp;${varSubmit}&nbsp;" name="Btn_Submit" id="Btn_Submit" class="button" onClick="javascript:fnAckSubmit()" >&nbsp;&nbsp;
	<%}else{ %>
	<input type="button" value="&nbsp;${varSubmit}&nbsp;" name="Btn_Submit" class="button" onClick="javascript:fnSubmit()" >&nbsp;&nbsp;
	<%} %>
	</td></tr>
	<tr><td colspan="5" height="2" bgcolor="#eeeeee"></td></tr>
</table>
<%
	}
if (strhAction.equals("EditControl"))
{
%>
<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
			<td colspan="4">
					<jsp:include page="/common/GmRuleDisplayAjaxInclude.jsp"/>
			</td>
		</tr>
		
	</table>
</FORM>
<%
}
%>
</BODY>
<%@ include file="/common/GmFooter.inc" %>
</HTML>
