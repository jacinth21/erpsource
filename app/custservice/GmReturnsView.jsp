
<%
/**********************************************************************************
 * File		 		: GmReturnsView.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
 <!-- \custservice\GmReturnsView.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="java.util.Date"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtReturnsView" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtReturnsView:setLocale value="<%=strLocale%>"/>
<fmtReturnsView:setBundle basename="properties.labels.custservice.GmReturnsReport"/>

<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
	
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));	

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	log.debug(" Values inside hmReturn is " + hmReturn);
	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");

	String strDesc = "";
	String strRAId = "";
	String strSetName = "";
	String strReason = "";
	String strType = "";
	String strCreatedBy = "";
	String strCreatedDate = "";
	String strDistName = "";
	String strAcctName = "";
	String strRetDate = "";
	String strExpDate = "";
	String strOrgOrderId = "";
	
	String strControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	
	String strStatusFl = "";
	String strSetId = "";
	
	Date dtCreatedDate = null;
	Date dtExpDate = null;
	Date dtRetDate = null;
	
	ArrayList alReturnsItems = new ArrayList();
	HashMap hmReturnDetails = new HashMap();
	
	boolean bolFl = false;

	int intPriceSize = 0;
	

	if (hmReturn != null)
	{
		hmReturnDetails = (HashMap)hmReturn.get("RADETAILS");
		alReturnsItems = (ArrayList)hmReturn.get("RAITEMDETAILS");
	strSetId =GmCommonClass.parseNull((String) hmReturn.get("SETID"));

		strRAId = GmCommonClass.parseNull((String)hmReturnDetails.get("RAID"));
		strSetName = GmCommonClass.parseNull((String)hmReturnDetails.get("SNAME"));
		strType = GmCommonClass.parseNull((String)hmReturnDetails.get("TYPE"));
		strReason = GmCommonClass.parseNull((String)hmReturnDetails.get("REASON"));
		
		strCreatedBy = GmCommonClass.parseNull((String)hmReturnDetails.get("CUSER"));
		strCreatedDate = GmCommonClass.getStringFromDate((java.util.Date)hmReturnDetails.get("CDATE"),strGCompDateFmt);
		//strCreatedDate = GmCommonClass.getStringFromDate(dtCreatedDate,strApplDateFmt);
		strDesc = GmCommonClass.parseNull((String)hmReturnDetails.get("COMMENTS"));
		strDistName = GmCommonClass.parseNull((String)hmReturnDetails.get("DNAME"));
		strAcctName = GmCommonClass.parseNull((String)hmReturnDetails.get("ANAME"));
		strRetDate = GmCommonClass.getStringFromDate((java.util.Date)hmReturnDetails.get("RETDATE"),strGCompDateFmt);
		//strRetDate = GmCommonClass.getStringFromDate(dtRetDate,strApplDateFmt);
		strExpDate = GmCommonClass.getStringFromDate((java.util.Date)hmReturnDetails.get("EDATE"),strGCompDateFmt);
		//strExpDate = GmCommonClass.getStringFromDate(dtExpDate,strApplDateFmt);
		strOrgOrderId = GmCommonClass.parseNull((String)hmReturnDetails.get("ORDID"));
		
		strDistName = strDistName.equals("")?strAcctName:strDistName;
	}

	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Veiw Returns </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>

function fnPrintCreditMemo()
{
windowOpener("/GmReportCreditsServlet?hAction=PrintVersion&hId=<%=strRAId%>","Credit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmVendor" method="POST">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hConsignId" value="<%=strRAId%>">

	<table border="0" width="600" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
			<td height="25" class="RightDashBoardHeader"><fmtReturnsView:message key="TD_RTNS_VIEW_HEADER"/></td>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="598" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtReturnsView:message key="LBL_RA_NUM"/></b>:</td>
						<td class="RightText">&nbsp;<%=strRAId%></td>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtReturnsView:message key="LBL_RTN_FROM"/>:</td>
						<td class="RightText">&nbsp;<%=strDistName%></td>
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="4"></td></tr>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtReturnsView:message key="LBL_INIT_DATE"/>:</td>
						<td class="RightText">&nbsp;<%=strCreatedDate%></td>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtReturnsView:message key="LBL_INIT_BY"/>:</td>
						<td class="RightText">&nbsp;<%=strCreatedBy%></td>
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="4"></td></tr>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtReturnsView:message key="LBL_RTN_TYPE"/>:</td>
						<td class="RightText">&nbsp;<%=strType%></td>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtReturnsView:message key="LBL_RTN_REASON"/>:</td>
						<td class="RightText">&nbsp;<%=strReason%></td>
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="4"></td></tr>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtReturnsView:message key="LBL_EXPECT_DATE"/>:</td>
						<td class="RightText">&nbsp;<%=strExpDate%></td>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtReturnsView:message key="LBL_RTN_DATE"/>:</td>
						<td class="RightText">&nbsp;<%=strRetDate%></td>
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="4"></td></tr>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtReturnsView:message key="LBL_SET_NAME"/>:</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strSetName)%></td>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtReturnsView:message key="LBL_ORG_ORDER"/>:</td>
						<td class="RightText">&nbsp;<%=strOrgOrderId%></td>
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="4"></td></tr>
					<tr>
						<td class="RightText" HEIGHT="20" align="right"><fmtReturnsView:message key="LBL_COMMENTS"/></td>
						<td class="RightText" colspan="3">&nbsp;<%=strDesc%></td>
					</tr>

					<tr>
						<td colspan="4">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td height="25" width="70" align="center"><fmtReturnsView:message key="LBL_PART_NUM"/></td>
									<td width="400"><fmtReturnsView:message key="LBL_DESC"/></td>
									<td width="50" align="center"><fmtReturnsView:message key="LBL_QTY"/></td>
									<td width="50" align="center"><fmtReturnsView:message key="LBL_STATUS"/></td>
								</tr>
								<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
<%
						intSize = alReturnsItems.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();

							String strLine = "";
							String strRate = "";
							String strAmount = "";
							String strPrice = "";
							log.debug(" strSetId is -- |" +strSetId +"|");
							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alReturnsItems.get(i);
								log.debug(" hmLoop values are " + hmLoop);
									if (strSetId.equals("0") || !strSetId.equals(""))
									{
											strQty = GmCommonClass.parseZero((String)hmLoop.get("IQTY"));	
									}
									else
									strQty = GmCommonClass.parseZero((String)hmLoop.get("QTY"));
									
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strPrice = GmCommonClass.parseZero((String)hmLoop.get("PRICE"));
								strStatusFl = GmCommonClass.parseNull((String)hmLoop.get("SFL"));

%>
								<tr>
									<td class="RightText" height="20">&nbsp;<%=strPartNum%></td>
									<td class="RightText"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td class="RightText" align="center">&nbsp;<%=GmCommonClass.getRedText(strQty)%></td>
									<td class="RightText" align="center">&nbsp;<%=strStatusFl%></td>
								</tr>
								<tr><td colspan="4" height="1" bgcolor="#eeeeee"></td></tr>
<%
							}//End of FOR Loop
						}else {
%>
						<tr><td colspan="4" height="50" align="center" class="RightTextRed"><BR><fmtReturnsView:message key="MSG_NO_PART_RTN"/></td></tr>
<%
						}
%>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="4" height="30" align="center">
<%	
	if (strhAction.equals("ReturnDetails"))
	{
%>						
							<fmtReturnsView:message key="BTN_PRINT" var="varPrint"/>
							<gmjsp:button value="${varPrint}" gmClass="button" onClick="javascript:window.print()" tabindex="13" buttonType="Load" />
						
<%
	}else{
%>
							<fmtReturnsView:message key="BTN_PRINT_CREDIT" var="varPrintCredit"/>
							<gmjsp:button value="${varPrintCredit}" gmClass="button" onClick="fnPrintCreditMemo();" tabindex="13" buttonType="Load" />
<%
	}
%>
							<fmtReturnsView:message key="BTN_CLOSE" var="varClose"/>
						&nbsp;&nbsp;<gmjsp:button value="${varClose}" gmClass="button" onClick="window.close();" tabindex="14" buttonType="Load" />
							</td>
					</tr>
				 </table>
			 </td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
    </table>

</FORM>
</BODY>

</HTML>
