

<%
	/**********************************************************************************
	 * File		 		: GmAckOrdBOReport.jsp
	 * Desc		 		: This screen display the BO lisrt for an Acknowledgement Order by Parts 
	 * Version	 		: 1.0
	 * Author			: Rajkumar Jayakumar
	 ************************************************************************************/
%>
<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.HashMap,java.util.ArrayList" %>	
<%@include file="/common/GmHeader.inc"%>
<!-- GmAckOrdBOReport.jsp -->
<%@ taglib prefix="fmtACKBORpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtACKBORpt:setLocale value="<%=strLocale%>"/>
<fmtACKBORpt:setBundle basename="properties.labels.custservice.GmAckOrdBOReport"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
String strAccountsFieldAuditJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_FIELDAUDIT");
String strCustServiceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
String strGridData=GmCommonClass.parseNull((String)request.getAttribute("XMLDATA"));
String strWikiTitle = GmCommonClass.getWikiTitle("ACK_ORD_BO_LIST_REPORT");
String strAccId = "";
String strAccName = "";
HashMap hmParam = new HashMap();
HashMap hmReturn = new HashMap();
ArrayList alAccount = new ArrayList();
hmParam = (HashMap)request.getAttribute("hmParam");
hmReturn = (HashMap)request.getAttribute("hmReturn");
String strInventoryPartNos = GmCommonClass.parseNull((String)request.getAttribute("LOOKUPPARTNOS"));
String strInvButtonDisabled = (!strInventoryPartNos.equals(""))?"false":"true";

if (hmParam != null)
{
	strAccId = GmCommonClass.parseNull((String)hmParam.get("ACCID"));
	strAccName = GmCommonClass.parseNull((String)hmParam.get("ACCNAME"));
}

if (hmReturn != null)
{
	alAccount = (ArrayList)hmReturn.get("ACCOUNTLIST");
}
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Acknowledgement Order BO List Report</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="javascript" src="<%=strCustServiceJsPath%>/GmAckOrdBOReport.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%= strAccountsFieldAuditJsPath%>/GmFieldAuditSetCostRpt.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>

<script>
var gridObjData ='<%=strGridData%>';
var mygrid;
var partNos = '<%=strInventoryPartNos%>';

function fnOnPageLoad() {
	if(gridObjData!=''){
		mygrid = initGridData('dataGridDiv',gridObjData);
		mygrid.groupBy(0,["#title","","","","","","","","","","","#stat_total","#stat_total","#stat_total"]);
		//mygrid.attachHeader('#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#rspan,#rspan,#rspan,#text_filter,#text_filter,#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter');
		mygrid.enableDistributedParsing(true);		 
	}
}

function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.attachHeader('#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#rspan,#rspan,#rspan,#text_filter,#text_filter,#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter');
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;
}

function fnEdit(ordid){
 	document.frmAckOrd.hOrdId.value = ordid;
	document.frmAckOrd.hAction.value = "ACKORD";
	document.frmAckOrd.action = "/GmOrderItemServlet";
	document.frmAckOrd.submit(); 
}
function fnExport(){
	mygrid.setColumnHidden(6,true);
	mygrid.expandAllGroups();
	mygrid.toExcel('/phpapp/excel/generate.php');
	mygrid.setColumnHidden(6,false);
	mygrid.groupBy(0,["#title","","","","","","","","","","","#stat_total","#stat_total","#stat_total"]);
}

</script>
</head>
<body leftmargin="20" topmargin="20" onload="fnOnPageLoad();">
<form name="frmAckOrd" method="post" action="<%=strServletPath%>/GmOrderItemServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hOrdId" value="">
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="hMode" value="">
<table border="0" cellspacing="0" cellpadding="0" class="DtTable1100">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="3">&nbsp;<fmtACKBORpt:message key="TD_ACK_BO_RPT_HEADER"/></td>
			<fmtACKBORpt:message key="IMG_ALT_HELP" var = "varHelp"/>
			<td align="right" class=RightDashBoardHeader><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr>
			<%-- <td height="30" class="RightTableCaption" align="right">
			<fmtACKBORpt:message key="LBL_ACCOUNTS" var = "varAccounts"/>
			<gmjsp:label type="RegularText"  SFLblControlName="${varAccounts}:" td="false"/>
			</td>
			<td>
			&nbsp;<gmjsp:autolist comboType="DhtmlXCombo" controlName="Cbo_AccId" seletedValue="<%=strAccId%>" value="<%=alAccount%>" codeId="ID" codeName="NAME" 
			defaultValue=" [Choose One]" tabIndex="1" width="300" />
			</td>	 --%>
			<td class="RightTableCaption" align="right" HEIGHT="24"><fmtACKBORpt:message key="LBL_ACCOUNTS"/>:</td>
                <td class="RightText" HEIGHT="24">
 							<table><tr HEIGHT="16"><td>	
 							<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="Cbo_AccId" />
							<jsp:param name="METHOD_LOAD" value="loadAccountList&searchType=LikeSearch" />
							<jsp:param name="WIDTH" value="400" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="CONTROL_NM_VALUE" value="<%=strAccName%>" />
							<jsp:param name="CONTROL_ID_VALUE" value="<%=strAccId%>" />
							<jsp:param name="SHOW_DATA" value="50" />
							<jsp:param name="ON_BLUR" value="fnAcBlur(this);" />
							</jsp:include>	
							</td>
						   </tr>
						  </table>
			  </td>
			
			<td class="RightTableCaption" align="left" height="24">
			<table><tr height="16"><td>
			<fmtACKBORpt:message key="BTN_LOAD" var = "varLoad"/>
            <gmjsp:button value="&nbsp;${varLoad}&nbsp;" name="LoadBtn" gmClass="button" onClick="fnGo();" tabindex="16" buttonType="Load" />
            </td>
            </tr>
            </table>
            </td>
            <td class="RightTableCaption" align="left" height="24">
            <table><tr height="16"><td>
			<fmtACKBORpt:message key="LBL_INVENTORY_LOOKUP" var = "varInventoryLookup"/>
            <gmjsp:button value="&nbsp;${varInventoryLookup}&nbsp;" name="LoadBtn" gmClass="button" disabled="<%=strInvButtonDisabled%>" onClick="fnOverallLookup();" tabindex="17" buttonType="Load" /> 
            </td>
            </tr>
            </table>
            </td>
		</tr>
		
		<%
		if(!strGridData.equals("")) {
		%>
        <tr>
		<td colspan="4">
			<div id="dataGridDiv" height="600" width="1200"></div>
			</td>
		</tr>
		<tr align="center">
				<td colspan="4" >
				<div class='exportlinks'><fmtACKBORpt:message key="DIV_EXPORT_OPT"/> : <img
					src='img/ico_file_excel.png' />&nbsp;<a href="#"
					onclick="fnExport();"> <fmtACKBORpt:message key="DIV_EXCEL"/> </a></div>
				</td>
		</tr>
		<%
		}else {
		%>
       <tr>
       <tr class="Shade"><td colspan="4" align="center" class="RightText" height="20px" ><fmtACKBORpt:message key="MSG_NOTHING_FOUND"/></td></tr>
       </tr>
		<%
		}
		%>
</table>
</form>
<%@ include file="/common/GmFooter.inc" %> 
</body>
</html>