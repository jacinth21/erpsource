 <%
/**********************************************************************************
 * File		 		: GmITItemPicSlip.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: rajan
 * ------------------------------------------------
 * 12/19/2008 bvidyasankar Added barcode to this report
************************************************************************************/
%>
<!-- \custservice\GmITItemPicSlip.jsp-->
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- Imports for Logger -->
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%

	log = GmLogger.getInstance(GmCommonConstants.CUSTOMERSERVICE);
	strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	strCssPath = GmCommonClass.getString("GMSTYLES");
	strImagePath = GmCommonClass.getString("GMIMAGES");
	strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	strCommonPath = GmCommonClass.getString("GMCOMMON");
		
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmAtbData = new HashMap();
	HashMap hmCompAtbData = new HashMap();
	hmAtbData = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("CONSIGN_ATB"));
	hmCompAtbData = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("COMP_ATB"));
	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");
	String strSource = (String)request.getAttribute("source");
	String strRuleSource = (String) request.getAttribute("strRuleSource");
	String strCrComments= GmCommonClass.parseNull((String)request.getAttribute("CRCOMMENTS"));
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strSetId = "";
	String strSetNm = "";
	String strEtchId =	"";
	String strDesc = "";
	String strRefId = "";
	String strShade = "";
	String strConsignId = "";
	String strSetName= "";
	String strAccName = "";
	String strUserName = "";
	String strIniDate = "";
	String strRequestorName = "";
	String strType = "";
	String strInHousePurpose = "";
	String strConsignStatusFlag = "";
	String strCtype="";
	String strBillNm="";
	String strToHeader="";
	String strSlipHeader="Pick Slip";
	String strdetails="Transaction Details";
	String strShippingLabel = "Shipping Date";
	String strCarrier = "";
	String strMode="";
	String strTrackno="";
	String  strLog_Comments="";
	String strCompanyId = "";
	String strCustPO = "";
	String strShowPartSize = "";
	String strDivisionId = "";
	String strGmCompanyVat="";
	String strGmCompanyCst="";
	String strGmCompanyPAN="";
	String strGmLocalLic="";
	String strGmExportLic="";
	String strExportDruglic="";
	String strLocalDruglic="";
	String strShippingDt = "";
	String strTransCompId = "";
	String strTransCompanyLocale = "";
	String strPlantName = "";
	String strExpiryDateFl="";
	// getting rule value to show building name in picslip for PMT-36675 
	String strBuildingName="";
    String strHospAccId = "";	
	boolean flg =false;
	String strTranAdd = "";
	String strApplnDateFmt = strGCompDateFmt;
	ArrayList alSetLoad = new ArrayList();
	HashMap hmConsignDetails = new HashMap();
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	// Locale
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    GmResourceBundleBean gmComResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    strCountryCode = GmCommonClass.parseNull((String)gmComResourceBundleBean.getProperty("COUNTRYCODE"));
    
	int intPriceSize = 0;

	if (hmReturn != null)
	{
		alSetLoad = (ArrayList)hmReturn.get("SETLOAD");
		//strSetId = (String)session.getAttribute("hSetId") == null?"":(String)session.getAttribute("hSetId");
	
		hmConsignDetails = (HashMap)hmReturn.get("CONDETAILS");
		strConsignId = (String)hmConsignDetails.get("CID");
		strSetId = GmCommonClass.parseNull((String)hmConsignDetails.get("SETID"));
		strSetName= GmCommonClass.parseNull((String)hmConsignDetails.get("SETNM"));
		// When Type is CN then Set Name is empty so, get the set name from Key SNAME.
		strSetName = strSetName.equals("")? GmCommonClass.parseNull((String)hmConsignDetails.get("SNAME")):strSetName;
		strAccName = (String)hmConsignDetails.get("ANAME");
		String strDistName = (String)hmConsignDetails.get("DNAME");
		strAccName = strAccName.equals("")?strDistName:strAccName;
		strPlantName = GmCommonClass.parseNull((String)hmConsignDetails.get("PNAME"));
		strUserName = (String)hmConsignDetails.get("UNAME");
		strIniDate = (String)hmConsignDetails.get("CDATE");
		//strIniDate = GmCommonClass.getStringFromDate((java.util.Date)hmConsignDetails.get("CDATE"),strApplnDateFmt);
		strDesc = (String)hmConsignDetails.get("COMMENTS");
		strType = (String)hmConsignDetails.get("TYPE");
		strCtype = (String)hmConsignDetails.get("CTYPE");
		strInHousePurpose = (String)hmConsignDetails.get("PURP");
		strBillNm = GmCommonClass.parseNull((String)hmConsignDetails.get("HISTBILLNM"));
		strTranAdd = GmCommonClass.parseNull((String)hmConsignDetails.get("TRBILLNM"));
		strRequestorName = GmCommonClass.parseNull((String)hmConsignDetails.get("SHIPADD"));
		strConsignStatusFlag = GmCommonClass.parseNull((String)hmConsignDetails.get("SFL"));
		strCarrier = GmCommonClass.parseNull((String)hmConsignDetails.get("CARRIER"));
		strMode=GmCommonClass.parseNull((String)hmConsignDetails.get("SHIPMODE"));
		strTrackno= GmCommonClass.parseNull((String)hmConsignDetails.get("TRACKNO"));
		strEtchId = GmCommonClass.parseNull((String)hmConsignDetails.get("ETCHID"));
		strEtchId = strEtchId.equals("")?"-":strEtchId;
		strRefId = GmCommonClass.parseNull((String)hmConsignDetails.get("REFID"));
		strCustPO = GmCommonClass.parseNull((String)hmConsignDetails.get("CSTPO"));
		strDivisionId = GmCommonClass.parseNull((String)hmConsignDetails.get("DIVISION_ID"));
		strDivisionId = strDivisionId.equals("")? "2000" :strDivisionId;
		strShowPartSize = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PICK_SLIP.SHOW_PART_SIZE")); 
		strExpiryDateFl = GmCommonClass.parseNull((String)hmReturn.get("ExpiredShipDateFl"));
		//When do item initiate enter the value in CustPO in screen. The ref id only for Profoma Invoice. 
		if(strRefId.equals("") && !strCustPO.equals(""))
			strRefId = strCustPO;
		
		strLog_Comments= GmCommonClass.parseNull((String)hmConsignDetails.get("LOG_COMMENTS"));
		strCompanyId = GmCommonClass.parseNull((String)gmComResourceBundleBean.getProperty(strDivisionId+".LOGO"));
		strTransCompId = GmCommonClass.parseNull((String)hmConsignDetails.get("COMPANYCD"));
		strBuildingName = GmCommonClass.parseNull((String)hmConsignDetails.get("BUILDINGNAME"));//get the building name for PMT-36675
		strHospAccId = GmCommonClass.parseNull((String)hmConsignDetails.get("HOSPACC"));
	}
	if (strCtype.equals("50154")) {//Move from Shelf to Loaner Extension w/Replenish
		strToHeader = "Replenished To";
	}else if(strCtype.equals("9110")||strCtype.equals("1006572")||strCtype.equals("1006571")||strCtype.equals("1006570")||strCtype.equals("1006573")||strCtype.equals("50150")||strCtype.equals("1006575")){//ISMP,ISBO,BLIS,IHIS,FGIS,IHLE
		strToHeader = "Loaned To";
	}else {
		strToHeader = "Consigned To";
	}
	
	if (strConsignStatusFlag.equals("4") && strCtype.equals("50154") ){//Packslip 
	    strTransCompanyLocale = GmCommonClass.getCompanyLocale(strTransCompId);
	    GmResourceBundleBean gmTransResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Paperwork", strTransCompanyLocale);
	    strSlipHeader = GmCommonClass.parseNull((String)gmTransResourceBundleBean.getProperty("PICK_SLIP.SHELF_TO_LOANER_EXT_TITLE"));
		strdetails = "Transaction Details";
		 flg = true;
	}
	if (strCtype.equals("93341") ){		
		strSlipHeader = "Stock Transfer";
	}
	if(strCtype.equals("100062"))
	{
		strRequestorName="<b><font color=\"red\">Back Order</font></b>";
	}
	if(Double.parseDouble(strConsignStatusFlag) >= 2 || strCtype.equals("50154")){
		strDesc+="<BR>"+strCrComments+"<BR>"+strLog_Comments;
	strGmCompanyVat =  GmCommonClass.parseNull((String) hmCompAtbData.get("GMCOMPANYVAT"));
	strGmCompanyCst =  GmCommonClass.parseNull((String) hmCompAtbData.get("GMCOMPANYCST"));
	strGmCompanyPAN =  GmCommonClass.parseNull((String) hmCompAtbData.get("GMCOMPANYPAN"));
	strGmLocalLic =  GmCommonClass.parseNull((String) hmCompAtbData.get("GMLOCALLIC"));
	strGmExportLic =  GmCommonClass.parseNull((String) hmCompAtbData.get("GMEXPORTLIC"));
	strExportDruglic =  GmCommonClass.parseNull((String) hmAtbData.get("EXPORTDRUGLIC"));
	strLocalDruglic =  GmCommonClass.parseNull((String) hmAtbData.get("LOCALDRUGLIC"));
	}
	int intSize = 0;
	HashMap hcboVal = null;
	
	StringBuffer sbStartSection = new StringBuffer();
	sbStartSection.append("<tr><td colspan=2>");
	sbStartSection.append("<table border=0 width=100% cellspacing=0 cellpadding=0>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption>");
	//sbStartSection.append("<td height=20 align=center width=250>&nbsp;Consigned To</td>");
	sbStartSection.append("<td height=20 align=center width=250>&nbsp;");
	sbStartSection.append(strToHeader);
	sbStartSection.append("</td>");
	sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=11></td>");
	sbStartSection.append("<td width=250 align=center>&nbsp;Ship To</td>");
	sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=11></td>");
	sbStartSection.append("<td width=100 align=center>&nbsp;Date</td>");
	sbStartSection.append("<td width=100 align=center>&nbsp;Transaction #</td>");
	sbStartSection.append("</tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
	sbStartSection.append("<tr><td class=RightText rowspan=5 valign=top>&nbsp;");
	if (strCtype.equals("50154")) {//Move from Shelf to Loaner Extension w/Replenish
		sbStartSection.append(strBillNm);
	}else if (strCtype.equals("93341")){
		sbStartSection.append(strTranAdd);
	}else if (strCtype.equals("106703") || strCtype.equals("106704")){
	  sbStartSection.append(strPlantName);
	}else {
		sbStartSection.append(strAccName);
	}
	sbStartSection.append("</td>");
	if(!strHospAccId.equals("")){
	    sbStartSection.append("<td rowspan=5 class=RightText valign=top>&nbsp;");sbStartSection.append(strHospAccId+" - "+strRequestorName);
	}else{
		sbStartSection.append("<td rowspan=5 class=RightText valign=top>&nbsp;");sbStartSection.append(strRequestorName);
	}
	
	sbStartSection.append("</td>");
	sbStartSection.append("<td height=25 class=RightText align=center>&nbsp;");sbStartSection.append(strIniDate);
	sbStartSection.append("</td><td class=RightText align=center>&nbsp;");
	sbStartSection.append(strConsignId);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption><td align=center>Ref ID</td><td align=center>Initiated By</td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	sbStartSection.append("<tr><td align=center class=RightText>&nbsp;");sbStartSection.append(strRefId);
	sbStartSection.append("<Br></td><td align=center height=25 nowrap class=RightText>&nbsp;");sbStartSection.append(strUserName);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");	
	if (flg == true){
		sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption>");
		sbStartSection.append("<td  width=250 height=20 align=center >&nbsp;Carrier </td>");
		sbStartSection.append("<td width=250 height=20 align=center >&nbsp;Mode</td>");
		sbStartSection.append("<td width=250 colspan=3 height=20 align=center >&nbsp;Tracking #</td>");		
		sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
		
		sbStartSection.append("<tr> <td  height=20 align=center >&nbsp;");		
		sbStartSection.append(strCarrier);
		sbStartSection.append("</td>");
		sbStartSection.append("<td  height=20 align=center >&nbsp;");
		sbStartSection.append(strMode);
		sbStartSection.append("</td>");
		sbStartSection.append("<td colspan=3 height=20 align=center >&nbsp");
		sbStartSection.append(strTrackno);
		sbStartSection.append("</td>");		
		sbStartSection.append("<tr><td  bgcolor=#666666 height=1 colspan=6></td></tr>");
		
	}
	sbStartSection.append("</table></td></tr>");
	String strId = strConsignId+"$"+strSource;
	//strRuleSource = strRuleSource.equals("")?strCtype : strRuleSource;
	String strRuleId = strConsignId + "$" + strCtype;

	strCompanyLocale = strCompanyLocale.equals("")? "en_US" : "en_"+strCompanyLocale;
    request.setAttribute("CompanyLocale", strCompanyLocale);
%>
<HTML>
<HEAD>
<TITLE> SpineIT Italy : Pick Slip </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script>

function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}

</script>


</HEAD>

<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmVendor">
<table align ="center"  border="0" class="DtRuleTable700" cellspacing="0" cellpadding="0">
<fmt:setLocale value="${requestScope.CompanyLocale}"/>
<fmt:setBundle basename="properties.Paperwork"/>
		<tr>
			<td align="right" height="30" id="button">
				<img style="cursor:hand" src='<%=strImagePath%>/print-icon.png' height="25" width="25" alt="Click here to print page" onClick="fnPrint();" />
				<img style="cursor:hand" src='<%=strImagePath%>/close_icon.png' height="20" width="20" alt="Click here to close window" onClick="window.close();" />
			</td>
		<tr>
	</table>
	 
	<table align ="center"  border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table  cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td height="80" width="170"><img src="<%=strImagePath%>/<%=strCompanyId%>.gif" width="138" height="60"></td>
						<%if(strCountryCode.equals("en")){ %>
							<td align="left" class="RightText"><font size="+3"><%=strSlipHeader%></font>&nbsp;</td>
						<%}else{ %>
							<td align="right" class="RightText"><font size="+2"><%=strSlipHeader%></font>&nbsp;</td>
						<%}%>
						<%
							if (!strCtype.equals("50159") && !strCtype.equals("93341")){ //Usage sheet
						%>
						<td rowspan="2">
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr>
									<td height="30"><img src='/GmCommonBarCodeServlet?ID=<%=strId%>' height="20" width="220" /></td>
									<td>&nbsp;Fedex Module</td>
								</tr>
								<tr>
									<td height="30" align="right"><img src='/GmCommonBarCodeServlet?ID=<%=strConsignId%>' height="20" width="170" /></td>
									<td>&nbsp;Paperwork</td>
								</tr>
								<tr>
									<td height="30" align="right"><img src='/GmCommonBarCodeServlet?ID=<%=strRuleId%>&type=2d' height="40"	width="40" /></td>
									<td>&nbsp;Motorola Device</td>
								</tr>
							<%if(!strBuildingName.equals("")){%>	    
				               <tr>
				                    <td colspan="3" align="right">&nbsp;<fmt:message key="PICK_SLIP.BUILDING_NAME"/>: <span align="right"><%=strBuildingName%></span>&nbsp;</td>	
				              </tr>
				             <%}%>	
							</table>			
						</td>
						<%} %>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="698" height="100" valign="top">
				<table  border="0" cellspacing="0" cellpadding="0">
<!--StartSection starts here-->
<%out.print(sbStartSection);%>
<!--StartSection ends here-->				
					<tr>
						<td class="RightTableCaption" bgcolor="#eeeeee" colspan = "2" HEIGHT="20"><%=strdetails%></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>					
					<tr>
							<td colspan="2" class="RightText">
								<table  border="0" width="100%" cellspacing="0" cellpadding="0">
									<tr height="20">
										<td align="right" class="RightTableCaption" width="10%">Type:</td>
										<td>&nbsp;<%=strType%></td>
										<td align="right" class="RightTableCaption" width="8%">Reason:</td>
										<td colspan="3">&nbsp;<%=strInHousePurpose%></td>
									</tr>
<%
			if (!strSetId.equals(""))
			{
%>
									<tr><td colspan="6" height="1" class="LLine"></td></tr>
									<tr height="20">
										<td align="right" class="RightTableCaption">Set:</td>
										<td>&nbsp;<%=strSetId%>&nbsp;-&nbsp;<%=strSetName%></td>
										<td align="right" class="RightTableCaption">CN ID:</td>
										<td>&nbsp;<%=strRefId%></td>
										<td align="right" class="RightTableCaption">Etch ID:</td>
										<td>&nbsp;<%=strEtchId%></td>
									</tr>
<%
			}
%>
								</table>
							</td>
					</tr>
					<tr>
						<td colspan="2"  height="400" valign="top">
							<table  cellspacing="0" cellpadding="0" border="0" id="myTable" width="100%">
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
								<tr bgcolor="#eeeeee" class="RightTableCaption">
									<td height="25" width="14%">Part Number</td>
									<td width="35%">Description</td>
									<!-- the following condition is added to show the Part Size Column in Pic Slip based on the Rule Value[if it is "Y"] -->
									<%if(strShowPartSize.equals("Y")){%>
										<td width="18%">Size</td>
									<%}%>
									<td width="8%" align="center">Req. Qty</td>
									<td width="8%" align="center">Qty</td>
									<td width="25%"><fmt:message key="PICK_SLIP.ITEM_CONTROLNUM"/></td>
									
								 <% if (strExpiryDateFl.equals("Y")) { %>	
									
									<td width="18%"><fmt:message key="PICK_SLIP.EXPIRY_DATE"/></td>
								<%} %>
								</tr>
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
<%
						intSize = alSetLoad.size();
						if (intSize > 0)
						{
							hcboVal = new HashMap();
							for (int i=0;i<intSize;i++)
							{
								hcboVal = (HashMap)alSetLoad.get(i);
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
%>
								<tr <%=strShade%>>
									<td class="RightText" height="20" width="14%"><%=(String)hcboVal.get("PNUM")%></td>
									<td class="RightText" width="35%"><%=GmCommonClass.getStringWithTM((String)hcboVal.get("PDESC"))%></td>
									<!-- the following condition is added to show the Part Size Column in Pic Slip based on the Rule Value[if it is "Y"] -->
									<%if(strShowPartSize.equals("Y")){%>
										<td class="RightText" width="18%"><%=GmCommonClass.parseNull((String)hcboVal.get("PARTSIZE"))%></td>
									<%}%>
									<td align="center" class="RightText" width="8%"><%=(String)hcboVal.get("IQTY")%></td>
<%									if(Double.parseDouble(strConsignStatusFlag) >= 2) {  %>
									<td class="RightText" width="8%" align="center">_ _ _
									<td class="RightText" width="25%"><%=(String)hcboVal.get("CNUM")%>
<% } else { %>									
									<td class="RightText" width="8%" align="center">_ _ _
									<td class="RightText" width="25%">_ _ _ _ _ _ _ _ _ _
<% } %>		

                               <% if (strExpiryDateFl.equals("Y")) { %>	

										<td class="RightText" width="18%"><%=GmCommonClass.parseNull((String)hcboVal.get("EXPRYDATE"))%>					
									</td>
							  <% } %>		
									
							</td>											
								</tr>
								<tr><td colspan="6" height="1" bgcolor="#eeeeee"></td></tr>
<%
							}
						}else {
%>
						<tr><td colspan="4" height="50" align="center" class="RightTextRed"><BR>Transaction does not contain any part numbers !</td></tr>
<%
						}		
%>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" height="1"></td></tr>
					<tr>
						<td bgcolor="#eeeeee" colspan="2">
							<jsp:include page="/common/GmRuleDisplayInclude.jsp">
							<jsp:param name="Show" value="false" />
							<jsp:param name="Fonts" value="false" />
							</jsp:include>
						</td>
					</tr>
				<%-- <% if (!strConsignStatusFlag.equals("4")){ //Packslip %>  --%>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr valign="top" height="60">
						<td colspan="2" class="RightText"><b>Comments:</b><BR>&nbsp;<%=strDesc%></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>					
					<tr><td class="RightText" colspan="2"><u>Please Initial here:</u></td></tr>					
					<tr><td colspan="2" height="50">&nbsp;</td></tr>
					<tr>
						<td class="RightText">&nbsp;&nbsp;&nbsp;Initiated/Requested By</td>
						<td class="RightText" align="right">Verified By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
				<% 
					/* } */
		
				 %>
				 <%
	if (!strGmCompanyVat.equals("") || !strGmCompanyCst.equals("")|| !strGmCompanyPAN.equals("")){
                 %>
				<tr><td height=1 class=RightText valign=top bgcolor="#666666" colspan="2"></td></tr> 
				<tr><td height=10 class=RightText valign=top ></tr>
				<tr>
				<td height=20 width=350  class=RightText valign=top>&nbsp;Company's VAT TIN :&nbsp; <%=strGmCompanyVat%> </td>
				</tr>
				<tr>
				<td height=20 width=350 class=RightText valign=top>&nbsp;Company's CST No :&nbsp; <%=strGmCompanyCst%></td>
				</tr>
				<tr>
				<td height=20 class=RightText valign=top>&nbsp;Company's PAN &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<%=strGmCompanyPAN%></td>
				</tr>
				<tr>
				<td height=1 width=350 class=RightText valign=top>
				<td width=350 class=RightText valign=top  rowspan="1" bgcolor="#666666"></td> 
				</tr>
								
					<tr>
						<td colspan="2">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr class="RightTableCaption">
									<td height="44" align="center" width="152">&nbsp;</td>
									<td  width="1" rowspan="11"></td>
									<td width="154" align="center">&nbsp;</td>
									<td bgcolor="#666666" width="1" rowspan="11"></td>
									<td width="50" align="center">&nbsp;</td>
									<td width="250" align="center" valign="top">&nbsp;<b>for Globus Medical India Pvt.Ltd</b></td>
								</tr>
								
								<tr>
									<td class="RightText" rowspan="9" valign="top"></td>
									<td rowspan="5" class="RightText" valign="top">&nbsp;</td>
									<td height="25" class="RightText" align="center">&nbsp;</td>
									<td class="RightTableCaption" align="center">&nbsp;Authorised Signatory</td>
								</tr>
					
								
							</table>
														
							</td>
							</tr>
				
				 <%
	}
%>

				 </table>
			 </td>
		</tr>
		<tr><td colspan="3" height="1"></td></tr>		
    </table>
	<table align ="center" border="0" class="DtRuleTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td>
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					<jsp:include page="/common/GmIncludeDateStamp.jsp" />
			</td>	
		</tr>
	</table>
</FORM>
</BODY>

</HTML>