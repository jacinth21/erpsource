<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<% 
 /**********************************************************************************
 * File		 		: GmDOEditControlNumber.jsp
 * Desc		 		: This screen is used to Edit the ControlNumber Details.
 * Version	 		: 1.0
 * author			: DhanaReddy
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ taglib prefix="fmtDoEditControlNumber" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmDOEditControlNumber.jsp -->
<fmtDoEditControlNumber:setLocale value="<%=strLocale%>"/>
<fmtDoEditControlNumber:setBundle basename="properties.labels.custservice.GmDOEditControlNumber"/>
<bean:define id="orderTypeNm" name="frmDOSummaryEdit" property="orderTypeNm" type="java.lang.String"></bean:define>
<bean:define id="alCtrlNmDetails" name="frmDOSummaryEdit" property="alCtrlNmDetails" type="java.util.ArrayList"></bean:define>
<bean:define id="orderID" name="frmDOSummaryEdit" property="orderID" type="java.lang.String"></bean:define>
<bean:define id="accountID" name="frmDOSummaryEdit" property="accountID" type="java.lang.String"></bean:define>
<bean:define id="caseID" name="frmDOSummaryEdit" property="caseID" type="java.lang.String"></bean:define>
<bean:define id="caseInfoID" name="frmDOSummaryEdit" property="caseInfoID" type="java.lang.String"></bean:define>
<%
Object bean = pageContext.getAttribute("frmCaseBuilder", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);

request.setAttribute("alControlNumberDetails", alCtrlNmDetails );
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
String strSessDeptId = GmCommonClass.parseNull((String)session.getAttribute("strSessDeptId"));
String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
String strSessTodaysDate = GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate"));
String strLotOverideAccess = GmCommonClass.parseNull((String)request.getAttribute("LOTOVERRIDEACCESS"));
String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
HashMap hmResult = new HashMap();
HashMap hmReturn = new HashMap();
HashMap hmAccsum = new HashMap();
HashMap hmOrderDetails = new HashMap();
hmReturn = (HashMap)request.getAttribute("hmResult");
String strRegn ="";
String strAdName ="";
String strDistName ="";
String strTerrName ="";
String strRepNm ="";
String strRepId ="";
String strGpoNm ="";
String strDoID = "";
String strCaseID = "";
String strAccountID = "";
String strAccountName = "";
String strRepName = "";
String strRepID = "";
String strCustPo = "";
String strCaseInfoID = "";
String strErrorMsg = ""; 
String strOrderTypeNm = "";
String strInvoiceId="";
String straccessBtn="";

HashMap hmControlNumDet = new HashMap();
hmResult = (HashMap)request.getAttribute("hmReturn");
strErrorMsg  = GmCommonClass.parseNull((String)request.getAttribute("ERRMSG"));
String strFormName = GmCommonClass.parseNull(request.getParameter("FRMNAME"));
String strAction = GmCommonClass.parseNull(request.getParameter("hAction"));
String strOrdId = GmCommonClass.parseNull((String)hmOrderDetails.get("ID"));
String strScreenName = "EditControl";
if (hmResult != null)
{
	hmOrderDetails = (HashMap)hmResult.get("ORDERDETAILS");
	strDoID = GmCommonClass.parseNull((String)hmOrderDetails.get("ID"));
	strCaseID = GmCommonClass.parseNull((String)hmOrderDetails.get("CASE_ID"));
	strAccountID = GmCommonClass.parseNull((String)hmOrderDetails.get("ACCID"));
	strAccountName = GmCommonClass.parseNull((String)hmOrderDetails.get("ANAME"));
	strRepName = GmCommonClass.parseNull((String)hmOrderDetails.get("REPNM"));
	strRepID = GmCommonClass.parseNull((String)hmOrderDetails.get("REPID"));
	strCustPo = GmCommonClass.parseNull((String)hmOrderDetails.get("PO"));
	strCaseInfoID = GmCommonClass.parseNull((String)hmOrderDetails.get("CASEINFO_ID"));
	strOrderTypeNm = GmCommonClass.parseNull((String)hmOrderDetails.get("ORDERTYPENAME"));
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmDOEditControlNumber", strSessCompanyLocale);	
	if (orderTypeNm != null && orderTypeNm.equals("") && strOrderTypeNm.trim().equals(""))
	{
		orderTypeNm =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_BILL_SHIP"));		
	}
	String strAccessEnable = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.SHOW_USAGE_LOT"));
	strInvoiceId =GmCommonClass.parseNull(GmCommonClass.parseNull((String)hmOrderDetails.get("INVOICEID")));
	if(!strInvoiceId.equals("")&&(strAccessEnable.equals("Y"))){
		straccessBtn="Y";
	}
	
}

if(hmReturn != null){
hmAccsum = (HashMap)hmReturn.get("ACCSUM");
strRegn = GmCommonClass.parseNull((String)hmAccsum.get("RGNAME"));
strAdName = GmCommonClass.parseNull((String)hmAccsum.get("ADNAME"));
strDistName = GmCommonClass.parseNull((String)hmAccsum.get("DNAME"));
strTerrName = GmCommonClass.parseNull((String)hmAccsum.get("TRNAME"));
strRepNm = GmCommonClass.parseNull((String)hmAccsum.get("RPNAME"));
strRepId = GmCommonClass.parseNull((String)hmAccsum.get("REPID"));
strGpoNm = GmCommonClass.parseNull((String)hmAccsum.get("PRNAME"));
}

String strAcessFl =straccessBtn.equals("Y")?"true":"false";

%>
<html>
<head>
<title>Globus Medical:Edit Control number Details</title>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmDOEditControlNumber.js"></script>
<script>
var deptId = '<%=strSessDeptId%>';
var frmNm = '<%= strFormName%>';
var frmObj = eval("document."+frmNm);
var lotOverrideAccess = '<%=strLotOverideAccess%>';
</script>
</head>
<body leftmargin="20" topmargin="10">
<html:form action="/gmDOSummaryEdit.do?method=editControlNumberDetails">
<html:hidden property="orderID"  name="frmDOSummaryEdit" value="<%=orderID%>" />
<html:hidden property="inputStr" name="frmDOSummaryEdit" value=""/>
<html:hidden property="strOpt" name="frmDOSummaryEdit" value=""/>
<html:hidden property="caseInfoID" name="frmDOSummaryEdit" value="<%=caseInfoID%>"/>
<html:hidden property="accountID" name="frmDOSummaryEdit" value="<%=accountID%>"/>

<input type="hidden" name="hAction"  value="EditOrder"/>
<input type="hidden" name="hOrdId" value="<%=strDoID%>">
<input type="hidden" name="FRMNAME" value="<%=strFormName%>">

 <table border="0" class="DtTable725" cellspacing="0" cellpadding="0">
	<tr>
			<td height="25" class="RightDashBoardHeader" colspan="5"><fmtDoEditControlNumber:message key="LBL_EDIT_CONTROL_NUMBERS"/></td>
			<td align="right" class=RightDashBoardHeader>
			<fmtDoEditControlNumber:message key="LBL_HELP" var="VarHelp"/>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${VarHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("EDIT_CONTROL_NUMBERS")%>');" />
			</td>	
	</tr>
	<!-- MNTTASK - 8623 - It will be displayed error message in top, when enter greater than ORDER QTY and click submit. --> 
	<%if(!strErrorMsg.equals("")){%>
		<tr><td colspan="5" style="color:red"><%=strErrorMsg%></td></tr>
	<%}%>
	<tr>
		<tr class="evenshade">
			<td height="25" class="RightTableCaption" align="right"><fmtDoEditControlNumber:message key="LBL_DO_ID"/> :</td>
			<td >&nbsp;<%=strDoID %></td>
			<td height="25" class="RightTableCaption" align="right"><fmtDoEditControlNumber:message key="LBL_CASE_ID"/> :</td>
			<td>&nbsp;<%=strCaseID %></td>
			<td height="25" class="RightTableCaption" align="Right" width=12%>&nbsp;<fmtDoEditControlNumber:message key="LBL_CUSTOMER_PO"/>:</td>
			<td >&nbsp;<%=strCustPo%></td>			
		</tr>		
		<tr><td class="LLine" height="1" colspan="6"></td></tr>
		<tr class="oddshade">
		<td height="25" class="RightTableCaption" align="right"><fmtDoEditControlNumber:message key="LBL_ACCOUNT_ID"/> :</td>
			<td >&nbsp;<%=strAccountID %></td>		
			<td height="25" class="RightTableCaption" align="right"><fmtDoEditControlNumber:message key="LBL_ACCOUNT_NAME"/> :</td>
			<td colspan="2">&nbsp;<%=strAccountName %></td>	
			<td >&nbsp;</td>				
		</tr>				  
	<tr><td class="LLine" height="1" colspan="6"></td></tr>
	<%if(!strSessDeptId.equals("S")){ %>
		<tr class="evenshade">
			<td height="25" class="RightTableCaption" align="Right">&nbsp;<fmtDoEditControlNumber:message key="LBL_REGION"/>: </td>
			<td >&nbsp;<%=strRegn %></td>
			<td height="25" class="RightTableCaption" align="Right">&nbsp;<fmtDoEditControlNumber:message key="LBL_AREA_DIRECTOR"/>:</td>
			<td >&nbsp;<%=strAdName %></td>
			<td height="25" class="RightTableCaption" align="Right">&nbsp;<fmtDoEditControlNumber:message key="LBL_FIELD_SALES"/>:</td>
			<td >&nbsp;<%=strDistName %></td>
		</tr>
		<tr>
				<td class="LLine" height="1" colspan="6"></td>
		</tr>
		<tr class="oddshade">
			<td class="RightTableCaption" align="Right"  height="25">&nbsp;<fmtDoEditControlNumber:message key="LBL_TERRITORY"/>:</td>
			<td >&nbsp;<%=strTerrName %></td>
			<td class="RightTableCaption" align="Right" height="25">&nbsp;<fmtDoEditControlNumber:message key="LBL_REP"/>:</td>
			<td >&nbsp;<%=strRepName%> &nbsp;(<%=strRepId %>)</td>
			<td class="RightTableCaption" align="Right">&nbsp;<fmtDoEditControlNumber:message key="LBL_GROUP_PRICE_BOOK"/>:</td>
			<td >&nbsp;<%=strGpoNm%></td>
		</tr>
		<tr>
				<td class="LLine" height="1" colspan="6"></td>
		</tr>
		<tr class="evenshade">
			<td height="25" class="RightTableCaption" align="Right" width=15%>&nbsp;<fmtDoEditControlNumber:message key="LBL_DATE_ENTERED"/>:</td>
			<td >&nbsp;<%=strSessTodaysDate%></td>
			<td height="25" class="RightTableCaption" align="Right" >&nbsp;<fmtDoEditControlNumber:message key="LBL_ORDER_TYPE"/>:</td>
			<td align="left">&nbsp;<%=orderTypeNm%></td>
			<td class="RightTableCaption" colspan="2"></td>
		</tr>
		<tr>
				<td class="LLine" height="1" colspan="6"></td>
		</tr>
		<tr class="oddshade">
		<%}else{ %>	
		<tr class="evenshade">
	<%}%>
	<td class="RightText" height="25" colspan="6">&nbsp;<img border="0"  src="<%=strImagePath%>/30.gif" height="15" width="15"> &nbsp;<fmtDoEditControlNumber:message key="LBL_EDIT_CONTROL_NUMBER"/> &nbsp; <IMG id="plus" border=0 src="<%=strImagePath%>/plus.gif"> &nbsp;<fmtDoEditControlNumber:message key="LBL_MORE_CONTROL_NUMBER"/> </td>
	</tr>
	<tr><td class="LLine" height="1" colspan="6"></td></tr>
	<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
		<TR>
			<td colspan="6"> 
			<div>
				<jsp:include page="/custservice/GmControlNumberCartInclude.jsp"/>
			
			</div>
			</td>
		</TR>
	<tr >
		  <td align="center" height="25" colspan="6" > <br>
		  <fmtDoEditControlNumber:message key="BTN_SAVE" var="varSave"/>
		  <fmtDoEditControlNumber:message key="BTN_CANCEL" var="varCancel"/>
		  		<%
		  		int intSize = alCtrlNmDetails.size();
		  		String strSubmit = "fnValidateCtrlNum('"+strScreenName+"')";
			  		if ( intSize > 0 )
					{				
		  		%>
				<gmjsp:button style="width: 5em" value="${varSave}" name="Btn_Save"  gmClass="button"  onClick="<%=strSubmit%>" tabindex="3" buttonType="Save" disabled="<%=strAcessFl%>" /> &nbsp;&nbsp;&nbsp;
				 <% } %>
				<gmjsp:button value="${varCancel}" style="width: 5em" gmClass="button" onClick="fnCancel();" tabindex="4" buttonType="Save" />&nbsp;
		 </td>
	</tr>
		<tr>
				<td height="7" colspan="6"></td>
		</tr>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>
		