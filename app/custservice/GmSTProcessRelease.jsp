 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmSTProcessRelease.jsp
 * Desc		 		: This screen is used for the release the stock tranfer request
 * Version	 		: 1.0
 * author			: Dinesh R
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.Date" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%> 
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtStackTransferControl" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmSTProcessRelease.jsp -->
<fmtStackTransferControl:setLocale value="<%=strLocale%>"/>
<fmtStackTransferControl:setBundle basename="properties.labels.custservice.GmStockTransfer"/>

<bean:define id="alrqitemdetails" name="frmStockTransferReport" property="alrqitemdetails" type="java.util.ArrayList"> </bean:define>
<bean:define id="alReleaseFrm" name="frmStockTransferReport" property="alReleaseFrm" type="java.util.ArrayList"> </bean:define>
<bean:define id="requeststatusflag" name="frmStockTransferReport" property="requeststatusflag" type="java.lang.String"> </bean:define>
<bean:define id="requireddate" name="frmStockTransferReport" property="requireddate" type="java.lang.String"> </bean:define>
<bean:define id="alreleasetransdtls" name="frmStockTransferReport" property="alreleasetransdtls" type="java.util.ArrayList"> </bean:define>

<%

String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");

String strWikiTitle = GmCommonClass.getWikiTitle("STOCK_TRANSFER_PROCESS_RELEASE");

String strPartNum = "";
String strPartDesc = "";
String strRequestedQty = "";
String strQtyToRelease = "";
String strTransactionID = "";
String strRequestID = "";
String strStatus = "";
String strUpdatedDate = "";
String strUpdatedBy = "";
%>

<HTML>
<HEAD>
	<TITLE> Globus Medical: Stock Transfer Release for Control  </TITLE>
	<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
	<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
	<script language="javascript" src="<%=strcustserviceJsPath %>/GmStockTransfer.js"></script>
	<script>
		var intSize = <%=alrqitemdetails.size()%>;
		var reqDate = '<%=requireddate%>';
	</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form  action="/gmStockTransferAction.do?method=stockTransferReleaseControl">
<html:hidden property="strOpt" name="frmStockTransferReport"/>
<html:hidden property="fgInputStr" name="frmStockTransferReport"/>
<html:hidden property="blInputStr" name="frmStockTransferReport"/>
<html:hidden property="transId" name="frmStockTransferReport"/>

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td height="25" class="RightDashBoardHeader" colspan="3"><fmtStackTransferControl:message key="LBL_ST_RELEASE_CTRL_HEADER"/></td>
					<fmtStackTransferControl:message key="LBL_HELP" var="varHelp"/>
					<td height="25" class="RightDashBoardHeader" align="right"><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
				</tr>
				</table>
			</td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="748" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
						<fmtStackTransferControl:message key="LBL_TRANS_ID" var="varTransId"/>
						<td class="RightText" HEIGHT="20" align="right" >&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varTransId}:" td="false"/></td>
						<td class="RightText">&nbsp;<b><bean:write name="frmStockTransferReport" property="transId"></bean:write></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<fmtStackTransferControl:message key="LBL_MASTER_TRANS_ID" var="varMasterTransId"/>
						&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varMasterTransId}:" td="false"/>
						&nbsp;<b><bean:write name="frmStockTransferReport" property="masterRequestId"></bean:write></b></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr class="Shade">
						<fmtStackTransferControl:message key="LBL_TRANS_TYPE" var="varTransType"/>
						<td class="RightText" HEIGHT="20" align="right" >&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varTransType}:" td="false"/></td>
						<td class="RightText">&nbsp;<b><bean:write name="frmStockTransferReport" property="requestfor"></bean:write></b></td>
					</tr>
					<tr > 
						<fmtStackTransferControl:message key="LBL_REQ_COMP_NM" var="varReqCompnm"/>
						<td class="RightText" HEIGHT="20" align="right" >&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varReqCompnm}:" td="false"/></td>
						<td class="RightText">&nbsp;<b><bean:write name="frmStockTransferReport" property="rqcompnm"></bean:write></b></td>
					</tr>					
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr class="Shade">
					 	<fmtStackTransferControl:message key="LBL_INITIATED_BY" var="varInitiateBy"/>
						<td class="RightText" HEIGHT="20" align="right" >&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varInitiateBy}:" td="false"/></td>
						<td class="RightText">&nbsp;<b><bean:write name="frmStockTransferReport" property="rqinitby"></bean:write></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<fmtStackTransferControl:message key="LBL_REQUIRED_DATE"/> :&nbsp;<b><bean:write name="frmStockTransferReport" property="requireddate"></bean:write></b></td>
					</tr>
					<tr height="20"> </tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
	            	<tr>
						<td colspan="2">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td colspan="2">
										<table cellspacing="0" cellpadding="0" border="0" width="100%">
				  							<thead>
												<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
											<%
												if(requeststatusflag.equals("0")){
											%>
												<tr class="Shade">
													<td align="center" height="25"><b><fmtStackTransferControl:message key="LBL_PART"/></b></td>
													<td align="center" ><b><fmtStackTransferControl:message key="LBL_DESCRIPTION"/></b></td>
													<td align="center" class="RightText"><b><fmtStackTransferControl:message key="LBL_REQUESTED_QTY"/></b></td>
													<td align="center" class="RightText"><b><fmtStackTransferControl:message key="LBL_RELEASE_FROM"/></b></td>
													<td align="center" class="RightText"><b><fmtStackTransferControl:message key="LBL_AVAILABLE_QTY"/></b></td>
													<td align="center" class="RightText"><b><fmtStackTransferControl:message key="LBL_QTY_TO_RELEASE"/></b></td>
												</tr>
												<tr><td colspan="11" height="1" bgcolor="#666666"></td></tr>
											</thead>
											<tbody>
												<%			
													int intSize = alrqitemdetails.size();
													if (intSize > 0)
													{
														HashMap hItemDetails = new HashMap();
														for (int i=0;i<intSize;i++)
														{
														  hItemDetails =  (HashMap)alrqitemdetails.get(i);
									  
														  strPartNum = GmCommonClass.parseNull((String)hItemDetails.get("PNUM"));
														  strPartDesc = GmCommonClass.parseNull((String)hItemDetails.get("PDESC"));
														  strRequestedQty = GmCommonClass.parseNull((String)hItemDetails.get("QTY"));
														  strQtyToRelease = GmCommonClass.parseNull((String)hItemDetails.get("QTY"));
												%>
												<tr>
													<td width="80" align="center"><span id="SpPart<%=i%>">&nbsp;<%=strPartNum%></span>
														<input type="hidden" name="hPartNum<%=i%>" value="<%=strPartNum%>" id ="<%=i%>" />
													</td>
													<td ><span id="SpDes<%=i%>">&nbsp;<%=strPartDesc%></span></td>
													<td align="center"><span id="requestQty<%=i%>"><%=strRequestedQty%></span>
														<input type="hidden" name="hrequestQty<%=i%>" value="<%=strRequestedQty%>" id ="<%=i%>" />
													</td>
													<td align="center">
														<select name="Cbo_Releasefrom<%=i%>" id="Cbo_Releasefrom<%=i%>" class="RightText"  onChange="javascript:fnCallAjax(hPartNum<%=i%>.value,this.value,<%=i%>);" id ="<%=i%>" >
															<option value="0" ><fmtStackTransferControl:message key="LBL_CHOOSE_ONE"/></option>
															<% int intSizeRF = alReleaseFrm.size();
																HashMap hcboValRF = new HashMap();
																for (int j = 0; j < intSizeRF; j++) {
																    hcboValRF = (HashMap) alReleaseFrm.get(j);
																	String strCodeID = (String) hcboValRF.get("CODEID");
																	String strCodeName = (String) hcboValRF.get("CODENM");
																
															%>
															<option value="<%=strCodeID%>"><%=strCodeName%></option>
															<%
																}
															%>							 
														</select>	
													</td>
													<td align="center"  class="RightText"  >  <input type="text" name="Txt_Stock<%=i%>" value="" size="5" class="hide" id ="<%=i%>" READONLY> </td>
													<td id="Cell<%=i%>" align="center">
														<span id="Sp<%=i%>">
														<input type="text" size="2" id="Txt_Qty_To_Release<%=i%>" name="Txt_Qty_To_Release<%=i%>" value="<%=strQtyToRelease%>"
																onChange="javascript:fnChangeReleaseFrom(this.value,<%=i%>);"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">&nbsp;
														</span>
													</td>
												</tr>		
												<%		}
													}
												%>					
												<tr><td colspan="5" height="2" bgcolor="#eeeeee"></td></tr>
												<tr >
													<fmtStackTransferControl:message key="BTN_BACK" var="varBack"/>
													<td colspan="10" align="center"><gmjsp:button gmClass="Button" name="Btn_Back" onClick="fnBack();" buttonType="Load" value="${varBack}"/>
													<fmtStackTransferControl:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button gmClass="Button" name="Btn_Submit" onClick="fnSubmit();" buttonType="Save" value="${varSubmit}"/></td>
												</tr>
												<tr><td></td></tr>
										<%
											}else{
										%>					
												<tr><td align="Left" height="25"><b><fmtStackTransferControl:message key="LBL_RELEASED_TRANS"/></b></td></tr>
												<tr class="Shade">
													<td align="center" height="25"><b><fmtStackTransferControl:message key="LBL_TRANS_ID"/></b></td>
													<td align="center" ><b><fmtStackTransferControl:message key="LBL_REQUEST_ID"/></b></td>
													<td align="center" class="RightText"><b><fmtStackTransferControl:message key="LBL_STATUS"/></b></td>
													<td align="center" class="RightText"><b><fmtStackTransferControl:message key="LBL_UPDATED_DATE"/></b></td>
													<td align="center" class="RightText"><b><fmtStackTransferControl:message key="LBL_UPDATED_BY"/></b></td>
												</tr>
												<tr><td colspan="11" height="1" bgcolor="#666666"></td></tr>
											</tbody>
											
											<tbody>
												<%						
													int intTransSize = alreleasetransdtls.size();
													if (intTransSize > 0)
													{
														HashMap hTransDetails = new HashMap();
														for (int i=0;i<intTransSize;i++)
														{
														  hTransDetails =  (HashMap)alreleasetransdtls.get(i);
														  
														  strTransactionID = GmCommonClass.parseNull((String)hTransDetails.get("CONS_ID"));
														  strRequestID = GmCommonClass.parseNull((String)hTransDetails.get("REQ_ID"));
														  strStatus = GmCommonClass.parseNull((String)hTransDetails.get("STATUS_FL"));
														  strUpdatedDate = GmCommonClass.parseNull((String)hTransDetails.get("UPDATED_DATE"));
														  strUpdatedBy = GmCommonClass.parseNull((String)hTransDetails.get("UPDATED_BY"));
												%>
													<tr>
														<td align="center"><span id="ConsId<%=i%>"><%=strTransactionID%></span>
														<td align="center"><span id="requestId<%=i%>"><%=strRequestID%></span>
														<td align="center"><span id="requestStatus<%=i%>"><%=strStatus%></span>
														<td align="center"><span id="updatedDt<%=i%>"><%=strUpdatedDate%></span>
														<td align="center"><span id="updatedBy<%=i%>"><%=strUpdatedBy%></span>
													</tr>
												<%		}
													}
												%>
													<tr><td colspan="5" height="2" bgcolor="#eeeeee"></td></tr>
													<tr >
														<fmtStackTransferControl:message key="BTN_BACK" var="varBack"/>
														<td colspan="10" align="center"><gmjsp:button gmClass="Button" name="Btn_Back" onClick="fnBack();" buttonType="Load" value="${varBack}"/></td>
													</tr>
										<%
											}
										%>
											</tbody>
									</table>
								</td>
							</tr>	
						<tr><td colspan="2" height="1" class="Line"></td></tr>
					</table>
				</td>
		 	</tr>
		</table>
		</td>
	</tr>
</table>
</html:form>
</BODY>

<%@ include file="/common/GmFooter.inc" %>
</HTML>
