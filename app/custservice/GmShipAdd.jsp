 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmShipAdd.jsp
 * Desc		 		: This screen is used for the
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtShipAdd" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmShipAdd.jsp -->
<fmtShipAdd:setLocale value="<%=strLocale%>"/>
<fmtShipAdd:setBundle basename="properties.labels.party.GmAddressSetup"/>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strShipAdd = (String)request.getAttribute("hShipAdd")==null?"":(String)request.getAttribute("hShipAdd");
	String strScrType = (String)request.getAttribute("hScrType")==null?"":(String)request.getAttribute("hScrType");
	GmResourceBundleBean gmResourceBundleBeanlbl = 
	    GmCommonClass.getResourceBundleBean("properties.labels.party.GmAddressSetup", strSessCompanyLocale);
	
	String 	strHeader =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SHIP_ADDRESS_LOOKUP"));
	if(strScrType.equals("InhouseLoaner")){
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_DEFAULT_SHIPPING_ADDRESS"));
	}
	
	
%>
<script type="text/javascript">
function fnClose(){
	dhxWins = new dhtmlXWindows();
	if(parent.dhxWins.isWindow("w1") == true){
	parent.dhxWins.window("w1").close();	
	}else{
	window.close();
	}
}
</script>
<HTML>
<HEAD>
<TITLE> Globus Medical: Ship Address Lookup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"></script>
</HEAD>

<BODY leftmargin="10" topmargin="10">
<FORM name="frmOrder" method="post">
	<table border="0" width="230" cellspacing="0" cellpadding="0" align=center>
		<tr>
			<td bgcolor="#666666" colspan="3"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td colspan="3" height="25" class="RightDashBoardHeader"><%=strHeader %></td>
					</tr>
					<tr><td bgcolor=#666666 colspan="3" height=1></td></tr>
					<tr>
						<td class="RightText" colspan="3" height=1>
						<%=strShipAdd%>
						</td>
					</tr>
					<tr><td bgcolor=#666666 colspan="3" height=1></td></tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
		</tr>
			<td bgcolor="#666666" colspan="3"></td>
		<tr>
	 </table>
	<table border="0" width="230" cellspacing="0" cellpadding="0" align=center>
		<tr>
			<td align="center" height="30" id="button">
				<fmtShipAdd:message key="BTN_CLOSE" var="varClose"/>
				<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="javaScript:fnClose();" buttonType="Load" />
			</td>
		<tr>
	</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
