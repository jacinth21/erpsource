
<%
	/**********************************************************************************
	 * File		 		: GmICTConsignReport.jsp
	 * Desc		 		: This screen is used for the ICT Report
	 * Version	 		: 1.0
	 * author			: Dhinakaran James
	 ************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Date"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ page import="org.apache.commons.beanutils.RowSetDynaClass"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page buffer="16kb"%>
<%@ taglib prefix="fmtICTConsRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- custservice\GmICTConsignReport.jsp -->
<fmtICTConsRpt:setLocale value="<%=strLocale%>"/>
<fmtICTConsRpt:setBundle basename="properties.labels.custservice.GmICTConsignReport"/>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response, session);
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmICTConsignReport", strSessCompanyLocale);
	//HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strType = GmCommonClass.parseNull((String)(String) request.getAttribute("hType"));
	String strFromDt = GmCommonClass.parseNull((String) request.getAttribute("hFrmDt"));
	String strToDt = GmCommonClass.parseNull((String) request.getAttribute("hToDt"));
	String strOpt = GmCommonClass.parseNull((String) request.getAttribute("strOpt"));
	String strDistId = GmCommonClass.parseZero((String) request.getAttribute("hDistId"));
	String strApplDateFmt = strGCompDateFmt;

	Date dtfromdate = GmCommonClass.getStringToDate(strFromDt,strApplDateFmt);
	Date dttodate = GmCommonClass.getStringToDate(strToDt,strApplDateFmt);

	String strGridData = GmCommonClass.parseNull((String) request.getAttribute("grid_XML"));
	String strScreenType = GmCommonClass.parseNull((String) request.getAttribute("screenType"));
	ArrayList alDist = GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("alDistList"));

	String strWikiTitle = GmCommonClass.getWikiTitle("CONSIGN_ICT_RPT");
	
	String strHeader = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_ICT_HEADER"));
	if(strType.equals("50235")){ // Field Sales - FD
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_FD_HEADER"));
	}else if(strType.equals("50236")){	//Field Sales - ICS
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_ICS_HEADER"));
	}else if(strType.equals("50237")){	//Field Sales - ICA
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_ICA_HEADER"));
	}else if(strType.equals("50234")){	//Field Sales - ICT
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_ICT_HEADER"));
	}
	
	String strHeaderOpt = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_STROPT_"+strOpt.toUpperCase()+"_HEADER"));
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: ICT By Set Reports</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmICTConsignReport.js"></script>

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<script>

var objGridData;

objGridData = '<%=strGridData%>';
var dateFmt = '<%=strApplDateFmt%>';
var strOption = '<%=strOpt%>';
var gridObj = '';
var rowID = '';
var lblFrmDt = '<fmtICTConsRpt:message key="LBL_FROM_DATE"/>';
var lblToDt = '<fmtICTConsRpt:message key="LBL_TO_DATE"/>';

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnOnLoad();" onkeypress="fnEnter();">
	<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmConsignmentReportServlet">
		<input type="hidden" name="Cbo_Set" value="">
		<input type="hidden" name="hAction" value="Load">
		<input type="hidden" name="hType" value="">
		<input type="hidden" name="hDistName" value="">
		<input type="hidden" name="screenType" value="<%=strScreenType%>">


		<table border="0" class="DtTable900" cellspacing="0" cellpadding="0">

			<tr>
				<td height="25" class="RightDashBoardHeader" colspan="7"><%=strHeader %> -
					<fmtICTConsRpt:message key="TD_HEADER_BY"/> <%=strHeaderOpt%> <fmtICTConsRpt:message key="TD_HEADER_RPT"/></td>
					<fmtICTConsRpt:message key="IMG_ALT_HELP" var = "varHelp"/>
				<td class="RightDashBoardHeader" align="right"> <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> </td>	
			</tr>

			<!-- Struts tag lib code modified for JBOSS migration changes -->
			<tr>
				<td class="RightTableCaption" height="30" align="right" width="125">&nbsp;<fmtICTConsRpt:message key="LBL_FIELD_SALES"/>:</td>
				<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Dist"
						seletedValue="<%=strDistId %>" value="<%=alDist%>" width="250" codeId="ID"
						codeName="NAME" defaultValue="[Choose One]" tabIndex="1" />
				</td>
				<fmtICTConsRpt:message key="LBL_FROM_DATE" var="varFromDate"/>
				<td class="RightTableCaption" height="25" align="right" width="120">&nbsp;<gmjsp:label
						type="MandatoryText" SFLblControlName="${varFromDate}:" td="false" /></td>
				<td>&nbsp;<gmjsp:calendar textControlName="Txt_FromDate"
						gmClass="InputArea"
						textValue="<%=dtfromdate==null?null:new java.sql.Date(dtfromdate.getTime())%>"
						onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" tabIndex="2" />&nbsp;
						<fmtICTConsRpt:message key="LBL_TO_DATE" var="varToDate"/>
				<td class="RightTableCaption" height="25" align="right" width="100">&nbsp;<gmjsp:label
						type="MandatoryText" SFLblControlName="${varToDate}:" td="false" /></td>
				<td>&nbsp;<gmjsp:calendar textControlName="Txt_ToDate"
					 	 gmClass="InputArea" 
						textValue="<%=dttodate==null?null:new java.sql.Date(dttodate.getTime())%>"
						onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" tabIndex="3" />&nbsp;</td>
						<fmtICTConsRpt:message key="BTN_LOAD" var="varLoad"/>
				<td colspan="2" align="center"><gmjsp:button
					value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnLoad();"
					tabindex="4" buttonType="Load" />
				</td>
			</tr>

			<%
				if (strGridData.indexOf("cell") != -1) {
			%>
			<tr>
				<td colspan="8">
					<div id="ConsignIctRpt" style="" height="400px" width="898px"></div>
				</td>
			</tr>
			<tr>
				<td colspan="8" align="center">&nbsp;
					<div class='exportlinks'>
						<fmtICTConsRpt:message key="DIV_EXPORT_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp;<a
							href="#" onclick="gridObj.toExcel('/phpapp/excel/generate.php');"><fmtICTConsRpt:message key="DIV_EXCEL"/></a>
					</div></td>
			</tr>
			<%
				} else {
			%>
			<tr>
				<td class="LLine" height="1" colspan="8"></td>
			</tr>
			<tr height="25">
				<td colspan="8" align="center" class="RegularText"><fmtICTConsRpt:message key="MSG_NO_DATA"/></td>

			</tr>
			<%
				}
			%>
		</table>


	</FORM>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
