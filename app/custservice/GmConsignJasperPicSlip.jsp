<%
/**********************************************************************************
 * File		 		: GmConsignJasperPicSlip.jsp
 * Desc		 		: Pic Slip in Jasper Report
 * Version	 		: 1.0
 * author			: HReddi
************************************************************************************/
%>
<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
 <%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>

<%@ taglib prefix="fmtConsignJasperPicSlip" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- custservice\GmConsignJasperPicSlip.jsp -->

<fmtConsignJasperPicSlip:setLocale value="<%=strLocale%>"/>
<fmtConsignJasperPicSlip:setBundle basename="properties.labels.custservice.GmConsignJasperPicSlip"/>


<%
	String strHtmlJasperRpt = "";
	HashMap hmResult = (HashMap)(request.getAttribute("HASHMAPDATA"));
	ArrayList alShippingDetails = (ArrayList)(request.getAttribute("ARRAYLISTDATA"));
	ArrayList alCommnetsLog = (ArrayList)(request.getAttribute("hmLog"));
	String strDate = "";
	String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	String strTodaysDate = GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate"));
	SimpleDateFormat dfDateFmt = new SimpleDateFormat(strApplnDateFmt+" HH:mm:ss");
	Date dtFromDate= null;
	Date dtPrintDate= new Date();
	dtFromDate = GmCommonClass.getStringToDate(strDate,strApplnDateFmt);	
	String strPrintDate = dfDateFmt.format(dtPrintDate);
	String strUname = "";
	String strLogDate = "";
	String strCommnets = "";
	String strTotLog = "";
	String strConsignId = GmCommonClass.parseNull((String)hmResult.get("CID"));
	String strCtype = GmCommonClass.parseNull((String)hmResult.get("CTYPE"));
	String strSource = (String)request.getAttribute("source");
	String strRuleSource = (String) request.getAttribute("strRuleSource");
	String strCrComments= GmCommonClass.parseNull((String)request.getAttribute("CRCOMMENTS"));
	
	ArrayList alSetLoad = new ArrayList();
	HashMap hmConsignDetails = new HashMap();
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	int intPriceSize = 0;
	
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strSetId = "";
	String strSetNm = "";
	String strEtchId =	"";
	String strDesc = "";
	String strRefId = "";
	String strShade = "";
	String strSetName= "";
	String strAccName = "";
	String strIniDate = "";
	String strRequestorName = "";
	String strType = "";
	String strInHousePurpose = "";
	String strConsignStatusFlag = "";
	String strBillNm="";
	String strToHeader="";
	String strCarrier = "";
	String strMode="";
	String strTrackno="";
	String  strLog_Comments="";
	String strCompanyId = "";
	String strCustPO = "";
	String strShowPartSize = "";
	String strUserName = "";

	if (hmReturn != null)
	{
		alSetLoad = (ArrayList)hmReturn.get("SETLOAD");	
		hmConsignDetails = (HashMap)hmReturn.get("CONDETAILS");
		strConsignId = (String)hmConsignDetails.get("CID");
		strAccName = (String)hmConsignDetails.get("ANAME");
		String strDistName = (String)hmConsignDetails.get("DNAME");
		strAccName = strAccName.equals("")?strDistName:strAccName;		
		strIniDate = (String)hmConsignDetails.get("CDATE");
		strDesc = (String)hmConsignDetails.get("COMMENTS");
		strType = (String)hmConsignDetails.get("TYPE");
		strCtype = (String)hmConsignDetails.get("CTYPE");
		strInHousePurpose = (String)hmConsignDetails.get("PURP");
		strRequestorName = GmCommonClass.parseNull((String)hmConsignDetails.get("SHIPADD"));
		strConsignStatusFlag = GmCommonClass.parseNull((String)hmConsignDetails.get("SFL"));
		strCarrier = GmCommonClass.parseNull((String)hmConsignDetails.get("CARRIER"));
		strMode=GmCommonClass.parseNull((String)hmConsignDetails.get("SHIPMODE"));
		strTrackno= GmCommonClass.parseNull((String)hmConsignDetails.get("TRACKNO"));
		strEtchId = GmCommonClass.parseNull((String)hmConsignDetails.get("ETCHID"));
		strEtchId = strEtchId.equals("")?"-":strEtchId;
		strRefId = GmCommonClass.parseNull((String)hmConsignDetails.get("REFID"));
		strCustPO = GmCommonClass.parseNull((String)hmConsignDetails.get("CSTPO"));
		strShowPartSize = GmCommonClass.parseNull((String)hmConsignDetails.get("SHOWSIZE")); 
		strUserName = GmCommonClass.parseNull((String)hmConsignDetails.get("LOGINNAME"));
		
		//When do item initiate enter the value in CustPO in screen. The ref id only for Profoma Invoice. 
		if(strRefId.equals("") && !strCustPO.equals(""))
			strRefId = strCustPO;		
		strLog_Comments= GmCommonClass.parseNull((String)hmConsignDetails.get("LOG_COMMENTS"));
		strCompanyId = GmCommonClass.parseNull((String)hmConsignDetails.get("COMPANYID"));
	}
	if(strConsignStatusFlag.equals("2")||strConsignStatusFlag.equals("3") || strCtype.equals("50154")){
		strDesc+="<BR>"+strCrComments+"<BR>"+strLog_Comments;
	}
	
	String strRuleId = strConsignId + "$" + strCtype;
	String strId = strConsignId+"$"+strSource;
	hmResult.put("2DBARCODE",strRuleId);
	hmResult.put("FEDEXCODE",strId);
	hmResult.put("BILLTO",strAccName);
	hmResult.put("REFID",strRefId);
	hmResult.put("LOGDETAILS",strDesc);
	hmResult.put("PRINTDATE",strPrintDate);
	hmResult.put("UNAME",strUserName);
	
%>
<script>
function fnPrint(){
	window.print();
	return false;
}
function hidePrint(){
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";	
}
function showPrint(){
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<table>
		<tr>		
			<td align="center" id="button">
				<fmtConsignJasperPicSlip:message key="BTN_PRINT" var="varPrint"/>
				<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" tabindex="2" onClick="return fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				<fmtConsignJasperPicSlip:message key="BTN_CLOSE" var="varClose"/>
				<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" tabindex="3"	onClick="window.close();" buttonType="Load" />				
			</td>
		</tr>		
	 	<tr>	
			 <td>
				<%
				String strJasperName 	= "/GmConsignmentJasperPicSlip.jasper";
				GmJasperReport gmJasperReport = new GmJasperReport();			
				gmJasperReport.setRequest(request);
				gmJasperReport.setResponse(response);
				gmJasperReport.setJasperReportName(strJasperName);
				gmJasperReport.setHmReportParameters(hmResult);
				gmJasperReport.setReportDataList(alShippingDetails);
				gmJasperReport.setBlDisplayImage(true);
				strHtmlJasperRpt = gmJasperReport.getHtmlReport();				
				%>	
				<%=strHtmlJasperRpt %>			
			</td>
		</tr>   
</table>
<%@ include file="/common/GmFooter.inc"%>
</BODY>