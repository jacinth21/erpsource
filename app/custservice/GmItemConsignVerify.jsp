 <%
/**********************************************************************************
 * File		 		: GmItemConsignVerify.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>

<%@ taglib prefix="fmtItemConsignVerify" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- custservice\GmItemConsignVerify.jsp -->

<fmtItemConsignVerify:setLocale value="<%=strLocale%>"/>
<fmtItemConsignVerify:setBundle basename="properties.labels.custservice.GmItemConsignVerify"/>

<%
try {
	GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmItemConsignVerify", strSessCompanyLocale);
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");
	String strConsignId = (String)request.getAttribute("hConsignId") == null?"":(String)request.getAttribute("hConsignId");

	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strProjId =	"";
	String strSetId = "";
	String strSetNm = "";
	String strDesc = "";
	String strTemp = "";
	String strShade = "";

	String strSetName= "";
	String strAccName = "";
	String strUserName = "";
	String strIniDate = "";
	String strItemQty = "";
	String strControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strConsignType = "";
	String strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_REPACKAGE_ITEMS_VERIFY"));
	String strLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_REPACKAGING"));
	String strComments = "";
	
	String strFlag = (String)request.getAttribute("hShipFl") == null?"":(String)request.getAttribute("hShipFl");
	strChecked = strFlag.equals("3")?"checked":"";
	String strPrice = "";

	String strStatusFl = "";
	String strShipFl = "";

	ArrayList alSetLoad = new ArrayList();
	HashMap hmConsignDetails = new HashMap();
	ArrayList alEmpList = new ArrayList();

	int intPriceSize = 0;

	if (hmReturn != null)
	{
		alSetLoad = (ArrayList)hmReturn.get("SETLOAD");
		strSetId = (String)session.getAttribute("hSetId") == null?"":(String)session.getAttribute("hSetId");
	
		hmConsignDetails = (HashMap)hmReturn.get("CONDETAILS");
		strConsignId = (String)hmConsignDetails.get("CID");
		strSetName= (String)hmConsignDetails.get("SNAME");
		strAccName = (String)hmConsignDetails.get("ANAME");
		String strDistName = (String)hmConsignDetails.get("DNAME");
		strAccName = strAccName.equals("")?strDistName:strAccName;
		strUserName = (String)hmConsignDetails.get("UNAME");
		strIniDate = (String)hmConsignDetails.get("CDATE");
		strDesc = (String)hmConsignDetails.get("COMMENTS");
		strStatusFl = (String)hmConsignDetails.get("SFL");
		strShipFl = (String)hmConsignDetails.get("SHIPFL");
		strConsignType = (String)hmConsignDetails.get("CTYPE");
		strComments = (String)hmConsignDetails.get("FCOMMENTS");
		if (strhAction.equals("EditVerify"))
		{
			alEmpList = (ArrayList)hmReturn.get("EMPLIST");
		}
	}

	int intSize = 0;
	HashMap hcboVal = null;
	String strVerifiedBy = 	(String)session.getAttribute("strSessUserId");
	
	if (strConsignType.equals("4113") || strConsignType.equals("4115") || strConsignType.equals("4116"))
	{
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_QUARANTINE_ITEMS_VERIFY"));
		strLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_QUARANTINE"));
	}
	
%>
<HTML>
<HEAD>
<TITLE>Globus Medical:Item Consignment </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>

function fnSubmit()
{
	fnStartProgress();
  	document.frmVendor.submit();
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmReprocessMaterialServlet">
<input type="hidden" name="hAction" value="Verify">
<input type="hidden" name="hConsignId" value="<%=strConsignId%>">
<input type="hidden" name="hConsignType" value="<%=strConsignType%>">

	<table border="0" width="600" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
			<td height="25" class="RightDashBoardHeader"><%=strHeader%></td>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="598" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<%=strLabel%> <fmtItemConsignVerify:message key="LBL_REQUEST_ID"/>:</td>
						<td class="RightText">&nbsp;<b><%=strConsignId%></b></td>
					</tr>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtItemConsignVerify:message key="LBL_ACCOUNT_NAME"/>:</td>
						<td class="RightText">&nbsp;<%=strAccName%></td>
					</tr>
					<tr class="shade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtItemConsignVerify:message key="LBL_INITIATED_BY"/>:</td>
						<td class="RightText">&nbsp;<%=strUserName%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtItemConsignVerify:message key="LBL_DATE_INITIATED"/>:&nbsp;<%=strIniDate%></td>
					</tr>
					<tr>
						<td class="RightText" valign="top" align="right" HEIGHT="30"><fmtItemConsignVerify:message key="LBL_INITIAL_COMMENTS"/>:</td>
						<td class="RightText" valign="top" >&nbsp;<%=strDesc%></td>
					</tr>
					<tr class="shade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtItemConsignVerify:message key="LBL_VERIFIED_BY"/>:</td>
<%
		if (strhAction.equals("EditVerify"))
		{
%>						
						<td class="RightText">&nbsp;<select name="Cbo_VerEmp" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtItemConsignVerify:message key="LBL_CHOOSE_ONE"/>
<%
			  		intSize = alEmpList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alEmpList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strVerifiedBy.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%
			  		}
%>
						</td>
<%
		}else{
%>
						<td class="RightText">&nbsp;<%=strVerifiedBy%></td>
<%
		}
%>						
					</tr>					
					<tr>
						<td class="RightText" valign="top" align="right" HEIGHT="60"><fmtItemConsignVerify:message key="LBL_FINAL_COMMENTS"/>:</td>
						<td>&nbsp;<textarea name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1 rows=5 cols=40 value=""><%=strComments%></textarea></td>
					</tr>
					<tr>
						<td colspan="2">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td height="25"><fmtItemConsignVerify:message key="LBL_PART_NUMBER"/></td>
									<td width="300"><fmtItemConsignVerify:message key="LBL_DESCRIPTION"/></td>
									<td width="50"><fmtItemConsignVerify:message key="LBL_QTY"/></td>
									<td width="100"><fmtItemConsignVerify:message key="LBL_CONTROL_NUMBER"/></td>
								</tr>
<%
						intSize = alSetLoad.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();

							String strNextPartNum = "";
							int intCount = 0;
							String strColor = "";
							String strLine = "";
							String strPartNumHidden = "";
							
							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alSetLoad.get(i);
								if (i<intSize-1)
								{
									hmTempLoop = (HashMap)alSetLoad.get(i+1);
									strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
								}
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
								strPartNumHidden = strPartNum;
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
								strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
								strTemp = strPartNum;

								if (strPartNum.equals(strNextPartNum))
								{
									intCount++;
									strLine = "<TR><TD colspan=4 height=1 class=Line></TD></TR>";
								}

								else
								{
									strLine = "<TR><TD colspan=4 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
										strPartNum = "";
										strPartDesc = "";
										strQty = "";
										//strColor = "bgcolor=white";
										strLine = "";
									}
									else
									{
										//strColor = "";
									}
									intCount = 0;
								}

								if (intCount > 1)
								{
									strPartNum = "";
									strPartDesc = "";
									strQty = "";
									//strColor = "bgcolor=white";
									strLine = "";
								}
							
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
								strItemQty = GmCommonClass.parseNull((String)hmLoop.get("IQTY"));
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								out.print(strLine);
%>
								<tr >
									<td class="RightText" height="20">&nbsp;<%=strPartNum%></td>
									<td class="RightText"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td class="RightText">&nbsp;<%=strItemQty%></td>
									<td class="RightText">&nbsp;<%=strControl%></td>
								</tr>
<%
							}//End of FOR Loop
						}else {
%>
						<tr><td colspan="4" height="50" align="center" class="RightTextRed"><BR><fmtItemConsignVerify:message key="LBL_NO_REPACKAGING"/> !</td></tr>
<%
						}		
%>
							</table>
						</td>
					</tr>
<%
				if (!strStatusFl.equals("4"))
				{
%>
					<tr>
						<td colspan="2" height="30" align="center">
							<fmtItemConsignVerify:message key="BTN_SUBMIT" var="varSubmit"/>
							<gmjsp:button value="${varSubmit}" gmClass="button" onClick="fnSubmit();" tabindex="13" buttonType="Save" />&nbsp;&nbsp;
						</td>
					</tr>
<%
				}else{
%>
					<tr>
						<td colspan="2" height="30" align="center" class="RightTextRed"><BR><fmtItemConsignVerify:message key="LBL_CONSIGNMENT_VERIFIED_NO_NEED_CHANGE_IF_CHANGE_MADE_CONTACT_SYSTEMADMIN"/>
						<BR><BR>
						</td>
					</tr>
<%
					}
%>
				 </table>
			 </td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
    </table>

</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
