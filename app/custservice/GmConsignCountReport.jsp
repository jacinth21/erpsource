 <%
/**********************************************************************************
 * File		 		: GmConsignCountReport.jsp
 * Desc		 		: This screen is used for the DashBoard Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Date" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="org.apache.commons.beanutils.RowSetDynaClass"%>
<%@ page buffer = "16kb" %>
<%@ taglib prefix="fmtConsSetRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- custservice\GmConsignCountReport.jsp -->
<fmtConsSetRpt:setLocale value="<%=strLocale%>"/>
<fmtConsSetRpt:setBundle basename="properties.labels.custservice.GmConsignCountReport"/>
<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);	
	//HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strType = (String)request.getAttribute("hType") == null?"50231":(String)request.getAttribute("hType");
	String strFromDt = (String)request.getAttribute("hFrmDt")==null?"":(String)request.getAttribute("hFrmDt");
	String strToDt = (String)request.getAttribute("hToDt")==null?"":(String)request.getAttribute("hToDt");
	String strApplDateFmt = strGCompDateFmt; //GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	Date dtfromdate=GmCommonClass.getStringToDate(strFromDt,strApplDateFmt);
	Date dttodate=GmCommonClass.getStringToDate(strToDt,strApplDateFmt);
	
	String strGrid = GmCommonClass.parseNull((String)request.getAttribute("grid_XML"));
	String strScreenType =GmCommonClass.parseNull((String)request.getAttribute("hScreenType"));
	String strWikiTitle = GmCommonClass.getWikiTitle("CONSIGN_SET_RPT");


	String strHeader = "";
	String strLabel1 = "";
	String strLabel2 = "";
	String strId1 = "";
	String strId2 = "";
	String strName1 = "";
	String strName2 = "";
	
	ArrayList allReportTypes =(ArrayList) request.getAttribute("ALLNEWREPORTTYPE");

	String strReportTypeID = "";
	String strReportTypeDesc = "";
	
	String strReportType = "";

	if (strType.equals("50231")) //SET
	{
		strHeader = "By Set";
		strLabel1 = "Set Name";
		strLabel2 = "Distributor Name";
		strId1 = "ID";
		strId2 = "DID";
		strName1 = "SNAME";
		strName2 = "DNAME";
		strReportType = "50230";
	}
	else if (strType.equals("50230")) //DIST
	{
		strHeader = "By Distributor";		
		strLabel1 = "Distributor Name";
		strLabel2 = "Set Name";
		strId1 = "DID";
		strId2 = "ID";
		strName1 = "DNAME";
		strName2 = "SNAME";
		strReportType = "50230";
	}
	else if (strType.equals("50232")) //INSEt
	{
		strHeader = "By InHouse - Sets";		
		strLabel1 = "Set Name";
		strLabel2 = "Purpose";
		strId1 = "ID";
		strId2 = "DID";
		strName1 = "SNAME";
		strName2 = "DNAME";
		strReportType = "INHOUSE";
	}
	else if (strType.equals("50233"))// INTYPE
	{
		strHeader = "By InHouse - Purpose";		
		strLabel1 = "Purpose";
		strLabel2 = "Set Name";
		strId1 = "DID";
		strId2 = "ID";
		strName1 = "DNAME";
		strName2 = "SNAME";
		strReportType = "INHOUSE";
	}
	if (strType.equals("50234") || strType.equals("50235")|| strType.equals("50236") || strType.equals("50237")) {
		strLabel1 = "Distributor Name";
		strLabel2 = "Set Name";
		strId1 = "DID";
		strId2 = "ID";
		strName1 = "DNAME";
		strName2 = "SNAME";
		if (strType.equals("50234")) //ICT
		{
			strHeader = "Distributor - ICT"; //"By Set";			
			strReportType = "50234";
		}
		else if (strType.equals("50235")) //FD
		{
			strHeader = "Distributor - FD"; //"By Set";			
			strReportType = "50235";			
		}
		else if (strType.equals("50236")) // "ICS"
		{
			strHeader = "Distributor - ICS"; 			
			strReportType = "50236";			
		}
		else if (strType.equals("50237")) //swiss
		{
			strHeader = "Distributor - ICA"; 		
			strReportType = "50237";			
		}
	}	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Consign Process</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>


<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
var objGridData;
objGridData = '<%=strGrid%>';
var dateFmt = '<%=strApplDateFmt%>';
var gridObj = '';
var rowID = '';
var lblFromDt = '<fmtConsSetRpt:message key="LBL_FROM_DATE"/>';
var lblToDt = '<fmtConsSetRpt:message key="LBL_TO_DATE"/>';
var lblType = '<fmtConsSetRpt:message key="LBL_TYPE"/>'


function fnDrillDown(distid,setid,distName )
{
	document.frmAccount.Cbo_Dist.value = distid;
	document.frmAccount.Cbo_Set.value = setid;
	document.frmAccount.hType.value = '<%=strReportType%>';
	document.frmAccount.hDistName.value = distName;
	document.frmAccount.submit();
}

function fnLoadType(val)
{
	document.frmAccount.hType.value = val;
}

function fnLoad()
{
	var obj = document.frmAccount.Cbo_Type;
	val = '<%=strType%>';
	
	if(obj!=undefined)
	{
		
		if (val = '50230')     //DIST
		{
			obj.selectedIndex = '1';
		}
		if (val = '50231')   // SET
		{
			obj.selectedIndex = '2';
		}
		if (val = '50232')    //INSET
		{
			obj.selectedIndex = '3';
		}
		if (val == '50233')  //INTYPE
		{
			obj.selectedIndex = '4';
		}
		if (val == '50234')  // ICT
		{
			obj.selectedIndex = '5';
		}
		if (val == '50235')  // FD
		{
			obj.selectedIndex = '6';
		}
	}
	document.frmAccount.Cbo_ReportType.value = val;
	if(objGridData !=''){
	
		gridObj = initGridData('ConsignSetRpt', objGridData);
		gridObj.enableTooltips("true,false,true,true,true,true,true");
		gridObj.attachHeader("#select_filter,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter");
	}
}

function initGridData(divRef,gridData)
{
	if(gridData !=''){
		var gObj = new dhtmlXGridObject(divRef);
		gObj.setSkin("dhx_skyblue");
		gObj.setImagePath("<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/imgs/");
		gObj.init();	
		gObj.loadXMLString(gridData);	
		return gObj;
	}
		
}

function fnAgree(cnid)
{
	
windowOpener("/GmConsignSetServlet?hId="+cnid+"&hAction=Ack&","Ack","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");

}

function fnPrintVer(cnid)
{
windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId="+cnid,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnReleaseForSale(cnid)
{
	windowOpener('/GmProdReportServlet?hAction=Part&hId=0&hConsignID='+cnid,"RFS","resizable=yes,scrollbars=yes,top=250,left=100,width=1170,height=510");
}

function fnPackslip(cnid)
{
windowOpener("/GmConsignSetServlet?hAction=PackVersion&hId="+cnid,"Pack","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnGo()
{
	fnValidateDropDn('Cbo_ReportType', lblType);
	// validate the Date format
	CommonDateValidation(document.frmAccount.Txt_FromDate,dateFmt,message[10527]+ message[611]);
	CommonDateValidation(document.frmAccount.Txt_ToDate,dateFmt,message[10528]+ message[611]);
	
	fnValidateTxtFld('Txt_FromDate', lblFromDt);
	fnValidateTxtFld('Txt_ToDate', lblToDt);
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmAccount.hAction.value = "Type";
	fnStartProgress();
	document.frmAccount.submit();
}

function Toggle(val)
{
	
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className="";
		tabobj.style.background="#ffffff";
	}
}
function enterPressed(evn) {
if (window.event && window.event.keyCode == 13) {
  fnGo();
} else if (evn && evn.keyCode == 13) {
  fnGo();
}
}
document.onkeypress = enterPressed;
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmConsignmentReportServlet">
<input type="hidden" name="Cbo_Dist" value="">
<input type="hidden" name="Cbo_Set" value="">
<input type="hidden" name="hAction" value="DrillDown">
<input type="hidden" name="hType" value="">
<input type="hidden" name="hDistName" value="">
<input type="hidden" name="screenType" value="<%=strScreenType%>">


	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="7">
				<fmtConsSetRpt:message key="TD_CONS_SET_RPT_HEADER"/> 
			</td>
			<fmtConsSetRpt:message key="IMG_ALT_HELP" var = "varHelp"/>
			<td class="RightDashBoardHeader" align="right"> <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> </td>
		</tr>
		
		<tr><td class="Line" height="1" colspan="8"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<fmtConsSetRpt:message key="LBL_TYPE" var = "varType"/>
		<tr><td class="RightTableCaption" height="30" align="right">&nbsp;<gmjsp:label
						type="MandatoryText" SFLblControlName="${varType}:" td="false" /></td>
				<td>&nbsp;<select name="Cbo_ReportType" class="RightText" tabindex="1" onFocus="changeBgColor(this,'#AACCE8');"  onBlur="changeBgColor(this,'#ffffff');">
				 <option value="0" ><fmtConsSetRpt:message key="OPT_CHOOSE"/>
				<option value="50230"><fmtConsSetRpt:message key="OPT_CONSIGN"/>
				<option value="50232"><fmtConsSetRpt:message key="OPT_INHOUSE_CONS"/>
			</select></td>
					<fmtConsSetRpt:message key="LBL_FROM_DATE" var = "varFromDate"/>
					<td class="RightTableCaption" height="25" align="right">&nbsp;<gmjsp:label
						type="MandatoryText" SFLblControlName="${varFromDate}:" td="false" /></td>
					
					<td>&nbsp;<gmjsp:calendar textControlName="Txt_FromDate" gmClass="InputArea" textValue="<%=dtfromdate==null?null:new java.sql.Date(dtfromdate.getTime())%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="2"/>&nbsp;
					<fmtConsSetRpt:message key="LBL_TO_DATE" var = "varToDate"/>
					<td class="RightTableCaption" height="25" align="right">&nbsp;<gmjsp:label
						type="MandatoryText" SFLblControlName="${varToDate}:" td="false" /></td>
					<td>&nbsp;<gmjsp:calendar textControlName="Txt_ToDate" gmClass="InputArea" textValue="<%=dttodate==null?null:new java.sql.Date(dttodate.getTime())%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="3"/>&nbsp;
					</td>
					<fmtConsSetRpt:message key="BTN_LOAD" var = "varLoad"/>
					<td colspan="2" align="center"><gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="Button" onClick="fnGo();" tabindex="4" buttonType="Load" /></td>
				</tr>
		
		
		<%
					if (strGrid.indexOf("cell") != -1) {
				%>
		<tr>
			<td colspan="8">
				<div id="ConsignSetRpt" style="height: 400px;"></div>
			</td>
		</tr>
		
		<tr>
	              <td colspan="8" align="center">&nbsp;
	              <div class='exportlinks'><fmtConsSetRpt:message key="DIV_EXPORT_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
	                              onclick="gridObj.toExcel('/phpapp/excel/generate.php');"><fmtConsSetRpt:message key="DIV_EXCEL"/></a>
	                              </div>
	              </td>
			</tr>
		<%}else{ %>
		<tr><td class="LLine" colspan="8" height="1"></td></tr>
		<tr height="25">
			<td colspan="8" align="center" class="RegularText">
				<fmtConsSetRpt:message key="MSG_NO_DATA"/>
			</td>
			
		</tr>
		<%} %>
	</table>


</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
