
<!-- \custservice/GmDODocumentUpload.jsp -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtDOFileUpload" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtDOFileUpload:setLocale value="<%=strLocale%>"/>
<fmtDOFileUpload:setBundle basename="properties.labels.custservice.GmDODocumentUpload"/>

 
<bean:define id="strOpt" name="frmDODocumentUpload" property="strOpt" type="java.lang.String"> </bean:define>
<bean:define id="gridData" name="frmDODocumentUpload" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="intResultSize" name="frmDODocumentUpload" property="intResultSize" type="java.lang.Integer"> </bean:define>
<bean:define id="uploadTypeId" name="frmDODocumentUpload" property="uploadTypeId" type="java.lang.String"> </bean:define>
<bean:define id="refId" name="frmDODocumentUpload" property="refId" type="java.lang.String"> </bean:define>
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("DO_DOCUMENT_UPLOAD");
	
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");
	
	String strRefId = "";
	
	
%>
 
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: DO File Upload</title>

<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmGridCommon.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmDODocumentUpload.js"></script>
<script>
var objGridData;
objGridData = '<%=gridData%>';
var lblOrderId = '<fmtDOFileUpload:message key="LBL_ORDER_ID"/>';
</script>

</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();" >
<html:form action="/gmDODocumentUpload.do?method=loadOrderUpload" enctype="multipart/form-data" >

 <html:hidden property="strOpt"/>
 <html:hidden property="uploadTypeId" value="26240412"/>
 <html:hidden property="haction" value=""/>
<html:hidden property="refId"/>
<html:hidden property="forwardAction"/>
<html:hidden property="fileInputStr" />
<html:hidden property="hTxnId" value="" />
<html:hidden property="hTxnName" value="" />
<html:hidden property="accessDcoed"/>
<html:hidden property="accessNonDcoed"/>
<input type="hidden" name="hAction" value="" />
<input type="hidden" name="hCancelType" value="" />
<input type="hidden" name="hRedirectURL" value="" />
<input type="hidden" name="hDisplayNm" value="" /> 

<table border="0" class="DtTable950" cellspacing="0" cellpadding="0" >
<tr class="shade">
	<td>
		<table  border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr>
				<td  height="25" class="RightDashBoardHeader" ><fmtDOFileUpload:message key="LBL_UPLOAD_DO_DOCUMENT"/></td>
            	<td  height="25" class="RightDashBoardHeader">
                	<fmtDOFileUpload:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
            	</td>
			</tr>
		</table>
	</td>
</tr>
<logic:notEqual value="View" property="haction" name="frmDODocumentUpload">
<tr>
	<td>
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr>
				<td class="RightTableCaption" align="right" height="30" width="30%"><font color="red">*</font>&nbsp;<fmtDOFileUpload:message key="LBL_ORDER_ID"/>:</td>
				<td colspan="1" height="30" >&nbsp;<html:text property="strOrderId"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" onchange="fnValidateDO (this);" />&nbsp;
					<span id="DivShowDOIDUnAvail" style="vertical-align:middle; display: none;"><img height="20" width="20" title="DO ID unavailable" src="<%=strImagePath%>/delete.gif"></img></span>
					<span id="DivShowDOIDExists" style=" vertical-align:middle; display: none;"><img height="20" width="20" title="DO ID available" src="<%=strImagePath%>/success.gif"></img></span></td>
				<td align="left" width="40%"><fmtDOFileUpload:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" gmClass="Button" style="width: 5em; height: 22" buttonType="load" onClick="fnLoad();"/>
				</td>
			</tr>

		</table>
	</td>
	
</tr>

<tr>
	<td>
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr class="ShadeRightTableCaption">
				<td height="25" colspan="3" >&nbsp;<fmtDOFileUpload:message key="LBL_UPLOAD_FILES"/></td>
			</tr>

			<tr>
				<td>
					 <jsp:include page="/custservice/GmDODocMultiFileUploadInclude.jsp" />
				</td>
			</tr>
		</table>
	</td>
</tr>
</logic:notEqual>
<tr>
	<td>
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr class="ShadeRightTableCaption">
				<td height="25" colspan="3" >&nbsp;<fmtDOFileUpload:message key="LBL_FILES_UPLOADED"/></td>
			</tr>
			<tr>
				<td>
					<jsp:include  page="/custservice/GmDoDocFileUploaded.jsp"/>
				</td>
			</tr>
			<logic:equal value="View" property="haction" name="frmDODocumentUpload">
			<tr>
			<td align="center">
			<gmjsp:button value="Close" gmClass="Button" style="width: 5em; height: 22" buttonType="load" onClick="fnClose();"/>
			 </td>
			</tr>
			</logic:equal>
		</table>
	</td>
</tr>


<tr><td colspan="2" class="LLine" height="1"></td></tr>

</table>			
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>