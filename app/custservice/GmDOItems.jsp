
<%@page import="java.util.Date"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="fmtDOItems" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!--GmDOItems.jsp -->
<fmtDOItems:setLocale value="<%=strLocale%>"/>
<fmtDOItems:setBundle basename="properties.labels.custservice.GmLoanerRecon"/> 

    <%
    ArrayList alRepList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("REPLIST"));
	ArrayList alDistList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("DISTLIST"));
	ArrayList alAccList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ACCLIST"));
	ArrayList alStatus = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ORDSTATUS"));
	ArrayList alDateType = GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("DATETYPE"));
	HashMap hmparam = new HashMap();
	hmparam.put("DATETYPE", alDateType);
	String strOrderDate = GmCommonClass.parseNull((String)hmparam.get("ORDERDATE"));
	String strFromDt = GmCommonClass.parseNull((String)request.getAttribute("FROMDATE"));
	String strToDt = GmCommonClass.parseNull((String)request.getAttribute("TODATE"));
	String strLoanDt = GmCommonClass.parseNull((String)request.getAttribute("LOANDT"));
	
	String strApplDateFmt = strGCompDateFmt;
	Date dtFrmDate = GmCommonClass.getStringToDate(strFromDt,strApplDateFmt);
	Date dtToDate = GmCommonClass.getStringToDate(strToDt,strApplDateFmt);
	//The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();
	
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
	
	
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="STYLESHEET" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<title>Insert title here</title>
<script>
var companyInfoObj = '<%=strCompanyInfo%>';
var fromDt = '<%=strFromDt%>';
var LoanDt = '<%= strLoanDt %>';
</script>
</head>
<body>
<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tr><td colspan="8" height="24"  class="ShadeRightTableCaption"><a href="javascript:fnShowFilters('tabOrd');" tabindex="-1"><IMG id="tabOrdimg" border=0 src="<%=strImagePath%>/minus.gif"></a>&nbsp;<fmtDOItems:message key="LBL_ORDERS"/></td></tr>
<tr><td colspan="8">
<table width="100%" border="0" id="tabOrd" cellspacing="0" cellpadding="0">
<tr><td colspan="8" height="1" bgcolor="#cccccc"></td></tr>
<!-- Custom tag lib code modified for JBOSS migration changes -->
  <tr height="25">
    <td class="RightTableCaption" align="right"><fmtDOItems:message key="LBL_SALES_REP"/>:&nbsp;</td>
    <td> <gmjsp:dropdown controlName="Cbo_Rep" value="<%= alRepList%>" defaultValue="[Choose One]"  codeId="ID" codeName="NAME" width="250" tabIndex="1"></gmjsp:dropdown> </td>
    <td class="RightTableCaption" align="right" colspan="2"><fmtDOItems:message key="LBL_PART_NUMBER"/>:&nbsp;</td>
    <td colspan="2"> <input type="text" maxlength="20" name="Txt_pnum" id="Txt_pnum" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="2"> </td>
  </tr>
  <tr><td colspan="8" height="1" bgcolor="#cccccc"></td></tr>
  <tr height="25" class="shade">
    <td class="RightTableCaption" align="right"><fmtDOItems:message key="LBL_ACCOUNT"/>:&nbsp;</td>
    <td><gmjsp:dropdown controlName="Cbo_Acc" value="<%= alAccList%>" defaultValue="[Choose One]" codeId="ID" codeName="NAME" width="250" tabIndex="3"></gmjsp:dropdown></td>
    <td class="RightTableCaption" align="right" colspan="2"><fmtDOItems:message key="LBL_FIELD_SALES"/> :&nbsp;</td>
    <td colspan="2"><gmjsp:dropdown controlName="Cbo_Dist" value="<%= alDistList%>" defaultValue="[Choose One]"  codeId="ID" codeName="NAME" width="250" tabIndex="4"></gmjsp:dropdown></td>
  </tr>
  <tr><td colspan="8" height="1" bgcolor="#cccccc"></td></tr>
  
  <tr height="25">
  <td class="RightTableCaption" align="right"><fmtDOItems:message key="LBL_ORDER_ID"/>:&nbsp;</td>
    <td colspan="2"><input type="text" maxlength="20" name="Txt_ord" id="Txt_ord" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="8"> &nbsp;&nbsp;  
    </td>
    <td class="RightTableCaption" align="right"><fmtDOItems:message key="LBL_ORDER_STATUS"/>:&nbsp;</td>
    <td colspan="2"><gmjsp:dropdown controlName="Cbo_Status" controlId="Cbo_Status" value="<%= alStatus%>" defaultValue="[Choose One]" codeId="CODEID" codeName="CODENM" tabIndex="7"></gmjsp:dropdown></td>
  </tr>
   <tr><td colspan="8" height="1" bgcolor="#cccccc"></td></tr>
  <tr height="10" class="shade">
  </tr>
  
  
  <tr class="shade">
<td HEIGHT="50" class="RightTableCaption" align="Right"><fmtDOItems:message key="LBL_DATE_RANGE"/>:</td>  
     <td class="RightText" colspan="2">&nbsp;<fmtDOItems:message key="LBL_Type"/>:&nbsp;
   <gmjsp:dropdown controlName="Cbo_Type" controlId="Cbo_Type" value="<%= alDateType%>" defaultValue="<%=strOrderDate%>" codeId="CODEID" codeName="CODENM" tabIndex="5" onChange="fnChangeDate(this)"></gmjsp:dropdown>
   <BR><BR>&nbsp;<fmtDOItems:message  key="LBL_FROM"/>:&nbsp;
		       <gmjsp:calendar  textControlName="startDate" textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtFrmDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="5"/>
		
	          	&nbsp;<fmtDOItems:message key="LBL_TO"/>:
		       <gmjsp:calendar textControlName="endDate" textValue="<%=dtToDate==null?null:new java.sql.Date(dtToDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="6"/>
		   
		   	 &nbsp;&nbsp;</td>
			 	<td colspan="4">&nbsp;	
<fmtDOItems:message key="BTN_LOAD" var="varLoad"/>
<gmjsp:button value="${varLoad}" controlId="Btn_Load" style="width: 5em;height: 23" gmClass="Button" onClick="fnFilterLoad();" tabindex="9" buttonType="Load" /><BR>
  </td>
  </tr>
 <tr><td colspan="8" height="1" bgcolor="#cccccc"></td></tr>
<tr>
<td colspan="5">
<div id="dataGridDiv" style="height:200px" align="center">
<font color="blue"><fmtDOItems:message key="LBL_NOTHING_TO_DISPLAY"/></font>
</div>
</td>
</tr>
</table>
</td>
</tr>
	<tr><td colspan=8 height=1 class=Line></td></tr>
</table>
</body>
<%@ include file="/common/GmFooter.inc"%>
</html>