<%
/**********************************************************************************
 * File		 		: GmAssociatedReturn.jsp
 * Desc		 		: For Associated Return Report
 * Version	 		: 1.0
 * author			: Ritesh Shah
************************************************************************************/
%>
<!-- \custservice\GmAssociatedReturn.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<bean:define id="alDetail" name="frmAssociatedReturn" property="alDetail" type="java.util.ArrayList"></bean:define>
<%@ taglib prefix="fmtAssociatedRtn" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtAssociatedRtn:setLocale value="<%=strLocale%>"/>
<fmtAssociatedRtn:setBundle basename="properties.labels.custservice.GmAssociatedReturn"/>
<%
String strWikiTitle = GmCommonClass.getWikiTitle("MISSING_TAG_REPORT");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Associated Return Report</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath %>/customerservice/GmAssociatedReturn.js"></script>
</HEAD>

<BODY onkeyup="fnEnter();" leftmargin="20" topmargin="10" >
 
<html:form  action="/gmAsstRAReport.do"  >
<html:hidden property="strOpt" />
	<table border="0" class="DtTable850"  cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="4" height="25" class="RightDashBoardHeader">
				<fmtAssociatedRtn:message key="TD_ASSOCIATE_RTN_HEADER"/>
			</td>
			<td  height="25" class="RightDashBoardHeader">
			<fmtAssociatedRtn:message key="IMG_ALT_HELP" var="varHelp"/>
			<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr><td colspan="5" class="LLine" height="1"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td height="30"  class="RightTableCaption" align="right">&nbsp;<fmtAssociatedRtn:message key="LBL_INIT_RA"/>:&nbsp;</td>
			<td align="left">&nbsp;<html:text property="initRA"  size="15" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>
			<td height="30"  class="RightTableCaption" align="right">&nbsp;<fmtAssociatedRtn:message key="LBL_INIT_DIST"/>:&nbsp;</td>
			<td align="left">&nbsp;<gmjsp:dropdown controlName="initDist" SFFormName="frmAssociatedReturn" SFSeletedValue="initDist" 
							SFValue="alInitDistList" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]"  /></td>
			<td></td> 				
		</tr>
		
		 
		<tr><td colspan="5" class="LLine" height="1"></td></tr>
		<tr  class="shade" >
			<td height="30"  class="RightTableCaption" align="right">&nbsp;<fmtAssociatedRtn:message key="LBL_CREDIT_RA"/>:&nbsp;</td>
			<td align ="left">&nbsp;<html:text property="creditRA"  size="15" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>		
			<td height="30"  class="RightTableCaption" align="right">&nbsp;<fmtAssociatedRtn:message key="LBL_CREDIT_DIST"/>:&nbsp;</td>
			<td align ="left">&nbsp;<gmjsp:dropdown controlName="creditDist" SFFormName="frmAssociatedReturn" SFSeletedValue="creditDist" 
							SFValue="alCreditDistList" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]"  /></td>
			<fmtAssociatedRtn:message key="BTN_LOAD" var="varLoad"/>
			<td><gmjsp:button name = "loadBtn" value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;&nbsp;" gmClass="button" onClick="fnLoad();" buttonType="Load" /></td>				
		</tr>
		
		</table>
		<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
	    <tr>
			<td colspan="6" width="100%">
				<display:table name="requestScope.frmAssociatedReturn.alDetail"  freezeHeader="true" class="gmits" requestURI="/gmAsstRAReport.do" id="currentRowObject" 
				decorator="com.globus.operations.returns.displaytag.beans.DTAssociatedReturnWrapper">
				<fmtAssociatedRtn:message key="LBL_INIT_RA" var="varInitRA"/> 
 				<display:column property="INITRA" sortable="true" title="${varInitRA}" class="alignleft"/>
 				<fmtAssociatedRtn:message key="LBL_INIT_DIST" var="varInitDist"/>
 				<display:column property="INITDIST"  sortable="true" title="${varInitDist}" class="alignleft" />
 				<fmtAssociatedRtn:message key="LBL_CREDIT_RA" var="varCreditRA"/>
 				<display:column property="CREDITRA"  sortable="true" title="${varCreditRA}" class="alignleft" />
 				<fmtAssociatedRtn:message key="LBL_CREDIT_DIST" var="varCreditDist"/>
 				<display:column property="CREDITDIST"  sortable="true" title="${varCreditDist}" class="alignleft" />
 				<fmtAssociatedRtn:message key="DT_RELEATED_RA" var="varRelatedRA"/>
				<display:column property="OTHERRA" title="${varRelatedRA}"  sortable="true" style="width:100;" class="alignleft" />
				</display:table>
			</td>
		</tr>		
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>