 <%
/**********************************************************************************
 * File		 		: GmDoSummaryCtrlNum.jsp
 * Desc		 		: This screen is used to display Control Number
 * Version	 		: 1.0
 * author			: Jignesh Shah
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@page import="java.util.Date"%>
<%@ taglib prefix="fmtDoSummaryCtrlNum" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ page isELIgnored="false" %>
<!-- GmDoSummaryCtrlNum.jsp -->
<fmtDoSummaryCtrlNum:setLocale value="<%=strLocale%>"/>
<fmtDoSummaryCtrlNum:setBundle basename="properties.labels.custservice.GmDoSummaryCtrlNum"/>
<%
	String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	String strSessDeptId = GmCommonClass.parseNull((String)session.getAttribute("strSessDeptId"));
	String strFormName = GmCommonClass.parseNull(request.getParameter("FRMNAME"));
	String strAction = GmCommonClass.parseNull(request.getParameter("hAction"));
	String strScrToLoad = GmCommonClass.parseNull(request.getParameter("SCRTOLOAD"));
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strParantForm =  GmCommonClass.parseNull((String)request.getAttribute("hParantForm"));
	HashMap hmOrderDetails = new HashMap();
	ArrayList alControlNumDetails = new ArrayList();
	
	
	String strOrderHeader = "Order Details";
	String strStatus = "";
	String strDOType = "";
	String strOrdId = "";
	String strCaseID = "";
	String strAccNm = "";
	String strAccId = "";
	String strSurgeryDt="";
	Date dtSurgeryDate = null;
	String strDOTypeName = "";
	String strCustomerPO = "";
	String strCaseInfoID="";
	String strParentOrderID = "";
	String strPartNum = "";
	String strDesc = "";
	String strQty = "";
	String strControlNum = "";
	String strItemtype = "";
	String strPriceDisc = "";
	String strColor = "RightText";
	String strDesColor = "RightTextAS";
	String strDiscrepancyImage = "<img id=imgDiscrepancy src=".concat(strImagePath).concat("/30.gif title='Discrepancy' height=18 width=20>");
	String strPartNumInputStr = "";
	String strControlNumFL = "";
	boolean blControlNumDiscrepancy = false;
	int intControlSize = 0; 
	
	if(strParantForm.equals("CASE"))
		strOrderHeader = "Usage Details";
	
	if (hmReturn != null)
	{
		hmOrderDetails = (HashMap)hmReturn.get("ORDERDETAILS");
		if(strFormName.equals("frmOrder"))
		{
			alControlNumDetails =GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALCONTROLNUMDETAILS"));
		}
		else
		{
			alControlNumDetails =GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALFILTEREDCNUMDETAILS"));
		}
		log.debug("alControlNumDetails========="+alControlNumDetails);
		strStatus = GmCommonClass.parseNull((String)hmOrderDetails.get("STATUSFL")); 
		strDOType = GmCommonClass.parseZero((String)hmOrderDetails.get("ORDERTYPE"));
		strOrdId = GmCommonClass.parseNull((String)hmOrderDetails.get("ID"));
		strCaseID = GmCommonClass.parseNull((String)hmOrderDetails.get("CASE_ID"));
		strAccNm = GmCommonClass.parseNull((String)hmOrderDetails.get("ANAME"));
		strAccId = GmCommonClass.parseNull((String)hmOrderDetails.get("ACCID"));
		dtSurgeryDate = (java.sql.Date)hmOrderDetails.get("SDT");
		strSurgeryDt = GmCommonClass.getStringFromDate(dtSurgeryDate,strApplnDateFmt);
		strDOTypeName = GmCommonClass.parseNull((String)hmOrderDetails.get("ORDERTYPENAME"));
		strCustomerPO = GmCommonClass.parseNull((String)hmOrderDetails.get("PO"));
		strCaseInfoID = GmCommonClass.parseNull((String)hmOrderDetails.get("CASEINFO_ID"));
		strParentOrderID= GmCommonClass.parseNull((String)hmOrderDetails.get("PARENTORDID"));
		
		if(strStatus.equals("0")){
			strStatus = "Back Order";
		}else if(strStatus.equals("1")){
			strStatus = "Processing";
		}else if(strStatus.equals("2")){
			strStatus = "Shipped";
		}else if(strStatus.equals("7")){
			strStatus = "Pending Release";
		}else if(strStatus.equals("8")){
			strStatus = "Pending CS Confirmation";	
		} 
		
		intControlSize = alControlNumDetails.size();
		HashMap hmVal = new HashMap();
		for (int i=0;i<intControlSize;i++)
		{
			hmVal = (HashMap)alControlNumDetails.get(i);
			strControlNum = GmCommonClass.parseNull((String)hmVal.get("CNUM"));
			strControlNumFL = GmCommonClass.parseNull((String)hmVal.get("CNUMFL"));
			if(strControlNumFL.equals("Y") && strControlNum.equals("")){
		 		blControlNumDiscrepancy = true;
		 		break;
			}
		}
			
		hmVal = new HashMap();
		for (int i=0;i<intControlSize;i++){
			hmVal = (HashMap)alControlNumDetails.get(i);
			strControlNum = GmCommonClass.parseNull((String)hmVal.get("CNUM"));
			strPartNum = GmCommonClass.parseNull((String)hmVal.get("PNUM"));
			if(strControlNum.equals(""))
				strPartNumInputStr = strPartNumInputStr +strPartNum + ",";
		}
	}
	 
	int intSize = 0;
	int intCommentSize = 0;
	HashMap hcboVal = null;	
	HashMap hcmtVal = null;	
	
	if(!strParantForm.equals("CASE")){
		strDiscrepancyImage = "";
	}
	 
	String strColspan = "8";
	String strCollapseStyle = "display:block";
	String strCollapseImage = strImagePath+"/minus.gif";
	String strPriceDiscrepancy = "Please enter the control# before releasing the order";
	//To avoid js error while Account name contains apostrophe.			
    if(strAccNm.contains("'") == true){
		strAccNm = strAccNm.replaceAll("'", "&#39");
	}
    String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
    strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>

</style>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">
<script>
var strAction = '<%=strAction%>';
var ordId = '<%=strOrdId%>';
var caseId = '<%=strCaseID%>';
var accountNm = '<%=strAccNm%>';
var accountId = '<%=strAccId%>';
var surDt = '<%=strSurgeryDt%>';
var OrderType = '<%=strDOTypeName%>';
var customerPO = '<%=strCustomerPO%>';
var caseinfoID = '<%=strCaseInfoID%>';
var parentOrder = '<%=strParentOrderID%>';
var blControlNumDiscrepancy = '<%=blControlNumDiscrepancy%>';
var intControlSize ='<%=intControlSize%>';
var strSessDeptId = '<%=strSessDeptId%>';
var frmNm = '<%= strFormName%>';
var frmObj = eval("document."+frmNm);
var companyInfoObj = '<%=strCompanyInfo%>';

 function fnEditDetails(val){
	frmObj.action ='/gmDOSummaryEdit.do?method=edit'+val+'&FRMNAME='+frmNm+'&orderID='+ordId+'&caseID='+caseId+'&accountNm='+accountNm+'&accountID='+accountId+'&surgeryDt='+surDt+'&orderTypeNm='+OrderType+'&custPO='+customerPO+'&caseInfoID='+caseinfoID+'&parentOrderID='+parentOrder+'&hAction='+strAction+'&companyInfo='+companyInfoObj;
	frmObj.submit();
 }

 function fnShowFilters(val)
 {
 	var obj = eval("document.all."+val);
 	var obj1 = eval("document.all."+val+"img");
 	if (obj.style.display == 'none')
 	{
 		obj.style.display = 'block';
 		if(obj1){
 			obj1.src = '<%=strImagePath%>/minus.gif';
 		}
 	}
 	else
 	{
 		obj.style.display = 'none';	
 		if(obj1){
 			obj1.src = '<%=strImagePath%>/plus.gif';
 		}
 	}
 }
 
</script>
<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0">	
<% 
					strCollapseStyle = "display:block";
					strCollapseImage =  strImagePath+"/minus.gif";
					if(alControlNumDetails.size()==0){
						strCollapseStyle = "display:none";
						strCollapseImage =strImagePath+"/plus.gif";
					}
%>
	<tr class="ShadeRightTableCaption">
		<td height="22" colspan="1">&nbsp;<a href="javascript:fnShowFilters('tabControlNum');"  tabindex="-1" ><IMG id="tabControlNumimg" border=0 src="<%=strCollapseImage%>"></a>&nbsp;<fmtDoSummaryCtrlNum:message key="LBL_CONTROL_NUMBER_DETAILS"/>
			<%if((strSessDeptId.equals("S") && strStatus.equals("Pending Release") && blControlNumDiscrepancy)){%>
				<span style="color:red"><%=strPriceDiscrepancy%></span>	
			<%}%>
		</td>
		<%if(((strSessDeptId.equals("S") && strStatus.equals("Pending Release"))|| !strSessDeptId.equals("S")) && alControlNumDetails.size()>0){%>
		<td align="right">
		<!--  Below If/Else condition is to check the Pending Release -ipad pdf file view - PC-4960  -->
		<%if(strStatus.equals("Pending CS Confirmation")){ %> 
		<fmtDoSummaryCtrlNum:message key="IMG_ALT_OPEN_PDF" var="varOpnPdf"/>
		&nbsp;<img id="imgPDF" style= "cursor:hand;vertical-align:middle;" src="<%=strImagePath%>/pdf_icon.gif"
						onclick="fnExportFile('<%=strOrdId%>');" title='${varOpnPdf}'>&nbsp;
		<% } else { %>
					&nbsp;
		<% } %>
		<a href="javascript:fnEditDetails('ControlNumberDetails')"><fmtDoSummaryCtrlNum:message key="LBL_EDIT"/></a>&nbsp;</td>
		<%}else{ %>
		<td></td>
		<%}%>  
	</tr>
	<TR>
		<TD colspan="2">
			<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0" id="tabControlNum" style="<%=strCollapseStyle%>">
				<tr class="ShadeBlueBk" >
					<td width="80" height="20" align="center">&nbsp;<fmtDoSummaryCtrlNum:message key="LBL_PART"/><br><fmtDoSummaryCtrlNum:message key="LBL_NUMBER"/></td>
					<td width="1" class="Line"></td>
					<td  width="400"  style="width: 73%;">&nbsp;<fmtDoSummaryCtrlNum:message key="LBL_DESCRIPTION"/></td>
					<td width="1" class="Line"></td>
					<td align="center" width="40"><fmtDoSummaryCtrlNum:message key="LBL_QTY"/></td>
					<td width="1" class="Line"></td>
					<td align="center" width="120"><fmtDoSummaryCtrlNum:message key="LBL_CONTROL"/><br><fmtDoSummaryCtrlNum:message key="LBL_NUMBER"/></td>
				</tr>
				<tr><td bgcolor="#666666" height="1" colspan="11"></td></tr>
				<%
				if (alControlNumDetails != null)
				{
					intSize = alControlNumDetails.size();
				}
					hcboVal = new HashMap();
					String strControlNumFlg = ""; 
					if(intSize !=0){
					for (int i=0;i<intSize;i++)
					{
						hcboVal = (HashMap)alControlNumDetails.get(i);
						strPartNum = GmCommonClass.parseNull((String)hcboVal.get("PNUM"));
						strDesc = GmCommonClass.parseNull((String)hcboVal.get("PDESC"));
						strQty = GmCommonClass.parseNull((String)hcboVal.get("IQTY"));
						strControlNum = GmCommonClass.parseNull((String)hcboVal.get("CNUM"));
						strControlNumFlg = GmCommonClass.parseNull((String)hcboVal.get("CNUMFL"));
						strItemtype = GmCommonClass.parseNull((String)hcboVal.get("ITYPE"));
						if(strControlNumFlg.equals("Y") && strControlNum.equals("") && strParantForm.equals("CASE") && (strDOType.equals("2518")|| strDOType.equals("2519"))){
							strColor = "RightTextRed";
							strDesColor = "RightTextRed";
						}else {
							strColor = "RightText";
							strDesColor = "RightTextAs";
						}
				%>
				<tr class="<%=strColor%>">
					<td class="<%=strColor%>" height="20">&nbsp;<%=strPartNum%></td>
					<td width="1" bgcolor="#eeeeee"></td>
					<td class="<%=strDesColor%>">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
					<td width="1" bgcolor="#eeeeee"></td>
					<td class="<%=strColor%>" align="right">&nbsp;<%=strQty%></td>
					<td width="1" bgcolor="#eeeeee"></td>
					<td class="<%=strColor%>">&nbsp;<%=strControlNum%></td>
				</tr>
				<%} %>
				<%}else{ %>
				<tr>
					<td class="RightText" height="20" colspan="10">&nbsp;<fmtDoSummaryCtrlNum:message key="LBL_NEED_CONTROL_NUMBER"/>.</td>
				</tr>
				<%} %>
			</table>
		</td>
	</tr>
</table>