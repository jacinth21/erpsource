 <%
/**********************************************************************************
 * File		 		: GmPicSlipGlobal.jsp
 * Desc		 		: This screen is used for the
 * Version	 		: 1.0
 * author			: Xun 
************************************************************************************/
%>
<!-- \sales\AreaSet\GmAddInfo.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Date" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strApplnDateFmt = strGCompDateFmt;
	String strhAction = (String)request.getAttribute("hAction");
	
	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}
	strhAction = (strhAction == null)?"Load":strhAction;
	
	HashMap hmReturn = new HashMap();

	if (strhAction.equals("PopOrd") || strhAction.equals("OrdPlaced"))
	{
			hmReturn = (HashMap)request.getAttribute("hmReturn");
	}

	// String strPartNums = (String)session.getAttribute("strPartNumDupl");
	String strPartNums = (String)session.getAttribute("strPartNums");
	

	HashMap hmAcctDetails = new HashMap();

	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";

	ArrayList alRepList = new ArrayList();
	ArrayList alCartDetails = new ArrayList();
	ArrayList alOrderType = new ArrayList();
	
	String strAccId = "";
	String strAccNm = "";
	String strRepId = "";
	java.sql.Date dtCurrDate = null;
	String strPO = "";
	String strOrdId = "";
	String strShipAdd = "";
	String strUserName = "";
	String strPartNum = "";
	String strDesc = "";
	String strQty = "";
	String strItemOrdType = "";
	String strOrderType = "";
	String strParentId = "";
	String strLocation = "";
	String strRuleDomain = "";
	String strRuleLogo = "";
	//strOrderType = GmCommonClass.parseNull((String)request.getAttribute("STRORDTYPE"));
	String strCompId = "";
	String strCurrDate = "";
	
	String strDivisionId = "";
	String strCompanyLogo = "";
	String strPlantName = "";
	boolean blLoanerFl = false;
	if (hmReturn != null)
	{
		hmAcctDetails = (HashMap)hmReturn.get("ORDERDETAILS");
		//log.debug("Inside GmPicSlip Acc Detauks is :"  + hmAcctDetails);
		alCartDetails = (ArrayList)hmReturn.get("CARTDETAILS");
        //log.debug("Inside GmPicSlip Acc Detauks is :"  + alCartDetails);
		if (alCartDetails == null)
		{
			alCartDetails = new ArrayList();
		}
	}
	if (hmAcctDetails != null )
	{
		strShipAdd = (String)hmAcctDetails.get("SHIPADD");
		strOrdId = GmCommonClass.parseNull((String)hmAcctDetails.get("ID"));
		strPO = GmCommonClass.parseNull((String)hmAcctDetails.get("PO"));
		strAccId = GmCommonClass.parseNull((String)hmAcctDetails.get("ID"));
		strAccNm = GmCommonClass.parseNull((String)hmAcctDetails.get("BILLADD"));
		strRepId = GmCommonClass.parseNull((String)hmAcctDetails.get("REPID"));
		//dtCurrDate = (java.sql.Date)hmAcctDetails.get("ODT");
		strCurrDate = GmCommonClass.getStringFromDate((java.util.Date)hmAcctDetails.get("ODT"),strApplnDateFmt);
		strUserName = GmCommonClass.parseNull((String)hmAcctDetails.get("UNAME"));
		strOrderType = GmCommonClass.parseNull((String)hmAcctDetails.get("ORDERTYPE"));
		strParentId = GmCommonClass.parseNull((String)hmAcctDetails.get("PARENTORDID"));
		strCompId = GmCommonClass.parseNull((String)hmAcctDetails.get("COMPID"));
		strDivisionId = GmCommonClass.parseNull((String)hmAcctDetails.get("DIVISION_ID"));
		strPlantName = GmCommonClass.parseNull((String)hmAcctDetails.get("PLANTNAME"));
		strDivisionId = strDivisionId.equals("")?"2000":strDivisionId;
		HashMap hmCompanyAdd = new HashMap ();
		hmCompanyAdd = GmCommonClass.fetchCompanyAddress(gmDataStoreVO.getCmpid(), strDivisionId);
		strCompanyLogo = GmCommonClass.parseNull((String)hmCompanyAdd.get("LOGO"));
		strRuleDomain = GmCommonClass.parseNull((String)hmAcctDetails.get("RULEDOMAIN"));
		strRuleLogo = GmCommonClass.parseNull((String)hmAcctDetails.get("RULEIMAGE"));
	}
	
	/* if(!strDivisionId.equals("2006")){
		strCompanyLogo = strCompanyLogo + ".gif";
	}
 */	
	int intSize = 0;
	HashMap hcboVal = null;
	
	String strID = strOrdId + "$50180"; //50180 is the source that has to be passed for printing PicSlip barcode
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: PIC Slip </TITLE>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style> 
<script>
function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}

function fnLoadPick()
{
	document.frmOrder.action ="<%=strServletPath%>/GmOrderItemServlet";
	document.frmOrder.hAction.value = "PICSlip";
	document.frmOrder.submit();
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmOrder" method="post" action = "<%=strServletPath%>/GmOrderProcessServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hAccId" value="<%=strAccId%>">
<input type="hidden" name="hRepId" value="<%=strRepId%>">
<input type="hidden" name="hOrdId" value="<%=strOrdId%>">
<input type="hidden" name="hPartNums" value="<%=strPartNums%>">
<input type="hidden" name="hId" value="<%=strOrdId%>">

<table border="0" class="DtRuleTable700" cellspacing="0" cellpadding="0" >
<fmt:setLocale value="${requestScope.CompanyLocale}"/>
<fmt:setBundle basename="properties.Paperwork"/>
		<tr>
			<td align="center" height="30" id="button">
				<gmjsp:button value="&nbsp;Print&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				<gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" />
			</td>
		<tr>
	</table>
	<table border="0" width="700" cellspacing="0" cellpadding="0" class="DtTable700">
		<tr>
			<td bgcolor="#666666" colspan="3"></td>
		</tr>
		
		<tr>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<%if(strRuleDomain.equals("")){%>
							<td height="80" width="170"><img src="<%=strImagePath%>/<%=strCompanyLogo%>.gif" width="138" height="60"></td>
						<%}else{ %>
							<td height="80" width="170"><img src="<%=strImagePath%>/<%=strRuleLogo%>" width="138" height="60"></td>
						<%} %>
						<!-- <td align="left" class="RightText"><font size="+3">PIC Slip</font>&nbsp;</td> -->
						
			<td  align="left" class="LeftText"><font size="+3">
			<fmt:message key="PICK_SLIP.TITLE"/>			
			</font>&nbsp;</td>
			<td><img align="right" src='/GmCommonBarCodeServlet?ID=<%=strID%>' height="60" width="220" />&nbsp;&nbsp;</td>
		</tr>
						
					<tr>						
						<td colspan="2"><img align="left" src='/GmCommonBarCodeServlet?ID=<%=strOrdId%>' height="60" width="120" />&nbsp;&nbsp;</td>
						<td align="right"><img align="right" src='/GmCommonBarCodeServlet?ID=<%=strID%>&type=2d' height="50" width="50" />&nbsp;&nbsp;</td>							
					</tr>
				</table>
			</td> 
			<td bgcolor="#666666" width="1" rowspan="4"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="7"></td></tr>
		<tr>
			<td width="100%" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr bgcolor="#eeeeee" class="RightTableCaption">
									<td height="23" align="center" width="220">&nbsp;<fmt:message key="PICK_SLIP.BILLTO"/>	</td>
									<td bgcolor="#666666" width="1" rowspan="7"></td>
									<td width="210" align="center">&nbsp;<fmt:message key="PICK_SLIP.SHIPTO"/>	</td>
									<td bgcolor="#666666" width="1" rowspan="7"></td>
									<td width="100" align="center">&nbsp;<fmt:message key="PICK_SLIP.CUSTPO"/></td>
									<td width="110" align="center">&nbsp;<fmt:message key="PICK_SLIP.DOID"/></td>
									<td width="80" align="center">&nbsp;<fmt:message key="PICK_SLIP.ORDERDATE"/></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="7"></td>
								</tr>
								<tr>
									<td class="RightText" rowspan="5" valign="top">&nbsp;<%=strAccNm%>
									</td>
									<td rowspan="5" class="RightText" valign="top"><%=strShipAdd%></td>
									<td class="RightText" align="center"><%=strPO%></td>
									<td class="RightText" align="center"><b><%=strOrdId%></b></td>
									<td height="25" class="RightText" align="center">&nbsp;<%=strCurrDate%></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="3"></td>
								</tr>
								<tr bgcolor="#eeeeee" class="RightTableCaption">
									<td align="center">Parent DO Id</td>
									<td colspan="1" align="center">Order By</td>
									<td align="center">Plant</td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="3"></td>
								</tr>
								<tr>
									<td align="center" height="25" class="RightText">&nbsp;<%=strParentId%></td>
									<td colspan="1" align="center" class="RightText">&nbsp;<%=strUserName%><Br></td>
									<td align="center" class="RightText">&nbsp;<%=strPlantName%><Br></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="7"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align="center" colspan="2" valign="top" height="400">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr bgcolor="#eeeeee" class="RightTableCaption">
									<td width="100" height="21">&nbsp;<fmt:message key="PICK_SLIP.PARTNUMBER"/></td>
									<td width="200">&nbsp;<fmt:message key="PICK_SLIP.PARTDESC"/></td>
									<td align="center" width="100"><fmt:message key="PICK_SLIP.QTY"/></td>
									<td align="center" width="100">Location</td>
								</tr>
								<tr>
									<td colspan="4" height="1" bgcolor="#666666"></td>
								</tr>
<%
			  		intSize =alCartDetails.size();
					hcboVal = new HashMap();

			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alCartDetails.get(i);
			  			strPartNum = GmCommonClass.parseNull((String)hcboVal.get("ID"));
						strDesc = GmCommonClass.parseNull((String)hcboVal.get("PDESC"));
						strQty = GmCommonClass.parseNull((String)hcboVal.get("QTY"));
						strItemOrdType = GmCommonClass.parseNull((String)hcboVal.get("ITEMTYPE"));
						strLocation = GmCommonClass.parseNull((String)hcboVal.get("SUGLOCATIONCD"));
						if (strItemOrdType.equals("50300"))
						{
%>
								<tr>
									<td class="RightText" height="20">&nbsp;<%=strPartNum%></td>
									<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
									<td class="RightText" align="center">&nbsp;<%=strQty%></td>
									<td  class="RightText">&nbsp;<%=strLocation%></td>
								</tr>
								<tr><td colspan="4" height="1" bgcolor="#eeeeee"></td></tr>
<%
						}else
						{
							blLoanerFl = true;
						}
					}
%>	
							</table>
						</td>
					</tr>
					<tr><td colspan="2" height="1"></td></tr>
<%
					if (blLoanerFl)
					{
%>
					<tr>
						<td class="RightText" colspan="2">Note: Not all parts in the Order are displayed as there are Some parts that were from Loaner Sets.<BR>
						To view the entire order, please see the order summary screen.
						</td>
					</tr>
<%
					}
%>
					<tr>
						<td bgcolor="#eeeeee" colspan="2">
							<jsp:include page="/common/GmRuleDisplayInclude.jsp">
							<jsp:param name="Show" value="false" />
							<jsp:param name="Fonts" value="false" />
							</jsp:include>
						</td>
					</tr>	
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>	
					<tr valign="top" height="60">
						<td colspan="2">
							<jsp:include page="/common/GmIncludeLog.jsp" >
								<jsp:param name="LogType" value="" />
								<jsp:param name="LogMode" value="View" />
							</jsp:include>
						</td>
					</tr>	
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>					
					<tr><td class="RightText" colspan="2"><u>Please Initial here:</u></td></tr>					
					<tr><td colspan="2" height="50">&nbsp;</td></tr>
					<tr>
						<td class="RightText">&nbsp;&nbsp;&nbsp;Picked By</td>
						<td class="RightText" align="right">Verified By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>									
				</table>
			</td>
		</tr>		
    </table>
	<table border="0" class="DtRuleTable700" cellspacing="0" cellpadding="0">
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
		<tr>
			<td>
					<jsp:include page="/common/GmIncludeDateStamp.jsp" />
			</td>	
		</tr>
	</table>
</FORM>
<% 
	/* This code is to display bookmark value 
	* 2530 maps to Bill Only-Loaner */
	if (strOrderType.equals("2530"))
	{
%>
	<div id="waterMark" style="position:absolute; z-Index: 0; top: 240; left: 220">
		<img src="<%=strImagePath%>/loaner-picslip.gif"  border=0>
	</div>
<% }%>

<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>