<!--  \CS Label changes\custservice\GmBackOrderPart.jsp-->

 <%
/**********************************************************************************
 * File		 		: GmBackOrderPart.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ taglib prefix="fmtBackOrderPart" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtBackOrderPart:setLocale value="<%=strLocale%>"/>
<fmtBackOrderPart:setBundle basename="properties.labels.custservice.GmBackOrderPart"/>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	
	String strhAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: BO Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">	
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />

</HEAD>

<BODY leftmargin="15" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmOrderItemServlet">
<input type="hidden" name="hAction" value="PartBO">
	<table class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtBackOrderPart:message key="LBL_BACKORDER_REPORT_BY_PART_NUMBER"/></td>
		</tr>
		<tr><td class="Line" height="1"></td></tr>
		<tr>
			<td align="center">
				<display:table name="requestScope.BOREPORT.rows" export="true" class="its" id="currentRowObject">
                 <display:setProperty name="export.excel.filename" value="Back Orders.xls" />   
					<fmtBackOrderPart:message key="LBL_PARTNUMBER" var="varPartNumber"/>
					<display:column property="PNUM" title="${varPartNumber}" class="alignleft" group="1" />
					<fmtBackOrderPart:message key="LBL_DESCRIPTION" var="varDescription"/>
					<display:column property="PDESC" title="${varDescription}" class="alignleft" group="1"  sortable="true" />
					<fmtBackOrderPart:message key="LBL_SALES" var="varSales"/>
					<display:column property="SALESCOUNT" title="${varSales}" class="aligncenter" group="1" sortable="true" />
					<fmtBackOrderPart:message key="LBL_ITEMS_FROM_SHIPPED_SETS" var="varItemsfromShippedSets"/>
					<display:column property="SETITEMCOUNT" title="${varItemsfromShippedSets} <BR>" class="aligncenter" sortable="true"/>
					<fmtBackOrderPart:message key="LBL_ITEMS" var="varItems"/>
					<display:column property="ITEMCOUNT" title="Items" class="aligncenter" sortable="true" />
					<fmtBackOrderPart:message key="LBL_ITEMS_FOR_WIP_SETS" var="varItemsforWIPSets"/>
					<display:column property="SETCOUNT" title="${varItemsforWIPSets}<BR>" class="aligncenter" sortable="true"/>
					<fmtBackOrderPart:message key="LBL_TOTAL" var="varTotal"/>
					<display:column property="TOTALCOUNT" title="${varTotal}" class="aligncenter" sortable="true"/>
				</display:table>
			</TD>
		</tr>
    </table>

</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
