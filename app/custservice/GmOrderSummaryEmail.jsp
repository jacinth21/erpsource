f
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtOrderSummaryEmail" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmOrderSummaryEmail.jsp -->
<fmtOrderSummaryEmail:setLocale value="<%=strLocale%>"/>
<fmtOrderSummaryEmail:setBundle basename="properties.labels.custservice.GmOrderSummaryEmail"/>
<%
String strCustServiceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");

%>

<bean:define id="orderID" name="frmDODashboard" property="orderID" type="java.lang.String"></bean:define>
<bean:define id="caseID" name="frmDODashboard" property="caseID" type="java.lang.String"></bean:define>
<bean:define id="accountID" name="frmDODashboard" property="accountID" type="java.lang.String"></bean:define>
<bean:define id="accountNm" name="frmDODashboard" property="accountNm" type="java.lang.String"></bean:define>
<bean:define id="to" name="frmDODashboard" property="to" type="java.lang.String"></bean:define>
<bean:define id="haction" name="frmDODashboard" property="haction" type="java.lang.String"></bean:define>
<HTML>
<HEAD>
<TITLE>Globus Medical: Order Summary Email Information</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strCustServiceJsPath%>/GmDOProcess.js"></script>
</HEAD>
<BODY leftmargin="20" topmargin="10" >
<html:form  action="/gmDOReport.do?method=emailOrderSummary" onsubmit="javascript:return false;">
<input type="hidden" name ="orderID" value="<%=orderID%>" />
<input type="hidden" name ="hOrderID" value="<%=orderID%>" />
<input type="hidden" name ="caseID" value="<%=caseID%>" />
<input type="hidden" name ="accountID" value="<%=accountID%>" />
<input type="hidden" name ="accountNm" value="<%=accountNm%>" />
<input type="hidden" name ="to" value="<%=to%>" />
<html:hidden  property ="haction" value="<%=haction%>" />
<table class="DtTable800" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" colspan="3" align="left" class="RightDashBoardHeader">&nbsp;<fmtOrderSummaryEmail:message key="LBL_ORDER_SUMMARY_EMAIL"/></td>
				<td align="right" class=RightDashBoardHeader>
				<fmtOrderSummaryEmail:message key="LBL_HELP" var="varHelp"/>
					<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("ORDER_SUMMARY_EMAIL")%>');" />
				</td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="4"></td>
			</tr>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<tr>
			<td class="RightTableCaption" height="30"  align="right">
			<fmtOrderSummaryEmail:message key="LBL_TO" var="varTo"/>
			<gmjsp:label type="BoldText" SFLblControlName="${varTo}:" td="false" /></td>
			<td colspan="3">&nbsp; <bean:write name="frmDODashboard" property="to" />
			<%-- <html:text property="to" size="80" onfocus="changeBgColor(this,'#AACCE8');" 
							name="frmDODashboard" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"  onchange="" size="80"/>--%>&nbsp;
			</td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="4"></td>
			</tr>
			<tr class="Shade">
			<td class="RightTableCaption" height="30"  align="right">
			<fmtOrderSummaryEmail:message key="LBL_CC" var="varCc"/>
			<gmjsp:label type="BoldText" SFLblControlName="${varCc}:" td="false" /></td>
			<td colspan="3">&nbsp; <html:text property="cc" size="80" onfocus="changeBgColor(this,'#AACCE8');" 
							name="frmDODashboard" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>&nbsp;
			</td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="4"></td>
			</tr>
			<tr>
			<td class="RightTableCaption" height="20" align="right">
			<fmtOrderSummaryEmail:message key="LBL_SUBJECT" var="varSubject"/>
			<gmjsp:label type="BoldText" SFLblControlName="${varSubject}:" td="false" /></td>
			<td>&nbsp;<fmtOrderSummaryEmail:message key="LBL_DELIVERY_ORDER_SUMMARY_FOR"/> <bean:write name="frmDODashboard" property="orderID" /></td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="4"></td>
			</tr>
			<tr class="Shade">
			<td colspan="4"> &nbsp;
				<table width=800 border="0" cellspacing="0" cellpadding="0">
				<tr><td class="RightTableCaption" align="right" colspan="1" width=18%>
				<fmtOrderSummaryEmail:message key="LBL_MESSAGE" var="VarMessage"/>
				<gmjsp:label type="BoldText" SFLblControlName="${VarMessage}:" td="false" /></td>
				<td class="RightTableCaption" align="left" colspan="2">&nbsp;<fmtOrderSummaryEmail:message key="LBL_DO_SUMMARY_GENERATED_FOR"/></td>
				<td class="RightTableCaption" colspan="1">&nbsp;</td>
				</tr>
				<tr><td class="RightTableCaption" colspan="1">&nbsp;</td>
				<td class="RightTableCaption" align="right" colspan="1"  width=15%><fmtOrderSummaryEmail:message key="LBL_DO"/>#:</td>
				<td colspan="2">&nbsp; <bean:write name="frmDODashboard" property="orderID" /></td>
				</tr>
				<tr><td class="RightTableCaption" colspan="1">&nbsp;</td>
				<td class="RightTableCaption" align="right" colspan="1"><fmtOrderSummaryEmail:message key="LBL_CASE_ID"/>:</td>
				<td colspan="2">&nbsp; <bean:write name="frmDODashboard" property="caseID" /></td>
				</tr>
				<tr><td class="RightTableCaption" colspan="1">&nbsp;</td>
				<td class="RightTableCaption" align="right" colspan="1"><fmtOrderSummaryEmail:message key="LBL_ACCOUNT_ID"/>:</td>
				<td colspan="2">&nbsp; <bean:write name="frmDODashboard" property="accountID" /></td>
				</tr>
				<tr><td class="RightTableCaption" colspan="1">&nbsp;</td>
				<td class="RightTableCaption" align="right" colspan="1"><fmtOrderSummaryEmail:message key="LBL_ACCOUNT_NAME"/>:</td>
				<td colspan="2">&nbsp; <bean:write name="frmDODashboard" property="accountNm" /></td>
				</tr>
				</table>
			</td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="4"></td>
			</tr>
			<tr>
			<td class="RightTableCaption" height="100" align="right">
			<fmtOrderSummaryEmail:message key="LBL_ADD_MORE_MESSAGE" var="varAddMoreMEssage"/>
			<gmjsp:label type="BoldText" SFLblControlName="${varAddMoreMEssage}:" td="false" /></td>
			<td colspan="3"> &nbsp;<html:textarea rows="12" cols="80" name="frmDODashboard" property="addMoreMessage" styleClass="InputArea" 
			onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /><br></td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="4"></td>
			</tr>
			
			<tr>
		     <td  class="aligncenter" align="center" height="30" colspan="4">
		     <fmtOrderSummaryEmail:message key="BTN_SEND_EMAIL" var="varSendEmail"/>
		     <fmtOrderSummaryEmail:message key="BTN_CLOSE" var="varClose"/>
			<gmjsp:button value="${varSendEmail}" buttonType="Save" gmClass="button"  onClick="fnSendEmailDO();"/>&nbsp;
			<gmjsp:button value="${varClose}" buttonType="Load" gmClass="button" onClick="fnClose();"/>&nbsp;	
			</td>
			</tr>				
	</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
	</BODY>
	</HTML>
