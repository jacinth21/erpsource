
<%@page import="java.util.Iterator"%>
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
	/**********************************************************************************
	 * File		 		: GmPackSlip.jsp
	 * Desc		 		: This screen is used for the Packing Slip to japan
	 * Version	 		: 1.0
	 * author			: Mahavishnu
	 ************************************************************************************/
%>
<!-- \custservice\GmPackSlipJapan.jsp-->
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Date" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<%@ page import="com.globus.common.beans.GmJasperReport"%>

<%
	String strHtmlJasperRpt = "";
	String strAction = null;
	String strSource = "";
	String strApplnDateFmt = strGCompDateFmt;


	strAction = GmCommonClass.parseNull((String) request
			.getAttribute("hAction"));
	strAction = (strAction.trim() == null) ? "PrintPack" : strAction;
	strAction = (strAction.equals("")) ? "PrintPack" : strAction;
	String strOpt = GmCommonClass.parseNull(request
			.getParameter("hOpt"));
	strSource = GmCommonClass.parseNull((String) request	
			.getAttribute("SOURCE"));
	String strGUID = GmCommonClass.parseNull(request
			.getParameter("hGUId"));

	HashMap hmReturn = new HashMap();
	HashMap hmAllData = new HashMap();
	HashMap hmConsignDetails = new HashMap();
	hmReturn = (HashMap) request.getAttribute("hmReturn");

	HashMap hmOrderDetails = new HashMap();
	HashMap hmShipDetails = new HashMap();
	HashMap hmSetDetails = new HashMap();

	ArrayList alCartDetails = new ArrayList();
	ArrayList alPopulatedCartDtls = new ArrayList();
	ArrayList alBackOrderDetails = new ArrayList();
        ArrayList alResult = new ArrayList();

	String strAccNm = "";
	String strDistRepNm = "";
	String strOrdId = "";

	String strShipDate = "";
	String strTrack = "";
	String strCompId = "";
	String strDivisionId = "";
	String strShowPhoneNo = "";
	String strShowGMNAAddress = "";
	String strCompanyId = "";
	String strPlantId = "";
	String strRepNm="";
	String strWareHouse="";
	String strStatus="";
	String strComments="";
	String strJasperName="";
	String strExpDate="";
	String strSetId="";
	String strSetName="";
	String strBarCodeId="";
	String strConsId="";
	String strTagId="";
	String strPackslipJasper = "";
	String strSetIdName = "";
	String strCreatedBy = "";
	String strAccountName = "";
	String strBarCodeFl = "";
	String strShipToName = "";
	String strRaComments= "";
	String strEtchId = "";

	String surgDate = "";
	Date strShippingDate = null;
	String strLoanerReqId ="";
	strPlantId = gmDataStoreVO.getPlantid();
	strCompanyId = GmCommonClass.parseNull(GmCommonClass.getRuleValue(
			strPlantId, "PLANT_PACK_SLIP")); //Rulevalue:= 1010 (or) 1017
	strBarCodeFl =GmCommonClass.parseNull((String)request.getAttribute("STRBARCODEFL"));
	
	if (strCompanyId.equals("")) {
		strCompanyId = gmDataStoreVO.getCmpid();
	}
	String strCompanyLocale = GmCommonClass
			.getCompanyLocale(strCompanyId);
	GmResourceBundleBean gmCmpResourceBundleBean =GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	String strLocaleCompany = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBeanlbl = 
		    GmCommonClass.getResourceBundleBean("properties.labels.custservice.Shipping.GmPackSlip", strLocaleCompany);
	
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Paperwork",strCompanyLocale);
	strShowPhoneNo = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.SHOW_PHONE_NO"));
	strShowGMNAAddress = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.SHOW_GMNA_ADDRESS"));
	//PMT-53539 japan-new-warehouse-address
	String strSetRbObjectFl =
	    GmCommonClass.parseNull(gmResourceBundleBean.getProperty("ADD_RESOURCE_BUNDLE_OBJ")); 
	//PMT-53539 japan-new-warehouse-address

	boolean blBackFl = false;
	
	if (hmReturn != null) {
		hmOrderDetails = (HashMap) hmReturn.get("ORDERDETAILS");
		alBackOrderDetails = (ArrayList) hmReturn.get("BACKORDER");
		hmShipDetails = (HashMap) hmReturn.get("SHIPDETAILS");
		if(strBarCodeFl.equals("")){
		hmSetDetails = (HashMap) hmReturn.get("SETDETAILS");
		}else{
		  hmSetDetails = (HashMap) request.getAttribute("hmReturn");
		}
		hmConsignDetails = (HashMap)hmReturn.get("CONDETAILS");
		surgDate =(String)hmReturn.get("STRSURGERYDT");
		if(hmOrderDetails!=null){
		  strJasperName="\\invoice\\GmJapanPackSlip.jasper";
		  strPackslipJasper = "\\invoice\\GmJPOrderPackSlipPrint.jasper";
		 // alCartDetails = (ArrayList) hmReturn.get("CARTDETAILS");
		  alCartDetails =GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("alCartDetails"));
		  if(alCartDetails.isEmpty())
		  {
		    alCartDetails = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("CARTDETAILS"));
		  }
		 //To show sequence number in each line item even when lot number is same and ends with number equals to quantity .
		  if(alCartDetails.size()!=0){
		  Iterator itrMap = alCartDetails.iterator();
			 while (itrMap.hasNext()) {
	           HashMap hmResult = (HashMap) itrMap.next();
	           int strQty = Integer.parseInt(GmCommonClass.parseNull((String) hmResult.get("QTY")));
	         for(int i=1;i<=strQty;i++){
	           alResult.add(hmResult);
	           }
			 }
		  }
		  strOrdId = GmCommonClass.parseNull((String) hmOrderDetails
				.get("ID"));
		strAccNm = GmCommonClass.parseNull((String) hmOrderDetails
				.get("ANAME"));
		strDistRepNm = GmCommonClass.parseNull((String) hmOrderDetails
				.get("DEALERNM"));
		strRepNm = GmCommonClass.parseNull((String) hmOrderDetails
				.get("REPNM"));
		strWareHouse=GmCommonClass.parseNull((String) hmOrderDetails
				.get("WAR_HOUSE"));
		strStatus=GmCommonClass.parseNull((String) hmOrderDetails
				.get("STATUSFL"));
		strComments=GmCommonClass.parseNull((String) hmOrderDetails
						.get("LOGCOMMENTS"));
		strCreatedBy = GmCommonClass.parseNull((String) hmOrderDetails
			.get("UNAME"));
		
		strShippingDate =(java.util.Date)hmOrderDetails.get("SHIPPING_DT");
		strShipToName = GmCommonClass.parseNull((String) hmOrderDetails.get("SHIPTONAME"));
		
		strCompId = GmCommonClass.parseNull((String) hmOrderDetails
				.get("COMPID"));
		strDivisionId = GmCommonClass.parseNull((String) hmOrderDetails
				.get("DIVISION_ID"));
		strDivisionId = strDivisionId.equals("")
				? "2000"
				: strDivisionId;
		strBarCodeId= GmCommonClass.parseNull((String) hmOrderDetails
			.get("ID"));
		}
		
		if(hmSetDetails!=null){
		  strJasperName="\\invoice\\GmJapanConsPackSlip.jasper";
		  strPackslipJasper = "\\invoice\\GmJPConsignPackSlipPrint.jasper";
		  //alCartDetails = (ArrayList) hmReturn.get("SETLOAD");
		  alCartDetails =    GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alPopulatedCartDtls"));

		  if(alCartDetails.size()!=0){
		  Iterator itrMap = alCartDetails.iterator();
			 while (itrMap.hasNext()) {
	           HashMap hmResult = (HashMap) itrMap.next();
	           int strQty = Integer.parseInt(GmCommonClass.parseNull((String) hmResult.get("IQTY")));
	         for(int i=0;i<strQty;i++){
	           alResult.add(hmResult);
	           }
			 }
		  }
	        strRaComments = GmCommonClass.parseNull((String)hmReturn.get("RACOMMENTS"));
		  strConsId = GmCommonClass.parseNull((String) hmSetDetails
				.get("CID"));
		strAccNm = GmCommonClass.parseNull((String) hmSetDetails
				.get("ACCNM"));
		strRepNm = GmCommonClass.parseNull((String) hmSetDetails
				.get("DISTNM"));
		strWareHouse=GmCommonClass.parseNull((String) hmSetDetails
				.get("WAR_HOUSE"));
		strComments = GmCommonClass.parseNull((String) hmSetDetails.get("LOGCOMMENTS"));
		strStatus=GmCommonClass.parseNull((String) hmSetDetails
				.get("STATUSVALUE"));
		strSetId=GmCommonClass.parseNull((String) hmSetDetails
			.get("SETID"));
		strSetName=GmCommonClass.parseNull((String) hmSetDetails
			.get("SETNAME"));
		strTagId=GmCommonClass.parseNull((String) hmSetDetails
			.get("TAGID"));
		strShippingDate =   (java.util.Date)hmSetDetails.get("SDATE");
		strCreatedBy = GmCommonClass.parseNull((String) hmSetDetails
			.get("CREATEDBY"));
		strShipToName = GmCommonClass.parseNull((String) hmSetDetails.get("SHIPTONAME"));
		surgDate = GmCommonClass.getStringFromDate((java.util.Date) hmSetDetails.get("REQ_DATE"),"yyyy/MM/dd");
		strLoanerReqId = GmCommonClass.parseNull((String) hmSetDetails.get("LOANERREQID")); // PMT-32450 - to add Loaner Req ID column in Consignment paperwork
		if(strSetId.equals("")){
		  strBarCodeId =GmCommonClass.parseNull((String) hmSetDetails
				.get("CID"));
		}else{
		  strBarCodeId =GmCommonClass.parseNull((String) hmSetDetails
				.get("SETID"));
		}
		}
		
		if(hmConsignDetails!=null){
		  strJasperName="\\invoice\\GmJapanFGLEPackSlip.jasper";
		  strPackslipJasper = "\\invoice\\GmJPFGLEPackSlipPrint.jasper";
		  
		   alCartDetails = (ArrayList) hmReturn.get("SETLOAD");
		   if(alCartDetails.size()!=0){
		   Iterator itrMap = alCartDetails.iterator();
			 while (itrMap.hasNext()) {
	           HashMap hmResult = (HashMap) itrMap.next();
	           int strQty = Integer.parseInt(GmCommonClass.parseNull((String) hmResult.get("IQTY")));
	         for(int i=0;i<strQty;i++){
	           alResult.add(hmResult);
	           }
			 }
		   }
		    strConsId = (String)hmConsignDetails.get("CID");
			strSetId = GmCommonClass.parseNull((String)hmConsignDetails.get("SETID"));
			strBarCodeId=GmCommonClass.parseNull((String)hmConsignDetails.get("SETID"));
			strSetName= GmCommonClass.parseNull((String)hmConsignDetails.get("SETNM"));
			strSetName = strSetName.equals("")? GmCommonClass.parseNull((String)hmConsignDetails.get("SNAME")):strSetName;
			strAccNm = GmCommonClass.parseNull((String)hmConsignDetails.get("ANAME"));
			strDistRepNm = GmCommonClass.parseNull((String)hmConsignDetails.get("DISTNAME"));
			strRepNm=GmCommonClass.parseNull((String)hmConsignDetails.get("REPNM"));
			strAccNm = strAccNm.equals("")?strDistRepNm:strAccNm;
			strComments =  GmCommonClass.parseNull((String)hmConsignDetails.get("LOGCOMMENTS"));
			strStatus = GmCommonClass.parseNull((String)hmConsignDetails.get("STATUSVALUE"));
			strTagId=GmCommonClass.parseNull((String) hmConsignDetails.get("TAGID"));
			strShippingDate =   (java.util.Date)hmConsignDetails.get("SHIPDT");
			strCreatedBy = GmCommonClass.parseNull((String) hmConsignDetails.get("UNAME"));
			// the Account Name associated to the Loaner Set
			strAccountName = GmCommonClass.parseNull((String) hmConsignDetails.get("ACCNAME"));
			surgDate = GmCommonClass.getStringFromDate((java.util.Date) hmConsignDetails.get("SURGERYDATE"),"yyyy/MM/dd");
			strShipToName = GmCommonClass.parseNull((String) hmConsignDetails.get("SHIPTONAME"));
			strEtchId = GmCommonClass.parseNull((String) hmConsignDetails.get("ETCHID"));
		}
		
		
		
		
	}
	if (hmShipDetails != null) {
		strShipDate = GmCommonClass.getStringFromDate(
				(java.util.Date) hmShipDetails.get("SDT"),
				strApplnDateFmt);
	}
					strSetIdName = strSetId+" "+strSetName;
					//FLR_ADDRESS,3FLR_PHONE,3FLR_FAX only for loaner and consignment(loan detail sheet)
					hmAllData.put("3FLRADDRESS",GmCommonClass.parseNull((String)gmCmpResourceBundleBean.getProperty("3FLR_ADDRESS")));
					hmAllData.put("3FLRPHONE",GmCommonClass.parseNull((String)gmCmpResourceBundleBean.getProperty("3FLR_PHONE")));
					hmAllData.put("3FLRFAX",GmCommonClass.parseNull((String)gmCmpResourceBundleBean.getProperty("3FLR_FAX")));
					
				    hmAllData.put("GMCOMPANYNAME",GmCommonClass.parseNull((String)gmCmpResourceBundleBean.getProperty("GMCOMPANYNAME")));
					hmAllData.put("GMCOMPANYADDRESS",GmCommonClass.parseNull((String)gmCmpResourceBundleBean.getProperty("GMCOMPANYADDRESS")));
					hmAllData.put("GMCOMPANYPHONE",GmCommonClass.parseNull((String)gmCmpResourceBundleBean.getProperty("GMCOMPANYPHONE")));
					hmAllData.put("GMCOMPANYFAX",GmCommonClass.parseNull((String)gmCmpResourceBundleBean.getProperty("GMCOMPANYFAX")));
			  		hmAllData.put("ANAME",strAccNm);		
					hmAllData.put("REPDISTNM",strDistRepNm);	
					hmAllData.put("REPNM",strRepNm);
					hmAllData.put("CREATEDBY",strCreatedBy);
					hmAllData.put("SHIPTONAME",strShipToName);
					hmAllData.put("SETIDNAME",strSetIdName);
					hmAllData.put("SHIP_DATE",strShippingDate);
					hmAllData.put("ACCOUNTNAME",strAccountName);
					hmAllData.put("WAR_HOUSE","FG Warehouse");
					hmAllData.put("SET_ID",strSetId);
					hmAllData.put("SET_NAME",strSetName);
					hmAllData.put("SETIDNAME",strSetIdName);
					hmAllData.put("BAR_CODE_ID",strBarCodeId);
					hmAllData.put("SURG_DATE",surgDate);
					hmAllData.put("STATUS_FLG",strStatus);
					hmAllData.put("COMMENTS",strComments);
					hmAllData.put("CONS_ID",strConsId);
					hmAllData.put("ORDER_ID",strOrdId);
					hmAllData.put("TAG_ID",strTagId);
					hmAllData.put("ETCH_ID",strEtchId);
					hmAllData.put("RA_COMMENTS",strRaComments);
					hmAllData.put("LBL_HEADER",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_HEADER")));
					hmAllData.put("LBL_LBOX_NUM",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_LBOX_NUM")));
					hmAllData.put("LBL_LBOX_NAME",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_LBOX_NAME")));
					hmAllData.put("LBL_PRINT_DATE",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PRINT_DATE")));
					hmAllData.put("LBL_STATUS",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_STATUS")));
					hmAllData.put("LBL_WARE",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_WARE")));
					hmAllData.put("LBL_SHIP_DATE",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SHIP_DATE")));
					hmAllData.put("LBL_DEALER_NAME",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_DEALER_NAME")));
					hmAllData.put("LBL_HOSPITAL_NAME",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_HOSPITAL_NAME")));
					hmAllData.put("LBL_SUR_DATE",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SUR_DATE")));
					hmAllData.put("LBL_SALES_NAME",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SALES_NAME")));
					hmAllData.put("LBL_BILL_NAME",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_BILL_NAME")));
					hmAllData.put("LBL_MODEL_NUM",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_MODEL_NUM")));
					hmAllData.put("LBL_DESC",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_DESC")));
					hmAllData.put("LBL_LOT_NUM",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_LOT_NUM")));
					hmAllData.put("LBL_EXPIRE_DATE",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_EXPIRE_DATE")));
					hmAllData.put("LBL_QTY",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_QTY")));
					hmAllData.put("LBL_NOTE",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_NOTE")));
					hmAllData.put("LBL_FOOT_NOTE",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_FOOT_NOTE")));
					hmAllData.put("LBL_ORDER_ID",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ORDER_ID")));
					hmAllData.put("LBL_CONSID",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CONS_ID")));
					hmAllData.put("LBL_TAG_ID",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_TAG_ID")));
					// PMT-32450 - to add Loaner Req ID column in Consignment paperwork
					hmAllData.put("LBL_LOANER_REQ_ID",GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_LOANER_REQ_ID"))); 
					hmAllData.put("LOANERREQID",strLoanerReqId);
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Packing Slip</TITLE>

<script language="JavaScript" src="<%=strJsPath%>/GmPackSlip.js"></script>
<script>
var source = "<%=strSource%>";
var tdinnner = "";
var hopt = '<%=strOpt%>';
var strTrack ='<%=strTrack%>';
function hidePrint()
{
	strObject = eval("document.all.button_table");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button_table");
	strObject.innerHTML = tdinnner ;
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10"onload="fnHidebuttons();fnWPrint();" onbeforeprint="hidePrint();" onafterprint="showPrint();">
	
	<FORM name="frmOrder" method="post">
<input type="hidden" name="hMode" value="PrintPack">
<input type="hidden" name="hOrdId" value="<%=strOrdId%>">
        <div id = "button_table" align="center">
			<fmt:setLocale value="${requestScope.CompanyLocale}"/>
			<fmt:setBundle basename="properties.Paperwork"/>
			<td align="center" height="30" id="button" class="RightText" >
			<gmjsp:button value="&nbsp;Print&nbsp;" name="Btn_Print" gmClass="button" tabindex="2" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				<gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_Close" gmClass="button" tabindex="3"	onClick="fnClose();" buttonType="Load" />
				</div>

	<div align="center">
			<%
			GmJasperReport gmJasperReport = new GmJasperReport();
			gmJasperReport.setRequest(request);
			gmJasperReport.setResponse(response);
			gmJasperReport.setJasperReportName(strPackslipJasper);
			gmJasperReport.setHmReportParameters(hmAllData);
			gmJasperReport.setReportDataList(alResult);
			gmJasperReport.setBlDisplayImage(true);
			 if (strSetRbObjectFl.equalsIgnoreCase("YES") ) {
		          gmJasperReport.setRbPaperwork(gmResourceBundleBean.getBundle());//PMT-53539 japan-new-warehouse-address
		        }
			strHtmlJasperRpt = gmJasperReport.getXHtmlReport();
		%>
		<%=strHtmlJasperRpt%>
	</div>
	<p STYLE="page-break-after: always"></p>
	<% if(strBarCodeFl.equals("")){%>
	<div align="center">
		<%
			gmJasperReport.setRequest(request);
			gmJasperReport.setResponse(response);
			gmJasperReport.setJasperReportName(strJasperName);
			gmJasperReport.setHmReportParameters(hmAllData);
			gmJasperReport.setReportDataList(alCartDetails);
			gmJasperReport.setBlDisplayImage(true);
			strHtmlJasperRpt = gmJasperReport.getXHtmlReport();
		%>
		<%=strHtmlJasperRpt%>
	</div>
    <%} %>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
