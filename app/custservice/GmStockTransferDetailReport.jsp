<%
	/**********************************************************************************
	 * File		 		: GmStackTransferDetailReport.jsp
	 * Desc		 		: Stack Transfer Detail Report 
	 * Version	 		: 1.0
	 * author			: TamizhThangam
	 ************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>

<%@ page isELIgnored="false"%>
<%@ page import="java.util.ArrayList,java.util.HashMap"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%> 
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtStockTransfer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmStackTransferDetailReport.jsp -->
<fmtStockTransfer:setLocale value="<%=strLocale%>" />
<fmtStockTransfer:setBundle basename="properties.labels.custservice.GmStockTransfer" />

<bean:define id="gridData" name="frmStockTransferReport" property="gridXmlData" type="java.lang.String" />
<%
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
String strWikiTitle = GmCommonClass.getWikiTitle("STOCK_TRANSFER_DETAIL_REPORT");
String strApplDateFmt = strGCompDateFmt;
String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmResourceBundleBean1 = GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
String strPrintCountFl = GmCommonClass.parseNull( gmResourceBundleBean1.getProperty("PACK_SLIP.PRINT_COUNT"));
%>

<html>
<head>
<title>Stock Transfer Report</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmStockTransferDetailReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script>
	var objGridData;
	objGridData = '<%=gridData%>';
	var date_format = '<%=strApplDateFmt%>';
	var printCountFl ='<%=strPrintCountFl%>';
</script>
</head>

<body leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoad();" ">
<html:form action="/gmStockTransferAction.do?method=stockTransferDetailReport">
<html:hidden name="frmStockTransferReport" property="strOpt" value="" /> 

	<table class="DtTable1100" border="0" cellspacing="0" cellpadding="0" >
	<tr >
		<td height="25" class="RightDashBoardHeader" colspan="15">&nbsp;<fmtStockTransfer:message key="LBL_STOCk_TRANS_RPT_DTL_HEADER"/></td>
		<td align="right" class=RightDashBoardHeader><fmtStockTransfer:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
 		    title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
		</td>
		
	</tr>
	<tr><td colspan="16" height="1"></td></tr>
		<tr class="evenshade">
		     <td class="RightTableCaption"  align="right" colspan="3"><fmtStockTransfer:message key="LBL_FULFILL_COMPANY" var="varFulfillComp"/><gmjsp:label type="RegularText" SFLblControlName="${varFulfillComp}:" td="false" /></td>
			 <td class="RightTableCaption" align="left">&nbsp;<gmjsp:dropdown controlName="fulfillCompany" SFFormName="frmStockTransferReport" SFSeletedValue="fulfillCompany"
				 SFValue="alReqCompList" defaultValue= "[Choose One]" codeId = "A_COMP_ID"  codeName = "A_COMP_NM" /></td>	
			<td class="RightTableCaption" HEIGHT="30"  align="right" colspan="1">
			<fmtStockTransfer:message key="LBL_TRANS_ID" var="varTransId"/><gmjsp:label type="RegularText" SFLblControlName="${varTransId} :" td="false" /></td>
			<td class="RightTableCaption" HEIGHT="30"  align="left"  >&nbsp;<html:text property="trnasctionId" styleClass="InputArea" name="frmStockTransferReport" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
			<td class="RightTableCaption" HEIGHT="30"  align="right" colspan="9">
			<fmtStockTransfer:message key="LBL_FULL_TRANS_ID" var="varFullTransId"/><gmjsp:label type="RegularText" SFLblControlName="${varFullTransId} :" td="false" /></td>
			<td class="RightTableCaption" HEIGHT="30" align="left">&nbsp;<html:text property="fulfillTrnasctionId" styleClass="InputArea" name="frmStockTransferReport" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>	
	</tr>
	<tr><td colspan="16" height="1"></td></tr>
	<tr class="oddshade">
		<td class="RightTableCaption" HEIGHT="30" align=right colspan="3">
		<fmtStockTransfer:message key="LBL_DATE_TYPE" var="varDateType"/><gmjsp:label type="RegularText" SFLblControlName="${varDateType} :" td="false" />
		<td class="RightTableCaption" HEIGHT="30" align="left" colspan="3">&nbsp;<gmjsp:dropdown controlName="dateType" SFFormName="frmStockTransferReport" SFSeletedValue="dateType"
			SFValue="alDateType" defaultValue= "[Choose One]" codeId = "CODEID"  codeName = "CODENM" />&nbsp;&nbsp;
		<fmtStockTransfer:message key="LBL_FROM_DT" var="varFromDt"/><gmjsp:label type="RegularText" SFLblControlName="${varFromDt} :" td="false" />
		<gmjsp:calendar SFFormName="frmStockTransferReport" controlName="fromDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="5" />&nbsp;&nbsp;&nbsp;&nbsp;
		<fmtStockTransfer:message key="LBL_TO_DT" var="varToDt"/><gmjsp:label type="RegularText" SFLblControlName="${varToDt} :" td="false" />&nbsp;
		<gmjsp:calendar SFFormName="frmStockTransferReport" controlName="toDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" /></td>
     	<td class="RightTableCaption" HEIGHT="30" align="right" colspan="6">
		<fmtStockTransfer:message key="LBL_PART_NUM" var="varPartNum"/><gmjsp:label type="RegularText" SFLblControlName="${varPartNum} :" td="false" /></td>
		<td class="RightTableCaption" HEIGHT="30"  align="left" colspan="6">&nbsp;<html:text property="partNum" styleClass="InputArea" name="frmStockTransferReport" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
	</tr>
	<tr class="evenshade">
		<td class="RightTableCaption"  align="right" colspan="3"><fmtStockTransfer:message key="LBL_STATUS" var="varStatus"/><gmjsp:label type="RegularText" SFLblControlName="${varStatus} :" td="false" /></td>
		<td class="RightTableCaption"   align="left" >&nbsp;<gmjsp:dropdown controlName="status" SFFormName="frmStockTransferReport" SFSeletedValue="status"
			SFValue="alStatus" defaultValue= "[Choose One]" codeId = "CODEID"  codeName = "CODENM" /></td>
		<td class="RightTableCaption"  align="right"><fmtStockTransfer:message key="LBL_VARIANCE_QTY" var="varVarianceQty"/><gmjsp:label type="RegularText" SFLblControlName="${varVarianceQty} :" td="false" /></td>
		<td class="RightTableCaption" HEIGHT="30"  align="left" colspan="2">&nbsp;
		<html:text property="varianceQty" styleClass="InputArea" name="frmStockTransferReport" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
		<gmjsp:dropdown controlName="variance" SFFormName="frmStockTransferReport" SFSeletedValue="variance"
			SFValue="alQtyVariance" defaultValue= "[Choose One]" codeId = "CODENMALT"  codeName = "CODENM" />
		</td>
		
		<td colspan="3" width="200" align="center"><fmtStockTransfer:message key="LBL_LOAD" var="varLoad"/>
		<gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" name="Btn_Submit" gmClass="button" tabindex="3" buttonType="Load" onClick="fnLoad();" /> </td>
	</tr>
	<%
		if (gridData.indexOf("cell") != -1) {
	%>
		<tr>
			<td colspan="16">
				<div id="stockTranferDtlRpt" style="height: 500px; width: 1110px;"></div>
			</td>
		</tr>
		<tr>
			<td colspan="16" align="center">
				<div class='exportlinks'>
					<fmtStockTransfer:message key="EXPORT_OPTS" />: <img src='img/ico_file_excel.png' />&nbsp;
					<a href="#" onclick="fnExcel();"><fmtStockTransfer:message key="EXCEL" /></a>
				</div>
			</td>
		</tr>
	<%
		} else {
	%>
		<tr>
			<td colspan="9" height="1" class="LLine"></td>
		</tr>
		<tr>
			<td colspan="9" align="center" class="RightText"><fmtStockTransfer:message key="LBL_NO_DATA"/></td>
		</tr>
	<%}%>
	
	</table>
</html:form>
</body>
<%@ include file="/common/GmFooter.inc"%>
</html>