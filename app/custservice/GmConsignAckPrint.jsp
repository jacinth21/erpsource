 <%
/**********************************************************************************
 * File		 		: GmConsignAckPrint.jsp
 * Desc		 		: This screen is used for the
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<!-- \custservice/GmConsignAckPrint.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
 
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = new HashMap();
	HashMap hmDetails = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strDistName = "";
	String strDate = "";
	String strNextDate = "";
	String strConsignId = "";
	String strCompanyId = "";
	String strCompanyName = "";
	String strCompanyAddress = "";
	String strCompanyPhone = "";
	String strCompanyFax = "";
	String strCompanyLogo = "";
	String strDivisionID = "";
	if (hmReturn != null)
	{
		hmDetails = (HashMap)hmReturn.get("ACKDETAILS");
		strDistName = (String)hmDetails.get("NAME");
		strDate = (String)hmDetails.get("UDATE");
		strNextDate = (String)hmDetails.get("NDATE");
		strConsignId = (String)hmDetails.get("CID");
		strCompanyId = GmCommonClass.parseNull((String)hmDetails.get("COMPANYID"));
		strDivisionID = GmCommonClass.parseNull((String) hmDetails.get("DIVISION_ID"));
		strDivisionID = strDivisionID.equals("")?"2000":strDivisionID; 
		HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(gmDataStoreVO.getCmpid(), strDivisionID);
		strCompanyName = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMPNAME"));
		strCompanyAddress =  GmCommonClass.parseNull((String)hmCompanyAddress.get("COMPADDRESS"));
		strCompanyPhone = GmCommonClass.parseNull((String)hmCompanyAddress.get("PH"));
		strCompanyFax = GmCommonClass.parseNull((String)hmCompanyAddress.get("FAX"));
		strCompanyAddress = strCompanyAddress.replaceAll("/","<br>");
		strCompanyLogo = GmCommonClass.parseNull((String)hmCompanyAddress.get("LOGO"));
	}

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Consignment Acknowledgement Form </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">

<script>
function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
</HEAD>

<BODY topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<BR><BR><BR><BR><BR><BR>
	<table border="0" width="700" cellspacing="0" cellpadding="0" align=center>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="6"></td>
			<td bgcolor="#666666" colspan="3"></td>
			<td bgcolor="#666666" width="1" rowspan="6"></td>
		</tr>
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr><td bgcolor="#666666" height="1" colspan="6"></td></tr>
					<tr>
						<td width="170"><img src="<%=strImagePath%>/<%=strCompanyLogo%>.gif" width="138" height="60"></td>
						<td class="RightText" align="right" width="100%"><%=strCompanyName %><br><%=strCompanyAddress %> <br><br><font size=-2><%=strCompanyPhone %> <br> <%=strCompanyFax%> </font>
						<td>
						<td width="20">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td bgcolor="#666666"></td></tr>
		<tr>
			<td width="698" height="70" valign="top" align="center" class="RightText"><BR><font size="+1">CONSIGNMENT ACKNOWLEDGEMENT FORM</font><BR><BR></td>
		</tr>
		<tr>
			<td width="690" height="80" valign="top" class="RightText">
				&nbsp;On <b><%=strDate%></b>, <%=strCompanyName %> is shipping to <b><%=strDistName%></b> the Items listed 
				<BR><BR>&nbsp;on Consignment Number <b><%=strConsignId%></b> attached hereto.
				<BR><BR><BR>
				&nbsp;<b><%=strDistName%></b> hereby acknowledges that the items listed ("Consignment Inventory") on the <BR><BR>
				&nbsp;aforementioned Consignment Invoice are the property of <%=strCompanyName %>. <BR><BR>&nbsp;Handling of Consignment Intentory Items will be governed by the exclusive Agency Agreement ("Agency Agreement").<Br><br><Br>
				&nbsp;<b><%=strDistName%></b> hereby acknowledges that it is responsible to maintain the Consignment Inventory <BR><BR>
				&nbsp;in good working order and condition. In the event that <b><%=strDistName%></b> fails to maintain the Consignment<BR><BR>&nbsp;Inventory in good working order and condition, it will be liable to <%=strCompanyName %> for full<BR><BR>&nbsp;value of the Consignment Inventory as listed on the Consignment Invoice.
				<BR><BR><BR><BR>
				&nbsp;ACKNOWLEDGED AND AGREED BY:
				<BR><BR><BR><BR><BR><BR>
				<table width="100%" border="0">
					<tr>
						<td class="RightText" width="50%">____________________________________________________</td>
						<td width="50%" align="center">____________</td>
					</tr>
					<tr>
						<td class="RightText" align="center" width="50%">(Name)</td>
						<td class="RightText" align="center" width="50%">(Date)</td>
					</tr>
				</table>
				
				<BR><BR><BR><BR><BR>
				
			</td>
		</tr>
		<tr><td bgcolor="#666666" height="1"></td></tr>
    </table>

<p STYLE="page-break-after: always"></p>
<BR><BR><BR><BR><BR><BR>
	<table border="0" width="700" cellspacing="0" cellpadding="0" align=center>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="6"></td>
			<td bgcolor="#666666" colspan="3"></td>
			<td bgcolor="#666666" width="1" rowspan="6"></td>
		</tr>
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr><td bgcolor="#666666" height="1" colspan="6"></td></tr>
					<tr>
						<td width="170"><img src="<%=strImagePath%>/<%=strCompanyLogo%>.gif" width="138" height="60"></td>
						<td class="RightText" align="right" width="100%"><%=strCompanyName %> <br><%=strCompanyAddress%>  <br><br><font size=-2> <%=strCompanyPhone%> <br> <%=strCompanyFax%></font>
						<td>
						<td width="20">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td bgcolor="#666666"></td></tr>
		<tr>
			<td width="698" height="70" valign="top" align="center" class="RightText"><BR><font size="+1"> ACKNOWLEDGEMENT OF RECEIPT</font><BR><BR></td>
		</tr>
		<tr>
			<td width="690" height="80" valign="top" class="RightText">
				&nbsp;The Undersigned hereby acknowledges the receipt of the Items listed on <%=strCompanyName %> Consignment<BR><BR>
				&nbsp;Number <b><%=strConsignId%></b> attached hereto.
				<BR><BR><BR>
				&nbsp;The items listed on the Consignment Invoice Number <b><%=strConsignId%></b> are the property of <%=strCompanyName %>.<BR><BR>
			
				&nbsp;The Consignment Acknowledgement Form signed by an authorized individual from <b><%=strDistName%></b> <BR><BR>
				&nbsp;on <b><%=strNextDate%></b> governs handling and responsibility for the items.
				<BR><BR><BR><BR>
				&nbsp;ACKNOWLEDGED AND AGREED BY:
				<BR><BR><BR><BR><BR><BR>
				<table width="100%" border="0">
					<tr>
						<td class="RightText" width="50%">____________________________________________________</td>
						<td width="50%" align="center">____________</td>
					</tr>
					<tr>
						<td class="RightText" align="center" width="50%">(Name)</td>
						<td class="RightText" align="center" width="50%">(Date)</td>
					</tr>
				</table>
				
				<BR><BR><BR><BR>
				
			</td>
		</tr>
		<tr><td bgcolor="#666666" height="1"></td></tr>
    </table>


	<table border="0" width="700" cellspacing="0" cellpadding="0" align=center>
		<tr>
			<td align="center" height="30" id="button">
				<gmjsp:button value="&nbsp;Print&nbsp;" name="Btn_Prt" gmClass="button" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				<gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" />
			</td>
		</tr>
	</table>

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
