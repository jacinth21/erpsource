<%
/**********************************************************************************
 * File		 		: GmDistParamSetup.jsp
 * Desc		 		: This screen is used to ADD FIELD SALES PARAMETER
 * author			: Maikandan Rajasekaran
************************************************************************************/
%>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtDistParmsSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- custservice\GmDistParmsSetup.jsp -->

<fmtDistParmsSetup:setLocale value="<%=strLocale%>"/>
<fmtDistParmsSetup:setBundle basename="properties.labels.custservice.GmDistParmsSetup"/>

<%
 String strWikiTitle = GmCommonClass.getWikiTitle("FIELD_SALES_PARAMETER");
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
String strAction = (String)request.getAttribute("hAction");
if (strAction == null)
{
	strAction = (String)session.getAttribute("hAction");
}
String strOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
String distId = GmCommonClass.parseNull((String)request.getParameter("Cbo_DistId"));
String strChecked = GmCommonClass.parseNull((String)request.getAttribute("CHKSKIPRFS"));
if (strChecked.equals(""))
{
	strChecked = "N";
}
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Add Fiels Sales Parameter </TITLE>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel = "stylesheet" type = "text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>


<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmDistributorSetup.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>
var strChecked = '<%= strChecked %>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnonLoad();">

<FORM name="frmDistributor1" method="POST" action="<%=strServletPath%>/GmDistributorServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="Cbo_DistId" value="<%=distId%>">
<input type="hidden" name="hInputString" value="">

<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="100" valign="top">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" class="RightDashBoardHeader" colspan="3">&nbsp;<fmtDistParmsSetup:message key="LBL_ADD_FEILD_SALES_PARAMETER"/></td>
					<td  height="25" class="RightDashBoardHeader">
						<fmtDistParmsSetup:message key="IMG_ALT_HELP" var = "varHelp"/>
						<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
					</tr>
					<% if(strOpt.equals("Reload")){ %>			
					<tr>
						<td align="center" height="20" width="90%" >&nbsp;<font style="color:Green;" size="2"><b><fmtDistParmsSetup:message key="LBL_DATA_SAVED_SUCCESSFULLY"/></b></td>
					</tr>
						<%}%>
						<tr><td class="LLine" colspan="5"></td></tr>
					<tr>
						<td align ="center" height="25" width ="60%" class="RightTableCaption" > <fmtDistParmsSetup:message key="LBL_SKIP_RFS_VALIDATION"/>: <input type="checkbox"  name="Chk_UpdateFl" />&nbsp;
						</td>
					</tr>
					<tr><td class="LLine" colspan="5"></td></tr>
					<tr>
						<td  height="40" colspan="2"  align="center">&nbsp;
						        <fmtDistParmsSetup:message key="BTN_SUBMIT" var="varSubmit"/>
								<gmjsp:button style="width: 5em" value="${varSubmit}" gmClass="button"  onClick="fnSaveDistParams();" buttonType="Save" />&nbsp;
								<fmtDistParmsSetup:message key="BTN_CLOSE" var="varClose"/>
								<gmjsp:button style="width: 5em" value="${varClose}" gmClass="button"  onClick="fnClose();" buttonType="Load" />
						</td>
					</tr>
    			</table>
			</td>
		</tr>
</table>
					
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>