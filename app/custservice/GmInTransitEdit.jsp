<% 
/**********************************************************************************
 * File		 		: GmInTransitEdit.jsp
 * Desc		 		: This screen is used for the Split and Performa Invoice
 * Version	 		: 1.0
 * author			: AGILAN
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib prefix="fmtItemConsignEdit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmInTransitEdit.jsp -->
<fmtItemConsignEdit:setLocale value="<%=strLocale%>"/>
<fmtItemConsignEdit:setBundle basename="properties.labels.custservice.GmInTransitEdit"/>


<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session); 
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmInTransitEdit", strSessCompanyLocale);
	ArrayList alIntransitcodedetails = GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("INTRANSIT_CODE_DETAILS"));
	ArrayList alIntransitcodeRegdetails=GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("INTRANSIT_CODE_DETAILS_REGULAR"));
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmTransRules = (HashMap)request.getAttribute("TRANS_RULES");
	HashMap hmReturnValues = (HashMap)session.getAttribute("HMRETURNVAL");
	hmReturnValues=(HashMap)request.getAttribute("HMRETURNVALUE");
/* 	HashMap hmcodevalue=(HashMap)request.getAttribute("HMRETURNVALUE"); */
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strRuleType =GmCommonClass.parseNull((String)(String) hmTransRules.get("TRANS_RLTXN"));
	String strTransStatus =(String) hmTransRules.get("ICE_TXN_STATUS");
	String strRuleAjax =(String) hmTransRules.get("ICE_RULE_AJAX");
	String strIncludeRuleEng = GmCommonClass.parseNull((String) hmTransRules.get("INCLUDE_RULE_ENGINE"));
	String strTransTypeID = GmCommonClass.parseNull((String) hmTransRules.get("TRANS_ID"));
	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");
	String strConsignId = (String)request.getAttribute("hConsignId") == null?"":(String)request.getAttribute("hConsignId");
	String strPickLocRule = GmCommonClass.parseNull((String) hmTransRules.get("PICK_LOC"));
	HashMap hmReturnvalues = (HashMap)request.getAttribute("HMRETURNVAL");
	String strIntransitcodeRegdetails=(String) hmReturnvalues.get("CODENAME");
	String strIntransitcodedetails=(String) hmReturnvalues.get("CODENAME");
   /*  String strIntransitcode=(String) hmcodevalue.get("CODEID"); */
	String strSetId = "";
	String strDesc = "";
	String strTemp = "";
	String strShade = "";
	String strSetName= "";
	String strAccName = "";
	String strUserName = "";
	String strIniDate = "";
	String strItemQty = "";
	String strControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strConsignType = "";
	String strHeader =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INTRASIT_CONSIGN_HEADER"));
	String strLabel = "";
	String strRequestorName = "";
	String strConsignSetId = "";
	String strFlag = (String)request.getAttribute("hShipFl") == null?"":(String)request.getAttribute("hShipFl");
	String strPrice = "";
	String strBtnVal="";
	String strTagFl = "";
	String strStatusFl = "";
	String strShipFl = "";
	String strDisable=GmCommonClass.parseNull((String)request.getAttribute("disable"));
	String strText = "Release to Consignee";
	String strType = "";
	ArrayList alSetLoad = new ArrayList();
	HashMap hmConsignDetails = new HashMap();
	HashMap hmDetails = new HashMap();
	HashMap hmDetailsCnum=new HashMap();
	String strHeaderRule = "";
	String strLabelRule = "";
	String strTypeRule = "";
	String strApplDateFmt = strGCompDateFmt;
	String strPurpose="";
	String strErrorCnt = "";
	String strErrorDtls = "";
	String strConType="";
	String strCnum="";
	String strSucMsg="";
	String strShipTovalue="";
	String strConsignQty="";
	String strshipDate="";
	int intPriceSize = 0;
	int intsize=0;
	if (hmReturn != null)
	{
		if (strhAction.equals("LoadSet")|| strhAction.equals("EditLoad") || strhAction.equals("EditLoadDash"))
		{
			alSetLoad = (ArrayList)hmReturn.get("SETLOAD");
			strSetId = (String)session.getAttribute("hSetId") == null?"":(String)session.getAttribute("hSetId");
		
			hmConsignDetails = (HashMap)hmReturn.get("CONDETAILS");
			hmDetails = (HashMap)hmReturn.get("CONDETAILS12");

			strBtnVal = (String)hmDetails.get("DETAILS");
			strSucMsg = (String)hmDetails.get("INTRAVALUES");
			strShipTovalue=(String)hmConsignDetails.get("CSHIPTO");
			strshipDate=(String)hmConsignDetails.get("SDATE");
			intsize = alSetLoad.size();
			if (intsize > 0)
			{
				for (int i=0;i<intsize;i++)
				{
			hmDetailsCnum = (HashMap)alSetLoad.get(i);
			strCnum = GmCommonClass.parseNull((String)hmDetailsCnum.get("CNUM"));
			strConsignQty=GmCommonClass.parseNull((String)hmDetailsCnum.get("IQTY"));
			
				}
			}
			strConsignId = (String)hmConsignDetails.get("CID");
			strSetName= (String)hmConsignDetails.get("SNAME");
			strAccName = (String)hmConsignDetails.get("ANAME");
			String strDistName = (String)hmConsignDetails.get("DNAME");
			strAccName = strAccName.equals("")?strDistName:strAccName;
			strUserName = (String)hmConsignDetails.get("UNAME");
			strIniDate = (String)hmConsignDetails.get("CDATE");	
			strDesc = (String)hmConsignDetails.get("COMMENTS");
			strStatusFl = (String)hmConsignDetails.get("SFL");
			strShipFl = (String)hmConsignDetails.get("SHIPFL");
			strLabel = strShipFl.equals("1")?"Release to Shipping":strText;
			strConsignType = (String)hmConsignDetails.get("CTYPE");
			strRequestorName = (String)hmConsignDetails.get("RNAME");
			strConsignSetId = (String)hmConsignDetails.get("SETID");
		
			strErrorCnt = GmCommonClass.parseNull((String)hmConsignDetails.get("ERRCNT"));
			
			strConType = (String)hmConsignDetails.get("TYPE");
			strPurpose = (String)hmConsignDetails.get("PURP");
			
		}
	}
	int intSize = 0;
	String strReTxn = strType;
	String strPendingCtrl =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PENDING_RELEASE"));
	
	if(hmTransRules != null)
	{
	   strHeaderRule = GmCommonClass.parseNull((String)hmTransRules.get("ICE_HEADER"));
	   strLabelRule = GmCommonClass.parseNull((String)hmTransRules.get("ICE_LABEL"));
	   strTypeRule= GmCommonClass.parseNull((String)hmTransRules.get("ICE_TYPE"));
	   
	   if(!strHeaderRule.equals(""))
		   strHeader = strPendingCtrl+" : "+strHeaderRule;
	   if(!strLabelRule.equals(""))
		   strLabel = strLabelRule ;
	   if(!strTypeRule.equals(""))
		   strType = strTypeRule ;
	}
	request.setAttribute("alReturn", alSetLoad);	
			
%>
<HTML>
<HEAD>
<TITLE>Globus Medical:Item Consignment-Enter Control Numbers TEst </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmIntrasitEdit.js"></script>

<script type="text/javascript">
var pickRule = '<%=strPickLocRule%>';
var consignType = '<%=strConsignType%>';
var transStatus  = '<%=strTransStatus%>';
var strRuleAjax='<%=strRuleAjax%>';
var includeRuleEng = '<%=strIncludeRuleEng.toUpperCase()%>';
var vIncludeRuleEng = '<%=strIncludeRuleEng.toUpperCase()%>';
var Type ='<%=strType%>';
var TransTypeID = '<%=strTransTypeID%>';
var consignId = '<%=strConsignId%>';
var intraValues='<%=alIntransitcodedetails%>';
var shipToname='<%=strShipTovalue%>';
var cntlNumber='<%=strCnum%>';
var setId = '<%=strConsignSetId%>';
var strTotalConsignQty='<%=strConsignQty%>';

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnExistScanPartCount();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmConsignItemServlet">
<input type="hidden" name="fgInpStr" id="fgInpStr" value="">
<input type="hidden" name="qnInpuStr" id="qnInpuStr" value="">
<input type="hidden" name="rpInpStr" id="rpInpStr" value="">
<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">

		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
			<td height="25" class="RightDashBoardHeader"><%=strHeader%></td>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="598" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="shade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtItemConsignEdit:message key="LBL_TRANSACTION_ID"/>:</b></td>
						<td class="RightText">&nbsp;<%=strConsignId%></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>	
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtItemConsignEdit:message key="LBL_TRANSACTION_TYPE"/>:</b></td>
						<td class="RightText">&nbsp;<%=strConType%></td>
					</tr>
						<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>	
					<tr class="shade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtItemConsignEdit:message key="LBL_PURPOSE"/>:</b></td>
						<td class="RightText">&nbsp;<%=strPurpose%></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>	
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtItemConsignEdit:message key="LBL_SET_ID"/>:</b></td>
						<td class="RightText">&nbsp;<%=strConsignSetId%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;<b><fmtItemConsignEdit:message key="LBL_SET_NAME"/>:</b>&nbsp;&nbsp;<%=strSetName%></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>	
					<tr class="shade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtItemConsignEdit:message key="LBL_LAST_UPDATED_BY"/>:</b></td>
						<td class="RightText">&nbsp;<%=strUserName%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<b><fmtItemConsignEdit:message key="LBL_SHIP_DATE"/>:</b>&nbsp;&nbsp;<%=strshipDate%></td>
					</tr>
				
					<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
	
					<tr>
						<td colspan="3">
						<jsp:include page="/common/GmIncludeLog.jsp" >
						<jsp:param name="LogType" value="" />
						<jsp:param name="LogMode" value="Edit" />
						</jsp:include>
						</td>
					</tr>

					<tr>
						<td colspan="3">
							<table cellspacing="0" cellpadding="0" border="0" class="DtTable1000" id="myTable">
								<tr class="ShadeRightTableCaption">
									<td align="center" height="25"><fmtItemConsignEdit:message key="LBL_PART"/><BR><fmtItemConsignEdit:message key="LBL_NUMBER"/>&nbsp;&nbsp;</td>
									<td width="300"><fmtItemConsignEdit:message key="LBL_DESCRIPTION"/></td>
									<td align="center"><fmtItemConsignEdit:message key="LBL_CONSIGN"/><BR><fmtItemConsignEdit:message key="LBL_QTY"/></td>
									<td></td>
									<td width="200">&nbsp;&nbsp;&nbsp;&nbsp;<fmtItemConsignEdit:message key="LBL_QTY"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtItemConsignEdit:message key="LBL_CONTROL_NUMBER"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtItemConsignEdit:message key="LBL_ACTION"/> 	</td>
								</tr>
<%
						intSize = alSetLoad.size();
						StringBuffer sbPartNum = new StringBuffer();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();

							String strNextPartNum = "";
							int intCount = 0;
							String strColor = "";
							String strLine = "";
							String strPartNumHidden = "";
							String strPartNumIndex = "";
							String StrParthidsts = "";
							String strParthidfinalsts = "";
							String strPrevPartNum = "";
							String strPartMaterialType = "";
							double dbAllTotQty = 0;
							
							for (int i=0;i<intSize;i++)
							{								
								hmLoop = (HashMap)alSetLoad.get(i);
								if (i<intSize-1)
								{
									hmTempLoop = (HashMap)alSetLoad.get(i+1);
									strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
								}								
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));								
								if(sbPartNum.indexOf(strPartNum+",") == -1){
									sbPartNum.append(strPartNum);
									sbPartNum.append(",");
								}								
								strPartNumHidden = strPartNum;
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strPartMaterialType = GmCommonClass.parseNull((String)hmLoop.get("PARTMTRELTYPE"));	
								strQty = GmCommonClass.parseNull((String)hmLoop.get("QTYUSED"));
								strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
								strTagFl = GmCommonClass.parseNull((String)hmLoop.get("TAGFL"));
								strErrorDtls = GmCommonClass.parseNull((String)hmLoop.get("ERRORDTLS"));
								strErrorDtls = strErrorDtls.replaceAll("<B>", "").replaceAll("</B>", "");
								strTemp = strPartNum;
								StrParthidsts = "";
								
								if(!strPrevPartNum.equals(strPartNum)&&(i!=0)){									
									StrParthidsts = "<input type=\"hidden\" id=\""+strPrevPartNum+"\" name=\""+strPrevPartNum+"\" value=\""+strPartNumIndex+"\">";
									strPartNumIndex="";
								}
								
								if (strPartNum.equals(strNextPartNum))
								{
									intCount++;
									strLine = "<TR><TD colspan=6 height=1 class=Line></TD></TR>";
								}

								else
								{
									strLine = "<TR><TD colspan=6 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
										strPartNum = "";
										strPartDesc = "";
										strQty = "";
										strLine = "";
									}
									intCount = 0;
								}
								strPartNumIndex = strPartNumIndex +i+",";
								if (intCount > 1)
								{
									strPartNum = "";
									strPartDesc = "";
									strQty = "";
									strLine = "";
								}
								
								if(i==(intSize-1)){
									strParthidfinalsts = "<input type=\"hidden\" id=\""+(strPartNum.equals("")?strPrevPartNum:strPartNum)+"\" name=\""+(strPartNum.equals("")?strPrevPartNum:strPartNum)+"\" value=\""+strPartNumIndex+"\">";
									strPartNumIndex = "";
								}
							
								strShade = (i%2 != 0)?"class=Shade":""; 
								strItemQty = GmCommonClass.parseNull((String)hmLoop.get("IQTY"));
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								dbAllTotQty += Double.parseDouble(GmCommonClass.parseZero(strItemQty));
								strPrevPartNum = strPartNumHidden;
								out.print(strLine);
								String strAction ="Cbo_Action" + i;	 
%>
								<tr id="tr<%=i %>" colspan="3">
<%
								if ((strhAction.equals("EditLoad") ))
								{
									log.debug("test" +strPartNumHidden + "strPartNum  " +strPartNum + "strPrice " +strPrice);
%>										<%if (strConsignSetId.equals("")) { %>

									<td class="RightText" height="20">&nbsp;<%if(strTagFl.equals("Y")){%><a href=javascript:fnOpenTag('<%=strPartNumHidden %>','<%=strConsignId %>')> <img src=<%=strImagePath%>/tag.jpg border="0"></a><%}%><a class="RightText" tabindex=-1 href="javascript:fnSplit('<%=i%>','<%=strPartNum%>');"><%=strPartNum%></a>
									<% } else { %>
									 <td class="RightText" height="20">&nbsp;<%if(strTagFl.equals("Y")){%><a href=javascript:fnOpenTag('<%=strPartNumHidden %>','<%=strConsignId %>')> <img src=<%=strImagePath%>/tag.jpg border="0"></a><%}%><%=strPartNum%>
										<%} %> 
									<input type="hidden" name="hPartNum<%=i%>" id="hPartNum<%=i%>" value="<%=strPartNumHidden%>">
									<input type="hidden" name="hQty<%=strPartNum.replaceAll("\\.","").replaceAll("\\-","")%>" id="hQty<%=strPartNum.replaceAll("\\.","").replaceAll("\\-","")%>"  value="<%=strQty%>">
									<input type="hidden" name="hPrice<%=i%>" id="hPrice<%=i%>" value="<%=strPrice%>"></td>
									<input type="hidden" id="hPartMaterialType<%=i%>" name="hPartMaterialType<%=i%>" value="<%=strPartMaterialType%>">
									<input type="hidden" id="hControl<%=i%>" name="hControl<%=i%>" value="<%=strControl%>">				
													
																	
									<%if(StrParthidsts.length()>0){%>
										<%=StrParthidsts%>
									<%}%>
									<%if(strParthidfinalsts.length()>0){%>
										<%=strParthidfinalsts%>
									<%}%>
<%
								}
								else{
%>
									<td class="RightText" height="20">&nbsp;<%if(strTagFl.equals("Y")){%><a href=javascript:fnOpenTag('<%=strPartNumHidden %>','<%=strConsignId %>')> <img src=<%=strImagePath%>/tag.jpg border="0"></a><%}%> <%=strPartNum%></td>
<%
									}
%>
									<td class="RightText" ><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td class="RightText" align="center"><%=strQty%></td>
									<td align="center"><label id ="lblPartScan<%=strPartNum %>" ></label> </td>

									<td id="Cell<%=i%>" class="RightText">&nbsp;&nbsp;&nbsp;<input type="text" 
									size="3" value="<%=strItemQty%>" name="Txt_Qty<%=i%>"  id="Txt_Qty<%=i%>" 
									class="InputArea Controlinput"  onFocus="chgTRBgColor(<%=i %>,'#AACCE8');"
									 onBlur="chgTRBgColor(<%=i %>,'#ffffff');" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									 
									<input type="text" size="22" value="<%=strControl%>" id="Txt_CNum<%=i%>" 
									name="Txt_CNum<%=i%>" class="InputArea Controlinput" maxlength="40"  
									onBlur="fnBlankTBE(<%=i %>,this.value);&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										
									<%if(!strIncludeRuleEng.equalsIgnoreCase("NO"))
									{  %>
									
									fnFetchMessages(this,'<%=strPartNumHidden%>','PROCESS', '<%=strType %>','<%=strTransTypeID%>');<%}%>" 
									onFocus="fnBlankLine(<%=i %>,this.value);">
									<% if(!strErrorDtls.equals("") && !strControl.equals("")){ %>
									<span id="DivShowCnumNotExists<%=i%>" style="vertical-align:middle;">
									 <img height="12" width="12" title="<%=strErrorDtls %>" src="<%=strImagePath%>/delete.gif"></img></span>
									<%} %>
									
									<%
									if (strConsignSetId.equals(""))
											{
									%>
									
									&nbsp;&nbsp;&nbsp;<gmjsp:dropdown
									controlName ="<%=strAction%>" seletedValue="<%= strIntransitcodedetails %>"
									width="180" value="<%= alIntransitcodedetails%>" codeId="CODEID"
									codeName="CODENAME" defaultValue="" />								
									<%} else {%>
									&nbsp;&nbsp;&nbsp;<gmjsp:dropdown
									controlName ="<%=strAction%>" seletedValue="<%= strIntransitcodeRegdetails %>"
									width="180" value="<%= alIntransitcodeRegdetails%>" codeId="CODEID"
									codeName="CODENAME" defaultValue="" />
									<% } %> 
								</tr>
<%
								
							}
							out.print("<input type=hidden name=pnums value="+sbPartNum.toString().substring(0,sbPartNum.length()-1)+">");
							out.print("<input type=hidden id=hTotalParts name=hTotalParts value="+dbAllTotQty+">");
						}else {
%>
						<tr><td colspan="5" height="50" align="center" class="RightTextRed"><BR><fmtItemConsignEdit:message key="LBL_NO_PART_NUMBERS_HAVE_BEEN_MAPPED"/></td></tr>
<%
						}		
%>
							</table>
						</td>								
						<input type="hidden" id="hCnt"    name="hCnt"    value="<%=intSize-1%>">
						<input type="hidden" id="hNewCnt" name="hNewCnt" value="0">
					
					</tr>
          
					<%{ %>

					<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>	
					
					<%if(!strBtnVal.equals("Y"))  { %> 
						<td colspan="2" height="30" align="center" class="RightTextRed">
						<BR><BR>
							<fmtItemConsignEdit:message key="BTN_SUBMIT" var="varSubmit"/>
							<gmjsp:button value="${varSubmit}" gmClass="button" name="Btn_Submit" onClick="fnSubmitIntrasit('pickLocRule');"  buttonType="Save" />&nbsp;&nbsp;
							<fmtItemConsignEdit:message key="BTN_PROFORMA_INV" var="varProformaInv"/>
							<gmjsp:button value="${varProformaInv}" gmClass="button" onClick="fnPrintVersionLetter('manually');"  buttonType="Load" />&nbsp;&nbsp;
							<BR><BR>
						</td>
						<%} else { %>
					
				<tr>
				<td height="25" colspan="4" align="center" class=""><font style="color: green" size="2"><b><%=strSucMsg%></b></font></td>
			</tr>		
						<%} %>
						 
<%
					}
%>
				 </table>
			 </td>
		</tr>
		<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>	
		<tr>		
			<td colspan="3">
					<jsp:include page="/operations/itemcontrol/GmValidationDisplayInclude.jsp">
					<jsp:param value="<%=strErrorCnt%>" name="errCnt"/>
					</jsp:include>
			</td>
		</tr>
    </table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
