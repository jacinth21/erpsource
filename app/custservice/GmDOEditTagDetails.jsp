<% 
 /**********************************************************************************
 * File		 		: GmDOEditTagDetails
 * Desc		 		: This screen is used to update the Tag Details.
 * Version	 		: 1.0
 * author			: 
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="java.util.Date"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtDOEditTagDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- custservice\GmDOEditTagDetails.jsp -->

<fmtDOEditTagDetails:setLocale value="<%=strLocale%>"/>
<fmtDOEditTagDetails:setBundle basename="properties.labels.custservice.GmDOEditTagDetails"/>



<bean:define id="alTagID" name="frmDOSummaryEdit" property="alTagID" type="java.util.ArrayList"></bean:define>
<bean:define id="alTagDetails" name="frmDOSummaryEdit" property="alTagDetails" type="java.util.ArrayList"></bean:define>
<bean:define id="orderID" name="frmDOSummaryEdit" property="orderID" type="java.lang.String"></bean:define>
<bean:define id="accountID" name="frmDOSummaryEdit" property="accountID" type="java.lang.String"></bean:define>
<bean:define id="caseID" name="frmDOSummaryEdit" property="caseID" type="java.lang.String"></bean:define>
<bean:define id="caseInfoID" name="frmDOSummaryEdit" property="caseInfoID" type="java.lang.String"></bean:define>
<bean:define id="accountNm" name="frmDOSummaryEdit" property="accountNm" type="java.lang.String"></bean:define>
<bean:define id="surgeryDt" name="frmDOSummaryEdit" property="surgeryDt" type="java.lang.String"></bean:define>
<bean:define id="orderTypeNm" name="frmDOSummaryEdit" property="orderTypeNm" type="java.lang.String"></bean:define>
<bean:define id="tagID" name="frmDOSummaryEdit" property="tagID" type="java.lang.String"></bean:define>
<bean:define id="custPO" name="frmDOSummaryEdit" property="custPO" type="java.lang.String"></bean:define>
<bean:define id="parentOrderID" name="frmDOSummaryEdit" property="parentOrderID" type="java.lang.String"></bean:define>
<%

HashMap hmBillInfo = new HashMap();
HashMap hmAccsum = new HashMap();

String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
String strSessDeptId = GmCommonClass.parseNull((String)session.getAttribute("strSessDeptId"));
String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
ArrayList alOrderSummary = new ArrayList();

hmBillInfo = (HashMap)request.getAttribute("hmResult");

String strCaseID = "";
String strAlertImage = "<img id=imgDis src=".concat(strImagePath).concat("/30.gif title='Alert'  width='15' height='15' >");


String strAccNm = "";
String strAccId = "";
String strOrdId = "";

String strPartNum = "";
String strDesc = "";
String strPrice = "";
String strQty = "";
String strControlNum = "";

String strItemtype = "";

String strDOTypeName = "";
String strShade = "";
String strTagID = "";
String strRepName = "";
String strSelected = "";
String strRegn = "";
String strAdName = "";
String strDistName = "";
String strTerrName = "";
String strRepId = "";
String strGpoNm = "";
String strOrderDate = "";
String strCustPO = "";
Date dtOrderDate = null;

 if (hmBillInfo != null)
{
	hmAccsum = (HashMap)hmBillInfo.get("ACCSUM");
	
	strRegn = GmCommonClass.parseNull((String)hmAccsum.get("RGNAME"));
    strAdName = GmCommonClass.parseNull((String)hmAccsum.get("ADNAME"));
    strDistName = GmCommonClass.parseNull((String)hmAccsum.get("DNAME"));
    strTerrName = GmCommonClass.parseNull((String)hmAccsum.get("TRNAME"));
    strRepName = GmCommonClass.parseNull((String)hmAccsum.get("RPNAME"));
    strRepId = GmCommonClass.parseNull((String)hmAccsum.get("REPID"));
    strGpoNm = GmCommonClass.parseNull((String)hmAccsum.get("PRNAME"));
	
} 
 
 int intTagIdSize =  alTagID.size();
 
%>
<html>
<head>
<title>Globus Medical:Edit Tag Details</title>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmDOEditTagDetails.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script>
var cnt = 0;
var orderSize = '<%=alTagDetails.size()%>';
var dept = '<%=strSessDeptId%>';

var strBuffer = "";
<%HashMap hmTag = new HashMap();
  String strCodeid = "";
   for (int j=0;j<intTagIdSize;j++)
   {
	   hmTag = (HashMap)alTagID.get(j);
	   
		strCodeid = (String)hmTag.get("ID");
		%>
		strBuffer += "  <option  value='<%=strCodeid%>'> <%=strCodeid%> </option>";
		
		<% }%>
		
		
var OrderID = '<%=orderID%>';
var caseinfoid = '<%=caseInfoID%>';
var accID = '<%=accountID%>';
var parentOrdId = '<%=parentOrderID%>';


</script>
</head>
<body leftmargin="20" topmargin="10">
<html:form action="/gmDOSummaryEdit.do?method=editTagDetails">
<input type="hidden" name="orderID" value="<%=orderID%>">
<input type="hidden" name="caseID" value="<%=caseID%>">
<input type="hidden" name="caseInfoID" value="<%=caseInfoID%>">
<input type="hidden" name="accountID" value="<%=accountID%>">
<input type="hidden" name="strOpt" value="">
<input type="hidden" name="inputStr" value="">
 <table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
	<tr>
			<td height="25" class="RightDashBoardHeader" colspan="5"><fmtDOEditTagDetails:message key="LBL_EDIT_TAG_DETAILS"/></td>
			<td class="RightDashBoardHeader" align="right">
				<fmtDOEditTagDetails:message key="IMG_ALT_HELP" var = "varHelp"/>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("EDIT_TAG_DETAILS")%>');" />
			</td>
	</tr>
		<tr><td colspan="6"><font color="red"><span id ="ErrMessage"></span></font> </td></tr>
		<tr><td class="Line" height="1" colspan="6"></td></tr>
		<tr >
			<td height="25" class="RightTableCaption" align="right"><fmtDOEditTagDetails:message key="LBL_DO_ID"/>:</td>
			<td>&nbsp;<bean:write name="frmDOSummaryEdit" property="orderID" /></td>
			<td height="25" class="RightTableCaption" align="right" ><fmtDOEditTagDetails:message key="LBL_CASE_ID"/>:</td>
			<td>&nbsp;<bean:write name="frmDOSummaryEdit" property="caseID" /></td>
			<td class="RightTableCaption" align="Right" height="24"><fmtDOEditTagDetails:message key="LBL_CUSTOMER_PO"/>:</td>
			<td  width="20%">&nbsp;<bean:write name="frmDOSummaryEdit" property="custPO" /></td>
		</tr>
		<tr><td class="Line" height="1" colspan="6"></td></tr>
			
		<tr class="Shade">
		<td height="25" class="RightTableCaption" align="right"><fmtDOEditTagDetails:message key="LBL_ACCOUNT_ID"/>:</td>
			<td>&nbsp;<bean:write name="frmDOSummaryEdit" property="accountID" /></td>		
			<td height="25" class="RightTableCaption" align="right"><fmtDOEditTagDetails:message key="LBL_ACCOUNT_NAME"/>:</td>
			<td colspan="2">&nbsp;<bean:write name="frmDOSummaryEdit" property="accountNm" /></td>	
			<td height="25" class="RightTableCaption" colspan="1">
			</td>					
		</tr>
						<%if(!strSessDeptId.equals("S")){ %>
							        <tr><td class="Line" height="1" colspan="6"></td></tr>
									<tr>
										<td class="RightTableCaption" align="Right" height="24"><fmtDOEditTagDetails:message key="LBL_REGION"/>:  </td>
										<td >&nbsp;<%=strRegn %></td>
										<td class="RightTableCaption" align="Right"><fmtDOEditTagDetails:message key="LBL_AREA_DIRECTOR"/>:</td>
										<td >&nbsp;<%=strAdName %></td>
										<td class="RightTableCaption" align="Right"><fmtDOEditTagDetails:message key="LBL_FIELD_SALES"/>:</td>
										<td >&nbsp;<%=strDistName %></td>
									</tr>
									<tr><td class="Line" height="1" colspan="6"></td></tr>
									<tr class="Shade">
										<td class="RightTableCaption" align="Right"  height="24"><fmtDOEditTagDetails:message key="LBL_TERRITORY"/>:</td>
										<td >&nbsp;<%=strTerrName %></td>
										<td class="RightTableCaption" align="Right"><fmtDOEditTagDetails:message key="LBL_REP"/>:</td>
										<td >&nbsp;<%=strRepName%> &nbsp;(<%=strRepId %>)</td>
										<td class="RightTableCaption" align="Right"><fmtDOEditTagDetails:message key="LBL_GROUP_PRICE_BOOK"/>:</td>
										<td >&nbsp;<%=strGpoNm%></td>
									</tr>
									<tr><td class="Line" height="1" colspan="6"></td></tr>
									<tr>
										
										<td class="RightTableCaption" align="Right" height="24"><fmtDOEditTagDetails:message key="LBL_DATA_ENTERED"/>:</td>
										<td >&nbsp;<label for=surgeryDt></label><%=surgeryDt %></td>
										<td class="RightTableCaption" align="Right"><fmtDOEditTagDetails:message key="LBL_ORDER_TYPE"/>:</td>
										<td >&nbsp;<label for=orderTypeNm><%=orderTypeNm %> </label> </td>
									</tr>
									
		<% strShade ="Shade";}%>
	<tr><td class="Line" height="1" colspan="6"></td></tr>
	
	<tr class="<%=strShade%>">
	<td height="24" colspan="6" class="RightTableCaption">&nbsp;<%=strAlertImage %> &nbsp;<fmtDOEditTagDetails:message key="LBL_ADD_EDIT"/> &nbsp; <IMG id="plus" border=0 src="<%=strImagePath%>/plus.gif"> &nbsp; <fmtDOEditTagDetails:message key="LBL_ADD_MORE_TAGS"/></td>
	</tr>

	<tr><td class="Line" height="1" colspan="6"></td></tr>
						
					<TR>
			<td colspan="6"> 
			<div>
			<table cellpadding="0" cellspacing="0" width="100%" border="1" bordercolor="gainsboro" id="EditTagDetails">
			  <thead>
				<TR class="Shade" class="RightTableCaption">
					<TH class="RightText" width="120" align="center" height="25"><fmtDOEditTagDetails:message key="LBL_PARTNUMBER"/></TH>
					<TH class="RightText" width="400" >&nbsp;<fmtDOEditTagDetails:message key="LBL_PART_DESCRIPTION"/></TH>
					<TH class="RightText" width="100" align="center"><fmtDOEditTagDetails:message key="LBL_QTY"/></TH>
					<TH class="RightText" width="60" align="center"><fmtDOEditTagDetails:message key="LBL_TAG"/></TH>
					<TH class="RightText" width="100" align="center"><fmtDOEditTagDetails:message key="LBL_OTHER_TAGS"/></TH>
					<TH class="RightText" width="40" align="center"><fmtDOEditTagDetails:message key="LBL_TYPE"/></TH>
				</TR>	
			  </thead>
			  <TBODY>
<%
			  	int	intSize = alTagDetails.size();
				HashMap	hcboVal = new HashMap();
					for (int i=0;i<intSize;i++)
			  		{
						hcboVal = (HashMap)alTagDetails.get(i);
			  			strPartNum = (String)hcboVal.get("PNUM");
						strDesc = (String)hcboVal.get("PDESC");
						strQty = (String)hcboVal.get("IQTY");
						strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
						strItemtype = GmCommonClass.parseNull((String)hcboVal.get("ITYPE"));
						strTagID = GmCommonClass.parseNull((String)hcboVal.get("TAG_ID"));
						
%>
								<tr <%=strShade %>>
									<td  height="20" class="RightText">
										<%if(!strQty.equals("1")){ %>
										&nbsp;<a href="javascript:fnAddRow(<%=i+1%>,'<%=strPartNum %>');" ><IMG id="partInc<%=i+1%>" border=0 src="<%=strImagePath%>/plus.gif"></a>
										<%}else{ %>
										&nbsp;<span style="visibility: hidden"> <a href="javascript:fnAddRow(<%=i+1%>,'<%=strPartNum %>');" ><IMG id="partInc<%=i+1%>" border=0 src="<%=strImagePath%>/plus.gif"></a></span>
										<%} %>
										<%=strPartNum%> <input type="hidden" name="hPnum<%=i+1%>" value= "<%=strPartNum%>" id ="<%=i+1%>">
										<input type="hidden" name="hPartNum<%=i+1%>" value= "<%=strPartNum%>"/>
										 </td>
									
									<td id="CellRemove<%=i+1%>" class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
									<td align="center" id="Cell<%=i+1%>" class="RightText">&nbsp;<a href="javascript:fnDecresQty(<%=i+1%>);" ><IMG id="qtyDec<%=i+1%>" border=0  src="<%=strImagePath%>/minus.gif"></a>&nbsp;<input type="text" name="qty<%=i+1%>" value="<%=strQty%>" size="2"> <a href="javascript:fnIncrQty(<%=i+1%>,<%=strQty%>);" ><IMG id="qtyInc<%=i+1%>" border=0  src="<%=strImagePath%>/plus.gif"></a>
									<input type="hidden" name="hQty<%=i+1%>" value="<%=strQty%>" size="2"></td>
									
									<td align="center" id="CellTag<%=i+1%>" class="RightText" nowrap>&nbsp; <select name="tagID<%=i+1%>" class="RightText" onChange="fnOtherTag(<%=i+1 %>);javascript:fnCheckTagID(<%=i+1 %>,'<%=strPartNum %>');" id ="<%=i+1%>">  
		  <option  value="0"> [Choose One]</option>
		  <%  HashMap hmTagID = new HashMap();
		  String strCodeID = "";
           for (int j=0;j<intTagIdSize;j++)
           {
        	   hmTagID = (HashMap)alTagID.get(j);
				strCodeID = (String)hmTagID.get("ID");
				strSelected = strTagID.equals(strCodeID)?"selected":"";
				%>
				<option <%=strSelected%> value="<%=strCodeID%>"> <%=strCodeID%></option>
				<% }%>
				<option value="500500"><fmtDOEditTagDetails:message key="LBL_"/></option>
           </select> </td>
									
									  <td id="CellOtherTag<%=i+1%>" class="RightText" align="center"><input type="text" id="otherTag<%=i+1%>" id="otherTag<%=i+1%>" name="otherTag<%=i+1%>" value="" style="visibility: hidden;" size="8" onBlur="javascript:fnCheckOtherTagID(<%=i+1%>,'<%=strPartNum %>')";></td>
									
									<td  align="center" id="CellType<%=i+1%>" class="RightText"> <input type="hidden" name="Itype<%=i+1%>" value ="<%=strItemtype%>" > <%=strItemtype%>&nbsp;&nbsp;</td>
									
								</tr>
<%								} // For Loop ends here					
%>
			
			  </TBODY>
			</table>
			<INPUT type="hidden" name="hTextboxCnt" value="<%=intSize%>">
			</div>
			</td>
		</tr>
		<tr>
		<td align="center" height="25" colspan="6"><br><fmtDOEditTagDetails:message key="BTN_SAVE" var="varSave"/>
		<gmjsp:button style="width: 5em" value="${varSave}"  buttonType="save" onClick="fnSave();" />
		&nbsp;&nbsp;<fmtDOEditTagDetails:message key="BTN_CANCEL" var="varCancel"/>
		<gmjsp:button style="width: 5em" value="${varCancel}" buttonType="load" onClick="fnCancel();" />
		</td></tr>
		<tr>
				<td height="7" colspan="6"></td>
		</tr>
</table>		 

</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>