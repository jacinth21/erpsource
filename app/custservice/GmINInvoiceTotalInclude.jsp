<%
/**********************************************************************************
 * File		 		: GmInInvoiceTotalInclude.jsp
 * Desc		 		: This screen is used to display the Total section
 * Version	 		: 1.0
 * author			: 
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@page import="java.util.Date"%>
<%@ taglib prefix="fmtPrintPrice" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmInInvoiceTotalInclude.jsp -->
<fmtPrintPrice:setLocale value="<%=strLocale%>"/>
<fmtPrintPrice:setBundle basename="properties.labels.custservice.GmPrintPrice"/>
<%
String strAccCurrSymb= "";
String strTotalPrice = "";
String strTaxTotal = "";
String strTotal = "";
String strExOrderType = "";
String strQty = "";
String strItemQty = "";
String strAfterItemTotal = "";
String strAfterPrice = "";
String strWidth = "";
int intSize = 0;
int intQty = 0;
int intPreQty = 0;
double dbTotal = 0.0; 
double dbTaxTotal = 0.0;
double dbOrdTotal = 0.0;
double dbGrandTotal = 0.0;
double dbShipTotal = 0.0;
double dbInvTotal = 0.0;
double dbTaxGrandTotal = 0.0;
double dbInvGrandTotal = 0.0;
double dbSubTotal = 0.0;
double dbAfterItemTotal = 0.0;
double dbAfterPriceTotal = 0.0;
double dbAfterTotal = 0.0;
double dbAdjustPrice = 0.0;
double dbExchRate = 0.0;
HashMap hmReturn = new HashMap();
HashMap hmOrderDetails = new HashMap();
HashMap hcboVal = new HashMap();
ArrayList alOrderSummary = new ArrayList();
hmReturn = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturn"));
String strCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
String strColspan = GmCommonClass.parseNull((String)request.getParameter("COLSPAN"));
String strOrderSource = GmCommonClass.parseNull((String)request.getParameter("ORDERSOURCE"));
String strSessDeptId = GmCommonClass.parseNull((String)request.getParameter("STRSESSDEPTID"));
String strhidePartPrice = GmCommonClass.parseNull((String)request.getParameter("STRHIDEPARTPRICE"));
String strSubTotal = GmCommonClass.parseNull((String)request.getParameter("SUBTOTAL"));
String strGSTStartDtFl = GmCommonClass.parseNull((String)request.getAttribute("GSTStartFl"));
String strTaxGrandTotal = GmCommonClass.parseNull((String)request.getParameter("GRANDTOTAL"));
String strIGSTTotal = GmCommonClass.parseNull((String)request.getParameter("IGSTTOTAL"));
String strCGSTTotal = GmCommonClass.parseNull((String)request.getParameter("CGSTTOTAL"));
String strSGSTTotal = GmCommonClass.parseNull((String)request.getParameter("SGSTTOTAL"));
String strInvTotal = GmCommonClass.parseNull((String)request.getParameter("INVTOTAL"));
boolean blDOFromApp = false;
blDOFromApp = (strOrderSource.equals("103521"))? true:blDOFromApp;
hmOrderDetails = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("ORDERDETAILS"));
alOrderSummary = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ORDERSUMMARY"));
String strExchRate = GmCommonClass.parseNull((String)hmOrderDetails.get("EXCH_RATE"));
strExchRate = strExchRate.equals("0")? "1": strExchRate;

// Sub Total
dbAfterItemTotal = Double.parseDouble(strSubTotal);
dbExchRate = Double.parseDouble(strExchRate);
dbAfterItemTotal = dbAfterItemTotal / dbExchRate;
strSubTotal = "" + dbAfterItemTotal;

// Tax Grand total
dbTaxGrandTotal = Double.parseDouble(strTaxGrandTotal);
dbTaxGrandTotal = dbTaxGrandTotal/ dbExchRate;
strTaxGrandTotal = "" + dbTaxGrandTotal;

// Invoice Total
dbInvGrandTotal = Double.parseDouble(strInvTotal);
dbInvGrandTotal = dbInvGrandTotal/ dbExchRate;
strInvTotal = "" + dbInvGrandTotal;

strAccCurrSymb = GmCommonClass.parseNull((String)hmOrderDetails.get("ACC_CURR_SYMB"));
strCurrSign = strAccCurrSymb.equals("")? strCurrSign: strAccCurrSymb;

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Order Summary</TITLE>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>	
<script>

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" >
<FORM name="frmOrder" method="post" action="<%=strServletPath%>/GmOrderItemServlet">
<%-- <% if(strClientSysType.equals("IPAD")){%>
	<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
<% }else{%>
	<table style="margin:0 0 0 0;width:900px;"  border="0" cellspacing="0" cellpadding="0">
<%} %> --%>

<% 
		strColspan = (blDOFromApp)? ((Integer.parseInt(strColspan)+2)+"") :strColspan;
		strWidth =  (blDOFromApp)? "" : "87%";
		if(strSessDeptId.equals("S")){
		 	if(!strhidePartPrice.equals("N")){ 
%>
				<tr><td colspan="15" height="1" bgcolor="#666666"></td></tr>								
				<tr>
					<td colspan="15">
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td  height="20" width="87%" align="right" class="RightTableCaption" id="grdTotal">&nbsp; <fmtPrintPrice:message key="LBL_GRAND_TOTAL"/> </td>
								<gmjsp:currency textValue="<%=strSubTotal%>" gmClass="RightTableCaption"  type="BoldCurrTextSign" currSymbol="<%=strCurrSign %>"/>
							</tr>
						</table>
					</td>
				</tr>
			<%}else{ %>
			<%}
		 }else{ %>
			<tr><td colspan="15" height="1" bgcolor="#666666"></td></tr>								
			<tr>
				<td  height="20" width="<%=strWidth %>" colspan="<%=strColspan %>" align="right" class="RightTableCaption" id="grdTotal1">&nbsp; <fmtPrintPrice:message key="LBL_SUB_TOTAL"/></td>
				<gmjsp:currency textValue="<%=strSubTotal%>" gmClass="RightTableCaption" type="BoldCurrTextSign" currSymbol="<%=strCurrSign %>"/>
			</tr>
		<%} %>
		<% if(!strSessDeptId.equals("S"))
			{
			strColspan = (blDOFromApp)?"14":"12";									
		%>
		
		<%if(strGSTStartDtFl.equals("Y")){ %>
	    <tr>
			<td  height="15" colspan="<%=strColspan %>" align="right" class="RightTableCaption" >&nbsp; <fmtPrintPrice:message key="LBL_IGST_TOTAL"/></td>
			<gmjsp:currency textValue="<%=strIGSTTotal%>" gmClass="RightTableCaption"  type="BoldCurrTextSign" currSymbol="<%=strCurrSign%>"/>
		</tr>
		<tr>
			<td  height="15" colspan="<%=strColspan %>" align="right" class="RightTableCaption" >&nbsp; <fmtPrintPrice:message key="LBL_CGST_TOTAL"/></td>
			<gmjsp:currency textValue="<%=strCGSTTotal%>" gmClass="RightTableCaption"  type="BoldCurrTextSign" currSymbol="<%=strCurrSign%>"/>
		</tr>
		<tr>
			<td  height="15" colspan="<%=strColspan %>" align="right" class="RightTableCaption" >&nbsp; <fmtPrintPrice:message key="LBL_SGST_TOTAL"/></td>
			<gmjsp:currency textValue="<%=strSGSTTotal%>" gmClass="RightTableCaption"  type="BoldCurrTextSign" currSymbol="<%=strCurrSign%>"/>
		</tr>
		
	    <%}else{ %>
		<tr>
		
			<td  height="15" colspan="<%=strColspan %>" align="right" class="RightTableCaption" id="taxTotal">&nbsp; <fmtPrintPrice:message key="LBL_TAX_TOTAL"/></td>
			<gmjsp:currency textValue="<%=strTaxGrandTotal%>" gmClass="RightTableCaption"  type="BoldCurrTextSign" currSymbol="<%=strCurrSign%>"/>
		</tr>
		<%} %>
		<tr >
			<td  height="20" colspan="<%=strColspan %>" align="right" class="RightTableCaption" id="invTotal">&nbsp; <fmtPrintPrice:message key="LBL_INVOICE_TOTAL"/></td>
			<gmjsp:currency textValue="<%=strInvTotal%>" gmClass="RightTableCaption"  type="BoldCurrTextSign" currSymbol="<%=strCurrSign%>"/>
		</tr>
		<% 	}%>
<!-- </table> -->
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
