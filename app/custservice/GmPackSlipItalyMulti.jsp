
<%
	/**********************************************************************************
	 * File		 		: GmPackSlipItalyMulti.jsp
	 * Desc		 		: This screen is used for print Italy Pack Slip multiple times
	 * Version	 		: 1.0
	 * author			: Gopinathan
	 ************************************************************************************/
%>
<!-- \custservice\GmPackSlipItalyMulti.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java"%>
<%
	String strOpt = GmCommonClass.parseNull((String)request
			.getAttribute("hOpt"));
	String strSource = GmCommonClass.parseNull((String)request
			.getAttribute("SOURCE"));

	int packCount = 1;
	if( strSource.equals("shipout")){
		strOpt ="autoprint";
		String strPrintCnt = GmCommonClass.parseZero((String) request
				.getAttribute("PRINTCNT"));
		packCount = Integer.parseInt(strPrintCnt);
	}
%>


<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> -->
<!-- Hiding the DOcType: to show the jasper content with center alignment -->
<html>
<head>
<title>GlobusOne Enterprise Portal: Pack Slip</title>

<script>
var hopt = '<%=strOpt%>';
	function fnPrint() {
		window.print();
	}

	function hidePrintMain() {
		buttonTdObj = document.getElementById("buttonTd");
		buttonTd.style.display = 'none';

	}

	function showPrintMain() {

		buttonTdObj = document.getElementById("buttonTd");
		buttonTd.style.display = 'block';

	}
	function fnWPrint() {
		if (hopt == 'autoprint') {
			setTimeout(fnAutoPrint, 1);
		}
	}
	function fnAutoPrint() {
		var OLECMDID = 6;
		var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
		document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
		WebBrowser1.ExecWB(OLECMDID, 2);
		WebBrowser1.outerHTML = "";
	}
</script>
</head>
<BODY topmargin="10" onload="fnWPrint();"
	onbeforeprint="hidePrintMain();" onafterprint="showPrintMain();">
	<form>

		<%
			for (int i = 0; i < packCount; i++) {

				if (i != 0) {
		%>
		<p STYLE="page-break-after: always"></p>
		<%
			}
		%>
		<jsp:include page="/custservice/GmPackSlipItaly.jsp" />

		<%
			}
		%>



		<table border="0" width="700" cellspacing="0" cellpadding="0"
			align=center>
			<tr>
				<td align="center" height="30" width="700" id="buttonTd"><gmjsp:button
						value="&nbsp;Print&nbsp;" name="Btn_PrintMain" gmClass="button"
						onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp; <gmjsp:button
						value="&nbsp;Close&nbsp;" name="Btn_CloseMain" gmClass="button"
						onClick="window.close();" buttonType="Load" /></td>
			<tr>
		</table>
	</FORM>
</body>
</html>