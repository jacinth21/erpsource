
<%
	/*************************************************************************************************
	* File		 		: GmSalesBackOrderMail.jsp
	* Desc		 		: This screen is used for Loading Sales Back Order Details and Sending Emails.
	* Version	 		: 1.0
	* author			: HReddi
	************************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%> 	
<%@ taglib prefix="fmtBackOrdeEmail" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmBackOrderEmail.jsp -->
<fmtBackOrdeEmail:setLocale value="<%=strLocale%>"/>
<fmtBackOrdeEmail:setBundle basename="properties.labels.custservice.Shipping.GmShippingReport"/>
	<bean:define id="alBODetails" name="frmSalesBackOrderMail" property="alBODetails" type="java.util.List"> </bean:define>
	<bean:define id="alCompCurr" name="frmSalesBackOrderMail" property="alCompCurr" type="java.util.List"> </bean:define>
	<bean:define id="strAccCurrSymb" name="frmSalesBackOrderMail" property="accCurrSymb" type="java.lang.String"> </bean:define>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	 GmCalenderOperations gmCal = new GmCalenderOperations();
	String strApplDateFmt = strGCompDateFmt;
	String strCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	
	strCurrSign = strAccCurrSymb.equals("")?strCurrSign:strAccCurrSymb;
	
	String strCustServiceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strTodaysDate = gmCal.getCurrentDate(strGCompDateFmt)==null?"":gmCal.getCurrentDate(strGCompDateFmt);
	int intLength = alBODetails.size();
	String strWikiTitle = GmCommonClass.parseNull((String)GmCommonClass.getWikiTitle("BACK_ORDER_EMAIL"));
	String strPOColHead = "";
	String strShade = "";
	String strOrderId = "";
	String strParentOrderId = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strPoPendQty = "";
	int intAllCnt = 0;
	int intSize = 0;
	String strQtyPlaced = "";
	String strDate = "";
	String strOrdAmt = "";
	String strAccId = "";
	String strRepNm = "";
	String strNextRepId = "";
	String strRepID = "";
	
	String strNextId = "";
	String strNextRep = "";
	int intCount = 0;
	int intBOQty = 0;
	int intTemp = 0;
	String strLine = "";
	boolean blFlag = false;
	String strPartNumId = "";
	StringBuffer sbTemp = new StringBuffer();
	StringBuffer sbParts = new StringBuffer();
	String strOrdCallFlag = "";
	String strOrdCallFlagInv = "";
	String strTXNtype = "";
	String strCollapseImage = strImagePath+"/plus.gif";
	String strRepName = "";
	String strEmailDt = "";
	String strSalesRepIds = "";
	String strAttrId = "";
	String strHistoryFl = "";
	String strRepMailId = "";
	String strDisabled = "";
	ArrayList alDist = new ArrayList();
	ArrayList alSales = new ArrayList();
	ArrayList alPoEmailType = new ArrayList();
	
	
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Sales BackOrder Email</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE> Globus Medical: Lot Tracking Report </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strCustServiceJsPath%>/GmBackOrderEmail.js"></script>
<script>
	var dateFmt = '<%=strApplDateFmt%>';
	var todaysDate = '<%=strTodaysDate%>';
	var recordSize = '<%= intLength%>';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10"">
<html:form action="/gmBackOrderMailAction.do?method=loadBackOrderReport">
	<html:hidden property="strOpt" name="frmSalesBackOrderMail"/>
	<html:hidden property="hInputString" name="frmSalesBackOrderMail"/>
	<input type="hidden" name="hPartNums" value="">
	<table class="DtTable1200" border="0" cellspacing="0" cellpadding="0" >
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="5">&nbsp;<fmtBackOrdeEmail:message key="LBL_SALES_BACKORDER_EMAIL"/></td>
			<fmtBackOrdeEmail:message key="LBL_HELP" var="varHelp"/>
			<td align="right" class=RightDashBoardHeader><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>					
		<tr><td class="LLine" height="1" colspan="6"></td></tr>	
		<tr>
		<fmtBackOrdeEmail:message key="LBL_SALES_REP" var="varSalesRep"/>
			<td class="RightTableCaption" height="25" align="right" width="15%">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="&nbsp;${varSalesRep}:" td="false"/></td>
			<td style="width: fit-content">&nbsp;<gmjsp:dropdown controlName="distType" SFFormName="frmSalesBackOrderMail" SFSeletedValue="distType" defaultValue= "[Choose One]" 
								SFValue="alDistributorType" codeId="REPID" codeName="NAME"/>
						</td>
						<fmtBackOrdeEmail:message key="LBL_DATE_RANGE" var="varDateRange"/>
			<td class="RightTableCaption" height="25" width="10%" align="right">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varDateRange}:" td="false"/></td>
			<td class="RightTableCaption" width="30%">&nbsp;<fmtBackOrdeEmail:message key="LBL_FROM"/>:&nbsp;<gmjsp:calendar SFFormName="frmSalesBackOrderMail"  controlName="strFromDate" tabIndex="4"  gmClass="InputArea" 
					onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;&nbsp;<fmtBackOrdeEmail:message key="LBL_TO"/>:&nbsp;<gmjsp:calendar SFFormName="frmSalesBackOrderMail" controlName="strToDate"  gmClass="InputArea" 
					onFocus="changeBgColor(this,'#AACCE8');"  tabIndex="5" onBlur="changeBgColor(this,'#ffffff');"/></td>	
					<fmtBackOrdeEmail:message key="BTN_LOAD" var="varLaod"/>				
			<td height="25" colspan="2">&nbsp;<gmjsp:button value="&nbsp;${varLaod}&nbsp;" style="width: 4em;height: 23px" tabindex="14" name="Btn_Load" gmClass="button" onClick="fnLoad();" buttonType="Load" /></td>
		</tr>
		<tr><td class="LLine" height="1" colspan="6"></td></tr>
		<tr class="shade">
		<fmtBackOrdeEmail:message key="LBL_EMAIL" var="varEmail"/>
			<td class="RightTableCaption" height="25" align="right" width="15%">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="&nbsp;${varEmail}:" td="false"/></td>
			<td colspan="1">&nbsp;<gmjsp:dropdown controlName="emailType" SFFormName="frmSalesBackOrderMail" SFSeletedValue="emailType" defaultValue= "[Choose One]" 
							SFValue="alEmail" codeId="CODEID" codeName="CODENM"/>
			</td> 
			<td class="RightTableCaption" HEIGHT="25" align="right" width="5%">
				<fmtBackOrdeEmail:message key="LBL_CURRENCY"/>:
			</td>
			<td colspan="3">&nbsp;
				<gmjsp:dropdown controlName="accCurrId" SFFormName="frmSalesBackOrderMail" SFSeletedValue="accCurrId"
							SFValue="alCompCurr" codeId = "ID"  codeName = "NAMEALT" optionId="NAME" />
			</td>
		</tr>
		
		<logic:equal name="frmSalesBackOrderMail" property="strOpt" value="load">
		
			<tr><td colspan="9" height="1" class="LLine"></td></tr>
			<tr>
				<td colspan="9">
					<table border="0"  width="100%" cellspacing="0" cellpadding="0">
						<tr class="ShadeRightTableCaption">
							<td width="70" height="24"><input type="checkbox"  name="chkInvlookupAll" onclick="javascript:fnSelectAll(this);"/> <fmtBackOrdeEmail:message key="LBK_REP"/></td>
							<td width="99"><fmtBackOrdeEmail:message key="LBL_BACK_ORDER_ID"/></td>
							<td width="100"><fmtBackOrdeEmail:message key="LBL_DO_ID"/></td>
							<td width="150"><fmtBackOrdeEmail:message key="LBL_ACCOUNT"/></td>
							<td width="70"><fmtBackOrdeEmail:message key="LBL_PART"/></td>							
							<td width="40"><fmtBackOrdeEmail:message key="LBL_BO_QTY"/></td>
							<td width="40"><fmtBackOrdeEmail:message key="LBL_QTY_IN_PO"/></td>
							<td width="60"><fmtBackOrdeEmail:message key="LBL_ORDER_DATE"/></td>
							<td width="80"><fmtBackOrdeEmail:message key="LBL_AMOUNT"/></td>
							<td width="150"><fmtBackOrdeEmail:message key="LBL_SENT_EMAIL"/></td>
						</tr>
<%
						if (intLength > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();						
							intSize = alBODetails.size();
							hmTempLoop = new HashMap();
							strLine = "";
							blFlag = false;
	
							for (int i = 0;i < intSize ;i++ ){
								hmLoop = (HashMap)alBODetails.get(i);
								if (i<intSize-1){
									hmTempLoop = (HashMap)alBODetails.get(i+1);
									strNextRepId = GmCommonClass.parseNull((String)hmTempLoop.get("REPID"));
								}
	
								strOrderId = GmCommonClass.parseNull((String)hmLoop.get("ID"));
								strParentOrderId = GmCommonClass.parseNull((String)hmLoop.get("MASTER_ID"));
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("NUM"));							
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("DSC"));
								strQtyPlaced = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
								strPoPendQty = GmCommonClass.parseNull((String)hmLoop.get("POPEND"));
								intBOQty = Integer.parseInt(strQtyPlaced);
								strDate = GmCommonClass.getStringFromDate((java.util.Date)hmLoop.get("DT"),strApplDateFmt);
								strOrdAmt = GmCommonClass.parseNull((String)hmLoop.get("SALES"));
								strAccId = GmCommonClass.parseNull((String)hmLoop.get("ANAME"));
								strRepNm = GmCommonClass.parseNull((String)hmLoop.get("SALES_REP_NAME"));
								strRepID = GmCommonClass.parseNull((String)hmLoop.get("REPID"));	
								strOrdCallFlag =  GmCommonClass.parseNull((String)hmLoop.get("LOG_FLAG"));
								strOrdCallFlagInv =  GmCommonClass.parseNull((String)hmLoop.get("INT_LOG_FLAG"));
								strTXNtype = GmCommonClass.parseNull((String)hmLoop.get("TXNTYPE"));
								strEmailDt = GmCommonClass.parseNull((String)hmLoop.get("EMAIL_DATE"));
								strHistoryFl = GmCommonClass.parseNull((String)hmLoop.get("HISTFL"));
								strAttrId = GmCommonClass.parseNull((String)hmLoop.get("ATTRID"));
								strRepMailId = GmCommonClass.parseNull((String)hmLoop.get("EMAIL"));
								/* if(strRepMailId.equals("")){
									strDisabled = "disabled = true";
								}else{
									strDisabled = "";
								} */
								intTemp = intTemp + intBOQty;							
								if (strRepID.equals(strNextRepId)){
									intCount++;
									strLine = "<TR><TD colspan=12 height=1 class=Line></TD></TR>";
								}else{
									strSalesRepIds = strSalesRepIds+strRepID+',';
									sbTemp.append("document.all.td"+strRepID+".innerHTML = '<b>"+intTemp+"</b>';");
									intTemp = 0;
									strLine = "<TR><TD colspan=12 height=1 class=Line></TD></TR>";
									if (intCount > 0){
										strPartDesc = "";
										strLine = "";
									}else{
										blFlag = true;
									}
									intCount = 0;
								}
								
								if(i == intSize-1){ // To append the last salesrep id
									strSalesRepIds = strSalesRepIds+strRepID+',';
								}
	
								if (intCount > 1){
									//strRepNm = "";
									strLine = "";
								}
							
								strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
	
								out.print(strLine);	
								if (intCount == 1 || blFlag){
									sbParts.append(strRepID);
									sbParts.append(",");
%>
	
						<tr id="tr<%=strRepID%>">		
							<td class="RightText" colspan="5">
							<input type="checkbox" id="<%=strRepID%>" name="divSelectAll" onclick="javascript:fnSelectAllSub('<%=strRepID%>',this);"/>&nbsp;<A class="RightText" title="Click to Expand" href="javascript:Toggle('<%=strRepID%>')"><%=strRepNm%></td>
							<td colspan="5" id="td<%=strRepID%>" class="RightText">&nbsp;</td><input type="hidden" value="<%=strRepID%>" id="hPartNumID<%=intAllCnt%>" /> 
							<%-- <td colspan="3" class="RightText" ><%=strRepMailId %></td> --%>
						</tr>
							<tr>
								<td colspan="11"><div style="display:none" id="div<%=strRepID%>">
									<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tab<%=strRepID%>">
<%
									intAllCnt++;
									blFlag = false;
									}
%>					<TR <%=strShade%>>
						<td width="20" class="RightText"></td>
						<td width="50"><input type="checkbox" name="divSelectOrder<%=strRepID%>" id="<%=strOrderId%>" onclick="javascript:fnSelectMasterChkBox(this,'<%=strRepID%>');"/></td>	
						<td width="99" class="RightText" align="left">&nbsp;<%=strOrderId%></td>	
						<td width="100" class="RightText" align="left">&nbsp;<%=strParentOrderId%></td>
						<td width="150" class="RightText" align="left">&nbsp;<%=strAccId%></td>	
						<td width="75" class="RightText" align="left">&nbsp;<%=strPartNum%></td>						
						<td width="40" class="RightText">&nbsp;<%=intBOQty%></td>	
						<td width="40" class="RightText">&nbsp;<%=strPoPendQty%></td>	
						<td width="60" class="RightText" align="center">&nbsp;<%=strDate%></td>	
						<td width="80" class="RightText">&nbsp;<%=strCurrSign%><%=GmCommonClass.getStringWithCommas(strOrdAmt)%>&nbsp;</td>
						<td width="20" class="RightText">
						<%if(strHistoryFl.equals("Y")){ %>
						<fmtBackOrdeEmail:message key="LBL_CLICKTPVIEWHISTORY" var="varClickToHistory"/>
						<a href="#" title="${varClickToHistory}" onclick="javascript:fnReqHistory('<%=strAttrId %>')"><img alt="${varClickToHistory}" src="<%=strImagePath%>/icon_History.gif" width="18" height="18" border="0"></a>
						<%} %></td>	
						<td width="130" class="RightText" align="left">&nbsp;<%=strEmailDt %></td>		
					</TR>
									<tr><td colspan="9" height="1" bgcolor="#eeeeee"></td></tr>
<%					
								if ( intCount == 0 || i == intSize-1)
								{
									if (i+1==intSize)
									{	 						
										sbTemp.append("document.all.td"+strRepID+".innerHTML = '<b>"+intTemp+"</b>';");
									}
%>
								</table></div>
							</td>
						</tr>
<%
								}
								if(intSize ==1 && intTemp==0){intTemp =1; sbTemp.setLength(0); sbTemp.append("document.all.td"+strRepID+".innerHTML = '<b>"+intTemp+"</b>';"); 
								sbTemp.append("document.all.td"+strRepID+".innerHTML = '<b>"+intTemp+"</b>';");}
								
							} // End of For
								//sbTemp.append("document.all.hPartNums.value = '"+sbParts.toString()+"';");
								out.print("<script>");
								
								out.print(sbTemp.toString());
								out.print("</script>");
								 
						}else{
%>									<tr>
										<td colspan="11" align="center" class="RightTextRed">There are no Sales BackOrders !</td>
									</tr>
<%
						}
%>
					</TABLE>
				</TD>
			</tr>
			<input type="hidden" value="<%=strSalesRepIds%>" id="hSalesRepIDs" />
			<tr><td colspan="11" height="1" class="LLine"></td></tr>
			<tr>
			<fmtBackOrdeEmail:message key="BTN_SEND_EMAIL" var="varSentEmail"/>
				<td colspan="7" align="center" height="30"><gmjsp:button value="${varSentEmail}" gmClass="button" onClick="fnSubmit();" tabindex="13" buttonType="Save" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
		</logic:equal>
	</table>
		
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>