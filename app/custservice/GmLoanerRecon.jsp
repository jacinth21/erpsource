 <%
/**********************************************************************************
 * File		 		: GmLoanerRecon.jsp
 * Desc		 		: This screen is used for Loaner Reconciliation
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%> 	
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtLoanerRecon" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmLoanerRecon.jsp -->
<fmtLoanerRecon:setLocale value="<%=strLocale%>"/>
<fmtLoanerRecon:setBundle basename="properties.labels.custservice.GmLoanerRecon"/>  

<%
String strCustServiceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	GmCalenderOperations gmCal = new GmCalenderOperations();
	String strWikiTitle = GmCommonClass.getWikiTitle("RECONCILIATION");
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");
	String strTransId = (String)request.getAttribute("hTransId") == null?"":(String)request.getAttribute("hTransId");
	String strMsg = (String)request.getAttribute("strMsg") == null?"":(String)request.getAttribute("strMsg");
	String strChecked = "";
	String strSetId = "";
	String strDesc = "";
	String strTemp = "";
	String strShade = "";
	String strConsignId = "";
	String strType =  "";
	String strPurpose = "";
	String strUserName = "";
	String strIniDate = "";
	String strControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strConsignType = "";

	String strRequestorName = "";
	String strmissqty = "";
	String strSetCsgQty = "";
	String strQtyRef = "";
	String strTxnId = "";
	String strdisabled = "";

	String strPrice = "";
	String strTransType = "";
	String strItemQty = "";
	String strStatusFl = "";
	String strShipFl = "";
	String strSetName = "";
	String strSetTypeId = "";
	String strEtchId="";
	String strReconTransXmlData = "";
	String strReconXmlData = "";
	String strSelectedAction = "";
	
	String strAccessFl = "";
	String strDisable = "";
	ArrayList alCboAction = new ArrayList();
	ArrayList alSetLoad = new ArrayList();
	HashMap hmConsignDetails = new HashMap();

	strAccessFl = GmCommonClass.parseNull((String)request.getAttribute("ACCFL"));
	String strApplDateFmt = strGCompDateFmt;
	String strTodaysDate = gmCal.getCurrentDate(strApplDateFmt)==null?"":gmCal.getCurrentDate(strApplDateFmt); 
	
	int intPriceSize = 0;

	if (hmReturn != null)
	{
			alCboAction = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALCBOACTION"));
			strSetId = (String)session.getAttribute("hSetId") == null?"":(String)session.getAttribute("hSetId");
		
			hmConsignDetails = (HashMap)hmReturn.get("CONDETAILS");

			strReconXmlData = GmCommonClass.parseNull((String)hmReturn.get("RECON_XMLDATA"));
			strReconTransXmlData = GmCommonClass.parseNull((String)hmReturn.get("RECONTRAN_XMLDATA"));
			strSelectedAction = GmCommonClass.parseNull((String)hmReturn.get("CBO_ACTION"));
			
			strConsignId = GmCommonClass.parseNull((String)hmConsignDetails.get("CONSIGNID"));
			strTxnId = GmCommonClass.parseNull((String)hmConsignDetails.get("CID"));
			strSetName=  GmCommonClass.parseNull((String)hmConsignDetails.get("SNAME"));
			strPurpose =  GmCommonClass.parseNull((String)hmConsignDetails.get("PURP"));
			strType =  GmCommonClass.parseNull((String)hmConsignDetails.get("TYPE"));
			strUserName =  GmCommonClass.parseNull((String)hmConsignDetails.get("UNAME"));
			strIniDate =  GmCommonClass.parseNull((String)hmConsignDetails.get("CDATE"));
			strDesc =  GmCommonClass.parseNull((String)hmConsignDetails.get("COMMENTS"));
			strStatusFl =  GmCommonClass.parseNull((String)hmConsignDetails.get("SFL"));
			strTransType =  GmCommonClass.parseNull((String)hmConsignDetails.get("CTYPE"));
			strRequestorName =  GmCommonClass.parseNull((String)hmConsignDetails.get("RNAME"));
			strSetTypeId = GmCommonClass.parseNull((String)hmConsignDetails.get("SETCTYPEID"));
			strEtchId = GmCommonClass.parseNull((String)hmConsignDetails.get("ETCHID"));

	} 
ArrayList alAction = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ORDSTATUS"));
	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE>Globus Medical:Loaner Reconciliation </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="STYLESHEET" type="text/css" href="<%=strCssPath%>/displaytag.css"></link>
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">


<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>
<script language="javascript" src="<%=strJsPath%>/GmGrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlx.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strCustServiceJsPath%>/GmLoanerRecon.js"></script>

<script>
var cnt = 0;
var gridObj;
var format = '<%=strApplDateFmt%>';
var todaysDate = '<%=strTodaysDate%>';
var objReconData;
var objReconTransData;
var rowID = '';
objReconData = '<%=strReconXmlData%>';
objReconTransData = '<%=strReconTransXmlData%>';
var reconGridObj = '';
var reconTransGridObj = '';
var transType = '<%=strTransType%>';
var accessFl  = '<%=strAccessFl%>'
</script>


</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmLoanerReconServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hConsignId" value="<%=strTransId%>">
<input type="hidden" name="hId" value="<%=strTxnId%>">
<input type="hidden" name="hStatusFl" value="<%=strStatusFl%>">
<input type="hidden" name="hConsignType" value="<%=strConsignType%>">
<input type="hidden" name="hTxnType" value="<%=strTransType%>">
<input type="hidden" name="hInputStr" value ="">
<input type="hidden" name="hInputConStr" value ="">
<input type="hidden" name="hInputWriteStr" value ="">
<input type="hidden" name="hInputRetStr" value ="">
<input type="hidden" name="hInputihStr" value ="">
<input type="hidden" name="hRecDoStr" value ="">
<html:hidden property="hTxnId" value="" />
<html:hidden property="hTxnName" value="" />
<html:hidden property="hCancelType" value="VFAFSD" />
<html:hidden property="hLogReason" value="" />
<input type="hidden" name="hRedirectURL" />
<input type="hidden" name="hDisplayNm" />

<xml src="<%=strXmlPath%>/<%=strSetTypeId%>.xml" id="xmldso" async="false"></xml>
	<table border="0" class="dttable900" cellspacing="0" cellpadding="0">
		<tr>
			<%if (strSetTypeId.equals("4127")){%>
				<td height="25" class="RightDashBoardHeader" colspan="1"><fmtLoanerRecon:message key="LBL_RECONCILIATION_PRODUCT_LOANER"/></td>
			<%}else if (strSetTypeId.equals("4119")){ %>
				<td height="25" class="RightDashBoardHeader" colspan="1"><fmtLoanerRecon:message key="LBL_RECONCILIATION_IN_HOUSELOANER"/></td>
			<%}else{ %>
				<td height="25" class="RightDashBoardHeader" colspan="1"><span datasrc="#xmldso" datafld="TITLE-REFILL"></span><fmtLoanerRecon:message key="LBL_RECONCILIATION"/></td>
			<%} %>
			<td height="25" class="RightDashBoardHeader">
			<fmtLoanerRecon:message key="IMG_HELP" var="varHelp"/>
				    <img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
			       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> </td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="2"></td></tr>
		<tr>
			<td height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<%if(strTransType.equals("50159")){ %>
					<tr>
						<td><jsp:include
						page="/custservice/GmLoanerReconHeader.jsp">
						<jsp:param name="FORMNAME" value="frmVendor" />
						</jsp:include>
						</td>
					</tr>
					<%}else if(strTransType.equals("9112")){ %>
					<tr>
						<td><jsp:include
						page="/custservice/GmIHLoanerReconHeader.jsp">
						<jsp:param name="FORMNAME" value="frmVendor" />
						</jsp:include>
						</td>
					</tr>
					
					<%}else{ %>
					<tr>
						<td><jsp:include
						page="/custservice/GmPLLoanerReconHeader.jsp">
						<jsp:param name="FORMNAME" value="frmVendor" />
						</jsp:include>
						</td>
					</tr>
					<%} %>
					<tr>
						<td><jsp:include
						page="/custservice/GmLoanerReconciledDetails.jsp">
						<jsp:param name="FORMNAME" value="frmVendor" />
						</jsp:include>
						</td>
					</tr>	
					<%if(strTransType.equals("50159")) {%>
					<tr>
						<td><jsp:include 
						page="/custservice/GmDOItems.jsp" >
						<jsp:param name="FORMNAME" value="frmVendor" />
						</jsp:include>
						</td>
					</tr>
					<%} %>
					<tr>				
						<td><jsp:include
						page="/custservice/GmReconCiled.jsp">
						<jsp:param name="FORMNAME" value="frmVendor" />
						</jsp:include>
						</td>
					</tr>
					<tr>
						<td><jsp:include 
						page="/common/GmIncludeLog.jsp" >
						<jsp:param name="EXPAND_COLLAPSE" value="Y" />
						<jsp:param name="LogMode" value="Edit" />																					
						</jsp:include>	
						</td>
					</tr>
					<% if(strAccessFl.equals("Y")){ %>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
						<td colspan="2" height="30" align="center">
						<b><fmtLoanerRecon:message key="LBL_CHOOSE_ACTION"/>:</b>&nbsp;<gmjsp:dropdown
							controlName="Cbo_action" controlId="Cbo_action" seletedValue="<%=strSelectedAction%>"
							value="<%=alCboAction %>" codeId="CODEID" codeName="CODENM" tabIndex="11"
							defaultValue="[Choose One]" /> &nbsp;
							<fmtLoanerRecon:message key="BTN_SUBMIT" var="varSubmit"/>
						<gmjsp:button value="${varSubmit}" controlId="Btn_Submit" disabled="<%=strDisable%>" style="width: 7em;height: 23" gmClass="button" onClick="fnSubmit();" tabindex="12" buttonType="Save" />&nbsp;&nbsp;
						</td>
					</tr> 
					<%} %>
				<% if (!strMsg.equals("")){ %>
					<tr>
						<td colspan="2" >
							<font color="red" ><fmtLoanerRecon:message key="LBL_IN_HOUSE_TRANSACTION"/> 
				   <%
					strMsg = strMsg.substring(0, strMsg.length() - 1);	
					String[] arryInHouseTxn = strMsg.split(",");
					for(int i=0	;i<arryInHouseTxn.length;i++){
						log.debug("arryInHouseTxn.length="+arryInHouseTxn.length);
						String strInHouseTxn = arryInHouseTxn[i];
						strInHouseTxn=strInHouseTxn.trim();
					%>
					<b><a href="javascript:fnPicSlip('<%=strInHouseTxn%>','100064','<%=strTxnId%>')"><%=strInHouseTxn%></b></a>
					<%if(i!= arryInHouseTxn.length - 1){ %>,<%}%>
					<%} %> 
						
							<fmtLoanerRecon:message key="LBL_IN_HOUSE_AVAILABLE_MESSAGE"/></font>
						</td>
					</tr>
				<% } %>
				</table>
			 </td>
		</tr>
    </table>
</FORM>
</BODY>
<%@ include file="/common/GmFooter.inc"%>
</HTML>
