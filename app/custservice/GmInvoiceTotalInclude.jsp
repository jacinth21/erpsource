<%
/**********************************************************************************
 * File		 		: GmInvoiceTotalInclude.jsp
 * Desc		 		: This screen is used to display the Invoice Total section
 * Version	 		: 1.0
 * author			: 
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@page import="java.util.Date"%>
<%@ taglib prefix="fmtPrintPrice" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmInvoiceTotalInclude.jsp -->
<fmtPrintPrice:setLocale value="<%=strLocale%>"/>
<fmtPrintPrice:setBundle basename="properties.labels.custservice.GmPrintPrice"/>
<%
String strAccCurrSymb="";
HashMap hmReturn = new HashMap();
HashMap hmOrderDetails = new HashMap();
hmReturn = (HashMap)request.getAttribute("hmReturn");
String strColspan = GmCommonClass.parseNull((String)request.getParameter("COLSPAN"));
String strOrderSource = GmCommonClass.parseNull((String)request.getParameter("ORDERSOURCE"));
String strSessDeptId = GmCommonClass.parseNull((String)request.getParameter("STRSESSDEPTID"));
String strhidePartPrice = GmCommonClass.parseNull((String)request.getParameter("STRHIDEPARTPRICE"));
String strCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
String strSubTotal = GmCommonClass.parseNull((String)request.getParameter("SUBTOTAL"));
String strTaxGrandTotal = GmCommonClass.parseNull((String)request.getParameter("GRANDTOTAL"));
String strInvTotal = GmCommonClass.parseNull((String)request.getParameter("INVTOTAL"));
String strShipCharges = GmCommonClass.parseNull((String)request.getParameter("SHIPCHARGES"));

String strWidth = "";
boolean blDOFromApp = false;
blDOFromApp = (strOrderSource.equals("103521"))? true:blDOFromApp;

double dblOrderTotalminusShipCharge =  
									Double.parseDouble(GmCommonClass.parseZero(strInvTotal)) -
								  (Double.parseDouble(GmCommonClass.parseZero(strShipCharges))+
									Double.parseDouble(GmCommonClass.parseZero(strTaxGrandTotal)))

									;

String  strOrderTotalMinusShipCharge = dblOrderTotalminusShipCharge+"";
if (hmReturn != null)
{
	hmOrderDetails = (HashMap)hmReturn.get("ORDERDETAILS");
	strAccCurrSymb = GmCommonClass.parseNull((String)hmOrderDetails.get("ACC_CURR_SYMB"));
}
strCurrSign = strAccCurrSymb.equals("")? strCurrSign: strAccCurrSymb;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Order Summary</TITLE>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>	
<script>

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" >
<FORM name="frmOrder" method="post" action="<%=strServletPath%>/GmOrderItemServlet">
<%-- <% if(strClientSysType.equals("IPAD")){%>
	<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
<% }else{%>
	<table class="width:100%"  border="1" cellspacing="0" cellpadding="0">
<%} %> --%>

<% 
		strColspan = (blDOFromApp)? ((Integer.parseInt(strColspan)+2)+"") :strColspan;
		strWidth =  (blDOFromApp)? "" : "87%";
		if(strSessDeptId.equals("S")){
		 	if(!strhidePartPrice.equals("N")){ 
%>
				<tr><td colspan="15" height="1" bgcolor="#666666"></td></tr>								
				<tr>
					<td colspan="15">
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td  height="20" width="87%" align="right" class="RightTableCaption" id="grdTotal">&nbsp; <fmtPrintPrice:message key="LBL_GRAND_TOTAL"/> </td>
								<gmjsp:currency textValue="<%=strSubTotal%>" gmClass="RightTableCaption"  type="BoldCurrTextSign" currSymbol="<%=strCurrSign %>"/>
							</tr>
						</table>
					</td>
				</tr>
			<%}else{ %>
			<%}
		 }else{ %>
			<tr><td colspan="15" height="1" bgcolor="#666666"></td></tr>								
			<tr >
				<td  height="20" width="<%=strWidth %>" colspan="<%=strColspan %>" align="right" class="RightTableCaption" id="grdTotal1">&nbsp; <fmtPrintPrice:message key="LBL_SUB_TOTAL"/></td>
				<gmjsp:currency textValue="<%=strSubTotal%>" gmClass="RightTableCaption" type="BoldCurrTextSign" currSymbol="<%=strCurrSign %>"/>
		<%} %>
		<% if(!strSessDeptId.equals("S"))
			{
			
			// Modified for PMT-42836
			//When we have colspan=14 we have alignment issue,and for iPad Order there is no need to have diff colspan,hence hardcoding to 12.
			strColspan ="12";
			 //strColspan = (blDOFromApp)?"14":"12";								
		%>
		<tr >
			<td  height="15" colspan="<%=strColspan %>" align="right" class="RightTableCaption" id="orderTotal">&nbsp; <fmtPrintPrice:message key="LBL_ORDER_TOTAL_BEFORE_TAX"/></td>
			<gmjsp:currency textValue="<%=strOrderTotalMinusShipCharge%>" gmClass="RightTableCaption"  type="BoldCurrTextSign" currSymbol="<%=strCurrSign%>"/>
		</tr>
		
		<tr >
			<td  height="15" colspan="<%=strColspan %>" align="right" class="RightTableCaption" id="taxTotal">&nbsp; <fmtPrintPrice:message key="LBL_TAX_TOTAL"/></td>
			<gmjsp:currency textValue="<%=strTaxGrandTotal%>" gmClass="RightTableCaption"  type="BoldCurrTextSign" currSymbol="<%=strCurrSign%>"/>
		</tr>
		<tr >
			<td  height="20" colspan="<%=strColspan %>" align="right" class="RightTableCaption" id="invTotal">&nbsp; <fmtPrintPrice:message key="LBL_INVOICE_TOTAL"/></td>
			<gmjsp:currency textValue="<%=strInvTotal%>" gmClass="RightTableCaption"  type="BoldCurrTextSign" currSymbol="<%=strCurrSign%>"/>
		</tr>
		<% 	}%>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
