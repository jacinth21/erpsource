 <%
/**********************************************************************************
 * File		 		: GmPartNumSearch.jsp
 * Desc		 		: This screen is used for the DashBoard Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.Date"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtPartNumSearch" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- custservice\GmPartNumSearch.jsp -->
<fmtPartNumSearch:setLocale value="<%=strLocale%>"/>
<fmtPartNumSearch:setBundle basename="properties.labels.custservice.GmPartNumSearch"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
 
	String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strPartNum = (String)request.getAttribute("hPartNum")==null?"":(String)request.getAttribute("hPartNum");
	String strControl = (String)request.getAttribute("hCNum")==null?"":(String)request.getAttribute("hCNum");
	String strFrmDate = (String)request.getAttribute("hFrom")==null?"":(String)request.getAttribute("hFrom");
	String strToDate = (String)request.getAttribute("hTo")==null?"":(String)request.getAttribute("hTo");
	String strApplDateFmt = strGCompDateFmt;
	
	
	Date dtFrmDate = null;
	Date dtToDate = null;
	
	dtFrmDate = (Date)GmCommonClass.getStringToDate(strFrmDate,strApplDateFmt);
	dtToDate = (Date)GmCommonClass.getStringToDate(strToDate,strApplDateFmt);
	

	ArrayList alSoldReport = new ArrayList();
	ArrayList alConsignReport = new ArrayList();
	ArrayList alReturnReport = new ArrayList();
	ArrayList alQuarantineReport = new ArrayList();
	
	if (hmReturn != null) 
	{
		alSoldReport = (ArrayList)hmReturn.get("SALESREPORT");
		alConsignReport = (ArrayList)hmReturn.get("CONSIGNREPORT");
		alReturnReport = (ArrayList)hmReturn.get("RETURNREPORT");
		alQuarantineReport = (ArrayList)hmReturn.get("QUARANTINEREPORT");
	}

	int intSoldLength = alSoldReport.size();
	int intConsignLength = alConsignReport.size();
	int intReturnLength = alReturnReport.size();
	int intQuaranLength = alQuarantineReport.size();

	HashMap hmLoop = new HashMap();

	String strId = "";
	String strDesc = "";
	String strAcctName = "";
	String strQty = "";
	String strReportType = "";

	String strShade = "";
	String strDistName = "";
	String strSetItemFl = "";

	HashMap hmTempLoop = null;
	String strNextId = "";
	int intCount = 0;
	String strLine = "";
	boolean blFlag = false;
	String strPartNumId = "";
	int intTotal = 0;
	String strTotal = "";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Number Search</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmPartNumSearch.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>
//declaring company info
var compyid=null;
var appleDateFmt=null;
var plantid=null;
var partyid=null;
var cmplangid=null;
try{
	compyid=self.opener.top.compid;
	appleDateFmt=self.opener.top.applDateFmt;
	plantid=self.opener.top.plantid;
	partyid=self.opener.top.partyid;
	cmplangid=self.opener.top.cmplangid;
}catch(err){
	compyid=null;
	appleDateFmt=null;
	plantid=null;
	partyid=null;
	cmplangid=null;
}
var dateFmt = (appleDateFmt != null && appleDateFmt != undefined )?appleDateFmt:'<%=strApplDateFmt%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmPartNumSearchServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hDtFmt" value="">
<input type="hidden" name="hCompanyId" value="">

	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="15" width="1" class="Line"></td>
			<td height="1" bgcolor="#666666"></td>
			<td rowspan="15" width="1" class="Line"></td>
		</tr>
		<tr>
			<td height="25" class="RightDashBoardHeader">
				<fmtPartNumSearch:message key="TD_PART_TRANS_SEARCH_HEADER"/> 
			</td>
		</tr>
		<tr><td class="Line" height="1"></td></tr>
		<tr>
			<td>
				<table width="100%" cellpadding="0" cellspacing="1" border="0">
					<tr>
						<td align="right" class="RightText" HEIGHT="24" width="120"><fmtPartNumSearch:message key="LBL_PART_NUMS"/>:</td>
						<td  class="RightText">&nbsp;<input type="text" size="30" value="<%=strPartNum%>" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3>
						&nbsp;&nbsp;&nbsp;&nbsp;<fmtPartNumSearch:message key="LBL_CTRL_NUM"/>:&nbsp;<input type="text" size="15" value="<%=strControl%>" name="Txt_CNum" class="InputArea"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3>
						</td>
						<fmtPartNumSearch:message key="BTN_LOAD" var="varLoad"/>
						<td width="100" rowspan="3">&nbsp;<gmjsp:button value="&nbsp;&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;&nbsp;" name="Btn_Submit" gmClass="button" onClick="javascript:fnLoad();" buttonType="Load" /></td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
						
					<tr>
						<td align="right" class="RightText" HEIGHT="24"><fmtPartNumSearch:message key="LBL_FROM_DATE"/>:</td>
						<td class="RightText">&nbsp;
						 <gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtFrmDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="2"/>
			&nbsp;&nbsp;<fmtPartNumSearch:message key="LBL_TO_DATE"/>: <gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=dtToDate==null?null:new java.sql.Date(dtToDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="3"/>&nbsp;&nbsp;
					 	</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td class="Line" height="1"></td></tr>
		<tr><td height="25" class="RightDashBoardHeader">&nbsp;<fmtPartNumSearch:message key="LBL_SOLD"/></td></tr>
		<tr>
			<td>
				<table border="0" width="698" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td width="100">&nbsp;<fmtPartNumSearch:message key="LBL_PART_NUM"/></td>
						<td width="50"><fmtPartNumSearch:message key="LBL_DESC"/></td>
						<td width="300"><fmtPartNumSearch:message key="LBL_ACCT_NM"/></td>
						<td HEIGHT="24" align="center" width="50"><fmtPartNumSearch:message key="LBL_QTY"/></td>
					</tr>
<%
			if (intSoldLength > 0)
			{
				hmTempLoop = new HashMap();
				intCount = 0;
				strLine = "";
				blFlag = false;

				for (int i = 0;i < intSoldLength ;i++ )
				{
					hmLoop = (HashMap)alSoldReport.get(i);

					if (i<intSoldLength-1)
					{
						hmTempLoop = (HashMap)alSoldReport.get(i+1);
						strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("PARTNUM"));
					}
					strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PARTNUM"));					
					if(strPartNum.indexOf(".") != -1){
						strPartNumId = strPartNum.replaceAll("\\.","");
					}else{
						strPartNumId = strPartNum;
					}					
					strDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
					strAcctName = GmCommonClass.parseNull((String)hmLoop.get("ANAME"));
					strId = GmCommonClass.parseNull((String)hmLoop.get("ACCID"));
					strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
					intTotal = intTotal + Integer.parseInt(strQty);

					if (strPartNum.equals(strNextId))
					{
						intCount++;
						strLine = "<TR><TD colspan=4 height=1 class=Line></TD></TR>";
					}
					else
					{
						strLine = "<TR><TD colspan=4 height=1 class=Line></TD></TR>";
						if (intCount > 0)
						{
							strDesc = "";
							strLine = "";
						}
						else
						{
							blFlag = true;
						}
						intCount = 0;
						//
					}

					if (intCount > 1)
					{
						strDesc = "";
						strLine = "";
					}
				
					strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows

					out.print(strLine);
					if (intCount == 1 || blFlag)
					{
%>
					<tr id="trs<%=strPartNumId%>">
						<td width="100" height="20" class="RightText">&nbsp;<A class="RightText" title="Click to Expand" href="javascript:Toggle('s<%=strPartNumId%>')"><%=strPartNum%></a></td>
						<td colspan="2" width="550" class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
						<td width="50" class="RightText" id="tds<%=strPartNumId%>" align="center"></td>
					</tr>
					<tr>
						<td colspan="4"><div style="display:none" id="divs<%=strPartNumId%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tabs<%=strPartNumId%>">

<%
								blFlag = false;
								}
%>
					<tr <%=strShade%>>
						<td width="100" class="RightText">&nbsp;</td>
						<td width="50" class="RightText">&nbsp;</td>
						<td width="300" height="20">&nbsp;<a class="RightText" href="javascript:fnCallDrill('<%=strPartNum%>','<%=strId%>','S','');"><%=strAcctName%></a></td>
						<td width="50" class="RightText" align="right"><%=strQty%>&nbsp;</td>
					</tr>
					<tr><td colspan="4" height="1" bgcolor="#eeeeee"></td></tr>
<%					
					if ( intCount == 0 || i == intSoldLength-1)
					{
						strTotal = ""+intTotal;
%>
							</table></div>
						</td>
					</tr>
					<script>
					document.getElementById('tds<%=strPartNumId%>').innerHTML = "&nbsp;<%=strTotal%>";
					</script>
<%
					intTotal = 0;
					}
				}// end of FOR
			} else	{
%>
					<tr>
						<td height="30" colspan="4" align="center" class="RightTextBlue"><fmtPartNumSearch:message key="MSG_PART_NOT_SOLD"/></td>
					</tr>
<%
		}
%>
					<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
				</table>
			</td>
		</tr>
		<tr><td height="25" class="RightDashBoardHeader">&nbsp;<fmtPartNumSearch:message key="LBL_CONSINED"/></td></tr>
		<tr>
			<td>
				<table border="0" width="698" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td width="100">&nbsp;<fmtPartNumSearch:message key="LBL_PART_NUM"/></td>
						<td width="50"><fmtPartNumSearch:message key="LBL_DESC"/></td>
						<td width="300"><fmtPartNumSearch:message key="LBL_DIST_ACCT_NM"/></td>
						<td HEIGHT="24" align="center" width="50"><fmtPartNumSearch:message key="LBL_QTY"/></td>
					</tr>
<%
			if (intConsignLength > 0)
			{
				hmTempLoop = new HashMap();
				intCount = 0;
				strLine = "";
				blFlag = false;
				
				for (int i = 0;i < intConsignLength ;i++ )
				{
					hmLoop = (HashMap)alConsignReport.get(i);

					if (i<intConsignLength-1)
					{
						hmTempLoop = (HashMap)alConsignReport.get(i+1);
						strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("PARTNUM"));
					}
					strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PARTNUM"));
					//if '.' is not there in part, need to add 1,2 etc with the partnumber for each block to make the toggle work
					if(strPartNum.indexOf(".") != -1){
						strPartNumId = strPartNum.replaceAll("\\.","1");
					}else{
						strPartNumId = strPartNum+"1";
					}
					strDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
					strId = GmCommonClass.parseNull((String)hmLoop.get("DID"));
					strId = strId.equals("")?GmCommonClass.parseNull((String)hmLoop.get("ACCID")):strId;
										
					strDistName = GmCommonClass.parseNull((String)hmLoop.get("DNAME"));
					strAcctName = GmCommonClass.parseNull((String)hmLoop.get("ANAME"));
					strDistName = strDistName.equals("")?strAcctName:strDistName;
					strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
					strReportType = GmCommonClass.parseNull((String)hmLoop.get("REPORT_BY"));
					intTotal = intTotal + Integer.parseInt(strQty);
					if (strPartNum.equals(strNextId))
					{
						intCount++;
						strLine = "<TR><TD colspan=4 height=1 class=Line></TD></TR>";
					}
					else
					{
						strLine = "<TR><TD colspan=4 height=1 class=Line></TD></TR>";
						if (intCount > 0)
						{
							strDesc = "";
							strLine = "";
						}
						else
						{
							blFlag = true;
						}
						intCount = 0;
						//
					}

					if (intCount > 1)
					{
						strDesc = "";
						strLine = "";
					}
				
					strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows

					out.print(strLine);
					if (intCount == 1 || blFlag)
					{
%>
					<tr id="trc<%=strPartNumId%>">
						<td width="100" height="20" class="RightText">&nbsp;<A class="RightText" title="Click to Expand" href="javascript:Toggle('c<%=strPartNumId%>')"><%=strPartNum%></a></td>
						<td colspan="2" width="550" class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
						<td width="50" class="RightText" id="tdc<%=strPartNumId%>" align="center"></td>
					</tr>
					<tr>
						<td colspan="4"><div style="display:none" id="divc<%=strPartNumId%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tabc<%=strPartNumId%>">

<%
								blFlag = false;
								}
%>
					<tr <%=strShade%>>
						<td width="100" class="RightText">&nbsp;</td>
						<td width="50" class="RightText">&nbsp;</td>
						<td width="300" height="20">&nbsp;<a class="RightText" href="javascript:fnCallDrill('<%=strPartNum%>','<%=strId%>','C','<%=strReportType%>');"><%=strDistName%></a></td>
						<td width="50" class="RightText" align="right"><%=strQty%>&nbsp;</td>
					</tr>
					<tr><td colspan="4" height="1" bgcolor="#eeeeee"></td></tr>
<%					
					if ( intCount == 0 || i == intConsignLength-1)
					{
						strTotal = ""+intTotal;
%>
							</table></div>
						</td>
					</tr>
					<script>
					document.getElementById('tdc<%=strPartNumId%>').innerHTML = "&nbsp;<%=strTotal%>";
					</script>
<%
					intTotal = 0;
					}
				}// end of FOR
			} else	{
%>
					<tr>
						<td height="30" colspan="4" align="center" class="RightTextBlue"><fmtPartNumSearch:message key="MSG_PART_NOT_CINSIGN"/></td>
					</tr>
<%
		}
%>
					<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
				</table>
			</td>
		</tr>
		<tr><td height="25" class="RightDashBoardHeader">&nbsp;<fmtPartNumSearch:message key="LBL_RETURNED"/></td></tr>
		<tr>
			<td>
				<table border="0" width="698" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td width="100">&nbsp;<fmtPartNumSearch:message key="LBL_PART_NUM"/></td>
						<td width="50"><fmtPartNumSearch:message key="LBL_DESC"/></td>
						<td width="300"><fmtPartNumSearch:message key="LBL_DIST_ACCT_NM"/></td>
						<td HEIGHT="24" align="center" width="50"><fmtPartNumSearch:message key="LBL_QTY"/></td>
					</tr>
<%
			if (intReturnLength > 0)
			{
				hmTempLoop = new HashMap();
				intCount = 0;
				strLine = "";
				blFlag = false;
				
				for (int i = 0;i < intReturnLength ;i++ )
				{
					hmLoop = (HashMap)alReturnReport.get(i);

					if (i<intReturnLength-1)
					{
						hmTempLoop = (HashMap)alReturnReport.get(i+1);
						strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("PARTNUM"));
					}
					strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PARTNUM"));
					if(strPartNum.indexOf(".") != -1){
						strPartNumId = strPartNum.replaceAll("\\.","2");
					}else{
						strPartNumId = strPartNum+"2";
					}
					strDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
					strId = GmCommonClass.parseNull((String)hmLoop.get("DID"));
					//PMT-28613 Japan - Part Number Fetch Drill down is not fetching details of consigned and return qty
					// If distributor is null fetch report by account  else report by distributor
					strReportType = strId.equals("")?"ACCOUNT":"DIST";
					strId = strId.equals("")?GmCommonClass.parseNull((String)hmLoop.get("ACCID")):strId;
										
					strDistName = GmCommonClass.parseNull((String)hmLoop.get("DNAME"));
					strAcctName = GmCommonClass.parseNull((String)hmLoop.get("ANAME"));
					strDistName = strDistName.equals("")?strAcctName:strDistName;
					strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
					intTotal = intTotal + Integer.parseInt(strQty);
					if (strPartNum.equals(strNextId))
					{
						intCount++;
						strLine = "<TR><TD colspan=4 height=1 class=Line></TD></TR>";
					}
					else
					{
						strLine = "<TR><TD colspan=4 height=1 class=Line></TD></TR>";
						if (intCount > 0)
						{
							strDesc = "";
							strLine = "";
						}
						else
						{
							blFlag = true;
						}
						intCount = 0;
						//
					}

					if (intCount > 1)
					{
						strDesc = "";
						strLine = "";
					}
				
					strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows

					out.print(strLine);
					if (intCount == 1 || blFlag)
					{
%>
					<tr id="trr<%=strPartNumId%>">
						<td width="100" height="20" class="RightText">&nbsp;<A class="RightText" title="Click to Expand" href="javascript:Toggle('r<%=strPartNumId%>')"><%=strPartNum%></a></td>
						<td colspan="2" width="550" class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
						<td width="50" class="RightText" id="tdr<%=strPartNumId%>" align="center"></td>
					</tr>
					<tr>
						<td colspan="4"><div style="display:none" id="divr<%=strPartNumId%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tabr<%=strPartNumId%>">

<%
								blFlag = false;
								}
%>
					<tr <%=strShade%>>
						<td width="100" class="RightText">&nbsp;</td>
						<td width="50" class="RightText">&nbsp;</td>
						<td width="300" height="20">&nbsp;<a class="RightText" href="javascript:fnCallDrill('<%=strPartNum%>','<%=strId%>','R','<%=strReportType%>');"><%=strDistName%></a></td>
						<!--<td width="300" class="RightText" height="20">&nbsp;<%=strDistName%></td>-->
						<td width="50" class="RightText" align="right"><%=strQty%>&nbsp;</td>
					</tr>
					<tr><td colspan="4" height="1" bgcolor="#eeeeee"></td></tr>
<%					
					if ( intCount == 0 || i == intReturnLength-1)
					{
						strTotal = ""+intTotal;
%>
							</table></div>
						</td>
					</tr>
					<script>
					document.getElementById('tdr<%=strPartNumId%>').innerHTML = "&nbsp;<%=strTotal%>";
					</script>
<%
					intTotal = 0;
					}
				}// end of FOR
			} else	{
%>
					<tr>
						<td height="30" colspan="4" align="center" class="RightTextBlue"><fmtPartNumSearch:message key="MSG_PART_NOT_RTN"/></td>
					</tr>
<%
		}
%>
					<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
				</table>
			</td>
		</tr>
		<tr><td height="25" class="RightDashBoardHeader">&nbsp;<fmtPartNumSearch:message key="LBL_QUARAN"/></td></tr>
		<tr>
			<td>
				<table border="0" width="698" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td width="100">&nbsp;<fmtPartNumSearch:message key="LBL_PART_NUM"/></td>
						<td width="50"><fmtPartNumSearch:message key="LBL_DESC"/></td>
						<td width="300"><fmtPartNumSearch:message key="LBL_REASON"/></td>
						<td HEIGHT="24" align="center" width="50"><fmtPartNumSearch:message key="LBL_QTY"/></td>
					</tr>
<%
			if (intQuaranLength > 0)
			{
				hmTempLoop = new HashMap();
				intCount = 0;
				strLine = "";
				blFlag = false;
				
				for (int i = 0;i < intQuaranLength ;i++ )
				{
					hmLoop = (HashMap)alQuarantineReport.get(i);

					if (i<intQuaranLength-1)
					{
						hmTempLoop = (HashMap)alQuarantineReport.get(i+1);
						strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("PARTNUM"));
					}
					strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PARTNUM"));
					if(strPartNum.indexOf(".") != -1){
						strPartNumId = strPartNum.replaceAll("\\.","3");
					}else{
						strPartNumId = strPartNum+"3";
					}
					strDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
					strId = GmCommonClass.parseNull((String)hmLoop.get("PID"));
										
					strDistName = GmCommonClass.parseNull((String)hmLoop.get("TNAME"));
					strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
					intTotal = intTotal + Integer.parseInt(strQty);
					if (strPartNum.equals(strNextId))
					{
						intCount++;
						strLine = "<TR><TD colspan=4 height=1 class=Line></TD></TR>";
					}
					else
					{
						strLine = "<TR><TD colspan=4 height=1 class=Line></TD></TR>";
						if (intCount > 0)
						{
							strDesc = "";
							strLine = "";
						}
						else
						{
							blFlag = true;
						}
						intCount = 0;
						//
					}

					if (intCount > 1)
					{
						strDesc = "";
						strLine = "";
					}
				
					strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows

					out.print(strLine);
					if (intCount == 1 || blFlag)
					{
%>
					<tr id="trq<%=strPartNumId%>">
						<td width="100" height="20" class="RightText">&nbsp;<A class="RightText" title="Click to Expand" href="javascript:Toggle('q<%=strPartNumId%>')"><%=strPartNum%></a></td>
						<td colspan="2" width="550" class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
						<td width="50" class="RightText" id="tdq<%=strPartNumId%>" align="center"></td>
					</tr>
					<tr>
						<td colspan="4"><div style="display:none" id="divq<%=strPartNumId%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tabq<%=strPartNumId%>">

<%
								blFlag = false;
								}
%>
					<tr <%=strShade%>>
						<td width="100" class="RightText">&nbsp;</td>
						<td width="50" class="RightText">&nbsp;</td>
						<!--<td width="300" height="20">&nbsp;<a class="RightText" href="javascript:fnCallDrill('<%=strPartNum%>','<%=strId%>','R');"><%=strDistName%></a></td>-->
						<td width="300" class="RightText" height="20">&nbsp;<%=strDistName%></td>
						<td width="50" class="RightText" align="right"><%=strQty%>&nbsp;</td>
					</tr>
					<tr><td colspan="4" height="1" bgcolor="#eeeeee"></td></tr>
<%					
					if ( intCount == 0 || i == intQuaranLength-1)
					{
						strTotal = ""+intTotal;
%>
							</table></div>
						</td>
					</tr>
					<script>
					document.getElementById('tdq<%=strPartNumId%>').innerHTML = "&nbsp;<%=strTotal%>";
					</script>
<%
					intTotal = 0;
					}
				}// end of FOR
			} else	{
%>
					<tr>
						<td height="30" colspan="4" align="center" class="RightTextBlue"><fmtPartNumSearch:message key="MSG_PART_NOT_QUARAN"/></td>
					</tr>
<%
		}
%>
					<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
				</table>
			</td>
		</tr>
	</table>


</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
