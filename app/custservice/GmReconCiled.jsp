<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc" %>
<!--GmReconCiled.jsp  -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
}
%>
<%@ taglib prefix="fmtReconciled" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtReconciled:setLocale value="<%=strLocale%>"/>
<fmtReconciled:setBundle basename="properties.labels.custservice.GmLoanerRecon"/>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td height="24" class="ShadeRightTableCaption"><a
			href="javascript:fnShowFilters('tabUnRecon');"><IMG
				id="tabUnReconimg" border=0 src="<%=strImagePath%>/minus.gif"></a>&nbsp;<fmtReconciled:message key="IMG_RECONCILED_TRANSACTION"/></td>
	</tr>
	<tr id="tabUnRecon" >
		<td>
			<div id="div_ReconTrans" style="height:200px"></div>
		</td>
	</tr>
	<tr></tr>
	<tr>
		<td colspan=3 height=1 class=Line></td>
	</tr>	
</table>