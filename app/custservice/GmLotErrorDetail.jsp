<!-- \customerservice\GmLotErrorDetail.jsp -->
<%
	/**********************************************************************************
	 * File		 		: GmLotErrorDetail.jsp
	 * Desc		 		: Lot Error Detail view Page
	 * Version	 		: 1.0
	 * author			: Vinoth Petchimuthu
	 
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtLotErrorRpt"
	uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page isELIgnored="false"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>

<%@ page import="com.globus.common.beans.GmCommonClass"%>
<fmtLotErrorRpt value="<%=strLocale%>" />
<fmtLotErrorRpt:setBundle
	basename="properties.labels.custservice.GmLotErrorDetail" />

<bean:define id="lotNumber" name="frmLotErrorReport" property="lotNumber" type="java.lang.String"></bean:define>
<bean:define id="transactionId" name="frmLotErrorReport" property="transactionId" type="java.lang.String"></bean:define>
<bean:define id="transactionType" name="frmLotErrorReport" property="transactionType" type="java.lang.String"></bean:define>
<bean:define id="transactionDate" name="frmLotErrorReport" property="transactionDate" type="java.lang.String"></bean:define>
<bean:define id="partNumber" name="frmLotErrorReport" property="partNumber" type="java.lang.String"></bean:define>
<bean:define id="resolutionType" name="frmLotErrorReport" property="resolutionType" type="java.lang.String"></bean:define>
<bean:define id="resolutionDate" name="frmLotErrorReport" property="resolutionDate" type="java.lang.String"></bean:define>
<bean:define id="status" name="frmLotErrorReport" property="status" type="java.lang.String"></bean:define>
<bean:define id="comments" name="frmLotErrorReport" property="status" type="java.lang.String"></bean:define>
<bean:define id="resolutionType" name="frmLotErrorReport" property="status" type="java.lang.String"></bean:define>


<%
    String strWikiTitle = GmCommonClass.getWikiTitle("LOT_ERROR_DETAIL");
	String strcustserviceJsPath = GmFilePathConfigurationBean
			.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strErrorInfo = "";
	String strLotNumber = "";
	String strPartNumber = "";
	String strPartDescription = "";
	String strTransactionId = "";
	String strTransactionType = "";
	String strTransactionDate = "";
	String strTransFieldSales = "";
	String strConsignedFieldSales = "";
	String strErrorMesssage = "";
	String strStatus = "";
	String strResolvedBy = "";
	String strResolvedDate = "";
	String strResolutionType = "";
	String strNewLotNumber = "";
	String strComments = "";
	String strErrType = "";
	String strDisabled = "";

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap) request.getAttribute("HMRETURN");
	
	ArrayList alResType = new ArrayList();
        alResType =  GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALRESTYPE"));

	if (hmReturn != null) {
		strErrorInfo = GmCommonClass.parseNull((String) hmReturn.get("ERRORID"));
		strLotNumber = GmCommonClass.parseNull((String) hmReturn.get("LOTNO"));
		strPartNumber = GmCommonClass.parseNull((String) hmReturn.get("PARTNO"));
		strPartDescription = GmCommonClass.parseNull((String) hmReturn.get("PARTDESC"));
		strTransactionId = GmCommonClass.parseNull((String) hmReturn.get("TRANSID"));
		strTransactionType = GmCommonClass.parseNull((String) hmReturn.get("TRANSTYPE"));
		strTransactionDate = GmCommonClass.parseNull((String) hmReturn.get("TRANSDATE"));
		strTransFieldSales = GmCommonClass.parseNull((String) hmReturn.get("TRANSFIELDSALES"));
		strConsignedFieldSales = GmCommonClass.parseNull((String) hmReturn.get("CONSIGNEDFIELDSALES"));
		strErrorMesssage = GmCommonClass.parseNull((String) hmReturn.get("ERRORMESSAGE"));
		strStatus = GmCommonClass.parseNull((String) hmReturn.get("ERRORSTATUS"));
		strResolvedBy = GmCommonClass.parseNull((String) hmReturn.get("RESOLVEDBY"));
		strResolvedDate = GmCommonClass.parseNull((String) hmReturn.get("RESOLVEDDATE"));
		strErrType = GmCommonClass.parseNull((String) hmReturn.get("ERRTYPE"));
		strComments = GmCommonClass.parseNull((String) hmReturn.get("COMMENTS"));
		strNewLotNumber = GmCommonClass.parseNull((String) hmReturn.get("UPDATEDLOT"));
		strResolutionType = GmCommonClass.parseNull((String) hmReturn.get("RESOLID"));
	
	}
	String strSessApplDateFmt= strGCompDateFmt;
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
	
	
	if(strStatus.equals("Resolved")) {
		strDisabled = "true";
	}
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Lot Error Detail</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript"
	src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="javascript" src="<%=strJsPath%>/GmGrid.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="javascript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script type="text/javascript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_fast.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript"
	src="<%=strcustserviceJsPath%>/GmLotErrorReport.js"></script>
<script>
var comments = "<%=strComments%>";
var companyInfoObj = "<%=strCompanyInfo%>";
</script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<style>
#successCont {
	color:red;
}
#img {
	margin-left: -695px;
}
</style>

</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnLoad();">
	<html:form action="/gmLotErrorReport.do?method=gmLotErrorRpt">
		<html:hidden name="frmLotErrorReport" property="strOpt" value="" />
		<input type="hidden" name="partNumber" value="<%=strPartNumber%>">
		<input type="hidden" name="strErrorInfoID" value="<%=strErrorInfo%>">
		<input type="hidden" name="lotNumber" value="<%=strLotNumber%>">
		<input type="hidden" name="transactionId" value="<%=strTransactionId%>">
		<input type="hidden" name="transactionType" value="<%=strTransactionType%>">
		<input type="hidden" name="errorMessge" value="<%=strErrorMesssage%>">
		<input type="hidden" name="companyInfo" value="<%=strCompanyInfo%>">
		<input type="hidden" name="status" value="<%=strStatus%>">
		<input type="hidden" name="errortype" value="<%=strErrType%>">
		

		<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" colspan="2" class="RightDashBoardHeader">&nbsp;<fmtLotErrorRpt:message
						key="LBL_LOT_ERROR_DETAIL" /></td>
						
				<td height="25" class="RightDashBoardHeader" align="right">
				<fmtLotErrorRpt:message key="IMG_ALT_HELP" var="varHelp"/>
			    <img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    </td>		
				<!-- <td><div id="successCont" style="margin-left:-800px;"></div></td> -->

			</tr>
			<tr>
				<td class="Line" height="1" colspan="5"></td>
			</tr>
			<tr class="evenshade">
				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtLotErrorRpt:message
						key="LBL_ERROR_INFO" var="varErrorInfo" />
					<gmjsp:label type="RegularText" SFLblControlName="${varErrorInfo}"
						td="false" /></td>
				<td class="RightText">&nbsp;<%=strErrorInfo%></td>
				<td><div id="successCont" style="margin-left:-700px;"></div></td>
			</tr>
			<tr class="oddshade" colspan="2">

				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;" id="lotNum"><fmtLotErrorRpt:message
						key="LBL_LOT_NUMBER" var="varLotNumber" />
					<gmjsp:label type="RegularText" SFLblControlName="${varLotNumber}"
						td="false" /></td>
				<td class="RightText">&nbsp;<%=strLotNumber%></td>
						
			</tr>

			<tr class="evenshade" colspan="2">

				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtLotErrorRpt:message
						key="LBL_PART_NUMBER" var="varPartNumber" />
					<gmjsp:label type="RegularText" SFLblControlName="${varPartNumber}"
						td="false" /></td>
				<td class="RightText" id="partNum">&nbsp;<%=strPartNumber%></td>
				
			</tr>
			<tr class="oddshade" colspan="2">

				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtLotErrorRpt:message
						key="LBL_PART_DESCRIPTION" var="varPartDescription" />
					<gmjsp:label type="RegularText"
						SFLblControlName="${varPartDescription}" td="false" /></td>
				<td class="RightText">&nbsp;<%=strPartDescription%></td>

			</tr>
			<tr class="evenshade" colspan="3">
				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtLotErrorRpt:message
						key="LBL_TRANSACTION_ID" var="varTransactionId" />
					<gmjsp:label type="RegularText"
						SFLblControlName="${varTransactionId}" td="false" /></td>
				<td class="RightText">&nbsp;<%=strTransactionId%></td>
				<!-- <td><div class="s_icon" style="margin-left: -695px; color: red;"><a href="#" align="left" onClick="fnLoadSummary();"><b>S</b></a></div></td> -->
				  <td><a href="javascript:fnLoadSummary();"><img id="img" src="<%=strImagePath%>/ordsum.gif" title="${varOrderSummary}"  border=0></a></td> 
			</tr>

			<tr class="oddshade" colspan="2">

				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtLotErrorRpt:message
						key="LBL_TRANSACTION_TYPE" var="varTransactionType" />
					<gmjsp:label type="RegularText"
						SFLblControlName="${varTransactionType}" td="false" /></td>
				<td class="RightText">&nbsp;<%=strTransactionType%></td>

			</tr>

			<tr class="evenshade" colspan="2">

				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtLotErrorRpt:message
						key="LBL_TRANSACTION_DATE" var="varTransactionDate" />
					<gmjsp:label type="RegularText"
						SFLblControlName="${varTransactionDate}" td="false" /></td>
				<td class="RightText">&nbsp;<%=strTransactionDate%></td>

			</tr>

			<tr class="oddshade" colspan="2">

				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtLotErrorRpt:message
						key="LBL_TRANSACTION_FIELD_SALES" var="varTransactionFieldSales" />
					<gmjsp:label type="RegularText"
						SFLblControlName="${varTransactionFieldSales}" td="false" /></td>
				<td class="RightText">&nbsp;<%=strTransFieldSales%></td>

			</tr>

			<tr class="evenshade" colspan="2">

				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtLotErrorRpt:message
						key="LBL_CONSIGNED_FIELD_SALES" var="varConsignedFieldSales" />
					<gmjsp:label type="RegularText"
						SFLblControlName="${varConsignedFieldSales}" td="false" /></td>
				<td class="RightText">&nbsp;<%=strConsignedFieldSales%></td>

			</tr>

			<tr class="oddshade" colspan="2">

				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtLotErrorRpt:message
						key="LBL_ERROR_MESSAGE" var="varErrorMessage" />
					<gmjsp:label type="RegularText"
						SFLblControlName="${varErrorMessage}" td="false" /></td>
				<td class="RightText">&nbsp;<%=strErrorMesssage%></td>
				
				<%if(strErrType.equals("107973")){ %>
				<!-- <td><div class="s_icon" style="margin-left:-700px; color: red;"><a align="left" onClick="fnLoadSummary();" ><b>S</b></a></div></td> -->
				<td><a href="javascript:fnLoadSum();"><img style="margin-left: -370px;" src="<%=strImagePath%>/ordsum.gif" title="${varOrderSummary}"  border=0></a></td>
               <%} %>
			</tr>

			<tr class="evenshade" colspan="2">

				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtLotErrorRpt:message key="LBL_STATUS"
						var="varStatus" />
					<gmjsp:label type="RegularText" SFLblControlName="${varStatus}"
						td="false" /></td>
				<td class="RightText">&nbsp;<%=strStatus%></td>

			</tr>

			<tr class="oddshade" colspan="2">

				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtLotErrorRpt:message
						key="LBL_RESOLVED_BY" var="varResolvedBy" />
					<gmjsp:label type="RegularText" SFLblControlName="${varResolvedBy}"
						td="false" /></td>
				<td class="RightText">&nbsp;<%=strResolvedBy%></td>

			</tr>

			<tr class="evenshade" colspan="2">

				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtLotErrorRpt:message
						key="LBL_RESOLVED_DATE" var="varResolvedDate" />
					<gmjsp:label type="RegularText"
						SFLblControlName="${varResolvedDate}" td="false" /></td>
				<td class="RightText" >&nbsp; <%=strResolvedDate%></td>

			</tr>

			<tr class="oddshade" colspan="2">
				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtLotErrorRpt:message
						key="LBL_RESOLUTION_TYPE" var="varResolutionType" /> <gmjsp:label
						type="RegularText" SFLblControlName="${varResolutionType}"
						td="false" />&nbsp;</td>
				<td>&nbsp;<gmjsp:dropdown SFFormName="frmLotErrorReport" controlName="resolutionType" SFSeletedValue="resolutionType" seletedValue="<%=strResolutionType%>" value="<%=alResType%>" codeId="CODEID" codeName="CODENM" defaultValue= "[Choose One]" tabIndex="6" width="220" onChange="fnOnChange();"/>
			    </td>

			</tr>

			<tr class="evenshade" colspan="2">

				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 30px;"><fmtLotErrorRpt:message
						key="LBL_NEW_LOT_NUMBER" /></td>
				 <td>&nbsp;<input type="text" size="60" style="height: 18px; width:220px;"
					name="new_lot_number" class="InputArea"
					onFocus="changeBgColor(this,'#AACCE8');"
					onBlur="changeBgColor(this,'#ffffff');" disabled id="new_lot_tex_tbox" value="<%=strNewLotNumber%>" tabindex=1>
					</td>

			</tr>

			<tr class="oddshade" colspan="2">

				<td class="RightRedCaption" colspan="2" align="right"
					style="height: 80px;"><fmtLotErrorRpt:message
						key="LBL_COMMENTS" /></td>
						
				<td>&nbsp;<textarea id="comments" class="InputArea" name="comments" cols="110" rows="5" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="1" value="<%=strComments%>" ></textarea>
				</td>
					
			</tr>
			<tr>
				<td class="Line" height="1" colspan="5"></td>
			</tr>
			<tr class="evenshade">
				<td class="RightRedCaption" colspan="5" style="height: 80px; padding-left: 80px;" align="center">
				<fmtLotErrorRpt:message
						key="BTN_SUBMIT" var="varSubmit" /> 
				<gmjsp:button name="button_submit" style="width: 5em" value="${varSubmit}" gmClass="button"
						disabled="<%=strDisabled%>" onClick="fnSavLotDetails();" buttonType="Save" />&nbsp;
						<fmtLotErrorRpt:message key="BTN_CANCEL" var="varReset"/>
				<gmjsp:button name="button_reset" value="${varReset}" style="width: 6em" gmClass="button" onClick="fnReset();" disabled="<%=strDisabled%>" tabindex="4" buttonType="Save" />
				</td>
			</tr>
		</table>
	</html:form>

	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
