 <%
/**********************************************************************************
 * File		 		: GmProcessReturns.jsp
 * Desc		 		: This screen is used for the Territory Maintenance
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtProcessReturns" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!--custservice\GmProcessReturns.jsp  -->
<fmtProcessReturns:setLocale value="<%=strLocale%>"/>
<fmtProcessReturns:setBundle basename="properties.labels.custservice.GmProcessReturns"/>
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	HashMap hmLoad = (HashMap)session.getAttribute("hmLoad");
	// log.debug(" Value inside hmLoad is  " + hmLoad);
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction");
	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}
	strhAction = (strhAction == null)?"Load":strhAction;

	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strDistId = "";
	String strType = (String)session.getAttribute("TYPE")==null?"":(String)session.getAttribute("TYPE");
	String strReason = (String)session.getAttribute("REASON")==null?"":(String)session.getAttribute("REASON");
	String strAccId = (String)session.getAttribute("ID")==null?"":(String)session.getAttribute("ID");
	String strInvId = (String)request.getAttribute("hInvId")==null?"":(String)request.getAttribute("hInvId");
	String strExpDate = (String)session.getAttribute("EDATE")==null?"":(String)session.getAttribute("EDATE");
	String strComments = (String)session.getAttribute("COMMENTS")==null?"":(String)session.getAttribute("COMMENTS");
	String strEmpName = GmCommonClass.parseZero((String)session.getAttribute("EMPNAME"));

	ArrayList alDistributor = new ArrayList();
	ArrayList alReturnType = new ArrayList();
	ArrayList alReturnReasons = new ArrayList();
	ArrayList alAccounts = new ArrayList();
	ArrayList alSets = new ArrayList();
	ArrayList alEmpName = new ArrayList();

	if (hmLoad != null)
	{
		log.debug(" Value of alDistributor Before getting the value is is " + alDistributor.size());
		log.debug(" HashValue of alDistributor Before getting the value is " + alDistributor.hashCode());
		alDistributor = (ArrayList)hmLoad.get("DISTLIST");
		log.debug(" Value of alDistributor is " + alDistributor.size());
		log.debug(" HashValue of alDistributor is " + alDistributor.hashCode());
		if (strhAction.equals("Load"))
		{
			log.debug(" INside Load COndition");
			HashMap hmTemp = new HashMap();
			hmTemp.put("NAME","Globus Med-In House");
			hmTemp.put("ID","01");
			alDistributor.add(0,hmTemp);
		}
		alReturnType = (ArrayList)hmLoad.get("RETTYPES");
		alReturnReasons = (ArrayList)hmLoad.get("RETREASONS");
		alSets = (ArrayList)hmLoad.get("SETLIST");
		alEmpName = (ArrayList)hmLoad.get("EMPNAME");
		log.debug(" HashValue of alDistributor after adding element  is " + alDistributor.hashCode());
	}


	int intSize = 0;
	HashMap hcboVal = null;
	HashMap hcboDistVal = null;
	
	// This section for Item Consignment
	String strPartNums = (String)session.getAttribute("strPartNums") == null?"":(String)session.getAttribute("strPartNums");
	String strNewPartNums = "";
	String strPartDesc = "";
	HashMap hmTemp = new HashMap();
	HashMap hmParam =  new HashMap();
	String strItemQty = "";

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Process Returns </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
function fnSubmit()
{
	if (document.frmTerritory.Chk_ActiveFl.checked)
	{
		document.frmTerritory.Chk_ActiveFl.value = 'Y';
	}
	document.frmTerritory.submit();
}

function fnLoadList(val)
{
	if (val == '3301')
	{
		// document.all.list.innerHTML = document.all.Dist.innerHTML;
		document.all.cont.innerHTML = document.all.Set.innerHTML;
		var obj = document.frmTerritory.Cbo_Reason;
		for (var i=0;i<obj.length ;i++ )
		{
			val = obj.options[i].value;
			if (val == "3313")
			{
				obj.options[i].selected = true;
				break;
			}
		}
	}
	else if (val == '3302')
	{
//		document.all.list.innerHTML = document.all.Dist.innerHTML;
		if (document.frmTerritory.hAction.value != 'PopOrd')
		{
			document.all.cont.innerHTML = document.all.PartNums.innerHTML;
		}
	}

}

function fnInitiate()
{
	document.frmTerritory.hAction.value = "Initiate";
	fnStartProgress('Y');
	document.frmTerritory.submit();
}

function fnAddToCart()
{
	document.frmTerritory.hAction.value = "GoCart";
	fnStartProgress('Y');
	document.frmTerritory.submit();
}

function fnRemoveItem(val)
{
	document.frmTerritory.hDelPartNum.value = val;
	document.frmTerritory.hAction.value = "RemoveCart";
	document.frmTerritory.submit();	
}

function fnUpdateCart()
{
	document.frmTerritory.hAction.value = "UpdateCart";
	fnStartProgress('Y');
	document.frmTerritory.submit();	
}

function fnLoad()
{
	if (document.frmTerritory.hAction.value == 'PopOrd')
	{
		fnLoadList('<%=strType%>');
	}
	fnCallEmp(document.frmTerritory.hId);
}

function fnPlaceOrder()
{
	var objType = document.frmTerritory.Cbo_Type;
	if (objType.value == '0')
	{
		Error_Details(message[90]);
	}

	var objReason = document.frmTerritory.Cbo_Reason;
	if (objReason.value == '0')
	{
		Error_Details(message[91]);
	}
	
	var objDist = document.frmTerritory.Cbo_Id;
	if (objDist.value == '0')
	{
		Error_Details(message[92]);
	}

	var objEmp = document.frmTerritory.cbo_EmpName;
	if (objDist.value == '01' && objEmp.value == '0')
	{
		Error_Details(message[93]);
	}
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}

	document.frmTerritory.hAction.value = "PlaceOrder";
	fnStartProgress('Y');
	document.frmTerritory.submit();	
}

function fnCallEmp(obj)
{
	var objEmp = document.frmTerritory.cbo_EmpName;
	if (obj.value == '01')
	{
		objEmp.disabled = false;
		objEmp.focus();
	}else
	{
		objEmp.disabled = true;
		objEmp.options[0].selected = true;
	}
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnLoad();">
<FORM name="frmTerritory" method="POST" action="<%=strServletPath%>/GmProcessReturnsServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hDelPartNum" value="">
<input type="hidden" name="hId" value="<%=strAccId%>">

	<table border="0" width="850" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="848" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td class="Line" colspan="4"></td></tr>
					<tr>
						<td colspan="4" height="25" class="RightDashBoardHeader">&nbsp;<fmtProcessReturns:message key="LBL_PRO_RET"/></td>
					</tr>
					<tr><td class="Line" colspan="4"></td></tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtProcessReturns:message key="LBL_TYP_RET"/>:</td>
						<td>&nbsp;<select name="Cbo_Type" id="Region" class="RightText" tabindex="3" onFocus="changeBgColor(this,'#AACCE8');" onChange="javascript:fnLoadList(this.value);" onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
<%
			  		intSize = alReturnType.size();
					hcboVal = new HashMap();
			  		for (int i=1;i<intSize-1;i++)
			  		{
			  			hcboVal = (HashMap)alReturnType.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strType.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>

						</select></td>
					</tr>
					<tr class="shade">
						<td class="RightText" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtProcessReturns:message key="LBL_REASON"/>:</td>
						<td>&nbsp;<select name="Cbo_Reason" id="Region" class="RightText" tabindex="3" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
<%
			  		intSize = alReturnReasons.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alReturnReasons.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strReason.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>

						</select></td>
					</tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtProcessReturns:message key="LBL_CONSIGEE_NAME"/>:</td>
						<td class="RightText"> &nbsp;<select name = "Cbo_Id" class = "RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" onChange="javascript:fnCallEmp(this);">
							<option value="0" >[Choose One]
						
						<%
						  		intSize = alDistributor.size();
						  		log.debug(" Size val is " + intSize);
								hcboDistVal = new HashMap();
						  		for (int i=0;i<intSize;i++)
						  			{
						  			hcboDistVal = (HashMap)alDistributor.get(i);
						  			// log.debug(" Value of hcboDistVal is  " +hcboDistVal);
						  			strCodeID = (String)hcboDistVal.get("ID");
						  			String strCodeName = (String)hcboDistVal.get("NAME");
									strSelected = strAccId.equals(strCodeID)?"selected":"";
									// log.debug (" Val of Acc Id  is " + strAccId + " Val of Code id is  " + strCodeID + " Code Name is " + strCodeName);
							%>
										<option <%=strSelected%> value="<%=strCodeID%>"><%=strCodeName%></option>
							<%
								  		}
								%>
								</select>
								
						<font color="red">*</font>&nbsp;<fmtProcessReturns:message key="LBL_EMP_NAME"/>:&nbsp;<select disabled name = "cbo_EmpName" class = "RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
								<option value="0" ><fmtProcessReturns:message key="LBL_CHOOSE"/>
						
						<%
						  		intSize = alEmpName.size();
						  		log.debug(" Size val is " + intSize);
								hcboDistVal = new HashMap();
						  		for (int i=0;i<intSize;i++)
						  			{
						  			hcboDistVal = (HashMap)alEmpName.get(i);
						  			// log.debug(" Value of hcboDistVal is  " +hcboDistVal);
						  			strCodeID = (String)hcboDistVal.get("ID");
						  			String strCodeName = (String)hcboDistVal.get("NAME");
									strSelected = strEmpName.equals(strCodeID)?"selected":"";
									// log.debug (" Val of Acc Id  is " + strAccId + " Val of Code id is  " + strCodeID + " Code Name is " + strCodeName);
							%>
										<option <%=strSelected%> value="<%=strCodeID%>"><%=strCodeName%></option>
							<%
								  		}
								%>
								</select>
							</td> 
								
					</tr>
<%
if (!strhAction.equals("PopOrd"))
	{
%>
					<tr class="shade">
						<td class="RightText" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtProcessReturns:message key="LBL_SET_PART_NUM"/>:</td>
						<td id="cont">&nbsp;<input class="RightText" type="text" size="15" value="" disabled></td>
					</tr>
<%
	}
%>
					<tr>
						<td class="RightText" valign="top" align="right" HEIGHT="24"><fmtProcessReturns:message key="LBL_COMMENTS"/>:</td>
						<td>&nbsp;<textarea name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=7 rows=5 cols=45 value=""><%=strComments%></textarea></td>
					</tr>
					<tr>
						<td colspan="2">

						</td>
					</tr>
					<tr class="Shade">
						<td class="RightText" valign="top" align="right" HEIGHT="24"><fmtProcessReturns:message key="LBL_EXP_DT_RET"/>:</td>
						<td>&nbsp;<input type="text" size="10" value="<%=strExpDate%>" name="Txt_ExpDate" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=23>
						<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmTerritory.Txt_ExpDate');" title="Click to open Calendar"  src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 /></td>
					</tr>
<%
if (strhAction.equals("PopOrd"))
	{
%>						
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr>
						<td height="30" align="center" colspan="2" class="RightTextBlue"><br><b></b><br><fmtProcessReturns:message key="LBL_SHOOPING_CART"/>.<br>&nbsp;&nbsp;&nbsp;<input type="text" size="40" value="" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
						<fmtProcessReturns:message key="BTN_GO" var="varGo"/><gmjsp:button value="&nbsp;${varGo}&nbsp;" name="Btn_GoCart" gmClass="button" onClick="fnAddToCart();" tabindex="16" buttonType="Load" /><br><br>
						</td>
					</tr>
					<tr>
						<td align="center" colspan="2" valign="top">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td align="center" width="80" height="25"><fmtProcessReturns:message key="LBL_PART"/></td>
									<td align="center" width="200"><fmtProcessReturns:message key="LBL_DESC"/></td>
									<td align="center" width="80"><fmtProcessReturns:message key="LBL_QTY"/></td>
								</tr>
								<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>

<%
			if (!strPartNums.equals(""))
			{
					hmReturn = (HashMap)session.getAttribute("hmOrder");
					HashMap hmCart = (HashMap)session.getAttribute("hmCart");
					StringTokenizer strTok = new StringTokenizer(strPartNums,",");
					String strPartNum = "";
					HashMap hmVal = new HashMap();
					HashMap hmLoop = new HashMap();
					String strId = "";
					String strDesc = "";

					String strQty = "1";
					String strUnPartNums = "";

					String strShade = "";
					int i=0;
					boolean bolFl = false;
					boolean bolFl2 = false;

					int intQty = 0;
					if (strTok.countTokens() == 1)
					{
						bolFl2 = true;
					}
					while (strTok.hasMoreTokens())
					{
						i++;
						strPartNum 	= strTok.nextToken();
						if (hmCart != null)
						{
							hmLoop = (HashMap)hmCart.get(strPartNum);
							if (hmLoop != null)
							{
								strQty = (String)hmLoop.get("QTY");
							}
							else
							{
								strQty = "1";
							}
						}

						strQty = strQty == null?"1":strQty;
						intQty = Integer.parseInt(strQty);
						hmVal = (HashMap)hmReturn.get(strPartNum);
						if (hmVal.isEmpty())
						{
							//bolFl = false;
						}
						else if (hmVal.isEmpty() && bolFl2)
						{
							bolFl = false;
						}
						else
						{
							strNewPartNums = strNewPartNums + strPartNum + ",";
							bolFl = true;
							strId = (String)hmVal.get("ID");
							strDesc = (String)hmVal.get("PDESC");

							strShade	= (i%2 == 0)?"class=Shade":""; //For alternate Shading of rows

%>
								<tr <%=strShade%>>
									<td class="RightText" align="center" height="20">
<%
							if (!strhAction.equals("OrdPlaced"))
							{
%>									
									<a  href="javascript:fnRemoveItem('<%=strId%>');"><img border="0" Alt='Remove item from cart' valign="bottom" src="<%=strImagePath%>/btn_remove.gif" height="15" width="15"></a><%}%>&nbsp;<%=strId%></td>
									<td class="RightText"><%=GmCommonClass.getStringWithTM(strDesc)%></td>
									<td class="RightText" align="center">
<%
							if (!strhAction.equals("OrdPlaced"))
							{
%>
										&nbsp;<input type="text" size="5" value="<%=strQty%>" name="Txt_Qty<%=i%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
<%
							}else{	
%>
										<%=strQty%>
<%							}%>
									</td>
								</tr>
<%
						}
					}
						if (!strNewPartNums.equals(""))
						{
							strNewPartNums = strNewPartNums.substring(0,strNewPartNums.length()-1);
						}
						if (!bolFl)
						{
%>
								<tr>
									<td colspan="6" height="30" class="RightTextBlue" align="center"><fmtProcessReturns:message key="LBL_NO_ITEM"/></td>
								</tr>
<%
						}
}else{ %>
								<tr>
									<td colspan="6" height="30" class="RightTextBlue" align="center"><fmtProcessReturns:message key="LBL_NO_ITEM"/></td>
								</tr>
<%
			}
%>
							</table><input type="hidden" name="hPartNums" value="<%=strNewPartNums%>">
						</td>
					</tr>
<%
	}
%>	
				   <tr>
						<td colspan="2" align="Center" height="30">&nbsp;
<%
if (strhAction.equals("PopOrd"))
{
%>	
			<fmtProcessReturns:message key="BTN_UPD_QTY" var="varUpdQty"/><gmjsp:button value="&nbsp;${varUpdQty}&nbsp;" name="Btn_UpdateQty" gmClass="button" onClick="fnUpdateCart();" buttonType="Save" />&nbsp;&nbsp;
				<fmtProcessReturns:message key="BTN_INITIATE_RET" var="varInitiateRet"/><gmjsp:button value="&nbsp;${varInitiateRet}&nbsp;" name="Btn_PlaceOrd" gmClass="button" onClick="fnPlaceOrder();"  buttonType="Save" />
<%}else{%>
						<fmtProcessReturns:message key="BTN_INITIATE" var="varInitiate"/><gmjsp:button value="${varInitiate}" gmClass="button" onClick="fnInitiate();" tabindex="16" buttonType="Save" />&nbsp;
<%}%>
						</td>
					</tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>

<div style="visibility:hidden" id="Dist">
	&nbsp;<select name="Cbo_Id" id="Region" class="RightText" tabindex="3" onFocus="changeBgColor(this,'#AACCE8');"  onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtProcessReturns:message key="LBL_"/>[Choose One]
<%
		intSize = alDistributor.size();
		hcboVal = new HashMap();
		for (int i=0;i<intSize;i++)
		{
			hcboVal = (HashMap)alDistributor.get(i);
			strCodeID = (String)hcboVal.get("ID");
			strSelected = strAccId.equals(strCodeID)?"selected":"";
%>
			<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%
  		}
%>
	</select>
</div>
<div style="visibility:hidden" id="Set">
	&nbsp;<select name="Cbo_SetId" id="Region" class="RightText" tabindex="3" onFocus="changeBgColor(this,'#AACCE8');"  onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
<%
		intSize = alSets.size();
		hcboVal = new HashMap();
		for (int i=0;i<intSize;i++)
		{
			hcboVal = (HashMap)alSets.get(i);
			strCodeID = (String)hcboVal.get("ID");
			strSelected = strDistId.equals(strCodeID)?"selected":"";
%>
			<option <%=strSelected%> value="<%=strCodeID%>"><%=GmCommonClass.getStringWithTM((String)hcboVal.get("NAME"))%></option>
<%
  		}
%>
	</select>
</div>
<div style="visibility:hidden" id="InvId">
	&nbsp;<input type="text" size="10" value="<%=strInvId%>" name="Txt_Id" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
</div>
<div style="visibility:hidden" id="PartNums">
	&nbsp;<input type="text" size="40" value="" name="Txt_Id" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
</div>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
