<%
/**********************************************************************************
 * File		 		: GmTagTransfer.jsp
 * Created Date		: 12 Aug 2010 	
 * author			: Aananthy
************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmCommonClass"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ page import="java.util.List,java.util.Iterator"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<%@ taglib prefix="fmtTagTransfer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \accounts\physicalaudit\GmTagTransfer.jsp -->
<fmtTagTransfer:setLocale value="<%=strLocale%>"/>
<fmtTagTransfer:setBundle basename="properties.labels.accounts.physicalaudit.GmTagTransfer"/> 
<bean:define id="tagid" name="frmTagTransfer" property="tagId" type="java.lang.String"></bean:define>
<bean:define id="hmNames" name="frmTagTransfer" property="hmNames" type="java.util.HashMap"> </bean:define>
<bean:define id="consignedTo" name="frmTagTransfer" property="consignedTo" type="java.lang.String"></bean:define>
<bean:define id="setId" name="frmTagTransfer" property="setId" type="java.lang.String"></bean:define>
<bean:define id="strDisabled" name="frmTagTransfer" property="strDisabled" type="java.lang.String"></bean:define>
<bean:define id="hstatus" name="frmTagTransfer" property="status" type="java.lang.String"></bean:define>
<bean:define id="comments" name="frmTagTransfer" property="comments" type="java.lang.String"></bean:define>
<%
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.accounts.physicalaudit.GmTagTransfer", strSessCompanyLocale);
String strWikiTitle = GmCommonClass.getWikiTitle("TAG_TRANSFER");
ArrayList alDistributor = new ArrayList();
ArrayList alInhouseList = new ArrayList();
ArrayList alLoanerSetList = new ArrayList();
ArrayList alConsignSetList = new ArrayList();

String strVoidName = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("BTN_VOID"));

HashMap hcboVal = null;

if (hmNames != null){
	alDistributor = (ArrayList)hmNames.get("DISTLIST");
	alInhouseList = (ArrayList)hmNames.get("INHOUSELIST");
}
if(!strDisabled.equals("")){
	strVoidName = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("BTN_UNVOID"));
}
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Tag Transfer </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="styles/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Accounts/physicalaudit/GmTagTransfer.js"></script>

<style type="text/css" media="all">
     @import url("styles/screen.css");
</style>
</HEAD>
<script type="text/javascript">

var lblTagId = '<fmtTagTransfer:message key="LBL_TAG_ID"/>';
var lblStatus = '<fmtTagTransfer:message key="LBL_STATUS"/>';
var lblPartNum = '<fmtTagTransfer:message key="LBL_PART"/>';
var lblSetId = '<fmtTagTransfer:message key="LBL_SET_ID"/>';
var lblCtrlNum = '<fmtTagTransfer:message key="LBL_CONTROL"/>';
var lblRefId = '<fmtTagTransfer:message key="LBL_REF_ID"/>';
var lblLocType = '<fmtTagTransfer:message key="LBL_LOCATION_TYPE"/>';
var lblConsTo = '<fmtTagTransfer:message key="LBL_CONSIGNED_TO"/>';
var lblComments='<fmtTagTransfer:message key="LBL_COMMENTS"/>';

var DistLen = <%=alDistributor.size()%>;
var EmpLen = <%=alInhouseList.size()%>;
var SelectedLocId ='<%=consignedTo%>';
var SelectedSetId = '<%=setId%>';
var voidfl = '<%=strDisabled%>';
<%
	hcboVal = new HashMap();
	
	for (int i=0;i<alDistributor.size();i++)
	{
		hcboVal = (HashMap)alDistributor.get(i);
%>
	var DistArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>

<%
	hcboVal = new HashMap();
	for (int i=0;i<alInhouseList.size();i++)
	{
		hcboVal = (HashMap)alInhouseList.get(i);
%>
	var EmpArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<%
	} 
%>

</script>

<BODY leftmargin="20" topmargin="10" onLoad="fnLoad();">
<html:form action="/gmTagTransfer.do" >
<html:hidden property="strOpt" />
<html:hidden property="hTagId" value='<%=tagid%>' />
<html:hidden property="setType" />
<input type="hidden" name="hTxnId">
<input type="hidden" name="hCancelType">
<input type="hidden" name="hAction" >
<input type="hidden" name="hRedirectURL" >
<input type="hidden" name="hDisplayNm" >
<input type="hidden" name="hstatus" value="<%=hstatus%>" >
		<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="RightDashBoardHeader"><fmtTagTransfer:message key="LBL_EDIT_TAG"/></td>
				<td align="right" class=RightDashBoardHeader >
				<fmtTagTransfer:message key="IMG_HELP" var="varHelp"/> 	
					<img id='imgEdit' style='cursor:hand' src='/images/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>			
			</tr>
			<%-- 
			<tr><td  colspan="4" class="RightDashBoardHeader" >&nbsp;Tag Transfer <img id='imgEdit'  align="right"
				style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help'
				width='16' height='16'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td></tr>
			--%>
					<tr><td colspan="2" height="1" class="LLine"></td></tr> 
					<tr>					            
						<td class="RightTableCaption" align="right" HEIGHT="24" ><font color="red">*</font><fmtTagTransfer:message key="LBL_TAG_ID"/> : </td>	
						<td >&nbsp;<html:text size="20" styleClass="InputArea"  name= "frmTagTransfer"  property="tagId" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="1"/>
						&nbsp;&nbsp;<fmtTagTransfer:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}"  gmClass="button" onClick="fnTagLoad();" buttonType="Load" />
						&nbsp;&nbsp;<a href="javascript:fnTagHistory();"> <img width="18" height="18" title="Click here to see the history" style="cursor:hand" src="/images/icon_History.gif"/> </a>
						</td>												
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr> 	
					<tr class="shade" >
						<td  class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font> <fmtTagTransfer:message key="LBL_PART"/>: </td>
						<td>&nbsp;<html:text size="20" styleClass="InputArea"  name= "frmTagTransfer"  property="pnum"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="2"/></td>						
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
				<!-- Custom tag lib code modified for JBOSS migration changes -->	
				    <tr>
						<td  class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font><fmtTagTransfer:message key="LBL_SET_ID"/> : </td>
						<td>&nbsp;<gmjsp:dropdown   controlName= "setId" SFFormName="frmTagTransfer" SFSeletedValue="setId" SFValue="alSetIdResult" width="400" codeId="ID" codeName="IDNAME" defaultValue= "[Choose one]"  tabIndex="4"/></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr> 	
					<tr class="shade">
						<td  class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font><fmtTagTransfer:message key="LBL_CONTROL"/> : </td>
						<td>&nbsp;<html:text size="20" styleClass="InputArea"  name= "frmTagTransfer"  property="controlNum"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="5"/></td>	
						
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr> 	
					<tr>
						<td  class="RightTableCaption" align="right" HEIGHT="24"><fmtTagTransfer:message key="LBL_REF_ID"/>: </td>
						<td>&nbsp;<html:text size="20" styleClass="InputArea"  name= "frmTagTransfer"  property="refId"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="6"/></td>	
						
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr> 	
				   <tr class="shade">
						<td  class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font><fmtTagTransfer:message key="LBL_STATUS"/> : </td>
						<td>&nbsp;<gmjsp:dropdown  controlName= "status" SFFormName="frmTagTransfer" SFSeletedValue="status" SFValue="alStatus" codeId="CODEID" codeName="CODENM" defaultValue= "" onChange="fnChkStatus(this);" tabIndex="7"/></td>	
						 
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr> 	
					<tr>
						<td  class="RightTableCaption" align="right" HEIGHT="24"><fmtTagTransfer:message key="LBL_INVENTORY_TYPE"/>: </td>
						<td>&nbsp;<gmjsp:dropdown  controlName= "inventoryType" SFFormName="frmTagTransfer" SFSeletedValue="inventoryType" SFValue="alInventoryType" codeId="CODEID" codeName="CODENM" defaultValue= "[Choose one]"  onChange="fnStatus(this);" tabIndex="8"/></td>

					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr> 	
				   <tr class="ShadeRightTableCaption">
						<td  class="RightTableCaption" align="left" HEIGHT="20" colspan="2"><fmtTagTransfer:message key="LBL_CONSIGNED_INFO"/> </td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr> 
				   <tr>
						<td  class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font> <fmtTagTransfer:message key="LBL_LOCATION_TYPE"/>: </td>
						<td>&nbsp;<gmjsp:dropdown controlName="locationType" SFFormName="frmTagTransfer" SFSeletedValue="locationType" 
							SFValue="alLocationType" codeId = "CODENMALT"  codeName = "CODENM"  defaultValue= "[Choose One]"  onChange="javascript:fnGetNames(this)" tabIndex="9"/></td></td>	
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>					 
				    <tr class="shade">
						<td  class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font> <fmtTagTransfer:message key="LBL_CONSIGNED_TO"/> : </td>
						<td>&nbsp;<gmjsp:dropdown  controlName= "consignedTo" SFFormName="frmTagTransfer" SFSeletedValue="consignedTo"  codeId = "ID"  codeName = "NAME" defaultValue="[Choose one]"  tabIndex="10" /></td>	
						
					</tr>
					
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr>
						<td class="RightTableCaption" valign="center" align="right" HEIGHT="24"><font color="red">*</font><b><fmtTagTransfer:message key="LBL_COMMENTS"/> : </b></td>
						<td>&nbsp;<html:textarea property="comments"  name="frmTagTransfer"  cols="50" rows="5" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="11"/></td>
					</tr>
					
			 		<tr><td colspan="2" height="1" class="LLine"></td></tr> 
<%
		String strDisableBtnVal = strDisabled;
	if(strDisableBtnVal.equals("disabled"))
	{
		strDisableBtnVal = "true";
	}
%>
       	 			<tr>       	 	
						<td  width="40%" height="30" align="right"></td>									
						<td  width="60%" HEIGHT="24" align="left"><fmtTagTransfer:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button controlId="SubmitButton" value="${varSubmit}" disabled="<%=strDisableBtnVal %>" gmClass="button" buttonType="Save" onClick="fnSubmit();" />
						<gmjsp:button value="<%=strVoidName %>" gmClass="button" buttonType="Save" onClick="fnVoidTag();" /> 
						<fmtTagTransfer:message key="BTN_CURR_LOCATION" var="varCurrLocation"/><gmjsp:button controlId="currLocation" value="${varCurrLocation}" disabled="<%=strDisableBtnVal %>" gmClass="button" onClick="fnCurrLocation();" buttonType="Save" /> </td>
					</tr>
					<logic:notEqual name="frmTagTransfer" property="successMessage" value="">  
					<tr><td colspan="2" height="1" class="LLine"></td></tr> 
					<tr><td colspan="2" height="24" align="center">
					<font color="green"><b><bean:write name="frmTagTransfer" property="successMessage" /></b></font> </td></tr>
		
		 			</logic:notEqual>
   		</table>	     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
