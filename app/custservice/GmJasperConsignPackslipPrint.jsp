 <%
/**********************************************************************************
 * File		 		: GmConsignPackslipPrint.jsp
 * Desc		 		: This screen is used for the packing slip version of Consignment
 * Version	 		: 1.0
 * author			: Ranjith Sathurappan
************************************************************************************/
%>
<!-- \custservice\GmJasperConsignPackslipPrint.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
try { 
	String strCompAddress ="";
	String strApplDateFmt = strGCompDateFmt;
	HashMap hmReturn = new HashMap();
	HashMap hmConDetails = new HashMap();
	HashMap hmAtbData = new HashMap();
	HashMap hmCompAtbData = new HashMap();
	hmReturn = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturn"));
	hmAtbData = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("ORDER_ATB"));
	hmCompAtbData = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("COMP_ATB"));
	HashMap hmCompanyAddress= new HashMap();
	ArrayList alSetLoad = new ArrayList();
	ArrayList alPopulatedCartDtls = new ArrayList();
	
	String strConsignId = "";
	String strConsignAdd = "";	
	String strDate = "";
	String strShippingDt = "";		
	String strWhPicSlipHeader= "";
    String strConsignIdField =  "";
    String strConsignNameField =  "";
    String strShippingLable = ""; 
	String strUserName = "";
	String strCompanyName = "";
	String strCompanyAddress = "";
	String strCompanyPhone = "";	
	String strSpecialNotes = "";
	String strAccId = "";
	String strOrderedByLbl = "";
	String strDetailsLbl = "";
	String strModeLbl = "";
	String strTrackLbl = "";		 
	String strDrugLocalLic="";
	String strDrugExportLic="";
	String strExportDruglic="";
	String strLocalDruglic="";
	String strGmLocalLic="";
	String strGmExportLic="";
	String strGmDrugLocalLic="";
	String strGmDrugExportLic="";
	String strDivisionId = "";
	String strComapnyLogo = "";
	String strShowPhoneNo = "";
	String strCompId = "";
	String strPlantId = "";
	String strTransCompId = "";
	String strSalesRep="";
	String strShipAdd="";
	String strDLLocalNoVal="";
	String strDLExportNoVal="";
	String strShowDLNo ="";
	String strComapnySeal="";
	String strIsGSTEnabled = "";
	String strGSTStartDtFl = "";
	String strFooterLbl = "";
	String strShipToLbl ="" ;
	String strShipViaLbl =""; 
	String strLBShipModeLbl =""; 
	String strTrackingNoLbl = "";
	String strStockTitleLbl = "";
	String strTransDateLbl = "";
	String strTransIdLbl = "";
	String strRequestByLbl = "";
	String strRequestIdLbl = "";
	String strConsignType = "";
	
	 strImagePath = System.getProperty("ENV_PAPERWORKIMAGES");
	//GET THE VALUE FORM THE SERVLET FOR THE PRINT NAME
	String strPrintPurposelbl =GmCommonClass.parseNull((String)request.getAttribute("printPurpose"));
	String strGstFL =GmCommonClass.parseNull((String)request.getAttribute("GSTStartFl"));	
	
	strCompId=gmDataStoreVO.getCmpid(); 
	String strCompanyLocale = GmCommonClass.getCompanyLocale(strCompId);
	
	GmResourceBundleBean gmResourceBundleBean =
    GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);

	GmResourceBundleBean gmCompResourceBundleBean =
	GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);		
	String strJasperName=GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("CONSIGN.JASPERNM"));  
	String strSTJasperName=GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("STOCK_TRANSFER.JASPERNM"));   
	String strShowShipping = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("SHOW_SHIPPING"));
	strConsignNameField   = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("CONSIGNED_BY"));
	String strPartNumLbl  = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PART")); 
	String strPartDescLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("DESCRIPTION"));
	String strQtyUsedLbl  =  GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("QTY"));
	String strExpDateLbl  =  GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("EXP_DATE"));
	String strLotNumLbl   = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("CONTROL_NUMBER"));	
	String strCommentsLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("COMMENTS"));
	String strConsignAgentLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("CONSIGNING_AGENT"));
	String strSetRbObjectFl =  GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.ADD_RESOURCE_BUNDLE_OBJ"));
	String strDateLbl     = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("DATE"));
	String strConsignHeaderLbl    = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("PACKING_SLIP"));
	String strConsignToHeader     = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("CONSIGNED_TO"));
	String strShippingToHeader    = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("SHIP_TO"));
	String strShipListHeaderLbl   = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("SHIPPING_LIST"));
	String strShipListConsignToHeader  = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("CONSIGNEE"));
	String strShipListShippingToHeader  = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("SHIPPER"));
	strConsignIdField  = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("CONSIGN_NO"));
	strShippingLable = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("SHIPPING_DATE"));
	String strProformaLbl  = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("PROFORMA"));
	String strNotesLbl  =  GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("NOTES"));
	strSpecialNotes = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("SPECIALNOTES"));
	String strAccIdLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("ACCOUNTID"));
	String 	strShowGMNAAddress = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.SHOW_GMNA_ADDRESS"));
	String strExpDateFlg  =  GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.EXPDTENABLE"));
	String strFooterMsg  =  GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACKS_LIP.FOOTERMSG"));
	String strEnableExpDtForDist = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.ENABLE_EXP_DT_FOR_DIST"));
	String strLoanedToLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("LOANED_TO"));
	strOrderedByLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("ORDERBY")).equals("")? "Sales Rep" :GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("ORDERBY")) ;
	strDetailsLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("DETAILS")).equals("")? "Set ID/Name" :GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("DETAILS")) ;
	strModeLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("SHIPMODE")).equals("")? "Ship Mode" :GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("SHIPMODE")) ;
	strTrackLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("TRACK")).equals("")? "Tracking #" :GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("TRACK")) ;
     strFooterLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.FOOTER"));
	 strShipToLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.SHIPTO"));
	 strShipViaLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.SHIPVIA"));
	 strLBShipModeLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.SHIPMODE"));
	 strTrackingNoLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.TRACKINGNUM"));
	 strStockTitleLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("STOCK_TRANSFER.TITLE"));
	 strTransDateLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("STOCK_TRANSFER.TRANSDATE"));
	 strTransIdLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("STOCK_TRANSFER.TRANSID"));
	 strRequestByLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("STOCK_TRANSFER.REQUESTBY"));
	 strRequestIdLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("STOCK_TRANSFER.REQUESTID"));
	
	 alPopulatedCartDtls =  GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("alPopulatedCartDtls"));
	
	
	if (hmReturn != null)
	{
	  	if(alPopulatedCartDtls.isEmpty() && alPopulatedCartDtls != null)
	  	{
	  	alPopulatedCartDtls = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SETLOAD"));
	  	}
		//alSetLoad = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SETLOAD"));
		hmConDetails = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("SETDETAILS"));		 
		strShipAdd = GmCommonClass.parseNull((String)hmConDetails.get("SHIPADD"));
		strShippingDt = GmCommonClass.getStringFromDate((java.sql.Date)hmConDetails.get("SDATE"),strApplDateFmt);
		strConsignType = GmCommonClass.parseNull((String)hmConDetails.get("TYPE"));
		
		strTransCompId = GmCommonClass.parseNull((String)hmConDetails.get("COMPANYCD"));
		strDivisionId = strDivisionId.equals("")?"2000":strDivisionId;
		hmCompanyAddress = GmCommonClass.fetchCompanyAddress(strCompId , strDivisionId);
		if(gmDataStoreVO.getCmpid().equals("1000") || gmDataStoreVO.getCmpid().equals("1001")){
			strCompanyName = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMPNAME"));
			strCompanyAddress = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMPADDRESS"));
			strComapnyLogo = "/"+GmCommonClass.parseNull((String)hmCompanyAddress.get("LOGO"))+".gif";
			strComapnyLogo = strImagePath + strComapnyLogo + ".gif";
			strCompanyAddress = strCompanyAddress.replaceAll("/","<br>");
		}else{
			strCompanyAddress = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYADDRESS"));
			strCompanyName = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYNAME"));
			strCompanyName = strCompanyName.replaceAll("/","<br>");
			if(strShowPhoneNo.equals("YES")){
			strCompanyPhone =  GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYPHONE"));
			strCompanyPhone = strCompanyPhone.equals("")?"":"Phone: "+strCompanyPhone;
			}
			if(strShowGMNAAddress.equals("YES")){
				  strCompanyName = GmCommonClass.parseNull((String)gmCompResourceBundleBean.getProperty("GMNA.COMPNAME"));
				  strCompanyAddress = GmCommonClass.parseNull((String)gmCompResourceBundleBean.getProperty("GMNA.COMPADDRESS"));
				  strCompanyPhone = GmCommonClass.parseNull((String)gmCompResourceBundleBean.getProperty("GMNA.PH"));
				  strCompanyPhone = strCompanyPhone.equals("")?"":strCompanyPhone;
			}
			strCompanyAddress = strCompanyAddress + "/"+ strCompanyPhone;
			strCompanyAddress = strCompanyAddress.replaceAll("/", "\n<br>");
			strComapnyLogo = "/"+GmCommonClass.parseNull((String)hmCompanyAddress.get("LOGO"))+".gif";
			strComapnyLogo = strImagePath + strComapnyLogo;
		}
	}
	strGmLocalLic =  GmCommonClass.parseNull((String) hmCompAtbData.get("GMLOCALLIC"));
	strGmExportLic =  GmCommonClass.parseNull((String) hmCompAtbData.get("GMEXPORTLIC"));
	strGmDrugLocalLic =  GmCommonClass.parseNull((String) hmAtbData.get("GMDRUGLOCALLIC"));
	strGmDrugExportLic =  GmCommonClass.parseNull((String) hmAtbData.get("GMDRUGEXPORTLIC"));
	strExportDruglic =  GmCommonClass.parseNull((String) hmAtbData.get("EXPORTDRUGLIC"));
	strLocalDruglic =  GmCommonClass.parseNull((String) hmAtbData.get("LOCALDRUGLIC"));
	//drug license no from account attrbute
	if(!strExportDruglic.equals("") || !strLocalDruglic.equals(""))
	{
		strShipAdd = strShipAdd+"<br>&nbsp;DL- "+strLocalDruglic+"<br>&nbsp;DL- "+strExportDruglic;
	}
	//drug license no from cons attribute table if not empty,need to display the value
	if(!strGmDrugLocalLic.equals("") || !strGmDrugExportLic.equals(""))
	{
		strDLLocalNoVal=strGmDrugLocalLic;
		strDLExportNoVal=strGmDrugExportLic;
		strShowDLNo = "Y";
	}
	//otherwise need to display the drug license no from rule table 
	else{
	//drug license no from rule table if not empty,need to display the value 
		if(!strGmLocalLic.equals("") || !strGmExportLic.equals("")){
			strDLLocalNoVal=strGmLocalLic;
			strDLExportNoVal=strGmExportLic;
			strShowDLNo = "Y";
		}
		else{
		//otherwise need not to display the drug license no 
			strDLLocalNoVal="";
			strDLExportNoVal="";
			strShowDLNo = "N";
		}
	}
	
	 if(strConsignType.equals("106703") || strConsignType.equals("106704")){
	   strJasperName = strSTJasperName.equals("")? strJasperName : strSTJasperName;
	   log.debug("strJasperName=="+strJasperName);
	 }
	
	 String strGstEnable = GmCommonClass.parseNull((String) gmCompResourceBundleBean.getProperty("IS_GST_ENABLED"));
		strComapnySeal = "/"+GmCommonClass.parseNull((String)gmCompResourceBundleBean.getProperty("COMP_SEAL"))+".gif";
		strComapnySeal = strImagePath + strComapnySeal;
	if(strPrintPurposelbl.equals("ORG_RECIPIENT")){
		strPrintPurposelbl = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("ORG_RECIPIENT"));
	}
	if(strPrintPurposelbl.equals("DUP_TRANSPORTER")){
		strPrintPurposelbl = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("DUP_TRANSPORTER"));
	}
	if(strPrintPurposelbl.equals("TRIP_SUPPLIER")){
		strPrintPurposelbl = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TRIP_SUPPLIER"));
	}
 
	int intSize = 0;
	HashMap hcboVal = null;
	String strHtmlJasperRpt = "";	
	//PUT EXIXISTING ALL HASMAP VALUE IN THE ONE HAS MAP
	hmConDetails.putAll(hmAtbData);
	hmConDetails.putAll(hmCompAtbData);	
	hmConDetails.putAll(hmCompanyAddress);
	hmConDetails.put("GSTStartFl",strGstFL);
	hmConDetails.put("GSTENABLE",strGstEnable);
	
	
	hmConDetails.put("SHOW_SHIPPING",strShowShipping);
	hmConDetails.put("CONSIGNED_BY",strConsignNameField);
	hmConDetails.put("PART",strPartNumLbl);
	hmConDetails.put("DESCRIPTION",strPartDescLbl);
	hmConDetails.put("QTY",strQtyUsedLbl);
	hmConDetails.put("DATE",strDateLbl);
	hmConDetails.put("NOTES",strNotesLbl);	
	hmConDetails.put("SHIPMODE",strModeLbl);
	hmConDetails.put("TRACKLB",strTrackLbl);
	hmConDetails.put("SHIPPING_DATE",strShippingLable);
	hmConDetails.put("DETAILS",strDetailsLbl);
	hmConDetails.put("ACCOUNTID",strAccIdLbl);
	hmConDetails.put("PACKING_SLIP",strConsignHeaderLbl);
	hmConDetails.put("SHIP_TO",strShippingToHeader);	
	hmConDetails.put("CONSIGN_NO",strConsignIdField);
	hmConDetails.put("CONSIGNED_TO",strConsignToHeader);
	hmConDetails.put("CONTROL_NUM",strLotNumLbl);
	hmConDetails.put("SPECIALNOTES",strSpecialNotes);	
	hmConDetails.put("SALE_REP",strOrderedByLbl);
	hmConDetails.put("PRINTPURPOSE",strPrintPurposelbl);		  
	hmConDetails.put("COMPADDRESS",strCompanyAddress);
	hmConDetails.put("SDATE",strShippingDt);
	hmConDetails.put("COMPLOGO",strComapnyLogo);
	hmConDetails.put("DLLOCALLICNO",strDLLocalNoVal);
	hmConDetails.put("DLEXPORTLICNO",strDLExportNoVal);
	hmConDetails.put("SHOWDLNO",strShowDLNo);
	hmConDetails.put("STRSHIPADD",strShipAdd);
	hmConDetails.put("COMPLOGO",strComapnyLogo);
	hmConDetails.put("COMAPNYSEAL",strComapnySeal);
	
	
	hmConDetails.put("LBL_GMFOOTER",strFooterLbl);
	
	hmConDetails.put("LBL_SHIPTO",strShipToLbl);
	hmConDetails.put("LBL_SHIPVIA",strShipViaLbl);
	hmConDetails.put("LBL_MODE",strLBShipModeLbl );
	hmConDetails.put("LBL_TRACKINGNO",strTrackingNoLbl );
	hmConDetails.put("LBL_STOCK_TITLE",strStockTitleLbl);
	hmConDetails.put("LBL_TRANSDATE",strTransDateLbl);
	hmConDetails.put("LBL_TRANSNUM",strTransIdLbl);
	hmConDetails.put("LBL_REQUESTBY",strRequestByLbl);
	hmConDetails.put("LBL_REQID",strRequestIdLbl);
		
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Consignment Packing Slip</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<style>
TR.PageBreak {
     page-break-after: always;
}
</style>
<script>

function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
</HEAD>

<BODY topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<div id="button_table">
<table align="center" width="100%">
<tr>
	<td align="center" id="button">
		<gmjsp:button value="Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />
 		<gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" />
 	</td>
 </tr>
 </table>
 </div>	
<%          
           
            String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
			GmJasperReport gmJasperReport = new GmJasperReport();								
			gmJasperReport.setRequest(request);
			gmJasperReport.setResponse(response);
			gmJasperReport.setJasperReportName(strJasperName);
			gmJasperReport.setHmReportParameters(hmConDetails);
			//gmJasperReport.setReportDataList(alSetLoad); 					
			gmJasperReport.setReportDataList(alPopulatedCartDtls); 
			gmJasperReport.setBlDisplayImage(true);
			// to set the dynamic resource bundle object
	        if (strSetRbObjectFl.equalsIgnoreCase("YES")) {
	          gmJasperReport.setRbPaperwork(gmResourceBundleBean.getBundle());
	        }

			strHtmlJasperRpt = gmJasperReport.getXHtmlReport();
			
		%>				
		<%=strHtmlJasperRpt %>
		<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>
</HTML>
