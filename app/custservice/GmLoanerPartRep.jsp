
 
<%
/**********************************************************************************
 * File		 			: GmLoanerPartRep.jsp
 * Desc		 		: This screen is used for the displaying loaner info for a part / rep
 * Version	 		: 1.1
 * author			: Joe P Kumar
 * 
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtGmLoanerPartRep" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<!-- globusMedApp\custservice\GmLoanerPartRep.jsp -->
<fmtGmLoanerPartRep:setLocale value="<%=strLocale%>"/>
<fmtGmLoanerPartRep:setBundle basename="properties.labels.custservice.GmLoanerPartRep"/>

<!--  \custservice\GmLoanerPartRep.jsp-->
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	
	String strPartNum = GmCommonClass.parseNull((String)request.getAttribute("PARTNUM"));
	String strPartDesc = GmCommonClass.parseNull((String)request.getAttribute("PARTDESC"));
%>
<HTML>
<TITLE> Globus Medical: Loaner details </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

</HEAD>

<BODY leftmargin="20" topmargin="10">
<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"  colspan="2"><fmtGmLoanerPartRep:message key="LBL_LOAN_DET"/></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="2"></td></tr>
		<tr HEIGHT="24">
			<td  align="left" class="RightTableCaption" >&nbsp;<fmtGmLoanerPartRep:message key="LBL_PART_NUM"/></td>
			<td align="left" class="RightTableCaption" >&nbsp;<%=strPartNum%>  </td>
		<tr HEIGHT="24">
		<tr>
			<td align="left" class="RightTableCaption" >&nbsp;<fmtGmLoanerPartRep:message key="LBL_PART_DESC"/> </td>
			<td align="left" class="RightTableCaption" >&nbsp<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
		<tr>
		
		<tr><td bgcolor="#666666" height="1" colspan="2"></td></tr>
		<tr>
			<td colspan="2">
				<display:table name="requestScope.RSLOANERREPORT.rows" export="false" class="its" id="currentRowObject" >
				
							<fmtGmLoanerPartRep:message key="DT_SET_ID" var="varSetId"/><display:column property="SNAME" title="${varSetId}" class="alignleft" sortable="true" />
							<fmtGmLoanerPartRep:message key="DT_CONSIG_ID" var="varConsignmentID"/><display:column property="CONID" title="${varConsignmentID}" class="aligncenter"  sortable="true" />
							<fmtGmLoanerPartRep:message key="DT_ETCHID" var="varEtchId"/><display:column property="ETCHID" title="${varEtchId}" class="aligncenter" />
							<fmtGmLoanerPartRep:message key="DT_LOANERTO" var="varLoanerTo"/><display:column property="LOANNM" title="${varLoanerTo}" class="aligncenter"/>
							<fmtGmLoanerPartRep:message key="DT_LOANEDON" var="varLoanedOn"/><display:column property="LDATE" title="${varLoanedOn}" class="alignright" />
							<fmtGmLoanerPartRep:message key="DT_QTY" var="varQty"/><display:column property="CONS_QTY" title="${varQty}" class="alignright"  />
							<fmtGmLoanerPartRep:message key="DT_EX_DT" var="varExpirydate"/>><display:column property="EDATE" title="${varExpirydate}" class="alignright"  />
				</display:table>
				</td>
			</tr>
			<tr>
				<td align="center" height="30" id="button" colspan="2">
				<fmtGmLoanerPartRep:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" />
			</td>
			</tr>
	</table>
							
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
