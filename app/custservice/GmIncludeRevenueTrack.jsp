<%
/**********************************************************************************
 * File		 		: GmIncludeRevenueTrack.jsp
 * Desc		 		: This screen is used as a include file for displaying the revenue tracking information
 * Version	 		: 1.1
 * author			: Brinal
 * 
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.text.*" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtIncludeRevenue" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmIncludeRevenueTrack.jsp -->
<fmtIncludeRevenue:setLocale value="<%=strLocale%>"/>
<fmtIncludeRevenue:setBundle basename="properties.labels.custservice.ModifyOrder.GmModifyOrderEditPO"/>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String gridData = GmCommonClass.parseNull((String)request.getAttribute("XmlData"));
	String strWidth = GmCommonClass.parseNull((String)request.getParameter("WIDTH")); 
	strWidth = strWidth.equals("")?"800px":strWidth;
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Revenue Tracking</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script type="text/javascript" src="<%=strJsPath%>/operations/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<!-- <script language="JavaScript" src="<%=strJsPath%>/Error.js"></script> -->
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmIncludeRevenueTrack.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>

<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<script>
var gridObjData ='<%=gridData%>';
var gridObj;

</script>
</head>
<BODY leftmargin="20" topmargin="10" onload="javascript:fnOnPageLoad();">

<table cellspacing="0" cellpadding="0" >
	<tr>
		<td height="25" width="95%" class="RightDashBoardHeader"><fmtIncludeRevenue:message key="LBL_REVENUE_TRACKING"/></td>
		<td align="right" class="RightDashBoardHeader">						
		</td>
	</tr>
	<tr class="shade"></tr>	
	<tr>
		<td>
		<div id="dataGridDiv" width="<%=strWidth%>" height="100px"></div>		
		</td>
	</tr>	


</body>
</html>