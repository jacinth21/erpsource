 <%
/**********************************************************************************
 * File		 		: GmSetConsignSetup.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
 * Modification		: removed the Ship Req flag condition. Defaults to 1 meaning
 					  shipping is mandatory 
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.net.URLEncoder" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtSetConsign" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmSetConsignSetup.jsp -->
<fmtSetConsign:setLocale value="<%=strLocale%>"/>
<fmtSetConsign:setBundle basename="properties.labels.custservice.ProcessRequest.GmProcessRequestItems"/>
<%
try {
	
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	
	String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
	String strWikiTitle = GmCommonClass.getWikiTitle("SET_CONSIGN_SHIPOUT");
	

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");
	String strMode = (String)request.getAttribute("hMode") == null?"":(String)request.getAttribute("hMode");
	log.debug(" Mode is " + strMode);
	String strConsignmentId = GmCommonClass.parseNull((String)request.getAttribute("CONSIGNMENTID"));	
	String strRequestId = GmCommonClass.parseNull((String)request.getAttribute("REQUESTID"));
	String strSelectedAction = GmCommonClass.parseNull((String)request.getAttribute("hSelectedAction"));
	String strInitiatedFrom= GmCommonClass.parseNull((String)request.getAttribute("initiatedFrom"));
	String strDemandSheetMonthID= GmCommonClass.parseNull((String)request.getAttribute("demandSheetMonthID"));
	String strEnableComPolBtn = GmCommonClass.parseNull((String)request.getAttribute("ENABLE_COMPOOL_BTN")).equals("YES")?"":"true";
	boolean bolShowComPolBtn  = GmCommonClass.parseNull((String)request.getAttribute("SHW_COMPOOL_BTN")).equals("YES")?true:false;
	String strMessage =  GmCommonClass.parseNull((String)hmReturn.get("MSG"));
	 
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";

	String strDesc = "";
	String strTemp = "";
	String strShade = "";
	String strConsignId = "";
	String strSetName = "";
	String strAccName = "";
	String strUserName = "";
	String strIniDate = "";
	String strItemQty = "";
	String strControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strFlag = ""; 
	String strTissueFlag = "";
	String strType = "";
	String strDistribAccId = "";
	String strPurpose = "";
	String strBillTo = "";
	String strShipTo = "";
	String strShipToId = "";
	String strFinalComments = "";
	String strShipFl = "";
	String strDelFl = "";
	String strConsignmentStatus = "";
	String strLinkLnReqId = "";
	
	String strDisableValue = "";
	String strShipCarrier  = "";
	String strShipMode = "";

	ArrayList alSetLoad = new ArrayList();
	ArrayList alType = new ArrayList();
	ArrayList alShipTo = new ArrayList();
	ArrayList alPurpose = new ArrayList();
	ArrayList alConPurpose  = new ArrayList();
	
	ArrayList alDistributor = new ArrayList();
	ArrayList alRepList = new ArrayList();
	ArrayList alAccList = new ArrayList();
	ArrayList alEmpList = new ArrayList();

	HashMap hmConsignDetails = new HashMap();
	HashMap hmTemp = new HashMap();
	HashMap hmParam =  new HashMap();
	HashMap hmOtherDetails =  new HashMap();
	HashMap hmDefaultShipVal = new HashMap();
	String strCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	boolean bolFl = false;

	int intPriceSize = 0;

	if (hmReturn != null)
	{
		alSetLoad = (ArrayList)hmReturn.get("SETLOAD");
		hmTemp = (HashMap)hmReturn.get("SETCONLISTS");

		alType = (ArrayList)hmTemp.get("CONTYPE");
		alShipTo = (ArrayList)hmTemp.get("SHIPTO");
		alPurpose = (ArrayList)hmTemp.get("PURPOSE");
		alConPurpose = (ArrayList)hmTemp.get("CONPURPOSE");
		
		alDistributor = (ArrayList)hmTemp.get("DISTRIBUTORLIST");
		alRepList = (ArrayList)hmTemp.get("REPLIST");
		alAccList = (ArrayList)hmTemp.get("ACCLIST");
		alEmpList = (ArrayList)hmTemp.get("EMPLIST");
		

		hmConsignDetails = (HashMap)hmReturn.get("CONDETAILS");
		strConsignId = GmCommonClass.parseNull((String)hmConsignDetails.get("CID"));
		strSetName= GmCommonClass.parseNull((String)hmConsignDetails.get("SNAME"));
		strAccName = GmCommonClass.parseNull((String)hmConsignDetails.get("ANAME"));
		strUserName = GmCommonClass.parseNull((String)hmConsignDetails.get("UNAME"));
		strIniDate = GmCommonClass.parseNull((String)hmConsignDetails.get("CDATE"));
		strDesc = GmCommonClass.parseNull((String)hmConsignDetails.get("COMMENTS"));
		strFlag = GmCommonClass.parseNull((String)hmConsignDetails.get("SFL"));
		strTissueFlag = GmCommonClass.parseNull((String)hmConsignDetails.get("TISSUEFL"));
		strChecked = strFlag.equals("1")?"checked":"";

		//To get the csrrier and mode value for rules table
		hmDefaultShipVal = (HashMap)hmReturn.get("DEFAULTSHIPMODECARRVAL"); 
		strShipCarrier = GmCommonClass.parseNull((String)hmDefaultShipVal.get("SHIP_CARRIER"));
		strShipMode = GmCommonClass.parseNull((String) hmDefaultShipVal.get("SHIP_MODE"));
		if(strShipCarrier.equals("")){
			strShipCarrier ="0";	
		}
		if(strShipMode.equals("")){
			strShipMode ="0";	
		}
	}

	if (strhAction.equals("EditLoad"))
	{
		hmParam = (HashMap)hmTemp.get("OTHERDETAILS");
	}else
	{
		hmParam = (HashMap)request.getAttribute("PARAM");
	}

	if (hmParam != null)
	{
		strType = GmCommonClass.parseNull((String)hmParam.get("TYPE"));
		strConsignmentStatus = GmCommonClass.parseZero((String)hmParam.get("STATUS"));
		log.debug(" consignment status " + strConsignmentStatus);
		if (Double.parseDouble(strConsignmentStatus) >= 4)
		{
			bolFl = true;
		}
		strPurpose = (String)hmParam.get("PURPOSE");
		strBillTo = (String)hmParam.get("DISTACCID");
		strShipTo = (String)hmParam.get("SHIPTO");
		strShipToId = (String)hmParam.get("SHIPTOID");
		strFinalComments = (String)hmParam.get("COMMENTS");
		strShipFl = (String)hmParam.get("SHIPFL");
		strDelFl = (String)hmParam.get("SHIPFL");
		strLinkLnReqId = GmCommonClass.parseNull((String)hmParam.get("LINKLNREQID"));
	}

	if (!strType.equals("4110")&& !strType.equals("4112")) 
		strDisableValue = "Disabled";
	
	int intSize = 0;
	HashMap hcboVal = null;
	
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();
	
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Consign </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmSetConsignSetup.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
var sServletPath = '<%=strServletPath%>';
var distid;
var repid;
var accid;
var ConRepLen = <%=alRepList.size()%>;
var consignId = '<%=strConsignId%>';
var vshipMode = '<%=strShipMode%>';
var vshipCarrier = '<%=strShipCarrier%>';
var vtissuefl = '<%=strTissueFlag%>';
<%
	intSize = alRepList.size();
	hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alRepList.get(i);
%>
	var ConRepArr<%=i%> ="<%=hcboVal.get("REPID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>

var ConAccLen = <%=alAccList.size()%>;
<%
	intSize = alAccList.size();
	hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alAccList.get(i);
%>
	var ConAccArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>

var ConEmpLen = <%=alEmpList.size()%>;
<%
	intSize = alEmpList.size();
	hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alEmpList.get(i);
%>
	var ConEmpArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>

 
 
 
var consignLen= <%=alConPurpose.size()%>;
var inhouseLen = <%=alPurpose.size()%>;
 

<%
	hcboVal = new HashMap();
	for (int i=0;i<alConPurpose.size();i++)
	{
		hcboVal = (HashMap)alConPurpose.get(i); 
%>
	var ConArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<%
	}
%>

<%	
	hcboVal = new HashMap();
	for (int i=0;i<alPurpose.size();i++)
	{
		hcboVal = (HashMap)alPurpose.get(i); 
%>
	var InhouseArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%> ";
<%
	}
%>



function fnLoad()
{
	var selectedAct = document.all.hSelectedAction.value;
	if (selectedAct == 'BSTR'){
		document.frmVendor.Cbo_AccId.disabled = true;
		document.frmVendor.Cbo_SalesRepId.disabled = true;
		document.all.shipTo.disabled = true;
	}
	if (selectedAct != 'PROCLOANER'){
	
		if (document.frmVendor.hMode.value != 'INV' && document.frmVendor.hMode.value != 'ROLL')
		{
			var InHouseFl = '<%=strType%>';
			var ShipToFl = '<%=strShipTo%>';
			if (InHouseFl == '4112') // 4112 In-House Consignment
			{
				document.frmVendor.Cbo_Purpose.disabled = false;
				document.frmVendor.all.names.disabled = false;
				document.frmVendor.Cbo_BillTo.selectedIndex = 0;
				fnCallType('');
				fnSetValue();
			}
			/*
			if (ShipToFl == '4120') // 4120 dist
			{	
			alert(ShipToFl);	
				document.frmVendor.all.names.disabled = true;
				fnCallConsignShip();
					fnSetValue();
			}
			*/
			if (document.all.shipTo.value == '4120'){
				document.frmVendor.all.names.disabled = true;
			}
			if (document.all.shipCarrier.value == 0) {
				document.all.shipCarrier.value= vshipCarrier;//default value from rules table
			}
			if (document.all.shipMode.value == 0) {
				document.all.shipMode.value= vshipMode;// default value from rules table
			}
		}
		fnGetAddressList(document.all.names,addid);

		if(document.frmVendor.Cbo_Type.value =='102930'){
			document.frmVendor.Cbo_Purpose.value = 50060;
			document.frmVendor.Cbo_Purpose.disabled = true;
		}
	}else {
		document.frmVendor.Cbo_Type.value = '4127'; //Product Loaner
	} // selectedAct
}



function fnSetValue()
{
	var ShipToId = '<%=strShipToId%>';
	var obj5 = document.all.names;
	for (var j=0;j<obj5.length;j++)
	{
		if (obj5.options[j].value == ShipToId)
		{
			obj5.options[j].selected = true;
			break;
		}
	}
}

function fnSetShipTo(){
	if (document.all.Cbo_Type.value == '4110' || document.all.Cbo_Type.value == '102930'||document.all.Cbo_Type.value == '26240144'){
		var obj = document.all.Cbo_BillTo;		
		var distId = obj.options[obj.selectedIndex].value;
		if (distId != '01' || distId != 0)
		{	
			document.all.shipTo.value = 4120;
			distid = distId;
			accid = document.all.Cbo_AccId.value;
			repid = document.all.Cbo_SalesRepId.value;
			fnGetNames(document.all.shipTo);
			document.all.names.value=distId;
			document.all.names.disabled = true; 
			Reload (); 
		}	
		if (distId == '01' || distId == 0) {
		document.all.shipTo.value = 0;
		document.all.names.options.length = 0;
		document.all.names.options[0] = new Option("[Choose One]","0");
		document.all.names.value =0;
		document.all.names.disabled = false;
		}
	}
	if (document.all.Cbo_Type.value == '4112'){
		var obj = document.all.Cbo_BillTo;		
		var distId = obj.options[obj.selectedIndex].value;
		distid = distId;
	    accid = document.all.Cbo_AccId.value;
	    repid = document.all.Cbo_SalesRepId.value;
	    document.all.shipTo.value = 4123;
	    fnGetNames(document.all.shipTo);
	}
}

function fnOpenTag(strPnum,strConId)
{
	windowOpener("/gmTag.do?strOpt=reload&pnum="+strPnum+"&refID="+strConId+"&refType=51000","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
}

function Reload () {  
	var f = document.getElementById('iframe1'); 
	//alert("document.all.setID.value " + document.frmRequestHeader.setID.value);
	//alert("conid " + '<%=strConsignId%>');
 	//f.src  = document.all.RE_SRC.value + "&addressid=" + document.all.addressid.value + "&shipTo=" +document.all.shipTo.value + "&names=" +  document.all.names.value + "&hTxnId=" + '<%=strConsignId%>'+ "&RefName=RFS" ;
 	f.src  =  document.all.RE_SRC.value + "&refName=RFS" + "&addressid=" + document.all.addressid.value + "&shipTo=" +document.all.shipTo.value + "&names=" +  document.all.names.value + "&companyInfo=" + '<%=strCompanyInfo%>';
 	var divfr = document.getElementById('divf'); 
	 
 	divfr.style.display="block"; 
	     
 	f.src = f.src;   
	}

function fnMoveToCommonPool(){
	document.frmVendor.action = "/gmRequestEdit.do?FORWARD=gmIncShipDetails&RE_FORWARD=gmRequestHeader&RE_RE_FORWARD=gmConsignSetServlet&hAction=moveToCommonPool&hMode=CON&source=50184&screenType=Consign&FORMNAME=frmRequestHeader&hRequestView=headerView&requestId="+"<%=strRequestId%>"+"&requestFor=40021&hRequestId="+"<%=strRequestId%>"+"&demandSheetMonthID="+"<%=strDemandSheetMonthID%>"+"&initiatedFrom="+"<%=strInitiatedFrom%>";
	document.frmVendor.submit();	
}
<!-- this is migrated from OUS -->
function fnLoadBolonDetails(){
	windowOpener("/GmReprocessMaterialServlet?Cbo_Type=100062&hAction=Report&Txt_RefId="+consignId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=1100,height=600");
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad();">

 <input type="hidden" name="RE_SRC" value="<%=strServletPath%>/GmConsignSetServlet?hMode=CON&hAction=EditLoad&hRequestId=<%=strRequestId%>">
  <input  type="hidden" name = "hSelectedAction" value = "<%=strSelectedAction %>">
	<table border="0" cellspacing="0" cellpadding="0" class="DtTable800" > 
		<tr>
		<% 
			if(strSelectedAction.equals("BSTR")) {
			  %>
			<td height="25" class="RightDashBoardHeader"><fmtSetConsign:message key="LBL_SETRETURNHEADER"/></td>
			<% 
			} else{
			  %>
			<td height="25" class="RightDashBoardHeader"><fmtSetConsign:message key="LBL_SETCONSIGNHEADER"/></td>
			<% 
			}
		 %>
			<td height="25" class="RightDashBoardHeader" align="right"> <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> </td>
			 			
		</tr>
		<!-- this is migrated from OUS -->
		<%if(!strMessage.equals("")){ %>
			<tr><td bgcolor="#666666" height="1" colspan="2"></td></tr>			 		
			<tr><td class="RightText" height="24" align="center">&nbsp;<%=strMessage%>&nbsp;<a class="RightText" href="javascript:fnLoadBolonDetails();"><fmtSetConsign:message key="LBL_VIEWDETAILS"/></a></td></tr>
		<%}%>
		<tr><td bgcolor="#666666" height="1" colspan="2"></td></tr>
		
		 	<tr><td colspan="2"> 
						<jsp:include page="/operations/requests/GmRequestViewHeader.jsp" />
					</td></tr>
					
		<tr>
		<!-- <td  colspan="2" > 
		 <div id="divf" style="display:none;" >
		<iframe src="/gmRequestHeader.do?requestId=<%=strRequestId%>&FORMNAME=frmRequestHeader&hRequestView=headerView"  scrolling="yes" id="iframe2" marginheight="0" width="100%" height="100" frameborder="0"  ></iframe> 
		</div>  
			 -->	 
		</td></tr>	
		<tr><td bgcolor="#666666" height="1" colspan="2"></td></tr>
		<tr>
			<td width="100%" height="100" valign="top" colspan="2">
			
			<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmConsignSetServlet">
			<input type="hidden" name="hAction" value="<%=strhAction%>">
			<input type="hidden" name="hConsignId" value="<%=strConsignId%>">
			<input type="hidden" name="hRequestId" value="<%=strRequestId%>" >
			<input type="hidden" name="hMode" value="<%=strMode%>">
			<input type="hidden" name="RE_FORWARD">
			<input type="hidden" name="RE_RE_FORWARD">
			<input type="hidden" name="RE_TXN">
			<input type="hidden" name="txnStatus">
			<input type="hidden" name="Chk_ShipFlag" value="1">
				<table border="1" width="100%" cellspacing="0" cellpadding="0">

<%
				if (strMode.equals("CON")){
%>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtSetConsign:message key="LBL_TYPE"/>:</td>
						
						  <td class="RightTableCaption">&nbsp;<gmjsp:dropdown  controlName="Cbo_Type"  seletedValue="<%=strType %>" 
											value="<%=alType%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" onChange="javascript:fnCallType(this);"/>
						
						
						
				<%						
				if(!strSelectedAction.equals("PROCLOANER")){ 
				  if(!strSelectedAction.equals("BSTR")){ 
				%>				
											 &nbsp;&nbsp;&nbsp;&nbsp; <fmtSetConsign:message key="LBL_PURPOSE"/>:
							<select name="Cbo_Purpose" id="Region" class="RightText" tabindex="2" <%=strDisableValue%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
<%
					if(strType.equals("4110"))
					{
							intSize = alConPurpose.size();
					}
					else if(strType.equals("4112"))
					{
							intSize = alPurpose.size();
					}
					
						hcboVal = new HashMap();
							for (int i=0;i<intSize;i++)
							{
								if(strType.equals("4110"))
							{
								hcboVal = (HashMap)alConPurpose.get(i);
							}
								else  if(strType.equals("4112"))
							{
									hcboVal = (HashMap)alPurpose.get(i);
							}
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strPurpose.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
							</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtSetConsign:message key="LBL_LINKLOANER"/>:
							<input type="text" size="15" value="<%=strLinkLnReqId%>" name="Txt_LoanerReqId" class="InputArea" 
								onFocus="changeBgColor(this,'#AACCE8');" onBlur="javascript:fnValidateReqId(this); changeBgColor(this,'#ffffff');">
								<%
				  }
								%>
						</td>
					</tr>
					<tr class="shade">
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtSetConsign:message key="LBL_BILLTO"/>:</td>
						<td>&nbsp;<select name="Cbo_BillTo" id="Region" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" onChange="javascript:fnSetShipTo();"><option value="0" >[Choose One]
						<% 
						 if(!strSelectedAction.equals("BSTR")){ 
						 %>
						<option value="01" >Globus Med-In House
<%
						 }
			  		intSize = alDistributor.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alDistributor.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strBillTo.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%
			  		}
%>
							</select>
						</td>
					</tr>
					<tr>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtSetConsign:message key="LBL_ACCOUNT"/>:</td>
						<td>&nbsp;<select name="Cbo_AccId" id="Region" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" onChange="javascript:fnSetShipTo();"><option value="0" >[Choose One]
						<option value="01" >Globus Med-In House
<%
			  		intSize = alAccList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alAccList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strBillTo.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%
			  		}
%>
							</select>
						</td>
					</tr>
					<tr class="shade">
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtSetConsign:message key="LBL_SALESREP"/>:</td>
						<td>&nbsp;<select name="Cbo_SalesRepId" id="Region" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" onChange="javascript:fnSetShipTo();"><option value="0" >[Choose One]
						<option value="01" >Globus Med-In House
<%
			  		intSize = alRepList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alRepList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strBillTo.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%
			  		}
%>
							</select>
						</td>
					</tr>
					
				 	<tr><td colspan="2"> 
						<jsp:include page="/operations/shipping/GmIncShipDetails.jsp" />
					</td></tr>
					<tr>
					
		<!--			
		<td  colspan="2" > 
		 <div id="divf" style="display:none;" >
		<iframe src="<%=strServletPath%>/gmIncShipDetails.do?refId="<%=strRequestId%>"&source=50184&strOpt=fetch&screenType=Consign"  scrolling="yes" id="iframe3" marginheight="0" width="100%" height="100" frameborder="0"  ></iframe> 
		</div>  
				 
		</td></tr>	  -->
					<tr class="shade">
						<td class="RightTableCaption" valign="top" align="right" HEIGHT="24"><fmtSetConsign:message key="LBL_FINALCOMMENTS"/>:</td>
						<td>&nbsp;<textarea name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1 rows=5 cols=40 value="<%=strFinalComments%>"><%=strFinalComments%></textarea></td>
					</tr>
<%
				} // end if strSelectedAction
				}
%>
					<tr>
						<td colspan="2">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td height="25" width="60" align="center"><fmtSetConsign:message key="LBL_PART"/><BR><fmtSetConsign:message key="LBL_NUMBER"/></td>
									<td width="330"><fmtSetConsign:message key="LBL_DESCRIPTION"/></td>
									<td width="50"><fmtSetConsign:message key="LBL_QTY"/></td>
									<td width="70"><fmtSetConsign:message key="LBL_CONTROLNUMBER"/></td>
									<td width="70"><fmtSetConsign:message key="LBL_PRICE"/></td>
									<td width="70"><fmtSetConsign:message key="LBL_AMOUNT"/></td>
								</tr>
<%
						intSize = alSetLoad.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();

							String strNextPartNum = "";
							int intCount = 0;
							String strColor = "";
							String strLine = "";
							String strPartNumHidden = "";
							String strPrice = "";
							String strTotal = "";
							String strAmount = "";
							String strTagFl = "";
							double dbAmount = 0.0;
							double dbTotal = 0.0;
							int intQty = 0;
							
							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alSetLoad.get(i);
								if (i<intSize-1)
								{
									hmTempLoop = (HashMap)alSetLoad.get(i+1);
									strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
								}
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
								strPartNumHidden = strPartNum;
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
								strTemp = strPartNum;

								if (strPartNum.equals(strNextPartNum))
								{
									intCount++;
									strLine = "<TR><TD colspan=6 height=1 class=Line></TD></TR>";
								}

								else
								{
									strLine = "<TR><TD colspan=6 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
										strPartNum = "";
										strPartDesc = "";
										strQty = "";
										//strColor = "bgcolor=white";
										strLine = "";
									}
									else
									{
										//strColor = "";
									}
									intCount = 0;
								}

								if (intCount > 1)
								{
									strPartNum = "";
									strPartDesc = "";
									strQty = "";
									//strColor = "bgcolor=white";
									strLine = "";
								}
							
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
								strItemQty = GmCommonClass.parseNull((String)hmLoop.get("IQTY"));
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
								
								strTagFl = GmCommonClass.parseNull((String)hmLoop.get("TAGFL"));
								
								intQty = Integer.parseInt(strItemQty);
								dbAmount = Double.parseDouble(strPrice);
								dbAmount = intQty * dbAmount; // Multiply by Qty
								strAmount = ""+dbAmount;

								dbTotal = dbTotal + dbAmount;
								strTotal = ""+dbTotal;

								out.print(strLine);
%>
								<tr>
									<td class="RightText" height="20">&nbsp;<%if(strTagFl.equals("Y")){%><a href=javascript:fnOpenTag('<%=strPartNum %>','<%=strConsignId%>')> <img border="0" src=<%=strImagePath%>/tag.jpg ></a><%}%><%=strPartNum%></td>
									<td class="RightText"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td class="RightText">&nbsp;<%=strItemQty%></td>
									<td class="RightText">&nbsp;<%=strControl%></td>
									<td class="RightText"  align="center"><%=GmCommonClass.getStringWithCommas(strPrice)%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strAmount)%>&nbsp;&nbsp;&nbsp;</td>
								</tr>
<%
								//out.print(strLine);
							}//End of FOR Loop
%>
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
								<tr>
									<td colspan="4" height="25">&nbsp;</td>
									<td class="RightTableCaption" align="center"><fmtSetConsign:message key="LBL_TOTAL"/></td>
									<gmjsp:currency textValue="<%=strTotal%>" align="right" gmClass="RightTableCaption" type="CurrTextSign"/>
								</tr>
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
<%
						}else {
%>
						<tr><td colspan="6" height="50" align="center" class="RightTextRed"><BR><fmtSetConsign:message key="LBL_PARTMAPPEDSET"/>No Part Numbers have been mapped for this Set !</td></tr>
<%
						}
%>
							</table>
						</td>
					</tr>

<%if(!strRequestId.equals("")) { %>
				<tr><td colspan="2" class="Line" height="1"></td></tr>
		<tr>
			<td colspan="2">
					<jsp:include page="/operations/logistics/GmBackorderReport.jsp" >
					<jsp:param name="hConsignmentId" value="<%=strConsignmentId%>" />
					<jsp:param name="hRequestId" value="<%=strRequestId%>" />
					<jsp:param name="FORMNAME" value="" />
					</jsp:include>	
			</td>	
		</tr>		
<%} %>		 
				 </table>
			 </td>
		</tr>
				
	  	<tr><td colspan="2">
		<jsp:include  flush="true" page="/common/GmRuleDisplayInclude.jsp"/>
		</td></tr>
		 
	
		
<%
					if (strMode.equals("CON") && !bolFl){
%>
					<tr>
						<td colspan="2" height="30" align="center">
							<%if(bolShowComPolBtn){ %>
								<fmtSetConsign:message key="BTN_MOVETOCOMMON" var="varMoveCommon"/>
								<gmjsp:button value="${varMoveCommon}" tabindex="35" gmClass="button" disabled="<%=strEnableComPolBtn%>" onClick="javascript:fnMoveToCommonPool();" buttonType="Save" />&nbsp;
							<%} %>
							<fmtSetConsign:message key="BTN_SUBMIT" var="varSubmit"/>
							<gmjsp:button value="${varSubmit}" gmClass="button" onClick="fnSubmit();" tabindex="13" buttonType="Save" />
						</td>
					</tr>
<%
					}else if (strMode.equals("CON") && !bolFl){
%>
					<tr>
						<td colspan="2" height="30" align="center">
							<fmtSetConsign:message key="BTN_SUBMIT" var="varSubmit"/>
							<fmtSetConsign:message key="BTN_AGREEMENTFORM" var="varAgreementForm"/>
							<fmtSetConsign:message key="BTN_PRINTVERSION" var="varPrintversion"/>
							<gmjsp:button value="${varSubmit}" gmClass="button" onClick="fnSubmit();" tabindex="13" buttonType="Save" />&nbsp;&nbsp;
							<gmjsp:button value="${BTN_AGREEMENTFORM}" gmClass="button" onClick="fnAgree();" tabindex="13" buttonType="Save" />&nbsp;&nbsp;
							<gmjsp:button value="${BTN_PRINTVERSION}" gmClass="button" onClick="fnPrintVer();" tabindex="13" buttonType="Load" />
						</td>
					</tr>
<%
					}else if (strMode.equals("INV")){
%>
					<tr>
						<td colspan="2" height="30" align="center">
							<fmtSetConsign:message key="BTN_PUTBACKINV" var="varPutBackInve"/>
							<gmjsp:button value="${varPutBackInve}" gmClass="button" onClick="fnInv();" tabindex="13" buttonType="Save" />
						</td>
					</tr>
<%
					}else if (strMode.equals("ROLL")){
%>
					<tr>
						<td colspan="2" height="30" align="center" class="RightTextRed">
						<fmtSetConsign:message key="LBL_MODEROLL1"/><br><fmtSetConsign:message key="LBL_MODEROLL2"/>
						</td>
					</tr>
<%
					}else if (strMode.equals("AlloPic")){
%>
					<tr>
						<td colspan="2" height="30" align="center" class="RightTextRed">
							<fmtSetConsign:message key="BTN_PRINTPICSLIP" var="varPrintPicSlip"/>
							<gmjsp:button value="${varPrintPicSlip}" gmClass="button" onClick="fnPicSlip();" tabindex="13" buttonType="Load" />
						</td>
					</tr>
<%
					}
%>
		<tr>
		<td  colspan="2" > 
		 <div id="divf" style="display:none;" >
		<iframe src="<%=strServletPath%>/GmConsignSetServlet?hMode=CON&hAction=EditLoad&hRequestId=<%=strRequestId%>&refName=RFS&demandSheetMonthID=<%=strDemandSheetMonthID%>&initiatedFrom=<%=strInitiatedFrom%>&companyInfo=<%=strCompanyInfo%>" scrolling="yes" id="iframe1" marginheight="0" width="100%" height="100" frameborder="0"  ></iframe> 
		</div>  
				 
		</td></tr>	
    </table>

</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>
<%@ include file="/common/GmFooter.inc" %>
</HTML>
