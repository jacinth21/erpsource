
<%
/**********************************************************************************
 * File		 		: GmCustServHome.jsp
 * Desc		 		: This screen is used for the DashBoard Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtCustServHome" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ include file="/common/GmHeader.inc" %>
<!-- custservice\GmCustServHome.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtCustServHome:setLocale value="<%=strLocale%>"/>
<fmtCustServHome:setBundle basename="properties.labels.custservice.GmCustServHome"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);




	String strDeptId = (String)session.getAttribute("strSessDeptId")==null?"":(String)session.getAttribute("strSessDeptId");
	String strAccessLvl = (String)session.getAttribute("strSessAccLvl")==null?"":(String)session.getAttribute("strSessAccLvl");

	int intAccessLvl = Integer.parseInt(strAccessLvl);
	boolean bolAccess = false;

	if ((strDeptId.equals("O") && intAccessLvl > 3))
	{
		bolAccess = true;
	}


	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	ArrayList alControl = new ArrayList();
	alControl = (ArrayList)hmReturn.get("CNTRL");
	
	HashMap hmSales = new HashMap();
	hmSales = (HashMap)hmReturn.get("SALES");

	ArrayList alSets = new ArrayList();
	alSets = (ArrayList)hmReturn.get("SETS");
	
	ArrayList alPendingTransfers = new ArrayList();
	alPendingTransfers = (ArrayList)hmReturn.get("TRANSFERS");
	
	ArrayList alDummyConsignment = new ArrayList();
	alDummyConsignment = (ArrayList)hmReturn.get("DUMMYCONSG");
	
	ArrayList alLoanerDash = new ArrayList();
	alLoanerDash = (ArrayList)hmReturn.get("LOANERS");
	
	int intLength = alControl.size();

	int intSetLength = (alSets == null)?0:alSets.size();
	int intTransferLength = (alPendingTransfers == null)?0:alPendingTransfers.size();
	int intDummyConsignment = (alDummyConsignment == null)?0:alDummyConsignment.size();
	int intLoanerLen = (alLoanerDash == null)?0:alLoanerDash.size();
	

	HashMap hmLoop = new HashMap();

	String strId = "";
	String strName = "";
	String strCount = "";
	String strSum = "";
	String strCntrlFl = "";
	String strShipFl = "";
	String strPOFl = "";
	String strOrdDate = "";
	String strCSPerson = "";
	String strRedTextControl = "";
	String strRedTextShip = "";
	String strRedTextShipLink = "";
	String strRedTextPO = "";
	String strSales = "";
	String strMonthSales = "";
	String strOrderAmt = "";
	String strDistName = "";
					
	String strTemp1 = "";
	String strTemp2 = "";
	String strTemp3 = "";

	String strConId = "";
	String strSetName = "";
	String strPerson = "";
	String strDateInitiated = "";
	String strComments = "";

	String strRAId = "";
	String strReturnType = "";
	String strReturnReason = "";
	String strExpectedDate = "";
	String strRedTextShipNoLink = "<a class=RightTextRed>Y</a>";
	
    String strTransferID = "";
    String strFromName = "";
	String strToName = "";
	String strDate = "";
	String strReason = "";
	String strType = "";
	String strInitiatorName = "";
	
	String strDummyID = "";
	String strTransId = "";
	String strEtchId = "";
	String strRetDate = "";
	
	int intLoop = 0;

    if (hmSales != null)
    {
		HashMap hmDaySales = (HashMap)hmSales.get("DAYSALES");
		HashMap hmMonthSales = (HashMap)hmSales.get("MONTHSALES");
		strSales = (String)hmDaySales.get("SALES");
		strMonthSales = (String)hmMonthSales.get("SALES");
    }
	String strShade = "";
	String strDistId = "";
	String strLine = "";
	HashMap hmTempLoop = new HashMap();
	String strNextId = "";
	int intCount = 0;
	String strVendorId = "";
	boolean blFlag = false;
	boolean blFlag1 = false;
	String strAccId = "";
	String strOrderType = "";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Customer Service Dashboard </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script>
function fnSubmit(val)
{
	document.frmAccount.hColumn.value = val;
	document.frmAccount.submit();
}

function fnCallEdit(val)
{
	document.frmAccount.hId.value = val;
	document.frmAccount.submit();
}

function fnCallControl(val)
{
	document.frmAccount.hOrdId.value = val;
	document.frmAccount.hAction.value = "EditControl";
	document.frmAccount.action = "/GmOrderItemServlet";
	document.frmAccount.submit();
}

function fnCallShip(val)
{
	document.frmAccount.hOrdId.value = val;
	document.frmAccount.hAction.value = "EditShip";
	document.frmAccount.action = "/GmOrderItemServlet";
	document.frmAccount.submit();
}

function fnCallPO(val)
{
	document.frmAccount.hId.value = val;
	document.frmAccount.hMode.value = "PO";
	document.frmAccount.submit();
}

function fnCallEditItems(val)
{   
	document.frmAccount.hConsignId.value = val;
	document.frmAccount.hAction.value = "EditControl";
	document.frmAccount.action = "/GmConsignItemServlet";
	document.frmAccount.submit();
}

function fnCallSetShip(val)
{
	document.frmAccount.hId.value = val;
	document.frmAccount.hAction.value = "LoadSetShip";
	document.frmAccount.action = "/GmConsignSetServlet";
	document.frmAccount.submit();	
}

function fnCallEditTransfer(val)
{
	document.frmAccount.hTransferId.value = val;
	document.frmAccount.hMode.value="Load";
	document.frmAccount.action = "/GmAcceptTransferServlet";	
	document.frmAccount.submit();	
}

function fnCallEditDummyConsignment(val)
{
	document.frmAccount.hId.value = val;
	document.frmAccount.hAction.value="LoadSetDummy";
	document.frmAccount.action = "/GmConsignSetServlet";	
	document.frmAccount.submit();	
}

function fnCallRecon(val)
{
	document.frmAccount.hId.value = val;
	document.frmAccount.hAction.value = "LoanRecon";
	document.frmAccount.action = "/GmLoanerReconServlet";
	document.frmAccount.submit();	
}


function fnPrintPick(val)
{
	windowOpener('<%=strServletPath%>/GmOrderItemServlet?hAction=PICSlip&hId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
}

function fnViewOrder(val)
{
	windowOpener('<%=strServletPath%>/GmEditOrderServlet?hMode=PrintPrice&hOrdId='+val,"Summ","resizable=yes,scrollbars=yes,top=250,left=250,width=760,height=410");	
}

function fnCallReturn(val)
{
	document.frmAccount.hRAId.value = val;
	document.frmAccount.hAction.value = "CreditReturn";
	document.frmAccount.action = "/GmProcessReturnsServlet";
	document.frmAccount.submit();
}

function Toggle(val)
{
	
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className="";
		tabobj.style.background="#ffffff";
	}
}

function fnPicSlip(val)
{
	windowOpener("<%=strServletPath%>/GmConsignItemServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=600");
}

function fnInHousePicSlip(val)
{
	windowOpener("<%=strServletPath%>/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmCustFrameServlet">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hPgToLoad" value="GmEditOrderServlet">
<input type="hidden" name="hSubMenu" value="Trans">
<input type="hidden" name="hMode" value="">
<input type="hidden" name="hFrom" value="Dashboard">
<input type="hidden" name="hPO" value="">
<input type="hidden" name="hOrdId" value="">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hConsignId" value="">
<input type="hidden" name="hTransferId" value="">
<input type="hidden" name="hRAId" value="">
	<table border="0" width="601" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="5" bgcolor="black"></td>
		</tr>
		<tr>
			<td bgcolor="black" width="1"></td>
			<td width="300" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" class="RightDashBoardHeader" align="center">
							<fmtCustServHome:message key="LBL_TODAYS_SALE"/> - $ <%=GmCommonClass.getStringWithCommas(strSales)%>
						</td>
					</tr>
				</table>
			</td>
			<td bgcolor="black" width="1"></td>
			<td width="300" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" class="RightDashBoardHeader" align="center">
							<fmtCustServHome:message key="LBL_MONTH_TO_SALE"/> - $ <%=GmCommonClass.getStringWithCommas(strMonthSales)%>
						</td>
					</tr>
				</table>
			</td>
			<td bgcolor="black" width="1"></td>
		</tr>
		<tr>
			<td colspan="5" bgcolor="black"></td>
		</tr>
	</table>
<%
			if (strDeptId.equals("C") || strDeptId.equals("Z") || strDeptId.equals("D") || (strDeptId.equals("O") && intAccessLvl == 2 ))
			{
%>
<BR>
	<table border="0" width="1000" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="998" valign="top">
			<div style="overflow:auto; height:300px;">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				  <thead>
					<tr style="position:relative; top:expression(this.offsetParent.scrollTop);">
						<td colspan="8" height="25" class="RightDashBoardHeader"><fmtCustServHome:message key="LBL_DASHBOARD"/></td>
					</tr>
					<tr style="position:relative; top:expression(this.offsetParent.scrollTop);"><td class="Line" colspan="8"></td></tr>
					<tr bgcolor="#eeeeee" class="RightTableCaption" style="position:relative; top:expression(this.offsetParent.scrollTop);">
						<th width="60" ><fmtCustServHome:message key="LBL_ACCOUNT_NAME"/></th>
						<th HEIGHT="24" width="100"><fmtCustServHome:message key="LBL_DELIVERED_ORDER_ID"/><BR></th>
						<th width="120" align="center"><fmtCustServHome:message key="LBL_ORDER_TYPE"/></th>
						<th width="80" align="center"><fmtCustServHome:message key="LBL_AMOUNT"/></th>
						<th width="60" align="center"><fmtCustServHome:message key="LBL_PENDING_CONTROL"/></th>
						<th width="60" align="center"><fmtCustServHome:message key="LBL_PENDING_SHIPPING_VERIFICATION"/><BR></th>
						<th width="140" nowrap align="center"><fmtCustServHome:message key="LBL_ORDER_DATE"/></th>
						<th width="130" nowrap ><fmtCustServHome:message key="LBL_ORDER_RECIEVED_BY"/></th>
					</tr>
				  </thead>
				  <tbody>
<%
						
						if (intLength > 0)
						{
							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alControl.get(i);
								if (i<intLength-1)
								{
									hmTempLoop = (HashMap)alControl.get(i+1);
									strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("ACCID"));
								}								
								strAccId = GmCommonClass.parseNull((String)hmLoop.get("ACCID"));
								strId	= GmCommonClass.parseNull((String)hmLoop.get("ID"));
								strName = GmCommonClass.parseNull((String)hmLoop.get("NAME"));
								strOrderType = GmCommonClass.parseNull((String)hmLoop.get("OTYPE"));
								strCount = GmCommonClass.parseNull((String)hmLoop.get("CNT"));
								strSum = GmCommonClass.parseNull((String)hmLoop.get("SUM"));
								strOrdDate = GmCommonClass.parseNull((String)hmLoop.get("DT"));
								strCSPerson = GmCommonClass.parseNull((String)hmLoop.get("CS"));
								strOrderAmt = GmCommonClass.parseNull((String)hmLoop.get("TOTAL"));
								strRedTextControl = "<a class=RightTextRed href= javascript:fnCallControl('"+strId+"');>Y</a>";
								strRedTextShip = "<a class=RightTextRed>Y</a>";
								strRedTextShipLink = "<a class=RightTextRed href= javascript:fnCallShip('"+strId+"');>Y</a>";
								strTemp1 = (String)hmLoop.get("CFL");
								strTemp2 = (String)hmLoop.get("SFL");
								strCntrlFl = strTemp1.equals("Y")?strRedTextControl:"--";
								if (strTemp1.equals("Y"))
								{
									strShipFl = strRedTextShip;
								}
								else
								{
									strShipFl = strTemp2.equals("Y")?strRedTextShipLink:"--";
								}

								strShade	= (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows

								if (strAccId.equals(strNextId))
								{
									intCount++;
									strLine = "<TR><TD colspan=8 height=1 class=Line></TD></TR>";
								}
								else
								{
									blFlag1 = true;
									strLine = "<TR><TD colspan=8 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
										strName = "";
										strLine = "";
									}
									else
									{
										blFlag = true;
									}
									intCount = 0;
								}
								if (intCount > 1)
								{
									strName = "";
									strLine = "";
								}

								strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
								out.print(strLine);
								if (intCount == 1 || blFlag)
								{
%>
					<tr id="trAcc<%=strAccId%>">
						<td colspan="8" height="18" class="RightText">&nbsp;<A class="RightText" title="Click to Expand" href="javascript:Toggle('Acc<%=strAccId%>')"><%=strName%></a></td>
					</tr>
					<tr>
						<td colspan="8"><div style="display:none" id="divAcc<%=strAccId%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tabAcc<%=strAccId%>">
<%
								blFlag = false;
								}
%>

					<tr <%=strShade%>>
						<td width="30" class="RightText">&nbsp;</td>
						<td width="140" class="RightText" height="20">&nbsp;<a href="javascript:fnPrintPick('<%=strId%>');"><img src="<%=strImagePath%>/packslip.gif" border=0></a>
						<a href="javascript:fnViewOrder('<%=strId%>');"><img src="<%=strImagePath%>/ordsum.gif" border=0></a>&nbsp;<%=strId%></td>
						<td width="120" class="RightText" align="center"><%=strOrderType%>&nbsp;</td>
						<td width="80" class="RightText" align="right">$<%=GmCommonClass.getStringWithCommas(strOrderAmt)%>&nbsp;</td>
						<td width="60" class="RightText" align="center"><%=strCntrlFl%></td>
						<td width="60" class="RightText" align="center"><%=strShipFl%></td>
						<td width="140" class="RightText">&nbsp;<%=strOrdDate%></td>
						<td width="130" class="RightText">&nbsp;<%=strCSPerson%></td>
					</tr>
<%
								if (blFlag1 || i+1==intLength)
								{
%>
							</table></div>
						</td>
					</tr>
<%
								}
								blFlag1	= false;
							} // END of FOR loop
						} else	{
%>
					<tr>
						<td height="30" colspan="8" align="center" class="RightTextBlue"><fmtCustServHome:message key="LBL_NO_WIP_ORDERS"/> !</td>
					</tr>
<%
					}
%>
				  </tbody>
				</table>
			  </DIV>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>

<BR>
				<table border="0" width="1000" cellspacing="0" cellpadding="0">
					<tr>
						<td rowspan="<%=intSetLength*3%>" width="1" class="Line"></td>
						<td colspan="7" height="1" bgcolor="#666666"></td>
						<td rowspan="<%=intSetLength*3%>" width="1" class="Line"></td>
					</tr>
					<tr>
						<td colspan="7" height="25" class="RightDashBoardHeader">
							<fmtCustServHome:message key="LBL_DASHBOARD_SETS_ITEM_PROCESS"/>
						</td>
					</tr>
					<tr><td class="Line" height="1" colspan="7"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="80"><fmtCustServHome:message key="LBL_AGENT"/></td>
						<td HEIGHT="24" width="80"><fmtCustServHome:message key="LBL_CONSIGN_ID"/></td>
						<td width="400">&nbsp;<fmtCustServHome:message key="LBL_SET_NAME"/></td>
						<td align="center">&nbsp;<fmtCustServHome:message key="LBL_PENDING_CONTROL"/> &nbsp;</td>
						<td align="center">&nbsp;<fmtCustServHome:message key="LBL_PENDING_SHIPPING"/>&nbsp;</td>
						<td width="130" align="center"><fmtCustServHome:message key="LBL_BUILT_BY"/></td>
						<td width="300">&nbsp;&nbsp;<fmtCustServHome:message key="LBL_COMMENTS"/></td>
					</tr>
<%
	hmTempLoop = new HashMap();
	strNextId = "";
	intCount = 0;
	blFlag = false;
	blFlag1 = false;
	
						if (intSetLength > 0)
						{

							for (int i = 0;i < intSetLength ;i++ )
							{
								hmLoop = (HashMap)alSets.get(i);
								if (i<intSetLength-1)
								{
									hmTempLoop = (HashMap)alSets.get(i+1);
									strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("DID"));
									strNextId = strNextId.equals("")?GmCommonClass.parseNull((String)hmTempLoop.get("ACCID")):strNextId;
								}
								strDistId = GmCommonClass.parseNull((String)hmLoop.get("DID"));
								strDistId = strDistId.equals("")?GmCommonClass.parseNull((String)hmLoop.get("ACCID")):strDistId;
								strDistName = GmCommonClass.parseNull((String)hmLoop.get("DNAME"));
								strDistName = strDistName.equals("")?GmCommonClass.parseNull((String)hmLoop.get("ANAME")):strDistName;
								strConId = GmCommonClass.parseNull((String)hmLoop.get("CID"));
								strSetName = GmCommonClass.parseNull((String)hmLoop.get("SNAME"));
								strSetName = strSetName.equals("")?"Item Consignment":strSetName;
								strPerson = GmCommonClass.parseNull((String)hmLoop.get("PER"));
								strDateInitiated = GmCommonClass.parseNull((String)hmLoop.get("IDATE"));
								strComments = GmCommonClass.parseNull((String)hmLoop.get("COMMENTS"));
								strRedTextShip = "<a class=RightTextRed href= javascript:fnCallSetShip('"+strConId+"');>Y</a>";
								strRedTextControl = "<a class=RightTextRed alt='Click to Consign this Set' href= javascript:fnCallEditItems('"+strConId+"');>Y</a>";
								strTemp1 = (String)hmLoop.get("SHIPREQFL");
								strTemp2 = (String)hmLoop.get("CONSFL");

								strCntrlFl = strTemp2.equals("Y")?strRedTextControl:"--";
								
								if (strTemp1.equals("1"))
								{
									strShipFl = strTemp2.equals("Y")?strRedTextShipNoLink:strRedTextShip;
								}
								else
								{
									strShipFl = "--";
								}
								
								if (strDistId.equals(strNextId))
								{
									intCount++;
									strLine = "<TR><TD colspan=7 height=1 class=Line></TD></TR>";
								}
								else
								{
									blFlag1 = true;
									strLine = "<TR><TD colspan=7 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
										strDistName = "";
										strLine = "";
									}
									else
									{
										blFlag = true;
									}
									intCount = 0;
								}
								if (intCount > 1)
								{
									strDistName = "";
									strLine = "";
								}

								strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
								out.print(strLine);
								if (intCount == 1 || blFlag)
								{
%>
					<tr id="tr<%=strDistId%>">
						<td colspan="7" height="18" class="RightText">&nbsp;<A class="RightText" title="Click to Expand" href="javascript:Toggle('<%=strDistId%>')"><%=strDistName%></a></td>
					</tr>
					<tr>
						<td colspan="7"><div style="display:none" id="div<%=strDistId%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tab<%=strDistId%>">
<%
								blFlag = false;
								}
%>

					<tr <%=strShade%>>
						<td colspan="2" width="120" class="RightText" height="20">&nbsp;&nbsp;&nbsp;<a class="RightText" href="javascript:fnPicSlip('<%=strConId%>');"><%=strConId%></a></td>
						<td width="350" class="RightText">&nbsp;&nbsp;<%=GmCommonClass.getStringWithTM(strSetName)%></td>
						<td class="RightText" width="50" align="center">&nbsp;<%=strCntrlFl%></td>
						<td class="RightText" width="50" align="center">&nbsp;<%=strShipFl%></td>
						<td class="RightText" align="center"><%=strPerson%></td>
						<td class="RightText" width="300"><%=strComments%></td>
					</tr>
					<tr><td colspan="7" bgcolor="#cccccc" height="1"></td></tr>
<%
								if (blFlag1 || i+1==intSetLength)
								{
%>
							</table></div>
						</td>
					</tr>
<%
								}
							blFlag1	= false;
							}
						} else	{
%>
					<tr>
						<td height="30" colspan="7" align="center" class="RightTextBlue"><fmtCustServHome:message key="LBL_NO_CONSIGNED_SETS"/> !</td>
					</tr>
<%
					}
			}
%>
					<tr><td colspan="8" height="1" bgcolor="#666666"></td></tr>
				</table>

<BR>
<%
	if (strDeptId.equals("C") || strDeptId.equals("Z") || strDeptId.equals("M"))
	 {
%>
				<table border="0" width="1000" cellspacing="0" cellpadding="0">
					<tr>
						<td rowspan="<%=intLoanerLen*10%>" width="1" class="Line"></td>
						<td colspan="9" height="1" bgcolor="#666666"></td>
						<td rowspan="<%=intLoanerLen*10%>" width="1" class="Line"></td>
					</tr>
					<tr>
						<td colspan="9" height="25" class="RightDashBoardHeader"><fmtCustServHome:message key="LBL_DASHBOARD_LOANER_RECONCILIATION"/></td>
					</tr>
					<tr><td class="Line" height="1" colspan="9"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="60"><fmtCustServHome:message key="LBL_AGENT"/></td>
						<td width="100"><fmtCustServHome:message key="LBL_TRANS_ID"/></td>
						<td width="100"><fmtCustServHome:message key="LBL_CONS_ID"/></td>
						<td width="80"><fmtCustServHome:message key="LBL_ETCH_ID"/></td>
						<td width="400">&nbsp;<fmtCustServHome:message key="LBL_SET_NAME"/></td>
						<td width="80" align="center">&nbsp;<fmtCustServHome:message key="LBL_LOANED_DATE"/></td>
						<td width="80" align="center">&nbsp;<fmtCustServHome:message key="LBL_RETURNED_DATE"/></td>
						<td width="80" align="center">&nbsp;<fmtCustServHome:message key="LBL_RECIEVED_DATE"/></td>
						<td width="80" align="center">&nbsp;<fmtCustServHome:message key="LBL_PENDING_RECONCILIATION"/>&nbsp;</td>
					</tr>
<%
						hmTempLoop = new HashMap();
						strNextId = "";
						intCount = 0;
						blFlag = false;
						blFlag1 = false;
	
						if (intLoanerLen > 0)
						{

							for (int i = 0;i < intLoanerLen ;i++ )
							{
								hmLoop = (HashMap)alLoanerDash.get(i);
								if (i<intLoanerLen-1)
								{
									hmTempLoop = (HashMap)alLoanerDash.get(i+1);
									strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("DID"));
								}
								strDistId = GmCommonClass.parseNull((String)hmLoop.get("DID"));
								strDistName = GmCommonClass.parseNull((String)hmLoop.get("DNAME"));
								strConId = GmCommonClass.parseNull((String)hmLoop.get("CONID"));
								strSetName = GmCommonClass.parseNull((String)hmLoop.get("SNAME"));
								strTransId = GmCommonClass.parseNull((String)hmLoop.get("INHID"));
								strEtchId = GmCommonClass.parseNull((String)hmLoop.get("ETCHID"));
								strRetDate = GmCommonClass.parseNull((String)hmLoop.get("RDATE"));
								strDate = GmCommonClass.parseNull((String)hmLoop.get("LDATE"));
								strPerson = GmCommonClass.parseNull((String)hmLoop.get("CUSER"));
								strComments = GmCommonClass.parseNull((String)hmLoop.get("COMMENTS"));
								
								if (strDistId.equals(strNextId))
								{
									intCount++;
									strLine = "<TR><TD colspan=9 height=1 class=Line></TD></TR>";
								}
								else
								{
									blFlag1 = true;
									strLine = "<TR><TD colspan=9 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
										strDistName = "";
										strLine = "";
									}
									else
									{
										blFlag = true;
									}
									intCount = 0;
								}
								if (intCount > 1)
								{
									strDistName = "";
									strLine = "";
								}

								strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
								out.print(strLine);
								if (intCount == 1 || blFlag)
								{
%>
					<tr id="trLR<%=strDistId%>">
						<td colspan="9" height="18" class="RightText">&nbsp;<A class="RightText" title="Click to Expand" href="javascript:Toggle('LR<%=strDistId%>')"><%=strDistName%></a></td>
					</tr>
					<tr>
						<td colspan="9"><div style="display:none" id="divLR<%=strDistId%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tabLR<%=strDistId%>">
<%
								blFlag = false;
								}
%>

					<tr <%=strShade%>>
						<td class="RightText" width="40" align="right"><a class="RightText" href="javascript:fnInHousePicSlip('<%=strTransId%>');"><img src="<%=strImagePath%>/packslip.gif" border=0></a>&nbsp;</td>
						<td nowrap width="90" class="RightText" height="20">&nbsp;<%=strTransId%></td>
						<td class="RightText" width="90" align="center">&nbsp;<%=strConId%></td>
						<td class="RightText" width="80" align="center">&nbsp;<%=strEtchId%></td>
						<td width="350" class="RightText">&nbsp;&nbsp;<%=GmCommonClass.getStringWithTM(strSetName)%></td>
						<td class="RightText" width="80" align="center">&nbsp;<%=strDate%></td>
						<td class="RightText" width="80" align="center">&nbsp;<%=strRetDate%></td>
						<td class="RightText" width="80" align="center">&nbsp;<%=strPerson%></td>
						<td class="RightText" width="80" align="center">&nbsp;<a class=RightTextRed href= javascript:fnCallRecon('<%=strTransId%>');>Y</a></td>
					</tr>
					<tr><td colspan="9" bgcolor="#cccccc" height="1"></td></tr>
<%
								if (blFlag1 || i+1==intLoanerLen)
								{
%>
							</table></div>
						</td>
					</tr>
<%
								}
							blFlag1	= false;
							}
						} else	{
%>
					<tr>
						<td height="30" colspan="9" align="center" class="RightTextBlue"><fmtCustServHome:message key="LBL_NO_PENDING_RECONCILIATION"/> !</td>
					</tr>
<%
					}
%>
					<tr><td colspan="9" height="1" bgcolor="#666666"></td></tr>
				</table>
<BR>

<BR>
	<table border="0" width="1000" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="1000" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="7" height="25" class="RightDashBoardHeader">
							<fmtCustServHome:message key="LBL_DASHBOARD_CONSIGNED_TRANSFER"/>
						</td>
					</tr>
					<tr><td class="Line" colspan="7"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="16%"><fmtCustServHome:message key="LBL_TRANSFER_ID"/></td>						
						<td width="14%" align="center"><fmtCustServHome:message key="LBL_FROM"/></td>
						<td width="14%" align="center"><fmtCustServHome:message key="LBL_TO"/></td>
						<td width="14%" align="center"><fmtCustServHome:message key="LBL_DATE"/></td>
						<td width="14%"><fmtCustServHome:message key="LBL_REASON"/></td>
						<td width="14%" align="center"><fmtCustServHome:message key="LBL_TYPE"/></td>
						<td width="14%"><fmtCustServHome:message key="LBL_INITIATED_BY"/></td>						
					</tr>
					<tr><td class="Line" colspan="7"></td></tr>
<%			if( intTransferLength > 0) {
							HashMap hmPendingTransfer = new HashMap();
							for (int k = 0;k < intTransferLength ;k++ )
							{
								hmPendingTransfer = (HashMap)alPendingTransfers.get(k);

								strTransferID = GmCommonClass.parseNull((String)hmPendingTransfer.get("TSFID"));
								strFromName = GmCommonClass.parseNull((String)hmPendingTransfer.get("FROMNAME"));
								strToName = GmCommonClass.parseNull((String)hmPendingTransfer.get("TONAME"));
								strDate = GmCommonClass.parseNull((String)hmPendingTransfer.get("TSFDATE"));
								strReason = GmCommonClass.parseNull((String)hmPendingTransfer.get("TSFREASON"));
								strType = GmCommonClass.parseNull((String)hmPendingTransfer.get("TSFTYPE"));
								strInitiatorName = GmCommonClass.parseNull((String)hmPendingTransfer.get("INITIATORNAME"));
%>
					<tr>
					<td HEIGHT="20" class="RightText">&nbsp;<a class=RightText href= javascript:fnCallEditTransfer('<%=strTransferID%>');><%=strTransferID%></td>						
						<td class="RightText" align="center"><%=strFromName%></td>
						<td class="RightText" align="center"><%=strToName%></td>
						<td class="RightText" align="center"><%=strDate%></td>
						<td class="RightText">&nbsp;<%=strReason%></td>
						<td class="RightText">&nbsp;<%=strType%></td>
						<td class="RightText">&nbsp;<%=strInitiatorName%></td>
					</tr>
					<tr><td colspan="7" bgcolor="#eeeeee" height="1"></td></tr>

<%						}
						}else{
%>												
					<tr>
						<td colspan="7" align="center" class="RightTextBlue" height="30"><fmtCustServHome:message key="LBL_NO_PENDING_CONSIGNED_TRANSFER"/>!</td>
					</tr>
<%                  }
%>					
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
	</table>
<%
	 }
%>	
<br>
<%
	if (strDeptId.equals("C") || strDeptId.equals("Z") || strDeptId.equals("M"))
	 {
%>
	<table border="0" width="1000" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="1000" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="5" height="25" class="RightDashBoardHeader">
							<fmtCustServHome:message key="LBL_DASHBOARD_DUMMY_CONSIGNMENT"/>
						</td>
					</tr>
					<tr><td class="Line" colspan="5"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="20%"><fmtCustServHome:message key="LBL_ID"/></td>						
						<td width="20%" align="center"><fmtCustServHome:message key="LBL_FROM"/></td>
						<td width="20%" align="center"><fmtCustServHome:message key="LBL_DATE"/></td>
						<td width="20%"><fmtCustServHome:message key="LBL_INITIATED_BY"/></td>								
						<td width="20%"><fmtCustServHome:message key="LBL_COMMENTS"/></td>				
					</tr>
					<tr><td class="Line" colspan="5"></td></tr>
<%			if( intDummyConsignment > 0) {
							HashMap hmDummyConsignment = new HashMap();
							for (int k = 0;k < intDummyConsignment ;k++ )
							{
								hmDummyConsignment = (HashMap)alDummyConsignment.get(k);

								strDummyID = GmCommonClass.parseNull((String)hmDummyConsignment.get("CONDID"));
								strFromName = GmCommonClass.parseNull((String)hmDummyConsignment.get("FROMNAME"));
								strDate = GmCommonClass.parseNull((String)hmDummyConsignment.get("CONDDATE"));
								strInitiatorName = GmCommonClass.parseNull((String)hmDummyConsignment.get("INITIATORNAME"));								
								strComments = GmCommonClass.parseNull((String)hmDummyConsignment.get("COMMENTS"));
%>
					<tr>
					<td HEIGHT="20" class="RightText">&nbsp;<a class=RightText href= javascript:fnCallEditDummyConsignment('<%=strDummyID%>');><%=strDummyID%></td>						
						<td class="RightText" align="center"><%=strFromName%></td>						
						<td class="RightText" align="center"><%=strDate%></td>
						<td class="RightText">&nbsp;<%=strInitiatorName%></td>
						<td class="RightText">&nbsp;<%=strComments%></td>						
					</tr>
					<tr><td colspan="7" bgcolor="#eeeeee" height="1"></td></tr>

<%						}
						}else{
%>												
					<tr>
						<td colspan="5" align="center" class="RightTextBlue" height="30"><fmtCustServHome:message key="LBL_NO_DUMMY_CONSIGNMENT"/> !</td>
					</tr>
<%                  }
%>					
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
	</table>
<%
	 }
%>	
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>
</HTML>
