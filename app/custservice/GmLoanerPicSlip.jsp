

<%
/**********************************************************************************
 * File		 		: GmLoanerPicSlip.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtLoanerPicSlip" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}

%>
<fmtLoanerPicSlip:setLocale value="<%=strLocale%>"/>
<fmtLoanerPicSlip:setBundle basename="properties.labels.custservice.GmLoanerPicSlip"/>

<!--  \custservice\GmLoanerPicSlip.jsp-->
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strProjId =	"";
	String strSetId = "";
	String strSetNm = "";
	String strDesc = "";
	String strTemp = "";
	String strShade = "";
	String strConsignId = "";
	String strSetName= "";
	String strAccName = "";
	String strUserName = "";
	String strIniDate = "";
	String strRequestorName = "";
	String strDistName = "";

	ArrayList alSetLoad = new ArrayList();
	HashMap hmConsignDetails = new HashMap();

	int intPriceSize = 0;

	if (hmReturn != null)
	{
		alSetLoad = (ArrayList)hmReturn.get("SETLOAD");
		strSetId = (String)session.getAttribute("hSetId") == null?"":(String)session.getAttribute("hSetId");
	
		hmConsignDetails = (HashMap)hmReturn.get("CONDETAILS");
		strConsignId = (String)hmConsignDetails.get("CID");
		strSetName= (String)hmConsignDetails.get("SNAME");
		strAccName = (String)hmConsignDetails.get("ANAME");
		strDistName = (String)hmConsignDetails.get("DNAME");
		strAccName = strAccName.equals("")?strDistName:strAccName;
		strUserName = (String)hmConsignDetails.get("UNAME");
		strIniDate = (String)hmConsignDetails.get("CDATE");
		strDesc = (String)hmConsignDetails.get("FCOMMENTS");
		strRequestorName = GmCommonClass.parseNull((String)hmConsignDetails.get("SHIPADD"));
	}

	int intSize = 0;
	HashMap hcboVal = null;
	
	StringBuffer sbStartSection = new StringBuffer();
	sbStartSection.append("<tr><td colspan=2>");
	sbStartSection.append("<table border=0 width=100% cellspacing=0 cellpadding=0>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption>");
	sbStartSection.append("<td height=24 align=center width=250>&nbsp;Loaned To</td>");
	sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=11></td>");
	sbStartSection.append("<td width=250 align=center>&nbsp;Ship To</td>");
	sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=11></td>");
	sbStartSection.append("<td width=100 align=center>&nbsp;Date</td>");
	sbStartSection.append("<td width=100 align=center>&nbsp;Loaner #</td>");
	sbStartSection.append("</tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
	sbStartSection.append("<tr><td class=RightText rowspan=5 valign=top>&nbsp;");sbStartSection.append(strAccName);
	sbStartSection.append("</td>");
	sbStartSection.append("<td rowspan=5 class=RightText valign=top>&nbsp;");sbStartSection.append(strRequestorName);
	sbStartSection.append("</td>");
	sbStartSection.append("<td height=25 class=RightText align=center>&nbsp;");sbStartSection.append(strIniDate);
	sbStartSection.append("</td><td class=RightText align=center>&nbsp;");
	sbStartSection.append(strConsignId);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption><td align=center>&nbsp;</td><td align=center>Initiated By</td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	sbStartSection.append("<tr><td align=center class=RightText>&nbsp;");
	sbStartSection.append("<Br></td><td align=center height=25 nowrap class=RightText>&nbsp;");sbStartSection.append(strUserName);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr></table></td></tr>");
		
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Consignment PIC Slip </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script>

function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}

</script>


</HEAD>

<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmVendor">
	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td height="80" width="170"><img src="<%=strImagePath%>/s_logo.gif" width="138" height="60"></td>
						<td class="RightText" width="270"><b><fmtLoanerPicSlip:message key="LBL_GLOBUS_MEDICAL"/></font><br><td>
						<td align="right" class="RightText"><font size="+3"><fmtLoanerPicSlip:message key="LBL_LOANER_PIC_SLIP"/></font>&nbsp;</td>
					</tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="698" height="100" valign="top">
			
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
<!--StartSection starts here-->
<%out.print(sbStartSection);%>
<!--StartSection ends here-->
					<tr>
						<td colspan="2"  height="600" valign="top">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr class="RightTableCaption" bgcolor="#eeeeee">
									<td height="25"><fmtLoanerPicSlip:message key="LBL_PART_NUMBER"/></td>
									<td width="200"><fmtLoanerPicSlip:message key="LBL_DESC"/></td>
									<td><fmtLoanerPicSlip:message key="LBL_REQ_QTY"/></td>
									<td width="200">&nbsp;&nbsp;&nbsp;&nbsp;<fmtLoanerPicSlip:message key="LBL_QTY"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtLoanerPicSlip:message key="LBL_CONTROL_NUM"/></td>
								</tr>
								<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
<%
						intSize = alSetLoad.size();
						if (intSize > 0)
						{
							hcboVal = new HashMap();
							for (int i=0;i<intSize;i++)
							{
								hcboVal = (HashMap)alSetLoad.get(i);
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
%>
								<tr>
									<td class="RightText" height="20">&nbsp;<%=(String)hcboVal.get("PNUM")%></td>
									<td class="RightText"><%=GmCommonClass.getStringWithTM((String)hcboVal.get("PDESC"))%></td>
									<td align="center" class="RightText"><%=(String)hcboVal.get("IQTY")%></td>
									<td class="RightText">&nbsp;&nbsp;&nbsp;_ _ _ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_ _ _ _ _ _ _ _ _ _
									</td>
								</tr>
								<tr><td colspan="4" height="1" bgcolor="#eeeeee"></td></tr>
<%
							}
						}else {
%>
						<tr><td colspan="4" height="50" align="center" class="RightTextRed"><BR><fmtLoanerPicSlip:message key="LBL_NO_PART"/></td></tr>
<%
						}		
%>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr valign="top" height="60">
						<td colspan="2" class="RightText"><b><fmtLoanerPicSlip:message key="LBL_COMMENTS"/></b><BR>&nbsp;<%=strDesc%></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr><td class="RightText" colspan="2"><u><fmtLoanerPicSlip:message key="LBL_PLEASE_INITIAL"/></u></td></tr>					
					<tr valign="top" height="60">
						<td class="RightText">&nbsp;&nbsp;&nbsp;<fmtLoanerPicSlip:message key="LBL_INI_REQ_BY"/><BR><BR><BR><BR><BR>&nbsp;&nbsp;&nbsp;(<%=strUserName%>)</td>
						<td class="RightText" align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
				 </table>
			 </td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
    </table>
	<table border="0" width="600" cellspacing="0" cellpadding="0" >
		<tr>
			<td align="center" height="30" id="button">
				<fmtLoanerPicSlip:message key="BTN_PRINT" var="varPrint"/><gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				<fmtLoanerPicSlip:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close()"buttonType="Load" />&nbsp;&nbsp;
			</td>
		<tr>
	</table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
