
<%
/**********************************************************************************
 * File		 		: GmConsignReprocessData.jsp
 * Desc		 		: This screen is used 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtConsignReprocessData" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- custservice\GmConsignReprocessData.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtConsignReprocessData:setLocale value="<%=strLocale%>"/>
<fmtConsignReprocessData:setBundle basename="properties.labels.custservice.GmConsignReprocessData"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction");
	if (strhAction == null)
	{
		strhAction = (String)request.getAttribute("hAction");
	}
	strhAction = (strhAction == null)?"Load":strhAction;

	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strConId = (String)request.getAttribute("hConId")==null?"":(String)request.getAttribute("hConId");
	String strDistId = (String)request.getAttribute("Cbo_Id")==null?"":(String)request.getAttribute("Cbo_Id");
	String strSetId = (String)request.getAttribute("hSetId")==null?"":(String)request.getAttribute("hSetId");
	String strSetIds = (String)request.getAttribute("hSetIds")==null?"":(String)request.getAttribute("hSetIds");

	ArrayList alConsign = new ArrayList();
	ArrayList alReturn = new ArrayList();
	ArrayList alRemainder = new ArrayList();
	
	HashMap hmSets = new HashMap();
	
	int intColspan = 4;
	if (hmReturn != null)
	{
		if (strhAction.equals("Initiate"))
		{
			alConsign = (ArrayList)hmReturn.get("CONSIGN");
			intColspan = intColspan + hmReturn.size()-1;
			//System.out.println("HASHMAP SIZE:"+hmReturn.size());
			alReturn = (ArrayList)hmReturn.get("SETLIST");
		}
	}


	int intSize = 0;
	int intSetSize = 0;
	HashMap hcboVal = null;

	String strPartNum = "";
	String strDesc = "";
	String strQty = "";
	String strComments = "";

	HashMap hmLoop = new HashMap();
	HashMap hmTempLoop = new HashMap();
	int intCount = 0;
	String strLine = "";
	String strTemp = "";
	String strSetQty = "";
	String strSetTemp = "";
	String strControl = "";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Reprocess Consignments </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>
function fnSubmit()
{
	var obj = '';
	var cnt = document.frmMain.hCntRem.value;
	var strConsign = '';
	var strReturn = '';
	for (var i=0;i<cnt;i++)
	{
		obj = eval("document.frmMain.Rad_Rem"+i);
		 if(obj[0].checked == true)
		 {
			strReturn = strReturn + obj[0].value + '^' + obj[0].id + '|';
		 }
		 else if(obj[1].checked == true)
		 {
			strConsign = strConsign + obj[1].value + '^' + obj[1].id + '|';
		}
		
	}

	document.frmMain.hRetStr.value = strReturn;
	document.frmMain.hConStr.value = strConsign;

	var setids = document.frmMain.hSetIds.value;
	var setidsarr = setids.split(",");
	cnt = setidsarr.length;
	for (var i=0;i<cnt;i++)
	{
		var setid = setidsarr[i];
		setid = setid.replace('.','');
		//alert(setid);
		obj = eval("document.frmMain.h"+setid);

		//alert(obj.value);
	}
	document.frmMain.hAction.value = "Save";
	alert(message_operations[706]+document.frmMain.hRetStr.value);
	alert(message_operations[707]+document.frmMain.hConStr.value);
	document.frmMain.submit();
}

function fnReload()
{
	document.frmMain.hAction.value = "Reload";
	document.frmMain.submit();
}

function fnInitiate()
{
	document.frmMain.hAction.value = "Initiate";
	document.frmMain.submit();
}

function fnSetConId(val)
{
	document.frmMain.hConId.value = val;
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10"">
<FORM name="frmMain" method="POST" action="<%=strServletPath%>/GmConsignModifyServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hConId" value="<%=strConId%>">
<input type="hidden" name="hSetIds" value="<%=strSetIds%>">
<input type="hidden" name="hOpt" value="Reprocess">
<input type="hidden" name="hConStr" value="">
<input type="hidden" name="hRetStr" value="">

	<table border="0" width="750" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="748" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td class="Line" colspan="2"></td></tr>
					<tr>
						<td colspan="2" height="25" class="RightDashBoardHeader">&nbsp;<fmtConsignReprocessData:message key="LBL_REPROCESS_CONSIGNMENT"/></td>
					</tr>
					<tr><td class="Line" colspan="2"></td></tr>
					<tr height="24">
						<td colspan="2" class="RightText"><fmtConsignReprocessData:message key="LBL_CONSIGNMENT_ID"/>:<%=strConId%></td>
					</tr>
					<tr><td class="Line" colspan="2"></td></tr>
<%
	if (strhAction.equals("Initiate"))
	{
		StringTokenizer strTok = new StringTokenizer(strSetIds,",");
%>					
					<tr>
						<td colspan="2">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr><td colspan="<%=intColspan%>" class="Line" height="1"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td HEIGHT="24" colspan="<%=intColspan%>"><fmtConsignReprocessData:message key="LBL_CONSIGNMENT_DETAILS"/></td>
								</tr>
								<tr><td colspan="<%=intColspan%>" class="Line" height="1"></td></tr>
								<tr bgcolor="#EEEEEE" class="RightTableCaption">
									<td HEIGHT="24" width="100"><fmtConsignReprocessData:message key="LBL_PART_NUMBER"/></td>
									<td width="350"><fmtConsignReprocessData:message key="LBL_DESCRIPTION"/></td>
									<td width="100" align="center"><fmtConsignReprocessData:message key="LBL_QTY_IN"/><BR><%=strSetId%></td>
									<td width="100" align="center"><fmtConsignReprocessData:message key="LBL_CONTROL"/><BR><%=strSetId%></td>
<%
								while (strTok.hasMoreTokens())
								{
									strSetTemp = strTok.nextToken();
%>									
									<td width="100" align="center"><fmtConsignReprocessData:message key="LBL_SET_QTY"/><BR><%=strSetTemp%></td>
<%
								}
%>									
								</tr>
								<tr><td colspan="<%=intColspan%>" class="Line" height="1"></td></tr>
								<tr>
									<td colspan="<%=intColspan%>">
										<DIV style="overflow:auto; height:200px">
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
<%
						intSize = alConsign.size();
						boolean blFlag = false;
						boolean blFlag1 = false;
						boolean blRepeatFl = false;
						int intQty = 0;
						int intRemQty = 0;
						String strType = "";
						String strRemQty = "";
						String strPrevPNum = "";
						String strDispPNum = "";
						String strId = "";
						
						//System.out.println(">>>>>>>>>>");
						if (intSize > 0)
						{							
							for (int i = 0;i < intSize ;i++ )
							{
								hmLoop = (HashMap)alConsign.get(i);
								
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
								//if (strPartNum.equals("602.401")){
								strDispPNum = strPartNum;
								strDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strQty = GmCommonClass.parseZero((String)hmLoop.get("IQTY")); // Holds original qty
								strRemQty = GmCommonClass.parseZero((String)hmLoop.get("RQTY")); // Holds remaining qty
								intQty = Integer.parseInt(strQty);
								intRemQty = intQty;
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								if (strPrevPNum.equals(strPartNum))
								{
									blRepeatFl = true;
									strDispPNum = "";
									strDesc = "";
								}
								strPrevPNum = strPartNum;
%>
										<tr height="20">
											<td width="100" class="RightText">&nbsp;<%=strDispPNum%></td>
											<td width="350" class="RightText">&nbsp;<%=strDesc%></td>
											<td width="100" bgcolor="#eeeeee" class="RightText" align="center"><%=strQty%></td>
											<td width="100" bgcolor="#eeeeee" class="RightText" align="center"><%=strControl%></td>
<%								
								int intSetRemQty = 0;
								int intAllocQty = 0;
								strTok = new StringTokenizer(strSetIds,",");
								while (strTok.hasMoreTokens())
								{
									strSetTemp = strTok.nextToken();
									alReturn = (ArrayList)hmReturn.get(strSetTemp);
									intSetSize = alReturn.size();
									for (int j = 0;j < intSetSize ;j++ )
									{
										hmTempLoop = (HashMap)alReturn.get(j);
										strTemp = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
										strDesc = GmCommonClass.parseNull((String)hmTempLoop.get("PDESC"));
										strType = GmCommonClass.parseNull((String)hmTempLoop.get("TYPE"));
										strId = GmCommonClass.parseNull((String)hmTempLoop.get("MID"));
										
										//if (strTemp.equals("602.401")){
//System.out.println(">>>>");
										//System.out.println("strSetTemp HERE>>>>"+strSetTemp+":"+strTemp);
										
										strSetQty = GmCommonClass.parseZero((String)hmTempLoop.get("QTY"));
										intSetRemQty = Integer.parseInt(strSetQty);
//System.out.println(">>>>intSetRemQty"+intSetRemQty);
										if (strTemp.equals(strPartNum) && strType.equals(""))
										{
//System.out.println("intQty HERE 111 >>>>"+intQty);

											intAllocQty = intQty - intSetRemQty;
											
											if (intAllocQty == 0 || intAllocQty < 0)
											{
												intAllocQty = intQty;
											}else
											{
												if (intAllocQty > intSetRemQty)
												{
													intAllocQty = intSetRemQty;
												}
												else{
												intAllocQty = intAllocQty;
												}
											}
											
//System.out.println("intAllocQty>>>>"+intAllocQty);
											
											intQty = intQty - intAllocQty;
											hmLoop.put("IQTY",""+intQty);
//System.out.println("intQty HERE 222 >>>>"+intQty);
											alReturn.remove(j); // Clearing current values
											intSetRemQty = intSetRemQty - intAllocQty;
									
//System.out.println("intSetRemQty HERE>>>>"+intSetRemQty);
											
											if (intSetRemQty > 0)
											{
												hmTempLoop = new HashMap();
												hmTempLoop.put("PNUM",strTemp);
												hmTempLoop.put("PDESC",strDesc);
												hmTempLoop.put("CNUM","");
												hmTempLoop.put("QTY",""+intSetRemQty);
												hmTempLoop.put("MID",strId);
												hmTempLoop.put("TYPE","");
												alReturn.add(hmTempLoop);
											}
																						
											blFlag = true;
											blFlag1 = true;
											
											hmTempLoop = new HashMap(); 										
											hmTempLoop.put("PNUM",strTemp);
											hmTempLoop.put("CNUM",strControl);
											hmTempLoop.put("QTY",""+intAllocQty);
											hmTempLoop.put("TYPE","N");
											alReturn.add(hmTempLoop);
											
											strSetQty = blRepeatFl?"-":strSetQty;
%>
											<td width="100" class="RightText" align="center"><%=strSetQty%></td>
<%
											break;
										}
									//}// END OF INNER 602.401
									} // End of Inner FOR loop
									
									hmReturn.put(strSetTemp,alReturn);
									
									if (!blFlag){
%>
											<td width="100" class="RightText" align="center">-</td>
<%									
									}
									blFlag = false;
								}// End of StringTokenizer
								
								strQty = GmCommonClass.parseZero((String)hmLoop.get("IQTY"));
								intRemQty = Integer.parseInt(strQty);
								
								if(!blFlag1 || intRemQty > 0){
									alRemainder.add(hmLoop);
								}
								blFlag1 = false;
								intRemQty = 0;
%>											
										</tr>
										<tr><td colspan="<%=intColspan%>" height="1" bgcolor="#cccccc"></td></tr>
<%
							blRepeatFl = false;
							} // end of FOR	
							//}//602.401
%>
										<tr><td colspan="<%=intColspan+1%>" height="1" class="Line"></td></tr>
<%							
							int intLoop = 0;
							strTok = new StringTokenizer(strSetIds,",");
							StringBuffer sbSet = new StringBuffer();
							
							while (strTok.hasMoreTokens())
							{

								strSetTemp = strTok.nextToken();
								alReturn = (ArrayList)hmReturn.get(strSetTemp);
								intSetSize = alReturn.size();
								sbSet.setLength(0);
								
								if (intSetSize > 0)
								{
%>
										<tr>
											<td colspan="<%=intColspan%>" class="RightTableCaption" height="24" bgcolor="#eeeeee">
											Parts exclusive to Set <%=strSetTemp%>
											</td>
										</tr>
<%								
									for (int j = 0;j < intSetSize ;j++ )
									{
										hmTempLoop = (HashMap)alReturn.get(j);
										strPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
										strDesc = GmCommonClass.parseNull((String)hmTempLoop.get("PDESC"));
										strQty = GmCommonClass.parseNull((String)hmTempLoop.get("QTY"));
										strControl = GmCommonClass.parseNull((String)hmTempLoop.get("CNUM"));
										strType = GmCommonClass.parseNull((String)hmTempLoop.get("TYPE"));
										strId = GmCommonClass.parseNull((String)hmTempLoop.get("MID"));
										if (strType.equals("N") || (!strId.equals("") && strControl.equals("")))
										{
											sbSet.append(strPartNum);
											sbSet.append("^");
											sbSet.append(strControl);
											sbSet.append("^");
											sbSet.append(strQty);
											sbSet.append("|");
										}
										
										if (strControl.equals(""))
										{
%>
										<tr height="20">
											<td width="100" class="RightText">&nbsp;<%=strPartNum%></td>
											<td width="350" class="RightText">&nbsp;<%=strDesc%></td>
											<td width="100" bgcolor="#eeeeee" align="center" class="RightText">-</td>
											<td width="100" bgcolor="#eeeeee" class="RightText" align="center">-</td>
<%
											for (int m=0;m<intLoop;m++)
											{
%>
											<td width="100" class="RightText" align="center">-</td>
<%
											}
%>											
											<td width="100" class="RightText" align="center"><%=strQty%></td>
										</tr>
										<tr><td colspan="<%=intColspan%>" height="1" bgcolor="#cccccc"></td></tr>
<%								
										} // END OF IF
									} // End of FOR
								} // End of IF
								strSetTemp = strSetTemp.replaceAll("\\.","");
								out.println("<input type=hidden name=h"+strSetTemp+" value='"+sbSet.toString()+"'>");
								intLoop++;
%>
							<tr><td colspan="<%=intColspan%>" height="1" class="Line"></td></tr>
<%							
							} // End of StringTokenizer

						} else	{
%>
										<tr>
											<td height="30" colspan="<%=intColspan%>" align="center" class="RightTextBlue"><fmtConsignReprocessData:message key="LBL_NO_DATA"/></td>
										</tr>
<%
					}
%>
										<tr><td colspan="<%=intColspan%>" height="1" bgcolor="#666666"></td></tr>
									</table>
									</DIV>
								</td>
							</tr>	
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr class="ShadeRightTableCaption">
								<td HEIGHT="24" colspan="6"><fmtConsignReprocessData:message key="LBL_RETURN_ORIGINAL_CONSIGNMENT"/></td>
							</tr>
							<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
							<tr bgcolor="#EEEEEE" class="RightTableCaption">
								<td HEIGHT="24" width="100" rowspan="2"><fmtConsignReprocessData:message key="LBL_PART_NUMBER"/></td>
								<td width="350" rowspan="2"><fmtConsignReprocessData:message key="LBL_DESCRIPTION"/></td>
								<td width="100" align="center" rowspan="2"><fmtConsignReprocessData:message key="LBL_CONTROL"/></td>
								<td width="100" align="center" rowspan="2"><fmtConsignReprocessData:message key="LBL_QTY"/></td>
								<td align="center" colspan="2"><fmtConsignReprocessData:message key="LBL_ACTION"/></td>
							</tr>
							<tr bgcolor="#EEEEEE" class="RightTableCaption">
								<td width="100"><fmtConsignReprocessData:message key="LBL_RETURN"/></td>
								<td width="100"><fmtConsignReprocessData:message key="LBL_CONSIGN"/></td>
							</tr>
							<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
<%
								intSetSize = alRemainder.size();
								if (intSetSize > 0)
								{
									for (int j = 0;j < intSetSize ;j++ )
									{
										hmTempLoop = (HashMap)alRemainder.get(j);
										strPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
										strDesc = GmCommonClass.parseNull((String)hmTempLoop.get("PDESC"));
										strQty = GmCommonClass.parseNull((String)hmTempLoop.get("IQTY"));
										strControl = GmCommonClass.parseNull((String)hmTempLoop.get("CNUM"));
%>
										<tr height="20">
											<td width="100" class="RightText">&nbsp;<%=strPartNum%></td>
											<td width="350" class="RightText">&nbsp;<%=strDesc%></td>
											<td width="100" class="RightText" align="center"><%=strControl%></td>
											<td width="100" class="RightText" align="center"><%=strQty%></td>
											<td align="center"><input type="radio" checked name="Rad_Rem<%=j%>" value="<%=strPartNum%>" id="<%=strControl%>^<%=strQty%>"></td>
											<td align="center"><input type="radio" name="Rad_Rem<%=j%>" value="<%=strPartNum%>" id="<%=strControl%>^<%=strQty%>"></td>
										</tr>
										<tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>
<%
									}// End of FOR
								}else{
%>
										<tr>
											<td height="30" colspan="6" align="center" class="RightTextBlue"><fmtConsignReprocessData:message key="LBL_NO_PARTS_RETURNED"/></td>
										</tr>
<%
					}
%>						
						</table>
						<input type="hidden" name="hCntRem" value="<%=intSetSize%>">
					</td>
				</tr>
						
<%
	}
%>	
			   <tr>
					<td colspan="3" align="Center" height="30">&nbsp;
					<fmtConsignReprocessData:message key="BTN_SUBMIT" var="varSubmit"/>
					<gmjsp:button value="${varSubmit}" gmClass="button" onClick="fnSubmit();" tabindex="16" buttonType="Save" />&nbsp;
					</td>
				</tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
