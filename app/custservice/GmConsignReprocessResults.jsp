
<%
/**********************************************************************************
 * File		 		: GmConsignReprocessResults.jsp
 * Desc		 		: This screen is used 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtConsignReprocessResults" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- custservice\GmConsignReprocessResults.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtConsignReprocessResults:setLocale value="<%=strLocale%>"/>
<fmtConsignReprocessResults:setBundle basename="properties.labels.custservice.GmConsignReprocessResults"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction");
	if (strhAction == null)
	{
		strhAction = (String)request.getAttribute("hAction");
	}
	strhAction = (strhAction == null)?"Load":strhAction;

	String strConId = "";
	String strSwapIds = "";
	String strDistId = "";
	String strSetId = "";
	String strSetNm = "";
	String strParentConId = "";
	String strSetIds = "";
	String strUser = "";
	String strDate = "";
	ArrayList alConData = new ArrayList();
	String strConsignId = "";
	String strReturnId = "";
	
	HashMap hmSets = new HashMap();
	

	if (hmReturn != null)
	{
		strConId = (String)hmReturn.get("CONID");
		strSwapIds = (String)hmReturn.get("IDS");
		alConData = (ArrayList)hmReturn.get("IDSLIST");
		strReturnId = GmCommonClass.parseNull((String)hmReturn.get("RETURNID"));
		strConsignId = GmCommonClass.parseNull((String)hmReturn.get("CONSIGNID"));
	}

	int intSize = 0;
	int intSetSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Reprocess Consignments - Results</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10"">
<FORM name="frmMain" method="POST" action="<%=strServletPath%>/GmConsignModifyServlet">

	<table border="0" width="750" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="748" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td class="Line" colspan="2"></td></tr>
					<tr>
						<td colspan="2" height="25" class="RightDashBoardHeader">&nbsp;<fmtConsignReprocessResults:message key="LBL_REPROCESS_CONSIGNMENT_RESULTS"/></td>
					</tr>
					<tr><td class="Line" colspan="2"></td></tr>
					<tr height="24">
						<td colspan="2" class="RightText"><fmtConsignReprocessResults:message key="LBL_CONSIGNMENT_ID"/>:<%=strConId%></td>
					</tr>
				<tr><td class="Line" colspan="2"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" colspan="2"><fmtConsignReprocessResults:message key="LBL_REPROCESS_DETAILS"/></td>
					</tr>
					<tr><td class="Line" colspan="2"></td></tr>
					<tr><td class="RightText" colspan="2">
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<TR  height="24" bgcolor="#EEEEEE" class="RightTableCaption">
								<td width="100"><fmtConsignReprocessResults:message key="LBL_ID"/></td>
								<td width="100"><fmtConsignReprocessResults:message key="LBL_TYPE"/></td>
								<td width="150"><fmtConsignReprocessResults:message key="LBL_CREATED_BY"/></td>
								<td width="100"><fmtConsignReprocessResults:message key="LBL_CREATED_DATE"/></td>
							</tr>
							<tr><td class="Line" colspan="4"></td></tr>
							<TR height="20">
								<td class="RightText"><%=strConsignId%></td>
								<td class="RightText"><fmtConsignReprocessResults:message key="LBL_ITEM_CONSIGNMENT"/></td>
								<td class="RightText">-</td>
								<td class="RightText">-</td>
							</tr>
							<tr><td bgcolor="#eeeeee" colspan="6"></td></tr>
							<TR height="20">
								<td class="RightText"><%=strReturnId%></td>
								<td class="RightText"><fmtConsignReprocessResults:message key="LBL_ITEM_RETURN"/></td>
								<td class="RightText">-</td>
								<td class="RightText">-</td>
							</tr>
						</table>
					</td>
					</tr>					
					<tr><td class="Line" colspan="2"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" colspan="2"><fmtConsignReprocessResults:message key="LBL_SWAP_DETAILS"/></td>
					</tr>
					<tr><td class="Line" colspan="2"></td></tr>
					<tr><td class="RightText" colspan="2">
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<TR bgcolor="#EEEEEE" class="RightTableCaption">
								<td width="100"><fmtConsignReprocessResults:message key="LBL_CONSIGN_ID"/></td>
								<td width="100"><fmtConsignReprocessResults:message key="LBL_SET_ID"/></td>
								<td width="200"><fmtConsignReprocessResults:message key="LBL_SET_NAME"/></td>
								<td width="100"><fmtConsignReprocessResults:message key="LBL_PARENT_CONSIGN_ID"/><BR></td>
								<td width="150"><fmtConsignReprocessResults:message key="LBL_CREATED_BY"/></td>
								<td width="100"><fmtConsignReprocessResults:message key="LBL_CREATED_DATE"/></td>
							</tr>
							<tr><td class="Line" colspan="6"></td></tr>
<%
						intSize = alConData.size();
						hcboVal = new HashMap();
						for (int i=0;i<intSize;i++)
						{
							hcboVal = (HashMap)alConData.get(i);
							strConId = (String)hcboVal.get("CONID");
							strSetId = GmCommonClass.parseNull((String)hcboVal.get("SETID"));
							strSetNm = GmCommonClass.parseNull((String)hcboVal.get("SNAME"));
							strParentConId = GmCommonClass.parseNull((String)hcboVal.get("PCONID"));
							strUser = GmCommonClass.parseNull((String)hcboVal.get("CUSER"));
							strDate = GmCommonClass.parseNull((String)hcboVal.get("CDATE"));
%>
							<TR height="20">
								<td class="RightText"><%=strConId%></td>
								<td class="RightText"><%=strSetId%></td>
								<td class="RightText"><%=strSetNm%></td>
								<td class="RightText"><%=strParentConId%></td>
								<td class="RightText"><%=strUser%></td>
								<td class="RightText"><%=strDate%></td>
							</TR>
							<tr><td bgcolor="#eeeeee" colspan="6"></td></tr>
<%
						}
%>
						</table>
					</td></tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
