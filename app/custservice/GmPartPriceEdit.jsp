 <%
/**********************************************************************************
 * File		 		: GmPartPriceEdit.jsp
 * Desc		 		: This screen is used for the Part Number Maintenance
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- custservice\GmPartPriceEdit.jsp -->
<%@ taglib prefix="fmtPartPriceEdit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartPriceEdit:setLocale value="<%=strLocale%>"/>
<fmtPartPriceEdit:setBundle basename="properties.labels.custservice.GmPartPriceEdit"/>

<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strProjId = (String)request.getAttribute("hId");
	String strAccId = (String)request.getAttribute("hAccId");
	String strType = (String)request.getAttribute("hType");

	int intSize = 0;
	String strhAction = "";
	String strMode = "";

	String strPartNum = "";
	String strPartDesc = "";
	String strEPrice = "";
	String strLPrice = "";
	String strCPrice = "";
	String strLiPrice = "";
	String strFlag = "";
	String strPrice = "";
	String strDiscount = "";

	HashMap hmPartDetails = (HashMap)hmReturn.get("PARTDETAILS");
	HashMap hmAccPartDetails = (HashMap)hmReturn.get("ACCPARTDETAILS");

	if (hmPartDetails != null)
	{
		strPartNum = (String)hmPartDetails.get("ID");
		strPartDesc	= (String)hmPartDetails.get("PDESC");
		strEPrice	= (String)hmPartDetails.get("EPRICE");
		strLPrice	= (String)hmPartDetails.get("LPRICE");
		strCPrice	= (String)hmPartDetails.get("CPRICE");
		strLiPrice	= (String)hmPartDetails.get("LIPRICE");
		strhAction = "Edit";
		strFlag = "Part";
		strMode = "Save";
	}

	if (hmAccPartDetails != null)
	{
		strPartNum = (String)hmAccPartDetails.get("ID");
		strPartDesc	= (String)hmAccPartDetails.get("PDESC");
		strPrice = (String)hmAccPartDetails.get("PRICE");
		strDiscount = (String)hmAccPartDetails.get("DISC");
		strMode = "SaveAccPrice";
		strFlag = "Acc";
	}

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Number Pricing Setup </TITLE>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script><!-- PMT-36862 js file  -->
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmPartPriceEdit.js"></script>
<script>
var Flag = '<%=strFlag%>';

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="Main" method="POST" action="<%=strServletPath%>/GmPartPriceEditServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="PartNum" value="<%=strPartNum%>">
<input type="hidden" name="hMode" value="<%=strMode%>">
<input type="hidden" name="hId" value="<%=strProjId%>">
<input type="hidden" name="hAccId" value="<%=strAccId%>">
<input type="hidden" name="hType" value="<%=strType%>">

	<table border="0" width="650" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="648" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr>
						<td height="25" colspan="2" class="RightDashBoardHeader">&nbsp;<fmtPartPriceEdit:message key="LBL_PART_NUM_PRICE_EDIT"/></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr>
						<td class="RightText" width="200" align="right" HEIGHT="24"><fmtPartPriceEdit:message key="LBL_PART_NUM"/>:</td>
						<td class="RightText" width="448">&nbsp;<%=strPartNum%></td>
					</tr>
					<tr class="Shade">
						<td class="RightText" align="right" height="24"><fmtPartPriceEdit:message key="LBL_PART_DESC"/>:</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
					</tr>
<%
				if (strFlag.equals("Part"))
				{
%>
					<tr>
						<td class="RightText" width="200" align="right" HEIGHT="24"><fmtPartPriceEdit:message key="LBL_TIER1_PRICE"/>:</td>
						<td class="RightText">&nbsp;<input type="text" size="10" value="<%=strLiPrice%>" name="Txt_LiPrice" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
						</td>
					</tr>
					<tr class="Shade">
						<td class="RightText" width="200" align="right" HEIGHT="24"><fmtPartPriceEdit:message key="LBL_TIER2_PRICE"/>:</td>
						<td>&nbsp;<input type="text" size="10" value="<%=strLPrice%>" name="Txt_LPrice" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
						</td>
					</tr>
					<tr>
						<td class="RightText" width="200" align="right" HEIGHT="24"><fmtPartPriceEdit:message key="LBL_TIER3_PRICE"/>:</td>
						<td>&nbsp;<input type="text" size="10" value="<%=strCPrice%>" name="Txt_CPrice" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
						</td>
					</tr>
					<tr class="Shade">
						<td class="RightText" width="200" align="right" HEIGHT="24"><fmtPartPriceEdit:message key="LBL_TIER4_PRICE"/>:</td>
						<td>&nbsp;<input type="text" size="10" value="<%=strEPrice%>" name="Txt_EPrice" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
						</td>
					</tr>
<%
				}else if (strFlag.equals("Acc"))
				{
%>
					<tr>
						<td class="RightText" width="200" align="right" HEIGHT="24"><fmtPartPriceEdit:message key="LBL_PRICE"/>:</td>
						<td>&nbsp;<input type="text" size="10" value="<%=strPrice%>" name="Txt_Price" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
						</td>
					</tr>
					<tr class="Shade">
						<td class="RightText" width="200" align="right" HEIGHT="24"><fmtPartPriceEdit:message key="LBL_DISCOUNT_OFF"/>:</td>
						<td>&nbsp;<input type="text" size="10" value="<%=strDiscount%>" name="Txt_Discount" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
						</td>
					</tr>
<%
				}
%>
				   <tr>
						<td colspan="2" align="center" height="30">&nbsp;
						<fmtPartPriceEdit:message key="BTN_SUBMIT" var="varSubmit"/>
						<fmtPartPriceEdit:message key="BTN_RESET" var="varReset"/>
						<gmjsp:button value="${varSubmit}" gmClass="button" onClick="javascript:fnSubmit();" tabindex="15" buttonType="Save" />&nbsp;
						<gmjsp:button value="${varReset}" gmClass="button" onClick="javascript:fnReset(this.form);" tabindex="16" buttonType="Save" />
						</td>
					</tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
</FORM>
<%@ include file="/common/GmFooter.inc"%> 
</BODY>
</HTML>
