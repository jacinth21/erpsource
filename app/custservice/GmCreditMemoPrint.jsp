 <%
/**********************************************************************************
 * File		 		: GmCreditMemoPrint.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>

<%@ taglib prefix="fmtCreditMemoPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- custservice\GmCreditMemoPrint.jsp -->

<fmtCreditMemoPrint:setLocale value="<%=strLocale%>"/>
<fmtCreditMemoPrint:setBundle basename="properties.labels.custservice.GmCreditMemoPrint"/>


<%
try {
	GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmCreditMemoPrint", strSessCompanyLocale);
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");

	String strAdd = "";
	String strTemp = "";
	String strShade = "";
	String strRAId = "";
	String strSetName = "";
	String strCreditId = "";
	String strInvId = "";
	String strPO = "";

	String strDate = "";
	String strItemQty = "";
	String strControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strCreditDate = "";
	String strReturnDate = "";
	String strOrdComments  = "";
	String strShipCost = "";
	String strOrdType = "";
	
	String strType = "";
	String strStatusFl = "";
	String strOrderID = "";
	String strTitle = "";
	String strRAType = "";
	
	double dbAmount = 0.0;
	double dbTotal = 0.0;
	
	ArrayList alReturnsItems = new ArrayList();

	HashMap hmReturnDetails = new HashMap();
	
	boolean bolFl = false;

	int intPriceSize = 0;

	if (hmReturn != null)
	{
		hmReturnDetails = (HashMap)hmReturn.get("RADETAILS");
		alReturnsItems = (ArrayList)hmReturn.get("RAITEMDETAILS");

		strRAId = GmCommonClass.parseNull((String)hmReturnDetails.get("RAID"));
		strSetName = GmCommonClass.parseNull((String)hmReturnDetails.get("SNAME"));
		strDate = GmCommonClass.parseNull((String)hmReturnDetails.get("UDATE"));
		strAdd = GmCommonClass.parseNull((String)hmReturnDetails.get("ACCADD"));
		strCreditId = GmCommonClass.parseNull((String)hmReturnDetails.get("CREDITINVID"));
		strPO = GmCommonClass.parseNull((String)hmReturnDetails.get("PO"));
		strCreditDate = GmCommonClass.parseNull((String)hmReturnDetails.get("CREDITDATE"));
		strReturnDate = GmCommonClass.parseNull((String)hmReturnDetails.get("RETDATE"));	
		strInvId = GmCommonClass.parseNull((String)hmReturnDetails.get("INVID"));
		strOrdComments = GmCommonClass.parseNull((String)hmReturnDetails.get("ORDER_COMMENTS"));	
		strShipCost	= GmCommonClass.parseZero(((String)hmReturnDetails.get("SCOST")));	
		strOrderID = GmCommonClass.parseNull(((String)hmReturnDetails.get("ORDER_ID")));
		strOrdType = GmCommonClass.parseNull(((String)hmReturnDetails.get("ORDTYPE")));
		strRAType = GmCommonClass.parseNull(((String)hmReturnDetails.get("RETTYPE")));
		dbTotal = Double.parseDouble(strShipCost);
		log.debug(" OrdType is " + strOrdType + " Returns Type is " + strRAType)	;
	}
	
	
	if (strRAType.equals("3300"))
	{
				if (strOrdType.equals("2528"))
				{
					strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_CREDIT_MEMO"));
				}
				else
					strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_SALES_ADJUSTMENT"));
	}
	else 
		strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_CREDIT_MEMO"));
		
		log.debug(" strTitle is " + strTitle);
	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Consign </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/watermark.js"></script>
<script>

function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
</HEAD>

<BODY topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmReportCreditsServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hConsignId" value="<%=strRAId%>">
	<table border="0" width="700" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td bgcolor="#666666" colspan="3"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="6"></td>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td class="RightText" width="270" valign="top"><img src="<%=strImagePath%>/s_logo.gif" width="160" height="60"><br>&nbsp;<b><fmtCreditMemoPrint:message key="LBL_GLOBUS"/>Globus Medical Inc.</b><br>&nbsp;<fmtCreditMemoPrint:message key="LBL_ADDRESS"/><br>&nbsp;<br>&nbsp; <br><font size=-2>&nbsp;<fmtCreditMemoPrint:message key="LBL_PHONE"/></font></td>
						<td class="RightText" align="right"><font size="+3"><%=strTitle%></font>&nbsp;</td>
					</tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1" rowspan="6"></td>
		</tr>
		<tr><td bgcolor="#666666"></td></tr>
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr bgcolor="#eeeeee" class="RightTableCaption">
						<td height="25" align="center" width="350">&nbsp;<fmtCreditMemoPrint:message key="LBL_CUSTOMER"/></td>
						<td bgcolor="#666666" width="1" rowspan="7"></td>
						<td height="25" align="center">&nbsp;<fmtCreditMemoPrint:message key="LBL_CUSTOMER_PO"/></td>
						<td bgcolor="#666666" width="1" rowspan="7"></td>
						<td width="110" align="center">&nbsp;<fmtCreditMemoPrint:message key="LBL_CREDIT_NO"/>.</td>
						<td bgcolor="#666666" width="1" rowspan="7"></td>
						<td width="110" align="center">&nbsp;<fmtCreditMemoPrint:message key="LBL_CREDIT_DATE"/></td>
					</tr>
					<tr>
						 <td bgcolor="#666666" height="1" colspan="8"></td>
					</tr>
					<tr>
						<td class="RightText" rowspan="5" valign="top">
							<table>
								<tr>
									<td width="25">&nbsp;</td>
									<td width="300" class="RightTableCaption">&nbsp;<%=strAdd%>
									</td>
								</tr>
							</table>&nbsp;<BR>&nbsp;<BR>
						</td>
						<td class="RightText" align="center">&nbsp;<%=strPO%></td>
						<td height="25" class="RightText" align="center">&nbsp;<%=strCreditId%></td>
						<td class="RightText" align="center">&nbsp;<%=strCreditDate%></td>
					</tr>
					<tr>
						 <td bgcolor="#666666" height="1" colspan="2"></td>
						 <td bgcolor="#666666" height="1" colspan="2"></td>
						 <td bgcolor="#666666" height="1" colspan="2"></td>
					</tr>
					<tr bgcolor="#eeeeee" class="RightTableCaption">
						<td class="RightText" align="center" height="20"><fmtCreditMemoPrint:message key="LBL_INVOICE"/></td>
						<td colspan="2" align="center"><fmtCreditMemoPrint:message key="LBL_RA_ID"/></td>
						<td colspan="2" align="center"><fmtCreditMemoPrint:message key="LBL_RA_DATE"/></td>
					</tr>
					<tr>
						 <td bgcolor="#666666" height="1" colspan="2"></td>
						 <td bgcolor="#666666" height="1" colspan="2"></td>
						 <td bgcolor="#666666" height="1" colspan="2"></td>
					</tr>
					<tr>
						<td class="RightText" rowspan="3" align="center">&nbsp;<%=strInvId%><b></b></td>
						<td align="center" colspan="2" height="25"  class="RightText">&nbsp;<%=strRAId%></td>
						<td align="center" colspan="2" height="25"  class="RightText">&nbsp;<%=strReturnDate%></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2">
							<table cellspacing="0" cellpadding="0" border="0" height="400" width="100%" id="myTable">
								<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
								<tr bgcolor="#eeeeee" class="RightTableCaption">
									<td height="25" width="70" align="center"><fmtCreditMemoPrint:message key="LBL_PART_NUMBER"/></td>
									<td width="330"><fmtCreditMemoPrint:message key="LBL_DESCRIPTION"/></td>
									<td width="50" align="center"><fmtCreditMemoPrint:message key="LBL_QTY"/></td>
									<td align="center"><fmtCreditMemoPrint:message key="LBL_RATE_EA"/></td>
									<td align="center"><fmtCreditMemoPrint:message key="LBL_AMOUNT"/></td>
								</tr>
								<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
<%
						intSize = alReturnsItems.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();

							String strLine = "";
							String strRate = "";
							String strAmount = "";
							String strPrice = "";
							String strTotal = "";
							int intQty = 0;
							
							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alReturnsItems.get(i);

								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
								strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
								strStatusFl = GmCommonClass.parseNull((String)hmLoop.get("SFL"));

								intQty = Integer.parseInt(strQty);
								dbAmount = Double.parseDouble(strPrice);
								dbAmount = intQty * dbAmount; // Multiply by Qty
								strAmount = ""+dbAmount;

								dbTotal = dbTotal + dbAmount;
								strTotal = ""+dbTotal;

								out.print(strLine);
%>
								<tr>
									<td class="RightText" height="20">&nbsp;<%=strPartNum%></td>
									<td class="RightText"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td class="RightText" align="center">&nbsp;<%=GmCommonClass.getRedText(strQty)%></td>
									<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strPrice))%>&nbsp;&nbsp;</td>
									<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strAmount))%>&nbsp;&nbsp;&nbsp;</td>
								</tr>
<%
							}//End of FOR Loop
%>
								<tr >
									<td  height="25">&nbsp;</td>
									<td class="RightText" ><fmtCreditMemoPrint:message key="LBL_SHIPPING_CHARGES"/></td>
									<td colspan="2"  height="25">&nbsp;</td>
									<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strShipCost))%>&nbsp;&nbsp;&nbsp;</td>
								</tr>

								<tr><td colspan="5" height=""100%"">&nbsp;</td></tr>
								<tr > 
									<td class="RightText" >&nbsp; </td> 
									<td colspan="4"  class="RightText" align="left">&nbsp;<%=strOrdComments%> </td>
								</tr>
								
								<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
								
								<tr >
									<td colspan="3" height="25">&nbsp;</td>
									<td class="RightTableCaption" align="center"><fmtCreditMemoPrint:message key="LBL_TOTAL"/></td>
									<td class="RightTableCaption" align="right">$<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strTotal))%>&nbsp;&nbsp;&nbsp;</td>
								</tr>
								<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
<%
						}else {
%>
						<tr><td colspan="5" height="50" align="center" class="RightTextRed"><BR><fmtCreditMemoPrint:message key="LBL_NO_PART_NUMBERS"/>!</td></tr>
<%
						}
%>
							</table>
						</td>
					</tr>
				 </table>
			 </td>
		</tr>
		<tr>
			<td height="30" align="center" id="button">
			<fmtCreditMemoPrint:message key="BTN_PRINT" var="varPrint"/>
			<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
			<fmtCreditMemoPrint:message key="BTN_CLOSE" var="varClose"/>
			<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" />
			</td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
    </table>
</FORM>
<% /* Below code to display bookmark value 
	* 2522 maps to duplicate order 
	* If Credited before invoice then display the Water mark
	* so the 
	*/  
	System.out.println("strPO '" + strPO + "'");
if (strPO.equals("") && !strOrderID.equals("") )
{
%>

<div id="waterMark" style="position:absolute; z-Index: 0; top: 340; left: 340">
	<img src="<%=strImagePath%>/credit_bef_invoice.jpg"  border=0>
	</div>
<% }%>

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
