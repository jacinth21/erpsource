 <%
/**********************************************************************************
 * File		 		: GmITConsignPackslipPrint.jsp
 * Desc		 		: This screen is used for the packing slip version of Consignment
 * Version	 		: 1.0
 * author			: RAJA
************************************************************************************/
%>
 <!-- \custservice\GmITConsignPackslipPrint.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
	String strCompAddress ="";
	strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	strCssPath = GmCommonClass.getString("GMSTYLES");
	strImagePath = GmCommonClass.getString("GMIMAGES");
	strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	strCommonPath = GmCommonClass.getString("GMCOMMON");
	String strApplDateFmt = strGCompDateFmt;
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmConDetails = new HashMap();
	HashMap hmWhPicSlipDetails = new HashMap();
	HashMap hmAtbData = new HashMap();
	HashMap hmCompAtbData = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	hmAtbData = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("ORDER_ATB"));
	hmCompAtbData = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("COMP_ATB"));
	
   
	ArrayList alSetLoad = new ArrayList();
	ArrayList alPopulatedCartDtls = new ArrayList();

	String strShade = "";

	String strConsignId = "";
	String strConsignAdd = "";
	String strShipAdd = "";
	String strDate = "";
	String strShippingDt = "";
	
	String strTemp = "";
	String strItemQty = "";
	String strControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strWhPicSlipHeader= "";
    String strConsignIdField =  "Consign #";
    String strConsignNameField =  "Consigned By";
    String strShippingLable = "Shipping Date";
    String strComments ="";
	String strUserName = "";
	String strCompanyId = "";
	String strCompanyName = "";
	String strCompanyAddress = "";
	String strCompanyPhone = "";
	String strHeadAddress = "";
	String strSpecialNotes = "";
	String strAccId = "";
	String strLocAccId = "";
	String strAccNm = "";
	String strLocAccNm = "";
	String strRepNm = "";
	String strLocRepNm = "";
	String strSetID = "";
	String strSetNm = "";
	String strOrderedByLbl = "";
	String strDetailsLbl = "";
	String strModeLbl = "";
	String strTrackLbl = "";
	String strShipMode = "";
	String strTrackNum = "";
	String strGmCompanyVat="";
	String strGmCompanyCst="";
	String strGmLocalLic="";
	String strGmExportLic="";
	String strExportDruglic="";
	String strLocalDruglic="";
	String strGmCompanyPAN="";
	String strDivisionId = "";
	String strComapnyLogo = "";
	String strShowPhoneNo = "";
	String strCompId = "";
	String strPlantId = "";
	String strTransCompId = "";
	String strTransCompanyLocale = "";
	String strRaComments="";
	String strDistType ="";
	String strExpDtEnableFl ="";
	String strExpiryDateFl = "";
	String strhospAccountID = "";
	String strConsignAccountID = "";
	String strLoanerReqId  = "";

		
	strPlantId = gmDataStoreVO.getPlantid();
	strCompId = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strPlantId, "PLANT_PACK_SLIP")); //Rulevalue:= 1010 (or) 1017
	if(strCompId.equals("")){
		  strCompId=gmDataStoreVO.getCmpid(); 
		}
	strExpiryDateFl = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strPlantId, "PACK_SLIP_EXPIRY")); 
	String strCompanyLocale = GmCommonClass.getCompanyLocale(strCompId);
	
	GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    //GmResourceBundleBean gmResourceBundleBean = (GmResourceBundleBean) request.getAttribute("gmResourceBundleBean");
	GmResourceBundleBean gmCompResourceBundleBean =
	        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	String strPackSlipHeader = GmCommonClass.parseNull((String)request.getAttribute("PackSlipHeader"));
	String strIntrasitPackSlip = GmCommonClass.parseNull((String)request.getAttribute("INTRANSIT_PACK_SLIP"));
	String strShowShipping = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("SHOW_SHIPPING"));
	strConsignNameField   = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("CONSIGNED_BY"));
	String strPartNumLbl  = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PART")); 
	String strPartDescLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("DESCRIPTION"));
	String strQtyUsedLbl  =  GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("QTY"));
	String strExpDateLbl  =  GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("EXP_DATE"));
	String strLotNumLbl   = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("CONTROL_NUMBER"));	
	String strCommentsLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("COMMENTS"));
	String strConsignAgentLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("CONSIGNING_AGENT"));
	 String strLoanReqIdLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("LOANERREQID"));//PMT-45058
	 
	String strDateLbl     = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("DATE"));
	String strConsignHeaderLbl    = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("PACKING_SLIP"));
	String strConsignToHeader     = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("CONSIGNED_TO"));
	String strShippingToHeader    = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("SHIP_TO"));
	String strShipListHeaderLbl   = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("SHIPPING_LIST"));
	String strShipListConsignToHeader  = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("CONSIGNEE"));
	String strShipListShippingToHeader  = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("SHIPPER"));
	strConsignIdField  = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("CONSIGN_NO"));
	strShippingLable = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("SHIPPING_DATE"));
	String strProformaLbl  = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("PROFORMA"));
	String strNotesLbl  =  GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("NOTES"));
	strSpecialNotes = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("SPECIALNOTES"));
	String strAccIdLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("ACCOUNTID"));
	String 	strShowGMNAAddress = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.SHOW_GMNA_ADDRESS"));
	String strExpDateFlg  =  GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.EXPDTENABLE"));
	String strFooterMsg  =  GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACKS_LIP.FOOTERMSG"));

	
	String strEnableExpDtForDist = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.ENABLE_EXP_DT_FOR_DIST"));
	String strLoanedToLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("LOANED_TO"));
	strOrderedByLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("ORDERBY")).equals("")? "Sales Rep" :GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("ORDERBY")) ;
	strDetailsLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("DETAILS")).equals("")? "Set ID/Name" :GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("DETAILS")) ;
	strModeLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("SHIPMODE")).equals("")? "Ship Mode" :GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("SHIPMODE")) ;
	strTrackLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("TRACK")).equals("")? "Tracking #" :GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("TRACK")) ;
	strShowPhoneNo = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.SHOW_PHONE_NO_CON"));
	
	strGmCompanyVat =  GmCommonClass.parseNull((String) hmCompAtbData.get("GMCOMPANYVAT"));
	strGmCompanyCst =  GmCommonClass.parseNull((String) hmCompAtbData.get("GMCOMPANYCST"));
	strGmCompanyPAN =  GmCommonClass.parseNull((String) hmCompAtbData.get("GMCOMPANYPAN"));
	strGmLocalLic =  GmCommonClass.parseNull((String) hmCompAtbData.get("GMLOCALLIC"));
	strGmExportLic =  GmCommonClass.parseNull((String) hmCompAtbData.get("GMEXPORTLIC"));
	strExportDruglic =  GmCommonClass.parseNull((String) hmAtbData.get("EXPORTDRUGLIC"));
	strLocalDruglic =  GmCommonClass.parseNull((String) hmAtbData.get("LOCALDRUGLIC"));
	
	String strScreenName = GmCommonClass.parseNull((String)request.getParameter("screenName"));
	String strGUID = GmCommonClass.parseNull(request.getParameter("hGUId"));
	alPopulatedCartDtls =  GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("alPopulatedCartDtls"));
	
	if (hmReturn != null)
	{
	    //PMT-38134 Packing Slip is Blank in Details of Shipment Emails
	    if(alPopulatedCartDtls.isEmpty() && alPopulatedCartDtls != null)
	    {
	      alPopulatedCartDtls = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SETLOAD"));
	    }
		hmConDetails = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("SETDETAILS"));

		strConsignId = GmCommonClass.parseNull((String)hmConDetails.get("CID"));
		strConsignAdd = GmCommonClass.parseNull((String)hmConDetails.get("BILLADD"));
		strShipAdd = GmCommonClass.parseNull((String)hmConDetails.get("SHIPADD"));
		strRaComments = GmCommonClass.parseNull((String)hmReturn.get("RACOMMENTS"));
		strDistType = GmCommonClass.parseNull((String)hmConDetails.get("DISTTYPE"));
      // To enable thhe expiry date column only for this distributors types 70103(Inter-Company Transfer (ICT), 70106(Inter Company Account (ICA)
		if (strEnableExpDtForDist.indexOf("|" + strDistType + "|") != -1)
		{
		  strExpDtEnableFl ="Y";
		}
    
		
		if (hmConDetails.get("UDATE") instanceof java.util.Date){
			 strDate = GmCommonClass.getStringFromDate((java.sql.Date)hmConDetails.get("UDATE"),strApplDateFmt);  
		  }else{
			 strDate = GmCommonClass.parseNull((String)hmConDetails.get("UDATE"));
		}
		strShippingDt = GmCommonClass.getStringFromDate((java.sql.Date)hmConDetails.get("SDATE"),strApplDateFmt);
		strUserName = GmCommonClass.parseNull((String)hmConDetails.get("NAME"));
        strComments = GmCommonClass.parseNull((String)hmConDetails.get("FCOMMENTS"));
		strAccId = GmCommonClass.parseNull((String)hmConDetails.get("ACCID"));
		strLocAccId = GmCommonClass.parseNull((String)hmConDetails.get("LOCACCID"));
		strAccId = strLocAccId.equals("")?strAccId:strLocAccId;
		strAccNm = GmCommonClass.parseNull((String)hmConDetails.get("ACNAME"));
		strLocAccNm = GmCommonClass.parseNull((String)hmConDetails.get("LOCACNAME"));
		strRepNm = GmCommonClass.parseNull((String)hmConDetails.get("REPNM"));
		strLocRepNm = GmCommonClass.parseNull((String)hmConDetails.get("LOCREPNM"));
		strRepNm = strLocRepNm.equals("")?strRepNm:strLocRepNm;
		strSetID = GmCommonClass.parseNull((String)hmConDetails.get("SETID"));
		strSetNm = GmCommonClass.parseNull((String)hmConDetails.get("SETNM"));
		strShipMode = GmCommonClass.parseNull((String)hmConDetails.get("SMODE"));
		strTrackNum = GmCommonClass.parseNull((String)hmConDetails.get("TRACK"));
		strCompanyId = GmCommonClass.parseNull((String)hmConDetails.get("DIVISION_ID"));
		strDivisionId = strDivisionId.equals("")?"2000":strDivisionId;
		strTransCompId = GmCommonClass.parseNull((String)hmConDetails.get("COMPANYCD"));

        strhospAccountID = GmCommonClass.parseNull((String)request.getAttribute("HOSPACCID"));
        strConsignAccountID = GmCommonClass.parseNull((String) request.getAttribute("CONSIGNACCID"));
        strLoanerReqId  = GmCommonClass.parseNull((String)hmConDetails.get("LOANERREQID"));//LOANERREQID PMT-45058
		
		HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(strCompId , strDivisionId);
		if(gmDataStoreVO.getCmpid().equals("1000") || gmDataStoreVO.getCmpid().equals("1001")){
			strCompanyName = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMPNAME"));
			strCompanyAddress = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMPADDRESS"));
			strComapnyLogo = "/"+GmCommonClass.parseNull((String)hmCompanyAddress.get("LOGO"))+".gif";
			strCompanyAddress = strCompanyAddress.replaceAll("/","<br>");
		}else{
			if(strIntrasitPackSlip.equals("INTRANSIT_PACK_SLIP")){
				hmCompanyAddress = GmCommonClass.fetchCompanyAddress(strTransCompId , strDivisionId);
			}
			strCompanyAddress = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYADDRESS"));
			strCompanyName = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYNAME"));
			strCompanyName = strCompanyName.replaceAll("/","<br>");
			if(strShowPhoneNo.equals("YES")){
			strCompanyPhone =  GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYPHONE"));
			strCompanyPhone = strCompanyPhone.equals("")?"":"Phone: "+strCompanyPhone;
			}
			if(strShowGMNAAddress.equals("YES")){
				  strCompanyName = GmCommonClass.parseNull((String)gmCompResourceBundleBean.getProperty("GMNA.COMPNAME"));
				  strCompanyAddress = GmCommonClass.parseNull((String)gmCompResourceBundleBean.getProperty("GMNA.COMPADDRESS"));
				  strCompanyPhone = GmCommonClass.parseNull((String)gmCompResourceBundleBean.getProperty("GMNA.PH"));
				  strCompanyPhone = strCompanyPhone.equals("")?"":strCompanyPhone;
			}
			strCompanyAddress = strCompanyAddress + "/"+ strCompanyPhone;
			strCompanyAddress = strCompanyAddress.replaceAll("/", "\n<br>");
			strComapnyLogo = "/"+GmCommonClass.parseNull((String)hmCompanyAddress.get("COMP_LOGO"))+".gif";
		}
		
		if(!strShipAdd.equals("") && strShipAdd.substring(0,6).equals("&nbsp;")){
			strShipAdd= strShipAdd.replaceFirst("&nbsp;","");
		}
	}
	if(!strGmLocalLic.equals("") || !strGmExportLic.equals(""))
	{
		strCompAddress = "D.L.NO."+strGmLocalLic+"<br>D.L.NO."+strGmExportLic;
		strCompanyAddress = strCompanyAddress + strCompAddress;
	}
	if(!strExportDruglic.equals("") || !strLocalDruglic.equals(""))
	{
		strShipAdd = strShipAdd+"<br>&nbsp;DL- "+strLocalDruglic+"<br>&nbsp;DL- "+strExportDruglic;
	}
	// to set the header based on transaction company id.
	strTransCompanyLocale = GmCommonClass.getCompanyLocale(strTransCompId);
	GmResourceBundleBean gmTransCompResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strTransCompanyLocale);
	strWhPicSlipHeader = GmCommonClass.parseNull((String) gmTransCompResourceBundleBean.getProperty("PACKING_SLIP"));
	
	String strRowSpan1 = "11";
	String strRowSpan2 = "5";
	String strShowShipDate = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.SHOW_SHIP_DATE_CON"));
	if(strShowShipDate.equalsIgnoreCase("Yes")){
	  strRowSpan1 = "15";
	  strRowSpan2 = "9";
	}

	int intSize = 0;
	HashMap hcboVal = null;

	StringBuffer sbStartSection = new StringBuffer();
	sbStartSection.append("<table border=0 width=700 cellspacing=0 cellpadding=0 align=center>");
	sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=3></td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 width=1></td>");
	sbStartSection.append("<td>");
	sbStartSection.append("<table cellpadding=0 cellspacing=0 border=0 width='100%'>");
	sbStartSection.append("<tr>");
	sbStartSection.append("<td width=170 height=70><img src=");sbStartSection.append(strImagePath +strComapnyLogo);
	sbStartSection.append(" width=138 height=60></td>");
	
	if (strPackSlipHeader.equals("SHIPPINGLIST")){  
		sbStartSection.append("<td class=RightText width=270>Globus Medical Gmbh<br>Gotthardstrasse 3<br>6304 Zug<br>Switzerland <br></td>");
		sbStartSection.append("<td align=right class=RightText width=400><font size=+3>Shipping List</font>&nbsp;</td>");
	}
	else{
		sbStartSection.append("<td class=RightText width=270><b>"+strCompanyName+"</b><br>"+strCompanyAddress+"<br></td>");
		sbStartSection.append("<td align=right class=RightText width=400><font size=+3>"+strWhPicSlipHeader+"</font>&nbsp;</td>");
	}
	
	sbStartSection.append("</tr>");
	sbStartSection.append("<tr><td bgcolor=#666666  height=1 colspan=6></td></tr>");
	sbStartSection.append("<tr><td colspan=3>");
	sbStartSection.append("<table border=0 width=100% cellspacing=0 cellpadding=0>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption>");
	
	if (strPackSlipHeader.equals("SHIPPINGLIST")){ 
		sbStartSection.append("<td height=24 align=center width=250>&nbsp;"+strShipListConsignToHeader+"</td>");
		sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=11></td>");
		sbStartSection.append("<td width=250 align=center>&nbsp;"+strShipListShippingToHeader+"</td>");
	}else if(strPackSlipHeader.equals("9110")){  /* 9110 = IHLN Type*/
		sbStartSection.append("<td height=24 align=center width=250>&nbsp;"+strLoanedToLbl+"</td>");
		sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=11></td>");
		sbStartSection.append("<td width=250 align=center>&nbsp;Ship To</td>");
	}else{
		sbStartSection.append("<td height=24 align=center width=250>&nbsp;"+strConsignToHeader+"</td>");
		sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=15></td>");
		sbStartSection.append("<td width=250 align=center>&nbsp;"+strShippingToHeader+"</td>");
	}	
	sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=15></td>");
	sbStartSection.append("<td width=100 align=center>&nbsp;"+strDateLbl+"</td>");
	if (strPackSlipHeader.equals("SHIPPINGLIST")){  
	sbStartSection.append("<td width=100 align=center>&nbsp;"+strProformaLbl+"</td>");
	}else if(strPackSlipHeader.equals("9110")){ /* 9110 = IHLN Type*/
		sbStartSection.append("<td width=100 align=center>&nbsp;Transaction ID</td>");
	}else{
		
		sbStartSection.append("<td width=100 align=center>&nbsp;"+strConsignIdField+"</td>");
	}
	sbStartSection.append("</tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
	  if(!strConsignAccountID.equals("")){
          sbStartSection.append("<tr><td class=RightText rowspan="+strRowSpan2+" valign=top>&nbsp;");sbStartSection.append(strConsignAccountID+" "+strConsignAdd);  // added strConsignAccountID in PMT-42950
	  }else{
		  sbStartSection.append("<tr><td class=RightText rowspan="+strRowSpan2+" valign=top>&nbsp;");sbStartSection.append(strConsignAdd);  
	  }
    sbStartSection.append("</td>");
	sbStartSection.append("<td rowspan="+strRowSpan2+" class=RightText valign=top>&nbsp;");
	if (strPackSlipHeader.equals("SHIPPINGLIST")){  
		sbStartSection.append(strCompanyName+"<br>"+strCompanyAddress);
	}
	else{
		 if(!strhospAccountID.equals("")){
		     sbStartSection.append(strhospAccountID+" "+strShipAdd);  // added strhospAccountID in PMT-42950
		 }else{
			 sbStartSection.append(strShipAdd);
		 }
	}
	sbStartSection.append("</td>");
	sbStartSection.append("<td height=25 class=RightText align=center>&nbsp;");sbStartSection.append(strDate);
	sbStartSection.append("</td><td class=RightText align=center>&nbsp;");
	sbStartSection.append(strConsignId);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	if(!strShowShipDate.equalsIgnoreCase("Yes")){
	  if(strPackSlipHeader.equals("9110")){ /* 9110 = IHLN Type*/
		sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption><td align=center>&nbsp;</td><td align=center>Requested By</td></tr>");
	}else{
		sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption><td align=center>&nbsp;</td><td align=center>"+strConsignNameField+"</td></tr>");
	}
	sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	sbStartSection.append("<tr><td align=center class=RightText>&nbsp;");
	sbStartSection.append("<Br></td><td align=center height=25 nowrap class=RightText>&nbsp;");sbStartSection.append(strUserName);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
	}else{
	  sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption><td align=center colspan=2>"+strShippingLable+"</td></tr>");
	  sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	  sbStartSection.append("<tr><td align=center class=RightText colspan=2 height = 18>&nbsp;"+strShippingDt);
	  sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
	  sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption><td align=center>");
		if (strPackSlipHeader.equals("SHIPPINGLIST")){  
			sbStartSection.append("&nbsp;");
		}else{
			sbStartSection.append(strAccIdLbl);
		}
		sbStartSection.append("</td><td align=center>"+strConsignNameField+"</td></tr>");
		sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
		sbStartSection.append("<tr><td align=center class=RightText height=18>");
		if (strPackSlipHeader.equals("SHIPPINGLIST")){  
			sbStartSection.append("&nbsp;");
		}else{
			sbStartSection.append(strAccId);
		}
		sbStartSection.append("</td><td align=center height=18 class=RightText>&nbsp;");
		sbStartSection.append(strUserName);
		sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");	
	}
	
	if (!strPackSlipHeader.equals("SHIPPINGLIST") && strShowShipping.equals("YES")){
	  sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption>");
	  sbStartSection.append("<td width=250 height=24 align=center >&nbsp;"+strOrderedByLbl+"</td>");
	  sbStartSection.append("<td width=250 align=center>&nbsp;"+strDetailsLbl+"</td>");
	  sbStartSection.append("<td width=80 >&nbsp;"+strModeLbl+"</td>");
	  sbStartSection.append("<td width=140 align=left>&nbsp;"+strTrackLbl+"</td></tr><tr> <td bgcolor=#666666 height=1 colspan=6></td></tr>");
	  sbStartSection.append("<tr><td width=250 height=24 align=center >&nbsp;"+strRepNm+"</td>");
	  sbStartSection.append("<td width=250 align=center>&nbsp;"+strSetID+"-"+strSetNm+"</td>");
	  sbStartSection.append("<td width=80 align=center>&nbsp;"+strShipMode+"</td>");
	  sbStartSection.append("<td width=120 align=center>&nbsp;"+strTrackNum+"</td></tr><tr> <td bgcolor=#666666 height=1 colspan=6></td></tr>");
	}
	sbStartSection.append("</table></td></tr>");
	sbStartSection.append("<tr><td colspan=3><table border=0 width=100% cellspacing=0 cellpadding=0>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption><td width=25 height=24>S#</td><td width=60 height=24>&nbsp;"+strPartNumLbl+"</td>");
	sbStartSection.append("<td  width=320>"+strPartDescLbl+"</td><td align=center width=60>"+strQtyUsedLbl+"</td>");
	sbStartSection.append("<td align=center width=80>"+strLotNumLbl+"</td>");
	//To enable thhe expiry date lable only for this distributors types 70103(Inter-Company Transfer (ICT), 70106(Inter Company Account (ICA)
	//To enable the Expiry date for GMNA San Antonio plant
	if(strExpDateFlg.equals("Y") && strExpDtEnableFl.equals("Y")|| strExpiryDateFl.equals("Y"))
	{
	sbStartSection.append("<td align=center width=100>"+strExpDateLbl+"</td>");
	}
	sbStartSection.append("</tr>");
	sbStartSection.append("<tr><td height=1 bgcolor=#666666 colspan=6></td></tr>");
	sbStartSection.append("</table></td></tr></table></td><td bgcolor=#666666 width=1></td></tr></table>");

	StringBuffer sbEndSection = new StringBuffer();
	sbEndSection.append("<table width=700 cellspacing=0 cellpadding=0 align=center>");
	sbEndSection.append("<tr><td bgcolor=#666666 width=1 rowspan=15></td><td height=1 width=698></td><td bgcolor=#666666 width=1 rowspan=15></td></tr>");
	//sbEndSection.append("<tr><td height=10>&nbsp;</td></tr>");
	//sbEndSection.append("<tr><td height=50>&nbsp;</td></tr>");
	sbEndSection.append("<tr><td height=20 class=RightText valign=top><B>"+strConsignAgentLbl+"</B></td></tr>");
	sbEndSection.append("<tr><td height=20 class=RightText valign=top><B>"+strLoanReqIdLbl+"</B>&nbsp;"+strLoanerReqId+"</td></tr>");//PMT-45058
	sbEndSection.append("<tr><td height=30 class=RightText valign=top><B><u>"+strNotesLbl+"</u></B>:");
	
	if(!strRaComments.equals("")){
	  sbEndSection.append("<BR> "+strRaComments);
	}
	
	if (!strPackSlipHeader.equals("SHIPPINGLIST")){ 
		sbEndSection.append("<BR>-"+strSpecialNotes);
	}
	
	sbEndSection.append("</td></tr>");
	 if(!strGmCompanyVat.equals("") || !strGmCompanyCst.equals("") || !strGmCompanyPAN.equals(""))
		{
			sbEndSection.append("<tr></tr><tr><td height=20 class=RightText valign=top>&nbsp;Company's VAT TIN:&nbsp; "+strGmCompanyVat+" </td></tr>");
			sbEndSection.append("<tr><td height=20 class=RightText valign=top>&nbsp;Company's CST No :&nbsp; "+strGmCompanyCst+"</td></tr>");
			sbEndSection.append("<tr><td height=20 class=RightText valign=top>&nbsp;Company's PAN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   :&nbsp; "+strGmCompanyPAN+"</td></tr>");
			
			sbEndSection.append("<tr><td colspan=2><table border=0 width=100% cellspacing=0 cellpadding=0>");
			sbEndSection.append("<tr><td colspan=3></td><td colspan=2 height=1 bgcolor=#666666></td></tr><tr class=RightTableCaption><td height=44 align=center width=150>&nbsp;</td>");
			sbEndSection.append("<td width=150 align=center>&nbsp;</td><td bgcolor=#666666 class=line width=1 rowspan=11></td>");
			sbEndSection.append("<td width=50 align=center>&nbsp;</td><td width=250 align=center valign=top>&nbsp;<b>for Globus Medical India Pvt.Ltd</b></td>");
			sbEndSection.append("</tr><tr><td class=RightText rowspan=9 valign=top></td><td rowspan=5 class=RightText valign=top>&nbsp;</td>");
			sbEndSection.append("<td height=25 class=ightText align=center>&nbsp;</td><td class=RightTableCaption align=center>&nbsp;Authorised Signatory</td>");
			sbEndSection.append("</tr></table></td></tr>");
		} 
	// To enable thhe Footer message only for this distributors types 70103(Inter-Company Transfer (ICT) 70106(Inter Company Account (ICA)
	 if(strExpDateFlg.equals("Y") && strExpDtEnableFl.equals("Y"))
		{
	   sbEndSection.append("<tr><td height=20 class=RightText valign=top><B><p style=color:red;>"+strFooterMsg+"</p></B></td></tr>");
		}
	sbEndSection.append("<tr><td height=1 bgcolor=#666666></td></tr></table>");
	if(strScreenName.equals("")&&strGUID.equals("")){
		sbEndSection.append("<table border=0 width=700 cellspacing=0 cellpadding=0><tr><td align=center height=30 id=button>");
		sbEndSection.append("<input type=button value=&nbsp;Print&nbsp; name=Btn_Print class=button onClick=fnPrint();>&nbsp;&nbsp;");
		sbEndSection.append("<input type=button value=&nbsp;Close&nbsp; name=Btn_Close class=button onClick='window.close();';>&nbsp;&nbsp;");
		sbEndSection.append("</td><tr></table>");
	}

%>
<HTML>
<HEAD>
<TITLE> Globus Medical Italy : Consignment Packing Slip</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<style>
TR.PageBreak {
     page-break-after: always;
}
</style>
<script>

function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
</HEAD>

<BODY topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<!--StartSection starts here-->
<%out.print(sbStartSection);%>
<!--StartSection ends here-->
	<table border="0" width="700" cellspacing="0" cellpadding="0" align=center>
<%
	double dbCount = 0.0;
	//intSize = alSetLoad.size();
	intSize = alPopulatedCartDtls.size();
	//if (intSize > 0)
	//{
	HashMap hmLoop = new HashMap();
	HashMap hmTempLoop = new HashMap();

	String strNextPartNum = "";
	int intCount = 0;
	String strColor = "";
	String strLine = "";
	String strPartNumHidden = "";
	String strPrice = "";
	String strTotal = "";
	String strAmount = "";
	String strExpDt = "";
	String strRowCount = "";
	double dbAmount = 0.0;
	double dbTotal = 0.0;
	int intQty = 0;
	int intRowCnt = 0;
	
	
	//int intalSize = alSetLoad.size();
	int intalSize = alPopulatedCartDtls.size();
	intSize = (intSize >=29?intSize:29);
	for (int i=0;i<intSize;i++)
	{
		intRowCnt ++;
	if(i < intalSize){
		//hmLoop = (HashMap)alSetLoad.get(i);
		hmLoop = (HashMap)alPopulatedCartDtls.get(i);
		if (i<intalSize-1 && i<intSize-1)
		{
			//hmTempLoop = (HashMap)alSetLoad.get(i+1);
			hmTempLoop = (HashMap)alPopulatedCartDtls.get(i+1);
			strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
		}
		strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
		strPartNumHidden = strPartNum;
		strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
		strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
		strExpDt = GmCommonClass.getStringFromDate((java.util.Date)hmLoop.get("EXPDATE"),strGCompDateFmt);
		strExpDt = strExpDt.equals("")||strExpDt==null?"N/A":strExpDt;
		strTemp = strPartNum;
		if (strPartNum.equals(strNextPartNum))
		{
			intCount++;
			strLine = "<tr><td bgcolor=#666666 width=1><img src=images\\spacer.gif width=1></td><TD colspan=6 height=1 bgcolor=#eeeeee></TD><td bgcolor=#666666 width=1><img src=images\\spacer.gif width=1></td></TR>";
		}

		else
		{
			strLine = "<tr><td bgcolor=#666666 width=1><img src=images\\spacer.gif width=1></td><TD colspan=6 height=1 bgcolor=#eeeeee></TD><td bgcolor=#666666 width=1><img src=images\\spacer.gif width=1></td></TR>";
			if (intCount > 0)
			{
				strPartNum = "";
				strPartDesc = "";
				strQty = "";
				strLine = "";
			}
			else
			{
				//strColor = "";
			}
			intCount = 0;
		}

		if (intCount > 1)
		{
			strPartNum = "";
			strPartDesc = "";
			strQty = "";
			strLine = "";
		}
	
		strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
		strItemQty = GmCommonClass.parseNull((String)hmLoop.get("IQTY"));
		strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
		out.print(strLine);
		strRowCount = Integer.toString(i+1);
	}else{
		strPartNum = "";
		strPartDesc ="";
		strItemQty = "";
		strControl = "";
		strRowCount = "";
		strExpDt = "";
		strShade = "";
	}
%>
		<tr <%=strShade %>>
			<td bgcolor="#666666" width="1"><img src="images\spacer.gif" width="1"></td>
			<td class="RightText" width="25"><%=strRowCount%></td>
			<td class="RightText" width="60" height="20">&nbsp;<%=strPartNum%></td>
			<td class="RightText" width="350"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
			<td class="RightText" align="center" width="60">&nbsp;<%=strItemQty%></td>
			<td class="RightText" align="center" width="100"><%=strControl%></td>
			<!-- To enable thhe expiry date value only for this distributors types 70103(Inter-Company Transfer (ICT), 70106(Inter Company Account (ICA) -->
  <%if(strExpDateFlg.equals("Y") && strExpDtEnableFl.equals("Y") || strExpiryDateFl.equals("Y") )
	      {%>
		         <td align="center" width="100"><%=strExpDt%></td>   
		  <%}%>
			<td bgcolor="#666666" width="1"></td>
		</tr>
<%
		if (intRowCnt == 30)
		{
%>
		<tr><td colspan="7" height="100%"></td></tr>
		<tr><td colspan="7" height="1" bgcolor="#666666"></td></tr>
		<tr><td colspan="7" height="20" class="RightText" align="right">Cont. on Next Page -></td></tr>
		</table>
		<p><br style="page-break-before: always;" clear="all" /></p>
<%
		out.print(sbStartSection);
%>
		<table border="0" width="700" cellspacing="0" cellpadding="0" align=center>
<%
		intRowCnt = 0;
		} //end of IF

	} // end of FOR
%>
	</table>
<!--EndSection starts here-->
<%out.print(sbEndSection);%>
<!--EndSection ends here-->
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
