 <%
/**********************************************************************************
 * File		 		: GmPriceRequestPopup.jsp
 * Desc		 		: This screen is used to diplay the held order details with price request
 * Author			: Karthik
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@page import="java.util.Date"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- custservice\GmPriceRequestPopup.jsp -->
<%@ taglib prefix="fmtOrdRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<fmtChangePrice:setLocale value="<%=strLocale%>"/>
<fmtChangePrice:setBundle basename="properties.labels.custservice.ModifyOrder.GmOrderItemModify"/>

<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String gridData = (String) request.getAttribute("alResult")==null?"":(String) request.getAttribute("alResult");
	String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strPriceRequestdata = GmCommonClass.parseNull((String)request.getAttribute("PRICEREQTDET"));
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Order Details </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmOrderItemModify.js"></script>
<script>
var objGridData;
objGridData = '<%=strPriceRequestdata%>';

function fnClose(){
	parent.w4.close();
}

function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/javascript/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.enableMultiline(true);
	gObj.enablePaging(true, 100, 10, "pagingArea", true, "infoArea");
	gObj.setPagingSkin("bricks");
	gObj.init();
	gObj.loadXMLString(gridData);
	gObj.attachHeader("#select_filter,'',#select_filter,#text_filter,#text_filter");
	return gObj;
}

function fnOnPageLoad(){
	if (objGridData.value != '') {
		gridObj = initGridData('pricerequestdetail', objGridData);
	}
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">

	<table border="0"  width="600" cellspacing="0" cellpadding="0">
		<%if(!strPriceRequestdata.equals("")){%>
		<tr>
			<td colspan="6">
				<div  id ="pricerequestdetail" style="width:550px;height:300px"></div>
			</td>
		</tr> 
		<%} %>
		<tr>			
			<td align="center" height="30">
					<gmjsp:button value="&nbsp;Close&nbsp;" gmClass="button" onClick="fnClose();" buttonType="Load" />
			</td>
		</tr>
	</table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
