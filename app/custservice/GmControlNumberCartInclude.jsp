<%
/**********************************************************************************
 * File		 		: GmControlNumberCartInclude.jsp
 * Desc		 		: This screen is used to edit the control number details
                      used in O.R.
 * Version	 		: 1.0
 * author			: Venkata Prasath
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@page import="java.util.Date"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib prefix="fmtControlCartInclude" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmControlNumberCartInclude.jsp -->
<fmtControlCartInclude:setLocale value="<%=strLocale%>"/>
<fmtControlCartInclude:setBundle basename="properties.labels.custservice.GmControlNumberCartInclude"/>
<%
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);	
	ArrayList alCtrlNmDetails = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alControlNumberDetails"));
	String strSkpUsdLotValid =  GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SKP_USED_LOT_VALID"));
	GmResourceBundleBean gmPaperworkResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    String strShowUsageLot =
        GmCommonClass.parseNull(gmPaperworkResourceBundleBean.getProperty("DO.SHOW_USAGE_LOT"));
    String strCompanyId = GmCommonClass.parseNull(gmDataStoreVO.getCmpid());
%>

<script>
var count = 0;
var orderSize = '<%=alCtrlNmDetails.size()%>';
var skpUsdLotValid = '<%=strSkpUsdLotValid%>';
var showUsageLotFl = '<%=strShowUsageLot%>';
// MNTTASK-86523 - To check ITEMQTY and ATTRIBUTE QTY when edit qty in edit control number screen.
<%	
HashMap hmControlNumDet = new HashMap();
for (int i=0;i< alCtrlNmDetails.size();i++)
{
	hmControlNumDet = (HashMap) alCtrlNmDetails.get(i);
%>
var ItemQtyArr<%=i%> ="<%=hmControlNumDet.get("PNUM")%>,<%=hmControlNumDet.get("ITEM_QTY")%>";
var ordid = "<%=hmControlNumDet.get("ORDER_ID")%>";
<%
}
%>
</script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmControlNumberIncludeCart.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<table cellpadding="0" cellspacing="0" width="100%" border="1" bordercolor="gainsboro" id="EditControlNumber">
			  <thead>
				<TR class="Shade" class="RightTableCaption" style="position:relative;">
				<%-- <% if (strFormName.equals("frmVendor")) { %>
					<TH class="RightText" width="40" align="center">Remove</TH>
				<% } %>	 --%>
				<!-- Added below code to show order id for italy company
								Incorrect correct quantity displayed in usage screen when same Part # is on the Parent and Back Order(PC-2827) -->											
				<%if(strCompanyId.equals("1020")){ %>
					<TH class="RightText" width="150" align="left" height="25"><fmtControlCartInclude:message key="LBL_ORDER_ID"/> </TH>
				<%} %>	
					<TH class="RightText" width="110" align="center" height="25"><fmtControlCartInclude:message key="LBL_PART"/> #</TH>
					<TH class="RightText" width="330" align ="left">&nbsp;<fmtControlCartInclude:message key="LBL_PARTDESCRIPTION"/></TH>
					<TH class="RightText" width="50" align="center"><fmtControlCartInclude:message key="LBL_QTY"/></TH>
					<TH class="RightText" width="210" align="center"><fmtControlCartInclude:message key="LBL_CONTROL"/>#</TH>
				</TR>	
			  </thead>
			  <TBODY>
<%
					int intSize  = alCtrlNmDetails.size();

					if ( intSize == 0 )
					{
						%>
						<tr >
						<td  height="20" class="RightText" colspan = 6>
							<fmtControlCartInclude:message key="LBL_DO_DOES_NOT_NEED_CONTROL"/>
						</td>
						</tr>
					<%	
					}
					
					String strUsageDetailId = "";
					String strDDTNum = "";
					String strItemOrderId = "";
					String strOrderInfoId = "";
					String strOrderId = "";
					
					for (int i=0;i<intSize;i++)
			  		{
						HashMap hmControlNum =(HashMap)alCtrlNmDetails.get(i); 
						String strDesc ="";
						String strPartNum ="";
						String strQty = "";
						String strItemQty = "";
						String strControlNum ="";
						String strPkey = "";
						String strCnumNeedFl = "";
						String strProdMat = "";
						
						strPkey =  GmCommonClass.parseNull((String)hmControlNum.get("PKEY"));
						strPartNum =  GmCommonClass.parseNull((String)hmControlNum.get("PNUM"));
						strDesc =  GmCommonClass.parseNull((String)hmControlNum.get("PDESC"));
						strControlNum = GmCommonClass.parseNull((String)hmControlNum.get("CNUM"));
						strQty =  GmCommonClass.parseNull((String)hmControlNum.get("IQTY"));
						strItemQty =  GmCommonClass.parseNull((String)hmControlNum.get("ITEM_QTY"));
						strCnumNeedFl =  GmCommonClass.parseNull((String)hmControlNum.get("CNUMFL"));
						strUsageDetailId =  GmCommonClass.parseNull((String)hmControlNum.get("PKEY"));
						strDDTNum =  GmCommonClass.parseNull((String)hmControlNum.get("DDT_NUM"));
						strItemOrderId=  GmCommonClass.parseNull((String)hmControlNum.get("ITEMID"));
						strOrderInfoId = GmCommonClass.parseNull((String)hmControlNum.get("ORDER_INFO_ID")); 
						strOrderId = GmCommonClass.parseNull((String)hmControlNum.get("ORDER_ID"));
						strProdMat = GmCommonClass.parseNull((String)hmControlNum.get("PRODMAT"));  //ProdMat for Tissue(100845) Lot number validation used in PC-4659
						
						String strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
%>
								<tr <%=strShade %>>
										<%-- <% if (strFormName.equals("frmVendor")) { %>
											<td><input type="checkbox" name="chkVoidOrder<%=i+1%>"></input> </td>
										<% } %>	 --%>
								<!-- Added below code to show order id for italy company
								Incorrect correct quantity displayed in usage screen when same Part # is on the Parent and Back Order(PC-2827) -->		
									<%if(strCompanyId.equals("1020")){ %>	
										<td id="orderId<%=i+1%>" class="RightText">&nbsp;<%=strOrderId%></td>
									<%} %>
									<td  height="20" class="RightText">
										&nbsp;<a href="javascript:fnAddRow(<%=i+1%>,'<%=strPartNum %>', '<%=strUsageDetailId %>', '<%=strDDTNum %>', '<%=strItemOrderId %>', '<%=strOrderInfoId %>', '<%=strOrderId %>','<%=strProdMat %>');" ><IMG id="partInc<%=i+1%>" border=0 src="<%=strImagePath%>/plus.gif"></a>&nbsp;&nbsp;<%=strPartNum%>
										<input type="hidden" name="hPartNum<%=i+1%>" tabindex="-1" value="<%=strPartNum%>" />
										<input type="hidden" name="hUsageDetailsId<%=i+1%>" tabindex="-1" value="<%=strUsageDetailId %>" />
										<input type="hidden" name="hTmpUsageDetailsId<%=i+1%>" tabindex="-1" value="<%=strUsageDetailId %>" />
										<input type="hidden" name="hLotQty<%=i+1%>" value="<%=strQty%>">
										<input type="hidden" name="hDDT<%=i+1%>" tabindex="-1" value="<%=strDDTNum %>" />
										<input type="hidden" name="hItemOrderId<%=i+1%>" tabindex="-1" value="<%=strItemOrderId %>" />
										<input type="hidden" name="hOrderInfoId<%=i+1%>" tabindex="-1" value="<%=strOrderInfoId %>" />
										<input type="hidden" name="hUsedLotOrderId<%=i+1%>" tabindex="-1" value="<%=strOrderId %>" />
										 <!--strProdMat for Tissue(100845) Lot number validation used in PC-4659  -->	
										<input type="hidden" name="hProdMat<%=i+1%>" tabindex="-1" value="<%=strProdMat %>" /> 
								   </td>
									<td id="CellRemove<%=i+1%>" class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%>
								    </td>
									<td align="center" id="Qty<%=i+1%>" class="RightText">&nbsp;<input type="text" name="qtyTxt<%=i+1%>" value="<%=strQty%>" size="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/> 
									<input type="hidden" name="hQty<%=i+1%>" value="<%=strItemQty%>" tabindex="1" size="2">
									</td>
									<td id="controlNum<%=i+1%>" class="RightText" align="left">&nbsp;<input type="text" id="controlNumId<%=i+1%>" name="controlNumTxt<%=i+1%>" value="<%=strControlNum%>" style="text-transform: uppercase;" maxlength="40" size="25" onBlur="fnAjaxValidateCtrlNum(<%=i+1%>,this);changeBgColor(this,'#ffffff');" onFocus="fnRemoveImage(<%=i+1%>,this);changeBgColor(this,'#AACCE8');" />
									<input type="hidden" name="hcontrolnum<%=i+1%>"   id="hcontrolnum<%=i+1%>"  tabindex="2" value= "<%=strControlNum%>"/>
									<input type="hidden" name="hPkey<%=i+1%>"  tabindex="2" value= "<%=strPkey%>"/>
									<input type="hidden" name="hCnumFl<%=i+1%>" id="hCnumFl<%=i+1%>"  tabindex="2" value= "<%=strCnumNeedFl%>"/>
									<fmtControlCartInclude:message key="LBL_CONTROL_NUMBER_AVAILABLE" var="varControlNumAvail"/>
									<span id="DivShowCnumAvl<%=i+1%>" style=" vertical-align:middle; display: none;"> <img height="16" width="19" title="${varControlNumAvail}" src="<%=strImagePath%>/success.gif"></img></span>
						 			<fmtControlCartInclude:message key="LBL_LOT_CODE_REPORT" var="varLotCodeReport"/>
						 			<fmtControlCartInclude:message key="LBL_CONTROL_NUMBER_DOESNOT_EXIST" var="varControlNumDoesNotExist"/>
						 			<span id="DivShowCnumNotExists<%=i+1%>" style=" vertical-align:middle; display: none;"> <img height="12" width="12" title="${varControlNumDoesNotExist}" src="<%=strImagePath%>/delete.gif"></img>&nbsp;<a href="javascript:fnViewLotReport('<%=i+1 %>')"><img height="15" border="0" title="${varLotCodeReport}" src="<%=strImagePath%>/location.png"></img></a></span>
									</td>
								</tr>
<%								} // For Loop ends here					
%>
			  </TBODY>
			</table>
			<INPUT type="hidden" name="hTextboxCnt" value="<%=intSize%>" />