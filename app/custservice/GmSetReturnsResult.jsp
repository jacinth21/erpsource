<%@ include file="/common/GmHeader.inc" %>

<%
/**********************************************************************************
 * File		 		: GmSetReturnsResult.jsp
 * Desc		 		: This screen is used for the
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtSetReturnsResult" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- custservice\GmSetReturnsResult.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtSetReturnsResult:setLocale value="<%=strLocale%>"/>
<fmtSetReturnsResult:setBundle basename="properties.labels.custservice.GmSetReturnsResult"/>


<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = new HashMap();
	HashMap hmReturnDetails = new HashMap();

	hmReturn = (HashMap)request.getAttribute("hmReturn");

	String strDesc = "";
	String strShade = "";
	String strRAId = "";
	String strSetName= "";
	String strDistName = "";
	String strUserName = "";
	String strItemQty = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strType = "";
	String strReason = "";
	String strTypeId = "";
	String strReasonId = "";
	String strIniDate = "";
	String strOrderId = "";
	String strRetDate = "";
	String strExpDate = "";
	String strAccName = "";
	String strEmpName = "";

	ArrayList alRetLoad = new ArrayList();

	if (hmReturn != null)
	{
			alRetLoad = (ArrayList)hmReturn.get("RAITEMDETAILS");
			hmReturnDetails = (HashMap)hmReturn.get("RADETAILS");

			strRAId = (String)hmReturnDetails.get("RAID");
			strSetName= (String)hmReturnDetails.get("SNAME");
			strDistName = (String)hmReturnDetails.get("DNAME");
			strDesc = (String)hmReturnDetails.get("COMMENTS");
			strAccName = (String)hmReturnDetails.get("ANAME");
			strType = (String)hmReturnDetails.get("TYPE");
			strReason = (String)hmReturnDetails.get("REASON");
			strTypeId = (String)hmReturnDetails.get("TYPEID");
			strReasonId = (String)hmReturnDetails.get("REASONID");
			strIniDate = (String)hmReturnDetails.get("CDATE");
			strUserName = (String)hmReturnDetails.get("CUSER");
			strOrderId = (String)hmReturnDetails.get("ORDID");			
			strRetDate = GmCommonClass.parseNull((String)hmReturnDetails.get("RETDATE"));
			strExpDate = GmCommonClass.parseNull((String)hmReturnDetails.get("EDATE"));
			strEmpName = GmCommonClass.parseNull((String)hmReturnDetails.get("UNAME"));
			strDistName = strDistName.equals("")?strAccName:strDistName;
	}
	
	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Returns Result</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmOrder" method="post">
	<table border="0" width="600" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" colspan="3"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
			<td>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25"  class="RightDashBoardHeader" colspan="4">&nbsp;<fmtSetReturnsResult:message key="LBL_RETURNS_RESULT"/></td>
					</tr>
					<tr><td bgcolor="#666666" colspan="4"></td></tr>
					<tr>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtSetReturnsResult:message key="LBL_RA_ID"/>:</td>
						<td class="RightText" width="100">&nbsp;<%=strRAId%></td>
						<td class="RightTableCaption" align="right">&nbsp;<fmtSetReturnsResult:message key="LBL_TYPE"/>:</td>
						<td class="RightText" >&nbsp;<%=strType%></td>
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtSetReturnsResult:message key="LBL_INITIATED_DATE"/>:</td>
						<td class="RightText">&nbsp;<%=strIniDate%></td>
						<td class="RightTableCaption" align="right">&nbsp;<fmtSetReturnsResult:message key="LBL_REASON"/>:</td>
						<td class="RightText">&nbsp;<%=strReason%></td>
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtSetReturnsResult:message key="LBL_INITIATED_BY"/>:</td>
						<td class="RightText">&nbsp;<%=strUserName%></td>
						<td class="RightTableCaption" align="right">&nbsp;<fmtSetReturnsResult:message key="LBL_SET_NAME"/>:</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strSetName)%></td>
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtSetReturnsResult:message key="LBL_ACCOUNT_DISTRIBUTOR"/>:</td>
						<td class="RightText">&nbsp;<%=strDistName%><BR><%=strEmpName%></td>
						<td class="RightTableCaption" align="right">&nbsp;<fmtSetReturnsResult:message key="LBL_ORDER_ID"/>:</td>
						<td class="RightText">&nbsp;<%=strOrderId%></td>						
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="40"><fmtSetReturnsResult:message key="LBL_COMMENTS"/>:</td>
						<td class="RightText" colspan="3">&nbsp;<%=strDesc%></td>
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr>
						<td HEIGHT="23" class="RightTableCaption" align="right"><fmtSetReturnsResult:message key="LBL_DATE_EXPECTED"/>:</td>
						<td class="RightText">&nbsp;<%=strExpDate%></td>
						<td HEIGHT="23" class="RightTableCaption" align="right">&nbsp;</td>
						<td class="RightText">&nbsp;</td>
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr>
						<td colspan="4">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td width="60" align="center" height="25"><fmtSetReturnsResult:message key="LBL_PART_NUMBER"/></td>
									<td width="400"><fmtSetReturnsResult:message key="LBL_DESCRIPTION"/></td>
									<td width="60" align="center"><fmtSetReturnsResult:message key="LBL_QTY_TO_RETURN"/></td>
									<td width="60" align="center">&nbsp;</td>
									<td width="60" align="center">&nbsp;</td>
								</tr>
								<tr><td class=Line colspan=5 height=1></td></tr>
<%
						if (alRetLoad != null)
						{
							intSize = alRetLoad.size();
						}
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();

							int intSetQty = 0;
							int intRetQty = 0;
							int intReconQty = 0;
							String strReadOnly = "";
							
							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alRetLoad.get(i);

								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strQty = GmCommonClass.parseZero((String)hmLoop.get("QTY"));
								strQty = strQty.equals("")?"0":strQty;
								intSetQty = Integer.parseInt(strQty);
								strItemQty = GmCommonClass.parseNull((String)hmLoop.get("RETQTY"));
								strItemQty = strItemQty.equals("")?"0":strItemQty;
								intRetQty = Integer.parseInt(strItemQty);
								intReconQty = intSetQty - intRetQty;
								strReadOnly = intReconQty == 0?"ReadOnly":"";

%>
								<tr>
									<td class="RightText" height="20">&nbsp;<%=strPartNum%>
										<input type="hidden" name="hPartNum<%=i%>" value="<%=strPartNum%>"></td>
									<td class="RightText"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td align="center" class="RightText"><%=strQty%>
										<input type="hidden" name="hSpecQty<%=i%>" value="<%=strQty%>"></td>
									<td align="center" class="RightText">&nbsp;</td>
									<td align="center" class="RightText">&nbsp;</td>
								</tr>
								<tr><td  colspan="5" height="1" bgcolor="#eeeeee"></td></tr>
<%
							}//End of FOR Loop
						}else {
%>
						<tr><td colspan="5" height="50" align="center" class="RightTextRed"><BR><fmtSetReturnsResult:message key="LBL_NO_PART_NUMBERS"/>!</td></tr>
<%
						}		
%>
							</table>
						</td>	<input type="hidden" name="hCnt" value="<%=intSize%>">
					</tr>					
				</table>
			</td>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" colspan="3"></td>
		</tr>
	</table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
