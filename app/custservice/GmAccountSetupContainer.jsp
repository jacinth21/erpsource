
<!-- \CS Label changes\custservice\GmAccountSetupContainer.jsp -->
<%
/**********************************************************************************
 * File		 		: GmAccountSetupContainer.jsp
 * Desc		 		: This screen is used for account related details tab.
 * Version	 		: 1.0
 * author			: Karthik
************************************************************************************/
%>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<%@ taglib prefix="fmtAccountSetupContainer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtAccountSetupContainer:setLocale value="<%=strLocale%>"/>
<fmtAccountSetupContainer:setBundle basename="properties.labels.custservice.GmAccountSetupContainer"/>
<%
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strWikiTitle = GmCommonClass.getWikiTitle("ACCOUNT_SETUP");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmAccountDetails = null;
	ArrayList alLog = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("hmLog"));
	ArrayList alAccountsList = new ArrayList();
	
	String strSelected = "";
	String strCodeID = "";
	String strAccName = "";
	String strAccId = "";
	String strPartyId = "";
	String strDeptId = GmCommonClass.parseNull((String)session.getAttribute("strSessDeptId"));
	String strAccAddEditAcsFl = GmCommonClass.parseNull((String)request.getAttribute("ACC_ADD_EDIT_ACCESS"));
	String strAddEditAcsDisabled = (strAccAddEditAcsFl.equals("Y"))?"false":"true";

	if (hmReturn != null )
	{
		hmAccountDetails = (HashMap)hmReturn.get("ACCOUNTDETAILS");
		if (hmAccountDetails != null)
		{
			strAccName = (String)hmAccountDetails.get("ANAME");
			strAccId = (String)hmAccountDetails.get("ID");
			strPartyId = GmCommonClass.parseNull((String)hmAccountDetails.get("PARTYID"));
			
		}
	}	
	int intSize = 0;
	HashMap hcboVal = null;
	String strTDHeight = "1070px";
	String strTDWidth = "1100x";
	if(alLog.size() > 0 ){
		strTDHeight = "1100px";
	}
	
%>
<HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<TITLE> Globus Medical: Account Setup </TITLE>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxlayout.css">
<link rel="STYLESHEET" type="text/css"href="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.css">
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmAccountSetup.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>

<script>
var partyId = '<%=strPartyId%>';
var href;//To give the path
var activeTab;
var aSize = '<%=alLog.size()%>';
var trSize = '<%=strTDHeight%>';
var myTab = "";
var strAccId = "";

function fnLoadTab()
{
	
	myTab = new dhtmlXTabBar("my_tabbar");
	myTab.setSkin("winscarf");
	myTab.setImagePath("<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/imgs/");
	myTab.addTab("basic",message_operations[713]);
	myTab.addTab("shipping", message_operations[714]);
	myTab.addTab("invoice", message_operations[715]);
	myTab.addTab("pricing", message_operations[716]);	
	myTab.attachEvent("onSelect", function(id,last_id){			
	global_param = "&companyInfo="+encodeURIComponent(JSON.stringify(top.gCompanyInfo));
	strAccId = document.frmAccount.Txt_AccId.value;
	        if(id=='basic'){
	        		document.getElementById("my_tabbar").style.height = trSize;	
		            href = "/GmAccountServlet?method=loadEditAccount&hAction=Reload&hOpt=DIVCONTAINER&hAccountId="+strAccId+"&hTabName=basic"+global_param;			                  
		           // myTab.cells(id).progressOff();
			    }else if(id=='shipping'){
			    	document.getElementById("my_tabbar").style.height = "450px";
			    	href = "/gmPartyShipParamsSetup.do?method=setPartyShipParam&strOpt=fetchParams&screenType=group&accountId="+strAccId+"&partyID="+partyId+"&hTabName=shipping"+global_param;			    	
				}else if(id=='invoice'){
					document.getElementById("my_tabbar").style.height = "550px";
	                href = "/gmRuleParamSetup.do?codeGrpId=SHPPM&ruleGrpId="+strAccId+"&hTabName=invoice"+global_param;

			    }else if(id=='pricing'){
			    	document.getElementById("my_tabbar").style.height = "500px";		    	 	     
				    href="/gmPricingParamsSetup.do?method=fetchPricingParams&accid="+strAccId+"&hTabName=pricing"+global_param;
				}
		    
	   myTab.cells(id).attachURL(href); //Loading the contents in tab
	   return true;
	   });
	}

function fnOnloadpage(){
	 document.getElementById("Txt_AccId").disabled = true;
	 document.getElementById("Btn_AddNew").disabled = true;
	 strAccId = document.getElementById("Txt_AccId").value;
	 myTab.setTabInActive(); 
	 myTab.setTabActive("basic"); //To set basic info tab active when loaing the page	
}	

function fnAddNewAcc()
{
	document.frmAccount.Cbo_AccId.value = '0';
	document.frmAccount.Txt_AccId.value = '';
	strAccId = '';
	myTab.setTabInActive(); 
	myTab.setTabActive("basic"); //To set basic info tab active when loaing the page
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnLoadTab();" >
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmAccountServlet">
<input type="hidden" name="hAccountId" value="<%=strAccId%>">
<input type="hidden" name="hReloadCount">
<input type="hidden" name="hOpt" value="">
	<table class="DtTable800" cellspacing="0" cellpadding="0" style="height: auto;">
		<tr>	
		
			<td height="25" class="RightDashBoardHeader"><fmtAccountSetupContainer:message key="LBL_ACCOUNT_SETUP"/></td>
			<td align="right" class=RightDashBoardHeader > 
			<fmtAccountSetupContainer:message key="IMG_ALT_HELP" var="varHelp"/>
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
			</td>
		</tr>
		<tr><td colspan="2" height="1" ></td></tr>
		<tr>
			<td width="798" height="100" valign="top" colspan="2" style="height: auto;">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<fmtAccountSetupContainer:message key="LBL_REP_ACCOUNT" var="varRepAccount"/>
						<gmjsp:label type="BoldText" width="13%" SFLblControlName="${varRepAccount}:" td="true"/>

<td ><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="Cbo_AccId" />
					<jsp:param name="METHOD_LOAD" value="loadRepAccountList" />
					<jsp:param name="WIDTH" value="485" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value="1"/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%=strAccName %>" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="<%=strAccId %>" />
					<jsp:param name="SHOW_DATA" value="100" />
					<jsp:param name="AUTO_RELOAD" value="fnCheckAcct('check');" />					
				</jsp:include></td><td>
				&nbsp;<input type="text" size="10" value="<%=strAccId%>" name="Txt_AccId" id="Txt_AccId" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" onChange="javascript:fnCheckAcct(this);" tabindex=2>
						&nbsp; 
						<fmtAccountSetupContainer:message key="LBL_ADD_NEW" var="varAddNew"/>
						<gmjsp:button style="width: 7em"  value="${varAddNew}" disabled="<%=strAddEditAcsDisabled%>" controlId="Btn_AddNew" name="Btn_AddNew" gmClass="button" onClick="fnAddNewAcc();" buttonType="Fetch" tabindex="3"/>
				</td>
					</tr>
					<tr colspan=2>
						<td class="RightTextBlue" colspan="2" align="center">
							<fmtAccountSetupContainer:message key="LBL_CHOOSE_ONE_FROM_LIST_ABOVE_TO_EDIT_OR_ENTER_DETAILS_BELOW_FOR_NEW_ACCOUNT"/>
							
						</td>
					</tr>				
											
<!-- 	DHTMLX tabs -->
				    <tr><td colspan="16" height="1" ></td></tr>

					<tr>				
			            <td  colspan="16" id="my_tabbar" style=" height:'<%=strTDHeight%>'; width:'<%=strTDWidth%>'"></td>			    
			        </tr>
				</table>
			</td>
		</tr>
    </table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
