 <%
/**********************************************************************************
 * File		 		: GmPartNumSearchDrill.jsp
 * Desc		 		: This screen is used for the Part Number Search Drilldown Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib prefix="fmtPartSearchDrill" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- custservice\GmPartNumSearchDrill.jsp -->
<fmtPartSearchDrill:setLocale value="<%=strLocale%>"/>
<fmtPartSearchDrill:setBundle basename="properties.labels.custservice.GmPartNumSearch"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strApplnDateFmt = strGCompDateFmt;
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strPartNum = (String)request.getAttribute("hPartNum")==null?"":(String)request.getAttribute("hPartNum");
	String strControl = (String)request.getAttribute("hCNum")==null?"":(String)request.getAttribute("hCNum");
	String strFrmDate = (String)request.getAttribute("hFrom")==null?"":(String)request.getAttribute("hFrom");
	String strToDate = (String)request.getAttribute("hTo")==null?"":(String)request.getAttribute("hTo");

	ArrayList alReport = new ArrayList();

	if (hmReturn != null) 
	{
		alReport = (ArrayList)hmReturn.get("DRILLREPORT");

	}

	int intLength = alReport.size();

	HashMap hmLoop = new HashMap();

	String strId = "";
	String strShipDate = "";
	String strControlNum = "";
	String strQty = "";
	String strSetId = "";
	String strType = (String)request.getAttribute("hType")==null?"":(String)request.getAttribute("hType");
	
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmPartNumSearch", strSessCompanyLocale);
	String strLblOrder = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_PART_DRILL_ORD_HEADER"));
	String strLblConsign = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_PART_DRILL_CONS_HEADER"));
	String strHeader = strType.equals("S")?strLblOrder:strLblConsign;
	String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\",\"cmplangid\":\""+gmDataStoreVO.getCmplangid()+"\"}";
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Number Search Drilldown</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script>
//declaring company info
var companyInfo='<%=strCompanyInfo%>';
var compnyid=null;
var applDateFrmt=null;
var plantid=null;
var partyid=null;
var cmplangid=null;
try{
	compnyid=self.opener.top.compyid;
	applDateFrmt=self.opener.top.appleDateFmt;
	plantid=self.opener.top.plantid;
	partyid=self.opener.top.partyid;
	cmplangid=self.opener.top.cmplangid;	
	companyInfo={};
	companyInfo.cmpid = compnyid;
	companyInfo.plantid = plantid;
	companyInfo.token = "";
	companyInfo.partyid=partyid;
	companyInfo.cmplangid=cmplangid;
	companyInfo = encodeURIComponent(JSON.stringify(companyInfo));
}catch(err){
	compnyid=null;
	applDateFrmt=null;
	plantid=null;
	partyid=null;
	cmplangid=null;
}
companyInfo = (compnyid != null && compnyid != undefined )?companyInfo:'<%=strCompanyInfo%>';
function fnPrintVer(val)
{
	var type = document.frmAccount.hType.value;
	if (type == 'C')
	{
        windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId="+val+"&companyInfo="+encodeURIComponent(companyInfo),"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
	}
	else if (type == 'R')
	{
			windowOpener("/GmPrintCreditServlet?hAction=PRINT&hRAID="+val+"&companyInfo="+encodeURIComponent(companyInfo),"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=850");
    }
	else
	{
        windowOpener("/GmEditOrderServlet?hMode=PrintPrice&hOrdId="+val+"&companyInfo="+encodeURIComponent(companyInfo),"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=760,height=410");                
	}
}


</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmPartNumSearchServlet">
<input type="hidden" name="hType" value="<%=strType%>">

	<table border="0" width="450" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="10" width="1" class="Line"></td>
			<td height="1" bgcolor="#666666"></td>
			<td rowspan="10" width="1" class="Line"></td>
		</tr>
		<tr>
			<td height="25" class="RightDashBoardHeader">
				<fmtPartSearchDrill:message key="TD_PART_DRILL_HEADER"/>
			</td>
		</tr>
		<tr><td class="Line" height="1"></td></tr>
		<tr>
			<td>
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td align="right" class="RightText" HEIGHT="24" width="150"><fmtPartSearchDrill:message key="LBL_PART_NUM"/>:</td>
						<td  class="RightText">&nbsp;<%=strPartNum%>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtPartSearchDrill:message key="LBL_CTRL_NUM"/>&nbsp;<%=strControl%></td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
					<tr>
						<td align="right" class="RightText" HEIGHT="24" width="150"><fmtPartSearchDrill:message key="LBL_FROM_DATE"/>:</td>
						<td class="RightText">&nbsp;<%=strFrmDate%>&nbsp;&nbsp;&nbsp;<fmtPartSearchDrill:message key="LBL_TO_DATE"/>: &nbsp;<%=strToDate%></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td class="Line" height="1"></td></tr>
		<tr>
			<td>
				<table border="0" width="448" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td width="120">&nbsp;<%=strHeader%> <fmtPartSearchDrill:message key="LBL_ID"/></td>
						<td width="50" align="center"><fmtPartSearchDrill:message key="LBL_QTY"/></td>
						<td width="100" align="center"><fmtPartSearchDrill:message key="LBL_SHIP_DT"/></td>
						<td HEIGHT="24" align="center"><fmtPartSearchDrill:message key="LBL_CTRL_NUM"/></td>
						<td align="center"><fmtPartSearchDrill:message key="LBL_TYPE"/></td>
					</tr>
<%
			if (intLength > 0)
			{
				for (int i = 0;i < intLength ;i++ )
				{
					hmLoop = (HashMap)alReport.get(i);

					strId = GmCommonClass.parseNull((String)hmLoop.get("CID"));
					strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
					strShipDate = GmCommonClass.getStringFromDate((java.sql.Date)hmLoop.get("SDATE"),strApplnDateFmt);
					strControlNum = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
					strSetId = GmCommonClass.parseNull((String)hmLoop.get("SETID"));
					strSetId = strSetId.equals("")?"Item":"Set";
%>
					<tr>
						<td class="RightText">&nbsp;<a class="RightText" href="javascript:fnPrintVer('<%=strId%>');"><%=strId%></a></td>
						<td class="RightText" align="center"><%=strQty%>&nbsp;</td>
						<td class="RightText" align="center">&nbsp;<%=strShipDate%></td>
						<td class="RightText" align="center" height="20">&nbsp;<%=strControlNum%></td>
						<td class="RightText" align="center">&nbsp;<%=strSetId%></td>
					</tr>
					<tr><td colspan="5" height="1" bgcolor="#eeeeee"></td></tr>
<%
				}// end of FOR
			} else	{
%>
					<tr>
						<td height="30" colspan="5" align="center" class="RightTextBlue"><fmtPartSearchDrill:message key="MSG_DATA_NOT_AVAIL"/></td>
					</tr>
<%
		}
%>
					<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
				</table>
			</td>
		</tr>
	</table>
	<BR>		<fmtPartSearchDrill:message key="BTN_CLOSE" var="varClose"/>
				<center><gmjsp:button gmClass="button" value="${varClose}" onClick="window.close();" buttonType="Load" /></center>

</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
