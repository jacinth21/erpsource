<!-- \CS Label changes\custservice\GmAccountSetup.jsp -->
 
 <%
/**********************************************************************************
 * File		 		: GmAccountSetup.jsp
 * Desc		 		: This screen is used for the Distributorship Maintenance
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<!-- \custservice\GmAccountSetup.jsp -->
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="fmtAccountSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtAccountSetup:setLocale value="<%=strLocale%>"/>
<fmtAccountSetup:setBundle basename="properties.labels.custservice.GmAccountSetup"/>
<%
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strAcctPricing = GmCommonClass.parseNull(GmCommonClass.getString("LOAD_LIST_PRICE"));
	String strWikiTitle = GmCommonClass.getWikiTitle("ACCOUNT_SETUP");
	String strShipToAddWikiTitle = GmCommonClass.getWikiTitle("ACCOUNT_SHIP_TO_ADDRESS");
	String strUploadAcctPrice = GmCommonClass.parseNull(GmCommonClass.getString("UPLOAD_ACCT_PRICE"));
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strMapAccParams = GmCommonClass.parseNull((String) request.getAttribute("SHOW_ACC_PARAMS"));
	HashMap hmReturn = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturn"));
	HashMap hmRepAccountDetails = null;
	HashMap hmParentAccountDetails = null;
	//HashMap hmVal = null; //Account Affiliation part moved to PricingParameter tab so corresponding codes are commented.
	
	String strhAction =  GmCommonClass.parseNull((String) request.getAttribute("hAction")); //for credit hold button enable after reload
	String strCompanyId = GmCommonClass.parseNull((String)gmDataStoreVO.getCmpid());
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "checked";
	//String strCodeNm = "";
	
	ArrayList alAccountsList = new ArrayList();
	ArrayList alRepList = new ArrayList();
	ArrayList alDistributor = new ArrayList();
	ArrayList alState = new ArrayList();
	ArrayList alCountry = new ArrayList();
	ArrayList alPayment = new ArrayList();
	//ArrayList alGpoList = new ArrayList();
	ArrayList alAcctType = new ArrayList();
	//ArrayList alContFl = new ArrayList();
	//ArrayList alContFlAtt = new ArrayList();
	ArrayList alCompId = new ArrayList();
	ArrayList alUploadPrice= new ArrayList();
	ArrayList alCarrier = new ArrayList();
	ArrayList alParentAccount = new ArrayList ();
	
	//MNTTASK-6074 - Account Affiliation
	//ArrayList alGprAff = new ArrayList();
	//ArrayList alHlcAff = new ArrayList();
	ArrayList alCollector = new ArrayList();
	ArrayList alTaxType= new ArrayList();
	ArrayList alCompanyCurrency= new ArrayList();
	ArrayList alInvCloseDt= new ArrayList();
	ArrayList alAcctInternalRep= new ArrayList();
	
	String strRepAccName = "";
	String strRepAccNameEn="";
	String strParentAccName = "";
	String strAccId = "";
	String strRepId = "";
	String strPartyId = "";
	String strDistId = "";
	String strShName = "";
	String strContactPerson = "";
	String strBillName = "";
	String strShipName = "";
	String strBillAdd1 = "";
	String strShipAdd1 = "";
	String strBillAdd2 = "";
	String strShipAdd2 = "";
	String strBillCity = "";
	String strShipCity = "";
	String strBillState = "";
	String strShipState = "";
	String strBillCountry = "1101";
	String strShipCountry = "1101";
	String strPrefCarrier = "";
	String strBillZipCode = "";
	String strShipZipCode = "";
	String strPhone = "";
	String strFax = "";
	String strActiveFl = "N";
	String strPayment = "4100";
	String strIncDate = "";
	java.sql.Date dtIncDate = null;
	String strComments = "";
	//String strGpoId = "";
	String strAcctType = "";
	String strAcctGroup = "";
	//String strContractFl = "";
	String strCompId = "";
	String strCollectorId = "";
	String strPricingUser = "N";
	String strSelAcctSource = "";
	String strSourceHistFl ="";
	String strShipAddFl ="";
	String strAccCatValue = "";
	String strTaxTypeId = "";
	String strDealerId = "";
	String strInvCloseDt = "";
	String strPaymentMonth ="";
	String strPaymentDays ="";
	
	//String strGprAff = "";
	//String strHlthCrAff = "";
	String strUploadPrice = "";
	String strShowButton = GmCommonClass.parseNull((String)request.getAttribute("hShowButton")) ;
	String strAccAddEditAcsFl = GmCommonClass.parseNull((String)request.getAttribute("ACC_ADD_EDIT_ACCESS"));
	String strAccRepEditAcsFl = GmCommonClass.parseNull((String)request.getAttribute("ACC_REP_EDIT_ACCESS"));
	String strAccAddrsEditAcsFl = GmCommonClass.parseNull((String)request.getAttribute("ACC_ADDRS_EDIT_ACCESS"));
	String strApplDateFmt = strGCompDateFmt;//GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));

	String strHideButton = ""; 
	strHideButton = GmCommonClass.parseNull((String)request.getAttribute("HideButton")); 
	String strCreditRating="";
	String strHistoryFlag = "";
	String strCompanyAvaTaxFl = "";
	String strCompanyCurrencyId="";
	String strDefaultCurrencyId = "";
	String strComDisable = "";
	String strDistNm="";
	String strDealerNm="";
	String strSelInternalRep = "";
	String strInternalRepFl = "";

	String strDeptId = GmCommonClass.parseNull((String)session.getAttribute("strSessDeptId"));
	String strAccessLvl = GmCommonClass.parseNull((String)session.getAttribute("strSessAccLvl"));
	String strDisable = GmCommonClass.parseNull((String)request.getAttribute("DISABLE"));
	String strReadFl = GmCommonClass.parseNull((String)request.getAttribute("READFL"));
	String strShowAccountGrp = GmCommonClass.parseNull((String) request.getAttribute("SHOWACCOUNTGRP"));
	String strAddVAl =  GmCommonClass.parseNull((String) request.getAttribute("ADD_VAL"));
	
	// Getting the dropdown values of Group
	ArrayList alAccountGrp = GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("ACCOUNT_GRP_LIST"));
	ArrayList alCategory = new ArrayList ();
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	alAcctInternalRep = GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("ALACCTINTERNALREP")); //PMT-41644
	 
	GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	String strShowTaxType = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHOW_TAX_TYPE"));
 	String strShowRepAccNmEn = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHOW_ACCT_NAME_EN"));
 	 String strPaymentTermFlag = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHOW_PAYMENTTERM_MONTH"));

	if (hmReturn != null )
	{	
		alRepList = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("REPLIST")); 
		alState = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("STATE"));
		alCountry = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("COUNTRY"));
		alCarrier = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("PREFERCARRIER"));
		alPayment = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("PAYMENT"));
		alCategory = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ACCTCATGRY"));
		//alGpoList = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("GPOLIST"));
		alAcctType = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ACCTTYPE"));
		alAcctType = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ACCTTYPE"));
		//alContFl = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALCONTFL"));	
		//alContFlAtt = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALCONTFLATT"));
		alCompId = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALCOMPID"));
		//alGprAff = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALGPRAFF"));
		//alHlcAff = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALHLCAFF"));
		alCollector = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("COLLECTORS"));
		alTaxType = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALTAXTYPE"));
		alUploadPrice= GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("PRICE"));
		alCompanyCurrency=GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALCOMPCURRENCY"));
		alInvCloseDt = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALINVCLOSEDT"));		
		
	}
	if (hmReturn != null )
	{
		hmRepAccountDetails = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("REP_ACCOUNTDETAILS"));
		hmParentAccountDetails = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("PARENT_ACCOUNTDETAILS"));
		if (hmRepAccountDetails.size()  > 0)
		{
			strRepAccName = GmCommonClass.parseNull((String)hmRepAccountDetails.get("ANAME"));
			strRepAccNameEn = GmCommonClass.parseNull((String)hmRepAccountDetails.get("ANAME_EN"));
			strAccId = GmCommonClass.parseNull((String)hmRepAccountDetails.get("ID"));
			strRepId = GmCommonClass.parseNull((String)hmRepAccountDetails.get("REPID"));
			strPartyId = GmCommonClass.parseNull((String)hmRepAccountDetails.get("PARTYID"));
			strDistId = GmCommonClass.parseNull((String)hmRepAccountDetails.get("DISTID"));
			strDistNm = GmCommonClass.parseNull((String)hmRepAccountDetails.get("DNAME"));
			strShName = GmCommonClass.parseNull((String)hmRepAccountDetails.get("SNAME"));
			dtIncDate = (java.sql.Date)hmRepAccountDetails.get("INCDATE");
			strActiveFl = GmCommonClass.parseNull((String)hmRepAccountDetails.get("AFLAG"));
			strCompId = GmCommonClass.parseNull((String)hmRepAccountDetails.get("COMPID"));
			strAcctGroup = GmCommonClass.parseNull((String) hmRepAccountDetails.get("ACCGROUP"));
			strCompanyAvaTaxFl = GmCommonClass.parseNull((String) hmRepAccountDetails.get("COMPANY_AVATAX_FL"));
			strCompanyCurrencyId = GmCommonClass.parseZero((String) hmRepAccountDetails.get("ACC_CURR_ID"));
			strDefaultCurrencyId = GmCommonClass.parseNull((String) hmRepAccountDetails.get("COMPANY_CURR_ID"));
			strDealerId = GmCommonClass.parseZero((String) hmRepAccountDetails.get("DEALERID"));
			strDealerNm = GmCommonClass.parseNull((String) hmRepAccountDetails.get("DEALERNM"));
			strInvCloseDt = GmCommonClass.parseZero((String) hmRepAccountDetails.get("INVCLOSEDT"));

			// first time to set the default company currency id
			if(alCompanyCurrency.size() == 1){
				strCompanyCurrencyId = strCompanyCurrencyId.equals("0")? strDefaultCurrencyId :strCompanyCurrencyId;
			}
		}
		if (hmParentAccountDetails.size()  > 0)
		{	
			strParentAccName = GmCommonClass.parseNull((String)hmParentAccountDetails.get("PARTYNM"));
			strContactPerson = GmCommonClass.parseNull((String)hmParentAccountDetails.get("CPERSON"));
			strBillName = GmCommonClass.parseNull((String)hmParentAccountDetails.get("BNAME"));
			strShipName = GmCommonClass.parseNull((String)hmParentAccountDetails.get("CNAME"));
			strBillAdd1 = GmCommonClass.parseNull((String)hmParentAccountDetails.get("BADD1"));
			strShipAdd1 = GmCommonClass.parseNull((String)hmParentAccountDetails.get("SADD1"));
			strBillAdd2 = GmCommonClass.parseNull((String)hmParentAccountDetails.get("BADD2"));
			strShipAdd2 = GmCommonClass.parseNull((String)hmParentAccountDetails.get("SADD2"));
			strBillCity = GmCommonClass.parseNull((String)hmParentAccountDetails.get("BCITY"));
			strShipCity = GmCommonClass.parseNull((String)hmParentAccountDetails.get("SCITY"));
			strBillState = GmCommonClass.parseNull((String)hmParentAccountDetails.get("BSTATE"));
			strShipState = GmCommonClass.parseNull((String)hmParentAccountDetails.get("SSTATE"));
			strBillCountry = GmCommonClass.parseNull((String)hmParentAccountDetails.get("BCOUNTRY"));
			strShipCountry = GmCommonClass.parseNull((String)hmParentAccountDetails.get("SCOUNTRY"));
			strPrefCarrier = GmCommonClass.parseNull((String)hmParentAccountDetails.get("CARRIER"));			
			strBillZipCode = GmCommonClass.parseNull((String)hmParentAccountDetails.get("BZIP"));
			strShipZipCode = GmCommonClass.parseNull((String)hmParentAccountDetails.get("SZIP"));
			strPhone = GmCommonClass.parseNull((String)hmParentAccountDetails.get("PHONE"));
			strFax = GmCommonClass.parseNull((String)hmParentAccountDetails.get("FAX"));
			strPayment = GmCommonClass.parseNull((String)hmParentAccountDetails.get("PTERM"));
			strComments = GmCommonClass.parseNull((String)hmParentAccountDetails.get("COMMENTS"));
			//strGpoId = GmCommonClass.parseNull((String)hmRepAccountDetails.get("GPOID"));
			strAcctType = GmCommonClass.parseNull((String)hmParentAccountDetails.get("ACCTYPE"));
			strChecked = strActiveFl.equals("Y")?"checked":"";
			//strhAction = "Edit"; // switched this Add&Edit part to Servlet
			strCreditRating = GmCommonClass.parseNull((String)hmParentAccountDetails.get("CREDITRATING"));
			strHistoryFlag = GmCommonClass.parseNull((String)hmParentAccountDetails.get("RATING_FL"));
			//strContractFl = GmCommonClass.parseNull((String)hmRepAccountDetails.get("CONTRACTFL"));
			//strGprAff = GmCommonClass.parseNull((String)hmRepAccountDetails.get("GRPAFF"));
			//strHlthCrAff = GmCommonClass.parseNull((String)hmRepAccountDetails.get("HLCAFF"));
			strCollectorId = GmCommonClass.parseNull((String)hmParentAccountDetails.get("COLLECTORID"));
			strTaxTypeId = GmCommonClass.parseNull((String)hmParentAccountDetails.get("ACCTAXTYPE"));
			//strAcctGroup = GmCommonClass.parseNull((String) hmParentAccountDetails.get("ACCGROUP"));
			strUploadPrice= GmCommonClass.parseNull((String) hmParentAccountDetails.get("PRICELVL"));
			strSelAcctSource= GmCommonClass.parseNull((String) hmParentAccountDetails.get("ACCTSOURCE"));
			strSourceHistFl= GmCommonClass.parseNull((String) hmParentAccountDetails.get("SOURCEHISTFL"));
			strShipAddFl = GmCommonClass.parseNull((String) hmParentAccountDetails.get("SHIPADDFL"));
			strAccCatValue = GmCommonClass.parseNull((String) hmParentAccountDetails.get("ACCTCATVALUE"));
			strSelInternalRep =GmCommonClass.parseNull((String)hmParentAccountDetails.get("INTERNAL_REP_ID")); 
		}
	}

	int intSize = 0;
	HashMap hcboVal = null;
	String strCSAccess = GmCommonClass.parseNull((String)request.getAttribute("CSDISABLE"));
	String strShipSetupUpdFl = GmCommonClass.parseNull((String)request.getAttribute("ACCSHIPUPDFL"));
	String strDefaultCollectorID = GmCommonClass.parseZero((String)request.getAttribute("DEFCOLLECTORID"));
	
	if(!strReadFl.equals("Y") && !strCSAccess.equals("disabled=disabled")){
		strPricingUser = "Y";
	}
	
	ArrayList alAcctSrc = GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("ALACCTSRC"));
	String strAcctSrcFl = GmCommonClass.parseNull((String) request.getAttribute("ACCOUNTSRC"));
	
	String strActShade = "oddshade";
	String strCollectShade = "evenshade";
	
	boolean bolAccess = true;
	if ((strDeptId.equals("J")))
	{
		bolAccess = false;
	}
	
	String strReloadCount =  GmCommonClass.parseNull((String)request.getAttribute("RELOADCOUNT"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Account Setup </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmAccountSetup.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/jquery.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/json2.js"></script>

<script>
var lblCompany = '<fmtAccountSetup:message key="LBL_COMPANY"/>';
var lblDistributorName='<fmtAccountSetup:message key="LBL_DISTRIBUTOR_NAME"/>';
var partyId = '<%=strPartyId%>';
var acctSrc = '<%=strAcctSrcFl%>';
var acctSrcHistFl = '<%=strSourceHistFl%>';
var rCunt = '<%=strReloadCount%>';
var strAcctGroup = '<%=strAcctGroup%>';
var len = '<%=alRepList.size()%>';
var strHideButton = '<%= strHideButton %>';
var strShipSetupUpdFl = '<%=strShipSetupUpdFl%>';
var mapShipAccess = '<%=strReadFl%>';
var strMapAccParams ='<%=strMapAccParams%>';
var strCSAccess = '<%=strCSAccess%>';
var format = '<%=strApplDateFmt%>';
var strUploadAcctPrice='<%=strUploadAcctPrice%>';
var strListPricing='<%=strAcctPricing%>';
var defaultCollectorID = '<%=strDefaultCollectorID%>';
var pricingUser = '<%=strPricingUser%>';
var curCollectorId = '<%=strCollectorId%>';
var curSourceId = '<%=strSelAcctSource%>';
var avaTaxFl = '<%=strCompanyAvaTaxFl%>';
var strCompLangId = '<%=gmDataStoreVO.getCmplangid()%>';
var strDealerId = '<%=strDealerId%>';
var paymentTermFlag = '<%=strPaymentTermFlag%>';
var accEditUpdAcsFl ='<%=strAccAddEditAcsFl%>';
var accRepEditAcsFl = '<%=strAccRepEditAcsFl%>';
var accAddrsEditAcsFl = '<%=strAccAddrsEditAcsFl%>';

var prevShipAdd1 ="<%=strShipAdd1%>" ;
var prevShipAdd2 = "<%=strShipAdd2%>";
var prevShipCity = "<%=strShipCity%>";
var prevShipState = '<%=strShipState%>';
var prevShipCountry = '<%=strShipCountry%>';
var prevShipZip = '<%=strShipZipCode%>';
var internrepfl = '<%=strInternalRepFl%>'; 
<%
			  		intSize = alRepList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alRepList.get(i);
			  			strCodeID = (String)hcboVal.get("REPID");
%>
	var arr<%=i%> ="<%=hcboVal.get("DID")%>,<%=hcboVal.get("REPID")%>,<%=hcboVal.get("NAME")%>";
<%
			  		}
%>
</script>
<style>
table.DtTable750{	
	margin: 0 0 0 0;
	width: 750px;
	border: 1px solid  #676767;
}
#Collector{
    display: table-row!important;
}
</style>
</HEAD>

<BODY leftmargin="10" topmargin="20" onLoad="fnCallOnLoad();fnReloadContainer();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmAccountServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hRepId" value="<%=strRepId%>">
<input type="hidden" name="hAccountId" value="<%=strAccId%>">

<input type="hidden" name="hMode" value="">
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="hShowButton" value="<%=strShowButton%>">
<input type="hidden" name="hLoadListPrice" value="<%=strAcctPricing %>">
<input type="hidden" name="hAccAttInputStr" value="">
<input type="hidden" name="hReloadCount">
<input type="hidden" name="hAddressVal" value="<%=strAddVAl%>">
<input type="hidden" name="hShippingAddVerified" value="<%=strShipAddFl%>">
<input type="hidden" name="hRepAccInputStr" value = "">
<input type="hidden" name="hParentAccInputStr" value = "">
<input type="hidden" name="hDealerIDStr" value = "">
<input type="hidden" name="holdAccType" value = "<%=strAcctType%>">
<input type="hidden" name="hOld_Account_Name" value ="<%=strRepAccName%>">
<input type="hidden" name="hOld_Dist_Name" value ="<%=strDistNm%>">
<input type="hidden" name="hOld_Parant_Acc_Name" value ="<%=strParentAccName%>">

	<table class="DtTable750" cellspacing="0" cellpadding="0">
	
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="ShadeRightTableCaption"><td height="22" colspan="2">
					<fmtAccountSetup:message key="LBL_REP_ACCOUNT_INFORMATION" var="varRepAccountInformation"/>
					<gmjsp:label type="BoldText"  SFLblControlName="${varRepAccountInformation}" td="false"/></td></tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr> 
					<tr>
					<td colspan="2">
					<table id="tblAcctInfo" width="750px" cellspacing="0" cellpadding="0" style="width: 100%;">
					<%
							if (strShowAccountGrp.equals("YES")) {
					%>
					<tr>
					<%}else{ %>
					<tr class="oddshade">
					<%} %>
					<fmtAccountSetup:message key="LBL_REP_ACCOUNT_NAME" var="varRepAccountName"/>
						<gmjsp:label type="MandatoryText"  SFLblControlName="${varRepAccountName}:" td="true"/>
						<td>&nbsp;<input type="text" size="50" value="<%=strRepAccName%>" name="Txt_Rep_AccNm" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" TabIndex="3" maxlength="250">
						&nbsp;&nbsp;&nbsp;<b>
						<fmtAccountSetup:message key="LBL_REP_ACCOUNT_ID" var="varRepAccountID"/>
						<gmjsp:label type="BoldText"  SFLblControlName="${varRepAccountID}:" td="false"/></b>&nbsp;<%=strAccId%>					
						</td>
					</tr>
					<% 
						if (strShowRepAccNmEn.equals("YES")) {
					%> 
					<tr class="oddshade">
					<fmtAccountSetup:message key="LBL_REP_ACC_NME_EN" var="varRepAccNameEn"/>

                    <gmjsp:label type="MandatoryText"  SFLblControlName="${varRepAccNameEn}:" td="true"/>

						<td>&nbsp;<input type="text" size="50" value="<%=strRepAccNameEn%>" name="Txt_Rep_AccNmEn" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" TabIndex="3" maxlength="250">
					</tr>
					<%
						}
 					%> 

					
					<%
							if (strShowAccountGrp.equals("YES")) {
						%>
						<tr class="oddshade">
						<fmtAccountSetup:message key="LBL_GROUP" var="varGroup"/>
							<gmjsp:label type="BoldText" SFLblControlName="${varGroup}:" td="true" />
							<td class="RightText">&nbsp;<gmjsp:dropdown
									controlName ="Cbo_AccGrp" seletedValue="<%= strAcctGroup %>"
									tabIndex="4" width="150" value="<%= alAccountGrp%>" codeId="ID"
									codeName="NAME" defaultValue="[Choose One]" />&nbsp;&nbsp;&nbsp;</td>
						</tr>
						<%
							}
						%>
					<tr class="evenshade">
					<fmtAccountSetup:message key="LBL_COMPANY" var="varCompany"/><gmjsp:label type="MandatoryText"   SFLblControlName="${varCompany}:" td="true"/>
						<td class="RightText">&nbsp;<gmjsp:dropdown controlName="Cbo_CompId"  seletedValue="<%= strCompId %>" 	
							tabIndex="4"  width="150" value="<%= alCompId%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"  /></td>							
					</tr>
					<tr class="oddshade">
					<fmtAccountSetup:message key="LBL_DISTRIBUTOR_NAME" var="varDistributorName"/>
					<gmjsp:label type="MandatoryText"  SFLblControlName="${varDistributorName}:" td="true"/>

						<td ><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="Cbo_DistId" />
					<jsp:param name="METHOD_LOAD" value="loadFieldSalesList" />
					<jsp:param name="WIDTH" value="400" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value="4"/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%=strDistNm %>" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="<%=strDistId %>" />  					
					<jsp:param name="SHOW_DATA" value="10" />
					<jsp:param name="AUTO_RELOAD" value="javascript:fnLoadRep(this,'Load');" />
							</jsp:include></td></tr>
					<tr class="evenshade">
					<fmtAccountSetup:message key="LBL_SALES_REP" var="varSalesRep"/>
						<gmjsp:label type="MandatoryText"  SFLblControlName="${varSalesRep}:" td="true"/>
						<td class="RightText">&nbsp;<select name="Cbo_RepId" id="Region" class="RightText" TabIndex="6" onFocus="changeBgColor(this,'#EEEEEE');"onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
						</select>
						</td>
					</tr>
					<tr class="oddshade">
					<fmtAccountSetup:message key="LBL_ACCOUNT_SHORT_NAME" var="varAccountShortName"/>
					<gmjsp:label type="BoldText"  SFLblControlName="${varAccountShortName}:" td="true"/>
						<td>&nbsp;<input type="text" size="30" value="<%=strShName%>" name="Txt_ShName" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" TabIndex="7" maxlength="70"></td>
					</tr>
					<tr class="evenshade">
					<fmtAccountSetup:message key="LBL_ACTIVE" var="varActive"/>
						<gmjsp:label type="BoldText"  SFLblControlName="${varActive}:" td="true"/>
						<td>&nbsp;<input type="checkbox" size="30" <%=strChecked%> name="Chk_ActiveFl" value="<%=strActiveFl%>" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" TabIndex="8">
											
						</td>
					</tr>
					<tr class="oddshade">
					<fmtAccountSetup:message key="LBL_INCEPTION_DATE" var="varInceptionDate"/>
						<gmjsp:label type="MandatoryText"  SFLblControlName="${varInceptionDate}:" td="true"/>
						<td>&nbsp;<gmjsp:calendar tabIndex="9"  textControlName="Txt_IncDate" textValue="<%=dtIncDate%>"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;						
						</td>
					</tr>
					
					<tr class="evenshade">
					<fmtAccountSetup:message key="LBL_CURRENCY" var="varCurrency"/>
						<gmjsp:label type="MandatoryText" SFLblControlName="${varCurrency}:" td="true"/>
						<td class="RightText">&nbsp;<gmjsp:dropdown controlName="Cbo_Currency"  seletedValue="<%=strCompanyCurrencyId  %>" 	
							tabIndex="10"  width="150" value="<%=alCompanyCurrency%>" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]"  /></td>							
					</tr>
					</table>
					</td>
					</tr>
							
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="ShadeRightTableCaption"><td height="22" colspan="2">
					<fmtAccountSetup:message key="LBL_PARENT_ACCOUNT_INFORMATION" var="varParentAccountInformation"/>
					<gmjsp:label type="BoldText"  SFLblControlName="${varParentAccountInformation}" td="false"/></td></tr>					
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
                     <tr>
					<td colspan="2">
					<table id="tblParAcctInfo" width="750px"  cellspacing="0" cellpadding="0" style="width:100%;">
					<tr class="oddshade">
					<fmtAccountSetup:message key="LBL_ACCOUNT_TYPE" var="varAccountType"/>
						<gmjsp:label type="MandatoryText"  SFLblControlName="${varAccountType}:" width="160" td="true"/>
						<td class="RightText" colspan="2">&nbsp;<gmjsp:dropdown controlName="Cbo_AccType"  seletedValue="<%= strAcctType %>" 	
 							tabIndex="10"  value="<%= alAcctType%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"  onChange="fnChangeAcctType();" /></td>							
					</tr>
					
					
<!-- 					<tr id="showDealerNameLn"><td class="LLine" height="1" colspan="2"></td></tr>  -->
					<tr id="showDealerName" class="evenshade">
					<fmtAccountSetup:message key="LBL_DEALER_NAME" var="varDealerNam"/>
						<gmjsp:label type="MandatoryText"  SFLblControlName="${varDealerNam}:" td="true"/>
                             	<td class="RightText"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="Cbo_Dealer_Id" />
					<jsp:param name="METHOD_LOAD" value="loadPartyNameList&party_type=26230725" />
					<jsp:param name="WIDTH" value="450" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value="9"/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%=strDealerNm %>" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="<%=strDealerId %>" />
					<jsp:param name="SHOW_DATA" value="10" />
					<jsp:param name="AUTO_RELOAD" value="fnDealerInfo(this);" />
							</jsp:include></td>
                             <td><span  class="RightText" id='imgAccDisp' style="display:none;height:1">
                              <img style='cursor:hand' src='<%=strImagePath%>/a.gif'  title='View Dealer Active Account Information ' onclick="javascript:fnAccountDealerMap()"/>
                              </span></td>
                             
					</tr>
					<tr class="oddshade" height="38">
					<fmtAccountSetup:message key="LBL_PARENT_ACCOUNT" var="varParentAccount"/>
						<gmjsp:label type="BoldText"   SFLblControlName="${varParentAccount}:" td="true"/>
							<td  class="RightText"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="Cbo_Parent_Account_Id" />
					<jsp:param name="METHOD_LOAD" value="loadPartyNameList&party_type=4000877" />
					<jsp:param name="WIDTH" value="450" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value="10"/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%=strParentAccName %>" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="<%=strPartyId %>" />
					<jsp:param name="SHOW_DATA" value="100" />
					<jsp:param name="AUTO_RELOAD" value="fnLoadParentAcctInfo(this);" />				
							</jsp:include> </td>
							<td>
							<fmtAccountSetup:message key="LBL_REP_ACCOUNTS" var="varRepAccounts"/>
						<gmjsp:button buttonType="button" value="${varRepAccounts}" style="width:7.8em" tabindex="11" onClick="fnShowRepAcc();"></gmjsp:button>	
						
							</td>
						
							
					</tr>
					<tr class="evenshade">
					<fmtAccountSetup:message key="LBL_ACCOUNT_NAME" var="varAccountName"/>
						<gmjsp:label type="MandatoryText"  SFLblControlName="${varAccountName}:" td="true"/>
						<td colspan="2">&nbsp;<input type="text" size="50" value="<%=strParentAccName%>" name="Txt_Parent_AccNm" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" TabIndex="12" maxlength="100">
						</td>
						</tr>
						
					

					<tr class="oddshade">
					<fmtAccountSetup:message key="LBL_CONTACT_PERSON" var="varContactPerson"/>
						<gmjsp:label type="MandatoryText"  SFLblControlName="${varContactPerson}:" td="true"/>
						<td colspan="2">&nbsp;<input type="text" size="30" value="<%=strContactPerson%>" name="Txt_ContPer" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" TabIndex="14" maxlength="100"></td>
					</tr>

					<tr class="evenshade">
					<fmtAccountSetup:message key="LBL_PHONE" var="varPhone"/>
						<gmjsp:label type="BoldText"  SFLblControlName="${varPhone}:" td="true"/>
						<td colspan="2">&nbsp;<input type="text" size="20" value="<%=strPhone%>" name="Txt_Phn" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" TabIndex="15" maxlength="20"></td>
					</tr>

					<tr class="oddshade">
					<fmtAccountSetup:message key="LBL_FAX" var="varFAX"/>
						<gmjsp:label type="BoldText"  SFLblControlName="${varFAX}:" td="true"/>
						<td colspan="2">&nbsp;<input type="text" size="20" value="<%=strFax%>" name="Txt_Fax" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" TabIndex="16" maxlength="20"></td>
					</tr>
					</table>
					</td>
					</tr>
					<tr>
							<td class="LLine" height="1" colspan="2"></td>
						</tr>
					<tr class="ShadeRightTableCaption">
						<td Height="24" colspan="3">&nbsp;<fmtAccountSetup:message key="LBL_ACCOUNT_INFORMATION"/>:</td>	
						</tr>			
						<tr>
						<td colspan="3" height="50"  >&nbsp;<textarea name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
							onBlur="changeBgColor(this,'#ffffff');"  rows=3 cols=100 tabindex="17"><%=strComments%></textarea></td>
						</tr>					
					<tr><td class="Line" height="1" colspan="2"></td></tr>
					
					<!-- Account Affiliation part moved to PricingParameter tab -->
					<!-- MNTTASK:6074 - Account Affiliation Start code -->
					
                   <%--  <tr><td colspan="3">
					<table cellspacing="0" cellpadding="0" width="100%" border="0">
						<tr class="ShadeRightTableCaptionBlue">
								<gmjsp:label type="BoldText" SFLblControlName="Account Affiliation" td="true" align="left" />
								<td height="23"></td><td></td><td></td>
						</tr>
					<tr><td class="Line" height="1" colspan="4"></td></tr>
					
					<tr class="oddshade">
						<gmjsp:label type="BoldText"  SFLblControlName="Group Price Book:" td="true"/>
						<td>&nbsp;<gmjsp:dropdown controlName="Cbo_GpoId"  seletedValue="<%= strGpoId %>" 	
							width="150" value="<%= alGpoList%>" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" tabIndex="14" />	</td>
							<gmjsp:label type="BoldText"  SFLblControlName="IDN:" td="true"/>
						<td>&nbsp;<gmjsp:dropdown controlName="Cbo_GrpAff"  seletedValue="<%= strGprAff %>" 	
							width="150" value="<%= alGprAff%>" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" tabIndex="15" />	</td>
					</tr>
					<tr><td class="LLine" height="1" colspan="4"></td></tr>
					<tr class="evenshade">
					<% 	hmVal = new HashMap();	 
							hmVal = (HashMap) alContFlAtt.get(0); 
							strCodeNm = GmCommonClass.parseNull((String)hmVal.get("CODENM"));
						%>
						<td height="24" class="RightTableCaption" align="right"	width="18%"><%=strCodeNm%>:</td>
						<td>&nbsp;<gmjsp:dropdown controlName="contractFl"  seletedValue="<%= strContractFl %>" 	
							width="150" value="<%= alContFl%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" tabIndex="16" />	</td>
							<gmjsp:label type="BoldText"  SFLblControlName="GPO:" td="true"/>
						<td>&nbsp;<gmjsp:dropdown controlName="Cbo_HlcAff"  seletedValue="<%= strHlthCrAff %>" 	
							width="150" value="<%= alHlcAff%>" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]"  tabIndex="17"/>	</td>
					</tr>
					<!-- Account Affiliation End code -->
					</table></td></tr> 
					 --%>
					
					<tr><td class="Line" colspan="2"></td></tr>
					<tr>
						<td colspan="2">
							<table id="tblAddressInfo" cellspacing="0" cellpadding="0" width="100%" border="0">
								<tr class="ShadeRightTableCaptionBlue">
								<fmtAccountSetup:message key="LBL_BILL_TO_ADDRESS" var="varBillToAddress"/>
									<gmjsp:label type="BoldText" SFLblControlName="${varBillToAddress}" td="true" align="left" />
									<td height="23"></td>
									<td rowspan="18" class="Line" width="1"><img src="<%=strImagePath%>/blank.gif" width="1"></td>
									<fmtAccountSetup:message key="LBL_SHIP_TO_ADDRESS" var="varShipToAddress"/>
									<gmjsp:label type="BoldText" SFLblControlName="${varShipToAddress}" td="true" align="left" />
									<td align="right">
									<% if(strCompanyAvaTaxFl.equals("Y")){ %>
									<table><tr><td><div id="showAddValidate" align="center" style="display: none;"><img height="24" width="25" id="validImg" src="<%=strImagePath%>/success.gif"></img></div></td>
									<td><div id="showAddValidateTxt" style="display: none;"><fmtAccountSetup:message key="LBL_ADDRESS_VALIDATED"/></div></td>
									<td><div id="showAddInValidate" align="center" style="display: none;"> <img height="24" width="25" id="validImg" src="<%=strImagePath%>/delete.gif"></img></div></td> 
									<td><div id="showAddInValidateTxt" style="display: none;"><fmtAccountSetup:message key="LBL_ADDRESS_NOT_VALIDATED"/></div></td>
									<fmtAccountSetup:message key="IMG_ALT_HELP" var="varHelp"/>
									<td><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strShipToAddWikiTitle%>');" />&nbsp; </td>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
									</tr></table>
									<%} %>
									</td> 
								</tr>
								<tr><td class="Line" height="1" colspan="5"></td></tr>
								<tr class="evenshade">
								<fmtAccountSetup:message key="LBL_NAME" var="varName"/>
									<gmjsp:label type="MandatoryText"  SFLblControlName="${varName}:" td="true"/>
									<td>&nbsp;<input type="text" size="30" value="<%=strBillName%>" name="Txt_BillName" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex=18 maxlength="100"></td>
									<fmtAccountSetup:message key="LBL_NAME" var="varName"/>
									<gmjsp:label type="MandatoryText"  SFLblControlName="${varName}:" td="true"/>
									<td><input type="text" size="30" value="<%=strShipName%>" name="Txt_ShipName" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex=25 maxlength="100"></td>
								</tr>
								<tr><td class="LLine" height="1" colspan="5"></td></tr>
								<tr class="oddshade">
								<fmtAccountSetup:message key="LBL_ADDRESS_1" var="varAddress1"/>
									<gmjsp:label type="MandatoryText" width="140" SFLblControlName="${varAddress1}:" td="true"/>
									<td><input type="text" size="30" value="<%=strBillAdd1%>" name="Txt_BillAdd1" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex=19 maxlength="100"></td>
									<fmtAccountSetup:message key="LBL_ADDRESS_1" var="varAddress1"/>
									<gmjsp:label type="MandatoryText" width="140" SFLblControlName="${varAddress1}:" td="true"/>
									<td><input type="text" size="30" value="<%=strShipAdd1%>" name="Txt_ShipAdd1" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex=26 maxlength="100"></td>
								</tr>
								<tr><td class="LLine" height="1" colspan="5"></td></tr>
								<tr class="evenshade">
								<fmtAccountSetup:message key="LBL_ADDRESS_2" var="varAddress2"/>
									<gmjsp:label type="BoldText"  SFLblControlName="${varAddress2}:" td="true"/>
									<td><input type="text" size="30" value="<%=strBillAdd2%>" name="Txt_BillAdd2" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex=20 maxlength="100"></td>
									<fmtAccountSetup:message key="LBL_ADDRESS_2" var="varAddress2"/>
									<gmjsp:label type="BoldText"  SFLblControlName="${varAddress2}:" td="true"/>
									<td><input type="text" size="30" value="<%=strShipAdd2%>" name="Txt_ShipAdd2" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex=27 maxlength="100"></td>
								</tr>
								<tr><td class="LLine" height="1" colspan="5"></td></tr>
								<tr class="oddshade">
								<fmtAccountSetup:message key="LBL_CITY" var="varCity"/>
									<gmjsp:label type="MandatoryText"  SFLblControlName="${varCity}:" td="true"/>
									<td><input type="text" size="30" value="<%=strBillCity%>" name="Txt_BillCity" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex=21 maxlength="100"></td>
									<fmtAccountSetup:message key="LBL_CITY" var="varCity"/>
									<gmjsp:label type="MandatoryText"  SFLblControlName="${varCity}:" td="true"/>
									<td><input type="text" size="30" value="<%=strShipCity%>" name="Txt_ShipCity" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex=28 maxlength="100"></td>
								</tr>
								<tr><td class="LLine" height="1" colspan="5"></td></tr>
								<tr class="evenshade">
									<fmtAccountSetup:message key="LBL_STATE" var="varState"/>
									<gmjsp:label type="MandatoryText"  SFLblControlName="${varState}:" td="true"/>
									<td><gmjsp:dropdown controlName="Cbo_BillState"  seletedValue="<%= strBillState %>" 	
							tabIndex="22"  width="150" value="<%= alState%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" /></td>
									<fmtAccountSetup:message key="LBL_STATE" var="varState"/>
									<gmjsp:label type="MandatoryText"  SFLblControlName="${varState}:" td="true"/>
									<td><gmjsp:dropdown controlName="Cbo_ShipState"  seletedValue="<%= strShipState %>" 	
							tabIndex="29"  width="150" value="<%= alState%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" /></td>
								</tr>
								<tr><td class="LLine" height="1" colspan="5"></td></tr>
								<tr class="oddshade">
									<fmtAccountSetup:message key="LBL_COUNTRY" var="varCountry"/>
									<gmjsp:label type="MandatoryText"  SFLblControlName="${varCountry}:" td="true"/>
									<td><gmjsp:dropdown controlName="Cbo_BillCountry"  seletedValue="<%= strBillCountry %>" 	
							tabIndex="23"  width="150" value="<%= alCountry%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" /></td>
									<fmtAccountSetup:message key="LBL_COUNTRY" var="varCountry"/>
									<gmjsp:label type="MandatoryText"  SFLblControlName="${varCountry}:" td="true"/>
									<td><gmjsp:dropdown controlName="Cbo_ShipCountry"  seletedValue="<%= strShipCountry %>" 	
							tabIndex="30"  width="150" value="<%= alCountry%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" /></td>
								</tr>								
								<tr><td class="LLine" height="1" colspan="5"></td></tr>
								<tr class="evenshade">
									<fmtAccountSetup:message key="LBL_ZIP_POSTALCODE" var="varZipPostalCode"/>
									<gmjsp:label type="MandatoryText"  SFLblControlName="${varZipPostalCode}:" td="true"/>
									<td><input type="text" size="10" value="<%=strBillZipCode%>" name="Txt_BillZipCode" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex=24 maxlength="10"></td>
									<gmjsp:label type="MandatoryText"  SFLblControlName="${varZipPostalCode}:" td="true"/>
									<td><input type="text" size="10" value="<%=strShipZipCode%>" name="Txt_ShipZipCode" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex=31 maxlength="10">&nbsp;
									<% if(strCompanyAvaTaxFl.equals("Y")){ %>
									<fmtAccountSetup:message key="LBL_VALIDATE" var="varValidate"/>
									<gmjsp:button buttonType="button" value="${varValidate}" style="width:5 em" tabindex="32" onClick="fnValidateAdd();"></gmjsp:button>&nbsp;&nbsp;
									<%} %>
									</td>
								</tr>
								<tr><td class="LLine" height="1" colspan="5"></td></tr>
								<tr class="oddshade">
									<gmjsp:label type="BoldText" SFLblControlName="" td="true"/>
									<td>&nbsp;</td>
									<fmtAccountSetup:message key="LBL_PREFERRED_CARRIER" var="varPreferredCarrier"/>
									<gmjsp:label type="BoldText" SFLblControlName="${varPreferredCarrier}:" td="true"/>
									<td><gmjsp:dropdown controlName="Cbo_PrefCarrier" disabled="disabled" seletedValue="<%= strPrefCarrier %>" 	
										tabIndex="33"  width="150" value="<%= alCarrier%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />				
									</td>
							</table>
						</td>
					</tr>
					<tr><td class="Line" colspan="2"></td></tr>
					  <tr>
					<td colspan="2">
					<table id="tblAcctInfor" width="750px cellspacing="0" cellpadding="0" style="width: 100%;">
					<tr class="evenshade">
							<fmtAccountSetup:message key="LBL_PAYMENT_TERMS" var="varPaymentTerms"/>
							<gmjsp:label type="MandatoryText"  SFLblControlName="${varPaymentTerms}:" td="true" width="150"/>
							<td width="200">
							&nbsp;<gmjsp:dropdown controlName="Cbo_Payment"  seletedValue="<%= strPayment %>" 	
							tabIndex="34"  width="150" value="<%=alPayment%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />							
							</td>
								<!-- PMT-41644 -->
							<% if(alAcctInternalRep != null && !alAcctInternalRep.isEmpty()){ %> 
							
                                <fmtAccountSetup:message key="LBL_INTERNAL_REP" var="varInternalRep"/>
                                <gmjsp:label type="MandatoryText" SFLblControlName="${varInternalRep}:" td="true"/>
                                <td>&nbsp;<gmjsp:dropdown controlName="Cbo_InternalRep" seletedValue="<%=strSelInternalRep%>" 
                                 tabIndex="37" width="150" value="<%=alAcctInternalRep%>" codeId ="CODEID" codeName ="CODENM" defaultValue= "[Choose One]" />
							   </td>
							   <%}else{ %> <td colspan="2">&nbsp;</td><%} %>
							
					</tr>

					<tr class="oddshade"> 
							<fmtAccountSetup:message key="LBL_CREDIT_RATING" var="varCreditRating"/>
							<gmjsp:label type="BoldText"  SFLblControlName="${varCreditRating}:" td="true"/>
							<td colspan="3">
							    &nbsp;<input type="text" size="20" value="<%=strCreditRating%>" name="Txt_CreditRating" maxlength="20" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" TabIndex="35" <%if(bolAccess){%>disabled="disabled"<%}%> />&nbsp;
<%
                                if(strHistoryFlag.equals("Y")){
%>
                                <img id="imgEdit" style="cursor:hand" onclick="javascript:fnLoadHistory('53029','<%=strAccId%>');" title="Click to open Credit Rating Details" 
                                src="<%=strImagePath%>/icon_History.gif" align="bottom"  height=15 width=18 />
<%
                                }
%>
                                &nbsp;
					        </td>
			
					</tr>

					<% if(!strCompanyId.equals("1000") && !strCompanyId.equals("1001")){ %>
					<tr class="evenshade">
						<fmtAccountSetup:message key="LBL_CATEGORY" var="varCategory"/>
						<gmjsp:label type="BoldText" SFLblControlName="${varCategory}:" td="true"/>
						<td colspan="3">&nbsp;<gmjsp:dropdown controlName="Cbo_Category"  seletedValue="<%= strAccCatValue %>" 	
							tabIndex="36"  value="<%= alCategory%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />					
						</td>
						 
					</tr>
					<%} %>
					
				<% if(strAcctSrcFl.equals("Y")){ %>

					<tr class="oddshade">
						<fmtAccountSetup:message key="LBL_ACCOUNT_SOURCE" var="varAccountSource"/>
						<gmjsp:label type="BoldText" SFLblControlName="${varAccountSource}:" td="true"/>
						<td colspan="3">&nbsp;<gmjsp:dropdown controlName="Cbo_Source"  seletedValue="<%= strSelAcctSource %>" 	
							tabIndex="37"  width="150" value="<%= alAcctSrc%>" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" />					
							<%if(strSourceHistFl.equals("Y")){ %>
			                    &nbsp;
			                    <fmtAccountSetup:message key="IMG_CLICK_TO_OPEN_ACCOUNT_SOURCE_HISTORY" var="varClicktoopenAccountSourceHistory"/>
			               <img id="imgEdit" style="cursor:hand" onclick="javascript:fnLoadHistory('53030','<%=strAccId%>');" 
			                    title="${varClicktoopenAccountSourceHistory}" src="<%=strImagePath%>/icon_History.gif" align="bottom"  height=15 width=18 />
		                    <%} %>
						</td>
					</tr>
					<%} if(!strCSAccess.equals("disabled=disabled")){ %> <!-- A/R , Pricing User has Access -->

					<tr class="oddshade" id="Collector" >
							<fmtAccountSetup:message key="LBL_COLLECTOR_ID" var="varCollectorID"/>
							<gmjsp:label type="BoldText" SFLblControlName="${varCollectorID}:" td="true" />
							<td colspan="3"><gmjsp:dropdown controlName="Cbo_CollectorId"  seletedValue="<%=strCollectorId%>" 	
							tabIndex="38"  value="<%= alCollector%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" /></td>
					
					</tr>
					<%}else{ %>
							<input type="hidden" name="Cbo_CollectorId" value="<%=strCollectorId%>" >
					<%}%>

					<% if (strShowTaxType.equalsIgnoreCase("YES")) {
					%>
					<tr class="oddshade">
						<td class="RightTableCaption" align="right" height="24">&nbsp;<fmtAccountSetup:message key="LBL_TAX_TYPE"/>:&nbsp;
						</td>
					<td colspan="3"><gmjsp:dropdown controlName="Cbo_TaxType"
									seletedValue="<%=strTaxTypeId%>" 
									value="<%=alTaxType%>" codeId="CODEID" codeName="CODENM"
									defaultValue="[Choose One]" /><br></td>
					</tr>
					<%} %>	
				
					</table>
					</td>
					</tr>					
					<tr><td class="Line" height="1" colspan="5"></td></tr>
		<tr>
			<td colspan="2"> 
				<jsp:include page="/common/GmIncludeLog.jsp" >
					<jsp:param name="LogType" value="" />
					<jsp:param name="LogMode" value="Edit" />
					<jsp:param name="Mandatory" value="yes" />
					<jsp:param name="TabIndex" value="39" />
				</jsp:include>
			</td>
		</tr>
		<% if(strCSAccess.equals("disabled=disabled")){
			strCSAccess = "true";
		}
		%>
		 <% if(strDisable.equals("disabled=disabled")){
			 strDisable = "true";
		}%>
		<% if(strDisable.equals("disabled=disabled")||strCSAccess.equals("disabled=disabled")){
		  strComDisable = "true";
		}%>
	    <tr>
	    	<td colspan="2" height="40" align="center">
	    		<div id="divHideButton">
	    		<fmtAccountSetup:message key="LBL_SUBMIT" var="varSubmit"/>
				<gmjsp:button style="width: 5em"  value="${varSubmit}" name="Btn_Submit" gmClass="button" onClick="fnSubmit();" disabled="<%=strComDisable%>" buttonType="Save" tabindex="40" />&nbsp;
				<fmtAccountSetup:message key="LBL_RESET" var="varReset"/>
				<gmjsp:button style="width: 5em"  value="${varReset}" buttonType="Save" gmClass="button" onClick="fnReset();" onMouseDown="fnResetClicked();" disabled="<%=strCSAccess%>" tabindex="41" />
				<%if (strShipSetupUpdFl.equals("Y")){ %>
				<fmtAccountSetup:message key="LBL_SHIPPING_SETUP" var="varShippingSetup"/>
				<gmjsp:button style="width: 9em"  value="${varShippingSetup}" gmClass="button" name="Btn_ShipSetup" onClick="fnShipSetup();" buttonType="Save" tabindex="42" />
				<%} %>
				<%-- <%if(strReadFl.equals("Y") && strhAction.equals("Edit")){ %>
				<gmjsp:button  value="Account Parameter Setup" name="MapShpParams" controlId="MapShpParams" gmClass="button" onClick="fnShipParams();" disabled="<%=strCSAccess%>" buttonType="Save" />
				<%} %> --%>
				<%if(strMapAccParams.equals("Y") && !strAccId.equals("") && strhAction.equals("Edit")){ %>
				<fmtAccountSetup:message key="LBL_MAP_ACCOUNT_PARAMETERS" var="varMapAccountParameters"/>
				<gmjsp:button  value="${varMapAccountParameters}" name="MapAccParams" controlId="MapAccParams" gmClass="button" onClick="fnAccParams();" buttonType="Save" tabindex="43" />
				<%} %>
				<%if(strhAction.equals("Edit")){%>
				<fmtAccountSetup:message key="LBL_ASSOC_REP_MAPPING" var="varAssocRepMapping"/>
				<gmjsp:button  value="${varAssocRepMapping}" name="AssocRepMapping" controlId="AssocRepMapping" gmClass="button" onClick="fnAssocRepMapping();" buttonType="Save" disabled="<%=strCSAccess%>" tabindex="44"/>
				<fmtAccountSetup:message key="LBL_CREDIT_HOLD" var="varCreditHold"/>
				<gmjsp:button style="width: 9em"  value="${varCreditHold}" gmClass="button" name="Btn_ShipSetup" onClick="fnAccountCredit();" buttonType="Save" tabindex="45"/>
				<%} %>
				</div>
			</td>
			
		</tr>
	</table>		
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
<script>
parent.document.getElementById("Txt_AccId").disabled = false;
if(accEditUpdAcsFl == 'Y'){

	parent.document.getElementById("Btn_AddNew").disabled = false; 
}

</script>
 