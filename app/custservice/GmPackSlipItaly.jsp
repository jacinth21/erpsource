
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
	/**********************************************************************************
	 * File		 		: GmPackSlipItaly.jsp
	 * Desc		 		: This screen is used for the Packing Slip
	 * Version	 		: 1.0
	 * author			: Gopinathan
	 ************************************************************************************/
%>
<!-- \custservice\GmPackSlipItaly.jsp-->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<%
	String strAction = null;
	String strSource = "";
	String strApplnDateFmt = strGCompDateFmt;

	strAction = GmCommonClass.parseNull((String) request
			.getAttribute("hAction"));
	strAction = (strAction.trim() == null) ? "PrintPack" : strAction;
	strAction = (strAction.equals("")) ? "PrintPack" : strAction;
	strSource = GmCommonClass.parseNull((String) request
			.getAttribute("SOURCE"));
	String strGUID = GmCommonClass.parseNull(request
			.getParameter("hGUId"));

	HashMap hmReturn = new HashMap();
	HashMap hmAtbData = new HashMap();
	HashMap hmCompAtbData = new HashMap();
	hmReturn = (HashMap) request.getAttribute("hmReturn");
	hmAtbData = GmCommonClass.parseNullHashMap((HashMap) request
			.getAttribute("ORDER_ATB"));
	hmCompAtbData = GmCommonClass.parseNullHashMap((HashMap) request
			.getAttribute("COMP_ATB"));

	HashMap hmOrderDetails = new HashMap();
	HashMap hmShipDetails = new HashMap();

	ArrayList alCartDetails = new ArrayList();
	ArrayList alPopulatedCartDtls = new ArrayList();
	ArrayList alBackOrderDetails = new ArrayList();

	int intRowCnt = 0;
	int intPageNo =0;
	int intPageRecCnt =0;
	int intPageCnt =0; 
	
	String strShade = "";
	String strProjectType = "";

	String strAccNm = "";
	String strDistRepNm = "";
	String strBillAdd = "";
	java.sql.Date dtOrdDate = null;
	String strShipAdd = "";
	String strPO = "";
	String strOrdId = "";
    String strAccountID = "";
	String strPartNum = "";
	String strDesc = "";
	String strPrice = "";
	String strQty = "";
	String strItemOrdId = "";
	String strPartNums = "";
	String strControlNum = "";
	String strExpireDate = "";
	String strShipDate = "";
	String strShipCarr = "";
	String strShipMode = "";
	String strTrack = "";
	String strOrderType = "";
	String strParentId = "";
	String strItemOrdType = "";
	String strCompId = "";
	String strCompanyName = "";
	String strCompanyShortName = "";
	String strCompanyAddress = "";
	String strCompanyPhone = "";
	String strGmCompanyVat = "";
	String strGmCompanyCst = "";
	String strGmCompanyPAN = "";
	String strGmLocalLic = "";
	String strGmExportLic = "";
	String strExportDruglic = "";
	String strLocalDruglic = "";
	String strSurgeonName = "";
	String strCompLogo = "";
	String strDivisionId = "";
	String strShowTotalQty = "";
	String strShowPhoneNo = "";
	String strShowGMNAAddress = "";
	String strPlantId = "";
	String strPlantPackSlip ="";
	String strComment = "";
	String strPageRecCnt = "";
	String strOrdredDate = "";
	String strShippingDate = "";
	String strShippingTime = "";
	
	String strStickCompanyLocale = "";
    String strStrickerCompId = "";
    String strHeadAddrSticker = "";
    strPlantId = GmCommonClass.parseNull(gmDataStoreVO.getPlantid());
    strStrickerCompId= GmCommonClass.parseNull(gmDataStoreVO.getCmpid()); 
    
	String strCompanyLocale = GmCommonClass
			.getCompanyLocale(gmDataStoreVO.getCmpid());

	GmResourceBundleBean gmResourceBundleBean = GmCommonClass
			.getResourceBundleBean("properties.Paperwork",
					strCompanyLocale);
	GmResourceBundleBean gmCompResourceBundleBean = GmCommonClass
			.getResourceBundleBean("properties.Company",
					strCompanyLocale);
	strShowPhoneNo = GmCommonClass
			.parseNull((String) gmResourceBundleBean
					.getProperty("PACK_SLIP.SHOW_PHONE_NO"));
	strShowGMNAAddress = GmCommonClass
			.parseNull((String) gmResourceBundleBean
					.getProperty("PACK_SLIP.SHOW_GMNA_ADDRESS"));
		String strHeadAddress = "";
		strPageRecCnt = GmCommonClass.parseZero((String) request
				.getAttribute("RECORDCNT"));
		intPageRecCnt = Integer.parseInt(strPageRecCnt);
	boolean blBackFl = false;
	alPopulatedCartDtls =  GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("alCartDetails"));
	if (hmReturn != null) {
		hmOrderDetails = (HashMap) hmReturn.get("ORDERDETAILS");
		//alCartDetails = (ArrayList) hmReturn.get("CARTDETAILS");
		if(alPopulatedCartDtls.isEmpty() && alPopulatedCartDtls != null)
		{
		  alPopulatedCartDtls = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("CARTDETAILS"));
		}
		alBackOrderDetails = (ArrayList) hmReturn.get("BACKORDER");
		hmShipDetails = (HashMap) hmReturn.get("SHIPDETAILS");
		

		strPO = GmCommonClass.parseNull((String) hmOrderDetails
				.get("PO"));
		strPO = strPO.length() > 20 ? strPO.substring(0, 20) : strPO; //Should show only 20 characters of customer Po
		strOrdId = GmCommonClass.parseNull((String) hmOrderDetails
				.get("ID"));
		strShipAdd = GmCommonClass.parseNull((String) hmOrderDetails
				.get("SHIPADD"));
		strBillAdd = GmCommonClass.parseNull((String) hmOrderDetails
				.get("BILLADD"));
		strAccountID = GmCommonClass.parseNull((String) hmOrderDetails // PMT-42950 - Include Customer # in shipment and Invoice paperwork for Italy
				.get("ACCID"));   
		strAccNm = GmCommonClass.parseNull((String) hmOrderDetails
				.get("ANAME"));
		strDistRepNm = GmCommonClass.parseNull((String) hmOrderDetails
				.get("REPNM"));
		//dtOrdDate = (java.sql.Date)hmOrderDetails.get("ODT");
		//strOrdredDate = GmCommonClass.parseNull((String)hmOrderDetails.get("ODT"));
		strOrdredDate = GmCommonClass.getStringFromDate(
				(java.util.Date) hmOrderDetails.get("ODT"),
				strApplnDateFmt);
		strShippingDate = GmCommonClass.getStringFromDate(
				(java.util.Date) hmOrderDetails.get("SHIPPING_DT"),
				strApplnDateFmt);
		strShippingTime = GmCommonClass
				.parseNull((String) hmOrderDetails.get("SHIPTIME"));
		strOrderType = GmCommonClass.parseNull((String) hmOrderDetails
				.get("ORDERTYPE"));
		strParentId = GmCommonClass.parseNull((String) hmOrderDetails
				.get("PARENTORDID"));
		strShipCarr = GmCommonClass.parseNull((String) hmOrderDetails
				.get("CARRIER"));
		strShipMode = GmCommonClass.parseNull((String) hmOrderDetails
				.get("SHIPMODE"));
		strTrack = GmCommonClass.parseNull((String) hmOrderDetails
				.get("TRACKNO"));
		strComment = GmCommonClass.parseNull((String) hmOrderDetails
				.get("COMMENTS"));

		strGmCompanyVat = GmCommonClass
				.parseNull((String) hmCompAtbData.get("GMCOMPANYVAT"));
		strGmCompanyCst = GmCommonClass
				.parseNull((String) hmCompAtbData.get("GMCOMPANYCST"));
		strGmCompanyPAN = GmCommonClass
				.parseNull((String) hmCompAtbData.get("GMCOMPANYPAN"));
		strGmLocalLic = GmCommonClass.parseNull((String) hmCompAtbData
				.get("GMLOCALLIC"));
		strGmExportLic = GmCommonClass.parseNull((String) hmCompAtbData
				.get("GMEXPORTLIC"));
		strExportDruglic = GmCommonClass.parseNull((String) hmAtbData
				.get("EXPORTDRUGLIC"));
		strLocalDruglic = GmCommonClass.parseNull((String) hmAtbData
				.get("LOCALDRUGLIC"));
		strSurgeonName = GmCommonClass.parseNull((String) hmAtbData
				.get("SURGEON"));

		strCompId = GmCommonClass.parseNull((String) hmOrderDetails
				.get("COMPID"));
		strDivisionId = GmCommonClass.parseNull((String) hmOrderDetails
				.get("DIVISION_ID"));
		strDivisionId = strDivisionId.equals("")
				? "2000"
				: strDivisionId;
		strPlantPackSlip = GmCommonClass.parseNull((String) request
				.getAttribute("PLANTCOMP"));
		HashMap hmCompanyAdd = new HashMap();
		hmCompanyAdd = GmCommonClass.fetchCompanyAddress(
				gmDataStoreVO.getCmpid(), strDivisionId);
		if(!strPlantPackSlip.equals("")){
		  hmCompanyAdd = GmCommonClass.fetchCompanyAddress(strPlantPackSlip, strDivisionId);
		}
		strCompLogo = GmCommonClass.parseNull((String) hmCompanyAdd
				.get("LOGO"));
		strCompanyName = GmCommonClass.parseNull((String) hmCompanyAdd
				.get("COMPNAME"));
		strCompanyShortName = GmCommonClass
				.parseNull((String) hmCompanyAdd.get("COMP_SHORT_NAME"));
		strCompanyAddress = GmCommonClass
				.parseNull((String) hmCompanyAdd.get("COMPADDRESS"));
		

		strCompanyAddress = "&nbsp;"
				+ strCompanyAddress.replaceAll("/", "\n<br>&nbsp;");
		strCompanyPhone = !strCompanyPhone.equals("")
				? "<br>&nbsp;&nbsp;" + strCompanyPhone
				: "";
		//***PC-5344****		
		if(strStrickerCompId.equals("1020") && strPlantId.equals("3003"))
		{
			
			strStickCompanyLocale = GmCommonClass.getCompanyLocale(strStrickerCompId);
			GmResourceBundleBean gmStkCompResourceBundleBean =
			        GmCommonClass.getResourceBundleBean("properties.Company", strStickCompanyLocale);
			
			strHeadAddrSticker = GmCommonClass.parseNull((String)gmStkCompResourceBundleBean.getProperty("COMPANY_ADDRESS_STICKER")+"&nbsp;&nbsp;");
			}
		
    	if(!strShipAdd.equals("") && strShipAdd.substring(0,6).equals("&nbsp;")){
			strShipAdd= strShipAdd.replaceFirst("&nbsp;","");
		}
    	
		 strHeadAddress = "<br>&nbsp;&nbsp;<b>" +strHeadAddrSticker+ strCompanyName
				+ "</b><BR>&nbsp;" + strCompanyAddress
				+ strCompanyPhone; 
				
		
	}

	if (hmShipDetails != null) {
		strShipDate = GmCommonClass.getStringFromDate(
				(java.util.Date) hmShipDetails.get("SDT"),
				strApplnDateFmt);
		//	strShipCarr = (String)hmShipDetails.get("SCARNM");
		//	strShipMode = (String)hmShipDetails.get("SMODENM");
		//	strTrack = (String)hmShipDetails.get("STRK");
	}

	// Combo box variable
	int intSize = 0;
	int intTotShippedQty = 0;
	HashMap hcboVal = null;
	ArrayList alComboList = new ArrayList();

	String strSelected = "";
	String strCodeID = "";
	strShowTotalQty = GmCommonClass
			.parseNull((String) gmResourceBundleBean
					.getProperty("PACK_SLIP.SHOW_TOTAL_QTY"));

	if (!strGmLocalLic.equals("") || !strGmExportLic.equals("")) {
		strHeadAddress = strHeadAddress + "<br>&nbsp;&nbsp;D.L.NO."
				+ strGmLocalLic + "<br>&nbsp;&nbsp;D.L.NO."
				+ strGmExportLic;
	}
	if (!strExportDruglic.equals("") || !strLocalDruglic.equals("")) {
		strShipAdd = strShipAdd + "<br>&nbsp;DL- " + strLocalDruglic
				+ "<br>&nbsp;DL- " + strExportDruglic;
	}
	String strRowSpan1 = "11";
	String strRowSpan2 = "5";
	String strShowShipDate = GmCommonClass
			.parseNull((String) gmResourceBundleBean
					.getProperty("PACK_SLIP.SHOW_SHIP_DATE"));
	if (strShowShipDate.equalsIgnoreCase("Yes")) {
		strRowSpan1 = "15";
		strRowSpan2 = "9";
	}
%>
<HTML>
<HEAD>
<TITLE>GlobusOne Enterprise Portal: Packing Slip</TITLE>


</HEAD>

<BODY leftmargin="20" topmargin="10"
	>
	<FORM name="frmOrder" method="post">
		<input type="hidden" name="hMode" value="PrintPack"> <input
			type="hidden" name="hOrdId" value="<%=strOrdId%>">
			<fmt:setLocale value="${requestScope.CompanyLocale}" />
			<fmt:setBundle basename="properties.Paperwork" />
		<table align="center" class="DtTable700" cellspacing="0" style="border-bottom-style:none";
			cellpadding="0">
			<tr>
				<td>
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td height="80" width="170"><img
								src="<%=strImagePath%>/<%=strCompLogo%>.gif" width="138"
								height="60"></td>
							<td class="RightText" width="270"><%=strHeadAddress%>
							<td>
							<td align="right" class="RightText"><font size="+3"><fmt:message
										key="PACK_SLIP.TITLE" /></font>&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<td bgcolor="#666666" height="1" colspan="3"></td>
			<tr>
			<tr>
				<td colspan="2">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr class="RightTableCaption" bgcolor="#eeeeee">
							<td height="24" align="center" width="250">&nbsp;<fmt:message
									key="PACK_SLIP.BILLTO" /></td>
							<td bgcolor="#666666" width="1" rowspan="<%=strRowSpan1%>"></td>
							<td width="250" align="center">&nbsp;<fmt:message
									key="PACK_SLIP.SHIPTO" /></td>
							<td bgcolor="#666666" width="1" rowspan="<%=strRowSpan1%>"></td>
							<td width="100" align="center">&nbsp;<fmt:message
									key="PACK_SLIP.DODATE" /></td>
							<td width="100" align="center">&nbsp;<fmt:message
									key="PACK_SLIP.DONUM" /></td>
						</tr>
						<tr>
							<td bgcolor="#666666" height="1" colspan="6"></td>
						</tr>
						<tr>
						   <% if(!strAccountID.equals("")){%><!--  added this for PMT-42950 -->
						     <td class="RightText" rowspan="<%=strRowSpan2%>" valign="top"><%=strAccountID%>&nbsp;<%=strBillAdd%></td>
						    <%}else{%>
						     <td class="RightText" rowspan="<%=strRowSpan2%>" valign="top">&nbsp;<%=strBillAdd%></td>
							 <%}%>
							<td rowspan="<%=strRowSpan2%>" class="RightText" valign="top">&nbsp;<%=strShipAdd%></td>
							<td height="25" class="RightText" align="center">&nbsp;<%=strOrdredDate%></td>
							<td class="RightTableCaption" align="center">&nbsp;<%=strOrdId%></td>
						</tr>
						<tr>
							<td bgcolor="#666666" height="1" colspan="2"></td>
						</tr>
						<%
							if (!strShowShipDate.equalsIgnoreCase("Yes")) {
						%>
						<tr bgcolor="#eeeeee" class="RightTableCaption" height="14">
							<td align="center"><fmt:message key="PACK_SLIP.DISTREP" /></td>
							<td align="center"><fmt:message key="PACK_SLIP.CUSTPONUM" /></td>
						</tr>
						<tr>
							<td bgcolor="#666666" height="1" colspan="2"></td>
						</tr>
						<tr>
							<td align="center" class="RightText">&nbsp;<%=strDistRepNm%><Br></td>
							<td align="center" height="25" class="RightText">&nbsp;<%=strPO%></td>
						</tr>
						<%
							} else {
						%>
						<tr bgcolor="#eeeeee" class="RightTableCaption">
							<td align="center" colspan="2"><fmt:message
									key="PACK_SLIP.DISTREP" /></td>
						</tr>
						<tr>
							<td bgcolor="#666666" height="1" colspan="2"></td>
						</tr>
						<tr>
							<td align="center" class="RightText" colspan="2" height="20">&nbsp;<%=strDistRepNm%><Br></td>
						</tr>
						<tr>
							<td bgcolor="#666666" height="1" colspan="2"></td>
						</tr>
						<tr bgcolor="#eeeeee" class="RightTableCaption">
							<td align="center"><fmt:message key="PACK_SLIP.SHIPDATE" /></td>
							<td align="center"><fmt:message key="PACK_SLIP.CUSTPONUM" /></td>
						</tr>
						<tr>
							<td bgcolor="#666666" height="1" colspan="2"></td>
						</tr>
						<tr>
							<td align="center" height="20" class="RightText">&nbsp;<%=strShippingDate%></td>
							<td align="center" height="20" class="RightText">&nbsp;<%=strPO%></td>
						</tr>
						<%
							}
						%>
						<tr>
							<td bgcolor="#666666" height="1" colspan="6"></td>
						</tr>
						<tr class="RightTableCaption" bgcolor="#eeeeee">
							<td height="24" align="center"><fmt:message
									key="PACK_SLIP.SHIPVIA" /></td>
							<td align="center"><fmt:message key="PACK_SLIP.SHIPMODE" /></td>
							<td align="center" colspan="2"><fmt:message
									key="PACK_SLIP.TRACKINGNUM" /></td>
						</tr>
						<tr>
							<td bgcolor="#666666" height="1" colspan="6"></td>
						</tr>
						<tr>
							<td height="23" align="center" class="RightText"><%=strShipCarr%></td>
							<td align="center" class="RightText"><%=strShipMode%></td>
							<td align="center" colspan="2" class="RightText"><%=strTrack%></td>
						</tr>
						<tr>
							<td bgcolor="#666666" height="1" colspan="6"></td>
						</tr>
						<tr class="RightTableCaption" bgcolor="#eeeeee">
							<td height="24" colspan="6"><fmt:message
									key="PACK_SLIP.NOTES" /></td>
						</tr>
						<tr>
							<td bgcolor="#666666" height="1" colspan="6"></td>
						</tr>
						<tr>
							<td height="60" colspan="6" valign="top" class="RightText">&nbsp;<%=strComment%></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table align="center" border="0" class="DtTable700" cellspacing="0"
			cellpadding="0">
			<tr>
				<td align="center" colspan="2" valign="top" height="400">
					<table border="0" width="100%" height="100%" cellspacing="0"
						cellpadding="0">
						<tr class="RightTableCaption" bgcolor="#eeeeee">
							<td width="130" height="24">&nbsp;<fmt:message
									key="PACK_SLIP.PARTNUMBER" /></td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td width="300">&nbsp;<fmt:message key="PACK_SLIP.PARTDESC" /></td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td align="center" width="60"><fmt:message
									key="PACK_SLIP.QTY" /></td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td align="center" width="100"><fmt:message
									key="PACK_SLIP.CONTROLNUM" /></td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td align="center" width="80"><fmt:message
									key="PACK_SLIP.EXPIREDATE" /></td>
						</tr>
						<tr>
							<td colspan="9" height="1" bgcolor="#666666"></td>
						</tr>
						<%
							if (!strParentId.equals(strOrdId) || !strTrack.equals("")) {
								blBackFl = true;
								intSize = alPopulatedCartDtls.size();
								hcboVal = new HashMap();
								String strItems = "";
								intRowCnt = 20;
								intPageNo =0;
								intPageCnt = (intSize+20)/intPageRecCnt;
								intPageCnt++;
								for (int i = 0; i < intSize; i++) {
									hcboVal = (HashMap) alPopulatedCartDtls.get(i);
									strPartNum = (String) hcboVal.get("ID");
									strDesc = (String) hcboVal.get("PDESC");
									strQty = (String) hcboVal.get("QTY");
									strControlNum = (String) hcboVal.get("CNUM");
									strExpireDate = GmCommonClass.getStringFromDate(
											(java.util.Date) hcboVal.get("EXPDT"),
											strApplnDateFmt);
									strItemOrdType = GmCommonClass.parseNull((String) hcboVal
											.get("ITEMTYPE"));
									intTotShippedQty = intTotShippedQty
											+ Integer.parseInt(strQty);
									strShade = (i % 2 != 0) ? "class=Shade" : ""; //For alternate Shading of rows
									if (strItemOrdType.equals("50300")) {
										intRowCnt++;
						%>
						<tr>
							<td align="left" class="RightText" height="20">&nbsp;<%=strPartNum%></td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td align="left" class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td class="RightText" align="center">&nbsp;<%=strQty%></td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td align="center" class="RightText">&nbsp;<%=strControlNum%></td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td align="center" class="RightText">&nbsp;<%=strExpireDate%></td>
						</tr>
						<tr>
							<td colspan="9" height="1" bgcolor="#eeeeee"></td>
						</tr>
	<%
	if (intRowCnt == intPageRecCnt)
		{
		intPageNo++;
%>
			</table>
				</td>
			</tr>
		</table>
		<table border="0" width="700" cellspacing="0" cellpadding="0" align=center>
		<tr><td colspan="7" height="20" class="RightText" align="right"><fmt:message key="PACK_SLIP.PAGE"/> <%=intPageNo%> of <%=intPageCnt%></td></tr>
		</table>
		<p><br style="page-break-before: always;" clear="all" /></p>
		
		<table align="center" border="0" class="DtTable700" cellspacing="0"
			cellpadding="0">
			<tr>
				<td align="center" colspan="2" valign="top" height="400">
					<table border="0" width="100%" height="100%" cellspacing="0"
						cellpadding="0">
						<tr class="RightTableCaption" bgcolor="#eeeeee">
							<td width="130" height="24">&nbsp;<fmt:message
									key="PACK_SLIP.PARTNUMBER" /></td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td width="300">&nbsp;<fmt:message key="PACK_SLIP.PARTDESC" /></td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td align="center" width="60"><fmt:message
									key="PACK_SLIP.QTY" /></td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td align="center" width="100"><fmt:message
									key="PACK_SLIP.CONTROLNUM" /></td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td align="center" width="80"><fmt:message
									key="PACK_SLIP.EXPIREDATE" /></td>
						</tr>
						<tr>
							<td colspan="9" height="1" bgcolor="#666666"></td>
						</tr>
<%
		intRowCnt = 0;
		}
		%>

						<%
							} // end of IF for parts from Sales Consignments
								}
							}
							//*******************************************
							// Below code to display back order details
							//*******************************************
							if (strTrack.equals("")) {
								intSize = alBackOrderDetails.size();
								if (intSize != 0) {
						%>
						<tr>
							<td>&nbsp;</td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td>&nbsp;</td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td>&nbsp;</td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td>&nbsp;</td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="9" height="1" class="Line"></td>
						</tr>
						<tr bgcolor="#eeeeee">
							<td class="RightTextRed" colspan="9" height="24"><b><fmt:message
										key="PACK_SLIP.BACKORDMSG" /></b></td>
						</tr>
						<tr>
							<td colspan="9" height="1" class="Line"></td>
						</tr>
						<%
							for (int i = 0; i < intSize; i++) {
										hcboVal = (HashMap) alBackOrderDetails.get(i);
										strPartNum = (String) hcboVal.get("ID");
										strDesc = (String) hcboVal.get("PDESC");
										strQty = (String) hcboVal.get("QTY");
										strControlNum = (String) hcboVal.get("CNUM");
										strShade = (i % 2 != 0) ? "class=Shade" : ""; //For alternate Shading of rows
						%>
						<tr>
							<td class="RightText" height="20">&nbsp;<%=strPartNum%></td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td class="RightText" align="center">&nbsp;<%=strQty%></td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td align="center" class="RightText">&nbsp;Back Order</td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td align="center" class="RightText">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="9" height="1" bgcolor="#eeeeee"></td>
						</tr>
						<%
							} // For Loop ends here					
								}
							}
						%>
						<tr height="100%">
							<td>&nbsp;</td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td>&nbsp;</td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td>&nbsp;</td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td>&nbsp;</td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

		<%
			if (blBackFl && !strParentId.equals("")) {
		%>
		<table align="center" border="0" class="DtTable700" cellspacing="0" style="border-top-style:none";
			cellpadding="0">
			<tr bgcolor="#eeeeee">
				<td class="RightTextRed" colspan="2" height="24">&nbsp;<fmt:message
						key="PACK_SLIP.PARENTORDMSG" />: <B><%=strParentId%></B>
				</td>
			</tr>
		</table>
		<%
			}
		%>
		<table align="center" border="0" class="DtTable700" cellspacing="0" style="border-top-style:none";
			cellpadding="0">
			<tr>
				<td align="center" colspan="2" valign="top">
					<table border="0" width="100%" height="100%" cellspacing="0"
						cellpadding="0">
						<tr class="RightTableCaption">
							<td height="24" align="center" width="100">&nbsp;<fmt:message
									key="PACK_SLIP.SHIPBY" /></td>
							<td bgcolor="#666666" width="1"></td>
							<td width="100" align="center">&nbsp;<fmt:message
									key="PACK_SLIP.SHIPPEDTIME" /></td>
							<td bgcolor="#666666" width="1"></td>
							<td width="50" align="center">&nbsp;<fmt:message
									key="PACK_SLIP.SHIPCOST" /></td>
							<td bgcolor="#666666" width="1"></td>
							<td align="center" colspan="3" width="160">&nbsp;<fmt:message
									key="PACK_SLIP.SHIP_REASON" /></td>
							<td bgcolor="#666666" width="1"></td>
							<td width="150" align="center">&nbsp;<fmt:message
									key="PACK_SLIP.SIGNATURE_COUR" /></td>
						</tr>
						<tr>
							<td bgcolor="#666666" height="1" colspan="11"></td>
						</tr>
						<tr height="35">
							<td class="RightText" align="center">&nbsp;</td>
							<td bgcolor="#666666" width="1"></td>
							<td class="RightText" width="100">&nbsp;<%=strShippingTime.replace(" ", "&nbsp;")%></td>
							<td bgcolor="#666666" width="1"></td>
							<td class="RightText" align="center">&nbsp;<fmt:message
									key="PACK_SLIP.SHIPCOST_DEFAULT" /></td>
							<td bgcolor="#666666" width="1"></td>
							<td class="RightText" align="center" colspan="3">&nbsp;<fmt:message
									key="PACK_SLIP.SHIP_REASON_ORD" /></td>
							<td bgcolor="#666666" width="1"></td>
							<td class="RightText" align="center">&nbsp;</td>
						</tr>
						<tr>
							<td bgcolor="#666666" height="1" colspan="11"></td>
						</tr>
						<tr class="RightTableCaption">
							<td height="24" align="center" width="250" colspan="5">&nbsp;<fmt:message
									key="PACK_SLIP.CARRIER_ADD" /></td>
							<td bgcolor="#666666" width="1"></td>
							<td width="100" align="center">&nbsp;<fmt:message
									key="PACK_SLIP.PACK_ASPECT" /></td>
							<td bgcolor="#666666" width="1"></td>
							<td width="60" align="center">&nbsp;<fmt:message
									key="PACK_SLIP.BOXES" /></td>
							<td bgcolor="#666666" width="1"></td>
							<td width="150" align="center">&nbsp;<fmt:message
									key="PACK_SLIP.SIGNATURE_RECEIPT" /></td>
						</tr>
						<tr>
							<td bgcolor="#666666" height="1" colspan="11"></td>
						</tr>
						<tr height="45">
							<td class="RightText" align="center" colspan="5">&nbsp;</td>
							<td bgcolor="#666666" width="1"></td>
							<td class="RightText" align="center">&nbsp;</td>
							<td bgcolor="#666666" width="1"></td>
							<td class="RightText" align="center">&nbsp;</td>
							<td bgcolor="#666666" width="1"></td>
							<td class="RightText" align="center">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="1" bgcolor="#666666"></td>
			</tr>
			<tr>
				<td colspan="2" height="30" class="RightText"><fmt:message
						key="PACK_SLIP.FOOTER" /></td>
			</tr>
		</table>
		<table border="0" width="700" cellspacing="0" cellpadding="0" align=center>
		<tr><td colspan="7" height="20" class="RightText" align="right"><fmt:message key="PACK_SLIP.PAGE"/> <%=(++intPageNo)%> of <%=intPageCnt%></td></tr>
		</table>
	</FORM>

	<%
		/* Below code to display bookmark value 
		 * 2522 maps to duplicate order 
		 * 2530 maps to Bill Only - Loaner order 
		 */
		if (strOrderType.equals("2522") || strOrderType.equals("2530")) {
			String strImage = strOrderType.equals("2522")
					? "duplicate_order.jpg"
					: "loaner-picslip.gif";
	%>

	<div id="waterMark"
		style="position: absolute; z-Index: 0; top: 340; left: 340">
		<img src="<%=strImagePath%>/<%=strImage%>" border=0>
	</div>
	<script language="JavaScript1.3" src="watermark.js">
		

	</script>
	<%
		}
	%>

	<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
