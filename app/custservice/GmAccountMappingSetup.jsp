
<%
/**********************************************************************************
 * File		 		: GmAccountMappingSetup.jsp
 * Desc		 		: This screen is used to Add ICT Parameter
************************************************************************************/
%><%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>

<%@ page import="com.globus.common.servlets.GmServlet"%>

<%@ include file="/common/GmHeader.inc"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ page import="com.globus.common.forms.GmRuleParamsSetupForm"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.List" %>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ taglib prefix="fmtAccountMappingSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- \CS Label changes\custservice\GmAccountMappingSetup.jsp -->
<fmtAccountMappingSetup:setLocale value="<%=strLocale%>"/>
<fmtAccountMappingSetup:setBundle basename="properties.labels.custservice.GmAccountMappingSetup"/>

<%
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
String strWikiTitle = GmCommonClass.getWikiTitle("MAP_ACCOUNT_PARAMETERS");
ArrayList alMapAccParams =GmCommonClass.parseNullArrayList( (ArrayList) request.getAttribute("alMapAccParams"));
ArrayList alAccAttrbParams = GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("alAccAttributes"));
String strAccID = GmCommonClass.parseNull((String) request.getAttribute("hAccountId"));
String strHideSubmitButton = GmCommonClass.parseNull((String) request.getAttribute("hideSubmitButton"));
int intMapAccParamsSize = alMapAccParams.size();
int alAccAttrbParamsSize = alAccAttrbParams.size(); 

%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Map Account Parameter </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmAccountMappingSetup.js"></script>

<script>
var intMapAccParamsSize = '<%=intMapAccParamsSize%>';

</script>

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
</head>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<form name="frmAccountMapping" method="POST" action="/GmAccountServlet">
<input	type="hidden" name="hAccountId" value="<%=strAccID%>">
<table border="0" class="DtTable680" cellspacing="0" cellpadding="5">
		<tr colspan="4">
			<td colspan="2"height="25" class="RightDashBoardHeader" ><fmtAccountMappingSetup:message key="LBL_MAP_ACCOUNT_PARAMETERS"/>
			</td>
			<td  colspan="2"height="25" class="RightDashBoardHeader" align="right">
			 <fmtAccountMappingSetup:message key="IMG_ALT_HELP" var="varHelp"/><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		
	
		
		<%			
		        boolean blExcludeVATlbl =	false;	
		        HashMap hcboVal = new HashMap();
				for (int i = 0; i < intMapAccParamsSize; i++) 
				{			
					hcboVal = (HashMap) alMapAccParams.get(i);
					String strCodeID =GmCommonClass.parseNull((String) (String) hcboVal.get("CODEID"));
					String strCodeNm = GmCommonClass.parseNull((String)(String) hcboVal.get("CODENM"));
					String strCodeNmAlt = GmCommonClass.parseNull((String)(String) hcboVal.get("CODENMALT"));
					String strContrlType =GmCommonClass.parseNull((String) (String) hcboVal.get("CONTROLTYP"));
					ArrayList alAttDrpDwnValue = GmCommonClass.parseNullArrayList((ArrayList) hcboVal.get("DROPDOWNGRP"));
					String strCntrl = "AccNm"+i;
					String strShade = "Shade";
				if(strCodeID.equals("101183")){
					blExcludeVATlbl = true;
				}							
					if(i%2 == 0)
					{
						strShade = "";
					}
		%>        <tr class="<%=strShade %>">
					<td colspan="2" class="RightTableCaption" HEIGHT="24"  align="right"><%=strCodeNm%>:	</td>					
		<%       if(alAccAttrbParamsSize ==0){%>
							<input type="hidden" name="hAttributeId<%=i%>" value="<%=strCodeID%>">	<%			        
							 if(strContrlType.equals("COMBOX")){ %>
							<td colspan="2">&nbsp;<gmjsp:dropdown controlName="<%=strCntrl%>" seletedValue="0" width="150" value="<%=alAttDrpDwnValue%>" codeId="CODEID" defaultValue="[Choose One]" codeName="CODENM" />&nbsp;&nbsp;</td> <%
						    }else {%>								
							<td align="left">&nbsp;<input type="text" size="22" value="" name="AccNm<%=i%>" id="" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;&nbsp; 
							<%if(blExcludeVATlbl){ %> (Y)
							<%}%>
							</td>
						 	<%
					        }
			
		        }else{  for (int j = 0; j < alAccAttrbParamsSize; j++) 
						 {  
			                HashMap hcboValue = (HashMap) alAccAttrbParams.get(j);
							String strAttID = (String) hcboValue.get("ATRBTYPE");
							String strAttValue = (String) hcboValue.get("ATRBVALUE");
						 
							if(strCodeID.equalsIgnoreCase(strAttID)){	 %>
							<input type="hidden" name="hAttributeId<%=i%>" value="<%=strAttID%>">	<%			        
								 if(strContrlType.equals("COMBOX")){ %>
								<td colspan="2">&nbsp;<gmjsp:dropdown controlName="<%=strCntrl%>" seletedValue="<%=strAttValue%>" width="150" value="<%=alAttDrpDwnValue%>" codeId="CODEID" defaultValue="[Choose One]" codeName="CODENM" />&nbsp;&nbsp;</td> <%
							    }else {%>								
								<td align="left">&nbsp;<input type="text" size="22" value="<%=strAttValue%>" name="AccNm<%=i%>" id="<%=strAttID%>" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;&nbsp; 
								<% if(blExcludeVATlbl){	
								//we need to show this label Y only for Exclude VAT # field.
								%> (Y)
							    <% blExcludeVATlbl = false;	}%>
						       </td>
							 	<%
						        }
							}
					     }
		            }%></tr><tr><td colspan="3" height="1" class="Line"></td></tr> <%					
				}
%>		
       <tr>
			<td colspan="4" align="center" height="30">&nbsp;&nbsp;
				<% if(!strHideSubmitButton.equalsIgnoreCase("YES")){ %>   
				<fmtAccountMappingSetup:message key="BTN_SUBMIT" var="varSubmit"/>
                 	<gmjsp:button controlId="Btn_Submit" name="Btn_Submit" value="${varSubmit}" gmClass="button"   onClick="fnSubmit();" buttonType="Save" /> &nbsp;&nbsp;
                <%} %>
                <fmtAccountMappingSetup:message key="BTN_CLOSE" var="varClose"/>
                 	<gmjsp:button value="${varClose}" gmClass="button" onClick="fnClose();" buttonType="Load" />       </td>
		</tr>   
	</table>
</form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>