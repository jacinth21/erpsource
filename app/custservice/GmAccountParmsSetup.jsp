<!-- \CS Label changes\custservice\GmAccountParmsSetup.jsp -->
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%
	/**********************************************************************************
	 * File		 		: GmAccountParmsSetup.jsp
	 * Desc		 		: This screen is used for Account parameters
	 * Version	 		: 1.0
	 * author			: Dhana reddy
	 ************************************************************************************/
%>
 <!-- \custservice\GmAccountParmsSetup.jsp -->
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%-- <%@ taglib uri="/WEB-INF/gmjsp-taglib.tld" prefix="gmjsp"%> --%>
<%@ page import="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtAccountParmsSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtAccountParmsSetup:setLocale value="<%=strLocale%>"/>
<fmtAccountParmsSetup:setBundle basename="properties.labels.custservice.GmAccountSetup"/>


<bean:define id="lResult" name="frmRuleParamsSetup" property="lResult"
	type="java.util.List"></bean:define>
<bean:define id="ruleGrpId" name="frmRuleParamsSetup"
	property="ruleGrpId" type="java.lang.String"></bean:define>
<bean:define id="codeGrpId" name="frmRuleParamsSetup"
	property="codeGrpId" type="java.lang.String"></bean:define>
<bean:define id="hmResult" name="frmRuleParamsSetup" property="hmResult"
	type="java.util.HashMap"></bean:define>
<bean:define id="rulevalidation" name="frmRuleParamsSetup"
	property="rulevalidation" type="java.lang.String"></bean:define>
<bean:define id="strAccountType" name="frmRuleParamsSetup"
	property="strAccountType" type="java.lang.String"></bean:define>
<%
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strWikiTitle = GmCommonClass
			.getWikiTitle("ADD_ACCT_PARAMETERS");
HashMap hmReturn =new HashMap();
ArrayList alReturn = new ArrayList();
String strLabelID = "";
String strLabelNM = "";
String strRAID = "";
String strCntrlTyp = "";
String strCdGrp = "";
String strLblType="";
String strShade="";
String strDropDwnNm ="";
int j =0;
String strChecked = "checked";

String strCompanyLocale  = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);

String strCreateXMLFl =  GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.GENERATE_XML"));
String strSubmitAccess = GmCommonClass.parseNull((String)request.getAttribute("SUBMITACCFL"));
String strBtnDisable = "false";
if(!strSubmitAccess.equals("Y")){
	strBtnDisable = "true";
}


String strDisabled = strAccountType.equals("26230710")?"disabled":"";
String strReadOnly = strAccountType.equals("26230710")?"readOnly":"";

%>
<style>
table.DtTable750{	
	margin: 0 0 0 0;
	width: 775px;
	border: 1px solid  #676767;
}
</style>
<HTML>
<HEAD>
<TITLE>Globus Medical: Account Parameter Setup</TITLE>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/common/GmPartyShipParamsSetup.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmAccountParmsSetup.js"></script>
<script type="text/javascript">
var varRows = '<%=lResult.size()%>'; 
var vlidationStr = '<%=rulevalidation%>';
var createXMLFl = '<%=strCreateXMLFl%>';
var accountType = '<%=strAccountType%>';
</script>

</HEAD>
<BODY  onload="fnChange();">
	<html:form action="/gmRuleParamSetup.do">
		<html:hidden property="strOpt" name="frmRuleParamsSetup" value="" />
		<html:hidden property="strInputString" name="frmRuleParamsSetup"
			value="" />		
			<input type="hidden" name="codeGrpId" id="codeGrpId" value="<%=codeGrpId%>">
			<input type="hidden" name="ruleGrpId" id="ruleGrpId" value="<%=ruleGrpId%>">
			<input type="hidden" name="hVarRows" id="hVarRows" value="<%=lResult.size()%>">
			<input type="hidden" name="hValidationStr" id="hValidationStr" value="<%=rulevalidation%>">			
	    
		<table border="0" class="DtTable750" cellspacing="0" cellpadding="0">
			 
			<tr>
				<td class="Line" height="1" colspan="2"></td>
			</tr>
			<% for (int i=0 ; i < lResult.size();i++){
				hmReturn = GmCommonClass.parseNullHashMap((HashMap)lResult.get(i));
				strLabelID = GmCommonClass.parseNull(String.valueOf(hmReturn.get("LABELID")));
				strLabelNM = GmCommonClass.parseNull(String.valueOf(hmReturn.get("LABELNM")));
				strRAID = GmCommonClass.parseNull(String.valueOf(hmReturn.get("RULVALUE")));
				strCntrlTyp = GmCommonClass.parseNull(String.valueOf(hmReturn.get("CNTRLTYP")));
				strCdGrp = GmCommonClass.parseNull(String.valueOf(hmReturn.get("CDGRP")));
				if(rulevalidation.indexOf(strLabelID) != -1){
					strLblType = "MandatoryText";
				}else{
					strLblType = "BoldText";
				}
				strShade = (j%2 != 0)?"class=Shade":""; //For alternate Shading of rows
				j++;
			%>
			<input type="hidden" name="hLabelID<%=i%>" value="<%=strLabelID%>">
			<input type="hidden" name="hControlTyp<%=i%>" value="<%=strCntrlTyp%>">
			<input type="hidden" name="hLableNm<%=i%>" value="<%=strLabelNM%>">
			<%
			// 92000 Create XML
			if(strLabelID.equals("92000") && !strCreateXMLFl.equals("Y")){
			  j = j+1;
			  continue;
			}
			if(strCntrlTyp.equals("HEADER")) {
			   j =0;
			%>
			<!-- Struts tag lib code modified for JBOSS migration changes -->
			<% if(!strLabelNM.equals("Invoice Parameters")){%>
			<tr class="ShadeRightTableCaptionBlue" height="25">
				<gmjsp:label type="BoldText" SFLblControlName="<%=strLabelNM%>"
					td="true" align="left" />
				<td></td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="2"></td>
			</tr>
			<%} %>
			<%}else{ 
			strLabelNM =strLabelNM+":";
			%>
			<tr height="25" <%=strShade%> >
				<gmjsp:label type="<%=strLblType%>" SFLblControlName="<%=strLabelNM%>"
					td="true" />
			<td width=60%>
				<%if(strCntrlTyp.equals("")){ %>
				&nbsp;<input type=text size=30 name="attrValue<%=strLabelID%>" <%=strReadOnly%>
					value="<%=strRAID%>" />
			
				<%}else if(strCntrlTyp.equals("SELECT")){
					 alReturn = GmCommonClass.parseNullArrayList((ArrayList)hmResult.get(strLabelID));
					 strDropDwnNm ="attrValue"+strLabelID;
					 strRAID = strRAID.equals("")?"0":strRAID;
					 %>
					 <% if(strLabelID.equals("10304532")){ %>
					 		&nbsp;<gmjsp:dropdown controlName="<%=strDropDwnNm%>" disabled="<%=strDisabled%>"
						seletedValue="<%=strRAID%>" width="150" value="<%=alReturn%>"
						codeId="CODEID" codeName="CODENM"
						 onChange="fnChange()"/>
					 <%}else{ %>
					 &nbsp;<gmjsp:dropdown controlName="<%=strDropDwnNm%>" disabled="<%=strDisabled%>"
						seletedValue="<%=strRAID%>" width="150" value="<%=alReturn%>"
						codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"
						 onChange="fnChange()"/>
						 <%} %>
				<%}else if(strCntrlTyp.equals("CHKBOX")){
					strChecked = strRAID.equals("Y")?"checked":""; 
				%>
				 &nbsp;<input type="checkbox" name="attrValue<%=strLabelID%>" title ="<%=strLabelNM%>"  <%=strDisabled%> <%=strChecked%>/>
				<%} %>
				</td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="2"></td>
			</tr>
			<%}
			}%>
			<tr>
			<td colspan="2"> 
				<jsp:include page="/common/GmIncludeLog.jsp" >
					<jsp:param name="FORMNAME" value="frmRuleParamsSetup" />
					<jsp:param name="ALNAME" value="alLogReasons" />
					<jsp:param name="LogMode" value="Edit" />
					<jsp:param name="Mandatory" value="yes" />
				</jsp:include>
			</td>
			</tr>
			<tr height="30">
			<fmtAccountParmsSetup:message key="LBL_SUBMIT" var="varSubmit"/>
				<td colspan="2" align="center"><gmjsp:button name="subBtn" value="${varSubmit}" disabled="<%=strBtnDisable %>" gmClass="button" onClick="fnSubmit1(this.form);" buttonType="Save" />
					<%--For PAC,Close button is removed --%>
					<%-- &nbsp;&nbsp; <gmjsp:button value="Close" gmClass="button"
					onClick="fnClose();" buttonType="Load" /> --%>
				</td>
			</tr>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
	</BODY>
</HTML>