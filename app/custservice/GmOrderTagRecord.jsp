<%
/**********************************************************************************
 * File		 		: GmOrderTagRecord.jsp
 * Desc		 		: Add Tag record
 * Version	 		: 1.0
 * author			: Raja
************************************************************************************/
%>

<!DOCTYPE html>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.Locale"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.util.GmImageUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Iterator"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>	
<%@ taglib prefix="fmtOrderTagRecord" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtOrderTagRecord:setLocale value="<%=strLocale%>"/>
<fmtOrderTagRecord:setBundle basename="properties.labels.custservice.GmOrderTagRecord"/>

<bean:define id="accountId" name="frmOrderTagRecord" property="accountId" type="java.lang.String"></bean:define>
<bean:define id="tagRefId" name="frmOrderTagRecord" property="tagRefId" type="java.lang.String"></bean:define>
<bean:define id="tempId" name="frmOrderTagRecord" property="tempId" type="java.lang.String"></bean:define>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);	
    String strDoId="";
    String strAccId="";
    String strRefId="";
    String strSavedAccId ="";
    String strOrdId="";
    String strAccountID ="";
	strDoId = GmCommonClass.parseNull((String)request.getParameter("addTagDoId"));
	strAccId = GmCommonClass.parseNull((String)request.getParameter("accountId"));
	strRefId = GmCommonClass.parseNull((String) request.getAttribute("REF_ID"));
	strSavedAccId = GmCommonClass.parseNull((String) request.getAttribute("SAVED_ACCID"));
	strOrdId = GmCommonClass.parseNull((String) request.getAttribute("STRTAGREFID"));
	strAccountID = GmCommonClass.parseNull((String) request.getAttribute("ACCID"));
	String strGridXmlData = GmCommonClass.parseNull((String)request.getAttribute("GRIDXMLDATA"));
	log.debug("strGridXmlData==>"+strGridXmlData);
	int intSize=0;
	String strOpt = GmCommonClass.parseNull(request.getParameter("strOpt"));
	//log.debug(" FormName is " + strFormName);
	String strJSONString = "{\"STROPT\":\""+strOpt+"\"}";
	String strTagRefId = tagRefId;
	if(strTagRefId.startsWith("GMTEMP")) {
		strTagRefId = "";
	}
	%>

<html lang="en">
<!-- custservice\GmOrderTagRecord.jsp -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE> Globus Medical: <fmtOrderTagRecord:message key="LBL_TAG_RECORD"/> </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"	href="<%=strJsPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCombo/dhtmlxcombo.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/jquery-latest.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/customerservice/GmOrderTagRecord.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmPhoneOrder.js"></script>
<style type="text/css">

.pds_container{
 width: 780px;
 margin: 0 auto;
 margin-top:10px;
}
table.DtTable1100 td {
    padding: 5px 4px;
}
table.DtTable1100 td table{width: 99%;}
table.DtTable1100{width:100%}
#addingtagsRpt {margin-top:30px}
.centerAlign{text-align: center;}

div.gridbox table.hdr td{
border :0px;
}

</style>
<script type="text/javascript">
var strRefDoId;
 strRefDoId = '<%=strRefId%>';

var objGridData;
objGridData = '<%=strGridXmlData%>'; 
var gridObj;
var accountId = '<%=accountId%>';
var tagRefId = '<%=tagRefId%>';
var tempId = '<%=tempId%>';


var paramDoId = '<%=strDoId%>'; 
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form action="/gmOrderTagRecordAction.do"> 
 <input type="hidden" name="tagDoID" value="<%=strDoId%>">
 <input type="hidden" name="accountId" value="<%=strAccId%>">
<input type="hidden"  name="savedAccountId" value="<%=strSavedAccId%>"> 
<input type="hidden" name="tagRefID" value="<%=strRefId%>">
<table class="DtTable1100">
				<tr>
					<td colspan="4" height="25" class="RightDashBoardHeader">&nbsp;<fmtOrderTagRecord:message key="LBL_TAG_RECORD"/> &nbsp;- <%if(!strDoId.equals("") ){%><%=strDoId%><%}else{%><%=strTagRefId%><%}%></td>
					<fmtOrderTagRecord:message key="LBL_HELP" var="varHelp"/> 
				</tr>					
				<tr>
				<td>
				<table style="border: 1px solid #cccccc">
				<tr>
					<td class="RightText centerAlign"><fmtOrderTagRecord:message key="LBL_TAG_ID"/><br /><html:text property="tagId" styleClass="InputArea" name="frmOrderTagRecord" value='' onfocus="changeBgColor(this,'#AACCE8');"
				    onblur="changeBgColor(this,'#ffffff');fnProcessRecordTags();" tabindex="1" size="15" /> </td>
					<td class="RightText centerAlign"><fmtOrderTagRecord:message key="LBL_MISSING"/><br /><html:checkbox property="missing" name="frmOrderTagRecord" onclick="fnMissingCheck(this);" tabindex="2"/> </td>
					    <td class="RightText centerAlign"><fmtOrderTagRecord:message key="LBL_SET_ID"/><br />
						<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
						<jsp:param name="CONTROL_NAME" value="setId" />
						<jsp:param name="PARAMETER" value="<%=strJSONString%>" />	
						<jsp:param name="METHOD_LOAD" value="loadSetIdNameList&searchType=LikeSearch" />
						<jsp:param name="WIDTH" value="350" />
						<jsp:param name="CSS_CLASS" value="search" />
						<jsp:param name="TAB_INDEX" value="3"/>
						<jsp:param name="CONTROL_NM_VALUE" value="" /> 
						<jsp:param name="CONTROL_ID_VALUE" value="" />
						<jsp:param name="SHOW_DATA" value="100" />
						<jsp:param name="ON_BLUR" value="fnProcessRecordTags();" />					
						</jsp:include>
				</tr>
				</table>
				</td>
				</tr>				
				 <tr>
				 <td>
				<table style="border: 1px solid #cccccc">
				 <tr><td class="RightTableCaption" align="center">
				 <fmtOrderTagRecord:message key="LBL_ADDED_TAGS"/>
				 </td>
				 </tr>
				 <tr>
				<td>
				<div id="addingtagsRpt" style="height: 350px; margin-top:0; width:100%;"></div>
			    </td></tr>
				 </table>
				 </td>
				 </tr>
				
</table>
			</html:form>
</body>
</html>