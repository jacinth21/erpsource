 <%
/**********************************************************************************
 * File		 		: GmOrderReport.jsp
 * Desc		 		: This screen is used for the Order details 
 * Version	 		: 1.0
 * author			: Velu
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@page import="java.util.Date"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<!-- custservice\GmOrderReport.jsp -->
<%@ taglib prefix="fmtOrdRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtOrdRpt:setLocale value="<%=strLocale%>"/>
<fmtOrdRpt:setBundle basename="properties.labels.custservice.GmOrderReport"/>

<%
    String strWikiTitle = GmCommonClass.getWikiTitle("ORDER_DETAILS");

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	int intSize = 0;
	HashMap hmLoop = new HashMap();

	String strAccId = "";
	String strOrdId = "";
	String strFormattedOrderId = "";
	String strOrdDt = "";
	String strPrice = "";
	String strOrdBy = "";
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strItems = "";
	String strStatusFl = "";
	String strTrackNum = "";
	String strPO = "";
	String strInvId = "";
	String strShipToDate = "";
	String strOrderType = "";
	Date dtFrmDate = null;
	Date dtToDate = null;
	String strParentOId = "";
	String strShade = "";
	String strOrdCallFlag = "";
	String strInvCallFlag = "";
	String strShipDt = "";
	String strInvFl = "";
	String strOrderTypeDesc = "";
	String strAccName = "";
	String strHoldFl = "";
	String strOrdSumId = "";
	String strHoldFilter = ""; 
	String strDeptId = GmCommonClass.parseNull((String)session.getAttribute("strSessDeptSeq"));
		
	HashMap hmParam = new HashMap();
	
	String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	
	ArrayList alAccount = new ArrayList();
	ArrayList alAccountOrder = new ArrayList();
	ArrayList alRepList = new ArrayList();
	ArrayList alOrderByList = new ArrayList();
	
	int intLength = 0;

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	
	if (hmReturn != null)
	{
		alAccount = (ArrayList)hmReturn.get("ACCOUNTLIST");
		alRepList = (ArrayList)hmReturn.get("REPLIST");
		alOrderByList = (ArrayList)hmReturn.get("ORDERBYLIST");
		alAccountOrder = (ArrayList)hmReturn.get("ACCOUNTORDERLIST");
		intLength = alAccountOrder.size();
	}
	   //The following code added for passing the company info to window opener
		String strCompanyId = gmDataStoreVO.getCmpid();
		String strPlantId = gmDataStoreVO.getPlantid();
		String strPartyId = gmDataStoreVO.getPartyid();
		
		String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
		strCompanyInfo = URLEncoder.encode(strCompanyInfo);

%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Order Details </TITLE>

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script>
var compInfObj = '<%=strCompanyInfo%>';
function fnViewOrder(val)

{

	windowOpener('/GmEditOrderServlet?&companyInfo='+compInfObj+'&hMode=PrintPrice&hOrdId='+val,"PrintPack","resizable=yes,scrollbars=yes,top=250,left=250,width=810,height=450");	
}
//Open the invoice print
function fnPrintInvoice(varinv)
{
	windowOpener('/GmInvoiceInfoServlet?hAction=Print&hInv='+varinv+'&companyInfo='+compInfObj,"PrntInv","resizable=yes,scrollbars=yes,top=150,left=200,width=820,height=600");
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST">

	<table border="0" bordercolor="red" width="1140" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="5" width="1" class="Line"></td>
			<td bgcolor="#666666" height="1"></td>
			<td rowspan="5" width="1" class="Line"></td>
		</tr>
		
		<tr>
			<td height="15" colspan="4" class="RightDashBoardHeader">
				<table border="0" width="100%" >
					<tr>
						<td colspan="3" height="15" width="90%" class="RightDashBoardHeader"><fmtOrdRpt:message key="LBL_ORD_DTLS"/></td>
						<fmtOrdRpt:message key="IMG_ALT_HELP" var = "varHelp"/>
						<td height="15" width="10%"class="RightDashBoardHeader" align="right"> <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> </td>
					</tr>
				</table>					
			</td>			
		</tr>
		<tr><td colspan="7" bgcolor="#666666" width="1" height="1"></td></tr>
        <tr>
			<td height="70" valign="top">
			<div  id="OrderDiv">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				  <thead>
					<tr  class="ShadeRightTableCaption">
						<th HEIGHT="24" width="150"><fmtOrdRpt:message key="LBL_ORD_ID"/></th>
						<th HEIGHT="24" width="250"><fmtOrdRpt:message key="LBL_ACCOUNT"/></th>
						<th align="center" width="170"><fmtOrdRpt:message key="LBL_ORD_DT"/></th>
						<th width="120" align="center"><fmtOrdRpt:message key="LBL_TOTAL"/></th>
						<th width="140" align="center"><fmtOrdRpt:message key="LBL_TYPE"/></th>
						<th width="150" align="center"><fmtOrdRpt:message key="LBL_ORD_BY"/></th>
						<th align="center" width="60"><fmtOrdRpt:message key="LBL_SHIP_DT"/></th>
						<th align="center" width="110"><fmtOrdRpt:message key="LBL_TRACK_NUM"/></th>
						<th align="center"width="90"><fmtOrdRpt:message key="LBL_CUST_PO"/></th>
						<th align="center"width="110"><fmtOrdRpt:message key="LBL_PARENT_ORD_ID"/></th>
						<th align="center"width="100"><fmtOrdRpt:message key="LBL_INV_ID"/></th>
					</tr>
				<!-- 	<tr style="position:relative; top:expression(this.offsetParent.scrollTop);"><td class="Line" colspan="11"></td></tr> -->
				  </thead>
				  <tbody>
<%
		if (intLength > 0)
		{
				for (int i = 0;i < intLength ;i++ )
				{
					hmLoop = (HashMap)alAccountOrder.get(i);
					strOrdId = GmCommonClass.parseNull((String)hmLoop.get("ID"));
					strFormattedOrderId = strOrdId.replaceAll("-","");
					strOrdDt = GmCommonClass.parseNull((String)hmLoop.get("ODT"));
					strPrice = GmCommonClass.parseNull((String)hmLoop.get("COST"));
					strOrdBy = GmCommonClass.parseNull((String)hmLoop.get("NM"));
					strItems = GmCommonClass.parseNull((String)hmLoop.get("CNT"));
					strStatusFl = GmCommonClass.parseNull((String)hmLoop.get("SFL"));
					strOrdCallFlag =  GmCommonClass.parseNull((String)hmLoop.get("ORD_CALL_FLAG"));
					strInvCallFlag = GmCommonClass.parseNull((String)hmLoop.get("INV_CALL_FLAG"));
					strTrackNum = GmCommonClass.parseNull((String)hmLoop.get("TRACK"));
					strParentOId = GmCommonClass.parseNull((String)hmLoop.get("PARENTOID"));
					strTrackNum = strTrackNum.replaceAll("-","");
					strTrackNum = strTrackNum.replaceAll(" ","");
					/* strTrackNum = strTrackNum.equals("")?strTrackNum:"<a class=RightText href=javascript:fnOpenTrack('"+strTrackNum+"');>"+strTrackNum+"</a>"; */
					strShipToDate = GmCommonClass.parseNull((String)hmLoop.get("SHIP_DATE"));
					strPO = GmCommonClass.parseNull((String)hmLoop.get("PO"));
					strInvId = GmCommonClass.parseNull((String)hmLoop.get("INVID"));
					strOrderType = GmCommonClass.parseNull((String)hmLoop.get("ORDERTYPE"));
					strShipDt = GmCommonClass.getStringFromDate((Date)hmLoop.get("SDATE"),strApplnDateFmt);
					strInvFl = GmCommonClass.parseNull((String)hmLoop.get("INVFL"));
					strOrderTypeDesc = GmCommonClass.parseNull((String)hmLoop.get("ORDERTYPEDESC"));
					strAccName = GmCommonClass.parseNull((String)hmLoop.get("ANAME"));
					strOrdSumId = strParentOId.equals("")?strOrdId:strParentOId;
					strHoldFl = GmCommonClass.parseNull((String)hmLoop.get("HOLDFL"));
					strShade = (i%2 != 0)?"class=oddshade":""; //For alternate Shading of rows				
%>
						<td class="RightText" >&nbsp;<a class=RightText href= javascript:fnViewOrder('<%=strOrdId%>');><%=strOrdId%></td>		
						<td class="RightText" ><%=strAccName%></td>
						<td class="RightText"  align="center"><%=strOrdDt%></td>					
						<gmjsp:currency type="CurrTextSign" textValue="<%=strPrice%>"/>
						<td class="RightText" align="center">&nbsp;<%=strOrderTypeDesc%>&nbsp;</td>
						<td class="RightText" align="left"><%=strOrdBy%></td>
						<td class="RightText" align="center"><%=strShipDt%></td>
						<td class="RightText" align="center"><%=strTrackNum%>&nbsp;</td>
						<td class="RightText" align="center"><%=strPO%>&nbsp;</td>
						<td class="RightText" align="center" nowrap><%=strParentOId%></td>
						<td class="RightText" align="center"><a class=RightText href= javascript:fnPrintInvoice('<%=strInvId%>');><%=strInvId%>&nbsp;</td>
			 			<!-- Code for Invoice Call Log Ends -->
					</tr>
					<tr><td colspan="11" height="1" bgcolor="#CCCCCC"></td></tr>
<%
					} // end of FOR
%>
					</tbody>
				</table>
			</div>
		</td>
			</tr>
			<tr><td colspan="11" height="1" bgcolor="#CCCCCC"></td></tr>
<%}else{%>
               <tr><td colspan="10" height="50" class="RightTextRed" align="center"> <BR><fmtOrdRpt:message key="LBL_NO_ORD_ACCOUNT"/> </td></tr>
				</table>
<%
		}
%>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
