<!--  \CS Label changes\custservice\GmAckOrderModify.jsp-->

 
<%
/**********************************************************************************
 * File		 		: GmAckOrderModify.jsp
 * Desc		 		: This screen is used for Editing Acknowledgement order details
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ taglib prefix="fmtAckOrderModify" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>

<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtAckOrderModify:setLocale value="<%=strLocale%>"/>
<fmtAckOrderModify:setBundle basename="properties.labels.custservice.GmAckOrderModify"/>

<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmAckOrderModify", strSessCompanyLocale);
	String strScreenName = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_EDITORDER"));
	GmCalenderOperations gmCal = new GmCalenderOperations();
	String strWikiTitle = GmCommonClass.getWikiTitle("ACK_ORDER_EDIT");
	String strApplnDateFmt = strGCompDateFmt;
	String strCurrSymbol = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	String strAccCurrSymb ="";
	String strLotOverideAccess = GmCommonClass.parseNull((String)request.getAttribute("LOTOVERRIDEACCESS"));
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strCustServiceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strMode = (String)request.getAttribute("hMode")==null?"":(String)request.getAttribute("hMode");
	String strShipFl = GmCommonClass.parseZero((String)request.getAttribute("hShipFl"));
	String strChecked = "";
	strChecked = strShipFl.equals("2")?"checked":"0";
	String strDisabled = "";
	String strOrdStatus = "";
	String strMessages = (String)request.getAttribute("MESSAGES")==null?"":(String)request.getAttribute("MESSAGES");
	String strReadOnly="";
	String strCmtDisabled="";
	String strUpdFl = GmCommonClass.parseNull((String)request.getAttribute("QTYEDIT"));
	if(!strUpdFl.equals("Y")){
		strReadOnly = "readonly=readonly";
	}else{
		strCmtDisabled = "disabled=disable";
	}
	// Company properties
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	HashMap hmComboValue = (HashMap)request.getAttribute("hmComboValue");
	
	String strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_CONTROL_NUMBER"));
	if (strhAction.equals("EditShip"))
	{
		strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_SHIPPING_DETAILS"));
	}
	else if (strhAction.equals("EditPO"))
	{
		strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PO_DETAILS"));
	}
	String strOrderId = "";
	String strAccName = "";
	String strUserName = "";
	String strReceiveMode = "";
	java.sql.Date dtOrdDate = null;
	String strComments = "";
	String strShade = "";
	String strItemQty = "";
	String strControl = "";
	String strControlFl = "";
	
	String strCodeID = "";
	String strSelected = "";
//	String strShipDate = "";
//	String strShipCarr = "";
//	String strShipMode = "";
//	String strTrack = "";
 	String strShipCost = "";
	String strPO = "";
	String strPerson = "";
	String strAccId = "";
	
	String strReason = "";
	String strCodeNm = "";
	String strDiscountDisabled = "";
	
	String strParentOrderId = "";
	String strInvoiceId = "";
	String strSalesRepId = "";
	String strOrdInvFl = "";
	String strOrdType = "";
	String strHardCpPO = "";
	String strHardPOChecked = "";
	String strTxtBoxDisabled = "disabled";
	String strBOTxtDisable = "";
	String strMsgFl = GmCommonClass.parseNull((String)request.getAttribute("hMsgFl"));
	String strTodaysDate = gmCal.getCurrentDate(strGCompDateFmt)==null?"":gmCal.getCurrentDate(strGCompDateFmt);
	String strUserDept = (String)session.getAttribute("strSessDeptId")==null?"":(String)session.getAttribute("strSessDeptId");
	String strShowSurgAtr = GmCommonClass.parseNull( gmResourceBundleBean.getProperty("DO.SHOW_SURG_ATRB_DET"));
	ArrayList alCart = new ArrayList();
	HashMap hmOrderDetails = new HashMap();

	// Code Lookup value
	ArrayList alMode = new ArrayList();
	ArrayList alOrderMode = new ArrayList();
	ArrayList alShipTo = new ArrayList();
	ArrayList alAccount = new ArrayList();
	ArrayList alRepList = new ArrayList();
	ArrayList alLog = new ArrayList();
	ArrayList alQuoteDetails = new ArrayList();
	
	String strCollapseStyle = "display:block";
	String strCollapseImage =  strImagePath+"/minus.gif";
	String strDistName = "";
	String strBillTo = "";
	String strOrdredDate = "";
	int intPriceSize = 0;
	String strBeforeTotal = "";
	String strAfterTotal = "";
	String strLotOverrideFl = "";
	
	// To get the combo box value 
	if (hmComboValue != null)
	{
		alOrderMode = (ArrayList)hmComboValue.get("ORDERMODE");
		alAccount = (ArrayList)hmComboValue.get("ACCLIST");
	}
		
	if (hmReturn != null) 
	{
		hmOrderDetails 	= (HashMap)hmReturn.get("ORDERDETAILS");
		alQuoteDetails	= (ArrayList)hmReturn.get("QUOTEDETAILS");
		strOrderId 		= (String)hmOrderDetails.get("ID");
		strAccName 		= (String)hmOrderDetails.get("ANAME");
		strBillTo 		= (String)hmOrderDetails.get("ACCID");
		strReceiveMode	= (String)hmOrderDetails.get("RMODE");
		strPerson		= (String)hmOrderDetails.get("RFROM");
		strAccId 		= (String)hmOrderDetails.get("ACCID");
		strOrdStatus = GmCommonClass.parseZero((String)hmOrderDetails.get("STATUSFL"));
		strUserName 	= (String)hmOrderDetails.get("UNAME");
		//dtOrdDate 		= (java.sql.Date)hmOrderDetails.get("ODT");
		strOrdredDate = GmCommonClass.getStringFromDate((Date)hmOrderDetails.get("ODT"),strGCompDateFmt);
		strComments 	= (String)hmOrderDetails.get("COMMENTS");
		strPO 			= (String)hmOrderDetails.get("PO");
		strParentOrderId = (String)hmOrderDetails.get("PARENTORDID");
		strInvoiceId     = (String)hmOrderDetails.get("INVOICEID");
		alCart 			= (ArrayList)hmReturn.get("CARTDETAILS");
		strSalesRepId  = (String)hmOrderDetails.get("REPID");
		strOrdInvFl = (String)hmOrderDetails.get("INVFL");
		strOrdType = (String)hmOrderDetails.get("ORDERTYPE");
		strHardCpPO = (String)hmOrderDetails.get("HARDPOFL");
		strShipCost = GmCommonClass.parseNull((String)hmOrderDetails.get("SHIPCOST"));
		strHardPOChecked = strHardCpPO.equals("Y")?"checked":"";
		strLotOverrideFl = GmCommonClass.parseNull((String)hmOrderDetails.get("LOTOVERRIDEFL"));
		strLotOverrideFl = strLotOverrideFl.equals("Yes") ? strLotOverrideFl : "No";
		strAccCurrSymb = GmCommonClass.parseNull((String)hmOrderDetails.get("ACC_CURR_SYMB"));
		strCurrSymbol = strAccCurrSymb.equals("")?strCurrSymbol:strAccCurrSymb; 
		// The following code is for the  control number JSP included at the bottom.
		
		String strFormName = GmCommonClass.parseNull(request.getParameter("FRMNAME"));
		ArrayList alControlNumDetails = new ArrayList();
		
		if(strFormName.equals("frmOrder"))
		{
			alControlNumDetails =GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALCONTROLNUMDETAILS"));
		}
		else
		{
			alControlNumDetails =GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALFILTEREDCNUMDETAILS"));
		}
		HashMap hmControlNumDetails =new HashMap();
		String strControlNum="";
		for (int i=0;i<alControlNumDetails.size();i++)
		{
			hmControlNumDetails = (HashMap)alControlNumDetails.get(i);
			strControlNum = GmCommonClass.parseNull((String)hmControlNumDetails.get("CNUM"));
			if(!strControlNum.equals("")){
				strControlNum ="CTRL";
				break;
			}
		}
		if(alControlNumDetails.size()==0 || strControlNum.equals("")){
			strCollapseStyle = "display:none";
			strCollapseImage =  strImagePath+"/plus.gif";
		}
		request.setAttribute("alControlNumberDetails", alControlNumDetails);
		
		// J - A/R department
		if (!strInvoiceId.equals("") && !strUserDept.equals("J"))
		{
			strDisabled = "true";
		}
		   

		//C for Customer Service
		if (strUserDept.equals("C")) 
		{
			strTxtBoxDisabled = "";
			if (!strOrdInvFl.equals(""))
			{
				strTxtBoxDisabled = "disabled";
			}
		}
		
		// To get the user Log information 
		alLog = (ArrayList)request.getAttribute("hmLog");
		
	}
	
				log.debug(" Value of Ord Statuis " + strOrdStatus + " Val of Disable " +strDisabled );
				
	int intSize = 0;
	HashMap hcboVal = null;

	HashMap hmShip = new HashMap();
	ArrayList alCarrier = new ArrayList();

	hmShip = (HashMap)request.getAttribute("hmShipLoad");
	String strShipTo = "";
	String strShipToId = "";

	if (hmShip != null)
	{
		alCarrier = (ArrayList)hmShip.get("CARRIER");
		alMode = (ArrayList)hmShip.get("MODE");
		alShipTo = (ArrayList)hmShip.get("SHIPLIST");
		alRepList = (ArrayList)hmShip.get("REPLIST");
		strShipTo = (String)hmOrderDetails.get("SHIPTO");
		strShipToId = (String)hmOrderDetails.get("SHIPTOID");
	}
	String strScrnTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_ACKNOWLEDGEMENT_ORDER_EDIT"));
	
 %>

<%@page import="java.util.Date"%><HTML>
<HEAD>
<TITLE> Globus Medical: Enter Control Numbers </TITLE>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strCustServiceJsPath%>/GmOrderModify.js"></script>
<script language="JavaScript" src="<%=strCustServiceJsPath%>/GmAckOrderModify.js"></script>

<script>
var lotOverrideAccess = '<%=strLotOverideAccess%>';
var lotOverRideFl = '<%=strLotOverrideFl%>'
var splitAction = 0;//1-void order, 2- if order has single part then order move to back Order
var dateFmt = '<%=strApplnDateFmt%>';
var dept = '<%=strUserDept%>';
var servletPath = '<%=strServletPath%>';
var ShipToId = '<%=strShipToId%>';
 
</script>


</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnLoadBtn()">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmOrderEditServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hOrdId" value="<%=strOrderId%>">
<input type="hidden" name="hAccName" value="<%=strAccName%>">
<input type="hidden" name="Txt_PO" value="<%=strPO%>">
<input type="hidden" name="hSalesRepId" value="<%=strSalesRepId%>">
<input type="hidden" name="hOrdInvFl" value="<%=strOrdInvFl%>">
<input type="hidden" name="hOrdType" value="<%=strOrdType%>">
<input type="hidden" name="hRmQtyStr" value="">
<input type="hidden" name="hBOQtyStr" value="">
<input type="hidden" name="hCnumStr" value="">
<input type="hidden" name="hQuoteStr" value="">

	<table border="0" class="DtTable950" cellspacing="0" cellpadding="0">
		<!-- <tr><td bgcolor="#666666" height="1" colspan="3"></td></tr> -->
		<tr>
		<!-- 	<td bgcolor="#666666" width="1" rowspan="4"></td> -->
			<td>
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td height="25" class="RightDashBoardHeader" colspan="3"><%=strScrnTitle %></td>
					<td align="right" class=RightDashBoardHeader >
					<fmtAckOrderModify:message key="IMG_ALT_HELP" var="varHelp"/>
					<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' 
						onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />  
					</td>
				</tr>
			</table>
			</td>
		<!-- 	<td bgcolor="#666666" width="1" rowspan="4"></td> -->
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td height="100" valign="top">
			
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<% 
				if (!strMessages.equals("")){
			%>
					<tr><td colspan="2">
						<font color="red"> <%=strMessages%>	</font>
					</td><tr>
			<%}%>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="shade">
						<td class="RightTableCaption" HEIGHT="20" align="right" width="15%">
						<fmtAckOrderModify:message key="LBL_ORDER_ID" var="varOrderId"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varOrderId}" td="false"/>&nbsp;</td>
						<td class="RightText"><b><%=strOrderId%></b></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightTableCaption" HEIGHT="20" align="right"  width="15%">
						<fmtAckOrderModify:message key="LBL_ORDER_ENETRED_BY" var="varOrderEnteredBy"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varOrderEnteredBy}" td="false"/>&nbsp;</td>
						<td class="RightTableCaption"><b><%=strUserName%></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<fmtAckOrderModify:message key="LBL_DATE" var="varDate"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varDate}" td="false"/>&nbsp;<b><%=strOrdredDate%></b></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
<% // ********* Bill To Information ************** //  %>					
<% log.debug(" Vlaue of Disabled is " +strDisabled); %>
					<tr class="shade">
						<td class="RightTableCaption"  HEIGHT="23" align="right"  width="15%">
						<fmtAckOrderModify:message key="LBL_BILL_TO" var="varBillTo"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varBillTo}" td="false"/>&nbsp;</td>
						<td class="RightTableCaption" width="498">
							<select name="Cbo_BillTo" id="Region" class="RightText" tabindex="1" onChange="fnSetAccIdValue(this.value);" >
							<option value="0" ><fmtAckOrderModify:message key="LBL_CHOOSE_ONE"/>
<%			  				intSize = alAccount.size();
							hcboVal = new HashMap();
							String strRepId = "";
			  				for (int i=0;i<intSize;i++)
			  				{
					  			//strAccID
					  			hcboVal = (HashMap)alAccount.get(i);
					  			strCodeID = (String)hcboVal.get("ID");
					  			strRepId = (String)hcboVal.get("REPID");
					  			strDistName = (String)hcboVal.get("DNAME");
								strSelected = strBillTo.equals(strCodeID)?"selected":"";
%>								<option <%=strSelected%> value="<%=strCodeID%>" id="<%=strRepId%>"><%=hcboVal.get("NAME")%> / <%=strDistName%></option>
<%			  		}%> </select>
						<br>
						<fmtAckOrderModify:message key="LBL_OR_ACCOUNT_ID" var="varORAccountID"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varORAccountID}" td="false"/> <input type="text" size="6"  value="<%=strAccId%>" 
						name="Txt_AccId" class="InputArea" onBlur="fnSetCboVal(this.value);" tabindex=-1> </td></tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
<% // ********* Order Mode Information ************** //%>										
					<tr>
						<td class="RightTableCaption" HEIGHT="23" align="right"  width="15%">
						<fmtAckOrderModify:message key="LBL_MODE_OF_ORDER" var="varModeofOrder"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varModeofOrder}" td="false"/>&nbsp;</td>
						<td class="RightTableCaption"><select name="Cbo_Mode" id="Region" class="RightText" tabindex="2" 
						onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" 
						onChange="javascript:fnCallPerson();">
						
						<fmtAckOrderModify:message key="LBL_CHOOSE_ONE"/>
<%			  		intSize = alOrderMode.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alOrderMode.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strReceiveMode.equals(strCodeID)?"selected":"";
%>							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%			  		}%>							
					</select> &nbsp;&nbsp;&nbsp;&nbsp;<input class="RightTableCaption" type="checkbox" name="Chk_HardPO" <%=strHardPOChecked %>/>&nbsp;<fmtAckOrderModify:message key="LBL_HARD_COPY_PO"/>&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;
					<fmtAckOrderModify:message key="LBL_PERSON_NAME" var="varPersonName"/>
					<gmjsp:label type="RegularText"  SFLblControlName="${varPersonName}" td="false"/> &nbsp;
					<input type="text" size="20" disabled value="<%=strPerson%>"  
					name="Txt_PNm" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
					onBlur="changeBgColor(this,'#ffffff');" tabindex=3></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" HEIGHT="23" align="right"  width="15%"><fmtAckOrderModify:message key="LBL_SALES_REP"/>:&nbsp;</td>
						<td class="RightText">
							<gmjsp:dropdown controlName="Cbo_SalseRepID" seletedValue="<%=strSalesRepId%>" value="<%=alRepList%>" codeId="ID" codeName="NAME" defaultValue="[Choose One]" />
						</td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
<% // ********* Ship To Information ************** //%>															
 		
<% // ********* Order Comments Information ************** //%>																				
					<tr>
						<td class="RightTableCaption" valign="top" align="right" HEIGHT="60"  width="15%">
						<fmtAckOrderModify:message key="LBL_COMMENTS" var="varComments"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varComments}" td="false"/>&nbsp;</td>
						<td><textarea name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
						onBlur="changeBgColor(this,'#ffffff');" tabindex=6 rows=5 cols=100 
						value="<%=strComments%>"><%=strComments%></textarea></td>
					</tr>
					<tr><td colspan="3" height="1" bgcolor="#eeeeee"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" colspan="3" >
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td  class="RightTableCaption" width="15%" align="right">
									<fmtAckOrderModify:message key="LBL_CUSTOMER_PO" var="varCustomerPO"/>
									<gmjsp:label type="RegularText"  SFLblControlName="${varCustomerPO}" td="false"/>&nbsp;</td>
									<td class="RightText" HEIGHT="23" align="left" width="35%"><%=strPO%> </td>
									<td align="right" HEIGHT="24" class="RightTableCaption"  width="20%">
									<fmtAckOrderModify:message key="LBL_OVERRIDE_LOT_REQUIREMENT" var="varOverrideLotRequirement"/>
									<gmjsp:label type="RegularText"  SFLblControlName="${varOverrideLotRequirement}" td="false"/>&nbsp;</td>
									<td class="RightText" HEIGHT="23" align="left"><%=strLotOverrideFl%> </td>	
								</tr>
							</table>
						</td>
					</tr>
					
					<!-- Parent Order Id  -->
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					
					<tr>
						<td align="right" HEIGHT="24" class="RightTableCaption"  width="15%">
						<fmtAckOrderModify:message key="LBL_PARENT_ORDER_ID" var="varParentOrderId"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varParentOrderId}" td="false"/>&nbsp;</td>
						<td class="RightText" HEIGHT="23" align="left">
						<input type="text" size="20" maxlength="20"   value="<%=strParentOrderId%>" 
						name="Txt_ParentOrdId" class="InputArea">
						 
						</td>
					</tr>
					
					<!--  Invoice Id -->
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					
					<tr class="shade">
						<td align="right" HEIGHT="24" class="RightTableCaption"  width="15%">
						
						<fmtAckOrderModify:message key="LBL_INVOICE_ID" var="varInvoiceId"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varInvoiceId}" td="false"/>&nbsp;</td>
						<td class="RightText" HEIGHT="23" align="left">
						<input type="text" size="20" maxlength="20"   value="<%=strInvoiceId%>" 
						name="Txt_InvoiceId" class="InputArea">						 
						</td>
					</tr>
					<%if(strMsgFl.equals("Y")){ %>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr><td colspan="2" valign="middle"><font color="red"><b><fmtAckOrderModify:message key="LBL_PLEASE_NOTE_THAT_QTY_NOT_MODIFIED_YET_CHECK_QTY_BEFORE_PROCEEDING"/>.</b> </font></td></tr>
					<%} %>
<% // To Display Part Number Information %>
					<tr>
						<td colspan="2">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr ><td colspan="12" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption"><td height="22" colspan="12">&nbsp;<fmtAckOrderModify:message key="LBL_PART_NUMBER_DETAILS"/></td></tr>
								<tr ><td colspan="12" height="1" bgcolor="#666666"></td></tr>
								<tr class="Shade" class="RightTableCaption" style="position:relative;">
									<%-- <td width="30" height="25">&nbsp;Void</td>--%>
									<TH class="RightText" width="70" height="25">&nbsp;<fmtAckOrderModify:message key="LBL_PART"/></TH>
									<TH class="RightText" align="left" width="250"><fmtAckOrderModify:message key="LBL_PART_DESCRIPTION"/></TH>
									<TH class="RightText" width="40">&nbsp;<fmtAckOrderModify:message key="LBL_TYPE"/></TH>
									<TH class="RightText" width="40">&nbsp;<fmtAckOrderModify:message key="LBL_ORDER_QTY"/></TH>
									<TH class="RightText" width="40">&nbsp;<fmtAckOrderModify:message key="LBL_RFG_QTY"/></TH>
									<TH class="RightText" width="60" width="40">&nbsp;<fmtAckOrderModify:message key="LBL_REL_QTY"/></TH>
									<TH class="RightText" width="40">&nbsp;<fmtAckOrderModify:message key="LBL_REM_QTY"/></TH>
									<TH class="RightText" width="60" width="40">&nbsp;<fmtAckOrderModify:message key="LBL_UNIT_PRICE"/></TH>
									<TH class="RightText" width="60" width="40">&nbsp;<fmtAckOrderModify:message key="LBL_UNIT_PRICE_ADJ" /></TH>
									<TH class="RightText" width="60" width="40">&nbsp;<fmtAckOrderModify:message key="LBL_ADJ_CODE"/></TH>
									<TH class="RightText" width="60" align="right"><fmtAckOrderModify:message key="LBL_NET_UNIT_PRICE"/></TH>
									<TH class="RightText" width="60" align="right"><fmtAckOrderModify:message key="LBL_TOTAL_PRICE"/></TH>
								</tr>
								<TR><TD colspan=12 height=1 class=Line></TD></TR>
<%
						intSize = alCart.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();

							String strNextPartNum = "";
							int intCount = 0;
							String strColor = "";
							String strLine = "";
							String strPartNumHidden = "";
							String strPartDesc = "";
							String strPrice = "";
							String strTemp = "";
							String strPartNum = "";
							String strPartType = "";
							String strType = "";
							String strItemOrdId = "";
							String strPartString = "";
							String strPendShipQty = "";
							String strUnitPrice = "";
							String strUnitPriceAdj = "";
							String strAdjCode = "";
							String strNetUnitPrice = "";
							String strReserveQty = "";
							String strReleaseQty = "";
							int intQty = 0;
							int intPreQty = 0;
							int intPreRelQty = 0;
							int intReleaseQty = 0;
							double dbItemTotal = 0.0;
							double dbTotal = 0.0;
							double dbBeforeItemTotal = 0.0;
							double dbBeforePriceTotal = 0.0;
							double dbAfterItemTotal = 0.0;
							double dbAfterPriceTotal = 0.0;
							double dbAdjTotal = 0.0;
							double dbAdjstTotal = 0.0;
							
							String strItemTotal = "";
							String strBeforeItemTotal = "";
							String strAfterItemTotal = "";
							String strAdjItemTotal = "";
							String strTotal = "";
							String strAdjTotal = "";
							double dblShip = 0.0;
							double dbBeforeTotal = 0.0;
							double dbAfterTotal = 0.0;
							String strAfterPrice = "";
							String strPrevPartNum = "";
							String strPrevPrice = "";
							String strPrevAdjVal = "";
							String strPrevAdjCode = "";
							String strPrevRelQty = "0";

							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alCart.get(i);
								if (i<intSize-1)
								{
									hmTempLoop = (HashMap)alCart.get(i+1);
									strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("ID"));
								}
								
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("ID"));
								strPartNumHidden = strPartNum;
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
								strPartType = GmCommonClass.parseNull((String)hmLoop.get("ITEMTYPEDESC"));
								strType = GmCommonClass.parseNull((String)hmLoop.get("ITEMTYPE"));
								strItemOrdId = GmCommonClass.parseNull((String)hmLoop.get("IORDID"));
								strPendShipQty = (String)hmLoop.get("PENDSHIPQTY");
								strPendShipQty = strPendShipQty.equals("0")?"-":strPendShipQty;
								strReserveQty = (String)hmLoop.get("RESERVQTY");
								strReleaseQty = (String)hmLoop.get("SHIPQTY");
								
								//intReleaseQty = Integer.parseInt(strReserveQty) + Integer.parseInt(strReleaseQty);
								//strReleaseQty =  "" + intReleaseQty;
								strTemp = strPartNum;
								strUnitPrice = GmCommonClass.parseZero((String)(String)hmLoop.get("UNITPRICE"));
								strUnitPriceAdj = GmCommonClass.parseZero((String) (String)hmLoop.get("UNITPRICEADJ"));
								strAdjCode = GmCommonClass.parseNull((String)(String)hmLoop.get("UNITPRICEADJCODE"));
								strNetUnitPrice = GmCommonClass.parseZero((String)(String)hmLoop.get("NETUNITPRICE"));

								strItemQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								strControlFl = strControl.equals("")?"":"Y";
								System.out.println("====intPreQty===="+intPreQty);
								intQty = Integer.parseInt(strItemQty)+ intPreQty;
								strItemQty = ""+intQty;
								
								intReleaseQty = Integer.parseInt(strReleaseQty);
								if(!strPartNum.equals(strPrevPartNum)){
								  strPrevRelQty = "0";
								}
								
								if(strPartNum.equals(strPrevPartNum) && strUnitPrice.equals(strPrevPrice) && strAdjCode.equals(strPrevAdjCode)&& strUnitPriceAdj.equals(strPrevAdjVal)){
								  intReleaseQty = Integer.parseInt(strReleaseQty) - Integer.parseInt(strPrevRelQty);
								  strReleaseQty =  "" + intReleaseQty;
								  intPreRelQty = Integer.parseInt(strReleaseQty) + Integer.parseInt(strPrevRelQty);
								  strPrevRelQty = "" + intPreRelQty;
								}else{
								  strPrevRelQty 	= strReleaseQty;
								}
								if(intQty < intReleaseQty){
								  intReleaseQty = intQty;
								  strReleaseQty =  "" + intReleaseQty; 
								  strPrevRelQty = strReleaseQty;
								}
								strPrevPartNum 	= strPartNum;
								strPrevPrice 	= strUnitPrice;
								strPrevAdjCode	= strAdjCode;
								strPrevAdjVal	= strUnitPriceAdj;
								
								if (strPartNum.equals(strNextPartNum))
								{
									// Should not group the parts 
								/*	if((i<intSize-1) && strControlFl.equals("Y")){
										intPreQty = intQty;
										continue;
									} */
									intCount++;
									strLine = "<TR><TD colspan=12 height=1 bgcolor=#eeeeee></TD></TR>";
								}

								else
								{
									strLine = "<TR><TD colspan=12 height=1 bgcolor=#eeeeee></TD></TR>";
									if (intCount > 0)
									{
										strPartNum = "";
										strPartDesc = "";
										//strColor = "bgcolor=white";
										strLine = "";
									}
									else
									{
										//strColor = "";
									}
									intCount = 0;
									intPreQty =0;
								}

								if (intCount > 1)
								{
									strPartNum = "";
									strPartDesc = "";
									//strColor = "bgcolor=white";
									strLine = "";
								}

								if(strType.equals("50301")){ //50301: Loaner part; BO Qty text box is should be disabled for loaner parts 
								  	strBOTxtDisable = "disabled";
								}else{
								  	strBOTxtDisable = "";
								}
							
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
								//strItemQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
								//strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								//strControlFl = strControl.equals("")?"":"Y";
								//intQty = Integer.parseInt(strItemQty);
								if(strNetUnitPrice.equals("0")){
									strNetUnitPrice = strUnitPrice;
								}
								dbItemTotal = Double.parseDouble(strNetUnitPrice);
								dbItemTotal = intQty * dbItemTotal; // Multiply by Qty
								strItemTotal = ""+dbItemTotal;
								dbTotal = dbTotal + dbItemTotal;
								
								// Calculating the Total Price Before Adjustment
								dbBeforeItemTotal = Double.parseDouble(strUnitPrice);
								dbBeforeItemTotal = intQty * dbBeforeItemTotal; // Multiply by Qty
								strBeforeItemTotal = ""+dbBeforeItemTotal;
								dbBeforePriceTotal = dbBeforePriceTotal + dbBeforeItemTotal;
								strBeforeTotal = "" + 	dbBeforePriceTotal;	
								
								// Calculating the Total Price After Adjustment
								dbAfterItemTotal = Double.parseDouble(strPrice);
								dbAfterItemTotal = intQty * dbAfterItemTotal; // Multiply by Qty
								strAfterItemTotal = ""+dbAfterItemTotal;
								dbAfterPriceTotal = dbAfterPriceTotal + dbAfterItemTotal;
								strAfterPrice = "" + 	dbAfterPriceTotal;																				
								
								//strTotal = ""+dbTotal;
								strPartString = strItemQty+"^"+strItemOrdId+"^"+strPartNumHidden+"^"+strType+"^"+strPrice;
								out.print(strLine);
%>
								<tr>
									<input type="hidden" name="hPartDt<%=i%>" value="<%=strPartString%>">
									<input type="hidden" name="hcNum<%=i%>" value="<%=strControl%>"/>
									<input type="hidden" name="hpNum<%=i%>" value="<%=strPartNumHidden%>"/>
									<input type="hidden" name="hpendShipQty<%=i%>" value="<%=strPendShipQty%>"/>
									<input type="hidden" name="hRelQty<%=i%>" value="<%=strReleaseQty%>"/>
									<input type="hidden" name="hReserveQty<%=i%>" value="<%=strReserveQty%>"/>
									<%-- <td align="center"><input type="checkbox" name="chk_VdPart<%=i%>" value=""/></td>--%>
									<td class="RightText" height="20">&nbsp;<%=strPartNum%></td>
									<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td width="30" align="center">&nbsp;<%=strPartType%></td>
									<td width="30" class="RightText" align="center"><%=strItemQty%></td>
									<td class="RightText" align="center"><%=strPendShipQty%></td>
									<td class="RightText" align="center"><%=strReleaseQty%></td>
									<td align="center"><input type="text" size="3" maxlength="4" <%=strReadOnly %> <%=strTxtBoxDisabled %>  value="" name="Txt_RmQty<%=i%>" class="InputArea" onkeypress="return fnNumbersOnly(event)"></td>
									<gmjsp:currency currSymbol="<%=strCurrSymbol%>" type="CurrTextSign"  textValue="<%=strUnitPrice%>"/></td>
									<gmjsp:currency currSymbol="<%=strCurrSymbol%>" type="CurrTextSign"  textValue="<%=strUnitPriceAdj%>"/></td>	
									<td class="RightText" align="center"><%=strAdjCode%></td>		
									<gmjsp:currency currSymbol="<%=strCurrSymbol%>" type="CurrTextSign"  textValue="<%=strNetUnitPrice%>"/>&nbsp;</td>
									<gmjsp:currency currSymbol="<%=strCurrSymbol%>" type="CurrTextSign"  textValue="<%=strItemTotal%>"/>&nbsp;</td>
								</tr>
<%
							}//End of FOR Loop
								strShipCost = GmCommonClass.parseZero(strShipCost);
								dblShip = Double.parseDouble(strShipCost);
								dbBeforeTotal = Double.parseDouble(strBeforeTotal);
								dbTotal = dbBeforeTotal ;
								strBeforeTotal = ""+dbTotal;
								
								dbAfterTotal = Double.parseDouble(strAfterPrice);
								dblShip = Double.parseDouble(strShipCost);
								dbAdjstTotal = dbAfterTotal + dblShip;
								strAfterTotal = ""+dbAdjstTotal;
								
%>
								<tr><td colspan="12" height="1" bgcolor="#eeeeee"></td></tr>
								<tr class="shade">
									<!-- <td colspan="1">&nbsp;</td> -->
									<td class="RightTableCaption" height="20" colspan="11">&nbsp;<fmtAckOrderModify:message key="LBL_SHIPPING_CHARGES"/></td>
									<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strShipCost)%>&nbsp;</td>
								</tr>
								<tr><td colspan="12" height="1" bgcolor="#eeeeee"></td></tr>
								<tr>
									<td colspan="11" height="30" align="right" class="RightTableCaption"><fmtAckOrderModify:message key="LBL_TOTAL_BEFORE_ADJ"/>&nbsp;&nbsp;</td>
									<gmjsp:currency currSymbol="<%=strCurrSymbol%>" type="CurrTextSign"  textValue="<%=strBeforeTotal%>"/>&nbsp;<input type="hidden" name="hOrderAmt" value="<%=strBeforeTotal%>"></td>
								</tr>
								<tr>
								
									<td colspan="11" height="30" align="right" class="RightTableCaption"><fmtAckOrderModify:message key="LBL_TOTAL_AFTER_ADJ"/>&nbsp;&nbsp;</td>
									<gmjsp:currency currSymbol="<%=strCurrSymbol%>" type="CurrTextSign"  textValue="<%=strAfterTotal%>"/>&nbsp;<input type="hidden" name="hOrderAdjAmt" value="<%=strAfterTotal%>"></td>
								</tr>
<%
							if (strControlFl.equals("Y"))
							{
%>
								<tr><td colspan="12" height="1" bgcolor="#eeeeee"></td></tr>
								<tr><td colspan="12" align="center" class="RightTextRed"><fmtAckOrderModify:message key="LBL_PARTS_IN_THIS_ORDER_ALREADY_CONTROLLED"/>.</td></tr>
<% 								
							}
						}else {
%>
						<tr><td colspan="12" height="50" align="center" class="RightTextRed"><BR><fmtAckOrderModify:message key="LBL_NO_PART_NUMBERS_INT_THIS_ORDER"/>!</td></tr>
<%
						}		
%>
							</table>
							<input type="hidden" name="hCnt" value="<%=intSize-1%>">
						</td>
					</tr>
<%
	// End Of Part Number Information
%>					

					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr class="ShadeRightTableCaption"><td height="22" colspan="2">&nbsp;<fmtAckOrderModify:message key="LBL_REASON_FOR_CHANGES"/></td></tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr>
						<td colspan="2"><textarea name="Txt_Reason" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
						onBlur="changeBgColor(this,'#ffffff');" tabindex=6 rows=3 cols=115
						value="<%=strReason%>"><%=strReason%></textarea></td>
					</tr>					
				</table>
	</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr> 
   <!--  </table>	 -->	
<tr><td width="100%" colspan="3">	
<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
	<tr class="Shade" class="RightTableCaption" >
		<!-- <td  width="1" bgcolor="#666666"></td> -->
		<TH class="RightText" width="60" height="25"><fmtAckOrderModify:message key="LBL_DATE"/></TH>
		<TH class="RightText" align="left" width="140"><fmtAckOrderModify:message key="LBL_USER_NAME"/> </TH>
		<TH class="RightText" align="left" width="500"><fmtAckOrderModify:message key="LBL_REASON_FOR_MODIFICATION"/></TH>
		<!-- <TH class="RightText" width="1" bgcolor="#666666"></TH> -->
	</tr>
	<TR><TD colspan=5 height=1 class=Line></TD></TR>
<%	intSize = alLog.size();
	if (intSize > 0)
	{
		HashMap hmLog = new HashMap();
		for (int i=0;i<intSize;i++)
		{	hmLog = (HashMap)alLog.get(i);
%>
			<tr >
				<!-- <td width="1" bgcolor="#666666"></td> -->
				<td class="RightText" height="20">&nbsp;<%=GmCommonClass.parseNull((String)hmLog.get("DT"))%></td>
				<td class="RightText">&nbsp;<%=(String)hmLog.get("UNAME")%></td>
				<td class="RightText"><%=(String)hmLog.get("COMMENTS")%></td>
				<!-- <td width="1" bgcolor="#666666"></td> -->
			</tr>
<%		}//End of FOR Loop 
		}else {
%>		<tr><td colspan="6" height="25" align="center" class="RightTextRed">
		<BR><fmtAckOrderModify:message key="LBL_NO_UPDATE"/></td></tr>
<%		}%>
<TR><TD colspan=5 height=1 class=Line></TD></TR> 
</table>
</td></tr>
<%	/*********************************
	 * Contains the Buttons
	 *********************************/ %>
<tr><td width="100%" colspan="3">
<table border="0" width="100%" cellspacing="0" cellpadding="0" >
	<tr><td align="center" height="30" id="button" colspan="6">
	<%
		String strSubmit = "fnValidateCtrlNum('"+strScreenName+"')";
	%>
<fmtAckOrderModify:message key="" var="var"/>
		<gmjsp:button disabled="<%=strDisabled%>" value="&nbsp;Submit&nbsp;" name="Btn_Ack_Submit" gmClass="button" onClick="fnAckSubmit()" buttonType="Save" />&nbsp;&nbsp;
		<fmtAckOrderModify:message key="LBL_PACKING_SLIP" var="varPackingSlip"/>
		<gmjsp:button value="&nbsp;${varPackingSlip}&nbsp;" name="Btn_Pack" gmClass="button" onClick="fnPrintPack();" buttonType="Save" />&nbsp;&nbsp;
		<fmtAckOrderModify:message key="LBL_VOID_ORDER" var="varVoidOrder"/>
		<gmjsp:button value="&nbsp;${VoidOrder}&nbsp;" name="Btn_VoidOrd" gmClass="button" onClick="fnVoidAckOrder();" buttonType="Save" />&nbsp;&nbsp;
	</td><tr>
</table>
</td></tr>
</table>

<script>
<%
	if (strMode.equals("SaveShip"))
	{
%>
	alert("Your order is ready for Shipping. Please print Packing Slip");
<%
	}
%>
</script>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
