<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.net.URLEncoder"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %> 
<%@ page import="java.util.Date"%>

<!-- Imports for Logger -->
 <!-- \custservice\GmOrderAcknowledge.jsp -->

<%@ page buffer="16kb" autoFlush="true" %>
<%@ page import="java.util.StringTokenizer" %>
<%@ taglib prefix="fmtOrderAcknowledge" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmOrderAcknowledge.jsp -->
<fmtOrderAcknowledge:setLocale value="<%=strLocale%>"/>
<fmtOrderAcknowledge:setBundle basename="properties.labels.custservice.GmOrderAcknowledge"/>
<%
	response.setHeader("Cache-Control", "no-cache, post-check=0, pre-check=0"); //HTTP 1.1
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0
	response.setDateHeader("Expires", 0); //prevents caching at the proxy server 
	
		GmServlet gm = new GmServlet();
		gm.checkSession(response, session);
		
		GmCalenderOperations gmCal = new GmCalenderOperations();
		// to setting the time zone
		gmCal.setTimeZone(strGCompTimeZone);
		String strApplDateFmt = strGCompDateFmt;
		String strTOdayDate = gmCal.getCurrentDate(strGCompDateFmt);
		String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
		String strCompanyId = gmDataStoreVO.getCmpid();
		GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
		String strOpt = GmCommonClass.parseNull((String)session.getAttribute("strSessOpt"));
		String strCurrSym =  GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));		
		HashMap hmReturn = (HashMap) request.getAttribute("hmReturn");
		HashMap hmDefaultShipVal = new HashMap(); 
	    String strWikiTitle = GmCommonClass.getWikiTitle("PROCESS_ACKNOWLEDGEMENT_ORDER");
	    GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmOrderAcknowledge", strSessCompanyLocale);
		
		Date dtFrmDate = null;
		dtFrmDate = (Date)GmCommonClass.getStringToDate(strTOdayDate,strApplDateFmt);
		String strhAction = (String) request.getAttribute("hAction");
		if (strhAction == null) {
			strhAction = (String) session.getAttribute("hAction");
		}
		strhAction = (strhAction == null) ? "Load" : strhAction;
		
		String strShowSurgAtr = GmCommonClass.parseNull( gmResourceBundleBean.getProperty("DO.SHOW_SURG_ATRB_DET"));
		String strShowDiscount = GmCommonClass.parseNull( gmResourceBundleBean.getProperty("DO.SHOW_DISCOUNT"));
		String strHideControlNum =  GmCommonClass.parseNull((String)request.getAttribute("HIDECONTROLNUMCOL"));
		String strShowDOID = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.SHOW_DO_ID"));
		String strOrdHoldSts = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.ORDER_HOLD_STATUS"));
		String strDOdisbaledLabel= strShowDOID.equals("NO")?"disabled ":"";
		String strDOdisabledAlert= strShowDOID.equals("NO")?" Title='Ack.ID Field is disabled'":"";
		String strDODateFmt="";
		String strSelected = "";
		String strCodeID = "";
		String strChecked = "";
		String strDistName = "";
		String strCodeNm = "";
		int strDiscountIndex = 0;
		
		String strMode = "5015";
		String strShipTo = "4121"; // Hard coding for Default Shipto to Sales Rep
		String strOrdType = "101260";//GmCommonClass.parseNull(GmCommonClass.getString("PHN_ORD_TYPE"));
		String strCurDate = (String) session.getAttribute("strSessTodaysDate");
	    strDODateFmt = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.DO_FMT_ORDER"));
		java.sql.Date dtFromDate = null;
		java.sql.Date dtToDate = null;
		String strShipCarr = "";
		String strShipMode = "";		
		
		ArrayList alMode = new ArrayList();
		ArrayList alShipTo = new ArrayList();
		ArrayList alAccount = new ArrayList();
		ArrayList alRepList = new ArrayList();
		ArrayList alOrderType = new ArrayList();
		ArrayList alCarrier = new ArrayList();
		ArrayList alShipMode = new ArrayList();
		ArrayList alAllRepList = new ArrayList(); // Active and inactive.
		ArrayList alQuoteDetails = new ArrayList();
		
		
		if (hmReturn != null) {
			alMode = (ArrayList) hmReturn.get("ORDERMODE");
			alAccount = (ArrayList) hmReturn.get("ACCLIST");
			alShipTo = (ArrayList) hmReturn.get("SHIPTO");
			alRepList = (ArrayList) hmReturn.get("REPLIST");
			alOrderType = (ArrayList) hmReturn.get("ORDTYPE");
			alQuoteDetails = (ArrayList) hmReturn.get("QUOTELIST");
			hmDefaultShipVal = (HashMap)hmReturn.get("DEFAULTSHIPMODECARRVAL");
			if(hmDefaultShipVal.size() > 0){
				strShipCarr = GmCommonClass.parseNull((String)hmDefaultShipVal.get("SHIP_CARRIER"));
				strShipMode = GmCommonClass.parseNull((String) hmDefaultShipVal.get("SHIP_MODE"));
				strShipTo = GmCommonClass.parseNull((String) hmDefaultShipVal.get("SHIP_TO"));
				}
		}

		strShipCarr = strShipCarr.equals("")?"5001" : strShipCarr;
		strShipMode = strShipMode.equals("")?"5004" : strShipMode;
		strShipTo = strShipTo.equals("")?(strCountryCode.equals("en")?"4121":"4122") : strShipTo;
		int intSize = 0;
		HashMap hcboVal = null;
		int i = 0;

		String path = request.getContextPath();
		String basePath = request.getRemoteAddr() + request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		
		/* For Display the Bill Only Loaner for the Australia Changes */
		strOrdType = strOrdType.equals("") ? "101260": strOrdType;		
		// Quote Changes // 
		String strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PROCESS_NEW_ACK_ORDER"));
		String strDOLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_ACK_ID"));
		String strCustPOLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_CUSTOMER_PO"));
		String strSubmit = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_ACKNOWELDGE_ORDER"));
		String strParLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PARENT_ACK_ID"));
		// Get the company information from gmDataStoreVO
		String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
		strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>

<SCRIPT>
var showDiscount = '<%=strShowDiscount%>';
var cntCode		 = '<%=strCountryCode%>';
var showloan = '<%=strCountryCode%>';
var vcustPOLabel = '<%=strCustPOLabel%>';
var strOpt = '<%=strOpt%>'; 
var vshipMode = '<%=strShipMode%>';
var vshipCarrier = '<%=strShipCarr%>';
var vshipTo = '<%=strShipTo%>';
var imagePath = '<%=strImagePath%>';
var strHideControlNum ='<%=strHideControlNum%>';
var strShowDOID ='<%=strShowDOID%>'; 
var strAccSessCurrency ='<%=strCurrSym%>';
var vcountryCode = '<%=strCountryCode%>';
var currDate = '<%=strTOdayDate%>';
var format = '<%=strApplDateFmt%>';
var lotOverrideAccess = '';
var compId = '<%=strCompanyId%>';
function SetDefaultValues(){	
	document.all.shipCarrier.value="<%=strShipCarr%>";
	document.all.shipMode.value="<%=strShipMode%>";
	if(compId == '1000'){
		document.all.shipTo.value = "4120";
	}else{
		document.all.shipTo.value = "4122";
	}
	fnGetNames(document.all.shipTo); 
	fnAcIdBlur(document.all.Txt_AccId);	
}
 
</SCRIPT>
<html>
<head>
	<title> GlobusOne Enterprise Portal: New Order/Quote Process </title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css"> 
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
	<script language="JavaScript" src="<%=strJsPath%>/dhtmlxcommon.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/GmPhoneOrder.js"></script>
	<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
	<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>	
</head>

<BODY leftmargin="20" topmargin="10"  onload="SetDefaultValues();">
<FORM name="frmPhOrder" method="POST" action="<%=strServletPath%>/GmOrderItemServlet">
<input type="hidden" name="hAction" value="PlaceOrder">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hAdjInputStr" value="">
<input type="hidden" name="hOpt" value="<%=strOpt%>">
<input type="hidden" name="hTotal" value="">
<input type="hidden" name="hConCode" value="">
<input type="hidden" name="hRepId" value="">
<input type="hidden" name="hGpoId" value="">
<input type="hidden" name="hSysDate" value="<%=strCurDate%>"> 
<input type="hidden" name="dateFormat" value="<%=strApplDateFmt%>">
<input type="hidden" name="hQuoteStr" value="">
<input type="hidden" name="hdoDateFmt" value="<%=strDODateFmt%>">
<input type="hidden" name="hHoldFl" value="">
<input type="hidden" name="hHoldSts" value="<%=strOrdHoldSts%>">
<input type="hidden" name="RE_FORWARD" value="gmOrderItemServlet">
<!-- MNTTASK-4744 - Discount Calculation Add Hidden field-->
<input type="hidden" name="hAccDiscnt" value="">
<input type="hidden" name="hAccCurrency" value="">

<input type="hidden" name="RE_TXN" value="50925">

<input type="hidden" name="RE_SRC" value="/gmRuleEngine.do?companyInfo=<%=strCompanyInfo%>&RE_FORWARD=gmPhoneOrder&txnStatus=PROCESS&RE_TXN=50925">

	<TABLE border="0" class="DtTable900" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table  border="0" cellspacing="0" cellpadding="0"> 
		   				<tr>
		        			<td height="25" width="75%" class="RightDashBoardHeader" colspan="1"><%=strHeader%></td>
		        			<fmtOrderAcknowledge:message key="LBL_HELP" var="varHelp"/>
			    			<td class="RightDashBoardHeader" width="25%" colspan="1"><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
	    				</tr> 
	    			</table>
	    		</td>
			</tr>
			<tr><td bgcolor="#666666" height="1" ></td></tr>
			<tr bgcolor="#e4e6f2" class="RightTableCaption"><td height="25">&nbsp;<IMG id="tabBillimg" border=0 src="<%=strImagePath%>/minus.gif">&nbsp;<a href="javascript:fnShowFilters('tabBill');" tabindex="-1" class="RightText"><fmtOrderAcknowledge:message key="LBL_BILLININFO"/></a></td></tr>
			<tr>
				<td>
					<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" id="tabBill">
						<tr>
							<td class="RightTableCaption" HEIGHT="24" align="right"><font color="red">*</font> <fmtOrderAcknowledge:message key="LBL_ACCOUNT_ID"/>:</td>
							<td class="RightText">&nbsp;<input type="text" size="8" value="" name="Txt_AccId" class="InputArea" tabindex=1 onFocus="changeBgColor(this,'#AACCE8');" onBlur="javascript:fnAcIdBlur(this);"> &nbsp;&nbsp;<span class="RightTextBlue"></span></td>
						</tr>
						<tr class="oddshade">
							<td class="RightTableCaption" HEIGHT="24" align="right"><fmtOrderAcknowledge:message key="LBL_ACCOUNT_NAME"/>:</td>
							<%-- <td class="RightText" HEIGHT="24" colspan="3">&nbsp;<gmjsp:autolist controlName="Cbo_BillTo" onBlur="javascript:fnAcBlur(this)"  tabIndex="-1"  width="600" value="<%=alAccount%>" defaultValue= "[Choose One]"/></TD>
						</tr> --%>	
						<td class="RightText" HEIGHT="24" colspan="3">
							<table><tr HEIGHT="16"><td>					
							<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value=" Cbo_BillTo" />
							<jsp:param name="METHOD_LOAD" value="loadAccountList&searchType=LikeSearch" />
							<jsp:param name="WIDTH" value="500" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="CONTROL_NM_VALUE" value="" />
							<jsp:param name="CONTROL_ID_VALUE" value="" />
							<jsp:param name="SHOW_DATA" value="50" />
							<jsp:param name="ON_BLUR" value=" fnAcBlur(this);" />
							</jsp:include>	
							</td></tr>
							<tr HEIGHT="8"><td></td></tr>
							</table>
							</TD>
						</tr>					
						<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
						<tr>
							<td colspan="4" bgcolor="#eeeeee">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<IMG id="tabAcSumimg" border=0 src="<%=strImagePath%>/plus.gif">&nbsp;<a href="javascript:fnShowFilters('tabAcSum');" tabindex="-1" class="RightTableCaption"><i><fmtOrderAcknowledge:message key="LBL_SUMMARY"/></i></a>
								<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" id="tabAcSum" style="display:none">
									<tr>
										<td class="RightText" align="Right"><fmtOrderAcknowledge:message key="LBL_REGION"/>:</td>
										<td id="Lbl_Regn" class="RightText"></td>
										<td class="RightText" align="Right"><fmtOrderAcknowledge:message key="LBL_AREA_DIRECTOR"/>:</td>
										<td id="Lbl_AdName" class="RightText"></td>
										<td class="RightText" align="Right"><fmtOrderAcknowledge:message key="LBL_FIELD_SALES"/>:</td>
										<td id="Lbl_DName" class="RightText"></td>
									</tr>
									<tr>
										<td class="RightText" align="Right"><fmtOrderAcknowledge:message key="LBL_TERRITORY"/>:</td>
										<td id="Lbl_TName" class="RightText"></td>
										<td class="RightText" align="Right"><fmtOrderAcknowledge:message key="LBL_REP"/>:</td>
										<td id="Lbl_RName" class="RightText"></td>
										<td class="RightText" align="Right"><fmtOrderAcknowledge:message key="LBL_GROUP_PRICE_BOOK"/>:</td>
										<td id="Lbl_PName" class="RightText"></td>
									</tr>
									<tr>
										<td class="RightText" align="Right" valign ="top" width="100"><fmtOrderAcknowledge:message key="LBL_ACCOUNT"/>&nbsp;<fmtOrderAcknowledge:message key="LBL_INFO"/> :</td>
										<td id="Lbl_AccInfo" class="RightText" colspan="5"  width="750"></td>
								   </tr>
									<tr><td colspan="6" class="LLine"></td></tr>
								</table>
							</td>
						</tr>
						<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
						<tr>
							<td class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<fmtOrderAcknowledge:message key="LBL_ORDER_TYPE"/> :</td>
							<td>&nbsp;<select name="Cbo_OrdType" id="Region" onChange="fnSetShipSel();" class="RightText" tabindex="-1" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtOrderAcknowledge:message key="LBL_CHOOSEONE"/>
<%
								intSize = alOrderType.size();
									hcboVal = new HashMap();
									for (i = 0; i < intSize; i++) // Hardcoding to remove rest of order types - James 03/31
									{
										hcboVal = (HashMap) alOrderType.get(i);
										strCodeID = (String) hcboVal.get("CODEID");
										strSelected = strOrdType.equals(strCodeID) ? "selected" : "";
							%>
									<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
									}
%>
							</select>
						</td>
						<!-- MNTTASK-4744 - Discount Calculation Add Label-->
						<td class="RightTableCaption" HEIGHT="23" align="right"><fmtOrderAcknowledge:message key="LBL_DATE_ENTERED"/> :</td>
						<td>&nbsp; <%=strCurDate%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class="RightTableCaption" style="display:inline"><fmtOrderAcknowledge:message key="LBL_ACCOUNT_DISCOUNT"/>  % :</div><div id="Lbl_AccDiscnt" style="display:inline"></div></td>
						<!-- <td id="Lbl_AccDiscnt" class="RightText" width="20">&nbsp;</td> -->
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
					<%-- Sale Rep Change
					<tr>
						<td class="RightTableCaption" HEIGHT="24" align="right"><font color="red">*</font> Sales Rep :</td>
						<td class="RightText" HEIGHT="24" colspan="3">
						    &nbsp;<select name="Cbo_Rep" tabindex="-1" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
							<option value="0">[Choose One]</option>
							<%
								intSize = alAllRepList.size();
									hcboVal = new HashMap();
									for (i = 0; i < intSize; i++) {
										hcboVal = (HashMap) alAllRepList.get(i);
										strCodeID = (String) hcboVal.get("REPID");
							%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
							<%
								}
							%>
						</select> &nbsp;</td>
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
					--%>
					<tr >
						<td class="RightTableCaption" HEIGHT="24"  align="right"><%=strDOLabel %> :</td>
						<td class="RightText" HEIGHT="24">&nbsp;<input type="text" size="20" maxlength="20" value="" name="Txt_OrdId"   <%=strDOdisbaledLabel%> <%=strDOdisabledAlert%>  class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="3"> 
						&nbsp;<a onClick="javascript:fnPrintPack();" onmouseover="this.T_WIDTH=180;return escape('Leave this blank if there is no Ack. #');"><img src=<%=strImagePath%>/question.gif border=0></img></a></td>
					<td class="RightTableCaption" HEIGHT="24" align="right">&nbsp;<%=strParLabel%>:</td>
						<td class="RightText" HEIGHT="24">&nbsp;<input type="text" size="20" maxlength="20" value="" name="Txt_ParentOrdId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="4"> 
						&nbsp;<a onClick="javascript:fnPrintPack();"><img src=<%=strImagePath%>/question.gif border=0></img></a></td>
					
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
					<tr>
						<td class="RightTableCaption" HEIGHT="24"  align="right"><fmtOrderAcknowledge:message key="LBL_MODE_OF_ORDER"/> :</td>
						<td  class="RightText" HEIGHT="24">&nbsp;<gmjsp:dropdown controlName="Cbo_Mode"  seletedValue="<%=strMode%>"  defaultValue= "[Choose One]"	
						tabIndex="-1"  value="<%=alMode%>" codeId="CODEID" codeName="CODENM"/></td> 
					
						
						<td class="RightTableCaption" HEIGHT="24"  align="right"><%=strCustPOLabel%> :</td>
						<td  class="RightText" HEIGHT="24" >&nbsp;<input type="text" size="30" maxLength="50" value="" name="Txt_PO" class="InputArea"   onFocus="changeBgColor(this,'#AACCE8');fnPOClearDiv();" onBlur="changeBgColor(this,'#ffffff');fnCheckPO('CHECKPO');" tabindex=9>
						<span id="DivShowPOIDAvail" style="vertical-align:middle; display: none;"> <img height="24" width="25" title="Customer PO available" src="<%=strImagePath%>/success.gif"></img></span>
						<span id="DivShowPOIDExists" style="vertical-align:middle; display: none;"><img title="Customer PO already exists" src="<%=strImagePath%>/delete.gif"></img>&nbsp;<a href="javascript:fnModifyOrder()"><img height="15" border="0" title="Order Details" src="<%=strImagePath%>/location.png"></img></a></span>
						</td> 
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
					<tr>
						<td class="RightTableCaption" HEIGHT="24"  align="right"><fmtOrderAcknowledge:message key="LBL_REQUIRED_DATE"/> :</td>
						<td  class="RightText"  HEIGHT="24" >&nbsp;<gmjsp:calendar textControlName="Txt_ReqDate" textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtFrmDate.getTime())%>"   gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"  tabIndex="2"/></td>					
						
						<td class="RightTableCaption" HEIGHT="24"  align="right"></td>
						<td  class="RightText" HEIGHT="24" ></td> 
					</tr>
				</table>
				</td>
			</tr>
		   	<tr><td class="Line" height="1"></td></tr>
<%
			if (strShowSurgAtr.equals("YES") || strOpt.equals("QUOTE"))
			{
%>		   	
			<tr bgcolor="#eeeee" class="RightTableCaption"><td height="25">&nbsp;<IMG id="tabQuoteimg" border=0 src="<%=strImagePath%>/plus.gif">&nbsp;<a href="javascript:fnShowFilters('tabQuote');" tabindex="-1" class="RightText"><%=strShowSurgAtr.equals("YES")?"Surgery Details":"Quote Details" %> </a></td></tr>
			<tr>
				<td>
					<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" id="tabQuote" style="display:none">
						<tr>
<%
				intSize = alQuoteDetails.size();
				hcboVal = new HashMap();
				for (i = 0; i < intSize; i++) 
				{			
					hcboVal = (HashMap) alQuoteDetails.get(i);
					strCodeID = (String) hcboVal.get("TRTYPEID");
					strCodeNm = (String) hcboVal.get("TRTYPE");
					if(strCodeID.equals("400113") || strCodeID.equals("401405")){strDiscountIndex = i;}
%>							
							<td class="RightTableCaption" HEIGHT="24"  align="right"><%=strCodeNm%> :</td>
							<td  class="RightText" HEIGHT="24" >&nbsp;<input type="text" size="30" value="" name="Txt_Quote<%=i%>" class="InputArea" id="<%=strCodeID %>"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=9 
							<%if(strShowDiscount.equalsIgnoreCase("YES")&& strCodeID.equals("401405")){ %>onkeyup="javascript:fnEnableDiscount(this)"<%}%> ></td></td>					
<%
					if (i%2 == 0)
					{
%>									
						</tr>
						<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
						<tr>
<%
					}
				}
%>	
						</tr>
					</table>
					<input type="hidden" name="hQuoteCnt" value="<%=intSize %>">
					<input type="hidden" name="hDiscountIndex" value="<%=strDiscountIndex%>">
				</td>
			</tr>
			<tr><td class="Line" height="1"></td></tr>
<%
			}
%>			
			<tr bgcolor="#fef2cc" class="RightTableCaption"><td height="25">&nbsp;<IMG id="tabCartimg" border=0 src="<%=strImagePath%>/minus.gif">&nbsp;<a href="javascript:fnShowFilters('tabCart');"  tabindex="-1" class="RightText"><fmtOrderAcknowledge:message key="LBL_CART_DETAILS"/></a></td></tr>
			<tr>
				<td>
					<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" id="tabCart">
						<tr>			
							<td>
								<iframe src="/GmCommonCartServlet?companyInfo=<%=strCompanyInfo%>&hHideControlNumCol=<%=strHideControlNum%>" scrolling="no" id="fmCart" marginheight="0" width="100%" height="380"></iframe><BR>
						 </td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<jsp:include page="/operations/shipping/GmIncShipDetails.jsp" >
						<jsp:param name="SHIPCHRG" value="Y"/>
					</jsp:include>
				</td>
			</tr>	
			<tr><td colspan="2" class="Line" height="1"></td></tr>
			<tr>
				<td>
					<jsp:include page="/common/GmIncludeLog.jsp" >
					<jsp:param name="LogType" value="" />
					<jsp:param name="LogValue" value="" />
					<jsp:param name="LogMode" value="Edit" />
					</jsp:include>
				</td>
			</tr>
			<tr>
				<td align="center">
				<jsp:include page="/accounts/GmCreditHoldInclude.jsp" >
					<jsp:param name="ALERT" value="Y"/>
					<jsp:param name="SUBTYPE" value="104964"/>
					</jsp:include>
				</td>
			</tr>
			<tr>
		    	<td  align="center" height="35">
			    	<fmtOrderAcknowledge:message key="BTN_RESET_FORM" var="varResetForm"/>
		    		<gmjsp:button gmClass="ButtonReset" name="Btn_ResetForm" style="height: 25px" accesskey="E"  onClick="fnReset();" buttonType="Load" value="${varResetForm}"/>&nbsp;
		    		<gmjsp:button gmClass="Button" name="Btn_PlaceOrd" style="height: 25px" accesskey="O" tabindex="18" onClick="fnPlaceOrderSubmit();" buttonType="Save" value="<%=strSubmit %>"/>
					<div id="processimg" align="middle" style="display:none;"><IMG border=0 height="25" width="20" align="middle"  src="<%=strImagePath%>/process.gif"></img><font color="blue" align="middle"><b>&nbsp;&nbsp;<fmtOrderAcknowledge:message key="LBL_SAVING" /></b></font></div>
				</td>
			</tr>
			<tr><td colspan="6" height="1" class="Line"></td></tr>
			
			<tr>
			<td  colspan="6" > 
			<div id="divf" style="display:none;">
			<iframe src="/gmRuleEngine.do?companyInfo=<%=strCompanyInfo%>&RE_FORWARD=gmPhoneOrder&txnStatus=PROCESS&RE_TXN=50925" scrolling="yes" id="iframe1" marginheight="0" width="100%" height="100" frameborder="0"   ></iframe><BR>
			</div>
			</td></tr>	
			<tr><td colspan="6" height="1" class="Line"></td></tr>
		</TABLE>
								
		<BR>
		<script language="JavaScript" type="text/javascript" src="<%=strJsPath%>/wz_tooltip.js"></script> 
		</FORM>
		<%@ include file="/common/GmFooter.inc"%>
		</body>
	</html>
