 <%
/**********************************************************************************
 * File		 		: GmLoanerReconReport.jsp
 * Desc		 		: This screen is used for the displaying loaner info for reconciliation
 * Version	 		: 1.1
 * author			: D James
 * 
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import="java.util.Date"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%> 

<%@ page buffer="16kb" autoFlush="true" %>
<%@ taglib prefix="fmtLoanerReconReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmLoanerReconReport.jsp -->
<fmtLoanerReconReport:setLocale value="<%=strLocale%>"/>
<fmtLoanerReconReport:setBundle basename="properties.labels.custservice.GmLoanerReconReport"/>
<%

	String xmlGridData = (String)request.getAttribute("XmlGridData")==null?"":(String)request.getAttribute("XmlGridData");
	
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
        GmCalenderOperations gmCal = new GmCalenderOperations();
    gmCal.setTimeZone(strGCompTimeZone);
	String strWikiTitle = GmCommonClass.getWikiTitle("LOANER_RECONCD_SUMMARY");
	String strTodaysDate = gmCal.getCurrentDate(strGCompDateFmt);
	String strApplDateFmt = strGCompDateFmt;
	
	String strhAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");	
	String strDistId = GmCommonClass.parseNull((String)request.getAttribute("hDistId"));
	String strPartNum = GmCommonClass.parseNull((String)request.getAttribute("PNUM"));
	String strLnType = GmCommonClass.parseNull((String)request.getAttribute("LNTYPE"));
	String strReconType = GmCommonClass.parseNull((String)request.getAttribute("RECONTYPE"));
	String strFromDateEntered = GmCommonClass.parseNull((String)request.getAttribute("FROMDATE"));
	String strToDateEntered = GmCommonClass.parseNull((String)request.getAttribute("TODATE"));
	String strShwDet = GmCommonClass.parseNull((String)request.getAttribute("SHWDET"));
	String strChecked = strShwDet.equals("Y")?"checked":"";
	
	ArrayList alDistributorList = (ArrayList)request.getAttribute("DISTLIST");
	ArrayList alLnType = (ArrayList)request.getAttribute("LNTYLIST");
	ArrayList alReconType = (ArrayList)request.getAttribute("LNRCLIST");
	
	Date dtFrmDate = null;
	Date dtToDate = null;
	
	dtFrmDate = (Date)GmCommonClass.getStringToDate(strFromDateEntered,strApplDateFmt);
	dtToDate = (Date)GmCommonClass.getStringToDate(strToDateEntered,strApplDateFmt);
%>

<HTML>
<TITLE> Globus Medical: Loaner Reconciliation </TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmLoanerReconReport.js"></script>
<script>

var strTodaysDate = '<%=strTodaysDate%>';
var dateFmt = '<%=strApplDateFmt%>';
var objGridData;

objGridData = '<%=xmlGridData%>';


</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<FORM name="frmLoaner" method="POST" action="<%=strServletPath%>/GmLoanerPartRepServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hOpt" value="<%=strhAction%>">
<input type="hidden" name="hId" value="">

	<table border="0" class="GtTable900" cellspacing="0" cellpadding="0">
		
		<tr>
			<td height="25" class="RightDashBoardHeader"  colspan="5"><fmtLoanerReconReport:message key="LBL_LOANER_RECONCILIATION"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtLoanerReconReport:message key="IMG_HELP" var="varHelp"/>
			<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    </td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="6"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td height="25" class="RightTableCaption"><fmtLoanerReconReport:message key="LBL_DISTRIBUTOR_LIST"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="Cbo_DistId"  seletedValue="<%=strDistId%>" 	
					 value="<%=alDistributorList%>" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]"  />										
			</td>		
			<td height="25" class="RightTableCaption"><fmtLoanerReconReport:message key="LBL_PART_NUMBER"/>:</td>
			<td>&nbsp;<input type="text" size="20" value="<%=strPartNum%>" name="TxtPartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
			<td height="25" class="RightTableCaption"><fmtLoanerReconReport:message key="LBL_LOANER_TYPE"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="Cbo_lnType"  seletedValue="<%=strLnType%>" value="<%=alLnType%>" codeId = "CODENMALT"  codeName = "CODENM" defaultValue= "[Choose One]"  />
			</td>
		</tr>
		<tr><td class="lline" colspan="6"></td></tr>		
		<tr class="Shade">
			<td class="RightTableCaption" height="25" align="right">&nbsp;<fmtLoanerReconReport:message key="LBL_DATE"/>:&nbsp;</td>			
			<td>&nbsp;<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtFrmDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="2"/>
				&nbsp;<fmtLoanerReconReport:message key="LBL_TO"/>:<gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=dtToDate==null?null:new java.sql.Date(dtToDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="3"/>&nbsp;&nbsp;
			</td>
			<td height="25" class="RightTableCaption"><fmtLoanerReconReport:message key="LBL_RECONC_TYPE"/>:</td>
			<td>&nbsp;
			<gmjsp:dropdown controlName="Cbo_ReconType"  seletedValue="<%=strReconType%>"  value="<%=alReconType%>" codeId = "CODEID"  codeName= "CODENM" defaultValue= "[Choose One]"  />
			</td>
			<td>
			<input type="checkbox" <%=strChecked%> name="Chk_shwDet" title="Show Reconcilation Details"   onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;<fmtLoanerReconReport:message key="LBL_SHOW_DETAILS"/> 
			</td>
			<td>&nbsp;&nbsp;&nbsp;
			<fmtLoanerReconReport:message key="LBL_RELOAD" var="varReload"/>
			<gmjsp:button value="&nbsp;${varReload}&nbsp;" gmClass="Button" onClick="javascript:return fnReload();" buttonType="Load" />
			</td>
			</tr>
	</table>
		<table border="0"  cellspacing="0" cellpadding="0">
		<%if(!xmlGridData.equals("")){%>
		     <tr>
           	<td colspan="4" height="50" >
				<div id="LoanerReconReport" style="height: 400px; width: 900px"></div>
		    </td>	
		</tr>
			<tr>
			<td colspan="6" align="center">
			<div class='exportlinks'><fmtLoanerReconReport:message key="LBL_EXPORT_OPTIONS"/> : <img src='img/ico_file_excel.png' />&nbsp; 
			<a href="#" onclick="fnExport();"> <fmtLoanerReconReport:message key="LBL_EXCEL"/>  </a>			
			</div>
			</td>
			</tr>
			<%}%>
			 			
		</table>
		
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
