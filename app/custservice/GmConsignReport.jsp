 <%
/**********************************************************************************
 * File		 		: GmConsignReport.jsp
 * Desc		 		: This screen is used for the DashBoard Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtConsignReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- custservice\GmConsignReport.jsp -->

<fmtConsignReport:setLocale value="<%=strLocale%>"/>
<fmtConsignReport:setBundle basename="properties.labels.custservice.GmConsignReport"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	ArrayList alIniSets = new ArrayList();
	if (hmReturn != null) 
	{
		alIniSets = (ArrayList)hmReturn.get("INISETS");
	}

	int intSetLength = alIniSets.size();

	HashMap hmLoop = new HashMap();

	String strConId = "";
	String strSetName = "";
	String strPerson = "";
	String strDateInitiated = "";
	String strComments = "";

	String strShade = "";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Consign Process</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>
function fnSubmit(val)
{
	document.frmAccount.hColumn.value = val;
	document.frmAccount.submit();
}

function fnCallEdit()
{
	document.frmAccount.hMode.value = document.frmAccount.Cbo_Action.value;
	document.frmAccount.hAction.value = "EditLoad";
	document.frmAccount.submit();
}

function Toggle(val)
{
	
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className="";
		tabobj.style.background="#ffffff";
	}
}

function fnCallDiv(val)
{
	document.frmAccount.hId.value = val;
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmConsignSetServlet">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hMode" value="">

	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="5" width="1" class="Line"></td>
			<td height="1" bgcolor="#666666"></td>
			<td rowspan="5" width="1" class="Line"></td>
		</tr>
		<tr>
			<td height="25" class="RightDashBoardHeader">
				<fmtConsignReport:message key="LBL_REPORT_SETS_FOR_CONSIGNING"/>
			</td>
		</tr>
		<tr><td class="Line" height="1"></td></tr>
		<tr>
			<td>
				<table border="0" width="698" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td width="100"><fmtConsignReport:message key="LBL_SET_NAME"/></td>
						<td HEIGHT="24" width="100"><fmtConsignReport:message key="LBL_CONSIGN_ID"/></td>
						<td width="150" align="center"><fmtConsignReport:message key="LBL_BUILT_BY"/></td>
						<td width="350">&nbsp;<fmtConsignReport:message key="LBL_COMMENTS"/></td>
					</tr>
<%
			if (intSetLength > 0)
			{
				HashMap hmTempLoop = new HashMap();
				String strSetId = "";
				String strNextId = "";
				int intCount = 0;
				String strLine = "";
				boolean blFlag = false;
				boolean blFlag1 = false;
				String strTemp = "";
				int intSetCnt = 0;
				StringBuffer sbTemp = new StringBuffer();
				for (int i = 0;i < intSetLength ;i++ )
				{
					hmLoop = (HashMap)alIniSets.get(i);

					if (i<intSetLength-1)
					{
						hmTempLoop = (HashMap)alIniSets.get(i+1);
						strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("SID"));
					}
					strSetId = GmCommonClass.parseNull((String)hmLoop.get("SID"));
					strConId = GmCommonClass.parseNull((String)hmLoop.get("CID"));
					strSetName = GmCommonClass.parseNull((String)hmLoop.get("SNAME"));
					strPerson = GmCommonClass.parseNull((String)hmLoop.get("PER"));
					strDateInitiated = GmCommonClass.parseNull((String)hmLoop.get("IDATE"));
					strComments = GmCommonClass.parseNull((String)hmLoop.get("COMMENTS"));
					strTemp = strSetId.replaceAll("\\.","");

					if (strSetId.equals(strNextId))
					{
						intCount++;
						strLine = "<TR><TD colspan=4 height=1 class=Line></TD></TR>";
						intSetCnt++;
					}
					else
					{
						intSetCnt++;
						sbTemp.append("document.all.tdset"+strTemp+".innerHTML = '<b>"+intSetCnt+"</b>';");
						intSetCnt = 0;
						strLine = "<TR><TD colspan=4 height=1 class=Line></TD></TR>";
						if (intCount > 0)
						{
							strSetName = "";
							strLine = "";
						}
						else
						{
							blFlag = true;
						}
						intCount = 0;
						//
					}

					if (intCount > 1)
					{
						strSetName = "";
						strLine = "";
					}
				
					strShade = (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows

					out.print(strLine);
					
					if (intCount == 1 || blFlag)
					{
%>
					<tr id="tr<%=strTemp%>">
						<td colspan="3" width="350" height="18" nowrap class="RightText">&nbsp;<A class="RightText" title="Click to Expand" href="javascript:Toggle('<%=strTemp%>')"><%=GmCommonClass.getStringWithTM(strSetName)%></td>
						<td class="RightText"  id="tdset<%=strTemp%>"></td>
					</tr>
					<tr>
						<td colspan="4"><div style="display:none" id="div<%=strTemp%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tab<%=strTemp%>">
<%
								blFlag = false;
								}
%>

					<tr <%=strShade%>>
						<td width="100" class="RightText">&nbsp;<input type="radio" name="rad" onClick="fnCallDiv('<%=strConId%>');"></td>
						<td width="100" height="20" class="RightText">&nbsp;<%=strConId%></td>
						<td width="150" class="RightText" align="center"><%=strPerson%></td>
						<td width="350" class="RightText">&nbsp;<%=strComments%></td>
					</tr>
					<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
<%					
					if ( intCount == 0 || i == intSetLength-1)
					{
						if (i == intSetLength-1)
						{
							sbTemp.append("document.all.tdset"+strTemp+".innerHTML = '<b>"+intSetCnt+"</b>';");
						}
%>
							</table></div>
						</td>
					</tr>
<%
					}
				} // end of FOR	
							out.print("<script>");
							out.print(sbTemp.toString());
							out.print("</script>");
%>
					<tr>
						<td colspan="4" class="RightText" align="Center"><BR><fmtConsignReport:message key="LBL_CHOOSE_ACTION"/>:&nbsp;<select name="Cbo_Action" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtConsignReport:message key="LBL_CHOOSE_ONE"/>
							<option value="CON"><fmtConsignReport:message key="LBL_CONSIGN_TO_DISTRIBUTOR"/></option>
							<option value="INV"><fmtConsignReport:message key="LBL_BACK_INVENTORY"/></option>
							<!--<option value="VO">Void Order</option>-->
						</select>
						<fmtConsignReport:message key="BTN_SUBMIT" var="varSubmit"/>
						<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnCallEdit();" tabindex="25" buttonType="Save" />&nbsp;<BR><BR>
						</td>
					</tr>
<%
			} else	{
%>
					<tr>
						<td height="30" colspan="4" align="center" class="RightTextBlue"><fmtConsignReport:message key="LBL_NO_BUILT_SETS"/> !</td>
					</tr>
<%
		}
%>
					<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>

				</table>
			</td>
		</tr>
	</table>


</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
