 <%
/**********************************************************************************
 * File		 		: GmConsignItemReport.jsp
 * Desc		 		: This screen is used for the DashBoard Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Date" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtConsItemRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- custservice\GmConsignItemReport.jsp -->
<fmtConsItemRpt:setLocale value="<%=strLocale%>"/>
<fmtConsItemRpt:setBundle basename="properties.labels.custservice.GmConsignCountReport"/>

<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);


	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strType = (String)request.getAttribute("hType") == null?"":(String)request.getAttribute("hType");
	String strHeader = "";
	String strLabel = "";
	String strLabel2 = "";

	String strFromDt = (String)request.getAttribute("hFrmDt")==null?"":(String)request.getAttribute("hFrmDt");
	String strToDt = (String)request.getAttribute("hToDt")==null?"":(String)request.getAttribute("hToDt");
	String strApplDateFmt = strGCompDateFmt; //GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	Date dtfromdate=GmCommonClass.getStringToDate(strFromDt,strApplDateFmt);
	Date dttodate=GmCommonClass.getStringToDate(strToDt,strApplDateFmt);
	
	String strId = "";
	String strName = "";
	
	String strGrid = GmCommonClass.parseNull((String)request.getAttribute("grid_XML"));
	String strScreenType =GmCommonClass.parseNull((String)request.getAttribute("hScreenType"));
	String strWikiTitle = GmCommonClass.getWikiTitle("CONSIGN_ITEM_RPT");


	if (strType.equals("50230"))//DIST
	{
		strHeader = "By Distributor";
		strId = "DID";
		strName = "DNAME";
		strLabel = "Distributor";
		strLabel2 = "";
	}
	if (strType.equals("INHOUSE"))//INHOUSE
	{
		strHeader = "By In-House";
		strId = "PID";
		strName = "PURPOSE";
		strLabel = "Purpose";
		strLabel2 = "Employee";
	}
	if (strType.equals("50234") || strType.equals("50235")|| strType.equals("50236") || strType.equals("50237")) {
		strId = "DID";
		strName = "DNAME";
		strLabel = "Distributor";
		strLabel2 = "";
		if (strType.equals("50234")) //ICT
		{
			strHeader = "By ICT";
		}
		else if (strType.equals("50235")) //FD
		{
			strHeader = "By FD";
		}
		else if (strType.equals("50236")) //ICT
		{
			strHeader = "By ICT";
		}
		else if (strType.equals("50237")) //Swiss
		{
			strHeader = "By ICS";
		}
	}
	
	ArrayList alReport = new ArrayList();
	if (hmReturn != null) 
	{
		alReport = (ArrayList)hmReturn.get("REPORT");
	}

	int intSetLength = alReport.size();

	HashMap hmLoop = new HashMap();

	String strConsignee = "";

	String strConsignId = "";
	String strDate = "";
	String strComments = "";
	String strEmp = "";
	String strShade = "";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Item Consign Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>


var objGridData;
var rowID = '';
objGridData = '<%=strGrid%>';
var gridObj = '';
var dateFmt = '<%=strApplDateFmt%>';
var lblFromDt = '<fmtConsItemRpt:message key="LBL_FROM_DATE"/>';
var lblToDt = '<fmtConsItemRpt:message key="LBL_TO_DATE"/>';
var lblType = '<fmtConsItemRpt:message key="LBL_TYPE" />';


function fnLoadType()
{
	fnValidateDropDn('hType', lblType);
	// validate the Date format
	CommonDateValidation(document.frmAccount.Txt_FromDate,dateFmt,message[10527] + message[611]);
	CommonDateValidation(document.frmAccount.Txt_ToDate,dateFmt,message[10528]+ message[611]);
	
	fnValidateTxtFld('Txt_FromDate', lblFromDt);
	fnValidateTxtFld('Txt_ToDate', lblToDt);
	
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}	
	document.frmAccount.hAction.value = "Reload";
	fnStartProgress();
	document.frmAccount.submit();
}

function Toggle(val)
{
	
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className="";
		tabobj.style.background="#ffffff";
	}
}

function fnLoad()
{
	var val = '<%=strType%>';
	var obj = document.frmAccount.hType;
	
	if (val == '50230')
	{
		obj.selectedIndex = '1';
	}
	 if (val == 'INHOUSE')
	{
		obj.selectedIndex = '2';
	}
	 if (val == '50234') // ICT
	{
		obj.selectedIndex = '3';
	}
	 if (val == '50235') //FD
	{
		obj.selectedIndex = '4';
	}
	if (val == '50236') //ICS
	{
		obj.selectedIndex = '5';
	}
	if (val == '50237') //Swiss
	{
		obj.selectedIndex = '6';
	}
	document.frmAccount.hType.value = val;
	if(objGridData !=''){
	
		gridObj = initGridData('consignItemRpt', objGridData);
		gridObj.enableTooltips("true,false,true,true,true,true,true");
		gridObj.attachHeader("#select_filter,#text_filter,#select_filter,#select_filter,#select_filter");
	}
}


function fnAgree(cnid)
{
windowOpener("/GmConsignSetServlet?hId="+cnid+"&hAction=Ack&","Ack","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");

}

function fnPrintVer(cnid)
{
windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId="+cnid,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnReleaseForSale(cnid)
{
	windowOpener('/GmProdReportServlet?hAction=Part&hId=0&hConsignID='+cnid,"RFS","resizable=yes,scrollbars=yes,top=250,left=100,width=1170,height=510");
}

function fnPackslip(cnid)
{
windowOpener("/GmConsignSetServlet?hAction=PackVersion&hId="+cnid,"Pack","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function initGridData(divRef,gridData)
{
	if(gridData !=''){
		var gObj = new dhtmlXGridObject(divRef);
		gObj.setSkin("dhx_skyblue");
		gObj.setImagePath("<%=strJsPath%>/<%=strExtWebPath%>/dhtmlxGrid/imgs/");
		gObj.init();	
		gObj.loadXMLString(gridData);	
		return gObj;
	}
}
//This function will call the press the enter button.
function fnEnter(){
	if (event.keyCode == 13){ 
		fnLoadType();
	}
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad();" onkeypress="fnEnter();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmConsignmentReportServlet">
<input type="hidden" name="Cbo_Dist" value="">
<input type="hidden" name="Cbo_Set" value="">
<input type="hidden" name="hAction" value="DrillDown">
<input type="hidden" name="screenType" value="<%=strScreenType%>">

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="7">
				<fmtConsItemRpt:message key="TD_CONS_ITEM_RPT_HEADER"/>
			</td>
			<fmtConsItemRpt:message key="IMG_ALT_HELP" var = "varHelp"/>
			<td class="RightDashBoardHeader" align="right"> <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> </td>
		</tr>
		<tr><td class="Line" height="1" colspan="8"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<fmtConsItemRpt:message key="LBL_TYPE" var = "varType"/>
		<tr><td class="RightTableCaption" height="30" align="right">&nbsp;<gmjsp:label
						type="MandatoryText" SFLblControlName="${varType}:" td="false" /></td>
				<td>&nbsp;<select name="hType" class="RightText" tabindex="1" onFocus="changeBgColor(this,'#AACCE8');"  onBlur="changeBgColor(this,'#ffffff');">
				 <option value="0" ><fmtConsItemRpt:message key="OPT_CHOOSE"/>
				<option value="50230"><fmtConsItemRpt:message key="OPT_CONSIGN"/>
				<option value="50232"><fmtConsItemRpt:message key="OPT_INHOUSE_CONS"/>
			</select></td>
					<fmtConsItemRpt:message key="LBL_FROM_DATE" var = "varFromDate"/>
					<td class="RightTableCaption" height="25" align="right" width="100">&nbsp;<gmjsp:label
						type="MandatoryText" SFLblControlName="${varFromDate}:" td="false" /></td>
					<td>&nbsp;<gmjsp:calendar textControlName="Txt_FromDate" gmClass="InputArea" textValue="<%=dtfromdate==null?null:new java.sql.Date(dtfromdate.getTime())%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="2"/>&nbsp;
					<fmtConsItemRpt:message key="LBL_TO_DATE" var = "varToDate"/>
					<td class="RightTableCaption" height="25" align="right">&nbsp;<gmjsp:label
						type="MandatoryText" SFLblControlName="${varToDate}:" td="false" /></td>
					<td>&nbsp;<gmjsp:calendar textControlName="Txt_ToDate" gmClass="InputArea" textValue="<%=dttodate==null?null:new java.sql.Date(dttodate.getTime())%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="3"/>&nbsp;
					</td>
					<fmtConsItemRpt:message key="BTN_LOAD" var = "varLoad"/>
					<td colspan="2" align="center"><gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnLoadType();" tabindex="4" buttonType="Load" /></td>
				</tr>
				
			<%
					if (strGrid.indexOf("cell") != -1) {
				%>
		<tr>
			<td colspan="8">
				<div id="consignItemRpt" style="" height="400px" width="849px"></div>
			</td>
		</tr>
		<tr height="30">
	              <td colspan="8" align="center">&nbsp;
	              <div class='exportlinks'><fmtConsItemRpt:message key="DIV_EXPORT_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
	                              onclick="gridObj.toExcel('/phpapp/excel/generate.php');"><fmtConsItemRpt:message key="DIV_EXCEL"/></a>
	                              </div>
	              </td>
			</tr>
		<%}else{ %>
		<tr><td class="LLine" height="1" colspan="8"></td></tr>
		<tr height="25">
			<td colspan="8" align="center" class="RegularText">
				<fmtConsItemRpt:message key="MSG_NO_DATA"/>
			</td>
			
		</tr>
		<%} %>
	</table>


</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
