<%
/**********************************************************************************
 * File		 		: GmShipFileUpload.jsp
 * Desc		 		: Upload Shipping Document Files
 * Version	 		: 1.0
 * author			: HReddi
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmGridFormat"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ taglib prefix="fmtShipFileUpload" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmShipFileUpload.jsp -->
<fmtShipFileUpload:setLocale value="<%=strLocale%>"/>
<fmtShipFileUpload:setBundle basename="properties.labels.custservice.Shipping.GmShippingReport"/>
<bean:define id="strRefType" name="frmShipFileUpload" property="refType" type="java.lang.String"></bean:define>
<bean:define id="strXmlData" name="frmShipFileUpload" property="strXmlData" type="java.lang.String"></bean:define>
<bean:define id="strRefID" name="frmShipFileUpload" property="strRefID" type="java.lang.String"></bean:define>
<bean:define id="intRecSize" name="frmShipFileUpload" property="intRecSize" type="java.lang.Integer"></bean:define>
<bean:define id="strOpt" name="frmShipFileUpload" property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="message" name="frmShipFileUpload" property="message" type="java.lang.String"></bean:define>

<% 
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strWikiTitle =  GmCommonClass.getWikiTitle("SHIPFILEUPLOAD");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: COM/RSR Upload </TITLE>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmShipFileUpload.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmProgress.css">
<script>
	var objGridData = '<%=strXmlData%>';
	var strOpt;
	var message = '<%=message%>';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnLoad();" >
<html:form action="/gmShipFileUploadAction.do" enctype="multipart/form-data">
<html:hidden property="strOpt"  name="frmShipFileUpload"/>
<html:hidden property="refType" name="frmShipFileUpload"/>
<input type="hidden" name="hDisplayNm">
<input type="hidden" name="hRedirectURL">
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="5">&nbsp;<fmtShipFileUpload:message key="LBL_ORDER_UPLOAD_FILES"/></td>
			<td align="right" class=RightDashBoardHeader><fmtShipFileUpload:message key="LBL_HELP" var="varHelp"/><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<% if(!message.equals("")){ %>			
			<tr id="DivShowFileUploadSuccess">
				<td colspan="7" align="center" height="25">&nbsp;<font style="color: Blue;"><b><bean:write name="frmShipFileUpload" property="message"/></b></font></td>
			</tr>
		<%}%>
		<tr>
			<td height="30" colspan="5" class="RightTableCaption" align="right" width="40%"><fmtShipFileUpload:message key="LBL_TRANSACTION_ID"/>:</td>
			<td>&nbsp;<html:text maxlength="25" property="strRefID" size="15"   
							styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff'); fnValidateTransaction(this);" /></td>
		<tr id="DivErrorMessage"><td colspan="7" align="Center" style="color: red"><fmtShipFileUpload:message key="LBL_TRANSACTIONINVALID"/></td></tr>			
		<tr><td class="LLine" colspan="7" height="1"></td></tr>		
		<tr class="shade" id="DivShowFileUploadOne">
			<td height="30" colspan="5" class="RightTableCaption" align="right"><fmtShipFileUpload:message key="LBL_SELECT_FILE_TO_UPLOAD"/>:</td>
			<td>&nbsp;<html:file property="file"  name="frmShipFileUpload"/></td>			
		<tr id="DivShowFileUploadTwo" ><td class="LLine" colspan="7" height="1"></td></tr>
		<tr id="DivShowFileUploadThree">
			<td colspan="7" height="30" align="center">
			    <fmtShipFileUpload:message key="BTN_UPLOAD" var="varUpload"/>
				<gmjsp:button  name="Submit" value="${varUpload}"  gmClass="button" buttonType="Save" onClick="fnValidateUpload();"/>
			</td>
		</tr>			
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>