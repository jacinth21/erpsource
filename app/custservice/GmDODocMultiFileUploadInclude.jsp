
 <!-- \custservice\GmDODocMultiFileUploadInclude.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.Iterator" %>
<%@ taglib prefix="fmtDODocMultiFileUploadInclude" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import="com.globus.common.beans.GmCommonApplicationBean" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtDODocMultiFileUploadInclude:setLocale value="<%=strLocale%>"/>
<fmtDODocMultiFileUploadInclude:setBundle basename="properties.labels.custservice.GmDODocMultiFileUploadInclude"/>  
<%

String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
    String strWhiteList =  GmCommonClass.parseNull(GmCommonApplicationBean.getApplicationConfig("DO_DOC_WHITELIST"));
%>

<bean:define id="alFileType" name="frmDODocumentUpload" property="alFileType" type="java.util.ArrayList"></bean:define>
<bean:define id="strBtnAccessFl" name="frmDODocumentUpload" property="strBtnAccessFl" type="java.lang.String"></bean:define> 
<%
  String strSubmitDisable = "";
if(!strBtnAccessFl.equals("Y")){
	strSubmitDisable = "true";
}  

strFormName = strFormName == "" ? "frmDODocumentUpload" : strFormName;
int fileTypelen = alFileType.size();

 String strCodeId = "";
String strCodeName = "";

String strOptionString = "";
String strChooseOne = "<option value=\"0\" >[Choose One]</option>";

if(fileTypelen!=0)
{
	for (int i=0; i<fileTypelen; i++)
	{
		HashMap hmFileType = (HashMap)alFileType.get(i);
		
		strCodeId = GmCommonClass.parseNull((String)hmFileType.get("CODEID"));
		strCodeName = GmCommonClass.parseNull((String)hmFileType.get("CODENM"));
		
		strOptionString += " <option value=\""+strCodeId+"\">"+strCodeName+"</option> ";
	}
	strOptionString = strChooseOne + strOptionString;
	
} 
 
%>

<html>
<head>
<TITLE> Globus Medical:Multi File Upload</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmDODocMultiFileUploadInclude.js"></script>
<script>

var lblType = '<fmtDODocMultiFileUploadInclude:message key="LBL_TYPE"/>';
var lblComments = '<fmtDODocMultiFileUploadInclude:message key="LBL_COMMENTS"/>';
var strWhiteList = '<%=strWhiteList%>'
var fileCount = 1;
var optionString = '<%=strOptionString%>';
var subTypeArrLen = '';


</script>
</head>
<BODY leftmargin="20" topmargin="10">

<html:hidden property="fileTypeId"/>
<html:hidden property="fileTypeList"/>


<table border="0" bordercolor="red" width="100%" cellspacing="0" cellpadding="0">					
			<tr><td class="LLine" colspan="5"></td></tr>
				
			<tr>
				<td colspan="5" align="center" height="25">&nbsp;<font style="color: Green;"><b><bean:write name="frmDODocumentUpload" property="message"/></b></font></td>
			</tr>	
			<tr class="shade">
				<td>
					<table border="0" cellspacing="0" cellpadding="0" width="100%" id="UploadFiles">
					 <TBODY>
						<tr>
							<input type="hidden" name="hfilename" />
							<input type="hidden" name="hfileId" />
							<td align="right" height="30" width="80" class="RightTableCaption"><span id="typespan" style="vertical-align: middle"><font color="red">*</font>&nbsp;<fmtDODocMultiFileUploadInclude:message key="LBL_TYPE"/>:&nbsp;</span></td>	
							<td align="left"  height="30" width="80"><span id="fileTypeSpan"  width="70"><gmjsp:dropdown controlName="fileTypeId0" controlId="fileTypeId0"  
								SFFormName="frmDODocumentUpload" SFSeletedValue="fileTypeId" SFValue="alFileType"  
								codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"/></span>
							</td>						
							<td align="right" height="30" width="100" class="RightTableCaption"><span id="titlespan"  style="vertical-align: middle">&nbsp;<fmtDODocMultiFileUploadInclude:message key="LBL_COMMENTS"/>:&nbsp;</span></td>
							<td><span id="fileTitleSpan0">
							    <input type="text" name="fileTitle0" id="fileTitle0" style="width:200px" onfocus="changeBgColor(this,'#AACCE8');" class="InputArea" onblur="changeBgColor(this,'#ffffff');" value=""/>
							    <input type="hidden" name="titleType0" id="titleType0" value=""/>
							  </span></td>
							
							<td align="left"  height="30" ><span id="fileSectionSpan" style=" vertical-align: middle"><input type="file" name="theFile[0]" id="theFile[0]" value=""></span></td>
						</tr>
						</TBODY>
					</table>
				</td>
			</tr>
			<tr><td class="LLine" colspan="5"></td></tr>
			<tr>
				<td>
					<table cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<td colspan="5" height="25"><span id="attachMoreFile"><a href="javascript:fnAddElementMoreFiles('UploadFiles');" id="UploadBtn"><font color="#6699FF"><fmtDODocMultiFileUploadInclude:message key="LBL_ATTACH_MORE_FILES"/></font></a></span></td>
						</tr>
						<tr><td class="LLine" colspan="5"></td></tr>
						<tr>
							<td colspan="5" height="30" align="center">
						
							<fmtDODocMultiFileUploadInclude:message key="BTN_UPLOAD_FILES" var="varUploadFiles"/>
								<gmjsp:button controlId="UploadBtn" buttonType="Save" name="UploadBtn" value="${varUploadFiles}" gmClass="Button" style="height: 22" disabled='<%=strSubmitDisable%>' onClick="fnFileUpload(this.form);" />
						
							</td>
						</tr>
					</table>
				</td>
			</tr>
			

	</table>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>