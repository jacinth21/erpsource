
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtNPIErrorReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtNPIErrorReport:setBundle basename="properties.labels.party.GmNPIErrorReport"/>
<bean:define id="gridData" name="frmNPIProcess" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="intGridSize" name="frmNPIProcess" property="intGridSize" type="java.lang.Integer"> </bean:define>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

 <!-- \custservice\GmNPIErrorReport.jsp -->
<%
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
String strApplDateFmt = strGCompDateFmt;
String strWikiTitle = GmCommonClass.getWikiTitle("NPI_ERROR_RPT");
HashMap hmParam = new HashMap();
hmParam = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmParam"));
String strSearchNum = GmCommonClass.parseNull((String)hmParam.get("SEARCHNPINUM"));
String strSurgeonName = GmCommonClass.parseNull((String)hmParam.get("SEARCHSURGEONNAME"));
String strAccountNm = GmCommonClass.parseNull((String)hmParam.get("SEARCHACCOUNTID"));
String strRepName = GmCommonClass.parseNull((String)hmParam.get("SEARCHREPID"));
String strNPINum = GmCommonClass.parseNull((String)hmParam.get("NPINUM"));
String strSurgeonNm = GmCommonClass.parseNull((String)hmParam.get("SURGEONNAME"));
String strAccountId = GmCommonClass.parseNull((String)hmParam.get("ACCOUNTID"));
String strRepId = GmCommonClass.parseNull((String)hmParam.get("REPID"));
%> 


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>NPI Error Report</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid_form.js "></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmNPIErrorReport.js"></script>
<style type="text/css">
.grid{
	width: 1050px !important;
	height: 550px;
}
#npiFromDt, #npiToDt {
	vertical-align: middle;
}

.hdrcell select {
	font-weight: normal;
}

.hdrcell input {
	font-weight: normal;
}

</style>
<script>
var gridObj = '';
var dateFmt = '<%=strApplDateFmt%>';
var objGridData = '<%=gridData%>';
</script>
</head>

<body leftmargin="20px" topmargin="10px" onload="fnOnPageLoad()">
	<html:form action="/gmNPIProcess.do?method=reportNPIError">
	<html:hidden property="strOpt" name="frmNPIProcess"/>
		<table class="DtTable1055" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="5">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
						<td height="25" class="RightDashBoardHeader">&nbsp;<fmtNPIErrorReport:message key="LBL_NPI_ERROR_REPORT"/></td>
						<td  height="25" class="RightDashBoardHeader" align="right" colspan="4">
							<fmtNPIErrorReport:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
			      			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>',' <%=strWikiTitle%>');" />
			      			</td>
		      		</tr></table>
	      		</td>
			</tr>
			<tr>
				<td class="RightTableCaption" align="right" height="25"><fmtNPIErrorReport:message key="LBL_NPI_NUMBER"/></td>
				<td style="padding-left: 4px;"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
						<jsp:param name="CONTROL_NAME" value="npinum" />
						<jsp:param name="METHOD_LOAD" value="loadSurgeonName&strOpt=npilist" />
						<jsp:param name="WIDTH" value="200" />
						<jsp:param name="CSS_CLASS" value="search" />
						<jsp:param name="CONTROL_NM_VALUE" value="<%=strSearchNum%>" /> 
						<jsp:param name="CONTROL_ID_VALUE" value="<%=strSurgeonNm %>" />
						<jsp:param name="SHOW_DATA" value="40" />
						<jsp:param name="AUTO_RELOAD" value="" />
				     	</jsp:include>
				</td>
				     	
				<td class="RightTableCaption" align="right"><fmtNPIErrorReport:message key="LBL_SURGEON_NAME"/></td>
				<td colspan="2" style="padding-left: 4px;"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
						<jsp:param name="CONTROL_NAME" value="surgeonname" />
						<jsp:param name="METHOD_LOAD" value="loadSurgeonName&strOpt=surgeonlist" />
						<jsp:param name="WIDTH" value="200" />
						<jsp:param name="CSS_CLASS" value="search" />
						<jsp:param name="CONTROL_NM_VALUE" value="<%=strSurgeonName%>" /> 
						<jsp:param name="CONTROL_ID_VALUE" value="<%=strNPINum %>" />
						<jsp:param name="SHOW_DATA" value="40" />
						<jsp:param name="AUTO_RELOAD" value="" />
				     	</jsp:include>
				</td>
			</tr>
			<tr><td class="LLine" height="1px" colspan="5"></td></tr>
			<tr class="oddshade">
				<td class="RightTableCaption" align="right" height="25"><fmtNPIErrorReport:message key="LBL_SALES_REP"/></td>
				<td style="padding-left: 4px;">
					<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="repid" />
					<jsp:param name="METHOD_LOAD" value="loadSalesRepList&searchType=LikeSearch" />
					<jsp:param name="WIDTH" value="200" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="CONTROL_NM_VALUE" value="<%=strRepName%>" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="<%=strRepId %>" />
					<jsp:param name="SHOW_DATA" value="40" />
					<jsp:param name="AUTO_RELOAD" value="" />
					</jsp:include>
				</td>	
				
				<td class="RightTableCaption" align="right"><fmtNPIErrorReport:message key="LBL_ACCOUNT"/></td>
				<td colspan="2" style="padding-left: 4px;">
					<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="accountid" />
					<jsp:param name="METHOD_LOAD" value="loadAccountList&searchType=LikeSearch" />
					<jsp:param name="WIDTH" value="200" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="CONTROL_NM_VALUE" value="<%=strAccountNm%>" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="<%=strAccountId %>" />
					<jsp:param name="SHOW_DATA" value="40" />
					<jsp:param name="AUTO_RELOAD" value="" />					
					</jsp:include>
				</td>				
			</tr>
			<tr><td class="LLine" height="1px" colspan="5"></td></tr>
			
			<tr>
				<td class="RightTableCaption" align="right" height="30"><fmtNPIErrorReport:message key="LBL_ORDER_ID"/></td>
				<td style="padding-left: 5px;">&nbsp;<html:text property="txnid" styleClass="InputArea" name="frmNPIProcess" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" size="20"/></td>
				
				<td class="RightTableCaption" align="right" height="25"><fmtNPIErrorReport:message key="LBL_DATE_RANGE"/>&nbsp;</td>
				<td>
					<table>
						<tr>
							<td align="right" height="25"><fmtNPIErrorReport:message key="LBL_FROM"/></td>
							<td>&nbsp;<gmjsp:calendar SFFormName="frmNPIProcess" controlName="npiFromDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;</td>
							<td align="right" height="25"><fmtNPIErrorReport:message key="LBL_TO"/></td>
							<td>&nbsp;<gmjsp:calendar SFFormName="frmNPIProcess" controlName="npiToDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/></td>
						</tr>
					</table>
				</td>
				<td width="10%"><gmjsp:button name="Btn_Load" value="Load" gmClass="button" onClick="javascript:fnLoad();" buttonType="Load" /></td>
			</tr>
			<tr><td class="LLine" height="1px" colspan="5"></td></tr>
			<%if(intGridSize >= 1){ %>
				<tr> 
					<td colspan="5">
						<div id="dataGridDiv" class="grid" ></div>
					</td>
				</tr>
			
				<tr>
                <td colspan="5" height="25" align="center">
                <div class='exportlinks'>Export Options: <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
                                onclick="fnExport('excel');">Excel</a></div>
                </td>
				</tr>
			<%}else{%>
				<td height="25" colspan="5" align="center"><font color="blue"><fmtNPIErrorReport:message key="LBL_NO_DATA"/></font></td>
			<%} %> 
		
			<div id="npidtls" style="display: none;">
					<jsp:include page="/custservice/GmNPIErrorEdit.jsp" >															 
						<jsp:param name="SCREENTYPE" value='Modify' />
					</jsp:include>
			</div> 
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</body>
</html>