
<%@ page language="java"%>

<%@ include file="/common/GmHeader.inc" %>

<!--GmLoanerReconciledDetails.jsp  -->
<%


if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
}
%>
<%@ taglib prefix="fmtLoanerReconciledDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtLoanerReconciledDetails:setLocale value="<%=strLocale%>"/>
<fmtLoanerReconciledDetails:setBundle basename="properties.labels.custservice.GmLoanerRecon"/> 
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<TR><TD colspan=3 height=1 class=Line></TD></TR>	
	<tr>
		<td height="25" class="ShadeRightTableCaption"><a
			href="javascript:fnShowFilters('tabRecon');"><IMG
			id="tabReconimg" border=0 src="<%=strImagePath%>/minus.gif"></a>&nbsp;<fmtLoanerReconciledDetails:message key="IMG_RECONCILE"/></td>
	</tr>
	<tr id="tabRecon" >
		<td>
		<div id="div_rec" style="height:200px"></div>
		</td>
	</tr>
	<tr></tr>
	<tr>
		<td colspan=3 height=1 class=Line></td>
	</tr>	
</table>