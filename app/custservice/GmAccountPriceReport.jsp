<!-- \CS Label changes\custservice\GmAccountPriceReport.jsp -->

 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmAccountPriceReport.jsp
 * Desc		 		: This screen is used for the Account Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtAccountPriceReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtAccountPriceReport:setLocale value="<%=strLocale%>"/>
<fmtAccountPriceReport:setBundle basename="properties.labels.custservice.GmAccountPriceReport"/>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strApplDateFmt = strGCompDateFmt;
	String strRptFmt = "{0,date,"+strApplDateFmt+"}";
	String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");

	String strApplCurrFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplCurrFmt"));
	String strCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	String strAccCurrSymb = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_SYMB"));
	strCurrSign = strAccCurrSymb.equals("")?strCurrSign:strAccCurrSymb;
	String strCurrFmt = "{0,number,"+strCurrSign+strApplCurrFmt+"}";
	String strDiscountFmt = "{0,number,"+strApplCurrFmt+"}";
	GmResourceBundleBean gmResourceBundleBeanlbl = 
	    GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmAccountPriceReport", strSessCompanyLocale);
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBeanFlg =
			GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	
	int intSize = 0;
	HashMap hmLoop = new HashMap();
	HashMap hcboVal = null;

	String strAccId = "";
	String strPartNum = "";
	String strHeader = "";
	String strName ="";
	String strHeadName ="";
	String strDiscEnableFl ="";
	Double dbLprice=0.0;

	String strProjId = "";
	String strQueryBy = "";
	String strLikeOption = "";
	String strWikiTitle = "Account_Pricing_Report";
	String strWikiNm = "";
	String strWikiSufNm = "";
	String strVoidFl ="";
	String strBtnDisabled ="true";
	
	String strParam = GmCommonClass.parseNull((String)request.getAttribute("hMode"));
	String strmodeCheck = GmCommonClass.parseNull((String)request.getAttribute("hmodeCheck"));
	String strType = GmCommonClass.parseNull((String)request.getAttribute("hType"));
	String strOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
	ArrayList alAccount = new ArrayList();
	ArrayList alProject = new ArrayList();
	ArrayList alSystem = new ArrayList();
	ArrayList alLikeOptions = new ArrayList();
	ArrayList alProjSys = new ArrayList();
	ArrayList alResult = new ArrayList();
	ArrayList alYesNo = new ArrayList();

	HashMap hmReturn = new HashMap();
	HashMap hmResult = new HashMap();
	
	hmReturn = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturn"));
	alAccount = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ACCOUNTLIST"));
	alProject = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("PROJLIST"));
	alSystem = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SYSTEMLIST"));
	alLikeOptions = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("LIKEOPTIONS"));
	alResult = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("REPORT"));
	alYesNo = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("YES/NO"));
	alProjSys = alSystem;
	if (strParam.equals("Reload"))
	{
		hmResult = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmResult"));
		strAccId = GmCommonClass.parseNull((String)request.getAttribute("hAccId"));
		strProjId = GmCommonClass.parseNull((String)request.getAttribute("hProjId"));
		strQueryBy = GmCommonClass.parseNull((String)request.getAttribute("hQueryBy"));
		strVoidFl = GmCommonClass.parseNull((String)request.getAttribute("hVoidFl"));
		
		if(strQueryBy.equals("Project Name"))
			alProjSys = alProject;
		else
			alProjSys = alSystem;
		strPartNum = GmCommonClass.parseNull((String)request.getAttribute("hPartNum"));
		strLikeOption = GmCommonClass.parseNull((String)request.getAttribute("hLikeOption"));
		//strVoidFl = GmCommonClass.parseNull((String)request.getAttribute("hVoidFl"));
	}
	if (strType.equals("Account")){
		strName =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_REP_ACCOUNT"));
		strWikiNm = "Account_Pricing_";
		strHeadName =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ACCOUNT"));
		
	}
	else if (strType.equals("GPO")){
		strName =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_GROUP_PRICE_BOOK"));
		strWikiNm = "Price_Book_Pricing_";
		strHeadName =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_GROUP_PRICE_BOOK"));
	}
	else if (strType.equals("ICT")){
		strName =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ICT"));
		strWikiNm = "ICT_Pricing_";
	}
	if (strOpt.equals("Batch"))
	{
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_UPDATE"));
		strWikiSufNm = "Update";
	}
	else
	{
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_REPORT"));
		strWikiSufNm = "Report";
	}

	if(!strWikiNm.equals("")){
	  strWikiTitle = GmCommonClass.parseNull((String)GmCommonClass.getWikiTitle(strWikiNm+strWikiSufNm));
	  }
	//PC-5504 - to disabled/Enable the select price button based on the strButtonAccessFl value
	String strBtnAccessFl = GmCommonClass.parseNull((String)request.getAttribute("BTNACCESSFL"));
	if(strBtnAccessFl.equals("Y")){
		strBtnDisabled ="false";
	}
	//PC-5504 to set default value in void Drop down 
	if(strVoidFl.equals("")){
		strVoidFl="80131";//80131 - N Code id- set default value as N on Void dropdown
	}
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Account Price <%=strHeader %> </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/PhoneOrder.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmAccountPriceReport.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmParentRepInfo.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />

<script>
var lblName = '<fmtAccountPriceReport:message key="LBL_NAME"/>';
var type = '<%=strType%>';
var TypeName = '<%=strName%>'; 
var projLen = <%=alProject.size()%>;
var systemLen = <%=alSystem.size()%>;
var enablePriceLabel = 'Y';
var strmodeCheck = '<%=strmodeCheck%>';
var hmSize = '<%=alResult.size()%>'
var varBtnAccessFl ='<%=strBtnAccessFl%>';
<%
 intSize = alProject.size();
hcboVal = new HashMap();
for (int i=0;i<intSize;i++)
{
	hcboVal = (HashMap)alProject.get(i);
%>
var projArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>

<%
intSize = alSystem.size();
hcboVal = new HashMap();
for (int i=0;i<intSize;i++)
{
	hcboVal = (HashMap)alSystem.get(i);
%>
var systemArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnSetDefaultValues();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmAccountPriceReportServlet">
<input type="hidden" name="hColumn" value="ID">
<input type="hidden" name="hId" value="<%=strProjId%>">
<input type="hidden" name="hMode" value="">
<input type="hidden" name="PartNum" value="">
<input type="hidden" name="hOpt" value="<%=strOpt%>">
<input type="hidden" name="hAccId" value="<%=strAccId %>">
<input type="hidden" name="hQueryBy" value="<%=strQueryBy %>">
<input type="hidden" name="hType" value="<%=strType%>">
<input type="hidden" name="hInputStr" value="">
	<table class="DtTable900" cellspacing="0" cellpadding="0">
		<tr><td height="25" class="RightDashBoardHeader" colspan="1">&nbsp;<%=strHeadName %> Pricing <%=strHeader %></td>
		<td height="25" class="RightDashBoardHeader" align="right">
		<fmtAccountPriceReport:message key="IMG_ALT_HELP" var="varHelp"/>
				<img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
					height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
			    </td>
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td height="25" class="RightTableCaption" align="right">&nbsp;<%=strName %><fmtAccountPriceReport:message key="LBL_NAME"/>:</td>
			<td>&nbsp;<gmjsp:dropdown width="330"  controlName="Cbo_AccId" onBlur="fnCallAcBlur(this)" seletedValue="<%= strAccId %>"	tabIndex="1"  value="<%= alAccount%>" defaultValue= "[Choose One]" />
			</td>
		</tr>
		<tr><td colspan="2" class="LLine"></td></tr>
		<tr class="Shade">
			<td class="RightTableCaption" HEIGHT="24" align="right"><%=strName%><fmtAccountPriceReport:message key="LBL_ID"/>:</td>
			<td class="RightText">&nbsp;<input type="text" size="8" value="" name="Txt_AccId" class="InputArea" tabindex=1 onFocus="changeBgColor(this,'#AACCE8');" onBlur="javascript:fnCallAcIdBlur(this)"> &nbsp;&nbsp;<span class="RightTextBlue"></span></td>
		</tr>
		<tr><td colspan="2" class="LLine"></td></tr>
		<tr>
			<td colspan="2">
				<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" id="tabAcSum" style="display:none">
				<tbody style="display:table;width:100%;">
					<tr height="25">
						<td class="RightTableCaption" align="Right">
						<fmtAccountPriceReport:message key="LBL_REGION" var="varRegion"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varRegion}:" td="false"/></td>
						<td id="Lbl_Regn" class="RightText"></td>
						<td class="RightTableCaption" align="Right">
						<fmtAccountPriceReport:message key="LBL_AREA_DIRECTOR" var="varAreaDirector"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varAreaDirector}:" td="false"/></td>
						<td id="Lbl_AdName" class="RightText"></td>
						<td class="RightTableCaption" align="Right">
						<fmtAccountPriceReport:message key="LBL_DISTRIBUTOR" var="varDistributor"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varDistributor}:" td="false"/></td>
						<td id="Lbl_DName" class="RightText"></td>
					</tr>
					<tr><td colspan="6" class="LLine"></td></tr>
					<tr class="Shade" height="25">
						<td class="RightTableCaption" align="Right">
						<fmtAccountPriceReport:message key="LBL_TERRITORY" var="varTerritory"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varTerritory}:" td="false"/></td>
						<td id="Lbl_TName" class="RightText"></td>
						<td class="RightTableCaption" align="Right">
						<fmtAccountPriceReport:message key="LBL_REP" var="varRep"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varRep}:" td="false"/></td>
						<td id="Lbl_RName" class="RightText"></td>
						<td class="RightTableCaption" align="Right">
						<fmtAccountPriceReport:message key="LBL_GROUP_PRICE_BOOK" var="varGroupPriceBook"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varGroupPriceBook}:" td="false"/></td>
						<td id="Lbl_PName" class="RightText"></td>
					</tr>
					<tr><td colspan="6" class="LLine"></td></tr>
					<!-- MNTTASK:6074 - Account Affiliation Start code --> 
					  <tr height="25">
						<td class="RightTableCaption" align="Right">
						<fmtAccountPriceReport:message key="LBL_IDN" var="varIDN"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varIDN}:" td="false"/></td>
						<td id="Lbl_GPRAffName" class="RightText"></td>
						<td class="RightTableCaption" align="Right">
						<fmtAccountPriceReport:message key="LBL_GPO" var="varGPO"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varGPO}:" td="false"/></td>
						<td id="Lbl_HlthCrAffName" class="RightText"></td>
						<td class="RightTableCaption" align="Right">
						<fmtAccountPriceReport:message key="LBL_CONTRACT" var="varContract"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varContract}:" td="false"/></td>
						<td id="Lbl_ContractType" class="RightText"></td>
					</tr> 
					<!-- PC-2376  Add new fields in Account Pricing Report -->
					 <tr><td colspan="6" class="LLine"></td></tr>
					 <tr class="Shade" height="25">
						<td class="RightTableCaption" align="Right">
						<fmtAccountPriceReport:message key="LBL_RPC" var="varRpc"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varRpc}:" td="false"/></td>
						<td id="Lbl_RpcName" class="RightText"></td>
						<td class="RightTableCaption" align="Right">
						<fmtAccountPriceReport:message key="LBL_HEALTH" var="varHealth"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varHealth}:" td="false"/></td>
						<td id="Lbl_HealthName" class="RightText"></td>
						<td></td>
						<td></td>
					 </tr> 
					 <!-- PC-2376  Add new fields in Account Pricing Report-->
					<!-- Account Affiliation End code -->
					<tr><td colspan="6" class="LLine"></td></tr>
					</tbody>
				</table>
			</td>
		</tr>
		
		<tr  class="Shade">
			<td colspan="2">
				<div id="parentDiv"></div>
			</td>
		</tr>
				
<%
		if (strOpt.equals("Batch")){
%>
		<tr>
			<td class="RightTableCaption" align="right"><fmtAccountPriceReport:message key="LBL_ENTER_DATA_FOR_BATCH_UPLOAD"/>:</td>
			<td valign="absmiddle">&nbsp;<textarea name="Txt_Batch" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=23 rows=25 cols=40 value=""></textarea></td>
		</tr>
		<tr><td colspan="2" class="LLine"></td></tr>
		<tr>
			<td height="25" align="center" colspan="2">
			<fmtAccountPriceReport:message key="BTN_RESET" var="varReset"/>
				<gmjsp:button value="&nbsp;${varReset}&nbsp;" name="Btn_Reset" gmClass="button" onClick="fnReset();" buttonType="Save" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<fmtAccountPriceReport:message key="BTN_SUBMIT" var="varSubmit"/>
				<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" name="Btn_Submit" gmClass="button" onClick="fnBatchUpdate();" buttonType="Save" />
			
			</td>
		</tr>

<%
		}else{
			if(strType.equals("Account")){						
%>
		<tr class="RightTableCaption">
			<td height="25" class="RightTableCaption" align="right"><fmtAccountPriceReport:message key="LBL_QUERY_BY_PROJECTS"/>:</td>
			<td>&nbsp;	<select name="Cbo_QueryBy" class="RightText" tabindex="1" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" onChange="javascript:fnCallType(this);">
					    <option value="System Name" ><fmtAccountPriceReport:message key="LBL_SYSTEM_NAME" />
					    <option value="Project Name"><fmtAccountPriceReport:message key="LBL_PROJECT_NAME"/></option>
				        </select>		
				&nbsp;&nbsp;
				<gmjsp:dropdown controlName="Cbo_ProjId"  seletedValue="<%= strProjId %>" 	
								tabIndex="2"  value="<%= alProjSys%>" defaultValue= "[Choose One]" /> 
				&nbsp;&nbsp;
				<fmtAccountPriceReport:message key="BTN_LOAD" var="varLoad"/>
				<gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="return fnGo();" tabindex="25" buttonType="Load" />&nbsp;
			</td>
		</tr>
		<tr class="Shade">
		<td class="RightTableCaption"  align = "right" > &nbsp;<fmtAccountPriceReport:message key="LBL_PART_NUMBER"/>:</td>
						<td>&nbsp;<input type="text" size="30" value="<%=strPartNum%>" name="hPartNum" class="InputArea"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=2>
							&nbsp;&nbsp;<gmjsp:dropdown controlName="Cbo_Search"  seletedValue="<%= strLikeOption%>" 	
									tabIndex="3"  value="<%=alLikeOptions%>" codeId = "CODEID" codeName = "CODENM" 	/>&nbsp;&nbsp;&nbsp;
								<!-- 	PC-5504 - Add void drop down with Y ANd N value  -->
									<b><fmtAccountPriceReport:message key="LBL_VOID"/>:</b>&nbsp;
									<gmjsp:dropdown controlName="Cbo_Void" tabIndex="4"  value="<%=alYesNo%>" seletedValue="<%=strVoidFl%>" codeId = "CODEID" codeName = "CODENMALT"  width="75"/>
						</td>
     	</tr>

		<% 
		}
		else
		{
        %>			
			<tr class="Shade">
			<td height="25" class="RightTableCaption" align="right"><fmtAccountPriceReport:message key="LBL_PROJECTS"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="Cbo_ProjId"  seletedValue="<%= strProjId %>" 	
								tabIndex="1"  value="<%= alProject%>" defaultValue= "[Choose One]" />
				&nbsp;&nbsp;&nbsp;&nbsp;
				<fmtAccountPriceReport:message key="BTN_LOAD" var="varLoad"/>
				<gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="return fnGo();" tabindex="25" buttonType="Load" />&nbsp;
			</td>
		</tr>
			<!-- 	PC-5504 - Add void drop down with Y ANd N value  -->
		<tr class="Shade"><td colspan="2" class="LLine"></td></tr>
		<tr class="">
		<td class="RightTableCaption"  align = "right"  height="25"> &nbsp;<fmtAccountPriceReport:message key="LBL_VOID"/>:</td>
		<td >&nbsp;<gmjsp:dropdown controlName="Cbo_Void" tabIndex="4"  value="<%=alYesNo%>" seletedValue="<%=strVoidFl%>" codeId = "CODEID" codeName = "CODENMALT" width="75"/>
						</td>
     	</tr>
		<%
		}
		%>	
		<tr class="Shade"><td colspan="2" class="LLine"></td></tr>
<!-- 	
		<tr>
			<td height="25" class="RightTableCaption" align="right">Part Number:</td>
			<td>&nbsp;<input type="text" size="30" value="<%=strPartNum%>" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=8>
			<gmjsp:button value="&nbsp;Go&nbsp;" gmClass="button" onClick="fnGo();" tabindex="25" buttonType="Load" />&nbsp;
			</td>
		</tr>
 -->		
<%
		}
%>		

		<tr>
			<td align="center" colspan="2">
				<display:table name="REPORT" export="true" id="currentRowObject" class="its" decorator="com.globus.displaytag.beans.DTPriceReportWrapper">
					<display:setProperty name="export.excel.filename" value="Account Price Report.xls" /> 
				<%-- 	<display:column property="CHECKBOX" title="" class="aligncenter" style="width:50px"></display:column> --%>
					<fmtAccountPriceReport:message key="LBL_PART_NUMBER" var="varPartNumber"/>
					<display:column property="ID" title="${varPartNumber}<br>" sortable="true" style="width:150px"/>
	  				<fmtAccountPriceReport:message key="LBL_DESCRIPTION" var="varDescription"/>
	  				<display:column property="PDESC" title="${varDescription}" escapeXml="true" sortable="true" />
	  				<fmtAccountPriceReport:message key="LBL_UNIT_PRICE" var="varUnitPrice"/>
	  				<display:column property="PRICE" title="${varUnitPrice}" class="alignright"  format="<%=strCurrFmt%>" sortable="true"/>
	  				<%if(!strmodeCheck.equals("BatchUpdate")){ %>
	  				<fmtAccountPriceReport:message key="LBL_PRICE_REQID" var="varPriceReqId"/>
	  				<display:column property="PRICE_REQ_ID" title="${varPriceReqId}" class="alignleft"/>
	  				<%} %>
	  				<%if(strDiscEnableFl.equals("YES")){ %>
	  				<fmtAccountPriceReport:message key="LBL_DISCOUNT" var="varDiscount"/>
                    <display:column property="DISC" title="${varDiscount}" class="alignright" format="<%=strDiscountFmt%>" sortable="true"/>
                    <%} %>
	  				<fmtAccountPriceReport:message key="LBL_CREATED_BY" var="varCreatedBy"/>
	  				<display:column property="CUSER" title="${varCreatedBy}<BR>" sortable="true" class="alignleft"/>
	  				<fmtAccountPriceReport:message key="LBL_CREATED_DATE" var="varCreatedDate"/>
	  				<display:column property="CDATE" title="${varCreatedDate}<BR>" sortable="true" class="alignleft" format= "<%=strRptFmt%>"  />
	  				<fmtAccountPriceReport:message key="LBL_LAST_UPDATED_BY" var="varLastUpdatedBy"/>
	  				<display:column property="LUSER" title="${varLastUpdatedBy}<BR>" sortable="true" class="alignleft"/>
	  				<fmtAccountPriceReport:message key="LBL_LAST_UPDATED_DATE" var="varLastUpdatedDate"/>
	  				<display:column property="LDATE" title="${varLastUpdatedDate}<BR>" sortable="true" class="alignleft" format= "<%=strRptFmt%>" />
	  				<fmtAccountPriceReport:message key="LBL_VOID" var="varVoid"/> <!-- 	PC-5504 - Added void flag column -->
	  				<display:column property="VOID_FL" title="${varVoid}" sortable="true" class="aligncenter"/>
	  				<fmtAccountPriceReport:message key="LBL_HISTORY" var="varHistory"/>
	  				<display:column property="HISFL" title="${varHistory}" sortable="true" class="aligncenter"/>
	  				
		  		</display:table>	
		  </td>
		</tr>
	<!-- 	PC-5504 - Added void and revert Button -->
		<% if(alResult.size() >0){ %> 
		<tr> <td align="center" colspan="2">
		<fmtAccountPriceReport:message key="BTN_REVERT" var="varRevert"/>
		<gmjsp:button value="&nbsp;${varRevert}&nbsp;" name="BTN_Revert" gmClass="button" onClick="fnSave();" buttonType="Save" disabled="<%=strBtnDisabled%>" />
		<fmtAccountPriceReport:message key="BTN_VOID" var="varVoid"/>
		<gmjsp:button  value="&nbsp;${varVoid}&nbsp;" name="BTN_Void" gmClass="button" onClick="fnVoid();" buttonType="Save" disabled="<%=strBtnDisabled%>" />
		</td>
		</tr>
		<%} %> 
</table>
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
