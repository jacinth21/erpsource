<!-- \CS Label changes\custservice\GmAccountShipSetup.jsp -->
<%
/*******************************************************************************
 * File		 	: GmAccountShipSetup.jsp
 * Version	 	: 1.0
 * author		: Jignesh Shah
*******************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtAccountShipSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<fmtAccountShipSetup:setLocale value="<%=strLocale%>"/>
<fmtAccountShipSetup:setBundle basename="properties.labels.custservice.GmAccountShipSetup"/>
<%
    String strAccountsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
	String strWikiTitle = GmCommonClass.getWikiTitle("SHIPPING_SETUP");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Account Ship Setup</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strAccountsJsPath%>/GmAccountShipSetup.js"></script>
</HEAD>
<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmAccountShipSetup.do" >
<html:hidden property="strOpt" />
<html:hidden property="accID" />	
	<table border="0" class="DtTable500" cellspacing="0" cellpadding="0">	
		<tr>
			<td  height="25" class="RightDashBoardHeader" ><fmtAccountShipSetup:message key="LBL_SHIPPING_SETUP"/></td>
            <td  height="25" class="RightDashBoardHeader">
            <fmtAccountShipSetup:message key="IMG_ALT_HELP" var="varHelp"/>
            <img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
            </td>   
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr class="shade">
			<td class="RightTableCaption" align="right" HEIGHT="25" width="200"><font color="red">*</font>&nbsp;<fmtAccountShipSetup:message key="LBL_NAME"/>:</td>
			<td>&nbsp;<html:text property="shipName" maxlength="200" size="30" styleClass="InputArea"
				onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
			</td>	
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr>
			<td class="RightTableCaption" align="right" HEIGHT="25" width="200"><fmtAccountShipSetup:message key="LBL_ATTENTION_TO"/>:</td>
			<td>&nbsp;<html:text property="shipAttnTo" maxlength="200" size="30" styleClass="InputArea"
				onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
			</td>	
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr class="shade">
			<td class="RightTableCaption" align="right" HEIGHT="25" width="200"><font color="red">*</font>&nbsp;<fmtAccountShipSetup:message key="LBL_ADDRESS_1"/>:</td>
			<td>&nbsp;<html:text property="shipAdd1" maxlength="200" size="30" styleClass="InputArea"
				onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
			</td>	
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr>
			<td class="RightTableCaption" align="right" HEIGHT="25" width="200"><fmtAccountShipSetup:message key="LBL_ADDRESS_2"/>:</td>
			<td>&nbsp;<html:text property="shipAdd2" maxlength="200" size="30" styleClass="InputArea"
				onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
			</td>	
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr class="shade">
			<td class="RightTableCaption" align="right" HEIGHT="25" width="200"><font color="red">*</font>&nbsp;<fmtAccountShipSetup:message key="LBL_CITY"/>:</td>
			<td>&nbsp;<html:text property="shipCity" maxlength="200" size="30" styleClass="InputArea"
				onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
			</td>	
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr>
			<td class="RightTableCaption" align="right" HEIGHT="25" width="200"><font color="red">*</font>&nbsp;<fmtAccountShipSetup:message key="LBL_STATE"/>:</td>
			<!-- Struts tag lib code modified for JBOSS migration changes -->
			<td>&nbsp;<gmjsp:dropdown controlName="shipState" SFFormName="frmAccountShipSetup" SFSeletedValue="shipState" SFValue="alState"	
					codeId = "CODEID"  codeName = "CODENM" tabIndex="27"  width="170"  defaultValue= "[Choose One]" />
			</td>	
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr class="shade">
			<td class="RightTableCaption" align="right" HEIGHT="25" width="200"><font color="red">*</font>&nbsp;<fmtAccountShipSetup:message key="LBL_COUNTRY"/>:</td>
			<!-- Struts tag lib code modified for JBOSS migration changes -->
			<td>&nbsp;<gmjsp:dropdown controlName="shipCountry" SFFormName="frmAccountShipSetup" SFSeletedValue="shipCountry" SFValue="alCountry"	
					codeId = "CODEID"  codeName = "CODENM" tabIndex="27"  width="170"  defaultValue= "[Choose One]" />
			</td>	
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr>
			<td class="RightTableCaption" align="right" HEIGHT="25" width="200"><font color="red">*</font>&nbsp;<fmtAccountShipSetup:message key="LBL_PREFERRED_CARRIER"/>:</td>
			<!-- Struts tag lib code modified for JBOSS migration changes -->
			<td>&nbsp;<gmjsp:dropdown controlName="carrier" SFFormName="frmAccountShipSetup" SFSeletedValue="carrier" SFValue="alCarrier"	
					codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" />
			</td>	
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr class="shade">
			<td class="RightTableCaption" align="right" HEIGHT="25" width="200"><font color="red">*</font>&nbsp;<fmtAccountShipSetup:message key="LBL_ZIP_POSTAL_CODE"/>:</td>
			<td>&nbsp;<html:text property="shipZip" maxlength="200" size="15" styleClass="InputArea"
				onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
			</td>	
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr>
			<td colspan="2" HEIGHT="40" align="center">
			<fmtAccountShipSetup:message key="LBL_SUBMIT" var="varSubmit"/>
				<gmjsp:button style="width: 5em"  value="${varSubmit}" gmClass="button" onClick="fnSubmit();" buttonType="Save" />
				<fmtAccountShipSetup:message key="LBL_CLOSE" var="varClose"/>
				<gmjsp:button style="width: 5em"  value="${varClose}" gmClass="button" onClick="fnClose();" buttonType="Load" />
			</td>	
		</tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>