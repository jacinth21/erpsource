<!-- \CS Label changes\custservice\GmBackOrderProcess.jsp -->
 <%
/**********************************************************************************
 * File		 		: GmPOReportByPart.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtBackOrderProcess" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtBackOrderProcess:setLocale value="<%=strLocale%>"/>
<fmtBackOrderProcess:setBundle basename="properties.labels.custservice.GmBackOrderProcess"/>

<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strCustServiceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	String strhAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strhMode = GmCommonClass.parseNull((String)request.getAttribute("hMode"));
	String strhRptType = GmCommonClass.parseNull((String)request.getAttribute("hRptType"));
	String strAgingType = GmCommonClass.parseNull((String)request.getAttribute("hAgType")); 
	String strTypeVal = GmCommonClass.parseNull((String)request.getAttribute("hTypeVal"));
	String strSource = GmCommonClass.parseNull((String)request.getAttribute("hSource"));
	String strInvRptAccFl  = GmCommonClass.parseNull((String)request.getAttribute("BackOrdRptAccFl"));
	String strDeptId = GmCommonClass.parseNull((String)session.getAttribute("strSessDeptId"));
	String strBtnAccessFl = GmCommonClass.parseNull((String)request.getAttribute("strBtnAccessFl"));
	String strAccessLvl = GmCommonClass.parseNull((String)session.getAttribute("strSessAccLvl"));
	String strTagId = GmCommonClass.parseNull((String)request.getAttribute("strTagId"));
	String strScreen = GmCommonClass.parseNull((String)request.getAttribute("hScreen"));
	String strRefId = GmCommonClass.parseNull((String)request.getAttribute("hRefId"));
	ArrayList alOrderList = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ORDERLIST"));
	ArrayList alSourceList = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SOURCELIST"));
	ArrayList alTypeList = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("TYPELIST"));
	ArrayList alCboList = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("CBOLIST"));
	int intAccessLvl = Integer.parseInt(strAccessLvl);
	String strWikiTitle = GmCommonClass.parseNull((String)GmCommonClass.getWikiTitle("BACK_ORDER_REPORT"));
	String strBtnDisable = strBtnAccessFl.equalsIgnoreCase("Y")? "false" : "true";
	
	String strReqDate = "";
	if(!(strhRptType.equals("50261"))){
		strReqDate="Required Date";
	}

	String strPOColHead = "";
	if(!(strhRptType.equals("50264")||strhRptType.equals("50265"))){
		strPOColHead="Qty in<BR>PO";
	}

	String strShade = "";
	String strOrderId = "";
	String strParentOrderId = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strPoPendQty = "";
	int intAllCnt = 0;
	int intSize = 0;
	String strQtyPlaced = "";
	String strDate = "";
	
	String strTransDate="";
	
	String strOrdAmt = "";
	String strAccId = "";
	String strRepNm = "";
	
	String strNextId = "";
	int intCount = 0;
	int intBOQty = 0;
	int intTemp = 0;
	String strLine = "";
	boolean blFlag = false;
	String strPartNumId = "";
	StringBuffer sbTemp = new StringBuffer();
	StringBuffer sbParts = new StringBuffer();
	String strOrdCallFlag = "";
	String strOrdCallFlagInv = "";
	String strTXNtype = "";
	String strCollapseImage = strImagePath+"/plus.gif";
	String strApplDateFmt = strGCompDateFmt;
	String strAccCurrency = "";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: BO Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">	
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/sereen.css">
<script language="JavaScript" src="<%=strCustServiceJsPath%>/GmBackOrderProcess.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
var expandNum = 1;
var rpt = '<%=strhRptType%>';
var agType = '<%=strAgingType%>';
var typeVal = '<%=strTypeVal%>';
var source = '<%=strSource%>';
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmOrderItemServlet">
<input type="hidden" name="hAction" value="ProcessBO">
<input type="hidden" name="hOrderId" value="">
<input type="hidden" name="hPartNums" value="">
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="hRptType" value="<%=strhRptType%>">
<input type="hidden" name="hScreen" value="<%=strScreen%>">
<input type="hidden" name="hRefId" value="<%=strRefId%>">

	<table border="0" width="dttable1200" cellspacing="0" cellpadding="0">
		<tr><td colspan="10" height="1" bgcolor="#666666"></td></tr>
		<tr>
		 <td class="Line" width="1" rowspan="6"></td>
			<td height="30" class="RightDashBoardHeader" colspan="1"><fmtBackOrderProcess:message key="LBL_BACK_ORDER_REPORT"/>
			</td>
			<%if(strScreen.equals("PROCESS_TXN_SCN")){ %>
			<td class="RightDashBoardHeader" width="25%">
		
			        <img align="right" id='imgClose' style='cursor:hand' src='<%=strImagePath%>/close_1.png' title='Return to Process Transaction' width='16' height='16' onClick="javascript:fnReloadCN('<%=strRefId%>');" />
			</td>
			<%}else{%>
			<td class="RightDashBoardHeader" width="25%">
			<fmtBackOrderProcess:message key="IMG_ALT_HELP" var="varHelp"/>
			        <img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
			<%}%>
			 <td class="Line" width="1" rowspan="6"></td>
		</tr>
		<tr><td colspan="2">
			<table cellspacing="0" cellpadding="0" width=100%>
			<tr><td colspan="5">
				<jsp:include page="/sales/GmSalesFilters.jsp" >
				<jsp:param name="FRMNAME" value="frmAccount" />
				<jsp:param name="HACTION" value="<%=strhAction%>" />
				<jsp:param name="HIDEBUTTON" value="HIDEGO" />
				</jsp:include>
			</td></tr>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<tr> 
				<td class="RightTableCaption" align="right" width="15%">
				<fmtBackOrderProcess:message key="LBL_REPORT_TYPE" var="varReportType"/>
						    <gmjsp:label type="RegularText"  SFLblControlName="&nbsp;${varReportType}:" td="false"/>
				 </td>
			     <td>&nbsp; <gmjsp:dropdown controlName="Cbo_Type"   seletedValue="Cbo_Type"
					tabIndex="2"  value="<%=alOrderList%>" codeId = "CODEID"  codeName = "CODENM" onChange="fnChange(this.value)"/>	
			     </td>
			     <td class="RightTableCaption" align="right" width="10%">
			     <fmtBackOrderProcess:message key="LBL_AGING_TYPE" var="varAgingType"/>
			     <gmjsp:label type="RegularText"  SFLblControlName="&nbsp;${varAgingType}:" td="false"/>
				 </td>
				 <td>&nbsp;<select name="Cbo_Aging" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
					<option value="0" ><fmtBackOrderProcess:message key="LBL_CHOOSE_ONE"/>
					<option value="00" ><fmtBackOrderProcess:message key="LBL_ALL"/>
					<option value="14" ><fmtBackOrderProcess:message key="LBL_0_14DAYS"/>
					<option value="21" ><fmtBackOrderProcess:message key="LBL_15_21_DAYS"/>
					<option value="21x" ><fmtBackOrderProcess:message key="LBL_MORE_THAN_21_DAYS"/>
				</select>
				 </td>
				  <fmtBackOrderProcess:message key="LBL_TAG_ID" var="varTagId"/>
				 <td id="showTagId" style="display: none;"width="30%" height="30" class="RightTableCaption" align="center">
				  <gmjsp:label type="RegularText"  SFLblControlName="&nbsp;${varTagId}:"  td="false"/>
				 <input   type="text" size="15"  name="Txt_TagID" class="InputArea" onblur="javascript:fnValidateTag(this.value)" value="<%=strTagId %>" onclick="javascript:fnToggleTag()"/>
				 <span id="DivShowTagAvl" style="vertical-align:middle; display:none;"> <img height="16" width="19" src="<%=strImagePath%>/success.gif"></img></span>
				 <span id="DivShowTagNotExists" style="vertical-align:middle;display: none;"> <fmtBackOrderProcess:message key="LBL_INVALID_ID" var="varInvalidId"/><img title="${varInvalidId}" height="12" width="12" src="<%=strImagePath%>/delete.gif"></img>
						 &nbsp; <%-- <img height="15" onclick="javascript:fnTagLookup()" border="0" title="" src="<%=strImagePath%>/location.png"></img>  --%> 
						 </span>
				 </td>
				 <%if(!strScreen.equals("PROCESS_TXN_SCN")){ %>
				 <fmtBackOrderProcess:message key="LBL_LOAD" var="varLoad"/>
				 <td width="30%" height="30" align="center"><gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnReload();" buttonType="Load" /></td>
					<%}%>
			</tr>
			<tr><td colspan="6" class="Lline"></td></tr>
			<tr>
				<td colspan="6">
				  <div id="divHideButton">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
					<td class="RightTableCaption" align="right" width="15%">
					<fmtBackOrderProcess:message key="LBL_SOURCE" var="varSource"/>
							    <gmjsp:label type="RegularText"  SFLblControlName="&nbsp;${varSource}" td="false"/>
					 </td>
				     <td width="">&nbsp;&nbsp;<gmjsp:dropdown controlName="Cbo_Source_Type"   seletedValue="Cbo_Source_Type"
						tabIndex="2"  value="<%=alSourceList%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"/>	
				     </td>
				   
				<td class="RightTableCaption" align="right" width="15%">
				<fmtBackOrderProcess:message key="LBL_TYPE" var="varType"/>
							  &nbsp;&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="&nbsp;${varType}" td="false"/>
					 </td>
				     <td width="50%">&nbsp;&nbsp;<gmjsp:dropdown controlName="Cbo_Type_List"   seletedValue="Cbo_Type_List"
						tabIndex="2"  value="<%=alTypeList%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" />	
				     </td>
				     </tr></table>
				   </div>
				</td> 
			</tr>
			</table>
		</td></tr>
		<tr><td colspan="2" class="Line" height="1"></td></tr>
		<tr>
			<td width="898" height="70" valign="top" colspan="2">
				<TABLE cellpadding="1" cellspacing="0" border="0" width="100%">
					<TR class="Shade">
					<tr><td colspan="6" class="RightTableCaption" height="24">&nbsp;<a href="javascript:fnBackOrderFilter()">
						<IMG id="backOrderRptImg" border=0 src="<%=strCollapseImage%>">&nbsp;<fmtBackOrderProcess:message key="LBL_BACK_ORDER_REPORT_DETAILS"/></a></td></tr>
						<TD class="RightTableCaption" width="80" align="center"><fmtBackOrderProcess:message key="LBL_NUMBER"/></TD>
						<TD colspan="8" class="RightTableCaption" align="center" width="90"><fmtBackOrderProcess:message key="LBL_DESCRIPTION"/></TD>
					</TR>
					<tr><td colspan="10" class="Line" height="1"></td></tr>
					<TR class="Shade">
						<TD class="RightTableCaption" colspan="2">&nbsp;&nbsp;<input type="checkbox"  name="chkInvlookupAll" onclick="javascript:fnSelectAll(this);"/><fmtBackOrderProcess:message key="LBL_ID"/></TD>
						<TD class="RightTableCaption" align="center" width="90"><fmtBackOrderProcess:message key="LBL_PARENT_ID"/><BR> </TD>
						<TD class="RightTableCaption" width="200" align="center"><fmtBackOrderProcess:message key="LBL_ACCOUNT"/></TD>
						<TD class="RightTableCaption" width="210" align="center"><fmtBackOrderProcess:message key="LBL_DIS/REP"/></TD>
						<TD class="RightTableCaption" align="center" width="50"><fmtBackOrderProcess:message key="LBL_BO_QTY"/><BR></TD>
						<TD class="RightTableCaption" align="center" width="40"><%=strPOColHead%></TD>
						
						<TD class="RightTableCaption" align="center" width="200"><fmtBackOrderProcess:message key="LBL_REQUESTED_DATE"/>&nbsp;</TD>	
						
						<%-- <TD class="RightTableCaption" align="center" width="40"><%=strRequestDate%></TD>--%>
						<TD class="RightTableCaption" align="center" width="100"><%=strReqDate%></TD> 
					<%--  <TD class="RightTableCaption" align="center" width="61"><fmtBackOrderProcess:message key="LBL_DATE"/></TD>	  --%>
							
						<TD class="RightTableCaption" align="center" width="70"><fmtBackOrderProcess:message key="LBL_AMOUNT"/></TH>
					</TR>
<%
					if (hmReturn != null)
					{
						HashMap hmLoop = new HashMap();
						HashMap hmTempLoop = new HashMap();
						ArrayList alList = (ArrayList)hmReturn.get("BOREPORT");
						intSize = alList.size();
						hmTempLoop = new HashMap();
						strLine = "";
						blFlag = false;
						if(!strTagId.equals("") && intSize==0){
						  %>
						  <tr><td colspan="9" align="center"><fmtBackOrderProcess:message key="LBL_NOTHING_FOUND_TO_DISPLAY"/></td></tr>
						  
						  <%}else{
						for (int i = 0;i < intSize ;i++ )
						{
							hmLoop = (HashMap)alList.get(i);

							if (i<intSize-1)
							{
								hmTempLoop = (HashMap)alList.get(i+1);
								strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("NUM"));
							}

							strOrderId = GmCommonClass.parseNull((String)hmLoop.get("ID"));
							strParentOrderId = GmCommonClass.parseNull((String)hmLoop.get("MASTER_ID"));
							strPartNum = GmCommonClass.parseNull((String)hmLoop.get("NUM"));
							strPartNumId = strPartNum.replaceAll("\\.","");
							strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("DSC"));
							strQtyPlaced = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
							strPoPendQty = GmCommonClass.parseNull((String)hmLoop.get("POPEND"));
							intBOQty = Integer.parseInt(strQtyPlaced);
							//strDate = GmCommonClass.parseNull((String)hmLoop.get("DT"));
							strDate = GmCommonClass.getStringFromDate((java.util.Date) hmLoop.get("DT"), strApplDateFmt);
							strTransDate  = GmCommonClass.parseNull((String)hmLoop.get("TRANSDT"));
							//strRequestDate12 = GmCommonClass.getStringFromDate((java.util.Date) hmLoop.get("CDT"), strApplDateFmt);
							
							strOrdAmt = GmCommonClass.parseNull((String)hmLoop.get("SALES"));
							strAccCurrency = GmCommonClass.parseNull((String)hmLoop.get("ACC_CURRENCY")); 
							strCurrSign = strAccCurrency.equals("")? strCurrSign : strAccCurrency;   
							strAccId = GmCommonClass.parseNull((String)hmLoop.get("ANAME"));
							strRepNm = GmCommonClass.parseNull((String)hmLoop.get("REPDISTNM"));
							strOrdCallFlag =  GmCommonClass.parseNull((String)hmLoop.get("LOG_FLAG"));
							strOrdCallFlagInv =  GmCommonClass.parseNull((String)hmLoop.get("INT_LOG_FLAG"));
							strTXNtype = GmCommonClass.parseNull((String)hmLoop.get("TXNTYPE"));
							intTemp = intTemp + intBOQty;
							if (strPartNum.equals(strNextId))
							{
								intCount++;
								strLine = "<TR><TD colspan=10 height=1 class=Line></TD></TR>";
								//intTemp = intTemp + intBOQty;
							}
							else
							{
								sbTemp.append("document.getElementById('td"+strPartNumId+"').innerHTML = '<b>"+intTemp+"</b>';");
								intTemp = 0;
								strLine = "<TR><TD colspan=10 height=1 class=Line></TD></TR>";
								if (intCount > 0)
								{
									strPartDesc = "";
									strLine = "";
								}
								else
								{
									blFlag = true;
								}
								intCount = 0;

							}

							if (intCount > 1)
							{
								strPartDesc = "";
								strLine = "";
							}
						
							strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows

							out.print(strLine);

							if (intCount == 1 || blFlag)
							{
								sbParts.append(strPartNum);
								sbParts.append(",");
%>
						<tr id="tr<%=strPartNumId%>">
							<td width="120" height="20" class="RightText">&nbsp;
 
							<input type="checkbox"  name="chkInvlookup" id="<%=strPartNum%>" title="Inventory lookup"/>&nbsp;<img  style='cursor:hand' src='<%=strImagePath%>/location.png' onclick="javascript:fnLookup('<%=strPartNum%>')" title="Inventory lookup"/>
 
							 <A class="RightText" title="Click to Expand" href="javascript:Toggle('<%=strPartNumId%>')"><%=strPartNum%></a></td>
							<td colspan="4" class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
							<td id="td<%=strPartNumId%>" align="center" class="RightText">&nbsp;</td><input type="hidden" value="<%=strPartNumId%>" id="hPartNumID<%=intAllCnt%>" /> 
							<td colspan="3" class="RightText">&nbsp;<b><a class="RightText" href="javascript:fnViewDetails('<%=strPartNum%>','PO')"><%=strPoPendQty%></a></b>&nbsp;</td> 
							<td colspan="3" class="RightText">&nbsp;</td> 
						</tr>
						<tr>
							<td colspan="10"><div style="display:none" id="div<%=strPartNumId%>">
								<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tab<%=strPartNumId%>">			
<%
								intAllCnt++;
								blFlag = false;
								}
%>
								<TR <%=strShade%>>
									<td width="80" class="RightText" align="center">
									<% if ( strhRptType.equals("50261") || strhRptType.equals("50266")){ %>
									<input type="radio" name="rad" id="<%=i%>" value="<%=strOrderId%>" onClick="fnSelectRad(this);">
									<% }%>
									<img id="imgEdit" style="cursor:hand"  
									<% if (strOrdCallFlag.equals("N"))
	 								{ %>
		 							src="<%=strImagePath%>/phone_icon.jpg" 
		 							<%} else {%>	
		 							src="<%=strImagePath%>/phone-icon_ans.gif" 
			 						<% }%>
			 						title="Click here to add comments (communicate with globus inhouse backorder department)" 
			 						width="20" height="17" onClick="javascript:fnOpenOrdLog('<%=strOrderId%>' )"/>
			 						
			 						 
			 						<img id="imgEdit" style="cursor:hand"  
									<% if (strOrdCallFlagInv.equals("N"))
	 								{ %>
		 							src="<%=strImagePath%>/phone_icon.jpg" 
		 							<%} else {%>	
		 							src="<%=strImagePath%>/phone-icon_ans.gif" 
			 						<% }%>
			 						title="Click here to add comments (For internal employee use only)" 
			 						width="20" height="17" onClick="javascript:fnOpenOrdLogInv('<%=strOrderId%>' )"/> 
			 						  
									<input type="hidden" name="hPartNum<%=i%>" id="<%=strPartNumId%>" value="<%=strQtyPlaced%>">
									
									</td>
									<% if (!strhRptType.equals("50265")) { %>
									<td width="120" height="20"><a class="RightText" href="javascript:fnViewOrder('<%=strOrderId%>', '<%=strTXNtype%>')"><%=strOrderId%></td>
									<% }else { %>
									<td width="120" height="20"> <%=strOrderId%></td>
									<% }%>
									
									<td width="120" height="20" nowrap>&nbsp;<a class="RightText" href="javascript:fnViewOrder('<%=strParentOrderId%>')"><%=strParentOrderId%></td>
									<td width="200" class="RightText">&nbsp;<%=strAccId%></td>
									<td width="210" class="RightText" >&nbsp;<%=strRepNm%></td>
									<td width="130" class="RightText" align="left">&nbsp;<%=strQtyPlaced%></td>
									<td width="50" class="RightText" align="left"></td>
									
									<td width="220" class="RightText" align="center"><%=strTransDate%>&nbsp;</td> 
									
							 	<td width="120" class="RightText" align="center"><%=strDate%>&nbsp;</td> 
									
									<td width="70" class="RightText" align="right"><%=strCurrSign%><%=GmCommonClass.getStringWithCommas(strOrdAmt)%>&nbsp;</td>
									 
								<TR>
								<tr><td colspan="10" height="1" bgcolor="#eeeeee"></td></tr>
<%					
							if ( intCount == 0 || i == intSize-1)
							{
								if (i+1==intSize)
								{	 						
									sbTemp.append("document.getElementById('td"+strPartNumId+"').innerHTML = '<b>"+intTemp+"</b>';");
								}
%>
							</table></div>
						</td>
					</tr>
<%
							}
							if(intSize ==1 && intTemp==0){intTemp =1; sbTemp.setLength(0);  
							sbTemp.append("document.getElementById('td"+strPartNumId+"').innerHTML = '<b>"+intTemp+"</b>';");}
							
						}} // End of For
							sbTemp.append("document.all.hPartNums.value = '"+sbParts.toString()+"';");
							out.print("<script>");
							
							out.print(sbTemp.toString());
							out.print("</script>");
							 
					}else{
%>
								<tr>
									<td colspan="10" align="center" class="RightTextRed"><fmtBackOrderProcess:message key="LBL_THERE_ARE_NO_SALES_BACKORDERS"/></td>
								</tr>
<%
					}
%>
				</TABLE>
			</TD>
		</tr>
 

		<tr>
			<td align="center" height="30">

<%	if  (strhRptType.equals("50261") || strhRptType.equals("50266")){
			if(strhRptType.equals("50266")){
%>
         <b>Choose Action :</b>&nbsp;&nbsp;<gmjsp:dropdown controlName="Act_Type"   seletedValue="Act_Type"  value="<%=alCboList%>" codeId = "CODEID"  codeName = "CODENM" width="120"/>&nbsp;&nbsp;	
      <%} %>
      <fmtBackOrderProcess:message key="LBL_SUBMIT" var="varSubmit"/>
			<gmjsp:button value="${varSubmit}" gmClass="button" onClick="fnSubmit();" tabindex="13" buttonType="Save" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<%
		}
%>
<fmtBackOrderProcess:message key="LBL_INVENTORY_LOOKUP" var="varInventoryLookup"/>
				<gmjsp:button value="${varInventoryLookup}" gmClass="button" onClick="fnLookup();" tabindex="13" buttonType="Load" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<%	if  (strhRptType.equals("50261")) {   //the button only show for sales %> 
			<fmtBackOrderProcess:message key="LBL_UPDATE_TO_BILL_ONLY" var="varUpdatetoBillOnly"/>
				<gmjsp:button value="${varUpdatetoBillOnly}" gmClass="button" disabled="<%=strBtnDisable%>" onClick="fnUpdateToBillOnly();" tabindex="13" buttonType="Save" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<%
		}
%>
				
<%
		if (strhRptType.equals("50261")||strhRptType.equals("50262")||strhRptType.equals("50263"))
		{
%>

<fmtBackOrderProcess:message key="LBL_PART_FULFILL_REPORT" var="varPartFulfillReport"/>
				<gmjsp:button value="${varPartFulfillReport}" gmClass="button" onClick="fnloadPartFulfillReport();" tabindex="13" buttonType="Load" />
				
<%
		}
%>
			</td>
		</tr>
 
		<tr><td colspan="6"> <input type="hidden" name="allExpand" value="<%=intAllCnt%>"> </td></tr>
		<tr><td colspan="3" class="Line" height="1"></td></tr>
    </table>

</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
