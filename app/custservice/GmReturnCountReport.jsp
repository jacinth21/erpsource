 <%
/**********************************************************************************
 * File		 		: GmReturnCountReport.jsp
 * Desc		 		: This screen is used for the DashBoard Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="java.util.Date"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import ="org.apache.commons.beanutils.RowSetDynaClass"%>
<%@ page buffer = "16kb" %>
<%@ taglib prefix="fmtRtnCount" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- custservice\GmReturnCountReport.jsp -->
<fmtRtnCount:setLocale value="<%=strLocale%>"/>
<fmtRtnCount:setBundle basename="properties.labels.custservice.GmReturnCountReport"/>
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmReturnCountReport", strSessCompanyLocale);
	//HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strType = (String)request.getAttribute("hType") == null?"50231":(String)request.getAttribute("hType");
	String strFromDt = (String)request.getAttribute("hFrmDt")==null?"":(String)request.getAttribute("hFrmDt");
	String strToDt = (String)request.getAttribute("hToDt")==null?"":(String)request.getAttribute("hToDt");
	String gridData = (String)request.getAttribute("rsRetReport")==null?"":(String)request.getAttribute("rsRetReport");
	String strApplDateFmt = strGCompDateFmt;
	
	/* HashMap hmDateValues = new HashMap();
	hmDateValues.put("FROMADATE",strFromDt);
	hmDateValues.put("TODATE",strToDt); */
	
	String strHeader = "";
	String strLabel1 = "";
	String strLabel2 = "";
	String strId1 = "";
	String strId2 = "";
	String strName1 = "";
	String strName2 = "";
	
	Date dtFrmDate = null;
	Date dtToDate = null;
	
	dtFrmDate = (Date)GmCommonClass.getStringToDate(strFromDt,strApplDateFmt);
	dtToDate = (Date)GmCommonClass.getStringToDate(strToDt,strApplDateFmt);
	
	String strReportType = "";

	if (strType.equals("50231"))
	{
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_50231_HEADER"));
		strLabel1 = "Set Name";
		strLabel2 = "Distributor Name";
		strId1 = "ID";
		strId2 = "DID";
		strName1 = "SNAME";
		strName2 = "DNAME";
		strReportType = "50230";
	}
	else if (strType.equals("50230"))
	{
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_50230_HEADER"));	
		strLabel1 = "Distributor Name";
		strLabel2 = "Set Name";
		strId1 = "DID";
		strId2 = "ID";
		strName1 = "DNAME";
		strName2 = "SNAME";
		strReportType = "50230";
	}
	else if (strType.equals("50234")) //ICT
	{
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_50234_HEADER")); //"By Set";
		strLabel1 = "Distributor Name";
		strLabel2 = "Set Name";
		strId1 = "DID";
		strId2 = "ID";
		strName1 = "DNAME";
		strName2 = "SNAME";
		strReportType = "50234";
	}
	else if (strType.equals("50232"))
	{
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_50232_HEADER"));		
		strLabel1 = "Set Name";
		strLabel2 = "Purpose";
		strId1 = "ID";
		strId2 = "DID";
		strName1 = "SNAME";
		strName2 = "DNAME";
		strReportType = "INHOUSE";
	}
	else if (strType.equals("50233"))
	{
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_50233_HEADER"));		
		strLabel1 = "Purpose";
		strLabel2 = "Set Name";
		strId1 = "DID";
		strId2 = "ID";
		strName1 = "DNAME";
		strName2 = "SNAME";
		strReportType = "INHOUSE";
	}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Returns Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/gmCommon.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

<script>
var dateFmt = '<%=strApplDateFmt%>';
var objGridData;
objGridData = '<%=gridData%>';

function setDefalutGridProperties(divRef,footerArry){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/imgs/");
	if(footerArry != undefined && footerArry.length >0)
	gObj.attachFooter(footerArry);
	gObj.enableMultiline(true);	
	return gObj;
}

function initializeDefaultGrid(gObj, gridData, split_At){	
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;
}

function enterPressed(evn){
	if (window.event && window.event.keyCode == 13){
  		fnGo();
	}else if (evn && evn.keyCode == 13) {
  		fnGo();
	}
}
document.onkeypress = enterPressed;

function fnDrillDown(distid,setid,distName){	
	distName=distName.replace(/~/g," ");
	document.frmAccount.Cbo_Dist.value = distid;
	document.frmAccount.Cbo_Set.value = setid;
	document.frmAccount.hType.value = '50230'; //"DIST"
	document.frmAccount.hAction.value = "Drilldown";
	document.frmAccount.hDistName.value = distName;
	document.frmAccount.submit();
}

function fnLoadType(val){
	document.frmAccount.hType.value = val;
}

function fnLoad(){
	var obj = document.frmAccount.Cbo_Type.value='<%=strType%>';
	fnOnPageLoad();
}

function fnGo(){
	objStartDt = document.frmAccount.Txt_FromDate;
	objEndDt = document.frmAccount.Txt_ToDate;
	if(objStartDt.value != ""){		 
		CommonDateValidation(document.frmAccount.Txt_FromDate,dateFmt,Error_Details_Trans(message[10523],dateFmt));	 
	}

	if(objEndDt.value != ""){ 
		CommonDateValidation( document.frmAccount.Txt_ToDate,dateFmt,Error_Details_Trans(message[10524],dateFmt));	 
	}
	
	var fromToDiff = dateDiff(document.frmAccount.Txt_FromDate.value, document.frmAccount.Txt_ToDate.value, dateFmt);
	if(fromToDiff < 0){
   		Error_Details(message[10525]);
	}
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}	
	document.frmAccount.hAction.value = "Reload";
	document.frmAccount.hType.value = document.frmAccount.Cbo_Type.value;
	fnStartProgress();
	document.frmAccount.submit();
}

function Toggle(val){	
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none'){
		obj.style.display = '';
		trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		tabobj.style.background="#ecf6fe";
	}else{
		obj.style.display = 'none';
		trobj.className="";
		tabobj.style.background="#ffffff";
	}
}

function fnExport(str){
	//gridObj.expandAllGroups();  //For exporting Excel sheet both IE and MSEdge no need expandAllGroups
	gridObj.setColumnHidden(1,false);
	gridObj.setColumnHidden(0,true);
	gridObj.unGroup();
	if(str=='excel'){
		gridObj.toExcel('/phpapp/excel/generate.php');
	}else{
		gridObj.toExcel('/phpapp/pdf/generate.php');
	}
	gridObj.groupBy(1,["#title","","","","#stat_total"]);
	gridObj.setColumnHidden(1,true);
	gridObj.setColumnHidden(0,false);
}

function fnOnPageLoad(){	  
	if (objGridData != ''){		
		var footer=new Array();
		footer[0]="";
		footer[1]="";
		footer[2]="";
		footer[3]= message[10526];
		footer[4]="{#stat_total}";

		var gObj = setDefalutGridProperties('grpData',footer);		
		gridObj = initializeDefaultGrid(gObj,objGridData,''); //initGrid('grpData',objGridData,'',footer);
		
		var rowsAll = gridObj.getRowsNum();
		
		if(parseInt(rowsAll)==0)
		gridObj.detachFooter(0);
		
		gridObj.enableTooltips("false,true,true,true");
		gridObj.groupBy(1,["#title","","","","#stat_total"]);
		gridObj.customGroupFormat=function(name,count){
            return "<font size=1>"+name+" ( " +count+ " ) </font>";
        }        
		gridObj.setColumnHidden(1,true);
		gridObj.collapseAllGroups();
		gridObj.expandAllGroups();		
	}
}

/*
var form = document.getElementById('addProduct');
var els = form.elements,    
el,    
i;
for (i = 0; i < els.length; i += 1) 
{    
	el = els[i];    
	if (!(el.nodeName === 'INPUT' && el.type === 'submit')) 
	{        
		el.onkeypress = captureEnter;    
	}
}
*/
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmReturnsReportServlet">
<input type="hidden" name="Cbo_Dist" value="">
<input type="hidden" name="Cbo_Set" value="">
<input type="hidden" name="hAction" value="DrillDown">
<input type="hidden" name="hType" value="50230">
<input type="hidden" name="hDistName" value="">


	<table border="0" class="GtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">
				<fmtRtnCount:message key="TD_RTNS_RPT_HEADER"/> <%=strHeader%>
			</td>
		</tr>
		<tr><td class="Line" height="1"></td></tr>
		<tr>
			<td class="RightText" HEIGHT="24">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtRtnCount:message key="LBL_RPT_TYPE"/>:&nbsp;&nbsp;&nbsp;
			&nbsp;<select name="Cbo_Type" class="RightText" tabindex="1" onChange="javascript:fnLoadType(this.value);"><option value="0" ><fmtRtnCount:message key="OPT_CHOOSE_ONE"/>
				<option value="50230"><fmtRtnCount:message key="OPT_BY_DIST_ALL"/>
				<option value="50231"><fmtRtnCount:message key="OPT_BY_SET_ALL_DIST"/>
				<option value="50234"><fmtRtnCount:message key="OPT_BY_DIST_ICT"/>
				<!--<option value="INSET">In-House - By Sets-->
			</select>&nbsp;&nbsp;
			<fmtRtnCount:message key="LBL_FROM_DATE"/>:&nbsp;<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtFrmDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="2"/>
			&nbsp;&nbsp;<fmtRtnCount:message key="LBL_TO_DATE"/>: <gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=dtToDate==null?null:new java.sql.Date(dtToDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="3"/>&nbsp;&nbsp;
				<fmtRtnCount:message key="BTN_LOAD" var="varLoad"/>
				<gmjsp:button  value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="return fnGo();" tabindex="25" buttonType="Load" />
			</td>
		</tr>
		<tr><td class="Line" height="0"></td></tr>
		<%
			strName2 = strName2 + "1";			
		%>
				
			<tr>	<td><div id="grpData" style="grid" height="500px" width="900px"></div></td> </tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
			                <td colspan="6" align="center">
			                <div class='exportlinks'><fmtRtnCount:message key="DIV_EXPORT_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
			                                onclick="fnExport('excel');"><fmtRtnCount:message key="DIV_EXCEL"/>  </a>
			                                &nbsp; <!-- <img src='img/ico_file_pdf.png' />&nbsp;<a href="#"
			                                onclick="fnExport('pdf');"> Pdf </a> -->
			                              
			                                </div>
			                </td>
<tr><td>&nbsp;</td></tr>
					</tr>
					
			
	
	</table>


</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
