 <%
/**********************************************************************************
 * File		 		: GmTerritorySetup.jsp
 * Desc		 		: This screen is used for the Territory Maintenance
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Calendar" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtTerritorySetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- custservice\GmTerritorySetup.jsp -->
<fmtTerritorySetup:setLocale value="<%=strLocale%>"/>
<fmtTerritorySetup:setBundle basename="properties.labels.custservice.GmTerritorySetup"/>
<%
	response.setCharacterEncoding("UTF-8");
	
	String strWikiTitle = GmCommonClass.getWikiTitle("TERRITORY_SETUP");
	
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = "Add";
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strTxnCurrency = "";
	String strRptCurrency = "";

	ArrayList alReps = new ArrayList();
	HashMap hmTerritory = new HashMap();
	ArrayList alYear = new ArrayList();
	ArrayList alAccounts = new ArrayList();
	ArrayList alQuota = new ArrayList();
	String strTerrNm = "";
	String strTerrId = "";
	String strQuota = "";
	String strCounties = "";
	String strYearId = (String)request.getAttribute("hYear")==null?"2006":(String)request.getAttribute("hYear");
	String strADName = "";
	String strDistName = "";
	String strRepName = "";
	String strActiveFl = "N";
	String gridData = (String)request.getAttribute("rsQuotaReport")==null?"":(String)request.getAttribute("rsQuotaReport");
	String strRepId ="";
	String strCompanyId = "";
	String strCurTypeId = "";
	String strDivHeight = "150";
	String strDivWidth = "758";
	String strhDivId = (String)request.getAttribute("hDivId")==null?"":(String)request.getAttribute("hDivId");
	
	if ( hmReturn != null)
	{
		alReps= GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("REPLIST"));
		hmTerritory = (HashMap)hmReturn.get("TERRITORYDETAILS");
		alYear = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("YEARLIST"));
		alAccounts = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ACCOUNTSLIST"));
		alQuota = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("QUOTA"));
	}

	if (hmTerritory != null )
	{
		strTerrId = GmCommonClass.parseNull((String)hmTerritory.get("TERRID"));
		strTerrNm = GmCommonClass.parseNull((String)hmTerritory.get("TNAME"));
		strADName = GmCommonClass.parseNull((String)hmTerritory.get("ADNAME"));
		strDistName = GmCommonClass.parseNull((String)hmTerritory.get("DNAME"));
		strRepName = GmCommonClass.parseNull((String)hmTerritory.get("RNAME"));
		strhAction = (strTerrId.equals(""))?"Add":"Edit";
		strActiveFl = GmCommonClass.parseNull((String)hmTerritory.get("AFLAG"));
		strChecked = strActiveFl.equals("Y")?"checked":"";
		strRepId = GmCommonClass.parseNull((String)hmTerritory.get("REPID"));
	}
	strRepId = (strRepId.equals(""))?GmCommonClass.parseNull((String)request.getAttribute("hRepId")):strRepId;
	strCompanyId = GmCommonClass.parseNull((String)request.getAttribute("hCompanyId"));
	strCurTypeId = GmCommonClass.parseNull((String)request.getAttribute("hCurrTypeId"));
	
	int intSize = 0;
	int intQuotaSize = 0;
	HashMap hcboVal = null;
	HashMap hmYear = null;
	int intMapSize = 0;
	String strYear="",StrDropDownYear="";
	
	StringBuffer sbQuota = new StringBuffer();
	Calendar cal = Calendar.getInstance(); 
	int year = cal.get(Calendar.YEAR);
	
	if(alYear.size()>0)
	{
		for (int i=0;i<alYear.size();i++)
		{
			hmYear = (HashMap)alYear.get(i);
			strYear = (String)hmYear.get("CODENM");
			if(Integer.parseInt(strYear)>=year)
			StrDropDownYear+=strYear+"#@#";
		}
	}
	ArrayList alPartyCompany = new ArrayList();
	alPartyCompany = (ArrayList)request.getAttribute("COMPANY_LIST");
	
	ArrayList alCurrType = new ArrayList();
	alCurrType = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("CURRTYPE"));
	strTxnCurrency = GmCommonClass.parseNull((String)request.getAttribute("TXNCURR"));
	strRptCurrency = GmCommonClass.parseNull((String)request.getAttribute("RPTCURR"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Territory Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_form.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_markers.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxMenu/dhtmlxmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_undo.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmTerritory.js"></script>

<SCRIPT>

var currYear = '<%=year%>';
<%
	if (alQuota != null)
	{
  		intQuotaSize = alQuota.size();
		hcboVal = new HashMap();
		if (intQuotaSize > 0)
		{
			for (int i=0;i<intQuotaSize;i++)
			{
				hcboVal = (HashMap)alQuota.get(i);
				strQuota = (String)hcboVal.get("AMT");
				sbQuota.append(strQuota);
				sbQuota.append(",");
				strQuota = (String)sbQuota.toString();
			}
%>
			var quota = '<%=strQuota.substring(0,strQuota.length()-1)%>';  
<%
		}
	}
%>
var Total = 0.0;
var objGridData='<%=gridData%>';

var varNameDropDownValues = "<%=StrDropDownYear%>";
</SCRIPT>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnSetQuota(objGridData);">
<FORM name="frmTerritory" method="POST" action="<%=strServletPath%>/GmTerritoryServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="inputString" value="">
<input type="hidden" name="hQuotaLen" value="<%=intQuotaSize%>">
<input type="hidden" name="hTerrId" value="<%=strTerrId%>">
<input type="hidden" name="Cbo_Year"/>
<input type="hidden" name="hTxnCurr" value="<%=strTxnCurrency%>">
<input type="hidden" name="hRptCurr" value="<%=strRptCurrency%>">
<input type="hidden" name="hDivId" value="<%=strhDivId%>">

	<table border="1" width="730" cellspacing="0" cellpadding="0">
		<tr>
			<td width="730" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td class="Line" colspan="2"></td></tr>
					<tr>
						<td height="25" width="250" class="RightDashBoardHeader">&nbsp;<fmtTerritorySetup:message key="LBL_REP_QUOTA_SETUP"/></td>
						<td align="right" class=RightDashBoardHeader >
							<fmtTerritorySetup:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
						</td>
					</tr>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="oddshade" colspan="2">
						<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtTerritorySetup:message key="LBL_COMPANY" var="varCompany"/><gmjsp:label type="MandatoryText"  SFLblControlName="${varCompany}&nbsp; Name&nbsp;List:" td="false"/></td>
						<td>&nbsp;<gmjsp:dropdown controlName="Cbo_CompanyId"  seletedValue="<%=strCompanyId%>" 	
						 value="<%=alPartyCompany%>" codeId = "COMPANYID"   codeName = "COMPANYNM"  defaultValue= "[Choose One]"   onChange="javascript:fnPopulateRep(this.value);" />						
						</td>
					</tr>

					<tr class="evenshade" colspan="2">
						<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtTerritorySetup:message key="LBL_CURRENCY" var="varCurrency"/><gmjsp:label type="MandatoryText"  SFLblControlName="${varCurrency}&nbsp; Type:" td="false"/></td>
						<td>&nbsp;<gmjsp:dropdown controlName="Cbo_CurrType"  seletedValue="<%=strCurTypeId%>" 	
						 value="<%=alCurrType%>" codeId = "CODEID"   codeName = "CODENM"  defaultValue= "[Choose One]"   onChange="javascript:fnShowCurr(this.value);" />	
						 &nbsp;&nbsp;<span id="currsymbl"></span>					
						</td>
					</tr>
										
					<tr class="oddshade" colspan="2">
						<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtTerritorySetup:message key="LBL_SALES" var="varSales"/><gmjsp:label type="MandatoryText" SFLblControlName="${varSales}&nbsp;Rep&nbsp;List:" td="false"/></td>
						<td>&nbsp;<gmjsp:dropdown controlName="Cbo_RepId"  seletedValue="<%=strRepId%>" 	
						 value="<%=alReps%>" codeId = "REPID"   codeName = "NAME"  defaultValue= "[Choose One]"   onChange="javascript:fnCallTerritory(this.value);" />						
						</td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="2"></td></tr>
					<tr>
						<td class="RightTextBlue" colspan="2" align="center">
							<fmtTerritorySetup:message key="LBL_CHOOSE_ONE"/>
						</td>
					</tr>					
					<tr><td class="LLine" colspan="2"></td></tr>
					<tr class="oddshade" colspan="2">
						<td  class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtTerritorySetup:message key="LBL_TERRITORY" var="varTerritory"/><gmjsp:label type="MandatoryText"  SFLblControlName="${varTerritory}:" td="false"/></td>
						<td>&nbsp;<input type="text" size="46" value="<%=strTerrNm%>" name="Txt_TerrNm" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="2"></td></tr>
					<tr class="evenshade" colspan="2">
						<td  class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtTerritorySetup:message key="LBL_TID" var="varTid"/><gmjsp:label type="RegularText"  SFLblControlName="${varTid}:" td="false"/></td>
						<td>&nbsp;<%=strTerrId %></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="2"></td></tr>
					<tr class="oddshade" colspan="2">
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtTerritorySetup:message key="LBL_ACTIVE" var="varActive"/><gmjsp:label type="RegularText"  SFLblControlName="${varActive} ?:" td="false"/></td>
						<td >&nbsp;<input type="checkbox" size="30" <%=strChecked%> name="Chk_ActiveFl" value="<%=strActiveFl%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=25>				
						</td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="2"></td></tr>
					<tr class="evenshade" colspan="2">
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtTerritorySetup:message key="LBL_AREA" var="varArea"/><gmjsp:label type="RegularText"  SFLblControlName="${varArea}:" td="false"/></td>
						<td class="RightText">&nbsp;<%=strADName%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="2"></td></tr>					
					<tr class="oddshade" colspan="2">
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtTerritorySetup:message key="LBL_DISTRIBUTOR" var="varDistributor"/><gmjsp:label type="RegularText"  SFLblControlName="${varDistributor}:" td="false"/></td>
						<td class="RightText">&nbsp;<%=strDistName%></td>
					</tr>
			<%-- 	<tr><td bgcolor="#eeeeee" colspan="2"></td></tr>
					<tr class="evenshade" colspan="2">
						<td class="RightTableCaption" align="right" HEIGHT="24"><gmjsp:label Type="RegularText"  SFLblControlName="Sales Rep:" td="false"/>
                        </td>
						<td class="RightText">&nbsp;<%=strRepName%></td>
					</tr> --%>
					<tr><td bgcolor="#eeeeee" colspan="2"></td></tr>
					<tr class="evenshade" colspan="2">
						<td class="RightTableCaption" valign="top" align="right" HEIGHT="24"><fmtTerritorySetup:message key="LBL_ACC" var="varAcc"/><gmjsp:label type="RegularText"  SFLblControlName="${varAcc}:" td="false"/>
                         </td>
						<td class="RightText">
<%
				  		if (alAccounts != null)
				  		{
				  			intSize = alAccounts.size();
							hcboVal = new HashMap();
				  			for (int i=0;i<intSize;i++)
				  			{
				  				hcboVal = (HashMap)alAccounts.get(i);
%>
								&nbsp;<%=hcboVal.get("AID")%>&nbsp;:&nbsp;<%=hcboVal.get("ANAME")%><BR>
<%
							}
						}
%>						
						</td>
					</tr>
					<tr><td class="LLine" colspan="2"></td></tr>
					<tr class="ShadeRightTableCaption" >
						<td colspan="2" height="20"><fmtTerritorySetup:message key="LBL_QUOTA_MGT"/><BR></td>
					</tr>
					<tr><td class="LLine" colspan="2"></td></tr>
					<tr>
						<td class="RightTableCaption" colspan="2" align="center" height="20">
<%
				if (intQuotaSize == 0)
				{
%>
				<BR>
				<span class="RightTextBlue"><fmtTerritorySetup:message key="LBL_NO_QUOTA"/>  <%=strYearId%></span>
				<BR><BR>
<%
				}else{

%>				<table cellpadding="1" cellspacing="1" border="0"  width="470">
					<tr>
				   <td class="RightTableCaption">
								&nbsp;<a href="#" style="text-decoration:none;"> <img src="<%=strImagePath%>/dhtmlxGrid/copy.gif" onClick="javascript:docopy()" alt="copy" style="border: none;" height="14"></a>	
								&nbsp;<a href="#" style="text-decoration:none;"> <img src="<%=strImagePath%>/dhtmlxGrid/paste.gif" alt="paste" style="border: none;" onClick="javascript:pasteToGrid()" height="14"></a>	
								&nbsp;<a href="#" style="text-decoration:none;"> <img src="<%=strImagePath%>/dhtmlxGrid/redo.gif" alt="redo" onClick="javascript:doredo();" style="border: none;" height="14"></a>
								&nbsp;<a href="#" style="text-decoration:none;"> <img src="<%=strImagePath%>/dhtmlxGrid/undo.gif" alt="Undo" onClick="javascript:doundo();" style="border: none;" height="14"></a>	
								&nbsp;<a href="#" style="text-decoration:none;"> <img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="Add Row" style="border: none;" onClick="javascript:addRow()"height="14"></a>
								&nbsp;<a href="#" style="text-decoration:none;"> <img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="Remove Row" style="border: none;" onClick="javascript:removeSelectedRow();" height="14"></a>
								&nbsp;<a href="#" style="text-decoration:none;"> <img src="<%=strImagePath%>/dhtmlxGrid/row_add_after_1.gif" alt="Add Rows From Clipboard" style="border: none;" onClick="javascript:addRowFromClipboard();" height="14"></a>
								</td>	
				   		
				   </tr>	
				   <tr><td><div id="grpData" style="width:<%=strDivWidth%>px;height:<%=strDivHeight%>px" ></div></td></tr>   
				   </table>
						<%} %>
						</td>
				   </tr>
				   <tr class="oddshade" colspan="2">
						<td colspan="2"> 
							<jsp:include page="/common/GmIncludeLog.jsp" >
								<jsp:param name="LogType" value="" />
								<jsp:param name="LogMode" value="Edit" />
							</jsp:include>
						</td>
					</tr>
					<tr><td class="LLine" colspan="2"></td></tr>
				   <tr>
						<td colspan="2" align="center" height="40">&nbsp;
						<fmtTerritorySetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="btn_submit" value="${varSubmit}" gmClass="button" onClick="javascript:fnSubmit();" tabindex="16" buttonType="Save" />&nbsp;
						<fmtTerritorySetup:message key="BTN_RESET" var="varReset"/><gmjsp:button name="btn_reset" value="${varReset}" gmClass="button" onClick="javascript:fnReset();" tabindex="17" buttonType="Save" />
						</td>
					</tr>
				    <tr><td>			    
				    </td></tr>
				    
				</table>
			</td>
		</tr>

		<tr>
			<td colspan="3" height="1" ></td>
		</tr>
		
    </table>
    

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
