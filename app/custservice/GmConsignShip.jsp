 <%
/**********************************************************************************
 * File		 		: GmConsignShip.jsp
 * Desc		 		: This screen is used for the print version of Set Consignment
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="java.util.Date"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%> 
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib prefix="fmtConsignShip" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!--  GmConsignShip.jsp -->
<fmtConsignShip:setLocale value="<%=strLocale%>"/>
<fmtConsignShip:setBundle basename="properties.labels.custservice.GmConsignShip"/>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmConsignShip", strSessCompanyLocale);
	
	GmCalenderOperations gmCal = new GmCalenderOperations();
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");

	String strMode = (String)request.getAttribute("hMode")==null?"":(String)request.getAttribute("hMode");
	String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	HashMap hmConDetails = new HashMap();
	ArrayList alSetLoad = new ArrayList();

	String strSelected = "";
	String strCodeID = "";
	String strShade = "";
	String strTitle = "Consignment View";
	String strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_VIEW_CONSIGNMENT"));

	String strConsignId = "";
	String strConsignAdd = "";
	String strShipAdd = "";
	String strDate = "";
	
	String strTemp = "";
	String strItemQty = "";
	String strControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";

	String strUserName = "";

	String strTodaysDate = gmCal.getCurrentDate(strGCompDateFmt)==null?"":gmCal.getCurrentDate(strGCompDateFmt);
	//Getting date format from the session
	String strApplDateFmt = strGCompDateFmt;
	String strShipDate = "";
	String strShipCarr = "";
	String strShipMode = "";
	String strTrack = "";
	String strSetId = "";
	String strFreightAmt="";
	String strPONum="";
	String strType="";
	String strPartyId="";
	String strCurrency="";
	String strDistType ="";
	Date dtShipDate = null;
	String strCompanyID ="";
	if (hmReturn != null)
	{
		alSetLoad = (ArrayList)hmReturn.get("SETLOAD");
		hmConDetails = (HashMap)hmReturn.get("SETDETAILS");
		strSetId = GmCommonClass.parseNull((String)hmReturn.get("STRSETID"));		
		strConsignId = GmCommonClass.parseNull((String)hmConDetails.get("CID"));
		strConsignAdd = GmCommonClass.parseNull((String)hmConDetails.get("BILLADD"));
		strShipAdd = GmCommonClass.parseNull((String)hmConDetails.get("SHIPADD"));
		//strDate = GmCommonClass.parseNull((String)hmConDetails.get("UDATE"));
		strDate = GmCommonClass.getStringFromDate((java.util.Date) hmConDetails.get("UDATE"), strApplDateFmt);
		strUserName = GmCommonClass.parseNull((String)hmConDetails.get("NAME"));
		//strShipDate = GmCommonClass.parseNull((String)hmConDetails.get("SDATE"));
		strShipDate = GmCommonClass.getStringFromDate((java.util.Date) hmConDetails.get("SDATE"), strApplDateFmt);
		strShipCarr = GmCommonClass.parseNull((String)hmConDetails.get("SCARR"));
		strShipMode = GmCommonClass.parseNull((String)hmConDetails.get("SMODE"));
		strTrack = GmCommonClass.parseNull((String)hmConDetails.get("TRACK"));
		strFreightAmt=GmCommonClass.parseNull((String)hmConDetails.get("FREIGHTAMT"));
		strPONum=GmCommonClass.parseNull((String)hmConDetails.get("PONUM"));
		strType=GmCommonClass.parseNull((String)hmConDetails.get("TYPE"));
		strPartyId=GmCommonClass.parseNull((String)hmConDetails.get("PARTYID"));
		strDistType = GmCommonClass.parseNull((String) hmConDetails.get("DISTTYPE"));
		strShipDate = strShipDate.equals("")?strTodaysDate:strShipDate;
		dtShipDate = (Date)GmCommonClass.getStringToDate(strShipDate,strApplDateFmt);// converting to date format for using in calendar
		strCurrency = GmCommonClass.parseNull((String)hmConDetails.get("NEWCURRENCY"));
	}

	HashMap hmShip = new HashMap();
	ArrayList alCarrier = new ArrayList();
	ArrayList alMode = new ArrayList();
	
	if (strMode.equals("SHIP"))
	{
		hmShip = (HashMap)request.getAttribute("hmShipLoad");

		if (hmShip != null)
		{
			alCarrier = (ArrayList)hmShip.get("CARRIER");
			alMode = (ArrayList)hmShip.get("MODE");
		}
		strTitle = "Consignment Ship";
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_EDIT_CONSIGNMENT_SHIPPING"));
	} 
	else if (strMode.equals("DummyVerify")) {
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_APPROVE_DUMMY_CONSIGNMENT"));
	}

	int intSize = 0;
	HashMap hcboVal = null;
	strCompanyID = gmDataStoreVO.getCmpid();
	String strruleEnforceDate = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("50001", "ICTCHANGE",strCompanyID));
    strruleEnforceDate = strruleEnforceDate.equals("") ? GmCommonClass.parseNull(GmCommonClass.getRuleValue("50001", "ICTCHANGE")) : strruleEnforceDate;
	String strShipDt = GmCommonClass.parseNull((String)request.getAttribute("SHIPDATE"));
	String strPkSlpNm = GmCommonClass.parseNull((String)request.getAttribute("PkSlpNm"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: <%=strTitle%> </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmConsignShip.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>

var partyId = '<%=strPartyId%>';
var servletPath = '<%=strServletPath%>';
var consignId = '<%=strConsignId%>';
var dateFmt = '<%=strApplDateFmt%>';

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmShip" method="post" action = "<%=strServletPath%>/GmConsignSetServlet">
<input type="hidden" name="hAction" value="SaveShip">
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="hMode" value="">
<input type="hidden" name="hConsignId" value="<%=strConsignId%>">
<input type="hidden" name="hTxnId" value="<%=strConsignId%>">
<input type="hidden" name="hCancelType" value="RBSCN">

<table border=0  cellspacing=0 cellpadding=0 class="DtTable850" >
	
	<tr>
		<td>
			<table cellpadding=0 cellspacing=0 border=0 width='100%'>
				<tr>
					<td colspan="3" height="25" class="RightDashBoardHeader"><%=strHeader%></td>
				</tr>
				<tr><td bgcolor=#666666 colspan="3" height=1></td></tr>
				<tr>
					<td colspan=3>
						<table border=0 width=100% cellspacing=0 cellpadding=0>
							<tr bgcolor="#eeeeee" class=RightTableCaption>
								<td height=24 align=center width=250>&nbsp;<fmtConsignShip:message key="LBL_CONSIGNED_TO"/></td>
								<td bgcolor=#666666 width=1 rowspan=11></td>
								<td width=250 align=center>&nbsp;<fmtConsignShip:message key="LBL_SHIP_TO"/></td>
								<td bgcolor=#666666 width=1 rowspan=11></td>
								<td width=100 align=center>&nbsp;<fmtConsignShip:message key="LBL_DATE"/></td>
								<td width=100 align=center>&nbsp;<fmtConsignShip:message key="LBL_CONSIGN"/></td>
							</tr>
							<tr><td bgcolor=#666666 height=1 colspan=6></td></tr>
							<tr>
								<td class=RightText rowspan=5 valign=top>&nbsp;<%=strConsignAdd%></td>
								<td rowspan=5 class=RightText valign=top>&nbsp;<%=strShipAdd%></td>
								<td height=25 class=RightText align=center>&nbsp;<%=strDate%></td>
								<td class=RightText align=center>&nbsp;<%=strConsignId%></td>
							</tr>
							<tr><td bgcolor=#666666 height=1 colspan=2></td></tr>
							<tr bgcolor="#eeeeee" class=RightTableCaption><td align=center>&nbsp;</td><td  align=center><fmtConsignShip:message key="LBL_CONSIGNED_BY"/></td></tr>
							<tr><td bgcolor=#666666 height=1 colspan=2></td></tr>
							<tr>
								<td align=center class=RightText>&nbsp;<Br></td>
								<td align=center height=23 nowrap class=RightText>&nbsp;<%=strUserName%></td>
							</tr>
							<tr><td bgcolor=#666666 height=1 colspan=6></td></tr>
								<tr class="RightTableCaption" bgcolor="#eeeeee">
									<td height="20" align="center"><fmtConsignShip:message key="LBL_SHIP_VIA"/></td>
									<td align="center"><fmtConsignShip:message key="LBL_SHIP_MODE"/></td>
									<td align="center" colspan="2"><fmtConsignShip:message key="LBL_TRACKING"/></td>
								</tr>
								<tr><td bgcolor="#666666" height="1" colspan="6"></td></tr>
								<tr>
									<td height="20" align="center" class="RightText"><%=strShipCarr%></td>
									<td align="center" class="RightText"><%=strShipMode%></td>
									<td align="center" colspan="2" class="RightText"><%=strTrack%></td>
								</tr>
								<tr><td bgcolor="#666666" height="1" colspan="6"></td></tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan=3>
						<table border=0 width=100% cellspacing=0 cellpadding=0>
							<tr bgcolor="#eeeeee" class=RightTableCaption>
								<td width=25 height=24><fmtConsignShip:message key="LBL_S"/></td>
								<td width=60 height=24>&nbsp;<fmtConsignShip:message key="LBL_PART"/></td>
								<td  width=330><fmtConsignShip:message key="LBL_DESCRIPTION"/></td>
								<td align=center width=100><fmtConsignShip:message key="LBL_QUANTITY"/></td>
								<td align=center width=100><fmtConsignShip:message key="LBL_CONTROL"/></td>
								<td align=center width=100>&nbsp;&nbsp;<fmtConsignShip:message key="LBL_PRICE"/></td>
								<td align=center width=100><fmtConsignShip:message key="LBL_AMOUNT"/></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		
	</tr>
</table>

	
	<table border="0" class="DtTable850"  cellspacing="0" cellpadding="0">
<%
	double dbCount = 0.0;
	intSize = alSetLoad.size();
	//if (intSize > 0)
	//{
	HashMap hmLoop = new HashMap();
	HashMap hmTempLoop = new HashMap();

	String strNextPartNum = "";
	int intCount = 0;
	String strColor = "";
	String strLine = "";
	String strPartNumHidden = "";
	String strPrice = "";
	String strNewPrice = "";
	String strTotal = "";
	String strnewTotal = "";
	String strAmount = "";
	String strnewAmount = "";
	double dbAmount = 0.0;
	double newdbAmount = 0.0;
	double dbTotal = 0.0;
	double newdbTotal = 0.0;
	int intQty = 0;

	for (int i=0;i<intSize;i++)
	{
		hmLoop = (HashMap)alSetLoad.get(i);
		if (i<intSize-1)
		{
			hmTempLoop = (HashMap)alSetLoad.get(i+1);
			strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
		}
		else
		{
			strPartNumHidden = strNextPartNum + ","+strPartNumHidden;
		}
		strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
		strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
		strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
		strTemp = strPartNum;

		if (strPartNum.equals(strNextPartNum))
		{
			intCount++;
			strLine = "<TR><TD colspan=9 height=1 class=Line></TD></TR>";
		}

		else
		{
			strPartNumHidden = strPartNum + ","+strPartNumHidden;
			strLine = "<TR><TD colspan=9 height=1 class=Line></TD></TR>";
			if (intCount > 0)
			{
				strPartNum = "";
				strPartDesc = "";
				strQty = "";
				strLine = "";
			}
			else
			{
				//strColor = "";
			}
			intCount = 0;
		}

		if (intCount > 1)
		{
			strPartNum = "";
			strPartDesc = "";
			strQty = "";
			strLine = "";
		}
			//strPartNumHidden = strPartNum + ","+strPartNumHidden;
		strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
		strItemQty = GmCommonClass.parseNull((String)hmLoop.get("IQTY"));
		strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
		strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
		strNewPrice = GmCommonClass.parseNull((String)hmLoop.get("NEWPRICE"));

		intQty = Integer.parseInt(strItemQty);
		dbAmount = Double.parseDouble(strPrice);
		newdbAmount = Double.parseDouble(strNewPrice);		
		dbAmount = intQty * dbAmount; // Multiply by Qty
		newdbAmount = intQty * newdbAmount; // Multiply by Qty
		strAmount = ""+dbAmount;
		strnewAmount = ""+newdbAmount;

		dbTotal = dbTotal + dbAmount;
		strTotal = ""+dbTotal;
		
		newdbTotal = newdbTotal + newdbAmount;
		strTotal = ""+dbTotal;
		strnewTotal = ""+newdbTotal;

		
%>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td class="RightText" width="25"><%=i+1%></td>
			<td class="RightText" width="60" height="20">&nbsp;<%=strPartNum%></td>
			<td class="RightText" width="330"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
			<td class="RightText" align="center" width="100">&nbsp;<%=strItemQty%></td>
			<td class="RightText" align="center" width="100"><%=strControl%></td>
			<td class="RightText" align="right" width="100">
		
			<% if (strPartyId != "" && !strPartyId.equals("1938")) { // 1938 is n/a%>
				<%=	GmCommonClass.getStringWithCommas(strNewPrice) %>
				
			<%}
			else { %>			
				<%=	GmCommonClass.getStringWithCommas(strPrice) %>
			<%} %>
				
			&nbsp;&nbsp;</td>		
			<td class="RightText" align="right" width="100">
			<% if (strPartyId != "" && !strPartyId.equals("1938")) {%>
				<%=	GmCommonClass.getStringWithCommas(strnewAmount) %>
			<%}
			else { %>
				<%=	GmCommonClass.getStringWithCommas(strAmount) %>
			<%} %>
			&nbsp;&nbsp;</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
<%
	} // end of FOR
%>
<input type="hidden" name="hPartNum" value="<%=strPartNumHidden%>" >
		<tr><td colspan="9" height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1" height="1"></td>
			<td colspan="5" height="25">&nbsp;</td>
			<td width="100" class="RightTableCaption" align="center"><fmtConsignShip:message key="LBL_TOTAL"/></td>
			<td width="100" class="RightTableCaption" align="right"><%=strCurrency%>&nbsp;
			<% if (strPartyId != "" && !strPartyId.equals("1938")) {%>
				<%=	GmCommonClass.getStringWithCommas(strnewTotal) %>
			<%}
			else { %>
				<%=	GmCommonClass.getStringWithCommas(strTotal) %>
			<%} %>
			&nbsp;&nbsp;</td>
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1" height="1"></td>
		</tr>
		
	</table>

<%
	if (strMode.equals("SHIP"))
	{
%>
<span class="RightTableCaption"><fmtConsignShip:message key="LBL_PLEASE_ENTER_SHIPPING_DETAIL"/></span>
	<table border=0 class="DtTable850"  cellspacing=0 cellpadding=0>
		
		<tr>
			<td>
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr class="Shade">
						<td align="right" class="RightText" HEIGHT="24" width="320"><fmtConsignShip:message key="LBL_SHIPPING_DATE"/>:</td>
						<td class="RightText">&nbsp;<gmjsp:calendar textControlName="Txt_SDate" textValue="<%=dtShipDate==null?null:new java.sql.Date(dtShipDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
					</tr>
					<tr>
						<td HEIGHT="24" align="right" class="RightText"><fmtConsignShip:message key="LBL_SHIPPING_CARRIER"/>:</td>
						<td class="RightText">&nbsp;<select name="Cbo_ShipCarr" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
<%
			  		intSize = alCarrier.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alCarrier.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strShipCarr.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>

													</select>
							</td>
					</tr>
					<tr class="Shade">
						<td align="right" HEIGHT="24" class="RightText"><fmtConsignShip:message key="LBL_SHIPPING_MODE"/>:</td>
						<td class="RightText">&nbsp;
					<select name="Cbo_ShipMode" class="RightText" tabindex="3" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
<%
			  		intSize = alMode.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alMode.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strShipMode.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
					</select>
							</td>
					</tr>
					<tr>
						<td align="right" HEIGHT="24" class="RightText"><font color="red">*</font><fmtConsignShip:message key="LBL_TRACKING_NUMBER"/>:</td>
						<td class="RightText">&nbsp;<input type="text" size="40" value="<%=strTrack%>" name="Txt_Track" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="fnChangeTrack(this)" tabindex=4>
						<br>&nbsp;<fmtConsignShip:message key="LBL_USE_BARCODE_SCANNER"/></td>
					</tr>
				</td>
			</tr>
			</table>
			</td>
			</tr>
		</table>
	<%
			if (strPartyId != ""){
	%>
	<span class="RightTableCaption""><fmtConsignShip:message key="LBL_INTER_COMPANY_TRANSFER_DETAILS"/> </span>
	<table border=0 class="DtTable850"  cellspacing=0 cellpadding=0>		
		<tr>
			<td >
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td align="right" HEIGHT="24" class="RightText" width="320"><font color="red">*</font><fmtConsignShip:message key="LBL_FREIGHT_AMOUNT"/>:</td>
						<td class="RightText">&nbsp;<input type="text" size="25" value="<%=strFreightAmt%>" name="Txt_FreightAmt" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex=5>
						<font color="red"><fmtConsignShip:message key="LBL_NOT_AVAILABLE"/></font>						
					</tr>
					<tr>
						<td align="right" HEIGHT="24" class="RightText"><fmtConsignShip:message key="LBL_PO"/>:</td>
						<td class="RightText">&nbsp;<input type="text" size="25" value="<%=strPONum%>" name="Txt_PONum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex=6>
					</tr>
					 				
				</table>
				</td>				
				</tr>				
				</table>
	<% 
		}	
	%>
				<BR>
				<table border=0  cellspacing=0 cellpadding=0 >
					<tr>	
						<td height="30" width="600" align="right">
						<fmtConsignShip:message key="BTN_AGREEMENT_FORM" var="varAgreementForm"/>
						<input type="button" value="${varAgreementForm}" class="button" onClick="fnAgree();" tabindex=13>&nbsp;&nbsp;
						<fmtConsignShip:message key="BTN_PRINT_VERSION" var="varPrintVersion"/>
						<input type="button" value="${varPrintVersion}" class="button" onClick="fnPrintVer();" tabindex=13>&nbsp;&nbsp;
						<fmtConsignShip:message key="BTN_UPDATE" var="varUpdate"/>
						<input type="button" value="&nbsp;${varUpdate}&nbsp;" name="Btn_Update" class="button" onClick="fnUpdateOrder('SaveShip');" >&nbsp;&nbsp;
										<input type="button" value="Release For Sale Lookup" class="button" onClick="fnReleaseForSale();" tabindex=13><BR><BR>				
						</td>					
					</tr>
				</table>
	
					
<%
   }else if (strMode.equals("DummyVerify")) {
 %>
	<BR>
	<table border=0 class="DtTable850"  cellspacing=0 cellpadding=0>	
		<tr>
			<td align="center" height="30">	
				<fmtConsignShip:message key="BTN_VERIFY" var="varVerify"/>						
				<input type="button" value="${varVerify}" name="Btn_Update" class="button" onClick="fnUpdateDummyOrder('SaveShip');" >&nbsp;&nbsp;	
				<fmtConsignShip:message key="BTN_VOID" var="varVoid"/>			
				<input type="button" value="${varVoid}" class="button" onClick="fnVoidDummyConsignment();" tabindex=13>&nbsp;&nbsp;
			</td>
		</tr>
	</table>
 
 <%  
	}else{
%>
	<BR>
	<table border=0 class="DtTable850"  cellspacing=0 cellpadding=0>
		<%
		if(strShipDt.equals("")){
			strShipDt = strruleEnforceDate;
		} %>
		
		<%
		 	if((!GmCommonClass.getStringToDate(strShipDt, strApplDateFmt).after(GmCommonClass.getStringToDate(strruleEnforceDate, strApplDateFmt)))  || (strDistType.equals("70105"))){
		%>				
			<%
			if (strType.equals("40057")){
			%>
			<tr>			
			<td  height="30">
			<span class="RightTableCaption""><fmtConsignShip:message key="LBL_INTER_COMPANY_SALE_SUMMARY"/></span>
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
			<jsp:include page="/accounts/GmICTSummary.jsp" />									
			</td>
			</tr>
			<% } %>
		  <%} %>
		
		<tr>
			<td align="center" height="30" >
			<fmtConsignShip:message key="BTN_AGREEMENT_FORM" var="varAgreementForm"/>
				<input type="button" value="${varAgreementForm}" class="button" onClick="fnAgree();" tabindex=13>&nbsp;&nbsp;
				<fmtConsignShip:message key="BTN_PRINT_VERSION" var="varLoadPrintVersion"/>
				<input type="button" value="${varLoadPrintVersion}" class="button" onClick="fnPrintVer();" tabindex=13>&nbsp;&nbsp;
				<fmtConsignShip:message key="BTN_RELEASE_FOR_SALE_LOOKUP" var="varReleaseForSaleLookup"/>
				<input type="button" value="${varReleaseForSaleLookup}" class="button" onClick="fnReleaseForSale();" tabindex=13>&nbsp;&nbsp;
				
				<% if (strPkSlpNm.equals("SHIPPINGLIST") ) { %>	
				<fmtConsignShip:message key="BTN_SHIPPING_LIST" var="varShippingList"/>	
					<input type="button" value="${varShippingList}" class="button" onClick="fnPackslip();" tabindex=13><BR><BR>
				<% }else {%>
				<fmtConsignShip:message key="BTN_PACKING_SLIP" var="varPackingSlip"/>
					<input type="button" value="${varPackingSlip}" class="button" onClick="fnPackslip();" tabindex=13><BR><BR>
				<%  } 	%>					
				
			</td>
		</tr>
	</table>
	
	
<%
	}
%>

</form>
</BODY>
<%@ include file="/common/GmFooter.inc" %>
</HTML>
