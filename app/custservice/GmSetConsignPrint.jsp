
<%
/**********************************************************************************
 * File		 		: GmSetConsignPrint.jsp
 * Desc		 		: This screen is used for the print version of Set Consignment
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="org.apache.commons.beanutils.DynaBean"%>

<%@ taglib prefix="fmtSetConsignPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- custservice\GmSetConsignPrint.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtSetConsignPrint:setLocale value="<%=strLocale%>"/>
<fmtSetConsignPrint:setBundle basename="properties.labels.custservice.GmSetConsignPrint"/>


<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmConDetails = new HashMap();

	ArrayList alSetLoad = new ArrayList();
	ArrayList alChildRequest = new ArrayList();
	alChildRequest = (ArrayList)request.getAttribute("CHILDREQUESTLIST");
	
	 	
	int intBackSize = 0;
	if (alChildRequest != null)
	{
		intBackSize = alChildRequest.size();
	}
	String strShade = "";

	String strConsignId = "";
	String strConsignAdd = "";
	String strShipAdd = "";
	String strDate = "";
	
	String strTemp = "";
	String strItemQty = "";
	String strControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strShipDate = "";
	String strShipCarr = "";
	String strShipMode = "";
	String strTrack = "";
	String strUserName = "";
	String strPrintTitle = request.getAttribute("printTitle") == null ?"Consignment":(String)request.getAttribute("printTitle");

	if (hmReturn != null)
	{
		alSetLoad = (ArrayList)hmReturn.get("SETLOAD");
		hmConDetails = (HashMap)hmReturn.get("SETDETAILS");
		strConsignId = (String)hmConDetails.get("CID");
		strConsignAdd = (String)hmConDetails.get("BILLADD");
		strShipAdd = (String)hmConDetails.get("SHIPADD");
		strDate = (String)hmConDetails.get("UDATE");
		strUserName = (String)hmConDetails.get("NAME");
		strShipDate = GmCommonClass.parseNull((String)hmConDetails.get("SDATE"));
		strShipCarr = GmCommonClass.parseNull((String)hmConDetails.get("SCARR"));
		strShipMode = GmCommonClass.parseNull((String)hmConDetails.get("SMODE"));
		strTrack = GmCommonClass.parseNull((String)hmConDetails.get("TRACK"));		
	}
	Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);
	String strConsignmentId = GmCommonClass.parseNull((String)request.getAttribute("CONSIGNMENTID"));			
	String strRequestId = GmCommonClass.parseNull((String)request.getAttribute("REQUESTID"));	
	log.debug("here............. strRequestId is " + strRequestId);
	int intSize = 0;
	HashMap hcboVal = null;

	StringBuffer sbStartSection = new StringBuffer();
	sbStartSection.append("<table border=0 width=700 cellspacing=0 cellpadding=0 align=center>");
	sbStartSection.append("<tr><td bgcolor=#666666 colspan=3></td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 width=1></td>");
	sbStartSection.append("<td>");
	sbStartSection.append("<table cellpadding=0 cellspacing=0 border=0 width='100%'>");
	sbStartSection.append("<tr>");
	sbStartSection.append("<td width=170><img src=");sbStartSection.append(strImagePath);
	sbStartSection.append("/s_logo.gif width=138 height=60></td>");
	sbStartSection.append("<td class=RightText width=340>Globus Medical Inc.<br>Valley Forge Business Center<br>2560 General Armistead Avenue<br>Audubon, PA 19403 <br></td>");
	sbStartSection.append("<td align=right class=RightText width=500><font size=+3>"+strPrintTitle+"</font>&nbsp;</td>");
	sbStartSection.append("</tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 colspan=3></td></tr>");
	sbStartSection.append("<tr><td colspan=3>");
	sbStartSection.append("<table border=0 width=100% cellspacing=0 cellpadding=0>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption>");
	sbStartSection.append("<td height=24 align=center width=250>&nbsp;Consigned To</td>");
	sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=11></td>");
	sbStartSection.append("<td width=250 align=center>&nbsp;Ship To</td>");
	sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=11></td>");
	sbStartSection.append("<td width=100 align=center>&nbsp;Date</td>");
	sbStartSection.append("<td width=100 align=center>&nbsp;Consign #</td>");
	sbStartSection.append("</tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
	sbStartSection.append("<tr><td class=RightText rowspan=5 valign=top>&nbsp;");sbStartSection.append(strConsignAdd);
	sbStartSection.append("</td>");
	sbStartSection.append("<td rowspan=5 class=RightText valign=top>&nbsp;");sbStartSection.append(strShipAdd);
	sbStartSection.append("</td>");
	sbStartSection.append("<td height=25 class=RightText align=center>&nbsp;");sbStartSection.append(strDate);
	sbStartSection.append("</td><td class=RightText align=center>&nbsp;");
	sbStartSection.append(strConsignId);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption><td align=center>&nbsp;</td><td align=center>Consigned By</td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	sbStartSection.append("<tr><td align=center class=RightText>&nbsp;");
	sbStartSection.append("<Br></td><td align=center height=22 nowrap class=RightText>&nbsp;");sbStartSection.append(strUserName);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
	sbStartSection.append("<tr class=RightTableCaption bgcolor=#eeeeee><td height=20 align=center>Ship Via</td>");
	sbStartSection.append("<td align=center>Ship Mode</td><td align=center colspan=2>Tracking #</td>");
	sbStartSection.append("</tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
	sbStartSection.append("<tr><td height=18 align=center class=RightText>");
	sbStartSection.append(strShipCarr);
	sbStartSection.append("</td><td align=center class=RightText>");
	sbStartSection.append(strShipMode);
	sbStartSection.append("</td><td align=center colspan=2 class=RightText>");
	sbStartSection.append(strTrack);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
	sbStartSection.append("</table></td></tr>");
	sbStartSection.append("<tr><td colspan=3><table border=0 width=100% cellspacing=0 cellpadding=0>");
	sbStartSection.append("<tr class=RightTableCaption><td width=25 height=24>S#</td><td width=60 height=24>&nbsp;Part #</td>");
	sbStartSection.append("<td  width=420>Description</td><td align=center width=60>Qty</td>");
	sbStartSection.append("<td align=center width=80>Control #</td>");
	sbStartSection.append("<td align=center width=80>&nbsp;&nbsp;Price</td><td align=center width=80>Amount</td></tr>");
	sbStartSection.append("</table></td></tr></table></td><td bgcolor=#666666 width=1></td></tr></table>");

	StringBuffer sbBreakStartSection = new StringBuffer();
	//sbBreakStartSection.append("<table border=0 width=700 cellspacing=0 cellpadding=0 align=center>");
	//sbBreakStartSection.append("<tr><td bgcolor=#666666 colspan=3></td></tr>");
	//sbBreakStartSection.append("<tr ><td>");
	sbBreakStartSection.append("<table border=0 width=700 cellspacing=0 cellpadding=0 align=center>");
	sbBreakStartSection.append("<TR><TD colspan=9 height=1 bgcolor=#666666></TD></TR>");
	sbBreakStartSection.append("<tr class=RightTableCaption> ");
	sbBreakStartSection.append("<td bgcolor=#666666 width=1></td><td width=25 height=24>S#</td><td width=60 height=24>&nbsp;Part #</td>");
	sbBreakStartSection.append("<td  width=420>Description</td><td align=center width=60>Qty</td>");
	sbBreakStartSection.append("<td align=center width=80>Control #</td>");
	sbBreakStartSection.append("<td align=center width=80>&nbsp;&nbsp;Price</td><td align=center width=80>Amount</td>");
	sbBreakStartSection.append("<td bgcolor=#666666 width=1></td></tr>");
	//sbBreakStartSection.append("</table>");
	
	StringBuffer sbEndSection = new StringBuffer();
	sbEndSection.append("<table width=700 cellspacing=0 cellpadding=0 align=center>");
	sbEndSection.append("<tr><td bgcolor=#666666 width=1 rowspan=7></td><td height=1 width=698></td><td bgcolor=#666666 width=1 rowspan=7></td></tr>");
	sbEndSection.append("<tr><td height=10>&nbsp;</td></tr>");
	sbEndSection.append("<tr><td height=50>&nbsp;</td></tr>");
	sbEndSection.append("<tr><td height=20 class=RightText valign=top><B>Consigning Agent</B></td></tr>");
	sbEndSection.append("<tr><td height=30 class=RightText valign=top><B><u>NOTES</u></B>:");
	if(strPrintTitle.equals("Consignment")){
	sbEndSection.append("<BR>-Please sign and return the Consignment Agreement attached with this Form");
	} else {
	sbEndSection.append("<BR>-Instruments for Spinal Surgery.");
	sbEndSection.append("<BR>-Instruments being supplied by the parent company free of charge for customer site / market evaluation.");
	sbEndSection.append("<BR>-Prices stated are commercial prices only");
	sbEndSection.append("<BR>-No Payment necessary for the items supplied");
	}
	sbEndSection.append("</td></tr>");
	sbEndSection.append("<tr><td height=1 bgcolor=#666666></td></tr></table>");
	sbEndSection.append("<table border=0 width=700 cellspacing=0 cellpadding=0><tr><td align=center height=30 id=button>");
	sbEndSection.append("<input type=button value=&nbsp;Print&nbsp; name=Btn_Print class=button onClick=fnPrint();>&nbsp;&nbsp;");
	sbEndSection.append("</td><tr></table>");

	StringBuffer sbBackOrderSection = new StringBuffer();
	sbBackOrderSection.append("<table width=700 cellspacing=0 cellpadding=0 align=center>");
	sbBackOrderSection.append("<tr><td colspan=10 height=1 bgcolor=#666666></td></tr>");
	sbBackOrderSection.append("<tr bgcolor=#eeeeee class=RightTableCaption>");
	sbBackOrderSection.append("<td bgcolor=#666666 width=1></td>");
	sbBackOrderSection.append("<td height=25  colspan=8 >Items not in Initial Shipment </td>");
	sbBackOrderSection.append("<td bgcolor=#666666 width=1></td>");
	sbBackOrderSection.append("</tr>");
	sbBackOrderSection.append("<tr class=RightTableCaption>");
	sbBackOrderSection.append("<td bgcolor=#666666 width=1></td>");
	sbBackOrderSection.append("<td width=25 height=24>S#</td>");
	sbBackOrderSection.append("<td width=60 height=24>&nbsp;Part #</td>"); 
	sbBackOrderSection.append("<td  width=300>Description</td>");
	sbBackOrderSection.append("<td align=center width=30>Qty</td>"); 
	sbBackOrderSection.append("<td align=center width=80>Control #</td>"); 
	sbBackOrderSection.append("<td align=center width=80>Status</td>"); 
	sbBackOrderSection.append("<td align=center width=100>CN</td>"); 
	sbBackOrderSection.append("<td align=center width=90>RQ</td>"); 
	sbBackOrderSection.append("<td bgcolor=#666666 width=1></td>");
	sbBackOrderSection.append("</td></tr>");	
	sbBackOrderSection.append("");
	 
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Consignment </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<!--  <script language="JavaScript" src="<%=strJsPath%>/watermark.js"></script>	-->
<style>
TR.PageBreak {
     page-break-after: always;
}
</style>
<script>

function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
</HEAD>

<BODY topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<!--StartSection starts here-->
<%out.print(sbStartSection);%>
<!--StartSection ends here-->
	<table border="0" width="700" cellspacing="0" cellpadding="0" align=center>
<%
	double dbCount = 0.0;
	intSize = alSetLoad.size();
	//if (intSize > 0)
	//{
	HashMap hmLoop = new HashMap();
	HashMap hmTempLoop = new HashMap();

	String strNextPartNum = "";
	int intCount = 0;
	String strColor = "";
	String strLine = "";
	String strPartNumHidden = "";
	String strPrice = "";
	String strTotal = "";
	String strAmount = "";
	double dbAmount = 0.0;
	double dbTotal = 0.0;
	int intQty = 0;
	int j = 0;
	int k = 0;
	
	for (int i=0;i<intSize;i++)
	{
		j++;
		k++;
		hmLoop = (HashMap)alSetLoad.get(i);
		if (i<intSize-1)
		{
			hmTempLoop = (HashMap)alSetLoad.get(i+1);
			strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
		}
		strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
		strPartNumHidden = strPartNum;
		strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
		strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
		strTemp = strPartNum;

		if (strPartNum.equals(strNextPartNum))
		{
			intCount++;
			strLine = "<TR><TD colspan=9 height=1 bgcolor=#eeeeee></TD></TR>";
		}

		else
		{
			strLine = "<TR><TD colspan=9 height=1 bgcolor=#eeeeee></TD></TR>";
			if (intCount > 0)
			{
				strPartNum = "";
				strPartDesc = "";
				strQty = "";
				strLine = "";
			}
			else
			{
				//strColor = "";
			}
			intCount = 0;
		}

		if (intCount > 1)
		{
			strPartNum = "";
			strPartDesc = "";
			strQty = "";
			strLine = "";
		}
	
		strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
		strItemQty = GmCommonClass.parseNull((String)hmLoop.get("IQTY"));
		strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
		strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));

		intQty = Integer.parseInt(strItemQty);
		dbAmount = Double.parseDouble(strPrice);
		dbAmount = intQty * dbAmount; // Multiply by Qty
		strAmount = ""+dbAmount;

		dbTotal = dbTotal + dbAmount;
		strTotal = ""+dbTotal;

		out.print(strLine);

		if ((j != 0 && j % 45 ==0) || k == 30)
		{
			j = 0;
%>
			<tr><td colspan="9" height="1" bgcolor="#666666"></td></tr>
			<tr><td colspan="9" height="20" class="RightText" align="right"><fmtSetConsignPrint:message key="LBL_CONT_NXT_PAGE"/></td></tr>
			</table>
			<p STYLE="page-break-after: always"></p>
			<%out.print(sbBreakStartSection);%>
<%		} //end of IF
%>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td class="RightText" width="25"><%=i+1%></td>
			<td class="RightText" width="60" height="20">&nbsp;<%=strPartNum%></td>
			<td class="RightText" width="520"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
			<td class="RightText" align="center" width="60">&nbsp;<%=strItemQty%></td>
			<td class="RightText" align="center" width="80"><%=strControl%></td>
			<td class="RightText" align="right" width="80"><%=GmCommonClass.getStringWithCommas(strPrice)%>&nbsp;&nbsp;</td>
			<td class="RightText" align="right" width="80"><%=GmCommonClass.getStringWithCommas(strAmount)%>&nbsp;&nbsp;</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
<%
	} // end of FOR
%>
		<tr><td colspan="9" height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1" height="1"></td>
			<td colspan="5" height="25">&nbsp;</td>
			<td width="80" class="RightTableCaption" align="center">Total</td>
			<td width="80" class="RightTableCaption" align="right">$<%=GmCommonClass.getStringWithCommas(strTotal)%>&nbsp;&nbsp;</td>
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1" height="1"></td>
		</tr>
		<tr><td colspan="9" height="1" bgcolor="#666666"></td></tr>
	
	
<%
	// ************ BackOrderSection starts here  ************ 
	if (intBackSize > 0) 
	{
%>	
	<tr><td colspan="10">
		<%out.print(sbBackOrderSection);%>

<%  

	for (int i=0;i<intBackSize;i++)
	{
		j++;
		k++;
		DynaBean db = (DynaBean)alChildRequest.get(i);
		 
		String strPNum = String.valueOf(db.get("PNUM"));
		String strPDesc = String.valueOf(db.get("PDESC"));
		String strCRQty = String.valueOf(db.get("CRQTY"));
		String strCtrlnum = String.valueOf(db.get("CONTROLNUM"));
		String strStatus = String.valueOf(db.get("STATUS_FL"));
		String strConid = String.valueOf(db.get("CID"));
		String strReqid = String.valueOf(db.get("REQID"));
		 
		 
		strLine = "<TR><TD colspan=10 height=1 bgcolor=#eeeeee></TD></TR>";
	
		out.print(strLine);

		if ((j != 0 && j % 35 ==0) || k == 30)
		{
			j = 0;
	%>
				<tr><td colspan="10" height="1" bgcolor="#666666"></td></tr>
				<tr><td colspan="10" height="20" class="RightText" align="right"><fmtSetConsignPrint:message key="LBL_CONT_NXT_PAGE"/></td></tr>
				</table>
				
				<p STYLE="page-break-after: always"></p>
				<%out.print(sbBackOrderSection);%>
<%		}  // Page break 
	%>
			<tr>
				<td bgcolor="#666666" width="1"></td>
				<td class="RightText" width="25"><%=i+1%></td>
				<td class="RightText" width="60" height="20">&nbsp;<%=strPNum%></td>
				<td class="RightText" width="300"><%=strPDesc%></td>
				<td class="RightText" align="center" width="30">&nbsp;<%=strCRQty%></td>
				<td class="RightText" align="center" width="80"><%=strCtrlnum%></td>
				<td class="RightText" align="left" width="80"><%=strStatus%>&nbsp;&nbsp;</td>
				<td class="RightText" align="right" width="100"><%=strConid%>&nbsp;&nbsp;</td>
				<td class="RightText" align="right" width="90"><%=strReqid%>&nbsp;&nbsp;</td>
				<td bgcolor="#666666" width="1"></td>
			</tr>
<% 	} // back order for loop 
%>


</table>
</td></tr>	
<%
	}
%>
<!--BackOrderSection ends here-->

<!--EndSection starts here-->
<%out.print(sbEndSection);%>
<!--EndSection ends here-->
	<%
	if(!strPrintTitle.equals("Consignment"))
	{
	%>
	
	<div id="waterMark" style="position:absolute; z-Index: 0; top: 340; left: 340">
		<img src="<%=strImagePath%>/obo.gif"  border=0>
	</div>
	
	<% } %>
</table>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
