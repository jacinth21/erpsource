
<%
/**********************************************************************************
 * File		 		: GmConsignNetReport.jsp
 * Desc		 		: This screen is used for the displaying Net Count of Sets - By distributor
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtConsignNetReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- custservice\GmConsignNetReport.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtConsignNetReport:setLocale value="<%=strLocale%>"/>
<fmtConsignNetReport:setBundle basename="properties.labels.custservice.GmConsignNetReport"/>


<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmLoop = new HashMap();
	HashMap hcboVal = new HashMap();

	String strDistId = (String)request.getAttribute("hDistId")==null?"":(String)request.getAttribute("hDistId");
	String strhAction = (String)request.getAttribute("hAction")==null?"Load":(String)request.getAttribute("hAction");
	String strOpt = (String)request.getAttribute("hOpt")==null?"Load":(String)request.getAttribute("hOpt");
	
	String strSetId = "";
	String strSetName = "";
	String strNetCount = "";

	String strShade = "";
	String strCodeID = "";
	String strSelected = "";

	int intLoop = 0;
	int intDistLength = 0;

	ArrayList alDistributor = new ArrayList();
	ArrayList alReport = new ArrayList();

	if (hmReturn != null) 
	{
		alDistributor = (ArrayList)hmReturn.get("DISTLIST");
		alReport = (ArrayList)hmReturn.get("REPORT");
	}

	if (alDistributor != null)
	{
		intDistLength = alDistributor.size();
	}
	
	if (alReport != null)
	{
		intLoop = alReport.size();
	}

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Consignment Net Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>

function fnReload()
{
	document.frmAccount.hAction.value = 'Reload';
	document.frmAccount.submit();
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmConsignmentReportServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hOpt" value="<%=strOpt%>">

	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="20" width="1" class="Line"></td>
			<td colspan="2" height="1" bgcolor="#666666"></td>
			<td rowspan="20" width="1" class="Line"></td>
		</tr>
		<tr>
			<td colspan="2" height="25" class="RightDashBoardHeader">
				<fmtConsignNetReport:message key="LBL_SETS_REPORT_NET"/>
			</td>
		</tr>
		<tr><td class="Line" height="1" colspan="2"></td></tr>
		<tr>
			<td class="RightText" align="right" HEIGHT="24">&nbsp;<fmtConsignNetReport:message key="LBL_DISTRIBUTOR"/>:</td>
			<td>&nbsp;
				<select name="Cbo_Dist" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" onChange="javascript:fnReload();">
					<option value="0" ><fmtConsignNetReport:message key="LBL_CHOOSE_ONE"/>
<%
					hcboVal = new HashMap();
			  		for (int i=0;i<intDistLength;i++)
			  		{
			  			hcboVal = (HashMap)alDistributor.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strDistId.equals(strCodeID)?"selected":"";
%>
					<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%
			  		}
%>

				</select>
			</td>
		</tr>
		<tr><td class="Line" colspan="2"></td></tr>
		<tr>
			<td colspan="2" align="center">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="80"><fmtConsignNetReport:message key="LBL_SET_ID"/></td>
						<td width="300"><fmtConsignNetReport:message key="LBL_SET_NAME"/></td>
						<td width="100" align="center"><fmtConsignNetReport:message key="LBL_NET_COUNT"/></td>
					</tr>
					<tr><td class="Line" colspan="3"></td></tr>
<%
			if (intLoop > 0)
			{
				for (int i = 0;i < intLoop ;i++ )
				{
					hmLoop = (HashMap)alReport.get(i);
					strSetId = GmCommonClass.parseNull((String)hmLoop.get("SETID"));
					strSetName = GmCommonClass.parseNull((String)hmLoop.get("SNAME"));
					strNetCount = GmCommonClass.parseNull((String)hmLoop.get("NETCNT"));
					strShade	= (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
%>
					<tr <%=strShade%>>
						<td class="RightText" height="20">&nbsp;<%=strSetId%></a></td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strSetName)%></td>
						<td class="RightText" align="center"><%=strNetCount%></td>
					</tr>
<%
				}
			} else	{
%>
					<tr>
						<td height="30" colspan="3" align="center" class="RightTextBlue"><fmtConsignNetReport:message key="LBL_NO_DATA_AVAILABLE"/></td>
					</tr>
<%
		}
%>
					<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
				</table>

			</td>
		</tr>
	</table>
		
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
