<%
/**********************************************************************************
 * File		 		: GmMasterBarcode.jsp
 * Desc		 		: Master Barcode 
 * Version	 		: 1.0
 * author			: Tamizhthangam
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 <!-- custservice\shipping\GmMasterBarcode.jsp-->
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtMasterBarcode" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>


<%
    String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strWikiTitle = GmCommonClass.getWikiTitle("MASTER_BARCODE");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Master BarCode </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmMasterBarcode.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>

</HEAD>

<body leftmargin="20" topmargin="10" >
<html:form action="/gmMasterBarcode.do">

 <!-- Custom tag lib code modified for JBOSS migration changes -->
<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr height="25">
			<td colspan="4" class="RightDashBoardHeader">Master Barcode</td>
				<td colspan="1" class="RightDashBoardHeader" align="right" width="20"><fmtMasterBarcode:message key="IMG_HELP" var="varHelp" />
				 <img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr> 
		<tr class="shade"> 
			<td class="RightTableCaption" align="right" HEIGHT="30"><b>Order Number :&nbsp;</b></td>
			<td class="RightText">
			    <input type="text" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" class="InputArea" 
			    name="orderId" style="text-transform:uppercase;" tabindex="1">&nbsp;&nbsp;
			</td>  
			<td align="let" width="40%">
				<gmjsp:button name="loadReport" value="&nbsp;Generate PDF&nbsp;" gmClass="button" onClick="fnGenerateBarcode();" buttonType="Load"  tabindex="2" />
			 </td>	
		</tr>
		<tr><td colspan="3"><span id="noPartErrMsg" style="color: red;margin-left: 215px;"></span> <span id="successMsg" style="color: green;"></span></td></tr>
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>

</BODY>
</HTML>
