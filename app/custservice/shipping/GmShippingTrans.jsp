
<%
/**********************************************************************************
 * File		 		: GmShippingTrans.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtShippingTrans" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>


<!-- \CS Label changes\custservice\shipping\GmShippingTrans.jsp --> 

<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}

%>
<fmtShippingTrans:setLocale value="<%=strLocale%>"/>
<fmtShippingTrans:setBundle basename="properties.labels.custservice.shipping.GmShippingTrans"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strOpt = (String)request.getAttribute("hOpt")==null?"":(String)request.getAttribute("hOpt");
	
	ArrayList alReport = new ArrayList();
	int intCount = 0;
	
	if (hmReturn != null) 
	{
		alReport = (ArrayList)hmReturn.get("REPORT");
	}
	
	if (!alReport.isEmpty())
	{
		intCount = alReport.size();	
	}
	
	HashMap hmLoop = new HashMap();
	String strId = "";
	String strRefId = "";
	String strShipTo = "";
	String strCDate = "";
	String strCUser = "";
	String strSDate = "";
	String strSource = "";
	String strMode = "";
	String strModeId = "";
	String strCarr = "";
	String strCarrId = "";
	String strTrack = "";

	HashMap hmTempLoop = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Number Search</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
function fnSubmit()
{
	Error_Clear();
	var cnt = document.frmAccount.hCnt.value;
	var obj = '';
	var trkobj = '';
	var modeobj = '';
	var carrobj = '';
	var str = '';
	for (var i=0;i < cnt; i++)
	{
		trkobj = eval("document.frmAccount.Txt_Track"+i);
		if (trkobj.value != '')
		{
			obj = eval("document.frmAccount.hId"+i);
			modeobj = eval("document.frmAccount.Cbo_ModeId"+i);
			carrobj = eval("document.frmAccount.Cbo_CarrId"+i);
			str = str + obj.value + '^' + modeobj.value + '^' + carrobj.value + '^' + trkobj.value + '|';
		}
	}
	
	if (str == '')
	{
		Error_Details(message_operations[701]);
	}
	
	if (ErrorCount > 0)
	{
		Error_Show();
		return false;
	}else{	
		document.frmAccount.hInputStr.value = str;
		document.frmAccount.hAction.value = "SaveShip";
		document.frmAccount.submit();
	}	
}

function fnChangeTrack(obj)
{
	changeBgColor(obj,'#ffffff');
	var val = obj.value;
	var len = val.length;

	if (len == 32) // for FedEX Express Shipping
	{
		val = val.substring(16,28);
		obj.value = val;
	}
	else if (len == 22) // for FedEX Ground Shipping
	{
		val = val.substring(7,22);
		obj.value = val;
	}
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmShippingServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hInputStr" value="">
	<table border="0" width="900" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="15" width="1" class="Line"></td>
			<td height="1" bgcolor="#666666"></td>
			<td rowspan="15" width="1" class="Line"></td>
		</tr>
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtShippingTrans:message key="LBL_SHIPPING_REQUESTS_PENDING"/></td>
		</tr>
		<tr><td class="Line" height="1"></td></tr>
		<tr>
			<td align="center">
				<table border="0" width="898" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td width="100">&nbsp;<fmtShippingTrans:message key="LBL_REF_ID"/></td>
						<td width="100">&nbsp;<fmtShippingTrans:message key="LBL_SHIP_TO"/></td>
						<td width="100">&nbsp;<fmtShippingTrans:message key="LBL_SHIP_DATE"/></td>
						<td width="100">&nbsp;<fmtShippingTrans:message key="LBL_CREATED_BY"/></td>
						<td width="100">&nbsp;<fmtShippingTrans:message key="LBL_CREATED_DATE"/></td>
						<td width="100">&nbsp;<fmtShippingTrans:message key="LBL_SOURCE"/></td>
						<td width="100"><fmtShippingTrans:message key="LBL_SHIP_MODE"/></td>
						<td width="100"><fmtShippingTrans:message key="LBL_SHIP_CARRIER"/></td>
						<td HEIGHT="24" align="center" width="150"><fmtShippingTrans:message key="LBL_TRACKING"/></td>
					</tr>
					<tr><td class="Line" colspan="9" height="1"></td></tr>
<%
			if (intCount > 0)
			{
				for (int i = 0;i < intCount ;i++ )
				{
					hmLoop = (HashMap)alReport.get(i);
					strId = GmCommonClass.parseNull((String)hmLoop.get("SID"));
					strRefId = GmCommonClass.getStringWithTM((String)hmLoop.get("REFID"));
					strShipTo = GmCommonClass.getStringWithTM((String)hmLoop.get("SHIPTONM"));
					strCDate = GmCommonClass.getStringWithTM((String)hmLoop.get("CDATE"));
					strCUser = GmCommonClass.getStringWithTM((String)hmLoop.get("CUSER"));
					strSDate = GmCommonClass.getStringWithTM((String)hmLoop.get("SDATE"));
					strSource = GmCommonClass.getStringWithTM((String)hmLoop.get("SOURCE"));
					strMode = GmCommonClass.parseNull((String)hmLoop.get("SMODE"));
					strModeId = GmCommonClass.parseNull((String)hmLoop.get("SMODEID"));
					strCarr = GmCommonClass.parseNull((String)hmLoop.get("SCARR"));
					strCarrId = GmCommonClass.parseNull((String)hmLoop.get("SCARRID"));
					strTrack = GmCommonClass.parseNull((String)hmLoop.get("TRACK"));
%>
					<tr>
						<td class="RightText">&nbsp;<%=strRefId%>
						<input type="hidden" name="hId<%=i%>" value="<%=strId%>"></td>
						<td class="RightText">&nbsp;<%=strShipTo%></td>
						<td class="RightText">&nbsp;<%=strSDate%></td>
						<td class="RightText">&nbsp;<%=strCUser%></td>
						<td class="RightText">&nbsp;<%=strCDate%></td>
						<td class="RightText">&nbsp;<%=strSource%></td>
						<td class="RightText">&nbsp;<%=strMode%>
						<input type="hidden" name="Cbo_ModeId<%=i%>" value="<%=strModeId%>"></td>
						<td height="20">&nbsp;<%=strCarr%>
						<input type="hidden" name="Cbo_CarrId<%=i%>" value="<%=strCarrId%>"></td>
						<td class="RightText"><input type="text" size="30" value="<%=strTrack%>" name="Txt_Track<%=i%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="fnChangeTrack(this)" tabindex=4></td>
					</tr>
					<tr><td colspan="9" height="1" bgcolor="#eeeeee"></td></tr>
<%
				}// end of FOR
				
%>
					<tr>
						<td height="30" colspan="9" align="center">
						<fmtShippingTrans:message key="BTN_SUBMIT" var="varSubmit"/>
						<gmjsp:button value="${varSubmit}" gmClass="button" onClick="fnSubmit();" tabindex="13" buttonType="Save" /></td>
					</tr>
<%								
			} else	{
%>
					<tr>
						<td height="30" colspan="9" align="center" class="RightTextBlue"><fmtShippingTrans:message key="LBL_NO_SHIPPING_REQUESTS_AT_THIS_TIME"/></td>
					</tr>
<%
		}
%>
					<tr><td colspan="9" height="1" bgcolor="#666666"></td></tr>
					<input type="hidden" name="hCnt" value="<%=intCount%>"/>
				</table>
			</td>
		</tr>
	</table>


</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
