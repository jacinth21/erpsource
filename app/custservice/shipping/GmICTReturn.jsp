<!--custservice\shipping\GmICTReturn.jsp  -->
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmJasperReport"%>
<%@ page import="java.util.HashMap,java.util.*,java.text.*"%>
<bean:define id="hmJasperDetails" name="frmICTReturn"	property="hmJasperDetails" type="java.util.HashMap"></bean:define>
<bean:define id="alReturn" name="frmICTReturn" property="alReturn" type="java.util.ArrayList"></bean:define> 
<%
 
String strHtmlJasperRpt = "";
String strJasperName= GmCommonClass.parseNull((String) hmJasperDetails.get("JASPERNAME") );
String strOpt = GmCommonClass.parseNull((String) hmJasperDetails.get("STROPT"));
%>
<HTML>
<HEAD>
 <script>
 var hopt='<%=strOpt%>';
 function fnWPrint()
 {
 	if(hopt == 'autoprint'){
 		setTimeout(fnAutoPrint,1);
 	}
 }
 function fnAutoPrint(){
		var OLECMDID = 6;
		var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
		document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
		WebBrowser1.ExecWB(OLECMDID, 6);
		WebBrowser1.outerHTML = "";
	} 
 
 </script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnWPrint();">
	<table>
		<tr>
		<td><div>
			 <%
			    if (!hmJasperDetails.equals("")) {
			%>
			<td><div>
					<%
						String strJasperPath = GmCommonClass
									.getString("GMJASPERLOCATION");
							GmJasperReport gmJasperReport = new GmJasperReport();
							gmJasperReport.setRequest(request);
							gmJasperReport.setResponse(response);
							gmJasperReport.setJasperReportName(strJasperName);
							gmJasperReport.setHmReportParameters(hmJasperDetails);
							gmJasperReport.setReportDataList(alReturn);
							gmJasperReport.setBlDisplayImage(true);
							strHtmlJasperRpt = gmJasperReport.getXHtmlReport();
						
						}
					%>
					<%=strHtmlJasperRpt %>
				</div></td>
		</tr>

	</table>
	 
</BODY>
</HTML>