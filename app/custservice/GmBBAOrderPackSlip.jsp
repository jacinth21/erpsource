 <%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%
/**********************************************************************************
 * File		 		: GmBBAOrderPackSlip.jsp
 * Desc		 		: This screen is used for the BBA Packing Slip
 * Version	 		: 1.0
 * author			: HReddi
************************************************************************************/
%>
<!-- \custservice\GmBBAOrderPackSlip.jsp -->
<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@page import="java.util.Date"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%
	String strHtmlJasperRpt = "";
%>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strAction = null;
	String strSource = "";
	
	String strCompId = "";
	String strCompanyName = "";
	String strCompanyShortName = "";
	String strCompanyAddress = "";
	String strCompanyPhone = "";
	String strHeadAddress = "";
	String strShipTaxId = "";
	String strShipPurpose = "";
	String strShipTermSale = "";
	String strExportDate = "";
	String strConsignTaxID = "";
	String strConsignName = "";
	String strConsignPhone = "";
	String strConsignRegn = "";
	String strHormoTerrif = "";
	String strCommercialAddress = "";
	String strCommercialHtmlJasperRpt = "";
	String strCompanyLogo = "";
	String strShipCost = "";
	String strjasName = "/GmBBAOrderCommercialPrint.jasper";
	String strApplnDateFmt = strGCompDateFmt;
	String strTodaysDate = GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate"));
	SimpleDateFormat dfDateFmt = new SimpleDateFormat(strApplnDateFmt+" HH:mm:ss");
	
	strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	strAction = (strAction.trim() == null) ? "PrintPack" : strAction;
	strAction = (strAction.equals("")) ? "PrintPack" : strAction;
    String strOpt = GmCommonClass.parseNull(request.getParameter("hOpt"));
	strSource = GmCommonClass.parseNull((String)request.getAttribute("SOURCE"));
	String strJasperName = GmCommonClass.parseNull((String)request.getAttribute("PACKJASPERNAME"));
	String strIsICSAccount = GmCommonClass.parseNull((String)request.getAttribute("ISICSACCOUNT"));
	String strPackHeader = GmCommonClass.parseNull((String)request.getAttribute("PACKHEADER"));
	String strPackFooter = GmCommonClass.parseNull((String)request.getAttribute("PACKFOOTER"));
	Date dtFromDate= null;
	Date dtPrintDate= new Date();
	HashMap hmReturn = new HashMap();
	HashMap hmICSData = new HashMap();
	HashMap hmReportHeaderDetails = new HashMap();
	ArrayList alPopulatedCartDtls = new ArrayList();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmorderDetails= (HashMap)hmReturn.get("ORDERDETAILS");
	alPopulatedCartDtls =  GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("alCartDetails"));
	//ArrayList  alCartDetails = (ArrayList)hmReturn.get("CARTDETAILS");
	if(alPopulatedCartDtls.isEmpty() && alPopulatedCartDtls != null)
	{
	    alPopulatedCartDtls = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("CARTDETAILS"));
	}
	
	hmICSData = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("ICSDATA"));
	
	String strOrdID = GmCommonClass.parseNull((String)hmorderDetails.get("ID"));
	String strOrdDate = GmCommonClass.getStringFromDate((java.util.Date)hmorderDetails.get("ODT"),strApplnDateFmt); 
	String strShipAddress = GmCommonClass.parseNull((String)hmorderDetails.get("SHIPADD"));
	String strCustPO = GmCommonClass.parseNull((String)hmorderDetails.get("PO"));
	String strShipCarrier = GmCommonClass.parseNull((String)hmorderDetails.get("CARRIER"));
	String strShipMode = GmCommonClass.parseNull((String)hmorderDetails.get("SHIPMODE"));
	String strAttenOver = GmCommonClass.parseNull((String)hmorderDetails.get("ATTENTO"));
	strShipCost = GmCommonClass.parseNull((String)hmorderDetails.get("SHIPCOST"));
	String strUserID = GmCommonClass.parseNull((String)hmorderDetails.get("USERID"));
	String strUsername = GmCommonClass.parseNull((String)hmorderDetails.get("UNAME"));
	//String strPrintDate = strTodaysDate;
	dtFromDate = GmCommonClass.getStringToDate(strOrdDate,strApplnDateFmt);
	//dtPrintDate = GmCommonClass.getStringToDate(strTodaysDate,strApplnDateFmt);
	String strPrintDate = dfDateFmt.format(dtPrintDate); //To get the current date with time
	strCompId	= GmCommonClass.parseNull((String)hmorderDetails.get("COMPID"));
	String strDivisionId = GmCommonClass.parseNull((String)hmorderDetails.get("DIVISION_ID"));
	if(hmICSData.size() > 0){
		strShipTaxId = GmCommonClass.parseNull((String)hmICSData.get("SHIPTAXID"));
		strShipPurpose = GmCommonClass.parseNull((String)hmICSData.get("SHIPPURPOSE"));
		strShipTermSale = GmCommonClass.parseNull((String)hmICSData.get("TERMOFSALE"));
		strConsignTaxID = GmCommonClass.parseNull((String)hmICSData.get("CONSIGNEETAXID"));
		strConsignName = GmCommonClass.parseNull((String)hmICSData.get("CONSIGNEEPERSON"));
		strConsignPhone = GmCommonClass.parseNull((String)hmICSData.get("CONSIGNEEPHONE"));
		strConsignRegn = GmCommonClass.parseNull((String)hmICSData.get("CONSIGNEEREGION"));
		strHormoTerrif = GmCommonClass.parseNull((String)hmICSData.get("HARMCODE"));
		strCommercialAddress = GmCommonClass.parseNull((String)hmICSData.get("HEADERADDRESS"));
	}
	
	strDivisionId = strDivisionId.equals("")?"2000": strDivisionId;
	HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(gmDataStoreVO.getCmpid(), strDivisionId);
	strCompanyName = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMPNAME"));
	strCompanyShortName = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMP_SHORT_NAME")); 
	strCompanyAddress =  GmCommonClass.parseNull((String)hmCompanyAddress.get("COMPADDRESS"));
	strCompanyPhone = GmCommonClass.parseNull((String)hmCompanyAddress.get("PH"));
	strCompanyLogo = GmCommonClass.parseNull((String)hmCompanyAddress.get("LOGO"));
	
	strHeadAddress = strCompanyName +","+strCompanyAddress +","+strCompanyPhone;
	
	
	if(strCommercialAddress.equals("")){
		strCommercialAddress = strHeadAddress;
	}
	
	if(strIsICSAccount.equals("Y")){
		String gmAddress = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMADDRESS"));
		hmReportHeaderDetails.put("GMADDRESS", strCompanyName+"\n"+strCompanyAddress +"\n"+strCompanyPhone);		
	}
	
	hmReportHeaderDetails.put("ORDERID",strOrdID);
	hmReportHeaderDetails.put("ORDDATE",strOrdDate);
	hmReportHeaderDetails.put("SHIPADDRESS",strShipAddress);
	hmReportHeaderDetails.put("CUSTPO",strCustPO);
	hmReportHeaderDetails.put("SHIPMODE",strShipMode);
	hmReportHeaderDetails.put("SHIPCARRIER",strShipCarrier);
	hmReportHeaderDetails.put("ATTENTIONOVER",strAttenOver);
	hmReportHeaderDetails.put("PRINTDATE",strPrintDate);
	hmReportHeaderDetails.put("HEADERADDRESS",strHeadAddress);
	hmReportHeaderDetails.put("COMHEADERADDRESS",strCommercialAddress);	
	hmReportHeaderDetails.put("SHIPTAXID",strShipTaxId);
	hmReportHeaderDetails.put("SHIPPURPOSE",strShipPurpose);
	hmReportHeaderDetails.put("TERMOFSALE",strShipTermSale);
	hmReportHeaderDetails.put("CONSIGNEETAXID",strConsignTaxID);
	hmReportHeaderDetails.put("CONSIGNEEPERSON",strConsignName);
	hmReportHeaderDetails.put("CONSIGNEEPHONE",strConsignPhone);
	hmReportHeaderDetails.put("CONSIGNEEREGION",strConsignRegn);
	hmReportHeaderDetails.put("HARMCODE",strHormoTerrif);
	hmReportHeaderDetails.put("SHIPCOST",strShipCost);
	hmReportHeaderDetails.put("USERID",strUserID);
	hmReportHeaderDetails.put("UNAME",strUsername);
	hmReportHeaderDetails.put("PHEADER",strPackHeader);
	hmReportHeaderDetails.put("PFOOTER",strPackFooter);
	hmReportHeaderDetails.put("LOGO", strCompanyLogo);

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Packing Slip </TITLE>

<script language="JavaScript" src="<%=strJsPath%>/GmPackSlip.js"></script>
<script>
var source = "<%=strSource%>";
var tdinnner = "";
var hopt = '<%=strOpt%>';
function fnPrint(){
	window.print();
	return false;
}
function hidePrint(){
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";	
}
function showPrint(){
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}

// To print the paperwork automatically
function fnWPrint()
{
	if(hopt == 'autoprint'){
		setTimeout(fnAutoPrint,1);
	}
}
function fnAutoPrint(){
	var OLECMDID = 6;
	var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
	document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
	WebBrowser1.ExecWB(OLECMDID, 2);
	WebBrowser1.outerHTML = "";
} 

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();" onLoad="fnWPrint();">
	   <%
			GmJasperReport gmJasperReport = new GmJasperReport();
			gmJasperReport.setRequest(request);
			gmJasperReport.setResponse(response);
			gmJasperReport.setJasperReportName(strJasperName);
			gmJasperReport.setHmReportParameters(hmReportHeaderDetails);
			gmJasperReport.setReportDataList(alPopulatedCartDtls);
			gmJasperReport.setBlDisplayImage(true);
			strHtmlJasperRpt = gmJasperReport.getHtmlReport();
		%>
		<%=strHtmlJasperRpt%>
		<% if(strIsICSAccount.equals("Y")){ %> 
		<BR><BR><BR><BR><BR><BR><BR>
		<%
			GmJasperReport gmCommerceJasperReport = new GmJasperReport();
			gmCommerceJasperReport.setRequest(request);
			gmCommerceJasperReport.setResponse(response);
			gmCommerceJasperReport.setJasperReportName(strjasName);
			gmCommerceJasperReport.setHmReportParameters(hmReportHeaderDetails);
			gmCommerceJasperReport.setReportDataList(alPopulatedCartDtls);
			gmCommerceJasperReport.setBlDisplayImage(true);
			strCommercialHtmlJasperRpt = gmCommerceJasperReport.getHtmlReport();
		%>
		<%=strCommercialHtmlJasperRpt %>
		<%}%>
		<table cellpadding="0" cellspacing="0" width="100%">
	 	<tr>
			<td>
			<td align="center" id="button">
				<gmjsp:button value="&nbsp;Print&nbsp;" name="Btn_Print" gmClass="button" tabindex="2" onClick="return fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				<gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_Close" gmClass="button" tabindex="3"	onClick="window.close();" buttonType="Load" />				
			</td>
		</tr>
</table>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
