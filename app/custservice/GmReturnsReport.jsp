 <%
/**********************************************************************************
 * File		 		: GmCreditsReport.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@ page import="java.util.Date"%>

<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtReturnsRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- custservice\GmReturnsReport.jsp -->
<fmtReturnsRpt:setLocale value="<%=strLocale%>"/>
<fmtReturnsRpt:setBundle basename="properties.labels.custservice.GmReturnsReport"/>

<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	int intSize = 0;
	HashMap hmLoop = new HashMap();
	HashMap hcboVal = null;

	String strCodeID = "";
	String strDistId = "";
	String strSelected = "";
	String strFormat = "3302,3306,3307,26240177";
	
	Date dtFrmDate = null;
	Date dtToDate = null;
	
	String strRAId = "";
	String strComments = "";
	String strReason = "0";
	String strSetId = "";
	String strCreditDt = "";
	String strReturnDt = "";
	String strFrmDt = (String)request.getAttribute("hFrmDt")==null?"":(String)request.getAttribute("hFrmDt");
	String strToDt = (String)request.getAttribute("hToDt")==null?"":(String)request.getAttribute("hToDt");
	String strApplDateFmt = strGCompDateFmt;
	dtFrmDate = (Date)GmCommonClass.getStringToDate(strFrmDt,strApplDateFmt);
	dtToDate = (Date)GmCommonClass.getStringToDate(strToDt,strApplDateFmt);

	String strAction = "";
	String strStatusFl = "";

	String strShade = "";
	String strProjId = "";
	String strParam = (String)request.getAttribute("hAction");
	ArrayList alReturn = new ArrayList();
	ArrayList alDistributor = new ArrayList();
	ArrayList alDistOrder = new ArrayList();
	String strDistName = "";
	String strPartQty = "";
	int intLength = 0;

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmLoadReturn = new HashMap();
	hmLoadReturn = (HashMap)hmReturn.get("LOADLIST");

	alDistributor = (ArrayList)hmLoadReturn.get("DISTLIST");

	if (strParam.equals("Reload"))
	{
		strDistId = (String)request.getAttribute("hDistId")==null?"":(String)request.getAttribute("hDistId");
		alDistOrder = (ArrayList)hmReturn.get("DISTORDERLIST");
		intLength = alDistOrder.size();
		strAction = (String)request.getAttribute("hAction");
	}

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Order Action </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
var dateFmt = '<%=strApplDateFmt%>';
function fnGo(){
	if(document.frmAccount.Txt_FromDate.value != ""){	 
		CommonDateValidation(document.frmAccount.Txt_FromDate,dateFmt,Error_Details_Trans(message[10523],dateFmt));
	}

	if(document.frmAccount.Txt_ToDate.value != ""){ 
		CommonDateValidation( document.frmAccount.Txt_ToDate,dateFmt, Error_Details_Trans(message[10524],dateFmt));	 
	}
	
	var fromToDiff = dateDiff(document.frmAccount.Txt_FromDate.value, document.frmAccount.Txt_ToDate.value, dateFmt);
	if(fromToDiff < 0){
   		Error_Details(message[10525]);
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmAccount.hAction.value = "Reload";
	fnStartProgress();
	document.frmAccount.submit();	
}

function fnLoad(){
	var obj = document.frmAccount.Cbo_Action;
	for (var i=0;i<obj.length ;i++ ){
		if (document.frmAccount.hAction.value == obj.options[i].value){
			obj.options[i].selected = true;
			break;
		}
	}
}

function fnLoadRaId(val){
	windowOpener("/GmReportCreditsServlet?hAction=ViewReturn&hRAId="+val,"ReturnsView","resizable=yes,scrollbars=yes,top=150,left=200,width=680,height=400");
}

function fnDateValidation(){
	
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnLoad();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmReportCreditsServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hRAId" value="">
<input type="hidden" name="Cbo_Action" value="<%=strFormat%>">

	<table border="0" width="800" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="10" width="1" class="Line"></td>
			<td bgcolor="#666666" height="1" colspan="2"></td>
			<td rowspan="10" width="1" class="Line"></td>
		</tr>
		<tr>
			<td height="25"  colspan="2" class="RightDashBoardHeader"><fmtReturnsRpt:message key="TD_RTNS_ITEM_RPT_HEADER"/></td>
		</tr>
		<tr>
			<td class="RightTableCaption" height="25" align="right"><fmtReturnsRpt:message key="LBL_DIST_LIST"/>:</td>
			<td>&nbsp;<select name="Cbo_DistId" id="Region" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" >All Distributors
<%
					intSize = alDistributor.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alDistributor.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strDistId.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%
			  		}
%>
						</select>
						&nbsp;
			</td>
		</tr>
		<tr>
			<td class="RightTableCaption" height="25" align="right"><fmtReturnsRpt:message key="LBL_FROM_DATE"/>:</td>
			<td class="RightTableCaption">&nbsp;<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtFrmDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="2"/>
			&nbsp;&nbsp;<fmtReturnsRpt:message key="LBL_TO_DATE"/>: <gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=dtToDate==null?null:new java.sql.Date(dtToDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="3"/>&nbsp;&nbsp;
						<fmtReturnsRpt:message key="BTN_GO" var="varGo"/>
						<gmjsp:button value="&nbsp;${varGo}&nbsp;" gmClass="button" onClick="fnGo();" tabindex="25" buttonType="Load" />&nbsp;
			</td>
		</tr>
		<tr><td bgcolor="#666666"  colspan="2" height="1"></td></tr>
    </table>
<%
	if (strParam.equals("Reload"))
	{
%>
	<table border="0" width="800" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="800" height="100" valign="top">
<%
	if (intLength > 0)
	{
%>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="120"><fmtReturnsRpt:message key="LBL_RA_ID"/></td>
						<td width="250"><fmtReturnsRpt:message key="LBL_DIST"/></td>
						<td width="170"><fmtReturnsRpt:message key="LBL_REASON"/></td>
						<td width="80" align="center"><fmtReturnsRpt:message key="LBL_RETURN"/><BR><fmtReturnsRpt:message key="LBL_DATE"/></td>
						<td width="80" align="center"><fmtReturnsRpt:message key="LBL_CREDIT"/><BR><fmtReturnsRpt:message key="LBL_DATE"/></td>
						<td width="60" align="center"><fmtReturnsRpt:message key="LBL_PARTS"/><BR><fmtReturnsRpt:message key="LBL_QTY"/></td>
						<td width="300"><fmtReturnsRpt:message key="LBL_COMMENTS"/></td>
					</tr>
					<tr><td class="Line" colspan="7"></td></tr>
<%
							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alDistOrder.get(i);
								strRAId = GmCommonClass.parseNull((String)hmLoop.get("RAID"));
								strComments = GmCommonClass.parseNull((String)hmLoop.get("COMMENTS"));
								strReason = (String)hmLoop.get("REASON");
								strSetId = (String)hmLoop.get("SETID");
								strCreditDt = GmCommonClass.getStringFromDate((java.sql.Date)hmLoop.get("CRDATE"),strApplDateFmt);
								strReturnDt = GmCommonClass.getStringFromDate((java.sql.Date)hmLoop.get("RDATE"),strApplDateFmt);
								strDistName = (String)hmLoop.get("DNAME");
								strPartQty = (String)hmLoop.get("RAQTY");
%>
					<tr>
						<td height="20" class="RightText">&nbsp;<a href="javascript:fnLoadRaId('<%=strRAId%>');" class="RightText"><%=strRAId%></a>
						<td class="RightText">&nbsp;<%=strDistName%>&nbsp;</td>
						<td class="RightText">&nbsp;<%=strReason%>&nbsp;</td>
						<td class="RightText" align="center"><%=strReturnDt%></td>
						<td class="RightText" align="center"><%=strCreditDt%></td>
						<td class="RightText" align="center"><%=strPartQty%></td>
						<td class="RightText">&nbsp;<%=strComments%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="7"></td></tr>
<%
							}
%>
				</table>
<%
	}else{
%>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="100"><fmtReturnsRpt:message key="LBL_RA_ID"/></td>
						<td width="250"><fmtReturnsRpt:message key="LBL_DIST"/></td>
						<td width="150"><fmtReturnsRpt:message key="LBL_REASON"/></td>
						<td width="80" align="center"><fmtReturnsRpt:message key="LBL_RETURN"/><BR><fmtReturnsRpt:message key="LBL_DATE"/></td>
						<td width="80" align="center"><fmtReturnsRpt:message key="LBL_CREDIT"/><BR><fmtReturnsRpt:message key="LBL_DATE"/></td>
						<td width="80" align="center"><fmtReturnsRpt:message key="LBL_PARTS_QTY"/></td>
						<td width="300"><fmtReturnsRpt:message key="LBL_COMMENTS"/></td>
					</tr>
					<tr><td class="Line" colspan="7"></td></tr>					
					<tr><td colspan="7" class="RightTextRed" align="center"> <BR><fmtReturnsRpt:message key="MSG_NO_CREDIT_DIST"/> </td></tr>
				</table>
<%
		}
%>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
<%
	}
%>
</FORM>
</BODY>
<%@ include file="/common/GmFooter.inc" %>
</HTML>
