 <%
/**********************************************************************************
 * File		 		: GmPartSearch.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<!-- \custservice\GmPartSearch.jsp-->
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %> 
<%@ taglib prefix="fmtPartSearch" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartSearch:setLocale value="<%=strLocale%>"/>
<fmtPartSearch:setBundle basename="properties.labels.custservice.GmPartNumSearch"/>
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strPartNum = (String)request.getAttribute("hPartNum")==null?"":(String)request.getAttribute("hPartNum");
	String strDesc = (String)request.getAttribute("hDesc")==null?"":(String)request.getAttribute("hDesc");
	String strSessDeptId = GmCommonClass.parseNull((String)session.getAttribute("strSessDeptId"));
	String strGridData = GmCommonClass.parseNull((String)request.getAttribute("alSearchReport"));
	
	String strSessCurrSymb = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Number Search</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script>
var objGridData;
objGridData ='<%=strGridData%>';

function fnEnter()
{
		if (event.keyCode == 13)
		{ 
			fnGo();
		}
}

function fnGo()
{
	document.frmAccount.hAction.value = "Go";
	fnStartProgress();
	document.frmAccount.submit();
}
function fnOnPageLoad()
{	 	
	if(objGridData!=''){
		gridObj = setDefalutGridProperties('partNumData');	
		gridObj.setNumberFormat("<%=strSessCurrSymb%>0,000.00",8,".",",");
		gridObj.attachHeader('#rspan,#text_filter,#text_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#numeric_filter,#select_filter,#text_filter');
		gridObj = initializeDefaultGrid (gridObj, objGridData);
		gridObj.enableTooltips("false,true,true,true,true,true,true,true,true,true,true");
		gridObj.enableBlockSelection(true);
		gridObj.attachEvent("onKeyPress", keyPressed); 
		gridObj.enableSmartRendering(true);	
   }
}

function keyPressed(code, ctrl) {
    if (code == 67 && ctrl) {       
        gridObj.setCSVDelimiter("\t");
        gridObj.copyBlockToClipboard();
    } 
    return true;
}

function fnCallPart(id)
{			
	windowOpener("/GmPartNumServlet?hAction=LoadPartDetail&partNumber="+encodeURIComponent(id)+"&hOpt=Search","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=760,height=600");
}
</script>
</HEAD>

<BODY onkeyup="fnEnter();"  leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmPartSearchServlet">
<input type="hidden" name="hAction" value="">

	<table border="0" width="950" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="15" width="1" class="Line"></td>
			<td height="1" bgcolor="#666666"></td>
			<td rowspan="15" width="1" class="Line"></td>
		</tr>
		<tr>
			<td height="25" class="RightDashBoardHeader">
				<fmtPartSearch:message key="TD_PART_WILD_SEARCH_HEADER"/>
			</td>
		</tr>
		<tr><td class="Line" height="1"></td></tr>
		<tr>
			<td>
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td align="right" class="RightText" HEIGHT="23" width="150"><fmtPartSearch:message key="LBL_PART_NUMS"/>:</td>
						<td colspan="2" class="RightText">&nbsp;<input type="text" size="20" value="<%=strPartNum%>" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1></td>						
					</tr>
					<tr><td colspan="3" class="lline" ></td></tr>					
					<tr  class="Shade">
						<td align="right" class="RightText" HEIGHT="23" width="150"><fmtPartSearch:message key="LBL_PART_DESC"/>:</td>
						<td class="RightText">&nbsp;<input type="text" size="50" value="<%=strDesc%>" name="Txt_Desc" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=2></td>
						<fmtPartSearch:message key="BTN_LOAD" var="varLoad"/>
						<td width="200" >&nbsp;<gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" name="Btn_Submit" gmClass="button" onClick="javascript:fnGo()" tabindex="3" buttonType="Load" /></td>
					</tr>
					
				</table>
			</td>
		</tr>
		<tr><td class="Line" height="1"></td></tr>
		<tr><td height="25" class="RightDashBoardHeader">&nbsp;<fmtPartSearch:message key="MSG_SEARCH_RESULTS"/></td></tr>
		<tr><td class="Line" height="1"></td></tr>
		</table>
		<table width="950"  cellpadding="0" cellspacing="0" border="0"> 
		   	<tr>
       	 		<td>
					<div id="partNumData" style="grid" height="500px" width="950px"></div>  
		    	</td>
           	</tr>
           		
		</table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
