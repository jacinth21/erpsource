
<%@ include file="/common/GmHeader.inc" %>


<%
/**********************************************************************************
 * File		 		: GmOrderItemSetup.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtOrderItemSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>

<fmtOrderItemSetup:setLocale value="<%=strLocale%>"/>
<fmtOrderItemSetup:setBundle basename="properties.labels.custservice.GmOrderItemSetup"/>
<!-- \custservice\GmOrderItemSetup.jsp -->
<%
try {
	
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	
	String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");

	HashMap hmReturn = (HashMap)session.getAttribute("hmReturn");

	String strhAction = (String)request.getAttribute("hAction");
	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}
	strhAction = (strhAction == null)?"Load":strhAction;

	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strTemp = "";

	String strOrderId = (String)session.getAttribute("ORDERID") == null?"":(String)session.getAttribute("ORDERID");
	String strSetName = "";
	String strAccName = "";
	String strDistName = "";
	String strUserName = "";
	String strIniDate = "";
	String strItemQty = "";
	String strControl = "";
	String strPartNums = (String)session.getAttribute("strPartNums") == null?"":(String)session.getAttribute("strPartNums");
	String strNewPartNums = "";
	String strPartDesc = "";
	String strFlag = ""; 
	
	String strMode = "5015";
	String strDistribAccId = "";
	String strPurpose = "";
	String strBillTo = "";
	String strShipTo = "";
	String strShipToId = "";
	String strComments = "";
	String strPO = "";
	String strShipFl = "";
	String strDelFl = "";
	String strPerson = "";
	String strAccId = "";
	String strOrdType = "2521";

	ArrayList alMode = new ArrayList();
	ArrayList alShipTo = new ArrayList();
	ArrayList alPurpose = new ArrayList();
	ArrayList alAccount = new ArrayList();
	ArrayList alRepList = new ArrayList();
	ArrayList alAccList = new ArrayList();
	ArrayList alEmpList = new ArrayList();
	ArrayList alOrderType = new ArrayList();

	HashMap hmConsignDetails = new HashMap();
	HashMap hmTemp = new HashMap();
	HashMap hmParam =  new HashMap();
	HashMap hmOtherDetails =  new HashMap();
	
	int intPriceSize = 0;

	if (hmReturn != null)
	{
		alMode = (ArrayList)hmReturn.get("ORDERMODE");
		alAccount = (ArrayList)hmReturn.get("ACCLIST");
		alShipTo = (ArrayList)hmReturn.get("SHIPTO");
		alRepList = (ArrayList)hmReturn.get("REPLIST");
		alOrderType = (ArrayList)hmReturn.get("ORDTYPE");
	}

	hmParam = (HashMap)session.getAttribute("PARAM");

	if (hmParam != null)
	{
		strOrdType = GmCommonClass.parseNull((String)hmParam.get("TYPE"));
		strMode = GmCommonClass.parseNull((String)hmParam.get("MODE"));
		strBillTo = GmCommonClass.parseNull((String)hmParam.get("BILLTO"));
		strShipTo = GmCommonClass.parseNull((String)hmParam.get("SHIPTO"));
		strShipToId = GmCommonClass.parseNull((String)hmParam.get("SHIPTOID"));
		strComments = GmCommonClass.parseNull((String)hmParam.get("COMMENTS"));
		strPO = GmCommonClass.parseNull((String)hmParam.get("PO"));
		strAccId = GmCommonClass.parseNull((String)hmParam.get("ACCID"));
		strPerson = GmCommonClass.parseNull((String)hmParam.get("PERSON"));

		strChecked = strShipFl.equals("1")?"checked":"";
	}

	int intSize = 0;
	HashMap hcboVal = null;
	int i=0;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Consign </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>

function fnSubmit()
{
	document.frmVendor.hAction.value = 'Save';
  	document.frmVendor.submit();
}

function fnInitiate()
{
	document.frmVendor.hAction.value = 'EditLoad';
  	document.frmVendor.submit();
}

function fnAddToCart()
{
	document.frmVendor.hAction.value = "GoCart";
	document.frmVendor.submit();
}

function fnRemoveItem(val)
{
	document.frmVendor.hDelPartNum.value = val;
	document.frmVendor.hAction.value = "RemoveCart";
	document.frmVendor.submit();	
}

function fnUpdateCart()
{
	document.frmVendor.hAction.value = "UpdateCart";
	document.frmVendor.submit();	
}

function fnPlaceOrder()
{
	document.frmVendor.hAction.value = "PlaceOrder";
/*
	var cnt = document.frmVendor.hCnt.value;
	var pnum = '';
	var stock = '';
	var qty = '';
	var price = '';
	var obj = '';
	var j = 0;
	var backOrderstr = '';
	var cnum = '';
	for (var i = 0;i<cnt; i++)
	{
		j++;
		obj = eval("document.frmVendor.hPNum"+j);
		pnum = obj.value;
		obj = eval("document.frmVendor.hStock"+j);
		stock = obj.value;
		obj = eval("document.frmVendor.Txt_Qty"+j);
		qty = obj.value;
		obj = eval("document.frmVendor.hPrice"+j);
		price = obj.value;
		if (qty > stock)
		{
			obj = eval("document.frmVendor.Txt_Qty"+j);
			obj.value = stock;
			boqty = qty - stock;
			bopartnums = bopartnums + pnum;
			backOrderstr = backOrderstr + pnum+","+ boqty+","+cnum+","+RemoveComma(price)+"|";
			
		}
		
	}
	document.frmVendor.hBOStr.value = backOrderstr.substring(0,backOrderstr.length-1);
	document.frmVendor.hBOStr.value = backOrderstr;
//alert(backOrderstr);
	if (backOrderstr != '')
	{
		fnCallBoWindow();
	}*/
	document.frmVendor.submit();	
}
var boFlag = false;
var bopartnums = '';
function fnCallBoWindow()
{
	windowOpener("/bo.html","BO","resizable=yes,scrollbars=yes,top=150,left=200,width=250,height=200");
}

function fnCallPerson()
{
	obj = document.frmVendor.Cbo_Mode;
	var obj2 = document.frmVendor.Txt_PNm;
	if (obj.options[obj.selectedIndex].text != 'FAX')
	{
		obj2.disabled = false;
		obj2.focus();
	}
	else
	{
		obj2.disabled = true;
	}

}

function fnPicSlip()
{
windowOpener("/GmOrderItemServlet?hId=<%=strOrderId%>&hAction=PICSlip&","PICSlip","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=400");
}

function fnOpenAddress()
{
accid = document.frmVendor.Cbo_BillTo.value;
repid = document.frmVendor.Cbo_BillTo[document.frmVendor.Cbo_BillTo.selectedIndex].id;
val = document.frmVendor.Cbo_ShipTo.value;
id = document.frmVendor.Cbo_Rep.value;
if (accid == '0')
{
	alert(message_operations[717]);
	return;
}

if (val == '0')
{
	alert(message_operations[720]);
	return;
}
if (val == '4121' && id =='0')
{
	alert(message_operations[718]);
	return;
}

windowOpener("/GmOrderItemServlet?hAction=Address&hAccId="+accid+"&hRepId="+repid+"&hShip="+val+"&hShipId="+id,"Address","resizable=yes,scrollbars=yes,top=340,left=420,width=300,height=135");

}


function fnCallShip()
{
	obj = document.frmVendor.Cbo_ShipTo;
	var obj2 = document.frmVendor.Cbo_Rep;
	if (obj.options[obj.selectedIndex].text == 'Sales Rep')
	{
		var repid = document.frmVendor.Cbo_BillTo[document.frmVendor.Cbo_BillTo.selectedIndex].id;
		for (var j=0;j<obj2.length;j++)
		{
			if (obj2.options[j].value == repid)
			{
				obj2.options[j].selected = true;
				break;
			}
		}
		obj2.disabled = false;
		obj2.focus();
	}
	else
	{
		obj2.disabled = true;
	}
}


function fnLoad()
{
	var ShipToFl = '<%=strShipTo%>';
	if (ShipToFl != '4120')
	{
		fnCallShip();
		fnSetValue();
	}
}

function fnSetValue()
{
	var ShipToId = '<%=strShipToId%>';
	var obj5 = document.frmVendor.Cbo_Rep;
	for (var j=0;j<obj5.length;j++)
	{
		if (obj5.options[j].value == ShipToId)
		{
			obj5.options[j].selected = true;
			break;
		}
	}
}

function fnSetCboVal(val)
{
	if (val != '')
	{
		var blFl = '';
		var accobj = document.frmVendor.Cbo_BillTo;
		for (var j=0;j<accobj.length;j++)
		{
			if (accobj.options[j].value == val)
			{
				accobj.options[j].selected = true;
				blFl = 1;
				break;
			}
		}
		if (blFl != 1)
		{
			alert("Invalid Hospital ID");
			document.frmVendor.Txt_AccId.select();
			document.frmVendor.Txt_AccId.focus();
		}
	}
}

function fnSetAccIdValue(val,nm)
{
	document.frmVendor.Txt_AccId.value = val;
	nm = document.frmVendor.Cbo_BillTo[document.frmVendor.Cbo_BillTo.selectedIndex].name;
	id = document.frmVendor.Cbo_BillTo[document.frmVendor.Cbo_BillTo.selectedIndex].id;
	//document.frmVendor.Txt_RepDetails.value = nm + ' / '+ id;
	document.all.Lbl_RepDetails.innerHTML ='&nbsp;'+ nm + ' / '+ id;
}
</script>
</HEAD>
<%@page import="java.util.StringTokenizer"%>
<BODY leftmargin="20" topmargin="10">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmOrderItemServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hConsignId" value="<%=strOrderId%>">
<input type="hidden" name="hDelPartNum" value="">
<input type="hidden" name="hBOStr" value="">

	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
			<td height="25" class="RightDashBoardHeader"><fmtOrderItemSetup:message key="LBL_PROCESS_NEW"/></td>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="shade">
						<td class="RightText" HEIGHT="23" align="right">&nbsp;<fmtOrderItemSetup:message key="LBL_ORDER_TYPE"/></td>
						<td class="RightText">&nbsp;<select name="Cbo_OrdType" id="Region" class="RightText" tabindex="4" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" onChange="javascript:fnCallPerson();"><option value="0" ><fmtOrderItemSetup:message key="LBL_CHOOSE"/>
<%
			  		//intSize = alOrderType.size();
					hcboVal = new HashMap();
			  		for (i=0;i<3;i++) // Hardcoding to remove rest of order types - James 03/31
			  		{
			  			hcboVal = (HashMap)alOrderType.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strOrdType.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
							</select>
						</td>
					</tr>
<%
				if (strOrderId.equals("DUPLICATE"))
				{
					strOrderId = "";
					strhAction = "Load";
%>
					<tr>
						<td class="RightTextRed" colspan="2" HEIGHT="35" valign="middle" align="center"><b>&nbsp;<fmtOrderItemSetup:message key="LBL_DELEVERED_ORDER"/></td>
					</tr>
					<tr><td colspan="2" class="Line"></td></tr>
<%
				}
%>					
					<tr>
						<td class="RightText" HEIGHT="23" align="right">&nbsp;<fmtOrderItemSetup:message key="LBL_ORDER_ID"/></td>
						<td class="RightText">&nbsp;<fmtOrderItemSetup:message key="" var="var"/>DO-<input type="text" size="12" value="<%=strOrderId%>" name="Txt_OrdId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1> &nbsp;&nbsp;<span class="RightTextBlue"><fmtOrderItemSetup:message key="" var="var"/>(Leave this blank if there is no DO Number)</span></td>
					</tr>
					<tr class="shade">
						<td class="RightText" width="130" HEIGHT="23" align="right">&nbsp;<fmtOrderItemSetup:message key="LBL_BILL_TO"/></td>
						<td class="RightText" width="498">&nbsp;<select name="Cbo_BillTo" id="Region" class="RightText" tabindex="2" onChange="fnSetAccIdValue(this.value,this.name);" ><option value="0" ><fmtOrderItemSetup:message key="LBL_CHOOSE"/>
<%
			  		intSize = alAccount.size();
					hcboVal = new HashMap();
					String strRepId = "";
					String strRepNm = "";
			  		for (i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alAccount.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
			  			strRepId = (String)hcboVal.get("REPID");
			  			strRepNm = (String)hcboVal.get("RNAME");
			  			strDistName = (String)hcboVal.get("DNAME");
						strSelected = strBillTo.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>" name="<%=strRepNm%>" id="<%=strRepId%>"><%=hcboVal.get("NAME")%> / <%=strDistName%></option>
<%
			  		}
%>
							</select> <br><fmtOrderItemSetup:message key="LBL_OR_ACCOUNT_ID"/>: <input type="text" size="6" value="<%=strAccId%>" name="Txt_AccId" class="InputArea" onBlur="fnSetCboVal(this.value);" tabindex=3>
						</td>
					</tr>
					<tr>
						<td class="RightText" width="130" HEIGHT="23" align="right">&nbsp;<fmtOrderItemSetup:message key="LBL_REP_NAME_ID"/>:</td>
						<td class="RightText" id="Lbl_RepDetails"></td>
					</tr>
					<tr>
						<td class="RightText" HEIGHT="23" align="right">&nbsp;<fmtOrderItemSetup:message key="LBL_MODE_OF_ORDER"/>:</td>
						<td class="RightText">&nbsp;<select name="Cbo_Mode" id="Region" class="RightText" tabindex="4" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" onChange="javascript:fnCallPerson();"><option value="0" ><fmtOrderItemSetup:message key="LBL_CHOOSE"/>
<%
			  		intSize = alMode.size();
					hcboVal = new HashMap();
			  		for (i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alMode.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strMode.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
							</select> &nbsp;&nbsp;&nbsp;&nbsp;<fmtOrderItemSetup:message key="LBL_PER_NAME"/> &nbsp;<input type="text" size="20" disabled value="<%=strPerson%>" name="Txt_PNm" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3></td>
					</tr>
					<tr class="shade">
						<td class="RightText" HEIGHT="23" align="right">&nbsp;<fmtOrderItemSetup:message key="LBL_SHIP_TO"/></td>
						<td class="RightText">&nbsp;<select name="Cbo_ShipTo" id="Region" class="RightText" tabindex="5" onChange="javascript:fnCallShip();" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtOrderItemSetup:message key="LBL_CHOOSE"/>
<%
			  		intSize = 3; // Hardcoding to remove the 'Employee' value from list !
					hcboVal = new HashMap();
			  		for (i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alShipTo.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strShipTo.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
							</select>&nbsp;&nbsp;&nbsp;&nbsp; <fmtOrderItemSetup:message key="LBL_VALUES"/><select name="Cbo_Rep" disabled class="RightText" tabindex="5" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtOrderItemSetup:message key="LBL_CHOOSE"/>
<%
			  		intSize = alRepList.size();
					hcboVal = new HashMap();
			  		for (i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alRepList.get(i);
			  			strCodeID = (String)hcboVal.get("REPID");
						strSelected = strShipTo.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%
			  		}
%>
							</select>&nbsp;&nbsp;<fmtOrderItemSetup:message key="BTN_ADDR" var="varAddressLookup"/><gmjsp:button value="${varAddressLookup}" name="Btn_Address" gmClass="button" onClick="fnOpenAddress();" buttonType="Load" />
						</td>
					</tr>
					<tr>
						<td class="RightText" valign="top" align="right" HEIGHT="24"><fmtOrderItemSetup:message key="LBL_COMMENTS"/>:</td>
						<td>&nbsp;<textarea name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=6 rows=5 cols=40 value="<%=strComments%>"><%=strComments%></textarea></td>
					</tr>
					<tr class="shade">
						<td class="RightText" align="right" HEIGHT="24"><fmtOrderItemSetup:message key="LBL_CUSTOMER_PO#"/>:</td>
						<td>&nbsp;<input type="text" size="15" value="<%=strPO%>" name="Txt_PO" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=7></td>
					</tr>
					<script>
						fnLoad();
					</script>
<%
		if (!strhAction.equals("Load"))
		{
			if (!strhAction.equals("OrdPlaced"))
			{
%>					
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr>
						<td height="30" align="center" colspan="2" class="RightTextBlue"><br><b><fmtOrderItemSetup:message key="LBL_SHOOPING_CART"/><br>&nbsp;&nbsp;&nbsp;<input type="text" size="60" value="" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
						<fmtOrderItemSetup:message key="LBL_GO" var="varGo"/><gmjsp:button value="&nbsp;${varGo}&nbsp;" name="Btn_GoCart" gmClass="button" onClick="fnAddToCart();" tabindex="16" buttonType="Load" /><br><br>
						</td>
					</tr>
<%
			}
%>

					<tr>
						<td align="center" colspan="2" valign="top">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td align="center" width="80" height="25"><fmtOrderItemSetup:message key="LBL_PART#"/></td>
									<td align="center" width="200"><fmtOrderItemSetup:message key="LBL_DESC"/></td>
									<td align="center" width="70"><fmtOrderItemSetup:message key="LBL_SHELF_QTY"/></td>
									<td align="center" width="80"><fmtOrderItemSetup:message key="LBL_UNIT_PRICE"/></td>
									<td align="center" width="80"><fmtOrderItemSetup:message key="LBL_ORDER_QTY"/></td>
									<td align="center" width="100"><fmtOrderItemSetup:message key="LBL_EXTEN_PRICE"/></td>
								</tr>
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
<%
			String strTotal = "";

			if (!strPartNums.equals(""))
			{
					hmReturn = (HashMap)session.getAttribute("hmOrder");
					HashMap hmCart = (HashMap)session.getAttribute("hmCart");
					StringTokenizer strTok = new StringTokenizer(strPartNums,",");
					String strPartNum = "";
					HashMap hmVal = new HashMap();
					HashMap hmLoop = new HashMap();
					String strId = "";
					String strDesc = "";
					String strPrice = "";

					String strAmount = "";
					String strQty = "1";
					String strStock = "";
					String strUnPartNums = "";
					String strInputAmt = "";
					double dbPrice = 0.0;
					double dbDiscount = 0.0;
					double dbDiscPrice = 0.0;
					String strDiscPrice = "";
					String strShade = "";
					
					double dbAmt = 0.0;
					double dbTotal = 0.0;
					boolean bolFl = false;
					boolean bolFl2 = false;

					int intQty = 0;
					i = 0;
					if (strTok.countTokens() == 1)
					{
						bolFl2 = true;
					}
					while (strTok.hasMoreTokens())
					{
						i++;
						strPartNum 	= strTok.nextToken();
						if (hmCart != null)
						{
							hmLoop = (HashMap)hmCart.get(strPartNum);
							if (hmLoop != null)
							{
								strQty = (String)hmLoop.get("QTY");
								strInputAmt = (String)hmLoop.get("PRICE");
							}
							else
							{
								strQty = "1";
								strInputAmt = "";
							}
						}

						strQty = strQty == null?"1":strQty;
						intQty = Integer.parseInt(strQty);
						hmVal = (HashMap)hmReturn.get(strPartNum);
						if (hmVal.isEmpty())
						{
							//bolFl = false;
							out.print("<tr><td class=RightText height=20 align=center>"+strPartNum+"</td>");
							out.print("<td colspan=5 class=RightTextRed>Part Number / Pricing doesn't exist in the System</td></tr>");
						}
						else if (hmVal.isEmpty() && bolFl2)
						{
							bolFl = false;
						}
						else
						{
							strNewPartNums = strNewPartNums + strPartNum + ",";
							bolFl = true;
							strId = (String)hmVal.get("ID");
							strDesc = (String)hmVal.get("PDESC");
							strPrice = (String)hmVal.get("PRICE");
							strPrice = strInputAmt.equals("")?strPrice:strInputAmt;

							strStock = (String)hmVal.get("STOCK");
							int intStock = Integer.parseInt(strStock);
							strStock = intStock < 0?"0":strStock;

							/*if (strhAction.equals("OrdPlaced"))
							{
								int intStock = Integer.parseInt(strStock);
								intStock = intStock - intQty; 
								strStock = "" + intStock;
							}*/

							dbPrice = Double.parseDouble(strPrice);
							dbAmt = intQty * dbPrice; // Multiply by Qty
							strDiscPrice = String.valueOf(dbPrice);
							strAmount = String.valueOf(dbAmt);
							dbTotal = dbTotal + dbAmt;
							strShade	= (i%2 == 0)?"class=Shade":""; //For alternate Shading of rows

%>
								<tr <%=strShade%>>
									<td class="RightText" align="center" height="20">
<%
							if (!strhAction.equals("OrdPlaced"))
							{
%>									
									<a  href="javascript:fnRemoveItem('<%=strId%>');"><img border="0" Alt='Remove item from cart' valign="bottom" src="<%=strImagePath%>/btn_remove.gif" height="15" width="15"></a><%}%>&nbsp;<%=strId%></td>
									<td class="RightText"><%=strDesc%><input type="hidden" name="hPNum<%=i%>" value="<%=strId%>"></td>
									<td class="RightText" align="center"><%=strStock%><input type="hidden" name="hStock<%=i%>" value="<%=strStock%>"></td>
									<td class="RightText" align="center">&nbsp;<input type="text" size="8" value="<%=GmCommonClass.getStringWithCommas(strDiscPrice)%>" name="hPrice<%=i%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>

								<!--<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strDiscPrice)%>&nbsp;
									<input type="hidden" name="hPrice<%=i%>" value="<%=strDiscPrice%>">-->
									</td>
									<td class="RightText" align="center">
<%
							if (!strhAction.equals("OrdPlaced"))
							{
%>
										&nbsp;<input type="text" size="5" value="<%=strQty%>" name="Txt_Qty<%=i%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
<%
							}else{	
%>
										<%=strQty%>
<%							}%>
									</td>
									<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strAmount)%>&nbsp;</td>
								</tr>
<%
						strTotal = ""+dbTotal;
						}
					}
						if (!strNewPartNums.equals(""))
						{
							strNewPartNums = strNewPartNums.substring(0,strNewPartNums.length()-1);
						}
						else{
						strhAction = "";
						}
						if (bolFl)
						{
%>
								<tr>
									<td colspan="6" height="1" bgcolor="#666666"></td>
								</tr>
								<tr class="RightTableCaption">
									<td colspan="4" align="right" height="23">
									<input type="hidden" name="hTotal" value="<%=strTotal%>">
									</td>
									<td align="center"><fmtOrderItemSetup:message key="LBL_TOTAL"/></td>
									<td align="right">$<%=GmCommonClass.getStringWithCommas(strTotal)%>&nbsp;
									<input type="hidden" name="hTotal" value="<%=strTotal%>">
									</td>
								</tr>	
<%
						}						

						if (!bolFl)
						{
%>
								<tr>
									<td colspan="6" height="30" class="RightTextBlue" align="center"><fmtOrderItemSetup:message key="LBL_NO_ITEMS"/></td>
								</tr>
<%
						}
}else{ 
	strhAction = "";
%>
								<tr>
									<td colspan="6" height="30" class="RightTextBlue" align="center"><fmtOrderItemSetup:message key="LBL_NO_ITEMS"/></td>
								</tr>
<%
			}
		}
%>
							</table><input type="hidden" name="hPartNums" value="<%=strNewPartNums%>">
							<input type="hidden" name="hCnt" value="<%=i%>">
						</td>
					</tr>
<%
		if (strhAction.equals("Load"))
		{
%>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr>
						<td colspan="2" height="30" align="center">
							<fmtOrderItemSetup:message key="BTN_INITIATE" var="varInitiate"/><gmjsp:button value="${varInitiate}" gmClass="button" onClick="fnInitiate();" tabindex="13" buttonType="Save" />
						</td>
					</tr>
<%
		}else if (strhAction.equals("PopOrd")){
%>
		<tr>
			<td colspan="3" align="center" height="30">
				<fmtOrderItemSetup:message key="BTN_UPDATE_QTY" var="varUpdateQty"/><gmjsp:button value="&nbsp;${varUpdateQty}&nbsp;" name="Btn_UpdateQty" gmClass="button" onClick="fnUpdateCart();" buttonType="Save" />
				&nbsp;&nbsp;
				<fmtOrderItemSetup:message key="BTN_PLACE_ORDER" var="varPlaceOrder"/><gmjsp:button value="&nbsp;${varPlaceOrder}&nbsp;" name="Btn_PlaceOrd" gmClass="button" onClick="fnPlaceOrder();" buttonType="Save" />
			</td>
		<tr>
<%	
		}
		else if (strhAction.equals("OrdPlaced"))
		{
%>
		<tr>
			<td colspan="3" align="center" height="30">
				<fmtOrderItemSetup:message key="BTN_PIC_SLIP" var="varPrintPICSlip"/><gmjsp:button value="&nbsp;${varPrintPICSlip}&nbsp;" name="Btn_PicSlip" gmClass="button" onClick="fnPicSlip();" buttonType="Load" />
			</td>
		<tr>
<%
		}
%>
				 </table>
			 </td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
    </table>
<script>
<%
	if (strhAction.equals("OrdPlaced"))
	{
%>
	alert("Your order has been placed. Please print PIC Slip");
<%
	}
%>
</script>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
