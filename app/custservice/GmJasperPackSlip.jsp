
 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmJasperPackSlip.jsp
 * Desc		 		: This screen is used for the Packing Slip
 * Version	 		: 1.0
 * author			: Tamizhthangam Ramasamy
************************************************************************************/
%>
<!-- \custservice\GmJasperPackSlip.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="com.globus.common.beans.GmJasperReport"%>


<% 


	String strAction=null;
	String strSource="";
	String strApplnDateFmt = strGCompDateFmt;
	String strHtmlJasperRpt = "";
	String strJasperName = "";
	
   
    strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	strAction = (strAction.trim() == null) ? "PrintPack" : strAction;
	strAction = (strAction.equals("")) ? "PrintPack" : strAction;
    String strOpt = GmCommonClass.parseNull(request.getParameter("hOpt"));
	strSource = GmCommonClass.parseNull((String)request.getAttribute("SOURCE"));
	String strGUID = GmCommonClass.parseNull(request.getParameter("hGUId"));
	HashMap hmParam = new HashMap();
	HashMap hmReturn = new HashMap();
	HashMap hmAtbData = new HashMap();
	HashMap hmCompAtbData = new HashMap();
	hmReturn = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturn"));
	
	hmAtbData = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("ORDER_ATB"));
	hmCompAtbData = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("COMP_ATB"));
	
	HashMap hmCompanyAdd = new HashMap ();
	HashMap hmOrderDetails = new HashMap();
	HashMap hmShipDetails = new HashMap();
	ArrayList alCartDetails = new ArrayList();
	ArrayList alPopulatedCartDtls = new ArrayList();
	ArrayList alBackOrderDetails = new ArrayList();
	
	
	String strProjectType = "";
    java.sql.Date dtOrdDate = null;
	

	String strShipAdd = "";
	String strShipCarr = "";
	String strShipDate = "";
	String strOrdId = "";
	String strTrack = "";
	String strOrderType = "";
	String strCompanyName = "";
	String strCompanyShortName = "";
	String strCompanyAddress = "";
	String strCompanyPhone = "";
	String strGmLocalLic="";
	String strGmExportLic="";
	String strExportDrugLic="";
	String strLocalDrugLic="";
	String strGmDrugLocalLic="";
	String strGmDrugExportLic="";
	String strCompLogo = "";
	String strCompSeal = "";
	String strDivisionId = "";
	String strShowPhoneNo = "";
	String strShowGMNAAddress = "";
	String strCompanyId = "";
	String strGSTStartDtFl = "";
	String strOrdredDate  = "";
	String strShippingDate = "";
	String strIsGSTEnabled = "";
	String strDLLocalNoVal="";
	String strDLExportNoVal="";
	String strShowDLNo ="";
	

	strCompanyId=gmDataStoreVO.getCmpid(); 
	String strCompanyLocale = GmCommonClass.getCompanyLocale(strCompanyId);
	GmResourceBundleBean gmResourceBundleBean =
			GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
			GmResourceBundleBean gmCompResourceBundleBean =
			GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	
	strIsGSTEnabled = GmCommonClass.parseNull((String) gmCompResourceBundleBean.getProperty("IS_GST_ENABLED"));
	strJasperName =  GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.JASPERNM"));
	strShowPhoneNo = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.SHOW_PHONE_NO"));
	strShowGMNAAddress = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.SHOW_GMNA_ADDRESS"));
	String strFooterLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.FOOTER"));
	String strTitleLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.TITLE"));
	String strBillToLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.BILLTO"));
	String strShipToLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.SHIPTO"));
	String strDoDateLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.DODATE"));
	String strDoNumLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.DONUM"));
	String strDistRepLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.DISTREP"));
	String strCusPONumLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.CUSTPONUM"));
	String strShipViaLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.SHIPVIA"));
	String strLBShipModeLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.SHIPMODE"));
	String strTrackingNoLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.TRACKINGNUM"));
	String strPartNumberLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.PARTNUMBER"));
	String strPartDescLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.PARTDESC"));
	String strQtyLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.QTY"));
	String strControlNumLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.CONTROLNUM"));
	String strBackOrderLbl = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.BACKORDER"));
	
	String strHeadAddress = ""; 
	
	strImagePath = System.getProperty("ENV_PAPERWORKIMAGES");
	boolean blBackFl = false;
	alPopulatedCartDtls =  GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("alCartDetails"));
	if (hmReturn != null)
	{
		
		hmOrderDetails 		=  GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("ORDERDETAILS"));
	    //alCartDetails 		=  GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("CARTDETAILS"));
	    if(alPopulatedCartDtls.isEmpty() && alPopulatedCartDtls != null)
	    {
	      alPopulatedCartDtls	=  GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("CARTDETAILS"));
	    }
		alBackOrderDetails	=  GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("BACKORDER"));
		hmShipDetails 		=  GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("SHIPDETAILS"));
		
		strOrdId = GmCommonClass.parseNull((String)hmOrderDetails.get("ID"));
		strShipAdd = GmCommonClass.parseNull((String)hmOrderDetails.get("SHIPADD"));
		strOrderType  = GmCommonClass.parseNull((String)hmOrderDetails.get("ORDERTYPE"));
		strOrdredDate = GmCommonClass.getStringFromDate((java.util.Date)hmOrderDetails.get("ODT"),strApplnDateFmt);
		strShippingDate = GmCommonClass.getStringFromDate((java.util.Date)hmOrderDetails.get("SHIPPING_DT"),strApplnDateFmt);
		strTrack	= GmCommonClass.parseNull((String)hmOrderDetails.get("TRACKNO"));
		strDivisionId = GmCommonClass.parseNull((String)hmOrderDetails.get("DIVISION_ID"));
		strDivisionId = strDivisionId.equals("")?"2000":strDivisionId;	
		 
		
		hmCompanyAdd = GmCommonClass.fetchCompanyAddress(strCompanyId, strDivisionId);
		strGmLocalLic =  GmCommonClass.parseNull((String) hmCompAtbData.get("GMLOCALLIC"));
		strGmExportLic =  GmCommonClass.parseNull((String) hmCompAtbData.get("GMEXPORTLIC"));
		strGmDrugLocalLic =  GmCommonClass.parseNull((String) hmAtbData.get("GMDRUGLOCALLIC"));
	    strGmDrugExportLic =  GmCommonClass.parseNull((String) hmAtbData.get("GMDRUGEXPORTLIC"));
	    strLocalDrugLic =  GmCommonClass.parseNull((String) hmAtbData.get("LOCALDRUGLIC"));
	    strExportDrugLic =  GmCommonClass.parseNull((String) hmAtbData.get("EXPORTDRUGLIC"));
		strCompLogo = GmCommonClass.parseNull((String)hmCompanyAdd.get("LOGO"));
		strCompLogo = strImagePath + strCompLogo + ".gif";
		
		//drug license no from account attrbute
		if(!strExportDrugLic.equals("") || !strLocalDrugLic.equals(""))
		{
			strShipAdd = strShipAdd+"<br>&nbsp;DL- "+strLocalDrugLic+"<br>&nbsp;DL- "+strExportDrugLic;
		}
		//drug license no from Order attribute table if not empty,need to display the value
		if(!strGmDrugLocalLic.equals("") || !strGmDrugExportLic.equals(""))
		{
			strDLLocalNoVal=strGmDrugLocalLic;
			strDLExportNoVal=strGmDrugExportLic;
			strShowDLNo = "Y";
		}
		//otherwise need to display the drug license no from rule table 
		else{
			
			//drug license no from rule table if not empty,need to display the value 
			if(!strGmLocalLic.equals("") || !strGmExportLic.equals("")){
				strDLLocalNoVal=strGmLocalLic;
				strDLExportNoVal=strGmExportLic;
				strShowDLNo = "Y";
			}
			else{
				//otherwise need not to display the drug license no 
				strDLLocalNoVal="";
				strDLExportNoVal="";
				strShowDLNo = "N";
			}
		}
	    strCompanyName = GmCommonClass.parseNull((String)hmCompanyAdd.get("COMPNAME"));
		strCompanyShortName = GmCommonClass.parseNull((String)hmCompanyAdd.get("COMP_SHORT_NAME"));
		strCompanyAddress =  GmCommonClass.parseNull((String)hmCompanyAdd.get("COMPADDRESS"));

		if(strShowPhoneNo.equals("YES")){
		   strCompanyPhone = GmCommonClass.parseNull((String)hmCompanyAdd.get("PH"));
		}
		if(strShowGMNAAddress.equals("YES")){
		  strCompanyName = GmCommonClass.parseNull((String)gmCompResourceBundleBean.getProperty("GMNA.COMPNAME"));
		  strCompanyAddress = GmCommonClass.parseNull((String)gmCompResourceBundleBean.getProperty("GMNA.COMPADDRESS"));
		  strCompanyPhone = GmCommonClass.parseNull((String)gmCompResourceBundleBean.getProperty("GMNA.PH"));
		}
		 strCompanyAddress = "&nbsp;"+ strCompanyAddress.replaceAll("/", "\n<br>&nbsp;"); 
		 strCompanyPhone = !strCompanyPhone.equals("")? "<br>&nbsp;&nbsp;"+strCompanyPhone: "";
		 strHeadAddress = "<br>&nbsp;&nbsp;<b>" +strCompanyName +"</b><BR>&nbsp;"+strCompanyAddress +strCompanyPhone;
	 } 
	// Combo box variable
	int intSize = 0;
	int intTotShippedQty = 0;
	HashMap hcboVal = null;
	ArrayList alComboList 	= new ArrayList();
	
	String strSelected  = "";
	String strCodeID = "";
	//GET THE VALUE FORM THE SERVLET FOR THE PRINT NAME
	String strPrintPurposelbl = GmCommonClass.parseNull((String)request.getAttribute("printPurpose"));
    strGSTStartDtFl = GmCommonClass.parseNull((String)request.getAttribute("GSTStartFl"));
	if(strPrintPurposelbl.equals("ORG_RECIPIENT")) {
	  strPrintPurposelbl = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("ORG_RECIPIENT"));
	}
	if(strPrintPurposelbl.equals("DUP_TRANSPORTER")) {
	 strPrintPurposelbl= GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("DUP_TRANSPORTER"));       
	}
    if(strPrintPurposelbl.equals("TRIP_SUPPLIER")) {
		 strPrintPurposelbl=GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TRIP_SUPPLIER"));
    } 
    //for Company Seal
	 strCompSeal = GmCommonClass.parseNull(gmCompResourceBundleBean.getProperty("COMP_SEAL"));
	 strCompSeal = strImagePath + strCompSeal + ".gif";
		if (hmShipDetails != null)
		{
			strShipDate = GmCommonClass.getStringFromDate((java.util.Date)hmShipDetails.get("SDT"),strApplnDateFmt);

		}

	
//PUT EXIXISTING ALL HASMAP VALUE IN THE ONE HAS MAP
	hmParam.putAll(hmOrderDetails);
	hmParam.putAll(hmCompAtbData);
	hmParam.putAll(hmAtbData);
	hmParam.putAll(hmCompanyAdd);
	hmParam.put("LBL_RECIPEINT",strPrintPurposelbl); 
	hmParam.put("GSTSTARTFL",strGSTStartDtFl);
	hmParam.put("INDSEAL",strCompSeal);
	hmParam.put("COMPLOGO",strCompLogo);
	hmParam.put("LBL_GMFOOTER",strFooterLbl);
	hmParam.put("LBL_TITLE",strTitleLbl );
	hmParam.put("LBL_BILLTO",strBillToLbl );
	hmParam.put("LBL_SHIPTO",strShipToLbl);
	hmParam.put("LBL_DODATE",strDoDateLbl );
	hmParam.put("LBL_DONUM",strDoNumLbl);
	hmParam.put("LBL_DISTREP",strDistRepLbl);
	hmParam.put("LBL_CUSTPO",strCusPONumLbl );
	hmParam.put("LBL_SHIPVIA",strShipViaLbl);
	hmParam.put("LBL_MODE",strLBShipModeLbl );
	hmParam.put("LBL_TRACKINGNO",strTrackingNoLbl );
	hmParam.put("LBL_PARTNUMBER",strPartNumberLbl );
	hmParam.put("LBL_PARTDESC",strPartDescLbl );
	hmParam.put("LBL_QTY",strQtyLbl);
	hmParam.put("LBL_CONTROLNUM",strControlNumLbl );
	hmParam.put("ALBACKORDER",alBackOrderDetails );
	hmParam.put("GSTENABLED",strIsGSTEnabled );
	hmParam.put("BACKORDERCNUM",strBackOrderLbl );
	hmParam.put("DLLOCALLICNO",strDLLocalNoVal);
	hmParam.put("DLEXPORTLICNO",strDLExportNoVal);
	hmParam.put("SHOWDLNO",strShowDLNo);
	hmParam.put("STRSHIPADD",strShipAdd);
	hmParam.put("COMAPNYSEAL",strCompSeal);
	hmParam.put("ORDT",strOrdredDate);
	hmParam.put("SHIPDT",strShippingDate);
	hmParam.put("STRORDERTYPE",strOrderType);
	hmParam.put("SUBREPORT_DIR", request.getSession().getServletContext().getRealPath(GmCommonClass.getString("GMJASPERLOCATION")));

	
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Packing Slip </TITLE>

<script language="JavaScript" src="<%=strJsPath%>/GmPackSlip.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script>
var source = "<%=strSource%>";
var tdinnner = "";
var hopt = '<%=strOpt%>';
var strTrack ='<%=strTrack%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10"  onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmOrder" method="post">
<input type="hidden" name="hMode" value="PrintPack">
<input type="hidden" name="hOrdId" value="<%=strOrdId%>">

<%if(strAction.equals("PrintPack")&&strGUID.equals(""))
	{
%>
	<table border="0" width="700" cellspacing="0" cellpadding="0" >
	<fmt:setLocale value="${requestScope.CompanyLocale}"/>
	<fmt:setBundle basename="properties.Paperwork"/>
		<tr>
			<td align="center" height="30" id="button" class="RightText" >
				Project Type:&nbsp;<select name="Cbo_ProjectType" class="RightText" tabindex="1" 
				onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"
				onChange="javascript:fnPrintPack();" >
<% /****** Combo Box Value ***********/
				alComboList = (ArrayList)request.getAttribute("alcomboList");	
				strProjectType = (String) request.getAttribute("ProjectType");
				intSize = alComboList.size();
				hcboVal = new HashMap();
				
				// Functionality to remove the tracking information 
				// If the part is not delivered by Globus 
				if (!strProjectType.equals("1500"))
				{
					strShipDate = "";
					 strShipCarr = "";
				    String strShipMode = "";  
					strTrack = "";				
				}
			  	for (int i=0;i<intSize;i++)
			  	{	hcboVal = (HashMap)alComboList.get(i);
			  		strCodeID = (String)hcboVal.get("CODEID");
					strSelected = strProjectType.equals(strCodeID)?"selected":"";
%>					<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%			  	}
%>				</select>
				<gmjsp:button value="&nbsp;Print&nbsp;" name="Btn_Print" gmClass="button" tabindex="2" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				<gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_Close" gmClass="button" tabindex="3"	onClick="fnClose();" buttonType="Load" />
			</td>
		<tr>
	</table>

<%
	}
%>


<table>
		<tr>
		<td><div>
			<td><div>
			       
					<%
					
						    String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
					        GmJasperReport gmJasperReport = new GmJasperReport();
					        gmJasperReport.setRequest(request);
							gmJasperReport.setResponse(response);
					        gmJasperReport.setJasperReportName(strJasperName);
			                gmJasperReport.setHmReportParameters(hmParam);
			                gmJasperReport.setReportDataList(alPopulatedCartDtls);
		                    gmJasperReport.setBlDisplayImage(true);
							strHtmlJasperRpt = gmJasperReport.getXHtmlReport();
						%>
					
					<%=strHtmlJasperRpt%>
						
				</div></td>
		</tr>

	</table>

</FORM>


<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
