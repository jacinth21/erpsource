
<%
/**********************************************************************************
 * File		 		: GmDashBoardHome.jsp
 * Desc		 		: Customer DashBoard Home
 * Version	 		: 1.0
 * author			: Tarika Chandure
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="com.globus.common.beans.GmCommonConstants"%>
<%@ page import="com.globus.common.beans.GmCommonClass"%>
<%@ page import="org.apache.struts.action.ActionForm"%>

<%@ taglib prefix="fmtDashBoardHome" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmDashBoardHome.jsp -->
<fmtDashBoardHome:setLocale value="<%=strLocale%>"/>
<fmtDashBoardHome:setBundle basename="properties.labels.custservice.GmDashBoardHome"/>

<HTML>
<HEAD>
<%
String strCompanyId = gmDataStoreVO.getCmpid();
String srtShowInTrans = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue(strCompanyId,"SHOW-IN-TRANSIT"));
if (request.getAttribute("hAction").equals("CUSTDASHBOARD")) {
Object bean = pageContext.getAttribute("frmCustDashBoardDispatch", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
}else if (request.getAttribute("hAction").equals("OPERDASHBOARD")) {
	Object bean = pageContext.getAttribute("frmOperDashBoardDispatch", PageContext.REQUEST_SCOPE);
	pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
}else{
Object bean = pageContext.getAttribute("frmAdverseEvent", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
}
String strHeight = "";
if(strClientSysType.equals("IPAD")){
	strHeight ="auto";
}else{
	strHeight ="660px";
}
%>
<TITLE>Globus Medical: DashBoard Home</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/DemandSheet.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDashboard.js"></script>
<script>
var dStatus = "";
</script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>

</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnAjaxTabLoad();">
<html:form action="/gmCustDashBoardDispatch.do">
	<html:hidden property="hAction" />
	<html:hidden property="hOrdId" />
	<html:hidden property="hConsignId" />
	<html:hidden property="hId" />
	<html:hidden property="hRAId" />
	<html:hidden property="hTransferId" />
	<html:hidden property="hMode" />
	<html:hidden property="hTransferType" />
</html:form>

<html:form action="/gmOperDashBoardDispatch.do">
	<html:hidden property="hAction" />
	<html:hidden property="hId" />
	<html:hidden property="hMode" />
	<html:hidden property="hFrom" />
	<html:hidden property="hNCMRId" />
	<html:hidden property="hConsignId" />
	<html:hidden property="hType" />
	<html:hidden property="hOpt" />
</html:form>

<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
	<tr>
		<td height="25" class="RightDashBoardHeader"><fmtDashBoardHome:message key="LBL_DASHBOARD"/></td>
	</tr>
	<tr>
		<td>
		<ul id="maintab" class="shadetabs">
			<%String strAction=(String)request.getAttribute("hAction");
			if (strAction.equals("CUSTDASHBOARD")) {%>
			<li><a
				href="/gmCustDashBoardDispatch.do?method=wipOrderDashBoard"
				title="WipOrderDashBoard" rel="#iframe"><fmtDashBoardHome:message key="LBL_WIP_ORDERS"/></a></li>
			<li><a
				href="/gmCustDashBoardDispatch.do?method=consignedSetsItemsProcessing"
				rel="#iframe" title="ConsignedSetsItemsProcessing"><fmtDashBoardHome:message key="LBL_CONSIGNED_ITEMS"/></a></li>
			<li><a href="/gmOperDashBoardDispatch.do?method=consignedItems"
				title="ConsignedItems" rel="#iframe"><fmtDashBoardHome:message key="LBL_INHOUSE_TRANSACTIONS"/></a></li>
			<li><a
				href="/gmCustDashBoardDispatch.do?method=returnsPendingCredits"
				title="ReturnsPendingCredits" rel="#iframe"><fmtDashBoardHome:message key="LBL_RETURNS_PENDING"/>
			Credits</a></li>
			<li><a
				href="/gmCustDashBoardDispatch.do?method=consignedTransfer"
				title="ConsignedTransfer" rel="#iframe"><fmtDashBoardHome:message key="LBL_CONSIGNMENT_TRANSFER"/></a></li>
			<li><a
				href="/gmCustDashBoardDispatch.do?method=dummyConsignment"
				title="DummyConsignment" rel="ajaxcontentarea"><fmtDashBoardHome:message key="LBL_DUMMY_CONSIGNMENTS"/></a></li>
			
 	<% if(srtShowInTrans.equals("Y"))  {  %>  
			<li><a
				href="/gmCustDashBoardDispatch.do?method=inTransitProcessing"
				rel="#iframe" title="InTransit Tranasctions"><fmtDashBoardHome:message key="LBL_IN_TRANSIT"/></a></li>
			 <%} %>	  
				
			<%}else if(strAction.equals("OPERDASHBOARD")){%>
			<li style="margin-left: 0; margin-top: 0;"><a
				href="/gmOperDashBoardDispatch.do?method=dashboardDHR" title="DHR"
				rel="#iframe"><fmtDashBoardHome:message key="LBL_DHR"/></a></li>
				<% if (strCountryCode.equals("en")){ %>
			<li><a href="/gmOperDashBoardDispatch.do?method=dashboardNCMR"
				rel="ajaxcontentarea" title="NCMR"><fmtDashBoardHome:message key="LBL_NCMR"/></a></li>
			<li><a
				href="/gmCustDashBoardDispatch.do?method=returnsPendingCredits"
				title="ReturnsPendingCredits" rel="#iframe"><fmtDashBoardHome:message key="LBL_RETURNS_PENDING"/>
			Credits</a></li>
			<%} %>
			<li><a href="/gmOperDashBoardDispatch.do?method=consignedItems"
				title="ConsignedItems" rel="#iframe"><fmtDashBoardHome:message key="LBL_INHOUSE_TRANSACTIONS"/></a></li>
			<%}else if(strAction.equals("LOGISDASHBOARD")){%>
			<li><a
				href="/gmCustDashBoardDispatch.do?method=returnsPendingCredits"
				title="ReturnsPendingCredits" rel="#iframe"><fmtDashBoardHome:message key="LBL_RETURNS_PENDING"/>
			Credits</a></li>
			<li><a href="/gmOperDashBoardDispatch.do?method=consignedItems"
				title="ConsignedItems" rel="#iframe"><fmtDashBoardHome:message key="LBL_INHOUSE_TRANSACTIONS"/></a></li>
			<%}else if(strAction.equals("QUALDASHBOARD")){%>
			<li><a href="/gmOperDashBoardDispatch.do?method=dashboardDHR"
				title="DHR" rel="ajaxcontentarea"><fmtDashBoardHome:message key="LBL_DHR"/></a></li>
			<li><a href="/gmOperDashBoardDispatch.do?method=dashboardNCMR"
				rel="ajaxcontentarea" title="NCMR"><fmtDashBoardHome:message key="LBL_NCMR"/></a></li>
			<li><a
				href="/gmCustDashBoardDispatch.do?method=returnsPendingCredits"
				title="ReturnsPendingCredits" rel="#iframe"><fmtDashBoardHome:message key="LBL_RETURNS_PENDING"/>
			Credits</a></li>
			<li><a href="/gmOperDashBoardDispatch.do?method=consignedItems"
				title="ConsignedItems" rel="#iframe"><fmtDashBoardHome:message key="LBL_INHOUSE_TRANSACTIONS"/></a></li>
			<%}else if(strAction.equals("SHIPDASHBOARD")){%>
			<li><a
				href="/gmCustDashBoardDispatch.do?method=wipOrderDashBoard"
				title="WipOrderDashBoard" rel="ajaxcontentarea"><fmtDashBoardHome:message key="LBL_WIP_ORDERS"/></a></li>
			<li><a
				href="/gmCustDashBoardDispatch.do?method=consignedSetsItemsProcessing"
				rel="ajaxcontentarea" title="ConsignedSetsItemsProcessing"><fmtDashBoardHome:message key="LBL_CONSIGNED_SETS_ITEMS"/></a></li>
				<li><a 
				href="/gmOperDashBoardDispatch.do?method=consignedItems"
				title="ConsignedItems" rel="#iframe"><fmtDashBoardHome:message key="LBL_INHOUSE_TRANSACTIONS"/></a></li>
			<%}else if(strAction.equals("CLINICALDASHBOARD")){%>
			<li><a
				href="/gmClinicalDashBoardDispatch.do?method=pendingForms"
				title="pendingForms" rel="ajaxcontentarea"><fmtDashBoardHome:message key="LBL_PENDING_FORMS"/></a></li>
			<li><a href="/gmClinicalDashBoardDispatch.do?method=outOfWindow"
				rel="ajaxcontentarea" title="outOfWindow"><fmtDashBoardHome:message key="LBL_OUT_OF_WINDOW"/></a></li>
			<li><a
				href="/gmClinicalDashBoardDispatch.do?method=pendingVerification"
				rel="ajaxcontentarea" title="pendingVerification"><fmtDashBoardHome:message key="LBL_PENDING_VERIFICATION"/></a></li>
			<li><a
				href="/gmRptAdverseEvent.do?method=reportAdverseEvent&strOpt=Dashboard&selectAll=true&HIDDENSITELIST=dashboard"
				rel="ajaxcontentarea" title="adverseEvent"><fmtDashBoardHome:message key="LBL_OPEN_ADVERSE_EVENT"/></a></li>
			<%}else if(strAction.equals("RECVDASHBOARD")){%>
			<li style="margin-left: 0; margin-top: 0;"><a
				href="/gmOperDashBoardDispatch.do?method=dashboardDHR" title="DHR"
				rel="#iframe"><fmtDashBoardHome:message key="LBL_DHR"/></a></li>
			<li><a href="/gmOperDashBoardDispatch.do?method=dashboardNCMR"
				rel="ajaxcontentarea" title="NCMR"><fmtDashBoardHome:message key="LBL_NCMR"/></a></li>
			<li><a href="/gmOperDashBoardDispatch.do?method=consignedItems"
				title="ConsignedItems" rel="#iframe"><fmtDashBoardHome:message key="LBL_INHOUSE_TRANSACTIONS"/></a></li>
			<%}else if(strAction.equals("INVDASHBOARD")){%>
			<li style="margin-left: 0; margin-top: 0;"><a
				href="/gmOperDashBoardDispatch.do?method=dashboardDHR" title="DHR"
				rel="#iframe"><fmtDashBoardHome:message key="LBL_DHR"/></a></li>
			<li><a href="/gmOperDashBoardDispatch.do?method=consignedItems"
				title="ConsignedItems" rel="#iframe"><fmtDashBoardHome:message key="LBL_INHOUSE_TRANSACTIONS"/></a></li>
			<li><a
				href="/gmCustDashBoardDispatch.do?method=wipOrderDashBoard"
				title="WipOrderDashBoard" rel="#iframe"><fmtDashBoardHome:message key="LBL_WIP_ORDERS"/></a></li>
			<li><a
				href="/gmCustDashBoardDispatch.do?method=consignedSetsItemsProcessing"
				rel="#iframe" title="ConsignedSetsItemsProcessing"><fmtDashBoardHome:message key="LBL_CONSIGNED_ITEMS"/></a></li>				
			<%}%>
		</ul>

		</td>
	</tr>

	<tr>
		<td>
		<div id="ajaxdivcontentarea" class="contentstyle"
			style="display: visible; height: <%=strHeight%>; width: 1000px; overflow: auto; margin-left: 0; margin-top: 0;"></div>
		</td>
	</tr>

</table>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
