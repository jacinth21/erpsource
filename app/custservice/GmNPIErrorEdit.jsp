<!DOCTYPE html>
<!-- \custservice\GmNPIErrorEdit.jsp -->
<%@ taglib prefix="fmtCart" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ include file="/common/GmHeader.inc" %>
<html lang="en">
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
}
String strScreenType = "NpiErrorEdit";
String strSalesRepId = GmCommonClass.parseNull(request.getParameter("SALESREPID"));
%>
<fmtCart:requestEncoding value="UTF-8" />
<fmtCart:setLocale value="<%=strLocale%>"/>
<fmtCart:setBundle basename="properties.labels.common.cart.GmCommonCart"/>
<head>
<meta charset="UTF-8">

<title>NPI Details</title>

<link rel="stylesheet" href="<%=strCssPath%>/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=strJsPath%>/common/bootstrap.min.css">
<link rel="stylesheet" href="<%=strCssPath%>/GmNPIProcess.css">
<link rel="stylesheet" href="<%=strCssPath%>/GmNPIErrorEdit.css">
<script src="<%=strJsPath%>/common/bootstrap.min.js"></script>
<script type="text/javascript">

var salesRepId = '<%=strSalesRepId%>';

/* function fnSetNPIId(obj){
	var v_npiId = document.getElementById("surgeonName").value;
	document.getElementById("searchnpiNum").value = v_npiId;
	$("#searchsurgeonName").focus();
}

function fnSetSurgeonName(obj){
	var v_surgeonName = document.getElementById("npiNum").value;
	document.getElementById("searchsurgeonName").value = v_surgeonName;
} */

function fnSetNPINum(obj){
	console.log(JSON.stringify(obj));
	var surgeonnm = document.getElementById("surgeonName").value;
	var surgeonArr = surgeonnm.split("#");
	document.getElementById("searchnpiNum").value = surgeonArr[0];
	document.getElementById("surgeonId").value = surgeonArr[1];
}

function fnSetSurgeonNm(obj){	
	var npiId = document.getElementById("npiNum").value;
	var surgeonArr = npiId.split("#");
	document.getElementById("searchsurgeonName").value = surgeonArr[0];
	document.getElementById("surgeonId").value = surgeonArr[1];
}

</script>
<style type="text/css">
</style>
</head>
<body>

<input type="hidden" name="hRefId" id="hRefId" value="">
<input type="hidden" name="hAccountNm" id="hAccountNm" value="">
<input type="hidden" name="hRepNm" id="hRepNm" value="">
<input type="hidden" name="hAccountId" id="hAccountId" value="">
<input type="hidden" name="surgeonId" id="surgeonId" >
<!-- <input type="hidden" name="hNpiPareTranId" id="hNpiPareTranId" value="">
<input type="hidden" name="hScreen" id="hScreen" value=""> -->

	<div id="myModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header modal-header-bg">
					<button type="button" class="close main-close" data-dismiss="modal">&times;</button>
					<label class="main-quest" title="* Press Enter key in surgeon name to save records &#13;* Click delete icon to remove surgeon details" >&quest;</label>
               		<h4 class="modal-title" id="npilbl"></h4>
				</div>
				<div class="modal-body modal-body-grp data-grp">
	               		<div class="div-new-npi-dtls">
	               			<div class="div-header-sec">
								<h5 id="lblOrderId"><b>Order ID:</b> <span id="valOrderId"></span></h5>
								<h5 id="lblRep"><b>Sales Rep: </b><span id="valRep"></span></h5>
								<h5 id="lblAccount"><b>Account: </b><span id="valAccount"></span></h5>
							</div>
               			<div class="label-grp lbl-curr-dtls">
               				<div id="npilookup"><a href="https://npidb.org/" target="_blank">NPI Lookup</a></div>
               				<label id="lblNpiNum"> NPI #</label>
               				<label id="lblSurgeonName"> Surgeon Name</label>
               			</div>
               			<div class="field-grp">
                			<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
								<jsp:param name="CONTROL_NAME" value="npiNum" />
								<jsp:param name="METHOD_LOAD" value="loadSurgeonNameWId&strOpt=npilist" />
								<jsp:param name="WIDTH" value="157" />
								<jsp:param name="CSS_CLASS" value="search" />
								<jsp:param name="CONTROL_NM_VALUE" value="" />
								<jsp:param name="CONTROL_ID_VALUE" value="" />
								<jsp:param name="SHOW_DATA" value="40"/>
								<jsp:param name="AUTO_RELOAD" value="fnSetSurgeonNm(this);" />
							</jsp:include>	
                			<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
								<jsp:param name="CONTROL_NAME" value="surgeonName" />
								<jsp:param name="METHOD_LOAD" value="loadSurgeonNameWId&strOpt=surgeonlist" />
								<jsp:param name="WIDTH" value="250" />
								<jsp:param name="CSS_CLASS" value="search" />
								<jsp:param name="CONTROL_NM_VALUE" value="" />
								<jsp:param name="CONTROL_ID_VALUE" value="" />
								<jsp:param name="SHOW_DATA" value="40" />
								<jsp:param name="AUTO_RELOAD" value="fnSetNPINum(this);" />
							</jsp:include>	
							&nbsp;&nbsp;&nbsp;
                			<button class="btn btn-primary btn-xs btn-add" type="button" title="${varTitle}" id="addbtn" >
                			Add
                			</button>
                			             			
                		</div>
                		<div>
                			<label id="lblSurgDtls"> &nbsp;Added Surgeon details:</label>
                		</div>
	                	<div class="values-grp" id="selectedData">
	                		<ul id="ulCurrDtls"></ul>
	                	</div>
	                </div>
				</div>
			</div>
		</div>
	</div>
<script>
var closebtns = document.getElementsByClassName("close-sm");
var tickbtns = document.getElementsByClassName("tick-sm");
var i;
var entrycnt = 0;
var parentDataSetCnt = 0;
var savedNpiIdStr = '|';

$('#myModal').on('shown.bs.modal', function (e) {
	var refId = document.getElementById("hRefId").value;
	var accNm = document.getElementById("hAccountNm").value;
	var repNm = document.getElementById("hRepNm").value;
	accNm = accNm.replace(/~/g,' ');
	repNm = repNm.replace(/~/g,' ');
	
	$("#npilbl").empty();
	$("#npilbl").append("NPI Details - "+ refId);
	$("#valOrderId").empty();
	$("#valOrderId").append(refId);
	$("#valRep").empty();
	$("#valRep").append(accNm);
	$("#valAccount").empty();
	$("#valAccount").append(repNm);
	savedNpiIdStr = '|';
	fnFetchData();
})

var npiTxnId;
 
$("#searchsurgeonName,#searchnpiNum").keypress(function(e){
	var key = e.which;
  	if(key == 13){
   		callbackDispNPIDtls();
  	}
});
   
$("#addbtn").click(function(){
	callbackDispNPIDtls();
});
	
var callbackDispNPIDtls = function(){
	
   	var dataAppend;
   	var tempNpiData = '';
   	var acceptNum;
   	var errorFl;
   	// If the value is already availabe in the div, should not add it again
   	if($('#searchnpiNum').val() != ''){
   		acceptNum = /[^0-9]/g;
        errorFl = acceptNum.test($('#searchnpiNum').val());
        if (errorFl) {  /*Accept only Digits in NPI# field*/
        	Error_Details("Please provide NPI# in digits");
        }
   		tempNpiData = tempNpiData + $('#searchnpiNum').val();
   	}
   	if($('#searchnpiNum').val() != '' && $('#searchsurgeonName').val() != ''){
   		tempNpiData = tempNpiData + " - ";
   	}
   	if($('#searchsurgeonName').val() != ''){
   		acceptNum = /^[a-zA-Z\.\, ]*$/;
        errorFl = acceptNum.test($('#searchsurgeonName').val());
        if (!errorFl) {  /*Accept only Digits in NPI# field*/
        	Error_Details("Please provide surgeon name in letters");
        }
   		tempNpiData = tempNpiData + $('#searchsurgeonName').val();
   	}
   	if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}
   	if(savedNpiIdStr.indexOf(tempNpiData) != -1){
   		$("#searchnpiNum").val('');
   	   	$("#searchsurgeonName").val('');
   	 	$("#searchnpiNum").focus();
   		return;
   	}
   	
   	fnSaveRow('','');
   	
   	if($('#searchnpiNum').val() != '' || $('#searchsurgeonName').val() != ''){
   		dataAppend = "<li id='currDtls' style=\"list-style-type: none;\"><div>";	
   	}
   	if($('#searchnpiNum').val() != ''){
   		dataAppend = dataAppend + $('#searchnpiNum').val();
   		savedNpiIdStr = savedNpiIdStr + $('#searchnpiNum').val();
   	}
   	if($('#searchnpiNum').val() != '' && $('#searchsurgeonName').val() != ''){
   		dataAppend = dataAppend + " - ";
   		savedNpiIdStr = savedNpiIdStr + " - ";
   	}
   	if($('#searchsurgeonName').val() != ''){
   		dataAppend = dataAppend + $('#searchsurgeonName').val();
   		savedNpiIdStr = savedNpiIdStr + $('#searchsurgeonName').val();
   	}
   	if($('#searchnpiNum').val() != '' || $('#searchsurgeonName').val() != ''){
   		savedNpiIdStr = savedNpiIdStr + '|';
   		dataAppend = dataAppend + "&nbsp;&nbsp;&nbsp;<span class='close-sm' id='closebtn'><a href=\"#\"><span class=\"glyphicon glyphicon-remove\" title='Remove' width='15' height='15'></span></a></span>&nbsp;&nbsp;";
   		dataAppend = dataAppend + " <input type=\"hidden\" name=\"hNpiTxnId\" id=\"hNpiTxnId\" value="+npiTxnId+"></div></li>";
   	}

   	$("#selectedData ul").append(dataAppend);
   	
   	$("#searchnpiNum").focus();
   	
   	for (i = 0; i < closebtns.length; i++) {
   		closebtns[i].onclick = function(rownum) {
   	      fnRemove($(this));
   		}
   	}
    	
   	$("#searchnpiNum").val('');
   	$("#searchsurgeonName").val('');
};

// To save each row whenever added
function fnSaveRow(npiId, npiName){

		var v_npiId = document.getElementById("searchnpiNum").value;
		var v_surgeonNm = document.getElementById("searchsurgeonName").value;
		var v_surgeonId = document.getElementById("surgeonId").value;
    	$.ajax({
			url : "/gmNPIProcess.do?method=saveNPIWIDTrasactionDetails&submitType=<%=strScreenType%>&"+ fnAppendCompanyInfo(),
			type : "POST",
			async: false,
			data: {
	        	txnid : document.getElementById("hRefId").value,
	        	accountid : document.getElementById("hAccountId").value,
	        	npinum : v_npiId,
	        	surgeonname : v_surgeonNm,
	        	surgeonId : v_surgeonId
	        },
	        dataType: "json",

			success : function(data) {
				
				// to validate the data and show the message
				if (!data.length) {
					npiTxnId = data;
				} else {
				}
			}
		});
 }

// To remove the NPI details on click of remove icon
function fnRemove(obj){
	var parentNpiTxnIdObj = document.getElementById("hNpiPareTranId");
	var parentNpiTxnId = parentNpiTxnIdObj != undefined? parentNpiTxnIdObj.value:'';
	var npitxnId = $(obj).parent().find("#hNpiTxnId").val();
	var remTxtVal = $(obj).parent().text().trim();
	$.ajax({
		url : "/gmNPIProcess.do?method=cancelNPITrasactionDetails&"+ fnAppendCompanyInfo(),
		type : "POST",
		async: false,
		dataType: "json",
        data: {
        	npitranid: npitxnId
        },
		
		success : function(data) {
			// to validate the data and show the message
			if (!data.length) {
			} else {
			}
		}
	});
	$(obj).parent().parent().remove();
	// Remove the deleted value from string also to add it again in the div
	if(savedNpiIdStr != ''){
		savedNpiIdStr = savedNpiIdStr.replace(remTxtVal+'|','');
	}

}

// To load the saved details to the popup, if loading from Modify Order screen
function fnFetchData(){
	var dataAppend;
	$("#selectedData ul").empty();
	$.ajax({
		url : "/gmNPIProcess.do?method=fetchNPITrasactionDetails&"+ fnAppendCompanyInfo(),
		type : "POST",
		async: false,
		data: {
			txnid: document.getElementById("hRefId").value
        },
        dataType: "json",

		success : function(data) {
			if (data.length) {
				 $.each(data, function(index, element) {
					var id = element.NPTTXNID;
					var npiId = element.NPIID;
					var sugeonNm = element.SURGEONNM;
					var validFl = element.VALIDFL;
					var userValidFl = element.USERVALIDFL;
					
					if(npiId != '' || sugeonNm != ''){
			    		dataAppend = "<li id='currDtls'><div>";	
			    	}
					if(npiId != ''){
						dataAppend = dataAppend + npiId;
						savedNpiIdStr = savedNpiIdStr + npiId;
					}
					if(npiId != '' && sugeonNm != ''){
						dataAppend = dataAppend + " - ";
						savedNpiIdStr = savedNpiIdStr + " - ";
					}
					if(sugeonNm != ''){
						dataAppend = dataAppend + sugeonNm;
						savedNpiIdStr = savedNpiIdStr + sugeonNm;
					}
					if(npiId != '' || sugeonNm != ''){
						savedNpiIdStr = savedNpiIdStr + '|';
						dataAppend = dataAppend + "&nbsp;&nbsp;&nbsp;<span class=\"close-sm\" id=\"closebtn\"><a href=\"#\"><span class=\"glyphicon glyphicon-remove\" title='Remove' width='15' height='15'></span></a></span>&nbsp;&nbsp;";
						if(validFl == 'N' && userValidFl != 'Y'){
							dataAppend = dataAppend + "&nbsp;&nbsp;&nbsp;<span class=\"tick-sm\" id=\"tickmark\"><a href=\"#\"><span class=\"glyphicon glyphicon-ok\" title='Mark as Valid' width='15' height='15'></span></a></span>&nbsp;&nbsp;";
						}
						dataAppend = dataAppend + " <input type=\"hidden\" name=\"hNpiTxnId\" id=\"hNpiTxnId\" value="+id+"></div></li>";
					}
					$("#selectedData ul").append(dataAppend);
					$(".tick-sm").parent().addClass("gmFontRed");
					
				}); 
				 
				 for (i = 0; i < closebtns.length; i++) {
						closebtns[i].onclick = function(rownum) {
					      fnRemove($(this));
						}
				 }
				 for(i = 0; i< tickbtns.length; i++){
					 tickbtns[i].onclick = function(rownum){
						 fnValidateNPI($(this));
					 }
				 }
			}
		},
		error: function(jqXHR, exception){
		
		}
	});
}

function fnValidateNPI(obj){
	var npitxnId = $(obj).parent().find("#hNpiTxnId").val();
	$.ajax({
		url : "/gmNPIProcess.do?method=saveValidNPI&"+ fnAppendCompanyInfo(),
		type : "POST",
		async: false,
		dataType: "json",
        data: {
        	npitranid: npitxnId
        },
		
		success : function(data) {
			// to validate the data and show the message
			if (!data.length) {
			} else {
			}
		}
	});
	fnFetchData();
}


</script>
</body>
<script src="<%=strJsPath%>/common/bootstrap.min.js"></script>
</html>