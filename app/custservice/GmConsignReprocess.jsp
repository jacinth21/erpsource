 <%@page import="java.util.Locale"%>
<%
/**********************************************************************************
 * File		 		: GmConsignReprocess.jsp
 * Desc		 		: This screen is used 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtConsignReprocess" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- custservice\GmConsignReprocess.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtConsignReprocess:setLocale value="<%=strLocale%>"/>
<fmtConsignReprocess:setBundle basename="properties.labels.custservice.GmConsignReprocess"/>


<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction");
	if (strhAction == null)
	{
		strhAction = (String)request.getAttribute("hAction");
	}
	strhAction = (strhAction == null)?"Load":strhAction;

	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strDistId = (String)request.getAttribute("Cbo_Id")==null?"":(String)request.getAttribute("Cbo_Id");
	String strSetId = (String)request.getAttribute("Cbo_SetId")==null?"":(String)request.getAttribute("Cbo_SetId");
	
	ArrayList alDistributor = new ArrayList();
	ArrayList alSets = new ArrayList();
	ArrayList alConsign = new ArrayList();
	ArrayList alReturn = new ArrayList();

	if (hmReturn != null)
	{
		alDistributor = (ArrayList)hmReturn.get("DISTLIST");
		alSets = (ArrayList)hmReturn.get("SETLIST");
		if (strhAction.equals("Reload"))
		{
			alConsign = (ArrayList)hmReturn.get("CONSIGN");
			alReturn = (ArrayList)hmReturn.get("RETURNS");
		}
	}


	int intSize = 0;
	HashMap hcboVal = null;

	String strConId = "";
	String strPerson = "";
	String strDate = "";
	String strComments = "";
								
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Reprocess Consignments </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>

var lblDistributorName = '<fmtConsignReprocess:message key="LBL_DISTRIBUTOR_NAME"/>';

function fnSubmit()
{
	if (document.frmMain.Chk_ActiveFl.checked)
	{
		document.frmMain.Chk_ActiveFl.value = 'Y';
	}
	document.frmMain.submit();
}

function fnReload()
{
	document.frmMain.hAction.value = "Reload";
	document.frmMain.submit();
}

function fnInitiate()
{
	var obj = document.all.Chk_SetId;
	var len = obj.length;
	var str = '';
	for (var i=0;i<len;i++)
	{
		if (obj[i].checked == true)
		{
			str = str + obj[i].id + ',';
		}
	}
	document.frmMain.hSetIds.value = str.substr(str,str.length-1);
	document.frmMain.hAction.value = "Initiate";
	//alert(document.frmMain.hSetIds.value);
	document.frmMain.submit();
}

function fnSetConId(val)
{
	document.frmMain.hConId.value = val;
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10"">
<FORM name="frmMain" method="POST" action="<%=strServletPath%>/GmConsignModifyServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hConId" value="">
<input type="hidden" name="hOpt" value="Reprocess">
<input type="hidden" name="hSetIds" value="">
	<table border="0" width="750" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="748" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td class="Line" colspan="2"></td></tr>
					<tr>
						<td colspan="2" height="25" class="RightDashBoardHeader">&nbsp;<fmtConsignReprocess:message key="LBL_REPROCESS_CONSIGNMENTS"/></td>
					</tr>
					<tr><td class="Line" colspan="2"></td></tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtConsignReprocess:message key="LBL_DISTRIBUTOR_NAME"/>:</td>
						<td id="list">&nbsp;
					&nbsp;<select name="Cbo_Id" id="Region" class="RightText" tabindex="3" onFocus="changeBgColor(this,'#AACCE8');"  onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtConsignReprocess:message key="LBL_CHOOSE_ONE"/>
<%
						intSize = alDistributor.size();
						hcboVal = new HashMap();
						for (int i=0;i<intSize;i++)
						{
							hcboVal = (HashMap)alDistributor.get(i);
							strCodeID = (String)hcboVal.get("ID");
							strSelected = strDistId.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%
				  		}
%>
						</select>
						</td>
					</tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtConsignReprocess:message key="LBL_SET_NAME"/>:</td>
						<td id="cont">&nbsp;
						&nbsp;<select name="Cbo_SetId" id="Region" class="RightText" tabindex="3" onFocus="changeBgColor(this,'#AACCE8');"  onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtConsignReprocess:message key="LBL_CHOOSE_ONE"/>
<%
							intSize = alSets.size();
							hcboVal = new HashMap();
							for (int i=0;i<intSize;i++)
							{
								hcboVal = (HashMap)alSets.get(i);
								strCodeID = (String)hcboVal.get("ID");
								strSelected = strSetId.equals(strCodeID)?"selected":"";
%>
								<option <%=strSelected%> value="<%=strCodeID%>"><%=strCodeID%>:<%=GmCommonClass.getStringWithTM((String)hcboVal.get("NAME"))%></option>
<%
					  		}
%>
						</select>&nbsp;&nbsp;<fmtConsignReprocess:message key="BTN_GO" var="varGo"/><gmjsp:button value="&nbsp;${varGo}&nbsp;" gmClass="button" onClick="fnReload();" tabindex="16" buttonType="Load" />&nbsp;
						</td>
					</tr>
<%
	if (strhAction.equals("Reload"))
	{
%>					
					<tr>
						<td colspan="2">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr><td colspan="4" class="Line" height="1"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td HEIGHT="24" colspan="4"><fmtConsignReprocess:message key="LBL_CONSIGNMENT_DETAILS"/></td>
								</tr>
								<tr><td colspan="4" class="Line" height="1"></td></tr>
								<tr bgcolor="#EEEEEE" class="RightTableCaption">
									<td HEIGHT="24" width="150"><fmtConsignReprocess:message key="LBL_CONSIGN_ID"/></td>
									<td width="150"><fmtConsignReprocess:message key="LBL_CONSIGNED_BY"/></td>
									<td width="100" align="center"><fmtConsignReprocess:message key="LBL_DATE"/></td>
									<td width="350">&nbsp;<fmtConsignReprocess:message key="LBL_COMMENTS"/></td>
								</tr>
								<tr>
									<td colspan="4">
										<DIV style="overflow:auto; height:100px">
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
<%
						HashMap hmLoop = new HashMap();
						int intCount = 0;
						String strLine = "";
						String strTemp = "";
						
						intSize = alConsign.size();
						if (intSize > 0)
						{
			
							for (int i = 0;i < intSize ;i++ )
							{
								hmLoop = (HashMap)alConsign.get(i);
			
								strConId = GmCommonClass.parseNull((String)hmLoop.get("CID"));
								strPerson = GmCommonClass.parseNull((String)hmLoop.get("UPDBY"));
								strComments = GmCommonClass.parseNull((String)hmLoop.get("COMMENTS"));
								strDate = GmCommonClass.parseNull((String)hmLoop.get("SDATE"));
%>
										<tr >
											<td width="150" class="RightText">&nbsp;<input type="radio" name="rad" onClick="fnSetConId('<%=strConId%>');"><%=strConId%></td>
											<td width="150" class="RightText">&nbsp;<%=strPerson%></td>
											<td width="100" class="RightText" align="center"><%=strDate%></td>
											<td width="350" class="RightText">&nbsp;<%=strComments%></td>
										</tr>
										<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
<%
							} // end of FOR	
						} else	{
%>
										<tr>
											<td height="30" colspan="4" align="center" class="RightTextBlue"><fmtConsignReprocess:message key="LBL_NO_CONSIGNMENT_DATA"/></td>
										</tr>
<%
					}
%>
										<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
									</table>
									</DIV>
								</td>
							</tr>	
						</table>
					</td>
				</tr>
				
				<tr>
					<td colspan="2">
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr><td colspan="4" class="Line" height="1"></td></tr>
							<tr class="ShadeRightTableCaption">
								<td HEIGHT="24" colspan="4"><fmtConsignReprocess:message key="LBL_RETURN_DETAILS"/></td>
							</tr>
							<tr><td colspan="4" class="Line" height="1"></td></tr>
							<tr bgcolor="#EEEEEE" class="RightTableCaption">
								<td HEIGHT="24" width="150"><fmtConsignReprocess:message key="LBL_RA_ID"/></td>
								<td width="150"><fmtConsignReprocess:message key="LBL_CREDITED_BY"/></td>
								<td width="100" align="center"><fmtConsignReprocess:message key="LBL_DATE"/></td>
								<td width="350">&nbsp;<fmtConsignReprocess:message key="LBL_COMMENTS"/></td>
							</tr>
							<tr>
								<td colspan="4">
									<DIV style="overflow:auto; height:100px">
									<table border="0" width="100%" cellspacing="0" cellpadding="0">
<%
						if (alReturn != null)
						{
							intSize = alReturn.size();
						}
						if (intSize > 0)
						{
			
							for (int i = 0;i < intSize ;i++ )
							{
								hmLoop = (HashMap)alReturn.get(i);
			
								strConId = GmCommonClass.parseNull((String)hmLoop.get("RAID"));
								strPerson = GmCommonClass.parseNull((String)hmLoop.get("UPDBY"));
								strComments = GmCommonClass.parseNull((String)hmLoop.get("COMMENTS"));
								strDate = GmCommonClass.parseNull((String)hmLoop.get("SDATE"));
%>
										<tr>
											<td width="150" height="18" class="RightText">&nbsp;<%=strConId%></td>
											<td width="150" class="RightText">&nbsp;<%=strPerson%></td>
											<td width="100" class="RightText" align="center"><%=strDate%></td>
											<td width="350" class="RightText">&nbsp;<%=strComments%></td>
										</tr>
										<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
<%
							} // end of FOR	
						} else	{
%>
										<tr>
											<td height="30" colspan="4" align="center" class="RightTextBlue"><fmtConsignReprocess:message key="LBL_NO_RETURNS_DATA"/></td>
										</tr>
<%
					}
%>
										<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
									</table>
									</DIV>
								</td>
							</tr>	
						</table>
					</td>
				</tr>					

				<tr>
					<td colspan="2">
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr><td colspan="3" class="Line" height="1"></td></tr>
							<tr class="ShadeRightTableCaption">
								<td HEIGHT="24" colspan="3"><fmtConsignReprocess:message key="LBL_REPROCESS_DETAILS"/></td>
							</tr>
							<tr><td colspan="3" class="Line" height="1"></td></tr>
							<tr bgcolor="#EEEEEE" class="RightTableCaption">
								<td HEIGHT="24" width="25">&nbsp;</td>
								<td width="100" ><fmtConsignReprocess:message key="LBL_SET_ID"/></td>
								<td width="550" ><fmtConsignReprocess:message key="LBL_SET_NAME"/></td>
							</tr>
							<tr>
								<td colspan="3">
									<DIV style="overflow:auto; height:100px">
									<table border="0" width="100%" cellspacing="0" cellpadding="0">
<%
						if (alSets != null)
						{
							intSize = alSets.size();
						}

						if (intSize > 0)
						{
							for (int k = 0;k < intSize ;k++ )
							{
								hmLoop = (HashMap)alSets.get(k);
								strSetId = (String)hmLoop.get("ID");
%>
										<tr>
											<td width="25"><input type="checkbox" name="Chk_SetId" id="<%=strSetId%>">
											<td width="100" height="18" class="RightText">&nbsp;<%=strSetId%></td>
											<td width="550" class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM((String)hmLoop.get("NAME"))%></td>
										</tr>
										<tr><td colspan="3" height="1" bgcolor="#cccccc"></td></tr>
<%
							} // end of FOR	
						} else	{
%>
										<tr>
											<td height="30" colspan="3" align="center" class="RightTextBlue"><fmtConsignReprocess:message key="LBL_NO_RETURNS_DATA"/></td>
										</tr>
<%
					}
%>
										<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
									</table>
									</DIV>
								</td>
							</tr>	
						</table>
					</td>
				</tr>				
			   <tr>
					<td colspan="2" align="Center" height="30">&nbsp;
					<fmtConsignReprocess:message key="BTN_INITIATE" var="varInitiate"/>
					<gmjsp:button value="${varInitiate}" gmClass="button" onClick="fnInitiate();" tabindex="16" buttonType="Save" />&nbsp;
					</td>
				</tr>
<%
	}
%>	
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>	
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
