<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtEmailTrackRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmEmailTrackReport.jsp -->
<fmtEmailTrackRpt:setLocale value="<%=strLocale%>"/>
<fmtEmailTrackRpt:setBundle basename="properties.labels.custservice.GmEmailTrackReport"/>

<bean:define id="strEmailMsg" name="frmEmailTrackRpt" property="strEmailMsg" type="java.lang.String"> </bean:define>
<script>
function fnPrint()
{
	window.print();
	return false;
}
</script>
<HTML>
<HEAD>
<TITLE>Globus Medical: Popup Email</TITLE>
</HEAD>
<BODY leftmargin="20" topmargin="20">
<FORM name="frmEmailTrackRpt" method="POST" action="/gmEmailTrackRpt.do?method=fetchEmailMessage">
<table>
<tr>
	<td align="right" height="30" id="button1" style="padding-left:800px">
		<img style="cursor:hand";style="text-align:left" src='<%=strImagePath%>/print-icon.png' height="25" width="25" alt="Click here to print page" onClick="return fnPrint();" />
		<img style="cursor:hand";style="text-align:left" src='<%=strImagePath%>/close_icon.png' height="20" width="20" alt="Click here to close window" onClick="window.close();" />
	</td>
</tr>
</table>
<table border="1" class="DtTable1000" cellspacing="0" cellpadding="0">
<tr>
	<td colspan="3" height="25" class="RightDashBoardHeader"><fmtEmailTrackRpt:message key="LBL_EMAIL_DETAILS"/></td>
</tr>
<tr><td bgcolor=#666666 colspan="3" height=1></td></tr>
<tr>
    <td colspan="9" >
        <div id="emailLogId"  height="500px" width="950px"><%=strEmailMsg%></div> 
	</td>
</tr>
</table>
<%@ include file="/common/GmFooter.inc" %>
</FORM>
</BODY>
</HTML>