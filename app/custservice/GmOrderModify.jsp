 <%
/**********************************************************************************
 * File		 		: GmOrderModify.jsp
 * Desc		 		: This screen is used for Editing order (Like Shipping
 *						Account, Sales Rep and Customer Information)
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ taglib prefix="fmtOrderModify" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmOrderModify.jsp -->
<fmtOrderModify:setLocale value="<%=strLocale%>"/>
<fmtOrderModify:setBundle basename="properties.labels.custservice.ModifyOrder.GmOrderModify"/>
<%
    String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strScreenName = "EditOrder";
	GmCalenderOperations gmCal = new GmCalenderOperations();
	String strWikiTitle = GmCommonClass.getWikiTitle("SALES_ORDER_EDIT");
	String strApplnDateFmt = strGCompDateFmt;
	String strCurrSymbol = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	String strAccCurrSymb ="";
	String strLotOverideAccess = GmCommonClass.parseNull((String)request.getAttribute("LOTOVERRIDEACCESS"));
	String strOrderEditAccess = GmCommonClass.parseNull((String)request.getAttribute("EDITORDACCESS"));
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strCurDate = gmCal.getCurrentDate(strGCompDateFmt);
	
	String strMode = (String)request.getAttribute("hMode")==null?"":(String)request.getAttribute("hMode");
	String strShipFl = GmCommonClass.parseZero((String)request.getAttribute("hShipFl"));
	String strChecked = "";
	strChecked = strShipFl.equals("2")?"checked":"0";
	String strDisabled = "";
	String strOrdStatus = "";
	String strMessages = (String)request.getAttribute("MESSAGES")==null?"":(String)request.getAttribute("MESSAGES");
	String strReadOnly="";
	String strCmtDisabled="";
	String strUpdFl = GmCommonClass.parseNull((String)request.getAttribute("QTYEDIT"));
	String strEPGSfl = GmCommonClass.parseNull((String)request.getAttribute("hstrEPGSfl")==null?"":(String)request.getAttribute("hstrEPGSfl"));  // PC-4892 
	if(!strUpdFl.equals("Y")){
		strReadOnly = "readonly=readonly";
	}else{
		strCmtDisabled = "disabled=disable";
	}
	// Company properties
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	strCountryCode = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("COUNTRYCODE"));
	HashMap hmComboValue = (HashMap)request.getAttribute("hmComboValue");
	String strShowUsage = GmCommonClass.parseNull((String) hmReturn.get("SHOW_USAGE"));
	String strTitle = "Control Number";
	if (strhAction.equals("EditShip"))
	{
		strTitle = "Shipping Details";
	}
	else if (strhAction.equals("EditPO"))
	{
		strTitle = "PO Details";
	}
	String strOrderId = "";
	String strAccName = "";
	String strUserName = "";
	String strReceiveMode = "";
	java.sql.Date dtSurgDate = null;
	String strComments = "";
	String strShade = "";
	String strItemQty = "";
	String strControl = "";
	String strControlFl = "";
	
	String strCodeID = "";
	String strSelected = "";
//	String strShipDate = "";
//	String strShipCarr = "";
//	String strShipMode = "";
//	String strTrack = "";
 	String strShipCost = "";
	String strPO = "";
	String strPerson = "";
	String strAccId = "";
	
	String strReason = "";
	String strCodeNm = "";
	String strDiscountDisabled = "";
	
	String strParentOrderId = "";
	String strInvoiceId = "";
	String strSalesRepId = "";
	String strOrdInvFl = "";
	String strOrdType = "";
	String strHardCpPO = "";
	String strHardPOChecked = "";
	String strTxtBoxDisabled = "disabled";
	String strRemQtyDisabled = "";
	String strBOTxtDisable = "";
    Date dtOrderDate	= null;
	int intOrderMonth = 0;
	int intOrderYear = 0;

	String strMsgFl = GmCommonClass.parseNull((String)request.getAttribute("hMsgFl"));
	String strTodaysDate = gmCal.getCurrentDate(strGCompDateFmt)==null?"":gmCal.getCurrentDate(strGCompDateFmt);
    Date dtTodaysDate = GmCommonClass.getCurrentDate(strGCompDateFmt, strGCompTimeZone );
	int intCurrentMonth = dtTodaysDate.getMonth();//getting current month
	int intCurrentYear = dtTodaysDate.getYear()+1900;//getting current year

	String strUserDept = (String)session.getAttribute("strSessDeptId")==null?"":(String)session.getAttribute("strSessDeptId");
	String strShowSurgAtr = GmCommonClass.parseNull( gmResourceBundleBean.getProperty("DO.SHOW_SURG_ATRB_DET"));
	String strOrderDtEditAcsFl = GmCommonClass.parseNull((String)request.getAttribute("ORDER_DATE_EDIT_ACS")); 
	String strEGPSUsage = GmCommonClass.parseNull((String)request.getAttribute("STRSURGERYTYPE"));
	
	ArrayList alCart = new ArrayList();
	HashMap hmOrderDetails = new HashMap();

	// Code Lookup value
	ArrayList alMode = new ArrayList();
	ArrayList alOrderMode = new ArrayList();
	ArrayList alShipTo = new ArrayList();
	ArrayList alAccount = new ArrayList();
	ArrayList alRepList = new ArrayList();
	ArrayList alLog = new ArrayList();
	ArrayList alQuoteDetails = new ArrayList();
	ArrayList alEGPSUsage = new ArrayList();
	ArrayList alShippingDetails = new ArrayList(); //PC-5584 - Exclude usage code or construct code from PSFG
	
	String strCollapseStyle = "display:block";
	String strCollapseImage =  strImagePath+"/minus.gif";
	String strDistName = "";
	String strBillTo = "";
	String strOrdredDate = "";
	int intPriceSize = 0;
	String strBeforeTotal = "";
	String strAfterTotal = "";
	String strLotOverrideFl = "";
	String strOvrideCurr = "";
	String strSurgeryDate = "";
	String strOrderDtHistFl="";
	String strOrderDtEditFl="";
	String strOrderEditDays = "";
	String strDisableOrdDt="";
	
	// To get the combo box value 
	if (hmComboValue != null)
	{
		alOrderMode = (ArrayList)hmComboValue.get("ORDERMODE");
		alAccount = (ArrayList)hmComboValue.get("ACCLIST");
	}
		
	if (hmReturn != null) 
	{
		hmOrderDetails 	= (HashMap)hmReturn.get("ORDERDETAILS");
		alQuoteDetails	= (ArrayList)hmReturn.get("QUOTEDETAILS");
		alShippingDetails	= (ArrayList)hmReturn.get("SHIPSUMMARY"); //PC-5584 - Exclude usage code or construct code from PSFG
		strOrderId 		= (String)hmOrderDetails.get("ID");
		strAccName 		= (String)hmOrderDetails.get("ANAME");
		strBillTo 		= (String)hmOrderDetails.get("ACCID");
		strReceiveMode	= (String)hmOrderDetails.get("RMODE");
		strPerson		= (String)hmOrderDetails.get("RFROM");
		strAccId 		= (String)hmOrderDetails.get("ACCID");
		strOrdStatus = GmCommonClass.parseZero((String)hmOrderDetails.get("STATUSFL"));
		
		strUserName 	= (String)hmOrderDetails.get("UNAME");
		dtSurgDate 		= (java.sql.Date)hmOrderDetails.get("ORD_SURG_DT");
		strOrdredDate = GmCommonClass.getStringFromDate((Date)hmOrderDetails.get("ODT"),strGCompDateFmt);
        dtOrderDate		= (Date)hmOrderDetails.get("ODT");
		intOrderMonth = dtOrderDate.getMonth();//getting order date's month
		intOrderYear = dtOrderDate.getYear() +1900;//getting order date's year
		strComments 	= (String)hmOrderDetails.get("COMMENTS");
		strPO 			= (String)hmOrderDetails.get("PO");
		strParentOrderId = (String)hmOrderDetails.get("PARENTORDID");
		strInvoiceId     =  GmCommonClass.parseNull((String)hmOrderDetails.get("INVOICEID"));
		alCart 			= (ArrayList)hmReturn.get("CARTDETAILS");
		strSalesRepId  = (String)hmOrderDetails.get("REPID");
		strOrdInvFl = (String)hmOrderDetails.get("INVFL");
		strOrdType = (String)hmOrderDetails.get("ORDERTYPE");
		
		strHardCpPO = (String)hmOrderDetails.get("HARDPOFL");
		strShipCost = GmCommonClass.parseNull((String)hmOrderDetails.get("SHIPCOST"));
		strHardPOChecked = strHardCpPO.equals("Y")?"checked":"";
		strLotOverrideFl = GmCommonClass.parseNull((String)hmOrderDetails.get("LOTOVERRIDEFL"));
		strLotOverrideFl = strLotOverrideFl.equals("Yes") ? strLotOverrideFl : "No";
		strAccCurrSymb = GmCommonClass.parseNull((String)hmOrderDetails.get("ACC_CURR_SYMB"));
		strOvrideCurr = GmCommonClass.parseNull((String)hmOrderDetails.get("OVRIDE_CURR"));
		strCurrSymbol = strAccCurrSymb.equals("")?strCurrSymbol:strAccCurrSymb; 
		strCurrSymbol = !strOvrideCurr.equals("")? strOvrideCurr: strCurrSymbol;
		strOrderDtHistFl = GmCommonClass.parseNull((String)hmOrderDetails.get("ORD_DT_HIST_FL")); 
		strOrderDtEditFl = GmCommonClass.parseNull((String)hmOrderDetails.get("ORD_DATE_EDIT_FLG"));
		strOrderEditDays = GmCommonClass.parseNull((String)hmOrderDetails.get("ORD_DATE_EDIT_DAYS"));
		
		alEGPSUsage = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("EGPSUSAGE"));
		if(!strParentOrderId.equals("")){
			strEPGSfl = "N";
		}
		//if strOrderEditDays value is null, then get the value from Company property
		if(strOrderEditDays.equals("")){
		  strOrderEditDays = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("ORDER_DATE_EDIT_DAYS"));
		}
		 if(strOrderDtEditFl.equals("N") || !strOrderDtEditAcsFl.equals("Y") || !strInvoiceId.equals("")){ 
		   strDisableOrdDt = "disabled";
		 } 
		 
		// The following code is for the  control number JSP included at the bottom.
		
		String strFormName = GmCommonClass.parseNull(request.getParameter("FRMNAME"));
		ArrayList alControlNumDetails = new ArrayList();
		
		if(strFormName.equals("frmOrder"))
		{
			alControlNumDetails =GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALCONTROLNUMDETAILS"));
		}
		else
		{
			alControlNumDetails =GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALFILTEREDCNUMDETAILS"));
		}
		HashMap hmControlNumDetails =new HashMap();
		String strControlNum="";
		for (int i=0;i<alControlNumDetails.size();i++)
		{
			hmControlNumDetails = (HashMap)alControlNumDetails.get(i);
			strControlNum = GmCommonClass.parseNull((String)hmControlNumDetails.get("CNUM"));
			if(!strControlNum.equals("")){
				strControlNum ="CTRL";
				break;
			}
		}
		if(alControlNumDetails.size()==0 || strControlNum.equals("")){
			strCollapseStyle = "display:none";
			strCollapseImage =  strImagePath+"/plus.gif";
		}
		request.setAttribute("alControlNumberDetails", alControlNumDetails);
		
		// J - A/R department
		if (!strInvoiceId.equals("") && !strUserDept.equals("J"))
		{
			strDisabled = "true";
		}
		   

		//C for Customer Service or those who are mapped to the security group
		if (strUserDept.equals("C") || strOrderEditAccess.equals("Y")) 
		{
			strTxtBoxDisabled = "";
			if (!strOrdInvFl.equals(""))
			{
				strTxtBoxDisabled = "disabled";
			}
		}
		//check whether the order date is current month or not, if not disable the Remove Qty field(PMT-42209)
		if (intCurrentYear != intOrderYear || intCurrentMonth != intOrderMonth)
			{
			strRemQtyDisabled = "disabled";
			}

		log.debug(" Value of Ord Statuis " + strOrdStatus + " Val of Disable " +strDisabled );
		
		// To get the user Log information 
		alLog = (ArrayList)request.getAttribute("hmLog");
		
	}
	
				log.debug(" Value of Ord Statuis " + strOrdStatus + " Val of Disable " +strDisabled );
				
	int intSize = 0;
	HashMap hcboVal = null;

	HashMap hmShip = new HashMap();
	ArrayList alCarrier = new ArrayList();

	hmShip = (HashMap)request.getAttribute("hmShipLoad");
	String strShipTo = "";
	String strShipToId = "";

	if (hmShip != null)
	{
		alCarrier = (ArrayList)hmShip.get("CARRIER");
		alMode = (ArrayList)hmShip.get("MODE");
		alShipTo = (ArrayList)hmShip.get("SHIPLIST");
		alRepList = (ArrayList)hmShip.get("REPLIST");
		strShipTo = (String)hmOrderDetails.get("SHIPTO");
		strShipToId = (String)hmOrderDetails.get("SHIPTOID");
	}
	//to set the date range for dhtmlx calendar based on the edit order days for the editable Order date field
	String strToOrdDate = gmCal.getCurrentDate("yyyy-MM-dd");
	String strFromOrdDate = gmCal.addDays((Integer.parseInt("-"+strOrderEditDays))+1, "yyyy-MM-dd");
			
	intSize = alShippingDetails.size();
	HashMap hShippingVal = new HashMap();
	String strStatusFl ="";
	String strStatusFltemp ="";
	for (int i=0;i<intSize;i++)
	{
		hShippingVal = (HashMap)alShippingDetails.get(i);
		strStatusFltemp = (String)hShippingVal.get("SSTATUSFL");
		if(strStatusFltemp == "30"){
			strStatusFl = strStatusFltemp;
		}
		if(!strStatusFl.equals("30")){
			strStatusFl = strStatusFltemp;
		}
		
	}//PC-5584 - Exclude usage code or construct code from PSFG

 %>

<%@page import="java.util.Date"%><HTML>
<HEAD>
<TITLE> Globus Medical: Enter Control Numbers </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/prodmgmnt/GmRule.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmOrderModify.js"></script>
<script>
var lotOverrideAccess = '<%=strLotOverideAccess%>';
var lotOverRideFl = '<%=strLotOverrideFl%>'
var splitAction = 0;//1-void order, 2- if order has single part then order move to back Order
var dateFmt = '<%=strApplnDateFmt%>';
var dept = '<%=strUserDept%>';
var servletPath = '<%=strServletPath%>';
var ShipToId = '<%=strShipToId%>';
var usedLotFl = '<%=strShowUsage%>';
var todaysDate = '<%=strCurDate%>';
var orderEditDays = '<%=strOrderEditDays%>';
var hOrdDate = '<%=strOrdredDate %>';
var vcountryCode = '<%=strCountryCode%>';
var invoiceId  = '<%=strInvoiceId%>';
var epgsFl =  '<%=strEPGSfl%>';

var lblSurgeryDt = '<fmtOrderModify:message key="LBL_SURGERY_DATE"/>';
var lblOrderDt = '<fmtOrderModify:message key="LBL_ORDER_DATE"/>';
</script>


</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnLoadBtn()">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmOrderEditServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hOrdId" value="<%=strOrderId%>">
<input type="hidden" name="hAccName" value="<%=strAccName%>">
<input type="hidden" name="Txt_PO" value="<%=strPO%>">
<input type="hidden" name="hSalesRepId" value="<%=strSalesRepId%>">
<input type="hidden" name="hOrdInvFl" value="<%=strOrdInvFl%>">
<input type="hidden" name="hOrdType" value="<%=strOrdType%>">
<input type="hidden" name="hRmQtyStr" value="">
<input type="hidden" name="hBOQtyStr" value="">
<input type="hidden" name="hCnumStr" value="">
<input type="hidden" name="hQuoteStr" value="">
<input type="hidden" name="hUsedLotStr" value="">



	<table border="0" class="DtTable950" cellspacing="0" cellpadding="0">
		<!-- <tr><td bgcolor="#666666" height="1" colspan="3"></td></tr> -->
		<tr>
		<!-- 	<td bgcolor="#666666" width="1" rowspan="4"></td> -->
			<td>
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td height="25" class="RightDashBoardHeader" colspan="5"><fmtOrderModify:message key="LBL_HEADER"/></td>
					<td align="right" class=RightDashBoardHeader >
					    <fmtOrderModify:message key="IMG_HELP" var = "varHelp"/>
						<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' 
						onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />  
					</td>
				</tr>
			</table>
			</td>
		<!-- 	<td bgcolor="#666666" width="1" rowspan="4"></td> -->
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="5"></td></tr>
		<tr>
			<td height="100" valign="top">
			
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<% 
				if (!strMessages.equals("")){
			%>
					<tr><td colspan="5">
						<font color="red"> <%=strMessages%>	</font>
					</td><tr>
			<%}%>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="shade">
						<fmtOrderModify:message key="LBL_ORDERID" var = "varOrderId"/>
						<td class="RightTableCaption" HEIGHT="20" align="right" ><gmjsp:label type="RegularText"  SFLblControlName="${varOrderId}:" td="false"/>&nbsp;</td>
						<td class="RightText" Colspan="4"><b><%=strOrderId%></b></td>
					</tr>
					<tr><td colspan="5" height="1" bgcolor="#eeeeee"></td></tr>
					<tr>
						<fmtOrderModify:message key="LBL_ORDERENTEREDBY" var = "varOrderEntered"/>
						<td class="RightTableCaption" HEIGHT="20" align="right" ><gmjsp:label type="RegularText"  SFLblControlName="${varOrderEntered}:" td="false"/>&nbsp;</td>
						<td class="RightTableCaption"><b><%=strUserName%></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<fmtOrderModify:message key="LBL_ORDER_DATE" var = "varDate"/>
						<td class="RightTableCaption" align="right">
						<gmjsp:label type="RegularText"  SFLblControlName="${varDate} :" td="false"/>&nbsp;</td>
						<td class="RightTableCaption">
						<input type="text" <%=strDisableOrdDt%> size="10" value="<%=strOrdredDate%>" name="Txt_OrderDate" id="Txt_OrderDate" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');fnDateFmtOnBlur(this,'<%=strApplnDateFmt%>')" tabindex=1>
						<img id="Img_Date" <%=strDisableOrdDt%> style="cursor:hand" onclick="javascript:showSingleCalendarDisableDays('divTxt_Date','Txt_OrderDate','','<%=strFromOrdDate%>','<%=strToOrdDate%>');" title="calendar"   src="<%=strImagePath%>/nav_calendar.gif" border=0  align="absmiddle" height=18 width=19 />
						<div id="divTxt_Date" style="position: absolute; z-index: 10;"></div>
						 <%if(strOrderDtHistFl.equals("Y")){ %> 
						&nbsp;<img  id="imgEdit" style="cursor:hand" onclick="javascript:fnLoadHistory ('304','<%=strOrderId%>');" title="View Order Date Modifications"  src="<%=strImagePath%>/icon_History.gif" align="bottom"  height=15 width=18 />
						 <%} %> </td>
						 <td class="RightTableCaption" align="left">
						<fmtOrderModify:message key="LBL_SURGERY_DATE" var = "varSurgeryDate"/>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varSurgeryDate} :" td="false"/>&nbsp;
						<gmjsp:calendar textControlName="Txt_SurgDate" textValue="<%=dtSurgDate%>"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
						</td>
					</tr>
					<tr><td colspan="5" height="1" bgcolor="#eeeeee"></td></tr>
<% // ********* Bill To Information ************** //  %>					
<% log.debug(" Vlaue of Disabled is " +strDisabled); %>
					<tr class="shade">
						<fmtOrderModify:message key="LBL_BILLTO" var = "varBillTo"/>
						<td class="RightTableCaption"  HEIGHT="23" align="right"><gmjsp:label type="RegularText"  SFLblControlName="${varBillTo}:" td="false"/>&nbsp;</td>
						<td class="RightTableCaption" Colspan="5">
							<select name="Cbo_BillTo" id="Region" class="RightText" tabindex="1" onChange="fnSetAccIdValue(this.value);" >
							<option value="0" >[Choose One]
<%			  				intSize = alAccount.size();
							hcboVal = new HashMap();
							String strRepId = "";
			  				for (int i=0;i<intSize;i++)
			  				{
					  			//strAccID
					  			hcboVal = (HashMap)alAccount.get(i);
					  			strCodeID = (String)hcboVal.get("ID");
					  			strRepId = (String)hcboVal.get("REPID");
					  			strDistName = (String)hcboVal.get("DNAME");
								strSelected = strBillTo.equals(strCodeID)?"selected":"";
%>								<option <%=strSelected%> value="<%=strCodeID%>" id="<%=strRepId%>"><%=hcboVal.get("NAME")%> / <%=strDistName%></option>
<%			  		}%> </select>
						<fmtOrderModify:message key="LBL_ORACCOUNTID" var = "varOrAccountId"/>
						<br><gmjsp:label type="RegularText"  SFLblControlName="${varOrAccountId}:" td="false"/> <input type="text" size="6"  value="<%=strAccId%>" 
						name="Txt_AccId" class="InputArea" onBlur="fnSetCboVal(this.value);" tabindex=-1> </td></tr>
					<tr><td colspan="5" height="1" bgcolor="#eeeeee"></td></tr>
<% // ********* Order Mode Information ************** //%>										
					<tr>
					    <fmtOrderModify:message key="LBL_MODEOFORDER" var = "varModeOfOrder"/>
					    <fmtOrderModify:message key="LBL_PERSONNAME" var="varPersonNAme"/>
						<td class="RightTableCaption" HEIGHT="23" align="right" ><gmjsp:label type="RegularText"  SFLblControlName="${varModeOfOrder}:" td="false"/>&nbsp;</td>
						<td class="RightTableCaption" Colspan="4"><select name="Cbo_Mode" id="Region" class="RightText" tabindex="2" 
						onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" 
						onChange="javascript:fnCallPerson();"><option value="0" >[Choose One]
<%			  		intSize = alOrderMode.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alOrderMode.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strReceiveMode.equals(strCodeID)?"selected":"";
%>							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%			  		}%>							
					</select> &nbsp;&nbsp;&nbsp;&nbsp;<input class="RightTableCaption" type="checkbox" name="Chk_HardPO" <%=strHardPOChecked %>/>&nbsp;<fmtOrderModify:message key="LBL_HARDCOPYPO"/>&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varPersonNAme}" td="false"/> &nbsp;
					<input type="text" size="20" disabled value="<%=strPerson%>"  
					name="Txt_PNm" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
					onBlur="changeBgColor(this,'#ffffff');" tabindex=3></td>
					</tr>
					<tr><td colspan="5" height="1" bgcolor="#eeeeee"></td></tr>
					<tr class="shade">
						
						<td class="RightTableCaption" HEIGHT="23" align="right"><fmtOrderModify:message key="LBL_SALESREP"/>:&nbsp;</td>
						<td class="RightText"  Colspan="4">
						<fmtOrderModify:message key="LBL_CHOOSEONE" var = "varChooseOne"/>
							<gmjsp:dropdown controlName="Cbo_SalseRepID" seletedValue="<%=strSalesRepId%>" value="<%=alRepList%>" codeId="ID" codeName="NAME" defaultValue="${varChooseOne}" />
						</td>
					</tr>
					<tr><td colspan="5" height="1" bgcolor="#eeeeee"></td></tr>
<% // ********* Ship To Information ************** //%>															
 		
<% // ********* Order Comments Information ************** //%>																				
					<tr>
						<fmtOrderModify:message key="LBL_COMMENTS" var="varComments"/>
						<td class="RightTableCaption" valign="top" align="right" HEIGHT="60"><gmjsp:label type="RegularText"  SFLblControlName="${varComments}:" td="false"/>&nbsp;</td>
						<td  Colspan="4"><textarea name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
						onBlur="changeBgColor(this,'#ffffff');" tabindex=6 rows=5 cols=100 
						value="<%=strComments%>"><%=strComments%></textarea></td>
					</tr>
					<tr><td colspan="5" height="1" bgcolor="#eeeeee"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" colspan="5" >
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<fmtOrderModify:message key="LBL_CUSTOMERPO" var = "varCustPO"/>
									<td  class="RightTableCaption" width="15%" align="right"><gmjsp:label type="RegularText"  SFLblControlName="${varCustPO}:" td="false"/>&nbsp;</td>
									<td class="RightText" HEIGHT="23" align="left" width="35%"><%=strPO%> </td>
									<fmtOrderModify:message key="LBL_OVERRIDELOTREQUIREMENT" var = "varOverridelotreq"/>
									<td align="right" HEIGHT="24" class="RightTableCaption"  width="20%"><gmjsp:label type="RegularText"  SFLblControlName="${varOverridelotreq}:" td="false"/>&nbsp;</td>
									<td class="RightText" HEIGHT="23" align="left"><%=strLotOverrideFl%> </td>	
								</tr>
							</table>
						</td>
					</tr>
					
					<!-- Parent Order Id  -->
					<tr><td colspan="5" height="1" bgcolor="#eeeeee"></td></tr>
					
					<tr>
						<fmtOrderModify:message key="LBL_PARENTORDERID" var = "varParentOrderID"/>
						<td align="right" HEIGHT="24" class="RightTableCaption" ><gmjsp:label type="RegularText"  SFLblControlName="${varParentOrderID}:" td="false"/>&nbsp;</td>
						<td class="RightText" HEIGHT="23" align="left">
						<input type="text" size="20" maxlength="20"   value="<%=strParentOrderId%>" 
						name="Txt_ParentOrdId" class="InputArea">
						</td>
						
						<td class="RightTableCaption" HEIGHT="24" align="right" width="20%"><fmtOrderModify:message key="LBL_CAPITAL_EQUP_USED"/>:</td>
						<td>
						<fmtOrderModify:message key="LBL_CHOOSEONE" var = "varChooseOne"/>
					    <gmjsp:dropdown controlName="Cbo_egpsusage" seletedValue="<%=strEGPSUsage%>" value="<%=alEGPSUsage%>" codeId="CODEID" codeName="CODENM" defaultValue="${varChooseOne}" />
					    </td>
					</tr>
					
					<!--  Invoice Id -->
					<tr><td colspan="5" height="1" bgcolor="#eeeeee"></td></tr>
					
					<tr class="shade">
					    <fmtOrderModify:message key="LBL_INVOICEID" var = "varInvoiceId"/>
						<td align="right" HEIGHT="24" class="RightTableCaption" ><gmjsp:label type="RegularText"  SFLblControlName="${varInvoiceId}" td="false"/>&nbsp;</td>
						<td class="RightText" HEIGHT="23" align="left"  Colspan="4">
						<input type="text" size="20" maxlength="20"   value="<%=strInvoiceId%>" 
						name="Txt_InvoiceId" class="InputArea">						 
						</td>
					</tr>
					<%if(strMsgFl.equals("Y")){ %>
					<tr><td colspan="5" height="1" bgcolor="#eeeeee"></td></tr>
					<tr><td colspan="5" valign="middle"><font color="red"><b><fmtOrderModify:message key="LBL_NOTE"/></b> </font></td></tr>
					<%} %>
					<% if(!strOrdType.equals("101260") && strShowSurgAtr.equals("YES")){ %>
				<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
				<%-- Code added by hreddi for showing the Quote Details to Edut --%>
					<tr class="ShadeRightTableCaption"><td height="22" colspan="5" ><fmtOrderModify:message key="LBL_SURGERYDETAILS"/></td></tr>
					<TR><TD colspan=12 height=1 class=Line></TD></TR>
					
				<td colspan="5" width="948">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">		
				<tr>
			<%
				intSize = alQuoteDetails.size();
				hcboVal = new HashMap();
				for (int i = 0; i < intSize; i++) 
				{			
					hcboVal = (HashMap) alQuoteDetails.get(i);
					String strOrdID = (String) hcboVal.get("OAID");
					strCodeID = (String) hcboVal.get("TRTYPEID");
					strCodeNm = (String) hcboVal.get("TRTYPE");
					String strCodeValue = (String) hcboVal.get("ATTRVAL");					
					
			%>							
							<td class="RightTableCaption" HEIGHT="24"  align="right"><%=strCodeNm%> :</td>
							<td><input type="hidden" id="attrId<%=i%>" value="<%=strOrdID%>" ></input></td>
							<td  class="RightText" HEIGHT="24" >&nbsp;
						<% if(strCodeNm.equals("Discount %")){strDiscountDisabled ="disabled";%>
						<%}else{strDiscountDisabled ="enabled";} %>
							<input type="text" size="30" value="<%=strCodeValue%>" name="Txt_Quote<%=i%>" class="InputArea" id="<%=strCodeID %>" <%=strDiscountDisabled%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=9/></td>
							
			<%
					if (i%2 == 1)
					{
			%>									
						</tr>
						<tr><td bgcolor="#CCCCCC" height="1" colspan="9"></td></tr>
						<tr>
			<%
					}
				}
			%>	
						</tr>
					</table>
					</td>
					<td><input type="hidden" name="hQuoteCnt" value="<%=intSize %>">	</td></tr>					
		<%} %>
<% // To Display Part Number Information %>
					<tr>
						<td colspan="5" width="100%">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr ><td colspan="12" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption"><td height="22" colspan="12">&nbsp;<fmtOrderModify:message key="LBL_PARTNUMDETAILS"/></td></tr>
								<tr ><td colspan="12" height="1" bgcolor="#666666"></td></tr>
								<tr class="Shade" class="RightTableCaption" style="position:relative;">
									<%-- <td width="30" height="25">&nbsp;Void</td>--%>
									<TH class="RightText" width="70" height="25">&nbsp;<fmtOrderModify:message key="LBL_PART"/></TH>
									<TH class="RightText" align="left" width="300"><fmtOrderModify:message key="LBL_PARTDESCRIPTION"/></TH>
									<TH class="RightText" width="40">&nbsp;<fmtOrderModify:message key="LBL_TYPE"/></TH>
									<TH class="RightText" width="40">&nbsp;<fmtOrderModify:message key="LBL_ORDERQTY"/></TH>
									<TH class="RightText" width="40">&nbsp;<fmtOrderModify:message key="LBL_RFGQTY"/></TH>
									<TH class="RightText" width="60">&nbsp;<fmtOrderModify:message key="LBL_REMQTY"/></TH>
									<TH class="RightText" width="40">&nbsp;<fmtOrderModify:message key="LBL_BOQTY"/></TH>
									<TH class="RightText" width="40">&nbsp;<fmtOrderModify:message key="LBL_UNITPRICE"/></TH>
									<TH class="RightText" width="40">&nbsp;<fmtOrderModify:message key="LBL_UNITPRICEADJ"/></TH>
									<TH class="RightText" width="40">&nbsp;<fmtOrderModify:message key="LBL_ADJCODE"/></TH>
									<TH class="RightText" width="60" align="right"><fmtOrderModify:message key="LBL_NETUNITPRICE"/></TH>
									<TH class="RightText" width="60" align="right"><fmtOrderModify:message key="LBL_TOTALPRICE"/></TH>
								</tr>
								<TR><TD colspan=12 height=1 class=Line></TD></TR>
<%
						intSize = alCart.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();

							String strNextPartNum = "";
							int intCount = 0;
							String strColor = "";
							String strLine = "";
							String strPartNumHidden = "";
							String strPartDesc = "";
							String strPrice = "";
							String strTemp = "";
							String strPartNum = "";
							String strPartType = "";
							String strType = "";
							String strItemOrdId = "";
							String strPartString = "";
							String strPendShipQty = "";
							String strUnitPrice = "";
							String strUnitPriceAdj = "";
							String strAdjCode = "";
							String strNetUnitPrice = "";
							String strProductFamily = "";
							double intQty = 0.0;
							double intPreQty = 0.0;
							double dbItemTotal = 0.0;
							double dbTotal = 0.0;
							double dbBeforeItemTotal = 0.0;
							double dbBeforePriceTotal = 0.0;
							double dbAfterItemTotal = 0.0;
							double dbAfterPriceTotal = 0.0;
							double dbAdjTotal = 0.0;
							double dbAdjstTotal = 0.0;
							
							String strItemTotal = "";
							String strBeforeItemTotal = "";
							String strAfterItemTotal = "";
							String strAdjItemTotal = "";
							String strTotal = "";
							String strAdjTotal = "";
							double dblShip = 0.0;
							double dbBeforeTotal = 0.0;
							double dbAfterTotal = 0.0;
							String strAfterPrice = "";

							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alCart.get(i);
								if (i<intSize-1)
								{
									hmTempLoop = (HashMap)alCart.get(i+1);
									strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("ID"));
								}
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("ID"));
								strPartNumHidden = strPartNum;
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
								strPartType = GmCommonClass.parseNull((String)hmLoop.get("ITEMTYPEDESC"));
								strType = GmCommonClass.parseNull((String)hmLoop.get("ITEMTYPE"));
								strItemOrdId = GmCommonClass.parseNull((String)hmLoop.get("IORDID"));
								strPendShipQty = (String)hmLoop.get("PENDSHIPQTY");
								strPendShipQty = strPendShipQty.equals("0")?"-":strPendShipQty;
								strTemp = strPartNum;
								strUnitPrice = GmCommonClass.parseZero((String)(String)hmLoop.get("UNITPRICE"));
								strUnitPriceAdj = GmCommonClass.parseZero((String) (String)hmLoop.get("UNITPRICEADJ"));
								strAdjCode = GmCommonClass.parseNull((String)(String)hmLoop.get("UNITPRICEADJCODE"));
								strNetUnitPrice = GmCommonClass.parseZero((String)(String)hmLoop.get("NETUNITPRICE"));

								strItemQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								strControlFl = strControl.equals("")?"":"Y";
								System.out.println("====intPreQty===="+intPreQty);
								intQty = Double.parseDouble(strItemQty)+ intPreQty;
								strItemQty = ""+intQty;
								strProductFamily = GmCommonClass.parseZero((String)(String)hmLoop.get("PRODFAMILY")); //PC-5584 - Exclude usage code or construct code from PSFG
								
								
								if (strPartNum.equals(strNextPartNum))
								{
									// Should not group the parts 
								/*	if((i<intSize-1) && strControlFl.equals("Y")){
										intPreQty = intQty;
										continue;
									} */
									intCount++;
									strLine = "<TR><TD colspan=12 height=1 bgcolor=#eeeeee></TD></TR>";
								}

								else
								{
									strLine = "<TR><TD colspan=12 height=1 bgcolor=#eeeeee></TD></TR>";
									if (intCount > 0)
									{
										strPartNum = "";
										strPartDesc = "";
										//strColor = "bgcolor=white";
										strLine = "";
									}
									else
									{
										//strColor = "";
									}
									intCount = 0;
									intPreQty =0;
								}

								if (intCount > 1)
								{
									strPartNum = "";
									strPartDesc = "";
									//strColor = "bgcolor=white";
									strLine = "";
								}

								if(strType.equals("50301") || strOrdType.equals("102080")){ //50301: Loaner part; BO Qty text box is should be disabled for loaner parts 
								  	strBOTxtDisable = "disabled";
								}else{
								  	strBOTxtDisable = "";
								}
							
								if((strProductFamily.equals("26240399") || strProductFamily.equals("26240096")) && !strStatusFl.equals("15")){ //26240399 - Construct part , 26240096 - Usage code , 15(Pending Control) - Shipping table status ID 
									strBOTxtDisable = "disabled";
									strRemQtyDisabled ="disabled";
									strTxtBoxDisabled = "";
									
								}
								//PC-5584 - Exclude usage code or construct code from PSFG

								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
								//strItemQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
								//strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								//strControlFl = strControl.equals("")?"":"Y";
								//intQty = Integer.parseInt(strItemQty);
								if(strNetUnitPrice.equals("0")){
									strNetUnitPrice = strUnitPrice;
								}
								dbItemTotal = Double.parseDouble(strNetUnitPrice);
								dbItemTotal = intQty * dbItemTotal; // Multiply by Qty
								strItemTotal = ""+dbItemTotal;
								dbTotal = dbTotal + dbItemTotal;
								
								// Calculating the Total Price Before Adjustment
								dbBeforeItemTotal = Double.parseDouble(strUnitPrice);
								dbBeforeItemTotal = intQty * dbBeforeItemTotal; // Multiply by Qty
								strBeforeItemTotal = ""+dbBeforeItemTotal;
								dbBeforePriceTotal = dbBeforePriceTotal + dbBeforeItemTotal;
								strBeforeTotal = "" + 	dbBeforePriceTotal;	
								
								// Calculating the Total Price After Adjustment
								dbAfterItemTotal = Double.parseDouble(strPrice);
								dbAfterItemTotal = intQty * dbAfterItemTotal; // Multiply by Qty
								strAfterItemTotal = ""+dbAfterItemTotal;
								dbAfterPriceTotal = dbAfterPriceTotal + dbAfterItemTotal;
								strAfterPrice = "" + 	dbAfterPriceTotal;																				
								
								//strTotal = ""+dbTotal;
								strPartString = strItemQty+"^"+strItemOrdId+"^"+strPartNumHidden+"^"+strType+"^"+strPrice;
								out.print(strLine);
%>
								<tr>
								    
									<input type="hidden" name="hPartDt<%=i%>" value="<%=strPartString%>">
									<input type="hidden" name="hcNum<%=i%>" value="<%=strControl%>"/>
									<input type="hidden" name="hpNum<%=i%>" value="<%=strPartNumHidden%>"/>
									<input type="hidden" name="hpendShipQty<%=i%>" value="<%=strPendShipQty%>"/>
									<%-- <td align="center"><input type="checkbox" name="chk_VdPart<%=i%>" value=""/></td>--%>
									<td class="RightText" height="20" width="70">&nbsp;<%=strPartNum%></td>
									<td class="RightText" width="300">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td width="40" align="center">&nbsp;<%=strPartType%></td>
									<td width="40" class="RightText" align="center"><%=strItemQty%></td>
									<td class="RightText" align="center" width="40"><%=strPendShipQty%></td>
									<td align="center"><input type="text" size="3" width="60" maxlength="4" <%=strRemQtyDisabled%> <%=strTxtBoxDisabled%> <%=strReadOnly%> value="" name="Txt_RmQty<%=i%>" class="InputArea" onkeypress="return fnNumbersOnly(event)"></td>
									<td align="left">&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" size="3" width="40" maxlength="4" <%=strBOTxtDisable%> <%=strTxtBoxDisabled%> <%=strReadOnly%> value="" name="Txt_BoQty<%=i%>" class="InputArea" onkeypress="return fnNumbersOnly(event)"onblur="javascript:fnCheckProdFmly('<%=strPartNum%>', this,'<%=i%>');">					
									&nbsp;<span id="DivShowUsageCode<%=i%>" style=" vertical-align:middle; display: none;"><img title="Cannot back order Usage Code parts " src="<%=strImagePath%>/delete.gif"></img></span></td>
									<gmjsp:currency currSymbol="<%=strCurrSymbol%>" width="40" type="CurrTextSign"  textValue="<%=strUnitPrice%>"/>
									<gmjsp:currency currSymbol="<%=strCurrSymbol%>" width="40" type="CurrTextSign"  textValue="<%=strUnitPriceAdj%>"/>	
									<td class="RightText" align="center" width="40"><%=strAdjCode%></td>		
									<gmjsp:currency currSymbol="<%=strCurrSymbol%>" width="60" type="CurrTextSign"  textValue="<%=strNetUnitPrice%>"/>
									<gmjsp:currency currSymbol="<%=strCurrSymbol%>" width="60" type="CurrTextSign"  textValue="<%=strItemTotal%>"/>
								</tr>
<%
							}//End of FOR Loop
								strShipCost = GmCommonClass.parseZero(strShipCost);
								dblShip = Double.parseDouble(strShipCost);
								dbBeforeTotal = Double.parseDouble(strBeforeTotal);
								dbTotal = dbBeforeTotal ;
								strBeforeTotal = ""+dbTotal;
								
								dbAfterTotal = Double.parseDouble(strAfterPrice);
								dblShip = Double.parseDouble(strShipCost);
								dbAdjstTotal = dbAfterTotal + dblShip;
								strAfterTotal = ""+dbAdjstTotal;
								
%>
								<tr><td colspan="12" height="1" bgcolor="#eeeeee"></td></tr>
								<tr class="shade">
									<!-- <td colspan="1">&nbsp;</td> -->
									<td class="RightTableCaption" height="20" colspan="11">&nbsp;<fmtOrderModify:message key="LBL_SHIPPINGCHARGES"/></td>
									<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strShipCost)%>&nbsp;</td>
								</tr>
								<tr><td colspan="12" height="1" bgcolor="#eeeeee"></td></tr>
								<tr>
									<td colspan="11" height="30" align="right" class="RightTableCaption"><fmtOrderModify:message key="LBL_TOTALBEFOREADJ"/>&nbsp;&nbsp;</td>
									<td><gmjsp:currency currSymbol="<%=strCurrSymbol%>" type="CurrTextSign"  textValue="<%=strBeforeTotal%>" td="false"/><input type="hidden" name="hOrderAmt" value="<%=strBeforeTotal%>"></td>
								</tr>
								<tr>
									<td colspan="11" height="30" align="right" class="RightTableCaption"><fmtOrderModify:message key="LBL_TOTALAFTERADJ"/>&nbsp;&nbsp;</td>
									<td><gmjsp:currency currSymbol="<%=strCurrSymbol%>" type="CurrTextSign"  textValue="<%=strAfterTotal%>" td="false"/><input type="hidden" name="hOrderAdjAmt" value="<%=strAfterTotal%>"></td>
								</tr>
<%
							if (strControlFl.equals("Y"))
							{
%>
								<tr><td colspan="12" height="1" bgcolor="#eeeeee"></td></tr>
								<tr><td colspan="12" align="center" class="RightTextRed"><fmtOrderModify:message key="LBL_PARTSCONTROL"/></td></tr>
<% 								
							}
						}else {
%>
						<tr><td colspan="12" height="50" align="center" class="RightTextRed"><BR><fmtOrderModify:message key="LBL_NOPARTINORDER"/></td></tr>
<%
						}		
%>
							</table>
							<input type="hidden" name="hCnt" value="<%=intSize-1%>">
						</td>
					</tr>
<%
	// End Of Part Number Information
%>					
				<% if(!strShowUsage.equals("Y")){ %>
					<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
					<tr class="ShadeRightTableCaption"><td height="22" colspan="5"><a
			href="javascript:fnShowFilters('trEditControlNum');"><IMG
			id="trEditControlNumimg" border=0 src="<%=strCollapseImage%>"></a>&nbsp;<fmtOrderModify:message key="LBL_CONTROLNUMDETAILS"/></td></tr>
				<!-- 	<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr> -->
					<tr id="trEditControlNum" style="<%=strCollapseStyle%>">
						<td colspan="5">
							<jsp:include page="/custservice/GmControlNumberCartInclude.jsp"/>
						</td>
					</tr>
					<%} else{ %>
						<tr class="ShadeRightTableCaption">
							<td height="22" colspan="5"><a
								href="javascript:fnShowFilters('trEditControlNum');"><IMG
									id="trEditControlNumimg" border=0 src="<%=strCollapseImage%>"></a>&nbsp;Used
								Lot Number</td>
						</tr>
						<tr id="trEditControlNum" style="<%=strCollapseStyle%>">
							<td colspan="5"><jsp:include
									page="/custservice/GmUsedLotNumberInclude.jsp">
									<jsp:param name="InvoiceId" value="<%=strInvoiceId%>" />
									</jsp:include>
									</td>
						</tr>
						<%} %>
					<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
					<tr class="ShadeRightTableCaption"><td height="22" colspan="5">&nbsp;<fmtOrderModify:message key="LBL_REASONFORCHANGES"/></td></tr>
					<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
					<tr>
						<td colspan="5"><textarea name="Txt_Reason" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
						onBlur="changeBgColor(this,'#ffffff');" tabindex=6 rows=3 cols=115
						value="<%=strReason%>"><%=strReason%></textarea></td>
					</tr>					
				</table>
	</tr>
		<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr> 
   <!--  </table>	 -->	
<tr><td width="100%" colspan="5">	
<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
	<tr class="Shade" class="RightTableCaption" >
		<!-- <td  width="1" bgcolor="#666666"></td> -->
		<TH class="RightText" width="60" height="25"><fmtOrderModify:message key="LBL_DATE"/></TH>
		<TH class="RightText" align="left" width="140"><fmtOrderModify:message key="LBL_USERNAME"/> </TH>
		<TH class="RightText" align="left" width="500"><fmtOrderModify:message key="LBL_REASONFORMODIF"/></TH>
		<!-- <TH class="RightText" width="1" bgcolor="#666666"></TH> -->
	</tr>
	<TR><TD colspan=5 height=1 class=Line></TD></TR>
<%	intSize = alLog.size();
	if (intSize > 0)
	{
		HashMap hmLog = new HashMap();
		for (int i=0;i<intSize;i++)
		{	hmLog = (HashMap)alLog.get(i);
%>
			<tr >
				<!-- <td width="1" bgcolor="#666666"></td> -->
				<td class="RightText" height="20">&nbsp;<%=GmCommonClass.parseNull((String)hmLog.get("DT"))%></td>
				<td class="RightText">&nbsp;<%=(String)hmLog.get("UNAME")%></td>
				<td class="RightText"><%=(String)hmLog.get("COMMENTS")%></td>
				<!-- <td width="1" bgcolor="#666666"></td> -->
			</tr>
<%		}//End of FOR Loop 
		}else {
%>		<tr><td colspan="6" height="25" align="center" class="RightTextRed">
		<BR><fmtOrderModify:message key="LBL_NO_UPDATE"/></td></tr>
<%		}%>
<TR><TD colspan=5 height=1 class=Line></TD></TR> 
</table>
</td></tr>
<%	/*********************************
	 * Contains the Buttons
	 *********************************/ %>
<tr><td width="100%" colspan="5">
<table border="0" width="100%" cellspacing="0" cellpadding="0" >
	<tr><td align="center" height="30" id="button" colspan="6">
	<%
		String strSubmit = "fnValidateCtrlNum('"+strScreenName+"')";
	%>
				<fmtOrderModify:message key="BTN_SUBMIT" var = "varSubmit"/>
				<fmtOrderModify:message key="BTN_PACKINGSLIP" var = "varPackingSlip" />
				<fmtOrderModify:message key="BTN_VOIDORDER" var = "varVoidOrder"/>
				<fmtOrderModify:message key="BTN_MOVETOBACKORDER" var = "varMoveToBackOrder"/>
				<gmjsp:button disabled="<%=strDisabled%>" value="&nbsp;${varSubmit}&nbsp;" name="Btn_Submit" gmClass="button" onClick="<%=strSubmit %>" buttonType="Save" />&nbsp;&nbsp;
				<gmjsp:button value="&nbsp;${varPackingSlip}&nbsp;" name="Btn_Pack" gmClass="button" onClick="fnPrintPack();" buttonType="Save" />&nbsp;&nbsp;
				<gmjsp:button value="&nbsp;${varVoidOrder}&nbsp;" name="Btn_VoidOrd" gmClass="button" onClick="fnVoidOrd();" buttonType="Save" />&nbsp;&nbsp;
				<gmjsp:button value="&nbsp;${varMoveToBackOrder}&nbsp;" name="Btn_BoOrd" gmClass="button" onClick="fnSinglePartBoOrder();" buttonType="Save" />
	</td><tr>
</table>
</td></tr>
</table>

<script>
<%
	if (strMode.equals("SaveShip"))
	{
%>
	alert("Your order is ready for Shipping. Please print Packing Slip");
<%
	}
%>
</script>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
