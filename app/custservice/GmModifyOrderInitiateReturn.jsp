 
<%
/**********************************************************************************
 * File		 		: GmOrderItemModify.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%>  
<%@ page import ="java.util.Date"%>
<%@ page import ="org.apache.commons.validator.routines.DateValidator"%>
<%@ taglib prefix="fmtModifyOrderInit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmModifyOrderInitiateReturn.jsp -->
<fmtModifyOrderInit:setLocale value="<%=strLocale%>"/>
<fmtModifyOrderInit:setBundle basename="properties.labels.custservice.ModifyOrder.GmInitiateReturn"/>
<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strhAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strMode = (String)request.getAttribute("hMode")==null?"":(String)request.getAttribute("hMode");
	String strApplnDateFmt = strGCompDateFmt;
	String strCurrSymbol = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	String strAccCurrSymb ="";
	
	String strChecked = "";
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	String strHeader = "Change Pricing";
	String strOrderId = "";
	String strAccName = "";
	String strUserName = "";
	java.sql.Date dtOrdDate = null;
	java.sql.Date dtShipDate = null;
	java.util.Date dtOrderDt = null;
	String strComments = "";
	String strShade = "";
	String strItemQty = "";
	String strControl = "";
	String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("INITIATE_RETRUN"));
	
	String strCodeID = "";
	String strSelected = "";

	String strShipCost = "";
	String strPO = "";
	String strAccId = "";
	String strPriceReadOnly = "";
	String strShipReadOnly = "";
	String strNextPartNum = "";
	int intCount = 0;
	String strColor = "";
	String strLine = "";
	String strPartNumHidden = "";
	String strPartDesc = "";
	String strPrice = "";
	String strTemp = "";
	String strPartNum = "";
	String strItemId = "";
	String strReturnQty = "";
	String strUnitPrice = "";
	String strUnitPriceAdj = "";
	String strAdjCode = "";
	String strAdjCodeID = "";
	String strNetUnitPrice = "";
	double intQty = 0.0;
	double intReturnItemQTY = 0;
	double dbItemTotal = 0.0;
	double dbTotal = 0.0;
	
	double dbAdjTotal = 0.0;
	double dbAdjstTotal = 0.0;
	double dbBeforeTotal = 0.0;
	double dbAfterTotal = 0.0;
	
	double dbBeforeItemTotal = 0.0;
	double dbBeforePriceTotal = 0.0;
	double dbAfterItemTotal = 0.0;
	double dbAfterPriceTotal = 0.0;
	
	String strBeforeItemTotal = "";
	String strAfterItemTotal = "";
	String strAfterPrice = "";
	
	String strItemTotal = "";
	String strTotal = "";
	String strAdjTotal = "";
	double dblShip = 0.0;
	String strItemType = "";
	String strItemTypeDesc = "";
		
	ArrayList alCart = new ArrayList();
	ArrayList alReturnReasons = new ArrayList();
	ArrayList alReturns = new ArrayList();
	ArrayList alUsedLot = new ArrayList ();
	
	HashMap hmOrderDetails = new HashMap();
	String strReason = "";
	String strRAId = "";
	String strCreatedBy = "";
	String strCreatedDt = "";
	String strStatus = "";
	String strExpDt = "";
	String strCreditDt = "";
	String strShipDt = "";
	String strInvFl = "";	
	int intPriceSize = 0;
	String strPortalPrice = "";	
	String strTodaysDate =  GmCalenderOperations.getCurrentDate(strGCompDateFmt);
	String strDeptId =  GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessDeptSeq"));
	String disabled="";
	String strInvoiceId="";
	String strEditFL="Y";
	String strAccess="";
	String strOrdredDate = "";
	String strChangePriceFl="";
	String strChangePriceAccess = "";
	String strChnAccessFl = "";
	String strOrderType = "";
	String strEdtShipChrgFl = "";
	String strBeforeTotal = "";
	String strAfterTotal = "";
	String strShowUsage = "";
	String strOvrCurr = "";
	String strSurgeryDate = "";
	String strOrderTypevalue = "";
	//
	String strShowDDTFl = "";
	String strParentOrderId = "";
	
	if (hmReturn != null){
		hmOrderDetails = (HashMap)hmReturn.get("ORDERDETAILS");
		strOrderId = GmCommonClass.parseNull((String)hmOrderDetails.get("ID"));
		// to get the parent order id
		strParentOrderId = GmCommonClass.parseNull((String)hmOrderDetails.get("PARENTORDID"));
		strParentOrderId = strParentOrderId.equals("")? strOrderId: strParentOrderId;
		
		strAccName = GmCommonClass.parseNull((String)hmOrderDetails.get("ANAME"));
		strUserName = GmCommonClass.parseNull((String)hmOrderDetails.get("UNAME"));
		//dtOrdDate = (java.sql.Date)hmOrderDetails.get("ODT");
		strOrdredDate = GmCommonClass.getStringFromDate((java.sql.Date)hmOrderDetails.get("ODT"),strGCompDateFmt);
		strComments = GmCommonClass.parseNull((String)hmOrderDetails.get("COMMENTS"));
		strPO = GmCommonClass.parseNull((String)hmOrderDetails.get("PO"));
		strAccId = GmCommonClass.parseNull((String)hmOrderDetails.get("ACCID"));
		dtShipDate = (java.sql.Date)hmOrderDetails.get("SDATE");
		strInvFl = GmCommonClass.parseNull((String)hmOrderDetails.get("INVFL"));
		strInvoiceId =GmCommonClass.parseNull((String)hmOrderDetails.get("INVOICEID"));
		strOrderType = GmCommonClass.parseNull((String)hmOrderDetails.get("ORDERTYPE"));
		strAccCurrSymb = GmCommonClass.parseNull((String)hmOrderDetails.get("ACC_CURR_SYMB"));
		strOvrCurr = GmCommonClass.parseNull((String) hmOrderDetails.get("OVRIDE_CURR"));
		strSurgeryDate = GmCommonClass.getStringFromDate((java.sql.Date)hmOrderDetails.get("SURG_DT"),strGCompDateFmt);
		strCurrSymbol = strAccCurrSymb.equals("")?strCurrSymbol:strAccCurrSymb; 
		strCurrSymbol = !strOvrCurr.equals("")? strOvrCurr: strCurrSymbol ;

		
		alCart = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("CARTDETAILS"));
		HashMap hmShipDetails = (HashMap)hmReturn.get("SHIPDETAILS");
		if (hmShipDetails != null){
			strShipCost = GmCommonClass.parseZero((String)hmShipDetails.get("SCOST"));
		}
		alUsedLot = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALUSEDLOTDETAILS"));
		strShowUsage = GmCommonClass.parseNull((String) hmReturn.get("SHOW_USAGE"));
		strShowDDTFl = GmCommonClass.parseNull((String)  hmReturn.get("SHOW_DDT_FL"));
	}
	strOrderTypevalue = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue(strOrderType,"SHOW-MODIFY-ORDER")); 
	int intSize = 0;
	HashMap hcboVal = null;		
	alReturnReasons = (ArrayList)request.getAttribute("alReasons");
	alReturns = (ArrayList)hmReturn.get("RETURNS");	
	String strCollapseImage =  strImagePath+"/minus.gif";

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Enter Control Numbers </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmOrderItemModify.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmOrderUsedLotNumberInclude.js"></script>

<script>
var deptId = '<%=strDeptId%>';
var intSize = '<%=alCart.size()%>';
today = new Date('<%=strTodaysDate%>');
orderdt = '<%=dtOrderDt%>';
var dtFormat = '<%=strApplnDateFmt%>';
var currentdate = '<%=strTodaysDate%>';
var accessFl = '<%=strChnAccessFl%>';
var ruleval = '<%=strChangePriceAccess%>';
var usedLotFl = '<%=strShowUsage%>';
var orderType= '<%=strOrderType%>';
var orderTypevalue = '<%=strOrderTypevalue%>';
var orderDDTFl = '<%=strShowDDTFl %>';
var parent_ord_id ='<%=strParentOrderId%>';
// to capture the usage Lot data flag
var usageLotDataFl = 'N';

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnLoad();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmOrderItemServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hOrderId" value="<%=strOrderId%>">
<input type="hidden" name="hOrderType" value="<%=strOrderType%>">
<input type="hidden" name="Cbo_Id" value="<%=strAccId%>">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hInvFl" value="<%=strInvFl%>">
<input type="hidden" name="hReadFL" value="<%=strAccess%>">
<input type="hidden" name="hUsedLotStr" value="" id="hUsedLotStr">
	<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
		<!-- <tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
			<td height="25" class="RightDashBoardHeader">Delivered Order - Return Parts</td>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
		</tr> -->
	<tr>
		<td>
			<table width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2" height="25" class="RightDashBoardHeader">&nbsp;<fmtModifyOrderInit:message key="LBL_HEADER"/></td>
					<td  height="25" class="RightDashBoardHeader">
					    <fmtModifyOrderInit:message key="IMG_HELP" var = "varHelp"/>
						<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
				</tr>
			</table>
		</td>				
	</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="800" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
						<fmtModifyOrderInit:message key="LBL_ORDERID" var = "varOrderId"/>
						<td class="RightTableCaption" HEIGHT="20" align="right" width="25%">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varOrderId}:" td="false"/></td>
						<td class="RightText" width="75%">&nbsp;<%=strOrderId%></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr class="Shade">
						<fmtModifyOrderInit:message key="LBL_ACCOUNTNAME" var = "varAccountName"/>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varAccountName}:" td="false"/></td>
						<td class="RightText">&nbsp;<%=strAccName%></td>
					</tr>
					<tr>
						<fmtModifyOrderInit:message key="LBL_ACCOUNTID" var = "varAccountID"/>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varAccountID}:" td="false"/></td>
						<td class="RightText">&nbsp;<%=strAccId%></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr class="Shade">
						<fmtModifyOrderInit:message key="LBL_ORDERENTEREDBY" var = "varOrderEnteredBy"/>
						<fmtModifyOrderInit:message key="LBL_DATE" var = "varDate"/>
						<fmtModifyOrderInit:message key="LBL_SURGERY_DATE" var="varSurgDate"/>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varOrderEnteredBy}:" td="false"/></td>
						<td class="RightText">&nbsp;<%=strUserName%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B><gmjsp:label type="RegularText"  SFLblControlName="${varDate}:" td="false"/></B>&nbsp;<%=strOrdredDate%>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><gmjsp:label type="RegularText"  SFLblControlName="${varSurgDate}:" td="false"/></b>&nbsp;<%=strSurgeryDate%></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr>
						<fmtModifyOrderInit:message key="LBL_SHIPDATE" var = "varShipDate"/>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varShipDate}:" td="false"/></td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringFromDate(dtShipDate,strApplnDateFmt)%></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>					
					<tr   class="Shade">
						<fmtModifyOrderInit:message key="LBL_COMMENTS" var = "varComments"/>
						<td class="RightTableCaption" valign="top" align="right" HEIGHT="60"><gmjsp:label type="RegularText"  SFLblControlName="${varComments}:" td="false"/></td>
						<td class="RightText" valign="top" >&nbsp;<%=strComments%></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr >
						<fmtModifyOrderInit:message key="LBL_CUSTOMERPO" var = "varCustomerPO"/>
						<td align="right" HEIGHT="24" class="RightTableCaption"><gmjsp:label type="RegularText"  SFLblControlName="${varCustomerPO}:" td="false"/></td>
						<td class="RightText">&nbsp<%=strPO%></td>
					</tr>
					<tr class="Shade">
						<fmtModifyOrderInit:message key="LBL_INVOICEID" var = "varInvoiceID"/>
						<td align="right" HEIGHT="24" class="RightTableCaption"><gmjsp:label type="RegularText"  SFLblControlName="${varInvoiceID}:" td="false"/></td>
						<td class="RightText">&nbsp<%=strInvoiceId%></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr class="shadeRightTableCaption">
						<fmtModifyOrderInit:message key="LBL_RETURNDETAILS" var = "varReturnDetails"/>
						<td class="RightText" HEIGHT="24" colspan="2"><gmjsp:label type="RegularText"  SFLblControlName="${varReturnDetails}" td="false"/></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr>
						<fmtModifyOrderInit:message key="LBL_RETURNREASON" var = "varReturnReason"/>
						
						<td class="RightTableCaption" align="right" HEIGHT="24"><gmjsp:label type="MandatoryText"  SFLblControlName="${varReturnReason}" td="false"/></td>
						<td>&nbsp;<input type="hidden" name="Cbo_Type" value="3300"><select name="Cbo_Reason" id="Region" class="RightText" tabindex="3" onFocus="changeBgColor(this,'#AACCE8');" 
							onChange ="fnReasonChange()" onBlur="changeBgColor(this,'#ffffff');fnReasonChange()"><option value="0" ><fmtModifyOrderInit:message key="LBL_CHOOSEONE" />
<%
			  		intSize = alReturnReasons.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++){
			  			hcboVal = (HashMap)alReturnReasons.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strReason.equals(strCodeID)?"selected":"";
%>
						<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%			  		
					}
%>					
						</select></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<!-- Struts tag lib code modified for JBOSS migration changes -->
					<tr class="Shade">
						<fmtModifyOrderInit:message key="LBL_COMMENTS" var = "varComments"/>
						<td class="RightTableCaption" valign="top" align="right" HEIGHT="24"><gmjsp:label type="RegularText"  SFLblControlName="${varComments}:" td="false"/></td>
						<td>&nbsp;<textarea name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=7 rows=5 cols=45 value=""></textarea></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr>
						<fmtModifyOrderInit:message key="LBL_EXPDATEOFRETURN" var = "varExpDateOfReturn"/>
						<td class="RightTableCaption" align="right" HEIGHT="24"><gmjsp:label type="RegularText"  SFLblControlName="${varExpDateOfReturn}:" td="false"/></td>
						
						<td>
						&nbsp;<gmjsp:calendar textControlName="Txt_ExpDate"   gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
						</td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr class="Shade">
						<fmtModifyOrderInit:message key="LBL_PARENTORDERID" var = "varParentOrderID"/>
						<td class="RightTableCaption" align="right" HEIGHT="24"><gmjsp:label type="RegularText"  SFLblControlName="${varParentOrderID}:" td="false"/></td>
						<td>&nbsp;<input type="text" size="15" name="Txt_ParOrderID" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=23>
						</td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr class="shadeRightTableCaption">
						<fmtModifyOrderInit:message key="LBL_ASSOCIATEDRETURN" var = "varAssoReturn"/>
						<td class="RightText" HEIGHT="24" colspan="2"><gmjsp:label type="RegularText"  SFLblControlName="${varAssoReturn}" td="false"/></td>
					</tr>
					<tr>
						<td colspan="2">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr><td colspan="7" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td height="25" width="70" align="center"><fmtModifyOrderInit:message key="LBL_RAID"/></td>
									<td width="150"><fmtModifyOrderInit:message key="LBL_REASON"/></td>
									<td width="80" align="center"><fmtModifyOrderInit:message key="LBL_INITIATEDBY"/></td>
									<td width="80" align="center"><fmtModifyOrderInit:message key="LBL_INITIATEDDATE"/></td>
									<td width="80" align="center"><fmtModifyOrderInit:message key="LBL_EXPECTEDDATE"/></td>
									<td width="80" align="center"><fmtModifyOrderInit:message key="LBL_CREDITDATE"/></td>
									<td width="50" align="center"><fmtModifyOrderInit:message key="LBL_STATUS"/></td>
								</tr>
								<tr><td colspan="7" height="1" bgcolor="#666666"></td></tr>
<%
						intSize = alReturns.size();
						if (intSize > 0){
							HashMap hmLoop = new HashMap();
							for (int i=0;i<intSize;i++)	{
								hmLoop = (HashMap)alReturns.get(i);								
								strRAId = GmCommonClass.parseNull((String)hmLoop.get("RAID"));
								strReason = GmCommonClass.parseNull((String)hmLoop.get("REASON"));
								strCreatedBy = GmCommonClass.parseZero((String)hmLoop.get("CUSER"));
								strCreatedDt = GmCommonClass.getStringFromDate((java.sql.Date)hmLoop.get("CDATE"),strGCompDateFmt);
								strExpDt = GmCommonClass.getStringFromDate((java.sql.Date)hmLoop.get("EDATE"),strGCompDateFmt);
								strCreditDt = GmCommonClass.getStringFromDate((java.sql.Date)hmLoop.get("CRDATE"),strGCompDateFmt);
								strStatus = GmCommonClass.parseNull((String)hmLoop.get("SFL"));
%>
								<tr>
									<td class="RightText" height="20">&nbsp;<a class=RightText href=javascript:fnViewReturns('<%=strRAId%>');><%=strRAId%></a></td>
									<td class="RightText"><%=strReason%></td>
									<td class="RightText">&nbsp;<%=strCreatedBy%></td>
									<td class="RightText" align="center">&nbsp;<%=strCreatedDt%></td>
									<td class="RightText" align="center">&nbsp;<%=strExpDt%></td>
									<td class="RightText" align="center">&nbsp;<%=strCreditDt%></td>
									<td class="RightText" align="center">&nbsp;<%=strStatus%></td>
								</tr>
								<tr><td colspan="7" height="1" bgcolor="#eeeeee"></td></tr>
<%
							}//End of FOR Loop
						}else {
%>
						<tr><td colspan="7" height="30" valign="absmiddle" align="center" class="RightTextRed"><BR><fmtModifyOrderInit:message key="LBL_MESSAGE"/></td></tr>
<%
						}
%>
							</table>
							<input type="hidden" name="hReturnCnt" value="<%=intSize%>"> 
						</td>
					</tr>			

					<tr>
						<td colspan="2" width="100%">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr><td colspan="13" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td width="60" height="25">&nbsp;<fmtModifyOrderInit:message key="LBL_PART"/></td>
									<td width="400"><fmtModifyOrderInit:message key="LBL_PARTDESCRIPTION"/></td>
									<td width="60" align="center"><fmtModifyOrderInit:message key="LBL_ORDER"/><br><fmtModifyOrderInit:message key="LBL_QTY"/></td>
									<td width="60"><fmtModifyOrderInit:message key="LBL_TYPE"/></td>
									<td width="90" align="center"><fmtModifyOrderInit:message key="LBL_CONTROLNUMBER"/></td>														
									<td width="90" align="center"><fmtModifyOrderInit:message key="LBL_UNIT"/><br><fmtModifyOrderInit:message key="LBL_PRICE"/></td>
									<td width="90" align="center"><fmtModifyOrderInit:message key="LBL_UNIT"/><br><fmtModifyOrderInit:message key="LBL_PRICEADJ"/></td>
									<td width="90" align="center"><fmtModifyOrderInit:message key="LBL_ADJ"/><br><fmtModifyOrderInit:message key="LBL_CODE"/></td>
									<td width="90" align="center"><fmtModifyOrderInit:message key="LBL_NETUNIT"/><br><fmtModifyOrderInit:message key="LBL_PRICE"/></td>
									<td width="100" align="center"><fmtModifyOrderInit:message key="LBL_TOTAL"/><br><fmtModifyOrderInit:message key="LBL_PRICE"/></td>
									<td align="center" width="100" ><fmtModifyOrderInit:message key="LBL_RETURNED"/><br><fmtModifyOrderInit:message key="LBL_QTY"/></td>	
									<td align="center" width="100"><fmtModifyOrderInit:message key="LBL_QTYTORETURN"/></td>								
								</tr>
								<TR><TD colspan=13 height=1 class=Line></TD></TR>
<%
						intSize = alCart.size();
						if (intSize > 0){
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();
							intCount = 0;
							
							for (int i=0;i<intSize;i++)	{
								hmLoop = (HashMap)alCart.get(i);
								if (i<intSize-1){
									hmTempLoop = (HashMap)alCart.get(i+1);
									strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("ID"));
								}
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("ID"));
								strPartNumHidden = strPartNum;
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
								strItemId = GmCommonClass.parseNull((String)hmLoop.get("ITEMID"));
								strReturnQty = GmCommonClass.parseZero((String)hmLoop.get("RTN_ITEM_QTY"));
								strItemType = GmCommonClass.parseZero((String)hmLoop.get("ITEMTYPE"));
								strItemTypeDesc	= GmCommonClass.parseNull((String)hmLoop.get("ITEMTYPEDESC"));
								strPortalPrice = GmCommonClass.parseNull((String)hmLoop.get("CURR_PRICE"));
								strUnitPrice = GmCommonClass.parseZero((String)(String)hmLoop.get("UNITPRICE"));
								strUnitPriceAdj = GmCommonClass.parseZero((String) (String)hmLoop.get("UNITPRICEADJ"));
								strAdjCode = GmCommonClass.parseNull((String)(String)hmLoop.get("UNITPRICEADJCODE"));
								strAdjCodeID = GmCommonClass.parseNull((String)(String)hmLoop.get("UNITPRICEADJCODEID"));
								strNetUnitPrice = GmCommonClass.parseZero((String)(String)hmLoop.get("NETUNITPRICE"));

								strTemp = strPartNum;
								
								if (strPortalPrice.equals("")){ 
									strEditFL ="";									
								}
								
								if (strPartNum.equals(strNextPartNum)){
									intCount++;
									strLine = "<TR><TD colspan=13 height=1 bgcolor=#eeeeee></TD></TR>";
								}else{
									strLine = "<TR><TD colspan=13 height=1 bgcolor=#eeeeee></TD></TR>";
									if (intCount > 0){
										strPartNum = "";
										strPartDesc = "";
										//strColor = "bgcolor=white";
										strLine = "";
									}else{
										//strColor = "";
									}
									intCount = 0;
								}
								if (intCount > 1){
									strPartNum = "";
									strPartDesc = "";
									//strColor = "bgcolor=white";
									strLine = "";
								}
							
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
								strItemQty = GmCommonClass.parseZero((String)hmLoop.get("QTY"));
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								
								if(strNetUnitPrice.equals("0")){
									strNetUnitPrice = strUnitPrice;
								}
								
								intQty = Double.parseDouble(strItemQty);
								intReturnItemQTY = Double.parseDouble(strReturnQty);
								dbItemTotal = Double.parseDouble(strNetUnitPrice);
								dbItemTotal = (intQty - intReturnItemQTY)* dbItemTotal; // Multiply by Qty
								strItemTotal = ""+dbItemTotal;
								dbTotal = dbTotal + dbItemTotal;
								
								// Calculating the Total Price Before Adjustment
								dbBeforeItemTotal = Double.parseDouble(strUnitPrice);
								dbBeforeItemTotal = intQty * dbBeforeItemTotal; // Multiply by Qty
								strBeforeItemTotal = ""+dbBeforeItemTotal;
								dbBeforePriceTotal = dbBeforePriceTotal + dbBeforeItemTotal;
								strBeforeTotal = "" + 	dbBeforePriceTotal;	
								
								// Calculating the Total Price After Adjustment
								dbAfterItemTotal = Double.parseDouble(strPrice);
								dbAfterItemTotal = intQty * dbAfterItemTotal; // Multiply by Qty
								strAfterItemTotal = ""+dbAfterItemTotal;
								dbAfterPriceTotal = dbAfterPriceTotal + dbAfterItemTotal;
								strAfterPrice = "" + 	dbAfterPriceTotal;															
								//strTotal = ""+dbTotal;
								
								out.print(strLine);
%>
								<tr >
									<td class="RightText" height="20">&nbsp;<%=strPartNum%>
										<input type="hidden" name="hItemId<%=i%>" value="<%=strItemId%>">
										<input type="hidden" name="hQty<%=i%>" value="<%=strItemQty%>">
										<input type="hidden" name="hPNum<%=i%>" value="<%=strPartNumHidden%>">
										<input type="hidden" name="hContNum<%=i%>" value="<%=strControl%>">
										<input type="hidden" name="hItemType<%=i%>" value="<%=strItemType%>">
										<input type="hidden" name="hReturnedQty<%=i%>" value="<%=strReturnQty%>">
										<input type="hidden" name="hDOPrice<%=i%>" value="<%=strNetUnitPrice%>">
										<input type="hidden" name="hUnitPrice<%=i%>" value="<%=strUnitPrice%>">
										<input type="hidden" name="hUnitPriceAdj<%=i%>" value="<%=strUnitPriceAdj%>">
										<input type="hidden" name="hAdjCode<%=i%>" value="<%=strAdjCodeID%>">
									</td>
									<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td class="RightText" align="center">&nbsp;<%=strItemQty%></td>
									<td class="RightText" align="center">&nbsp;<%=strItemTypeDesc%></td>
									<td class="RightText" align="center">&nbsp;<%=strControl%></td>
									<gmjsp:currency currSymbol="<%=strCurrSymbol%>" type="CurrTextSign"  textValue="<%=strUnitPrice%>" />
									<gmjsp:currency currSymbol="<%=strCurrSymbol%>" type="CurrTextSign"  textValue="<%=strUnitPriceAdj%>" />
									<td class="RightText" align="center">&nbsp;<%=strAdjCode%></td>
									<gmjsp:currency currSymbol="<%=strCurrSymbol%>" type="CurrTextSign"  textValue="<%=strNetUnitPrice%>" />
									<gmjsp:currency currSymbol="<%=strCurrSymbol%>" type="CurrTextSign"  textValue="<%=strItemTotal%>"/>
									<td class="RightText" align="center" ><%=strReturnQty%></td> 
									<td class="RightText" align="center"><input type="text" size="3" value="" name="hRetQty<%=i%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
										 onBlur="changeBgColor(this,'#ffffff');" tabindex=1></td>
									</tr>
<%
							}//End of FOR Loop						
							strShipCost = GmCommonClass.parseZero(strShipCost);
							dblShip = Double.parseDouble(strShipCost);
							dbBeforeTotal = Double.parseDouble(strBeforeTotal);
							dbTotal = dbBeforeTotal ;
							strBeforeTotal = ""+dbTotal;
							
							dbAfterTotal = Double.parseDouble(strAfterPrice);
							dblShip = Double.parseDouble(strShipCost);
							dbAdjstTotal = dbAfterTotal + dblShip;
							strAfterTotal = ""+dbAdjstTotal;
								
%>								<tr><td colspan="12" height="1" bgcolor="#eeeeee"></td></tr>
								<tr>
									<td>&nbsp;</td>
									<fmtModifyOrderInit:message key="LBL_SHIPPINGCHARGES" var = "varShippingCharges"/>
									<td  height="20" colspan="10" class="RightText">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varShippingCharges}" td="false"/></td>
									<%if(strEdtShipChrgFl.equalsIgnoreCase("Y")){ %>
										<td class="RightText" align="right"><input type="text" size="4" <%=strShipReadOnly%> value="<%=GmCommonClass.getStringWithCommas(strShipCost)%>" 
										name="Txt_SCost" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
										onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;</td>
									<%}else{ %>
										<td class="RightText" align="right"><gmjsp:currency currSymbol="<%=strCurrSymbol%>" type="CurrTextSign"  textValue="<%=strShipCost%>" td="false"/>
										<input type="hidden" name="Txt_SCost" value="<%=GmCommonClass.getStringWithCommas(strShipCost)%>">
										</td>
									<%} %>
								</tr>
								<tr><td colspan="13" height="1" bgcolor="#eeeeee"></td></tr>
								<tr>
									<fmtModifyOrderInit:message key="LBL_TOTALBEFORADJ" var = "varTotBefAdj"/>
									<td colspan="11" height="30" align="right" class="RightTableCaption"><gmjsp:label type="RegularText"  SFLblControlName="${varTotBefAdj}" td="false"/>&nbsp;&nbsp;</td>
									<gmjsp:currency currSymbol="<%=strCurrSymbol%>" type="CurrTextSign"  textValue="<%=strBeforeTotal%>"/>
									<input type="hidden" name="hTotal" value="<%=strBeforeTotal%>">
								</tr>
								<tr>
									<fmtModifyOrderInit:message key="LBL_TOTALAFTERADJ" var = "varTotAftAdj"/>
									<td colspan="11" height="30" align="right" class="RightTableCaption"><gmjsp:label type="RegularText"  SFLblControlName="${varTotAftAdj}" td="false"/>&nbsp;&nbsp;</td>
									<gmjsp:currency currSymbol="<%=strCurrSymbol%>" type="CurrTextSign"  textValue="<%=strAfterTotal%>"/>
									<input type="hidden" name="hTotal" value="<%=strAfterTotal%>">
								</tr>
<%							
						}else {
%>						<fmtModifyOrderInit:message key="LBL_NOPARTINORDER" var = "varNoPartInOrder"/>
						<tr><td colspan="10" height="50" align="center" class="RightTextRed"><gmjsp:label type="RegularText"  SFLblControlName="<BR>${varNoPartInOrder}" td="false"/></td></tr>
<%
						}	
%>
							</table>
							<input type="hidden" name="hCnt" value="<%=intSize%>">
						</td>
					</tr>
					
						<%
						  if (strShowUsage.equals("Y")) {
						%>
						<tr class="ShadeRightTableCaption">
							<td height="22" colspan="2"><a
								href="javascript:fnShowFilters('trEditControlNum');"><IMG
									id="trEditControlNumimg" border=0 src="<%=strCollapseImage%>"></a>&nbsp;Used
								Lot Number</td>
						</tr>

						<tr id="trEditControlNum">
							<td colspan="2"><jsp:include
									page="/custservice/GmOrderUsedLotNumberInclude.jsp" >
									<jsp:param name="HIDE_HEADER" value="Y"/>
									</jsp:include>
									</td>
						</tr>
						<%} %>
						
						<tr>
				<tr>
					<td colspan="2"> 
						<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="FORMNAME" value="" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="Edit" />
							<jsp:param name="Mandatory" value="yes" />
						</jsp:include>
					</td>
				</tr>
				<tr>
					<td align="center" height="30" id="button" colspan="2">
						<fmtModifyOrderInit:message key="BTN_SUBMIT" var = "varSubmit"/>
						<gmjsp:button value="&nbsp;${varSubmit}&nbsp;"  controlId="Btn_Submit" name="Btn_Submit" disabled="<%=disabled%>" gmClass="button" onClick="javascript:fnSubmit();" tabindex="100" buttonType="Save" />&nbsp;&nbsp;
					</td>
				<tr>
				<% if  (disabled.equals("true") ){%>
				<tr>
					<td  align="center" colspan="2">
						<fmtModifyOrderInit:message key="LBL_EDITING" var = "varEditing"/>
						<font color="red"><B><gmjsp:label type="RegularText"  SFLblControlName="${varEditing}" td="false"/></B></font>
					</td>
				</tr>
				<%} %>	
				
				<% if  (strEditFL.equals("") ){%>
				<tr>
					<td  align="center" colspan="2">
						<font color="red"><B><fmtModifyOrderInit:message key="LBL_MESSAGE1"/> </B></font><br><br>
					</td>
				</tr>
				<%} %>				
				</table>
			</td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
    </table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
