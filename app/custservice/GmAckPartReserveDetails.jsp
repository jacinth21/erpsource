
 <!-- \custservice\GmAckPartReserveDetails.jsp -->
<%
	/*************************************************************************************
	 * File		 		: GmAckPartReserveDetails.jsp
	 * Desc		 		: This screen will load the Part Reserved Details for an ACK Order
	 * Version	 		: 1.0
	 * Author			: Harinadh Reddi
	 ************************************************************************************/
%>
<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
String strPartReservData = GmCommonClass.parseNull((String)request.getAttribute("PARTRESERVE"));
String strSessCurrSymbol= GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Acknowledgement Order BO List Report</title>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmAckOrdDetailsInclude.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script>
var ObjData2 = '<%=strPartReservData%>'
var curSymbol = "<%=strSessCurrSymbol%>";
</script>
</head>
<body leftmargin="20" topmargin="20" onLoad="javascript:fnOnLoad();">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
		<td colspan="4">
			<div id="dataGridDiv3" height="100"></div>
			</td>
		</tr>		
</table>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>