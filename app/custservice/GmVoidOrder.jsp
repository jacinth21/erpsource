 <%
/**********************************************************************************
 * File		 		: GmVoid.jsp
 * Desc		 		: This screen is used for voiding the order
 * Version	 		: 1.0
 * author			: RichardK
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!--custservice\GmVoidOrder.jsp  -->
<%@ taglib prefix="fmtVoidOrder" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtVoidOrder:setLocale value="<%=strLocale%>"/>
<fmtVoidOrder:setBundle basename="properties.labels.custservice.GmVoidOrder"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	String strhAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");

	/* To get the Order Returb dashBoard information */
	String strMode = (String)request.getAttribute("hMode")==null?"":(String)request.getAttribute("hMode");
	String StrAccID = (String)request.getAttribute("hAccId")==null?"":(String)request.getAttribute("hAccId");
	String strFromDate = (String)request.getAttribute("hFrom")==null?"":(String)request.getAttribute("hFrom");
	String strToDate = (String)request.getAttribute("hTo")==null?"":(String)request.getAttribute("hTo");

	String strChecked = "";

	String strHeader = "Change Pricing";
	String strOrderId = "";
	String strAccName = "";
	String strUserName = "";
	String strOrdDate = "";
	String strComments = "";
	String strShade = "";
	String strItemQty = "";
	String strControl = "";
	String strVoidFlag = "";
	
	String strCodeID = "";
	String strSelected = "";

	String strShipCost = "";
	String strPO = "";
	String strAccId = "";

	String strParentOrderID = "";	
	String strVoidComments = ""; 
	String strReasonCD = "";
	
	String strReadOnly = "";
	String strErrorCheck = "";
	String strNextPartNum = "";
	int intCount = 0;
	String strColor = "";
	String strLine = "";
	String strPartNumHidden = "";
	String strPartDesc = "";
	String strPrice = "";
	String strTemp = "";
	String strPartNum = "";
	String strItemId = "";

	int intQty = 0;
	double dbItemTotal = 0.0;
	double dbTotal = 0.0;
	String strItemTotal = "";
	String strTotal = "";
	double dblShip = 0.0;
	String strItemTypeDesc = "";

	ArrayList alCart = new ArrayList();
	ArrayList alReturnReasons = new ArrayList();
	HashMap hmOrderDetails = new HashMap();
	String strReason = "";

	int intPriceSize = 0;

	if (hmReturn != null)
	{
		hmOrderDetails 	= (HashMap)hmReturn.get("ORDERDETAILS");
		
		strOrderId 		= (String)hmOrderDetails.get("ID");
		strAccName 		= (String)hmOrderDetails.get("ANAME");
		strUserName 	= (String)hmOrderDetails.get("UNAME");
		strOrdDate 		= (String)hmOrderDetails.get("ODT");
		strComments 	= (String)hmOrderDetails.get("COMMENTS");
		strPO 			= (String)hmOrderDetails.get("PO");
		strAccId 		= (String)hmOrderDetails.get("ACCID");
		strReasonCD 	= GmCommonClass.parseNull((String)hmOrderDetails.get("REASONTYPE"));
		strParentOrderID = GmCommonClass.parseNull((String)hmOrderDetails.get("PARENTORDID"));
		strVoidFlag 	= GmCommonClass.parseNull((String)hmOrderDetails.get("VOIDFL"));
																				

		alCart = (ArrayList)hmReturn.get("CARTDETAILS");
		HashMap hmShipDetails = (HashMap)hmReturn.get("SHIPDETAILS");
		if (hmShipDetails != null)
		{
			strShipCost = GmCommonClass.parseZero((String)hmShipDetails.get("SCOST"));
		}
	}

	// Below condition is used to make the screen readonly
	
	if (strVoidFlag.equals("Y")) 
	{
		strReadOnly = "disabled";
	}
	
	//Check @ the time of Save Void Screen
	if (strhAction.equals("SaveVoidOrder"))
	{
		// Check if Any Error Found @ the time of save 
		strErrorCheck = GmCommonClass.parseNull((String)request.getAttribute("hErrorNumber"));
		
		if (!strErrorCheck.equals(""))
		{
			strReasonCD 	 = GmCommonClass.parseNull((String)request.getAttribute("ReasonCD"));
			strVoidComments	 = GmCommonClass.parseNull((String)request.getAttribute("Comments"));
			
			strParentOrderID = GmCommonClass.parseNull((String)request.getAttribute("ParentOrderID"));
		}
		
	}
				
	int intSize = 0;
	HashMap hcboVal = null;

	strHeader = "Void Order";
	alReturnReasons = (ArrayList)request.getAttribute("alReasons");

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Enter Control Numbers </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
function fnSubmit()
{
	Error_Clear();
	
	// Validate the Reason Code
	if (document.frmVendor.Cbo_Reason.value == '0')
	{
		Error_Details(message[50]);
	}

	// Comments
	if (document.frmVendor.Txt_Comments.value == '')
	{
		Error_Details(message[51]);
	}	

	// If Duplicate Order is selected user should select the 
	// Parent Order Information
	if (document.frmVendor.Cbo_Reason.value == '2500')
	{
		if (document.frmVendor.Txt_ParOrderID.value == '')
		{
			Error_Details(message[52]);
		}	
		
	}

	document.frmVendor.hAction.value = 'SaveVoidOrder';
	document.frmVendor.action = "GmOrderReturnServlet";

	if (ErrorCount > 0)
	{
			Error_Show();
			return false;
	}
	else
	{
		//if (confirm(message[53])
		if (confirm(message[53]))
		{
			document.frmVendor.submit();
		}
	}

  	
}

function fnReturnMain()
{
	document.frmVendor.action = "GmOrderVoidServlet";
	document.frmVendor.submit();
}

</script>


</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmOrderItemServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hOrdId" value="<%=strOrderId%>">
<input type="hidden" name="Cbo_Id" value="<%=strAccId%>">

<input type="hidden" name="hMode" value="Reload">
<input type="hidden" name="Cbo_AccId" value="<%=StrAccID%>">
<input type="hidden" name="Txt_FromDate" value="<%=strFromDate%>">
<input type="hidden" name="Txt_ToDate" value="<%=strToDate%>">

<input type="hidden" name="hInputStr" value="">
	<% /* If Error Found The same will be displayed in the Header section if the screen */ %>
	<jsp:include page="/common/GmScreenError.jsp" />
	
	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
			<td height="25" class="RightDashBoardHeader"><fmtVoidOrder:message key="LBL_ORDER_ITEM"/> - <%=strHeader%></td>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="700" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtVoidOrder:message key="LBL_ORDER_ID"/>:</td>
						<td class="RightText">&nbsp;<b><%=strOrderId%></b></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtVoidOrder:message key="LBL_ACC_NAME"/>:</td>
						<td class="RightText">&nbsp;<b><%=strAccName%></b></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtVoidOrder:message key="LBL_ORDER_ENT_BY"/>:</td>
						<td class="RightText">&nbsp;<%=strUserName%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtVoidOrder:message key="LBL_DATE"/>Date :&nbsp;<%=strOrdDate%></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightText" valign="top" align="right" HEIGHT="60"><fmtVoidOrder:message key="LBL_COMMENTS"/>:</td>
						<td class="RightText" valign="top" >&nbsp;<%=strComments%></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td align="right" HEIGHT="24" class="RightText"><fmtVoidOrder:message key="LBL_CUST_PO"/>:</td>
						<td class="RightText">&nbsp<%=strPO%></td>
					</tr>
<%
/**************************************************** 
 * Below Section has the Comment Section Information 
 ****************************************************/
%>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtVoidOrder:message key="LBL_REASON_VOIDING"/>:</td>
						<td>&nbsp;<input type="hidden" name="Cbo_Type" value="3300">
						<select name="Cbo_Reason" id="Region" <%=strReadOnly%> class="RightText" tabindex="3" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
<%			  		intSize = alReturnReasons.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alReturnReasons.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strReasonCD.equals(strCodeID)?"selected":"";
%>							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%			  		}
%>					</select></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightText" valign="top" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtVoidOrder:message key="LBL_COMMENTS"/>:</td>
						<td>&nbsp;<textarea name="Txt_Comments"  class="InputArea" <%=strReadOnly%> 
						onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=7 rows=5 
						cols=45 value=""><%=strVoidComments%></textarea></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24"><fmtVoidOrder:message key="LBL_PARENT_ORDER_ID"/>:</td>
						<td>&nbsp;<input type="text" size="10" value="<%=strParentOrderID%>" <%=strReadOnly%> name="Txt_ParOrderID" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=23>
						</td>
					</tr>
<%
/* 
 * End of Comment Section 
 */
%>
<%
/**************************************************** 
 * Part Line Item Information 
 ****************************************************/
%>
					<tr>
						<td colspan="2" width="698">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr><td colspan="7" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td width="60" height="25">&nbsp;<fmtVoidOrder:message key="LBL_PART_NUM"/></td>
									<td width="450"><fmtVoidOrder:message key="LBL_DESC"/></td>
									<td width="50"><fmtVoidOrder:message key="LBL_QTY"/></td>
									<td width="60"><fmtVoidOrder:message key="LBL_TYPE"/></td>
									<td width="100" align="center"><fmtVoidOrder:message key="LBL_CONTROL_NUMBER"/></td>
									<td width="100" align="center"><fmtVoidOrder:message key="LBL_PRICE_EACH"/></td>
									<td width="100" align="center"><fmtVoidOrder:message key="LBL_AMOUNT"/></td>
<%
					if (strhAction.equals("ReturnPart"))
					{
%>
									<td align="center"><fmtVoidOrder:message key="LBL_RETURN_QTY"/></td>
<%
					}
%>
								</tr>
								<TR><TD colspan=8 height=1 class=Line></TD></TR>
<%
						intSize = alCart.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();

							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alCart.get(i);
								if (i<intSize-1)
								{
									hmTempLoop = (HashMap)alCart.get(i+1);
									strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("ID"));
								}
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("ID"));
								strPartNumHidden = strPartNum;
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
								strItemId = GmCommonClass.parseNull((String)hmLoop.get("ITEMID"));
								strTemp = strPartNum;
								strItemTypeDesc	= GmCommonClass.parseNull((String)hmLoop.get("ITEMTYPEDESC"));
								
								if (strPartNum.equals(strNextPartNum))
								{
									intCount++;
									strLine = "<TR><TD colspan=8 height=1 bgcolor=#eeeeee></TD></TR>";
								}

								else
								{
									strLine = "<TR><TD colspan=8 height=1 bgcolor=#eeeeee></TD></TR>";
									if (intCount > 0)
									{
										strPartNum = "";
										strPartDesc = "";
										//strColor = "bgcolor=white";
										strLine = "";
									}
									else
									{
										//strColor = "";
									}
									intCount = 0;
								}

								if (intCount > 1)
								{
									strPartNum = "";
									strPartDesc = "";
									//strColor = "bgcolor=white";
									strLine = "";
								}
							
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
								strItemQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));

								intQty = Integer.parseInt(strItemQty);
								dbItemTotal = Double.parseDouble(strPrice);
								dbItemTotal = intQty * dbItemTotal; // Multiply by Qty
								strItemTotal = ""+dbItemTotal;
								dbTotal = dbTotal + dbItemTotal;
								//strTotal = ""+dbTotal;


								out.print(strLine);
%>
								<tr >
									<td class="RightText" height="20">&nbsp;<%=strPartNum%>
									<input type="hidden" name="hItemId<%=i%>" value="<%=strItemId%>">
									<input type="hidden" name="hQty<%=i%>" value="<%=strItemQty%>">
									<input type="hidden" name="hPNum<%=i%>" value="<%=strPartNumHidden%>">
									</td>
									<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td class="RightText">&nbsp;<%=strItemQty%></td>
									<td class="RightText">&nbsp;<%=strItemTypeDesc%></td>
									<td class="RightText" align="center">&nbsp;<%=strControl%></td>
									<td class="RightText" align="right">&nbsp;<%=GmCommonClass.getStringWithCommas(strPrice)%>&nbsp;</td>
									<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strItemTotal)%>&nbsp;</td>
								</tr>
<%							}//End of FOR Loop
							dblShip = Double.parseDouble(strShipCost);
							dbTotal = dbTotal + dblShip;
							strTotal = ""+dbTotal;
%>
							<tr><td colspan="8" height="1" bgcolor="#eeeeee"></td></tr>
							<tr>
								<td>&nbsp;</td>
								<td height="20" colspan="5" class="RightText">&nbsp;<fmtVoidOrder:message key="LBL_SHIP_CHARGES"/></td>
								<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strShipCost)%>&nbsp;</td>
							</tr>
							<tr><td colspan="8" height="1" bgcolor="#eeeeee"></td></tr>
							<tr>
								<td colspan="6" height="30" align="right" class="RightTableCaption"><fmtVoidOrder:message key="LBL_TOTAL"/>&nbsp;&nbsp;</td>
								<td class="RightText" align="right">$<%=GmCommonClass.getStringWithCommas(strTotal)%>&nbsp;
								<input type="hidden" name="hTotal" value="<%=strTotal%>"></td>
							</tr>
<%
						}else {
%>
						<tr><td colspan="8" height="50" align="center" class="RightTextRed"><BR><fmtVoidOrder:message key="LBL_NO_PART"/> !</td></tr>
<%
						}	
%>
							</table>
							<input type="hidden" name="hCnt" value="<%=intSize%>">
						</td>
					</tr>
<%
/* 
 * End of Part Item
 */

%>					
				</table>
			</td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
    </table>
<%
		String strDisableBtnVal = strReadOnly;
	if(strDisableBtnVal.equals("disabled"))
	{
		strDisableBtnVal = "true";
	}
%>
	<table border="0" width="700" cellspacing="0" cellpadding="0" >
	<tr>
		<td align="center" height="30" id="button">
			<fmtVoidOrder:message key="BTN_RETURN" var="varReturn"/><gmjsp:button value="&nbsp;${varReturn}&nbsp;" name="Btn_ReturnMain" 
			gmClass="button" onClick="javascript:fnReturnMain()" buttonType="Save" />&nbsp;&nbsp;&nbsp;&nbsp;
			<fmtVoidOrder:message key="BTN_VOID" var="varVoid"/><gmjsp:button disabled="<%=strDisableBtnVal%>" value="&nbsp;${varVoid}&nbsp;" name="Btn_Submit" 
			gmClass="button" onClick="javascript:fnSubmit()"buttonType="Save"  />
		</td>
	<tr>
	</table>

</FORM>
<%@ include file="/common/GmFooter.inc"%>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
