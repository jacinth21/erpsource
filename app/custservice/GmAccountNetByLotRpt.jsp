
<%
	/***************************************************************************************************************************************
	 * File		 		: GmAccountNetByLotRpt.jsp.jsp
	 * Desc		 		: This screen is used for Loading the Account Net Qty by Lot
	 * Version	 		: 1.0
	 * author			: gpalani
	 ****************************************************************************************************************************************/
%>
<!-- \custservice\GmAccountNetByLotRpt.jsp-->
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%> 	
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>
<%@ page import="com.globus.common.beans.GmCommonControls"%>
<%@ taglib prefix="fmtFSWarehouse" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<fmtFSWarehouse:setLocale value="<%=strLocale%>"/>
<fmtFSWarehouse:setBundle basename="properties.labels.operations.GmFieldSalesWarehouseRpt"/>
<bean:define id="gridData" name="frmfieldSalesWarehouse" property="gridData" type="java.lang.String"></bean:define>
<bean:define id="account" name="frmfieldSalesWarehouse" property="account" type="java.lang.String"></bean:define>
<bean:define id="searchaccount" name="frmfieldSalesWarehouse" property="searchaccount" type="java.lang.String"></bean:define>

<%
    String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");

	String strWikiTitle = GmCommonClass.parseNull((String)GmCommonClass.getWikiTitle("FIELD_SALES_NET_QUANTITY"));
	String strApplDateFmt = strGCompDateFmt;
	GmCalenderOperations gmCal = new GmCalenderOperations();
	String strCurDate = GmCommonClass.parseNull((String) session.getAttribute("strSessTodaysDate"));
	HashMap hmReturn = new HashMap();
	ArrayList alSystem = new ArrayList();
	hmReturn = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturn"));
	alSystem = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SYSLIST"));
	String strChkSelectAll = GmCommonClass.parseNull((String)request.getAttribute("CHKSELECTALL"));
	String strChkAll =strChkSelectAll.equals("")?"":"checked";
	
	String strTodaysDate = gmCal.getCurrentDate(strApplDateFmt)==null?"":gmCal.getCurrentDate(strApplDateFmt); 
	
	HashMap hmMapProject = new HashMap();
	hmMapProject.put("ID", "");
	hmMapProject.put("PID", "ID");
	hmMapProject.put("NM", "NAME");
%>
<HTML>
<HEAD>
<TITLE>GlobusOne Enterprise Portal: Account Net Qty By Lot Reports</TITLE>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
 <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">   
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmFieldSalesWarehouseRpt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<script>
var objGridData = '';
objGridData = '<%=gridData%>';
var date_format = '<%=strApplDateFmt%>';
var todaysDate = '<%=strTodaysDate%>';
var lblSurgeryDt = '<fmtFSWarehouse:message key="LBL_SURGERY_DATE"/>';
</script>
</HEAD>
<!-- Custom tag lib code modified for JBOSS migration changes -->
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();" onkeypress="fnLogEnter();">
	<html:form action="/gmFieldSalesWarehouseRpt.do">
		<html:hidden property="strOpt"/>
		<html:hidden property="warehouseId" value=""/>
		<html:hidden property="locationId" value="" />
		<html:hidden property="partNumber" value="" />
		<html:hidden property="sysIds" />
		<html:hidden property="strInput" />
		<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="7" height="25" class="RightDashBoardHeader">
		 <logic:equal name="frmfieldSalesWarehouse" property="locationType" value="56005"><fmtFSWarehouse:message key="TD_ACCT_NET_QTY_LOT_HEADER"/></logic:equal>
		 <logic:equal name="frmfieldSalesWarehouse" property="locationType" value="26230710"><fmtFSWarehouse:message key="TD_ACCT_NET_QTY_BY_LOT_HEADER"/></logic:equal>
		<logic:equal name="frmfieldSalesWarehouse" property="locationType" value="70110"><fmtFSWarehouse:message key="TD_ACCT_NET_QTY_BY_LOT_HEADER"/></logic:equal>
					
					</td>
				<fmtFSWarehouse:message key="IMG_ALT_HELP" var="varHelp"/>
				<td height="25" class="RightDashBoardHeader"><img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td colspan="8" class="LLine" height="1"></td>
			</tr>
			<tr height="25">
			<fmtFSWarehouse:message key="LBL_ACCOUNT_TYPE" var="varLocType"/> 
				<td class="RightTableCaption" colspan="3" align="right" width="15%">&nbsp;<gmjsp:label
						type="RegularText" SFLblControlName="${varLocType}:" td="false" />
				</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown controlName="locationType" SFFormName="frmfieldSalesWarehouse" SFSeletedValue="locationType" 
				SFValue="alLocationType" codeId = "CODEID" codeName = "CODENM" defaultValue= "[Choose One]" tabIndex="2" onChange="fnChangeAccSource(this.value,true);"/>
			</td>
			<fmtFSWarehouse:message key="LBL_NAME" var="varName"/>
			<td class="RightTableCaption" colspan="2" align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<gmjsp:label
						type="RegularText" SFLblControlName="${varName}:" td="false" />
			</td>
			
			<!--   
			<td colspan="2" id="fieldSales">&nbsp;<gmjsp:dropdown controlName="fieldSales" SFFormName="frmfieldSalesWarehouse" SFSeletedValue="fieldSales" 
				SFValue="alFieldSales" codeId = "ID" codeName = "NAME" defaultValue= "[Choose One]" tabIndex="2"/>  
			</td>
			
		-->
			
			<%-- <td id="accounts">&nbsp;<gmjsp:dropdown controlName="account" SFFormName="frmfieldSalesWarehouse" SFSeletedValue="account" 
				SFValue="alAccounts" codeId = "ID" codeName = "NAME" defaultValue= "[Choose One]" tabIndex="2"/>  
			</td> --%> 
						<td colspan="2" width="70%">
						<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="account" />
					<jsp:param name="METHOD_LOAD" value="loadAccountByType&searchType=PrefixSearch" />
					<jsp:param name="WIDTH" value="400" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value="2"/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%=searchaccount%>" />
					<jsp:param name="CONTROL_ID_VALUE" value="<%=account%>" />
					<jsp:param name="SHOW_DATA" value="100" />
					<jsp:param name="AUTO_RELOAD" value="" />
					<jsp:param name="DYNAMIC_OBJ" value="#locationType^" />
				</jsp:include>
				</td>
			</tr>			
			<tr>
				<td colspan="8" class="LLine" height="1"></td>
			</tr>
			<tr height="30" class="Shade">
		    <fmtFSWarehouse:message key="LBL_OUTPUT" var="varOutput"/>
		    <td colspan="2" class="RightTableCaption" align="right">&nbsp;<font color="red">*</font><gmjsp:label
						type="RegularText" SFLblControlName="${varOutput}:" td="false" />&nbsp;
				</td>
				<td colspan="1"> <gmjsp:dropdown controlName="accOutputType" SFFormName="frmfieldSalesWarehouse" SFSeletedValue="accOutputType" 
				SFValue="alAccOutType" codeId = "CODEID" codeName = "CODENM" tabIndex="3" onChange="javascript:fnChngeOutputType(this);"/>  
			</td> 
				<fmtFSWarehouse:message key="LBL_PART" var="varPart"/> 
				<td id="accOutType" class="RightTableCaption" align="right" colspan="2">&nbsp;&nbsp;<gmjsp:label
						type="RegularText" SFLblControlName="${varPart}:" td="false" />&nbsp;&nbsp;
				</td>
				<td class="RightTableCaption" style="vertical-align: middle;"><html:text property="pNum" size="40"
									onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
									onblur="changeBgColor(this,'#ffffff');"
									tabindex="4"/>
									</td> 
			<fmtFSWarehouse:message key="LBL_SYSTEM" var="varSystem"/> 
				<td id="system" align="right" class="RightTableCaption">&nbsp;<gmjsp:label
						type="RegularText" SFLblControlName="${varSystem}:" td="false" />
				</td>						
			
				<%
				GmCommonControls.setWidth(350);
				%> 
				<td id="systemVal"><input type="checkbox" name="Chk_selectAll" <%=strChkAll%> onClick="fnSelectAll(this);">Select All <table><tr>
				<td id="systemVal"><%=GmCommonControls.getChkBoxGroup("", alSystem,"Chk_System", hmMapProject, "NM")%></td></tr>
				</table>
				</td>
									
			<fmtFSWarehouse:message key="LBL_QTY" var="varQty"/> 
			 <td class="RightTableCaption" colspan="1" align="right">&nbsp;<gmjsp:label
						type="RegularText" SFLblControlName="${varQty}:" td="false" />
				</td> 
				<td colspan="4">
					<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							 <td width="50%">&nbsp;<gmjsp:dropdown controlName="fsQtyVal" SFFormName="frmfieldSalesWarehouse" SFSeletedValue="fsQtyVal" SFValue="alQtySearchList" codeId = "CODENMALT"  codeName = "CODENM"  defaultValue= "[Choose One]"  />
                        	<html:text property="fsQuantity" name="frmfieldSalesWarehouse" size="5" tabindex="21" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td> 
                        	<fmtFSWarehouse:message key="BTN_LOAD" var="varLoad"/>
                        	<td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="Button" onClick="fnRptLoad();" tabindex="5" buttonType="Load" />
                        	</td>
                        </tr>
                      </table>
                </td>
			</tr>
				<%
					if (gridData.indexOf("cell") != -1) {
				%>
				
			<tr>
				<td colspan="8">
					<div id="acctivityRpt" style="height: 320px; width: 1050px"></div>
					<div id="pagingArea" style=" width:800px"></div>
					</td>
					
			</tr>
				
				<tr>
				<td colspan="8" class="LLine" height="1"></td>
			</tr>
			<tr height="25">
			<fmtFSWarehouse:message key="LBL_SHIP_DATE" var="varShipDate"/>
			<td colspan="8" class="RightTableCaption" align="center">&nbsp;<gmjsp:label
						type="RegularText" SFLblControlName="${varShipDate}:" td="false" />
			
				<input type="text" size="10" readonly style ="cursor:default;" value="${varfromDt}" name="shipDate" id="shipDate" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onchange="javascript:onChangeFromDate(this)" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
						<img id="Img_Date" style="cursor:hand" onclick="javascript:showSingleCalendar('divShip_Date','shipDate');" title="calendar"   src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />
									<div id="divShip_Date" style="position: absolute; z-index: 10;margin-left: 320px;"></div>
									
			 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<fmtFSWarehouse:message key="LBL_SURGERY_DATE" var="varSurgeryDate"/>
			<gmjsp:label
						type="RegularText" SFLblControlName="${varSurgeryDate}:" td="false" />
						
				<input type="text" size="10" readonly style ="cursor:default;" value="${varfromDt}" name="surgeryDate" id="surgeryDate" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onchange="javascript:onChangeFromDate(this)" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
						<img id="Img_Date" style="cursor:hand" onclick="javascript:showSingleCalendar('divSurg_Date','surgeryDate');" title="calendar"   src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />
									<div id="divSurg_Date" style="position: absolute; z-index: 8;margin-left: 535px;"></div>
			 </td>
		</tr>
			<tr height="25"><fmtFSWarehouse:message key="LBL_CHOOSE_ACTION" var="varChooseAction"/>
			<td colspan="8" class="RightTableCaption" align="center">&nbsp;<gmjsp:label
						type="RegularText" SFLblControlName="${varChooseAction}:" td="false" />
				<gmjsp:dropdown controlName="downloadType" SFFormName="frmfieldSalesWarehouse"   defaultValue= "[Choose One]"
				SFValue="alDownloadType" codeId = "CODEID" codeName = "CODENM"  />&nbsp;&nbsp;  <gmjsp:button value="&nbsp;Submit&nbsp;" gmClass="Button" onClick="fnDownloadRpt();"  buttonType="Load" />
			</td>
		</tr>
		<tr height="25">
			<td colspan="8" align="center">
			    <div class='exportlinks'><fmtFSWarehouse:message key="DIV_EXPORT_OPT"/> : <img
					src='img/ico_file_excel.png' />&nbsp;<a href="#"
					onclick="fnExport();"><fmtFSWarehouse:message key="DIV_EXCEL"/> 
				</a></div>
			</td>
		</tr>
				<%
					}else{
				%>
			<tr>
				<td colspan="8" class="LLine" height="1"></td>
			</tr>	
			<tr>
			<td colspan="8" align="center" class="RegularText"><fmtFSWarehouse:message key="TD_NO_DATA_FOUND"/></td>
			</tr>
			<%} %>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
