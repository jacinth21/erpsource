
<!-- \sales\AreaSet\GmAddInfo.jsp -->

<%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>

<%
	String strHtmlJasperRpt = "";
	String strCompID = "";
	String strCompanyName = "";
	String strCompanyShortName = "";
	String strCompanyAddress = "";
	String strCompanyPhone = "";
	String strHeadAddress = ""; 
	String strShipDate = "";
	String strApplnDateFmt = strGCompDateFmt;
	GmCalenderOperations gmCal = new GmCalenderOperations();
	gmCal.setTimeZone(strGCompTimeZone);
	String strCompDateFmt = gmCal.getCurrentDate(strApplnDateFmt+" HH:mm:ss"); //To get the current date with time
	HashMap hmResult = (HashMap)request.getAttribute("hmResult");
	HashMap hmreturn = (HashMap)request.getAttribute("hmReturn1");
	//Removed the populated cart arraylist usage code since BBA doesnt use product family usagecode. Prod Issue
    //Future usage code part exclusion:Refer PMT-39130 Design forthe implementation.
	ArrayList alDataList =  GmCommonClass.parseNullArrayList((ArrayList)hmreturn.get("SETLOAD"));
	String strReportName = GmCommonClass.parseNull((String)hmResult.get("JASPERNAME"));
	String strScreenName = GmCommonClass.parseNull((String)request.getParameter("screenName"));
	strCompID = GmCommonClass.parseNull((String)hmResult.get("COMPANYID"));
	
	String strPackHeader = GmCommonClass.parseNull((String)request.getAttribute("PACKHEADER"));
	String strPackFooter = GmCommonClass.parseNull((String)request.getAttribute("PACKFOOTER"));
	String strDivisionId = GmCommonClass.parseNull((String) hmResult.get("DIVISION_ID"));
	strDivisionId = strDivisionId.equals("")? "2000" : strDivisionId;
	HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(gmDataStoreVO.getCmpid(), strDivisionId);
	String strCompanyLogo = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMP_LOGO"));
	if(gmDataStoreVO.getCmpid().equals("1000") || gmDataStoreVO.getCmpid().equals("1001")){
		if(!strCompID.equals("")){
			strCompanyName = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMPNAME"));
			strCompanyShortName = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMP_SHORT_NAME")); 
			strCompanyAddress =  GmCommonClass.parseNull((String)hmCompanyAddress.get("COMPADDRESS"));
			strCompanyPhone = GmCommonClass.parseNull((String)hmCompanyAddress.get("PH"));			
		}	
		strHeadAddress = strCompanyName +","+strCompanyAddress +","+strCompanyPhone;
	}else{
		//OUS code merge for Global_Hormonization
		strCompanyAddress = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYADDRESS"));
		strCompanyName = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYNAME"));
		strCompanyPhone =  GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYPHONE"));		
		strHeadAddress = strCompanyName + "," + strCompanyAddress; 
	}	
	hmResult.put("HEADERADDRESS",strHeadAddress);
	hmResult.put("PRINTDATE",strCompDateFmt);
	hmResult.put("PHEADER",strPackHeader);
	hmResult.put("PFOOTER",strPackFooter);
	hmResult.put("LOGO", strCompanyLogo);
	hmResult.put("COMPANYDFMT", strApplnDateFmt);
%>
<script>
function fnPrint(){
	window.print();
	return false;
}
function hidePrint(){
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
	strObject1 = eval("document.all.button1");
	tdimginnner = strObject1.innerHTML;
	strObject1.innerHTML = "";	
}
function showPrint(){
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
	strObject1 = eval("document.all.button1");
	strObject1.innerHTML = tdimginnner ;
}

</script>
<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<table>
	 	<tr>	
			<td>
				<%
				GmJasperReport gmJasperReport = new GmJasperReport();			
				gmJasperReport.setRequest(request);
				gmJasperReport.setResponse(response);
				gmJasperReport.setJasperReportName(strReportName);
				gmJasperReport.setHmReportParameters(hmResult);
				gmJasperReport.setReportDataList(alDataList);
				gmJasperReport.setBlDisplayImage(true);
				strHtmlJasperRpt = gmJasperReport.getHtmlReport();				
				%>	
				<%=strHtmlJasperRpt%>
			</td>
		</tr>
		<%if(strScreenName.equals("")){ %>
		<tr>
			<td align="center" id="button">
				<gmjsp:button value="&nbsp;Print&nbsp;" name="Btn_Print" gmClass="button" tabindex="2" onClick="return fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				<gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_Close" gmClass="button" tabindex="3"	onClick="window.close();" buttonType="Load" />				
			</td>
		</tr>
		<%} %>
</table>
<%@ include file="/common/GmFooter.inc"%>
</BODY>