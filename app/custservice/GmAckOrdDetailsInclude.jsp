

<%
	/**********************************************************************************
	 * File		 		: GmAckOrdDetailsInclude.jsp
	 * Desc		 		: This screen will load the Cartdetails for an Order
	 * Version	 		: 1.0
	 * Author			: Rajkumar Jayakumar
	 ************************************************************************************/
%>
<!-- \custservice\GmAckOrdDetailsInclude.jsp -->
<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@include file="/common/GmHeader.inc"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtAckOrdDetailsInclude" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmAckOrdDetailsInclude.jsp-->
<fmtAckOrdDetailsInclude:setLocale value="<%=strLocale%>"/>
<fmtAckOrdDetailsInclude:setBundle basename="properties.labels.custservice.GmOrderItemControl"/>
<%
HashMap hmOrderDetails = new HashMap();
HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
HashMap hmShipDetails = (HashMap)hmReturn.get("SHIPDETAILS");

String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
String strCartData = GmCommonClass.parseNull((String)request.getAttribute("CARTDET"));
String strDelNotes = GmCommonClass.parseNull((String)request.getAttribute("DELNOTES"));
String strSessCurrSymbol= GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
String strShipCost = "0.00";
String strAccCurrSymb = "";
if (hmReturn != null && hmShipDetails != null){
	strShipCost = GmCommonClass.parseZero((String)hmShipDetails.get("SCOST"));
	strShipCost = GmCommonClass.getStringWithCommas(strShipCost);
	hmOrderDetails = (HashMap)hmReturn.get("ORDERDETAILS");
	strAccCurrSymb = GmCommonClass.parseNull((String)hmOrderDetails.get("ACC_CURR_SYMB"));
	strSessCurrSymbol = strAccCurrSymb.equals("")?strSessCurrSymbol:strAccCurrSymb; 
}
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Acknowledgement Order BO List Report</title>

<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmAckOrdDetailsInclude.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<style type="text/css" media="all">@import url("<%=strCssPath%>/screen.css");</style>

<script>
var ObjData ='<%=strCartData%>';
var ObjData1 = '<%=strDelNotes%>'
var curSymbol = "<%=strSessCurrSymbol%>";
var mygrid;
var shipCost = '<%=strShipCost%>';
var lblTotal='<fmtAckOrdDetailsInclude:message key="LBL_TOTAL"/>';
</script>
</head>
<body leftmargin="20" topmargin="20" onLoad="javascript:fnOnLoad();fnLoad();">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
		<td colspan="4">
			<div id="dataGridDiv1" style="width: 100%; height:200"></div>
			</td>
		</tr>
		<tr>
		<td colspan="4" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
			<tr class="shade">
			<td class="RightTableCaption" align="right" height="24" width="92%">&nbsp;<fmtAckOrdDetailsInclude:message key="LBL_SHIPPING_CHARGES"/>:&nbsp;</td>
			<td width="10%">
				<input type="text" name="shipCharge" class="InputArea" style="text-align: right" onfocus="changeBgColor(this,'#AACCE8');" size="5" value="<%=strShipCost %>"
					 onblur="changeBgColor(this,'#ffffff');" /></td>
			</tr>
			</table>
		</td>
		</tr>
		<tr>
		<td colspan="4" align="right" height="24">
			<input type="hidden" name="hTotal" id="hTotal"></input>
			<span id="total" style="font-weight: bold;"></span>
		</td>
		</tr>
		<tr><td colspan="4">
			<div id="dataGridDiv2" style="width: 100%; height:100"></div>
			</td>
		</tr>		
</table>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>