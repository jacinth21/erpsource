
<%
/**********************************************************************************
 * File		 		: GmSetConsignRollback.jsp
 * Desc		 		: This screen is used to Rollback Set Consignments
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<!-- Imports for Logger -->
> 
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ include file="/common/GmHeader.inc" %>
<%@ page buffer="16kb" autoFlush="true" %>

<%@ taglib prefix="fmtSetConsignRollback" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- custservice\GmSetConsignRollback.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtSetConsignRollback:setLocale value="<%=strLocale%>"/>
<fmtSetConsignRollback:setBundle basename="properties.labels.custservice.GmSetConsignRollback"/>



<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	Logger log = GmLogger.getInstance(GmCommonConstants.CUSTOMERSERVICE);
	HashMap hmParam = new HashMap();
	ArrayList alCancelReason ;
	hmParam = (HashMap)request.getAttribute("HMPARAM");
	
	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	String strCommonPath = GmCommonClass.getString("GMCOMMON");
	
	String strAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strCancelType = GmCommonClass.parseNull((String)request.getAttribute("STRCANCELTYPE"));
	String strTxnId = GmCommonClass.parseNull((String)request.getAttribute("STRTXNID"));
	
	String strCancelReason = "";
	String strComments = "";
	String strMessage = "";
	String strDisabled = "";
   alCancelReason  = (ArrayList)request.getAttribute("ALCANCELREASON");
   
   log.debug(" values inside hmParam is " + hmParam);
	
	if (hmParam != null)
	{
	     strTxnId = GmCommonClass.parseNull((String)hmParam.get("TXNID"));
         strCancelReason = GmCommonClass.parseNull((String)hmParam.get("CANCELREASON"));
         strCancelType = GmCommonClass.parseNull((String)hmParam.get("CANCELTYPE"));
         strComments = GmCommonClass.parseNull((String)hmParam.get("COMMENTS"));
	}
	
	if(strAction.equals("SUCCESSCANCEL"))
	{
		strDisabled = "true";
		strMessage = " Consignment " + strTxnId +"  Successfully Rolled back";
	}
	
	log.debug("strComments is " +strComments);
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Consign Rollback </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<SCRIPT>
function fnRollBackSetCsg()
{
		
		document.frmConsignRollBack.hAction.value = "cancel";
		document.frmConsignRollBack.submit();	
}

</SCRIPT>
</head>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmConsignRollBack" method="POST" action="<%=strServletPath%>/GmCommonCancelServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hTxnId" value="<%=strTxnId%>">
<input type="hidden" name="hCancelType" value="<%=strCancelType%>">

	<table border="0" cellspacing="0" cellpadding="0" width="700">
		<tr>
			<td width="1" class="Line" rowspan="10"></td>
			<td height="1" colspan="2" class="Line"></td>
			<td width="1" class="Line" rowspan="10"></td>
		</tr>
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="2"><fmtSetConsignRollback:message key="LBL_SET_CONSIGNMENT_ROLLBACK"/></td>
		</tr>
		<tr><td colspan="2" height="1" class="Line"></td></tr>	
		<tr><td colspan="2" height="30"><%=strMessage%></td></tr>
		<tr>
			<td height="30" align="right" class="RightTableCaption"><fmtSetConsignRollback:message key="LBL_TRANSACTION_ID"/> :</td>
			<td>&nbsp;<%=strTxnId%></td>
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td height="30" align="right" class="RightTableCaption"> <fmtSetConsignRollback:message key="LBL_REASON"/>: </td>
			<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Reason"  seletedValue="<%= strCancelReason %>" 	
						tabIndex="1"  value="<%= alCancelReason%>" codeId = "CODEID" codeName = "CODENM" defaultValue= "[Choose One]"  />
			 </td>
		</tr>
		<tr>
			<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;&nbsp;<fmtSetConsignRollback:message key="LBL_COMMENTS"/>:</td>
			<td>&nbsp;<textarea name="Txt_Comments"  class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
						onBlur="changeBgColor(this,'#ffffff');" rows=5 cols=45 value="" tabindex="9"><%=strComments%></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center" height="30">
			     <fmtSetConsignRollback:message key="LBL_SUBMIT" var="varSubmit"/>
				<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" disabled="<%=strDisabled%>" onClick="fnRollBackSetCsg();" tabindex="2" buttonType="Save" />
			</td>
		</tr>		
		<tr><td colspan="2" height="1" class="Line"></td></tr>	
	</table>
</FORM>
</BODY>
</HTML>
