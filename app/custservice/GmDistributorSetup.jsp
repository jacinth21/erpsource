
<%
/**********************************************************************************
 * File		 		: GmDistributorSetup.jsp
 * Desc		 		: This screen is used for the Distributorship Maintenance
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="fmtDistributorSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!--custservice\GmDistributorSetup.jsp-->

<fmtDistributorSetup:setLocale value="<%=strLocale%>"/>
<fmtDistributorSetup:setBundle basename="properties.labels.custservice.GmDistributorSetup"/>


<%

	String strWikiTitle = GmCommonClass.getWikiTitle("FIELD_SALES_SETUP");
    String strCustServiceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE"); 

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String applDateFmt = strGCompDateFmt;
	String strAction = (String)request.getAttribute("hAction");
	if (strAction == null)
	{
		strAction = (String)session.getAttribute("hAction");
	}
	strAction = (strAction == null)?"Add":strAction;

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmLoad = new HashMap();

	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";

	ArrayList alRegion = new ArrayList();
	ArrayList alState = new ArrayList();
	ArrayList alCountry = new ArrayList();
	ArrayList alDist = new ArrayList();
	ArrayList alDistTyp = new ArrayList();		
	ArrayList alPartyPric = new ArrayList();
	ArrayList alCommType = new ArrayList();
	ArrayList alAccountList = new ArrayList();
		
	String strDistId = (String)request.getAttribute("hDistId") == null?"":(String)request.getAttribute("hDistId");
	String strDistNm = "";
	String strDistNmEn="";
	String strDistShortNm = "";
	String strRegion = "";
	String strDistNames = "";
	String strContactPerson = "";
	String strBillName = "";
	String strShipName = "";
	String strBillAdd1 = "";
	String strShipAdd1 = "";
	String strBillAdd2 = "";
	String strShipAdd2 = "";
	String strBillCity = "";
	String strShipCity = "";
	String strBillState = "";
	String strShipState = "";
	String strBillCountry = "";
	String strShipCountry = "";
	String strBillZipCode = "";
	String strShipZipCode = "";
	String strPhone = "";
	String strFax = "";
	String strActiveFl = "N";
	java.sql.Date dtStartDate = null;
	java.sql.Date dtEndDate = null;
	String strComments = "";
	String strCommPercent = "";
	String strDistTyp="";
	String strPartyPric="";
	String strCommissionType = "";
	String strAccountId = "";
	String strSkipRFS = GmCommonClass.parseNull((String)request.getAttribute("hAccess"));
	HashMap hmDistributorDetails = new HashMap();
	
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean =
	        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
		
	String strShowFieldSalesNmEn = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHOW_DIST_NAME_EN"));

	if (hmReturn != null)
	{
		hmLoad = (HashMap)hmReturn.get("DISTLOAD");
		alRegion = (ArrayList)hmLoad.get("REGION"); 
		alState = (ArrayList)hmLoad.get("STATE");
		alCountry = (ArrayList)hmLoad.get("COUNTRY");
		alDist = (ArrayList)hmReturn.get("DISTLIST");
		alCommType = GmCommonClass.parseNullArrayList((ArrayList)hmLoad.get("COMNTYPE"));
		alDistTyp = GmCommonClass.parseNullArrayList((ArrayList)hmLoad.get("DISTTYP"));		
		alPartyPric= GmCommonClass.parseNullArrayList((ArrayList)hmLoad.get("PARTYPRIC"));
		alAccountList = GmCommonClass.parseNullArrayList((ArrayList)hmLoad.get("ACCLIST"));
		
	}

	if (strAction.equals("EditLoad"))
	{
		hmDistributorDetails = (HashMap)hmReturn.get("EDITLOAD");

		if (hmDistributorDetails != null )
		{
			strDistNm = (String)hmDistributorDetails.get("NAME");
			strDistNmEn = (String)hmDistributorDetails.get("NAMEEN");
			strDistShortNm = GmCommonClass.parseNull((String)hmDistributorDetails.get("SHNAME"));
			strRegion = (String)hmDistributorDetails.get("REGION");
			strDistNames = (String)hmDistributorDetails.get("INCHARGE");
			strContactPerson = (String)hmDistributorDetails.get("CPERSON");
			strCommPercent = (String)hmDistributorDetails.get("CPERCENT");
			strDistTyp= (String)hmDistributorDetails.get("DISTTYP");
			strPartyPric= (String)hmDistributorDetails.get("PARTYPRIC");
			strAccountId = GmCommonClass.parseZero((String)hmDistributorDetails.get("ICTACCID"));
			
			strBillName = (String)hmDistributorDetails.get("BNAME");
			strShipName = (String)hmDistributorDetails.get("SNAME");
			strBillAdd1 = (String)hmDistributorDetails.get("BADD1");
			strShipAdd1 = (String)hmDistributorDetails.get("SADD1");
			strBillAdd2 = (String)hmDistributorDetails.get("BADD2");
			strShipAdd2 = (String)hmDistributorDetails.get("SADD2");

			strBillCity = (String)hmDistributorDetails.get("BCITY");
			strShipCity = (String)hmDistributorDetails.get("SCITY");
			strBillState = (String)hmDistributorDetails.get("BSTATE");
			strShipState = (String)hmDistributorDetails.get("SSTATE");
			strBillCountry = (String)hmDistributorDetails.get("BCOUNTRY");
			strShipCountry = (String)hmDistributorDetails.get("SCOUNTRY");
			strBillZipCode = (String)hmDistributorDetails.get("BZIP");
			strShipZipCode = (String)hmDistributorDetails.get("SZIP");
			strPhone = (String)hmDistributorDetails.get("PHONE");
			strFax = (String)hmDistributorDetails.get("FAX");
			dtStartDate = (java.sql.Date)hmDistributorDetails.get("STARTDATE");
			dtEndDate = (java.sql.Date)hmDistributorDetails.get("ENDDATE");
			strActiveFl = (String)hmDistributorDetails.get("AFL");
			strComments = (String)hmDistributorDetails.get("COMMENTS");
			strChecked = strActiveFl.equals("Y")?"checked":"";
			strCommissionType = GmCommonClass.parseNull((String)hmDistributorDetails.get("COMNTYPE"));
			strAction = "Edit";
			
		}
	}

	int intSize = 0;
	HashMap hcboVal = null;
	// to get the company lang id and based on that - show correct name
	// 103097 - English
	String strCompLangId = gmDataStoreVO.getCmplangid();
	String strUpdDistName = !strCompLangId.equals("103097")? strDistNm: strDistNmEn;
%>
<HTML>
<HEAD>
<TITLE> GlobusOne Enterprise Portal: Field Sales Setup </TITLE>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strCustServiceJsPath%>/GmDistributorSetup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screenOus.css">

</HEAD>
<script>
var skipRFS = '<%= strSkipRFS %>';
var lblIcpAccount = '<fmtDistributorSetup:message key="LBL_ICS_ACCOUNT"/>';
var lblPrincipalName = '<fmtDistributorSetup:message key="LBL_PRINCIPAL_NAME"/>';
var lblContactPerson = '<fmtDistributorSetup:message key="LBL_CONTACT_PERSON"/>';
var lblCommissionType = '<fmtDistributorSetup:message key="LBL_COMMISSION_TYPE"/>';
</script>
<BODY leftmargin="20" topmargin="10" onload="fnchkICT();">
<FORM name="frmDistributor" method="POST" action="<%=strServletPath%>/GmDistributorServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name ="format"  value="<%=applDateFmt%>"/>
<input type="hidden" name="hOld_DistNm" value ="<%=strDistNm%>">
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="100" valign="top">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" class="RightDashBoardHeader">&nbsp;<fmtDistributorSetup:message key="LBL_FIELD_SALES_SETUP"/></td>
						<td align="right" class=RightDashBoardHeader > 
						<fmtDistributorSetup:message key="IMG_ALT_HELP" var = "varHelp"/>
						<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
						</td>
					</tr>
					<tr><td class="Line" colspan="2"></td></tr>
					<tr height="25">
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					     <fmtDistributorSetup:message key="LBL_FIELD_SALES_LIST" var = "varSalesList"/>
						<gmjsp:label type="BoldText"  SFLblControlName="${varSalesList}" td="true"/>
						<td ><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="Cbo_DistId" />
					<jsp:param name="METHOD_LOAD" value="loadFieldSalesList" />
					<jsp:param name="WIDTH" value="400" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value="1"/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%=strUpdDistName %>" />
					<jsp:param name="CONTROL_ID_VALUE" value="<%=strDistId %>" />
					<jsp:param name="SHOW_DATA" value="10" />
					<jsp:param name="AUTO_RELOAD" value="fnLoadRep ();" />
					
				</jsp:include>
						</td>
					</tr>
					<tr class="evenshade">
						<td class="RightTextBlue" colspan="2" align="center">
							<fmtDistributorSetup:message key="LBL_CHOOSE_ONE"/>
						</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="oddshade">
						<fmtDistributorSetup:message key="LBL_FIELD_SALES_NAME" var = "varSalesName"/>
						<gmjsp:label type="MandatoryText"  SFLblControlName="${varSalesName}" td="true"/>
						<td >&nbsp;<input maxlength='255'   type="text" size="40" value="<%=strDistNm%>" name="Txt_DistNm" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex=2></td>
					</tr>
					
					
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<% 
						if (strShowFieldSalesNmEn.equals("YES")) {
					%> 
					<tr class="oddshade">
						<fmtDistributorSetup:message key="LBL_FIELD_SALES_NME_EN"  var="varFieldSalesNameEn"/>
						<gmjsp:label type="MandatoryText"  SFLblControlName="${varFieldSalesNameEn}" td="true"/>
						<td>&nbsp;<input type="text" size="50" value="<%=strDistNmEn%>" name="Txt_DistNmEn" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" TabIndex="3" maxlength="250">
					</tr>
					<%
						}
 					%> 
					
					
					
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="evenshade">
						<fmtDistributorSetup:message key="LBL_FIELD_SALES_SHORT_NAME" var = "varShortName"/>
						<gmjsp:label type="BoldText"  SFLblControlName="${varShortName}" td="true"/>
						<td >&nbsp;<input maxlength='20' type="text" size="25" value="<%=strDistShortNm%>" name="Txt_Dist_Short_Nm" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3></td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="oddshade">
						<fmtDistributorSetup:message key="LBL_REGION" var = "varRegion"/>
						<gmjsp:label type="MandatoryText"  SFLblControlName="${varRegion}" td="true"/>
						<td class="RightText">&nbsp;<gmjsp:dropdown controlName="Cbo_Region"  seletedValue="<%=strRegion%>" value="<%=alRegion%>" 
									codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"  tabIndex="3"/></td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="evenshade">					
					<fmtDistributorSetup:message key="LBL_TYPE" var = "varType"/>
					<gmjsp:label type="MandatoryText"  SFLblControlName="${varType}" td="true"/>
					<td >&nbsp;<gmjsp:dropdown controlName="Cbo_DistTyp"  seletedValue="<%=strDistTyp%>" value="<%=alDistTyp%>" 
									codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" onChange="fnchkICT();" tabIndex="4" />
					</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="oddshade">
						<fmtDistributorSetup:message key="LBL_PRINCIPAL_NAME" var = "varPrincipalName"/>
						<gmjsp:label type="MandatoryText"  SFLblControlName="${varPrincipalName}" td="true"/>
						<td >&nbsp;<input maxlength='200' type="text" size="40" value="<%=strDistNames%>" name="Txt_DistNames"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex="5" ></td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="evenshade">
						<fmtDistributorSetup:message key="LBL_CONTACT_PERSON" var = "varContactPerson"/>
						<gmjsp:label type="MandatoryText"  SFLblControlName="${varContactPerson}" td="true"/>
						<td >&nbsp;<input maxlength='100' type="text" size="30" value="<%=strContactPerson%>" name="Txt_ContPer"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex="5"></td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="oddshade">
						<fmtDistributorSetup:message key="LBL_COMMISSION" var = "varCommission"/>
						<gmjsp:label type="BoldText"  SFLblControlName="${varCommission}" td="true"/>
						<td >&nbsp;<input type="text" maxlength='6' size="5" value="<%=strCommPercent%>" name="Txt_ComnPerc"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex="5"></td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="evenshade">
						<fmtDistributorSetup:message key="LBL_COMMISSION_TYPE" var = "varCommissionType"/>
						<gmjsp:label type="MandatoryText"  SFLblControlName="${varCommissionType}" td="true"/>
						<td class="RightText">&nbsp;<gmjsp:dropdown controlName="Cbo_CommType"  seletedValue="<%=strCommissionType%>" value="<%=alCommType%>" 
									codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"  tabIndex="6"/></td>
					</tr>
					<tr><td class="Line" colspan="2"></td></tr>
					<tr>
						<td colspan="2">
							<table cellspacing="0" cellpadding="0" width="100%" border="0">
								<tr class="ShadeRightTableCaptionBlue">
									<fmtDistributorSetup:message key="LBL_BILL_TO_ADDRESS" var = "varBillAddress"/>
									<gmjsp:label type="BoldText" SFLblControlName="${varBillAddress}" td="true" align="left" />
									<td height="23">
									<div id="Copy_Undo">									
									<a href="#" style="text-decoration: none" onClick="fnCopyAddress()" id="Copy_Addr"> <fmtDistributorSetup:message key="IMG_ALT_COPY" var = "varCopy"/><img src="<%=strImagePath%>/dhtmlxGrid/copy.gif"  alt="${varCopy}" style="border: none;" height="14" width="20"></a>
									</div>		
									</td>
									<td rowspan="15" class="Line" width="1"><img src="<%=strImagePath%>/blank.gif" width="1"></td>
									<fmtDistributorSetup:message key="LBL_SHIP_ADDRESS" var = "varShipAddress"/>
									<gmjsp:label type="BoldText" SFLblControlName="${varShipAddress}" td="true" align="left" />
									<td></td>
								</tr>
								<tr><td class="Line" colspan="5"></td></tr>
								<tr class="oddshade">
									<fmtDistributorSetup:message key="LBL_NAME" var = "varName"/>
									<gmjsp:label type="MandatoryText" width="140" SFLblControlName="${varName}" td="true"/>
									<td style="padding-left: 4px;padding-right: 9px;"><input maxlength='100' type="text" size="30" value="<%=strBillName%>" name="Txt_BillName"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex="7"></td>
									<fmtDistributorSetup:message key="LBL_NAME" var = "varName"/>
									<gmjsp:label type="BoldText" width="140" SFLblControlName="${varName}" td="true"/>
									<td style="padding-left: 5px;padding-right: 15px;"><input maxlength='100' type="text" size="30" value="<%=strShipName%>" name="Txt_ShipName"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex="14"></td>
								</tr>
								<tr><td class="LLine" colspan="5"></td></tr>
								<tr class="evenshade">
									<fmtDistributorSetup:message key="LBL_ADDRESS_LINE1" var = "varAddressLine1"/>
									<gmjsp:label type="MandatoryText"  SFLblControlName="${varAddressLine1}" td="true"/>
									<td >&nbsp;<input maxlength='100' type="text" size="30" value="<%=strBillAdd1%>" name="Txt_BillAdd1"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex="8"></td>
									<fmtDistributorSetup:message key="LBL_ADDRESS_LINE1" var = "varAddressLine1"/>
									<gmjsp:label type="BoldText"  SFLblControlName="${varAddressLine1}" td="true"/>
									<td >&nbsp;<input maxlength='100' type="text" size="30" value="<%=strShipAdd1%>" name="Txt_ShipAdd1"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex="15"></td>
								</tr>
								<tr><td class="LLine" colspan="5"></td></tr>
								<tr class="oddshade">
									<fmtDistributorSetup:message key="LBL_ADDRESS_LINE2" var = "varAddressLine2"/>
									<gmjsp:label type="BoldText"  SFLblControlName="${varAddressLine2}" td="true"/>
									<td >&nbsp;<input maxlength='100' type="text" size="30" value="<%=strBillAdd2%>" name="Txt_BillAdd2"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex="9"></td>
									<fmtDistributorSetup:message key="LBL_ADDRESS_LINE2" var = "varAddressLine2"/>
									<gmjsp:label type="BoldText"  SFLblControlName="${varAddressLine2}" td="true"/>
									<td >&nbsp;<input maxlength='100' type="text" size="30" value="<%=strShipAdd2%>" name="Txt_ShipAdd2"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex="16"></td>
								</tr>
								<tr><td class="LLine" colspan="5"></td></tr>
								<tr class="evenshade">
									<fmtDistributorSetup:message key="LBL_CITY" var = "varCity"/>
									<gmjsp:label type="MandatoryText"  SFLblControlName="${varCity}" td="true"/>
									<td >&nbsp;<input maxlength='100' type="text" size="30" value="<%=strBillCity%>" name="Txt_BillCity"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex="10"></td>
									<gmjsp:label type="BoldText"  SFLblControlName="${varCity}" td="true"/>
									<td >&nbsp;<input maxlength='100' type="text" size="30" value="<%=strShipCity%>" name="Txt_ShipCity"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex="17"></td>
								</tr>
								<tr><td class="LLine" colspan="5"></td></tr>
								<tr class="oddshade">
									<fmtDistributorSetup:message key="LBL_STATE" var = "varState"/>
									<gmjsp:label type="MandatoryText"  SFLblControlName="${varState}" td="true"/>
									<td >&nbsp;<gmjsp:dropdown controlName="Cbo_BillState"  seletedValue="<%=strBillState%>" value="<%=alState%>" 
									codeId = "CODEID"  codeName = "CODENM"   defaultValue= "[Choose One]"  tabIndex="11" /> </td>
									<gmjsp:label type="BoldText"  SFLblControlName="${varState}" td="true"/>
									<td >&nbsp;<gmjsp:dropdown controlName="Cbo_ShipState"  seletedValue="<%=strShipState%>" value="<%=alState%>" 
									codeId = "CODEID"  codeName = "CODENM"   defaultValue= "[Choose One]" tabIndex="18" /></td>
								</tr>
								<tr><td class="LLine" colspan="5"></td></tr>
								<tr class="evenshade">
									<fmtDistributorSetup:message key="LBL_COUNTRY" var = "varCountry"/>
									<gmjsp:label type="MandatoryText"  SFLblControlName="${varCountry}" td="true"/>
									<td >&nbsp;<gmjsp:dropdown controlName="Cbo_BillCountry"  seletedValue="<%=strBillCountry%>" value="<%=alCountry%>" 
									codeId = "CODEID"  codeName = "CODENM"   defaultValue= "[Choose One]" tabIndex="12" /></td>
									
									<gmjsp:label type="BoldText"  SFLblControlName="${varCountry}" td="true"/>
									<td >&nbsp;<gmjsp:dropdown controlName="Cbo_ShipCountry"  seletedValue="<%=strShipCountry%>" value="<%=alCountry%>" 
									codeId = "CODEID"  codeName = "CODENM"   defaultValue= "[Choose One]" tabIndex="19" /></td>
								</tr>
								<tr><td class="LLine" colspan="5"></td></tr>
								<tr class="oddshade">
									<fmtDistributorSetup:message key="LBL_ZIP_POSTAL_CODE" var = "varPostalCode"/>
									<gmjsp:label type="MandatoryText"  SFLblControlName="${varPostalCode}" td="true"/>
									<td >&nbsp;<input maxlength='10' type="text" size="10" value="<%=strBillZipCode%>" name="Txt_BillZipCode"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex="13"></td>
									
									<gmjsp:label type="BoldText"  SFLblControlName="${varPostalCode}" td="true"/>
									<td >&nbsp;<input maxlength='10' type="text" size="10" value="<%=strShipZipCode%>" name="Txt_ShipZipCode"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex="20"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td class="Line" colspan="2"  ></td></tr>
					<tr class="evenshade">
						<fmtDistributorSetup:message key="LBL_PHONE" var = "varPhone"/>
						<gmjsp:label type="BoldText"  SFLblControlName="${varPhone}" td="true"/>
						<td >&nbsp;<input maxlength='20' type="text" size="20" value="<%=strPhone%>" name="Txt_Phn"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex="21"></td>
					</tr>
					<tr><td class="LLine" colspan="2"  ></td></tr>
					<tr class="oddshade">
						<fmtDistributorSetup:message key="LBL_FAX" var = "varFax"/>
						<gmjsp:label type="BoldText"  SFLblControlName="${varFax}" td="true"/>
						<td >&nbsp;<input maxlength='20' type="text" size="20" value="<%=strFax%>" name="Txt_Fax"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex="22" ></td>
					</tr>
					<tr><td class="LLine" colspan="2"  ></td></tr>
					<tr class="evenshade">
						<fmtDistributorSetup:message key="LBL_CONTRACT_START_DATE" var = "varStartDate"/>
						<gmjsp:label type="MandatoryText"  SFLblControlName="${varStartDate}" td="true"/>
						<td >&nbsp;<gmjsp:calendar textControlName="Txt_StartDate" textValue="<%=dtStartDate%>" tabIndex="23"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;						
						</td>
					</tr>
					<tr><td class="LLine" colspan="2"  ></td></tr>
					<tr class="oddshade">
						<fmtDistributorSetup:message key="LBL_CONTRACT_END_DATE" var = "varEndDate"/>
						<gmjsp:label type="BoldText"  SFLblControlName="${varEndDate}" td="true"/>
						<td >&nbsp;<gmjsp:calendar textControlName="Txt_EndDate" textValue="<%=dtEndDate%>" tabIndex="24"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
						</td>
					</tr>
					<tr><td class="LLine" colspan="2"  ></td></tr>
					<tr class="evenshade">
						<fmtDistributorSetup:message key="LBL_ACTIVE" var = "varActive"/>
						<gmjsp:label type="BoldText"  SFLblControlName="${varActive}" td="true"/>
						<td >&nbsp;<input type="checkbox" size="30" <%=strChecked%> name="Chk_ActiveFl" value="<%=strActiveFl%>" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex=25></td>
					</tr>
					<tr><td class="Line" colspan="2"  ></td></tr>
						<tr class="ShadeRightTableCaption">
							<td colspan="2" height="24"><fmtDistributorSetup:message key="LBL_OUS_PARAMETER" var = "varOUSParameters"/>
							<gmjsp:label type="BoldText"
									SFLblControlName="${varOUSParameters}" td="false" align="left" /></td>
						</tr>
						<tr>
							<td class="Line" colspan="5"></td>
						</tr>
						<tr class="evenshade">
							<fmtDistributorSetup:message key="LBL_PRICING" var = "varPricing"/>
							<gmjsp:label type="BoldText" SFLblControlName="${varPricing}"
								td="true" />
							<td>&nbsp;<gmjsp:dropdown controlName="Cbo_PartyPric"
									seletedValue="<%=strPartyPric%>" value="<%=alPartyPric%>"
									codeId="ID" codeName="NAME" defaultValue="[Choose One]"
									tabIndex="5" /> <span class="RightTextBlue">
									(Applicable when type is FD/ICA/ICT/ICS)</span></td>
						</tr>
						<tr>
							<td colspan="2" class="LLine" height="1"></td>
						</tr>
						<tr class="oddshade">
							<fmtDistributorSetup:message key="LBL_ICS_ACCOUNT" var = "varICSAccount"/>
							<gmjsp:label type="BoldText" SFLblControlName="${varICSAccount}"
								td="true" />
							<td>&nbsp;<gmjsp:dropdown controlName="Cbo_ICT_Account"
									seletedValue="<%=strAccountId%>" value="<%=alAccountList%>"
									codeId="ID" codeName="NAME" defaultValue="[Choose One]"
									tabIndex="5" /></td>
						</tr>
						<tr>
						<tr><td class="Line" colspan="2"  ></td></tr>
					<tr>
						<td colspan="2"> 
							<jsp:include page="/common/GmIncludeLog.jsp" >
							    <jsp:param name="Mandatory" value="yes" />
								<jsp:param name="LogType" value="" />
								<jsp:param name="LogMode" value="Edit" />
							</jsp:include>
						</td>
					</tr>
						<%if(strSkipRFS.equalsIgnoreCase("Y") ){ %>
							<% if(strAction.equalsIgnoreCase("Edit") || strAction.equalsIgnoreCase("Add")){%>
								<tr>
									<td  height="40" colspan="2" align="center">&nbsp;
									<fmtDistributorSetup:message key="BTN_SUBMIT" var = "varSubmit"/>
									<gmjsp:button style="width: 5em" value="${varSubmit}" gmClass="button" disabled="true" onClick="fnSubmit();" buttonType="Save" />&nbsp;
									<fmtDistributorSetup:message key="BTN_RESET" var = "varReset"/>
									<gmjsp:button style="width: 5em" value="${varReset}" gmClass="button" disabled="true" onClick="fnReset();" buttonType="Save" />
									<% if(strAction.equalsIgnoreCase("Edit")){%>
									<fmtDistributorSetup:message key="BTN_MAP_PARAMETERS" var = "varMapParameters"/>
									<gmjsp:button value="${varMapParameters}" name="MapParameters" controlId="MapParameters" gmClass="button" onClick="fnSkipRFS();" buttonType="Save" />
									<%}%>
									</td>
								</tr>
							<%}%>
						<%}else{%>
						   <tr>
								<td  height="40" colspan="2" align="center">&nbsp;
								<fmtDistributorSetup:message key="BTN_SUBMIT" var = "varSubmit"/>
								<gmjsp:button style="width: 5em" value="${varSubmit}" gmClass="button" onClick="fnSubmit();" buttonType="Save" />&nbsp;
								<fmtDistributorSetup:message key="BTN_RESET" var = "varReset"/>
								<gmjsp:button style="width: 5em" value="${varReset}" gmClass="button" onClick="fnReset();" buttonType="Save" />
								<fmtDistributorSetup:message key="BTN_ADD_ICT_PARAMETER" var = "varAddICTParams"/>
								<gmjsp:button value="${varAddICTParams}" name="AddICTParams" controlId="AddICTParams" gmClass="button" onClick="fnICTVar();" buttonType="Save" />
								</td>
							</tr>
					<%}%>
				</table>
			</td>
		</tr>
    </table>
</FORM>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
