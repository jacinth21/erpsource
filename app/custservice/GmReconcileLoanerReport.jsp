
<%@page import="java.util.HashMap"%>
<%
/**********************************************************************************
 * File		 		: GmReconcileLoanerReport.jsp
 * Desc		 		: Loaner Reconciliation Report
 * Version	 		: 1.0
 * author			: 
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ taglib prefix="fmtReconcileLoanerReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmReconcileLoanerReport.jsp -->
<fmtReconcileLoanerReport:setLocale value="<%=strLocale%>"/>
<fmtReconcileLoanerReport:setBundle basename="properties.labels.custservice.GmReconcileLoanerReport"/> 
<%
request.setCharacterEncoding("UTF-8");
response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8");
GmCalenderOperations gmCal = new GmCalenderOperations();
String strWikiTitle = GmCommonClass.getWikiTitle("LOANER_TRANSACTION"); 
gmCal.setTimeZone(strGCompTimeZone);
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
String strTodaysDate = gmCal.getCurrentDate(strGCompDateFmt)==null?"":gmCal.getCurrentDate(strGCompDateFmt);
String strApplnDateFmt = strGCompDateFmt;
HashMap hcboVal = new HashMap();
%>
<bean:define id="gridData" name="frmLoanerReconciliation" property="gridData" type="java.lang.String"> </bean:define>
<bean:define id="deptid" name="frmLoanerReconciliation" property="deptId" type="java.lang.String"> </bean:define>
<bean:define id="haction" name="frmLoanerReconciliation" property="haction" type="java.lang.String"> </bean:define>
<bean:define id="loanTo" name="frmLoanerReconciliation" property="loanTo" type="java.lang.String"></bean:define>
<bean:define id="alEmployeeList" name="frmLoanerReconciliation" property="alEmployeeList" type="java.util.ArrayList"></bean:define>
<bean:define id="alRepList" name="frmLoanerReconciliation" property="alRepList" type="java.util.ArrayList"></bean:define>
<bean:define id="alOUSList" name="frmLoanerReconciliation" property="alOUSList" type="java.util.ArrayList"></bean:define>
<bean:define id="alUSRepList" name="frmLoanerReconciliation" property="alUSRepList" type="java.util.ArrayList"></bean:define>

<bean:define id="strDistId" name="frmLoanerReconciliation" property="dist" type="java.lang.String"></bean:define>
<bean:define id="strDistNm" name="frmLoanerReconciliation" property="searchdist" type="java.lang.String"></bean:define>
<bean:define id="strRepId" name="frmLoanerReconciliation" property="rep" type="java.lang.String"></bean:define>
<bean:define id="strRepNm" name="frmLoanerReconciliation" property="searchrep" type="java.lang.String"></bean:define>


<HTML>
<HEAD>
<TITLE> Globus Medical: Loaner Reconciliation </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script type="text/javascript">
var deptid		= '<%=deptid%>';
var haction		= '<%=haction%>'
var todaysDate 	= '<%=strTodaysDate%>';
var format 		= '<%=strApplnDateFmt%>';
</script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmLoanerReconcilitionReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/operations/GmCommonShippingReport.js"></script>


<!-- MNTTASK-3518 Stack Overflow commented bolow line -->

<link rel="STYLESHEET" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid_form.js "></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<link rel="STYLESHEET" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="STYLESHEET" type="text/css" href="<%=strCssPath%>/displaytag.css"></link>
<script type="text/javascript">
var objGridData;
objGridData		= '<%=gridData%>';
var RepLen = <%=alRepList.size()%>;
var EmpLen = <%=alEmployeeList.size()%>;
var OUSLen = <%=alOUSList.size()%>;
<%
hcboVal = new HashMap();
for (int i=0;i<alRepList.size();i++)
{
	hcboVal = (HashMap)alRepList.get(i);
%>
var alRepListArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>

<%
hcboVal = new HashMap();
for (int i=0;i<alEmployeeList.size();i++)
{
	hcboVal = (HashMap)alEmployeeList.get(i);
%>
var alEmpListArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>

<%
hcboVal = new HashMap();
for (int i=0;i<alOUSList.size();i++)
{
	hcboVal = (HashMap)alOUSList.get(i);
%>
var alOUSListArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<html:form  action="/gmLoanerReconciliationReport.do">
<html:hidden property="strOpt" name="frmLoanerReconciliation"/>
<html:hidden property="hreqids" name="frmLoanerReconciliation"/>
<html:hidden property="requestId" name="frmLoanerReconciliation"/>
<html:hidden property="loanToName" id="loanToName" name="frmLoanerReconciliation"/>

<input type="hidden" name ="hId" />
<input type="hidden" name ="hAction" />

	<table border="0" class="DtTable1300" cellspacing="0" cellpadding="0" onkeypress="">
			<tr>
				<td height="25" colspan="3" class="RightDashBoardHeader" ><fmtReconcileLoanerReport:message key="LBL_LOANER_RECONCILIATION"/></td>			
				<td height="25" class="RightDashBoardHeader" align="right" colspan="1">
				<fmtReconcileLoanerReport:message key="IMG_HELP" var="varHelp"/>
				    <img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'  
			       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> </td>
			</tr>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<tr>
				<td height="25" class="RightTableCaption" align="Right"><fmtReconcileLoanerReport:message key="LBL_LOANER_TYPE"/>:</td>	
			    <td >&nbsp;<gmjsp:dropdown controlName="loanerType" onChange="fnLoadDist(this.value);"  SFFormName='frmLoanerReconciliation' SFSeletedValue="loanerType"  defaultValue= "[Choose One]" SFValue="alType" codeId="CODENMALT" codeName="CODENM" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			    <td class="RightTableCaption" align="Right"><fmtReconcileLoanerReport:message key="LBL_SET_NAME"/>:</td>
				<td> &nbsp;<html:text property="setName" name="frmLoanerReconciliation" size="28"/></td>
			</tr>
			<tr><td class="LLine" height="1" colspan="6"></td></tr>
			<tr class="Shade" id="IHL">			
				<td height="25" class="RightTableCaption" align="Right"><fmtReconcileLoanerReport:message key="LBL_LOAN_TO"/>:</td>
				<td>&nbsp;<gmjsp:dropdown controlName="loanTo" SFFormName="frmLoanerReconciliation" SFSeletedValue="loanTo" defaultValue= "[Choose One]" 
								SFValue="alEventLoanToList" codeId="CODEID" codeName="CODENM" onChange="javascript:fnLoadLoanToNames(this);"/></td>		
				<td height="25" class="RightTableCaption" align="Right"><fmtReconcileLoanerReport:message key="LBL_LOAN_TO_NAME"/>:</td>
				<%-- <% if(loanTo.equals("19518") ){%> --%>	
				<td id="us_rep" style="display: block">&nbsp;<gmjsp:dropdown controlId="usLoanToName" controlName="usLoanToName" SFFormName="frmLoanerReconciliation" SFSeletedValue="usLoanToName" defaultValue= "[Choose One]" 
								SFValue="alUSRepList" codeId="ID" codeName="NAME" /></td>
			    <%-- <%}else if(loanTo.equals("19523") ){%> --%>
			    <td id="ous_rep" style="display: none">&nbsp;<gmjsp:dropdown  controlId="ousLoanToName" controlName="ousLoanToName" SFFormName="frmLoanerReconciliation" SFSeletedValue="ousLoanToName" defaultValue= "[Choose One]" 
								SFValue="alOUSList" codeId="ID" codeName="NAME" /></td>
				<%-- <%}else{%> --%>
				<td id="emp_name" style="display: none">&nbsp;<gmjsp:dropdown  controlId="empLoanToName" controlName="empLoanToName" SFFormName="frmLoanerReconciliation" SFSeletedValue="empLoanToName" defaultValue= "[Choose One]" 
								SFValue="alEmployeeList" codeId="ID" codeName="NAME" /></td>
				<%-- <%} %> --%>
				
			</tr>
			<tr class="Shade" id="PLL" style="display: none;">
				<td height="25" class="RightTableCaption" align="Right"><fmtReconcileLoanerReport:message key="LBL_FIELD_SALES"/>:</td>
				<td style="margin-top: -9px; position:absolute;">&nbsp;
				<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
				<jsp:param name="CONTROL_NAME" value="dist" />
				<jsp:param name="METHOD_LOAD" value="loadFieldSalesList&searchType=LikeSearch" />
				<jsp:param name="WIDTH" value="300" />
				<jsp:param name="CSS_CLASS" value="search" />
				<jsp:param name="TAB_INDEX" value=""/>
				<jsp:param name="CONTROL_NM_VALUE" value="<%=strDistNm %>" /> 
				<jsp:param name="CONTROL_ID_VALUE" value="<%=strDistId %>" />
				<jsp:param name="SHOW_DATA" value="50" />
				<jsp:param name="AUTO_RELOAD" value="" />
				</jsp:include>
				<%-- <gmjsp:dropdown controlName="dist"  SFFormName='frmLoanerReconciliation' SFSeletedValue="dist" defaultValue= "[Choose One]"	
					SFValue="alDist" codeId="ID" codeName="NAME" /> --%>
				</td>		
				<td height="25" class="RightTableCaption" align="Right"><fmtReconcileLoanerReport:message key="LBL_SALES_REP"/>:</td>	
				<td style="margin-top: -9px; position:absolute;">&nbsp;
					<%-- <gmjsp:dropdown controlName="rep"  SFFormName='frmLoanerReconciliation' SFSeletedValue="rep" width="255" defaultValue= "[Choose One]" SFValue="alRepList" codeId="ID" codeName="NM"/> --%>
				<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
				<jsp:param name="CONTROL_NAME" value="rep" />
				<jsp:param name="METHOD_LOAD" value="loadSalesRepList&searchType=LikeSearch" />
				<jsp:param name="WIDTH" value="300" />
				<jsp:param name="CSS_CLASS" value="search" />
				<jsp:param name="TAB_INDEX" value=""/>
				<jsp:param name="CONTROL_NM_VALUE" value="<%=strRepNm %>" /> 
				<jsp:param name="CONTROL_ID_VALUE" value="<%=strRepId %>" />
				<jsp:param name="SHOW_DATA" value="50" />
				<jsp:param name="AUTO_RELOAD" value="" />
				</jsp:include>
				</td> 
			</tr>
			<tr><td class="LLine" height="1" colspan="6"></td></tr>
			<tr>
				<td height="25" class="RightTableCaption" align="Right"><fmtReconcileLoanerReport:message key="LBL_FROM_DATE"/>:</td>
				<td align="left" colspan="1" class="RightTableCaption">&nbsp;<gmjsp:calendar SFFormName="frmLoanerReconciliation" controlName="fromDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtReconcileLoanerReport:message key="LBL_TO_DATE"/>:&nbsp;<gmjsp:calendar SFFormName="frmLoanerReconciliation" controlName="toDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" /> </td>
				<td height="25" class="RightTableCaption" align="Right"><fmtReconcileLoanerReport:message key="LBL_STATUS"/>:</td>			
				<td >&nbsp;<gmjsp:dropdown controlName="status"  SFFormName='frmLoanerReconciliation' SFSeletedValue="status"  defaultValue= "[Choose One]"	SFValue="alStatus" codeId="CODEID" codeName="CODENM" />  
				&nbsp;&nbsp;<fmtReconcileLoanerReport:message key="BTN_LOAD" var="varLoad"/>
				<gmjsp:button name="Btn_Load" value="${varLoad}" gmClass="button" style="width: 5em;height: 23" onClick="fnLoad();" buttonType="Load" /></td> 
			</tr>		
				<%if(gridData.indexOf("cell") != -1){ %>	
				<tr>
					<td colspan="6"><div id="gridData"   height = "462" width="1300" ></div> 
						<div id="pagingArea" style="width=1230" ></div>
					</td>
				</tr>
				<tr>
					<td colspan="6" align="center">
				        <div class='exportlinks'><fmtReconcileLoanerReport:message key="LBL_EXPORT_OPTION"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnDownloadExcel();"><fmtReconcileLoanerReport:message key="LBL_EXCEL"/>  </a>&nbsp; </div>
				    </td>
			   </tr>
			   <tr>
					<td align="center"  height="25" valign="middle" colspan="6">
					<fmtReconcileLoanerReport:message key="BTN_CREATE_TICKET" var="varCreateTicket"/>
					<gmjsp:button value="&nbsp;${varCreateTicket}&nbsp;" name="Btn_CrTi" gmClass="button" style="width: 9em;height: 22"  onClick="fnSendEmail();"  buttonType="Save" />&nbsp;<BR><BR></td> <!-- tabindex="25" -->
			   </tr>
			   <%}else{ %>
			   <tr><td class="LLine" height="1" colspan="6"></td></tr>
			   <tr><td colspan="13" class="RightTextRed" align="center"><fmtReconcileLoanerReport:message key="LBL_NOTHING_FOUND_TO_DISPLAY"/>.</td></tr>
			   <%}%>
	</table>	
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>

