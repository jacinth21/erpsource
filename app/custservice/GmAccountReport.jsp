<!--\CS Label changes\custservice\GmAccountReport.jsp  -->
 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmAccountReport.jsp
 * Desc		 		: This screen is used for the Account Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
 * Modified			: Gopinathan for change report to DHTMLX
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtAccountReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<fmtAccountReport:setLocale value="<%=strLocale%>"/>
<fmtAccountReport:setBundle basename="properties.labels.custservice.GmAccountReport"/>
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strCustomerServiceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strWikiTitle = GmCommonClass.getWikiTitle("ACCOUNTS_REPORT");
	String strOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));	//
	String strUSFilter = GmCommonClass.parseNull((String)request.getAttribute("hUSFilter"));
	String strAccId =  GmCommonClass.parseZero((String)request.getAttribute("hAccId"));
	// When without select the account name - to setting the account id empty 
	strAccId = strAccId.equals("0")? "": strAccId;
	String strHeader = "";
	
	ArrayList alGpo = new ArrayList();
	ArrayList alAccount = new ArrayList();
	alGpo = (ArrayList)request.getAttribute("alGpo");//
	alAccount = (ArrayList)request.getAttribute("ACCOUNTLIST"); // Accounts List
	String strDeptId = (String)session.getAttribute("strSessDeptId")==null?"":(String)session.getAttribute("strSessDeptId");//
	String strAccessLvl = (String)session.getAttribute("strSessAccLvl")==null?"":(String)session.getAttribute("strSessAccLvl");
	String strHideButton = (String)session.getAttribute("HideButton")==null?"":(String)session.getAttribute("HideButton");
	String strParentFl = GmCommonClass.parseNull((String)request.getAttribute("Chk_ParentFl"));
	String strGridData = GmCommonClass.parseNull((String)request.getAttribute("gridData"));
	
	String strARRptByDealerFlag = GmCommonClass.parseNull((String)request.getAttribute("ARRptByDealerFlag"));
	int intAccessLvl = Integer.parseInt(strAccessLvl);
	boolean bolAccess = true;
	GmResourceBundleBean gmResourceBundleBeanlbl = 
	    GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmAccountReport", strSessCompanyLocale);
	if ((strDeptId.equals("S") && intAccessLvl < 5))
	{
		bolAccess = false;
	}
	if (strOpt.equals("ACCTRPT")){
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ACCOUNT_REPORT"));
	}else{
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_REPORT_ALL_ACCOUNT"));
	}
	String strGpoId = GmCommonClass.parseNull((String)request.getAttribute("hGpoId"));
	//Below code is to track down the visits on the report.
	String strUserId = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
	String strShUserName = GmCommonClass.parseNull((String)session.getAttribute("strSessShName"));
	String strUserName = strUserId + " - " + strShUserName;
	String strAccountCurrency = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_ID"));
	ArrayList alCompCurrency = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCOMPANYCURRENCY"));
	String strAccountName = GmCommonClass.parseNull((String) request.getAttribute("SEARCH_ACC_NAME"));
	//Visit code ends
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Account Report </TITLE>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strCustomerServiceJsPath%>/GmAccountReport.js"></script>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">

<script>
var objGridData;
objGridData ='<%=strGridData%>';
var strOpt = '<%=strOpt%>';
var parentFl = '<%=strParentFl%>';
var compCurrSign = '<%=strAccountCurrency%>';
var arRptByDealerFlag = '<%=strARRptByDealerFlag%>';

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10"  onLoad="fnOnPageLoad();">
<FORM name="frmAccount" method="POST" action="/GmAccountReportServlet">
<input type="hidden" name="hOpt" value="<%=strOpt %>">
<input type="hidden" name="hAction" value="">
	<table class="DtTable1600" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><%=strHeader %></td>
			<td  height="25" class="RightDashBoardHeader">
			
			<fmtAccountReport:message key="IMG_ALT_HELP" var="varHelp"/>
						<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
		</tr>
		<!-- Addition Filter Information -->
		<tr><td colspan="4">
		<% if(strOpt.equals("ACCTRPT")){ %>
				<jsp:include page="/sales/GmSalesFilters.jsp" >
				<jsp:param name="FRMNAME" value="frmAccount" />
				<jsp:param name="HIDE" value="SYSTEM" />
				<jsp:param name="HACTION" value="Reload"/>
				</jsp:include>
		<% }else{ %>
				<jsp:include page="/sales/GmSalesFilters.jsp" >
				<jsp:param name="FRMNAME" value="frmAccount" />
				<jsp:param name="HIDE" value="SYSTEM" />
				</jsp:include>
		<% } %>
		</td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td height="25" class="RightTablecaption" width="100%">

<%
		if (bolAccess && strOpt.equals("ALL"))
		{
%>
					&nbsp;&nbsp;<fmtAccountReport:message key="LBL_GROUP_PRICE_BOOK"/>:&nbsp;<gmjsp:dropdown controlName="Cbo_GpoId" seletedValue="<%= strGpoId %>" 	
							tabIndex="2"  width="150" value="<%= alGpo%>" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]"  />
							&nbsp;&nbsp;&nbsp;&nbsp;
		<fmtAccountReport:message key="LBL_LOAD" var="varLoad"/>
		<gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="Button" style="width: 5em; height: 23px;" onClick="javascript:fnSubmit();" buttonType="Load" />
<%
			} else if (bolAccess && strOpt.equals("ACCTRPT")){
%>				<table ><tr><td align="right" class="RightTableCaption">		
				&nbsp;&nbsp;&nbsp;&nbsp;<fmtAccountReport:message key="LBL_REP_ACCOUNT"/>:</td><td align="left" width="400px"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
										<jsp:param name="CONTROL_NAME" value="Cbo_AccId" />
										<jsp:param name="METHOD_LOAD" value="loadAccountList" />
										<jsp:param name="WIDTH" value="400" />
										<jsp:param name="CSS_CLASS" value="search" />
										<jsp:param name="CONTROL_NM_VALUE" value="<%=strAccountName %>" /> 
										<jsp:param name="CONTROL_ID_VALUE" value="<%=strAccId %>" />
										<jsp:param name="SHOW_DATA" value="100" />
										<jsp:param name="AUTO_RELOAD" value="fnDisplayAccID(this);" />					
									</jsp:include></td><td align="left">&nbsp;<input type="text" size="8" value="<%=strAccId%>" name="Txt_AccId" class="InputArea" 
			           onFocus="changeBgColor(this,'#AACCE8');" onChange="fnDisplayAccNm(this)" >	
							&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td><gmjsp:dropdown controlName="Cbo_Comp_Curr"  seletedValue="<%=strAccountCurrency%>" value="<%=alCompCurrency%>" codeId = "ID"  codeName = "NAMEALT" optionId="NAME"/></td>
		       <fmtAccountReport:message key="LBL_LOAD" var="varLoad"/>
		       <td><gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="Button" style="width: 5em; height: 23px;" onClick="javascript:fnSubmit();" buttonType="Load" /></td><tr></table>
<%
			}
%>	
			</td>
		</tr>
		<%
		if (bolAccess && strOpt.equals("ACCTRPT"))
		{
%>
		<tr height="35">
		<td colspan="1" class="RightTablecaption" width = "45%" align="center"><fmtAccountReport:message key="LBL_SHOW_PARENT_ACCOUNT"/>:&nbsp;
		<input type="checkbox" size="30" name="Chk_ParentFl" checked="<%=strParentFl%>" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" >					
		</td>
		</tr>
	<%
			}
%>	
		
		<%
		if (strGridData.indexOf("cell") != -1) {
		%>
		   	<tr>
       	 		<td colspan="9" >
					<div id="GridData" style="grid" height="500px" width="100%"></div>  
		    	</td>
           	</tr>
           	<tr>
               <td colspan="6" align="center">
                <div class='exportlinks'><fmtAccountReport:message key="DIV_EXPORT_OPT"/>:<img src='img/ico_file_excel.png'/>&nbsp;<a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> <fmtAccountReport:message key="DIV_EXCEL"/> </a>
                               <!-- 
                                |<img src='<%=strImagePath%>/pdf_icon.gif' />&nbsp;<a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a>
                                 -->
                </div>
                </td>
			</tr>
			<%
			}
			%>
			
</table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>
<%@ include file="/common/GmFooter.inc" %>
</HTML>
