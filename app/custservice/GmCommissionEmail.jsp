 
 <!--  \CS Label changes\custservice\GmCommissionEmail.jsp-->
 <%
/**********************************************************************************
 * File		 		: GmCommissionEmail.jsp
 * Desc		 		: This screen is used for preparing the Commisioning e-mails
 * author			: Elango
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib uri="com.corda.taglib" prefix="ctl" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtCommissionEmail" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtCommissionEmail:setLocale value="<%=strLocale%>"/>
<fmtCommissionEmail:setBundle basename="properties.labels.custservice.GmCommissionEmail"/>
<%

HashMap hmParamDetails = new HashMap();
String strWikiTitle = GmCommonClass.getWikiTitle("COMMISSION_EMAIL");

HashMap hmEmailDetails = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("EMAILDETAILS"));
String strAppCurFmt = GmCommonClass.parseNull((String)request.getAttribute("APPLCURRFMT"));
String strAppCurSym = GmCommonClass.parseNull((String)request.getAttribute("ARCURRSYMBOL"));
ArrayList alPOList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("EMAILPOLIST"));
String hInputOrdStr = GmCommonClass.parseNull((String)request.getAttribute("hInputOrdStr"));
String hEmailAction = GmCommonClass.parseNull((String)request.getAttribute("hEmailAction"));
hmParamDetails = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmParam"));

String strFromDate = GmCommonClass.parseNull((String)request.getAttribute("FROMDATE"));
String strToDate = GmCommonClass.parseNull((String)request.getAttribute("TODATE"));

String strAgingType = GmCommonClass.parseNull((String)hmParamDetails.get("TYPE"));
/* java.util.Date frmDate = (java.util.Date)hmParamDetails.get("FRMDT");
java.util.Date toDate = (java.util.Date)hmParamDetails.get("TODT"); */
String strDistType = GmCommonClass.parseNull((String)hmParamDetails.get("DTYPE"));
String strReportType = GmCommonClass.parseNull((String)hmParamDetails.get("REPORTTYPE"));
String strMasterId = GmCommonClass.parseNull((String)hmParamDetails.get("MASTERID"));
String strCollecId = GmCommonClass.parseNull((String)hmParamDetails.get("COLLECTORID"));
String strCompId = GmCommonClass.parseNull((String)hmParamDetails.get("COMPANYID"));
String strOrdType = GmCommonClass.parseNull((String)hmParamDetails.get("ORDERTYPE"));
String strAccountCurrency = GmCommonClass.parseNull((String)hmParamDetails.get("ARCURRID"));
String strDueDays = GmCommonClass.parseNull((String)request.getAttribute("DEFAULTDUEDAYS"));

String strEmailTo = GmCommonClass.parseNull((String)hmEmailDetails.get("EMAILTO"));
String strEmailCc = GmCommonClass.parseNull((String)hmEmailDetails.get("EMAILCC"));
String strEmailSubject = GmCommonClass.parseNull((String)hmEmailDetails.get("EMAILSUBJECT"));
String strEmailMessage = GmCommonClass.parseNull((String)hmEmailDetails.get("EMAILMESSAGE"));
//Replacing the commas, spaces if any in the TO, CC id's.
strEmailTo = (strEmailTo.startsWith(","))?strEmailTo.substring(1, strEmailTo.length()) : strEmailTo;
strEmailTo = (strEmailTo.endsWith(","))?strEmailTo.substring(0, strEmailTo.length()-1) : strEmailTo;
strEmailCc = (strEmailCc.startsWith(","))?strEmailCc.substring(1, strEmailCc.length()) : strEmailCc;
strEmailCc = (strEmailCc.endsWith(","))?strEmailCc.substring(0, strEmailCc.length()-1) : strEmailCc;
strEmailSubject = (strEmailSubject.endsWith("-"))?strEmailSubject.substring(0, strEmailSubject.length()-1) : strEmailSubject;
strEmailSubject = strEmailSubject.replaceAll("-"," - ");
int intLength = alPOList.size();

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Pending Orders (PO) Commission Reminder Email </TITLE>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" type="text/javascript" src="<%=strExtWebPath%>/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
		tinyMCE.init({
			mode : "specific_textareas",
		    editor_selector : "HtmlTextArea",
			theme : "advanced" ,
			theme_advanced_path : false,
			height : "200",
			width: "620" 
		});
</script> 
<script>

function fnBack(){
	fnStartProgress("Y");
	window.history.go(-1);
}
/*Below function used to validaet the Email Ids entered in the UI.
Input Object - Input Object which has to be validaetd
delimiter - This is the Email ID delimiter in the Field. 
Errmessage field- This is the Field control to be dispalyed Error msg.
EMail ID Format: anystring@anystring.anystring */
function validateEmails(obj, delimiter, errField) {
	var objRegExp = /\S+@\S+\.\S+/;
	var toemail = obj.split(delimiter);
	var errMailIDs='';
	var errFl = false;
	for(var i=0;i<toemail.length;i++){
		toemail[i] = (toemail[i].replace(/^\s+/,'')).replace(/\s+$/,'');
		if(toemail[i] != '' && toemail[i].search(objRegExp) == -1){			
			errFl = true;
			errMailIDs =  errMailIDs + toemail[i] +' ,';
		}
	}
	if(errFl){
		var array = [errField,errMailIDs.substr(0,errMailIDs.length-1)]
		Error_Details(Error_Details_Trans(message_operations[703],array));
	}
}

function fnSendMail(){
	
		validateEmails( document.frmAccount.emailTo.value, "," ,message_operations[704]);
		validateEmails( document.frmAccount.emailCC.value, "," ,message_operations[705]);
	
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}else{
			var orderHtmlstring = document.getElementById('gridData');
			document.frmAccount.hHtmlOrderDetails.value = orderHtmlstring.innerHTML;
			document.frmAccount.hAction.value = 'send_commsn_email';
			document.frmAccount.Btn_Email.disabled=true;
			fnStartProgress("Y");
			document.frmAccount.submit();
		}
}
</script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     .ui-richtextarea-content {overflow:auto; height: 100%; width:100%; border-left : 1 solid black;	border-right : 1 solid black;	border-top : 1 solid black;	border-bottom : 1 solid black;}
</style>

</HEAD>

<BODY leftmargin="20" topmargin="10" >
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmPendingOrdersPOServlet">
<input type="hidden" name="hHtmlOrderDetails" value="">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hInputOrdStr" value="<%=hInputOrdStr%>">
<input type="hidden" name="hEmailAction" value="<%=hEmailAction%>">

<input type="hidden" name="hAgingType" value="<%=strAgingType%>">
<input type="hidden" name="hFromDate" value="<%=strFromDate%>">
<input type="hidden" name="hToDate" value="<%=strToDate%>">
<input type="hidden" name="hDistType" value="<%=strDistType%>">
<input type="hidden" name="hReportType" value="<%=strReportType%>">
<input type="hidden" name="hMasterId" value="<%=strMasterId%>">
<input type="hidden" name="hCollecId" value="<%=strCollecId%>">
<input type="hidden" name="hOdrType" value="<%=strOrdType%>">
<input type="hidden" name="hCompID" value="<%=strCompId%>">
<input type="hidden" name="hduedays" value="<%=strDueDays%>">
<input type="hidden" name="hAccCurrency" value="<%=strAccountCurrency%>">
 

	<table border="0" class="dttable950" cellspacing="0" cellpadding="0">
		<tr><td colspan="6" bgcolor="#666666"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="5"></td>
			<td valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr height="25"><td colspan="2"><table border="0" width="100%" cellspacing="0" cellpadding="0"><tr>
						<td height="25" class="RightDashBoardHeader" width="800" align="left"><fmtCommissionEmail:message key="LBL_COMMISSION_REMAINDER_EMAIL"/></td>
						<td height="25" class=RightDashBoardHeader align="right"  >
						<fmtCommissionEmail:message key="IMG_ALT_HELP" var="varHelp"/>
						<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' 
						                  onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
					</tr></table></td></tr>
					<tr><td class="Line" colspan="2"></td></tr>
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr>
						<td height="30"  colspan="1" align="right" width="300"><b><fmtCommissionEmail:message key="LBL_TO"/>:</b>&nbsp;</td>
						<td colspan="1" height="30" ><textarea name="emailTo" name="To" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
								onBlur="changeBgColor(this,'#ffffff');"  rows=3 cols=100><%=strEmailTo%></textarea></td>
					</tr>
					<tr>
						<td height="30" colspan="1" align="right" width="300"><b><fmtCommissionEmail:message key="LBL_CC"/>:</b>&nbsp;</td>
						<td colspan="1" height="30"><textarea name="emailCC" name="Cc" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
								onBlur="changeBgColor(this,'#ffffff');"  rows=3 cols=100><%=strEmailCc%></textarea></td>
					</tr>				
					<tr>
						<td height="30" colspan="1" align="right" width="300"><b><fmtCommissionEmail:message key="LBL_SUBJECT"/>:</b>&nbsp;</td>
						<td colspan="1" height="30" width="300"><input type="text" name="emailSubject"  onFocus="changeBgColor(this,'#AACCE8');" value="<%= strEmailSubject%>"
									   onBlur="changeBgColor(this,'#ffffff');"  size="100" width="300px"/></td>
					</tr>
					<tr>
						<td height="30" colspan="1" align="right" width="310"><b><fmtCommissionEmail:message key="LBL_EMAIL_MESSAGE"/>:</b>&nbsp;</td>
						<td colspan="1" height="30"  width="400"><textarea name="emailMessage"  class="HtmlTextArea" onFocus="changeBgColor(this,'#AACCE8');" 
								onBlur="changeBgColor(this,'#ffffff');"  rows=10 cols=100 ><%= strEmailMessage%></textarea></td>
					</tr>
					<tr  height="200px">
						<td colspan="2" align="center">&nbsp;
						<div id="gridData" style="width: 950px; height: 200px; overflow-y: scroll;" >
						<table border="1"  bordercolr="#676767" cellspacing="0" cellpadding="0">
						<tr class="ShadeRightTableCaption" HEIGHT="24" style="position:relative; top:expression(this.offsetParent.scrollTop);">
							<td width="275"><fmtCommissionEmail:message key="LBL_ACCOUNT_NAME"/></td>
							<td width="175"><fmtCommissionEmail:message key="LBL_ORDER_ID"/></td>
						<% 	if(hEmailAction.equals("4000627")){ // 4000627 - Held Order Commission E-mail %>
							<td width="160"><fmtCommissionEmail:message key="LBL_CUSTOMER_PO"/></td>
							<%}%>
							<td width="100"><fmtCommissionEmail:message key="LBL_ORDER_DATE"/></td>
							<td width="150"><fmtCommissionEmail:message key="LBL_AMOUNT"/></td>
							<td width="250"><fmtCommissionEmail:message key="LBL_SALES_REP"/>Sales Rep</td>
						</tr>
						
						<%
						
						if (intLength > 0)
						{
							HashMap hmLoop = new HashMap();

							String strOrdId = "";
							String strAccName = "";
							String strOrdDate = "";
							String strRepNm = "";
							String strOrdAmount = "";
							String strShade="";
							String strCustPO="";
							double dbSales = 0.0;		
							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = GmCommonClass.parseNullHashMap((HashMap)alPOList.get(i));
								
								strOrdId = GmCommonClass.parseNull((String)hmLoop.get("ORDID"));
								strCustPO = GmCommonClass.parseNull((String)hmLoop.get("CUSTPO"));
								strAccName = GmCommonClass.parseNull((String)hmLoop.get("ANAME")); 
								strOrdDate = GmCommonClass.getStringFromDate((java.util.Date) hmLoop.get("ORDDT"), strGCompDateFmt); 
								
								strRepNm = GmCommonClass.parseNull((String)hmLoop.get("RNAME"));
								strOrdAmount = GmCommonClass.parseNull((String)hmLoop.get("ORDAMT"));
								dbSales = Double.parseDouble(strOrdAmount);
								strOrdAmount =""+dbSales;
								strShade	= (i%2 != 0)?"class=ShadeBlue":"";
										
								%>	
								<tr <%=strShade%> >
								<td class="RightText" align="left"><%=strAccName%></td>
								<td class="RightText" align="left"><%=strOrdId%></td>
								<% 	if(hEmailAction.equals("4000627")){ // 4000627 - Held Order Commission E-mail %>
								<td class="RightText" align="left">&nbsp;<%=strCustPO%></td>
								<%}%>
								<td class="RightText" align="right">&nbsp;<%=strOrdDate%></td>
								<td class="RightText" align="right"><%=strAppCurSym%>
								<%=GmCommonClass.getStringWithCommas(strOrdAmount,strAppCurFmt)%></td>
								<td class="RightText" align="left"><%=strRepNm%></td>								
								</tr>
				<%
							}
						}else{					
						
						%>
						<tr>
						<td colspan="9" class="RightTextRed" height="50" align=center><fmtCommissionEmail:message key="LBL_NO_CHARGES_PENDING_PO"/> </td>
						</tr>
						<%} %>
						</table></div></td>
					</tr>
					<tr><td colspan="2">&nbsp;&nbsp;</td></tr>
					<tr>
						<td colspan="2" height="30" align="center">
						<fmtCommissionEmail:message key="LBL_BACK" var="varBack"/>
							<gmjsp:button value="&nbsp;${varBack}&nbsp;" name="Btn_Back" gmClass="button" buttonType="Load" onClick="fnBack();" />&nbsp;&nbsp;
							<fmtCommissionEmail:message key="LBL_SEND" var="varSend"/>
							<gmjsp:button value="&nbsp;${varSend}&nbsp;" name="Btn_Email" gmClass="button" buttonType="Save" onClick="fnSendMail();" />
						</td>
					</tr>
					<tr><td colspan="2">&nbsp;</td></tr>
				</table>
			</td>
		</tr>
    </table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
