<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtEmailTrackRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmEmailTrackReport.jsp -->
<fmtEmailTrackRpt:setLocale value="<%=strLocale%>"/>
<fmtEmailTrackRpt:setBundle basename="properties.labels.custservice.GmEmailTrackReport"/>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>
<bean:define id="gridXmlData" name="frmEmailTrackRpt" property="gridXmlData" type="java.lang.String"> </bean:define>
<%
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strWikiTitle = GmCommonClass.getWikiTitle("EMAIL_TRACKING_REPORT");
    String strApplDateFmt = strGCompDateFmt;
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Email Tracking Report</TITLE>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmEmailTrackReport.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script type="text/javascript">
var objGridData ='<%=gridXmlData%>';
var format = '<%=strApplDateFmt%>';
var lblFromDate = '<fmtEmailTrackRpt:message key="LBL_FROM"/>';
var lblToDate = '<fmtEmailTrackRpt:message key="LBL_TO"/>';
</script>

</HEAD>
<body leftmargin="20" topmargin="20" onload="fnOnPageLoad();">
<FORM name="frmEmailTrackRpt" method="POST" action="/gmEmailTrackRpt.do?method=loadEmailTrackRpt">

<html:hidden  property="strOpt" name="frmEmailTrackRpt"/>

<table  border="0" cellspacing="0" cellpadding="0" class="DtTable1000">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="2">&nbsp;Email Tracking System</td>	
			<td height="20" class="RightDashBoardHeader" colspan="2">&nbsp;</td>
			<fmtEmailTrackRpt:message key="IMG_ALT_HELP" var = "varHelp"/>		
			<td align="right" class=RightDashBoardHeader><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}'
			 onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
				
		<tr class="oddshade">
		<td class="RightTableCaption" colspan="2" align="left" HEIGHT="12" style="width: 40%">&nbsp;<fmtEmailTrackRpt:message key="LBL_TO_MAIL_ID"/>:&nbsp;<html:text
						property="toEmailId" styleId="toEmailId" styleClass="InputArea"
						name="frmEmailTrackRpt" onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" tabindex="1" size="25" maxlength="100"/></td>			
		<td class="RightTableCaption" colspan="3" align="left" HEIGHT="50"><fmtEmailTrackRpt:message key="LBL_SUBJECT"/>:&nbsp;<html:text name="frmEmailTrackRpt" property="subject" tabindex = "2" 
			onfocus="changeBgColor(this,'#AACCE8');" styleId="subject" size="40" styleClass="InputArea gmVerticalMid" onblur="changeBgColor(this,'#ffffff');fnOnBlurEnterData();"/></td>
		</tr>
		
 		<tr><td class="LLine" height="1" colspan="2"></td></tr>
		<tr class="evenshade">
			<td class="RightTableCaption" height="35" align="left" width="10%"><fmtEmailTrackRpt:message key="LBL_DATE_RANGE"/>:</td>
			<td class="RightText" colspan="1">&nbsp;&nbsp;<fmtEmailTrackRpt:message key="LBL_FROM"/>:&nbsp;<gmjsp:calendar SFFormName="frmEmailTrackRpt" controlName="fromDate" tabIndex="3"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
	         <fmtEmailTrackRpt:message key="LBL_TO"/>:&nbsp;<gmjsp:calendar SFFormName="frmEmailTrackRpt" controlName="toDate"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"  tabIndex="4" onBlur="changeBgColor(this,'#ffffff');"/></td>
			<fmtEmailTrackRpt:message key="BTN_LOAD" var="varLoad"/>
			<td align="center"><gmjsp:button value="&nbsp;${varLoad}&nbsp;" name="LoadBtn" gmClass="button" onClick="fnLoad();" tabindex = "-1" buttonType="Load" /></td>
    	<td></td>
		</tr>
 		<tr><td class="Line" height="1" colspan="8"></td></tr>			
		<%
		if (gridXmlData.indexOf("cell") != -1) {
		%>
			
		   	<tr>
       	 		<td colspan="9" >
					<div id="dataGridDiv" style="grid" height="500px" width="980px"></div> 
					<div id="pagingArea" style="width: 980px"></div> 
		    	</td>
           	</tr>
           	<tr>
               <td colspan="6" align="center" width="40%" height="70">
		       <div class='exportlinks'><fmtEmailTrackRpt:message key="LBL_EXPORT_OPTIONS"/>:&nbsp;<img src='img/ico_file_excel.png'/>&nbsp;<a href="#"
                    onclick="javascript:fnExportExcel();"> <fmtEmailTrackRpt:message key="LBL_EXCEL"/>  </a>
                </div>
                </td>
			</tr>
			<%
			}else  if(!gridXmlData.equals("")){%>
				<tr height="25px"><td colspan="8" align="center" class="RightText" ><fmtEmailTrackRpt:message key="LBL_NOTHING"/></td></tr>
			<%} %>
			
 		 </table>
 		 </FORM>
 		 <%@ include file="/common/GmFooter.inc" %>
 		 </body>
 		 </HTML>