<%
/**********************************************************************************
 * File		 		: GmInHouseItemConsignSetup.jsp
 * Desc		 		: Currently used for all InHouse transactions except BULK
 * Version	 		: 1.0
 * author			: Dhinakaran James
 * Modification		: removed the Ship Req flag condition for InHouse. Defaults to 1 meaning
 					  shipping is mandatory
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>


<%@ taglib prefix="fmtInHouseItemConsignSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- custservice\GmInHouseItemConsignSetup.jsp -->

<fmtInHouseItemConsignSetup:setLocale value="<%=strLocale%>"/>
<fmtInHouseItemConsignSetup:setBundle basename="properties.labels.custservice.GmInHouseItemConsignSetup"/>


<%
try {
	GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmInHouseItemConsignSetup", strSessCompanyLocale);
	response.setCharacterEncoding("UTF-8");
	
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
	
	// getting currency symbol from session
	String strCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	
	HashMap hmReturn = (HashMap)session.getAttribute("hmReturn");
	HashMap hmTransRules=GmCommonClass.parseNullHashMap((HashMap)session.getAttribute("TRANS_RULES"));
	String strhAction = (String)session.getAttribute("hAction") == null?"":(String)session.getAttribute("hAction");
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strTemp = "";

	String strConsignId = (String)session.getAttribute("CONSIGNID") == null?"":(String)session.getAttribute("CONSIGNID");
	String strSetName = "";
	String strAccName = "";
	String strUserName = "";
	String strIniDate = "";
	String strItemQty = "";
	String strControl = "";
	String strStockLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_IN_SHELF")); 
	String strPartNums = (String)session.getAttribute("strPartNums") == null?"":(String)session.getAttribute("strPartNums");
	String strOpt = (String)session.getAttribute("strSessOpt") == null?"":(String)session.getAttribute("strSessOpt");
	String strhMode = (String)session.getAttribute("hMode") == null?"":(String)session.getAttribute("hMode");
	String strAddCart = (String)session.getAttribute("ADDCARTTYPE") == null?"106457":(String)session.getAttribute("ADDCARTTYPE");
	String strRowCount = (String)session.getAttribute("CARTROWCOUNT") == null?"0":(String)session.getAttribute("CARTROWCOUNT"); 
	String strTransParts = "";
	String strBillComp = "";
	String strNewPartNums = "";
	String strPartDesc = "";
	String strFlag = ""; 
	
	String strType = "";
	String strDistribAccId = "";
	String strPurpose = "";
	String strBillTo = "";
	String strShipTo = "";
	String strShipToId = "";
	strShipToId = (String)session.getAttribute("strSessUserId");
	String strFinalComments = "";
	String strShipFl = "";
	String strDelFl = "";
	String strHeader = "";
	String strLabel1 = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_INITIATED_BY"));
	//String strMessage = "";
	String strAccess=GmCommonClass.parseNull((String)request.getAttribute("ACCESSFL"));
	String strBillToFl = "";
	String strWikiTitle = "";
	String ownCompId = "";
	
	ArrayList alPurpose = new ArrayList();
	ArrayList alEmpList = new ArrayList();
	ArrayList alDistList = new ArrayList();
	ArrayList alQDList = new ArrayList();

	HashMap hmConsignDetails = new HashMap();
	HashMap hmTemp = new HashMap();
	HashMap hmParam =  new HashMap();
	HashMap hmOtherDetails =  new HashMap();
	
	boolean bolFl0 = false;
	
	String strTypeRule = "";
	String strShipToRule = "";
	String strHeaderRule = "";
	//String strLabel1Rule = "";
	//String strMessageRule = "";

	int intPriceSize = 0;

	if (hmReturn != null)
	{
		hmTemp = (HashMap)hmReturn.get("CONLISTS");
		alPurpose = (ArrayList)hmTemp.get("PURPOSE");
		alEmpList = (ArrayList)hmTemp.get("EMPLIST");
		alDistList = (ArrayList)hmTemp.get("DISTLIST");
		alQDList = (ArrayList)hmReturn.get("QDLIST");
	}

	hmParam = (HashMap)session.getAttribute("PARAM");

	if (hmParam != null)
	{
		strType = (String)hmParam.get("TYPE");
		if (strType != "")
		{
			bolFl0 = true;
		}
		strPurpose = (String)hmParam.get("PURPOSE");
		strBillTo = (String)hmParam.get("BILLTO");
		strShipTo = (String)hmParam.get("SHIPTO");
		strShipToId = GmCommonClass.parseNull((String)hmParam.get("SHIPTOID"));
		strFinalComments = (String)hmParam.get("COMMENTS");
		strShipFl = (String)hmParam.get("SHIPFL");
		strChecked = strShipFl.equals("1")?"checked":"";
		strDelFl = (String)hmParam.get("SHIPFL");
		strBillComp = (String)hmParam.get("BILLCOMP");
	}
	if (strOpt.equals("InHouse"))
	{
		strType = "4112";
		strShipTo = "4123";
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_CONSIGN_IN_HOUSE_ITEMS"));
		strLabel1 = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_CONSIGN_TO"));
	}
	else if (strOpt.equals("Repack"))
	{
		strType = "4114";
		strShipTo = "";
		strHeader =  GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_SHELF_REPACKAGE_INITIATE"));
		strLabel1 =  GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_AUTHORIZED_BY"));
		//strMessage = "&nbsp;This transaction enables you to move parts from the Repacking to Shelf .<Br>&nbsp;Please make sure that paperwork is attached to the bin containing the parts when being processed.";
	}
	else if (strOpt.equals("QuaranIn"))
	{
		strType = "4113";
		strShipTo = "";
		strHeader =  GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_SHELF_TO_QUARANTINE"));
		strLabel1 =  GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_AUTHORIZED_BY"));
		//strMessage = "&nbsp;This transaction enables you to remove parts from Inventory and move to the Quarantine Area. The Qty moved will reflect changes (in Qty on Shelf '-' and Qty in Quarantine '+') only after this transaction has been verified.<Br>&nbsp;Please make sure that paperwork is attached to the bin containing the parts when being processed.";
	}		
	else if (strOpt.equals("QuaranOut"))
	{
		strType = "4115";
		strShipTo = "";
		strHeader =  GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_QUARANTINE_SCRAP_INITIATE"));
		strLabel1 =  GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_INITIATED_BY"));
		//strMessage = "&nbsp;This transaction enables you to remove/scrap parts from Quarantine and move it totally out of the system. The Qty moved will reflect changes (Qty in Quarantine '-') only after this transaction has been verified.<Br>&nbsp;Please make sure that paperwork is attached to the bin containing the parts when being processed.";
	}
	else if (strOpt.equals("InvenIn"))
	{
		strType = "4116";
		strShipTo = "";
		strHeader =  GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_QUARANTINE_SHELF_INITIATE"));
		strLabel1 =  GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_INITIATED_BY"));
		//strMessage = "&nbsp;This transaction enables you to put back parts to the Shelf from Quarantine. The Qty moved will reflect changes (Qty in Quarantine '-' and Qty on Shelf '+') only after this transaction has been verified.<Br>&nbsp;Please make sure that paperwork is attached to the bin containing the parts when being processed.";
	}
	
	///////////////////////
	if(hmTransRules.size()>0){
		strTypeRule =GmCommonClass.parseNull((String)hmTransRules.get("ICE_TYPE")) ;
		strShipToRule = GmCommonClass.parseNull((String)hmTransRules.get("ICE_SHIPTO")) ;
		strHeaderRule = GmCommonClass.parseNull((String)hmTransRules.get("ICE_HEADER")) ;
		//strLabel1Rule = GmCommonClass.parseNull((String)hmTransRules.get("ICE_LABEL")) ;
		//strMessageRule =GmCommonClass.parseNull((String)hmTransRules.get("ICE_MESSAGE"));
		strBillToFl    = GmCommonClass.parseNull((String)hmTransRules.get("BILL_TO_FL"));
		
		if(!strTypeRule.equals(""))
			strType = strTypeRule;
		if(!strShipToRule.equals(""))
			strShipTo = strShipToRule;
		if(!strHeaderRule.equals(""))
			strHeader = strHeaderRule+" - Initiate";
		/*if(!strLabel1Rule.equals(""))
			strLabel1 = strLabel1Rule;*/
		//if(!strMessageRule.equals(""))
		//	strMessage = strMessageRule;
	}
	///////////////////////
	
	if(strType.equals("4116")){//  4116 --  Quarantine To Shelf 
	  strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("Quarantine_To_Shelf_Initiate"));
	}else if(strType.equals("400063")){// 400063 -- Repack to Shelf 
	  strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("Repack_to_Shelf_Initiate"));
	}
	
	String strSetClr="";  
	strSetClr = strhAction.equals("Load")?"class='shade'":""; //Set Alternate Color for Bill to Dropdown
	int intSize = 0;
	HashMap hcboVal = null;
%>

<%@page import="java.util.StringTokenizer"%><HTML>
<HEAD>
<TITLE> Globus Medical: InHouse Item Consign </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
var billToFl = '<%=strBillToFl%>';
var cartType="<%=strAddCart%>";
var rowCnt = '<%=strRowCount%>';
var strBillComp = '<%=strBillComp%>';


function fnSubmit()
{
	document.frmVendor.hAction.value = 'Save';
  	document.frmVendor.submit();
}

function fnInitiate()
{
	Error_Clear();
	fnValidateDropDn('Cbo_Purpose','Reason');
	if(billToFl == "YES"){
		fnValidateDropDn('Cbo_BillTo','Bill To');
	}
	fnValidateDropDn('Cbo_Values','<%=strLabel1%>');
	
	if (ErrorCount > 0)
	{
		Error_Show();
		return false;
	}else{
		document.frmVendor.hAction.value = 'EditLoad';
		fnStartProgress('Y');
	  	document.frmVendor.submit();
  	}

}

function fnAddToCart()
{
	var addCart=document.frmVendor.Cbo_Add_Cart.value;

	Error_Clear();
	if(addCart == '106458'){//106458-by transaction
		var copyTransErr='';
		var arrTransId=new Array();
		var arrNewTransId= new Array();
		var Cbo_Type=document.frmVendor.Cbo_Type.value;
		var transactionId = document.frmVendor.Txt_PartNum.value;
		var tempDate = new Date();
		var hidTransIds=document.frmVendor.hPartNums.value;
		var transErr='';
		
		arrTransId=transactionId.split(',');
		// check duplicate transaction entered in txtbox
			for(var i=0;i<arrTransId.length;i++){
				var cnt=0;
				for(var j=i+1;j<arrTransId.length;j++){
					if(arrTransId[i]==arrTransId[j]){
						cnt++;
					}
				}
				if(cnt>0){// If count greater than zero, duplicate transactions are entered
					transErr+=arrTransId[i]+',';
				}
			}
		
			if(transErr!=''){// If transErr is not empty, throws duplicate transaction validation
				Error_Details(message_operations[743]+transErr.substr(0,transErr.length-1));
			}
		
		//check transaction is already added to cart
		if(hidTransIds!=''){
				arrTransId=transactionId.split(',');
				arrNewTransId=hidTransIds.split(',');
				for(var i=0;i<arrTransId.length;i++){
					for(var k=0;k<arrNewTransId.length;k++){
						if(arrTransId[i]==arrNewTransId[k]){
							copyTransErr+=arrTransId[i]+',';
							break;
						}
					}
				}
			
		}
		
		if(copyTransErr!=''){// copyTransErr is not null, already transaction added to cart validation
			 Error_Details(copyTransErr.substr(0,copyTransErr.length-1)+message_operations[744]);
		} 
		
		if(transactionId.substr(transactionId.length-1,transactionId.length-1) != ','){
			transactionId=transactionId+',';
		}
		 
		//ajax call to check validation 
		var ajaxUrl = fnAjaxURLAppendCmpInfo("/GmConsignItemServlet?hAction=ByTransaction&Src_Trans_Id="+transactionId+"&Trans_Type="+Cbo_Type+"&encode="+tempDate.getTime());
				dhtmlxAjax.get(ajaxUrl,function(loader)
				{
			var response = loader.xmlDoc.responseText;
				if(response == ''){// if response is empty, all transactios are valid
					if (ErrorCount > 0)
					{
						Error_Show();
						return false;
					}
					document.frmVendor.hAddCartType.value=addCart;
					document.frmVendor.hAction.value = "GoCart";
					fnStartProgress('Y');
					document.frmVendor.submit();
				}else{
					var arr=response.split('|');
					if(arr[0]!=''){
						Error_Details(message_operations[745]+arr[0].substr(0,arr[0].length-1));//--- to show invalid transaction validation
					}
					if(arr[1]!=''){
						Error_Details(arr[1].substr(0,arr[1].length-1)+message_operations[746]);//-- to show voided transaction validation 
					}
					if(arr[2]!=''){
						Error_Details(message_operations[747]+arr[2].substr(0,arr[2].length-1));//-- to show not verified transaction validation
					}
					if(arr[3]!=''){
						Error_Details(arr[3].substr(0,arr[3].length-1)+message_operations[748]);//-- to show already transaction copied validation
					}
					if (ErrorCount > 0)
					{
						Error_Show();
						return false;
					}
				}
				});
				
				
				
				
	}else{
	document.frmVendor.hAddCartType.value=addCart;
	document.frmVendor.hAction.value = "GoCart";
	fnStartProgress('Y');
	document.frmVendor.submit();
	}
}

function fnRemoveItem(val)
{
	document.frmVendor.hDelPartNum.value = val;
	document.frmVendor.hAction.value = "RemoveCart";
	document.frmVendor.submit();	
}

function fnUpdateCart()
{
	document.frmVendor.hAction.value = "UpdateCart";
	fnStartProgress('Y');
	document.frmVendor.submit();	
}

function fnPlaceOrder(val)
{
	var lblReason = '<fmtInHouseItemConsignSetup:message key="LBL_REASON"/>'
	Error_Clear();
	fnValidateDropDn('Cbo_Purpose',lblReason);
	fnValidateDropDn('Cbo_Values','<%=strLabel1%>');
	
	//
	var arr =new Array(); 
	var addCart=document.frmVendor.Cbo_Add_Cart.value;

	if(addCart == '106458'){//106458 - by transaction,to split transaction partnumber keys
		arr=	document.frmVendor.hTransPartNums.value.split(',');
	}else{
		arr=	document.frmVendor.hPartNums.value.split(',');
	}
	
	
	var pnum = '';
	var stock = '';
	var qty = '';
	var obj = '';
	var cnum = '';
	var strErrorMore = '';
	var j = 0;
	var strErrorQty= '';
	var strErrorNegitive = '';
	var strPart = '';
	var strOwnCompid = '';
    for(var i=0;i<arr.length;i++)
    {
        qid = arr[i];
        j++;
        if(addCart == '106458'){//106458 - by transaction,to split transaction partnumber keys
        	obj = eval("document.frmVendor.hPNum"+j);
        	qid = obj.value;
    	}
		obj = eval("document.frmVendor.hStock"+j);
		stock = parseInt(obj.value);
		obj = eval("document.frmVendor.Txt_Qty"+j);
		qty = parseInt(obj.value);
		var qtyVal = obj.value;
		if(qty == 0 || qty == ''){
			strErrorQty = strErrorQty +","+ qid; 
		}else if(qty < 0 || isNaN(qtyVal))
		{
			strErrorNegitive = strErrorNegitive +","+ qid; 
		}
		
		if (qty > stock)
		{
			strErrorMore  = strErrorMore + ","+ qid;
		}
		strOwnCompid= eval("document.frmVendor.hOwnComp.value");
		if (strOwnCompid != strBillComp)
		{
			strPart = strPart +","+ qid;
		}

	}
	 if (strPart != '')
		{
		 Error_Details(Error_Details_Trans(message_operations[898],val)+strPart.substr(1,strPart.length));
		}
	/*
	Validations added for Negative and Zero quantity
	*/
    if (strErrorNegitive != '')
	{
		Error_Details(message_operations[708]+strErrorNegitive.substr(1,strErrorNegitive.length));
	}
	if(strErrorQty != '')
	{
    	Error_Details(message_operations[709]+strErrorQty.substr(1,strErrorQty.length));
    } 
	if (strErrorMore != '')
	{
		Error_Details(Error_Details_Trans(message_operations[710],val)+strErrorMore.substr(1,strErrorMore.length));
	} 
	
	//to validate more than 100 rows in cart
	if(j>parseInt(rowCnt)){
		Error_Details(message_operations[749]);
	}

		if (ErrorCount > 0)
		{
			Error_Show();
			return false;
		}else{
			document.frmVendor.hAddCartType.value=addCart;
			document.frmVendor.hAction.value = 'PlaceOrder';
			fnStartProgress('Y');
		  	document.frmVendor.submit();
	  	}
}

function fnPrintVer()
{
windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId=<%=strConsignId%>","Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnAgree()
{
windowOpener("/GmConsignSetServlet?hId=<%=strConsignId%>&hAction=Ack&","Ack","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");

}

function fnPicSlip()
{
	val = document.frmVendor.hConsignId.value;	
	windowOpener("<%=strServletPath%>/GmConsignItemServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}

// function calls onchange in add to cart dropdown, to show related labels
function fnAddCartTxt(obj){
	if(obj.value=='106458'){//106458-By Transaction
		document.getElementById("byTransTxtId").style.display="block";
		document.getElementById("byPartTxtId").style.display="none";
	}else{
		document.getElementById("byTransTxtId").style.display="none";
		document.getElementById("byPartTxtId").style.display="block";
	}
}

//-- to prevent auto form submit by scanning barcode and press enter button 
function fnPreventAutoSubmit(){
	if(window.event.keyCode== 13){
		fnAddToCart();
	}
	return !(window.event && window.event.keyCode == 13); 
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmConsignItemServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hConsignId" value="<%=strConsignId%>">
<input type="hidden" name="hDelPartNum" value="">
<input type="hidden" name="Cbo_Type" value="<%=strType%>">
<!-- <input type="hidden" name="Cbo_BillTo" value="01"> -->
<input type="hidden" name="Cbo_ShipTo" value="<%=strShipTo%>">
<input type="hidden" name="hMode" value="<%=strhMode%>">
<input type="hidden" name="hAddCartType" value="<%=strAddCart%>">

	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="5"></td>
			<td height="25" class="RightDashBoardHeader"><span style="width: 95%;"><%=strHeader%></span>
				 <fmtInHouseItemConsignSetup:message key="IMG_ALT_HELP" var = "varHelp"/>
				<span style="width: 4%;"><img id='imgEdit' class="RightDashBoardHeader"  style='cursor:hand;float: right;' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' 
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"  /></span>
			</td>
			<td bgcolor="#666666" width="1" rowspan="5" ></td> 
		</tr>
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<%-- <tr>
			<td class="RightTextBlue" height="40"><%=strMessage%></td>
		</tr>--%>
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<%
	   if(strBillToFl.equals("YES"))
	   {
	%>
	<tr <%=strSetClr%>>
	<fmtInHouseItemConsignSetup:message key="LBL_Bill_To" var="varBillTo"/>
						<td class="RightText" HEIGHT="25" align="right">&nbsp;<B>${varBillTo}:</B></td>
						<td class="RightText">&nbsp;<select name="Cbo_BillTo" id="Region" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtInHouseItemConsignSetup:message key="LBL_CHOOSE_ONE"/>			
				        <%
			  		intSize = alDistList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alDistList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strBillTo.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%
			  		}
%>
				        </select>
						</td>
					</tr>
<%
	   }
	   else
	   {
	     %>
	     <input type="hidden" name="Cbo_BillTo" value="01">
	     <% 
	   }

		if (!strhAction.equals("Load"))
		{
%>
					<tr class="shade">
						<td class="RightText" HEIGHT="20" width="200" align="right">&nbsp;<b><fmtInHouseItemConsignSetup:message key="LBL_CONSIGNMENT_ID"/>:</b></td>
						<td class="RightText" width="498">&nbsp;<%=strConsignId%></td>
					</tr>
<%
		}
%>
					<tr>
						<td class="RightText" HEIGHT="25" align="right">&nbsp;<b><fmtInHouseItemConsignSetup:message key="LBL_REASON"/>:</b></td>
						<td class="RightText">&nbsp;<select name="Cbo_Purpose" id="Region" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtInHouseItemConsignSetup:message key="LBL_CHOOSE_ONE"/>
<%
			  		intSize = alPurpose.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alPurpose.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strPurpose.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
							</select>
						</td>
					</tr>
					<tr class="shade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><%=strLabel1%>:</b></td>
						<td class="RightText">&nbsp;<select name="Cbo_Values" id="Region" class="RightText"  tabindex="3" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtInHouseItemConsignSetup:message key="LBL_CHOOSE_ONE"/>
<%
			  		intSize = alEmpList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alEmpList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strShipToId.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%
			  		}
%>						
						</select>
						</td>
					</tr>
					<tr>
						<td class="RightText" valign="top" align="right" HEIGHT="24"><b><fmtInHouseItemConsignSetup:message key="LBL_COMMENTS"/>:</b></td>
						<td>&nbsp;<textarea name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1 rows=5 cols=50 value="<%=strFinalComments%>"><%=strFinalComments%></textarea></td>
					</tr>
					<tr>
						<td colspan="2"><input type="hidden" name="Chk_ShipFlag" value="1"></td>
					</tr>										
<%
		if (!strhAction.equals("Load") && !strhAction.equals("OrdPlaced"))
		{
%>					
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr>
					<td class="RightText" align="right" height="30"><b><fmtInHouseItemConsignSetup:message key="LBL_ADD_CART"/></b></td>
					<td style="padding-left: 4px;">
					 <gmjsp:dropdown controlName="Cbo_Add_Cart" value="<%=alQDList%>" seletedValue="<%=strAddCart%>" onChange="fnAddCartTxt(this)"  codeId="CODEID"  codeName="CODENM" />  
					</td>
					</tr>
					<tr>
						<td height="30" align="center" colspan="2" class="RightTextBlue">
						<b><fmtInHouseItemConsignSetup:message key="LBL_SHOPPING_CART"/></b><br><div id="byPartTxtId"><fmtInHouseItemConsignSetup:message key="LBL_ENTER_PART_CLICK_GO"/></div>
						<div id="byTransTxtId"><fmtInHouseItemConsignSetup:message key="LBL_SCAN_TRANS"/></div>
						<br>&nbsp;&nbsp;&nbsp;<input type="text" size="40" value="" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1 onkeypress="return fnPreventAutoSubmit()">
						<fmtInHouseItemConsignSetup:message key="BTN_GO" var="varGo"/>
						<gmjsp:button value="&nbsp;${varGo}&nbsp;" name="Btn_GoCart" gmClass="button" onClick="fnAddToCart();" tabindex="16" buttonType="Load" /><br><br>
						</td>
					</tr>
					<%if(strAddCart.equals("106458")){ %>
					<tr>
					<td colspan="2" id="transCopyViewId"><b><fmtInHouseItemConsignSetup:message key="LBL_COPY_TRANS_ID"/></b><%=strPartNums %></td>
					</tr>
					<%} %>
					<%if(!strRowCount.equals("0")){ %>
					<tr>
					<td colspan="2" class="RightTextRed"><b><fmtInHouseItemConsignSetup:message key="LBL_NO_ROWS_1"/> <%=strRowCount %> <fmtInHouseItemConsignSetup:message key="LBL_NO_ROWS_2"/></b></td>
					</tr>
					<%} %>
<%
		}
		if (!strhAction.equals("Load"))
		{
			//String strStockLabel = "In Shelf";
			String strStockLabelRule = "";
			strStockLabelRule=GmCommonClass.parseNull((String)hmTransRules.get("TRANS_LABEL"));
			System.out.println("strStockLabel==>"+strStockLabel +" strStockLabelRule ?"+strStockLabelRule);
							if (strOpt.equals("QuaranOut") || strOpt.equals("InvenIn"))
							{
								strStockLabel = "In Quarantine";
							}else if(strOpt.equals("Repack")){
								strStockLabel = "In Re-Packaging";
							}
							if(!strStockLabelRule.equals("")){
								strStockLabel = strStockLabelRule;
							}
%>	

					<tr>
						<td align="center" colspan="2" valign="top">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td align="center" width="80" height="25"><fmtInHouseItemConsignSetup:message key="LBL_PART"/></td>
									<td align="center" width="200"><fmtInHouseItemConsignSetup:message key="LBL_DESCRIPTION"/></td>
									<td align="center" width="70"><%=strStockLabel%></td>
									<td align="center" width="80"><fmtInHouseItemConsignSetup:message key="LBL_PRICE"/></td>
									<td align="center" width="80"><fmtInHouseItemConsignSetup:message key="LBL_QUANTITY"/></td>
									<td align="center" width="100"><fmtInHouseItemConsignSetup:message key="LBL_AMOUNT"/></td>
								</tr>
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
<%
			String strTotal = "";
			if (!strPartNums.equals(""))
			{
					hmReturn = GmCommonClass.parseNullHashMap((HashMap)session.getAttribute("hmOrder"));
					HashMap hmCart = GmCommonClass.parseNullHashMap((HashMap)session.getAttribute("hmCart"));
					StringTokenizer strTok=null;
					String strTransIds="";
					if(strAddCart.equals("106458")){// 106458 - by transaction
					  //-- get transaction part number keys and split to fetch details from hashmap
					  strTransParts= (String)hmReturn.get("TRANSPARTNUMS");
					  strTransIds = (String)hmReturn.get("TRANSIDS");
					  strTok = new StringTokenizer(strTransParts,",");
					}else{
					  strTok = new StringTokenizer(strPartNums,",");
					}
					
					String strPartNum = "";
					HashMap hmVal = new HashMap();
					HashMap hmLoop = new HashMap();
					String strId = "";
					
					String strDesc = "";
					String strPrice = "";

					String strAmount = "";
					String strQty = "1";
					String strStock = "";
					String strUnPartNums = "";
					double dbPrice = 0.0;
					double dbDiscount = 0.0;
					double dbDiscPrice = 0.0;
					String strDiscPrice = "";
					String strShade = "";
					int i=0;
					double dbAmt = 0.0;
					double dbTotal = 0.0;
					boolean bolFl = false;
					boolean bolFl2 = false;
					String strQuarantineStock = "";
					String strControlNum = "";
					String strTransPartQty = "";
					String strDelPartNum = "";//-- strDelPartNum to send transaction part key to delete row
					
					double dbQty = 0.0;
					if (strTok.countTokens() == 1)
					{
						bolFl2 = true;
					}
					while (strTok.hasMoreTokens())
					{
						i++;
						strPartNum 	= strTok.nextToken();
						if (hmCart != null)
						{
							hmLoop = (HashMap)hmCart.get(strPartNum);
							if (hmLoop != null)
							{
								strQty = (String)hmLoop.get("QTY");
							}
							else
							{
								strQty = "1";
							}
						}
						strQty = strQty == null?"1":strQty;
						dbQty = Double.parseDouble(strQty);
						hmVal = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get(strPartNum));
						ownCompId = GmCommonClass.parseZero((String)hmVal.get("OWN_COMP"));
						if (hmVal.isEmpty())
						{
							//bolFl = false;
							i--;
						}
						else if (hmVal.isEmpty() && bolFl2)
						{
							bolFl = false;
						}
						else
						{
							strNewPartNums = strNewPartNums + strPartNum + ",";
							bolFl = true;
							strId = (String)hmVal.get("ID");
							strDesc = (String)hmVal.get("PDESC");
							strPrice = GmCommonClass.parseZero((String)hmVal.get("PRICE"));
							strDelPartNum = strId;
							
							if(strAddCart.equals("106458")){//-- 106458- by transaction type
							  if (hmCart.size()==0){
							  strQty=GmCommonClass.parseZero((String)hmVal.get("QTY"));
							  dbQty = Double.parseDouble(strQty);
							  }
							  strControlNum = (String)hmVal.get("CTN_NUM");//-- control number from transaction
							  strTransPartQty = (String)hmVal.get("QTY");// -- transaction qty
							  strDelPartNum = strPartNum;
							}

							
							if (strOpt.equals("QuaranOut") || strOpt.equals("InvenIn"))
							{
								strStock = GmCommonClass.parseZero((String)hmVal.get("QSTOCK"));
							}else if(strOpt.equals("REPACK-SHELF")){
								strStock = GmCommonClass.parseZero((String)hmVal.get("REPACK_STOCK"));
							}else if(!strStockLabelRule.equals("")){
								strStock=GmCommonClass.parseZero((String)hmVal.get((String)hmTransRules.get("STOCK_KEY")));
							}
							else{
								strStock = GmCommonClass.parseZero((String)hmVal.get("STOCK"));
							}
							/*if (strhAction.equals("OrdPlaced"))
							{
								int intStock = Integer.parseInt(strStock);
								intStock = intStock - intQty; 
								strStock = "" + intStock;
							}*/

							double dbStock = Double.parseDouble(strStock);
							strStock = dbStock < 0?"0":strStock;

							dbPrice = Double.parseDouble(strPrice);
							dbAmt = dbQty * dbPrice; // Multiply by Qty
							strDiscPrice = String.valueOf(dbPrice);
							strAmount = String.valueOf(dbAmt);
							dbTotal = dbTotal + dbAmt;
							strShade	= (i%2 == 0)?"class=Shade":""; //For alternate Shading of rows

%>
								<tr <%=strShade%>>
									<td class="RightText" align="center" height="20">
<%
							if (!strhAction.equals("OrdPlaced"))
							{
%>									
									<a  href="javascript:fnRemoveItem('<%=strDelPartNum%>');"><fmtInHouseItemConsignSetup:message key="IMG_ALT_REMOVE" var = "varRemove"/><img border="0" Alt='${varRemove}' valign="bottom" src="<%=strImagePath%>/btn_remove.gif" height="15" width="15"></a><%}%>&nbsp;<%=strId%></td>
									<td class="RightText"><%=strDesc%></td>
									<td class="RightText" align="center"><%=strStock%><input type="hidden" name="hStock<%=i%>" value="<%=strStock%>"></td>
									<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strDiscPrice)%>&nbsp;
									<input type="hidden" name="hPrice<%=i%>" value="<%=strDiscPrice%>">
									<input type="hidden" name="hOldTransQty<%=i%>" value="<%=strTransPartQty%>">
									<input type="hidden" name="hCtlNum<%=i%>" value="<%=strControlNum%>">
									<input type="hidden" name="hPNum<%=i%>" value="<%=strId%>">
									</td>
									<td class="RightText" align="center">
<%
							if (!strhAction.equals("OrdPlaced"))
							{
%>
										&nbsp;<input type="text" size="5" value="<%=strQty%>" name="Txt_Qty<%=i%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
<%
							}else{	
%>
										<%=strQty%>
<%							}%>
									</td>
									<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strAmount)%>&nbsp;</td>
								</tr>
<%
						strTotal = ""+dbTotal;
						}
					}
						if (!strNewPartNums.equals(""))
						{
						  if(strAddCart.equals("106458")){//--106458 - By transaction type 
						    strNewPartNums = strTransIds;
						  }else{
							strNewPartNums = strNewPartNums.substring(0,strNewPartNums.length()-1);
						  }
						}
						if (bolFl)
						{
%>
								<tr>
									<td colspan="6" height="1" bgcolor="#666666"></td>
								</tr>
								<tr class="RightTableCaption">
									<td colspan="4" align="right" height="23">
									<input type="hidden" name="hTotal" value="<%=strTotal%>">
									</td>
									<td align="center">Total</td>
									<td align="right"><%=strCurrSign%><%=GmCommonClass.getStringWithCommas(strTotal)%>&nbsp;</td>
								</tr>	
<%
						}						

						if (!bolFl)
						{
%>
								<tr>
									<td colspan="6" height="30" class="RightTextBlue" align="center"><fmtInHouseItemConsignSetup:message key="LBL_NO_ITEM"/></td>
								</tr>
<%
						}
}else{ %>
								<tr>
									<td colspan="6" height="30" class="RightTextBlue" align="center"><fmtInHouseItemConsignSetup:message key="LBL_NO_ITEM"/></td>
								</tr>
<%
			}
		}
%>
							</table><input type="hidden" name="hPartNums" value="<%=strNewPartNums%>">
							<input type="hidden" name="hTransPartNums" value="<%=strTransParts%>">
							<input type="hidden" name="hOwnComp" value="<%=ownCompId%>">
							
						</td>
					</tr>
<%
		if (strhAction.equals("Load"))
		{
			strAccess=GmCommonClass.parseNull((String)request.getAttribute("ACCESSFL")).equals("")?GmCommonClass.parseNull((String)session.getAttribute("ACCESSFL")): GmCommonClass.parseNull((String)request.getAttribute("ACCESSFL"));
%>
										
					<tr>
				
						<td colspan="2" height="30" align="center">
						<%
						if(strAccess.equals("Y")){
						%>
							<fmtInHouseItemConsignSetup:message key="BTN_INITIATE" var="varInitiate"/>
							<gmjsp:button value="${varInitiate}" gmClass="button" onClick="fnInitiate();" tabindex="13" buttonType="Save" />
							
							<% }else{
								out.print("<b><center><span style=\"color:red;\">You do not have enough permission to do this action</span><center></b>");	
							}%>
						</td>
						<td  height="1" bgcolor="#666666"></td>
						
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>		
<%
	}else if (strhAction.equals("PopOrd")){
%>
		<tr>
		<%
		String strPlaceOrder = "fnPlaceOrder('"+strStockLabel+"')";
		%>
			<td colspan="3" align="center" height="30">
				<fmtInHouseItemConsignSetup:message key="BTN_UPDATE" var="varUpdate"/>
				<gmjsp:button value="&nbsp;${varUpdate}&nbsp;" name="Btn_UpdateQty" gmClass="button" onClick="fnUpdateCart();" buttonType="Save" />
				&nbsp;&nbsp;
				<fmtInHouseItemConsignSetup:message key="BTN_SUBMIT" var="varSubmit"/>
				<gmjsp:button value="&nbsp; ${varSubmit} &nbsp;" name="Btn_PlaceOrd" gmClass="button" onClick="<%=strPlaceOrder%>" buttonType="Save" />
			</td>
		<tr>
<%	
	}else if (strhAction.equals("OrdPlaced"))
	{
%>
					<tr>
						<td colspan="2" height="30" align="center">
							<fmtInHouseItemConsignSetup:message key="BTN_PICSLIP" var="varPicSlip"/>
							<gmjsp:button value="${varPicSlip}" gmClass="button" onClick="fnPicSlip();" tabindex="13" buttonType="Save" />&nbsp;&nbsp;
						</td>
					</tr>
<%
	}								
%>
				</table>
			 </td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
    </table>
    <script>
    var transType='<%=strType%>';
    var hidPartVal='<%=strNewPartNums%>';
    // 4116-Quarantine To Shelf & 400063-Repack to Shelf, to disabled add to cart dropdown for other transaction type
    if(transType=='4116' || transType=='400063' || transType=='400085' || transType=='400086'){//-- 
    	if(document.getElementById('Cbo_Add_Cart') != undefined){
    		document.getElementById('Cbo_Add_Cart').disabled=false;
    	}	
    }else{
    	if(document.getElementById('Cbo_Add_Cart') != undefined){	
    		document.getElementById('Cbo_Add_Cart').disabled=true;
    	}
    }
    
    if(hidPartVal!=''){//  if transaction is entered with part or transaction , disabled add to cart dropdown 
    	document.getElementById('Cbo_Add_Cart').disabled=true;
    }else{
    	if(cartType=='106458'){//--cart is empty and 106458 - by transaction type , empty transaction copied label
    	document.getElementById('transCopyViewId').innerHTML='';
    	}
    }
    
    if(cartType=='106458'){//--  106458 - by transaction , to display related label
    	document.getElementById("byTransTxtId").style.display="block";
		document.getElementById("byPartTxtId").style.display="none";
    }else {
    	if(document.getElementById("byTransTxtId")!= undefined){
    		document.getElementById("byTransTxtId").style.display="none";
			document.getElementById("byPartTxtId").style.display="block";
    	}
    }
   
   
    </script>
<%-- 
<script>
<%
	if (strhAction.equals("OrdPlaced"))
	{
%>
	alert("Your consignment has been placed. Please print PIC Slip");
<%
	}
%>
</script>
--%>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>