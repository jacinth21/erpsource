 <%
/**********************************************************************************
 * File		 		: GmPartPriceUpdate.jsp
 * Desc		 		: This screen is used for the Part Number Pricing having BatchUpdate and ByProject
 * Version	 		: 1.0
 * author			: Harinadh Reddy
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="strOpt" name="frmPartPriceUpdate" property="strOpt" type="java.lang.String" />
<bean:define id="strGridData" name="frmPartPriceUpdate" property="strGridData" type="java.lang.String" />
<bean:define id="strProjId" name="frmPartPriceUpdate" property="hProjectId" type="java.lang.String" />
<bean:define id="hmReturn" name="frmPartPriceUpdate" property="hmReturn" type="java.util.HashMap" />
<bean:define id="hmResult" name="frmPartPriceUpdate" property="hmResult" type="java.util.HashMap" />
<bean:define id="strAction" name="frmPartPriceUpdate" property="hAction" type="java.lang.String" />
<bean:define id="alResult" name="frmPartPriceUpdate" property="alResult" type="java.util.ArrayList" />
<bean:define id="alPartList" name="frmPartPriceUpdate" property="alPartList" type="java.util.ArrayList" />
<bean:define id="strJSONData" name="frmPartPriceUpdate" property="strJSONData" type="java.lang.String" />
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%> 
<%@ page import ="com.globus.common.beans.GmGridFormat"%>
<%@ taglib prefix="fmtPartPriceUpdate" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartPriceUpdate:setLocale value="<%=strLocale%>"/>
<fmtPartPriceUpdate:setBundle basename="properties.labels.custservice.GmPartPriceUpdate"/>
<!--  \custservice\GmPartPriceUpdate.jsp-->
<%

GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmPartPriceUpdate", strSessCompanyLocale);
	GmServlet gm = new GmServlet();
    GmGridFormat gmGridFormat = new GmGridFormat();
	gm.checkSession(response,session);
	HashMap hPartList =null;
	String strCustServiceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strruleEnforceDate   = GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));	
	String strApplCurrFmt	 	= GmCommonClass.parseNull((String)session.getAttribute("strSessApplCurrFmt"));
	String strCurrSign 			= GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	String strApplDateFmt 		= GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));	
	String strNameDropDownValues = GmCommonClass.parseNull(gmGridFormat.getDropDownMasterValue("Name"));
	
	String strRptFmt 			= "{0,date,"+strApplDateFmt+"}";
	String strCurrFmt 			= "{0,number,"+strCurrSign+strApplCurrFmt+"}";
	String strDiscountFmt 		= "{0,number,"+strApplCurrFmt+"}";	
	int intSize 				= 0;		
	String strHeader 			= "";
	String strName 				= GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PROJECT"));
	Double dbLprice				= 0.0;	
	
	String strMode				= GmCommonClass.parseNull((String)request.getAttribute("hMode"));
	String strParam 			= GmCommonClass.parseNull((String)request.getAttribute("hLoad"));
	
	String strWikiTitle = GmCommonClass.parseNull((String)GmCommonClass.getWikiTitle("PART_NUMBER_PRICE"));
	
	ArrayList alProject 		= new ArrayList();
	ArrayList alFetchHistory	= new ArrayList();	
	HashMap hmLoop				= new HashMap();
	
	alProject 					= GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("PROJLIST"));	
	strProjId 					= strProjId.equals("")?"0":strProjId; 	
	
	if (strParam.equals("Reload")){			
		strProjId 				= GmCommonClass.parseNull((String)request.getAttribute("hProjId"));
		strAction 				= GmCommonClass.parseNull((String)request.getAttribute("hAction"));		
	}
	if (strOpt.equals("Batch")){
		strHeader 				= GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_BATCH_UPDATE"));
		strMode 				= "Batch";		
	}
	else {
		strHeader 				= GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_BATCH_PROJECT"));
		strMode 				= "Save"; 
	}
	String strCol = GmCommonClass.parseNull((String)request.getAttribute("hCol"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Number Price <%=strHeader %> </TITLE>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/fonts/font_roboto/roboto.css"/>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.css">
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>
<script language="JavaScript" src="<%=strCustServiceJsPath%>/GmPartPriceByProject.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx_deprecated.js"></script> 
<%-- <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script> --%>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<%-- <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_form.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_markers.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxMenu/dhtmlxmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_undo.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script> --%>

<style type="text/css" media="all">




 .even{
			background-color:#ffffff !important;
		}
		.uneven{
			background-color:#dcedfc !important;;
		}
		
		div.gridbox table.hdr td {
   
    color: #000000 !important;
    background-color: #d2e9fc !important;
    }	
    
    div.gridbox_material.gridbox .xhdr {
    background:#dcedfc !important;
}
</style>

<script>
var gridsarr;
var TypeName 					= '<%=strName%>';
var changedCol 					= '<%=strCol%>';
var objGridData					= '<%=strGridData%>';
var varNameDropDownValues   	= '<%=strNameDropDownValues%>';
var action = '<%=strAction%>';
var partNumListLen = <%=alPartList.size()%>;
var jsonData = '<%=strJSONData%>';

//For setting the drop down value of Project List
<%	
hPartList = new HashMap();	
for (int i=0;i< alPartList.size();i++)
{
	hPartList = (HashMap) alPartList.get(i); 
%>
var partArr<%=i%> ="<%=GmCommonClass.parseNull((String)hPartList.get("ID"))%>,<%=GmCommonClass.parseNull((String)hPartList.get("ID"))%>";
<%
}
%>

</script>


</HEAD>

<BODY  leftmargin="20" 	topmargin="10" 			onload="javascript:fnOnLoad();">
<FORM  method="POST" 	name="frmPartPriceUpdate"  	action="/gmPartNumPriceUpdate.do">
<input type="hidden" 	name="hColumn" 			value="ID">
<input type="hidden" 	name="hProjectId" 		value=" ">
<input type="hidden" 	name="hMode" 			value="<%=strMode %>">
<input type="hidden" 	name="PartNum" 			value="">
<input type="hidden" 	name="hAction" 			value="<%=strOpt%>">
<input type="hidden" 	name="hInputString" 	value="">
<input type="hidden" 	name="hLoad" 			value="">
<input type="hidden" 	name="hCol" 			value="<%=strCol%>">


	<table class="DtTable850" cellspacing="0" cellpadding="0">
		<tr><td height="25" class="RightDashBoardHeader" colspan="2">&nbsp;<fmtPartPriceUpdate:message key="LBL_PART_NUMBER"/> <%=strHeader %></td>
		<td height="25" class="RightDashBoardHeader">
<fmtPartPriceUpdate:message key="IMG_ALT_HELP" var = "varHelp"/>
		<img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		</td>
		</tr>
		
 		<%
		if (strOpt.equals("Batch")){
%>
 		<tr>			
			<td height="25" class="RightTableCaption" align="right">&nbsp;<%=strName %> <fmtPartPriceUpdate:message key="LBL_NAME"/>:</td>
			<td>&nbsp;<gmjsp:dropdown  controlName="Cbo_ProjId"  seletedValue="<%= strProjId %>" tabIndex="1"  value="<%= alProject%>" defaultValue= "[Choose One]" />
			</td>
		</tr>
<%		}else{ 
%>
		<tr>			
			<td height="25" class="RightTableCaption" align="right">&nbsp;<%=strName %> <fmtPartPriceUpdate:message key="LBL_NAME"/>:</td>
			<td>&nbsp;<gmjsp:dropdown  controlName="Cbo_ProjId" onBlur="javascript:fnCallAcBlur(this)" seletedValue="<%= strProjId %>"	tabIndex="1"  value="<%= alProject%>" defaultValue= "[Choose One]" onChange="fnLoad();" />
			</td>
		</tr>
<%		} 
%>
		<tr><td colspan="3" class="LLine"></td></tr>		
<%
		if (strOpt.equals("Batch")){
%>
		<tr>
			<td class="RightTableCaption" align="right"><fmtPartPriceUpdate:message key="LBL_ENT_DATA_BTC_UPD"/>:</td>
			<td valign="absmiddle">&nbsp;<textarea name="Txt_Batch" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="2" rows=25 cols=40 value=""></textarea></td>
		</tr>
		<tr><td colspan="3" class="LLine"></td></tr>
		<tr>
			<td height="30" align="center" colspan="2">
				<fmtPartPriceUpdate:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" 			gmClass="button" 		onClick="javascript:fnBatchSubmit();" tabindex="3" buttonType="Save" />&nbsp;
				<fmtPartPriceUpdate:message key="BTN_RESET" var="varReset"/><gmjsp:button value="&nbsp;${varReset}&nbsp;" 	name="Btn_Reset" 	gmClass="button" onClick="fnResetBatch();" tabindex="4" buttonType="Save" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
		</tr>
<%
		}else{
			if(strAction.equals("report")){
%>

		<tr><td colspan="3" class="LLine"></td></tr>
		<TR>
			<td class="RightTableCaption" colspan="3">
			&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/copy.gif"  				alt="copy" 						style="border: none;" 	onClick="javascript:docopy()" 				height="14"></a>	
			&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/paste.gif" 				alt="paste" 					style="border: none;" 	onClick="javascript:pasteToGrid()" 			height="14"></a>	
			&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/redo.gif" 				alt="redo" 						style="border: none;" 	onClick="javascript:doredo();" 				height="14"></a>
			&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/undo.gif"			 	alt="Undo" 						style="border: none;" 	onClick="javascript:doundo();" 				height="14"></a>	
			&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/add.gif" 				alt="Add Row" 					style="border: none;" 	onClick="javascript:addRow()">							</a>
			&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/row_add_after_1.gif" 	alt="Add Rows From Clipboard" 	style="border: none;" 	onClick="javascript:addRowFromClipboard()" 	height="14"></a>
			&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" 			alt="Remove Row" 				style="border: none;" 	onClick="javascript:removeSelectedRow()" 	height="14"></a>
			&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/undo_delete.gif" 		alt="Undo Remove Row" 			style="border: none;" 	onClick="javascript:enableSelectedRow()" 	height="14"></a>
			&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/highlight.gif" 			alt="Enable Marking" 			style="border: none;" 	onClick="javascript:enableMarker()"  		height="14"></a>
			&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/disable-highlight.gif" 	alt="Disable Marking" 			style="border: none;" 	onClick="javascript:disableMarker()" 		height="14"></a>
			</td>										
		</TR>
		 <%
				if (!strGridData.equals("")) {
		%>	 		
		<tr><td colspan="3"><div id="dataGridDiv" style="height:425px;"></div></td></tr>
		<tr><td colspan="3" class="LLine"></td></tr>
		<tr>
			<td colspan="2" align="center" height="30">&nbsp;
			<fmtPartPriceUpdate:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" name="submitbtn" gmClass="button" onClick="javascript:fnSubmit();" tabindex="15" buttonType="Save" />&nbsp;
			<fmtPartPriceUpdate:message key="BTN_RESET" var="varReset"/><gmjsp:button value="${varReset}"  gmClass="button" onClick="javascript:fnOnLoad();" tabindex="16" buttonType="Save" />
			</td>
		</tr>
	 <%}else{ %> 
			<tr>
				<td colspan="3" class="LLine" height="1"></td>
			</tr>
			<tr height="30">
				<td colspan="6" class="RightText" align="center"><fmtPartPriceUpdate:message key="LBL_NO_DATA"/></td>
			</tr>
<%			}		
		}
	}
%>	
<%		if (strOpt.equals("Batch")){
%>
		<tr><td colspan="3" class="LLine"></td></tr>
 		<tr>
			<td align="center" colspan="10">
			<display:table  name="frmPartPriceUpdate.alResult"  export="true" id="currentRowObject" class="its"  requestURI="/gmPartNumPriceUpdate.do">
			<fmtPartPriceUpdate:message key="DT_PART_NUMBER" var="varPartNumber"/><display:column property="PNUMID" title="${varPartNumber}"       sortable="true" />
	  		<fmtPartPriceUpdate:message key="DT_DESC" var="varDescription"/><display:column property="PDESC"  title="${varDescription}"/>
	  		<fmtPartPriceUpdate:message key="DT_TIER1" var="varTier1"/><display:column property="TIER1"  title="${varTier1}" 	   class="alignright"   	sortable="true"/>
	  		<fmtPartPriceUpdate:message key="DT_TIER2" var="varTier2"/><display:column property="TIER2"  title="${varTier2}" 	   class="alignright"   	sortable="true"/>
	  		<fmtPartPriceUpdate:message key="DT_TIER3" var="varTier3"/><display:column property="TIER3"  title="${varTier3}" 	   class="alignright"   	sortable="true"/>
	  		<fmtPartPriceUpdate:message key="DT_TIER4" var="varTier4"/><display:column property="TIER4"  title="${varTier4}" 	   class="alignright"    	sortable="true"/>
	  		<fmtPartPriceUpdate:message key="DT_CRE_DT" var="varCreated"/><display:column property="CDATE"  title="${varCreated}" 	   class="alignleft"    	format= "<%=strRptFmt%>" sortable="true"/>
	  		<fmtPartPriceUpdate:message key="DT_CRE_BY" var="varCreated"/><display:column property="CUSER"  title="${varCreated}"   	   class="alignleft"    	sortable="true" />
	  		<fmtPartPriceUpdate:message key="DT_LT_UPD_DT" var="varLastUpdatedDate"/><display:column property="LDATE"  title="${varLastUpdatedDate}" class="alignleft"    	format= "<%=strRptFmt%>" sortable="true"   />
	  		<fmtPartPriceUpdate:message key="DT_LT_UPD_BY" var="varLastUpdatedBy"/><display:column property="LUSER"  title="${varLastUpdatedBy}"   class="alignleft"    	sortable="true" />
	  		</display:table>	
		    </td>
		</tr>	
<% 		} 
%>	
</table>
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
