 <%
/*******************************************************************************
 * File		 		: GmCaseSummaryReport.jsp
 * Desc		 		: This screen is used for displaying the Case Summary Report
 * Version	 		: 1.0
 * author			: Rick Schmid
*******************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.Date"%>
<%@ taglib prefix="fmtCaseSummaryReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmCaseSummaryReport.jsp -->
<fmtCaseSummaryReport:setLocale value="<%=strLocale%>"/>
<fmtCaseSummaryReport:setBundle basename="properties.labels.custservice.ProcessRequest.GmLoanerProcessRequest"/>
<%

	//GmServlet gm = new GmServlet();
	//gm.checkSession(response,session);
	
		String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	String strhAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strDistId = GmCommonClass.parseNull((String)request.getAttribute("hDistId"));
	String strAccId = GmCommonClass.parseNull((String)request.getAttribute("hAccId"));
	String strStsId = GmCommonClass.parseNull((String)request.getAttribute("hStsId"));
	String strSetName = GmCommonClass.parseNull((String)request.getAttribute("hSetName"));
	String strDtTyp = GmCommonClass.parseNull((String)request.getAttribute("hDtType"));
	String strCaseId = GmCommonClass.parseNull((String)request.getAttribute("CASEID"));
	String strFromDt = GmCommonClass.parseNull((String)request.getAttribute("hFromDt"));
	
	String strToDt = GmCommonClass.parseNull((String)request.getAttribute("hToDt"));
	String strStatus = GmCommonClass.parseNull((String)request.getAttribute("status"));
	String strXMLString = GmCommonClass.parseNull((String)request.getAttribute("strXmlData"));

	Date dtFromDt = GmCommonClass.getStringToDate(strFromDt,strApplDateFmt);
	Date dtToDt = GmCommonClass.getStringToDate(strToDt,strApplDateFmt);
	
	ArrayList alDistributorList = (ArrayList)request.getAttribute("DISTLIST");
	ArrayList alAccountList = (ArrayList)request.getAttribute("ACCLIST");
	ArrayList alStatusList = (ArrayList)request.getAttribute("STATUSLIST");

%>
<HTML>
<TITLE> Globus Medical: Case Summary Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%= strcustserviceJsPath%>/GmCaseSummaryReport.js"></script>
<script>

var strdtType  = '<%=strDtTyp%>';
var objGridData  = '<%=strXMLString%>';
var format = '<%=strApplDateFmt%>';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="javascript:fnOnLoad();">
<FORM name="frmCaseSumReport" method="POST" action="<%=strServletPath%>/GmLoanerPartRepServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="strOpt" value="<%=strhAction%>">
<input type="hidden" name="hOpt" value="<%=strhAction%>">
<input type="hidden" name="hCaseId" value="">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hStatus" value="<%=strStatus%>">
<input type="hidden" name="hCancelType" value="VDLON">
	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4"><fmtCaseSummaryReport:message key="LBL_LOANER_SUMMARY_REPORT"/></td>
		</tr>
		<tr>
		    <fmtCaseSummaryReport:message key="LBL_CHOOSEONE" var="varChooseOne"/>
			<td height="30" width="15%" class="RightTableCaption" align="Right">&nbsp;<fmtCaseSummaryReport:message key="LBL_FIELD_SALES_NAME"/>:</td>
			<td align="left">&nbsp;<gmjsp:autolist controlName="Cbo_DistId"  seletedValue="<%=strDistId%>" tabIndex="1" width="200"  value="<%=alDistributorList%>"   defaultValue="${varChooseOne}"/>
			</td>
			<td height="30" width="15%" class="RightTableCaption" align="Right">&nbsp;<fmtCaseSummaryReport:message key="LBL_ACCOUNT"/>:</td>
			<td align="left">&nbsp;<gmjsp:dropdown controlName="Cbo_AccId"  seletedValue="<%=strAccId%>" codeId="ID" codeName="NAME" tabIndex="2" width="200" value="<%=alAccountList%>"   defaultValue="${varChooseOne}"/>
			</td>			
		</tr>
		<tr><td colspan="4" class="Line" height="1"></td></tr>
		<tr class="shade">
			<td height="30" width="15%" class="RightTableCaption" align="Right">&nbsp;<fmtCaseSummaryReport:message key="LBL_SURGERY_DATE_FROM"/>:</td>
			<td class="RightTableCaption" style= "width: fit-content; vertical-align: middle;height: 25px;">&nbsp;<gmjsp:calendar textControlName="fromDt" textValue="<%=(dtFromDt==null)?null:new java.sql.Date(dtFromDt.getTime())%>" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			&nbsp;&nbsp;&nbsp;&nbsp;<fmtCaseSummaryReport:message key="LBL_TO"/>:&nbsp;<gmjsp:calendar textControlName="toDt" textValue="<%=(dtToDt==null)?null:new java.sql.Date(dtToDt.getTime())%>" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			</td>
			<td height="30" width="15%" class="RightTableCaption" align="Right">&nbsp;<fmtCaseSummaryReport:message key="LBL_STATUS"/>:</td>
			<td align="left">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						
						<td width="20%">&nbsp;<gmjsp:dropdown controlName="Cbo_StsId"  seletedValue="<%=strStsId%>" codeId="CODENM" codeName="CODENMALT" tabIndex="3" value="<%=alStatusList%>"   defaultValue="${varChooseOne}"/>
						</td>
						<fmtCaseSummaryReport:message key="BTN_LOAD" var="varLoad"/>
						<td width="15%" align="center"><gmjsp:button value="${varLoad}" style="width: 6em; height: 23" name="Btn_Submit" gmClass="button" onClick="javascript:fnLoadRpt();" buttonType="Load" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<div id="dataGrid" height="360px" width="848px"></div>
			</td>
		</tr>
		<tr id="excelrow" height="25">
			<td colspan="4" align="center" height="25" >
				<div class='exportlinks'><fmtCaseSummaryReport:message key="LBL_EXPORT_OPTION"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="javascript:fnExcelExport();"> <fmtCaseSummaryReport:message key="LBL_EXCEL"/> </a>
				</div>
			</td>
		</tr>
		<%if(strXMLString.indexOf("cell") == -1){%>
		<tr>
			<td colspan="4" align="center"><div  id="ShowDetails" align="center"></div></td>							
		</tr>
		<%} %>
	</table>
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
