
<%
/**********************************************************************************
 * File		 		: GmProcessCredits.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- Imports for Logger -->

<!--  \custservice\GmProcessCredits.jsp-->
<%@ taglib prefix="fmtProcessCredits" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>

<fmtProcessCredits:setLocale value="<%=strLocale%>"/>
<fmtProcessCredits:setBundle basename="properties.labels.custservice.GmProcessCredits"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	
	Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");
	String strFrom = GmCommonClass.parseNull((String)request.getAttribute("hFrom"));
	request.setAttribute("hFrom",strFrom);
	
	log.debug(" From Page in JSP is " + strFrom);

	String strDesc = "";
	String strShade = "";
	String strRAId = "";
	String strSetName= "";
	String strDistName = "";
	String strUserName = "";
	String strItemQty = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strType = "";
	String strReason = "";
	String strTypeId = "";
	String strReasonId = "";
	String strIniDate = "";
	String strOrderId = "";
	String strRetDate = "";
	String strExpDate = "";
	String strAccName = "";

	ArrayList alRetLoad = new ArrayList();
	HashMap hmReturnDetails = new HashMap();

	if (hmReturn != null)
	{
			alRetLoad = (ArrayList)hmReturn.get("RETLOAD");
			hmReturnDetails = (HashMap)hmReturn.get("RETURNDETAILS");

			strRAId = (String)hmReturnDetails.get("RAID");
			strSetName= (String)hmReturnDetails.get("SNAME");
			strDistName = (String)hmReturnDetails.get("DNAME");
			strDesc = (String)hmReturnDetails.get("COMMENTS");
			strAccName = (String)hmReturnDetails.get("ANAME");
			strType = (String)hmReturnDetails.get("TYPE");
			strReason = (String)hmReturnDetails.get("REASON");
			strTypeId = (String)hmReturnDetails.get("TYPEID");
			strReasonId = (String)hmReturnDetails.get("REASONID");
			strIniDate = (String)hmReturnDetails.get("CDATE");
			strUserName = (String)hmReturnDetails.get("CUSER");
			strOrderId = (String)hmReturnDetails.get("ORDID");			
			strRetDate = GmCommonClass.parseNull((String)hmReturnDetails.get("RETDATE"));
			strExpDate = GmCommonClass.parseNull((String)hmReturnDetails.get("EDATE"));
			strDistName = strDistName.equals("")?strAccName:strDistName;
	}
	log.debug(" Valus in hmReturn is " + hmReturn);
	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Credit </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>

function fnSubmit()
{
	Error_Clear();
	hcnt = parseInt(document.frmVendor.hCnt.value);
	missingstr = "";
	reconstr = "";
	for (var i=0;i<hcnt;i++ )
	{
		pnum = eval("document.frmVendor.hPartNum"+i);
		qtyspec = eval("document.frmVendor.hSpecQty"+i);
		qtyret = eval("document.frmVendor.hQtyRet"+i);
		qtyrecon = eval("document.frmVendor.Txt_ReconQty"+i);
		qtyspec = parseInt(qtyspec.value);
		qtyret = parseInt(qtyret.value);
		qtyrecon = parseInt(qtyrecon.value);
		
		if (qtyrecon > qtyspec)
		{
		Error_Details(message[9]);
			break;
		}
		
		if (qtyspec != qtyret)
		{
			sum = qtyrecon + qtyret;
			val = qtyspec - sum;
			rem = qtyspec - qtyret;
			if (val > 0)
			{
				missingstr = missingstr + pnum.value +"^" + val + "^M|";
			}
			if (qtyrecon != 0)
			{
				reconstr = reconstr + pnum.value +"^" + qtyrecon + "^N|";
			}
		}


	}

	if (ErrorCount > 0)
	{
			Error_Show();
			return false;
	}
	
	document.frmVendor.hAction.value = "SaveCredit";
	document.frmVendor.hInputStr.value = missingstr+reconstr;
 // alert(document.frmVendor.hInputStr.value);
  	document.frmVendor.submit();
}

</script>


</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmProcessReturnsServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hRAId" value="<%=strRAId%>">
<input type="hidden" name="hInputStr" value="<%=strRAId%>">
<input type="hidden" name="hFrom" value="<%=strFrom%>">
<input type="hidden" name="hTypeId" value="<%=strTypeId%>">
<input type="hidden" name="hReasonId" value="<%=strReasonId%>">

	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
			<td height="25" class="RightDashBoardHeader"><fmtProcessCredits:message key="LBL_CREDIT_RET"/></td>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtProcessCredits:message key="LBL_RA_ID"/>:</td>
						<td class="RightText" width="100">&nbsp;<%=strRAId%></td>
						<td class="RightTableCaption" align="right">&nbsp;<fmtProcessCredits:message key="LBL_TYPE"/>:</td>
						<td class="RightText" >&nbsp;<%=strType%></td>
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtProcessCredits:message key="LBL_INITIATED_DATE"/>:</td>
						<td class="RightText">&nbsp;<%=strIniDate%></td>
						<td class="RightTableCaption" align="right">&nbsp;<fmtProcessCredits:message key="LBL_REASON"/>:</td>
						<td class="RightText">&nbsp;<%=strReason%></td>
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtProcessCredits:message key="LBL_INITIATED_BY"/>:</td>
						<td class="RightText">&nbsp;<%=strUserName%></td>
						<td class="RightTableCaption" align="right">&nbsp;<fmtProcessCredits:message key="LBL_SET_NAME"/>:</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strSetName)%></td>
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtProcessCredits:message key="LBL_ACC_DIS_NAME"/>:</td>
						<td class="RightText">&nbsp;<%=strDistName%></td>
						<td class="RightTableCaption" align="right">&nbsp;<fmtProcessCredits:message key="LBL_ORDER_ID"/>:</td>
						<td class="RightText">&nbsp;<%=strOrderId%></td>						
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="40"><fmtProcessCredits:message key="LBL_COMMENTS"/>:</td>
						<td class="RightText" colspan="3">&nbsp;<%=strDesc%></td>
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr>
						<td HEIGHT="23" class="RightTableCaption" align="right"><fmtProcessCredits:message key="LBL_DATE_EXPEC"/>:</td>
						<td class="RightText">&nbsp;<%=strExpDate%></td>
						<td HEIGHT="23" class="RightTableCaption" align="right"><fmtProcessCredits:message key="LBL_DATE_RET"/>:</td>
						<td class="RightText">&nbsp;<%=strRetDate%></td>
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtProcessCredits:message key="LBL_CREDIT_DATE"/>:</td>
						<td colspan="3">&nbsp;<input type="text" size="10" value="" name="Txt_Credit_Date" class="InputArea" 
						onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=23>
						<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmVendor.Txt_Credit_Date');" 
						title="Click to open Calendar"  src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" 
						height=18 width=19 /></td>
					</tr>
					
<%
					if ( strhAction.equals("Load"))
					{
%>
					<tr>
						<td colspan="4">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td width="60" align="center" height="25"><fmtProcessCredits:message key="LBL_PART_NUM"/></td>
									<td width="400"><fmtProcessCredits:message key="LBL_DESCRIPTION"/></td>
									<td width="60" align="center"><fmtProcessCredits:message key="LBL_QTY-SPECIFIED"/></td>
									<td width="60" align="center"><fmtProcessCredits:message key="LBL_QTY_RET"/></td>
									<td width="60" align="center"><fmtProcessCredits:message key="LBL_QTY_RECON"/></td>
								</tr>
								<tr><td class=Line colspan=5 height=1></td></tr>
<%
						intSize = alRetLoad.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();

							int intSetQty = 0;
							int intRetQty = 0;
							int intReconQty = 0;
							String strReadOnly = "";
							
							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alRetLoad.get(i);

								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strQty = GmCommonClass.parseZero((String)hmLoop.get("QTY"));
								strQty = strQty.equals("")?"0":strQty;
								intSetQty = Integer.parseInt(strQty);
								strItemQty = GmCommonClass.parseNull((String)hmLoop.get("RETQTY"));
								strItemQty = strItemQty.equals("")?"0":strItemQty;
								intRetQty = Integer.parseInt(strItemQty);
								intReconQty = intSetQty - intRetQty;
								strReadOnly = intReconQty == 0?"ReadOnly":"";

%>
								<tr>
									<td class="RightText" height="20">&nbsp;<%=strPartNum%>
										<input type="hidden" name="hPartNum<%=i%>" value="<%=strPartNum%>"></td>
									<td class="RightText"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td align="center" class="RightText"><%=strQty%>
										<input type="hidden" name="hSpecQty<%=i%>" value="<%=strQty%>"></td>
									<td align="center" class="RightText">&nbsp;<%=strItemQty%>
										<input type="hidden" name="hQtyRet<%=i%>" value="<%=strItemQty%>"></td>
									<td align="center" class="RightText">&nbsp;<input type="text" size="3" value="<%=intReconQty%>" name="Txt_ReconQty<%=i%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" <%=strReadOnly%> onBlur="changeBgColor(this,'#ffffff');" tabindex=1></td>
								</tr>
								<tr><td  colspan="5" height="1" bgcolor="#eeeeee"></td></tr>
<%
							}//End of FOR Loop
						}else {
%>
						<tr><td colspan="5" height="50" align="center" class="RightTextRed"><BR><fmtProcessCredits:message key="LBL_NO_PART"/></td></tr>
<%
						}		
%>
							</table>
						</td>	<input type="hidden" name="hCnt" value="<%=intSize%>">
					</tr>
<%
					}
					/* Below Section to enter comments information */
%>					
					<tr><td class=Line colspan=4 height=1></td></tr>
					<tr><td colspan=4 height=1>&nbsp;</td></tr>
					<tr>
						<td class="RightText" valign="top" align="left" HEIGHT="24"><fmtProcessCredits:message key="LBL_COMMENTS"/>:</td>
						<td colspan="3">&nbsp;<textarea name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
						onBlur="changeBgColor(this,'#ffffff');" tabindex=1 rows=5 cols=85 value=""></textarea></td>
					</tr>
					
					<tr>
						<td colspan="4" height="30" align="center">
							<fmtProcessCredits:message key="BTN_ISSUE_CREDIT" var="varIssueCredit"/><gmjsp:button value="${varIssueCredit}" gmClass="button" onClick="fnSubmit();" tabindex="13" buttonType="Save" />
						</td>
					</tr>
			 
				 </table>
			 </td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
    </table>

</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
