
<%@ include file="/common/GmHeader.inc"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%> 
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ page import="java.util.StringTokenizer" %>
<%@ taglib prefix="fmtOrderQuote" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtOrderQuote:setLocale value="<%=strLocale%>"/>
<fmtOrderQuote:setBundle basename="properties.labels.custservice.GmOrderQuote"/>
<%-- \custservice\GmOrderQuote.jsp --%>
<%
	response.setHeader("Cache-Control", "no-cache, post-check=0, pre-check=0"); //HTTP 1.1
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0
	response.setDateHeader("Expires", 0); //prevents caching at the proxy server 
	
		GmServlet gm = new GmServlet();
		gm.checkSession(response, session);
		
		GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmOrderQuote", strSessCompanyLocale);
		
		GmCalenderOperations gmCal = new GmCalenderOperations();
		String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
		GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
		String strWikiTitle = "";
		String strApplDateFmt = strGCompDateFmt;
		String strOpt = GmCommonClass.parseNull((String)session.getAttribute("strSessOpt"));
		String strCurrSym =  GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
		String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
		// to setting the time zone
		gmCal.setTimeZone(strGCompTimeZone);
		String strCurDate = gmCal.getCurrentDate(strGCompDateFmt);
		strCountryCode = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("COUNTRYCODE"));
		java.sql.Date dtFromDate = null;
		java.sql.Date dtToDate = null;		
		String strAccId = "";
		String strCaseId = "";
		String strCaseInfoId = "";
		String strParentOrdId = "";
		String strDODateFmt="";
		
		if (strOpt.equals("PHONE"))  {  
				strDODateFmt=GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.DO_FMT_ORDER"));
		     	strWikiTitle = GmCommonClass.parseNull((String)GmCommonClass.getWikiTitle("SALES_ORDER"));
		}else if (strOpt.equals("QUOTE"))  {  
			    strDODateFmt=GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.DO_FMT_QUOTE"));
			    strWikiTitle = GmCommonClass.parseNull((String)GmCommonClass.getWikiTitle("NEW_QUOTE"));
		}
		String strFmtedDt  =  GmCommonClass.parseNull(GmCommonClass.getStringFromDate(GmCommonClass.getStringToDate(strCurDate,strApplDateFmt), strDODateFmt));
		HashMap hmReturn = (HashMap) request.getAttribute("hmReturn");
		HashMap hmDefaultShipVal = new HashMap();
		//HashMap hmShip = (HashMap)request.getAttribute("ShippingInfo"); 

		HashMap hmCaseInfo  = (HashMap) request.getAttribute("HMCASEINFO");
		if (hmCaseInfo != null) {
			strAccId = GmCommonClass.parseNull((String) hmCaseInfo.get("ACCTID"));
			strCaseId = GmCommonClass.parseNull((String) hmCaseInfo.get("CASEID"));
			strParentOrdId = GmCommonClass.parseNull((String) hmCaseInfo.get("PARENTID"));
		}
		String strhAction = (String) request.getAttribute("hAction");
		if (strhAction == null) {
			strhAction = (String) session.getAttribute("hAction");
		}
		strhAction = (strhAction == null) ? "Load" : strhAction;

		String strShowSurgAtr = GmCommonClass.parseNull( gmResourceBundleBean.getProperty("DO.SHOW_SURG_ATRB_DET"));
		String strShowDiscount = GmCommonClass.parseNull( gmResourceBundleBean.getProperty("DO.SHOW_DISCOUNT"));
		String strOrdType =GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.PHN_ORD_TYPE"));
		String strOrdHoldSts =GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.ORDER_HOLD_STATUS"));
		String strShowDOID = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.SHOW_DO_ID"));
		String strSelected = "";
		String strCodeID = "";
		String strChecked = "";
		String strDistName = "";
		String strCodeNm = "";
		int strDiscountIndex = -1; // ONly when we have discount attribute,only then the index should have a value.We cannot have 0 as default value.	
		
		String strMode = "5015";
		strOrdType = strOrdType.equals("") ? "2521": strOrdType;
		String strShipCarr = "";
		String strShipMode = "";
		String strShipTo = "";

		ArrayList alMode = new ArrayList();
		ArrayList alShipTo = new ArrayList();
		ArrayList alAccount = new ArrayList();
		ArrayList alRepList = new ArrayList();
		ArrayList alOrderType = new ArrayList();
		ArrayList alCarrier = new ArrayList();
		ArrayList alShipMode = new ArrayList();
		ArrayList alAllRepList = new ArrayList(); // Active and inactive.
		ArrayList alQuoteDetails = new ArrayList();
		
		if (hmReturn != null) {
			alMode = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ORDERMODE"));
			alAccount = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ACCLIST"));
			alShipTo = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("SHIPTO"));
			alRepList = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("REPLIST"));
			alOrderType = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ORDTYPE"));
			alQuoteDetails = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("QUOTELIST"));
			//alAllRepList = (ArrayList) hmReturn.get("ALLREPLIST"); Sale Rep Change
			hmDefaultShipVal = (HashMap)hmReturn.get("DEFAULTSHIPMODECARRVAL");
			if(hmDefaultShipVal.size() > 0){
			strShipCarr = GmCommonClass.parseNull((String)hmDefaultShipVal.get("SHIP_CARRIER"));
			strShipMode = GmCommonClass.parseNull((String) hmDefaultShipVal.get("SHIP_MODE"));
			strShipTo = GmCommonClass.parseNull((String) hmDefaultShipVal.get("SHIP_TO"));
			}
		}
		strShipCarr = strShipCarr.equals("")?"5001" : strShipCarr;
		strShipMode = strShipMode.equals("")?"5004" : strShipMode;
		strShipTo = strShipTo.equals("")?(strCountryCode.equals("en")?"4121":"4122") : strShipTo;

		int intSize = 0;
		HashMap hcboVal = null;
		int i = 0;

		String path = request.getContextPath();
		String basePath = request.getRemoteAddr() + request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		// Quote Changes // 
		String strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PROCESS_SALES_ORDER"));
		String strDOLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_DO_ID"));
		String strCustPOLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_CUSTOMER_PO"));
		String strSubmit = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PLACE_ORDER"));
		
		if (strOpt.equals("QUOTE"))
		{
			strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PROCESS_NEW_QUOTE"));
			strDOLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_QUOTE_ID"));
			strCustPOLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_CUSTOMER_REF"));
			strSubmit = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PLACE_QUOTE"));
		}
		// Get the company information from gmDataStoreVO
		String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
		strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>

<SCRIPT>
var strOpt = '<%=strOpt%>';
var vshipMode = '<%=strShipMode%>';
var vshipCarrier = '<%=strShipCarr%>';
var vshipTo = '<%=strShipTo%>';
var strAccSessCurrency ='<%=strCurrSym%>';
var vcountryCode = '<%=strCountryCode%>';
var vcustPOLabel = '<%=strCustPOLabel%>';
var dateFmt = '<%=strApplDateFmt%>';
var strShowDOID ='<%=strShowDOID%>'; 
function SetDefaultValues(){
	document.all.shipCarrier.value="<%=strShipCarr%>";
	document.all.shipMode.value="<%=strShipMode%>";
	document.all.shipTo.value="<%=strShipTo%>";
	fnGetNames(document.all.shipTo); 
	fnAcIdBlur(document.all.Txt_AccId);
	/* if (strOpt == "PHONE")  -- NSP-398 Changes
	{
		var modeval = document.frmPhOrder.Cbo_Mode.value;
	
		if (modeval == '5017')
		{
			document.frmPhOrder.Chk_HardPO.disabled = true;
		}else{
			document.frmPhOrder.Chk_HardPO.disabled = false;
		}
	} */
}
 
</SCRIPT>
<html>
<head>
	<title> GlobusOne Enterprise Portal: New Order/Quote Process </title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
	<script language="JavaScript" src="<%=strJsPath%>/dhtmlxcommon.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/GmOrderQuote.js"></script>
	<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
	<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>

</head>

<BODY leftmargin="20" topmargin="10"  onload="SetDefaultValues();">
<FORM name="frmPhOrder" method="POST" action="<%=strServletPath%>/GmOrderItemServlet">
<input type="hidden" name="hAction" value="PlaceOrder">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hOpt" value="<%=strOpt %>">
<input type="hidden" name="hTotal" value="">
<input type="hidden" name="hConCode" value="">
<input type="hidden" name="hRepId" value="">
<input type="hidden" name="hGpoId" value="">
<input type="hidden" name="hSysDate" value="<%=strFmtedDt%>"> 
<input type="hidden" name="dateFormat" value="<%=strDODateFmt%>">
<input type="hidden" name="hQuoteStr" value="">
<input type="hidden" name="hHoldFl" value="">
<input type="hidden" name="hHoldSts" value="<%=strOrdHoldSts%>">
<input type="hidden" name="hAccDiscnt" value="">
<input type="hidden" name="hAccCurrency" value="">
<!-- MNTTASK - 8623 - Create input string for Sterile parts -->
<input type="hidden" name="hCtrlNumberStr" value="">

<input type="hidden" name="RE_FORWARD" value="gmOrderItemServlet">

<input type="hidden" name="RE_TXN" value="50925">

<input type="hidden" name="RE_SRC" value="/gmRuleEngine.do?companyInfo=<%=strCompanyInfo%>&RE_FORWARD=gmPhoneOrder&txnStatus=PROCESS&RE_TXN=50925">


<TABLE border="0" width="880" cellspacing="0" cellpadding="0">
			<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
			<tr>
				<td bgcolor="#666666" width="1" rowspan="15"></td>
				<td height="25" width="860"  ><table cellspacing="0" cellpadding="0" border="0" width = 100%><tr>
				<td  class="RightDashBoardHeader"><%=strHeader %></td>
<%				if(strOpt.equals("PHONE") || strOpt.equals("QUOTE")) { 
%>				
				<td height="25" class="RightDashBoardHeader" align="right">
				
<fmtOrderQuote:message key="IMG_ALT_HELP" var = "varHelp"/>
				<img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${Help}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
			    </td>
<%				}else{
%>
				<td height="25" class="RightDashBoardHeader" width="20px"></td>
<%				} 
%>
			    </tr>
			    </table> 
				<td bgcolor="#666666" width="1" rowspan="15"></td>
			</tr>
			<tr><td bgcolor="#666666" height="1"></td></tr>
			<tr bgcolor="#e4e6f2" class="RightTableCaption"><td height="25">&nbsp;<IMG id="tabBillimg" border=0 src="<%=strImagePath%>/minus.gif">&nbsp;<a href="javascript:fnShowFilters('tabBill');" tabindex="-1" class="RightText"><fmtOrderQuote:message key="LBL_BILLING_INFO"/></a></td></tr>
			<tr>
				<td>
					<TABLE border="0" width="878" cellspacing="0" cellpadding="0" id="tabBill" style="display:block">
						<tr>
							<td class="RightTableCaption" HEIGHT="24" align="right"><font color="red">*</font> <fmtOrderQuote:message key="LBL_ACC_ID"/>:</td>
							<td class="RightText">&nbsp;<input type="text" size="8" value="<%=strAccId%>" name="Txt_AccId" class="InputArea" tabindex=1 onFocus="changeBgColor(this,'#AACCE8');" onBlur="javascript:fnAcIdBlur(this);"> &nbsp;&nbsp;<span class="RightTextBlue"></span></td>
						</tr>
						<!-- Custom tag lib code modified for JBOSS migration changes -->
						<tr class="oddshade">
							<td class="RightTableCaption" HEIGHT="24" align="right"><fmtOrderQuote:message key="LBL_ACC_NAME"/></td>
							<td class="RightText" HEIGHT="24" colspan="3">&nbsp;<gmjsp:autolist controlName="Cbo_BillTo" onBlur="javascript:fnAcBlur(this)"  tabIndex="-1"  width="600" value="<%=alAccount%>" defaultValue= "[Choose One]"/></TD>
						</tr>
						<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
						<tr>
							<td colspan="4" bgcolor="#eeeeee">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<IMG id="tabBillimg" border=0 src="<%=strImagePath%>/minus.gif">&nbsp;<a href="javascript:fnShowFilters('tabAcSum');" tabindex="-1" class="RightTableCaption"><i><fmtOrderQuote:message key="LBL_SUMMARY"/></i></a>
								<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" id="tabAcSum" style="display:none">
									<tr>
										<td class="RightText" align="Right"><fmtOrderQuote:message key="LBL_REGION"/>:</td>
										<td id="Lbl_Regn" class="RightText"></td>
										<td class="RightText" align="Right"><fmtOrderQuote:message key="LBL_AREA_DIRECTOR"/>:</td>
										<td id="Lbl_AdName" class="RightText"></td>
										<td class="RightText" align="Right"><fmtOrderQuote:message key="LBL_FEILD_SALES"/>:</td>
										<td id="Lbl_DName" class="RightText"></td>
									</tr>
									<tr>
										<td class="RightText" align="Right"><fmtOrderQuote:message key="LBL_TERRITORY"/>:</td>
										<td id="Lbl_TName" class="RightText"></td>
										<td class="RightText" align="Right"><fmtOrderQuote:message key="LBL_REP"/>:</td>
										<td id="Lbl_RName" class="RightText"></td>
										<td class="RightText" align="Right"><fmtOrderQuote:message key="LBL_GROUP_PRICE"/>:</td>
										<td id="Lbl_PName" class="RightText"></td>
									</tr>
									<tr>
										<td class="RightText" align="Right" valign ="top" width="100"><fmtOrderQuote:message key="LBL_ACCOUNT_INFO"/>&nbsp;:</td>
										<td id="Lbl_AccInfo" class="RightText" colspan="5"  width="750"></td>
								   </tr>
									<tr><td colspan="6" class="LLine"></td></tr>
								</table>
							</td>
						</tr>
						<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
<%
						if (strOpt.equals("PHONE"))
						{
%>						
						<tr>
							<td class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<fmtOrderQuote:message key="LBL_ORDER_TYPE"/></td>
							<td class="RightText">&nbsp;<select name="Cbo_OrdType" id="Region" onChange="fnSetShipSel();fnCalcShipCharge();" class="RightText" tabindex="-1" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtOrderQuote:message key="LBL_CHOOSE"/>
<%
								intSize = alOrderType.size();
									hcboVal = new HashMap();
									for (i = 0; i < intSize; i++) // Hardcoding to remove rest of order types - James 03/31
									{
										hcboVal = (HashMap) alOrderType.get(i);
										strCodeID = (String) hcboVal.get("CODEID");
										strSelected = strOrdType.equals(strCodeID) ? "selected" : "";
							%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
	}
%>
							</select>
						</td>
<%
						}else if (strOpt.equals("QUOTE")){
%>
						<tr>
							<td colspan="2"><input type="hidden" name="Cbo_OrdType" value="2520"></td>
						</tr>
<%
						}
%>
						<td class="RightTableCaption" HEIGHT="23" align="right"><fmtOrderQuote:message key="LBL_DT_ENTERED"/></td>
						<td>&nbsp;<%=strCurDate%></td>
<%
						if (strOpt.equals("QUOTE")){//To fix the allignment issue
%>
						<td class="RightTableCaption" HEIGHT="24" align="right">&nbsp;<fmtOrderQuote:message key="LBL_REQ_BY"/></td>
						<td class="RightText" HEIGHT="24">&nbsp;<input type="text" size="20" maxlength="100" value="" name="Txt_ReqBy" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="4"> </td>
<%
						}
%>
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
					<%-- Sale Rep Change
					<tr>
						<td class="RightTableCaption" HEIGHT="24" align="right"><font color="red">*</font> Sales Rep :</td>
						<td class="RightText" HEIGHT="24" colspan="3">
						    &nbsp;<select name="Cbo_Rep" tabindex="-1" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
							<option value="0">[Choose One]</option>
							<%
								intSize = alAllRepList.size();
									hcboVal = new HashMap();
									for (i = 0; i < intSize; i++) {
										hcboVal = (HashMap) alAllRepList.get(i);
										strCodeID = (String) hcboVal.get("REPID");
							%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
							<%
								}
							%>
						</select> &nbsp;</td>
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
					--%>
					<tr  class="oddshade">
						<td class="RightTableCaption" HEIGHT="24"  align="right"><%=strDOLabel %>:</td>
						<td class="RightText" HEIGHT="24" >&nbsp;<input type="text" size="20" maxlength="20" value="" name="Txt_OrdId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');fnClearDiv();" onBlur="changeBgColor(this,'#ffffff');fnCheckOrderId(this,'<%=strDOLabel%>');" tabindex="3">
						<span id="DivShowDOIDAvail" style="vertical-align:middle; display: none;"> <img height="24" width="25" title="<%=strDOLabel %> available" src="<%=strImagePath%>/success.gif"></img></span>
						<span id="DivShowDOIDExists" style=" vertical-align:middle; display: none;"><img title="<%=strDOLabel %> already exists" src="<%=strImagePath%>/delete.gif"></img></span></td>
<%
						if (strOpt.equals("PHONE"))
						{
%>
						<td class="RightTableCaption" HEIGHT="24" align="right">&nbsp;<fmtOrderQuote:message key="LBL_PARENT_ID"/>:</td>
						<td class="RightText" HEIGHT="24">&nbsp;<input type="text" size="20" maxlength="20" value="<%=strParentOrdId%>" name="Txt_ParentOrdId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="4"> 
						&nbsp;<a onClick="javascript:fnPrintPack();"><img src=<%=strImagePath%>/question.gif border=0></img></a></td>
<%
						}else if(strOpt.equals("QUOTE")){//To fix the allignment 
%>
						<td class="RightTableCaption" HEIGHT="24"  align="right"><%=strCustPOLabel %>:</td>
						<td  class="RightText" HEIGHT="24" >&nbsp;<input type="text" size="20" maxlength="100" value="" name="Txt_PO" class="InputArea"   onFocus="changeBgColor(this,'#AACCE8');fnPOClearDiv();" onBlur="changeBgColor(this,'#ffffff');fnCheckPO('CHECKPO');fnPartNumTxtFocus();" tabindex=9>
						<!-- Removing the condition to make the customer po validation functionality available for US also -->
						<%-- <% if (!strCountryCode.equals("en")){
						%> --%>
						<span id="DivShowPOIDAvail" style="vertical-align:middle; display: none;"> <img height="24" width="25" title="Customer PO available" src="<%=strImagePath%>/success.gif"></img></span>
						<span id="DivShowPOIDExists" style="vertical-align:middle; display: none;"><img title="Customer PO already exists" src="<%=strImagePath%>/delete.gif"></img>&nbsp;<a href="javascript:fnModifyOrder()"><img height="15" border="0" title="Order Details" src="<%=strImagePath%>/location.png"></img></a></span>		
						<%
						//}
						}
%>
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
					<tr>
<%
						if (strOpt.equals("PHONE"))
						{
%>					
						<td class="RightTableCaption" HEIGHT="24"  align="right"><fmtOrderQuote:message key="LBL_MODE_ORDER"/></td>
						<td  class="RightText" HEIGHT="24">&nbsp;<gmjsp:dropdown controlName="Cbo_Mode"  seletedValue="<%=strMode%>"  defaultValue= "[Choose One]"	
						tabIndex="-1"  value="<%=alMode%>" codeId="CODEID" codeName="CODENM" onChange="fnSetHardCopyPO();"/>
						&nbsp;&nbsp;<input class="RightText" type="checkbox" name="Chk_HardPO"/>&nbsp;<fmtOrderQuote:message key="LBL_HARD_COPY"/>
						</td> 
<%
						}else if (strOpt.equals("QUOTE")){
%>
						<td colspan="2"></td>
<%
						}
						if (strOpt.equals("PHONE")){
%>						
						<td class="RightTableCaption" HEIGHT="24"  align="right"><%=strCustPOLabel %>:</td>
						<td  class="RightText" HEIGHT="24" >&nbsp;<input type="text" size="20" maxlength="20" value="" name="Txt_PO" class="InputArea"   onFocus="changeBgColor(this,'#AACCE8');fnPOClearDiv();" onBlur="changeBgColor(this,'#ffffff');fnCheckPO('CHECKPO');fnPartNumTxtFocus();" tabindex=9>
						<%-- <% if (!strCountryCode.equals("en")){
						%> --%>
						<span id="DivShowPOIDAvail" style="vertical-align:middle; display: none;"> <img height="24" width="25" title="Customer PO available" src="<%=strImagePath%>/success.gif"></img></span>
						<span id="DivShowPOIDExists" style="vertical-align:middle; display: none;"><img title="Customer PO already exists" src="<%=strImagePath%>/delete.gif"></img>&nbsp;<a href="javascript:fnModifyOrder()"><img height="15" border="0" title="Order Details" src="<%=strImagePath%>/location.png"></img></a></span>		
						<%
						//}
						}
						if (strOpt.equals("PHONE") && !strCaseId.equals(""))
						{
%>	
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<font class="RightTableCaption"><fmtOrderQuote:message key="LBL_CASE_ID"/>&nbsp;&nbsp;:</font>&nbsp;<input type="text" size="20" maxlength="20" value="<%=strCaseId%>" name="Txt_CaseId" class="InputArea"   onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=9>
<%
						}
%>	
					</td>	 
					</tr>
				</table>
				</td>
			</tr>
		   	<tr><td class="Line" height="1"></td></tr>
<%
			if (strOpt.equals("QUOTE") || strShowSurgAtr.equals("YES"))
			{
%>		   	
			<tr bgcolor="#eeeee" class="RightTableCaption"><td height="25">&nbsp;<IMG id="tabQuoteimg" border=0 src="<%=strImagePath%>/plus.gif">&nbsp;<a href="javascript:fnShowFilters('tabQuote');" tabindex="-1" class="RightText"><%=strShowSurgAtr.equals("YES")?"Surgery Details":"Quote Details" %> </a></td></tr>
			<tr>
				<td>
					<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" id="tabQuote" style="display:none">
						<tr>
<%
				intSize = alQuoteDetails.size();
				hcboVal = new HashMap();
				for (i = 0; i < intSize; i++) 
				{			
					hcboVal = (HashMap) alQuoteDetails.get(i);
					strCodeID = (String) hcboVal.get("TRTYPEID");
					strCodeNm = (String) hcboVal.get("TRTYPE");
					if(strCodeID.equals("400113") || strCodeID.equals("401405")){strDiscountIndex = i;}
%>							
							<td class="RightTableCaption" HEIGHT="24"  align="right"><%=strCodeNm %>:</td>
							<td  class="RightText" HEIGHT="24" >&nbsp;<input type="text" size="30" value="" name="Txt_Quote<%=i %>" class="InputArea" id="<%=strCodeID %>"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=9
							<%if(strShowDiscount.equalsIgnoreCase("YES")&& (strCodeID.equals("400113") || strCodeID.equals("401405"))){ %> onkeyup="javascript:fnEnableDiscount(this);" onkeypress="return fnNumbersOnlyWithDecimal(event,this)"<%}%>></td>
<%
					if (i%2 == 1)
					{
%>									
						</tr>
						<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
						<tr>
<%
					}
				}
%>	
						</tr>
					</table>
					<input type="hidden" name="hQuoteCnt" value="<%=intSize %>">					
				</td>
			</tr>
			<tr><td class="Line" height="1"></td></tr>
<%
			}
%>			<input type="hidden" name="hDiscountIndex" value="<%=strDiscountIndex%>">
			<tr bgcolor="#fef2cc" class="RightTableCaption"><td height="25">&nbsp;<IMG id="tabCartimg" border=0 src="<%=strImagePath%>/minus.gif">&nbsp;<a href="javascript:fnShowFilters('tabCart');"  tabindex="-1" class="RightText"><fmtOrderQuote:message key="LBL_CART_DETAILS"/></a></td></tr>
			<tr>
				<td>
					<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" id="tabCart" style="display:block">
						<tr>			
							<td>
								<iframe src="/GmCommonCartServlet?hScreen=QUOTE&companyInfo=<%=strCompanyInfo%>" scrolling="no" id="fmCart" marginheight="0" width="100%" height="340"></iframe><BR>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<jsp:include page="/operations/shipping/GmIncShipDetails.jsp" >
						<jsp:param name="SHIPCHRG" value="Y" />
					</jsp:include>
				</td>
			</tr>	
			<tr><td colspan="2" class="Line" height="1"></td></tr>
			<tr>
				<td>
					<jsp:include page="/common/GmIncludeLog.jsp" >
					<jsp:param name="LogType" value="" />
					<jsp:param name="LogValue" value="" />
					<jsp:param name="LogMode" value="Edit" />
					</jsp:include>
				</td>
			</tr>
			<tr>
			<!-- 
			Including Credit Hold validation functionality. it will check account is on credit hold or not when Account id is enterted.
			If Account is on credit hold then it will show warning message and if function is not allowed then disable place order button. 
			104962 - Place Order
			 -->
				<td colspan="2">
					<jsp:include page="/accounts/GmCreditHoldInclude.jsp" >
					<jsp:param name="ALERT" value="Y"/>
					<jsp:param name="SUBTYPE" value="104962"/>
					</jsp:include>
				</td>
			</tr>
			<tr>
				<td  align="center" height="35">
					<fmtOrderQuote:message key="BTN_RESET" var="varBtnResetForm"/><gmjsp:button gmClass="ButtonReset" name="${varBtnResetForm}" style="height: 25px" accesskey="E"  onClick="fnReset();" buttonType="Load" value="Reset Form"/>&nbsp;
					<fmtOrderQuote:message key="LBL_BTNPLACEORD" var="varBtnPlaceOrd"/><gmjsp:button gmClass="Button" name="${varBtnPlaceOrd}"  accesskey="O" tabindex="18" onClick="fnPlaceOrderSubmit();" buttonType="Save" value="<%=strSubmit%>"/>
				</td>
			</tr>
			<tr><td colspan="6" height="1" class="Line"></td></tr>
			
			<tr>
			<td  colspan="6" > 
			<div id="divf" style="display:none;">
			<iframe src="/gmRuleEngine.do?companyInfo=<%=strCompanyInfo%>&RE_FORWARD=gmPhoneOrder&txnStatus=PROCESS&RE_TXN=50925" scrolling="yes" id="iframe1" marginheight="0" width="100%" height="100" frameborder="0"   ></iframe><BR>
			</div>
			</td></tr>	
			<tr><td colspan="6" height="1" class="Line"></td></tr>
		</TABLE>
			 
		<BR>
		<script language="JavaScript" type="text/javascript" src="<%=strJsPath%>/wz_tooltip.js"></script> 
		</FORM>	
	<%@ include file="/common/GmFooter.inc"%>
		</body>
	</html>