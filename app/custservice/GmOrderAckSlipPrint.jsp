 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>

<%
/**********************************************************************************
 * File		 		: GmOrderSlipPrint.jsp
 * Desc		 		: This screen is used for the print version of OrderSlip
 * Version	 		: 1.0
 * author			: Elango
************************************************************************************/
%>
<!-- \custservice\GmOrderAckSlipPrint.jsp -->



<%@ page language="java" %>
<%--
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
 --%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ include file="/common/GmHeader.inc" %>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);	
	log = GmLogger.getInstance(GmCommonConstants.CUSTOMERSERVICE);
	HashMap hmReturn = new HashMap();

	HashMap hmCartDetails = new HashMap();
	HashMap hmOrderDetails = new HashMap();
	HashMap hmOrderAttrib = null;
	hmReturn = (HashMap)request.getAttribute("ORDERDETAILS");
    hmOrderAttrib = (HashMap)request.getAttribute("ORDER_ATB"); 
    String strOrdType = GmCommonClass.parseNull((String)request.getAttribute("ORDERTYPE"));
	String strApplnDateFmt = strGCompDateFmt;
	String strApplnCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	String strInvoiceFormat = GmCommonClass.parseNull((String)request.getAttribute("ACKODRSLIPNM"));
	log.debug("strInvoiceFormat in JSP"+strInvoiceFormat);	
 	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
    GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmOrderPackSlip", strCompanyLocale );
    String strSetRbObjectFl=GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("RESOURCE_BUNDLE_OBJ"));
	
	String strOdrId  = "";
	String strCartOrdID = "";
	String subReportPath = "";
	java.sql.Date dtOrderDate = null;

	if (hmReturn != null)
	{  
	    strOdrId = GmCommonClass.parseNull((String)hmReturn.get("ID"));
	    subReportPath = GmCommonClass.parseNull((String)hmReturn.get("SUBREPORT_DIR"));
		hmOrderDetails.put("ID",strOdrId); 
        hmOrderDetails.put("ORDERDATE",GmCommonClass.getStringFromDate((java.sql.Date)hmReturn.get("ORDERDATE"),strApplnDateFmt));
        hmOrderDetails.put("ACKORDERON",hmOrderAttrib.get("ACKORDERON"));        
        hmOrderDetails.put("PO",GmCommonClass.parseNull((String)hmReturn.get("PO")));
        hmOrderDetails.put("ORDERMODE",GmCommonClass.parseNull((String)hmReturn.get("ORDERMODE")));
        hmOrderDetails.put("CREATEDBY",GmCommonClass.parseNull((String)hmReturn.get("CREATEDBY")));   
        hmOrderDetails.put("SHIPMODE",GmCommonClass.parseNull((String)hmReturn.get("SHIPMODE")));  
        hmOrderDetails.put("SHIPADD",GmCommonClass.parseNull((String)hmReturn.get("SHIPADD"))); 
        hmOrderDetails.put("BILLADD",GmCommonClass.parseNull((String)hmReturn.get("BILLADD"))); 
        hmOrderDetails.put("CURRSIGN",GmCommonClass.parseNull((String)hmReturn.get("CURRSIGN"))); 
		hmOrderDetails.put("PAYNM", GmCommonClass.parseNull((String)hmReturn.get("PAYNM")));
		hmOrderDetails.put("ACKREFID", GmCommonClass.parseNull((String)hmReturn.get("ACKREFID")));
		hmOrderDetails.put("ACCID", GmCommonClass.parseNull((String)hmReturn.get("ACCID")));
		hmOrderDetails.put("ORDER_COMMENT", GmCommonClass.parseNull((String)hmReturn.get("CMENT")));
		hmOrderDetails.put("GMCOMPANYIBAN",GmCommonClass.parseNull(GmCommonClass.getString("GMCOMPANYIBAN")));
        hmOrderDetails.put("GMCOMPANYBIC",GmCommonClass.parseNull(GmCommonClass.getString("GMCOMPANYBIC")));
        hmOrderDetails.put("CUSTREFID", GmCommonClass.parseNull((String)hmReturn.get("CUSTREFID")));
	}
	
	log.debug("strInvoiceFormat in JSP strOdrId "+strOdrId);	
	ArrayList alPopOrderItem = new ArrayList();
	ArrayList alOrderItem = new ArrayList();
	ArrayList alBackOrderItem = new ArrayList();
	HashMap hmOrderDetls = new HashMap();
	alOrderItem = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ITEMDETAILS"));
	alPopOrderItem = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alpopCartDetails"));
	alBackOrderItem = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("BACKORDERDETAILS"));
	hmOrderDetls = GmCommonClass.parseNullHashMap(((HashMap)request.getAttribute("ALORDERDETAILS")));
	hmOrderDetails.put("BACKORDERDETAILS",alBackOrderItem);
	hmOrderDetails.put("PARENTORDID", GmCommonClass.parseNull((String)hmOrderDetls.get("PARENTORDID")));
	
	log.debug("strInvoiceFormat in JSP Parent Order "+GmCommonClass.parseNull((String)hmOrderDetls.get("PARENTORDID")));	
	
	int intSize = alPopOrderItem.size();
	int intLoop = 0;
	ArrayList alLoop = new ArrayList();
	ArrayList allList = new ArrayList();
	ArrayList alHashMap = new ArrayList();
	HashMap hmTemp = new HashMap();
	HashMap hmLoop = new HashMap();
	String strVATPER = "";
	String blBackFl="";
	String blConFl="";
	//= GmCommonClass.parseNull((String)request.getAttribute("NIP"));
	//log.debug("strVATPER~~~~~~~~~~~~~~~~~~~~~~~~"+strVATPER+"~~~~"+strVATPER.length());
	double vatPer = 0.0;
	int intQty = 0;
	double dbItemTotal = 0.0;
	double dbTotalItemVAT = 0.0;
	double dbGrossTotal = 0.0;  
	double dbTotal = 0.0;
	String strItemTotal = "";
	String strTotal = "";
	String strPrice = "";
	String strQty = "";
	String strActualPrice="";
	String strBckQty="";
	double dbItemVATTotaltemp=0.0;
	double dbItemTotaltemp=0.0;
	double dbItemActTotalTemp=0.0;
	double dbItemActTotal=0.0;
	if(intSize==0){ 
		hmLoop.put( "ID", "-");
		hmLoop.put( "PDESC", "-");
		hmLoop.put( "QTY", "0");
		hmLoop.put( "PRICE", "0");
		hmLoop.put( "ACTUALPRICE", "0");
		hmLoop.put( "CONFL", "-");
		hmLoop.put( "ORDERID", "-"); 
		hmLoop.put( "BCKQTY", "0");
		hmLoop.put( "VAT", "-");
		hmLoop.put( "ACTUALPRICE", "0");
		hmOrderDetails.put("GROSSTOTAL", dbGrossTotal);
		hmOrderDetails.put("TOTALVAT", dbTotalItemVAT);
		hmOrderDetails.put("NETTOTAL",dbTotal);
		alHashMap.add(hmLoop);
	}
	log.debug("After HMLOOP ************"+dbTotal);
	
 			alLoop = GmCommonClass.parseNullArrayList((ArrayList)hmCartDetails.get(strCartOrdID));
			intLoop = alLoop.size(); 
			for ( int i=0;i<intSize;i++)
			{
				hmLoop = (HashMap)alPopOrderItem.get(i);
				log.debug("=========KEYS==========="+hmLoop);
				strVATPER = GmCommonClass.parseZero((String)hmLoop.get("VAT"));
				if(!strVATPER.equals("")){
					vatPer = Double.parseDouble(strVATPER)/ 100;
				}
				strPrice =GmCommonClass.parseZero((String)hmLoop.get("PRICE"));
				strQty = GmCommonClass.parseZero((String)hmLoop.get("QTY"));
				intQty = Integer.parseInt(strQty);
				dbItemTotal = Double.parseDouble(strPrice);
				dbItemTotal = intQty * dbItemTotal; // Multiply by Qty
				strActualPrice = GmCommonClass.parseZero((String)hmLoop.get("ACTUALPRICE"));
				log.debug("strActualPrice :::: "+strActualPrice);
				dbItemActTotalTemp = Double.parseDouble(strActualPrice);
				log.debug("dbItemActTotalTemp :::: "+dbItemActTotalTemp);
				dbItemActTotal = dbItemActTotal + (intQty * dbItemActTotalTemp);				
				if(!strVATPER.equals("")){
					dbItemTotaltemp= dbItemTotal + GmCommonClass.parseDouble(((Double)hmOrderDetails.get("TOTALPRICE"+strVATPER))) ;
					hmOrderDetails.put("TOTALPRICE"+strVATPER,dbItemTotaltemp);
					dbTotalItemVAT = dbTotalItemVAT  + (dbItemTotal * vatPer);
					dbItemVATTotaltemp= (dbItemTotal * vatPer) + GmCommonClass.parseDouble(((Double)hmOrderDetails.get("TOTALVATPRICE"+strVATPER))) ;
					hmOrderDetails.put("TOTALVATPRICE"+strVATPER,dbItemVATTotaltemp);
				}
				strItemTotal = ""+dbItemTotal;
				dbTotal = dbTotal + dbItemTotal;
				strBckQty = (String)hmLoop.get("BCKQTY");
				strTotal = ""+dbTotal;
				hmLoop.put( "ORDERID", strCartOrdID);
				hmLoop.put( "ITEMTOTAL", strItemTotal);
				if(strOrdType.equals("2520")){
					hmLoop.put( "BCKQTY","");
				}else{
					hmLoop.put( "BCKQTY", strBckQty.equals("")?"":strBckQty);
				}
				if(!strBckQty.equals("")){
				   blBackFl="Y";
				}
			    String strConFl= GmCommonClass.parseNull((String)hmLoop.get("CONFL"));
			    if(!strConFl.equals("")){
			    	blConFl="Y";
				}
				alHashMap.add(hmLoop);
			}
		dbGrossTotal = dbTotalItemVAT + dbTotal;
		hmOrderDetails.put("GROSSTOTAL", dbGrossTotal);
		hmOrderDetails.put("TOTALVAT", dbTotalItemVAT);
		hmOrderDetails.put("NETTOTAL",dbTotal);
		hmOrderDetails.put("TOTALACTUALPRICE",(Double)dbItemActTotal);	
		hmOrderDetails.put("BACKFL",blBackFl);
		hmOrderDetails.put("CONFL",blConFl);
		hmOrderDetails.put("SUBREPORT_DIR",subReportPath);
	if(hmOrderAttrib!=null && hmOrderAttrib.size()>0){            
            
            Set atrbSet = hmOrderAttrib.keySet();
            String strAtribName = "";
            for(Iterator it=atrbSet.iterator(); it.hasNext();){
                  strAtribName = (String)it.next();                     
                  hmOrderDetails.put(strAtribName,hmOrderAttrib.get(strAtribName));
            }
      }	
	
	log.debug("After HMLOOP  strOrdType ************"+strOrdType);
	hmOrderDetails.put("ORDER_TYPE",strOrdType);
	
	if(strOrdType.equals("2520")){
		hmOrderDetails.put("BACKFL","");
		hmOrderDetails.put("CONFL","");
	}
%>
<HTML>
<HEAD>
<TITLE> GlobusOne Enterprise Portal: Order Pack Slip Print</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script type="text/javascript">
function fnPrint()
{
	window.print();
}
var tdinnner = "";
var strObject="";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screenOus.css">

</HEAD>
<BODY leftmargin="0" topmargin="0" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM method="post" action = "<%=strServletPath%>/GmOrderItemServlet">
<iframe id="ifmPrintContents" style="height: 0px; width: 0px; position: absolute"></iframe>
<div id="jasper" > 
		<%
		log.debug("hmOrderDetails before going to Jasper"+hmOrderDetails);
			String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
			GmJasperReport gmJasperReport = new GmJasperReport();								
			gmJasperReport.setRequest(request);
			gmJasperReport.setResponse(response);
			gmJasperReport.setJasperReportName(strInvoiceFormat);
			gmJasperReport.setHmReportParameters(hmOrderDetails);		
			// to set the dynamic resource bundle object
	        if (strSetRbObjectFl.equalsIgnoreCase("YES")) {
	          gmJasperReport.setRbPaperwork(gmResourceBundleBean.getBundle());
	        }
	        if(intSize==0){ 
	        	gmJasperReport.setReportDataList(alPopOrderItem);
	        	gmJasperReport.createJasperHtmlReport();
	        }
	        else
	        {
	        	gmJasperReport.setReportDataList(alHashMap);
	        	gmJasperReport.createJasperReport();
	        }
				
		%>				
</div>
<table align="center" width="100%">
<tr>
	<td align="center" id="button">
		<input type="button" value="Print" class="Button" onclick="fnPrint();"/>
 		<input type="button" value="&nbsp;Close&nbsp;" name="Btn_Close" class="button" onClick="window.close();" >
 	</td>
 </tr>
 </table>		
</FORM>
</BODY>
</HTML>
