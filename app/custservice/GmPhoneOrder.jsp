
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.net.URLEncoder"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%> 
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ page import="java.util.StringTokenizer" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- custservice\GmPhoneOrder.jsp -->
<fmt:setLocale value="<%=strLocale%>"/>
<fmt:setBundle basename="properties.labels.custservice.GmPhoneOrder"/>
<%
	response.setHeader("Cache-Control", "no-cache, post-check=0, pre-check=0"); //HTTP 1.1
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0
	response.setDateHeader("Expires", 0); //prevents caching at the proxy server 
	
		GmServlet gm = new GmServlet();
		gm.checkSession(response, session);
		
		GmCalenderOperations gmCal = new GmCalenderOperations();
		// to setting the time zone
		gmCal.setTimeZone(strGCompTimeZone);
		String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
		GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
		String strWikiTitle = "";
		String strApplDateFmt = strGCompDateFmt;
		String strOpt = GmCommonClass.parseNull((String)session.getAttribute("strSessOpt"));
		String strCurrSym =  GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
		String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
		String strCurDate = gmCal.getCurrentDate(strGCompDateFmt);
		strCountryCode = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("COUNTRYCODE"));
		String strLotOverideAccess = GmCommonClass.parseNull((String)request.getAttribute("LOTOVERRIDEACCESS"));
		String strSkpUsdLotValid =  GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SKP_USED_LOT_VALID"));
		// Get the flag to check whether the company is enabled for viacell parts or not
		String strViaCellCmpnyFl = GmCommonClass.parseNull((String)request.getAttribute("VIACELLCMPNY"));
		// Get the flag to check whether the plant can store viacell parts or not
		String strViaCellPlantFl = GmCommonClass.parseNull((String)request.getAttribute("VIACELLPLANT"));
		String strSurgeryDtInfo = GmCommonClass.parseNull((String)request.getAttribute("SURGERYDTINFO"));
		java.sql.Date dtFromDate = null;
		java.sql.Date dtToDate = null;	
		java.sql.Date dtSurgDate = null;
		String strAccId = "";
		String strCaseId = "";
		String strCaseInfoId = "";
		String strParentOrdId = "";
		String strDODateFmt="";
		String strTagColumn = GmCommonClass.parseNull((String)request.getAttribute("TagColumn"));
		String strCapFlColumn = GmCommonClass.parseNull((String)request.getAttribute("CapFlColumn"));
		
		if (strOpt.equals("PHONE"))  {  
				strDODateFmt=GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.DO_FMT_ORDER"));
		     	strWikiTitle = GmCommonClass.parseNull((String)GmCommonClass.getWikiTitle("SALES_ORDER"));
		}else if (strOpt.equals("QUOTE"))  {  
			    strDODateFmt=GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.DO_FMT_QUOTE"));
			    strWikiTitle = GmCommonClass.parseNull((String)GmCommonClass.getWikiTitle("NEW_QUOTE"));
		}
		String strFmtedDt  =  GmCommonClass.parseNull(GmCommonClass.getStringFromDate(GmCommonClass.getStringToDate(strCurDate,strApplDateFmt), strDODateFmt));
		HashMap hmReturn = (HashMap) request.getAttribute("hmReturn");
		HashMap hmDefaultShipVal = new HashMap();
		//HashMap hmShip = (HashMap)request.getAttribute("ShippingInfo"); 

		HashMap hmCaseInfo  = (HashMap) request.getAttribute("HMCASEINFO");
		if (hmCaseInfo != null) {
			strAccId = GmCommonClass.parseNull((String) hmCaseInfo.get("ACCTID"));
			strCaseId = GmCommonClass.parseNull((String) hmCaseInfo.get("CASEID"));
			strParentOrdId = GmCommonClass.parseNull((String) hmCaseInfo.get("PARENTID"));
		}
		String strhAction = (String) request.getAttribute("hAction");
		if (strhAction == null) {
			strhAction = (String) session.getAttribute("hAction");
		}
		strhAction = (strhAction == null) ? "Load" : strhAction;

		String strShowSurgAtr = GmCommonClass.parseNull( gmResourceBundleBean.getProperty("DO.SHOW_SURG_ATRB_DET"));
		String strShowDiscount = GmCommonClass.parseNull( gmResourceBundleBean.getProperty("DO.SHOW_DISCOUNT"));
		String strOrdType =GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.PHN_ORD_TYPE"));
		String strOrdHoldSts =GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.ORDER_HOLD_STATUS"));
		String strShowDOID = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.SHOW_DO_ID"));
		String strSelected = "";
		String strCodeID = "";
		String strChecked = "";
		String strDistName = "";
		String strCodeNm = "";
		int strDiscountIndex = -1; // ONly when we have discount attribute,only then the index should have a value.We cannot have 0 as default value.	
		
		String strMode = "0";
		strOrdType = strOrdType.equals("") ? "2521": strOrdType;
		String strShipCarr = "";
		String strShipMode = "";
		String strShipTo = "";
		String strEGPSUsage = "0"; // PC-5892 default value set 'No eGPS' for Capital Equp. Used
		
		
		ArrayList alMode = new ArrayList();
		ArrayList alShipTo = new ArrayList();
		ArrayList alAccount = new ArrayList();
		ArrayList alRepList = new ArrayList();
		ArrayList alOrderType = new ArrayList();
		ArrayList alCarrier = new ArrayList();
		ArrayList alShipMode = new ArrayList();
		ArrayList alAllRepList = new ArrayList(); // Active and inactive.
		ArrayList alQuoteDetails = new ArrayList();
		ArrayList alEGPSUsage = new ArrayList();
		
		if (hmReturn != null) {
			alMode = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ORDERMODE"));
			alAccount = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ACCLIST"));
			alShipTo = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("SHIPTO"));
			alRepList = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("REPLIST"));
			alOrderType = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ORDTYPE"));
			alQuoteDetails = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("QUOTELIST"));
			alEGPSUsage = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("EGPSUSAGE"));
			//alAllRepList = (ArrayList) hmReturn.get("ALLREPLIST"); Sale Rep Change
			hmDefaultShipVal = (HashMap)hmReturn.get("DEFAULTSHIPMODECARRVAL");
			if(hmDefaultShipVal.size() > 0){
			strShipCarr = GmCommonClass.parseNull((String)hmDefaultShipVal.get("SHIP_CARRIER"));
			strShipMode = GmCommonClass.parseNull((String) hmDefaultShipVal.get("SHIP_MODE"));
			strShipTo = GmCommonClass.parseNull((String) hmDefaultShipVal.get("SHIP_TO"));
			}
		}
		// The default value is getting from DB, if there are not value in the rules table, then need to select choose one for other than US company
		//5001: Fedex; 5004: Priority Overnight; 4121:Sales Rep; 4122: Hospital
		strShipCarr = strShipCarr.equals("") && strCountryCode.equals("en")?"5001" : strShipCarr;
		strShipMode = strShipMode.equals("") && strCountryCode.equals("en")?"5004" : strShipMode;
		strShipTo = strShipTo.equals("")?(strCountryCode.equals("en")?"4121":"4122") : strShipTo;

		/*	if (hmShip != null)
		 {
		 alCarrier = (ArrayList)hmShip.get("CARRIER");
		 alShipMode = (ArrayList)hmShip.get("MODE");
		 alShipTo = (ArrayList)hmShip.get("SHIPLIST");
		 alRepList = (ArrayList)hmShip.get("REPLIST");
		 }*/

		int intSize = 0;
		HashMap hcboVal = null;
		int i = 0;

		String path = request.getContextPath();
		String basePath = request.getRemoteAddr() + request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmPhoneOrder", strSessCompanyLocale);
		// Quote Changes // 
		String strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("TD_PHONE_HEADER"));
		String strDOLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PHONE_DO_ID"));
		String strCustPOLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PHONE_CUST_PO"));
		String strSubmit = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("BTN_PHONE_SUBMIT"));
		
		String strLblSurgDtls = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SURG_DTLS"));
		String strLblQuoteDtls = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_QUOTE_DTLS"));
		String strAddTags = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("BTN_ADD_TAGS"));
		
		if (strOpt.equals("QUOTE"))
		{
			strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("TD_QUOTE_HEADER"));
			strDOLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_QUOTE_DO_ID"));
			strCustPOLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_QUOTE_CUST_REF"));
			strSubmit = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("BTN_QUOTE_SUBMIT"));
		}
		// Get the company information from gmDataStoreVO
		String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
		strCompanyInfo = URLEncoder.encode(strCompanyInfo);

		String strLotOverrideFl = "disabled";
		if(strLotOverideAccess != "N"){
			strLotOverrideFl = "";
		}
		//To fix the allignment issue for surgery date 
		String strSurgWidth=(strSessCompanyLocale.equals("JP"))?"101px":"11%";
		
		//code change for PMT-39389 To make NPI details mandatory for orders
		String strNPIMandatoryFl = GmCommonClass.parseNull((String)request.getAttribute("NPIMANDATORYFLAG"));
		String strNPIOrderType = GmCommonClass.parseNull((String)request.getAttribute("NPIORDERTYPES"));
		String strNewRowLot = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.NEWROWLOT")).equals("")?"NO":"YES";
	 %>

<SCRIPT>
var lotOverrideAccess = '<%=strLotOverideAccess%>';
var strOpt = '<%=strOpt%>';
var vshipMode = '<%=strShipMode%>';
var vshipCarrier = '<%=strShipCarr%>';
var vshipTo = '<%=strShipTo%>';
var strAccSessCurrency ='<%=strCurrSym%>';
var vcountryCode = '<%=strCountryCode%>';
var vcustPOLabel = '<%=strCustPOLabel%>';
var dateFmt = '<%=strApplDateFmt%>';
var strShowDOID ='<%=strShowDOID%>'; 
var skpUsdLotValid = '<%=strSkpUsdLotValid%>';
var viaCellCompFl = '<%=strViaCellCmpnyFl%>';
var viaCellPlantFl = '<%=strViaCellPlantFl%>';
var todaysDate = '<%=strCurDate%>';
var lblSurgeryDt = '<fmt:message key="LBL_SURGERY_DATE"/>';
var surgeryDateInfo  = '<%=strSurgeryDtInfo%>';
var vNPIMandatoryFl  = '<%=strNPIMandatoryFl%>';
var vNPIOrderType  = '<%=strNPIOrderType%>';
function SetDefaultValues(){
	if(document.frmPhOrder.Txt_AccId.value == ''){
	 	document.frmPhOrder.Btn_AddTags.disabled = true; 
	  } else {
		document.frmPhOrder.Btn_AddTags.disabled = false; 
	  }
	
	document.all.shipCarrier.value="<%=strShipCarr%>";
	document.all.shipMode.value="<%=strShipMode%>";
	document.all.shipTo.value="<%=strShipTo%>";
	fnGetNames(document.all.shipTo); 
	fnAcIdBlur(document.all.Txt_AccId);
	var orderTyp = document.frmPhOrder.Cbo_OrdType.value;
	// Checking the order type (Direct/Intercompany) then, to disable the Type and control # field
	if (orderTyp == 26240232 || orderTyp == 26240233) // 26240232 Direct 26240233 Intercompany
	{
		fnCartPut ('Cbo_OrdPartType', true);
		fnCartPut ('Txt_Cnum', true);
		fnCartEnableDisableFields ();
	}
	//show surgery date mandatory symbol while raising Bill&ship,BOSC and Bill only loaner for japan
	var surgeryDtMand = '';
	if (strOpt == 'PHONE' && surgeryDateInfo != ''){
		var fl = fnSurDtMandFl();
			if(fl == true){
				 document.getElementById("divMandSurgDt").style.display = 'inline';
			}else{
				document.getElementById("divMandSurgDt").style.display = 'none';
			}
	}
		
	/* if (strOpt == "PHONE")  -- NSP-398 Changes
	{
		var modeval = document.frmPhOrder.Cbo_Mode.value;
	
		if (modeval == '5017')
		{
			document.frmPhOrder.Chk_HardPO.disabled = true;
		}else{
			document.frmPhOrder.Chk_HardPO.disabled = false;
		}
	} */
}

</SCRIPT>
<html>
<head>
	<title> GlobusOne Enterprise Portal: New Order/Quote Process </title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252"> 
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
	<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/GmPhoneOrder.js"></script>
	<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
	<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
	
	
	<link rel="stylesheet" href="/styles/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="/javascript/common/bootstrap.min.css">
<link rel="stylesheet" href="/styles/font-awesome.css">
<link rel="stylesheet" href="/styles/GmNPIProcess.css">
<script src="/javascript/common/jquery.min.js"></script>
<script src="/javascript/common/bootstrap.min.js"></script>

</head>

<BODY leftmargin="20" topmargin="10"  onload="SetDefaultValues();">
<FORM name="frmPhOrder" method="POST" action="<%=strServletPath%>/GmOrderItemServlet">
<input type="hidden" name="hAction" value="PlaceOrder">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hAdjInputStr" value="">
<input type="hidden" name="hOpt" value="<%=strOpt %>">
<input type="hidden" name="hTotal" value="">
<input type="hidden" name="hConCode" value="">
<input type="hidden" name="hRepId" id="hRepId" value="">
<input type="hidden" name="hGpoId" value="">
<input type="hidden" name="hSysDate" value="<%=strFmtedDt%>"> 
<input type="hidden" name="dateFormat" value="<%=strDODateFmt%>">
<input type="hidden" name="hQuoteStr" value="">
<input type="hidden" name="hHoldFl" value="">
<input type="hidden" name="hHoldSts" value="<%=strOrdHoldSts%>">
<input type="hidden" name="hAccDiscnt" value="">
<input type="hidden" name="hAccCurrency" value="">
<input type="hidden" name="hTagColumn" value="<%=strTagColumn %>">
<input type="hidden" name="hCapFlColumn" value="<%=strCapFlColumn %>">
<!-- MNTTASK - 8623 - Create input string for Sterile parts -->
<input type="hidden" name="hCtrlNumberStr" value="">
<input type="hidden" name="hOrderId" id="hOrderId" value="">
<input type="hidden" name="hIsCreditHold">
<input type="hidden" name="RE_FORWARD" value="gmOrderItemServlet">
<input type="hidden" name="tempId" value="">
<input type="hidden" name="hNewRowLot" value="<%=strNewRowLot%>">
<input type="hidden" name="RE_TXN" value="50925">

<input type="hidden" name="RE_SRC" value="/gmRuleEngine.do?companyInfo=<%=strCompanyInfo%>&RE_FORWARD=gmPhoneOrder&txnStatus=PROCESS&RE_TXN=50925">


<TABLE border="0" width="980" cellspacing="0" cellpadding="0">
			<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
			<tr>
				<td bgcolor="#666666" width="1" rowspan="15"></td>
				<td height="25" width="960"  ><table cellspacing="0" cellpadding="0" border="0" width = 100%><tr>
				<td  class="RightDashBoardHeader"><%=strHeader %></td>
<%				if(strOpt.equals("PHONE") || strOpt.equals("QUOTE")) { 
%>				
				<td height="25" class="RightDashBoardHeader" align="right">
				<img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
			    </td>
<%				}else{
%>
				<td height="25" class="RightDashBoardHeader" width="20px"></td>
<%				} 
%>
			    </tr>
			    </table> 
				<td bgcolor="#666666" width="1" rowspan="15"></td>
			</tr>
			<tr><td bgcolor="#666666" height="1"></td></tr>
			<tr bgcolor="#e4e6f2" class="RightTableCaption"><td height="25">&nbsp;<IMG id="tabBillimg" border=0 src="<%=strImagePath%>/minus.gif">&nbsp;<a href="javascript:fnShowFilters('tabBill');" tabindex="-1" class="RightTableCaption"><fmt:message key="IMG_BILL_INFO"/></a></td></tr>
			<tr>
				<td>
					<TABLE border="0" width="978" cellspacing="0" cellpadding="0" id="tabBill">
						<tr>
							<td class="RightTableCaption" HEIGHT="24" align="right"><font color="red">*</font> <fmt:message key="LBL_ACCT_ID"/>:</td>
							<td class="RightText">&nbsp;<input type="text" size="8" value="<%=strAccId%>" name="Txt_AccId" id="Txt_AccId" class="InputArea" tabindex=1 onFocus="changeBgColor(this,'#AACCE8');" onBlur="javascript:fnAcIdBlur(this); fnEnableAddTagBtn();" onchange="fnEnableAddTagBtn();"> &nbsp;&nbsp;<span class="RightTextBlue"></span></td>
						</tr>
						<!-- Custom tag lib code modified for JBOSS migration changes -->
						<tr class="oddshade">
							<td class="RightTableCaption" HEIGHT="24" align="right"><fmt:message key="LBL_ACCT_NM"/>:</td>
							<td class="RightText" HEIGHT="24" colspan="3">
							<table><tr HEIGHT="16"><td>					
							<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value=" Cbo_BillTo" />
							<jsp:param name="METHOD_LOAD" value="loadAccountList&searchType=LikeSearch" />
							<jsp:param name="WIDTH" value="500" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="CONTROL_NM_VALUE" value="" />
							<jsp:param name="CONTROL_ID_VALUE" value="" />
							<jsp:param name="SHOW_DATA" value="50" />
							<jsp:param name="ON_BLUR" value=" fnAcBlur(this); fnEnableAddTagBtn();" />
							</jsp:include>	
							</td></tr>
							<tr HEIGHT="8"><td></td></tr>
							</table>
													
							</TD>
						</tr>
						<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
						<tr>
							<td colspan="4" bgcolor="#eeeeee">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<IMG id="tabAcSumimg" border=0 src="<%=strImagePath%>/minus.gif">&nbsp;<a href="javascript:fnShowFilters('tabAcSum');" tabindex="-1" class="RightTableCaption"><i><fmt:message key="IMG_SUMMARY"/></i></a>
								<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" id="tabAcSum" style="display:none">
									<tr>
										<td class="RightText" align="Right"><fmt:message key="LBL_REGN"/>:</td>
										<td id="Lbl_Regn" class="RightText"></td>
										<td class="RightText" align="Right"><fmt:message key="LBL_AREA_DIR"/>:</td>
										<td id="Lbl_AdName" class="RightText"></td>
										<td class="RightText" align="Right"><fmt:message key="LBL_FIELD_SALES"/>:</td>
										<td id="Lbl_DName" class="RightText"></td>
									</tr>
									<tr>
										<td class="RightText" align="Right"><fmt:message key="LBL_TERR"/>:</td>
										<td id="Lbl_TName" class="RightText"></td>
										<td class="RightText" align="Right"><fmt:message key="LBL_REP"/>:</td>
										<td id="Lbl_RName" class="RightText"></td>
										<td class="RightText" align="Right"><fmt:message key="LBL_GRP_PRICE_BK"/>:</td>
										<td id="Lbl_PName" class="RightText"></td>
									</tr>
									<tr>
										<td class="RightText" align="Right" valign ="top" width="100"><fmt:message key="LBL_ACCT_INFO"/>:</td>
										
								 	<% 	if(strCountryCode.equals("jp")){ %> 
								    	<td id="Lbl_AccInfo" class="RightText"></td>
										<td class="RightText" align="Right"><fmt:message key="LBL_DNAME"/>:</td>
										<td id="Lbl_Dname" class="RightText"></td>
										<td class="RightText" align="Right"><fmt:message key="LBL_DEALER_ID"/>:</td>
										<td id="Lbl_DId" class="RightText"></td>
								   </tr>
								   <tr>
								   <td class="RightText" align="Right" valign ="top" width="100"><fmt:message key="LBL_CLOSING_DATE"/>:</td>
										<td id="Lbl_Closing_Date" class="RightText" colspan="5"  width="750"></td>
								  
								 <%}else{%> 
								 <td id="Lbl_AccInfo" class="RightText" colspan="5"  width="750"></td>
								 <%} %> 
								 </tr>
									<tr><td colspan="6" class="LLine"></td></tr>
								</table>
							</td>
						</tr>
						<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
<%
						if (strOpt.equals("PHONE"))
						{
%>						
						<tr class ="oddshade">
							<td  class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<font color="red">*</font> <fmt:message key="LBL_ORD_TYPE"/>:</td>
							<td  class="RightText" colspan="3">&nbsp;<select name="Cbo_OrdType" id="Region" onChange="fnSetShipSel();fnCalcShipCharge();fnShowSurgDtMandSymb()" class="RightText" tabindex="-1" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
<%
								intSize = alOrderType.size();
									hcboVal = new HashMap();
									for (i = 0; i < intSize; i++) // Hardcoding to remove rest of order types - James 03/31
									{
										hcboVal = (HashMap) alOrderType.get(i);
										strCodeID = (String) hcboVal.get("CODEID");
										strSelected = strOrdType.equals(strCodeID) ? "selected" : "";
							%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
	                                }
%>
							</select>
						</td>
<%
						}else if (strOpt.equals("QUOTE"))
						{
%>
						<tr>
							<td colspan="2"><input type="hidden" name="Cbo_OrdType" value="2520"></td>
						</tr>
<%
				        }

%>
					 </tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>


                          <tr>  
                          <td class="RightTableCaption" align="right" width="<%=strSurgWidth%>"><span id="divMandSurgDt" style="vertical-align:middle; display: none;"><font color="red">*</font></span>&nbsp;<fmt:message key="LBL_SURGERY_DATE"/>:</td>
									<td>&nbsp;<input type="text" size="10" name="Txt_SurgDate" id="Txt_SurgDate" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');fnSetDOID('tabout','');" tabindex="2">
                                    <img id="Img_Date" style="cursor:hand"onclick="javascript:showSglCalendar('dcalendardiv','Txt_SurgDate');" title="Click to open Calendar" src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />
                                     <div id="dcalendardiv" style="position: absolute; z-index: 10;"></div>
						</td> 
						<td class="RightTableCaption" HEIGHT="23" align="right"><fmt:message key="LBL_DT_ENTER"/>:</td>
						<td>&nbsp;<%=strCurDate%></td>
<%
						if (strOpt.equals("QUOTE")){//To fix the allignment issue
%>
						<td class="RightTableCaption" HEIGHT="24" align="right">&nbsp;<fmt:message key="LBL_REQ_BY"/>:</td>
						<td class="RightText" HEIGHT="24">&nbsp;<input type="text" size="20" maxlength="100" value="" name="Txt_ReqBy" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="4"> </td>
<%
						}
%>
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
					<%-- Sale Rep Change
					<tr>
						<td class="RightTableCaption" HEIGHT="24" align="right"><font color="red">*</font> Sales Rep :</td>
						<td class="RightText" HEIGHT="24" colspan="3">
						    &nbsp;<select name="Cbo_Rep" tabindex="-1" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
							<option value="0">[Choose One]</option>
							<%
								intSize = alAllRepList.size();
									hcboVal = new HashMap();
									for (i = 0; i < intSize; i++) {
										hcboVal = (HashMap) alAllRepList.get(i);
										strCodeID = (String) hcboVal.get("REPID");
							%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
							<%
								}
							%>
						</select> &nbsp;</td>
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
					--%>
					<tr class="oddshade">
						<td class="RightTableCaption" HEIGHT="24"  align="right"><font color="red">*</font> <%=strDOLabel %>:</td>
						<td class="RightText" HEIGHT="24" >&nbsp;<input type="text" size="20" maxlength="20" value="" name="Txt_OrdId" id="Txt_OrdId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');fnClearDiv();" onBlur="changeBgColor(this,'#ffffff');fnCheckOrderId(this,'<%=strDOLabel%>');" tabindex="3"/>
						<span id="DivShowDOIDAvail" style="vertical-align:middle; display: none;"> <img height="24" width="25" title="<%=strDOLabel %> available" src="<%=strImagePath%>/success.gif"></img></span>
						<span id="DivShowDOIDExists" style=" vertical-align:middle; display: none;"><img title="<%=strDOLabel %> already exists" src="<%=strImagePath%>/delete.gif"></img></span></td>
<%
						if (strOpt.equals("PHONE"))
						{
%>
						<td class="RightTableCaption" HEIGHT="24" align="right">&nbsp;<fmt:message key="LBL_PARENT_DO_ID"/>:</td>
						<td class="RightText" HEIGHT="24">&nbsp;<input type="text" size="20" maxlength="20" value="<%=strParentOrdId%>" name="Txt_ParentOrdId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="4"> 
						&nbsp;<a onClick="javascript:fnPrintPack();"><img src=<%=strImagePath%>/question.gif border=0></img></a></td>
<%
						}else if(strOpt.equals("QUOTE")){//To fix the allignment 
%>	
						<td class="RightTableCaption" HEIGHT="24"  align="right"><%=strCustPOLabel %>:</td>
						<td  class="RightText" HEIGHT="24" >&nbsp;<input type="text" size="20" maxlength="100" value="" name="Txt_PO" class="InputArea"   onFocus="changeBgColor(this,'#AACCE8');fnPOClearDiv();" onBlur="changeBgColor(this,'#ffffff');fnCheckPO('CHECKPO');fnPartNumTxtFocus();" tabindex=9>
						<!-- Removing the condition to make the customer po validation functionality available for US also -->
						<%-- <% if (!strCountryCode.equals("en")){
						%> --%>
						<span id="DivShowPOIDAvail" style="vertical-align:middle; display: none;"> <img height="24" width="25" title="Customer PO available" src="<%=strImagePath%>/success.gif"></img></span>
						<span id="DivShowPOIDExists" style="vertical-align:middle; display: none;"><img title="Customer PO already exists" src="<%=strImagePath%>/delete.gif"></img>&nbsp;<a href="javascript:fnModifyOrder()"><img height="15" border="0" title="Order Details" src="<%=strImagePath%>/location.png"></img></a></span>		
						<%
						//}
						}
%>
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
					<tr>
<%
						if (strOpt.equals("PHONE"))
						{
%>					
						<td class="RightTableCaption" HEIGHT="24"  align="right"><font color="red">*</font> <fmt:message key="LBL_MODE_ORD"/>:</td>
						<td  class="RightText" HEIGHT="24">&nbsp;<gmjsp:dropdown controlName="Cbo_Mode"  seletedValue="<%=strMode%>"  defaultValue= "[Choose One]"	
						tabIndex="9"  value="<%=alMode%>" codeId="CODEID" codeName="CODENM" onChange="fnSetHardCopyPO();"/>
						&nbsp;&nbsp;<input class="RightText" type="checkbox" name="Chk_HardPO"/>&nbsp;<fmt:message key="LBL_HARD_PO"/>
						</td> 
<%
						}else if (strOpt.equals("QUOTE")){
%>
						<td colspan="2"></td>
<%
						}
						if (strOpt.equals("PHONE")){
%>
						<td class="RightTableCaption" HEIGHT="24"  align="right"><%=strCustPOLabel %>:</td>
						<td class="RightText" HEIGHT="24" >&nbsp;<input type="text" size="20" maxlength="20" value="" name="Txt_PO" class="InputArea"   onFocus="changeBgColor(this,'#AACCE8');fnPOClearDiv();" onBlur="changeBgColor(this,'#ffffff');fnCheckPO('CHECKPO');fnPartNumTxtFocus();" tabindex=9>
						<%-- <% if (!strCountryCode.equals("en")){
						%> --%>
						<span id="DivShowPOIDAvail" style="vertical-align:middle; display: none;"> <img height="24" width="25" title="Customer PO available" src="<%=strImagePath%>/success.gif"></img></span>
						<span id="DivShowPOIDExists" style="vertical-align:middle; display: none;"><img title="Customer PO already exists" src="<%=strImagePath%>/delete.gif"></img>&nbsp;<a href="javascript:fnModifyOrder()"><img height="15" border="0" title="Order Details" src="<%=strImagePath%>/location.png"></img></a></span>		
						<%
						//}
						}
						if (strOpt.equals("PHONE") && !strCaseId.equals(""))
						{
%>	
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<font class="RightTableCaption"><fmt:message key="LBL_CASE_ID"/>:</font>&nbsp;<input type="text" size="20" maxlength="20" value="<%=strCaseId%>" name="Txt_CaseId" class="InputArea"   onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=9>
<%
						}
%>	
					</td>	 
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="8"></td></tr>
					<tr class="shade" height="25">
						<td class="RightTableCaption" HEIGHT="24" align="right"><font color="red">*</font> <fmt:message key="LBL_NPI"/>:</td>
						<!-- <td class="RightText" HEIGHT="24" >&nbsp;<input type="text" size="20" value="" name="Txt_Npi" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"></td> -->
						<td class="RightText" HEIGHT="24" >
							<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="Txt_Npi" />
							<jsp:param name="METHOD_LOAD" value="loadSurgeonName&strOpt=npilist" />
							<jsp:param name="WIDTH" value="155" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="CONTROL_NM_VALUE" value="" />
							<jsp:param name="CONTROL_ID_VALUE" value="" />
							<jsp:param name="SHOW_DATA" value="40" />
							<jsp:param name="TAB_INDEX" value="10" />
							<jsp:param name="AUTO_RELOAD" value="fnSetSurgeonNm(this);" />
							<jsp:param name="ON_BLUR" value="fnAutoFocus(this);" />
							</jsp:include>	
						</td>
						<td class="RightTableCaption" HEIGHT="24" align="right"><font color="red">*</font> <fmt:message key="LBL_SURGEON"/>:</td>
						<!-- <td class="RightText" HEIGHT="24" >&nbsp;<input type="text" size="20" value="" name="Txt_Surgeon" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"> -->
						<td class="RightText" HEIGHT="24" >
							<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="Txt_Surgeon" />
							<jsp:param name="METHOD_LOAD" value="loadSurgeonName&strOpt=surgeonlist" />
							<jsp:param name="WIDTH" value="200" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="CONTROL_NM_VALUE" value="" />
							<jsp:param name="CONTROL_ID_VALUE" value="" />
							<jsp:param name="SHOW_DATA" value="40" />
							<jsp:param name="TAB_INDEX" value="11" />
							<jsp:param name="AUTO_RELOAD" value="fnSetNPINum(this);" />
							<jsp:param name="ON_BLUR" value="fnAutoFocus(this);" />
							</jsp:include>	
						<div id="npidtls" style="display: none;">&nbsp;
						<jsp:include page="/custservice/GmNPIProcess.jsp" >															 
							<jsp:param name="npiTxnId" value='' />
						</jsp:include>
						</div> 
						</td>
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
					<tr>
    					<td class="RightTableCaption" HEIGHT="24" align="left" colspan="2">
         					<table>
             					<tr>
                  					<td class="RightTableCaption" HEIGHT="24" align="right" colspan="4"><fmt:message key="LBL_OVERRIDE_LOT_REQ"/>:</td>
                  					<td class="RightText" HEIGHT="24" align="right" colspan="2">&nbsp;<input class="RightText" type="checkbox" <%=strLotOverrideFl %> name="Chk_LotOverride"/></td>
								</tr>
							</table>
						</td>
						<td class="RightTableCaption" HEIGHT="24" align="right" colspan="4">
						<table style="width:100%;">
					   <tr>
						<td class="RightTableCaption" HEIGHT="24" style="width:32%;" align="right"><fmt:message key="LBL_CAPITAL_EQUP_USED"/>:</td>
						<td class="RightText" HEIGHT="24">&nbsp;<gmjsp:dropdown controlName="Cbo_egpsusage" seletedValue="<%=strEGPSUsage%>"  defaultValue= "[Choose One]"	
						tabIndex="9"  value="<%=alEGPSUsage%>" codeId="CODEID" codeName="CODENM"/>
						</td>
						<td class="RightTableCaption" HEIGHT="24">
						<fmt:message key="BTN_ADD_TAGS" var="varAddTag"/>
			              <gmjsp:button gmClass="Button" name="Btn_AddTags" accesskey="O" tabindex="18" buttonType="Save" onClick="fnAddTags()" value="${varAddTag}"/>
			            </td>
						</tr>
						</table>
						</td>
					</tr>					
				</table>
				</td>
			</tr>
		   	<tr><td class="Line" height="1"></td></tr>
<%
			if (strOpt.equals("QUOTE") || strShowSurgAtr.equals("YES"))
			{
%>		   	
			<tr bgcolor="#eeeee" class="RightTableCaption"><td height="25">&nbsp;<IMG id="tabQuoteimg" border=0 src="<%=strImagePath%>/plus.gif">&nbsp;<a href="javascript:fnShowFilters('tabQuote');" tabindex="-1" class="RightText"><%=strShowSurgAtr.equals("YES")?strLblSurgDtls:strLblQuoteDtls %> </a></td></tr>
			<tr>
				<td>
					<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" id="tabQuote" style="display:none">
						<tr>
<%
				intSize = alQuoteDetails.size();
				hcboVal = new HashMap();
				for (i = 0; i < intSize; i++) 
				{			
					hcboVal = (HashMap) alQuoteDetails.get(i);
					strCodeID = (String) hcboVal.get("TRTYPEID");
					strCodeNm = (String) hcboVal.get("TRTYPE");
					if(strCodeID.equals("400113") || strCodeID.equals("401405")){strDiscountIndex = i;}
%>							
							<td class="RightTableCaption" HEIGHT="24"  align="right"><%=strCodeNm %>:</td>
							<td  class="RightText" HEIGHT="24" >&nbsp;<input type="text" size="30" value="" name="Txt_Quote<%=i %>" class="InputArea" id="<%=strCodeID %>"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=9
							<%if(strShowDiscount.equalsIgnoreCase("YES")&& (strCodeID.equals("400113") || strCodeID.equals("401405"))){ %> onkeyup="javascript:fnEnableDiscount(this);" onkeypress="return fnNumbersOnlyWithDecimal(event,this)"<%}%>></td>
<%
					if (i%2 == 1)
					{
%>									
						</tr>
						<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
						<tr>
<%
					}
				}
%>	
						</tr>
					</table>
					<input type="hidden" name="hQuoteCnt" value="<%=intSize %>">					
				</td>
			</tr>
			<tr><td class="Line" height="1"></td></tr>
<%
			}
%>			<input type="hidden" name="hDiscountIndex" value="<%=strDiscountIndex%>">
			<tr bgcolor="#fef2cc" class="RightTableCaption">
				<td height="20" > &nbsp;
				<IMG id="tabCartimg" border=0 src="<%=strImagePath%>/minus.gif">&nbsp;<a href="javascript:fnShowFilters('tabCart');"  tabindex="-1" class="RightTableCaption"><fmt:message key="IMG_CART_DTLS"/></a>
				 </td>
			</tr>
			
			<tr>
				<td>
					<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" id="tabCart">
						<tr>			
							<td>
								<iframe src="/GmCommonCartServlet?companyInfo=<%=strCompanyInfo%>&hTagColumn=<%=strTagColumn %>&hCapFlColumn=<%=strCapFlColumn%>" scrolling="no" id="fmCart" marginheight="0" width="100%" height="400"></iframe><BR>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<jsp:include page="/operations/shipping/GmIncShipDetails.jsp" >
						<jsp:param name="SHIPCHRG" value="Y" />
					</jsp:include>
				</td>
			</tr>	
			<tr><td colspan="2" class="Line" height="1"></td></tr>
			<tr>
				<td>
					<jsp:include page="/common/GmIncludeLog.jsp" >
					<jsp:param name="LogType" value="" />
					<jsp:param name="LogValue" value="" />
					<jsp:param name="LogMode" value="Edit" />
					</jsp:include>
				</td>
			</tr>
			<tr>
			<!-- 
			Including Credit Hold validation functionality. it will check account is on credit hold or not when Account id is enterted.
			If Account is on credit hold then it will show warning message and if function is not allowed then disable place order button. 
			104962 - Place Order
			 -->
				<td colspan="2">
					<jsp:include page="/accounts/GmCreditHoldInclude.jsp" >
					<jsp:param name="ALERT" value="Y"/>
					<jsp:param name="SUBTYPE" value="104962"/>
					</jsp:include>
				</td>
			</tr>
			<tr>
				<td  align="center" height="35">
					<fmt:message key="BTN_RESET" var="varReset"/>
					<gmjsp:button gmClass="ButtonReset" name="Btn_ResetForm" style="height: 25px" accesskey="E"  onClick="fnReset();" buttonType="Load" value="${varReset}"/>&nbsp;
					<gmjsp:button gmClass="Button" name="Btn_PlaceOrd"  accesskey="O" tabindex="18" onClick="fnValidatePartPlant();" buttonType="Save" value="<%=strSubmit%>"/>
				</td>
			</tr>
			<tr><td colspan="6" height="1" class="Line"></td></tr>
			
			<tr>
			<td  colspan="6" > 
			 <div id="divf" style="display:none;">
			<iframe src="/gmRuleEngine.do?companyInfo=<%=strCompanyInfo%>&RE_FORWARD=gmPhoneOrder&txnStatus=PROCESS&RE_TXN=50925" scrolling="yes" id="iframe1" marginheight="0" width="100%" height="100" frameborder="0"   ></iframe><BR>
			</div>
			</td></tr>	
			<tr><td colspan="6" height="1" class="Line"></td></tr>
		</TABLE>
			 
		<BR>
		<script language="JavaScript" type="text/javascript" src="<%=strJsPath%>/wz_tooltip.js"></script> 
		</FORM>	
	<%@ include file="/common/GmFooter.inc"%>
		</body>
	</html>