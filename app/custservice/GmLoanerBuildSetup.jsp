<%
/**********************************************************************************
 * File		 		: GmLoanerBuildSetup.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtLoanerBuildSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtLoanerBuildSetup:setLocale value="<%=strLocale%>"/>
<fmtLoanerBuildSetup:setBundle basename="properties.labels.custservice.GmLoanerBuildSetup"/>
<!--\custservice\GmLoanerBuildSetup.jsp  -->
<%
try {
	
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");

	HashMap hmReturn = (HashMap)session.getAttribute("hmReturn");
	String strhAction = (String)session.getAttribute("hAction") == null?"":(String)session.getAttribute("hAction");
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strTemp = "";

	String strConsignId = (String)session.getAttribute("CONSIGNID") == null?"":(String)session.getAttribute("CONSIGNID");
	String strSetName = "";
	String strAccName = "";
	String strUserName = "";
	String strIniDate = "";
	String strItemQty = "";
	String strControl = "";
	String strPartDesc = "";
	String strFlag = ""; 
	
	String strType = "4111";
	String strDistribAccId = "";
	String strPurpose = "";
	String strBillTo = "";
	String strShipTo = "";
	String strShipToId = "";
	String strFinalComments = "";
	String strShipFl = "";
	String strDelFl = "";


	ArrayList alType = new ArrayList();
	ArrayList alShipTo = new ArrayList();
	ArrayList alPurpose = new ArrayList();
	ArrayList alDistributor = new ArrayList();
	ArrayList alRepList = new ArrayList();
	ArrayList alAccList = new ArrayList();
	ArrayList alSetLoad = new ArrayList();

	HashMap hmTemp = new HashMap();
	HashMap hmParam =  new HashMap();
	
	int intPriceSize = 0;

	if (hmReturn != null)
	{
		hmTemp = (HashMap)hmReturn.get("CONLISTS");

		alType = (ArrayList)hmTemp.get("CONTYPE");
		alShipTo = (ArrayList)hmTemp.get("SHIPTO");
		alPurpose = (ArrayList)hmTemp.get("PURPOSE");
		alDistributor = (ArrayList)hmTemp.get("DISTRIBUTORLIST");
		alRepList = (ArrayList)hmTemp.get("REPLIST");
		alAccList = (ArrayList)hmTemp.get("ACCLIST");
		alSetLoad = (ArrayList)hmReturn.get("SETLOAD");
	}
	hmParam = (HashMap)session.getAttribute("PARAM");

	if (hmParam != null)
	{
		strType = (String)hmParam.get("TYPE");
		strPurpose = (String)hmParam.get("PURPOSE");
		strBillTo = (String)hmParam.get("BILLTO");
		strShipTo = (String)hmParam.get("SHIPTO");
		strShipToId = (String)hmParam.get("SHIPTOID");
		strFinalComments = (String)hmParam.get("COMMENTS");
		strShipFl = (String)hmParam.get("SHIPFL");
		strChecked = strShipFl.equals("1")?"checked":"";
		strDelFl = (String)hmParam.get("SHIPFL");
	}

	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Item Consign </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>

function fnSubmit()
{
	document.frmVendor.hAction.value = 'Save';
  	document.frmVendor.submit();
}

function fnInitiate()
{
	document.frmVendor.hAction.value = 'Initiate';
	fnStartProgress("Y");
  	document.frmVendor.submit();
}

function fnPrintVer()
{
windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId=<%=strConsignId%>","Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnAgree()
{
windowOpener("/GmConsignSetServlet?hId=<%=strConsignId%>&hAction=Ack&","Ack","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");

}

function fnPicSlip()
{
	val = document.frmVendor.hConsignId.value;	windowOpener("<%=strServletPath%>/GmConsignItemServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=670,height=500");
}


function fnCallShip()
{
	obj = document.frmVendor.Cbo_ShipTo;
	var obj2 = document.frmVendor.Cbo_Values;
	if (obj.options[obj.selectedIndex].text == 'Sales Rep')
	{
		obj2.disabled = false;
		obj2.options.length = 0;
		obj2.options[0] = new Option("[Choose One]","0");
		for (var i=0;i<RepLen;i++)
		{
			arr = eval("RepArr"+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			obj2.options[i+1] = new Option(name,id);
		}
		obj2.focus();
	}
	else if (obj.options[obj.selectedIndex].text == 'Hospital')
	{
		obj2.disabled = false;
		obj2.options.length = 0;
		obj2.options[0] = new Option("[Choose One]","0");
		for (var i=0;i<AccLen;i++)
		{
			arr = eval("AccArr"+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			obj2.options[i+1] = new Option(name,id);
		}
		obj2.focus();
	}
	else if (obj.options[obj.selectedIndex].text == 'Employee')
	{
		obj2.disabled = false;
		obj2.options.length = 0;
		obj2.options[0] = new Option("[Choose One]","0");
		for (var i=0;i<EmpLen;i++)
		{
			arr = eval("EmpArr"+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			obj2.options[i+1] = new Option(name,id);
		}
		obj2.focus();
	}
	else
	{
		obj2.options.length = 0;
		obj2.options[0] = new Option("[Choose One]","0");
		obj2.disabled = true;
	}
}

var RepLen = <%=alRepList.size()%>;
<%
	intSize = alRepList.size();
	hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alRepList.get(i);
%>
	var RepArr<%=i%> ="<%=hcboVal.get("REPID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>

var AccLen = <%=alAccList.size()%>;
<%
	intSize = alAccList.size();
	hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alAccList.get(i);
%>
	var AccArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>

function fnLoad()
{
	var InHouseFl = '<%=strType%>';
	var ShipToFl = '<%=strShipTo%>';
	
	if (InHouseFl == '4112')
	{
		document.frmVendor.Cbo_Purpose.disabled = false;
		document.frmVendor.Cbo_Values.disabled = false;
		document.frmVendor.Cbo_BillTo.selectedIndex = 0;
		fnSetValue();
	}
	if (ShipToFl != '4120')
	{
		fnCallShip();
		fnSetValue();
	}
}

function fnSetValue()
{
	var ShipToId = '<%=strShipToId%>';
	var obj5 = document.frmVendor.Cbo_Values;
	for (var j=0;j<obj5.length;j++)
	{
		if (obj5.options[j].value == ShipToId)
		{
			obj5.options[j].selected = true;
			break;
		}
	}
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmConsignLoanerServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hConsignId" value="<%=strConsignId%>">
<input type="hidden" name="hDelPartNum" value="">

	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
			<td height="25" class="RightDashBoardHeader"><fmtLoanerBuildSetup:message key="LBL_PRO_ALL_SET"/></td>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
<%
		if (!strhAction.equals("Load"))
		{
%>
					<tr class="shade">
						<td class="RightText" HEIGHT="20" width="200" align="right">&nbsp;<fmtLoanerBuildSetup:message key="LBL_LOANER_ID"/></td>
						<td class="RightText" width="498">&nbsp;<%=strConsignId%></td>
					</tr>
<%
		}
%>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtLoanerBuildSetup:message key="LBL_TYPE"/></td>
						<td class="RightText">&nbsp;<select name="Cbo_Type" id="Region" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtLoanerBuildSetup:message key="LBL_CHOOSE"/>
<%
			  		intSize = alType.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<2;i++) // hardcoding to 2 to remove the In-House and following values
			  		{
			  			hcboVal = (HashMap)alType.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strType.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
							</select>
						</td>
					</tr>
					<tr class="shade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtLoanerBuildSetup:message key="LBL_BILL_TO"/></td>
						<td>&nbsp;<select name="Cbo_BillTo" id="Region" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtLoanerBuildSetup:message key="LBL_CHOOSE"/>
<%	                
                    intSize = alDistributor.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alDistributor.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strBillTo.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%
			  		}
%>
							</select>
						</td>
					</tr>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtLoanerBuildSetup:message key="LBL_SHIP_TO"/></td>
						<td class="RightText">&nbsp;<select name="Cbo_ShipTo" id="Region" class="RightText" tabindex="2" onChange="javascript:fnCallShip();" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtLoanerBuildSetup:message key="LBL_CHOOSE"/>
<%
			  		intSize = alShipTo.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<3;i++) // hardcoding to 3 to remove Employee as Ship To value -- cust service cannot use InHouse consignment from this screen
			  		{
			  			hcboVal = (HashMap)alShipTo.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strShipTo.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
							</select>&nbsp;&nbsp;&nbsp;&nbsp; <fmtLoanerBuildSetup:message key="LBL_VALUES"/><select name="Cbo_Values" id="Region" class="RightText"  disabled tabindex="3" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtLoanerBuildSetup:message key="LBL_CHOOSE"/>
						</select>
						</td>
					</tr>
					<tr class="shade">
						<td class="RightText" valign="top" align="right" HEIGHT="24"><fmtLoanerBuildSetup:message key="LBL_CMTS"/></td>
						<td>&nbsp;<textarea name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1 rows=5 cols=40 value="<%=strFinalComments%>"><%=strFinalComments%></textarea></td>
					</tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24"><fmtLoanerBuildSetup:message key="LBL_SHIP_REQ"/></td>
						<td><input type="checkbox" name="Chk_ShipFlag" onFocus="changeBgColor(this,'#AACCE8');" <%=strChecked%> onBlur="changeBgColor(this,'#ffffff');" tabindex=2></td>
					</tr>
					<tr>
						<td align="center" colspan="2" valign="top">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td height="25"><fmtLoanerBuildSetup:message key="LBL_PART_NUM"/></td>
									<td width="200"><fmtLoanerBuildSetup:message key="LBL_DESC"/></td>
									<td><fmtLoanerBuildSetup:message key="LBL_QTY"/></td>
									<td width="200">&nbsp;&nbsp;&nbsp;&nbsp;<fmtLoanerBuildSetup:message key="LBL_QTY"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtLoanerBuildSetup:message key="LBL_CONTRL_NO"/></td>
								</tr>
								<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
<%
						intSize = alSetLoad.size();
						if (intSize > 0)
						{
							hcboVal = new HashMap();
							for (int i=0;i<intSize;i++)
							{
								hcboVal = (HashMap)alSetLoad.get(i);
%>
								<tr>
									<td class="RightText" height="20">&nbsp;<%=(String)hcboVal.get("PNUM")%></td>
									<td class="RightText"><%=GmCommonClass.getStringWithTM((String)hcboVal.get("PDESC"))%></td>
									<td align="center" class="RightText"><%=(String)hcboVal.get("QTY")%></td>
									<td class="RightText">&nbsp;&nbsp;&nbsp;_ _ _ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_ _ _ _ _ _ _ _ _ _
									</td>
								</tr>
								<tr><td colspan="4" height="1" bgcolor="#eeeeee"></td></tr>
<%
							}
						}else {
%>
						<tr><td colspan="4" height="50" align="center" class="RightTextRed"><BR><fmtLoanerBuildSetup:message key="LBL_NO_PART"/></td></tr>
<%
						}		
%>
							</table>
						</td>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>					
					<tr>
						<td colspan="2" height="30" align="center">
							<fmtLoanerBuildSetup:message key="BTN_INITIATE" var="varInitiate"/><gmjsp:button value="${varInitiate}" gmClass="button" onClick="fnInitiate();" tabindex="13" buttonType="Save" />
						</td>
					</tr>

				</table>
			 </td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
    </table>
  <%--
<script>

<%
	if (strhAction.equals("OrdPlaced"))
	{
%>
	alert("Your consignment has been placed. Please print PIC Slip");
<%
	}
%>
</script>
--%>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
