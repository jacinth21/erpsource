
<%
/*******************************************************************************************************
 * File		 		: GmModifyOrderEditPO.jsp
 * Desc		 		: This screen is used for the Updating the Cust PO details from Modify Order Screen
 * Version	 		: 1.0
 * author			: Harinadh Reddi N
********************************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%>
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.Date"%>
<%@ taglib prefix="fmtEditPo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- PC-3880 New field in record PO Date in GM Italy-->
<%@page import="java.util.Date"%>
<!-- GmModifyOrderEditPO.jsp -->
<fmtEditPo:setLocale value="<%=strLocale%>"/>
<fmtEditPo:setBundle basename="properties.labels.custservice.ModifyOrder.GmModifyOrderEditPO"/>
<%
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strApplnDateFmt = strGCompDateFmt;
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	String strTOdayDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt);
	String strCurrSymbol = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	String strAccCurrSymb ="";
	Date dtFrmDate = null;
	Date dtTodayDate = null;
	String strWikiTitle = "";
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strMode = (String)request.getAttribute("hMode")==null?"":(String)request.getAttribute("hMode");
	String strShipFl = (String)request.getAttribute("hShipFl")==null?"":(String)request.getAttribute("hShipFl");
	String strChecked = "";
	int intSize= 0;
	strChecked = strShipFl.equals("2")?"checked":"";
	GmResourceBundleBean gmResourceBundleBeanlbl = 
	    GmCommonClass.getResourceBundleBean("properties.labels.custservice.ModifyOrder.GmModifyOrderEditPO", strSessCompanyLocale);
	String strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CONTROL_NUMBER"));
	strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PO_DETAILS"));
	strWikiTitle = GmCommonClass.getWikiTitle("ORDER_ITEM_EDIT_PO");
	String strCurrencyFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplCurrFmt"));
	String strOrderId = "";
	String strAccName = "";
	String strUserName = "";
	//Date dtOrdDate = null;
	String strComments = "";
	String strShade = "";
	String strItemQty = "";
	String strControl = "";
	String strAccid = "";
	String strCodeID = "";
	String strSelected = "";
	String strShipDate = "";
	String strShipCarr = "";
	String strShipMode = "";
	String strTrack = "";
	String strShipCost = "";
	String strPO = "";
	String strItemOrdType = "";
	String strItemOrdDesc = "";
	String strDisabled = "";
	String strOrderType = "";
	String strReadOnly = "";
	String strCapFlag = "";
	String strStatus = "";
	String strRAReason = "";
	String strInvoiceId = "";
	String strParentOrdId="";
	String strOrdDate = "";
	String strRequiredDt = "";
	String strOrdAtbID = "";
	String strOrdCodeID = "";
	String strOrdAtbVal = "";
	String strBeforeTotal = "";
	String strAfterTotal = "";
	String strContractTp = "";
	String strGpbNm = "";
	String strGpoNm = "";
	String strIpadOrder = "";
	String strPOAmt = "";
	String strCopay  = "";
	String strCapAmount = "";
	String strSurgeryDate = "";
	String strOrderAdjId = "";
	boolean blLoanerFl = false;
	long diff = 0;
	
	double dbBeforeItemTotal = 0.0;
	double dbBeforePriceTotal = 0.0;
	double dbAfterItemTotal = 0.0;
	double dbAfterPriceTotal = 0.0;
	
	String strBeforeItemTotal = "";
	String strAfterItemTotal = "";
	String strAfterPrice = "";
	String strOvrCurr = "";
	String strTxnCompID = "";
	String strDoFlag="";
	String strTodaysDate = strTOdayDate;
	String strDeptId = (String)session.getAttribute("strSessDeptId")==null?"":(String)session.getAttribute("strSessDeptId");
	

	String strEditPOAccess = GmCommonClass.parseNull((String)request.getAttribute("EDITPOACCESS")); 
    String strSubmitDisable = strEditPOAccess.equals("Y")?"false":"true";
    //PC-3880 New field in record PO Date in GM Italy
    java.sql.Date dtPODate = null;
    
	ArrayList alCart = new ArrayList();
	ArrayList alOrdAtbDetails = new ArrayList();	
	HashMap hmOrderDetails = new HashMap();
	HashMap hcboVal = new HashMap();

	int intPriceSize = 0;
	if (hmReturn != null){
		hmOrderDetails = (HashMap)hmReturn.get("ORDERDETAILS");
		alOrdAtbDetails	= GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ORDATBDETAILS"));
		strOrderId = (String)hmOrderDetails.get("ID");
		strInvoiceId = (String)hmOrderDetails.get("INVOICEID");		
		strAccName = (String)hmOrderDetails.get("ANAME");
		strAccid = (String)hmOrderDetails.get("ACCID");
		strUserName = (String)hmOrderDetails.get("UNAME"); 
		strTxnCompID = (String)hmOrderDetails.get("COMPANYCD");//Taking company id from t501 tbl.
		//dtOrdDate = (Date)hmOrderDetails.get("ODT");
		strDoFlag = (String)hmOrderDetails.get("DO_FLAG");
		strOrdDate = GmCommonClass.getStringFromDate((java.sql.Date)hmOrderDetails.get("ODT"),strApplnDateFmt);
		strComments = (String)hmOrderDetails.get("COMMENTS");
		strPO = (String)hmOrderDetails.get("PO");
		strOrderType = GmCommonClass.parseNull((String)hmOrderDetails.get("ORDERTYPE"));
		strReadOnly = strOrderType.equals("2530")?"disabled":"";
		strRAReason =	GmCommonClass.parseNull((String)hmOrderDetails.get("RAREASON"));
		strParentOrdId =	GmCommonClass.parseNull((String)hmOrderDetails.get("PARENTORDID"));
		strRequiredDt = GmCommonClass.parseNull((String)hmOrderDetails.get("REQDATE"));	
		strAccCurrSymb = GmCommonClass.parseNull((String)hmOrderDetails.get("ACC_CURR_SYMB"));
		strCurrSymbol = strAccCurrSymb.equals("")?strCurrSymbol:strAccCurrSymb;
		strContractTp = GmCommonClass.parseNull((String)hmOrderDetails.get("CONTRACTTP"));
		strGpoNm = GmCommonClass.parseNull((String)hmOrderDetails.get("GPONAME"));
		strGpbNm = GmCommonClass.parseNull((String)hmOrderDetails.get("GPBNAME"));
		strIpadOrder = GmCommonClass.parseNull((String)hmOrderDetails.get("IPADORDER"));
	    strCopay = GmCommonClass.parseNull((String)hmOrderDetails.get("COPAY"));
	    strCapAmount = GmCommonClass.parseNull((String)hmOrderDetails.get("CAPAMOUNT"));
		strIpadOrder = strIpadOrder.equals("")?"":"Yes";
		strPOAmt = (String)hmOrderDetails.get("POAMT");
		strOvrCurr = GmCommonClass.parseNull((String) hmOrderDetails.get("OVRIDE_CURR"));
		strCurrSymbol = strOvrCurr.equals("")?strCurrSymbol:strOvrCurr;
		strSurgeryDate = GmCommonClass.getStringFromDate((java.sql.Date)hmOrderDetails.get("ORD_SURG_DT"),strApplnDateFmt);
		strOrderAdjId = strParentOrdId.equals("")?strParentOrdId:strOrderId;
		alCart = (ArrayList)hmReturn.get("PARENTORDERSUMMARY");
		//PC-3880 New field in record PO Date in GM Italy
		dtPODate = (java.sql.Date)hmOrderDetails.get("PODATE");
		HashMap hmShipDetails = (HashMap)hmReturn.get("SHIPDETAILS"); 

		if (hmShipDetails != null){
			strShipDate = GmCommonClass.getStringFromDate((java.sql.Date)hmShipDetails.get("SDT"),strApplnDateFmt); 
			strShipCarr = GmCommonClass.parseNull((String)hmShipDetails.get("SCAR"));
			strShipMode = GmCommonClass.parseNull((String)hmShipDetails.get("SMODE"));
			strTrack = GmCommonClass.parseNull((String)hmShipDetails.get("STRK"));
			strShipCost = GmCommonClass.parseZero((String)hmShipDetails.get("SCOST"));					
			strShipDate = strShipDate.equals("")?strTodaysDate:strShipDate;
			strShipCarr = strShipCarr.equals("")?"5001":strShipCarr;
			strShipMode = strShipMode.equals("")?"5005":strShipMode;
		}

		intSize = alOrdAtbDetails.size();
		hcboVal = new HashMap();
	}

	HashMap hmShip = new HashMap();
	ArrayList alCarrier = new ArrayList();
	ArrayList alMode = new ArrayList();
	ArrayList alShipTo = new ArrayList();
	ArrayList alRepList = new ArrayList();

	hmShip = (HashMap)request.getAttribute("hmShipLoad");
	String strShipTo = "";
	String strShipToId = "";
	String strShipLable = "";
	String strOrdLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ORDER_ID"));
	if (hmShip != null)	{
		alCarrier = (ArrayList)hmShip.get("CARRIER");
		alMode = (ArrayList)hmShip.get("MODE");
		alShipTo = (ArrayList)hmShip.get("SHIPLIST");
		alRepList = (ArrayList)hmShip.get("REPLIST");
		strShipTo = (String)hmOrderDetails.get("SHIPTO");
		strShipToId = (String)hmOrderDetails.get("SHIPTOID");
	}	
	strTxnCompID = strTxnCompID.equals("") ? gmDataStoreVO.getCmpid() : strTxnCompID;
	String strCompanyLocale = GmCommonClass.getCompanyLocale(strTxnCompID); 
%>

<%@page import="java.util.Date"%><HTML>
<HEAD>
<TITLE> Globus Medical: Enter Control Numbers </TITLE>
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="javascript" src="<%=strcustserviceJsPath%>/GmOrderItemControl.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/prodmgmnt/GmRule.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>	
<script>
var servletPath = '<%=strServletPath%>';
var ShipToId = '<%=strShipToId%>';
var custpo = '<%=strPO%>';
var POMessage = '';
var DeptId = '<%=strDeptId%>';
var ParentOrdId = '<%=strParentOrdId%>';
var strOrdAtbId = '<%=strOrdAtbID%>';
var strOrdCodeId = '<%=strOrdCodeID%>';
var format = '<%=strApplnDateFmt%>';
var currDate = '<%=strTodaysDate%>';
var orderType = '<%=strOrderType%>';
var lblCopay = '<fmtEditPo:message key="LBL_COPAY"/>';
var lblCapAmount = '<fmtEditPo:message key="LBL_CAP"/>';
var companyLocale = '<%=strCompanyLocale%>';
</script>


</HEAD>

<BODY leftmargin="20" topmargin="10" onkeydown="fnEnter();fnSubmitForm();" onload="fnOnPageLoad();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmOrderItemServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hOrderId" value="<%=strOrderId%>">
<input type="hidden" name="hParentOrdId" value="<%=strParentOrdId%>">
<input type="hidden" name="hAccName" value="<%=strAccName%>">
<input type="hidden" name="hAccId" value="<%=strAccid%>">
<input type="hidden" name="hRaReason" value="<%=strRAReason%>">
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="RE_FORWARD" value="gmOrderItemServlet">
<input type="hidden" name="txnStatus" value="PROCESS">
<input type="hidden" name="RE_TXN" value="50925">
<input type="hidden" name="hInvoiceId" value="<%=strInvoiceId%>">
<input type="hidden" name="hInputString" value="">
<input type="hidden" name="hCopayCap" value="CopyAction">
<input type="hidden" name="POStatus" value="">
<input type="hidden" name="DOStatus" value="">
    
<input type="hidden" name="Txt_OrderComments" value="<%=strComments%>"/>
<input type="hidden" name="hEditPOAccess" value="<%=strEditPOAccess%>">
	<table border="0" class="DtTable950" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td height="25" class="RightDashBoardHeader" colspan="3"><fmtEditPo:message key="LBL_EDIT_POHEADER"/> - <%=strTitle%><fmtEditPo:message key="LBL_EDIT_POHEADER1"/> </td>
					<fmtEditPo:message key="LBL_HELP" var="vaHelp"/>
					<td height="25" class="RightDashBoardHeader" align="right"><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${vaHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
				</tr>
				</table>
			</td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="6"></td></tr>
		<tr>
			<td width="800" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
					    <!-- <fmtEditPo:message key="LBL_HELP" var="vaHelp"/> -->
						<td class="RightText" HEIGHT="20" align="right" colspan="1"><gmjsp:label type="RegularText"  SFLblControlName="<%=strOrdLabel%>" td="false"/></td></span>
						<td colspan="1">&nbsp;<b><%=strOrderId%></b>
						<%
						if(strDoFlag.equals("Y")){
						%>
						<img id="imgHold" style="cursor:hand" src="<%=strImagePath%>/d_upload.gif" title="${varDoOnHold}" onclick="fnOpenDOFile('<%=strOrderId%>');" >
						<%
					}
						%>
						</td>
						<td align="right"><fmtEditPo:message key="LBL_IPAD_ORDER"/>:</td><td colspan="1">
						
				<!--  Below If/Else condition is to check the order source -ipad or portal) -->
						<%
						  if (strIpadOrder.equals("Yes")) {
						%>
						<fmtEditPo:message key="IMG_ALT_OPEN_PDF" var="varOpnPdf"/>
						&nbsp;<b><%=strIpadOrder%></b>
						&nbsp;<img id="imgPDF" style= "cursor:hand;vertical-align:middle;" src="<%=strImagePath%>/pdf_icon.gif"
						onclick="fnExportFile('<%=strOrderId%>');" title='${varOpnPdf}'>&nbsp;
					<% } else { %>
					
					
					&nbsp;<b> No </b>
					
					<% } %>
						<td HEIGHT="20" colspan="1" align="right">
						<fmtEditPo:message key="LBL_CONTRACT"/>:</td>
						<td colspan="1">&nbsp;<b><%=strContractTp%></b></span>
						</td>
					</tr>
					<tr><td colspan="6" height="1" class="LLine"></td></tr>
					<tr class="Shade" >
					    <fmtEditPo:message key="LBL_ACCOUNTNAME" var="varAccountName"/>
						<td class="RightText" HEIGHT="20" align="right" colspan="1">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varAccountName}:" td="false"/>
						</td>
						<td HEIGHT="20" colspan="1">&nbsp;<b><%=strAccName%></b></td>
					    <fmtEditPo:message key="LBL_ACCOUNTID" var="varAccountId"/>
						<td HEIGHT="20" align="right" colspan="1"><gmjsp:label type="RegularText"  SFLblControlName="${varAccountId}:" td="false"/></td>
						<td colspan="3">&nbsp;<b><%=strAccid%></b></td>
					</tr>					
					<tr><td colspan="6" height="1" class="LLine"></td></tr>
					<tr>
					    <fmtEditPo:message key="LBL_GPB" var="varGPBName"/>
						<td class="RightText" align="right" HEIGHT="20" colspan="1">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varGPBName}:" td="false"/>
						</td>
						<td HEIGHT="20" colspan="1">&nbsp;<b><%=strGpbNm%></b></td>
						<fmtEditPo:message key="LBL_GPO" var="varGPOName"/>
						<td HEIGHT="20" align="right" colspan="1"><gmjsp:label type="RegularText"  SFLblControlName="${varGPOName}:" td="false"/></td>
						<td colspan="4">&nbsp;<b><%=strGpoNm%></b></td>
					</tr>
					<tr><td colspan="6" height="1" class="LLine"></td></tr>
					 <tr class="Shade" >
					    <fmtEditPo:message key="LBL_ORDERENTEREDBY" var="varOrderEnteredBy"/>
						<td colspan="1" class="RightText" HEIGHT="20" align="right" >&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varOrderEnteredBy}:" td="false"/>
						</td>
						<td colspan="1">&nbsp;<b><%=strUserName%></b></td>
						<td colspan="1" align="right">
						<fmtEditPo:message key="LBL_DATE"/>:</td><td colspan="1">&nbsp;<b><%=strOrdDate%></b></td>
						<td HEIGHT="20" colspan="1" align="right">
						<fmtEditPo:message key="LBL_SURGERY_DATE"/>:</td><td colspan="1">&nbsp;<b><%=strSurgeryDate%></b></span>
						</td>
					</tr>
					<tr><td colspan="6" height="1" class="LLine"></td></tr>
					
					<tr>  
					    <fmtEditPo:message key="LBL_COMMENTS" var="varComments"/>
						<td class="RightText" valign="top" align="right" HEIGHT="60" colspan="1">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varComments}:" td="false"/>
						</td>
						<td colspan="5">&nbsp;&nbsp;<%=strComments%></td>
						</tr>
					<tr><td colspan="6" height="1" class="LLine"></td></tr>
					<tr class="Shade">
					    <fmtEditPo:message key="LBL_CUSTPO" var="varCustPO"/>
						<td align="right" colspan="1" HEIGHT="24" class="RightText"><gmjsp:label type="RegularText"  SFLblControlName="${varCustPO}:" td="false"/>
						</td>
						<fmtEditPo:message key="LBL_POIDAVAIL" var="varPoIdAvail"/>
						<fmtEditPo:message key="LBL_POIDEXIST" var="varCustPO"/>
						<fmtEditPo:message key="LBL_ORD_DTLS" var="varOrdDtls"/>
						<td class="RightText" colspan="1">&nbsp;<input type="text" size="15" tabindex="1" value="<%=strPO%>" name="Txt_PO" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');fnClearDiv();" onBlur="changeBgColor(this,'#ffffff');fnCheckPO('CHECKPO');">
						<span id="DivShowPOIDAvail" style="vertical-align:middle; display: none;"> <img height="24" width="25" title="${varPoIdAvail}" src="<%=strImagePath%>/success.gif"></img></span>
						<span id="DivShowPOIDExists" style="vertical-align:middle; display: none;"><img title="${varCustPO}" src="<%=strImagePath%>/delete.gif"></img>&nbsp;<a href="javascript:fnModifyOrder()"><img height="15" title="${varOrdDtls}" src="<%=strImagePath%>/location.png"></img></a></span></td>
						<!-- <span id="DivShowLocation" style="vertical-align:middle; display: none;"> --><!-- </span> -->						
						<td align="right" colspan="1"><fmtEditPo:message key="LBL_PO_AMOUNT" />:</td>
						<td colspan="1">&nbsp;<input type="text" size="15" tabindex="2" value="<%=strPOAmt%>" name="Txt_POAmt" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');">
						</td>
						<!-- PC-3880 New field in record PO Date in GM Italy -->
						<td align="right" colspan="1"><fmtEditPo:message key="LBL_PO_DATE" />:</td>
						<td colspan="1">
						&nbsp;&nbsp;<gmjsp:calendar textControlName="Txt_PODate" textValue = "<%=dtPODate%>"
						gmClass="InputArea"
						onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" tabIndex="" />
						</td>
						<tr><td colspan="6" height="1" class="LLine"></td></tr>
						<tr>
                        <td align="right" HEIGHT="24" class="RightText" colspan="1">&nbsp;
                        <fmtEditPo:message key="LBL_COPAY" var="varCopay"/>
                        <gmjsp:label type="RegularText" SFLblControlName="${varCopay}:" td="false"/></td>
                        <td class="RightText" colspan="1">&nbsp;<input type="text" size="15" value="<%=strCopay%>" tabindex="3" name="Txt_Copay" class="InputArea">
                        <td align="right" colspan="1">
                        <fmtEditPo:message key="LBL_CAP" var="VarCapAmount"/>
                        <gmjsp:label type="RegularText"  SFLblControlName="${VarCapAmount}:" td="false"/></td>
                        <td colspan="3">&nbsp;<input type="text" size="15" value="<%=strCapAmount%>" name="Txt_Cap_Amount" class="InputArea" tabindex="4"  >
                        </td>
    
						
						<%	if (strParentOrdId.equals("") && strDeptId.equals("J") && !strhAction.equals("ACKORD")){ // only a/r should have access				 
			%>
					</tr>
					<tr>
					<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
						<td colspan="6">
							<jsp:include page="/custservice/GmIncludeRevenueTrack.jsp">
							<jsp:param name="WIDTH" value="950px" />
							</jsp:include>
						</td>
					</tr>
			<% } %>
					<tr>
						<td colspan="6" width="950">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr><td colspan="13" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
								<td width="135" height="25">&nbsp;<fmtEditPo:message key="LBL_PACKSLIP" /></td>
									<td width="70" height="25">&nbsp;<fmtEditPo:message key="LBL_PART" /></td>
									<td width=550"><fmtEditPo:message key="LBL_PARLBLESC"/></td>
									<td width="60" align="center"><fmtEditPo:message key="LBL_QTY" /></td>
									<td width="60" align="center"><fmtEditPo:message key="LBL_ITEM" /><br><fmtEditPo:message key="LBL_TYPE" /></td>
									<td width="70" >&nbsp;<fmtEditPo:message key="LBL_CONST"/><br>&nbsp;<fmtEditPo:message key="LBL_FLAG"/></td>
									<td width="110"><fmtEditPo:message key="LBL_CONTROL" /> <br><fmtEditPo:message key="LBL_NUMBER" /></td>
									<td width="70" >&nbsp;<fmtEditPo:message key="LBL_STATUS" /></td>
									<%-- <td width="130" align="center"><fmtEditPo:message key="LBL_UNITPRICE" /></td> --%>
									<td width="100" align="center"><fmtEditPo:message key="LBL_UNIT" /><br><fmtEditPo:message key="LBL_ADJ" /></td>
									<td width="75" align="center"><fmtEditPo:message key="LBL_ADJ" /> <fmtEditPo:message key="LBL_CODE" /></td>
									<td width="130" align="center"><fmtEditPo:message key="LBL_DO" /><br><fmtEditPo:message key="LBL_UNITPRICE" /></td>
									<td width="150" align="center"><fmtEditPo:message key="LBL_TOTALPRICE" /></td>
						</tr>
								<TR><TD colspan=13 height=1 class=Line></TD></TR>
<%
						intSize = alCart.size();
						StringBuffer sbPartNum = new StringBuffer();
						if (intSize > 0){
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();

							String strNextPartNum = "";
							int intCount = 0;
							String strColor = "";
							String strLine = "";
							String strPartNumHidden = "";
							String strPartDesc = "";
							String strPrice = "";
							String strTemp = "";
							String strPartNum = "";
							String strItemOrderId = "";
							String strDispQty = "";
							String strRefId = "";
							String strAdjustAmount = "";
							double intQty = 0.0;
							double dbItemTotal = 0.0;
							double dbTotal = 0.0;
							String strItemTotal = "";
							String strTotal = "";
							double dblShip = 0.0;
							String strUnitPrice = "";
							String strUnitPriceAdjFl = "";
							String strAdjCode = "";
							String strNetUnitPrice = "";
							
							double dbAdjTotal = 0.0;
							double dbAdjstTotal = 0.0;
							double dbBeforeTotal = 0.0;
							double dbAfterTotal = 0.0;
							String strAdjTotal = "";
							
							double dbBeforePrice = 0.0;
							double dbAfterPrice = 0.0;
							double dbAdjustPrice = 0.0;

							for (int i=0;i<intSize;i++)	{
								hmLoop = (HashMap)alCart.get(i);
								if (i<intSize-1){
									hmTempLoop = (HashMap)alCart.get(i+1);
									strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("ID"));
								}
								strItemOrderId =  GmCommonClass.parseNull((String)hmLoop.get("ORDID"));
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("ID"));
								sbPartNum.append(strPartNum);
								sbPartNum.append(",");
								strPartNumHidden = strPartNum;
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
								strDispQty = GmCommonClass.parseZero((String)hmLoop.get("PARTQTY"));
								strTemp = strPartNum;
								strRefId = GmCommonClass.parseNull((String)hmLoop.get("REFID"));
								if (strPartNum.equals(strNextPartNum)){
									intCount++;
									strLine = "<TR><TD colspan=13 height=1 bgcolor=#eeeeee></TD></TR>";
								}else{
									strLine = "<TR><TD colspan=13 height=1 bgcolor=#eeeeee></TD></TR>";
									if (intCount > 0){
										strPartNum = "";
										strPartDesc = "";
										//strColor = "bgcolor=white";
										strLine = "";
										strDispQty = "";
									}else{	
										//strColor = "";
									}
									intCount = 0;
								}

								if (intCount > 1){
									strPartNum = "";
									strPartDesc = "";
									//strColor = "bgcolor=white";
									strLine = "";
									strDispQty = "";
								}
							
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
								strItemQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								strItemOrdType = GmCommonClass.parseNull((String)hmLoop.get("ITEMTYPE"));
								strItemOrdDesc = GmCommonClass.parseNull((String)hmLoop.get("ITEMTYPEDESC"));
								strCapFlag = GmCommonClass.parseNull((String)hmLoop.get("CONSFL"));
								strStatus =  GmCommonClass.parseNull((String)hmLoop.get("SFL"));
								strUnitPrice = GmCommonClass.parseZero((String)(String)hmLoop.get("UPRICE"));
								strUnitPriceAdjFl = GmCommonClass.parseNull((String) (String)hmLoop.get("UADJFL"));
								strAdjCode = GmCommonClass.parseNull((String)(String)hmLoop.get("ADJCODENAME"));
								strNetUnitPrice = GmCommonClass.parseZero((String)(String)hmLoop.get("NETUNITPRICE"));
																
								if(strNetUnitPrice.equals("0")){
									strNetUnitPrice = strUnitPrice;
								}
								intQty = Double.parseDouble(strItemQty);
								dbItemTotal = Double.parseDouble(strNetUnitPrice);
								dbItemTotal = intQty * dbItemTotal; // Multiply by Qty
								strItemTotal = ""+dbItemTotal;
								dbTotal = dbTotal + dbItemTotal;	
								
								// Calculating the Total Price Before Adjustment
								dbBeforeItemTotal = Double.parseDouble(strUnitPrice);
								dbBeforeItemTotal = intQty * dbBeforeItemTotal; // Multiply by Qty
								strBeforeItemTotal = ""+dbBeforeItemTotal;
								dbBeforePriceTotal = dbBeforePriceTotal + dbBeforeItemTotal;
								strBeforeTotal = "" + 	dbBeforePriceTotal;	
								
								// Calculating the Total Price After Adjustment
								dbAfterItemTotal = Double.parseDouble(strPrice);
								dbAfterItemTotal = intQty * dbAfterItemTotal; // Multiply by Qty
								strAfterItemTotal = ""+dbAfterItemTotal;
								dbAfterPriceTotal = dbAfterPriceTotal + dbAfterItemTotal;
								strAfterPrice = "" + 	dbAfterPriceTotal;		
								// Caliculating the Adjustment Amount
								dbBeforePrice = Double.parseDouble(strBeforeTotal);
								dbAfterPrice = Double.parseDouble(strAfterPrice);						
								dbAdjustPrice = dbAfterPrice - dbBeforePrice;						
								strAdjustAmount = ""+dbAdjustPrice;
								//strTotal = ""+dbTotal;
								out.print(strLine);
								
								if (strItemOrdType.equals("50301"))	{
									strDisabled = "readonly";
									strControl = "NOC#";
									blLoanerFl = true;
								}else{
									blLoanerFl = false;
									strDisabled = "";
								}
								if (strRAReason.equals("3316"))	{
									strDisabled = "readonly";
								}

%>
								<tr>
									<input type="hidden" name="hPartNum<%=i%>" value="<%=strPartNumHidden%>">
									<input type="hidden" name="hRefId<%=i%>" value="<%=strRefId%>">
									<input type="hidden" name="hQty<%=strPartNum.replaceAll("\\.","")%>" value="<%=strDispQty%>">
									<input type="hidden" name="hPrice<%=i%>" value="<%=strPrice%>">
									<input type="hidden" name="hType<%=i%>" value="<%=strItemOrdType%>">
									<input type="hidden" name="hCapFl<%=i%>" value="<%=strCapFlag%>">
									<td class="RightText" height="20">&nbsp;<%=strItemOrderId%></td>
									<td class="RightText" height="20">&nbsp;<%=strPartNum%></td>
									<td class="RightText"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td class="RightText" align="center"><%=strItemQty%></td>
									<td class="RightText" align="center">&nbsp;<%=strItemOrdDesc%></td>
									<td class="RightText" align="center">&nbsp;<%=strCapFlag%></td>
									<td><%=strControl%></td>		
									<td class="RightText" align="center">&nbsp;<%=strStatus%></td>					
									<td width="75" class="RightText" align="center"><%=strUnitPriceAdjFl%></td>
									<td width="75" class="RightText" align="center"><%=strAdjCode%></td>		
									<gmjsp:currency width="130" type="CurrTextSign"  textValue="<%=strNetUnitPrice%>" currSymbol="<%=strCurrSymbol%>"/></td>
									<td width="150" class="RightText" align="right"><%=strCurrSymbol %>&nbsp;<%=GmCommonClass.getStringWithCommas(strItemTotal)%>&nbsp;</td>
								</tr>
<%
							}//End of FOR Loop
							out.print("<input type=hidden name=pnums value="+sbPartNum.toString().substring(0,sbPartNum.length()-1)+">");
							if (!strhAction.equals("EditControl")){								
								strShipCost = GmCommonClass.parseZero(strShipCost);
								dblShip = Double.parseDouble(strShipCost);
								dbBeforeTotal = Double.parseDouble(strBeforeTotal);
								dbTotal = dbBeforeTotal ;
								strBeforeTotal = ""+dbTotal;
								
								dbAfterTotal = Double.parseDouble(strAfterPrice);
								dblShip = Double.parseDouble(strShipCost);
								dbAdjstTotal = dbAfterTotal + dblShip;
								strAfterTotal = ""+dbAdjstTotal;
%>
								<tr><td colspan="13" height="1" bgcolor="#eeeeee"></td></tr>
								<tr>
									<td>&nbsp;</td>
									<td  height="20" colspan="9" class="RightText">&nbsp;<fmtEditPo:message key="LBL_SHIPPINGCHARGES" /></td>
									<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strShipCost)%>&nbsp;</td>
								</tr>
								<tr><td colspan="13" height="1" bgcolor="#eeeeee"></td></tr>								
								<tr>
									<td colspan="11" height="30" align="right" class="RightTableCaption"><fmtEditPo:message key="LBL_TOTALBEFOREADJ"/>:&nbsp;&nbsp;</td>
									<gmjsp:currency currSymbol="<%=strCurrSymbol%>" type="CurrTextSign" textValue="<%=strBeforeTotal%>"/>&nbsp;<input type="hidden" name="hOrderAmt" value="<%=strBeforeTotal%>"></td>
								</tr>
								
										<tr >
									<td  height="20" colspan="11" align="right" class="RightTableCaption">
											<fmtEditPo:message key="IMG_ALT_OPEN_ADJ" var="varOpnAdj"/>
											<a class="RightText" tabindex=-1 href="javascript:fnOpenAdjustment('<%=strOrderAdjId%>');">  
											<img id=imgDetail src="<%=strImagePath%>/magnifyGlassSmall.png" title='${varOpnAdj}'></a>								
									<fmtEditPo:message key="LBL_ADJUSTMENTS"/>:&nbsp;&nbsp;
									</td>
	     							<gmjsp:currency  textValue="<%=strAdjustAmount%>"   type="CurrTextSign" currSymbol="<%=strCurrSymbol %>"/></td>
								</tr>
								
								<tr>
									<td colspan="11" height="30" align="right" class="RightTableCaption"><fmtEditPo:message key="LBL_TOTALAFTERADJ"/>:&nbsp;&nbsp;</td>
									<gmjsp:currency currSymbol="<%=strCurrSymbol%>"type="CurrTextSign" textValue="<%=strAfterTotal%>" />&nbsp;<input type="hidden" name="hOrderAdjAmt" value="<%=strAfterTotal%>"></td>
								</tr>
<%		}
	}else { %>
						<tr><td colspan="7" height="50" align="center" class="RightTextRed"><BR><fmtEditPo:message key="LBL_PARTNUMBERINORDER"/></td></tr>
<% } %>
							</table>
							<input type="hidden" name="hCnt" value="<%=intSize-1%>">
						</td>
					</tr>	
				</table>
			</td>
		</tr>
		<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
    </table>
<BR>
	<table border="0" class="DtTable950" cellspacing="0" cellpadding="0" >
		<tr>
			<td  width=250></td>
			<td align="center" height="30" id="button">

<%	if (strMode.equals("SaveShip")){ %>
				<fmtEditPo:message key="BTN_PACKINGSLIP" var="varBtnPackingSlip"/>
				<gmjsp:button value="&nbsp;${varBtnPackingSlip}&nbsp;" name="Btn_Pack" gmClass="button" onClick="fnPrintPack();" buttonType="Save" />
<% } %>
			</td>
			
		<tr>
		
	</table>
	<script>
<% if (strMode.equals("SaveShip")){ %>
	alert("Your order is ready for Shipping. Please print Packing Slip");
<% } %>
</script>
<% if (!strhAction.equals("EditShip")){%>
<table border="0" class="DtTable950" cellspacing="0" cellpadding="0" >
	<!-- <tr><td bgcolor=#666666 colspan=3></td></tr> -->
	<tr> 
		<!-- <td bgcolor=#666666 width=1></td> -->
		<td>
			<jsp:include page="/common/GmIncludeLog.jsp" >
				<jsp:param name="LogType" value="" />
				<jsp:param name="LogMode" value="Edit" />
			</jsp:include>
		</td>
		<!-- <td bgcolor=#666666 width=1></td> -->
	</tr>
	<tr><td colspan="5" height="2" bgcolor="#eeeeee"></td></tr>
	<tr>
				<td align="center" colspan="5">
				<jsp:include page="/accounts/GmCreditHoldInclude.jsp" >
					<jsp:param name="ALERT" value="N"/>
					<jsp:param name="SUBTYPE" value="104965"/>
					</jsp:include>
				</td>
	</tr>
	<tr><td colspan="5" align="center">	
	<fmtEditPo:message key="BTN_SUBMIT" var="varSUBMIT"/>
	<%-- <input type="button" value="${varSUBMIT}" name="Btn_Submit" class="button" tabindex="5"  onClick="javascript:fnSubmit();">&nbsp;&nbsp; --%>
	<gmjsp:button value="${varSUBMIT}" disabled="<%=strSubmitDisable%>" gmClass="button" buttonType="Save" onClick="javascript:fnSubmit();"/>&nbsp;&nbsp;
	
	<fmtEditPo:message key="BTN_UPLOAD_FILE" var="varUPLOAD"/>
	<input type="button" value="${varUPLOAD}" name="Btn_Upload" class="button" tabindex="5"  onClick="javascript:fnUploadFile('<%=strOrderId%>');">&nbsp;&nbsp;
	
	
	
	
	</td>
	
	
	</tr>
	<tr><td colspan="5" height="2" bgcolor="#eeeeee"></td></tr>
</table>
<% } %>
</BODY>
<%@ include file="/common/GmFooter.inc" %>
</HTML>
