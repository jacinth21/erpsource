<!-- \CS Label changes\custservice\GmAccountAffiliation.jsp -->
<%
/**********************************************************************************
* File                    : GmAccountAffiliation.jsp
* Desc                    : Account Affiliation details
* author                  : Karthik
************************************************************************************/
%>
<!-- \sales\AreaSet\GmAddInfo.jsp -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtAccountAffiliation" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<fmtAccountAffiliation:setLocale value="<%=strLocale%>"/>
<fmtAccountAffiliation:setBundle basename="properties.labels.custservice.GmAccountAffiliation"/>
<bean:define id="strContractNm" name="frmPricingParamsSetup" property="strContractNm" type="java.lang.String"> </bean:define>
<bean:define id="idn" name="frmPricingParamsSetup" property="idn" type="java.lang.String"> </bean:define>
<bean:define id="gpo" name="frmPricingParamsSetup" property="gpo" type="java.lang.String"> </bean:define>
<bean:define id="grouppricebook" name="frmPricingParamsSetup" property="grouppricebook" type="java.lang.String"> </bean:define>
<bean:define id="idnName" name="frmPricingParamsSetup" property="idnName" type="java.lang.String"> </bean:define>
<bean:define id="gpoName" name="frmPricingParamsSetup" property="gpoName" type="java.lang.String"> </bean:define>
<bean:define id="groupPriceBookName" name="frmPricingParamsSetup" property="groupPriceBookName" type="java.lang.String"> </bean:define>
<bean:define id="rpcName" name="frmPricingParamsSetup" property="rpcName" type="java.lang.String"> </bean:define>
<bean:define id="rpc" name="frmPricingParamsSetup" property="rpc" type="java.lang.String"> </bean:define>
<bean:define id="hlthName" name="frmPricingParamsSetup" property="hlthName" type="java.lang.String"> </bean:define>
<bean:define id="hlthId" name="frmPricingParamsSetup" property="hlthId" type="java.lang.String"> </bean:define>
<%
String strCustServiceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Party Setup </TITLE>
<meta http-equiv="Content-Type"content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strCustServiceJsPath%>/GmPricingParamsSetup.js"></script>
</HEAD>

<BODY leftmargin="10" topmargin="20"  >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
       	<td colspan="4" class="ShadeRightTableCaption" height="22"><fmtAccountAffiliation:message key="LBL_ACCOUNT_AFFILIATION"/></td>
       </tr>
       <tr><td colspan="4" class="LLine" height="1" ></td></tr>       
       <tr class="evenshade" >
			<td class="RightTableCaption" align="right" HEIGHT="24" width="17%"> &nbsp;<fmtAccountAffiliation:message key="LBL_GROUP_PRICE_BOOK"/>:</td>
                 <td class="RightText"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="grouppricebook" />
					<jsp:param name="METHOD_LOAD" value="loadPartyNameList&party_type=7003" />
					<jsp:param name="WIDTH" value="250" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value=""/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%=groupPriceBookName%>" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="<%=grouppricebook%>" />
					<jsp:param name="SHOW_DATA" value="10" />
					
							</jsp:include></td>
			<td class="RightTableCaption" align="Right" HEIGHT="24" width="7%"> &nbsp;<fmtAccountAffiliation:message key="LBL_IDN"/>:</td>
            <td class="RightText"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="idn" />
					<jsp:param name="METHOD_LOAD" value="loadPartyNameList&party_type=7010" />
					<jsp:param name="WIDTH" value="220" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value=""/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%=idnName%>" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="<%=idn%>" />
					<jsp:param name="SHOW_DATA" value="10" />
					<jsp:param name="AUTO_RELOAD" value="" />
							</jsp:include></td>
		</tr>				    
		<tr><td class="LLine" height="1" colspan="4"></td></tr>
		<tr class="oddshade" >
			<td class="RightTableCaption" align="right" HEIGHT="24" width="17%"> &nbsp;<%=strContractNm%>:</td>
	        <td>&nbsp;<gmjsp:dropdown width="250" controlName="strContract" SFFormName="frmPricingParamsSetup" SFSeletedValue="strContract" 
								SFValue="alContract" codeId="CODEID" codeName="CODENM"  defaultValue= "[Choose One]"/></td>
	        <td class="RightTableCaption" align="right" HEIGHT="24" width="7%"> &nbsp;<fmtAccountAffiliation:message key="LBL_GPO"/>:</td>
                 <td class="RightText"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="gpo" />
					<jsp:param name="METHOD_LOAD" value="loadPartyNameList&party_type=7011" />
					<jsp:param name="WIDTH" value="220" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value=""/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%=gpoName%>" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="<%=gpo%>" />
					<jsp:param name="SHOW_DATA" value="10" />
					<jsp:param name="AUTO_RELOAD" value="" />
							</jsp:include></td>
		 </tr>            
		 <tr><td colspan="4" class="LLine" height="1" ></td></tr>
	   	<!--  PC-2039 Add new fields on the Pricing Parameter tab in Account Setup screen -->
	   	  <tr class="evenshade" >
			<td class="RightTableCaption" align="right" HEIGHT="24" width="17%"> &nbsp;<fmtAccountAffiliation:message key="LBL_HEALTH_SYSTEM"/>:</td>
                 <td class="RightText"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="hlthId" />
					<jsp:param name="METHOD_LOAD" value="loadPartyNameList&party_type=26241118" />
					<jsp:param name="WIDTH" value="250" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value=""/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%=hlthName%>" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="<%=hlthId%>" />
					<jsp:param name="SHOW_DATA" value="10" />
					
							</jsp:include></td>
			<td class="RightTableCaption" align="Right" HEIGHT="24" width="7%"> &nbsp;<fmtAccountAffiliation:message key="LBL_PRC"/>:</td>
            <td class="RightText"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="rpc" />
					<jsp:param name="METHOD_LOAD" value="loadPartyNameList&party_type=26241117" />
					<jsp:param name="WIDTH" value="220" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value=""/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%=rpcName%>" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="<%=rpc%>" />
					<jsp:param name="SHOW_DATA" value="10" />
					<jsp:param name="AUTO_RELOAD" value="" />
							</jsp:include></td>
		</tr>
	   	 <tr><td colspan="4" class="LLine" height="1" ></td></tr>
		 <tr class="evenshade" >
		 <td class="RightTableCaption" align="right" HEIGHT="24" width="17%"> &nbsp;<fmtAccountAffiliation:message key="LBL_GLN"/></td>
		 <td>&nbsp;<html:text styleId="strId" property="strId" styleClass="InputArea" name="frmPricingParamsSetup" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/>&nbsp;</td>							
		 <td class="RightTableCaption" align="right" HEIGHT="24" width="17%"> &nbsp;<fmtAccountAffiliation:message key="LBL_GPOID"/></td>	
		 <td>&nbsp;<html:text styleId="strDetails" property="strDetails" styleClass="InputArea" name="frmPricingParamsSetup" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/>&nbsp;</td>			
	   	 </tr>
	   	 
</table>                          
              <%@ include file="/common/GmFooter.inc"%>
       </BODY>
</HTML>
