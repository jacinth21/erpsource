 <%
/**********************************************************************************
 * File		 		: GmConsignPackslipPrintGlobal.jsp
 * Desc		 		: This screen is used for the packing slip version of Consignment
 * Version	 		: 1.0
 * author			: Xun
************************************************************************************/
%>
 <!-- \sales\AreaSet\GmAddInfo.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	strCssPath = GmCommonClass.getString("GMSTYLES");
	strImagePath = GmCommonClass.getString("GMIMAGES");
	strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	strCommonPath = GmCommonClass.getString("GMCOMMON");

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmConDetails = new HashMap();

	ArrayList alSetLoad = new ArrayList();

	String strShade = "";

	String strConsignId = "";
	String strConsignAdd = "";
	String strShipAdd = "";
	String strDate = "";
	
	String strTemp = "";
	String strItemQty = "";
	String strControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";

	String strUserName = "";
	String strCompanyId = "";
	String strCompanyName = "";
	String strCompanyAddress = "";
	String strCompanyPhone = "";
	String strHeadAddress = "";
	String strPackSlipHeader = GmCommonClass.parseNull((String)request.getAttribute("PackSlipHeader"));
	strCountryCode = GmCommonClass.getString("COUNTRYCODE");
	if (hmReturn != null)
	{
		alSetLoad = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SETLOAD"));
		hmConDetails = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("SETDETAILS"));
		strConsignId = GmCommonClass.parseNull((String)hmConDetails.get("CID"));
		strConsignAdd = GmCommonClass.parseNull((String)hmConDetails.get("BILLADD"));
		strShipAdd = GmCommonClass.parseNull((String)hmConDetails.get("SHIPADD"));	
		strDate = GmCommonClass.parseNull((String)hmConDetails.get("UDATE"));
		strUserName = GmCommonClass.parseNull((String)hmConDetails.get("NAME"));
		strCompanyId = GmCommonClass.parseNull((String)hmConDetails.get("COMPANYID"));
		if(strCountryCode.equals("en")){
			strCompanyName = GmCommonClass.parseNull((String)GmCommonClass.getString(strCompanyId+"_COMPNAME"));
			strCompanyAddress = GmCommonClass.parseNull((String)GmCommonClass.getString(strCompanyId+"_COMPADDRESS"));
			strCompanyId = "/"+strCompanyId +".gif";
			strCompanyAddress = strCompanyAddress.replaceAll("/","<br>");
		}else{
			strCompanyAddress = GmCommonClass.getString("GMCOMPANYADDRESS");
			strCompanyName = GmCommonClass.getString("GMCOMPANYNAME");
			strCompanyPhone =  GmCommonClass.getString("GMCOMPANYPHONE");
			strCompanyAddress = strCompanyAddress + "/"+ "Phone:"+strCompanyPhone;
			strCompanyAddress = strCompanyAddress.replaceAll("/", "\n<br>");
			strCompanyId = "/"+strCompanyId +".gif";
		}
	}

	int intSize = 0;
	HashMap hcboVal = null;

	StringBuffer sbStartSection = new StringBuffer();
	sbStartSection.append("<table border=0 width=700 cellspacing=0 cellpadding=0 align=center>");
	sbStartSection.append("<tr><td bgcolor=#666666 colspan=3></td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 width=1></td>");
	sbStartSection.append("<td>");
	sbStartSection.append("<table cellpadding=0 cellspacing=0 border=0 width='100%'>");
	sbStartSection.append("<tr>");
	sbStartSection.append("<td width=170 height=70><img src=");sbStartSection.append(strImagePath +strCompanyId);
	sbStartSection.append(" width=138 height=60></td>");
	
	if (strPackSlipHeader.equals("SHIPPINGLIST")){  
		sbStartSection.append("<td class=RightText width=270>Globus Medical Gmbh<br>Gotthardstrasse 3<br>6304 Zug<br>Switzerland <br></td>");
		sbStartSection.append("<td align=right class=RightText width=400><font size=+3>Shipping List</font>&nbsp;</td>");
	}
	else{
		sbStartSection.append("<td class=RightText width=270>"+strCompanyName+"<br>"+strCompanyAddress+"<br></td>");
		sbStartSection.append("<td align=right class=RightText width=400><font size=+3>Packing Slip</font>&nbsp;</td>");
	}
	
	sbStartSection.append("</tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 colspan=3></td></tr>");
	sbStartSection.append("<tr><td colspan=3>");
	sbStartSection.append("<table border=0 width=100% cellspacing=0 cellpadding=0>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption>");
	
	if (strPackSlipHeader.equals("SHIPPINGLIST")){ 
		sbStartSection.append("<td height=24 align=center width=250>&nbsp;Consignee/Ship To</td>");
		sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=11></td>");
		sbStartSection.append("<td width=250 align=center>&nbsp;Shipper/Exporter</td>");
	}else if(strPackSlipHeader.equals("9110")){  /* 9110 = IHLN Type*/
		sbStartSection.append("<td height=24 align=center width=250>&nbsp;Loaned To</td>");
		sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=11></td>");
		sbStartSection.append("<td width=250 align=center>&nbsp;Ship To</td>");
	}else{
		sbStartSection.append("<td height=24 align=center width=250>&nbsp;Consigned To</td>");
		sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=11></td>");
		sbStartSection.append("<td width=250 align=center>&nbsp;Ship To</td>");
	}	
	sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=11></td>");
	sbStartSection.append("<td width=100 align=center>&nbsp;Date</td>");
	if (strPackSlipHeader.equals("SHIPPINGLIST")){  
	sbStartSection.append("<td width=100 align=center>&nbsp;Proforma</td>");
	}else if(strPackSlipHeader.equals("9110")){ /* 9110 = IHLN Type*/
		sbStartSection.append("<td width=100 align=center>&nbsp;Transaction ID</td>");
	}else{
		sbStartSection.append("<td width=100 align=center>&nbsp;Consign #</td>");
	}
	sbStartSection.append("</tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
	sbStartSection.append("<tr><td class=RightText rowspan=5 valign=top>&nbsp;");sbStartSection.append(strConsignAdd);
	sbStartSection.append("</td>");
	sbStartSection.append("<td rowspan=5 class=RightText valign=top>&nbsp;");
	if (strPackSlipHeader.equals("SHIPPINGLIST")){  
		sbStartSection.append(strCompanyName+"<br>"+strCompanyAddress);
	}
	else{
		sbStartSection.append(strShipAdd);
	}
	sbStartSection.append("</td>");
	sbStartSection.append("<td height=25 class=RightText align=center>&nbsp;");sbStartSection.append(strDate);
	sbStartSection.append("</td><td class=RightText align=center>&nbsp;");
	sbStartSection.append(strConsignId);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	if(strPackSlipHeader.equals("9110")){ /* 9110 = IHLN Type*/
		sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption><td align=center>&nbsp;</td><td align=center>Requested By</td></tr>");
	}else{
		sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption><td align=center>&nbsp;</td><td align=center>Consigned By</td></tr>");
	}
	sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	sbStartSection.append("<tr><td align=center class=RightText>&nbsp;");
	sbStartSection.append("<Br></td><td align=center height=25 nowrap class=RightText>&nbsp;");sbStartSection.append(strUserName);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr></table></td></tr>");
	sbStartSection.append("<tr><td colspan=3><table border=0 width=100% cellspacing=0 cellpadding=0>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption><td width=25 height=24>S#</td><td width=60 height=24>&nbsp;Part #</td>");
	sbStartSection.append("<td  width=420>Description</td><td align=center width=60>Qty</td>");
	sbStartSection.append("<td align=center width=80>Location</td></tr>");
	sbStartSection.append("</table></td></tr></table></td><td bgcolor=#666666 width=1></td></tr></table>");

	StringBuffer sbEndSection = new StringBuffer();
	sbEndSection.append("<table width=700 cellspacing=0 cellpadding=0 align=center>");
	sbEndSection.append("<tr><td bgcolor=#666666 width=1 rowspan=7></td><td height=1 width=698></td><td bgcolor=#666666 width=1 rowspan=7></td></tr>");
	sbEndSection.append("<tr><td height=10>&nbsp;</td></tr>");
	sbEndSection.append("<tr><td height=50>&nbsp;</td></tr>");
	sbEndSection.append("<tr><td height=20 class=RightText valign=top><B>Agent</B></td></tr>");
	sbEndSection.append("<tr><td height=30 class=RightText valign=top><B><u>NOTES</u></B>:");
	if (!strPackSlipHeader.equals("SHIPPINGLIST")){ 
		sbEndSection.append("<BR>-ITEM(S) LISTED ABOVE ARE NOT FOR SALE.");
	}
	sbEndSection.append("</td></tr>");
	sbEndSection.append("<tr><td height=1 bgcolor=#666666></td></tr></table>");
	sbEndSection.append("<table border=0 width=700 cellspacing=0 cellpadding=0><tr><td align=center height=30 id=button>");
	sbEndSection.append("<input type=button value=&nbsp;Print&nbsp; name=Btn_Print class=button onClick=fnPrint();>&nbsp;&nbsp;");
	sbEndSection.append("<input type=button value=&nbsp;Close&nbsp; name=Btn_Close class=button onClick='javascript:window.close();';>&nbsp;&nbsp;");
	sbEndSection.append("</td><tr></table>");

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Consignment Packing Slip</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<style>
TR.PageBreak {
     page-break-after: always;
}
</style>
<script>

function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
</HEAD>

<BODY topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<!--StartSection starts here-->
<%out.print(sbStartSection);%>
<!--StartSection ends here-->
	<table border="0" width="700" cellspacing="0" cellpadding="0" align=center>
<%
	double dbCount = 0.0;
	intSize = alSetLoad.size();
	//if (intSize > 0)
	//{
	HashMap hmLoop = new HashMap();
	HashMap hmTempLoop = new HashMap();

	String strNextPartNum = "";
	int intCount = 0;
	String strColor = "";
	String strLine = "";
	String strPartNumHidden = "";
	String strPrice = "";
	String strTotal = "";
	String strAmount = "";
	String strRowCount = "";
	String strLocation = "";
	double dbAmount = 0.0;
	double dbTotal = 0.0;
	int intQty = 0;
	int intRowCnt = 0;
	int intalSize = alSetLoad.size();
	intSize = (intSize >=29?intSize:29);
	for (int i=0;i<intSize;i++)
	{
		intRowCnt ++;
	if(i < intalSize){
		hmLoop = (HashMap)alSetLoad.get(i);
		if (i<intalSize-1 && i<intSize-1)
		{
			hmTempLoop = (HashMap)alSetLoad.get(i+1);
			strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
		}
		strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
		strPartNumHidden = strPartNum;
		strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
		strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
		strLocation =  GmCommonClass.parseNull((String)hmLoop.get("SUGLOCATION"));
		strTemp = strPartNum;

		if (strPartNum.equals(strNextPartNum))
		{
			intCount++;
			strLine = "<tr><td bgcolor=#666666 width=1><img src=images\\spacer.gif width=1></td><TD colspan=5 height=1 bgcolor=#eeeeee></TD><td bgcolor=#666666 width=1><img src=images\\spacer.gif width=1></td></TR>";
		}

		else
		{
			strLine = "<tr><td bgcolor=#666666 width=1><img src=images\\spacer.gif width=1></td><TD colspan=5 height=1 bgcolor=#eeeeee></TD><td bgcolor=#666666 width=1><img src=images\\spacer.gif width=1></td></TR>";
			if (intCount > 0)
			{
				strPartNum = "";
				strPartDesc = "";
				strQty = "";
				strLine = "";
			}
			else
			{
				//strColor = "";
			}
			intCount = 0;
		}

		if (intCount > 1)
		{
			strPartNum = "";
			strPartDesc = "";
			strQty = "";
			strLine = "";
		}
	
		strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
		strItemQty = GmCommonClass.parseNull((String)hmLoop.get("IQTY"));
		strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
		out.print(strLine);
		strRowCount = Integer.toString(i+1);
	}else{
		strPartNum = "";
		strPartDesc ="";
		strItemQty = "";
		strControl = "";
		strRowCount = "";
		strShade = "";
	}
%>
		<tr <%=strShade %>>
			<td bgcolor="#666666" width="1"><img src="images\spacer.gif" width="1"></td>
			<td class="RightText" width="25"><%=strRowCount%></td>
			<td class="RightText" width="60" height="20">&nbsp;<%=strPartNum%></td>
			<td class="RightText" width="520"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
			<td class="RightText" align="center" width="60">&nbsp;<%=strItemQty%></td>
			<td class="RightText" align="center" width="100"><%=strLocation%></td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
<%
		if (intRowCnt == 30)
		{
%>
		<tr><td colspan="7" height="100%"></td></tr>
		<tr><td colspan="7" height="1" bgcolor="#666666"></td></tr>
		<tr><td colspan="7" height="20" class="RightText" align="right">Cont. on Next Page -></td></tr>
		</table>
		<p><br style="page-break-before: always;" clear="all" /></p>
<%
		out.print(sbStartSection);
%>
		<table border="0" width="700" cellspacing="0" cellpadding="0" align=center>
<%
		intRowCnt = 0;
		} //end of IF

	} // end of FOR
%>
	</table>
<!--EndSection starts here-->
<%out.print(sbEndSection);%>
<!--EndSection ends here-->

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
