<%@page import="java.util.Locale"%>
<%@ taglib uri="com.corda.taglib" prefix="ctl" %>
 <%
/**********************************************************************************
 * File		 		: GmDailySalesReport.jsp
 * Desc		 		: This screen is used for the Account Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>

<%@ taglib prefix="fmtDailySalesReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!--  custservice\GmDailySalesReport.jsp --> 
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtDailySalesReport:setLocale value="<%=strLocale%>"/>
<fmtDailySalesReport:setBundle basename="properties.labels.custservice.GmDailySalesReport"/>


<%
try {
	GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmDailySalesReport", strSessCompanyLocale);
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	String strCommonPath = GmCommonClass.getString("GMCOMMON");
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	ArrayList alReturn = (ArrayList)hmReturn.get("SALESREPORT");
	String strType = (String)request.getAttribute("hType")==null?"":(String)request.getAttribute("hType");
	String strFrmDate = (String)request.getAttribute("hFrom")==null?"":(String)request.getAttribute("hFrom");
	String strToDate = (String)request.getAttribute("hTo")==null?"":(String)request.getAttribute("hTo");
	int intLength = alReturn.size();

	String strGraphTitle = "";
	if (strType.equals("DAY"))
	{
		strGraphTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_TODAYS_SALES"));
	}
	else if (strType.equals("MON"))
	{
		strGraphTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_MONTH_TO_DATE_SALES"));
	}
	int intSize = 0;
	HashMap hmLoop = new HashMap();

	String strId = "";
	String strName = "";
	String strDistId = "";
	String strDistName = "";
	String strDate = "";
	String strSales = "";
	String strDay = "";

	String strPrice = "";

	String strShade = "";
	String strNextId = "";
	int intCount = 0;
	String strLine = "";
	String strTotal = "";
	String strAcctTotal = "";
	String strAvgDaily = "";
	
	boolean blFlag = false;
	double dbSales = 0.0;
	double dbAcctTotal = 0.0;
	double dbTotal = 0.0;
	double dbAvgDaily = 0.0;
	double dbTempTotal = 0.0;
	
	ArrayList alDist = new ArrayList();
	ArrayList alSales = new ArrayList();
	ArrayList alDate = new ArrayList();

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Sales Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>

function fnLoadReport(val)
{
	if (val=="ACCT" || val=="MON")
	{
		document.all.filter.style.visibility = "visible";
	}
	else
	{
		document.all.filter.style.visibility = "hidden";
	}
}

function fnSubmit(val)
{
	document.frmAccount.hType.value = val;
	document.frmAccount.submit();
}

function fnLoad()
{
	obj = document.frmAccount.hType;
	obj1 = document.frmAccount.Cbo_Type;

	for (var i=0;i<obj1.length;i++)
	{
		if (obj.value == obj1.options[i].value)
		{
			obj1.options[i].selected = true;
			fnLoadReport(obj.value);
			break;
		}
	}

}

function Toggle(val)
{
	
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className="";
		tabobj.style.background="#ffffff";
	}
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnLoad();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmDailySalesServlet">
<input type="hidden" name="hType" value="<%=strType%>">

	<table border="0" width="550" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="550" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" class="RightDashBoardHeader" colspan="3">
							<fmtDailySalesReport:message key="LBL_DASHBOARD_SALES"/>
						</td>
					</tr>
					<tr><td class="Line" colspan="3"></td></tr>
					<tr>
						<td height="25" class="RightTextBlue" colspan="3">&nbsp;<fmtDailySalesReport:message key="LBL_CHOOSE_TYPE_REPORT"/>:
&nbsp;<select name="Cbo_Type" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" onChange="javascript:fnLoadReport(this.value);">
								<option value="0" ><fmtDailySalesReport:message key="LBL_CHOOSE_ONE"/>
								<option value="DAY"><fmtDailySalesReport:message key="LBL_TODAYS_SALES"/>
								<option value="DAILY"><fmtDailySalesReport:message key="LBL_DAILY_SALES_CURRENT_MONTH"/>
								<option value="MON"><fmtDailySalesReport:message key="LBL_MONTH_DATE_SALES"/>
								<option value="ACCT"><fmtDailySalesReport:message key="LBL_SALES_BY_ACCOUNT"/>
							</select>&nbsp;&nbsp;
							<fmtDailySalesReport:message key="BTN_GO" var="varGo"/>
							<gmjsp:button value="&nbsp;${varGo}&nbsp;" name="Btn_Go" gmClass="button" onClick="javascript:fnSubmit(document.frmAccount.Cbo_Type.value);" buttonType="Load" />
							<BR><div id="filter">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td align="right" class="RightText" HEIGHT="24" width="150"><fmtDailySalesReport:message key="LBL_FROM_DATE"/>:</td>
						<td class="RightText">&nbsp<input type="text" size="10" value="<%=strFrmDate%>" name="Txt_FromDate" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;<fmtDailySalesReport:message key="IMG_ALT_OPEN" var = "varOpen"/>
						<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmAccount.Txt_FromDate');" title="${varOpen}"  src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />
&nbsp;&nbsp;<fmtDailySalesReport:message key="LBL_TO_DATE"/>: <input type="text" size="10" value="<%=strToDate%>" name="Txt_ToDate" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;<fmtDailySalesReport:message key="IMG_ALT_OPEN" var = "varOpen"/>
<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmAccount.Txt_ToDate');" title="${varOpen}"  src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />						
						</td>
					</tr>
				</table>
							</div>
						</td>
					</tr>
					<tr><td class="Line" colspan="3"></td></tr>
<%
					if (!strType.equals("DAILY"))
					{
%>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="200"><fmtDailySalesReport:message key="LBL_DISTRIBUTOR"/></td>
						<td width="450"><fmtDailySalesReport:message key="LBL_ACCOUNT_NAME"/></td>
						<td width="150"><fmtDailySalesReport:message key="LBL_AMOUNT"/></td>
					</tr>
<%
						if (intLength > 0)
						{
							HashMap hmTempLoop = new HashMap();

							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alReturn.get(i);
								if (i<intLength-1)
								{
									hmTempLoop = (HashMap)alReturn.get(i+1);
									strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("DID"));
								}

								strDistId = GmCommonClass.parseNull((String)hmLoop.get("DID"));
								strDistName = GmCommonClass.parseNull((String)hmLoop.get("DNAME"));
								strId	= GmCommonClass.parseNull((String)hmLoop.get("ID"));
								strName = GmCommonClass.parseNull((String)hmLoop.get("NAME"));
								strPrice = GmCommonClass.parseZero((String)hmLoop.get("SALES"));

								dbSales = Double.parseDouble(strPrice);
								dbAcctTotal = dbAcctTotal + dbSales;
								dbTotal = dbTotal + dbSales;
								if (strDistId.equals(strNextId))
								{
									intCount++;
									strLine = "<TR><TD colspan=3 height=1 class=Line></TD></TR>";
								}
								else
								{
									strLine = "<TR><TD colspan=3 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
										strDistName = "";
										strLine = "";
									}
									else
									{
										blFlag = true;
									}
									intCount = 0;
									
								}
								strAcctTotal = ""+dbAcctTotal;

								if (intCount > 1)
								{
									strDistName = "";
									strLine = "";
								}
							
								strShade = (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows

								out.print(strLine);
								strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
								if (intCount == 1 || blFlag)
								{
																		alDist.add(strDistName);
%>
					<tr id="tr<%=strDistId%>">
						<td colspan="2" height="20" class="RightText">&nbsp;<A class="RightText" title="Click to Expand" href="javascript:Toggle('<%=strDistId%>')"><%=strDistName%></a></td>
						<td class="RightText" id="td<%=strDistId%>" align="right"></td>
					</tr>
					<tr>
						<td colspan="5"><div style="display:none" id="div<%=strDistId%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tab<%=strDistId%>">
<%
								blFlag = false;
								}
%>
					<tr <%=strShade%>>
						<td width="200" class="RightText" height="20">&nbsp;</td>
						<td width="450" class="RightText">&nbsp;<%=strName%></td>
						<td width="150" class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strPrice)%>&nbsp;</td>
					</tr>
					<tr><td colspan="3" bgcolor="#cccccc"></td></tr>
<%					
					if ( intCount == 0 || i == intLength-1)
					{
						alSales.add(GmCommonClass.getStringWithCommas(strAcctTotal));
%>
							</table></div>
						</td>
					</tr>
					<script>
						document.all.td<%=strDistId%>.innerHTML = "$<%=GmCommonClass.getStringWithCommas(strAcctTotal)%>&nbsp;";
					</script>
<%
						dbAcctTotal = 0.0;					
					}
				strTotal = ""+dbTotal;
				}// end of FOR
%>
					<tr><td colspan="3" class="Line"></td></tr>
					<tr>
						<td align="right"  class="RightTableCaption" height="20" colspan="2"><fmtDailySalesReport:message key="LBL_GRAND_TOTAL"/>&nbsp;</td>
						<td class="RightTableCaption" align="right">$<%=GmCommonClass.getStringWithCommas(strTotal)%>&nbsp;</td>
					</tr>
					<tr><td colspan="3" class="Line"></td></tr>
					<tr>
						<td colspan="3">
					<%
						//out.print(alDist);
						//out.print(alSales);
						StringBuffer sbPcScript = new StringBuffer();
						sbPcScript.append("graph.setcategories()");
						for (int d=0;d<alDist.size();d++)
						{
							sbPcScript.append("graph.setseries("+alDist.get(d)+";"+alSales.get(d)+")");
						}
						//out.print(sbPcScript.toString());
					%>
					<!-- Begin Embedder Code -->
					<ctl:Corda
						appearanceFile="apfiles/Chart2.pcxml"
						externalServerAddress="http://www.globusmedical.info:2001"
						internalCommPortAddress="http://192.168.1.22:2002"
						language="EN"
						returnDescriptiveLink="false"
						width="540"
						height="330"
						outputType="FLASH">
						<ctl:pcScript>Title.SetText(<%=strGraphTitle%>)<%=sbPcScript.toString()%></ctl:pcScript>
					</ctl:Corda>
					<!-- End Embedder Code -->
						</td>
					</tr>
<%
						} // End of IF
						else{
%>
					<tr><td colspan="3" class="Line"></td></tr>					
					<tr><td colspan="3" class="RightTextRed" height="50" align=center><fmtDailySalesReport:message key="LBL_NO_SALES_REPORT"/> ! </td></tr>
<%					
						}
					}else{
						strGraphTitle = "Daily Sales - Current Month";
%>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="200">&nbsp;<fmtDailySalesReport:message key="LBL_DATE"/></td>
						<td width="450"><fmtDailySalesReport:message key="LBL_DAY"/></td>
						<td width="150"><fmtDailySalesReport:message key="LBL_TOTAL_AMOUNT"/></td>
					</tr>
					<tr><td colspan="3" class="Line"></td></tr>
<%
						if (intLength > 0)
						{
							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alReturn.get(i);
								strDate = GmCommonClass.parseZero((String)hmLoop.get("SDATE"));
								strDay = GmCommonClass.parseZero((String)hmLoop.get("DAY"));
								strSales = GmCommonClass.parseZero((String)hmLoop.get("SALES"));
								dbSales = Double.parseDouble(strSales);
								dbTotal = dbTotal + dbSales;
								dbTempTotal = dbTempTotal + dbSales;
								alDate.add(strDate);
								alSales.add(GmCommonClass.getStringWithCommas(strSales));
%>
					<tr class="RightText">
						<td HEIGHT="22" >&nbsp;<%=strDate%></td>
						<td ><%=strDay%></td>
						<td align="right">$<%=GmCommonClass.getStringWithCommas(strSales)%>&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<tr><td colspan="3" bgcolor="#cccccc"></td></tr>
					
<%
								if (strDay.equals("Friday"))
								{
									strTotal = ""+dbTempTotal;
%>
					<tr bgcolor="#ecf6fe">
						<td class="RightTableCaption" align="right" height="20" colspan="2"><fmtDailySalesReport:message key="LBL_WEEK_TOTAL"/>&nbsp;</td>
						<td class="RightTableCaption" align="right">$<%=GmCommonClass.getStringWithCommas(strTotal)%>&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<tr><td colspan="3" class="Line"></td></tr>
<%								
									dbTempTotal = 0.0;
								}
							}
						strTotal = ""+dbTotal;
						dbAvgDaily = dbTotal / intLength;
						strAvgDaily = ""+dbAvgDaily;
%>
					<tr class="ShadeRightTableCaptionBlue">
						<td align="right"  class="RightTableCaption" height="20" colspan="2"><fmtDailySalesReport:message key="LBL_GRAND_TOTAL"/>&nbsp;</td>
						<td class="RightTableCaption" align="right">$<%=GmCommonClass.getStringWithCommas(strTotal)%>&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<tr><td colspan="3" class="Line"></td></tr>
					<tr>
						<td align="right"  class="RightTableCaption" height="20" colspan="2"><fmtDailySalesReport:message key="LBL_AVG_MONTH_SALE"/>&nbsp;</td>
						<td class="RightTableCaption" align="right">$<%=GmCommonClass.getStringWithCommas(strAvgDaily)%>&nbsp;&nbsp;&nbsp;</td>
					</tr>
					<tr><td colspan="3" class="Line"></td></tr>					
					<tr>
						<td colspan="3">
					<%
						//out.print(alDist);
						//out.print(alSales);
						StringBuffer sbPcScript = new StringBuffer();
						sbPcScript.append("graph.setcategories()");
						for (int d=0;d<alDate.size();d++)
						{
							sbPcScript.append("graph.setseries("+alDate.get(d)+";"+alSales.get(d)+")");
						}
						//out.print(sbPcScript.toString());
					%>
					<!-- Begin Embedder Code -->
					<ctl:Corda
						appearanceFile="apfiles/Chart3.pcxml"
						externalServerAddress="http://www.globusmedical.info:2001"
						internalCommPortAddress="http://192.168.1.22:2002"
						language="EN"
						returnDescriptiveLink="false"
						width="540"
						height="330"
						outputType="FLASH">
						<ctl:pcScript>Title.SetText(<%=strGraphTitle%>)<%=sbPcScript.toString()%></ctl:pcScript>
					</ctl:Corda>
					<!-- End Embedder Code -->
						</td>
					</tr>
<%
						}
						else
						{
%>
					<tr><td colspan="3" class="RightTextRed" height="50" align=center><fmtDailySalesReport:message key="LBL_NO_SALES_REPORT"/> ! </td></tr>
<%
						}					
				}
%>
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>

</FORM>


<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
