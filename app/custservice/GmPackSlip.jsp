
 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmPackSlip.jsp
 * Desc		 		: This screen is used for the Packing Slip
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%><!-- \custservice\GmPackSlip.jsp-->
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<% 
	String strAction=null;
	String strSource="";
	String strApplnDateFmt = strGCompDateFmt;
	
	strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
		   strAction = (strAction.trim() == null) ? "PrintPack" : strAction;
		   strAction = (strAction.equals("")) ? "PrintPack" : strAction;
    String strOpt = GmCommonClass.parseNull(request.getParameter("hOpt"));
	strSource = GmCommonClass.parseNull((String)request.getAttribute("SOURCE"));
	String strGUID = GmCommonClass.parseNull(request.getParameter("hGUId"));
	
	HashMap hmReturn = new HashMap();
	HashMap hmAtbData = new HashMap();
	HashMap hmCompAtbData = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	hmAtbData = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("ORDER_ATB"));
	hmCompAtbData = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("COMP_ATB"));
	
	HashMap hmOrderDetails = new HashMap();
	HashMap hmShipDetails = new HashMap();

	ArrayList alCartDetails = new ArrayList();
	ArrayList alPopulatedCartDtls = new ArrayList();
	ArrayList alSterilePartList = new ArrayList();
	 
	ArrayList alBackOrderDetails = new ArrayList();
	
	String strShade = "";
	String strProjectType = "";
	String strHeadAddrSticker = "";
	String strStickCompanyLocale  = "";

	String strAccNm = "";
	String strDistRepNm = "";
	String strBillAdd = "";
	java.sql.Date dtOrdDate = null;
	String strShipAdd = "";
	String strPO = "";
	String strOrdId = "";

	String strPartNum = "";
	String strDesc = "";
	String strPrice = "";
	String strQty = "";
	String strItemOrdId = "";
	String strPartNums = "";
	String strControlNum = "";
	String strShipDate = "";
	String strShipCarr = "";
	String strShipMode = "";
	String strTrack = "";
	String strOrderType = "";
	String strParentId = "";
	String strItemOrdType = "";
	String strCompId = "";
	String strCompanyName = "";
	String strCompanyShortName = "";
	String strCompanyAddress = "";
	String strCompanyPhone = "";
	String strGmCompanyVat="";
	String strGmCompanyCst="";
	String strGmCompanyPAN="";
	String strGmLocalLic="";
	String strGmExportLic="";
	String strExportDruglic="";
	String strLocalDruglic="";
	String strSurgeonName="";
	String strCompLogo = "";
	String strDivisionId = "";
	String strShowTotalQty = "";
	String strShowPhoneNo = "";
	String strShowGMNAAddress = "";
	String strCompanyId = "";
	String strPlantId = "";
	String strCompStickerId = "";
	
	String strOrdredDate  = "";
	String strShippingDate = "";
	
	String strExpiryDateFl = "";
	String strExpiryDateVal = "";
	String strSterileExpiryFl = "";
	
	strPlantId = GmCommonClass.parseNull(gmDataStoreVO.getPlantid());
	strCompStickerId = GmCommonClass.parseNull(gmDataStoreVO.getCmpid()); 
	strCompanyId = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strPlantId, "PLANT_PACK_SLIP")); //Rulevalue:= 1010 (or) 1017
	strExpiryDateFl = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strPlantId, "PACK_SLIP_EXPIRY")); 
	if(strCompanyId.equals("")){
		  strCompanyId=gmDataStoreVO.getCmpid(); 
		}
	String strCompanyLocale = GmCommonClass.getCompanyLocale(strCompanyId);
	
	GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
	GmResourceBundleBean gmCompResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	strShowPhoneNo = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.SHOW_PHONE_NO"));
	strShowGMNAAddress = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.SHOW_GMNA_ADDRESS"));
	//String straddress = GmCommonClass.getString("GMCOMPANYADDRESS");
	//String strGMName = GmCommonClass.getString("GMCOMPANYNAME");
	//straddress = "&nbsp;" + straddress.replaceAll("/", "\n<br>&nbsp;");
	String strHeadAddress = ""; 
	String strBackOrderSec = "";
	//PMT-32393 Ability to record usage code but not list them on the Pack Slip
	alPopulatedCartDtls =  GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("alCartDetails"));
	alSterilePartList =  GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALSTERILEPARTLIST"));
	boolean blBackFl = false;
	boolean sterilePartFl = false;
	
	if (hmReturn != null)
	{
		hmOrderDetails 		= (HashMap)hmReturn.get("ORDERDETAILS");
		//alCartDetails 		= (ArrayList)hmReturn.get("CARTDETAILS");
		//PMT-38134 Packing Slip is Blank in Details of Shipment Emails
		if(alPopulatedCartDtls.isEmpty() && alPopulatedCartDtls != null)
		{
			alPopulatedCartDtls	= GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("CARTDETAILS"));
		}
		alBackOrderDetails	= (ArrayList)hmReturn.get("BACKORDER");
		hmShipDetails 		= (HashMap)hmReturn.get("SHIPDETAILS");

		strPO = GmCommonClass.parseNull((String)hmOrderDetails.get("PO"));
		strPO = strPO.length()>20? strPO.substring(0, 20):strPO; //Should show only 20 characters of customer Po
		strOrdId = GmCommonClass.parseNull((String)hmOrderDetails.get("ID"));
		strShipAdd = GmCommonClass.parseNull((String)hmOrderDetails.get("SHIPADD"));
		strBillAdd = GmCommonClass.parseNull((String)hmOrderDetails.get("BILLADD"));
		strAccNm = GmCommonClass.parseNull((String)hmOrderDetails.get("ANAME"));
		strDistRepNm = GmCommonClass.parseNull((String)hmOrderDetails.get("REPDISTNM"));
		//dtOrdDate = (java.sql.Date)hmOrderDetails.get("ODT");
		//strOrdredDate = GmCommonClass.parseNull((String)hmOrderDetails.get("ODT"));
		strOrdredDate = GmCommonClass.getStringFromDate((java.util.Date)hmOrderDetails.get("ODT"),strApplnDateFmt);
		strShippingDate = GmCommonClass.getStringFromDate((java.util.Date)hmOrderDetails.get("SHIPPING_DT"),strApplnDateFmt);
		strOrderType  = GmCommonClass.parseNull((String)hmOrderDetails.get("ORDERTYPE"));
		strParentId	= GmCommonClass.parseNull((String)hmOrderDetails.get("PARENTORDID"));
		strShipCarr	= GmCommonClass.parseNull((String)hmOrderDetails.get("CARRIER"));
		strShipMode	= GmCommonClass.parseNull((String)hmOrderDetails.get("SHIPMODE"));
		strTrack	= GmCommonClass.parseNull((String)hmOrderDetails.get("TRACKNO"));
		
		strGmCompanyVat =  GmCommonClass.parseNull((String) hmCompAtbData.get("GMCOMPANYVAT"));
		strGmCompanyCst =  GmCommonClass.parseNull((String) hmCompAtbData.get("GMCOMPANYCST"));
		strGmCompanyPAN =  GmCommonClass.parseNull((String) hmCompAtbData.get("GMCOMPANYPAN"));
		strGmLocalLic =  GmCommonClass.parseNull((String) hmCompAtbData.get("GMLOCALLIC"));
		strGmExportLic =  GmCommonClass.parseNull((String) hmCompAtbData.get("GMEXPORTLIC"));
		strExportDruglic =  GmCommonClass.parseNull((String) hmAtbData.get("EXPORTDRUGLIC"));
		strLocalDruglic =  GmCommonClass.parseNull((String) hmAtbData.get("LOCALDRUGLIC"));
		strSurgeonName =  GmCommonClass.parseNull((String) hmAtbData.get("SURGEON"));
	
		strCompId	= GmCommonClass.parseNull((String)hmOrderDetails.get("COMPID"));
		strDivisionId = GmCommonClass.parseNull((String)hmOrderDetails.get("DIVISION_ID"));
		strDivisionId = strDivisionId.equals("")?"2000":strDivisionId;	
		
		HashMap hmCompanyAdd = new HashMap ();
		hmCompanyAdd = GmCommonClass.fetchCompanyAddress(strCompanyId, strDivisionId);
		strCompLogo = GmCommonClass.parseNull((String)hmCompanyAdd.get("LOGO"));
	    strCompanyName = GmCommonClass.parseNull((String)hmCompanyAdd.get("COMPNAME"));
		strCompanyShortName = GmCommonClass.parseNull((String)hmCompanyAdd.get("COMP_SHORT_NAME"));
		strCompanyAddress =  GmCommonClass.parseNull((String)hmCompanyAdd.get("COMPADDRESS"));
		if(strShowPhoneNo.equals("YES")){
		strCompanyPhone = GmCommonClass.parseNull((String)hmCompanyAdd.get("PH"));
		}
		if(strShowGMNAAddress.equals("YES")){
		  strCompanyName = GmCommonClass.parseNull((String)gmCompResourceBundleBean.getProperty("GMNA.COMPNAME"));
		  strCompanyAddress = GmCommonClass.parseNull((String)gmCompResourceBundleBean.getProperty("GMNA.COMPADDRESS"));
		  strCompanyPhone = GmCommonClass.parseNull((String)gmCompResourceBundleBean.getProperty("GMNA.PH"));
		}
		strCompanyAddress = "&nbsp;"+ strCompanyAddress.replaceAll("/", "\n<br>&nbsp;"); 
		strCompanyPhone = !strCompanyPhone.equals("")? "<br>&nbsp;&nbsp;"+strCompanyPhone: "";
		strHeadAddress = "<br>&nbsp;&nbsp;<b>" +strCompanyName +"</b><BR>&nbsp;"+strCompanyAddress +strCompanyPhone;
		
		//****PC_5344****
		if(strCompStickerId.equals("1010") && strPlantId.equals("3003"))
		{
			strStickCompanyLocale = GmCommonClass.getCompanyLocale(strCompStickerId);
			GmResourceBundleBean gmStkCompResourceBundleBean =
			        GmCommonClass.getResourceBundleBean("properties.Company", strStickCompanyLocale);
			
			strHeadAddrSticker = "&nbsp;&nbsp;"+GmCommonClass.parseNull((String)gmStkCompResourceBundleBean.getProperty("COMPANY_ADDRESS_STICKER")).replace("<br>","");
			}
		if(!strShipAdd.equals("") && strShipAdd.substring(0,6).equals("&nbsp;")){
			strShipAdd= strShipAdd.replaceFirst("&nbsp;","");
		}
	
	}

	if (hmShipDetails != null)
	{
		strShipDate = GmCommonClass.getStringFromDate((java.util.Date)hmShipDetails.get("SDT"),strApplnDateFmt);
	//	strShipCarr = (String)hmShipDetails.get("SCARNM");
	//	strShipMode = (String)hmShipDetails.get("SMODENM");
	//	strTrack = (String)hmShipDetails.get("STRK");
	}

	
	// Combo box variable
	int intSize = 0;
	double intTotShippedQty = 0.0;
	HashMap hcboVal = null;
	ArrayList alComboList 	= new ArrayList();
	
	String strSelected  = "";
	String strCodeID = "";
	strShowTotalQty = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.SHOW_TOTAL_QTY"));
	
	if(!strGmLocalLic.equals("") || !strGmExportLic.equals(""))
	{
		strHeadAddress = strHeadAddress+"<br>&nbsp;&nbsp;D.L.NO."+strGmLocalLic+"<br>&nbsp;&nbsp;D.L.NO."+strGmExportLic;
	}
	if(!strExportDruglic.equals("") || !strLocalDruglic.equals(""))
	{
		strShipAdd = strShipAdd+"<br>&nbsp;DL- "+strLocalDruglic+"<br>&nbsp;DL- "+strExportDruglic;
	}
	String strRowSpan1 = "11";
	String strRowSpan2 = "5";
	String strShowShipDate = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("PACK_SLIP.SHOW_SHIP_DATE"));
	if(strShowShipDate.equalsIgnoreCase("Yes")){
	  strRowSpan1 = "15";
	  strRowSpan2 = "9";
	}
	strBackOrderSec = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("PACK_SLIP.BACKORDERSECTION"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Packing Slip </TITLE>

<script language="JavaScript" src="<%=strJsPath%>/GmPackSlip.js"></script>
<script>
var source = "<%=strSource%>";
var tdinnner = "";
var hopt = '<%=strOpt%>';
var strTrack ='<%=strTrack%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnHidebuttons();fnWPrint();" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmOrder" method="post">
<input type="hidden" name="hMode" value="PrintPack">
<input type="hidden" name="hOrdId" value="<%=strOrdId%>">
<fmt:setLocale value="${requestScope.CompanyLocale}"/>
<fmt:setBundle basename="properties.Paperwork"/>
<%if(strAction.equals("PrintPack")&&strGUID.equals(""))
	{
%>
	<table border="0" width="700" cellspacing="0" cellpadding="0" >
		<tr>
			<td align="center" height="30" id="button" class="RightText" >
				Project Type:&nbsp;<select name="Cbo_ProjectType" class="RightText" tabindex="1" 
				onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"
				onChange="javascript:fnPrintPack();" >
<% /****** Combo Box Value ***********/
				alComboList = (ArrayList)request.getAttribute("alcomboList");	
				strProjectType = (String) request.getAttribute("ProjectType");
				intSize = alComboList.size();
				hcboVal = new HashMap();
				
				// Functionality to remove the tracking information 
				// If the part is not delivered by Globus 
				if (!strProjectType.equals("1500"))
				{
					strShipDate = "";
					strShipCarr = "";
					strShipMode = "";
					strTrack = "";				
				}
			  	for (int i=0;i<intSize;i++)
			  	{	hcboVal = (HashMap)alComboList.get(i);
			  		strCodeID = (String)hcboVal.get("CODEID");
					strSelected = strProjectType.equals(strCodeID)?"selected":"";
%>					<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%			  	}
%>				</select>
				<gmjsp:button value="&nbsp;Print&nbsp;" name="Btn_Print" gmClass="button" tabindex="2" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				<gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_Close" gmClass="button" tabindex="3"	onClick="fnClose();" buttonType="Load" />
			</td>
		<tr>
	</table>
<%
	}
%>
	<table align="center" border="0" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" height="1" colspan="3"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td height="80" width="170"><img src="<%=strImagePath%>/<%=strCompLogo%>.gif" width="138" height="60"></td>
						<td class="RightText" width="270"><%=strHeadAddrSticker%><%=strHeadAddress%><td>
						<!-- 102364:Stock Tranfer order Type -->
						 <%if(strOrderType.equals("102364")){%>
						<td align="right" class="RightText"><font size="+3"><fmt:message key="PACK_SLIP.STOCKTRNS_TITLE"/></font>&nbsp;</td>
						<%}else{%>
						<td align="right" class="RightText"><font size="+3"><fmt:message key="PACK_SLIP.TITLE"/></font>&nbsp;</td>
						<%}%> 
					</tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
		</tr>
			<td bgcolor="#666666" height="1" colspan="3"></td>
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr class="RightTableCaption" bgcolor="#eeeeee">
									<td height="24" align="center" width="250">&nbsp;<fmt:message key="PACK_SLIP.BILLTO"/></td>
									<td bgcolor="#666666" width="1" rowspan="<%=strRowSpan1%>"></td>
									<td width="250" align="center">&nbsp;<fmt:message key="PACK_SLIP.SHIPTO"/></td>
									<td bgcolor="#666666" width="1" rowspan="<%=strRowSpan1%>"></td>
									<td width="100" align="center">&nbsp;<fmt:message key="PACK_SLIP.DODATE"/></td>
									<td width="100" align="center">&nbsp;<fmt:message key="PACK_SLIP.DONUM"/></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="6"></td>
								</tr>
								<tr>
									<td class="RightText" rowspan="<%=strRowSpan2%>" valign="top">&nbsp;<%=strBillAdd%></td>
									<td rowspan="<%=strRowSpan2%>" class="RightText" valign="top">&nbsp;<%=strShipAdd%></td>
									<td height="25" class="RightText" align="center">&nbsp;<%=strOrdredDate%></td>
									<td class="RightTableCaption" align="center">&nbsp;<%=strOrdId%></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
								</tr>
								<% if(!strShowShipDate.equalsIgnoreCase("Yes")){ %>
								<tr bgcolor="#eeeeee" class="RightTableCaption" height="14">
									<td align="center"><fmt:message key="PACK_SLIP.DISTREP"/></td>
									<td align="center"><fmt:message key="PACK_SLIP.CUSTPONUM"/></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
								</tr>
								<tr>
									<td align="center" class="RightText">&nbsp;<%=strDistRepNm%><Br></td>
									<td align="center" height="25" class="RightText">&nbsp;<%=strPO%></td>
								</tr>
								<%}else{ %>
								<tr bgcolor="#eeeeee" class="RightTableCaption">
									<td align="center" colspan="2"><fmt:message key="PACK_SLIP.DISTREP"/></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
								</tr>
								<tr>
									<td align="center" class="RightText" colspan="2" height="20">&nbsp;<%=strDistRepNm%><Br></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
								</tr>
								<tr bgcolor="#eeeeee" class="RightTableCaption">
								<td align="center"><fmt:message key="PACK_SLIP.SHIPDATE"/></td>
								<td align="center"><fmt:message key="PACK_SLIP.CUSTPONUM"/></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
								</tr>
								<tr>
								<td align="center" height="20" class="RightText">&nbsp;<%=strShippingDate %></td>
								<td align="center" height="20" class="RightText">&nbsp;<%=strPO%></td>
								</tr>
								<%} %>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="6"></td>
								</tr>
								<tr class="RightTableCaption" bgcolor="#eeeeee">
									<td height="24" align="center"><fmt:message key="PACK_SLIP.SHIPVIA"/></td>
									<td align="center"><fmt:message key="PACK_SLIP.SHIPMODE"/></td>
									<td align="center" colspan="2"><fmt:message key="PACK_SLIP.TRACKINGNUM"/></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="6"></td>
								</tr>
								<tr>
									<td height="23" align="center" class="RightText"><%=strShipCarr%></td>
									<td align="center" class="RightText"><%=strShipMode%></td>
									<td align="center" colspan="2" class="RightText"><%=strTrack%></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" height="1" bgcolor="#666666"></td>
					</tr>
					<tr>
						<td align="center" colspan="2" valign="top" height="400">
							<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0">
								<tr class="RightTableCaption" bgcolor="#eeeeee">
									<td  width="100" height="24">&nbsp;<fmt:message key="PACK_SLIP.PARTNUMBER"/></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td  width="300">&nbsp;<fmt:message key="PACK_SLIP.PARTDESC"/></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="center" width="100"><fmt:message key="PACK_SLIP.QTY"/></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="center" width="100"><fmt:message key="PACK_SLIP.CONTROLNUM"/></td>
									 <%if(strExpiryDateFl.equals("Y")||(alSterilePartList.size()>0)){%>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="center" width="100"><fmt:message key="PACK_SLIP.EXPIRYDTAE"/></td>
									<%} %> 
								</tr>
								<tr>
								<%if(strExpiryDateFl.equals("Y")||(alSterilePartList.size()>0)){%>
								<td colspan="9" height="1" bgcolor="#666666"></td>
								<%}else{ %>	
								<td colspan="7" height="1" bgcolor="#666666"></td>
								<%} %>	
								</tr>
<%
			  	if (!strParentId.equals(strOrdId) || !strTrack.equals(""))
			  	{
			  		blBackFl = true;
			  		intSize = alPopulatedCartDtls.size();
					hcboVal = new HashMap();
					String strItems = "";

			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alPopulatedCartDtls.get(i);
			  			strPartNum = (String)hcboVal.get("ID");
						strDesc = (String)hcboVal.get("PDESC");
						strQty = (String)hcboVal.get("QTY");
						strControlNum = (String)hcboVal.get("CNUM");
						strItemOrdType = GmCommonClass.parseNull((String)hcboVal.get("ITEMTYPE"));
						intTotShippedQty =  intTotShippedQty + Double.parseDouble(strQty);
						strExpiryDateVal = GmCommonClass.parseNull((String)hcboVal.get("PRODEXPIRY"));
						strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
						//check if alSterilePartList contains sterile part, if sterile available then make sterile expiry flag 'Y'-PMT#48418
						if(alSterilePartList.size()>0)
						{
						sterilePartFl = alSterilePartList.contains(strPartNum);
						}
						if(sterilePartFl)
						{
							strSterileExpiryFl = "Y";
						}
						else
						{
							strSterileExpiryFl = "";
						}
						if (strItemOrdType.equals("50300"))
						{						
%>
								<tr>
									<td align = "left"  class="RightText" height="20">&nbsp;<%=strPartNum%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align = "left"  class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText" align="center">&nbsp;<%=strQty%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="center" class="RightText">&nbsp;<%=strControlNum%></td>
									<%if((strExpiryDateFl.equals("Y"))|| (alSterilePartList.size()>0)){%>
									<td width="1" bgcolor="#eeeeee"></td>
									<%}%>
									 <%if((strExpiryDateFl.equals("Y"))|| (strSterileExpiryFl.equals("Y"))){%>
									<td align="center" class="RightText">&nbsp;<%=strExpiryDateVal%></td>
									<%} %> 
								</tr>
								<tr>
								<%if((strExpiryDateFl.equals("Y"))|| (alSterilePartList.size()>0)){%>
								<td colspan="9" height="1" bgcolor="#666666"></td>
								 <%}else{ %>	
								<td colspan="7" height="1" bgcolor="#666666"></td>
								<%} %>
								</tr>						
<%
						} // end of IF for parts from Sales Consignments
					}
				}
				//*******************************************
				// Below code to display back order details
				//*******************************************
				 if (strTrack.equals("") || strBackOrderSec.equalsIgnoreCase("YES")) //Need to show the Back Order section for NEDC Entity Companies
				{						
					intSize = alBackOrderDetails.size();
					if (intSize != 0)
					{
%>						
					<tr>
						<td>&nbsp;</td>
						<td width="1" bgcolor="#eeeeee"></td>
						<td>&nbsp;</td>
						<td width="1" bgcolor="#eeeeee"></td>
						<td>&nbsp;</td>
						<td width="1" bgcolor="#eeeeee"></td>
						<td>&nbsp;</td>
					</tr>
					
					<%if(alSterilePartList.size()>0){%>
						<tr><td colspan="9" height="1" class="Line"></td></tr>
						<tr bgcolor="#eeeeee" ><td class="RightTextRed" colspan="9" height="24">
						<%}else{ %>	
						<tr><td colspan="7" height="1" class="Line"></td></tr>
						<tr bgcolor="#eeeeee" ><td class="RightTextRed" colspan="7" height="24">
						<%} %>
						<b>The following item(s) will ship separately.</b>
					</td></tr>
					<%if(alSterilePartList.size()>0){%>
					<tr><td colspan="9" height="1" class="Line"></td></tr>
					<%}else{ %>
					<tr><td colspan="7" height="1" class="Line"></td></tr>
					<%} %>									
<%			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alBackOrderDetails.get(i);
			  			strPartNum = (String)hcboVal.get("ID");
						strDesc = (String)hcboVal.get("PDESC");
						strQty = (String)hcboVal.get("QTY");
						strControlNum = (String)hcboVal.get("CNUM");
						strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
%>						<tr>
							<td class="RightText" height="20">&nbsp;<%=strPartNum%></td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td class="RightText" align="center">&nbsp;<%=strQty%></td>
							<td width="1" bgcolor="#eeeeee"></td>
							<td align="center" class="RightText">&nbsp;</td>
						</tr>
						<%if(alSterilePartList.size()>0){%>
						<tr><td colspan="9" height="1" bgcolor="#eeeeee"></td></tr>	
						<%}else{ %>
						<tr><td colspan="7" height="1" bgcolor="#eeeeee"></td></tr>	
						<%} %>								
<%					} // For Loop ends here					
				}	
		}
%>
								<tr height="100%">
									<td>&nbsp;</td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td>&nbsp;</td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td>&nbsp;</td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td>&nbsp;</td>
									<%if(strExpiryDateFl.equals("Y")){%>
									<td width="1" bgcolor="#eeeeee"></td> 
									<td>&nbsp;</td>
									<%} %> 	
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<% if(strShowTotalQty.equalsIgnoreCase("YES")){ %>
					<tr><td height=10 class=RightText valign=top></td></tr> 
					<tr><td height=20 width=400  class=RightText valign=top>&nbsp;&nbsp;&nbsp; </td>
					<td height=30 width=350 valign=top align="left">&nbsp;<fmt:message key="PACK_SLIP.TOTAL_QTY"/> : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=intTotShippedQty%></td></tr>
					<%} %>
<%
	if (!strGmCompanyVat.equals("") || !strGmCompanyCst.equals("") || !strGmCompanyPAN.equals("")){
%>
				<tr>
				<td height=20 width=350  class=RightText valign=top>&nbsp;Company's VAT TIN&nbsp;:&nbsp;<%=strGmCompanyVat%> </td>
				<td height=20 width=350 valign=top align="left">&nbsp;<b>Surgery Details</b> &nbsp; </td>
				</tr>
				<tr>
				<td height=20 width=350 class=RightText valign=top>&nbsp;Company's CST No&nbsp;&nbsp;:&nbsp;<%=strGmCompanyCst%></td>
				<td height=20 width=350 valign=top align="left">&nbsp;Surgeon&nbsp;&nbsp;&nbsp;:<%=strSurgeonName%></td>
				</tr>
				<tr>
				<td height=20 class=RightText valign=top>&nbsp;Company's PAN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<%=strGmCompanyPAN%></td>
				</tr>
				
				<tr>
				
				<td height=1 width=350 class=RightText valign=top>
				<td width=350 class=RightText valign=top  rowspan="1" bgcolor="#666666"></td> 
				 				</tr>
								
					<tr>
						<td colspan="2">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr class="RightTableCaption">
									<td height="44" align="center" width="150">&nbsp;</td>
									<td  width="1" rowspan="11"></td>
									<td width="197" align="center">&nbsp;</td>
									<td bgcolor="#666666" width="1" rowspan="11"></td>
									<td width="50" align="center">&nbsp;</td>
									<td width="250" align="center" valign="top">&nbsp;<b>for Globus Medical India Pvt.Ltd</b></td>
								</tr>
								
								<tr>
									<td class="RightText" rowspan="9" valign="top"></td>
									<td rowspan="5" class="RightText" valign="top">&nbsp;</td>
									<td height="25" class="RightText" align="center">&nbsp;</td>
									<td class="RightTableCaption" align="center">&nbsp;Authorised Signatory</td>
								</tr>
					
								
							</table>
														
							</td>
							</tr>
						
				<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
				
<%
	}
	if (blBackFl && !strParentId.equals("")){
%>
					<tr bgcolor="#eeeeee" >
						<td class="RightTextRed" colspan="2" height="24">
							&nbsp;<u>NOTE</u>: The contents of this Packing Slip are part of the original Delivered Order #: <B><%=strParentId%></B>
						</td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
<%
	}
%>	
					<tr>
						<td colspan="2" align="center" height="30" class="RightText"><fmt:message key="PACK_SLIP.FOOTER"/></td>
					</tr>
				</table>

			</td>
		</tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
</FORM>

<% /* Below code to display bookmark value 
	* 2522 maps to duplicate order 
	* 2530 maps to Bill Only - Loaner order 
	*/  
	if (strOrderType.equals("2522") || strOrderType.equals("2530"))
	{
		String strImage = strOrderType.equals("2522")?"duplicate_order.jpg":"loaner-picslip.gif";
%>

<div id="waterMark" style="position:absolute; z-Index: 0; top: 340; left: 340">
	<img src="<%=strImagePath%>/<%=strImage%>"  border=0>
	</div>
<script language="JavaScript1.3" src="watermark.js">
</script>
<% 
	}
%>

<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
