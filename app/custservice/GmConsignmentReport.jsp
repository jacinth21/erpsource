
<%
/**********************************************************************************
 * File		 		: GmConsignmentReport.jsp
 * Desc		 		: This screen is used for the DashBoard Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>


<%@ taglib prefix="fmtConsignmentReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- custservice\GmConsignmentReport.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtConsignmentReport:setLocale value="<%=strLocale%>"/>
<fmtConsignmentReport:setBundle basename="properties.labels.custservice.GmConsignmentReport"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmConsignmentReport", strSessCompanyLocale);

	

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmLoop = new HashMap();
	HashMap hcboVal = new HashMap();

	String strDistId = (String)request.getAttribute("hDistId")==null?"":(String)request.getAttribute("hDistId");
	String strSetId = (String)request.getAttribute("hSetId")==null?"":(String)request.getAttribute("hSetId");
	String strType = (String)request.getAttribute("hType")==null?"":(String)request.getAttribute("hType");
	String strFromDt = (String)request.getAttribute("hFrmDt")==null?"":(String)request.getAttribute("hFrmDt");
	String strToDt = (String)request.getAttribute("hToDt")==null?"":(String)request.getAttribute("hToDt");	
	String strDistName = (String)request.getAttribute("distributorName")==null?"":(String)request.getAttribute("distributorName");
	String strConId = "";
	String strSetName = "";
	String strPerson = "";
	String strDateInitiated = "";
	String strComments = "";

	String strShade = "";
	String strCodeID = "";
	String strSelected = "";
	int intLoop = 0;
	String strLabel = "";
	String strCodeIDParam = "";
	String strCodeNameParam = "";

	if (strType.equals("50230"))
	{
		strLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_DISTRIBUTOR_LIST"));
		strCodeIDParam = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_ID"));
		strCodeNameParam = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_NAME"));

	}else if (strType.equals("INHOUSE"))
	{
		strLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_IN_HOUSE_PURPOSE"));
		strCodeIDParam = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_CODE_ID"));
		strCodeNameParam = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_CODENM"));
	}
	else  //strType.equals("ICT")  
	{
		strLabel = "ICT";
		strCodeIDParam = ""; //
		strCodeNameParam = ""; //
	}

	ArrayList alDistributor = new ArrayList();
	ArrayList alSets = new ArrayList();
	ArrayList alReport = new ArrayList();

	if (hmReturn != null) 
	{
		alDistributor = (ArrayList)hmReturn.get("DISTRIBUTORLIST");
		alSets = (ArrayList)hmReturn.get("SETLIST");
		alReport = (ArrayList)hmReturn.get("REPORT");
	}

	
	int intDistLength = GmCommonClass.parseNullArrayList(alDistributor).size();
	int intSetLength = GmCommonClass.parseNullArrayList(alSets).size();
	intLoop = GmCommonClass.parseNullArrayList(alReport).size();
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Consignment Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>
function fnPrintVer(val)
{
windowOpener("/GmConsignSetServlet?hAction=LoadConsign&hId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmConsignmentReportServlet">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hAction" value="">

	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="20" width="1" class="Line"></td>
			<td colspan="2" height="1" bgcolor="#666666"></td>
			<td rowspan="20" width="1" class="Line"></td>
		</tr>
		<tr>
			<td colspan="2" height="25" class="RightDashBoardHeader">
				<fmtConsignmentReport:message key="LBL_REPORT_SETS_CONSIGNMENT"/>
			</td>
		</tr>
		<tr><td class="Line" height="1" colspan="2"></td></tr>
		<tr><td> Name:&nbsp;<%=strDistName %> &nbsp; &nbsp;&nbsp;&nbsp;<fmtConsignmentReport:message key="LBL_SET_NUMBER"/>:&nbsp; <%=strSetId%></td></tr>
		
		<tr><td class="Line" colspan="2"></td></tr>
		<tr>
			<td colspan="2" align="center">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="80"><fmtConsignmentReport:message key="LBL_CONSIGN_ID"/></td>
						<td width="300"><fmtConsignmentReport:message key="LBL_SET_NAME"/></td>
						<td width="100" align="center"><fmtConsignmentReport:message key="LBL_CONSIGNED_DATE"/></td>
						<td width="200">&nbsp;<fmtConsignmentReport:message key="LBL_COMMENTS"/></td>
					</tr>
					<tr><td class="Line" colspan="4"></td></tr>
<%
			if (intLoop > 0)
			{
				for (int i = 0;i < intLoop ;i++ )
				{
					hmLoop = (HashMap)alReport.get(i);
					strConId = GmCommonClass.parseNull((String)hmLoop.get("CID"));
					strSetName = GmCommonClass.parseNull((String)hmLoop.get("SNAME"));
					strDateInitiated = GmCommonClass.parseNull((String)hmLoop.get("SDATE"));
					strComments = GmCommonClass.parseNull((String)hmLoop.get("COMMENTS"));

					strShade	= (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
%>
					<tr <%=strShade%>>
						<td height="20">&nbsp;<a class="RightText" href="javascript:fnPrintVer('<%=strConId%>');"><%=strConId%></a></td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strSetName)%></td>
						<td class="RightText" align="center"><%=strDateInitiated%></td>
						<td class="RightText">&nbsp;<%=strComments%></td>
					</tr>
<%
				}
			} else	{
%>
					<tr>
						<td height="30" colspan="4" align="center" class="RightTextBlue"><fmtConsignmentReport:message key="LBL_NO_SETS_CONSIGNED_DISTRIBUTOR"/> !</td>
					</tr>
<%
		}
%>
					<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
					<tr>	
						<td colspan="4" height="35" align="center">
				<fmtConsignmentReport:message key="BTN_RETURN" var="varReturn"/>
				<gmjsp:button value="&nbsp;${varReturn}&nbsp;" name="Btn_Return" gmClass="button" onClick="history.go(-1)" buttonType="Save" />
						</td>
					</tr>
					<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
				</table>

			</td>
		</tr>
	</table>
		
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
