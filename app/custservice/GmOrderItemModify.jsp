 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>

<%
/**********************************************************************************
 * File		 		: GmOrderItemModify.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%>  
<%@ page import ="java.util.Date"%>
<%@ page import ="org.apache.commons.validator.routines.DateValidator"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib prefix="fmtChangePrice" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmOrderItemModify.jsp -->
<fmtChangePrice:setLocale value="<%=strLocale%>"/>
<fmtChangePrice:setBundle basename="properties.labels.custservice.ModifyOrder.GmOrderItemModify"/>
<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	GmCalenderOperations gmCal = new GmCalenderOperations();
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	
	String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strhAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strMode = (String)request.getAttribute("hMode")==null?"":(String)request.getAttribute("hMode");
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.ModifyOrder.GmOrderItemModify", strSessCompanyLocale);
	strCountryCode = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("COUNTRYCODE"));
	String strApplnDateFmt = strGCompDateFmt;
	String strCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	String strAccCurrSymb ="";
	String strChecked = "";

	String strHeader =GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_CHANGE_PRICING"));
	String strWikiTitle = "Change_Pricing";
	String strOrderId = "";
	String strAccName = "";
	String strUserName = "";
	String dtOrdDate = "";
	java.sql.Date dtShipDate = null;
	java.util.Date dtOrderDt = null;
	String strComments = "";
	String strShade = "";
	String strItemQty = "";
	String strControl = "";
	
	String strCodeID = "";
	String strSelected = "";

	String strShipCost = "";
	String strPO = "";
	String strAccId = "";
	String strPriceReadOnly = "";
	String strReadOnly = "";
	String strShipReadOnly = "";
	String strNextPartNum = "";
	int intCount = 0;
	String strColor = "";
	String strLine = "";
	String strPartNumHidden = "";
	String strPartDesc = "";
	String strPrice = "";
	String strTemp = "";
	String strPartNum = "";
	String strItemId = "";
	String strReturnQty = "";
	double intQty = 0.0;
	int intReturnItemQTY = 0;
	double dbItemTotal = 0.0;
	double dbTotal = 0.0;
	String strItemTotal = "";
	String strTotal = "";
	double dblShip = 0.0;
	String strItemType = "";
	String strItemTypeDesc = "";
	
	double dbBeforeItemTotal = 0.0;
	double dbBeforePriceTotal = 0.0;
	double dbAfterItemTotal = 0.0;
	double dbAfterPriceTotal = 0.0;
	double dbBeforeTotal = 0.0;
	double dbAfterTotal = 0.0;
	
	String strBeforeItemTotal = "";
	String strAfterItemTotal = "";
	String strAfterPrice = "";
	String strBeforeTotal = "";
	String strAfterTotal = "";
	
	ArrayList alCart = new ArrayList(); 
	ArrayList alReturnReasons = new ArrayList();
	ArrayList alReturns = new ArrayList();
	ArrayList alAdjCodeList = new ArrayList();
	ArrayList alPricReqDtl = new ArrayList();
	
	HashMap hmOrderDetails = new HashMap();
	String strReason = "";
	String strRAId = "";
	String strCreatedBy = "";
	String strCreatedDt = "";
	String strStatus = "";
	String strExpDt = "";
	String strCreditDt = "";
	String strShipDt = "";
	String strInvFl = "";	
	int intPriceSize = 0;
	String strPortalPrice = "";
	String strTodaysDate =  gmCal.getCurrentDate(strGCompDateFmt);
	String strDeptId =  GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessDeptSeq"));
	alAdjCodeList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ADJCODEDETAILS")); 
	
	String disabled="";
	String strInvoiceId="";
	String strEditFL="Y";
	String strAccess="";
	String strOrdredDate = "";
	String strChangePriceFl="";
	String strChangePriceAccess = "";
	String strChnAccessFl = "";
	String strOrderType = "";
	String strEdtShipChrgFl = "";
	String strUnitPrice = "";
	String strUnitPriceAdj = "";
	String strAdjCode = "";
	String strNetUnitPrice = "";
	String strAdjTotal = "";
	double dbAdjTotal = 0.0;
	double dbAdjstTotal = 0.0;
	String strLblAdjCode = "";
	String strCurrPortlaPrice = "";
	String strAdjCodeValue = "";
	String strAccountPrice = "";
	double dbBeforePrice = 0.0;
	double dbAfterPrice = 0.0;
	double dbAdjustPrice = 0.0;
	double dbSubTotal = 0.0;
	String strAdjustAmount = "";
	String strOvrCurr = "";
	String strSurgeryDate = "";
	if (hmReturn != null)
	{
		hmOrderDetails = (HashMap)hmReturn.get("ORDERDETAILS");
		strOrderId = (String)hmOrderDetails.get("ID");
		strAccName = (String)hmOrderDetails.get("ANAME");
		strUserName = (String)hmOrderDetails.get("UNAME");
		//dtOrdDate = (java.sql.Date)hmOrderDetails.get("ODT");
		strOrdredDate = GmCommonClass.getStringFromDate((java.sql.Date)hmOrderDetails.get("ODT"),strApplnDateFmt);
		strComments = (String)hmOrderDetails.get("COMMENTS");
		strPO = (String)hmOrderDetails.get("PO");
		strAccId = (String)hmOrderDetails.get("ACCID");
		dtShipDate = (java.sql.Date)hmOrderDetails.get("SDATE");
		strInvFl = GmCommonClass.parseNull((String)hmOrderDetails.get("INVFL"));
		strInvoiceId =GmCommonClass.parseNull((String)hmOrderDetails.get("INVOICEID"));
		strOrderType = GmCommonClass.parseNull((String)hmOrderDetails.get("ORDERTYPE")); 
		strAccCurrSymb = GmCommonClass.parseNull((String)hmOrderDetails.get("ACC_CURR_SYMB"));
		strOvrCurr = GmCommonClass.parseNull((String) hmOrderDetails.get("OVRIDE_CURR"));
		strSurgeryDate = GmCommonClass.getStringFromDate((java.sql.Date)hmOrderDetails.get("ORD_SURG_DT"),strApplnDateFmt);
		strCurrSign = strAccCurrSymb.equals("")?strCurrSign:strAccCurrSymb; 
		strAccCurrSymb = strAccCurrSymb.equals("")?strCurrSign:strAccCurrSymb;
		strCurrSign = !strOvrCurr.equals("")? strOvrCurr: strCurrSign;
		
		alCart = (ArrayList)hmReturn.get("CARTDETAILS");		
		HashMap hmShipDetails = (HashMap)hmReturn.get("SHIPDETAILS");
		if (hmShipDetails != null)
		{
			strShipCost = GmCommonClass.parseZero((String)hmShipDetails.get("SCOST"));
		}
		alPricReqDtl = (ArrayList)hmReturn.get("ALPRICEREQDTL");	
	}

	int intSize = 0;
	HashMap hcboVal = null;

	if (strhAction.equals("ReturnPart"))
	{
		strHeader = "";GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_RETURN_PARTS"));
		alReturnReasons = (ArrayList)request.getAttribute("alReasons");
		strReadOnly = "ReadOnly";
		alReturns = (ArrayList)hmReturn.get("RETURNS");
	}
	
if (strhAction.equals("EditPrice")){
	strAccess= GmCommonClass.parseNull((String)request.getAttribute("READFL"));
	strChangePriceFl=GmCommonClass.parseNull((String)request.getAttribute("CHANGEPRICEFL"));
	strChangePriceAccess = GmCommonClass.parseNull((String)request.getAttribute("CHGPRICACCESSFL"));
	strChnAccessFl = GmCommonClass.parseNull((String)request.getAttribute("CHNACCESS"));
	strEdtShipChrgFl = GmCommonClass.parseNull((String)request.getAttribute("EDITSHIPCHARGE"));

	if (!strInvoiceId.equals("")){
			disabled="true";
		}
	
	if(strChangePriceAccess.equals("")){
		if(strAccess.equals("Y")){
			strPriceReadOnly ="ReadOnly";
		}else{
			strShipReadOnly = "ReadOnly";
		}
		if (!strDeptId.equals("2020") && !strDeptId.equals("2026") && strCountryCode.equals("en") ){//Dept id validation for US
			disabled="true";
		}
		if ((strDeptId.equals("2026") || !strCountryCode.equals("en")) && strChangePriceFl.equals("N")){//Change price access		
			disabled="true";
		}
		//Allow user to edit pricing based on security access,even if they are not part of A/R or Pricing dept. 
		// When Invoice is raised or the Month crossed, user should not be allowed to edit price.
		if (strChnAccessFl.equals("Y") && strInvoiceId.equals("") 
									   && strChangePriceFl.equals("Y")){
			disabled="";
		}
	}else{
		if (!strChnAccessFl.equals("Y")){
			disabled="true";
		} 
		/* strChangePriceFl is the flag for checking past month
		User should be able to edit the shipping charge even if the order is raised on past month*/
		
		/* if ((strChnAccessFl.equals("Y")) && strChangePriceFl.equals("N")){//Change price access
			disabled="true";
		} */
	}
	
}

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Enter Control Numbers </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%= strcustserviceJsPath %>/GmOrderItemModify.js"></script>
<script language="JavaScript" src="<%= strcustserviceJsPath %>/GmOrderItemPricing.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>

<script>
var deptId = '<%=strDeptId%>';
var intSize = '<%=alCart.size()%>';
today = new Date('<%=strTodaysDate%>');
orderdt = '<%=dtOrderDt%>';
var dtFormat = '<%=strApplnDateFmt%>';
var currentdate = '<%=strTodaysDate%>';
var accessFl = '<%=strChnAccessFl%>';
var ruleval = '<%=strChangePriceAccess%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnLoad();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmOrderItemServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hOrderId" value="<%=strOrderId%>">
<input type="hidden" name="hOrderType" value="<%=strOrderType%>">
<input type="hidden" name="Cbo_Id" value="<%=strAccId%>">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hInvFl" value="<%=strInvFl%>">
<input type="hidden" name="hReadFL" value="<%=strAccess%>">
<input type="hidden" name="hOrdId">
	<table border="0" width="900" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
			<td height="25" class="RightDashBoardHeader"><fmtChangePrice:message key="LBL_DELIVERED_ORDER"/> - <%=strHeader%></td>
			<td height="25" class="RightDashBoardHeader">
			<fmtChangePrice:message key="LBL_HELP" var="vaHelp"/>
			<img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${vaHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
			</td>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>
		<tr>
			<td width="900" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
					    <fmtChangePrice:message key="LBL_ORDERID" var="varOrderId"/>
						<td class="RightTableCaption" HEIGHT="20" align="right" width="25%">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varOrderId}:" td="false"/></td>
						<td class="RightText" width="75%">&nbsp;<%=strOrderId%></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr class="Shade">
					    <fmtChangePrice:message key="LBL_ACCOUNTNAME" var="varAcctName"/>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varAcctName}:" td="false"/></td>
						<td class="RightText">&nbsp;<%=strAccName%></td>
					</tr>
					<tr>
					    <fmtChangePrice:message key="LBL_ACCOUNTID" var="varAcctId"/>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varAcctId}:" td="false"/></td>
						<td class="RightText">&nbsp;<%=strAccId%></td><input type="hidden" name="hAccountId" id="hAccountId" value="<%=strAccId%>"></input>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr class="Shade">
					<fmtChangePrice:message key="LBL_ORDERENTEREDBY" var="varOrderEnteredBy"/>
					<fmtChangePrice:message key="LBL_DATE" var="varDate"/>
					<fmtChangePrice:message key="LBL_SURGERY_DATE" var="varSurgDate"/>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varOrderEnteredBy}:" td="false"/></td>
						<td class="RightText">&nbsp;<%=strUserName%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B><gmjsp:label type="RegularText"  SFLblControlName="${varDate}:" td="false"/></B>&nbsp;<%=strOrdredDate%>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><gmjsp:label type="RegularText"  SFLblControlName="${varSurgDate}:" td="false"/></b>&nbsp;<%=strSurgeryDate%></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr>
					    <fmtChangePrice:message key="LBL_SHIPDATE" var="varShipDate"/>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varShipDate}:" td="false"/></td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringFromDate(dtShipDate,strApplnDateFmt)%></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>					
					<tr   class="Shade">
					    <fmtChangePrice:message key="LBL_COMMENTS" var="varComments"/>
						<td class="RightTableCaption" valign="top" align="right" HEIGHT="60"><gmjsp:label type="RegularText"  SFLblControlName="${varComments}:" td="false"/></td>
						<td class="RightText" valign="top" >&nbsp;<%=strComments%></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr >
					    <fmtChangePrice:message key="LBL_CUSTPO" var="varCustPo"/>
						<td align="right" HEIGHT="24" class="RightTableCaption"><gmjsp:label type="RegularText"  SFLblControlName="${varCustPo}:" td="false"/></td>
						<td class="RightText">&nbsp<%=strPO%></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr class="Shade">
					    <fmtChangePrice:message key="LBL_INVOICEID" var="varInvoiceID"/>
						<td align="right" HEIGHT="24" class="RightTableCaption"><gmjsp:label type="RegularText"  SFLblControlName="${varInvoiceID}:" td="false"/></td>
						<td class="RightText">&nbsp;<%=strInvoiceId%>
						<fmtChangePrice:message key="LBL_PRICE_REQ" var="varPriceReq"/>
						<span style='margin-left:33%'  class="RightTableCaption"><gmjsp:label type="RegularText"  SFLblControlName="${varPriceReq}:" td="false"/></span>
						<%	if (alPricReqDtl.size() > 0) { %>
						<fmtChangePrice:message key="LBL_PRICE_REQ_VIEW" var="varPriceReqView"/>
							<span style="cursor:hand;"><a style="color:blue;" class="RightText" align="left" href="javascript:fnCallPrice('<%=strOrderId%>');"> <gmjsp:label type="RegularText"  SFLblControlName="${varPriceReqView}" td="false"/> </a></span>
						<% } %>	
						</td>
					</tr>
<%
					if (strhAction.equals("ReturnPart"))
					{
%>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr class="shadeRightTableCaption">
					    <fmtChangePrice:message key="LBL_RETURNDETAILS" var="varReturnDetails"/>
						<td class="RightText" HEIGHT="24" colspan="2"><gmjsp:label type="RegularText"  SFLblControlName="${varReturnDetails}" td="false"/></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr>
					    <fmtChangePrice:message key="LBL_RETURNREASON" var="varReturnReason"/>
						<td class="RightTableCaption" align="right" HEIGHT="24"><gmjsp:label type="MandatoryText"  SFLblControlName="${varReturnReason}:" td="false"/></td>
						<td>&nbsp;<input type="hidden" name="Cbo_Type" value="3300"><select name="Cbo_Reason" id="Region" class="RightText" tabindex="3" onFocus="changeBgColor(this,'#AACCE8');" 
							onChange ="fnReasonChange()" onBlur="changeBgColor(this,'#ffffff');fnReasonChange()"><option value="0" >[Choose One]
<%
			  		intSize = alReturnReasons.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alReturnReasons.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strReason.equals(strCodeID)?"selected":"";
%>
						<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%			  		
					}
%>					
						</select></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<!-- Struts tag lib code modified for JBOSS migration changes -->
					<tr class="Shade">
					     <fmtChangePrice:message key="LBL_COMMENTS" var="varComments"/>
						<td class="RightTableCaption" valign="top" align="right" HEIGHT="24"><gmjsp:label type="RegularText"  SFLblControlName="${varComments}:" td="false"/></td>
						<td>&nbsp;<textarea name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=7 rows=5 cols=45 value=""></textarea></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr>
					    <fmtChangePrice:message key="LBL_EXPDATERETURN" var="varExpDateReturn"/>
						<td class="RightTableCaption" align="right" HEIGHT="24"><gmjsp:label type="RegularText"  SFLblControlName="${varExpDateReturn}:" td="false"/></td>
						
						<td>
						&nbsp;<gmjsp:calendar textControlName="Txt_ExpDate"   gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
						</td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr class="Shade">
					    <fmtChangePrice:message key="LBL_PARENTORDERID" var="varParentOrderId"/>
						<td class="RightTableCaption" align="right" HEIGHT="24"><gmjsp:label type="RegularText"  SFLblControlName="${varParentOrderId}:" td="false"/></td>
						<td>&nbsp;<input type="text" size="15" name="Txt_ParOrderID" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=23>
						</td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr class="shadeRightTableCaption">
					     <fmtChangePrice:message key="LBL_ASSOCIATEDRETURN" var="varAssReturn"/>
						<td class="RightText" HEIGHT="24" colspan="2"><gmjsp:label type="RegularText"  SFLblControlName="${varAssReturn}" td="false"/></td>
					</tr>
					<tr>
						<td colspan="2">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr><td colspan="7" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td height="25" width="70" align="center"><fmtChangePrice:message key="LBL_RAID" /></td>
									<td width="150"><fmtChangePrice:message key="LBL_ORDERID" /></td>
									<td width="80" align="center"><fmtChangePrice:message key="LBL_INITIATEDBY" /></td>
									<td width="80" align="center"><fmtChangePrice:message key="LBL_INITIATEDDATE" /></td>
									<td width="80" align="center"><fmtChangePrice:message key="LBL_EXPECTEDDATE" /></td>
									<td width="80" align="center"><fmtChangePrice:message key="LBL_CREDILBLATE" /></td>
									<td width="50" align="center"><fmtChangePrice:message key="LBL_STATUS" /></td>
								</tr>
								<tr><td colspan="7" height="1" class="LLine"></td></tr>
<%
						intSize = alReturns.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alReturns.get(i);
								
								strRAId = GmCommonClass.parseNull((String)hmLoop.get("RAID"));
								strReason = GmCommonClass.parseNull((String)hmLoop.get("REASON"));
								strCreatedBy = GmCommonClass.parseZero((String)hmLoop.get("CUSER"));
								strCreatedDt = GmCommonClass.getStringFromDate((java.sql.Date)hmLoop.get("CDATE"),strApplnDateFmt);
								strExpDt = GmCommonClass.getStringFromDate((java.sql.Date)hmLoop.get("EDATE"),strApplnDateFmt);
								strCreditDt = GmCommonClass.getStringFromDate((java.sql.Date)hmLoop.get("CRDATE"),strApplnDateFmt);
								strStatus = GmCommonClass.parseNull((String)hmLoop.get("SFL"));
%>
								<tr>
									<td class="RightText" height="20">&nbsp;<a class=RightText href=javascript:fnViewReturns('<%=strRAId%>');><%=strRAId%></a></td>
									<td class="RightText"><%=strReason%></td>
									<td class="RightText">&nbsp;<%=strCreatedBy%></td>
									<td class="RightText" align="center">&nbsp;<%=strCreatedDt%></td>
									<td class="RightText" align="center">&nbsp;<%=strExpDt%></td>
									<td class="RightText" align="center">&nbsp;<%=strCreditDt%></td>
									<td class="RightText" align="center">&nbsp;<%=strStatus%></td>
								</tr>
								<tr><td colspan="7" height="1" bgcolor="#eeeeee"></td></tr>
<%
							}//End of FOR Loop
						}else {
%>
						
						<tr><td colspan="7" height="30" valign="absmiddle" align="center" class="RightTextRed"><BR><fmtChangePrice:message key="LBL_RAINITIATEDORDER" /></td></tr>
<%
						}
%>
							</table>
							<input type="hidden" name="hReturnCnt" value="<%=intSize%>"> 
						</td>
					</tr>					
<%
					}// End of IF ReturnPart
	/********************************************
	 * Part Details Information 
	 ********************************************/
					if (strhAction.equals("EditPrice") || strhAction.equals("ReturnPart"))
					{
%>
					<tr>
						<td colspan="3" width="898">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr><td colspan="11" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td width="60" height="25">&nbsp;<fmtChangePrice:message key="LBL_PART" /></td>
									<td width="400"><fmtChangePrice:message key="LBL_PARLBLESC" /></td>
									<td width="60" align="center"><fmtChangePrice:message key="LBL_ORDER" /><br><fmtChangePrice:message key="LBL_QTY" /></td>
									<td width="60"><fmtChangePrice:message key="LBL_TYPE" /></td>
									<td width="90" align="center"><fmtChangePrice:message key="LBL_CONTROLNUMBER" /></td>
<%					if (strhAction.equals("EditPrice"))
					{
%>							<td width="100" align="center"><fmtChangePrice:message key="LBL_CURRENT" /><br><fmtChangePrice:message key="LBL_PORTALPRICE" /></td>
							<td width="100" align="center"><fmtChangePrice:message key="LBL_DOUNIT" /><br><fmtChangePrice:message key="LBL_PRICE" /></td>	
							<td width="100" align="center"><fmtChangePrice:message key="LBL_UNIT" /><br><fmtChangePrice:message key="LBL_PRICEADJ" /></td>
							<td width="100" align="center"><fmtChangePrice:message key="LBL_ADJ" /><br><fmtChangePrice:message key="LBL_CODE" /></td>			
<%					}
%>														
									<td width="90" align="center"><fmtChangePrice:message key="LBL_NET" /><br><fmtChangePrice:message key="LBL_UNITPRICE" /></td>
									<td width="100" align="center"><fmtChangePrice:message key="LBL_TOTALPRICE" /></td>
<%					if (strhAction.equals("ReturnPart"))
					{
%>							<td align="center" width="100" ><fmtChangePrice:message key="LBL_RETURNED" /><br><fmtChangePrice:message key="LBL_QTY" /></td>	
							<td align="center" width="100"><fmtChangePrice:message key="LBL_QTYTORETURN" /></td>
<%					}
%>								
					</tr>
								<TR><TD colspan=11 height=1 class=Line></TD></TR>
<%
						intSize = alCart.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();
							intCount = 0;
							
							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alCart.get(i);
								if (i<intSize-1)
								{
									hmTempLoop = (HashMap)alCart.get(i+1);
									strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("ID"));
								}
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("ID"));
								strPartNumHidden = strPartNum;
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
								strItemId = GmCommonClass.parseNull((String)hmLoop.get("ITEMID"));
								strReturnQty = GmCommonClass.parseZero((String)hmLoop.get("RTN_ITEM_QTY"));
								strItemType = GmCommonClass.parseZero((String)hmLoop.get("ITEMTYPE"));
								strItemTypeDesc	= GmCommonClass.parseNull((String)hmLoop.get("ITEMTYPEDESC"));
								strPortalPrice = GmCommonClass.parseNull((String)hmLoop.get("CURR_PRICE"));
								strUnitPrice = GmCommonClass.parseNull((String)(String)hmLoop.get("UNITPRICE"));
								strUnitPriceAdj = GmCommonClass.parseNull((String) (String)hmLoop.get("UNITPRICEADJ"));
								strAdjCode = GmCommonClass.parseNull((String)(String)hmLoop.get("UNITPRICEADJCODEID"));
								strNetUnitPrice = GmCommonClass.parseNull((String)(String)hmLoop.get("NETUNITPRICE"));
								strCurrPortlaPrice = GmCommonClass.parseNull((String)(String)hmLoop.get("CURRPORTPRICE"));
								strAdjCodeValue = GmCommonClass.parseNull((String)(String)hmLoop.get("CODEADJUST"));
								strAccountPrice = GmCommonClass.parseNull((String)(String)hmLoop.get("ACCOUNTPRICE"));
								
								strTemp = strPartNum;
								
								if (strPortalPrice.equals("")){ 
									strEditFL ="";
									
								}
								
								if (strPartNum.equals(strNextPartNum))
								{
									intCount++;
									strLine = "<TR><TD colspan=10 height=1 bgcolor=#eeeeee></TD></TR>";
								}

								else
								{
									strLine = "<TR><TD colspan=10 height=1 bgcolor=#eeeeee></TD></TR>";
									if (intCount > 0)
									{
										strPartNum = "";
										strPartDesc = "";
										//strColor = "bgcolor=white";
										strLine = "";
									}
									else
									{
										//strColor = "";
									}
									intCount = 0;
								}

								if (intCount > 1)
								{
									strPartNum = "";
									strPartDesc = "";
									//strColor = "bgcolor=white";
									strLine = "";
								}
								if(strNetUnitPrice.equals("0")){
									strNetUnitPrice = strUnitPrice;
								}
								strLblAdjCode = "Cbo_AdjCode" + i;
							
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
								strItemQty = GmCommonClass.parseZero((String)hmLoop.get("QTY"));
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));

								intQty = Double.parseDouble(strItemQty);
								intReturnItemQTY = Integer.parseInt(strReturnQty);
													
								dbItemTotal = Double.parseDouble(strNetUnitPrice);
								dbItemTotal = (intQty - intReturnItemQTY) * dbItemTotal; // Multiply by Qty
								strItemTotal = ""+dbItemTotal;
								dbTotal = dbTotal + dbItemTotal;
								
								dbAdjTotal = Double.parseDouble(strUnitPrice);
								dbAdjTotal = (intQty - intReturnItemQTY) * dbAdjTotal; // Multiply by Qty								
								dbAdjstTotal = dbAdjstTotal + dbAdjTotal;
								
								// Calculating the Total Price Before Adjustment
								dbBeforeItemTotal = Double.parseDouble(strUnitPrice);
								dbBeforeItemTotal = intQty * dbBeforeItemTotal; // Multiply by Qty
								strBeforeItemTotal = ""+dbBeforeItemTotal;
								dbBeforePriceTotal = dbBeforePriceTotal + dbBeforeItemTotal;
								strBeforeTotal = "" + 	dbBeforePriceTotal;	
								
								// Calculating the Total Price After Adjustment
								dbAfterItemTotal = Double.parseDouble(strPrice);
								dbAfterItemTotal = intQty * dbAfterItemTotal; // Multiply by Qty
								strAfterItemTotal = ""+dbAfterItemTotal;
								dbAfterPriceTotal = dbAfterPriceTotal + dbAfterItemTotal;
								strAfterPrice = "" + 	dbAfterPriceTotal;
								
								// Caliculating the Adjustment Amount
								dbBeforePrice = Double.parseDouble(strBeforeTotal);
								dbAfterPrice = Double.parseDouble(strAfterPrice);						
								dbAdjustPrice = dbAfterPrice - dbBeforePrice;						
								strAdjustAmount = ""+dbAdjustPrice;
								
								out.print(strLine);
%>
								<tr >
									<td class="RightText" height="20">&nbsp;<%=strPartNum%>
										<input type="hidden" name="hItemId<%=i%>" value="<%=strItemId%>">
										<input type="hidden" name="hQty<%=i%>" value="<%=strItemQty%>">
										<input type="hidden" name="hPNum<%=i%>" value="<%=strPartNumHidden%>">
										<input type="hidden" name="hContNum<%=i%>" value="<%=strControl%>">
										<input type="hidden" name="hItemType<%=i%>" value="<%=strItemType%>">
										<input type="hidden" name="hReturnedQty<%=i%>" value="<%=strReturnQty%>">
										<input type="hidden" name="hDOPrice<%=i%>" value="<%=strNetUnitPrice%>">
										<input type="hidden" name="hUnitPrice<%=i%>" value="<%=strUnitPrice%>">
										<input type="hidden" name="hUnitPriceAdj<%=i%>" value="<%=strUnitPriceAdj%>">
									</td>
									<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td class="RightText" align="center">&nbsp;<%=strItemQty%></td>
									<td class="RightText" align="center">&nbsp;<%=strItemTypeDesc%></td>
									<td class="RightText" align="center">&nbsp;<%=strControl%></td>
									
<%					if (strhAction.equals("EditPrice"))
					{
%>									<gmjsp:currency type="CurrTextSign"  textValue="<%=strCurrPortlaPrice%>" currSymbol="<%=strAccCurrSymb%>"/>
									<gmjsp:currency type="CurrTextSign"  textValue="<%=strUnitPrice%>" currSymbol="<%=strCurrSign%>"/>

									<gmjsp:currency type="CurrTextSign"  textValue="<%=strUnitPriceAdj%>" currSymbol="<%=strCurrSign%>"/>
									<% String strfunctionn = "javascript:fnGetNetUnitPrice('"+i+"',this,'"+strPartNum+"','"+strCurrPortlaPrice+"','"+strUnitPriceAdj+"','"+strItemId+"');"; %>
									<td>&nbsp;<gmjsp:dropdown controlId="<%=strLblAdjCode%>" controlName="<%=strLblAdjCode%>" seletedValue="<%= strAdjCode %>"
										tabIndex="30"  width="75" value="<%= alAdjCodeList%>" codeId = "CODEID"  codeName = "CODENM" onChange="<%=strfunctionn%>"  defaultValue= "N/A" /></td>		
									
									<td class="RightText" align="center" ><input type="text" size="8" value="<%=GmCommonClass.getStringWithCommas(strNetUnitPrice)%>" id="hPrice<%=i%>" name="hPrice<%=i%>" <%=strPriceReadOnly%> class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1></td>
<%					}else{
%>									<gmjsp:currency type="CurrTextSign"  textValue="<%=strNetUnitPrice%>" currSymbol="<%=strCurrSign%>"/>
<%					}
%>									<gmjsp:currency type="CurrTextSign"  textValue="<%=strItemTotal%>" currSymbol="<%=strCurrSign%>"/>
<%					if (strhAction.equals("ReturnPart"))
					{
%>									<td class="RightText" align="center" ><%=strReturnQty%></td> 
									<td class="RightText" align="center"><input type="text" size="3" value="" name="hRetQty<%=i%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1></td>
<%					}
%>								</tr>
<%
							}//End of FOR Loop
							if (!strhAction.equals("EditControl"))
							{
								strShipCost = GmCommonClass.parseZero(strShipCost);
								dblShip = Double.parseDouble(strShipCost);
								dbBeforeTotal = Double.parseDouble(strBeforeTotal);
								dbTotal = dbBeforeTotal ;
								strBeforeTotal = ""+dbTotal;
								
								dbAfterTotal = Double.parseDouble(strAfterPrice);
								dblShip = Double.parseDouble(strShipCost);
								dbAdjstTotal = dbAfterTotal + dblShip;
								strAfterTotal = ""+dbAdjstTotal;								
								
%>								<tr><td colspan="11" height="1" bgcolor="#eeeeee"></td></tr>
								<tr>
									<td>&nbsp;</td>
									<fmtChangePrice:message key="LBL_SHIPPINGCHARGES" var="varShippingCharges"/>
									<td  height="20" colspan="9" class="RightText">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varShippingCharges}" td="false"/></td>
									<%if(strEdtShipChrgFl.equalsIgnoreCase("Y")){ %>
										<td class="RightText" align="right"><input type="text" size="4" <%=strShipReadOnly%> value="<%=GmCommonClass.getStringWithCommas(strShipCost)%>" 
										name="Txt_SCost" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
										onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;</td>
									<%}else{ %>
										<td class="RightText" align="right"><gmjsp:currency type="CurrTextSign"  textValue="<%=strShipCost%>" td="false" currSymbol="<%=strCurrSign%>"/>
										<input type="hidden" name="Txt_SCost" value="<%=GmCommonClass.getStringWithCommas(strShipCost)%>">
										</td>
									<%} %>
								</tr>
								<tr><td colspan="11" height="1" bgcolor="#eeeeee"></td></tr>
								<tr>
								    <fmtChangePrice:message key="LBL_TOTALBEFOREADJ" var="varTotalBefAdj"/>
									<td colspan="10" height="30" align="right" class="RightTableCaption"><gmjsp:label type="RegularText"  SFLblControlName="${varTotalBefAdj}:" td="false"/>&nbsp;&nbsp;</td>
									<gmjsp:currency type="CurrTextSign"  textValue="<%=strBeforeTotal%>" currSymbol="<%=strCurrSign%>"/>
									<input type="hidden" name="hTotal" value="<%=strBeforeTotal%>">
								</tr>
								<tr>
								    <fmtChangePrice:message key="LBL_ADJUSTMENTS" var="varAdjustments"/>
									<td colspan="10" height="30" align="right" class="RightTableCaption"><gmjsp:label type="RegularText"  SFLblControlName="${varAdjustments}:" td="false"/>&nbsp;&nbsp;</td>
									<gmjsp:currency type="CurrTextSign"  textValue="<%=strAdjustAmount%>" currSymbol="<%=strCurrSign%>"/>
									<input type="hidden" name="hTotalAdj" value="<%=strAdjustAmount%>">
								</tr>
								<tr>
								    <fmtChangePrice:message key="LBL_TOTALAFTERADJ" var="varTotalAfterAdj"/>
									<td colspan="10" height="30" align="right" class="RightTableCaption"><gmjsp:label type="RegularText"  SFLblControlName="${varTotalAfterAdj}:" td="false"/>&nbsp;&nbsp;</td>
									<gmjsp:currency type="CurrTextSign"  textValue="<%=strAfterTotal%>" currSymbol="<%=strCurrSign%>"/>
									<input type="hidden" name="hAdjTotal" value="<%=strAfterTotal%>">
								</tr>
<%
							}
						}else {
%>
						<fmtChangePrice:message key="LBL_PARTNUMBERINORDER" var="varPartNumberInOrder"/>
						<tr><td colspan="11" height="50" align="center" class="RightTextRed"><gmjsp:label type="RegularText"  SFLblControlName="<BR>${varPartNumberInOrder}" td="false"/></td></tr>
<%
						}	
%>
							</table>
							<input type="hidden" name="hCnt" value="<%=intSize%>">
						</td>
					</tr>
<%
					}
%>		
				<tr>
					<td colspan="2"> 
						<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="FORMNAME" value="" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="Edit" />
							<jsp:param name="Mandatory" value="yes" />
						</jsp:include>
					</td>
				</tr>
				<tr>
					<td align="center" height="30" id="button" colspan="2">
					    <fmtChangePrice:message key="BTN_SUBMIT" var="varSubmit"/>
						<gmjsp:button value="&nbsp;${varSubmit}&nbsp;"  controlId="Btn_Submit" name="Btn_Submit" disabled="<%=disabled%>" gmClass="button" onClick="javascript:fnSubmit();" tabindex="100" buttonType="Save" />&nbsp;&nbsp;
						<% if(strEdtShipChrgFl.equalsIgnoreCase("Y")){%>
						<fmtChangePrice:message key="BTN_UPDATE_SHIPCOST" var="varShipCost"/>
						<gmjsp:button value="&nbsp;${varShipCost}&nbsp;"  controlId="Btn_Submit" name="Btn_Submit" gmClass="button" onClick="javascript:fnUpdateShipCost();" disabled="<%=disabled%>" tabindex="100" buttonType="Save" />&nbsp;&nbsp;
						<%}%>
					</td>
				</tr>
				<% if  (disabled.equals("true") ){%>
				<tr>
					<td  align="center" colspan="2">
					    <fmtChangePrice:message key="LBL_EDITING" var="varEditing"/>
						<font color="red"><B><gmjsp:label type="RegularText"  SFLblControlName="${varEditing}" td="false"/></B></font>
					</td>
				</tr>
				<%} %>	
				
				<% if  (strEditFL.equals("") ){%>
				<tr>
					<td  align="center" colspan="2">
					    <fmtChangePrice:message key="LBL_PRICEUPLOAD" />
						<font color="red"><B> </B></font><br><br>
					</td>
				</tr>
				<%} %>				
				</table>
			</td>
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
    </table>

	

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
