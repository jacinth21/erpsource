
<%
/**********************************************************************************
 * File		 		: GmOrderQuotePrint/jsp
 * Desc		 		: This screen is used for the Quotation Print Version
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<!-- \custservice\GmOrderQuotePrint.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strAction=null;
	String strSource="";
	String strApplnDateFmt = strGCompDateFmt;
	
	strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
		   strAction = (strAction.trim() == null) ? "PrintPack" : strAction;
		   strAction = (strAction.equals("")) ? "PrintPack" : strAction;

	strSource = GmCommonClass.parseNull((String)request.getAttribute("SOURCE"));
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmOrderDetails = new HashMap();
	HashMap hmShipDetails = new HashMap();

	ArrayList alCartDetails = new ArrayList();
	ArrayList alQuoteDetails = new ArrayList();
	
	String strShade = "";
	String strProjectType = "";

	String strAccNm = "";
	String strDistRepNm = "";
	String strBillAdd = "";
	java.sql.Date dtOrdDate = null;
	String strShipAdd = "";
	String strPO = "";
	String strOrdId = "";
	String strEmptyValue = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	String strPartNum = "";
	String strDesc = "";
	String strPrice = "";
	String strQty = "";
	String strItemOrdId = "";
	String strPartNums = "";
	String strControlNum = "";
	String strShipDate = "";
	String strShipCarr = "";
	String strShipMode = "";
	String strTrack = "";
	String strOrderType = "";
	String strParentId = "";
	String strItemOrdType = "";
	String strTotalPrice = "";
	String strCreatedBy = "";
	String strPerson = "";
	String strPaymentterm="";
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	GmResourceBundleBean gmPaperworkResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
	String straddress = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYADDRESS"));
	String strGMName = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYNAME"));
	straddress = "&nbsp;" + straddress.replaceAll("/", "\n<br>&nbsp;");
	String strHeadAddress = "<br>&nbsp;&nbsp;<b>" + strGMName + "</b><BR>&nbsp;" + straddress; 
	String strPhone =  GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYPHONE"));
	String strFax =  GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYFAX"));
	String strCompanyMail = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYEMAIL"));
	String strIsPhone =  GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("ISGMCOMPANYPHONE"));//To check whether phone number needs to add seperately with address of not
	String strShowFaxDtls = GmCommonClass.parseNull((String)gmPaperworkResourceBundleBean.getProperty("QUOTATION.SHOW_FAX"));
	String strShowYourRefId = GmCommonClass.parseNull((String)gmPaperworkResourceBundleBean.getProperty("QUOTATION.SHOW_REF_ID"));
	String strShowBankDtls = GmCommonClass.parseNull((String)gmPaperworkResourceBundleBean.getProperty("QUOTATION.SHOW_BANK_DTLS"));
	String strCompanyVat = GmCommonClass.parseZero((String)gmPaperworkResourceBundleBean.getProperty("QUOTATION.VATRATE"));
	String strShowVenPatId = GmCommonClass.parseNull((String)gmPaperworkResourceBundleBean.getProperty("QUOTATION.SHOW_VENDOR_PATIENT_ID"));
	String strTotalAmtlbl = GmCommonClass.parseNull((String)gmPaperworkResourceBundleBean.getProperty("QUOTATION.TOTAL_AMOUNT"));
	    
	String strInvTotal = "";
	String strVatAmt = "";
	String strGrandTotal = "";
	String strPartCode = "";
	String strCreatedDt = "";
	
	int intTempSize = 0;
	int j = 0;
	String strTempCodeID = "";
	String strTempCodeNm = "";
	String strPatHospId = "";
	String strRefPO = "";
	String strVendorId = "";
	String strVatExclsn="";
	String strAccTaxValue = "";
	String strAccCurrency = "";
	
	if(strIsPhone.equals("YES")){
		strHeadAddress = strHeadAddress+"<br>&nbsp;&nbsp;"+"Phone no:"+strPhone+"&nbsp;&nbsp;&nbsp;&nbsp;";
	}
	if(strShowFaxDtls.equals("YES")){
	  strHeadAddress = strHeadAddress+"<br>&nbsp;&nbsp;"+"Fax:"+strFax+"&nbsp;&nbsp;&nbsp;&nbsp;";
	  strHeadAddress = strHeadAddress+"<br>&nbsp;&nbsp;"+"Email:"+strCompanyMail+"&nbsp;&nbsp;&nbsp;&nbsp;";
	}
	boolean blBackFl = false;
	
	if (hmReturn != null)
	{
		hmOrderDetails 		= (HashMap)hmReturn.get("ORDERDETAILS");
		alCartDetails 		= (ArrayList)hmReturn.get("CARTDETAILS");
		alQuoteDetails	= (ArrayList)hmReturn.get("QUOTEDETAILS");
		hmShipDetails 		= (HashMap)hmReturn.get("SHIPDETAILS");
		
		//To Show the Your ref with the Patient Hospital# on Quote for South Africa.
		intTempSize = alQuoteDetails.size();
		HashMap hmTemp = new HashMap();
		for (j = 0; j < intTempSize; j++) 
		{			
			hmTemp = (HashMap) alQuoteDetails.get(j);
			strTempCodeID = (String) hmTemp.get("TRTYPEID");
			strTempCodeNm = (String) hmTemp.get("ATTRVAL");
			
			if(strTempCodeID.equals("106646")) 
			{
			  strPatHospId = strTempCodeNm;
			}
			
		}

		strRefPO = (String)hmOrderDetails.get("PO");

		if(strShowVenPatId.equals("YES") && (!strPatHospId.equals(""))){
		  strPO = strRefPO + "-" + strPatHospId;
		}else{
		  strPO = strRefPO;
		}
		
		strOrdId = (String)hmOrderDetails.get("ID");
		strShipAdd = (String)hmOrderDetails.get("SHIPADD_OLD");
		strBillAdd = (String)hmOrderDetails.get("BILLADD");
		strAccNm = (String)hmOrderDetails.get("ANAME");
		strDistRepNm = (String)hmOrderDetails.get("REPDISTNM");
		//dtOrdDate = (java.sql.Date)hmOrderDetails.get("ODT");
		strCreatedDt = GmCommonClass.getStringFromDate((java.sql.Date)hmOrderDetails.get("ODT"),strApplnDateFmt);
		strOrderType  = GmCommonClass.parseNull((String)hmOrderDetails.get("ORDERTYPE"));
		strParentId	= GmCommonClass.parseNull((String)hmOrderDetails.get("PARENTORDID"));
		strShipCarr	= GmCommonClass.parseNull((String)hmOrderDetails.get("CARRIER"));
		strShipMode	= GmCommonClass.parseNull((String)hmOrderDetails.get("SHIPMODE"));
		strTrack	= GmCommonClass.parseNull((String)hmOrderDetails.get("TRACKNO"));
		strCreatedBy = GmCommonClass.parseNull((String)hmOrderDetails.get("UNAME"));
		strPerson = GmCommonClass.parseNull((String)hmOrderDetails.get("RFROM"));
		strPaymentterm= GmCommonClass.parseNull((String)hmOrderDetails.get("PMTTERM"));
		strVendorId = GmCommonClass.parseNull((String) hmOrderDetails.get("VENDORID"));
		strAccTaxValue = GmCommonClass.parseNull((String) hmOrderDetails.get("ACCVATTAX"));
		strAccCurrency = GmCommonClass.parseNull((String) hmOrderDetails.get("ACC_CURRN_NM"));
		strTotalAmtlbl = (!strAccCurrency.equals(""))?strTotalAmtlbl+"("+strAccCurrency+")":strTotalAmtlbl;
		//to show account VAT tax value for Australia
		strCompanyVat = !strAccTaxValue.equals("")?strAccTaxValue:strCompanyVat;
		strVatExclsn = GmCommonClass.parseNull((String) hmOrderDetails.get("VATEXCL"));
		if(strVatExclsn.equals("Y")){
		  strCompanyVat="0";
		}
		    
	}

	if (hmShipDetails != null)
	{
		strShipDate = (String)hmShipDetails.get("SDT");
	}

	double dbTotal = 0.0;
	double dbOrdTotal = 0.0;
	double dbVatAmt = 0.0;
	double dbGrandTotal = 0.0;
	
	// Combo box variable
	int intSize = 0;
	int i = 0;
	HashMap hcboVal = null;
	ArrayList alComboList 	= new ArrayList();
	
	String strSelected  = "";
	String strCID = "";
	String strCodeID = "";
	String strCodeNm = "";
	request.setAttribute("CompanyLocale", "en_"+strCompanyLocale);
	if(!strShowYourRefId.equals("YES")){
	  strPO = "";
	}
%>
<HTML>
<HEAD>
<TITLE> GlobusOne Enterprise Portal: Quotation Print </TITLE>

<script language="JavaScript" src="<%=strJsPath%>/GmPackSlip.js"></script>
<script>
var source = "<%=strSource%>";
var tdinnner = "";
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnHidebuttons()" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<xml src="<%=strXmlPath%>/custservice/GmQuotation.xml" id="xmldso" async="false"></xml>
<FORM name="frmOrder" method="post">
<input type="hidden" name="hMode" value="PrintPack">
<input type="hidden" name="hOrdId" value="<%=strOrdId%>">
<fmt:setLocale value="${requestScope.CompanyLocale}"/>
<fmt:setBundle basename="properties.Paperwork"/>
	<table border="0" class="DtRuleTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td align="right" height="30" id="button">
				<img style="cursor:hand" src='<%=strImagePath%>/print-icon.png' height="25" width="25" alt="Click here to print page" onClick="fnPrint();" />
				<img style="cursor:hand" src='<%=strImagePath%>/close_icon.png' height="20" width="20" alt="Click here to close window" onClick="window.close();" />
			</td>
		<tr>
	</table>
	
	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr>
				 <td bgcolor="#666666" height="1" colspan="6"></td>
			</tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td height="80" width="170"><img src="<%=strImagePath%>/s_logo.gif" width="138" height="60"></td>
						<td class="RightText" width="290"><%=strHeadAddress%><td>
						<td align="right" class="RightText"><font size="+3">Quotation</font>&nbsp;</td>
					</tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
		</tr>
			<tr>
				 <td bgcolor="#666666" height="1" colspan="6"></td>
			</tr>
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr class="RightTableCaption" bgcolor="#eeeeee">
									<td height="24" align="center" width="250">&nbsp;Bill To</td>
									<td bgcolor="#666666" width="1" rowspan="11"></td>
									<td width="250" align="center">&nbsp;Ship To</td>
									<td bgcolor="#666666" width="1" rowspan="11"></td>
									<td width="100" align="center">&nbsp;Date</td>
									<td width="100" align="center">&nbsp;Quote #</td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="6"></td>
								</tr>
								<tr>
									<td class="RightText" rowspan="5" valign="top">&nbsp;<%=strBillAdd%></td>
									<td rowspan="5" class="RightText" valign="top">&nbsp;<%=strShipAdd%></td>
									<td height="25" class="RightText" align="center">&nbsp;<%=strCreatedDt%></td>
									<td class="RightTableCaption" align="center">&nbsp;<%=strOrdId%></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
								</tr>
								<% if(strShowVenPatId.equals("YES")) {%>
								
								<tr bgcolor="#eeeeee" class="RightTableCaption">
									<td align="center" colspan="2">&nbsp;Vendor #</td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
								</tr>
								<tr>
									<td align="center" colspan="2 height="25">&nbsp;<%=strVendorId%></td>
								</tr>
								
								<%} else {%>
								<tr bgcolor="#eeeeee" class="RightTableCaption">
									<td align="center"></td>
									<td align="center">&nbsp;</td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
								</tr>
								<tr>
									<td align="center" class="RightText">&nbsp;<Br></td>
									<td align="center" height="25" class="RightText">&nbsp;</td>
								</tr>
								<%}%>
								
								<tr>
									 <td bgcolor="#666666" height="1" colspan="6"></td>
								</tr>
								<tr class="RightTableCaption" bgcolor="#eeeeee">
									<td height="24" align="center"><fmt:message key="QUOTATION.YOUR_REF"/></td>
									<td align="center">Requested By</td>
									<td align="center" colspan="2">CS Contact</td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="6"></td>
								</tr>
								<tr>
									<td height="23" align="center" class="RightText"><%=strPO %></td>
									<td align="center" class="RightText"><%=strPerson %></td>
									<td align="center" colspan="2" class="RightText"><%=strCreatedBy %></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr>
						<td colspan="2">
							<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0">
								<tr>
<%
			intSize = alQuoteDetails.size();
			hcboVal = new HashMap();
				for (i = 0; i < intSize; i++) 
				{			
					hcboVal = (HashMap) alQuoteDetails.get(i);
					strCID = (String) hcboVal.get("TRTYPEID");
					strCodeID = (String) hcboVal.get("TRTYPE");
					strCodeNm = (String) hcboVal.get("ATTRVAL");
					//400108 - Patient Name, Appending Patient Hospital # for SA.
					if(strShowVenPatId.equals("YES") && strCID.equals("400108") && (!strPatHospId.equals(""))){
					  
					  strCodeNm = strCodeNm + "-" + strPatHospId;
					  
					} 
%>							
									<td class="RightTableCaption" HEIGHT="24"  align="left"><%=strCodeID.replaceAll(" ", "&nbsp;") %></td>
									<td class="RightText" HEIGHT="24" >&nbsp;:</td>
									<td class="RightText" HEIGHT="24" >&nbsp;<%=strCodeNm.equals("")?strEmptyValue:strCodeNm%>&nbsp;</td>
<%
					if (i%3 == 2)
					{
%>									
								</tr>
								<tr><td bgcolor="#CCCCCC" height="1" colspan="9"></td></tr>
								<tr>
<%
					}
				}
%>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr>
						<td align="center" colspan="2" valign="top" height="100">
							<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0">
								<tr class="RightTableCaption" bgcolor="#eeeeee">
									<td  width="60" height="24">&nbsp;Part Number</td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td  width="300">&nbsp;Description</td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="center" width="100"><fmt:message key="QUOTATION.LOTCODE"/></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="center" width="40">Qty</td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="center" width="40">UM</td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="center" width="100">Unit Price</td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="center" width="100">Total Price</td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="center" width="40"><fmt:message key="QUOTATION.VATNAME"/></td>									
								</tr>
								<tr><td colspan="15" height="1" bgcolor="#666666"></td></tr>
<%
			  	if (!strParentId.equals(strOrdId) || !strTrack.equals(""))
			  	{
			  		blBackFl = true;
			  		intSize = alCartDetails.size();
					hcboVal = new HashMap();
					String strItems = "";

			  		for (i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alCartDetails.get(i);
			  			strPartNum = (String)hcboVal.get("ID");
						strDesc = (String)hcboVal.get("PDESC");
						strQty = (String)hcboVal.get("QTY");
						strPrice = (String)hcboVal.get("PRICE");
						strTotalPrice = (String)hcboVal.get("TOTAL_VALUE");
						strItemOrdType = GmCommonClass.parseNull((String)hcboVal.get("ITEMTYPE"));
						strPartCode = GmCommonClass.parseNull((String)hcboVal.get("PCODE"));
						
						dbTotal = Double.parseDouble(strTotalPrice);
						dbOrdTotal = dbOrdTotal + dbTotal;
						strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
						if (strItemOrdType.equals("50300"))
						{						
%>
								<tr>
									<td class="RightText" height="20">&nbsp;<%=strPartNum%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="center" class="RightText"><%=strPartCode %></td>									
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText" align="center">&nbsp;<%=strQty%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText" align="center">&nbsp;EA</td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="right" class="RightText"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strPrice,2))%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="right" class="RightText"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strTotalPrice,2))%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="center" class="RightText">&nbsp;<%=strCompanyVat%> %</td>
									<td width="1" bgcolor="#eeeeee"></td>
								</tr>
								<tr><td colspan="15" height="1" bgcolor="#eeeeee"></td></tr>								
<%
						} // end of IF for parts from Sales Consignments
					}
			  		
			  		strInvTotal = ""+dbOrdTotal;
				    double vat = Double.parseDouble(strCompanyVat)/100;
			  		dbVatAmt = dbOrdTotal * vat;
			  		strVatAmt = ""+dbVatAmt;
			  		strGrandTotal = ""+(dbOrdTotal+dbVatAmt);
				}
%>						
							</table>
						</td>
					</tr>
					<tr><td colspan="2" height="100%">&nbsp;</td></tr>
					<tr>
						<td>
							<table border="0" width="60%" height="100%" cellspacing="0" cellpadding="0">
								<tr class="RightTableCaption" bgcolor="#eeeeee">
									<td  width="60" height="24" align="center">Subtotal</td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td  width="60" height="24" align="center"><fmt:message key="QUOTATION.VAT_PER"/></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td  width="60" height="24" align="center"><fmt:message key="QUOTATION.VAT_AMOUNT"/></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td  width="100" height="24" align="center"><%=strTotalAmtlbl%></td>
								</tr>
								<tr><td colspan="7" height="1" bgcolor="#eeeeee"></td></tr>
								<tr>
									<td height="24" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strInvTotal,2))%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td height="24" align="center"><%=strCompanyVat%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td height="24" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strVatAmt,2))%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td height="24" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strGrandTotal,2))%></td>
								</tr>
								<tr><td colspan="7" height="1" bgcolor="#eeeeee"></td></tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" height="50">&nbsp;</td></tr>
					<%if(strShowBankDtls.equalsIgnoreCase("YES") ){ %>
					<tr>
						<td>
							<table border="0" width="80%" height="100%" cellspacing="0" cellpadding="0">
								<tr class="RightTableCaption" bgcolor="#eeeeee">
									<td width="200" height="24">Bank Details</td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td width="120" height="24" align="center">Payment Terms</td>
								</tr>
								<tr><td colspan="7" height="1" bgcolor="#eeeeee"></td></tr>
								<tr>
									<td height="24">NEDBANK Corporate<br>
									Branch Name: Business Central<br>
									Branch Clearing Code: 128405 (Deposits)<br>
									Branch Clearing Code: 198765 (Electronic Banking)<br>
									Current Account Number: 1284148262
									</td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td height="24" align="center"><%=strPaymentterm%></td>
								</tr>
								<tr><td colspan="7" height="1" bgcolor="#eeeeee"></td></tr>
							</table>
						</td>
					</tr>
					<%}else{ %>
					<tr>
						<td>
							<table border="0" width="60%" height="100%" cellspacing="0" cellpadding="0">
								<tr class="RightTableCaption" bgcolor="#eeeeee">
									<td width="120" height="24" align="center">Payment Terms</td>
								</tr>
								<tr><td colspan="7" height="1" bgcolor="#eeeeee"></td></tr>
								<tr>
									<td height="24" align="center"><%=strPaymentterm%></td>
								</tr>
								<tr><td colspan="7" height="1" bgcolor="#eeeeee"></td></tr>
							</table>
						</td>
					</tr>
					<%} %>
					<tr><td colspan="2" height="20">&nbsp;</td></tr>
					<tr>
						<td colspan="2">
						<b>Customer Message:</b><br>
						<fmt:message key="QUOTATION.CUST_MSG"/>
						</td>
					</tr>
					<tr><td colspan="2" height="20">&nbsp;</td></tr>
					<tr>
						<td height="50" valign="baseline">
							<b><fmt:message key="QUOTATION.FOOTER_MSG1"/></b><br>
							<b><fmt:message key="QUOTATION.FOOTER_MSG2"/></b><br>
						</td>
						<td height="50" valign="baseline"><fmt:message key="QUOTATION.FOOTER_TITLE"/><br>
							<b><fmt:message key="QUOTATION.FOOTER_MSG3"/></b><br>
							<b><fmt:message key="QUOTATION.FOOTER_MSG4"/></b><br>
						</td>						
					</tr>					
				</table>

			</td>
		</tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
</FORM>


<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
