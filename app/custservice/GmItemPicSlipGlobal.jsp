 <%
/**********************************************************************************
 * File		 		: GmItemPicSlipGlobal.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Xun
 * ------------------------------------------------
 
************************************************************************************/
%>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- Imports for Logger -->
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmtItemPicSlipGlobal" uri="http://java.sun.com/jstl/fmt" %>

<%@ page isELIgnored="false" %>

<!-- custservice\GmItemPicSlipGlobal.jsp -->

<fmtItemPicSlipGlobal:setBundle basename="properties.labels.custservice.GmItemPicSlipGlobal"/>

<%
try {
	GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmItemPicSlipGlobal", strSessCompanyLocale);
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	log = GmLogger.getInstance(GmCommonConstants.CUSTOMERSERVICE);
	strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	strCssPath = GmCommonClass.getString("GMSTYLES");
	strImagePath = GmCommonClass.getString("GMIMAGES");
	strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	strCommonPath = GmCommonClass.getString("GMCOMMON");
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");
	String strSource = (String)request.getAttribute("source");
	String strRuleSource = (String) request.getAttribute("strRuleSource");
	String strCrComments= GmCommonClass.parseNull((String)request.getAttribute("CRCOMMENTS"));
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strSetId = "";
	String strSetNm = "";
	String strEtchId =	"";
	String strDesc = "";
	String strRefId = "";
	String strShade = "";
	String strConsignId = "";
	String strSetName= "";
	String strAccName = "";
	String strUserName = "";
	String strIniDate = "";
	String strRequestorName = "";
	String strType = "";
	String strInHousePurpose = "";
	String strConsignStatusFlag = "";
	String strCtype="";
	String strBillNm="";
	String strToHeader="";
	String strSlipHeader=GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PICKSLIP"));
	String strdetails=GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_TRANSCATION_DETAILS"));
	String strShippingLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_SHIPPING_DATE"));
	String strCarrier = "";
	String strMode="";
	String strTrackno="";
	String  strLog_Comments="";
	String strCompanyId = "";
	String strCustPO = "";

	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	strCountryCode = gmResourceBundleBean.getProperty("COUNTRYCODE");

	boolean flg =false;
	String strTranAdd = "";
	
	ArrayList alSetLoad = new ArrayList();
	HashMap hmConsignDetails = new HashMap();

	int intPriceSize = 0;

	if (hmReturn != null)
	{
		alSetLoad = (ArrayList)hmReturn.get("SETLOAD");
		log.debug(" loaded set details " +alSetLoad);
		//strSetId = (String)session.getAttribute("hSetId") == null?"":(String)session.getAttribute("hSetId");
	
		hmConsignDetails = (HashMap)hmReturn.get("CONDETAILS");
		log.debug(" hmConsignDetails set details " +		hmConsignDetails);
		strConsignId = (String)hmConsignDetails.get("CID");
		strSetId = GmCommonClass.parseNull((String)hmConsignDetails.get("SETID"));
		strSetName= GmCommonClass.parseNull((String)hmConsignDetails.get("SETNM"));
		// When Type is CN then Set Name is empty so, get the set name from Key SNAME.
		strSetName = strSetName.equals("")? GmCommonClass.parseNull((String)hmConsignDetails.get("SNAME")):strSetName;
		strAccName = (String)hmConsignDetails.get("ANAME");
		String strDistName = (String)hmConsignDetails.get("DNAME");
		strAccName = strAccName.equals("")?strDistName:strAccName;
		strUserName = (String)hmConsignDetails.get("UNAME");
		strIniDate = (String)hmConsignDetails.get("CDATE");
		strDesc = (String)hmConsignDetails.get("COMMENTS");
		strType = (String)hmConsignDetails.get("TYPE");
		strCtype = (String)hmConsignDetails.get("CTYPE");
		strInHousePurpose = (String)hmConsignDetails.get("PURP");
		strBillNm = GmCommonClass.parseNull((String)hmConsignDetails.get("HISTBILLNM"));
		strTranAdd = GmCommonClass.parseNull((String)hmConsignDetails.get("TRBILLNM"));
		strRequestorName = GmCommonClass.parseNull((String)hmConsignDetails.get("SHIPADD"));
		strConsignStatusFlag = GmCommonClass.parseNull((String)hmConsignDetails.get("SFL"));
		strCarrier = GmCommonClass.parseNull((String)hmConsignDetails.get("CARRIER"));
		strMode=GmCommonClass.parseNull((String)hmConsignDetails.get("SHIPMODE"));
		strTrackno= GmCommonClass.parseNull((String)hmConsignDetails.get("TRACKNO"));
		strEtchId = GmCommonClass.parseNull((String)hmConsignDetails.get("ETCHID"));
		strEtchId = strEtchId.equals("")?"-":strEtchId;
		strRefId = GmCommonClass.parseNull((String)hmConsignDetails.get("REFID"));
		strCustPO = GmCommonClass.parseNull((String)hmConsignDetails.get("CSTPO"));
		
		//When do item initiate enter the value in CustPO in screen. The ref id only for Profoma Invoice. 
		if(strRefId.equals("") && !strCustPO.equals(""))
			strRefId = strCustPO;
		
		strLog_Comments= GmCommonClass.parseNull((String)hmConsignDetails.get("LOG_COMMENTS"));
		strCompanyId = GmCommonClass.parseNull((String)hmConsignDetails.get("COMPANYID"));
	}
	if (strCtype.equals("50154")) {//Move from Shelf to Loaner Extension w/Replenish
		strToHeader = "Replenished To";
	}else if(strCtype.equals("9110")||strCtype.equals("1006572")||strCtype.equals("1006571")||strCtype.equals("1006570")||strCtype.equals("1006573")||strCtype.equals("50150")||strCtype.equals("1006575")){//ISMP,ISBO,BLIS,IHIS,FGIS,IHLE
		strToHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_LOANED_TO"));
	}else {
		strToHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_CONSIGNED_TO"));
	}
	
	if (strConsignStatusFlag.equals("4") && strCtype.equals("50154") ){//Packslip 
		strSlipHeader =GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PACKING_SLIP"));
		strdetails = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_TRANSACTION_DETAILS"));
		 flg = true;
	}
	if (strCtype.equals("93341") ){		
		strSlipHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_STOCK_TRANSFER"));
	}
	if(strCtype.equals("100062"))
	{
		strRequestorName=GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_BACK_ORDER"));
	}
	if(strConsignStatusFlag.equals("2")||strConsignStatusFlag.equals("3") || strCtype.equals("50154")){
		strDesc+="<BR>"+strCrComments+"<BR>"+strLog_Comments;
	}
	int intSize = 0;
	HashMap hcboVal = null;
	
	StringBuffer sbStartSection = new StringBuffer();
	sbStartSection.append("<tr><td colspan=2>");
	sbStartSection.append("<table border=0 width=100% cellspacing=0 cellpadding=0>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption>");
	//sbStartSection.append("<td height=20 align=center width=250>&nbsp;Consigned To</td>");
	sbStartSection.append("<td height=20 align=center width=250>&nbsp;");
	sbStartSection.append(strToHeader);
	sbStartSection.append("</td>");
	sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=11></td>");
	sbStartSection.append("<td width=250 align=center>&nbsp;Ship To</td>");
	sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=11></td>");
	sbStartSection.append("<td width=100 align=center>&nbsp;Date</td>");
	sbStartSection.append("<td width=100 align=center>&nbsp;Transaction #</td>");
	sbStartSection.append("</tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
	sbStartSection.append("<tr><td class=RightText rowspan=5 valign=top>&nbsp;");
	if (strCtype.equals("50154")) {//Move from Shelf to Loaner Extension w/Replenish
		sbStartSection.append(strBillNm);
	}else if (strCtype.equals("93341")){
		sbStartSection.append(strTranAdd);
	}
	else {
		sbStartSection.append(strAccName);
	}
	sbStartSection.append("</td>");
	sbStartSection.append("<td rowspan=5 class=RightText valign=top>&nbsp;");sbStartSection.append(strRequestorName);
	sbStartSection.append("</td>");
	sbStartSection.append("<td height=25 class=RightText align=center>&nbsp;");sbStartSection.append(strIniDate);
	sbStartSection.append("</td><td class=RightText align=center>&nbsp;");
	sbStartSection.append(strConsignId);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption><td align=center>Ref ID</td><td align=center>Initiated By</td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	sbStartSection.append("<tr><td align=center class=RightText>&nbsp;");sbStartSection.append(strRefId);
	sbStartSection.append("<Br></td><td align=center height=25 nowrap class=RightText>&nbsp;");sbStartSection.append(strUserName);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");	
	if (flg == true){
		sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption>");
		sbStartSection.append("<td  width=250 height=20 align=center >&nbsp;Carrier </td>");
		sbStartSection.append("<td width=250 height=20 align=center >&nbsp;Mode</td>");
		sbStartSection.append("<td width=250 colspan=3 height=20 align=center >&nbsp;Tracking #</td>");		
		sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
		
		sbStartSection.append("<tr> <td  height=20 align=center >&nbsp;");		
		sbStartSection.append(strCarrier);
		sbStartSection.append("</td>");
		sbStartSection.append("<td  height=20 align=center >&nbsp;");
		sbStartSection.append(strMode);
		sbStartSection.append("</td>");
		sbStartSection.append("<td colspan=3 height=20 align=center >&nbsp");
		sbStartSection.append(strTrackno);
		sbStartSection.append("</td>");		
		sbStartSection.append("<tr><td  bgcolor=#666666 height=1 colspan=6></td></tr>");
		
	}
	sbStartSection.append("</table></td></tr>");
	String strId = strConsignId+"$"+strSource;
	//strRuleSource = strRuleSource.equals("")?strCtype : strRuleSource;
	String strRuleId = strConsignId + "$" + strCtype;

	log.debug("strId value is "+strId);
%>
<HTML>
<HEAD>
<TITLE> GlobusOne Enterprise Portal: Pick Slip </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script>

function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}

</script>


</HEAD>

<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmVendor">
<table border="0" class="DtRuleTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td align="right" height="30" id="button">
				<fmtItemPicSlipGlobal:message key="IMG_ALT_PRINT" var = "varPrint"/>
				<img style="cursor:hand" src='<%=strImagePath%>/print-icon.png' height="25" width="25" alt="${varPrint}" onClick="fnPrint();" />
				<fmtItemPicSlipGlobal:message key="IMG_ALT_CLOSE" var = "varClose"/>
				<img style="cursor:hand" src='<%=strImagePath%>/close_icon.png' height="20" width="20" alt="${varClose}" onClick="window.close();" />
			</td>
		<tr>
	</table>
	 
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td height="80" width="170"><img src="<%=strImagePath%>/<%=strCompanyId%>.gif" width="138" height="60"></td>
						<%if(strCountryCode.equals("en")){ %>
							<td align="left" class="RightText"><font size="+3"><%=strSlipHeader%></font>&nbsp;</td>
						<%}else{ %>
							<td align="right" class="RightText"><font size="+2"><%=strSlipHeader%></font>&nbsp;</td>
						<%}%>
						<%
							if (!strCtype.equals("50159") && !strCtype.equals("93341")){ //Usage sheet
						%>
						<td rowspan="2">
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr>
									<td height="30"><img src='/GmCommonBarCodeServlet?ID=<%=strId%>' height="20" width="220" /></td>
									<td>&nbsp;<fmtItemPicSlipGlobal:message key="LBL_FEDEX_MODULE"/></td>
								</tr>
								<tr>
									<td height="30" align="right"><img src='/GmCommonBarCodeServlet?ID=<%=strConsignId%>' height="20" width="170" /></td>
									<td>&nbsp;<fmtItemPicSlipGlobal:message key="LBL_PAPER_WORK"/></td>
								</tr>
								<tr>
									<td height="30" align="right"><img src='/GmCommonBarCodeServlet?ID=<%=strRuleId%>&type=2d' height="40"	width="40" /></td>
									<td>&nbsp;<fmtItemPicSlipGlobal:message key="LBL_MOTOROLA_DEVICE"/></td>
								</tr>
							</table>			
						</td>
						<%} %>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" cellspacing="0" cellpadding="0">
<!--StartSection starts here-->
<%out.print(sbStartSection);%>
<!--StartSection ends here-->				
					<tr>
						<td class="RightTableCaption" bgcolor="#eeeeee" colspan = "2" HEIGHT="20"><%=strdetails%></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>					
					<tr>
							<td colspan="2" class="RightText">
								<table border="0" width="100%" cellspacing="0" cellpadding="0">
									<tr height="20">
										<td align="right" class="RightTableCaption" width="10%"><fmtItemPicSlipGlobal:message key="LBL_TYPE"/>:</td>
										<td>&nbsp;<%=strType%></td>
										<td align="right" class="RightTableCaption" width="8%"><fmtItemPicSlipGlobal:message key="LBL_REASON"/>:</td>
										<td colspan="3">&nbsp;<%=strInHousePurpose%></td>
									</tr>
<%
			if (!strSetId.equals(""))
			{
%>
									<tr><td colspan="6" height="1" class="LLine"></td></tr>
									<tr height="20">
										<td align="right" class="RightTableCaption"><fmtItemPicSlipGlobal:message key="LBL_SET"/>:</td>
										<td>&nbsp;<%=strSetId%>&nbsp;-&nbsp;<%=strSetName%></td>
										<td align="right" class="RightTableCaption"><fmtItemPicSlipGlobal:message key="LBL_CN_ID"/>:</td>
										<td>&nbsp;<%=strRefId%></td>
										<td align="right" class="RightTableCaption"><fmtItemPicSlipGlobal:message key="LBL_ETCH_ID"/>:</td>
										<td>&nbsp;<%=strEtchId%></td>
									</tr>
<%
			}
%>
								</table>
							</td>
					</tr>
					<tr>
						<td colspan="2"  height="400" valign="top">
							<table cellspacing="0" cellpadding="0" border="0" id="myTable" width="100%">
								<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
								<tr bgcolor="#eeeeee" class="RightTableCaption">
									<td height="25" width="14%"><fmtItemPicSlipGlobal:message key="LBL_PART_NUMBER"/></td>
									<td width="50%"><fmtItemPicSlipGlobal:message key="LBL_DESCRIPTION"/></td>
									<td width="8%"><fmtItemPicSlipGlobal:message key="LBL_REQ_QTY"/></td>
									<td width="50%">&nbsp;&nbsp;&nbsp;&nbsp;<fmtItemPicSlipGlobal:message key="LBL_QTY_LOCATION"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								</tr>
								<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
<%
						intSize = alSetLoad.size();
						if (intSize > 0)
						{
							hcboVal = new HashMap();
							for (int i=0;i<intSize;i++)
							{
								hcboVal = (HashMap)alSetLoad.get(i);
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
%>
								<tr <%=strShade%>>
									<td class="RightText" height="20" width="14%"><%=(String)hcboVal.get("PNUM")%></td>
									<td class="RightText" width="50%"><%=GmCommonClass.getStringWithTM((String)hcboVal.get("PDESC"))%></td>
									<td align="center" class="RightText" width="8%"><%=GmCommonClass.parseNull((String) hcboVal.get("IQTY"))%></td>
<%									if(Double.parseDouble(strConsignStatusFlag) >= 2) {  %>
									<td class="RightText" width="50%">&nbsp;&nbsp;&nbsp;_ _ _ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=GmCommonClass.parseNull((String)hcboVal.get("SUGLOCATIONCD"))%>
<% } else { %>									
									<td class="RightText" width="50%">&nbsp;&nbsp;&nbsp;_ _ _ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_ _ _ _ _ _ _ _ _ _
<% } %>									
									</td>
								</tr>
								<tr><td colspan="4" height="1" bgcolor="#eeeeee"></td></tr>
<%
							}
						}else {
%>
						<tr><td colspan="4" height="50" align="center" class="RightTextRed"><BR><fmtItemPicSlipGlobal:message key="LBL_NO_TRANSACTION_PART_NUMBERS"/> !</td></tr>
<%
						}		
%>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" height="1"></td></tr>
					<tr>
						<td bgcolor="#eeeeee" colspan="2">
							<jsp:include page="/common/GmRuleDisplayInclude.jsp">
							<jsp:param name="Show" value="false" />
							<jsp:param name="Fonts" value="false" />
							</jsp:include>
						</td>
					</tr>
				<% if (!strConsignStatusFlag.equals("4")){//Packslip %>  
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr valign="top" height="60">
						<td colspan="2" class="RightText"><b><fmtItemPicSlipGlobal:message key="LBL_COMMENTS"/>:</b><BR>&nbsp;<%=strDesc%></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>					
					<tr><td class="RightText" colspan="2"><u><fmtItemPicSlipGlobal:message key="LBL_PLEASE_INITIAL_HERE"/>:</u></td></tr>					
					<tr><td colspan="2" height="50">&nbsp;</td></tr>
					<tr>
						<td class="RightText">&nbsp;&nbsp;&nbsp;<fmtItemPicSlipGlobal:message key="LBL_INITIATED_BY"/></td>
						<td class="RightText" align="right"><fmtItemPicSlipGlobal:message key="LBL_VERIFIED_BY"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
				<% } %>
				 </table>
			 </td>
		</tr>
		<tr><td colspan="3" height="1"></td></tr>		
    </table>
	<table border="0" class="DtRuleTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td>
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					<jsp:include page="/common/GmIncludeDateStamp.jsp" />
			</td>	
		</tr>
	</table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>