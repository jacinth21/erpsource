<%
/**********************************************************************************
 * File		 		: GmItemConsignEdit.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib prefix="fmtItemConsignEdit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmItemConsignEdit.jsp -->
<fmtItemConsignEdit:setLocale value="<%=strLocale%>"/>
<fmtItemConsignEdit:setBundle basename="properties.labels.custservice.GmItemConsignEdit"/>


<%
String strCustServiceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
try {
	
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session); 
	
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmItemConsignEdit", strSessCompanyLocale);
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmTransRules = (HashMap)request.getAttribute("TRANS_RULES");
	//HashMap hmTransRules = (HashMap)session.getAttribute("TRANS_RULES");
	String strRuleType =GmCommonClass.parseNull((String)(String) hmTransRules.get("TRANS_RLTXN"));
	String strRuleConsignType =(String) hmTransRules.get("TRANS_ID");
	String strTransStatus =(String) hmTransRules.get("ICE_TXN_STATUS");
	String strRuleAjax =(String) hmTransRules.get("ICE_RULE_AJAX");
	String strOpt=GmCommonClass.parseNull((String)hmTransRules.get("STR_OPT"));
	String strIncludeRuleEng = GmCommonClass.parseNull((String) hmTransRules.get("INCLUDE_RULE_ENGINE"));
	String strTransTypeID = GmCommonClass.parseNull((String) hmTransRules.get("TRANS_ID"));
	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");
	String strConsignId = (String)request.getAttribute("hConsignId") == null?"":(String)request.getAttribute("hConsignId");
	String strControlNoScan = GmCommonClass.parseNull((String)GmCommonClass.getString("SCAN_CONTROL_NO"));
	String strPickLocRule = GmCommonClass.parseNull((String) hmTransRules.get("PICK_LOC"));
	String strSkpLotValidFl = GmCommonClass.parseNull((String)request.getAttribute("strSkpLotValidFl"));
	
	String strChecked = "";
	String strSetId = "";
	String strDesc = "";
	String strTemp = "";
	String strShade = "";

	String strSetName= "";
	String strAccName = "";
	String strUserName = "";
	String strIniDate = "";
	String strItemQty = "";
	String strControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strConsignType = "";
	String strHeader =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ITEM_CONSIGN_HEADER"));
	String strLabel = "";
	String strRequestorName = "";
	String strConsignSetId = "";
	String strFlag = (String)request.getAttribute("hShipFl") == null?"":(String)request.getAttribute("hShipFl");
	strChecked = strFlag.equals("3")?"checked":"";
	String strPrice = "";
	
	String strTagFl = "";
	String strStatusFl = "";
	String strShipFl = "";

	String strDisable=GmCommonClass.parseNull((String)request.getAttribute("disable"));
	
	String strText = "Release to Consignee";
	String strType = "";
	ArrayList alSetLoad = new ArrayList();
	HashMap hmConsignDetails = new HashMap();
	
	String strHeaderRule = "";
	String strLabelRule = "";
	String strTypeRule = "";
	String strApplDateFmt = strGCompDateFmt;

	String strErrorCnt = "";
	String strErrorDtls = "";
	
	String strQtyId = "";
	
	int intPriceSize = 0;

	if (hmReturn != null)
	{
		if (strhAction.equals("LoadSet")|| strhAction.equals("EditLoad") || strhAction.equals("EditLoadDash"))
		{
			alSetLoad = (ArrayList)hmReturn.get("SETLOAD");
			strSetId = (String)session.getAttribute("hSetId") == null?"":(String)session.getAttribute("hSetId");
		
			hmConsignDetails = (HashMap)hmReturn.get("CONDETAILS");
			strConsignId = (String)hmConsignDetails.get("CID");
			strSetName= (String)hmConsignDetails.get("SNAME");
			strAccName = (String)hmConsignDetails.get("ANAME");
			String strDistName = (String)hmConsignDetails.get("DNAME");
			strAccName = strAccName.equals("")?strDistName:strAccName;
			strUserName = (String)hmConsignDetails.get("UNAME");
			strIniDate = (String)hmConsignDetails.get("CDATE");
			    //GmCommonClass.getStringFromDate((java.util.Date) hmConsignDetails.get("CDATE"), strApplDateFmt);	
			strDesc = (String)hmConsignDetails.get("COMMENTS");
			strStatusFl = (String)hmConsignDetails.get("SFL");
			strShipFl = (String)hmConsignDetails.get("SHIPFL");
			strLabel = strShipFl.equals("1")?"Release to Shipping":strText;
			strConsignType = (String)hmConsignDetails.get("CTYPE");
			strRequestorName = (String)hmConsignDetails.get("RNAME");
			strConsignSetId = (String)hmConsignDetails.get("SETID");
			strErrorCnt = GmCommonClass.parseNull((String)hmConsignDetails.get("ERRCNT"));
		}
	}
	int intSize = 0;
	HashMap hcboVal = null;
	if (strConsignType.equals("4114"))
	{
		strHeader =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ITEM_CONSIGN_HEADER_SHELF_TO_RE_PACKAGING")); 
		strLabel = "Release for Verification";
		strType = "50911";
	}
	else if (strConsignType.equals("4115"))
	{
		//strHeader = "Quarantine Items - Control";
		strType = "50915";
		strHeader =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ITEM_CONSIGN_HEADER_QUARANTINE_TO_SCRAP"));
		strLabel = "Release for Verification";		
	}
	
	if (strConsignType.equals("4113"))
	{
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ITEM_CONSIGN_HEADER_SHELF_TO_QUARARANTINE"));
		strLabel = "Release for Verification";		
		strType = "50907";
	}
	else if(strConsignType.equals("4116"))
	{
		strHeader =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ITEM_CONSIGN_HEADER_QUARANTINE_TO_SHELF")); 
		strType = "50914";
		strLabel = "Release for Verification";
	}
	else if(strConsignType.equals("4110"))
	{
		strType = "50927";
		strHeader =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ITEM_CONSIGN_HEADER_ITEM_CONSIGNMENT"));
	}
	else if(strConsignType.equals("4112"))
	{
		strType = "50928";
		strHeader =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ITEM_CONSIGN_HEADER_IN_HOUSE_CONSIGNMENT")); 
	}
	else if(strConsignType.equals("4119"))
	{
		strHeader =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ITEM_CONSIGN_HEADER_IN_HOUSE_LOANER")); 
	}
	if(!strRuleType.equals("")){
		strType = strRuleType;
	} 
	if(strRuleConsignType!=null){
		strConsignType = strRuleConsignType;
	}
	
	// Same variable strType is used to set the TRANS_RULE AND ICE_TYPE to avoid that introduced a new variable for TRANS_RULE
	String strReTxn = strType;
	String strPendingCtrl =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PENDING_CONTROL"));
	
	if(hmTransRules != null)
	{
	   strHeaderRule = GmCommonClass.parseNull((String)hmTransRules.get("ICE_HEADER"));
	   strLabelRule = GmCommonClass.parseNull((String)hmTransRules.get("ICE_LABEL"));
	   strTypeRule= GmCommonClass.parseNull((String)hmTransRules.get("ICE_TYPE"));
	   
	   if(!strHeaderRule.equals(""))
		   strHeader = strPendingCtrl+" : "+strHeaderRule;
	   if(!strLabelRule.equals(""))
		   strLabel = strLabelRule ;
	   if(!strTypeRule.equals(""))
		   strType = strTypeRule ;
	}
	request.setAttribute("alReturn", alSetLoad);	
	
			
%>
<HTML>
<HEAD>
<TITLE>Globus Medical:Item Consignment-Enter Control Numbers TEst </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strCustServiceJsPath%>/GmItemConsignEdit.js"></script>

<script type="text/javascript">
var pickRule = '<%=strPickLocRule%>';
var consignType = '<%=strConsignType%>';
var transStatus  = '<%=strTransStatus%>';
var strRuleAjax='<%=strRuleAjax%>';
var includeRuleEng = '<%=strIncludeRuleEng.toUpperCase()%>';
var vIncludeRuleEng = '<%=strIncludeRuleEng.toUpperCase()%>';
var skpLotValidFl = '<%=strSkpLotValidFl%>';
var Type ='<%=strType%>';
var TransTypeID = '<%=strTransTypeID%>';
var consignId = '<%=strConsignId%>';

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnExistScanPartCount();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmConsignItemServlet">
<input type="hidden" name="hAction" id="hAction"  value="<%=strhAction%>">
<input type="hidden" name="hConsignId" value="<%=strConsignId%>">
<input type="hidden" name="hShipFl" value="<%=strShipFl%>">
<input type="hidden" name="hConsignType" value="<%=strConsignType%>">
<input type="hidden" name="hCancelType" value="VODCN">
<input type="hidden" name="hTxnId" value="<%=strConsignId%>">
<input type="hidden" name="consignSetId" value="<%=strConsignSetId%>">
<input type="hidden" name="RE_FORWARD" value="gmConsignItemServlet">
<input type="hidden" name="txnStatus" value="PROCESS">
<input type="hidden" name="RE_TXN" id="RE_TXN" value="<%=strReTxn%>">
<input type="hidden" id="TXN_TYPE_ID" name="TXN_TYPE_ID" value="<%=strTransTypeID%>">
<input type="hidden" name="hTxnName" value="<%=strConsignId%>">
<input type="hidden" name="hType"  name="hType" value="<%=strConsignType%>">
<input type="hidden" name="hMode" id="hType" value="">
<input type="hidden" name="partMaterialType" id="partMaterialType" value= "">
<input type="hidden" name="chkRule" id="chkRule" value= "No">

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
			<td height="25" class="RightDashBoardHeader"><%=strHeader%></td>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr >
			<td width="598" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="shade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtItemConsignEdit:message key="LBL_CONSIGNMENT_ID"/>:</b></td>
						<td class="RightText">&nbsp;<%=strConsignId%></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>	
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtItemConsignEdit:message key="LBL_INITIATED_BY"/>:</b></td>
						<td class="RightText">&nbsp;<%=strUserName%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B><fmtItemConsignEdit:message key="LBL_DATE_INITIATED"/>:</B>&nbsp;<%=strIniDate%></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>	
					<tr  class="shade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtItemConsignEdit:message key="LBL_CONSIGN_TO"/>:</b></td>
						<td class="RightText">&nbsp;<%=strRequestorName%></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>	
					<tr>
						<td class="RightText" valign="top" align="right" HEIGHT="60"><b><fmtItemConsignEdit:message key="LBL_COMMENTS"/>:</b></td>
						<td class="RightText" valign="top" >&nbsp;<%=strDesc%></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
<%
				if (!strStatusFl.equals("4"))
				{
%>									
					<tr class="shade">
						<td class="RightText" align="right" HEIGHT="24"><b><%=strLabel%> ?:</b></td>
						<td width="320">&nbsp;<input type="checkbox" size="30" name="Chk_ShipFl" value="" <%=strChecked%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=25></td>
					</tr>
					<%if(strControlNoScan.equals("YES")){%>
					 <!-- To include the FG BIN Section-->
							<tr>
							<td colspan="3">
								<jsp:include page="/operations/itemcontrol/GmControlNoInclude.jsp" >
								<jsp:param name="FORMNAME" value="frmVendor" />
								</jsp:include>
							</td>
					     </tr>
  					<%} %> 
<%}if (strhAction.equals("EditLoad") || strhAction.equals("EditLoadDash")){ %>
					<tr>
						<td colspan="3">
						<jsp:include page="/common/GmIncludeLog.jsp" >
						<jsp:param name="LogType" value="" />
						<jsp:param name="LogMode" value="Edit" />
						</jsp:include>
						</td>
					</tr>
					
					
<%
				}
					if (strhAction.equals("LoadSet")|| strhAction.equals("EditLoad") || strhAction.equals("EditLoadDash"))
					{
%>
					<tr>
						<td colspan="2">
							<table cellspacing="0" cellpadding="0" border="0" class="DtTable700" id="myTable">
								<tr class="ShadeRightTableCaption">
									<td align="center" height="25"><fmtItemConsignEdit:message key="LBL_PART"/><BR><fmtItemConsignEdit:message key="LBL_NUMBER"/></td>
									<td width="200"><fmtItemConsignEdit:message key="LBL_DESCRIPTION"/></td>
									<td align="center"><fmtItemConsignEdit:message key="LBL_CONSIGN"/><BR><fmtItemConsignEdit:message key="LBL_QTY"/></td>
									<td align="center"><fmtItemConsignEdit:message key="LBL_SCANNED"/><BR><fmtItemConsignEdit:message key="LBL_QTY"/></td>
									<td width="200">&nbsp;&nbsp;&nbsp;&nbsp;<fmtItemConsignEdit:message key="LBL_QTY"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtItemConsignEdit:message key="LBL_CONTROL_NUMBER"/></td>
								</tr>
<%
						intSize = alSetLoad.size();
						StringBuffer sbPartNum = new StringBuffer();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();

							String strNextPartNum = "";
							int intCount = 0;
							String strColor = "";
							String strLine = "";
							String strPartNumHidden = "";
							String strPartNumIndex = "";
							String StrParthidsts = "";
							String strParthidfinalsts = "";
							String strPrevPartNum = "";
							String strPartMaterialType = "";
							double dbAllTotQty = 0;
							
							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alSetLoad.get(i);
								if (i<intSize-1)
								{
									hmTempLoop = (HashMap)alSetLoad.get(i+1);
									strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
								}								
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
								if(sbPartNum.indexOf(strPartNum+",") == -1){
									sbPartNum.append(strPartNum);
									sbPartNum.append(",");
								}								
								strPartNumHidden = strPartNum;
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strPartMaterialType = GmCommonClass.parseNull((String)hmLoop.get("PARTMTRELTYPE"));	
								strQty = GmCommonClass.parseNull((String)hmLoop.get("QTYUSED"));
								strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
								strTagFl = GmCommonClass.parseNull((String)hmLoop.get("TAGFL"));
								strErrorDtls = GmCommonClass.parseNull((String)hmLoop.get("ERRORDTLS"));
								strErrorDtls = strErrorDtls.replaceAll("<B>", "").replaceAll("</B>", "");
								strTemp = strPartNum;
								StrParthidsts = "";
												
								strQtyId = strPartNum.replaceAll("[^a-zA-Z0-9]","");
				
								
								if(!strPrevPartNum.equals(strPartNum)&&(i!=0)){									
									StrParthidsts = "<input type=\"hidden\" id=\""+strPrevPartNum+"\" name=\""+strPrevPartNum+"\" value=\""+strPartNumIndex+"\">";
									strPartNumIndex="";
								}
								
								if (strPartNum.equals(strNextPartNum))
								{
									intCount++;
									strLine = "<TR><TD colspan=5 height=1 class=Line></TD></TR>";
								}

								else
								{
									strLine = "<TR><TD colspan=5 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
										strPartNum = "";
										strPartDesc = "";
										strQty = "";
										//strColor = "bgcolor=white";
										strLine = "";
									}
									else
									{
										//strColor = "";
									}
									intCount = 0;
								}
								strPartNumIndex = strPartNumIndex +i+",";
								if (intCount > 1)
								{
									strPartNum = "";
									strPartDesc = "";
									strQty = "";
									//strColor = "bgcolor=white";
									strLine = "";
								}
								
								if(i==(intSize-1)){
									strParthidfinalsts = "<input type=\"hidden\" id=\""+(strPartNum.equals("")?strPrevPartNum:strPartNum)+"\" name=\""+(strPartNum.equals("")?strPrevPartNum:strPartNum)+"\" value=\""+strPartNumIndex+"\">";
									strPartNumIndex = "";
								}
							
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
								strItemQty = GmCommonClass.parseNull((String)hmLoop.get("IQTY"));
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								dbAllTotQty += Double.parseDouble(GmCommonClass.parseZero(strItemQty));
								strPrevPartNum = strPartNumHidden;
								out.print(strLine);
%>
								<tr id="tr<%=i %>">
<%
								if ((strhAction.equals("EditLoad") || strhAction.equals("EditLoadDash") || !strErrorCnt.equals("0")) && !strStatusFl.equals("4"))
								{
%>
									<td class="RightText" height="20">&nbsp;<%if(strTagFl.equals("Y")){%><a href=javascript:fnOpenTag('<%=strPartNum %>','<%=strConsignId %>')> <img src=<%=strImagePath%>/tag.jpg border="0"></a><%}%><a class="RightText" tabindex=-1 href="javascript:fnSplit('<%=i%>','<%=strPartNum%>');"><%=strPartNum%></a>
									<input type="hidden" name="hPartNum<%=i%>" id="hPartNum<%=i%>" value="<%=strPartNumHidden%>">
									<input type="hidden" name="hQty<%=strQtyId%>" id="hQty<%=strQtyId%>"  value="<%=strQty%>">
									<input type="hidden" name="hPrice<%=i%>" id="hPrice<%=i%>" value="<%=strPrice%>"></td>
									<input type="hidden" id="hPartMaterialType<%=i%>" name="hPartMaterialType<%=i%>" value="<%=strPartMaterialType%>">
									<input type="hidden" id="hControl<%=i%>" name="hControl<%=i%>" value="<%=strControl%>">									
									<%if(StrParthidsts.length()>0){%>
										<%=StrParthidsts%>
									<%}%>
									<%if(strParthidfinalsts.length()>0){%>
										<%=strParthidfinalsts%>
									<%}%>
<%
								}else{
%>
									<td class="RightText" height="20">&nbsp;<%if(strTagFl.equals("Y")){%><a href=javascript:fnOpenTag('<%=strPartNum %>','<%=strConsignId %>')> <img src=<%=strImagePath%>/tag.jpg border="0"></a><%}%> <%=strPartNum%></td>
<%
									}
%>
									<td class="RightText"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td class="RightText" align="center"><%=strQty%></td>
									<td align="center"><label id ="lblPartScan<%=strPartNum %>" ></label> </td>
									
<%
								if (strhAction.equals("EditLoad")|| strhAction.equals("EditLoadDash") && strErrorCnt.equals("0"))
								{
%>
									<td id="Cell<%=i%>" class="RightText">&nbsp;&nbsp;&nbsp;<input type="text" size="3" value="<%=strItemQty%>" name="Txt_Qty<%=i%>"  id="Txt_Qty<%=i%>" class="InputArea Controlinput"  onFocus="chgTRBgColor(<%=i %>,'#AACCE8');" onBlur="chgTRBgColor(<%=i %>,'#ffffff');" tabindex=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="text" size="22" value="<%=strControl%>" id="Txt_CNum<%=i%>" name="Txt_CNum<%=i%>" class="InputArea Controlinput" maxlength="40"  onBlur="fnBlankTBE(<%=i %>,this.value);<%if(!strIncludeRuleEng.equalsIgnoreCase("NO")){%>fnFetchMessages(this,'<%=strPartNumHidden%>','PROCESS', '<%=strType %>','<%=strTransTypeID%>');<%}%>" onFocus="fnBlankLine(<%=i %>,this.value);" tabindex=3>
									<% if(!strErrorDtls.equals("") && !strControl.equals("")){ %>
									<span id="DivShowCnumNotExists<%=i%>" style="vertical-align:middle;"> <img height="12" width="12" title="<%=strErrorDtls %>" src="<%=strImagePath%>/delete.gif"></img></span>
									<%} %>
									</td>
<%
								}else{
%>
									<td>&nbsp;</td>
<%
									}
%>
								</tr>
<%
								//out.print(strLine);
							}//End of FOR Loop
							out.print("<input type=hidden name=pnums value='"+sbPartNum.toString().substring(0,sbPartNum.length()-1)+"'>");
							out.print("<input type=hidden id=hTotalParts name=hTotalParts value="+dbAllTotQty+">");
						}else {
%>
						<tr><td colspan="5" height="50" align="center" class="RightTextRed"><BR><fmtItemConsignEdit:message key="LBL_NO_PART_NUMBERS_HAVE_BEEN_MAPPED"/></td></tr>
<%
						}		
%>
							</table>
						</td>								
						<input type="hidden" id="hCnt"    name="hCnt"    value="<%=intSize-1%>">
						<input type="hidden" id="hNewCnt" name="hNewCnt" value="0">
					
					</tr>
<%
					}
if(strConsignType.equals("4114") || strConsignType.equals("4113")||strConsignType.equals("4116") || strConsignType.equals("4110") ||strConsignType.equals("4112") || strConsignType.equals("4115") || strConsignType.equals((String)hmTransRules.get("TRANS_ID")))
{ 
%>				
		<tr>
			<td colspan="3">
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					<jsp:include page="/common/GmRuleDisplayAjaxInclude.jsp"/>
			</td>
		</tr>
<%} %>				
<%
				if (!strStatusFl.equals("4"))
				{
					if (strhAction.equals("LoadSet"))
					{
%>
					<tr>
						<td colspan="2" height="30" align="center">
							<fmtItemConsignEdit:message key="BTN_INITIATE" var="varInitiate"/>
							<gmjsp:button value="${varInitiate}" gmClass="button" onClick="fnInitiate();" tabindex="13" buttonType="Save" />
						</td>
					</tr>
<%
					}else if (strhAction.equals("EditLoad") || strhAction.equals("EditLoadDash")){
						String strAccess=GmCommonClass.parseNull((String)request.getAttribute("ACCESSFL")).equals("")?GmCommonClass.parseNull((String)session.getAttribute("ACCESSFL")): GmCommonClass.parseNull((String)request.getAttribute("ACCESSFL"));
%>                
                      <tr>
						<td colspan="2" height="30" align="center">
						<%						if(strAccess.equals("Y")){ %>
<!-- 							<gmjsp:button value=" Void   " gmClass="button" onClick="fnVoidCsg();" tabindex="13" buttonType="Save" />&nbsp;&nbsp;  -->

							<fmtItemConsignEdit:message key="BTN_SUBMIT" var="varSubmit"/>
							<gmjsp:button value="${varSubmit}" gmClass="button" onClick="fnSubmit();" tabindex="13" disabled="<%=strDisable%>" buttonType="Save" />&nbsp;&nbsp;
							<fmtItemConsignEdit:message key="BTN_VOID" var="varVoid"/>
							<gmjsp:button value="${varVoid}" gmClass="button" onClick="fnVRule();" tabindex="14" buttonType="Save" />
							<%}else{
								out.print("<b><center><span style=\"color:red;\">You do not have enough permission to do this action</span><center></b>");	
							}
								%>
						</td>
					</tr>
<%
					}
				}else{
%>
					<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>	
					<tr>
						<td colspan="2" height="30" align="center" class="RightTextRed"><BR><fmtItemConsignEdit:message key="LBL_CONSIGNMENT_VERIFICATION"/>
						<BR><BR>
							<fmtItemConsignEdit:message key="BTN_AGREEMENT_FORM" var="varAgreementForm"/>
							<gmjsp:button value="${varAgreementForm}" gmClass="button" onClick="fnAgree();" tabindex="13" buttonType="Save" />&nbsp;&nbsp;
							<fmtItemConsignEdit:message key="BTN_PRINT_VERSION" var="varLoadPrintVersion"/>
							<gmjsp:button value="${varLoadPrintVersion}" gmClass="button" onClick="fnPrintVer();" tabindex="13" buttonType="Load" />&nbsp;&nbsp;
							<fmtItemConsignEdit:message key="BTN_PACKING_SLIP" var="varPackingSlip"/>
							<gmjsp:button value="${varPackingSlip}" gmClass="button" onClick="fnPackslip();" tabindex="13" buttonType="Save" /><BR><BR>
						</td>
					</tr>
<%
					}
%>
				 </table>
			 </td>
		</tr>
		<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>	
		<tr>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
			<td colspan="3">
					<jsp:include page="/operations/itemcontrol/GmValidationDisplayInclude.jsp">
					<jsp:param value="<%=strErrorCnt%>" name="errCnt"/>
					</jsp:include>
			</td>
		</tr>
    </table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
