
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtRepAccountInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- \custservice\GmRepAccountInfo.jsp -->
<fmtRepAccountInfo:setLocale value="<%=strLocale%>"/>
<fmtRepAccountInfo:setBundle basename="properties.labels.custservice.GmAccountMappingSetup"/>
<%
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Rep Account Information Associated with Parent Account</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmAccountPriceReport.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmParentRepInfo.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />

<%

String strRepAccID = GmCommonClass.parseNull((String)request.getParameter("repAccountId"));
%>
<script>
var repAccID = '<%=strRepAccID%>';

function fnFetchRepAcc(){
	fnParentAcInfo(repAccID,'N');
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnFetchRepAcc();">


	<table class="DtTable900" cellspacing="0" cellpadding="0">		
		<tr  class="Shade">
			<td colspan="2">
				<div id="parentDiv"></div>
			</td>
		</tr>
		
		<tr>
        <td align="center" height="30">&nbsp;
        <fmtRepAccountInfo:message key="BTN_CLOSE" var="varClose"/>
			<gmjsp:button value="${varClose}"  name="Btn_Close"  gmClass="Button" buttonType="Load"  onClick="window.close();" />&nbsp;
	    </td>
	</tr>	

</table>
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
