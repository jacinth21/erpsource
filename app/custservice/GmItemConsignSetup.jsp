
<%
/**********************************************************************************
 * File		 		: GmItemConsignSetup.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>


<%@ taglib prefix="fmtItemConsignSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- custservice\GmItemConsignSetup.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtItemConsignSetup:setLocale value="<%=strLocale%>"/>
<fmtItemConsignSetup:setBundle basename="properties.labels.custservice.GmItemConsignSetup"/>


<%
try {
	GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmItemConsignSetup", strSessCompanyLocale);
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	strCssPath = GmCommonClass.getString("GMSTYLES");
	strImagePath = GmCommonClass.getString("GMIMAGES");
	strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	strCommonPath = GmCommonClass.getString("GMCOMMON");
	String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");

	HashMap hmReturn = (HashMap)session.getAttribute("hmReturn");
	String strhAction = GmCommonClass.parseNull((String)session.getAttribute("hAction"));
	String strSource = GmCommonClass.parseNull((String)session.getAttribute("source"));
	String strFieldStatus = GmCommonClass.parseNull((String)session.getAttribute("fieldStatus"));
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strTemp = "";

	String strConsignId = (String)session.getAttribute("CONSIGNID") == null?"":(String)session.getAttribute("CONSIGNID");
	String strSetName = "";
	String strAccName = "";
	String strUserName = "";
	String strIniDate = "";
	String strItemQty = "";
	String strControl = "";
	String strPartNums = (String)session.getAttribute("strPartNums") == null?"":(String)session.getAttribute("strPartNums");
	String strNewPartNums = "";
	String strPartDesc = "";
	String strFlag = ""; 
	
	String strType = "";
	String strDistribAccId = "";
	String strPurpose = "";
	String strBillTo = "";
	String strShipTo = "";
	String strShipToId = "";
	String strFinalComments = "";
	String strShipFl = "";
	String strDelFl = "";
	
	String strPageTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PROCESS_CONSIGNMENT_ITEMS"));
	int listSize =2;
	String strColumnName = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_INSHELF"));


	ArrayList alType = new ArrayList();
	ArrayList alShipTo = new ArrayList();
	ArrayList alPurpose = new ArrayList();
	ArrayList alDistributor = new ArrayList();
	ArrayList alRepList = new ArrayList();
	ArrayList alAccList = new ArrayList();

	HashMap hmConsignDetails = new HashMap();
	HashMap hmTemp = new HashMap();
	HashMap hmParam =  new HashMap();
	HashMap hmOtherDetails =  new HashMap();
	
	boolean bolFl0 = false;

	int intPriceSize = 0;

	if (hmReturn != null)
	{
		hmTemp = (HashMap)hmReturn.get("CONLISTS");
		alType = (ArrayList)hmTemp.get("CONTYPE");
		alShipTo = (ArrayList)hmTemp.get("SHIPTO");	
		alPurpose = (ArrayList)hmTemp.get("PURPOSE");	
		alDistributor = (ArrayList)hmTemp.get("DISTRIBUTORLIST");	
		alRepList = (ArrayList)hmTemp.get("REPLIST");	
		alAccList = (ArrayList)hmTemp.get("ACCLIST");	
	}
	hmParam = (HashMap)session.getAttribute("PARAM");

	if (hmParam != null)
	{
	  
		strType = (String)hmParam.get("TYPE");
		if (strType != "")
		{
			bolFl0 = true;
		}
		strPurpose = GmCommonClass.parseNull((String)hmParam.get("PURPOSE"));
		strBillTo = GmCommonClass.parseNull((String)hmParam.get("BILLTO"));
		strShipTo = GmCommonClass.parseNull((String)hmParam.get("SHIPTO"));
		strShipToId = GmCommonClass.parseNull((String)hmParam.get("SHIPTOID"));
		strFinalComments = GmCommonClass.parseNull((String)hmParam.get("COMMENTS"));
		strShipFl = GmCommonClass.parseNull((String)hmParam.get("SHIPFL"));
		strChecked = GmCommonClass.parseNull(strShipFl.equals("1")?"checked":"");
		strDelFl = GmCommonClass.parseNull((String)hmParam.get("SHIPFL"));
		

	}
if(strSource.equals("Dummy")){
strPageTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PROCESS_DUMMY_CONSIGNMENT"));
listSize = 1;
strColumnName="";
}

	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Item Consign </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>

var lblType = '<fmtItemConsignSetup:message key="LBL_TYPE"/>';
var lblBillTo = '<fmtItemConsignSetup:message key="LBL_BLL_TO"/>';
var lblShipTo = '<fmtItemConsignSetup:message key="LBL_SHIP_TO"/>';
var lblValues = '<fmtItemConsignSetup:message key="LBL_VALUES"/>';

function fnSubmit()
{
	document.frmVendor.hAction.value = 'Save';
  	document.frmVendor.submit();
}

function fnInitiate()
{
	Error_Clear();
	fnValidateDropDn('Cbo_Type',lblType);
	fnValidateDropDn('Cbo_BillTo',lblBillTo);
	fnValidateDropDn('Cbo_ShipTo',lblShipTo);
	fnValidateDropDn('Cbo_Values',lblValues);
	if (ErrorCount > 0)
	{
		Error_Show();
		return false;
	}else{
		enableVariablesForPosting();
		document.frmVendor.hAction.value = 'EditLoad';
	  	document.frmVendor.submit();
  	}
  	
}
function enableVariablesForPosting()
{
if(document.frmVendor.Cbo_Type.disabled == true || document.frmVendor.Cbo_ShipTo.disabled == true || document.frmVendor.Chk_ShipFlag.disabled == true)
 {
	document.frmVendor.Cbo_Type.disabled = false;
	document.frmVendor.Cbo_ShipTo.disabled = false;
	document.frmVendor.Chk_ShipFlag.disabled = false;    
 } 
}
function fnAddToCart()
{
enableVariablesForPosting();
document.frmVendor.hAction.value = "GoCart";
document.frmVendor.submit();
}

function fnRemoveItem(val)
{
	document.frmVendor.hDelPartNum.value = val;
	document.frmVendor.hAction.value = "RemoveCart";
	document.frmVendor.submit();	
}

function fnUpdateCart()
{
	enableVariablesForPosting();
	document.frmVendor.hAction.value = "UpdateCart";
	document.frmVendor.submit();	
}

function fnPlaceOrder(val)
{
	Error_Clear();
	fnValidateDropDn('Cbo_Type',lblType);
	fnValidateDropDn('Cbo_BillTo',lblBillTo);
	fnValidateDropDn('Cbo_ShipTo',lblShipTo);
	fnValidateDropDn('Cbo_Values',lblValues);
	var arr = document.frmVendor.hPartNums.value.split(',');
	var pnum = '';
	var stock = '';
	var qty = '';
	var obj = '';
	var cnum = '';
	var strErrorMore = '';
	var strErrorQty= '';
	var strErrorNegitive = '';
	var j = 0;
	if(val != 'Dummy') {
    for(var i=0;i<arr.length;i++)
    {
        qid = arr[i];
        j++;
		obj = eval("document.frmVendor.hStock"+j);
		stock = parseInt(obj.value);
		obj = eval("document.frmVendor.Txt_Qty"+j);
		qty = parseInt(obj.value);
		var qtyVal = obj.value;
		if(qty == 0 || qty == ''){
			strErrorQty = strErrorQty +","+ qid; 
		}else if(qty < 0 || isNaN(qtyVal))
		{
			strErrorNegitive = strErrorNegitive +","+ qid; 
		}
		if (qty > stock)
		{
			strErrorMore  = strErrorMore + ","+ qid;
		}
		
		
	}
 }
	
	if (strErrorNegitive != '')
	{
		Error_Details(message_operations[147]+strErrorNegitive.substr(1,strErrorNegitive.length));
	}

	if(strErrorQty != ''){
    	Error_Details(message_operations[586]"+strErrorQty.substr(1,strErrorQty.length));
    } 
	
	if (strErrorMore != '')
	{
		Error_Details(message_operations[711]+strErrorMore.substr(1,strErrorMore.length));
	}
	
		if (ErrorCount > 0)
		{
			Error_Show();
			return false;
		}else{
			enableVariablesForPosting();
			document.frmVendor.hAction.value = 'PlaceOrder';
		  	document.frmVendor.submit();
	  	}

}

function fnPrintVer()
{
windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId=<%=strConsignId%>","Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnAgree()
{
windowOpener("/GmConsignSetServlet?hId=<%=strConsignId%>&hAction=Ack&","Ack","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");

}

function fnPicSlip()
{
	val = document.frmVendor.hConsignId.value;	windowOpener("<%=strServletPath%>/GmConsignItemServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=670,height=500");
}


function fnCallShip()
{
	obj = document.frmVendor.Cbo_ShipTo;
	var obj2 = document.frmVendor.Cbo_Values;
	if (obj.options[obj.selectedIndex].text == 'Sales Rep')
	{
		obj2.disabled = false;
		obj2.options.length = 0;
		obj2.options[0] = new Option("[Choose One]","0");
		for (var i=0;i<RepLen;i++)
		{
			arr = eval("RepArr"+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			obj2.options[i+1] = new Option(name,id);
		}
		obj2.focus();
	}
	else if (obj.options[obj.selectedIndex].text == 'Hospital')
	{
		obj2.disabled = false;
		obj2.options.length = 0;
		obj2.options[0] = new Option("[Choose One]","0");
		for (var i=0;i<AccLen;i++)
		{
			arr = eval("AccArr"+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			obj2.options[i+1] = new Option(name,id);
		}
		obj2.focus();
	}
	else if (obj.options[obj.selectedIndex].text == 'Employee')
	{
		obj2.disabled = false;
		obj2.options.length = 0;
		obj2.options[0] = new Option("[Choose One]","0");
		for (var i=0;i<EmpLen;i++)
		{
			arr = eval("EmpArr"+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			obj2.options[i+1] = new Option(name,id);
		}
		obj2.focus();
	}
	else
	{
		obj2.options.length = 0;
		obj2.options[0] = new Option("[Choose One]","0");
		obj2.disabled = true;
	}
}

var RepLen = <%=alRepList.size()%>;
<%
	intSize = alRepList.size();
	hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alRepList.get(i);
%>
	var RepArr<%=i%> ="<%=hcboVal.get("REPID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>

var AccLen = <%=alAccList.size()%>;
<%
	intSize = alAccList.size();
	hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alAccList.get(i);
%>
	var AccArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>

function fnLoad()
{
	var InHouseFl = '<%=strType%>';
	var ShipToFl = '<%=strShipTo%>';
	
	if (InHouseFl == '4112')
	{
		document.frmVendor.Cbo_Purpose.disabled = false;
		document.frmVendor.Cbo_Values.disabled = false;
		document.frmVendor.Cbo_BillTo.selectedIndex = 0;
		fnSetValue();
	}
	if (ShipToFl != '4120')
	{
		fnCallShip();
		fnSetValue();
	}
}

function fnSetValue()
{
	var ShipToId = '<%=strShipToId%>';
	var obj5 = document.frmVendor.Cbo_Values;
	for (var j=0;j<obj5.length;j++)
	{
		if (obj5.options[j].value == ShipToId)
		{
			obj5.options[j].selected = true;
			break;
		}
	}
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmConsignItemServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hConsignId" value="<%=strConsignId%>">
<input type="hidden" name="hDelPartNum" value="">
<input type="hidden" name="hMode" value="">

	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
			<td height="25" class="RightDashBoardHeader"><%=strPageTitle%></td>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
		</tr>
				
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
<%
		if (!strhAction.equals("Load"))
		{
%>
					<tr class="shade">
						<td class="RightText" HEIGHT="20" width="200" align="right">&nbsp;<fmtItemConsignSetup:message key="LBL_CONSIGNMENT_ID"/>:</td>
						<td class="RightText" width="498">&nbsp;<%=strConsignId%></td>
					</tr>
<%
		}
%>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtItemConsignSetup:message key="LBL_TYPE"/>:</td>
						<td class="RightText">&nbsp;<select <%=strFieldStatus%> name="Cbo_Type" id="Region" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtItemConsignSetup:message key="LBL_CHOOSE_ONE"/>
<%  		
			  		for (int i=0;i<listSize;i++) // hardcoding to 2 to remove the In-House and following values
			  		{
			  			hcboVal = new HashMap();          
			  			hcboVal = (HashMap)alType.get(i);
			  			strCodeID =(String)hcboVal.get("CODEID");
						strSelected = strType.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
                     }
%>							
							</select>
						</td>
					</tr>
					<tr class="shade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtItemConsignSetup:message key="LBL_BLL_TO"/>:</td>
						<td>&nbsp;<select name="Cbo_BillTo" id="Region" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtItemConsignSetup:message key="LBL_CHOOSE_ONE"/>[Choose One]
<%
			  		intSize = alDistributor.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alDistributor.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strBillTo.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%
			  		}
%>
							</select>
						</td>
					</tr>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtItemConsignSetup:message key="LBL_SHIP_TO"/>:</td>
						<td class="RightText">&nbsp;<select <%=strFieldStatus%> name="Cbo_ShipTo" id="Region" class="RightText" tabindex="2" onChange="javascript:fnCallShip();" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtItemConsignSetup:message key="LBL_CHOOSE_ONE"/>
<%
			  		intSize = alShipTo.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<3;i++) // hardcoding to 3 to remove Employee as Ship To value -- cust service cannot use InHouse consignment from this screen
			  		{
			  			hcboVal = (HashMap)alShipTo.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strShipTo.equals(strCodeID)?"selected":"";					
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
							</select>&nbsp;&nbsp;&nbsp;&nbsp; <fmtItemConsignSetup:message key="LBL_VALUES"/>:<select name="Cbo_Values" id="Region" class="RightText"  disabled tabindex="3" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtItemConsignSetup:message key="LBL_CHOOSE_ONE"/>
						</select>
						</td>
					</tr>
					<tr class="shade">
						<td class="RightText" valign="top" align="right" HEIGHT="24"><fmtItemConsignSetup:message key="LBL_COMMENTS"/>:</td>
						<td>&nbsp;<textarea name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1 rows=5 cols=40 value="<%=strFinalComments%>"><%=strFinalComments%></textarea></td>
					</tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24"><fmtItemConsignSetup:message key="LBL_SHIPPING_REQUIRED"/> ?:</td>
						<td><input <%=strFieldStatus%> type="checkbox" name="Chk_ShipFlag" onFocus="changeBgColor(this,'#AACCE8');" <%=strChecked%> onBlur="changeBgColor(this,'#ffffff');" tabindex=2></td>
					</tr>
<%
		if (!strhAction.equals("Load") && !strhAction.equals("OrdPlaced"))
		{
%>					
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr>
						<td height="30" align="center" colspan="2" class="RightTextBlue"><br><b><fmtItemConsignSetup:message key="LBL_SHOPPING_CART"/></b><br><fmtItemConsignSetup:message key="LBL_ENTER_PART#_CLICK_GO"/><br>&nbsp;&nbsp;&nbsp;<input type="text" size="40" value="" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
						<fmtItemConsignSetup:message key="BTN_GO" var="varGo"/><gmjsp:button value="&nbsp;${varGo}&nbsp;" name="Btn_GoCart" gmClass="button" onClick="fnAddToCart();" tabindex="16" buttonType="Load" /><br><br>
						</td>
					</tr>
<%
		}
		if (!strhAction.equals("Load"))
		{
%>	

					<tr>
						<td align="center" colspan="2" valign="top">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td align="center" width="80" height="25"><fmtItemConsignSetup:message key="LBL_PART#"/></td>
									<td align="center" width="200"><fmtItemConsignSetup:message key="LBL_DESCRIPTION"/></td>
									<td align="center" width="70"><%=strColumnName%></td>
									<td align="center" width="80"><fmtItemConsignSetup:message key="LBL_PRICE"/></td>
									<td align="center" width="80"><fmtItemConsignSetup:message key="LBL_QUANTITY"/></td>
									<td align="center" width="100"><fmtItemConsignSetup:message key="LBL_AMOUNT"/></td>
								</tr>
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
<%
			String strTotal = "";
			if (!strPartNums.equals(""))
			{
					hmReturn = (HashMap)session.getAttribute("hmOrder");
					HashMap hmCart = (HashMap)session.getAttribute("hmCart");
					java.util.StringTokenizer strTok = new java.util.StringTokenizer(strPartNums,",");
					String strPartNum = "";
					HashMap hmVal = new HashMap();
					HashMap hmLoop = new HashMap();
					String strId = "";
					String strDesc = "";
					String strPrice = "";

					String strAmount = "";
					String strQty = "1";
					String strStock = "";
					String strUnPartNums = "";
					double dbPrice = 0.0;
					double dbDiscount = 0.0;
					double dbDiscPrice = 0.0;
					String strDiscPrice = "";
					String strShade = "";
					int i=0;
					double dbAmt = 0.0;
					double dbTotal = 0.0;
					boolean bolFl = false;
					boolean bolFl2 = false;

					int intQty = 0;
					int intShelfQty = 0;
					if (strTok.countTokens() == 1)
					{
						bolFl2 = true;
					}
					while (strTok.hasMoreTokens())
					{
						i++;
						strPartNum 	= strTok.nextToken();
						if (hmCart != null)
						{
							hmLoop = (HashMap)hmCart.get(strPartNum);
							if (hmLoop != null)
							{
								strQty = (String)hmLoop.get("QTY");
							}
							else
							{
								strQty = "1";
							}
						}

						strQty = strQty == null?"1":strQty;
						intQty = Integer.parseInt(strQty);
						hmVal = (HashMap)hmReturn.get(strPartNum);
						if (hmVal.isEmpty())
						{
							//bolFl = false;
						}
						else if (hmVal.isEmpty() && bolFl2)
						{
							bolFl = false;
						}
						else
						{
							strNewPartNums = strNewPartNums + strPartNum + ",";
							bolFl = true;
							strId = (String)hmVal.get("ID");
							strDesc = (String)hmVal.get("PDESC");
							strPrice = (String)hmVal.get("PRICE");

							strStock = (String)hmVal.get("STOCK");
							intShelfQty = Integer.parseInt(strStock);
							strStock = intShelfQty < 0?"0":strStock;

							if (strhAction.equals("OrdPlaced"))
							{
								int intStock = Integer.parseInt(strStock);
								intStock = intStock - intQty; 
								strStock = "" + intStock;
							}

							dbPrice = Double.parseDouble(strPrice);
							dbAmt = intQty * dbPrice; // Multiply by Qty
							strDiscPrice = String.valueOf(dbPrice);
							strAmount = String.valueOf(dbAmt);
							dbTotal = dbTotal + dbAmt;
							strShade	= (i%2 == 0)?"class=Shade":""; //For alternate Shading of rows

%>
								<tr <%=strShade%>>
									<td class="RightText" align="center" height="20">
<%
							if (!strhAction.equals("OrdPlaced"))
							{
%>									
									<a  href="javascript:fnRemoveItem('<%=strId%>');"><fmtItemConsignSetup:message key="IMG_ALT_REMOVE" var="varRemove"/><img border="0" Alt='${varRemove}' valign="bottom" src="<%=strImagePath%>/btn_remove.gif" height="15" width="15"></a><%}%>&nbsp;<%=strId%></td>
									<td class="RightText"><%=strDesc%><input type="hidden" name="hStock<%=i%>" value="<%=strStock%>"></td>									
<%									if(!strSource.equals("Dummy")){							
%>									
									<td class="RightText" align="center"><%=strStock%></td>
<%
									 } else {
%>									
									<td align="center">&nbsp;</td>
<%
									 }
%>									
									<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strDiscPrice)%>&nbsp;
									<input type="hidden" name="hPrice<%=i%>" value="<%=strDiscPrice%>">
									</td>
									<td class="RightText" align="center">
<%
							if (!strhAction.equals("OrdPlaced"))
							{
%>
										&nbsp;<input type="text" size="5" value="<%=strQty%>" name="Txt_Qty<%=i%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
<%
							}else{	
%>
										<%=strQty%>
<%							}%>
									</td>
									<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strAmount)%>&nbsp;</td>
								</tr>
<%
						strTotal = ""+dbTotal;
						}
					}
						if (!strNewPartNums.equals(""))
						{
							strNewPartNums = strNewPartNums.substring(0,strNewPartNums.length()-1);
						}
						if (bolFl)
						{
%>
								<tr>
									<td colspan="6" height="1" bgcolor="#666666"></td>
								</tr>
								<tr class="RightTableCaption">
									<td colspan="4" align="right" height="23">
									<input type="hidden" name="hTotal" value="<%=strTotal%>">
									</td>
									<td align="center">Total</td>
									<td align="right">$<%=GmCommonClass.getStringWithCommas(strTotal)%>&nbsp;</td>
								</tr>	
<%
						}						

						if (!bolFl)
						{
%>
								<tr>
									<td colspan="6" height="30" class="RightTextBlue" align="center"><fmtItemConsignSetup:message key="LBL_NO_ITEM"/></td>
								</tr>
<%
						}
}else{ %>
								<tr>
									<td colspan="6" height="30" class="RightTextBlue" align="center"><fmtItemConsignSetup:message key="LBL_NO_ITEM"/></td>
								</tr>
<%
			}
		}
%>
							</table><input type="hidden" name="hPartNums" value="<%=strNewPartNums%>">
						</td>
					</tr>
<%
		if (strhAction.equals("Load"))
		{
%>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>					
					<tr>
						<td colspan="2" height="30" align="center">
							<fmtItemConsignSetup:message key="BTN_INITIATE" var="varInitiate"/>
							<gmjsp:button value="${varInitiate}" gmClass="button" onClick="fnInitiate();" tabindex="13" buttonType="Save" />
						</td>
					</tr>
<%
		}else if (strhAction.equals("PopOrd")){
%>
		<tr>
			<td colspan="3" align="center" height="30">
				<fmtItemConsignSetup:message key="BTN_UPDATE" var="varUpdate"/>
				<gmjsp:button value="&nbsp;${varUpdate}&nbsp;" name="Btn_UpdateQty" gmClass="button" onClick="fnUpdateCart();" buttonType="Save" />
				&nbsp;&nbsp;
				<fmtItemConsignSetup:message key="BTN_PLACEORDER" var="varPlaceOrder"/>
				<gmjsp:button value="&nbsp;${varPlaceOrder}&nbsp;" name="Btn_PlaceOrd" gmClass="button" onClick="fnPlaceOrder('<%=strSource%>');" buttonType="Save" />
			</td>
		<tr>
<%	
	}else if (strhAction.equals("OrdPlaced") && ! strSource.equals("Dummy"))
	{
%>
					<tr>
						<td colspan="2" height="30" align="center">
							<fmtItemConsignSetup:message key="BTN_PICSLIP" var="varPicSlip"/>
							<gmjsp:button value="${varPicSlip}" gmClass="button" onClick="fnPicSlip();" tabindex="13" buttonType="Save" />&nbsp;&nbsp;
						</td>
					</tr>
<%
	}								
%>
				</table>
			 </td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
    </table>
<%--
<script>
<%
	if (strhAction.equals("OrdPlaced") && ! strSource.equals("Dummy"))
	{
%>
	alert("Your consignment has been placed. Please print PIC Slip");
<%
	}
%>
</script>
--%>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
