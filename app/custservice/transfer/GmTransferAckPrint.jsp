
 <%
/**********************************************************************************
 * File		 		: GmTransferAckPrint.jsp
 * Desc		 		: This screen is used for the Acknowledgment form for Transfer transactions
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>

<!-- Imports for Logger -->

<%@ taglib prefix="fmtTransferAckPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \CS Label changes\custservice\transfer\GmTransferAckPrint.jsp -->
<fmtTransferAckPrint:setLocale value="<%=strLocale%>"/>
<fmtTransferAckPrint:setBundle basename="properties.labels.custservice.transfer.GmTransferAckPrint"/>
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strApplDateFmt = strGCompDateFmt;
	
	HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(gmDataStoreVO.getCmpid(), "2000");
	//To get the address from constant.properties
	String strCompanyName = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMPNAME"));
	String strAddress = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMPADDRESS"));
	String strGMphone = GmCommonClass.parseNull((String)hmCompanyAddress.get("PH"));
	String strGMFax = GmCommonClass.parseNull((String)hmCompanyAddress.get("FAX"));
	if(!strAddress.equals("")){
		strAddress = strAddress.replaceAll("/", "<br>");
	}
	strAddress = strCompanyName + "<br>" + strAddress;
	strAddress = (!strGMphone.equals(""))?strAddress+"<br><br>"+strGMphone:strAddress;
	strAddress = (!strGMFax.equals(""))?strAddress+"<br>"+strGMFax:strAddress;

	String strCompanyLogo = GmCommonClass.parseNull((String)hmCompanyAddress.get("LOGO"));
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("HMACKTRANSFER");
	String strFromName = "";
	String strToName = "";
	String strTsfDate = "";
	String strNextDate = "";
	String strTsId = "";
	String strFormatDate = "";

		log.debug(" hmReturn " + hmReturn);
		
	if (hmReturn != null)
	{
		strFromName = (String)hmReturn.get("FROMNAME");
		strToName = (String)hmReturn.get("TONAME");
		strTsfDate = GmCommonClass.getStringFromDate((java.util.Date)hmReturn.get("TSFDATE"),strApplDateFmt);
		strNextDate = GmCommonClass.getStringFromDate((java.util.Date)hmReturn.get("NDATE"),strApplDateFmt);
		strTsId= (String)hmReturn.get("TSFID");
		strFormatDate = GmCommonClass.getFormattedDate(strTsfDate, strApplDateFmt,"EEEE, MMMM d, yyyy");
		log.debug(" Formatted date " + strFormatDate);
	}


%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Transfer Acknowledgement Form </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">

<script>
function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
</HEAD>

<BODY topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<BR><BR><BR><BR>
	<table border="0" width="700" cellspacing="0" cellpadding="0" align=center>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="6"></td>
			<td bgcolor="#666666" colspan="3"></td>
			<td bgcolor="#666666" width="1" rowspan="6"></td>
		</tr>
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td width="170"><img src="<%=strImagePath%>/<%=strCompanyLogo%>.gif" width="138" height="60"></td>
						<td class="RightText" align="right" width="100%"><%=strAddress %></td>
						<td width="20">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td bgcolor="#666666"></td></tr>
		<tr>
			<td width="698" height="70" valign="top" align="center" class="RightText"><BR><font size="+1">
<fmtTransferAckPrint:message key="LBL_TRANSFER_ACKNOWLEDGEMENT_FORM"/></font><BR><BR></td>
		</tr>
		<tr>
			<td width="690" height="80" valign="top" class="RightText">
				&nbsp;On <b><%=strFormatDate%></b>, <b><%=strFromName%></b>.<fmtTransferAckPrint:message key="LBL_HAS_TRANSFERRED_TO"/>  <b><%=strToName%></b> <fmtTransferAckPrint:message key="LBL_THE_ITEMS_LISTED"/>
				<BR><BR>&nbsp;<fmtTransferAckPrint:message key="LBL_ON_TRANSFER_NUMBER"/> <b><%=strTsId%></b> <fmtTransferAckPrint:message key="LBL_ATTACHED_HERETO"/>
				<BR><BR><BR>
				&nbsp;<b><%=strToName%></b> <fmtTransferAckPrint:message key="LBL_ACKNOWLEDGES_ITEMS_LISTED_CONSIGNMNET_INVENTORY"/> <BR><BR>
				&nbsp;<fmtTransferAckPrint:message key="LBL_FORMENTIONED_TRANSFER_INVOICE_ARE_PROPERTY_OF_GLOBUS_MEDICAL_INC"/> <BR><BR>&nbsp;<fmtTransferAckPrint:message key="LBL_HANDLING_TRANSFER_ITEMS_GOVERNED_BY_EXCLUSIVE_AGENCY_AGREEMENT"/><Br><br><Br>
				&nbsp;<b><%=strToName%></b><fmtTransferAckPrint:message key="LBL_HERE_BY_ACKNOWLEDGES"/><BR><BR>
				&nbsp;<fmtTransferAckPrint:message key="LBL_GOOD_WORKING_CONDITION_IN_EVENT_THAT"/> <b><%=strToName%></b><fmtTransferAckPrint:message key="LBL_FAILS_TO_MAINTAIN"/> <BR><BR> <fmtTransferAckPrint:message key="LBL_CONSIGNMNET_INVENTORY_IN_GOOD_WORKING_LIABLE_TO_GLOBUS_MEDICAL_INC"/><BR><BR>&nbsp;<fmtTransferAckPrint:message key="LBL_VALUE_CONSIGNMNET_INVENTORY_LISTED_ON_CONSIGNMENT_INVOICE"/>.
				<BR><BR><BR><BR>
				&nbsp;<fmtTransferAckPrint:message key="LBL_ACKNOWLEDGED_AND_AGREED_BY"/>:
				<BR><BR><BR><BR><BR><BR>
				<table width="100%" border="0">
					<tr>
						<td class="RightText" width="50%">____________________________________________________</td>
						<td width="50%" align="center">____________</td>
					</tr>
					<tr>
						<td class="RightText" align="center" width="50%"><fmtTransferAckPrint:message key="LBL_NAME"/></td>
						<td class="RightText" align="center" width="50%"><fmtTransferAckPrint:message key="LBL_DATE"/></td>
					</tr>
				</table>
				
				<BR><BR><BR><BR><BR>
				
			</td>
		</tr>
		<tr><td bgcolor="#666666" height="1"></td></tr>
    </table>

<p STYLE="page-break-after: always"></p>
<BR><BR><BR><BR>
	<table border="0" width="700" cellspacing="0" cellpadding="0" align=center>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="6"></td>
			<td bgcolor="#666666" colspan="3"></td>
			<td bgcolor="#666666" width="1" rowspan="6"></td>
		</tr>
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td width="170"><img src="<%=strImagePath%>/<%=strCompanyLogo%>.gif"></td>
						<td class="RightText" align="right" width="100%"><%=strAddress %></td>
						<td width="20">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td bgcolor="#666666"></td></tr>
		<tr>
			<td width="698" height="70" valign="top" align="center" class="RightText"><BR><font size="+1"> <fmtTransferAckPrint:message key="LBL_ACKNOWLEDGEMENT_OF_RECEIPT"/></font><BR><BR></td>
		</tr>
		<tr>
			<td width="690" height="80" valign="top" class="RightText">
				&nbsp;<fmtTransferAckPrint:message key="LBL_UNDERSIGNED_ACKNOWLEDGES_THE_RECEIPT_ITEMS_LISTED_ON_GLOBUS_MEDICAL_INC_TRANSFER"/><BR><BR>
				&nbsp;<fmtTransferAckPrint:message key="LBL_NUMBER"/> <b><%=strTsId%></b> <fmtTransferAckPrint:message key="LBL_ATTACTHED_HERTO"/>.
				<BR><BR><BR>
				&nbsp;<fmtTransferAckPrint:message key="LBL_ITEMS_LISTED_ON_TRANSFER_INVOICE_NUMBER"/> <b><%=strTsId%></b><fmtTransferAckPrint:message key="LBL_ARE_THE_PROPERTY_OF"/><b><fmtTransferAckPrint:message key="LBL_GLOBUS_MEDICAL_INC"/></b>.<BR><BR>
			
				&nbsp;<fmtTransferAckPrint:message key="LBL_TRANSFER_ACKNOWLEDGEMENT_FORM_SIGNED_BY_AN_AUTHORIZED_INDIVIDUAL_FROM"/> <b><%=strToName%></b> <BR><BR>
				&nbsp;on <b><%=strFormatDate%></b> <fmtTransferAckPrint:message key="LBL_GOVERNS_HANDLING_RESPONSIBILITY_FOR_ITEMS_TRANSFERRED_FROM"/> <b><%=strFromName%> .</b>
				<BR><BR><BR><BR>
				&nbsp;<fmtTransferAckPrint:message key="LBL_ACKNOWLEDGED_AND_AGREED_BY"/>:
				<BR><BR><BR><BR><BR><BR>
				<table width="100%" border="0">
					<tr>
						<td class="RightText" width="50%">____________________________________________________</td>
						<td width="50%" align="center">____________</td>
					</tr>
					<tr>
						<td class="RightText" align="center" width="50%"><fmtTransferAckPrint:message key="LBL_NAME"/></td>
						<td class="RightText" align="center" width="50%"><fmtTransferAckPrint:message key="LBL_DATE"/></td>
					</tr>
				</table>
				
				<BR><BR><BR><BR><BR>
				
			</td>
		</tr>
		<tr><td bgcolor="#666666" height="1"></td></tr>
    </table>


	<table border="0" width="700" cellspacing="0" cellpadding="0" align=center>
		<tr>
			<td align="center" height="30" id="button">
			<fmtTransferAckPrint:message key="LBL_PRINT" var="varPrint"/>
				<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrint();" buttonType="Load"  />&nbsp;&nbsp;
				<fmtTransferAckPrint:message key="LBL_CLOSE" var="varClose"/>
				<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" />
			</td>
		</tr>
	</table>

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>