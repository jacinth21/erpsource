<%
/**********************************************************************************
 * File		 		     : GmTransferReport.jsp
 * Desc		 		: This screen is a header include jsp for transfer transactions.
 *								Contains transfer type, From and To type and the reason for transfer
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- \custservice\transfer\GmTransferReport.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.Date"%>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<!-- Imports for Logger -->


<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ page buffer="16kb" autoFlush="true" %>

<%@ taglib prefix="fmtTransRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtTransRpt:setLocale value="<%=strLocale%>"/>
<fmtTransRpt:setBundle basename="properties.labels.custservice.transfer.GmTransferReport"/>


<%
	String strWikiTitle = GmCommonClass.getWikiTitle("TRANSFER_REPORT");


	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmParam = new HashMap();
	
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	strCountryCode = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("COUNTRYCODE"));
	ArrayList alDistributorList = (ArrayList)request.getAttribute("ALDISTLIST");
	ArrayList alEmployeeList = (ArrayList)request.getAttribute("ALEMPLIST");
	ArrayList alAccountList = (ArrayList)request.getAttribute("ALACCLIST");
	
	ArrayList alTransferType =  (ArrayList)request.getAttribute("ALTSFTP");
	// ArrayList alTransferFrom = alEmployeeList;
	 ArrayList alTransferFrom = alDistributorList;
	ArrayList alTransferTo = alDistributorList;
	ArrayList alTransferReason  =  (ArrayList)request.getAttribute("ALTSFRN");
	ArrayList alTransferReasonAC  =  (ArrayList)request.getAttribute("ALTSFRNAC");
	
	hmParam = (HashMap)request.getAttribute("HMPARAM");
	
	String strSessApplDateFmt = strGCompDateFmt;
	String strDateFmt = "{0,date,"+strSessApplDateFmt+"}";
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("ACTION"));
	String strTransferType = "";
	String strTransferFrom = "";
	String strTransferTo = "";
	String strTransferReason = "";
	String strFromDate = GmCommonClass.parseNull((String)request.getAttribute("FROMDATE"));
	String strToDate = GmCommonClass.parseNull((String)request.getAttribute("TODATE"));
	String strMissingDist = GmCommonClass.parseNull((String)request.getAttribute("MISSINGDIST"));
	String strStatus = "0";
	String strCNID = "";
	//For autocomplete changes
	String strMethodNm = "";
	String strTransferFromNm = "";
	String  strTransferFm = "";
	String strOnBlurfn = "";
	String strTransferToNm="";
	String strTransferToAc="";
	
	Date dtFromDate = null;
	Date dtToDate = null;
	if (hmParam != null)
	{
         strTransferType = GmCommonClass.parseNull((String)hmParam.get("TRANSFERTYPE"));
         strTransferFrom = GmCommonClass.parseNull((String)hmParam.get("TRANSFERFROM"));    
         strTransferTo = GmCommonClass.parseNull((String)hmParam.get("TRANSFERTO"));
         strTransferReason = GmCommonClass.parseNull((String)hmParam.get("TRANSFERREASON"));
         strFromDate = GmCommonClass.parseNull((String)hmParam.get("FROMDATE"));
         strToDate = GmCommonClass.parseNull((String)hmParam.get("TODATE"));
         strStatus = GmCommonClass.parseNull((String)hmParam.get("STATUS"));
         strCNID = GmCommonClass.parseNull((String)hmParam.get("CNID"));
         
         //For Autocomplete changes
         strTransferFm = GmCommonClass.parseNull((String)hmParam.get("TRANSFERFM"));
         strTransferToAc = GmCommonClass.parseNull((String)hmParam.get("TRANSFERTOAC"));
           
         dtFromDate = GmCommonClass.getStringToDate(strFromDate,strSessApplDateFmt);
         dtToDate = GmCommonClass.getStringToDate(strToDate,strSessApplDateFmt);
         if(strTransferType.equals("26240577")){
        	 strMethodNm = "loadAccountList";
        	 strOnBlurfn = "fnchkDist()";
         }else if(strTransferType.equals("90300") || strTransferType.equals("90301") ||strTransferType.equals("26240616")||strTransferType.equals("26240615")){
        	 strMethodNm = "loadFieldSalesList";
        	 strOnBlurfn = "fnDisplayAccID(this)";
         }     
	}
	
	
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Transfer Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<script>
var strMissingDist = '<%=strMissingDist%>';
var vCountryCode = '<%=strCountryCode%>';
var format = '<%=strSessApplDateFmt%>';
var gridObj;
var tfId_row_id='';
var tfId_value='';
function fnChangeType()
{
	var obj = document.frmIncludeTransfer.Cbo_TransferType;
	var obj2 = document.frmIncludeTransfer.Cbo_TransferFrom;
	if (obj.options[obj.selectedIndex].text == '')
	{
		obj2.disabled = false;
		obj2.options.length = 0;
		obj2.options[0] = new Option("[Choose One]","0");
		for (var i=0;i<RepLen;i++)
		{
			arr = eval("RepArr"+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			obj2.options[i+1] = new Option(name,id);
		}
		obj2.focus();
	}
	}
	
	function fnSetFromValues(val)
	{
		 var obj = eval("document.all.div"+val);
		//var objTo = eval("document.all.select"+val);
		var objReason = eval("document.all.reason");
		
		if(val == "26240577") {
			objReason = eval("document.all.reason"+val);
		}
		   var transType = document.frmTransfer.Cbo_TransferType.value;
		   document.frmTransfer.searchCbo_TransferFrom.value="";
		   document.frmTransfer.searchCbo_TransferTo.value="";
		   if(val == "26240615"){
			   document.frmTransfer.searchCbo_TransferTo.value = strMissingDist;
			   document.frmTransfer.Cbo_TransferReason.value = '26240617';
			   document.frmTransfer.hTransferReason.value = '26240617';
		   }else{
			   document.frmTransfer.hTransferReason.value = '0';
		   }if(val == "26240616"){
			   document.frmTransfer.searchCbo_TransferFrom.value = strMissingDist;
		   }
		   fnStartProgress();
		   document.frmTransfer.action = '/GmTransferReportServlet?Cbo_TransferType='+transType;	
		   document.frmTransfer.submit();
	}
	
	function fnSetFromSel(val)
	{
		document.frmTransfer.hTransferFrom.value = val;
	}
	
	function fnSetToSel (val) {
		document.frmTransfer.hTransferTo.value = val;
	}
	
	function fnSetReasonSel (val) {
		document.frmTransfer.hTransferReason.value = val;
	}
	
   function fnGo()
   {
	   var strTrTyp = document.frmTransfer.Cbo_TransferType.value;
	   if (strTrTyp != "0") {
	   if(vCountryCode == 'en'){
   	  		DateValidate(document.frmTransfer.Txt_FromDate,726);
    		DateValidate(document.frmTransfer.Txt_ToDate,726);
	   }else{
		   CommonDateValidation(document.frmTransfer.Txt_FromDate, format, Error_Details_Trans(message[10530],format));
		   CommonDateValidation(document.frmTransfer.Txt_ToDate, format, Error_Details_Trans(message[10530],format));
	   }
	   var fromDate = document.frmTransfer.Txt_FromDate.value;
	   var toDate = document.frmTransfer.Txt_ToDate.value;
	   var transferype = document.frmTransfer.Cbo_TransferType.value;
	   var transferReason = document.frmTransfer.hTransferReason.value;
	   var transferFrom = document.frmTransfer.searchCbo_TransferFrom.value;
	   var transferTo = document.frmTransfer.searchCbo_TransferTo.value;
	   var status = document.frmTransfer.Cbo_Status.value;
	   var cnId = document.frmTransfer.Cbo_CNID.value;
	   var transferFm = document.frmTransfer.Cbo_TransferFrom.value;
	   var transfTo = document.frmTransfer.Cbo_TransferTo.value;
	  
	   var ajaxUrl = fnAjaxURLAppendCmpInfo('/GmTransferReportServlet?hAction=reportTransfer&Txt_FromDate='+fromDate+'&Txt_ToDate='+toDate+
			   '&Cbo_TransferType='+transferype+'&hTransferReason='+transferReason+'&searchCbo_TransferFrom='+transferFrom+
			   '&searchCbo_TransferTo='+transferTo+'&Cbo_Status='+status+'&Cbo_CNID='+cnId+
			   '&Cbo_TransferFrom='+transferFm+'&Cbo_TransferTo='+transfTo+'&ramdomId=' + new Date().getTime());
		dhtmlxAjax.get(ajaxUrl, fnLoadTransferRptDtls)
   	 // document.frmTransfer.hAction.value = 'reportTransfer';
   	  fnStartProgress();
   	 // document.frmTransfer.submit(); 
   	  }
	   else {
		        Error_Details("Please select Transfer Report Type");   
		        if (ErrorCount > 0)
		        {
		        Error_Show();
		        Error_Clear();
		        return false;
		        }

	   }
   }
   
   function fnLoad() {
	   
		val = '<%=strTransferType%>';
		if (val != ' ' && val != '0')
		{
			var obj = eval("document.all.div"+val);
			var objTo = eval("document.all.select"+val);
			//document.all.idFrom.innerHTML = obj.innerHTML;
			//document.all.idTo.innerHTML = objTo.innerHTML;
			
			var objReason = eval("document.all.reason");
			
			if(val == "26240577") {
				objReason = eval("document.all.reason"+val);
			}if(val == "26240615"){
				document.frmTransfer.Cbo_TransferReason.value = '26240617';
				document.frmTransfer.Cbo_TransferReason.disabled = "disabled";
				document.frmTransfer.hTransferReason.value = '26240617';
				document.frmTransfer.searchCbo_TransferTo.value = "Missing";
				document.frmTransfer.searchCbo_TransferTo.disabled = "disabled";
			}else{
				document.frmTransfer.hTransferReason.value = '0';
			}if(val == "26240616"){
				document.frmTransfer.searchCbo_TransferFrom.value = "Missing";
				document.all.searchCbo_TransferFrom.disabled=true;
			} 
			//document.all.idReason.innerHTML = objReason.innerHTML;
		}
		
<%			// To defaul to all order 
		if (strStatus.equals("")) 
		{
			log.debug (" inside ");
			strStatus = "0";
		}
%>
		document.frmTransfer.Cbo_Status.value = '<%=strStatus%>';
}

function fnCallEditTransfer(val)
{
	document.frmTransfer.hTransferId.value = val;
	document.frmTransfer.hAction.value="Load";
	document.frmTransfer.action=	"<%=strServletPath%>/GmAcceptTransferServlet";
	document.frmTransfer.submit();
}
//this function used to generate the DHTMLX grid
function fnLoadTransferRptDtls(loader){
	var response = loader.xmlDoc.responseText;
	if(response !='' && response != 'null' && response != undefined ){
		var setInitWidths = "80,137,120,100,80,100,90,120";
		var setColAlign = "left,left,left,left,left,center,left,left";
		var setColTypes = "tfIdFl,ro,ro,ro,ro,ro,ro,ro";
		var setColSorting = "str,str,str,str,str,str,str,str";
		var setHeader = ["Transfer ID", "From","To","Reason","Type","From Date","Status", "Consignment ID"];
		var setFilter = ["#text_filter", "#text_filter", "#text_filter","#select_filter", "#select_filter","#text_filter","#select_filter","#text_filter"];
		var setColIds = "tsfid,fromname,toid,tsfreason,tsfmode,tsfdate,status,cnid";
		var enableTooltips = "true,true,true,true,true,true,true,true";
		fnStopProgress();
	
	    document.getElementById("dataGridDiv").style.display = "block";
	    document.getElementById("DivNothingMessage").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = "table-row";
	 // split the functions to avoid multiple parameters passing in single
		// function
		gridObj = initGridObject('dataGridDiv');
		gridObj = fnCustomTypes(gridObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips, setFilter,400);
		gridObj = loadDhtmlxGridByJsonData(gridObj, response);
         tfId_row_id=gridObj.getColIndexById("tsfid");
		
		gridObj.attachEvent("onRowSelect", doOnRowSelect);
  			
}else{
	fnStopProgress();
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
		document.getElementById("DivExportExcel").style.display = 'none';
}
}
//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips,setFilter,gridHeight){
		if (setHeader != undefined && setHeader.length > 0) {
		 gObj.setHeader(setHeader);
		 var colCount = setHeader.length;
		 }    

		if (setInitWidths != "")
		gObj.setInitWidths(setInitWidths);

		if (setColAlign != "")
		gObj.setColAlign(setColAlign);

		if (setColTypes != "")
		gObj.setColTypes(setColTypes);

		if (setColSorting != "")
		gObj.setColSorting(setColSorting);

		if (enableTooltips != "")
		gObj.enableTooltips(enableTooltips);

		if (setFilter != "")
		gObj.attachHeader(setFilter);


		if(setColIds != ""){
		 gObj.setColumnIds(setColIds);
		}

		if (gridHeight != "")
		gObj.enableAutoHeight(true, gridHeight, true);
		else
		gObj.enableAutoHeight(true, 400, true);

		
	return gObj;
}
//This Function Used to download Excel Sheet
function fnExport() {
  gridObj.toExcel('/phpapp/excel/generate.php');
}
//This function used to load edit transfer screen
function doOnRowSelect(rowId,cellInd){
	
		if(cellInd == tfId_row_id){
			tfId_value = gridObj.cellById(rowId, tfId_row_id).getValue();
			
			if(tfId_value != ''){
				fnCallEditTransfer(tfId_value);
			}
		   
		}  
}
//This function used to create user defined column types in Grid
function eXcell_tfIdFl(cell) {
	///alert("ttpNm");// the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}
	
	this.setValue = function(val) {
		this.setCValue("<a href=\"#\">"+val+"",val);	
	}
	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
}
eXcell_tfIdFl.prototype = new eXcell;
</script>	

<BODY leftmargin="20" topmargin="10" onLoad="fnLoad();">
<FORM name="frmTransfer" method="POST" action="<%= strServletPath %>/GmTransferReportServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hTransferFrom" value="<%= strTransferFrom%>">
<input type="hidden" name="hTransferTo" value="<%= strTransferTo%>">
<input type="hidden" name="hTransferReason" value="<%= strTransferReason %>">
<input type="hidden" name="hTransferId" value="">
<input type="hidden" name="hTransferFromNm" value="<%=strTransferFm%>">
<input type="hidden" name="hTransferToNm" value="<%=strTransferToAc%>">


	<table border="0"  class="DtTable850" cellspacing="0" cellpadding="0">
	
	<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>
	
		<tr>
				<td colspan="2" height="25" class="RightDashBoardHeader"><fmtTransRpt:message key="TD_TRANS_RPT_HEADER"/> </td>
				<td align="right" colspan="2" class=RightDashBoardHeader ><fmtTransRpt:message key="IMG_ALT_HELP" var = "varHelp"/> 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
		</tr>
		
		<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		
		<tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
			<td height="25" align="right" class="RightTableCaption"><fmtTransRpt:message key="LBL_TYPE"/>:</td>
			<td >&nbsp;<gmjsp:dropdown controlName="Cbo_TransferType"  seletedValue="<%= strTransferType %>" 	
					tabIndex="1"   value="<%= alTransferType%>" codeId = "CODEID"  codeName = "CODENM"  onChange="fnSetFromValues(this.value)" defaultValue= "[Choose One]" />
			</td>
			<td height="25" class="RightTableCaption" align="right"><fmtTransRpt:message key="LBL_REASON"/>:</td>
			<td>&nbsp;
			 <%if(strTransferType.equals("26240577")) {%> 
			 <gmjsp:dropdown controlName="Cbo_TransferReason"  seletedValue="<%= strTransferReason %>" 	
			 tabIndex="1"   value="<%= alTransferReasonAC%>" codeId = "CODEID"  codeName = "CODENM" onChange="fnSetReasonSel(this.value)" defaultValue= "[Choose One]"  />
	        <%}else{ %>
	         <gmjsp:dropdown controlName="Cbo_TransferReason"  seletedValue="<%= strTransferReason %>" 	
			tabIndex="1"   value="<%= alTransferReason%>" codeId = "CODEID"  codeName = "CODENM" onChange="fnSetReasonSel(this.value)" defaultValue= "[Choose One]"  />
	        <%} %>
		</td>
		</tr>
		
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>	
		
		<tr class="shade">
			<td height="25"  class="RightTableCaption" align="right" ><fmtTransRpt:message key="LBL_FROM"/>:</td>
			<td>
			<%if(strTransferType.equals("26240577") || strTransferType.equals("90300")|| strTransferType.equals("26240615") || strTransferType.equals("26240616")) {%>
			<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="Cbo_TransferFrom" />
					<jsp:param name="METHOD_LOAD" value="<%=strMethodNm%>" />
					<jsp:param name="WIDTH" value="250" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="CONTROL_NM_VALUE" value="<%=strTransferFm%>" />
					<jsp:param name="CONTROL_ID_VALUE" value="" />
					<jsp:param name="SHOW_DATA" value="10" />
					<jsp:param name="ON_BLUR" value="" />
					
			    	</jsp:include>
						
	<%}else{ %>&nbsp;
	<gmjsp:dropdown controlName="searchCbo_TransferFrom"  seletedValue="<%=strTransferFm%>" 	
					tabIndex="1"   value="<%= alEmployeeList%>" codeId = "CODEID"  codeName = "CODENM" onChange="fnSetFromSel(this.value)" defaultValue= "[Choose One]"  />
	<%} %>
	</td>
			<td class="RightTableCaption" align="right"><fmtTransRpt:message key="LBL_TO"/>:</td>
			<td>
			 <%if(strTransferType.equals("26240577") || strTransferType.equals("90300") || strTransferType.equals("26240616") ||strTransferType.equals("26240615")) {%> 
  		         <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="Cbo_TransferTo" />
					<jsp:param name="METHOD_LOAD" value="<%=strMethodNm%>" />
					<jsp:param name="WIDTH" value="250" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="CONTROL_NM_VALUE" value="<%=strTransferToAc%>" />
					<jsp:param name="CONTROL_ID_VALUE" value="" />
					<jsp:param name="SHOW_DATA" value="10" />
					<jsp:param name="ON_BLUR" value="" />
			    	</jsp:include>
	<%}else{ %>&nbsp;
	<gmjsp:dropdown controlName="searchCbo_TransferTo"  seletedValue="<%=strTransferToAc%>" 	
					tabIndex="1"   value="<%= alDistributorList%>" codeId = "CODEID"  codeName = "CODENM" onChange="fnSetToSel(this.value)" defaultValue= "[Choose One]"  />
	<%} %>
			
			</td>
		</tr>
		
		
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>	
		
		
		<tr>
			<td height="25" class="RightTableCaption" align="right"><fmtTransRpt:message key="LBL_FROM_DATE"/>:</td>
			<td >&nbsp;<gmjsp:calendar textControlName="Txt_FromDate" gmClass="InputArea" textValue="<%=dtFromDate==null?null:new java.sql.Date(dtFromDate.getTime())%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;&nbsp; 
			</td>
			<td height="25" class="RightTableCaption" align="right"><fmtTransRpt:message key="LBL_TO_DATE"/>:</td>
			<td >&nbsp;<gmjsp:calendar textControlName="Txt_ToDate" gmClass="InputArea" textValue="<%=dtToDate==null?null:new java.sql.Date(dtToDate.getTime())%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;&nbsp; 
						
			</td>
		</tr>	
		
			
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>	
		
		
		<tr class="shade">
			<td height="25" class="RightTableCaption"align="right"><fmtTransRpt:message key="LBL_STATUS"/>:</td>
			<td >&nbsp;<select name="Cbo_Status" id="Status" class="RightText" tabindex="7">
							<option value="0" ><fmtTransRpt:message key="OPT_STATUS_ALL"/> </option>
						 	<option value="1" ><fmtTransRpt:message key="OPT_STATUS_OPEN"/> </option>
						 	<option value="2" ><fmtTransRpt:message key="OPT_STATUS_CLOSED"/></option>
						 	<option value="3" ><fmtTransRpt:message key="OPT_STATUS_REVERSED"/></option>
					 		</select></td>
			<td class="RightTableCaption" align="right"><fmtTransRpt:message key="LBL_CONSIGNMENT_ID"/>:</td>
			<td>&nbsp;<input type="text" size="28" value="<%=strCNID%>" name="Cbo_CNID" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"> 
					 		<fmtTransRpt:message key="BTN_LOAD" var="varLoad"/>
				<gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button" onClick="javascript:fnGo();" buttonType="Load" />
    		
		</tr>	
		
			
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		
			
<%-- <% if (strAction.equals("reportTransfer")) { %> --%>
<tr>
	<td colspan="4">
	<div id="dataGridDiv"  style="width: 100%"></div>
			<%-- <display:table name="requestScope.RSTRANSFERREPORT.rows"  class="its" id="currentRowObject" export="true" decorator="com.globus.displaytag.beans.DTTransferReportWrapper"> 
				<fmtTransRpt:message key="DT_TRANSFER_ID" var="varTransId"/>
				<display:column property="TSFID" title="${varTransId}" sortable="true"  />
				<fmtTransRpt:message key="LBL_FROM" var="varFrom"/>
				<display:column property="FROMNAME" title="${varFrom}" sortable="true"  />
				<fmtTransRpt:message key="LBL_TO" var="varTo"/>
				<display:column property="TOID" title="${varTo}" sortable="true"  />
				<fmtTransRpt:message key="LBL_REASON" var="varReason"/>
				<display:column property="TSFREASON" title="${varReason}" class="alignright"  sortable="true" />
				<fmtTransRpt:message key="LBL_TYPE" var="varType"/>
			  	<display:column property="TSFMODE" title="${varType}" class="alignright" sortable="true"   />
			  	<fmtTransRpt:message key="DT_DATE" var="varDate"/>
			  	<display:column property="TSFDATE" title="${varDate}" class="aligncenter"  sortable="true" format="<%=strDateFmt%>"/>
			  	<fmtTransRpt:message key="LBL_STATUS" var="varStatus"/>
			  	<display:column property="STATUS" title=" ${varStatus}" class="alignleft"  sortable="true"  />
			  	<fmtTransRpt:message key="LBL_CONSIGNMENT_ID" var="varCNID"/>
			  	<display:column property="CNID" title=" ${varCNID}" class="alignright"  sortable="true"  />
			</display:table>  --%>
			
			
		</td>
	</tr>	
	<tr style="display: none" id="DivExportExcel" height="25">
			<td colspan="4" align="center">
			    <div class='exportlinks'><fmtTransRpt:message key="DIV_EXPORT_OPT"/> : <img
					src='img/ico_file_excel.png' />&nbsp;<a href="#"
					onclick="fnExport();"><fmtTransRpt:message key="DIV_EXCEL"/> 
				</a></div>
			</td>
		</tr>
		<tr><td colspan="4" align="center"  class="RegularText"  align="center"><div id="DivNothingMessage" style="display: none;"><fmtTransRpt:message key="NO_DATA_FOUND"/></div></td></tr>	
		
			<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>	
<%-- <% } %> --%>
	</table>
	
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>