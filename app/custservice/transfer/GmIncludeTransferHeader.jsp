<%@page import="java.util.Date"%>
<%
/**********************************************************************************
 * File		 		     : GmIncludeTransferHeader.jsp
 * Desc		 		: This screen is a header include jsp for transfer transactions.
 *								Contains transfer type, From and To type and the reason for transfer
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ taglib prefix="fmtIncludeTransHeader" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmIncludeTransferHeader.jsp -->


<fmtIncludeTransHeader:setLocale value="<%=strLocale%>"/>
<fmtIncludeTransHeader:setBundle basename="properties.labels.custservice.transfer.GmFullTransfer"/>


<%

	HashMap hmParam = new HashMap();
	HashMap hcboVal = new HashMap();
	ArrayList alDistributorList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALDISTLIST"));
	ArrayList alEmployeeList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALEMPLIST"));
	
	log.debug("alDistributorList size is "+alDistributorList.size( ));
	
	ArrayList alTransferType =  (ArrayList)request.getAttribute("ALTSFTP");
	// ArrayList alTransferFrom = alEmployeeList;
	 ArrayList alTransferFrom = alDistributorList;
	ArrayList alTransferTo = alDistributorList;
	ArrayList alTransferReasonDtoD  =  (ArrayList)request.getAttribute("ALTSFRNDtoD");
	ArrayList alTransferReasonItoD  =  (ArrayList)request.getAttribute("ALTSFRNItoD");
	ArrayList alTransferReason = new ArrayList();
	
	hmParam = (HashMap)request.getAttribute("HMPARAM");
	
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strTransferType = "90301";
	String strTransferFrom = "";
	String  strTransferTo = "";
	String strTransferReason = "";
	String strDate = GmCommonClass.parseNull((String)request.getAttribute("STRDATE"));
	
	String strCancelReason = "";
	String strComments = "";
	String strTsfId = "";
//	String strDisabled = GmCommonClass.parseNull((String)request.getParameter("DISABLED"));
	String strDisabled = GmCommonClass.parseNull((String)request.getAttribute("DISABLED"));
	String strCodeID = "";
	String strSelected = "";
   
	String strApplDateFmt = strGCompDateFmt;
	Date dtSetDt = null;
	String strMissingDist = GmCommonClass.parseNull((String)request.getAttribute("MISSINGDIST"));
	
    log.debug(" strDisabled is " + strDisabled);
	if (hmParam != null)
	{
	     strTsfId = GmCommonClass.parseNull((String)hmParam.get("TSFID"));
         strTransferType = GmCommonClass.parseNull((String)hmParam.get("TRANSFERTYPE"));
         strTransferFrom = GmCommonClass.parseNull((String)hmParam.get("TRANSFERFROM"));
         strTransferTo = GmCommonClass.parseNull((String)hmParam.get("TRANSFERTO"));
          strTransferReason = GmCommonClass.parseNull((String)hmParam.get("TRANSFERREASON"));
          strDate = GmCommonClass.parseNull((String)hmParam.get("DATE"));
          log.debug(" strTransferReason ************************************** " +strTransferReason);
	}
	dtSetDt = GmCommonClass.getStringToDate(strDate,strApplDateFmt);
%>

<TITLE> Globus Medical: Full Transfer Header </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>     
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>

<script>
var strMissingDist ='<%=strMissingDist%>';
function fnChangeType()
{
	var obj = document.frmIncludeTransfer.Cbo_TransferType;
	var obj2 = document.frmIncludeTransfer.Cbo_TransferFrom;
	if (obj.options[obj.selectedIndex].text == '')
	{
		obj2.disabled = false;
		obj2.options.length = 0;
		obj2.options[0] = new Option("[Choose One]","0");
		for (var i=0;i<RepLen;i++)
		{
			arr = eval("RepArr"+i);
			arrobj = arr.split(",");
			id = arrobj[0];
			name = arrobj[1];
			obj2.options[i+1] = new Option(name,id);
		}
		obj2.focus();
	}
	}
	
	function fnSetFromValues(val)
	{
		var obj = eval("document.all.div"+val);
		if (val == 26240616) {   //26240616 -- Missing to Distributor
			document.all.idFrom.innerHTML = '&nbsp;<select style="width:310px" name="Cbo_TransferFrom" class="RightText" tabindex="1" disabled="disabled" > <option value="" >Missing</select>';
			document.frmTransfer.hTransferFrom.value = strMissingDist;
		}else{
			document.getElementById('Cbo_TransferFrom').value='0';
			document.getElementById('Cbo_TransferFrom').disabled=false;
			document.all.idFrom.innerHTML = obj.innerHTML;
	    } 
	}
	
	function fnSetReason(val)
	{
	removeAllOptions(document.frmTransfer.Cbo_TransferReason);		
		// setting the reason drop down
	addOption(document.frmTransfer.Cbo_TransferReason,"0","[Choose One]");
		if (val == '90300' || val == '26240616')		
		{
			<%
				  		int intSize = alTransferReasonDtoD.size();
				  		hcboVal = new HashMap();
				  		for (int i=0;i<intSize;i++)
				  		{
					  			hcboVal = (HashMap)alTransferReasonDtoD.get(i);
			%>
					addOption(document.frmTransfer.Cbo_TransferReason,'<%=hcboVal.get("CODEID")%>', '<%=hcboVal.get("CODENM")%>');
			<%
					  		}
			%>
		}
		else if (val == '90301')		
		{
			<%	         
						  		intSize = alTransferReasonItoD.size();
						  		hcboVal = new HashMap();
						  		for (int i=0;i<intSize;i++)
						  		{
						  			hcboVal = (HashMap)alTransferReasonItoD.get(i);
				%>
					addOption(document.frmTransfer.Cbo_TransferReason,'<%=hcboVal.get("CODEID")%>', '<%=hcboVal.get("CODENM")%>');
				<%
							  		}
				%>
		}
	}
	
	function fnSetFromSel(val)
	{
		document.frmTransfer.hTransferFrom.value = val;
	}
	
		function removeAllOptions(selectbox)
		{
			var i;
			for(i=selectbox.options.length-1;i>=0;i--)
			{
				//selectbox.options.remove(i);
				selectbox.remove(i);
			}
		}
		
		
		function addOption(selectbox, value, text )
		{
			var optn = document.createElement("OPTION");
			optn.text = text;
			optn.value = value;
		    selectbox.options.add(optn);
		}
	
</script>	

	<table border="0" cellspacing="0" cellpadding="0">
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td height="25" class="RightTableCaption" align="right" ><font color="red">*</font>  <fmtIncludeTransHeader:message key="LBL_TYPE"/>:</td>
			<td colspan="3">&nbsp;<gmjsp:dropdown controlName="Cbo_TransferType"  seletedValue="<%= strTransferType %>" 	
					tabIndex="1"   value="<%= alTransferType%>" codeId = "CODEID"  codeName = "CODENM"  onChange="fnSetFromValues(this.value);fnSetReason(this.value)"  />
			</td>
			<td rowspan="10" width="1" bgcolor="#cccccc"></td>
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>	
		<tr class="shade">
			<td height="25" class="RightTableCaption" align="right" width="10%"><font color="red">*</font><fmtIncludeTransHeader:message key="LBL_FROM"/>:</td>
			<td id="idFrom" width="40%">&nbsp;<select style="width:310px" name="Cbo_TransferFrom" class="RightText" tabindex="1" > <option value=0 >[Choose One]</select></td>
			<td class="RightTableCaption" align="right" width="10%"><font color="red">*</font><fmtIncludeTransHeader:message key="LBL_TO"/>:</td>
			<td width="40%">&nbsp;<gmjsp:dropdown controlName="Cbo_TransferTo"  seletedValue="<%= strTransferTo %>" 	
					tabIndex="1" disabled="<%=strDisabled%>"  value="<%= alTransferTo%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" width="310" />
			</td>
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>	
		<tr>
			<td height="25" class="RightTableCaption" align="right"><font color="red">*</font><fmtIncludeTransHeader:message key="LBL_REASON"/>:</td>
			<td> <%  if(strTransferType.equals("90301")) {alTransferReason = alTransferReasonItoD; } else {  alTransferReason = alTransferReasonDtoD; }  %>
			 	&nbsp;<gmjsp:dropdown controlName="Cbo_TransferReason"  seletedValue="<%= strTransferReason %>" 	
					tabIndex="1"   value="<%= alTransferReason%>" codeId = "CODEID" codeName = "CODENM" defaultValue= "[Choose One]"  />

			</td>
			<td class="RightTableCaption" align="right"><font color="red" >*</font><fmtIncludeTransHeader:message key="LBL_DATE"/>:</td>
			<td >&nbsp;<gmjsp:calendar textControlName="Txt_Date" 
						textValue="<%=(dtSetDt==null)?null:new java.sql.Date(dtSetDt.getTime())%>" 
	 					gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="1"/>
			</td>
		</tr>		
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>	
		<tr>  <td  colspan="4" height="1" >
<DIV style="visibility:hidden" id="div90300" >&nbsp;<gmjsp:dropdown controlName="Cbo_TransferFrom"  seletedValue="<%= strTransferFrom %>" 	
					tabIndex="1"   value="<%= alDistributorList%>" codeId = "CODEID"  codeName = "CODENM" onChange="fnSetFromSel(this.value)" defaultValue= "[Choose One]" width="310"/>
</DIV>
<DIV style="visibility:hidden" id="div26240616" >&nbsp;<select style="width:310px" name="Cbo_TransferFrom" class="RightText" tabindex="1" disabled="disabled" > <option value="<%=strTransferFrom%>">Missing</select>
</DIV>
<DIV  style="visibility:hidden" id="div90301">&nbsp;<gmjsp:dropdown controlName="Cbo_TransferFrom"  seletedValue="<%= strTransferFrom %>"
					tabIndex="1"   value="<%= alEmployeeList%>" codeId = "ID"  codeName = "NAME" onChange="fnSetFromSel(this.value)" defaultValue= "[Choose One]" width="310" />
</DIV> </td>
</tr>
	</table>