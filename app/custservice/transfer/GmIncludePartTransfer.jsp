<%@ include file="/common/GmHeader.inc" %>
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
}
%>
<%@ taglib prefix="fmtIncludePartTrans" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- GmIncludePartTransfer.jsp -->

<fmtIncludePartTrans:setLocale value="<%=strLocale%>"/>
<fmtIncludePartTrans:setBundle basename="properties.labels.custservice.transfer.GmFullTransfer"/>
<% 

String strMode = GmCommonClass.parseNull(request.getParameter("LISTMODE"));
String strPage = GmCommonClass.parseNull(request.getParameter("PAGE"));
%>


<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>    
<!-- GmIncludePartTransfer.jsp. Will be used by Set and Part Transfer.jsp -->
			<display:table name="ALTRANSFERLIST"  class="its" id="currentRowObject"   decorator="com.globus.displaytag.beans.DTSetTransferWrapper"> 
				<display:column property="CHKBOX" class="aligncenter" title="<input type=checkbox name=Chk_SelectAll  onClick=fnSelectAll(this);>"  />
				<fmtIncludePartTrans:message key="DT_PART_NUMBER" var="varPartNumber"/><display:column property="PNUM" title="${varPartNumber}" sortable="true"  />
				<fmtIncludePartTrans:message key="DT_PART_DESCRIPTION" var="varPartDescription"/><display:column property="PARTDESC" title="${varPartDescription}" sortable="true"  />
				<fmtIncludePartTrans:message key="DT_QTY" var="varQty"/><display:column property="QTY" title="${varQty}" class="alignright"  />
				<% if (!strPage.equals("PART")) { %>
			  	<fmtIncludePartTrans:message key="DT_SET_QTY" var="varSetQty"/><display:column property="SETQTY" title="${varSetQty}" class="alignright" sortable="true"   />
			  	<% } %>
			  	<fmtIncludePartTrans:message key="DT_TRANSFER_QTY" var="varTransferQty"/><display:column property="TSFQTY" title="${varTransferQty}" class="aligncenter"   />
			</display:table> 
			