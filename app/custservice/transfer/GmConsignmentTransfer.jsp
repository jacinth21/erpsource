<%
/**********************************************************************************
 * File		 		: GmConsignmentTransfer.jsp
 * Desc		 		: Account Consignment Transfer Screen
 * Version	 		: 1.0
 * author			: Prabhu vigneshwaran M D 
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.net.URLEncoder"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%>
<%@ taglib prefix="fmtGmConsignmentTransfer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtGmConsignmentTransfer:setLocale value="<%=strLocale%>"/>
<fmtGmConsignmentTransfer:setBundle basename="properties.labels.custservice.transfer.GmConsignmentTransfer" />
<!-- \custservice\transfer\GmConsignmentTransfer.jsp -->
 <bean:define id="strxmlGridData" name="frmConsignmentTransfer" property="strxmlGridData" type="java.lang.String"></bean:define>
   <bean:define id="transferFrom" name="frmConsignmentTransfer" property="transferFrom" type="java.lang.String"></bean:define>
   <bean:define id="transferTo" name="frmConsignmentTransfer" property="transferTo" type="java.lang.String"></bean:define>
   <bean:define id="transferType" name="frmConsignmentTransfer" property="transferType" type="java.lang.String"></bean:define>
   <bean:define id="accountId" name="frmConsignmentTransfer" property="accountId" type="java.lang.String"></bean:define>
    <bean:define id="consignmentId" name="frmConsignmentTransfer" property="consignmentId" type="java.lang.String"></bean:define>
 <%
	String strCustServiceJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	String strWikiTitle = GmCommonClass.getWikiTitle("ACC_CN_TRANSFER");
	String strApplDateFmt = strGCompDateFmt;
 %>
 
 <HTML>
<head>
<title>Globus Medical:Account Consignment Transfer Screen</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<style type="text/css" media="all"> @import url("<%=strCssPath%>/screen.css");</style>
<!-- Screen Js -->
<script language="JavaScript" src="<%=strCustServiceJsPath%>/GmConsignmentTransfer.js"></script>
<!-- Dhtmlx Import -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_fast.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_form.js"></script>
<script language="javascript" src="<%=strJsPath%>/GmGrid.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script>
var format  = '<%=strApplDateFmt%>';
var objGridData = '<%=strxmlGridData%>' ;
var lblFrom = '<fmtGmConsignmentTransfer:message key="LBL_FROM"/>';
var lblTo = '<fmtGmConsignmentTransfer:message key="LBL_TO"/>';
var lblReason = '<fmtGmConsignmentTransfer:message key="LBL_REASON"/>';
var lblDate = '<fmtGmConsignmentTransfer:message key="LBL_DATE"/>';

var lblAccNm = '<fmtGmConsignmentTransfer:message key="LBL_ACC_NM"/>';
var lblReqId = '<fmtGmConsignmentTransfer:message key="LBL_REQUEST_ID"/>';
var lblShippedDt = '<fmtGmConsignmentTransfer:message key="LBL_SHIPPED_DATE"/>';
var lblCreatedBy = '<fmtGmConsignmentTransfer:message key="LBL_CREATED_BY"/>';
var lblCreatedDate = '<fmtGmConsignmentTransfer:message key="LBL_CREATED_DATE"/>';

</script>
<!-- PC-3967 - to avoid scroll in the grid -->
<style type="text/css">
.objbox{
overflow: hidden !important; 
}
</style>
</head>
<BODY leftmargin="20" topmargin="10"  onload = "fnOnPageLoad()" onkeyup="fnEnter();">
<html:form action="/gmConsignmentTransfer.do?method=loadAccountConsignmentScreen">
<html:hidden property="inputStr"/>
<input type="hidden" name="transferMode" value="26240576">
<input type="hidden" name="transferFrom" value="<%=transferFrom%>"/>
<input type="hidden" name="hTransferFrom" value="<%=transferFrom%>"/>
<html:hidden property="hTransferTo"/>
<html:hidden property="hTransferType"/>
<html:hidden property="hTransferFromLoad"/>
<table border="0"  height="60" class=DtTable850 cellspacing="0" cellpadding="0">

       <tr  height="25">
			<td class="RightDashBoardHeader" colspan="4"><fmtGmConsignmentTransfer:message key="LBL_ACCOUNT_CONSIGNMENT_TRANSFER_SCREEN"/></td>
			<td align="right" colspan="4" class=RightDashBoardHeader ><fmtGmConsignmentTransfer:message key="IMG_ALT_HELP" var = "varHelp"/> 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
	     </tr>
	     <tr height="25">
	     <td class="RightTableCaption"  align="right"><fmtGmConsignmentTransfer:message key="LBL_TYPE" var="varType"/><gmjsp:label type="MandatoryText" SFLblControlName="${varType}" td="false" />:
	       </td> 
	     <td  colspan="7">&nbsp;<gmjsp:dropdown controlName="transferType" disabled="disabled" SFFormName="frmConsignmentTransfer" SFSeletedValue="transferType" SFValue="alType" codeId="CODEID"  codeName="CODENM" tabIndex="1"/></td>
	     </tr>
          <tr>
		  <td class="LLine" colspan="8" height="1"></td>
		  </tr>
		  
		<tr class="Shade" height="25">
		<td class="RightTableCaption"  align="right">
		<fmtGmConsignmentTransfer:message key="LBL_FROM" var="varAictype"/><gmjsp:label type="MandatoryText" SFLblControlName="${varAictype}" td="false" />:&nbsp;
		</td>
		<td  align="left">
		<html:text name="frmConsignmentTransfer" property="consignmentId" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" onchange="fnValidateConsignment(this.value,this.form);" tabindex="2" />
		<span id="validcheck" style="vertical-align:top; width:28px; display:inline-flex;">&nbsp;
				    <img id="setSuccimg" style="display:none;" tabindex="-1" height=16 width=19 src="<%=strImagePath%>/success.gif"></img>
				    <img id="setErrimg" style="display:none;" tabindex="-1"  height=13 width=13 src="<%=strImagePath%>/error.gif"></img>
			</span>
		</td> 
		<td class="RightTableCaption" colspan="2"  HEIGHT="25" align="right"><fmtGmConsignmentTransfer:message key="LBL_TO" var="varAccName"/><gmjsp:label type="MandatoryText" SFLblControlName="${varAccName}" td="false" />:</td>
		<td class="RightText" colspan="4"  HEIGHT="25">
							<table><tr HEIGHT="10"><td>					
							<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="transferTo" />
							<jsp:param name="METHOD_LOAD" value="loadAccountList&searchType=LikeSearch" />
							<jsp:param name="WIDTH" value="300" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="CONTROL_NM_VALUE" value="<%=accountId%>" />
							<jsp:param name="CONTROL_ID_VALUE" value="<%=transferTo%>" />
							<jsp:param name="SHOW_DATA" value="50" />
							<jsp:param name="ON_BLUR" value="" />
							<jsp:param name="TAB_INDEX" value="3" />
							</jsp:include>	
							</td></tr>
							<tr HEIGHT="4"><td></td></tr>
							</table>
		</td>
		</tr>
		 <tr>
		  <td class="LLine" colspan="8" height="1"></td>
		  </tr>	
					   <tr>
                       <td colspan="8">
	                   <div id="dataConsingmentInfo" height="300px" width="500"></div>	
    		           </td>	
  						</tr>
						
		 <!--  <tr>
		  <td class="LLine" colspan="8" height="1"></td>
		  </tr> -->
	       <tr  height="25">
	      <td class="RightTableCaption" align="right"><fmtGmConsignmentTransfer:message key="LBL_REASON" var="varReason"/>
	      <gmjsp:label type="MandatoryText" SFLblControlName="${varReason}" td="false" />:&nbsp;</td>
	      <td><gmjsp:dropdown controlName="transferReason" SFFormName="frmConsignmentTransfer" SFSeletedValue="transferReason" SFValue="alMode" codeId="CODEID"  codeName="CODENM" defaultValue="[Choose One]"  tabIndex="4"/></td>
	       <td class="RightTableCaption" colspan="2"  align="right"> <fmtGmConsignmentTransfer:message key="LBL_DATE" var="varDate"/><gmjsp:label type="MandatoryText"  SFLblControlName="${varDate}:" td="false"/></td>
		   <td colspan="2" width="200" align="left">&nbsp;<gmjsp:calendar SFFormName="frmConsignmentTransfer" controlName="date" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="5"/></td>
	       <td colspan="2"  class="RightTableCaption" align="center"><fmtGmConsignmentTransfer:message key="BTN_LOAD" var="varLoad"/>
			<gmjsp:button value="${varLoad}" gmClass="button" onClick="javascript:fnLoad()" tabindex="6" buttonType="Load"/> 	
			</td>
	       </tr>
			    <tr id="trDiv"  style="display: none">
			    	<td colspan="8">
					<div id="dataGridDiv" class="" height="400px"></div>
				</td>
				</tr>
			  		<tr style="display: none" id="trComments">
				    <td colspan="8" > 
							<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="FORMNAME" value="frmConsignmentTransfer" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="txt_LogReason" />
							<jsp:param name="LogMode" value="Edit" />
							<jsp:param name="Mandatory" value="yes" />
							<jsp:param name="TabIndex" value="7" />
							</jsp:include>
					</td>
		          </tr>
			  <tr style="display: none"  id="Initiate">
				<td colspan="8" align="center" height="30" class="RegularText"><fmtGmConsignmentTransfer:message key="BTN_INITIATE" var="varInitiate"/><gmjsp:button value="&nbsp;&nbsp;${varInitiate}&nbsp;&nbsp;"
							name="BTN_INITIATE" gmClass="button" buttonType="Initiate"
							onClick="fnConsignmentInitiateTransfer();" tabindex="8" />&nbsp;</td>
			</tr>	
			<tr>	
</table>
</html:form> 
</BODY>
<%@ include file="/common/GmFooter.inc" %>
</HTML>