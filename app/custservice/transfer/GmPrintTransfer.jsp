<!--\CS Label changes\custservice\transfer\GmPrintTransfer.jsp  -->
 <%
/**********************************************************************************
 * File		 			: GmPrintTransfer.jsp
 * Desc		 		: This screen is used to print the transfer details 
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<!-- \sales\transfer\GmPrintTransfer.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>

<!-- Imports for Logger -->

<%@ taglib prefix="fmtPrintTransfer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPrintTransfer:setLocale value="<%=strLocale%>"/>
<fmtPrintTransfer:setBundle basename="properties.labels.custservice.transfer.GmPrintTransfer"/>
<%
try {
	
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");
	
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(gmDataStoreVO.getCmpid(), "2000");
	
	//Getting currency format to show along with price
	String strCurrencyFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	//String strCountryCode = GmCommonClass.parseNull(GmCommonClass.getString("COUNTRYCODE"));
	String strCompanyName = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYNAME"));
	String strAddress = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYADDRESS"));
	String strCompanyLogo = GmCommonClass.parseNull((String)hmCompanyAddress.get("LOGO"));
	if(!strAddress.equals("")){
		strAddress = strAddress.replaceAll("/", "<br>");
	}
	strAddress = strCompanyName + "<br>" + strAddress;
	
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("HMPRINTTRANSFER");
	HashMap hmPrintHeader = new HashMap();
	ArrayList alTransferDetails = new ArrayList();
	
	log.debug(" Hashmap values are " +hmReturn);

	String strShade = "";

	String strTransferId = "";
	String strTransferFrom = "";
	String strTransferTo = "";
	String strDate = "";
	String strTsfReason = "";
	String strTsfType = "";
	String strInitiatorName = "";
	String strTsfMode = "";
	String strPartNum = "";
	String strControl =  "";
	String strPartDesc = "";
	String strQty = "";
	String strStatus = "";
	String strApplnDateFmt = strGCompDateFmt;
	

	if (hmReturn != null)
	{
		alTransferDetails = (ArrayList)hmReturn.get("ALPRINTDETAILS");
		hmPrintHeader = (HashMap)hmReturn.get("HMPRINTHEADER");
		strTransferId = GmCommonClass.parseNull((String)hmPrintHeader.get("TSFID"));
		strTransferFrom = GmCommonClass.parseNull((String)hmPrintHeader.get("FROMNAME"));
		strTransferTo = GmCommonClass.parseNull((String)hmPrintHeader.get("TONAME"));
		strDate = GmCommonClass.getStringFromDate((java.util.Date)hmPrintHeader.get("TSFDATE"),strApplnDateFmt);
		strTsfReason = GmCommonClass.parseNull((String)hmPrintHeader.get("TSFREASON"));
		strTsfType = GmCommonClass.parseNull((String)hmPrintHeader.get("TSFTYPE"));
		strInitiatorName = GmCommonClass.parseNull((String)hmPrintHeader.get("INITIATORNAME"));
		strTsfMode = GmCommonClass.parseNull((String)hmPrintHeader.get("TSFMODE"));
		strStatus = GmCommonClass.parseNull((String)hmPrintHeader.get("STATUS"));
	}

	int intSize = 0;
	HashMap hcboVal = null;

	StringBuffer sbStartSection = new StringBuffer();
	sbStartSection.append("<table border=0 width=702 cellspacing=0 cellpadding=0 align=center>");
	sbStartSection.append("<tr><td bgcolor=#666666 colspan=3></td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 width=1></td>");
	sbStartSection.append("<td>");
	sbStartSection.append("<table cellpadding=0 cellspacing=0 border=0 width='100%'>");
	sbStartSection.append("<tr><td height=1 bgcolor=#666666 colspan=4></td></tr>");
	sbStartSection.append("<tr>");
	sbStartSection.append("<td width=170><img src=");sbStartSection.append(strImagePath);
	sbStartSection.append("/"+ strCompanyLogo+".gif width=138 height=60></td>");
	sbStartSection.append("<td class=RightText width=300>");
	sbStartSection.append(strAddress);
	sbStartSection.append("<br></td>");
	sbStartSection.append("<td align=right class=RightText width=400><font size=+3>Consignment</font>&nbsp;</td>");
	sbStartSection.append("</tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 colspan=3></td></tr>");
	sbStartSection.append("<tr><td colspan=3>");
	
	// Header information 
	sbStartSection.append("<table border=0 width=700 cellspacing=0 cellpadding=0>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption>");
	sbStartSection.append("<td height=24 align=center width=250>&nbsp;Consigned From</td>");
	sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=8 ></td>");
	sbStartSection.append("<td width=250 align=center>&nbsp;Consigned To</td>");
	sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=8></td>");
	sbStartSection.append("<td width=98 align=center>&nbsp;Date</td>");
	sbStartSection.append("<td  width=98 align=center colspan=2>&nbsp;Transfer #</td>");
	sbStartSection.append("</tr><tr><td bgcolor=#666666 height=1 colspan=7></td></tr>");
	sbStartSection.append("<tr><td class=RightText rowspan=6 valign=top>&nbsp;");sbStartSection.append(strTransferFrom);
	sbStartSection.append("</td>");
	sbStartSection.append("<td rowspan=6 class=RightText valign=top>&nbsp;");sbStartSection.append(strTransferTo);
	sbStartSection.append("</td>");
	sbStartSection.append("<td height=25 class=RightText align=center>&nbsp;");sbStartSection.append(strDate);
	sbStartSection.append("</td><td class=RightText align=center colspan=2>&nbsp;");
	sbStartSection.append(strTransferId);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=3></td></tr>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption><td align=center colspan=3> By</td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=3></td></tr>");
	sbStartSection.append("<tr>");
    sbStartSection.append("<td align=center height=22  colspan=3 nowrap class=RightText>&nbsp;");
    sbStartSection.append(strInitiatorName);
	sbStartSection.append("</td></tr>");
	sbStartSection.append("<tr class=RightTableCaption bgcolor=#eeeeee>");
	sbStartSection.append("</tr><tr><td bgcolor=#666666 height=1 colspan=7></td></tr>");
	
	// Column header
	sbStartSection.append("<tr><td colspan=7 > <table  border=0 width=100% cellspacing=0 cellpadding=0>");
	sbStartSection.append("<tr bgcolor=#eeeeee  class=RightTableCaption>");
   sbStartSection.append("<td width=25 height=24>S#</td>");
   sbStartSection.append(" <td width=60 >&nbsp;Part #</td>");
	sbStartSection.append("<td  width=295>Description</td>");
	sbStartSection.append("<td align=center width=50>&nbsp;&nbsp;Qty</td>");
	sbStartSection.append("<td align=center width=60>Lot #</td>");
	sbStartSection.append("<td align=center width=80>Price</td>");
	sbStartSection.append("<td align=center width=73>Amount</td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=7></td></tr>");
	sbStartSection.append("</tr></td> </table>");
	sbStartSection.append("</table></td></tr></table>");
	sbStartSection.append("</td><td bgcolor=#666666 width=1></td></tr> </table>");

	StringBuffer sbEndSection = new StringBuffer();
	sbEndSection.append("<table width=702 cellspacing=0 cellpadding=0 align=center>");
	sbEndSection.append("<tr><td bgcolor=#666666 width=1 rowspan=7></td><td height=1 width=698></td><td bgcolor=#666666 width=1 rowspan=7></td></tr>");
	sbEndSection.append("<tr><td height=10>&nbsp;</td></tr>");
	sbEndSection.append("<tr><td height=50>&nbsp;</td></tr>");
	sbEndSection.append("<tr><td height=20 class=RightText valign=top><B>Consigning Agent</B></td></tr>");
	sbEndSection.append("<tr><td height=30 class=RightText valign=top><B><u>NOTES</u></B>:");
	sbEndSection.append("<BR>-Please sign and return the Consignment Agreement attached with this Form");
	sbEndSection.append("</td></tr>");
	sbEndSection.append("<tr><td height=1 bgcolor=#666666></td></tr></table>");
	sbEndSection.append("<table border=0 width=700 cellspacing=0 cellpadding=0><tr><td align=center height=30 id=button>");
	sbEndSection.append("<input type=button value=&nbsp;Print&nbsp; name=Btn_Print class=button onClick=fnPrint();>&nbsp;&nbsp;");
	sbEndSection.append("</td><tr></table>");

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Print Transfer </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/watermark.js"></script>
<style>
TR.PageBreak {
     page-break-after: always;
}
</style>
<script>

function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
</HEAD>

<BODY topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<!--StartSection starts here-->
<%out.print(sbStartSection);%>
<!--StartSection ends here-->
	<table border="0" width="702" cellspacing="0" cellpadding="0" align=center>
<%
	double dbCount = 0.0;
	intSize = alTransferDetails.size();
	//if (intSize > 0)
	//{
	HashMap hmLoop = new HashMap();
	HashMap hmTempLoop = new HashMap();

	String strNextPartNum = "";
	int intCount = 0;
	String strColor = "";
	String strLine = "";
	String strPartNumHidden = "";
	String strPrice = "";
	String strTotal = "";
	String strAmount = "";
	double dbAmount = 0.0;
	double dbTotal = 0.0;
	int intQty = 0;

	for (int i=0;i<intSize;i++)
	{
		hmLoop = (HashMap)alTransferDetails.get(i);
		if (i<intSize-1)
		{
			hmTempLoop = (HashMap)alTransferDetails.get(i+1);
			strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
		}
		strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
		strPartNumHidden = strPartNum;
		strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
		strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));

		if (strPartNum.equals(strNextPartNum))
		{
			intCount++;
			strLine = "<TR><TD colspan=7 height=1 bgcolor=#eeeeee></TD></TR>";
		}

		else
		{
			strLine = "<TR><TD colspan=7 height=1 bgcolor=#eeeeee></TD></TR>";
			if (intCount > 0)
			{
				strPartNum = "";
				strPartDesc = "";
				strQty = "";
				strLine = "";
			}
			else
			{
				//strColor = "";
			}
			intCount = 0;
		}

		if (intCount > 1)
		{
			strPartNum = "";
			strPartDesc = "";
			strQty = "";
			strLine = "";
		}
	
		strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
		strControl = GmCommonClass.parseNull((String)hmLoop.get("CONTROL_NUM"));
		strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));

		intQty = Integer.parseInt(strQty);
		dbAmount = Double.parseDouble(strPrice);
		dbAmount = intQty * dbAmount; // Multiply by Qty
		strAmount = ""+dbAmount;

		dbTotal = dbTotal + dbAmount;
		strTotal = ""+dbTotal;

		out.print(strLine);
%>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td class="RightText" width="25"><%=i+1%></td>
			<td class="RightText" width="60" height="20">&nbsp;<%=strPartNum%></td>
			<td class="RightText" width="340"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
			<td class="RightText" align="center" width="50"><%=GmCommonClass.getRedText(strQty)%></td>
			<td class="RightText" align="center" width="70"><%=strControl%>&nbsp;&nbsp;</td>
			<td class="RightText" align="center" width="60">&nbsp;&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strPrice))%></td>
			<td class="RightText" align="center" width="72">&nbsp;&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strAmount))%></td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
<%
		if (i == 29)
		{
%>
		<tr><td colspan="9" height="1" bgcolor="#666666"></td></tr>
		<tr><td colspan="9" height="20" class="RightText" align="right"><fmtPrintTransfer:message key="LBL_CONT_ON_NEXT_PAGE"/></td></tr>
		</table>
		<p STYLE="page-break-after: always"></p>
<%
		out.print(sbStartSection);
%>
		<table border="0" width="700" cellspacing="0" cellpadding="0" align=center>
<%
		} //end of IF

	} // end of FOR
%>
		<tr><td colspan="9" height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1" height="1"></td>
			<td colspan="5" height="25">&nbsp;</td>
			<td width="80" class="RightTableCaption" align="center">Total</td>
			<td width="80" class="RightTableCaption" align="right"><%=strCurrencyFmt%><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strTotal))%>&nbsp;&nbsp;</td>
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1" height="1"></td>
		</tr>
		<tr><td colspan="9" height="1" bgcolor="#666666"></td></tr>
	</table>
<!--EndSection starts here-->
<%out.print(sbEndSection);%>
<!--EndSection ends here-->

<%	
	 /* Below code to display watermark
	* status 1 means initiated status
	*/  
if(strStatus.equals("1"))
{
%>

<div id="waterMark" style="position:absolute; z-Index: 0; top: 340; left: 340">
	<img src="<%=strImagePath%>/draft.jpg"  border=0>
</div>

<% } %>

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>