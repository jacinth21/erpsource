<%
/**********************************************************************************
 * File		 		: GmFullTransfer.jsp
 * Desc		 		: This screen is used to load and initate Full transfer
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>

<!-- Imports for Logger -->

<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ page buffer="16kb" autoFlush="true" %>
<%@ taglib prefix="fmtFullTrans" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
 <!-- GmFullTransfer.jsp-->

<fmtFullTrans:setLocale value="<%=strLocale%>"/>
<fmtFullTrans:setBundle basename="properties.labels.custservice.transfer.GmFullTransfer"/>


<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmParam = new HashMap();
	
	ArrayList alDistributorList = (ArrayList)request.getAttribute("ALDISTLIST");
	ArrayList alEmployeeList = (ArrayList)request.getAttribute("ALEMPLIST");
	
	ArrayList alTransferType =  (ArrayList)request.getAttribute("ALTSFTP");
	ArrayList alTransferFrom = alEmployeeList;
	ArrayList alTransferTo = alDistributorList;
	ArrayList alTransferReason  =  (ArrayList)request.getAttribute("ALTSFRN");
	
	hmParam = (HashMap)request.getAttribute("HMPARAM");
	
	String strCustTransferPath = GmCommonClass.getString("GMTRANSFER");
	String strApplDateFmt = strGCompDateFmt;
	
	String strAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	
	String gridData = (String) request.getAttribute("alResult")==null?"":(String) request.getAttribute("alResult");
	
	String strTransferType = "";
	String strTransferFrom = "";
	String  strTransferTo = "";
	String strTransferReason = "";
	String strDate = "";
	
	String strCancelReason = "";
	String strComments = "";
	String strTxnId = "";
	String strTsfType = "";
	String strTsfFrom = "";
	String strTsfTo = "";
   
   log.debug(" values inside hmParam is " + hmParam);
	
	if (hmParam != null)
	{
	     strTxnId = GmCommonClass.parseNull((String)hmParam.get("TXNID"));
         strComments = GmCommonClass.parseNull((String)hmParam.get("COMMENTS"));
         strTransferType = GmCommonClass.parseNull((String)hmParam.get("TRANSFERTYPE"));
         strTransferFrom = GmCommonClass.parseNull((String)hmParam.get("TRANSFERFROM"));
         strTransferTo = GmCommonClass.parseNull((String)hmParam.get("TRANSFERTO"));
          strTransferReason = GmCommonClass.parseNull((String)hmParam.get("TRANSFERREASON"));
          strTsfType = strTransferType;
          strTsfFrom = strTransferFrom;
          strTsfTo = strTransferTo;
          
          log.debug(" strTsfType " + strTsfType);
	}
	
	log.debug( " Action is " + strAction);
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Full Transfer </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">

<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>  

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<SCRIPT>
var format = '<%=strApplDateFmt%>';
function fnOnPageLoad()
{
	var objGridData;
	objGridData = '<%=gridData%>';
	
	if (objGridData != '')
	{
		
		gridObj = initGrid('includepnumdata',objGridData,'',[]);
		gridObj.enableTooltips("false,true,true,true,true");
	}

}



function fnReload()
{
		
		
		var varTstType = document.frmTransfer.Cbo_TransferType.value;
		// var varTsfFrom = document.frmTransfer.Cbo_TransferFrom.value;
		var varTsfFrom = document.frmTransfer.hTransferFrom.value;
		var varTsfTo = document.frmTransfer.Cbo_TransferTo.value;

		if (varTstType == '0')
		{
			Error_Details(message[715]);
		}
		
		if (varTsfFrom == "undefined" || varTsfFrom == '')
		{
			Error_Details(message[716]);
		}
		
		 if (varTsfTo == '0')
		{
			Error_Details(message[717]);
		}
		
			if (varTsfFrom == varTsfTo)
		{
				Error_Details(message[726]);
		}
		
		if (ErrorCount > 0)
	 {
					Error_Show();
					Error_Clear();
					return false;
		}
		
	else {			
		
		document.frmTransfer.hAction.value = "loadFullTransferInfo";
		document.frmTransfer.hOpt.value = "ViewFullTransfer";
		fnStartProgress();
		document.frmTransfer.submit();	
		
		}
}


function fnCallCSGInfo(val)
{
windowOpener("/GmConsignSetServlet?hAction=LoadConsign&hId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnCallRMAInfo(val)
{
windowOpener("/GmReportCreditsServlet?hAction=PrintVersion&hId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnInitiateFullTransfer()
{
	// Verify if the Tsftype, from and To are the same
	var varTstType = document.frmTransfer.Cbo_TransferType.value;
	var varTsfFrom = document.frmTransfer.hTransferFrom.value;
	var varTsfTo = document.frmTransfer.Cbo_TransferTo.value;
	var varReason = document.frmTransfer.Cbo_TransferReason.value;
	
	var varTsfTypeLoad = document.frmTransfer.hTransferType.value;
	var varTsfFromLoad = document.frmTransfer.hTransferFromLoad.value;
	var varTsfToLoad = document.frmTransfer.hTransferTo.value;
	
	var varcomments = document.frmTransfer.Txt_LogReason.value;
	
		if (varTstType != varTsfTypeLoad)
	{
		 Error_Details(message[711]);
	}
	
		if (varTsfFrom != varTsfFromLoad)
	{
		 Error_Details(message[712]);
	}
	
		if (varTsfTo != varTsfToLoad)
	{
		 Error_Details(message[713]);
	}
	
		if (varcomments == '')
	{
				Error_Details(message[714]);
	}
	
	if (varReason == '0')
	{
				Error_Details(message[723]);
	}
	
	if (varTsfFrom == varTsfTo)
	{
				Error_Details(message[726]);
	}
	
else {
	
	CommonDateValidation(document.frmTransfer.Txt_Date, format,Error_Details_Trans(message[10002],format));
	document.frmTransfer.hAction.value = "saveTransfer";
	/*var str ='hey r u there ';
	var str ='';
	
	var theinputs=document.getElementsByTagName('input');
	var len = theinputs.length;
	var checkAll = document.frmTransfer.Chk_SelectAll;
	alert(str);
	alert(len);
	alert(checkAll);
				for(var n=0;n<len;n++)
					{
							objId = eval("document.frmTransfer.rad"+n); // The checkboxes would be like rad0, rad1 and so on
							if(objId)
							{			
								txnId = objId.value; // value would the CN or RA #
								txnType = objId.id;
							
									if (objId.checked)
									{
												str = str +  txnId +'^'+txnType+ '|'; 
									}
							  }
					   }	
			  	if (str == '' || str == null)
					 {
					 		Error_Details(message[719]);
					 		
					 }
			    }
			    	if (str.length >= 3900)
					 {
					 		Error_Details(message[722]);
					 }
		
		document.frmTransfer.hInputStr.value = str;
	}   */ 	
	}
	//document.frmTransfer.hInputStr.value = str;
	//document.frmTransfer.hAction.value = "saveTransfer";
	//document.frmTransfer.hAction.value = "initiateTransfer";
	       fnSubmit();
}

function fnSubmit() {

		if (ErrorCount > 0)
		 {
						Error_Show();
						Error_Clear();
						return false;
			}
			
			confirmed = window.confirm(message[10001]);
			if (confirmed)
			{
					fnStartProgress();						
					document.frmTransfer.submit();	
			} 
}

function fnLoad()
{
	
	//alert("In fnLoad GRID 3:"+objGridData);
		val = '<%=strTransferType%>';
		if (val != '')
		{
			var obj = eval("document.all.div"+val);
			document.all.idFrom.innerHTML = obj.innerHTML;
		}
		else 
		{
				document.frmTransfer.Cbo_TransferType.value = "90300";
				fnSetFromValues("90300");
		}
		
			
}

function fnOnCheckRad()
{
	objId = eval("document.frmTransfer.Chk_SelectAll");
	objId.checked = false;
}

function fnSelectAll(obj)
{
	var theinputs=document.getElementsByTagName('input');
	var len = theinputs.length;
	var status = obj.checked;
	for(var n=0;n<len;n++)
	{
			objId = eval("document.frmTransfer.rad"+n); // The checkboxes would be like rad0, rad1 and so on
			if(objId)
			{			
					objId.checked = status;
			}
	 }		
}


</SCRIPT>
</head>

<BODY leftmargin="20" topmargin="10" onLoad="fnLoad();fnOnPageLoad();">
<FORM name="frmTransfer" method="POST" action="<%=strServletPath%>/GmInitiateTransferServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="hac" value="">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hTsfMode" value="90350">
<input type="hidden" name="hTransferFrom" value="<%=strTransferFrom%>">
<input type="hidden" name="hTransferTo" value="<%=strTransferTo%>">
<input type="hidden" name="hTransferType" value="<%=strTransferType%>">
<input type="hidden" name="hTransferFromLoad" value="<%=strTsfFrom%>">

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader"><fmtFullTrans:message key="LBL_FULL_TRANS_HEADER"/></td>
		</tr>
		<tr><td colspan="4" height="1" class="Line"></td></tr>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
		<tr >
			<td>
				<jsp:include page='<%=strCustTransferPath + "/GmIncludeTransferHeader.jsp"%>' />
			</td>
			<td align="center" valign="middle"><fmtFullTrans:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button" onClick="javascript:fnReload();" buttonType="Load" tabindex="25" /></td>
		</tr>

		<% if(strAction.equals("LoadFullTransfer"))  { %>
		<%if(!gridData.equals("")){%>
		<tr>
			<td colspan="2">
			
  				<jsp:include page='<%=strCustTransferPath + "/GmIncludeFullTransfer.jsp"%>' >
						<jsp:param name="LISTMODE" value="Initiate"  />
				</jsp:include>
			</td>
		</tr>
		<%} %>
		<tr>
			<td height="5" colspan="2"> 
				<jsp:include page="/common/GmIncludeLog.jsp" >
				<jsp:param name="LogType" value="" />
				<jsp:param name="LogMode" value="Edit" />
				</jsp:include>
			</td>
		</tr>
		<tr>
     		<td height="30" colspan="2" align="center">
     		<fmtFullTrans:message key="BTN_INITIATE" var="varInitiate"/>
     		<gmjsp:button value="&nbsp;&nbsp;${varInitiate}&nbsp;&nbsp;" buttonType="Save" gmClass="button" onClick="javascript:fnInitiateFullTransfer();" tabindex="25" /></td>
		</tr>

		<% } %>
		
		
		
		
	</table>
</FORM>
<SCRIPT>

</SCRIPT>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
