 <%@page import="java.util.Date"%>
<%
/**********************************************************************************
 * File		 		: GmApproveTransfer.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Venkataprasath
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- Imports for Logger -->
 
<%@ taglib prefix="fmtApproveTrans" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
 <!-- GmApproveTransfer.jsp -->
<fmtApproveTrans:setLocale value="<%=strLocale%>"/>
<fmtApproveTrans:setBundle basename="properties.labels.custservice.transfer.GmApproveTransfer"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmTransfer = (HashMap)request.getAttribute("hmTransfer");
	
	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");
	String strFrom = GmCommonClass.parseNull((String)request.getAttribute("hFrom"));
	String strButtonStatus = GmCommonClass.parseNull((String)request.getAttribute("hBtnStatus"));
	String gridData = (String) request.getAttribute("alResult")==null?"":(String) request.getAttribute("alResult");
	String strTransferType = (String) request.getAttribute("hTransferType")==null?"":(String) request.getAttribute("hTransferType");
	String strRevButtonStatus = GmCommonClass.parseNull((String)request.getAttribute("hRevBtnStatus"));
	//System.out.println("Button Status"+strButtonStatus);
	String strTransferID="";
	String strType="";
	String strFromName="";
	String strToName="";
	String strReason="";
	String strDate="";
	String strMode="";
	String strStatus = "";
	String strDisabled = "";
	String strTypeId = "";
	request.setAttribute("hFrom",strFrom);
	String strFromNameId = "";
	String strChecked = "";
	String strConsignmentId="";
	String strRevDisableBtnVal="";
	String strDisableBtnVal="";
	//PMT-32065-Account Item Consignment Transfer
	if(strRevButtonStatus.equals("true")){
		strRevDisableBtnVal ="false";
		strDisableBtnVal ="true";
	}else {
		strRevDisableBtnVal ="true";
	}
	
	
%>	

<HTML>
<HEAD>
<TITLE> Globus Medical: Verify Transfer </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>


<script>
function fnOnPageLoad()
{
	var objGridData;
	objGridData = '<%=gridData%>';
	var hTransferType = document.frmTransfer.hTransferType.value;
	if (objGridData != '')
	{
		
		gridObj = initGrid('includepnumdata',objGridData,'',[]);
		gridObj.enableTooltips("false,true,true,true");
	}	
}
/*function fnAcceptTransfer(val) {
    strLog = document.frmTransfer.Txt_LogReason.value;

    if(strLog != "") {
	document.frmTransfer.hTransferId.value = val;
	document.frmTransfer.hFrom.value = "SaveTransfer";	
	document.frmTransfer.hAction.value = "Save";
	document.frmTransfer.action = "<%=strServletPath%>/GmAcceptTransferServlet";	
	}
	else 
	{
	 Error_Details(message[702]);	 
	}
  fnSubmit();	
}*/
function fnVoidTransfer()
{	
		val = document.frmTransfer.hTransferId.value;
        document.frmTransfer.hTxnId.value = val;
		document.frmTransfer.action ="<%=strServletPath%>/GmCommonCancelServlet";
		document.frmTransfer.hAction.value = "Load";
		document.frmTransfer.submit();
}
function fnCallCSGInfo(val)
{
windowOpener("/GmConsignSetServlet?hAction=LoadConsign&hId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnCallRMAInfo(val)
{
windowOpener("/GmReportCreditsServlet?hAction=PrintVersion&hId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintPreview()
{
	val = document.frmTransfer.hTransferId.value;
windowOpener("/GmPrintTransferServlet?hAction=PrintVersion&hTransferId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnAcknowledge()
{
	val = document.frmTransfer.hTransferId.value;
windowOpener("/GmPrintTransferServlet?hAction=AckTransfer&hTransferId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnReverse()
{    
strLog = document.frmTransfer.Txt_LogReason.value;
var consignmentId = document.frmTransfer.hConsignmentId.value;
if(strLog != "") {
	if (confirm(message[10187])) {	
    var transIdval = document.frmTransfer.hTransferId.value;
    fnStartProgress();
	document.frmTransfer.action = '/gmConsignmentTransfer.do?method=reverseConsignmentTransfer&txt_LogReason='+strLog+'&transferId='+transIdval;	  
	document.frmTransfer.submit();
	}
    }
    else 
    {
    	// validate the Comments
    	fnValidateTxtFld('Txt_LogReason',message[5120]);
    }
    if (ErrorCount > 0)
    {
    Error_Show();
    Error_Clear();
    return false;
    }

}
//Function used to click printversionAgreemnent Button
function fnPrintVersionLetter(){
	 var val = document.frmTransfer.hConsignmentId.value;
	 windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId="+val,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
//Function used to Click packslip button
function fnPackSlip(){
    var val = document.frmTransfer.hConsignmentId.value;
    windowOpener("/GmConsignSetServlet?hAction=PackVersion&hId="+val,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");               
}

</script>


</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad ="fnOnPageLoad();">
<FORM name="frmTransfer" method="POST" action="/GmAcceptTransferServlet" >

<input type="hidden" name="hFrom" value="AcceptTransfer">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hTxnId">
<input type="hidden" name="hCancelType" value="VDTSF">
<input type="hidden" name="hBtnStatus" value="<%=strButtonStatus%>">
<input type="hidden" name="hinputStr" value="" />
<input type="hidden" name="strOpt" value="" />
<input type="hidden" name="hTxt_LogReason" value="" />



<table border="1" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtApproveTrans:message key="LBL_CONSIGNMENT_VERIFY_TRANSFER"/></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td height="100" valign="top">
				<table border="0"  cellspacing="0" cellpadding="0">
				<%
                     if(hmTransfer.size() > 0) {
                     strTransferID = GmCommonClass.parseNull((String)hmTransfer.get("TSFID"));
                     strType = GmCommonClass.parseNull((String)hmTransfer.get("TSFTYPE"));
                     strTypeId = GmCommonClass.parseNull((String)hmTransfer.get("TSFTYPEID"));
                     strFromName = GmCommonClass.parseNull((String)hmTransfer.get("FROMNAME"));
                     strFromNameId = GmCommonClass.parseNull((String)hmTransfer.get("FROMNAMEID"));
                     strToName = GmCommonClass.parseNull((String)hmTransfer.get("TONAME"));
                     strReason = GmCommonClass.parseNull((String)hmTransfer.get("TSFREASON"));
                     strDate = GmCommonClass.getStringFromDate((Date)hmTransfer.get("TSFDATE"),strGCompDateFmt);
                     strMode = GmCommonClass.parseNull((String)hmTransfer.get("TSFMODE"));
                 	 strStatus = GmCommonClass.parseNull((String)hmTransfer.get("STATUS"));
                 	 strChecked = strStatus.equals("2")?"checked":"";
                 	 strConsignmentId = GmCommonClass.parseNull((String)hmTransfer.get("CONSIGN_ID"));//PMT-32065-Account Item Consignment Transfer
                     }
				%>		  
					  <tr>
						<td class="RightTableCaption" HEIGHT="20" align="right"><fmtApproveTrans:message key="LBL_TRANSFER_ID"/>:</td>
						<td class="RightText" width="250" >&nbsp;<%=strTransferID%><input type="hidden" name="hTransferId" value="<%=strTransferID%>"></td>
						<td class="RightTableCaption" HEIGHT="20" align="right"><fmtApproveTrans:message key="LBL_TYPE"/>:</td>
						<td class="RightText" width="150" >&nbsp;<%=strType%><input type="hidden" name="hTransferType" value="<%=strTypeId%>"></td>
					</tr>
					<tr class="shade">
						<td class="RightTableCaption" HEIGHT="20" align="right" ><fmtApproveTrans:message key="LBL_FROM"/>:</td>
						<td class="RightText" >&nbsp;<%=strFromName%></td>
						<td class="RightTableCaption" HEIGHT="20"  align="right"><fmtApproveTrans:message key="LBL_TO"/>:</td>
						<td class="RightText" >&nbsp;<%=strToName%></td>
					</tr>
					<tr>
						<td class="RightTableCaption" HEIGHT="20"  align="right"><fmtApproveTrans:message key="LBL_REASON"/>:</td>
						<td class="RightText" >&nbsp;<%=strReason%></td>
						<td class="RightTableCaption" HEIGHT="20"  align="right"><fmtApproveTrans:message key="LBL_DATE"/>:</td>
						<td class="RightText" >&nbsp;<%=strDate%></td>
					</tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right"  HEIGHT="24"><fmtApproveTrans:message key="LBL_TRANSFER_TYPE"/>:</td>
						<td class="RightText" >&nbsp;<%=strMode%></td>
						<td class="RightTableCaption" HEIGHT="20"  align="right" ><fmtApproveTrans:message key="LBL_COMPLETE"/>:</td>
						<td class="RightText" ><input type="checkbox" <%=strButtonStatus%> <%=strChecked%> name="Chk_Complete" ></td>	
					</tr>
						<!--//PMT-32065-Account Item Consignment Transfer -->
					<%if(strTypeId.equals("26240577")){%>
					   <tr>
				           <td class="RightTableCaption" HEIGHT="20" align="right" ><fmtApproveTrans:message key="LBL_CONSIGNMENT"/>:</td>
				           <td class="RightText" >&nbsp;<%=strConsignmentId%><input type="hidden" name="hConsignmentId" value="<%=strConsignmentId%>"></td>
				           <td></td>
				           <td></td>
				      </tr>
					<%}%>
					<tr>
						<td colspan="4">
						<DIV style="display:visible;height: 100px; overflow: auto;">
					  		<jsp:include page="/custservice/transfer/GmIncludeIDTransfer.jsp" >
				          		<jsp:param name="LISTMODE" value="Review"  />
		    	    		</jsp:include>
		    	    	</DIV>
		        		 </td>	
		    		 </tr> 	
					<%if(!gridData.equals("")){%>			
					  <tr>
						<td colspan="4">
								<jsp:include page="/custservice/transfer/GmIncludeFullTransfer.jsp" >
				          		<jsp:param name="LISTMODE" value="Review"  />
				          		</jsp:include>
						<!-- <DIV style="display:visible;height: 200px; overflow: auto;">
					  		
		    	    		
		    	    	</DIV> -->
		        		 </td>	
		    		  </tr> 
		    		  <%} %>
		    		  	<tr height="13"><td colspan="4"></td></tr>					
		    		   <tr>
						<td colspan="4">
						<DIV style="display:visible;height: 200px; overflow: auto;">
					  		<jsp:include page="/custservice/transfer/GmTransferTagInclude.jsp" >
				          		<jsp:param name="LISTMODE" value="Review"  />
				          		<jsp:param name="FROMNAMEID" value="<%=strFromNameId%>" />
		    	    		</jsp:include>
		    	    	</DIV>
		        		 </td>	
		    		  </tr> 		
					<tr>
				    	<td  colspan="4">
						<jsp:include page="/common/GmIncludeLog.jsp" >
						<jsp:param name="LogType" value="" />
						<jsp:param name="LogMode" value="Edit" />
						</jsp:include>
				    	</td>
					</tr>
					<% strDisableBtnVal = strButtonStatus;
						if(strDisableBtnVal.equals("disabled")){
							strDisableBtnVal ="true";
						}
					 %>
				  	<tr>
				    	<td align="center" height="30" colspan="4">
				    	<fmtApproveTrans:message key="BTN_ACKNOWLEDGE" var="varAcknowledge"/>
				    	 <gmjsp:button value="${varAcknowledge}" name="Btn_Acknowledge" gmClass="button" buttonType="Save" onClick="fnAcknowledge();" />
				    	 <fmtApproveTrans:message key="BTN_PRINT_PREVIEW" var="varPrintPreview"/>				    	
				    	 <gmjsp:button value="${varPrintPreview}" name="Btn_Print_Preview" gmClass="button" buttonType="Load" onClick="fnPrintPreview();" />
				    	 <fmtApproveTrans:message key="BTN_VOID" var="varVoid"/>
    				    <gmjsp:button value="${varVoid}" name="Btn_Void" gmClass="button" disabled="<%=strDisableBtnVal%>" buttonType="Save"  onClick="fnVoidTransfer();"/>
    				    <fmtApproveTrans:message key="BTN_SUBMIT" var="varSubmit"/>
						<gmjsp:button value="${varSubmit}" name="Btn_Accept" gmClass="button" disabled="<%=strDisableBtnVal%>"  buttonType="Save" onClick="fnSubmitTag();" />
						<!--//PMT-32065-Account Item Consignment Transfer -->
						 <%if(strTypeId.equals("26240577")){%>	
						 <fmtApproveTrans:message key="BTN_REVERSE" var="varReverse"/>
						<gmjsp:button value="${varReverse}" name="Btn_Reverse" gmClass="button" disabled="<%=strRevDisableBtnVal%>" buttonType="Reverse" onClick="fnReverse();" />
						 <fmtApproveTrans:message key="BTN_PACKSLIP" var="varPackSlip"/>
						<gmjsp:button value="${varPackSlip}" name="Btn_PackSlip" gmClass="button" buttonType="Save" onClick="fnPackSlip();" />
						 <fmtApproveTrans:message key="BTN_PRINTVERAGRE" var="varPrintVerAgre"/>
						<gmjsp:button value="${varPrintVerAgre}" name="Btn_PrintVerAgre" gmClass="button" buttonType="Load" onClick="fnPrintVersionLetter();" />
						<%}%>
		    			</td>
		  			</tr>
	    		</table>
	    	 </td>	
	     </tr>	 
	   </table>
     </FORM>		
     <%@ include file="/common/GmFooter.inc" %>
 </BODY>    
<%
}catch(Exception e)
{
	e.printStackTrace();
	}
%>							
</HTML>