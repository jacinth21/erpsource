 <%
/**********************************************************************************
 * File		 		: GmTransferTagInclude.jsp
 * Desc		 		: This screen is inclluded in the ...jsp
 * Version	 		: 1.0
 * author			: Balamurugan Ramasamy
************************************************************************************/
%>
<!-- \custservice\transfer\GmTransferTagInclude.jsp -->
<%@ include file="/common/GmHeader.inc" %>


<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.lang.*" %>

<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
 	strLocale = GmCommonClass.parseNull((String)locale.toString());
 	strJSLocale = "_"+strLocale;
}

	
	ArrayList alTransferTagDet = (ArrayList)request.getAttribute("alTransferTagDet");
	String accessFlag = (String)request.getAttribute("accessFlag");
	String strShade= "";
	String strFromNameId = GmCommonClass.parseNull(request.getParameter("FROMNAMEID"));
	String strButtonStatus = GmCommonClass.parseNull((String)request.getAttribute("hBtnStatus"));
	
%>
<%@ taglib prefix="fmtTransTagInclude" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmTransferTagInclude.jsp -->
<fmtTransTagInclude:setLocale value="<%=strLocale%>"/>
<fmtTransTagInclude:setBundle basename="properties.labels.custservice.transfer.GmApproveTransfer"/>

<HTML>
<HEAD>
<TITLE> Globus Medical: Transfer Tag Entry </TITLE>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
function fnSubmitTag()
{
	
	if (document.frmTransfer.Chk_Complete.checked)
	{
		var CompleteChecked = document.frmTransfer.Chk_Complete.checked.value = 'Y';
	}
	
	
	var varRows =  <%=alTransferTagDet.size()%>;
	var inputString = '';   
	var missingFlag="";
	
	for (var k=0; k < varRows; k++) 
	{

		objTag="";
		missingFlag="";
		var tag_group=document.forms["frmTransfer"].elements["TagNo"+k];
					
		
		objRefID =  eval(document.getElementById("RefID"+k));
		objPartNum = eval(document.getElementById("PartNum"+k)); 
		//objLocId = eval(document.getElementById("LocID"+k)); 
		//objControlNum = eval(document.getElementById("ControlNum"+k));
		
		//objSetID =  eval(document.getElementById("SetID"+k));
		
		var item =  eval(document.getElementById("ItemId"+k));

		
		var flag_group=document.forms["frmTransfer"].elements["missingFlag"+k];
		if(tag_group.length!=null && flag_group.length!=null)
		{
			for(var i=0;i<tag_group.length;i++)
			{
				objTag=tag_group[i].value;
				missingFlag=flagCheck(flag_group[i]);
				inputString = inputString + item.value + '^'+ objTag +'^'+ missingFlag +'^'+objRefID.value  +'|'; 
				errCheck(objTag,missingFlag,CompleteChecked,objPartNum);
			}
			
		}else
		{
			objTag = document.getElementById("TagNo"+k).value;
			missingFlag = flagCheck(eval(document.getElementById("missingFlag"+k)));
			inputString = inputString + item.value + '^'+ objTag +'^'+ missingFlag +'^'+objRefID.value  +'|'; 
			errCheck(objTag,missingFlag,CompleteChecked,objPartNum);
		}

		
		
		
	} 
	
	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}  
	document.frmTransfer.hinputStr.value = inputString;
	document.frmTransfer.hAction.value =  'Save';
	document.getElementsByName("hTxt_LogReason").value = document.getElementsByName("Txt_LogReason").value;
	fnStartProgress();
	document.frmTransfer.submit();	
}

function flagCheck(varmissingFlag)
{
var missingFlag;
if(varmissingFlag.checked == true)
{
	missingFlag = 'Y';
}
else
{
	missingFlag = "";
}
return missingFlag;
}

function errCheck(objTag,missingFlag,CompleteChecked,objPartNum)
{
	
if(CompleteChecked == 'Y')
{
	if(objTag.length==0&& missingFlag!='Y')
	{
		Error_Details(Error_Details_Trans(message[10022],objPartNum.value));
	}
	if(objTag.length !=0&& missingFlag=='Y')
	{
		Error_Details(Error_Details_Trans(message[10023],objPartNum.value));
	}	
}else if(objTag.length >0&& missingFlag=='Y')
	{
	Error_Details(Error_Details_Trans(message[10023],objPartNum.value));
	}
}

function fnValidateTag (trcnt, obj) {
	var tagId = obj.value
	if(tagId != ''){
		fnCallAjaxTagId('ValidateTag', tagId, trcnt);
	}else{
		document.getElementById("DivShowTag"+trcnt).style.display = 'none';
		document.getElementById("DivShowTagNotExists"+trcnt).style.display = 'none';
		document.frmTransfer.Btn_Accept.disabled = false;
	}
}

//Ajax call to validate the tag
function fnCallAjaxTagId(val, tagId,  trcnt){
    var url = fnAjaxURLAppendCmpInfo("/GmAcceptTransferServlet?tagId="+encodeURIComponent(tagId)+"&hAction="+encodeURIComponent(val)+'&randomId='+Math.random());
dhtmlxAjax.get(url,function(loader){
	var tagIdFl =loader.xmlDoc.responseText;
 	if(tagIdFl == 'Y'){
 		document.getElementById("DivShowTagNotExists"+trcnt).style.display = 'inline-block';
		document.getElementById("DivShowTag"+trcnt).style.display = 'none';
		document.frmTransfer.Btn_Accept.disabled = true;
 	}else{
 		document.getElementById("DivShowTag"+trcnt).style.display = 'inline-block';
		document.getElementById("DivShowTagNotExists"+trcnt).style.display = 'none';
		document.frmTransfer.Btn_Accept.disabled = false;
	}
	
});
}

</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" >
<table id="TagTable" width="100%" border="0" class="DtTable850">
<tr ><td bgcolor="#666666" height="1"  colspan="7"></td></tr>
<tr>
	<td height="25" class="RightDashBoardHeader" colspan="7"><fmtTransTagInclude:message key="LBL_TRANSFER_TAG_ENTRY"/></td>
</tr>
<tr><td bgcolor="#666666" height="1" colspan="7"></td></tr>
 <tr class="ShadeRightTableCaption" height="25">
							<td  width="150"><fmtTransTagInclude:message key="LBL_TRANSFER_TAG_ENTRY"/></td>
							<td class="RightText" align="center" width="100"><fmtTransTagInclude:message key="LBL_TAG_MISSING"/></td>
							<td><fmtTransTagInclude:message key="LBL_PART_NUMBER"/></td>
							<td><fmtTransTagInclude:message key="LBL_CURRENT_LOCATION"/></td>
							<td><fmtTransTagInclude:message key="LBL_CONTROL"/></td>
							<td><fmtTransTagInclude:message key="LBL_SET_ID"/></td>
						</tr>	
		<%
		if(alTransferTagDet.size()>0) {
		HashMap hmTransferDet;
		int qty=0,x=0;
		for(int i=0;i<alTransferTagDet.size();i++) {
			hmTransferDet = (HashMap) alTransferTagDet.get(i);
			strShade = (x%2 != 1)?"class=Shade":"";
			String strMissFl = (String)hmTransferDet.get("MISSING_FL");
			qty=Integer.parseInt(hmTransferDet.get("QTY")==null?"0":(String)hmTransferDet.get("QTY"));
			for(int j=0;j<qty;j++) {
				strShade = (x%2 != 1)?"class=Shade":"";
				
		%>
		

		<tr height="10px;"  <%=strShade%>>
		<td>
		 <input type = 'text' size='17' id = 'TagNo<%=i%>' name = 'TagNo<%=i%>' value='<%=hmTransferDet.get("TAGID")%>' onBlur='javascript:fnValidateTag(<%=i%>,this);'/> 
		<input type='hidden' id='ItemId<%=i%>' name='ItemId<%=i%>' value='<%=hmTransferDet.get("ITEMID")%>' /> 
		<span id="DivShowTag<%=i%>" style="vertical-align:middle; display: none;"> <img height="14" width="15" src="<%=strImagePath%>/success.gif"></img></span>
		<span id="DivShowTagNotExists<%=i%>" style=" vertical-align:middle; display: none;"><img height="11" width="12" src="<%=strImagePath%>/delete.gif"></img></span></td>
		<%if(strMissFl.equals("Y")) { %>
			<td class="RightText" align="center"><INPUT type='checkbox' id = "missingFlag<%=i%>" name="missingFlag<%=i%>"  <%=strButtonStatus%>  checked="true" onchange(this)/></td>
		<%}else{ %>
			<%if(accessFlag.equalsIgnoreCase("Y")){ %>
				<td class="RightText" align="center"><input type = 'checkbox' name ='missingFlag<%=i%>' id ='missingFlag<%=i%>'/></td>
			<%} else {%>
				<td class="RightText" align="center"><input type = 'checkbox' name ='missingFlag<%=i%>' id ='missingFlag<%=i%>' disabled="disabled" /></td>
			<%} %>
		<%} %>
		
			<td width="100"><%=hmTransferDet.get("PNUM")%> <input type='hidden' id = 'PartNum<%=i%>' name= 'PartNum<%=i%>' value='<%=hmTransferDet.get("PNUM")%>' /></td>
	
		<%if(!strFromNameId.equals( hmTransferDet.get("LOCIDNUM")) && ! (hmTransferDet.get("LOCIDNUM").equals("")) && !strButtonStatus.equals("disabled")) {%>
			<td width="220" bgcolor="#F78181"><%=hmTransferDet.get("LOCID")%><input type='hidden' id= 'LocID<%=i%>' name= 'LocID<%=i%>' value='<%=hmTransferDet.get("LOCID")%>'/></td>
		<%}else{ %>
			<td width="220"><%=hmTransferDet.get("LOCID")%><input type='hidden' id= 'LocID<%=i%>' name= 'LocID<%=i%>' value='<%=hmTransferDet.get("LOCID")%>'/></td>
		<%} %>
			<td width="100"><%=hmTransferDet.get("CNTLNUM")%><input type='hidden' id= 'ControlNum<%=i%>' name= 'ControlNum<%=i%>' value='<%=hmTransferDet.get("CNTLNUM")%>' /><input type='hidden' id= 'RefID<%=i%>' name= 'RefID<%=i%>' value='<%=hmTransferDet.get("REFIDRTN")%>' /></td>
			
			<td><%=hmTransferDet.get("SETID")%><input type='hidden' id= 'SetID<%=i%>' name= 'SetID<%=i%>' value='<%=hmTransferDet.get("SETID")%>' /></td>
		</tr>
		<%x++;}
		} %>
		<%} else {
				%>
				
			<tr > <td colspan="6"><fmtTransTagInclude:message key="LBL_NO_DATA_AVAILABLE"/></td></tr>

		<%} %>
			
		
</table>
</BODY>
</HTML>