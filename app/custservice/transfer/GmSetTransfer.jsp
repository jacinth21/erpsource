<%
/**********************************************************************************
 * File		 		: GmSetTransfer.jsp
 * Desc		 		: This screen is used to load and initate Set transfer
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtSetTrans" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmSetTransfer.jsp -->

<fmtSetTrans:setLocale value="<%=strLocale%>"/>
<fmtSetTrans:setBundle basename="properties.labels.custservice.transfer.GmFullTransfer"/>

<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	HashMap hmParam = new HashMap();
	
	ArrayList alDistributorList = (ArrayList)request.getAttribute("ALDISTLIST");
	ArrayList alEmployeeList = (ArrayList)request.getAttribute("ALEMPLIST");
	
	ArrayList alTransferType =  (ArrayList)request.getAttribute("ALTSFTP");
	ArrayList alTransferFrom = alEmployeeList;
	ArrayList alTransferTo = alDistributorList;
	ArrayList alTransferReason  =  (ArrayList)request.getAttribute("ALTSFRN");
	ArrayList alSet = (ArrayList)request.getAttribute("ALSET");
	
	hmParam = (HashMap)request.getAttribute("HMPARAM");
	
	String strCustTransferPath = GmCommonClass.parseNull(GmCommonClass.getString("GMTRANSFER"));
	String strApplDateFmt = strGCompDateFmt;
	
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strTransferType = "";
	String strTransferFrom = "";
	String  strTransferTo = "";
	String strTransferReason = "";
	String strDate = "";
	
	String strCancelReason = "";
	String strComments = "";
	String strTxnId = "";
	String strTsfType = "";
	String strTsfFrom = "";
	String strTsfTo = "";
	String strSetId = "";
	String strSetIdName="";
	
   log.debug(" values inside hmParam is " + hmParam);
	
	if (hmParam != null)
	{
	     strTxnId = GmCommonClass.parseNull((String)hmParam.get("TXNID"));
         strComments = GmCommonClass.parseNull((String)hmParam.get("COMMENTS"));
         strTransferType = GmCommonClass.parseNull((String)hmParam.get("TRANSFERTYPE"));
         strTransferFrom = GmCommonClass.parseNull((String)hmParam.get("TRANSFERFROM"));
         strTransferTo = GmCommonClass.parseNull((String)hmParam.get("TRANSFERTO"));
         strTransferReason = GmCommonClass.parseNull((String)hmParam.get("TRANSFERREASON"));
		 strSetId = GmCommonClass.parseNull((String)hmParam.get("SETID"));
		 strSetIdName=GmCommonClass.parseNull((String)hmParam.get("SETNAME"));
		  
         strTsfFrom = strTransferFrom;
         log.debug(" strTsfType " + strTsfType);
	}
	
	log.debug(" Action is " + strAction);
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Transfer </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>     
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/customerservice/GmSetTransfer.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/common/GmAutoCompleteInclude.js"></script>
<SCRIPT>
var dateFmt = '<%=strApplDateFmt%>';
var val = '<%=strTransferType%>';
</SCRIPT>
</head>

<BODY leftmargin="20" topmargin="10" onLoad="fnLoad();">
<FORM name="frmTransfer" method="POST" action="<%=strServletPath%>/GmInitiateTransferServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="Cbo_DistId" value="">
<input type="hidden" name="hPartID" value="">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hTsfMode" value="90351">
<input type="hidden" name="hTransferFrom" value="<%=strTransferFrom%>">
<input type="hidden" name="hTransferTo" value="<%=strTransferTo%>">
<input type="hidden" name="hTransferType" value="<%=strTransferType%>">
<input type="hidden" name="hTransferFromLoad" value="<%=strTsfFrom%>">
<input type="hidden" name="hSetId" value="<%=strSetId%>">

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
	<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>
		<tr>
				<td colspan="4" height="25" class="RightDashBoardHeader"><fmtSetTrans:message key="LBL_SET_TRANS_HEADER"/></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>
		<tr>
				<td colspan="4"> 
				<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
				<jsp:include page='<%=strCustTransferPath + "/GmIncludeTransferHeader.jsp"%>' />
			
				</td>
		</tr>
	
		<tr>
                   <td class="RightTableCaption" align="right" height="30"><font color="red">*</font>&nbsp;<fmtSetTrans:message key="LBL_SET_LIST"/>:</td> 
                   <td>
                        <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
                        <jsp:param name="CONTROL_NAME" value="Cbo_Set"/>
                        <jsp:param name="METHOD_LOAD" value="loadSetMapList" />
                        <jsp:param name="WIDTH" value="600" />
                    	<jsp:param name="CSS_CLASS" value="search" />
                    	<jsp:param name="TAB_INDEX" value="4"/>
                    	<jsp:param name="CONTROL_NM_VALUE" value="<%=strSetIdName %>" />
                    	<jsp:param name="CONTROL_ID_VALUE" value="<%=strSetId %>" />                     
                    	<jsp:param name="SHOW_DATA" value="100" />
                    	<jsp:param name="AUTO_RELOAD" value="" />                   
                        </jsp:include>
                    </td>
                    
                    <td ><fmtSetTrans:message key="BTN_LOAD" var="varLoad"/> <gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button" buttonType="Load" onClick="javascript:fnGo();" tabindex="25" /> </td>
   			<td > </td>
                   </tr>
                   
                   	
		<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>

		<% if(strAction.equals("LoadSetTransfer"))  { %>
		<tr>
					<td colspan="4"> 
					<DIV style="display:visible;height: 200px; overflow: auto;">
					<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
						<jsp:include page='<%=strCustTransferPath + "/GmIncludePartTransfer.jsp"%>' />
					</DIV>
					</td>
		</tr>
		<tr>
				<td colspan="4" > 
							<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="LogType" value="" />
							<jsp:param name="LogMode" value="Edit" />
							<jsp:param name="Mandatory" value="yes" />
							</jsp:include>
					</td>
		</tr>
		<tr>
		     		<td colspan="4"  align="center" ><fmtSetTrans:message key="BTN_INITIATE" var="varInitiate"/><gmjsp:button value="&nbsp;&nbsp;${varInitiate}&nbsp;&nbsp;" gmClass="button" onClick="javascript:fnInitiateSetTransfer();" tabindex="25" buttonType="Save" /> </td>
		</tr>	
		
		
		<% } %>
		
	</table>
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
