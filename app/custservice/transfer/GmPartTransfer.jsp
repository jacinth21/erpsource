<%
/**********************************************************************************
 * File		 			: GmPartTransfer.jsp
 * Desc		 		: This screen is used to load and initate Part transfer
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ taglib prefix="fmtSetTrans" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmPartTransfer.jsp -->

<fmtSetTrans:setLocale value="<%=strLocale%>"/>
<fmtSetTrans:setBundle basename="properties.labels.custservice.transfer.GmFullTransfer"/>




<%

	HashMap hmParam = new HashMap();
	
	ArrayList alDistributorList = (ArrayList)request.getAttribute("ALDISTLIST");
	ArrayList alEmployeeList = (ArrayList)request.getAttribute("ALEMPLIST");
	
	ArrayList alTransferType =  (ArrayList)request.getAttribute("ALTSFTP");
	ArrayList alTransferFrom = alEmployeeList;
	ArrayList alTransferTo = alDistributorList;
	ArrayList alTransferReason  =  (ArrayList)request.getAttribute("ALTSFRN");
	ArrayList alSet = (ArrayList)request.getAttribute("ALSET");
	
	hmParam = (HashMap)request.getAttribute("HMPARAM");
	
	String strApplDateFmt = strGCompDateFmt;
	String strCustTransferPath = GmCommonClass.parseNull(GmCommonClass.getString("GMTRANSFER"));
	
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strTransferType = "";
	String strTransferFrom = "";
	String  strTransferTo = "";
	String strTransferReason = "";
	String strDate = "";
	
	String strCancelReason = "";
	String strComments = "";
	String strTxnId = "";
	String strTsfType = "";
	String strTsfFrom = "";
	String strTsfTo = "";
	String strPartNums = "";
   
   log.debug(" values inside hmParam is " + hmParam);
	
	if (hmParam != null)
	{
	     strTxnId = GmCommonClass.parseNull((String)hmParam.get("TXNID"));
         strComments = GmCommonClass.parseNull((String)hmParam.get("COMMENTS"));
         strTransferType = GmCommonClass.parseNull((String)hmParam.get("TRANSFERTYPE"));
         strTransferFrom = GmCommonClass.parseNull((String)hmParam.get("TRANSFERFROM"));
         strTransferTo = GmCommonClass.parseNull((String)hmParam.get("TRANSFERTO"));
         strTransferReason = GmCommonClass.parseNull((String)hmParam.get("TRANSFERREASON"));
         strPartNums = GmCommonClass.parseNull((String)hmParam.get("PARTNUMS"));
         strTsfFrom = strTransferFrom;
         log.debug(" strTsfType " + strTsfType);
	}
	
	log.debug(" Action is " + strAction);
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Transfer </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>     
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/customerservice/GmPartTransfer.js"></script>
<SCRIPT>
var format = '<%=strApplDateFmt%>';
var val = '<%=strTransferType%>';
</SCRIPT>
</head>

<BODY leftmargin="20" topmargin="10" onLoad="fnLoad();">
<FORM name="frmTransfer" method="POST" action="<%=strServletPath%>/GmInitiateTransferServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="Cbo_DistId" value="">
<input type="hidden" name="hPartID" value="">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hTsfMode" value="90352">
<input type="hidden" name="hTransferFrom" value="<%=strTransferFrom%>">
<input type="hidden" name="hTransferTo" value="<%=strTransferTo%>">
<input type="hidden" name="hTransferType" value="<%=strTransferType%>">
<input type="hidden" name="hTransferFromLoad" value="<%=strTsfFrom%>">

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
	<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>
		<tr>
				<td colspan="4" height="25" class="RightDashBoardHeader"><fmtSetTrans:message key="LBL_PART_TRANS_HEADER"/></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>
		<tr>
				<td colspan="4"> 
				<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
				<jsp:include page="/custservice/transfer/GmIncludeTransferHeader.jsp" />
				</td>
		</tr>
		<tr>
			<td height="30" class="RightTableCaption" ><font color="red">*</font><fmtSetTrans:message key="LBL_PART_NUMBER"/>:</td>
			<td>
				<input type="text" size="50" value="<%=strPartNums%>" name="Txt_PartNum" class="InputArea"   onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=9>
			</td>		
     		<td ><fmtSetTrans:message key="BTN_LOAD" var="varLoad"/> <gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button" onClick="javascript:fnGo();" tabindex="10" buttonType="Load" /> </td>
   			<td > </td>
		</tr>	
		<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>

		<% if(strAction.equals("LoadPartTransfer"))  { %>
		<tr>
					<td colspan="4"> 
					<DIV style="display:visible;height: 200px; overflow: auto;">
						<jsp:include page='<%=strCustTransferPath + "/GmIncludePartTransfer.jsp"%>' >
						<jsp:param name="PAGE" value="PART" />
						</jsp:include>
					</DIV>
					</td>
		</tr>
		<tr>
				<td colspan="4" > 
							<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="LogType" value="" />
							<jsp:param name="LogMode" value="Edit" />
							<jsp:param name="Mandatory" value="yes" />
							</jsp:include>
					</td>
		</tr>
		<tr>
		     		<td colspan="4"  align="center"><fmtSetTrans:message key="BTN_INITIATE" var="varInitiate"/><gmjsp:button value="&nbsp;&nbsp;${varInitiate}&nbsp;&nbsp;" gmClass="button" onClick="javascript:fnInitiatePartTransfer();" tabindex="25" buttonType="Save" /> </td>
		</tr>	
		
		
		<% } %>
		
	</table>
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
