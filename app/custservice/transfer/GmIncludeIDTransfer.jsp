
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtIncludeIDTransfer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmIncludeIDTransfer.jsp -->
<fmtIncludeIDTransfer:setLocale value="<%=strLocale%>"/>
<fmtIncludeIDTransfer:setBundle basename="properties.labels.custservice.transfer.GmApproveTransfer"/>

<% 
String strMode = GmCommonClass.parseNull(request.getParameter("LISTMODE"));
String strDTDateFmt = "{0,date,"+strGCompDateFmt+"}";
%>


<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>    
		
		<display:table name="ALTRANSFERLIST"  class="its" id="currentRowObject"   decorator="com.globus.displaytag.beans.DTFullTransferWrapper"> 
		<% if (strMode.equals("Initiate")) { %>
				<display:column property="CHKBOX" class="aligncenter" title="<input type=checkbox name=Chk_SelectAll checked='true' onClick=fnSelectAll(this);>"  />
		<% } %>				
				<fmtIncludeIDTransfer:message key="DT_TRANSACTION_ID" var="varTransactionID"/><display:column property="TXNID" title="${varTransactionID}" sortable="true"  />
				<fmtIncludeIDTransfer:message key="DT_TYPE" var="varType"/><display:column property="TYPE" title="${varType}" sortable="true"  />
				<fmtIncludeIDTransfer:message key="DT_DATE" var="varDate"/><display:column property="TXNDATE" title="${varDate}" class="alignleft"  format="<%=strDTDateFmt%>"  />
			  	<fmtIncludeIDTransfer:message key="DT_STATUS" var="varStatus"/><display:column property="TXNSTATUS" title="${varStatus}" class="alignleft" sortable="true"   />
			  	<fmtIncludeIDTransfer:message key="DT_SET_ID" var="varSetId"/><display:column property="SETID" title="${varSetId}" class="alignleft"   />
			</display:table>
			
			
			
			
			