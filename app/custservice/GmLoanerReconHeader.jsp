<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtLoanerReconHeader" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!--GmLoanerReconHeader.jsp  -->
<fmtLoanerReconHeader:setLocale value="<%=strLocale%>"/>
<fmtLoanerReconHeader:setBundle basename="properties.labels.custservice.GmLoanerRecon"/>  

<%
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");
String strTransId = (String)request.getAttribute("hTransId") == null?"":(String)request.getAttribute("hTransId");

String strSetId = "";
String strDesc = "";
String strConsignId = "";
String strType =  "";
String strPurpose = "";
String strUserName = "";
String strIniDate = "";
String strConsignType = "";

String strRequestorName = "";
String strTxnId = "";
String strTransType = "";

String strStatusFl ="";
String strShipFl = "";
String strSetName = "";
String strSetTypeId = "";
String strEtchId="";
String strTicketId = "";
String strTicketUrl = "";

String strRepId = "";
String strRepName = "";
String strDistId = "";
String strDistName = "";
String strRequestID = "";
String strRequestCnt = "";
String strTransferFl = "";
String strAssocRepName = "";


HashMap hmConsignDetails = new HashMap();

if (hmReturn != null)
{
		hmConsignDetails = (HashMap)hmReturn.get("CONDETAILS");

		strConsignId = GmCommonClass.parseNull((String)hmConsignDetails.get("CONSIGNID"));
		strTxnId = GmCommonClass.parseNull((String)hmConsignDetails.get("CID"));
		strSetName=  GmCommonClass.parseNull((String)hmConsignDetails.get("SNAME"));
		strPurpose =  GmCommonClass.parseNull((String)hmConsignDetails.get("PURP"));
		strType =  GmCommonClass.parseNull((String)hmConsignDetails.get("TYPE"));
		strUserName =  GmCommonClass.parseNull((String)hmConsignDetails.get("UNAME"));
		strIniDate =  GmCommonClass.parseNull((String)hmConsignDetails.get("CDATE"));
		strDesc =  GmCommonClass.parseNull((String)hmConsignDetails.get("COMMENTS"));
		strStatusFl =  GmCommonClass.parseNull((String)hmConsignDetails.get("SFL"));
		strTransType =  GmCommonClass.parseNull((String)hmConsignDetails.get("TYPE"));
		strRequestorName =  GmCommonClass.parseNull((String)hmConsignDetails.get("RNAME"));
		strSetTypeId = GmCommonClass.parseNull((String)hmConsignDetails.get("SETCTYPEID"));
		strEtchId = GmCommonClass.parseNull((String)hmConsignDetails.get("ETCHID"));
		strTicketId = GmCommonClass.parseNull((String)hmConsignDetails.get("TICKETID"));
		
		strRepId = GmCommonClass.parseNull((String)hmConsignDetails.get("REPID"));;
		strRepName = GmCommonClass.parseNull((String)hmConsignDetails.get("REPNAME"));;
		strDistId = GmCommonClass.parseNull((String)hmConsignDetails.get("DISTID"));;
		strDistName = GmCommonClass.parseNull((String)hmConsignDetails.get("DISTNM"));;
		strRequestID = GmCommonClass.parseNull((String)hmConsignDetails.get("REQID"));
		strRequestCnt = GmCommonClass.parseNull((String)hmConsignDetails.get("RECONCMTCNT"));
		strTransferFl = GmCommonClass.parseNull((String)hmConsignDetails.get("TRANSFER_FL"));
		strAssocRepName = GmCommonClass.parseNull((String)hmConsignDetails.get("ASSOCREPNM"));
		
		if(!strTicketId.equals("")){
			strTicketUrl = GmCommonClass.getJiraConfig("TICKET_URL")+strTicketId;
		}		
} 


%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Loaner Reconciliation Information</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmLoanerReconHeader.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<table width="100%" cellpadding="0" cellspacing="0">
<input type="hidden" name="hRepid" id="hRepid" value="<%=strRepId%>">
<input type="hidden" name="hDistid" id="hDistid" value="<%=strDistId%>">
<tr>
	<td class="RightTableCaption" HEIGHT="25" align="right" colspan="2" width="15%"><fmtLoanerReconHeader:message key="LBL_TRANSACTION_ID"/>:&nbsp;</td>
	<td class="RightText"><%=strTransId %></td>
	<td class="RightTableCaption" align="right" colspan="2"><fmtLoanerReconHeader:message key="LBL_TRANSACTION_TYPE"/>:&nbsp;</td>
	<td class="RightText"><%=strTransType %></td>
</tr>
<tr>
	<td colspan="6" height="1" bgcolor="#cccccc"></td>
</tr>
<tr class="Shade">
	<td class="RightTableCaption" align="right" colspan="2" HEIGHT="25"><fmtLoanerReconHeader:message key="LBL_PURPOSE"/>:&nbsp;</td>
	<td class="RightText"><%=strPurpose %></td>
	<td class="RightTableCaption" HEIGHT="25" align="right" colspan="2"><fmtLoanerReconHeader:message key="LBL_SET_CN_ID"/>:&nbsp;</td>
	<td class="RightText"><%=strConsignId %>&nbsp;&nbsp;
					<%if(strRequestCnt.equals("N")){ %>
					<fmtLoanerReconHeader:message key="IMG_RECONCILIATION_COMMENTS" var="varClickToAddRecComments"/>
						<img id="imgEdit" style="cursor:hand" src="<%=strImagePath%>/phone_icon.jpg" title="${varClickToAddRecComments}"
                             width="14" height="14" onClick="fnOpenLog('<%=strRequestID%>',4000316)"/>
					<%}else{ %>
					<fmtLoanerReconHeader:message key="IMG_RECONCILIATION_COMMENTS" var="varClickToAddRecComments"/>
						<img id="imgEdit" style="cursor:hand" src="<%=strImagePath%>/phone-icon_ans18.gif" title="${varClickToAddRecComments}" 
						     width="14" height="14" onClick="fnOpenLog('<%=strRequestID%>',4000316)"/>						
					<%}%>				
				<%if(strTransferFl.equals("Y")){ %>
					&nbsp;&nbsp;<a href=javascript:fnOpenUsageHistory('<%=strConsignId%>','<%=strEtchId%>');><font color="blue"><b><u><fmtLoanerReconHeader:message key="LBL_USAGE_HISTORY"/></u></b></font></a>
				<%}%>
		
	</td>
</tr>
<tr>
	<td  colspan="6" height="1" bgcolor="#cccccc"></td>
</tr>
<tr>
	<td class="RightTableCaption" align="right" HEIGHT="25" colspan="2"><fmtLoanerReconHeader:message key="LBL_SET_NAME"/>:&nbsp;</td>
	<td class="RightText"  width="30%"><%=strSetName %></td>
	<td class="RightTableCaption" align="right" colspan="2"><fmtLoanerReconHeader:message key="LBL_ETCH_ID"/>:&nbsp;</td>
	<td class="RightText"><%=strEtchId %></td>
</tr>
<tr>
	<td colspan="6" height="1" bgcolor="#cccccc"></td>
</tr>
<tr class="Shade">
	<td class="RightTableCaption" align="right" HEIGHT="25" colspan="2"><fmtLoanerReconHeader:message key="LBL_FIELD_SALES"/>:&nbsp;</td>
	<td class="RightText"  width="30%"><%=strDistName %></td>
	
	<td class="RightTableCaption" align="right" colspan="2"><fmtLoanerReconHeader:message key="LBL_SALES_REP"/>:&nbsp;</td>
	<td class="RightText"><%=strRepName %></td>
</tr>
<tr>
	<td colspan="6" height="1" bgcolor="#cccccc"></td>
</tr>
<tr >
	<td class="RightTableCaption" align="right" HEIGHT="25" colspan="2"><fmtLoanerReconHeader:message key="LBL_ASSOC_REP"/>:&nbsp;</td>
	<td class="RightText"  width="30%"><%=strAssocRepName %></td>
	
	<td class="RightTableCaption" align="right" colspan="2"><fmtLoanerReconHeader:message key="LBL_TICKET"/>:&nbsp;</td>
	<td class="RightText"><a target="new" href="<%=strTicketUrl%>"><%=strTicketId %></a></td>
</tr>

<tr>
	<td colspan="6" height="1" bgcolor="#cccccc"></td>
</tr>
<tr class="shade">
	<td  class="RightTableCaption" HEIGHT="25" align="right" colspan="2"><fmtLoanerReconHeader:message key="LBL_INITIATED_BY"/>:&nbsp;</td>
	<td class="RightText"><%=strUserName %></td>
	<td class="RightTableCaption" align="right" colspan="2"><fmtLoanerReconHeader:message key="LBL_DATE_INITIATED"/>:&nbsp;</td>
	<td class="RightText"><%=strIniDate %></td>
</tr>
<tr>
	<td  colspan="6" height="1" bgcolor="#cccccc"></td>
</tr>
<tr >
	<td class="RightTableCaption" HEIGHT="25" align="right" colspan="2"><fmtLoanerReconHeader:message key="LBL_COMMENTS"/>:&nbsp;</td>
	<td class="RightText"  nowrap="nowrap"><%=strDesc %></td>
</tr>
<tr><td></td></tr>
</table>
</BODY>