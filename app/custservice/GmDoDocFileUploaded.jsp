
<!-- \custservice\GmDoDocFileUploaded.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtDoDocFileUploaded" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtDoDocFileUploaded:setLocale value="<%=strLocale%>"/>
<fmtDoDocFileUploaded:setBundle basename="properties.labels.common.GmUpload"/>
<%    
response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8");
String strFormName = GmCommonClass.parseNull((String)request.getParameter("FORMNAME"));
String strRefId = GmCommonClass.parseNull(request.getParameter("refID"));
%>
<bean:define id="intResultSize" name="frmDODocumentUpload" property="intResultSize" type="java.lang.Integer"> </bean:define>
 
 
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: Files Uploaded</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<%-- <link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.css"> --%>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>


</HEAD>
<BODY>
<table  border="0" width="100%" cellspacing="0" cellpadding="0" >
	<%
	if(intResultSize >= 1) {%>
	<tr>
		<td>
			<div id="uploadedFilesGrid" class="grid"  height="200px"></div>
		</td>
	</tr>
	
	
	
	<%}else{%>
	<tr>
		<td height="25" align="center"><font color="blue"><fmtDoDocFileUploaded:message key="LBL_NO_FILES_UPLOADED"/></font></td>
	</tr>
	<%} %>  

</table>			
<%@ include file="/common/GmFooter.inc"%>
</BODY>
