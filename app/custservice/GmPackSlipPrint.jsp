<%
/**********************************************************************************
 * File		 		: GmPackSlipPrint.jsp
 * Desc		 		: This screen is used for Printing Pack Slip
 * Version	 		: 1.0
 * author			: Pramaraj
************************************************************************************/
%>
<!-- \sales\AreaSet\GmAddInfo.jsp -->
<%@ include file="/common/GmHeader.inc" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%
try { 
	
	
	
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Globus Medical: Order - Print Pack Slip</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css"/>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script>
function fnPrint()
{
	window.print();
}
function hidebuttons()
{
 	divObj=document.getElementById("Duplicate");
	divObj.style.display='none';
//	window.print();
}
function hidePrintMain()
{
	divObj=document.getElementById("Duplicate");
	divObj.style.display='block';
	buttonTdObj=document.getElementById("buttonTd");
	buttonTd.style.display='none';

}

function showPrintMain()
{
	divObj=document.getElementById("Duplicate");
	divObj.style.display='none';
	buttonTdObj=document.getElementById("buttonTd");
	buttonTd.style.display='block';
	
}
</script>
</head>
<BODY topmargin="10" onload="hidebuttons()" onbeforeprint="hidePrintMain();" onafterprint="showPrintMain();">

		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
		<div>
			<jsp:include page="/custservice/GmPackSlip.jsp" />			
			</div>
			<!--  <p STYLE="page-break-after: always"></p>-->
			<div id="Duplicate">
			<p><br style="page-break-before: always;" clear="all" /></p>
				<jsp:include page="/custservice/GmPackSlip.jsp" />
				
				</div>
	
		
<table id="buttonTd" border="0" width="700" cellspacing="0" cellpadding="0" align="center" >
		<tr>
			<td  align="center" height="30"  width="700">
				<gmjsp:button value="&nbsp;Print&nbsp;" name="Btn_PrintMain" gmClass="button" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				
				<gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_CloseMain" gmClass="button" onClick="window.close();" buttonType="Load" />
			</td>
		<tr>
</table>

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</body>
</html>