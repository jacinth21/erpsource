<!DOCTYPE html>
<!-- \custservice\GmNPIProcess.jsp -->
<%@ taglib prefix="fmtCart" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ include file="/common/GmHeader.inc" %>
<html lang="en">
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
}
String strScreenType = GmCommonClass.parseNull(request.getParameter("SCREENTYPE"));
String strSalesRepId = GmCommonClass.parseNull(request.getParameter("SALESREPID"));
%>
<fmtCart:requestEncoding value="UTF-8" />
<fmtCart:setLocale value="<%=strLocale%>"/>
<fmtCart:setBundle basename="properties.labels.common.cart.GmCommonCart"/>
<head>
<meta charset="UTF-8">

<title>NPI Details</title>

<link rel="stylesheet" href="<%=strCssPath%>/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=strJsPath%>/common/bootstrap.min.css">
<!-- <link rel="stylesheet" href="<%=strCssPath%>/font-awesome.css"> -->
<link rel="stylesheet" href="<%=strCssPath%>/GmNPIProcess.css">
<!-- <script src="<%=strJsPath%>/common/jquery.min.js"></script> -->
<script src="<%=strJsPath%>/common/bootstrap.min.js"></script>
<script type="text/javascript">

var salesRepId = '<%=strSalesRepId%>';

function fnSetNPIId(obj){
	var v_npiId = document.getElementById("surgeonName").value;
	document.getElementById("searchnpiNum").value = v_npiId;
	$("#searchsurgeonName").focus();
}

function fnSetSurgeonName(obj){
	var v_surgeonName = document.getElementById("npiNum").value;
	document.getElementById("searchsurgeonName").value = v_surgeonName;
}
</script>
<style type="text/css">
</style>
</head>
<body>

<input type="hidden" name="hRefId" id="hRefId" value="">
<input type="hidden" name="hAccountId" id="hAccountId" value="">
<input type="hidden" name="hFlag" id="hFlag" value="N">
<input type="hidden" name="hNpiPareTranId" id="hNpiPareTranId" value="">
<input type="hidden" name="hScreen" id="hScreen" value="">

	<% if(!strScreenType.equals("Modify")){ %>	
		<fmtCart:message key="BTN_NPI_TITLE" var="varTitle"/>
		<button  data-toggle="modal" data-target="#myModal" type="button" title="${varTitle}" id="npiBtn" >
		<a href="#"> <span class="glyphicon glyphicon-new-window add-surgeon-icon" title='Add more surgeon details'></span> </a> 
		</button>
	<%} %>
	
	<div id="myModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header modal-header-bg">
					<button type="button" class="close main-close" data-dismiss="modal">&times;</button>
					<label class="main-quest" title="* Press Enter key in surgeon name to save records &#13;* Click delete icon to remove surgeon details" >&quest;</label>
               		<h4 class="modal-title" id="npilbl"></h4>
				</div>
				<div class="modal-body modal-body-grp data-grp">
					<!-- <div class="form-group">
	                	<div class="col-sm-10">
	                		<div class="field-grp">
	                			<label> -->
	                		<div class="div-new-npi-dtls">
	                			<div class="label-grp lbl-curr-dtls">
	                				<label id="lblNpiNum"> NPI #</label>
	                				<label id="lblSurgeonName"> Surgeon Name</label>
	                			</div>
	                			<div class="field-grp">
		                			<!-- <input name="npiNum" id="npiNum" type="text" size="23%"> -->
		                			<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
										<jsp:param name="CONTROL_NAME" value="npiNum" />
										<jsp:param name="METHOD_LOAD" value="loadSurgeonName&strOpt=npilist" />
										<jsp:param name="WIDTH" value="157" />
										<jsp:param name="CSS_CLASS" value="search" />
										<jsp:param name="CONTROL_NM_VALUE" value="" />
										<jsp:param name="CONTROL_ID_VALUE" value="" />
										<jsp:param name="SHOW_DATA" value="40" />
										<jsp:param name="AUTO_RELOAD" value="fnSetSurgeonName(this);" />
									</jsp:include>	
		                			<!-- <input name="surgeonName" id="surgeonName" type="text" size="35%"> -->
		                			<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
										<jsp:param name="CONTROL_NAME" value="surgeonName" />
										<jsp:param name="METHOD_LOAD" value="loadSurgeonName&strOpt=surgeonlist" />
										<jsp:param name="WIDTH" value="250" />
										<jsp:param name="CSS_CLASS" value="search" />
										<jsp:param name="CONTROL_NM_VALUE" value="" />
										<jsp:param name="CONTROL_ID_VALUE" value="" />
										<jsp:param name="SHOW_DATA" value="40" />
										<jsp:param name="AUTO_RELOAD" value="fnSetNPIId(this);" />
									</jsp:include>	
									&nbsp;&nbsp;&nbsp;
		                			<button class="btn btn-primary btn-xs btn-add" type="button" title="${varTitle}" id="addbtn" >
		                			Add
		                			</button>		                			
		                		</div>
		                		<!-- </label> -->
		                	<!-- </div> -->
		                		<div>
		                			<label id="lblSurgDtls"> &nbsp;Added Surgeon details:</label>
		                		</div>
			                	<div class="values-grp" id="selectedData">
			                		<ul id="ulCurrDtls"></ul>
			                	</div>
			                </div>
		                	<div>
		                		<label id="lblPrevDtls"> Most Recent Surgeon Details</label> <label id="lblPrevSubDtls">(Please select surgeon details from below to add)</label>
		                	</div>
		                	<div class="label-grp lbl-prev-dtls">
                				<label id="lblNpiNum"> NPI #</label>
                				<label id="lblSurgeonName"> Surgeon Name</label>
                			</div>
		                	<div class="history-values" id="historyDtls">
		                		<ul id="ulPreDtls"></ul>
		                	</div>
	                   <!--  </div>
	                </div> -->
				</div>
			</div>
		</div>
	</div>
<script>
var closebtns = document.getElementsByClassName("close-sm");
var i;
var entrycnt = 0;
var parentDataSetCnt = 0;
var savedNpiIdStr = '|';

$('#myModal').on('shown.bs.modal', function (e) {
	var refId = document.getElementById("hRefId").value;
	var v_screen = document.getElementById("hScreen").value;
	var txtOrderIdObj = document.all.Txt_OrdId;
	
	if(refId == ''){
		document.getElementById("hRefId").value = (txtOrderIdObj != undefined && txtOrderIdObj != '')? txtOrderIdObj.value: $('#ordId').text();	
		refId = document.getElementById("hRefId").value; 
	}
	$("#npilbl").empty();
	$("#npilbl").append("NPI Details - "+ refId);
	
	document.getElementById("hAccountId").value = document.all.Txt_AccId.value;
	document.getElementById("hFlag").value= 'Y';
	// Need to set the NPI Details from parent screen first time
	if(v_screen != "EditVal"){
		fnSetParentScreenData();
	}
	
	if(v_screen == "EditVal"){
		fnFetchData();
	}
	savedNpiIdStr = '|';
	fnFetchHistoryDtls();
})

$('#myModal').on('hidden.bs.modal', function (e) {
	document.getElementById("hFlag").value= 'N';
})

 /* $(document).ready(function(){ */
var npiTxnId;
 
$("#searchsurgeonName,#searchnpiNum").keypress(function(e){
	var key = e.which;
  	if(key == 13){
   		callbackDispNPIDtls();
  	}
});
   
$("#addbtn").click(function(){
	callbackDispNPIDtls();
});
	
var callbackDispNPIDtls = function(){
	
   	var dataAppend;
   	var parentNpiIdObj = document.getElementById("searchTxt_Npi");
   	var parentSurgeonNmObj = document.getElementById("searchTxt_Surgeon");
   	var tempNpiData = '';
   	var acceptNum;
   	var errorFl;
   	// If the value is already availabe in the div, should not add it again
   	if($('#searchnpiNum').val() != ''){
   		acceptNum = /[^0-9]/g;
        errorFl = acceptNum.test($('#searchnpiNum').val());
        if (errorFl) {  /*Accept only Digits in NPI# field*/
        	Error_Details("Please provide NPI# in digits");
        }
   		tempNpiData = tempNpiData + $('#searchnpiNum').val();
   	}
   	if($('#searchnpiNum').val() != '' && $('#searchsurgeonName').val() != ''){
   		tempNpiData = tempNpiData + " - ";
   	}
   	if($('#searchsurgeonName').val() != ''){
   		acceptNum = /^[a-zA-Z\.\, ]*$/;
        errorFl = acceptNum.test($('#searchsurgeonName').val());
        if (!errorFl) {  /*Accept only Digits in NPI# field*/
        	Error_Details("Please provide surgeon name in letters");
        }
   		tempNpiData = tempNpiData + $('#searchsurgeonName').val();
   	}
   	if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}
   	
   	if(savedNpiIdStr.indexOf(tempNpiData) != -1){
   		$("#searchnpiNum").val('');
   	   	$("#searchsurgeonName").val('');
   	 	$("#searchnpiNum").focus();
   		return;
   	}
   	
   	fnSaveRow('','');
   	
   	if($('#searchnpiNum').val() != '' || $('#searchsurgeonName').val() != ''){
   		dataAppend = "<li id='currDtls' style=\"list-style-type: none;\"><div>";	
   	}
   	if($('#searchnpiNum').val() != ''){
   		dataAppend = dataAppend + $('#searchnpiNum').val();
   		savedNpiIdStr = savedNpiIdStr + $('#searchnpiNum').val();
   	}
   	if($('#searchnpiNum').val() != '' && $('#searchsurgeonName').val() != ''){
   		dataAppend = dataAppend + " - ";
   		savedNpiIdStr = savedNpiIdStr + " - ";
   	}
   	if($('#searchsurgeonName').val() != ''){
   		dataAppend = dataAppend + $('#searchsurgeonName').val();
   		savedNpiIdStr = savedNpiIdStr + $('#searchsurgeonName').val();
   	}
   	if($('#searchnpiNum').val() != '' || $('#searchsurgeonName').val() != ''){
   		savedNpiIdStr = savedNpiIdStr + '|';
   		dataAppend = dataAppend + "&nbsp;&nbsp;&nbsp;<span class='close-sm' id='closebtn'><a href=\"#\"><span class=\"glyphicon glyphicon-remove\" title='Remove' width='15' height='15'></span></a></span>&nbsp;&nbsp;";
   		dataAppend = dataAppend + " <input type=\"hidden\" name=\"hNpiTxnId\" id=\"hNpiTxnId\" value="+npiTxnId+"></div></li>";
   	}

   	$("#selectedData ul").append(dataAppend);
   	/* If the value is not entered in NPI/Surgeon Name field in Parent order screen, display the first value from Popup */
   	if(entrycnt == 0){
   		if((parentNpiIdObj != undefined && parentNpiIdObj.value == '') && (parentSurgeonNmObj != undefined && parentSurgeonNmObj.value == '')){
   			parentNpiIdObj.value = $('#searchnpiNum').val();
   			parentSurgeonNmObj.value = $('#searchsurgeonName').val();
   			entrycnt ++;
   		}
   	}
   	
   	$("#searchnpiNum").focus();
   	
   	for (i = 0; i < closebtns.length; i++) {
   		closebtns[i].onclick = function(rownum) {
   	      fnRemove($(this));
   		}
   	}
    	
   	$("#searchnpiNum").val('');
   	$("#searchsurgeonName").val('');
};

// To save each row whenever added
function fnSaveRow(npiId, npiName){

		var v_npiId = (npiId != '' && npiId != undefined)? npiId: document.getElementById("searchnpiNum").value;
		var v_surgeonNm = (npiName != '' && npiName != undefined)? npiName: document.getElementById("searchsurgeonName").value;
  	
    	$.ajax({
			url : "/gmNPIProcess.do?method=saveNPITrasactionDetails&submitType=<%=strScreenType%>&"+ fnAppendCompanyInfo(),
			type : "POST",
			async: false,
			data: {
	        	txnid : document.getElementById("hRefId").value,
	        	accountid : document.getElementById("hAccountId").value,
	        	npinum : v_npiId,
	        	surgeonname : v_surgeonNm
	        },
	        dataType: "json",

			success : function(data) {
				
				// to validate the data and show the message
				if (!data.length) {
					npiTxnId = data;
				} else {
				}
			}
		});
 }

// To save the value from parent screen
function fnSaveParentRow(vaction){  
	var txtOrderIdObj = document.getElementById("Txt_OrdId");
   	var v_hOrderid = document.getElementById("hOrderId") != undefined ? document.getElementById("hOrderId").value : '';
   	var v_txnid = txtOrderIdObj != undefined ? txtOrderIdObj.value : $('#ordId').text();
   	
   	var input = new Object();
   	input.npitranid = document.getElementById("hNpiPareTranId").value;
   	input.txnid  = v_txnid;
   	input.accountid = document.getElementById("Txt_AccId").value;
   	input.npinum = document.getElementById("searchTxt_Npi").value;
   	input.surgeonname = document.getElementById("searchTxt_Surgeon").value;  
   	if(input.npinum.length > 0 || input.surgeonname.length > 0 ) {	
   		$.ajax({
			url : "/gmNPIProcess.do?method=saveNPITrasactionDetails&hSaveAction="+vaction+"&"+ fnAppendCompanyInfo(),
			type : "POST",
			async: false,
			data: {
				npitranid : document.getElementById("hNpiPareTranId").value,
	        	txnid : v_txnid,
	        	accountid : document.getElementById("Txt_AccId").value,
	        	npinum : document.getElementById("searchTxt_Npi").value,
	        	surgeonname : document.getElementById("searchTxt_Surgeon").value,
	        	htxnId : v_hOrderid
	        },
	        dataType: "json",

			success : function(data) {
				// to validate the data and show the message
				if (!data.length) {
					npiTxnId = data;					
					document.getElementById("hNpiPareTranId").value = npiTxnId;
				} else {
				}
			}
		});
	}	
 }

// To remove the NPI details on click of remove icon
function fnRemove(obj){
	var parentNpiTxnIdObj = document.getElementById("hNpiPareTranId");
	var parentNpiTxnId = parentNpiTxnIdObj != undefined? parentNpiTxnIdObj.value:'';
	var npitxnId = $(obj).parent().find("#hNpiTxnId").val();
	var remTxtVal = $(obj).parent().text().trim();
	$.ajax({
		url : "/gmNPIProcess.do?method=cancelNPITrasactionDetails&"+ fnAppendCompanyInfo(),
		type : "POST",
		async: false,
		dataType: "json",
        data: {
        	npitranid: npitxnId
        },
		
		success : function(data) {
			// to validate the data and show the message
			if (!data.length) {
			} else {
			}
		}
	});
	$(obj).parent().parent().remove();
	// Remove the deleted value from string also to add it again in the div
	if(savedNpiIdStr != ''){
		savedNpiIdStr = savedNpiIdStr.replace(remTxtVal+'|','');
	}
	// If the first row is deleted, then next value should display in the text box of parent screen
	if(parentNpiTxnIdObj != undefined && (parentNpiTxnId == '' || parentNpiTxnId == npitxnId)){
		fnUpdateParentField();	
	}
}
var prevNPIid = "";
var prevSurgeonname = "";
// To set the NPI details from parent screen to Pop up
function fnSetParentScreenData(){
	var dataAppend;
	var parentNpiId = document.getElementById("searchTxt_Npi").value;
	var parentSurgeonNm = document.getElementById("searchTxt_Surgeon").value;
	var parentNpiTransId = document.getElementById("hNpiPareTranId").value;
	var strToReplace = '';
	
	if(prevNPIid == '' && prevSurgeonname == '' ) {
		//$("#selectedData ul").empty();
		if(parentNpiId != '' || parentSurgeonNm != ''){
			dataAppend = "<li id='currDtls'><div>";	
		}
		if(parentNpiId != ''){
			dataAppend = dataAppend + parentNpiId;
			savedNpiIdStr = savedNpiIdStr + parentNpiId;
		}
		if(parentNpiId != '' && parentSurgeonNm != ''){
			dataAppend = dataAppend + " - ";
			savedNpiIdStr = savedNpiIdStr +  " - ";
		}
		if(parentSurgeonNm != ''){
			dataAppend = dataAppend + parentSurgeonNm;
			savedNpiIdStr = savedNpiIdStr + parentSurgeonNm;
		}
		if(parentNpiId != '' || parentSurgeonNm != ''){
			savedNpiIdStr = savedNpiIdStr + '|';
			dataAppend = dataAppend + "&nbsp;&nbsp;&nbsp;<span class=\"close-sm\" id=\"closebtn\"><a href=\"#\"><span class=\"glyphicon glyphicon-remove\" title='Remove' width='15' height='15'></span></a></span>&nbsp;&nbsp;";
			dataAppend = dataAppend + " <input type=\"hidden\" name=\"hNpiTxnId\" id=\"hNpiTxnId\" value="+parentNpiTransId+"></div></li>";
		}
		$("#selectedData ul").append(dataAppend);
		
	} else if (prevNPIid != parentNpiId && parentSurgeonNm != prevSurgeonname){
		
		if(parentNpiId != ''){
			strToReplace = strToReplace + parentNpiId;
		}
		if(parentNpiId != '' && parentSurgeonNm != ''){
			strToReplace = strToReplace + " - ";
		}
		if(parentSurgeonNm != ''){
			strToReplace = strToReplace + parentSurgeonNm;
			strToReplace = strToReplace + "&nbsp;&nbsp;&nbsp;<span class=\"close-sm\" id=\"closebtn\"><a href=\"#\"><span class=\"glyphicon glyphicon-remove\" title='Remove' width='15' height='15'></span></a></span>";
			strToReplace = strToReplace + "<input type=\"hidden\" name=\"hNpiTxnId\" id=\"hNpiTxnId\" value="+parentNpiTransId+">";
		}
		if(parentNpiId != '' || parentSurgeonNm != ''){
			$("#currDtls div").each(function(){
				var currentText = $(this).text();
				$(this).html(currentText.replace(prevNPIid+" - "+prevSurgeonname ,strToReplace));
			});
		}
	} 
	prevNPIid = parentNpiId;
	prevSurgeonname = parentSurgeonNm;
}

// To load the saved details to the popup, if loading from Modify Order screen
function fnFetchData(){
	var dataAppend;
	$("#selectedData ul").empty();
	$.ajax({
		url : "/gmNPIProcess.do?method=fetchNPITrasactionDetails&"+ fnAppendCompanyInfo(),
		type : "POST",
		async: false,
		data: {
			txnid: document.getElementById("hRefId").value
        },
        dataType: "json",

		success : function(data) {
			if (data.length) {
				 $.each(data, function(index, element) {
					var id = element.NPTTXNID;
					var npiId = element.NPIID;
					var sugeonNm = element.SURGEONNM;
					
					if(npiId != '' || sugeonNm != ''){
			    		dataAppend = "<li id='currDtls'><div>";	
			    	}
					if(npiId != ''){
						dataAppend = dataAppend + npiId;
						savedNpiIdStr = savedNpiIdStr + npiId;
					}
					if(npiId != '' && sugeonNm != ''){
						dataAppend = dataAppend + " - ";
						savedNpiIdStr = savedNpiIdStr + " - ";
					}
					if(sugeonNm != ''){
						dataAppend = dataAppend + sugeonNm;
						savedNpiIdStr = savedNpiIdStr + sugeonNm;
					}
					if(npiId != '' || sugeonNm != ''){
						savedNpiIdStr = savedNpiIdStr + '|';
						dataAppend = dataAppend + "&nbsp;&nbsp;&nbsp;<span class=\"close-sm\" id=\"closebtn\"><a href=\"#\"><span class=\"glyphicon glyphicon-remove\" title='Remove' width='15' height='15'></span></a></span>&nbsp;&nbsp;";
						dataAppend = dataAppend + " <input type=\"hidden\" name=\"hNpiTxnId\" id=\"hNpiTxnId\" value="+id+"></div></li>";
					}
					$("#selectedData ul").append(dataAppend);
					
				}); 
				 
				 for (i = 0; i < closebtns.length; i++) {
						closebtns[i].onclick = function(rownum) {
					      fnRemove($(this));
						}
					}
			}
		},
		error: function(jqXHR, exception){
		
		}
	});
}

/* If the first row is deleted, then next value should display in the text box of parent screen */
function fnUpdateParentField(){
	var parentNpiIdObj = document.getElementById("searchTxt_Npi");
	var parentSurgeonNmObj = document.getElementById("searchTxt_Surgeon");
	if(parentNpiIdObj == undefined && parentSurgeonNmObj == undefined){
		return;
	}
	var parentNpiTxnIdObj = document.getElementById("hNpiPareTranId");
	$.ajax({
		url : "/gmNPIProcess.do?method=fetchNPITempDetails&"+ fnAppendCompanyInfo(),
		type : "POST",
		async: false,
		data: {
			txnid: document.getElementById("hRefId").value
        },
        dataType: "json",

		success : function(data) {
			if (data.length) {
				 $.each(data, function(index, element) {
					var id = element.NPTTXNID;
					var npiId = element.NPIID;
					var sugeonNm = element.SURGEONNM;
		    		parentNpiIdObj.value = npiId;
		    		parentSurgeonNmObj.value = sugeonNm;
		    		parentNpiTxnIdObj.value = id;
		    		return false;	
				}); 
				
			}else{
				parentNpiIdObj.value = '';
	    		parentSurgeonNmObj.value = '';
	    		parentNpiTxnIdObj.value = '';
	    		entrycnt--;
			}
		}
	});
}

// To fetch the history details to the previous surgeon details section
function fnFetchHistoryDtls(){
	var dataAppend;
	var v_rep_id = document.getElementById("hRepId") != undefined ? document.getElementById("hRepId").value : salesRepId;
	$("#historyDtls ul").empty();
	$.ajax({
		url : "/gmNPIProcess.do?method=fetchNPIHistoryDtls&"+ fnAppendCompanyInfo(),
		type : "POST",
		async: false,
		data: {
			txnid: document.getElementById("hRefId").value,
			repid: v_rep_id
        },
        dataType: "json",

		success : function(data) {
			if (data.length) {
				 $.each(data, function(index, element) {
					//var id = element.NPTTXNID;
					var npiId = element.NPIID;
					var sugeonNm = element.SURGEONNM;
					if(npiId != '' || sugeonNm != ''){
			    		dataAppend = "<li id='preDtls'> <input type=checkbox id='chkbox' onclick='fnAddRemoveVal(this);'>";
			    		dataAppend = dataAppend + "<div id='preNpiId'>";
			    		dataAppend = dataAppend + npiId;
			    		dataAppend = dataAppend + "</div><div id='preSurgNm'>";
			    		dataAppend = dataAppend + sugeonNm;
			    		dataAppend = dataAppend + "</div>";
			    		dataAppend = dataAppend + "</li>";
			    	}
					$("#historyDtls ul").append(dataAppend);
					
				}); 
				 
				 for (i = 0; i < closebtns.length; i++) {
						closebtns[i].onclick = function(rownum) {
					      fnRemove($(this));
						}
					}
			}
		},
		error: function(jqXHR, exception){
		
		}
	});
}

//To add the values from the previous surgeon details to the current transaction
function fnAddRemoveVal(obj){
	if(obj.checked){
		var npiId = $(obj).parent().find("#preNpiId").text();
		var surgeonNm = $(obj).parent().find("#preSurgNm").text();
		// To add the values from the previous surgeon details to the current transaction
		fnAddToList(npiId, surgeonNm);
	}
}

// To add the values from the previous surgeon details to the current transaction
function fnAddToList(npiId, surgeonNm){
	var dataAppend;
	var parentNpiIdObj = document.getElementById("searchTxt_Npi");
	var parentSurgeonNmObj = document.getElementById("searchTxt_Surgeon");
	var tempNpiData = '';
	
	if(npiId != '')
   		tempNpiData = tempNpiData + npiId;
   	if(npiId != '' && surgeonNm != '')
   		tempNpiData = tempNpiData + " - ";
   	if(surgeonNm != '')
   		tempNpiData = tempNpiData + surgeonNm;
   	
   	if(savedNpiIdStr.indexOf(tempNpiData) != -1){
   		return;
   	}
	
	fnSaveRow(npiId,surgeonNm);
	
	if(npiId != '' || surgeonNm != ''){
		dataAppend = "<li id='currDtls' style=\"list-style-type: none;\"><div>";	
	}
	if(npiId != ''){
		dataAppend = dataAppend + npiId;
		savedNpiIdStr = savedNpiIdStr + npiId;
	}
	if(npiId != '' && surgeonNm != ''){
		dataAppend = dataAppend + " - ";
		savedNpiIdStr = savedNpiIdStr + " - ";
	}
	if(surgeonNm != ''){
		dataAppend = dataAppend + surgeonNm;
		savedNpiIdStr = savedNpiIdStr + surgeonNm;
	}
	if(npiId != '' || surgeonNm != ''){
		savedNpiIdStr = savedNpiIdStr + '|';
		dataAppend = dataAppend + "&nbsp;&nbsp;&nbsp;<span class='close-sm' id='closebtn'><a href=\"#\"><span class=\"glyphicon glyphicon-remove\" title='Remove' width='15' height='15'></span></a></span>&nbsp;";
		dataAppend = dataAppend + " <input type=\"hidden\" name=\"hNpiTxnId\" id=\"hNpiTxnId\" value="+npiTxnId+"></div></li>";
	}
	
	$("#selectedData ul").append(dataAppend);
	
	/* If the value is not entered in NPI/Surgeon Name field in Parent order screen, display the first value from Popup */
   	if(entrycnt == 0){
   		if((parentNpiIdObj != undefined && parentNpiIdObj.value == '') && (parentSurgeonNmObj != undefined && parentSurgeonNmObj.value == '')){
   			parentNpiIdObj.value = npiId;
   			parentSurgeonNmObj.value = surgeonNm;
   		}
   		entrycnt++;
   	}
	
	for (i = 0; i < closebtns.length; i++) {
		closebtns[i].onclick = function(rownum) {
	      fnRemove($(this));
		}
	}
}
	
/* }); */

</script>
</body>
<script src="<%=strJsPath%>/common/bootstrap.min.js"></script>
</html>