<!--\CS Label changes\custservice\GmAddUsageDetails.jsp  -->
<%@page import="java.util.HashMap"%>
<%
/**********************************************************************************
 * File		 		: GmAddUsageDetails.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Dhana Reddy
************************************************************************************/
%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
 
<bean:define id="repID" name="frmOrderDetails" property="repID" type="java.lang.String"></bean:define>
<bean:define id="caseID" name="frmOrderDetails" property="caseID" type="java.lang.String"></bean:define>
<bean:define id="accountID" name="frmOrderDetails" property="accountID" type="java.lang.String"></bean:define>
<bean:define id="accountNm" name="frmOrderDetails" property="accountNm" type="java.lang.String"></bean:define>
<bean:define id="orderID" name="frmOrderDetails" property="orderID" type="java.lang.String"></bean:define>
<bean:define id="strOpt" name="frmOrderDetails" property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="alResult" name="frmOrderDetails" property="alResult" type="java.util.ArrayList"></bean:define>
<bean:define id="alReturn" name="frmOrderDetails" property="alReturn" type="java.util.ArrayList"></bean:define>
<bean:define id="usage" name="frmOrderDetails" property="usage" type="java.lang.String"></bean:define>
<bean:define id="alUsage" name="frmOrderDetails" property="alUsage" type="java.util.ArrayList"></bean:define>
<bean:define id="orderTypeNm" name="frmOrderDetails" property="orderTypeNm" type="java.lang.String"></bean:define>
<bean:define id="caseInfoID" name="frmOrderDetails" property="caseInfoID" type="java.lang.String"></bean:define>
<bean:define id="hmResult" name="frmOrderDetails" property="hmResult" type="java.util.HashMap"></bean:define>
<bean:define id="parentOrderID" name="frmOrderDetails" property="parentOrderID" type="java.lang.String"></bean:define>
<bean:define id="custPO" name="frmOrderDetails" property="custPO" type="java.lang.String"></bean:define>
<bean:define id="surgeryDt" name="frmOrderDetails" property="surgeryDt" type="java.util.Date"></bean:define>
<bean:define id="applnDateFmt" name="frmOrderDetails" property="applnDateFmt" type="java.lang.String"> </bean:define>
<bean:define id="alNonUsage" name="frmOrderDetails" property="alNonUsage" type="java.util.ArrayList"> </bean:define>
<bean:define id="alSetType" name="frmOrderDetails" property="alSetType" type="java.util.ArrayList"> </bean:define>
<bean:define id="setType" name="frmOrderDetails" property="setType" type="java.lang.String"> </bean:define>
<bean:define id="npiMandatoryFl" name="frmOrderDetails" property="npiMandatoryFl" type="java.lang.String"> </bean:define>
<bean:define id="orderMode" name="frmOrderDetails" property="orderMode" type="java.lang.String"> </bean:define>
<bean:define id="cbo_repName" name="frmOrderDetails" property="cbo_repName" type="java.lang.String"> </bean:define>
<bean:define id="alEGPSUsage" name="frmOrderDetails" property="alEGPSUsage" type="java.util.ArrayList"> </bean:define>

<%@ taglib prefix="fmtAddUsageDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtAddUsageDetails:setLocale value="<%=strLocale%>"/>
<fmtAddUsageDetails:setBundle basename="properties.labels.custservice.GmAddUsageDetails"/>
<%
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(strGCompDateFmt);
GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmAddUsageDetails", strSessCompanyLocale);
GmCalenderOperations gmCal = new GmCalenderOperations();
String strSurgeryDt =sdf.format(surgeryDt);
int intTRCount = 10;
if(alResult.size()>10){
	intTRCount = alResult.size();
}

String strHeader=GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_ADD_USAGE_DETAILS"));
if(strOpt.equals("edit")){
	strHeader=GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_EDIT_USAGE_DETAILS"));
}
String strRegn = "";
String strAdName="";
String strDistName="";
String strTerrName="";
String strRepName = "";
String strRepId ="";
String strGpoNm ="";
String strGpoID ="";
String strUsageFree ="";
String strAccCurrSymb = "";
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
String strSessDeptId = GmCommonClass.parseNull((String)session.getAttribute("strSessDeptId"));
String strCurrSym =  GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
String strApplnDateFmt = strGCompDateFmt;
String strEgpsFl = "";
//to setting the time zone
gmCal.setTimeZone(strGCompTimeZone);
String strSessTodaysDate = gmCal.getCurrentDate(strGCompDateFmt);
HashMap hmAccsum = new HashMap();
if(hmResult != null){
hmAccsum = (HashMap)hmResult.get("ACCSUM");
strRegn = GmCommonClass.parseNull((String)hmAccsum.get("RGNAME"));
strAdName = GmCommonClass.parseNull((String)hmAccsum.get("ADNAME"));
strDistName = GmCommonClass.parseNull((String)hmAccsum.get("DNAME"));
strTerrName = GmCommonClass.parseNull((String)hmAccsum.get("TRNAME"));
strRepName = GmCommonClass.parseNull((String)hmAccsum.get("RPNAME"));
strRepId = GmCommonClass.parseNull((String)hmAccsum.get("REPID"));
strGpoNm = GmCommonClass.parseNull((String)hmAccsum.get("PRNAME"));
strGpoID = GmCommonClass.parseNull((String)hmAccsum.get("PRID"));
strAccCurrSymb = GmCommonClass.parseNull((String)hmAccsum.get("ACC_CURRENCY"));
strCurrSym = strAccCurrSymb.equals("")?strCurrSym:strAccCurrSymb;
strEgpsFl = GmCommonClass.parseNull((String)hmAccsum.get("EGPSFL"));
}
String strEGPSUsage = "0"; // PC-5892 default value set 'No eGPS' for Capital Equp. Used
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Order Usage DEtails </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javaScript" src="<%=strcustserviceJsPath%>/GmAddUsageDetails.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js" type="text/javascript"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js" type="text/javascript"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"  type="text/javascript"></script>	
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">
	
<script>
var count =0;
var orderid = '<%=orderID%>';
var repId='<%=repID%>';
var accID='<%=accountID%>';
var gpoid='<%=strGpoID%>';
var parentOrdId = '<%=parentOrderID%>';
var caseinfoid = '<%=caseInfoID%>';
var alResultSize = <%=alResult.size()%>;
var usageLen = <%=alUsage.size()%>;
var usageSelect = '<%=usage%>';
var sessCurrSym = '<%=strCurrSym%>';
var alSetTypeLen = <%=alSetType.size()%>;
var setTypeSelect = '<%=setType%>';
var vNPIMandatoryFl  = '<%=npiMandatoryFl%>';
if(alResultSize >10)
	{
	count = alResultSize; 
	}else{
	 count = <%=intTRCount%>;
	}
var trcnt = 0;
var sessDeptID = '<%=strSessDeptId%>';
var strOpt = '<%=strOpt%>';
// to hashmap search
var alPartDetail = <%=alReturn.size()%>;
var orderMode = '<%=orderMode%>';
var cbo_repName = "<%=cbo_repName%>";
var strApplnDateFmt = '<%=strApplnDateFmt%>';
var egpsFl = '<%=strEgpsFl%>';
</script>
<%
HashMap hmPartDetail = new HashMap();
for (int i=0;i<alReturn.size();i++)
{
	hmPartDetail = (HashMap)alReturn.get(i);
	String strPartNum = (String)hmPartDetail.get("PNUM");
%>
<script>put('<%=strPartNum%>','<%=hmPartDetail.get("PDESC")%>^<%=hmPartDetail.get("PRICE")%>');</script>
<%
	}
%> 

<%
HashMap hmNonUsageParts = new HashMap();
for (int i=0;i<alNonUsage.size();i++)
{
	hmNonUsageParts = (HashMap)alNonUsage.get(i);
	String strPartNum = (String)hmNonUsageParts.get("ID");
%>
<script>put('<%=strPartNum%>','<%=strPartNum%>');</script>
<%
	}
%>
<script>
<%
HashMap hmUsageDrop = new HashMap();
for(int i=0; i<alUsage.size(); i++){
	hmUsageDrop = (HashMap)alUsage.get(i);
%>
var UsageArr<%=i%> = "<%=hmUsageDrop.get("CODEID")%> , <%=hmUsageDrop.get("CODENM")%>";
<%
	}
%>

<%
HashMap hmTypeDrop = new HashMap();
for(int i=0; i<alSetType.size(); i++){
	hmTypeDrop = (HashMap)alSetType.get(i);
%>
var TypeArr<%=i%> = "<%=hmTypeDrop.get("CODEID")%> , <%=hmTypeDrop.get("CODENMALT")%>";
<%
	}
%>
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnLoad();">
<html:form action="/gmOrderDetails?method=saveUsageDeatils">
<input type="hidden" name="hRowCnt" value="<%=intTRCount%>"/>
<input type="hidden" name="hTotal" value="">
<html:hidden property="caseID" name="frmOrderDetails" value="<%=caseID%>"/>
<html:hidden property="accountID" name="frmOrderDetails" value=" "/>
<html:hidden property="repID" name="frmOrderDetails" value=" "/>
<html:hidden property="orderID" name="frmOrderDetails" value="<%=orderID%>"/>
<html:hidden property="caseInfoID" name="frmOrderDetails" value="<%=caseInfoID%>"/>
<html:hidden property="strOpt" name="frmOrderDetails" />
<html:hidden property="inputStr" name="frmOrderDetails" />
<html:hidden property="total" name="frmOrderDetails" />
<html:hidden property="haction" name="frmOrderDetails" />
<%-- <html:hidden property="surgeryDt" name="frmOrderDetails" value="<%=strSurgeryDt%>"/>  --%>
<html:hidden property="parentOrderID" name="frmOrderDetails" value="<%=parentOrderID%>"/>
<html:hidden property="ordComments" name="frmOrderDetails" value=" "/>
<span id="hordId" style="valign:top;"></span>
<input type="hidden" id="Txt_AccId" name="Txt_AccId" value="<%=accountID%>">

<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader" ><%=strHeader%></td>
			<td class="RightDashBoardHeader"></td>
			<td  class="RightDashBoardHeader" align="right" colspan="2"></td>
			<td align="right" class=RightDashBoardHeader>
			<fmtAddUsageDetails:message key="IMG_ALT_HELP" var="varHelp"/>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("ADD_USAGE_DETAILS")%>');" />
			</td>
		</tr>
		<tr><td colspan="6"><font color="red"><span id ="ErrMessage"></span></font> </td></tr>
		<tr>
				<td class="LLine" height="1" colspan="6"></td>
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr class="evenshade">
		
			<td height="25" class="RightTableCaption" align="Right">&nbsp;<fmtAddUsageDetails:message key="LBL_SURGERY_DATE"/>:</td>
			<%if(!strOpt.equals("edit")) {
			if(parentOrderID.equals("")){%>
			<td onmouseout="fnChangeDoId();">&nbsp;
			<gmjsp:calendar SFFormName="frmOrderDetails" SFDtTextControlName="surgeryDt"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			</td>
			<%}}%>
			<%if(!orderMode.equals("IPAD") && (strOpt.equals("edit") || !parentOrderID.equals(""))) {%>	
			<td > &nbsp;<%=strSurgeryDt%></td>
			<%}%>
			
			<%if(strOpt.equals("edit") && orderMode.equals("IPAD")) {%>
				<td onmouseout="fnChangeDoId();">&nbsp;
				<gmjsp:calendar SFFormName="frmOrderDetails" SFDtTextControlName="surgeryDt"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
				</td>		
			<%}%>
			
			<td   height="25" class="RightTableCaption" align="Right" >&nbsp;<fmtAddUsageDetails:message key="LBL_DO_ID"/>:</td>
			<td valign="middle">&nbsp;<span id="ordId" style="valign:top;"><%=orderID%></span>
			<%if(!strOpt.equals("edit")) {
			if(parentOrderID.equals("")){%>
			<gmjsp:dropdown controlName="surgeryNo"  SFFormName='frmOrderDetails' SFSeletedValue="surgeryNo" SFValue="alSurgeryNo" codeId="CODEID" codeName="CODENM" disabled="true" onChange="fnChangeDoId()"/>
			</td>
			<%}} %>
			<td height="25" class="RightTableCaption" align="Right" width=12%>&nbsp;<fmtAddUsageDetails:message key="LBL_CUSTOMER_PO"/>:</td>
			<td >&nbsp;<html:text size="15"  value="<%=custPO%>" property="custPO" maxlength="20" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
		</tr>
		<tr>
			<td class="LLine" height="1" colspan="6"></td>
		</tr>
		<tr class="oddshade">
			<td  height="25" class="RightTableCaption" align="Right" >&nbsp;<fmtAddUsageDetails:message key="LBL_REP_ID"/>:</td>
			<%if(strOpt.equals("edit") && orderMode.equals("IPAD")) {%>		
			<td  id="strRepId">&nbsp;<%=repID%></td>
			<%}else{ %>		
			<td > &nbsp;<bean:write name="frmOrderDetails" property="repID" /></td>
			<%} %>		
			<td height="25" class="RightTableCaption" align="Right">&nbsp;<fmtAddUsageDetails:message key="LBL_CASE_ID"/>:</td>
			<td >&nbsp;<bean:write name="frmOrderDetails" property="caseID" /></td>			
			<td height="25" class="RightTableCaption" align="Right" width=12%>&nbsp;<fmtAddUsageDetails:message key="LBL_PARENT_DO_ID"/>:</td>
			<td >&nbsp;<bean:write name="frmOrderDetails" property="parentOrderID" /></td>	
			
		</tr>
		<tr>
				<td class="LLine" height="1" colspan="6"></td>
		</tr>
		<tr class="evenshade">
			<td   height="25" class="RightTableCaption" align="Right" >&nbsp;<fmtAddUsageDetails:message key="LBL_ACCOUNT_ID"/>:</td>
			<td > &nbsp;<bean:write name="frmOrderDetails" property="accountID" /></td>
			<td height="25" class="RightTableCaption" align="Right">&nbsp;<fmtAddUsageDetails:message key="LBL_ACCOUNT_NAME"/>:</td>
			<td colspan="2">&nbsp;<bean:write name="frmOrderDetails" property="accountNm" /></td>			
			<td height="25" class="RightTableCaption" colspan="1">
			</td>	
		</tr>
		<tr>
				<td class="LLine" height="1" colspan="6"></td>
		</tr>
		<%if(!strSessDeptId.equals("S")){ %>
		
		<tr class="oddshade">
			<td height="25" class="RightTableCaption" align="Right">&nbsp;<fmtAddUsageDetails:message key="LBL_REGION"/>: </td>
			<td >&nbsp;<%=strRegn %></td>
			<td height="25" class="RightTableCaption" align="Right">&nbsp;<fmtAddUsageDetails:message key="LBL_AREA_DIRECTOR"/>:</td>
			<td >&nbsp;<%=strAdName %></td>
			<td height="25" class="RightTableCaption" align="Right">&nbsp;<fmtAddUsageDetails:message key="LBL_FIELD_SALES"/>:</td>
			<td >&nbsp;<%=strDistName %></td>
		</tr>
		<tr>
				<td class="LLine" height="1" colspan="6"></td>
		</tr>
		<tr class="evenshade">
			<td class="RightTableCaption" align="Right"  height="25">&nbsp;<fmtAddUsageDetails:message key="LBL_TERRITORY"/>:</td>
			<td >&nbsp;<%=strTerrName %></td>
			<td class="RightTableCaption" align="Right" height="25">&nbsp;<fmtAddUsageDetails:message key="LBL_REP"/>:</td>
			<%if(strOpt.equals("edit") && orderMode.equals("IPAD")){ %>
				<td class="RightText">
				<jsp:include page="/common/GmAutoCompleteInclude.jsp">  
				<jsp:param name="CONTROL_NAME" value="Cbo_RepId" />
		        <jsp:param name="METHOD_LOAD" value="loadSalesRepList" />
				<jsp:param name="WIDTH" value="200" />
				<jsp:param name="CSS_CLASS" value="search" />
				<jsp:param name="CONTROL_NM_VALUE" value="<%=cbo_repName%>" />
				<jsp:param name="CONTROL_ID_VALUE" value="<%=repID%>" />
				<jsp:param name="SHOW_DATA" value="40" />
				<jsp:param name="AUTO_RELOAD" value="fnValidateRep(this)" />
				<jsp:param name="ON_BLUR" value="" />
		        <jsp:param name="SALESREPID" value="id" /> 
		        </jsp:include>	 
	  			</td>
				
			<%} else{%>
			<td >&nbsp;<%=strRepName%> &nbsp;(<%=strRepId %>)</td>
			<%} %>
			<td class="RightTableCaption" align="Right">&nbsp;<fmtAddUsageDetails:message key="LBL_GROUP_PRICE_BOOK"/>:</td>
			<td >&nbsp;<%=strGpoNm%></td>
		</tr>
		<tr>
				<td class="LLine" height="1" colspan="6"></td>
		</tr>
		<tr class="oddshade">
			<td height="25" class="RightTableCaption" align="Right" width=12%>&nbsp;<fmtAddUsageDetails:message key="LBL_DATE_ENTERED"/>:</td>
			<td >&nbsp;<%=strSessTodaysDate%></td>
			<td height="25" class="RightTableCaption" align="Right" >&nbsp;<fmtAddUsageDetails:message key="LBL_ORDER_TYPE"/>:</td>
			<td align="left"> &nbsp;<%=orderTypeNm%></td>
			<td class="RightTableCaption" align="Right">&nbsp;<fmtAddUsageDetails:message key="LBL_CAPITAL_EQUP_USED"/>:</td>
			<td>
			<fmtAddUsageDetails:message key="LBL_CHOOSEONE" var = "varChooseOne"/>
		    <gmjsp:dropdown controlName="cboegpsusage" seletedValue="<%=strEGPSUsage%>" value="<%=alEGPSUsage%>" codeId="CODEID" codeName="CODENM" defaultValue= "[Choose One]"	/> 
		    </td>
		</tr>
		 <tr>
				<td class="LLine" height="1" colspan="6"></td>
		</tr>
		<%} %>	
		
		<!-- PC-2478: Remove npi details -->
					<%-- <tr  height="20">
						<td class="RightTableCaption" HEIGHT="20" align="right"><fmtAddUsageDetails:message key="LBL_NPI"/>:</td>
						<td class="RightText">
							<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="Txt_Npi" />
							<jsp:param name="METHOD_LOAD" value="loadSurgeonName&strOpt=npilist" />
							<jsp:param name="WIDTH" value="100" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="CONTROL_NM_VALUE" value="" />
							<jsp:param name="CONTROL_ID_VALUE" value="" />
							<jsp:param name="SHOW_DATA" value="40" />
							<jsp:param name="AUTO_RELOAD" value="fnSetSurgeonNm(this);" />
							<jsp:param name="ON_BLUR" value="fnAutoFocus(this);" />
							</jsp:include>	
						</td> 
					
						<td class="RightTableCaption" HEIGHT="20" align="right"><fmtAddUsageDetails:message key="LBL_SURGEON"/>:</td>
						<td>
							<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="Txt_Surgeon" />
							<jsp:param name="METHOD_LOAD" value="loadSurgeonName&strOpt=surgeonlist" />
							<jsp:param name="WIDTH" value="200" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="CONTROL_NM_VALUE" value="" />
							<jsp:param name="CONTROL_ID_VALUE" value="" />
							<jsp:param name="SHOW_DATA" value="40" />
							<jsp:param name="AUTO_RELOAD" value="fnSetNPINum(this);" />
							<jsp:param name="ON_BLUR" value="fnAutoFocus(this);" />
							</jsp:include>	
							<div id="npidtls" style="display: inline;">&nbsp;
							<jsp:include page="/custservice/GmNPIProcess.jsp" >															 
								<jsp:param name="npiTxnId" value='' />
								<jsp:param name="SALESREPID" value='<%=repID%>' />
							</jsp:include>
							</div> 
						</td>
						<td colspan="3"></td>
						<td height="25" class="RightTableCaption" align="center" colspan="3">
						 <fmtAddUsageDetails:message key="BTN_ADD_TAGS" var="varAddTag"/>
			              <gmjsp:button disabled="true" gmClass="Button" name="Btn_AddTags" accesskey="O" tabindex="18" buttonType="Save" onClick="fnAddTags()" value="${varAddTag}"/>
						</td>
					</tr> --%>
		<tr>
				<td class="LLine" height="1" colspan="6"></td>
		</tr>
		<logic:notEqual property="strOpt" value="edit" name="frmOrderDetails">
		<tr>
			<td  height="25" class="RightTableCaption" align="left" colspan="6" >&nbsp;<img border="0"  src="<%=strImagePath%>/30.gif" height="20" width="20">&nbsp;<fmtAddUsageDetails:message key="LBL_ADD_PARTS_USED_IN_SURGERY_CLICK_ON"/> &nbsp;<img border="0"  src="<%=strImagePath%>/btn_remove.gif" height="20" width="18">&nbsp; <fmtAddUsageDetails:message key="LBL_TO_REMOVE_PART"/></td>	
		</tr>
		</logic:notEqual>
		<logic:equal property="strOpt" value="edit" name="frmOrderDetails">
		<tr>
			<td  height="25" class="RightTableCaption" align="left" colspan="6" >&nbsp;<img border="0"  src="<%=strImagePath%>/30.gif" height="20" width="20">&nbsp;<fmtAddUsageDetails:message key="LBL_EDIT_PARTS_USED_IN_SURGERY_CLICK_ON"/> &nbsp;<img border="0"  src="<%=strImagePath%>/plus.gif" height="20" width="18">&nbsp; <fmtAddUsageDetails:message key="LBL_ADD_PARTS_USED_IN_SURGERY_CLICK_ON"/> &nbsp;<img border="0"  src="<%=strImagePath%>/btn_remove.gif" height="20" width="18">&nbsp; <fmtAddUsageDetails:message key="LBL_TO_REMOVE_PART"/></td>	
		</tr>
		</logic:equal>
		<tr>
			<td class="LLine" height="1" colspan="6"></td>
		</tr>
		<TR>
			<td colspan="6">
				<div style="overflow:auto; height:auto;" >
					<table cellpadding="0" cellspacing="0" width="100%" border="1" bordercolor="gainsboro"  id="addUsage">
			  		<thead>
			  			<TR class="Shade" class="RightTableCaption" >
			  				<TH class="RightText" width="20" align="center"></TH>
			  				<TH class="RightText" width="10" align="center"><a href="javascript:fnClearCart();" tabindex=-1><img border="0" Alt='Clear Cart' valign="left" src="<%=strImagePath%>/btn_remove.gif" height="20" width="18"></a></TH>
			  				<TH class="RightText" width="100" align="center"><fmtAddUsageDetails:message key="LBL_PART"/></TH>
			  				<TH class="RightText" width="100" align="center"><fmtAddUsageDetails:message key="LBL_QTY"/></TH>
			  				<TH class="RightText" width="300" align="center"><fmtAddUsageDetails:message key="LBL_PART_DESCRIPTION"/></TH>
			  				<TH class="RightText" width="80" align="center"><fmtAddUsageDetails:message key="LBL_TYPE"/></TH>
			  				<TH class="RightText" width="80" align="center"><fmtAddUsageDetails:message key="LBL_PRICE_EA"/><BR></TH>
			  				<TH class="RightText" width="80" align="center"><fmtAddUsageDetails:message key="LBL_EXT_PRICE"/><BR></TH>
			  				<TH class="RightText" width="80" align="center"><fmtAddUsageDetails:message key="LBL_USAGE"/></TH>
			 	   		</TR>
			  		</thead>
			  		 <TBODY>
			  	<% 
			  	String strPartNum = "";
			  	String strQty="0";
				String strPartDesc = "";
				String strPrice = "";
				String strExtPrice = "";
				String strType = "";
				double dbprice=0.0;
				double dbTotal = 0.0;
				double dbOrdTotal = 0.0;
				HashMap hmLoop = new HashMap();
				int intAlSize =0;
				if(alResult != null){
				intAlSize = alResult.size();
				}
				int intLoop =0;
				if(intAlSize > intTRCount){
					intLoop = intAlSize;
					intTRCount =intAlSize;
				}else{
					intLoop=intTRCount;
				}
				for (int i=0; i < intLoop;i++)
				{
					if (i<intAlSize)
					{
						hmLoop = (HashMap)alResult.get(i);
						strPartNum  = (String)hmLoop.get("PNUM");
						strPartDesc = (String)hmLoop.get("PDESC");
						strQty = (String)hmLoop.get("QTY");
						strPrice  = (String)hmLoop.get("PRICE");
						strExtPrice  = (String)hmLoop.get("EXT_PRICE");
						usage = (String)hmLoop.get("ATTR");
						strUsageFree = (String)hmLoop.get("UFREE");
						strType = (String)hmLoop.get("OTYPE");
					}else
						{
							strPartNum = "";
							strQty = "0";
							strPartDesc = "";
							strPrice = "";
							strExtPrice = "";
							usage ="2560"; 
							strType ="50300";  //No part details then default type as consignment
						}
					if(strPrice.equals("")){
						strPrice ="0.0";
					}
					if(strExtPrice.equals("")){
						strExtPrice="0.0";
					}
					
					    dbprice = Double.parseDouble(strPrice);
						dbTotal = Double.parseDouble(strExtPrice);
						dbOrdTotal = dbOrdTotal + dbTotal;
						hmLoop = null;
				%>
			  		
			  			<tr height="30">
						<td class="RightText" align="center"><%=i+1%></td>
						<td class="RightText" align="center"><a href="javascript:fnRemoveItem('<%=i%>');" tabindex="-1"><img align="middle" border="0" Alt='Remove from cart' valign="left" src="<%=strImagePath%>/btn_remove.gif" height="20" width="18"></a></td>
						
						<td id="Lbl_Part<%=i%>" class="RightText" align="center"><input type="text" align="middle" size="15"  value="<%=strPartNum%>" class=InputArea name="Txt_Part<%=i%>"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="fnSetPartSearch(<%=i%>,this);" onchange="fnUpperCase(this);" ></td>

						<td id="Lbl_Qty<%=i%>" class="RightText" align="center" ><a href="javascript:fnDecresQty(<%=i%>,this);" tabindex="-1"><IMG id="qtyDec<%=i%>" align="top" border=0 src="<%=strImagePath%>/minus.gif" height="20" width="18" ></a>&nbsp;
						<input type="text" name="Txt_Qty<%=i%>" value="<%=strQty%>" maxlength=3 size=2 align="middle" onkeypress="return fnIsNumeric(event);" onFocus="changeBgColor(this,'#AACCE8');" onBlur="fnPartSet(<%=i%>,this);" >&nbsp;<a href="javascript:fnIncrQty(<%=i%>,this); " tabindex="-1"/>
						<IMG id="qtyInc<%=i%>" align="top" border=0 src="<%=strImagePath%>/plus.gif" height="20" width="18" ></a>
						</td>
						<td id="Lbl_Desc<%=i%>" class="RightText" align="left" tabindex="-1">&nbsp;<%=strPartDesc%></td>
						<td>
						<select  name="type<%=i%>" id="type<%=i%>"  class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" onchange="fnUsage(this,'<%=i%>');">
						<%
			  				int intsize = alSetType.size();
			  				HashMap hcboValType = new HashMap();
			  				String strCodeIDType="";
			  				String strSelectedType="";
			  				for (int j=0;j<intsize;j++)
			  				{
			  					hcboValType = (HashMap)alSetType.get(j);
			  					strCodeIDType = (String)hcboValType.get("CODEID");
								strSelectedType = strType.equals(strCodeIDType)?"selected":"";
						%>
								<option <%=strSelectedType%> value="<%=strCodeIDType%>"><%=hcboValType.get("CODENMALT")%></option>
						<%
			  				}
			 			%>
						</select>
					</td>
						
						<%if(strSessDeptId.equals("S")){ if(!usage.equals("2563")){%>
						<td id="Lbl_PriceEA<%=i%>" class="RightText" align="Right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strPrice,2))%>
						</td>
						<%}else{ %>
						<td  id="Lbl_PriceEA<%=i%>" class="RightText" align="right"><input style="text-align: right;" type="text" name="Txt_PriceEA<%=i%>"  onkeypress="return fnPriceOnly(event);" size="8"  value="<%=GmCommonClass.getStringWithCommas(strPrice,2)%>" class=InputArea  onFocus="changeBgColor(this,'#AACCE8');" onBlur="fnCalExtPrice(this,'<%=i%>');"></td>
						<%} %>
						<%}else{ %>
						<td  class="RightText" align="center"><input style="text-align: right;" type="text" name="Txt_PriceEA<%=i%>"  onkeypress="return fnPriceOnly(event);" size="8"  value="<%=GmCommonClass.getStringWithCommas(strPrice,2)%>" class=InputArea  onFocus="changeBgColor(this,'#AACCE8');" onBlur="fnCalExtPrice(this,'<%=i%>');"></td>
						<%} %>
						<input type="hidden" name="hPriceEA<%=i%>" value="<%=GmCommonClass.getStringWithCommas(strPrice,2)%>"/>
						
						<td id="Lbl_ExtPrice<%=i%>" class="RightText" align="Right" tabindex="-1"><gmjsp:currency type="CurrTextSign"  textValue="<%=strExtPrice%>" td="false"/></td>
						
						<td align="center">
						<select  name="usage<%=i%>" id="usage<%=i%>"  class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" onchange="fnUsage(this,'<%=i%>');">
<%
			  		int intSize = alUsage.size();
			  		HashMap hcboVal = new HashMap();
			  		String strCodeID="";
			  		String strSelected="";
			  		for (int j=0;j<intSize;j++)
			  		{
			  			hcboVal = (HashMap)alUsage.get(j);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = usage.equals(strCodeID)?"selected":"";
%>
								<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
			  		
%>
						</select>
					</td>
						</tr>
				<%} 
				String strOrdTotal = ""+dbOrdTotal;
				%>
			  		 </TBODY>
					</table>
			  	</div>
			 </td>
		</TR>
		<tr>
				<td class="LLine" height="1" colspan="6"></td>
		</tr>
		<tr class="oddshade">
			<td   class="RightTableCaption" colspan="4">&nbsp;</td>	
			<td height="25" class="RightTableCaption" align="Right"> <fmtAddUsageDetails:message key="LBL_TOTAL_PRICE"/>:&nbsp;</td>
			
			<td class="RightCaption" align="left" id="Lbl_Total">&nbsp;<B><gmjsp:currency type="CurrTextSign"  textValue="<%=strOrdTotal%>" td="false" /></B> 
			<input type="hidden" name="totalPrice" value ="<%=dbOrdTotal%>" /></td>
		</tr>  
		<tr>
				<td class="LLine" height="1" colspan="6"></td>
		</tr>
		 <tr>
		<td colspan="6">
			<jsp:include page="/common/GmIncludeLog.jsp" >
			<jsp:param name="FORMNAME" value="frmOrderDetails" />
			<jsp:param name="ALNAME" value="alLogReasons" />
			<jsp:param name="LogMode" value="Edit" />
			</jsp:include>
		</td>
     </tr>
    	 <tr>
				<td class="LLine" height="1" colspan="6"></td>
		</tr>
		<tr>
			<td class="RightTableCaption" align="left" colspan="3">&nbsp;<a href="javaScript:fnAddRow('addUsage')"><U><fmtAddUsageDetails:message key="LBL_CLICK_HERE_TO_ADD_MORE_ROWS"/></U> </a><BR> &nbsp;<a href="javaScript:fnSearchPart()"><U><fmtAddUsageDetails:message key="LBL_SEARCH_FOR_PARTS"/></U></td>
			<td  height="30" colspan="2" align="left">
			<fmtAddUsageDetails:message key="LBL_SAVE" var="varSave"/>
			<gmjsp:button value="${varSave}"  buttonType="save" style="width: 5em" onClick="fnSaveUsage();"/>&nbsp;&nbsp;&nbsp;
			<logic:equal property="strOpt" value="edit" name="frmOrderDetails">
			<fmtAddUsageDetails:message key="LBL_CANCEL" var="varCancel"/>
			<gmjsp:button value="${varCancel}" buttonType="load" style="width: 7em"  onClick="fnCancel();"/>&nbsp;
			</logic:equal>
			</td>
			<td class="RightTableCaption"  colspan="1">
		</tr>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>