<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib uri="/WEB-INF/struts-logic-el.tld" prefix="logicel" %>

<!-- custservice\GmPartNumOrderDetail.jsp -->
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<%@ page import="java.util.Date"%>
<%@ taglib prefix="fmtPartOrdDtls" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartOrdDtls:setLocale value="<%=strLocale%>"/>
<fmtPartOrdDtls:setBundle basename="properties.labels.custservice.GmPartNumOrderDetail"/>

<bean:define id="strOpt" name="frmPartNumOrderDetail" property="strOpt" type="java.lang.String"> </bean:define>
<bean:define id="strFromDt" name="frmPartNumOrderDetail" property="fromDate" type="java.lang.String"> </bean:define>
<bean:define id="strToDt" name="frmPartNumOrderDetail" property="toDate" type="java.lang.String"> </bean:define>
 
<%
request.setCharacterEncoding("UTF-8");
response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8");
GmServlet gm = new GmServlet();
gm.checkSession(response,session);

String strWikiTitle = GmCommonClass.getWikiTitle("PART_NUMBER_DETAILS");
String strApplDateFmt = strGCompDateFmt;

Date dtFrmDate = null;
Date dtToDate = null;

dtFrmDate = GmCommonClass.getStringToDate(strFromDt, strApplDateFmt);
dtToDate = GmCommonClass.getStringToDate(strToDt, strApplDateFmt);
String strDODtFmt = "{0,date," + strApplDateFmt + "}"; //Date format for DO Date

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Order Part Number Details</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css"> 
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>


<script>

var format = '<%=strApplDateFmt%>';
var lblOrdFromDt = '<fmtPartOrdDtls:message key="LBL_ORD_DATE_FROM"/>';
var lblordToDt = '<fmtPartOrdDtls:message key="LBL_ORD_DATE_TO"/>';
var gridObj;
function fnGo()
{
	fnValidateTxtFld("fromDate" , lblOrdFromDt);
	fnValidateTxtFld("toDate" , lblordToDt);
	
	// Checking for valid date format
	CommonDateValidation(document.frmPartNumOrderDetail.fromDate,format,Error_Details_Trans(message[10523],format));
	CommonDateValidation(document.frmPartNumOrderDetail.toDate,format,Error_Details_Trans(message[10524],format));
	
	// Checking for Valid date range. To date always greater than or equals to From date.
	var fromToDiff = dateDiff(document.frmPartNumOrderDetail.fromDate.value, document.frmPartNumOrderDetail.toDate.value,format);	
	if(fromToDiff < 0){
		Error_Details(message[10525]);
	}
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}	
	
	var fromDate = document.frmPartNumOrderDetail.fromDate.value;
	var toDate = document.frmPartNumOrderDetail.toDate.value;
	var distrubutorName = document.frmPartNumOrderDetail.dist.value;
	var saleRepName = document.frmPartNumOrderDetail.rep.value;
	var ordType = document.frmPartNumOrderDetail.ordType.value;
	var partNum = document.frmPartNumOrderDetail.partNum.value;
	
	  var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPartNumOrderDetail.do?strOpt=RELOAD&fromDate='+fromDate+
			  '&toDate='+toDate+'&dist='+distrubutorName+'&rep='+saleRepName+'&ordType='+ordType+'&partNum='+partNum+'&ramdomId=' + new Date().getTime());
		dhtmlxAjax.get(ajaxUrl, fnLoadPartDtlsRpt)
		fnStartProgress();
	/* document.frmPartNumOrderDetail.strOpt.value = "RELOAD";
	fnStartProgress();
	document.frmPartNumOrderDetail.submit(); */
	
}

function enterPressed(evn) {
	if (window.event && window.event.keyCode == 13) {
  		fnGo();
	} else if (evn && evn.keyCode == 13) {
  		fnGo();
	}
}
document.onkeypress = enterPressed;
//This function used to generate DHTMLX grid
function fnLoadPartDtlsRpt(loader){
	var response = loader.xmlDoc.responseText;
	if(response !='' && response != null && response != undefined ){
		var setInitWidths = "75,150,150,94,50,70,83,170,85";
		var setColAlign = "left,left,left,left,center,center,left,left,right";
		var setColTypes = "ro,ro,ro,ro,ro,ro,ro,ro,ro";
		var setColSorting = "str,str,str,str,str,str,str,str,str";
		var setHeader = ["Acct #", "Acct Name","Rep","DO #","Type","DO Date","Part #", "Part Desc","Qty"];
		var setFilter = ["#text_filter", "#text_filter", "#text_filter","#text_filter", "#select_filter","#text_filter","#text_filter","#text_filter","#text_filter"];
		var setColIds = "acctnum,acctname,repname,DO,ordertyp,orddate,pnum,pdesc,qty";
		var enableTooltips = "true,true,true,true,true,true,true,true";
		fnStopProgress();
		
		var gridHeight = "";
		var footerStyles = [];
		var footerExportFL = true;
		var pagination = "Y";
			
		
	    document.getElementById("dataGridDiv").style.display = "block";
	    document.getElementById("DivNothingMessage").style.display = 'none';
		document.getElementById("DivExportExcel").style.display = "table-row";
	 // split the functions to avoid multiple parameters passing in single
		// function
		gridObj = initGridObject('dataGridDiv');
		gridObj = fnCustomTypes(gridObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips, setFilter,400,footerStyles,pagination);
		gridObj = loadDhtmlxGridByJsonData(gridObj, response);
		document.getElementById("pagingArea").style.display = 'block';
}else{
	fnStopProgress();
		document.getElementById("dataGridDiv").style.display = 'none';
		document.getElementById("DivNothingMessage").style.display = 'block';
		document.getElementById("DivExportExcel").style.display = 'none';
		document.getElementById("pagingArea").style.display = 'none';
}
}
//This Function Used to set custom types conditions in Grid
function fnCustomTypes(gObj,setHeader, setColIds, setInitWidths, setColAlign, setColTypes,setColSorting, enableTooltips,setFilter,gridHeight,footerStyles,pagination){
	
		if (setHeader != undefined && setHeader.length > 0) {
		 gObj.setHeader(setHeader);
		 var colCount = setHeader.length;
		 }    

		if (setInitWidths != "")
		gObj.setInitWidths(setInitWidths);

		if (setColAlign != "")
		gObj.setColAlign(setColAlign);

		if (setColTypes != "")
		gObj.setColTypes(setColTypes);

		if (setColSorting != "")
		gObj.setColSorting(setColSorting);

		if (enableTooltips != "")
		gObj.enableTooltips(enableTooltips);

		if (setFilter != "")
		gObj.attachHeader(setFilter);


		if(setColIds != ""){
		 gObj.setColumnIds(setColIds);
		}

		if (gridHeight != "")
		gObj.enableAutoHeight(true, gridHeight, true);
		else
		gObj.enableAutoHeight(true, 400, true);
		if(pagination == "Y"){
		    gObj.enablePaging(true,100,10,"pagingArea",true);
		    gObj.setPagingSkin("bricks");
		}	
		
	return gObj;
}
//This Function Used to download Excel Sheet
function fnExport() {
	gridObj.toExcel('/phpapp/excel/generate.php');
}
function fnOnPageLoad(){
	
	var fromDate = document.frmPartNumOrderDetail.fromDate.value;
	var toDate = document.frmPartNumOrderDetail.toDate.value;
	var distrubutorName = document.frmPartNumOrderDetail.dist.value;
	var saleRepName = document.frmPartNumOrderDetail.rep.value;
	var ordType = document.frmPartNumOrderDetail.ordType.value;
	var partNum = document.frmPartNumOrderDetail.partNum.value;
	
	  var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmPartNumOrderDetail.do?strOpt=RELOAD&fromDate='+fromDate+
			  '&toDate='+toDate+'&dist='+distrubutorName+'&rep='+saleRepName+'&ordType='+ordType+'&partNum='+partNum+'&ramdomId=' + new Date().getTime());
		dhtmlxAjax.get(ajaxUrl, fnLoadPartDtlsRpt)
}
</script>	

</HEAD>

<BODY leftmargin="20" topmargin="10"  onload="fnOnPageLoad();">
 
<html:form  action="/gmPartNumOrderDetail.do" >
 <html:hidden property="strOpt" value="" />  

		<table border="0" class="DtTable950"  cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="5" height="25" class="RightDashBoardHeader">
				<fmtPartOrdDtls:message key="TD_PART_ORD_DETAILS_HEADER"/>
			</td>
			<fmtPartOrdDtls:message key="IMG_ALT_HELP" var = "varHelp"/>
			<td  height="25" class="RightDashBoardHeader" align="right" >
			    <img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    </td>
		</tr>
		<tr><td colspan="6" class="LLine" height="1"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
		<td height="30"  class="RightTableCaption" align="right">&nbsp;<fmtPartOrdDtls:message key="LBL_DIST_NAME"/> :&nbsp;</td>
		<td align ="left"><gmjsp:dropdown controlName="dist" SFFormName="frmPartNumOrderDetail" SFSeletedValue="dist" 
							SFValue="alDistList" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" width="230"/></td>
		<td height="30"  class="RightTableCaption" align="right">&nbsp;<fmtPartOrdDtls:message key="LBL_SALES_REP"/> :&nbsp;</td>
		<td align ="left"><gmjsp:dropdown controlName="rep" SFFormName="frmPartNumOrderDetail" SFSeletedValue="rep" 
							SFValue="alRepList" codeId = "REPID"  codeName = "NAME"  defaultValue= "[Choose One]" width="230"/></td>
		</tr>
		<tr><td colspan="6" class="LLine" height="1"></td></tr>
		<tr >
				<td height="25" class="RightTableCaption" align="right"><fmtPartOrdDtls:message key="LBL_ORD_DATE_FROM"/>:&nbsp;</td>
				<td align ="left"><gmjsp:calendar textControlName="fromDate" textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtFrmDate.getTime())%>" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
				</td>
				<td height="25" class="RightTableCaption" align="right">&nbsp;&nbsp;<fmtPartOrdDtls:message key="LBL_ORD_DATE_TO"/>:&nbsp;</td>
				<td><gmjsp:calendar textControlName="toDate" textValue="<%=dtToDate==null?null:new java.sql.Date(dtToDate.getTime())%>" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
				</td>
		 </tr>
		 <tr><td colspan="6" class="LLine" height="1"></td></tr>
		 <tr>
		 		<td height="30"  class="RightTableCaption" align="right">&nbsp; <fmtPartOrdDtls:message key="LBL_ORD_TYPE"/>:&nbsp;</td>
				<td align ="left"><gmjsp:dropdown controlName="ordType" SFFormName="frmPartNumOrderDetail" SFSeletedValue="ordType" 
							SFValue="alOrdTypeList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" /></td>
				
				<fmtPartOrdDtls:message key="BTN_LOAD" var="varLoad"/>
				<td height="30"  class="RightTableCaption" align="right">&nbsp;<fmtPartOrdDtls:message key="LBL_PART_NUM"/>:&nbsp;</td>
				<td align ="left"><html:text property="partNum"  size="15" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
				&nbsp;<gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnGo();"buttonType="Load" /></td>	 	
		 </tr>
		</table>

		<table border="0" class="DtTable950" cellspacing="0" cellpadding="0">
	   <%--  <tr>
			<td colspan="6" width="100%">
				<display:table name="requestScope.frmPartNumOrderDetail.rdReport.rows" export="true" class="its" requestURI="/gmPartNumOrderDetail.do" id="currentRowObject" freezeHeader="true" >
 				<fmtPartOrdDtls:message key="DT_ACCT_ID" var="varAcctId"/>
 				<display:column property="ACCTNUM" title="${varAcctId}" class="alignleft" sortable="true"/>
 				<fmtPartOrdDtls:message key="DT_ACCT_NM" var="varAcctNm"/>
 				<display:column property="ACCTNAME" title="${varAcctNm}" class="alignleft" sortable="true" />
 				<fmtPartOrdDtls:message key="DT_REP_NM" var="varRepNm"/>
 				<display:column property="REPNAME" title="${varRepNm}" class="alignleft" sortable="true" />
 				<fmtPartOrdDtls:message key="DT_DO_ID" var="varDOId"/>
				<display:column property="DO" title="${varDOId}" class="alignleft" sortable="true" />
				<fmtPartOrdDtls:message key="DT_ORD_TYPE" var="varOrdType"/>
				<display:column property="ORDERTYP" title="${varOrdType}" class="alignleft" sortable="true"/>
				<fmtPartOrdDtls:message key="DT_ORD_DATE" var="varOrdDate"/>
				<display:column property="ORDDATE" title="${varOrdDate}" class="alignleft" sortable="true" style="width:70" format="<%=strDODtFmt%>"/><!-- Converting date in query itself -->	
				<fmtPartOrdDtls:message key="DT_PART_ID" var="varPartNum"/>
				<display:column property="PNUM" title="${varPartNum}" class="alignleft" sortable="true"/>
				<fmtPartOrdDtls:message key="DT_PART_DESC" var="varPartDesc"/>
				<display:column property="PDESC" title="${varPartDesc}" class="alignleft" sortable="true" />
				<fmtPartOrdDtls:message key="DT_QTY" var="varQty"/>
				<display:column property="QTY" title="${varQty}" class="alignleft" sortable="true" style="width:30" />
				</display:table>
			</td>
		</tr> --%>
		<tr>
		<td colspan="6"><div id="dataGridDiv"  style="width: 100%"></div>	
		 <div id="pagingArea" height="500px" width="950"></div>	
		</td>
		</tr>
			<tr style="display: none" id="DivExportExcel" height="25">
			<td colspan="4" align="center">
			    <div class='exportlinks'><fmtPartOrdDtls:message key="DIV_EXPORT_OPT"/> : <img
					src='img/ico_file_excel.png' />&nbsp;<a href="#"
					onclick="fnExport();"><fmtPartOrdDtls:message key="DIV_EXCEL"/> 
				</a></div>
			</td>
		</tr>
		<tr><td colspan="6" align="center" class="RegularText"><div  id="DivNothingMessage" style="display: none;"><fmtPartOrdDtls:message key="NO_DATA_FOUND"/></div></td></tr>	
		
			<tr>
				<fmtPartOrdDtls:message key="BTN_CLOSE" var="varClose"/>
				<td colspan="6" align="center" height="30">
				<logic:notEqual name="frmPartNumOrderDetail" property="strOpt" value="CloseBtn">
					<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" />
				</logic:notEqual>
				</td>
			</tr> 
		</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>