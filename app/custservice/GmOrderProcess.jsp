
<%
/**********************************************************************************
 * File		 		: GmOrderProcess.jsp
 * Desc		 		: This screen is used for the Territory Maintenance
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtOrderProcess" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%


String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));

if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}

%>
<fmtOrderProcess:setLocale value="<%=strLocale%>"/>
<fmtOrderProcess:setBundle basename="properties.labels.custservice.GmOrderProcess"/>

<!-- \custservice\GmOrderProcess.jsp -->
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);



	String strhAction = (String)request.getAttribute("hAction");
	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}
	strhAction = (strhAction == null)?"Load":strhAction;

	HashMap hmReturn = new HashMap();

	if (strhAction.equals("LoadOrd")||strhAction.equals("PopOrd"))
	{
			hmReturn = (HashMap)session.getAttribute("hmReturn");
	}
	else
	{
		hmReturn = (HashMap)request.getAttribute("hmReturn");
	}
	
	HashMap hmAcctDetails = new HashMap();

	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";

	ArrayList alShipList = new ArrayList();
	ArrayList alRepList = new ArrayList();

	String strOMode = "";
	String strPName = "";
	String strAccId = "";
	String strAccNm = "";
	String strDistRepNm = "";
	String strRepId = "";
	String strBillNm = "";
	String strBillAdd1 = "";
	String strBillAdd2 = "";
	String strBillCity = "";
	String strBillState = "";
	String strBillZip = "";
	String strCurrDate = "";
	String strShipTo = "";
	String strPO = "";
	String strOrdId = "";
	String strShipAdd = "";

	HashMap hmTerritory = new HashMap();

	if (hmReturn != null)
	{
		hmAcctDetails = (HashMap)hmReturn.get("ACCOUNTDETAILS");
		alShipList = (ArrayList)hmReturn.get("SHIP");
		strOMode = (String)hmReturn.get("OMODE");
		strPName = (String)hmReturn.get("PNAME");
		if (strhAction.equals("LoadOrd"))
		{
			strPO = (String)hmReturn.get("PO");
			strOrdId = (String)hmReturn.get("ORDID");
			strShipTo = (String)hmReturn.get("SHIPTO");
			strShipAdd = (String)hmReturn.get("SHIPADD");
		}
	}
	if (hmAcctDetails != null )
	{
		strAccId = (String)hmAcctDetails.get("ID");
		strAccNm = (String)hmAcctDetails.get("ANAME");
		strDistRepNm = (String)hmAcctDetails.get("REPDISTNM");
		strRepId = (String)hmAcctDetails.get("REPID");
		strBillNm = (String)hmAcctDetails.get("BNAME");
		strBillAdd1 = (String)hmAcctDetails.get("BADD1");
		strBillAdd2 = (String)hmAcctDetails.get("BADD2");
		strBillCity = (String)hmAcctDetails.get("BCITY");
		strBillState = (String)hmAcctDetails.get("BSTATE");
		strBillZip = (String)hmAcctDetails.get("BZIP");
		strCurrDate = (String)hmAcctDetails.get("CDATE");
	}

	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Order Process </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script>
function fnSubmit()
{
	document.frmOrder.hAction.value = "Initiate";
	document.frmOrder.submit();
}

function fnGo()
{
	Error_Clear();
	obj = document.frmOrder;
	obj1 = document.frmOrder.Txt_AccId;
	obj2 = document.frmOrder.Cbo_Acct;
	if (obj1.value == '' && obj2.value == '0')
	{
		Error_Details(message[2]);
	}

	if (obj.Cbo_OMode.value == '0')
	{
		Error_Details(message[3]);
	}
	else
	{
		if (obj.Cbo_OMode.selectedIndex != '1' && obj.Txt_PNm.value=='')
		{
			Error_Details(message[4]);
		}
	}

	if (ErrorCount > 0)
	{
			Error_Show();
			return false;
	}


	if (obj1.value == '' && obj2.value != '0')
	{
		document.frmOrder.Txt_AccId.value = obj2.value;
		document.frmOrder.hAction.value = "Go";
		document.frmOrder.submit();
	}
	else
	{
		document.frmOrder.hAction.value = "Go";
		document.frmOrder.submit();
	}
}

function fnAddToCart()
{
	document.frmOrder.hAction.value = "GoCart";
	document.frmOrder.submit();
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmOrder" method="post" action = "<%=strServletPath%>/GmOrderProcessServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hAccId" value="<%=strAccId%>">
<input type="hidden" name="hRepId" value="<%=strRepId%>">
<input type="hidden" name="hOrdId" value="<%=strOrdId%>">
<input type="hidden" name="hOMode" value="<%=strOMode%>">
<input type="hidden" name="hPName" value="<%=strPName%>">
	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" colspan="3"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr class="shade">
						<td width="170"><img src="<%=strImagePath%>/s_logo.gif" width="160" height="60"></td>
						<td class="RightText" width="130"><fmtOrderProcess:message key="LBL_GLOBUS_MEDICAL"/><td>
						<td align="right" class="RightBigHeader"><fmtOrderProcess:message key="LBL_ORDER_FORM"/></td>
					</tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
		</tr>
			<td bgcolor="#666666" colspan="3"></td>
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
<%
			if (strhAction.equals("Load"))
			{
				ArrayList alMode = new ArrayList();
				alMode = (ArrayList)hmReturn.get("ORDERMODE");
				ArrayList alAccounts = new ArrayList();
				alAccounts = (ArrayList)hmReturn.get("ACCOUNTLIST");
%>
					<tr>
						<td colspan="2"  class="RightTextBlue" align="center" HEIGHT="24"><fmtOrderProcess:message key="LBL_ACC_ID"/>:
						&nbsp;<input type="text" size="10" value="" name="Txt_AccId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1> <fmtOrderProcess:message key="LBL_CHOOSE_ACC"/>&nbsp;<select name="Cbo_Acct" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtOrderProcess:message key="LBL_CHOOSE"/>
<%
					intSize = alAccounts.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alAccounts.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						//strSelected = strShipTo.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%
			  		}
%>
							</select>
						</td>
					</tr>

					<tr class="shade">
						<td width="320" class="RightTextBlue" align="right" HEIGHT="24"><fmtOrderProcess:message key="LBL_MODE_OF_ORDER"/>:</td>
						<td class="RightText" valign="top">&nbsp;<select name="Cbo_OMode" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
<%
					intSize = alMode.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alMode.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						//strSelected = strShipTo.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
							</select>
						</td>
					</tr>
					<tr>
						<td width="320" class="RightTextBlue" align="right" HEIGHT="24"><fmtOrderProcess:message key="LBL_PERSON_NAME"/>:</td>
						<td >&nbsp;<input type="text" size="30" value="" name="Txt_PNm" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center">	<fmtOrderProcess:message key="BTN_GO" var="varGo"/><gmjsp:button value="&nbsp;${varGo}&nbsp;" name="Btn_Go" gmClass="button" onClick="fnGo();" tabindex="4" buttonType="Load" />&nbsp;
						</td>
					</tr>
<%
			}else if (strhAction.equals("LoadAcc") || strhAction.equals("LoadOrd"))
			{

%>
				
					<tr>
						<td colspan="2">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr class="ShadeRightTableCaption">
									<td height="25" align="center" width="250">&nbsp;<fmtOrderProcess:message key="LBL_BILL_TO"/></td>
									<td bgcolor="#666666" width="1" rowspan="7"></td>
									<td width="250" align="center">&nbsp;<fmtOrderProcess:message key="LBL_SHIP_TO"/>

										&nbsp;<select name="Cbo_ShipTo" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtOrderProcess:message key="LBL_CHOOSE"/>
<%
			  		intSize = alShipList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alShipList.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strShipTo.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>

						</select>

									</td>
									<td bgcolor="#666666" width="1" rowspan="7"></td>
									<td width="100" align="center">&nbsp;<fmtOrderProcess:message key="LBL_ORDER_DATE"/></td>
									<td width="100" align="center">&nbsp;<fmtOrderProcess:message key="LBL_ORDER#"/></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="6"></td>
								</tr>
								<tr>
									<td class="RightText" rowspan="5" valign="top">&nbsp;<%=strAccNm%><br>
									&nbsp;<u>Attn:</u><%=strBillNm%><br>&nbsp;<%=strBillAdd1%><br>&nbsp;<%=strBillCity%><br>
									&nbsp;<%=strBillState%>,<%=strBillZip%>
									</td>
									<td rowspan="5" class="RightText" valign="top">&nbsp;<%=strShipAdd%></td>
									<td height="25" class="RightText" align="center">&nbsp;<%=strCurrDate%></td>
									<td class="RightText" align="center">&nbsp;<%=strOrdId%></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
								</tr>
								<tr class="ShadeRightTableCaption">
									<td align="center"><fmtOrderProcess:message key="LBL_DIST_REP"/></td>
									<td align="center"><fmtOrderProcess:message key="LBL_CUST_PO"/></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
								</tr>
								<tr>
									<td align="center" class="RightText">&nbsp;<%=strDistRepNm%><Br></td>
									<td align="center" height="25" class="RightText">&nbsp;
										<input type="text" size="10" value="<%=strPO%>" name="Txt_PO" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
									</td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="6"></td>
								</tr>
							</table>
						</td>
					</tr>
<%
			if (strhAction.equals("LoadOrd"))
			{
%>
					<tr>
						<td height="30" align="center" colspan="2" class="RightTextBlue"><fmtOrderProcess:message key="LBL_SHOPPING_MODULE"/><br><br>&nbsp;<input type="text" size="40" value="" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
						<fmtOrderProcess:message key="BTN_GO" var="varGo"/><gmjsp:button value="&nbsp;${varGo}&nbsp;" name="Btn_GoCart" gmClass="button" onClick="fnAddToCart();" tabindex="16" buttonType="Load" /><br><br>
						</td>
					</tr>

<%
			}
%>
					<tr>
						<td height="30" align="center">&nbsp;
<%
			if (strhAction.equals("LoadAcc"))
			{
%>
						<fmtOrderProcess:message key="BTN_INITITATE" var="varInitiate"/><gmjsp:button value="${varInitiate}" name="Btn_Initiate" gmClass="button" onClick="fnSubmit();" tabindex="16" buttonType="Save" />&nbsp;
<%			}
		}
%>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
