 <%
/**********************************************************************************
 * File		 		: GmPartPriceReport.jsp
 * Desc		 		: This screen is used for the Part Number Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- custservice\GmPartPriceReport.jsp -->
<%@ taglib prefix="fmtPartPriceRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartPriceRpt:setLocale value="<%=strLocale%>"/>
<fmtPartPriceRpt:setBundle basename="properties.labels.custservice.GmPartPriceReport"/>

<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	ArrayList alReturn = (ArrayList)request.getAttribute("alReturn");
	int intLength = alReturn.size();

	int intSize = 0;
	HashMap hmLoop = new HashMap();

	String strPartNum = "";
	String strDesc = "";
	String strEPrice = "";
	String strLPrice = "";
	String strCPrice = "";
	String strListPrice = "";

	String strShade = "";
	String strProjId = "";
	String strParam = (String)request.getAttribute("hLoad");

	strProjId = request.getParameter("hId")== null ?"":request.getParameter("hId");

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Price Project Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>
function fnSubmit(val)
{
	document.frmProject.hColumn.value = val;
	document.frmProject.submit();
}

function fnCallProject(val)
{

	document.frmProject.hId.value = val;
	document.frmProject.submit();
}

function fnCallEdit(val)
{
	document.frmProject.PartNum.value = val;
	document.frmProject.hMode.value = "Edit";
	document.frmProject.action = "<%=strServletPath%>/GmPartPriceEditServlet";
	document.frmProject.submit();
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmProject" method="POST" action="<%=strServletPath%>/GmPartPriceReportServlet">
<input type="hidden" name="hColumn" value="ID">
<input type="hidden" name="hId" value="<%=strProjId%>">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hMode" value="">
<input type="hidden" name="PartNum" value="">

<%
	if (strParam.equals("Project"))
	{

		String strProjName = "";
%>
	<table border="0" width="550" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="5"></td>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtPartPriceRpt:message key="LBL_PART_NUM_PRICE_RPT"/></td>
			<td bgcolor="#666666" width="1" rowspan="5"></td>
		</tr>
		<tr><td bgcolor="#666666"></td></tr>
		<tr>
			<td class="RightTextBlue">&nbsp;<fmtPartPriceRpt:message key="LBL_PROJ_PART_NUM_PRICE_CHANGE"/><BR><BR></td>
		</tr>
		<tr><td bgcolor="#666666"></td></tr>
		<tr>
			<td width="550" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="200"> <fmtPartPriceRpt:message key="LBL_PROJ_ID"/></td>
						<td width="200"><fmtPartPriceRpt:message key="LBL_PROJ_NM"/></td>
					</tr>
					<tr><td class="Line" colspan="2"></td></tr>
					<tr>
<%
							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alReturn.get(i);
								strProjId	= GmCommonClass.parseNull((String)hmLoop.get("ID"));
								strProjName = GmCommonClass.getStringWithTM(GmCommonClass.parseNull((String)hmLoop.get("NAME")));
								strShade	= (i%2 == 0)?"class=Shade":""; //For alternate Shading of rows
%>
						<td class="RightText">&nbsp;<%=strProjId%></td>
						<td height="20">&nbsp;<a  class="RightText" href="javascript:fnCallProject('<%=strProjId%>');"><%=strProjName%></a></td>

					</tr>
					<tr <%=strShade%>>
<%
							}
%>
		          	</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
<%
	}else{
%>
	<table border="0" width="750" cellspacing="0" cellpadding="0">
		<tr><td class="Line" colspan="6"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="5"></td>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtPartPriceRpt:message key="LBL_PART_NUM_PRICE_RPT"/></td>
			<td bgcolor="#666666" width="1" rowspan="5"></td>
		</tr>
		<tr>
			<td width="750" height="100" valign="top">
<%
	if (intLength > 0)
	{
%>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td class="Line" colspan="6"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="70">&nbsp;<a href="javascript:fnSubmit('ID');"><fmtPartPriceRpt:message key="LBL_PART_BR_NUM"/></a></td>
						<td width="400"><fmtPartPriceRpt:message key="LBL_PART_DESC"/></td>
						<td width="80" align="center"><fmtPartPriceRpt:message key="LBL_TIER1_PRICE"/></td>
						<td width="80" align="center"><fmtPartPriceRpt:message key="LBL_TIER2_PRICE"/></td>
						<td width="80" align="center"><fmtPartPriceRpt:message key="LBL_TIER3_PRICE"/></td>
						<td width="80" align="center"><fmtPartPriceRpt:message key="LBL_TIER4_PRICE"/></td>						
					</tr>
					<tr><td class="Line" colspan="6"></td></tr>
<%
							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alReturn.get(i);
								strPartNum	= GmCommonClass.parseNull((String)hmLoop.get("ID"));
								strDesc = GmCommonClass.getStringWithTM(GmCommonClass.parseNull((String)hmLoop.get("PDESC")));
								strListPrice = GmCommonClass.parseZero((String)hmLoop.get("LIPRICE"));
								strEPrice = GmCommonClass.parseZero((String)hmLoop.get("EPRICE"));
								strLPrice = GmCommonClass.parseZero((String)hmLoop.get("LPRICE"));
								strCPrice	= GmCommonClass.parseZero((String)hmLoop.get("CPRICE"));
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
%>
					<tr <%=strShade%>>
						<td height="20" class="RightText">&nbsp;
						<%if (!strAction.equals("FROMPD")) { %> 
							<a class="RightText" href="javascript:fnCallEdit('<%=strPartNum%>');">
							<%=strPartNum%></a>
						<% } else {%>
							<%=strPartNum%>
						<%}%>
						</td>
						<td class="RightText">&nbsp;<%=strDesc%></td>
						<td class="RightText" align="right">&nbsp;<%=GmCommonClass.getStringWithCommas(strListPrice)%></td>
						<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strLPrice)%>&nbsp;</td>
						<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strCPrice)%>&nbsp;</td>
						<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strEPrice)%>&nbsp;</td>
					</tr>
<%
							}
%>
		          	</tr>
					</table>
<%
	}else{
%>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="100"><fmtPartPriceRpt:message key="LBL_PART_NUM"/></td>
						<td width="300"><fmtPartPriceRpt:message key="LBL_PART_DESC"/></td>
						<td width="100"><fmtPartPriceRpt:message key="LBL_EQUITY_PRICE"/></td>
						<td width="100"><fmtPartPriceRpt:message key="LBL_LOAN_PRICE"/></td>
						<td width="100" align="middle"><fmtPartPriceRpt:message key="LBL_CONS_PRICE"/></td>
						<td width="100"><fmtPartPriceRpt:message key="LBL_LIST_PRICE"/></td>
					</tr>
					<tr><td class="Line" colspan="5"></td></tr>					
					<tr><td colspan="5" class="RightTextRed" align="center"> <BR><fmtPartPriceRpt:message key="LBL_NO_PART_FOR_PROJ"/>	</td></tr>
				</table>
<%
		}
%>
			</td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
<%
	}
%>
</FORM>
</BODY>
 <%@ include file="/common/GmFooter.inc" %>

</HTML>
