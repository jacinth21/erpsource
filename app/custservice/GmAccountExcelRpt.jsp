 <%@page import="java.util.Locale"%>
<%
/**********************************************************************************
 * File		 		: GmAccountExcelRpt.jsp
 * Desc		 		: This screen is used for Excel in Account net Qnty by lot
 * Version	 		: 1.0
 * author			: Sindhu Mariyappan
************************************************************************************/
%>
<!-- \custservice\GmAccountExcelRpt.jsp -->
<%@ page import ="com.globus.common.beans.GmCommonClass"%>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Date" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtFSWarehouseExcel" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<%
Locale locale = null;
String strLocale = "";

String strJSLocale = "";

String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));



if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
String format = request.getParameter("hMode");
if ((format != null) && (format.equals("Excel"))) {
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-disposition","attachment;filename=345"+"accountrpt"+"11.xls"); 
		 

}
HashMap hmReturn = new HashMap();
HashMap hmLoop = new HashMap();
ArrayList alResult = new ArrayList();
HashMap hmTemp = new HashMap();
hmReturn = GmCommonClass.parseNullHashMap((HashMap)(HashMap)request.getAttribute("HMDATAVAL"));
alResult = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALACCRPT"));
String strAccOutType = GmCommonClass.parseNull((String)request.getAttribute("ACCOUTTYPE"));
String strCompanyName =GmCommonClass.parseNull((String)hmReturn.get("GMCOMPANYNAME"));
String strAddress =GmCommonClass.parseNull((String)hmReturn.get("3FLRADDRESS"));
String strPhone =GmCommonClass.parseNull((String)hmReturn.get("3FLRPHONE"));
String strFax =GmCommonClass.parseNull((String)hmReturn.get("3FLRFAX"));
String strRequestBy =GmCommonClass.parseNull((String)hmReturn.get("REQUESTEDBY"));
String strAccName =GmCommonClass.parseNull((String)hmReturn.get("ACCT_NAME"));
String strSurgDate = GmCommonClass.parseNull((String)request.getAttribute("SURGDATE"));
String strShipDate = GmCommonClass.parseNull((String)request.getAttribute("SHIPDATE")); 

String strLot ="";
String strPartDesc="";
String strPart ="";
String strQty ="";
String strSetName ="";
String strDealerId = "";
String strDealerNm = "";
String strAccId = "";
String strAccNm="";
String strColspan ="";
if(strAccName.equals("")){ 
     strColspan = "1";
} else{ 
	 strColspan = "2";
} 
%>
<fmtFSWarehouseExcel:setLocale value="<%=strLocale%>"/>
<fmtFSWarehouseExcel:setBundle basename="properties.labels.operations.GmFieldSalesWarehouseExcelRpt_en_JP"/>
<HTML>
<HEAD>
<TITLE> Globus Medical: Pending Invoices Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style>
.RightTableCaption {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
}

.Line{
	background-color: #676767;
}
TR.ShadeRightTableCaption{
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
	background-color: #cccccc;
}
.RightText {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
}
.text{
  mso-number-format:"\@";     /*force text*/
}
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10">

	<table border="0" width="1200" cellspacing="0" cellpadding="0" align="left">
	<tr>
	<td colspan="4"></td>
	<td style="font-size: 24px;"><fmtFSWarehouseExcel:message key="LBL_DETAIL_SLIP"/></td>
	</tr>
	<tr align="right">
	<td colspan="6"></td>
	<td colspan="4" style="font-size: 20px;"><fmtFSWarehouseExcel:message key="LBL_HEADER"/></td>
	</tr>
	<tr align="left"><td></td>
	<td colspan="6" ></td>
	<td align="right" style="border-top:solid;border-left:solid;border-right:solid; border: 1px;font-size: 20px;" colspan="3" >&nbsp;<%=strCompanyName%></td>
	</tr>
	<tr align="left"><td></td>
	<td style="border-bottom: solid;border: 1px;font-size: 20px;" width="50px;"><fmtFSWarehouseExcel:message key="LBL_SHIP_DATE"/>:&nbsp;<%=strShipDate%></td>&nbsp;&nbsp;&nbsp;
	<td colspan="1"></td><td colspan="1" style="border-bottom: solid;border: 1px;font-size: 20px;"><fmtFSWarehouseExcel:message key="LBL_SURGERY_DATE"/>:&nbsp;<%=strSurgDate%></td>
	<td colspan="3"></td>
	
	<td align="right" style="border-left:solid;border-right:solid; border: 1px;font-size: 20px;" colspan="3" >&nbsp;<%=strAddress%></td>
	
	</tr>
	<tr>
	<td colspan="7" align="right" ></td>
	<td style="border-left:solid;border-right:solid; border-bottom:solid; border: 1px;font-size: 20px;" colspan="3" align="center" >&nbsp;TEL :&nbsp;<%=strPhone%>&nbsp;&nbsp;&nbsp;&nbsp;FAX :&nbsp;&nbsp;<%=strFax%></td>
	</tr>
	<tr>
	<td colspan="7" ></td>
	<td style="border-bottom: solid;border: 1px;font-size: 20px;" colspan="3" ><fmtFSWarehouseExcel:message key="LBL_REQ_BY"/>:&nbsp;<%=strRequestBy %></td>
	</tr>
	
	<tr> <td></td>
	<td style="font-size: 20px;"><fmtFSWarehouseExcel:message key="LBL_ACC_NAME"/>:</td>
	<td colspan="2" style="border-bottom: solid;border: 1px;font-size: 20px;" align="left"><%=strAccName %></td>
	<td style="font-size: 20px;"><fmtFSWarehouseExcel:message key="LBL_SAN"/></td>
	</tr>
	<tr><td colspan="8">&nbsp;&nbsp;</td></tr>
	</table>
<BODY leftmargin="20" topmargin="10">
	<table border="1" width="1200" cellspacing="0" cellpadding="0">
					
					<tr class="ShadeRightTableCaption">
					<td HEIGHT="24" width="50" align="center">No.</td>
					<td HEIGHT="24" width="300" align="center"><fmtFSWarehouseExcel:message key="LBL_SYSTEM_NAME"/></td>
					<%-- <% if(!strAccOutType.equals("106930")){ %>
					<td HEIGHT="24" width="250" align="center">SYSTEM NAME</td>
					<%} %> --%>
					<% if(strAccName.equals("")){ %>
					<td width="300" align="center"><fmtFSWarehouseExcel:message key="LBL_DELEAR_NAME"/></td>
					<td HEIGHT="24" width="150" align="center"><fmtFSWarehouseExcel:message key="LBL_DELEAR_ID"/></td>
					<td HEIGHT="24" width="150" align="center"><fmtFSWarehouseExcel:message key="LBL_ACCOUNT"/></td>
					<td HEIGHT="24" width="300" align="center"><fmtFSWarehouseExcel:message key="LBL_ACCOUNT_NAME"/></td>
					<%} %>
					<td colspan="<%=strColspan %>" width="150" align="center"><fmtFSWarehouseExcel:message key="LBL_PART_NUM"/></td>
					<td colspan="<%=strColspan %>" HEIGHT="24" width="300" align="center"><fmtFSWarehouseExcel:message key="LBL_DESC"/></td>
					<td colspan="<%=strColspan %>" width="100" align="center"><fmtFSWarehouseExcel:message key="LBL_LOT"/></td>
					<td colspan="<%=strColspan %>" width="75" align="center"><fmtFSWarehouseExcel:message key="LBL_QTY"/></td>
					</tr>
					<%
					for(int i=0;i<alResult.size();i++){
						hmLoop = (HashMap)alResult.get(i);
						strLot = GmCommonClass.parseNull((String)hmLoop.get("LOT"));
						strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PART_DESC"));
						strPart = GmCommonClass.parseNull((String)hmLoop.get("PART"));
						strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
						strSetName = GmCommonClass.parseNull((String)hmLoop.get("SET_NAME"));
						strDealerNm = GmCommonClass.parseNull((String)hmLoop.get("DEALERNAME"));
						strDealerId = GmCommonClass.parseNull((String)hmLoop.get("DEALERID"));
						strAccId = GmCommonClass.parseNull((String)hmLoop.get("ACCOUNTID"));
						strAccNm = GmCommonClass.parseNull((String)hmLoop.get("ACCOUNTNAME"));
						%>
						<tr>
						<td align="center"><%=i+1 %></td>
						<td><%=strSetName %></td>
						<% if(strAccName.equals("")){ %>
						<td><%=strDealerNm %></td>
						<td><%=strDealerId %></td>
						<td><%=strAccId%></td>
						<td><%=strAccNm%> </td>
						<%} %>
						<td align="left" class="text" colspan="<%=strColspan %>" ><%=strPart %></td>
						<td colspan="<%=strColspan %>"><%=strPartDesc %></td>
						<td align="left" colspan="<%=strColspan %>" ><%=strLot %></td>
						<td align="right" colspan="<%=strColspan %>" ><%=strQty %></td>
						</tr>
						<%
					}
					%>
    </table>

</BODY> 

</HTML>
