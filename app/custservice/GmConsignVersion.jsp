<!-- \custservice\GmConsignVersion.jsp -->
<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>

<%
	String strHtmlJasperRpt = "";
	HashMap hmResult = (HashMap)request.getAttribute("hmResult");
	String strReportName = GmCommonClass.parseNull((String)hmResult.get("JASPERNAME"));
	
%>
<script>
function fnPrint()
{
	window.print();
	return false;
}
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
	strObject1 = eval("document.all.button1");
	tdimginnner = strObject1.innerHTML;
	strObject1.innerHTML = "";	
}
function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
	strObject1 = eval("document.all.button1");
	strObject1.innerHTML = tdimginnner ;
}
</script>
<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<table>
		<tr>
			<td align="right" height="30" id="button1">
				<img style="cursor:hand" src='<%=strImagePath%>/print-icon.png' height="25" width="25" alt="Click here to print page" onClick="return fnPrint();" />
				<img style="cursor:hand" src='<%=strImagePath%>/close_icon.png' height="20" width="20" alt="Click here to close window" onClick="window.close();" />
			</td>
		</tr>
		
		
	 	<tr>	
			<td>
				<%
				
				GmJasperReport gmJasperReport = new GmJasperReport();			
				gmJasperReport.setRequest(request);
				gmJasperReport.setResponse(response);
				gmJasperReport.setJasperReportName(strReportName);
				gmJasperReport.setHmReportParameters(hmResult);
				gmJasperReport.setReportDataList(null);
				gmJasperReport.setBlDisplayImage(true);
				strHtmlJasperRpt = gmJasperReport.getHtmlReport();				
				%>	
				<%=strHtmlJasperRpt%>
			</td>
		</tr>
		
		<tr>
		<td align="center" colspan="6" height="30" id="button">
			<gmjsp:button value="Print" name="Btn_Print" style="width: 4em; height: 23;"  gmClass="button" buttonType="Load" onClick="fnPrint();" />&nbsp;&nbsp;&nbsp;&nbsp;
			<gmjsp:button value="Close" name="Btn_Close" style="width: 4em; height: 23;"  gmClass="button" buttonType="Load" onClick="window.close();" />
			</td>
		</tr>
	
	  
</table>
<%@ include file="/common/GmFooter.inc"%>
</BODY>