<%
/**********************************************************************************
 * File		 		: GmDODashBoard.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Dhana Reddy
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtDODashBoard" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<!-- custservice\GmDODashBoard.jsp -->

<fmtDODashBoard:setLocale value="<%=strLocale%>"/>
<fmtDODashBoard:setBundle basename="properties.labels.custservice.GmDODashBoard"/>


<bean:define id="currFmt" name="frmDODashboard" property="applnCurrFmt" type="java.lang.String"></bean:define>
<bean:define id="currsign" name="frmDODashboard" property="applnCurrSign" type="java.lang.String"></bean:define>
<%
String strCustServiceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
String strCurrFmt = "{0,number,"+currsign+" "+currFmt+"}";
String strApplDateFmt = strGCompDateFmt;
String strDTDateFmt = "{0,date,"+strApplDateFmt+"}";
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: DO DashBaord </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta http-equiv="Cache-control" content="no-cache">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strCustServiceJsPath%>/GmDOProcess.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"></script>
</HEAD>


<BODY leftmargin="20" topmargin="10" >

<html:form  action="/gmDOReport.do?method=loadDODashboard">
<table border="0" class="DtTable900" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader" ><fmtDODashBoard:message key="LBL_DO_DASHBOARD"/></td>
			
			<td class="RightDashBoardHeader"></td>
			<td  class="RightDashBoardHeader" align="right" colspan="2">
		    </td>
		    <td align="right" class=RightDashBoardHeader>
			<fmtDODashBoard:message key="IMG_ALT_HELP" var = "varHelp"/>
			<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("DO_DASH_BOARD")%>');" />
			</td>
		</tr>
		<tr>
			<td colspan="6"> <display:table 
					name="requestScope.frmDODashboard.alResult" 
					export="false" class="its" id="currentRowObject" 
					decorator="com.globus.custservice.displaytag.beans.DTDODashWrapper"
					freezeHeader="true"  paneSize="600"
					requestURI="/gmDOReport.do?method=loadDODashboard">
					<fmtDODashBoard:message key="DT_CASE" var="varCase"/>
					<display:column property="CASEID" title="${varCase}" style="width:260" sortable="false"/>
					<fmtDODashBoard:message key="DT_DO_ID" var="varDoId"/>
					<display:column property="ORDER_ID" title="${varDoId}" style="width:150" sortable="true"/>
					<fmtDODashBoard:message key="DT_TYPE" var="varType"/>
					<display:column property="ORDER_TYPE" title="${varType}" style="width:80" sortable="true"/>
					<fmtDODashBoard:message key="DT_STATUS" var="varStatus"/>
					<display:column property="STATUS" title="${varStatus}" style="width:100" sortable="true"/>
					<fmtDODashBoard:message key="DT_REQUEST_FORM" var="varReqForm"/>
					<display:column property="REP_NAME" title="${varReqForm}" style="width:150" sortable="true"/>
					<fmtDODashBoard:message key="DT_ACCOUNT" var="varAccount"/>
					<display:column property="ACC_NAME" title="${varAccount}" style="width:160" sortable="true"/>
					<fmtDODashBoard:message key="DT_REPLN_REQ" var="varReplnReq"/>
					<display:column property="REPLN" title="${varReplnReq}" style="width:80" sortable="true"/>
					<fmtDODashBoard:message key="DT_SURGERY_DATE" var="varSurgeryDate"/>
					<display:column property="SDATE" title="${varSurgeryDate}" style="width:100" sortable="true" format="<%=strDTDateFmt%>" />
					<fmtDODashBoard:message key="DT_REQUESTED_DATE" var="varRequestedDate"/>
					<display:column property="REQUESTED_DATE_FMT" title="${varRequestedDate}" style="width:100" sortable="true"/>
					<fmtDODashBoard:message key="DT_ORDER_VALUE" var="varOrderValue"/>
					<display:column property="TOTAL_COST_FMT" sortProperty="TOTAL_COST" title="${varOrderValue}"  style="width:90" sortable="true"/>
				</display:table> 
			</td>
		</tr>
</table>	
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>

