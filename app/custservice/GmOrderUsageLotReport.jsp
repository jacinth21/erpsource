<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
/**********************************************************************************
 * File		 		:GmOrderUsageLotReport.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Tamizhthangam Ramasamy
************************************************************************************/
%>
<!-- /custservice/GmOrderUsageLotReport.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false"%>
<%@ page import="java.util.ArrayList,java.util.HashMap"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%> 
<%@ taglib prefix="fmtSalesOrdUsageLotrpt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtSalesOrdUsageLotrpt:setLocale value="<%=strLocale%>" />
<fmtSalesOrdUsageLotrpt:setBundle basename="properties.labels.custservice.GmOrderUsageLotReport" />
<bean:define id="gridData" name="frmUsageLotRpt" property="xmlString" type="java.lang.String"></bean:define>
<bean:define id="account" name="frmUsageLotRpt" property="account" type="java.lang.String"></bean:define>
<bean:define id="accountId" name="frmUsageLotRpt" property="accountId" type="java.lang.String"></bean:define>
<bean:define id="searchaccount" name="frmUsageLotRpt" property="searchaccount" type="java.lang.String"></bean:define>
<bean:define id="dealer" name="frmUsageLotRpt" property="dealer" type="java.lang.String"></bean:define>
<bean:define id="searchdealer" name="frmUsageLotRpt" property="searchdealer" type="java.lang.String"></bean:define>
<bean:define id="fieldSales" name="frmUsageLotRpt" property="fieldSales" type="java.lang.String"></bean:define>
<bean:define id="searchfieldSales" name="frmUsageLotRpt" property="searchfieldSales" type="java.lang.String"></bean:define>
<bean:define id="searchsalesRep"  name="frmUsageLotRpt" property="searchsalesRep" type="java.lang.String"></bean:define>
<bean:define id="salesRep" name="frmUsageLotRpt" property="salesRep" type="java.lang.String"></bean:define>
<bean:define id="searchprojectID"  name="frmUsageLotRpt" property="searchprojectID" type="java.lang.String"></bean:define>
<bean:define id="projectID" name="frmUsageLotRpt" property="projectID" type="java.lang.String"></bean:define>
<bean:define id="strHideOrdUsageRptFl" name="frmUsageLotRpt" property="strHideOrdUsageRptFl" type="java.lang.String"></bean:define>

<%
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
String strWikiTitle = GmCommonClass.getWikiTitle("SALES_ORDER_USAGE_LOT_REPORT");
String strApplDateFmt = strGCompDateFmt;
String strProjName = (String)request.getAttribute("hProjNm")==null?"":(String)request.getAttribute("hProjNm");
String strProjId = (String)request.getAttribute("hProjId")==null?"":(String)request.getAttribute("hProjId");
String strFromDt = GmCommonClass.parseNull((String)request.getAttribute("hFrmDt"));  
String strToDt = GmCommonClass.parseNull((String)request.getAttribute("hToDt"));  
String strSessApplDateFmt= strGCompDateFmt;

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Globus Medical: Sales Order Usage Lot Report</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmOrderUsagelotReport.js"></script>
<script>
var objGridData;
objGridData='<%=gridData%>';
var format = '<%=strApplDateFmt%>';
var strHideFSOrdUsageRptFl = '<%=strHideOrdUsageRptFl%>';
</script>
<style>
#searchfieldSales, #searchsalesRep{
margin-left: 5px;
}
</style>

</head>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<html:form action="/gmOrderUsageLotRpt.do?method=orderUsageLotRpt">
<html:hidden property="strOpt" value="" />
<input type="hidden" id="hAccId" value="">
<table border="0" class="DtTable1650" cellspacing="0" cellpadding="0">

		 <tr>
	   <td colspan="5" height="25" class="RightDashBoardHeader"><fmtSalesOrdUsageLotrpt:message key="LBL_SALES_ORDER_USAGE_LOT_REPORT" /></td>
		<td class="RightDashBoardHeader"><fmtSalesOrdUsageLotrpt:message key="IMG_HELP" var="varHelp" />
		<img align="right" id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16'
			onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		</td>
   </tr>		
    <tr><td class="LLine" colspan="6" height="1"></td></tr>
   <tr>
      <td class="RightTableCaption" align="right" height="30">&nbsp;<fmtSalesOrdUsageLotrpt:message key="LBL_DEALER" />:</td>
	  <td class="RightText"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="dealer" />
					<jsp:param name="METHOD_LOAD" value="loadPartyNameList&party_type=26230725" />
					<jsp:param name="WIDTH" value="200" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value="1"/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%=searchdealer%>" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="<%=dealer%>" />
					<jsp:param name="SHOW_DATA" value="10" />
				</jsp:include>
				
	 </td> 
	<td class="RightTableCaption" align="right" height="30">&nbsp;<fmtSalesOrdUsageLotrpt:message key="LBL_ACCOUNT" />:</td>
	 <td colspan="1" align="left">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
										<jsp:param name="CONTROL_NAME" value="account" />
										<jsp:param name="METHOD_LOAD" value="loadAccountList&searchType=LikeSearch" />
										<jsp:param name="WIDTH" value="" />
										<jsp:param name="CSS_CLASS" value="search" />
										<jsp:param name="TAB_INDEX" value="2"/>
										<jsp:param name="CONTROL_NM_VALUE" value="<%=searchaccount %>" /> 
										<jsp:param name="CONTROL_ID_VALUE" value="<%=account %>" />
										<jsp:param name="SHOW_DATA" value="100" />
										<jsp:param name="AUTO_RELOAD" value="fnChange(this.form);" />			
									</jsp:include></td>
				<td align="left"><input type="text" size="8"  name="accountId" class="InputArea" value="<%=account%>" onfocus="changeBgColor(this,'#AACCE8');" onBlur="javascript:fnDisplayAccNm(this);" tabindex="3" /></td>
		</tr>					
		</table>
 	</td>	

	<td colspan="2"></td> 
	 
   </tr>
   <tr><td class="LLine" colspan="6" height="1"></td></tr>
   <tr Height="25" class="Shade">
	  <td class="RightTableCaption"  align="right">&nbsp; <fmtSalesOrdUsageLotrpt:message key="LBL_DATE_TYPE"/>:</td>
	  <td>&nbsp;<gmjsp:dropdown controlName="datetype" SFFormName="frmUsageLotRpt" SFSeletedValue="datetype" 
	                SFValue="alDateType" codeId="CODEID" codeName="CODENM"   tabIndex="3" /></td>
	     	     <fmtSalesOrdUsageLotrpt:message key="LBL_FROM_DATE" var="varFromDate"/>
		<td class="RightTableCaption" align="right" height="25"><gmjsp:label type="RegularText"  SFLblControlName="${varFromDate}:" td="false"/>
		<%if(!strHideOrdUsageRptFl.equals("YES")) {%>
         <td>&nbsp;<gmjsp:calendar SFFormName="frmUsageLotRpt" textControlName="fromDate" textValue="<%=(strFromDt.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strFromDt,strSessApplDateFmt).getTime())))%>" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="4"/> </td>
		<%  }else{ %>
		   <td>&nbsp;<gmjsp:calendar SFFormName="frmUsageLotRpt" controlName="fromDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="5"/> </td>
		<% } %>
		 <fmtSalesOrdUsageLotrpt:message key="LBL_TO_DATE" var="varToDate"/>
		<td class="RightTableCaption"  align="left" height="25" ><gmjsp:label type="RegularText"  SFLblControlName="${varToDate}:" td="false"/>
		<%if(!strHideOrdUsageRptFl.equals("YES")) {%>
		&nbsp;<gmjsp:calendar SFFormName="frmUsageLotRpt" textControlName="toDate" textValue="<%=(strToDt.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strToDt,strSessApplDateFmt).getTime())))%>"   gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="5"/>
		<%  }else{ %>
	    &nbsp;<gmjsp:calendar SFFormName="frmUsageLotRpt" controlName="toDate"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="5"/> 
	   <% } %>
         <fmtSalesOrdUsageLotrpt:message key="BTN_LOAD" var="varLoad"/>
        <td align="left"><gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnLoad();" buttonType="Load" tabindex="11"/></td>
  </tr>
  <tr><td class="LLine" colspan="6" height="1"></td></tr>
  <tr>
     <td class="RightTableCaption" align="right" height="30"><fmtSalesOrdUsageLotrpt:message key="LBL_SHOW_MISSING_LOT_ORDERS" />:</td>
     <td> <html:checkbox property="missingLot"></html:checkbox> </td>
     <td class="RightTableCaption" align="right" height="30"><fmtSalesOrdUsageLotrpt:message key="LBL_PART_NUMBER" />:</td>
	<td >&nbsp;<html:text size="40" name="frmUsageLotRpt" property="partNumber" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="6" />
	 <html:select property ="pnumSuffix"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" >
							<html:option value="0" ><fmtSalesOrdUsageLotrpt:message key="LBL_CHOOSEONE"/></html:option>
							<html:option value="LIT" ><fmtSalesOrdUsageLotrpt:message key="LBL_LITERAL"/></html:option>
							<html:option value="LIKEPRE" ><fmtSalesOrdUsageLotrpt:message key="LBL_LIKE_PREFIX"/></html:option>
							<html:option value="LIKESUF" ><fmtSalesOrdUsageLotrpt:message key="LBL_LIKE_SUFFIX"/></html:option>
	</html:select>
	</td>
	
  </tr>
  <!-- LBL_DISTRIBUTOR, LBL_REP_NAME, LBL_PRO_LIST,LBL_LOT_NUM used in PC-3894 - Sterile - Order usage report-->
  <%if(!strHideOrdUsageRptFl.equals("YES")) {%>
 <tr Height="25" class="Shade">
 <td class="RightTableCaption" align="right" height="30">&nbsp;<fmtSalesOrdUsageLotrpt:message key="LBL_DISTRIBUTOR" />:</td>
	 <td class="RightText">
							<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="fieldSales" /> 
							<jsp:param name="METHOD_LOAD" value="loadFieldSalesList" />
							<jsp:param name="WIDTH" value="250" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="TAB_INDEX" value="7"/>
							<jsp:param name="CONTROL_NM_VALUE" value="<%=searchfieldSales %>" /> 
							<jsp:param name="CONTROL_ID_VALUE" value="<%=fieldSales %>" />  					
							<jsp:param name="SHOW_DATA" value="10" />
							<jsp:param name="ON_BLUR" value="fnDistName(this.value);" />
					</jsp:include>
</td>
<td height="25" class="RightTableCaption" align="right"  width="12%">&nbsp;<fmtSalesOrdUsageLotrpt:message key="LBL_REP_NAME" />:</td>
<td class="RightText">
			<jsp:include page="/common/GmAutoCompleteInclude.jsp">
						<jsp:param name="CONTROL_NAME" value="salesRep" />
						<jsp:param name="METHOD_LOAD" value="loadSalesRepList" />
						<jsp:param name="WIDTH" value="300" />
						<jsp:param name="CSS_CLASS" value="search" />
						<jsp:param name="TAB_INDEX" value="8" />
						<jsp:param name="CONTROL_NM_VALUE" value="<%=searchsalesRep%>" />
						<jsp:param name="CONTROL_ID_VALUE" value="<%=salesRep%>" />  
						<jsp:param name="SHOW_DATA" value="10" />
					</jsp:include>		         		
</td>
<td class="RightTableCaption" align="right" >&nbsp;<fmtSalesOrdUsageLotrpt:message key="LBL_PRO_LIST"/>:</td>
<td class="RightText">
<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="projectID" />
							<jsp:param name="METHOD_LOAD" value="loadProjectNameList&searchType=LikeSearch&searchBy=ProjectId" />
							<jsp:param name="WIDTH" value="350" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="TAB_INDEX" value="9"/>
							<jsp:param name="CONTROL_NM_VALUE" value="<%=searchprojectID%>" /> 
							<jsp:param name="CONTROL_ID_VALUE" value="<%=projectID%>" />
							<jsp:param name="SHOW_DATA" value="100" />
							</jsp:include>						
</td>
</tr>
<tr>

<td class="RightTableCaption" height="30" align="Right" >&nbsp;<fmtSalesOrdUsageLotrpt:message key="LBL_LOT_NUM"/>:</td>
			<td align="left" >&nbsp;
   <html:text size="20" name="frmUsageLotRpt" property="lotNumber" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="10" />
</td>
<td colspan="2"></td>
</tr>
<%  } %>
  <tr><td class="LLine" colspan="6" height="1"></td></tr>
 
 <%if (gridData.indexOf("cell") != -1) {%>
  <tr>
    <td colspan="6">
		<div id="dataGridDiv" height="500px" width="1650"></div>
		<div id="pagingArea" style="width: 1650px"></div> 			
    </td>	
  </tr>
  <tr><td class="LLine" colspan="6" height="1"></td></tr>
 
  <tr>
	  <td colspan="6" align="center">
			<div class='exportlinks'><fmtSalesOrdUsageLotrpt:message key="LBL_EXPORT_OPTIONS" />:<img src='img/ico_file_excel.png' />&nbsp;
			  <a href="#"onclick="fnDownloadXLS();"><fmtSalesOrdUsageLotrpt:message key="LBL_EXCEL" /></a>
			</div>
	  </td>
  </tr>
<%}else{ %>
      <tr><td class="LLine" colspan="6" height="1"></td></tr>
	<tr height="30">
		<td colspan="6" align="center"><fmtSalesOrdUsageLotrpt:message key="LBL_NO_DATA_AVAILABLE" /></td>
	</tr>
<%
  }
%>

		
</table>
 
<%@ include file="/common/GmFooter.inc" %>
</html:form>
</BODY>

</HTML>
