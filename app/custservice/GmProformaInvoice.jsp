
<%
	/*******************************************************************************
	 * File		 		: GmProformaInvoice.jsp
	 * Desc		 		: This screen is used for Loading the Proforma Invoice.
	 * Version	 		: 1.0
	 * author			: Xun
	 ********************************************************************************/
%>
 <!-- \custservice\GmProformaInvoice.jsp -->
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmJasperReport"%>
<%@ page import="java.util.HashMap,java.util.*,java.text.*"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<bean:define id="hmJasperDetails" name="frmProformaInvoice"	property="hmJasperDetails" type="java.util.HashMap"></bean:define>
<bean:define id="alReturn" name="frmProformaInvoice" property="alReturn" type="java.util.ArrayList"></bean:define> 
<%
 
String strHtmlJasperRpt = "";
String strJasperName =  GmCommonClass.parseNull((String)hmJasperDetails.get("JASPER_NAME"));
String strOpt=GmCommonClass.parseNull((String)hmJasperDetails.get("STROPT"));
//PMT-53539 japan-new-warehouse-address

String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
String strSetRbObjectFl =
    GmCommonClass.parseNull(gmResourceBundleBean.getProperty("ADD_RESOURCE_BUNDLE_OBJ"));
//PMT-53539 japan-new-warehouse-address
%>
<HTML>
<HEAD>
 
</HEAD>
<BODY leftmargin="20" topmargin="10"   >
	<table>
		<tr>
		<td><div>
			 <%
			    if (!hmJasperDetails.equals("")) {
			%>
			<td><div>
					<%
						String strJasperPath = GmCommonClass
									.getString("GMJASPERLOCATION");
							GmJasperReport gmJasperReport = new GmJasperReport();
							gmJasperReport.setRequest(request);
							gmJasperReport.setResponse(response);
							gmJasperReport.setJasperReportName(strJasperName);
							gmJasperReport.setHmReportParameters(hmJasperDetails);
							gmJasperReport.setReportDataList(alReturn);
							gmJasperReport.setBlDisplayImage(true);
						   if (strSetRbObjectFl.equalsIgnoreCase("YES") ) {
					          gmJasperReport.setRbPaperwork(gmResourceBundleBean.getBundle());//PMT-53539 japan-new-warehouse-address
					        }
							if(strOpt.equals("BOD")||strOpt.equals("DORECEIPT")){
								strHtmlJasperRpt = gmJasperReport.getXHtmlReport();
							}
							else
							{
								strHtmlJasperRpt = gmJasperReport.getHtmlReport();
							}
							
						}
					%>
					<%=strHtmlJasperRpt %>
				</div></td>
		</tr>

	</table>
	 
</BODY>
</HTML>