<% 
 /**********************************************************************************
 * File		 		: GmDOEditShipDetails
 * Desc		 		: This screen is used to Edit the Tag Details.
 * Version	 		: 1.0
 * author			: 
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
 <%@page import="com.globus.custservice.forms.GmDOSummaryEditForm" %>
 <%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

 <%@ taglib prefix="fmtDOEditShipping" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- custservice\GmDOEditShipping.jsp -->

<fmtDOEditShipping:setLocale value="<%=strLocale%>"/>
<fmtDOEditShipping:setBundle basename="properties.labels.custservice.GmDOEditShipping"/>
 
 
<bean:define id="alRepAdd" name="frmDOSummaryEdit" property="alRepAdd" type="java.util.ArrayList"></bean:define> 
<bean:define id="alShipTo" name="frmDOSummaryEdit" property="alShipTo" type="java.util.ArrayList"></bean:define>
<bean:define id="alShipToId" name="frmDOSummaryEdit" property="alShipToId" type="java.util.ArrayList"></bean:define>
<bean:define id="orderTypeNm" name="frmDOSummaryEdit" property="orderTypeNm" type="java.lang.String"></bean:define>
<bean:define id="custPO" name="frmDOSummaryEdit" property="custPO" type="java.lang.String"></bean:define>
<bean:define id="hmNames" name="frmDOSummaryEdit" property="hmNames" type="java.util.HashMap"></bean:define>
<bean:define id="shipMode" name="frmDOSummaryEdit" property="shipMode" type="java.lang.String"></bean:define>
<%
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
String strSessDeptId = GmCommonClass.parseNull((String)session.getAttribute("strSessDeptId"));
String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
String strSessTodaysDate = GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate"));
HashMap hmReturn = new HashMap();
HashMap hmOrderDetails = new HashMap();
ArrayList alShippingSummary = new ArrayList();
HashMap hmShipSummary = new HashMap();
HashMap hmAccsum =  new HashMap();
HashMap hmResult = new HashMap();
hmReturn = (HashMap)request.getAttribute("hmResult");
hmResult = (HashMap)request.getAttribute("hmReturn");
String strDoID = "";
String strCaseID = "";
String strAccountID = "";
String strAccountName = "";
String strAlertImage = "<img id=imgDis src=".concat(strImagePath).concat("/30.gif title='Alert'>");
String strRepName = "";
String strAdd1 = "";
String strAdd2 = "";
String strRepID = "";
String strRepAddId = "";
String strRegn = "";
String strAdName="";
String strDistName="";
String strTerrName="";
String strRepNm = "";
String strRepId ="";
String strGpoNm ="";
String strCustPo ="";
String strCaseInfoID ="";
String strDistID = "";
String strShipDoID ="";
String strShippTo ="";
String strShippTOID ="";
String strShippAddress ="";
String strModeID = "";
if (hmReturn != null)
{
	hmOrderDetails = (HashMap)hmReturn.get("ORDERDETAILS");
	strDoID = GmCommonClass.parseNull((String)hmOrderDetails.get("ID"));
	strCaseID = GmCommonClass.parseNull((String)hmOrderDetails.get("CASE_ID"));
	strAccountID = GmCommonClass.parseNull((String)hmOrderDetails.get("ACCID"));
	strAccountName = GmCommonClass.parseNull((String)hmOrderDetails.get("ANAME"));
	strRepName = GmCommonClass.parseNull((String)hmOrderDetails.get("REPNM"));
	strRepID = GmCommonClass.parseNull((String)hmOrderDetails.get("REPID"));
	strCustPo = GmCommonClass.parseNull((String)hmOrderDetails.get("PO"));
	strCaseInfoID = GmCommonClass.parseNull((String)hmOrderDetails.get("CASEINFO_ID"));
	alShippingSummary = (ArrayList)hmReturn.get("SHIPSUMMARY");
	for(int i=0;i<alShippingSummary.size(); i++){
		hmShipSummary = (HashMap)alShippingSummary.get(i);
		strShipDoID = GmCommonClass.parseNull((String)hmShipSummary.get("ORDID"));
		if(strShipDoID.equals(strDoID)){
		   strShippTo = GmCommonClass.parseNull((String)hmShipSummary.get("SHIPTO"));
		   strShippTOID = GmCommonClass.parseNull((String)hmShipSummary.get("SHIPTOID"));
		   strShippAddress = GmCommonClass.parseNull((String)hmShipSummary.get("ADDRESSID"));
		   strModeID = GmCommonClass.parseNull((String)hmShipSummary.get("SMODEID"));
		   strModeID = strModeID.equals("") ? shipMode : strModeID ;  
 		}
	}
}

strCustPo = (custPO == null || custPO.equals("")) ? strCustPo : custPO;

if(hmResult != null){
hmAccsum = (HashMap)hmResult.get("ACCSUM");
strRegn = GmCommonClass.parseNull((String)hmAccsum.get("RGNAME"));
strAdName = GmCommonClass.parseNull((String)hmAccsum.get("ADNAME"));
strDistName = GmCommonClass.parseNull((String)hmAccsum.get("DNAME"));
strTerrName = GmCommonClass.parseNull((String)hmAccsum.get("TRNAME"));
strRepNm = GmCommonClass.parseNull((String)hmAccsum.get("RPNAME"));
strRepId = GmCommonClass.parseNull((String)hmAccsum.get("REPID"));
strGpoNm = GmCommonClass.parseNull((String)hmAccsum.get("PRNAME"));
strDistID = GmCommonClass.parseNull((String)hmAccsum.get("DISTID"));
}
ArrayList alDistributor = new ArrayList();
ArrayList alRepList = new ArrayList();
ArrayList alAccList = new ArrayList();
ArrayList alEmpList = new ArrayList();
ArrayList alShipacctList = new ArrayList();
HashMap hcboVal = null;
if (hmNames != null)
{
	alDistributor = (ArrayList)hmNames.get("DISTLIST");
	alRepList = (ArrayList)hmNames.get("REPLIST");
	alAccList = (ArrayList)hmNames.get("ACCLIST");
	alEmpList = (ArrayList)hmNames.get("EMPLIST");
	alShipacctList = (ArrayList)hmNames.get("SHIPACCTLIST");
}
%>
<html>
<head>
<title>Globus Medical:Edit Shipping Details</title>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmIncShipDetails.js"></script>
<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
<script language="javascript" src="<%=strcustserviceJsPath%>/GmDOEditShipping.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>

var lblNames = '<fmtDOEditShipping:message key="LBL_NAMES"/>';
var lblMode = '<fmtDOEditShipping:message key="LBL_MODE"/>';
var lblAddress = '<fmtDOEditShipping:message key="LBL_ADDRESS"/>';

var ordId = '<%=strDoID%>';
var repid = '<%=strRepID%>';
var caseRepID = '<%=strRepID%>';
var orderTypeNm = '<%=orderTypeNm%>';
var accid = '<%=strAccountID%>';
var custPo = '<%=strCustPo%>';
var caseInfoID = '<%=strCaseInfoID%>';
var shippedTO ='<%=strShippTo%>';
var shippedName = '<%=strShippTOID%>';
var addid = '<%=strShippAddress%>';

var DistLen = <%=alDistributor.size()%>;
var RepLen = <%=alRepList.size()%>;
var AccLen = <%=alAccList.size()%>;
var EmpLen = <%=alEmpList.size()%>;
var ShipacctLen = <%=alShipacctList.size()%>;
var distid ='<%=strDistID%>';
var repname ='<%=strRepName%>';
var acname ='<%=strAccountName%>';

<%
hcboVal = new HashMap();
for (int i=0;i<alShipacctList.size();i++)
{
	hcboVal = (HashMap)alShipacctList.get(i);
%>
var ShipAcctArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>

<%
	hcboVal = new HashMap();
	for (int i=0;i<alDistributor.size();i++)
	{
		hcboVal = (HashMap)alDistributor.get(i);
%>
	var DistArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>

<%	
	hcboVal = new HashMap();
	for (int i=0;i<alAccList.size();i++)
	{
		hcboVal = (HashMap)alAccList.get(i);
%>
	var AccArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("DID")%>";
<%
	}
%>


<%
	hcboVal = new HashMap();
	for (int i=0;i<alEmpList.size();i++)
	{
		hcboVal = (HashMap)alEmpList.get(i);
%>
	var EmpArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>


<%
	hcboVal = new HashMap();
	for (int i=0;i<alRepList.size();i++)
	{
		hcboVal = (HashMap)alRepList.get(i);
%>
	var RepArr<%=i%> ="<%=hcboVal.get("REPID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("PARTYID")%>,<%=hcboVal.get("DID")%>";
<%
	}
%>

</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnLoad();">
<html:form action="/gmDOSummaryEdit.do?method=editShipDetails">
<html:hidden  property="orderID" name="frmDOSummaryEdit" value=""/>
<html:hidden  property="strOpt" name="frmDOSummaryEdit" value=" "/>
<html:hidden  property="shipCarrier" name="frmDOSummaryEdit" value=" "/>
<html:hidden  property="source" name="frmDOSummaryEdit" value=" "/>
<html:hidden  property="shipId" name="frmDOSummaryEdit" value=" "/>
<html:hidden  property="shipToID" name="frmDOSummaryEdit" value=" "/>
<html:hidden  property="repAddressID" name="frmDOSummaryEdit" value=" "/>
<html:hidden  property="parentOrderID" name="frmDOSummaryEdit"/>
<html:hidden  property="caseInfoID" name="frmDOSummaryEdit" value=" "/>
<html:hidden  property="accountID" name="frmDOSummaryEdit" value="<%=strAccountID%>"/>
<input type="hidden" name="screenType" value="Case" />
<input type="hidden" name="addIdReturned" />
<input type="hidden" name="hAddrId" />
 <table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
	<tr>
			<td height="25" class="RightDashBoardHeader" colspan="5"><fmtDOEditShipping:message key="LBL_EDIT_SHIPPING_ADDRESS"/></td>
			<td align="right" class=RightDashBoardHeader>
				<fmtDOEditShipping:message key="IMG_ALT_HELP" var = "varHelp"/>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("EDIT_SHIPP_ADDRESS")%>');" />
			</td>	
	</tr>
	<tr>
		<tr class="evenshade">
			<td height="25" class="RightTableCaption" align="right"><fmtDOEditShipping:message key="LBL_DO_ID"/>:</td>
			<td >&nbsp;<%=strDoID %></td>
			<td height="25" class="RightTableCaption" align="right"><fmtDOEditShipping:message key="LBL_CASE_ID"/>:</td>
			<td>&nbsp;<%=strCaseID %></td>
			<td height="25" class="RightTableCaption" align="Right">&nbsp;<fmtDOEditShipping:message key="LBL_CUSTOMER_PO"/>:</td>
			<td >&nbsp;<%=strCustPo%></td>			
		</tr>		
		<tr><td class="LLine" height="1" colspan="6"></td></tr>
		<tr class="oddshade">
			<td height="25" class="RightTableCaption" align="right"><fmtDOEditShipping:message key="LBL_ACCOUNT_ID"/>:</td>
			<td >&nbsp;<%=strAccountID %></td>		
			<td height="25" class="RightTableCaption" align="right"><fmtDOEditShipping:message key="LBL_ACCOUNT_NAME"/>:</td>
			<td colspan="2">&nbsp;<%=strAccountName %></td>	
			<td>&nbsp;</td>				
		</tr>				  
	<tr><td class="LLine" height="1" colspan="6"></td></tr>
	<%if(!strSessDeptId.equals("S")){ %>
		<tr class="evenshade">
			<td height="25" class="RightTableCaption" align="Right">&nbsp;<fmtDOEditShipping:message key="LBL_REGION"/>: </td>
			<td >&nbsp;<%=strRegn %></td>
			<td height="25" class="RightTableCaption" align="Right">&nbsp;<fmtDOEditShipping:message key="LBL_AREA_DIRECTOR"/>:</td>
			<td >&nbsp;<%=strAdName %></td>
			<td height="25" class="RightTableCaption" align="Right">&nbsp;<fmtDOEditShipping:message key="LBL_FIELD_SALES"/>:</td>
			<td >&nbsp;<%=strDistName %></td>
		</tr>
		<tr>
				<td class="LLine" height="1" colspan="6"></td>
		</tr>
		<tr class="oddshade">
			<td class="RightTableCaption" align="Right"  height="25">&nbsp;<fmtDOEditShipping:message key="LBL_TERRITORY"/>:</td>
			<td >&nbsp;<%=strTerrName %></td>
			<td class="RightTableCaption" align="Right" height="25">&nbsp;<fmtDOEditShipping:message key="LBL_REP"/>:</td>
			<td >&nbsp;<%=strRepName%> &nbsp;(<%=strRepId %>)</td>
			<td class="RightTableCaption" align="Right">&nbsp;<fmtDOEditShipping:message key="LBL_GROUP_PRICE_BOOK"/>:</td>
			<td >&nbsp;<%=strGpoNm%></td>
		</tr>
		<tr>
				<td class="LLine" height="1" colspan="6"></td>
		</tr>
		<tr class="evenshade">
			<td height="25" class="RightTableCaption" align="Right" width=15%>&nbsp;<fmtDOEditShipping:message key="LBL_DATA_ENTERED"/>:</td>
			<td >&nbsp;<%=strSessTodaysDate%></td>
			<td height="25" class="RightTableCaption" align="Right" >&nbsp;<fmtDOEditShipping:message key="LBL_ORDER_TYPE"/>:</td>
			<td align="left"> &nbsp;<%=orderTypeNm%></td>
			<td class="RightTableCaption" colspan="2"></td>
		</tr>
		<tr><td class="LLine" height="1" colspan="6"></td></tr>
		<tr  class="oddshade">
		<%}else{ %>	
	<tr  class="evenshade">
	<%} %>
	<td height="25" colspan="6" class="RightTableCaption">&nbsp;<img border="0"  src="<%=strImagePath%>/30.gif" height="15" width="15"> &nbsp;<fmtDOEditShipping:message key="LBL_CLICK"/>Click on the Address you want the parts to be shipped to</td>
	</tr>
	<tr><td class="LLine" height="1" colspan="6"></td></tr>
	
	<% 
		int size = alRepAdd.size();

		int aSize = size;
		String strShade = "";
		String strWidth ="";		
		int actualRow = 0;
		int no_rows_full = (aSize/3);
		int no_rows_partial = (aSize%3) > 0 ? 1 : 0;
		int no_rows = no_rows_full + no_rows_partial;
		String strClick = "";
		for ( int row =0 ; row < no_rows  ; row++) {
	%>
					
	<tr><td colspan="6" ><table cellspacing="0" cellpadding="0" border="0" width=100%><tr>	
					<% 
					HashMap hm = new HashMap();
					for(int col=0;col<3;col++){						 
						  if (actualRow < size )
						  {
								hm = (HashMap) alRepAdd.get(actualRow);
								strAdd1 = GmCommonClass.parseNull((String)hm.get("NAME"));
								strRepAddId = GmCommonClass.parseNull((String)hm.get("ID"));
								strClick = "onclick=javascript:fnSave('" + strRepAddId + "'); onmouseover=this.style.cursor='hand'";
						  }	
						  else
						  {
							  strRepName ="";
							  strAdd1 = "";
							  strRepAddId ="";
							  strClick = "";
						  }
						
						if ((actualRow)%2 ==0)
						{
							strShade = "evenshade";						
						}
						else
						{
							strShade = "oddshade";							
						}
						
						if (col%2 == 0)
						{
							strWidth ="1";
						}
						else
						{
							strWidth ="0";
						}
						%>	
								
					<td class="LLine" width="<%=strWidth%>"></td>
					<td width=33%>
					<table cellspacing="0" cellpadding="0" border="0" <%=strClick%>>
					<tr class="<%=strShade %>"><td align="center"><b> <%=strRepName %> </b></td>  </tr> 
					<tr class="<%=strShade %>">
					<td align="center"><%=strAdd1 %> <br></td></tr>
					</table>
					</td>
					<td class="LLine" width="<%=strWidth%>"></td>					
		<%	
		actualRow ++;
     	}					
		%></tr>
		</table>
		</td>		
		</tr>
		<tr><td class="LLine" height="1" colspan="6"></td></tr>		
		<% }  %>
		
		<tr><br>
			<td height="25" class="RightDashBoardHeader" colspan="6" align="center"><fmtDOEditShipping:message key="LBL_OR_SHIP_TO_OTHER_ADDRESS"/></td>
		</tr>
		<tr>
		<td colspan="6">
		<table cellspacing="0" cellpadding="0" border="0" width=100%>
		<tr class="evenshade"> 
		<td height="30" class="RightTableCaption" align="right">To:</td>
		<td align="left" colspan="2">&nbsp;
				<span style="vertical-align:middle;"> 
				<!-- Custom tag lib code modified for JBOSS migration changes -->
		<gmjsp:dropdown controlName="shipTo" width="230" SFFormName='frmDOSummaryEdit' SFSeletedValue="shipTo"   defaultValue= "[Choose One]"	
						tabIndex="-1"  value="<%=alShipTo%>" codeId="CODEID" codeName="CODENM" onChange="javascript:fnGetNames(this);"/> </td>
		</span>
		<td height="30" class="RightTableCaption" align="right"><fmtDOEditShipping:message key="LBL_NAMES"/>:</td>
		<td align="left" colspan="2" class="RightTableCaption"> &nbsp;<gmjsp:dropdown controlName="names" controlId="names" width="230" SFFormName='frmDOSummaryEdit' SFSeletedValue="names"  defaultValue= "[Choose One]"	
				optionId="PARTYID" tabIndex="-1"  value="<%=alShipToId%>" codeId="ID" codeName="NAME" onChange="javascript:fnGetAddressList(this,'','Y')"/>
				&nbsp;&nbsp;<input type="checkbox" name="chkNames" onclick="javascript:fnGetAllNames();"/>&nbsp;<fmtDOEditShipping:message key="LBL_SHOWW_ALL"/> </td>
		</tr>
		<tr><td class="LLine" height="1" colspan="6"></td></tr>
		<tr class="oddshade"><td height="30" class="RightTableCaption" align="right">&nbsp;<fmtDOEditShipping:message key="LBL_ADDRESS"/>:</td>
		<td align="left" colspan="2">&nbsp;
		<span style="vertical-align:middle;"> 
			<select id="addressid" name="addressid" style="width:250px" onchange="fnChangeAddress(this)"></select>
		</span> 
			&nbsp;<fmtDOEditShipping:message key="IMG_ALT_ADDLOOKUP" var = "varAddLookup"/>
			<img style='cursor:hand' align="middle" onClick="javascript:fnAddress();" src='<%=strImagePath%>/Address-Book-icon.png' title="${varAddLookup}&nbsp;" height="26" width="26">
		</td>
		<td height="30" class="RightTableCaption" align="right"><fmtDOEditShipping:message key="LBL_MODE"/>:</td>
		<td align="left" colspan="2">&nbsp;<gmjsp:dropdown controlName="shipMode"  SFFormName='frmDOSummaryEdit'  seletedValue="<%=strModeID%>"  defaultValue="[Choose One]" tabIndex="-1"  SFValue="alShipMode" codeId="CODEID" codeName="CODENM"/></td>
		</tr>
		</table>
		</td>
		</tr>
		<tr>
		<td colspan="6">
			<jsp:include page="/common/GmIncludeLog.jsp" >
			<jsp:param name="FORMNAME" value="frmDOSummaryEdit" />
			<jsp:param name="ALNAME" value="alLogReasons" />
			<jsp:param name="LogMode" value="Edit" />
			</jsp:include>
		</td>
		</tr>
		<tr><td class="LLine" height="1" colspan="6"></td></tr>
		<tr >
			<td align="center" height="25" colspan="6" >
			<fmtDOEditShipping:message key="BTN_SAVE" var="varSave"/> 
			<gmjsp:button style="width: 5em" value="${varSave}" buttonType="Save" gmClass="button" onClick="fnSave('other');" /> &nbsp;&nbsp;&nbsp;
			<fmtDOEditShipping:message key="BTN_CANCEL" var="varCancel"/> 
			<gmjsp:button value="${varCancel}" style="width: 7em" buttonType="Load" onClick="fnCancel();"/>&nbsp;
		 	</td>
		</tr>
</table>
	<!-- 	<br>
		<a href="javascript:fnEditMultiShipping();" ><U>Ship Part to Multiple Address</U></a>  -->
		</html:form>
<%@ include file="/common/GmFooter.inc" %>
		</body>
		</html>
		