<!-- \customerservice\GmLotErrorReport.jsp -->
<%
	/**********************************************************************************
	 * File		 		: GmLotErrorReport.jsp
	 * Desc		 		: Lot Error Report view Page
	 * Version	 		: 1.0
	 * author			: N Raja
   ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtLotErrorRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page isELIgnored="false"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>

<%@ page import="com.globus.common.beans.GmCommonClass"%>
<fmtLotErrorRpt value="<%=strLocale%>" />
<fmtLotErrorRpt:setBundle basename="properties.labels.custservice.GmLotErrorReport" />

<bean:define id="fieldSales" name="frmLotErrorReport" property="fieldSales" type="java.lang.String"></bean:define>
<bean:define id="searchfieldSales" name="frmLotErrorReport" property="searchfieldSales" type="java.lang.String"></bean:define>
<bean:define id="lotNumber" name="frmLotErrorReport" property="lotNumber" type="java.lang.String"></bean:define>
<bean:define id="transactionId" name="frmLotErrorReport" property="transactionId" type="java.lang.String"></bean:define>
<bean:define id="transactionType" name="frmLotErrorReport" property="transactionType" type="java.lang.String"></bean:define>
<bean:define id="transFromDate" name="frmLotErrorReport" property="transFromDate" type="java.lang.String"></bean:define>
<bean:define id="transToDate" name="frmLotErrorReport" property="transToDate" type="java.lang.String"></bean:define>
<bean:define id="resolutionFromDate" name="frmLotErrorReport" property="resolutionFromDate" type="java.lang.String"></bean:define>
<bean:define id="resolutionToDate" name="frmLotErrorReport" property="resolutionToDate" type="java.lang.String"></bean:define>
<bean:define id="partNumber" name="frmLotErrorReport" property="partNumber" type="java.lang.String"></bean:define>
<bean:define id="resolutionType" name="frmLotErrorReport" property="resolutionType" type="java.lang.String"></bean:define>
<bean:define id="strFlag" name="frmLotErrorReport" property="strFlag" type="java.lang.String"></bean:define>
<%
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
String strWikiTitle = GmCommonClass.getWikiTitle("LOT_ERROR_REPORT");
String strApplDateFmt = strGCompDateFmt;
String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Lot Error Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css"> 
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script> 
<script language="javascript" src="<%=strJsPath%>/GmGrid.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmLotErrorReport.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_fast.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>	
<script>
var gCmpDateFmt = "<%=strGCompDateFmt%>";
var companyInfoObj = '<%=strCompanyInfo%>';
var strFlag = '<%=strFlag%>';
</script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     
     #transactionType { 
       width: 152px;
    }
     #resolutionType{
     width: 152px;
     }
     #status{
     width: 152px;
     }
   
</style> 

</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form action="/gmLotErrorReport.do?method=gmLotErrorRpt">
 <html:hidden name="frmLotErrorReport"  property="strOpt" value="" /> 
		<table class="DtTable1500" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader">&nbsp;<fmtLotErrorRpt:message key="LBL_LOT_ERROR_REPORT"/></td>
			<td colspan="7" height="25" class="RightDashBoardHeader" align="right">
				<fmtLotErrorRpt:message key="IMG_ALT_HELP" var="varHelp"/>
			    <img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    </td>
		</tr>
		
	    <tr>
			<td class="Line" height="0" colspan="9"></td>
		</tr>
		
		<tr class="Shade">
		<td class="RightRedCaption" align="Right"><fmtLotErrorRpt:message
						key="LBL_FIELD_SALES" />:&nbsp;</td>
		<td>
			<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="fieldSales" />
					<jsp:param name="METHOD_LOAD" value="loadFieldSalesList" />
					<jsp:param name="WIDTH" value="250" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="CONTROL_NM_VALUE" value="<%=searchfieldSales%>" />
					<jsp:param name="CONTROL_ID_VALUE" value="<%=fieldSales%>" />
					<jsp:param name="SHOW_DATA" value="10" />
					<jsp:param name="ON_BLUR" value="fnchkDist();" />
			    	</jsp:include>
		</td>
		
		<td class="RightRedCaption" align="Right"><fmtLotErrorRpt:message
						key="LBL_PART_NUMBER" />:</td>		
	    <td>&nbsp; <html:text property="partNumber" size="23" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
		</td>
		
		<td class="RightRedCaption" align="Right"><fmtLotErrorRpt:message
						key="LBL_LOT_NUMBER" />:</td>		
	    <td>&nbsp;		 			
	    <html:text property="lotNumber" size="23" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" />
		</td>
		<td></td>
		<td></td>
		<td></td>
		</tr>
	  
	   <tr>
		<td class="LLine" colspan="9" height="1"></td>
	  </tr>
	 
	  <tr>
		 <td class="RightRedCaption" align="Right"><fmtLotErrorRpt:message
						key="LBL_TRANSACTION_ID" />:</td>		
	    <td>&nbsp; <html:text property="transactionId" size="23" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"  />
		</td>
		<td class="RightRedCaption" align="Right" ><fmtLotErrorRpt:message
						key="LBL_TRANSACTION_TYPE" />:</td>	
						
		<td>&nbsp;	<gmjsp:dropdown controlName="transactionType" SFFormName="frmLotErrorReport" defaultValue="[Choose one]"
					SFValue="alTransType" codeId="CODEID"  codeName="CODENM" /></td>	
		
		<td class="RightRedCaption" align="Right" height="25">
			<fmtLotErrorRpt:message key="LBL_TRANSACTION_DATE"/>:</td>		
		<td>
		 <fmtLotErrorRpt:message key="LBL_FROM_DATE" var="varFromDate"/>
		<gmjsp:label type="RegularText"  SFLblControlName="${varFromDate}:" td="false"/>
        <gmjsp:calendar SFFormName="frmLotErrorReport" controlName="transFromDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="5"/>
          </td>
		 
		<td>
		<fmtLotErrorRpt:message key="LBL_TO_DATE" var="varToDate"/>
		<gmjsp:label type="RegularText"  SFLblControlName="${varToDate}:" td="false"/>
		 &nbsp;
		 <gmjsp:calendar SFFormName="frmLotErrorReport" controlName="transToDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="6"/></td>
		 <td></td>
		</tr>
		<tr>
		 <td class="LLine" colspan="9" height="1"></td>
		</tr>
		
		<tr class="Shade">  
		<td class="RightRedCaption" align="Right"><fmtLotErrorRpt:message
						key="LBL_STATUS" />:</td>		
		<td>&nbsp;	<gmjsp:dropdown controlName="status" SFFormName="frmLotErrorReport" SFSeletedValue="unResolvedStatus" defaultValue="[Choose one]" SFValue="alStatus" codeId="CODEID"  codeName="CODENM" />
		</td>					
					
		<td class="RightRedCaption" align="Right"><fmtLotErrorRpt:message
						key="LBL_RESOLUTION_TYPE" />:</td>
						
					<td>&nbsp;	<gmjsp:dropdown controlName="resolutionType" SFFormName="frmLotErrorReport" defaultValue="[Choose one]"
					SFValue="alResolutionType" codeId="CODEID"  codeName="CODENM" /></td>
					
		<td class="RightRedCaption" align="Right">
		<fmtLotErrorRpt:message key="LBL_RESOLUTION_DATE"/>:</td>	
		<td>			
		<fmtLotErrorRpt:message key="LBL_FROM_DATE" var="varFromDate"/>
		<gmjsp:label type="RegularText"  SFLblControlName="${varFromDate}:" td="false"/>
        <gmjsp:calendar SFFormName="frmLotErrorReport" controlName="resolutionFromDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="5"/> </td>
		<td>
		 <fmtLotErrorRpt:message key="LBL_TO_DATE" var="varToDate"/>
		<gmjsp:label type="RegularText"  SFLblControlName="${varToDate}:" td="false"/>
		 &nbsp;<gmjsp:calendar SFFormName="frmLotErrorReport" controlName="resolutionToDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="6"/>	
		</td>
		<td><fmtLotErrorRpt:message key="BTN_LOAD" var="varLoad"/>
					<gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" buttonType="Load" onClick="fnLoadRpt();"/></td>
		
		</tr>
		 <tr>
				<td class="LLine" colspan="10" height="1"></td>
			</tr>
        <tr> <td colspan="9">
          <div id="lotErrorReport" style="height:800px; display:table-caption;"></div>
 	 	 </td>
 	    </tr>
        <tr>
        <td colspan=9" height="1" bgcolor="#cccccc"></td>
        </tr>
				
		<tr>
		<td colspan="9" align="center">
		 <div class='exportlinks' id="DivExportExcel"><fmtLotErrorRpt:message key="LBL_EXPORT_OPTIONS"/> : <img src='img/ico_file_excel.png' onclick='fnExcel();' />&nbsp;<a href="#" onclick="fnExcel();"><fmtLotErrorRpt:message key="LBL_EXCEL"/></a></div>
		</td>
		 <tr>
		 <td height="30" colspan="9" align="center" class="RightTextBlue"><div id="DivNothingMessage"><fmtLotErrorRpt:message key="LBL_NO_DIS"/></div></td>
		</tr>
	  </table>
	  </html:form>
	
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
