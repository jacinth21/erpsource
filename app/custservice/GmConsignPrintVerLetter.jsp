<%
/**********************************************************************************
 * File		 		: GmConsignPrintVerLetter.jsp
 * Desc		 		: This screen is used for printing letter and print version 
 * Version	 		: 1.0
 * author			: Pramaraj
************************************************************************************/
%>
<!-- \custservice\GmConsignPrintVerLetter.jsp -->
<%@ include file="/common/GmHeader.inc" %>

<%@ page errorPage="/common/GmError.jsp" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmJasperReport"%>


<%

	GmServlet gm = new GmServlet();
	
	String strCssPath1 = GmCommonClass.getString("GMSTYLES");
	
	gm.checkSession(response,session);
	
	
	HashMap hmResult = new HashMap();
	hmResult = (HashMap)request.getAttribute("hmReturn");
	String strShipTo=(String)hmResult.get("SHIPTOID");
	String strOpt = GmCommonClass.parseNull(request.getParameter("hopt"));
	// Jasper print issue
	String strPrintVerOriginalCopy = "";
	String strPrintVerDuplocateCopy = "";
	strPrintVerOriginalCopy = GmCommonClass.parseNull((String)hmResult.get("PRINT_VER_ORIGINAL"));
	strPrintVerDuplocateCopy = GmCommonClass.parseNull((String)hmResult.get("PRINT_VER_DUPLICATE"));
	String strIctPer=(String)hmResult.get("INTRANSPERFROMA");	
%>


<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> --> <!-- Hiding the DOcType: to show the jasper content with center alignment -->
<html>
<head>
<title>Globus Medical:Consignment Set - Print Version and Consignment Letter</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath1%>/Globus.css"/>
<link rel="stylesheet" href="<%=strCssPath1%>/print.css" type="text/css" media="print" />
<script>
var hopt = '<%=strOpt%>';
function fnPrint()
{
	//window.print();
}
function hidebuttons()
{	


var strShipTo='<%=strShipTo%>';

	strObjectPrt=document.all.Btn_Prt;
	strObjectPrt.style.display = 'none';
	
	
	strObjectCls=document.all.Btn_Close;
	strObjectCls.style.display='none';
	if(strShipTo=="4122")
	{
	strObjectPrt=document.all.Btn_Print;
	strObjectPrt.style.display = 'none';
	}
	if(hopt != 'autoprint'){
		window.print();
	}
}
function hidePrintMain()
{
	buttonTdObj=document.getElementById("buttonTd");
	buttonTd.style.display='none';

}

function showPrintMain()
{

	buttonTdObj=document.getElementById("buttonTd");
	buttonTd.style.display='block';
	
}
function fnWPrint()
{
	if(hopt == 'autoprint'){
		setTimeout(fnAutoPrint,1);
	}
}
function fnAutoPrint(){
	var OLECMDID = 6;
	var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
	document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
	WebBrowser1.ExecWB(OLECMDID, 2);
	WebBrowser1.outerHTML = "";
} 
</script>
</head>
<BODY topmargin="10" onload="hidebuttons();fnWPrint();" onbeforeprint="hidePrintMain();" onafterprint="showPrintMain();">
<form>
		<!--  to print the Consignment Print copy -->
		
		<%if(!strIctPer.equals("INTRAPERFORMA")) { %>
		<%=strPrintVerOriginalCopy%>
		<%
			if (!strPrintVerDuplocateCopy.equals("")) {
		%>
		<%-- to print the duplicate consignment copy (if ship to not equal to sales rep) --%>
		<p STYLE="page-break-after: always"></p>
		<%=strPrintVerDuplocateCopy%>
		<%
			}
		%>

 <p STYLE="page-break-after: always"></p>
 				<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
				<jsp:include page="/custservice/GmConsignAckPrint.jsp" />
				
				
<% if(strShipTo.equals("4122"))
	{%>
            		<p STYLE="page-break-after: always"></p>
            		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
				<jsp:include page="/custservice/GmConsignPackslipPrint.jsp" />
				
	<%}%>	
            
				
		<%} else{ strPrintVerOriginalCopy="";%>
		
		<%=strPrintVerOriginalCopy%>
		<%
			if (!strPrintVerDuplocateCopy.equals("")) {
		%>
		<%-- to print the duplicate consignment copy (if ship to not equal to sales rep) --%>
		<p STYLE="page-break-after: always"></p>
		<%=strPrintVerDuplocateCopy%>
		<%
			}
		%>

	<%} %>
		
<table border="0" width="700" cellspacing="0" cellpadding="0" align=center>
		<tr>
			<td  align="center" height="30"  width="700" id="buttonTd">
				<gmjsp:button value="&nbsp;Print&nbsp;" name="Btn_PrintMain" gmClass="button" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				
				<gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_CloseMain" gmClass="button" onClick="window.close();" buttonType="Load" />
			</td>
		<tr>
</table>


</FORM>
</body>
</html>