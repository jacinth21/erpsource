<%@page import="com.globus.common.beans.GmResourceBundleBean"%>

 <%
/**********************************************************************************
 * File		 		: GmOrderAction.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@page import="java.util.Date"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtMdfOrder" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmOrderAction.jsp -->
<fmtMdfOrder:setLocale value="<%=strLocale%>"/>
<fmtMdfOrder:setBundle basename="properties.labels.custservice.ModifyOrder.GmOrderAction"/>
<%
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
    String strWikiTitle = GmCommonClass.getWikiTitle("ACCOUNT_SETUP");
    String strParentFl = GmCommonClass.parseNull((String)request.getAttribute("PARFLAG")); 

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	
	int intSize = 0;
	int intExclCmpnyFl=0;
	HashMap hmLoop = new HashMap();
	HashMap hcboVal = null;

	String strCodeID = "";
	String strSelected = "";
	String strAccId = "";
	String strRepId = "";
	
	String strOrdId = "";
	String strRefId = "";
	String strTempOrderId ="";
	String strFormattedOrderId = "";
	String strOrdDt = "";
	String strPrice = "";
	String strOrdBy = "";
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strItems = "";
	String strStatusFl = "";
	String strTrackNum = "";
	String strPO = "";
	String strInvId = "";
	String strShipToDate = "";
	String strOrderType = "";
	Date dtFrmDate = null;
	Date dtToDate = null;
	String strParentOId = "";
	String strShipCarr = "";
	
	String strShade = "";
	String strProjId = "";
	String strOrdCallFlag = "";
	String strInvCallFlag = "";
	String strShipDt = "";
	String strInvFl = "";
	String strOrderTypeDesc = "";
	String strAccName = "";
	
	String strOrderParam = "";
	String strRepParam = "";
	String strCustPOParam = "";
	String strTrackParam = "";
	Date dtShipFrmParam = null;
	Date dtShipToParam = null;
	String strTotalParam = "";
	String strOrderByParam = "";
	String strOrdSumId = "";
	String strParentOrdId = "";
	String strInvoiceId = "";
	String strHoldFl = "";
	String strHoldFilter = "";
	String strDoFlag= "";
	String strQuoteId = "";
	String strDeptId = GmCommonClass.parseNull((String)session.getAttribute("strSessDeptSeq"));
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.ModifyOrder.GmOrderAction", strSessCompanyLocale);
	String strLblOpnPDF = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_OPN_PDF"));
	String strLblOpnCSV = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_OPN_CSV"));
    HashMap hmCurrency = new HashMap();
	
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strExclCmpny =  GmCommonClass.parseNull((String)request.getAttribute("EXCLCOMPNY"));
	String strCurrSign = GmCommonClass.parseNull((String)hmCurrency.get("CMPCURRSMB"));
	String strCompCurrSign = strCurrSign;
	String strCntlNm = "";
	strCntlNm = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_TOTAL1"));
	String strEmailReqFl = "";
	String strEmailVersion = "";
	String strPDFFileID = "";
	String strCSVFileID = "";
	String strInvoicedYear = "";
	String strDisplayImage = "";
	String strRAStatusFL = "";
	String strAccCurrId = "";	
	String strAccCurrSym= "";   
	String strTrckNoSpaces = "";
	String strRuleCurr = "";
	String strAccNm = "";
	String strRepNm = "";
	String strOrderByNm = "";
	HashMap hmParam = new HashMap();

	
	hmParam = (HashMap)request.getAttribute("hmParam");
	if (hmParam != null)
	{
		strAccId = GmCommonClass.parseNull((String)hmParam.get("ACCID"));
		strOrderParam = GmCommonClass.parseNull((String)hmParam.get("ORDERID"));
		strRepParam = GmCommonClass.parseNull((String)hmParam.get("REPID"));
		strCustPOParam = GmCommonClass.parseNull((String)hmParam.get("CUSTPO"));
		strTrackParam = GmCommonClass.parseNull((String)hmParam.get("TRACK"));
		strTotalParam = GmCommonClass.parseNull((String)hmParam.get("TOTAL"));
		strOrderByParam = GmCommonClass.parseNull((String)hmParam.get("ORDERBY"));
		dtFrmDate = (Date)hmParam.get("ORDERFRM");
		dtToDate = (Date)hmParam.get("ORDERTO");
		dtShipFrmParam = (Date)hmParam.get("SHIPFRM");
		dtShipToParam = (Date)hmParam.get("SHIPTO");
		strInvoiceId = GmCommonClass.parseNull((String)hmParam.get("INVOICEID"));
		strParentOrdId = GmCommonClass.parseNull((String)hmParam.get("PARENTID"));
		strHoldFilter = GmCommonClass.parseNull((String)hmParam.get("HOLDFL"));
		strHoldFilter = strHoldFilter.equals("on")?"checked":"";
		strAccCurrId = GmCommonClass.parseNull((String)hmParam.get("ACC_CURR_ID"));
		strAccCurrSym = GmCommonClass.parseNull((String)hmParam.get("ACC_CURR_SYM"));
		// To get the Account name, Rep name, Order by name
		strAccNm = GmCommonClass.parseNull((String)hmParam.get("ACC_NAME"));
		strRepNm = GmCommonClass.parseNull((String)hmParam.get("REP_NAME"));
		strOrderByNm = GmCommonClass.parseNull((String)hmParam.get("ORDER_BY_NAME"));
	}
	
	strCurrSign = strAccCurrSym.equals("")?strCurrSign:strAccCurrSym;
	strCntlNm = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_TOTAL1"));
	
	String strApplnDateFmt = strGCompDateFmt;
	strOrderType = GmCommonClass.parseNull((String)request.getAttribute("ENBL_UPDATE_LOTNUM"));
	
	ArrayList alAccount = new ArrayList();
	ArrayList alAccountOrder = new ArrayList();
	ArrayList alRepList = new ArrayList();
	ArrayList alOrderByList = new ArrayList();
	ArrayList alCompCurrency = new ArrayList();
	ArrayList alChooseAction = new ArrayList();
	
	int intLength = 0;

	HashMap hmReturn = new HashMap();
	hmReturn = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturn"));
	if (hmReturn != null)
	{
		//alAccount = (ArrayList)hmReturn.get("ACCOUNTLIST");
		//alRepList = (ArrayList)hmReturn.get("REPLIST");
		//alOrderByList = (ArrayList)hmReturn.get("ORDERBYLIST");
		alAccountOrder = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ACCOUNTORDERLIST"));
		alCompCurrency = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALCOMPANYCURRENCY"));
		alChooseAction = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALCHOOSEACTION"));
		intLength = alAccountOrder.size();
	}

%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Order Action </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="javascript" src="<%=strcustserviceJsPath%>/GmOrderAction.js"></script>
<script language="javascript" src="<%=strcustserviceJsPath%>/GmOrderSelectAction.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxCombo.js"></script>
<script>
var compCurrSign = '<%=strCompCurrSign%>';
var orderByParam = '<%=strOrderByParam%>';
var servletPath = '<%=strServletPath%>';
var prevtr = 0;
var prevColor = '';
var clickCnt = 0;
var format = '<%=strApplnDateFmt%>';
var parAccfl = '<%=strParentFl%>';
var strRuleOrderType = '<%=strOrderType%>';
var currSign = '<%=strCurrSign%>';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnLoad();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmOrderVoidServlet" border="1">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hMode" value="">
<input type="hidden" name="hOrdId" value="">
<input type="hidden" name="hLen" value="<%=intLength%>">
<input type="hidden" name="hTxnId" value="">
<input type="hidden" name="hCancelType" value="VDRES">
<input type="hidden" name="hDeptId" value="<%=strDeptId%>">
<input type="hidden" name="hAccId" id="hAccId" value="">
<input type="hidden" name="hAccNm" id="hAccNm" value="">
<input type="hidden" name="hRepNm" id="hRepNm" value="">
<input type="hidden" name="hOrderByNm" id="hOrderByNm" value="">
<input type="hidden" name="hParentOrdId" value="">
<input type="hidden" name="hChangePrice" value="">

	<table border="0" bordercolor="red" width="1200" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="10" width="1" class="Line"></td>
			<td bgcolor="#666666" height="1"></td>
			<td rowspan="10" width="1" class="Line"></td>
		</tr>
		
		<tr>
			<td height="15" colspan="4" class="RightDashBoardHeader">
				<table border="0" width="100%" >
					<tr>
						<td colspan="3" height="15" width="90%" class="RightDashBoardHeader"></font><fmtMdfOrder:message key="LBL_MODIFY_ORDER"/></td>
						<fmtMdfOrder:message key="LBL_HELP" var="vaHelp"/>
						<td height="15" width="10%"class="RightDashBoardHeader" align="right"> <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${vaHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("MODIFY_ORDER")%>');" /> </td>
					</tr>
				</table>					
			</td>			
		</tr>

		<tr><td colspan="7" bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="evenshade">
						<td height="30" class="RightTableCaption" align="right" width="144px">
						<fmtMdfOrder:message key="LBL_REP_ACCT" var="varRepAcct"/>
						    <gmjsp:label type="RegularText"  SFLblControlName="${varRepAcct}:" td="false"/>
						</td>
				        <td width="420px">
				        <table border="0" width="100%" cellspacing="0" cellpadding="0">
				        	<tr>
				        		<td>
				        			<fmtMdfOrder:message key="LBL_CHOOSEACTION" var="varChooseAction"/>
				        			<%-- &nbsp;<gmjsp:autolist comboType="DhtmlXCombo" controlName="Cbo_AccId" seletedValue="<%=strAccId%>" value="<%=alAccount%>" codeId="ID" codeName="NAME" 
				        									defaultValue="${varChooseAction}" tabIndex="1" width="300" onChange="fnDisplayAccID"/> --%>
				        			<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
										<jsp:param name="CONTROL_NAME" value="Cbo_AccId" />
										<jsp:param name="METHOD_LOAD" value="loadAccountList&searchType=LikeSearch" />
										<jsp:param name="WIDTH" value="300" />
										<jsp:param name="CSS_CLASS" value="search" />
										<jsp:param name="TAB_INDEX" value="1"/>
										<jsp:param name="CONTROL_NM_VALUE" value="<%=strAccNm %>" /> 
										<jsp:param name="CONTROL_ID_VALUE" value="<%=strAccId %>" />
										<jsp:param name="SHOW_DATA" value="100" />
										<jsp:param name="AUTO_RELOAD" value="fnDisplayAccID();" />					
									</jsp:include>
				        		</td>
				        		<td class="RightTableCaption" align="right">
				        			&nbsp;
				        		</td>
				        		<td>
				        			&nbsp;<input type="text" size="8" value="<%=strAccId%>" name="Txt_AccId" class="InputArea" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="javascript:fnDisplayAccNm();">
				        		</td>
				        	</tr>
					    </table>	
					    </td>
						
						<td class="RightTableCaption" align="right" width="84px">
						    <fmtMdfOrder:message key="LBL_REP" var="varRep"/>
						    <gmjsp:label type="RegularText"  SFLblControlName="${varRep}:" td="false"/>
						</td>
						<td width="240px">
						
						    <%-- &nbsp;<gmjsp:dropdown controlName="Cbo_RepId"  seletedValue="<%= strRepParam %>" tabIndex="3"  width="200" value="<%= alRepList%>" codeId = "REPID"  codeName = "NAME"  defaultValue= "${varChooseAction}"/> --%>
						    <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="Cbo_RepId" />
							<jsp:param name="METHOD_LOAD" value="loadSalesRepList&searchType=LikeSearch" />
							<jsp:param name="WIDTH" value="200" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="TAB_INDEX" value="3"/>
							<jsp:param name="CONTROL_NM_VALUE" value="<%= strRepNm%>" /> 
							<jsp:param name="CONTROL_ID_VALUE" value="<%=strRepParam %>" />
							<jsp:param name="SHOW_DATA" value="50" />
							<jsp:param name="AUTO_RELOAD" value="" />
					     	</jsp:include>
						</td>
						<td class="RightTableCaption" align="right" width="84px">
						    <fmtMdfOrder:message key="LBL_ORDERID" var="varOrderId"/>
						    <gmjsp:label type="RegularText"  SFLblControlName="${varOrderId}:" td="false"/>
						</td>
						<td width="228px">
						    &nbsp;<input maxlength='20' type="text" size="15" value="<%=strOrderParam%>" name="Txt_OrdId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="4">
						</td>
						<td></td>
					</tr>
					<tr><td colspan="7" bgcolor="#CCCCCC" height="1"></td></tr>
					<tr class="oddshade">
						<td colspan="1" class="RightTablecaption" align="right"><fmtMdfOrder:message key="LBL_PARENT_ACCOUNT" />:</td>
						<td  class="RightTablecaption">&nbsp;<input type="checkbox" size="30" name="Chk_ParentFl" checked="<%=strParentFl%>" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex="5">					
				        			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Currency:&nbsp;<gmjsp:dropdown controlName="Cbo_AccCurrId"  seletedValue="<%=strAccCurrId%>" value="<%=alCompCurrency%>" codeId = "ID"  codeName = "NAMEALT" optionId="NAME" tabIndex="6"/>
				        </td>
						<td class="RightTableCaption" align="right">
						    <fmtMdfOrder:message key="LBL_CUSTPO" var="varCustPO"/>
						    <gmjsp:label type="RegularText"  SFLblControlName="${varCustPO}:" td="false"/>
						</td>
						<td>
						    &nbsp;<input maxlength='20' type="text" size="15" value="<%=strCustPOParam%>" name="Txt_CustPO" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="7">
						</td>
						<td class="RightTableCaption" align="right">
						    <fmtMdfOrder:message key="LBL_PARDO" var="varParentDO"/>
						    <gmjsp:label type="RegularText"  SFLblControlName="${varParentDO}:" td="false"/>
						</td>
						<td>
						    &nbsp;<input maxlength='20' type="text" size="15" value="<%=strParentOrdId%>" name="Txt_ParentOrd" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="8">
						</td>
						<fmtMdfOrder:message key="BTN_LOAD" var="varLoad"/>
						<td class="RightTableCaption" align="center">
                            <gmjsp:button value="&#818;${varLoad}&nbsp;" name="LoadBtn" gmClass="button" accesskey="L" onClick="fnGo();" tabindex="18" buttonType="Load" />&nbsp;
                        </td>						
					</tr>
					<tr><td colspan="7" bgcolor="#CCCCCC" height="1"></td></tr>
					<tr class="evenshade">
					<td height="30" align="right" class="RightTableCaption">
							<fmtMdfOrder:message key="LBL_ORDDATE" var="varOrdDate"/>
						    <gmjsp:label type="RegularText"  SFLblControlName="${varOrdDate}:" td="false"/>
						</td>
						<td>
						    <fmtMdfOrder:message key="LBL_FROMDATE" var="varFromDate"/>
						    <fmtMdfOrder:message key="LBL_TODATE" var="varToDate"/>
						    &nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varFromDate}:" td="false"/>
						    &nbsp;<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtFrmDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="9"/>
						    &nbsp;&nbsp;&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varToDate}:" td="false"/>
                            &nbsp;<gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=dtToDate==null?null:new java.sql.Date(dtToDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="10"/>&nbsp;
                            
						</td>
						<fmtMdfOrder:message key="LBL_INVOICEID" var="varInvId"/>
						<td class="RightTableCaption" align="right"><gmjsp:label type="RegularText"  SFLblControlName="${varInvId}:" td="false"/>
						</td>
						<td>&nbsp;<input maxlength='20' type="text" size="15" value="<%=strInvoiceId%>" name="Txt_InvoiceId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
						onBlur="changeBgColor(this,'#ffffff');" tabindex="11">
						</td>
						<td class="RightTableCaption" align="right">
						    <fmtMdfOrder:message key="LBL_ORDERBY" var="varOrderBy"/>
						    <gmjsp:label type="RegularText"  SFLblControlName="${varOrderBy}:" td="false"/>
						</td>
						<td colspan="7">
						    <%-- &nbsp;<gmjsp:dropdown controlName="Cbo_OrderBy"  seletedValue="<%=strOrderByParam%>" tabIndex="12"  width="175" value="<%=alOrderByList%>" defaultValue= "${varChooseAction}"/> --%>
						    <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="Cbo_OrderBy" />
							<jsp:param name="METHOD_LOAD" value="fetchEmployeeWithOrders&searchType=LikeSearch" />
							<jsp:param name="WIDTH" value="175" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="TAB_INDEX" value="12"/>
							<jsp:param name="CONTROL_NM_VALUE" value="<%=strOrderByNm%>" /> 
							<jsp:param name="CONTROL_ID_VALUE" value="<%=strOrderByParam %>" />
							<jsp:param name="SHOW_DATA" value="50" />
							<jsp:param name="AUTO_RELOAD" value="" />
					     	</jsp:include>
					    </td>
					    <td> </td>			
					</tr>
					<tr><td colspan="7" bgcolor="#CCCCCC" height="1"></td></tr>
					<tr class="oddshade">
					    <fmtMdfOrder:message key="LBL_SHIPPEDDATE" var="varShippedDate"/>
						<td align="right"  height="30" class="RightTableCaption"><gmjsp:label type="RegularText"  SFLblControlName="${varShippedDate}:" td="false"/>
                        </td>
					<td>
						    <fmtMdfOrder:message key="LBL_FROMDATE" var="varFromDate"/>
						    <fmtMdfOrder:message key="LBL_TODATE" var="varToDate"/>	
						    <fmtMdfOrder:message key="LBL_HOLD" var="varHold"/>					
							&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varFromDate}:" td="false"/>
							&nbsp;<gmjsp:calendar textControlName="Txt_ShipFromDate" textValue="<%=dtShipFrmParam==null?null:new java.sql.Date(dtShipFrmParam.getTime())%>"  gmClass ="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="13"/>
							&nbsp;&nbsp;&nbsp;<gmjsp:label type="RegularText" SFLblControlName="${varToDate}:" td="false"/>
							&nbsp;<gmjsp:calendar textControlName="Txt_ShipToDate" textValue="<%=dtShipToParam==null?null:new java.sql.Date(dtShipToParam.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="14"/>&nbsp;
						    &nbsp;&nbsp;&nbsp; <gmjsp:label type="RegularText"  SFLblControlName="<b>${varHold}?:</b>" td="false"/>
						    &nbsp;<input type="checkbox" name="Chk_HoldFl" <%=strHoldFilter%> tabindex="15">
						</td>
<!-- 						<td height="30" class="RightTableCaption" align="right">
						   
						</td> -->
						<fmtMdfOrder:message key="LBL_TRACKING" var="varTracking"/>
						<td class="RightTableCaption" align="right"><span id='label'><%=strCntlNm%></span><%-- <gmjsp:label type="RegularText"  SFLblControlName='<%=strCntlNm%>' td="false"/> --%></td>
						<td>&nbsp;<input type="text" size="15" value="<%=strTotalParam%>" name="Txt_Total" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
						onBlur="changeBgColor(this,'#ffffff');" tabindex="16"></td>
						<td class="RightTableCaption" align="right"><gmjsp:label type="RegularText"  SFLblControlName="${varTracking}:" td="false"/></td>
						<td colspan="7">
						    &nbsp;<input type="text" size="15" value="<%=strTrackParam%>" name="Txt_Track" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="17">
					    </td>
					    <td> </td>
					</tr>
					<tr><td colspan="7" bgcolor="#CCCCCC" height="1"></td></tr>
				</table>					
			</td>
		</tr>
		<tr><td colspan="7" bgcolor="#666666" width="1" height="1"></td></tr>
        <tr>
			<td height="100" valign="top">
			<div style="overflow:auto; height:400px;" id="OrderDiv">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				  <thead>
					<tr style="position:relative; top:expression(this.offsetParent.scrollTop);" class="ShadeRightTableCaption">
						<th HEIGHT="24" width="200"><fmtMdfOrder:message key="LBL_ORDERID" /></th>
						<th HEIGHT="24" width="200"><fmtMdfOrder:message key="LBL_REPACCOUNT" /></th>
						<th align="center" width="170"><fmtMdfOrder:message key="LBL_ORDERDATE"/></th>
						<th width="120" align="center"><fmtMdfOrder:message key="LBL_TOTAL" /></th>
						<th width="140" align="center"><fmtMdfOrder:message key="LBL_TYPE" /></th>
						<th width="150" align="center"><fmtMdfOrder:message key="LBL_ORDERBY" /></th>
						<th align="center" width="60"><fmtMdfOrder:message key="LBL_SHIPDAE" /></th>
						<th align="center" width="110"><fmtMdfOrder:message key="LBL_TRACKING1" /></th>
						<th align="center" width="180"><fmtMdfOrder:message key="LBL_CUST" /><BR><fmtMdfOrder:message key="LBL_PO"/></th>
						<th align="center" width="110"><fmtMdfOrder:message key="LBL_PARENT" /><BR><fmtMdfOrder:message key="LBL_ORDERID"/></th>
						<th align="center" width="140"><fmtMdfOrder:message key="LBL_INVOICE" /><BR><fmtMdfOrder:message key="LBL_ID"/></th>
						<th align="center"></th>
					</tr>
					<tr style="position:relative; top:expression(this.offsetParent.scrollTop);"><td class="Line" colspan="12"></td></tr>
				  </thead>
				  <tbody>
<%
		if (intLength > 0)
		{
				for (int i = 0;i < intLength ;i++ )
				{
					hmLoop = (HashMap)alAccountOrder.get(i);
					strOrdId = GmCommonClass.parseNull((String)hmLoop.get("ID"));
					strRefId = GmCommonClass.parseNull((String)hmLoop.get("REFID"));
					strFormattedOrderId = strOrdId.replaceAll("-","");
					strOrdDt = GmCommonClass.parseNull((String)hmLoop.get("ODT"));
					strPrice = GmCommonClass.parseNull((String)hmLoop.get("COST"));
					strOrdBy = GmCommonClass.parseNull((String)hmLoop.get("NM"));
					strItems = GmCommonClass.parseNull((String)hmLoop.get("CNT"));
					strStatusFl = GmCommonClass.parseNull((String)hmLoop.get("SFL"));
					strOrdCallFlag =  GmCommonClass.parseNull((String)hmLoop.get("ORD_CALL_FLAG"));
					strInvCallFlag = GmCommonClass.parseNull((String)hmLoop.get("INV_CALL_FLAG"));
					strTrackNum = GmCommonClass.parseNull((String)hmLoop.get("TRACK"));
					strParentOId = GmCommonClass.parseNull((String)hmLoop.get("PARENTOID"));
					strShipCarr = GmCommonClass.parseNull((String)hmLoop.get("CARRIER"));
					strTrckNoSpaces = strTrackNum.replaceAll("-","");
					strTrckNoSpaces = strTrackNum.replaceAll(" ","");
					strTrackNum = strTrackNum.equals("")?strTrackNum:"<a class=RightText href=javascript:fnOpenTrack('"+strTrckNoSpaces+"','"+strShipCarr+"');>"+strTrackNum+"</a>";
					strShipToDate = GmCommonClass.parseNull((String)hmLoop.get("SHIP_DATE"));
					strPO = GmCommonClass.parseNull((String)hmLoop.get("PO"));
					strInvId = GmCommonClass.parseNull((String)hmLoop.get("INVID"));
					strOrderType = GmCommonClass.parseNull((String)hmLoop.get("ORDERTYPE"));
					strQuoteId = GmCommonClass.parseNull((String)hmLoop.get("QUOTEID"));
					strShipDt = GmCommonClass.getStringFromDate((Date)hmLoop.get("SDATE"),strApplnDateFmt);
					strInvFl = GmCommonClass.parseNull((String)hmLoop.get("INVFL"));
					strOrderTypeDesc = GmCommonClass.parseNull((String)hmLoop.get("ORDERTYPEDESC"));
					strAccName = GmCommonClass.parseNull((String)hmLoop.get("ANAME"));
					strOrdSumId = strParentOId.equals("")?strOrdId:strParentOId;
					strHoldFl = GmCommonClass.parseNull((String)hmLoop.get("HOLDFL"));
					strEmailReqFl = GmCommonClass.parseNull((String)hmLoop.get("EMAILREQ"));
					strEmailVersion = GmCommonClass.parseNull((String)hmLoop.get("EVERSION"));
					strPDFFileID = GmCommonClass.parseNull((String)hmLoop.get("PDFFILEID"));
					strCSVFileID = GmCommonClass.parseNull((String)hmLoop.get("CSVFILEID"));
					strInvoicedYear = GmCommonClass.parseNull((String)hmLoop.get("INVYEAR"));
					strRuleCurr = GmCommonClass.parseNull((String)hmLoop.get("OVRIDE_CURR"));
					strDoFlag = GmCommonClass.parseNull((String)hmLoop.get("DO_FLAG"));
					strCurrSign = strRuleCurr.equals("")? strCurrSign: strRuleCurr;
					strDisplayImage = "";
					strShade = (i%2 != 0)?"class=oddshade":""; //For alternate Shading of rows	
					strRAStatusFL =GmCommonClass.parseNull((String)hmLoop.get("PARTSWAP_RA_STATUS"));
					intExclCmpnyFl = strOrdId.indexOf(strExclCmpny);
					// Remove the company id append with the order id
					if(intExclCmpnyFl != -1){
					  strTempOrderId = strOrdId.replace("-"+strExclCmpny, "");
					}else{
					  strTempOrderId = strOrdId;
					}

%>
					<tr id="tr<%=i%>" <%=strShade%> onClick="changeTRBgColor(<%=i%>,'#AACCE8');">
						<td nowrap height="20" class="RightText">
						
						<%
						if(strDoFlag.equals("Y")){
						%>
						<img id="imgHold" style="cursor:hand" src="<%=strImagePath%>/d_upload.gif" title="${varDoOnHold}" onclick="fnViewDODocument('<%=strOrdId%>');" > 
						
						<%
					}
						%>						
    
<%
					if (strHoldFl.equals("Y")){
%>						
                        <fmtMdfOrder:message key="LBL_DOONHOLD" var="varDoOnHold"/>
						<img id="imgHold" style="cursor:hand" src="<%=strImagePath%>/hold-icon.png" title="${varDoOnHold}" >
<%
					}
%>						
                         <fmtMdfOrder:message key="LBL_COMMENTS" var="varComments"/>
						<img id="imgEdit" style="cursor:hand"  
									<% if (strOrdCallFlag.equals("N"))
	 								{ %>
		 							src="<%=strImagePath%>/phone_icon.jpg" 
		 							<%} else {%>	
		 							src="<%=strImagePath%>/phone-icon_ans.gif" 
			 						<% }%>
			 						title="${varComments}" 
			 						width="20" height="17" onClick="javascript:fnOpenOrdLog('<%=strOrdId%>' )"/> 
			 						<fmtMdfOrder:message key="LBL_PICKSLIP" var="varPickSlip"/>
			 						<fmtMdfOrder:message key="LBL_ORDERSUMMARY" var="varOrderSummary"/>
                        <a href="javascript:fnPrintPick('<%=strOrdId%>');"><img src="<%=strImagePath%>/packslip.gif" title="${varPickSlip}"  border=0></a>
						<a href="javascript:fnViewOrder('<%=strOrdSumId%>');"><img src="<%=strImagePath%>/ordsum.gif" title="${varOrderSummary}"  border=0></a><input type="radio" value="<%=strOrdId%>|<%=strInvId%>|<%=strOrdSumId%>" id="<%=strFormattedOrderId%>"
								name="rad">
<%						// Check if the Orded is a Return or Duplicate Order -- redirect based on the type 						
						if (((strTempOrderId.charAt(strTempOrderId.length()-1)== 'R') && (strTempOrderId.charAt(strTempOrderId.length() - 2) != 'F')) ||((strTempOrderId.charAt(strTempOrderId.length()-1)== 'D') && (strTempOrderId.charAt(strTempOrderId.length() - 2) != 'N')) )
						{%>
						   <a href="javascript:fnPrintCreditMemo('<%=strOrdId%>');" class="RightText"><%=strOrdId%></a>
<%
                        } else if (strTempOrderId.charAt(strTempOrderId.length()-1)== 'A' && ((strTempOrderId.charAt(strTempOrderId.length() - 2) != 'Z') || (strTempOrderId.charAt(strTempOrderId.length() - 2) != 'B'))) {
%>
						   <a href="javascript:fnPrintCashAdjustMemo('<%=strOrdId%>');" class="RightText"><%=strOrdId%></a>
<%
                        } else {
%>                         <fmtMdfOrder:message key="LBL_PACKINGSLIP" var="varPackingSlip"/>
                           <a href="javascript:fnPrintPack('<%=strOrdId%>');" title="${varPackingSlip}" class="RightText"><%=strOrdId%></a>
<%
                        }
%>
						
						<input type="hidden" name="GM<%=strFormattedOrderId%>" value="<%=strStatusFl%>">
						<input type="hidden" name="GM<%=strFormattedOrderId%>Type" value="<%=strOrderType%>">
						<input type="hidden" name="GM<%=strFormattedOrderId%>InvFl" value="<%=strInvFl%>">
						<input type="hidden" name="GM<%=strFormattedOrderId%>HoldFl" value="<%=strHoldFl%>">
						<input type="hidden" name="GM<%=strFormattedOrderId%>REFID" value="<%=strRefId%>">
						<input type="hidden" name="GM<%=strFormattedOrderId%>RaStatusFl" value="<%=strRAStatusFL%>">
						<input type="hidden" name="GM<%=strFormattedOrderId%>QuoteId" value="<%=strQuoteId%>">
						<input type="hidden" name="hCurrSign" id="hCurrSign" value="<%=strCurrSign%>">
						</td>
						<td class="RightText" ><%=strAccName%></td>
						<td class="RightText"  align="center"><%=strOrdDt%></td>					
						<gmjsp:currency type="CurrTextSign" textValue="<%=strPrice%>" currSymbol="<%=strCurrSign%>"/>
						<td class="RightText" align="center">&nbsp;<%=strOrderTypeDesc%>&nbsp;</td>
						<td class="RightText" align="left"><%=strOrdBy%></td>
						<td class="RightText" align="center"><%=strShipDt%></td>
						<td class="RightText" align="center"><%=strTrackNum%>&nbsp;</td>
						<td class="RightText" align="center"><%=strPO%>&nbsp;</td>
						<td class="RightText" align="center" nowrap><a class=RightText href= javascript:fnViewOrder('<%=strParentOId%>')><%=strParentOId%></a>&nbsp;</td>
						<td class="RightText" align="center"><a class=RightText href= javascript:fnPrintInvoice('<%=strInvId%>')><%=strInvId%></a></td>
						<td>
						<!-- Code for Invoice Call Log Starts -->
<% 
						if (!strInvId.equals(""))
						{
							if(!strPDFFileID.equals("")){ // check the PDF file is stored 
								strDisplayImage = "<img id='imgPDF' style='cursor:hand;vertical-align:middle;' src='"+strImagePath+"/pdf_icon.gif' onclick=fnExportFile('"+strPDFFileID+"'); title='"+strLblOpnPDF+"'>&nbsp;";
							}
							if(!strCSVFileID.equals("")){ // check the CSV file is stored
								strDisplayImage += "<img id='imgCSV' style='cursor:hand;vertical-align:middle;' src='"+strImagePath+"/csv_icon.gif' onclick=fnExportFile('"+strCSVFileID+"'); title='"+strLblOpnCSV+"'>&nbsp;";
							}
%>                              <fmtMdfOrder:message key="LBL_CALLLOG" var="varCallLog"/>
								<img id="imgEdit" style="cursor:hand"  
									<% if (strInvCallFlag.equals("N"))
	 								{ %>
		 							src="<%=strImagePath%>/phone_icon.jpg" style="cursor:hand;vertical-align:middle;"
		 							<%} else {%>	
		 							src="<%=strImagePath%>/phone-icon_ans.gif" style="cursor:hand;vertical-align:middle;"
			 						<% }%>
			 						title="${varCallLog}" 
			 						width="20" height="17" 
			 						onClick="javascript:fnOpenInvLog('<%=strInvId%>' )"/>&nbsp;<%=strDisplayImage%>
<%
			 			}
%>
			 						</td>
			 			<!-- Code for Invoice Call Log Ends -->
					</tr>
					<tr><td colspan="12" height="1" bgcolor="#CCCCCC"></td></tr>
<%
					} // end of FOR
%>
					</tbody>
				</table>
			</div>
			<BR>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="10" class="RightText" align="Center"><BR><fmtMdfOrder:message key="LBL_CHOOSE_ACTION"/>:&nbsp;<gmjsp:dropdown controlName="Cbo_Action1" seletedValue="" value="<%=alChooseAction%>"
						  codeId="CODEID"  codeName ="CODENM" defaultValue="${varChooseAction}"/>
					
						<fmtMdfOrder:message key="BTN_SUBMIT" var="varSubmit"/>
						<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnCallEdit();" buttonType="Save" />&nbsp;<BR><BR>
						</td>
					</tr>
				</table>
				<div id="npidtls" style="display: none;">&nbsp;
				<jsp:include page="/custservice/GmNPIProcess.jsp" >															 
					<jsp:param name="SCREENTYPE" value='Modify' />
				</jsp:include>
				</div>
				</td>
			</tr>
			<tr><td colspan="11" height="1" bgcolor="#CCCCCC"></td></tr>
<%
		}else{
%>
                       <tr><td colspan="10" height="50" class="RightTextRed" align="center"> <BR><fmtMdfOrder:message key="LBL_NO_ORDERS_FOR_ACCOUNT" /> </td></tr>
				</table>
<%
		}
%>
		<tr><td colspan="11" height="1" bgcolor="#666666"></td></tr>
	</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
