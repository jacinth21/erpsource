 <%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
 <%@ page import ="com.globus.common.beans.GmCalenderOperations"%>  
<%@ taglib prefix="fmtOrderUpdateDDT" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<fmtOrderUpdateDDT:setLocale value="<%=strLocale%>"/>
<fmtOrderUpdateDDT:setBundle basename="properties.labels.custservice.GmOrderUpdateDDT"/>

<bean:define id="hmResult" name="frmOrderDDTForm" property="hmResult" type="java.util.HashMap" />
<bean:define id="alLogReasons" name="frmOrderDDTForm" property="alLogReasons" type="java.util.ArrayList" />
<bean:define id ="accessFL" name="frmOrderDDTForm" property="accessFL" type="java.lang.String"/> 
<%-- \custservice\GmOrderUpdateDDT.jsp --%>
<%

HashMap hmOrderDetails = new HashMap();
ArrayList alCart = new ArrayList();
HashMap hmShipDetails = new HashMap();

GmCalenderOperations gmCal = new GmCalenderOperations();
hmOrderDetails = GmCommonClass.parseNullHashMap((HashMap) hmResult.get("ORDERDETAILS"));
alCart= GmCommonClass.parseNullArrayList((ArrayList) hmResult.get("CARTDETAILS"));
hmShipDetails= GmCommonClass.parseNullHashMap((HashMap) hmResult.get("SHIPDETAILS"));


String strShade = "";
String strcustserviceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");

String strOrderId = "";
String strAccName = "";
String strUserName = "";
String strOrdredDate = "";
String strComments = "";
String strPO = "";
String strAccId = "";
String strInvoiceId = "";
String strNextPartNum ="";
String strPartNum="";
String strTmpPartNum = "";
String strPartDesc="";
String strOrderQty="";
String strUnitPrice="";
String strUnitPriceAdj="";
String strNetUnitPrice="";
String strDDT="";
String strItemOrderUsageId="";
String strItemUsageLot="";

String strShipCost = "";
String strLine = "";
String strItemId = "";
String strInfoId = "";
int intQty = 0;
int intReturnItemQTY = 0;
double dbItemTotal = 0.0;
double dbTotal = 0.0;
String strItemTotal = "";
double dblShip = 0.0;
String strItemTypeDesc = "";

double dbBeforeItemTotal = 0.0;
double dbBeforePriceTotal = 0.0;
double dbAfterItemTotal = 0.0;
double dbAfterPriceTotal = 0.0;
double dbBeforeTotal = 0.0;
double dbAfterTotal = 0.0;
String strAfterPrice = "";
String strBeforeTotal = "";
String strAfterTotal = "";
String strEdtShipChrgFl = "";
double dbAdjTotal = 0.0;
double dbAdjstTotal = 0.0;
String strAdjCodeValue = "";
double dbBeforePrice = 0.0;
double dbAfterPrice = 0.0;
String strAdjustAmount = "";

strEdtShipChrgFl = GmCommonClass.parseNull((String)request.getAttribute("EDITSHIPCHARGE"));
String strCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
String strhAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");

java.sql.Date dtShipDate = null;

int intSize = 0;
int intCount = 0;

strShipCost = GmCommonClass.parseZero((String)hmShipDetails.get("SCOST"));

strOrderId = GmCommonClass.parseNull((String)hmOrderDetails.get("ID"));

strAccName = GmCommonClass.parseNull((String)hmOrderDetails.get("ANAME"));

strUserName = GmCommonClass.parseNull((String)hmOrderDetails.get("UNAME"));

strComments = GmCommonClass.parseNull((String)hmOrderDetails.get("COMMENTS"));

strPO = GmCommonClass.parseNull((String)hmOrderDetails.get("PO"));

strAccId = GmCommonClass.parseNull((String)hmOrderDetails.get("ACCID"));
strOrdredDate = GmCommonClass.getStringFromDate((java.sql.Date)hmOrderDetails.get("ODT"),strGCompDateFmt);

dtShipDate = (java.sql.Date)hmOrderDetails.get("SHIPPING_DT"); 


strInvoiceId =GmCommonClass.parseNull(GmCommonClass.parseNull((String)hmOrderDetails.get("INVOICEID")));

//If InvoiceId came then Submit button will be disabled

if(!strInvoiceId.equals("")){
	accessFL = "N";
}

intSize = alCart.size();

String strAcessFl =accessFL.equals("Y")?"false":"true";

String strWikiTitle = GmCommonClass.getWikiTitle("UPDATE_DDT"); 

%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Delivered Order - Update DDT </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strcustserviceJsPath%>/GmOrderUpdateDDT.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script type="text/javascript">
var intSize = '<%=intSize%>';
var actualSize = '<%=intSize%>';
var ddtOrderId = '<%=strOrderId%>';
var count = 0;
<%	
HashMap hmDDTDetails = new HashMap();
for (int i=0;i< alCart.size();i++)
{
  hmDDTDetails = (HashMap) alCart.get(i);
%>
var ItemQtyArr<%=i%> ="<%=hmDDTDetails.get("PNUM")%>,<%=hmDDTDetails.get("IQTY")%>";
<%
}
%>
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmOrderDDTAction.do">
<html:hidden property="inputStr" name="frmOrderDDTForm" value=""/>
<html:hidden property="orderId" name="frmOrderDDTForm"/>
<html:hidden property="strOpt" name="frmOrderDDTForm"/>
<html:hidden property="accessFL" name="frmOrderDDTForm"/>


	<table border="0" width="900" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
			<td height="25" class="RightDashBoardHeader"><fmtOrderUpdateDDT:message key="LBL_DELIVERED_ORDER"/></td>  
			<td height="25" class="RightDashBoardHeader">
			
			<fmtOrderUpdateDDT:message key="IMG_ALT_HELP" var = "varHelp"/>
			<img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
			</td>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>
		<tr>
			<td width="900" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
					    
						<td class="RightTableCaption" HEIGHT="20" align="right" width="25%">&nbsp;<fmtOrderUpdateDDT:message key="LBL_ORDER_ID"/>:</td>
						<td class="RightText" width="75%">&nbsp;<%=strOrderId%></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr class="Shade">
					    
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtOrderUpdateDDT:message key="LBL_ACC_NAME"/></td>
						<td class="RightText">&nbsp;<%=strAccName%></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr>
					    
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtOrderUpdateDDT:message key="LBL_ACC_ID"/>:</td>
						<td class="RightText">&nbsp;<%=strAccId%></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr class="Shade">
					
					
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtOrderUpdateDDT:message key="LBL_ORDERED_ENTERED"/>:</td>
						<td class="RightText">&nbsp;<%=strUserName%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B><fmtOrderUpdateDDT:message key="LBL_DATE"/>:</B>&nbsp;<%=strOrdredDate %></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr>
					    
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtOrderUpdateDDT:message key="LBL_SHIP_DATE"/>:</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringFromDate(dtShipDate,strGCompDateFmt)%></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>					
					<tr   class="Shade">
					    
						<td class="RightTableCaption" valign="top" align="right" HEIGHT="60"><fmtOrderUpdateDDT:message key="LBL_COMMENTS"/>:</td>
						<td class="RightText" valign="top" >&nbsp;<%=strComments%></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr >
					    
						<td align="right" HEIGHT="24" class="RightTableCaption"><fmtOrderUpdateDDT:message key="LBL_CUSTOMER_PO"/>:</td>
						<td class="RightText">&nbsp;<%=strPO%></td>
					</tr>
					<tr><td colspan="2" height="1" class="LLine"></td></tr>
					<tr class="Shade">
					    
						<td align="right" HEIGHT="24" class="RightTableCaption"><fmtOrderUpdateDDT:message key="LBL_INVOICE_ID"/>:</td>
						<td class="RightText">&nbsp;<%=strInvoiceId%></td>
					</tr>

					<tr>
						<td colspan="3" width="898">
						<div style="overflow: auto; height: auto;">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="ddtCartDetails">
								<tr><td colspan="12" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td width="300" height="25">&nbsp;<fmtOrderUpdateDDT:message key="LBL_PART#"/></td>
									<td width="800"><fmtOrderUpdateDDT:message key="LBL_PART_DESC"/></td>
									<td width="80" align="center"><fmtOrderUpdateDDT:message key="LBL_ORDER_QTY"/></td>
									<td width="80" align="left">&nbsp;&nbsp;<fmtOrderUpdateDDT:message key="LBL_QTY"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td width="500"><fmtOrderUpdateDDT:message key="LBL_LOT"/></td>
									<td width="500"><fmtOrderUpdateDDT:message key="LBL_DDT"/></td>
									<td width="80"><fmtOrderUpdateDDT:message key="LBL_TYPE"/></td>
							<td width="100" align="center"><fmtOrderUpdateDDT:message key="LBL_UNIT_PRICE"/></td>
							<td width="100" align="center"><fmtOrderUpdateDDT:message key="LBL_UNIT_ADJ"/></td>	
							<td width="100" align="center"><fmtOrderUpdateDDT:message key="LBL_ADJ_CODE"/></td>
							<td width="100" align="center"><fmtOrderUpdateDDT:message key="LBL_NET_UNIT_PRICE"/></td>			
												
									<td width="150" align="center"><fmtOrderUpdateDDT:message key="LBL_TOTAL_PRICE"/></td>
					</tr>

								<TR><TD colspan=12 height=1 class=Line></TD></TR>
<%


								HashMap hmLoop = new HashMap();
								HashMap hmTempLoop = new HashMap();
								intCount = 0;
								
								for (int i=0;i<intSize;i++)
								{
									hmLoop = (HashMap)alCart.get(i);
									if (i<intSize-1)
									{
										hmTempLoop = (HashMap)alCart.get(i+1);
										strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("ID"));
									}
									strPartNum = GmCommonClass.parseNull((String)hmLoop.get("ID"));
									strTmpPartNum = GmCommonClass.parseNull((String)hmLoop.get("ID"));
									strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
									strOrderQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
									strItemId = GmCommonClass.parseNull((String)hmLoop.get("ITEMID"));
									strItemTypeDesc	= GmCommonClass.parseNull((String)hmLoop.get("ITEMTYPEDESC"));
									strUnitPrice = GmCommonClass.parseZero((String)(String)hmLoop.get("UNITPRICE"));
									strUnitPriceAdj = GmCommonClass.parseNull((String) (String)hmLoop.get("UNITPRICEADJ"));
									strNetUnitPrice = GmCommonClass.parseZero((String)(String)hmLoop.get("NETUNITPRICE"));
									strAdjCodeValue = GmCommonClass.parseNull((String)(String)hmLoop.get("UNITPRICEADJCODE"));
									strItemOrderUsageId = GmCommonClass.parseNull((String)(String)hmLoop.get("ITEM_ORDER_USAGE_ID"));
									strDDT = GmCommonClass.parseNull((String)(String)hmLoop.get("DDT"));
									strItemUsageLot = GmCommonClass.parseNull((String)(String)hmLoop.get("LOT_NUM"));
									strInfoId = GmCommonClass.parseNull((String)(String)hmLoop.get("ORDER_INFO_ID"));
									strShade = (i%2 != 0)?"class=Shade":"";
									
									if (strPartNum.equals(strNextPartNum))
									{
										intCount++;
										strLine = "<TR><TD colspan=10 height=1 bgcolor=#eeeeee></TD></TR>";
									}

									else
									{
										strLine = "<TR><TD colspan=10 height=1 bgcolor=#eeeeee></TD></TR>";
										
										intCount = 0;
									}

									
									if(strNetUnitPrice.equals("0")){
										strNetUnitPrice = strUnitPrice;
									}
									
									strOrderQty = GmCommonClass.parseZero((String)hmLoop.get("QTY"));
									
									
									
									intQty = Integer.parseInt(strOrderQty);
																							
									dbItemTotal = Double.parseDouble(strNetUnitPrice);
									dbItemTotal = (intQty - intReturnItemQTY) * dbItemTotal; // Multiply by Qty
									strItemTotal = ""+dbItemTotal;
									dbTotal = dbTotal + dbItemTotal;
									
									dbAdjTotal = Double.parseDouble(strUnitPrice);
									dbAdjTotal = (intQty - intReturnItemQTY) * dbAdjTotal; // Multiply by Qty								
									dbAdjstTotal = dbAdjstTotal + dbAdjTotal;
									
									// Calculating the Total Price Before Adjustment
									dbBeforeItemTotal = Double.parseDouble(strUnitPrice);
									dbBeforeItemTotal = intQty * dbBeforeItemTotal; // Multiply by Qty
									dbBeforePriceTotal = dbBeforePriceTotal + dbBeforeItemTotal;
									strBeforeTotal = "" + 	dbBeforePriceTotal;	
									
									// Calculating the Total Price After Adjustment
									dbAfterItemTotal = Double.parseDouble(strUnitPrice);
									dbAfterItemTotal = intQty * dbAfterItemTotal; // Multiply by Qty
									dbAfterPriceTotal = dbAfterPriceTotal + dbAfterItemTotal;
									strAfterPrice = "" + 	dbAfterPriceTotal;
									
									// Caliculating the Adjustment Amount
									dbBeforePrice = Double.parseDouble(strBeforeTotal);
									dbAfterPrice = Double.parseDouble(strAfterPrice);						
									
									out.print(strLine);
	%>
										<tr <%=strShade %>>
										
										<td class="RightText"><span id="SpPart<%=i%>">&nbsp;
										<%if(!strOrderQty.equals("1") && !strPartNum.equals("")) { %>
										<a href="javascript:fnAddRow(<%=i%>,'<%=strTmpPartNum %>', '<%=strItemUsageLot %>', '<%=strItemId %>', <%=strItemOrderUsageId%>, <%=strInfoId%> );" ><IMG id="partInc<%=i+1%>" border=0 src="<%=strImagePath%>/plus.gif" tabindex="5"></a>
										<%}else{ %>
										&nbsp;&nbsp;
										<%} %>
										&nbsp;<%=strPartNum%></span></td>
										<td class="RightText"><span id="SpDes<%=i%>">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></span></td>
										<td class="RightText" align="center"><%=strOrderQty%></td>
										<td class="RightText"  id="Cell<%=i%>"><span id="SpQty<%=i%>">&nbsp;<input type="text" class="InputArea" size="3" name="txt_Qty<%=i %>" id="txt_Qty<%=i %>"
										  value="<%=strOrderQty%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									
										  <input type="hidden" name="hItemId<%=i%>" id="hItemId<%=i%>" value="<%=strItemId%>">
											<input type="hidden" name="hQty<%=i%>" id="hQty<%=i%>" value="<%=strOrderQty%>">
											<input type="hidden" name="hUsageLot<%=i%>" id="hUsageLot<%=i%>" value="<%=strItemUsageLot%>"> 
											<input type="hidden" name="hItemUsageID<%=i%>" id="hItemUsageID<%=i%>" value="<%=strItemOrderUsageId%>">
											<input type="hidden" name="hTmpItemUsageID<%=i%>" id="hTmpItemUsageID<%=i%>" value="<%=strItemOrderUsageId%>">
											<input type="hidden" name="hPnum<%=i%>" id="hPnum<%=i%>" value="<%=strTmpPartNum%>">
											<input type="hidden" name="hInfoId<%=i%>" id="hInfoId<%=i%>" value="<%=strInfoId%>"></span></td>
											 <td class="RightText" align="center" id="hUsageLot<%=i%>"> <%=strItemUsageLot%></td>
										  <!--PMT-55577 - Record DDT # for Parts used in Surgery -- added control num, DDT change to Drop down  -->
										  <td class="RightText" id="DDTCell<%=i%>">
										    <input type="hidden" class="InputArea" name="txt_DDT<%=i%>" id="txt_DDT<%=i%>" value="<%=strDDT%>">
										  <span id="SpDDT<%=i%>">
										  <select name="ddtId<%=i%>" id="ddtId<%=i%>" style="width: 150px;" 
										  onchange="" >
								          </select> </span></td>
										<td class="RightText" align="center"><span id="SpType<%=i%>">&nbsp;<%=strItemTypeDesc%></span></td>
						
										<gmjsp:currency type="CurrTextSign"  textValue="<%=strUnitPrice%>" currSymbol="<%=strCurrSign%>"/>

										<gmjsp:currency type="CurrTextSign"  textValue="<%=strUnitPriceAdj%>" currSymbol="<%=strCurrSign%>"/>
										<td>&nbsp;<%=strAdjCodeValue %></td>		
										
										<td class="RightText" align="center" ><%=GmCommonClass.getStringWithCommas(strNetUnitPrice)%></td>
	
										<gmjsp:currency type="CurrTextSign"  textValue="<%=strItemTotal%>" currSymbol="<%=strCurrSign%>"/>
			</tr>
	<%
								} //End of FOR Loop
								if (!strhAction.equals("EditControl"))
								{
									strShipCost = GmCommonClass.parseZero(strShipCost);
									dblShip = Double.parseDouble(strShipCost);
									strBeforeTotal = strBeforeTotal.equals("") ? "0" : strBeforeTotal;
									strAfterPrice = strAfterPrice.equals("") ? "0" : strAfterPrice;
									dbBeforeTotal = Double.parseDouble(strBeforeTotal);
									dbTotal = dbBeforeTotal ;
									strBeforeTotal = ""+dbTotal;
									
									dbAfterTotal = Double.parseDouble(strAfterPrice);
									dblShip = Double.parseDouble(strShipCost);
									dbAdjstTotal = dbAfterTotal + dblShip;
									strAfterTotal = ""+dbAdjstTotal;								
									
	%>								<tr><td colspan="12" height="1" bgcolor="#eeeeee"></td></tr>
									<tr>
										<td>&nbsp;</td>
										
										<td  height="20" colspan="10" class="RightText">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="Shipping Charges" td="false"/></td>
										<%if(strEdtShipChrgFl.equalsIgnoreCase("Y")){ %>
											<td class="RightText" align="right"><input type="text" size="4" value="<%=GmCommonClass.getStringWithCommas(strShipCost)%>" 
											name="Txt_SCost" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
											onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;</td>
										<%}else{ %>
											<td class="RightText" align="right"><gmjsp:currency type="CurrTextSign"  textValue="<%=strShipCost%>" td="false" currSymbol="<%=strCurrSign%>"/>
											<input type="hidden" name="Txt_SCost" value="<%=GmCommonClass.getStringWithCommas(strShipCost)%>">
											</td>
										<%} %>
									</tr>
									<tr><td colspan="12" height="1" bgcolor="#eeeeee"></td></tr>
									<tr>
									   
										<td colspan="11" height="30" align="right" class="RightTableCaption"><gmjsp:label type="RegularText"  SFLblControlName="Total Before Adj:&nbsp;&nbsp;:" td="false"/>&nbsp;&nbsp;</td>
										<gmjsp:currency type="CurrTextSign"  textValue="<%=strBeforeTotal%>" currSymbol="<%=strCurrSign%>"/>
										<input type="hidden" name="hTotal" value="<%=strBeforeTotal%>">
									</tr>
									
									<tr>
									   
										<td colspan="11" height="30" align="right" class="RightTableCaption"><gmjsp:label type="RegularText"  SFLblControlName="Total After Adj:&nbsp;&nbsp;:" td="false"/>&nbsp;&nbsp;</td>
										<gmjsp:currency type="CurrTextSign"  textValue="<%=strAfterTotal%>" currSymbol="<%=strCurrSign%>"/>
										<input type="hidden" name="hAdjTotal" value="<%=strAfterTotal%>">
									</tr>
	<% }	%>							

							</table></div>
							<input type="hidden" name="hCnt" value="<%=intSize%>">
						</td>
					</tr>
		
				<tr> 
						
	<td colspan="2"> 
						<jsp:include page="/common/GmIncludeLog.jsp" >
						<jsp:param name="FORMNAME" value="frmOrderDDTForm" />
						<jsp:param name="ALNAME" value="alLogReasons" />
						<jsp:param name="LogMode" value="Edit" />
						<jsp:param name="Mandatory" value="yes" />
						<jsp:param name="TabIndex" value="2" />
						</jsp:include>
					</td>
				</tr>
				<tr>
					<td align="center" height="30" id="button" colspan="2">
					    <fmtOrderUpdateDDT:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" name="Btn_Submit" gmClass="button" onClick="fnSubmit()" buttonType="Save" disabled="<%=strAcessFl%>" tabindex="3"/>
					</td>
				<tr>
				<%if(!strInvoiceId.equals("")){ %>
				<tr>
				<td  align="center" colspan="2">
						<font color="red"><B><gmjsp:label type="RegularText"  SFLblControlName="Cannot update the DDT # for this order; Invoice has been generated" td="false"/></B></font>
				</td>
				</tr>
				<%} %>		
								
				</table>
			</td>
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
    </table>

	

</html:form>
 <%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
