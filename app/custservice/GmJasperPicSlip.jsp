<%
/**********************************************************************************
 * File		 		: GmJasperPicSlip.jsp
 * Desc		 		: Pic Slip in Jasper Report
 * Version	 		: 1.0
 * author			: HReddi
************************************************************************************/
%>
<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
 <%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>

<%@ taglib prefix="fmtJasperPicSlip" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- custservice\GmJasperPicSlip.jsp -->

<fmtJasperPicSlip:setLocale value="<%=strLocale%>"/>
<fmtJasperPicSlip:setBundle basename="properties.labels.custservice.GmJasperPicSlip"/>

<%
	String strHtmlJasperRpt = "";
	HashMap hmResult = (HashMap)(request.getAttribute("HASHMAPDATA"));
	ArrayList alShippingDetails = (ArrayList)(request.getAttribute("ARRAYLISTDATA"));
	ArrayList alCommnetsLog = (ArrayList)(request.getAttribute("hmLog"));
	String strBarCodeID = "";
	strBarCodeID = GmCommonClass.parseNull((String)hmResult.get("ID"));
	String strDate = "";//GmCommonClass.parseNull((String)hmResult.get("ODT"));
	String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	String strTodaysDate = GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate"));
	SimpleDateFormat dfDateFmt = new SimpleDateFormat(strApplnDateFmt+" HH:mm:ss");
	Date dtFromDate= null;
	Date dtPrintDate= new Date();
	dtFromDate = GmCommonClass.getStringToDate(strDate,strApplnDateFmt);	
	String strPrintDate = dfDateFmt.format(dtPrintDate);
	String strUname = "";
	String strLogDate = "";
	String strCommnets = "";
	String strTotLog = "";
	HashMap hmCommnets = null;
	int logSize = 0;
	hmCommnets = new HashMap();
	logSize = alCommnetsLog.size();
	for(int i=0;i<alCommnetsLog.size();i++){		
		hmCommnets = (HashMap)alCommnetsLog.get(i);
		strUname = GmCommonClass.parseNull((String)hmCommnets.get("UNAME"));
		strLogDate = GmCommonClass.parseNull((String)hmCommnets.get("DT"));
		strCommnets = GmCommonClass.parseNull((String)hmCommnets.get("COMMENTS"));
		strTotLog = strTotLog + strLogDate + "&nbsp;" + strUname + "&nbsp;&nbsp;&nbsp;" + strCommnets + "<BR>";
	}
	if(alCommnetsLog.size() == 0){
		strTotLog = "                                                               No Data Available";
	}
	strBarCodeID = strBarCodeID + "$50180";	
	hmResult.put("BARCODEID",strBarCodeID);
	hmResult.put("COMMENTS",alCommnetsLog);
	hmResult.put("PRINTDATE",strPrintDate);
	hmResult.put("LOGDETAILS",strTotLog);
	hmResult.put("LOGSIZE",logSize);
%>
<script>
function fnPrint(){
	window.print();
	return false;
}
function hidePrint(){
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";	
}
function showPrint(){
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<table>
		<tr>
			<td align="center" id="button">
				<fmtJasperPicSlip:message key="BTN_PRINT" var="varPrint"/>
				<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" tabindex="2" onClick="return fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				<fmtJasperPicSlip:message key="BTN_CLOSE" var="varClose"/>
				<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" tabindex="3"	onClick="window.close();" buttonType="Load" />				
			</td>
		</tr>		
	 	<tr>	
			<td>
				<% 
				String strJasperName 	= "/GmOrderPickSlipPrint.jasper";
				GmJasperReport gmJasperReport = new GmJasperReport();			
				gmJasperReport.setRequest(request);
				gmJasperReport.setResponse(response);
				gmJasperReport.setJasperReportName(strJasperName);
				gmJasperReport.setHmReportParameters(hmResult);
				gmJasperReport.setReportDataList(alShippingDetails);
				gmJasperReport.setBlDisplayImage(true);
				strHtmlJasperRpt = gmJasperReport.getHtmlReport();				
				%>	
				<%=strHtmlJasperRpt %>			
			</td>
		</tr>   
</table>
<%@ include file="/common/GmFooter.inc"%>
</BODY>