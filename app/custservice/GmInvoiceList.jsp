 <%
/**********************************************************************************
 * File		 		: GmInvoiceList.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="org.apache.commons.beanutils.RowSetDynaClass" %>
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtInvoiceList" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- custservice\GmInvoiceList.jsp -->

<fmtInvoiceList:setLocale value="<%=strLocale%>"/>
<fmtInvoiceList:setBundle basename="properties.labels.custservice.GmInvoiceList"/>

<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strWikiTitle = GmCommonClass.getWikiTitle("MODIFY_INVOICE");
	String strApplDateFmt = strGCompDateFmt;
	String strRptFmt = "{0,date,"+strApplDateFmt+"}";
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	String strARRptByDealer   =GmCommonClass.parseNull(gmResourceBundleBean.getProperty("AR_REPORT_BY_DEALER"));
	HashMap hmCurrency = new HashMap();
	ArrayList alDateType = new ArrayList();
	ArrayList alCompDiv = new ArrayList();//PMT-41835
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strApplCurrFmt = GmCommonClass.parseNull((String)hmCurrency.get("CMPCURRFMT"));
	String strCurrSign = GmCommonClass.parseNull((String)hmCurrency.get("CMPCURRSMB"));
	String strCurrFmt = "{0,number,"+strApplCurrFmt+"}";

	int intSize = 0;
	HashMap hmLoop = new HashMap();
	HashMap hcboVal = null;
	
	java.util.Date dtFrom = null;
	java.util.Date dtTo	= null;

	String strCodeID = "";
	String strDistId = "";
	String strSelected = "";

	String strInvId = "";
	String strInvDt = "";
	double dbInvAmt = 0.00;
	double dbShipAmt = 0.00;
	String strCustPO = "";
	String strAccId = "";
	String strAmtPaid = "";

	String strAction = "";
	String strStatusFl = "";
	String strInvAmt = "";
	String strInvType = "";
	String strShade = "";
	String strCustServiceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
	
	String strParam = (String)request.getAttribute("hAction");
	String strMessage =  GmCommonClass.parseNull((String)request.getAttribute("hMessage"));
	String strInvSource = GmCommonClass.parseNull((String)request.getAttribute("INVSOURCE"));
	String strIdAccount = GmCommonClass.parseNull((String)request.getAttribute("IDACCOUNT"));
	String strParentFl = GmCommonClass.parseNull((String)request.getAttribute("Chk_ParentFl"));
	String strMode = (String)request.getAttribute("hMode");
	String strStatusType = GmCommonClass.parseNull((String)request.getAttribute("Cbo_Type"));
	String strDateType = GmCommonClass.parseNull((String)request.getAttribute("hDateType"));
	//Get selected division id -PMT-41835
	String strDivisionId = GmCommonClass.parseNull((String)request.getAttribute("DIVISIONID"));//PMT-41835
	ArrayList alType = (ArrayList)request.getAttribute("ALTYPE");
	
	dtFrom	= (java.util.Date)request.getAttribute("FROMDATE");
	dtTo	= (java.util.Date)request.getAttribute("TODATE");
	
	//ArrayList alAccount = new ArrayList();
	ArrayList alAccountOrder = new ArrayList();
	RowSetDynaClass rdAccountOrder = null;
	int intLength = 0;

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	alDateType = (ArrayList)request.getAttribute("DATETYPE");
	//alAccount = (ArrayList)hmReturn.get("ACCOUNTLIST");

	//Get Division arraylist values from request - PMT-41835 
	alCompDiv = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCOMPDIVLIST"));
	if (strParam.equals("Reload"))
	{
		strAccId = (String)request.getAttribute("hAccId");
		rdAccountOrder =(RowSetDynaClass) hmReturn.get("ACCOUNTORDERLIST");
		//don't do this, pass rdAccountOrder directly
		alAccountOrder=(ArrayList) rdAccountOrder.getRows();
		intLength = alAccountOrder.size();
		strAction = (String)request.getAttribute("hAction");
		strMode = (String)request.getAttribute("hMode");
	}

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Invoice List </TITLE>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strCustServiceJsPath%>/GmInvoiceList.js"></script> 
<script>
var varSource = '<%=strInvSource%>';
var format = '<%=strApplDateFmt%>';
var lblChooseReport= '<fmtInvoiceList:message key="LBL_CHOOSE_REPORT"/>';
function fnparentFlLoad(){
	if(document.frmAccount.hAction.value == "Reload"){
		document.frmAccount.Cbo_Type.value = '<%=request.getAttribute("Cbo_Type")%>';
	}
	//we implement the auto complete functionality - so, not required to call below function.
	fnDispatchLoad();
	var parentFl = '<%=strParentFl%>';
	if(parentFl == 'false'){
		document.frmAccount.Chk_ParentFl.checked = false;
	}


}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnparentFlLoad()">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmProcessCreditsServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hMode" value="<%=strMode%>">
<input type="hidden" name="hInvId" value="<%=strInvId%>">
<input type="hidden" name="hAccId" value="<%=strAccId%>">
<input type="hidden" name="hLen" value="<%=intLength%>">
<input type="hidden" name="hSource" value="">
<input type="hidden" name="hPO" value="">
<input type="hidden" name="hTxnId" value="">
<input type="hidden" name="hCancelType" value="">
<input type="hidden" name="hCancelReturnVal" value="">
<input type="hidden" name="strOpt" value="">
<input type="hidden" name="hScreen" value="">
	<table border="0" class="DtTable900" cellspacing="0" cellpadding="0">
		<tr>
	<td  colspan="2" height="25" class="RightDashBoardHeader"><fmtInvoiceList:message key="LBL_DASHBOARD_INVOICE_ACCOUNT"/> </td>
	<td align="right" class=RightDashBoardHeader > 	
		<fmtInvoiceList:message key="IMG_ALT_HELP" var = "varHelp"/>
		<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
	</td>
	</tr>
			<tr> <td class="RightTextBlue"><BR>  <B><fmtInvoiceList:message key="LBL_CHOOSE_ACCOUNT_VALUE_REPORT"/> </B>
			</td>	</tr>
	<%if( strMessage!=null &&!strMessage.equals("")){%>	    
	<tr>
			<td class="RightTextBlue" align = "center" colspan="2">&nbsp;<%=strMessage%><BR>
			</td>
		</tr>
		<%} %>
	
	<tr>
			<td  align="left" height="25">&nbsp;
				<jsp:include page="/accounts/GmIncludeAccountType.jsp" >
					<jsp:param name="DISPACCFL" value="Y"/>
					<jsp:param name="REPACCTLABELFL" value="Y"/> 
				</jsp:include>
				</td>	
 <td><BR><fmtInvoiceList:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" buttonType="Load" onClick="fnGo();" />&nbsp;
			</td>

		</tr>
		<tr>			
			<td class="RightTablecaption" colspan="1" align="right" width="680px"><fmtInvoiceList:message key="LBL_SHOW_PARENT_ACCOUNT"/>:
			<input type="checkbox" size="30" name="Chk_ParentFl" checked="<%=strParentFl %>" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" >					
			
			</td>
		</tr>
		<tr><td colspan="4" class=line></td><td colspan="2" class=line></td></tr>
				<tr> <td colspan="6" >
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr colspan="2" class="Shade" height="24">
					
					<td class="RightTablecaption" align="Right">
						<fmtInvoiceList:message key="LBL_STATUS_TYPE"/>: </td>
					<td colspan="1" class="RightTablecaption" align="left">&nbsp;
					<select name="Cbo_Type" class="RightText" tabindex="1" value="2">
							<option value="0">[All] <option value="1" selected><fmtInvoiceList:message key="LBL_PENDING"/>
							 <option value="2"><fmtInvoiceList:message key="LBL_CLOSED"/> 
					</select> </td>
					  <!-- PMT-41835 -->
					<td class="RightTableCaption" align="right"><fmtInvoiceList:message key="LBL_DIVISION"/>:</td>
					<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Division"  seletedValue="<%=strDivisionId%>" value="<%=alCompDiv%>" 
					codeId = "DIVISION_ID" codeName = "DIVISION_NAME" defaultValue= "[Choose One]" /></td> <!-- PMT-41835 -->
					<td class="RightTablecaption" align="Right" colspan="1"><fmtInvoiceList:message key="LBL_CUSTOMER_PO"/>:</td>
					<td class="RightTablecaption" >&nbsp;<input type="text" size="20" 
						value="<%=GmCommonClass.parseNull((String)request.getAttribute("CUSTPO")) %>" name="Txt_PO" class="InputArea"   
					tabindex="2"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"></td>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
				<tr Height="24" >
					
					<td class="RightTablecaption" align="Right"><fmtInvoiceList:message key="LBL_INVOICE_ID"/>:</td>
					<td align="left">&nbsp;<input type="text" size="20" 
						value="<%=GmCommonClass.parseNull((String)request.getAttribute("INVOICEID")) %>" name="Txt_InvoiceID" class="InputArea"   
					tabindex="3" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"></td> 
					<td class="RightTablecaption" align="Right"><fmtInvoiceList:message key="LBL_ORDER_ID"/>:</td>
					<td colspan="1">&nbsp;<input type="text" size="20" 
						value="<%=GmCommonClass.parseNull((String)request.getAttribute("ORDERID")) %>" name="Txt_OrderID" class="InputArea"   
					tabindex="4" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"></td> 
				</tr> 
				<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
				<tr Height="25" class="Shade">
				<fmtInvoiceList:message key="LBL_DATE_TYPE" var="varDateType"/>
	           <td class="RightTablecaption" align="Right"><gmjsp:label type="RegularText"  SFLblControlName="${varDateType}" td="false"/>:&nbsp;</td><td class="RightTablecaption" align="left"><gmjsp:dropdown controlName="Cbo_DateType"  seletedValue="<%=strDateType%>"
					 value="<%=alDateType%>" codeId = "CODEID" codeName = "CODENM" />&nbsp;&nbsp;
				<fmtInvoiceList:message key="LBL_FROM_DATE"/>:
				&nbsp;<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=(dtFrom==null)?null:new java.sql.Date(dtFrom.getTime())%>"  
					gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="5"/> </td>
					<td class="RightTablecaption" align="Right"><fmtInvoiceList:message key="LBL_TO_DATE"/>:</td>
					<td>&nbsp;<gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=(dtTo==null)?null:new java.sql.Date(dtTo.getTime())%>"  
					       gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="6"/> </td>
					
			 	<td colspan="2">&nbsp;</td> 
				</tr>
							<tr><td colspan="8" class="Line"></td></tr>		
			<tr height="25">
						<td colspan = "1" align = "center" class="RightTextRed"><fmtInvoiceList:message key="LBL_CREDIT_RED"/></td>
						<td colspan = "2" align = "center" class="RightTextBlue"><fmtInvoiceList:message key="LBL_DEBIT_BLUE"/></td>
						<td colspan = "2" align = "center" class="RightTextGreen"><fmtInvoiceList:message key="LBL_REGULAR_GREEN"/></td>
						<td colspan = "2" align = "center" class="RightTextPurple"><fmtInvoiceList:message key="LBL_OBO_PURPLE"/></td>
					</tr>
				<tr><td colspan="8" class="Line"></td></tr>	
				</table>
				</td></tr>


		<tr>
			<td colspan="3" bgcolor="#666666" height="1"></td>			
		</tr>
    </table>
<%	if (strParam.equals("Reload"))
	{
%>
	<table border="0" width="650" class="DtTable900" cellspacing="0" cellpadding="0">
<%
	if (intLength > 0)
	{
%>
		
<tr>
<td  colspan="4" width="100%">

<display:table name="<%=alAccountOrder%>" export="false"  class="its" varTotals="totals" 
					cellpadding="0" cellspacing="0" decorator="com.globus.accounts.displaytag.DTModifyInvoiceWrapper">
<fmtInvoiceList:message key="DT_INVOICE_ID" var="varInvoiceID"/>
<display:column property="INVID" title="${varInvoiceID}" sortable="true" />
<fmtInvoiceList:message key="DT_INVOICE_DATE" var="varInvoiceDate"/>
<display:column property="INVDT" title="${varInvoiceDate}" sortable="true" format= "<%=strRptFmt%>"/>
<fmtInvoiceList:message key="DT_CUSTOMER_PO" var="varCustomerPO"/>
<display:column property="PO" title="${varCustomerPO}" class="alignleft" sortable="true" />
<fmtInvoiceList:message key="DT_CURRENCY" var="varCurrency"/>
<display:column property="CURRENCY" title="${varCurrency}" class="alignleft" sortable="true" />
<fmtInvoiceList:message key="DT_INVOICE_AMT" var="varInvoiceAmt"/>
<display:column property="INVAMT" title="${varInvoiceAmt}" class="alignright" sortable="true" format= "<%=strCurrFmt%>"/> 
<fmtInvoiceList:message key="DT_AMOUNT_RECD" var="varAmountRecd"/>
<display:column property="AMTPAID" title="${varAmountRecd}" class="alignright" sortable="true" format= "<%=strCurrFmt%>"/>
</display:table></td>
</tr>
					<tr>
						<td colspan="5" class="RightText" align="Center"><BR><fmtInvoiceList:message key="LBL_CHOOSE_ACTION"/>&nbsp;<select name="Cbo_Action1" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
                        	<option value="EC"><fmtInvoiceList:message key="LBL_EDIT_COMMENTS"/></option>
							<option value="IC"><fmtInvoiceList:message key="LBL_ISSUE_CREDIT"/></option>
							<option value="IDB"><fmtInvoiceList:message key="LBL_ISSUE_DEBIT"/></option>
							<option value="PP"><fmtInvoiceList:message key="LBL_POST_PAYMENT"/></option>
							<option value="VO"><fmtInvoiceList:message key="LBL_VOID_INVOICE"/></option>
							<%if(!strARRptByDealer.equals("YES")){ %>
							<option value="IU"><fmtInvoiceList:message key="LBL_INVOICE_UPLOAD"/></option>
							<option value="IL"><fmtInvoiceList:message key="LBL_INVOICE_LAYOUT"/></option>
							<%} %>
						</select>
						<fmtInvoiceList:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnCallEdit();"  buttonType="Save"/>&nbsp;<BR><BR>
						</td>
					</tr>
				</table>
<%
	}else{
%>
				<table border="0" width="100%" class="DtTable900" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="100"><fmtInvoiceList:message key="LBL_INVOICE_ID"/></td>
						<td width="100"><fmtInvoiceList:message key="LBL_INVOICE_DATE"/></td>
						<td width="100" align="middle"><fmtInvoiceList:message key="LBL_CUSTOMER_PO"/></td>
						<td width="100" align="center"><fmtInvoiceList:message key="LBL_INVOIVE_AMT"/></td>
						<td width="100" align="middle"><fmtInvoiceList:message key="LBL_AMOUNT_RECD"/></td>
					</tr>
					<tr><td class="Line" colspan="5"></td></tr>					
					<tr><td colspan="5" class="RightTextRed" align="center"> <BR><fmtInvoiceList:message key="LBL_NO_INVOICES_ACCOUNT"/> </td></tr>
				</table>
<%
		}
%>
<!-- 			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>-->	
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
<%
	}
%>
</FORM>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
