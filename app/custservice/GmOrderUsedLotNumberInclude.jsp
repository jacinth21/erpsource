<%
/**********************************************************************************
 * File		 		: GmOrderUsedLotNumberInclude.jsp
 * Desc		 		: This screen is show/update the order usage lot number details
 * Version	 		: 1.0
 * author			: Mani
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@page import="java.util.Date"%>
<%@ taglib prefix="fmtUsedLotNumberInclude" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- custservice\GmOrderUsedLotNumberInclude.jsp -->
<fmtUsedLotNumberInclude:setLocale value="<%=strLocale%>"/>
<fmtUsedLotNumberInclude:setBundle basename="properties.labels.custservice.GmUsedLotNumberInclude"/>
<%
	String strHideHeaderFl = GmCommonClass.parseNull(request.getParameter("HIDE_HEADER"));
	
%>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_form.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_fast.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>

<script>

var usedLotAjaxStr = '';
var gridObj = '';
var responStr = '';
var firstTimeFl = false;
// header cell id
var partNum_cellId = '';
var partDesc_cellId = '';
var usageQty_cellId = '';
var returnQty_cellId = '';
var usageLot_cellId = '';
var ddtId_cellId = '';
var orderId_cellId = '';
var usedLotStr = '';
//to create the map and validate the used and return qty
var usedLotMap = new Map;
var returnsQtyMap = new Map;
// label values
var lblpartNum = '<fmtUsedLotNumberInclude:message key="LBL_PART" />';
var lblpartDesc = '<fmtUsedLotNumberInclude:message key="LBL_PART_DESCRIPTION" />';
var lblcurrQty = '<fmtUsedLotNumberInclude:message key="LBL_CURRENT_QTY" />';
var lblqty = '<font color="red">*</font><fmtUsedLotNumberInclude:message key="LBL_QTY" />';
var lblLotNum = '<fmtUsedLotNumberInclude:message key="LBL_LOT_NUM" />';
var lblDDTNum = '<fmtUsedLotNumberInclude:message key="LBL_DDT_NUM" />';

</script>

<table cellpadding="0" cellspacing="0" width="100%" border="0" bordercolor="gainsboro">
		<% if(strHideHeaderFl.equals("")){ %>
		<tr class="ShadeRightTableCaption">
						<td height="30" colspan="14">Used Lot Number</td>
		</tr>
		<tr><td colspan="8" height="1" bgcolor="#cccccc"></td></tr>
		<%} %>
			 <tr>
				<td colspan="8">
					<div id="dataGridDiv" height="600px" width="100% !important;"></div>
				</td>
			</tr>
			<tr height="25" id="DivNothingMessage">
			  <td colspan="8" align="center"><div>
			  <fmtUsedLotNumberInclude:message key="LBL_NO_DATA" /></div></td>
			</tr>
			<tr><td colspan="8" height="1" bgcolor="#cccccc"></td></tr>
			
</table>
			