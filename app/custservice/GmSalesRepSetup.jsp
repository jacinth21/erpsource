<%@page import="com.globus.common.beans.GmResourceBundleBean"%>

<%
/**********************************************************************************
 * File		 		: GmSalesRepSetup.jsp
 * Desc		 		: This screen is used for the Sales Rep
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtSalesRepSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- custservice\GmSalesRepSetup.jsp -->
<fmtSalesRepSetup:setLocale value="<%=strLocale%>"/>
<fmtSalesRepSetup:setBundle basename="properties.labels.custservice.GmSalesRepSetup"/>

<%
String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
/**
 * PMT-28307 Access Driven Sales Rep Setup 
*/
String strSalesRepAddEdit = GmCommonClass.parseNull((String)request.getAttribute("SALES_REP_ADD_EDIT"));
String strDisabled="";
strDisabled = strSalesRepAddEdit.equals("Y") ? "false" : "true"; 
 
GmResourceBundleBean gmResourceBundleBean =
GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
String strShowRepNmEn = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHOW_REP_NAME_EN"));

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strWikiTitle = GmCommonClass.getWikiTitle("SALES_REP");
	
	String strAction = (String)request.getAttribute("hAction");
	if (strAction == null)
	{
		strAction = (String)session.getAttribute("hAction");
	}
	strAction = (strAction == null)?"Add":strAction;
	
	String strAccessFl = GmCommonClass.parseNull((String)request.getAttribute("ACCESSFL"));
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmLoad = new HashMap();

	String strSelected = "";
	String strCodeID = "";
	ArrayList alDistributor = new ArrayList();
	ArrayList alCategory = new ArrayList();
	ArrayList alReps = new ArrayList();
	ArrayList alTerritory = new ArrayList();
	ArrayList alDsgn =new ArrayList();
	String strRepId 	="";// (String)request.getAttribute("hRepId") == null?"":(String)request.getAttribute("hRepId");
	
	String strPartyId  = GmCommonClass.parseNull((String)request.getParameter("hPartyId")) ;	
	String strFirstNm 	= "";
	String strLastNm 	= ""; 
	String strRepNameEn = "";
	String strRepName   = "";
	String strDistId 	= "";
	String strCatId 	= "";
	String strTerrId 	= "";
	 String strTerrName 	= "";
	String strChecked 	= "checked";
	String strActiveFl = "N";
	java.sql.Date dtStartDt=null;
	java.sql.Date dtEndDt=null;
	String strDsgn ="70116";
	String strReadOnly = "";
	String strUserLoginName = "";
	String strLoginChecked = "";
	String strQuotaCnt = "";
	String strDistName = "";
	
	String strXmlGridData = (String)request.getAttribute("GRIDDATA");
	HashMap hmSalesRep = new HashMap();

	if (!hmReturn.isEmpty())
	{
		hmLoad = (HashMap)hmReturn.get("REPLOAD");
		alDistributor = (ArrayList)hmLoad.get("DISTRIBUTORLIST");
		alCategory = (ArrayList)hmLoad.get("CATEGORY");
		alReps = (ArrayList)hmReturn.get("REPLIST");
		alTerritory = (ArrayList)hmLoad.get("TERRITORY");
		alDsgn = (ArrayList)hmLoad.get("DSGN");
	}
	if (strAction.equals("EditLoad"))
	{
		hmSalesRep = (HashMap)hmReturn.get("EDITLOAD");
		if (!hmSalesRep.isEmpty())
		{
			strRepId	 = (String)hmSalesRep.get("ID");
			strFirstNm	 = (String)hmSalesRep.get("FNAME");
			strLastNm	 = (String)hmSalesRep.get("LNAME");	
			strRepName   = strFirstNm+" "+strLastNm;
			strRepNameEn = (String)hmSalesRep.get("REPNAMEEN");		
		    strDistId 	= (String)hmSalesRep.get("DID");
		    strDistName	= (String)hmSalesRep.get("DNAME");
			strCatId 	= GmCommonClass.parseNull((String)hmSalesRep.get("CAT"));
			strTerrId 	= GmCommonClass.parseNull((String)hmSalesRep.get("TERR_ID"));
			strTerrName = GmCommonClass.parseNull((String)hmSalesRep.get("TERR_NAME"));
			strActiveFl = (String)hmSalesRep.get("AFLAG");
			strChecked 	= strActiveFl.equals("Y")?"checked":"";
			strAction 	= "Edit";
			dtStartDt=(java.sql.Date)hmSalesRep.get("STARTDT");
			dtEndDt=(java.sql.Date)hmSalesRep.get("ENDDT");
			strPartyId =  GmCommonClass.parseNull((String)hmSalesRep.get("PARTYID"));
			strDsgn =  GmCommonClass.parseNull((String)hmSalesRep.get("DSGN"));
			strQuotaCnt =  GmCommonClass.parseZero((String)hmSalesRep.get("QUOTACNT"));
			strUserLoginName =  GmCommonClass.parseNull((String)hmSalesRep.get("USERLOGINNAME"));
			if(!strUserLoginName.equals("")){
				strLoginChecked = "checked";
			}
			if(strCatId.equals("40028")){
				strReadOnly = "readonly";
			}	
		}	
	}
	int intSize = 0;
	HashMap hcboVal = null;
	String strApplDateFmt = strGCompDateFmt;
	// to get the company lang id and based on that - show correct name
	// 103097 - English
	String strCompLangId = gmDataStoreVO.getCmplangid();
	String strUpdatedRepName = !strCompLangId.equals("103097")? strRepName : strRepNameEn;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Sales Rep Setup </TITLE>

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Sales<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/party/GmParty.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>

<script language="JavaScript">
var lblFirstName='<fmtSalesRepSetup:message key="LBL_FIRST_NAME"/>';
var lblLastName='<fmtSalesRepSetup:message key="LBL_LAST_NAME"/>';
var lblRepNameEn='<fmtSalesRepSetup:message key="LBL_REP_NAME_EN"/>';
var lblCategory='<fmtSalesRepSetup:message key="LBL_CATEGORY"/>';
var lblStartDate='<fmtSalesRepSetup:message key="LBL_START_DATE"/>';
var lblSalesRep='<fmtSalesRepSetup:message key="LBL_REP_ID"/>';
var lblUserName='<fmtSalesRepSetup:message key="LBL_USER_NAME"/>';
var lblDesignation='<fmtSalesRepSetup:message key="LBL_DESIGNATION"/>';
var format;
format='<%=strApplDateFmt%>';
var strLoginChecked = '<%=strLoginChecked%>';
var strAccess = '<%=strAccessFl%>';
var objGridData;
objGridData = '<%=strXmlGridData%>';
/**
 * PMT-28307 Access Driven Sales Rep Setup 
*/
var strRepEditAccess ="<%=strSalesRepAddEdit%>";
</script>
<script language="JavaScript" src="<%=strJsPath%>/customerservice/GmSalesRepSetup.js"></script>
<style> 
#dataGridDiv{
    width: 99.75%!important;
}
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnEnableDesg();fnDisableCategory();fnOnLoad();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmSalesRepServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hDistId" value="<%=strDistId%>">
<input type="hidden" name="hDistNm" value="">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hPartyId"   value="<%=strPartyId%>" >
<input type="hidden" name="hQuotaCnt"   value="<%=strQuotaCnt%>" >
<input type="hidden" name="hRepDivInputStr" value = "">
<input type="hidden" name="hPrimaryDivID" value = "">
<input type="hidden" name="hOld_SalesNm" value ="<%=strRepName%>">
<input type="hidden" name="hOld_ActiveFl" value ="<%=strActiveFl%>">
<input type="hidden" name="hOld_Category" value ="<%=strCatId%>">
	<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" class="RightDashBoardHeader">&nbsp;<fmtSalesRepSetup:message key="LBL_SALES_REP"/></td>
						<td  height="25" class="RightDashBoardHeader" align="right">
						<fmtSalesRepSetup:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		       			</td>
					</tr>
					<tr><td class="Line" height="1" colspan="2"></td></tr>
					<tr height="25">
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					     <fmtSalesRepSetup:message key="LBL_SALES_REP_LIST" var = "varSalesRepList" />
						<gmjsp:label type="BoldText"  SFLblControlName="${varSalesRepList}:" td="true"/>
						<td style="padding-left: 4px;"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="Cbo_RepId" />
					<jsp:param name="METHOD_LOAD" value="loadSalesRepList" />
					<jsp:param name="WIDTH" value="400" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="CONTROL_NM_VALUE" value="<%=strUpdatedRepName %>" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="<%=strRepId %>" />
					<jsp:param name="SHOW_DATA" value="10" />
					<jsp:param name="AUTO_RELOAD" value="fnLoadRep();" />
			     	</jsp:include>
						</td>
					</tr>
			
					<tr>
						<td class="RightTextBlue" colspan="2" align="center">
							<fmtSalesRepSetup:message key="LBL_CHOOSE_NEW_REP"/>
						</td>
					</tr>					
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="oddshade">
					<fmtSalesRepSetup:message key="LBL_REP_ID" var="varRepID"/>	
						<gmjsp:label type="BoldText"  SFLblControlName="${varRepID}:" td="true"/>
						<td>&nbsp;<input type="text" size="8" value="<%=strRepId%>" name="Txt_RepId" readonly ></td>
					</tr>					
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="evenshade">
						<fmtSalesRepSetup:message key="LBL_FIRST_NAME" var="varFirstName"/><gmjsp:label type="MandatoryText"  SFLblControlName="${varFirstName}:" td="true"/>
						<td >&nbsp;<input type="text" size="35" <%=strReadOnly%> value="<%=strFirstNm%>" name="Txt_FirstNm"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" ></td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="oddshade">
						<fmtSalesRepSetup:message key="LBL_LAST_NAME" var="varLastName"/><gmjsp:label type="MandatoryText"  SFLblControlName="${varLastName}:" td="true"/>
						<td >&nbsp;<input type="text" size="35" <%=strReadOnly%> value="<%=strLastNm%>" name="Txt_LastNm"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" ></td>
					</tr>	
					
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<% 
						if (strShowRepNmEn.equals("YES")) {
					%> 
					<tr class="oddshade">
						<fmtSalesRepSetup:message key="LBL_REP_NAME_EN" var="varRepNameEn"/>
						<gmjsp:label type="MandatoryText"  SFLblControlName="${varRepNameEn}:" td="true"/> 
						<td>&nbsp;<input type="text" size="50" value="<%=strRepNameEn%>" name="Txt_RepNm_En" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" TabIndex="7" maxlength="70"></td> 
					</tr>		
					<%
						}
 					%> 
					<tr>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					    <fmtSalesRepSetup:message key="LBL_FIELD_SALES_NAME" var="varFieldSalesName"/>
						<gmjsp:label type="MandatoryText" SFLblControlName="${varFieldSalesName}:" td="true"/>
						<td style="padding-left: 1px;"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="Cbo_DistId" />
					<jsp:param name="METHOD_LOAD" value="loadFieldSalesList" />
					<jsp:param name="WIDTH" value="400" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="CONTROL_NM_VALUE" value="<%=strDistName %>" />
					<jsp:param name="CONTROL_ID_VALUE" value="<%=strDistId %>" />
					<jsp:param name="SHOW_DATA" value="10" />
					<jsp:param name="ON_BLUR" value="fnchkDist();" />
					
			    	</jsp:include>
						</td>
					</tr>				
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="oddshade">
						<fmtSalesRepSetup:message key="LBL_CATEGORY" var="varCategory"/><gmjsp:label type="MandatoryText"  SFLblControlName="${varCategory}:" td="true"/>
						<td >&nbsp;<select name="Cbo_CatId" id="Region" class="RightText"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" onChange="fnEnableDesg();"><option value="0" ><fmtSalesRepSetup:message key="LBL_CHOOSE"/>
<%
			  		intSize = alCategory.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alCategory.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strCatId.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
						</select></td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="evenshade">
						<fmtSalesRepSetup:message key="LBL_TERRITORY_NAME" var="varTerritoryName"/><gmjsp:label type="BoldText"  SFLblControlName="${varTerritoryName}:" td="true"/>
						<td>&nbsp;<select name="Cbo_Terr" class="RightText" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtSalesRepSetup:message key="LBL_CHOOSE_ONE"/>
               <%
			  		if (alTerritory != null)
			  		{
			  			intSize = alTerritory.size();
						hcboVal = new HashMap();
			  			for (int i=0;i<intSize;i++)
			  			{
			  				hcboVal = (HashMap)alTerritory.get(i);
			  				strCodeID = (String)hcboVal.get("TID");
							strSelected = strTerrId.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("TNAME")%></option>
<%			  			}
			  		}
			  		if (!strTerrId.equals(""))
			  		{
%>
						<option selected value="<%=strTerrId%>"><%=strTerrName%></option>
<%					}	%>

						</select></td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="oddshade">
						<fmtSalesRepSetup:message key="LBL_START_DATE" var="varStartDate"/><gmjsp:label type="MandatoryText"  SFLblControlName="${varStartDate}:" td="true"/>
						<td >&nbsp;<gmjsp:calendar textControlName="Txt_StDate" textValue="<%=dtStartDt%>"   onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
						</td>				
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="evenshade">
						<fmtSalesRepSetup:message key="LBL_END_DATE" var="varEndDate"/><gmjsp:label type="BoldText"  SFLblControlName="${varEndDate}:" td="true"/>
						<td >&nbsp;<gmjsp:calendar textControlName="Txt_EndDate" textValue="<%=dtEndDt%>"   onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
						</td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="oddshade">
						<fmtSalesRepSetup:message key="LBL_DESIGNATION" var="varDesignation"/><gmjsp:label type="BoldText"  SFLblControlName="${varDesignation}:" td="true"/>
						<td >&nbsp;<gmjsp:dropdown controlName="Cbo_Dsgn" disabled="<%=strDisabled%>" seletedValue="<%= strDsgn %>" 	
							   value="<%=alDsgn%>" codeId = "CODEID"   codeName = "CODENM"  defaultValue= "[Choose One]" />			
						</td>
					</tr>					
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="evenshade">
						<fmtSalesRepSetup:message key="LBL_ACTIVE" var="varActive"/><gmjsp:label type="BoldText"  SFLblControlName="${varActive}:" td="true"/>
						<td >&nbsp;<input type="checkbox" size="30" <%=strChecked%> name="Chk_ActiveFl"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" ></td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="oddshade">
						<fmtSalesRepSetup:message key="LBL_LINKS" var="varLinks"/><gmjsp:label type="BoldText"  SFLblControlName="${varLinks}:" td="true"/>
						<td >&nbsp;<img style='cursor:hand' onClick="fnOpenAddress();" src='<%=strImagePath%>/Address-Book-icon.png' title='Click to enter addresses' height="26" width="26"></img>&nbsp;&nbsp; <img style='cursor:hand' onClick="fnOpenContact();" src='<%=strImagePath%>/phone.png' title='Click to enter contact info' height="26" width="26"></img>
						</tr>
					<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
					<%if(strAccessFl.equalsIgnoreCase("y")) {%>
					<tr bgcolor="#e4e6f2" class="RightTableCaption"><td colspan="4" height="25">&nbsp;<IMG id="tabRecvimg" border=0 src="<%=strImagePath%>/plus.gif">&nbsp;<a href="javascript:fnShowFilters('tabRecv');" tabindex="-1" class="RightText"><fmtSalesRepSetup:message key="LBL_LOGIN_CREDENTIALS"/></a></td></tr>					
					<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
					<tr>
						<td colspan="6">
							<table border="0" width="100%" cellspacing="0" cellpadding="0" id="tabRecv" style="display:none">
							<tr><td colspan="8" height="1" bgcolor="#cccccc"></td></tr>
							<tr class="Shade">
									<td class="RightTableCaption" align="right" HEIGHT="24" colspan="6"><fmtSalesRepSetup:message key="LBL_CREATE_LOGIN"/>:</td>
									<td colspan="6">&nbsp;<input type="checkbox" size="30" name="Chk_Login_Cr" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" <%=strLoginChecked%>></input></td>
								</tr>
							
								<tr><td colspan="8" height="1" bgcolor="#cccccc"></td></tr>
								<tr >
									<td colspan="6" class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtSalesRepSetup:message key="LBL_USER_NAME"/>:</td>
									<td>&nbsp;<input type="text" size="30"  id="UserName" name="UserName" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');fnClearDiv();" onBlur="changeBgColor(this,'#ffffff');fnLoadUserName();" value="<%=strUserLoginName%>"></td>
									<td width="300">
									<div id="processimg"><IMG border=0 height="20" width="20" src="<%=strImagePath%>/process.gif"></img></div>
									<div id="DivShowUNMYes"><IMG border=0 height="20" width="20" src="<%=strImagePath%>/success.gif"></img></div>
									<div id="DivShowUNMExists"><b><fmtSalesRepSetup:message key="LBL_USER_NAME_EXIST"/></b></div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<%}%>
					<tr>
						<td class="RightTableCaption" align="left" colspan="2" height="25">
							<div id="tool_data" style="display:inline" >
								<table cellpadding="0" cellspacing="0" border="0" width="200" >
									<TR>
										<td>
										<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="Add Row" style="border: none;" onClick="javascript:addRow()"height="14"></a>
										<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="Remove Row" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a>
										</td>										
									</TR>
								</table>
							</div>
						</td>				
				    </tr>
				    <tr>
			           <td colspan="2">
							<div id="dataGridDiv" style="grid" height="150px" width="700px"></div>
					   </td>
					</tr>	
					<tr class="evenshade">
						<td colspan="2"> 
							<jsp:include page="/common/GmIncludeLog.jsp" >
								<jsp:param name="LogType" value="" />
								<jsp:param name="LogMode" value="Edit" />
							</jsp:include>
						</td>
					</tr>
				   <tr>
						<td  height="40" colspan="2" align="center">&nbsp;
							<fmtSalesRepSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button style="width: 5em" value="${varSubmit}" disabled="<%=strDisabled%>" gmClass="button" onClick="fnSubmit();" buttonType="Save" />&nbsp;
							<fmtSalesRepSetup:message key="BTN_RESET" var="varReset"/><gmjsp:button style="width: 5em" value="${varReset}" gmClass="button" onClick="fnReset();" buttonType="Save" />
						</td>
					</tr>
				</table>
			</td>
		</tr>

    </table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
