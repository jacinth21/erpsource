
<%
/**********************************************************************************
 * File		 			: GmCustSvcSearch.jsp
 * Desc		 		: This screen is used for Searching on the following parameters
 *												1. Sales Representative Name
 * 												2.	Sales Representative Number
 *												3. Hospital Name
 *												4. Part Number
 *												5. Part Name		
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>

<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.HashMap"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ taglib prefix="fmtCSSearch" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- custservice\GmCustSvcSearch.jsp -->
<fmtCSSearch:setLocale value="<%=strLocale%>"/>
<fmtCSSearch:setBundle basename="properties.labels.custservice.GmCustSvcSearch"/>
<%
response.setHeader("Cache-Control", "no-cache, post-check=0, pre-check=0"); //HTTP 1.1
response.setHeader("Pragma", "no-cache");  //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server 
%>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmCustSvcSearch", strSessCompanyLocale);
		
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strPartNum	 	= (String)request.getAttribute("hPartNum")==null?"":(String)request.getAttribute("hPartNum");
	String strDesc 			= GmCommonClass.getStringWithTM((String)request.getAttribute("hDesc")==null?"":(String)request.getAttribute("hDesc"));
	String strRepName	= (String)request.getAttribute("hRepName")==null?"":(String)request.getAttribute("hRepName");
	String strRepNum 	= (String)request.getAttribute("hRepNum")==null?"":(String)request.getAttribute("hRepNum");
	String strRepPhNum = (String)request.getAttribute("hRepPhNum")==null?"":(String)request.getAttribute("hRepPhNum");
	String strAccName	 = (String)request.getAttribute("hAccName")==null?"":(String)request.getAttribute("hAccName");
	String strAccId 			= (String)request.getAttribute("hAccId")==null?"":(String)request.getAttribute("hAccId");
	String strAccCity 		= (String)request.getAttribute("hAccCity")==null?"":(String)request.getAttribute("hAccCity");
	String strSearchParam = (String)request.getAttribute("hSearchParam")==null?"":(String)request.getAttribute("hSearchParam");
	String strCount			 = GmCommonClass.parseZero((String)request.getAttribute("hCount")); 
	String strSearchTitle = "";
    String strExcSubComp = (String)request.getAttribute("ExcSubComp")==null?"":(String)request.getAttribute("ExcSubComp");
	log.debug("Count value of the Row inside JSP is " +strSearchParam );
	
	ArrayList alSearchReport = new ArrayList();
	int intCount = 0;
	
	if (hmReturn != null) 
	{
		alSearchReport = (ArrayList)hmReturn.get("SEARCHREPORT");
		 intCount = alSearchReport.size();
	}

	HashMap hmLoop = new HashMap();

	String strId = "";
	String strProjName = "";

	HashMap hmTempLoop = null;
	// strSearchParam = "SalesRep";
	// strSearchParam = "PartNum";
	// strSearchParam = "AccountDetails";
%>
<HTML>
	<HEAD>
		<TITLE>Globus Medical: Search</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
					<style type="text/css" media="all">
				     @import url("<%=strCssPath%>/screen.css");
			     </style>
		<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js" type="text/javascript"></script>
		<script language="JavaScript" src="<%=strJsPath%>/date-picker.js" type="text/javascript"></script>
		<script type="text/javascript">
var vcount = 0;	
var excSubComp = '<%=strExcSubComp%>';
function fnSearchPart()
{
	document.frmCustSvcSearch.hAction.value = "SearchPart";
	fnStartProgress('Y');
	document.frmCustSvcSearch.submit();
}
function fnClose()
{
	if(excSubComp != ''){
	parent.dhxWins.window("w1").close();
	}else{
	window.close();	
	}
}
function fnSearchRep()
{
	document.frmCustSvcSearch.hAction.value = "SearchRep";
	fnStartProgress('Y');
	document.frmCustSvcSearch.submit();
}

function fnSearchAcc()
{
	document.frmCustSvcSearch.hAction.value = "SearchAcc";
	fnStartProgress('Y');
	document.frmCustSvcSearch.submit();
}

function fnSelectAcc(AccId, AccName)
{
self.opener.document.frmPhOrder.Txt_AccId.value=AccId;
self.opener.document.frmPhOrder.Txt_AccName.value=AccName;
self.opener.document.frmPhOrder.Txt_AccId.focus();
//self.opener.document.frmPhOrder.Txt_OrdId.focus();
window.close()
}

function fnSelectRep(obj)
{
/*var SRepName;
 SRepName = document.frmCustSvcSearch.radSRep.value;
 alert("SRepName is " + SRepName); */
self.opener.document.frmPhOrder.Txt_SRepNum.value=obj.id;
self.opener.document.frmPhOrder.Txt_SRepName.value=obj.value;
self.opener.document.frmPhOrder.Txt_SRepNum.focus();
window.close()
}

function fnSelectPart(obj){

vcount = <%=strCount%>;
var objPartCell = eval("self.opener.document.frmPhOrder.Txt_PartNum"+vcount);
var objPartDescCell = eval("self.opener.document.all.Lbl_PartDesc"+vcount);
var objClose = eval("self.opener.document.frmPhOrder.Txt_PartNum"+vcount);

objPartCell.value = obj.id;
objPartDescCell.innerHTML = obj.value;
objPartCell.focus();

/*self.opener.document.frmPhOrder.objPartDescCell.value=obj.value;
self.opener.document.frmPhOrder.objPartCell.value=obj.id;
self.opener.document.frmPhOrder.objPartDescCell.focus();*/
window.close()
}

function fnCallDrill(pnum,id,type)
{
	var frmDate = document.frmCustSvcSearch.Txt_FromDate.value;
	var toDate = document.frmCustSvcSearch.Txt_ToDate.value;
	
	windowOpener("/GmPartNumSearchServlet?hId="+id+"&hAction=Drill&Txt_PartNum="+pnum+"&hType="+type+"&Txt_FromDate="+frmDate+"&Txt_ToDate="+toDate,"Drill","resizable=yes,scrollbars=yes,top=300,left=300,width=520,height=300");
}
</script>
	</HEAD>

	<BODY leftmargin="20" topmargin="10">
		<FORM name="frmCustSvcSearch" method="POST" action="<%=strServletPath%>/GmSearchServlet">
			<input type="hidden" name="hAction" value="">
			<INPUT type="hidden" name="hCount" value="<%=strCount%>">
			<INPUT type="hidden" name="Txt_AccId" value="<%=strAccId%>">
			<INPUT type="hidden" name="hExcSubComp" value="<%=strExcSubComp%>">

			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td rowspan="15" width="1" class="Line"></td>
					<td height="1" bgcolor="#666666"></td>
					<td rowspan="15" width="1" class="Line"></td>
				</tr>
				<tr>
					<td height="25" class="RightDashBoardHeader">
<%			if (strSearchParam.equals("PartNum")) 
					strSearchTitle = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_TITLE_PART"));
				else if (strSearchParam.equals("SalesRep")) 
					strSearchTitle = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_TITLE_SALESREP"));
				else if (strSearchParam.equals("AccountDetails")) 
					strSearchTitle = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_TITLE_ACCOUNT"));
%>
					<%=strSearchTitle%>
					</td>
				</tr>
				<tr>
					<td class="Line" height="1"></td>
				</tr>
			
<% 	
		
		if (strSearchParam.equals("PartNum")) 
				{
				log.debug("Inside Part Num in JSP");
				String strPartTxtVal = "Txt_Param".concat(strCount);
				log.debug("String Value of the Part field is " + strPartTxtVal);
%>			
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td align="right" class="RightText" HEIGHT="24" width="150"><fmtCSSearch:message key="LBL_PART_NUM"/>:</td>
								<td class="RightText">&nbsp;
									<input type="text" size="20" value="<%=strPartNum%>" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="1">
								</td>
								<td width="200" rowspan="3">&nbsp;
									<fmtCSSearch:message key="BTN_LOAD" var="varLoad"/>
									<gmjsp:button value="&nbsp;${varLoad}&nbsp;" name="Btn_Submit" gmClass="button" onClick="javascript:fnSearchPart()" tabindex="3" buttonType="Load" />
								</td>
							</tr>
							<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
							<tr>
								<td align="right" class="RightText" HEIGHT="24" width="150">
										<fmtCSSearch:message key="LBL_PART_DESC"/>:
								</td>
								<td class="RightText" style="padding-left: 7px;">
 										<input type="text" size="30" value="<%=strDesc%>" name="Txt_Desc" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="2">
								</td>
							</tr>
							<tr><td colspan="3" bgcolor="#eeeeee" height="1"></td></tr>
							<tr>
								<td colspan="3">
								 <display:table name="alSearchReport" class = "its" id="currentRowObject"  decorator = "com.globus.displaytag.beans.DTCustSvcSearchWrapper" pagesize="30"> 
<%
	 	 if (strAccId.equals("") || strPartNum.equals("") || (strCount.equals("0") || strCount.equals("undefined"))){
	 	 log.debug("ACC ID inside Search PART JSP is " + strAccId);
  	     log.debug("strPartNum inside Search PART JSP is " + strPartNum);
 	     log.debug("strCount inside Search PART JSP is " + strCount);
 	         	 }
	 else {
%>
								 <display:column property="PARTRADBUTTON" title="" />
<% 
}
%>			
								<fmtCSSearch:message key="DT_PNUM" var="varPnum"/>
								<fmtCSSearch:message key="DT_PDESC" var="varPdesc"/>
								<display:column property="PNUM" title="${varPnum}" sortable="true" />
							  	<display:column property="PDESC" title="${varPdesc}"  />
							    </display:table> 
<%				
		}
%>	
								</td>
							</tr>
							<fmtCSSearch:message key="BTN_CLOSE" var="varClose"/>
							<tr><td colspan="3" align="center"><gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="javascript:fnClose()" buttonType="Load" /></td></tr>					
						</table>
					</td>
				</tr>
				


<%
		if (strSearchParam.equals("SalesRep")) 
			{
			log.debug("Inside Sales Rep in JSP");
%>
<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td align="right" class="RightText" HEIGHT="24" width="150">
									Rep Name :
								</td>
								<td class="RightText">&nbsp;
									<input type="text" size="20" value="<%=strRepName%>" name="Txt_RepName" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="1">
								</td>
								<td width="200" rowspan="3">&nbsp;
									<gmjsp:button value="&nbsp;Go&nbsp;" name="Btn_Submit" gmClass="button" onClick="javascript:fnSearchRep()" tabindex="3" buttonType="Load" />
								</td>
							</tr>
							<tr>
								<td colspan="2" bgcolor="#eeeeee" height="1"></td>
							</tr>

							<tr>
								<td align="right" class="RightText" HEIGHT="24" width="150">
										Rep Number :
								</td>
								<td class="RightText"> &nbsp;
 										<input type="text" size="30" value="<%=strRepNum%>" name="Txt_RepNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="2">
								</td>
							</tr>
							
							<tr>
								<td align="right" class="RightText" HEIGHT="24" width="150">
										Rep Phone Number :
								</td>
								<td class="RightText"> &nbsp;
 										<input type="text" size="30" value="<%=strRepPhNum%>" name="Txt_RepPhNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="2">
								</td>
							<tr>
										 <display:table name="alRepSearchResult"  class = "its" id="currentRowObject"  decorator = "com.globus.displaytag.beans.DTCustSvcSearchWrapper"> 
									 	 <display:column property="REPRADBUTTON" title="" />
										<display:column property="SREPNAME" title="Sales Rep Name" sortable="true" />
									  	<display:column property="SREPID" title="Sales Rep ID"  sortable="true"/>
									  	<display:column property="SREPPHNUM" title="Sales Rep Phone Number"  sortable="true"/>
									    </display:table> 
							</tr>
							</tr>

						</table>
					</td>
				</tr>
				
	
<%	
			}
%>							

<%
		if (strSearchParam.equals("AccountDetails")) 
			{
			log.debug("Inside AccountDetails in JSP");
%>
<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td align="right" class="RightText" HEIGHT="24" width="150">
									Account Name :
								</td>
								<td class="RightText">&nbsp;
									<input type="text" size="20" value="<%=strAccName%>" name="Txt_AccName" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="1">
								</td>
								<td width="200" rowspan="3">&nbsp;
									<gmjsp:button value="&nbsp;Go&nbsp;" name="Btn_Submit" gmClass="button" onClick="javascript:fnSearchAcc()" tabindex="3" buttonType="Save" />
								</td>
							</tr>
							<tr>
								<td colspan="2" bgcolor="#eeeeee" height="1"></td>
							</tr>

							<tr>
								<td align="right" class="RightText" HEIGHT="24" width="150">
										Account  ID :
								</td>
								<td class="RightText"> &nbsp;
 										<input type="text" size="30" value="<%=strAccId%>" name="Txt_AccountId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="2">
								</td>
							</tr>
							
							<tr>
								<td align="right" class="RightText" HEIGHT="24" width="150">
										Account City :
								</td>
								<td class="RightText"> &nbsp;
 										<input type="text" size="30" value="<%=strAccCity%>" name="Txt_AccCity" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="2">
								</td>
							</tr>
							<tr>
									 <display:table name="alAccSearchResult"  class = "its" id="currentRowObject"  decorator = "com.globus.displaytag.beans.DTCustSvcSearchWrapper"> 
									 <display:column property="ACCRADBUTTON" title="" />
									<display:column property="ACCNAME" title="Account Name" sortable="true" />
								  	<display:column property="ACCID" title="Account ID"  sortable="true"/>
								  	<display:column property="ACCCITY" title="Account City"  sortable="true"/>
								    </display:table> 
							</tr>						
						</table>
					</td>
				</tr>
				
	

<%	
			}
%>											
									<tr>
								<td colspan="4" height="1" bgcolor="#666666"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>



		</FORM>
		<%
}
catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
	</BODY>

</HTML>
