

<!--include\GmFromToDateConsign.jsp -->

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtIncDate" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtIncDate:setLocale value="<%=strLocale%>"/>
<fmtIncDate:setBundle basename="properties.labels.include.GmFromToDateConsign"/>
<%
 int intTabFromMonth = 0;
 int intTabToMonth = 0;
 int intTabFromYear = 0;
 int intTabToYear = 0;  
 int intTabGo = 0;  
  
 int tabStartCount = ((request.getParameter("tabStartCount") == null )? -1 : Integer.parseInt((String)request.getParameter("tabStartCount")));
 
 if (tabStartCount != -1)
 {
 	intTabFromMonth = tabStartCount + 1;
 	intTabFromYear = intTabFromMonth + 1;
 	intTabToMonth = intTabFromYear + 1;
 	intTabToYear = intTabToMonth + 1;
 	intTabGo = intTabToYear + 1;
 }
 String strintTabGo = Integer.toString(intTabGo);
 
 HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
 ArrayList alMonthDropDown = new ArrayList();
 ArrayList alYearDropDown = new ArrayList();

 alMonthDropDown = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALMONTHDROPDOWN"));
 alYearDropDown = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALYEARDROPDOWN"));

 String strToMonth = "";
 String strToYear ="";
 String strFrmMonth = "";
 String strFrmYear = "";

 strFrmMonth = GmCommonClass.parseNull((String)request.getAttribute("Cbo_FromMonth"));
 strFrmYear = GmCommonClass.parseNull((String)request.getAttribute("Cbo_FromYear"));
 strToMonth = GmCommonClass.parseNull((String)request.getAttribute("Cbo_ToMonth"));
 strToYear = GmCommonClass.parseNull((String)request.getAttribute("Cbo_ToYear"));
  
%>
<!-- Custom tag lib code modified for JBOSS migration changes -->
<table cellSpacing=0 cellPadding=0  border=0>
<tr height=30>
				<TD class=RightText align="right" >&nbsp;&nbsp;&nbsp;<fmtIncDate:message key="LBL_FROM_DATE"/>:&nbsp;
				</TD>
				<TD class=RightText width=105>
				<gmjsp:dropdown controlName="Cbo_FromMonth" seletedValue="<%=strFrmMonth %>" value="<%=alMonthDropDown%>" 
			codeId = "CODENMALT"  codeName = "CODENM" tabIndex="<%=intTabFromMonth%>"/>
				</TD>
				<TD class=RightText width=75>
				<gmjsp:dropdown controlName="Cbo_FromYear"  seletedValue="<%=strFrmYear%>" value="<%=alYearDropDown%>" 
			codeId = "CODENM"  codeName = "CODENM"  tabIndex="<%= intTabFromYear %>" />
				</TD>	
				<TD class=RightText align="right" >&nbsp;&nbsp;&nbsp;<fmtIncDate:message key="LBL_TO_DATE"/>:&nbsp;
				</TD>		
				<TD class=RightText width=105>
				<gmjsp:dropdown controlName="Cbo_ToMonth"  seletedValue="<%=strToMonth %>" value="<%=alMonthDropDown%>" 
			codeId = "CODENMALT"  codeName = "CODENM"  tabIndex="<%= intTabToMonth %>"  />
				<TD>
				<TD class=RightText width=75>
				<gmjsp:dropdown controlName="Cbo_ToYear"  seletedValue="<%=strToYear%>" value="<%=alYearDropDown%>" 
			codeId = "CODENM"  codeName = "CODENM"  tabIndex="<%= intTabToYear %>" />
				</TD>
				<fmtIncDate:message key="BTN_LOAD" var="varLoad"/>	
				<TD width=100 align="center"> <gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" onClick="javascript:return fnGo();" 
				tabindex="<%=strintTabGo%>" gmClass="button" buttonType="Load" /></TD>
</TR>
</table>
<%@ include file="/common/GmFooter.inc" %>
