<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtIncConsign" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- include\GmIncludeConsignment.jsp -->
<fmtIncConsign:setLocale value="<%=strLocale%>"/>
<fmtIncConsign:setBundle basename="properties.labels.operations.logistics.GmInHouseSetReturnProcessing"/>

<%	
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strSetId=GmCommonClass.parseNull(request.getParameter("strSetID"));
String strConsignId=GmCommonClass.parseNull(request.getParameter("strConsignId"));

%>
<html:html>
<HEAD>
<script type="text/javascript">
var setid ='<%=strSetId%>';	
var consignId ='<%=strConsignId%>';	

</script>
 <link rel="stylesheet" href="<%=strCssPath%>/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=strJsPath%>/common/bootstrap.min.css">
<!-- <link rel="stylesheet" href="<%=strCssPath%>/font-awesome.css"> -->
 <link rel="stylesheet" href="<%=strCssPath%>/GmNPIProcess.css"> 
<!-- <script src="<%=strJsPath%>/common/jquery.min.js"></script> -->
<script src="<%=strJsPath%>/common/bootstrap.min.js"></script> 

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_fast.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_form.js"></script>
<!-- Method allows user to add new row from clipboard. New row is being added to the last position in the grid. -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<!-- paste content of clipboard into block selection of grid -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_markers.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<!-- The library provides support for different keyboard commands that let users to navigate through the grid. -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmLotReprocess.js"></script>
<!-- Custom tag lib code modified for JBOSS migration changes -->
</head>
<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoadConsignDtl();">
<audio id="myAudio">
  <source src="<%=strExtWebPath%>/sound/system_error.mp3" type="audio/mp3">
</audio>

<html:form action="/gmLoanerLotReprocess.do?" >
<input type="hidden" name="hPartNum">
<table border="0" width="100%" cellspacing="0" cellpadding="0">
 <tr>
					<td class="Line" colspan="4"><embed src="sounds/beep.wav"
							autostart="false" width="0" height="0" id="beep"
							enablejavascript="true"></td>
				</tr>
				<tr height="30">
					<td colspan="1" class="RightText" HEIGHT="20" align="right" ><b><fmtIncConsign:message
								key="LBL_SCAN_ENTER_CONTROL" />:</b></td>

					<td colspan="3">&nbsp;<input type="text" name="controlNo"
						id="controlNo" class="InputArea"
						style="text-transform: uppercase;"  size="60"
						onkeypress="javascript:fnAddRow(this);"
						onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" />
					&nbsp; <fmtIncConsign:message key="LBL_ADD"
							var="varAdd" /> <gmjsp:button name="Btn_Cno_Add"
							value="&nbsp;&nbsp;&nbsp;${varAdd}&nbsp;&nbsp;&nbsp;"
							gmClass="btn btn-primary btn-xs btn-add" buttonType="Load"
							onClick="javascript:fnAddRow(this);" />
					&nbsp; <fmtIncConsign:message key="BTN_VIEW"
							var="varView" /> 
							<button  data-toggle="modal" data-target="#myModal" type="button" title="${varTitle}" id="npiBtn" >
							<a href="#"> <span class="btn btn-primary btn-xs btn-add" title='Add more surgeon details'>View Lots</span></a> 
							</button>
							<!--PMT-52000 - add Save button  -->
				    &nbsp; <fmtIncConsign:message key="BTN_SAVE"
							var="varSave" /> <gmjsp:button name="Btn_Save"
							value="&nbsp;${varSave}&nbsp;"
							gmClass="btn btn-primary btn-xs btn-add" buttonType="Save"
							onClick="javascript:fnSave();" />
							</td>
				</tr>
				<tr><td><div id="successCont"></div></td></tr>
				
				<div id="myModal" class="modal fade">
				
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header modal-header-bg">
		        <!--  <label class="main-quest"> &nbsp;Process Check Received Lots:</label> -->
							<button type="button" class="close main-close" data-dismiss="modal" onClick="javascript:fnClose();">&times;</button>
							<label class="RightDashBoardHeader" title="View Parts">Process Check Received Lots:</label>
						</div>
						<div class="modal-body modal-body-grp data-grp">
							<div id="dataInfo" height="300px" width="600px"></div>
						</div>
					</div>
				</div>
			</div>  
			<tr><td colspan="4" height="1" class="Line"></td></tr>
 	    <tr class="shade">
 	   <td colspan="4" class="RightTableCaption" HEIGHT="23" align="left">&nbsp;<fmtIncConsign:message key="LBL_MISSING_REPLENISHMENT"/>&nbsp;:</td>
 	    </tr>
 	    <tr><td colspan="4" height="1" class="Line"></td></tr>
 	    <tr><td colspan="4" height="23"><div id="inhouseTrans" style="color:blue;"> </div></td></tr>
 	    <tr>
			<td colspan="4">
					 <div  id="setConInfo" style="height:650px ; "></div>
			</td>
       </tr> 
</table>
</html:form>
</BODY>
</html:html>
