<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtIncConsign" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- include\GmIncludeConsignment.jsp -->
<fmtIncConsign:setLocale value="<%=strLocale%>"/>
<fmtIncConsign:setBundle basename="properties.labels.operations.logistics.GmInHouseSetReturnProcessing"/>

<%	
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strSetId=GmCommonClass.parseNull(request.getParameter("strSetID"));
String strConsignId=GmCommonClass.parseNull(request.getParameter("strConsignId"));

%>
<html:html>
<HEAD>
<script type="text/javascript">
var setid ='<%=strSetId%>';	
var consignId ='<%=strConsignId%>';	
</script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_fast.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_form.js"></script>
<!-- Method allows user to add new row from clipboard. New row is being added to the last position in the grid. -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<!-- paste content of clipboard into block selection of grid -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_markers.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<!-- The library provides support for different keyboard commands that let users to navigate through the grid. -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmLotReprocess.js"></script>
<!-- Custom tag lib code modified for JBOSS migration changes -->
</head>
<BODY leftmargin="20" topmargin="10">
<html:form action="/gmLoanerLotReprocess.do?" >
<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tr class="shade">
			<td colspan="4" HEIGHT="20" align="left"><b><fmtIncConsign:message
								key="LBL_OPEN_TRANS_ASS_TO" /><%=strConsignId %></b></td>
		</tr>
  <tr>
		<td colspan="4">
					 <div  id="opentransInfo" style="height:650px ; "></div>
			</td>
	</tr> 
	 <tr><td colspan="4" align="center"  class="RegularText"><div  id="DivNothingMessage"><fmtIncConsign:message key="LBL_NO_DATA_FOUND"/></div></td></tr>
</table>
</html:form>
</BODY>
</html:html>
