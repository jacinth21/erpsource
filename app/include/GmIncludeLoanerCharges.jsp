<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import ="org.apache.commons.beanutils.PropertyUtils"%>

<%@ taglib prefix="fmtIncConsign" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- include\GmIncludeConsignment.jsp -->
<fmtIncConsign:setLocale value="<%=strLocale%>"/>
<fmtIncConsign:setBundle basename="properties.labels.operations.logistics.GmInHouseSetReturnProcessing"/>

<%-- <bean:define id="alReturn" name="frmLoanerReqRpt" property="alReturn" type="java.util.ArrayList"></bean:define> --%>
<%	
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strSetId=GmCommonClass.parseNull(request.getParameter("strSetID"));
String strConsignId=GmCommonClass.parseNull(request.getParameter("strConsignId"));
String strArrayListName = GmCommonClass.parseNull(request.getParameter("alList"));
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
		 Object objTemp = pageContext.getRequest().getAttribute(strFormName);
		 ArrayList alLog  = GmCommonClass.parseNullArrayList((ArrayList)PropertyUtils.getProperty(objTemp,strArrayListName)); 
log.debug("alLog=========== "+alLog);
%>
<html:html>
<HEAD>
<script type="text/javascript">
var setid ='<%=strSetId%>';	
var consignId ='<%=strConsignId%>';	
</script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusParty.css">
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmLotReprocess.js"></script>
<!-- Custom tag lib code modified for JBOSS migration changes -->
</head>
<BODY leftmargin="20" topmargin="10">
<html:form action="/gmLoanerLotReprocess.do?" >
<table  width="100%" cellspacing="0" cellpadding="0">
	<tr height="24" class="ShadeRightTableCaption">
		<td align="left" class="RightTableCaption" colspan="2">&nbsp;<fmtIncConsign:message key="LBL_CHARGES"/></td>
	</tr>
	<tr><td colspan="2" class="Line" height="1"></td></tr>
	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<tr height="24">
		<td class="RightTableCaption" align="right" width="30%">&nbsp;&nbsp;<fmtIncConsign:message key="LBL_BIO_HAZARD"/>&nbsp;:&nbsp;</td>
		<td>
			<select id="biohazard"  name="biohazard" class="RightText">
			<option value="0" >[Choose One]</select>
		</td> 
		</tr>
		<tr><td colspan="2" class="Line" height="1"></td></tr>
		<tr height="24" class="shade">
		 <td class="RightTableCaption" align="right" width="30%"><fmtIncConsign:message key="LBL_DAMAGE"/>&nbsp;:&nbsp;</td>
		 <td>
			<select id="damage"  name="damage" class="RightText" >
			<option value="0" >[Choose One]</select>
		</td> 
	</tr>
	<tr><td colspan="2" class="Line" height="1"></td></tr>
	<tr>
	<td class="RightTableCaption" align="right" width="30%">&nbsp;&nbsp;<fmtIncConsign:message key="LBL_ENTER_EXCESS"/>&nbsp;:&nbsp;</td>
	 <td><textarea rows="4" cols="40" name="enterExcess" id="enterExcess" onfocus="changeBgColor(this,'#AACCE8');" 
			onblur="changeBgColor(this,'#ffffff');" class="InputArea"></textarea></td>
			</tr> 
	<tr><td colspan="2" class="Line" height="1"></td></tr>
	
</table>
</html:form>
</html:html>
