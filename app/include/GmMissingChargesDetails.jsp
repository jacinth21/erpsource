 <%
/**********************************************************************************
 * File		 		: GmMissingChargesDetails.jsp
 * Desc		 		: This screen is used for Missing Charges Details
 * Version	 		: 1.1
 * author			: Harinadh Reddi N 
************************************************************************************/
%>

<!--include\GmMissingChargesDetails.jsp -->


<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ taglib prefix="fmtMissingChargesDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmMissingChargesDetails.jsp -->
<fmtMissingChargesDetails:setLocale value="<%=strLocale%>"/>
<fmtMissingChargesDetails:setBundle basename="properties.labels.include.GmMissingChargesDetails"/>
<%
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
strFormName = (strFormName.equals(""))? "frmGmMissingChargesDtl" : strFormName;
String strApplDateFmt = strGCompDateFmt;
String strDtFmt = "{0,date,"+strApplDateFmt+"}";
%>
<bean:define id="ldResult" name="<%=strFormName %>"  property="ldResult" type="java.util.List"> </bean:define>
<bean:define id="screenType" name="<%=strFormName %>"  property="screenType" type="java.lang.String"> </bean:define>
<bean:define id="applnDateFmt" name="<%=strFormName %>" property="applnDateFmt" type="java.lang.String"> </bean:define>
<bean:define id="applnCurrFmt" name="<%=strFormName %>" property="applnCurrFmt" type="java.lang.String"> </bean:define>
<bean:define id="applnCurrSign" name="<%=strFormName %>" property="applnCurrSign" type="java.lang.String"> </bean:define>

<%
	String strChkTicket 		= GmCommonClass.parseNull((String)request.getAttribute("CHK_TICKET"));
	String strChkRepname 		= GmCommonClass.parseNull((String)request.getAttribute("CHK_REPNAME"));
	String strChkListPrice 		= GmCommonClass.parseNull((String)request.getAttribute("CHK_LSTPRICE"));
	String strChkConsignment 	= GmCommonClass.parseNull((String)request.getAttribute("CHK_CONSIGNMENT"));
	String strChkLastUpdBy 		= GmCommonClass.parseNull((String)request.getAttribute("CHK_LSTUPDBY"));
	String strChkLastUpdDt 		= GmCommonClass.parseNull((String)request.getAttribute("CHK_LSTUPDDT"));
	String strSaveBtnAccess 	= GmCommonClass.parseNull((String)request.getAttribute("SAVEUPDFL"));
	String strChargeBtnAccess 	= GmCommonClass.parseNull((String)request.getAttribute("CHARGEUPDFL"));
	String strCreditBtnAccess 	= GmCommonClass.parseNull((String)request.getAttribute("CREDITUPDFL"));
	String strChkAssocRepname 	= GmCommonClass.parseNull((String)request.getAttribute("CHK_ASSOCREPNAME"));
	ArrayList alReportList 		= new ArrayList();
	String strFormListName 		= "requestScope."+strFormName+".ldResult";
	alReportList = GmCommonClass.parseNullArrayList((ArrayList) ldResult);
	String strCurrFmt = "{0,number,"+applnCurrSign+applnCurrFmt+"}";
	String strRequestURI = (strFormName.equals("frmGmMissingChargesDtl"))? "/gmMissingChargesDtlAction.do":"/gmChargesApproval.do";

	int intColmnCnt = (Integer)request.getAttribute("COLUMNCOUNT");
	String strSetNameStyle="text-align:left;width:80;word-wrap:break-word;" ;
	if(intColmnCnt > 5){
		strSetNameStyle="text-align:left;width:50;word-wrap:break-word;" ;
	}
%>
<script>
var url	= '<%=strRequestURI%>';
</script>

<table>
<tr>
            <td colspan="8" align="center" >
                <display:table name="<%=strFormListName%>" export="true" requestURI="<%=strRequestURI%>" decorator="com.globus.operations.logistics.displaytags.DTMissingChargesDetailsWrapper" class="its" id="currentRowObject" >
                <display:setProperty name="export.excel.decorator" value="com.globus.operations.logistics.displaytags.DTMissingChargesDetailsWrapper" />
                <display:setProperty name="export.excel.filename" value="MissingChargesDtls.xls" />
                <display:column property="CHECKBOX" title="<input onClick='javascript:fnSelectAll(this);' type='checkbox' name='selectall' />"  media="html"/>               
                <% if(strChkTicket.equals("on")){ %> 
                	<fmtMissingChargesDetails:message key="DT_TICKET" var="varTicket"/><display:column property="TKT_ID" title="${varTicket}" style="text-align:left;width:110;" media="html" />
                	<fmtMissingChargesDetails:message key="DT_TICKET" var="varTicket"/><display:column property="TKT_IDEX" title="${varTicket}" style="text-align:left;width:110;" media="excel" />
                <%}else{%>
                	<fmtMissingChargesDetails:message key="DT_TICKET" var="varTicket"/><display:column property="TKT_ID" title="${varTicket}" style="text-align:left;width:110;" media="none" />
                <%} %>
				<fmtMissingChargesDetails:message key="DT_DIST_NAME" var="varDistributor"/><display:column property="DIST_NAME" title="${varDistributor}" sortable="true" style="text-align:left;width:80;word-wrap:break-word;"/>
				<% if(strChkRepname.equals("on")){ %>
					<fmtMissingChargesDetails:message key="DT_REP_NAME" var="varRepName"/><display:column property="REP_NAME" title="${varRepName}" sortable="true" style="text-align:left;width:80;word-wrap:break-word;"  />	
				<%}else{ %>	
					<fmtMissingChargesDetails:message key="DT_REP_NAME" var="varRepName"/><display:column property="REP_NAME" title="${varRepName}" sortable="true" style="text-align:left;width:100;" media="none"  />		
				<%} %>
				<% if(strChkAssocRepname.equals("on")){ %>
					<fmtMissingChargesDetails:message key="DT_ASSOC_REP" var="varAssocRep"/><display:column property="ASSOCREPNM" title="${varAssocRep}" sortable="true" style="text-align:left;width:80;word-wrap:break-word;"  />	
				<%}else{ %>	
					<fmtMissingChargesDetails:message key="DT_ASSOC_REP" var="varAssocRep"/><display:column property="ASSOCREPNM" title="${varAssocRep}" sortable="true" style="text-align:left;width:100;" media="none"  />		
				<%} %>	
								
				<fmtMissingChargesDetails:message key="DT_REQUEST" var="varRequest"/><display:column property="REQ_ID" title="${varRequest}" style="text-align:left;width:60;" media="html"/>
				<fmtMissingChargesDetails:message key="DT_REQUEST" var="varRequest"/><display:column property="REQ_IDEX" title="${varRequest}" style="text-align:left;width:50;" media="excel"/>
				<fmtMissingChargesDetails:message key="DT_INCIDENT_DATE" var="varIncidentDate"/><display:column property="INCIDENT_DT" title="${varIncidentDate}" sortable="true" style="text-align:center;width:70;" format="<%=strDtFmt%>"/>
				<%if(strChkConsignment.equals("on")) {%>
					<fmtMissingChargesDetails:message key="DT_CONSIGNMENT" var="varConsignment"/><display:column property="CN_ID" title="${varConsignment}" style="text-align:center;width:100;"  sortable="true" class="alignleft"/>
				<%}else{ %>
					<fmtMissingChargesDetails:message key="DT_CONSIGNMENT" var="varConsignment"/><display:column property="CN_ID" title="${varConsignment}" sortable="true" class="alignleft" media="none"/>
				<%} %>
				<fmtMissingChargesDetails:message key="DT_SET_NAME" var="varSetName"/><display:column property="SET_NM" title="${varSetName}" sortable="true" style="<%=strSetNameStyle%>" />
				<fmtMissingChargesDetails:message key="DT_ETCH_ID" var="varEtchID"/><display:column property="ETCH_ID" title="${varEtchID}" sortable="true" style="text-align:left;width:90;"/>
				<fmtMissingChargesDetails:message key="DT_PART_SET" var="varPartSet"/><display:column property="P_S_NUM" title="${varPartSet}" sortable="true" style="text-align:left;width:75;"/>
				<fmtMissingChargesDetails:message key="DT_PART_DESCRIPTION" var="varPartDescription"/><display:column property="PDESC" escapeXml="true" title="${varPartDescription}" sortable="true" class="alignleft"/>
				<fmtMissingChargesDetails:message key="DT_MISSING_QTY" var="varMissingQty"/><display:column property="MISSING_QTY" title="${varMissingQty}" sortable="true" style="text-align: right;width:30;" />
				<fmtMissingChargesDetails:message key="DT_RECONCILED_QTY" var="varReconciledQty"/><display:column property="RECONCILED_QTY" title="${varReconciledQty}" sortable="true" style="text-align: right;width:30;" />
				<fmtMissingChargesDetails:message key="DT_CHARGE_QTY" var="varChargeQty"/><display:column property="CHARGE_QTY" title="${varChargeQty}" sortable="true" style="text-align: right;width:30;"/>
				<%if(strChkListPrice.equals("on")){ %>
					<fmtMissingChargesDetails:message key="DT_LIST_PRICE" var="varListPrice"/><display:column property="LIST_PRICE" title="${varListPrice}" sortable="true" class="alignright" />
				<%}else{ %>
					<fmtMissingChargesDetails:message key="DT_LIST_PRICE" var="varListPrice"/><display:column property="LIST_PRICE" title="${varListPrice}" sortable="true" style="text-align:right;width:30;" media="none"/>
				<%} %>
				<fmtMissingChargesDetails:message key="DT_CHARGE" var="varCharge"/><display:column property="CHARGE" title="${varCharge}" sortable="true" style="text-align:right;width:30;" media="html"/>
				<fmtMissingChargesDetails:message key="DT_CHARGE" var="varCharge"/><display:column property="CHARGEEX" title="${varCharge}" sortable="true" style="text-align:right;width:30;" media="excel"/>
				<%if(!screenType.equals("LoanerCharges")){%>
				<fmtMissingChargesDetails:message key="DT_DEDUCTION" var="varDeduction"/>				
					<fmtMissingChargesDetails:message key="DT_DATE" var="varDate"/><display:column property="DED_DATE" title="${varDeduction}<BR>${varDate} &nbsp;<img id='Img_Date'  style='cursor:hand' onClick='fnCopyDate();' title='Click to open Calendar'src='/images/nav_calendar.gif' border=0 align='absmiddle' height='16' width='19' /> &nbsp;<div id='divDate' style='position: absolute; z-index: 10;'></div>" style="text-align: left;width:140;" media="html"/>
					<fmtMissingChargesDetails:message key="DT_DEDUCTION_DATE" var="varDeductionDate"/><display:column property="DED_DATE_EXCEL" title="${varDeductionDate}" style="text-align: left;width:120;" media="excel"/>				
					<% if(strChkLastUpdBy.equals("on")){ %>
						<fmtMissingChargesDetails:message key="DT_LAST_UPDATED_BY" var="varLastUpdatedBy"/><display:column property="LAST_UPDATED_BY" title="${varLastUpdatedBy}" sortable="true" class="alignleft"/>
					<%}else{ %>
						<fmtMissingChargesDetails:message key="DT_LAST_UPDATED_BY" var="varLastUpdatedBy"/><display:column property="LAST_UPDATED_BY" title="${varLastUpdatedBy}" sortable="true" class="alignleft" media="none"/>
					<%} %>
					<%if(strChkLastUpdDt.equals("on")){ %>
					<fmtMissingChargesDetails:message key="DT_LAST_UPDATED_DATE" var="varLastUpdatedDate"/>
						<display:column property="LAST_UPDATED_DATE" title="${varLastUpdatedDate}" sortable="true" style="text-align:center;width:60;" format="<%=strDtFmt%>"/>
					<%}else{ %>
					<fmtMissingChargesDetails:message key="DT_LAST_UPDATED_DATE" var="varLastUpdatedDate"/>
						<display:column property="LAST_UPDATED_DATE" title="${varLastUpdatedDate}" sortable="true" style="text-align:center;width:60;" format="<%=strDtFmt%>" media="none"/>
					<%} %>
					<fmtMissingChargesDetails:message key="DT_EDIT_CHRG" var="varEditCharge"/><display:column property="EDIT_CHRG" title="${varEditCharge}" style="text-align:right;width:60;" media="html"/>
					<fmtMissingChargesDetails:message key="DT_EDIT_CHRG" var="varEditCharge"/><display:column property="EDIT_CHRGEX" title="${varEditCharge}" style="text-align:right;width:60;" media="excel"/>
					<fmtMissingChargesDetails:message key="DT_RECONCILE_COMMENTS" var="varReconcileComments"/><display:column property="RECON_COMMENTS" title="${varReconcileComments}" class="alignleft" media="html"/>
					<fmtMissingChargesDetails:message key="DT_RECONCILE_COMMENTS" var="varReconcileComments"/><display:column property="RECON_COMMENT" title="${varReconcileComments}" class="alignleft" media="excel"/>
				<%}%>
				<%if(screenType.equals("LoanerCharges")){ %>
					<fmtMissingChargesDetails:message key="DT_STATUS" var="varStatus"/><display:column property="STATUS_FL" title="${varStatus}" class="alignleft" media="html"/>
					<fmtMissingChargesDetails:message key="DT_STATUS" var="varStatus"/><display:column property="STATUS_FL_EXCEL" title="${varStatus}" class="alignleft" media="excel"/>
					<fmtMissingChargesDetails:message key="DT_CREDIT_AMOUNT" var="varCreditAmount"/><display:column property="CEREDIT_AMOUNT" title="${varCreditAmount}" style="text-align:left;width:60;" media="html" />
					<fmtMissingChargesDetails:message key="DT_CREDIT_DATE" var="varCreditDate"/><display:column property="CREDIT_DATE" title="${varCreditDate}" style="text-align:left;width:140;" media="html" />
					<fmtMissingChargesDetails:message key="DT_CREDIT_DATE" var="varCreditDate"/><display:column property="CREDIT_DATEEX" title="${varCreditDate}" style="text-align:left;width:120;" media="excel" />
					<fmtMissingChargesDetails:message key="DT_RECON_COMMENTS" var="varReconComments"/><display:column property="RECONCI_COMMENTS" title="${varReconComments}" class="alignleft" media="html"/>
					<fmtMissingChargesDetails:message key="DT_RECON_COMMENTS" var="varReconComments"/><display:column property="RECONCI_COMMENTSEX" title="${varReconComments}" class="alignleft" media="excel"/>					
				<%} %>
				<fmtMissingChargesDetails:message key="DT_ADDITIONAL_COMMENTS" var="varAdditionalComments"/><display:column property="RECONCMTCNT" title="${varAdditionalComments}" style="text-align:center;width:40;" media="html"/>
				</display:table> 
		     </td>
        </tr>
      <%if((alReportList.size() > 0)){ %>
        <tr>
        	<td height="25" colspan="8" align="center">
        		<%if(screenType.equals("LoanerCharges")){ %>
        			<%if(strCreditBtnAccess.equals("Y")){ %>
        			<fmtMissingChargesDetails:message key="BTN_SUBMIT" var="varSubmit"/>
        				<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" style="width: 4em;height: 23px" tabindex="14" name="Btn_Credit" gmClass="button" buttonType="Save"  onClick="fnCredit();" />
        			<%} %>
        		<%}else{ %>
        			<%if(strSaveBtnAccess.equals("Y")){ %>
        			<fmtMissingChargesDetails:message key="BTN_SAVE_CHANGES" var="varSaveChanges"/>
        				<gmjsp:button value="&nbsp;${varSaveChanges}&nbsp;" style="width: 8em;height: 23px" tabindex="14" name="Btn_Save" gmClass="button" buttonType="Save" onClick="fnSubmit('Save');" />
        			<%} %>
        			<%if(strChargeBtnAccess.equals("Y")){ %>
        			<fmtMissingChargesDetails:message key="BTN_CHARGE_AND_WRITEOFF" var="varChargeandWriteoff"/>
        				<gmjsp:button value="&nbsp;${varChargeandWriteoff}&nbsp;" style="width: 12em;height: 23px" tabindex="14" name="Btn_Charge" gmClass="button" buttonType="Save" onClick="fnSubmit('Charge');" />
        			<%} %>
        			<%if(strSaveBtnAccess.equals("Y")){ %>
        			<fmtMissingChargesDetails:message key="BTN_LATE_FEE_MAIL" var="varLateFeeMail"/>
        				<gmjsp:button value="&nbsp;${varLateFeeMail}&nbsp;" style="width: 12em;height: 23px" tabindex="14" name="Btn_mail" gmClass="button" buttonType="Save" onClick="fnprocessLateFeeEmail();" />
        			<%} %>
        		<%} %>
        	</td>
        </tr>        	
        <%} %>
        </table>
<%@ include file="/common/GmFooter.inc"%>