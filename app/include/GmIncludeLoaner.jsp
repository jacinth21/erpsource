<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtIncConsign" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- include\GmIncludeConsignment.jsp -->
<fmtIncConsign:setLocale value="<%=strLocale%>"/>
<fmtIncConsign:setBundle basename="properties.labels.include.GmIncludeConsignment"/>

<%	
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strSetId=GmCommonClass.parseNull(request.getParameter("strSetID"));
String strConsignId=GmCommonClass.parseNull(request.getParameter("strConsignId"));
log.debug("strConsignId=include============ "+strConsignId);
String strApplnDateFmt = strGCompDateFmt;

String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);

String strLoanerPrStatus = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("LNSETPRIORITYSTS"));
String strLnDispPriority = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("LNDISPPRIORITY"));
/* Boolean blPrStatus = strLoanerPrStatus.indexOf("|"+loansflcd+"|") != -1? true: false;
Boolean blShowLbl = false;
String strShowTagMsg = "";
//Should show the label, only if it is from below screens
if(strLnDispPriority.indexOf("|"+strParentJsp+"|") != -1){
	blShowLbl = true;
} */
%>
<html:html>
<HEAD>
<script type="text/javascript">
var setid ='<%=strSetId%>';	
var consignId ='<%=strConsignId%>';	
var prevtr = 0;
var val = '<%=strConsignId%>';
</script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_fast.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_form.js"></script>
<!-- Method allows user to add new row from clipboard. New row is being added to the last position in the grid. -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<!-- paste content of clipboard into block selection of grid -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_markers.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<!-- The library provides support for different keyboard commands that let users to navigate through the grid. -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmLotReprocess.js"></script>
<!-- Custom tag lib code modified for JBOSS migration changes -->
</head>
<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnOnLoad();">
<html:form action="/gmLoanerLotReprocess.do?" >
<!-- <input type="hidden" name="loansflcd" value=""> -->
<table border="0" width="100%" cellspacing="0" cellpadding="0">
  <tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr> 
		      <td class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<fmtIncConsign:message key="LBL_CONSIGN_ID"/>:</td>
		      <td colspan="2">&nbsp;<a class="RightText" href="javascript:fnPrintVer('<%=strConsignId%>');"><%=strConsignId%></a></td>
	          <td> <div id="priMsg"></div> <div id="reqId" style="display: none;"></div> </td>	<!--PMT-52000 Add priority column  -->
	</tr>
 <tr class="shade">
		<td class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<fmtIncConsign:message key="LBL_SET_NM"/>:&nbsp;</td>
					<td colspan="3" id="setName" width="30%" align="left"></td> 
	</tr>  
			<tr>
					<td class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<fmtIncConsign:message key="LBL_SET_TYPE"/>:&nbsp;</td>
					<td id="settype" width="30%" align="left"></td> 
					<td class="RightTableCaption" align="right"><fmtIncConsign:message key="LBL_ETCH_ID"/>:&nbsp;</td>
					<td id="Txt_EtchId" width="30%" align="left"></td>
			</tr>
					<tr><td colspan="4" height="1" bgcolor="#eeeeee"></td></tr>
			<tr class="shade">
					<td class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<fmtIncConsign:message key="LBL_STATUS"/>:&nbsp;</td>
					<td id="loansfl" width="30%" align="left"></td> 
					<td class="RightTableCaption" align="right">&nbsp;<fmtIncConsign:message key="LBL_CURR_LOC"/>:&nbsp;</td>
				    <td id="billnm" align="left"></td>
		    </tr>
				    <tr><td colspan="4" height="1" bgcolor="#eeeeee"></td></tr>
		 	<tr>
					<td class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<fmtIncConsign:message key="LBL_LOAN_DATE"/>:&nbsp;</td>
					<td id="ldate"></td>
					<td class="RightTableCaption" align="right">&nbsp;<fmtIncConsign:message key="LBL_EXPC_DATE"/>:&nbsp;</td>
					<td id="edate"></td>
			</tr>
			<tr class="Shade">
					<fmtIncConsign:message key="LBL_ACTUAL_DT_RTN" var="varActDate"/>
					<td class="RightTableCaption" height="23" align="right"
						width="155"><gmjsp:label type="BoldText" SFLblControlName="${varActDate}:" td="false" />&nbsp;</td>
					<td align="left" id="returndt">&nbsp;</td>
					<td colspan="2">&nbsp;</td>
			</tr>
				
	</table>
	</td>
	</tr> 
</table>
</html:form>
</BODY>
</html:html>
