 <%
/**********************************************************************************
 * File		 		: GmFromToDateInc.jsp
 * Desc		 		: This screen is used to display From and the To Date 
 * Version	 		: 1.0
 * author			: Richard
************************************************************************************/
%>

<!--include\GmFromToDateInc.jsp -->

<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtFromToDateInc" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmFromToDateInc.jsp -->
<fmtFromToDateInc:setLocale value="<%=strLocale%>"/>
<fmtFromToDateInc:setBundle basename="properties.labels.include.GmFromToDateInc"/>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<%
	java.util.Date dtFrm 	= (java.util.Date)request.getAttribute("hFrom");
	java.util.Date dtTo 	= (java.util.Date)request.getAttribute("hTo");
	String strCSSClass = GmCommonClass.parseNull((String)request.getParameter("CSSClass")) == "" ? "RightText" : GmCommonClass.parseNull((String)request.getParameter("CSSClass"));

%>

<table cellpadding="0" cellspacing="0" border="0" width="100">
	<tr>
	<!-- Custom tag lib code modified for JBOSS migration changes -->
		<fmtFromToDateInc:message key="LBL_FROM_DATE" var="varFromDate"/>
		<td align="right" class="<%=strCSSClass%>">&nbsp;&nbsp;&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varFromDate}:" td="false"/></td>
		<td class="RightText">&nbsp;<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=(dtFrm==null)?null:new java.sql.Date(dtFrm.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
		</td>
		<fmtFromToDateInc:message key="LBL_TO_DATE" var="varToDate"/>
		<td align="right" class="<%=strCSSClass%>"><gmjsp:label type="RegularText"  SFLblControlName="${varToDate}:" td="false"/></td>
		<td class="RightText">&nbsp;<gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=(dtTo==null)?null:new java.sql.Date(dtTo.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
		</td>
	</tr>					
</table>
<%@ include file="/common/GmFooter.inc" %>
