 <%
/**********************************************************************************
 * File		 		: GmChargesApproval.jsp
 * Desc		 		: This screen is used for Charges Approval
 * Version	 		: 1.1
 * author			: Brinal G
 * 
************************************************************************************/
%>

<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ page import="org.apache.commons.beanutils.DynaBean"%>

<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page buffer="16kb" autoFlush="true" %>

<%@ taglib prefix="fmtIncludeChargesApprovalDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!--GmIncludeChargeApprovalDetails.jsp  -->
<fmtIncludeChargesApprovalDetails:setLocale value="<%=strLocale%>"/>
<fmtIncludeChargesApprovalDetails:setBundle basename="properties.labels.operations.logistics.GmChargesApproval"/>

<bean:define id="statuslist" name="frmChargesApproval" property="alStatustListRpt" type="java.util.List"> </bean:define>
<bean:define id="returnList" name="frmChargesApproval" property="alReturn" type="java.util.ArrayList"> </bean:define>
<bean:define id="deptId" name="frmChargesApproval" property="deptId" type="java.lang.String"> </bean:define>
<bean:define id="chargesFor" name="frmChargesApproval" property="chargesFor" type="java.lang.String"> </bean:define>
<bean:define id="shwDtlFl" name="frmChargesApproval" property="shwDtl" type="java.lang.String"> </bean:define>
<bean:define id="applnDateFmt" name="frmChargesApproval" property="applnDateFmt" type="java.lang.String"> </bean:define>
<bean:define id="applnCurrFmt" name="frmChargesApproval" property="applnCurrFmt" type="java.lang.String"> </bean:define>
<bean:define id="applnCurrSign" name="frmChargesApproval" property="applnCurrSign" type="java.lang.String"> </bean:define>
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("CHARGES_APPROVAL");
	ArrayList alList = new ArrayList();
	alList= GmCommonClass.parseNullArrayList((ArrayList) returnList);
	int rowsize = alList.size();
	String strRptFmt = "{0,date,"+applnDateFmt+"}";
	String strCurrFmt = "{0,number,"+applnCurrSign+applnCurrFmt+"}";
		
%>
<HTML>
<TITLE> Globus Medical: Charges Approval</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="javascript" src="<%=strJsPath%>/operations/GmChargesApproval.js"></script>
<script>
var totalRecs = '<%=rowsize%>';</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnFormLoad();">
<tr>
			<td colspan="8">
				 <display:table name="frmChargesApproval.alReturn" requestURI="/gmChargesApproval.do" decorator="com.globus.operations.displaytag.beans.DTChargesApprovalWrapper" class="its" id="currentRowObject"  export="true"  freezeHeader="true">
				 <display:setProperty name="export.excel.decorator" value="com.globus.operations.displaytag.beans.DTChargesApprovalWrapper" />
				 	<fmtIncludeChargesApprovalDetails:message key="DT_DIST_NAME" var="varDistName"/><display:column property="DISTNAME" title="${varDistName}" sortable="true" />
				  	<fmtIncludeChargesApprovalDetails:message key="LBL_REP_NAME" var="varRepName"/><display:column property="SALESREP" title="${varRepName}" sortable="true" /> 
				  	<fmtIncludeChargesApprovalDetails:message key="LBL_ASSOC_REP" var="varAssocRep"/><display:column property="ASSOCREP" title="${varAssocRep}" sortable="true" /> 
				    <fmtIncludeChargesApprovalDetails:message key="DT_Req_ID" var="varRefID"/><display:column property="CNID" title="${varRefID}" sortable="true" style="width:100" />
				    <fmtIncludeChargesApprovalDetails:message key="DT_SET_NAME" var="varSetName"/><display:column property="SETDESC" title="${varSetName}" sortable="true" />
				    <%if(chargesFor.equals("50891")){ %>
				     <fmtIncludeChargesApprovalDetails:message key="DT_PART_NUMBER" var="varPartNumber"/><display:column property="PARTNUM" title="${varPartNumber}" sortable="true" />
				     <fmtIncludeChargesApprovalDetails:message key="DT_PART_DESCRIPTION" var="varPartDescription"/><display:column property="PARTDESC" escapeXml="true"  title="${varPartDescription}" sortable="true" />
				     <fmtIncludeChargesApprovalDetails:message key="DT_MISSING_QTY" var="varMissingQty"/><display:column property="QTYMISS" title="${varMissingQty}" sortable="true" />
				     <fmtIncludeChargesApprovalDetails:message key="DT_UNIT_COST" var="varUnitCost"/><display:column property="UNITCOST" title="${varUnitCost}" class="alignright" format="<%=strCurrFmt%>" />
				    <%} %>
				    <fmtIncludeChargesApprovalDetails:message key="LBL_INCIDENT_TYPE" var="varIncidentType"/><display:column property="INCIDENTTYPE" title="${varIncidentType}" sortable="true" style="width:80"  media="html"/>
				    <fmtIncludeChargesApprovalDetails:message key="LBL_INCIDENT_TYPE" var="varIncidentType"/><display:column property="INCIDENTTYPEEX" title="${varIncidentType}" sortable="true" style="width:80"  media="excel"/>
				    <%if(shwDtlFl.equals("on")){ 
				    %>
				    
				    <fmtIncludeChargesApprovalDetails:message key="DT_LOANED_DATE" var="varLoanedDate"/><display:column property="LDATE" title="${varLoanedDate}" style="width:80" format="<%=strRptFmt%>"/>
				    <fmtIncludeChargesApprovalDetails:message key="DT_ERDATE" var="varEReturnDate"/><display:column property="ERDATE" title="${varEReturnDate}" style="width:80" format="<%=strRptFmt%>"/>
				    <fmtIncludeChargesApprovalDetails:message key="DT_RDATE" var="varRDate"/><display:column property="RDATE" title="${varRDate}" style="width:80" format="<%=strRptFmt%>"/>
				    <fmtIncludeChargesApprovalDetails:message key="DT_ELPDAYS" var="varELPDays"/><display:column property="ELPDAYS" title="${varELPDays}" sortable="true" sortName="ELPDAYS" style="width:80"/>
				    <%
				    } 
				    %>
				    <fmtIncludeChargesApprovalDetails:message key="DT_INCIDENT_VALUE" var="varIncidentValue"/><display:column property="INCIDENTVALUE" title="${varIncidentValue}" sortable="true" style="width:150" media="html"/>
				    <fmtIncludeChargesApprovalDetails:message key="DT_INCIDENT_VALUE" var="varIncidentValue"/><display:column property="INCIDENTVALUEEX" title="${varIncidentValue}" sortable="true" style="width:150" media="excel"/>				    
				    <fmtIncludeChargesApprovalDetails:message key="DT_INCIDENT_DATE" var="varIncidentDate"/><display:column property="INCIDENTDATE" title="${varIncidentDate}" sortable="true" style="text-align:center;width:60;" format="<%=strRptFmt%>"/>
				    <%if(chargesFor.equals("50891")){ %>
				     <fmtIncludeChargesApprovalDetails:message key="DT_EXTENDED_AMOUNT" var="varExtendedAmount"/><display:column property="EXTENDAMOUNT" title="${varExtendedAmount}" class="alignright"  format="<%=strCurrFmt%>"/>	
				    <%} %>			    
				    <fmtIncludeChargesApprovalDetails:message key="DT_AMOUNT" var="varAmount"/><display:column property="AMOUNT" title="${varAmount}" style="text-align:right;width:120;" format="<%=strCurrFmt%>"/> 
					<fmtIncludeChargesApprovalDetails:message key="LBL_STATUS" var="varStatus"/><display:column property="STFLNAME" title="${varStatus}" sortable="true" class="alignleft" media="excel" />	
				    <% if (deptId.equals("2001") || deptId.equals("2006") ){ %>				  	   
				    <fmtIncludeChargesApprovalDetails:message key="LBL_STATUS" var="varStatus"/><display:column property="STFL" style="text-align:left;width:100;"  title="${varStatus} <select name='cbo_head_stat' onChange='fnLoadStatus(this.value);' />" media="html">		    
						<select name='select' />
					</display:column>	
					 <% } else {%>
					<fmtIncludeChargesApprovalDetails:message key="LBL_STATUS" var="varStatus"/><display:column property="STFL" title="${varStatus}"  class="alignleft"  sortable="true"/>
					<%} %>	
					<fmtIncludeChargesApprovalDetails:message key="DT_CREDIT_AMOUNT" var="varCreditAmount"/><display:column property="CEREDIT_AMOUNT" title="${varCreditAmount}" style="text-align:left;width:60;" media="html" />
					<fmtIncludeChargesApprovalDetails:message key="DT_CREDIT_DATE" var="varCreditDate"/><display:column property="CREDIT_DATE" title="${varCreditDate}" style="text-align:left;width:120;" media="html" />			
				 </display:table> 
			</td>
		</tr>	
		</BODY>
		</HTML>
		
