<%@ page import ="java.util.HashMap"%>
<%@ page import ="java.util.ArrayList"%>
<%@ page import="org.apache.log4j.Logger"%> 
<%@page import="java.util.Date"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtIncConsign" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- include\GmIncludeConsignment.jsp -->
<fmtIncConsign:setLocale value="<%=strLocale%>"/>
<fmtIncConsign:setBundle basename="properties.labels.include.GmIncludeConsignment"/>
<%	/*********************************
	 * GmIncludeConsignment.jsp
	 *********************************/ 
	String strApplnDateFmt = strGCompDateFmt; //GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");

	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	
	Date dtLDate = null; 
	Date dtEDate = null; 
	
	HashMap hmCsg = new HashMap(); 
		
	// To get the Consignment information 
	hmCsg = (HashMap)request.getAttribute("hmCONSIGNMENT");
	if (hmCsg == null)
	{
		hmCsg = (HashMap)session.getAttribute("hmCONSIGNMENT");
	}
    String strConsignId = GmCommonClass.parseNull((String)hmCsg.get("CONSIGNID"));
	String strSetName = GmCommonClass.parseNull((String)hmCsg.get("SETNAME"));
	String strSetType = GmCommonClass.parseNull((String)hmCsg.get("SETTYPE"));
	String strEtchId = GmCommonClass.parseNull((String)hmCsg.get("ETCHID"));
	strEtchId = strEtchId.equals("")?"LOANER-":strEtchId;
	String strEtchStatus = GmCommonClass.parseNull(request.getParameter("ETCHSTATUS"));
	String strLoanStatus = GmCommonClass.parseNull((String)hmCsg.get("LOANSFL"));
	String strBillNm = GmCommonClass.parseNull((String)hmCsg.get("BILLNM"));
	dtLDate = (Date)hmCsg.get("LDATE");
	dtEDate = (Date)hmCsg.get("EDATE"); 
	
	String strType = GmCommonClass.parseNull((String)hmCsg.get("CTYPE"));
	String strLoanTransId = GmCommonClass.parseNull((String)hmCsg.get("LOANTRANSID"));
	String strHoldFl=GmCommonClass.parseNull((String)hmCsg.get("HOLDFL"));
	String strDistId  = GmCommonClass.parseNull((String)hmCsg.get("DISTID"));
	String strRepId  = GmCommonClass.parseNull((String)hmCsg.get("REPID"));
	String strAssocRepId  = GmCommonClass.parseNull((String)hmCsg.get("ASSOCREPID"));
	String strRepNm  = GmCommonClass.parseNull((String)hmCsg.get("REPNM"));
	String strAccId  = GmCommonClass.parseNull((String)hmCsg.get("ACCID"));
	String strAccNm  = GmCommonClass.parseNull((String)hmCsg.get("ACCNM"));
	//String strPriority = GmCommonClass.parseNull((String)hmCsg.get("SETPRIORITY"));
	//String strPriorityId = GmCommonClass.parseNull((String)hmCsg.get("SETPRIORITYID"));
	if(strHoldFl.equalsIgnoreCase("y")){
		strLoanStatus=strLoanStatus + " (Hold) ";
	}
	// to fix the alignment problem (Accept Return, Process Refill, Pending Picture and Set View screen)
	String strCamId = GmCommonClass.parseNull((String)request.getAttribute("strCamId"));
	String strPendPictureCamId = GmCommonClass.parseNull((String)request.getAttribute("strPendPictureCamId"));
	ArrayList alPendPictureCam = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALPENDPICTURECAM"));
	ArrayList alCam = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCAMERA"));
	String strTodaysDate = (String)session.getAttribute("strSessTodaysDate")==null?"":(String)session.getAttribute("strSessTodaysDate");
	Date dtTodaysDate = GmCommonClass.getStringToDate(strTodaysDate,GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt")));
	// to replace the br and add the one space 
	strBillNm = strBillNm.replaceAll("<BR>","<BR>&nbsp;");
	String strParentJsp = GmCommonClass.parseNull((String)request.getParameter("PARENTJSP"));
	String strReturnDt = GmCommonClass.parseNull((String)hmCsg.get("RETURNDT"));
	String strloansflcd = GmCommonClass.parseNull((String)hmCsg.get("LOANSFLCD"));
	String strMsg = GmCommonClass.parseNull((String)hmCsg.get("MSG"));
	String strLoanerPrStatus = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("LNSETPRIORITYSTS"));
	String strLnDispPriority = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("LNDISPPRIORITY"));
	Boolean blPrStatus = strLoanerPrStatus.indexOf("|"+strloansflcd+"|") != -1? true: false;
	Boolean blShowLbl = false;
	String strShowTagMsg = "";
	strShowTagMsg = GmCommonClass.parseNull((String)request.getAttribute("strShowTagMsg"));
	//Should show the label, only if it is from below screens
	if(strLnDispPriority.indexOf("|"+strParentJsp+"|") != -1){
		blShowLbl = true;
	}
	
	String strDisabled = "false";
	if (strType.equals("4119")) {
	strDisabled = "disabled";
	}
	String LoanDate = GmCommonClass.getStringFromDate(dtLDate,strApplnDateFmt);
	String ExpectedDate = GmCommonClass.parseNull((String)GmCommonClass.getStringFromDate(dtEDate,strApplnDateFmt));
%>
<script type="text/javascript">
distid = '<%=strDistId%>';
repid = '<%=strRepId%>';
repname = "<%=strRepNm%>";
accid = '<%=strAccId%>';
acname = "<%=strAccNm%>";
var transType = '<%=strType%>';
var loanDT = '<%=LoanDate%>'; 
var ExpectedDT = '<%=ExpectedDate%>';
</script>

<input type = "hidden" name="hConsignId" value="<%=strConsignId%>">
<input type = "hidden" name="hType" value="<%=strType%>">
<input type = "hidden" name="hLoanTransId" value="<%=strLoanTransId%>">
<input type="hidden" name="hCancelType" value="">
<input type="hidden" name="Cbo_LoanToASSORep" value="<%=strAssocRepId%>">
<input type="hidden" name="hTxnId" value="">

<!-- Custom tag lib code modified for JBOSS migration changes -->

<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
		<td class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<fmtIncConsign:message key="LBL_CONSIGN_ID"/>:</td>
		<td colspan="2">&nbsp;<a class="RightText" href="javascript:fnPrintVer('<%=strConsignId%>');"><%=strConsignId%></a></td>
		<td>
				<%if(blPrStatus && blShowLbl){ %>
						<jsp:include page="/operations/logistics/GmIncludePriority.jsp" >
						<jsp:param name="CONSIGNID" value="<%=strConsignId%>" />
						</jsp:include>
				<%} %>
		</td>
		
	</tr>
	<tr class="shade">
		<td class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<fmtIncConsign:message key="LBL_SET_NM"/>:</td>
		<td colspan="3">&nbsp;<%=GmCommonClass.getStringWithTM(strSetName)%></td>
	</tr>
	
				<tr>
					<td class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<fmtIncConsign:message key="LBL_SET_TYPE"/>:</td>
					<td>&nbsp;<%=strSetType%></td>
					<td class="RightTableCaption" align="right"><fmtIncConsign:message key="LBL_ETCH_ID"/>:</td>
<% 
					if (strEtchStatus.equals( "edit"))
					{
%>
					<td width="200">&nbsp;
						<input type="text" maxlength="20" size="22" value="<%=strEtchId%>" name="Txt_EtchId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
					</td>
<%
					}
					else
					{
%>
					<td width="200">&nbsp;<%=strEtchId%></td>
<% 				} 
%>					
				</tr>
				<tr><td colspan="4" height="1" bgcolor="#eeeeee"></td></tr>
				<tr class="shade">
					<td class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<fmtIncConsign:message key="LBL_STATUS"/>:</td>
					<td>&nbsp;<%=strLoanStatus%></td>
					<td class="RightTableCaption" align="right">&nbsp;<fmtIncConsign:message key="LBL_CURR_LOC"/>:</td>
					<td>&nbsp;<%=strBillNm%></td>
				</tr>
				<tr><td colspan="4" height="1" bgcolor="#eeeeee"></td></tr>
				<tr>
					<td class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<fmtIncConsign:message key="LBL_LOAN_DATE"/>:</td>
					<td>&nbsp;<%=GmCommonClass.getStringFromDate(dtLDate,strApplnDateFmt)%></td>
					<td class="RightTableCaption" align="right">&nbsp;<fmtIncConsign:message key="LBL_EXPC_DATE"/>:</td>
					<td>&nbsp;<%=GmCommonClass.getStringFromDate(dtEDate,strApplnDateFmt)%></td>
				</tr>	
				<%if(strParentJsp.equals("AcceptReturn")){ %>
				<tr class="Shade">
						<td class="RightTableCaption" align="right" height="23"
							><fmtIncConsign:message key="LBL_ACTUAL_DT_RTN"/>:</td>
						<td>&nbsp;<gmjsp:calendar
								textControlName="Txt_RetDate"
								textValue="<%=dtTodaysDate==null?null:new java.sql.Date(dtTodaysDate.getTime())%>"
								gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
								onBlur="changeBgColor(this,'#ffffff');" />
						</td>
						<td class="RightTableCaption" align="right" height="23"><font color="red">*</font>&nbsp;<fmtIncConsign:message key="LBL_CAMERA"/>:</td>
						<td>&nbsp;<gmjsp:dropdown
								controlName="Cbo_CameraId" seletedValue="<%=strCamId%>" disabled = "<%=strDisabled%>" 
								codeId="CODEID" codeName="CODENM" value="<%= alCam%>" 
								defaultValue="[Choose One]" onChange="fnChangeCamera(this);"/>
						</td>
					</tr>	
					<%}else if(strParentJsp.equals("PendingPic")){ %>
					<tr class="Shade">
							<fmtIncConsign:message key="LBL_CAMERA" var="varCamera"/>
							<td class="RightTableCaption" height="23" align="right"
								width="155"><gmjsp:label type="BoldText" SFLblControlName="${varCamera}:" td="false" /></td>
							<td align="left">&nbsp;<gmjsp:dropdown controlName="Cbo_CameraId"
									seletedValue="<%=strPendPictureCamId%>" codeId="CODEID"
									codeName="CODENM" value="<%=alPendPictureCam%>"
									defaultValue="[Choose One]" onChange="fnChangeCamera(this);" />
							</td>
							<td colspan="2">&nbsp;</td>
						</tr>
					<%}else if(strParentJsp.equals("SetView")){ %>
					<tr class="Shade">
							<fmtIncConsign:message key="LBL_RTN_DATE" var="varRtnDate"/>
							<td class="RightTableCaption" height="23" align="right"
								width="155"><gmjsp:label type="BoldText" SFLblControlName="${varRtnDate}:" td="false" /></td>
							<td align="left">&nbsp;<%=strReturnDt%></td>
							<td colspan="2">&nbsp;</td>
						</tr>
					<%if(!strMsg.equals("")){ %>
					<tr >
							<fmtIncConsign:message key="LBL_NOTE" var="varNote"/>
							<td class="RightTableCaption" height="23" align="right"
								width="155"><gmjsp:label type="BoldText" SFLblControlName="${varNote}:" td="false" /></td>
							<td align="left" colspan="3">&nbsp;<font color="red"><b><%=strMsg%></b></font></td>
							 
						</tr>
					<%} }else if(strParentJsp.equals("ProcessCheck") && strloansflcd.equals("25")){
						// 25 - Pending check %>		
					<tr class="Shade">
							<fmtIncConsign:message key="LBL_ACTUAL_DT_RTN" var="varActDate"/>
							<td class="RightTableCaption" height="23" align="right"
								width="155"><gmjsp:label type="BoldText" SFLblControlName="${varActDate}:" td="false" /></td>
							<td align="left">&nbsp;<%=strReturnDt%></td>
							<td colspan="2">&nbsp;</td>
						</tr>
					<%} %>
					<%-- <%if(blPrStatus && blShowLbl && strShowTagMsg.equals("YES")){ %>
					<tr>
						<td colspan="4" align="center">
							<jsp:include page="/operations/logistics/GmIncludePriority.jsp" >
							<jsp:param name="PRIORITY" value="<%=strPriority%>" />
							<jsp:param name="PRIORITYID" value="<%=strPriorityId%>" />
							<jsp:param name="LOCATION" value="tag" />
							</jsp:include>
						</td>
					</tr>
					<%} %> --%>
			</table>
		</td>
	</tr>
</table>
