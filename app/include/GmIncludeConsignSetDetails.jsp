 <%
/**********************************************************************************
 * File		 		: GmIncludeConsignSetDetails.jsp
 * Desc		 		: This screen is used for the displaying Set details of a Consignment
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>

<!--include\GmIncludeConsignSetDetails.jsp -->

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<%@ taglib prefix="fmtIncludeConsignSetDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- include/GmIncludeConsignSetDetails.jsp -->
<fmtIncludeConsignSetDetails:setLocale value="<%=strLocale%>"/>
<fmtIncludeConsignSetDetails:setBundle basename="properties.labels.include.GmIncludeConsignSetDetails"/>

<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strAction=GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	
%>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<%
if(strAction.equals("REPROCESS") || strAction.equals("Process_Reprocess")){
%>
 <display:table name="CONSIGNSETDETAILS" class="its" id="currentRowObject" decorator="com.globus.operations.displaytag.beans.DtInHouseSetdetailsWrapper"> 
	<fmtIncludeConsignSetDetails:message key="DT_PART" var="varPart"/><display:column property="PNUM" title="${varPart}" sortable="true"  />
	<fmtIncludeConsignSetDetails:message key="DT_PART_DESCRIPTION" var="varPartDescription"/><display:column property="PDESC" title="${varPartDescription}" />
 	<fmtIncludeConsignSetDetails:message key="DT_SETLIST_QTY" var="varSetlistQty"/><display:column property="SETLISTQTY" title="${varSetlistQty}" class="alignright" sortable="true"/>	
  	<fmtIncludeConsignSetDetails:message key="DT_ACTUAL_QTY" var="varActualQty"/><display:column property="QTY" title="${varActualQty}" class="alignright" sortable="true" />  	
 </display:table> 
<%
}else{
  %>
  
 <display:table name="CONSIGNSETDETAILS" class="its" id="currentRowObject" decorator=""> 
	<fmtIncludeConsignSetDetails:message key="DT_PART" var="varPart"/><display:column property="PNUM" title="${varPart}" sortable="true"  />
	<fmtIncludeConsignSetDetails:message key="DT_PART_DESCRIPTION" var="varPartDescription"/><display:column property="PDESC" title="${varPartDescription}" />
  	<fmtIncludeConsignSetDetails:message key="DT_QTY_IN_SET" var="varQtyInSet"/><display:column property="QTY" title="${varQtyInSet}" class="alignright" sortable="true" />
  	<fmtIncludeConsignSetDetails:message key="DT_MISSING_QTY" var="varMissingQty"/><display:column property="MISSQTY" title="${varMissingQty}" class="alignright" sortable="true" />
  	<fmtIncludeConsignSetDetails:message key="DT_RECON_QTY" var="varReconQty"/><display:column property="QTYRECON" title="${varReconQty}" class="alignright" sortable="true" />
  	<fmtIncludeConsignSetDetails:message key="DT_COGS_QTY" var="varCOGSQty"/><display:column property="COGSQTY" title="${varCOGSQty}" class="alignright" sortable="true" />
 </display:table>
 
 <%
}
 %>