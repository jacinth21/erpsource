
<%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%
/**********************************************************************************
 * File		 		: GmInvCosting.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="org.apache.commons.beanutils.RowSetDynaClass"%>
<%@page import="java.util.Date"%>
<%@ taglib prefix="fmtInvCosting" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmInvCosting.jsp -->
<fmtInvCosting:setLocale value="<%=strLocale%>"/>
<fmtInvCosting:setBundle basename="properties.labels.accounts.GmInvCosting"/>
<%
try {
	String strWikiTitle = GmCommonClass.getWikiTitle("INVENTORY_COSTINGS");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	int intSize = 0;
	String strCodeID = "";
	String strSelected = "";
	
	HashMap hmLoop = new HashMap();
	String strApplDateFmt = strGCompDateFmt;
	// to getting currency format
	HashMap hmCurrency = new HashMap();
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strCurrSign =(String)hmCurrency.get("CMPCURRSMB");
	String strApplCurrFmt = (String)hmCurrency.get("CMPCURRFMT");
	String strCurrFmt = "{0,number,"+" "+strApplCurrFmt+"}";

	String strCostId = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strTxnType = "";
	String strTxnDate = "";
	String strPurchAmt = "";
	String strStatus = "";
	String strQtyOnHand = "";
	String strQtyAdded = "";
	String strQty = "";
	String strCost = "";
	String strCostType = "";
	String strFromDt = "";
	String strToDt = "";
	String strChecked 	= "checked";
	String strCheckFl = "";
	String strArchiveFl = "";
	String strArchivedDate = "";
	Date archivedDate = null;
	String strOwnerCurrency = "";
	String strDivisionId = "";
	String strInvPlant = "";
	String strInvOwnerCompanyId = "";
	String strCogsStatusId = "";
	String strColspan = "";
	String strCurCnt="";
	
	String strAction = (String)request.getAttribute("hAction");

	String strShade = "";

	ArrayList alInvCosting = new ArrayList();
	ArrayList alCogsType = new ArrayList();
	ArrayList alCurrency = new ArrayList();
	ArrayList alPlant = new ArrayList ();
	ArrayList alOwnerCompany = new ArrayList ();
	ArrayList alCogsStatus = new ArrayList ();
	ArrayList alDivision = new ArrayList ();
	
	int intLength = 0;

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
		
	if (strAction.equals("Load") || strAction.equals("ReloadCost"))
	{
		alCogsType = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("COGSTYPE"));
		alCurrency = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALCURRN"));
		alPlant = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALPLANT"));
		alOwnerCompany = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALOWNERCOMPANY"));
		alCogsStatus = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALCOGSSTATUS"));
		alDivision = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALDIVISION"));
		if (strAction.equals("ReloadCost"))
		{
			alInvCosting =  GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("COSTING"));
			intLength = alInvCosting.size();
		}
		strFromDt = GmCommonClass.parseNull((String)request.getAttribute("hFrmDt"));
		strToDt = GmCommonClass.parseNull((String)request.getAttribute("hToDt"));
		strPartNum = GmCommonClass.parseNull((String)request.getAttribute("hPartNum"));
		strTxnType = GmCommonClass.parseNull((String)request.getAttribute("hCogsType"));
		strCheckFl = GmCommonClass.parseNull((String)request.getAttribute("hCheck"));
		strArchiveFl = GmCommonClass.parseZero((String)request.getAttribute("Chk_archive_fl"));
		strInvPlant = GmCommonClass.parseZero((String)hmReturn.get("hInvPlant"));
		strInvOwnerCompanyId = GmCommonClass.parseZero((String)hmReturn.get("hOwnerCompany"));
		strOwnerCurrency = GmCommonClass.parseZero((String)hmReturn.get("hOwnerCurrency"));
		strCogsStatusId =  GmCommonClass.parseZero((String)hmReturn.get("hCogsStatus"));
		strDivisionId = GmCommonClass.parseZero((String)hmReturn.get("hDivision"));

	}
	strChecked 	= strCheckFl.equals("on")?"checked":"";
	strArchiveFl = strArchiveFl.equals("true")?"checked":"";
	strArchivedDate = GmCommonClass.parseNull((String)request.getAttribute("COST_ARCHIVE_DATE"));
	//Hide/Show Owner Company footer total based on currency count
	strCurCnt=GmCommonClass.parseNull((String)request.getAttribute("CUR_CNT"));	
	strColspan = strCurCnt.equals("2")?"4":"2";	
	
	archivedDate = (java.util.Date)(GmCommonClass.getStringToDate(strArchivedDate, "yyyy-MM-dd"));
	strArchivedDate = GmCommonClass.getStringFromDate(archivedDate, strGCompDateFmt);
	HashMap hcboVal = null;
	double dbAmt = 0.0;
	double dbTotal = 0.0;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Inventory Costing</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmTransactionLog.js"></script>

<script>
var lblInvoiceType = '<fmtInvCosting:message key="LBL_INVENTORY_TYPE"/>';
function fnReload(obj)
{
	var date_fmt = '<%=strGCompDateFmt%>';
	
	CommonDateValidation(document.frmAccount.Txt_FromDate,date_fmt,message_accounts[263]+ message[611]);
	CommonDateValidation(document.frmAccount.Txt_ToDate,date_fmt,message_accounts[264]+ message[611]);


	fnValidateDropDn('Cbo_Cogs',lblInvoiceType);
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	fnStartProgress('Y');	
	document.frmAccount.hAction.value = "ReloadCost";
	document.frmAccount.submit();
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmInvPostServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hAccId" value="<%=strPurchAmt%>">

	<table border="0" class="DtTable1200" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="3"><fmtInvCosting:message key="LBL_INVENTORY_COSTING"/></td>
			<td height="25" class="RightDashBoardHeader" align="right" >
					<fmtInvCosting:message key="LBL_HELP" var="varHelp"/>
				    <img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
			       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> </td>
		</tr>		
		<tr class="shade">
			<td height="25" class="RightTableCaption" align="right" width="13%"><fmtInvCosting:message key="LBL_PART_NUMBER"/>:</td>
			<td align="left" width="40%">&nbsp;<input type="text" name="Txt_PNum" value="<%=strPartNum%>" class="RightText" size="50" tabindex="1" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"></td>
			<td class="RightTableCaption" align="right" width="15%"><font color="red">*</font>&nbsp;<fmtInvCosting:message key="LBL_INVENTORY_TYPE"/>:</td>
			<td align="left">&nbsp;<gmjsp:dropdown controlName="Cbo_Cogs" seletedValue="<%=strTxnType %>" defaultValue="[Choose One]" codeId="CODEID" codeName="CODENM" value="<%=alCogsType %>" tabIndex="2"/>
			</td>
		</tr>					
		<tr><td class="LLine" height="1" colspan="4"></td></tr>		
		<tr>
			<td height="25" class="RightTableCaption" align="right" width="13%"><fmtInvCosting:message key="LBL_OWNER_COMPANY"/>:</td>
			<td align="left" width="40%">&nbsp;<gmjsp:dropdown controlName="cbo_owner_company" seletedValue="<%=strInvOwnerCompanyId %>" defaultValue="[Choose One]" codeId="COMPANYID" codeName="COMPANYNM" value="<%=alOwnerCompany %>" tabIndex="3"/></td>
			<td class="RightTableCaption" align="right" width="15%"><fmtInvCosting:message key="LBL_OWNER_CURRENCY"/>:</td>
			<td align="left">&nbsp;<gmjsp:dropdown controlName="cbo_owner_currency" seletedValue="<%=strOwnerCurrency %>" defaultValue="[Choose One]" codeId="CODEID" codeName="CODENMALT" value="<%=alCurrency %>" tabIndex="4"/>
			</td>
		</tr>	
		<tr><td class="LLine" height="1" colspan="4"></td></tr>		
		<tr class="Shade">
			<td height="25" class="RightTableCaption" align="right" width="13%"><fmtInvCosting:message key="LBL_PLANT"/>:</td>
			<td align="left" width="40%">&nbsp;<gmjsp:dropdown controlName="cbo_inv_plant" seletedValue="<%=strInvPlant %>" defaultValue="[Choose One]" codeId="PLANTID" codeName="PLANTNM" value="<%=alPlant %>" tabIndex="5"/>
			
			<span class="RightTableCaption">&nbsp;<fmtInvCosting:message key="LBL_DIVISION"/>:</span>
			<span>&nbsp;<gmjsp:dropdown controlName="cbo_division" seletedValue="<%=strDivisionId %>" defaultValue="[Choose One]" codeId="DIVISION_ID" codeName="DIVISION_NAME" value="<%=alDivision %>" tabIndex="6"/></span>
					
			</td>
			<td class="RightTableCaption" align="right" width="15%">&nbsp;<fmtInvCosting:message key="LBL_STATUS"/>:</td>
			<td align="left">&nbsp;<gmjsp:dropdown controlName="cbo_cogs_status" seletedValue="<%=strCogsStatusId %>" defaultValue="[Choose One]" codeId="CODEID" codeName="CODENM" value="<%=alCogsStatus %>" tabIndex="7"/></td>
		</tr>				
		<tr><td class="LLine" height="1" colspan="4"></td></tr>	
		<tr>
			<td class="RightTableCaption" align="right" height="25" ><fmtInvCosting:message key="LBL_DATE_RANGE"/>:</td>
			<td class="RightTableCaption" >&nbsp;<fmtInvCosting:message key="LBL_FROM_DATE"/>:&nbsp;
			<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=(strFromDt.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strFromDt,strApplDateFmt).getTime())))%>"  gmClass="InputArea" 
				onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="8"/>
			&nbsp;<fmtInvCosting:message key="LBL_TO_DATE"/>:&nbsp;
			<gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=(strToDt.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strToDt,strApplDateFmt).getTime())))%>"  gmClass="InputArea" 
				onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="9"/>
			</td>
			<td align="right"><input type="checkbox" <%=strChecked%>   name="Chk_Open" tabindex="10" ></td>
			<td>
				<table>
					<tr>
					
						<td colspan="2" class="RightTableCaption" align="left">&nbsp;<fmtInvCosting:message key="LBL_OPEN"/>&nbsp;&nbsp;</td>
						<td class="RightTableCaption" align="left" ><input type="checkbox" <%=strArchiveFl%>   name="Chk_archive_fl" tabindex="11" >&nbsp;<fmtInvCosting:message key="LBL_ARCHIVED_DATA"/>
						</td>
						<fmtInvCosting:message key="BTN_LOAD" var="varLoad"/>
			    		<td><gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="javascript:fnReload(this);" tabindex="12" buttonType="Load" /></td></tr></table></td>
		</tr>
		<tr>
		<td  class="LLine" height="1" colspan="4"></td></tr>
		<tr>
		<td colspan="4" align = "center"  class="RightTableCaption" ><span align = "center" id='arcived'><fmtInvCosting:message key="LBL_ARCHIVED_DATA_ASOF"/>  <%=strArchivedDate %></span>
		</td>
		</tr>
		<tr><td  class="LLine" height="1" colspan="4"></td></tr>	
		<tr>
			<td colspan="4" align="center">
				<display:table name="requestScope.results.rows" export="true" class="its" varTotals="totals" >
                    <display:setProperty name="export.excel.filename" value="Inv Costing.xls" />
					<fmtInvCosting:message key="LBL_PART_NUMBER" var="varPartNumber"/><display:column property="PNUM" title="${varPartNumber}"  sortable="true" />
					<fmtInvCosting:message key="LBL_DESCRIPTION" var="varDescription"/><display:column property="PDESC" escapeXml="true" title="${varDescription}" sortable="true" />
					<fmtInvCosting:message key="LBL_DATE_POSTED" var="varDatePosted"/><display:column property="TXNDATE" title="${varDatePosted}" class="aligncenter" />
					<fmtInvCosting:message key="LBL_QTY_POSTED" var="varQtyPosted"/><display:column property="QTYREC" title="${varQtyPosted}" class="alignright"/>
					<fmtInvCosting:message key="LBL_PLANT" var="varPlant"/><display:column property="PLANT_NAME" title="${varPlant}" sortable="true" class="aligncenter" />
					<fmtInvCosting:message key="LBL_OWNER_COMPANY" var="varOwnerComp"/><display:column property="OWNER_COMPANY_CD" title="${varOwnerComp}" sortable="true" class="aligncenter" />
					<fmtInvCosting:message key="LBL_QOH" var="varQoh"/><display:column property="QOH" title="${varQoh}" class="alignright"  total="true" sortable="true" />
					<fmtInvCosting:message key="LBL_EA_COST" var="varEaCost"/><display:column property="PURCAMT" title="${varEaCost}" sortable="true" format="<%=strCurrFmt %>" headerClass="ShadeGreenAlignRightTD" class="ShadeGreenAlignRightTD" total="true"/>
					<fmtInvCosting:message key="LBL_EXTENDED_COST" var="varExtendedCost"/><display:column property="EXTCOST" title="${varExtendedCost}" sortable="true" format="<%=strCurrFmt %>" headerClass="ShadeGreenAlignRightTD" class="ShadeGreenAlignRightTD" total="true"/>
					<fmtInvCosting:message key="LBL_OWNER_CURRENCY" var="varOwnerCurr"/><display:column property="OWNER_CURRENCY" title="${varOwnerCurr}" sortable="true"  headerClass="ShadeGreenAlignCenterTD" class="ShadeGreenAlignCenterTD"/>
					<fmtInvCosting:message key="LBL_STATUS" var="varStatus"/><display:column property="SFL" title="${varStatus}" sortable="true" />
					<fmtInvCosting:message key="LBL_LOCAL_COMPANY_COST" var="varLocalCost"/><display:column property="LOCAL_COMPANY_COST" title="${varLocalCost}" total="true" sortable="true" format="<%=strCurrFmt %>" headerClass="ShadeDarkBlueAlignRightTD" class="ShadeDarkBlueAlignRightTD"/>
					<fmtInvCosting:message key="LBL_LOCAL_COMPANY_EXT" var="varLocalExtCost"/><display:column property="EXT_LOCAL_COMPANY_COST" title="${varLocalExtCost}" total="true" sortable="true" format="<%=strCurrFmt %>" headerClass="ShadeDarkBlueAlignRightTD" class="ShadeDarkBlueAlignRightTD"/>
					<fmtInvCosting:message key="LBL_LOCAL_COMPANY_CURRENCY" var="varLocalComp"/><display:column property="LOCAL_COMPANY_CURRENCY" title="${varLocalComp}" sortable="true" headerClass="ShadeDarkBlueAlignCenterTD" class="ShadeDarkBlueAlignCenterTD"/>
					<fmtInvCosting:message key="LBL_DIVISION" var="vardivision"/><display:column property="DIVISION" title="${vardivision}" sortable="true" />
					<display:footer media="html"> 
<% 
				String strVal ;
				HashMap strTest = (HashMap)pageContext.getAttribute("totals");
				strVal=   String.valueOf(((HashMap)pageContext.getAttribute("totals")).get("column7"));
		  		String strQOH = strVal.substring(0, strVal.indexOf("."));  
		  		strVal = ((HashMap)pageContext.getAttribute("totals")).get("column8").toString();
		   		String strEAPrice =  GmCommonClass.getStringWithCommas(strVal);
		  		strVal = ((HashMap)pageContext.getAttribute("totals")).get("column9").toString();
		  		String strExtPrice =  GmCommonClass.getStringWithCommas(strVal); 
		  		strVal = ((HashMap)pageContext.getAttribute("totals")).get("column12").toString();
		  		String strLocalPrice =  GmCommonClass.getStringWithCommas(strVal);
		  		strVal = ((HashMap)pageContext.getAttribute("totals")).get("column13").toString();
		  		String strLocalExtPrice =  GmCommonClass.getStringWithCommas(strVal);
		  		
%>			
				  	<tr class = shade>
						<td colspan="6" height="20" class = "alignright" > <B> <fmtInvCosting:message key="LBL_TOAL"/>: </B></td>
						<td class = "alignright" ><B><%=strQOH%></B></td>
						<%if(strCurCnt.equals("1")){ %>
					 	<td class = "alignright" ><B><%=strEAPrice%></B></td>
						<td class = "alignright" ><B><%=strExtPrice%></B></td>
						<%} %> 
					  	<td colspan="<%=strColspan%>">&nbsp;</td>
					  	<td class = "alignright" ><B><%=strLocalPrice%></B></td> 
						<td class = "alignright" ><B><%=strLocalExtPrice%></B></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
				  	</tr>
					 </display:footer>
				</display:table>
			</td>
		</tr>
    </table>

</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
