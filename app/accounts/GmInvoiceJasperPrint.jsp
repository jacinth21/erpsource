<%
/**********************************************************************************
 * File		 		: GmInvoiceJasperPrint.jsp
 * Desc		 		: This screen is used for invocie JASPER report.
 * author			: Elango
************************************************************************************/
%>
 <!--\accounts\GmInvoiceJasperPrint.jsp -->
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import="org.apache.commons.beanutils.BeanComparator"%>
<%@ page import="org.apache.commons.collections.comparators.NullComparator"%>
<%@ include file="/common/GmHeader.inc" %>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);	
	String strJASNAme = GmCommonClass.parseNull((String)request.getAttribute("INVJASNAME"));
	strJASNAme = strJASNAme.equals("") ? "/GmUSInvPrintJasper.jasper":strJASNAme;
	HashMap hmReturnData = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("INVRPTDATA"));
	ArrayList alHashMap = GmCommonClass.parseNullArrayList((ArrayList)hmReturnData.get("ALORDERLIST"));
	//below code added to sort arraylist by surgery date
	Comparator comparator = new BeanComparator("SURG_DT", new NullComparator(false));
    Collections.sort(alHashMap, comparator);
	
	HashMap hmOrderDetails = GmCommonClass.parseNullHashMap((HashMap)hmReturnData.get("HMORDERDETAILS"));
	HashMap hmDealerInvoice = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("DEALERDETAILS"));
	HashMap hmDealerInfo = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("DEALERINFO"));
	hmOrderDetails.putAll(hmDealerInfo);
	hmOrderDetails.putAll(hmDealerInvoice);
	
	String strHtmlJasperRpt = "";
	hmOrderDetails.put("ACCOUNTVATNUM", GmCommonClass.parseNull((String)request.getAttribute("ACCOUNTVATNUM")));
	hmOrderDetails.put("BANK_DETAILS", GmCommonClass.parseNull((String)request.getAttribute("BANKNAME")));
	hmOrderDetails.put("ALORDERLIST",alHashMap);
	hmOrderDetails.put("SIZE",alHashMap.size());
	//
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
	String strSetRbObjectFl =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.ADD_RESOURCE_BUNDLE_OBJ"));
	String strSetRbObFl = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("ADD_RESOURCE_BUNDLE_OBJ"));
    String strInvXhtmlFlag =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("XHTML_INVOICE_FLAG"));
	String strImgPath = System.getProperty("ENV_PAPERWORKIMAGES");
	String strxhtmlinvflag =  GmCommonClass.parseNull((String)request.getAttribute("STRXHTMLINVOICEFLAG"));
    log.debug("strInvXhtmlFlag" +strInvXhtmlFlag);
    hmOrderDetails.put("IMAGEPATH",strImgPath);
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Invoice Print </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script type="text/javascript">

function fnPrint()
{
    window.print();
}
var tdinnner = "";
var strObject="";
function hidePrint()
{
	strObject = eval("document.all.button_table");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button_table");
	strObject.innerHTML = tdinnner ;
}
</script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screenOus.css">
</HEAD>
<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM method="post" action = "<%=strServletPath%>/GmInvoiceServlet">
<div id="button_table">
<table align="center" width="100%">
<tr>
	<td align="center" id="button">
		<gmjsp:button value="Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />
 		<gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" />
 	</td>
 </tr>
 </table>
 </div>	
<iframe id="ifmPrintContents" style="height: 0px; width: 0px; position: absolute"></iframe>
<div id="jasper" style=" max-height: 500px"> 
		<%
			//if alHashMap size is 0 , need to handle here, otherwise it will not print in Jasper.
			if(alHashMap != null && alHashMap.size()==0)
			{
				HashMap hmTemp = new HashMap();
				hmTemp.put("PRICE","0");
				hmTemp.put("UNITPRICE","0");
				hmTemp.put("CUST_PDESC","");
				hmTemp.put("NTFTCODE","");
				hmTemp.put("CONFL","");
				hmTemp.put("VATPER","0");
				hmTemp.put("ORDERID","");
				hmTemp.put("ITEMTOTAL","0.0");
				hmTemp.put("ID","");
				hmTemp.put("QTY","0");
				hmTemp.put("CUST_PNUM","");
				hmTemp.put("VAT","0");
				hmTemp.put("SCOST","0");
				hmTemp.put("PDESC","");
				hmTemp.put("ORDERDATE","");
				hmTemp.put("TYPE","");
				hmTemp.put("ODT","");
				alHashMap.add(hmTemp);
			}
			String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
			GmJasperReport gmJasperReport = new GmJasperReport();								
			gmJasperReport.setRequest(request);
			gmJasperReport.setResponse(response);
			gmJasperReport.setJasperReportName("invoice"+strJASNAme);
			gmJasperReport.setHmReportParameters(hmOrderDetails);
			gmJasperReport.setReportDataList(alHashMap);
			gmJasperReport.setBlDisplayImage(true);
			/* gmJasperReport.setPageHeight(791);   */ //PMT-30557 : (Page height avoid blank page at end of report);
			// to set the dynamic resource bundle object
			if (strSetRbObFl.equalsIgnoreCase("YES") ) {
		          gmJasperReport.setRbPaperwork(gmResourceBundleBean.getBundle());//PMT-53539 japan-new-warehouse-address
		        }
	        if (strSetRbObjectFl.equalsIgnoreCase("YES") ) {
	          gmJasperReport.setRbPaperwork(gmResourceBundleBean.getBundle());
	        }
            if(strInvXhtmlFlag.equals("YES") || strxhtmlinvflag.equals("Y")){   
				strHtmlJasperRpt = gmJasperReport.getXHtmlReport();
			}else{
			  	strHtmlJasperRpt = gmJasperReport.getHtmlReport();
			}       
		%>				
		<%=strHtmlJasperRpt %>
</div>	
</FORM>
</BODY>
</HTML>