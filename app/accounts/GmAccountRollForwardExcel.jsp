 <%@page import="java.util.Locale"%>
<%
/**********************************************************************************
 * File		 		: GmAccountRollForwardReport.jsp TEST
 * Desc		 		: This screen is used to display Account RollForward Report
 * Version	 		: 1.0 
 * author			: Richard 
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtAccountRollforwardExcel" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmAccountRollForwardExcel.jsp -->

<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	Locale locale = null;
	String strLocale = "";

	String strJSLocale = "";

	String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));


	if(!strSessCompanyLocale.equals("")){
		locale = new Locale("en", strSessCompanyLocale);
		strLocale = GmCommonClass.parseNull((String)locale.toString());
		strJSLocale = "_"+strLocale;
	}
	
	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	String strCommonPath = GmCommonClass.getString("GMCOMMON");
	
	ArrayList alOpenBalance = new ArrayList();
	ArrayList alDetailList = new ArrayList();
	ArrayList alAccountList = new ArrayList();
	HashMap hmTempLoop = new HashMap();
						
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	
	// Opening Balance and det
	alOpenBalance 	= (ArrayList)hmReturn.get("OPEN_BALANCE");
	alDetailList	= (ArrayList)hmReturn.get("DETAIL_LIST");
	alAccountList	= (ArrayList)request.getAttribute("alAccountList");
	
	int intLength = alOpenBalance.size();
	int intDetailList = alDetailList.size();
	
	String strEList		= GmCommonClass.parseNull((String)request.getAttribute("hElementList"));
	String strFrmDate 	= GmCommonClass.parseNull((String)request.getAttribute("hFrom"));
	String strToDate 	= GmCommonClass.parseNull((String)request.getAttribute("hTo"));

	HashMap hmLoop = new HashMap();

	String strAccId = "";
	String strAccName = "";
	String strOrdId = "";
	String strCodeID = "";
	String strElemId = "";
	String strSelected = "";
	String strOrdDate = "";
	String strCSPerson = "";
	String strPrice = "";

	String strShade = "";
	String strCurCnt="";
	//Hide/Show Owner Company footer total based on currency count
	if(intDetailList!=0)
	{
	HashMap hmCurCnt = GmCommonClass.parseNullHashMap((HashMap)alDetailList.get(0));
	strCurCnt=GmCommonClass.parseNull((String)hmCurCnt.get("CUR_CNT"));
	}
	String strColspan = strCurCnt.equals("2")?"4":"2";
%>
<fmtAccountRollforwardExcel:setLocale value="<%=strLocale%>"/>
<fmtAccountRollforwardExcel:setBundle basename="properties.labels.accounts.GmAccountRollForwardExcel"/>
<HTML>
<HEAD>
<TITLE> Globus Medical: Sales Orders Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmAccountRollforwardServlet">
<input type="hidden" name="hAccountList" value="">
	<table border="0" width="900" cellspacing="0" cellpadding="0">
	<tr>
		<td width="900" valign="top">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
					<tr><td class="Line" colspan="7"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="400"><font color="blue"> <fmtAccountRollforwardExcel:message key="LBL_ACCOUNT_NAME"/></font></td>
						<td HEIGHT="24"  align="left" ><font color="blue"><fmtAccountRollforwardExcel:message key="LBL_OWNER_COMPANY"/></font></td>
						<td HEIGHT="24"  align="left" ><font color="blue"><fmtAccountRollforwardExcel:message key="LBL_OWNER_CURRENCY"/></font></td>
						<td HEIGHT="24"  align="right" width="100" ><font color="blue"><fmtAccountRollforwardExcel:message key="LBL_AMOUNT"/></font></td>
						<td HEIGHT="24"  align="right" ><font color="blue"><fmtAccountRollforwardExcel:message key="LBL_LOCAL_CURRENCY"/></font></td>
						<td HEIGHT="24"  align="right" ><font color="blue"><fmtAccountRollforwardExcel:message key="LBL_LOCAL_AMOUNT"/></font></td>
					</tr>
<%				if (intLength > 0)
				{
					String strNextId = "";
					int intCount = 0;
					String strLine = "";
					String strTotal = "";
					String strAmt = "";
					String strOwnerCompanyNm = "";
					String strOwnerCurrency = "";
					String strDetailAccount = "";
					String strLocalAmt = "";
					String strLocalCurrency = "";
					String strOwnerId= "";
					String strDetailOwnerId= "";

					double dbTotal = 0.0;
					double dbLocalTotal = 0.0;
					
					for (int i = 0;i < intLength ;i++ )
					{
						hmLoop 		= (HashMap)alOpenBalance.get(i);
						strAccId 	= (String)hmLoop.get("ID");
						strAccName 	= (String)hmLoop.get("NAME");
						strAmt		= GmCommonClass.parseZero((String)hmLoop.get("AMT"));
						strLocalAmt = GmCommonClass.parseZero((String)hmLoop.get("LOCAL_AMT"));
						
						dbTotal		= Double.parseDouble(strAmt);
						dbLocalTotal = Double.parseDouble(strLocalAmt);

						intCount++;
						strShade	= (intCount%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
						
%>						<tr id="tr<%=strAccId%>">
							<td colspan="6" height="20" class="RightText">
							&nbsp;<%=strAccName%></td>
						</tr>
						<tr><td colspan="6">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tab<%=strAccId%>">

						<tr <%=strShade%>>
							<td height="20" class="RightText"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtAccountRollforwardExcel:message key="LBL_OPENING_BALANCE"/></b></td>
							<td colspan="2">&nbsp;</td>	
							<td height="20" class="RightText" align="right">
								<%= GmCommonClass.getRedText(GmCommonClass.getStringWithCommas((String)hmLoop.get("AMT"))).replace("&nbsp", "") %></td>
							<td>&nbsp;</td>
							<td height="20" class="RightText" align="right">
								<%= GmCommonClass.getRedText(GmCommonClass.getStringWithCommas((String)hmLoop.get("LOCAL_AMT"))).replace("&nbsp", "") %></td>	
						</tr>
						<tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>
<%						for (int j =0;j < intDetailList; j++)
						{
							hmLoop = (HashMap)alDetailList.get(j);
							strDetailAccount = (String)hmLoop.get("A_NAME");

							if (strDetailAccount.equals(strAccName))
							{
								intCount++;
								strShade	= (intCount%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
								strAmt		= GmCommonClass.parseZero((String)hmLoop.get("AMT"));
								strOwnerCompanyNm = GmCommonClass.parseNull((String)hmLoop.get("OWNER_COMPANY"));
								strOwnerCurrency = GmCommonClass.parseNull((String)hmLoop.get("OWNER_CURRENCY"));
								strLocalAmt = GmCommonClass.parseZero((String)hmLoop.get("LOCAL_AMT"));
								strLocalCurrency= GmCommonClass.parseNull((String)hmLoop.get("LOCAL_CURRENCY"));
								dbTotal 	= dbTotal + Double.parseDouble(strAmt);
								dbLocalTotal 	= dbLocalTotal + Double.parseDouble(strLocalAmt);

%>							
						<tr <%=strShade%> >
							<td height="20" class="RightText"  width="400">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - <%=(String)hmLoop.get("TXN_NAME")%></td>
							<td height="20" class="RightText"><%= strOwnerCompanyNm %></td>
							<td height="20" class="RightText" width="300"><%= strOwnerCurrency %></td>			
							<td height="20" class="RightText" align="right">
								<%= GmCommonClass.getRedText(GmCommonClass.getStringWithCommas((String)hmLoop.get("AMT"))).replace("&nbsp", "") %></td>
							<td height="20" class="RightText"><%= strLocalCurrency %></td>		
							<td height="20" class="RightText" align="right">
								<%= GmCommonClass.getRedText(GmCommonClass.getStringWithCommas((String)hmLoop.get("LOCAL_AMT"))).replace("&nbsp", "") %></td>	
						</tr>
						<tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>
							
<%							}
						}// end of FOR loop j
						intCount++;
						strShade	= (intCount%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows						
%>						<tr <%=strShade%>>
							<td height="20" class="RightText"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtAccountRollforwardExcel:message key="LBL_ENDING_BALANCE"/></b></td>
							<td colspan="<%=strColspan%>">&nbsp;</td>	
							<%if(strCurCnt.equals("1")){ %>
							<td class="RightText" align="right">
								<%= GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(dbTotal+"")).replace("&nbsp", "") %></td>
							<td>&nbsp;</td>	
							<%} %>	
							<td height="20" class="RightText" align="right">
								<%= GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(dbLocalTotal+"")).replace("&nbsp", "") %></td>
						</tr>
	
						</table>
						<TR><TD colspan="6" height=1 class=Line></TD></TR>					
						
<%					}// end of FOR loop i 
%>
<%
						} // End of IF
						else{
%>
					<tr><td colspan="7" class="Line"></td></tr>					
					<tr><td colspan="7" class="RightTextRed" height="50" align=center><fmtAccountRollforwardExcel:message key="LBL_NO_SALES_PERIOD"/> ! </td></tr>
<%					
						}
%>
				</table>
			</td>
		</tr>
    </table>

</FORM>
</BODY>

</HTML>
