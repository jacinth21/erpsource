<%
/**********************************************************************************
 * File		 		: GmDownloadInvoiceDtls.jsp
 * Desc		 		: Download Invoice Detail Screen
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>

<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ page import="java.text.DecimalFormat" %>
<%@ taglib prefix="fmtDownloadInoviceDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmDownloadInvoiceDtls.jsp -->
<fmtDownloadInoviceDetails:setLocale value="<%=strLocale%>"/>
<fmtDownloadInoviceDetails:setBundle basename="properties.labels.accounts.GmDownloadInvoiceDtls"/>
<bean:define id="returnList" name="frmDownload" property="returnList" type="java.util.List"></bean:define>
 
 
<%
	DecimalFormat doublef = new DecimalFormat("#,###,###.00");
	ArrayList alSageVendorList = new ArrayList();
	 
	alSageVendorList = GmCommonClass.parseNullArrayList((ArrayList) returnList);
	
 	int rowsize = alSageVendorList.size();
	double invoiceAmtTotal =0; 
	String strApplnDateFmt = strGCompDateFmt;
	String strWikiTitle = GmCommonClass.getWikiTitle("DOWNLOAD_INVOICE_DETAILS");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Download Invoice Detail</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
#vendorId{width:100%;}
</style>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"	media="print" />
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
var format = '<%=strApplnDateFmt%>';
function fnReload()
{     
//	alert(document.frmDownload.missingSageVenIds.value);
	 
	var objFromDT = document.frmDownload.invoiceDateFrom.value;
	var objToDT = document.frmDownload.invoiceDateTo.value;
	// check correct month format entered
    CommonDateValidation(document.frmDownload.invoiceDateFrom,format,Error_Details_Trans(message_accounts[267],format));
	CommonDateValidation(document.frmDownload.invoiceDateTo,format,Error_Details_Trans(message_accounts[268],format));
	
	// Validate Order From/To Dates in proper order
	if (dateDiff(objFromDT, objToDT,format) < 0)
	{
		Error_Details(message_accounts[269]);
	}
	document.frmDownload.strOpt.value = "reload"; 
	if (ErrorCount > 0)  
	{
		Error_Show();
		Error_Clear();
		return false;
					} 
	fnStartProgress();
	document.frmDownload.submit();   
} 
 
function fnSelectAll(){
  TotalRows = <%=rowsize%>;
 for (var i=0;i<TotalRows ;i++ )
	{
		obj = eval("document.frmDownload.Chk_Sage"+i);	
		if(!obj.disabled)
		obj.checked = document.frmDownload.Chk_selectAll.checked;		 
	}
	fnCalcInvcAmt();
}
 
function fnRollback()
{ 
	var varRow = <%=rowsize%>;  
	var counter = 0; 
 
	for (var j =0; j < varRow; j++) 
	{
		obj = eval("document.frmDownload.Chk_Sage"+j);  
		obj1 = eval("document.frmDownload.hPaymentID"+j);
		if(obj.checked)		
		{  	varPaymentId = obj1.value;
			counter++;
		} 
	 
	}
 	if(counter==0 || counter >1)
 	Error_Details(message[37]);
	
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	} 
 		document.frmDownload.hTxnId.value = varPaymentId;
		document.frmDownload.hCancelType.value = 'RPYMT'
		document.frmDownload.action ="/GmCommonCancelServlet";
		document.frmDownload.hAction.value = "Load";				
 		document.frmDownload.submit();
		//alert(" counting " + counter);
	//	return counter;
} 
 
  
function fnSubmit()
{
	var varRows = <%=rowsize%>;  
	var totalAmt = 0; 
	var inputString = '';  
	var count = 0; 
	
	
	for (var i =0; i < varRows; i++) 
	{
		obj = eval("document.frmDownload.Chk_Sage"+i);
	 	obj1 = eval("document.frmDownload.hPaymentID"+i);
	 	obj2 = eval("document.frmDownload.hInvoiceAmt"+i);
		varStatus = obj.value;
		varPaymentID = obj1.value;
		varInvoiceAmt = obj2.value; 
		amt = varInvoiceAmt * 1;
		if(obj.checked)		
		{ 
			inputString = inputString  + varPaymentID +  '^';
			obj.value = "on";
			totalAmt = totalAmt +amt ;
			count++;
		}
		else
		 obj.value = ""; 
	 
	}
	if(count==0)
	Error_Details(message[36]);
	
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
//  	alert("count is " + fnRollback());
 //	if(count>1) inputString = inputString.substring(0, inputString.length-1);
	 
	document.frmDownload.hdownloadAmtTotal.value = totalAmt;	    
	document.frmDownload.strOpt.value =  'save'; //'save';  
	document.frmDownload.hinputStr.value = inputString;
   	document.frmDownload.submit();	
 
} 
 
 function fnCalcInvcAmt()
 {
var varRows = <%=rowsize%>;  
	var totalAmt = 0;   
	var count = 0; 
	
	
	for (var i =0; i < varRows; i++) 
	{
		obj = eval("document.frmDownload.Chk_Sage"+i);
	 	 
	 	obj2 = eval("document.frmDownload.hInvoiceAmt"+i);
		varStatus = obj.value;
	 
		varInvoiceAmt = obj2.value; 
		amt = varInvoiceAmt * 1;
		if(obj.checked)		
		{ 
		 
			totalAmt = totalAmt +amt ;
			count++;
		}
		else
		 obj.value = ""; 
	 
	}
	document.frmDownload.hdownloadAmtTotal.value = totalAmt;	
	document.all.Lbl_InvcTotal.innerHTML = "&nbsp;<B>$ " + formatNumber(document.frmDownload.hdownloadAmtTotal.value) + "</B>&nbsp;";
	    
	}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmACDownload.do?method=loadInvoiceBatch">
	<html:hidden property="strOpt" value="" /> 
	<input type="hidden" name="hAction" value="">
	<html:hidden property="hinputStr" /> 
	<html:hidden property="hdownloadAmtTotal" />
	<html:hidden property="hTxnId" value=""/>
	<html:hidden property="hCancelType" value=""/> 
	 
	<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4"><fmtDownloadInoviceDetails:message key="LBL_DOWNLOAD_INOVICE_DETAILS"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtDownloadInoviceDetails:message key="LBL_HELP" var="varHelp"/>
			 <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		<tr>
			<td bgcolor="#666666" height="1" colspan="6"></td>
		</tr>
		<tr>
			<td width="798" height="100" valign="top" colspan="6">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">  
			<!-- Custom tag lib code modified for JBOSS migration changes -->	
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtDownloadInoviceDetails:message key="LBL_VENDOR_NAME"/> :</td> 
                        <td style="padding-left: 5px;">
	                        <gmjsp:dropdown controlName="vendorId"	SFFormName="frmDownload" SFSeletedValue="vendorId"
								SFValue="alVendorList" defaultValue="[Choose One]" codeId="ID" codeName="NAME" />
	                    </td> 
                         <td class="RightTableCaption" align="right" HEIGHT="24"> &nbsp;<fmtDownloadInoviceDetails:message key="LBL_PO_NUMBER"/> :</td> 
				                     <td>&nbsp;<html:text property="poNumber"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;
			    		             </td>
                    </tr>       
                    <tr><td colspan="6" class="ELine"></td></tr> 
			                     <tr>
			                        <td class="RightTableCaption" align="right" HEIGHT="24" style= "width:15%;"></font>&nbsp;<fmtDownloadInoviceDetails:message key="LBL_INVOICE_DATE_FROM"/> :</td> 
				                     <td style= "width:38%;">&nbsp;
				                     <gmjsp:calendar SFFormName="frmDownload" controlName="invoiceDateFrom"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
				                   	&nbsp;&nbsp;	<fmtDownloadInoviceDetails:message key="LBL_TO"/>:
						             <gmjsp:calendar SFFormName="frmDownload" controlName="invoiceDateTo"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
							          </td>
			    		             <td class="RightTableCaption" align="right" HEIGHT="24"> &nbsp;<fmtDownloadInoviceDetails:message key="LBL_INVOICE_NUMBER"/> :</td> 
				                     <td>&nbsp;<html:text property="invoiceNumber"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;&nbsp;
			    		             <fmtDownloadInoviceDetails:message key="BTN_LOAD" var="varLoad"/>
			    		             <gmjsp:button value="${varLoad}" gmClass="button" buttonType="Load"  onClick="fnReload();" /> </td>
			                    </tr>
                     
		<tr>
			<td class="Line" height="1" colspan="6"></td>
		</tr>
		<%
			if (rowsize > 0) {
		%>
		 <tr>
  			<td colspan="6" align="left"  ><input type="checkbox" name="Chk_selectAll" onClick="fnSelectAll();"><fmtDownloadInoviceDetails:message key="LBL_SELECT_ALL"/> </td>
 		</tr> 
 		<%
			}
		%>  
		  
		<tr>
			<td colspan="6">
			<display:table name="requestScope.frmDownload.returnList" requestURI="/gmACDownload.do?method=loadInvoiceBatch" export= "true" class="its" id="currentRowObject" varTotals="totals"  decorator="com.globus.accounts.displaytag.beans.DTDownloadInvoiceWrapper" >
                <display:setProperty name="export.excel.filename" value="Download Invoice Details.xls" />  
				<display:column property="PAYMENTID" title=""   />
 		 		<fmtDownloadInoviceDetails:message key="LBL_SAGE_VENDOR_ID" var="varSageVendorId"/><display:column property="SAGE_VENDOR_ID" title="${varSageVendorId}"   />
				<fmtDownloadInoviceDetails:message key="LBL_VENDOR_NAME" var="varVendorName"/><display:column property="VENDOR_NAME" title="${varVendorName}"  class="alignleft" /> 
				<fmtDownloadInoviceDetails:message key="LBL_INVOICE_NUMBER" var="varInoviceNumber"/><display:column property="INVOICE_NUMBER" title="${varInoviceNumber}"  class="alignleft" />
				<fmtDownloadInoviceDetails:message key="LBL_INVOICE_DATE" var="varInvoiceDate"/><display:column property="INVOICE_DT" title="${varInoviceNumber}"  class="alignleft" /> 
				<fmtDownloadInoviceDetails:message key="LBL_INOVICE_AMT" var="varInvoiceAmt"/><display:column property="INVOICE_AMOUNT" title="${varInvoiceAmt}."  class="alignright" format="{0,number,$#,###,###.00}" total="true"/> 
				
				
			<display:footer media="html"> 
					<%
					String strVal ; 
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column6").toString();
					String strTotal = " $ " +  GmCommonClass.getStringWithCommas(strVal,2);
					 				
					%>
				<tr class = shade>
		  		<td colspan="5"> <B> <fmtDownloadInoviceDetails:message key="LBL_TOTAL"/> : </B></td> 
    	    	 <td class = "alignright" > <B><%=strTotal%></B></td>
	    	 	 
  				</tr>
  			</display:footer>
				 
			</display:table>
			</td>
		</tr>
		
		
		 
		 <%
			if (rowsize > 0) {
		%> 
		 <tr>				 
							<td class="RightText"  colspan="3"  align="left" > <B><fmtDownloadInoviceDetails:message key="LBL_TOAL_SELECTED_INOVICE_AMOUNT"/>:</B>&nbsp;</td>
							<td class="RightCaption"    align="right" id="Lbl_InvcTotal">&nbsp;<B>$&nbsp;0.00 </B>&nbsp;</td>
						</tr>
		<tr>
			<td class="LLine" height="1" colspan="6"></td>
		</tr>
		 
		<tr>
		<fmtDownloadInoviceDetails:message key="BNT_ROLLBACK_DOWNLOAD" var="varRollbackDownload"/>
		<fmtDownloadInoviceDetails:message key="BTN_CREATE_BATCH_DOWNLOAD" var="varCreateBatchDownload"/>
		 <td colspan="6" align="center">&nbsp;&nbsp;    <p>&nbsp;</p>
			<logic:equal name="frmDownload" property="enableRollbackPayment" value="true">					 
	           <gmjsp:button value="${varRollbackDownload}" name ="Rollback" gmClass="button"  buttonType="Save" onClick="fnRollback();"/>&nbsp;&nbsp;&nbsp;&nbsp;
	        </logic:equal>
			<logic:equal name="frmDownload" property="enableRollbackPayment" value="false">					 
	           <gmjsp:button value="${varRollbackDownload}" disabled="true" name ="Rollback" gmClass="button" buttonType="Save"  onClick="fnRollback();"/>&nbsp;&nbsp;&nbsp;&nbsp;
	        </logic:equal>	        
	        <logic:equal name="frmDownload" property="enableDownload" value="true">   
	           <gmjsp:button value="${varCreateBatchDownload}" gmClass="button" buttonType="Save"  onClick="fnSubmit();" /> <p>&nbsp;</p>
	        </logic:equal>
	        <logic:equal name="frmDownload" property="enableDownload" value="false">   
	           <gmjsp:button value="${varCreateBatchDownload}" disabled="true" gmClass="button" buttonType="Save"  onClick="fnSubmit();" /> <p>&nbsp;</p>
	        </logic:equal>   
         </td>
		</tr>               
	 	
		<% 
			}
		%> 
		 
		
		 
	</table>
	</FORM>
	 
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>