<%
/**********************************************************************************
 * File		 		: GmManualAdjTxn.jsp
 * Version	 		: 1.0
 * author			: Joe Prasanna Kumar
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtManualAdjTxn" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- \accounts\ma\GmManualAdjTxn.jsp -->
<fmtManualAdjTxn:setLocale value="<%=strLocale%>"/>
<fmtManualAdjTxn:setBundle basename="properties.labels.accounts.ma.GmMAAjaxCart"/>

<bean:define id="updOprQtyDisable" name="frmManualAdjTxn" property="updOprQtyDisableFlag" type="java.lang.Boolean"> </bean:define>
<bean:define id="strDeptId" name="frmManualAdjTxn" property="deptId" type="java.lang.String"> </bean:define>
<bean:define id="strOpt" name="frmManualAdjTxn" property="strOpt" type="java.lang.String"> </bean:define>
<bean:define id="strXmlData" name="frmManualAdjTxn" property="xmlStringData" type="java.lang.String"> </bean:define>
<bean:define id="rowcnt" name="frmManualAdjTxn" property="rows" type="java.lang.String"> </bean:define>
<bean:define id="alOwnerCompany" name="frmManualAdjTxn" property="alOwnerCompany" scope="request" type="java.util.ArrayList"></bean:define>
<bean:define id="alOwnerCurrency" name="frmManualAdjTxn" property="alOwnerCurrency" scope="request" type="java.util.ArrayList"></bean:define>
<bean:define id="strOwnerCmp" name="frmManualAdjTxn" property="strOwnerCmp" type="java.lang.String"> </bean:define>
<bean:define id="strOwnerCur" name="frmManualAdjTxn" property="strOwnerCur" type="java.lang.String"> </bean:define>
<%

	String strAccountMaJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_MA");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strTotal = "";
	String strWikiTitle = GmCommonClass.getWikiTitle("MANUAL_ADJUSTMENT");
	String strCompanyId = gmDataStoreVO.getCmpid();
	int OwnerCmpSize = alOwnerCompany.size();
	int OwnerCurSize = alOwnerCurrency.size();
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Manual Adjustment Screen </TITLE>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/fonts/font_roboto/roboto.css"/>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx_deprecated.js"></script> 
<script language="JavaScript" src="<%=strAccountMaJsPath%>/GmManualAdjTxn.js"></script>
<script>
var xmlData = '<%=strXmlData%>';
var strOpt1 = '<%=strOpt%>';
var rowSize= '<%=rowcnt%>';
var deptId= '<%=strDeptId%>';
var lblMAType = '<fmtManualAdjTxn:message key="LBL_MA_TYPE"/>';
var lblMADesc = '<fmtManualAdjTxn:message key="LBL_MA_DESC"/>';
var compIdVal = '<%=strCompanyId%>';
var OwnerCmpSize = '<%=OwnerCmpSize%>';
var OwnerCurSize = '<%=OwnerCurSize%>';
//Create Array Object and initialize array values for PO dropdown
var ownerCmpArr = new Array(1);
<%=strOwnerCmp%>;
var ownerCurArr = new Array(1);
<%=strOwnerCur%>;
</script>
<style type="text/css">
.even{
	background-color:#ffffff !important;
}
.uneven{
background-color:#dcedfc !important;;
}
		
div.gridbox table.hdr td {
   color: #000000 !important;
   background-color: #d2e9fc !important;
}

div.gridbox_material.gridbox .xhdr {
	background:#dcedfc !important;
}

</style>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnLoad();">
<html:form action="/gmManualAdjTxn.do" >
<html:hidden property="strOpt" value=""/>
<html:hidden property="onChangeAction" value=""/>
<html:hidden property="hmaFromType" />
<html:hidden property="deptId" />
<html:hidden property="haction" />
<html:hidden property="updOprQtyDisableFlag" />
<html:hidden property="hinputStr" value = ""/>

	<table border="0"  class="border" width="1100" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>
		<tr>
			<td colspan="4" height="25">
			    <table border="0" width="100%" cellspacing="0" cellpadding="0"><tr>
					<td height="25" class="RightDashBoardHeader" width="1100" align="left"><fmtManualAdjTxn:message key="LBL_MANUAL_ADJUSTMENT"/> </td>
					<td height="25" class=RightDashBoardHeader align="right"  ><fmtManualAdjTxn:message key="IMG_HELP" var="varHelp"/><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' 
					                  onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td></tr>
				</table>
			</td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td height="25" class="RightTableCaption" align="right"><font color="red">*</font><fmtManualAdjTxn:message key="LBL_MA_TYPE"/> :</td>
			<td >&nbsp;<gmjsp:dropdown controlName="maTypeId" SFFormName="frmManualAdjTxn" SFSeletedValue="maTypeId"
							SFValue="alMAType" codeId = "CODEID" tabIndex="1" codeName = "CODENM" onChange="fnFetch();"  defaultValue= "[Choose One]" />													
			</td>
			<td height="25" class="RightTableCaption" align="right"><fmtManualAdjTxn:message key="LBL_DATE_CREATED"/>:</td>
			<td>&nbsp;<bean:write name="frmManualAdjTxn" property="createdDate"/>
			</td>
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>	
		<tr>
			<td height="25" class="RightTableCaption" align="right"><font color="red">*</font> <fmtManualAdjTxn:message key="LBL_MA_DESC"/>:</td>
			<td>&nbsp;<html:text property="maDesc"  size="50" tabindex="2" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
    		</td>
    		<logic:equal name="frmManualAdjTxn" property="deptId" value="2022">
				<td class="RightTableCaption" align="right"><fmtManualAdjTxn:message key="LBL_UPDATE_OPERATION_QTY"/>:</td>
				
				<!-- 
				We need to update the MA process for accounting so that they do not have to affect operations qty (MNTTASK-2504) 
				<td >&nbsp;<html:checkbox property="updOprQty" disabled="<%=updOprQtyDisable.booleanValue()%>" /> --> 	
				<td >&nbsp;<html:checkbox property="updOprQty" disabled="true" /> 
				</td>
			</logic:equal>
		</tr>
		</html:form>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr> 
      <%if(strOpt.equals("loadGrid")){ %>
		<tr>
			<td class="RightTableCaption" align="left" colspan="4" height="23"><table>
				<tr>
					<td width="20"><a href="#"><fmtManualAdjTxn:message key="IMG_COPY" var="varcopy"/><img src="<%=strImagePath%>/dhtmlxGrid/copy.gif" onClick="javascript:docopy()" alt="${varcopy}" style="border: none;" height="14"></a></td>
					<td width="20"><a href="#"><fmtManualAdjTxn:message key="IMG_PASTE" var="varpaste"/> <img src="<%=strImagePath%>/dhtmlxGrid/paste.gif" alt="${varpaste}" style="border: none;" onClick="javascript:pasteToGrid()" height="14"></a></td>
					<td width="20"><a href="#"><fmtManualAdjTxn:message key="IMG_REDO" var="varredo"/> <img src="<%=strImagePath%>/dhtmlxGrid/redo.gif" alt="${varredo}" onClick="javascript:doredo();" style="border: none;" height="14"></a></td>
					<td width="20"><a href="#"><fmtManualAdjTxn:message key="IMG_UNDO" var="varUndo"/> <img src="<%=strImagePath%>/dhtmlxGrid/undo.gif" alt="${varUndo}" onClick="javascript:doundo();" style="border: none;" height="14"></a></td>
					<td width="20"><a href="#"><fmtManualAdjTxn:message key="LBL_ADD_ROW" var="varAddRow"/> <img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="${varAddRow}" style="border: none;" onClick="javascript:addRow()"height="14"></a></td>
					<td width="20"><a href="#"><fmtManualAdjTxn:message key="IMG_ADD_ROWS_FROM_CLIPBOARD" var="varAddRowsFromClipboard"/> <img src="<%=strImagePath%>/dhtmlxGrid/row_add_after_1.gif" alt="${varAddRowsFromClipboard}" style="border: none;" onClick="javascript:addRowFromClipboard()" height="14"></a></td>
					<td width="20"><a href="#"><fmtManualAdjTxn:message key="IMG_REMOVE_ROW" var="varRemoveRow"/> <img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="${varRemoveRow}" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a></td>
				</tr></table>
				</td>

		</tr>
		<tr>
			<td colspan="4" height="17"><font color="#6699FF">&nbsp;<fmtManualAdjTxn:message key="LBL_MAX_ROWS_TO_PASTE_FROM_EXCEL"/></font></td>
		</tr>
		<tr>
			<td colspan="4">
				<div id="div_manualAdjGrid" style="width:1250px;height:230px;"></div>	
			</td>
		</tr>
		<%}else{ %>
		
		<tr>
			<td colspan="4" align="center" height="30">&nbsp;
               	<jsp:include page="/accounts/ma/GmMAAjaxCart.jsp" >
               	<jsp:param name="DEPTID" value="<%=strDeptId%>" />
				</jsp:include>
			</td>
		</tr>
		<%}%>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		
		 <%if(strOpt.equals("loadGrid")){ %>
		 <tr class="shade" height="23" >
			<td class="RightText" colspan="3" align="right" ><B><fmtManualAdjTxn:message key="LBL_TOTAL_PRICE"/>:</B>&nbsp;</td>
			<td class="RightCaption" colspan="1" align="right" id="Lbl_Grid_Total"><B><%=GmCommonClass.getStringWithCommas(strTotal)%></B>&nbsp;</td>
		</tr>
		<tr><td colspan="4" class="Line" height="1"></td></tr>
		<tr> <td colspan="3" align="center" height="30">
			 <fmtManualAdjTxn:message key="BTN_FETCH_COST" var="varFetchCost"/><gmjsp:button value="${varFetchCost}" gmClass="button" onClick="fnFetchCost();" buttonType="Load" />&nbsp;&nbsp;&nbsp;
			 <fmtManualAdjTxn:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnSubmit();" />
			</td>
		</tr>
	
		<%}else{%>
		<tr>
			<td colspan="4" align="center">  <fmtManualAdjTxn:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnSubmit();" /> </td>
		</tr> <%}%>
		
	</table>

<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>