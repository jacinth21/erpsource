<%
/**********************************************************************************
 * File		 		: GmMADetail.jsp
 * Desc		 		: Report for MA Detail
 * Version	 		: 1.0
 * author			: Rakhi G
************************************************************************************/
%>
<!-- Custom tag lib code modified for JBOSS migration changes -->
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib uri="/WEB-INF/struts-logic-el.tld" prefix="logicel" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ taglib prefix="fmtMADetail" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \accounts\ma\GmMADetail.jsp -->
<fmtMADetail:setLocale value="<%=strLocale%>"/>
<fmtMADetail:setBundle basename="properties.labels.accounts.ma.GmMAAjaxCart"/>

<bean:define id="strOpt" name="frmManualAdjTxn" property="strOpt" type="java.lang.String"> </bean:define>

<%
// Set to expire far in the past.
response.setHeader("Expires", "Sat, 6 May 1995 12:00:00 GMT");
// Set standard HTTP/1.1 no-cache headers.
response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
// Set IE extended HTTP/1.1 no-cache headers (use addHeader).
response.addHeader("Cache-Control", "post-check=0, pre-check=0");
// Set standard HTTP/1.0 no-cache header.
response.setHeader("Pragma", "no-cache");

HashMap hmCurrency = new HashMap();
hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
String strCurrSign =(String)hmCurrency.get("CMPCURRSMB");
String strApplCurrFmt = (String)hmCurrency.get("CMPCURRFMT");
String strCurrFmt = "{0,number,"+" "+strApplCurrFmt+"}";
%>


<HTML>
<HEAD>
<TITLE> Globus Medical: MA Detail </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>


<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>

function fnPrint()
{
	window.print();
}

var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}

function formKeyValidate() {
// keycode for F5 function
if (window.event && window.event.keyCode == 116) {

window.event.cancelBubble = true;
window.event.returnValue = false;
cancelRefresh();
}
}

function DisablingBackFunctionality()
{
var URL;
var i ;
var QryStrValue;
URL=window.location.href ;
i=URL.indexOf("?");
QryStrValue=URL.substring(i+1);
if (QryStrValue!='X')
{
window.location=URL + "?X";
}
}

function fnLoad(){
	document.frmManualAdjTxn.strOpt.value = "report";
	document.frmManualAdjTxn.action = "/gmMADetail.do?method=reportMADetail"
	fnStartProgress("Y");
	document.frmManualAdjTxn.submit();
}
// javascript:window.history.forward(-1);


function fnRedirect(){
	var varOpt = document.frmManualAdjTxn.strOpt.value;
	if (varOpt == 'save'){
		setTimeout('afterFiveSeconds()',5000)
	}
}

function afterFiveSeconds(){
	var hAction = document.frmManualAdjTxn.haction.value;
	if(hAction == 'loadGrid'){
		window.location.href = "/gmManualAdjTxn.do?strOpt="+hAction;
	}else{
		window.location.href = "/gmManualAdjTxn.do";
	}

}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10"  onKeyDown="formKeyValidate()" onLoad="fnRedirect()"> 

<html:form action="/gmMADetail.do"  >
<html:hidden property="strOpt"/> 
<html:hidden property="haction"/> 

<!-- --> <table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
<!--  -->	<tr> 
				<td height="25" class="RightDashBoardHeader"><fmtMADetail:message key="LBL_MA_DETAIL"/></td> 
			</tr>
		
<!--  -->	<tr>
			  <td height="100" valign="top">
				
<!-- i -->		<table border="0" cellspacing="0" cellpadding="0" >
<!-- i1 -->		<tr>
	            	<td class="RightTableCaption" align="right"  height="30" nowrap ><fmtMADetail:message key="LBL_MA_ID"/>:&nbsp;</td>
	            	<td> 
<% if(strOpt.equals("FromMenu")){ %>
	            		<html:text property="maID"  size="22" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
	            		&nbsp;&nbsp;<fmtMADetail:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" name="Btn_Load" gmClass="button" onClick="fnLoad();" buttonType="Load" />
<%} else { %>
	            		<bean:write name="frmManualAdjTxn" property="maID" />
<%}%>	            		 
	            	</td>
	            	
	                 <td class="RightTableCaption"  height="30" align="right" nowrap><fmtMADetail:message key="LBL_MA_DESC"/> :&nbsp;</td>
   					<td> <bean:write name="frmManualAdjTxn" property="maDesc" /></td>
				</tr>
<!-- i2 -->		<tr><td colspan="4" class="LLine"></td></tr>
<!-- i3 -->		<tr class="oddshade">
					<td class="RightTableCaption"  height="30"  align="right"  nowrap><fmtMADetail:message key="LBL_MA_TYPE"/>:&nbsp;</td>
    	            <td>  <bean:write name="frmManualAdjTxn" property="maTypeId" /> </td>
    	            
    	            <td class="RightTableCaption"  height="30"  align="right" nowrap  ><fmtMADetail:message key="LBL_OPER_UPD_FL"/>:&nbsp;</td>
    	            <td >  <bean:write name="frmManualAdjTxn" property="updOprQty" /> </td>
    	            
				</tr>
<!-- i4 -->     <tr><td colspan="4" class="LLine"></td></tr>
<!-- i5 -->		<tr>
                    <td class="RightTableCaption" HEIGHT="24" align="right"><fmtMADetail:message key="LBL_CREATED_DT"/>:&nbsp;</td>                     
                    <td> <bean:write name="frmManualAdjTxn" property="createdDate" /> </td>
   		            <td class="RightTableCaption" HEIGHT="24" align="right">&nbsp;<fmtMADetail:message key="LBL_CREATED_BY"/>:&nbsp;</td>
                    <td><bean:write name="frmManualAdjTxn" property="createdBy" />   </td>
    		     </tr>
    		     
<!-- i6 -->      <tr><td colspan="4" class="Line"></td></tr>
<!-- i7 -->      <tr>
					<td colspan="4">           

					<!--  display:Table tag here --> 
					<display:table cellspacing="0" cellpadding="0" class="its" name="requestScope.frmManualAdjTxn.ldtResult"  varTotals="totals" export="true"   decorator="com.globus.displaytag.beans.DTMADetailsWrapper"   requestURI="/gmMADetail.do?method=reportMADetail"  >				    																		     							
					<display:setProperty name="export.excel.filename" value="gmMADetail.xls" />
					<fmtMADetail:message key="DT_PART" var="varPart"/><display:column property="PNUM" title="${varPart}" style="text-align:left"  sortable="true"   />								
					<fmtMADetail:message key="LBL_PART_DESCRIPTION" var="varPartDescription"/><display:column property="PARTDESC" title="${varPartDescription}"     maxLength="80" sortable="true" />
					<fmtMADetail:message key="DT_OWNER_COMPANY" var="varOwnerCompany"/><display:column property="OWNER_CD" title="${varOwnerCompany}"  />
					<fmtMADetail:message key="DT_OWNER_CURRENCY" var="varOwnerCurrency"/><display:column property="OWNER_CURR" title="${varOwnerCurrency}"  />
					<fmtMADetail:message key="DT_QTY" var="varQty"/><display:column property="MAQTY" title="${varQty}" style="text-align:right" />					
					<fmtMADetail:message key="LBL_COST_EA" var="varCostEa"/><display:column property="MAPRICE" title="${varCostEa}" class="alignright" format="<%=strCurrFmt %>"  />
					<fmtMADetail:message key="LBL_EXT_COST" var="varExtCost"/><display:column property="EXTCOST" title="${varExtCost}" class="alignright"/>
					<fmtMADetail:message key="DT_LOCAL_CURRENCY" var="varLocalCurrency"/><display:column property="LOCAL_CURR" title="${varLocalCurrency}" class="aligncenter"/>
					<fmtMADetail:message key="DT_LOCAL_COST" var="varLocalCost"/><display:column property="LOCAL_COST" title="${varLocalCost}" format="<%=strCurrFmt%>" style="text-align:right"  />
					<fmtMADetail:message key="DT_LOCAL_EXT_COST" var="varLocalExtCost"/><display:column property="LOCAL_EXT_COST" title="${varLocalExtCost}" format="<%=strCurrFmt%>" style="text-align:right" total="true" />
					<display:footer media="html">
					<% 
							String strVal ;
							strVal = ((HashMap)pageContext.getAttribute("totals")).get("column10").toString();
						
					  		//String strCurVal = " $ " + GmCommonClass.getStringWithCommas(strVal); //before red total
					  		
					  		String strCurVal = GmCommonClass.getRedText( GmCommonClass.getStringWithCommas(strVal)  ); 
					  	
								
					%>
					<tr><td colspan="10" class="Line"></td></tr>
	  				<tr class = "shade">
			  			<td colspan="9" class = "alignright"> <B> <fmtMADetail:message key="LBL_TOTAL_COST"/>: </B></td>
			  			<td class = "alignright" >&nbsp;<B><%=strCurVal%></B></td>
	  				</tr>
	 				</display:footer>
					</display:table>		

					</td></tr>		
				 <tr><td colspan="4" class="Line"></td></tr>
			</table>
																
<!--  -->		</td>
<!-- o2 -->	</tr>
	
<!-- o3  --><tr>
				<td align="center" height="30" id="button">
				<logic:notEqual  name="frmManualAdjTxn" property="strOpt" value="FromMenu">
					<fmtMADetail:message key="BTN_PRINT" var="varPrint"/><gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				</logic:notEqual>				
				</td>
				<tr>
				
				<tr>
				<td align="center" height="30" >
				<logic:equal  name="frmManualAdjTxn" property="strOpt" value="save">
					<B> <font color="red"> <i> <fmtMADetail:message key="LBL_REDIRECT_BACK_TO_MA_TRANSACTION_SCREEN"/> </B> </font> </i>
				</logic:equal>				
				</td>
				<tr>
	
	
</table>




			   	
</html:form>

</BODY>
<%@ include file="/common/GmFooter.inc"%>
</HTML>

