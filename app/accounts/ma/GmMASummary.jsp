<%
/**********************************************************************************
 * File		 		: GmMASummary.jsp
 * Desc		 		: Report for MA Summary
 * Version	 		: 1.0
 * author			: Rakhi G
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ taglib prefix="fmtMASummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \accounts\ma\GmMASummary.jsp -->
<fmtMASummary:setLocale value="<%=strLocale%>"/>
<fmtMASummary:setBundle basename="properties.labels.accounts.ma.GmMAAjaxCart"/>


<HTML>
<HEAD>
<TITLE> Globus Medical: MA Summary </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>


<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<%
HashMap hmCurrency = new HashMap();
hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
String strCurrSign =(String)hmCurrency.get("CMPCURRSMB");
String strApplCurrFmt = (String)hmCurrency.get("CMPCURRFMT");
	String strCurrFmt = "{0,number,"+" "+strApplCurrFmt+"}";
	 %>
<script>

function fnSubmit()
{
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmMASummary.strOpt.value = "Report";
	fnStartProgress();
	document.frmMASummary.submit();
}


function fnReport()
{
	//fnValidateDropDn('typeId','MA Type');
	//fnValidateTxtFld('maID',' MA ID ');
	//DateValidate(document.frmMASummary.fromDate,message[1]);
	//DateValidate(document.frmMASummary.toDate,message[1]);
		
	if( TRIM( eval(document.all.maID).value ) == ''  && 
	    TRIM( eval(document.all.fromDate).value ) == '' &&
	    TRIM( eval(document.all.toDate).value ) == '' && 
	    TRIM( eval(document.all.partNumber).value ) == '' && 
	    (eval(document.all.typeId)).value == '0'  )	
		Error_Details(message_accounts[500]);
		
	document.frmMASummary.action = "/gmMASummary.do?method=reportMASummary";
	fnSubmit();
}	

function fnPrint(){
	window.print();
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" > <html:form action="/gmMASummary.do"  > <html:hidden property="strOpt" />
<table border="0" class="DtTable1200" cellspacing="0" cellpadding="0">
	<tr> 
		<td height="25" class="RightDashBoardHeader"><fmtMASummary:message key="LBL_MA_SUMMARY"/></td> </tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td height="100" valign="top">
				<table border="0" cellspacing="0" cellpadding="0" >
				<tr>
	            	<td class="RightTableCaption" align="right"  height="30" nowrap >&nbsp;<fmtMASummary:message key="LBL_MA"/>:</td>
	            	<td>&nbsp;
    		        	<html:text property="maID"  size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
    		         </td>
	                 <td class="RightTableCaption"  height="30" align="right" nowrap><fmtMASummary:message key="LBL_MA_TYPE"/> :</td>
   					<td>&nbsp;
						<gmjsp:dropdown controlName="typeId" SFFormName="frmMASummary" SFSeletedValue="typeId"
						SFValue="alType" codeId = "CODEID"  codeName = "CODENM"   defaultValue= "[Choose One]" />													
					 </td>
				</tr>
				<tr><td colspan="4" class="LLine"></td></tr>
				<tr class="Shade">
					<td class="RightTableCaption"  height="30"  align="right"  nowrap>&nbsp;<fmtMASummary:message key="LBL_PART_NUM"/> :</td> 
    	            <td colspan="3">&nbsp;
    		        	<html:text property="partNumber"  size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
    		        </td>
				</tr>
                  <tr><td colspan="4" class="LLine"></td></tr>
				<tr>
                  <td class="RightTableCaption" HEIGHT="24" align="right">&nbsp;<fmtMASummary:message key="LBL_FROM_DATE"/>:</td> 
                  <td>&nbsp;
						<gmjsp:calendar SFFormName="frmMASummary"
					 controlName="fromDate" 
					 gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
					 onBlur="changeBgColor(this,'#ffffff');"/>	&nbsp;&nbsp; 
    		        </td>
   		            <td class="RightTableCaption" HEIGHT="24" align="right">&nbsp;<fmtMASummary:message key="LBL_TO_DATE"/>:</td> 
                    <td>&nbsp;
						<gmjsp:calendar SFFormName="frmMASummary"
					 controlName="toDate" 
					 gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
					 onBlur="changeBgColor(this,'#ffffff');"/>	&nbsp;&nbsp; 
    		             <fmtMASummary:message key="BTN_GO" var="varGo"/><gmjsp:button value="&nbsp;${varGo}&nbsp;" gmClass="button" onClick="fnReport();" buttonType="Load" />&nbsp;&nbsp;
    		             <fmtMASummary:message key="BTN_PRINT" var="varPrint"/><gmjsp:button value="${varPrint}" gmClass="button" buttonType="Load" onClick="fnPrint();" />
    		        </td></tr>
                    <tr><td colspan="4" class="ELine"></td></tr>
        <tr><td colspan="4">           
		<!--  display:Table tag here --> 
		<display:table cellspacing="0" cellpadding="0" class="its" name="requestScope.frmMASummary.ldtResult" export="true" requestURI="/gmMASummary.do?method=reportMASummary"  >	
        <display:setProperty name="export.excel.filename" value="MA Summary Report.xls" />        
		<fmtMASummary:message key="LBL_MA_ID" var="varMAID"/><display:column property="MAID" title="${varMAID}"   sortable="true"   href="gmMADetail.do?method=reportMADetail"  paramId="maID"  />	<!-- form should have field MAID - what paramId has. -->																	
		<fmtMASummary:message key="DT_PART" var="varPart"/><display:column property="PNUM" title="${varPart}" style="text-align:left"    sortable="true" />								
		<fmtMASummary:message key="DT_MA_DESCRIPTION" var="varMADescription"/><display:column property="MADESC" title="${varMADescription}"     maxLength="25" />
		<fmtMASummary:message key="LBL_MA_TYPE" var="varMAType"/><display:column property="MATYPE" title="${varMAType}"    sortable="true" maxLength="25" />		
		<fmtMASummary:message key="DT_CREDIT_ACCOUNT" var="varCreditAccount"/><display:column property="CREDITACCT" title="${varCreditAccount}"  maxLength="25"  />
		<fmtMASummary:message key="DT_DEBIT_ACCOUNT" var="varDebitAccount"/><display:column property="DEBITACCT" title="${varDebitAccount}"   maxLength="25"   />
		<fmtMASummary:message key="DT_CREATED_DATE" var="varCreatedDate"/><display:column property="CREATEDDATE" title="${varCreatedDate}"   />
		<fmtMASummary:message key="DT_CREATED_BY" var="varCreatedBy"/><display:column property="CREATEDBY" title="${varCreatedBy}"  />
		<fmtMASummary:message key="DT_OWNER_COMPANY" var="varOwnerCompany"/><display:column property="OWNER_CD" title="${varOwnerCompany}"  />
		<fmtMASummary:message key="DT_OWNER_CURRENCY" var="varOwnerCurrency"/><display:column property="OWNER_CURR" title="${varOwnerCurrency}"  />
		<fmtMASummary:message key="DT_MA_QTY" var="varMAQty"/><display:column property="MAQTY" title="${varMAQty}" style="text-align:right"   />
		<fmtMASummary:message key="DT_MA_COST" var="varMACost"/><display:column property="MAPRICE" title="${varMACost}" format="<%=strCurrFmt%>" style="text-align:right;" />
		<fmtMASummary:message key="DT_LOCAL_CURRENCY" var="varLocalCurrency"/><display:column property="LOCAL_CURR" title="${varLocalCurrency}" class="aligncenter"/>
		<fmtMASummary:message key="DT_LOCAL_COST" var="varLocalCost"/><display:column property="LOCAL_COST" title="${varLocalCost}" format="<%=strCurrFmt%>" style="text-align:right"/>
		
		</display:table>
		
		</td></tr>		
</table>										
</td></tr></table>			   	
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>

