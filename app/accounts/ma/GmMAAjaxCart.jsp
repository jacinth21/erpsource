 <%
/**********************************************************************************
 * File		 		: GmMAAjaxCart.jsp
 * Version	 		: 1.0
 * author			: Joe Prasanna Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtMAAjaxCart" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \accounts\ma\GmMAAjaxCart.jsp -->
<fmtMAAjaxCart:setLocale value="<%=strLocale%>"/>
<fmtMAAjaxCart:setBundle basename="properties.labels.accounts.ma.GmMAAjaxCart"/>



<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	int intTRCount = 10;
	int intSize = 0;
	int intAlSize = 0;
	HashMap hcboVal = new HashMap();
	String strCodeID = "";
	String strSelected = "";
	String strPartOrdType = "";
	String strTotal = "";
	String strReadOnly = "readonly";
	String strDisabled = "disabled";
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	ArrayList alOrderPartType = new ArrayList();
	ArrayList alReturn = (ArrayList)request.getAttribute("alReturn");
	ArrayList alLoop = new ArrayList();
	String strDeptId = GmCommonClass.parseNull(request.getParameter("DEPTID"));
	ArrayList alOwnerCompany = GmCommonClass.parseNullArrayList( (ArrayList)request.getAttribute("ALOWNERCOMPANY"));
	ArrayList alOwnerCurrency = GmCommonClass.parseNullArrayList( (ArrayList)request.getAttribute("ALOWNERCURRENCY"));
	String strOwnerCompanyStr = "<option value=0 id=0 >[Choose One]</option>";
	String strOwnerCurrencyStr = "<option value=0 id=0 >[Choose One]</option>";
	HashMap hmTemp = new HashMap ();
	//PC-1002 - Auto Populate Company and Currency Info on Manual Adjusment
	HashMap hmTempCompany = new HashMap ();
	
	for (int i=0;i<alOwnerCompany.size();i++)
		{
	  hmTemp = (HashMap)alOwnerCompany.get(i);
	  // PC-1002 - Auto Populate Company and Currency Info on Manual Adjusment
	  // to add company id is key and currency id is value in hashmap
	  hmTempCompany.put((String)hmTemp.get("COMPANYID"),(String)hmTemp.get("CURRENCYID"));
		strOwnerCompanyStr = strOwnerCompanyStr + "<option value='"+(String)hmTemp.get("COMPANYID") +"'>"+(String)hmTemp.get("COMPANYNM") +"</option>";
	}
		
		hmTemp = new HashMap();
		for (int i=0;i<alOwnerCurrency.size();i++)
		{
	  hmTemp = (HashMap)alOwnerCurrency.get(i);
	  strOwnerCurrencyStr = strOwnerCurrencyStr + "<option value='"+(String)hmTemp.get("CODEID") +"'>"+(String)hmTemp.get("CODENMALT") +"</option>";
	}
	
	if(strDeptId.equals("2022"))
	{
		strReadOnly = "";	
		strDisabled = "";
	}	
	log.debug(" deptid " + strDeptId + " strReadONly " + strReadOnly);
	
	if (hmReturn != null)
	{
		alOrderPartType = (ArrayList)hmReturn.get("ORDPARTTP");
	}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: MA Ajax Cart</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/macart.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
var req;
var trcnt = 0;
var cnt = <%=intTRCount%>;
//PC-1002 - Auto Populate Company and Currency Info on Manual Adjusment
// string value convert to hashmap
var company = '<%=hmTempCompany.toString()%>';
var companyIdObject = {};
var strComapny = company.substring(1,company.length-1);
strComapny = strComapny.split(",")
for(i = 0; i< strComapny.length;i++){
	companyIdObject[strComapny[i].split('=')[0].trim()] = strComapny[i].split('=')[1].trim();
}

function fnInventoryDetails(varIndex){
	objpnum =  eval("document.frmCart.Txt_PNum"+varIndex);
	varpnum = objpnum.value; 
	windowOpener("/GmPartInvReportServlet?hAction=Go&hOpt=MultipleParts&hPartNum="+encodeURIComponent(varpnum),"PO2","resizable=yes,scrollbars=yes,top=40,left=50,width=850,height=200");
}

</script>
</HEAD>
<body>
<FORM name="frmCart" method="POST" action="<%=strServletPath%>/GmCommonCartServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hRowCnt" value="<%=intTRCount%>">
<input type="hidden" name="hTotal" value="">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hPartNumStr" value="">
<input type="hidden" name="hDeptID" value="<%=strDeptId%>">

	<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<TR>
			<td colspan="2">
			<div style="overflow:auto; height:252px;">
			<table cellpadding="0" cellspacing="0" width="100%" border="0" bordercolor="gainsboro" id="PartnPricing">
			  <thead>
				<TR bgcolor="#EEEEEE" class="RightTableCaption" style="position:relative; top:expression(this.offsetParent.scrollTop);height: 35px;">
					<TH class="RightText" width="10" align="center">#</TH>
					<TH class="RightText" width="20" align="left"><a tabindex="-1"><fmtMAAjaxCart:message key="IMG_INVENTORY_DETAILS" var="varInventorydetails"/><img border="0" Alt='${varInventorydetails}' valign="center" src="<%=strImagePath%>/product_icon.gif" height="10" width="9"></a></TH>
					<TH class="RightText" width="20" align="left"><a tabindex="-1"><fmtMAAjaxCart:message key="IMG_CLEAR_CART" var="varClearCart"/><img border="0" Alt='${varClearCart}' valign="center" src="<%=strImagePath%>/btn_remove.gif" height="10" width="9"></a></TH>
					<TH class="RightText" width="70" align="center"><fmtMAAjaxCart:message key="LBL_PART_NUMBER"/></TH>
					<TH class="RightText" width="50" align="center"><fmtMAAjaxCart:message key="LBL_MA_QTY"/></TH>
					<TH class="RightText" width="50" align="center"><fmtMAAjaxCart:message key="LBL_ACC_QOH"/></TH>
					<TH class="RightText" width="50" align="center"><fmtMAAjaxCart:message key="LBL_OPR_QOH"/></TH>
					<TH class="RightText" width="330" >&nbsp;<fmtMAAjaxCart:message key="LBL_PART_DESCRIPTION"/></TH>
					<TH class="RightText" width="100" align="left" >&nbsp;<fmtMAAjaxCart:message key="LBL_OWNER_COMPANY"/>
					 <!-- 
					 PC-1002 - Auto Populate Company and Currency Info on Manual Adjusment 
					 Adding company dropdown in table header
					 -->
						<select name="cbo_owner_company" id="cbo_owner_company" class="RightText" onChange="fnChangeOwnerCompSelectAll();"><%=strOwnerCompanyStr %></select>
					</TH>
					<TH class="RightText" width="100" align="left">&nbsp;<fmtMAAjaxCart:message key="LBL_OWNER_CURRENCY"/></TH>
					<TH class="RightText" width="60" align="center"><fmtMAAjaxCart:message key="LBL_COST_EA"/></TH>
					<TH class="RightText" width="80" align="center"><fmtMAAjaxCart:message key="LBL_EXT_COST"/></TH>
					<TH class="RightText" width="80" align="center"><fmtMAAjaxCart:message key="LBL_LOCAL_COST"/></TH>
					<TH class="RightText" width="80" align="center"><fmtMAAjaxCart:message key="LBL_LOCAL_EXT_COST"/></TH>
				</TR>	
				<tr><th class="Line" height="1" colspan="14"></th></tr>
			  </thead>
			  <TBODY>
<%
				String strPartNum = "";
				String strQty = "";
				String strPartDesc = "";
				String strBOFlag = "";
				String strParttype = "";
				String strCapFl = "";
				String strPrice = "";
				String strExtPrice = "";
				String strStock = "";
				String strGroupId = "";
				String strCapSelected = "";
				String strOwnerCompany = "";
				String strOwnerCurrency = "";
				String strLocalCost = "";
				String strLocalExtCost = "";
				
				double dbPrice = 0.0;
				double dbExtPrice = 0.0;
				double dbTotal = 0.0;
				int intQty = 0;
				
				if (alReturn != null)
				{
					intAlSize = alReturn.size();
				}
				
				for (int i=0; i < intTRCount;i++)
				{
					if (i<intAlSize)
					{
						alLoop = (ArrayList)alReturn.get(i);
						strPartNum = (String)alLoop.get(0);
						strQty = (String)alLoop.get(1);
						strPrice = (String)alLoop.get(5);
						strGroupId = (String)alLoop.get(6);
						strCapSelected = strGroupId.equals("")?"":"checked";
						strStock = (String)alLoop.get(8) + "&nbsp;&nbsp;";
						strPartDesc = "&nbsp;".concat(GmCommonClass.getStringWithTM((String)alLoop.get(7)));
						intQty = Integer.parseInt(strQty);
						dbPrice = Double.parseDouble(strPrice);
						dbExtPrice = dbPrice * intQty;
						dbTotal = dbTotal + dbExtPrice;
						strPrice = GmCommonClass.getStringWithCommas(strPrice);
						strExtPrice = GmCommonClass.getStringWithCommas(""+dbExtPrice).concat("&nbsp;");
					}
					else
					{
						strPartNum = "";
						strPartDesc = "";
						strPrice = "";
						strQty = "";
						strExtPrice = "";
						strStock = "";
						strGroupId = "";
						strCapSelected = "";
					}
					alLoop = null;

%>
				<tr>
					<td class="RightText"><%=i+1%></td>
					<td class="RightText"><a href="javascript:fnInventoryDetails('<%=i%>');" tabindex="-1"><fmtMAAjaxCart:message key="IMG_PART_INVENTORY_DETAILS" var="varPartInventorydetails"/><img border="0" Alt='${varPartInventorydetails}' valign="left" src="<%=strImagePath%>/product_icon.gif" height="10" width="9"></a></td>
					<td class="RightText"><a href="javascript:fnRemoveItem('<%=i%>');" tabindex="-1"><fmtMAAjaxCart:message key="IMG_REMOVE_FROM_CART" var="varRemovefromcart"/><img border="0" Alt='${varRemovefromcart}' valign="left" src="<%=strImagePath%>/btn_remove.gif" height="10" width="9"></a></td>
					<td><input type="text" size="7" class=InputArea value="<%=strPartNum%>" name="Txt_PNum<%=i%>" onblur="changeBgColor(this,'#ffffff');" onchange="fnSetPartSearch('<%=i%>',this);"; onFocus="changeBgColor(this,'#AACCE8');">
					<input type="hidden" name="hLocalCurrency<%=i%>" value="">
					<input type="hidden" name="hOwnerId<%=i%>" value=""></td>
					<td align="center"><input type="text" size="3" value="<%=strQty%>" class=InputArea name="Txt_Qty<%=i%>" onblur="validate(<%=i%>,this);" onFocus="changeBgColor(this,'#AACCE8');"></td>
					<td id="Lbl_Stock<%=i%>" class="RightText" align="right"><%=strStock%> </td><input type="hidden" name="hStock<%=i%>" value=""> 
					<td id="Lbl_OprStock<%=i%>" class="RightText" align="right"> </td><input type="hidden" name="hOprQty<%=i%>" value=""> 
					<td id="Lbl_Desc<%=i%>" class="RightText"><%=strPartDesc%></td>
					<td class="RightText"><select name="cbo_owner_company<%=i%>" id="cbo_owner_company<%=i%>" class="RightText" onChange="fnChangeOwnerComp('cbo_owner_company', <%=i%>);"><%=strOwnerCompanyStr %></select></td>
					<td class="RightText"><select name="cbo_owner_currency<%=i%>" id="cbo_owner_currency<%=i%>" class="RightText"><%=strOwnerCurrencyStr %></select></td>
					<td id="Lbl_Price<%=i%>" class="RightText" align="left"><input type="text" <%=strReadOnly%> <%=strDisabled%> size="8" value="<%=strPrice%>" tabindex="-1" class=InputArea name="Txt_Price<%=i%>" onBlur="fnCalExtPrice(this,'<%=i%>');" onFocus="changeBgColor(this,'#AACCE8');" tabindex="-1"></td>
					<td id="Lbl_Amount<%=i%>" class="RightText" align="right"><%=strExtPrice%></td>
					<td id="Lbl_local_cost<%=i%>" class="RightText"><input type="text" size="7" class=InputArea value="" name="Txt_Local_Cost<%=i%>" onchange="fnChangeLocalCost(this, <%=i %>);" onFocus="changeBgColor(this,'#AACCE8');"></td>
					<td id="Lbl_local_ext_cost<%=i%>" class="RightText" align="right"><%=strLocalExtCost%></td>
				</tr>
				<!--<tr><td bgcolor="#eeeeee" height="1" colspan="11"></td></tr>-->
<%
				}
				strTotal = ""+dbTotal;
%>
			  </TBODY>
			</table>
			</div>
			</td>
		</tr>
		<tr><td colspan="2" class="Line" height="1"></td></tr>
		<tr class="shade" height="23">
			<td class="RightText" width="90%" align="right" ><B><fmtMAAjaxCart:message key="LBL_TOTAL_PRICE"/>:</B>&nbsp;</td>
			<td class="RightCaption" width="10%" align="right" id="Lbl_Total"><B><%=GmCommonClass.getStringWithCommas(strTotal)%></B>&nbsp;</td>
		</tr>
		<tr><td colspan="2" class="Line" height="1"></td></tr>
		<tr>
			<td colspan="2" align="center" height="25">
			<script>
				document.frmCart.hTotal.value = '<%=strTotal%>';
			</script>
				<fmtMAAjaxCart:message key="LBL_ADD_ROW" var="varAddRow"/><gmjsp:button value="${varAddRow}" gmClass="button" buttonType="Load" onClick="fnAddRow('PartnPricing');" tabindex="16" />&nbsp;&nbsp;
			</td>
		</tr>		
    </table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
