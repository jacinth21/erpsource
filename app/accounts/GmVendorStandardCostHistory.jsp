 <!-- \accounts\GmVendorStandardCostHistory.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ taglib prefix="fmtCostPriceLog" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtCostPriceLog:setLocale value="<%=strLocale%>"/>
<bean:define id="gridData" name="frmStandardReport" property="gridHistoryData" type="java.lang.String"> </bean:define>
<%
String strAccountsJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: History Log Information </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strAccountsJsPath%>/GmVendorStandardCostHistory.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
 function fnClose(){
	  CloseDhtmlxWindow();
	 return false; 
} 
var gridObj = '';
var objGridData = '<%=gridData%>';
 
 
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad()">

		<table border="0" class="DtTable900" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="RightDashBoardHeader">Standard Cost History Details</td>
			</tr>
		    <tr>
	            <%if(!gridData.equals("")){ %>
				<tr> 
					<td colspan="5">
						<div id="dataGridDiv" class="grid" height="200px" ></div>
					</td>
				</tr>
				<%}%>
	    
	        <tr><td class="LLine" height="1" ></td></tr>
	        <tr><td height="1" ></td>&nbsp;&nbsp;</tr>
			<tr>
			    <td align="center" >&nbsp;
			    		
	          			<gmjsp:button value="&nbsp;Close&nbsp;" gmClass="button" buttonType="Load" onClick="fnClose();" />&nbsp;
			    </td>			                				                    
		    </tr>
		    <tr><td height="1" ></td>&nbsp;&nbsp;</tr>
	    </table>

 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

