<%
/**********************************************************************************
 * File		 		: GmInvoiceDHRDtls.jsp
 * Desc		 		: Invoice - DHR Detail Screen
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>

<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ page import="java.text.DecimalFormat" %>
<%@ taglib prefix="fmtInvoiceDHRDtls" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmInvoiceDHRDtls.jsp -->
<fmtInvoiceDHRDtls:setLocale value="<%=strLocale%>"/>
<fmtInvoiceDHRDtls:setBundle basename="properties.labels.accounts.GmInvoiceDHRDtl"/>
<bean:define id="returnList" name="frmInvoiceDHR" property="returnList" type="java.util.List"></bean:define>
<%
	DecimalFormat doublef = new DecimalFormat("#,###,###.00");
	String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
	String strWikiTitle = GmCommonClass.getWikiTitle("INVOICE_DETAILS");
	String strApplDateFmt = strGCompDateFmt;
	String strDateFmt = "{0,date,"+strApplDateFmt+"}";
	HashMap hmCurrency = new HashMap();
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strCurrSign =(String)hmCurrency.get("CMPCURRSMB");
	String strApplCurrFmt = (String)hmCurrency.get("CMPCURRFMT");
	String strCurrPos = GmCommonClass.getString("CURRPOS");
	String strCurrFmt = "{0,number,"+strCurrSign+" "+strApplCurrFmt+"}";
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Download Invoice Detail</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"	media="print" />
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmInvoiceDHRDtls.js"></script>
<script>
var format = '<%=strApplDateFmt%>';
var lblInvDtFrm = '<fmtInvoiceDHRDtls:message key="LBL_INV_DT_FROM"/>';
var lblInvDtTo = '<fmtInvoiceDHRDtls:message key="LBL_INV_DT_TO"/>';
var lblDownldDtFrm = '<fmtInvoiceDHRDtls:message key="LBL_DOWNLOAD_DT_FROM"/>';
var lblDownldDtTo = '<fmtInvoiceDHRDtls:message key="LBL_DOWNLOAD_DT_TO"/>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmInvoiceDHR.do">
	<html:hidden property="strOpt" value="" /> 
	<html:hidden property="operatorSign" value=""/>
	<html:hidden property="hCloseFlag" />
	 
	<table border="0" class="DtTable900" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4"><fmtInvoiceDHRDtls:message key="LBL_INVOICE_DETAILS"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtInvoiceDHRDtls:message key="LBL_HELP" var="varHelp"/>
			 <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		<tr>
			<td bgcolor="#666666" height="1" colspan="9"></td>
		</tr>
		<tr>
			<td width="900" height="100" valign="top" colspan="9">
				<table border="0" width="100%" cellspacing="0" cellpadding="0" >  
			<!-- Custom tag lib code modified for JBOSS migration changes -->	
                    <tr HEIGHT="25">
                        <td class="RightTableCaption" align="right" HEIGHT="25" width="300"><fmtInvoiceDHRDtls:message key="LBL_VENDOR_NAME"/> :&nbsp;</td> 
                        <td width="150" align="left" HEIGHT="25">
	                        <gmjsp:dropdown controlName="vendorId"	SFFormName="frmInvoiceDHR" SFSeletedValue="vendorId"
								SFValue="alVendorList" defaultValue="[Choose One]" codeId="ID" codeName="NAME" />
	                    </td> 
                       <!--    <td class="RightTableCaption" align="right" HEIGHT="24"> &nbsp;DHR Number :</td> 
				                     <td>&nbsp;<html:text property="dhrNumber"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;
			    		             </td>
			    		             -->
			    		  <td class="RightTableCaption" align="right" HEIGHT="25" width="300" style="white-space: nowrap;">&nbsp;<fmtInvoiceDHRDtls:message key="LBL_PO"/># :</td>
			    		  <td width="300" align="left" HEIGHT="25" style="padding-left: 5px;"><html:text property="poID"  size="20"  onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
    				  	</td>
			    		 <td class="RightTableCaption" align="right" HEIGHT="25" width="200">  <fmtInvoiceDHRDtls:message key="LBL_INVOICE_TYPE"/> :&nbsp;</td> 
		                  <td width="200" align="left" HEIGHT="25" style="padding-left: 5px;">
		                   <gmjsp:dropdown controlName="invoiceType"	SFFormName="frmInvoiceDHR"  SFSeletedValue="invoiceType"
								  SFValue="alInvoiceType"   codeId="CODENMALT" codeName="CODENM"  defaultValue= "[Choose One]"   />
							</td>
							<td align="right" width="300" colspan="1" HEIGHT="25">	 
								<gmjsp:dropdown controlName="invoiceOper"	SFFormName="frmInvoiceDHR"  SFSeletedValue="invoiceOper"
								 SFValue="alInvoiceOper"   codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"   />
	    		          </td> 
                    </tr>       
                    <tr><td colspan="9" height="1" class="LLine"></td></tr> 
                    <tr class="shade" HEIGHT="25">
                        <td class="RightTableCaption" align="right" HEIGHT="25" width="300" style="white-space: nowrap;"></font>&nbsp;&nbsp;<fmtInvoiceDHRDtls:message key="LBL_INVOICE_DATE_FROM"/> :&nbsp;</td> 
	                    <td width="150" align="left" HEIGHT="25">
	                    	<gmjsp:calendar SFFormName="frmInvoiceDHR" controlName="invoiceDateFrom"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
	                    </td>
	           			<td  class="RightTableCaption" align="right" width="300" HEIGHT="35"><fmtInvoiceDHRDtls:message key="LBL_TO"/> :</td>
	           			<td width="300" align="left" HEIGHT="25">
				           &nbsp;<gmjsp:calendar SFFormName="frmInvoiceDHR" controlName="invoiceDateTo"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
				        </td>
    		            <td class="RightTableCaption" align="right" HEIGHT="25" width="200" style="white-space: nowrap;"> <fmtInvoiceDHRDtls:message key="LBL_DOWNLOAD_DATE_FROM"/> :&nbsp;</td> 
	                    <td width="200" align="left" HEIGHT="25">
	                        <gmjsp:calendar SFFormName="frmInvoiceDHR" controlName="downloadDateFrom"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" gmClass="InputArea" />
	                   </td>
	                     <td class="RightTableCaption" align="left" width="300" HEIGHT="25" style="white-space: nowrap;"><fmtInvoiceDHRDtls:message key="LBL_TO"/>:
	                     	<gmjsp:calendar SFFormName="frmInvoiceDHR" controlName="downloadDateTo"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" gmClass="InputArea" />
				         </td>
				         <td width="200" align="left" HEIGHT="26" >
				         <fmtInvoiceDHRDtls:message key="BTN_LOAD" var="varLoad"/>
				         &nbsp;&nbsp;<gmjsp:button name="Load" value="&nbsp;${varLoad}&nbsp;" style="height: 22px; width: 4em;" gmClass="button" buttonType="Load" onClick="fnReload();" />
				        </td>
                    </tr>
                     
					<tr>
						<td class="LLine" height="2" colspan="9"></td>
					</tr>
		 
		  
		<tr>
			<td colspan="9" align="center" height="40">
				<display:table name="requestScope.frmInvoiceDHR.returnList" requestURI="/gmInvoiceDHR.do"  class="its"  export="true" paneSize ="600" freezeHeader="true" id="currentRowObject" varTotals="totals" decorator="com.globus.accounts.displaytag.beans.DTInvoiceDtlsWrapper" >
                <display:setProperty name="export.excel.filename" value="A/P Invoice Report.xls" />
			<fmtInvoiceDHRDtls:message key="LBL_INVOICE_ID" var="varInoviceId"/>	<display:column property="INVOICEID" title="${varInoviceId}"   />		
			<fmtInvoiceDHRDtls:message key="LBL_INVOICE_DATE" var="varInoviceDate"/>	<display:column property="INVDT" title="${varInoviceDate}" format= "<%=strDateFmt%>"/>
			<fmtInvoiceDHRDtls:message key="LBL_PO" var="varPo"/>	<display:column property="POID" title="${varPo}#" />
			<fmtInvoiceDHRDtls:message key="LBL_VENDOR_NAME" var="varVendorName"/>	<display:column property="VENDNM" title="${varVendorName}"/>	 						  												
			<fmtInvoiceDHRDtls:message key="LBL_INVOICE_AMT" var="varInovieAmt"/>	<display:column property="INVOICE_AMT" title="${varInovieAmt}" class="alignright"   format="<%=strCurrFmt%>"  total="true" />							
			<fmtInvoiceDHRDtls:message key="LBL_ADDL_AMT" var="varAddlAmt"/>	<display:column property="ADDL_CHARGE" title="${varAddlAmt}" class="alignright"  format="<%=strCurrFmt%>"   total="true"/>
			<fmtInvoiceDHRDtls:message key="LBL_DHR_AMT" var="varDhrAmt"/>	<display:column property="DHR_AMT" title="${varDhrAmt}" class="alignright" format="<%=strCurrFmt%>"  total="true"/> 
			<fmtInvoiceDHRDtls:message key="LBL_INVOICE_STATUS" var="varInvoiceStatus"/>	<display:column property="INVC_STATUS" title="${varInvoiceStatus}" class="aligncenter"  />
			 	<display:footer media="html"> 
					<%
					String strVal ; 
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column5").toString();
					String strInvoiceAmt = strCurrSign +  GmCommonClass.getStringWithCommas(strVal,2);
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column6").toString();
					String strAddlAmt = strCurrSign +  GmCommonClass.getStringWithCommas(strVal,2);
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column7").toString();
					String strDHRAmt =strCurrSign +  GmCommonClass.getStringWithCommas(strVal,2);
					 				
					%>
				<tr class = shade>
		  		<td colspan="4"> <B> <fmtInvoiceDHRDtls:message key="LBL_TOATL"/> : </B></td> 
    	    	 <td class = "alignright" > <B><%=strInvoiceAmt%></B></td>
    	    	 <td class = "alignright" > <B><%=strAddlAmt%></B></td>
    	    	  <td class = "alignright" > <B><%=strDHRAmt%></B></td>
    	    	  <td class = "alignright" > <B></B></td>
	    	 	 
  				</tr>
  			</display:footer>
																 
			</display:table>
			</td>
		</tr> 
		 
	</table>
	</FORM>
	 
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>