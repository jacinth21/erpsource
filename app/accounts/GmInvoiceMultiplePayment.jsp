 <%@page import="java.util.Date"%>
 <%
/**********************************************************************************
 * File		 		: GmInvoiceMultiplePayment.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.TimeZone"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@page import="java.util.Date,com.globus.common.beans.GmCalenderOperations"%>

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtInoviceMultiplePayment" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmInoviceMultiplePayment.jsp -->
<fmtInoviceMultiplePayment:setLocale value="<%=strLocale%>"/>
<fmtInoviceMultiplePayment:setBundle basename="properties.labels.accounts.GmInvoiceMultiplePayment"/>
<%
	String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strApplDateFmt =strGCompDateFmt;
	 String strTimeZone = strGCompTimeZone;	
	String strWikiTitle = GmCommonClass.getWikiTitle("POST_PAYMENTS");
	/* String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));  */
	/* /*  String  = GmCommonClass.getStringFromDate((Date)request.getAttribute("strSessApplDateFmt"),strApplDateFmt);   */
	 
	//strApplDateFmt = GmCommonClass.getStringFromDate((Date)request.getAttribute("strSessApplDateFmt"),strApplDateFmt);
	HashMap hmCurrency = new HashMap();
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strCurrSymbol = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_SYMB"));

	int intSize = 0;
	HashMap hmLoop = new HashMap();
	HashMap hcboVal = null;

	String strCodeID = "";
	String strDistId = "";
	String strSelected = "";

	String strInvId = "";
	String strInvIdSc = "";
	String strInvDt = "";
	double dbInvAmt = 0.00;
	double dbShipAmt = 0.00;
	double dbPendAmt = 0.00;
	String strCustPO = "";
	String strAccId = "";
	String strAmtPaid = "";
	String strDiscount = "";

	String strAction = (String)request.getAttribute("hAction");
	String strStatusFl = "";

	String strShade = "";
	String strCallFlag = "";
	String strInvType = "";
	
	ArrayList alAcctInvoice = new ArrayList();
	ArrayList alAccount = new ArrayList();
	ArrayList alPayModes = new ArrayList();
	
	ArrayList alInvTypes = new ArrayList();
	int intInvTypeSize = 0;
	HashMap hmInvType = null;
	String strInvTypeCodeID = "";
	String strInvTypeCodeNM ="";
	String strInvTypeId = "";
	String strUpdFl="";
	strUpdFl = GmCommonClass.parseNull((String)request.getAttribute("PPUPD"));
	if(strUpdFl.equals("")){
		strUpdFl = GmCommonClass.parseNull((String) request.getParameter("hUpdFl"));
	}
	int intLength = 0;

	HashMap hmReturn = new HashMap();
	HashMap hmParam = new HashMap();
	
	String strAccID_PAYALL = "";
	String strInvID_PAYALL = "";
	String strCustPO_PAYALL = "";
	String strInvAmt_PAYALL = "";	
	String strInvType_PAYALL = "";
	String strSortCName = "INVDT";
	String strSortType = "";
	String strCurrency= "";
	String strInvSource = GmCommonClass.parseNull((String)request.getAttribute("INVSOURCE"));
	String strIdAccount = GmCommonClass.parseNull((String)request.getAttribute("IDACCOUNT"));
	String strMode = GmCommonClass.parseNull((String)request.getAttribute("hMode"));
	ArrayList alType = (ArrayList)request.getAttribute("ALTYPE");
	String strPaymentTerm = GmCommonClass.parseNull((String)request.getAttribute("PAYMENTTERM"));
	String strParentFl = GmCommonClass.parseNull((String)request.getAttribute("DEFAULTPARENTFL")); 
	
	String strEmailReqFl = "";
	String strEmailVersion = "";
	String strPDFFileID = "";
	String strCSVFileID = "";
	String strInvoicedYear = "";
	String strDisplayImage = "";
	String strRTFFileID = "";
	
	
	double dbInvoiceAmt = 0.00;
	
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	hmParam = (HashMap)request.getAttribute("hmParam");
	alAccount = (ArrayList)hmReturn.get("ACCOUNTLIST");		
	if (strAction.equals("Load") || strAction.equals("Reload"))
	{
		alInvTypes = (ArrayList)hmReturn.get("CODELIST");
				
		if (strAction.equals("Reload"))
		{
			strAccId = (String)request.getAttribute("hAccId");
			alAcctInvoice = (ArrayList)hmReturn.get("OPENINVOICES");
			intLength = alAcctInvoice.size();
			alPayModes = (ArrayList)hmReturn.get("PAYMODEDETAILS");
			
			strAccID_PAYALL = (String)hmParam.get("ACCID");
			strInvID_PAYALL = (String)hmParam.get("INVID");
			strCustPO_PAYALL = (String)hmParam.get("CUSTPO");
			strInvAmt_PAYALL = (String)hmParam.get("INVAMT");
			strInvType_PAYALL = (String)hmParam.get("INVOTYP");
			strSortCName = (String)hmParam.get("SORT");
			strSortType = (String)hmParam.get("SORTTYPE");
		}
	}

	HashMap hmInvTypeForCobo = new HashMap();
	hmInvTypeForCobo.put("ID","");
	hmInvTypeForCobo.put("PID","CODEID");
	hmInvTypeForCobo.put("NM","CODENM");
	
	java.util.Date paymentDt =(java.util.Date) request.getAttribute("STRPAYMENTDATE");
	java.sql.Date paymentSqlDt = paymentDt==null?null:new java.sql.Date(paymentDt.getTime());
	
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.labels.accounts.GmInvoiceMultiplePayment", strSessCompanyLocale);
	String strImgAltOpenRTF = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_OPEN_RTF"));
	String strImgAltOpenPDF = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_OPEN_PDF"));
	String strImgAltOpenCSV = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_OPEN_CSV"));
	String strChooseAccRpt = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_CHOOSE_ACCOUNT_REPORT"));
	String strChooseDlrRpt = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_CHOOSE_DEALER_REPORT"));
	String strChoRptFl="Y";

	//The Below Three Lines is commented for PMT-5022 .Payment Date Needs to return last working day instead of previous day.
    // Calendar cal = Calendar.getInstance(TimeZone.getDefault());
    //cal.add(Calendar.DATE, -1);
    //java.sql.Date dtyester = new java.sql.Date(cal.getTimeInMillis());
    
    //java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(DATE_FORMAT);
    //sdf.setTimeZone(TimeZone.getDefault());
	//String strDate = sdf.format(cal.getTime());
	
	
	// Get today as a Calendar
	//Calendar today = Calendar.getInstance();
	// Subtract 1 day
	//today.add(Calendar.DATE, -1);
	// Make an SQL Date out of that
	
	//java.sql.Date yesterday = new java.sql.Date(today.getTimeInMillis());
	int currMonth = GmCalenderOperations.getCurrentMonth()-1;// To get the current month
	String strFromdaysDate = GmCommonClass.parseNull(GmCalenderOperations.getFirstDayOfMonth(currMonth,strApplDateFmt)); //To get the first day of month
	String strTodaysDate = GmCommonClass.parseNull(GmCalenderOperations.getCurrentDate(strApplDateFmt));
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean rbCompany = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	String strPaymentvalidFl = GmCommonClass.parseNull(rbCompany.getProperty("INVOICE.PAYMENT_DATE_VALIDATION_FL"));
%>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>

<HTML>
<HEAD>
<TITLE> Globus Medical: Post Multiple Payments - By Account</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script type="text/javascript">
var ppupd = '<%=strUpdFl%>';
var varSource = '<%=strInvSource%>';
var varIdAccount = '<%=strIdAccount%>';	
var strInvoiceType = '<%=strInvType_PAYALL%>';
var sortCname = '<%=strSortCName%>';
var accId = '<%=strAccId%>';
var sessCurSymbol = '<%=strCurrSymbol%>';
var parAccfl = '<%=strParentFl%>';
var lblChooseRpt = '<fmtInoviceMultiplePayment:message key="LBL_CHOOSE_REPORT"/>';
var ChoRptFl='<%=strChoRptFl%>';
//PC-4637-payment date validation
var strFromdaysDate = '<%=strFromdaysDate%>';
var strLastdaysDate = '<%=strTodaysDate%>';
var format = '<%=strApplDateFmt%>';
var paymentValidationFl = '<%=strPaymentvalidFl%>';
</script>
<script language="javascript" src="<%=strJsPath%>/GmInvoicePayment.js"></script>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmInvoiceMultiplePayment.js"></script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnLoad1();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmInvoiceServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hAccId" value="<%=strAccId%>">
<input type="hidden" name="hLen" value="<%=intLength%>">
<input type="hidden" name="hTotalValue">
<input type="hidden" name="hInvType" value="<%=strInvType%>">
<input type="hidden" name="hCheckedInvTypes" value="<%=strInvType_PAYALL%>">
<input type="hidden" name="hSort" value="<%=strSortCName%>">
<input type="hidden" name="hSortAscDesc" value="<%=strSortType%>">
<input type="hidden" name="hMode" value="<%=strMode%>">
<input type="hidden" name="hUpdFl" value="<%=strUpdFl%>">

	<table border="0" class="DtTable900" cellspacing="0" cellpadding="0"> <%--class="border" --%>
		<tr>
			<td bgcolor="#666666" height="1" colspan="3"></td>
		
		</tr>
		<tr>
	<td  colspan="2" height="25" class="RightDashBoardHeader"><fmtInoviceMultiplePayment:message key="LBL_POST_PAYMENTS"/> </td>
	<td align="right" class=RightDashBoardHeader > 	
	<fmtInoviceMultiplePayment:message key="LBL_HELP" var="varHelp"/>
		<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
	</td>
	</tr>
	<tr> <td class="RightTextBlue"><BR>  <B><fmtInoviceMultiplePayment:message key="LBL_CHOOSE_ACCOUNT_REPORT"/> </B>
			</td>	</tr>
	<tr class="Shade">
		<td id="tdAccInfoCommon"  align="center" height="25" colspan="2">&nbsp;
				<jsp:include page="/accounts/GmIncludeAccountType.jsp" >
				<jsp:param name="DISPACCFL" value="Y"/>
				<jsp:param name="COLLECTORFL" value="Y"/>
				<jsp:param name="REPACCTLABELFL" value="Y"/>		
				</jsp:include>		
			</td>
			<fmtInoviceMultiplePayment:message key="BTN_LOAD" var="varLoad"/>
			<td><BR>&nbsp;&nbsp;<gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnGo();" buttonType="Load" />&nbsp;
			</td>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<tr id="trSubParentPanel">
			<td class="RightTablecaption" colspan="2" height="25">
			<fmtInoviceMultiplePayment:message key="LBL_CHOOSE_REPORT" var="varChooseREport"/>
						<gmjsp:label type="RegularText"  SFLblControlName="${varChooseREport}:" td="false"/>&nbsp;<select name="Cbo_Action" id="Region" class="RightText"  
						onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">				
							
						<%if(strMode!=null &&	!strMode.equals("0")&&!strMode.equals("")){	%>
							<option value="<%=strMode%>" >
							<% if(strMode.equalsIgnoreCase("L10")){%><fmtInoviceMultiplePayment:message key="LBL_LAST_TEN_INVOICES"/> 
								<option value="OPEN"><fmtInoviceMultiplePayment:message key="LBL_OPEN_INVOICES"/></option>
								<option value="ALL"><fmtInoviceMultiplePayment:message key="LBL_ALL_INVOICES"/></option>	
							<%}else if  (strMode.equalsIgnoreCase("OPEN")){%><fmtInoviceMultiplePayment:message key="LBL_OPEN_INVOICES"/>
								<option value="ALL"><fmtInoviceMultiplePayment:message key="LBL_ALL_INVOICES"/></option>
								<option value="L10"><fmtInoviceMultiplePayment:message key="LBL_LAST_TEN_INVOICES"/></option>	
							<%}else if  (strMode.equalsIgnoreCase("ALL")){%><fmtInoviceMultiplePayment:message key="LBL_ALL_INVOICES"/>
								<option value="OPEN"><fmtInoviceMultiplePayment:message key="LBL_OPEN_INVOICES"/></option>
								<option value="L10"><fmtInoviceMultiplePayment:message key="LBL_LAST_TEN_INVOICES"/></option>
							<% } %></option>
						<%}else{%>
									<option value="OPEN"><fmtInoviceMultiplePayment:message key="LBL_OPEN_INVOICES"/></option>
									<option value="ALL"><fmtInoviceMultiplePayment:message key="LBL_ALL_INVOICES"/></option>									
									<option value="L10"><fmtInoviceMultiplePayment:message key="LBL_LAST_TEN_INVOICES"/></option>
							<%} %>
						
											
						</select>
						
			</td>
			<td colspan="1" class="RightTablecaption" align="Right"><fmtInoviceMultiplePayment:message key="LBL_SHOW_PARENT_ACCOUNT"/>:</td>
					<td>&nbsp;<input type="checkbox" size="30" checked="<%=strParentFl %>" name="Chk_ParentFl" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" >					
						</td>
		</tr>
		<tr>
			
			<td class="Line" colspan="3"></td>
		</tr>
		<tr><td colspan="3">
		<table border="1" width="755" cellspacing="0" cellpadding="0"> <%--class="border"--%>
			 
			 <tr> 
			 <td width="250">
			  <table cellspacing="0" cellpadding="0" border="0" ">
			  <tr height="25">
			 	<td class="RightTablecaption" align="left" valign="middle" colspan="2">&nbsp;&nbsp;<fmtInoviceMultiplePayment:message key="LBL_INVOCIE_TYPE"/>:</td>
			 	</tr>
			 	<tr>
			 	 <td width="10"></td><td valign="middle">&nbsp;&nbsp;<%=GmCommonControls.getChkBoxGroup ("",alInvTypes,"Chk_Invoice_Type",hmInvTypeForCobo)%></td>	
			 	</tr>
			 	<tr height="10"><td></td></tr>
			 </table>
			</td>
			<td>
			 <table cellspacing="0" cellpadding="0" border="0" width="100%">
			
			 	<tr height="25">
			 	<fmtInoviceMultiplePayment:message key="LBL_INVOICE_ID" var="varInvocieId"/>
			 		<td class="RightTablecaption" align="right"><gmjsp:label type="RegularText"  SFLblControlName="${varInvocieId}:" td="false"/></td>
					<td class="RightText">&nbsp;<input type="text" size="10" value="<%=strInvID_PAYALL%>" name="Txt_InvoiceID" class="InputArea" 
							onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" ></td>
				</tr>
				<tr height="25">
				<fmtInoviceMultiplePayment:message key="LBL_CUSTOMER_PO" var="varCustomerPO"/>
					<td class="RightTablecaption" align="right"><gmjsp:label type="RegularText"  SFLblControlName="${varCustomerPO}:" td="false"/></td>
					<td class="RightText">&nbsp;<input type="text" size="10" value="<%=strCustPO_PAYALL%>" name="Txt_CustomerPO" class="InputArea" 
							onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" ></td>
				</tr>
				<tr height="25">
				<fmtInoviceMultiplePayment:message key="LBL_INVOICE_AMOUNT" var="varInvoiceAmount"/>
					<td class="RightTablecaption" align="right"><gmjsp:label type="RegularText"  SFLblControlName="${varInvoiceAmount}:" td="false"/></td>
					<td class="RightText">&nbsp;<input type="text" size="10" value="<%=strInvAmt_PAYALL%>" name="Txt_Invoice_Amount" class="InputArea" 
							onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" ></td>
			 	</tr>
			 </table>
			 </td>
		</tr>
		 </table>
			 </td>
		</tr>
		<%--
		<tr><td class="Line" ></td><td class="Line" ></td><td class="Line" ></td></tr>		
		 	 </table>
		 --%>
<%
	if (strAction.equals("Reload"))
	{	
%>
	<tr><td colspan="3">
		<table border="0" width="755" cellspacing="0" cellpadding="0">	
	<tr><td class="Line" colspan="10"></td></tr>
		<tr>
			<td height="100" valign="top" align="center">

				<table border="0" width="100%" cellspacing="0" cellpadding="0" >
						
						<tr height="28">
						<fmtInoviceMultiplePayment:message key="LBL_PAYMENT_AMOUNT" var="varPaymentAmount"/>
							<td align="right" nowrap class="RightTablecaption" HEIGHT="24"><gmjsp:label type="MandatoryText"  SFLblControlName="${varPaymentAmount}:" td="false"/></td>
							<td colspan="6">&nbsp;<input type="text" size="10" value="" name="Txt_PAmt" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
								onBlur="changeBgColor(this,'#ffffff');" >&nbsp;
							</td>
							<fmtInoviceMultiplePayment:message key="LBL_TOATAL_OUTSTANDING" var="varTotaOutstanding"/>
							<td align="right" nowrap class="RightTablecaption" HEIGHT="24" colspan="2"><gmjsp:label type="RegularText"  SFLblControlName="${varTotaOutstanding}:" td="false"/></td>
							<td align="right" nowrap class="RightText" HEIGHT="24" id="Lbl_Total"></td>
						</tr>
						<tr><td class="LLine" colspan="10"></td></tr>						
						<tr height="28" class="Shade">
						<fmtInoviceMultiplePayment:message key="LBL_PAYMENT_DATE" var="varPaymentDate"/>
							<td align="right" class="RightTablecaption" HEIGHT="24"><gmjsp:label type="MandatoryText"  SFLblControlName="${varPaymentDate}:" td="false"/></td>
							<td colspan="6">&nbsp;<gmjsp:calendar textControlName="Txt_PDate" textValue="<%=paymentSqlDt%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
								</td>
								<td align="right" nowrap class="RightTablecaption" HEIGHT="24" colspan="2"><fmtInoviceMultiplePayment:message key="LBL_PAYMENT_TERM"/>:</td>
								<td align="right" nowrap class="RightText" HEIGHT="24" id="Lbl_Term"><%=strPaymentTerm%></td>
								
						</tr>
						<tr><td class="LLine" colspan="10"></td></tr>
						<tr height="28">
						<fmtInoviceMultiplePayment:message key="LBL_PAYMENT_MODE" var="varPaymentMode"/>
							<td HEIGHT="24" align="right" class="RightTablecaption"><gmjsp:label type="MandatoryText"  SFLblControlName="${varPaymentMode}:" td="false"/></td>
							
							<td colspan="9">&nbsp;<select name="Cbo_PayMode" class="RightText"  
							onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtInoviceMultiplePayment:message key="LBL_CHOOSE_ONE"/>
	<%
							intSize = alPayModes.size();
							hcboVal = new HashMap();
							strCodeID = "";
							strSelected  = "";
							String strPayMode = "5010";
							for (int i=0;i<intSize;i++)
							{
								hcboVal = (HashMap)alPayModes.get(i);
								strCodeID = (String)hcboVal.get("CODEID");
								strSelected = strPayMode.equals(strCodeID)?"selected":"";
	%>
								<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
	<%
				  			}
	%>
					</select>												
						</td>
					</tr>
					<tr><td class="LLine" colspan="10"></td></tr>	
					<tr class="Shade">
					<fmtInoviceMultiplePayment:message key="LBL_DETAILS" var="varDetails"/>
						<td class="RightTablecaption" valign="top" align="right" HEIGHT="24"><gmjsp:label type="RegularText"  SFLblControlName="${varDetails}:" td="false"/></td>
						<td colspan="9">&nbsp;<textarea name="Txt_Details" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" rows=5 cols=45 value="" ></textarea></td>
					</tr>
					<tr><td class="LLine" colspan="10"></td></tr>									
					<tr>
						<td class="RightText" nowrap align="right" HEIGHT="24"><fmtInoviceMultiplePayment:message key="LBL_TOTAL_AMOUNT_TO_POST"/>:</td>
						<td class="RightTextBlue" id="IDCalculate">&nbsp;0.00</td>
						<td colspan="8">
							<table border="0" width="100%"><tr>
							<td colspan = "2" align = "center" class="RightTextRed"> <fmtInoviceMultiplePayment:message key="LBL_CREDIT_BLUE"/>  </td>
							<td colspan = "2" align = "center" class="RightTextBlue"> <fmtInoviceMultiplePayment:message key="LBL_DEBIT_BLUE"/> </td>
							<td colspan = "2" align = "center" class="RightTextGreen" ><fmtInoviceMultiplePayment:message key="LBL_REGULAR_GREEN"/>   </td>
							<td colspan = "2" align = "center" class="RightTextPurple"><fmtInoviceMultiplePayment:message key="LBL_OBO_PURPLE"/>  </td>
							</tr></table>
						</td>
					</tr>					
				
					<tr><td class="Line" colspan="10"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="150"><a href= javascript:fnSort('INVID');><fmtInoviceMultiplePayment:message key="LBL_INVOICE_ID"/></a></td>
						<td width="120"  align="center"><a href= javascript:fnSort('INVDT');><fmtInoviceMultiplePayment:message key="LBL_INVOICE_DATE"/></a></td>
						<td width="80" align="left"><a href= javascript:fnSort('PO');><fmtInoviceMultiplePayment:message key="LBL_CUSTOMER_PO"/></a></td>						
						<% if(strInvSource.equals("26240213")){ //26240213:Company Dealers  %>
                        <td width="140" align="right" colspan="2"><fmtInoviceMultiplePayment:message key="LBL_DEALER_ID"/></td> 
	                    <%}else{%>
	                     <td width="140" align="right" colspan="2"><fmtInoviceMultiplePayment:message key="LBL_REP_ACCOUNT_ID"/></td> 
		                <%}%>						
						<td width="80" align="right"><fmtInoviceMultiplePayment:message key="LBL_CURRENCY"/></a></td>
						<td width="100" align="right"><a href= javascript:fnSort('INVAMT');><fmtInoviceMultiplePayment:message key="LBL_INVOICE_AMT"/></a></td>
						<td width="80" align="right"><fmtInoviceMultiplePayment:message key="LBL_AMOUNT_RECD"/>.</td>
						<td width="90" align="right"><font color ="red">*</font><fmtInoviceMultiplePayment:message key="LBL_AMOUNT_TO_POST"/></td>
						<td width="80" align="right"><fmtInoviceMultiplePayment:message key="LBL_DISCOUNT"/></td>
					</tr>
					<tr><td class="Line" colspan="10"></td><td class="Line" ></td><td class="Line" ></td></tr>
					
<%
					if (intLength > 0)
					{
							dbInvoiceAmt = 0;
							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alAcctInvoice.get(i);
								strInvType = GmCommonClass.parseNull((String)hmLoop.get("INVTP"));
								strInvId = GmCommonClass.parseNull((String)hmLoop.get("INVID"));
								strInvDt = GmCommonClass.getStringFromDate((java.util.Date)hmLoop.get("INVDT"),strApplDateFmt);
								dbInvAmt = Double.parseDouble(GmCommonClass.parseZero((String)hmLoop.get("INVAMT")));
								strInvTypeId = GmCommonClass.parseNull((String)hmLoop.get("INVTPID"));
								strAmtPaid = GmCommonClass.parseZero((String)hmLoop.get("AMTPAID"));
								strCurrency = GmCommonClass.parseNull((String)hmLoop.get("CURRENCY"));
								if(strInvTypeId.equals("50200")	|| strInvTypeId.equals("50202") || strInvTypeId.equals("50203"))// Invoice type, 50200 - regular, 50202 - credit, 50203 - debit
								{
									dbInvoiceAmt = dbInvoiceAmt + (dbInvAmt - Double.parseDouble(strAmtPaid));
								}
								strStatusFl = GmCommonClass.parseNull((String)hmLoop.get("SFL"));
								strCallFlag = GmCommonClass.parseNull((String)hmLoop.get("CALL_FLAG"));
								strCustPO = GmCommonClass.parseNull((String)hmLoop.get("PO"));
								strAccId = GmCommonClass.parseNull((String)hmLoop.get("ACCID"));
								dbPendAmt = dbInvAmt - Double.parseDouble(strAmtPaid);
								strEmailReqFl = GmCommonClass.parseNull((String)hmLoop.get("EMAILREQ"));
								strEmailVersion = GmCommonClass.parseNull((String)hmLoop.get("EVERSION"));
								strPDFFileID = GmCommonClass.parseNull((String)hmLoop.get("PDFFILEID"));
								strCSVFileID = GmCommonClass.parseNull((String)hmLoop.get("CSVFILEID"));
								strInvoicedYear = GmCommonClass.parseNull((String)hmLoop.get("INVYEAR"));
								strDisplayImage = "";
								strShade	= (i%2 == 0)?"class=Shade":""; //For alternate Shading of rows
								strRTFFileID = GmCommonClass.parseNull((String)hmLoop.get("RTFFILEID"));
								strInvIdSc = strInvId.replace("-","_");
%>
					<tr <%=strShade%>>
						<td height="20" class="RightText" style="white-space: nowrap;">
							<input type="checkbox" value="<%=strInvIdSc%>"  name="<%=strInvIdSc%>" onClick="fnCalculateAmount(this);">&nbsp;<a class = "<%=strInvType%>" href="javascript:fnPrintInvoice('<%=strInvId%>');" class="RightText"><%=strInvId%></a>&nbsp;
						<!-- Code for Call Log Starts -->												
						<%
			
						if(!strInvSource.equals("50253")) //ICS
						{
							
							strDisplayImage = "<img id='imgRTF' style='cursor:hand;vertical-align:middle;' src='"+strImagePath+"/word_icon.jpg' onclick=fnExportRTFFile('"+strInvId+"','" +strRTFFileID+"');  height='16' width='16' title='"+strImgAltOpenRTF+"' >&nbsp;";
							
							if(!strPDFFileID.equals("")){ // check the PDF file is stored 
								strDisplayImage += "<img id='imgPDF' style='cursor:hand;vertical-align:middle;' src='"+strImagePath+"/pdf_icon.gif' onclick=fnExportFile('"+strPDFFileID+"'); title='"+strImgAltOpenPDF+"'>&nbsp;";
							}
							if(!strCSVFileID.equals("") && strCountryCode.equals("en")){ // check the CSV file is stored and is for US only
								strDisplayImage += "<img id='imgCSV' style='cursor:hand;vertical-align:middle;' src='"+strImagePath+"/csv_icon.gif' onclick=fnExportFile('"+strCSVFileID+"'); title='"+strImgAltOpenCSV+"'>&nbsp;";
								
							}
						}
						%>
			  <fmtInoviceMultiplePayment:message key="LBL_CLICK_TO_ENTER_LOG" var="varClickToEnterLog"/>
			  <img id="imgEdit" style="cursor:hand"  
									<% if (strCallFlag.equals("N"))
	 									{ 
	 								%>
		 								src="<%=strImagePath%>/phone_icon.jpg" ;
		 							<%
		 								} else {
									%>	
		 								src="<%=strImagePath%>/phone-icon_ans.gif" 
			 						<% } %>
			 						title="${varClickToEnterLog}" 
			 						width="20" height="17" 
			 						onClick="javascript:fnOpenLog('<%=strInvId%>' )"/>&nbsp;<%=strDisplayImage %> 						
			 												<!-- Code for Call Log Ends -->
				 					&nbsp;</td>
						<td class="RightText" width="120" align="center">&nbsp;<%=strInvDt%></td>
						<td class="RightText" align="left"><%=strCustPO%>&nbsp;<input type="hidden" value="<%=strCustPO%>" name="Txt_PO<%=strInvIdSc%>"></td>					
						<td class="RightText" align="right" colspan="2"><%=strAccId%>&nbsp;</td>	
						<td class="RightText" align="right"><%=strCurrency%>&nbsp;</td>
						<gmjsp:currency type="CurrText"  textValue="<%=String.valueOf(dbInvAmt)%>" currSymbol="<%=strCurrSymbol %>"/>
						<gmjsp:currency type="CurrText"  textValue="<%=strAmtPaid%>" currSymbol="<%=strCurrSymbol %>"/>
						<input type="hidden" value="<%=dbInvAmt%>" name="Txt_InvAmount<%=strInvIdSc%>">
						<input type="hidden" value="<%=strAmtPaid%>" name="Txt_Recd<%=strInvIdSc%>">
						<input type="hidden" value="<%=dbPendAmt%>" name="Txt_Pend<%=strInvIdSc%>"></td>
						
						<td class="RightText" align="right"><input type="text" onBlur="javascript:fnCal(this);" name="Txt_Post<%=strInvIdSc%>" size="8" value="" class="RightText">&nbsp;</td>
						<td class="RightText" align="right" style="position: relative;top: 6px;"><input type="text" onBlur="javascript:fnCal(this);" name="Txt_Discount<%=strInvIdSc%>" size="8" value="" class="RightText">&nbsp;</td>
					</tr>
<%
							}
%>
						<tr><td class="LLine" colspan="10"></td></tr>
						<tr>
							<td align="center" colspan="10" height="30">
							<fmtInoviceMultiplePayment:message key="BTN_SUBMIT" var="varSubmit"/>
							<fmtInoviceMultiplePayment:message key="BTN_STATEMENT" var="varStatement"/>
								<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" name="Btn_Submit" gmClass="button" onClick="fnPostPayment();" buttonType="Save" />&nbsp;&nbsp;&nbsp;
								<gmjsp:button value="&nbsp;${varStatement}&nbsp;" name="Btn_Stmt" gmClass="button" onClick="fnOpenStatement();" buttonType="Load" />
								
							</td>
						</tr>
<%
					}else{				
%>
						<tr>
							<td align="center" colspan="10" class="RightTextBlue" height="40">
								<fmtInoviceMultiplePayment:message key="LBL_THERE_NO_INVOICES"/>
							</td>
						</tr>
<%							
					}
%>
				<input type="hidden" name="hTotalAmt" value="<%=strCurrSymbol%>&nbsp;<%=GmCommonClass.getStringWithCommas(""+dbInvoiceAmt)%>">
				</table>
			</td>
		</tr>
<%
	}
%>
<%-- 
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
 --%>
    </table>
</td>
</tr>
</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
