 <%
/**********************************************************************************
 * File		 		: GmVATSummary.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Arockia Prasath
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ taglib prefix="fmtVATSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmVATSummary.jsp -->
<fmtVATSummary:setLocale value="<%=strLocale%>"/>
<fmtVATSummary:setBundle basename="properties.labels.accounts.GmVATSummary"/>
<bean:define id="gridData" name="frmVATSummary" property="gridData" type="java.lang.String"> </bean:define>
<bean:define id="strARRptByDealerFlag" name="frmVATSummary" property="strARRptByDealerFlag" type="java.lang.String"> </bean:define>

<%
	String applDateFmt = strGCompDateFmt;
	String strAccountsJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: VAT Summary</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">

<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strAccountsJsPath%>/GmVATSummary.js"></script>
<script type="text/javascript">


var objGridData;
objGridData='<%=gridData%>';
var format = '<%=applDateFmt%>';
var arRptByDealerFlag = '<%=strARRptByDealerFlag%>';
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">

<html:form  action="/gmVATSummary.do">
<html:hidden property="strOpt" name="frmVATSummary"/>

<input type="hidden" name ="format"  value="<%=applDateFmt%>"/>


<table border="0"  class="GtTable850" cellspacing="0" cellpadding="0">
		<tr><td>

<table border="0"   width="850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader" >&nbsp;&nbsp;<fmtVATSummary:message key="LBL_VAR_SUMMARY"/></td>
			
		</tr>
		
		<!-- Custom tag lib code modified for JBOSS migration changes -->
	
		<tr><td>
		   <table><tr>
			<td HEIGHT="50" class="RightTableCaption" align="Right" width="25">&nbsp;&nbsp;&nbsp;</td>
			<td HEIGHT="50" class="RightTableCaption" align="Right"><fmtVATSummary:message key="LBL_FROM_DATE"/>:</td>
			<td HEIGHT="50" class="RightTableCaption" align="Right">		
			&nbsp;<gmjsp:calendar  SFFormName='frmVATSummary'   SFDtTextControlName="fromDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;</td>
			
			<td HEIGHT="50" class="RightTableCaption" align="Right"><fmtVATSummary:message key="LBL_TO_DATE"/>:</td>
			<td HEIGHT="50" class="RightTableCaption" align="Right">
			&nbsp;<gmjsp:calendar   SFFormName='frmVATSummary'  SFDtTextControlName="toDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
			</td>
			<td class="RightTablecaption" align="right" >
			    <fmtVATSummary:message key="LBL_CURRENCY"/>:</td>&nbsp;
			<td class="RightText"><gmjsp:dropdown	controlName="strCompCurrency" SFFormName="frmVATSummary"
						              SFSeletedValue="strCompCurrency" SFValue="alCompCurrency" codeId="ID" codeName="NAMEALT" />	
            </td>	
			<td  align="center" >
			<fmtVATSummary:message key="BTN_LOAD" var="varLoad"/>
			<gmjsp:button value="&nbsp;${varLoad}&nbsp;"  name="Btn_Submit" gmClass="Button" buttonType="Load" onClick="fnLoad();"/>
			</td>
			</tr>
			</table>	
			</td>	
		</tr>
	
</table>	
</td></tr>
<tr><td>
<table  border="0" width="850"  cellspacing="0" cellpadding="0">

			
		
			<tr>	<td><div id="vatSummaryData"  height="500px" width="850px" ></div></td> </tr>
			<tr>
			                <td colspan="6" align="center">
			              	<div class='exportlinks'><fmtVATSummary:message key="LBL_EXPORT_OPTION"/> : <img
			src='img/ico_file_excel.png' />&nbsp;<a href="#"
			onclick="fnExport('excel');"> <fmtVATSummary:message key="LBL_EXCEL"/> </a></div>
			                </td>

					</tr>
					
			
	

		
</table>	

</td></tr></table>

</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
