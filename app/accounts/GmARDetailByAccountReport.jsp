 <%
/**********************************************************************************
 * File		 			: GmARDetailByAccountReport.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.1
 * author			: Dhinakaran James
 * Modified by Joe P Kumar on Apr 27 '06 to implement Display Tag
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.*,java.text.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="org.apache.commons.beanutils.RowSetDynaClass"%>
<%@ page import="org.apache.commons.beanutils.DynaBean" %>
<%@ taglib prefix="fmtARDetailByAccountReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmARDetailByAccountReport.jsp -->
<fmtARDetailByAccountReport:setLocale value="<%=strLocale%>"/>
<fmtARDetailByAccountReport:setBundle basename="properties.labels.accounts.GmARDetailByAccountReport"/>
<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strWikiTitle = GmCommonClass.getWikiTitle("INVOICE_DETAILS");
	String strApplCurrFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplCurrFmt"));
	String strCurrFmt = "{0,number,"+strApplCurrFmt+"}";
	String strApplDateFmt = strGCompDateFmt;
	String strRptFmt = "{0,date,"+strApplDateFmt+"}";
	RowSetDynaClass rsd= (RowSetDynaClass)request.getAttribute("results");
	List al = rsd.getRows();	
	String strCurr="";
	if (al.size()>0){
		DynaBean db = (DynaBean)al.get(0);
		strCurr =(String) db.get("CURRENCY");
	}
	
%>
<HTML>
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>

function fnSubmit()
{
	document.frmAccount.hAction.value = "Go";
	document.frmAccount.submit();
}

function fnPrintInvoice(varinv)
{
windowOpener('/GmInvoiceInfoServlet?hAction=Print&hInv='+varinv,"PrntInv","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=500");
}

function fnViewDetails(pnum,act)
{
windowOpener("/GmPartInvReportServlet?hAction="+act+"&hPartNum="+encodeURIComponent(pnum),"INVPOP","resizable=yes,scrollbars=yes,top=300,left=300,width=650,height=200");
}

function fnOpenLog(id)
{
	windowOpener("/GmCommonLogServlet?hType=1201&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
// this function used to export the files
function fnExportFile(fileID){
	var uploadDir="BATCH_INVOICE";
	document.frmARDetail.action = "/GmCommonFileOpenServlet?sId="+fileID+"&uploadString="+uploadDir;
	document.frmARDetail.submit();
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmARDetail"  method="POST" action="<%=strServletPath%>/GmARSummaryServlet">
<table border="0" class="DtTable700"  cellspacing="0" cellpadding="0" >	
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="3"><fmtARDetailByAccountReport:message key="LBL_INVOICE_DETAILS"/> <%=strCurr %> </td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtARDetailByAccountReport:message key="LBL_HELP" var="varHelp"/>
			<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    </td>
		</tr>	
		<tr><td colspan="4">
		<display:table name="requestScope.results.rows" export="false" class="its" id="currentRowObject" decorator="com.globus.displaytag.beans.DTAccInvoiceWrapper">		
					<fmtARDetailByAccountReport:message key="LBL_NAME" var="varName"/><display:column property="NAME" title="${varName}" class="alignleft" style="width:450;" sortable="true" />
					<fmtARDetailByAccountReport:message key="LBL_INVOICE_ID" var="varInoviceId"/><display:column property="INV_ID" title="${varInoviceId}" class="alignleft" style="width:300;" sortable="true" />
					<fmtARDetailByAccountReport:message key="LBL_INVOICE_DATE" var="varInoiceDate"/><display:column property="INV_DATE" title="${varInoiceDate}" class="aligncenter" style="width:80;" format="<%=strRptFmt%>"/>
					<fmtARDetailByAccountReport:message key="LBL_CUSTOMER_PO" var="varCustomerPo"/><display:column property="CUSTOMER_PO" title="${varCustomerPo}" style="width:100;" class="alignleft"/>
					<fmtARDetailByAccountReport:message key="LBL_INVOICE_AMT" var="varInvoiceAmt"/><display:column property="INV_AMT" title="${varInvoiceAmt}" style="width:110;" class="alignright" />
					<fmtARDetailByAccountReport:message key="LBL_PAYMENT_AMT" var="varPaymentAmt"/><display:column property="PAYMENT_AMT" title="${varPaymentAmt}" style="width:110;" class="alignright" format="<%=strCurrFmt%>"/>
		</display:table>
		</td>
		</tr>
		</table>
</FORM>					
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
