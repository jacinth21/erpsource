 <%
/**********************************************************************************
 * File		 		: GmPendingPOSummary.jsp
 * Desc		 		: This screen is used to Payment List for the selected customer
 * Version	 		: 1.0
 * author			: Richard
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ taglib prefix="fmtPendingPOSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmPendingPOSummary.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%

	String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	HashMap hmCurrency = new HashMap();
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strApplCurrFmt = (String)hmCurrency.get("CMPCURRFMT");
	String strCurrSign = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_SYMB"));
	String strCurrFmt = "{0,number,"+strApplCurrFmt+"}";
	String strAccountCurrency = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_ID"));
	ArrayList alCompCurrency = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCOMPANYCURRENCY"));
	
%>
<fmtPendingPOSummary:setLocale value="<%=strLocale%>"/>
<fmtPendingPOSummary:setBundle basename="properties.labels.accounts.GmPendingPOSummary"/>
<HTML>
<HEAD>
<TITLE> Globus Medical: Pending PO Summary</TITLE>
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script type="text/javascript" src="<%=strAccountJsPath%>/GmPendingPOSummary.js"></script>


<script>

function fnSubmit()
{
    //document.frmAccount.hAction.value = "Reload";
	//document.frmAccount.submit();
}


</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmPaymentListServlet">
<input type="hidden" name="hAccountList" value="">
<input type="hidden" name="hAccountListPlus" value="">
<input type="hidden" name="hAction" value="">
<table border="0" class="DtTable950" cellspacing="0" cellpadding="0">
	<tr><td height="25" class="RightDashBoardHeader" colspan="2" ><fmtPendingPOSummary:message key="LBL_PO_PENDING_LIST"/></td></tr>
	<tr>
	<td class="RightTableCaption" height="25" align="right"><fmtPendingPOSummary:message key="LBL_CURRENCY"/>:</td>
	<td class="LeftText">&nbsp;<gmjsp:dropdown controlName="Cbo_Comp_Curr"  seletedValue="<%=strAccountCurrency%>" value="<%=alCompCurrency%>" codeId = "ID"  codeName = "NAMEALT" />
	&nbsp;&nbsp;&nbsp;<fmtPendingPOSummary:message key="BTN_LOAD" var="varLoad"/><gmjsp:button  value="${varLoad}" name="Btn_Load" gmClass="Button" style="width: 5em; height: 23px;" onClick="fnLoad();" buttonType="Load" /></td></tr>
	<tr><td class="Line" height="1" colspan="2"></td></tr>
	<tr>
		<td colspan="2">
		<display:table name="requestScope.results.rows" export="true" id="currentRowObject" class="its" varTotals="POtotal">
            <display:setProperty name="export.excel.filename" value="Pending Order Summary.xls" />  
		  	<fmtPendingPOSummary:message key="LBL_REP_ACCOUNT_NAME" var="varRepAccountName"/><display:column property="ACCT_NAME" title="${varRepAccountName}" sortable="true" />
		  	<fmtPendingPOSummary:message key="LBL_REP_ID" var="varRepId"/><display:column property="REPID" title="${varRepId}" sortable="true" />
		  	<fmtPendingPOSummary:message key="LBL_REP_NAME" var="varRepName"/><display:column property="REPNM" title="${varRepName}" sortable="true" />
		  	<fmtPendingPOSummary:message key="LBL_CURRENCY" var="varCurrency"/><display:column property="ARCURRENCYSYMB" title="${varCurrency}" style="text-align: center; "/>
		  	<fmtPendingPOSummary:message key="LBL_CURRENT_VALUE" var="varCurrentValue"/><display:column property="CURRENT_VALUE" title="${varCurrentValue}" class="alignright" sortable="true" format="<%=strCurrFmt%>" total="true" />
		  	<fmtPendingPOSummary:message key="LBL_DAYS" var="varDays"/><display:column property="FIFTEEN_THIRTY" title="${varDays}" class="alignright" sortable="true" format="<%=strCurrFmt%>" total="true" />
		  	<fmtPendingPOSummary:message key="LBL_PRIOR_MONTHS" var="varPriorMonths"/><display:column property="GREATER_THIRTY" title="${varPriorMonths}" class="alignright" sortable="true" format="<%=strCurrFmt%>"  total="true" />
		  	<fmtPendingPOSummary:message key="LBL_TOTAL_AMOUNT" var="varTotalAmount"/><display:column property="TOT_AMOUNT"  title="${varTotalAmount}" class="alignright" sortable="true" format="<%=strCurrFmt%>" total="true" />
		  	
		  	<display:footer media="html"> 
<% 
		HashMap temp = ((HashMap)pageContext.getAttribute("POtotal"));
  		String strCurVal = ((HashMap)pageContext.getAttribute("POtotal")).get("column5").toString();//" $ " + GmCommonClass.getStringWithCommas(strVal);
  		String str1530 =((HashMap)pageContext.getAttribute("POtotal")).get("column6").toString(); //" $ " + GmCommonClass.getStringWithCommas(strVal);
  		String str30plus =((HashMap)pageContext.getAttribute("POtotal")).get("column7").toString();// " $ " + GmCommonClass.getStringWithCommas(strVal);
  		String strTotal = ((HashMap)pageContext.getAttribute("POtotal")).get("column8").toString();//" $ " + GmCommonClass.getStringWithCommas(strVal); 
%>
	<!-- Custom tag lib code modified for JBOSS migration changes -->
  	<tr class = shade>
  				<td> <B> </B></td>
  				<td> <B> </B></td>
  				<td> <B> </B></td>
		  		<td> <B> <fmtPendingPOSummary:message key="LBL_TOTAL"/> : </B></td>
		  		<td  Height="25"   class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strCurVal)%></td>
		  		<td  Height="25"   class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(str1530)%></td>
		  		<td  Height="25"   class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(str30plus)%></td>
		  		<td  Height="25"   class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strTotal)%></td>
	</tr>
 </display:footer>
		  	
		</display:table>
		</td>
	</tr>
</table>	


</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
