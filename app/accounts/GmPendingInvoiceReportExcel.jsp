 <%@page import="java.util.Locale"%>
<%
/**********************************************************************************
 * File		 		: GmPendingInvoiceReportExcel.jsp
 * Desc		 		: This screen is used for the Account Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtPendingInvoiceReportExcel" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmPendingInvoiceReportExcel.jsp -->

<%

Locale locale = null;
String strLocale = "";

String strJSLocale = "";

String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));

if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
String format = request.getParameter("hMode");
if ((format != null) && (format.equals("Excel"))) {
response.setContentType("application/vnd.ms-excel");
response.setHeader("Content-disposition","attachment;filename=Report.xls");
}


	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);


	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	String strCommonPath = GmCommonClass.getString("GMCOMMON");
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	ArrayList alReturn = (ArrayList)hmReturn.get("REPORT");
	int intLength = alReturn.size();

	int intSize = 0;
	HashMap hmLoop = new HashMap();
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	String strcurrSymbol = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_SYMB"));
	String strParentFl = GmCommonClass.parseNull((String)request.getAttribute("PARFLAG"));
	String strInvSource = GmCommonClass.parseNull((String)request.getAttribute("INVSOURCE"));
	String strCategory = GmCommonClass.parseNull((String)request.getAttribute("strCategory"));
	String strAccId = "";
	String strAccName = ""; 
	String strInvId = "";
	String strInvDate = "";
	String strSales = "";
	String strDay = "";
	String strPO = "";
	String strPrice = "";
    String strColspan ="4";
    String strColspn ="6";
	String strShade = "";
%>
<fmtPendingInvoiceReportExcel:setLocale value="<%=strLocale%>"/>
<fmtPendingInvoiceReportExcel:setBundle basename="properties.labels.accounts.GmPendingInvoiceReport"/>
<HTML>
<HEAD>
<TITLE> Globus Medical: Pending Invoices Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style>
.RightTableCaption {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
}

.Line{
	background-color: #676767;
}
TR.ShadeRightTableCaption{
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
	background-color: #cccccc;
}
.RightText {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
}
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10">
	<table border="0" width="550" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" bgcolor="#666666" height="1"></td>
		</tr>
		<tr>
			<td width="1"></td>
			<td width="550" valign="top">
				<table border="0"  width="100%" cellspacing="0" cellpadding="0">
					<tr class="RightTableCaption">
						<td height="25" align="center" colspan="6"><font size="+2"><fmtPendingInvoiceReportExcel:message key="LBL_REPORT_INVOICES"/></font></td>
					</tr>
					<%if(!strParentFl.equals("false")){ 
					 strColspn="7";
					}if((strCategory.equals("Y")) && strParentFl.equals("true")){
					strColspn="8";
					}else if((strCategory.equals("Y")) && strParentFl.equals("false")){ 
						strColspn="7";
					}%>
					<tr><td class="Line" height="1" colspan="<%=strColspn%>"></td></tr>
					<tr class="ShadeRightTableCaption">
					<%if(strInvSource.equals("26240213")){ %>
					<td HEIGHT="24" width="300"><fmtPendingInvoiceReportExcel:message key="LBL_DEALER_NAME"/></td>
					<td width="24" align="center"><fmtPendingInvoiceReportExcel:message key="LBL_DEALER_ID"/></td>
					<td width="200" align="center"><fmtPendingInvoiceReportExcel:message key="LBL_INVOICE_ID"/></td>
					
					<%}else{ %>
                    <%if(!strParentFl.equals("false")){ %>
					<td HEIGHT="24" width="300"><fmtPendingInvoiceReportExcel:message key="LBL_PARENT_ACCOUNT_NAME"/></td>
					<td HEIGHT="24" width="120"><fmtPendingInvoiceReportExcel:message key="LBL_ACCOUNT_ID"/></td>
					<%if(strCategory.equals("Y")){ %>
							<td width="80" align="center"><fmtPendingInvoiceReportExcel:message key="LBL_CATEGORY"/></td>
					<%}%>
					<td width="300" align="center"><fmtPendingInvoiceReportExcel:message key="LBL_INVOICE_ID"/></td>
					<% }else{%>
					<td HEIGHT="24" width="300"><fmtPendingInvoiceReportExcel:message key="LBL_REP_ACCOUNT_NAME"/></td>
					<%if(strCategory.equals("Y")){ %>
							<td width="80" align="center"><fmtPendingInvoiceReportExcel:message key="LBL_CATEGORY"/></td>
					<%}%>
					<td width="200" align="center"><fmtPendingInvoiceReportExcel:message key="LBL_INVOICE_ID"/></td>
					
					<%}%>
					<%} %>
					
       				<td width="160" align="center"><fmtPendingInvoiceReportExcel:message key="LBL_INVOICE_DATE"/></td>
					<td width="160" align="center"><fmtPendingInvoiceReportExcel:message key="LBL_CUSTOMER_PO"/></td>
					<td width="100" align="center"><fmtPendingInvoiceReportExcel:message key="LBL_INOVICE_AMOUNT"/></td>
					<td width="100" align="center"><fmtPendingInvoiceReportExcel:message key="LBL_AMOUNT_PENDING"/></td></tr>
<%
						if (intLength > 0)
						{
							HashMap hmTempLoop = new HashMap();

							String strNextId = "";
							int intCount = 0;
							String strLine = "";
							String strTotal = "";
							String strAcctTotal = "";
							String strPdAmt = "";
							String strPendingAmt = "";
							String strPendingTotal = "";
							String strGrandTotal = "";
							String strParentAccId = "";
							String strParentAccName = "";
							String strCategoryVal = "";
							double invAmt = 0.0;
							double pdAmt = 0.0;
							double pendingAmt = 0.0;
							boolean blFlag = false;
							double dbSales = 0.0;								
							double dbAcctTotal = 0.0;
							double dbTotal = 0.0;
							double dbPendingTotal = 0.0;
							double dbGrandTotal = 0.0;
							java.sql.Date dtInvDate = null;
							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alReturn.get(i);
								if (i<intLength-1)
								{
									hmTempLoop = (HashMap)alReturn.get(i+1);
									// if Source Dealer or Parent account flag unchecked then, show the Account information otherwise show the parent order information.
									if(strInvSource.equals("26240213") || ((strParentFl.equals("false")) || (strParentFl.equals("")))){
									  strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("ACCID"));
									}
									else{
									  strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("PARENTACCTID"));
									}
								}

								strAccId = GmCommonClass.parseNull((String)hmLoop.get("ACCID"));
								strParentAccId = GmCommonClass.parseNull((String)hmLoop.get("PARENTACCTID"));
								strParentAccName = GmCommonClass.parseNull((String)hmLoop.get("PARENTACCTNM"));
								strAccName = GmCommonClass.parseNull((String)hmLoop.get("NAME"));
								strAccName = (!strParentFl.equals("false")&&(!strParentFl.equals("")))?strParentAccName:strAccName;
								// if Source - Dealer or Parent account unchecked then, to set the parenet account id to - Rep Account id
								if(strInvSource.equals("26240213") || ((strParentFl.equals("false")) || (strParentFl.equals("")))){
								  strParentAccId = strAccId;
								}
								strInvId = GmCommonClass.parseNull((String)hmLoop.get("INVID"));
								strCategoryVal = GmCommonClass.parseNull((String)hmLoop.get("CUSTCATEGORY"));
								dtInvDate = (java.sql.Date)hmLoop.get("INVDT");
								strInvDate = GmCommonClass.getStringFromDate(dtInvDate,strApplDateFmt);
								strPrice = GmCommonClass.parseZero((String)hmLoop.get("COST"));
								strPO = GmCommonClass.parseZero((String)hmLoop.get("PO"));
								strPdAmt = GmCommonClass.parseZero((String)hmLoop.get("AMTPAID"));
								
								invAmt = Double.parseDouble(strPrice);
								pdAmt = Double.parseDouble(strPdAmt);
								pendingAmt =  invAmt - pdAmt; 
								strPendingAmt = ""+pendingAmt;

								dbSales = Double.parseDouble(strPrice);
								dbAcctTotal = dbAcctTotal + dbSales;
								dbPendingTotal = dbPendingTotal + pendingAmt;
								dbGrandTotal = dbGrandTotal + pendingAmt;
								dbTotal = dbTotal + dbSales;
							
								if (strParentAccId.equals(strNextId))
								{
									intCount++;
									strLine = "<TR><TD colspan="+strColspn+" height=1 class=Line></TD></TR>";
								}
								else
								{
										strLine = "<TR><TD colspan=6 height=1 class=Line></TD></TR>";
									if(strCategory.equals("Y") && strParentFl.equals("false"))
										strLine = "<TR><TD colspan=7 height=1 class=Line></TD></TR>";
									if(strCategory.equals("Y") && strParentFl.equals("true"))
										strLine = "<TR><TD colspan=8 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
										strAccName = "";
										strLine = "";
									}
									else
									{
										blFlag = true;
									}
									intCount = 0;
									//
								}
								strAcctTotal = ""+dbAcctTotal;
								strPendingTotal = ""+ dbPendingTotal;

								if (intCount > 1)
								{
									strAccName = "";
									strLine = "";
								}
							
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows

								out.print(strLine);
								strShade	= (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
								if (intCount == 1 || blFlag)
								{
%>
					<tr class="Shade">
						<td colspan="5" height="15" class="RightText">&nbsp;<b><%=strAccName%></b></td>
					</tr>
<%
								blFlag = false;
								}
								
							
%>
					<tr >
					     <td class="RightText" height="20">&nbsp;</td>
						<%if(!strParentFl.equals("false")||(strParentFl.equals(""))){ %>
						    <td class="RightText" height="20"><%=strAccId %></td>
						    <% strColspan="5";%>		   
						<%}%>
						 <% if((strCategory.equals("Y"))){	%>						
							<td width=80  align=center>&nbsp;<%=strCategoryVal%> </td>
							 <% strColspan="6";%>
							  <%strColspn="8"; %>					
						<%}if((strCategory.equals("Y")) && strParentFl.equals("false")){ %> 
							<% strColspan="5";%>
							  <%strColspn="7"; %>
						<%} %>
						<td class="RightText">&nbsp;<%=strInvId%></td>
						<td class="RightText">&nbsp;<%=strInvDate%></td>
						<td width="150" class="RightText">&nbsp;<%=strPO%></td>
						<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strPrice)%></td>
						<td align="right" class="RightText"><%=GmCommonClass.getStringWithCommas(strPendingAmt)%></td>
					</tr>
<%					
					if ( intCount == 0 || i == intLength-1)
					{
%>
					<tr>
						<td class="RightText" colspan="<%=strColspan%>" align="right" ></td>
						<td class="Line" height="1"></td>
						
						<td class="Line" height="1"></td>
					</tr>
					<tr>
						<td class="RightText" colspan="<%=strColspan%>" align="right">&nbsp;<fmtPendingInvoiceReportExcel:message key="LBL_ACCOUNT_TOTAL"/> =</td>
						<td class="RightText" align="right"><%=strcurrSymbol%><%=GmCommonClass.getStringWithCommas(strAcctTotal)%></td>
					
						<td class="RightText" align="right"><%=strcurrSymbol%><%=GmCommonClass.getStringWithCommas(strPendingTotal)%></td>
					</tr>
<%
						dbAcctTotal = 0.0;
						dbPendingTotal = 0.0;

					}
				strTotal = ""+dbTotal;
				strGrandTotal = ""+ dbGrandTotal;
				}// end of FOR
%>
					<tr><td colspan="<%=strColspn%>" height="1" class="Line"></td></tr>
					<tr>
						<td align="right"  class="RightTableCaption" height="20" colspan="<%=strColspan%>"><fmtPendingInvoiceReportExcel:message key="LBL_GRAND_TOTAL"/>&nbsp;</td>
						<td class="RightTableCaption" align="right"><%=strcurrSymbol%><%=GmCommonClass.getStringWithCommas(strTotal)%></td>
						<td align="right"  class="RightTableCaption" height="20" > <%=strcurrSymbol%><%=GmCommonClass.getStringWithCommas(strGrandTotal)%></td>
					</tr>
<%
						} // End of IF
						else{
%>
					<tr><td colspan="5" class="Line"></td></tr>					
					<tr><td colspan="5" class="RightTextRed" height="50" align=center><fmtPendingInvoiceReportExcel:message key="LBL_NO_INVOICES_RAISED_SO_FAR"/> </td></tr>
<%					
						}
%>
				</table>
			</td>
			<td width="1"></td>
		</tr>
    </table>

</FORM>
</BODY>

</HTML>
