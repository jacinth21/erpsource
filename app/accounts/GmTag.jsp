<%
	/**********************************************************************************
	 * File		 		: GmTag.jsp
	 * Desc		 		: This screen is used tag entry or edit
	 * Version	 		: 1.0
	 * author			: Xun
	 * 
	 ************************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ taglib prefix="fmtTag" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmTag.jsp -->
<fmtTag:setLocale value="<%=strLocale%>"/>
<fmtTag:setBundle basename="properties.labels.accounts.GmTag"/>
<bean:define id="returnList1" name="frmTag" property="returnList1" type="java.util.List"></bean:define>
<bean:define id="returnList2" name="frmTag" property="returnList2" type="java.util.List"></bean:define>
 
<%
	DynaBean dbReturnDtls =null;
	ArrayList alList1 = new ArrayList();
	ArrayList alList2 = new ArrayList();	
	 
	alList1 = GmCommonClass.parseNullArrayList((ArrayList) returnList1);
	alList2 = GmCommonClass.parseNullArrayList((ArrayList) returnList2);
	int rowsize1 = alList1.size();
	int rowsize2 = alList2.size();
	
	StringBuffer sbInput = new StringBuffer();
	String strPar = "";
	String strInput = "";
	  
 	
//	String strWikiTitle = GmCommonClass.getWikiTitle("REQUEST_SWAP");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Part Tag Info</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"
	media="print" />

<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script> 

function fnTagHistory(tagid)
{ 	 
	windowOpener("/gmTagHistory.do?tagID="+tagid,"TagHistory","resizable=yes,scrollbars=yes,top=250,left=300,width=800,height=600");
} 
 
function fnTagVoid(tagid){
            document.frmTag.hCancelType.value = 'VODTG'           
            document.frmTag.hTxnId.value = tagid;//document.frmTag.tagID.value;
            document.frmTag.action ="/GmCommonCancelServlet";
            document.frmTag.hAction.value = "Load";                     
            document.frmTag.submit();
      }

function fnTagUnlink(tagid){
            document.frmTag.hCancelType.value = 'UNKTG'           
            document.frmTag.hTxnId.value = tagid;//document.frmTag.tagID.value;
            document.frmTag.action ="/GmCommonCancelServlet";
            document.frmTag.hAction.value = "Load";                     
            document.frmTag.submit();
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmTag.do">
	<html:hidden property="strOpt" value="" />
	<input type="hidden" name="hTxnId" value=""/>
	<input type= "hidden" name="hTxnName" value=""/>
	<input type= "hidden"  name="hCancelType" value=""/>
	<input type="hidden" name="hAction" >
	<input type="hidden" name="hinputStr" >
	<input type="hidden" name="refType" > 
	<html:hidden property="pnum"   />
	 <html:hidden property="refID" />
	 
	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4"><fmtTag:message key="LBL_PART_TAG_INFO"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			  
	       </td>
		</tr>
		<tr>
			<td bgcolor="#666666" height="1" colspan="7"></td>
		</tr>
		 
	 
		<tr>
			<td class="Line" height="1" colspan="7"></td>
		</tr>
		 
		 

		<tr>
			<td class="LLine" height="1" colspan="7"></td>
		</tr>
		 
		<tr>
			<td colspan="7" align="center">&nbsp;&nbsp; <p>&nbsp;</p>  
                    
         </td>
		</tr>               
		 <table border="0"   class="DtTable850" cellspacing="0"  cellpadding="0"  >
					  <thead>
						<TR bgcolor="#EEEEEE" colspan="7">
							<TH class="RightText"  >&nbsp;<fmtTag:message key="LBL_TAG"/> #</TH>
							<TH class="RightText"  ><fmtTag:message key="LBL_PART"/> # </TH>
							<TH class="RightText"  ><fmtTag:message key="LBL_CONTROL"/> #</TH>
							<TH class="RightText"  >&nbsp;<fmtTag:message key="LBL_REF_ID"/></TH>
							<TH class="RightText"  ><fmtTag:message key="LBL_SET_ID"/></TH>
							<TH class="RightText"  ><fmtTag:message key="LBL_LOCATION_TYPE"/></TH>
							<TH class="RightText" ><fmtTag:message key="LBL_LOCATION"/></TH>
						</TR>	
					  </thead>
					  <TBODY>
					  <%
					  	String strTag = "";
						String strPnum = "";
						String strControlNum = "";
						String strRefId = "";
						String strSetId = "";
						String strLocationType = "";
						String strLocation = ""; 
						int rowcount =0;
						
						
						for (int i=0; i < rowsize1;i++)
						{ 	
							dbReturnDtls =  (DynaBean)alList1.get(i) ;
							strTag =  GmCommonClass.parseNull((String)dbReturnDtls.get("TAGID")) ; 
							strPnum = GmCommonClass.parseNull((String)dbReturnDtls.get("PNUM"));
							strControlNum = GmCommonClass.parseNull((String)dbReturnDtls.get("CONTROLNUM"));
							strRefId = GmCommonClass.parseNull((String)dbReturnDtls.get("REFID"));
							strSetId = GmCommonClass.parseNull((String)dbReturnDtls.get("SETID"));
							strLocationType = GmCommonClass.parseNull((String)dbReturnDtls.get("LOCATION_TYPE"));
							strLocation = GmCommonClass.parseNull((String)dbReturnDtls.get("LOCATION")); 
	
	                %>
					<tr colspan="7" height="20">
						<td class="RightText" align="center">&nbsp;<a href=javascript:fnTagVoid('<%=strTag%>')>  <img   style='cursor:hand' src = '<%=strImagePath%>/void.jpg'  width='14' height='15' />  </a>
						<a href=javascript:fnTagUnlink('<%=strTag%>')>  <img   style='cursor:hand' src = '<%=strImagePath%>/tag_unlink.jpg'  width='14' height='15' />  </a>
						<a href=javascript:fnTagHistory('<%=strTag%>')>  <img   style='cursor:hand' src='<%=strImagePath%>/icon_History.gif'  width='14' height='15' />  </a>
						<%=strTag%>  </td>
						<td class="RightText" align="center">&nbsp;<%=strPnum%></td>
						<td class="RightText" align="center">&nbsp;<%=strControlNum%></td>
						<td class="RightText" align="center">&nbsp;<%=strRefId%></td>
						<td class="RightText" align="center">&nbsp;<%=strSetId%></td>
						<td class="RightText" align="center">&nbsp;<%=strLocationType%></td>
						<td class="RightText" align="center">&nbsp;<%=strLocation%></td> 
					</tr>
					<!--<tr><td bgcolor="#eeeeee" height="1" colspan="11"></td></tr>-->
				   <%
						 
					}
	               
 
						for (int j=0; j < rowsize2;j++)
						{
							dbReturnDtls = (DynaBean)alList2.get(j);
						 	String strItemQty =   String.valueOf( dbReturnDtls.get("ITEM_QTY"))  ; 
						 	String strTagCount =   String.valueOf(dbReturnDtls.get("TAGCOUNT"))  ;
						 
						 	int rowloop = Integer.parseInt(strItemQty) - Integer.parseInt(strTagCount);
						 	rowcount = rowloop + rowcount;
						// 	System.out.println("rowloop>>>>>>>>>>>>>........" + rowloop);
							for (int i=0; i < rowloop;i++)
							{ 	
								dbReturnDtls =  (DynaBean)alList2.get(j) ;
								strTag =  GmCommonClass.parseNull((String)dbReturnDtls.get("TAGID")) ; 
								strPnum = GmCommonClass.parseNull((String)dbReturnDtls.get("PNUM"));
								strControlNum = GmCommonClass.parseNull((String)dbReturnDtls.get("CONTROLNUM"));
								strRefId = GmCommonClass.parseNull((String)dbReturnDtls.get("REFID"));
								strSetId = GmCommonClass.parseNull((String)dbReturnDtls.get("SETID"));
								strLocationType = GmCommonClass.parseNull((String)dbReturnDtls.get("LOCATION_TYPE"));
								strLocation = GmCommonClass.parseNull((String)dbReturnDtls.get("LOCATION")); 
		
		                %> 
						<tr colspan="7"  height="20">
							<td class="RightText" align="center">&nbsp;<input type="text" size="15" class=InputArea value="<%=strTag%>" name="TagID<%=i%>"   onFocus="changeBgColor(this,'#AACCE8');"> </td>
							<td class="RightText" align="center">&nbsp;<input type=hidden value='<%=strPnum%>' name="PartNum<%=i%>"><%=strPnum%></td>
							<td class="RightText" align="center">&nbsp;<input type=hidden value='<%=strControlNum%>' name="ControlNum<%=i%>"><%=strControlNum%></td>
							<td class="RightText" align="center">&nbsp;<input type=hidden value='<%=strRefId%>' name="RefID<%=i%>"><%=strRefId%></td>
							<td class="RightText" align="center">&nbsp;<input type=hidden value='<%=strSetId%>' name="SetID<%=i%>"><%=strSetId%></td>
							<td class="RightText" align="center">&nbsp;<input type=hidden value='<%=strLocationType%>' name="LocationType<%=i%>"><%=strLocationType%></td>
							<td class="RightText" align="center">&nbsp;<input type=hidden value='<%=strLocation%>' name="Location<%=i%>"><%=strLocation%></td> 
						</tr>
						<!--<tr><td bgcolor="#eeeeee" height="1" colspan="11"></td></tr>-->
					   <%
							}
						}
		               %>
					  </TBODY>
					  <script>
 function fnSubmit()
{
	 
	var varRows =  '<%=rowcount%>'  ;
    
	var inputString = '';   
	
	 
	for (var k=0; k < varRows; k++) 
	{
		
		objTag = eval("document.frmTag.TagID"+k);
		 	
		if(objTag.value =='')
		{
		Error_Details(message_accounts[284]);
		}
		objPartNum = eval("document.frmTag.PartNum"+k); 
		objControlNum = eval("document.frmTag.ControlNum"+k);
		objRefID = eval("document.frmTag.RefID"+k);
		objSetID = eval("document.frmTag.SetID"+k);
		objLocationType = eval("document.frmTag.LocationType"+k);
		objLocation = eval("document.frmTag.Location"+k);
		 
		 LocationType  = 4120 ;
		 
		inputString = inputString + objTag.value + '^'+ objControlNum.value +'^'+ objPartNum.value + '^'+ objSetID.value +'^' + document.frmTag.refType.value +'^' + objRefID.value +'^'+ LocationType
		 + '^'+ objLocation.value  +  '|'; 
	  
	} 
	if (ErrorCount > 0)
		{
				Error_Show();
				Error_Clear();
				return false;
		}  
	 
//	 alert("varRows is " + varRows);
	 
	document.frmTag.strOpt.value =  'save'; //'save';
	document.frmTag.hinputStr.value = inputString;
   	document.frmTag.submit();	
  }
	 </script> 
					<tr>
						<td colspan="7" align="center">&nbsp;&nbsp; <p>&nbsp;</p> 
						<fmtTag:message key="BTN_SUBMIT" var="varSubmit"/> 
			                         <gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save"  onClick="fnSubmit();" /> 
			                         <p>&nbsp;</p>
			         </td>
					</tr> 
					</table>
	 
		
		 	
		 
	</table>
 
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>