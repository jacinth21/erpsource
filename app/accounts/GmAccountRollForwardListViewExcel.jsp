 <%@page import="java.util.Locale"%>
<%
/**********************************************************************************
 * File		 		: GmAccountRollForwardListViewExcel.jsp 
 * Desc		 		: This screen is used to display Account RollForward Report in List View
 * Version	 		: 1.0 
 * author			: Rajeshwaran
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtAccountRollforwardExcel" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmAccountRollForwardListViewExcel.jsp-->

<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	Locale locale = null;
	String strLocale = "";

	String strJSLocale = "";

	String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));


	if(!strSessCompanyLocale.equals("")){
		locale = new Locale("en", strSessCompanyLocale);
		strLocale = GmCommonClass.parseNull((String)locale.toString());
		strJSLocale = "_"+strLocale;
	}
	
	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	String strCommonPath = GmCommonClass.getString("GMCOMMON");
	
	ArrayList alOpenBalance = new ArrayList();
	ArrayList alDetailList = new ArrayList();
	ArrayList alAccountList = new ArrayList();
	HashMap hmTempLoop = new HashMap();
						
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	
	// Opening Balance and det
	alOpenBalance 	= (ArrayList)hmReturn.get("OPEN_BALANCE");
	alDetailList	= (ArrayList)hmReturn.get("DETAIL_LIST");
	alAccountList	= (ArrayList)request.getAttribute("alAccountList");
	
	int intLength = alOpenBalance.size();
	int intDetailList = alDetailList.size();
	
	String strEList		= GmCommonClass.parseNull((String)request.getAttribute("hElementList"));
	String strFrmDate 	= GmCommonClass.parseNull((String)request.getAttribute("hFrom"));
	String strToDate 	= GmCommonClass.parseNull((String)request.getAttribute("hTo"));

	HashMap hmLoop = new HashMap();

	String strAccId = "";
	String strAccName = "";
	String strOrdId = "";
	String strCodeID = "";
	String strElemId = "";
	String strSelected = "";
	String strOrdDate = "";
	String strCSPerson = "";
	String strPrice = "";

	String strShade = "";
	String strCurCnt="";
	String strTxnCompanyNm = "";
	//Hide/Show Owner Company footer total based on currency count
	if(intDetailList!=0)
	{
	HashMap hmCurCnt = GmCommonClass.parseNullHashMap((HashMap)alDetailList.get(0));
	strCurCnt=GmCommonClass.parseNull((String)hmCurCnt.get("CUR_CNT"));
	}
	String strColspan = "5";
%>
<fmtAccountRollforwardExcel:setLocale value="<%=strLocale%>"/>
<fmtAccountRollforwardExcel:setBundle basename="properties.labels.accounts.GmAccountRollForwardExcel"/>
<HTML>
<HEAD>
<TITLE> Globus Medical: Account Rollforward Exceldown (List View) </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>

<BODY leftmargin="0" topmargin="0">
<FORM name="frmAccount">

			
				<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
					
					<tr class="ShadeRightTableCaption">
					<!-- Added Transaction company name column in because of PMT-51247 -->
						<td HEIGHT="24" width="300"><font color="blue"> <fmtAccountRollforwardExcel:message key="LBL_TXN_COMPANY"/></font></td>
						<td HEIGHT="24" width="300"><font color="blue"> <fmtAccountRollforwardExcel:message key="LBL_ACCOUNT_NAME_ONLY"/></font></td>
						<td HEIGHT="24"  align="left" ><font color="blue"><fmtAccountRollforwardExcel:message key="LBL_SAGE_ACC_ID"/></font></td>
						<td HEIGHT="24" width="150"><font color="blue"> <fmtAccountRollforwardExcel:message key="LBL_TXN_TYPE"/></font></td>
						<td HEIGHT="24"  align="left" ><font color="blue"><fmtAccountRollforwardExcel:message key="LBL_OWNER_COMPANY"/></font></td>
						<td HEIGHT="24"  align="left" ><font color="blue"><fmtAccountRollforwardExcel:message key="LBL_OWNER_CURRENCY"/></font></td>
						<td HEIGHT="24"  align="right" width="100" ><font color="blue"><fmtAccountRollforwardExcel:message key="LBL_AMOUNT"/></font></td>
						<td HEIGHT="24"  align="right" ><font color="blue"><fmtAccountRollforwardExcel:message key="LBL_LOCAL_CURRENCY"/></font></td>
						<td HEIGHT="24"  align="right" ><font color="blue"><fmtAccountRollforwardExcel:message key="LBL_LOCAL_AMOUNT"/></font></td>						
						<td HEIGHT="24"><fmtAccountRollforwardExcel:message key="LBL_DIVISION"/></td>
					</tr>
<%				if (intDetailList > 0)
				{
						String strNextId = "";
						int intCount = 0;
						String strLine = "";
						String strTotal = "";
						String strAmt = "";
						String strOwnerCompanyNm = "";
						String strOwnerCurrency = "";
						String strDetailAccount = "";
						String strLocalAmt = "";
						String strLocalCurrency = "";
						String strOwnerId= "";
						String strDetailOwnerId= "";
						String strSageAccId = "";
						String strDivisionName = "";
						
						double dbTotal = 0.0;
						double dbLocalTotal = 0.0;
						for (int j =0;j < intDetailList; j++)
						{
							hmLoop = (HashMap)alDetailList.get(j);
							strDetailAccount = (String)hmLoop.get("A_NAME");

								strShade	= (intCount%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
								strAmt		= GmCommonClass.parseZero((String)hmLoop.get("AMT"));
								strOwnerCompanyNm = GmCommonClass.parseNull((String)hmLoop.get("OWNER_COMPANY"));
								strOwnerCurrency = GmCommonClass.parseNull((String)hmLoop.get("OWNER_CURRENCY"));
								strLocalAmt = GmCommonClass.parseZero((String)hmLoop.get("LOCAL_AMT"));
								strLocalCurrency= GmCommonClass.parseNull((String)hmLoop.get("LOCAL_CURRENCY"));
								strSageAccId = GmCommonClass.parseNull((String)hmLoop.get("SAGE_ACC_ID"));
								strTxnCompanyNm = GmCommonClass.parseNull((String)hmLoop.get("TXN_COMPANY_NM"));
								strDivisionName =  GmCommonClass.parseNull((String)hmLoop.get("DIVISION_NAME"));
								dbTotal 	= dbTotal + Double.parseDouble(strAmt);
								dbLocalTotal 	= dbLocalTotal + Double.parseDouble(strLocalAmt);

%>							
						<tr <%=strShade%> >
							<td height="20" class="RightText"><%=strTxnCompanyNm%></td>
							<td height="20" class="RightText"  width="300">
								 <%=strDetailAccount%></td>
							<td height="20" class="RightText" align="left"><%= strSageAccId %></td>	 
							<td height="20" class="RightText"  width="150">
								 <%=(String)hmLoop.get("TXN_NAME")%></td>
							
							<td height="20" class="RightText"><%= strOwnerCompanyNm %></td>
							<td height="20" class="RightText" ><%= strOwnerCurrency %></td>			
							<td height="20" class="RightText" align="right">
								<%= GmCommonClass.getRedText(GmCommonClass.getStringWithCommas((String)hmLoop.get("AMT"))).replace("&nbsp", "") %></td>
							<td height="20" class="RightText"><%= strLocalCurrency %></td>		
							<td height="20" class="RightText" align="right">
								<%= GmCommonClass.getRedText(GmCommonClass.getStringWithCommas((String)hmLoop.get("LOCAL_AMT"))).replace("&nbsp", "") %></td>
									<td height="20" class="RightText">&nbsp;<%=strDivisionName %></td>	
							
						</tr>
						
							
<%							
						}// end of FOR loop j
						intCount++;
						strShade	= (intCount%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows						
%>						<tr <%=strShade%>>
							<td height="20" class="RightText"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtAccountRollforwardExcel:message key="LBL_ENDING_BALANCE"/></b></td>
							<td colspan="<%=strColspan%>">&nbsp;</td>	
							<%if(strCurCnt.equals("1")){ %>
							<td class="RightText" align="right">
								<%= GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(dbTotal+"")).replace("&nbsp", "") %></td>
							<td>&nbsp;</td>	
							<%} %>	
							<td height="20" class="RightText" align="right">
								<%= GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(dbLocalTotal+"")).replace("&nbsp", "") %></td>
						</tr>
	
						</table>
						<TR><TD colspan="6" height=1 class=Line></TD></TR>					
						
<%
						} // End of IF
						else{
%>
					<tr><td colspan="8" class="Line"></td></tr>					
					<tr><td colspan="8" class="RightTextRed" height="50" align=center><fmtAccountRollforwardExcel:message key="LBL_NO_SALES_PERIOD"/> ! </td></tr>
<%					
						}
%>
			

</FORM>
</BODY>

</HTML>
