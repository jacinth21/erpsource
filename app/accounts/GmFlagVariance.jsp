<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page language="java" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<%@ taglib prefix="fmtFlagVariance" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmFlagVariance.jsp -->
<fmtFlagVariance:setLocale value="<%=strLocale%>"/>
<fmtFlagVariance:setBundle basename="properties.labels.accounts.GmFlagVariance"/>
 <bean:define id="strVarianceType" name="frmFlagVarianceForm" property="strVarianceType" type="java.lang.String"> </bean:define>
 <bean:define id="setId" name="frmFlagVarianceForm" property="setId" type="java.lang.String"> </bean:define>
 <bean:define id="auditId" name="frmFlagVarianceForm" property="auditId" type="java.lang.String"> </bean:define>
 <bean:define id="deviationQty" name="frmFlagVarianceForm" property="deviationQty" type="java.lang.String"> </bean:define>
 <bean:define id="hmHeaderInfo" name="frmFlagVarianceForm" property="hmHeaderInfo" type="java.util.HashMap"> </bean:define>
<%
String strWikiTitle="";
String strHeader="";
String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.accounts.GmFlagVariance", strSessCompanyLocale);
//HashMap hmHeaderInfo=(HashMap)alHeaderInfo.get(1);

	if (strVarianceType.equals("negative")){
		 strWikiTitle = GmCommonClass.getWikiTitle("FLAG_NEGATIVE_QTY");  
		 strHeader=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_FLAG_NEGATIVE_QUANTITY"));
	}
	else{
		 strWikiTitle = GmCommonClass.getWikiTitle("FLAG_POSITIVE_QTY"); 
		 strHeader=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_FLAG_POSITIVE_QTY"));
	}
	String strSetDesc=GmCommonClass.parseNull((String)hmHeaderInfo.get("SET_DESC"));
	String strSystemName= GmCommonClass.parseNull((String)hmHeaderInfo.get("SYSTEM_NAME"));
	String strDistName= GmCommonClass.parseNull((String)hmHeaderInfo.get("DIST_NAME"));
	String strPartNum= GmCommonClass.parseNull((String)hmHeaderInfo.get("PNUM"));
	String strPartDesc= GmCommonClass.parseNull((String)hmHeaderInfo.get("PDESC"));
	String strPrice= GmCommonClass.parseNull((String)hmHeaderInfo.get("SET_PRICE"));
	String strCurDate= GmCommonClass.parseNull((String)hmHeaderInfo.get("CURDATE"));
%>
<script type="text/javascript">

</script>

<HTML>
<HEAD>
<TITLE> Globus Medical: Field Sales Report</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmFlagVariance.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script>

</script>
</HEAD>

<BODY onload="fnOnload();" leftmargin="20" topmargin="10">
<html:form  action="/gmFlagVariance.do">
<html:hidden property="strOpt"/>
<html:hidden property="auditId"/>
<html:hidden property="strVarianceType"/>
<html:hidden property="distId"/>
<html:hidden property="setId"/>
<html:hidden property="strInput"/>

	<table border="0" class="DtTable1000" width="1000" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="5" height="25" class="RightDashBoardHeader"><%=strHeader%></td>
			<td  height="25" class="RightDashBoardHeader">
			<fmtFlagVariance:message key="LBL_HELP" var="varHelp"/>
				<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr><td colspan="6" class="LLine" height="1"></td></tr>
		<tr  class="shade" >		
			<td height="30"   align ="Right" class="RightTableCaption"><fmtFlagVariance:message key="LBL_DISTRIBUTOR"/>:&nbsp;</td>
		    <td  ><%=strDistName %></td>					
				<td height="30"   align ="Right" class="RightTableCaption"><fmtFlagVariance:message key="LBL_SET_ID"/>:&nbsp;</td>
				<td  align ="left"><%=setId%></td>		
				<td  height="30" align ="Right" class="RightTableCaption"><fmtFlagVariance:message key="LBL_PART_NO"/>:&nbsp;</td>
				<td   align ="left"><%=strPartNum%></td>					
		
		</tr>	
		<tr><td colspan="6" class="LLine" height="1"></td></tr>
		<tr >		
			<td height="30"  align ="Right" class="RightTableCaption"><fmtFlagVariance:message key="LBL_SYSTEM"/>:&nbsp;</td>
		    <td><%=strSystemName %></td>					
				<td height="30" align ="Right" class="RightTableCaption"><fmtFlagVariance:message key="LBL_SET_DESC"/>:&nbsp;</td>
				<td   align ="left"><%=strSetDesc%></td>		
				<td  height="30" align ="Right" class="RightTableCaption"><fmtFlagVariance:message key="LBL_PART_DESC"/>:&nbsp;</td>
				<td align ="left"><%=strPartDesc%></td>					
		</tr>
		<tr><td colspan="6" class="LLine" height="1"></td></tr>
		<tr>
		<td align="center" colspan="6">
		<p> &nbsp;</p>
	<table border="0" offset="20" class="dttable950" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtFlagVariance:message key="LBL_AUDIT_DETAILS"/></td>
		</tr>		
		<tr><td bgcolor="#666666" height="1"></td></tr>
			<tr>
			<td>
		    	<display:table name="requestScope.frmFlagVarianceForm.alOverall" requestURI="/gmFlagVariance.do?auditId=<%=auditId%>&distId=<%=strDistName %>&setId=<%=setId%>" excludedParams="strOpt"  class="its"  > 
				<fmtFlagVariance:message key="LBL_AUDIT_COUNT" var="varAuditCount"/><display:column property="AUDIT_COUNT" title="${varAuditCount}"  />
				<fmtFlagVariance:message key="LBL_SYSTEM_QTY" var="varSystemQty"/><display:column property="SYSTEM_QTY" title="${varSystemQty}"   />
				<fmtFlagVariance:message key="LBL_DEVIATION_QTY" var="varDeviationQty"/><display:column property="DEVIATION_QTY" title="${varDeviationQty}" />	
				<fmtFlagVariance:message key="LBL_FLAGGED_QTY" var="varFlaggedQty"/><display:column property="FLAGGED_QTY" title="${varFlaggedQty}"  />		
				<fmtFlagVariance:message key="LBL_UNRESOLVED_QTY" var="varUnresolvedQty"/><display:column property="UNRESOLVED_QTY" title="${varUnresolvedQty}"  />			
				</display:table>  
			</td>
			</tr>
		</table>
		<td></td>
		</tr>
		
		<tr>
			<td align="center"  colspan="6">
			<logic:equal name="frmFlagVarianceForm" property="strVarianceType" value="positive">
					<jsp:include page="/accounts/GmAuditInfo.jsp" >
					<jsp:param name="FORMNAME" value="frmFlagVarianceForm" />
					</jsp:include>
			</logic:equal>
			<logic:equal name="frmFlagVarianceForm" property="strVarianceType" value="negative">	
						<jsp:include page="/accounts/GmSystemInfo.jsp" >
						<jsp:param name="FORMNAME" value="frmFlagVarianceForm" />
						</jsp:include>						
			</logic:equal>
			</td>
		</tr>	
	<tr>
	<td align="center" colspan="6">
	<p> &nbsp;</p>
	<table border="0" class="dttable950" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtFlagVariance:message key="LBL_RETURNS"/></td>
		</tr>		
		<tr><td  colspan="1">
		    <display:table name="requestScope.frmFlagVarianceForm.alReturnsReport" requestURI="/gmFlagVariance.do?auditId=<%=auditId%>&distId=<%=strDistName %>&setId=<%=setId%>" excludedParams="strOpt"  class="its"  > 
			<fmtFlagVariance:message key="LBL_TRANSACTION_ID" var="varTransId"/><display:column property="TXN_ID" title="${varTransId}"  />	
			<fmtFlagVariance:message key="LBL_TXN_TYPE" var="varTxnType"/><display:column property="TXN_TYPE" title="${varTxnType}"/>	
			<fmtFlagVariance:message key="LBL_CONTROL_NUMBER" var="varCnum"/><display:column property="CNUM" title="${varCnum}"  />
			<fmtFlagVariance:message key="LBL_QTY" var="varQty"/><display:column property="QTY" title="${varQty}"  />
			<fmtFlagVariance:message key="LBL_CREDIT_TYPE" var="varCreditDate"/><display:column property="CREDIT_DATE" title="${varCreditDate}"  />		
			</display:table>  
		</td>
		</tr>
</table>
</td>	
</tr>	
	<tr><td>
	<html:hidden property="price" value="<%=strPrice%>"/>
	<html:hidden property="curDate" value="<%=strCurDate%>"/>	
	</td></tr>	
</table>
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>