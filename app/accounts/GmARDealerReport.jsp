<%
/**********************************************************************************
 * File		 		: GmARDealerReport.jsp
 * author			: Jeeva Balaraman
************************************************************************************/
%>
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Date" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.PopChartUtils"%>
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="fmtArDealerReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmARDealerReport.jsp -->
<fmtArDealerReport:setLocale value="<%=strLocale%>"/>
<fmtArDealerReport:setBundle basename="properties.labels.accounts.GmARDealerReport"/>
<fmtArDealerReport:message key="LBL_CHOOSE_ONE" var="varChooseOne"/>
<%
	String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");	
	HashMap hm = GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strWikiTitle = GmCommonClass.getWikiTitle("REQUEST_MODIFY_VIEW");
	
	String strCurrSign = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_SYMB"));
	String strCurrId = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_ID"));
	String strAccountCurrency = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_ID"));
	ArrayList alReturn = (ArrayList)request.getAttribute("alReturn");
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	ArrayList alRegion = new ArrayList();
	ArrayList alDistributor = new ArrayList();
	ArrayList alCollector = (ArrayList)request.getAttribute("ALCOLLECTOR");	
	ArrayList alCompDiv = (ArrayList)request.getAttribute("ALCOMPDIV");
	String strARRptByDealerFlag = GmCommonClass.parseNull((String)request.getAttribute("ARREPORTBYDEALER"));
	ArrayList alSnapShot = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("AL_SNAPSHOT_DATE"));
	String strhScreenType = GmCommonClass.parseNull((String)request.getAttribute("HSCREENTYPE")); 
	String strInvId = "";
	String strAccId = "";

	HashMap hmLoop = new HashMap();
	int intLoop = 0;
	
	if (alReturn != null)
	{
		intLoop = alReturn.size();
	}
	String gridData = (String) request.getAttribute("alResult")==null?"":(String) request.getAttribute("alResult");
	
	int intDistSize = alDistributor.size();
	ArrayList alCompCurrency = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALCOMPANYCURRENCY"));
	String strReportType = GmCommonClass.parseNull((String)request.getAttribute("REPORTTYPE"));
	String strDefaultThrough = GmCommonClass.parseNull((String)request.getAttribute("DAFAULTTHROUGH"));
	String strInvSource = GmCommonClass.parseNull((String)request.getAttribute("INVSOURCE"));
    String strDivisionId 	= GmCommonClass. parseZero ((String)request.getAttribute("DIVISIONID"));
    
    GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.accounts.GmARDealerReport", strSessCompanyLocale);
    String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
    GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
   //PMT-14375 - Including ALL option for GM Japan to show
    String strRptByDealerFl = "";
	String strIncludeAllFl = "";
	strRptByDealerFl = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("AR_REPORT_BY_DEALER"));
	if(strRptByDealerFl.equals("YES")){
	  strIncludeAllFl = "Y";
	}
	String strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_AR_DEALER_REPORT"));
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: A/R Amount Report by Dealers</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmARDealerReport.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<script>
var objGridData;
objGridData = '<%=gridData%>';
var currSymbol = '<%=strCurrSign%>';
var reporttype = '<%=strInvSource%>';
var arCurrID = '<%=strCurrId%>';
var arRptByDealerFlag = '<%=strARRptByDealerFlag%>';
</script>

</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoad();">
<html:form action="/gmARDealerRpt.do?">
<input type="hidden" name="hInvId" value="<%=strInvId%>">
<input type="hidden" name="hAccId" value="<%=strAccId%>">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="reportType" value="<%=strReportType%>">
	

	<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
		<tr>			
			<td height="25" class="RightDashBoardHeader" colspan="5">
				<%=strHeader%>
			</td>	
			<fmtArDealerReport:message key="LBL_HELP" var="varHelp"/>
	       <td  height="25" class="RightDashBoardHeader" align="right"><img id='imgEdit' 
	       style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
	       </td>
		</tr>
		<tr><td class="Line" height="1" colspan="6"></td></tr>
		<!-- Addition Filter Information -->
		<tr><td colspan="6" height="25">
				<jsp:include page="/sales/GmSalesFilters.jsp" >
				<jsp:param name="FRMNAME" value="frmARDealerRpt" />
				<jsp:param name="HIDE" value="SYSTEM" />
				<jsp:param name="HIDEBUTTON" value="HIDEGO" />
				</jsp:include>
		</td></tr>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
		<tr>
			<td colspan="6" align="left" height="25">
			<!-- Added table to fix the alignment -->
			<table border="0" width="100%" cellspacing="0" cellpadding="0"><tr>
			<td width="75%">
				<jsp:include page="/accounts/GmIncludeAccountType.jsp">
					<jsp:param name="DISPACCFL" value="Y"/>
					<jsp:param name="REPACCTLABELFL" value="Y"/>
					<jsp:param name="INC_ALL_FL" value="<%=strIncludeAllFl%>"/>
				</jsp:include>
			</td>
			</tr></table></td>
		</tr>
		<tr><td class="LLine" height="1" colspan="6"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
		  <td class="LLine" colspan="7" height="1"></td>
		</tr>
		<tr class="Shade">
		<td class="RightTableCaption"height="25" align="right" width="10%"><fmtArDealerReport:message key="LBL_DIVISION_NAME"/>:
		   <gmjsp:dropdown width="100" controlName="Cbo_Division"  seletedValue="<%= strDivisionId %>" value="<%= alCompDiv %>" codeId = "DIVISION_ID" codeName = "DIVISION_NAME" defaultValue= "${varChooseOne}"  />
		</td>
		<fmtArDealerReport:message key="BTN_LOAD" var="varLoad"/>
		<fmtArDealerReport:message key="BTN_RESET" var="varReset"/>
		<td class="RightTableCaption" HEIGHT="30" align="right" width="10%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<gmjsp:button  value="${varLoad}" name="Btn_Load" gmClass="Button" style="width: 5em; height: 23px;" onClick="return fnDealerSales();" buttonType="Load" />&nbsp;&nbsp;
		 <gmjsp:button value="${varReset}" name="Btn_Reset" gmClass="Button" style="width: 5em; height: 23px;" onClick="fnReset();" buttonType="Save" />
		</td>
		<td colspan="7"></td>
		</tr>	
		<%if(!gridData.equals("")){%>
		<tr>
			<td colspan="6">
				<div  id ="ardealerdata" style="width:1160px;height:400px"></div>
			</td>
		</tr> 
		<tr>
            <td colspan="6" align="center">
               <div class='exportlinks'><fmtArDealerReport:message key="LBL_EXPORT_OPTIONS"/> : <img src='img/ico_file_excel.png' onclick="return fnDownloadXLS()"/>&nbsp;<a href="#"
                     onclick="return fnDownloadXLS()"> <fmtArDealerReport:message key="LBL_EXCEL"/> </a></div>
            </td>

		</tr>
		<%} %>
</table>
</html:form> 	
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>