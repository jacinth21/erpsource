<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page language="java" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtVarianceRptByDist" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmVarianceRptByDist.jsp -->
<fmtVarianceRptByDist:setLocale value="<%=strLocale%>"/>
<fmtVarianceRptByDist:setBundle basename="properties.labels.accounts.GmVarianceRptByDist"/>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
 <bean:define id="reportType" name="frmFieldSalesRptForm" property="reportType" type="java.lang.String"> </bean:define>
 <bean:define id="dispflaggedDet" name="frmFieldSalesRptForm" property="dispflaggedDet" type="java.lang.Boolean"> </bean:define>
 <bean:define id="ldtResult" name="frmFieldSalesRptForm" property="ldtResult" type="java.util.List"></bean:define>
<%
String strWikiTitle="";
String strHeader="";
String flaggedDet= dispflaggedDet.toString();
String header="";
String strTransferTo ="";
String strBorrowedFrom  ="";
String strExtra        ="";
String strReturns ="";
String strTransferFrom  ="";
String strLendTo       ="";
String strMissing      ="";
String strUnresolvedQty  ="";
String strFlaggedQty ="";
String strAccountsJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.accounts.GmVarianceRptByDist", strSessCompanyLocale);
	if (reportType.equals("globalSet")){
		 strWikiTitle = GmCommonClass.getWikiTitle("GLOBAL_SET");
		 strHeader=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_GLOBAL_VARIANCE_BY_SET"));
	}
	else{
		 strWikiTitle = GmCommonClass.getWikiTitle("VARIANCE_DISTRIBUTOR");
		 strHeader=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_VARIANCE_REPORT_BY_DISTRIBUTOR"));
	}
	int rowsize = ldtResult.size();
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Field Sales Report</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     
    table.DtTable1000 {
	width: 1100px;
	}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strAccountsJsPath%>/GmFieldSalesRpt.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script>

</script>
</HEAD>

<BODY onkeyup="fnEnter();" leftmargin="20" topmargin="10" b>
 
<html:form  action="/gmFieldSalesRpt.do?method=reportVarianceByDist"  >
<html:hidden property="strOpt"/>
<html:hidden property="reportType"/>
	<table border="0" class="DtTable1000" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" height="25" class="RightDashBoardHeader"><%=strHeader%></td>
			<td  height="25" class="RightDashBoardHeader">
			<fmtVarianceRptByDist:message key="LBL_HELP" var="varHelp"/>
				<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr  class="shade" >		
			<td height="30" align ="Right" class="RightTableCaption"><font color="red">*</font><fmtVarianceRptByDist:message key="LBL_AUDIT_LIST"/>:&nbsp;</td>
		    <td width="200"><gmjsp:dropdown controlName="auditId" SFFormName="frmFieldSalesRptForm" SFSeletedValue="auditId" 
								SFValue="alAuditName" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" onChange="fnReload()"/></td>
				
			<% if (!reportType.equals("globalSet")){	%>
			
				<td height="30" align ="Right" class="RightTableCaption"><font color="red">*</font><fmtVarianceRptByDist:message key="LBL_DISTRIBUTOR"/>:&nbsp;</td>
				<td  align ="left"><gmjsp:dropdown controlName="distId" SFFormName="frmFieldSalesRptForm" SFSeletedValue="distId" 
								SFValue="aldist" codeId = "DISTID"  codeName = "DISTNAME"  defaultValue= "[Choose One]"  /></td>
			<%} else { %>
				<td colspan="2"></td>					
			<% } %>						
		
		</tr>	
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		<tr>
			<td colspan="1">
				<jsp:include page="/accounts/GmVarianceRptFilter.jsp" >
					<jsp:param name="FORMNAME" value="frmFieldSalesRptForm" />
				</jsp:include>
			</td>
		</tr>
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		
		
		<tr>
			<td colspan="4" >
				<display:table name="requestScope.frmFieldSalesRptForm.ldtResult" requestURI="/gmFieldSalesRpt.do?method=reportVarianceByDist" export="true"  decorator="com.globus.accounts.displaytag.beans.DTFieldSalesRptWrapper" id="currentRowObject" varTotals="totals" freezeHeader="true" cellpadding="0" cellspacing="0" >
                    <display:setProperty name="export.excel.filename" value="Variance Distributor Rpt.xls" />
					<fmtVarianceRptByDist:message key="LBL_SET_ID" var="varSetId"/><display:column property="SET_ID"  title="${varSetId}" headerClass="ShadeDarkGrayTDCenter" style="text-align: left;"  class="ShadeLightGrayTD" sortable="true" />																	  
			 		<fmtVarianceRptByDist:message key="LBL_SET_DESC" var="VarSetDesc"/><display:column property="SET_DESC" title="${VarSetDesc}" headerClass="ShadeDarkGrayTDCenter"  style="text-align: left;width:150;"  sortable="true" class="ShadeLightGrayTD"  maxLength="20" titleKey="SETDESC"/>
			 		<display:column  property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>			 		
					<fmtVarianceRptByDist:message key="LBL_PART" var="varPart"/><display:column property="PNUM" title="${varPart}#"  style="text-align: left;"  sortable="true" class="ShadeLightGrayTD" headerClass="ShadeDarkGrayTDCenter" />
					<fmtVarianceRptByDist:message key="LBL_PART_DESC" var="varPartDescription"/><display:column property="PDESC" escapeXml="true" title="${varPartDescription}" style="text-align: left;width:150;"  sortable="true" class="ShadeLightGrayTD" headerClass="ShadeDarkGrayTDCenter" maxLength="20" titleKey="PDESC"/>
					<display:column  property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
					<fmtVarianceRptByDist:message key="LBL_SYSTEM" var="varSystem"/><display:column property="SYSTEM" title="${varSystem}"  sortable="true" headerClass="ShadeDarkGrayTDCenter"  style="width:50;text-align: left;" maxLength="6" titleKey="SYSTEM" class="ShadeLightGrayTD"/>
					<fmtVarianceRptByDist:message key="LBL_TYPE" var="varType"/><display:column property="TYPE" title="${varType}" sortable="true" headerClass="ShadeDarkGrayTDCenter" class="ShadeLightGrayTD" style="text-align: left;"/>
					<display:column  property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
					<fmtVarianceRptByDist:message key="LBL_AUDIT_QTY" var="varAudintCount"/><display:column property="AUDIT_QTY" title="${varAudintCount}" sortable="true" total="true" headerClass="ShadeDarkBlueTD" class="ShadeLightBlueTD"/>
					<fmtVarianceRptByDist:message key="LBL_SYSTEM_QTY" var="varSystemqty"/><display:column property="SYSTEM_QTY" title="${varSystemqty}" sortable="true" total="true" headerClass="ShadeDarkBlueTD" class="ShadeMedBlueTD"/>
					<fmtVarianceRptByDist:message key="LBL_DEVIATION_QTY" var="varDeviationQty"/><display:column property="DEVIATION_QTY" title="${varDeviationQty}" sortable="true" total="true" headerClass="ShadeDarkBlueTD" class="ShadeLightBlueTD" comparator="com.globus.common.displaytag.beans.DTComparator"/>
					<display:column  property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>				
					<logic:equal name="frmFieldSalesRptForm" property="dispflaggedDet" value="true" >						
						<fmtVarianceRptByDist:message key="LBL_TRANSFER_FROM" var="varTransferFrom"/><display:column property="TRANSFER_FROM" title="${varTransferFrom}" style="text-align: right;" sortable="true" total="true" headerClass="ShadeDarkBrownTD" class="ShadeLightBrownTD" />
						<fmtVarianceRptByDist:message key="LBL_EXTRA" var="varExtra"/><display:column property="EXTRA" title="${varExtra}" style="text-align: right;"  sortable="true" total="true" headerClass="ShadeDarkBrownTD" class="ShadeMedBrownTD" />
						<fmtVarianceRptByDist:message key="LBL_BORROWED_FROM" var="varBorrowedFrom"/><display:column property="BORROWED_FROM" title="${varBorrowedFrom}" style="text-align: right;"  sortable="true" total="true" headerClass="ShadeDarkBrownTD" class="ShadeLightBrownTD" />
						<display:column  property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
						<fmtVarianceRptByDist:message key="LBL_RETURNS" var="varReturns"/><display:column property="RETURNS" title="${varReturns}" style="text-align: right;"  sortable="true" total="true" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD" />
						<fmtVarianceRptByDist:message key="LBL_MISSING" var="varMissing"/><display:column property="MISSING" title="${varMissing}" style="text-align: right;"  sortable="true" total="true" headerClass="ShadeDarkGreenTD" class="ShadeLightGreenTD" />
						<fmtVarianceRptByDist:message key="LBL_TRANSFER_TO" var="varTrnasferTo"/><display:column property="TRANSFER_TO" title="${varTrnasferTo}" style="text-align: right;"  sortable="true" total="true" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD" />
						<fmtVarianceRptByDist:message key="LBL_LEND_TO" var="varLendTo"/><display:column property="LEND_TO" title="${varLendTo}" style="text-align: right;"  sortable="true" total="true" headerClass="ShadeDarkGreenTD" class="ShadeLightGreenTD" />
						<display:column  property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
						<fmtVarianceRptByDist:message key="LBL_UNRESOLVED_QTY" var="varUnresolvedQty"/><display:column property="UNRESOLVED_QTY" title="${varUnresolvedQty}" sortable="true" total="true" headerClass="ShadeDarkOrangeTD" class="ShadeLightOrangeTD" />
					</logic:equal>
					<logic:equal name="frmFieldSalesRptForm" property="dispflaggedDet" value="false">	
						<fmtVarianceRptByDist:message key="LBL_FLAGGED_QTY" var="varFalggedQty"/><display:column property="FLAGGED_QTY" title="${varFalggedQty}" sortable="true" total="true" headerClass="ShadeDarkOrangeTD" class="ShadeLightOrangeTD"/>			
						<fmtVarianceRptByDist:message key="LBL_UNRESOLVED_QTY" var="varUnresolvedQty"/><display:column property="UNRESOLVED_QTY" title="${varUnresolvedQty}" sortable="true" total="true" headerClass="ShadeDarkOrangeTD" class="ShadeMedOrangeTD" />			
					</logic:equal>
					<display:footer media="html">
					<%					 
					String strAuditQty = ((HashMap)pageContext.getAttribute("totals")).get("column10").toString();
					strAuditQty =   GmCommonClass.getStringWithCommas(strAuditQty,0);
					String strSystemQty = ((HashMap)pageContext.getAttribute("totals")).get("column11").toString();
					strSystemQty =   GmCommonClass.getStringWithCommas(strSystemQty,0);
					String strDeviationQty = ((HashMap)pageContext.getAttribute("totals")).get("column12").toString();
					strDeviationQty =   GmCommonClass.getStringWithCommas(strDeviationQty,0);
					if(flaggedDet.equals("true")){
						 strTransferTo = ((HashMap)pageContext.getAttribute("totals")).get("column14").toString();
						 strTransferTo =   GmCommonClass.getStringWithCommas(strTransferTo,0);
						 strExtra = ((HashMap)pageContext.getAttribute("totals")).get("column15").toString();
						 strExtra =   GmCommonClass.getStringWithCommas(strExtra,0);
						 strBorrowedFrom = ((HashMap)pageContext.getAttribute("totals")).get("column16").toString();
						 strBorrowedFrom =   GmCommonClass.getStringWithCommas(strBorrowedFrom,0);
						 strReturns= ((HashMap)pageContext.getAttribute("totals")).get("column18").toString();
						 strReturns =   GmCommonClass.getStringWithCommas(strReturns,0);
						 strMissing= ((HashMap)pageContext.getAttribute("totals")).get("column19").toString();
						 strMissing =   GmCommonClass.getStringWithCommas(strMissing,0);
						 strTransferFrom= ((HashMap)pageContext.getAttribute("totals")).get("column20").toString();
						 strTransferFrom =   GmCommonClass.getStringWithCommas(strTransferFrom,0);
						 strLendTo= ((HashMap)pageContext.getAttribute("totals")).get("column21").toString();
						 strLendTo =   GmCommonClass.getStringWithCommas(strLendTo,0);
						 strUnresolvedQty= ((HashMap)pageContext.getAttribute("totals")).get("column23").toString();	
						 strUnresolvedQty =   GmCommonClass.getStringWithCommas(strUnresolvedQty,0);
						}
					else {
						 strFlaggedQty= ((HashMap)pageContext.getAttribute("totals")).get("column14").toString();
						 strFlaggedQty =   GmCommonClass.getStringWithCommas(strFlaggedQty,0);
						 strUnresolvedQty= ((HashMap)pageContext.getAttribute("totals")).get("column15").toString();	
						 strUnresolvedQty =   GmCommonClass.getStringWithCommas(strUnresolvedQty,0);
					}
					%>
					<tr><td class="Line" colspan="23"></td></tr>
					<tr>
							<td class = "ShadeDarkGrayTD" colspan="9"> <B> <fmtVarianceRptByDist:message key="LBL_TOTAL_RECORDS"/>: <%=rowsize %> </B></td>
							<td class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strAuditQty%></B></td>
							<td class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strSystemQty%></B></td> 
							<td class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strDeviationQty%></B></td> 
					<% if(flaggedDet.equals("true"))
                         {%> 					
							<td  colspan="2" class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strTransferTo%></B></td>
							<td class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strExtra%></B></td>
	                        <td class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strBorrowedFrom%></B></td>
	                        <td  colspan="2" class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strReturns%></B></td>
	                        <td class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strMissing%></B></td>
	                        <td class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strTransferFrom%></B></td>
	                        <td class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strLendTo%></B></td>
	                        <td colspan="2" class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strUnresolvedQty%></B></td>
                       <%}
					else{ %>
						 <td colspan="2" class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strFlaggedQty%></B></td>
						  <td  class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strUnresolvedQty%></B></td>
						 <% } %>
						 <tr><td class="Line" colspan="23"></td></tr>
					</tr>
					</display:footer>	
				</display:table>						
			</td>
		</tr>			
	</table>

</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>