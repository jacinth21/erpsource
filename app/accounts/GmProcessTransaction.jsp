<%
/**********************************************************************************
 * File		 		: GmProcessTransaction.jsp
 * Desc		 		: Process Transaction Invoice Update screen
 * Version	 		: 1.0
 * author			: Velu
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%String strWikiTitle = GmCommonClass.getWikiTitle("PROCESS_TRANSACTION");%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Process Transaction </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<%@ taglib prefix="fmtProcessTransaction" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmProcessTransaction.jsp -->
<fmtProcessTransaction:setLocale value="<%=strLocale%>"/>
<fmtProcessTransaction:setBundle basename="properties.labels.accounts.GmProcessTransaction"/>

<%

String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
%>

<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmProcessTransaction.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script>
var lblRefId = '<fmtProcessTransaction:message key="LBL_REF_ID"/>';
var lblSource = '<fmtProcessTransaction:message key="LBL_SOURCE"/>';
</script>
</head>
<body leftmargin="20" topmargin="10" onLoad="fnSetFocus();" onkeydown="javaScript:return fnCallGo();">
 <html:form action ="/gmProcessTrans.do" >
 <html:hidden property="strOpt" />
 <html:hidden property="haction" />  

<table border="0" class="DtTable1000"  cellspacing="0" cellpadding="0" >	
		<tr>
			<td height="25" colspan="4" class="RightDashBoardHeader" ><fmtProcessTransaction:message key="LBL_PROCESS_TRANSACTION"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right" colspan="3">
			<fmtProcessTransaction:message key="LBL_HELP" var="varHelp"/>
			<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    </td>
		</tr>	
		<tr><td colspan="7" height="1" bgcolor="#cccccc"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr height="30" style="vertical-align: middle;">
			<td class="RightTableCaption" width="120" align="right"><font color="red">*&nbsp;</font><fmtProcessTransaction:message key="LBL_REF_ID"/>:</td>
			 <td width="210" style="padding-left: 5px;">
		        <html:text property="strRefID" name="frmProcessTransaction" size="30" onfocus="changeBgColor(this,'#AACCE8');" 
		        styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" onkeypress="fnCallGo();"/>
		        </td>
		    	<td class="RightTableCaption" width="200" align="right"><font color="red">*&nbsp;</font><fmtProcessTransaction:message key="LBL_SOURCE"/>:</td>
				<td class="RightText" style="padding-left: 5px;"><gmjsp:dropdown controlName="strSource" SFFormName="frmProcessTransaction"	SFSeletedValue="strSource" SFValue="alSource" codeId="CODEID" codeName="CODENM"	defaultValue="[Choose One]" />
		        </td>
		        <td class="RightTableCaption" width="200" align="right"><fmtProcessTransaction:message key="LBL_CURRENCY"/>:</td>
				<td style="padding-left: 5px;"><gmjsp:dropdown	controlName="strCompCurrency" SFFormName="frmProcessTransaction"
						    SFSeletedValue="strCompCurrency" SFValue="alCompCurrency" codeId="ID" codeName="NAMEALT" />
				<fmtProcessTransaction:message key="BTN_LOAD" var="varLoad"/>
		        <td width="200" align="center"><gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnLoad();" buttonType="Load" /></td>
		</tr> 
		<tr><td colspan="7" height="1" bgcolor="#cccccc"></td></tr>
		 <logic:notEqual name="frmProcessTransaction" property="strRefID" value="">
		   <logic:notEqual name="frmProcessTransaction" property="strURL" value="">
			<tr>
				<td colspan="7">
					<iframe id="myFrame" src='<bean:write name="frmProcessTransaction" property="strURL"/>' width="100%" scrolling="yes" height="575" frameborder="0"  vspace="0"  hspace="0"  marginwidth="0"  marginheight="0"></iframe>
				</td>
			</tr>	
			</logic:notEqual>
		</logic:notEqual> 
        </table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>