 <%
/**********************************************************************************
 * File		 		: GmSalesOrderReport.jsp
 * Desc		 		: This screen is used for the Account Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="java.util.Date"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtSalesOrderReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmSalesOrderReport.jsp -->
<fmtSalesOrderReport:setLocale value="<%=strLocale%>"/>
<fmtSalesOrderReport:setBundle basename="properties.labels.accounts.GmSalesOrderReport"/>
<%
	String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("DAILY_SALES"));
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	String strSuccessMsg = "";
	Date dtFromDate = null;
	Date dtToDate = null;

	String strCurrSign = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_SYMB"));
	ArrayList alReturn = (ArrayList)hmReturn.get("REPORT");
	int intLength = alReturn.size();
	String strType = (String)request.getAttribute("hType")==null?"":(String)request.getAttribute("hType");
	String strFrmDate = (String)request.getAttribute("hFrom")==null?"":(String)request.getAttribute("hFrom");
	String strToDate = (String)request.getAttribute("hTo")==null?"":(String)request.getAttribute("hTo");
	strSuccessMsg = GmCommonClass.parseNull((String)request.getAttribute("SuccessMsg"));
	String strAccountCurrency = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_ID"));
	// to get the session date fmt
	String strApplDateFmt = strGCompDateFmt;
	String strUpdFl = GmCommonClass.parseNull((String)request.getAttribute("UPDFL"));
	// to convert the string to date object
	dtFromDate = (Date)GmCommonClass.getStringToDate(strFrmDate,strApplDateFmt);
	dtToDate = (Date)GmCommonClass.getStringToDate(strToDate,strApplDateFmt);
	ArrayList alCompCurrency = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALCOMPANYCURRENCY"));
	HashMap hmLoop = new HashMap();

	int intAllCnt = 0;
	int intDOCnt = 0;
	String strAccId = "";
	String strAccName = "";
	String strOrdId = "";
	String strOrdDate = "";
	String strCSPerson = "";
	String strPrice = "";
	String strParentOrdId = "";
	String strOrdSumId = "";
	String strShade = "";
	String strOrderType = "";
	String strParentAccName = "";
	String strParentAccountId = "";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Sales Orders Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script>

var expandNum = 1;
var dateFmt = '<%=strApplDateFmt%>';
var dateFmt = '<%=strApplDateFmt%>';
var lblFromDate = '<fmtSalesOrderReport:message key="LBL_FROM_DATE"/>';
var lblToDate = '<fmtSalesOrderReport:message key="LBL_TO_DATE"/>';

function fnSubmit()
{
	var fromDtObj = document.frmAccount.Txt_FromDate;
	var toDtObj = document.frmAccount.Txt_ToDate;
	// validate if empty
	fnValidateTxtFld('Txt_FromDate', lblFromDate);
	fnValidateTxtFld('Txt_ToDate', lblToDate);
	// validate Date Format
	CommonDateValidation(fromDtObj,dateFmt,'<b>'+lblFromDate +'- </b>'+ message[611]);
	CommonDateValidation(toDtObj,dateFmt,'<b>'+lblToDate +'- </b>'+ message[611]);
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	fnReloadSales(); // Call the Action in the Filters jsp
	
}

function fnPrintVer()
{
    // currency dropdown id is send to servlet, when download excel action performs.
	var arCurrID = document.frmAccount.Cbo_Comp_Curr.value;
    windowOpener("/GmDailySalesServlet?hType=ACCT&hMode=Excel&Txt_FromDate=<%=strFrmDate%>&Txt_ToDate=<%=strToDate%>&Cbo_Comp_Curr="+arCurrID,"Excel","menubar=yes,resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintPick(val)
{
	windowOpener('<%=strServletPath%>/GmEditOrderServlet?hMode=PrintPack&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
}

function fnViewOrder(val)
{
	windowOpener('/GmEditOrderServlet?hMode=PrintPrice&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=810,height=450");	
}

function fnPrintCreditMemo(val)
{
	windowOpener('<%=strServletPath%>/GmReportCreditsServlet?hAction=PrintVersion&hOrderId='+val,"Credit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintCashAdjustMemo(val)
{
	windowOpener('<%=strServletPath%>/GmReportCreditsServlet?hAction=PrintCashAdj&hOrderId='+val,"Credit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}


function Toggle(val)
{
	
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className="";
		tabobj.style.background="#ffffff";
	}
}
// this function used to expand all the accounts
function fnExpandAll(){
	var totalAccount = document.frmAccount.allExpand.value;
	var expFlag = false;
	var imgObj = eval("document.all.ExpandImg");
	// Change the image style
	if(expandNum%2==0){
		if(imgObj){
			imgObj.src = '<%=strImagePath%>/plus.gif';
		}
	}else{
		if(imgObj){
			imgObj.src = '<%=strImagePath%>/minus.gif';
		}
	}
	for(i=0;i<totalAccount;i++){
		var accountObj = document.getElementById("hAccountID"+i);
		//var accountObj = eval("document.frmAccount.hAccountID"+i);
		if(accountObj != null){
			var account = accountObj.value;
			// to expand or collopse the dynamic view
			var obj = eval("document.all.div"+account);
			var trobj = eval("document.all.tr"+account);
			var tabobj = eval("document.all.tab"+account);

			if(expandNum%2!=0){
				obj.style.display = '';
				trobj.className="ShadeRightTableCaptionBlue";
				tabobj.style.background="#ecf6fe";	
			}else{
				obj.style.display = 'none';
				trobj.className="";
				tabobj.style.background="#ffffff";
			}
		}		
	}
	expandNum++;
}
// this function used to click the accoun check box to select all orders
function fnCheckAll(val){
	var accountObj = document.getElementById("Chk_DOSummary"+val);
	var accountVal = accountObj.value;
	var doSummaryObj = document.getElementById("hDoCount"+accountVal);
	var doSummaryVal = doSummaryObj.value;
	if(doSummaryVal !=0){
		if (accountObj != null && accountObj.checked == true){
			
			for(i=0;i<doSummaryVal;i++){
				var orderIDChk = document.getElementById("Chk_DOOrder"+accountVal+"_"+i);
				orderIDChk.checked = true;
			}
			
		}else{
			for(i=0;i<doSummaryVal;i++){
				var orderIDChk = document.getElementById("Chk_DOOrder"+accountVal+"_"+i);
				orderIDChk.checked = false;
			}
		}
	}
}
// this function used to form the selected order to add the batch
function fnCreateBatch(){ 
	var inputStr = '';
	var totalAccount = document.frmAccount.allExpand.value;
	//passing curr id value when creating batch
	var arCurrID = document.frmAccount.Cbo_Comp_Curr.value;
	// to get the account id value
	for(i=0;i<totalAccount;i++){
		var accountObj = document.getElementById("Chk_DOSummary"+i);
		//var accountObj = eval("document.frmAccount.hAccountID"+i);
		if(accountObj != null){
			var accountVal = accountObj.value;
			var doSummaryObj = document.getElementById("hDoCount"+accountVal);
			var doSummaryVal = doSummaryObj.value;
			if(doSummaryVal !=0){
				for(j=0;j<doSummaryVal;j++){
					var orderIDChk = document.getElementById("Chk_DOOrder"+accountVal+"_"+j);
					if(orderIDChk.checked == true){
						inputStr = inputStr+orderIDChk.value +'|';
					}
				}
			}
		}		
	}
	
	if(inputStr ==''){
		Error_Details(message_accounts[253]);
	}
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	var fromDt = document.frmAccount.Txt_FromDate.value;
	var toDt = document.frmAccount.Txt_ToDate.value;
	 document.frmAccount.hInputString.value = inputStr;
	 
	document.frmAccount.action='/gmDoPrintBatch.do?method=saveDOSummaryFiles&strOpt=Save&fromDate='+fromDt+'&toDate='+toDt+'&Cbo_Comp_Curr='+arCurrID;
	fnStartProgress('Y'); 
	document.frmAccount.submit();
}
//This function used to press the enter key then submit the page.
function fnEnterKey() {
	if (event.keyCode == 13) {
		fnSubmit();
	}
}
//This function is used to select the all account orders 
function fnCheckAllAccont(){
	var totalAccount = document.frmAccount.allExpand.value;
	var checkAllObj = document.frmAccount.Chk_All;
	for(j=0;j<totalAccount;j++){
		var accountObj = document.getElementById("hAccountID"+j);
		if(accountObj != null){
			var account = accountObj.value;
			var doSummaryObj = document.getElementById("hDoCount"+account);
			var doSummaryVal = doSummaryObj.value;
			var accountSumObj = document.getElementById("Chk_DOSummary"+j);
			if(doSummaryVal !=0 && accountSumObj != null){
				var checkAllObjVal = checkAllObj.checked;
				accountSumObj.checked = checkAllObjVal;
				fnCheckAll(j);
				}
			}			
		}
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onkeydown="fnEnterKey();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmDailySalesServlet">
<input type="hidden" name="hType" value="<%=strType%>">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hInputString" value="">

	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="700" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" class="RightDashBoardHeader" colspan="4"><fmtSalesOrderReport:message key="LBL_REPORT_DAILY_SALES"/></td>
						<fmtSalesOrderReport:message key="LBL_HELP" var="VarHelp"/>
						<td height="25" class="RightDashBoardHeader"><img align="right"
							id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
							title='${VarHelp}' width='16' height='16'
							onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
					</tr>
						<tr>
							<td colspan="5" height="20"><jsp:include
									page="/sales/GmSalesFilters.jsp">
									<jsp:param name="HIDE" value="SYSTEM" />
									<jsp:param name="FRMNAME" value="frmAccount" />
									<jsp:param name="HACTION" value="Reload" />
								</jsp:include></td>
						</tr>
					<tr><td class="Line" colspan="5"></td></tr>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
								<td align="right" class="RightTableCaption" HEIGHT="24"><fmtSalesOrderReport:message key="LBL_FROM_DATE"/>:</td>
								<td class="RightText">&nbsp;<gmjsp:calendar textControlName="Txt_FromDate" 
									textValue="<%=(dtFromDate==null)?null:new java.sql.Date(dtFromDate.getTime())%>" 
	 								gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/></td>
								<td align="right" class="RightTableCaption"><fmtSalesOrderReport:message key="LBL_TO_DATE"/>:</td>
								<td class="RightText">&nbsp;<gmjsp:calendar textControlName="Txt_ToDate" 
									textValue="<%=(dtToDate==null)?null:new java.sql.Date(dtToDate.getTime())%>" 
	 								gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/></td>	
	 							<td>
	 								<table width="100%" cellpadding="0" cellspacing="0" border="0">	<tr>
                                <td class="RightTableCaption" HEIGHT="30" align="right">&nbsp;<fmtSalesOrderReport:message key="LBL_CURRECY"/>:&nbsp;</td>
			                    <td class="LeftText"><gmjsp:dropdown controlName="Cbo_Comp_Curr"  seletedValue="<%=strAccountCurrency%>" value="<%=alCompCurrency%>" codeId = "ID"  codeName = "NAMEALT" />&nbsp;&nbsp;&nbsp;			
			 					<fmtSalesOrderReport:message key="BTN_LOAD" var="VarLoad"/>
			 					<td height="30"><gmjsp:button value="&nbsp;${VarLoad}&nbsp;" name="Btn_Go" gmClass="button" onClick="javascript:fnSubmit();" buttonType="Load" />
										</td>
										</tr>
										</table>
										</td>
					</tr>
					<%if(!strSuccessMsg.equals("")){%>
					<tr><td class="Line" colspan="6"></td></tr>
					<tr><td colspan="5" class="RightTextGreen" height="30"  align="center"> <%=strSuccessMsg %> </td></tr>
					<%} %>
				</table>
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr><td class="Line" colspan="7"></td></tr>
					<tr><td colspan="7" class="RightTableCaption" height="24">&nbsp;<a href="javascript:fnExpandAll();"><IMG id="ExpandImg" border=0 src="<%=strImagePath%>/plus.gif">&nbsp;<fmtSalesOrderReport:message key="LBL_EXPAND_ALL"/></a></td></tr>
					<tr><td class="Line" colspan="7"></td></tr>
					<tr class="ShadeRightTableCaption">
					<td HEIGHT="24" width="200">&nbsp;<input class="RightText" type="checkbox" id="Chk_All" onclick="fnCheckAllAccont();">&nbsp;<fmtSalesOrderReport:message key="LBL_PARENT_ACCOUNT"/></td>
						<td  width="150">&nbsp;&nbsp;<fmtSalesOrderReport:message key="LBL_REP_ACCOUNT"/></td>
						<td width="200"><fmtSalesOrderReport:message key="LBL_PACK_SLIP"/></td>
						<td width="200"><fmtSalesOrderReport:message key="LBL_DO_ID"/></td>
						<td width="170" align="center"><fmtSalesOrderReport:message key="LBL_ORDER_DATE"/></td>
						<td width="150" align="center"><fmtSalesOrderReport:message key="LBL_ORDER_RECEIVED_BY"/></td>
						<td width="100" align="center"><fmtSalesOrderReport:message key="LBL_AMOUNT"/></td>
					</tr>
<%
						if (intLength > 0)
						{
							HashMap hmTempLoop = new HashMap();

							String strNextId = "";
							int intCount = 0;
							String strLine = "";
							String strTotal = "";
							String strAcctTotal = "";
							boolean blFlag = false;
							boolean blFlag1 = false;
							double dbSales = 0.0;								
							double dbAcctTotal = 0.0;
							double dbTotal = 0.0;

							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alReturn.get(i);
								if (i<intLength-1)
								{
									hmTempLoop = (HashMap)alReturn.get(i+1);
									strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("PARENTACID"));
								}

								strAccId = GmCommonClass.parseNull((String)hmLoop.get("PARENTACID"));
								strParentAccName = GmCommonClass.parseNull((String)hmLoop.get("PARENTACCTNM"));
								strAccName = GmCommonClass.parseNull((String)hmLoop.get("NAME"));
								strOrdId = GmCommonClass.parseNull((String)hmLoop.get("ORDID"));
								strOrdDate = GmCommonClass.getStringFromDate((java.util.Date)hmLoop.get("ORDDT"),strApplDateFmt);
								strPrice = GmCommonClass.parseZero((String)hmLoop.get("COST"));
								strCSPerson = GmCommonClass.parseZero((String)hmLoop.get("CS"));
								strParentOrdId = GmCommonClass.parseNull((String)hmLoop.get("PARENTORDID"));
								strOrderType = GmCommonClass.parseNull((String)hmLoop.get("ORDERTYPE"));
								strOrdSumId = strParentOrdId.equals("")?strOrdId:strParentOrdId;
								strParentAccountId = GmCommonClass.parseNull((String)hmLoop.get("PARENTACID"));
								
								dbSales = Double.parseDouble(strPrice);
								dbAcctTotal = dbAcctTotal + dbSales;
								dbTotal = dbTotal + dbSales;
								if (strParentAccountId.equals(strNextId))
								{
									intCount++;
									strLine = "<TR><TD colspan=7 height=1 class=Line></TD></TR>";
								}
								else
								{
									strLine = "<TR><TD colspan=7 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
									    strParentAccName = "";
										strLine = "";
									}
									else
									{
										blFlag = true;
									}
									intCount = 0;
									//
								}
								strAcctTotal = ""+dbAcctTotal;

								if (intCount > 1)
								{
								  strParentAccName = "";
									strLine = "";
								}
							
								strShade = (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows

								out.print(strLine);
								strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
								if (intCount == 1 || blFlag)
								{
%>
					<tr id="tr<%=strAccId%>">
					<fmtSalesOrderReport:message key="LBL_CLICK_HERE_TO_EXPAND" var="varClickHereToExpand"/>
						<td colspan="6" height="20" class="RightText">&nbsp;<input class="RightText" type="checkbox" id="Chk_DOSummary<%=intAllCnt%>" value="<%=strAccId%>" onclick="fnCheckAll('<%=intAllCnt%>');">&nbsp;<A class="RightText" title="${varClickHereToExpand}" href="javascript:Toggle('<%=strAccId%>')"><%=strParentAccName%></a></td>
						<td class="RightText" id="td<%=strAccId%>" align="right"></td><input type="hidden" value="<%=strAccId%>" id="hAccountID<%=intAllCnt%>" />
					</tr>
					<tr>
						<td colspan="7"><div style="display:none" id="div<%=strAccId%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tab<%=strAccId%>">
<%
								intAllCnt++;
								intDOCnt = 0;
								blFlag = false;
								}
%>
					<tr <%=strShade%>>
					<fmtSalesOrderReport:message key="LBL_PACKING_SLIP" var="varPackingSlip"/>
					<fmtSalesOrderReport:message key="LBL_PACK_SLIP" var="varPackSlip"/>
					<fmtSalesOrderReport:message key="LBL_VIEW_ORDER_SUMMARY" var="varViewOrderSummary"/>
						<td width="40" class="RightText" height="20">&nbsp;</td>
						<!-- <td width="300" class="RightText" align="center" height="150"></td> -->
						<td width="20" >
						<%
						 //2521 - Bill & Ship - 2526 - Bill Only && 2530 - Bill Only - Loaner && 2532 - Bill Only - From Sales Consignments
						if(strOrderType.equals("") || strOrderType.equals("2521") || strOrderType.equals("2526") || strOrderType.equals("2530") || strOrderType.equals("2532")){ %>
						<input class="RightText" type="checkbox" id="Chk_DOOrder<%=strAccId%>_<%=intDOCnt%>" value="<%=strParentOrdId.equals("")?strOrdId:strParentOrdId%>" >&nbsp;
						<% intDOCnt++;
						}else{ %>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<%} %></td>
						<td width="300"><%=strAccName%></td><td width="300">
<%						// Check if the Orded is a Return or Duplicate Order -- redirect based on the type 						
						if ((strOrdId.charAt(strOrdId.length()-1)== 'R') ||(strOrdId.charAt(strOrdId.length()-1)== 'D') )
						{%><a href="javascript:fnPrintCreditMemo('<%=strOrdId%>');" 
<%						}else if (strOrdId.charAt(strOrdId.length()-1)== 'A') 
						{%><a href="javascript:fnPrintCashAdjustMemo('<%=strOrdId%>');" 
<%						}else
						{%>	<a href="javascript:fnPrintPick('<%=strOrdId%>');" title="${varPackingSlip}" 
<%						}%>								
						class="RightText"><img src="<%=strImagePath%>/packslip.gif" title="${varPackSlip}"  border=0></a>
						<a href="javascript:fnViewOrder('<%=strOrdSumId%>');"><img src="<%=strImagePath%>/ordsum.gif" title="${varViewOrderSummary}"  border=0></a>&nbsp;<%=strOrdId%></td>
						<td width="220">&nbsp;<%=strParentOrdId%></td>
						<td width="170" align="center" ><%=strOrdDate%></td>
						<td width="150" align="center">&nbsp;<%=strCSPerson%></td>
						<td width="100" class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strPrice)%>&nbsp;</td>
					</tr>
					<tr><td colspan="8" height="1" bgcolor="#cccccc"></td></tr>
<%					
					if ( intCount == 0 || i == intLength-1)
					{
%>
							<input type="hidden" name="hDoCount<%=strAccId%>" id="hDoCount<%=strParentAccountId%>" value="<%=intDOCnt%>">
							</table></div>
						</td>
					</tr>
					<script>
						document.all.td<%=strParentAccountId%>.innerHTML = "<%=strCurrSign%><%=GmCommonClass.getStringWithCommas(strAcctTotal)%>&nbsp;";
					</script>
<%
						dbAcctTotal = 0.0;					
					}
				strTotal = ""+dbTotal;
				}// end of FOR
%>
					<tr><td colspan="7" class="Line"></td></tr>
					<tr>
						<td align="right"  class="RightTableCaption" height="20" colspan="6"><fmtSalesOrderReport:message key="LBL_GRAND_TOTAL"/>&nbsp;</td>
						<td class="RightTableCaption" align="right"><%=strCurrSign%><%=GmCommonClass.getStringWithCommas(strTotal)%>&nbsp;</td>
					</tr>
					<tr><td colspan="7" class="Line"></td></tr>
					<tr>
						<td colspan="7" align="center" height="30">
						<% if ( strUpdFl.equals("Y")) {   %>
						<fmtSalesOrderReport:message key="BTN_CREATE_BATCH" var="VarCreateBatch"/>
						<gmjsp:button gmClass="button" value="${VarCreateBatch}" onClick="fnCreateBatch();" tabindex="12" buttonType="Save" />&nbsp;&nbsp;
						<% }  %>
						<fmtSalesOrderReport:message key="BTN_DOWNLAOD_EXCEL" var="varDownloadToExcel"/>
						<gmjsp:button value="${varDownloadToExcel}" buttonType="Load" gmClass="button" onClick="fnPrintVer();" tabindex="13" />
						</td>
					</tr>
<%
						} // End of IF
						else{
%>
					<tr><td colspan="7" class="Line"></td></tr>					
					<tr><td colspan="7" class="RightTextRed" height="50" align=center><fmtSalesOrderReport:message key="LBL_NO_SALES_PERIOD"/> </td></tr>
<%					
						}
%>
				<tr><td colspan="7"> <input type="hidden" name="allExpand" value="<%=intAllCnt%>"> </td></tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>

</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
