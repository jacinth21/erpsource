<%@ page language="java" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<%@ taglib prefix="fmtFieldSalesAudit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmFieldSalesAudit.jsp -->
<fmtFieldSalesAudit:setLocale value="<%=strLocale%>"/>
<fmtFieldSalesAudit:setBundle basename="properties.labels.accounts.GmFieldSalesAudi"/>
 
<%
GmServlet gm = new GmServlet();
gm.checkSession(response,session);




	String strWikiTitle = GmCommonClass.getWikiTitle("FIELD_SALES_AUDIT_LOCK");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Field Sales Audit Lock</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<bean:define id="alAuditor" name="frmAutoAuditLock" property="alAuditor" type="java.util.List"></bean:define>
<% 
ArrayList alList1 = new ArrayList();

alList1 = GmCommonClass.parseNullArrayList((ArrayList) alAuditor);

	int rowsize1 = alList1.size();

	
	
	String strAuditorId="";
%>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>



function fnSubmit()
{
	/*if(document.frmAutoAuditLock.tagIdFrom.value != ""&&document.frmAutoAuditLock.tagIdTo.value == ""){
		Error_Details(" Please select a Tag Range From: and To:");
		}	
	if(document.frmAutoAuditLock.tagIdFrom.value == ""&&document.frmAutoAuditLock.tagIdTo.value != ""){
		Error_Details(" Please select a Tag Range From: and To:");
		}	 	*/
	//fnValidateDropDn('physicalAuditID',' Physical Audit ');
	//fnValidateDropDn('locationID', 'Location');
	
	objLoop = eval("document.frmAutoAuditLock.rowloop");
	var inputString = '';   
	
		for (var k=1; k <= objLoop.value; k++) 
						{
							  objauditorId = eval("document.frmAutoAuditLock.auditorId"+k);
							 objTagStart = eval("document.frmAutoAuditLock.tagStart"+k);
							
							//document.frmRequestMaster.requestStatusval.value=frmRequestMaster.requestStatusoper.options[frmRequestMaster.requestStatusoper.selectedIndex].text;
							
							if(objauditorId.value!="0")
							{
							    objTagEnd = eval("document.frmAutoAuditLock.tagEnd"+k);
							    inputString = inputString + objauditorId.value+ '^' +objauditorId.options[objauditorId.selectedIndex].text+ '^' + objTagStart.value + '^'+ objTagEnd .value +  '|';
							}

						} 
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmAutoAuditLock.distName.value  = document.frmAutoAuditLock.distId.options[document.frmAutoAuditLock.distId.selectedIndex].text;
	document.frmAutoAuditLock.strOpt.value  = "save";
	document.frmAutoAuditLock.inputString.value  = inputString;
	document.frmAutoAuditLock.Submit.disabled = true;		
    document.body.style.cursor = 'wait'; 
    fnStartProgress("Y");
	document.frmAutoAuditLock.submit();
}
function fnReload()
{
	
	//document.frmAutoAuditLock.auditId.value=document.frmAutoAuditLock.auditId.value;
	document.frmAutoAuditLock.strOpt.value = "reload";
	document.frmAutoAuditLock.submit();
}
function fnEnter()
{
		if (event.keyCode == 13)
		{ 
			fnGo();
		}
}
</script>
</HEAD>

<BODY onkeyup="fnEnter();" leftmargin="20" topmargin="10" >
 
<html:form  action="/gmAutoAuditLock.do"  >
<html:hidden property="strOpt"/>
<input type="hidden" name="inputString"/ >
<input type="hidden" name="distName"/ >


	<table border="0" class="DtTable850" width="700" cellspacing="0" cellpadding="0">
		<tr>
					<td colspan="5" height="25" class="RightDashBoardHeader">
				<fmtFieldSalesAudit:message key="LBL_FIELD_SALES_AUDIT_LOCK"/>
			</td>
			<td  height="25" class="RightDashBoardHeader">
			<fmtFieldSalesAudit:message key="LBL_HELP" var="varHelp"/>
			<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr><td colspan="6" class="Line" height="1"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr  class="shade" >
		
		<td height="30" align ="Right" class="RightTableCaption">&nbsp; <fmtFieldSalesAudit:message key="LBL_AUIDT_NAME"/>:&nbsp;<td>
	   <td><gmjsp:dropdown controlName="auditId" SFFormName="frmAutoAuditLock" SFSeletedValue="auditId" 
							SFValue="alAuditName" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" onChange="fnReload()" /></td>
		<td height="30" align ="Right" class="RightTableCaption">&nbsp; <fmtFieldSalesAudit:message key="LBL_DISTRIBUTOR_NAME"/>:&nbsp;</td>
		<td colspan="2"  align ="left"><gmjsp:dropdown controlName="distId" SFFormName="frmAutoAuditLock" SFSeletedValue="distId" 
							SFValue="aldist" codeId = "DISTID"  codeName = "DISTNAME"  defaultValue= "[Choose One]"  /></td>
		
		
		</tr>
		<tr>
		<td colspan="6">
		<br>
		<table border="1" width="100%" cellspacing="0" cellpadding="0">
		<tr>
		<td colspan="2"></td>
		<td>
		
		  <thead>
						<TR bgcolor="#EEEEEE"   class="ShadeRightTableCaption" height = "20"  colspan="3">
							<TH class="RightText"  >&nbsp;<fmtFieldSalesAudit:message key="LBL_AUDITOR"/></TH>
							<TH class="RightText"  ><fmtFieldSalesAudit:message key="LBL_TAG_START_RANGE"/> </TH>
							<TH class="RightText"  ><fmtFieldSalesAudit:message key="LBL_TAG_END_RANGE"/></TH>
						</TR>	
						<tr>
						<td class="Line" height="1" colspan="3"></td>
						</tr>
					  </thead>
					  
					    <TBODY> 
					    
			<%
				for (int i=1; i <= rowsize1; i++)
				{
					
					strAuditorId="auditorId"+i;
				%>
				
		<tr  >
	
			<td   align ="left"><gmjsp:dropdown controlName="<%=strAuditorId%>" SFFormName="frmAutoAuditLock" SFSeletedValue="auditorId" 
							SFValue="alAuditor" codeId = "USERID"  codeName = "UNAME"  defaultValue= "[Choose One]" /></td>
							<td class="RightText" align="center">&nbsp;<input type="text" size="15" class=InputArea  name="tagStart<%=i%>"   onFocus="changeBgColor(this,'#AACCE8');"> </td>
							<td class="RightText" align="center">&nbsp;<input type="text" size="15" class=InputArea  name="tagEnd<%=i%>"  onFocus="changeBgColor(this,'#AACCE8');"></td>

	
		</tr>
			<%
				}
	%>
	
	
		</TBODY>
		</tr>
		</table>
		</tr>
		
	 </td>
	 </tr>
		<tr>
		<td>
		<td colspan="7" align="center">&nbsp;&nbsp; <p>&nbsp;</p>  
		<input type=hidden value='<%=rowsize1%>' name="rowloop">
		<fmtFieldSalesAudit:message key="BTN_SUBMIT" var="varSubmit"/>
	 <gmjsp:button value="${varSubmit}" gmClass="button" name="Submit"  onClick="fnSubmit();" buttonType="Save" />
	 
	 </td>
	 </tr>	
			
		</table>

	

</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>