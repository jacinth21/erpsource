<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Iterator,java.lang.*" %>
<%@ taglib prefix="fmtVarianceRptFilter" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmVarianceRptFilter.jsp -->
<fmtVarianceRptFilter:setLocale value="<%=strLocale%>"/>
<fmtVarianceRptFilter:setBundle basename="properties.labels.accounts.GmVarianceRptFilter"/>
<%
String strProdMgmtJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
%>
<script language="JavaScript" src="j<%=strProdMgmtJsPath%>/GmFilter.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script type="text/javascript">
<%

String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>

</script>

<BODY>
<FORM name="<%=strFormName%>" method="POST" >
<input type="hidden" name="hAction" value="">
	<!-- Custom tag lib code modified for JBOSS migration changes -->
            <tr>
                  <td height="30" align ="Right" class="RightTableCaption">&nbsp; <fmtVarianceRptFilter:message key="LBL_DEVIATION_QTY"/>: &nbsp;</td>
                      <td>  <gmjsp:dropdown controlName="devOpr" SFFormName="<%=strFormName%>" SFSeletedValue="devOpr" SFValue="alDevOpr" codeId = "CODENMALT"  codeName = "CODENM"  defaultValue= "[Choose One]"  />
                        &nbsp;<html:text property="devQty" name="<%=strFormName%>" size="10" tabindex="21" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>&nbsp;
                  </td>
                  <td height="30" align ="Right" class="RightTableCaption">&nbsp; <fmtVarianceRptFilter:message key="LBL_DISPLAY_ONLY_DEVIATION"/>: &nbsp;</td>
                  <td colspan="2" ><html:checkbox property="dispOnlyDeviation" name="<%=strFormName%>" />
                  </td>             
            </tr>
             <tr><td colspan="5" class="LLine" height="1"></td></tr>
              <tr class="shade">
                <td height="30" align ="Right" class="RightTableCaption">&nbsp;<fmtVarianceRptFilter:message key="LBL_DISPLAY_UNRESOLVED_QTY"/>: &nbsp;</td>
                  <td height="30" class="RightTableCaption"><html:checkbox property="dispUnresolvedDet" name="<%=strFormName%>" />   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtVarianceRptFilter:message key="LBL_DISPLAY_NON_APPROVED"/> : <html:checkbox property="dispNonApproved" name="<%=strFormName%>" /> </td>
                  <td height="30" align ="Right" class="RightTableCaption">&nbsp; <fmtVarianceRptFilter:message key="LBL_DISPLAY_NON_RECONCILATION"/>: &nbsp;</td>
                  <td><html:checkbox property="dispNonReconciliation" name="<%=strFormName%>" />
                  </td> 
                  </td>             
            </tr>
            <tr><td colspan="5" class="LLine" height="1"></td></tr>
            <tr>
                  <td height="30" align ="Right" class="RightTableCaption">&nbsp; <fmtVarianceRptFilter:message key="LBL_SHOW_FLAGGED_DETAILS"/>: &nbsp;</td>
                  <td ><html:checkbox property="dispflaggedDet" name="<%=strFormName%>" />      </td>
                  <td height="30" align ="Right" class="RightTableCaption">&nbsp; <fmtVarianceRptFilter:message key="LBL_DISPLAY_NON_FLAGGED"/>: &nbsp;</td>
                  <td><html:checkbox property="dispNonflagged" name="<%=strFormName%>" />
                  <fmtVarianceRptFilter:message key="BTN_LOAD" var="varLoad"/>
                  &nbsp;&nbsp;<gmjsp:button value="${varLoad}" gmClass="button" name="Submit" buttonType="Load" onClick="fnSubmit(); " /> 
                  </td>                     
            </tr>           	
		</tr>

</FORM>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>