<%
/**********************************************************************************
 * File		 		: GmSageVendorMapping.jsp
 * Desc		 		: SageVendort Mapping Screen
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>

<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ taglib prefix="fmtSageVendorMapping" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmSageVendorMapping.jsp -->
<fmtSageVendorMapping:setLocale value="<%=strLocale%>"/>
<fmtSageVendorMapping:setBundle basename="properties.labels.accounts.GmSageVendorMapping"/>
<bean:define id="returnList" name="frmSageVendorMap" property="returnList" type="java.util.List"></bean:define>
 
 
<%
	ArrayList alSageVendorList = new ArrayList();
	 
	alSageVendorList = GmCommonClass.parseNullArrayList((ArrayList) returnList);
 
	int rowsize = alSageVendorList.size();
	 
	StringBuffer sbInput = new StringBuffer();
	String strVendorId = "";
	String strInput = "";
	 
	DynaBean db ;

		for (int i=0;i<rowsize;i++)
		{
			db = (DynaBean)alSageVendorList.get(i);
			strVendorId = String.valueOf(db.get("VENDORID"));
			sbInput.append(strVendorId);
			sbInput.append("^");
		}
		 strInput = sbInput.toString();
	
	String strWikiTitle = GmCommonClass.getWikiTitle("VENDOR_MAPPING_REPORT");
	
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Vendor Mapping Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"
	media="print" />

<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>
function fnReload()
{     
//	alert(document.frmSageVendorMap.missingSageVenIds.value);

	if(!document.frmSageVendorMap.missingSageVenIds.checked)
	document.frmSageVendorMap.hmissingNotcheck.value ="true";
	else document.frmSageVendorMap.hmissingNotcheck.value ="false";
	 
	document.frmSageVendorMap.strOpt.value = "reload";
	fnStartProgress();
	document.frmSageVendorMap.submit();   
} 
 

function fnSubmit()
{
	var varRows = <%=rowsize%>;  
	 
	var inputString = ''; 
	var varInput = "<%=strInput%>";
	
	var arrVendorId = varInput.split('^');
	 
	for (var i =0; i < varRows; i++) 
	{
		obj = eval("document.frmSageVendorMap.sageVendorId"+i);
		varSageVendorId = obj.value; 
		inputString = inputString + arrVendorId[i] +'^'+ varSageVendorId +  '|';
		  
	//alert("input is " + inputString);
	}
	
	 
	document.frmSageVendorMap.strOpt.value =  'save'; //'save';
	document.frmSageVendorMap.hinputStr.value = inputString;
	fnStartProgress();
	document.frmSageVendorMap.submit();	
 
} 
 
 
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmACSageVendorMapping.do">
	<html:hidden property="strOpt" value="" />
	<html:hidden property="haction" />
	<html:hidden property="hinputStr" /> 
	<html:hidden property="hmissingNotcheck" />
	 
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4"><fmtSageVendorMapping:message key="LBL_VENDOR_MAPPING_REPORT"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtSageVendorMapping:message key="LBL_HELP" var="VarHelp"/>
			 <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${VarHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		<tr>
			<td bgcolor="#666666" height="1" colspan="5"></td>
		</tr>
		<tr>
			<td width="698" height="100" valign="top" colspan="5">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">  
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtSageVendorMapping:message key="LBL_VENDOR_NAME"/> :</td> 
                        <td>&nbsp;
	                         <html:text property="vendorName"  size="40" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/> 
	                           &nbsp;&nbsp; <fmtSageVendorMapping:message key="LBL_WILD_SEARCH"/>&nbsp;&nbsp;
	                           <fmtSageVendorMapping:message key="LBL_PERFORM_WILD_CARD" var="varPerformWildCardSearch"/>
	                           <a title="${varPerformWildCardSearch}"><img src=<%=strImagePath%>/question.gif border=0</a>
	                    
                        </td> 
                    </tr>       
                    <tr><td colspan="5" class="ELine"></td></tr>
                    
                    
                    <tr>
                    	 <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp; </td> 
                    	<td  colspan="4" class="RightTableCaption"  > &nbsp; <html:checkbox  property="missingSageVenIds" />
						 
						<fmtSageVendorMapping:message key="LBL_MISSING_SAGE_VENDOR"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <fmtSageVendorMapping:message key="BTN_LOAD" var="varLoad"/><gmjsp:button name="Load" value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;&nbsp;" gmClass="button" onClick="fnReload();" buttonType="Load" />
						 </td>								
					</td>
			 		 
				</tr>
             
		 
		<tr>
			<td class="Line" height="1" colspan="5"></td>
		</tr>
		  
		<tr>
			<td colspan="5">
			<display:table name="requestScope.frmSageVendorMap.returnList" requestURI="/gmACSageVendorMapping.do" class="its" id="currentRowObject"  decorator="com.globus.accounts.displaytag.beans.DTSageVendorWrapper"> 
 				<fmtSageVendorMapping:message key="LBL_GM_VEDOR_ID" var="varGmVendorId"/><display:column property="VENDORID" title="${varGmVendorId}"   />
				<fmtSageVendorMapping:message key="LBL_VENDOR_NAME" var="VarVendorName"/><display:column property="VENDORNAME" title="${VarVendorName}"  class="alignleft" /> 
				<fmtSageVendorMapping:message key="LBL_SAGE_VENDOR_ID" var="varSageVendorId"/><display:column property="SAGEVENDORID" title="${varSageVendorId}"  class="aligncenter" />
				 
			</display:table>
			</td>
		</tr>

		<tr>
			<td class="LLine" height="1" colspan="5"></td>
		</tr>
		 
		 <%
			if (rowsize > 0) {
		%> 
		</tr> 
		<tr>
			<td colspan="5" align="center">&nbsp;&nbsp;    <p>&nbsp;</p>
						 <fmtSageVendorMapping:message key="BTN_SUBMIT" var="VarSubmit"/>
                         <gmjsp:button value="${VarSubmit}" gmClass="button" buttonType="Save"  onClick="fnSubmit();" /> <p>&nbsp;</p>
         </td>
		</tr>               
	 	
		<% 
			}
		%> 
		 
		
		 
	</table>
	</FORM>
	 
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>