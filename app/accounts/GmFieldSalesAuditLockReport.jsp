<%@ page language="java" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<%@ taglib prefix="fmtFieldSalesAuditLockReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmFieldSalesAuditLockReport.jsp -->
<fmtFieldSalesAuditLockReport:setLocale value="<%=strLocale%>"/>
<fmtFieldSalesAuditLockReport:setBundle basename="properties.labels.accounts.GmFieldSalesAuditLockReport"/>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strWikiTitle = GmCommonClass.getWikiTitle("FIELD_SALES_AUDIT_LOCK_REPORT");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Field Sales Audit Lock Report</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<bean:define id="alAuditor" name="frmAutoAuditLock" property="alAuditor" type="java.util.List"></bean:define>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>

function fnOpenForm(Id)
{
	var uploadDir="FIELDSALESUPLOADDIR";
    document.frmAutoAuditLock.fileId.value=Id;
	document.frmAutoAuditLock.action = "/GmCommonFileOpenServlet?sId="+Id+"&uploadString="+uploadDir;
	document.frmAutoAuditLock.submit();

}



function fnReload()
{
	
	document.frmAutoAuditLock.auditId.value=document.frmAutoAuditLock.auditId.value;
	document.frmAutoAuditLock.strOpt.value = "loadDistributor";
	document.frmAutoAuditLock.submit();
}function fnLoadReport()
{
	document.frmAutoAuditLock.auditId.value=document.frmAutoAuditLock.auditId.value;
	document.frmAutoAuditLock.distId.value=document.frmAutoAuditLock.distId.value;
	document.frmAutoAuditLock.strOpt.value = "load_lock_report";
	document.frmAutoAuditLock.submit();
}
function fnEnter()
{
		if (event.keyCode == 13)
		{ 
			fnGo();
		}
}
</script>
</HEAD>

<BODY onkeyup="fnEnter();" leftmargin="20" topmargin="10" >
 
<html:form  action="/gmAuditLockReport.do"  >
<html:hidden property="strOpt"/>
<html:hidden property="fileId"/>
<input type="hidden" name="inputString"/ >
<input type="hidden" name="distName"/ >


	<table border="0" class="DtTable850" width="700" cellspacing="0" cellpadding="0">
		<tr>
					<td colspan="6" height="25" class="RightDashBoardHeader">
				<fmtFieldSalesAuditLockReport:message key="LBL_FIELD_SALES_AUDIT_LOCK_REPORT"/>
			</td>
			<td  height="25" class="RightDashBoardHeader">
			<fmtFieldSalesAuditLockReport:message key="LBL_HELP" var="varHelp"/>
			<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr><td colspan="7" class="Line" height="1"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr  class="shade" >
		
		<td height="30" align ="Right" class="RightTableCaption">&nbsp; <fmtFieldSalesAuditLockReport:message key="LBL_AUDIT_NAME"/>:&nbsp;<td>
	   <td><gmjsp:dropdown controlName="auditId" SFFormName="frmAutoAuditLock" SFSeletedValue="auditId" 
							SFValue="alAuditName" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" onChange="fnReload()" /></td>
		<td height="30" align ="Right" class="RightTableCaption">&nbsp; <fmtFieldSalesAuditLockReport:message key="LBL_DISTRIBUTOR_NAME"/>:&nbsp;</td>
		<td colspan="2"  align ="left"><gmjsp:dropdown controlName="distId" SFFormName="frmAutoAuditLock" SFSeletedValue="distId" 
						onChange="fnLoadReport()"	SFValue="aldist" codeId = "DISTID"  codeName = "DISTNAME"  defaultValue= "[Choose One]"  /></td>
		
		<td>&nbsp;</td>
		</tr>
		<tr>
		<td colspan="7">
            
            <display:table name="requestScope.frmAutoAuditLock.ldtAuditLockReport" requestURI="/gmAuditLockReport.do" 
			class="its" id="currentRowObject" decorator="com.globus.accounts.displaytag.beans.DTFieldAuditLockReportWrapper"> 
			<display:column property="FILE_ID" title="#"  class="aligncenter" style="width:20px"/>
			<fmtFieldSalesAuditLockReport:message key="LBL_AUDITOR" var="varAuditor"/><display:column property="AUDITOR" title="${varAuditor}" sortable="true" class="aligncenter" style="width:100px" /> 
			<fmtFieldSalesAuditLockReport:message key="LBL_FILE_NAME" var="varFileName"/><display:column property="FILE_NAME" title="${varFileName}" sortable="true" class="aligncenter" style="width:175px" /> 
			<fmtFieldSalesAuditLockReport:message key="LBL_LOCKED_BY" var="varLockedBy"/><display:column property="UPLOADED_BY" title="${varLockedBy}" sortable="true" class="alignleft" style="width:100px" /> 
			<fmtFieldSalesAuditLockReport:message key="LBL_LOCKED_ON" var="varLockedOn"/><display:column property="CREATED_ON" title="${varLockedOn}" sortable="false" class="alignleft" style="width:120px" /> 			
			</display:table> 
			
		</td>
    </tr> 
		</table>

	

</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>