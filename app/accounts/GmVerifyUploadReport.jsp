<%@ page language="java" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<%@ taglib prefix="fmtVerifyUploadReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmVerifyUploadReport.jsp -->
<fmtVerifyUploadReport:setLocale value="<%=strLocale%>"/>
<fmtVerifyUploadReport:setBundle basename="properties.labels.accounts.GmVerifyUploadReport"/>
<bean:define id="ldtResult" name="frmFieldSalesRptForm" property="ldtResult" type="java.util.List"></bean:define>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strWikiTitle = GmCommonClass.getWikiTitle("VERIFY_UPLOAD");
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	int rowsize = ldtResult.size();
	String strAccountsJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Verify Upload Report</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     
    table.DtTable1000 {
	width: 1200px;
	}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strAccountsJsPath%>/GmFieldSalesRpt.js"></script>

</HEAD>

<BODY onkeyup="fnEnter();" leftmargin="20" topmargin="10" >
 
<html:form  action="/gmFieldSalesRpt.do?method=reportUploadedData"  >
<html:hidden property="strOpt"/>
<input type="hidden" name="hauditEntryID"/ >


	<table border="0" class="DtTable1000" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" height="25" class="RightDashBoardHeader"><fmtVerifyUploadReport:message key="LBL_VERIFY_UPLOADED_DATA"/></td>
			<td  height="25" class="RightDashBoardHeader">
			<fmtVerifyUploadReport:message key="BTN_HELP" var="varHelp"/>
				<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr  class="shade" >		
			<td height="30" align ="Right" class="RightTableCaption"><font color="red">*</font><fmtVerifyUploadReport:message key="LBL_AUDIT_LIST"/>:&nbsp;</td>
		    <td width="200"><gmjsp:dropdown controlName="auditId" SFFormName="frmFieldSalesRptForm" SFSeletedValue="auditId" 
								SFValue="alAuditName" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" onChange="fnReload()"/></td>
			<td height="30" align ="Right" class="RightTableCaption"><font color="red">*</font><fmtVerifyUploadReport:message key="LBL_DISTRIBUTOR"/>:&nbsp;</td>
			<td  align ="left"><gmjsp:dropdown controlName="distId" SFFormName="frmFieldSalesRptForm" SFSeletedValue="distId" 
								SFValue="aldist" codeId = "DISTID"  codeName = "DISTNAME"  defaultValue= "[Choose One]"  /></td>
		</tr>
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		<tr> 
		  <td height="30" align ="Right" class="RightTableCaption">&nbsp; <fmtVerifyUploadReport:message key="LBL_COUNTED_BY"/>:&nbsp;</td>
		  <td align ="left"><gmjsp:dropdown controlName="auditorId" SFFormName="frmFieldSalesRptForm" 
		    SFSeletedValue="auditorId" SFValue="alAuditor" codeId = "USERID"  codeName = "UNAME"  defaultValue= "[Choose One]"  />
		  </td>
	  		<td colspan="2"  align ="left">
	  			<fmtVerifyUploadReport:message key="BTN_LOAD" var="varLoad"/>
		     	<gmjsp:button value="${varLoad}" gmClass="button"  onClick="fnSubmit();" buttonType="Load" />
		    </td>
	  	</tr>      
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		 <tr>
			<td colspan="4"  height="10" >
				<display:table name="requestScope.frmFieldSalesRptForm.ldtResult" requestURI="/gmFieldSalesRpt.do?method=reportUploadedData" defaultsort="1"  export= "true" id="currentRowObject" varTotals="totals"  decorator="com.globus.accounts.displaytag.beans.DTFieldSalesRptWrapper" freezeHeader="true" cellpadding="0" cellspacing="0" >
			 		<fmtVerifyUploadReport:message key="LBL_TAG_ID" var="varTagId"/><display:column property="TAGID" title="${varTagId}" headerClass="ShadeDarkGrayTDCenter" style="text-align: left;width:80;" class="ShadeMedGrayTD"  sortable="true" comparator="com.globus.common.displaytag.beans.DTComparator" />
			 		<display:column  property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
			 		<fmtVerifyUploadReport:message key="LBL_SET_ID" var="varSetId"/><display:column property="SETID" title="${varSetId}" headerClass="ShadeDarkGrayTDCenter" class="ShadeMedGrayTD" style="text-align: left;width:50;" sortable="true" />
					<fmtVerifyUploadReport:message key="LBL_SET_DESCRIPTION" var="varSetDescription"/><display:column property="SETDESC" title="${varSetDescription}"  headerClass="ShadeDarkGrayTD"  class="ShadeMedGrayTD" style="text-align: left;width:200;"  sortable="true"  maxLength="28" titleKey="SETDESC"/>
					<display:column  property="PLINE" title="" headerClass="Line" class="Line"  media="html"/>
					<fmtVerifyUploadReport:message key="LBL_PART" var="VarPart"/><display:column property="PNUM" title="${VarPart}" headerClass="ShadeDarkGrayTDCenter" class="ShadeMedGrayTD" style="text-align: left;width:50;" sortable="true"/> 
					<fmtVerifyUploadReport:message key="LBL_PART_DESC" var="varPartDesc"/><display:column property="PDESC" title="${varPartDesc}" headerClass="ShadeDarkGrayTD" class="ShadeMedGrayTD" style="text-align: left;width:150;"  sortable="true" maxLength="20" titleKey="PDESC"/>									
					<fmtVerifyUploadReport:message key="LBL_CONTROL" var="varControl"/><display:column property="CNUM" title="${varControl}" headerClass="ShadeDarkGrayTDCenter"  class="ShadeMedGrayTD" sortable="true"   titleKey="CNUM"  style="text-align: left;"/>					
					<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html" />
					<fmtVerifyUploadReport:message key="LBL_COMMENT" var="varComment"/><display:column property="COMMENTS" title="${varComment}" headerClass="ShadeDarkGrayTDCenter"  sortable="true"  maxLength="10" titleKey="COMMENTS" class="ShadeMedBlueTD" style="text-align: left;"/>
					<fmtVerifyUploadReport:message key="LBL_STATUS" var="varStatus"/><display:column property="STATUS" title="${varStatus}" headerClass="ShadeDarkGrayTDCenter" sortable="true" style="width:90;text-align: left;" maxLength="12" titleKey="COMMENTS" class="ShadeLightBlueTD" />
					<fmtVerifyUploadReport:message key="LBL_BORROWED" var="varBorrowed"/><display:column property="BORROWED_FRM" title="${varBorrowed}" headerClass="ShadeDarkGrayTDCenter" sortable="true" style="width:70;text-align: left;" maxLength="8" titleKey="BORROWED_FRM" class="ShadeLightBlueTD" />
					<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html" />	
					<fmtVerifyUploadReport:message key="LBL_LOCATION" var="varLocation"/><display:column property="LOCATIONTYPE" title="${varLocation}"   headerClass="ShadeDarkGrayTDCenter" sortable="true" class="ShadeMedBlueTD" style="text-align: left;"/>
					<fmtVerifyUploadReport:message key="LBL_LOCATION_DETAILS" var="varLocationDetails"/><display:column property="LOCATION_DETAIL" title="${varLocationDetails}" headerClass="ShadeDarkGrayTDCenter" sortable="true" maxLength="15" titleKey="LOCATION_DETAIL" style="width:180;text-align: left;" class="ShadeLightBlueTD"/>
					<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html" />	
					<fmtVerifyUploadReport:message key="LBL_COUNTED_DATE" var="varCountedDate"/><display:column property="COUNTEDDT" title="${varCountedDate}"  headerClass="ShadeDarkGrayTDCenter" sortable="true" format="{0,date,'<%=strApplDateFmt%>'}" style="width:50;text-align: left;" class="ShadeMedBlueTD"/>
					<fmtVerifyUploadReport:message key="LBL_COUNTED_BY" var="varCountedBy"/><display:column property="COUNTEDBY" title="${varCountedBy}"  headerClass="ShadeDarkGrayTD"  sortable="true" maxLength="8" titleKey="COUNTEDBY" class="ShadeLightBlueTD"  style="text-align: left;"/>			
				
				<display:footer media="html">
				<tr><td class="Line" colspan="18"></td></tr>
					<tr>					
						<td class = "ShadeDarkGrayTD" colspan="3"> <B> <fmtVerifyUploadReport:message key="LBL_TOTAL_RECORDS"/>: <%=rowsize %></B></td>					
						<td class = "ShadeDarkGrayTD" colspan="16" > </td>
					</tr>
				<tr><td class="Line" colspan="18"></td></tr>	
				</display:footer>
				</display:table>
			</td>
		</tr>
		  <% if (rowsize >0) { %> 
		<tr>
			<td colspan="10" class="RightText" align="Center">
			<BR><b><fmtVerifyUploadReport:message key="LBL_CHOOSE_ACTION"/>:</b>&nbsp;<select name="Cbo_Action" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
				<option value="0" ><fmtVerifyUploadReport:message key="LBL_CHOOSE_ONE"/>
				<option value="Edit"><fmtVerifyUploadReport:message key="LBL_EDIT"/></option>
			</select>
			<fmtVerifyUploadReport:message key="BTN_SUBMIT" var="varSubmit"/>
			<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnChooseAction();" tabindex="25" buttonType="Save" />&nbsp;<BR><BR>
			</td>
		</tr>	
		<% } %>				
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>