
<%
/**********************************************************************************
 * File		 		: GmInvoiceStatementReport.jsp
 * Desc		 		: This screen is used for the Account/Group Statement
 * Version	 		: 1.0
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.Date,java.lang.*" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.TimeZone"%>
 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%> 
<%@ taglib prefix="fmtInvoiceStatementReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmInvoiceStatementReport.jsp -->
<fmtInvoiceStatementReport:setLocale value="<%=strLocale%>"/>
<fmtInvoiceStatementReport:setBundle basename="properties.labels.accounts.GmInvoiceStatement"/>
<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	GmCalenderOperations gmCal = new GmCalenderOperations();
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.accounts.GmInvoiceStatement", strSessCompanyLocale);
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	String strTodaysDate = gmCal.getCurrentDate(strGCompDateFmt);
	String strApplDateFmt = strGCompDateFmt;
	String strFrmDate = (String)request.getAttribute("hFrom")==null?"":(String)request.getAttribute("hFrom");
	String strToDate = (String)request.getAttribute("hTo")==null?"":(String)request.getAttribute("hTo");
	
	HashMap hmCurrency = new HashMap();
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strApplCurrFmt = GmCommonClass.parseNull((String)hmCurrency.get("CMPCURRFMT"));
	String strSessCurrSymbol= GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_SYMB"));
	
	String strAccountCurrSign = GmCommonClass.parseNull((String)request.getAttribute("strAccountCurrSign"));
	String gridData =GmCommonClass.parseNull((String)request.getAttribute("alResult"));
	String strIDAccId = GmCommonClass.parseNull((String)request.getAttribute("IDACCOUNT"));
	String strhScreenType = GmCommonClass.parseNull((String)request.getAttribute("HSCREENTYPE")); 
	String strParentFl = GmCommonClass.parseNull((String)request.getAttribute("Chk_ParentFl"));	
	Date dtFrom=null;
	Date dtTo=null;
	dtFrom  = GmCommonClass.getStringToDate(strFrmDate, strApplDateFmt);
	dtTo  = GmCommonClass.getStringToDate(strToDate, strApplDateFmt);

	int intSize = 0;
	HashMap hmLoop = new HashMap();
	HashMap hcboVal = null;

	String strCodeID = "";
	String strDistId = "";
	String strSelected = "";

	String strInvId = "";
	String strInvDt = "";
	double dbInvAmt = 0.00;
	double dbShipAmt = 0.00;
	double dbPendAmt = 0.00;
	String strCustPO = "";
	String strAccId = "";
	String strAmtPaid = "";
	String strDiscount = "";

	String strAction = (String)request.getAttribute("hAction");
	String strStatementType = (String)request.getAttribute("hStatementType");
	String strStatusFl = "";
	//String strCurrSymbol = (String) session.getAttribute("strSessCurrSymbol");
	String strShade = "";
	String strCallFlag = "";
	String strInvType = "";
	
	ArrayList alAcctInvoice = new ArrayList();
	ArrayList alAccount = new ArrayList();
	ArrayList alPayModes = new ArrayList();
	
	ArrayList alInvTypes = new ArrayList();
	ArrayList alLoop = new ArrayList();
	alLoop = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("GROUPLIST"));
	int intInvTypeSize = 0;
	HashMap hmInvType = null;
	String strInvTypeCodeID = "";
	String strInvTypeCodeNM ="";
	String strInvTypeId = "";
	
	int intLength = 0;

	HashMap hmReturn = new HashMap();
	HashMap hmParam = new HashMap();
	
	String strAccID_PAYALL = "";
	String strInvID_PAYALL = "";
	String strCustPO_PAYALL = "";
	String strInvAmt_PAYALL = "";	
	String strInvType_PAYALL = "";
	String strSortCName = "";
	String strSortType = "";
	String strCurrency= "";
	String strNoRecordMsg= "";
	String strAccCurrId = "";
	String strAccountID = GmCommonClass.parseZero((String)request.getAttribute("hAccId"));
	String strInvSource = GmCommonClass.parseNull((String)request.getAttribute("INVSOURCE"));
	String strIdAccount = GmCommonClass.parseNull((String)request.getAttribute("IDACCOUNT"));
	String strMode = GmCommonClass.parseNull((String)request.getAttribute("hMode"));
	String strType = GmCommonClass.parseNull((String)request.getParameter("hType"));
	ArrayList alType = (ArrayList)request.getAttribute("ALTYPE");
	String strPaymentTerm = GmCommonClass.parseNull((String)request.getAttribute("PAYMENTTERM"));
	double dbInvoiceAmt = 0.00;
	
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	hmParam = (HashMap)request.getAttribute("hmParam");
	alAccount = (ArrayList)hmReturn.get("ACCOUNTLIST");
	strAccCurrId = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_ID"));
	ArrayList alCompCurrency = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCOMPANYCURRENCY"));
	if (strAction.equals("Load") || strAction.equals("Reload"))
	{
		alInvTypes = (ArrayList)hmReturn.get("CODELIST");
				
		if (strAction.equals("Reload"))
		{
			strAccId = (String)request.getAttribute("hAccId");
			if((strAccId.equals("") || strAccId.equals("0")))
			{
				strAccId = strIDAccId;
			}
			//strPaymentTerm = GmCommonClass.parseNull((String)request.getAttribute("PAYMENTTERM"));
		    alAcctInvoice = (ArrayList)hmReturn.get("OPENINVOICES");
			intLength = alAcctInvoice.size();
			alPayModes = (ArrayList)hmReturn.get("PAYMODEDETAILS");
			strAccID_PAYALL = (String)hmParam.get("ACCID");
			strInvID_PAYALL = (String)hmParam.get("INVID");
			strCustPO_PAYALL = (String)hmParam.get("CUSTPO");
			strInvAmt_PAYALL = (String)hmParam.get("INVAMT");
			strInvType_PAYALL = (String)hmParam.get("INVOTYP");
			strSortCName = (String)hmParam.get("SORT");
			strSortType = (String)hmParam.get("SORTTYPE");
		}
	}

	HashMap hmInvTypeForCobo = new HashMap();
	hmInvTypeForCobo.put("ID","");
	hmInvTypeForCobo.put("PID","CODEID");
	hmInvTypeForCobo.put("NM","CODENM");
	   
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Post Multiple Payments - By Account</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="javascript" src="<%=strJsPath%>/GmInvoicePayment.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>
var parentFl = '<%=strParentFl%>';
var strAccId= '<%=strAccId%>';
var objGridData;
var gridIndex;
var gridSortOrder;
var gridSortValue;
var orderByField;
var invAmtIndex;
var recAmtIndex;
var fromDate;
var toDate;
var varIdAccount = '<%=strIdAccount%>';
var strInvoiceType= '<%=strInvType_PAYALL%>';
var strInvoiceID;
var strCustomerPO;
var TotAmt='0.00';
var TotOver='0.00';
var TotOverdue='';
var strAccGrpVal;
var strType='<%=strType%>';
var sessCurSymbol = '<%=strSessCurrSymbol%>';
var AccountCurrSign = '<%=strAccountCurrSign%>';
objGridData = '<%=gridData%>';
var todaysDate = '<%=strTodaysDate%>';
var dateFmt = '<%=strApplDateFmt%>';

var strAction = '<%=strAction%>';
var fromDtDiff;
var toDtDiff;
var strTotalAmtVal;
var strTotalOverdue;
var hlength = '<%=intLength %>';
</script>
</HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<BODY leftmargin="20" topmargin="10" onload="fnLoad1();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmInvoiceServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hStatementType" value=""> 
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hAccId" value="<%=strAccId%>">
<input type="hidden" name="hTotalValue">
<input type="hidden" name="hInvType" value="<%=strInvType%>">
<input type="hidden" name="hCheckedInvTypes" value="<%=strInvType_PAYALL%>">
<input type="hidden" name="hType" value="<%=strType%>">

	<table border="0" class="DtTable950" cellspacing="0" cellpadding="0"> <%--class="border" --%>
		<tr>
			<td bgcolor="#666666" height="1" colspan="3"></td>
		
		</tr>
		<tr>
	<td  colspan="2" height="25" class="RightDashBoardHeader"><fmtInvoiceStatementReport:message key="LBL_INVOICE_STATEMENT_REPORT"/> </td>
	<td align="right" class=RightDashBoardHeader > 	
	<fmtInvoiceStatementReport:message key="LBL_HELP" var="varHelp"/>
		<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','Invoice Statement Report');" /> 
	</td>
	</tr>
	<%
	if(strType.equals("Account"))
	{
		strNoRecordMsg = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INVOICES_FOR_ACCOUNT"));
	%>
	<tr> <td class="RightTextBlue" height="15"><B><fmtInvoiceStatementReport:message key="LBL_PLEASE_CHOOSE_ACCOUNT"/></B>
			</td>	</tr>
	<tr>
			<td class="Line" colspan="3"></td>
	</tr>		
	<tr class="Shade">
		 <td  align="right" height="10" colspan="2">&nbsp;
				<jsp:include page="/accounts/GmIncludeAccountType.jsp" >
				<jsp:param name="DISPACCFL" value="Y"/>
				<jsp:param name="REPACCTLABELFL" value="Y"/> 
				</jsp:include>	
			</td>
			<td valign="top">
			<fmtInvoiceStatementReport:message key="BTN_LOAD" var="varLoad"/>
			 <br>&nbsp;&nbsp;<gmjsp:button value="&nbsp;${varLoad}&nbsp;" controlId="Btn_load" gmClass="Button" onClick="fnGetValues();" tabindex="9" buttonType="Load" />&nbsp;
			</td>
		</tr>	
		<tr>
		<td align="right" colspan="1"><fmtInvoiceStatementReport:message key="LBL_SHOW_REP_ACCOUNT"/>:&nbsp;<input type="checkbox" size="30" name="Chk_ParentFl"  checked="<%=strParentFl %>" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" >					
			</td>
		</tr>
			<tr>
			<td class="Line" colspan="3"></td>
		</tr>
		<tr><td colspan="3">
		<table border="1" width="755" cellspacing="0" cellpadding="0"> <%--class="border"--%>
			 <tr> 
			 <td width="350">
			  <table cellspacing="1" cellpadding="0" border="0" height="40" >
			  <tr>
			 	<td class="RightTablecaption" align="left" valign="top" > &nbsp;<fmtInvoiceStatementReport:message key="LBL_INVOICE_TYPE"/>:&nbsp;&nbsp; </td>
			 	<td valign="top" tabindex="4"><%=GmCommonControls.getChkBoxGroup ("",alInvTypes,"Chk_Invoice_Type",hmInvTypeForCobo)%></td>
			 	</tr>
			 </table>
			</td>
	<%
	}else
	{
		strNoRecordMsg = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INVOICE_FOR_GROUP"));
	%>	
	<tr><td class="RightTextBlue" colspan="2" height="15"><B><fmtInvoiceStatementReport:message key="LBL_CHOOSE_GROUP"/></B>
			</td>	</tr>
	<tr>
			<td class="Line" colspan="3"></td>
	</tr>		
	<tr class="Shade">
			<fmtInvoiceStatementReport:message key="LBL_GROUPS" var="varGroups"/>
			<td height="28" class="RightTableCaption" align="right">&nbsp;&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varGroups}:" td="false"/></td>
			
			<td align="left" height="18" colspan="1">&nbsp;<gmjsp:autolist comboType="DhtmlXCombo" controlName="Cbo_AccId" seletedValue="<%=strAccId%>" value="<%=alLoop%>" codeId="ID" 
			     codeName="NAME"  width="350"  defaultValue=" [Choose One]" tabIndex="1"/></td>
			     <td class="RightTableCaption" align="left"><fmtInvoiceStatementReport:message key="LBL_CURRENCY"/>:&nbsp;<gmjsp:dropdown
									controlName="Cbo_Comp_Curr" seletedValue="<%=strAccCurrId%>"
									value="<%=alCompCurrency%>" codeId="ID" codeName="NAMEALT"
									optionId="NAME" />&nbsp;&nbsp;&nbsp;&nbsp;
									<fmtInvoiceStatementReport:message key="BTN_LOAD" var="varLoad"/>
				&nbsp;&nbsp;&nbsp;&nbsp;<gmjsp:button value="&nbsp;${varLoad}&nbsp;" controlId="Btn_load"  gmClass="Button" onClick="fnGetValues();" tabindex="9" buttonType="Load" />&nbsp;
			</td>
		</tr>	
			<tr>
			<td class="Line" colspan="3"></td>
		</tr>
		<tr><td colspan="3">
		<table border="1" width="755" cellspacing="0" cellpadding="0"> <%--class="border"--%>
			 <tr> 
			 <td width="350">
			  <table cellspacing="1" cellpadding="0" border="0" height="40" >
			  <tr>
			 	<td class="RightTablecaption" align="left" valign="top" > &nbsp;&nbsp;<fmtInvoiceStatementReport:message key="LBL_INVOICE_TYPE"/>:&nbsp;&nbsp; </td>
			 	<td valign="top" tabindex="4"><%=GmCommonControls.getChkBoxGroup ("",alInvTypes,"Chk_Invoice_Type",hmInvTypeForCobo)%></td>
			 	</tr>
			 </table>
			</td>
 	<%
	}
	%>
	
			<td>
			 <table cellspacing="0" cellpadding="0" border="0" width="100%">
				 	<tr height="25">
				 	<fmtInvoiceStatementReport:message key="LBL_INVOICE_ID" var="varInoviceId"/>
			 		<td class="RightTablecaption" align="right"><gmjsp:label type="RegularText"  SFLblControlName="${varInoviceId}:" td="false"/></td>
					<td class="RightText">&nbsp;&nbsp;<input type="text" size="10" value="<%=strInvID_PAYALL%>" name="Txt_InvoiceID" class="InputArea" 
							onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="5" ></td>
				</tr>
				<tr height="25">
				<fmtInvoiceStatementReport:message key="LBL_CUSTOMER_PO" var="varCustomerPO"/>
					<td class="RightTablecaption" align="right"><gmjsp:label type="RegularText"  SFLblControlName="${varCustomerPO}:" td="false"/></td>
					<td class="RightText">&nbsp;&nbsp;<input type="text" size="10" value="<%=strCustPO_PAYALL%>" name="Txt_CustomerPO" class="InputArea" 
							onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="6"></td>
				</tr>
				<tr height="25">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<td align="right" class="RightTablecaption" HEIGHT="24"><fmtInvoiceStatementReport:message key="LBL_FROM_DATE"/>:</td>
										<td class="RightTablecaption">&nbsp;
			                        	<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=(dtFrom==null)?null:new java.sql.Date(dtFrom.getTime())%>"  
				gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
				 onBlur="changeBgColor(this,'#ffffff');" tabIndex="7"/>&nbsp;
			                        	&nbsp;&nbsp;<fmtInvoiceStatementReport:message key="LBL_TO_DATE"/>: 
				                     <gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=(dtTo==null)?null:new java.sql.Date(dtTo.getTime())%>"  
				gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
				onBlur="changeBgColor(this,'#ffffff');" tabIndex="8"/>
				                      	</td>
			 	</tr>
			 </table>
			 </td>
		</tr>
		 </table>
			 </td>
		</tr>
		
<%
	if (strAction.equals("Reload"))
	{	
%>
	<tr><td colspan="3">
		<table border="0" width="755" cellspacing="0" cellpadding="0">	
	<tr><td class="Line" colspan="8"></td></tr>
		<tr>
			<td valign="top" align="center">

				<table border="0" width="100%" cellspacing="0" cellpadding="0" >
						<%
	if(strType.equals("Account") && intLength > 0)
	{
	%>
	 					<tr>
	 					<fmtInvoiceStatementReport:message key="LBL_TOTAL_OUTSTANDING" var="varTotalOutstanding"/>
							<td align="right" nowrap class="RightTablecaption" HEIGHT="16" WIDTH="630" colspan="2">&nbsp;&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varTotalOutstanding}:" td="false"/></td>
							<td align="left" nowrap class="RightText" HEIGHT="16" WIDTH="120" id="Lbl_Total"></td>
						</tr>
						<tr>
						<fmtInvoiceStatementReport:message key="LBL_TOTAL_OVERDUE_AMT" var="varTotalOverDue"/>
							<td align="right" nowrap class="RightTablecaption" HEIGHT="16" WIDTH="630" colspan="2">&nbsp;&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varTotalOverDue}:" td="false"/></td>
							<td align="left" nowrap class="RightText" HEIGHT="16" WIDTH="120" id="Lbl_overdue"></td>
						</tr>
						<tr>
							<td align="right" nowrap class="RightTablecaption" HEIGHT="15" WIDTH="630" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;<fmtInvoiceStatementReport:message key="LBL_PAYMENT_TERM"/>:</td>
							<td align="left" nowrap class="RightText" HEIGHT="15" WIDTH="120" id="Lbl_Term"><%=strPaymentTerm%></td>
						</tr>
						<tr><td class="Line" colspan="8"></td></tr>
							<%
	}if(strType.equals("Group") && intLength > 0)
	{
		
	%>	
	<tr>
							<fmtInvoiceStatementReport:message key="LBL_TOTAL_OUTSTANDING" var="varTotalOutstanding"/>
							<td align="right" nowrap class="RightTablecaption" HEIGHT="20" WIDTH="630" colspan="2">&nbsp;&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varTotalOutstanding}:" td="false"/></td>
							<td align="left" nowrap class="RightText" HEIGHT="20" WIDTH="120" id="Lbl_Total"></td>
						</tr>
						<tr><td class="Line" colspan="8"></td></tr>					
	<%
	}
	%>
	<!-- code included to resolve BUG-3666. To Calculate Outstanding Amount For Debit Account -->
	<%
					if (intLength > 0)
					{
							dbInvoiceAmt = 0;
							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alAcctInvoice.get(i);
								
								
								dbInvAmt = Double.parseDouble(GmCommonClass.parseZero((String)hmLoop.get("INVAMT")));
								strInvTypeId = GmCommonClass.parseNull((String)hmLoop.get("INVTPID"));
								strAmtPaid = GmCommonClass.parseZero((String)hmLoop.get("AMTPAID"));
								
								if(strInvTypeId.equals("50200")	|| strInvTypeId.equals("50202") || strInvTypeId.equals("50203"))// Invoice type, 50200 - regular, 50202 - credit, 50203 - debit
								{
									dbInvoiceAmt = dbInvoiceAmt + (dbInvAmt - Double.parseDouble(strAmtPaid));
								}
								
							}
					}
								
%>
		<input type="hidden" name="hTotalAmt" value="<%=strSessCurrSymbol%>&nbsp;<%=GmCommonClass.getStringWithCommas(""+dbInvoiceAmt)%>">		
		<fmtInvoiceStatementReport:message key="BTN_SUBMIT" var="varSubmit"/>			
					<%if(intLength > 0){%>
				 
					<tr>
						<td colspan="7" >
							<div  id ="arsummarydata" style="height:400px;"></div>
						</td>
					</tr>
					<%if(strType.equals("Account")){%>					
					<tr>
			        <td colspan="6" class="RightText" align="Center"><BR><fmtInvoiceStatementReport:message key="LBL_CHOOSE_ACTION"/>:&nbsp;
				 <select name="Cbo_Action1" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
				 			<option value="0" ><fmtInvoiceStatementReport:message key="LBL_CHOOSE_ONE"/>
							<option value="DE"><fmtInvoiceStatementReport:message key="LBL_DOWNLOAD_TO_EXCEL"/></option>
							<option value="ST"><fmtInvoiceStatementReport:message key="LBL_STATEMENT_TOTAL"/></option>
							<option value="SO"><fmtInvoiceStatementReport:message key="LBL_STATEMENT_OVERDUE"/></option>
						</select>
								<%if(!strAccountID.equals("0")){ 
							
								%>
									&nbsp;&nbsp;&nbsp;&nbsp;
									
									<gmjsp:button value="${varSubmit}" name="Btn_Statement" gmClass="Button" onClick="fnOpenStatement();" buttonType="Load" />&nbsp;&nbsp;&nbsp;
									 <% if(strhScreenType.equals("CrdHld")){%>
					 <fmtInvoiceStatementReport:message key="BTN_CLOSE" var="varClose"/>	
                     <gmjsp:button value="${varClose}"  name="Btn_Close"  gmClass="Button" buttonType="Load"  onClick="window.close()" />
                     <%} %>
								<% }   %>
							</td></td>
						</tr></tr> 
							
			 		<%
					}else{%>
					<tr>
			        <td colspan="6" class="RightText" align="Center"><BR><fmtInvoiceStatementReport:message key="LBL_CHOOSE_ACTION"/>:&nbsp;
				 <select name="Cbo_Action1" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
				 			<option value="0" ><fmtInvoiceStatementReport:message key="LBL_CHOOSE_ONE"/>
							<option value="DE"><fmtInvoiceStatementReport:message key="LBL_DOWNLOAD_TO_EXCEL"/></option>
							<option value="ST"><fmtInvoiceStatementReport:message key="LBL_STATEMENT_TOTAL"/></option>
						</select>
								<%if(!strAccountID.equals("0")){ 
							
								%>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<gmjsp:button value="${varSubmit}" name="Btn_Statement" gmClass="Button" onClick="fnOpenStatement();" buttonType="Load" />
									
								<% }   %>
							</td></td>
						</tr></tr> 
<%
					 }}
					else
					 {
%>
						<tr>
							<td align="center" colspan="7" class="RightTextBlue" height="25">
								<b><%=strNoRecordMsg%></b> 
						</td>
						</tr> 
<%							
					}
%>				
				</table>
			</td>
		</tr>
<%
	}
%>

		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
 
 <input type="hidden" name="hTotalAmtVal" value="">
 <input type="hidden" name="hTotalOverdue" value=""> 
    </table>
</td>
</tr>
</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
