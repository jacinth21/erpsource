 <%
/**********************************************************************************
 * File		 		: GmAccountRollForwardReport.jsp
 * Desc		 		: This screen is used to display Account RollForward Report
 * Version	 		: 1.0
 * author			: Richard
************************************************************************************/
%>
<%@include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Date" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtAccountRollForwardReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmAccountRollForwardReport.jsp -->
<fmtAccountRollForwardReport:setLocale value="<%=strLocale%>"/>
<fmtAccountRollForwardReport:setBundle basename="properties.labels.accounts.GmAccountRollForwardReport"/>
<%
	
	String strWikiTitle = GmCommonClass.getWikiTitle("ACCOUNT_ROLLFORWARD");
	String strApplDateFmt = strGCompDateFmt;
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	ArrayList alOpenBalance = new ArrayList();
	ArrayList alDetailList = new ArrayList();
	ArrayList alAccountList = new ArrayList();
	ArrayList alPlantList = new ArrayList ();
	ArrayList alOwnerCompany = new ArrayList ();
	ArrayList alOwnerCurrency = new ArrayList ();
	ArrayList alDivision = new ArrayList ();
	HashMap hmTempLoop = new HashMap();
						
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	
	// Opening Balance and det
	alOpenBalance 	= (ArrayList)hmReturn.get("OPEN_BALANCE");
	alDetailList	= (ArrayList)hmReturn.get("DETAIL_LIST");
	
	alAccountList	= GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alAccountList"));
	alPlantList = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALPLANT"));
	alOwnerCompany = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALOWNERCOMPANY"));
	alOwnerCurrency = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALOWNERCURRENCY"));
	alDivision = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALDIVISION"));
	
	int intLength = alOpenBalance.size();
	int intDetailList = alDetailList.size();
	
	int intGLAccSize = alAccountList.size();
	
	String strEList		= GmCommonClass.parseNull((String)request.getAttribute("hElementList"));
	String strFrmDate 	= GmCommonClass.parseNull((String)request.getAttribute("hFrom"));
	String strToDate 	= GmCommonClass.parseNull((String)request.getAttribute("hTo"));
	
	String strInvSource = GmCommonClass.parseNull((String)request.getAttribute("INVSOURCE"));
	String strIdAccount = GmCommonClass.parseNull((String)request.getAttribute("IDACCOUNT"));
	String strPlantId = GmCommonClass.parseNull((String)request.getAttribute("hPlantId"));
	String strOwnerCompanyId = GmCommonClass.parseNull((String)request.getAttribute("hOwnerCompanyId"));
	String strOwnerCurrencyId = GmCommonClass.parseNull((String)request.getAttribute("hOwnerCurrency"));
	String strDivisionId =  GmCommonClass.parseNull((String)request.getAttribute("hDivision"));	

	Date dtFromDate = null;
	Date dtToDate = null;
	dtFromDate = GmCommonClass.getStringToDate(strFrmDate,strApplDateFmt);
	dtToDate = GmCommonClass.getStringToDate(strToDate,strApplDateFmt);
	HashMap hmLoop = new HashMap();

	String strAccId = "";
	String strAccName = "";
	String strOrdId = "";
	String strCodeID = "";
	String strElemId = "";
	String strSelected = "";
	String strOrdDate = "";
	String strCSPerson = "";
	String strPrice = "";
	String strShade = "";	
	String strArchiveFl = "";
	String strArchivedDate = "";
	String strColspan = "";
	String strCurCnt = "";
	Date archivedDate = null;
	String strSelectGLAccFl = "";
	
	strArchiveFl = GmCommonClass.parseZero((String)request.getAttribute("Chk_archive_fl"));
	strArchiveFl = strArchiveFl.equals("true")?"checked":"";
	
	strSelectGLAccFl = GmCommonClass.parseZero((String)request.getAttribute("Chk_glaccount_fl"));
	strSelectGLAccFl = strSelectGLAccFl.equals("on")?"checked":"";
	
	
	strArchivedDate = GmCommonClass.parseNull((String)request.getAttribute("ArchivedDate"));
	//Hide/Show Owner Company footer total based on currency count
	if(intDetailList!=0)
	{
	HashMap hmCurCnt = GmCommonClass.parseNullHashMap((HashMap)alDetailList.get(0));
	strCurCnt=GmCommonClass.parseNull((String)hmCurCnt.get("CUR_CNT"));
	}
	strColspan = strCurCnt.equals("2")?"4":"2";
	
	archivedDate = (java.util.Date)(GmCommonClass.getStringToDate(strArchivedDate, "yyyy-MM-dd"));
	strArchivedDate = GmCommonClass.getStringFromDate(archivedDate, strGCompDateFmt);
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Sales Orders Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">  
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     
     table.DtTable600 {
	margin: 0 0 0 0;
	width: 600px;
	border: 1px solid  #676767;
}
</style>
<script>
var date_fmt = '<%=strApplDateFmt%>';
var ownerCompId = '<%=strOwnerCompanyId%>';
var ownerCurrency = '<%=strOwnerCurrencyId%>';
var costPlantId = '<%=strPlantId%>';
var glaccsize = '<%=intGLAccSize%>';

function toggleGLAccountSelection(){
	if(document.frmAccount.Chk_glaccount_fl.checked==1)
		document.frmAccount.Chk_glaccount_fl.checked = 0;
}

function selectAllGLAccount(){
		
	if(document.frmAccount.Chk_glaccount_fl.checked==1){
		for (	i=glaccsize-1; i>=0 ;i--){
	
		this.frmAccount.Cbo_ElemId.options[i].selected = true;
		}
	}else{
		for (i=glaccsize-1; i>=0 ;i--){
			this.frmAccount.Cbo_ElemId.options[i].selected = false;
		}
	}
			
	
}

 function getSelected(opt, filter) {
    var selected = new Array();
    var index = 0;
    var strTag = "";
    
    if (filter == 1) 
    {
    	strTag = "|";
    }
    for (var intLoop=0; intLoop < opt.length; intLoop++) {
       if (opt[intLoop].selected) {
          index = selected.length;
          selected[index] = new Object;
          selected[index].value = opt[intLoop].value;
          selected[index].index = intLoop;
       }
    }
    
    var sel = selected;
    var strSel = "";
    for (var item in sel)
    {       
    	strSel += strTag + sel[item].value + strTag + ",";
	}
    return strSel;
 }


function fnSubmit()
{

	var date_fmt = '<%=strGCompDateFmt%>';
    var ArchivedDate = '<%=strArchivedDate%>';
	var From_date_year = dateDiff(document.frmAccount.Txt_FromDate.value,ArchivedDate,date_fmt);
	var to_date_year = dateDiff(document.frmAccount.Txt_ToDate.value,ArchivedDate,date_fmt);
	var archiveFl =document.frmAccount.Chk_archive_fl.checked;

	fnValidateTxtFld('Txt_FromDate',message_accounts[257]);
	fnValidateTxtFld('Txt_ToDate',message_accounts[258]);
	CommonDateValidation(document.frmAccount.Txt_FromDate,date_fmt,message_accounts[263]+ message[611]);
	CommonDateValidation(document.frmAccount.Txt_ToDate,date_fmt,message_accounts[264]+ message[611]);

	if((archiveFl == true) && ( From_date_year < 0 || to_date_year < 0 ) ){
		Error_Details(message_accounts[282]);
	}

	 if(archiveFl == false && ( From_date_year > 0 ||to_date_year > 0) ){
		Error_Details(message_accounts[283]);
	} 
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
    var strListSel 	= getSelected(this.frmAccount.Cbo_ElemId.options,0);
    var strListPlus = getSelected(this.frmAccount.Cbo_ElemId.options,1);    
    document.frmAccount.hAccountList.value = strListSel;
    document.frmAccount.hAccountListPlus.value = strListPlus;
    fnStartProgress();
	document.frmAccount.submit();
}


function fnDownloadVer(exceltype)
{
	var strSel = getSelected(this.frmAccount.Cbo_ElemId.options);
    document.frmAccount.hAccountList.value = strSel;
    
    var hAccountList 	= document.frmAccount.hAccountList.value;
	var Txt_ToDate		= document.frmAccount.Txt_ToDate.value;	
	var	Txt_FromDate	= document.frmAccount.Txt_FromDate.value;
	var plant_id      = document.frmAccount.Cbo_Plant_Id.value;
	var division_id      = document.frmAccount.cbo_division.value;
	var archive_fl = document.frmAccount.Chk_archive_fl.checked;
	archive_fl = archive_fl==true?'on':'';
	
	fnValidateTxtFld('Txt_FromDate',message_accounts[257]);
	fnValidateTxtFld('Txt_ToDate',message_accounts[258]);
	CommonDateValidation(document.frmAccount.Txt_FromDate,date_fmt,message_accounts[263]+ message[611]);
	CommonDateValidation(document.frmAccount.Txt_ToDate,date_fmt,message_accounts[264]+ message[611]);
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	//var	Cbo_AccId	= document.frmAccount.Cbo_AccId.value;	
	windowOpener("<%=strServletPath%>/GmAccountRollforwardServlet?hExcel=Excel&hAccountList="+hAccountList+"&cbo_division="+division_id+"&Cbo_Plant_Id="+costPlantId+"&Cbo_Owner_Company="+ownerCompId+"&Cbo_Owner_Currency="+ownerCurrency+"&Txt_ToDate="+Txt_ToDate+"&Txt_FromDate="+Txt_FromDate+"&Chk_archive_fl="+archive_fl+"&exceltype="+exceltype,"INVEXCEL","resizable=yes,scrollbars=yes,top=300,left=200,width=800,height=490");
}




function fnViewPostingDetails(ElementID,TransID, ownerId, costPlantId,divisionId)
{
	var Txt_ToDate		= document.frmAccount.Txt_ToDate.value;	
	var	Txt_FromDate	= document.frmAccount.Txt_FromDate.value;
	
	var archive_fl = document.frmAccount.Chk_archive_fl.checked;
	archive_fl = archive_fl==true?'on':'';
	//var	Cbo_AccId	=  document.frmAccount.Cbo_AccId.value;
	//var Cbo_InvSource = document.frmAccount.Cbo_InvSource.value;	
	windowOpener("<%=strServletPath%>/GmInvPostServlet?hAction=Reload&Cbo_ElemId="+ElementID+"&Cbo_TransTp="+TransID+"&cbo_division="+divisionId+"&cbo_inv_plant="+costPlantId+"&cbo_owner_company="+ownerId+"&Txt_ToDate="+Txt_ToDate+"&Txt_FromDate="+Txt_FromDate+"&Chk_archive_fl="+archive_fl,"INVEXCEL","resizable=yes,scrollbars=yes,top=300,left=10,width=1000,height=550");
}	


function Toggle(val)
{
	
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className="";
		tabobj.style.background="#ffffff";
	}
}
  
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmAccountRollforwardServlet">
<input type="hidden" name="hAccountList" value="">
<input type="hidden" name="hAccountListPlus" value="">
<input type="hidden" name="hSource" value="<%=strInvSource%>">
<input type="hidden" name="hAccId" value="<%=strIdAccount%>">

	<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
		<tr><td colspan="2" bgcolor="#666666"></td></tr>
		<tr>
		<td bgcolor="#666666" width="1"></td>
		<td width="100%" valign="top" >
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td height="25" class="RightDashBoardHeader" colspan="4" ><fmtAccountRollForwardReport:message key="LBL_ACCOUNT_ROLLFORWARD_REPORT"/></td>
					<td align="right" class=RightDashBoardHeader colspan="1"> 	
					<fmtAccountRollForwardReport:message key="LBL_HELP" var="varHelp"/>
					<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
					</td>
				</tr>
				<tr><td class="Line" colspan="5"></td></tr>
				<tr>
				<td height="25" class="RightTextBlue" colspan="5">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
				
				<td align="right" class="RightTableCaption"  rowspan="5"><input type="checkbox" <%=strSelectGLAccFl%>  onClick="javascript:selectAllGLAccount();" name="Chk_glaccount_fl" tabindex="7" ><fmtAccountRollForwardReport:message key="LBL_ACCOUNT"/>:&nbsp;</td>
				<td rowspan="5" width="43%">
					<select name="Cbo_ElemId" id="Region" class="RightText" tabIndex="1" MULTIPLE  onChange="javascript:toggleGLAccountSelection()">
<%					int intSize = alAccountList.size();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hmLoop = (HashMap)alAccountList.get(i);
			  			strCodeID = (String)hmLoop.get("ACCELEMID");
			  			strSelected = "";
			  			if (strEList.indexOf("|" + strCodeID + "|") >= 0) 
			  			{		  			
							strSelected = "selected";
						}
%>						<option <%=strSelected%> value="<%=strCodeID%>"><%=hmLoop.get("ACCNM")%></option>
<%			  		}
			  		
%>					</td>
					
					<td align="right" class="RightTableCaption" HEIGHT="24" width="20%">&nbsp;<font color="red">*</font><fmtAccountRollForwardReport:message key="LBL_FROM_DATE"/>:</td>
					<td align="left"  class="RightText" width="25%">&nbsp;
					<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=(strFrmDate.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strFrmDate,strApplDateFmt).getTime())))%>"  gmClass="InputArea" 
						onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="2"/>
					 </td>
					</tr>	
					<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>
					<tr>
						<td align="right" class="RightTableCaption" HEIGHT="24" width="20%">&nbsp;<font color="red">*</font><fmtAccountRollForwardReport:message key="LBL_TO_DATE"/>:</td>
						<td align="left" class="RightText" width="25%">&nbsp;
						<gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=(strToDate.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strToDate,strApplDateFmt).getTime())))%>"  gmClass="InputArea" 
							onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="3"/>
						
						
					</tr>														
				</table></td></tr>
				<tr>
				<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>
				<tr class="Shade">
				<td height="25" class="RightTableCaption" align="right"><fmtAccountRollForwardReport:message key="LBL_OWNER_COMPANY"/>:</td>
				<td align="left">&nbsp;<gmjsp:dropdown controlName="Cbo_Owner_Company" seletedValue="<%=strOwnerCompanyId %>" defaultValue="[Choose One]" codeId="COMPANYID" codeName="COMPANYNM" value="<%=alOwnerCompany %>" tabIndex="4"/></td>
				<td class="RightTableCaption" align="right"><fmtAccountRollForwardReport:message key="LBL_OWNER_CURRENCY"/>:</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown controlName="Cbo_Owner_Currency" seletedValue="<%=strOwnerCurrencyId %>" defaultValue="[Choose One]" codeId="CODEID" codeName="CODENMALT" value="<%=alOwnerCurrency %>" tabIndex="5"/>
				</td>
			    </tr>	
				<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>
				<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
				<tr>						
						<td class="RightTableCaption" height="25" align="Right">&nbsp;<fmtAccountRollForwardReport:message key="LBL_PLANT"/>:</td>
						<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Plant_Id"
									seletedValue="<%= strPlantId %>" tabIndex="6"
									value="<%= alPlantList%>" codeId="PLANTID" codeName="PLANTNM" defaultValue="[Choose One]"  /></td>	
						<td class="RightTableCaption" align="right">&nbsp;</td>
						<td>&nbsp; 
						<span class="RightTableCaption">&nbsp;<fmtAccountRollForwardReport:message key="LBL_DIVISION"/>:</span>
						<span>&nbsp;<gmjsp:dropdown controlName="cbo_division" seletedValue="<%=strDivisionId %>" defaultValue="[Choose One]" codeId="DIVISION_ID" codeName="DIVISION_NAME" value="<%=alDivision %>" tabIndex="7"/></span>
					
						
						</td>
						
						<td>
						<table>
						<tr>
						<td class="RightTableCaption" align="left" ><input type="checkbox" <%=strArchiveFl%>   name="Chk_archive_fl" tabindex="8" >&nbsp;<fmtAccountRollForwardReport:message key="LBL_ARCHIVED_DATA"/>	
						</td>	
						<fmtAccountRollForwardReport:message key="BTN_LOAD" var="varLoad"/>
						<td colspan="7" width="">&nbsp;<gmjsp:button value="&nbsp;${varLoad}&nbsp;" name="Btn_Go" gmClass="button" onClick="javascript:fnSubmit();" buttonType="Load" tabindex="9" /> </td>
						</tr>
						</table>
						</td>
				</tr>
				<tr>
		                <td  class="LLine" height="1" colspan="5"></td></tr>
		        <tr>
		                <td colspan="7" align = "center" class="RightTableCaption" ><span align = "center" id='arcived'><fmtAccountRollForwardReport:message key="LBL_ARCHIVED_DATA_AS_OF"/>  <%=strArchivedDate %></span>
		                </td>
		       </tr>		
				<tr>				
				<td height="25" class="RightTextBlue" colspan="7">
				<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
					<tr><td class="Line" colspan="7"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24"  align="left"><fmtAccountRollForwardReport:message key="LBL_ACCOUNT_NAME"/></td>
						<td HEIGHT="24"><fmtAccountRollForwardReport:message key="LBL_OWNER_COMPANY"/></td>
						<td HEIGHT="24" align="center"><fmtAccountRollForwardReport:message key="LBL_OWNER_CURRENCY"/></td>
						<td HEIGHT="24"  align="right" ><fmtAccountRollForwardReport:message key="LBL_AMOUNT"/></td>
						<td HEIGHT="24" align="center"><fmtAccountRollForwardReport:message key="LBL_LOCAL_CURRENCY"/></td>
						<td HEIGHT="24"  align="right" ><fmtAccountRollForwardReport:message key="LBL_LOCAL_AMOUNT"/></td>
						<td HEIGHT="24"  align="right" ><fmtAccountRollForwardReport:message key="LBL_DIVISION"/></td>
						
					</tr>
<%				if (intLength > 0)
				{
					String strNextId = "";
					int intCount = 0;
					String strLine = "";
					String strTXNID = "";
					String strTotal = "";
					String strAmt = "";
					
					String strLocalAmt = "";
					String strOwnerCD = "";
					String strDetailAccount = "";
					String strCompanyNm = "";
					String strOwnerCurrency = "";
					String strLocalCurrency = "";
					String strOwnerId= "";
					String strDetailOwnerId= "";
					String strDivisionName = "";

					double dbTotal = 0.0;
					double dbLocalTotal = 0.0;
					for (int i = 0;i < intLength ;i++ )
					{
						hmLoop 		= (HashMap)alOpenBalance.get(i);
						strAccId 	= (String)hmLoop.get("ID");
						strAccName 	= (String)hmLoop.get("NAME");
						strAmt		= GmCommonClass.parseZero((String)hmLoop.get("AMT"));
						strLocalAmt = GmCommonClass.parseZero((String)hmLoop.get("LOCAL_AMT"));
						
						dbTotal		= Double.parseDouble(strAmt);
						dbLocalTotal = Double.parseDouble(strLocalAmt);

						intCount++;
						strShade	= (intCount%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
						
%>						<tr id="tr<%=strAccId%>">
							<td colspan="7" height="20" class="RightText">
							<fmtAccountRollForwardReport:message key="LBL_CLICK_TO_EXPAND" var="varClickToExpand"/>
							&nbsp;<A class="RightText" title="${varClickToExpand}" href="javascript:Toggle('<%=strAccId%>')">
							<%=strAccName%></a></td>
						</tr>
						<tr><td colspan="7">
							<div style="display:none" id="div<%=strAccId%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tab<%=strAccId%>_<%=strDivisionId%>">
						</td></tr>
						<tr <%=strShade%>>
							<td height="20" class="RightText" width="320"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtAccountRollForwardReport:message key="LBL_OPENING_BALNCE"/></b></td>
							<td colspan="1" width="250">&nbsp;</td>	
							<td colspan="1">&nbsp;</td>
							<td height="20" class="RightText" align="right" width="130">
								<%= GmCommonClass.getRedText(GmCommonClass.getStringWithCommas((String)hmLoop.get("AMT"))) %></td>
							<td colspan="1">&nbsp;</td>
							<td height="20" class="RightText" align="right">
								<%= GmCommonClass.getRedText(GmCommonClass.getStringWithCommas((String)hmLoop.get("LOCAL_AMT"))) %></td>
							<td colspan="1">&nbsp;</td>	
						</tr>
						<tr><td colspan="10" height="1" bgcolor="#cccccc"></td></tr>
<%						for (int j =0;j < intDetailList; j++)
						{
							hmLoop = (HashMap)alDetailList.get(j);
							strDetailAccount = (String)hmLoop.get("A_NAME");
							strTXNID = (String)hmLoop.get("TXN_ID");
							
							if (strDetailAccount.equals(strAccName))
							{
								intCount++;
								strShade	= (intCount%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
								strAmt		= GmCommonClass.parseZero((String)hmLoop.get("AMT"));
								strLocalAmt = GmCommonClass.parseZero((String)hmLoop.get("LOCAL_AMT"));
								strCompanyNm = GmCommonClass.parseNull((String)hmLoop.get("OWNER_COMPANY"));
								strOwnerCurrency = GmCommonClass.parseNull((String)hmLoop.get("OWNER_CURRENCY"));
								strLocalCurrency= GmCommonClass.parseNull((String)hmLoop.get("LOCAL_CURRENCY"));
								strDetailOwnerId = GmCommonClass.parseNull((String)hmLoop.get("OWNER_COMPANY_ID"));
								strDivisionName =  GmCommonClass.parseNull((String)hmLoop.get("DIVISION_NAME"));
								strDivisionId =  GmCommonClass.parseNull((String)hmLoop.get("DIVISION_ID"));
								
								dbTotal 	= dbTotal + Double.parseDouble(strAmt);
								dbLocalTotal = dbLocalTotal + Double.parseDouble(strLocalAmt);

%>							
						<tr <%=strShade%> >
							<td height="20" class="RightText">
							<fmtAccountRollForwardReport:message key="LBL_CLICK_TO_EXPAND" var="varClickToExpand"/>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - 
								<A class="RightText" title="${varClickToExpand}" href="javascript:fnViewPostingDetails('<%=strAccId%>', '<%=strTXNID%>', '<%=strDetailOwnerId%>', '<%=strPlantId%>', '<%=strDivisionId%>')">
									<%=(String)hmLoop.get("TXN_NAME")%> </a></td>
							<td height="20" class="RightText">&nbsp;<%=strCompanyNm %></td>
							<td height="20" class="RightText" align="center">&nbsp;<%=strOwnerCurrency %></td>
							<td height="20" class="RightText" align="right">
								<%= GmCommonClass.getRedText(GmCommonClass.getStringWithCommas((String)hmLoop.get("AMT"))) %></td>
							<td height="20" class="RightText" align="center">&nbsp;<%=strLocalCurrency %></td>	
							<td height="20" class="RightText" align="right">
								<%= GmCommonClass.getRedText(GmCommonClass.getStringWithCommas((String)hmLoop.get("LOCAL_AMT"))) %></td>
							<td height="20" class="RightText" align="center">&nbsp;<%=strDivisionName %></td>
						</tr>
						<tr><td colspan="7" height="1" bgcolor="#cccccc"></td></tr>
							
<%							}
						}// end of FOR loop j
						intCount++;
						strShade	= (intCount%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows						
%>						<tr <%=strShade%>>
							<td height="20" class="RightText"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtAccountRollForwardReport:message key="LBL_ENDING_BALANCE"/></b></td>
							<td colspan="<%=strColspan%>">&nbsp;</td>
							<%if(strCurCnt.equals("1")){ %>	
							<td height="20" class="RightText" align="right">
								<%= GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(dbTotal+"")) %></td>
							<td>&nbsp;</td>		
							<%} %>
							<td height="20" class="RightText" align="right">
								<%= GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(dbLocalTotal+"")) %></td>
						</tr>
	
						</table></div>	
						<TR><TD colspan=7 height=1 class=Line></TD></TR>					
						
<%					}// end of FOR loop i 
%>
					<tr>
						<fmtAccountRollForwardReport:message key="BTN_DOWNLOAD_TO_EXCEL" var="varDownloadToExcel"/>
						<fmtAccountRollForwardReport:message key="BTN_DOWNLOAD_TO_EXCEL_LIST_VW" var="varDownloadToExcellistvw"/>
						<td colspan="7" align="center" height="30">
						<gmjsp:button value="${varDownloadToExcel}" gmClass="button" onClick="fnDownloadVer('group_view');" tabindex="13" buttonType="Load" />
						<gmjsp:button value="${varDownloadToExcellistvw}" gmClass="button" onClick="fnDownloadVer('list_view');" tabindex="14" buttonType="Load" />
						</td>
						
					</tr>
					
					
<%
						} // End of IF
						else{
%>
					<tr><td colspan="7" class="Line"></td></tr>					
					<tr><td colspan="7" class="RightTextRed" height="50" align=center><fmtAccountRollForwardReport:message key="LBL_ACCOUNT_DISPLAY"/> </td></tr>
<%					
						}
%>
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
			<td  width="1"></td>
		</tr>
		<tr>
			<td colspan="7" height="1" bgcolor="#666666"></td>
		</tr>
    	</table>
		</td>

		</tr>
	</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>	
</BODY>

</HTML>
