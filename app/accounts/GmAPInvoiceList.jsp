<!--
/**********************************************************************************
 * File		 		: GmAPInvoiceList.jsp
 * Desc		 		: AP Invoice List - report dispaying invoice list for given PO
 * Version	 		: 1.0
 * author			: Basudev Vidyasankar
************************************************************************************/
-->

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtAPInvoiceList" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!--GmAPInvoiceList.jsp -->
<fmtAPInvoiceList:setLocale value="<%=strLocale%>"/>
<fmtAPInvoiceList:setBundle basename="properties.labels.accounts.GmAPInvoiceList"/>
<HTML>
<HEAD>
<TITLE> AP Invoice List </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/accounts/GmDHRPayment.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>
<%
String strApplDateFmt = strGCompDateFmt;
String strDateFmt = "{0,date,"+strApplDateFmt+"}";
%>
<BODY>
<html:form action="/gmDHRPayment.do"  >
<html:hidden property="strOpt" value=""/>
<html:hidden property="hpoNumber"/>
<html:hidden property="hpaymentId"/>
	<table border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td width="1150" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2">
							<display:table name="requestScope.frmAPInvoiceDtls.ldtResult" class="its" freezeHeader="true" paneSize="100"
								id="currentRowObject" style="height:35" requestURI="/gmDHRPayment.do" decorator="com.globus.accounts.displaytag.beans.DTInvoiceListWrapper">
								<display:column property="PAYMENTID" title=""/>
								<fmtAPInvoiceList:message key="LBL_INVOICE_NUMBER" var="varInoviceNumber"/><display:column property="INVOICEID" title="${varInoviceNumber}"/>							
								<fmtAPInvoiceList:message key="LBL_INVOCIE_DATE" var="varInvoiceDate"/><display:column property="INVOICEDT" title="${varInvoiceDate}" format= "<%=strDateFmt%>"/>
								<fmtAPInvoiceList:message key="LBL_INVOICE_AMOUNT" var="varInvoiceAmount"/><display:column property="PAYMENTAMOUNT" title="${varInvoiceAmount}" class="alignright" total="true" format="{0,number,#,###,###.00}"/>
								<fmtAPInvoiceList:message key="LBL_PAYMENT_STATUS" var="varPaymentStatus"/><display:column property="PSTATUS" title="${varPaymentStatus}" />
				  				<fmtAPInvoiceList:message key="LBL_RELEASE_DATE" var="varReleaseDate"/><display:column property="RELEASEDT" title="${varReleaseDate}" format= "<%=strDateFmt%>"/>							
				  				<fmtAPInvoiceList:message key="LBL_RELEASE_BY" var="varReleaseBy"/><display:column property="RELEASEBY" title="${varReleaseBy}"/>
								<fmtAPInvoiceList:message key="LBL_BATCH_NUMBER" var="varBatchNumber"/><display:column property="BATCHID" title="${varBatchNumber}"/>							
								<fmtAPInvoiceList:message key="LBL_BATCH_DATE" var="varBatchDate"/><display:column property="BATCHDT" title="${varBatchDate}" format= "<%=strDateFmt%>"/>								
							</display:table>  
						</td>
					</tr>
			   	</table>
  			   </td>
  		  </tr>
	 </table>
</html:form>
</BODY>
</HTML>