<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Iterator,java.lang.*" %>
<%@ taglib prefix="fmtSystemInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmSystemInfo.jsp -->
<fmtSystemInfo:setLocale value="<%=strLocale%>"/>
<fmtSystemInfo:setBundle basename="properties.labels.accounts.GmSystemInfo"/>
<%
String strProdMgmtJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
%>
<script language="JavaScript" src="<%=strProdMgmtJsPath%>/GmFilter.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script type="text/javascript">
<%

String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));


%>

</script>
<bean:define id="deviationQty" name="<%=strFormName%>" property="deviationQty" type="java.lang.String"> </bean:define>
<BODY>
<FORM name="<%=strFormName%>" method="POST" >
<input type="hidden" name="hAction" value="">
<p> &nbsp;</p>
      <table border="0" leftmargin="20" topmargin="10" class="dttable950" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtSystemInfo:message key="LBL_SYSTEM_QTY"/></td>
		</tr>	
         <tr>
         <td>
         	<tr><td> 
			<display:table name="requestScope.frmFlagVarianceForm.alSystemReport" requestURI="/gmFlagVariance.do?auditId=<%=auditId%>&distId=<%=strDistName %>&setId=<%=setId%>" decorator="com.globus.accounts.displaytag.beans.DTFlagVarianceWrapper" excludedParams="strOpt"  class="its"  > 
			<fmtSystemInfo:message key="LBL_TRANSACTION_ID" var="varTransactionId"/><display:column property="TXN_ID" title="${varTransactionId}"   />
			<fmtSystemInfo:message key="LBL_CONTROL_NUMBER" var="varControlNum"/><display:column property="CNUM" title="${varControlNum}"   />	
			<fmtSystemInfo:message key="LBL_QTY" var="varQty"/><display:column property="QTY" title="${varQty}"   />	
			<fmtSystemInfo:message key="LBL_FLAG" var="varFlag"/><display:column property="FLAG" title="${varFlag}"  />
			<fmtSystemInfo:message key="LBL_REASON" var="varReason"/><display:column property="REASONS" title="${varReason}" />	
			<fmtSystemInfo:message key="LBL_APPROVED" var="varApprove"/><display:column style='width:10px' property="APPROVED" title="${varApprove}"   />	
			<fmtSystemInfo:message key="LBL_LAST_UPDATED_BY" var="varLastUpdatedBy"/><display:column property="UPDATED_BY" title="${varLastUpdatedBy}"   />		
			<fmtSystemInfo:message key="LBL_LAST_UPDATED_DATE" var="varLastUpdatedDate"/><display:column property="UPDATED_DATE" title="${varLastUpdatedDate}"   />									
			<fmtSystemInfo:message key="LBL_POSTING_OPTIONS" var="varPostingOptions"/><display:column property="POST" title="${varPostingOptions}"   />
			</display:table>
		<td>
		</tr>
    </table>
      	<tr>	
			<td align="center" colspan="6">
			<p> &nbsp;</p>
			<%if(!deviationQty.equals("0")) {
			%>
				<fmtSystemInfo:message key="BTN_SUBMIT" var="varSubmit"/>
				<gmjsp:button value="${varSubmit}" gmClass="button" name="Submit" buttonType="Save" onClick="fnSubmit(); " /> 
			<%} %>
			</td>		
		</tr>
		<tr>
		<td align="center" colspan="6">
		<p> &nbsp;</p>
    <table border="0" class="dttable950" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtSystemInfo:message key="LBL_AUDIT_COUNT"/></td>
		</tr>	
         	<tr>
         	<td>
   			<display:table name="requestScope.frmFlagVarianceForm.alAuditReport" requestURI="/gmFlagVariance.do?auditId=<%=auditId%>&distId=<%=strDistName %>&setId=<%=setId%>" excludedParams="strOpt"  class="its"  > 
			<fmtSystemInfo:message key="LBL_TAG_ID" var="VarTagId"/><display:column property="TAG_ID" title="${VarTagId}"   />
			<fmtSystemInfo:message key="LBL_CONTROL" var="varControl"/><display:column property="CNUM" title="${varControl} #"  />
			<fmtSystemInfo:message key="LBL_COUNTED_BY" var="varCountedBy"/><display:column property="COUNTED_BY" title="${varCountedBy}"  />
			<fmtSystemInfo:message key="LBL_COUNTED_DATE" var="varCountedDate"/><display:column property="COUNTED_DATE" title="${varCountedDate}"  />		
			<fmtSystemInfo:message key="LBL_COMMEMNTS" var="varComments"/><display:column property="COMMENTS" title="${varComments}" />								
			</display:table> 
		<td>
		</tr>
         </table>
         </td>		
		</tr>
<p> &nbsp;</p>
	<tr>
		<td align="center" colspan="6">
		<p> &nbsp;</p>
    <table border="0" class="dttable950" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtSystemInfo:message key="LBL_OTHER_ASSOCIATED"/> </td>
		</tr>	
         	<tr>
         	<td>
   		   <display:table name="requestScope.frmFlagVarianceForm.alPAssociatedReport" requestURI="/gmFlagVariance.do?auditId=<%=auditId%>&distId=<%=strDistName %>&setId=<%=setId%>" excludedParams="strOpt"  class="its"  > 
			<fmtSystemInfo:message key="LBL_TAG_ID" var="VarTagId"/><display:column property="TAG_ID" title="${VarTagId}"  />
			<fmtSystemInfo:message key="LBL_CONTROL" var="varControlNo"/><display:column property="CONTROL_NO" title="${varControlNo} #"  />
			<fmtSystemInfo:message key="LBL_FLAG" var="varFlag"/><display:column property="FLAG" title="${varFlag}"  />	
			<fmtSystemInfo:message key="LBL_REASON" var="VarReason"/><display:column property="REASON" title="${VarReason}" />								
			</display:table>    
		<td>
		</tr>
         </table>
         </td>		
	</tr>
	
	<p> &nbsp;</p>
	<tr>
		<td align="center" colspan="6">
		<p> &nbsp;</p>
    <table border="0" class="dttable950" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtSystemInfo:message key="LBL_OTHER_UNRESOLVED_QTY"/></td>
		</tr>	
         	<tr>
         	<td>
   	  <display:table name="requestScope.frmFlagVarianceForm.alNAssociatedReport" requestURI="/gmFlagVariance.do?auditId=<%=auditId%>&distId=<%=strDistName %>&setId=<%=setId%>" excludedParams="strOpt"  class="its"  > 
			<fmtSystemInfo:message key="LBL_TAG_ID" var="VarTagId"/><display:column property="TAG_ID" title="${VarTagId}"  />
			<fmtSystemInfo:message key="LBL_CONTROL" var="varControlNo"/><display:column property="CONTROL_NO" title="${varControlNo} #"  />
			<fmtSystemInfo:message key="LBL_FLAG" var="varFlag"/><display:column property="FLAG" title="${varFlag}"  />	
			<fmtSystemInfo:message key="LBL_REASON" var="VarReason"/><display:column property="REASON" title="${VarReason}" />								
			</display:table>   
		<td>
		</tr>
         </table>
         </td>		
	</tr>

</FORM>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>