
 <%
/**********************************************************************************
 * File		 		: GmInvPosting.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*,java.util.Date" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="org.apache.commons.beanutils.RowSetDynaClass"%>
<%@ taglib prefix="fmtInvPosting" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmInvPosting.jsp -->
<fmtInvPosting:setLocale value="<%=strLocale%>"/>
<fmtInvPosting:setBundle basename="properties.labels.accounts.GmInvPosting"/>
<%
String strWikiTitle = GmCommonClass.getWikiTitle("INVENTORY_POSTINGS");

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	int intSize = 0;
	String strCodeID = "";
	String strSelected = "";
	HashMap hmLoop = new HashMap();

	String strElemId = "";
	String strAccTxnId = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strTxnType = "";
	String strTxnDate = "";
	String strAccDate = "";
	String strAccElementId = "";
	String strDRAmt = "";
	String strCRAmt = "";
	String strQty = "";
	String strCost = "";
	String strTxnId = "";
	String strFromDt = "";
	String strToDt = "";
	String strTransId = "";
	String strCompanyId = "";
	String strDivisionId = "";
	String strCountryId = "0";
	String strArchiveFl = "";
	String strArchivedDate="";
	String strColspan = "";
	
	String strApplDateFmt = strGCompDateFmt;
	
	String strInvSource = GmCommonClass.parseNull((String)request.getAttribute("INVSOURCE"));
	String strIdAccount = GmCommonClass.parseNull((String)request.getAttribute("IDACCOUNT"));
	String strAction = (String)request.getAttribute("hAction");

	String strShade = "";
	HashMap hcboVal = null;
	ArrayList alElements = new ArrayList();
	ArrayList alTxnTypes = new ArrayList();
	ArrayList alCompId = new ArrayList();
	ArrayList alCountryList = new ArrayList();
	ArrayList alCurrency = new ArrayList();
	ArrayList alPlant = new ArrayList ();
	ArrayList alOwnerCompany = new ArrayList ();
	ArrayList alDivision = new ArrayList ();
	
	int intLength = 0;

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	alElements = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ELEMENTLIST"));
	alTxnTypes = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("TXNTYPELIST"));
	alCompId = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALCOMPID"));
	alCountryList = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALCOUNTRYLIST"));
	// Internation posting changes
	alCurrency = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALCURRN"));
	alPlant = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALPLANT"));
	alOwnerCompany = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALOWNERCOMPANY"));
	alDivision = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALDIVISION"));
	String strInvPlant = GmCommonClass.parseZero((String)hmReturn.get("hInvPlant"));
	String strOwnerCurrency = GmCommonClass.parseZero((String)hmReturn.get("hOwnerCurrency"));
	String strInvOwnerCompanyId = GmCommonClass.parseZero((String)hmReturn.get("hOwnerCompany"));
	strDivisionId = GmCommonClass.parseZero((String)hmReturn.get("hDivision"));			
	
	if (strAction.equals("Reload"))
	{
		strElemId = GmCommonClass.parseNull((String)request.getAttribute("hElemId"));
		strTxnType = GmCommonClass.parseNull((String)request.getAttribute("hTxnType"));
		strFromDt = GmCommonClass.parseNull((String)request.getAttribute("hFrmDt"));
		strToDt = GmCommonClass.parseNull((String)request.getAttribute("hToDt"));
		strTransId = GmCommonClass.parseNull((String)request.getAttribute("hTransId"));
		strPartNum = GmCommonClass.parseNull((String)request.getAttribute("hPartNum"));
		strCompanyId = GmCommonClass.parseNull((String)request.getAttribute("hCompanyId"));
		strCountryId = GmCommonClass.parseZero((String)request.getAttribute("hCountryId"));
		strArchiveFl = GmCommonClass.parseZero((String)request.getAttribute("Chk_archive_fl"));
		
	}	
	Date dtFromDate = null;
	Date dtToDate = null;
	Date archivedDate = null;
	dtFromDate = GmCommonClass.getStringToDate(strFromDt,strApplDateFmt);
	dtToDate = GmCommonClass.getStringToDate(strToDt,strApplDateFmt);
	strArchivedDate = GmCommonClass.parseNull((String)request.getAttribute("POST_ARCHIVEDDATE"));
	
	archivedDate = (java.util.Date)(GmCommonClass.getStringToDate(strArchivedDate, "yyyy-MM-dd"));
	strArchivedDate = GmCommonClass.getStringFromDate(archivedDate, strGCompDateFmt);
	double dbAmt = 0.0;
	
	strArchiveFl = strArchiveFl.equals("true")?"checked":"";
	String strDateStr = "Date ("+strApplDateFmt +")";
	//Hide/Show Owner Company footer total based on currency count
	String strCurCnt=GmCommonClass.parseNull((String)request.getAttribute("CUR_CNT"));
	strColspan = strCurCnt.equals("2")?"15":"12";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Inventory Posting By Account</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmTransactionLog.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
var date_fmt = '<%=strApplDateFmt%>';
function fnReload(obj)
{
	var date_fmt = '<%=strGCompDateFmt%>';
    var ArchivedDate = '<%=strArchivedDate%>';
	var From_date_year = dateDiff(document.frmAccount.Txt_FromDate.value,ArchivedDate,date_fmt);
	var to_date_year = dateDiff(document.frmAccount.Txt_ToDate.value,ArchivedDate,date_fmt);
	var archiveFl =document.frmAccount.Chk_archive_fl.checked;
	
	CommonDateValidation(document.frmAccount.Txt_FromDate,date_fmt,message_accounts[263]+ message[611]);
	CommonDateValidation(document.frmAccount.Txt_ToDate,date_fmt,message_accounts[264]+ message[611]);

if((archiveFl == true) && ( From_date_year < 0 || to_date_year < 0 ) ){
	Error_Details(message_accounts[261]);
}
 if(archiveFl == false && ( From_date_year > 0 || to_date_year > 0) ){
	Error_Details(message_accounts[262]);
} 
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	fnStartProgress('Y');
	document.frmAccount.hAction.value = "Reload";
	document.frmAccount.submit();
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnDispatchLoad();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmInvPostServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hSource" value="<%=strInvSource%>">
<input type="hidden" name="hAccId" value="<%=strIdAccount%>">

	<table border="0" class="DtTable1200" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" ><fmtInvPosting:message key="LBL_INCENTORY_POSTING"/></td>
			<td align="right" class=RightDashBoardHeader > 	
					<fmtInvPosting:message key="LBL_HELP" var="varHelp"/>
					<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
		</tr>
		<tr>
			<td colspan="2">	
				<table border="0" cellspacing="0" cellpadding="0">
					<tr class="shade">
						<td class="RightTableCaption" height="25" align="Right" width="10%">&nbsp;<fmtInvPosting:message key="LBL_GL_ACCOUNT"/>:</td>
						<td>&nbsp;<select name="Cbo_ElemId" id="Region" class="RightText" tabIndex="1" >
								<option value="0" ><fmtInvPosting:message key="LBL_CHOOSE_ONE"/>
<%
					intSize = alElements.size();
				
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alElements.get(i);
			  			strCodeID = (String)hcboVal.get("ACCELEMID");
						strSelected = strElemId.equals(strCodeID)?"selected":"";
%>
									<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("ACCNM")%></option>
<%
			  		}
%>
							</select>
						</td>	
						<td class="RightTableCaption" align="right" ><fmtInvPosting:message key="LBL_TXN_TYPE"/>:</td>
						<td colspan="3">&nbsp;<select name="Cbo_TransTp" class="RightText" tabIndex="2"  align="top" >
								<option value="0" ><fmtInvPosting:message key="LBL_CHOOSE_ONE"/>
								<option value="00" ><fmtInvPosting:message key="LBL_ALL"/>
<%
					intSize = alTxnTypes.size();
				
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alTxnTypes.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strTxnType.equals(strCodeID)?"selected":"";
%>
								<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
							</select>
						</td>	
					</tr>
													
					<tr><td class="LLine" height="1" colspan="4"></td></tr>
					<tr>
						<td class="RightTableCaption" height="25" align="Right"><fmtInvPosting:message key="LBL_FROM_DATE"/>:</td>
						<td class="RightTableCaption">&nbsp;<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=(strFromDt.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strFromDt,strApplDateFmt).getTime())))%>"  gmClass="InputArea" 
							onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="3"/>&nbsp;&nbsp;(<%=strApplDateFmt %>)
					 			
					 	</td>
					 	<td class="RightTableCaption" align="Right"><fmtInvPosting:message key="LBL_TO_DATE"/>:</td>
					 	<td class="RightTableCaption">&nbsp;<gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=(strToDt.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strToDt,strApplDateFmt).getTime())))%>"  gmClass="InputArea" 
							onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="4"/>&nbsp;&nbsp;(<%=strApplDateFmt %>)		
					 	</td>					 	
					 </tr>
					<tr><td height="1" class="LLine" colspan="4"></td></tr>
					 <tr class="shade">
					 	<td class="RightTableCaption" height="25" align="Right"><fmtInvPosting:message key="LBL_TRANSACTION_DATE"/>:</td>
					 	<td>&nbsp;<input type="text" size="20" value="<%=strTransId%>"  width="250" onBlur="changeBgColor(this,'#ffffff');"
					 			name="Txt_TransId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" tabIndex="5" />
						</td>
					 	<td class="RightTableCaption" height="25" align="Right"><fmtInvPosting:message key="LBL_PART_NUMBER"/>:</td>
					 	<td>
					 		<table border="0" cellpadding="0" cellspacing="0">
					 			<tr><td></td>
					 				<td colspan="2" class="RightTableCaption" align="left">&nbsp;<input type="text" name="Txt_PNum" value="<%=strPartNum%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" size="40" tabIndex="6"></td>								
								    
								</tr>
							</table>
						</td>																	
					</tr>					
					<tr><td height="1" class="LLine" colspan="4"></td></tr>
					<tr>
						<td class="RightTableCaption" height="25" align="Right" width="10%">&nbsp;<fmtInvPosting:message key="LBL_OWNER_COMPANY"/>:</td>
			            <td>&nbsp;<gmjsp:dropdown controlName="cbo_owner_company" seletedValue="<%=strInvOwnerCompanyId %>" defaultValue="[Choose One]" codeId="COMPANYID" codeName="COMPANYNM" value="<%=alOwnerCompany %>" tabIndex="7"/></td>
						<td class="RightTableCaption" align="right" ><fmtInvPosting:message key="LBL_OWNER_CURRENCY"/>:</td>
						<td>&nbsp;<gmjsp:dropdown controlName="cbo_owner_currency" seletedValue="<%=strOwnerCurrency %>" defaultValue="[Choose One]" codeId="CODEID" codeName="CODENMALT" value="<%=alCurrency %>" tabIndex="8"/></td>
							
					</tr>	
					<tr><td class="LLine" height="1" colspan="4"></td></tr>
					<tr class="shade">
					<td class="RightTableCaption" height="25" align="Right" width="10%">&nbsp;<fmtInvPosting:message key="LBL_PLANT"/>:</td>
					<td>
						&nbsp;<gmjsp:dropdown controlName="cbo_inv_plant" seletedValue="<%=strInvPlant %>" defaultValue="[Choose One]" codeId="PLANTID" codeName="PLANTNM" value="<%=alPlant %>" tabIndex="9"/>
						<span class="RightTableCaption">&nbsp;<fmtInvPosting:message key="LBL_DIVISION"/>:</span>
						<span>&nbsp;<gmjsp:dropdown controlName="cbo_division" seletedValue="<%=strDivisionId %>" defaultValue="[Choose One]" codeId="DIVISION_ID" codeName="DIVISION_NAME" value="<%=alDivision %>" tabIndex="10"/></span>
					</td>
 					<td colspan="2">
						<table border="0" cellpadding="0" cellspacing="0">
						<tr>
						<td colspan="3">&nbsp;
						</td>
						<td class="RightTableCaption" align="left" ><input type="checkbox" <%=strArchiveFl%>   name="Chk_archive_fl" tabindex="11">&nbsp;<fmtInvPosting:message key="LBL_ARCHIVED_DATA"/></td>
						<fmtInvPosting:message key="BTN_LOAD" var="varLoad"/>
						<td align="center"><gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="javascript:fnReload(this);" tabindex="12" buttonType="Load" /></td>
						</tr>
						</table>
					</td>
					</tr>
					<tr><td class="LLine" height="1" colspan="4"></td></tr>
					<tr>
		                <td colspan="4" align = "center" height="4" class="RightTableCaption"  ><span align = "center" id='arcived'><fmtInvPosting:message key="LBL_ARCHIVED_DATA_AS_OF"/>  <%=strArchivedDate %></span>
		                </td>
		            </tr>		
				</table>
			</td>
		</tr>
		<tr><td class="LLine" height="1" colspan="2"></td></tr>
		<tr height="30">
			<td align="center" colspan="2" class="RegularText">
				
		    <display:table name="requestScope.REPORT.rows" class = "its" id="currentRowObject" export="true" varTotals="totals"  decorator="com.globus.displaytag.beans.DTAccPostingWrapper"> 
            <display:setProperty name="export.excel.filename" value="Inventory Posting.xls" />
			<fmtInvPosting:message key="LBL_PART_NUMBER" var="varPartNumber"/><display:column property="PNUM" title="${varPartNumber}" sortable="true" />
			<fmtInvPosting:message key="LBL_DESCRIPTION" var="varDescription"/><display:column property="PDESC" escapeXml="true" title="${varDescription}" sortable="true" />
			<fmtInvPosting:message key="LBL_QTY" var="varQty"/><display:column property="QTY" class="aligncenter" title="${varQty}" sortable="true" />
			<fmtInvPosting:message key="LBL_PLANT_NAME" var="varPlantNm"/><display:column property="PLANT_NM" class="aligncenter" title="${varPlantNm}" sortable="true" />
			<fmtInvPosting:message key="LBL_OWNER_COMPANY" var="varOwnerComp"/><display:column property="OWNER_COMPANY_CD" class="aligncenter" title="${varOwnerComp}" sortable="true" maxLength="10" />
			<fmtInvPosting:message key="LBL_TXN_COMPANY" var="varTxnComp"/><display:column property="TXN_COMPANY_CD" class="aligncenter" title="${varTxnComp}" sortable="true" maxLength="10" />
			<fmtInvPosting:message key="LBL_ICT_COMPANY" var="varICTComp"/><display:column property="DEST_COMPANY_CD" class="aligncenter" title="${varICTComp}" sortable="true" maxLength="10" />
			<fmtInvPosting:message key="LBL_TYPE" var="varType"/><display:column property="TXNTYPE" title="${varType}" sortable="true" />
			<fmtInvPosting:message key="LBL_TXN_ID" var="varTxnId"/><display:column property="TXNID" title="${varTxnId}" sortable="true" />
			<display:column property="TXNDATE" title="<%=strDateStr %>"  />
			<fmtInvPosting:message key="LBL_TIME" var="varTime"/><display:column property="TXNTIME" title="${varTime}" sortable="true" />
			<fmtInvPosting:message key="LBL_CREATED_BY" var="varCreatedBy"/><display:column property="CREATEDBY" title="${varCreatedBy}" sortable="true" />
			<fmtInvPosting:message key="LBL_DRAMT" var="varDrAmt"/><display:column property="DRAMT"  title="${varDrAmt}" headerClass="ShadeGreenAlignRightTD" class="ShadeGreenAlignRightTD" sortable="true" total="true"/>
			<fmtInvPosting:message key="LBL_CR_AMT" var="varCrAmt"/><display:column property="CRAMT"  title="${varCrAmt}" headerClass="ShadeGreenAlignRightTD" class="ShadeGreenAlignRightTD" sortable="true" total="true"/>
			<fmtInvPosting:message key="LBL_OWNER_CURRENCY" var="varOwnerCurr"/><display:column property="OWNER_CURRENCY"  title="${varOwnerCurr}" headerClass="ShadeGreenAlignCenterTD" class="ShadeGreenAlignCenterTD" sortable="true" />
			<fmtInvPosting:message key="LBL_LOCAL_COMPANY_DR" var="varLocalDr"/><display:column property="CORP_DR_AMT" headerClass="ShadeDarkBlueAlignRightTD" class="ShadeDarkBlueAlignRightTD"  title="${varLocalDr}" sortable="true" total="true"/>
			<fmtInvPosting:message key="LBL_LOCAL_COMPANY_CR" var="varLocalCr"/><display:column property="CORP_CR_AMT" headerClass="ShadeDarkBlueAlignRightTD" class="ShadeDarkBlueAlignRightTD"  title="${varLocalCr}" sortable="true" total="true"/>
			<fmtInvPosting:message key="LBL_LOCAL_COMPANY_CURRENCY" var="varLocalCurr"/><display:column property="COMPANY_CURRENCY" headerClass="ShadeDarkBlueAlignCenterTD" class="ShadeDarkBlueAlignCenterTD" title="${varLocalCurr}" sortable="true" />
			<fmtInvPosting:message key="LBL_DIVISION" var="vardivision"/><display:column property="DIVISION" title="${vardivision}" sortable="true" />
			<display:footer media="html"> 
				<% 
						String strLocalDebitAmount ;
						String strLocalCreditAmount ;
						String strDebitAmount ;
						String strCreditAmount ;
						
						strDebitAmount = ((HashMap)pageContext.getAttribute("totals")).get("column13").toString();
				  		String strDRamt = GmCommonClass.getStringWithCommas(strDebitAmount);
				  		
				  		strCreditAmount = ((HashMap)pageContext.getAttribute("totals")).get("column14").toString();
				  		String strCRamt  = GmCommonClass.getStringWithCommas(strCreditAmount);
						
				  		strLocalDebitAmount = ((HashMap)pageContext.getAttribute("totals")).get("column16").toString();
				  		String strLocalDR = GmCommonClass.getStringWithCommas(strLocalDebitAmount);
				  		
				  		strLocalCreditAmount = ((HashMap)pageContext.getAttribute("totals")).get("column17").toString();
				  		String strLocalCR = GmCommonClass.getStringWithCommas(strLocalCreditAmount);
				%>
				  	<tr class = shade>
						  		<td colspan="<%=strColspan%>" align="right"> <B><fmtInvPosting:message key="LBL_TOTAL"/>  : </B></td>
						  		<%if(strCurCnt.equals("1")){ %>
						  		<td class = "alignright" ><B><%=strDRamt%></B></td>
						  		<td class = "alignright" ><B><%=strCRamt%></B></td>
						  		<td></td>
						  		<%} %>
						  		<td class = "alignright" ><B><%=strLocalDR%></B></td>
						  		<td class = "alignright" ><B><%=strLocalCR%></B></td>
						  		<td class = "alignright" >&nbsp;</td>
						  		<td class = "alignright" >&nbsp;</td>
						  		
				  	</tr>
			 </display:footer>
 			</display:table> 
			</td>
		</tr>
    </table>

</FORM>

<%@ include file="/common/GmFooter.inc" %>	
</BODY>

</HTML>
