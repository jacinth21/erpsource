 <%
/**********************************************************************************
 * File		 		: GmOUSInvoicePrint.jsp
 * Desc		 		: This screen is used for the print version of OUS Invoice
 * Version	 		: 1.0
 * author			: JAmes
************************************************************************************/
%>
<%@ page language="java" %>
<%--
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
 --%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ include file="/common/GmHeader.inc" %>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	log = GmLogger.getInstance(GmCommonConstants.CUSTOMERSERVICE);
	
	String strAction = (String)request.getAttribute("hAction");
	String strInvoiceMode = "";
	if (strAction == null)
	{
		strAction = (String)session.getAttribute("hAction");
	}
	strAction = (strAction == null)?"Load":strAction;

	HashMap hmReturn = new HashMap();

	ArrayList alOrderNums = new ArrayList();
	ArrayList alDOControlNum = new ArrayList();
	ArrayList alDOCNums = new ArrayList();
	HashMap hmCartDetails = new HashMap();
	HashMap hmConstructs = new HashMap();
	HashMap hmDoCnum = new HashMap();
	
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	//strInvoiceMode = GmCommonClass.parseNull((String)request.getAttribute("hVoidFlag"));
	String strApplnDateFmt = strGCompDateFmt;
	String strApplnCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	String strInvoiceFormat = GmCommonClass.parseNull((String)request.getAttribute("INVJASNAME"));
	strInvoiceFormat = strInvoiceFormat.equals("") ? "/GmUSInvPrintJasper.jasper":strInvoiceFormat;
	log.debug("strInvoiceFormat in JSP"+strInvoiceFormat);
	HashMap hmOrderDetails = new HashMap();
	HashMap hmOrderAttrib = null;
	HashMap hmAccAttrib = null;
	HashMap hmInvAttrib = null;
	String strPayNm = "";
	String strOrdId = "";
	String strAckOrdOn = "";
	String strOriginal="";
	String strDuplicate="";
	String subReportPath="";
	String strPayTermId = "";
	String strInvoiceCompanyId = GmCommonClass.parseNull((String)request.getAttribute("CompanyId"));
	String strCompanyLocale = GmCommonClass.getCompanyLocale(strInvoiceCompanyId);
	// Locale
    GmResourceBundleBean rbPaperWork = GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    GmResourceBundleBean rbCompany = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	int intInvoiceCopySize = Integer.valueOf(GmCommonClass.parseZero(rbPaperWork.getProperty("INVOICE.GM_INV_PARTNOCNT")));
    String strInvXhtmlFlag =
        GmCommonClass.parseNull(rbPaperWork.getProperty("XHTML_INVOICE_FLAG"));
	HashMap hmReturnData = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("INVRPTDATA"));
	ArrayList alHashMap = GmCommonClass.parseNullArrayList((ArrayList)hmReturnData.get("ALORDERLIST"));
	hmOrderDetails = GmCommonClass.parseNullHashMap((HashMap)hmReturnData.get("HMORDERDETAILS"));
	hmOrderDetails.put("ACCOUNTVATNUM", GmCommonClass.parseNull((String)request.getAttribute("ACCOUNTVATNUM")));
	hmOrderDetails.put("ALORDERLIST",alHashMap);
	hmOrderDetails.put("SIZE",alHashMap.size());
	String strSetRbObjectFl =
	        GmCommonClass.parseNull(rbPaperWork.getProperty("INVOICE.ADD_RESOURCE_BUNDLE_OBJ"));
%>
<HTML>
<HEAD>
<TITLE> GlobusOne Enterprise Portal: Invoice Print </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<style>
.PageBreak {
	page-break-after: always;
}
</style>
<script type="text/javascript">
function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screenOus.css">

</HEAD>
<BODY leftmargin="0" topmargin="0" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM method="post" action = "<%=strServletPath%>/GmInvoiceServlet">
<iframe id="ifmPrintContents" style="height: 0px; width: 0px; position: absolute"></iframe>
<%if(strInvXhtmlFlag.equals("YES")){ %>
<table align="center" width="100%">
<tr>
	<td align="center" id="button">
		<gmjsp:button value="Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />
 		<gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" />
 	</td>
 </tr>
 </table>	
 <%}%>	
<div id="jasper" > 
		<%
		String strHtmlJasperRpt = "";
		for (int i=1;i<=intInvoiceCopySize;i++){
			if(i==1){
				hmOrderDetails.put("DOCTYPE",strOriginal);
			}else{
				hmOrderDetails.put("DOCTYPE",strDuplicate+" "+(i-1));
			}	
		//log.debug("hmOrderDetails before going to Jasper"+hmOrderDetails);
		//log.debug("alHashMap before going to Jasper"+alHashMap);
			String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
			GmJasperReport gmJasperReport = new GmJasperReport();								
			gmJasperReport.setRequest(request);
			gmJasperReport.setResponse(response);
			gmJasperReport.setJasperReportName("invoice"+strInvoiceFormat);
			gmJasperReport.setHmReportParameters(hmOrderDetails);							
			gmJasperReport.setReportDataList(alHashMap);				
			gmJasperReport.setBlDisplayImage(true);
			// to set the dynamic resource bundle object
	        if (strSetRbObjectFl.equalsIgnoreCase("YES") ) {
	          gmJasperReport.setRbPaperwork(rbPaperWork.getBundle());
	        }
 			if(strInvXhtmlFlag.equals("YES")){
			strHtmlJasperRpt = gmJasperReport.getXHtmlReport();
			}else{ 
			  strHtmlJasperRpt = gmJasperReport.getHtmlReport();
			}
			
		 if(intInvoiceCopySize!=1 && i!=intInvoiceCopySize-1){
		 %>	<p class="PageBreak"></p>
		<%}}%>	
		<%=strHtmlJasperRpt %>			
</div>
<%if(!strInvXhtmlFlag.equals("YES")){ %>
<table align="center" width="100%">
<tr>
	<td align="center" id="button">
		<gmjsp:button value="Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />
 		<gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" />
 	</td>
 </tr>
 </table>	
 <%}%>	
</FORM>
</BODY>
</HTML>
