
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@page import="com.globus.common.beans.GmGridFormat"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtCycleCountRank" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtCycleCountRank:setBundle basename="properties.labels.accounts.cyclecount.GmCycleCountRankRpt" />
<bean:define id="gridData" name="frmCycleCountReport" property="xmlString" type="java.lang.String"></bean:define>
<bean:define id="projectId" name="frmCycleCountReport" property="projectId" type="java.lang.String"></bean:define>
<bean:define id="projectName" name="frmCycleCountReport" property="projectName" type="java.lang.String"></bean:define>
<%
String strAccountsCycleJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_CYCLECOUNT");
String strWikiTitle = GmCommonClass.getWikiTitle("CYCLE_COUNT_RANK_REPORT");
String strApplDateFmt = strGCompDateFmt;

%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cycle Count - Rank Report</title>

<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strAccountsCycleJsPath%>/GmCycleCountRankRpt.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css"> 
<script>
    var objGridData;
    objGridData = '<%=gridData%>';
    var date_format = '<%=strApplDateFmt%>';
    
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad()" onkeyup="fnEnter();">
 <html:form action="/gmCycleCountReport.do?method=loadRankRpt">
  <html:hidden property="strOpt" value="" />
  <html:hidden property="projectName"/>
  <table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
    <tr>
       <td colspan="3" height="25" class="RightDashBoardHeader"><fmtCycleCountRank:message key="LBL_CYCLE_COUNT_RANK_REPORT" /></td>
	   <td height="25" class="RightDashBoardHeader"><fmtCycleCountRank:message key="IMG_HELP" var="varHelp" />
	    <img align="right" id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	   </td>
	</tr>
	<tr><td class="LLine" colspan="4" height="1"></td></tr>
	<tr>
	  <td class=RightTableCaption align="right" width="100"><font color="red">*</font><fmtCycleCountRank:message key="LBL_YEAR"/>:</td>
	  <td style="width:350px;">&nbsp;<gmjsp:dropdown controlName="cycleCountYear" SFFormName="frmCycleCountReport" SFSeletedValue="cycleCountYear" SFValue="alYearDropDown" 
			codeId ="ID" codeName = "YEAR"  tabIndex="1" defaultValue="[Choose One]"/>
	  </td>
	  <td class="RightTableCaption" align="right" height="30"><fmtCycleCountRank:message key="LBL_RANK" />:</td>
	  <td>&nbsp;<gmjsp:dropdown controlName="rank" SFFormName="frmCycleCountReport" SFSeletedValue="rank" SFValue="alRank" codeId="RANK" codeName="RANK"
		defaultValue="[Choose One]" tabIndex="2" />
	</td>
	</tr>
	<tr><td class="LLine" colspan="4" height="1"></td></tr>
	<tr class="Shade" height="30">
				<td class="RightTableCaption" align="right" >&nbsp;<fmtCycleCountRank:message key="LBL_PRO_LIST"/>:</td>
				<td>
				<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="projectId" />
							<jsp:param name="METHOD_LOAD" value="loadProjectNameList&searchType=LikeSearch&searchBy=ProjectId" />
							<jsp:param name="WIDTH" value="400" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="TAB_INDEX" value="3"/>
							<jsp:param name="CONTROL_NM_VALUE" value="<%=projectName %>" /> 
							<jsp:param name="CONTROL_ID_VALUE" value="<%=projectId %>" />
							<jsp:param name="SHOW_DATA" value="100" />
							<jsp:param name="AUTO_RELOAD" value="" />					
							</jsp:include>
				</td>
				<td class="RightTableCaption" align="right"><fmtCycleCountRank:message key="LBL_FAMILY"/>:</td>
				<td>&nbsp;<gmjsp:dropdown controlName="productFamily" SFFormName="frmCycleCountReport" SFSeletedValue="productFamily"
							 SFValue="alProductFamily" codeId="CODEID" codeName="CODENM"	defaultValue="[Choose One]" tabIndex="4" />
				</td>
	</tr>
	<tr><td class="LLine" colspan="4" height="1"></td></tr>			
	<tr>
	  <td class="RightTableCaption" align="right" height="30"><fmtCycleCountRank:message key="LBL_PART" />:</td>
	<td colspan="2">&nbsp;<html:text size="69" name="frmCycleCountReport" property="partNo" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="5" />
	<html:select property ="pnumSuffix"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="6" >
		<html:option value="0" ><fmtCycleCountRank:message key="LBL_CHOOSEONE"/></html:option>
		<html:option value="LIT" ><fmtCycleCountRank:message key="LBL_LITERAL"/></html:option>
		<html:option value="LIKEPRE" ><fmtCycleCountRank:message key="LBL_LIKE_PREFIX"/></html:option>
		<html:option value="LIKESUF" ><fmtCycleCountRank:message key="LBL_LIKE_SUFFIX"/></html:option>
	</html:select>
	</td>
	<td align="left" colspan="1" height="30">
	     <gmjsp:button name="Btn_Load" value="Load" gmClass="button" onClick="fnLoad();" buttonType="Load" tabindex="7" /> 
	 </td>
	</tr>
	<tr><td class="LLine" colspan="4" height="1"></td></tr>
	<%
			  if (gridData.indexOf("cell") != -1) {
			%>
	<tr>
	   <td colspan="4">
				<div id="dataGridDiv" height="400px" width="1000px"></div>			
		    </td>	
	</tr>
	<tr><td class="LLine" colspan="4" height="1"></td></tr>
	<tr>
		<td colspan="4" align="center">
			<div class='exportlinks'><fmtCycleCountRank:message key="LBL_EXPORT_OPTIONS" />:<img src='img/ico_file_excel.png' />&nbsp;<a href="#"onclick="fnDownloadXLS();">
			<fmtCycleCountRank:message key="LBL_EXCEL" /> </a>
			</div>
		</td>
	</tr>
	<%
			  } else {
			%>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr height="30">
				<td colspan="4" align="center"><fmtCycleCountRank:message
						key="LBL_NO_DATA_AVAILABLE" /></td>
			</tr>
			<%
			  }
			%>
 </table>


</html:form>

<%@ include file="/common/GmFooter.inc"%>
</body>

</html>