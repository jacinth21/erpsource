<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

 <!-- \accounts\cyclecount\GmCycleCountAddNewparts.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtCycleCountAddParts" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmtCycleCountAddParts:setBundle basename="properties.labels.accounts.cyclecount.GmCycleCountAddParts" />
<bean:define id="xmlPartData" name="frmCycleCountRecord" property="xmlPartData" type="java.lang.String"></bean:define>
<%
String strAccountsCycleJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_CYCLECOUNT");
String strWikiTitle = GmCommonClass
.getWikiTitle("CYCLE_COUNT_ADD_NEW_PARTS");
%>
<html>
<head>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script language="JavaScript" src="<%=strAccountsCycleJsPath%>/GmCycleCountAddParts.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">


<script>
var objGridData;
objGridData = '<%=xmlPartData%>';

</script>
</head>

<body leftmargin="20" topmargin="10" onload="fnOnPageLoad()">
	<html:form action="/gmCycleCountRecord.do?method=addnewParts">
		<html:hidden property="lockid" />
			<html:hidden property="partInputString" />
				<html:hidden property="inputString" />
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="3" height="25"  class="RightDashBoardHeader">
		<fmtCycleCountAddParts:message key="LBL_ADD_NEW_PARTS"/>
		</td>
		<td height="25" class="RightDashBoardHeader">
		<fmtCycleCountAddParts:message key="IMG_HELP" var="varHelp" />
		<img align="right" id='imgEdit'style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}'width='16' height='16'
			onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		</td>
	</tr>	
	<tr>
           	<td colspan="4">
				<div id="dataGridDiv"  height="400px" width="700px"></div>
		    </td>	
	</tr>	
	
			<tr height="30">
				<td colspan="4" align="center">
					<div class='exportlinks'>
						<fmtCycleCountAddParts:message key="LBL_EXPORT_OPTIONS" />
						: <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
							onclick="fnDownloadXLS();"><fmtCycleCountAddParts:message
								key="LBL_EXCEL" /> </a>
					</div>
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr>
                <td colspan="4" align="center">
                <gmjsp:button name="Btn_Submit" value="Submit" gmClass="button"
							onClick="javascript:fnAddPartsSubmit();" buttonType="Save"/>
                </td>
		</tr>
</table>
</html:form>


</body>