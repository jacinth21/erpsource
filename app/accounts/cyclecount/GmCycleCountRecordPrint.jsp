
<%
  /***************************************************************************************************************************************
   * File		 		: GmCycleCountRecordPrint.jsp
   * Desc		 		: This screen is used for printing the Cycle count sheet details
   ****************************************************************************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmJasperReport"%>
<%@ page import="java.util.HashMap,java.util.*,java.text.*"%>
<%@ page isELIgnored="false"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<bean:define id="hmData" name="frmCycleCountRecord" property="hmData"
	type="java.util.HashMap"></bean:define>
 <!-- \accounts\cyclecount\GmCycleCountRecordPrint.jsp -->
<%
String strAccountsCycleJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_CYCLECOUNT");
  request.setCharacterEncoding("UTF-8");
  response.setContentType("text/html; charset=UTF-8");
  response.setCharacterEncoding("UTF-8");

  HashMap hmHeaderDtls = new HashMap();
  // to get the header infor
  hmHeaderDtls = GmCommonClass.parseNullHashMap((HashMap) hmData.get("HEADER"));
  ArrayList alResult = GmCommonClass.parseNullArrayList((ArrayList) hmData.get("LOCK_DTLS"));
  String strLotTrackFl = GmCommonClass.parseNull((String) hmData.get("LOTTRACKFL"));
  String strHtmlJasperRpt = "";
%>
<HTML>
<HEAD>

<script language="JavaScript"
	src="<%=strAccountsCycleJsPath%>/GmCycleCountRecord.js"></script>

</HEAD>
<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();"
	onafterprint="showPrint();">
	<%
	  int arraySize = alResult.size();
	%>
	<table>
	<tr>
	<td>
		<div id="button" align="center">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr align="center">
					<td height="30"><gmjsp:button value="Print"
							name="Btn_Print" gmClass="button" buttonType="Load"
							onClick="fnPrint();" />&nbsp;&nbsp;<gmjsp:button value="Close"
							name="Btn_Close" gmClass="button" buttonType="Load"
							onClick="fnClose();" />&nbsp;</td>
				<tr>
			</table>
		</div>
		</td>
		</tr>
		<tr>
			<td><div>
					<%
					  if (arraySize == 0) {
					%>

					<%
					  } else if (!hmHeaderDtls.equals("")) {
							    GmJasperReport gmJasperReport = new GmJasperReport();
							    gmJasperReport.setRequest(request);
							    gmJasperReport.setResponse(response);
							    if(strLotTrackFl.equals("Y")){
							    gmJasperReport.setJasperReportName("cyclecount/GmCycleCountPartPrintByLot.jasper");
							    }else{
							    gmJasperReport.setJasperReportName("cyclecount/GmCycleCountPartPrint.jasper");
							    }
							    gmJasperReport.setHmReportParameters(hmHeaderDtls);
							    gmJasperReport.setReportDataList(alResult);
							    gmJasperReport.setBlDisplayImage(true);
							    strHtmlJasperRpt = gmJasperReport.getHtmlReport();
							  }
							%>
					<%=strHtmlJasperRpt%>
				</div></td>
		</tr>

	</table>

</BODY>
</HTML>