<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 <!-- \accounts\cyclecount\GmCycleCountFullDataRpt.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtCycleCountFullReport" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="gridData" name="frmCycleCountReport" property="xmlString" type="java.lang.String"></bean:define>
<fmtCycleCountFullReport:setBundle basename="properties.labels.accounts.cyclecount.GmCycleCountFullDataRpt" />
<%
String strAccountsCycleJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_CYCLECOUNT");
String strWikiTitle = GmCommonClass.getWikiTitle("CYCLE_COUNT_FULL_DATA_RPT");
String strApplDateFmt = strGCompDateFmt;
%>
<html>
<head>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strAccountsCycleJsPath%>/GmCycleCountFullDataRpt.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">

<script>
var objGridData;
objGridData = '<%=gridData%>';
var date_format = '<%=strApplDateFmt%>';
</script>
</head>

<body leftmargin="20" topmargin="10" onload="fnOnPageLoad()"
	onkeyup="fnEnter();">
	
	<html:form action="/gmCycleCountReport.do?method=loadFullDataRpt">
		<html:hidden property="strOpt" value="" />
		<table border="0" class="DtTable1200" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="3" height="25" class="RightDashBoardHeader"><fmtCycleCountFullReport:message
						key="LBL_CYCLE_CNT_RPT" /></td>
				<td height="25" class="RightDashBoardHeader"><fmtCycleCountFullReport:message
						key="IMG_HELP" var="varHelp" /><img align="right" id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}'
					width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>

			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>

			<tr>
				
				<td class="RightTableCaption" align="right" height="30">&nbsp;<fmtCycleCountFullReport:message
						key="LBL_WAREHOUSE" />:
				</td>
				<td>&nbsp;<gmjsp:dropdown controlName="warehouse"
						SFFormName="frmCycleCountReport" SFSeletedValue="warehouse"
						SFValue="alWarehouse" codeId="ID" codeName="WAREHOUSE"
						defaultValue="[Choose One]" tabIndex="1" /></td>
							<td class="RightTableCaption" align="right" height="30"><fmtCycleCountFullReport:message
						key="LBL_PART" />:</td>
				<td>&nbsp;<html:text property="part" size="57"  styleClass="InputArea"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="2"/>	            				
			&nbsp;&nbsp;<html:select property ="pnumSuffix"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="3">
							<html:option value="" ><fmtCycleCountFullReport:message key="LBL_CHOOSEONE"/></html:option>
							<html:option value="LIT" ><fmtCycleCountFullReport:message key="LBL_LITERAL"/></html:option>
							<html:option value="LIKEPRE" ><fmtCycleCountFullReport:message key="LBL_LIKE_PREFIX"/></html:option>
							<html:option value="LIKESUF" ><fmtCycleCountFullReport:message key="LBL_LIKE_SUFFIX"/></html:option>
						</html:select></td>
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>

			<tr class="Shade">
				<td class="RightTableCaption" align="right" height="30">&nbsp;<fmtCycleCountFullReport:message
						key="LBL_FROM_DATE" />:
				</td>
				<td>&nbsp;<gmjsp:calendar SFFormName="frmCycleCountReport"
						controlName="fromDate" gmClass="InputArea"
						onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" tabIndex="4" /></td>

				<td class="RightTableCaption" align="right" height="30">
					<fmtCycleCountFullReport:message key="LBL_TO_DATE" />:
				</td>
				<td>&nbsp;<gmjsp:calendar SFFormName="frmCycleCountReport"
						controlName="toDate" gmClass="InputArea"
						onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" tabIndex="5" /></td>
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr>
					<td class="RightTableCaption" align="right" height="30">&nbsp;<fmtCycleCountFullReport:message
						key="LBL_CYCLE_CNT_ID" />:
				</td>
				<td>&nbsp;<html:text size="20" maxlength="20"
						name="frmCycleCountReport" property="cycleCountId"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" tabindex="6" /></td>
			
				<td align="center" colspan="4" height="30"><gmjsp:button
						name="Btn_Load" value="Load" gmClass="button"
						onClick="javascript:fnLoad();" buttonType="Load" tabindex="7" /></td>
			</tr>
			<%
			  if (gridData.indexOf("cell") != -1) {
			%>

			<tr>
				<td colspan="4">
					<div id="dataGridDiv" class="" height="500px"></div>
				</td>
			</tr>

			<tr>
				<td colspan="4" align="center">
					<div class='exportlinks'>
						<fmtCycleCountFullReport:message key="LBL_EXPORT_OPTIONS" />
						: <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
							onclick="fnDownloadXLS();"><fmtCycleCountFullReport:message
								key="LBL_EXCEL" /> </a>
					</div>
				</td>
			</tr>
			<%
			  } else {
			%>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr height="30">
				<td colspan="4" align="center"><fmtCycleCountFullReport:message
						key="LBL_NO_DATA_AVAILABLE" /></td>
			</tr>
			<%
			  }
			%>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</body>
</html> 