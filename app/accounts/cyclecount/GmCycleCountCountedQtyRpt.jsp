<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.globus.common.beans.GmGridFormat"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtCycleCountedQtyRpt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtCycleCountedQtyRpt:setBundle basename="properties.labels.accounts.cyclecount.GmCycleCountCountedQtyRpt" />
<bean:define id="hmCrossTabDtls" name="frmCycleCountReport" property="hmCrossTabDtls" type="java.util.HashMap"></bean:define>
<%
  String strAccountsCycleJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_CYCLECOUNT");
  String strWikiTitle = GmCommonClass.getWikiTitle("CYCLE_COUNT_QUANTITY_SUMMARY");
  String strApplDateFmt = strGCompDateFmt;
 String strHiddenFrmDate = GmCommonClass.parseNull((String)request.getAttribute("hidden_FromDate"));
 String strHiddenToDate = GmCommonClass.parseNull((String)request.getAttribute("hidden_ToDate"));
  
  // Grid - Cross tab changes 
  GmGridFormat gmGridFormat = new GmGridFormat();
  String strGridData = "";
	String strDivHeight = "400";
	String strDivWidth = "1050";
	
	boolean showGrid=false;
	if(hmCrossTabDtls!=null && hmCrossTabDtls.size()>0){
		int headerSize = 0;
		int partDesSize = 120;
		if(hmCrossTabDtls.get("Header")!=null && (GmCommonClass.parseNullArrayList((ArrayList)hmCrossTabDtls.get("Header"))).size()>0){
			showGrid = true;
			headerSize = (GmCommonClass.parseNullArrayList((ArrayList)hmCrossTabDtls.get("Header"))).size();
			// based on dynamic header to set the values
			if(headerSize == 3){
			  partDesSize = 530;
			}else if(headerSize == 4){
			  partDesSize = 470;
			}else if(headerSize == 5){
			  partDesSize = 410;
			} else if(headerSize == 6){
			  partDesSize = 350;
			}else if(headerSize == 7){
			  partDesSize = 290;
			}else if(headerSize == 8){
			  partDesSize = 230;
			}else if(headerSize == 9){
			  partDesSize = 170;
			}
		}
		
		gmGridFormat.setDecorator("com.globus.crosstab.beans.GmCycleCountCountedRptGridDecorator");
		
		gmGridFormat.setColumnWidth("Name",120);
		gmGridFormat.setColumnWidth("Description",partDesSize);
		
		gmGridFormat.addColumnSortType("Name","str");
		gmGridFormat.addColumnSortType("Description","str");
		gmGridFormat.addColumnAlign("Name","left");
		gmGridFormat.addColumnAlign("Description","left");
		
		// to add the new column
		gmGridFormat.setAddColumn("Description",1); // Adding the column to 2nd postion.
		gmGridFormat.setAddColumn("refyear", 2);
		gmGridFormat.setAddColumn("refmonthtype", 3);
		//gmGridFormat.setHideColumn("Total");
		gmGridFormat.addSortType("Name","String");
		// rename the column
		gmGridFormat.reNameColumn("Description", "Warehouse");
		gmGridFormat.reNameColumn("refyear", "Rank");
		gmGridFormat.reNameColumn("refmonthtype", "Part Description");
		gmGridFormat.displayZeros(true);
		gmGridFormat.setSortRequired(true);
		
		gmGridFormat.setColumnWidth("refyear",180);
		gmGridFormat.setColumnWidth("refmonthtype",80);
		gmGridFormat.addColumnAlign("refyear","left");
		//
		gmGridFormat.addColumnSortType("refyear","str");
		gmGridFormat.addColumnSortType("refmonthtype","str");
		
		strGridData = gmGridFormat.generateGridXML(hmCrossTabDtls);
	}
	
  
%>
<html>
<!-- accounts\cyclecount\GmCycleCountCountedQtyRpt.jsp -->
<head>
<title>Cycle Coount Quantity Summary</title>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strAccountsCycleJsPath%>/GmCycleCountCountedQtyRpt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>

<script>
    var objGridData;
    
    var date_format = '<%=strApplDateFmt%>';
    var hFromDate = '<%=strHiddenFrmDate%>';
    var hToDate = '<%=strHiddenToDate%>';
  
</script>
</head>
<body leftmargin="20" topmargin="10" onkeyup="fnEnter();" onload="fnPageLoad();">
  <html:form action="/gmCycleCountReport.do?method=loadCountedRpt">
  <html:hidden property="strOpt" value="" />
   <table border="0" class="DtTable1055" cellspacing="0" cellpadding="0">
     <tr>
	   <td colspan="3" height="25" class="RightDashBoardHeader"><fmtCycleCountedQtyRpt:message key="LBL_CYCLE_COUNT_QTY_SUMMARY" /></td>
				<td height="25" class="RightDashBoardHeader"><fmtCycleCountedQtyRpt:message key="IMG_HELP" var="varHelp" /><img align="right" id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}'
					width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
	</tr>
			
	<tr><td class="LLine" colspan="4" height="1"></td></tr>
	<tr>
	 <td class="RightTableCaption" align="right" height="30">&nbsp;<fmtCycleCountedQtyRpt:message key="LBL_WAREHOUSE" />:</td>
	<td>&nbsp;<gmjsp:dropdown controlName="warehouse" SFFormName="frmCycleCountReport" SFSeletedValue="warehouse" SFValue="alWarehouse" codeId="ID" codeName="WAREHOUSE"
		defaultValue="[Choose One]" tabIndex="1" />
	</td>
	<td class="RightTableCaption" align="right" height="30"><fmtCycleCountedQtyRpt:message key="LBL_PART" />:</td>
	<td>&nbsp;<html:text size="20" maxlength="20" name="frmCycleCountReport" property="partNo" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="2" />
	<html:select property ="pnumSuffix"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" >
							<html:option value="0" ><fmtCycleCountedQtyRpt:message key="LBL_CHOOSEONE"/></html:option>
							<html:option value="LIT" ><fmtCycleCountedQtyRpt:message key="LBL_LITERAL"/></html:option>
							<html:option value="LIKEPRE" ><fmtCycleCountedQtyRpt:message key="LBL_LIKE_PREFIX"/></html:option>
							<html:option value="LIKESUF" ><fmtCycleCountedQtyRpt:message key="LBL_LIKE_SUFFIX"/></html:option>
						</html:select>
	</td>
	</tr>
    <tr><td class="LLine" colspan="4" height="1"></td></tr>
    <tr class="Shade">
	<td class=RightTableCaption align="right"><fmtCycleCountedQtyRpt:message key="LBL_FROM_DATE"/>:</td>
	<td class="RightTableCaption">&nbsp;<html:text styleId="fromDate" property="fromDate" size="10" maxlength="6" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="3"/>
	<td class=RightTableCaption align="right"><fmtCycleCountedQtyRpt:message key="LBL_TO_DATE"/>:</td>
	<td>&nbsp;<html:text styleId="toDate" property="toDate" size="10" maxlength="6" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="4"/>
	&nbsp;&nbsp;<img align="center" src="<%=strImagePath%>/nav_calendar.gif"  style="cursor:hand" onclick="javascript:showHideCalendar(hFromDate,hToDate)"  height="14">
	<div id="dhtmlxDblCalendar" style="position:absolute"></div>
	</td>	
	
	</tr>
	<tr><td class="LLine" colspan="4" height="1"></td></tr>
	<tr>
	<td class="RightTableCaption" align="right" height="30"><fmtCycleCountedQtyRpt:message key="LBL_RANK" />:</td>
	<td>&nbsp;<gmjsp:dropdown controlName="rank" SFFormName="frmCycleCountReport" SFSeletedValue="rank" SFValue="alRank" codeId="RANK" codeName="RANK"
		defaultValue="[Choose One]" tabIndex="5" />
	</td>
	<td align="center" colspan="2" height="30">
	     <gmjsp:button name="Btn_Load" value="Load" gmClass="button" onClick="fnLoad();" buttonType="Load" tabindex="6" /> 
	 </td>
	</tr>
	 <%
			  if (strGridData.indexOf("cell") != -1) {
			%>
     <tr>
	   <td colspan="4">
				<div id="dataGridDiv" style="width:<%=strDivWidth%>px;height:<%=strDivHeight%>px"></div>			
		    </td>	
	</tr>
	<script>
				<% 
				long startTime = System.currentTimeMillis();
			
				%>
				  objGridData= '<%=strGridData%>';
				  if(objGridData.length>0){ 
				  
					  fnLoadGridData();
				}
				</script>
			
	<tr>
	   <td colspan="4" align="center">
		<div class='exportlinks'>
			<fmtCycleCountedQtyRpt:message key="LBL_EXPORT_OPTIONS" />:<img src='img/ico_file_excel.png' />&nbsp;
			 <a href="#" onclick="fnDownloadXLS();"><fmtCycleCountedQtyRpt:message key="LBL_EXCEL" /> </a>
		</div>
		</td>
	</tr>
	<%
			  } else {
			%>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr height="30">
				<td colspan="4" align="center"><fmtCycleCountedQtyRpt:message
						key="LBL_NO_DATA_AVAILABLE" /></td>
			</tr>
			<%
			  }
			%>
   </table>
  </html:form>
  <%@ include file="/common/GmFooter.inc"%>
</body>
</html>