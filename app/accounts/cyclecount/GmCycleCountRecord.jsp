<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtCycleCountRecord"	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtCycleCountRecord:setBundle basename="properties.labels.accounts.cyclecount.GmCycleCountRecord" />
<bean:define id="gridData" name="frmCycleCountRecord" property="xmlString" type="java.lang.String"></bean:define>
<bean:define id="buttonAccessFl" name="frmCycleCountRecord" property="buttonAccessFl" type="java.lang.String"></bean:define>
<bean:define id="strRuleValue" name="frmCycleCountRecord" property="strRuleValue" type="java.lang.String"></bean:define>

 <!-- \accounts\cyclecount\GmCycleCountRecord.jsp -->
<html>
<head>
<%
String strAccountsCycleJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_CYCLECOUNT");
String strWikiTitle = GmCommonClass.getWikiTitle("CYCLE_COUNT_RECORD");
String strCCMicroAppUrl =  GmCommonClass.getString("CCMICROAPPURL");
String strButtonDisablefl = buttonAccessFl.equals("Y")? "": "true";
%>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strAccountsCycleJsPath%>/GmCycleCountRecord.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>



<script>
var objGridData;
objGridData = '<%=gridData%>';
var ccMicroAppURL = '<%=strCCMicroAppUrl%>';
</script>

</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad()">
	<html:form action="/gmCycleCountRecord.do?method=loadRecordInfo">
		<html:hidden property="strOpt" value="" />
		<html:hidden property="inputString" value="" />
		<html:hidden property="lockid" />
		<html:hidden property="totalCount" />
		<html:hidden property="warehouseType" />
		<html:hidden property="statusId" />
		<html:hidden property="recordCompleteFl" />
		<html:hidden property="countType" />
		<html:hidden property="lockType" />
		<html:hidden property="strRuleValue" />
		<html:hidden property="strLotTrackFl" />

		<table border="0" class="DtTable1055" width="1055" cellspacing="0"
			cellpadding="0">
			<div>
			<tr>

				<td height="25" class="RightDashBoardHeader" colspan="3"><fmtCycleCountRecord:message
						key="LBL_CYCLE_CNT_RECORD" /></td>
				<td height="25" class="RightDashBoardHeader"><fmtCycleCountRecord:message
						key="IMG_HELP" var="varHelp" /><img align="right" id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}'
					width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>

			</tr>
			<tr>
				<td height="25" class="RightTableCaption" align="right"><fmtCycleCountRecord:message
						key="LBL_CYCLE_CNT_ID" />:</td>
				<td>&nbsp;<bean:write property="cycleCountId"
						name="frmCycleCountRecord" /></td>
				<td height="25" class="RightTableCaption" align="right"><fmtCycleCountRecord:message
						key="LBL_WAREHOUSE" />:</td>
				<td>&nbsp;<bean:write property="warehouseName"
						name="frmCycleCountRecord" /></td>
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr class="Shade">
				<td height="25" class="RightTableCaption" align="right"><fmtCycleCountRecord:message
						key="LBL_LOCK_DATE" />:</td>
				<td>&nbsp;<bean:write property="lockDate"
						name="frmCycleCountRecord" /></td>
				<td height="25" class="RightTableCaption" align="right"><fmtCycleCountRecord:message
						key="LBL_SCHEDULE_DATE" />:</td>
				<td>&nbsp;<bean:write property="scheduleDate"
						name="frmCycleCountRecord" /></td>
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr>
				<td height="25" class="RightTableCaption" align="right"><fmtCycleCountRecord:message
						key="LBL_TOTAL_CNT" />:</td>
				<td>&nbsp;<bean:write property="totalCount"
						name="frmCycleCountRecord" /></td>
				<td height="25" class="RightTableCaption" align="right"><fmtCycleCountRecord:message
						key="LBL_COUNTED_QTY" />:</td>
				<td>&nbsp;<bean:write property="countedQty"
						name="frmCycleCountRecord" /></td>
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr class="Shade">
				<td height="25" class="RightTableCaption" align="right"><fmtCycleCountRecord:message
						key="LBL_STATUS" />:</td>
				<td colspan="3">&nbsp;<bean:write property="status"
						name="frmCycleCountRecord" /></td>
			</tr>
			</div>
			<%
			  if (gridData.indexOf("cell") != -1) {
			%>
			<tr>
				<td colspan="4">
					<div id="dataGridDiv" class="" height="400px"></div>
				</td>
			</tr>
			<tr height="30">
				<td colspan="4" align="center">
					<div class='exportlinks'>
						<fmtCycleCountRecord:message key="LBL_EXPORT_OPTIONS" />
						: <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
							onclick="fnDownloadXLS();"><fmtCycleCountRecord:message
								key="LBL_EXCEL" /> </a>
					</div>
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr>

				<td colspan="4" align="center" height="30">
				<logic:notEqual value="106816" name="frmCycleCountRecord" property="countType"><gmjsp:button
						name="Btn_Print" value="Print" gmClass="button"
						onClick="javascript:fnCycleCountPrint();" buttonType="Load" />&nbsp;&nbsp;&nbsp;</logic:notEqual>
					<logic:equal value="106829" property="statusId"
						name="frmCycleCountRecord">
						<gmjsp:button name="Btn_Save" value="Save" gmClass="button"
							onClick="javascript:fnSave();" buttonType="Save" disabled="true" />&nbsp;&nbsp;&nbsp;<gmjsp:button
							name="Btn_Submit" value="Submit" gmClass="button"
							onClick="javascript:fnSubmit();" buttonType="Save"
							disabled="true" />&nbsp;&nbsp;&nbsp;
								<!-- 	Add new Parts button only show  FG Bulk and RW bulk face. -->
							<%if(strRuleValue.equals("Y")){ %>
							<gmjsp:button
							name="Btn_Submit" value="Add New Parts" gmClass="button"
							onClick="javascript:fnAddNew();" buttonType="Save" disabled="true"/>
							  <%} %>
					</logic:equal> <logic:notEqual value="106829" property="statusId"
						name="frmCycleCountRecord">
						<gmjsp:button name="Btn_Save" value="Save" gmClass="button"
							onClick="javascript:fnSave();" buttonType="Save" disabled="<%=strButtonDisablefl %>" />&nbsp;&nbsp;&nbsp;<gmjsp:button
							name="Btn_Submit" value="Submit" gmClass="button"
							onClick="javascript:fnSubmit();" buttonType="Save" disabled="<%=strButtonDisablefl %>" />&nbsp;&nbsp;&nbsp;
							<!-- 	Add new Parts button only show  FG Bulk and RW bulk face. -->
							<%if(strRuleValue.equals("Y")){ %>
							<gmjsp:button
							name="Btn_Submit" value="Add New Parts" gmClass="button"
							onClick="javascript:fnAddNew();" buttonType="Save" disabled="<%=strButtonDisablefl %>" />
							
					     <%} %>
					</logic:notEqual></td>
			</tr>
			<%
			  } else {
			%>

			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr height="30">
				<td colspan="4" align="center"><fmtCycleCountRecord:message
						key="LBL_NO_DATA_AVAILABLE" /></td>
			</tr>
			<%
			  }
			%>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</body>

</html>