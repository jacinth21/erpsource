<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 <!-- \accounts\cyclecount\GmCycleCountAccuracyRpt.jsp -->
 
 <%@ page import="com.globus.common.beans.GmGridFormat" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtCycleCountAccuracyRpt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="gridData" name="frmCycleCountReport" property="xmlString" type="java.lang.String"></bean:define>
<bean:define id="hmCrossTabDtls" name="frmCycleCountReport" property="hmCrossTabDtls" type="java.util.HashMap"></bean:define>

<fmtCycleCountAccuracyRpt:setBundle basename="properties.labels.accounts.cyclecount.GmCycleCountAccuracyRpt"/>
<%
  String strAccountsCycleJsPath =
      GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_CYCLECOUNT");
  String strWikiTitle = GmCommonClass.getWikiTitle("CYCLE_COUNT_ACCURACY_RPT");
  String strApplDateFmt = strGCompDateFmt;

  String strGridData = "";
  int warehouseSize = 160;
  int headerSize = 0;
  headerSize =
      (GmCommonClass.parseNullArrayList((ArrayList) hmCrossTabDtls.get("Header"))).size();
  if (headerSize != 0) {
    //based on dynamic header to set the values
    if (headerSize == 3) {
      warehouseSize = 880;
    } else if (headerSize == 4) {
      warehouseSize = 820;
    } else if (headerSize == 5) {
      warehouseSize = 760;
    } else if (headerSize == 6) {
      warehouseSize = 700;
    } else if (headerSize == 7) {
      warehouseSize = 640;
    } else if (headerSize == 8) {
      warehouseSize = 580;
    } else if (headerSize == 9) {
      warehouseSize = 520;
    } else if (headerSize == 10) {
      warehouseSize = 460;
    } else if (headerSize == 11) {
      warehouseSize = 400;
    } else if (headerSize == 12) {
      warehouseSize = 340;
    } else if (headerSize == 13) {
      warehouseSize = 280;
    } else if (headerSize == 14) {
      warehouseSize = 220;
    }
    //Cross Tab Implementation
    GmGridFormat gmGridFormat = new GmGridFormat();
    gmGridFormat.setDecorator("com.globus.crosstab.beans.GmCycleCountAccuracyRptGridDecorator");
    gmGridFormat.setColumnWidth("Name", warehouseSize);
    gmGridFormat.addColumnAlign("Name", "left");
    gmGridFormat.reNameColumn("Name", "Warehouse");
    gmGridFormat.displayZeros(true);
    gmGridFormat.setSortRequired(true);
    gmGridFormat.setHideColumn("Total");
    gmGridFormat.addColumnSortType("Name","str");
    gmGridFormat.addSortType("Name", "String");
    strGridData = gmGridFormat.generateGridXML(hmCrossTabDtls);

  }
%>
<html>
<head>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_fast.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strAccountsCycleJsPath%>/GmCycleCountAccuracyRpt.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">

<script>
var objGridData;
var date_format = '<%=strApplDateFmt%>';
var hFromDate = '<%=request.getAttribute("hidden_FromDate")%>';
var hToDate = '<%=request.getAttribute("hidden_ToDate")%>';
</script>
</head>

<body leftmargin="20" topmargin="10" onload="fnOnPageLoad()"
	onkeyup="fnEnter();">
	
	<html:form action="/gmCycleCountReport.do?method=loadAccuracyRpt">
		<html:hidden property="strOpt" value="" />
		<table border="0" class="DtTable950" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="3" height="25" class="RightDashBoardHeader"><fmtCycleCountAccuracyRpt:message
						key="LBL_CYCLE_CNT_ACC_RPT" /></td>
				<td height="25" class="RightDashBoardHeader"><fmtCycleCountAccuracyRpt:message
						key="IMG_HELP" var="varHelp" /><img align="right" id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}'
					width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>

			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>

			<tr>
				
				<td class="RightTableCaption" align="right" height="30">&nbsp;<fmtCycleCountAccuracyRpt:message
						key="LBL_WAREHOUSE" />:
				</td>
				<td>&nbsp;<gmjsp:dropdown controlName="warehouse"
						SFFormName="frmCycleCountReport" SFSeletedValue="warehouse"
						SFValue="alWarehouse" codeId="ID" codeName="WAREHOUSE"
						defaultValue="[Choose One]" tabIndex="2" /></td>
							<td class="RightTableCaption" align="right" height="30"><font color="red">*</font>&nbsp;<fmtCycleCountAccuracyRpt:message
						key="LBL_VARIANCE_BY" />:</td>
				<td>&nbsp;<gmjsp:dropdown controlName="varianceBy"
						SFFormName="frmCycleCountReport" SFSeletedValue="varianceBy"
						SFValue="alVarianceBy" codeId="CODEID" codeName="CODENM"
						tabIndex="2" /></td>
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>

			<tr class="Shade">
			
			<td class="RightTableCaption" align="right" height="30">
			<font color="red">*</font> &nbsp;<fmtCycleCountAccuracyRpt:message key="LBL_FROM_DATE" />:</td>
								<td class="RightTableCaption">&nbsp;<html:text styleId="fromDate" 
										   property="fromDate" 
										   size="10" 
										   maxlength="6" 
										   onfocus="changeBgColor(this,'#AACCE8');" 
										   styleClass="InputArea" 
										   onblur="changeBgColor(this,'#ffffff');" 
										   tabindex="8"/>
										   &nbsp;
									<td class="RightTableCaption" align="right" height="30">
									<font color="red">*</font>&nbsp;<fmtCycleCountAccuracyRpt:message key="LBL_TO_DATE" />:</td>
									<td>&nbsp;<b><html:text styleId="toDate" 
											   property="toDate" 
											   size="10" 
											   maxlength="6" 
											   onfocus="changeBgColor(this,'#AACCE8');" 
											   styleClass="InputArea" 
											   onblur="changeBgColor(this,'#ffffff');" 
											   tabindex="9"/></b>
									&nbsp;
									<img align="center" src="<%=strImagePath%>/nav_calendar.gif"  style="cursor:hand" onclick="javascript:showHideCalendar(hFromDate,hToDate)"  height="14">&nbsp;&nbsp;&nbsp;<gmjsp:button
						name="Btn_Load" value="Load" gmClass="button"
						onClick="javascript:fnLoad();" buttonType="Load" tabindex="6" /><div id="dhtmlxDblCalendar" style="position:absolute"></div>
							</td>
				</tr>
		
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
		 	<%
			  if (strGridData.indexOf("cell") != -1) {
			%>

			<tr>
				<td colspan="4">
					<div id="dataGridDiv" class="" height="400px"></div>
				</td>
			</tr>
			<script>
			
			<% 
			long startTime = System.currentTimeMillis();
			log.debug("Grid Start Time"+startTime); 
			%>
			  objGridData= '<%=strGridData%>';
			  if(objGridData.length>0){ 
			  
				  fnOnPageLoad();
			}
			</script>
				<%
				log.debug("End Time"+System.currentTimeMillis() +"  Time Taken : "+(System.currentTimeMillis()-startTime));
				 %>	
			<tr>
				<td colspan="4" align="center">
					<div class='exportlinks'>
						<fmtCycleCountAccuracyRpt:message key="LBL_EXPORT_OPTIONS" />
						: <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
							onclick="fnDownloadXLS();"><fmtCycleCountAccuracyRpt:message
								key="LBL_EXCEL" /> </a>
					</div>
				</td>
			</tr>
			<%
			  } else {
			%>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr height="30">
				<td colspan="4" align="center"><fmtCycleCountAccuracyRpt:message
						key="LBL_NO_DATA_AVAILABLE" /></td>
			</tr>
			<%
			  }
			%>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</body>
</html>