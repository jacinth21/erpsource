<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

 <!-- \accounts\cyclecount\GmCycleCountDashBoard.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtCycleCountReport" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="gridData" name="frmCycleCountDashBoard" property="xmlString" type="java.lang.String"></bean:define>
<fmtCycleCountReport:setBundle basename="properties.labels.accounts.cyclecount.GmCycleCountDashBoard" />
<%
String strAccountsCycleJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_CYCLECOUNT");
  String strWikiTitle = GmCommonClass
					.getWikiTitle("CYCLE_COUNT_DASHBOARD");
 String strApplDateFmt = strGCompDateFmt;
%>
<html>
<head>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strAccountsCycleJsPath%>/GmCycleCountDashBoard.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">

<script>
var objGridData;
objGridData = '<%=gridData%>';
var date_format = '<%=strApplDateFmt%>';
</script>
</head>

<body leftmargin="20" topmargin="10" onload="fnOnPageLoad()"
	onkeyup="fnEnter();">
	
	<html:form action="/gmCycleCountDashboard.do?method=loadDashboard">
		<html:hidden property="strOpt" value="" />
		<table border="0" class="DtTable1055" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="3" height="25" class="RightDashBoardHeader"><fmtCycleCountReport:message
						key="LBL_CYCLE_CNT_DBOARD" /></td>
				<td height="25" class="RightDashBoardHeader"><fmtCycleCountReport:message
						key="IMG_HELP" var="varHelp" /><img align="right" id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}'
					width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>

			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>

			<tr>
				<td class="RightTableCaption" align="right" height="30">&nbsp;<fmtCycleCountReport:message
						key="LBL_CYCLE_CNT_ID" />:
				</td>
				<td>&nbsp;<html:text size="20" maxlength="20"
						name="frmCycleCountDashBoard" property="cycleCountId"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" tabindex="1" /></td>
				<td class="RightTableCaption" align="right" height="30">&nbsp;<fmtCycleCountReport:message
						key="LBL_WAREHOUSE" />:
				</td>
				<td>&nbsp;<gmjsp:dropdown controlName="warehouse"
						SFFormName="frmCycleCountDashBoard" SFSeletedValue="warehouse"
						SFValue="alWarehoue" codeId="ID" codeName="WAREHOUSE"
						defaultValue="[Choose One]" tabIndex="2" /></td>
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>

			<tr class="Shade">
				<td class="RightTableCaption" align="right" height="30">&nbsp;<fmtCycleCountReport:message
						key="LBL_FROM_DATE" />:
				</td>
				<td>&nbsp;<gmjsp:calendar SFFormName="frmCycleCountDashBoard"
						controlName="fromDate" gmClass="InputArea"
						onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" tabIndex="3" /></td>

				<td class="RightTableCaption" align="right" height="30">
					<fmtCycleCountReport:message key="LBL_TO_DATE" />:
				</td>
				<td>&nbsp;<gmjsp:calendar SFFormName="frmCycleCountDashBoard"
						controlName="toDate" gmClass="InputArea"
						onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" tabIndex="4" /></td>
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr>

				<td class="RightTableCaption" align="right" height="30"><fmtCycleCountReport:message
						key="LBL_STATUS" />:</td>
				<td>&nbsp;<gmjsp:dropdown controlName="status"
						SFFormName="frmCycleCountDashBoard" SFSeletedValue="status"
						SFValue="alStatus" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" tabIndex="5" /></td>
				<td align="center" colspan="4" height="30"><gmjsp:button
						name="Btn_Load" value="Load" gmClass="button"
						onClick="javascript:fnLoad();" buttonType="Load" tabindex="6" /></td>
			</tr>
			<%
			  if (gridData.indexOf("cell") != -1) {
			%>

			<tr>
				<td colspan="4">
					<div id="dataGridDiv" class="" height="400px"></div>
				</td>
			</tr>

			<tr>
				<td colspan="4" align="center">
					<div class='exportlinks'>
						<fmtCycleCountReport:message key="LBL_EXPORT_OPTIONS" />
						: <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
							onclick="fnDownloadXLS();"><fmtCycleCountReport:message
								key="LBL_EXCEL" /> </a>
					</div>
				</td>
			</tr>
			<%
			  } else {
			%>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr height="30">
				<td colspan="4" align="center"><fmtCycleCountReport:message
						key="LBL_NO_DATA_AVAILABLE" /></td>
			</tr>
			<%
			  }
			%>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</body>
</html>