<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtCycleCountLotRpt"	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtCycleCountLotRpt:setBundle basename="properties.labels.accounts.cyclecount.GmCycleCountLotRpt" />

 <!-- \accounts\cyclecount\GmCycleCountLotRpt.jsp -->
<html>
<head>
<%
String strAccountsCycleJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_CYCLECOUNT");
  String strWikiTitle = GmCommonClass.getWikiTitle("CYCLE_COUNT_LOT_RPT");
%>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strAccountsCycleJsPath%>/GmCycleCountLotRpt.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">

<style type="text/css" media="all">
@import url("styles/screen.css"); 
</style>

</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad()">
	<html:form action="/gmCycleCountLotRpt.do?method=loadScannedLots">
			<html:hidden property="hcycleCountId" />
			<html:hidden property="hpart" />
			<html:hidden property="hlocationId" />
			<html:hidden property="hconsignmentId" />
			<html:hidden property="hstrPopupFlag" />
		<table border="0" class="DtTable1055" width="1055" cellspacing="0"
			cellpadding="0">
			<tr>

				<td height="25" class="RightDashBoardHeader" colspan="5"><fmtCycleCountLotRpt:message key="LBL_CYCLE_CNT_LOT_RPT" /></td>
				<td height="25" class="RightDashBoardHeader"><fmtCycleCountLotRpt:message key="IMG_HELP" var="varHelp" /><img align="right" id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}'
					width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>

			</tr>
			<tr>
				<td class="LLine" colspan="5" height="1"></td>
			</tr>

			<tr>
				
				<td class="RightTableCaption" align="right" height="30">&nbsp;<fmtCycleCountLotRpt:message key="LBL_CYCLE_CNT_ID" />: </td>
				<td>&nbsp;<html:text property="cycleCountId" size="20" maxlength="20" 
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" tabindex="6" /></td>
						
							<td class="RightTableCaption" align="right" height="30"><fmtCycleCountLotRpt:message key="LBL_CYCLE_CNT_PART" />:</td>
				<td>&nbsp;<html:text property="part"  size="20"  styleClass="InputArea"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="2"/>	            				
			&nbsp;&nbsp;<html:select property ="pnumSuffix"   onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="3">
							<html:option value="" ><fmtCycleCountLotRpt:message key="LBL_CHOOSEONE"/></html:option>
							<html:option value="LIT" ><fmtCycleCountLotRpt:message key="LBL_LITERAL"/></html:option>
							<html:option value="LIKEPRE" ><fmtCycleCountLotRpt:message key="LBL_LIKE_PREFIX"/></html:option>
							<html:option value="LIKESUF" ><fmtCycleCountLotRpt:message key="LBL_LIKE_SUFFIX"/></html:option>
						</html:select> </td>
									
						<td align="center" height="30"><gmjsp:button
						name ="Btn_Load" value="Load" gmClass="button"
						onClick="javascript:fnLoadRpt();" buttonType="Load" tabindex="7" /></td>
			</tr>
			
			<tr>
					<td class="RightTableCaption" align="right" height="30">&nbsp;<fmtCycleCountLotRpt:message key="LBL_CYCLE_CNT_LOCATION_NM" />: </td>
				<td>&nbsp;<html:text property="locationId" size="20" maxlength="20" 
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" tabindex="6" /></td>
			
					<td class="RightTableCaption" align="right" height="30">&nbsp;<fmtCycleCountLotRpt:message key="LBL_CYCLE_CNT_CONSIGN_ID" />:
				</td>
				<td>&nbsp;<html:text property="consignmentId" size="20" maxlength="20" 
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" tabindex="6" /></td>
			
			</tr>
		   <tr class="oddshade"> 
				<td colspan="6"><div id="divScannedLotData" height="300px"></div></td>
		  </tr>

			<tr>
				<td colspan="6" align="center">
					<div id="DivExportExcel" class='exportlinks'>
						<fmtCycleCountLotRpt:message key="LBL_EXPORT_OPTIONS" />
						: <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
							onclick="fnExcel();"><fmtCycleCountLotRpt:message key="LBL_EXCEL" /> </a>
					</div>
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="6" height="1"></td>
			</tr>
			<tr><td colspan="4" align="center"  class="RegularText" height="20"><div  id="DivNothingMessage"><fmtCycleCountLotRpt:message key="NO_DATA_FOUND"/></div></td></tr>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</body>

</html>