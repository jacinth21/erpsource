<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 <!-- \accounts\cyclecount\GmCycleCountApproval.jsp -->
<%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtCycleCountApproval" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<fmtCycleCountApproval:setBundle basename="properties.labels.accounts.cyclecount.GmCycleCountApproval" />
<bean:define id="gridData" name="frmCycleCountApproval"	property="xmlString" type="java.lang.String" />
<bean:define id="statusId" name="frmCycleCountApproval" property="statusId" type="java.lang.String" />
<bean:define id="transLogDate" name="frmCycleCountApproval"	property="transLogDate" type="java.lang.String" />
<bean:define id="alTxnRef" name="frmCycleCountApproval"	property="alTxnRef" type="java.util.ArrayList"/>
<bean:define id="buttonAccessFl" name="frmCycleCountApproval" property="buttonAccessFl" type="java.lang.String"></bean:define>
<html>
<head>
<%
String strAccountsCycleJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_CYCLECOUNT");
  String strWikiTitle = GmCommonClass.getWikiTitle("CYCLE_COUNT_APPROVAL");
String strApplDateFmt = strGCompDateFmt;
GmCalenderOperations.setTimeZone(strGCompTimeZone);
String strCurDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt);

String strButtonDisablefl = buttonAccessFl.equals("Y")? "": "true";

%>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strAccountsCycleJsPath%>/GmCycleCountApproval.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_fast.js"></script>
<script type="text/javascript" src="<%=strJsPath%>/GmTransactionLog.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script>
var objGridData;
objGridData = '<%=gridData%>';
var todayDt = '<%=strCurDate%>';
var transLogDt = '<%=transLogDate%>';
</script>

</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad()">
	<html:form action="/gmCycleCountApproval.do?method=loadReviewDtls">
		<html:hidden property="strOpt" value="" />
		<html:hidden property="warehouseId" />
		<html:hidden property="invMapId" />
		<html:hidden property="inputString" />
		<html:hidden property="lockid" />
		<html:hidden property="countType" />

		<table border="0" class="DtTable1200" width="1200" cellspacing="0"
			cellpadding="0">
			<div>
				<tr>

					<td height="25" class="RightDashBoardHeader" colspan="3"><fmtCycleCountApproval:message
						key="LBL_CYCLE_CNT_APPROVAL" /></td>
					<td height="25" class="RightDashBoardHeader"><fmtCycleCountApproval:message
							key="IMG_HELP" var="varHelp" /><img align="right" id='imgEdit'
						style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}'
						width='16' height='16'
						onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>

				</tr>

				<tr>
					<td height="25" class="RightTableCaption" align="right"><fmtCycleCountApproval:message
						key="LBL_CYCLE_CNT_ID" />:</td>
					<td>&nbsp;<bean:write property="cycleCountId"
							name="frmCycleCountApproval" /></td>
					<td height="25" class="RightTableCaption" align="right"><fmtCycleCountApproval:message
						key="LBL_WAREHOUSE" />:</td>
					<td>&nbsp;<bean:write property="warehouseName"
							name="frmCycleCountApproval" /></td>
				</tr>
				<tr>
					<td class="LLine" colspan="4" height="1"></td>
				</tr>
				<tr class="Shade">
					<td height="25" class="RightTableCaption" align="right"><fmtCycleCountApproval:message
						key="LBL_LOCK_DATE" />:</td>
					<td>&nbsp;<bean:write property="lockDate"
							name="frmCycleCountApproval" /></td>
					<td height="25" class="RightTableCaption" align="right"><fmtCycleCountApproval:message
						key="LBL_SCHEDULE_DATE" />:</td>
					<td>&nbsp;<bean:write property="scheduleDate"
							name="frmCycleCountApproval" /></td>
				</tr>
				<tr>
					<td class="LLine" colspan="4" height="1"></td>
				</tr>
				<tr>
					<td height="25" class="RightTableCaption" align="right"><fmtCycleCountApproval:message
						key="LBL_TOTAL_CNT" />:</td>
					<td>&nbsp;<bean:write property="totalCount"
							name="frmCycleCountApproval" /></td>
					<td height="25" class="RightTableCaption" align="right"><fmtCycleCountApproval:message
						key="LBL_COUNTED_QTY" />:</td>
					<td>&nbsp;<bean:write property="countedQty"
							name="frmCycleCountApproval" /></td>
				</tr>
				<tr>
					<td class="LLine" colspan="4" height="1"></td>
				</tr>
				
				
				<tr class="Shade">
						<td height="25" class="RightTableCaption" align="right"><fmtCycleCountApproval:message
						key="LBL_APPROVED_BY" />:</td>
					<td>&nbsp;<bean:write property="approvedBy"
							name="frmCycleCountApproval" /></td>
							<td height="25" class="RightTableCaption" align="right"><fmtCycleCountApproval:message
						key="LBL_APPROVED_DATE" />:</td>
					<td>&nbsp;<bean:write property="approvedDate"
							name="frmCycleCountApproval" /></td> 
				</tr>
					<tr>
					<td class="LLine" colspan="4" height="1"></td>
				</tr>
				<tr class="Shade">
					<td height="25" class="RightTableCaption" align="right"><fmtCycleCountApproval:message
						key="LBL_STATUS" />:</td>
					<td colspan="3">&nbsp;<bean:write property="status"
							name="frmCycleCountApproval" /></td>
				</tr>
			</div>
			<%
			  if (gridData.indexOf("cell") != -1) {
			%>
			<tr>
				<td colspan="4">
					<div id="dataGridDiv" class="" height="400px"></div>
				</td>
			</tr>

			<tr>
				<td colspan="4" align="center">
					<div class='exportlinks'>
						<fmtCycleCountApproval:message key="LBL_EXPORT_OPTIONS" />
						: <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
							onclick="fnDownloadXLS();"><fmtCycleCountApproval:message
								key="LBL_EXCEL" /> </a>
					</div>
				</td>
			</tr>
			<tr>

				<td class="RightTableCaption" align="right" height="30" colspan="2"><fmtCycleCountApproval:message
								key="LBL_CHOOSE_ACTION" />:</td>
				<td>&nbsp;<gmjsp:dropdown controlName="chooseAction"
						SFFormName="frmCycleCountApproval" SFSeletedValue="chooseAction"
						SFValue="alChooseAction" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" />&nbsp;&nbsp;
						
						<logic:equal value="106829"
						property="statusId" name="frmCycleCountApproval">
						<gmjsp:button name="Btn_Submit" value="Submit" gmClass="button"
							onClick="javascript:fnCycleCntSubmit();" buttonType="Submit"  disabled="<%=strButtonDisablefl %>"/>
					</logic:equal> <logic:notEqual value="106829" property="statusId"
						name="frmCycleCountApproval">
						<gmjsp:button name="Btn_Submit" value="Submit" gmClass="button"
							onClick="javascript:fnCycleCntSubmit();" buttonType="Submit"
							disabled="true" />
					</logic:notEqual>
				</td>
				<td colspan="1" align="left"></td>
			</tr>
			<%
			  } else {
			%>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr height="30">
				<td colspan="4" align="center"><fmtCycleCountApproval:message
						key="LBL_NO_DATA_AVAILABLE" /></td>
			</tr>
			<%
			  }
			%>

			<%
			  if (alTxnRef.size() != 0) {
			%>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr>
				<td colspan="4">
					<table>
						<tr class="ShadeRightTableCaption">
							<td height="25" class="RightTableCaption"><fmtCycleCountApproval:message key="LBL_LOCK_ID" /></td>
							<td height="25" class="RightTableCaption"><fmtCycleCountApproval:message key="LBL_TXN_REF_ID" /></td>
							<td height="25" class="RightTableCaption"><fmtCycleCountApproval:message key="LBL_TXN_CREATED_BY" /></td>
							<td height="25" class="RightTableCaption"><fmtCycleCountApproval:message key="LBL_TXN_CREATED_DATE" /></td>
						</tr>
						<%
						  for (int i = 0; i < alTxnRef.size(); i++) {
						        HashMap hmTemp = new HashMap();
						        hmTemp = (HashMap) alTxnRef.get(i);
						%>
						<Tr>
							<td><%=GmCommonClass.parseNull((String) hmTemp.get("LOCK_ID"))%></td>
							<td><a href=javascript:fnTxnDetail('<%=GmCommonClass.parseNull((String) hmTemp.get("TXN_ID"))%>')><%=GmCommonClass.parseNull((String) hmTemp.get("TXN_ID"))%></a></td>
							<td><%=GmCommonClass.parseNull((String) hmTemp.get("UPDATED_BY"))%></td>
							<td><%=GmCommonClass.parseNull((String) hmTemp.get("UPDATED_DATE"))%></td>
						</Tr>


						<%
						  }
						%>

					</table> <%} %>
				
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</body>

</html>