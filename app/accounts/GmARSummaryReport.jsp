
<%
/**********************************************************************************
 * File		 		: GmPartNumInvReport.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Date" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.PopChartUtils"%>
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="fmtArSummaryReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmARSummaryReport.jsp -->
<fmtArSummaryReport:setLocale value="<%=strLocale%>"/>
<fmtArSummaryReport:setBundle basename="properties.labels.accounts.GmARSummaryReport"/>
<fmtArSummaryReport:message key="LBL_CHOOSE_ONE" var="varChooseOne"/>
<%


	String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");	
	HashMap hm = GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strWikiTitle = GmCommonClass.getWikiTitle("REQUEST_MODIFY_VIEW");
	
	String strCurrSign = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_SYMB"));
	String strCurrId = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_ID"));
	
	String strCollectorId = GmCommonClass.parseNull((String)request.getAttribute("COLLECTORID"));
	String strAccountCurrency = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_ID"));
	String strShowAccGrp = GmCommonClass.parseNull(GmCommonClass.getString("SHOW_ACCOUNT_GRP"));
	ArrayList alReturn = (ArrayList)request.getAttribute("alReturn");
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	ArrayList alRegion = new ArrayList();
	ArrayList alDistributor = new ArrayList();
	ArrayList alCollector = (ArrayList)request.getAttribute("ALCOLLECTOR");	
	ArrayList alCategory = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCATEGORY"));	
	ArrayList alGroup = new ArrayList();
	ArrayList alCompDiv = (ArrayList)request.getAttribute("ALCOMPDIV");
	ArrayList alSnapShot = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("AL_SNAPSHOT_DATE"));
	String strhScreenType = GmCommonClass.parseNull((String)request.getAttribute("HSCREENTYPE")); 
	String strInvId = "";
	String strAccId = "";

	/*String strNAME = "";
	String strID = "";
	String strCURRENTVALUE = "";
	String strONE_THIRTY = "";
	String strTHIRTYONE_SIXTY = "";
	String strSIXTYONE_NINETY = "";
	String strGREATER_NINETY = ""; */

	HashMap hmLoop = new HashMap();
	int intLoop = 0;
	
	if (alReturn != null)
	{
		intLoop = alReturn.size();
	}
	String gridData = (String) request.getAttribute("alResult")==null?"":(String) request.getAttribute("alResult");
	Date dtPaySince = (Date) request.getAttribute("PAYSINCEDT");
  	
	int intDistSize = alDistributor.size();
	ArrayList alCompCurrency = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALCOMPANYCURRENCY"));
	alGroup = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ACCOUNT_GRP_LIST"));
	String strAccGrpId = GmCommonClass.parseNull((String)request.getAttribute("GROUPID"));
	String strReportType = GmCommonClass.parseNull((String)request.getAttribute("REPORTTYPE"));
	String strIntervalDays = GmCommonClass.parseNull((String)request.getAttribute("INTERVALDAYS"));
	String strThroughDays = GmCommonClass.parseNull((String)request.getAttribute("THROUGHDAYS"));
	String strDefaultThrough = GmCommonClass.parseNull((String)request.getAttribute("DAFAULTTHROUGH"));
	String strParentFl = GmCommonClass.parseNull((String)request.getAttribute("PARENTCHKFL"));
    String strInvSource = GmCommonClass.parseNull((String)request.getAttribute("INVSOURCE"));
    String strARRptByDealerFlag = GmCommonClass.parseNull((String)request.getAttribute("ARREPORTBYDEALER"));
    String strDivisionId 	= GmCommonClass. parseZero ((String)request.getAttribute("DIVISIONID"));
    String strCategoryId 	= GmCommonClass.parseZero((String)request.getAttribute("CATEGORYID"));
    String strSnapShot 	= GmCommonClass. parseZero ((String)request.getAttribute("AGING_LOCK_ID"));
    String strCustCategoryId 	= GmCommonClass. parseNull ((String)request.getAttribute("CUSTCATEGORY"));
	int ColumnSize = (Integer)request.getAttribute("COLUMNSIZE");
	
    GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.accounts.GmARSummaryReport", strSessCompanyLocale);
    String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
    GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    //PMT-14375 - Including ALL option for GM Japan to show 
	String strRptByDealerFl = "";
	String strIncludeAllFl = "";
	strRptByDealerFl = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("AR_REPORT_BY_DEALER"));
	if(strRptByDealerFl.equals("YES")){
	  strIncludeAllFl = "Y";
	}
	String strHeader = "";
	if (strReportType.equals("ARByInvoiceDt"))
	{
		strHeader =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_AR_SUMMARY_INVOICE_DATE"));
	}else{
		strHeader =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_AR_SUMMARY_DUE_DATE"));
	}
%>


<HTML>
<HEAD>

<TITLE> Globus Medical: Consignment Report</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmARSummaryReport.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<script>
var parentFl = '<%=strParentFl%>';
var strCustCategoryId = '<%=strCustCategoryId%>';
var objGridData;
objGridData = '<%=gridData%>';
var columnSize = '<%=ColumnSize%>';
var currSymbol = '<%=strCurrSign%>';
var reporttype = '<%=strInvSource%>';
var arCurrID = '<%=strCurrId%>';
var arRptByDealerFlag = '<%=strARRptByDealerFlag%>';
</script>

</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoad();">
<FORM name="frmARSummary"  method="POST" action="<%=strServletPath%>/GmARSummaryServlet">
<input type="hidden" name="hInvId" value="<%=strInvId%>">
<input type="hidden" name="hAccId" value="<%=strAccId%>">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="reportType" value="<%=strReportType%>">
	

	<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
		<tr>			
			<td height="25" class="RightDashBoardHeader" colspan="5">
				<%=strHeader%>
			</td>	
			<!-- <td class="RightDashBoardHeader"></td> -->
			<fmtArSummaryReport:message key="LBL_HELP" var="varHelp"/>
	       <td  height="25" class="RightDashBoardHeader" align="right"><img id='imgEdit' 
	       style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
	       </td>
		</tr>
		<tr><td class="Line" height="1" colspan="6"></td></tr>
		<!-- Addition Filter Information -->
		<tr><td colspan="6" height="25">
				<jsp:include page="/sales/GmSalesFilters.jsp" >
				<jsp:param name="FRMNAME" value="frmARSummary" />
				<jsp:param name="HIDE" value="SYSTEM" />
				<jsp:param name="HIDEBUTTON" value="HIDEGO" />
				</jsp:include>
		</td></tr>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
		<tr>
			<td colspan="6" align="left" height="25">
			<!-- Added table to fix the alignment -->
			<table border="0" width="100%" cellspacing="0" cellpadding="0"><tr>
			<td width="75%">
				<jsp:include page="/accounts/GmIncludeAccountType.jsp">
					<jsp:param name="DISPACCFL" value="Y"/>
					<jsp:param name="REPACCTLABELFL" value="Y"/>
					<jsp:param name="INC_ALL_FL" value="<%=strIncludeAllFl%>"/>
				</jsp:include>
			</td>
			</tr></table></td>
		</tr>
		<tr><td class="LLine" height="1" colspan="6"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr class="Shade">
		
		<td align="right" class="RightTableCaption" height="25" width="15%" style="white-space: nowrap;padding-left: 5px;"><fmtArSummaryReport:message key="LBL_SHOW_PARENT_ACCOUNT"/>:</td>
			<td height="25" width="12%">&nbsp;<input type="checkbox" size="50" checked="<%=strParentFl%>" name="Chk_ParentFl" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');">
			</td>
			<%if(strShowAccGrp.equals("YES")){%>	
			<td class="RightTableCaption" align="right" width="12%"><fmtArSummaryReport:message key="LBL_GROUP"/>:</td>
			<td width="20%">&nbsp;<gmjsp:dropdown controlName="Cbo_AccGrp"  seletedValue="<%=strAccGrpId%>" value="<%= alGroup%>" codeId = "ID"  codeName = "NAME"  defaultValue= "${varChooseOne}" /></td>
			<%} %>
					<td class="RightTableCaption" align="right" width="25%" ><fmtArSummaryReport:message key="LBL_DIVISION_NAME"/>:
					<gmjsp:dropdown width="100" controlName="Cbo_Division"  seletedValue="<%= strDivisionId %>" value="<%= alCompDiv %>" codeId = "DIVISION_ID" codeName = "DIVISION_NAME" defaultValue= "${varChooseOne}"  /></td>		
		<td></td>
		</tr>
		<tr><td class="LLine" height="1" colspan="6"></td></tr>
		<tr>
		<td align="right" class="RightTableCaption" height="25" width="15%"><fmtArSummaryReport:message key="LBL_NO_PAYMENT_SINCE"/>:</td>
			<td height="25" width="20%">&nbsp;<gmjsp:calendar textControlName="Txt_PaySinceDate" textValue="<%=dtPaySince==null?null:new java.sql.Date(dtPaySince.getTime())%>"  
		 			gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			</td>
			<td class="RightTableCaption" align="right" width="12%"><fmtArSummaryReport:message key="LBL_COLLECTOR_ID"/>:</td>
			<td width="20%">&nbsp;<gmjsp:dropdown controlName="Cbo_CollectorId"  seletedValue="<%=strCollectorId%>" 	
					value="<%= alCollector%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "${varChooseOne}" /></td>
					<td class="RightTableCaption" align="right" width="30%"><fmtArSummaryReport:message key="LBL_REPORT_DATE"/>:
					<gmjsp:dropdown width="100" controlName="Cbo_SnapShot"  seletedValue="<%= strSnapShot %>" value="<%= alSnapShot %>" codeId = "ID" codeName = "LOCK_DATE" defaultValue= "Current Date"  /></td>	
				<td></td>	
			</tr>
		<tr><td class="LLine" height="1" colspan="6"></td></tr>
		<tr class="Shade">
		<td align="right" class="RightTableCaption" height="25" width="15%"><fmtArSummaryReport:message key="LBL_INTERVEL"/>:</td>
			<td height="25" width="12%">&nbsp;<input type="text" size="2" value="<%=strIntervalDays%>" name="Txt_Interval" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
			</td>
			<td class="RightTableCaption" align="right" width="12%"><fmtArSummaryReport:message key="LBL_THROUGH"/>:</td>
			<td width="20%">&nbsp;<input type="text" size="2" value="<%=strThroughDays%>" name="Txt_Through" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
			</td>
			 <td class="RightTableCaption" align="right" width="25%"><fmtArSummaryReport:message key="LBL_CATEGORY"/>:
			 <gmjsp:dropdown width="100" controlName="Cbo_Category"  seletedValue="<%= strCategoryId %>" value="<%= alCategory %>" codeId = "CODEID" codeName = "CODENM" defaultValue= "${varChooseOne}"  /></td> 
			
			<fmtArSummaryReport:message key="BTN_LOAD" var="varLoad"/>
			<fmtArSummaryReport:message key="BTN_RESET" var="varReset"/>
			<td align="left" width="50%" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<gmjsp:button  value="${varLoad}" name="Btn_Load" gmClass="Button" style="width: 5em; height: 23px;" onClick="return fnGo();" buttonType="Load" />&nbsp;&nbsp;
				<gmjsp:button value="${varReset}" name="Btn_Reset" gmClass="Button" style="width: 5em; height: 23px;" onClick="fnReset();" buttonType="Save" />
			</td>
		</tr>			
		<%if(!gridData.equals("")){%>
		<tr>
			<td colspan="6">
				<div  id ="arsummarydata" style="width:1160px;height:400px"></div>
			</td>
		</tr> 
		<tr>
            <td colspan="6" align="center">
               <div class='exportlinks'><fmtArSummaryReport:message key="LBL_EXPORT_OPTIONS"/> : <img src='img/ico_file_excel.png' onclick="return fnDownloadXLS()"/>&nbsp;<a href="#"
                     onclick="return fnDownloadXLS()"> <fmtArSummaryReport:message key="LBL_EXCEL"/> </a></div>
                <% if(strhScreenType.equals("CrdHld")){%>
               		 <fmtArSummaryReport:message key="BTN_CLOSE" var="varClose"/>
                     <gmjsp:button value="${varClose}"  name="Btn_Close"  gmClass="Button" buttonType="Load"  onClick="window.close()" />
                     <%} %>
			</td>

		</tr>
		<%} %>
</table>
 	
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
