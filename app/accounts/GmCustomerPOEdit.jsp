 <%
/**********************************************************************************
 * File		 		: GmLogin.jsp
 * Desc		 		: This screen is used for the Login Process
 * Version	 		: 1.0 
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector" %>
<%@ taglib prefix="fmtCustomerPOEdit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmCustomerPOEdit.jsp -->
<fmtCustomerPOEdit:setLocale value="<%=strLocale%>"/>
<fmtCustomerPOEdit:setBundle basename="properties.labels.accounts.GmCustomerPOEdit"/>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Globus Medical: Login</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script>
function fnSubmitPO()
{
	Error_Clear();
	obj1 = document.frmPOChange.Txt_CustomerPO;
	obj2 = document.frmPOChange.Txt_Reason;

	var obj1_Val = TRIM(obj1.value);
	var obj2_Val = TRIM(obj2.value)
	document.frmPOChange.Txt_CustomerPO.value = obj1_Val;
	//validate the space - Customer PO
	fnValidatePOSpace (obj1_Val);

	var custPoLen = obj1_Val.length;

	if(custPoLen > 20){
		Error_Details(message_accounts[270]);
	}
	
	if (obj1_Val == '')
	{
		Error_Details(message[60]);
	}
	if (obj2_Val == '')
	{
		Error_Details(message[59]);
	}

	if (ErrorCount > 0)
	{
			Error_Show();
			return false;
	}
	
	self.opener.document.frmInvoice.hCustomerPO.value = obj1_Val;
	self.opener.document.frmInvoice.hReasonForChange.value = obj2_Val;
	self.opener.fnSubmitPO();
	window.close();
}

function fnClose()
{
	window.close();
}

//validate the space - Customer PO
function fnValidatePOSpace(CustPO) {
	var regexp = /\s/g;
	if (regexp.test(CustPO)) {
		Error_Details(message_accounts[271]);
	}
}
</script>
</head>

<body leftmargin="0" topmargin="5" onLoad=document.frmPOChange.Txt_CustomerPO.focus();>
<form name="frmPOChange" method="post" action="<%=strServletPath%>/GmLogonServlet" onSubmit="javascript:return fnSubmitLogon();">
<table border="0" width="625" cellspacing="0" cellpadding="0" bgcolor="#CCCCCC" align="center" >
  <tr>
    <td bgcolor="white" height="200" align="center" valign="top">
		<table border="0" width="625" cellspacing="0" cellpadding="0">
			<tr>
				<td rowspan="7" bgcolor="#666666" width="1"></td>
				<td colspan="2" bgcolor="#666666" height="1"></td>
				<td rowspan="7" bgcolor="#666666" width="1"></td>
			</tr>
			<tr>
				<td colspan="4" class="RightDashBoardHeader" height="20"><fmtCustomerPOEdit:message key="LBL_CUSTOMER_PO_UPDATE"/></td>
			</tr>
			<tr height="25" >
				<td class="RightTableCaption" align="right">&nbsp;<font color="red">*&nbsp;</font><fmtCustomerPOEdit:message key="LBL_CUSTOMER_PO"/>: </td>
				<td>&nbsp;<input tabindex=1 type="text" size="15" value="" name="Txt_CustomerPO" 
				onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" 
				class="InputArea">
			</tr>
			<tr height="25" class="oddshade">
				<td class="RightTableCaption" align="right"><font color="red">*&nbsp;</font><fmtCustomerPOEdit:message key="LBL_REASON_FOR_CHANGE"/>:</td>
				<td>&nbsp;<textarea name="Txt_Reason" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
				onBlur="changeBgColor(this,'#ffffff');" tabindex=2 rows=5 cols=80
				value=""></textarea></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td  height="30" align="center">
				<fmtCustomerPOEdit:message key="BTN_SUBMIT" var="varSubmit"/>
				<fmtCustomerPOEdit:message key="BTN_CLOSE" var="varClose"/>
					<gmjsp:button tabindex="3"  value="${varSubmit}" gmClass="button" onClick="fnSubmitPO();" buttonType="Save"  />&nbsp;
					<gmjsp:button tabindex="4"  value="${varClose}" gmClass="button" onClick="fnClose();" buttonType="Load" />&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="4" height="1" colspan="4" bgcolor="#666666"></td>
			</tr>
		</table>
	</td>
  </tr>

</table>
</form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>

