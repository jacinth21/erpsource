<%
/**********************************************************************************
 * File		 		: GmInvoiceDHRDtls.jsp
 * Desc		 		: Invoice - DHR Detail Screen
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>

<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ page import="java.text.DecimalFormat" %>
<%@ taglib prefix="fmtEditAuditTag" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmEditAuditTag.jsp -->
<fmtEditAuditTag:setLocale value="<%=strLocale%>"/>
<fmtEditAuditTag:setBundle basename="properties.labels.accounts.GmEditAuditTag"/>
<bean:define id="alLocDetails" name="frmAuditVerify" property="alLocDetails" type="java.util.ArrayList"></bean:define>
<bean:define id="auditEntryID" name="frmAuditVerify" property="auditEntryID" type="java.lang.String"></bean:define>
<bean:define id="tagID" name="frmAuditVerify" property="tagID" type="java.lang.String"></bean:define>
<bean:define id="distID" name="frmAuditVerify" property="distID" type="java.lang.String"></bean:define>
 
<%
HashMap hmNames = new HashMap();
hmNames =	(HashMap)request.getAttribute("NAMES");
	ArrayList alDistributor = new ArrayList();
	ArrayList alRepList = new ArrayList();
	ArrayList alAccList = new ArrayList();
	
	HashMap hcboVal = null;
	
	if (hmNames != null)
	{
		alDistributor = (ArrayList)hmNames.get("DISTLIST");
		alRepList = (ArrayList)hmNames.get("REPLIST");
		alAccList = (ArrayList)hmNames.get("ACCLIST");		
	}	 

	String strMessage = GmCommonClass.parseNull((String)request.getAttribute("RESPONSE"));
	String strWikiTitle = GmCommonClass.getWikiTitle("EDIT_UPLOADED_DATA");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Edit Uploaded Data</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/TagReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script>

var DistLen = <%=alDistributor.size()%>;
var RepLen = <%=alRepList.size()%>;
var AccLen = <%=alAccList.size()%>;
var lblPart='<fmtEditAuditTag:message key="LBL_PART"/>';
var lblControl='<fmtEditAuditTag:message key="LBL_CONTROL"/>';
var lblSet='<fmtEditAuditTag:message key="LBL_SET"/>';
var lblLocation='<fmtEditAuditTag:message key="LBL_LOCATION"/>';
var lblLocationDetails='<fmtEditAuditTag:message key="LBL_LOCATION_DETAILS"/>';
var lblCountedDate='<fmtEditAuditTag:message key="LBL_COUNTED_DATE"/>';
<%
	hcboVal = new HashMap();
String strDistId="";
String strDistName="";
	
	for (int i=0;i<alDistributor.size();i++)
	{
		hcboVal = (HashMap)alDistributor.get(i);
		strDistId=GmCommonClass.parseNull((String)hcboVal.get("ID"));
		if(strDistId.equals(distID))
		{
			strDistName=GmCommonClass.parseNull((String)hcboVal.get("NAME"));
		}
%>
	var DistArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>

<%	
	hcboVal = new HashMap();
	for (int i=0;i<alAccList.size();i++)
	{
		hcboVal = (HashMap)alAccList.get(i);
%>
	var AccArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("DID")%>";
<%
	}
%>

<%
	hcboVal = new HashMap();
	for (int i=0;i<alRepList.size();i++)
	{
		hcboVal = (HashMap)alRepList.get(i);
%>
	var RepArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("ID")%>,<%=hcboVal.get("ID")%>";
<%
	}
%>

 function fnSubmit()
 {   
	var vsetId= document.frmAuditVerify.cbo_setNumber.options[document.frmAuditVerify.cbo_setNumber.selectedIndex].value;
	document.frmAuditVerify.setID.value=vsetId;
	var vstatusId= document.frmAuditVerify.cbo_status.options[document.frmAuditVerify.cbo_status.selectedIndex].value;
	document.frmAuditVerify.statusID.value=vstatusId;
	var vborrowedFrom= document.frmAuditVerify.cbo_borrowedfrm.options[document.frmAuditVerify.cbo_borrowedfrm.selectedIndex].value;
	document.frmAuditVerify.borrowedFrom.value=vborrowedFrom;		
	var vlocationID= document.frmAuditVerify.cbo_location.options[document.frmAuditVerify.cbo_location.selectedIndex].value;
	document.frmAuditVerify.locationID.value=vlocationID;				
	var vlocationDet= document.frmAuditVerify.names.options[document.frmAuditVerify.names.selectedIndex].value;
	document.frmAuditVerify.locationDet.value=vlocationDet;				


	//fnValidateTxtFld('tagID',' Tag ID');
	fnValidateTxtFld('partNumber',lblPart);	
	fnValidateTxtFld('controlNumber',lblControl);	
	fnValidateDropDn('cbo_setNumber',lblSet);
	fnValidateDropDn('cbo_location',lblLocation);
	fnValidateDropDn('names',lblLocationDetails);
	fnValidateTxtFld('countedDate',lblCountedDate);
	DateValidateMMDDYYYY(document.frmAuditVerify.countedDate, message_accounts[266]+message[1]);
	
	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
	}	

	document.frmAuditVerify.strOpt.value='update_audit_tag';
	document.frmAuditVerify.submit();

 }
   
  
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmEditAuditTag.do">
	<html:hidden property="strOpt" value="" /> 
	<html:hidden property="setID" value="" /> 
	<html:hidden property="statusID" value="" /> 
	<html:hidden property="borrowedFrom" value="" />
	<html:hidden property="locationID" value="" />
	<html:hidden property="auditEntryID" value="<%=auditEntryID%>" />
	<html:hidden property="locationDet" value="" />
	<html:hidden property="operatorSign" value=""/>
	<html:hidden property="distID" />
	<html:hidden property="tagID" />
	
	<table border="0" width="680" cellspacing="0" cellpadding="0">	
		<tr>		
			<td bgcolor="#666666" width="1" rowspan="10"></td>		
			<td  height="25" class="RightDashBoardHeader" ><fmtEditAuditTag:message key="LBL_EDIT_UPLOAD_DATA"/> </td>
            <td  height="25" class="RightDashBoardHeader">
            <fmtEditAuditTag:message key="LBL_HELP" var="varHelp"/>
                <img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
            </td>   
            <td bgcolor="#666666" width="1" rowspan="10"></td>  	     
		</tr>
		<tr>
			<td width="680" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<% if (!strMessage.equals("")) { %>
				<!--	<tr>
						<td class="RightTableCaption" align="center" HEIGHT="24" colspan="2"> <font color = "green"> <%=strMessage%> </font></td>
					</tr>-->
				<% } %>
				
					<tr  >
						<td class="RightTableCaption" align="right" HEIGHT="24" >&nbsp;<fmtEditAuditTag:message key="LBL_DISTRIBUTOR"/>:</td>
						<td width="380">&nbsp;<%=strDistName%>
							
						</td>	
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtEditAuditTag:message key="LBL_TAG_ID"/>:</td>
						<td width="320">&nbsp;<%=tagID%></td>
					</tr>
				   
				
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr >
						<td class="RightTableCaption" align="right" HEIGHT="24" >&nbsp;<fmtEditAuditTag:message key="LBL_PART"/><font color="red">*</font>:</td>
						<td width="380">&nbsp;<html:text maxlength="20" property="partNumber"  size="15" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
							
						</td>	
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtEditAuditTag:message key="LBL_CONTROL"/><font color="red">*</font>:</td>
						<td width="380">&nbsp;<html:text property="controlNumber" maxlength="20" size="15" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
							
						</td>	
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr >
						<td class="RightTableCaption" align="right" HEIGHT="24" >&nbsp;<fmtEditAuditTag:message key="LBL_SET"/><font color="red">*</font>:</td>
						<td width="380" >&nbsp;
						<gmjsp:dropdown controlName="cbo_setNumber" width="300"	SFFormName="frmAuditVerify"  SFSeletedValue="setID"
									 SFValue="alSet"   codeId="ID" codeName="IDNAME"  defaultValue= "[Choose One]"   />
						</td>	
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtEditAuditTag:message key="LBL_COMMENTS"/>:</td>
						<td width="380">&nbsp;
						<html:textarea property="comments"  cols="41" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
						</td>	
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24" >&nbsp;<fmtEditAuditTag:message key="LBL_STATUS"/>:</td>
						<td width="380" >&nbsp;
						<gmjsp:dropdown controlName="cbo_status"	SFFormName="frmAuditVerify"  SFSeletedValue="statusID"
									 SFValue="alStatus"   codeId="CODEID" codeName="CODENM"  defaultValue= "[Choose One]"/>
						</td>	
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption"  align="right" HEIGHT="24">&nbsp;<fmtEditAuditTag:message key="LBL_BORROWED_FROM"/>:</td>
						<td width="380">&nbsp;<gmjsp:dropdown width="300" controlName="cbo_borrowedfrm"	SFFormName="frmAuditVerify"  SFSeletedValue="borrFrom"
									 SFValue="alBorrfrm"   codeId="ID" codeName="NAME"  defaultValue= "[Choose One]"   />
							
						</td>	
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr >
						<td class="RightTableCaption" align="right" HEIGHT="24" >&nbsp;<fmtEditAuditTag:message key="LBL_LOCATION"/><font color="red">*</font>:</td>
						<td width="380" >&nbsp;<gmjsp:dropdown controlName="cbo_location"	SFFormName="frmAuditVerify"  SFSeletedValue="locationID"
						onChange="javascript:fnGetNames(this)"
									 SFValue="alLocation"   codeId="CODENMALT" codeName="CODENM"  defaultValue= "[Choose One]"   />
							
						</td>	
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right"  HEIGHT="24">&nbsp;<fmtEditAuditTag:message key="LBL_LOCATION_DETAILS"/><font color="red">*</font>:</td>
						<td width="380">&nbsp;<gmjsp:dropdown width="300" controlName="names"	SFFormName="frmAuditVerify"  SFSeletedValue="names"
									 SFValue="alLocDetails"   codeId="ID" codeName="NAME"  defaultValue= "[Choose One]"   />
							
						</td>	
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24" >&nbsp;<fmtEditAuditTag:message key="LBL_COUNTED_DATE"/><font color="red">*</font>:</td>
						<td width="380" >&nbsp;<html:text property="countedDate" maxlength="10"  size="15" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
						<fmtEditAuditTag:message key="LBL_CLICK_TO_OPEN_CALENDER" var="varOpenCalender"/>
						<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmAuditVerify.countedDate');" 
						title="${varOpenCalender}"  src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />	
						</td>	
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					  <tr class="shade">
						<td>&nbsp;</td>
						<!--<tr><td><font color="red">*</font> Indicates Mandatory Fields</td>-->
						<td  height="30">&nbsp;
						<fmtEditAuditTag:message key="BTN_SUBMIT" var="varSubmit"/>
						<gmjsp:button value="${varSubmit}" gmClass="button" onClick="fnSubmit();" buttonType="Save" />&nbsp;						
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
	 
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>