 <%@page import="java.util.Date"%>
 
 <%
/**********************************************************************************
 * File		 		: GmIssueCredit.jsp
 * Desc		 		: This screen is used to issue Credit for Accounts
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page buffer="16kb" autoFlush="true" %>

<%@ taglib prefix="fmtIssueCredit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmIssueCredit.jsp -->
<fmtIssueCredit:setLocale value="<%=strLocale%>"/>
<fmtIssueCredit:setBundle basename="properties.labels.accounts.GmIssueCredit"/>
<%
try {
	
    String strTimeZone = strGCompTimeZone;
    String strApplDateFmt =strGCompDateFmt;
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strLoadScreen 	= GmCommonClass.parseNull((String)request.getAttribute("hLoadScreen"));
	String strOrderType 	= GmCommonClass.parseNull((String)request.getAttribute("Cbo_OrderType"));
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strMessage =  GmCommonClass.parseNull((String)request.getAttribute("hMessage"));
	
	String strCreditMemoId = GmCommonClass.parseNull((String)request.getAttribute("hCMId"));
	String strAccId = GmCommonClass.parseNull((String)request.getAttribute("hAccId"));
	ArrayList alInvoiceType = new ArrayList();
	ArrayList alAccount = new ArrayList();
	alInvoiceType = (ArrayList)request.getAttribute("INVOICETYPE");
	alAccount = (ArrayList)request.getAttribute("ACCOUNTLIST");
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Issue Credit </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts.js"></script>
<script>
var varCount;
var objPartCell;
function fnSubmit()
{
	
		
	
    document.frmIssueCredit.hAction.value = "Reload";
    fnStartProgress();
	document.frmIssueCredit.submit();
}

function fnPrintInvoice(varinv)
{
windowOpener('/GmInvoiceInfoServlet?hAction=Print&hInv='+varinv,"PrntInv","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=500");
}

function fnPrintPack(val)
{
	windowOpener('<%=strServletPath%>/GmEditOrderServlet?hMode=PrintPack&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
}

function fnPrintCreditMemo(val)
{
	windowOpener('<%=strServletPath%>/GmReportCreditsServlet?hAction=PrintVersion&hOrderId='+val,"Credit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintCashAdjustMemo(val)
{
	windowOpener('<%=strServletPath%>/GmReportCreditsServlet?hAction=PrintCashAdj&hOrderId='+val,"Credit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnSetPartSearch(count)
{
varCount = count;
var objPartCntCell;
var objOrdQty;

objPartCell = eval("document.frmIssueCredit.Txt_PartNum"+count);

}

function fnOpenPart()
	{
	var varAccId = document.frmIssueCredit.Cbo_AccId.value;
	if (objPartCell == null)
	{
		varPartNum = "";
	}
	else 	
		varPartNum = objPartCell.value; 
		windowOpener("/GmSearchServlet?hAction=SearchPart&Txt_PartNum="+varPartNum+"&hCount="+varCount+"&Txt_AccId="+varAccId,"Search","resizable=yes,scrollbars=yes,top=340,left=420,width=600,height=435");
	}
	

function fnGenerateCredit()
{	
 fnGetPayDetails();
}	


function fnGetPayDetails(){
var theinputs=document.getElementsByTagName("input");
	var partnum='';
	var desc = '';
	var amt = 0;
	var date = '';
	var str = '';
	var cnt = 0;
	var totalamt = 0;
	var negamt = -1;
	var objVAT ="";
	var VAT = "";
	var str_vat="";
	var creditAmt = "";
	var partNo = "";
	
for(var n=0;n<=5;n++)
	{
				val = n;
				obj = eval("document.frmIssueCredit.Txt_PartNum"+val);
				partnum = obj.value; 
				objAmt = eval("document.frmIssueCredit.Txt_Amt"+val);
				amt = RemoveComma(objAmt.value);
				negamt = parseFloat(amt) * -1;
				objVAT = eval("document.frmIssueCredit.Txt_VAT"+val);
				VAT = objVAT.value;
				creditAmt = creditAmt+ amt+',';
				partNo = partNo+ partnum +',';
				var creditAmount = RemoveComma(creditAmt);
				var partNumber =  RemoveComma(partNo);
				//VAT% Decimal Validation
				var exp=  /^[1-9]\d*(\.\d+)?$/;
				var objval = exp.test(VAT);
				if(VAT !=""){	
					if(!objval){	
					Error_Details(message_accounts[543]);
					} 
				}
				//Amount- Number Validation
				if (amt < 0) {
					Error_Details(message_accounts[547]);
				} 
				if (isNaN(amt)) {
					Error_Details(message_accounts[544]);
					} 	
				if(amt != 0) {
						str = str + partnum + ',' +'1' + ','+'x' + ',' + negamt + '|' ;				
						totalamt = parseFloat(totalamt) + parseFloat(amt);
						str_vat= str_vat + partnum +',' + negamt +','+VAT + '|' ;	
					}
				
				cnt++;
				
	}
		if(TRIM(partNumber) == ''){
			Error_Details(message_accounts[549]);
		}
		if(TRIM(creditAmount) == ''){
			Error_Details(message_accounts[548]);
		}
	
		if (ErrorCount > 0) {
			Error_Show();
			Error_Clear();
			return false; 
		}
    
	totalamt = totalamt * -1;
	document.frmIssueCredit.hAction.value = "UPDIssueCredit";
	document.frmIssueCredit.hOpt.value = "UpdateCredit";
	document.frmIssueCredit.hInputStr.value =str; 
	document.frmIssueCredit.hVatInputStr.value =str_vat; 
	document.frmIssueCredit.hTotal.value =totalamt;
	fnStartProgress();
	document.frmIssueCredit.submit();
}

function fnPrintInvoice(varinv)
{
windowOpener('/GmInvoiceInfoServlet?hAction=Print&hInv='+varinv,"PrntInv","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=500");
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" >
<FORM name="frmIssueCredit" method="POST" action="<%=strServletPath%>/GmCreditMemoServlet">
<input type="hidden" name="hAccountList" value="">
<input type="hidden" name="hAccountListPlus" value="">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hLoadScreen" value="<%=strLoadScreen %>">
<input type="hidden" name="hCMId" value="<%=strCreditMemoId%>">
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hTotal" value="">
<input type="hidden" name="hVatInputStr" value="">

<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
	<tr><td height="25" class="RightDashBoardHeader" >
	<fmtIssueCredit:message key="LBL_GENERATED_CREDIT_MEMO"/>
	</td></tr>

</table>	
<table class="DtTable850" cellpadding="0" cellspacing="0" border="0">
<!-- Custom tag lib code modified for JBOSS migration changes -->
 <tr>

	<td>	<jsp:include page="/accounts/GmIncludeAccountType.jsp">
		<jsp:param name="DISPACCFL" value="Y"/>  
	</jsp:include>	</td>
	<td align="left" valign="middle">
	<fmtIssueCredit:message key="BTN_LOAD" var="varLoad"/>
		<gmjsp:button value="&nbsp;${varLoad}&nbsp;" name="Btn_Go" gmClass="button" 
		onClick="javascript:fnSubmit();" buttonType="Load" /> </td>
	</tr>
</table>

<table class="DtTable850" cellpadding="0" cellspacing="0" border="0" >
<% if (strAction.equals("UPDIssueCredit")) { %>
		    <tr>
				<td  colspan="2" class="RightTextBlue" align = "center">&nbsp;<%=strMessage%><BR>
			</td>
    		</tr>
		<tr> <td  colspan="3" bgcolor="#666666" height="1"></td>	</tr>
<% } %>
								<tr class="ShadeRightTableCaption">
									<td  HEIGHT="24" colspan="3"><fmtIssueCredit:message key="LBL_ENTER_CREDIT_DETAILS"/>:</td>
								</tr>
								<tr><td colspan="3" class=line></td></tr>
								<tr bgcolor="#eeeeee">
									<td align="center" class="RightText" HEIGHT="20" width="150"><fmtIssueCredit:message key="LBL_PART"/>  <font color="red">*</font></td>
									<td align="center" class="RightText" width="700"><fmtIssueCredit:message key="LBL_CREDIT_AMOUNT"/>  <font color="red">*</font></td>
									<td align="center" class="RightText" width="700"><fmtIssueCredit:message key="LBL_VAT"/>  </td>
								</tr>
								<tr><td colspan="3" class=line></td></tr>
								
								<% for (int intRowCnt = 0; intRowCnt<=5; intRowCnt++) 
								{ 	
								%>
								<tr>
									<td height="24" align="right" class="RightText"  width="150">
									<input type="text" size="10" value="" name="Txt_PartNum<%=intRowCnt%>" class="InputArea" onFocus=fnSetPartSearch(<%=intRowCnt%>); onBlur="changeBgColor(this,'#ffffff');" tabindex=1>									
									</td>
									<td align="center" class="RightText"  width="700">
									<input type="text" size="10" value="" name="Txt_Amt<%=intRowCnt%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
									</td>
									<td align="center" class="RightText"  width="700">
									<input type="text" size="10" value="" name="Txt_VAT<%=intRowCnt%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
									</td>
								</tr>
								<tr><td colspan="3" bgcolor="#cccccc"></td></tr>
								<% } %>
								<tr><td colspan="3" bgcolor="#cccccc"></td></tr>
								<tr>
								
							<tr>
								<td class="alignright" colspan = "1" align="right" HEIGHT="24">&nbsp;&nbsp;<fmtIssueCredit:message key="LBL_DETAILS"/>:</td>
								<td colspan = "2">&nbsp;<textarea name="Txt_Comments" width = 0%; height =0px;
								  class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
								onBlur="changeBgColor(this,'#ffffff');" rows=5 cols=100 value="" tabindex="9"></textarea>
								</td>
							</tr>
								
								<tr><td colspan="3" bgcolor="#cccccc"></td></tr>
								<tr><td colspan="3" align = "center" height="35">
								<fmtIssueCredit:message key="BTN_GENERATE_CREDIT" var="varGenerateCredit"/>
								<fmtIssueCredit:message key="BTN_PART_LOOKUP" var="varPartLookup"/>
								<gmjsp:button value="&nbsp;${varGenerateCredit}&nbsp;" name="Btn_GenCredit" buttonType="Save" gmClass="button" onClick="javascript:fnGenerateCredit();" /> &nbsp;&nbsp;&nbsp;
								<gmjsp:button gmClass="button" name="Btn_PartLookUp" value="&nbsp;${varPartLookup}&nbsp;" accesskey="P" tabindex="-1" onClick="fnOpenPart();" buttonType="Load" buttonTag="True"/>&nbsp;
								</td>
								</tr>
				</table>

</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
