
<%
	/**********************************************************************************
	 * File		 		: GmAuditUpload.jsp
	 * Desc		 		: Upload - Upload page
	 * Version	 		: 1.0
	 * author			: Pramaraj
	 ************************************************************************************/
%>


<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@ page import="org.apache.commons.beanutils.RowSetDynaClass" %>

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtAuditUpload" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmAuditUpload.jsp -->
<fmtAuditUpload:setLocale value="<%=strLocale%>"/>
<fmtAuditUpload:setBundle basename="properties.labels.accounts.GmAuditUpload"/>
<html>
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("AUDIT_UPLOAD");

	HashMap hmParam= (HashMap)request.getAttribute("HMPARAM");
	
	String strAuditList=GmCommonClass.parseNull((String)hmParam.get("AUDITID"));
	String strDistList=GmCommonClass.parseNull((String)hmParam.get("DISTID"));
	String strAuditorId=GmCommonClass.parseNull((String)hmParam.get("AUDITOR"));
	ArrayList alAuditList =GmCommonClass.parseNullArrayList((ArrayList)hmParam.get("ALAUDITLIST"));
	ArrayList alDistList =GmCommonClass.parseNullArrayList((ArrayList)hmParam.get("ALDISTLIST"));
	ArrayList alAuditor =GmCommonClass.parseNullArrayList((ArrayList)hmParam.get("ALAUDITORLIST"));
	
	
	String strFormList = GmCommonClass.parseNull((String) request.getParameter("strFormList"));
	ArrayList alUploadinfo = GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("alUploadinfo"));
	String strRecordupdated = GmCommonClass.parseNull(request.getParameter("recordUpdated"));
	String strAction = GmCommonClass.parseNull(request.getParameter("hAction"));
	request.setAttribute("alUploadinfo", alUploadinfo);
	request.setAttribute("strUploadHome", GmCommonClass.getString("UPLOADHOME"));
	String strUploadHome = GmCommonClass.getString("UPLOADHOME");
%>
<head>
<title>Upload File</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script type="text/javascript">

function fnReload(){

	    document.upform.hAction.value="reload";
	    document.upform.action="/GmAuditUploadServlet?hAction=reload&strAuditList="+document.upform.strAuditList.value+"&strDistList="+document.upform.alDistList.value+"&strAuditorId="+document.upform.Auditor.value;
	     document.upform.strDistList.value=document.upform.alDistList.value;
	     document.upform.Auditor.value=document.upform.Auditor.value;
	   
	    document.upform.submit();    
}
function fnOpenForm(Id)
{
	var uploadDir="GMAUDITORUPLOAD";
    document.upform.sId.value=Id;
	document.upform.action = "/GmCommonFileOpenServlet?sId="+Id+"&uploadString="+uploadDir;
	document.upform.submit();

}

function upload()
  {
 
 Error_Clear();
    if(document.upform.uploadfile.value == "")
    { 
	     // alert("Select a file to upload");   
	      
	      Error_Details(message_accounts[275]);   
    }
    else if(document.upform.strAuditList.value==0||document.upform.alDistList.value==0||document.upform.Auditor.value==0)
    {
    Error_Details(message_accounts[276]); 
    }
    else{
 		document.upform.todo.value="upload";
	    document.upform.hAction.value="upload";
	    document.upform.action.value="/GmAuditUploadServlet?hAction=upload&strAuditList="+document.upform.strAuditList.value+"&strDistList="+document.upform.alDistList.value+"&strAuditorId="+document.upform.Auditor.value;
	    document.upform.submit();    
    }
      	  if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
    toggledisplay();
  
  
  }
</script>
</head>
<BODY leftmargin="20" topmargin="10">
<ul class="style1">
</ul>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<table style="border: thin;" border="1" bordercolor="black">
	<tr >
		<td>
		<form method="post" name="upform" enctype="multipart/form-data" >
				<input type="hidden" name="strUploadHome" value="<%=strUploadHome %>"> 
		<input type="hidden" name="hAction"> <input type="hidden"
			name="strDistList"> <input type="hidden" name="strFormList">
		<input type="hidden" name="sId"> <input type="hidden"
			name="strRecordupdated">
		<table width="700" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="3" class="Line" height="1"></td>
			</tr>
			<tr>
				<td height="25" class="RightDashBoardHeader"><fmtAuditUpload:message key="LBL_AUDIT_UPLOAD"/></td> 
				<fmtAuditUpload:message key="LBL_HELP" var="varHelp"/>
       <td height="25" class="RightDashBoardHeader" align="Right"><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
    </tr>
			</tr>
			<tr>
				<td width="100%" height="30">
				<table>
				<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="shade">
					<td width="30%" colspan="2" class="RightTableCaption" align="left" height="30">&nbsp;<%
						%> &nbsp;<fmtAuditUpload:message key="LBL_AUDIT_NAME"/>: <gmjsp:dropdown controlName="strAuditList"
							seletedValue="<%=strAuditList%>" value="<%=alAuditList%>"
							codeId="ID" codeName="NAME" defaultValue="[Choose One]"
							onChange="fnReload()" /></td>
						<td width="40%" colspan="2" class="RightTableCaption" align="left" height="30">&nbsp;<%
						%> &nbsp;<fmtAuditUpload:message key="LBL_DISTRIBUTOR_NAME"/>: <gmjsp:dropdown  width="150" controlName="alDistList"
							seletedValue="<%=strDistList%>" value="<%=alDistList%>"
							codeId="DISTID" codeName="DISTNAME" defaultValue="[Choose One]"
							onChange="fnReload()"
							/></td>
						<td width="30%" colspan="2" class="RightTableCaption" align="right" height="30">&nbsp;<%
						%> &nbsp;<fmtAuditUpload:message key="LBL_AUDITOR"/>: <gmjsp:dropdown controlName="Auditor"
							seletedValue="<%=strAuditorId%>" value="<%=alAuditor%>"
							codeId="USERID" codeName="UNAME" defaultValue="[Choose One]"
							onChange="fnReload()"/></td>
					</tr>
				</table>
				</td>
			</tr>
			<%
				if ((strAction.equals("reload") || strAction.equals("upload")) && !strDistList.equals("0")) {
			%>
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
			<tr>
				<td width="100%" height="100" valign="top">
				<div><jsp:include page="/common/GmUpload.jsp"/>
				</div>
				</td>
			</tr>
			<tr>
				<td class="TableCaption" colspan="1"><fmtAuditUpload:message key="LBL_PREVIOUS_UPLOAD"/></td>
			</tr>
			<tr>
				<td colspan="1">
			<display:table name="requestScope.results.rows" class="its" export="false" cellpadding="0" cellspacing="0" decorator="com.globus.accounts.displaytag.beans.DTFieldAuditLockReportWrapper">		
			<display:column property="FILE_ID" title="#"  class="aligncenter" style="width:20px"/>
			<fmtAuditUpload:message key="LBL_AUDITOR" var="varAuditor"/><display:column property="AUDITOR" title="${varAuditor}" sortable="true" class="aligncenter" style="width:100px" /> 
			<fmtAuditUpload:message key="LBL_FILE_NAME" var="varFileName"/><display:column property="FILE_NAME" title="${varFileName}" sortable="true" class="aligncenter" style="width:175px" /> 
			<fmtAuditUpload:message key="LBL_LOCKED_BY" var="varLockedBy"/><display:column property="UPLOADED_BY" title="${varLockedBy}" sortable="true" class="alignleft" style="width:100px" /> 
			<fmtAuditUpload:message key="LBL_LOCKED_ON" var="varLockedOn"/><display:column property="CREATED_ON" title="${varLockedOn}" sortable="false" class="alignleft" style="width:120px" /> 			
			</display:table> 
						</td>
						<%
							}
						%>
					</table>

				</table>
				</form>
				</td>
			</tr>
			<tr>
				<td colspan="3" class="Line" height="1"></td>
			</tr>
		</table>
		<%@ include file="/common/GmFooter.inc" %>
</body>
</html>
