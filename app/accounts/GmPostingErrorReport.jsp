<%@ page language="java" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<%@ taglib prefix="fmtPostingErrorReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmPostingErrorReport.jsp -->
<fmtPostingErrorReport:setLocale value="<%=strLocale%>"/>
<fmtPostingErrorReport:setBundle basename="properties.labels.accounts.GmPostingErrorReport"/>
<bean:define id="ldtResult" name="frmFieldSalesRptForm" property="ldtResult" type="java.util.List"></bean:define>
<%
	String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strWikiTitle = GmCommonClass.getWikiTitle("POSTING_ERROR_REPORT");
	int rowsize = ldtResult.size();
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Posting Error Report</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     
    table.DtTable1000 {
	width: 1200px;
	}
</style>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmFieldSalesRpt.js"></script>
<script>
function fnCallEditTransfer(val)
{
	document.frmFieldSalesRptForm.hTransferId.value = val;
	document.frmFieldSalesRptForm.hAction.value="Load";
	document.frmFieldSalesRptForm.action="GmAcceptTransferServlet";
	document.frmFieldSalesRptForm.submit();
}

function fnViewReturns(val)
{
	windowOpener('<%=strServletPath%>/GmPrintCreditServlet?hAction=VIEW&hRAID='+val,"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=660,height=400");
}
	
</script>
</HEAD>

<BODY onkeyup="fnEnter();" leftmargin="20" topmargin="10" >
 
<html:form  action="/gmPostingErrorReport.do?"  >
<html:hidden property="strOpt"/>
<input type="hidden" name="hAction" >
<input type="hidden" name="hTransferFrom" >
<input type="hidden" name="hTransferId" >


	<table border="0" class="DtTable900" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" height="25" class="RightDashBoardHeader"><fmtPostingErrorReport:message key="LBL_POSTING_REPORT"/></td>
			<td  height="25" class="RightDashBoardHeader">
				<fmtPostingErrorReport:message key="LBL_HELP" var="varHelp"/>
				<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr  class="shade" >		
			<td height="30" align ="Right" class="RightTableCaption"><font color="red">*</font><fmtPostingErrorReport:message key="LBL_AUDIT_LIST"/>:&nbsp;</td>
		    <td width="200"><gmjsp:dropdown controlName="auditId" SFFormName="frmFieldSalesRptForm" SFSeletedValue="auditId" 
								SFValue="alAuditName" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" /></td>
			<td height="30" align ="Right" class="RightTableCaption"></td>
			<td  align ="Right" class="RightTableCaption"> <font color="red">*</font><fmtPostingErrorReport:message key="LBL_DISTRIBUTOR"/>:&nbsp;<gmjsp:dropdown controlName="distId" SFFormName="frmFieldSalesRptForm" SFSeletedValue="distId" 
								SFValue="aldist" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]"  />&nbsp;&nbsp;</td>
		</tr>
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		<tr> 
		  </td>
	  		<td colspan="4"  align ="center">
	  			<fmtPostingErrorReport:message key="BTN_LOAD" var="varLoad"/>
		     	<gmjsp:button value="${varLoad}" gmClass="button"  onClick="fnSubmit();" buttonType="Load" />
		    </td>
	  	</tr>      
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		 <tr>
			<td colspan="4"  height="10" >
				<display:table name="requestScope.frmFieldSalesRptForm.ldtResult" requestURI="/gmPostingErrorReport.do" decorator="com.globus.accounts.displaytag.beans.DTPostingErrorReportWrapper" class="its" export= "true" id="currentRowObject"   freezeHeader="true" cellpadding="0" cellspacing="0" >
			 		<fmtPostingErrorReport:message key="LBL_DISTRIBUTOR_NAME" var="varDistributorName"/><display:column property="DIST_NAME" title="${varDistributorName}"  class="alignleft" sortable="true"   />
			 		<fmtPostingErrorReport:message key="LBL_SET_ID" var="varSetId"/><display:column property="SETID" title="${varSetId}"   class="alignleft" sortable="true"  style="width:50"/>
					<fmtPostingErrorReport:message key="LBL_SET_DESCRIPTION" var="varSetDescription"/><display:column property="SETDESC" title="${varSetDescription}"   class="alignleft"  sortable="true" style="width:200" maxLength="28" />
					<fmtPostingErrorReport:message key="LBL_PART" var="VarPart"/><display:column property="PNUM" title="${VarPart}#"  class="alignleft" sortable="true" style="width:50"/> 
					<fmtPostingErrorReport:message key="LBL_PART_DESC" var="VarPartDesc"/><display:column property="PDESC" title="${VarPartDesc}"   class="alignleft"  sortable="true" style="width:150" maxLength="20" />									
					<fmtPostingErrorReport:message key="LBL_REF_ID" var="varRegId"/><display:column property="TRANSFERID" title="${varRegId}"   sortable="true"   titleKey="Ref Id"  class="alignleft"/>					
					<fmtPostingErrorReport:message key="LBL_FLAG_TYPE" var="varFlagType"/><display:column property="REFTYPE" title="${varFlagType}"   sortable="true"   titleKey="Flag Type"  class="alignleft"/>
					<fmtPostingErrorReport:message key="LBL_REASON" var="varReason"/><display:column property="REASON" title="${varReason}"   sortable="true"    class="alignleft"/>
				</display:table>
				
						
			</td>
		</tr>
				
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>