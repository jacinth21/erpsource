 <%
/**********************************************************************************
 * File		 		: GmPaymentList.jsp
 * Desc		 		: This screen is used to Payment List for the selected customer
 * Version	 		: 1.0
 * author			: Richard
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ taglib prefix="fmtPaymentList" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmPaymentList.jsp -->
<fmtPaymentList:setLocale value="<%=strLocale%>"/>
<fmtPaymentList:setBundle basename="properties.labels.accounts.GmPaymentList"/>
<fmtPaymentList:message key="LBL_CHOOSE_ONE" var="varChooseOne"/>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strApplDateFmt = strGCompDateFmt;
	String strRptFmt = "{0,date,"+strApplDateFmt+"}";
	
	HashMap hmCurrency = new HashMap();
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strApplCurrFmt = (String)hmCurrency.get("CMPCURRFMT");
	String strCurrSign = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_SYMB"));
	String strCurrFmt = "{0,number,"+strApplCurrFmt+"}";
	String strWikiTitle = GmCommonClass.getWikiTitle("PAYMENT_LIST");
	String strLoadScreen 	= GmCommonClass.parseNull((String)request.getAttribute("hLoadScreen"));
	String strOrderType 	= GmCommonClass.parseNull((String)request.getAttribute("Cbo_OrderType"));
	String strPayType 	= GmCommonClass.parseNull((String)request.getAttribute("Txt_PayType"));
	String strPayMode = GmCommonClass.parseNull((String)request.getAttribute("Txt_PayMode"));
	String strAccId = GmCommonClass.parseNull((String)request.getAttribute("AccId"));
	String strParentFl = GmCommonClass.parseNull((String)request.getAttribute("PARFLAG"));
	ArrayList alPayType = ((ArrayList) request.getAttribute("PAYTYPE"));
	ArrayList alPayMode = ((ArrayList) request.getAttribute("PAYMODE"));
	HashMap hcboVal;
	String strCodeID = "";
	String strSelected = "";
	
	String strInvSource = GmCommonClass.parseNull((String)request.getAttribute("INVSOURCE"));
	String strIdAccount = GmCommonClass.parseNull((String)request.getAttribute("IDACCOUNT"));
	String strMode = GmCommonClass.parseNull((String)request.getAttribute("hMode"));
	ArrayList alType = (ArrayList)request.getAttribute("ALTYPE");
	//strAccId = (String)request.getAttribute("hAccId");
	String strCollectorId = GmCommonClass.parseNull((String)request.getAttribute("COLLECTORID"));
	ArrayList alCollector = (ArrayList)request.getAttribute("ALCOLLECTOR");
	String strCreditType = GmCommonClass.parseNull((String)request.getAttribute("CREDITTYPE"));
	ArrayList alCreditHold = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCREDITTYPE"));
	String strAccountCurrency = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_ID"));
	ArrayList alCompCurrency = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCOMPANYCURRENCY"));
	String strDivisionId 	= GmCommonClass. parseZero ((String)request.getAttribute("DIVISIONID"));
	ArrayList alCompDiv = (ArrayList)request.getAttribute("ALCOMPDIV");	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Sales Orders Report </TITLE>
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />

<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script type="text/javascript">
var parAccfl = '<%=strParentFl%>';
</script>
<script>

function fnSubmit()
{
	fnValidateTxtFld('Txt_FromDate',message_accounts[257]);
	fnValidateTxtFld('Txt_ToDate',message_accounts[258]);
	var parentFl  = document.frmAccount.Chk_ParentFl.checked;
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	fnStartProgress("Y"); // When click Go button, it will show Progress Bar.
    document.frmAccount.hAction.value = "Reload";
	document.frmAccount.submit();

}

function fnPrintInvoice(varinv)
{
windowOpener('/GmInvoiceInfoServlet?hAction=Print&hInv='+varinv,"PrntInv","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=500");
}

function fnPrintPack(val)
{
	windowOpener('<%=strServletPath%>/GmEditOrderServlet?hMode=PrintPack&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
}

function fnPrintCreditMemo(val)
{
	windowOpener('<%=strServletPath%>/GmReportCreditsServlet?hAction=PrintVersion&hOrderId='+val,"Credit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintCashAdjustMemo(val)
{
	windowOpener('<%=strServletPath%>/GmReportCreditsServlet?hAction=PrintCashAdj&hOrderId='+val,"Credit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}


function fnLoad1()
{
	//we implement the auto complete functionality - so, not required to call below function.
	fnDispatchLoad();
	
	if(parAccfl == 'false'){
		document.frmAccount.Chk_ParentFl.checked = false;
	}
	
} 
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnLoad1();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmPaymentListServlet">
<input type="hidden" name="hAccountList" value="">
<input type="hidden" name="hAccountListPlus" value=""> <input type="hidden" name="hAction" value=""> <input type="hidden" name="hLoadScreen" value="<%=strLoadScreen %>">
<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
	
	<tr>
	<td  colspan="5" height="25" class="RightDashBoardHeader"><fmtPaymentList:message key="LBL_PAYMENT_LIST"/></td>
	<td align="right" class=RightDashBoardHeader > 	
	<fmtPaymentList:message key="LBL_HELP" var="VarHelp"/>
		<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${VarHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
	</td>
	</tr>
<tr><td colspan="6">	
<table cellpadding="0" cellspacing="0" border="0">
	<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
	<tr>
		<td colspan="6" align="center" height="25">
			<jsp:include page="/accounts/GmIncludeAccountType.jsp" >
				<jsp:param name="DISPACCFL" value="Y"/>
				<jsp:param name="REPACCTLABELFL" value="Y"/> 
			</jsp:include>
		</td>
	</tr>
	<tr><td colspan="6" class=Lline></td></tr>
	<tr class="Shade">
		<td colspan="4" height="25">
		<jsp:include page="../include/GmFromToDateInc.jsp" flush="true" >
		<jsp:param name="CSSClass" value="RightTablecaption" />	
		</jsp:include>
		</td>
		<td colspan="1" class="RightTablecaption" align="Right"><fmtPaymentList:message key="LBL_SHOW_PRENT_ACCOUNT"/>:</td>
					<td colspan="1">&nbsp;<input type="checkbox" size="30"  checked="<%=strParentFl %>" name="Chk_ParentFl" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" >					
						</td>
	</tr>
	<tr><td colspan="6" class=Lline></td></tr>
	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<tr>
	<fmtPaymentList:message key="LBL_PAYENT_TYPE" var="VarPaymentType"/>
		<td class="RightTablecaption" align="right" height="25" width="18%"><gmjsp:label type="RegularText"  SFLblControlName="${VarPaymentType}:" td="false"/></td>
		<td width="19%">&nbsp;<gmjsp:dropdown controlName="Cbo_PayType"  seletedValue="<%= strPayType %>" 	
				width="150" value="<%= alPayType%>" codeId = "CODEID" codeName = "CODENM" defaultValue= "${varChooseOne}"  />
		</td>	
		<fmtPaymentList:message key="LBL_PAYMENT_MODE" var="varPaymentMode"/>
		<td width="14%" class="RightTablecaption" align="right" ><gmjsp:label type="RegularText"  SFLblControlName="${varPaymentMode}:" td="false"/></td>			
		<td>&nbsp;<gmjsp:dropdown controlName="Cbo_PayMode"  seletedValue="<%= strPayMode %>" 	
					 width="150" value="<%= alPayMode%>" codeId = "CODEID" codeName = "CODENM" defaultValue= "${varChooseOne}"  />
		</td>
		<fmtPaymentList:message  key="LBL_DIVISION_NAME" var="varDivisionName"/>
		<td colspan="2" class="RightTablecaption"  align="center"><gmjsp:label type="RegularText"  SFLblControlName="${varDivisionName}:" td="false"/>
		<gmjsp:dropdown controlName="Cbo_Division"  seletedValue="<%= strDivisionId %>" 	
					 value="<%= alCompDiv %>" codeId = "DIVISION_ID" codeName = "DIVISION_NAME" defaultValue= "${varChooseOne}"  />
	</tr>
	<tr><td colspan="6" class="Lline"></td></tr><tr>
	<tr class="Shade">
		<td class="RightTableCaption" align="right" width="18%" height="25%"><fmtPaymentList:message key="LBL_COLLECTOR_ID"/>:</td>
		<td  width="19%" align="left">&nbsp;<gmjsp:dropdown controlName="Cbo_CollectorId"  seletedValue="<%=strCollectorId%>" 	
				value="<%= alCollector%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "${varChooseOne}" />
		</td>
		<td width="15%" class="RightTableCaption" align="right" width="22%"><fmtPaymentList:message key="LBL_CREDIThOLDtYPE"/>:</td>
			<td align="left">&nbsp;<gmjsp:dropdown controlName="Cbo_CreditTypeId"  seletedValue="<%=strCreditType%>" 	
					value="<%= alCreditHold%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "${varChooseOne}" /> 
			</td>
			<fmtPaymentList:message key="BTN_LOAD" var="varLoad"/>
		<!-- <td align="left" width="150"><gmjsp:button value="Go" name="Btn_Go" gmClass="button" style="width: 5em; height: 22" onClick="javascript:fnSubmit();" buttonType="Load" /></td> -->
		<td colspan="2" align="center" height="25"><gmjsp:button value="${varLoad}" name="Btn_Go" gmClass="button" style="width: 5em; height: 22" onClick="javascript:fnSubmit();" buttonType="Load" /> </td>
	</tr>

	<tr><td colspan="4">
	</td>
	</tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td colspan="4" class="Lline"></td></tr>
	<tr>
		<td height="30" class="aligncenter">
    
		<display:table name="requestScope.results.rows" export="true" id="currentRowObject" class="its"	varTotals="ordertotals"		decorator="com.globus.displaytag.beans.DTInvoiceOrderWrapper">
        <display:setProperty name="export.excel.filename" value="A/R Payments.xls" /> 
		  <fmtPaymentList:message key="LBL_RECEIVED_DATE" var="varReceivedDate"/><display:column property="R_DATE" title="${varReceivedDate}" sortable="true" format="<%=strRptFmt%>"/>
		  <fmtPaymentList:message key="LBL_RECEIVED_MODE" var="varRecievedMode"/><display:column property="R_MODE" title="${varRecievedMode}" />
		  <fmtPaymentList:message key="LBL_INVOICE_ID" var="varInvoiceId"/><display:column property="INV_ID" title="${varInvoiceId}" />
		  <fmtPaymentList:message key="LBL_INVOICE_DATE" var="varInoviceDate"/><display:column property="INVOICE_DATE" title="${varInoviceDate}" sortable="true" format="<%=strRptFmt%>"/>
		  <%if(strInvSource.equals("26240213")){ %>
		  <fmtPaymentList:message key="LBL_DEALER_ID" var="varDealerName"/><display:column property="ACCT_ID" title="${varDealerName}" sortable="true"/><!-- PMT-36541 -->
		  <fmtPaymentList:message key="LBL_DEALER_NAME" var="varDealerName"/><display:column property="ACCT_NAME" title="${varDealerName}" sortable="true"/>
		  <%}else{ %>
		  <fmtPaymentList:message key="LBL_ACCOUNT_ID" var="varRepAccountName"/><display:column property="ACCT_ID" title="${varRepAccountName}" sortable="true"/><!-- PMT-36541 -->
		  <fmtPaymentList:message key="LBL_REP_ACCOUNT_NAME" var="VarRepAccountName"/><display:column property="ACCT_NAME" title="${VarRepAccountName}" sortable="true"/>
		  <%} %>
		   <fmtPaymentList:message key="LBL_DIVISION_NAME" var="varDivName"/><display:column property="DIVISION_NAME" class="alignleft" title="${varDivName}" sortable="true"/>
		  <fmtPaymentList:message key="LBL_CREDIT_HOLD_TYPE" var="varCreditHoldType"/><display:column property="CREDITTYPE_NAME" class="alignleft" title="${varCreditHoldType}" sortable="true"/>		
		  <fmtPaymentList:message key="LBL_PAYMENT_TERMS" var="varPaymentTerms"/><display:column property="PAYMENT_TERMS" title="${varPaymentTerms}" style="width:100px" sortable="true" />
		  <fmtPaymentList:message key="LBL_COLLECTOR_ID" var="varCollectorId"/> <display:column property="COLLECTOR_NAME" title="${varCollectorId}" style="width:100px" sortable="true" />
		  <fmtPaymentList:message key="LBL_PAYMENT_AMOUNT" var="varPaymentAmount"/> <display:column property="PAYMENT_AMT"  title="${varPaymentAmount}" class="alignright" total ="true" format="<%=strCurrFmt%>" />
		  <fmtPaymentList:message key="LBL_DETAILS" var="varDetails"/><display:column property="R_DTLS" title="${varDetails}" />
		  <fmtPaymentList:message key="LBL_CURRENCY" var="varCurrency"/><display:column property="CURRENCY" title="${varCurrency}" style="text-align: center; " />
		  	<display:footer media="html"> 
		<% 
			HashMap temp = ((HashMap)pageContext.getAttribute("ordertotals"));
		  	String strPayAmt = ((HashMap)pageContext.getAttribute("ordertotals")).get("column11").toString();
		%>

  		<tr class = shade>
		  		<td colspan = "10" align="right"> <B> <fmtPaymentList:message key="LBL_TOTAL"/> : </B></td>
		  		<td  Height="25"   class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strPayAmt)%></td>
		  		<td colspan = "2"> </td>
		</tr>
 		</display:footer>
  	
		</display:table>
 		</td>
	</tr>
</table>
</td></tr>
</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
