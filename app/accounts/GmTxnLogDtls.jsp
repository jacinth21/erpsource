 <%
/**********************************************************************************
 * File		 		: GmTxnLogDtls.jsp
 * Desc		 		: This screen is used to view all the log informations
 * Version	 		: 1.0 
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ include file="/common/GmHeader.inc" %>


 <!-- \accounts\GmTxnLogDtls.jsp -->
<%
String strID 	= GmCommonClass.parseNull(request.getParameter("hID"));
String strMode = GmCommonClass.parseNull(request.getParameter("hMode"));

// Used to align submit function
String strAlignSumit = GmCommonClass.parseNull(request.getParameter("hAlignSubmit"));
String strHideComments = GmCommonClass.parseNull(request.getParameter("hHideComment"));

ArrayList alresult =  GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("results"));
%>
<html>

<head>
<title>Globus Medical: View Log Details</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script>
function fnClose()
{
	window.close();
}
</script>
</head>

<body leftmargin="5" topmargin="5" >
<form name="frmCallLog" method="post" action="<%=strServletPath%>/GmCommonLogServlet">
<table border="1px" width="100%" cellspacing="0" cellpadding="0">
<tr><td>
<table border="0px" width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td> 
			<jsp:include page="/accounts/GmCallLogDetails.jsp" >
			<jsp:param name="hID" value="<%=strID %>" />
			<jsp:param name="hMode" value="<%=strMode %>" />
			<jsp:param name="hAlignSubmit" value="<%=strAlignSumit %>" />
			<jsp:param name="hHideComment" value="<%=strHideComments %>" />
			<jsp:param name="results" value="<%=alresult %>" />
			<jsp:param name="hEdit" value="No" />
			</jsp:include>
		</td>
	</tr>
	<tr><td class="Line"></td></tr> 
	<tr>
		<td class="aligncenter" align="center" height="30px">
			<gmjsp:button tabindex="4" value="Close" gmClass="button" onClick="fnClose();" buttonType="Load" />
		</td>
	</tr>
</table>
</td></tr>
</table>
</form>

<%@ include file="/common/GmFooter.inc" %>
</body>
</html>

