 <%
/**********************************************************************************
 * File		 		: GmCallLogDetails.jsp
 * Desc		 		: This screen is used to record all log information
 * Version	 		: 1.0 
 * author			: Richard K
************************************************************************************/
%>
<%@ page language="java" %>
<%@page import="java.net.URLEncoder"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.servlets.GmCommonCancelServlet" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtCallLogDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmCallLogDetails.jsp -->
<fmtCallLogDetails:setLocale value="<%=strLocale%>"/>
<fmtCallLogDetails:setBundle basename="properties.labels.custservice.ProcessRequest.GmProcessRequestItems"/>
<%


String strPdmgmntJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.ProcessRequest.GmProcessRequestItems", strSessCompanyLocale);
	String strSubmit = "";
	String strType 	= GmCommonClass.parseNull(request.getParameter("hType")) ;
	String strID 	= GmCommonClass.parseNull(request.getParameter("hID"));
	String strMode = GmCommonClass.parseNull(request.getParameter("hMode"));
	String strEditFl = GmCommonClass.parseNull(request.getParameter("hEdit"));
	
	String strHeaderFL = GmCommonClass.parseNull(request.getParameter("hHeader"));
	String strHeight = GmCommonClass.parseNull(request.getParameter("htxtHeight"));
	
	// Used to align submit function
	String strAlignSumit = GmCommonClass.parseNull(request.getParameter("hAlignSubmit"));
	String strSkipRefID = GmCommonClass.parseNull(request.getParameter("hSkipRefID"));
	String strHideComments = GmCommonClass.parseNull(request.getParameter("hHideComment"));
	String strGetComments = GmCommonClass.parseNull((String)request.getAttribute("COMMENTS"));
	
	String strLogId = GmCommonClass.parseNull((String)request.getAttribute("LOGID"));
	String strJNDIConnection = GmCommonClass.parseNull((String)request.getAttribute("hJNDIConnection"));
	
	ArrayList alresult =  GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("results"));

	// Set default height
	strHeight = strHeight.equals("") ? "80":strHeight;
	
	
	//Added code for getting company info
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();
	String strCompDtFmt = gmDataStoreVO.getCmpdfmt();
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\",\"cmpdfmt\":\""+strCompDtFmt+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);


	
%>
<html>

<head>
<title>Globus Medical: Call Log Details</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strPdmgmntJsPath%>/GmGroupPartMapping.js"></script>



<!-- tinyMCE -->
<script language="javascript" type="text/javascript" src="<%=strExtWebPath%>/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
	// Notice: The simple theme does not use all options some of them are limited to the advanced theme
	tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		theme_advanced_path : false,
		height : "<%=strHeight%>",
		width: "100%" 
	});
</script>
<!-- /tinyMCE -->

<script>

function fnSubmit()
{
	var companyInfoObj = '<%=strCompanyInfo%>';
	document.frmCallLog.hAction.value = 'Save';
	document.frmCallLog.action = '/GmCommonLogServlet?companyInfo='+companyInfoObj;
	fnStartProgress('Y');
	document.frmCallLog.submit();
}
function fnEditComment(logid)
{
	
	document.frmCallLog.hAction.value ='editcomment';
	document.frmCallLog.hLogId.value = logid;
	document.frmCallLog.submit();
	
}
function fnVoid(log)
{
	document.frmCallLog.hTxnId.value = log;
	document.frmCallLog.hTxnName.value = log;
	document.frmCallLog.hAction.value='Load';
	document.frmCallLog.hCancelType.value='VDCLOG';
	document.frmCallLog.action ="/GmCommonCancelServlet";
	document.frmCallLog.submit();
	
}

function fnLoadAuditTrail(logid,refid){
	url = "GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId="+refid+"&txnId="+logid;
	name = "";
	args = "resizable=yes,scrollbars=yes,top=250,left=350,width=735,height=600";		
	window.open(url,name,args);
}

function fnClose()
{
	window.close();
}

</script>
</head>

<body leftmargin="0" topmargin="5" >
<form name="frmCallLog" method="post" action="<%=strServletPath%>/GmCommonLogServlet">
<input type="hidden" name="hID" value="<%=strID%>">
<input type="hidden" name="hType" value="<%=strType%>">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hTxnId" value="">
<input type="hidden" name="hCancelType" value="VDCLOG">
<input type="hidden" name="hTxnName" value="">
<input type="hidden" name="hMode" value="<%=strMode%>">

<input type="hidden" name="hLogId" value="<%=strLogId%>" >
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="hHeader" value="<%=strHeaderFL%>">
<input type="hidden" name="htxtHeight" value="<%=strHeight%>">
<input type="hidden" name="hAlignSubmit" value="<%=strAlignSumit%>">
<input type="hidden" name="hSkipRefID" value="<%=strSkipRefID%>">
<input type="hidden" name="hJNDIConnection" value="<%=strJNDIConnection%>">

<% if ( (strSkipRefID.equals("Y")) && alresult.size() != 0) { %>
	<display:table name="results" class="its" id="currentRowObject" excludedParams="hAction">
	<fmtCallLogDetails:message key="LBL_USERNAME" var="varUserName"/>
	<display:column property="UNAME" title="${varUserName}" sortable="true" />
	<%System.out.println("This is test "); %>
	<fmtCallLogDetails:message key="LBL_DATEENTERED" var="varDateEntered"/>
	<display:column property="DT" title="${varDateEntered}"  sortable="true"/>
	<fmtCallLogDetails:message key="LBL_USERCOMMENTS" var="varUserComments"/>
	<display:column property="COMMENTS" title="${varUserComments}" sortable="true" />
	</display:table>
<% }%>
<table border="0" width="100%" cellspacing="0" cellpadding="0" style="">
<%	if (!strHeaderFL.equals("N")) {%>
	<tr>
		<td class="RightDashBoardHeader" height="20" > <fmtCallLogDetails:message key="LBL_LOG_DETAILS"/></td></tr>
	<tr><td height="1" bgcolor="#666666"></td></tr>
	<tr><td  height="1"> </td></tr>
<% }%>	




	<tr>
		<%if (strHideComments.equals("")) 
			{ %>
				
			<td align="left" width="90%">&nbsp; &nbsp;<textarea name="Txt_Reason" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');"  	
			onBlur="changeBgColor(this,'#ffffff');" tabindex=2><%=GmCommonClass.replaceForXML(strGetComments)%></textarea></td>
	    <%if(!strGetComments.equals("")){ 
	        strSubmit= GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("BTN_SUBMIT"));
	    } else {
	        strSubmit=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("BTN_UPDATE"));
	        } %>
			<%	if (strAlignSumit.equals("L")) {%>	
					<td>
						&nbsp; <gmjsp:button tabindex="3"  value="<%=strSubmit%>" buttonType="Save"  gmClass="button" onClick="fnSubmit();" />&nbsp;
			 
					</td>
<%				}				
		 } 		%>	

	</tr>
	
	
	
	
	
	
	
	
	

<%	if (!strAlignSumit.equals("L")) {%>		
	<tr><td  height="2"> </td></tr>
	<tr>
		<td  class="aligncenter" align="center">
			<fmtCallLogDetails:message key="BTN_SUBMIT" var="varSubmit"/>
			<gmjsp:button tabindex="3" value="${varSubmit}" gmClass="button" onClick="fnSubmit();" buttonType="Save" />&nbsp;
<%
			if (!strMode.equals("INC")){
%>			
			<fmtCallLogDetails:message key="BTN_CLOSE" var="varClose"/>
			<gmjsp:button tabindex="4" value="${varClose}" gmClass="button" onClick="fnClose();" buttonType="Load" />&nbsp;
<%	}	%>
		</td>
	</tr>
<% }%>	
</table>

<% if (!strSkipRefID.equals("Y")) { %>
<display:table name="results" class="its" id="currentRowObject" excludedParams="hAction"
decorator="com.globus.common.displaytag.beans.DTEditCommentWrapper"  
 freezeHeader="true"> 
<%if(!strEditFl.equals("No")){ %>
<display:column property="LOGID" title="" />
<display:column property="VOID" title=""  />
<display:column property="AUDIT_LOG" title=""  />
<%} %>
<fmtCallLogDetails:message key="LBL_USERNAME" var="varUserName"/>
<display:column property="UNAME" title="${varUserName}" sortable="true" />
<fmtCallLogDetails:message key="LBL_DATEENTERED" var="varDateEntered"/>
<display:column property="DT" title="${varDateEntered}"  sortable="true"/>
<fmtCallLogDetails:message key="LBL_REFERENCEID" var="varReferenceId"/>
<display:column property="ID" title="${varReferenceId}" sortable="true" />
<fmtCallLogDetails:message key="LBL_USERCOMMENTS" var="varUserComments"/>
<display:column property="COMMENTS" title="${varUserComments}" sortable="true" />
</display:table>
<%} %>
</form>

<SCRIPT> 

	document.frmCallLog.Txt_Reason.focus();

</SCRIPT>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>

