<%
/**********************************************************************************
 * File		 		: GmCreditHoldSetup.jsp
 * Desc		 		: Account credit hold setup screen
 * author			: Gabriel
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="fmtCreditHoldSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmCreditHoldSetup.jsp -->
<fmtCreditHoldSetup:setLocale value="<%=strLocale%>"/>
<fmtCreditHoldSetup:setBundle basename="properties.labels.accounts.GmCreditHoldSetup"/>
<%

String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Account Credit Hold Setup </TITLE>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="javascript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strAccountJsPath%>/GmCreditHoldSetup.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<bean:define id="alAccountsList" name="frmCreditHold" property="alAccountsList" type="java.util.ArrayList"></bean:define>
<bean:define id="submitAccFl" name="frmCreditHold" property="submitAccFl" type="java.lang.String"></bean:define>

<%
	/*The Below Mentioned Code is Added To Fix PBUG-257.When Loading Account Statement Report From Credit Hold Screen From Date needs to be 
	Defaulted to Start Date of the month and To Date Needs to Be Sysdate.*/
	Calendar cal = new GregorianCalendar();
	int month = cal.get(Calendar.MONTH);
	String strSessApplDateFmt = strGCompDateFmt;
	String strSessCurrSymbol = strGCompDateFmt;
	String strTodaysDate =
	    GmCalenderOperations.getCurrentDate(strSessApplDateFmt);
	String strFromDate = GmCalenderOperations.getAnyDateOfMonth(month, 01, strSessApplDateFmt);// First Date of Month is assigned to strFromDate
%>
</HEAD>
<script>
	var currsymbol = '<%=strSessCurrSymbol%>';
	var datformat = '<%=strSessApplDateFmt%>';
	var ToDate = '<%=strTodaysDate%>';
	var FromDate = '<%=strFromDate%>';
</script>
<BODY leftmargin="30" topmargin="15" onload="fnPageLoad();" >
<html:form action="/gmCreditHold.do?method=fetchAcctCreditHold" >
<html:hidden property="strOpt" />
<html:hidden property="accountName" />
<html:hidden property="creditAttrString" />
<html:hidden property="emailType" />
<input type="hidden" name="hCreditHoldType" value=""/>
<table class="DtTable800" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td height="25" colspan="6" class="RightDashBoardHeader">&nbsp;<fmtCreditHoldSetup:message key="LBL_ACCOUNT_CREDIT_HOLD_SETUP"/></td>
	<td  height="25" class="RightDashBoardHeader" align="right">
	<fmtCreditHoldSetup:message key="LBL_HELP" var="varHelp"/>
	<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
    height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("ACCT_CREDIT_HOLD_SETUP")%>');" />
	</td></tr>
	<tr><td class="LLine" height="1" colspan="7"></td></tr>	
		<tr class="shade">
		<td colspan="3" class="RightTableCaption" align="right" height="25" ><fmtCreditHoldSetup:message key="LBL_REP_ACCOUNT_LIST"/>:</td>
		<td colspan="5">&nbsp;<gmjsp:dropdown controlName="accountId" SFFormName="frmCreditHold" SFSeletedValue="accountId" defaultValue= "[Choose One]" 
				SFValue="alAccountsList" codeId="ID" codeName="NAME" onChange="javascript:fnAccountChange();"/>&nbsp;&nbsp;
		 <input  name="acctId" size="8" value="<bean:write name="frmCreditHold" property="accountId"/>" onBlur="javascript:fnLoadAccInfo(this);">&nbsp; 	   
		  </td>
	</tr>
	<tr><td class="LLine" height="1" colspan="7"></td></tr>	
	<tr>
		<td colspan="3" class="RightTableCaption" align="right" HEIGHT="25"><fmtCreditHoldSetup:message key="LBL_CREDIT_LIMIT"/>:</td>
		<td colspan="5">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td>&nbsp;<html:text property="creditLimit" maxlength="100" size="25" styleClass="InputArea"
				onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
				<fmtCreditHoldSetup:message key="LBL_CLICK_TO_OPEN_CREDIT_LOG_DETAILS" var="varOpemCredit"/>
<td><span id="spanCreditLimitHist" style="display:none;"><img id="imgEdit" style="cursor:hand" onclick="javascript:fnLoadHistory('53031');" title="${varOpemCredit}" src="<%=strImagePath%>/icon_History.gif" align="bottom"  height=15 width=18 />
                    </span></td>
                	</tr>				
			</table>	</td>			
	</tr>
<tr><td class="LLine" height="1" colspan="7"></td></tr>	
<tr class="shade">
	 <td colspan="3"  class="RightTableCaption" align="right" height="25" ><font color="red">*&nbsp;</font><fmtCreditHoldSetup:message key="LBL_CREDIT_HOLD"/>:</td>
	 <td colspan="5">
	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr> 
	<td>&nbsp;<gmjsp:dropdown  controlName="creditHoldType" SFFormName="frmCreditHold" SFSeletedValue="creditHoldType" defaultValue= "[Choose One]" 
								SFValue="alCreditHoldList" codeId="CODEID" codeName="CODENM"/>&nbsp;<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/d.gif' title='Credit Hold Type' onclick="javascript:fnShowCreditType()"/>
     <td><span id="spanCreditTypeHist" style="display:none;">&nbsp;<img id="imgEdit" style="cursor:hand" onclick="javascript:fnLoadHistory('53032');" title="${varOpemCredit}" src="<%=strImagePath%>/icon_History.gif" align="bottom"  height=15 width=18 /></span>
     </td>
 </tr>
 </table>
</td>
</tr>
	<tr><td class="LLine" height="1" colspan="7"></td></tr>	
    <tr>		
		<td colspan="3"class="RightTableCaption"  align="right" HEIGHT="25" ><fmtCreditHoldSetup:message key="LBL_OUTSTANDING_BALANCE"/>:</td>
		<td colspan="5">&nbsp;<span id="outStand"></span></td>
	
	</tr>
			
	<tr class="shade">
		<td class="RightTableCaption" colspan="3" align="right" HEIGHT="25"><fmtCreditHoldSetup:message key="LBL_LAST_PAYMENT"/>:</td>
		<td colspan="5">&nbsp;<span id="lastPayment"></span></td></td>
				</tr>
	<tr><td class="LLine" height="1" colspan="7"></td></tr>
	<tr>
	<td class="RightTableCaption" colspan="3" align="right" HEIGHT="25"><fmtCreditHoldSetup:message key="LBL_LAST_PAYMENT_DATE"/>:</td>
	<td colspan="5">&nbsp;<span id="lastPaymentDate"></span></td>
	</tr>
	<tr><td class="LLine" height="1" colspan="7"></td></tr>	
	<tr class="shade">
		<td colspan="3" class="RightTableCaption" align="right" HEIGHT="25"><fmtCreditHoldSetup:message key="LBL_LAST_INVOICE_PAYMENT"/>:</td>
		<td colspan="5">&nbsp;<span id="lastInvoicePayment"></span></td>	
		</tr>
	<tr><td class="LLine" height="1" colspan="7"></td></tr>	
	<tr>
		<td colspan="3" class="RightTableCaption" align="right" height="24"><fmtCreditHoldSetup:message key="LBL_AR_SUMMARY"/>:</td>
		<fmtCreditHoldSetup:message key="BTN_LOAD" var="varLoad"/>
		<td colspan="5">&nbsp;<gmjsp:dropdown controlName="arSummary" SFFormName="frmCreditHold" SFSeletedValue="arSummary" defaultValue= "[Choose One]" 
				SFValue="alARSummaryList" codeId="CODEID" codeName="CODENM" onChange="javascript:fnLoadAccId();"/>&nbsp;&nbsp;&nbsp;<gmjsp:button value="${varLoad}" name="Btn_Load" gmClass="button" buttonType="Load" style="height:20px" onClick="javascript:fnLoadARSummary();" />
				<fmtCreditHoldSetup:message key="BTN_ACCOUNT_STATEMENT" var="varAccountStatement"/>
				&nbsp;&nbsp;<gmjsp:button value="${varAccountStatement}" name="Btn_Load" gmClass="button" buttonType="Load" style="height:20px" onClick="javascript:fnLoadAccountStatement();" />
		</td>	</tr>
	<tr><td class="LLine" height="1" colspan="7"></td></tr>	
	<tr>
		<td colspan="8">
				<jsp:include page="/common/GmIncludeLogAjax.jsp" >
					<jsp:param name="AuditType" value="400915" />
					<jsp:param name="LogMode" value="Edit" />
					<jsp:param name="Mandatory" value="yes" />
				</jsp:include>
		</td>
	</tr>
	<tr><td class="LLine" height="1" colspan="7"></td></tr>	
	<tr>
        <td colspan="7" align="center" height="30">&nbsp;
			<logic:equal name="frmCreditHold" property="submitAccFl" value="Y">
			<fmtCreditHoldSetup:message key="BTN_SUBMIT" var="varSubmit"/>
			<gmjsp:button value="${varSubmit}" name="Btn_Submit" gmClass="button" buttonType="Save"  onClick="javascript:fnSubmit();" />&nbsp;
			</logic:equal>	
			<fmtCreditHoldSetup:message key="BTN_CLOSE" var="varClose"/>
			<gmjsp:button value="${varClose}"  name="Btn_Close"  gmClass="Button" buttonType="Load"  onClick="javascript:fnClosePopup();" />&nbsp;
	    </td>
	</tr>	
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %> 
</BODY>
</HTML>