<%
/**********************************************************************************
 * File		 		: GmReleaseInvoicePayment.jsp
 * Desc		 		: Ready to payment Screen
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>

<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Date"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.Date,java.lang.*" %>
<%@ taglib prefix="fmtReleaseInvoicePayment" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmReleaseInvoicePayment.jsp -->
<fmtReleaseInvoicePayment:setLocale value="<%=strLocale%>"/>
<fmtReleaseInvoicePayment:setBundle basename="properties.labels.accounts.GmReleaseInvoicePayment"/>
<bean:define id="returnPOList" name="frmACInvoicePayment" property="returnPOList" type="java.util.List"></bean:define>
<bean:define id="returnInvcList" name="frmACInvoicePayment" property="returnInvcList" type="java.util.List"></bean:define> 
<bean:define id="matchingPO" name="frmACInvoicePayment" property="matchingPO" type="java.lang.String"> </bean:define>
<bean:define id="invcStatus" name="frmACInvoicePayment" property="invcStatus" type="java.lang.String"> </bean:define>
 
<bean:define id="matchingType" name="frmACInvoicePayment" property="matchingType" type="java.lang.String"> </bean:define>
<%
	
	ArrayList alPOList = new ArrayList();
	ArrayList alInvcList = new ArrayList();
	 
	alPOList = GmCommonClass.parseNullArrayList((ArrayList) returnPOList);
	alInvcList = GmCommonClass.parseNullArrayList((ArrayList) returnInvcList);
	// System.out.println("matchingPO  Outer----------------------------:"+matchingPO);
	int porowsize = alPOList.size();
	int invcrowsize = alInvcList.size();
	double invoiceAmtTotal =0;
	int intPOAdjusted	= 0;
	int intInvcAdjusted = 0;
	double dTotalReadyForPayment = 0;
	
	HashMap hmLoop = new HashMap();
	HashMap hmInvcLoop = new HashMap();
	HashMap hmCurrency = new HashMap();
	
	String strPOorderId = "";
	String strVendorNm = "";
	String strVendorId = "";
	String strPOreceivedQty = "";
	String strPOorderedQty = "";
	String strPOacceptedQty = "";
	String strDateInitiated = "";
	String strPOTotalAmount = "";
	String strPOReadyPayment = "";
	String strShade = "";
	String strShadeInv ="";
	String strPONumber = "";
	String strVendorName = "";
	String strGmInvoiceID = "";
	String strVendorInvcID = "";
	String strAcceptedQty = "";
	String strReceivedQty = "";
	String strInvcAmount = "";
	String strReadyForPayment = "";
	String strInvoiceDate ="";
	String strBatchNumber = "";
	String strBatchDate = "";
	String strInitiatedBy = ""; 
	String strReleasedBy ="";
	String strDisabled ="";
	String strchecked ="";
	String	strStatusfl ="";
	String strReleasedDt ="";
	String strPaymentStatus ="";
	String flag="";
	String strCurrSign="";
	String strApplCurrFmt="";
	
	String strTotalReadyForPayment = "";
	String strApplnDateFmt = strGCompDateFmt;
	String strWikiTitle = GmCommonClass.getWikiTitle("READY_FOR_PAYMENT");
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	strCurrSign =(String)hmCurrency.get("CMPCURRSMB");
	strApplCurrFmt = (String)hmCurrency.get("CMPCURRFMT");
	DecimalFormat doublef = new DecimalFormat(strApplCurrFmt);
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Download Invoice Detail</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"	media="print" />
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
var format = '<%=strApplnDateFmt%>';
var lblVendorName='<fmtReleaseInvoicePayment:message key="LBL_VENDOR_NAME"/>';
function fnReload()
{   
	var objFromDT = document.frmACInvoicePayment.invcInitiatedDateFrom.value;
	var objToDT = document.frmACInvoicePayment.invcInitiatedDateTo.value;
	// check correct month format entered
    CommonDateValidation(document.frmACInvoicePayment.invcInitiatedDateFrom,format,Error_Details_Trans(message_accounts[254],format));
	CommonDateValidation(document.frmACInvoicePayment.invcInitiatedDateTo,format,Error_Details_Trans(message_accounts[255],format));
	
	// Validate Order From/To Dates in proper order
	if (dateDiff(objFromDT, objToDT,format) < 0)
	{
		Error_Details(message_accounts[256]);
	}
	var varInvoiceStatus = document.frmACInvoicePayment.invcStatus.value;
	var varPOnumber = document.frmACInvoicePayment.poNumber.value; 								 
	if((varInvoiceStatus =='30')&& varPOnumber =='')
 	fnValidateDropDn('vendorId',lblVendorName);
	if (ErrorCount > 0)  
					{
						Error_Show();
						Error_Clear();
						return false;
									} 
	
	document.frmACInvoicePayment.strOpt.value = "reload";
	fnStartProgress(); 
	document.frmACInvoicePayment.submit();   
} 
 
function fnRollback()
{ 
	var varRow = <%=invcrowsize%>;  
	var counter = 0; 
 
	for (var j =0; j < varRow; j++) 
	{ 
		obj  = eval("document.frmACInvoicePayment.Chk_payid"+j);
		if(obj.checked)		
		{  	varPaymentId = obj .value;
			counter++;
		} 
	 
	}
 	if(counter==0 || counter >1)
 	Error_Details(message[37]);
	
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	} 
 		document.frmACInvoicePayment.hTxnId.value = varPaymentId;
		document.frmACInvoicePayment.hCancelType.value = 'RBPYT'
		document.frmACInvoicePayment.action ="/GmCommonCancelServlet";
		document.frmACInvoicePayment.hAction.value = "Load";				
 		document.frmACInvoicePayment.submit();
		//alert(" counting " + counter);
	//	return counter;
} 
function fnSelectPOAll(index){

   TotalRows =    <%=invcrowsize%>  ;
// 	TotalpoRows =   <%=intPOAdjusted%> ; 
//	for (var i=0;i<TotalpoRows   ;i++ )
//	{
		obj = eval("document.frmACInvoicePayment.Chk_poid"+index);
		 
		for (var j=0;j<TotalRows  ;j++ )
		{ 
			obj1 = eval("document.frmACInvoicePayment.Chk_payid"+j);	
			 
			if((obj.id ==obj1.id)&&(!obj1.disabled))
		 	obj1.checked = obj.checked;		 
		}  
//	}

	} 
	
	function fnSelectAll(){
		  TotalRows =  <%=invcrowsize%>;
	  TotalpoRows = <%=porowsize%>; 
	 for (var i=0;i<TotalpoRows   ;i++ )
		{
			obj = eval("document.frmACInvoicePayment.Chk_poid"+i);	
			 if(obj)
			obj.checked = document.frmACInvoicePayment.Chk_selectAll.checked;		 
		} 
	 for (var j=0;j<TotalRows  ;j++ )
		{
			obj = eval("document.frmACInvoicePayment.Chk_payid"+j);	
			if(!obj.disabled)
			obj.checked = document.frmACInvoicePayment.Chk_selectAll.checked;		 
		}
	}  
function fnSubmit(release_fl)
	{
		var varRows =   <%=invcrowsize%>;
		var totalAmt = 0; 
		var inputString = '';  
		var count = 0; 
	 	
		for (var i =0; i < varRows ; i++) 
		{
			obj = eval("document.frmACInvoicePayment.Chk_payid"+i);
		 
			varPaymentID = obj.value;
		 
			if(obj.checked&&(!obj.disabled))		
			{ 
				inputString = inputString  + varPaymentID +  '^';
			 
				count++;
			}
			else
			 obj.value = ""; 
		 
		}
		if(count==0)
		Error_Details(message[36]);
		
		if (ErrorCount > 0)  
		{
				Error_Show();
				Error_Clear();
				return false;
		}
		 
	// 	 alert("inputString"+inputString);
		document.frmACInvoicePayment.release_fl.value = release_fl
		document.frmACInvoicePayment.strOpt.value =  'save'; //'save';  
		document.frmACInvoicePayment.hinputStr.value = inputString;
	   	document.frmACInvoicePayment.submit();	
	 
	}    

function fnPODetails(po,vid)
{ 
	 windowOpener("<%=strServletPath%>/GmPOServlet?hAction=ViewPO&hPOId="+po+"&hVenId="+vid,"ViewPO","resizable=yes,scrollbars=yes,top=150,left=50,width=750,height=600");
}
 
 function fnInvcDetails(strPaymentID)
 {   
      windowOpener("/gmAPSaveInvoiceDtls.do?strOpt=viewInvc&hpaymentId="+strPaymentID,"viewInvc","resizable=yes,scrollbars=yes,top=250,left=300,width=900,height=500");
    }
   
  
function fnEditInvoice(strPaymentID, strPOnumber)
 {   
      windowOpener("/gmDHRPayment.do?strOpt=InvcGo&hpaymentId="+strPaymentID+"&containerPONumber="+strPOnumber,"InvcGo","resizable=yes,scrollbars=yes,top=250,left=300,width=900,height=500");
    }
      
function changeDefault(val)
{ 
	// document.frmACInvoicePayment.poNumber.value = val;
	 if(document.frmACInvoicePayment.poNumber.value =="")
	 insertAtEnd(document.frmACInvoicePayment.poNumber,val);
} 

 
function checkDefault()
{ 
	 if(document.frmACInvoicePayment.poNumber.value=='GM-PO-') 
		document.frmACInvoicePayment.poNumber.value = ''	;
}   

function setCaretToEnd (el) {
  if (el.createTextRange) {
    var v = el.value;
    var r = el.createTextRange();
    r.moveStart('character', v.length);
    r.select();
  }
}
function insertAtEnd (el, txt) {
  el.value += txt;
  setCaretToEnd (el);
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmACInvoicePayment.do">
	<html:hidden property="strOpt" value="" /> 
	<input type="hidden" name="hAction" value="">
	<html:hidden property="hinputStr" />  
	<html:hidden property="release_fl" /> 
	<html:hidden property="hTxnId" value=""/>
	<html:hidden property="hCancelType" value=""/> 
	 
	<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="3">&nbsp;<fmtReleaseInvoicePayment:message key="LBL_INVOICE_MATCH"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtReleaseInvoicePayment:message key="LBL_HELP" var="varHelp"/>
			 <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		<tr>
			<td bgcolor="#666666" height="1" colspan="4"></td>
		</tr>
		<tr>
			<td width="1098" height="100" valign="top" colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">  
			<!-- Custom tag lib code modified for JBOSS migration changes -->	
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">  <fmtReleaseInvoicePayment:message key="LBL_VENDOR_NAME"/> :</td> 
                        <td style="padding-left: 3px;">
	                        <gmjsp:dropdown controlName="vendorId"	SFFormName="frmACInvoicePayment" SFSeletedValue="vendorId"
								SFValue="alVendorList" defaultValue="[Choose One]" codeId="ID" codeName="NAME" />
	                    </td> 
                         <td class="RightTableCaption" align="right" HEIGHT="24">  <fmtReleaseInvoicePayment:message key="LBL_PO_NUMBER"/> :</td> 
				                     <td style="padding-left: 3px;">&nbsp;<html:text property="poNumber"  size="20" styleClass="InputArea" onfocus="changeDefault('GM-PO-');" onblur="checkDefault();" />&nbsp;
			    		              </td> 
			    		            
                       <td class="RightTableCaption" align="right" HEIGHT="24" style="white-space: nowrap; padding-left: 6px;"><fmtReleaseInvoicePayment:message key="LBL_INITIATED_BY"/> : </td> 
				                     <td>&nbsp;
				                     <gmjsp:dropdown controlName="invcInitiatedBy"	SFFormName="frmACInvoicePayment" SFSeletedValue="invcInitiatedBy"
								SFValue="alInvcInitiatedBy" defaultValue="[Choose All]" codeId="CODENMALT" codeName="CODENM" />
				                      
						 </td>	 
                    </tr>  
                    <tr>
						<td class="ELine" height="1" colspan="8"></td>
					</tr>
                    <tr>
                     <td class="RightTableCaption" align="right" HEIGHT="24">  <fmtReleaseInvoicePayment:message key="LBL_INVOICE_STATUS"/> :</td> 
				                     <td>&nbsp;&nbsp;<gmjsp:dropdown controlName="invcStatus"	SFFormName="frmACInvoicePayment" SFSeletedValue="invcStatus"
											SFValue="alInvcStatus"   codeId="CODENMALT" codeName="CODENM" />
			    		             </td> 
			    		   <td class="RightTableCaption" align="right" HEIGHT="24" style="white-space: nowrap;"><fmtReleaseInvoicePayment:message key="LBL_INITIATED_DATE_FROM"/> :</td> 
				                     <td>&nbsp; 
   				      <gmjsp:calendar SFFormName="frmACInvoicePayment" controlName="invcInitiatedDateFrom" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>        
						 </td>    
						 	 <td class="RightTableCaption" align="right" HEIGHT="24"><fmtReleaseInvoicePayment:message key="LBL_TO"/> :</td> 
				                     <td style="white-space: nowrap;">&nbsp; 
				       <fmtReleaseInvoicePayment:message key="BTN_LOAD" var="varLaod"/>
 				       <gmjsp:calendar SFFormName="frmACInvoicePayment" controlName="invcInitiatedDateTo"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>        
			                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<gmjsp:button name="Load" value="&nbsp;&nbsp;${varLaod}&nbsp;&nbsp;&nbsp;" gmClass="button" onClick="fnReload();" buttonType="Load" />   
						 </td>   
                    </tr>
                     
		<tr>
			<td class="Line" height="1" colspan="8"></td>
		</tr>
		<%
			if (porowsize > 0) {
		%>
		 <tr>
  			<td colspan="8" align="left"  ><input type="checkbox" name="Chk_selectAll" onClick="fnSelectAll();"><fmtReleaseInvoicePayment:message key="LBL_SELECT_ALL"/> </td>
 		</tr> 
 		 
		
					   <tr><td colspan="8" class="Line"></td></tr>  	 
					<tr>
						<td colspan="8"> 
						<div style="overflow:auto;height=500">
							<table width="100%" cellpadding="0" cellspacing="0" border="0"  >
						 <thead>	
					<tr class="ShadeRightTableCaption" style="position:relative; top:expression(this.offsetParent.scrollTop);">
						<th width="100"  align="center"><fmtReleaseInvoicePayment:message key="LBL_PO_NUMBER"/></td>
						<th width="160"  align="center"><fmtReleaseInvoicePayment:message key="LBL_VENDOR_NAME"/> </td>
						<th   width="6" align="center"></td>
						<th   width="15" align="center"></td>
				 		<th   width="60"  align="center"> <fmtReleaseInvoicePayment:message key="LBL_VENDOR_INVOICE_NUMBER"/></td>	
				 		<th width="50" align="center"><fmtReleaseInvoicePayment:message key="LBL_ORDER_QTY"/></td>
						<th width="50" align="center"><fmtReleaseInvoicePayment:message key="LBL_RECE_QTY"/></td>
						<th width="50" align="center"><fmtReleaseInvoicePayment:message key="LBL_ACCP_QTY"/></td>
						<th width="90" align="center"><fmtReleaseInvoicePayment:message key="LBL_AMOUNT"/></td>
						
						<th width="90" align="center"><fmtReleaseInvoicePayment:message key="LBL_READY_FOR_PAYMENT"/></td>
						<th width="80" align="center"><fmtReleaseInvoicePayment:message key="LBL_INVOICE_DATE"/></td>
						<th width="80" align="center"><fmtReleaseInvoicePayment:message key="LBL_PAYMENT_STATUS"/></td>
						
						<th width="80" align="center"><fmtReleaseInvoicePayment:message key="LBL_RELEASED_DATE"/></td>
						<th width="80" align="center"><fmtReleaseInvoicePayment:message key="LBL_RELEASED_BY"/></td>
						<th width="80" align="center"><fmtReleaseInvoicePayment:message key="LBL_BATCH_NUMBER"/></td>
						<th width="80" align="center"><fmtReleaseInvoicePayment:message key="LBL_BATCH_DATE"/></td>
					 	<th width="80" align="center"><fmtReleaseInvoicePayment:message key="LBL_BATCH_INITIATED_BY"/></td>	
					</tr>
					 </thead>
					 <tr><td colspan="17" class="Line"></td></tr>  
			<%
			 
							hmLoop = new HashMap(); 
							hmInvcLoop = new HashMap();
							
							for (int i = 0;i < porowsize ;i++ )
							{ 	
								 flag = ""; 
								hmLoop = (HashMap)alPOList.get(i);
 
								strPOorderId = GmCommonClass.parseNull((String)hmLoop.get("PURCHASE_ORDER_ID"));
								strVendorNm = GmCommonClass.parseNull((String)hmLoop.get("VENDOR_NAME"));
								strVendorId = GmCommonClass.parseNull((String)hmLoop.get("VENDORID"));
								strPOorderedQty = GmCommonClass.parseNull((String)hmLoop.get("PO_ORDERED_QTY")); 
								strPOreceivedQty = GmCommonClass.parseNull((String)hmLoop.get("PO_RECEIVED_QTY"));
								strPOacceptedQty = GmCommonClass.parseNull((String)hmLoop.get("PO_ACCEPTED_QTY"));
								strPOTotalAmount = GmCommonClass.parseZero((String)hmLoop.get("PO_TOTAL_AMOUNT"));
								strPOReadyPayment = GmCommonClass.parseZero((String)hmLoop.get("PO_READY_FOR_PAYMENT"));
							//	if(strPOorderId.equals("GM-PO-239238")) strPOorderedQty="100";
							// 	if(strPOorderId.equals("GM-PO-238301")) strPOorderedQty="100";
							//	if(!matchingPO.equals("")&&!strPOorderedQty.equals(strPOacceptedQty))  flag="N";  //continue;
							//	if(matchingType.equals("50030")&&!strPOreceivedQty.equals(strPOacceptedQty))  flag="N";
							//	if(!matchingPO.equals("")&&!strPOTotalAmount.equals(strPOReadyPayment))flag="N";
								double dPOTotalAmount = Double.parseDouble(strPOTotalAmount);
								strPOTotalAmount = doublef.format(dPOTotalAmount);
								double dPOReadyPayment = Double.parseDouble(strPOReadyPayment);
								strPOReadyPayment = doublef.format(dPOReadyPayment);
								
							 
							//	if(!flag.equals("N")) {	intPOAdjusted++;
							//	strShade	= (i%2 != 0)?"class=ShadeRightTableCaptionBlue":""; //For alternate Shading of rows 
								 %>
				
 
								<tr bgcolor=#AACCE8>
									<td width="160" class="RightText" HEIGHT="24" align="center"> <input type="checkbox" id= "<%=strPOorderId%>" name="Chk_poid<%=i%>"  onClick="fnSelectPOAll(<%=i%>);"> 
									<a href="javascript:fnPODetails('<%=strPOorderId%>', '<%=strVendorId%>')"><%=strPOorderId%><a></td>
									<td width="200" colspan=4 class="RightText" align="left">&nbsp;&nbsp;<%=strVendorNm%></td>
									<td width="50" class="RightText" align="right"> <%=strPOorderedQty%></td>  
									<td width="50" class="RightText" align="right"> <%=strPOreceivedQty%></td>
									<td width="50" class="RightText" align="right"><%=strPOacceptedQty%></td>
									<td width="90" class="RightText" align="right"> <% if (!strPOTotalAmount.equals(".00")) { %><%=strCurrSign%> <%=strPOTotalAmount%><%}%></td>
									<td width="90" class="RightText" align="right">&nbsp;<% if (!strPOReadyPayment.equals(".00")) { %><%=strCurrSign%>  <%=strPOReadyPayment%><%}%></td>
									<td width="80" class="RightText" align="center">&nbsp; </td>
									<td width="80" class="RightText" align="right">&nbsp; </td>
									<td width="80" class="RightText" align="right">&nbsp; </td> 
									<td width="80" class="RightText" align="right">&nbsp; </td>
									<td width="80" class="RightText" align="right">&nbsp; </td> 
									<td width="80" class="RightText" align="right">&nbsp; </td> 
									<td width="80" class="RightText" align="right">&nbsp; </td> 
								</tr>
								
				<%  	//}
						if (invcrowsize > 0)
						{ 	
							for (int j = 0;j < invcrowsize ;j++ )
							{	
								hmInvcLoop = (HashMap)alInvcList.get(j);
								 
								strPONumber = GmCommonClass.parseNull((String)hmInvcLoop.get("PO_NUMBER"));
								strVendorName = GmCommonClass.parseNull((String)hmInvcLoop.get("VENDOR_NAME"));
								strGmInvoiceID = GmCommonClass.parseNull((String)hmInvcLoop.get("GM_INVOICE_ID")); 
								strVendorInvcID = GmCommonClass.parseNull((String)hmInvcLoop.get("VENDOR_INVOICE_ID"));
								strAcceptedQty = GmCommonClass.parseNull((String)hmInvcLoop.get("ACCEPTED_QTY"));
								strReceivedQty = GmCommonClass.parseNull((String)hmInvcLoop.get("RECEIVED_QTY"));
								strInvcAmount = GmCommonClass.parseZero((String)hmInvcLoop.get("INVOICE_AMOUNT"));
								strReadyForPayment = GmCommonClass.parseZero((String)hmInvcLoop.get("READY_FOR_PAYMENT"));
								strInvoiceDate = GmCommonClass.getStringFromDate((Date)hmInvcLoop.get("INVOICE_DATE"),strGCompDateFmt); 
								strBatchNumber = GmCommonClass.parseNull((String)hmInvcLoop.get("BATCH_NUMBER"));
								strBatchDate = GmCommonClass.getStringFromDate((Date)hmInvcLoop.get("BATCH_DATE"),strGCompDateFmt);
								strInitiatedBy = GmCommonClass.parseNull((String)hmInvcLoop.get("INITIATED_BY"));
								strStatusfl = GmCommonClass.parseNull((String)hmInvcLoop.get("STATUSFL"));
								
								strPaymentStatus = GmCommonClass.parseNull((String)hmInvcLoop.get("PAYMENT_STATUS"));
								strReleasedBy = GmCommonClass.parseNull((String)hmInvcLoop.get("RELEASE_BY")); 
								strReleasedDt = GmCommonClass.getStringFromDate((java.util.Date)hmInvcLoop.get("RELEASE_DT"),strGCompDateFmt); 
								
							 
								//if(strPaymentStatus.equals("Released")||strPaymentStatus.equals("Downloaded")) 
							//	{ 	
									if(strStatusfl.equals("20")||strStatusfl.equals("30")) {strchecked = "checked"; strDisabled = "disabled";}
									if(strStatusfl.equals("0")||strStatusfl.equals("5")) {strchecked = ""; strDisabled = "disabled";} 
									if(strStatusfl.equals("10")||strStatusfl.equals("15")) 
									{	strchecked = ""; strDisabled = "";	}
							//			if(!strAcceptedQty.equals(strReceivedQty)||!strInvcAmount.equals(strReadyForPayment)) strDisabled = "disabled"; else strDisabled = "";
									
							//	else { 
							//		   strchecked  = "";
									   
									  
							//	}
							//	if(!strOrderedQty.equals(strReceivedQty)&&strReleasedBy.equals(""))	strDisabled = "disabled"; else strDisabled = "";
								
								double dInvcAmount = Double.parseDouble(strInvcAmount);
								strInvcAmount = doublef.format(dInvcAmount);
								double dReadyForPayment = Double.parseDouble(strReadyForPayment);
								strReadyForPayment = doublef.format(dReadyForPayment);
								
								strShadeInv	= (j%2 != 0)?"class=ShadeBlueBk":""; //For alternate Shading of rows
								
							//	if(flag.equals("N"))	intInvcAdjusted--;
								if(strPONumber.equals(strPOorderId)){
								//	intInvcAdjusted = j;
								 
							/*	 if(!flag.equals("N")) {	
									   if((matchingType.equals("50031")&&strAcceptedQty.equals(strReceivedQty))||(matchingType.equals("0")||(matchingType.equals("50030"))))
									   {
										  
											  intInvcAdjusted++;  
								*/ 
							%>
						
							  
							<tr <%=strShadeInv%>>
									<td width="100" class="RightText" align="center" > </td>
									<td width="160" class="RightText" align="center"> </td>
									<td width="6" class="RightText"  align="center"> <input type="checkbox"   <%=strchecked%>  <%=strDisabled%> id= "<%=strPONumber%>"  value="<%=strGmInvoiceID%>"  name="Chk_payid<%=j%>"  >  </td>
									<td width="15" class="RightText"  align="center"> <a href="javascript:fnEditInvoice('<%=strGmInvoiceID%>', '<%=strPONumber%>')"> <img src=<%=strImagePath%>/edit_icon.gif height='14' width='12' alt='Edit Invoice'></a> </td>
								 	<td width="60" class="RightText" align="left">&nbsp;
								 	<a href="javascript:fnInvcDetails('<%=strGmInvoiceID%>')"> <%=strVendorInvcID%> </a>
								 	 </td>   
									<td width="50" class="RightText" align="center"> </td>
									<td width="50" class="RightText" align="right"> <%=strReceivedQty%></td>
									<td width="50" class="RightText" align="right"><% if (strAcceptedQty.equals("")) { %>-<%}else %> <%=strAcceptedQty%></td>
									<td width="90" class="RightText" align="right"> <% if (!strInvcAmount.equals(".00")) { %> <%=strCurrSign%><%=strInvcAmount%><%}%></td>
									<td width="90" class="RightText" align="right"> <% if (!strReadyForPayment.equals(".00")) { %> <%=strCurrSign%> <%=strReadyForPayment%><%}%></td>
									<td width="80" class="RightText" align="center"> <%=strInvoiceDate%> </td>
									<td width="80" class="RightText" align="center">  <%=strPaymentStatus%> </td>
									
									<td width="80" class="RightText" align="center">  <%=strReleasedDt%></td>
									<td width="80" class="RightText" align="center">  <%=strReleasedBy%></td>
									<td width="80" class="RightText" align="center">  <%=strBatchNumber%></td>
									<td width="80" class="RightText" align="center"> <%=strBatchDate%> </td> 
									<td width="80" class="RightText" align="center">  <%=strInitiatedBy%></td> 
								</tr>
							
				<%  			 	 
									//   }
									    dTotalReadyForPayment = dTotalReadyForPayment + dReadyForPayment;
										strTotalReadyForPayment = doublef.format(dTotalReadyForPayment);
									}else  continue; 
								 
								//}
						
							}										
					}
							
				%>
				
				
				<%  					
					}
					%>		
					
						 
								
								<TR>
									<TD colspan=1 height=1 bgcolor=#cccccc class="RightText" align="lfte"><b> <fmtReleaseInvoicePayment:message key="LBL_TOAL"/>:</b></TD>
									<TD colspan=9 height=1 bgcolor=#cccccc class="RightText" align="right"><b> <%=strCurrSign%> <%=strTotalReadyForPayment%></b></TD>
									<TD colspan=7 height=1 bgcolor=#cccccc> </TD>
								</TR>
 
							</table>
							</div>
						</td>
					</tr>
					 
		<%  
				}
		%>
		<tr>
			<td class="LLine" height="1" colspan="8"></td>
		</tr>
		 
		 <%
			if (porowsize > 0) {
		%> 
		 
		<tr>
				 <fmtReleaseInvoicePayment:message key="LBL_RELEASE_FOR_PAYMENT" var="varReleaseForPaymen"/>
				 <fmtReleaseInvoicePayment:message key="LBL_ROLLBACK_RELEASED_PAYMENT" var="varRollbackReleasedPayment"/>
				 <fmtReleaseInvoicePayment:message key="LBL_RELEASE_FOR_DOWNLOAD" var="varReleaseForDownload"/>
		  		 <td colspan="8" align="center">&nbsp;&nbsp;    <p>&nbsp;</p>
		  		 <logic:equal name="frmACInvoicePayment" property="strEnableReleasePymt" value="true">
	                   <gmjsp:button value="${varReleaseForPaymen}" gmClass="button" buttonType="Save"  onClick="fnSubmit('P');" /> &nbsp;&nbsp;
	              </logic:equal>
	              <logic:equal name="frmACInvoicePayment" property="strEnableReleasePymt" value="false">
	                   <gmjsp:button value="${varReleaseForPaymen}" gmClass="button" disabled="true" buttonType="Save" onClick="fnSubmit();" /> &nbsp;&nbsp;
	                   </logic:equal>
	              <logic:equal name="frmACInvoicePayment" property="strEnableRollbackRelease" value="true">
	       <%       if(invcStatus.equals("15")) { 	%>
	                    <gmjsp:button value="${varRollbackReleasedPayment}" gmClass="button" buttonType="Save"  onClick="fnRollback();" />&nbsp;&nbsp;
	                  <% 
						}else {
					  %>  
					  <gmjsp:button value="${varRollbackReleasedPayment}" gmClass="button" disabled="true" buttonType="Save" onClick="fnSubmit();" />&nbsp;&nbsp;
					   <% 
						} 
					  %> 
	                 </logic:equal>  
                  <logic:equal name="frmACInvoicePayment" property="strEnableRollbackRelease" value="false">
                   <gmjsp:button value="${varRollbackReleasedPayment}" gmClass="button" disabled="true" buttonType="Save"  onClick="fnSubmit();" />&nbsp;&nbsp;
                   </logic:equal> 
	              <logic:equal name="frmACInvoicePayment" property="strEnableReleaseDownload" value="true">      
	                   
	           <%       if(invcStatus.equals("15")) { 	%>
	                    <gmjsp:button value="${varReleaseForDownload}" gmClass="button"  buttonType="Save"  onClick="fnSubmit('D');" />&nbsp;&nbsp;
	                  <% 
						}else {
					  %>  
					  <gmjsp:button value="${varReleaseForDownload}" gmClass="button" disabled="true" buttonType="Save" onClick="fnSubmit();" />&nbsp;&nbsp;
					   <% 
						} 
					  %>          
	                   
	              </logic:equal>
	                   
	                <logic:equal name="frmACInvoicePayment" property="strEnableReleaseDownload" value="false"> 
	                   <gmjsp:button value="${varReleaseForDownload}" gmClass="button" disabled="true"  buttonType="Save" onClick="fnSubmit();" />
	                   </logic:equal>
	                    <p>&nbsp;</p>
	                    
		         </td> 
            
		</tr>               
	 	
		<% 
			}else{
				%>
				<tr>
					<td style="white-space: nowrap;padding-top: 4px;padding-left: 2px;"><fmtReleaseInvoicePayment:message key="LBL_NOTHING_FOUND_DISPLAY"/>.</td>
				</tr>					
							<%		
						}
		%> 
		 
		
		 
	</table>
	</FORM>
	 
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>