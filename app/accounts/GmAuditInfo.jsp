<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Iterator,java.lang.*" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%
String strProdMgmtJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");	
%>

<script language="JavaScript" src="<%=strProdMgmtJsPath%>/GmFilter.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<%@ taglib prefix="fmtAuditInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- GmAuditInfo.jsp -->
<fmtAuditInfo:setLocale value="<%=strLocale%>"/>
<fmtAuditInfo:setBundle basename="properties.labels.accounts.GmAuditInfo"/>
<script type="text/javascript">
<%

String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>

</script>

<BODY>
<FORM name=frmFlagVarianceForm method="POST" >
<input type="hidden" name="hAction" value="">

<p> &nbsp;</p>
      <table border="0" style="overflow:auto;" class="dttable950" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtAuditInfo:message key="LBL_AUDIT_COUNT"/></td>
		</tr>	
         <tr>
         
         <td>
         	<tr><td>
		    <display:table name="requestScope.frmFlagVarianceForm.alAuditReport" requestURI="/gmFlagVariance.do?auditId=<%=auditId%>&distId=<%=strDistName %>&setId=<%=setId%>" decorator="com.globus.accounts.displaytag.beans.DTFlagVarianceWrapper"   class="its"  > 
			<fmtAuditInfo:message key="LBL_TAG_ID" var="varTagId"/><display:column property="TAG_ID" title="${varTagId}" sortable="true"  />
			<fmtAuditInfo:message key="LBL_CONTROL_NUMBER" var="varControlNumber"/><display:column property="CNUM" title="${varControlNumber}" sortable="true"  />			
			<fmtAuditInfo:message key="LBL_FLAG" var="varFlag"/><display:column property="FLAG" title="${varFlag}" sortable="true"  />	
			<fmtAuditInfo:message key="LBL_REASON" var="varReason"/><display:column property="REASONS" title="${varReason}" sortable="true" />
			<fmtAuditInfo:message key="LBL_APPROVE" var="varApprove"/><display:column property="APPROVED" title="${varApprove}" sortable="true"  />
			<fmtAuditInfo:message key="LBL_LAST_UPDATED_BY" var="varLastUpdatedBy"/><display:column property="UPDATED_BY" title="${varLastUpdatedBy}" sortable="true"  />		
			<fmtAuditInfo:message key="LBL_LAST_UPDATED_DATE" var="varUpdatedDate"/><display:column property="UPDATED_DATE" title="${varUpdatedDate}" sortable="true"  />
			<fmtAuditInfo:message key="LBL_POSTING_UPDATE" var="varPostingUpdate"/><display:column property="POST" title="${varPostingUpdate}"   />									
			</display:table>  
		<td>
		</tr>
    </table>
      	<tr>	
			<td align="center" colspan="6">
			<p> &nbsp;</p>
				<fmtAuditInfo:message key="BTN_SUBMIT" var="varSubmit"/>
				<gmjsp:button value="${varSubmit}" gmClass="button" name="Submit" buttonType="Save" onClick="fnSubmit(); " /> 
			</td>		
		</tr>
		<tr>
		<td align="center" colspan="6">
		<p> &nbsp;</p>
    <table border="0" style="overflow:auto;" class="dttable950" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtAuditInfo:message key="LBL_SYSTEM_QTY"/></td>
		</tr>	
         	<tr>
         	<td>
		    <display:table name="requestScope.frmFlagVarianceForm.alSystemReport" requestURI="/gmFlagVariance.do?auditId=<%=auditId%>&distId=<%=strDistName %>&setId=<%=setId%>" excludedParams="strOpt"  class="its"  > 
			<fmtAuditInfo:message key="LBL_PART_NO" var="varPartNo"/><display:column property="PNUM" title="${varPartNo}" sortable="true"  />
			<fmtAuditInfo:message key="LBL_CONTROL_NUMBER" var="varCotrolNum"/><display:column property="CNUM" title="${varCotrolNum}" sortable="true"  />
			<fmtAuditInfo:message key="LBL_QTY" var="varQty"/><display:column property="QTY" title="${varQty}"   />	
			<fmtAuditInfo:message key="LBL_TRANSACTION_ID" var="varTransactionId"/><display:column property="TXN_ID" title="${varTransactionId}" sortable="true"  />	
			<fmtAuditInfo:message key="LBL_TRANSACTION_TYPE" var="varTransactionType"/><display:column property="TXN_TYPE" title="${varTransactionType}" sortable="true"  />								
			</display:table>  
		<td>
		</tr>
         </table>
         </td>		
		</tr>
   <p> &nbsp;</p>
	<tr>
		<td align="center" colspan="6">
		<p> &nbsp;</p>
    <table border="0" class="dttable950" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtAuditInfo:message key="LBL_ASSOCIATED_DECREASE_UNRESOLVED_QTY"/></td>
		</tr>	
         	<tr>
         	<td>
   			<display:table name="requestScope.frmFlagVarianceForm.alNAssociatedReport" requestURI="/gmFlagVariance.do?auditId=<%=auditId%>&distId=<%=strDistName %>&setId=<%=setId%>" excludedParams="strOpt"  class="its"  > 
			<fmtAuditInfo:message key="LBL_TAG_ID" var="varTagId"/><display:column property="TAG_ID" title="${varTagId}"  />
			<fmtAuditInfo:message key="LBL_CONTROL" var="varControl"/><display:column property="CONTROL_NO" title="${varControl} #"  />
			<fmtAuditInfo:message key="LBL_FLAG" var="varFlag"/><display:column property="FLAG" title="${varFlag}"  />	
			<fmtAuditInfo:message key="LBL_REASON" var="varReason"/><display:column property="REASON" title="${varReason}" />								
			</display:table> 
		<td>
		</tr>
         </table>
         </td>		
	</tr>
	
	<p> &nbsp;</p>
	<tr>
		<td align="center" colspan="6">
		<p> &nbsp;</p>
    <table border="0" class="dttable950" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtAuditInfo:message key="LBL_ASSOCIATED_INCREASE_UNRESOLVED_QTY"/></td>
		</tr>	
         	<tr>
         	<td>
   		  <display:table name="requestScope.frmFlagVarianceForm.alPAssociatedReport" requestURI="/gmFlagVariance.do?auditId=<%=auditId%>&distId=<%=strDistName %>&setId=<%=setId%>" excludedParams="strOpt"  class="its"  > 
			<fmtAuditInfo:message key="LBL_TAG_ID" var="varTagId"/><display:column property="TAG_ID" title="${varTagId}"  />
			<fmtAuditInfo:message key="LBL_CONTROL" var="varControl"//><display:column property="CONTROL_NO" title="${varControl} #"  />
			<fmtAuditInfo:message key="LBL_FLAG" var="varFlag"/><display:column property="FLAG" title="${varFlag}"  />	
			<fmtAuditInfo:message key="LBL_REASON" var="varReason"/><display:column property="REASON" title="${varReason}" />								
			</display:table>  
		<td>
		</tr>
         </table>
         </td>		
	</tr>

</FORM>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>