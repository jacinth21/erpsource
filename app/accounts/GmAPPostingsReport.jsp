<%
/**********************************************************************************
 * File		 		: GmAPPostingsReport.jsp
 * Desc		 		: A/P postings report
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>

<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap, java.util.Calendar" %>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%> 
<%@ taglib prefix="fmtAPPostingReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmAPPostingsReport.jsp -->
<fmtAPPostingReport:setLocale value="<%=strLocale%>"/>
<fmtAPPostingReport:setBundle basename="properties.labels.accounts.GmAPPostingsReport"/>
<bean:define id="returnList" name="frmAPPostings" property="returnList" type="java.util.List"></bean:define>
 
 
<% 
	double invoiceAmtTotal =0;
	 
	String strWikiTitle = GmCommonClass.getWikiTitle("GENERAL_LEDGER_POSTINGS_SUMMARY");
	
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	String strTodaysDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt);
	String strApplDateFmt = strGCompDateFmt;
	String strFromdaysDate = GmCalenderOperations.addMonths(-1, strApplDateFmt);
	//String strFromdaysDate = GmCalenderOperations.getCalculatedDateFromToDate(1, Calendar.MONTH);
	
	HashMap hmCurrency = new HashMap();
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strCurrSign =(String)hmCurrency.get("CMPCURRSMB");
	String strApplCurrFmt = (String)hmCurrency.get("CMPCURRFMT");
	String strCurrPos = GmCommonClass.getString("CURRPOS");
	String strCurrFmt = "{0,number,"+strCurrSign+" "+strApplCurrFmt+"}";
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: A/P Posting Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"	media="print" />
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
var format = '<%=strApplDateFmt%>';
function fnReload()
{     
//	alert(document.frmAPPostings.missingSageVenIds.value);
	var FromDT = document.frmAPPostings.batchDateFrom.value;
	var ToDT = document.frmAPPostings.batchDateTo.value;
	// check correct month format entered
	
    CommonDateValidation(document.frmAPPostings.batchDateFrom, format,message_accounts[277]);
    CommonDateValidation(document.frmAPPostings.batchDateTo, format,message_accounts[278]);
	
	// Validate Order From/To Dates in proper order
	if (dateDiff(FromDT, ToDT,format) < 0)
	{
		Error_Details(message_accounts[279]);
	}
	fnValidateTxtFld('batchDateTo',message_accounts[280]);
	fnValidateTxtFld('batchDateFrom',message_accounts[281]);
	varBatchNumber = document.frmAPPostings.batchNumber.value;
	if(varBatchNumber != "")  
	chkValue(varBatchNumber);
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}	
	document.frmAPPostings.strOpt.value = "reload";
	fnStartProgress();
 	document.frmAPPostings.submit();   
} 
   
function chkValue(value){

   if (isNaN(value))
    {   	
    	Error_Details(message[35]);   	
    }
}

 function fnPostingDetails(strAccID, strRuleID)
 { 
 	  strBatchId = document.frmAPPostings.batchNumber.value;
 	  strDateFrom = document.frmAPPostings.batchDateFrom.value;
 	  strDateTo = document.frmAPPostings.batchDateTo.value; 
      windowOpener("/gmAPPostings.do?strOpt=DtlsFromRpt&batchNumber="+strBatchId+"&batchDateFrom="+strDateFrom+"&batchDateTo="+strDateTo+"&haccountID="+strAccID+"&hruleID="+strRuleID,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=880,height=400");
    }
 function   fnDefaultDate()
 {
 	if(document.frmAPPostings.batchDateTo.value=="")
 	document.frmAPPostings.batchDateTo.value = "<%=strTodaysDate%>"; 
 	if(document.frmAPPostings.batchDateFrom.value=="")
 	document.frmAPPostings.batchDateFrom.value = "<%=strFromdaysDate%>"; 
 }

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10"	onLoad="fnDefaultDate()">
<html:form action="/gmAPPostings.do">
	<html:hidden property="strOpt" value="" />  
	 
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="3"><fmtAPPostingReport:message key="LBL_GENERAL_LEDGER_POSTING"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtAPPostingReport:message key="LBL_HELP" var="varHelp"/>
			 <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		<tr>
			<td bgcolor="#666666" height="1" colspan="4"></td>
		</tr>
		<tr>
			<td width="698" height="100" valign="top" colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">  
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtAPPostingReport:message key="LBL_BATCH_NUMBER"/> :</td> 
                         <td>&nbsp;<html:text property="batchNumber"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;
			    		             </td> 
                    </tr>       
                    <tr><td colspan="4" class="ELine"></td></tr> 
		    <!-- Custom tag lib code modified for JBOSS migration changes -->
			                     <tr>
			                        <td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp; <fmtAPPostingReport:message key="LBL_DATE_FROM"/> :</td> 
				                     <td>&nbsp;
				                     <gmjsp:calendar SFFormName="frmAPPostings" controlName="batchDateFrom"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
				              &nbsp;&nbsp;	<fmtAPPostingReport:message key="LBL_TO"/>:
				              <gmjsp:calendar SFFormName="frmAPPostings" controlName="batchDateTo"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			    		            &nbsp;&nbsp;
			    		             <fmtAPPostingReport:message key="LBL_LOAD" var="varLoad"/>
			    		             <gmjsp:button value="${varLoad}" gmClass="button" buttonType="Load"  onClick="fnReload();" /> </td>
			                    </tr>
                     
		<tr>
			<td class="Line" height="1" colspan="4"></td>
		</tr>
		 
		<tr>
			<td colspan="4">
			<display:table name="requestScope.frmAPPostings.returnList" requestURI="/gmAPPostings.do"  export= "true" class="its" id="currentRowObject" varTotals="totals"  decorator="com.globus.accounts.displaytag.beans.DTAPPostingWrapper" >
			 <display:setProperty name="export.excel.filename" value="A/P Postings Report.xls" />
 		 		<fmtAPPostingReport:message key="LBL_GL_ACCOUNT_NUMBER" var="varGlAccountName"/><display:column property="ACCOUNTID" title="${varGlAccountName}"   />
				<fmtAPPostingReport:message key="LBL_ACCOUNT_NAME" var="varAccountName"/><display:column property="ACCOUNTNAME" title="${varAccountName}"  class="alignleft" /> 
				<fmtAPPostingReport:message key="LBL_DEBIT" var="varDebit"/><display:column property="DEBIT" title="${varDebit}" class="alignright" format="<%=strCurrFmt%>" total="true"/> 
				<fmtAPPostingReport:message key="LBL_CREDIT" var="varCredit"/><display:column property="CREDIT" title="${varCredit}"  class="alignright" format="<%=strCurrFmt%>"  total="true"/> 
				
				
			<display:footer media="html"> 
					<%
					String strVal ; 
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column3").toString();
					String strDebitTotal = strCurrSign +  GmCommonClass.getStringWithCommas(strVal,2);
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column4").toString();
					String strCreditTotal = strCurrSign +  GmCommonClass.getStringWithCommas(strVal,2);
					 				
					%>
				<tr class = shade>
		  		<td colspan="2"> <B> <fmtAPPostingReport:message key="LBL_TOTAL_POSTING"/>: </B></td> 
    	    	 <td class = "alignright" > <B><%=strDebitTotal%></B></td>
    	    	 <td class = "alignright" > <B><%=strCreditTotal%></B></td>
	    	 	 
  				</tr>
  			</display:footer>
				 
			</display:table>
			</td>
		</tr>

		<tr>
			<td class="LLine" height="1" colspan="4"></td>
		</tr>
		 
		 
		 
	</table>
	</FORM>
	 
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>