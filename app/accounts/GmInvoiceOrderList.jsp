 <%
/**********************************************************************************
 * File		 		: GmInvoiceOrderList.jsp
 * Desc		 		: This screen is used to display Account RollForward Report
 * Version	 		: 1.0
 * author			: Richard
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtInoviceOrderList" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmInvoiceOrderList.jsp -->
<fmtInoviceOrderList:setLocale value="<%=strLocale%>"/>
<fmtInoviceOrderList:setBundle basename="properties.labels.accounts.GmInvoiceOrderList"/>
<fmtInoviceOrderList:message key="LBL_CHOOSE_ONE" var="varChooseOne"/>
<%

	
	String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strWikiTitle = GmCommonClass.getWikiTitle("INVOICE_LIST");
	String strApplDateFmt = strGCompDateFmt;
	String strRptFmt = "{0,date,"+strApplDateFmt+"}";
	
	HashMap hmCurrency = new HashMap();
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strApplCurrFmt = GmCommonClass.parseNull((String)hmCurrency.get("CMPCURRFMT"));
	String strCurrSign = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_SYMB"));
	ArrayList alCategory = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCATEGORY"));	
	String strCurrPos = GmCommonClass.parseNull(GmCommonClass.getString("CURRPOS"));
	String strCustCategoryId 	= GmCommonClass.parseZero((String)request.getAttribute("CATEGORYID"));
	String strCategoryRuleId 	= GmCommonClass.parseNull((String)request.getAttribute("CUSTCATEGORY"));
	String strCurrFmt = "";
	if(strCurrPos.equalsIgnoreCase("RIGHT")){
		strCurrFmt = "{0,number,"+strApplCurrFmt+"}";
	}else{
		strCurrFmt = "{0,number,"+strApplCurrFmt+"}";
	}
	
	String strLoadScreen 	= GmCommonClass.parseNull((String)request.getAttribute("hLoadScreen"));
	String strOrderType 	= GmCommonClass.parseNull((String)request.getAttribute("Cbo_OrderType"));
	String strInvSource = GmCommonClass.parseNull((String)request.getAttribute("INVSOURCE"));
	String strIdAccount = GmCommonClass.parseNull((String)request.getAttribute("IDACCOUNT"));
	String strInvoiceType = GmCommonClass.parseNull((String)request.getAttribute("hInvType"));
	String strAccId = GmCommonClass.parseNull((String)request.getAttribute("hAccId"));
	String strInvRepType = GmCommonClass.parseNull((String)request.getAttribute("INVREPTYPE"));
	// to get the Issue memo screen details.
	String strCallFrom = GmCommonClass.parseNull((String)request.getAttribute("hCallFrom"));
    String strSuccessMsg = GmCommonClass.parseNull((String)request.getAttribute("hSuccessMsg"));
    String strDivisionId 	= GmCommonClass. parseZero ((String)request.getAttribute("DIVISIONID"));
    
	ArrayList alInvoiceType = new ArrayList();
	ArrayList alDateType = new ArrayList();
	ArrayList alCompDiv = new ArrayList();
	ArrayList alInvoiceListType = new ArrayList();
	alInvoiceType =  GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("INVOICETYPE"));
	alInvoiceListType =  GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("INVOICELISTTYPE"));
	if(alInvoiceListType.size() !=0){
		alInvoiceType.addAll(alInvoiceListType);
	}
	
	alDateType = (ArrayList)request.getAttribute("DATETYPE");
	alCompDiv = (ArrayList)request.getAttribute("ALCOMPDIV");	
	java.util.Date dtFrm 	= (java.util.Date)request.getAttribute("hFrom");
	java.util.Date dtTo 	= (java.util.Date)request.getAttribute("hTo");
	String strCollectorId = GmCommonClass.parseNull((String)request.getAttribute("COLLECTORID"));
	ArrayList alCollector = (ArrayList)request.getAttribute("ALCOLLECTOR");
	String strCreditHoldType = GmCommonClass.parseNull((String)request.getAttribute("CREDITTYPE"));
	String strParentFl = GmCommonClass.parseNull((String)request.getAttribute("Chk_ParentFl"));
	ArrayList alCreditHoldList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCREDITTYPE"));
	String strAccountCurrency = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_ID"));
	ArrayList alCompCurrency = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCOMPANYCURRENCY"));
	String strDateType = GmCommonClass.parseNull((String)request.getAttribute("hDateType"));
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Sales Orders Report </TITLE>
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strAccountJsPath%>/GmInvoiceOrderList.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
var loadScreen = '<%=strLoadScreen%>';
var orderType  = '<%=strOrderType%>';
var invRepType = '<%=strInvRepType%>';
var callFrom   = '<%=strCallFrom%>';
var parentFl = '<%=strParentFl%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad1()">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmInvoiceOrderListServlet">
<input type="hidden" name="hAccountList" value="">
<input type="hidden" name="hAccountListPlus" value="">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hLoadScreen" value="<%=strLoadScreen %>">
<input type="hidden" name="hSource" value="<%=strInvSource%>">
<input type="hidden" name="hAccId" value="<%=strIdAccount%>">
<input type="hidden" name="hInvId" value="">


	
<table class="DtTable1200"  cellpadding="0" cellspacing="0" border="0">

    <tr>
      <td height="25" class="RightDashBoardHeader" width="90%"><fmtInoviceOrderList:message key="LBL_INVOICE_LIST"/></td> 
      <fmtInoviceOrderList:message key="LBL_HELP" var="varHelp"/>
       <td height="25" class="RightDashBoardHeader" align="Right"><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
    </tr>
<tr>
		<td ></td>
	 <td ></td>
 <tr >

<%if(strCallFrom.equals("IssueMemo") && !strSuccessMsg.equals("")){ %>
    <tr>
			<td colspan="4" align="center" height="30" class="RightTextGreen">
			<b><%=strSuccessMsg %></b>
			</td>
	</tr>
<%}else{%>
	<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->			
	 <tr>
		<td height="25" align="center" colspan="4">
			<jsp:include page="/accounts/GmIncludeAccountType.jsp" >
				<jsp:param name="DISPACCFL" value="Y"/>
				<jsp:param name="REPACCTLABELFL" value="Y"/>
			</jsp:include>
		</td>
		
	</tr>

<tr><td colspan="4" class="Lline"></td></tr><tr>
	<tr class="Shade" height="30">
		<td colspan = "4"><table cellpadding="0" cellspacing="0" width="100%" border="0">
		<tr>
			<td class="RightTableCaption" align="right" width="150"><fmtInoviceOrderList:message key="LBL_SHOW_PARENT_ACCOUNT"/>:</td>
			<td align="left" width="100"><input type="checkbox" size="30" checked="<%=strParentFl%>" name="Chk_ParentFl" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" >	</td>
	 <fmtInoviceOrderList:message  key="LBL_DIVISION_NAME" var="varDivisionName"/>
			<td class="RightTableCaption" align="right" width="200"><gmjsp:label type="RegularText"  SFLblControlName="${varDivisionName}:" td="false"/>&nbsp;</td>
			<td align="left" width="120"><gmjsp:dropdown controlName="Cbo_Division"  seletedValue="<%= strDivisionId %>" value="<%= alCompDiv %>" codeId = "DIVISION_ID" codeName = "DIVISION_NAME" defaultValue= "${varChooseOne}"  />
			</td> 
			<td class="RightTableCaption" align="right" width="200"><fmtInoviceOrderList:message key="LBL_CREDIT_HOLD_TYPE"/>:&nbsp;</td>
			<td align="left" width="120"><gmjsp:dropdown controlName="Cbo_CreditTypeId"  seletedValue="<%=strCreditHoldType%>" value="<%= alCreditHoldList%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "${varChooseOne}" />
					</td>
			<td class="RightTableCaption" align="right"><fmtInoviceOrderList:message key="LBL_CUST_CATEGORY"/>:&nbsp;</td>
			<td align="left"><gmjsp:dropdown controlName="Cbo_Category"  seletedValue="<%= strCustCategoryId %>"  value="<%= alCategory %>" codeId = "CODEID" codeName = "CODENM" defaultValue= "${varChooseOne}"/>&nbsp;&nbsp;&nbsp;</td> 
		</tr>
		</table>
		</td>
	</tr>

	<tr><td colspan="4" class="Lline"></td></tr><tr>
	<tr  height="30">
	<td colspan = "4"><table cellpadding="0" cellspacing="0" width="100%" border="0">
		<tr>
		<td class="RightTableCaption" align="right" width="150"><fmtInoviceOrderList:message key="LBL_DATE_TYPE"/>:&nbsp;</td>
		<td align="left" width="100"><gmjsp:dropdown controlName="Cbo_DateType" seletedValue="<%=strDateType%>" value="<%= alDateType%>" codeId = "CODEID"  codeName = "CODENM" /></td>
		<fmtInoviceOrderList:message key="LBL_FROM_DATE" var="varFromDate"/>
		<td class="RightTableCaption" align="right" width="200"><gmjsp:label type="RegularText"  SFLblControlName="${varFromDate}:" td="false"/>&nbsp;</td>
		<td align="left" width="120"><gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=(dtFrm==null)?null:new java.sql.Date(dtFrm.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
		</td>
		<fmtInoviceOrderList:message key="LBL_TO_DATE" var="varToDate"/>
		<td class="RightTableCaption" align="right" width="200"><gmjsp:label type="RegularText"  SFLblControlName="${varToDate}:" td="false"/>&nbsp;</td>
		<td align="left" width="120"><gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=(dtTo==null)?null:new java.sql.Date(dtTo.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
		</td>
		 <td></td> 
		 <td></td>
		</tr>
		</table>
		</td>
	</tr>
	<tr><td colspan="4" class="Lline"></td></tr>
	<tr class="Shade" height="30">
		<td class="RightTablecaption" height="24" colspan="4">
		<table cellpadding="0" cellspacing="0" width="100%"  border="0">
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
		<td class="RightTableCaption" align="right" width="150"><fmtInoviceOrderList:message key="LBL_COLLECTOR_ID"/>:&nbsp;</td>
	    <td align="left" width="100"><gmjsp:dropdown controlName="Cbo_CollectorId"  seletedValue="<%=strCollectorId%>" value="<%= alCollector%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "${varChooseOne}" /></td>
			
			<fmtInoviceOrderList:message key="LBL_REPORT_TYPE" var="varReportType"/>
			<td class="RightTablecaption" align="right" width="200"><gmjsp:label type="RegularText"  SFLblControlName="${varReportType}:" td="false"/>&nbsp;</td>
			<td align="left" width="120"><select name="Cbo_InvRepType" class="RightText" OnChange="fnChange(this.value)" >
							<option value="0"><fmtInoviceOrderList:message key="LBL_CHOOSE_ONE"/>
							<option value="1"><fmtInoviceOrderList:message key="LBL_OPEN"/>
							<option value="2"><fmtInoviceOrderList:message key="LBL_CLOSED"/> 
			</select> 
			</td>
			
		<fmtInoviceOrderList:message key="LBL_INVOICE_TYPE" var="varInvoiceType"/>
		<td  class="RightTablecaption" align="right" width="200"><gmjsp:label type="RegularText"  SFLblControlName="${varInvoiceType}:" td="false"/>&nbsp;</td>
		<td align="left" width="120"><gmjsp:dropdown controlName="Cbo_InvoiceType"  seletedValue="<%= strInvoiceType %>" value="<%= alInvoiceType%>" codeId = "CODEID" codeName = "CODENM" defaultValue= "${varChooseOne}"  />
			</td>
			<fmtInoviceOrderList:message key="BTN_LOAD" var="varLoad"/>
     		<td align="center"><gmjsp:button value="${varLoad}" name="Btn_Go" gmClass="button"  onClick="javascript:fnSubmit();" buttonType="Load" />
			</td>
		</tr>
		</table>
		</td>
	</tr>
	<tr><td colspan="4" class="Lline" height="24"></td></tr><tr>

<%} %>
<tr><td colspan="4">
<table cellpadding="0" cellspacing="0" border="0">
	<tr><td colspan="4" class="Lline"></td></tr>
	<tr>
		<td height="30" class="aligncenter">

	<display:table name="requestScope.results.rows" export="true" id="currentRowObject" class="its" varTotals ="invoicetotal" decorator="com.globus.displaytag.beans.DTInvoiceOrderWrapper">
	<display:setProperty name="export.excel.filename" value="Invoice List Report.xls" />
	  <fmtInoviceOrderList:message key="LBL_INVOICE_DATE" var="varInvoiceDate"/>	<display:column property="INV_DATE" title="${varInvoiceDate}" sortable="true" format="<%=strRptFmt%>"/>
	  <fmtInoviceOrderList:message key="LBL_INVOICE_ID" var="varInvoiceId"/><display:column property="INV_ID" title="${varInvoiceId}" sortable="true" />
	  <fmtInoviceOrderList:message key="LBL_CUSTOMER_PO" var="varCustomerPO"/>	<display:column property="CUSTOMER_PO" title="${varCustomerPO}" style="width:6opx" sortable="true"/>
	  <fmtInoviceOrderList:message key="LBL_INVOICE_TYPE" var="varInvoiceType"/>	<display:column property="INV_TYP" title="${varInvoiceType}"  style="width:100px" sortable="true"  />  
	  <% if(strInvSource.equals("26240213")){ //26240213:Company Dealers  %>
      <fmtInoviceOrderList:message key="LBL_DEALER_ID" var="varDealerId"/>	<display:column property="ACCT_ID" title="${varDealerId}" sortable="true"/> 
	  <fmtInoviceOrderList:message key="LBL_DEALER_NAME" var="varDealerName"/><display:column property="NAME" title="${varDealerName}"  sortable="true" />           
	  <%}else{%>
	  <fmtInoviceOrderList:message key="LBL_ACCOUNT_ID" var="varAccountId"/>	<display:column property="ACCT_ID" title="${varAccountId}" sortable="true"/> 
	  <fmtInoviceOrderList:message key="LBL_REP_ACCOUNT_NAME" var="varRepAccountName"/><display:column property="NAME" title="${varRepAccountName}"  sortable="true" />              
	  <fmtInoviceOrderList:message key="LBL_REP_NAME" var="varRepName"/><display:column property="REPNAME" title="${varRepName}"  sortable="true" />
	  <%}%>
	   <fmtInoviceOrderList:message key="LBL_DIVISION_NAME" var="varDivName"/>	<display:column property="DIVISION_NAME" class="alignleft" title="${varDivName}" sortable="true"/> 	   
	  <% if(strCategoryRuleId.equals("Y")){  %>
		   <fmtInoviceOrderList:message key="LBL_CATEGORY" var="varCategory"/>	<display:column property="CUSTCATEGORY" class="alignleft" title="${varCategory}" sortable="true"/> 
	  <%}%>   
	  <fmtInoviceOrderList:message key="LBL_CREDIT_HOLD_TYPE" var="varCredutHoldType"/>	<display:column property="CREDITTYPE_NAME" class="alignleft" title="${varCredutHoldType}" sortable="true"/>		  	
	  <fmtInoviceOrderList:message key="LBL_PAYMENT_TERMS" var="varPaymentTerms"/>	<display:column property="PAYMENT_TERMS" title="${varPaymentTerms}" style="width:100px" sortable="true" />	  	 
	  <fmtInoviceOrderList:message key="LBL_COLLECTOR_ID" var="varCollectorId"/>	<display:column property="COLLECTOR_NAME" title="${varCollectorId}" sortable="true" />
	  <fmtInoviceOrderList:message key="LBL_CURRENCY" var="varCurrency"/>	<display:column property="CURRENCY" title="${varCurrency}" sortable="true" style="text-align: center; "/>
	  <fmtInoviceOrderList:message key="LBL_ORDER_AMOUNT" var="VarOrderAmount"/>	<display:column property="ORDER_AMT" class="alignright" title="${VarOrderAmount}" sortable="true" total = "true"  format="<%=strCurrFmt%>" />
	  <fmtInoviceOrderList:message key="LBL_SHIPPING_CHARGES" var="varShippingCharges"/>	<display:column property="SHIP_AMT" class="alignright" title="${varShippingCharges}" sortable="true" total = "true"  format="<%=strCurrFmt%>" />
	  <fmtInoviceOrderList:message key="LBL_TAX_AMOUNT" var="varTaxAmount"/>	<display:column property="TAX_AMT" class="alignright" title="${varTaxAmount}" sortable="true" total = "true"   />
	  <fmtInoviceOrderList:message key="LBL_INVOICE_AMOUNT" var="varInoviceAmount"/>	<display:column property="INV_AMT" class="alignright" title="${varInoviceAmount}" sortable="true"  total = "true" />
	  <fmtInoviceOrderList:message key="LBL_PAYMENT_AMOUNT" var="varPaymentAmount"/>	<display:column property="PAYMENT_AMT" class="alignright" title="${varPaymentAmount}" sortable="true" total = "true" format="<%=strCurrFmt%>" />
	  <fmtInoviceOrderList:message key="LBL_CRETED_BY" var="varCreatedBy"/>	<display:column property="CREATED_BY" title="${varCreatedBy}" style="width:120px" sortable="true" />   
	  <fmtInoviceOrderList:message key="LBL_CREATED_DATE" var="varCreatedDate"/>	<display:column property="CREATED_DATE" class="alignright" title="${varCreatedDate}" sortable="true" format="<%=strRptFmt%>"/>	  		  	
	  	<display:footer media="html"> 
<% 
	HashMap temp = ((HashMap)pageContext.getAttribute("invoicetotal"));
	log.debug(" Total value is " + temp);
	String strOrdAmt = "";
	String strShipAmt = "";
	String strTaxAmt = "";
	String strInvAmt = "";
	String strPayAmt = "";
	if(strCategoryRuleId.equals("Y")){
		//PC-2061: Add rep name mapped to account column in Invoice List 
		if(strInvSource.equals("26240213")){ //26240213:Company Dealers  
			strOrdAmt =  ((HashMap)pageContext.getAttribute("invoicetotal")).get("column13").toString();
			strShipAmt =  ((HashMap)pageContext.getAttribute("invoicetotal")).get("column14").toString();
			strTaxAmt =  ((HashMap)pageContext.getAttribute("invoicetotal")).get("column15").toString();
  			strInvAmt =  ((HashMap)pageContext.getAttribute("invoicetotal")).get("column16").toString();
  			strPayAmt =  ((HashMap)pageContext.getAttribute("invoicetotal")).get("column17").toString();
		}else{
			strOrdAmt =  ((HashMap)pageContext.getAttribute("invoicetotal")).get("column14").toString();
			strShipAmt =  ((HashMap)pageContext.getAttribute("invoicetotal")).get("column15").toString();
			strTaxAmt =  ((HashMap)pageContext.getAttribute("invoicetotal")).get("column16").toString();
  			strInvAmt =  ((HashMap)pageContext.getAttribute("invoicetotal")).get("column17").toString();
  			strPayAmt =  ((HashMap)pageContext.getAttribute("invoicetotal")).get("column18").toString();
		}
	}else{
		if(strInvSource.equals("26240213")){ //26240213:Company Dealers  
			strOrdAmt =  ((HashMap)pageContext.getAttribute("invoicetotal")).get("column12").toString();
			strShipAmt =  ((HashMap)pageContext.getAttribute("invoicetotal")).get("column13").toString();
			strTaxAmt =  ((HashMap)pageContext.getAttribute("invoicetotal")).get("column14").toString();
  			strInvAmt =  ((HashMap)pageContext.getAttribute("invoicetotal")).get("column15").toString();
  			strPayAmt =  ((HashMap)pageContext.getAttribute("invoicetotal")).get("column16").toString();
		}else{
			strOrdAmt =  ((HashMap)pageContext.getAttribute("invoicetotal")).get("column13").toString();
			strShipAmt =  ((HashMap)pageContext.getAttribute("invoicetotal")).get("column14").toString();
			strTaxAmt =  ((HashMap)pageContext.getAttribute("invoicetotal")).get("column15").toString();
  			strInvAmt =  ((HashMap)pageContext.getAttribute("invoicetotal")).get("column16").toString();
  			strPayAmt =  ((HashMap)pageContext.getAttribute("invoicetotal")).get("column17").toString();
		}
	}

%>	<tr class = shade>
  	<% if(strCategoryRuleId.equals("Y")){ 
  			if(strInvSource.equals("26240213")){ //26240213:Company Dealers %>
				<td colspan = "12" align="right"> <B> <fmtInoviceOrderList:message key="LBL_TOTAL"/> : </B></td>
	   <%}else{ %>
	   		<td colspan = "13" align="right"> <B> <fmtInoviceOrderList:message key="LBL_TOTAL"/> : </B></td>
	   	<%} %>
		
	<%}else{ 
		if(strInvSource.equals("26240213")){ //26240213:Company Dealers %>
			<td colspan = "11" align="right"> <B> <fmtInoviceOrderList:message key="LBL_TOTAL"/> : </B></td>
	   <%}else{ %>
	   		<td colspan = "12" align="right"> <B> <fmtInoviceOrderList:message key="LBL_TOTAL"/> : </B></td>
	   	<%}} %>
		  		<td  Height="25"   class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strOrdAmt)%></td>
		  		<td  Height="25"   class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strShipAmt)%></td>
		  		<td  Height="25"   class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strTaxAmt)%></td>
		  		<td  Height="25"   class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strInvAmt)%></td>
		  		<td  Height="25"   class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strPayAmt)%></td>
		  		<td colspan = "2"> </td>
	</tr>
 </display:footer>
	  	
	  	
	</display:table>
 		</td>
	</tr>
</table>
</td></tr>
</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
