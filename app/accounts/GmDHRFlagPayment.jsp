 <%
/**********************************************************************************
 * File		 		: GmInvoiceMultiplePayment.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@page import="java.util.Date"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="org.apache.commons.beanutils.RowSetDynaClass"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- Imports for Logger -->

<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ include file="/common/GmHeader.inc" %>						
<%@ taglib prefix="fmtDhrFlagPayment" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmDHRFlagPayment.jsp -->
<fmtDhrFlagPayment:setLocale value="<%=strLocale%>"/>
<fmtDhrFlagPayment:setBundle basename="properties.labels.accounts.GmDHRFlagPayment"/>
<%


	String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);


	int intSize = 0;
	HashMap hmLoop = new HashMap();
	HashMap hcboVal = null;

	String strSelected = "";
	String strAction = (String)request.getAttribute("hAction");
	String strShade 	= "";
	String strCodeID 	= "";	
	
	String strDisplableValue = "";
	String strApplDateFmt = strGCompDateFmt;
	String strDateFmt = "{0,date,"+strApplDateFmt+"}";
	
	ArrayList alVendorList = new ArrayList();
	ArrayList alDHR = new ArrayList();
	ArrayList alPoType =  new ArrayList();
	ArrayList alInvcStatus =  new ArrayList();
	ArrayList alInvcStatusVal =  new ArrayList();
	ArrayList alDateType =  new ArrayList();
	ArrayList alDHRType =  new ArrayList();
	
	alPoType = (ArrayList)request.getAttribute("alPOTYPE");
	alInvcStatus = (ArrayList)request.getAttribute("alInvcStatus");
	alInvcStatusVal = (ArrayList)request.getAttribute("alInvcStatusVal");
	alDHRType = (ArrayList)request.getAttribute("alDHRType");
	
	alDateType = (ArrayList)request.getAttribute("alDateType");
	int intLength = 0;

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	
		
	if (strAction.equals("Load") || strAction.equals("Reload"))
	{
		alVendorList = (ArrayList)hmReturn.get("VENDORLIST");
		
		if (strAction.equals("Reload"))
		{
			//strAccId = (String)request.getAttribute("hAccId");
		//	alDHR = (ArrayList)hmReturn.get("DHRLIST");
		}
	}
	// Filter Variable 
	String strFromDt 	= GmCommonClass.parseNull((String)request.getAttribute("hFromDate")) ;
	String strToDt 		= GmCommonClass.parseNull((String)request.getAttribute("hToDate"));
	Date dtFrom  		= GmCommonClass.getStringToDate(strFromDt, strApplDateFmt);
	Date dtTO           = GmCommonClass.getStringToDate(strToDt, strApplDateFmt);
	String strPartNums 	= GmCommonClass.parseNull((String)request.getAttribute("hPartNum"));
	String strVendorID	= GmCommonClass.parseNull((String)request.getAttribute("hVendID"));
	String strSortCName = GmCommonClass.parseNull((String)request.getAttribute("hSort"));
	String strSortType 	= GmCommonClass.parseNull((String)request.getAttribute("hSortAscDesc"));
	String strPOID 		= GmCommonClass.parseNull((String)request.getAttribute("hPOId"));	
	String strWOID 		= GmCommonClass.parseNull((String)request.getAttribute("hWOId"));	
	String strDHRId		= GmCommonClass.parseNull((String)request.getAttribute("hDHRId"));	
	String strType 		= GmCommonClass.parseNull((String)request.getAttribute("Cbo_Type"));
	
	//We do not have to default to any value.
	//strType = strType.equals("")?"1":strType;
	String strPoType = GmCommonClass.parseNull((String)request.getAttribute("hPoType"));
	
	String strInvcStatus		= GmCommonClass.parseNull((String)request.getAttribute("hInvcType"));
	String strInvcStatusVal		= GmCommonClass.parseNull((String)request.getAttribute("hInvcTypeVal"));
	String strDateType		= GmCommonClass.parseNull((String)request.getAttribute("hType"));
	
	//When the DHR TYpe is DHR Verified No Inv, we should disable the Invoice Type Dropdown.
	if (strType.equals("103301")) 
		strDisplableValue = "Disabled";

	HashMap hmCurrency = new HashMap();
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strCurrSign =(String)hmCurrency.get("CMPCURRSMB");
	String strApplCurrFmt = (String)hmCurrency.get("CMPCURRFMT");
	String strCurrPos = GmCommonClass.getString("CURRPOS");
	String strCurrFmt = "{0,number,"+strCurrSign+" "+strApplCurrFmt+"}";
	String strNumberFmt = "{0,number,#,###,###}";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: DHR Flag Payment</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
	<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmDHRPayment.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script> 
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     #Cbo_InvoiceType{"width:54%;"}
     #Cbo_InvoiceOper{"width:54%;"}
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnLoad();" onkeypress="fnEnter();">
<FORM name="frmDHRFlagPayment" method="POST" action="<%=strServletPath%>/GmDHRFlagPaymentServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hDHRId" value="">
<input type="hidden" name="hVenId" value="">
<input type="hidden" name="hSort" value="<%=strSortCName%>">
<input type="hidden" name="hSortAscDesc" value="<%=strSortType%>">
<input type="hidden" name="hType" value="<%=strType%>">
<input type="hidden" name="hInvcType" value="">
<input type="hidden" name="hInvcTypeVal" value="">
<input type="hidden" name="hOperatorSign" value="">
<input type="hidden" name="hDateType" value="">

	<table class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr><td height="25" class="RightDashBoardHeader"><fmtDhrFlagPayment:message key="LBL_DHR_FLAG_PAYMENT"/></td></tr>
		<tr><td height="1" class="Line"></td></tr>
		<tr>
			<td>
				<table border="0" width="100%"  cellspacing="0" cellpadding="0">
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr height="25">
						<td class="RightTableCaption" align="right"><fmtDhrFlagPayment:message key="LBL_VENDOR"/>:</td>
						<td class="RightText"  colspan="2" style="padding-left: 5px;"><gmjsp:dropdown controlName="Cbo_VendorId"  seletedValue="<%=strVendorID%>"  defaultValue= "[All Vendors]"	
							tabIndex="1"  value="<%=alVendorList%>" codeId="ID" codeName="NAME"/></td>
						<td class="RightTableCaption" align="right" > </td>
						 
						<td class="RightTableCaption" align="right" style="width: 8%;"><fmtDhrFlagPayment:message key="LBL_PART_NUMEBR"/>:</td>
						<td style="width: 20%; padding-left: 5px;"><input type="text" class="RightText"  name="Txt_PARTID"  value="<%=strPartNums%>" onBlur="changeBgColor(this,'#ffffff');" onFocus="changeBgColor(this,'#AACCE8');" class="InputArea" size="30" tabindex="2" /></td>
						<td class="LLine" rowspan="9" width="1"></td>
						<fmtDhrFlagPayment:message key="BTN_GO" var="varGo"/>
						<td align="center" rowspan="9"><gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" gmClass="button" onClick="fnGo();" tabindex="9" buttonType="Load" />
					</tr>
					<tr><td colspan="6" height="1" class="LLine"></td></tr>
					<tr class="Shade" height="25">
						<td class="RightTableCaption" align="right" style="width: 6%;white-space: nowrap;"><fmtDhrFlagPayment:message key="LBL_PO_TYPE"/>:</td> 
						<td class="RightText" colspan="2" style="padding-left: 5px;"><gmjsp:dropdown controlName="Cbo_PoType"  seletedValue="<%= strPoType %>" 	
							tabIndex="2"  width="150" value="<%= alPoType%>" codeId = "CODEID" codeName = "CODENM" defaultValue= "[Choose One]"  /></td>
							<td class="RightTableCaption" align="right" > </td>
						 
						<td class="RightTableCaption" align="right"><fmtDhrFlagPayment:message key="LBL_PO_ID"/>:</td>
						<td>&nbsp;<input type="text" class="RightText"  tabindex="4" name="Txt_PORDER" onBlur="changeBgColor(this,'#ffffff');" onFocus="changeBgColor(this,'#AACCE8');" value="<%=strPOID%>" class="InputArea" size="20"/></td>
					</tr>
					<tr><td colspan="6" height="1" class="LLine"></td></tr>
					<tr height="24">
						<td class="RightTableCaption" align="right" style="width: 6%;white-space: nowrap;"><fmtDhrFlagPayment:message key="LBL_DHR_TYPE"/>:</td>
						<td class="RightText">&nbsp;<gmjsp:dropdown controlName="Cbo_Type" onChange="fnCheckOption();"	  seletedValue="<%=strType%>"
									 value="<%=alDHRType%>"   codeId = "CODEID" codeName = "CODENM" defaultValue= "[Choose One]" />
					 	</td>
					 	 <td class="RightTableCaption" align="right" HEIGHT="24" style="width: 12%;position: relative;padding-right: 10px;">  <fmtDhrFlagPayment:message key="LBL_INVOICE_TYPE"/> :</td> 
		                     <td style="display: inline-block;width: 100%;margin-top: 3px;white-space: nowrap;">&nbsp;&nbsp;<gmjsp:dropdown controlName="Cbo_InvoiceType" onChange="fnSetInvOperator();"		  seletedValue="<%=strInvcStatus%>"
									 value="<%=alInvcStatus%>"   codeId="CODEID" codeName="CODENM"  defaultValue= "[Choose One]" disabled="<%=strDisplableValue%>" />
									 &nbsp;<gmjsp:dropdown controlName="Cbo_InvoiceOper"	  seletedValue="<%=strInvcStatusVal%>"
									 value="<%=alInvcStatusVal%>"   codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" disabled="<%=strDisplableValue%>"  />
	    		             </td> 
						<td class="RightTableCaption" align="right"><fmtDhrFlagPayment:message key="LBL_DHR_ID"/>:</td> 
						<td class="RightText">&nbsp;<input type="text" class="RightText"  tabindex="3" name="Txt_DHR" onBlur="changeBgColor(this,'#ffffff');" onFocus="changeBgColor(this,'#AACCE8');"  value="<%=strDHRId%>" class="InputArea" size="20"/></td>
					</tr>
					<tr><td colspan="6" height="1" class="LLine"></td></tr>
					<tr class="Shade" HEIGHT="25">
						<td class="RightTableCaption" align="right"> <fmtDhrFlagPayment:message key="LBL_DATE"/>:</td>
						<td class="RightText" colspan="3">&nbsp;<fmtDhrFlagPayment:message key="LBL_FROM_DATE"/>:&nbsp;<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=(dtFrom==null)?null:new java.sql.Date(dtFrom.getTime())%>"  
	                      gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
	                       onBlur="changeBgColor(this,'#ffffff');"/>
							&nbsp;&nbsp;&nbsp;<fmtDhrFlagPayment:message key="LBL_TO_DATE"/>:&nbsp;<gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=(dtTO==null)?null:new java.sql.Date(dtTO.getTime())%>"  
	                      gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
	                       onBlur="changeBgColor(this,'#ffffff');"/>
							 &nbsp;&nbsp;<gmjsp:dropdown controlName="Cbo_DateType"	  seletedValue="<%=strDateType%>"
									 value="<%=alDateType%>"   codeId = "CODEID"  codeName = "CODENM" />
						</td>
						  
						<td class="RightTableCaption" align="Right" style="width: 9%;white-space: nowrap;"><fmtDhrFlagPayment:message key="LBL_WORK_ORDER_ID"/>:</td>
						<td>&nbsp;<input type="text" class="RightText"  tabindex="5" name="Txt_WORDER" onBlur="changeBgColor(this,'#ffffff');" onFocus="changeBgColor(this,'#AACCE8');" value="<%=strWOID%>" class="InputArea" size="20"/></td>
					</tr>		
				</table>
			</td>
		</tr>
		<tr><td height="1" class="LLine"></td></tr>
		<tr> 
			<td align="center" height="50">
				<display:table name="requestScope.DHRLIST.rows" class="its" id="currentRowObject" defaultsort="3" varTotals="dhrtotals" freezeHeader="true" paneSize="500" export = "true" decorator="com.globus.displaytag.beans.DTFlagDHRWrapper" > 
                    <display:setProperty name="export.excel.filename" value="DHR Flag Payment.xls" />  
					<display:setProperty name="export.excel.decorator" value="com.globus.displaytag.beans.DTFlagDHRWrapper" /> 	
					<fmtDhrFlagPayment:message key="LBL_PART_NUMEBER" var="varPartNumber"/><display:column property="PNUM" title="${varPartNumber}" sortable="true" class="alignleft"/>
					<fmtDhrFlagPayment:message key="LBL_VENDOR" var="varVendor"/><display:column property="VENDNM" title="${varVendor}" sortable="true" class="alignleft"/>
					<fmtDhrFlagPayment:message key="LBL_DHR_ID" var="varDhrId"/><display:column property="IDHLINK" title="${varDhrId}" class="aligncenter" style="white-space: nowrap" media="html"/> <!-- Using ID alias Name to display the View Icon for corrresponding Action-->
					<fmtDhrFlagPayment:message key="LBL_DHR_ID"var="varDhrId"/><display:column property="ID" title="${varDhrId}" class="aligncenter" style="white-space: nowrap" media="excel"/>
					<fmtDhrFlagPayment:message key="LBL_WORK_ORDER" var="varWorkOrder"/><display:column property="WORKID" title="${varWorkOrder}" sortable="true" class="aligncenter" style="white-space: nowrap" />
					<fmtDhrFlagPayment:message key="LBL_PURCHASE_ORDER" var="varPurchaseOrder"/><display:column property="POIDHLINK" title="${varPurchaseOrder}" sortable="true" class="aligncenter" style="white-space: nowrap" media="html"/>
					<fmtDhrFlagPayment:message key="LBL_PURCHASE_ORDER" var="varPurchaseOrder"/><display:column property="POID" title="${varPurchaseOrder}" sortable="true" class="aligncenter" style="white-space: nowrap" media="excel"/>
					<fmtDhrFlagPayment:message key="LBL_QTY_REC" var="varQtyRec"/><display:column property="QTYREC" title="${varQtyRec}" sortable="true"  class="alignright" format="<%=strNumberFmt%>" />
					<fmtDhrFlagPayment:message key="LBL_SHELF_QTY" var="varShelfQty"/><display:column property="SHELFQTY" title="${varShelfQty}" sortable="true"  class="alignright" format="<%=strNumberFmt%>" />
					<fmtDhrFlagPayment:message key="LBL_COST_EA" var="varCostEA"/><display:column property="USDCOST" title="${varCostEA}" sortable="true"  class="alignright" format="<%=strCurrFmt%>" total="true"/>
					<fmtDhrFlagPayment:message key="LBL_UOM_BR_QTY" var="varUomBrQty"/><display:column property="UOM" title="${varUomQtty}" sortable="true"  class="aligncenter" media="html"/>
					<fmtDhrFlagPayment:message key="LBL_UOM_QTY" var="varUomQtty"/><display:column property="UOM" title="${varUomQtty}" sortable="true"  class="aligncenter" media="excel"/>
					<fmtDhrFlagPayment:message key="LBL_TOTAL_COST" var="varTotalCost"/><display:column property="USDTCOST" title="${varTotalCost}" sortable="true"   class="alignright" format="<%=strCurrFmt%>"  total="true" />
					<fmtDhrFlagPayment:message key="LBL_RECD_BR_DATE" var="varRecdBrDate"/><display:column property="CDATE" title="${varRecdBrDate}" sortable="true" class="alignleft" media="html" format= "<%=strDateFmt%>" />
					<fmtDhrFlagPayment:message key="LBL_RECD_DATE" var="varRecdDate"/><display:column property="CDATE" title="${varRecdDate}" sortable="true" class="alignleft" media="excel" format= "<%=strDateFmt%>"/>
					<fmtDhrFlagPayment:message key="LBL_VER_BR_DATE" var="varVerBrDate"/><display:column property="VDATE" title="${varVerBrDate}" sortable="true" class="alignleft" media="html" format= "<%=strDateFmt%>"/>
					<fmtDhrFlagPayment:message key="LBL_VER_DATE" var="varVerDate"/><display:column property="VDATE" title="${varVerDate}" sortable="true" class="alignleft" media="excel" format= "<%=strDateFmt%>"/>
					<fmtDhrFlagPayment:message key="LBL_PACK_SLIP" var="varPackSlip"/><display:column property="PSLIP" title="${varPackSlip}" sortable="true" class="alignleft" />
					<fmtDhrFlagPayment:message key="LBL_INVOICE_ID" var="varInoviceId"/><display:column property="INVID" title="${varInoviceId}" sortable="true" class="alignleft" />
					<fmtDhrFlagPayment:message key="LBL_PO_TYPE" var="varPoType"/><display:column property="POTYPE" title="${varPoType}" sortable="true" class="aligncenter" />
					<fmtDhrFlagPayment:message key="LBL_STATUS" var="varStatus"/><display:column property="SFL" title="${varStatus}" sortable="true" class="alignleft"/>
					<display:footer media="html"> 
<% 
		String strVal ;
		strVal = ((HashMap)pageContext.getAttribute("dhrtotals")).get("column8").toString();
  		String strCost = strCurrSign + GmCommonClass.getStringWithCommas(strVal);
  		strVal = ((HashMap)pageContext.getAttribute("dhrtotals")).get("column10").toString();
  		String stTotrCost = strCurrSign + GmCommonClass.getStringWithCommas(strVal);
  		
%>
					  	<tr class=shade height="23">
					  		<td colspan="7"><B><fmtDhrFlagPayment:message key="LBL_TOTAL"/> :</B></td>
					  		<td nowrap class="alignright"><B><%=strCost%></B></td>
					  		<td>&nbsp;</td>
					  		<td nowrap class="alignright"><B><%=stTotrCost%></B></td>
					  		<td colspan="6"> </td>
					  	</tr>
					 </display:footer>
				 </display:table> 
			</td> 
		</tr>
    </table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
