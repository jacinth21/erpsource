 <%
/**********************************************************************************
 * File		 		: GmInvoiceMultiplePayment.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.TimeZone"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>

<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtInvoiceStatement" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmInvoiceStatement.jsp -->
<fmtInvoiceStatement:setLocale value="<%=strLocale%>"/>
<fmtInvoiceStatement:setBundle basename="properties.labels.accounts.GmInvoiceStatement"/>
<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
	String strWikiTitle = GmCommonClass.getWikiTitle("POST_PAYMENTS");
	String strApplDateFmt = strGCompDateFmt;
	// getting the customer date format based on Currency symbol
	String strAccCurrencyDateFmt = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_DATEFMT"));
	strApplDateFmt = strAccCurrencyDateFmt.equals("")? strApplDateFmt : strAccCurrencyDateFmt;
	
	String strCurDate = GmCalenderOperations.getCurrentDate(strApplDateFmt);
	String strCurrSymbol = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_SYMB"));
	String strAccountIds = GmCommonClass.parseNull((String)request.getAttribute("REPACTID"));
	String strParentFl = GmCommonClass.parseNull((String)request.getAttribute("Chk_ParentFl"));	
	String strMulitiAcc =  "";
	String strCompId = gmDataStoreVO.getCmpid();
	String strCompanyLocale = GmCommonClass.getCompanyLocale(strCompId);
	GmResourceBundleBean gmResourceBundleBean =	GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
	String strShowVendorNo = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("INVOICE_STAMT.SHOW_VENDOR_NO"));
		
	int intSize = 0;
	HashMap hmLoop = new HashMap();
	HashMap hcboVal = null;
	String strCodeID = "";
	String strDistId = "";
	String strSelected = "";

	String strInvId = "";
	String strInvDt = "";
	String strPayDt = "";
	String strOverdue = "";
	double dbInvAmt = 0.00;
	double dbInvOverdue;
	double dbShipAmt = 0.00;
	double dbPendAmt = 0.00;
	String strCustPO = "";
	String strAccId = "";
	String strAmtPaid = "";
	String strDiscount = "";

	String strAction = (String)request.getAttribute("hAction");
	String strStatusFl = "";

	String strShade = "";
	String strCallFlag = "";
	String strInvType = "";
	
	ArrayList alAcctInvoice = new ArrayList();
	ArrayList alAccount = new ArrayList();
	ArrayList alPayModes = new ArrayList();
	
	ArrayList alInvTypes = new ArrayList();
	int intInvTypeSize = 0;
	HashMap hmInvType = null;
	String strInvTypeCodeID = "";
	String strInvTypeCodeNM ="";
	String strInvTypeId = "";
	String strBillAdd="";
	int intQty = 0;
	int intLength = 0;

	HashMap hmReturn = new HashMap();
	HashMap hmParam = new HashMap();
	
	String strAccID_PAYALL = "";
	String strInvID_PAYALL = "";
	String strCustPO_PAYALL = "";
	String strInvAmt_PAYALL = "";	
	String strInvType_PAYALL = "";
	String strSortCName = "";
	String strSortType = "";
	String strCurrency= "";
	String strCurrencySymbol = "";
	String strCompanyID = ""; // default Globus.
	String strCompanyName = "";
	String strCompanyAddress = "";
	String strCompayLogo = "";
	String strGrpAccAddr= "";
	String strAccountCurrSign = "";
	String straddress = "";
	String strGMName = "";
	String strGMphone = "";
	String strTotAmt = "";	
	String strTotOut = "";
	HashMap hmHeaderDtls = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HEADERDETAILS"));
	String strPaymentTerm = GmCommonClass.parseNull((String)request.getAttribute("PAYMENTTERM"));	
	String strCreditHldMsg = GmCommonClass.parseNull((String)request.getAttribute("CREDITHOLDMSG"));	
	String strAddToJasper = "";
	String strDivisionId = "";
	String strVendorId = "";
	
    HashMap hmCompanyAddress = new HashMap ();
    strDivisionId = GmCommonClass.parseNull((String)hmHeaderDtls.get("DIVISION_ID"));
    strDivisionId = strDivisionId.equals("")?"2000":strDivisionId;
	hmCompanyAddress = GmCommonClass.fetchCompanyAddress(gmDataStoreVO.getCmpid(), strDivisionId);
	if(!hmHeaderDtls.isEmpty()){
		strCompanyID = GmCommonClass.parseNull((String)hmHeaderDtls.get("COMPANYID"));
		strCurrencySymbol = GmCommonClass.parseNull((String)hmHeaderDtls.get("CURRENCY"));
		strCompanyID = strCompanyID.equals("")?"100800":strCompanyID;
		strCompanyName = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMPNAME"));
		strCompanyAddress = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMPADDRESS"));
		String strCompanyPhone = GmCommonClass.parseNull((String)hmCompanyAddress.get("PH"));
		strCompanyAddress = "&nbsp;" + strCompanyAddress.replaceAll("/", "\n<br>&nbsp;");
		strCompanyAddress = strCompanyAddress+"<br>&nbsp;&nbsp;"+strCompanyPhone;
	}	
	strCompayLogo = GmCommonClass.parseNull((String)hmCompanyAddress.get("LOGO"))+".gif";
	
    String strHeadAddress = "<br>&nbsp;&nbsp;<b>" + strCompanyName + "</b><BR>&nbsp;" + strCompanyAddress;
	String strInvSource = GmCommonClass.parseNull((String)request.getAttribute("INVSOURCE"));
	String strIdAccount = GmCommonClass.parseNull((String)request.getAttribute("IDACCOUNT"));
	String strMode = GmCommonClass.parseNull((String)request.getAttribute("hMode"));
	String strType = GmCommonClass.parseNull((String)request.getAttribute("strType"));
	String strStatementType = GmCommonClass.parseNull((String)request.getAttribute("strStatementType"));
	String strInvStmntJasNm = GmCommonClass.parseNull((String)request.getAttribute("INVSTMNTJAS"));
	String strPrintAccess = GmCommonClass.parseNull((String)request.getAttribute("hAccess"));
	ArrayList alType = (ArrayList)request.getAttribute("ALTYPE");
	
	strAccountCurrSign = GmCommonClass.parseNull((String)request.getAttribute("strAccountCurrSign"));
	strTotAmt =  GmCommonClass.parseNull((String)request.getAttribute("strTotAmt"));	
	strTotOut =  GmCommonClass.parseNull((String)request.getAttribute("strTotOut"));
	 if(strType.equals("Group")) { 
		//String strPaymentTerm = GmCommonClass.parseNull((String)request.getAttribute("PAYMENTTERM"));
		//strAccId = (String)request.getAttribute("hAccId");
		
		straddress = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYADDRESS"));
		strGMName = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYNAME"));
		strGMphone = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYPHONE"));
		if(!strGMphone.equals("")){
		strGMphone = strGMphone.replaceAll(",", "\n<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		} 
		strAddToJasper = strGMName+"/"+straddress; //+"/Phone :"+strGMphone ; -- for UK phone number is in Address. 
		straddress = "&nbsp;" + straddress.replaceAll("/", "\n<br>&nbsp;");
		if(!strGMphone.equals("")){// If phone number is not there with address
			strHeadAddress = "<br>&nbsp;&nbsp;<b>" + strGMName + "</b><BR>&nbsp;" + straddress + "</b><BR>&nbsp;&nbsp;" + strGMphone; 
		}else{
			strHeadAddress = "<br>&nbsp;&nbsp;<b>" + strGMName + "</b><BR>&nbsp;" + straddress;
		}
	} 
	
	straddress = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYADDRESS"));
	strGMName = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYNAME"));
	strGMphone = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYPHONE"));
	strGMphone = strGMphone.replaceAll(",", "\n<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
	strAddToJasper = strGMName+"/"+straddress; //+"/Phone :"+strGMphone ; -- for UK phone number is in Address. 
	straddress = "&nbsp;" + straddress.replaceAll("/", "\n<br>&nbsp;");
	
	String strRremitaddr = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMREMIT"));
	String strRemitName = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMREMITHEAD"));
	strRremitaddr = "&nbsp;" + strRremitaddr.replaceAll("/", "\n&nbsp;&nbsp;<br>&nbsp;");
	//String strHeadAddress = "<br>&nbsp;&nbsp;<b>" + strGMName + "</b><BR>&nbsp;" + straddress + "</b><BR>&nbsp;&nbsp;Phone:" + strGMphone;
	
	// previously UK statement used below condition but now it is working like other countries so no need of this below code
	/* boolean blJasFlg = false;
	boolean grpblJasFlg = false; */
	
	/* if(!strInvStmntJasNm.equals("")){
		blJasFlg = true;
		if(strStatementType.equals("SO"))
		strInvStmntJasNm="/GmUKInvoiceStatementOverdue.jasper";
	}
	if(strType.equals("Group")){
		//grpblJasFlg = true;
		if(blJasFlg)
		{
			strInvStmntJasNm="/GmUKGroupStatementPrint.jasper";
		}
	} */
	double dbInvoiceAmt = 0.00;
	double dbInvoiceTotal = 0.00;
	double dbOverdue = 0.00;
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	hmParam = (HashMap)request.getAttribute("hmParam");
	alAccount = (ArrayList)hmReturn.get("ACCOUNTLIST");		
	if (strAction.equals("Load") || strAction.equals("Reload"))
	{
		alInvTypes = (ArrayList)hmReturn.get("CODELIST");
				
		if (strAction.equals("Reload"))
		{
			strAccId = (String)request.getAttribute("hAccId");
			alAcctInvoice = (ArrayList)hmReturn.get("OPENINVOICES");
			intLength = alAcctInvoice.size();
			alPayModes = (ArrayList)hmReturn.get("PAYMODEDETAILS");
			
			strAccID_PAYALL = (String)hmParam.get("ACCID");
			strInvID_PAYALL = (String)hmParam.get("INVID");
			strCustPO_PAYALL = (String)hmParam.get("CUSTPO");
			strInvAmt_PAYALL = (String)hmParam.get("INVAMT");
			strInvType_PAYALL = (String)hmParam.get("INVOTYP");
			strSortCName = (String)hmParam.get("SORT");
			strSortType = (String)hmParam.get("SORTTYPE");
			strGrpAccAddr =GmCommonClass.parseNull((String) hmParam.get("GROUPACCADD"));
		}
	}
	alAcctInvoice = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("OPENINVOICES"));
	if(alAcctInvoice.size()>0){
	hmLoop = (HashMap)alAcctInvoice.get(0);
	strVendorId = GmCommonClass.parseNull((String)hmLoop.get("VENDOR"));
	}	 
	
	HashMap hmInvTypeForCobo = new HashMap();
	hmInvTypeForCobo.put("ID","");
	hmInvTypeForCobo.put("PID","CODEID");
	hmInvTypeForCobo.put("NM","CODENM");
	
    Calendar cal = Calendar.getInstance(TimeZone.getDefault());
    cal.add(Calendar.DATE, -1);
    java.sql.Date dtyester = new java.sql.Date(cal.getTimeInMillis());
	String strAccountNm = "";
	String strAccountAddress = "";
	String strICSHeaderAddress = "";
	 
%>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
var accountid = '<%=strParentFl%>';
</script>
<HTML>
<HEAD>
<TITLE> GlobusOne Enterprise Portal: Invoice Statement</TITLE>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmInvoiceStatement.js"></script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnLoad();" onbeforeprint="hidePrint();" onafterprint="showPrint();">
	<table border="0" class="border" width="700" cellspacing="0" cellpadding="0" align="center">
		<%if(strType.equals("Account") || strType.equals("Group")){%>
			<tr>
				<td align="right" colspan="0" height="20" id="button1">
					<%if(strPrintAccess.equals("Y")){%>	
					<fmtInvoiceStatement:message key="LBL_CLICK_HERE_TO_PRINT" var="varPrintPage"/>
						<img style="cursor:hand" src='<%=strImagePath%>/print-icon.png' height="25" width="25" alt="${varPrintPage}" onClick="fnPrint();" />
					<%} %>	
					<fmtInvoiceStatement:message key="LBL_CLICK_HERE_CLSOE" var="varClickClsoe"/>	
					<img style="cursor:hand" src='<%=strImagePath%>/close_icon.png' height="20" width="20" alt="${varClickClsoe}" onClick="window.close();" />
				</td>
			</tr>   
		<%} %>
		<tr>
			<td colspan="3">	
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<%-- <%if(!strType.equals("Account") && !strType.equals("Group")){%> --%>
							<td class="RightText" width="120" valign="middle" align="center"><img src="<%=strImagePath%>/<%=strCompayLogo%>" width="138" height="70"></td>
						<%-- <%}else{ %>
							<td class="RightText" width="120" valign="middle" align="center"><img src="<%=strImagePath%>/<%=strCompayLogo%>" width="138" height="70"></td>
						<%} %> --%>
						<td id="HeadAddress" class="RightText" valign="middle" height="80" width="250"><%=strHeadAddress%><td>
						<td valign="top" class="RightText" wrap="nowrap" align="right"><font size="+3"><fmtInvoiceStatement:message key="LBL_INVOICE_STATEMENT"/></font>&nbsp;<BR><BR>
						<font color="red" size="4px" face="sans-serif"><strong><i><%=(!strCreditHldMsg.equals(""))?strCreditHldMsg:""%></i></strong></font>&nbsp;
					
						</td>
					</tr>
				</table>
			<br>
			</td>
		</tr>
		<tr><td class="Line" colspan="3"></td></tr>
		<tr>
			<td colspan="3">
				<table border="0" width="700" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td>
						<table border="0" width="100%" cellspacing="0" cellpadding="0" height="70">
							
							<%if(!strType.equals("Group")){ %>
							<tr bgcolor="#CCCCCC" class="RightTableCaption">
								
								<%if(strInvSource.equals("26240213")){ %>
								<td height="24" width="40%">&nbsp;<fmtInvoiceStatement:message key="LBL_DEALER_DETAILS"/></td>
								<td></td>
								<td align="center"><fmtInvoiceStatement:message key="LBL_DEALER_ID"/></td>
								<%}else{ %>
								<td height="24" width="40%">&nbsp;<fmtInvoiceStatement:message key="LBL_ACCOUNT_DETAILS"/></td>
								<td></td>
								<td align="center"><fmtInvoiceStatement:message key="LBL_ACCOUNT"/></td>
								<%} %>
								<%if(strStatementType.equals("SO")){%>
									<td align=center><fmtInvoiceStatement:message key="LBL_TOTAL_OVER_DUE"/></td>
								<%}else{ %>
									<td align="center"><fmtInvoiceStatement:message key="LBL_TOTAL_OUTSTANDING"/></td>
								<%} %>
								<td align="center"><fmtInvoiceStatement:message key="LBL_PAYMENT_TERMS"/></td>
								<td align="center"><fmtInvoiceStatement:message key="LBL_REPORT_AS_OF"/></td>
							</tr>
							<tr><td class="Line" colspan="6"></td></tr>
							<tr>
								<td height="24" rowspan="5" width="40%" id="AcName"></td>
								<td width="1" bgcolor="#666666" rowspan="6"></td>
								<td height="24" id="AccID" align="center"></td>
								<%if(strStatementType.equals("SO")){%>
									<td id="Outs" align="center"><%=strAccountCurrSign%> <%=strTotOut%></td>
								<%}else{ %>
									<td id="Outs" align="center"><%=strAccountCurrSign%> <%=strTotAmt%></td>
								<%} %>
								<td height="24" align="center"><%=strPaymentTerm %></td>
								<td height="24" align="center"><%=strCurDate %></td>
																						
							</tr>
							
							<% if(strShowVendorNo.equals("YES")){%>			
							<tr><td class="Line" colspan="5"></td></tr>
							<tr   bgcolor="#CCCCCC" class="RightTableCaption">
								<td  height="24"  align="center"><fmtInvoiceStatement:message key="LBL_VENDOR"/></td>
								<td   height="24" colspan="4"  align="left">&nbsp;</td>
							</tr>				
						<tr><td class="Line" colspan="5"></td></tr>
						<tr><td  height="24" align="center"><%=strVendorId=(strVendorId.equals(""))?"-":strVendorId%></td></tr>	<%}%>					
							<%}else{ %>
							<tr bgcolor="#CCCCCC" class="RightTableCaption">
								<td height="24" width="50%">&nbsp;<fmtInvoiceStatement:message key="LBL_GROUP_DETAILS"/></td>
								<td align="center"><fmtInvoiceStatement:message key="LBL_TOTAL_OUTSTANDING"/></td>
								<td align="center"><fmtInvoiceStatement:message key="LBL_REPORT_AS_OF"/></td>							
							</tr>
							<tr><td class="Line" colspan="5"></td></tr>
							<tr>
								<td id="AccID"><%=strGrpAccAddr%></td>
								<td id="Outs" align="center"><%=strAccountCurrSign%> <%=strTotAmt%></td>
								<td align="center"><%=strCurDate %></td>						
							</tr>					
							<%} %>						
						</table>						
					</td>
				</tr>
				<tr>
					<td height="100" valign="top" align="center">
						<table border="0" width="100%" cellspacing="0" cellpadding="0" >
							<tr><td class="Line" colspan="8"></td></tr>							
							<%if(!strType.equals("Group")){ %>
								<tr bgcolor="#CCCCCC" class="RightTableCaption">
									<td HEIGHT="24" width="100"><fmtInvoiceStatement:message key="LBL_INVOICE_ID"/></td>
									<%if(strStatementType.equals("SO")){ %>
										<td align="left" width=100><fmtInvoiceStatement:message key="LBL_CUSTOMER_PO"/></td>
										<td align="center" width=100><fmtInvoiceStatement:message key="LBL_INVOICE_DATE"/></td>
										<td align="right" width=100><fmtInvoiceStatement:message key="LBL_PAYMENT_DATE"/></td>
										<td align="center" width=80>&nbsp;<fmtInvoiceStatement:message key="LBL_OVER_DUE"/></td>
									<%} else{ %>
										<td width="120"  align="center"><fmtInvoiceStatement:message key="LBL_INVOICE_DATE"/></td>
										<td align="left"><fmtInvoiceStatement:message key="LBL_CUSOTMER_PO"/></td>
									<%} %>
									<td align="center"><fmtInvoiceStatement:message key="LBL_CURRENCY"/></a></td>
							<%}else{ %>
								<tr bgcolor="#CCCCCC" class="RightTableCaption">
									<td HEIGHT="24" width="100"><fmtInvoiceStatement:message key="LBL_INVOICE_ID"/></td>
									<td width="120"  align="center"><fmtInvoiceStatement:message key="LBL_INVOICE_DATE"/></td>
									<td align="left"><fmtInvoiceStatement:message key="LBL_CUSOTMER_PO"/></td>
									<td align="left"><fmtInvoiceStatement:message key="LBL_AC_NAME"/></a></td>
							<%} %>
									<td align="right" width="135"><fmtInvoiceStatement:message key="LBL_INVOICE_AMT"/></td>
									<td align="right"><fmtInvoiceStatement:message key="LBL_AMOUNT_RECD"/>.</td>
								</tr>
								
							<tr><td class="Line" colspan="8"></td></tr>					
<%
					if (intLength > 0)
					{
							dbInvoiceAmt = 0;
							dbInvoiceTotal = 0;
							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alAcctInvoice.get(i);
								strInvType = GmCommonClass.parseNull((String)hmLoop.get("INVTP"));
								strInvId = GmCommonClass.parseNull((String)hmLoop.get("INVID"));
								strInvDt = GmCommonClass.getStringFromDate((java.util.Date)hmLoop.get("INVDT"),strApplDateFmt);
								strPayDt = GmCommonClass.getStringFromDate((java.util.Date)hmLoop.get("PAYDT"),strApplDateFmt);
								strOverdue = GmCommonClass.parseZero((String)hmLoop.get("OVERDUE"));
								intQty = Integer.parseInt(strOverdue);
								dbInvAmt = Double.parseDouble(GmCommonClass.parseZero((String)hmLoop.get("INVAMT")));
								dbInvOverdue = Double.parseDouble(GmCommonClass.parseZero((String)hmLoop.get("OVERDUE")));
								
								strInvTypeId = GmCommonClass.parseNull((String)hmLoop.get("INVTPID"));
								strCurrency = GmCommonClass.parseNull((String)hmLoop.get("CURRENCY"));
								strAmtPaid = GmCommonClass.parseZero((String)hmLoop.get("AMTPAID"));
								if(strInvTypeId.equals("50200")	|| strInvTypeId.equals("50202") || strInvTypeId.equals("50203"))	// Invoice type, 50200 - regular, 50202 - credit
								{
									dbInvoiceAmt = dbInvoiceAmt + (dbInvAmt - Double.parseDouble(strAmtPaid));
								}
								if(dbInvOverdue<=0)
								{
									if(strInvTypeId.equals("50200") || strInvTypeId.equals("50202")){
									dbInvoiceTotal =  dbInvAmt + dbInvoiceTotal;
								}}
								dbOverdue      =  dbInvoiceAmt - dbInvoiceTotal;
								strBillAdd = GmCommonClass.parseNull((String)hmLoop.get("BILLADD"));
								strStatusFl = GmCommonClass.parseNull((String)hmLoop.get("SFL"));
								strCallFlag = GmCommonClass.parseNull((String)hmLoop.get("CALL_FLAG"));
								strCustPO = (String)hmLoop.get("PO");							
								strMulitiAcc = (String)hmLoop.get("ACCID");			
								if(strParentFl.equals("true")){
								  strMulitiAcc = strAccountIds;
								}
								strAccountNm = (String)hmLoop.get("NAME");
								strAccountAddress = GmCommonClass.parseNull((String)hmLoop.get("BILLADD"));
								strICSHeaderAddress = GmCommonClass.parseNull((String)hmLoop.get("ICSHEADADD"));
								
								dbPendAmt = dbInvAmt - Double.parseDouble(strAmtPaid);
								strShade	= (i%2 == 0)?"class=Shade":""; //For alternate Shading of rows
								if(strStatementType.equals("SO") && intQty > 0)
								{
%>
					<tr <%=strShade%>>
						<td height="20" class="RightText"><%=strInvId%></td>
						<td class="RightText" width="120" align="left"><%=strCustPO%></td>
						<td class="RightText" width="120" align="center">&nbsp;<%=strInvDt%></td>
						<td class="RightText" width="120" align="center">&nbsp;<%=strPayDt%></td>
						<td class="RightText" width="120" align="center">&nbsp;<%=strOverdue%></td>  
						<td class="RightText" align="center"><%=strCurrency%></td>
						<gmjsp:currency  align="right" type="CurrText"  textValue="<%=String.valueOf(dbInvAmt)%>"/>
						<gmjsp:currency  align="right" type="CurrText"  textValue="<%=strAmtPaid%>"/></td>
					<%
					} 
					else if(!strType.equals("Group"))
					{ 
					%>
					<tr <%=strShade%>>
						<td height="20" class="RightText"><%=strInvId%></td>
						<td class="RightText" width="120" align="center">&nbsp;<%=strInvDt%></td>
						<td class="RightText" align="left"><%=strCustPO%></td>
						<td class="RightText" align="right"><%=strCurrency%>&nbsp;</td>
						<gmjsp:currency type="CurrText"  textValue="<%=String.valueOf(dbInvAmt)%>"/>
						<gmjsp:currency type="CurrText"  textValue="<%=strAmtPaid%>"/></td>
					<%
					}
					if(strType.equals("Group"))
					{
					%>
					<tr <%=strShade%>>
						<td height="20" class="RightText"><%=strInvId%></td>
						<td class="RightText" width="120" align="center">&nbsp;<%=strInvDt%></td>
						<td class="RightText" align="left"><%=strCustPO%></td>
						<td class="RightText" align="left"><%=strAccountNm%>&nbsp;</td>
						<gmjsp:currency type="CurrText"  textValue="<%=String.valueOf(dbInvAmt)%>"/>
						<gmjsp:currency type="CurrText"  textValue="<%=strAmtPaid%>"/></td>
					<%
					}
					%>
					</tr>
<%
							}
							strAccountAddress = strAccountAddress +"<br> <br>";

					
%>
						<tr><td class="LLine" colspan="8" height="1"></td></tr>
						<tr>
							<td align="center" colspan="8" height="30" id="button">
							<fmtInvoiceStatement:message key="BTN_PRINT" var="varPrint"/>
							<fmtInvoiceStatement:message key="BTN_CLOSE" var="varClose"/>
								<gmjsp:button value="${varPrint}" style="width: 4em; height: 23;" name="Btn_Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;&nbsp;&nbsp;
								<gmjsp:button value="${varClose}" name="Btn_Close" style="width: 4em; height: 23;" gmClass="button" onClick="window.close();" buttonType="Load" />
							</td>
						</tr>
<%
				
					}else{				
%>
						<tr>
							<td align="center" colspan="7" class="RightTextBlue" height="40">
							<%if(strStatementType.equals("SO")){%>
								<fmtInvoiceStatement:message key="LBL_THERE_NO_OVERDUE_INVOICES"/>
							<%}else{ %>
								<fmtInvoiceStatement:message key="LBL_NO_INVOICES"/>
							<%} %>
							
							</td>
						</tr>
<%							
					}
%>
<%
String strFontColor = "";
String strFontClose = "";
if(dbInvoiceAmt < 0){
	String strdbInvoiceAmt = String.valueOf(dbInvoiceAmt);
	int len = strdbInvoiceAmt.length();
	strdbInvoiceAmt = strdbInvoiceAmt.substring(1,len);
	strFontColor = "<font color=red>(";
	dbInvoiceAmt = Double.parseDouble(strdbInvoiceAmt);
	strFontClose = ")</font>";

}
%>
				<input type="hidden" name="hTotalAmt" value="<%=strCurrSymbol%>&nbsp;<%=strFontColor%><%=GmCommonClass.getStringWithCommas(""+dbInvoiceAmt)%><%=strFontClose%>">
				<input type="hidden" name="hAccName" value="<%=strAccountAddress%>">
				<input type="hidden" name="hAccID" value="<%=strMulitiAcc%>">
				<input type="hidden" name="hICSHeaderAddress" value="<%=strICSHeaderAddress%>">
				</table>
			</td>
		</tr>
    </table>
</td>
</tr>
</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
