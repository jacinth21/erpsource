<%
/**********************************************************************************
 * File		 		: GmAPPostingsRptDtls.jsp
 * Desc		 		: Report for A/P Postings Report Detail
 * Version	 		: 1.0
 * author			: Xun
************************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtAPpostingRptDtls" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmAPPostingsRptDtls.jsp -->
<fmtAPpostingRptDtls:setLocale value="<%=strLocale%>"/>
<fmtAPpostingRptDtls:setBundle basename="properties.labels.accounts.GmAPPostingsRptDtl"/>
<bean:define id="returnList" name="frmAPPostings" property="returnList" type="java.util.List"></bean:define>  
 
<%   
String strWikiTitle = GmCommonClass.getWikiTitle("GL_ACCOUNT_INVOICE_DETAILS");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Invoice Details of GL Account</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css"> 

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>

function fnPrint()
{
	window.print();
}

  function fnClose()
{ 
	window.close();
}

function fnInvcDetails(strPaymentID)
 {   
      windowOpener("/gmAPSaveInvoiceDtls.do?strOpt=viewInvc&hpaymentId="+strPaymentID,"viewInvc","resizable=yes,scrollbars=yes,top=250,left=300,width=880,height=400");
    } 
</script> 
</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmAPPostings.do">
<html:hidden property="strOpt" value = ""/>

 
	<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtAPpostingRptDtls:message key="LBL_INVOICE_DETAILS_GL_ACCOUNT"/></td>
			
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtAPpostingRptDtls:message key="LBL_HELP" var="varHelp"/>
			 <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		<tr>
			<td width="798"   valign="top" colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr><td colspan="4" class="ELine"></td></tr> 
                    <tr>
						<td class="RightTableCaption" align="right" HEIGHT="30" > &nbsp;<fmtAPpostingRptDtls:message key="LBL_GL_ACCOUNT_NUMBER"/>:</td>
						<td >&nbsp;
							<bean:write name="frmAPPostings" property="haccountID" />												
						</td>
						<td class="RightTableCaption" align="right" HEIGHT="30" > &nbsp;<fmtAPpostingRptDtls:message key="LBL_DATE_RANGE"/>:</td>
						<td >&nbsp;	<bean:write name="frmAPPostings" property="batchDateFrom" />&nbsp;&nbsp;<fmtAPpostingRptDtls:message key="LBL_TO"/> &nbsp;&nbsp; <bean:write name="frmAPPostings" property="batchDateTo" />
						 									
						</td>
						 
					</tr>	
					 
					<tr><td colspan="4" class="LLine"></td></tr> 
					<tr>
						<td colspan="4"> 		
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
		 	
		<tr>
			<td colspan="9">
			<display:table name="requestScope.frmAPPostings.returnList" requestURI="/gmAPPostings.do"  export= "true" paneSize ="200" class="its" id="currentRowObject" varTotals="totals" freezeHeader="true" decorator="com.globus.accounts.displaytag.beans.DTInvoiceDtlsWrapper" > 
 		 		<fmtAPpostingRptDtls:message key="LBL_VENDOR_INVOICE_ID" var="varVendorInoviceId"/><display:column property="INVOICEID" title="${varVendorInoviceId}"   />
				<fmtAPpostingRptDtls:message key="LBL_PART_NUMBER" var="varPartNumber"/><display:column property="PARTNUMBER" title="${varPartNumber}"  class="alignleft" /> 
				<fmtAPpostingRptDtls:message key="LBL_PART_DESCRIPTION" var="varPartDescription"/><display:column property="PDESC" title="${varPartDescription}"  class="alignleft" />
				<fmtAPpostingRptDtls:message key="LBL_VENDOR_NAME" var="varVendorName"/><display:column property="VENDORNM" title="${varVendorName}"  class="alignleft" />
				<fmtAPpostingRptDtls:message key="LBL_DETAILS" var="varDetails"/><display:column property="DETAIL" title="${varDetails}"  class="aligncenter" />
				<fmtAPpostingRptDtls:message key="LBL_DHR_ID" var="varDhrId"/><display:column property="DHRID" title="${varDhrId}"  class="alignleft" /> 
				<fmtAPpostingRptDtls:message key="LBL_QUANTITY_ACCEPTED" var="varQtyAccepted"/><display:column property="QTYACC" title="${varQtyAccepted}"  class="alignright"  /> 
				<fmtAPpostingRptDtls:message key="LBL_COST_EA" var="varCostEa"/><display:column property="EACHCOST" title="${varCostEa}"  format="{0,number,$#,###,###.00}" class="alignright"  />
				<fmtAPpostingRptDtls:message key="LBL_TOTAL_COST" var="varTotalCost"/><display:column property="TOTCOST" title="${varTotalCost}"  class="alignright" format="{0,number,$#,###,###.00}" total="true"/>
				
				
			<display:footer media="html"> 
					<%
					String strVal ; 
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column9").toString();
					String strTotal = " $ " +  GmCommonClass.getStringWithCommas(strVal,2);
					 				
					%>
				<tr class = shade>
		  		<td colspan="8"> <B> <fmtAPpostingRptDtls:message key="LBL_TOTAL"/> : </B></td> 
    	    	 <td class = "alignright" > <B><%=strTotal%></B></td>
	    	 	 
  				</tr>
  			</display:footer>
				 
			</display:table>
			</td>
		</tr>
		
				<tr>
					<td class="LLine" height="1" colspan="9"></td>
				</tr> 
				<tr>
					<td  colspan="9" class = "aligncenter" ><p> &nbsp;</p>
					<fmtAPpostingRptDtls:message key="BTN_CLOSE" var="varClose"/>
					<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="fnClose();" buttonType="Load" />
					</td>
				</tr> 
    <table>
						</td>
					</tr>
					
				</table>
  			   </td>
  		  </tr>	 
    </table>	
    
    
    	     	
</html:form>
 
</BODY>

</HTML>

