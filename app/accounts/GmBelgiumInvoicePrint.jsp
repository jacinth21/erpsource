 <%
/**********************************************************************************
 * File		 		: GmBelgiumInvoicPrint.jsp
 * Desc		 		: This screen is used for the Order Maintenance
 * Version	 		: 1.0
 * author			: Gopinathan
************************************************************************************/
%>

 <!-- \accounts\GmBelgiumInvoicePrint.jsp -->
<%@ page language="java" %>
<%--
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
 --%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ include file="/common/GmHeader.inc" %>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	
	String strAction = (String)request.getAttribute("hAction");
	String strInvoiceMode = "";
	String strHtmlJasperRpt = "";
	
	if (strAction == null)
	{
		strAction = (String)session.getAttribute("hAction");
	}
	strAction = (strAction == null)?"Load":strAction;

	HashMap hmReturn = new HashMap();

	ArrayList alOrderNums = new ArrayList();
	HashMap hmCartDetails = new HashMap();
	HashMap hmConstructs = new HashMap();
	
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	//strInvoiceMode = GmCommonClass.parseNull((String)request.getAttribute("hVoidFlag"));
	String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	String strApplnCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	
	HashMap hmOrderDetails = new HashMap();
	String strPayNm = "";
	String strOrdId = "";
	java.sql.Date dtOrderDate = null;
	if (hmReturn != null)
	{

                         
		hmOrderDetails = (HashMap)hmReturn.get("ORDERDETAILS");
		hmOrderDetails.put("INVDT",GmCommonClass.getStringFromDate((java.sql.Date)hmOrderDetails.get("INVDT"),strApplnDateFmt));
        hmOrderDetails.put("PARTYFLAG", (String)hmReturn.get("PARTYFLAG"));
		hmOrderDetails.put("PARTYNAME", (String)hmReturn.get("PARTYNAME"));
		hmOrderDetails.put("HEADERADDRESS",(String)hmReturn.get("HEADERADDRESS"));
		hmOrderDetails.put("REMITADDRESS",(String)hmReturn.get("REMITADDRESS"));
		alOrderNums = (ArrayList)hmReturn.get("ORDERNUMS");
		hmCartDetails = (HashMap)hmReturn.get("CARTDETAILS");
		hmOrderDetails.put("INVOICETITLE",(String)request.getAttribute("INVOICETITLE"));
		strPayNm = GmCommonClass.parseNull((String)hmOrderDetails.get("PAYNM"));
		strPayNm = strPayNm.equals("")?"Net 30 Days":strPayNm;
        hmOrderDetails.put("PAYNM", strPayNm);
        hmOrderDetails.put("GLOBUSVATNUM", GmCommonClass.parseNull((String)request.getAttribute("GLOBUSVATNUM")));
        hmOrderDetails.put("COUNTRYVATNUM", GmCommonClass.parseNull((String)request.getAttribute("NIP")));
        hmOrderDetails.put("CURRSIGN",strApplnCurrSign);
	}
	int intSize = alOrderNums.size();
	int intLoop = 0;
	ArrayList alLoop = new ArrayList();
	ArrayList allList = new ArrayList();
	ArrayList alHashMap = new ArrayList();
	HashMap hmTemp = new HashMap();
	HashMap hmLoop = new HashMap();
	String strVATPER = "";
	
	//= GmCommonClass.parseNull((String)request.getAttribute("NIP"));
	//log.debug("strVATPER~~~~~~~~~~~~~~~~~~~~~~~~"+strVATPER+"~~~~"+strVATPER.length());
	double vatPer = 0.0;
	String strItems = "";
	int intQty = 0;
	double dbItemTotal = 0.0;
	double dbTotalItemVAT = 0.0;
	double dbGrossTotal = 0.0;  
	double dbTotal = 0.0;
	String strItemTotal = "";
	String strTotal = "";
	String strPrice = "";
	String strQty = "";
	String strNumInWords = "";
	if(intSize==0){
		hmLoop.put( "ORDERID", "-");
		hmLoop.put( "ORDERDATE", "-"); 
		hmLoop.put( "ITEMTOTAL", "-");
		hmLoop.put( "VATPER", "-");
		hmLoop.put( "ID", "-");
		hmLoop.put( "PDESC", "-");
		hmLoop.put( "QTY", "0");
		hmLoop.put( "PRICE", "0");
		hmOrderDetails.put("GROSSTOTAL", dbGrossTotal);
		hmOrderDetails.put("TOTALVAT", dbTotalItemVAT);
		hmOrderDetails.put("NETTOTAL",dbTotal);
		alHashMap.add(hmLoop);
	}	
	for (int i=0;i<intSize;i++)
	{
		hmTemp = (HashMap)alOrderNums.get(i);
		strOrdId = (String)hmTemp.get("ID");
		dtOrderDate = (java.sql.Date)hmTemp.get("ORDERDATE");

 		if(strOrdId.endsWith("S")!=true) // remove return order informations
		{
			alLoop = (ArrayList)hmCartDetails.get(strOrdId);
			intLoop = alLoop.size();
			for ( int j=0;j<intLoop;j++)
			{
				hmLoop = (HashMap)alLoop.get(j);
				log.debug("=========KEYS==========="+hmLoop);
				strVATPER = (String)hmLoop.get("VAT");
				if(!strVATPER.equals("")){
					vatPer = Double.parseDouble(strVATPER)/ 100;
				}
				strPrice = (String)hmLoop.get("PRICE");
				strQty = (String)hmLoop.get("QTY");
				intQty = Integer.parseInt(strQty);
				dbItemTotal = Double.parseDouble(strPrice);
				dbItemTotal = intQty * dbItemTotal; // Multiply by Qty
				if(!strVATPER.equals("")){
					dbTotalItemVAT = dbTotalItemVAT  + (dbItemTotal * vatPer);
				}
				strItemTotal = ""+dbItemTotal;
				dbTotal = dbTotal + dbItemTotal;
				
				strTotal = ""+dbTotal;
				hmLoop.put( "ORDERID", strOrdId);
				hmLoop.put( "ORDERDATE", new java.util.Date(dtOrderDate.getTime())); 
				hmLoop.put( "ITEMTOTAL", strItemTotal);
				hmLoop.put( "VATPER", strVATPER.equals("")?"-":strVATPER);
				alHashMap.add(hmLoop);
			}
		dbGrossTotal = dbTotalItemVAT + dbTotal;
		strNumInWords = GmCommonClass.convetToPolishWords(dbGrossTotal, gmDataStoreVO.getCmpid());
		hmOrderDetails.put("GROSSTOTAL", dbGrossTotal);
		hmOrderDetails.put("TOTALVAT", dbTotalItemVAT);
		hmOrderDetails.put("NETTOTAL",dbTotal);
		hmOrderDetails.put("TOTALINWORDS",strNumInWords);
		}
	}
	System.out.println("Done");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Belgium Invoice Print </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script type="text/javascript">
function fnPrint()
{
var prtContent = document.getElementById("jasper");
var WinPrint = document.getElementById("ifmPrintContents").contentWindow;
WinPrint.document.open();
WinPrint.document.write(prtContent.innerHTML);
WinPrint.document.close();
WinPrint.focus();
WinPrint.print();
}
</script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screenOus.css">

</HEAD>
<BODY leftmargin="20" topmargin="10">
<FORM method="post" action = "<%=strServletPath%>/GmInvoiceServlet">
<iframe id="ifmPrintContents" style="height: 0px; width: 0px; position: absolute"></iframe>
<div id="jasper"> 
		<%
			String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
			GmJasperReport gmJasperReport = new GmJasperReport();								
			gmJasperReport.setRequest(request);
			gmJasperReport.setResponse(response);
			gmJasperReport.setJasperReportName("/GmBelgiumInvoicePrint.jasper");
			gmJasperReport.setHmReportParameters(hmOrderDetails);							
			gmJasperReport.setReportDataList(alHashMap);				
			gmJasperReport.setBlDisplayImage(true);
			strHtmlJasperRpt = gmJasperReport.getHtmlReport();	
		%>
		<%=strHtmlJasperRpt %>				
</div>
<table align="center" width="100%">
<tr>
	<td align="center">
		<gmjsp:button value="Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />
 		<gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" />
 	</td>
 </tr>
 </table>		
</FORM>
</BODY>
</HTML>