
<%
/**********************************************************************************
 * File		 		: GmAccountsHome.jsp
 * Desc		 		: This screen is used for the DashBoard Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmCommonConstants"%> 
<%@ page import ="org.apache.commons.beanutils.RowSetDynaClass"%>
<%@ taglib prefix="fmtAccountsHome" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmAccountsHome.jsp -->
<fmtAccountsHome:setLocale value="<%=strLocale%>"/>
<fmtAccountsHome:setBundle basename="properties.labels.accounts.GmAccountsHome"/>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strDeptId = (String)session.getAttribute("strSessDeptId")==null?"":(String)session.getAttribute("strSessDeptId");
	String strAccessLvl = (String)session.getAttribute("strSessAccLvl")==null?"":(String)session.getAttribute("strSessAccLvl");
	String strApplDateFmt = strGCompDateFmt;
	String strRptFmt = "{0,date,"+strApplDateFmt+"}";

	HashMap hmCurrency = new HashMap();
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strCurrSign = GmCommonClass.parseNull((String)request.getAttribute("ACC_CUR_NM"));
	String strApplCurrFmt = (String)hmCurrency.get("CMPCURRFMT");
	String strCurrPos = GmCommonClass.getString("CURRPOS");
	String strCurrFmt = "";
	String strScreenType = "";
	
	strScreenType = GmCommonClass.parseNull((String) request.getAttribute("screenType"));
	
	if(strCurrPos.equalsIgnoreCase("RIGHT")){
		strCurrFmt = "{0,number,"+strApplCurrFmt+" "+strCurrSign+"}";
	}
	else{
		strCurrFmt = "{0,number,"+strCurrSign+" "+strApplCurrFmt+"}";
	}
	int intAccessLvl = Integer.parseInt(strAccessLvl);
	boolean bolAccess = false;

	if ( intAccessLvl > 3 )
	{
		bolAccess = true;
	}


	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmSales = new HashMap();
	hmSales = (HashMap)hmReturn.get("SALES");
	RowSetDynaClass rsdn = (RowSetDynaClass) request.getAttribute("invoice");
	log.debug("Vlus inside RSDN  " + rsdn.getRows().size());
	ArrayList alre = new ArrayList();
	alre = (ArrayList)rsdn.getRows();

	HashMap hmLoop = new HashMap();

	String strId = "";
	String strName = "";
	String strCount = "";
	String strSum = "";


	String strSales = "";
	String strMonthSales = "";

	String strPO = "";
	String strCost = "";
	String strDistName = "";
	String strInvFl = "";
	String strPayFl = "";
	String strRedTextInv = "";
	String strRedTextPay = "";
							
    if (hmSales != null)
    {
		HashMap hmDaySales = (HashMap)hmSales.get("DAYSALES");
		HashMap hmMonthSales = (HashMap)hmSales.get("MONTHSALES");
		strSales = GmCommonClass.parseZero((String)hmDaySales.get("SALES"));
		strMonthSales = (String)hmMonthSales.get("SALES");
    }
	String strShade = "";
	String strLine = "";
	String strRAId = "";
	String strReturnType = "";
	String strReturnReason = "";
	String strExpectedDate = "";
	String strPerson = "";
	String strComments = "";
	String strDateInitiated = "";
	String strAccName = "";
	String strStatus = "";
	String strReturnAmt = "";
	String strOrderId = "";
	String strDate = "";
	String strAccessFl = GmCommonClass.parseNull((String)request.getAttribute("AccessFlg"));
	String strAccountCurrency = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_ID"));
	ArrayList alCompCurrency = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCOMPANYCURRENCY"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Accounts Dashboard </TITLE>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
var strApplCurrFmt = '<%=strApplCurrFmt%>';
function fnCallInv(accid,obj)
{

	document.frmAccount.hId.value = accid;
	document.frmAccount.hAccId.value = accid;
	document.frmAccount.hPO.value = obj;
	document.frmAccount.hAction.value = "INV";
	//document.frmAccount.hMode.value = "INV";
	document.frmAccount.hOpt.value = "LOAD";
	//document.frmAccount.hPgToLoad.value = "GmInvoiceServlet";
	document.frmAccount.action = "GmInvoiceServlet";
	document.frmAccount.submit();
}


function fnViewInv(accid,obj)
{
	var varpo = obj.id;
	windowOpener("/GmInvoiceServlet?hPO="+varpo+"&hAccId="+accid+"&hAction=INV&hOpt=VIEW","ViewInv","resizable=yes,scrollbars=yes,top=150,left=200,width=900,height=600");
}
	

function fnCallPay(po,id)
{
	document.frmAccount.hId.value = id;
	document.frmAccount.hPO.value = po;
	document.frmAccount.hMode.value = "PAY";
	document.frmAccount.hPgToLoad.value = "GmInvoiceServlet";
	document.frmAccount.submit();
}

function Toggle(val)
{
	
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className="";
		tabobj.style.background="#ffffff";
	}
}

function fnCallEditReturn(val)
{
	document.frmAccount.hRAId.value = val;
	document.frmAccount.hFrom.value = "AccountsReturnsDashboard";
	document.frmAccount.hAction.value = "LoadCredit";
	document.frmAccount.action = "GmReturnCreditServlet";
	document.frmAccount.submit();
}

function fnViewReturns(val)
{
	windowOpener('<%=strServletPath%>/GmPrintCreditServlet?hAction=VIEW&hRAID='+val,"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=660,height=400");
}

function fnViewOrder(val)
{
	windowOpener('/GmEditOrderServlet?hMode=PrintPrice&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=760,height=410");	
}

// this function used to reload the page
function fnLoad() {
	fnStartProgress('Y');
	document.frmAccount.action = "GmAcctDashBoardServlet";
	document.frmAccount.hAction.value = "Load";
	document.frmAccount.submit();
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hPgToLoad" value="GmEditOrderServlet">
<input type="hidden" name="hSubMenu" value="Trans">
<input type="hidden" name="hMode" value="">
<input type="hidden" name="hFrom" value="Dashboard">
<input type="hidden" name="hPO" value="">
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hRAId" value="">
<input type="hidden" name="hAccId" value="">
<input type="hidden" name="hOUSDistFl" value="<%=strAccessFl%>">

<%-- <%if(strScreenType.equals("")){ %>
	<table border="0"  cellspacing="0" cellpadding="0" CLASS ="dttable900">
		<tr>
			<td width="450" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
						<td colspan = "2" height="25" class="RightDashBoardHeader" align="right">
							Today's Sales :  
						</td>
						<gmjsp:currency textValue="<%=strSales%>" gmClass="RightDashBoardHeader" align = "left" type="CurrTextSign"/>
						<td class="RightDashBoardHeader"><%=strCurrSign%>&nbsp;<%=strSales%></td>
						
					</tr>
				</table>
			</td>
			<td width="450" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan = "2" height="25" class="RightDashBoardHeader" align="right">
							Month To Date Sales :  
						</td>
						<td class="RightDashBoardHeader"><%=strCurrSign%>&nbsp;<%=strMonthSales%></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<% }
		 
%> --%>
<BR>
	<table border="0" width="900" cellspacing="0" cellpadding="0" CLASS ="dttable900">
	<tr>
	<%
		if(strAccessFl.equals("Y")){
	%>
		<td height="25" class="RightDashBoardHeader"><fmtAccountsHome:message key="LBL_OUS_PENDING_INVOICE"/></td>
	<%
		}else {
	%>
		<td height="25" class="RightDashBoardHeader"><fmtAccountsHome:message key="LBL_ORDER_PENDING_INVOICE"/></td>
	<%
		}
	%>
	</tr>
	<%
			if(strScreenType.equals("")){
	%>
	<tr>
		<td class="RightTableCaption" HEIGHT="25" align="center">
			&nbsp;<fmtAccountsHome:message key="LBL_CURRENCY"/>:&nbsp; <gmjsp:dropdown controlName="Cbo_Comp_Curr"
			seletedValue="<%=strAccountCurrency%>" value="<%=alCompCurrency%>"
			codeId="ID" codeName="NAMEALT" />&nbsp;&nbsp;&nbsp; <fmtAccountsHome:message key="LBL_LOAD" var="varLoad"/><gmjsp:button
			value="${varLoad}" name="Btn_Load" gmClass="Button"
			style="width: 5em; height: 23px;" onClick="fnLoad();"
			buttonType="Load" />
				</td>
			</tr>
	<%
	}
	%>		
	<tr> 
		<td>
<display:table name="requestScope.invoice.rows" class="its" id="currentRowObject" defaultsort="4" varTotals="totals"  export = "true"  
decorator="com.globus.displaytag.beans.DTAccHomeWrapper" freezeHeader="true"> 
<display:setProperty name="export.excel.filename" value="A/R Home Pending Invoice.xls" />  
<fmtAccountsHome:message key="LBL_CUSTOMER_PO" var="varCustomerPO"/><display:column property="PO" title="${varCustomerPO}" sortable="true" class="alignleft"/>
<fmtAccountsHome:message key="LBL_PO_ENTERED_DATE" var="varPOenteredDate"/><display:column property="CUST_PO_DATE" title="${varPOenteredDate}" sortable="true" class="aligncenter" format= "<%=strRptFmt%>"/>
<fmtAccountsHome:message key="LBL_VIEW" var="varView"/><display:column property="ID" title="${varView}" class="aligncenter"/> <!-- Using ID alias Name to display the View Icon for corrresponding Action-->
<fmtAccountsHome:message key="LBL_PARENT_ACCOUNT_NAME" var="varParentAccountName"/><display:column property="PARACCTNM" title="${varParentAccountName}" sortable="true"  />
<fmtAccountsHome:message key="LBL_REP_ACCOUNT_ID" var="varAccountId"/><display:column property="ACCTID" title="${varAccountId}" sortable="true"  />
<fmtAccountsHome:message key="LBL_TOTAL_INVOICE" var="varTotalInvoice"/><display:column property="COST" title="${varTotalInvoice}" total = "true" sortable="true" class="alignright" format="<%=strCurrFmt%>" /><%--format="{0,number,$#,###,###.00}" --%>
<fmtAccountsHome:message key="LBL_DATE" var="varDate"/><display:column property="MDATE" title="${varDate}" sortable="true"  class="aligncenter" format= "<%=strRptFmt%>"/>
<fmtAccountsHome:message key="LBL_SURGERY_DATE" var="varSurgDate"/><display:column property="SURG_DATE" title="${varSurgDate}" sortable="true"  class="aligncenter" format= "<%=strRptFmt%>"/>
<fmtAccountsHome:message key="LBL_STATUS" var="varStatus"/><display:column property="ORDSTATUS" title="${varStatus}" sortable="true"  />
<fmtAccountsHome:message key="LBL_DISTRIBUTOR" var="varDistributor"/><display:column property="DNAME" title="${varDistributor}" sortable="true"  />

<display:footer media="html"> 
<% 
		String strVal ;
		strVal = ((HashMap)pageContext.getAttribute("totals")).get("column6").toString();
  		//String strCurVal = " $ " + GmCommonClass.getStringWithCommas(strVal);
%>
  	<tr class = shade>
		  		<td colspan = "5" align="right"> <B> <fmtAccountsHome:message key="LBL_TOTAL"/> : &nbsp;</B></td>
		  		<%-- <td class = "alignright" ><B><%=strCurVal%></B></td>--%>
		  		<gmjsp:currency textValue="<%=strVal%>" type="CurrTextSign" currSymbol="<%=strCurrSign %>"/>
	   		
		  		<td colspan = "5" > </td>
  	</tr>
 </display:footer>

 </display:table> 
	</td> </tr>
	</table>
<%
			if(strScreenType.equals("")){
%>
<BR>

	<table border="0" width="900" cellspacing="0" cellpadding="0" CLASS ="dttable900">
	<tr>
	<%
		if(strAccessFl.equals("Y")){
	%>
		<td height="25" class="RightDashBoardHeader"><fmtAccountsHome:message key="LBL_OUS_ORDER_PENDING_CREATING"/></td>
	<%
		}else {
	%>
	<td height="25" class="RightDashBoardHeader"><fmtAccountsHome:message key="LBL_ORDER_PENDING_CREATING"/></td>
	<%
		}
	%>
	</tr>	
	<tr> <td>
	<display:table name="requestScope.SalesReturn.rows" class="its" id="currentRowObjectRACredit" defaultsort="1" varTotals="salestotals"  defaultorder="descending" export = "true"  decorator="com.globus.displaytag.beans.DTAccHomeWrapper">
    <display:setProperty name="export.excel.filename" value="A/R Home Pending Credit.xls" />  
<fmtAccountsHome:message key="LBL_RA_ID" var="varRaId"/><display:column property="RAID" title="${varRaId}" sortable="true"  style="width:120px" />
<fmtAccountsHome:message key="LBL_PARENT_ORDER_ID" var="varParentOrderID"/><display:column property="PARENTOID" title="${varParentOrderID}" sortable="true" class="alignleft" />
<fmtAccountsHome:message key="LBL_PARENT_ACCOUNT_NAME" var="varParentAccountName"/><display:column property="PARACCTNM" title="${varParentAccountName}" sortable="true"  />
<fmtAccountsHome:message key="LBL_REP_ACCOUNT_ID" var="varRepAccountId"/><display:column property="ACCID" title="${varRepAccountId}" sortable="true"  />
<fmtAccountsHome:message key="LBL_INITIATED_DATE" var="varInitiatedDate"/><display:column property="CDATE" title="${varInitiatedDate}" sortable="true" class="aligncenter" format= "<%=strRptFmt%>"/>
<fmtAccountsHome:message key="LBL_REASON" var="varReason"/><display:column property="REASON" title="${varReason}"  />
<fmtAccountsHome:message key="LBL_PENDING_RETURN" var="varPendingReturn"/><display:column property="STATUS_FL" title="${varPendingReturn}"  class="aligncenter"/>
<fmtAccountsHome:message key="LBL_PENDING_CREDIT" var="varPendingCredit"/><display:column property="PENDCREDIT" title="${varPendingCredit}" media="html" class="aligncenter"/>
<fmtAccountsHome:message key="LBL_TOTAL_AMOUNT" var="varTotalAmount"/><display:column property="RAMT" title="${varTotalAmount}" sortable="true"  format="<%=strCurrFmt%>" total ="true" class="alignright"/><%-- format="{0,number,$#,###,###.00}"  --%>

<display:footer media="html"> 
<% 
		String strVal ;
		strVal = ((HashMap)pageContext.getAttribute("salestotals")).get("column9").toString();
  		//String strCurVal = " $ " + GmCommonClass.getStringWithCommas(strVal);
%>
  	<tr class = shade>
		  		<td colspan = "8"> <B> <fmtAccountsHome:message key="LBL_TOTAL"/> : </B></td>
		  	<%--	<td class = "alignright" ><B><%=strCurVal%></B></td> --%>
		  	<gmjsp:currency textValue="<%=strVal%>" type="CurrTextSign" currSymbol="<%=strCurrSign %>"/>
  	</tr>
 </display:footer>

 </display:table> 	
	</table>
</td> </tr>
</FORM>
<%
 
 // end if strScreenType Empty
}
%>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
