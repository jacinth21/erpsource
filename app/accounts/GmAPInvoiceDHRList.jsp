<!-- 
/**********************************************************************************
 * File		 		: GmAPInvoiceDHRList.jsp
 * Desc		 		: AP Invoice DHR List - DHR List displayed with in Invoice details screen.
 * Version	 		: 1.0
 * author			: Basudev Vidyasankar
************************************************************************************/
-->

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtInvoiceDHRList" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmAPInvoiceDHRList.jsp -->
<fmtInvoiceDHRList:setLocale value="<%=strLocale%>"/>
<fmtInvoiceDHRList:setBundle basename="properties.labels.accounts.GmAPInvoiceDHRList"/>
<%
String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
%>
<HTML>
<HEAD>
<TITLE> AP DHR List </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmDHRPayment.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<%

String strRptFmt = "{0,date,"+strGCompDateFmt+"}";
%>
</HEAD>

<BODY onload="fnOnPageLoad();" style="margin-left: 0px;margin-right: 0px;">
<html:form action="/gmAPSaveInvoiceDtls.do"  >
<html:hidden property="strOpt" value="loadDHRList"/>
<html:hidden property="hPONumber" value=""/>
	<table border="0" cellspacing="0" width="1150" cellpadding="0">
		  <tr>
			<td width="1150" height="100" valign="top">
					<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<display:table name="requestScope.frmAPInvoiceDtls.ldtDHR" class="gmits" id="currentRowObject" style="height:35;" 
								 requestURI="/gmAPSaveInvoiceDtls.do" sort="list" excludedParams="haction" freezeHeader="true" paneSize="200" decorator="com.globus.accounts.displaytag.beans.DTDHRListWrapper">
								    <display:column property="CNUM" title="" />
									<fmtInvoiceDHRList:message key="LBL_PACK_SLIP" var="varPackSlip"/><display:column property="PSLIP" title="${varPackSlip}" sortable="true"/>							
									<fmtInvoiceDHRList:message key="LBL_PART_NUMBER" var="varPartNumber"/><display:column property="PNUM" title="${varPartNumber}" sortable="true"/>
									<fmtInvoiceDHRList:message key="LBL_REC_DATE" var="varRecDate"/><display:column property="CDATE" title="${varRecDate}" sortable="true" format= "<%=strRptFmt%>"/>
					  				<fmtInvoiceDHRList:message key="LBL_CURR_STATUS" var="varCurrStatus"/><display:column property="CURSTS" title="${varCurrStatus}" sortable="true"/>								
					  				<fmtInvoiceDHRList:message key="LBL_QTY_REC" var="varQtyRec"/><display:column property="QTYREC" title="${varQtyRec}" sortable="true" class="alignright" total="true" format="{0,number,0}"/>							
					  				<fmtInvoiceDHRList:message key="LBL_QTY_ACC" var="varQtyAcc"/><display:column property="SHELFQTY" title="${varQtyAcc}" sortable="true" class="alignright" total="true" format="{0, number, 0}"/>
					  				<fmtInvoiceDHRList:message key="LBL_COST_EA" var="varCostEa"/><display:column property="COST" title="${varCostEa}" sortable="true" class="alignright" format="{0,number,#,###,###.00}"/>
									<fmtInvoiceDHRList:message key="LBL_REC_COST" var="varRecCost"/><display:column property="RECCOST" title="${varRecCost}" sortable="true" class="alignright" total="true" format="{0,number,#,###,###.00}"/>							
									<fmtInvoiceDHRList:message key="LBL_ACCP_COST" var="varAccpCost"/><display:column property="SHELFCOST" title="${varAccpCost}" sortable="true" class="alignright" total="true" format="{0,number,#,###,###.00}"/>
									<fmtInvoiceDHRList:message key="LBL_DHR_ID" var="varDhrId"/><display:column property="ID" title="${varDhrId}" sortable="true"/>
									<fmtInvoiceDHRList:message key="LBL_PFAMY" var="varPfamy"/><display:column property="PFAMYNM" title="${varPfamy}" sortable="true"/>
									<fmtInvoiceDHRList:message key="LBL_INV_AMT" var="varInvAmt"/><display:column property="INVAMT" title="${varInvAmt}" sortable="true" format="{0,number,#,###,###.00}"/>
									<fmtInvoiceDHRList:message key="LBL_EXT_INV_AMT" var="varExtInvAmt"/><display:column property="EXTINVAMT" title="${varExtInvAmt}" class="alignright" sortable="true" format="{0,number,#,###,###.00}"/>
									<fmtInvoiceDHRList:message key="LBL_CRE_AMT" var="varCreAmt"/><display:column property="CREAMT" title="${varCreAmt}" sortable="true" format="{0,number,#,###,###.00}"/>
									<fmtInvoiceDHRList:message key="LBL_DIFF" var="varDiff"/><display:column property="DIFF" title="${varDiff}" class="alignright" sortable="true" format="{0,number,#,###,###.00}"/>
									
								</display:table>
							</td>
						</tr>
					</table> 
  			   </td>
  		  </tr>
    </table>
</html:form>   
 <%@ include file="/common/GmFooter.inc" %> 		     	
</BODY>
</HTML>