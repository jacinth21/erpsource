<%
/************************************************************************************************************************************************
	* File		 	: GmFieldAuditVarianceFilterRpt.jsp
	* Desc		 	: This common included screen is used to display the Filter options in Variance By Dist and Variance by Set Report screens.
	* Version	 	: 1.0
	* author		: HReddi
*************************************************************************************************************************************************/
%> 

<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Iterator" %>
<%@ taglib prefix="fmtFieldAuditVarianceFilterRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- \accounts\fieldaudit\GmFieldAuditVarianceFilterRpt.jsp -->
<fmtFieldAuditVarianceFilterRpt:setLocale value="<%=strLocale%>"/>
<fmtFieldAuditVarianceFilterRpt:setBundle basename="properties.labels.accounts.fieldaudit.GmFieldAuditVarianceByDistRpt"/>
<bean:define id="reportType" name="frmFieldAuditReport" property="reportType" type="java.lang.String"> </bean:define>
<%
String strAccountFaJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_FIELDAUDIT");
String strProdmgmtJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
%>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_dhxcalendar.js"></script>
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strProdmgmtJsPath%>/GmFilter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strAccountFaJsPath%>/GmFieldAuditVarianceRpt.js"></script>
<script type="text/javascript">
<%

String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
%>

</script>
<script>	
	var reportType = '<%=reportType%>';
	var format = '<%=strApplnDateFmt%>';
</script>

<BODY>
<FORM name="<%=strFormName%>" method="POST" >
<input type="hidden" name="hAction" value="">
<html:hidden property="reportType"/>
            <!-- Custom tag lib code modified for JBOSS migration changes -->
            <tr>
                 <td height="30" align ="Right" class="RightTableCaption">&nbsp; <fmtFieldAuditVarianceFilterRpt:message key="LBL_DEVIATION_QTY" />:&nbsp;</td>
                 <td><table><tr>
                 <td width="50"> <gmjsp:dropdown controlName="devOpr" SFFormName="<%=strFormName%>" SFSeletedValue="devOpr" SFValue="alDevOpr" codeId = "CODENMALT"  codeName = "CODENM"  defaultValue= "[Choose One]"/> </td>
                 <td><html:text  property="devQty" name="<%=strFormName%>" size="10" tabindex="21" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>&nbsp;  </td>
               	 </tr></table></td>
                 <td height="30" align ="Right" class="RightTableCaption">&nbsp; <fmtFieldAuditVarianceFilterRpt:message key="LBL_DISPLAY_ONLY_DEVIATION" />:&nbsp;</td>
                 <td colspan="2" ><html:checkbox property="dispOnlyDeviation" name="<%=strFormName%>" /></td>             
            </tr>
            <tr>
            	<td colspan="5" class="LLine" height="1"></td>
            </tr>
            <tr class="shade">
                <td height="30" align ="Right" class="RightTableCaption">&nbsp;<fmtFieldAuditVarianceFilterRpt:message key="LBL_DISPLAY_UNRESOLVED_QTY" />:&nbsp;</td>
                <td height="30" class="RightTableCaption"><html:checkbox property="dispUnresolvedDet" name="<%=strFormName%>" />   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtFieldAuditVarianceFilterRpt:message key="LBL_DISPLAY_NON_APPROVED" />: <html:checkbox property="dispNonApproved" name="<%=strFormName%>" /> </td>
                <td height="30" align ="Right" class="RightTableCaption">&nbsp; <fmtFieldAuditVarianceFilterRpt:message key="LBL_DISPLAY_NON_RECONCILIATION" />:&nbsp;</td>
                <td><html:checkbox property="dispNonReconciliation" name="<%=strFormName%>" /> </td>                           
            </tr>
            <% if((reportType.equals("globalSet")) || (reportType.equals("globalDist"))){%>
            <tr>
            	<td colspan="5" class="LLine" height="1"></td>
            </tr>
			<tr>
            	<td height="30" align="Right" class="RightTableCaption">&nbsp;<fmtFieldAuditVarianceFilterRpt:message key="LBL_LOCKED_FROM" />:&nbsp;</td>
            	<td><gmjsp:calendar SFFormName="<%=strFormName%>" controlName = "lockedFrmDt"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/></td>
            	<td height="30" align="Right" class="RightTableCaption">&nbsp;<fmtFieldAuditVarianceFilterRpt:message key="LBL_LOCKED_TO" />:&nbsp;</td>
            	<td><gmjsp:calendar SFFormName="<%=strFormName%>" controlName="lockedToDt"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/></td>	
			</tr> 	
			<%}%>			
            <tr>
            	<td colspan="5" class="LLine" height="1"></td>
            </tr>  
            <% if((reportType.equals("globalSet")) || (reportType.equals("globalDist"))){%>         
            <tr class="shade"><%} else{%>
            <tr>	<%}%>
                <td height="30" align ="Right" class="RightTableCaption">&nbsp; <fmtFieldAuditVarianceFilterRpt:message key="LBL_SHOW_FLAGGED_DETAILS" />:&nbsp;</td>
                <td ><html:checkbox property="dispflaggedDet" name="<%=strFormName%>" />      </td>
                <td height="30" align ="Right" class="RightTableCaption">&nbsp; <fmtFieldAuditVarianceFilterRpt:message key="LBL_DISPLAY_NON_FLAGGED" />:&nbsp;</td>
                <td><html:checkbox property="dispNonflagged" name="<%=strFormName%>" />
                &nbsp;&nbsp;<fmtFieldAuditVarianceFilterRpt:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" gmClass="button" name="Submit" style="width: 6em" buttonType="Load" onClick="fnSubmit();" /> 
                </td>                     
            </tr>           	
		</tr>

</FORM>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>