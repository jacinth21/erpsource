 <%
/**********************************************************************************
 * File		 		: GmCallLogList.jsp
 * Desc		 		: This screen is used to Payment List for the selected customer
 * Version	 		: 1.0
 * author			: Richard
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@page import="com.globus.common.beans.GmGridFormat"%>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtCallLogList" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmCallLogList.jsp -->
<fmtCallLogList:setLocale value="<%=strLocale%>"/>
<fmtCallLogList:setBundle basename="properties.labels.accounts.GmCallLogList"/>
<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
	String strAction 	= GmCommonClass.parseNull((String)request.getAttribute("hAction"));

	String strInvID = GmCommonClass.parseNull((String)request.getAttribute("hInvID"));
	String strID 	= GmCommonClass.parseNull((String)request.getAttribute("hID"));
	String strUserId=GmCommonClass.parseNull((String)request.getAttribute("hUsrId"));
	String strComments=GmCommonClass.parseNull((String)request.getAttribute("hComments"));
	String strJspHeader = GmCommonClass.parseNull((String)request.getAttribute("JspHeader"));
	ArrayList alEmpList=new ArrayList();
	String strGridXmlData = GmCommonClass.parseNull((String)request.getAttribute("XMLGRIDDATA"));
	alEmpList=(ArrayList)request.getAttribute("alEmpList");
	
%>

<HTML>
<HEAD>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: Sales Orders Report</title>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmCallLogList.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css"> 

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
var objGridData;
objGridData ='<%=strGridXmlData%>';

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onkeypress="fnEnterPress();" onload="fnOnPageLoad();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmCommonLogRetrieveServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">

<table class="DtTable1200" cellspacing="0" cellpadding="0">
	<tr><td colspan="5" height="25" class="RightDashBoardHeader" ><%=strJspHeader %></td></tr>
	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<tr>
		<td><jsp:include page="../include/GmFromToDateInc.jsp" flush="true" /></td>
		<fmtCallLogList:message key="LBL_REF_ID" var="varRefId"/>
		<gmjsp:label type="RegularText"  SFLblControlName="${varRefId}:" align="right"/>
		<td>&nbsp; <input type="text" size="20" value="<%=strID%>"  name="Txt_ID" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
		onBlur="changeBgColor(this,'#ffffff');" > </td>
	</tr>
	<tr><td class="LLine" height="1" colspan="5"></td></tr>
	<tr>
	   <td class="RightTablecaption" >&nbsp;&nbsp;&nbsp;&nbsp;	
	   <fmtCallLogList:message key="LBL_USER_NAME" var="varUserName"/>
	   <gmjsp:label type="BoldText"  SFLblControlName="${varUserName}:" align="right" td="false"/>
	   <gmjsp:dropdown controlName="Cbo_UsrNmList"  seletedValue="<%= strUserId %>" 	
		  tabIndex="1" value="<%=alEmpList%>" defaultValue= "[Choose One]"/>
	   </td> 
	   <fmtCallLogList:message key="LBL_COMMENTS" var="varComments"/>
		<gmjsp:label type="BoldText"  SFLblControlName="${varComments}:" align="right"/>
	    <td>&nbsp;&nbsp;<input type="text" size="20" value="<%=strComments%>"  name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" />  </td>   
	     <td></td>
		<td align="left" valign="middle">
		<fmtCallLogList:message key="BTN_LOAD" var="varLoad"/>
		<gmjsp:button value="&nbsp;${varLoad}&nbsp;" name="Btn_Go" gmClass="button" buttonType="Load" onClick="javascript:fnSubmit();"/> </td>
	</tr>
	<%if(!strGridXmlData.equals("")){ %>
	<tr>
		<td colspan="5">	
		<div id="OrderCmtLog" style="height: 680px; width:1200px;"></div>
		</td>
	</tr>
	<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>
     		<tr>
			<td colspan="5" align="center">
			    <div class='exportlinks'><fmtCallLogList:message key="LBL_EXPORT_OPTS"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnExcel();"><fmtCallLogList:message key="LBL_EXCEL"/></a></div>
			</td>
		</tr>
		<%}else{ %>
		<tr>
		<td><fmtCallLogList:message key="LBL_NTG_TO_DIS"/></td>
		</tr>
		<%} %>
</table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
