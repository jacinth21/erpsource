 <%
/**********************************************************************************
 * File		 		: GmIssueMemo.jsp
 * Desc		 		: This screen is used to generate the Issue Memo
 * Version	 		: 1.0
 * author			: 
************************************************************************************/
%>
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap"%>
<bean:define id="gridData" name="frmIssueMemo" property="gridData" type="java.lang.String"></bean:define>
<bean:define id="strARRptByDealerFlag" name="frmIssueMemo" property="strARRptByDealerFlag" type="java.lang.String"></bean:define>
<%@ taglib prefix="fmtIssueMemo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmIssueMemo.jsp -->
<fmtIssueMemo:setLocale value="<%=strLocale%>"/>
<fmtIssueMemo:setBundle basename="properties.labels.accounts.GmIssueMemo"/>
<%
	String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
	String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("ISSUE_MEMO"));
	String strApplDateFmt = GmCommonClass.parseNull((String) session.getAttribute("strSessApplDateFmt"));
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Issue Memo</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmIssueMemo.js"></script>

<script>
var lblCredit='<fmtIssueMemo:message key="VM_CREDIT"/>';
var lblDebit='<fmtIssueMemo:message key="VM_DEBIT"/>';
var lblPriceEA='<fmtIssueMemo:message key="LBL_PRICE_EA"/>';
var lblQty='<fmtIssueMemo:message key="VM_QTY"/>';
var lblAmt = '<fmtIssueMemo:message key="LBL_AMOUNT"/>';

var objGridData;
var rowID = '';
objGridData = '<%=gridData%>';
	var gridObj = '';
	var myRowId = 1;
	// global variable for the index
	var inv_rowId = '';
	var part_rowId = '';
	var accName_rowId = '';
	var qty_rowId = '';
	var price_rowId = '';
	var userQty_rowId = '';
	var userPrice_rowId = '';
	var totalAmt_rowId = '';
	var accID_rowId = '';
	var orderID_rowId = '';
	var custPO_rowId = '';
	var itemType_rowId = '';
	var comments_rowId = '';
	// Validation purpose
	var blFirstRec = false;
	var firstAccRowID = 1;
	var blOtherAcc = false;
	var lblMemoType = '<fmtIssueMemo:message key="LBL_MEMO_TYPE"/>';
	var arDealerFlag = '<%=strARRptByDealerFlag%>';
</script>

</HEAD>


<BODY leftmargin="20" topmargin="10" onkeyup="fnEnter();" onload="fnOnPageLoad();">

	<html:form action="/gmIssueMemo.do?method=loadIssueMemoDtls">
		<html:hidden property="strOpt" value="" />
		<html:hidden property="inputString" value="" />

		<table border="0" class="DtTable1000" width="700" cellspacing="0"
			cellpadding="0">
			<tr>
				<td colspan="5" height="25" class="RightDashBoardHeader"><fmtIssueMemo:message key="LBL_ISSUE_CREDIT"/></td>
				<fmtIssueMemo:message key="LBL_HELP" var="varHelp"/>
				<td height="25" class="RightDashBoardHeader"><img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td colspan="6" class="LLine" height="1"></td>
			</tr>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<tr>
				<fmtIssueMemo:message key="LBL_MEMO_TYPE" var="varMemoType"/>
				<td height="30" align="Right" class="RightTableCaption" colspan="3"><gmjsp:label type="MandatoryText"
						SFLblControlName="${varMemoType}:" td="false" /></td>
				<td colspan="3" align="left">&nbsp;<gmjsp:dropdown
						controlName="memoType" SFFormName="frmIssueMemo"
						SFSeletedValue="memoType" SFValue="alMemoType" codeId="CODEID"
						codeName="CODENMALT" defaultValue="[Choose One]" tabIndex="1"
						onChange="fnChangeMemo(this);" />
				    <font style="padding-left:5em;">&nbsp;</font><b><fmtIssueMemo:message key="LBL_CURRENCY"/>:</b>&nbsp;
					<gmjsp:dropdown	controlName="strCompCurrency" SFFormName="frmIssueMemo"
					SFSeletedValue="strCompCurrency" SFValue="alCompCurrency" codeId="ID" codeName="NAMEALT" />	</td>

			</tr>
			<tr>
				<td colspan="6" class="LLine" height="1"></td>
			</tr>

			<tr>
				<td class="RightTableCaption" align="left" colspan="6" height="20">
					<table cellpadding="1" cellspacing="1" id="addTbl">
						<TR>
						<fmtIssueMemo:message key="LBL_ADD_ROW" var="varAddRow"/>
						<fmtIssueMemo:message key="LBL_REMOVE_ROW" var="varRemoveRow"/>
							<td class="RightTableCaption"><a
								href="javascript:addRow();"> <img
									src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="${varAddRow}"
									style="border: none;" height="14"> </a><a
								href="javascript:removeSelectedRow();"> <img
									src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="${varRemoveRow}"
									style="border: none;" height="14"> </a>
							</td>
						</TR>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="6">
					<div id="issueMemoRpt" style="" height="400px" width="1000px"></div>
				</td>
			</tr>

			<tr height="40">
				<td colspan="6" align="center" id="AllButton">
					<fmtIssueMemo:message key="BTN_SUBMIT" var="varSubmit"/>
					<gmjsp:button value="${varSubmit}" gmClass="Button" name="btn_Submit"
					style="width: 6em" onClick="fnSubmit();" buttonType="Save" />
				</td>
			</tr>

		</table>



	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>