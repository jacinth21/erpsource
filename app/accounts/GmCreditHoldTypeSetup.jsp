
<%
  /**********************************************************************************
 * File		 		: GmCreditHoldTypeSetup.jsp
 * Desc		 		: Account credit hold type setup screen
 * author			: Gabriel
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page
	import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.List"%>
	<%@ taglib prefix="fmtCreditHoldType" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmCreditHoldTypeSetup.jsp -->
<fmtCreditHoldType:setLocale value="<%=strLocale%>"/>
<fmtCreditHoldType:setBundle basename="properties.labels.accounts.GmCreditHoldTypeSetup"/>
<bean:define id="alSubTypeCtrl" name="frmCreditHold"
	property="alSubTypeCtrl" type="java.util.ArrayList"></bean:define>
<bean:define id="alSubTypeValue" name="frmCreditHold"
	property="alSubTypeValue" type="java.util.ArrayList"></bean:define>
<%

String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
%>
<html>
<head>
<title>Account Credit Hold Type</title>
<script language="javascript" src="<%=strAccountJsPath%>/GmCreditHoldSetup.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
</head>
<body>
	<html:form action="/gmCreditHold.do?method=fetchAcctCreditHold">
		<html:hidden property="strOpt" />
		<html:hidden property="creditAttrString" />
		<table class="DtTable500" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" colspan="5" class="RightDashBoardHeader">&nbsp;<fmtCreditHoldType:message key="LBL_ACOUNT_CREDOT_HOLD_TYPE"/></td>
					<fmtCreditHoldType:message key="LBL_HELP" var="varHelp"/>
				<td height="25" class="RightDashBoardHeader" align="right"><img
					id='imgEdit' align="right" style='cursor: hand'
					src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'
					height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("CREDIT_HOLD_TYPE_FUNC_ALLOWED")%>');" /></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="7"></td>
			</tr>
			<tr class="shade">
				<td colspan="3" class="RightTableCaption" align="right" height="25"><fmtCreditHoldType:message key="LBL_ACCOUNT_NAME"/>:</td>
				<td colspan="5">&nbsp;<b><span id="accountName"><bean:write
								name="frmCreditHold" property="accountName" /></span></b></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="7"></td>
			</tr>
			<tr>
				<td colspan="3" class="RightTableCaption" align="right" height="25"><fmtCreditHoldType:message key="LBL_CREDIT_HOLD"/>:</td>
				<td colspan="5">&nbsp;<gmjsp:dropdown
						controlName="creditHoldType" SFFormName="frmCreditHold"
						SFSeletedValue="creditHoldType" disabled="disabled"
						defaultValue="[Choose One]" SFValue="alCreditHoldList"
						codeId="CODEID" codeName="CODENM" />
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="7"></td>
			</tr>
			<tr class="shade">
				<td colspan="3" class="RightTableCaption" align="right" HEIGHT="25"><fmtCreditHoldType:message key="LBL_FUNCTIONS_ALLOWED"/>:</td>
				<td colspan="5">&nbsp;</td>

			</tr>
			<%
			int alSubTypeCtrlSize = alSubTypeCtrl.size();
			int alSubTypeValueSize = alSubTypeValue.size();
			  boolean blExcludeVATlbl = false;
			    String strAttValue = "N/A";
			    HashMap hcboVal = new HashMap();
			    for (int i = 0; i < alSubTypeCtrlSize; i++) {
			      hcboVal = (HashMap) alSubTypeCtrl.get(i);
			      String strCodeID = GmCommonClass.parseNull((String) (String) hcboVal.get("CODEID"));
			      String strCodeNm = GmCommonClass.parseNull((String) (String) hcboVal.get("CODENM"));
			      String strCntrl = "AccNm" + i;
			      String strShade = "Shade";

			      if (i % 2 == 0) {
			        strShade = "";
			      }
			%>
			<tr class="<%=strShade%>">
				<td colspan="3" class="RightTableCaption" HEIGHT="24" align="right"><%=strCodeNm%>:
				</td>
				<%
				  if (alSubTypeCtrlSize > 0) {

				        for (int j = 0; j < alSubTypeValueSize; j++) {

				          HashMap hcboValue = (HashMap) alSubTypeValue.get(j);
				          String strAttID = (String) hcboValue.get("CREDITSUBTYPE");


				          if (strCodeID.equalsIgnoreCase(strAttID)) {
				            strAttValue = (String) hcboValue.get("CREDITVALUENM");
				          }
				%>

				<%
				  }
				      }
				%>
				<td align="left" colspan="5">&nbsp;<%=strAttValue%></td>
			</tr>
			<tr>
				<td colspan="7" height="1" class="LLine"></td>
			</tr>
			<%
			  }
			%>

			<tr>
				<fmtCreditHoldType:message key="BTN_CLOSE" var="varClose"/>
				<td colspan="7" align="center" height="30">&nbsp; <gmjsp:button
						value="${varClose}" name="Btn_Close" gmClass="button" buttonType="Load"
						onClick="javascript:fnClosePopup();" />&nbsp;
				</td>
			</tr>
		</table>
	</html:form>
</body>
<%@ include file="/common/GmFooter.inc"%>
</html>