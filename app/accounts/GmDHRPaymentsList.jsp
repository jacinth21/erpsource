 <%
/**********************************************************************************
 * File		 		: GmDHRPaymentsList.jsp
 * Desc		 		: This screen is used to display DHR Payments Report
 * Version	 		: 1.0
 * author			: James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtDhrPaymentList" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmDHRPaymentsList.jsp -->
<fmtDhrPaymentList:setLocale value="<%=strLocale%>"/>
<fmtDhrPaymentList:setBundle basename="properties.labels.accounts.GmDHRPaymentsList"/>				
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = new HashMap();	
	HashMap hmCurrency = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	
	HashMap hmParam = new HashMap();
	hmParam = (HashMap)request.getAttribute("hmParam");
	
	ArrayList alVendorList = new ArrayList();
	alVendorList = (ArrayList)hmReturn.get("VENDORLIST");
	
	ArrayList alPOTYPE = new ArrayList();
	alPOTYPE = (ArrayList)request.getAttribute("alPOTYPE");
	ArrayList alCreatedBy = new ArrayList();
	alCreatedBy = (ArrayList)request.getAttribute("alCreatedBy");
	
	String strAction = (String)request.getAttribute("hAction");
	String strPOID 	= "";
	String strVendorID = "";
	String strPOType = "";
	String strInvID = "";
	String strPostedBy = "";
	
	if (hmParam != null)
	{
		strVendorID = GmCommonClass.parseNull((String)hmParam.get("VENDID"));
		strPOID = GmCommonClass.parseNull((String)hmParam.get("POID"));
		strPOType = GmCommonClass.parseNull((String)hmParam.get("POTYPE"));
		strInvID = GmCommonClass.parseNull((String)hmParam.get("INVID"));
		strPostedBy = GmCommonClass.parseNull((String)hmParam.get("POSTBY"));
	}
	String strApplDateFmt = strGCompDateFmt;
	String strDateFmt = "{0,date,"+strApplDateFmt+"}";

	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strCurrSign =(String)hmCurrency.get("CMPCURRSMB");
	String strApplCurrFmt = (String)hmCurrency.get("CMPCURRFMT");
	String strCurrFmt = "{0,number,"+strCurrSign+" "+strApplCurrFmt+"}";
%>

<%
String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: DHR Payments Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     #Cbo_VendorId{width:85%;}
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmDHRPaymentsList.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmDHRFlagPaymentServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">

<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
	<tr><td height="25" class="RightDashBoardHeader" colspan="6"><fmtDhrPaymentList:message key="LBL_PAYMENTS_REPORT"/></td></tr>
	<tr><td colspan="6" height="1" class="Line"></td></tr>
	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<tr height="25">
		<td class="RightTableCaption" align="Right"><fmtDhrPaymentList:message key="LBL_VENDOR"/>:</td>
		<td width= "15" style="padding-left: 5px;"><gmjsp:dropdown controlName="Cbo_VendorId"  seletedValue="<%=strVendorID%>"  defaultValue= "[All Vendors]"	
							tabIndex="1"  value="<%=alVendorList%>" codeId="ID" codeName="NAME"/></td>
		<td class="RightTableCaption" align="Right" display= "inline-block"><fmtDhrPaymentList:message key="LBL_PAYMENT_DATE"/>:</td>
		<td><jsp:include page="../include/GmFromToDateInc.jsp" flush="true" /></td>
		<td width="1" rowspan="5" class="LLine"></td>
		<td align="center" valign="middle" rowspan="5">
			<fmtDhrPaymentList:message key="BTN_GO" var="varGo"/>
			<gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" name="Btn_Go" gmClass="button" onClick="javascript:fnSubmit();" buttonType="Load" />
		</td>
	</tr>
	<tr><td colspan="4" height="1" class="LLine"></td></tr>
	<tr class="Shade" height="25">
		<td class="RightTableCaption" align="Right"><fmtDhrPaymentList:message key="LBL_PO_ID"/>:</td>
		<td>&nbsp;<input type="text" size="20" value="<%=strPOID%>" name="Txt_POId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1></td>
		<td class="RightTableCaption" align="Right"><fmtDhrPaymentList:message key="LBL_PO_TYPE"/>:</td>
		<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Type"  seletedValue="<%=strPOType%>"  defaultValue= "[Choose One]"	
							tabIndex="1"  value="<%=alPOTYPE%>" codeId="CODEID" codeName="CODENM"/></td>
	</tr>
	<tr><td colspan="4" height="1" class="LLine"></td></tr>
	<tr height="25">
		<td class="RightTableCaption" align="Right"><fmtDhrPaymentList:message key="LBL_INVOICE_ID"/>:</td>
		<td>&nbsp;<input type="text" size="20" value="<%=strInvID%>" name="Txt_InvId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1></td>
		<td class="RightTableCaption" align="Right"><fmtDhrPaymentList:message key="LBL_POSTED_BY"/>:</td>
		<td>&nbsp;<gmjsp:dropdown controlName="Cbo_PostBy"  seletedValue="<%=strPostedBy%>"  defaultValue= "[Choose One]"	
							tabIndex="1"  value="<%=alCreatedBy%>" codeId="ID" codeName="NAME"/></td>
	</tr>
	<tr><td colspan="6" height="1" class="LLine"></td></tr>
	<tr>
		<td height="30" class="aligncenter" colspan="6">
			<display:table name="requestScope.results.rows" id="currentRowObject" export="true" class="its" varTotals="totals" decorator="com.globus.displaytag.beans.DTDHRWrapper">
			  <fmtDhrPaymentList:message key="LBL_VENDOR_NAME" var="varVendorName"/>	<display:column property="VNAME" title="${varVendorName}" sortable="true" />
			  <fmtDhrPaymentList:message key="LBL_INOVICE_ID" var="VarInoviceId"/>	<display:column property="INVID" title="${VarInoviceId}" sortable="true" />
			  <fmtDhrPaymentList:message key="LBL_DHR_ID" var="varDhrId"/>	<display:column property="ID" title="${varDhrId}" sortable="true" style="white-space: nowrap" />
			  <fmtDhrPaymentList:message key="LBL_PART_NUMBER" var="varPartNumber"/>	<display:column property="PNUM" title="${varPartNumber}" sortable="true" class="aligncenter"/>
			  <fmtDhrPaymentList:message key="LBL_DATE_ENTERED" var="varDateEntered"/>	<display:column property="ENTDT" title="${varDateEntered}" sortable="true"  format= "<%=strDateFmt%>"/>
			  <fmtDhrPaymentList:message key="LBL_PAYMENT_DATE" var="varPaymentDate"/>	<display:column property="PAYDT" title="${varPaymentDate}" sortable="true" format= "<%=strDateFmt%>"/>
			  <fmtDhrPaymentList:message key="LBL_PAYMENT_AMOUNT" var="varPaymentAmount"/>	<display:column property="PAYAMT" class="alignright" title="${varPaymentAmount}" total="true" sortable="true" format="<%=strCurrFmt%>" />
			  <fmtDhrPaymentList:message key="LBL_PO_TYPE" var="varPoType"/>	<display:column property="POTYPE" title="${varPoType}" sortable="true" />
			  <fmtDhrPaymentList:message key="LBL_COMMENTS" var="varComments"/>	<display:column property="COMMENTS" title="${varComments}"/>
			  <fmtDhrPaymentList:message key="LBL_POSTED_BY" var="varPostedBy"/>	<display:column property="UNAME" title="${varPostedBy}"/>
				<display:footer media="html"> 
<% 
				String strVal ;
				HashMap strTest = (HashMap)pageContext.getAttribute("totals");
				strVal = ((HashMap)pageContext.getAttribute("totals")).get("column7").toString();
		  		String strCurVal = strCurrSign + GmCommonClass.getStringWithCommas(strVal);
%>
			  	<tr class = shade>
				  		<td colspan="5">&nbsp;</td>
					  		<td class = "alignright"> <B> <fmtDhrPaymentList:message key="LBL_TOTAL"/>: </B></td>
					  		<td class = "alignright" ><B><%=strCurVal%></B></td>
			  	</tr>
			 </display:footer>
			</display:table>
 		</td>
	</tr>
</table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
