 <%
/**********************************************************************************
 * File		 		: GmInvoicePrint.jsp
 * Desc		 		: This screen is used for the Order Maintenance
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>

 <!-- \accounts\GmInvoicePrint.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));

	String strAction = (String)request.getAttribute("hAction");
	String strInvoiceMode = "";
	if (strAction == null)
	{
		strAction = (String)session.getAttribute("hAction");
	}
	strAction = (strAction == null)?"Load":strAction;

	HashMap hmReturn = new HashMap();

	ArrayList alOrderNums = new ArrayList();
	HashMap hmCartDetails = new HashMap();
	HashMap hmConstructs = new HashMap();
	
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	strInvoiceMode = GmCommonClass.parseNull((String)request.getAttribute("hVoidFlag"));

	HashMap hmOrderDetails = new HashMap();
	HashMap hmShipDetails = new HashMap();
	
	HashMap hmThirdParty = new HashMap();
	
	String strShade = "";

	String strInvNum = "";
	String strAccId= "";
	String strDistRepNm = "";
	String strBillAdd = "";
	java.sql.Date dtInvDate = null;
	java.sql.Date dtDueDate = null;
	String strPO = "";
	String strOrdId = "";

	String strPartNum = "";
	String strDesc = "";
	String strPrice = "";
	String strQty = "";
	String strItemOrdId = "";
	String strPartNums = "";
	String strControlNum = "";
	String strPayNm = "";
	String strPay = "";
	String strDueDate = "";
	String strInvType = "";
	String strInvTitle = "";
	String strParentInvId = "";
	String strCreditInvId = "";

	String strLabel = "Generate";

	String strConFlag = "";
	String strConstructId = "";
	String strConstructNm = "";
	String strConstructValue = "";
	String strInterComInv = "";
	
	String strPartyFlag = "";
	String strHeaderAddress = ""; 
	String strPartyNm = "";
	String strRemitAddress = ""; 
	String strInvoiceNote = "";
	String strCompId = ""; 
	String strCurrency ="";
	
	
	if (hmReturn != null)
	{
		hmOrderDetails = (HashMap)hmReturn.get("ORDERDETAILS");
		alOrderNums = (ArrayList)hmReturn.get("ORDERNUMS");
		hmConstructs = (HashMap)hmReturn.get("CONSTRUCTDETAILS");
		
	//	hmThirdParty = (HashMap)hmReturn.get("THIRDPARTYINFO"); 
		strPartyFlag = (String)hmReturn.get("PARTYFLAG");
		strPartyNm = (String)hmReturn.get("PARTYNAME");
	 
		strHeaderAddress = (String)hmReturn.get("HEADERADDRESS");
		strRemitAddress = (String)hmReturn.get("REMITADDRESS");
		
		hmCartDetails = (HashMap)hmReturn.get("CARTDETAILS");
		strInvNum = (String)hmOrderDetails.get("INV");
		strAccId = (String)hmOrderDetails.get("ACCID");
		strPO = (String)hmOrderDetails.get("PO");
		strBillAdd = (String)hmOrderDetails.get("BILLADD");
		strDistRepNm = (String)hmOrderDetails.get("REPDISTNM");
		dtInvDate = (java.sql.Date)hmOrderDetails.get("INVDT");
		dtDueDate = (java.sql.Date)hmOrderDetails.get("DUEDT");
		strPayNm = GmCommonClass.parseNull((String)hmOrderDetails.get("PAYNM"));
		strPay = (String)hmOrderDetails.get("PAY");
		strInvType = GmCommonClass.parseNull((String)hmOrderDetails.get("INVTYPE"));
		//strInvTitle =  ((String)hmOrderDetails.get("INVTITLE"))==null?"Invoice":((String)hmOrderDetails.get("INVTITLE"));
		strInvTitle =(String)request.getAttribute("INVOICETITLE");
		strParentInvId = GmCommonClass.parseNull((String)hmOrderDetails.get("PARENT_INV_ID"));
		strPayNm = strPayNm.equals("")?"Net 30 Days":strPayNm;
		strInterComInv =(String)request.getAttribute("COMMERCIALINV");
		strInvoiceNote = GmCommonClass.parseNull((String)hmOrderDetails.get("INVOICENOTE"));
		strCompId = GmCommonClass.parseNull((String)hmOrderDetails.get("COMPID"));
		strCurrency = GmCommonClass.parseNull((String)hmOrderDetails.get("CURRENCY"));
	}

	if(strInvType.equals("50202") || strInvType.equals("50203"))
	{
		strCreditInvId = strInvNum;
		strInvNum = strParentInvId;
		strCreditInvId = "<b>" + strCreditInvId + "</b>";
	}else
		strInvNum =  "<b>" + strInvNum + "</b>";

	int intSize = 0;
	ArrayList alLoop = null;
	HashMap hmTemp = null;
	HashMap hmLoop = null;
	ArrayList alConsLoop = null;
	HashMap hmConsLoop = null;	
	 
	if(strCurrency.equals("USD") || strCurrency.equals("")){
		strCurrency = "$";
	}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Invoice Print </TITLE>
<script language="JavaScript" src="<%=strJsPath%>/watermark.js"></script>
<script>

function fnPrintInvoice()
{
windowOpener("/GmInvoiceServlet?hId=<%=strInvNum%>&hAction=Print","PrntInv","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=400");
}


function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}

</script>
</HEAD>

<BODY topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmOrder" method="post" action = "<%=strServletPath%>/GmInvoiceServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hPO" value="<%=strPO%>">
<input type="hidden" name="hInv" value="<%=strInvNum%>">
<input type="hidden" name="hAccId" value="<%=strAccId%>">
<input type="hidden" name="hPay" value="<%=strPay%>">

<BR>
	<table border="0" width="700" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td bgcolor="#666666" colspan="3"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="6"></td>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
					<%if(strPartyFlag.equals("Y")) {%>

					<td class="RightText" width="270" valign="top"> <br> 

					<% } else {	  %>
					
						<td class="RightText" width="270" valign="top"><img src="<%=strImagePath%>/<%=strCompId%>.gif" width="138" height="60">  
				 		<% } %>
				 		
				 	 <%=strHeaderAddress%></td>
				 		
					<%if(strInterComInv.equals("yes")) {%>	
						<td class="RightText" valign="middle" height="120" width="100"><td>
					
						<td valign="top" class="RightText" wrap="wrap"><font size="+3"><%=strInvTitle%></font>&nbsp;<BR><BR>
						<table>
					<tr>
					<td width="150"></td>
					<td>
					 
					&nbsp;<%=strRemitAddress %> 
					 					
					</td>
					</tr>
					</table>
					</td>	
					<%} if(!strInterComInv.equals("yes")) {%>
						<td class="RightText" valign="middle" height="120" width="260"><td>
						<td valign="top" class="RightText"><font size="+3"><%=strInvTitle%></font>&nbsp;<BR><BR>
						 
						&nbsp; 	<%=strRemitAddress %>					
						</td>
					<%}  
					%>													
					</tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1" rowspan="6"></td>
		</tr>
			<td bgcolor="#666666" colspan="3"></td>
		<tr>
			<td width="698" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr bgcolor="#eeeeee" class="RightTableCaption">
									<td height="23" align="center" width="400">&nbsp;Bill To</td>
									<td bgcolor="#666666" width="1" rowspan="7"></td>
									<td align="center">&nbsp;Customer PO #</td>
									<td bgcolor="#666666" width="1" rowspan="7"></td>
									<td width="80" align="center">&nbsp; Date</td>
									
									<%  if(strInvType.equals("50202"))
												{	%>
									<td width="80" align="center">&nbsp;Credit Memo #</td>
										<% }else if(strInvType.equals("50203")){ %>
											<td width="80" align="center">&nbsp;Debit Memo #</td>
									<%}else { %>	
										<td width="80" align="center">&nbsp;Terms</td>
									<% } %>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="6"></td>
								</tr>
								<tr>
									<td class="RightText" rowspan="5" valign="top">
										<table>
											<tr>
												<td width="25">&nbsp;</td>
												<td width="300" class="RightTableCaption"><BR><BR>&nbsp;<%=strBillAdd%>
												</td>
											</tr>
										</table>&nbsp;<BR><BR>
									</td>
									<td class="RightText" align="center">&nbsp;<%=strPO%></td>
									<td height="23" class="RightText" align="center">&nbsp;<%=GmCommonClass.getStringFromDate(dtInvDate,strApplnDateFmt)%></td>
									<%  if(strInvType.equals("50202") || strInvType.equals("50203"))
												{	%>
									<td class="RightText" align="center">&nbsp;<%= strCreditInvId%></td>
											<% } else { %>	
									<td class="RightText" align="center">&nbsp;<%=strPayNm%></td>
									<% } %>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
								</tr>
								<tr bgcolor="#eeeeee" class="RightTableCaption">
									<td class="RightText" align="center" height="18">Invoice #</td>
									<td align="center" colspan="1">Account ID</td>
									<td align="center" colspan="1">Dist/Rep</td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
								</tr>
								<tr>
									<td class="RightText" rowspan="3" align="center">&nbsp;<%=strInvNum%></td>
									<td align="center" class="RightText" colspan="1">&nbsp;<%=strAccId%></td>
									<td align="center" class="RightText" colspan="1">&nbsp;<%=strDistRepNm%><Br></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" height="1" bgcolor="#666666"></td>
					</tr>
					<tr>
						<td align="center" colspan="2" valign="top"  height="500">
							<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0">
								<tr bgcolor="#eeeeee" class="RightTableCaption">
									<td width="70" height="25">&nbsp;Order #</td>
									<td width="60" height="25">&nbsp;Part #</td>
									<td width="390">Description</td>
									<td align="center" width="40">Qty</td>
									<td align="center" width="80">Price EA</td>
									<td align="center" width="90">Amount</td>
								</tr>
								<tr>
									<td colspan="6" height="1" bgcolor="#666666"></td>
								</tr>
<%
					int intLoop = 0;
			  		intSize = alOrderNums.size();
					alLoop = new ArrayList();
					String strItems = "";
					int intQty = 0;
					double dbItemTotal = 0.0;
					String strItemTotal = "";
					double dbTotal = 0.0;
					String strTotal = "";
					String strShipCost = "";
					String strOrderComments = "";
					double dblShip = 0.0;
					int intConLoop = 0;
					String strSpace = "";
					String strOrderType = "";
					String strRAId = "";
					// To get the Order Information 
			  		for (int i=0;i<intSize;i++)
			  		{
				  		hmTemp = (HashMap)alOrderNums.get(i);
						strOrdId = (String)hmTemp.get("ID");
					 if(strOrdId.endsWith("S")!=true) // remove return order informations
					 {
							strOrderComments = GmCommonClass.parseNull((String)hmTemp.get("CMENT"));
							strOrderType = GmCommonClass.parseNull((String)hmTemp.get("ORDERTYPE"));
							strRAId = GmCommonClass.parseNull((String)hmTemp.get("RMAID"));
							// L to C swap (back order)
							if(strOrderType.equals("2525") && !strRAId.equals("")){
								break;
							}
							alLoop = (ArrayList)hmCartDetails.get(strOrdId);
							intLoop = alLoop.size();
												
							alConsLoop = (ArrayList)hmConstructs.get(strOrdId);
							intConLoop = alConsLoop.size();
												
							if (intConLoop > 0)
							{
								for ( int j=0;j<intConLoop;j++)
								{
									hmConsLoop = (HashMap)alConsLoop.get(j);
									strConstructId = (String)hmConsLoop.get("CONID");
									strConstructNm = (String)hmConsLoop.get("CONNM");
									strConstructValue = (String)hmConsLoop.get("CVALUE");
									dbItemTotal = Double.parseDouble(strConstructValue);
									dbTotal = dbTotal + dbItemTotal;
%>
									<tr class="RightTableCaption">
										<td class="RightText" height="20">&nbsp;<%=strOrdId%></td>
										<td class="RightText">&nbsp;<%=strConstructId%></td>
										<td class="RightText">&nbsp;<%=strConstructNm%></td>
										<td colspan="2">&nbsp;</td>
										<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strConstructValue)%>&nbsp;&nbsp;</td>
									</tr>
<%
									strOrdId = "";
								}
							}
												
						// To get the Item Information
							for ( int j=0;j<intLoop;j++)
							{
								hmLoop = (HashMap)alLoop.get(j);
								strPartNum = (String)hmLoop.get("ID");
								strDesc = (String)hmLoop.get("PDESC");
								strPrice = (String)hmLoop.get("PRICE");
								strQty = (String)hmLoop.get("QTY");
								strControlNum = (String)hmLoop.get("CNUM");
								strConFlag = GmCommonClass.parseNull((String)hmLoop.get("CONFL"));
							
								if (strConFlag.equals(""))
								{
									intQty = Integer.parseInt(strQty);
									dbItemTotal = Double.parseDouble(strPrice);
									dbItemTotal = intQty * dbItemTotal; // Multiply by Qty
									strItemTotal = ""+dbItemTotal;
									dbTotal = dbTotal + dbItemTotal;
									strTotal = ""+dbTotal;
								}
							
								strSpace = strConFlag.equals("")?"":"&nbsp;&nbsp;&nbsp;";

								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
%>								<tr>
<%								if ( j == 0) {	
%>										<td class="RightTextAS" rowspan="<%=intLoop%>" height="20">&nbsp;<%=strOrdId%></td>
<%								}%>
										<td class="RightText" height="20"><%=strSpace%>&nbsp;<%=strPartNum%></td>
										<td class="RightText" >&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
										<td class="RightText" align="center">&nbsp;<%=GmCommonClass.getRedText(strQty)%></td>
<%
								if (!strConFlag.equals(""))
								{
%>
										<td class="RightText" align="center" colspan="2" valign="middle">Construct Pricing</td>
<%
								}else{
%>
										<td class="RightText" align="right" valign="middle"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strPrice))%>&nbsp;</td>
										<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strItemTotal))%>&nbsp;</td>
<%
								}
%>
									</tr>
<%					
								if ( j == intLoop -1)
								{	
									strShipCost = GmCommonClass.parseZero((String)hmLoop.get("SCOST"));
								
									dblShip = Double.parseDouble(strShipCost);
									dbTotal = dbTotal + dblShip;
									strTotal = ""+dbTotal;
%>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td class="RightText">&nbsp;Shipping Charges</td>
									<td colspan="2">&nbsp;</td>
									<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strShipCost))%>&nbsp;</td>
								</tr>
					<% if
					(!strOrderComments.equals(""))
							{
					%>								
								<tr>
									<td class="RightText" colspan="6">
									<img src="<%=strImagePath%>/icon_pen.gif" width="30" height="20">
									<font size="-2"><i><%=strOrderComments%>&nbsp;</i></font></td>
								</tr>
							
					<% } %>							
							
<%
								}
							}
					  	}	
					}
%>
								<tr height="100%"><td colspan="6"></td></tr>
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
								<tr>
									<td colspan="4" height="30" class="RightText">&nbsp;-Please make checks payable to  
									<b><%= strPartyNm %></b><BR><BR>&nbsp; <%=strRemitAddress %> </td> 
									
									<td class="RightTableCaption" align="center" valign="top"><br>Total</td>
									<td class="RightTableCaption" valign="top" align="right"><br><%=strCurrency %>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strTotal))%>&nbsp;</td>
								</tr>
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
								<tr>
									<td colspan="6" class="RightText"><font size="-3">
	Terms are <%=strPayNm%>. <br> Interest will be charged at an annual interest rate on the above balance if payment in full is not received by due date. 
	<%if(!strPartyFlag.equals("Y")) {%><br> Invoicee shall reimburse Globus Medical for all reasonable costs (including
attorneys' fees) relating to collection of past due amounts.<% } %><%if(strInvoiceNote.equals("Y")) {%><br> This invoice may be subject to discount.<% } %></font>
									</td>
								</tr>
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
							</table>
						</td>
					</tr>
				</table>

			</td>
		</tr>
    </table>
	<table border="0" width="700" cellspacing="0" cellpadding="0" >
<%	
	 /* Below code to display watermark
	* 50201 maps to OBO Inventory Type
	*/  
if(strInvType.equals("50201"))
{
%>

<div id="waterMark" style="position:absolute; z-Index: 0; top: 340; left: 340">
	<img src="<%=strImagePath%>/obo.gif"  border=0>
</div>

<% }

if (strInvoiceMode.equals("VoidInvoice"))
{
%>
<div id="waterMark" style="position:absolute; z-Index: 0; top: 340; left: 340">
	<img src="<%=strImagePath%>/voidinvoice.gif"  border=0>
</div>

<% } %>
		<tr>
			<td align="center" height="30" id="button">
				<gmjsp:button value="&nbsp;Print&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				<gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" />
			</td>
		<tr>
	</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
