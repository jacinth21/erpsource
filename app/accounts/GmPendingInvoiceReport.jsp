 <%@page import="java.util.Date"%> 
 <%
/**********************************************************************************
 * File		 		: GmPendingInvoiceReport.jsp
 * Desc		 		: This screen is used for the Account Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>


<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="fmtPendingInvoiceReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmPendingInvoiceReport.jsp -->
<fmtPendingInvoiceReport:setLocale value="<%=strLocale%>"/>
<fmtPendingInvoiceReport:setBundle basename="properties.labels.accounts.GmPendingInvoiceReport"/>
<%


	String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	int intSize = 0;
	HashMap hmLoop = new HashMap();
	HashMap hmCurrency = new HashMap();
	//String strApplDateFmt = "";
	 String strTimeZone = strGCompTimeZone;
    String strApplDateFmt =strGCompDateFmt;
    
	String strCodeID 	= "";
	String strSelected  = "";
	
	String strAccId 	= "";
	String strAccName 	= "";
	String strInvId 	= "";
	String strInvDate 	= "";
	String strPO 		= "";
	String strPrice 	= "";
	
	
	String strShade 	= "";
	String strAmtPaid 	= "";
	String strInvCallFlag = "";
	
	
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	
	String strApplCurrFmt = GmCommonClass.parseNull((String)hmCurrency.get("CMPCURRFMT"));
	String strcurrSymbol = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_SYMB"));
	String strSuccessMsg = GmCommonClass.parseNull((String)request.getAttribute("MailSent"));
	String strParentFl = GmCommonClass.parseNull((String)request.getAttribute("PARFLAG"));
	String strCategory = GmCommonClass.parseNull((String)request.getAttribute("strCategory"));
	strApplDateFmt = strGCompDateFmt;
	//String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));

	java.util.Date dtFrom = null;
	java.util.Date dtTo	= null;
	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(strApplDateFmt);
	String strHeader = "";
	String strWikiTitle = GmCommonClass.getWikiTitle("INVOICE_REPORT");
	
	String strAction = (String)request.getAttribute("hAction");
	
	strAction = (strAction == null)?"":strAction;
	String strCustPO = "";
	String strInvoiceID = "";
	String strOrderID = "";
	String strDivisionId = "";//PMT-41835
			
	
	if (strAction.equals("4"))
	{
		strHeader = "Pending";
	}else if (strAction.equals("5"))
	{
		strHeader = "Closed";	
	}
	HashMap hcboVal ;
	ArrayList alAccount2 = new ArrayList();
	ArrayList alReturn = new ArrayList();
	ArrayList alDateType = new ArrayList();
	ArrayList alCompDiv = new ArrayList();//PMT-41835
	int intLength=0;
	if(strAction.equals("GO")){ 	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	alReturn = (ArrayList)hmReturn.get("REPORT");
	intLength = alReturn.size();
	}
	alDateType = (ArrayList)request.getAttribute("DATETYPE");
	//Get Division arraylist values from request - PMT-41835 
	alCompDiv = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCOMPDIVLIST"));
	// Filter Value 
	
	HashMap hmParam = (HashMap)request.getAttribute("hmParam");
	if(hmParam != null){
		dtFrom	= (java.util.Date) hmParam.get("FROMDT");//GmCommonClass.parseNull((String)request.getParameter("Txt_FromDate"));
		dtTo = (java.util.Date) hmParam.get("TODT");//GmCommonClass.parseNull((String)request.getParameter("Txt_ToDate"));
		strCustPO = GmCommonClass.parseNull((String)hmParam.get("PO")) ;
		strInvoiceID = GmCommonClass.parseNull((String)hmParam.get("INVOICEID"));
		strOrderID = GmCommonClass.parseNull((String)hmParam.get("ORDERID"));
		//Get selected division id from hmparam hashmap-PMT-41835
		strDivisionId = GmCommonClass.parseNull((String)hmParam.get("DIVISIONID"));
	}
	
	strAccId	= GmCommonClass.parseZero((String)request.getParameter("Cbo_AccId"));
	
	
	StringBuffer sbScript = new StringBuffer();
	StringBuffer sbHeader = new StringBuffer();
	StringBuffer sbDetails = new StringBuffer();
	String strDisabled = "";
	Boolean blEmailFlag = false;
	
	String strInvSource = GmCommonClass.parseZero((String)request.getAttribute("INVSOURCE"));
	String strIdAccount = GmCommonClass.parseNull((String)request.getAttribute("IDACCOUNT"));
	String strStatusType = GmCommonClass.parseNull((String)request.getAttribute("Cbo_Type"));
	String strDateType = GmCommonClass.parseNull((String)request.getAttribute("hDateType"));
	
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.labels.accounts.GmPendingInvoiceReport", strSessCompanyLocale);
	String strImgAltXML = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("IMG_ALT_OPEN_XML"));
	String strImgAltHTML = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("IMG_ALT_OPEN_HTML"));
	String strImgAltRTF = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("IMG_ALT_OPEN_RTF"));
	String strImgAltPDF = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("IMG_ALT_OPEN_PDF"));
	String strImgAltCSV = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("IMG_ALT_OPEN_CSV"));
	String strImgEmailOpt = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("IMG_ALT_EMAIL_OPT"));
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: <%=strHeader%> Invoices Report </TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusMin.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmPendingInvoiceReport.js"></script>
<script>

var actionVal = '<%=strAction%>';
var invSourceVal = '<%=strInvSource%>';
var accVal = '<%=strIdAccount%>';
var fromDtVal = '<%=(dtFrom==null)?"":sdf.format(dtFrom)%>';
var toDtVal = '<%=(dtTo==null)?"":sdf.format(dtTo)%>';
var statusTypeVal = '<%=strStatusType%>';
var custPOVal = '<%=strCustPO%>';
var invoiceVal = '<%=strInvoiceID%>';
var orderVal = '<%=strOrderID%>';
var format = '<%=strApplDateFmt%>';
var parAccfl = '<%=strParentFl%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad1();">
<FORM name="frmAccount" method="POST" action="/GmInvoiceServlet">

<input type="hidden" name="hPgToLoad" value="">
<input type="hidden" name="hSubMenu" value="Trans">
<input type="hidden" name="hMode" value="">
<input type="hidden" name="hFrom" value="Dashboard">
<input type="hidden" name="hPO" value="">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hInv" value="">
<input type="hidden" name="hSource" value="<%=strInvSource%>">
<input type="hidden" name="hAccId" value="<%=strIdAccount%>">
<input type="hidden" name="hCount" value="<%=intLength%>">


<table class="DtTable900" cellpadding="0" cellspacing="0" border="0" >

    <tr>
      <td height="25" class="RightDashBoardHeader" width="90%"><fmtPendingInvoiceReport:message key="LBL_INVOICE_REPORT"/></td> 
       <td height="25" class="RightDashBoardHeader"></td> 
        <td height="25" class="RightDashBoardHeader"></td> <td height="25" class="RightDashBoardHeader"></td> 
        <fmtPendingInvoiceReport:message key="LBL_HELP" var="varHelp"/>
       <td height="25" class="RightDashBoardHeader" align="Right"><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
    </tr>	 
	<tr>
	
<td class="RightTextBlue" colspan="2"> <BR> <B>&nbsp;	<fmtPendingInvoiceReport:message key="LBL_CHOOSE_ACCOUNT"/> </B> <br><br>
	</td>
		<td  height="1" colspan="2"></td>		</tr>	 <tr><td colspan="4" class=line></td><td colspan="2" class=line></td></tr>

					<tr>
		<td>
		<jsp:include page="/accounts/GmIncludeAccountType.jsp">
				<jsp:param name="DISPACCFL" value="Y"/>
				<jsp:param name="REPACCTLABELFL" value="Y"/>  
		</jsp:include>	
		</td>
		<fmtPendingInvoiceReport:message key="BTN_LOAD" var="varLoad"/>
		<td  height="25" align="Right"><gmjsp:button gmClass="button" name="Btn_Go" value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" accesskey="L" tabindex="7" onClick="fnGo();" buttonType="load" buttonTag="True" />
		</td>	
		</tr>	
		<tr><td colspan="4" class=line></td><td colspan="2" class=line></td></tr>
				<tr> <td colspan="6" >
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				
					<tr colspan="2" class="Shade" height="24">
					
					<td class="RightTablecaption" align="Right">
						<fmtPendingInvoiceReport:message key="LBL_STATUS_TYPE"/>: 
					</td><td>&nbsp;<select name="Cbo_Type" class="RightText" tabindex="1">
							<option value="0">[All] <option value="1"><fmtPendingInvoiceReport:message key="LBL_PENDING"/> <option value="2"><fmtPendingInvoiceReport:message key="LBL_CLOSED"/> 
					</select> </td>
					<td colspan="1" class="RightTablecaption" align="Right"><fmtPendingInvoiceReport:message key="LBL_SHOW_PARENT_ACCOUNT"/>:</td>
					<td>&nbsp;<input type="checkbox" size="30" checked="<%=strParentFl%>" name="Chk_ParentFl" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" >					
						</td>
					<!-- <td colspan="2">&nbsp;</td> -->
					<!-- PMT-41835 -->
					<td class="RightTableCaption" align="right"><fmtPendingInvoiceReport:message key="LBL_DIVISION"/>:</td>
					<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Division"  seletedValue="<%=strDivisionId%>" value="<%=alCompDiv%>" 
					codeId = "DIVISION_ID" codeName = "DIVISION_NAME" defaultValue= "[Choose One]" /></td> <!-- PMT-41835 -->
				</tr>
				<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
				<tr Height="24" >
					<td class="RightTablecaption" align="Right"><fmtPendingInvoiceReport:message key="LBL_CUSTOMER_PO"/>:</td>
					<td class="RightTablecaption" >&nbsp;<input type="text" size="20" 
						value="<%=GmCommonClass.parseNull((String)request.getParameter("Txt_PO")) %>" name="Txt_PO" class="InputArea"   
					tabindex="2"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"></td>
					<td class="RightTablecaption" align="Right"><fmtPendingInvoiceReport:message key="LBL_INVOICE_ID"/>:</td>
					<td>&nbsp;<input type="text" size="20" 
						value="<%=GmCommonClass.parseNull((String)request.getParameter("Txt_InvoiceID")) %>" name="Txt_InvoiceID" class="InputArea"   
					tabindex="3" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"></td> 
					<td class="RightTablecaption" align="Right"><fmtPendingInvoiceReport:message key="LBL_ORDER_ID"/>:</td>
					<td>&nbsp;<input type="text" size="20" 
						value="<%=GmCommonClass.parseNull((String)request.getParameter("Txt_OrderID")) %>" name="Txt_OrderID" class="InputArea"   
					tabindex="4" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"></td> 
				</tr>
				<tr><td bgcolor="#CCCCCC" height="1" colspan="6"></td></tr>
				<!-- Custom tag lib code modified for JBOSS migration changes -->
				<tr Height="25" class="Shade">
				<td class="RightTableCaption" align="right"><fmtPendingInvoiceReport:message key="LBL_DATE_TYPE"/>:</td>
			<td align="left">&nbsp;<gmjsp:dropdown controlName="Cbo_DateType"  seletedValue="<%=strDateType%>" 	
					 value="<%=alDateType%>" codeId = "CODEID" codeName = "CODENM" /> 
			</td>
				
				<td class="RightTablecaption" align="Right"><fmtPendingInvoiceReport:message key="LBL_FROM_DATE"/>:</td>
					 
					<td class="RightTablecaption" >&nbsp;<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=(dtFrom==null)?null:new java.sql.Date(dtFrom.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="5"/></td>
					<%--
					<input type="text" size="10" value="<%=strFromDt%>" 
					name="Txt_FromDate" class="InputArea" tabindex=5>&nbsp;
					<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmAccount.Txt_FromDate');" title="Click to open Calendar"  
					src="/images/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />
					--%>
					<td class="RightTablecaption" align="Right"><fmtPendingInvoiceReport:message key="LBL_TO_DATE"/>:</td>
					<td>&nbsp;<gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=(dtTo==null)?null:new java.sql.Date(dtTo.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="6"/></td>
					<%--<input type="text" size="10" value="<%=strToDt%>" name="Txt_ToDate" class="InputArea" 
					tabindex=6>&nbsp;<img id="Img_Date" style="cursor:hand" 
					onclick="javascript:show_calendar('frmAccount.Txt_ToDate');" title="Click to open Calendar"  
					src="/images/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />	&nbsp;&nbsp;&nbsp;&nbsp;
					--%>
				</tr>	
				</table><%if(strAction.equals("GO")){ %>
<table width="800" cellpadding="0" cellspacing="0" border="0"></td><td></td></tr>
			<%if(!strSuccessMsg.equals("")){ %> 
				<tr><td colspan="7" class="Line"></td></tr>
				<tr><td colspan="7" class="RightTableCaption" align="center" height="25"><font color="green"><%=strSuccessMsg %> </font></td></tr>
			<%} %>		
			<%if(strCategory.equals("Y")){ %>
				<tr><td colspan="9" class="Line"></td></tr>	
				<%}else{ %>	
					<tr><td colspan="8" class="Line"></td></tr>	
					<%} %>		
			<tr height="25">
						<td colspan = "1" align = "center" class="RightTextRed"><fmtPendingInvoiceReport:message key="LBL_CREDIT_RED"/></td>
						<td colspan = "2" align = "center" class="RightTextBlue"><fmtPendingInvoiceReport:message key="LBL_DEBIT_BLUE"/></td>
						<td colspan = "2" align = "center" class="RightTextGreen"><fmtPendingInvoiceReport:message key="LBL_REGULAR_GREEN"/></td>
						<td colspan = "2" align = "center" class="RightTextPurple"><fmtPendingInvoiceReport:message key="LBL_OBO_PURPLE"/></td>
					</tr>
					<%if(strCategory.equals("Y")){ %>
				<tr><td colspan="9" class="Line"></td></tr>	
				<%}else{ %>	
					<tr><td colspan="8" class="Line"></td></tr>	
					<%} %>				
				<tr class="ShadeRightTableCaption">
				<%if(strInvSource.equals("26240213")){ %>
				<td HEIGHT="24" width="300"><fmtPendingInvoiceReport:message key="LBL_DEALER_NAME"/></td>
					<td HEIGHT="24" width="120"><fmtPendingInvoiceReport:message key="LBL_DEALER_ID"/></td>
					<td width="300" align="center"><fmtPendingInvoiceReport:message key="LBL_INVOICE_ID"/></td>
					<%}else{ %>
				
					<%if(!strParentFl.equals("false")){ %>
					<td HEIGHT="24" width="300"><fmtPendingInvoiceReport:message key="LBL_PARENT_ACCOUNT_NAME"/></td>
					<td HEIGHT="24" width="120" align="center"><fmtPendingInvoiceReport:message key="LBL_ACCOUNT_ID"/></td>
					<%if(strCategory.equals("Y")){ %>
							<td width="80" align="center"><fmtPendingInvoiceReport:message key="LBL_CATEGORY"/></td>
					<%}%>
					<td width="600" align="center"><fmtPendingInvoiceReport:message key="LBL_INVOICE_ID"/></td>		
					<% }else{%>
					<td HEIGHT="24" width="300"><fmtPendingInvoiceReport:message key="LBL_REP_ACCOUNT_NAME"/></td>
					<%if(strCategory.equals("Y")){ %>
							<td width="80" align="center"><fmtPendingInvoiceReport:message key="LBL_CATEGORY"/></td>
							<td width="370" align="center"><fmtPendingInvoiceReport:message key="LBL_INVOICE_ID"/></td>
							
					<%}else{%>
					<td width="200" align="center"><fmtPendingInvoiceReport:message key="LBL_INVOICE_ID"/></td>
					<%}}%>
						<%}%>	
					<td width="160" align="center"><fmtPendingInvoiceReport:message key="LBL_INVOICE_DATE"/></td>
					<td width="160" align="center"><fmtPendingInvoiceReport:message key="LBL_CUSTOMER_PO"/></td>
					<td width="60"  align="center"><fmtPendingInvoiceReport:message key="LBL_CURRENCY"/></td>
					<td width="100" align="center"><fmtPendingInvoiceReport:message key="LBL_INOVICE_AMOUNT"/></td>
					<td width="100" align="center" colspan = "2"><fmtPendingInvoiceReport:message key="LBL_AMOUNT_PENDING"/></td></tr>
								
<%				//**********************************************
				// Code to fecth and display the invoice list 
		
				
				if (intLength > 0)
				{
					HashMap hmTempLoop = new HashMap();

					String strNextId = "";
					int intCount = 0;
					String strLine = "";
					String strTotal = "";
					String strAcctTotal = "";
					String strCurrency = "";
					String strEmailReq = "";
					String strEmailVer = "";
					String strUploadFileID = "";
					String strPDFFileID = "";
					String strCSVFileID = "";
					String strXMLFileID = "";
					Date strInvoicedYear = null;
					String strDisplayImage = "";
					String strInvType = "";
					String strRTFFileID = "";
					String strParentAccountId = "";
					String strParentAcctName = "";
					String strICSInclude = "";
					String strCategoryVal = "";
					boolean blFlag = false;
					boolean blFlag1 = false;
					double dbPendAmt = 0.0;
					double dbAcctTotal = 0.0;
					double dbTotal = 0.0;

					for (int i = 0;i < intLength ;i++ )
					{
						hmLoop = (HashMap)alReturn.get(i);
						//log.debug("hmloop================="+hmLoop);
						//To fecth the next record value 
						// to frame the tree  
						if (i<intLength-1)
						{
							hmTempLoop = (HashMap)alReturn.get(i+1);
							// if Source Dealer or Parent account flag unchecked then, show the Account information otherwise show the parent order information.
							if(strInvSource.equals("26240213") || strParentFl.equals("false")){
							  strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("ACCID"));
							}
							else{
							    strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("PARENTACCTID"));
							}
						}

						strAccId = GmCommonClass.parseNull((String)hmLoop.get("ACCID"));
						strParentAccountId = GmCommonClass.parseNull((String)hmLoop.get("PARENTACCTID"));
						strAccName = GmCommonClass.parseNull((String)hmLoop.get("NAME"));
						strParentAcctName = GmCommonClass.parseNull((String)hmLoop.get("PARENTACCTNM"));
						if(!strInvSource.equals("26240213")){
						strAccName = (!strParentFl.equals("false"))?strParentAcctName:strAccName;
						}
						// if Source - Dealer or Parent account unchecked then, to set the parenet account id to - Rep Account id
						if((strParentFl.equals("false") || strInvSource.equals("26240213"))){
						  strParentAccountId = strAccId;
						}
						
						strInvId = GmCommonClass.parseNull((String)hmLoop.get("INVID"));
						//strInvDate = GmCommonClass.getStringFromDate((Date)hmLoop.get("strSessApplDateFmt"),strApplDateFmt);
						strInvDate = GmCommonClass.getStringFromDate((java.sql.Date)hmLoop.get("INVDT"),strGCompDateFmt);
						strPrice = GmCommonClass.parseZero((String)hmLoop.get("COST"));
						strAmtPaid = GmCommonClass.parseZero((String)hmLoop.get("AMTPAID"));
						strPO = GmCommonClass.parseNull((String)hmLoop.get("PO"));
						strCurrency = GmCommonClass.parseNull((String)hmLoop.get("CURRENCY"));
						strInvCallFlag = GmCommonClass.parseNull((String)hmLoop.get("CALL_FLAG"));
						strEmailReq = GmCommonClass.parseNull((String)hmLoop.get("EMAILREQ"));
						strEmailVer = GmCommonClass.parseNull((String)hmLoop.get("EVERSION"));
						strUploadFileID = GmCommonClass.parseNull((String)hmLoop.get("FILEID"));
						strPDFFileID = GmCommonClass.parseNull((String)hmLoop.get("PDFFILEID"));
						strCSVFileID = GmCommonClass.parseNull((String)hmLoop.get("CSVFILEID"));
						strXMLFileID = GmCommonClass.parseNull((String)hmLoop.get("XMLFILEID"));
						strCategoryVal = GmCommonClass.parseNull((String)hmLoop.get("CUSTCATEGORY"));
						strInvoicedYear = (java.util.Date)hmLoop.get("INVYEAR");
						strInvType =  GmCommonClass.parseNull((String)hmLoop.get("INVTYPE"));
						strDisplayImage = "";
						strRTFFileID = GmCommonClass.parseNull((String)hmLoop.get("RTFFILEID"));
						strICSInclude = GmCommonClass.parseNull((String)hmLoop.get("ICSINCLUDE"));

						dbPendAmt = Double.parseDouble(strPrice) - Double.parseDouble(strAmtPaid);
						dbAcctTotal = dbAcctTotal + dbPendAmt;
						dbTotal = dbTotal + dbPendAmt;

						  if (strParentAccountId.equals(strNextId))
							{
								intCount++;
							}
							else
							{
								if (intCount > 0)
								{
									strLine = "";
								}
								else
								{
									blFlag = true;
								}
								intCount = 0;
								//
							}
						  
						
						strAcctTotal = ""+dbAcctTotal;

						if (intCount > 1)
						{
							strLine = "";
						}
						

						if (intCount == 1 || blFlag)
						{
							blFlag = false;
						}
						
						strShade = (i%2 != 0)?"class=SB":""; //For alternate Shading of rows
						sbDetails.append("<tr " + strShade + ">" );
						sbDetails.append("<td width=300></td>");
						if(!strParentFl.equals("false")){							
							sbDetails.append("<td width=120  align=center>&nbsp;" + strAccId + "</td>");
						 }
						if(strCategory.equals("Y")){							
							sbDetails.append("<td width=80  align=center>&nbsp;" + strCategoryVal + "</td>");
							 sbDetails.append("<td width=370  class=ART>");
						 }else{
							 sbDetails.append("<td width=300  class=ART>");
						 }
						 
						
						if(!strInvSource.equals("50253") || strICSInclude.equals("Y")) ////ICS
						{
						  
						  if(!strXMLFileID.equals("")){
						    strDisplayImage = "<img id='imgXML' style='cursor:hand;vertical-align:middle;' src='"+strImagePath+"/xml_icon.jpg' onclick=fnExportFile('" +strXMLFileID+"');  height='18' width='18' title='"+strImgAltXML+"' >&nbsp;"; 
						  }
						  
							if(strCountryCode.equals("en")){// Needed only for US
								strDisplayImage += "<img id='imgHTML' style='cursor:hand;vertical-align:middle;' src='"+strImagePath+"/html_icon.png' onclick=fnPrintInvoice('"+strInvId+"');  height='16' width='16' title='"+strImgAltHTML+"' >&nbsp;";
							}
							strDisplayImage += "<img id='imgRTF' style='cursor:hand;vertical-align:middle;' src='"+strImagePath+"/word_icon.jpg' onclick=fnExportRTFFile('"+strInvId+"','" +strRTFFileID+"');  height='16' width='16' title='"+strImgAltRTF+"' >&nbsp;";
							if(!strPDFFileID.equals("")){ // check the PDF file is stored 
								strDisplayImage += "<img id='imgPDF' style='cursor:hand;vertical-align:middle;' src='"+strImagePath+"/pdf_icon.gif' onclick=fnExportFile('"+strPDFFileID+"');  height='16' width='16' title='"+strImgAltPDF+"' >&nbsp;";
							}
							if(!strCSVFileID.equals("") && strCountryCode.equals("en")){ // check the CSV file is stored
								strDisplayImage += "<img id='imgCSV' style='cursor:hand;vertical-align:middle;' src='"+strImagePath+"/csv_icon.gif' onclick=fnExportFile('"+strCSVFileID+"');  height='16' width='16' title='"+strImgAltCSV+"'>&nbsp;";
							}
						}
						// check the account email flag is true and invoice is stored the location then only show the check box (send mail)
						if(strEmailReq.equals("Y") && !strPDFFileID.equals("") && !strInvType.equals("OBO")){
							blEmailFlag = true; // based on the flag to diable the "Send Email" button.
							sbDetails.append(strDisplayImage+"<input class=RightText type='checkbox' id='Chk_Mail"+i+"' value='"+ strInvId +"'>&nbsp;");
							sbDetails.append("<input class=RightText type='hidden' id='hEmailVersion"+strInvId+"' value='"+ strEmailVer +"'>");
						}else{
							sbDetails.append(strDisplayImage);
						}						
						sbDetails.append("<a class='"+strInvType+"' ");
						sbDetails.append("href=\"javascript:fnCallInv('" + strInvId + "')\">" + strInvId + "</a>&nbsp;");

						// Code for Invoice Call Log Starts 
						if (!strInvId.equals(""))
						{
	 						sbDetails.append("<img  style=cursor:hand ");
							if (strInvCallFlag.equals("N"))
	 						{ 
		 						sbDetails.append("src=" + strImagePath + "/phone_icon.jpg " );
		 					} else {
		 						
		 						sbDetails.append("src=" + strImagePath + "/phone-icon_ans.gif " );
			 				}
			 				sbDetails.append("onClick=\"javascript:fnOpenInvLog('" + strInvId + "')\"/>"); 						
			 			}
			 			//Code for Invoice Call Log Ends
 						sbDetails.append("</td>");
 						if(strParentFl.equals("false")){									
							sbDetails.append("<td width=160  class=ACT>&nbsp;" + strInvDate + "</td>");
							sbDetails.append("<td width=160  class=ART>&nbsp;" + strPO + "</td>");
							sbDetails.append("<td width=150   class=ART>&nbsp;" + strCurrency + "</td>");
							sbDetails.append("<td width=110  class=ART>" + GmCommonClass.getStringWithCommas(strPrice,strApplCurrFmt) + "&nbsp;</td>");
							sbDetails.append("<td width=110  class=ART>" + GmCommonClass.getStringWithCommas(""+dbPendAmt,strApplCurrFmt)+ "&nbsp;</td>");
							sbDetails.append("</tr>");
 						} else {
 							sbDetails.append("<td width=160  class=ALT>&nbsp;" + strInvDate + "</td>");
 							sbDetails.append("<td width=160  class=ALT>&nbsp;" + strPO + "</td>");
 							sbDetails.append("<td width=60   class=RT>&nbsp;" + strCurrency + "</td>");
 							sbDetails.append("<td width=90  class=ART>" + GmCommonClass.getStringWithCommas(strPrice,strApplCurrFmt) + "&nbsp;</td>");
 							sbDetails.append("<td width=90  class=ART>" + GmCommonClass.getStringWithCommas(""+dbPendAmt,strApplCurrFmt)+ "&nbsp;</td>");
 							sbDetails.append("</tr>");
 						}
						
						//sbDetails.append("<tr><td colspan=5 class=LNL ></td></tr>");
						
					// Print after detail table printed
					if ( intCount == 0 || i == intLength-1)
					{
						if(strCategory.equals("Y")){
							sbHeader.append("<TR><TD colspan=9  class=LN></TD></TR>");
							sbHeader.append("<tr id='tr" + strAccId + "'>");
							sbHeader.append("<td colspan=8 height=20 class=RT>&nbsp;");
						}else{
						sbHeader.append("<TR><TD colspan=8  class=LN></TD></TR>");
						sbHeader.append("<tr id='tr" + strAccId + "'>");
						sbHeader.append("<td colspan=7 height=20 class=RT>&nbsp;");
						}
						sbHeader.append("<A class=RT  href=\"javascript:Toggle('" + strAccId + "')\">" ) ;
						sbHeader.append(strAccName + "</a>");
						
						if(strEmailReq.equals("Y")){ // check the PDF file is stored
							sbHeader.append("&nbsp;<img id='imgPDFEmail' style='cursor:hand;vertical-align:middle;' src='"+strImagePath+"/Email.jpg' height='13' width='10' title='"+strImgEmailOpt+"'></td>");
						}else{
							sbHeader.append("</td>");
						}
						sbHeader.append("<td class=ART id='td" + strAccId + "' > " ) ;
						sbHeader.append(GmCommonClass.getStringWithCommas(strAcctTotal,strApplCurrFmt)+ "&nbsp;&nbsp;" + "</td>");
						sbHeader.append("</tr>");
						
						sbHeader.append("<tr>");
						if(strCategory.equals("Y")){
							sbHeader.append("<td colspan=9><div style='display:none' id='div" + strAccId + "'> ");
						}else{
							sbHeader.append("<td colspan=8><div style='display:none' id='div" + strAccId + "'> ");
						}				   	
						sbHeader.append("<table class=TB id='tab" + strAccId + "'>" ) ;
						out.print(sbHeader.toString());
						sbHeader.setLength(0);
						
						/* Code to fetch the detail(child) row information */
						out.print(sbDetails.toString());
						sbDetails.setLength(0);
						
						// Closing the main table 
						out.print("</table></div>");
						out.print("</td></tr>");

						dbAcctTotal = 0.0;					
					}
				strTotal = ""+dbTotal;
				
				}// end of FOR
				if(!blEmailFlag){
					strDisabled = "true";
				}
				
				//*************************************
				// End of main loop to display value
%>
				<%if(strCategory.equals("Y")){ %>
				<tr><td colspan="9" class="Line"></td></tr>	
				<%}else{ %>	
					<tr><td colspan="8" class="Line"></td></tr>	
					<%} %>	
				<tr>
				<%if(strCategory.equals("Y")){ %>
					<td align="right"  class="RightTableCaption" height="20" colspan="8"><fmtPendingInvoiceReport:message key="LBL_GRAND_TOTAL"/>&nbsp;</td>
					<%}else{ %>
					<td align="right"  class="RightTableCaption" height="20" colspan="7"><fmtPendingInvoiceReport:message key="LBL_GRAND_TOTAL"/>&nbsp;</td>
					<%} %>
					<gmjsp:currency type="CurrTextSign"  textValue="<%=strTotal%>" currSymbol="<%=strcurrSymbol %>"/>
					<%-- <td class="RightTableCaption" align="right"><%=GmCommonClass.getStringWithCommas(strTotal)%>&nbsp;</td>--%>
				</tr>
				<tr>
					<fmtPendingInvoiceReport:message key="LBL_DOWNLOADTOEXCEL" var="varDownloadToExcel"/>
					<fmtPendingInvoiceReport:message key="LBL_SEND_EMAIL" var="varSendEmail"/>
					<td colspan="8" align="center" height="30"><gmjsp:button value="${varDownloadToExcel}" gmClass="button" buttonType="Load" onClick="fnPrintVer();" tabindex="7"/>
					&nbsp;&nbsp;&nbsp;<gmjsp:button value="${varSendEmail}" gmClass="button" buttonType="Save" name="btn_sendMail" onClick="fnSendEmail();" tabindex="8" disabled="<%=strDisabled %>"/>
					</td>
				</tr>
<%			} // End of IF
			else{
%>
					<tr><td colspan="7" class="Line"></td></tr>					
					<tr><td colspan="7" class="RightTextRed" height="50" align=center><fmtPendingInvoiceReport:message key="LBL_NO_INVOICES_RAISED_SO_FAR"/></td></tr>
<%					
			}
} %>
			
			</td>
			<td ></td>
		</tr>
		
    </table>
    </td></tr>
    </table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
