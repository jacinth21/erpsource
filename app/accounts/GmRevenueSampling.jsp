<%
/**********************************************************************************
 * File		 		: GmRevenueSampling.jsp
 * Desc		 		: Revenue Sampling Report Screen
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@page import="java.util.Date"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ taglib prefix="fmtRevenueSampling" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!--GmRevenueSampling.jsp-->
<fmtRevenueSampling:setLocale value="<%=strLocale%>"/>
<fmtRevenueSampling:setBundle basename="properties.labels.accounts.GmRevenueSampling"/>
<bean:define id="gridData" name="frmRevenueSampling" property="strXmlString" type="java.lang.String"></bean:define>
<bean:define id="hsize" name="frmRevenueSampling" property="hsize" type="java.lang.Integer"></bean:define>
<bean:define id="chk_ParentFl" name="frmRevenueSampling" property="chk_ParentFl" type="java.lang.String"></bean:define>
<bean:define id="strCompCurrency" name="frmRevenueSampling" property="strCompCurrency" type="java.lang.String"></bean:define>
<bean:define id="strARRptByDealerFlag" name="frmRevenueSampling" property="strARRptByDealerFlag" type="java.lang.String"></bean:define>
<bean:define id="strAccountID" name="frmRevenueSampling" property="accountID" type="java.lang.String"></bean:define>
<bean:define id="strAccountName" name="frmRevenueSampling" property="searchaccountID" type="java.lang.String"></bean:define>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
	HashMap hmCurrency = new HashMap();
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strDeptId = GmCommonClass.parseNull((String)session.getAttribute("strSessDeptSeq"));
	String strCurrSign = GmCommonClass.getCodeNameFromCodeId(strCompCurrency);
	String strApplnDateFmt = strGCompDateFmt;
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	String currentdate =  GmCalenderOperations.getCurrentDate(strGCompDateFmt);

%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Revenue Sampling </TITLE>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css"> 
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>

<script language="JavaScript"  src="<%=strAccountJsPath%>/GmRevenueSampling.js"></script>

<script type="text/javascript">

var gridObjData='<%=gridData%>';
var format = '<%=strApplnDateFmt%>';
var compCurrSign = '<%=strCompCurrency%>';
var arRptByDealerFlag = '<%=strARRptByDealerFlag%>';
</script> 
 
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<html:form  action="/gmRevenueSampling.do" >
<input type="hidden" name="hOrdId" value=""> 
<input type="hidden" name="hAction" value=""> 
<input type="hidden" name="hDeptId" value="<%=strDeptId%>">
<input type="hidden" name="currentdate" value="<%=currentdate %>"/>
<html:hidden property="strOpt" value="" />
<html:hidden property="hsize"  />
<html:hidden property="hinputStr"  />
<html:hidden property="haction"  />

	<table border="0" class="DtTable1650" cellspacing="0" cellpadding="0">
		
		<tr><td>
		<table border="0" width="100%"  cellspacing="0" cellpadding="0" >
			<tr>
				<td height="25"  class="RightDashBoardHeader"><fmtRevenueSampling:message key="LBL_REVENUE_SAMPLING_REPORT"/></td>
				<fmtRevenueSampling:message key="LBL_TUTORIAL_WINDOW" var="varTutorialWindow"/>
				<td  align="right" class="RightDashBoardHeader"><a href="javascript:fnViewFile('1','V');"><img src="<%=strImagePath%>/wmv_icon.gif" border="0" height="16" width="16" alt="${varTutorialWindow}"></a>&nbsp;&nbsp;</td>
			</tr>
		</table>
		</td>
			
		</tr>
			<tr>
				<td colspan="5" height="20">
						<jsp:include page="/sales/GmSalesFilters.jsp">
						<jsp:param name="HIDE" value="SYSTEM" />
						<jsp:param name="FRMNAME" value="frmRevenueSampling" />
						<jsp:param name="HACTION" value="Reload" />
						<jsp:param name="URL" value="/gmRevenueSampling.do?strOpt=Reload" />
					</jsp:include></td>
			</tr>
			<tr>
			<td>
				<table border="0" width="100%"  cellspacing="0" cellpadding="0">
					<tr class="evenshade">
						<fmtRevenueSampling:message key="LBL_REP_ACCOUNT" var="varRepAccount"/>
						<td  height="30" class="RightTableCaption" align="right" width="13%"><gmjsp:label type="RegularText"  SFLblControlName="${varRepAccount}:" td="false"/></td>
						<td><table cellpadding="0" cellspacing="0" border="0"><tr><td width="46%" style="padding-left: 5px;"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
										<jsp:param name="CONTROL_NAME" value="accountID" />
										<jsp:param name="METHOD_LOAD" value="loadAccountList" />
										<jsp:param name="WIDTH" value="400" />
										<jsp:param name="CSS_CLASS" value="search" />
										<jsp:param name="TAB_INDEX" value="1"/>
										<jsp:param name="CONTROL_NM_VALUE" value="<%=strAccountName %>" /> 
										<jsp:param name="CONTROL_ID_VALUE" value="<%=strAccountID %>" />
										<jsp:param name="SHOW_DATA" value="100" />
										<jsp:param name="AUTO_RELOAD" value="fnDisplayAccID(this);" />					
									</jsp:include></td><td align="left" width="40px">&nbsp;<input type="text" size="8"  name="Txt_AccId" class="InputArea" 
			                                                       onFocus="changeBgColor(this,'#AACCE8');" onChange="fnDisplayAccNm(this)"></td></tr></table>	
						</td>
						<fmtRevenueSampling:message key="LBL_REP" var="varRep"/>
						<td class="RightTableCaption" align="right"><gmjsp:label type="RegularText"  SFLblControlName="${varRep}:" td="false"/></td>
						<td width="330" style="padding-left: 5px;"><gmjsp:dropdown controlName="repID" SFFormName="frmRevenueSampling" SFSeletedValue="repID" 
							SFValue="alRepList" codeId = "ID"  codeName = "NAME"   defaultValue= "[Choose One]" />
						</td>
						<td></td>
						
					</tr>
					<tr><td colspan="5" class="LLine" height="1"></td></tr>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr  class="oddshade">
						<td class="RightTableCaption">&nbsp;&nbsp;<fmtRevenueSampling:message key="LBL_DATE_TYPE" var="varDateType"/>
						<gmjsp:label type="RegularText"  SFLblControlName="&nbsp;${varDateType}:" td="false"/>
						<gmjsp:dropdown controlName="strOrdDateType" SFFormName="frmRevenueSampling" SFSeletedValue="strOrdDateType"
							SFValue="alOrdDateType" codeId = "CODEID"  codeName = "CODENM" />
						</td>
						<td class="RightTableCaption">&nbsp;
							<fmtRevenueSampling:message key="LBL_FROM" var="varFrom"/>
							<fmtRevenueSampling:message key="LBL_TO" var="varTo"/>
							<gmjsp:label type="RegularText"  SFLblControlName="${varFrom}:" td="false"/>&nbsp;<gmjsp:calendar SFFormName="frmRevenueSampling" 
							SFDtTextControlName="dtOrderFromDT" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />&nbsp;
							&nbsp;&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varTo}:" td="false"/>&nbsp;<gmjsp:calendar SFFormName="frmRevenueSampling" 
							SFDtTextControlName="dtOrderToDT" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />&nbsp;
							<fmtRevenueSampling:message key="LBL_SHOW_PARENT_ACCOUNT"/>:&nbsp;<html:checkbox property="chk_ParentFl"   title="Show Parent Account"></html:checkbox>
						</td>
						<fmtRevenueSampling:message key="LBL_REGION" var="varRegion"/>
						<td class="RightTableCaption" align="right"><gmjsp:label type="RegularText"  SFLblControlName="${varRegion}:" td="false"/>
						</td>
						<td>&nbsp;<gmjsp:dropdown controlName="regionID" SFFormName="frmRevenueSampling" SFSeletedValue="regionID"
							SFValue="alRegion" codeId = "CODEID"  codeName = "CODENM"      defaultValue= "[Choose One]" />
						</td>
						<fmtRevenueSampling:message key="BTN_LOAD" var="varLoad"/>
						<td width="80" align="center"><gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnGo();" tabindex="4" buttonType="Load" />&nbsp;</td>				
					</tr>
					<tr><td colspan="5" class="LLine" height="1"></td></tr>
					<tr class="evenshade">
						<fmtRevenueSampling:message key="LBL_STATUS_UPDATED_DATE" var="varStatusUpdate"/>
						<td align="right"  height="30" class="RightTableCaption"><gmjsp:label type="RegularText"  SFLblControlName="${varStatusUpdate}:" td="false"/>
                        </td>
						<td class="RightTableCaption">&nbsp;
							<fmtRevenueSampling:message key="LBL_FROM" var="varFrom"/>
							<fmtRevenueSampling:message key="LBL_TO" var="varTo"/>
							<gmjsp:label type="RegularText" SFLblControlName="${varFrom}:" td="false"/>&nbsp;<gmjsp:calendar SFFormName="frmRevenueSampling" 
							SFDtTextControlName="dtStatusFromDT" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />&nbsp;
							&nbsp;&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varTo}:" td="false"/>&nbsp;<gmjsp:calendar SFFormName="frmRevenueSampling" 
							SFDtTextControlName="dtStatusToDT" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />&nbsp;
                            <font  style="padding-left:5em;">&nbsp;</font><b><fmtRevenueSampling:message key="LBL_CURRENCY"/>:</b>&nbsp;
							<gmjsp:dropdown	controlName="strCompCurrency" SFFormName="frmRevenueSampling"
						    SFSeletedValue="strCompCurrency" SFValue="alCompCurrency" codeId="ID" codeName="NAMEALT" optionId="NAME" />	
						</td>
						<fmtRevenueSampling:message key="LBL_STATUS" var="varStatus"/>	
						<td class="RightTableCaption" align="right"><gmjsp:label type="RegularText"  SFLblControlName="${varStatus}:" td="false"/></td>
						<td>&nbsp;<gmjsp:dropdown controlName="status" SFFormName="frmRevenueSampling" SFSeletedValue="status"
							SFValue="alStatus" codeId = "CODEID"  codeName = "CODENM"      defaultValue= "[Choose One]" /> 
						</td>
						<td></td>						
					</tr>
				</table>
			</td>
		</tr>
		<tr><td class="LLine" height="1" colspan="5"></td></tr>
		<%if(gridData.indexOf("cell") != -1) {%>
		<tr>
			<td  colspan="5" width="100%">
				<div id="dataGridDiv"  height="500px" width="1650px"></div>
				<div id="pagingArea" height="500px" width="1650px"></div>
			</td>
		</tr>
		<%}else if(!gridData.equals("")){%>
			<tr><td colspan="5" align="center" height="30" class="RightText"><fmtRevenueSampling:message key="LBL_NO_DATA_AVAILABLE"/></td></tr>
		<%}else{%>
			<tr><td colspan="5" align="center" height="30" class="RightText"><fmtRevenueSampling:message key="LBL_NOTHING_FOUND_TO_DISPLAY"/></td></tr>
		<%}%>
		<%if(gridData.indexOf("cell") != -1) {%>
		<tr>
			<td colspan="5" align="center" width="1200px">
				<fmtRevenueSampling:message key="BTN_SEND_EMAIL" var="varSendEmail"/>
				<gmjsp:button value="&nbsp;${varSendEmail}&nbsp;" gmClass="button" onClick="fnSendMail();" buttonType="Save" />&nbsp;<BR><BR>
				   
				<div class='exportlinks'><fmtRevenueSampling:message key="LBL_EXPORT_OPTIONS"/> : 
					<img src='img/ico_file_excel.png' onclick="fnExport();"/>&nbsp;<a href="#" onclick="fnExport();"> <fmtRevenueSampling:message key="LBL_EXCEL"/></a>
				</div>
			</td>
		</tr>
		<%} %>	
</table>
 
<%@ include file="/common/GmFooter.inc" %>
</html:form>
</BODY>

</HTML>
