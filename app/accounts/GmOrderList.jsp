 <%
/**********************************************************************************
 * File		 		: GmInvoiceOrderList.jsp
 * Desc		 		: This screen is used to display Account RollForward Report
 * Version	 		: 1.0
 * author			: Richard
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtOrderList" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmOrderList.jsp -->
<fmtOrderList:setLocale value="<%=strLocale%>"/>
<fmtOrderList:setBundle basename="properties.labels.accounts.GmOrderList"/>
<fmtOrderList:message key="LBL_CHOOSE_ONE" var="varChooseOne"/>
<%
	String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
	GmServlet gm = new GmServlet();
	//gm.checkSession(response,session);
	String strApplDateFmt = strGCompDateFmt;
	String strRptFmt = "{0,date,"+strApplDateFmt+"}";
	HashMap hmCurrency = new HashMap();
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strApplCurrFmt = (String)hmCurrency.get("CMPCURRFMT");
	String strCurrSign = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_SYMB"));
	String strCurrPos = GmCommonClass.parseNull(GmCommonClass.getString("CURRPOS"));
    String strDivisionId 	= GmCommonClass. parseZero ((String)request.getAttribute("DIVISIONID"));
    String strCustCategoryId 	= GmCommonClass.parseZero((String)request.getAttribute("CATEGORYID"));
    String strCategoryRuleId 	= GmCommonClass.parseZero((String)request.getAttribute("CUSTCATEGORY"));
	String strCurrFmt = "";

	if(strCurrPos.equalsIgnoreCase("RIGHT")){
		strCurrFmt = "{0,number,"+strApplCurrFmt+"}";
	}
	else{
		strCurrFmt = "{0,number,"+strApplCurrFmt+"}";
	}
	
	String strLoadScreen 	= GmCommonClass.parseNull((String)request.getAttribute("hLoadScreen"));
	String strOrderType 	= GmCommonClass.parseNull((String)request.getAttribute("Cbo_OrderType"));
	String strAccId = GmCommonClass.parseNull((String)request.getAttribute("hAccId"));
	String strAccountName = GmCommonClass.parseNull((String)request.getAttribute("SEARCH_ACC_NAME"));
	String strAccountId = strAccId.equals("0")?"":strAccId;
	String strWikiTitle = GmCommonClass.getWikiTitle("ORDER_LIST");
	
	java.util.Date dtFrm 	= (java.util.Date)request.getAttribute("hFrom");
	java.util.Date dtTo 	= (java.util.Date)request.getAttribute("hTo");
	ArrayList alCollector = (ArrayList)request.getAttribute("ALCOLLECTOR");
	String strCollectorId = GmCommonClass.parseNull((String)request.getAttribute("COLLECTORID"));
	ArrayList alordertype = GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("ALORDERTYPE"));
	String strCreditHoldType = GmCommonClass.parseNull((String)request.getAttribute("CREDITTYPE"));
	String strModeOfOrder = GmCommonClass.parseNull((String)request.getAttribute("MODEOFORDER"));
	String strParentFl = GmCommonClass.parseNull((String)request.getAttribute("Chk_ParentFl"));
	ArrayList alCreditHoldList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCREDITTYPE"));
	String strAccountCurrency = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_ID"));
	ArrayList alCompCurrency = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCOMPANYCURRENCY"));
	ArrayList altMode = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ORDERMODES"));
	ArrayList alOrdDateType = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ORDDATETYPE"));
	String strOrdDateType = GmCommonClass.parseNull((String)request.getAttribute("hOrdDateType"));
	ArrayList alCompDiv = (ArrayList)request.getAttribute("ALCOMPDIV");	
	ArrayList alCategory = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCATEGORY"));
	String strShowAddnlColumnFl = GmCommonClass.parseNull((String)request.getAttribute("Chk_addnl_clmn_Fl"));
	int intcolspan=16;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Sales Orders Report </TITLE>
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     #Txt_ToDate{width:74%;}
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="javascript" src="<%=strAccountJsPath%>/GmOrderList.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
var account_Id = '<%=strAccId%>';
var parentFl = '<%=strParentFl%>';
var currency = '<%=strCurrSign%>';
var strApplCurrFmt = '<%=strApplCurrFmt%>';
var strCurrFmt = '<%=strCurrFmt%>';
var compCurrSign = '<%=strAccountCurrency%>';
var showAddnlColumnFl = '<%=strShowAddnlColumnFl%>';
</script>
<script>
function fnLoad()
{
	// Export option not work, because DHTMLX combo (xml string) so, here hide the variable.
		var form = document.getElementsByName('frmAccount')[0];

		 if(parentFl == 'false'){
			document.frmAccount.Chk_ParentFl.checked = false;
		} 
 		
		 if(showAddnlColumnFl == 'false'){
				document.frmAccount.Chk_addnl_clmn_Fl.checked = false;
		} 
		 
		for ( var i = 0; i < form.length; i++) {
			var obj = form.elements[i];
			var objName = obj.name;
			var objType = obj.type;
			var objValue = obj.value;

			if (objType == 'hidden' && objName == 'hCbo_AccId') {
				obj.disabled = true;
				if (account_Id == '0')
					Cbo_AccId.setComboText("[All Accounts]");
			}
		}
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnLoad();" onkeypress="fnEnterPress();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmInvoiceOrderListServlet">
<input type="hidden" name="hAccountList" value="">
<input type="hidden" name="hAccountListPlus" value="">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hLoadScreen" value="<%=strLoadScreen %>">
<input type="hidden" name="hDateFmt" value="<%=strApplDateFmt%>">	
<table border="0" class="DtTable1300" cellspacing="0" cellpadding="0">
	<tr><td height="25" class="RightDashBoardHeader" colspan="9">
	<fmtOrderList:message key="LBL_ORDER_LIST_REPORT"/>
	</td>
	<fmtOrderList:message key="LBL_HELP" var="varHelp"/>
	<td height="25" class="RightDashBoardHeader"><img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
				</tr>
			<tr>
				<td colspan="10" height="20"><jsp:include
						page="/sales/GmSalesFilters.jsp">
						<jsp:param name="HIDE" value="SYSTEM" />
						<jsp:param name="FRMNAME" value="frmAccount" />
						<jsp:param name="HACTION" value="Reload" />
					</jsp:include></td>
			</tr>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr height="30">
		<td colspan = "10"><table cellpadding="0" cellspacing="0" width="100%" border="0">
		<tr>
		<td class="RightTableCaption" align="right" height="25" width="100">
			<fmtOrderList:message key="LBL_REP_ACCOUNT" var="varRepAccount"/>
			<gmjsp:label type="RegularText"  SFLblControlName="${varRepAccount}:" td="false"/></td>
			
				<td align="left" width="650" style="padding-left: 5px;"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
										<jsp:param name="CONTROL_NAME" value="Cbo_AccId" />
										<jsp:param name="METHOD_LOAD" value="loadAccountList&searchType=LikeSearch" />
										<jsp:param name="WIDTH" value="600" />
										<jsp:param name="CSS_CLASS" value="search" />
										<jsp:param name="CONTROL_NM_VALUE" value="<%=strAccountName %>" /> 
										<jsp:param name="CONTROL_ID_VALUE" value="<%=strAccId %>" />
										<jsp:param name="SHOW_DATA" value="100" />
										<jsp:param name="AUTO_RELOAD" value="fnDisplayAccID();" />					
									</jsp:include></td>
				<td width="250">&nbsp;<input type="text" size="8"  name="Txt_AccId" class="InputArea" value="<%= strAccountId %>"
			onFocus="changeBgColor(this,'#AACCE8');" onBlur="fnDisplayAccNm()" >&nbsp;&nbsp;
			<gmjsp:dropdown controlName="Cbo_Comp_Curr"  seletedValue="<%=strAccountCurrency%>" value="<%=alCompCurrency%>" codeId = "ID"  codeName = "NAMEALT" optionId="NAME"/></td>
			<td  class="RightTablecaption" align="left"><fmtOrderList:message key="LBL_SHOW_PARENT_ACCOUNT"/>:&nbsp;<input type="checkbox" size="30" checked="<%=strParentFl %>" name="Chk_ParentFl" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" >					
					 </td>	
				<!-- 	 <td></td>	  -->												
		</tr></table></td>
 </tr>
 <tr><td colspan="10" class="Lline"></td></tr>
 
 <tr height="30" class="Shade">
 <td colspan = "10"><table cellpadding="0" cellspacing="0" width="100%" border="0">
		<tr>
	<fmtOrderList:message  key="LBL_DIVISION_NAME" var="varDivisionName"/>
		<td class="RightTableCaption" align="right" width="100"><gmjsp:label type="RegularText"  SFLblControlName="${varDivisionName}:" td="false"/>&nbsp;</td>
		<td align="left" width="100"><gmjsp:dropdown width="100" controlName="Cbo_Division"  seletedValue="<%= strDivisionId %>" value="<%= alCompDiv %>" codeId = "DIVISION_ID" codeName = "DIVISION_NAME" defaultValue= "${varChooseOne}"/></td>
			
		<td class="RightTableCaption"  align="right" width="200"><fmtOrderList:message key="LBL_COLLECTOR_ID"/>:&nbsp;</td>
		<td align="left" width="250"><gmjsp:dropdown controlName="Cbo_CollectorId"  seletedValue="<%=strCollectorId%>" value="<%= alCollector%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "${varChooseOne}" /></td>
						
		<td class="RightTableCaption"   align="right" width="200" ><fmtOrderList:message key="LBL_CREDIT_HOLD_TYPE"/>:&nbsp;</td>
		<td align="left" width="132"><gmjsp:dropdown controlName="Cbo_CreditTypeId"  seletedValue="<%=strCreditHoldType%>" value="<%= alCreditHoldList%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "${varChooseOne}" /></td>
		<td  class="RightTablecaption" align="left"><fmtOrderList:message key="LBL_SHOW_ADDLN_COLUMN"/>:&nbsp;<input type="checkbox" size="30" checked="<%=strShowAddnlColumnFl %>" name="Chk_addnl_clmn_Fl" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" >					
					 </td>
	<td></td>
	</tr></table></td>
	</tr>
	<tr><td colspan="10" class="Lline"></td></tr>
	
	<tr height="30">
		<td colspan = "10"><table cellpadding="0" cellspacing="0" width="100%" border="0">
		<tr>
	<td class="RightTableCaption" align="right" width="100"><fmtOrderList:message key="LBL_DATE_TYPE"/>:&nbsp;</td>
	<td align="left" width="100"><gmjsp:dropdown controlName="Cbo_OrdDateType" seletedValue="<%=strOrdDateType%>" value="<%= alOrdDateType%>" codeId = "CODEID"  codeName = "CODENM" /></td>
		
	<fmtOrderList:message key="LBL_FROM_DATE" var="varFromDate"/>
	<td class="RightTableCaption"  align="right" width="200"><gmjsp:label type="RegularText"  SFLblControlName="${varFromDate}:" td="false"/>&nbsp;</td>
	<td align="left" width="250"><gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=(dtFrm==null)?null:new java.sql.Date(dtFrm.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/></td>
		
	<fmtOrderList:message key="LBL_TO_DATE" var="varToDate"/>
	<td class="RightTableCaption"  align="right" width="200"><gmjsp:label type="RegularText"  SFLblControlName="${varToDate}:" td="false"/>&nbsp;</td>
	<td align="left" width="100"><gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=(dtTo==null)?null:new java.sql.Date(dtTo.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/></td>
	<td></td>
	</tr></table></td>
	</tr>
	<tr><td colspan="10" class=Lline></td></tr>
	
	<tr class="Shade" height="30">
		<td colspan = "10"><table cellpadding="0" cellspacing="0" width="100%" border="0">
		<tr>
	<td  class="RightTableCaption" align="right" width="100"><fmtOrderList:message key="LBL_MODE_OF_ORDER"/>:&nbsp;</td>
	<td align="left" width="100"><gmjsp:dropdown controlName="Cbo_OrderModeId" seletedValue="<%=strModeOfOrder%>" value="<%= altMode%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "${varChooseOne}" /></td>	 		
	
	<td align="right" class="RightTableCaption" width="200"><fmtOrderList:message key="LBL_ORDER_TYPE" var="varOrderType"/><gmjsp:label type="RegularText"  SFLblControlName="${varOrderType}:" td="false"/>&nbsp;</td>
	<td align="left" width="250"><gmjsp:dropdown controlName="Cbo_OrderType"  seletedValue="<%=strOrderType%>" value="<%= alordertype%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "${varChooseOne}"/></td>
	
	<td class="RightTableCaption" align="right" width="200"><fmtOrderList:message key="LBL_CUST_CATEGORY"/>:&nbsp;</td>
	<td align="left" width="100"><gmjsp:dropdown controlName="Cbo_Category"  seletedValue="<%= strCustCategoryId %>" value="<%=alCategory  %>" codeId = "CODEID" codeName = "CODENM" defaultValue= "${varChooseOne}"  /></td> 
					 			 
	<td class="RightTableCaption" align="center" ><fmtOrderList:message key="BTN_LOAD" var="varLoad"/>
	<gmjsp:button value="&nbsp;${varLoad}&nbsp;" name="Btn_Go" gmClass="button" onClick="javascript:fnSubmit();" buttonType="Load" /> </td>
		</tr></table></td>
	</tr>
		<tr><td colspan="10" class=line></td></tr>
	<tr>
		<td height="30" class="aligncenter" colspan="10">
	<display:table name="requestScope.results.rows" export="true" id="currentRowObject" class="its"  varTotals="ordertotals"   decorator="com.globus.displaytag.beans.DTInvoiceOrderWrapper">
	<display:setProperty name="export.excel.filename" value="A/R Orders.xls" /> 
	  	<fmtOrderList:message key="LBL_ORDER_DATE" var="varOrderDate"/><display:column property="ORDER_DATE" title="${varOrderDate}" style="text-align: center; width:80;"  sortable="true" format="<%=strRptFmt%>"/>
	  	<fmtOrderList:message key="LBL_SURGERY_DATE" var="varSurgDate"/><display:column property="SURG_DATE" title="${varSurgDate}" style="text-align: center; width:80;"  sortable="true" format="<%=strRptFmt%>"/>
	  	<fmtOrderList:message key="LBL_ORDER_ID" var="varOrderId"/><display:column property="ORDER_ID" title="${varOrderId}"  style="text-align: left; width:130;"/>
	  	<fmtOrderList:message key="LBL_ACC_ID" var="varAccId"/><display:column property="ACCT_ID" title="${varAccId }" style="text-align:left;width:80;" sortable="true"/><!-- PMT-36541 -->
	  	<fmtOrderList:message key="LBL_REP_ACCOUNT_NAME" var="varRepAccountName"/><display:column property="ACCT_NAME" title="${varRepAccountName}" sortable="true" style="text-align: left; width:210;" maxLength="27"/>
	    <fmtOrderList:message  key="LBL_DIVISION_NAME" var="varDivName"/><display:column property="DIVISION_NAME" title="${varDivName}" style="width:100px" sortable="true" />
	  	<% if(strCategoryRuleId.equals("Y")){%>
	  	<fmtOrderList:message  key="LBL_CATEGORY" var="varCategory"/><display:column property="CUSTCATEGORY" title="${varCategory}" style="width:100px" sortable="true" />
	  	<%} %>
	  	<fmtOrderList:message key="LBL_MODE_OF_ORDER" var="varModeOfOrder"/><display:column property="MODE_OF_ORDER" title="${varModeOfOrder}" style="width:100px" sortable="true" />
	  	<fmtOrderList:message key="LBL_CREDITTYPE_NAME" var="varCreditTypeName"/><display:column property="CREDITTYPE_NAME" class="alignleft" title="${varCreditTypeName}" sortable="true"/>	
	  	<fmtOrderList:message key="LBL_PAYMENT_TERMS" var="varPaymentTerms"/><display:column property="PAYMENT_TERMS" title="${varPaymentTerms}" style="width:100px" sortable="true" />	
	  	<fmtOrderList:message key="LBL_COLLECTOR_ID" var="VarColletorId"/><display:column property="COLLECTOR_NAME" title="${VarColletorId}" sortable="true" />
	  	<fmtOrderList:message key="LBL_CUSTOMER_PO" var="VarCustomerPO"/><display:column property="CUSTOMER_PO" title="${VarCustomerPO} #" sortable="true" style="text-align: left; width:90;"/>
	  	<fmtOrderList:message key="LBL_PO_ENTERED_DATE" var="varPOEnteredDate"/><display:column property="CUST_PO_DATE" title="${varPOEnteredDate}" sortable="true" style="text-align: center; width:100;"  format= "<%=strRptFmt%>"/>
	  	<fmtOrderList:message key="LBL_PO_AMOUNT" var="varPOAmt"/><display:column property="PO_AMOUNT" title="${varPOAmt}" style="text-align: right; width:100px;" sortable="true" format="<%=strCurrFmt%>"/>
	  	<fmtOrderList:message key="LBL_COPAY" var="varCopay"/><display:column property="COPAY" title="${varCopay}" style="text-align: right; width:100px" sortable="true" />
	  	<fmtOrderList:message key="LBL_CAP" var="VarCapAmount"/><display:column property="CAP_AMOUNT" title="${VarCapAmount}" style="text-align: right; width:100px" sortable="true" />   	
	  	<fmtOrderList:message key="LBL_CURRENCY" var="varCurrency"/><display:column property="ARCURRENCYSYMB" title="${varCurrency}" style="text-align: center; " />
	  	<fmtOrderList:message key="LBL_ORDER_VALUE" var="varOrderValue"/><display:column property="ORDER_VALUE"  title="${varOrderValue}"   style="text-align: right; width:120;" total = "true" format="<%=strCurrFmt%>"/>
	  	<fmtOrderList:message key="LBL_FREIGHT" var="varFreight"/><display:column property="SHIP_COST" title="${varFreight}"    style="text-align: right; width:70;"  total = "true"  format="<%=strCurrFmt%>" />
	  	<fmtOrderList:message key="LBL_TOTAL_ORDER" var="varTotalOrder"/><display:column property="TOT_COST" title="${varTotalOrder}"   style="text-align: right; width:120;"  total = "true"  format="<%=strCurrFmt%>" />
<%		// If mapped to pending PO then display below column 
		if (strOrderType.equals("103184")) 
		{ %>
			<fmtOrderList:message key="LBL_DUE_DAYS" var="varDueDays"/><display:column property="PENDING_DT" title="${varDueDays}" style="text-align:right; width:75;" sortable="true"/>
<% 		}
		else
		{ %>		
	  	<fmtOrderList:message key="LBL_INVOICE_ID" var="varInvoiceId"/><display:column property="INV_ID" title="${varInvoiceId}" style="text-align: left; width:100;" />
	  	<fmtOrderList:message key="LBL_INVOICE_DATE" var="varInvoiceId"/><display:column property="INV_DATE" title="${varInvoiceId}" style="text-align: center; width:100;" format="<%=strRptFmt%>"/>
	  	<fmtOrderList:message key="LBL_TAX_AMOUNT" var="varTaxAmount"/><display:column property="TAX_AMT" class="alignright" title="${varTaxAmount}" sortable="true" total = "true"  />
		<fmtOrderList:message key="LBL_INVOICE_AMOUNT" var="varInoviceAmount"/>	<display:column property="INV_AMT" class="alignright" title="${varInoviceAmount}" sortable="true"  total = "true" />
<%		}%>	  

<%		// If mapped to pending PO then display below column 
		if (strOrderType.equals("103186")) 
		{ %>
			<fmtOrderList:message key="LBL_COGS_VALUE" var="varCOGSValue"/><display:column property="PART_COGS_VAL" title="${varCOGSValue}" class="alignright"  total = "true"  sortable="true"  format="<%=strCurrFmt%>"/>
<% 		} 
		
		if (strShowAddnlColumnFl.equals("true")){
			%>
			<fmtOrderList:message key="LBL_PARENT_ORDER_ID" var="varParentOrdId"/><display:column property="PARENT_ORDER_ID" title="${varParentOrdId}"  sortable="true"  style="text-align: left; width:100;" />
			<fmtOrderList:message key="LBL_CONTRACT" var="varContract"/><display:column property="CONTRACT_TYPE_NAME" sortable="true"   title="${varContract}" style="text-align: left; width:100;" />
			<fmtOrderList:message key="LBL_AGING" var="varAging"/><display:column property="AGING" title="${varAging}"  sortable="true"  style="text-align: left; width:100;" />
			<fmtOrderList:message key="LBL_REP_NM" var="varRepNm"/><display:column property="REP_NAME" title="${varRepNm}" sortable="true"   style="text-align: left; width:100;" />
			<fmtOrderList:message key="LBL_AD_NM" var="varADNm"/><display:column property="AD_NAME" title="${varADNm}"  sortable="true"  style="text-align: left; width:100;" />
			<fmtOrderList:message key="LBL_VP_NM" var="varVPNm"/><display:column property="VP_NAME" title="${varVPNm}" sortable="true"   style="text-align: left; width:100;" />		
			<fmtOrderList:message key="LBL_PO_STATUS" var="varPOStatus"/><display:column property="PO_STATUS" title="${varPOStatus}" sortable="true"   style="text-align: left; width:100;" />
		<% }
		%>
<display:footer media="html"> 
<% 
	HashMap temp = ((HashMap)pageContext.getAttribute("ordertotals"));
	log.debug(" Total value is " + temp);
	log.debug(" strOrderType is " + strOrderType);
	String strCogsTotal = "";
	String strOrdVal = "";
	String strShipCost = "";
	String strTotalCost = "";
	String strTaxAmt = "";
	String strInvAmt = "";
	if(strCategoryRuleId.equals("Y")){
  		strOrdVal =((HashMap)pageContext.getAttribute("ordertotals")).get("column18").toString();
  		strShipCost = ((HashMap)pageContext.getAttribute("ordertotals")).get("column19").toString();
  		strTotalCost = ((HashMap)pageContext.getAttribute("ordertotals")).get("column20").toString();

  		if (strOrderType.equals("103186")) 
		{ 
  		strCogsTotal = ((HashMap)pageContext.getAttribute("ordertotals")).get("column25").toString();
		}
  		if(!strOrderType.equals("103184")) {//Pending PO

  			strTaxAmt = ((HashMap)pageContext.getAttribute("ordertotals")).get("column23").toString(); 
  	  		strInvAmt = ((HashMap)pageContext.getAttribute("ordertotals")).get("column24").toString(); 
  		}
	}else{
  		strOrdVal =((HashMap)pageContext.getAttribute("ordertotals")).get("column17").toString();
  		strShipCost = ((HashMap)pageContext.getAttribute("ordertotals")).get("column18").toString();
  		strTotalCost = ((HashMap)pageContext.getAttribute("ordertotals")).get("column19").toString();
  		
  		if (strOrderType.equals("103186")) 
		{ 
  		strCogsTotal = ((HashMap)pageContext.getAttribute("ordertotals")).get("column24").toString();
		}
  		if(!strOrderType.equals("103184")) {//Pending PO
  			strTaxAmt = ((HashMap)pageContext.getAttribute("ordertotals")).get("column22").toString(); 
  	  		strInvAmt = ((HashMap)pageContext.getAttribute("ordertotals")).get("column23").toString(); 
  		}
	}

%>

  	<tr class = shade>
  	<% if(strCategoryRuleId.equals("Y")){ intcolspan=intcolspan+1;%>
  	<td colspan = "<%=intcolspan %>" align="right"> <B> <fmtOrderList:message key="LBL_TOTAL"/> : </B></td>
  	<%}else{ %>
  	<td colspan = "<%=intcolspan %>" align="right"> <B> <fmtOrderList:message key="LBL_TOTAL"/> : </B></td>
  	<%} %>
		  		
		  		<td  Height="25"   class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strOrdVal)%></td>
		  		<td  Height="25"   class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strShipCost)%></td>
		  		<td  Height="25"   class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strTotalCost)%></td>
		  		<td></td>
		  		<%//if not  pending PO then display below column 
				if (!strOrderType.equals("103184") ) 
				{ %>
				<td> </td> 
		  		<td  Height="25"   class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strTaxAmt)%></td>
		  		<td  Height="25"   class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strInvAmt)%></td>
		  		<%}%>
		  		<%		// If mapped to pending PO then display below column 
				if (strOrderType.equals("103186") ) 
				{ %>
				<td  Height="25"   class="RightText" align="right"><%=strCogsTotal%></td>
			<% 		} 
			if (strShowAddnlColumnFl.equals("true") ) 
				{ %>
				<td colspan=7 Height="25"   class="RightText" align="right">&nbsp;</td>
			<% 		} %>
		  		
	</tr>
 </display:footer>

	</display:table>

 		</td>
	</tr>
</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
