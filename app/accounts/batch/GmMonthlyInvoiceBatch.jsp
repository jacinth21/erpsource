
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %> 
<%@ page import="java.util.ArrayList,java.util.HashMap"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%> 
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib prefix="fmtMonthlyInvoiceBatch" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- accounts/batch/GmMonthlyInvoiceBatch.jsp -->
<fmtMonthlyInvoiceBatch:setLocale value="<%=strLocale%>"/>
<fmtMonthlyInvoiceBatch:setBundle basename="properties.labels.accounts.batch.GmMonthlyInvoiceBatch"/>
<bean:define id="alTransactionType" name="frmMonthlyInvoiceBatch" property="alTransactionType" type="java.util.ArrayList"/>
<bean:define id="alNameList" name="frmMonthlyInvoiceBatch" property="alNameList" type="java.util.ArrayList"/>
 <bean:define id ="invoiceClosingDate" name="frmMonthlyInvoiceBatch" property="invoiceClosingDate" type="java.lang.String"/> 
<bean:define id="gridData" name="frmMonthlyInvoiceBatch" property="gridData" type="java.lang.String"/>
<bean:define id="strOpt" name="frmMonthlyInvoiceBatch" property="strOpt" type="java.lang.String"/>  
<bean:define id="strBatchId" name="frmMonthlyInvoiceBatch" property="strBatchId" type="java.lang.String"/> 
<bean:define id="strAccessFlag" name="frmMonthlyInvoiceBatch" property="strAccessFlag" type="java.lang.String"/> 


<%
String strAccountsBatchJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_BATCH");
String strWikiTitle = GmCommonClass.getWikiTitle("MONTHLY_INVOICE_BATCH");
GmCalenderOperations gmCal = new GmCalenderOperations();
//to setting the time zone
gmCal.setTimeZone(strGCompTimeZone);
String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
String strCurDate = gmCal.getCurrentDate(strGCompDateFmt+"Kms");
/* String strDODateFmt=GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.DO_FMT_ORDER"));
String strFmtedDt  =  GmCommonClass.parseNull(GmCommonClass.getStringFromDate(GmCommonClass.getStringToDate(strCurDate,strGCompDateFmt), strDODateFmt));  */
String strDisabled = (strBatchId.equals("")) ? "false":"true";
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Invoice Batch Add Account</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strAccountsBatchJsPath%>/GmMonthlyInvoiceBatch.js"></script>

<script>
  var objGridData = '<%=gridData%>';
  var namelistLen='<%=alNameList.size()%>';
  var lblClosingDate='<fmtMonthlyInvoiceBatch:message key="LBL_CLOSING_DATE"/>';
  var lblNameList='<fmtMonthlyInvoiceBatch:message key="LBL_NAME_LIST"/>';
  var dtFormat = '<%=strGCompDateFmt%>';
  var fmtDate = '<%=strCurDate%>';
  	  fmtDate = fmtDate.replace(/[/]/g , "");
  var incldtAcsFlg = '<%=strAccessFlag%>';
</script> 
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoadRpt();">

	<html:form action="/gmMonthlyInvoiceBatch.do?method=loadMonthlyInvoiceBatch">
	<html:hidden property="strOpt" />
	<html:hidden property="dealerInputStr" />
	<html:hidden property="accInputStr" />
	<html:hidden property="poInputStr" />
	<html:hidden property="transactionType" name="frmMonthlyInvoiceBatch"/>
	
<table border="0" class="DtTable1100" cellspacing="0"
			cellpadding="0">
			<tr>
				<td colspan="5" height="25" class="RightDashBoardHeader"><fmtMonthlyInvoiceBatch:message key="LBL_MONTHLY_INVOICE_BATCH"/></td>
				<td height="25" class="RightDashBoardHeader"><fmtMonthlyInvoiceBatch:message key="IMG_HELP" var="varHelp"/><img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			
		
		<tr class="evenshade" height="28">
		
						 <td  class="RightTableCaption"  align="right"><fmtMonthlyInvoiceBatch:message key="LBL_BILLING_CUSTOMER"/>:&nbsp;</td>
						 <td class="RightTableCaption"><gmjsp:dropdown controlName="billingCustomer" SFFormName="frmMonthlyInvoiceBatch" SFSeletedValue="billingCustomer"
					SFValue="alBillingCustomer" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]" onChange="fnOnBillingCustomerChange(this);"/>
						</td>
						
						 <td class="RightTableCaption" align="right" colspan="3"><fmtMonthlyInvoiceBatch:message key="LBL_NAME_LIST"/>:&nbsp; </td>
						<td id="namelist_id">
						<gmjsp:dropdown controlName="selectedNameList" SFFormName="frmMonthlyInvoiceBatch" SFSeletedValue="selectedNameList" 
						codeId="ID" codeName="NAME" tabIndex="1" SFValue="alNameList" defaultValue="[Choose One]" onChange="fnOnChange();"/>
						 <html:text property="selectedNameId" size="10" styleId="selectedNameId"  styleClass="InputArea"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff'); fntabOut(this);"/>  
						</td>
						
						
						
						
		</tr>
					
		<tr class="oddshade" height="28">
						 <td  class="RightTableCaption"  align="right"><font color="red">*</font>&nbsp;<fmtMonthlyInvoiceBatch:message key="LBL_CLOSING_DATE"/>:&nbsp;</td>
						 <td class="RightTableCaption"> <gmjsp:dropdown controlName="invoiceClosingDate" SFFormName="frmMonthlyInvoiceBatch" SFSeletedValue="invoiceClosingDate"
					SFValue="alInvoiceClosingDate" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]" onChange="fnOnClosingDateChange();"/>
						</td>
						
						 <td class="RightTableCaption" align="right"><fmtMonthlyInvoiceBatch:message key="LBL_DATE_OF_ISSUE"/>:&nbsp;&nbsp;<fmtMonthlyInvoiceBatch:message key="LBL_FROM_DATE"/>:&nbsp;&nbsp;</td>
						 <td colspan="3" class="RightTableCaption">
						 <fmtMonthlyInvoiceBatch:formatDate value="${frmMonthlyInvoiceBatch.fromDate}" pattern="<%=strGCompDateFmt%>" var="varfromDt"/>
						<input type="text" size="10" readonly style ="cursor:default;" value="${varfromDt}" name="fromDate" id="fromDate" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onchange="javascript:onChangeFromDate(this)" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
						<img id="Img_Date" style="cursor:hand" onclick="javascript:showSingleCalendar('divTxt_Date','fromDate');" title="calendar"   src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />
									<div id="divTxt_Date" style="position: absolute; z-index: 10;"></div>

						  <fmtMonthlyInvoiceBatch:message key="LBL_TO_DATE"/>:&nbsp;
						
						 <gmjsp:calendar  SFFormName='frmMonthlyInvoiceBatch'   SFDtTextControlName="toDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/> 
						</td>
						
						
						
		</tr>
		<tr class="evenshade" height="1000px">
						 <td  class="RightTableCaption"  align="right"><font color="red">*</font>&nbsp;<fmtMonthlyInvoiceBatch:message key="LBL_TRANSACTION_TYPE"/>:&nbsp;</td>
						
						<td> <DIV style="display: visible; height: 65px; width:150px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 1px; margin-right: 2px;">
			

			<table>
				  <logic:iterate id="selectedTransType"
					name="frmMonthlyInvoiceBatch" property="alTransactionType">
					<tr>
						<td><htmlel:multibox property="checkSelectedTransType" value="${selectedTransType.CODEID}" onclick="fnChkTransType(this);" />
							 <bean:write name="selectedTransType" property="CODENM" /></td>
					</tr>
				</logic:iterate>
			</table>
			</DIV>   </td>
			
  
						
						<fmtMonthlyInvoiceBatch:message key="BTN_LOAD" var="varLoad"/>
						 <td class="RightTableCaption" align="center" colspan="3"><gmjsp:button value="&nbsp;${varLoad}&nbsp;"gmClass="button" buttonType="Load" onClick="fnLoad()" /> </td>
						
						
						
						
		</tr>
		<tr id="invStmtSuccessId" style="display: none;">
		<td colspan=6 align="center"><font color="green"><fmtMonthlyInvoiceBatch:message key="LBL_INV_ST"/></font></td>
		</tr>
		 <logic:notEmpty name="frmMonthlyInvoiceBatch" property="strBatchId">
					<tr>
						<td height="25" align="center" colspan="6" class="RightTableCaption">
						<font color="green"><fmtMonthlyInvoiceBatch:message key="LBL_BATCH"/> # <%=strBatchId%> <fmtMonthlyInvoiceBatch:message key="LBL_SUCCESSFULLY"/></font>
						</td>
					<tr>			
			 </logic:notEmpty> 
		
		 <%if(strOpt.equals("Reload")||strOpt.equals("save")){ %>			
<logic:empty name="frmMonthlyInvoiceBatch" property="strBatchId">
				<tr>
				<td colspan="9"><div  id="dataGridDiv" height="300px"></div></td>
				
				</tr>
				<tr height="34" id="submitButtonId">
				<fmtMonthlyInvoiceBatch:message key="BTN_SUBMIT" var="varSubmit"/>
				<td class="RightTableCaption" align="center" colspan="9"><gmjsp:button value="&nbsp;${varSubmit}&nbsp;"gmClass="button" disabled="<%=strDisabled%>" buttonType="Save" onClick="fnSubmit()" /> </td>
				</tr>
				 </logic:empty> 
		<%} %>
			
			
		</table>
	</html:form>


	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>