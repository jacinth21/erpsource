<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap"%>
<%@ taglib prefix="fmtInvBatchAddAccount" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- accounts\batch\GmInvBatchAddAccount.jsp -->
<fmtInvBatchAddAccount:setLocale value="<%=strLocale%>"/>
<fmtInvBatchAddAccount:setBundle basename="properties.labels.accounts.batch.GmInvBatchAddAccount"/>

<bean:define id="gridData" name="frmInvBatch" property="gridData" type="java.lang.String"></bean:define>
<%
	String strAccountsBatchJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_BATCH");
	String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("CREATE_INVOICE_BATCH"));
	String strApplDateFmt = GmCommonClass.parseNull((String) session.getAttribute("strSessApplDateFmt"));
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Invoice Batch Add Account</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>
<script language="javascript" src="<%=strJsPath%>/GmGrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strAccountsBatchJsPath%>/GmInvBatchAddAccount.js"></script>

<script>
var objGridData;
objGridData = '<%=gridData%>';
var gridObj = '';
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">

	<html:form action="/gmInvBatchAdd.do?method=loadInvBatchAddAccount">
		<html:hidden property="strOpt" />
		<html:hidden property="inputString" value="" />

		<table border="0" class="DtTable850" width="850" cellspacing="0"
			cellpadding="0">
			<tr>
				<td colspan="5" height="25" class="RightDashBoardHeader"><fmtInvBatchAddAccount:message key="LBL_INVOICE_BATCH_ADD_ACCOUNTS"/></td>
				<td height="25" class="RightDashBoardHeader"><fmtInvBatchAddAccount:message key="IMG_HELP" var="varHelp"/><img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<%
				if (gridData.indexOf("cell") != -1) {
			%>
			<tr>
				<td colspan="6">
					<div id="invBatchAddAccount" style="grid" height="400px" width="848px"></div>
				</td>
			</tr>
			<tr height="40">
				<td colspan="6" align="center" id="AllButton"><fmtInvBatchAddAccount:message key="BTN_NEXT" var="varNext"/><gmjsp:button
					 value="${varNext}" buttonType="Load" gmClass="Button" name="btn_Next"
					style="width: 6em" onClick="fnNext();" />
				</td>
			</tr>
			<%}else{ %>
			<tr height="30">
				<td colspan="6" class="RightText" align="center"><fmtInvBatchAddAccount:message key="LBL_NO_POS_ARE_PENDING_INVOICE_GENERATION"/></td>
			</tr>
			<%} %>

		</table>



	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
