 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap"%>
<%@ taglib prefix="fmtInvBatchAddEditPO" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- accounts/batch/GmInvBatchAddEditPO.jsp -->
<fmtInvBatchAddEditPO:setLocale value="<%=strLocale%>"/>
<fmtInvBatchAddEditPO:setBundle basename="properties.labels.accounts.batch.GmInvBatchAddAccount"/>

<bean:define id="gridData" name="frmInvBatchEdit" property="gridData" type="java.lang.String"></bean:define>
<bean:define id="screenType" name="frmInvBatchEdit" property="screenType" type="java.lang.String"></bean:define>
<bean:define id="invBatchID" name="frmInvBatchEdit"	property="invBatchID" type="java.lang.String"></bean:define>
<bean:define id="strErrorCustPos" name="frmInvBatchEdit"	property="strErrorCustPos" type="java.lang.String"></bean:define>
<bean:define id="invBatchStatus" name="frmInvBatchEdit" property="invBatchStatus" type="java.lang.String"></bean:define>
<bean:define id="refType" name="frmInvBatchEdit" property="refType" type="java.lang.String"></bean:define>
<%
	String strAccountsBatchJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_BATCH");
    GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.accounts.batch.GmInvBatchAddAccount", strSessCompanyLocale);
	String strWikiTitle = GmCommonClass.getWikiTitle("INVOICE_BATCH");
	String strApplDateFmt = GmCommonClass.parseNull((String) session.getAttribute("strSessApplDateFmt"));
	String strSessCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	String strDisabled = "";
	String strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_BATCH_DETAILS_REPORT"));
	String strSubHeader = "";
	String strScreenButton = "";
	String strProcessButDisabled = "";
// Batch Type (Invoice Batch)
if(refType.equals("18751")){
	// to show the default Header and Sub header
	strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INVOICE_BATCH_ADD_POS"));
	strSubHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ADD_POS"));
	
	if (invBatchStatus.equals("18742") && screenType.equals("SavePOs")) { // Pending Processing
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INVOICE_BATCH_DETAILS"));
		strSubHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_BATCH_CUSTOMER_POS"));
	} else if (invBatchStatus.equals("0") || (invBatchStatus.equals("18742") && screenType.equals("EditPOs"))) { // Pending Processing
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INVOICE_BATCH_EDIT_BATCH"));
		strSubHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_EDIT_BATCH_POS"));
		strScreenButton = "EditPOs";
	} else if (invBatchStatus.equals("18743") || invBatchStatus.equals("18744")) { //Processing In Progress (18743) - Processed (18744)
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INVOICE_BATCH_PROCESSED"));
		strSubHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INVOICED_CUSTOMER_POS"));
		strScreenButton = "ProcessedPOs";
		strDisabled = "true";
		if(invBatchStatus.equals("18744")){
			strProcessButDisabled = "true";
		}
	}
}else if(refType.equals("18752")){ // (Do Summary Print)
	//strHeader = "Processed";
	strSubHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_EDIT_BATCH"));
	
	if(invBatchStatus.equals("18744") || invBatchStatus.equals("18743")){
		strDisabled = "true";
		strProcessButDisabled = "true";
	}
}

if(invBatchStatus.equals("0")){
	strDisabled = "true";
	strProcessButDisabled = "true";
}

%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Invoice Batch - Add POs</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strAccountsBatchJsPath%>/GmInvBatchAddEditPO.js"></script>


<script>
var objGridData;
var rowID = '';
objGridData = '<%=gridData%>';
	var gridObj = '';

	 var accName_rowID = '';
	 var chk_rowID = '';
	 var img_rowID = '';
	 var custPO_rowID = '';
	 var poDate_rowID = '';	
	 var totalInv_rowID = '';	
	 var orderDate_rowID = '';
	 var status_rowID = '';
	 var dist_rowID = '';
	 var emailReq_rowID = '';
	 var electVer_rowID = '';
	 var inv_rowID = '';
	 var order_rowID = '';		
	 var accID_rowID = '';
	 var holdFl_rowID = '';
	 var currSign = '<%=strSessCurrSign%>';
	 var screen_type = '<%=screenType%>';
	 var errorPos = "<%=strErrorCustPos%>";
</script>

<style>
#batchEditPO .ftr table{
 table-layout: auto!important;
 height: 21px;
 padding-right: 0px!important;
}
</style>
    
    
</HEAD>


<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
	<html:form action="/gmInvBatchEdit.do?method=invBatchAddPOs">
		<html:hidden property="strOpt" />
		<html:hidden property="inputString" value="" />
		<html:hidden property="screenType" />
		<html:hidden property="refType" />
		<html:hidden property="invBatchID" />
		<html:hidden property="invBatchStatus" />
		<html:hidden property="hTxnId" value="" />
		<html:hidden property="hTxnName" value="" />
		<html:hidden property="strAccountIDs"/>
		<html:hidden property="strCompCurrency"/>
		<input type="hidden" name="hAction" value="" />
		<input type="hidden" name="hCancelType" value="" />
		<input type="hidden" name="hRedirectURL" value="" />
		<input type="hidden" name="hDisplayNm" value="" />




		<table border="0" class="DtTable1200" width="1200" cellspacing="0"
			cellpadding="0">
			<tr>
				<td colspan="5" height="25" class="RightDashBoardHeader"><%=strHeader%></td>
				<td height="25" class="RightDashBoardHeader"><fmtInvBatchAddEditPO:message key="IMG_HELP" var="varHelp"/><img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>

			<tr>
				<td colspan="6" class="LLine" height="1"></td>
			</tr>
			<tr>
				<td colspan="6"><jsp:include page="/gmBatchInfo.do" flush="true">
								<jsp:param name="batchID" value="<%=invBatchID %>" />
								</jsp:include>
					</td>
			</tr>
			<tr>
				<td colspan="6" class="LLine" height="1"></td>
			</tr>
			
			<tr>
			<logic:notEmpty name="frmInvBatchEdit" property="strSuccessMsg">
					<tr>
						<td height="25" align="center" colspan="6" class="RightTableCaption">
						<font color="green"><bean:write name="frmInvBatchEdit" property="strSuccessMsg" /></font>
						</td>
					<tr>			
			</logic:notEmpty>

				<td height="25" class="ShadeRightTableCaption" colspan="6"><%=strSubHeader%></td>
			</tr>

			<tr>
				<td colspan="6">
					<div id="batchEditPO" style="width: 1200px; height: 400px;"></div>
				</td>
			</tr>
			
			<tr height="40">

				<td colspan="6" align="center" id="AllButton">
					<logic:equal name="frmInvBatchEdit" property="invBatchStatus" value="18741">
						<fmtInvBatchAddEditPO:message key="BTN_BACK" var="varBack"/><gmjsp:button value="${varBack}" gmClass="Button" name="btn_Back" style="width: 6em" buttonType="Load" onClick="fnBack();" />&nbsp;&nbsp;&nbsp;
						<fmtInvBatchAddEditPO:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="Button" name="btn_Submit" style="width: 6em" buttonType="Save" onClick="fnSubmit();" />&nbsp;&nbsp;&nbsp;
				 	</logic:equal>
				 	 <logic:greaterEqual name="frmInvBatchEdit" property="invBatchStatus" value="18742">
				 	 <logic:notEqual name="frmInvBatchEdit" property="screenType"  value="SavePOs">
						<fmtInvBatchAddEditPO:message key="BTN_PROCESS_BATCH" var="varProcessBatch"/><gmjsp:button value="${varProcessBatch}" gmClass="Button" name="btn_Prcess" style="width: 8em" buttonType="Save" onClick="fnProcessBatch();" disabled="<%=strProcessButDisabled%>" />&nbsp;&nbsp;&nbsp;
						<fmtInvBatchAddEditPO:message key="BTN_UNLINK" var="varUnlink"/><gmjsp:button value="${varUnlink}" gmClass="Button" name="btn_Unlink" style="width: 6em" buttonType="Save" onClick="fnUnlink();" disabled="<%=strDisabled%>" />&nbsp;&nbsp;&nbsp;
					</logic:notEqual>
					</logic:greaterEqual>
					<fmtInvBatchAddEditPO:message key="BTN_VOID_BATCH" var="varVoidBatch"/><gmjsp:button value="${varVoidBatch}" gmClass="Button" name="btn_voidBatch" style="width: 6em" buttonType="Save" onClick="fnVoidBatch();"	disabled="<%=strDisabled%>" />
				</td>
			</tr>
		</table>
	</html:form>


	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>