<%
	/**********************************************************************************
	 * File		 		: GmStandardCostReport.jsp
	 * Desc		 		: This screen is used for the Standard Cost Reports
	 * author			: Prabhu Vingeshwaran M D
	 ***********************************************************************************
	 */
%>
 <!-- \accounts\GmVendorStandardCostRpt.jsp -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtStandardCostReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtStandardCostReport:setBundle basename="properties.labels.accounts.GmStandardCostReport"/>
<bean:define id="gridData" name="frmStandardReport" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="name" name="frmStandardReport" property="name" type="java.lang.String"> </bean:define> 

<%
String strAccountsJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
String strWikiTitle = GmCommonClass.getWikiTitle("STD_COST_RPT");
%> 


<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Standard Cost Report</title>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strAccountsJsPath%>/GmVendorStandardCostRpt.js"></script>

<script>
var gridObj = ''; 
var objGridData = '<%=gridData%>';

</script>
</head>

<BODY leftmargin="20" topmargin="10" onkeyup="javascript:fnEnter();" onload="javascript:fnOnPageLoad();">
	<html:form action="/gmVendorStandardCost.do?method=loadStandardCostRpt">
	<html:hidden property="strOpt" value=""/>
		<table border="0" class="DtTable1055" width="1055" cellspacing="0" cellpadding="0" 
		style="border-width: 1px ; border-style: solid;border-color: #000000">
			<tr>
				<td colspan="5">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
						<td height="25" class="RightDashBoardHeader">&nbsp;<fmtStandardCostReport:message key="LBL_STD_COST_REPORT"/></td>
						<td  height="25" class="RightDashBoardHeader" align="right" colspan="4">
							<fmtStandardCostReport:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
			      			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>',' <%=strWikiTitle%>');" />
			      			</td>
		      		</tr></table>
	      		</td>
			</tr>
			<tr>
				<td class="RightTableCaption"  align="right" height="30" width="100">&nbsp;<fmtStandardCostReport:message key="LBL_VENDOR_NAME"/></td>
				<td>&nbsp;<gmjsp:dropdown 
				controlName="strVendorId" 
				SFFormName="frmStandardReport" 
				SFSeletedValue="strVendorId"
				SFValue="alVendorList" 
				codeId = "ID"  
				codeName = "NAME"  
				 defaultValue="[Choose One]" 
				 width="315"/>
				&nbsp;&nbsp;&nbsp;
				
				<td class="RightTableCaption" align="right" height="30" width="100"><fmtStandardCostReport:message key="LBL_PART_NUMBER"/></td>
			    <td>&nbsp;
			    <html:text 
			    size="30" 
			    name="frmStandardReport" 
			    property="strPartNumbers"  
			    styleClass="InputArea"	
			    onfocus="changeBgColor(this,'#AACCE8');" 
			    onblur="changeBgColor(this,'#ffffff');" />
        		
				
			</tr>
			<tr><td class="LLine" colspan="4" height="1"></td></tr>
			<tr class="Shade">
				<td class="RightTableCaption" align="right" height="30" width="100"><fmtStandardCostReport:message key="LBL_PROJECT_NAME"/></td>
				<td>&nbsp;<gmjsp:dropdown 
				controlName="projectId" 
				SFFormName="frmStandardReport" 
				SFSeletedValue="projectId"
				SFValue="projectList" 
				codeId = "ID"  
				codeName = "NAME"  
				 defaultValue="[Choose One]" 
				 width="315"/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<td align = "center"  colspan="5" height="30" width="10"><gmjsp:button name="Btn_Load" value="Load" gmClass="button" onClick="javascript:fnLoad();" buttonType="Load" /></td>
			</tr>
			<tr><td class="LLine" colspan="4"></td></tr>
			<%if(gridData.indexOf("</row>")!=-1){ %>
				<tr> 
					<td colspan="5">
						<div id="dataGridDiv" class="" height="450px"></div>
					</td>
				</tr>
			
				<tr>
                <td colspan="5"  height="20" align="center">
                <div class='exportlinks'>Export Options: <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
                                onclick="fnExport();">Excel</a></div>
                </td>
				</tr>
				<%}else{ %>
					<tr><td colspan="8" height="1" class="LLine"></td></tr>
					<tr>
						<td colspan="8" align="center" height="30" class="RightText">
							No Data Found
						</td>
					</tr>
					<%} %>
				
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</body>
</html>