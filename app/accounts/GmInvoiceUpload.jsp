<%/**********************************************************************************
 * File		 		: GmInvoiceUpload.jsp
 * Desc		 		: This screen is used for upload invoice doc
 * Version	 		: 1.0
 * author			: Gopinathan
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap" %>
<%@ taglib prefix="fmtInoviceUpload" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmInvoiceUpload.jsp -->
<fmtInoviceUpload:setLocale value="<%=strLocale%>"/>
<fmtInoviceUpload:setBundle basename="properties.labels.accounts.GmInvoiceUpload"/>
<html>
<bean:define id="hmOrderDetails" name="frmInvoiceUpload" property="hmOrderDetails" type="java.util.HashMap"></bean:define>
<%
String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
String strRptFmt = "{0,date,"+strApplDateFmt+"}";
String strInvNum = "";
String strAccId= "";
String strDistRepNm = "";
String strBillAdd = "";
java.sql.Date dtInvDate = null;
java.sql.Date dtDueDate = null;
String strPO = "";
String strOrdId = "";
String strPayNm = "";

if(hmOrderDetails != null){

strInvNum = GmCommonClass.parseNull((String)hmOrderDetails.get("INV"));
strAccId = GmCommonClass.parseNull((String)hmOrderDetails.get("ACCID"));
strPO = GmCommonClass.parseNull((String)hmOrderDetails.get("PO"));
strBillAdd = GmCommonClass.parseNull((String)hmOrderDetails.get("BILLADD"));
strDistRepNm = GmCommonClass.parseNull((String)hmOrderDetails.get("REPDISTNM"));
strPayNm = GmCommonClass.parseNull((String)hmOrderDetails.get("PAYNM"));
dtInvDate = (java.sql.Date)hmOrderDetails.get("INVDT");
dtDueDate = (java.sql.Date)hmOrderDetails.get("DUEDT");
}
%>
<head>
<TITLE> Globus Medical:Invoice document Upload</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmInvoiceUpload.js"></script>
</head>
<BODY leftmargin="20" topmargin="10">
<html:form method="post" action = "/gmInvoiceUpload.do?" enctype="multipart/form-data">
	<html:hidden property="invoiceId" />
	<html:hidden property="strOpt" />
	<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
			<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
						<fmtInoviceUpload:message key="LBL_INVOICE_UPLOAD" var="varInvoiceUpload"/>
						<td height="25" class="RightDashBoardHeader"><gmjsp:label type="RegularText"  SFLblControlName="${varInvoiceUpload}" td="false"/></td>
						<td class="RightDashBoardHeader" align="right">
						<fmtInoviceUpload:message key="LBL_HELP" var="VarHelp"/>
						<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${VarHelp}'onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("INVOICE_UPLOAD")%>');" />
						</td>
						
					</tr>
					<tr>
						<td colspan="2">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr class="ShadeRightTableCaption">
									<fmtInoviceUpload:message key="LBL_BILL_TO" var="varBillTo"/>
									<td height="25" align="center" class="RightText" width="450">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varBillTo}" td="false"/></td>
									<td bgcolor="#666666" width="1" rowspan="7"></td>
									<fmtInoviceUpload:message key="LBL_CUSTOMER_PO" var="varCustomerPO"/>
									<td height="25" align="center" class="RightText">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varCustomerPO} #" td="false"/></td>
									<td bgcolor="#666666" width="1" rowspan="7"></td>
									<fmtInoviceUpload:message key="LBL_INVOCIE_DATE" var="varInvoiceDate"/>
									<fmtInoviceUpload:message key="LBL_TERMS" var="varTerms"/>
									<td height="25" width="100" class="RightText" align="center">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varInvoiceDate}" td="false"/></td>
									<td height="25" width="100" class="RightText" align="center">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varTerms}" td="false"/></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="6"></td>
								</tr>
								<tr>
									<td class="RightText" rowspan="5" valign="top">&nbsp;<%=strBillAdd%></td>
									<td class="RightText" align="center">&nbsp;<%=strPO%></td>
									<td height="25" class="RightText" align="center">
										<%=GmCommonClass.getStringFromDate(dtInvDate,strApplDateFmt)%>
									</td>
									<td class="RightText" align="center">&nbsp;<%=strPayNm%></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
								</tr>
								<tr class="ShadeRightTableCaption">
									<fmtInoviceUpload:message key="LBL_INVOICE" var="varInvoice"/>
									<fmtInoviceUpload:message key="LBL_DIST_REP" var="varDistRep"/>
									<td align="center" class="RightText" height="20"><gmjsp:label type="RegularText"  SFLblControlName="${varInvoice} #" td="false"/></td>
									<td align="center" class="RightText"><gmjsp:label type="RegularText"  SFLblControlName="${varDistRep}" td="false"/></td>
									<td align="center">&nbsp;</td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
								</tr>
								<tr>
									<td class="RightText" rowspan="3" align="center">&nbsp;<%=strInvNum%></td>
									<td align="center" class="RightText">&nbsp;<%=strDistRepNm%><Br></td>
									<td align="center" height="25" class="RightText">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
			<tr><td class="Line" colspan="2"></td></tr>
			<tr id="pgupload" style="display: block">
				<td class="RightText" width="30%" align="right" height="30"><b><fmtInoviceUpload:message key="LBL_SELECT_FILE_TO_UPLOAD"/> :</b></td>
				<td height="30">&nbsp;
				<!-- Removed id="file" for JBOSS migration, since the attribute is not there in tld file -->
					<html:file name="frmInvoiceUpload" property="file" size="50" accept="*.rtf"/>
				</td>
			</tr>
			<tr>
				<fmtInoviceUpload:message key="BTN_UPLOAD" var="varUpload"/>
				<td colspan="2" height="40" align="center"><br><gmjsp:button controlId="UploadBtn"  name="UploadBtn" value="${varUpload}" gmClass="button"
					onClick="upload();" buttonType="Save" /></td>
			</tr>
			<tr id="pgtitle" style="display: none">
				<td colspan=2 align="center"><fmtInoviceUpload:message key="LBL_UPLOAD_IN_PROGRESS"/></td>
			</tr>
			<tr id="pgbar" style="display: none">
				<td colspan=2 align="center"><img
					src="<%=strImagePath%>/progress_bar.gif"></td>
			</tr>
			<tr><td class="Line" colspan="2"></td></tr>
			<tr>
				<td class="RightTableCaption" colspan="2"><fmtInoviceUpload:message key="LBL_PREVIOUS_UPLOADS"/></td>
			</tr>
			<tr><td class="LLine" colspan="2"></td></tr>
 			<tr>
				<td colspan="2">
				<display:table paneSize="400" name="requestScope.frmInvoiceUpload.alUploadinfo" export="false" cellpadding="0" cellspacing="0" class="its" varTotals="totals" requestURI="/gmInvoiceUpload.do?" id="currentRowObject" decorator="com.globus.accounts.displaytag.DTInvoiceUploadWrapper" freezeHeader="true" >
            		<display:column property="ID" title="#" class="alignleft" />
            		<fmtInoviceUpload:message key="LBL_FILE_NAME" var="varFileName"/><display:column property="NAME" title="${varFileName}" class="alignleft" />
            		<fmtInoviceUpload:message key="LBL_DATE" var="varDate"/><display:column property="CDATE" title="${varDate}" class="aligncenter" format="<%=strRptFmt%>"/>
            		<fmtInoviceUpload:message key="LBL_UPLOAD_BY" var="varUploadBy"/><display:column property="UNAME" title="${varUploadBy}" class="alignleft" />            	
            	</display:table>					
				</td>
			</tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>