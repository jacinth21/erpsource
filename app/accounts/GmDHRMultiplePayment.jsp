 <%@page import="java.util.Locale"%>
<%
/**********************************************************************************
 * File			 		: GmDHRMultiplePayment.jsp
 * Desc		 		: This screen is used for the DHR Ready for Payment
 * Version	 		: 1.0
 * author			: Joe Prasanna Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtDhrMulitplePayment" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmDHRMultiplePayment.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtDhrMulitplePayment:setLocale value="<%=strLocale%>"/>
<fmtDhrMulitplePayment:setBundle basename="properties.labels.accounts.GmDHRMultiplePayment"/>
<%
response.setHeader("Cache-Control", "no-cache, post-check=0, pre-check=0"); //HTTP 1.1
response.setHeader("Pragma", "no-cache");  //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server 
%>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	int intSize = 0;
	int intLength = 0;

	
	String strCodeID = "";
	String strSelected = "";
	String strVendId = "";
	String strCurDate = "";
	
	ArrayList alVendor = new ArrayList();
	ArrayList alPayModes = new ArrayList();
		
	HashMap hmLoadReturn = new HashMap();
	HashMap hcboVal = new HashMap();
	HashMap hmReturn = new HashMap();
	HashMap hmParam = new HashMap();

	hmLoadReturn = (HashMap)request.getAttribute("hmLoadReturn");
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	hmParam = (HashMap)request.getAttribute("hmParam");
	
	// Getting Details from hmLoadReturn which has the Filter parameter datas that needs to be reposted on the screen
	String strPartID = GmCommonClass.parseNull((String)hmLoadReturn.get("PARTID"));
	String strPOID = GmCommonClass.parseNull((String)hmLoadReturn.get("POID"));
	String strPSlip = GmCommonClass.parseNull((String)hmLoadReturn.get("PSLIP"));
	String strAction = (String)request.getAttribute("hAction");
	
	if (strAction.equals("load") || strAction.equals("Reload"))
	{
		alVendor = (ArrayList)hmReturn.get("VENDORLIST");
		alPayModes = (ArrayList)hmReturn.get("PAYMODEDETAILS");
		strVendId = GmCommonClass.parseNull((String)hmParam.get("VENDID"));
		strCurDate = GmCommonClass.parseNull((String)hmParam.get("CURRDT"));
	}
   
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Post Multiple Payments - By Account</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     
	width: 60%;
	height: 60px;
}
</style>

<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script>

function fnCallDHR(id)
{
var venId = document.frmAccount.Cbo_VendorId.value;
windowOpener("/GmPOReceiveServlet?hAction=ViewDHR&hDHRId="+id+"&hVendorId="+venId,"INVPOP","resizable=yes,scrollbars=yes,top=300,left=300,width=750,height=750");
}

function fnGo()
{
	Error_Clear();	

	// Validate if the Vendor is selected
	if(document.frmAccount.Cbo_VendorId.value == "0")	
			{
				Error_Details(message[70]);
			}
	else 
			{
				document.frmAccount.hAction.value = "Reload";
				document.frmAccount.submit();
			}
			
			if (ErrorCount > 0)
		{
				Error_Show();
				return false;
		}
}


function fnPostPayment()
{
	Error_Clear();	
	
	// Date Validation
	DateValidate(document.frmAccount.Txt_PDate,message[1]);
	
	// To validate if the Invoice Number is Blank
	var invoiceNum = document.frmAccount.Txt_InvNum.value;
	if (invoiceNum == "")
	 {
		Error_Details(message[71]);
	 }
	
	// To validate if the Payment Date is Blank
	var pdate = document.frmAccount.Txt_PDate.value;
	if (pdate == "")
	 {
		Error_Details(message[72]);
	 }
	
	// Get the DHR ID and set it to hInputStr which will be used by the Bean for update query 
	var theinputs=document.getElementsByTagName("input");
	var the_textfields=new Array();
	var str = '';
	var Id_DHRid = '';
	var dhrID = ' ';
	var cnt = 0;
	
	for(var n=0;n<theinputs.length;n++)
	{
		if(theinputs[n].type=="checkbox")
		{
			if (theinputs[n].checked)
			{
				val = theinputs[n].value;
				Id_DHRid = eval("document.frmDtag.hDHRID_"+val);
				dhrID = Id_DHRid.value;
				str =  str + dhrID + '^';
				cnt++;
			}
		}
	}
	// Validate if atleast one Pack Slip is checked
	if (cnt == 0) {
		Error_Details(message[73]);
	}
	else if (cnt > 1 )	{
		str = str.substr(0,str.length-1);
	}
	
	if (ErrorCount > 0)
	{
			Error_Show();
			return false;
	}

	document.frmAccount.hInputStr.value = str;
	document.frmAccount.hAction.value = "UPDALL";
	document.frmAccount.submit();
}


// Calculate the Total Value of the TCOST for those checked rows
var TotalValue = 0;
function fnCalculateAmount(CheckValue)
{
	var amt = 0;
	val = CheckValue.value;
	pendobj = eval("document.frmDtag.hDHRAmt"+val);
	pendamt = pendobj.value;
	amt = pendamt * 1;
	
	if (CheckValue.checked)
	{
		TotalValue += amt ;
	}
	else
	{
		TotalValue -= amt ;
	}
	
	document.all.IDCalculate.innerHTML = "&nbsp;$" + formatNumber(TotalValue) + "&nbsp;"; 
	document.frmAccount.hTotal.value = TotalValue;
}	
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GMDHRFlagInvoiceServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hTotal" value="">
<input type="hidden" name="hVendorId" value="">
<input type="hidden" name="hDHRId" value="">

	<table class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader"><fmtDhrMulitplePayment:message key="LBL_POST_PAYMETNS_DHR"/></td>
		</tr>
		<tr height="24" class=Shade>
			<td class="alignright" width="140"><fmtDhrMulitplePayment:message key="LBL_VENDOR"/>:</td>                  
			<td>&nbsp;
				 <select name="Cbo_VendorId" id="Region" class="RightText" tabindex="1"
     				onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
	    			<option value="0" >[Choose One]
<%
		  				ArrayList alVendorList = (ArrayList)hmReturn.get("VENDORLIST");
						intSize = alVendorList.size();
			  			for (int i=0;i<intSize;i++)		  		{
				  			hcboVal = (HashMap)alVendorList.get(i);
				  			strCodeID = (String)hcboVal.get("ID");
			  				strSelected = strVendId.equals(strCodeID)?"selected":" ";
				%>				<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%	
					}
%>
				</select>
				<fmtDhrMulitplePayment:message key="LBL_GO" var="varGo"/>
			&nbsp;&nbsp;&nbsp;<gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" gmClass="button" onClick="fnGo();" tabindex="2" buttonType="Load" />&nbsp;
				<BR>
			</td>
		</tr>
		<tr>
			<td colspan="2"> <fmtDhrMulitplePayment:message key="LBL_MORE_FILTERS"/>
				<table class="NoBorder" cellspacing="0" cellpadding="0">
					<tr class=Shade>		
						<td class="alignright" width="140"><fmtDhrMulitplePayment:message key="LBL_PACKING_SLIP_NUMBER"/>: </td>
						<td class="RightText">
							<input type="text" class="RightText"  tabindex="3" name="Txt_PACKINGSLIP"   value="<%=strPSlip%>"  onFocus="changeBgColor(this,'#AACCE8');"onBlur="changeBgColor(this,'#ffffff');" size="20"/>
							<BR>
						</td>
						<td class="alignright" >&nbsp;&nbsp;<fmtDhrMulitplePayment:message key="LBL_PART_NUMBER"/>:</td>
						<td class="RightText">
							<input type="text" class="RightText"  tabindex="4" name="Txt_PARTID"   value="<%=strPartID%>"   onFocus="changeBgColor(this,'#AACCE8');"onBlur="changeBgColor(this,'#ffffff');" size="20"/>
							<BR>
						</td>
						<td class="alignright" > <fmtDhrMulitplePayment:message key="LBL_PO_NUMBER"/>:</td>
					 	<td class="RightText">
					 		<input type="text" class="RightText"  tabindex="5" name="Txt_PORDER"     value="<%=strPOID%>" onFocus="changeBgColor(this,'#AACCE8');"onBlur="changeBgColor(this,'#ffffff');" size="20"/> 
						</td>
					 	<td width="200">&nbsp;</td>						
					</tr>
				</table>
			</td>
		</tr>
<%	
		if (strAction.equals("Reload"))	{
%>	
		<tr><td colspan="2" class="Line"></td></tr>		
		<tr>
			<td colspan="2">
				<table class="NoBorder" cellspacing="0" cellpadding="0">
					<tr class=Shade>
						<td class="alignright" HEIGHT="24" Width ="135"><B>&nbsp;&nbsp;<fmtDhrMulitplePayment:message key="LBL_DATE_ENTERED"/>:</B></td>
						<td  class="RightText" HEIGHT="24" Width ="155"><B>&nbsp; <%=strCurDate%> </B></td> 
						<td colspan =2></td>
					</tr>
					<tr>
						<td nowrap class="alignright" HEIGHT="24">&nbsp;&nbsp;<fmtDhrMulitplePayment:message key="LBL_INOVICE_NUMBER"/>:</td>
						<td >&nbsp;<input type="text" size="20" value="" name="Txt_InvNum"  class="RightText"   onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="5">
						</td>
						<td  class="alignright" HEIGHT="24" Width ="100"><fmtDhrMulitplePayment:message key="LBL_PAYMENT_DATE"/>:</td>
						<td> &nbsp;<input type="text" size="20" value="<%=strCurDate%>" name="Txt_PDate"  class="RightText"   onFocus="changeBgColor(this,'#AACCE8');" 
								onBlur="changeBgColor(this,'#ffffff');" tabindex="6">
								<fmtDhrMulitplePayment:message key="LBL_CLICK_TO_OPEN_CALENDER" var="varClickToOpenCalender" />
								<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmAccount.Txt_PDate');" 
								title="${varClickToOpenCalender}"  src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" 
								height=18 width=19 />&nbsp;
						</td>
					</tr>	
					<tr class="Shade"">
							<td HEIGHT="24" class="alignright">&nbsp;&nbsp;<fmtDhrMulitplePayment:message key="LBL_PAYMENT_MODE"/>:</td>
							<td>&nbsp;<select name="Cbo_PayMode" class="RightText" tabindex="7" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
								<option value="0" >[Choose One]
<%
		   					intSize = alPayModes.size();
							hcboVal = new HashMap();
							strCodeID = "";
							strSelected  = "";
							String strPayMode = "5010";
							for (int i=0;i<intSize;i++)
							{
								hcboVal = (HashMap)alPayModes.get(i);
								strCodeID = (String)hcboVal.get("CODEID");
								strSelected = strPayMode.equals(strCodeID)?"selected":"";
%>
								<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
				  		}
%>
								</select>									
							
							</td>
							<td nowrap class="alignright" HEIGHT="24"><fmtDhrMulitplePayment:message key="LBL_CHEQUE_NUMBER"/>:</td>
							<td >&nbsp;<input type="text" size="20" value="" class="RightText" name="Txt_chqNum"  onFocus="changeBgColor(this,'#AACCE8');" 
								onBlur="changeBgColor(this,'#ffffff');" tabindex="8">&nbsp;
							</td>
					</tr>
					<tr>
						<td class="alignright"  align="right" HEIGHT="24">&nbsp;&nbsp;<fmtDhrMulitplePayment:message key="LBL_DETAILS"/>:</td>
						<td colspan = "3">&nbsp;<textarea name="Txt_Details" width = 0%; height =0px;
						  class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
						onBlur="changeBgColor(this,'#ffffff');" rows=2 cols=45 value="" tabindex="9"></textarea>
						</td>
					</tr>
					<tr class = Shade>
						<td nowrap class="alignright" HEIGHT="24">&nbsp;&nbsp;<fmtDhrMulitplePayment:message key="LBL_TOTAL_AMOUNT_TO_POST"/>:</td>
						<td class="RightTextBlue" colspan="5" id="IDCalculate">&nbsp;$0.00</td>
					</tr>	
				</table>
			</td>
		</tr>
		<tr>
			<fmtDhrMulitplePayment:message key="BTN_SUBMIT" var="varSubmit"/>
			<td colspan="2" class="aligncenter" height="30"><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" name="Btn_Submit" gmClass="button" onClick="fnPostPayment();" buttonType="Save" /></td>
   		</tr>		
		<tr><td colspan="2" class="Line"></td></tr>				
		<tr>
			<td colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td valign="top" class="aligncenter" height="30">
						</FORM> 
						
				<FORM name ="frmDtag" > 
						
						<display:table name="requestScope.resultSet.rows" export="false" class="its"  decorator="com.globus.displaytag.beans.DTDHRWrapper" defaultsort="2" varTotals="totals" >
						<display:column property="CNUM" title="" />
						<fmtDhrMulitplePayment:message key="LBL_PACK_SLIP" var="varPackSlip"/><display:column property="PSLIP" title="${varPackSlip}" sortable="true" />
						<fmtDhrMulitplePayment:message key="LBL_PART_NUMBER"var="varpartNumber"/>/><display:column property="PNUM" title="${varpartNumber}" class="aligncenter" sortable="true" />
						<fmtDhrMulitplePayment:message key="LBL_QTY_REC"var="varQtyRec"/>/><display:column property="QTYREC" title="${varQtyRec}" class="alignright" sortable="true" />
						<fmtDhrMulitplePayment:message key="LBL_QTY_ACC"var="varQtyAcc"/>/><display:column property="SHELFQTY" title="${varQtyAcc}" class="alignright" sortable="true" />
						<fmtDhrMulitplePayment:message key="LBL_COST_EA"var="varCostEa"/>/><display:column property="COST" title="${varCostEa}" class="alignright" sortable="true" format="$ {0,number,.00}"/>
						<fmtDhrMulitplePayment:message key="LBL_TOTAL_COST"var="varTotalCost"/>/><display:column property="TCOST" title="${varTotalCost}" class="alignright" sortable="true" total="true" format="$ {0,number,.00}"/>
						<fmtDhrMulitplePayment:message key="LBL_DATE"var="varDate"/>/><display:column property="CDATE" title="${varDate}" class="alignright" sortable="true" />
						<fmtDhrMulitplePayment:message key="LBL_PURCHASE_ORDER"var="varPurchaseOrder"/>/><display:column property="POID" title="${varPurchaseOrder}" class="aligncenter" sortable="true" />
						<fmtDhrMulitplePayment:message key="LBL_PO_TYPE"var="varPoType"/>/><display:column property="POTYPE" title="${varPoType}" />
						<fmtDhrMulitplePayment:message key="LBL_DHR_ID"var="varDhrId"/>/><display:column property="ID" title="${varDhrId}" class="aligncenter" sortable="true" />
						<fmtDhrMulitplePayment:message key="LBL_DESCRIPTION"var="varDescription"/>/><display:column property="PDESC" title="${varDescription}" class="smallText"/>
						<display:footer media="html"> 

		<% 
		String strVal ;
		strVal = ((HashMap)pageContext.getAttribute("totals")).get("column7").toString();
		String strTot = " $ " + GmCommonClass.getStringWithCommas(strVal);
		%>
		
		  	<tr class = "shade">
		  		<td colspan = "6"> <B> <fmtDhrMulitplePayment:message key="LBL_TOTAL_COST"/> : </B></td>
		  		<td colspan = "2" class = "alignleft" ><B><%=strTot%></B></td>
		  		<td colspan = "4"> </td>
		  	</tr>
		  	 </display:footer>					
						</display:table>
-					</FORM>	
						</td>
					</tr>
				</table>
			</td>
		</tr>
<%
	}
%>
	</table>

<%
	}catch(Exception e)
		{
			e.printStackTrace();
		}
%>

</BODY>

</HTML>
