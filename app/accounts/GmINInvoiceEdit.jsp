 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>

  <%@page import="java.util.Date"%>
<%
/**********************************************************************************
 * File		 		: GmINInvoiceEdit.jsp
 * Desc		 		: This screen is used for the Order Maintenance
 * Version	 		: 1.0
 * author			: Tamizhthangam Ramasamy
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%> 
<%@ taglib prefix="fmtINInvoiceEdit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmINInvoiceEdit.jsp -->
<fmtINInvoiceEdit:setLocale value="<%=strLocale%>"/>
<fmtINInvoiceEdit:setBundle basename="properties.labels.accounts.GmINInvoiceEdit"/>
<%
	String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strWikiTitleName = "";
	String strApplDateFmt = strGCompDateFmt;
    
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	String strTodaysDate = GmCommonClass.parseNull(GmCalenderOperations.getCurrentDate(strGCompDateFmt));
	
	String strAcctPath = GmCommonClass.getString("GMACCOUNTS");
	String strPOJSP = strAcctPath.concat("/GmCustomerPOEdit.jsp");
	
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strAddBatch = GmCommonClass.parseNull((String)request.getAttribute("hAddBatch"));
	String strBatchID = GmCommonClass.parseNull((String)request.getAttribute("hBatchID"));
	log.debug(" Action inside JSP is " + strAction);
	String strOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
	String strUpdFl = GmCommonClass.parseNull((String) request.getAttribute("UPDFL"));
	String strInvUpd = GmCommonClass.parseNull((String) request.getAttribute("INVUPDFL"));
	log.debug(" strInvUpd " + strInvUpd);
	String strCboAction = GmCommonClass.parseZero((String) request.getAttribute("hCboAction"));
	String strBatchSuccessMsg = GmCommonClass.parseNull((String)request.getAttribute("hSuccessMsg")); 
	String strMultipleInvMsg = GmCommonClass.parseNull((String)request.getAttribute("MULTI_INV_MESSAGE"));
	String strEmailReqFl = GmCommonClass.parseNull((String)request.getAttribute("hEmailReqFl")); 
	String strAccountCurrencyId = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_ID"));
	String strInvsource = GmCommonClass.parseNull((String)request.getAttribute("INVSOURCE")); 
	String strGSTStartDtFl = GmCommonClass.parseNull((String)request.getAttribute("strGSTStartFl"));
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
	String strPrintCountFl = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.PRINT_COUNT"));
	
	
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.accounts.GmINInvoiceEdit", strSessCompanyLocale);
	// to set the default choose action value
	
	if(strCboAction.equals("0") && strAddBatch.equals("Y")){
		if(strBatchID.equals("")){
			strCboAction = "18853"; //Create In Progress batch and Add PO
		}else{
			strCboAction = "18854"; // Add PO to In Progress Batch
		}
	}
	String strEmailChecked = "checked=checked";
 	if(strAction.equals("INV") && strEmailReqFl.equals("N")  ){
		strEmailChecked = "";
	} 
	String strDisable = "";
		if(!strInvUpd.equals("Y") && strAction.equals("PAY") ){
			strDisable = "true";
		}
		if(!strUpdFl.equals("Y") && strAction.equals("INV")){
			strDisable = "true";
		}
	if (strAction == null)
	{
		strAction = (String)session.getAttribute("hAction");
	}
	strAction = (strAction == null)?"Load":strAction;

	HashMap hmReturn = new HashMap();

	ArrayList alOrderNums = new ArrayList();
	ArrayList alInvoiceBatch = new ArrayList();
	HashMap hmCartDetails = new HashMap();
	HashMap hmConstructs = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmOrderDetails = new HashMap();
	HashMap hmShipDetails = new HashMap();

	String strShade = "";

	String strInvNum = "";
	String strAccId= "";
	String strDistRepNm = "";
	String strBillAdd = "";
	java.sql.Date dtInvDate = null;
	java.sql.Date dtDueDate = null;
	String strPO = "";
	String strOrdId = "";

	String strPartNum = "";
	String strDesc = "";
	String strPrice = "";
	String strQty = "";
	String strItemOrdId = "";
	String strPartNums = "";
	String strControlNum = "";
	String strPayNm = "";
	String strPay = "";
	String strDueDate = "";
	String strStatusFl = "";
	String strCallFlag = "";
	String strInvType = "";
	String strMode = "";
	String strControlName = "";
	String strItemType = "";
	String strElectEmailFl = "";
	String strElectVersion = "";
	String strComments ="";

	String strLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_GENERATE"));
	
	String strConFlag = "";
	String strConstructId = "";
	String strConstructNm = "";
	String strConstructValue = "";
	String strPriceLbl = "";
	String strQtyLbl = ""; 
	String strCommentName = "";
	String strValidAddressFl = "";
	String strTaxCountryFl = "";
	String strTaxDate = "";
	String strUnitPrice = "";
	String strAdjVal = "";
	String strAdjCode = "";
	String strGrandTotal = ""; // Total Before Adjustment
	String strAdjGrandTotal = ""; // Total After Adjustment
	String strAdjTotalwShipCst = ""; // For sub total
	String strBfrAdjTotalwShipCst = ""; // Total Before Adjustment with ship cost
	String strtotalFinal = ""; // For the grand total
	String strTCSCost ="";
	String strLblTotalAftAdj = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_TOTAL_AFTER_ADJ"));
	double dbGrandTotal = 0.0;
	double dbGrandAdjTotal = 0.0;
	double dbtotalFinal = 0.0;
	double dbTCSValue = 0.0;	
	// Setting dynamic wiki page
	strWikiTitleName = "INVOICE_GENERATE";
	String strAcccountCurrencySymb = "";
	String strOrderDate = "";
	String strTCS="";
	
	if (strAction.equals("PAY"))
	{
			strLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_UPDATE"));
			strWikiTitleName = "INVOICE_UPDATE";
	}
	else if (strAction.equals("IC"))
	{
		strLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ISSUE_CREDIT"));
		strQtyLbl = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CREDIT_QTY"));
		strPriceLbl = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CREDIT_PRICE"));
		strCommentName =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_COMMENTS_MEMO"));
		strWikiTitleName = "INVOICE_ISSUE_CREDIT";
	}else if (strAction.equals("EC"))
	{
		strLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_EDIT_COMMENTS"));
		strCommentName =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_COMMENTS_TO_INOVICE"));
		strWikiTitleName = "INVOICE_EDIT_COMMENTS";
    }else if(strAction.equals("IDB"))
    { 
		strLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ISSUE_DEBIT"));
		strQtyLbl = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_DEBIT_QTY"));
		strPriceLbl = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_DEBIT_PRICE_EA"));
		strCommentName = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_COMMENTS_MEMO"));
		strWikiTitleName = "INVOICE_ISSUE_DEBIT";
	} 
	if (hmReturn != null)
	{
		hmOrderDetails = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("ORDERDETAILS"));
		alOrderNums = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ORDERNUMS"));
		hmConstructs = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("CONSTRUCTDETAILS"));

		alInvoiceBatch = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("alInvoiceBatch")); 
		hmCartDetails = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("CARTDETAILS"));
		
		
		
		strInvNum = GmCommonClass.parseNull((String)hmOrderDetails.get("INV"));
		strAccId = GmCommonClass.parseNull((String)hmOrderDetails.get("ACCID"));
		strPO = GmCommonClass.parseNull((String)hmOrderDetails.get("PO"));
		strBillAdd = GmCommonClass.parseNull((String)hmOrderDetails.get("BILLADD"));
		strDistRepNm = GmCommonClass.parseNull((String)hmOrderDetails.get("REPDISTNM"));
		dtInvDate = (java.sql.Date)hmOrderDetails.get("INVDT");
		dtDueDate = (java.sql.Date)hmOrderDetails.get("DUEDT");
		strPayNm = GmCommonClass.parseNull((String)hmOrderDetails.get("PAYNM"));
		strPay = GmCommonClass.parseNull((String)hmOrderDetails.get("PAY"));
		strStatusFl = GmCommonClass.parseNull((String)hmOrderDetails.get("SFL"));
		strCallFlag = GmCommonClass.parseNull((String)hmOrderDetails.get("CALL_FLAG"));
		strInvType = GmCommonClass.parseNull((String)hmOrderDetails.get("INVTYPE"));
		strPayNm = strPayNm.equals("")?"Net 30":strPayNm;
		strElectEmailFl = GmCommonClass.parseNull((String)hmOrderDetails.get("EMAILREQ"));
		strElectVersion = GmCommonClass.parseNull((String)hmOrderDetails.get("EVERSION"));
		strComments = GmCommonClass.parseNull((String)hmOrderDetails.get("COMMENTS"));
		strValidAddressFl = GmCommonClass.parseNull((String)hmOrderDetails.get("VALID_ADD_FL"));
		strTaxCountryFl = GmCommonClass.parseNull((String)hmOrderDetails.get("TAX_COUNTRY_FL"));
		strTaxDate = GmCommonClass.parseNull((String) hmOrderDetails.get("TAX_START_DATE"));
		strAcccountCurrencySymb = GmCommonClass.parseNull((String) hmOrderDetails.get("CURRSIGN"));
		strOrderDate = GmCommonClass.getStringFromDate(dtInvDate, strApplDateFmt);

	}

	if (strOpt.equals("VIEW") || strOpt.equals("LOAD"))
	{
		strInvNum = "TO - BE";
	}
	int intSize = 0;
	ArrayList alLoop = null;
	HashMap hmTemp = null;
	HashMap hmLoop = null;
	ArrayList alConsLoop = null;
	HashMap hmConsLoop = null;
	// getting the wiki link title
	strWikiTitleName = GmCommonClass.getWikiTitle(strWikiTitleName);
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Invoice Edit </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script type="text/javascript" src="<%=strAccountJsPath%>/GmInvoiceEdit.js"></script>


<script language = "javascript">
var todaysDate = '<%=strTodaysDate%>';
var dateFmt = '<%=strApplDateFmt%>';
var validAddFl = '<%=strValidAddressFl%>';
var taxCountryFl = '<%=strTaxCountryFl%>';
var taxDate = '<%=strTaxDate%>';
var poJsp = '<%=strPOJSP %>';
var orderDate = '<%=strOrderDate%>';
var strPrintCount ='<%=strPrintCountFl%>';
var orderUsageLotFl = 'N';
var orderDDTFl = 'N';
//to capture the usage Lot data flag
var usageLotDataFl = 'N';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnActionChange();">
<FORM name="frmInvoice" method="post" action = "<%=strServletPath%>/GmInvoiceServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hPO" value="<%=strPO%>">
<input type="hidden" name="hInv" value="<%=strInvNum%>">
<input type="hidden" name="hAccId" value="<%=strAccId%>">
<input type="hidden" name="hPay" value="<%=strPay%>">
<input type="hidden" name="hStr" value="">
<input type="hidden" name="hCustomerPO" value="">
<input type="hidden" name="hReasonForChange" value="">
<input type="hidden" name="hInvType" value="">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="strOpt" value="">
<input type="hidden" name="hEmailFl" value="<%=strElectEmailFl%>">
<input type="hidden" name="hEmailVersion" value="<%=strElectVersion %>">
<input type="hidden" name="hAddBatch" value="<%=strAddBatch %>">
<input type="hidden" name="hBatchID" value="<%=strBatchID %>">
<input type="hidden" name="Cbo_Comp_Curr" value="<%=strAccountCurrencyId %>">
<input type="hidden" name="Cbo_InvSource" value="<%=strInvsource %>">
	<table border="0" class="DtTable1050" cellspacing="0" cellpadding="0">
	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<%
	//When there are multiple Invoices generated, we should show only the message and not the other details.
	if(strMultipleInvMsg.equals("")){ %>
					<tr>
					<fmtINInvoiceEdit:message key="LBL_INVOICE" var="varInvoice"/>
						<td height="25" class="RightDashBoardHeader"><gmjsp:label type="RegularText"  SFLblControlName="${varInvoice} - " td="false"/><%=strLabel%></td>
						<fmtINInvoiceEdit:message key="LBL_HELP" var="varHelp"/>
						<td height="25" class="RightDashBoardHeader"><img align="right"
						id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
						title='${varHelp}' width='16' height='16'
						onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitleName%>');" />
						</td>
					</tr>
					
					
		                    <%
								String strColPrice = "14";
		                    if (strAction.equals("IC") || strAction.equals("IDB"))
								{
									strColPrice = "8";
								}
                               %>
		<tr>
			<td width="100%" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr class="ShadeRightTableCaption">
									<td height="25" align="center" width="450">&nbsp;<fmtINInvoiceEdit:message key="LBL_BILL_TO" var="varBillTo"/><gmjsp:label type="RegularText"  SFLblControlName="${varBillTo}" td="false"/></td>
									<td bgcolor="#666666" width="1" rowspan="7"></td>
									<td height="25" align="center">&nbsp;<fmtINInvoiceEdit:message key="LBL_CUSOTMER_PO" var="varCustomerPO"/><gmjsp:label type="RegularText"  SFLblControlName="${varCustomerPO} #" td="false"/></td>
									<td bgcolor="#666666" width="1" rowspan="7"></td>
									<td width="150" align="center">&nbsp;<fmtINInvoiceEdit:message key="LBL_INVOICE_DATE" var="varInvoiceDate"/><gmjsp:label type="RegularText"  SFLblControlName="${varInvoiceDate}" td="false"/></td>
									<td width="150" align="center">&nbsp;<fmtINInvoiceEdit:message key="LBL_TERMS" var="varTerms"/><gmjsp:label type="RegularText"  SFLblControlName="${varTerms}" td="false"/></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="6"></td>
								</tr>
								<tr>
									<td class="RightText" rowspan="5" valign="top">&nbsp;<%=strBillAdd%></td>
									<td class="RightText" align="center">&nbsp;<%=strPO%></td>
									<td height="25" class="RightText" align="center">
<%
								if (strAction.equals("INV"))
								{
%>
									
										&nbsp;<gmjsp:calendar textControlName="Txt_InvDate" textValue="<%=dtInvDate%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
<%
								}else{
%>
										<%=GmCommonClass.getStringFromDate(dtInvDate,strApplDateFmt)%>
<%
										}
%>
									</td>
									<td class="RightText" align="center">&nbsp;<%=strPayNm%></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
								</tr>
								<tr class="ShadeRightTableCaption">
									<td class="RightText" align="center" height="20"><fmtINInvoiceEdit:message key="LBL_INVOICE" var="varInovice"/><gmjsp:label type="RegularText"  SFLblControlName="${varInovice} #" td="false"/></td>

									<td align="center"><fmtINInvoiceEdit:message key="LBL_DIT_REP" var="varDistRep"/><gmjsp:label type="RegularText"  SFLblControlName="${varDistRep}" td="false"/></td>
									<td align="center"><fmtINInvoiceEdit:message key="LBL_ACCOUNT_ID" var="varAccountId"/><gmjsp:label type="RegularText"  SFLblControlName="${varAccountId}" td="false"/></td>
								
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
								</tr>
								<tr>
									<td class="RightText" rowspan="3" align="center">&nbsp;<%=strInvNum%></td>
									<td align="center" class="RightText">&nbsp;<%=strDistRepNm%><Br></td>
									<td align="center" height="25" class="RightText">&nbsp;<%=strAccId %></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" height="1" bgcolor="#666666"></td>
					</tr>
					<tr>
						<td  align="center" colspan="2" valign="top">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr class="ShadeRightTableCaption">
									<td width="120" height="25">&nbsp;<fmtINInvoiceEdit:message key="LBL_ORDER" var="varOrder"/><gmjsp:label type="RegularText"  SFLblControlName="${varOrder} #" td="false"/></td>
									<td height="25" align="center" >&nbsp;<fmtINInvoiceEdit:message key="LBL_ORDER_DATE" var="varOrderDate"/><gmjsp:label type="RegularText"  SFLblControlName="${varOrderDate}" td="false"/></td>
									<td height="25" align="center" >&nbsp;<fmtINInvoiceEdit:message key="LBL_SURGERY_DATE" var="varSurgDate"/><gmjsp:label type="RegularText"  SFLblControlName="${varSurgDate}" td="false"/></td>
									<td width="70" align="left" height="25">&nbsp;<fmtINInvoiceEdit:message key="LBL_PART" var="varPart"/><gmjsp:label type="RegularText"  SFLblControlName="${varPart} #" td="false"/></td>
									<td width="200" align="left">&nbsp;<fmtINInvoiceEdit:message key="LBL_PART_DESCRIPTION" var="varPartDescription"/><gmjsp:label type="RegularText"  SFLblControlName="${varPartDescription}" td="false"/></td>
									<td align="center" width="70"><fmtINInvoiceEdit:message key="LBL_CONTROL_NUMBER" var="varControlNumber"/><gmjsp:label type="RegularText"  SFLblControlName="${varControlNumber}" td="false"/></td>
									<td align="center" width="80"><fmtINInvoiceEdit:message key="LBL_ORDER_QTY" var="varOrerQty"/><gmjsp:label type="RegularText"  SFLblControlName="${varOrerQty}" td="false"/></td>
									<td align="center" width="80"><fmtINInvoiceEdit:message key="LBL_UNIT_PRICE" var="varUnitPrice"/><gmjsp:label type="RegularText"  SFLblControlName="${varUnitPrice}" td="false"/></td>
									<td align="center" width="80"><fmtINInvoiceEdit:message key="LBL_UNIT_PRICE_ADJ" var="varUnitPriceAdj"/><gmjsp:label type="RegularText"  SFLblControlName="${varUnitPriceAdj}" td="false"/></td>
									<td align="left" width="80"><fmtINInvoiceEdit:message key="LBL_ADJ_CODE" var="varAdjCode"/><gmjsp:label type="RegularText"  SFLblControlName="${varAdjCode}" td="false"/></td>
									<td align="center" width="80"><fmtINInvoiceEdit:message key="LBL_NET_UNIT_PRICE" var="varUnitPrice"/><gmjsp:label type="RegularText"  SFLblControlName="${varUnitPrice}" td="false"/></td>
<%
								if (strAction.equals("PRINT")|| strAction.equals("PAY"))
								{
									if(!strGSTStartDtFl.equals("Y")){
%>
									<td align="center" width="80"><fmtINInvoiceEdit:message key="LBL_SALES_TAX_RATE" var="varSalesTaxRate"/><gmjsp:label type="RegularText"  SFLblControlName="${varSalesTaxRate}" td="false"/></td>
									<td align="center" width="80"><fmtINInvoiceEdit:message key="LBL_SALES_TAX_VALUE" var="varSalesTaxValue"/><gmjsp:label type="RegularText"  SFLblControlName="${varSalesTaxValue}" td="false"/></td>
									<%}else{ %>
									     <td align="center" width="80"><fmtINInvoiceEdit:message key="LBL_SALES_IGST_RATE" var="varSalesIGSTRate"/>
										 <gmjsp:label type="RegularText"  SFLblControlName="${varSalesIGSTRate}" td="false"/></td>
										<td align="center" width="80"><fmtINInvoiceEdit:message key="LBL_SALES_IGST_VALUE" var="varSalesIGSTValue"/>
										<gmjsp:label type="RegularText"  SFLblControlName="${varSalesIGSTValue}" td="false"/></td>
										<td align="center" width="80"><fmtINInvoiceEdit:message key="LBL_SALES_CGST_RATE" var="varSalesCGSTRate"/>
										<gmjsp:label type="RegularText"  SFLblControlName="${varSalesCGSTRate}" td="false"/></td>
										<td align="center" width="80"><fmtINInvoiceEdit:message key="LBL_SALES_CGST_VALUE" var="varSalesCGSTValue"/>
										<gmjsp:label type="RegularText"  SFLblControlName="${varSalesCGSTValue}" td="false"/></td>
										<td align="center" width="80"><fmtINInvoiceEdit:message key="LBL_SALES_SGST_RATE" var="varSalesSGSTRate"/>
										<gmjsp:label type="RegularText"  SFLblControlName="${varSalesSGSTRate}" td="false"/></td>
										<td align="center" width="80"><fmtINInvoiceEdit:message key="LBL_SALES_SGST_VALUE" var="varSalesSGSTValue"/>
										<gmjsp:label type="RegularText"  SFLblControlName="${varSalesSGSTValue}" td="false"/></td>
										<td align="center" width="80"><fmtINInvoiceEdit:message key="LBL_SALES_TCS_RATE" var="varSalesTCSRate"/>
										<gmjsp:label type="RegularText"  SFLblControlName="${varSalesTCSRate}" td="false"/></td>
									<%} %>
									
								<%} %>
									 <fmtINInvoiceEdit:message key="LBL_TOTAL_PRICE" var="varTotalPrice"/>
									<td colspan="<%=strColPrice %>" align="center" width="100"><gmjsp:label type="RegularText"  SFLblControlName="${varTotalPrice}" td="false"/></td>
									
<%
							if (strAction.equals("IC") || strAction.equals("IDB"))
							{
%>
     
									
									<td colspan="4" align="center" width="80"><gmjsp:label type="RegularText"  SFLblControlName="<%=strQtyLbl%>" td="false"/></td>
									<td colspan="4" align="center" width="100"><gmjsp:label type="RegularText"  SFLblControlName="<%=strPriceLbl%>" td="false"/></td>
<%
							}
%>
								</tr>
								<tr>
									<td colspan="26" height="1" bgcolor="#666666"></td>
								</tr>
<%
					int intLoop = 0;
			  		intSize = alOrderNums.size();
					alLoop = new ArrayList();
					String strItems = "";
					java.sql.Date dtOrdDate=null;
					java.sql.Date dtSurgDate=null;
					double intQty = 0.0;
					double dbItemTotal = 0.0;
					double dbItemUnitPriceTotal = 0.0;
					String strItemTotal = "";
					double dbTotal = 0.0;
					String strTotal = "";
					String strShipCost = "";
					String strVat="";
					String strIGST = "";
					String strCGST = "";
					String strSGST = "";
					String strVatAmount="";
					String strIGSTAmt = "";
					String strCGSTAmt = "";
					String strSGSTAmt = "";
				   
					double dbVat=0.0;
					double dbIGST=0.0;
					double dbCGST=0.0;
					double dbSGST=0.0;
					double dbTCS=0.0;
					double dbVatAmount=0.0;
					double dbIGSTAmount=0.0;
					double dbCGSTAmount=0.0;
					double dbSGSTAmount=0.0;
					double dbVatSubTotal=0.0;
					double dbIGSTSubTotal=0.0;
					double dbCGSTSubTotal=0.0;
					double dbSGSTSubTotal=0.0;
					
					double dblShip = 0.0;
					double dbAdjTotalwShipCst = 0.0;
					double dbTotalShipCst = 0.0;
					double dbBfrAdjTotalwShipCst = 0.0;
					
					//double dbGrandTotal = 0.0;
					String strTaxCost = "";
					String strIGSTCost = "";
					String strCGSTCost = "";
					String strSGSTCost = "";
					//String strGrandTotal = "";
                   
					int intTotalLoop = 0;
					int k = 0;
					int intConLoop = 0;

			  		for (int i=0;i<intSize;i++)
			  		{
				  		hmTemp = (HashMap)alOrderNums.get(i);
						strOrdId = (String)hmTemp.get("ID");

						alLoop = (ArrayList)hmCartDetails.get(strOrdId);
						intLoop = alLoop.size();
						intTotalLoop = intTotalLoop + intLoop;
						
						alConsLoop = (ArrayList)hmConstructs.get(strOrdId);
						intConLoop = alConsLoop.size();

						if (intConLoop > 0)
						{
							for ( int j=0;j<intConLoop;j++)
							{
								hmConsLoop = (HashMap)alConsLoop.get(j);
								strConstructId = (String)hmConsLoop.get("CONID");
								strConstructNm = (String)hmConsLoop.get("CONNM");
								strConstructValue = (String)hmConsLoop.get("CVALUE");
								dbItemTotal = Double.parseDouble(strConstructValue);
								dbTotal = dbTotal + dbItemTotal;
%>
								<tr>
									<td class="RightText" height="20">&nbsp;<%=strOrdId%></td>
									<td class="RightText">&nbsp;</td>
									<td class="RightText" align="center">&nbsp;<%=strConstructId%></td>
									<td class="RightText">&nbsp;<%=strConstructNm%></td>
									<td colspan="3">&nbsp;</td>
									<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strConstructValue)%>&nbsp;&nbsp;</td>
								</tr>
<%
							}
						}
												
						for ( int j=0;j<intLoop;j++)
						{

							hmLoop = (HashMap)alLoop.get(j);
							dtOrdDate = (java.sql.Date)hmLoop.get("ODT");
							dtSurgDate = (java.sql.Date)hmLoop.get("SURG_DT");
							strPartNum = (String)hmLoop.get("ID");
							strDesc = (String)hmLoop.get("PDESC");
							strPrice = (String)hmLoop.get("PRICE"); //Net unit price after adjustments
							strQty = (String)hmLoop.get("QTY");
							strControlNum = (String)hmLoop.get("CNUM");
							strItemType = GmCommonClass.parseNull((String)hmLoop.get("TYPE"));
							strConFlag = GmCommonClass.parseNull((String)hmLoop.get("CONFL"));
							strVat=GmCommonClass.parseNull((String)hmLoop.get("VAT"));
							strIGST = GmCommonClass.parseNull((String)hmLoop.get("IGST"));
							strCGST = GmCommonClass.parseNull((String)hmLoop.get("CGST"));
							strSGST = GmCommonClass.parseNull((String)hmLoop.get("SGST"));
							strTCS = GmCommonClass.parseNull((String) hmLoop.get("ACCOUNTTCS"));
							
							strTaxCost = GmCommonClass.parseZero((String)hmLoop.get("TAX_COST"));
							
							strUnitPrice = GmCommonClass.parseZero((String)hmLoop.get("UNITPRICE")); // Unit Price
							strAdjVal = GmCommonClass.parseZero((String)hmLoop.get("ADJVAL"));
							strAdjCode = GmCommonClass.parseNull((String)hmLoop.get("ADJCODE"));
							strItemOrdId = GmCommonClass.parseNull((String)hmLoop.get("ITEM_ORD_ID"));
/*							if (strConFlag.equals(""))
							{*/
								intQty = Double.parseDouble(strQty);
								dbItemTotal = Double.parseDouble(strPrice);
								
								//PC-4888 : To set the hidden field (h_NetUnitPrice) so that we can validate the unit price vs issue amount
								
								dbItemUnitPriceTotal = Double.parseDouble(strUnitPrice);
								if(!strVat.equals("")){
									dbVat=Double.parseDouble(strVat);
								}
								if(!strIGST.equals("")){
								  dbIGST = (!strIGST.equals(""))?Double.parseDouble(strIGST):dbIGST;
									
								}
								if(!strCGST.equals("")){
								  dbSGST = (!strSGST.equals(""))?Double.parseDouble(strSGST):dbSGST;
								}
								if(!strSGST.equals("")){
								  dbCGST = (!strCGST.equals(""))?Double.parseDouble(strCGST):dbCGST;
								}
								if(!strTCS.equals("")){
								   dbTCS = (!strTCS.equals(""))?Double.parseDouble(strTCS):dbTCS;
								}
								
								
								
								dbItemTotal = GmCommonClass.roundDigit((intQty * dbItemTotal), 2); // Multiply by Qty
								dbItemUnitPriceTotal = GmCommonClass.roundDigit((intQty * dbItemUnitPriceTotal), 2); // Multiply by Qty
								if(strAction.equals("PRINT") || strAction.equals("PAY"))
								{
								if(!strVat.equals("")){
									dbVatAmount= Double.parseDouble(strTaxCost);
								}
								if(!strIGST.equals("")){
									dbIGSTAmount=(dbItemTotal*dbIGST)/100;
								}
								if(!strCGST.equals("")){
									dbCGSTAmount=(dbItemTotal*dbCGST)/100;
								}
								if(!strSGST.equals("")){
									dbSGSTAmount=(dbItemTotal*dbSGST)/100;
								}
								strVatAmount=""+dbVatAmount;
								strVatAmount=""+dbVatAmount;
								strIGSTAmt = ""+dbIGSTAmount;
								strCGSTAmt = ""+dbCGSTAmount;
								strSGSTAmt = ""+dbSGSTAmount;
								//dbItemTotal+=dbVatAmount+dbIGSTAmount+dbCGSTAmount+dbSGSTAmount;
								dbVatSubTotal+=dbVatAmount;
								dbIGSTSubTotal+=dbIGSTAmount;
								dbCGSTSubTotal+=dbCGSTAmount;
								dbSGSTSubTotal+=dbSGSTAmount;
								}
								
								strItemTotal = ""+dbItemTotal;
								dbTotal = dbTotal + dbItemTotal;
								dbGrandTotal = dbGrandTotal + dbItemUnitPriceTotal; // Total of the Unit Price(before adjustments)
								dbGrandAdjTotal = dbGrandAdjTotal + dbItemTotal;// Total of the Net Unit Price(after adjustments)
								strTotal = ""+dbTotal;
				/*			}
							else
							{
								strPrice = "CON";
								strItemTotal = "0.0"; 
							}*/
							strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
%>
								<tr >
<%
							if ( j == 0)
							{	/* To print the order in for the list link */
%>
									<td class="RightText" rowspan="<%=intLoop%>" height="20">
<%						// Check if the Orded is a Return or Duplicate Order -- redirect based on the type 						
						if ((strOrdId.charAt(strOrdId.length()-1)== 'R') ||(strOrdId.charAt(strOrdId.length()-1)== 'D') )
						{%>
						&nbsp;<a href="javascript:fnPrintCreditMemo('<%=strOrdId%>');"
<%						}else if (strOrdId.charAt(strOrdId.length()-1)== 'A') 
						{%>
						&nbsp;<a href="javascript:fnPrintCashAdjustMemo('<%=strOrdId%>');"
<%						}else 
						{%>
						&nbsp;<a href="javascript:fnPrintPack('<%=strOrdId%>');"
<%						}%>
						class="RightText"><%=strOrdId%></a> </td>
									<td class="RightText" rowspan="<%=intLoop%>" height="20">&nbsp;<%=GmCommonClass.getStringFromDate(dtOrdDate,strApplDateFmt)%></td>
									<td class="RightText" rowspan="<%=intLoop%>" height="20">&nbsp;<%=GmCommonClass.getStringFromDate(dtSurgDate,strApplDateFmt)%></td>
<%							
							}
%>
									<input type="hidden" name="hItemOrdId<%=k%>" value="<%=strItemOrdId%>">
									<td class="RightText" align="left" height="20">&nbsp;<%=strPartNum%></td>
									<td class="RightText" align="left" width="150" >&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
									<td class="RightText" height="20">&nbsp;<%=strControlNum%></td>
									<td class="RightText" align="center">&nbsp;<%=GmCommonClass.getRedText(strQty)%><input type="hidden" name="h_InvQty<%=k%>" value="<%=strQty%>"></td>
									<td class="RightText" align="center"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strUnitPrice))%> <input type="hidden" name="h_unitprice<%=k%>" value="<%=GmCommonClass.parseZero(strUnitPrice)%>"></td>
									<td class="RightText" align="center"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strAdjVal))%></td>
									<td class="RightText" align="center">&nbsp;<%=strAdjCode%></td>
									<td class="RightText" align="center"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strPrice))%></td>
									
<%
								if (strAction.equals("PRINT") || strAction.equals("PAY"))
								{
									if(!strGSTStartDtFl.equals("Y")){
%>
									<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strVat))%></td>
									<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strVatAmount))%></td>
									
									<%}else{%>
									<td class="RightText" align="right">&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strIGST))%></td>
									<td class="RightText" align="right">&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strIGSTAmt))%></td>
									<td class="RightText" align="right">&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strCGST))%></td>
									<td class="RightText" align="right">&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strCGSTAmt))%></td>
									<td class="RightText" align="right">&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strSGST))%></td>
									<td class="RightText" align="right">&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strSGSTAmt))%></td>
									<td class="RightText" align="right">&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strTCS))%></td>
									
									<%} %>
									
								<%} %>	
						         <td colspan="<%=strColPrice %>" class="RightText" align="Right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strItemTotal))%></td>
									<input type="hidden" name="hTotalPrice<%=k%>" value="<%=strItemTotal%>">
									<input type="hidden" name="h_NetUnitPrice<%=k%>" value="<%=GmCommonClass.parseZero(strPrice)%>">
								
						
<%		/*IC Means Invoice Credit Servlet*/
							if (strAction.equals("IC") || strAction.equals("IDB"))
							{%>
									<td class="RightText" align="center" colspan="4">
										<input type="hidden" name="hOrderID<%=k%>" value="<%=strOrdId%>">
										<input type="hidden" name="hPNum<%=k%>" value="<%=strPartNum%>">
										<input type="hidden" name="hItemType<%=k%>" value="<%=strItemType%>">
										&nbsp;<input type="text" size="3" value="" name="Txt_CreditQty<%=k%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1></td>
									<td class="RightText" align="center">&nbsp;<input type="text" size="7" value="" name="Txt_CreditAmt<%=k%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1></td>
<%
							k++;							
							}
%>
								</tr>
<%					
							if ( j == intLoop -1)
							{	
								strShipCost = GmCommonClass.parseZero((String)hmLoop.get("SCOST"));
								dblShip = Double.parseDouble(strShipCost);
								String strCountryInvoice = GmCommonClass.parseNull(GmCommonClass.getRuleValue("1","INVOICEFMT"));
								dbTotal = dbTotal + dblShip;
								dbTotalShipCst = dbTotalShipCst + dblShip;
								strTotal = ""+dbTotal;
%>
							<tr>
								<td colspan="3">&nbsp;</td>
								<td class="RightText" height="24px">&nbsp;<fmtINInvoiceEdit:message key="LBL_SHIPPING_CHARGES" var="varShippingCharges"/><gmjsp:label type="RegularText"  SFLblControlName="${varShippingCharges}" td="false"/></td>
								<%
								String strCol = "5";
								if (strAction.equals("PRINT") || strAction.equals("PAY"))
								{
								  strCol = "11";
								}
%>
								<td colspan="<%=strCol%>">&nbsp;<td>
								<td class="RightText" align="right" colspan="3"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strShipCost))%></td>
<%
							if (strAction.equals("IC")|| strAction.equals("IDB"))
							{
%>
									<td class="RightText" align="center" colspan="2">&nbsp;</td>
<%
							}
%>
							</tr>
							<tr><td colspan="26" height="1" bgcolor="#cccccc"></td></tr>
<%
							}
						}
			  		}
  dbAdjTotalwShipCst = dbGrandAdjTotal+dbTotalShipCst; // Getting the total after adjustment price 
  dbBfrAdjTotalwShipCst = dbGrandTotal+dbTotalShipCst;
  strAdjTotalwShipCst = "" + dbAdjTotalwShipCst;
  strBfrAdjTotalwShipCst = "" + dbBfrAdjTotalwShipCst;
  //dbGrandTotal = dbGrandTotal + dbVatSubTotal;
  //dbGrandAdjTotal = dbGrandAdjTotal + dbVatSubTotal;
  strGrandTotal = "" + dbGrandTotal;
  strAdjGrandTotal = "" + dbGrandAdjTotal;
  strTaxCost = "" + dbVatSubTotal;
  strIGSTCost = ""+dbIGSTSubTotal;
  strCGSTCost = ""+dbCGSTSubTotal;
  strSGSTCost = ""+dbSGSTSubTotal;
  if(!strGSTStartDtFl.equals("Y")){
  dbtotalFinal = dbtotalFinal + dbTotal + dbVatSubTotal;
  strtotalFinal = "" + dbtotalFinal;
  }else{
	  dbtotalFinal = dbtotalFinal + dbTotal + dbIGSTSubTotal + dbCGSTSubTotal + dbSGSTSubTotal;
	  dbTCSValue=(dbtotalFinal*dbTCS)/100;
	  dbtotalFinal=dbtotalFinal+dbTCSValue;
	  strTCSCost=""+dbTCSValue;
	  strtotalFinal = "" + dbtotalFinal;
  }
  
  //if (!strAction.equals("PRINT") && (!strAction.equals("PAY"))) {// Total values including the shipping cost
    strGrandTotal = strBfrAdjTotalwShipCst;
    strAdjGrandTotal = strAdjTotalwShipCst;
  //}
  if(!strInvUpd.equals("Y") && strAction.equals("PAY")){
    strLblTotalAftAdj = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SUBTOTAL"));
  }
  
%>

			<%if(!strAction.equals("PAY") || (strInvUpd.equals("Y") && strAction.equals("PAY"))){ %>
									<tr>
									<td colspan="13" height="24">&nbsp;</td>
				<%if (strAction.equals("PRINT") || strAction.equals("PAY"))
								{%>
							      <td class="RightTableCaption" align="center">&nbsp;</td>
									<td class="RightTableCaption" align="center">&nbsp;</td>
									<%} 
									%>
									<td class="RightTableCaption" align="right" colspan="10"><fmtINInvoiceEdit:message key="LBL_TOTAL_BEFORE_ADJ" var="varTotalBeforAdj"/><gmjsp:label type="RegularText"  SFLblControlName="${varTotalBeforAdj}:" td="false"/></td>
									<gmjsp:currency type="CurrTextSign"  textValue="<%=strGrandTotal%>" currSymbol="<%=strAcccountCurrencySymb %>" />
									
								</tr>
								<%} %>
								<tr>
									<td colspan="11" height="24">&nbsp;</td>
			<%if (strAction.equals("PRINT") || (strAction.equals("PAY")))
								{%>
									<td class="RightTableCaption" align="center">&nbsp;</td>
									<td class="RightTableCaption" align="center">&nbsp;</td> 
									<%} %>
									<td class="RightTableCaption" align="right" colspan="12"><gmjsp:label type="RegularText"  SFLblControlName="<%=strLblTotalAftAdj%>" td="false"/></td>
									
							         <gmjsp:currency type="CurrTextSign"  textValue="<%=strAdjGrandTotal%>" currSymbol="<%=strAcccountCurrencySymb %>"/>
									
									
									
								</tr>
								
                  
								
								<%if (strAction.equals("PRINT") || (strAction.equals("PAY")))
								{%>
								<%if(!strGSTStartDtFl.equals("Y")){ %>
								<tr>
									<td colspan="13" height="24">&nbsp;</td>
									<td class="RightTableCaption" align="right" colspan="12"><fmtINInvoiceEdit:message key="LBL_SALES_TAX_VAT_TOTAL" var="varSalesTAxVatTotal"/><gmjsp:label type="RegularText"  SFLblControlName="${varSalesTAxVatTotal}:" td="false"/></td>
									
									<gmjsp:currency type="CurrTextSign"  textValue="<%=strTaxCost%>" currSymbol="<%=strAcccountCurrencySymb %>"/>
									<%}else{%>
									<td colspan="15" height="24">&nbsp;</td>
									<td class="RightTableCaption" align="right" colspan="10"><fmtINInvoiceEdit:message key="LBL_IGST_TOTAL" var="varIGSTTotal"/><gmjsp:label type="RegularText"  SFLblControlName="${varIGSTTotal}:" td="false"/></td>
									
									<gmjsp:currency type="CurrTextSign"  textValue="<%=strIGSTCost%>" currSymbol="<%=strAcccountCurrencySymb %>"/>
								</tr>
								<tr>
								   	<td colspan="15" height="24">&nbsp;</td>
									<td class="RightTableCaption" align="right" colspan="10"><fmtINInvoiceEdit:message key="LBL_CGST_TOTAL" var="varCGSTTotal"/><gmjsp:label type="RegularText"  SFLblControlName="${varCGSTTotal}:" td="false"/></td>
									
									<gmjsp:currency type="CurrTextSign"  textValue="<%=strCGSTCost%>" currSymbol="<%=strAcccountCurrencySymb %>"/>
								</tr>
									<td colspan="15" height="24">&nbsp;</td>
									<td class="RightTableCaption" align="right" colspan="10"><fmtINInvoiceEdit:message key="LBL_SGST_TOTAL" var="varSGSTTotal"/><gmjsp:label type="RegularText"  SFLblControlName="${varSGSTTotal}:" td="false"/></td>
									
									<gmjsp:currency type="CurrTextSign"  textValue="<%=strSGSTCost%>" currSymbol="<%=strAcccountCurrencySymb %>"/>
			
									
								<tr>	
									<td colspan="15" height="24">&nbsp;</td>
									<td class="RightTableCaption" align="right" colspan="10"><fmtINInvoiceEdit:message key="LBL_TCS_TOTAL" var="varTCSTotal"/><gmjsp:label type="RegularText"  SFLblControlName="${varTCSTotal}:" td="false"/></td>
									
									<gmjsp:currency type="CurrTextSign"  textValue="<%=strTCSCost%>" currSymbol="<%=strAcccountCurrencySymb %>"/>
						        </tr>
								<%} %>
								<tr>
									<td colspan="16" height="24">&nbsp;</td>
									<td class="RightTableCaption" align="right" colspan="9"><fmtINInvoiceEdit:message key="LBL_TOTAL" var="varTotal"/><gmjsp:label type="RegularText"  SFLblControlName="${varTotal}:" td="false"/></td>
									
									<gmjsp:currency type="CurrTextSign"  textValue="<%=strtotalFinal%>" currSymbol="<%=strAcccountCurrencySymb %>"/>
									
								</tr>
								<%} %>
								
								<tr>
									<td colspan="26" height="1" bgcolor="#666666"><input type="hidden" name="hTotalCnt" value="<%=""+intTotalLoop%>"></td>
								</tr>
<%
					if (strAction.equals("IC") || strAction.equals("IDB") || strAction.equals("EC"))
					{
						if(!strAction.equals("EC"))
						{
							strComments ="";
						}
						
%>						<tr>
							<td class="RightText" align="right" colspan="3" valign="top"><gmjsp:label type="RegularText"  SFLblControlName='<%=strCommentName%>' td="false"/>&nbsp; </td>&nbsp;
							<td class="RightText" colspan="10" valign="top"><textarea name="Txt_Details" class="InputArea" 
								onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" rows=5 cols=70 value="" 
								tabindex=24><%=strComments %></textarea></td>
						</tr>
						<tr><td colspan="24" height="1" bgcolor="#cccccc"></td></tr>
						
<%					}
%>					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<%
		if (strAction.equals("PAY"))
		{
				ArrayList alPayModes = (ArrayList)hmReturn.get("PAYMODEDETAILS");
				ArrayList alPayDetails = (ArrayList)hmReturn.get("PAYDETAILS");
				strLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_UPDATE"));
				int intPaySize = alPayDetails.size();
%>
		<tr>
			<td colspan="2">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td class="RightText" HEIGHT="24" colspan="2">
						
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr class="ShadeRightTableCaption">
									<td  HEIGHT="24" colspan="4"><fmtINInvoiceEdit:message key="LBL_ENTER_PAYMENT_DETAILS" var="varPaymentDetails"/><gmjsp:label type="RegularText"  SFLblControlName="${varPaymentDetails}:" td="false"/></td>
								</tr>
								<tr><td colspan="4" class=line></td></tr>
								<tr bgcolor="#eeeeee">
									<td align="center" class="RightTableCaption" HEIGHT="20" width="80"> <fmtINInvoiceEdit:message key="LBL_MODE" var="varMode"/><gmjsp:label type="MandatoryText"  SFLblControlName="${varMode}" td="false"/></td>
									<td align="center" class="RightTableCaption" width="200"> <fmtINInvoiceEdit:message key="LBL_DESC" var="varDesc"/><gmjsp:label type="MandatoryText"  SFLblControlName="${varDesc}" td="false"/></td>
									<td align="center" class="RightTableCaption" width="80">  <fmtINInvoiceEdit:message key="LBL_AMOUNT" var="varAmount"/><gmjsp:label type="MandatoryText"  SFLblControlName="${varAmount}" td="false"/></td>
									<td class="RightTableCaption" width="100"> <fmtINInvoiceEdit:message key="LBL_DATE" var="varDate"/><gmjsp:label type="MandatoryText"  SFLblControlName="${varDate}" td="false"/></td>
								</tr>
								<tr><td colspan="4" class=line></td></tr>
								
								<% for (int intRowCnt = 0; intRowCnt<=5; intRowCnt++) 
								{ 	strControlName = "Cbo_Purpose" + intRowCnt;
								%>
								<tr>
									<td height="24" align="right" class="RightText"  width="100">
									<gmjsp:dropdown controlName="<%= strControlName %>" 	   seletedValue="<%= strMode %>" 	
									tabIndex="1"  width="100" value="<%= alPayModes%>" codeId = "CODEID" codeName = "CODENM" defaultValue= "[Choose One]"  />
									</td>
									<td align="center" class="RightText"  width="200">
									<input type="text" size="50" value="" name="Txt_Desc<%=intRowCnt%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
									</td>
									<td align="center" class="RightText"  width="50">
									<input type="text" size="20" value="" name="Txt_Amt<%=intRowCnt%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
									</td>
									<td class="RightText"  width="120">
									<fmtINInvoiceEdit:message key="LBL_CLICK_TO_OPEN_CALENDER" var="varClickToOpenCalender"/>
									<input type="text" size="10" value="" name="Txt_Date<%=intRowCnt%>" id="Txt_Date<%=intRowCnt%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
									<img id="Img_Date" style="cursor:hand" onclick="javascript:showSingleCalendar('divTxt_Date<%=intRowCnt%>','Txt_Date<%=intRowCnt%>');" title="${varClickToOpenCalender}"  src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />
									<div id="divTxt_Date<%=intRowCnt%>" style="position: absolute; z-index: 10;"></div>
									</td>
								</tr>
								<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
								<% } %>
								<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
							</table>
						</td>
					</tr>

			<tr>
				<td class="RightText" HEIGHT="24" colspan="5">
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr class="ShadeRightTableCaption">
									<td  HEIGHT="24" colspan="6"><fmtINInvoiceEdit:message key="LBL_PAYMENT_HISTORY" var="varPaymentHistory"/><gmjsp:label type="RegularText"  SFLblControlName="${varPaymentHistory}:" td="false"/></td>
									<fmtINInvoiceEdit:message key="LBL_CLICK_TO_ENTER_LOG" var="varClickToEnterLog"/>
									<!-- Adding code for Call Log Starts -->
									<td align ="right">
									<img id="imgEdit" style="cursor:hand"  
									<% if (strCallFlag.equals("N"))
	 								{ %>
		 							src="<%=strImagePath%>/phone_icon.jpg" 
		 							<%} else {%>	
		 							src="<%=strImagePath%>/phone-icon_ans.gif" 
			 						<% }%>
			 						title="${varClickToEnterLog}" 
			 						width="22" height="20" 
			 						onClick="javascript:fnOpenLog('<%=strInvNum%>' )"/> 						
				 					&nbsp;</td>
									<!-- Adding code for Call Log Ends -->									
				
								</tr>
								<tr><td colspan="7" class=line></td></tr>
								<tr bgcolor="#eeeeee">
									<td align="left" class="RightTableCaption" HEIGHT="20" width="100"><fmtINInvoiceEdit:message key="LBL_INVOICE_ID" var="varInvoiceId"/><gmjsp:label type="RegularText"  SFLblControlName="${varInvoiceId} " td="false"/></td>
									<td align="center" class="RightTableCaption" HEIGHT="20" width="80"><fmtINInvoiceEdit:message key="LBL_AMOUNT" var="varAmount"/><gmjsp:label type="RegularText"  SFLblControlName="${varAmount} " td="false"/></td>
									<td align="center" class="RightTableCaption"><fmtINInvoiceEdit:message key="LBL_DEPOSIT_DATE" var="varDepositDate"/><gmjsp:label type="RegularText"  SFLblControlName="${varDepositDate}" td="false"/> </td>
									<td align="left" class="RightTableCaption" width="100"><fmtINInvoiceEdit:message key="LBL_MODE" var="varMode"/><gmjsp:label type="RegularText"  SFLblControlName="${varMode}" td="false"/> </td>
									<td class="RightTableCaption" width="210" ><fmtINInvoiceEdit:message key="LBL_DETAILS" var="varDetails"/><gmjsp:label type="RegularText"  SFLblControlName="${varDetails}" td="false"/> </td>
									<td align="center" class="RightTableCaption" HEIGHT="20" width="100"><fmtINInvoiceEdit:message key="LBL_POSTED_BY" var="varPostedBy"/><gmjsp:label type="RegularText"  SFLblControlName="${varPostedBy} " td="false"/></td>
									<td align="center" class="RightTableCaption" HEIGHT="20" width="100"><fmtINInvoiceEdit:message key="LBL_POSTED_DATE" var="varPostedDate"/><gmjsp:label type="RegularText"  SFLblControlName="${varPostedDate} " td="false"/></td>
								</tr>
								<tr><td colspan="7" class=line></td></tr>
<%
								if (intPaySize > 0)
								{
									String strAmount = "";
									String strDetails = "";
									String strInvoiceId = "";
									String strPaymentBy = "";
									java.sql.Date strPaymentDate = null;
									java.sql.Date strDate = null;
									for (int i=0;i<intPaySize;i++)
									{
										hmLoop = (HashMap)alPayDetails.get(i);
										strInvoiceId = GmCommonClass.parseNull((String)hmLoop.get("INVID"));
										strAmount = GmCommonClass.parseNull((String)hmLoop.get("PAYAMT"));
										strDate = (java.sql.Date)(hmLoop.get("RDATE"));
										strMode = GmCommonClass.parseNull((String)hmLoop.get("RMODE"));
										strDetails = GmCommonClass.parseNull((String)hmLoop.get("DETAILS"));
										strPaymentBy = GmCommonClass.parseNull((String)hmLoop.get("CREATEDBY"));
										strPaymentDate = (java.sql.Date)(hmLoop.get("CREATEDDATE"));
			
%>
								<tr>
									<td align="left" class="RightText">&nbsp;<%=strInvoiceId%></td>
									<td height="24" align="right" class="RightText"><%=GmCommonClass.getStringWithCommas(strAmount)%>&nbsp;&nbsp;</td>
									<td  align="center" class="RightText"><%=GmCommonClass.getStringFromDate(strDate,strApplDateFmt)%></td>
									<td align="left" class="RightText"><%=strMode%></td>
									<td class="RightText"><%=strDetails%></td>
									<td class="RightText"><%=strPaymentBy%></td>
									<td align="center" class="RightText"><%=GmCommonClass.getStringFromDate(strPaymentDate,strApplDateFmt)%></td>
								</tr>
								<tr><td colspan="7" bgcolor="#cccccc"></td></tr>
<%
									}		
								}else{
%>
								<tr>
									<td colspan="7" align="center" class=RightTextRed height=30><fmtINInvoiceEdit:message key="LBL_NO_PAMENT_RECIEVED" var="varNoPaymentReceived"/><gmjsp:label type="RegularText"  SFLblControlName="${varNoPaymentReceived}" td="false"/></td>
								</tr>
								<tr><td colspan="7" class=line></td></tr>
<%
								}
%>
							</table>
						</td>
					</tr>
			</table>
		</td>
	</tr>
<%
  		}
	}
if (!strOpt.equals("VIEW"))
{
		if(!strAction.equals("PRINT") && !strStatusFl.equals("5")) 
  		{
			if(!strBatchSuccessMsg.equals("")){
%>
		<tr>
			<td colspan="5" align="center" height="30" class="RightTableCaption">
			<font color="green"><b><%=strBatchSuccessMsg %></b></font>
			</td>
		</tr>
		<%
			} // strBatchSuccessMsg
			if(strElectEmailFl.equals("Y")){
		%>
			<tr>
				<td colspan="2"><div id="divEmailOverride" style="display:none;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr height="30">
								<td><fmtINInvoiceEdit:message key="SEND_EMAIL_INVOICE"/>?&nbsp;
								<input type="checkbox" name="chkEmailOverride"  <%=strEmailChecked%> >&nbsp;(Yes)</td>
							</tr>
							<tr>
								<td class=line></td>
							</tr>
						</table>
						</div>
				</td>
			</tr>
			<%} %>
			<tr>
			<td colspan="5" align="center" height="30" class="RightTableCaption">
<%
			if (strAction.equals("PAY"))
			{
%>				
				<fmtINInvoiceEdit:message key="BTN_PRINT_INVOICE" var="varPrintInvoice"/>
				<fmtINInvoiceEdit:message key="BTN_UPDATE_PO" var="varUpdatePo"/>
				<gmjsp:button value="&nbsp;${varPrintInvoice}&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrintInvoice();" buttonType="Load" />
				<gmjsp:button value="&nbsp;${varUpdatePo}&nbsp;" name="Btn_UpdatePO" gmClass="button" onClick="fnUpdatePO();" buttonType="Save" />
<%
			}

%>
<%
			if (strAction.equals("EC"))
			{
%>
				<fmtINInvoiceEdit:message key="BTN_PRINT_VERSION" var="varPrintVersion"/>
				<gmjsp:button value="&nbsp;${varPrintVersion}&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrintInvoice();" buttonType="Load"  />
<%
			}
%>
				
					<%
					if (strAction.equals("INV"))
						{
		%>
			<fmtINInvoiceEdit:message key="LBL_CHOOSE_ACTION"/>:&nbsp;<gmjsp:dropdown controlName="Cbo_userBatch"
						seletedValue="<%=strCboAction%>" value="<%=alInvoiceBatch%>"
						defaultValue="[Choose one]" codeId="CODEID" codeName="CODENM" onChange="fnActionChange();"/>
						<fmtINInvoiceEdit:message key="BTN_SUBMIT" var="varSubmit"/>
					&nbsp;&nbsp;<gmjsp:button value="&nbsp;${varSubmit}&nbsp;"
					name="Btn_Submit" gmClass="button" onClick="fnSubmit();"
					disabled="<%=strDisable%>" buttonType="Save" />
			<%
			}else{ // strAction = "INV"
			%>
				<gmjsp:button value="<%=strLabel%>" name="Btn_Generate" controlId="Gen" gmClass="button" onClick="fnGenerateInvoice(this.id);" disabled="<%=strDisable%>" buttonType="Save" />&nbsp;&nbsp;&nbsp;
			<%} %>
				</td>
			</tr>
  	<% }else{
  		if(strMultipleInvMsg.equals("")){
%>
		<tr>
			<td colspan="5" align="center" height="30">
			<fmtINInvoiceEdit:message key="BTN_PRINT_INVOICE" var="varPrintInvoice"/>
				<gmjsp:button value="&nbsp;${varPrintInvoice}&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrintInvoice();" buttonType="Load" />
			</td>
		<tr>
<%
		}
  		if(!strMultipleInvMsg.equals("")){
	%>		
			<div id="parentId" style="width: 1198px;height:250px; position: relative;">
			<tr>
				<td colspan="5" align="center" height="30" class="RightTableCaption">
				<font color="green"><b><%=strMultipleInvMsg %></b></font>
				</td>
			</tr>
			</div>
<%
  		}
  		}
%>

			
<%-- 		<%if (!strAction.equals("PRINT") && !strAction.equals("PAY") && !strAction.equals("EC"))
								{%>
								<tr><td colspan="5" class=line></td></tr>
								<tr class="ShadeRightTableCaption"><td colspan="2">
								<fmtINInvoiceEdit:message key="LBL_VAT_AMOUNT_NOT_INCLUDED" var="varVatAmountIncluded"/>
								<b><gmjsp:label type="RegularText"  SFLblControlName="${varVatAmountIncluded}." td="false"/> </b>
								</td></tr>
								<%}%> --%>
			
    </table>
<%@ include file="/common/GmFooter.inc" %>

</FORM>

<%
}

%>
</BODY>

</HTML>
