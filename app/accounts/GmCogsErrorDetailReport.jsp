
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtCogsErrorDetailReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmCogsErrorDetailReport.jsp -->
<fmtCogsErrorDetailReport:setLocale value="<%=strLocale%>"/>
<fmtCogsErrorDetailReport:setBundle basename="properties.labels.accounts.GmCogsErrorDetailReport"/>
<bean:define id="gridData" name="frmCogsErrorForm" property="gridXmlData" type="java.lang.String"> </bean:define>
<%

String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
String strWikiTitle = GmCommonClass.getWikiTitle("COGS_ERROR_REPORT");
String strApplDateFmt = strGCompDateFmt;
//String strGridData = GmCommonClass.parseNull(gridData);

//String strGridData = GmCommonClass.parseNull((String)request.getAttribute("strXml"));

%>

<HTML>
<HEAD>
<TITLE> Globus Medical: COGS Error Detail Report </TITLE>
<%-- <link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
 --%>
 <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<%-- <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Screen.css"> --%>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/fonts/font_roboto/roboto.css"/>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/sources/dhtmlxCalendar/codebase/skins/dhtmlxcalendar_dhx_skyblue.css">
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<%-- <script language="JavaScript" src="<%=strJsPath%>/Error.js"></script> --%>
 <%-- <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
 --%>
 <script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx_deprecated.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_export.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script> 
 <script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/sources/dhtmlxCalendar/codebase/dhtmlxcalendar.js"></script>
  <script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/sources/dhtmlxCalendar/codebase/dhtmlxcalendar_deprecated.js"></script>
 
 <script language="JavaScript" src="<%=strAccountJsPath%>/GmCogsErrorReport.js"></script>
<style type="text/css">
.even{
	background-color:#ffffff !important;
}
.uneven{
background-color:#dcedfc !important;
}
		
div.gridbox table.hdr td {
   color: #000000 !important;
   background-color: #d2e9fc !important;
    border: 1px solid white;
}
div.gridbox_material.gridbox .xhdr {
	background:#dcedfc !important;
}
   
</style>

</HEAD>
 <script>
 	var date_fmt = '<%=strApplDateFmt%>';
    var objGridData;
	objGridData = '<%=gridData%>';
</script>
 
<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoad()">
	<html:form  action="/gmCogsErrorReport.do">	
	<html:hidden   property="strOpt"  />
	<html:hidden property="inputStr" value=""/>	
	<table border="0" width="1300" class="DtTable1300" cellspacing="0"
		cellpadding="0" style="border-width: 1px ; border-style: solid;border-color: #000000">
				   <tr>
					   <td  valign="middle" colspan="5" height="25" class="RightDashBoardHeader" >&nbsp;<fmtCogsErrorDetailReport:message key="LBL_COGS_ERROR_DETAIL_REPORT"/></td> 
					   <td  height="25" class="RightDashBoardHeader" colspan="1">
					   <fmtCogsErrorDetailReport:message key="LBL_HELP" var="varHelp"/>
					   <img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					   </td>		
				    </tr>
				
				    <tr><td colspan="6" height="1" class="line"></td></tr> 
					<tr>					            
						<td  class="RightTableCaption" align="right" HEIGHT="24"><fmtCogsErrorDetailReport:message key="LBL_TRANSACTION_ID"/>:</td>							
						<td>&nbsp;<html:text size="20" styleClass="InputArea"  name= "frmCogsErrorForm"  property="txnId"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>					
						<td  class="RightTableCaption" align="right" HEIGHT="24"><fmtCogsErrorDetailReport:message key="LBL_PART_NUMBER"/>:</td>
						<td  class="RightTableCaption"  HEIGHT="24">&nbsp;<html:text  size="20" styleClass="InputArea" name= "frmCogsErrorForm" property="pnum"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"  /></td>
						<td colspan="2">&nbsp;</td>
					
					 </tr>
					 <tr><td colspan="6" height="1" class="LLine"></td></tr> 	
					 <tr  class="oddshade" >
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtCogsErrorDetailReport:message key="LBL_ERROR_DATE_FROM"/>:</td>
						<td>&nbsp;<gmjsp:calendar  SFFormName='frmCogsErrorForm'  
	 							controlName="fromDate" 
								gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
								onBlur="changeBgColor(this,'#ffffff');" tabIndex="3" />
						 </td>
						<td class="RightTableCaption"  align="right" ><fmtCogsErrorDetailReport:message key="LBL_ERROR_DATE_TO"/>:</td>
						<td>&nbsp;<gmjsp:calendar  SFFormName='frmCogsErrorForm'  
	 							controlName="toDate" 
								gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
								onBlur="changeBgColor(this,'#ffffff');" tabIndex="4" />
						</td>
						<td colspan="2">&nbsp;</td>
					 </tr>
					 <tr><td colspan="6" height="1" class="LLine"></td></tr> 	
					 <!-- Custom tag lib code modified for JBOSS migration changes -->
					 <tr >
						<td  class="RightTableCaption" align="right" HEIGHT="24"><fmtCogsErrorDetailReport:message key="LBL_INVENTORY_TYPE"/>:</td>
						<td >&nbsp;<gmjsp:dropdown  controlName= "invType" SFFormName="frmCogsErrorForm" SFSeletedValue="invType" SFValue="alInvType" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"   />
						</td>
					
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtCogsErrorDetailReport:message key="LBL_STATUS"/>:</td>
						<td>&nbsp;<gmjsp:dropdown controlName= "status"   SFFormName="frmCogsErrorForm" SFSeletedValue="status" SFValue="alStatus" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"   onChange="fnChangeStatus(this);"  /></td>
						<td colspan="2" height="30" align="left"></td>
					
					</tr>
					<tr><td colspan="6" height="1" class="LLine"></td></tr>
					<tr class="oddshade">
						<td class="RightTableCaption" height="25" align="Right">&nbsp;<fmtCogsErrorDetailReport:message key="LBL_OWNER_COMPANY"/>:</td>
						<td>&nbsp;<gmjsp:dropdown controlName="ownerCompanyId"
						SFFormName="frmCogsErrorForm" SFSeletedValue="ownerCompanyId"
						SFValue="alCogsOwnerCompany" codeId="COMPANYID" codeName="COMPANYNM"
						defaultValue="[Choose One]"/>
						</td>	
						<td class="RightTableCaption" align="right"><fmtCogsErrorDetailReport:message key="LBL_PLANT"/>:</td>
						<td colspan="2">&nbsp;<gmjsp:dropdown controlName="costingPlantId"
						SFFormName="frmCogsErrorForm" SFSeletedValue="costingPlantId"
						SFValue="alCogsPlant" codeId="PLANTID" codeName="PLANTNM"
						defaultValue="[Choose One]"/> 
					</td>	
					<td colspan="2">&nbsp;</td>
					</tr>		
					<tr><td colspan="6" height="1" class="LLine"></td></tr>
					<tr>
						<td class="RightTableCaption" height="25" align="Right">&nbsp;<fmtCogsErrorDetailReport:message key="LBL_COSTING_ERROR_TYPE"/>:</td>
						<td>&nbsp;<gmjsp:dropdown controlName="cogsErrorType"
						SFFormName="frmCogsErrorForm" SFSeletedValue="cogsErrorType"
						SFValue="alCogsErrorType" defaultValue="[Choose One]" codeId="CODEID" codeName="CODENM" />
						</td>	
						<fmtCogsErrorDetailReport:message key="BTN_LOAD" var="varLoad"/>
					<td colspan="4" height="30" align="center"> <gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="Button" onClick="fnReload();" buttonType="Load" /></td>
					</tr>	

					<%if(!gridData.equals("")){%>
				    <tr><td colspan="6" height="1" class="LLine"></td></tr> 
					
					<tr>
						<td colspan="6">
							<div  id ="cogsdata" style="height:500px;"></div>
						</td>
					</tr>
					<tr><td colspan="6" height="1" class="LLine"></td></tr>
					<tr>
					<fmtCogsErrorDetailReport:message key="BTN_SUBMIT" var="varSubmit"/>
					<td colspan="6" align="center" height="30"> <gmjsp:button gmClass="Button" name="Submit_button" value="${varSubmit}" buttonType="Save" onClick="fnSubmit();"/> </td>
					</tr>
					<tr><td colspan="6" height="1" class="LLine"></td></tr>
					<tr>
			                <td colspan="6" align="center">
			                <div class='exportlinks'><fmtCogsErrorDetailReport:message key="LBL_EXPORT_OPTOPMS"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
			                                onclick="fnExport();"> <fmtCogsErrorDetailReport:message key="LBL_EXCEL"/> </a></div>
			                </td>

					</tr>
					<%}else{ %>
					<tr><td colspan="6" height="1" class="LLine"></td></tr>
					<tr>
						<td colspan="6" align="center" height="30" class="RightText">
							<fmtCogsErrorDetailReport:message key="LBL_NO_DATA_AVAILABLE"/>
						</td>
					</tr>
					<%} %>
				</table>	
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
					
