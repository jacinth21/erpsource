<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page language="java" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtVarianceRptBySet" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!--GmVarianceRptBySet.jsp -->
<fmtVarianceRptBySet:setLocale value="<%=strLocale%>"/>
<fmtVarianceRptBySet:setBundle basename="properties.labels.accounts.GmVarianceRptBySet"/>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<bean:define id="reportType" name="frmFieldSalesRptForm" property="reportType" type="java.lang.String"> </bean:define>
<bean:define id="dispflaggedDet" name="frmFieldSalesRptForm" property="dispflaggedDet" type="java.lang.Boolean"> </bean:define>
<bean:define id="ldtResult" name="frmFieldSalesRptForm" property="ldtResult" type="java.util.List"></bean:define>
<%
      String strWikiTitle = "";

  String flaggedDet= dispflaggedDet.toString();
  String header="";
  String strTransferTo ="";
  String strBorrowed  ="";
  String strExtra 	 ="";
  String strReturns ="";
  String strTransferFrom  ="";
  String strLendTo 	 ="";
  String strMissing 	 ="";
  String strUnresolvedQty  ="";
  String strFlagQty="";
  int rowsize = ldtResult.size();
  String strAccountsJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
  GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.accounts.GmVarianceRptBySet", strSessCompanyLocale);
	
  if(reportType.equals("globalDist"))
  {
	  header=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_GLOBAL_VARIANCE"));
	  strWikiTitle = GmCommonClass.getWikiTitle("GLOBAL_DISTRIBUTOR");
  }
  else
  {
	  header=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_VARIANCE_REPORT_BY_SET"));
	  strWikiTitle = GmCommonClass.getWikiTitle("VARIANCE_SET");
  }
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Field Sales Report</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     
    table.DtTable1000 {
	width: 1100px;
	}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strAccountsJsPath%>/GmFieldSalesRpt.js"></script>
</HEAD>

<BODY onkeyup="fnEnter();" leftmargin="20" topmargin="10" >
 
<html:form  action="/gmFieldSalesRpt.do?method=reportVarianceRptBySet"  >
<html:hidden property="strOpt"/>
<html:hidden property="reportType"/>
      <table border="0" class="DtTable1000"  cellspacing="0" cellpadding="0">
            <tr>
                  <td colspan="5" height="25" class="RightDashBoardHeader">
                        <%=header%>
                  </td>
                  <td  height="25" class="RightDashBoardHeader">
                  <fmtVarianceRptBySet:message key="LBL_HELP" var="varHelp"/>
                        <img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
                  </td>
            </tr>
            <tr><td colspan="6" class="LLine" height="1"></td></tr>
	    <!-- Custom tag lib code modified for JBOSS migration changes -->
            <tr  class="shade" >          
                  <td height="30" align ="Right" class="RightTableCaption"><font color="red">*</font><fmtVarianceRptBySet:message key="LBL_AUDIT_LIST"/>:&nbsp;</td>
                  <td><gmjsp:dropdown controlName="auditId" SFFormName="frmFieldSalesRptForm" SFSeletedValue="auditId" 
                     SFValue="alAuditName" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]"/></td>
                                               
                 <% if(!reportType.equals("globalDist"))
                		 {%>
                  <td height="30"  width="50" align ="Right" class="RightTableCaption"><font color="red">*</font><fmtVarianceRptBySet:message key="LBL_SET"/>:&nbsp;</td>
                  <td align ="Left" width="90">
                  <gmjsp:dropdown controlName="setId" width="250" SFFormName="frmFieldSalesRptForm" SFSeletedValue="setId" SFValue="alSet" codeId = "ID"  codeName = "IDNAME" defaultValue= "[Choose One]" />
                  </td>                   
                  <td height="30" align ="Right" class="RightTableCaption" ><fmtVarianceRptBySet:message key="LBL_TERRITORY"/>:&nbsp;</td>
                  <td><gmjsp:dropdown width="170" controlName="territoryId" SFFormName="frmFieldSalesRptForm" SFSeletedValue="territoryId" SFValue="alTerritory" codeId = "TID"  codeName = "TNAME" defaultValue= "[Choose One]" /> 		</td>    
                      <%}else
                      {
                      %> 
                      <td   colspan="4" >    </td>    
                        <%}
                      %>                       
            </tr>         
            <tr><td colspan="6" class="LLine" height="1"><td><tr>
            	<tr>
		       <td colspan="6">
		       <table>
			       	<tr>
			       		<td>
							<jsp:include page="/accounts/GmVarianceRptFilter.jsp" >
							<jsp:param name="FORMNAME" value="frmFieldSalesRptForm" />
							</jsp:include>
						</td>
					</tr>
				</table>	
				</td>
			</tr>
            <tr><td colspan="6" class="LLine" height="1"><td><tr>            
    		<tr>
			<td  colspan="6" width="100%">
			<display:table name="requestScope.frmFieldSalesRptForm.ldtResult" id="currentRowObject" varTotals="totals" decorator="com.globus.accounts.displaytag.beans.DTFieldSalesRptWrapper" requestURI="/gmFieldSalesRpt.do?method=reportVarianceRptBySet" export="true" freezeHeader="true" cellpadding="0" cellspacing="0">
            <display:setProperty name="export.excel.filename" value="Variance Rpt.xls" />
			<fmtVarianceRptBySet:message key="LBL_DISTRIBUTOR" var="varDistributor"/><display:column property="DIST_NAME" title="${varDistributor}" headerClass="ShadeDarkGrayTDCenter" style="text-align: left;" class="ShadeLightGrayTD" sortable="true" comparator="com.globus.common.displaytag.beans.DTComparator" />
			<display:column  property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
			<fmtVarianceRptBySet:message key="LBL_AUDIT_QTY" var="varAuditQty"/><display:column property="AUDIT_QTY" title="${varAuditQty}" headerClass="ShadeDarkBlueTD"  total="true" sortable="true" class="ShadeLightBlueTD"/>
			<fmtVarianceRptBySet:message key="LBL_SYSTEM_QTY" var="varSystemQty"/><display:column property="SYSTEM_QTY" title="${varSystemQty}" headerClass="ShadeDarkBlueTD"  total="true" sortable="true" class="ShadeMedBlueTD"/>
			<fmtVarianceRptBySet:message key="LBL_DEVIATION_QTY" var="varDeviationQty"/><display:column property="DEVIATION_QTY" title="${varDeviationQty}" headerClass="ShadeDarkBlueTD"  total="true" class="alignleft;ShadeLightBlueTD" sortable="true"  comparator="com.globus.common.displaytag.beans.DTComparator"/>
			<display:column  property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
		  <% if(flaggedDet.equals("true"))
                		 {%>
			<fmtVarianceRptBySet:message key="LBL_TRANSFER_FROM" var="varTransferFrom"/><display:column property="TRANSFER_FROM" title="${varTransferFrom}" headerClass="ShadeDarkBrownTD" class="ShadeLightBrownTD" style="text-align: right;" total="true"  sortable="true" />
			<fmtVarianceRptBySet:message key="LBL_BORROWED_FROM" var="varBorrowedFrom"/><display:column property="BORROWED_FROM" title="${varBorrowedFrom}" headerClass="ShadeDarkBrownTD" class="ShadeMedBrownTD" style="text-align: right;" total="true"  sortable="true" />
			<fmtVarianceRptBySet:message key="LBL_EXTRA" var="VarExtra"/><display:column property="EXTRA" title="${VarExtra}" headerClass="ShadeDarkBrownTD" class="ShadeLightBrownTD" style="text-align: right;" total="true" sortable="true" />
			<display:column  property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
			<fmtVarianceRptBySet:message key="LBL_RETURNS" var="varReturns"/><display:column property="RETURNS" title="${varReturns}" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD" style="text-align: right;" total="true" sortable="true" />
			<fmtVarianceRptBySet:message key="LBL_TRANSFER_TO" var="varTransferTo"/><display:column property="TRANSFER_TO" headerClass="ShadeDarkGreenTD" class="ShadeLightGreenTD" style="text-align: right;" title="${varTransferTo}" total="true" sortable="true" />
			<fmtVarianceRptBySet:message key="LBL_LEND_TO" var="varLendTo"/><display:column property="LEND_TO" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD"  style="text-align: right;" title="${varLendTo}" total="true" sortable="true" />
			<fmtVarianceRptBySet:message key="LBL_MISSING" var="varMissing"/><display:column property="MISSING" headerClass="ShadeDarkGreenTD" class="ShadeLightGreenTD" style="text-align: right;" title="${varMissing}" total="true"  sortable="true" />
			<display:column  property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
			<fmtVarianceRptBySet:message key="LBL_UNRESOLVED_QTY" var="varUnresolveQty"/><display:column property="UNRESOLVED_QTY" title="${varUnresolveQty}" total="true" class="alignleft;ShadeMedOrangeTD" sortable="true" headerClass="ShadeDarkOrangeTD"/>
			<%} else{ %>
			<fmtVarianceRptBySet:message key="LBL_FLAGGED_QTY" var="varFlagQty"/><display:column property="FLAGGED_QTY" title="${varFlagQty}" headerClass="ShadeDarkOrangeTD" total="true" sortable="true" class="ShadeLightOrangeTD"/>
			<fmtVarianceRptBySet:message key="LBL_UNRESOLVED_QTY" var="varUnResolvedQty"/><display:column property="UNRESOLVED_QTY" title="${varUnResolvedQty}" total="true" class="alignleft;ShadeMedOrangeTD" sortable="true" headerClass="ShadeDarkOrangeTD" />
			<%} %>
			<display:footer media="html"> 
			<% if(flaggedDet.equals("true"))
             		 {%> 
             		 
             	<tr><td class="Line" colspan="16"></td></tr>
             	
             	<%}else{ %>
			<tr><td class="Line" colspan="16"></td></tr>
			<%} %>
					<tr><td class = "ShadeDarkGrayTD"> <B> <fmtVarianceRptBySet:message key="LBL_TOTAL_RECORDS"/>: <%=rowsize %> </B></td>
					<%String strAuditQty 		= ((HashMap)pageContext.getAttribute("totals")).get("column3").toString();
					String strSystemQty 		= ((HashMap)pageContext.getAttribute("totals")).get("column4").toString();
					String strDeviationQty 		= ((HashMap)pageContext.getAttribute("totals")).get("column5").toString();
					
					strAuditQty =GmCommonClass.getStringWithCommas(strAuditQty,0);
					strSystemQty =GmCommonClass.getStringWithCommas(strSystemQty,0);
					strDeviationQty =GmCommonClass.getStringWithCommas(strDeviationQty,0);
				
					  if(flaggedDet.equals("true"))
             		 {
             		 
						 strTransferTo 		= ((HashMap)pageContext.getAttribute("totals")).get("column7").toString();
						 strBorrowed 		= ((HashMap)pageContext.getAttribute("totals")).get("column8").toString();
						 strExtra 		= ((HashMap)pageContext.getAttribute("totals")).get("column9").toString();
						 strReturns 		= ((HashMap)pageContext.getAttribute("totals")).get("column11").toString();
						 strTransferFrom 		= ((HashMap)pageContext.getAttribute("totals")).get("column12").toString();
						 strLendTo 		= ((HashMap)pageContext.getAttribute("totals")).get("column13").toString();
						 strMissing 		= ((HashMap)pageContext.getAttribute("totals")).get("column14").toString();
						 strUnresolvedQty 	= ((HashMap)pageContext.getAttribute("totals")).get("column16").toString();
						 strTransferTo  =GmCommonClass.getStringWithCommas( strTransferTo,0);
						 strBorrowed =GmCommonClass.getStringWithCommas(strBorrowed,0);
						 strExtra =GmCommonClass.getStringWithCommas(strExtra,0);
						 strReturns =GmCommonClass.getStringWithCommas(strReturns,0);
						 strTransferFrom =GmCommonClass.getStringWithCommas(strTransferFrom,0);
						 strLendTo =GmCommonClass.getStringWithCommas(strLendTo,0);
						 strMissing =GmCommonClass.getStringWithCommas(strMissing,0);
						 strUnresolvedQty =GmCommonClass.getStringWithCommas(strUnresolvedQty,0);
             		 } else  {
             			 
             			strFlagQty 	= ((HashMap)pageContext.getAttribute("totals")).get("column7").toString();
             			strFlagQty =GmCommonClass.getStringWithCommas(strFlagQty,0);
             			strUnresolvedQty 	= ((HashMap)pageContext.getAttribute("totals")).get("column8").toString();
             			strUnresolvedQty =GmCommonClass.getStringWithCommas(strUnresolvedQty,0);
             		 }
					%>
					 
					<td colspan="2" class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strAuditQty%></B></td>
					<td class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strSystemQty %></B></td>
					<td class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strDeviationQty%></B></td>
					<% if(flaggedDet.equals("true"))
             		 {%> 
             		 	<td colspan="2" class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strTransferTo%></B></td>
             		  	<td class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strBorrowed%></B></td>
             		   	<td class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strExtra%></B></td>
             		    <td colspan="2" class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strReturns%></B></td>
             		    <td  class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strTransferFrom%></B></td>
             		    <td class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strLendTo%></B></td>
             		    <td colspan="2" class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strMissing%></B></td>
             		    <td class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strUnresolvedQty%></B></td>
             		   <%}else {  %>
             		   <td colspan="2" class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strFlagQty%></B></td>
             		   <td class = "ShadeDarkGrayTD" style="text-align: right;" ><B><%=strUnresolvedQty%></B></td>
             		   <%} %>
             		   <tr><td class="Line" colspan="16"></td></tr>
			</display:footer>
			</display:table></td>
			</tr>
      </table>

</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

