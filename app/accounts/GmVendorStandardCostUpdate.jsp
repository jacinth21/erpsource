
<%
	/**********************************************************************************
	 * File		 		: GmStandardReportEdit.jsp
	 * Desc		 		: This screen is used for Edit Report 
	 * Version	 		: 
	 * author			: Prabhu Vigneshwaran M D
	 *******
	 *****************************************************************************/
%>

 <!-- \accounts\GmVendorStandardCostUpdate.jsp -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import = "java.util.*,java.io.*"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="fmtStandardCostReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<bean:define id="editAccess" name="frmStandardReport"  property="strEditAccess" type="java.lang.String"></bean:define>
<bean:define id="strsuccess" name="frmStandardReport"  property="strsuccess" type="java.lang.String"></bean:define>
<% 
String strWikiTitle = GmCommonClass.getWikiTitle("STD_COST_RPT"); 

String strSubmitDisable = "";
if(!editAccess.equals("Y")){
	strSubmitDisable = "true";
} 

String strAccountsJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
%>  


<HTML>
<HEAD>
<title>GlobusOne Enterprise Portal: Standard Report Edit Cost</title>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strAccountsJsPath%>/GmVendorStandardCostUpdate.js"></script>

<BODY leftmargin="20" topmargin="10" onkeyup="fnEnter();">
 	<html:form action="/gmVendorStandardCost.do?method=loadStandardCostRpt">
	<html:hidden property="strOpt" name="frmStandardReport" value="" />
	<html:hidden property="pnum" name="frmStandardReport" />
	<html:hidden property="vendorId" name ="frmStandardReport"/>
	<html:hidden property="projectId" name ="frmStandardReport"/>
	<html:hidden property="strPartNumbers" name ="frmStandardReport"/>
	<html:hidden property="strVendorId" name ="frmStandardReport"/>
   <table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
	<tr>
				<td height="25" class="RightDashBoardHeader">Standard Edit Cost</td>
				<td align="right" class="RightDashBoardHeader"><img
					id='imgEdit' align="right" style='cursor: hand'
					src='<%=strImagePath%>/help.gif' title='Help' width='16'
					height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td class="Line" colspan="2"></td>
			</tr>
			
			<tr >
				<td height="30" class="RightTableCaption" align="right"><gmjsp:label
							type="RegularText" SFLblControlName="Vendor Name:" td="false" />
								<td>&nbsp;<bean:write
						property="vendorName" name="frmStandardReport" /></td>
			</tr>
			<tr class="Shade">
		
				<td height="30" class="RightTableCaption" align="right"><gmjsp:label
							type="RegularText" SFLblControlName="Part #:" td="false" />
				</td>
				<td>&nbsp;<bean:write
						property="pnum" name="frmStandardReport" /></td>			</tr>
			<tr>
				<td class="Line" colspan="2"></td>
			</tr>
			<tr >
				<td height="25" class="RightTableCaption" align="right"><gmjsp:label
							type="RegularText" SFLblControlName="Part Description:" td="false" /></td>
				<td>&nbsp;<bean:write
						property="partDesc" name="frmStandardReport" /></td>
				</td>
			</tr>
			<tr>
				<td class="Line" colspan="2"></td>
			</tr>
			<tr class="Shade">
		
				<td height="25" class="RightTableCaption" align="right"><gmjsp:label
						type="RegularText" SFLblControlName="Cost:" td="false" /></td>
			         <td>&nbsp;<html:text property="cost"  
			         styleClass="InputArea"  
			         onfocus="changeBgColor(this,'#AACCE8');" 
			         onblur="changeBgColor(this,'#ffffff');"
			         onkeypress="return isNumberKey(event);"
			       ></html:text> 
					 <logic:equal
						name="frmStandardReport" property="historyFl" value="Y">
						<img id="imgEdit" style="cursor: hand"
							onclick="javascript:fnSetCostHistory();"
							title="Click to open Part history"
							src="<%=strImagePath%>/icon_History.gif" align="bottom" height=18
							width=18 />&nbsp;</logic:equal></td>

			</tr>
			<tr>
				<td class="Line" colspan="2"></td>
			</tr>
			<tr>
				<td height="30" align="center" colspan="2"><gmjsp:button
					value="&nbsp;Submit&nbsp;" disabled="<%=strSubmitDisable%>" gmClass="button" buttonType="Save" onClick="fnSubmit();"/>
					&nbsp;&nbsp;<gmjsp:button value="&nbsp;Back&nbsp;"
					gmClass="button" buttonType="Save" onClick="fnBack();"/>
				</td>
			</tr>
			  <logic:notEmpty name="frmStandardReport" property="strsuccess">
				<tr>
					<td class="Line" colspan="2"></td>
				</tr> 
				<tr>
					<td height="30" align="center" colspan="2">
					 <font color="green"><b><bean:write name="frmStandardReport" property="strsuccess" /></b></font></td>
				</tr>
			</logic:notEmpty>  

		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>

							 