<%
/**********************************************************************************
 * File		 		: GmCashApplication.jsp
 * Desc		 		: This screen is used Multi Invoice post payment. 
 * Version	 		: 1.0
 * author			: Velu
************************************************************************************/
%>
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>
<%@ taglib prefix="fmtCashApplication" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@page import="java.util.Date,com.globus.common.beans.GmCalenderOperations"%>
<!-- GmCashApplication.jsp -->
<fmtCashApplication:setLocale value="<%=strLocale%>"/>
<fmtCashApplication:setBundle basename="properties.labels.accounts.GmCashApplication"/>
<bean:define id="gridData" name="frmCashApp" property="gridData" type="java.lang.String"></bean:define>
<bean:define id="strCompCurrName" name="frmCashApp" property="strCompCurrName" type="java.lang.String"></bean:define>
<bean:define id="strARRptByDealerFlag" name="frmCashApp" property="strARRptByDealerFlag" type="java.lang.String"></bean:define>
<%

	String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");	
	String strWikiTitle = GmCommonClass.getWikiTitle("CASH_APPLICATION");
	
    HashMap hmCurrency = new HashMap();
    hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strApplDateFmt = strGCompDateFmt;
	//PC-4637-payment date validation
	int currMonth = GmCalenderOperations.getCurrentMonth()-1;
	String strFromdaysDate = GmCommonClass.parseNull(GmCalenderOperations.getFirstDayOfMonth(currMonth,strApplDateFmt)); //To get the first day of month
	String strTodaysDate = GmCommonClass.parseNull(GmCalenderOperations.getCurrentDate(strApplDateFmt));
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	String strPaymentValidFl = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.PAYMENT_DATE_VALIDATION_FL"));
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Cash Application</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmCashApplication.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>

<script>
	var objGridData;
	var rowID = '';
	objGridData = '<%=gridData%>';
	var currSign = '<%=strCompCurrName%>';
	var gridObj = '';
	var myRowId = 1;
	var format = '<%=strApplDateFmt%>';
	var arDealerFlag = '<%=strARRptByDealerFlag%>';
	var lblPayDate = '<fmtCashApplication:message key="LBL_PAYMENT_DATE"/>';
	//PC-4637-payment date validation
	var strFromdaysDate='<%=strFromdaysDate%>';
	var strLastdaysDate='<%=strTodaysDate%>';
	var paymentValidationFl = '<%=strPaymentValidFl%>';
</script>

</HEAD>


<BODY leftmargin="20" topmargin="10"  onkeyup="fnEnter();" onload="fnOnPageLoad();">

	<html:form action="/gmCashApplication.do?method=loadCashAppDtls" >
		<html:hidden property="strOpt" value="" />
		<html:hidden property="inputString" value="" />
		<html:hidden property="paymentAmt" value="" />
		<html:hidden property="paidInvoiceIDs" value="" />
		<input type="hidden" name="hRedirectURL" value="" />

		<table border="0" class="DtTable1200" width="900" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="3" height="25" class="RightDashBoardHeader"><fmtCashApplication:message key="LBL_CASH_APPLICATION"/></td>
				<fmtCashApplication:message key="LBL_HELP" var="varHelp"/>
				<td height="25" colspan="1" class="RightDashBoardHeader"><img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td colspan="4" class="LLine" height="1"></td>
			</tr>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<tr>
			<fmtCashApplication:message key="LBL_PAYMENT_DATE" var="varPaymentDate"/>
				<td height="25" align="right" colspan="1" class="RightTableCaption" ><gmjsp:label type="MandatoryText"
						SFLblControlName="${varPaymentDate}:" td="false" />
				</td>
				<td  align="left" colspan="1">&nbsp;<gmjsp:calendar 
						SFFormName="frmCashApp" controlName="paymentDt"
						gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" tabIndex="1" /></td>	
				<td colspan="2" class="RightTableCaption">
				     <b><fmtCashApplication:message key="LBL_CURRENCY"/>:</b>&nbsp;
				     <gmjsp:dropdown	controlName="strCompCurrency" SFFormName="frmCashApp"
				       SFSeletedValue="strCompCurrency" SFValue="alCompCurrency" codeId="ID" codeName="NAMEALT" />	</td>				
			</tr>
			<tr>
				<td colspan="4" class="LLine" height="1"></td>
			</tr>
			<tr height="25">
			<td class="RightTableCaption" align="center"><fmtCashApplication:message key="LBL_CREDIT_RED"/></font></td>
			<td class="RightTableCaption" align="center"><fmtCashApplication:message key="LBL_DEBIT"/></font></td>
			<td class="RightTableCaption" align="center"><fmtCashApplication:message key="LBL_REGULAR"/></font></td>
			<td colspan="2" class="RightTableCaption" align="center"><fmtCashApplication:message key="LBL_OBO"/></font></td>
			</tr>
			<tr>
				<td colspan="4" class="LLine" height="1"></td>
			</tr>
			
		<tr>
			<td class="RightTableCaption" align="left" colspan="4" height="20">
				<table cellpadding="1" cellspacing="1" id="addTbl">
					<TR>
						<td class="RightTableCaption"><a href="javascript:addRow();"><img
								src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="Add Row"
								style="border: none;" height="14">
						</a><a href="javascript:removeSelectedRow();"><img src="<%=strImagePath%>/dhtmlxGrid/delete.gif"
								alt="Remove Row" style="border: none;"
								height="14">
						</a></td>
					</TR>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<div id="issueMemoRpt" style="" height="400px" width="1198px"></div>
			</td>
		</tr>
			<tr height="40">
				<td colspan="4" align="center" id="AllButton">
				<fmtCashApplication:message key="BTN_SUBMIT" var="varSubmit"/>
					<gmjsp:button value="${varSubmit}" gmClass="Button" name="btn_Submit"
					style="width: 6em" onClick="fnSubmit();" buttonType="Save" /></td>
			</tr>
		</table>

	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>