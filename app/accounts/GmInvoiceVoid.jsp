 <%@page import="java.util.Locale"%>
<%
/**********************************************************************************
 * File		 		: GmInvoiceEdit.jsp
 * Desc		 		: This screen is used for the Order Maintenance
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<!-- Imports for Logger -->

<%@ taglib prefix="fmtInvoiceVoid" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmInvoiceVoid.jsp -->

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	Logger log = GmLogger.getInstance(GmCommonConstants.ACCOUNTING);

	if(!strSessCompanyLocale.equals("")){
		locale = new Locale("en", strSessCompanyLocale);
		strLocale = GmCommonClass.parseNull((String)locale.toString());
		strJSLocale = "_"+strLocale;
	}
	
	String strAcctPath = GmCommonClass.getString("GMACCOUNTS");
	String strPOJSP = strAcctPath.concat("/GmCustomerPOEdit.jsp");
	
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	log.debug(" Action inside JSP is " + strAction);
	String strOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
	
	if (strAction == null)
	{
		strAction = (String)session.getAttribute("hAction");
	}
	strAction = (strAction == null)?"Load":strAction;

	HashMap hmReturn = new HashMap();

	ArrayList alOrderNums = new ArrayList();
	HashMap hmCartDetails = new HashMap();

	hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmOrderDetails = new HashMap();
	HashMap hmShipDetails = new HashMap();

	String strShade = "";

	String strInvNum = "";
	String strAccId= "";
	String strDistRepNm = "";
	String strBillAdd = "";
	String strInvDate = "";
	String strPO = "";
	String strOrdId = "";

	String strPartNum = "";
	String strDesc = "";
	String strPrice = "";
	String strQty = "";
	String strItemOrdId = "";
	String strPartNums = "";
	String strControlNum = "";
	String strPayNm = "";
	String strPay = "";
	String strDueDate = "";
	String strStatusFl = "";
	String strCallFlag = "";
	String strInvType = "";
	String strMode = "";
	String strControlName = "";

	String strLabel = "Generate";
	if (strAction.equals("PAY"))
	{
			strLabel = "Update";
	}
	else if (strAction.equals("IC"))
	{
			strLabel = "Issue Credit";
	}

	if (hmReturn != null)
	{
		hmOrderDetails = (HashMap)hmReturn.get("ORDERDETAILS");
		alOrderNums = (ArrayList)hmReturn.get("ORDERNUMS");
		
		hmCartDetails = (HashMap)hmReturn.get("CARTDETAILS");
		strInvNum = (String)hmOrderDetails.get("INV");
		strAccId = (String)hmOrderDetails.get("ACCID");
		strPO = (String)hmOrderDetails.get("PO");
		strBillAdd = (String)hmOrderDetails.get("BILLADD");
		strDistRepNm = (String)hmOrderDetails.get("REPDISTNM");
		strInvDate = (String)hmOrderDetails.get("INVDT");
		strDueDate = (String)hmOrderDetails.get("DUEDT");
		strPayNm = (String)hmOrderDetails.get("PAYNM");
		strPay = (String)hmOrderDetails.get("PAY");
		strStatusFl = (String)hmOrderDetails.get("SFL");
		strCallFlag = (String)hmOrderDetails.get("CALL_FLAG");
		strInvType = (String)hmOrderDetails.get("INVTYPE");
		strPayNm = strPayNm.equals("")?"Net 30":strPayNm;
	}

	
	int intSize = 0;
	ArrayList alLoop = null;
	HashMap hmTemp = null;
	HashMap hmLoop = null;
%>
<fmtInvoiceVoid:setLocale value="<%=strLocale%>"/>
<fmtInvoiceVoid:setBundle basename="properties.labels.accounts.GmInvoiceVoid"/>
<HTML>
<HEAD>
<TITLE> Globus Medical: Invoice Void </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>

<script language = "javascript">

function fnPrintInvoice(varinv)
{
windowOpener('/GmInvoiceInfoServlet?hAction=Print&hInv='+varinv,"PrntInv","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=500");
}


function fnPrintPack(val)
{
	windowOpener('<%=strServletPath%>/GmEditOrderServlet?hMode=PrintPack&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
}

function fnPrintCreditMemo(val)
{
	windowOpener('<%=strServletPath%>/GmReportCreditsServlet?hAction=PrintVersion&hOrderId='+val,"Credit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintCashAdjustMemo(val)
{
	windowOpener('<%=strServletPath%>/GmReportCreditsServlet?hAction=PrintCashAdj&hOrderId='+val,"Credit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnVoidInvoice()
{
	document.frmInvoice.hAction.value = "VOIDCONFIRM";
	document.frmInvoice.submit();
}



</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmInvoice" method="post" action = "<%=strServletPath%>/GmInvoiceServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hPO" value="<%=strPO%>">
<input type="hidden" name="hInv" value="<%=strInvNum%>">
<input type="hidden" name="hAccId" value="<%=strAccId%>">
<input type="hidden" name="hPay" value="<%=strPay%>">
<input type="hidden" name="hStr" value="">
<input type="hidden" name="hCustomerPO" value="">
<input type="hidden" name="hReasonForChange" value="">
<input type="hidden" name="hInvType" value="">
<input type="hidden" name="hInputStr" value="">

	<table border="0" width="800" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" colspan="4"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="6"><img src="<%=strImagePath%>/spacer.gif" width = "1" height = "1"></td>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td height="25" class="RightDashBoardHeader"><fmtInvoiceVoid:message key="LBL_INVOICE_VOID"/> </td>
					</tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1" rowspan="6"><img src="<%=strImagePath%>/spacer.gif" width = "1" height = "1"></td>
		</tr>
			<td bgcolor="#666666" colspan="4"></td>
		<tr>
			<td width="800" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr class="ShadeRightTableCaption">
									<td height="25" align="center" width="450">&nbsp;<fmtInvoiceVoid:message key="LBL_BILL_TO"/></td>
									<td bgcolor="#666666" width="1" rowspan="7"></td>
									<td height="25" align="center">&nbsp;<fmtInvoiceVoid:message key="LBL_CUSOTMER_PO"/> #</td>
									<td bgcolor="#666666" width="1" rowspan="7"></td>
									<td width="100" align="center">&nbsp;<fmtInvoiceVoid:message key="LBL_INVOICE_DATE"/></td>
									<td width="100" align="center">&nbsp;<fmtInvoiceVoid:message key="LBL_TERMS"/></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="6"></td>
								</tr>
								<tr>
									<td class="RightText" rowspan="5" valign="top">&nbsp;<%=strBillAdd%></td>
									<td class="RightText" align="center">&nbsp;<%=strPO%></td>
									<td height="25" class="RightText" align="center">
										<%=strInvDate%>
									</td>
									<td class="RightText" align="center">&nbsp;<%=strPayNm%></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
								</tr>
								<tr class="ShadeRightTableCaption">
									<td class="RightText" align="center" height="20"><fmtInvoiceVoid:message key="LBL_INVOICE"/> #</td>
									<td align="center"><fmtInvoiceVoid:message key="LBL_DIST_REP"/></td>
									<td align="center">&nbsp;</td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
								</tr>
								<tr>
									<td class="RightText" rowspan="3" align="center">&nbsp;<%=strInvNum%></td>
									<td align="center" class="RightText">&nbsp;<%=strDistRepNm%><Br></td>
									<td align="center" height="25" class="RightText">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" height="1" bgcolor="#666666"></td>
					</tr>
					<tr>
						<td align="center" colspan="2" valign="top">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr class="ShadeRightTableCaption">
									<td width="70" height="25">&nbsp;<fmtInvoiceVoid:message key="LBL_ORDER"/> #</td>
									<td height="25" align="center" >&nbsp;<fmtInvoiceVoid:message key="LBL_ORDER_DATE"/></td>
									<td width="70" align="center" height="25">&nbsp;<fmtInvoiceVoid:message key="LBL_PART_NUMBER"/></td>
									<td width="250">&nbsp;<fmtInvoiceVoid:message key="LBL_DESCRIPTION"/></td>
									<td align="center" width="70"><fmtInvoiceVoid:message key="LBL_CONTROL_NUMBER"/></td>
									<td align="center" width="80"><fmtInvoiceVoid:message key="LBL_QTY"/></td>
									<td align="center" width="80"><fmtInvoiceVoid:message key="LBL_PRICE_EA"/></td>
									<td align="center" width="100"><fmtInvoiceVoid:message key="LBL_AMOUNT"/></td>
									<td align="center" width="80"><fmtInvoiceVoid:message key="LBL_CREDIT_QTY"/></td>
									<td align="center" width="80"><fmtInvoiceVoid:message key="LBL_CREDIT_PRICE_EA"/></td>
								</tr>
								<tr>
									<td colspan="10" height="1" bgcolor="#666666"></td>
								</tr>
<%
					int intLoop = 0;
			  		intSize = alOrderNums.size();
					alLoop = new ArrayList();
					String strItems = "";
					String strOrdDate = "";
					int intQty = 0;
					double dbItemTotal = 0.0;
					String strItemTotal = "";
					double dbTotal = 0.0;
					String strTotal = "";
					String strShipCost = "";
					double dblShip = 0.0;

					int intTotalLoop = 0;
					int k = 0;

			  		for (int i=0;i<intSize;i++)
			  		{
				  		hmTemp = (HashMap)alOrderNums.get(i);
						strOrdId = (String)hmTemp.get("ID");

						alLoop = (ArrayList)hmCartDetails.get(strOrdId);
						intLoop = alLoop.size();
						intTotalLoop = intTotalLoop + intLoop;
						for ( int j=0;j<intLoop;j++)
						{

							hmLoop = (HashMap)alLoop.get(j);
							strOrdDate = (String)hmLoop.get("ODT");
							strPartNum = (String)hmLoop.get("ID");
							strDesc = (String)hmLoop.get("PDESC");
							strPrice = (String)hmLoop.get("PRICE");
							strQty = (String)hmLoop.get("QTY");
							strControlNum = (String)hmLoop.get("CNUM");
							intQty = Integer.parseInt(strQty);
							dbItemTotal = Double.parseDouble(strPrice);
							dbItemTotal = intQty * dbItemTotal; // Multiply by Qty
							strItemTotal = ""+dbItemTotal;
							dbTotal = dbTotal + dbItemTotal;
							strTotal = ""+dbTotal;
							strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
%>
								<tr >
<%
							if ( j == 0)
							{	/* To print the order in for the list link */
%>
									<td class="RightText" rowspan="<%=intLoop%>" height="20">
<%						// Check if the Orded is a Return or Duplicate Order -- redirect based on the type 						
						if ((strOrdId.charAt(strOrdId.length()-1)== 'R') ||(strOrdId.charAt(strOrdId.length()-1)== 'D') )
						{%><a href="javascript:fnPrintCreditMemo('<%=strOrdId%>');" 
<%						}else if (strOrdId.charAt(strOrdId.length()-1)== 'A') 
						{%><a href="javascript:fnPrintCashAdjustMemo('<%=strOrdId%>');" 
<%						}else
						{%>	<a href="javascript:fnPrintPack('<%=strOrdId%>');" 
<%						}%>class="RightText"><%=strOrdId%></a> </td>
									<td class="RightText" rowspan="<%=intLoop%>" height="20">&nbsp;<%=strOrdDate%></td>
<%							
							}
%>
									<td class="RightText" align="center" height="20">&nbsp;<%=strPartNum%></td>
									<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
									<td class="RightText" height="20">&nbsp;<%=strControlNum%></td>
									<td class="RightText" align="center">&nbsp;<%=GmCommonClass.getRedText(strQty)%></td>
									<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strPrice))%>&nbsp;</td>
									<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strItemTotal))%>&nbsp;</td>
									
								</tr>
<%					
							if ( j == intLoop -1)
							{	
								strShipCost = GmCommonClass.parseZero((String)hmLoop.get("SCOST"));
								dblShip = Double.parseDouble(strShipCost);
								dbTotal = dbTotal + dblShip;
								strTotal = ""+dbTotal;
%>
							<tr>
								<td colspan="3">&nbsp;</td>
								<td class="RightText">&nbsp;<fmtInvoiceVoid:message key="LBL_SHIPPING_CHARGES"/></td>
								<td colspan="2">&nbsp;<td>
								<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strShipCost))%>&nbsp;</td>
							</tr>
							<tr><td colspan="10" height="1" bgcolor="#cccccc"></td></tr>
<%
							}
						}
					}
%>
								<tr>
									<td colspan="6" height="30">&nbsp;</td>
									<td class="RightTableCaption" align="center"><fmtInvoiceVoid:message key="LBL_TOTAL"/></td>
									<td class="RightTableCaption" align="right">$<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strTotal))%>&nbsp;</td>
								</tr>
								<tr>
									<td colspan="10" height="1" bgcolor="#666666"><input type="hidden" name="hTotalCnt" value="<%=""+intTotalLoop%>"></td>
								</tr>
					
							<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="LogType" value="" />
							<jsp:param name="LogMode" value="Edit" />
							</jsp:include>			
							
							<tr>
								<td align="center" height="30" id="button">
									<fmtInvoiceVoid:message key="BTN_VOID_INVOICE" var="varVoidInvoice"/>
									<gmjsp:button value="&nbsp;${varVoidInvoice} &nbsp;" name="Btn_VoidInvoice" gmClass="button" onClick="fnVoidInvoice();" buttonType="Save" />&nbsp;&nbsp;
								</td>
							</tr>
								<tr><td colspan="10" height="1"  bgcolor="#666666"></td></tr>
					
</FORM>
  </table>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
