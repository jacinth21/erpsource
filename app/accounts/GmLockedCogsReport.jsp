 <%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.text.*" %>
<%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtLockedCogReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!--GmLockedCogsReport.jsp -->
<fmtLockedCogReport:setLocale value="<%=strLocale%>"/>
<fmtLockedCogReport:setBundle basename="properties.labels.accounts.GmLockedCogsReport"/>
<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strWikiTitle = GmCommonClass.getWikiTitle("VIEW_LOCKED_PRICING");
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	String strDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt+" K:m:s a");
	String strUserName = (String)session.getAttribute("strSessShName");
	String strApplDateFmt = strGCompDateFmt;
	String strDateFmt = "{0,date,"+strApplDateFmt+"}";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Locked Inventory Unit Pricing
</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
function fnGo()
{	
	if(document.frmCogsForm.pnum.value == 0&&document.frmCogsForm.projectListID.value == 0){
		Error_Details(message_accounts[259]);
		}
	
	if(document.frmCogsForm.inventoryListID.value == 0){
		Error_Details(message_accounts[260]);
		}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.body.style.cursor = 'wait';
	fnStartProgress();
	document.frmCogsForm.submit();
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10"  >
<html:form action="/gmLockedCogsReport.do" >

<input type="hidden" name="strOpt" value="ReloadLock">

<table class="DtTable850" border="0"  cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2" height="1" bgcolor="#666666"></td>
		</tr>
		<tr>
		<td colspan="2">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td width="90%" height="25" class="RightDashBoardHeader"><fmtLockedCogReport:message key="LBL_LOCKED_INVENTORY_UNIT_PRICING"/>	</td>
				<td  height="25" class="RightDashBoardHeader" align="right">
				<fmtLockedCogReport:message key="LBL_HELP" var="varHelp"/>
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		       </td>
		      </tr>
		       </table>
		  </td>
		</tr>
		<tr><td class="Line" height="1" colspan="2"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td class="RightText" align="right" HEIGHT="24">&nbsp;<font color="red">*</font><fmtLockedCogReport:message key="LBL_INVENTORY_LIST"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="inventoryListID"   SFFormName='frmCogsForm' SFSeletedValue="inventoryListID"  defaultValue= "[Choose One]"	
						tabIndex="1"  SFValue="inventoryList" codeId="LOCKID" codeName="LOCKDT"/>					
						
			</td>
		</tr>		
		<tr><td class="LLine" height="1" colspan="2"></td></tr>
		<tr class="Shade">
			<td class="RightText" align="right" HEIGHT="24">&nbsp;<fmtLockedCogReport:message key="LBL_PART_NUMBER"/>:</td>
			<td>&nbsp;
			<html:text property="pnum" size="57"  styleClass="InputArea"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/>	            				
			&nbsp;&nbsp;<html:select property ="pnumSuffix"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" >
							<html:option value="0" ><fmtLockedCogReport:message key="LBL_CHOOSEONE"/></html:option>
							<html:option value="LIT" ><fmtLockedCogReport:message key="LBL_LITERAL"/></html:option>
							<html:option value="LIKEPRE" ><fmtLockedCogReport:message key="LBL_LIKE_PREFIX"/></html:option>
							<html:option value="LIKESUF" ><fmtLockedCogReport:message key="LBL_LIKE_SUFFIX"/></html:option>
						</html:select>
			</td>
		</tr>
		<tr><td class="LLine" height="1" colspan="2"></td></tr>			
		<tr>
			<td class="RightText" align="right" HEIGHT="24">&nbsp;<fmtLockedCogReport:message key="LBL_PROJECT_LIST"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="projectListID"  SFFormName='frmCogsForm' SFSeletedValue="projectListID"  defaultValue= "[Choose One]"	
						tabIndex="1"  SFValue="projectList" codeId="ID" codeName="NAME"/>
			&nbsp;
			<fmtLockedCogReport:message key="BTN_GO" var="VarGo"/>
			<gmjsp:button value="&nbsp;&nbsp;${VarGo}&nbsp;&nbsp;" name="Btn_Go" gmClass="button" onClick="javascript:fnGo()" tabindex="3" buttonType="Load" />
			</td>
		</tr>
		<tr><td class="Line" colspan="2"></td></tr>
		<tr>
			<td colspan="2" align="center">
				Report as of: <%=strDate%>   By:<%=strUserName%><BR>				
			</td>
		</tr>
		<tr>			
			<td colspan="2">
					<display:table name="requestScope.frmCogsForm.rptSummary" class="its"
						id="currentRowObject" requestURI="/gmLockedCogsReport.do?" style="height:35" export="true">
                        <display:setProperty name="export.excel.filename" value="Locked Inv Unit Price.xls" />
							<fmtLockedCogReport:message key="LBL_PART_NUMBER" var="varPartNumber"/><display:column property="ID" title="${varPartNumber}" style="width:100" sortable="true"/>							
							<fmtLockedCogReport:message key="LBL_PART_DESCRIPTION" var="VarPartDescription"/><display:column property="DESCRIPTION"  escapeXml="true" title="${VarPartDescription}"  style="width:350" sortable="true"/>
							<fmtLockedCogReport:message key="LBL_TYPE" var="varType"/><display:column property="ITYPE" title="${varType}" style="width:100" sortable="true"/>
							<fmtLockedCogReport:message key="LBL_COST" var="varCost"/><display:column property="COST" title="${varCost}" style="width:80" class="alignright" format="{0,number,#,###,###.00}" sortable="true"/>
							<fmtLockedCogReport:message key="LBL_POSTED_DATE" var="varPostedDate"/><display:column property="POSTDATE" title="${varPostedDate}" style="width:130"  sortable="true" format="<%=strDateFmt%>"/>							
					</display:table>  
			</td>			     	
		</tr>
	</table>
		
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>


</html>