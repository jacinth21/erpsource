<%
/**********************************************************************************
 * File		 		: GmFieldSalesConAdj.jsp
 * Desc		 		: This screen is used for the asset attribute Report
 * Version	 		: 1.0
 * author			: 
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<% 

String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
String strProdmgmtJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
String strWikiTitle = GmCommonClass.getWikiTitle("CONSIGNMENT_ADJUSTMENT");
%>
<%@ taglib prefix="fmtFieldSalesConAdj" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmFieldSalesConAdj.jsp -->
<fmtFieldSalesConAdj:setLocale value="<%=strLocale%>"/>
<fmtFieldSalesConAdj:setBundle basename="properties.labels.accounts.GmFieldSalesConAdj"/>

<HTML>
<HEAD>
<TITLE> Globus Medical: Consignment Adjustment </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmFieldSalesConAdj.js"></script>
<script language="JavaScript" src="<%=strProdmgmtJsPath%>/GmRule.js"></script>
<script>
var lblAdjType = '<fmtFieldSalesConAdj:message key="LBL_ADJUSTMENT_TYPE"/>';
var lblConsTo = '<fmtFieldSalesConAdj:message key="LBL_CONSIGN_TO"/>';
var lblSetId = '<fmtFieldSalesConAdj:message key="LBL_SET_ID"/>';
var lblPartNum = '<fmtFieldSalesConAdj:message key="LBL_PART_NUM"/>';
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnPageLoad();">
<html:form action="/gmFieldSalesConAdj.do"> 
<input type="hidden" name="strOpt" value="" />
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="1">&nbsp;<fmtFieldSalesConAdj:message key="LBL_CONSIGNMETN_ADJUSTMENT"/> </td>
			<td height="25" class="RightDashBoardHeader" colspan="1">
			<fmtFieldSalesConAdj:message key="LBL_HELP" var="varHelp"/>
				<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr><td colspan="2" height="1" class="LLine"></td></tr> 
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td  class="RightTableCaption" align="right" HEIGHT="24" colspan="1"><font color="red">*</font> <fmtFieldSalesConAdj:message key="LBL_ADJUSTMENT_TYPE"/>:&nbsp;</td>
			<td colspan="1"><gmjsp:dropdown controlName="adjustmentType" SFFormName="frmFieldSalesConAdj" SFSeletedValue="adjustmentType" 
								SFValue="alAjustmentType" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" /></td>						
		</tr>
		<tr><td colspan="2" height="1" class="LLine"></td></tr>
		<tr class="shade" >
			<td HEIGHT="24" class="RightTableCaption" align="right" colspan="1"><font color="red">*</font> <fmtFieldSalesConAdj:message key="LBL_CONSIGN_TO"/>:&nbsp;</td>
			<td  align ="left" colspan="1"><gmjsp:dropdown controlName="consignTo" SFFormName="frmFieldSalesConAdj" SFSeletedValue="consignTo" 
								SFValue="alConsignTo" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" />
			</td>
		</tr>
		<tr><td colspan="2" height="1" class="LLine"></td></tr> 
		<tr>					            
			<td class="RightTableCaption" align="right" HEIGHT="24" colspan="1"><fmtFieldSalesConAdj:message key="LBL_SET_ID"/>:&nbsp;</td>	
			<td colspan="1"><gmjsp:dropdown controlName="setID" SFFormName="frmFieldSalesConAdj" SFSeletedValue="setID" 
								SFValue="alSetList" width="400" codeId="ID" codeName="IDNAME" defaultValue= "[Choose one]" /></td>												
		</tr>
		<tr><td colspan="2" height="1" class="LLine"></td></tr> 
		<tr class="Shade">					            
			<td class="RightTableCaption" align="right" HEIGHT="24" colspan="1" ><fmtFieldSalesConAdj:message key="LBL_TAG"/> #:&nbsp;</td>	
			<td colspan="1"><html:text property="tagID"  size="15" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>												
		</tr>
		<tr><td colspan="2" height="1" class="LLine"></td></tr> 
		<tr>					            
			<td class="RightTableCaption" align="right" HEIGHT="24" colspan="1"><fmtFieldSalesConAdj:message key="LBL_PART"/> #:&nbsp;</td>	
			<td colspan="1"><html:textarea property="pnumStr" cols="60" rows="6" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" /></td>
		</tr>
		
		<tr><td colspan="2" height="1" class="LLine"></td></tr> 
		<tr class="Shade">					            
			<td class="RightTableCaption" align="right" HEIGHT="24" colspan="1"><font color="red">*</font> <fmtFieldSalesConAdj:message key="LBL_COMMENTS"/>:&nbsp;</td>	
			<td colspan="1"><html:textarea property="comments" cols="60" rows="6" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>												
		</tr>
		<tr><td colspan="2" height="1" class="LLine"></td></tr>
		<tr>
			<fmtFieldSalesConAdj:message key="BTN_SUBMIT" var="varSubmit"/>
			<fmtFieldSalesConAdj:message key="BTN_RESET" var="varReset"/>
			<td align="center" colspan="2" height="35"><gmjsp:button name="btn_submit" value="${varSubmit}"  gmClass="button" onClick="fnSubmit();" buttonType="Save" />
			&nbsp;&nbsp;<gmjsp:button name="btn_close" value="${varReset}"  gmClass="button" onClick="fnReset();" buttonType="Save" /> </td>
		</tr>
	</table>
		
 </html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>

