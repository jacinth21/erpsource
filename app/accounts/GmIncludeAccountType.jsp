 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmIncludeAccountType.jsp
 * Desc		 		: This screen is inclluded in the GmInvoiceList.jsp
 * Version	 		: 1.0
 * author			: Angela Xiang
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@ taglib prefix="fmtIncludeAccountType" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmIncludeAccountType.jsp -->
<fmtIncludeAccountType:setLocale value="<%=strLocale%>"/>
<fmtIncludeAccountType:setBundle basename="properties.labels.accounts.GmIncludeAccountType"/>
<%
	response.setCharacterEncoding("UTF-8");
	String strInvSource = GmCommonClass.parseNull((String)request.getAttribute("INVSOURCE")) == "" ? "50255" : GmCommonClass.parseNull((String)request.getAttribute("INVSOURCE"));
	String strDisplayfl = GmCommonClass.parseNull(request.getParameter("DISPLAYFL")) == "" ? "N" :GmCommonClass.parseNull(request.getParameter("DISPLAYFL"));
	String strDispAccfl = GmCommonClass.parseNull(request.getParameter("DISPACCFL")) == "" ? "N" :GmCommonClass.parseNull(request.getParameter("DISPACCFL"));
	String strCollectorfl = GmCommonClass.parseNull(request.getParameter("COLLECTORFL")) == "" ? "N" :GmCommonClass.parseNull(request.getParameter("COLLECTORFL"));
	String strRepAcctLblfl = GmCommonClass.parseNull(request.getParameter("REPACCTLABELFL")) == "" ? "N" :GmCommonClass.parseNull(request.getParameter("REPACCTLABELFL"));
	String strIncAllFl = GmCommonClass.parseNull(request.getParameter("INC_ALL_FL")) == "" ? "N" :GmCommonClass.parseNull(request.getParameter("INC_ALL_FL"));
	ArrayList alCompCurrency = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCOMPANYCURRENCY"));
	String strCurrSign = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_SYMB"));
	String strAccountCurrency = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_ID"));
	HashMap hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strCompanyCurrId = (String) hmCurrency.get("TXNCURRENCYID");
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.accounts.GmIncludeAccountType", strSessCompanyLocale);
	String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
	String strAccId = GmCommonClass.parseNull((String)request.getAttribute("hAccId"));
	String strAccountName = GmCommonClass.parseNull((String)request.getAttribute("SEARCH_ACC_NAME"));
	ArrayList alType = (ArrayList)request.getAttribute("ALTYPE");
	//For GM Japan , Including ALL option in Type Dropdown , ability to view the reports of ALL customer types (Company Customers,Company Dealer)(PMT-14375)
	if(strIncAllFl.equals("Y")){
	  	HashMap hmType = new HashMap();
		hmType.put("CODENM", "ALL");
		hmType.put("CODEID", "ALL");
		alType.add(hmType);
	}
	
	log.debug(" Type " + strInvSource + " Acc Id  "+ strAccId +" alType " + alType);
	String strXMLString = GmCommonClass.parseNull((String)request.getAttribute("XMLSTRING"));
	log.debug(" Length of XML String " + strXMLString.length());
	//log.debug(" XML String " + strXMLString);	
	 String strAcctLbl = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ACCOUNTS"));
	if(strRepAcctLblfl.equalsIgnoreCase("Y")) {
	   strAcctLbl = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_REP_ACCOUNT"));
	}
	
	if(strInvSource.equals("26240213")) //26240213:Company Dealers
	{
	  strAcctLbl=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_DEALER_NAME"));
	}
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Include Account Type </TITLE>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmIncludeAccountType.js"></script>

<script>

var xmlhttp;
var boolChange = false;
var collectorFl = '<%=strCollectorfl%>';
var restext;
var invSource = '<%=strInvSource%>';
var compCurrSign = '<%=strCurrSign%>';
var defaultCurrency = '<%=strCompanyCurrId%>';
var repLblFl='<%=strRepAcctLblfl%>';
var incAllFl='<%=strIncAllFl%>';
var lblDealerName = '<fmtIncludeAccountType:message key="LBL_DEALER_NAME"/>';
var lblRepAccountName = '<fmtIncludeAccountType:message key="LBL_REP_ACCOUNT"/>';
var lblAccountName = '<fmtIncludeAccountType:message key="LBL_ACCOUNTS"/>';

</script>
</HEAD>
<BODY leftmargin="20" topmargin="10">
<table width="50%" border="0">
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr id="processimg"   style="display:none;">
		<td align="center" colspan="4"><span><IMG border=0 height="20" width="20" src="<%=strImagePath%>/process.gif"></img></span><font color="blue"><fmtIncludeAccountType:message key="LBL_LOADING"/></font></td>
		</tr>
		<tr>
			<fmtIncludeAccountType:message key="LBL_TYPE" var="varType"/>
			<td height="25" class="RightTableCaption" align="right">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varType}:" td="false"/></td>
			<td align="left">&nbsp;<gmjsp:dropdown controlName="Cbo_InvSource"   seletedValue="<%=strInvSource%>" onChange="fnChangeAccSource(this.value,true);"
					value="<%=alType%>" codeId = "CODEID"  codeName = "CODENM"  />	
			</td>
			<td height="25" id="td_nm_id" class="RightTableCaption" align="right">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="<%=strAcctLbl%>" td="false"/></td>
			<td align="left"><jsp:include page="/common/GmAutoCompleteInclude.jsp"  flush="true">
										<jsp:param name="CONTROL_NAME" value="Cbo_AccId" />
										<jsp:param name="METHOD_LOAD" value="loadARAccountList" />
										<jsp:param name="WIDTH" value="300" />
										<jsp:param name="CSS_CLASS" value="search" />
										<jsp:param name="CONTROL_NM_VALUE" value="<%=strAccountName %>" /> 
										<jsp:param name="CONTROL_ID_VALUE" value="<%=strAccId %>" />
										<jsp:param name="SHOW_DATA" value="100" />
										<jsp:param name="AUTO_RELOAD" value="fnloadAcc();" />	
										<jsp:param name="DYNAMIC_OBJ" value="#Cbo_InvSource^" />
									</jsp:include>
			</td>
		<!--  </tr>-->
		<%if(strDisplayfl.equalsIgnoreCase("Y")) {%>
		
		<tr>
			<fmtIncludeAccountType:message key="LBL_ACCOUNT_ID" var="varAccountId"/>
			<td class="RightTableCaption" HEIGHT="24" align="right"> &nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varAccountId}:" td="false"/></td>
			<td class="RightText" align="left">&nbsp;<input type="text" size="8" value="<%=strAccId %>" name="Txt_AccId" class="InputArea"
			onFocus="changeBgColor(this,'#AACCE8');" onchange="fnAcIdBlur(this);"></td>
			<%if(strCollectorfl.equalsIgnoreCase("Y")) {%>
			
			<td class="RightTableCaption" HEIGHT="24" align="right">&nbsp;<fmtIncludeAccountType:message key="LBL_COLLECTOR_ID" var="varCollectorId"/><gmjsp:label type="RegularText"  SFLblControlName="${varCollectorId}:" td="false"/></td>
			<td class="RightText"><span id="spanCollector"></span></td>
			<%}else{ %>
			<td class="RightTableCaption" HEIGHT="24" align="right">&nbsp; </td>
			<td class="RightText">&nbsp;</td>
			<%} %>
		</tr>
		<%}else if(strDispAccfl.equalsIgnoreCase("Y")) {%>
	<td class="RightText" align="left">&nbsp;<input type="text" size="8" value="<%=strAccId %>" name="Txt_AccId" class="InputArea"
			onFocus="changeBgColor(this,'#AACCE8');" onchange="fnAcIdBlur(this);">
						<span id="spnCurrLbl" name="spnCurrLbl">
			<gmjsp:dropdown controlName="Cbo_Comp_Curr"  seletedValue="<%=strAccountCurrency%>" value="<%=alCompCurrency%>" codeId = "ID"  codeName = "NAMEALT" optionId="NAME"/>&nbsp;&nbsp;&nbsp;</span></td>
			
			<%if(strCollectorfl.equalsIgnoreCase("Y")) {%>
			<tr>
			
			<td class="RightTableCaption" HEIGHT="24" align="right">&nbsp;<fmtIncludeAccountType:message key="LBL_COLLECTOR_ID" var="varCollectorId"/><gmjsp:label type="RegularText"  SFLblControlName="${varCollectorId}:" td="false"/></td>
			<td class="RightText"><span id="spanCollector"></span></td>
			<%}else{ %>			
			<td class="RightTableCaption" HEIGHT="24" align="right">&nbsp; </td>
			<td class="RightText">&nbsp;</td>
			<%} %>
			</tr>
		<%}else{ %>
<input type="hidden" name="Txt_AccId"/>
<%} %>

</table>





