 <%@page import="com.globus.common.beans.GmCommonClass"%>
<%
/**********************************************************************************
 * File		 		: GmInvoiceEdit.jsp
 * Desc		 		: This screen is used for the Order Maintenance
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtInoviceLayout" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmInvoiceLayout.jsp -->
<fmtInoviceLayout:setLocale value="<%=strLocale%>"/>
<fmtInoviceLayout:setBundle basename="properties.labels.accounts.GmInvoiceLayout"/>
<%
	String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	String strTodaysDate = GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate"));
	
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strScreen = GmCommonClass.parseNull((String)request.getAttribute("hScreen"));
	String strAddBatch = GmCommonClass.parseNull((String)request.getAttribute("hAddBatch"));
	String strBatchID = GmCommonClass.parseNull((String)request.getAttribute("hBatchID"));
	log.debug(" Action inside JSP is " + strAction+ " strScreen "+strScreen);
	String strOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
	String strUpdFl = GmCommonClass.parseNull((String) request.getAttribute("UPDFL"));
	String strInvUpd = GmCommonClass.parseNull((String) request.getAttribute("INVUPDFL"));
	log.debug(" strInvUpd " + strInvUpd);
	String strCboAction = GmCommonClass.parseZero((String) request.getAttribute("hCboAction"));
	String strCboInvLayout = GmCommonClass.parseZero((String) request.getAttribute("hCboInvLayout"));
	String strBatchSuccessMsg = GmCommonClass.parseNull((String)request.getAttribute("hSuccessMsg")); 
	String strEmailReqFl = GmCommonClass.parseNull((String)request.getAttribute("hEmailReqFl")); 
	// to set the default choose action value
	
	if (strAction == null)
	{
		strAction = (String)session.getAttribute("hAction");
	}
	strAction = (strAction == null)?"Load":strAction;

	HashMap hmReturn = new HashMap();

	ArrayList alOrderNums = new ArrayList();
	ArrayList alInvoiceBatch = new ArrayList();
	ArrayList alInvLayout = new ArrayList();
	HashMap hmCartDetails = new HashMap();
	HashMap hmConstructs = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmOrderDetails = new HashMap();
	HashMap hmShipDetails = new HashMap();

	String strShade = "";

	String strInvNum = "";
	String strAccId= "";
	String strDistRepNm = "";
	String strBillAdd = "";
	java.sql.Date dtInvDate = null;
	java.sql.Date dtDueDate = null;
	String strPO = "";
	String strOrdId = "";

	String strPartNum = "";
	String strDesc = "";
	String strPrice = "";
	String strQty = "";
	String strItemOrdId = "";
	String strPartNums = "";
	String strControlNum = "";
	String strPayNm = "";
	String strPay = "";
	String strDueDate = "";
	String strStatusFl = "";
	String strCallFlag = "";
	String strInvType = "";
	String strMode = "";
	String strControlName = "";
	String strItemType = "";
	String strElectEmailFl = "";
	String strElectVersion = "";
	String strComments ="";

	String strConFlag = "";
	String strConstructId = "";
	String strConstructNm = "";
	String strConstructValue = "";
	String strValidAddressFl = "";
	String strTaxCountryFl = "";
	String strTaxDate = "";
	String strUnitPrice = "";
	String strAdjVal = "";
	String strAdjCode = "";
	String strGrandTotal = ""; // Total Before Adjustment
	String strAdjGrandTotal = ""; // Total After Adjustment
	String strAdjTotal = ""; // For sub total
	String strInvLayout = "";
	double dbGrandTotal = 0.0;
	double dbGrandAdjTotal = 0.0;
	
	if (hmReturn != null)
	{
		hmOrderDetails = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("ORDERDETAILS"));
		alOrderNums = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ORDERNUMS"));
		hmConstructs = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("CONSTRUCTDETAILS"));

		alInvoiceBatch = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("alInvoiceBatch"));
		alInvLayout = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("alInvLayout"));
		hmCartDetails = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("CARTDETAILS"));
		strInvNum = GmCommonClass.parseNull((String)hmOrderDetails.get("INV"));
		strAccId = GmCommonClass.parseNull((String)hmOrderDetails.get("ACCID"));
		strPO = GmCommonClass.parseNull((String)hmOrderDetails.get("PO"));
		strBillAdd = GmCommonClass.parseNull((String)hmOrderDetails.get("BILLADD"));
		strDistRepNm = GmCommonClass.parseNull((String)hmOrderDetails.get("REPDISTNM"));
		dtInvDate = (java.sql.Date)hmOrderDetails.get("INVDT");
		dtDueDate = (java.sql.Date)hmOrderDetails.get("DUEDT");
		strPayNm = GmCommonClass.parseNull((String)hmOrderDetails.get("PAYNM"));
		strPay = GmCommonClass.parseNull((String)hmOrderDetails.get("PAY"));
		strStatusFl = GmCommonClass.parseNull((String)hmOrderDetails.get("SFL"));
		strCallFlag = GmCommonClass.parseNull((String)hmOrderDetails.get("CALL_FLAG"));
		strInvType = GmCommonClass.parseNull((String)hmOrderDetails.get("INVTYPE"));
		strPayNm = strPayNm.equals("")?"Net 30":strPayNm;
		strElectEmailFl = GmCommonClass.parseNull((String)hmOrderDetails.get("EMAILREQ"));
		strElectVersion = GmCommonClass.parseNull((String)hmOrderDetails.get("EVERSION"));
		strComments = GmCommonClass.parseNull((String)hmOrderDetails.get("COMMENTS"));
		strValidAddressFl = GmCommonClass.parseNull((String)hmOrderDetails.get("VALID_ADD_FL"));
		strTaxCountryFl = GmCommonClass.parseNull((String)hmOrderDetails.get("TAX_COUNTRY_FL"));
		strTaxDate = GmCommonClass.parseNull((String) hmOrderDetails.get("TAX_START_DATE"));
		//strGrandTotal = GmCommonClass.parseNull((String) hmOrderDetails.get("TOTAL_BEF_ADJ"));
		//strAdjGrandTotal = GmCommonClass.parseNull((String) hmOrderDetails.get("TOTAL_AFT_ADJ"));
		//dbGrandTotal =  GmCommonClass.parseDouble(Double.parseDouble((String)hmOrderDetails.get("TOTAL_BEF_ADJ")));
		//dbGrandAdjTotal = GmCommonClass.parseDouble(Double.parseDouble((String)hmOrderDetails.get("TOTAL_AFT_ADJ")));
		strInvLayout = GmCommonClass.parseNull((String) hmOrderDetails.get("INVOICELAYOUT"));
	}

	if (strOpt.equals("VIEW") || strOpt.equals("LOAD"))
	{
		strInvNum = "TO - BE";
	}
	int intSize = 0;
	ArrayList alLoop = null;
	HashMap hmTemp = null;
	HashMap hmLoop = null;
	ArrayList alConsLoop = null;
	HashMap hmConsLoop = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Invoice Edit </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script type="text/javascript" src="<%=strAccountJsPath%>/GmInvoiceEdit.js"></script>


<script language = "javascript">
var todaysDate = '<%=strTodaysDate%>';
var dateFmt = '<%=strApplDateFmt%>';
var validAddFl = '<%=strValidAddressFl%>';
var taxCountryFl = '<%=strTaxCountryFl%>';
var taxDate = '<%=strTaxDate%>';

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnActionChange();">
<FORM name="frmInvoice" method="post" action = "<%=strServletPath%>/GmInvoiceServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hPO" value="<%=strPO%>">
<input type="hidden" name="hInv" value="<%=strInvNum%>">
<input type="hidden" name="hAccId" value="<%=strAccId%>">
<input type="hidden" name="hPay" value="<%=strPay%>">
<input type="hidden" name="hStr" value="">
<input type="hidden" name="hCustomerPO" value="">
<input type="hidden" name="hReasonForChange" value="">
<input type="hidden" name="hInvType" value="">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="strOpt" value="">
<input type="hidden" name="hEmailFl" value="<%=strElectEmailFl%>">
<input type="hidden" name="hEmailVersion" value="<%=strElectVersion %>">
<input type="hidden" name="hAddBatch" value="<%=strAddBatch %>">
<input type="hidden" name="hBatchID" value="<%=strBatchID %>">
	<table border="0" class="DtTable1050" cellspacing="0" cellpadding="0">
	<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
					<fmtInoviceLayout:message key="LBL_INVOICE_LAYOUT" var="varInvoiceLayout"/>
						<td height="25" class="RightDashBoardHeader"><gmjsp:label type="RegularText"  SFLblControlName="${varInvoiceLayout}" td="false"/></td>
						<fmtInoviceLayout:message key="LBL_HELP" var="varHelp"/>
						<td height="25" class="RightDashBoardHeader"><img align="right"
						id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
						title='${varHelp}' width='16' height='16'
						onClick="javascript:fnHelp('<%=strWikiPath%>','INVOICE_LAYOUT');" />
						</td>
					</tr>
		<tr>
			<td width="800" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr class="ShadeRightTableCaption">
								<fmtInoviceLayout:message key="LBL_BILL_TO" var="varBillTo"/>
									<td height="25" align="center" width="450">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varBillTo}" td="false"/></td>
									<td bgcolor="#666666" width="1" rowspan="7"></td>
									<fmtInoviceLayout:message key="LBL_CUSTOMER_PO" var="varCustomerPO"/>
									<td height="25" align="center">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varCustomerPO} #" td="false"/></td>
									<td bgcolor="#666666" width="1" rowspan="7"></td>
									<fmtInoviceLayout:message key="LBL_INVOCIE_DATE" var="varInvocieDate"/>
									<fmtInoviceLayout:message key="LBL_TERMS" var="varTerms"/>
									<td width="150" align="center">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varInvocieDate}" td="false"/></td>
									<td width="150" align="center">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varTerms}" td="false"/></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="6"></td>
								</tr>
								<tr>
									<td class="RightText" rowspan="5" valign="top">&nbsp;<%=strBillAdd%></td>
									<td class="RightText" align="center">&nbsp;<%=strPO%></td>
									<td height="25" class="RightText" align="center"><%=GmCommonClass.getStringFromDate(dtInvDate,strApplDateFmt)%></td>
									<td class="RightText" align="center">&nbsp;<%=strPayNm%></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
								</tr>
								<tr class="ShadeRightTableCaption">
								<fmtInoviceLayout:message key="LBL_INVOICE" var="varInovice"/>
									<td class="RightText" align="center" height="20"><gmjsp:label type="RegularText"  SFLblControlName="${varInovice} #" td="false"/></td>
									<fmtInoviceLayout:message key="LBL_DIST_REP" var="varDistReo"/>
									<fmtInoviceLayout:message key="LBL_ACCOUNT_ID" var="varAccountId"/>
									<td align="center"><gmjsp:label type="RegularText"  SFLblControlName="${varDistReo}" td="false"/></td>
									<td align="center"><gmjsp:label type="RegularText"  SFLblControlName="${varAccountId}" td="false"/></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
									 <td bgcolor="#666666" height="1" colspan="2"></td>
								</tr>
								<tr>
									<td class="RightText" rowspan="3" align="center">&nbsp;<%=strInvNum%></td>
									<td align="center" class="RightText">&nbsp;<%=strDistRepNm%><Br></td>
									<td align="center" height="25" class="RightText">&nbsp;<%=strAccId %></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" height="1" bgcolor="#666666"></td>
					</tr>
					<tr>
						<td align="center" colspan="2" valign="top">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr class="ShadeRightTableCaption">
									<td width="120" height="25">&nbsp;<fmtInoviceLayout:message key="LBL_ORDER" var="varOrder"/><gmjsp:label type="RegularText"  SFLblControlName="${varOrder} #" td="false"/></td>
									<td height="25" align="center" >&nbsp;<fmtInoviceLayout:message key="LBL_ORDER_DATE" var="varOrderDate"/><gmjsp:label type="RegularText"  SFLblControlName="${varOrderDate}" td="false"/></td>
									<td width="70" align="left" height="25">&nbsp;<fmtInoviceLayout:message key="LBL_PART" var="varPart"/><gmjsp:label type="RegularText"  SFLblControlName="${varPart} #" td="false"/></td>
									<td width="300" align="left">&nbsp;<fmtInoviceLayout:message key="LBL_PART_DESCRIPTION" var="varPartDescription"/><gmjsp:label type="RegularText"  SFLblControlName="${varPartDescription}" td="false"/></td>
									<td align="center" width="70"><fmtInoviceLayout:message key="LBL_CONTROL_NUMBER" var="varControlNumber"/><gmjsp:label type="RegularText"  SFLblControlName="${varControlNumber}" td="false"/></td>
									<td align="center" width="80"><fmtInoviceLayout:message key="LBL_ORDER_QTY" var="varOrerQty"/><gmjsp:label type="RegularText"  SFLblControlName="${varOrerQty}" td="false"/></td>
									<td align="center" width="80"><fmtInoviceLayout:message key="LBL_UNIT_PRICE" var="varUnitPrice"/><gmjsp:label type="RegularText"  SFLblControlName="${varUnitPriceAdj}" td="false"/></td>
									<td align="center" width="80"><fmtInoviceLayout:message key="LBL_UNIT_PRICE_ADJ" var="varUnitPriceAdj"/><gmjsp:label type="RegularText"  SFLblControlName="${varUnitPriceAdj}" td="false"/></td>
									<td align="left" width="80"><fmtInoviceLayout:message key="LBL_ADJ_CODE" var="varAdjCode"/><gmjsp:label type="RegularText"  SFLblControlName="${varAdjCode}" td="false"/></td>
									<td align="center" width="80"><fmtInoviceLayout:message key="LBL_NET_UNIT_PRICE" var="varUnitPrice"/><gmjsp:label type="RegularText"  SFLblControlName="${varUnitPrice}" td="false"/></td>
									<td align="center" width="80"><fmtInoviceLayout:message key="LBL_SALES_TAX_RATE" var="varSalesTaxRate"/><gmjsp:label type="RegularText"  SFLblControlName="${varSalesTaxRate}" td="false"/></td>
									<td align="center" width="80"><fmtInoviceLayout:message key="LBL_SALES_TAX_VALUE" var="varSalesTaxValue"/><gmjsp:label type="RegularText"  SFLblControlName="${varSalesTaxValue}" td="false"/></td>
									<td align="center" width="100"><fmtInoviceLayout:message key="LBL_TOTAL_PRICE" var="varTotaPrice"/><gmjsp:label type="RegularText"  SFLblControlName="${varTotaPrice}" td="false"/></td>
								</tr>
								<tr>
									<td colspan="13" height="1" bgcolor="#666666"></td>
								</tr>
<%
					int intLoop = 0;
			  		intSize = alOrderNums.size();
					alLoop = new ArrayList();
					String strItems = "";
					java.sql.Date dtOrdDate=null;
					int intQty = 0;
					double dbItemTotal = 0.0;
					String strItemTotal = "";
					double dbTotal = 0.0;
					String strTotal = "";
					String strShipCost = "";
					String strVat="";
					String strVatAmount="";
					double dbVat=0.0;
					double dbVatAmount=0.0;
					double dbVatSubTotal=0.0;
					double dblShip = 0.0;
					//double dbGrandTotal = 0.0;
					String strTaxCost = "";
					//String strGrandTotal = "";

					int intTotalLoop = 0;
					int k = 0;
					int intConLoop = 0;

			  		for (int i=0;i<intSize;i++)
			  		{
				  		hmTemp = (HashMap)alOrderNums.get(i);
						strOrdId = (String)hmTemp.get("ID");

						alLoop = (ArrayList)hmCartDetails.get(strOrdId);
						intLoop = alLoop.size();
						intTotalLoop = intTotalLoop + intLoop;
						
						alConsLoop = (ArrayList)hmConstructs.get(strOrdId);
						intConLoop = alConsLoop.size();

						if (intConLoop > 0)
						{
							for ( int j=0;j<intConLoop;j++)
							{
								hmConsLoop = (HashMap)alConsLoop.get(j);
								strConstructId = (String)hmConsLoop.get("CONID");
								strConstructNm = (String)hmConsLoop.get("CONNM");
								strConstructValue = (String)hmConsLoop.get("CVALUE");
								dbItemTotal = Double.parseDouble(strConstructValue);
								dbTotal = dbTotal + dbItemTotal;
%>
								<tr>
									<td class="RightText" height="20">&nbsp;<%=strOrdId%></td>
									<td class="RightText">&nbsp;</td>
									<td class="RightText" align="center">&nbsp;<%=strConstructId%></td>
									<td class="RightText">&nbsp;<%=strConstructNm%></td>
									<td colspan="3">&nbsp;</td>
									<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strConstructValue)%>&nbsp;&nbsp;</td>
								</tr>
<%
							}
						}
												
						for ( int j=0;j<intLoop;j++)
						{

							hmLoop = (HashMap)alLoop.get(j);
							dtOrdDate = (java.sql.Date)hmLoop.get("ODT");
							strPartNum = (String)hmLoop.get("ID");
							strDesc = (String)hmLoop.get("PDESC");
							strPrice = (String)hmLoop.get("PRICE");
							strQty = (String)hmLoop.get("QTY");
							strControlNum = (String)hmLoop.get("CNUM");
							strItemType = GmCommonClass.parseNull((String)hmLoop.get("TYPE"));
							strConFlag = GmCommonClass.parseNull((String)hmLoop.get("CONFL"));
							strVat=GmCommonClass.parseNull((String)hmLoop.get("VAT"));
							strTaxCost = GmCommonClass.parseZero((String)hmLoop.get("TAX_COST"));
							strUnitPrice = GmCommonClass.parseZero((String)hmLoop.get("UNITPRICE"));
							strAdjVal = GmCommonClass.parseZero((String)hmLoop.get("ADJVAL"));
							strAdjCode = GmCommonClass.parseNull((String)hmLoop.get("ADJCODE"));
							if (strConFlag.equals(""))
							{
								intQty = Integer.parseInt(strQty);
								dbItemTotal = Double.parseDouble(strPrice);
								if(!strVat.equals("")){
									dbVat=Double.parseDouble(strVat);
								}
								dbItemTotal = GmCommonClass.roundDigit((intQty * dbItemTotal), 2); // Multiply by Qty
								if(strAction.equals("PRINT") || strAction.equals("PAY"))
								{
								if(!strVat.equals("")){
									dbVatAmount= Double.parseDouble(strTaxCost);
								}
								strVatAmount=""+dbVatAmount;
								//dbItemTotal+=dbVatAmount;
								dbVatSubTotal+=dbVatAmount;
								}
								strItemTotal = ""+dbItemTotal;
								dbTotal = dbTotal + dbItemTotal;
								strTotal = ""+dbTotal;
							}
							else
							{
								strPrice = "CON";
								strItemTotal = "0.0"; 
							}
							strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
%>
								<tr >
<%
							if ( j == 0)
							{	/* To print the order in for the list link */
%>
									<td class="RightText" rowspan="<%=intLoop%>" height="20">
<%						// Check if the Orded is a Return or Duplicate Order -- redirect based on the type 						
						if ((strOrdId.charAt(strOrdId.length()-1)== 'R') ||(strOrdId.charAt(strOrdId.length()-1)== 'D') )
						{%>
						&nbsp;<a href="javascript:fnPrintCreditMemo('<%=strOrdId%>');"
<%						}else if (strOrdId.charAt(strOrdId.length()-1)== 'A') 
						{%>
						&nbsp;<a href="javascript:fnPrintCashAdjustMemo('<%=strOrdId%>');"
<%						}else
						{%>
						&nbsp;<a href="javascript:fnPrintPack('<%=strOrdId%>');"
<%						}%>
						class="RightText"><%=strOrdId%></a> </td>
									<td class="RightText" rowspan="<%=intLoop%>" height="20">&nbsp;<%=GmCommonClass.getStringFromDate(dtOrdDate,strApplDateFmt)%></td>
<%							
							}
%>
									<td class="RightText" align="left" height="20">&nbsp;<%=strPartNum%></td>
									<td class="RightText" align="left">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
									<td class="RightText" height="20">&nbsp;<%=strControlNum%></td>
									<td class="RightText" align="center">&nbsp;<%=GmCommonClass.getRedText(strQty)%><input type="hidden" name="h_InvQty<%=k%>" value="<%=strQty%>"></td>
									<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strUnitPrice))%></td>
									<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strAdjVal))%></td>
									<td class="RightText" align="left">&nbsp;<%=strAdjCode%></td>
									<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strPrice))%></td>
									<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strVat))%></td>
									<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strVatAmount))%></td>
									<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strItemTotal))%></td>
									
								</tr>
<%					
							if ( j == intLoop -1)
							{	
								strShipCost = GmCommonClass.parseZero((String)hmLoop.get("SCOST"));
								dblShip = Double.parseDouble(strShipCost);
								String strCountryInvoice = GmCommonClass.parseNull(GmCommonClass.getRuleValue("1","INVOICEFMT"));
								dbTotal = dbTotal + dblShip;
								strTotal = ""+dbTotal;
%>
							<tr>
								<td colspan="3">&nbsp;</td>
								<fmtInoviceLayout:message key="LBL_SHIPPING_CHARGES" var="varShippingCharges"/>
								<td class="RightText" height="24px">&nbsp;<gmjsp:label type="RegularText"  SFLblControlName="${varShippingCharges}" td="false"/></td>
								<td colspan="7">&nbsp;<td>
								<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strShipCost))%></td>
							</tr>
							<tr><td colspan="13" height="1" bgcolor="#cccccc"></td></tr>
<%
							}
						}
			  		}
  strAdjTotal = "" + dbGrandAdjTotal;
  dbGrandTotal = dbGrandTotal + dbVatSubTotal;
  dbGrandAdjTotal = dbGrandAdjTotal + dbVatSubTotal;
  strGrandTotal = "" + dbGrandTotal;
  strAdjGrandTotal = "" + dbGrandAdjTotal;
  strTaxCost = "" + dbVatSubTotal;
%>
									
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
<td colspan="5" align="center" height="30" class="RightTableCaption">

	
		<fmtInoviceLayout:message key="LBL_CHOOSE_INVOICE_LAYOUT"/>:&nbsp;<gmjsp:dropdown controlName="Cbo_inv_layout"
			seletedValue="<%=strCboInvLayout%>" value="<%=alInvLayout%>"
			defaultValue="[Choose one]" codeId="CODEID" codeName="CODENM"/>
			<fmtInoviceLayout:message key="BTN_SUBMIT" var="varSubmit"/>
		&nbsp;&nbsp;<gmjsp:button value="&nbsp;${varSubmit}&nbsp;"
		name="Btn_Submit" gmClass="button" onClick="fnOpenInvoice();" buttonType="Save" />

	</td>
</tr>
			
</table>
<%@ include file="/common/GmFooter.inc" %>

</FORM>

</BODY>

</HTML>
