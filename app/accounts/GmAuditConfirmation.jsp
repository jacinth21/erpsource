<%@ page language="java" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<%@ taglib prefix="fmtAuditConfirmation" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmAuditConfirmation.jsp -->
<fmtAuditConfirmation:setLocale value="<%=strLocale%>"/>
<fmtAuditConfirmation:setBundle basename="properties.labels.accounts.GmAuditConfirmation"/>
<bean:define id="hmResult" name="frmAuditReconcilationForm" property="hmResult" type="java.util.HashMap"></bean:define>
<bean:define id="distId" name="frmAuditReconcilationForm" property="distId" type="java.lang.String"></bean:define>
<bean:define id="auditId" name="frmAuditReconcilationForm" property="auditId" type="java.lang.String"></bean:define>
<%
	String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");	
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strWikiTitle = GmCommonClass.getWikiTitle("CONFIRMATION_SHEET");
	String strHtmlJasperRpt = "";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Confirmation Sheet</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
    table.DtTable1000 {
	width: 1200px;
	}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmAuditConfirmation.js"></script>
<script>
var lblDist = '<fmtAuditConfirmation:message key="LBL_DISTRIBUTOR"/>';
var lblAuditId = '<fmtAuditConfirmation:message key="LBL_AUDIT_ID"/>'

</script>

</HEAD>

<BODY onkeyup="fnEnter();" leftmargin="20" topmargin="10" >
 
<html:form  action="/gmAuditConfirmation.do?"  >
<html:hidden property="strOpt"/>



	<table border="0" class="DtTable700" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" height="25" class="RightDashBoardHeader"><fmtAuditConfirmation:message key="LBL_CONFIRMATION_SHEET"/></td>
			<td  height="25" class="RightDashBoardHeader">
			<fmtAuditConfirmation:message key="LBL_HELP" var="varHelp"/>
				<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr  class="shade" >		
			<td height="30" align ="Right" class="RightTableCaption"><font color="red">*</font><fmtAuditConfirmation:message key="LBL_AUDIT_LIST"/>:&nbsp;</td>
		    <td width="200"><gmjsp:dropdown controlName="auditId" SFFormName="frmAuditReconcilationForm" SFSeletedValue="auditId" 
								SFValue="alAuditList" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" onChange="fnReload()"/></td>
			<td height="30" align ="Right" class="RightTableCaption"><font color="red">*</font><fmtAuditConfirmation:message key="LBL_DISTRIBUTOR"/>:&nbsp;</td>
			<td  align ="left"><gmjsp:dropdown controlName="distId" SFFormName="frmAuditReconcilationForm" SFSeletedValue="distId" 
								SFValue="alDistList" codeId = "DISTID"  codeName = "DISTNAME"  defaultValue= "[Choose One]"  /></td>
		</tr>
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		<tr> 
		    
	  		<td colspan="4" class="RightTableCaption" align ="center">
	  			<fmtAuditConfirmation:message key="BTN_LOAD" var="varLoad"/>
		     	<gmjsp:button value="${varLoad}" gmClass="button"  onClick="fnLoad();" buttonType="Load" />
		    </td>
	  	</tr>      
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		
			<tr>
		<td colspan="4">
		<%
		if(!distId.equals("")&&!distId.equals("0"))
		{
			String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
			GmJasperReport gmJasperReport = new GmJasperReport();
		
			gmJasperReport.setRequest(request);
			gmJasperReport.setResponse(response);
			gmJasperReport.setJasperReportName("/GmAuditConfirmationPDF.jasper");
			gmJasperReport.setHmReportParameters(hmResult);
			gmJasperReport.setReportDataList(null);
			gmJasperReport.setBlDisplayImage(true);
			strHtmlJasperRpt = gmJasperReport.getHtmlReport();
		}
		%>
		<%=strHtmlJasperRpt %>
		</td>
		</tr>
		 <tr>
			<td colspan="4"  height="10" >
				
			</td>
		</tr>
		<tr>
			<td colspan="10" class="RightText" align="Center">
						<BR><b><fmtAuditConfirmation:message key="LBL_CHOOSE_ACTION"/>:</b>&nbsp;<gmjsp:dropdown controlName="cbo_Action" SFFormName="frmAuditReconcilationForm" SFSeletedValue="cbo_Action" 
								SFValue="alAuditAction" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"  />
			<fmtAuditConfirmation:message key="BTN_SUBMIT" var="varSubmit"/>
			<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnChooseAction();" tabindex="25" buttonType="Save" />&nbsp;<BR><BR>
			</td>
		</tr>	
		</table>			
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>