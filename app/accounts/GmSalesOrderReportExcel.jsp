 <%@page import="java.util.Locale"%>
<%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%
/**********************************************************************************
 * File		 		: GmSalesOrderReportExcel.jsp
 * Desc		 		: This screen is used for the Account Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="java.util.Date"%>
<%@ taglib prefix="fmtSalesOrderReportExcelExcel" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmSalesOrderReportExcel.jsp -->
<%
Locale locale = null;
String strLocale = "";

String strJSLocale = "";

String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));


if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtSalesOrderReportExcel:setLocale value="<%=strLocale%>"/>
<fmtSalesOrderReportExcel:setBundle basename="properties.labels.accounts.GmSalesOrderReport"/>
<%
	response.setContentType("application/vnd.ms-excel");
	response.setHeader("Content-disposition","attachment;filename=Report.xls");
%>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	String strCommonPath = GmCommonClass.getString("GMCOMMON");


	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strFrmDate = (String)request.getAttribute("hFrom")==null?"":(String)request.getAttribute("hFrom");
	String strToDate = (String)request.getAttribute("hTo")==null?"":(String)request.getAttribute("hTo");
	String strARCurrSymb = GmCommonClass.parseNull((String)request.getAttribute("ACC_CURR_SYMB"));
	ArrayList alReturn = (ArrayList)hmReturn.get("REPORT");
	int intLength = alReturn.size();

	HashMap hmLoop = new HashMap();

	String strAccId = "";
	String strAccName = "";
	String strOrdId = "";
	Date strOrdDate = null;
	String strCSPerson = "";
	String strPrice = "";
	String strParentOrdId = "";
	String strShade = "";
	String strParentAcctName="";
	String strParentAcctId = "";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Sales Orders Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<style>
.RightTableCaption {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
}

.Line{
	background-color: #676767;
}
TR.ShadeRightTableCaption{
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
	background-color: #cccccc;
}
.RightText {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
}
</style>

</HEAD>

<BODY leftmargin="20" topmargin="10">
	<table border="0" width="550" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
		<tr>
			<td width="1"></td>
			<td width="550" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<td height="25" align="center" colspan="5"><font size="+2"><fmtSalesOrderReportExcel:message key="LBL_REPORT_SALES"/>. <%=strFrmDate%> To <%=strToDate%></font></td>
					<tr><td class="Line" colspan="7" height="1"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="500"><fmtSalesOrderReportExcel:message key="LBL_PARENT_ACCOUNT"/> </td>
						<td width="200" align="center"><fmtSalesOrderReportExcel:message key="LBL_REP_ACCOUNT"/></td>
						<td width="200"><fmtSalesOrderReportExcel:message key="LBL_PACK_SLIP"/></td>
						<td width="200"><fmtSalesOrderReportExcel:message key="LBL_DO_ID"/></td>						
						<td width="220" align="center"><fmtSalesOrderReportExcel:message key="LBL_ORDER_DATE"/></td>
						<td width="140" align="center"><fmtSalesOrderReportExcel:message key="LBL_ORDER_RECEIVED_BY"/></td>
						<td width="100" align="center"><fmtSalesOrderReportExcel:message key="LBL_AMOUNT"/></td>
					</tr>
<%
						if (intLength > 0)
						{
							HashMap hmTempLoop = new HashMap();

							String strNextId = "";
							int intCount = 0;
							String strLine = "";
							String strTotal = "";
							String strAcctTotal = "";
							boolean blFlag = false;
							double dbSales = 0.0;								
							double dbAcctTotal = 0.0;
							double dbTotal = 0.0;

							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alReturn.get(i);
								if (i<intLength-1)
								{
									hmTempLoop = (HashMap)alReturn.get(i+1);
									strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("PARENTACID"));
								}

								strAccId = GmCommonClass.parseNull((String)hmLoop.get("PARENTACID"));
								strAccName = GmCommonClass.parseNull((String)hmLoop.get("NAME"));
								strOrdId = GmCommonClass.parseNull((String)hmLoop.get("ORDID"));
								strOrdDate = (java.util.Date)hmLoop.get("ORDDT");
								strPrice = GmCommonClass.parseZero((String)hmLoop.get("COST"));
								strCSPerson = GmCommonClass.parseZero((String)hmLoop.get("CS"));
								strParentOrdId = GmCommonClass.parseNull((String)hmLoop.get("PARENTORDID"));
								strParentAcctId = GmCommonClass.parseNull((String)hmLoop.get("PARENTACID"));
							    strParentAcctName = GmCommonClass.parseNull((String)hmLoop.get("PARENTACCTNM"));
								
								dbSales = Double.parseDouble(strPrice);
								dbAcctTotal = dbAcctTotal + dbSales;
								dbTotal = dbTotal + dbSales;
								if (strParentAcctId.equals(strNextId))
								{
									intCount++;
									strLine = "<TR><TD colspan=7 height=1 class=Line></TD></TR>";
								}
								else
								{
									strLine = "<TR><TD colspan=7 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
									    strParentAcctName = "";
										strLine = "";
									}
									else
									{
										blFlag = true;
									}
									intCount = 0;
									//
								}
								strAcctTotal = ""+dbAcctTotal;

								if (intCount > 1)
								{
								    strParentAcctName = "";
									strLine = "";
								}
							
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows

								out.print(strLine);
								strShade	= (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
								if (intCount == 1 || blFlag)
								{
%>
					<tr class="Shade">
						<td colspan="6" height="15" class="RightText">&nbsp;<b><%=strParentAcctName%></b></td>
					</tr>
<%
								blFlag = false;
								}
%>
					<tr >
						<td class="RightText" height="20">&nbsp;</td>
						<td class="RightText">&nbsp;<%=strAccName%></td>
						<td class="RightText">&nbsp;<%=strOrdId%></td>
						<td class="RightText" width="200">&nbsp;<%=strParentOrdId%></td>
						<td class="RightText"><%=strOrdDate%></td>
						<td class="RightText">&nbsp;<%=strCSPerson%></td>
						<td class="RightText" align="right"><%=GmCommonClass.getStringWithCommas(strPrice)%></td>
					</tr>
<%					
					if ( intCount == 0 || i == intLength-1)
					{
%>
					<tr>
						<td class="RightText" colspan="6" align="right"></td>
						<td class="Line" height="1"></td>
					</tr>
					<tr>
						<td class="RightText" colspan="6" align="right">&nbsp;<fmtSalesOrderReportExcel:message key="LBL_ACCOUNT_TOTAL"/> =</td>
						<td class="RightText" align="right"><%=strARCurrSymb%><%=GmCommonClass.getStringWithCommas(strAcctTotal)%></td>
					</tr>
<%
						dbAcctTotal = 0.0;					
					}
				strTotal = ""+dbTotal;
				}// end of FOR
%>
					<tr><td colspan="7" height="1" class="Line"></td></tr>
					<tr>
						<td align="right"  class="RightTableCaption" height="20" colspan="6"><fmtSalesOrderReportExcel:message key="LBL_GRAND_TOTAL"/>Grand Total</td>
						<td class="RightTableCaption" align="right"><%=strARCurrSymb%><%=GmCommonClass.getStringWithCommas(strTotal)%></td>
					</tr>
<%
						} // End of IF
						else{
%>
					<tr><td colspan="7" class="Line"></td></tr>					
					<tr><td colspan="7" class="RightTextRed" height="50" align=center><fmtSalesOrderReportExcel:message key="LBL_NO_SALES_PERIOD"/>No Sales for this period ! </td></tr>
<%					
						}
%>
				</table>
			</td>
			<td width="1"></td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>

</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
