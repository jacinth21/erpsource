<!-- 
/**********************************************************************************
 * File		 		: GmAPDHRList.jsp
 * Desc		 		: AP DHR List - report to display DHRs belonging to a PO
 * Version	 		: 1.0
 * author			: Basudev Vidyasankar
************************************************************************************/
-->

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtAPDHRList" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmAPDHRList.jsp -->
<fmtAPDHRList:setLocale value="<%=strLocale%>"/>
<fmtAPDHRList:setBundle basename="properties.labels.accounts.GmAPDHRList"/>
<%
String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
%>
<HTML>
<HEAD>
<TITLE> AP DHR List </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmDHRPayment.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

</HEAD>
<%
String strApplDateFmt = strGCompDateFmt;
String strDateFmt = "{0,date,"+strApplDateFmt+"}";
%>
<BODY>
<html:form action="/gmAPDHRList.do"  >
<html:hidden property="strOpt" value=""/>
<html:hidden property="hPONumber" value=""/>
	<table border="0" cellspacing="0" cellpadding="0" style="WIDTH: 1150px">
		  <tr>
			<td width="1150" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0" style="WIDTH: 1150px">
					<tr>
						<td colspan="2">
							<display:table name="requestScope.frmAPDHRList.ldtResult" class="its" freezeHeader="true" paneSize="100"
								id="currentRowObject" requestURI="/gmAPDHRList.do" style="height:35" decorator="com.globus.accounts.displaytag.beans.DTDHRListWrapper">
								<fmtAPDHRList:message key="LBL_DHR_ID" var="varDhrId"/><display:column property="ID" title="${varDhrId}" sortable="true" style="width:100px"/>
								<fmtAPDHRList:message key="LBL_PACK_SLIP" var="varPackSlip"/><display:column property="PSLIP" title="${varPackSlip}" sortable="true"/>							
								<fmtAPDHRList:message key="LBL_PART_NO" var="varPartNo"/><display:column property="PNUM" title="${varPartNo}." sortable="true"/>
								<fmtAPDHRList:message key="LBL_REC_DATE" var="varRecDate"/><display:column property="CDATE" title="${varRecDate}"  sortable="true" format= "<%=strDateFmt%>"/>
				  				<fmtAPDHRList:message key="LBL_CURR_STAT" var="varCurrStat"/><display:column property="CURSTS" title="${varCurrStat}" sortable="true"/>								
				  				<fmtAPDHRList:message key="LBL_QTY_REC" var="varQtyRec"/><display:column property="QTYREC" title="${varQtyRec}." sortable="true" class="alignright" total="true" format="{0, number, 0}"/>							
				  				<fmtAPDHRList:message key="LBL_QTY_ACCP" var="varQtyAccp"/><display:column property="SHELFQTY" title="${varQtyAccp}." sortable="true" class="alignright" total="true" format="{0, number, 0}"/>
								<fmtAPDHRList:message key="LBL_REC_COST" var="varRecCost"/><display:column property="RECCOST" title="${varRecCost}" sortable="true" class="alignright" total="true" format="{0,number,#,###,###.00}"/>							
								<fmtAPDHRList:message key="LBL_APP_COST" var="varAppCost"/><display:column property="SHELFCOST" title="${varAppCost}" sortable="true" class="alignright" total="true" format="{0,number,#,###,###.00}"/>
								<fmtAPDHRList:message key="LBL_INV_NO" var="varInvNo"/><display:column property="INVID" title="${varInvNo}." class="aligncenter" sortable="true"/>
								<fmtAPDHRList:message key="LBL_INV_AMT" var="varInvAmt"/><display:column property="PAYMENTINVAMT" title="${varInvAmt}" sortable="true" format="{0,number,#,###,###.00}"/>
								<fmtAPDHRList:message key="LBL_EXT_INV_AMT" var="varExtInvAmt"/><display:column property="PAYMENTEXTINVAMT" title="${varExtInvAmt}" class="alignright" sortable="true" format="{0,number,#,###,###.00}"/>
								<fmtAPDHRList:message key="LBL_CRE_AMT" var="varCreAmt"/><display:column property="PAYMENTCREAMT" title="${varCreAmt}" sortable="true" format="{0,number,#,###,###.00}"/>
								<fmtAPDHRList:message key="LBL_DIFF" var="varDiff"/><display:column property="PAYMENTDIFF" title="${varDiff}" class="alignright" sortable="true" format="{0,number,#,###,###.00}"/>
																	
							</display:table>  
						</td>
					</tr>
			   	</table>
  			   </td>
  		  </tr>
    </table>
</html:form>  
<%@ include file="/common/GmFooter.inc"%>  		     	
</BODY>
</HTML>