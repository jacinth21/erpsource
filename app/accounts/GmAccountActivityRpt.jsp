

<%
	/***************************************************************************************************************************************
	 * File		 		: GmAccountActivityRpt.jsp
	 * Desc		 		: This screen is used for Loading the Account Activity Report as well as Invoice Report Info. for multiple Accounts.
	 * Version	 		: 1.0
	 * author			: Hreddi
	 ****************************************************************************************************************************************/
%>
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>
<%@ page import="com.globus.common.beans.GmCommonControls"%>
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="fmtAccountActivityRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- \accounts\GmAccountActivityRpt.jsp -->
<fmtAccountActivityRpt:setLocale value="<%=strLocale%>"/>
<fmtAccountActivityRpt:setBundle basename="properties.labels.accounts.GmAccountActivityRpt"/> 
<bean:define id="alInvType" name="frmAccountActivity" property="alInvType" type="java.util.ArrayList"></bean:define>
<bean:define id="alAccount" name="frmAccountActivity" property="alAccount" type="java.util.ArrayList"></bean:define>
<bean:define id="gridData" name="frmAccountActivity" property="gridData" type="java.lang.String"></bean:define>
<bean:define id="status" name="frmAccountActivity" property="status" type="java.lang.String"></bean:define>
<bean:define id="invSource" name="frmAccountActivity" property="invSource" type="java.lang.String"></bean:define>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%> 
<%
String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
String strLabel="";
HashMap hmCurrency = new HashMap();
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.accounts.GmIncludeAccountType", strSessCompanyLocale);
	String strWikiTitle = GmCommonClass.parseNull((String)GmCommonClass.getWikiTitle("ACCOUNT_ACTIVITY"));
	String strApplDateFmt = strGCompDateFmt;
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	String strCurDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt);
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strCurrSymbol = GmCommonClass.parseNull((String)hmCurrency.get("CMPCURRSMB"));
	log.debug("invSource"+invSource);
	if(invSource.equals("26240213")){
		strLabel=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_DEALER_NAME"));
	}
	else
	{
		strLabel=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ACCOUNTS"));
	}
	HashMap hmMapProject = new HashMap();
	hmMapProject.put("ID", "");
	hmMapProject.put("PID", "ID");
	hmMapProject.put("NM", "NAME");

	HashMap hmInvTypeForCobo = new HashMap();
	hmInvTypeForCobo.put("ID", "");
	hmInvTypeForCobo.put("PID", "CODEID");
	hmInvTypeForCobo.put("NM", "CODENM");
	
%>
<HTML>
<HEAD>
<TITLE>GlobusOne Enterprise Portal: Account Activity Reports</TITLE>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmAccountActivityReport.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<script>
var objGridData = '';
objGridData = '<%=gridData%>';
var date_format = '<%=strApplDateFmt%>';
var inv_status = '<%=status%>';
var invsource = <%=invSource%>
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();" onkeypress="fnEnter();">
	<html:form action="/gmAccountActivity.do">
		<html:hidden property="strOpt" />
		<html:hidden property="typeSelected" />
		<html:hidden property="accSelected" />
		<table border="0" class="DtTable950" cellspacing="0"
			cellpadding="0">
			<tr>
				<td colspan="8" height="25" class="RightDashBoardHeader"><fmtAccountActivityRpt:message key="LBL_ACCOUNT_ACTIVITY_REPORT"/></td>
				<td height="25" class="RightDashBoardHeader"><fmtAccountActivityRpt:message key="IMG_HELP" var="varHelp"/><img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td colspan="10" class="LLine" height="1"></td>
			</tr>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<tr height="25"> 
		
				<td class="RightTableCaption" colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtAccountActivityRpt:message key="LBL_TYPE" var="varType"/><gmjsp:label type="RegularText" SFLblControlName="${varType}:" td="false" />&nbsp;<gmjsp:dropdown
						controlName="invSource" SFFormName="frmAccountActivity"
						SFSeletedValue="invSource" SFValue="alInvSource" codeId="CODEID"
						codeName="CODENM" onChange="fnGetAccountList(this);"
						tabIndex="1" />
				</td>

				<%
				GmCommonControls.setWidth(350);
				%> 
				<td colspan="4"> <table><tr><td class="RightTableCaption" align="left" id="td_nm_id">&nbsp;&nbsp;&nbsp;&nbsp;<%=strLabel%></td>
				<td><%=GmCommonControls.getChkBoxGroup("", alAccount,"Chk_Account", hmMapProject, "NM")%></td></tr>
				</table>
				</td>
			</tr>
			<tr>
				<td colspan="8">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="10" class="LLine" height="1"></td>
			</tr>
			<tr class="Shade">
				<td>
					<table>
						<tr height="25">
							<td class="RightTablecaption" align="left"
								colspan="2">&nbsp;&nbsp;<fmtAccountActivityRpt:message key="LBL_INVOICE_TYPE"/>:</td>
								<td><%=GmCommonControls.getChkBoxGroup("", alInvType,"Chk_Invoice_Type", hmInvTypeForCobo)%></td>
						</tr>
					</table></td>
				<td colspan="7">
					<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr height="25">
							<td class="RightTablecaption" align="right" ><fmtAccountActivityRpt:message key="LBL_STATUS" var="varStatus"/><gmjsp:label
									type="RegularText" SFLblControlName="${varStatus}:" td="false" />
							</td>
							<td class="RightText">&nbsp;<select name="status"
								class="RightText" tabindex="1">
									<option value="0"><fmtAccountActivityRpt:message key="OPT_ALL"/>
									<option value="1"><fmtAccountActivityRpt:message key="OPT_PENDING"/>
									<option value="2"><fmtAccountActivityRpt:message key="OPT_CLOSED"/>
							</select>
							</td>
							<%if(invSource.equals("50255")) {%>
							<td class="RightTablecaption" align="right" ><fmtAccountActivityRpt:message key="LBL_CURRENCY" var="varCurrency"/><gmjsp:label
									type="RegularText" SFLblControlName="${varCurrency}:" td="false" /></td>
							<td class="RightText">&nbsp;<gmjsp:dropdown	controlName="strCompCurrency" SFFormName="frmAccountActivity"
						                    SFSeletedValue="strCompCurrency" SFValue="alCompCurrency" codeId="ID"
						                    codeName="NAMEALT"  optionId="NAME"/>	
						                     </td>	
						     <%} %>
						   </tr>
						<tr height="25">
							<td class="RightTablecaption" align="right"><fmtAccountActivityRpt:message key="LBL_FROM_DATE" var="varFromDate"/><gmjsp:label
									type="RegularText" SFLblControlName="${varFromDate}:" td="false" />
							</td>
							<td class="RightText">&nbsp;<gmjsp:calendar
						SFFormName="frmAccountActivity" controlName="fromDt"
						gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');"  /></td>
						<td class="RightTablecaption" align="right"><fmtAccountActivityRpt:message key="LBL_TO_DATE" var="varToDate"/><gmjsp:label type="RegularText" SFLblControlName="${varToDate}:" td="false" /></td>
						<td>&nbsp;<gmjsp:calendar
						SFFormName="frmAccountActivity" controlName="toDt"
						gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');"  /></td>
						</tr>
						<tr height="10">
							<td></td>
						</tr>
					</table></td>
				<td align="center" width="200px"><fmtAccountActivityRpt:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}"
					gmClass="Button" name="btn_Load" style="width: 6em"
					onClick="fnLoad();" tabindex="6" buttonType="Load" />
				</td>
			</tr>
			<tr>
				<td colspan="10" class="LLine" height="1"></td>
			</tr>
			<tr height="25">
			<td colspan="10">
				<table border="0" width="100%"><tr>
					<td colspan="2" align="center" class="RightTextRed"><fmtAccountActivityRpt:message key="LBL_CREDIT_RED"/></td>
					<td colspan="2" align="center" class="RightTextBlue"><fmtAccountActivityRpt:message key="LBL_DEBIT_BLUE"/></td>
					<td colspan="2" align="center" class="RightTextGreen"><fmtAccountActivityRpt:message key="LBL_REGULAR_GREEN"/></td>
					<td colspan="2" align="center" class="RightTextPurple"><fmtAccountActivityRpt:message key="LBL_OBO_PURPLE"/></td>
				</tr></table>
			</td>	
			</tr>

			<tr>
				<td colspan="10">
					<div id="acctivityRpt" style="height: 320px;"></div>
					<div id="pagingArea" style=" width:800px"></div>
					</td>
					
			</tr>
				<%
					if (gridData.indexOf("cell") != -1) {
				%>
				<tr>
				<td colspan="10" class="LLine" height="1"></td>
			</tr>
			<tr>
				<td align="center" colspan="9" height="35">&nbsp;&nbsp; 
					<fmtAccountActivityRpt:message key="BTN_PRINT" var="varPrint"/><gmjsp:button value="${varPrint}" gmClass="Button" name="btn_print"
					style="width: 6em" buttonType="Load" onClick="fnPrintRpt();" tabindex="6" />
				</td>
				</tr>
				<tr height="25">
			<td colspan="9" align="center">
			    <div class='exportlinks'><fmtAccountActivityRpt:message key="LBL_EXPORT_OPTIONS"/> : <img
					src='img/ico_file_excel.png' />&nbsp;<a href="#"
					onclick="fnExport();"> <fmtAccountActivityRpt:message key="LBL_EXCEL"/>
				</a></div>
			</td>
		</tr>
				<%
					}
				%>
			</tr>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
