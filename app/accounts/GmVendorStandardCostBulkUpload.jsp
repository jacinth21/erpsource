<%
/**********************************************************************************
 * File		 		: GmVendorStandardCostBulkUpload.jsp
 * Desc		 		: Provide new screen to update the part price in Standard cost by Vendor 
 * Version	 		: 
 * author			: Prabhu vigneshwaran M D 
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>
<%@ taglib prefix="fmtStandardCostBulkUpload" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtStandardCostBulkUpload:setBundle basename="properties.labels.accounts.GmStandardCostReport"/>
<%@ page isELIgnored="false" %>
<%@ page buffer="16kb" autoFlush="true" %>

<bean:define id="editAccess" name="frmStandardReport"  property="strEditAccess" type="java.lang.String"></bean:define>
<bean:define id="strMaxUploadData" name="frmStandardReport" property="strMaxUploadData" type="java.lang.String"> </bean:define>
<%
response.setHeader("Cache-Control", "no-cache, post-check=0, pre-check=0"); //HTTP 1.1
response.setHeader("Pragma", "no-cache"); //HTTP 1.0
response.setDateHeader("Expires", 0); //prevents caching at the proxy server 

String strAccountsJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
String strWikiTitle = GmCommonClass.getWikiTitle("STD_COST_BULK_UPLOAD");
%> 

<html>
<head>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<title>Standard Cost Bulk Upload</title>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all"> @import url("<%=strCssPath%>/screen.css");</style>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_fast.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_form.js"></script>
<!-- Method allows user to add new row from clipboard. New row is being added to the last position in the grid. -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<!-- paste content of clipboard into block selection of grid -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_markers.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<!-- The library provides support for different keyboard commands that let users to navigate through the grid. -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strAccountsJsPath%>/GmVendorStandardCostBulkUpload.js"></script>
<script>
var maxUploadData = '<%=strMaxUploadData%>';
</script>
</head>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form action="/gmVendorStandardCost.do?method=loadStandardCostBulkUpload">
<html:hidden property="inputString" name ="frmStandardReport"/>
<html:hidden property="strPartNumbers" name ="frmStandardReport"/>

		<table border="0" class="DtTable700"  cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="4">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
						<td height="25" class="RightDashBoardHeader" colspan="2">&nbsp;<fmtStandardCostBulkUpload:message key="LBL_STD_COST_BULK_UPLOAD"/></td>
						<td  height="25" class="RightDashBoardHeader" align="right" colspan="2">
							<fmtStandardCostBulkUpload:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
			      			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>',' <%=strWikiTitle%>');" />
			      			</td>
		      		</tr></table>
	      		</td>
			</tr>
			<tr>
				<td class="RightTableCaption"  colspan="2" align="right" height="30" width="100">&nbsp;<gmjsp:label type="MandatoryText" SFLblControlName="${varVendorName}" td="false"/><fmtStandardCostBulkUpload:message key="LBL_VENDOR_NAME"/></td>
				<td colspan="2">&nbsp;<gmjsp:dropdown 
				controlName="strVendorId"
				SFFormName="frmStandardReport" 
				SFSeletedValue="strVendorId"
				onChange="fnChange(this);" 
				SFValue="alVendorList" 
				codeId = "ID"  
				codeName = "NAME"  
				 defaultValue="[Choose One]" 
				 width="315"/>
				&nbsp;&nbsp;&nbsp;	
			</tr>
			<tr><td class="LLine" colspan="4" height="1"></td></tr>
		<tr  style="display: none" id="trImageShow"  class="Shade" height="25">
		<td colspan="4">
		<table>	
				<tr  height="25">		
						<td  width="28" colspan="1" tabIndex="-1"><fmtStandardCostBulkUpload:message key="IMG_ALT_COPY" var="varCopy"/><a href="#"><img src="<%=strImagePath%>/dhtmlxGrid/copy.gif" onClick="javascript:docopy()" alt="${varCopy}" style="border: none;" height="14"></a></td>
						<td  width="28" colspan="1" tabIndex="-1"><fmtStandardCostBulkUpload:message key="IMG_ALT_PASTE" var="varpaste"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/paste.gif" alt="${varpaste}" style="border: none;" onClick="javascript:pasteToGrid()" height="14"></a></td>	
						<td  width="28" colspan="1" tabIndex="-1"><fmtStandardCostBulkUpload:message key="IMG_ALT_ADD_ROW" var="varAddRow"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="${varAddRow}" style="border: none;" onClick="javascript:fnAddRows()"height="14"></a></td>
						<td  width="28" colspan="1" tabIndex="-1"><fmtStandardCostBulkUpload:message key="IMG_ALT_REMOVE_ROW" var="varRemoveRow"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="${varRemoveRow}" style="border: none;" onClick="javascript:fnRemoveSetUploadRow()" height="14"></a></td>
						<td  width="28" colspan="1" tabIndex="-1"><fmtStandardCostBulkUpload:message key="IMG_ALT_ADD_ROWS_CLIPBOARD" var="varAddRowsClipboard"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/row_add_after_1.gif" alt="${varAddRowsClipboard}" style="border: none;" onClick="javascript:fnAddRowFromClipboard()" height="14"></a></td>
					
						<td  colspan="2" align="right" >
						<div id="progress" style="display:none;"><img src="<%=strImagePath%>/success_y.gif" height="15"> </div></td>
						<td><div id="msg"  align="center"  style="display:none;color:green;font-weight:bold;"></div> 
					</td>
			   </tr>
			   <tr height="25" id="trLabelShow"><td colspan="8"><font color="#6699FF">&nbsp;
                    <fmtStandardCostBulkUpload:message key="LBL_MAXIMUM"/>&nbsp;<%=strMaxUploadData%>&nbsp;<fmtStandardCostBulkUpload:message key="LBL_LESS"/> </font></td>
		            </tr>													
		</table>
		</td>
		</tr>	
		<tr><td class="LLine" colspan="4" height="1"></td></tr>
		<tr id="trDiv">
              <td colspan="4">
	         <div id="dataGridDiv"  height="400px"></div></td>
        </tr> 
		<tr><td class="LLine" colspan="4" height="1"></td></tr>
		
		   <tr style="display: none" id="buttonshow" height="30"><td colspan="4"  align="center">
           
           <div id="buttonDiv">
               <logic:equal name="frmStandardReport" property="strEditAccess" value="Y"> 
	            <fmtStandardCostBulkUpload:message key="LBL_SUBMIT" var="varSubmit"/>
	            <gmjsp:button value="${varSubmit}" gmClass="button" name="LBL_SUBMIT" onClick="fnSubmit();"  buttonType="Save"/>
			  </logic:equal>
			</div>
			
			</td>
			
			</tr> 
        		
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</body>
</html>