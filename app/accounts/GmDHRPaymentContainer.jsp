<!--
/**********************************************************************************
 * File		 		: GmDHRPaymentContainer.jsp
 * Desc		 		: DHR Payment Container
 * Version	 		: 1.0
 * author			: Basudev Vidyasankar
************************************************************************************/
-->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>

<%@ taglib prefix="fmtDhrPaymentContainer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!--GmDHRPaymentContainer.jsp -->
<fmtDhrPaymentContainer:setLocale value="<%=strLocale%>"/>
<fmtDhrPaymentContainer:setBundle basename="properties.labels.accounts.GmDHRPaymentContainer"/>
<bean:define id="hmPODetails" name="frmAPInvoiceDtls" property="hmpurchaseOrdDtls" scope="request" type="java.util.HashMap"></bean:define>
<bean:define id="hpoNumber" name="frmAPInvoiceDtls" property="hpoNumber"></bean:define>

<%
	String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
	//To display AP Invoice details within container
	Object bean = pageContext.getAttribute("frmAPInvoiceDtls", PageContext.REQUEST_SCOPE);
	pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	
	String strWikiTitle = GmCommonClass.getWikiTitle("POST_DHR_PAYMENT");
	//To get Vendor name and Currency
	String strVendorName = GmCommonClass.parseNull((String)hmPODetails.get("VNAME"));
	String strCurrency = GmCommonClass.parseNull((String)hmPODetails.get("CURR"));
	
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();
	
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Post DHR Payment</title>

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmDHRPayment.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script>
var format = '<%=strApplDateFmt%>';
</script>
</head>

<body leftmargin="20" topmargin="10" onLoad="fnSetCursorPosition('containerPONumber');fnAjaxTabLoad(); ">
<html:form action="/gmDHRPayment.do">
<html:hidden property="strOpt" value=""/>
<input type="hidden" name="hpaymentId" value=""/>
<input type="hidden" name="hpoNumber" value=""/>
<input type="hidden" name="hpoId" value="<%=hpoNumber%>"/>
<table border="0" class="DtTable1055" cellspacing="0" cellpadding="0" style="WIDTH: 1150px">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtDhrPaymentContainer:message key="LBL_POST_DHR_PAYMENT"/></td>
			<td align="right" class="RightDashBoardHeader" colspan="3"> 	
			<fmtDhrPaymentContainer:message key="LBL_HELP" var="varHelp"/>
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' 
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
		</tr>
		<tr>
			<td colspan="4" height="1" bgcolor="#666666"></td>
		</tr>
		<tr>
			<td class="RightTableCaption" align="right" height="30" width="20%"><font color="red">*</font>&nbsp;<fmtDhrPaymentContainer:message key="LBL_PO_NUMBER"/>&nbsp;:</td>					
			<td>&nbsp;
   		            <html:text property="containerPONumber"  size="15" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex = "1" />
   		            <fmtDhrPaymentContainer:message key="BTN_LOAD" var='varLoad'/>
				    <gmjsp:button value="${varLoad}" name="Btn_Fetch" gmClass="Button" onClick="fnDHRPaymentGo(this.form);" tabindex = "2" buttonType="Load" />
  			</td>
  			<script type="text/javascript">
  				fnFocusPO('');
  			</script>
  			<td align="center" colspan="2">
  			<fmtDhrPaymentContainer:message key="BTN_NEW_INVOICE" var="varInvoice"/>
				  <logic:equal name="frmAPInvoiceDtls" property="strEnableNewInvoice" value="true">
		    		    <gmjsp:button value="${varInvoice}"  name="Btn_NewInvoice" gmClass="Button" buttonType="Save" onClick="fnNewInvoice(this.form);" tabindex = "-1"/>
				  </logic:equal>
				  <logic:equal name="frmAPInvoiceDtls" property="strEnableNewInvoice" value="false">
						<gmjsp:button value="${varInvoice}" disabled="true" name="Btn_NewInvoice" gmClass="Button" buttonType="Save" onClick="fnNewInvoice(this.form);"/>
				  </logic:equal>
  			</td>
		</tr>
</html:form>
		<tr>
			<td colspan="4" height="1" bgcolor="#666666"></td>
		</tr>
		<!-- div for displaying AP Invoice Details. Ajax calls made from GmDHRPayment.js -->
		<logic:notEqual name="frmAPInvoiceDtls" property="containerPONumber" value="GM-PO-">
		<logic:notEqual name="frmAPInvoiceDtls" property="hpoNumber" value="">
		<tr>
		    <td colspan ="4" align=center>
				<div id="invoicearea" style="display:visible;width=100%">
					<fmtDhrPaymentContainer:message key="LBL_INVOICE_DETAILS" var="varInvoiceDetails"/>
					<a href="/gmAPSaveInvoiceDtls.do?hpoNumber=<bean:write name="frmAPInvoiceDtls" property="hpoNumber"/>&hpaymentId=<bean:write name="frmAPInvoiceDtls" property="hpaymentId"/>&strOpt=<bean:write name="frmAPInvoiceDtls" property="strOpt"/>&companyInfo=<%=strCompanyInfo %>" title="${varInvoiceDetails}" rel="ajaxinvoicearea"></a>
					<div id="ajaxinvoicearea"></div>
				</div>
				<script type="text/javascript">
					var invoicearea=new ddajaxtabs("invoicearea", "ajaxinvoicearea");				
					invoicearea.setpersist(true);								
					invoicearea.init();
					/*					
					invoicearea.onajaxpageload=function(pageurl){
  						alert(pageurl);
						if (pageurl.indexOf("/gmAPSaveInvoiceDtls.do")!=-1){
							fnRefreshInvoiceList();
						}
					}
					*/
				</script>	
		 	</td>
		</tr>
		</logic:notEqual>
		</logic:notEqual>
		<!-- div for displaying Invoice List and DHR List in tabs. Ajax calls made from GmDHRPayment.js -->
		<tr>
			<td colspan="4">
			
			
			<logic:notEqual name="frmAPInvoiceDtls" property="containerPONumber" value="GM-PO-">
			<logic:notEqual name="frmAPInvoiceDtls" property="hpoNumber" value="">
				<ul id="maintab" class="shadetabs">
					<fmtDhrPaymentContainer:message key="LBL_INVOICE_LIST" var="varInvoiceList"/>
					<li> <a href="/gmDHRPayment.do?strOpt=loadInvc&hpoNumber=<bean:write name="frmAPInvoiceDtls" property="containerPONumber"></bean:write>&hpaymentId=<bean:write name="frmAPInvoiceDtls" property="hpaymentId"></bean:write>"
					 title="${varInvoiceList}"  rel="ajaxdivcontentarea" name="Invoice List"><fmtDhrPaymentContainer:message key="LBL_INVOICE_LIST"/></a></li>
					<li> <a href="/gmAPDHRList.do?strOpt=LoadDHR&hpoNumber=<bean:write name="frmAPInvoiceDtls" property="containerPONumber"></bean:write>"
						 title="DHR List" rel="#iframe" name="DHR List"><fmtDhrPaymentContainer:message key="LBL_DHR_LIST"/></a></li>
				</ul>
				<div id="ajaxdivcontentarea" style="display:visible;height:50px;width:100%">
				</div>
			</logic:notEqual>
			</logic:notEqual>
			</td>
		</tr>
	</table>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>