
<%
	/***************************************************************************************************************************************
	 * File		 		: GmAccountActivityRptPrint.jsp
	 * Desc		 		: This screen is used for Loading the Account Activity Report as well as Invoice Report Info. for multiple Accounts.
	 * Version	 		: 1.0
	 * author			: Hreddi
	 ****************************************************************************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmJasperReport"%>
<%@ page import="java.util.HashMap,java.util.*,java.text.*"%>
<%@ taglib prefix="fmtAccountActivityReportPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- \accounts\GmAccountActivityReportPrint.jsp -->
<fmtAccountActivityReportPrint:setLocale value="<%=strLocale%>"/>
<fmtAccountActivityReportPrint:setBundle basename="properties.labels.accounts.GmAccountActivityReportPrint"/> 
<bean:define id="hmJasperDetails" name="frmAccountActivity"	property="hmJasperDetails" type="java.util.HashMap"></bean:define>
<bean:define id="alReturn" name="frmAccountActivity" property="alReturn" type="java.util.ArrayList"></bean:define>
<bean:define id="strAccountName" name="frmAccountActivity" property="strAccountName" type="java.lang.String"></bean:define>
<bean:define id="strJasRptName" name="frmAccountActivity" property="strJasRptName" type="java.lang.String"></bean:define>
<%
request.setCharacterEncoding("UTF-8");
response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8"); 
String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");
String strAccName = GmCommonClass.parseNull((String)hmJasperDetails.get("ACCNAME"));
String strHtmlJasperRpt = "";
%>
<HTML>
<HEAD>
<script language="JavaScript" src="<%=strAccountJsPath%>/GmAccountActivityReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

</HEAD>
<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();"
	onafterprint="showPrint();">
	<table>
		<tr>
		<td><div>
			<%
				int arraySize = alReturn.size();
				if (arraySize == 0) {
			%>
			
			<table border="0" class="border" width="800" cellspacing="0"cellpadding="0">
			<tr><td height="30" align="center" class="RegularText">&nbsp;&nbsp;<fmtAccountActivityReportPrint:message key="LBL_NO_DATA_AVAILABLE"/>  <b><%=strAccountName %> </b>&nbsp;&nbsp;</td></tr></table>
			<%
				} else if (!hmJasperDetails.equals("")) {
			%>
			<td><div>
					<%
						String strJasperPath = GmCommonClass
									.getString("GMJASPERLOCATION");
							GmJasperReport gmJasperReport = new GmJasperReport();
							gmJasperReport.setRequest(request);
							gmJasperReport.setResponse(response);
							gmJasperReport.setJasperReportName(strJasRptName);
							gmJasperReport.setHmReportParameters(hmJasperDetails);
							gmJasperReport.setReportDataList(alReturn);
							gmJasperReport.setBlDisplayImage(true);
							strHtmlJasperRpt = gmJasperReport.getHtmlReport();
						}
					%>
					<%=strHtmlJasperRpt %>
				</div></td>
		</tr>

	</table>
	<div id="button" align="center">
		<table border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center" height="30">
					<%
						if (arraySize != 0) {
					%> <fmtAccountActivityReportPrint:message key="BTN_PRINT" var="varPrint"/><gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print"
					gmClass="button" buttonType="Load" onClick="fnPrint();"/>&nbsp;&nbsp; <%
 	}
 %> <fmtAccountActivityReportPrint:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close"
					gmClass="button" buttonType="Load" onClick="fnClose();"/>&nbsp;</td>
			<tr>
		</table>
	</div>
</BODY>
</HTML>