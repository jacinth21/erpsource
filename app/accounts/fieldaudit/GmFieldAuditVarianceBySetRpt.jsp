<%
/******************************************************************************************************************
	* File		 	: GmFieldAuditVarianceBySetRpt.jsp
	* Desc		 	: This screen is used to display the Variance Report by Set and Global variance By Distributer.
	* Version	 	: 1.0
	* author		: HReddi
*******************************************************************************************************************/
%> 
<%@ page language="java" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>

<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.List" %>
<%@ taglib prefix="fmtFieldAuditVarianceBySetRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- \accounts\fieldaudit\GmFieldAuditVarianceBySetRpt.jsp -->
<fmtFieldAuditVarianceBySetRpt:setLocale value="<%=strLocale%>"/>
<fmtFieldAuditVarianceBySetRpt:setBundle basename="properties.labels.accounts.fieldaudit.GmFieldAuditVarianceByDistRpt"/>

<bean:define id="reportType" name="frmFieldAuditReport" property="reportType" type="java.lang.String"> </bean:define>
<bean:define id="dispflaggedDet" name="frmFieldAuditReport" property="dispflaggedDet" type="java.lang.Boolean"> </bean:define>
<bean:define id="ldtResult" name="frmFieldAuditReport" property="ldtResult" type="java.util.List"></bean:define>

<%
  String strWikiTitle 	  = "";
  String flaggedDet		  = dispflaggedDet.toString();
  String strHeader		  = "";
  String strTransferTo 	  = "";
  String strBorrowedFrom  = "";
  String strExtra 	 	  = "";
  String strReturns       = "";
  String strTransferFrom  = "";
  String strLendTo 	 	  = "";
  String strMissing 	  = "";
  String strUnresolvedQty = "";
  String strFlagQty		  = "";
  String strNotADevNeg 	  = "";
  String strNotADevPos 	  = "";
  String strUnVoidTag	  = "";
  String strVerifyPostCnt = "";
  String strVoidTag 	  = "";
  int rowsize = ldtResult.size(); 
  String strAccountFaJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_FIELDAUDIT");
  GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.accounts.fieldaudit.GmFieldAuditVarianceByDistRpt", strSessCompanyLocale);
  String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
  if(reportType.equals("globalDist"))
  {
	  strHeader= GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_GLOBAL_VARIANCE_BY_DISTRIBUTOR"));
	  strWikiTitle = GmCommonClass.getWikiTitle("GLOBAL_DISTRIBUTOR");
  }
  else
  {
	  strHeader=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_VARIANCE_REPORT_BY_SET"));
	  strWikiTitle = GmCommonClass.getWikiTitle("VARIANCE_SET");
  }
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Field Sales Report</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");     
     table.DtTable1000 {	width: 1140px;	}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strAccountFaJsPath%>/GmFieldAuditVarianceRpt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>


<script type="text/javascript">
var dhxWins,w1;
var format = '<%=strApplnDateFmt%>';
</script>
</HEAD>

<BODY onkeyup="fnEnter();" leftmargin="20" topmargin="10" onmouseover="fnRowHighlight();">
 
<html:form  action="/gmFieldAuditReport.do?method=reportVarianceBySet">
<html:hidden property="strOpt"/>
<html:hidden property="reportType"/>
      <table border="0" class="DtTable1000"  cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="5" height="25" class="RightDashBoardHeader"> <%=strHeader%> </td>
                <td height="25" class="RightDashBoardHeader"> <fmtFieldAuditVarianceBySetRpt:message key="IMG_HELP" var="varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> </td>
            </tr>
            <tr>
            	<td colspan="6" class="LLine" height="1"></td>
            </tr>
	    <!-- Custom tag lib code modified for JBOSS migration changes -->
            <tr class="shade" >          
            	<% if(!reportType.equals("globalDist") && flaggedDet.equals("true")){ %>
                 	<td height="30" width="300" align ="Right" class="RightTableCaption"><font color="red">*</font><fmtFieldAuditVarianceBySetRpt:message key="LBL_AUDIT_LIST" />:&nbsp;</td>
                 	<td><gmjsp:dropdown controlName="auditId" SFFormName="frmFieldAuditReport" SFSeletedValue="auditId" SFValue="alAuditName" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]"/></td>
                <%}else if(!reportType.equals("globalDist")){ %>
                 	<td height="30" align ="Right" class="RightTableCaption"><font color="red">*</font><fmtFieldAuditVarianceBySetRpt:message key="LBL_AUDIT_LIST" />:&nbsp;</td>
                 	<td><gmjsp:dropdown controlName="auditId" SFFormName="frmFieldAuditReport" SFSeletedValue="auditId" SFValue="alAuditName" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]"/></td>
                <%}%>                 
                <% if(reportType.equals("globalDist") && flaggedDet.equals("true")){ %>
                 	<td height="30" width="270" align="Right" class="RightTableCaption"><fmtFieldAuditVarianceBySetRpt:message key="LBL_AUDIT_LIST" />:&nbsp;</td>
                 	<td align="left" width="650"><gmjsp:dropdown controlName="auditId" SFFormName="frmFieldAuditReport" SFSeletedValue="auditId" SFValue="alAuditName" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" width="300"/></td>
                 	<td colspan="4"> </td>
                <%}else if(reportType.equals("globalDist")){ %> 	
                 	<td height="30" width="270" align="Right" class="RightTableCaption"><fmtFieldAuditVarianceBySetRpt:message key="LBL_AUDIT_LIST" />:&nbsp;</td>
                 	<td align="left" width="855"><gmjsp:dropdown controlName="auditId" SFFormName="frmFieldAuditReport" SFSeletedValue="auditId" SFValue="alAuditName" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" width="300"/></td>
                 	<td colspan="4"> </td>   
                <%}%> 
                <% if(!reportType.equals("globalDist") && flaggedDet.equals("true")){%>
                 	<td height="30"  width="50" align ="Right" class="RightTableCaption"><font color="red">*</font><fmtFieldAuditVarianceBySetRpt:message key="LBL_SET" />:&nbsp;</td>
                 	<td align ="Left" width="90"><gmjsp:dropdown controlName="setId" width="250" SFFormName="frmFieldAuditReport" SFSeletedValue="setId" SFValue="alSet" codeId = "ID"  codeName = "IDNAME" defaultValue= "[Choose One]" /></td>                   
                 	<td height="30" align ="Right" class="RightTableCaption" ><fmtFieldAuditVarianceBySetRpt:message key="LBL_REGION" />:&nbsp;</td>
                 	<td><gmjsp:dropdown   width="170" controlName="regionId" SFFormName="frmFieldAuditReport" SFSeletedValue="regionId" SFValue="alResions" codeId = "ID"  codeName = "NM" defaultValue= "[Choose One]" /></td>    
                <%}else if(!reportType.equals("globalDist")){%> 
                 	<td height="30"   align ="Right" class="RightTableCaption"><font color="red">*</font><fmtFieldAuditVarianceBySetRpt:message key="LBL_SET" />:&nbsp;</td>
                 	<td align ="Left" ><gmjsp:dropdown controlName="setId" width="280" SFFormName="frmFieldAuditReport" SFSeletedValue="setId" SFValue="alSet" codeId = "ID"  codeName = "IDNAME" defaultValue= "[Choose One]" /></td>                   
                 	<td height="30" align ="Right" class="RightTableCaption" ><fmtFieldAuditVarianceBySetRpt:message key="LBL_REGION" />:&nbsp;</td>
                 	<td><gmjsp:dropdown   width="170" controlName="regionId" SFFormName="frmFieldAuditReport" SFSeletedValue="regionId" SFValue="alResions" codeId = "ID"  codeName = "NM" defaultValue= "[Choose One]" /></td>    
                  	<td colspan="4"> </td>    
                <%}%>                       
            </tr>         
            <tr>
                <td colspan="6" class="LLine" height="1"></td>
            </tr>
            <tr>     
            	<td colspan="6">
		       		<table cellspacing="0" cellpadding="0" border="0">
			       		<tr>
			       			<td>
								<jsp:include page="/accounts/fieldaudit/GmFieldAuditVarianceFilterRpt.jsp" >
									<jsp:param name="FORMNAME" value="frmFieldAuditReport" />
								</jsp:include>
							</td>
						</tr>
					</table>	
				</td>
			</tr>
			<tr>
				<td colspan="6" class="LLine" height="1"></td>
			</tr>
		<%if(rowsize >= 1){ %>
			<logic:equal name="frmFieldAuditReport" property="dispflaggedDet" value="true" >
		<tr>
			<td colspan="8">
				<table border="0" class="DtTable1000" width="700" height="25" cellspacing="0" cellpadding="0">
					<tr>
						<td class = "ShadeDarkGrayTD" style="text-align:center;width:80;" >	</td> <td class = "ShadeDarkGrayTD" style="text-align:center;width:100;">	</td>
						<td class = "ShadeDarkGrayTD" style="text-align:center;width:80;" >	</td> <td class = "ShadeDarkGrayTD" style="text-align:center;width:50;" >	</td>
						<td class = "ShadeDarkGrayTD" style="text-align:center;width:50;" >	</td> <td class = "ShadeDarkGrayTD" style="text-align:center;width:35;" >	</td>
						<td class = "ShadeDarkGrayTD" style="text-align:center;width:35;" >	</td> <td class = "ShadeDarkGrayTD" style="text-align:center;width:35;" >	</td>
						<td class = "ShadeDarkGrayTD" style="text-align:center;width:15;" >	</td> <td class = "ShadeDarkGrayTD" style="text-align:center;width:10;" >	</td>
						<td class = "ShadeDarkGrayTD" style="text-align:center;width:198;">	<b><fmtFieldAuditVarianceBySetRpt:message key="LBL_POSITIVE_DEVIATION" /></b></td>
						<td class = "ShadeDarkGrayTD" style="text-align:center;width:230;">	<b><fmtFieldAuditVarianceBySetRpt:message key="LBL_NEGATIVE_DEVIATION" /></b></td>
						<td class = "ShadeDarkGrayTD" style="text-align:center;width:80;" >	</td> <td class = "ShadeDarkGrayTD" style="text-align:center;width:70;" >	</td>
					</tr>
		 		</table>
			</td>
		</tr>
			</logic:equal>	
		<%}%>
    	<tr>
			<td colspan="6" width="100%" align="center">
				<display:table 	name="requestScope.frmFieldAuditReport.ldtResult" requestURI="/gmFieldAuditReport.do?method=reportVarianceBySet" export="true" decorator="com.globus.accounts.fieldaudit.displaytag.beans.DTFieldAuditRptWrapper" freezeHeader="true" paneSize="400" id="currentRowObject" varTotals="totals" cellpadding="0" cellspacing="0" >
				<fmtFieldAuditVarianceBySetRpt:message key="LBL_FIELD_SALES" var="varFieldSales"/><display:column property="DIST_NAME" title="${varFieldSales}" headerClass="ShadeDarkGrayTDCenter" style="text-align: left;width:330;" class="ShadeLightGrayTD" sortable="true" comparator="com.globus.common.displaytag.beans.DTComparator"/>
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
				<fmtFieldAuditVarianceBySetRpt:message key="DT_AUDIT_QTY" var="varAuditQty"/><display:column property="AUDIT_QTY" title="${varAuditQty}" headerClass="ShadeDarkBlueTD"  style="width:80;" total="true" sortable="true" class="ShadeLightBlueTD"/>
				<fmtFieldAuditVarianceBySetRpt:message key="DT_TAG_QTY" var="varTagQty"/><display:column property="TAG_QTY" title="${varTagQty}" sortable="true" total="true" style="width:80;" headerClass="ShadeDarkBlueTD" class="ShadeMedBlueTD"/>
				<fmtFieldAuditVarianceBySetRpt:message key="DT_OPS_QTY" var="varOpsQty"/><display:column property="OPS_QTY" title="${varOpsQty}" sortable="true" total="true"  style="width:80;" headerClass="ShadeDarkBlueTD" class="ShadeLightBlueTD"/>
				<fmtFieldAuditVarianceBySetRpt:message key="DT_SYSTEM_QTY" var="varSystemQty"/><display:column property="SYSTEM_QTY" title="${varSystemQty}" headerClass="ShadeDarkBlueTD" style="width:80;" total="true" sortable="true" class="ShadeMedBlueTD"/>
				<fmtFieldAuditVarianceBySetRpt:message key="DT_ABS_DEV" var="varAbsDev"/><display:column property="DEVIATION_QTY" title="${varAbsDev}" headerClass="ShadeDarkBlueTD"  style="width:80;" total="true" class="alignleft;ShadeLightBlueTD" sortable="true" comparator="com.globus.common.displaytag.beans.DTComparator"/>
				<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
				  <logic:equal name="frmFieldAuditReport" property="dispflaggedDet" value="true" >		
					<fmtFieldAuditVarianceBySetRpt:message key="DT_TF" var="varTF"/><display:column property="TRANSFER_FROM" 	title="${varTF}"  style="text-align: right;width:50;" sortable="true" total="true" headerClass="ShadeDarkBrownTD" class="ShadeMedBrownTD" />
					<fmtFieldAuditVarianceBySetRpt:message key="DT_BF" var="varBF"/><display:column property="BORROWED_FROM" 	title="${varBF}"  style="text-align: right;width:50;" sortable="true" total="true" headerClass="ShadeDarkBrownTD" class="ShadeLightBrownTD" />
					<fmtFieldAuditVarianceBySetRpt:message key="DT_EXA" var="varExa"/><display:column property="EXTRA" 			title="${varExa}" style="text-align: right;width:50;" sortable="true" total="true" headerClass="ShadeDarkBrownTD" class="ShadeMedBrownTD" />
					<fmtFieldAuditVarianceBySetRpt:message key="DT_ND" var="varND"/><display:column property="NOT_A_DEV_POS" 	title="${varND}"  style="text-align: right;width:50;" sortable="true" total="true" headerClass="ShadeDarkBrownTD" class="ShadeLightBrownTD" />
					<fmtFieldAuditVarianceBySetRpt:message key="DT_UV" var="varUV"/><display:column property="UNVOID_TAG" 		title="${varUV}"  style="text-align: right;width:50;" sortable="true" total="true" headerClass="ShadeDarkBrownTD" class="ShadeMedBrownTD" />
					<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
					<fmtFieldAuditVarianceBySetRpt:message key="DT_RA" var="varRA"/><display:column property="RETURNS" 			title="${varRA}"  style="text-align: right;width:50;" sortable="true" total="true" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD" />
					<fmtFieldAuditVarianceBySetRpt:message key="DT_M" var="varM"/><display:column property="MISSING" 			title="${varM}"   style="text-align: right;width:50;" sortable="true" total="true" headerClass="ShadeDarkGreenTD" class="ShadeLightGreenTD" />
					<fmtFieldAuditVarianceBySetRpt:message key="DT_TT" var="varTT"/><display:column property="TRANSFER_TO" 		title="${varTT}"  style="text-align: right;width:50;" sortable="true" total="true" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD" />
					<fmtFieldAuditVarianceBySetRpt:message key="DT_LT" var="varLT"/><display:column property="LEND_TO" 			title="${varLT}"  style="text-align: right;width:50;" sortable="true" total="true" headerClass="ShadeDarkGreenTD" class="ShadeLightGreenTD" />
					<fmtFieldAuditVarianceBySetRpt:message key="DT_VPC" var="varVPC"/><display:column property="VER_POST_CNT" 	title="${varVPC}" style="text-align: right;width:50;" sortable="true" total="true" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD" />
					<fmtFieldAuditVarianceBySetRpt:message key="DT_ND" var="varND"/><display:column property="NOT_A_DEV_NEG" 	title="${varND}"  style="text-align: right;width:50;" sortable="true" total="true" headerClass="ShadeDarkGreenTD" class="ShadeLightGreenTD" />
					<fmtFieldAuditVarianceBySetRpt:message key="DT_VT" var="varVT"/><display:column property="VOID_TAG" 		title="${varVT}"  style="text-align: right;width:50;" sortable="true" total="true" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD" />
					<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
					<fmtFieldAuditVarianceBySetRpt:message key="DT_FLAGGED_QTY" var="varFlaggedQty"/><display:column property="FLAGGED_QTY" 		title="${varFlaggedQty}" 	  style="text-align: right;width:100;" headerClass="ShadeDarkOrangeTD"  total="true" sortable="true" class="ShadeLightOrangeTD"/>						
					<fmtFieldAuditVarianceBySetRpt:message key="DT_UNRESOLVED_QTY" var="varUnresolvedQty"/><display:column property="UNRESOLVED_QTY" 	title="${varUnresolvedQty}" style="text-align: right;width:100;" sortable="true" total="true" headerClass="ShadeDarkOrangeTD" class="ShadeLightOrangeTD" />
				  </logic:equal>
				  <logic:equal name="frmFieldAuditReport" property="dispflaggedDet" value="false">
					<fmtFieldAuditVarianceBySetRpt:message key="DT_FLAGGED_QTY" var="varFlaggedQty"/><display:column property="FLAGGED_QTY" title="${varFlaggedQty}" headerClass="ShadeDarkOrangeTD" style="width:100;" total="true" sortable="true" class="ShadeLightOrangeTD"/>
					<fmtFieldAuditVarianceBySetRpt:message key="DT_UNRESOLVED_QTY" var="varUnresolvedQty"/><display:column property="UNRESOLVED_QTY" title="${varUnresolvedQty}" total="true" class="alignleft;ShadeMedOrangeTD" style="width:100;" sortable="true" headerClass="ShadeDarkOrangeTD" />
				  </logic:equal>
				<display:footer media="html"> 
					<% if(flaggedDet.equals("true")){ %>              		 
            			<tr>
            				<td class="Line" colspan="24"></td>
            			</tr>             	
             		<%}else{ %>
						<tr>
							<td class="Line" colspan="24"></td>
						</tr>
					<%} %>
				<tr>
					<td class = "ShadeDarkGrayTD"> <B> <fmtFieldAuditVarianceBySetRpt:message key="LBL_TOTAL_RECORDS"/>: <%=rowsize %> </B></td>
						<%String strAuditQty 		= ((HashMap)pageContext.getAttribute("totals")).get("column3").toString();
						String strTagQty 			= ((HashMap)pageContext.getAttribute("totals")).get("column4").toString();
						String strOpsQty 		    = ((HashMap)pageContext.getAttribute("totals")).get("column5").toString();
						String strSystemQty 		= ((HashMap)pageContext.getAttribute("totals")).get("column6").toString();
						String strDeviationQty 		= ((HashMap)pageContext.getAttribute("totals")).get("column7").toString();
						
						strAuditQty 	= GmCommonClass.getStringWithCommas(strAuditQty,0);
						strTagQty 		= GmCommonClass.getStringWithCommas(strTagQty,0);
						strOpsQty 		= GmCommonClass.getStringWithCommas(strOpsQty,0);
						strSystemQty 	= GmCommonClass.getStringWithCommas(strSystemQty,0);
						strDeviationQty = GmCommonClass.getStringWithCommas(strDeviationQty,0);
						
					  if(flaggedDet.equals("true")) {
						 strTransferFrom 	= ((HashMap)pageContext.getAttribute("totals")).get("column9").toString();						 
						 strBorrowedFrom 	= ((HashMap)pageContext.getAttribute("totals")).get("column10").toString();
						 strExtra 			= ((HashMap)pageContext.getAttribute("totals")).get("column11").toString();
						 strNotADevPos 		= ((HashMap)pageContext.getAttribute("totals")).get("column12").toString();
						 strUnVoidTag 		= ((HashMap)pageContext.getAttribute("totals")).get("column13").toString();
						 strReturns 		= ((HashMap)pageContext.getAttribute("totals")).get("column15").toString();						 
						 strMissing 		= ((HashMap)pageContext.getAttribute("totals")).get("column16").toString();
						 strTransferTo 		= ((HashMap)pageContext.getAttribute("totals")).get("column17").toString();
						 strLendTo 			= ((HashMap)pageContext.getAttribute("totals")).get("column18").toString();
						 strVerifyPostCnt 	= ((HashMap)pageContext.getAttribute("totals")).get("column19").toString();
						 strNotADevNeg 		= ((HashMap)pageContext.getAttribute("totals")).get("column20").toString();						 
						 strVoidTag 		= ((HashMap)pageContext.getAttribute("totals")).get("column21").toString();
             			 strFlagQty 	  	= ((HashMap)pageContext.getAttribute("totals")).get("column23").toString();
						 strUnresolvedQty 	= ((HashMap)pageContext.getAttribute("totals")).get("column24").toString();
						 strTransferTo  	= GmCommonClass.getStringWithCommas( strTransferTo,0);
						 strBorrowedFrom 	= GmCommonClass.getStringWithCommas(strBorrowedFrom,0);						
						 strExtra 			= GmCommonClass.getStringWithCommas(strExtra,0);
						 strNotADevPos 		= GmCommonClass.getStringWithCommas(strNotADevPos,0);
						 strUnVoidTag 		= GmCommonClass.getStringWithCommas(strUnVoidTag,0);
						 strReturns 		= GmCommonClass.getStringWithCommas(strReturns,0);
						 strTransferFrom 	= GmCommonClass.getStringWithCommas(strTransferFrom,0);
						 strLendTo 			= GmCommonClass.getStringWithCommas(strLendTo,0);	
						 strMissing 		= GmCommonClass.getStringWithCommas(strMissing,0);
						 strVerifyPostCnt 	= GmCommonClass.getStringWithCommas(strVerifyPostCnt,0);
						 strNotADevNeg 		= GmCommonClass.getStringWithCommas(strNotADevNeg,0);
						 strVoidTag 		= GmCommonClass.getStringWithCommas(strVoidTag,0);						 
						 strFlagQty 	  	= GmCommonClass.getStringWithCommas(strFlagQty,0);
						 strUnresolvedQty 	= GmCommonClass.getStringWithCommas(strUnresolvedQty,0);
             		 	} else{              			 
             			 strFlagQty 	  	= ((HashMap)pageContext.getAttribute("totals")).get("column9").toString();
             			 strFlagQty 	  	= GmCommonClass.getStringWithCommas(strFlagQty,0);
             			 strUnresolvedQty 	= ((HashMap)pageContext.getAttribute("totals")).get("column10").toString();
             			 strUnresolvedQty 	= GmCommonClass.getStringWithCommas(strUnresolvedQty,0);
             		 	}
					%>
					 
					<td colspan="2" class = "ShadeDarkGrayTD" style="text-align: right;width:80;" ><B><%=strAuditQty%>		</B> </td>
					<td class = "ShadeDarkGrayTD" style="text-align: right;width:80;" ><B><%=strTagQty %>					</B> </td>
					<td class = "ShadeDarkGrayTD" style="text-align: right;width:80;" ><B><%=strOpsQty %>					</B> </td>
					<td class = "ShadeDarkGrayTD" style="text-align: right;width:80;" ><B><%=strSystemQty %>				</B> </td>
					<td class = "ShadeDarkGrayTD" style="text-align: right;width:80;" ><B><%=strDeviationQty%>				</B> </td>
				<% if(flaggedDet.equals("true")){%> 
             		<td colspan="2" class = "ShadeDarkGrayTD" style="text-align: right;width:50;" ><B><%=strTransferFrom%>	</B> </td>             		 
             		<td class = "ShadeDarkGrayTD" style="text-align: right;width:50;" ><B><%=strBorrowedFrom%>				</B> </td>
             		<td class = "ShadeDarkGrayTD" style="text-align: right;width:50;" ><B><%=strExtra%>						</B> </td>
             		<td class = "ShadeDarkGrayTD" style="text-align: right;width:50;" ><B><%=strNotADevPos%>				</B> </td>
             		<td class = "ShadeDarkGrayTD" style="text-align: right;width:50;" ><B><%=strUnVoidTag%>					</B> </td>
             		<td colspan="2" class = "ShadeDarkGrayTD" style="text-align: right;width:50;" ><B><%=strReturns%>		</B> </td>
             		<td class = "ShadeDarkGrayTD" style="text-align: right;width:50;" ><B><%=strMissing%>					</B> </td>
             		<td class = "ShadeDarkGrayTD" style="text-align: right;width:50;" ><B><%=strTransferTo%>				</B> </td>       
             		<td class = "ShadeDarkGrayTD" style="text-align: right;width:50;" ><B><%=strLendTo%>					</B> </td>
             		<td class = "ShadeDarkGrayTD" style="text-align: right;width:50;" ><B><%=strVerifyPostCnt%>				</B> </td>
             		<td class = "ShadeDarkGrayTD" style="text-align: right;width:50;" ><B><%=strNotADevNeg%>				</B> </td>
             		<td class = "ShadeDarkGrayTD" style="text-align: right;width:50;" ><B><%=strVoidTag%>					</B> </td>             		          		    
             		<td colspan="2" class = "ShadeDarkGrayTD" style="text-align: right;width:100;" ><B><%=strFlagQty%>		</B> </td>
             		<td class = "ShadeDarkGrayTD" style="text-align: right;width:100;" ><B><%=strUnresolvedQty%>			</B> </td>             		
             	<%} else { %>
             		<td colspan="2" class = "ShadeDarkGrayTD" style="text-align: right;width:100;" ><B><%=strFlagQty%>		</B> </td>
             		<td class = "ShadeDarkGrayTD" style="text-align: right;width:100;" ><B><%=strUnresolvedQty%>			</B> </td>
                <%} %>
            	</tr>
            <tr>
                <td class="Line" colspan="24"></td>
            </tr>
			</display:footer>
			</display:table></td>
		</tr>			
		<% if((flaggedDet.equals("true"))  && (rowsize >= 1) ){%> 	
		<tr>
			<td class="Line" colspan="24"></td>
		</tr>				
		<tr>
			<td colspan="4" valign="top" >
				<b>Legend - Column Type:</b><BR>
				<table cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<tr> <td width="20" align="Right">&nbsp;&nbsp;<fmtFieldAuditVarianceBySetRpt:message key="DT_TF"/>  </td> <td width="350">- <fmtFieldAuditVarianceBySetRpt:message key="LBL_TRANSFER_FROM"/>   </td> <td width="20"><fmtFieldAuditVarianceBySetRpt:message key="LBL_RA"/>  </td> <td width="150">- <fmtFieldAuditVarianceBySetRpt:message key="LBL_RETURNS"/>             </td> </tr>
						<tr> <td width="20" align="Right">&nbsp;&nbsp;<fmtFieldAuditVarianceBySetRpt:message key="DT_BF"/>  </td> <td width="350">- <fmtFieldAuditVarianceBySetRpt:message key="LBL_BORROWED_FROM"/>   </td> <td width="20"><fmtFieldAuditVarianceBySetRpt:message key="DT_M"/>   </td> <td width="150">- <fmtFieldAuditVarianceBySetRpt:message key="LBL_MISSING"/>             </td> </tr>
						<tr> <td width="20" align="Right">&nbsp;&nbsp;<fmtFieldAuditVarianceBySetRpt:message key="DT_EXA"/> </td> <td width="350">- <fmtFieldAuditVarianceBySetRpt:message key="LBL_EXTRA"/>           </td> <td width="20"><fmtFieldAuditVarianceBySetRpt:message key="DT_TT"/>  </td> <td width="200">- <fmtFieldAuditVarianceBySetRpt:message key="LBL_TRANSFER_TO"/>         </td> </tr>
						<tr> <td width="20" align="Right">&nbsp;&nbsp;<fmtFieldAuditVarianceBySetRpt:message key="DT_ND"/>  </td> <td width="350">- <fmtFieldAuditVarianceBySetRpt:message key="LBL_NOT_A_DEVIATION"/> </td> <td width="20"><fmtFieldAuditVarianceBySetRpt:message key="DT_LT"/>  </td> <td width="200">- <fmtFieldAuditVarianceBySetRpt:message key="LBL_LEND_TO"/>             </td> </tr>
						<tr> <td width="20" align="Right">&nbsp;&nbsp;<fmtFieldAuditVarianceBySetRpt:message key="DT_UV"/>   </td> <td width="350">- <fmtFieldAuditVarianceBySetRpt:message key="LBL_UNVOIDED_TAG"/>    </td> <td width="20"><fmtFieldAuditVarianceBySetRpt:message key="DT_VPC"/> </td> <td width="200">- <fmtFieldAuditVarianceBySetRpt:message key="LBL_VERIFIED_POST_COUNT"/> </td> </tr>
						<tr> <td width="20" align="Right">&nbsp;&nbsp;    </td> <td width="350"> 				  </td> <td width="20"><fmtFieldAuditVarianceBySetRpt:message key="DT_ND"/>  </td> <td width="200">- <fmtFieldAuditVarianceBySetRpt:message key="LBL_NOT_A_DEVIATION"/>     </td> </tr>
						<tr> <td width="20" align="Right">&nbsp;&nbsp;    </td> <td width="350">                  </td> <td width="20"><fmtFieldAuditVarianceBySetRpt:message key="DT_VT"/>  </td> <td width="200">- <fmtFieldAuditVarianceBySetRpt:message key="LBL_VOIDED_TAG_ONLY"/> 	   </td> </tr>						
					</td>			
				</tr>
			</table>
			</td>
			<td colspan="2">&nbsp;</td>				
		</tr>	
		<%} %>	
     </table>
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

