<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtFieldAuditFlagAssocRecInc" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- accounts\fieldaudit\GmFieldAuditFlagAssocRecInc.jsp -->
<fmtFieldAuditFlagAssocRecInc:setLocale value="<%=strLocale%>"/>
<fmtFieldAuditFlagAssocRecInc:setBundle basename="properties.labels.accounts.fieldaudit.GmFieldAuditFlagAssocRecInc"/>

<table border="0" height="100%" width="1300" cellspacing="0"
	cellpadding="0">
	<tr>
		<td height="25" class="RightDashBoardHeader">&nbsp;<fmtFieldAuditFlagAssocRecInc:message key="LBL_OTHER_ASSOCIATED_RECORDS_THAT_INCREASE_THE_UNRESOLVED_QTY"/></td>
	</tr>
	<tr>
		<td><display:table
				name="requestScope.frmFieldAuditFlagVariance.alPAssociatedReport"
				requestURI="/gmFieldAuditFlagVar.do?"
				excludedParams="strOpt" class="its">
				<fmtFieldAuditFlagAssocRecInc:message key="DT_TAG" var="varTag"/><display:column property="TAG_ID" title="${varTag}" style="width:150;" />
				<fmtFieldAuditFlagAssocRecInc:message key="DT_CONTROL" var="varControl"/><display:column property="CNUM" title="${varControl}" style="width:150;" />
				<fmtFieldAuditFlagAssocRecInc:message key="DT_FLAG" var="varFlag"/><display:column property="FLAG" title="${varFlag}" style="width:350;" />
				<fmtFieldAuditFlagAssocRecInc:message key="DT_REASON" var="varReason"/><display:column property="REASON" title="${varReason}" style="width:450;" />
			</display:table></td>
	</tr>
</table>

<table border="0" height="100%" width="1300" cellspacing="0"
	cellpadding="0">
	<tr>
		<td height="25" class="RightDashBoardHeader"><fmtFieldAuditFlagAssocRecInc:message key="LBL_OTHER_ASSOCIATED_RECORDS_THAT_DECREASE_UNRESOLVED_QTY" />&nbsp;</td>
	</tr>
	<tr>
		<td><display:table
				name="requestScope.frmFieldAuditFlagVariance.alNAssociatedReport"
				requestURI="/gmFieldAuditFlagVar.do?"
				excludedParams="strOpt" class="its">
				<fmtFieldAuditFlagAssocRecInc:message key="DT_TAG" var="varTag"/><display:column property="TAG_ID" title="${varTag}" style="width:150;" />
				<fmtFieldAuditFlagAssocRecInc:message key="DT_CONTROL" var="varControl"/><display:column property="CNUM" title="${varControl}" style="width:150;" />
				<fmtFieldAuditFlagAssocRecInc:message key="DT_FLAG" var="varFlag"/><display:column property="FLAG" title="${varFlag}" style="width:350;" />
				<fmtFieldAuditFlagAssocRecInc:message key="DT_REASON" var="varReason"/><display:column property="REASON" title="${varReason}" style="width:450;" />
			</display:table></td>
	</tr>
</table>