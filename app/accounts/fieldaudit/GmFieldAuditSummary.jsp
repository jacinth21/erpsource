<%
/*******************************************************************************
 * File		 	: GmFieldAuditSummary.jsp
 * Desc		 	: This screen is used for displayimg the Field Audit Summary.
 * Version	 	: 1.0
 * author		: Jignesh Shah
*******************************************************************************/
%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import ="com.globus.accounts.fieldaudit.forms.GmFieldAuditSummaryForm"%>
<%@ taglib prefix="fmtFieldAuditSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- accounts\fieldaudit\GmFieldAuditSummary.jsp -->
<fmtFieldAuditSummary:setLocale value="<%=strLocale%>"/>
<fmtFieldAuditSummary:setBundle basename="properties.labels.accounts.fieldaudit.GmFieldAuditSummary"/>
<bean:define id="xmlGridData" name="frmFieldAuditSummary" property="xmlGridData" type="java.lang.String"></bean:define>
<%

	String strAccountFaJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_FIELDAUDIT");
	String strWikiTitle = GmCommonClass.getWikiTitle("FA_SUMMARY_REPORT");
	String strApplnDateFmt = strGCompDateFmt;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Field Audit Summary Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<link rel="STYLESHEET" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/skins/dhtmlxlayout_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxTreeGrid/dhtmlxtreegrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxTreeGrid/ext/dhtmlxtreegrid_filter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strAccountFaJsPath%>/GmFieldAuditSummary.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     #faSumFromDt{width:74%;}
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script>
	var objGridData;
	objGridData = '<%=xmlGridData%>';
	var gridObj = '';
	var format = '<%=strApplnDateFmt%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();" onkeyup="fnEnter();" >
<html:form action="/gmFieldAuditSummaryAction.do?method=execute">
<html:hidden property="strOpt" value="" />

	<table class="DtTable1000" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4">&nbsp;<fmtFieldAuditSummary:message key="LBL_SUMMARY_REPORT"/></td>
			
			<td  height="25" class="RightDashBoardHeader"><fmtFieldAuditSummary:message key="IMG_HELP" var="varHelp"/>
				<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr height="30" >
			<td class="RightTableCaption" align="right" HEIGHT="24" width="170"><fmtFieldAuditSummary:message key="LBL_LOCKED_FROM"/>:&nbsp;</td>
			<td align="left" width="100">
				<gmjsp:calendar
						SFFormName="frmFieldAuditSummary" controlName="faSumFromDt"
						gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" />
			</td>
			<td class="RightTableCaption" align="right" HEIGHT="24" width="150"><fmtFieldAuditSummary:message key="LBL_LOCKED_TO"/>:&nbsp;</td>
			<td align="left" width="160">
				<gmjsp:calendar
						SFFormName="frmFieldAuditSummary" controlName="faSumToDt"
						gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" />
			</td>
			<td width="400">&nbsp;<fmtFieldAuditSummary:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" name="btn_Load" gmClass="button" style="width: 6em" buttonType="Load" onClick="fnLoad();" /></td>
		</tr>
		<tr>
			<td class="LLine" colspan="5" height="1"></td>
		</tr>
		<%if(!xmlGridData.equals("")){%>
		<tr>
	    	<td colspan="5" align="center">
				<div id="GridData" style="grid" height="500px" width="1000px"></div>
		   	</td>	
		</tr>
		<%}else{%>
		<tr>
			<td colspan="5" align="center" height="30" class="RightText"><fmtFieldAuditSummary:message key="LBL_NO_DATA_AVAILABLE"/></td>
		</tr>
		<%}%>
		<tr>
			<td height="1" colspan="5" class="LLine"></td>
		</tr>
		<%if(xmlGridData.indexOf("cell") != -1) {%>
		<tr>
			<td colspan="5" align="center">
			    <div class='exportlinks'><fmtFieldAuditSummary:message key="LBL_EXPORT_OPTIONS"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnDownloadXLS();"><fmtFieldAuditSummary:message key="LBL_EXCEL"/>  </a></div>
			</td>
		</tr>
		<%} %>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>