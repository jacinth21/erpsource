<%
/*******************************************************************************
 * File		 	: GmFieldAuditEditUploadData.jsp
 * Desc		 	: This screen is used for to edit the Field Audit Data.
 * Version	 	: 1.0
 * author		: Jignesh Shah
*******************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ taglib prefix="fmtFieldAuditEditUploadData" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- accounts/fieldaudit/GmFieldAuditEditUploadData.jsp -->
<fmtFieldAuditEditUploadData:setLocale value="<%=strLocale%>"/>
<fmtFieldAuditEditUploadData:setBundle basename="properties.labels.accounts.fieldaudit.GmFieldAuditEditUploadData"/>


<bean:define id="alLocDetails" name="frmFieldAuditEditUploadData" property="alLocDetails" type="java.util.ArrayList"></bean:define>
<bean:define id="auditEntryID" name="frmFieldAuditEditUploadData" property="auditEntryID" type="java.lang.String"></bean:define>
<bean:define id="tagID" name="frmFieldAuditEditUploadData" property="tagID" type="java.lang.String"></bean:define>
<bean:define id="reTag" name="frmFieldAuditEditUploadData" property="reTag" type="java.lang.String"></bean:define>
<bean:define id="distName" name="frmFieldAuditEditUploadData" property="distName" type="java.lang.String"></bean:define>
<bean:define id="auditId" name="frmFieldAuditEditUploadData" property="auditId" type="java.lang.String"></bean:define>
<bean:define id="distId" name="frmFieldAuditEditUploadData" property="distId" type="java.lang.String"></bean:define>
<bean:define id="auditorId" name="frmFieldAuditEditUploadData" property="auditorId" type="java.lang.String"></bean:define>
<bean:define id="pnum" name="frmFieldAuditEditUploadData" property="partNumber" type="java.lang.String"></bean:define>
<bean:define id="cnum" name="frmFieldAuditEditUploadData" property="controlNumber" type="java.lang.String"></bean:define>
<bean:define id="countedDate" name="frmFieldAuditEditUploadData" property="countedDate" type="java.lang.String"></bean:define>
<bean:define id="comments" name="frmFieldAuditEditUploadData" property="comments" type="java.lang.String"></bean:define>
<bean:define id="locationComments" name="frmFieldAuditEditUploadData" property="locationComments" type="java.lang.String"></bean:define>
<bean:define id="strOpt" name="frmFieldAuditEditUploadData" property="strOpt" scope="request" type="java.lang.String"></bean:define>
<bean:define id="setCountFL" name="frmFieldAuditEditUploadData" property="setCountFL" scope="request" type="java.lang.String"></bean:define>
<bean:define id="partCountFL" name="frmFieldAuditEditUploadData" property="partCountFL" scope="request" type="java.lang.String"></bean:define>
<bean:define id="countedBy" name="frmFieldAuditEditUploadData" property="countedBy" scope="request" type="java.lang.String"></bean:define>
<bean:define id="loctyp" name="frmFieldAuditEditUploadData" property="loctyp" scope="request" type="java.lang.String"></bean:define>
<bean:define id="hmAuditEntryDetails" name="frmFieldAuditEditUploadData" property="hmAuditEntryDetails" scope="request" type="java.util.HashMap"></bean:define>

 
<%
String strAccountFaJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_FIELDAUDIT");
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.accounts.fieldaudit.GmFieldAuditEditUploadData", strSessCompanyLocale);
	String strMessage = GmCommonClass.parseNull((String)request.getAttribute("RESPONSE"));
	String strWikiTitle = GmCommonClass.getWikiTitle("EDIT_UPLOADED_DATA");
	String strTitle = "";
	String strMandatory = "";
	boolean strDisable = false;
	String strDisableDpdwn = "";
	String strTodaysDate = "";
	String strAppRecon = "";
	String strBtnDisabled = "";
	String strLockedSetIdFlag = "";
	
	strAppRecon = GmCommonClass.parseNull((String)hmAuditEntryDetails.get("RECONCILED"));
	strLockedSetIdFlag = GmCommonClass.parseNull((String)hmAuditEntryDetails.get("LOCKEDSETFL"));
	if(strAppRecon.equals("Y")){
		strBtnDisabled = "true";
	}
	if(strOpt.equals("lockedset") || strOpt.equals("savelockedset"))
	{
		strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_UPDATE_LOCKED_SET")); 
		strDisable = true;
		strDisableDpdwn ="Disabled";
	}
	else
	{
		strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_EDIT_UPLOADED_DATA"));
		strMandatory = "*";
	}
	
	strTodaysDate = GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate"));
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Edit Uploaded Data</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strAccountFaJsPath%>/GmFieldAuditEditUploadData.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"></script>


<script language="JavaScript">
	var strToday = "<%=strTodaysDate%>";
	var dhxWins,w1;
	var format = '<%=strApplDateFmt%>';
	var recon = '<%=strAppRecon%>';
	var lockedSetFl = '<%=strLockedSetIdFlag%>';
	var lblPartNum = '<fmtFieldAuditEditUploadData:message key="LBL_PART" />';
	var lblControlNum = '<fmtFieldAuditEditUploadData:message key="LBL_CONTROL" />';
	var lblLocation = '<fmtFieldAuditEditUploadData:message key="LBL_LOCATION" />';
	var lblLocationDtls = '<fmtFieldAuditEditUploadData:message key="LBL_LOCATION_DETAILS" />';
	var lblSetId = '<fmtFieldAuditEditUploadData:message key="LBL_SET_ID" />';
	var lblCountDt = '<fmtFieldAuditEditUploadData:message key="LBL_COUNTED_DATE" />';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnload();">
<html:form action="/gmFieldAuditEditUploadData.do?method=editUploadData" >
	
	<html:hidden property="strOpt" value="<%=strOpt%>" /> 
	<html:hidden property="auditEntryID" value="<%=auditEntryID%>" />
	<html:hidden property="auditId" value="<%=auditId%>" />
	<html:hidden property="distId" value="<%=distId%>"/>
	<html:hidden property="auditorId" value="<%=auditEntryID%>"/>
	<html:hidden property="tagID" />
	<html:hidden property="reTag" />	
	<html:hidden property="distName" />
	<html:hidden property="setCountFL" />
	<html:hidden property="partCountFL" />
	<html:hidden property="countedBy" />
	<html:hidden property="hauditId" />
	<html:hidden property="hdistId" />
	<html:hidden property="hauditorId" />
	<html:hidden property="tagStatus"/>
	<html:hidden property="approvedFl"/>
	
	<table border="0" width="800" cellspacing="0" cellpadding="0">	
		<tr>
			<td bgcolor="#666666" width="1" rowspan="10"></td>		
			
			<td  height="25" class="RightDashBoardHeader" ><%=strTitle%></td>
            <td  height="25" class="RightDashBoardHeader">
            	<fmtFieldAuditEditUploadData:message key="IMG_HELP" var="varHelp"/>
                <img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
            </td>   
            <td bgcolor="#666666" width="1" rowspan="10"></td>  	     
		</tr>
		<tr>
			<td width="800" height="100" valign="top" colspan="2">
				<table border="0" bordercolor="red" width="100%" cellspacing="0" cellpadding="0">
				<% if (!strMessage.equals("")) { %>
					<tr>
						<td class="RightTableCaption" align="center" HEIGHT="24" colspan="2"> <font color = "green"> <%=strMessage%> </font></td>
					</tr>
				<% } %>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24" width="200">&nbsp;<fmtFieldAuditEditUploadData:message key="LBL_FIELD_SALES" />:</td>
						<td colspan="3">&nbsp;<bean:write name="frmFieldAuditEditUploadData" property="distName"/>						
						</td>	
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right" HEIGHT="24" width="200"><fmtFieldAuditEditUploadData:message key="LBL_TAG_ID" />:</td>
						<td colspan="3">&nbsp;<bean:write name="frmFieldAuditEditUploadData" property="tagID"/>	</td>
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24" width="200"><font color="red"><%=strMandatory%></font>&nbsp;<fmtFieldAuditEditUploadData:message key="LBL_PART" />:</td>
						<td colspan="3">&nbsp;<html:text maxlength="20" property="partNumber"  size="15" value="<%=pnum%>" disabled="<%=strDisable%>"  
							styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
						</td>	
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right" HEIGHT="24" width="200"><font color="red"><%=strMandatory%></font>&nbsp;<fmtFieldAuditEditUploadData:message key="LBL_CONTROL" />:</td>
						<td colspan="3">&nbsp;<html:text property="controlNumber" maxlength="20" size="15" value="<%=cnum%>" disabled="<%=strDisable%>" 
							styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
						</td>	
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24" width="200">
							<% if(strOpt.equals("lockedset") || strOpt.equals("savelockedset")){ %>
							<font color="red">*</font>
							<% } %>	
							&nbsp;<fmtFieldAuditEditUploadData:message key="LBL_SET_ID" />:
						</td>
						<td>&nbsp;
							<gmjsp:dropdown controlName="setID" width="300"	SFFormName="frmFieldAuditEditUploadData" 
								SFSeletedValue="setID" SFValue="alSet"   codeId="ID" codeName="IDNAME"  defaultValue= "[Choose One]" />
						</td>
					<% if(strOpt.equals("lockedset") || strOpt.equals("savelockedset")){ %>
						<td class="RightTableCaption" align="right" HEIGHT="24" width="200">&nbsp;<fmtFieldAuditEditUploadData:message key="LBL_UPDATE_LOCKED_SET" />:</td>
						<td width="80">&nbsp;<html:checkbox property="lockedSet"></html:checkbox></td>
					<% } %>	
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right" HEIGHT="24" width="200">&nbsp;<fmtFieldAuditEditUploadData:message key="LBL_COMMENTS" />:</td>
						<td colspan="3">&nbsp;
							<html:textarea property="comments" value="<%=comments%>" disabled="<%=strDisable%>" cols="41" 
								styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
						</td>	
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24" width="200">&nbsp;<fmtFieldAuditEditUploadData:message key="LBL_STATUS" />:</td>
						<td colspan="3">&nbsp;
							<gmjsp:dropdown controlName="statusID"	SFFormName="frmFieldAuditEditUploadData"  SFSeletedValue="statusID"
								SFValue="alStatus"   codeId="CODEID" codeName="CODENM"  defaultValue= "[Choose One]" disabled="<%=strDisableDpdwn%>"/>
						</td>	
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right" HEIGHT="24" width="200">&nbsp;<fmtFieldAuditEditUploadData:message key="LBL_RETAG" />:</td>
						<td colspan="3">&nbsp;<bean:write name="frmFieldAuditEditUploadData" property="reTag"/>	</td>						
						</td>	
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption"  align="right" HEIGHT="24" width="200">&nbsp;<fmtFieldAuditEditUploadData:message key="LBL_FLAG_TYPE" />:</td>
						<td colspan="3">&nbsp;
							<gmjsp:dropdown width="150" controlName="flagType"	SFFormName="frmFieldAuditEditUploadData"  SFSeletedValue="flagType"
								SFValue="alFlagType"   codeId="CODEID" codeName="CODENM"  defaultValue= "[Choose One]"  onChange="fnChangeFlag(this);" disabled="<%=strDisableDpdwn%>" />
						</td>	
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption"  align="right" HEIGHT="24" width="200">&nbsp;<fmtFieldAuditEditUploadData:message key="LBL_FLAG_DETAILS" />:</td>
						<td colspan="3">&nbsp;
						<logic:equal property="showDist" name="frmFieldAuditEditUploadData" value="Yes">
							<gmjsp:dropdown width="300" controlName="flagDetID"	SFFormName="frmFieldAuditEditUploadData"  SFSeletedValue="flagDetID"
								SFValue="alRefId"   codeId="ID" codeName="NAME"  defaultValue= "[Choose One]" disabled="<%=strDisableDpdwn%>" />
						</logic:equal>
						<logic:equal property="showDist" name="frmFieldAuditEditUploadData" value="No">
						<gmjsp:dropdown controlName="flagDetID"	SFFormName="frmFieldAuditEditUploadData"  SFSeletedValue="flagDetID"
								SFValue="alRefId"   codeId="CODEID" codeName="CODENM"  defaultValue= "[Choose One]" />
						</logic:equal>		
						<logic:equal property="showRA" name="frmFieldAuditEditUploadData" value="Yes">
						<html:text property="flagDetID" disabled="true"> </html:text>
						</logic:equal>
						</td>	
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24" width="200">&nbsp;<fmtFieldAuditEditUploadData:message key="LBL_LOCATION_TYPE" />:</td>
						<td colspan="3">&nbsp;
							<gmjsp:dropdown controlName="loctyp" controlId="loctyp" SFFormName="frmFieldAuditEditUploadData"  SFSeletedValue="loctyp" SFValue="alLocationType" 
								  codeId="CODEID" codeName="CODENM"  defaultValue= "[Choose One]" disabled="<%=strDisableDpdwn%>" onChange="fnLocation();" />
						</td>	
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right" HEIGHT="24" width="200">&nbsp;<fmtFieldAuditEditUploadData:message key="LBL_LOCATION_COMMENTS" />:</td>
						<td colspan="3">&nbsp;
							<html:textarea property="locationComments" value="<%=locationComments%>" disabled="<%=strDisable%>" cols="41" 
								styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
						</td>	
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr>
          				<td class="RightTableCaption"  align="right" HEIGHT="24" width="200"><font color="red"><%=strMandatory%></font>&nbsp;<fmtFieldAuditEditUploadData:message key="LBL_LOCATION" />:</td>
						<td colspan="3">&nbsp;
							<gmjsp:dropdown width="300" controlName="locationID"	SFFormName="frmFieldAuditEditUploadData"  SFSeletedValue="locationID"
								 SFValue="alLocation"   codeId="ID" codeName="NAME"  defaultValue= "[Choose One]"  disabled="<%=strDisableDpdwn%>" onChange="fnLocationDetails();"  />
						</td>
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption"  align="right" HEIGHT="24" width="200">&nbsp;<fmtFieldAuditEditUploadData:message key="LBL_ADDRESS_TYPE" /> :</td>
						<td colspan="3">&nbsp;
							<gmjsp:dropdown width="110" controlName="addressType"	SFFormName="frmFieldAuditEditUploadData"  SFSeletedValue="addressType"
								 SFValue="alAddressType"   codeId="CODEID" codeName="CODENM" defaultValue= "[Choose One]" disabled="<%=strDisableDpdwn%>" />
						</td>	
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right"  HEIGHT="24" width="200"><font color="red"><%=strMandatory%></font>&nbsp;<fmtFieldAuditEditUploadData:message key="LBL_LOCATION_DETAILS" />:</td>
						<td colspan="3">&nbsp;
							<gmjsp:dropdown width="350" controlName="locationDet" SFFormName="frmFieldAuditEditUploadData" SFSeletedValue="locationDet" defaultValue= "[Choose One]" 
								SFValue="alLocDetails" codeId="LOCATION_ID" codeName="LOCATION_DETAILS"  disabled="<%=strDisableDpdwn%>" onChange="fnAddressType();"/>
						</td>	
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right" HEIGHT="24" width="200"><fmtFieldAuditEditUploadData:message key="LBL_SET_COUNT" /> :</td>
						<td colspan="3">&nbsp;<bean:write name="frmFieldAuditEditUploadData" property="setCountFL"/></td>
					</tr>
				   <tr><td colspan="4" class="LLine" height="1"></td></tr>
				   <tr>
						<td class="RightTableCaption" align="right" HEIGHT="24" width="200"><fmtFieldAuditEditUploadData:message key="LBL_PART_COUNT" /> :</td>
						<td colspan="3">&nbsp;<bean:write name="frmFieldAuditEditUploadData" property="partCountFL"/></td>
					</tr>
				   <tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right" HEIGHT="24" width="200"><fmtFieldAuditEditUploadData:message key="LBL_COUNTED_BY" /> :</td>
						<td colspan="3">&nbsp;<bean:write name="frmFieldAuditEditUploadData" property="countedBy"/>	</td>
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24" width="200"><font color="red"><%=strMandatory%></font>&nbsp;<fmtFieldAuditEditUploadData:message key="LBL_COUNTED_DATE" /> :</td>
						<td colspan="3">&nbsp;
						<% if(strOpt.equals("lockedset") || strOpt.equals("savelockedset"))	
						{ %>
							<bean:write name="frmFieldAuditEditUploadData" property="countedDate"/>
						<% }
						else{ %>
							<gmjsp:calendar SFFormName="frmFieldAuditEditUploadData" controlName="countedDate" gmClass="InputArea" 
									onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
						<% } %>	
						</td>
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr class="shade">
						<td colspan="4" height="30" align="center">&nbsp;
							<fmtFieldAuditEditUploadData:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" name="Btn_Submit" onClick="javascript:fnSubmit();" buttonType="Save" disabled="<%=strBtnDisabled %>" ></gmjsp:button>&nbsp;
							<% if(strOpt.equals("lockedset") || strOpt.equals("savelockedset"))	
							{ %>
								<fmtFieldAuditEditUploadData:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="&nbsp;${varClose}&nbsp;" buttonType="Load" onClick="javascript:fnBack();"></gmjsp:button>
							<% }
						 	else{ %>
								<fmtFieldAuditEditUploadData:message key="BTN_BACK" var="varBack"/><gmjsp:button value="&nbsp;${varBack}&nbsp;" buttonType="Load" onClick="javascript:fnBack();"></gmjsp:button>
							<% } %>	
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
	 
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>