<%
/**************************************************************************************************
	* File		 	: GmFieldAuditApprReconcile.jsp
	* Desc		 	: This screen is used to display the Flagged for Approval Report.
	* Version	 	: 1.0
	* author		: HReddi
****************************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Iterator,java.lang.*"%>
<%@ taglib prefix="fmtFieldAuditApprReconcile" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- accounts/fieldaudit/GmFieldAuditApprReconcile.jsp -->
<fmtFieldAuditApprReconcile:setLocale value="<%=strLocale%>"/>
<fmtFieldAuditApprReconcile:setBundle basename="properties.labels.accounts.fieldaudit.GmFieldAuditApprReconcile"/>
<bean:define id="xmlGridData" name="frmFieldAuditApprReconcile" property="xmlGridData" type="java.lang.String"></bean:define>
<bean:define id="deviationType" name="frmFieldAuditApprReconcile" property="deviationType" type="java.lang.String"></bean:define> 
<bean:define id="strDisableButton" name="frmFieldAuditApprReconcile" property="strDisableButton" type="java.lang.String"></bean:define>
<%
String strAccountFaJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_FIELDAUDIT");
String strWikiTitle="";
String strHeader="";
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.accounts.fieldaudit.GmFieldAuditApprReconcile", strSessCompanyLocale);
HashMap hmLoop = new HashMap();
ArrayList alList = new ArrayList();

	if (deviationType.equals("Approval")){
		 strWikiTitle = GmCommonClass.getWikiTitle("FLAG_FOR_APPROVAL");
		 strHeader= GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_FLAGGED_FOR_APPROVAL"));
	}
	else{
		 strWikiTitle = GmCommonClass.getWikiTitle("APPR_FOR_RECONCILE");
		 strHeader=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_APPROVED_FOR_RECONCILE"));
	}	
%>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">

<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script language="JavaScript" src="<%=strAccountFaJsPath%>/GmFieldAuditApprReconcile.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>

<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>


<script type="text/javascript">
var objGridData;
var deviationType;
objGridData = '<%=xmlGridData%>';
deviationType = '<%=deviationType%>';
</script>

<BODY leftmargin="20" topmargin="10" onload="fnOnLoad();">
<html:form action="/gmFieldAuditApprReconcile.do">
<html:hidden property="haction"/>
<html:hidden property="deviationType"/>
<html:hidden property="inputString"/>
<table border="0" style="overflow: auto;" class="DtTable1200" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="3" height="25" class="RightDashBoardHeader"><%=strHeader%></td>
		<td class="RightDashBoardHeader"><fmtFieldAuditApprReconcile:message key="IMG_HELP" var="varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
	</tr>
	<tr>
	<logic:notEqual name="frmFieldAuditApprReconcile" property="message" value="" >
	<tr>
		<td height="1" colspan="8" class="LLine"></td>
	</tr>
	<tr>
	<td colspan="6" height="30">
		<font style="color: green" size="2"><fmtFieldAuditApprReconcile:message key="LBL_FOLLOWING_TRANSACTION_ID_CREATED"/>: <b><bean:write name="frmFieldAuditApprReconcile" property="message" /></b></font>
	</td>
	</tr>
	</logic:notEqual>
		<td colspan="8">
		<logic:equal name="frmFieldAuditApprReconcile" property="deviationType" value="Approval" >
		<div id="FlagApprovalRpt" style="height:520px; width:1200px"></div>
		<div id="pagingArea" style=" width:1200px"></div>
		</logic:equal>
		<logic:equal name="frmFieldAuditApprReconcile" property="deviationType" value="Reconcile" >
		<div id="ApprovedReconcileRpt" style="height:520px; width:1202px"></div>
		<div id="pagingArea" style=" width:1200px"></div>
		</logic:equal>
		</td>
	</tr>
	 <% String strDisableBtnVal = strDisableButton;
		   if(strDisableBtnVal.equals("disabled")){
				  strDisableBtnVal ="true";
		   }
     %>
	<%if( xmlGridData.indexOf("cell") != -1) {%>
	<tr>
		<td height="1" colspan="8" class="LLine"></td>
	</tr>
	<tr>
		<td height="40" colspan="8" align="center">
			<logic:equal name="frmFieldAuditApprReconcile"  property="deviationType" value="Approval">
			<fmtFieldAuditApprReconcile:message key="BTN_APPROVE" var="varApprove"/><gmjsp:button value="${varApprove}" name="Btn_Approve" buttonType="Save" gmClass="button" onClick="javascript:fnApproval();" disabled="<%=strDisableBtnVal%>" />
			</logic:equal>
			<logic:equal name="frmFieldAuditApprReconcile"  property="deviationType" value="Reconcile">
			<fmtFieldAuditApprReconcile:message key="BTN_RECONCILE" var="varReconcile"/><gmjsp:button value="${varReconcile}" name="Btn_Reconcile" gmClass="button" buttonType="Save" onClick="javascript:fnReconcile();" disabled="<%=strDisableBtnVal%>" />
			</logic:equal>
		</td>		
	</tr>
	<tr>
		<td height="1" colspan="8" class="LLine"></td>
	</tr>
	<tr>
		<td colspan="8" align="center">
		<div class='exportlinks'><fmtFieldAuditApprReconcile:message key="LBL_EXPORT_OPTIONS"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnDownloadXLS();"><fmtFieldAuditApprReconcile:message key="LBL_EXCEL"/>  </a></div>
		</td>
	</tr>
	<%} %>	
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
