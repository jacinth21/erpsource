 <%
/**********************************************************************************
 * File		 		: GmFieldAuditSetCostRpt.jsp .jsp
 * Desc		 		: This screen is used for displayimg the Set Cost Report.
 * Version	 		: 1.0
 * author			: 
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.accounts.fieldaudit.forms.GmFieldAuditSetCostForm"%>
<%@ taglib prefix="fmtFieldAuditSetCostRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- accounts\fieldaudit\GmFieldAuditSetCostRpt.jsp -->
<fmtFieldAuditSetCostRpt:setLocale value="<%=strLocale%>"/>
<fmtFieldAuditSetCostRpt:setBundle basename="properties.labels.accounts.fieldaudit.GmFieldAuditSetCostRpt"/>
<bean:define id="xmlGridData" name="frmFieldAuditSetCost" property="xmlGridData" type="java.lang.String"> </bean:define>
<%

String strAccountFaJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_FIELDAUDIT");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Cost Update</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strAccountFaJsPath%>/GmFieldAuditSetCostRpt.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script>
var objGridData;
objGridData = '<%=xmlGridData%>';
var gridObj ='';
</script>
</HEAD>
<%
String strWikiTitle = GmCommonClass.getWikiTitle("SET_COST_REPORTS"); 
%>
<BODY leftmargin="20" topmargin="10" onload="fnOnLoad();" >
<html:form action="/gmFieldAuditSetCost.do?method=fieldAuditSetCostRpt">
<html:hidden  property="attType"/>
	<table class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="3">&nbsp;<fmtFieldAuditSetCostRpt:message key="LBL_SET_COST_REPORT"/></td>
			<td class="RightDashBoardHeader"><fmtFieldAuditSetCostRpt:message key="IMG_HELP" var="varHelp"/> <img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
		</tr>	
			<%if(!xmlGridData.equals("")){%>
				<tr>
					<td colspan="8">
						<div  id="SetCostReport" style="" height="500px" width="870px"></div>
						<div id="pagingArea" width="870px"></div>
					</td>
				</tr>
				<tr>
					<td height="1" colspan="8" class="LLine"></td>
				</tr>			
			<%}else{%>
				<tr>
					<td colspan="8" height="1" class="LLine"></td></tr>
				<tr>
					<td colspan="8" align="center" height="30" class="RightText">
					<fmtFieldAuditSetCostRpt:message key="LBL_NO_DATA_AVAILABLE"/>
					</td>
				</tr>
			<%}%>
		<tr>
			<td height="1" colspan="8" class="LLine"></td>
		</tr>
		<%if( xmlGridData.indexOf("cell") != -1) {%>
		<tr>
			<td colspan="8" align="center">
			    <div class='exportlinks'><fmtFieldAuditSetCostRpt:message key="LBL_EXPORT_OPTIONS"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnDownloadXLS();"><fmtFieldAuditSetCostRpt:message key="LBL_EXCEL"/>  </a></div>
			</td>
		</tr>
		<%} %>			
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
