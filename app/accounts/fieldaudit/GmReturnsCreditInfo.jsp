<%
/**************************************************************************************************
	* File		 	: GmReturnsCreditInfo.jsp
	* Desc		 	: This screen is used to show the information about the Returns Credit Details.
	* Version	 	: 1.0
	* author		: Hreddi
****************************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Iterator,java.lang.*"%>
<%@ taglib prefix="fmtReturnsCreditInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- \accounts\fieldaudit\GmReturnsCreditInfo.jsp -->
<fmtReturnsCreditInfo:setLocale value="<%=strLocale%>"/>
<fmtReturnsCreditInfo:setBundle basename="properties.labels.accounts.fieldaudit.GmFieldSalesLockInc"/>

<bean:define id="xmlGridData" name="frmReturnsCreditInfo" property="xmlGridData" type="java.lang.String"></bean:define>
<bean:define id="refId" name="frmReturnsCreditInfo" property="refId" type="java.lang.String"></bean:define>
<bean:define id="hRAID" name="frmReturnsCreditInfo" property="hRAID" type="java.lang.String"></bean:define>
<%
String strAccountFaJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_FIELDAUDIT");
%>
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css"></link>
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" type="text/css"	href="<%=strCssPath%>/displaytag.css">

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strAccountFaJsPath%>/GmReturnsCreditInfo.js"></script>

<script type="text/javascript">
var objGridData;
var dhxWins,popupWindow;
objGridData = '<%=xmlGridData%>';
</script>

<BODY leftmargin="20" topmargin="10" onload="fnOnLoad();">
<html:form action="/gmReturnsCreditInfo.do?method=loadReturnsCreditedInfo">
<html:hidden property="refId" value="<%=refId%>"/>
<html:hidden property="hRAID" value="<%=hRAID%>"/>
<table border="0" style="overflow: auto;" class="dttable950" cellspacing="0" cellpadding="0">
	<tr>
		<td height="25" class="RightDashBoardHeader">&nbsp;<fmtReturnsCreditInfo:message key="LBL_RETURNS_CREDITED_INFO."/></td>
	</tr>
	<tr>
		<td colspan="8">
		<div id="Div_ReturnCredit" style="height: 320px;"></div>
		</td>
	</tr>
	<tr>
		<td height="40" align="center">
			<%if( xmlGridData.indexOf("cell") != -1) {%>
			<fmtReturnsCreditInfo:message key="BTN_APPLY" var="varApply"/><gmjsp:button value="&nbsp;${varApply}&nbsp;" gmClass="button" buttonType="Save" onClick="javascript:fnApply();"/>
			<%}%>
			<fmtReturnsCreditInfo:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="&nbsp;${varClose}&nbsp;" gmClass="button" buttonType="Load" onClick="javascript:fnClose();"/>
		</td>
	</tr>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
