<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>
<%@ taglib prefix="fmtFieldAuditConfirmation" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- accounts/fieldaudit/GmFieldAuditConfirmation.jsp -->
<fmtFieldAuditConfirmation:setLocale value="<%=strLocale%>"/>
<fmtFieldAuditConfirmation:setBundle basename="properties.labels.accounts.fieldaudit.GmFieldAuditConfirmation"/>

<%

	String strAccountFaJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_FIELDAUDIT");
	String strWikiTitle = GmCommonClass.getWikiTitle("CONFIRMATION_SHEET");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Confirmation Sheet</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strAccountFaJsPath%>/GmFieldAuditConfirmation.js"></script>
<script>
var lblAuditList = '<fmtFieldAuditConfirmation:message key="LBL_AUDIT_LIST"/>';
var lblDist = '<fmtFieldAuditConfirmation:message key="LBL_DIST"/>';
var lblChooseAction = '<fmtFieldAuditConfirmation:message key="OPT_CHOOSE_ACTION"/>';

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">

	<html:form action="/gmFieldAuditConfirmation.do?">
		<html:hidden property="strOpt" value=""/>

		<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="5" height="25" class="RightDashBoardHeader"><fmtFieldAuditConfirmation:message key="LBL_CONFIRMATION_SHEET"/></td>
				<td height="25" class="RightDashBoardHeader"><fmtFieldAuditConfirmation:message key="IMG_HELP" var="varHelp"/><img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>

			<tr>
				<td colspan="6" class="LLine" height="1"></td>
			</tr>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<tr class="shade">
				<td width="95px" height="30" align="Right" class="RightTableCaption"><fmtFieldAuditConfirmation:message key="LBL_AUDIT_LIST" var="varAuditList"/><gmjsp:label
						type="MandatoryText" SFLblControlName="${varAuditList}:" td="false" />
				</td>
				<td>&nbsp;<gmjsp:dropdown controlName="auditId"
						SFFormName="frmFieldAuditConfirmation" SFSeletedValue="auditId"
						SFValue="alAuditList" codeId="ID" codeName="NAME"
						defaultValue="[Choose One]" onChange="fnReload()" tabIndex="1" width="285" />
				</td>
				<td height="30" align="Right" class="RightTableCaption"><fmtFieldAuditConfirmation:message key="LBL_FIELD_SALES" var="varFieldSales"/><gmjsp:label
						type="MandatoryText" SFLblControlName="${varFieldSales}" td="false" />
				</td>
				<td align="left" width="30%">&nbsp;<gmjsp:dropdown controlName="distId"
						SFFormName="frmFieldAuditConfirmation" SFSeletedValue="distId"
						SFValue="alDistList" codeId="DISTID" codeName="DISTNAME"
						defaultValue="[Choose One]" tabIndex="2" width="240"/>
				</td>
				<td colspan="2" class="RightTableCaption" align="center" width="12%">
					<fmtFieldAuditConfirmation:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" 
					onClick="fnLoad();" buttonType="Load" tabindex="3" style="width: 5em" /></td>
			</tr>
			<logic:equal name="frmFieldAuditConfirmation" property="strOpt"
				value="Load">
				<tr>
					<td colspan="6" class="LLine" height="1"></td>
				</tr>
				<tr>
					<td colspan="6"><jsp:include
							page="/accounts/fieldaudit/GmFieldAuditConfirmOverAllPDF.jsp">
							<jsp:param name="FORMNAME" value="frmFieldAuditConfirmation" />
						</jsp:include></td>
				</tr>
			<tr>
				<td colspan="6" class="LLine" height="1"></td>
			</tr>
			<tr>
				<td colspan="6" height="10"></td>
			</tr>
			<tr>
				<td colspan="6" class="RightText" align="Center"><BR>
				<b><fmtFieldAuditConfirmation:message key="OPT_CHOOSE_ACTION"/>:</b>&nbsp;<gmjsp:dropdown controlName="pdfAction"
						SFFormName="frmFieldAuditConfirmation" SFSeletedValue="pdfAction"
						SFValue="alPdfList" codeId="CODEID" codeName="CODENM" onChange="fnOnChange(this);"
						defaultValue="[Choose One]"  tabIndex="4"/> &nbsp;&nbsp; <fmtFieldAuditConfirmation:message key="BTN_GENERATE_PDF" var="varGeneratePDF"/><gmjsp:button
					value="&nbsp;${varGeneratePDF}&nbsp;" gmClass="button" onClick="fnSubmit();" buttonType="Save"
					tabindex="5" />&nbsp;&nbsp; <fmtFieldAuditConfirmation:message key="BTN_SEND_EMAIL" var="varSendEmail"/><gmjsp:button  name="btn_sendMail"
					value="&nbsp;${varSendEmail}&nbsp;" gmClass="button" buttonType="Save" onClick="fnSendEmail();"
					tabindex="6" />&nbsp;<BR>
				<BR></td>
			</tr>
			</logic:equal>
			<logic:notEmpty name="frmFieldAuditConfirmation" property="strMessage">
				<tr>
					<td class="Line" colspan="6" height="1"></td>
				</tr>
				<tr>
					<td height="30" align="center" colspan="6">
					 <font color="green"><b><fmtFieldAuditConfirmation:message key="LBL_MAIL_SENT_SUCCESSFULLY"/></b></font></td>
				</tr>
			</logic:notEmpty>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>