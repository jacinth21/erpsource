
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtFieldAuditFlagAuditDetailsInc" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \accounts\fieldaudit\GmFieldAuditFlagAuditDetailsInc.jsp -->
<fmtFieldAuditFlagAuditDetailsInc:setLocale value="<%=strLocale%>"/>
<fmtFieldAuditFlagAuditDetailsInc:setBundle basename="properties.labels.accounts.fieldaudit.GmFieldAuditFlagAuditDetailsInc"/>
<bean:define id="hmHeaderInfo" name="frmFieldAuditFlagVariance"	property="hmHeaderInfo" type="java.util.HashMap"></bean:define>
<%
	String strAuditQty = GmCommonClass.parseZero((String) hmHeaderInfo.get("AUDIT_QTY"));
	String strTagQty = GmCommonClass.parseZero((String) hmHeaderInfo.get("TAG_QTY"));
	String strOpsQty = GmCommonClass.parseZero((String) hmHeaderInfo.get("OPS_QTY"));
	String strSysQty = GmCommonClass.parseZero((String) hmHeaderInfo.get("SYSTEM_QTY"));
	String strAbsQty = GmCommonClass.parseZero((String) hmHeaderInfo.get("DEVIATION_QTY"));
	String strFlagQty = GmCommonClass.parseZero((String) hmHeaderInfo.get("FLAGGED_QTY"));
	String strUnresolveQty = GmCommonClass.parseZero((String) hmHeaderInfo.get("UNRESOLVED_QTY"));
	String strQtyDiff ="";
	String strBgColor ="";
	if(!strOpsQty.equals(strTagQty)){
		strBgColor = "#eee;";
		strQtyDiff = "<b>"+strOpsQty +"</b>";
	}else{
		strQtyDiff = strOpsQty;
	}
%>
<html:hidden property="auditQty" value="<%= strAuditQty%>" />
<html:hidden property="tagQty" value="<%= strTagQty%>" />
<html:hidden property="opsQty" value="<%= strOpsQty%>" />
<html:hidden property="sysQty" value="<%= strSysQty%>" />
<html:hidden property="absQty" value="<%= strAbsQty%>" />
<html:hidden property="flaggedQty" value="<%= strFlagQty%>" />
<html:hidden property="unResolveQty" value="<%= strUnresolveQty%>" />

<table border="0" height="100%" width="1300" cellspacing="0"
	cellpadding="0">
	<tr>
		<td height="25" class="RightDashBoardHeader" colspan="8">&nbsp;<fmtFieldAuditFlagAuditDetailsInc:message key="LBL_AUDIT_DETAILS"/></td>
	</tr>
	<tr></tr>
	<tr class="ShadeDarkGrayTD" height="25">
		<td align="center" bgcolor="#C0C0C0" height="27"><b><fmtFieldAuditFlagAuditDetailsInc:message key="LBL_AUDIT_QTY"/></b></td>
		<td align="center" bgcolor="#C0C0C0" height="27"><b><fmtFieldAuditFlagAuditDetailsInc:message key="LBL_TAG_QTY"/></b></td>
		<td align="center" bgcolor="#C0C0C0" height="27"><b><fmtFieldAuditFlagAuditDetailsInc:message key="LBL_OPERATION_QTY"/></b></td>
		<td align="center" bgcolor="#C0C0C0" height="27"><b><fmtFieldAuditFlagAuditDetailsInc:message key="LBL_SYSTEM_QTY"/></b></td>
		<td align="center" bgcolor="#C0C0C0" height="27"><b><fmtFieldAuditFlagAuditDetailsInc:message key="LBL_ABSOLUTE_DEVIATION_QTY"/></b></td>
		<td align="center" bgcolor="#C0C0C0" height="27"><b><fmtFieldAuditFlagAuditDetailsInc:message key="LBL_FLAGGED_QTY"/></b></td>
		<td align="center" bgcolor="#C0C0C0" height="27"><b><fmtFieldAuditFlagAuditDetailsInc:message key="LBL_UNRESOLVED_QTY"/></b></td>
	</tr>
	<tr height="20">
		<td align="center"><%=strAuditQty%></td>
		<td align="center"><%=strTagQty%></td>
		<td align="center" bgcolor="<%=strBgColor%>"><%=strQtyDiff%></td>
		<td align="center"><%=strSysQty%></td>
		<td align="center"><%=strAbsQty%></td>
		<td align="center"><%=strFlagQty%></td>
		<td align="center"><%=strUnresolveQty%></td>
	</tr>

</table>
