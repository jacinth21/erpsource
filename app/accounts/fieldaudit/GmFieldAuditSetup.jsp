<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>
<%@ taglib prefix="fmtFieldAuditSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- accounts\fieldaudit\GmFieldAuditSetup.jsp -->
<fmtFieldAuditSetup:setLocale value="<%=strLocale%>"/>
<fmtFieldAuditSetup:setBundle basename="properties.labels.accounts.fieldaudit.GmFieldAuditSetCostRpt"/>
<bean:define id="alInactiveDist" name="frmFieldAuditSetup" property="alInactiveDist" type="java.util.ArrayList"></bean:define>
<bean:define id="auditStatusId" name="frmFieldAuditSetup" property="auditStatusId" type="java.lang.String"></bean:define>
<%

	String strAccountFaJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_FIELDAUDIT");
	String strWikiTitle = GmCommonClass.getWikiTitle("FIELD_AUDIT_SETUP");
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	String strButtonDisabled = "";
	if (auditStatusId.equals("18504")) { //Closed
		strButtonDisabled = "true";
	}
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Field Sales Audit Setup</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strAccountFaJsPath%>/GmFieldAuditSetup.js"></script>
<script>

var Au_status = '<%=auditStatusId%>';
var format = '<%=strApplDateFmt%>';

this.keyArray = new Array(); 
this.valArray = new Array();
this.put = put; 
this.get = get; 
this.findIt = findIt;

</script>
<%
	HashMap hmInactive = new HashMap();
	for (int i = 0; i < alInactiveDist.size(); i++) {
		hmInactive = (HashMap) alInactiveDist.get(i);
		String strInactiveDist = (String) hmInactive.get("ID"); 
%>
<script>put('<%=strInactiveDist%>','<%=strInactiveDist%>');</script>
<%
	}
%>


</HEAD>


<BODY leftmargin="20" topmargin="10">

	<html:form action="/gmFieldAuditSetup.do?method=loadFieldAuditDetails">
		<html:hidden property="strOpt" value="" />
		<html:hidden property="hAction" value="" />
		<html:hidden property="inputString" value="" />
		<html:hidden property="hTxnId" value="" />
		<html:hidden property="hTxnName" value="" />
		<html:hidden property="hCancelType" value="VFAFSD" />
		<html:hidden property="hCancelReturnVal" value="true" />
		<input type="hidden" name="hRedirectURL" />
		<input type="hidden" name="hDisplayNm" />

		<table border="0" class="DtTable850" width="700" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="5" height="25" class="RightDashBoardHeader"><fmtFieldAuditSetup:message key="LBL_FIELD_AUDIT_SETUP"/></td>
				<td height="25" class="RightDashBoardHeader"><fmtFieldAuditSetup:message key="IMG_HELP" var="varHelp"/><img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td colspan="6" class="LLine" height="1"></td>
			</tr>
			<!-- Custom tag lib code modified for JBOSS migration changes -->

			<tr>

				<td height="30" align="Right" class="RightTableCaption" colspan="3"
					width="40%"><fmtFieldAuditSetup:message key="LBL_AUDIT_LIST" var="varAuditList"/><gmjsp:label type="RegularText"
						SFLblControlName="${varAuditList}:" td="false" />
				</td>
				<td colspan="3" align="left">&nbsp;<gmjsp:dropdown
						controlName="auditId" SFFormName="frmFieldAuditSetup"
						SFSeletedValue="auditId" SFValue="alAuditName" codeId="ID"
						codeName="NAME" defaultValue="[Choose One]" onChange="fnReload();"
						tabIndex="1" />
				</td>

			</tr>
			<tr>
				<td colspan="6" class="LLine" height="1"></td>
			</tr>
			<tr>
				<td class="RightTextBlue" colspan="6" align="center"><fmtFieldAuditSetup:message key="LBL_CHOOSE_ONE_FROM_THE_LIST_FOR_CREATING_A_NEW_AUDIT_SETUP_ENTITY"/></td>
			</tr>
			<tr>
				<td class="LLine" colspan="6"></td>
			</tr>
			<tr class="Shade">

				<td height="30" align="Right" class="RightTableCaption" colspan="3"><fmtFieldAuditSetup:message key="LBL_AUDIT_NAME" var="varAuditName"/><gmjsp:label
						type="MandatoryText" SFLblControlName="${varAuditName}:" td="false" />
				</td>
				<td colspan="3" align="left">
					<table>
						<tr>
							<td width="40%"><html:text property="auditName"
									onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
									onblur="changeBgColor(this,'#ffffff');"
									onchange="javascript:fnvalidateAuditName();" tabindex="2"
									size="45" maxlength="40" /></td>
							<td><IMG align="left" id="validateName" border=0
								height="20" width="20" src="" style="display: none" /></td>
								<td align="left" width="60%"><div id="validateText" class="RegularText"></div> </td>
						</tr>
					</table>
			</tr>
			<tr>
				<td class="LLine" colspan="6"></td>
			</tr>
			<tr>

				<td height="30" align="Right" class="RightTableCaption" colspan="3"><fmtFieldAuditSetup:message key="LBL_PRIMARY_AUDITOR" var="varPrimaryAuditor"/><gmjsp:label
						type="MandatoryText" SFLblControlName="${varPrimaryAuditor}:"
						td="false" />
				</td>
				<td colspan="3" align="left">&nbsp;<gmjsp:dropdown
						controlName="primaryAuditorId" SFFormName="frmFieldAuditSetup"
						SFSeletedValue="primaryAuditorId" SFValue="alAuditor"
						codeId="USERNM" codeName="RUSERNM" defaultValue="[Choose One]"
						onChange="fnChangeAuditor(this);" tabIndex="3" />
				</td>

			</tr>
			<tr>
				<td class="LLine" colspan="6"></td>
			</tr>
			<tr class="Shade">

				<td height="30" align="Right" class="RightTableCaption" colspan="3"><fmtFieldAuditSetup:message key="LBL_AUDIT_START_DATE" var="varAuditStartDate"/><gmjsp:label
						type="MandatoryText" SFLblControlName="${varAuditStartDate}:"
						td="false" />
				</td>
				<td colspan="3" align="left">&nbsp;<gmjsp:calendar
						SFFormName="frmFieldAuditSetup" controlName="auditStartDt"
						gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" tabIndex="4" />
				</td>

			</tr>
			<tr>
				<td class="LLine" colspan="6"></td>
			</tr>
			<tr>

				<td height="30" align="Right" class="RightTableCaption" colspan="3"><fmtFieldAuditSetup:message key="LBL_AUDIT_END_DATE" var="varAuditEndDate"/><gmjsp:label
						type="RegularText" SFLblControlName="${varAuditEndDate}:" td="false" />
				</td>
				<td colspan="3" align="left">&nbsp;<bean:write
						property="auditEndDt" name="frmFieldAuditSetup" /></td>

			</tr>
			<tr>
				<td class="LLine" colspan="6"></td>
			</tr>
			<tr class="Shade">

				<td height="30" align="Right" class="RightTableCaption" colspan="3"><fmtFieldAuditSetup:message key="LBL_AUDIT_STATUS" var="varAuditStatus"/><gmjsp:label
						type="RegularText" SFLblControlName="${varAuditStatus}:" td="false" />
				</td>
				<td colspan="3" align="left">&nbsp;<bean:write
						property="auditStatus" name="frmFieldAuditSetup" /></td>

			</tr>
			<tr>
				<td class="LLine" colspan="6"></td>
			</tr>
			<tr>
				<td><jsp:include
						page="/accounts/fieldaudit/GmFieldSalesLockInc.jsp">
						<jsp:param name="FORMNAME" value="frmFieldAuditSetup" />
					</jsp:include></td>
			</tr>

			<tr>
				<td class="LLine" colspan="6"></td>
			</tr>
			<tr height="40">
				<td colspan="6" align="center" id="AllButton">
					<fmtFieldAuditSetup:message key="BTN_SAVE" var="varSave"/><gmjsp:button value="${varSave}" gmClass="button" name="btn_Save"
					style="width: 6em" buttonType="Save" onClick="fnSave();" disabled="<%=strButtonDisabled%>"
					tabindex="9" />&nbsp;&nbsp; <fmtFieldAuditSetup:message key="BTN_LOCK" var="varLock"/><gmjsp:button value="${varLock}"
					gmClass="button" name="btn_Lock" style="width: 6em"
					onClick="fnLock();" buttonType="Save" disabled="<%=strButtonDisabled%>" tabindex="10" />&nbsp;&nbsp;
					<fmtFieldAuditSetup:message key="BTN_VOID" var="varVoid"/><gmjsp:button value="${varVoid}" gmClass="button"
					name="btn_void" style="width: 6em" buttonType="Save" onClick="fnVoid();"
					disabled="<%=strButtonDisabled%>" tabindex="11" /></td>
			</tr>

		</table>



	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>