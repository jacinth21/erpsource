 <%
/***********************************************************************************************
 * File		 		: GmFieldAuditSetCostEdit.jsp
 * Desc		 		: This screen is used for the setting the Cost to the particular Set Id's
 * Version	 		: 1.0
 * author			:
************************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.HashMap" %>
<%@ page import ="com.globus.accounts.fieldaudit.forms.GmFieldAuditSetCostForm"%>
<%@ taglib prefix="fmtGmFieldAuditSetCostEdit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- accounts\fieldaudit\GmFieldAuditSetCostEdit.jsp -->
<fmtGmFieldAuditSetCostEdit:setLocale value="<%=strLocale%>"/>
<fmtGmFieldAuditSetCostEdit:setBundle basename="properties.labels.accounts.fieldaudit.GmFieldAuditPostingErrorRpt"/>
<bean:define id="message" name="frmFieldAuditSetCost" property="message" type="java.lang.String"> </bean:define>
<%

String strAccountFaJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_FIELDAUDIT");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Cost Update</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strAccountFaJsPath%>/GmFieldAuditSetCostEdit.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
</HEAD>
<%
String strWikiTitle = GmCommonClass.getWikiTitle("SET_COST_BATCH"); 
%>
<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmFieldAuditSetCost.do?method=editBatchSetCost">
<html:hidden  property="hInputStr" />
<html:hidden  property="hSetInputStr" />
<html:hidden  property="attType" />
<html:hidden  property="strOpt" />

	<table class="DtTable900" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="3">&nbsp;<fmtGmFieldAuditSetCostEdit:message key="LBL_SET_COST_BATCH_UPDATE"/></td>
			<td class="RightDashBoardHeader"><fmtGmFieldAuditSetCostEdit:message key="IMG_HELP" var="varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
		</tr>		
		<tr>
			<td colspan="4" class="LLine"></td>
		</tr><tr></tr><tr></tr>		
	    <tr>
			<td class="RightTableCaption" align="right"><font color="red">*</font><fmtGmFieldAuditSetCostEdit:message key="LBL_ENTER_DATA_FOR_BATCH_UPLOAD"/>:</td>
			<td valign="absmiddle">&nbsp;<html:textarea property="batchData" rows="25" cols="40"></html:textarea></td>
		</tr>
		<tr></tr><tr></tr>
		<tr>
			<td colspan="4"><font color="red" size="2">*<fmtGmFieldAuditSetCostEdit:message key="LBL_SET_ID_AND_COST_COMMA_SEPARATED_LIST_AND_WITH_EACH_PAIR_ON_A_NEW_LINE"/></font></td>
		</tr><tr></tr><tr></tr>		
	
		<tr>
			<td colspan="4" class="LLine"></td>
		</tr>
		<tr>
			<td height="30" width="1200" align="center" colspan="2">
				<fmtGmFieldAuditSetCostEdit:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" style="width: 6em"  name="Btn_Submit" buttonType="Save" gmClass="button" onClick="fnSubmit();" />&nbsp;
				<fmtGmFieldAuditSetCostEdit:message key="BTN_RESET" var="varReset"/><gmjsp:button value="&nbsp;${varReset}&nbsp;" style="width: 6em" name="Btn_Reset" gmClass="button" buttonType="Save" onClick="fnReset();" />			
			</td>
		</tr>	
		<tr></tr><tr></tr>
		<tr>
			<td colspan="4" align="center">
			<logic:equal property="message" name="frmFieldAuditSetCost" value="Cost updated successfully."> <font style="color: green" size="2"><b><bean:write name="message"/></b></font> </logic:equal>
			<logic:notEqual property="message" name="frmFieldAuditSetCost" value="Cost updated successfully."> <font style="color: red" size="2"><b><bean:write name="message"/></b></font> </logic:notEqual>
			</td>
		</tr>
		<tr></tr><tr></tr>	
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>	
</BODY>
</HTML>