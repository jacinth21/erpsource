<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtFieldAuditEntryHistoryReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- accounts/fieldaudit/GmFieldAuditEntryHistoryReport.jsp -->
<fmtFieldAuditEntryHistoryReport:setLocale value="<%=strLocale%>"/>
<fmtFieldAuditEntryHistoryReport:setBundle basename="properties.labels.accounts.fieldaudit.GmFieldAuditEntryHistoryReport"/>

<%
	String strApplDateFmt = GmCommonClass.parseNull((String) session.getAttribute("strSessApplDateFmt"));
	String strRptFmt = "{0,date," + strApplDateFmt + "}";
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Field Audit History</TITLE>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script>
	
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">

	<html:form action="/gmFieldAuditEntryHistory.do">

		<table border="0" class="DtTable1200" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="12" height="25" class="RightDashBoardHeader">
					<fmtFieldAuditEntryHistoryReport:message key="LBL_FIELD_AUDIT_HISTORY_DETAILS"/></td>
			</tr>
			<tr>
				<td colspan="12" class="Line" height="1"></td>
			</tr>
			<tr height="30" colspan="12"></tr>
			<tr>
				<td colspan="12"></td>
			</tr>
			<tr>
				<td colspan="12" width="100%"><display:table
						name="requestScope.frmFieldAuditFlagVariance.alHistory"
						export="false" class="its"
						requestURI="/gmFieldAuditEntryHistory.do" id="currentRowObject">
						<fmtFieldAuditEntryHistoryReport:message key="DT_TAG" var="varTag"/><display:column property="TAGNUM" title="${varTag}" class="alignleft" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_PART" var="varPart"/><display:column property="PNUM" title="${varPart}" class="alignleft" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_CONTROL" var="varControl"/><display:column property="CNUM" title="${varControl}" class="alignleft" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_SET_ID" var="varSetId"/><display:column property="SETID" title="${varSetId}" class="alignleft" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_COUNTED_BY" var="varCountedBy"/><display:column property="COUNTEDBY" title="${varCountedBy}" class="alignleft" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_COUNTED_DATE" var="varCountedDate"/><display:column property="COUNTED_DATE" title="${varCountedDate}" class="aligncenter" format="<%=strRptFmt%>" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_TAG_STATUS" var="varTagStatus"/><display:column property="TAGSTATUS" title="${varTagStatus}" class="alignleft" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_STATUS" var="varStatus"/><display:column property="STATUS" title="${varStatus}" class="alignleft" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_COMMENTS" var="varComments"/><display:column property="COMMENTS" title="${varComments}" class="alignleft" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_FLAG_TYPE" var="varFlagType"/><display:column property="REF_TYPE" title="${varFlagType}" class="alignleft" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_REASON" var="varReason"/><display:column property="REF_ID" title="${varReason}" class="alignleft" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_FLAG_DETAILS" var="varFlagDetails"/><display:column property="REFDETAILS" title="${varFlagDetails}" class="alignleft" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_RECON_STATUS" var="varReconStatus"/><display:column property="RECON_STATUS" title="${varReconStatus}" class="alignleft" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_RETAG" var="varRetag"/><display:column property="RETAGID" title="${varRetag}" class="alignleft" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_FLAGGED_BY" var="varFlaggedBy"/><display:column property="FLAGGED_BY" title="${varFlaggedBy}"	class="alignleft" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_FLAGGED_DATE" var="varFlaggedDate"/><display:column property="FLAGGED_DT" title="${varFlaggedDate}" class="alignleft" format="<%=strRptFmt%>" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_APPROVED_BY" var="varApprovedBy"/><display:column property="APPROVED_BY" title="${varApprovedBy}" class="alignleft" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_APPPROVED_DATE" var="varAppprovedDate"/><display:column property="APPROVED_DT" title="${varAppprovedDate}" class="alignleft" format="<%=strRptFmt%>" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_RECONCILED_BY" var="varReconciledBy"/><display:column property="RECON_BY" title="${varReconciledBy}" class="alignleft" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_RECONCILED_DATE" var="varReconciledDate"/><display:column property="RECON_DT" title="${varReconciledDate}" class="alignleft" format="<%=strRptFmt%>" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_CREATED_BY" var="varCreatedBy"/><display:column property="CREATED_BY" title="${varCreatedBy}" class="alignleft" />
						<fmtFieldAuditEntryHistoryReport:message key="DT_CREATED_DATE" var="varCreatedDate"/><display:column property="CREATED_DT" title="${varCreatedDate}" class="alignleft" />
						
					</display:table></td>
			</tr>
		</table>
		<table border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="12"></td>
			</tr>
			<tr>
				<td align="center" height="30" colspan="12">
					<fmtFieldAuditEntryHistoryReport:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="&nbsp;${varClose}&nbsp;" gmClass="button"
					onClick="window.close();" buttonType="Load" />
				</td>
			</tr>
		</table>
	</html:form>

</BODY>
</HTML>