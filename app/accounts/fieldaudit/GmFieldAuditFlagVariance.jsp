<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtFieldAuditFlagVariance" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- accounts\fieldaudit\GmFieldAuditFlagVariance.jsp -->
<fmtFieldAuditFlagVariance:setLocale value="<%=strLocale%>"/>
<fmtFieldAuditFlagVariance:setBundle basename="properties.labels.accounts.fieldaudit.GmFieldAuditFlagVariance"/>
<bean:define id="setId" name="frmFieldAuditFlagVariance" property="setId" type="java.lang.String"></bean:define>
<bean:define id="auditId" name="frmFieldAuditFlagVariance" property="auditId" type="java.lang.String"></bean:define>
<bean:define id="deviationQty" name="frmFieldAuditFlagVariance"	property="deviationQty" type="java.lang.String"></bean:define>
<bean:define id="hmHeaderInfo" name="frmFieldAuditFlagVariance"	property="hmHeaderInfo" type="java.util.HashMap"></bean:define>
<%

	String strAccountFaJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_FIELDAUDIT");
	String strWikiTitle = "";

	strWikiTitle = GmCommonClass.parseNull((String) GmCommonClass.getWikiTitle("FLAG_DEVIATION"));
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	String strTodaysDate = GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate"));

	String strSetDesc = GmCommonClass.parseNull((String) hmHeaderInfo.get("SET_DESC"));
	String strSystemName = GmCommonClass.parseNull((String) hmHeaderInfo.get("SYSTEM_NAME"));
	String strDistName = GmCommonClass.parseNull((String) hmHeaderInfo.get("DIST_NAME"));
	String strPartNum = GmCommonClass.parseNull((String) hmHeaderInfo.get("PART_NUM"));
	String strPartDesc = GmCommonClass.parseNull((String) hmHeaderInfo.get("PDESC"));
	String strSetCost = GmCommonClass.parseNull((String) hmHeaderInfo.get("SET_COST"));
	String strPartPrice = GmCommonClass.parseNull((String) hmHeaderInfo.get("PART_PRICE"));
	String strCurDate = GmCommonClass.parseNull((String) hmHeaderInfo.get("CURDATE"));
	String strSetId = setId;
	if (strSetId.equals("null")) {
		strSetId = "";
	}
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Field Audit - Flag Deviation Report</TITLE>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strAccountFaJsPath%>/GmFieldAuditFlagVariance.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"></script>
<script>
var dhxWins,w1;
var today_dt = '<%=strTodaysDate%>';
var format = '<%=strApplDateFmt%>';
</script>
</HEAD>

<BODY onload="fnOnload();" leftmargin="10" topmargin="10">
	<html:form action="/gmFieldAuditFlagVar.do">
		<html:hidden property="strOpt" value="" />
		<html:hidden property="auditId" />
		<html:hidden property="distId" />
		<html:hidden property="distName" />
		<html:hidden property="setId" value="<%=strSetId%>" />
		<html:hidden property="pnum" />
		<html:hidden property="strInput" />
		<html:hidden property="setType" />

		<table border="0" class="DtTable1300" width="1300" cellspacing="0"
			cellpadding="0">
			<tr>
				<td colspan="5" height="25" class="RightDashBoardHeader"><fmtFieldAuditFlagVariance:message key="LBL_FLAG_DEVIATION"/></td>
				<td height="25" class="RightDashBoardHeader"><fmtFieldAuditFlagVariance:message key="IMG_HELP" var="varHelp"/><img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td colspan="6" class="LLine" height="1"></td>
			</tr>
			<tr class="shade">
				<td height="30" align="Right" class="RightTableCaption"><fmtFieldAuditFlagVariance:message key="LBL_FIELD_SALES"/>:&nbsp;</td>
				<td><%=strDistName%></td>
				<td height="30" align="Right" class="RightTableCaption"><fmtFieldAuditFlagVariance:message key="LBL_SET_ID"/>:&nbsp;</td>
				<td align="left"><%=strSetId%></td>
				<td height="30" align="Right" class="RightTableCaption"><fmtFieldAuditFlagVariance:message key="LBL_PART"/>:&nbsp;</td>
				<td align="left"><%=strPartNum%></td>

			</tr>
			<tr>
				<td colspan="6" class="LLine" height="1"></td>
			</tr>
			<tr>
				<td height="30" align="Right" class="RightTableCaption"><fmtFieldAuditFlagVariance:message key="LBL_SYSTEM"/>:&nbsp;</td>
				<td><%=strSystemName%></td>
				<td height="30" align="Right" class="RightTableCaption"><fmtFieldAuditFlagVariance:message key="LBL_SET_DESC"/>:&nbsp;</td>
				<td align="left"><%=strSetDesc%></td>
				<td height="30" align="Right" class="RightTableCaption"><fmtFieldAuditFlagVariance:message key="LBL_PART_DESC"/>:&nbsp;</td>
				<td align="left"><%=strPartDesc%></td>
			</tr>
			<tr>
				<td colspan="6" class="LLine" height="1"></td>
			</tr>

			<tr>
				<td align="center" colspan="6"><jsp:include
						page="/accounts/fieldaudit/GmFieldAuditFlagAuditDetailsInc.jsp">
						<jsp:param name="FORMNAME" value="frmFieldAuditFlagVariance" />
					</jsp:include></td>
			</tr>
			<tr>
				<td align="center" colspan="6"><jsp:include
						page="/accounts/fieldaudit/GmFieldAuditFlagMatchingTagsInc.jsp">
						<jsp:param name="FORMNAME" value="frmFieldAuditFlagVariance" />
					</jsp:include></td>
			</tr>
			<tr>

				<td align="center" colspan="6"><jsp:include
						page="/accounts/fieldaudit/GmFieldAuditFlagDeviationTagsInc.jsp">
						<jsp:param name="FORMNAME" value="frmFieldAuditFlagVariance" />
					</jsp:include></td>
			</tr>
			<tr>
				<td colspan="6" class="LLine" height="1"></td>
			</tr>
			<tr>
				<td align="center" colspan="6" height="40">
					<p>&nbsp;</p> <fmtFieldAuditFlagVariance:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="button"
					name="Btn_Submit" style="width: 5em" buttonType="Save" onClick="fnFlagDeviation(); " /> &nbsp;
					<fmtFieldAuditFlagVariance:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="${varClose}" buttonType="Load" style="width: 5em" gmClass="button" name="Close"
					onClick="fnClose();" /></td>
			</tr>
			<tr>
				<td colspan="6" class="LLine" height="1"></td>
			</tr>
			<tr>

				<td align="center" colspan="6"><jsp:include
						page="/accounts/fieldaudit/GmFieldAuditFlagAssocRecInc.jsp">
						<jsp:param name="FORMNAME" value="frmFieldAuditFlagVariance" />
					</jsp:include></td>
			</tr>
			<tr>
				<td><html:hidden property="setCost" value="<%=strSetCost%>" />
					<html:hidden property="partPrice" value="<%=strPartPrice%>" /> 
					<html:hidden property="curDate" value="<%=strCurDate%>" /></td>
			</tr>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>