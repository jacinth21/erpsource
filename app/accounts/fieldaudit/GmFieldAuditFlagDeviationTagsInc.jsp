
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtFieldAuditFlagDeviationTagsInc" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- accounts\fieldaudit\GmFieldAuditFlagDeviationTagsInc.jsp -->
<fmtFieldAuditFlagDeviationTagsInc:setLocale value="<%=strLocale%>"/>
<fmtFieldAuditFlagDeviationTagsInc:setBundle basename="properties.labels.accounts.fieldaudit.GmFieldAuditFlagDeviationTagsInc"/>

<% 
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	String strRptFmt = "{0,date,"+strApplDateFmt+"}";
%>
<table border="0" height="100%" width="1300" cellspacing="0"
	cellpadding="0">
	<tr>
		<td height="25" class="RightDashBoardHeader"><a
			href="javascript:fnShowFilters('tabPosit');" tabindex="-1"><IMG
				id="tabPositimg" border=0 src="<%=strImagePath%>/minus.gif">
		</a>&nbsp;<fmtFieldAuditFlagDeviationTagsInc:message key="LBL_POSITIVE_DEVIATION_TAGS"/></td>
	</tr>

	<TR>
		<TD colspan="2">
			<table border="0" height="100%" width="1300" cellspacing="0"
				cellpadding="0" id="tabPosit" style="display: block">
				<tr>
					<td><display:table
							name="requestScope.frmFieldAuditFlagVariance.alPositiveReport"
							requestURI="/gmFieldAuditFlagVar.do?"
							decorator="com.globus.accounts.fieldaudit.displaytag.beans.DTFieldAuditFlagVarianceWrapper"
							class="its">
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_TAG" var="varTag"/><display:column property="TAG_ID" title="${varTag}"	style="width:110;" />
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_CONTROL" var="varControl"/><display:column property="CNUM" title="${varControl}" style="width:70;" />
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_FLAG" var="varFlag"/><display:column property="FLAG" title="${varFlag}" style="width:130;" />
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_REASON" var="varReason"/><display:column property="REASONS" title="${varReason}" style="width:200;" />
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_RETAG" var="varReTag"/><display:column property="RETAG" title="${varReTag}" style="width:60;" />
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_FLAG_DATE" var="varFlagDate"/><display:column property="FL_DATE" title="${varFlagDate}" style="width:110;" />
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_AUDIT_LOCATION" var="varAuditLocation"/><display:column property="AUDIT_LOC" title="${varAuditLocation}"	style="width:100;" />
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_ASSIGNED_TO" var="varAssignedTo"/><display:column property="ASSIGNED" title="${varAssignedTo}" style="width:100;" />
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_APP" var="varApp"/><display:column property="APPROVED" title="${varApp}" style="width:20;" />
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_REC_DATE" var="varRecDate"/><display:column property="RECON_DT" title="${varRecDate}" style="width:70;" format="<%=strRptFmt %>"/>
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_TAG_VOID" var="varTagVoid"/><display:column property="TAG_VOID" title="${varTagVoid}" style="width:10;" />
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_POSTING_OPTIONS" var="varPostingOptions"/><display:column property="POST" title="${varPostingOptions}"	style="width:250;" />
						</display:table></td>
				</tr>
			</table>
		</TD>
	</TR>
</table>



<table border="0" height="100%" width="1300" cellspacing="0" cellpadding="0">
	<tr>
		<td height="25" class="RightDashBoardHeader"><a
			href="javascript:fnShowFilters('tabNega');" tabindex="-1"><IMG
				id="tabNegaimg" border=0 src="<%=strImagePath%>/minus.gif">
		</a>&nbsp;<fmtFieldAuditFlagDeviationTagsInc:message key="LBL_NEGATIVE_DEVIATION_TAGS"/></td>
	</tr>

	<TR>
		<TD colspan="2">
			<table border="0" height="100%" width="1300" cellspacing="0"
				cellpadding="0" id="tabNega" style="display: block">
				<tr>
					<td><display:table
							name="requestScope.frmFieldAuditFlagVariance.alNegativeReport"
							requestURI="/gmFieldAuditFlagVar.do?"
							decorator="com.globus.accounts.fieldaudit.displaytag.beans.DTFieldAuditFlagVarianceWrapper"
							class="its">
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_TAG" var="varTag"/><display:column property="TAG_ID" title="${varTag}" style="width:110;" />
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_CONTROL" var="varControl"/><display:column property="CNUM" title="${varControl}" style="width:70;" />
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_FLAG" var="varFlag"/><display:column property="FLAG" title="${varFlag}" style="width:130;" />
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_REASON" var="varReason"/><display:column property="REASONS" title="${varReason}" style="width:200;" />
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_RETAG" var="varReTag"/><display:column property="RETAG" title="${varReTag}" style="width:60;" />
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_FLAG_DATE" var="varFlagDate"/><display:column property="FL_DATE" title="${varFlagDate}" style="width:110;"/>
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_AUDIT_LOCATION" var="varAuditLocation"/><display:column property="AUDIT_LOC" title="${varAuditLocation}"	style="width:100;" />
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_ASSIGNED_TO" var="varAssignedTo"/><display:column property="ASSIGNED" title="${varAssignedTo}" style="width:100;" />
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_APP" var="varApp"/><display:column property="APPROVED" title="${varApp}" style="width:10;" />
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_REC_DATE" var="varRecDate"/><display:column property="RECON_DT" title="${varRecDate}" style="width:70;" format="<%=strRptFmt %>"/>
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_TAG_VOID" var="varTagVoid"/><display:column property="TAG_VOID" title="${varTagVoid}" style="width:10;" />
							<fmtFieldAuditFlagDeviationTagsInc:message key="DT_POSTING_OPTIONS" var="varPostingOptions"/><display:column property="POST" title="${varPostingOptions}"	style="width:250;" />
						</display:table></td>
				</tr>
			</table></TD>
	</TR>
</table>