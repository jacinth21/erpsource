<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>
<%@ taglib prefix="fmtFieldSalesLockInc" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- \accounts\fieldaudit\GmFieldSalesLockInc.jsp -->
<fmtFieldSalesLockInc:setLocale value="<%=strLocale%>"/>
<fmtFieldSalesLockInc:setBundle basename="properties.labels.accounts.fieldaudit.GmFieldSalesLockInc"/>
<bean:define id="gridData" name="frmFieldAuditSetup" property="gridData" type="java.lang.String"></bean:define>
<%
	String strAccountFaJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_FIELDAUDIT");
%>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strAccountFaJsPath%>/GmFieldAuditSetup.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>


<script>
var objGridData;
var rowID = '';
objGridData = '<%=gridData%>';
	var gridObj = '';
	var distFl = false;
	var auditorFl = false;
	var fs_rowId = '';
	var tag_rowIdId = '';
	var auditor_rowId = '';
	var status_rowId = '';
	var check_rowId = '';
</script>


<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">

	<html:form action="/gmFieldAuditSetup.do?method=loadFieldAuditDetails">

		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="6"><fmtFieldSalesLockInc:message key="LBL_FIELD_SALES_LOCK_SETUP"/></td>
		</tr>
		<tr>
			<td class="LLine" height="1"></td>
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>

			<td height="30" align="left" class="RightTableCaption" colspan="3">&nbsp;&nbsp;<fmtFieldSalesLockInc:message key="LBL_REGION" var="varRegion"/><gmjsp:label
					type="RegularText" SFLblControlName="${varRegion}:" td="false" /><gmjsp:dropdown
					controlName="regionId" SFFormName="frmFieldAuditSetup"
					SFSeletedValue="regionId" SFValue="alRegionName" codeId="ID"
					codeName="NM" defaultValue="[Choose One]" tabIndex="5" />
			</td>
			<td colspan="3" align="left">&nbsp;&nbsp; <fmtFieldSalesLockInc:message key="BTN_LOAD" var="varLoad"/><gmjsp:button
				value="${varLoad}" gmClass="button" buttonType="Load" name="btn_Load" style="width: 6em"
				onClick="fnLoad();" tabindex="6" />
			</td>
		</tr>
		<tr>
			<td class="LLine" colspan="6"></td>
		</tr>

		<tr>
			<td class="RightTableCaption" align="left" colspan="6" height="20">
				<table cellpadding="1" cellspacing="1" id="addTbl">
					<TR>
						<td class="RightTableCaption">&nbsp;<a href="javascript:addRow();"><fmtFieldSalesLockInc:message key="IMG_ADD_ROW" var="varAddRow"/> <img
								src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="${varAddRow}"
								style="border: none;" height="14">
						</a> &nbsp;<a href="javascript:removeSelectedRow();"> <fmtFieldSalesLockInc:message key="IMG_REMOVE_ROW" var="varRemoveRow"/><img src="<%=strImagePath%>/dhtmlxGrid/delete.gif"
								alt="${varRemoveRow}" style="border: none;"
								height="14">
						</a></td>
					</TR>
				</table></td>
		</tr>

		<tr>
			<td colspan="6">
				<div id="fieldSalesLockRpt" style="" height="300px" width="870px"></div>
			</td>
		</tr>
		<tr>
			<td class="LLine" colspan="6"></td>
		</tr>
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="6">
				<fmtFieldSalesLockInc:message key="LBL_PART_SET_LEVEL_COUNT"/></td>
		</tr>
		<logic:notEqual property="invalidSetIds" name="frmFieldAuditSetup"
			value="">
			<tr>
				<td class="LLine" colspan="6"></td>
			</tr>
			<tr height="30">
				<td colspan="6" align="left"><font color="red"><fmtFieldSalesLockInc:message key="LBL_SET_IDS_ARE_INVALID"/> : &nbsp;<bean:write
							property="invalidSetIds" name="frmFieldAuditSetup" />
				</font></td>
			</tr>
		</logic:notEqual>
		<tr>
			<td class="LLine" colspan="6"></td>
		</tr>

		<tr>
			<td height="30" align="Right" class="RightTableCaption" colspan="3"><fmtFieldSalesLockInc:message key="LBL_SET_IDS" var="varSetIds"/><gmjsp:label
					type="RegularText" SFLblControlName="${varSetIds}:" td="false" />
			</td>
			<td colspan="3" align="left" style="padding-right: 14px;padding-left: 3px;">&nbsp;<html:textarea
					property="setIds" cols="60" rows="6"
					onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
					onblur="changeBgColor(this,'#ffffff');" tabindex="7" />
			</td>
		</tr>
		<tr>
			<td class="LLine" colspan="6"></td>
		</tr>
		<tr class="Shade">
			<td height="30" align="Right" class="RightTableCaption" colspan="3"><fmtFieldSalesLockInc:message key="LBL_PART" var="varPart"/><gmjsp:label
					type="RegularText" SFLblControlName="${varPart}:" td="false" />
			</td>
			<td colspan="3" align="left">&nbsp;<html:textarea
					property="pnums" cols="60" rows="6"
					onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
					readonly="true" onblur="changeBgColor(this,'#ffffff');"
					tabindex="8" />
			</td>
		</tr>
		<tr>
			<td class="LLine" colspan="6"></td>
		</tr>
		<tr>
			<td class="RegularText" colspan="6" align="left">
			<fmtFieldSalesLockInc:message key="LBL_SET_ID_FOR_SET_LEVEL_COUNT"/></td>
		</tr>
		<tr>
			<td class="LLine" colspan="6"></td>
		</tr>
		<tr class="Shade">
			<td class="RegularText" colspan="6" align="left"><fmtFieldSalesLockInc:message key="LBL_PART_NUMBER_FOR_PART_LEVEL_COUNT"/></td>
		</tr>

	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>