
 <!-- \accounts\GmFieldAuditConfirmOverAllPDF.jsp-->

<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<bean:define id="hmResult" name="frmFieldAuditConfirmation" property="hmResult" type="java.util.HashMap"></bean:define>
<bean:define id="distId" name="frmFieldAuditConfirmation" property="distId" type="java.lang.String"></bean:define>
<bean:define id="auditId" name="frmFieldAuditConfirmation" property="auditId" type="java.lang.String"></bean:define>
<%
	String strHtmlJasperRpt = "";
%>
<table>
<tr>
		<td>
		<%
		if(!distId.equals("")&&!distId.equals("0"))
		{
			String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
			GmJasperReport gmJasperReport = new GmJasperReport();
		
			gmJasperReport.setRequest(request);
			gmJasperReport.setResponse(response);
			gmJasperReport.setJasperReportName("/GmFieldAuditConfirmationOverAllReports.jasper");
			gmJasperReport.setHmReportParameters(hmResult);
			gmJasperReport.setReportDataList(null);
			gmJasperReport.setBlDisplayImage(true);
			strHtmlJasperRpt = gmJasperReport.getHtmlReport();
		}
		%>
		<%=strHtmlJasperRpt %>
		</td>
		</tr>
</table>