<%
/*******************************************************************************
 * File		 	: GmFieldAuditVerifyUploadData.jsp
 * Desc		 	: This screen is used for displayimg the Field Audit Uploaded Data Report.
 * Version	 	: 1.0
 * author		: Jignesh Shah
*******************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<%@ taglib prefix="fmtFieldAuditVerifyUploadData" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- \accounts\fieldaudit\GmFieldAuditVerifyUploadData.jsp -->
<fmtFieldAuditVerifyUploadData:setLocale value="<%=strLocale%>"/>
<fmtFieldAuditVerifyUploadData:setBundle basename="properties.labels.accounts.fieldaudit.GmFieldAuditVerifyUploadData"/>
<bean:define id="ldtResult" name="frmFieldAuditEditUploadData" property="ldtResult" type="java.util.List"></bean:define>
<bean:define id="alDistributor" name="frmFieldAuditEditUploadData" property="alDistributor" type="java.util.ArrayList"></bean:define>
<bean:define id="reportType" name="frmFieldAuditEditUploadData" property="reportType" type="java.lang.String"> </bean:define>
<%
	String strAccountFaJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_FIELDAUDIT");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strWikiTitle = GmCommonClass.getWikiTitle("VERIFY_UPLOAD");
	int rowsize = ldtResult.size();
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Verify Upload Data</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     
    table.DtTable1200 {
	width: 1400px;
	}
</style>
<%-- <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css"> --%>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strAccountFaJsPath%>/GmFieldAuditVerifyUploadData.js"></script>

</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmFieldAuditEditUploadData.do?method=verifyUploadData">

<html:hidden property="reportType"/>
<html:hidden property="strOpt" value="" />
<html:hidden property="hAction" value="" />
<input type="hidden" name="hauditEntryID" value="" />
<!-- 
<input type="hidden" name="hauditId" value="" />
<input type="hidden" name="hdistId" value="" />
<input type="hidden" name="hauditorId" value="" />
 -->
	<table border="0" style="overflow: auto;" class="DtTable1200" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="7" height="25" class="RightDashBoardHeader"><fmtFieldAuditVerifyUploadData:message key="LBL_UPLOADED_DATA"/></td>
			<td  height="25" class="RightDashBoardHeader">
			<fmtFieldAuditVerifyUploadData:message key="IMG_HELP" var="varHelp"/>
				<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr><td colspan="8" class="LLine" height="1"></td></tr>
		<% if(!reportType.equals("SummaryRpt")){ %>
		<tr class="shade" >		
			<td class="RightTableCaption" height="30" width="150" align ="Right"><font color="red">*</font><fmtFieldAuditVerifyUploadData:message key="LBL_AUDIT_LIST"/>:&nbsp;</td>
		    <td align ="left" width="300"><gmjsp:dropdown controlName="auditId" SFFormName="frmFieldAuditEditUploadData" SFSeletedValue="auditId" 
								SFValue="alAuditName" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" onChange="fnReload()" />
			</td>
			<td class="RightTableCaption" height="30" width="100" align ="Right"><fmtFieldAuditVerifyUploadData:message key="LBL_FIELD_SALES"/>:&nbsp;</td>
			<td  align ="left" width="300"><gmjsp:dropdown controlName="distId" SFFormName="frmFieldAuditEditUploadData" SFSeletedValue="distId" 
								SFValue="alDistributor" codeId = "DISTID"  codeName = "DISTNAME"  defaultValue= "[Choose One]" />
			</td>
			<td class="RightTableCaption" height="30" width="100" align ="Right">&nbsp; <fmtFieldAuditVerifyUploadData:message key="LBL_COUNTED_BY"/>:&nbsp;</td>
			<td align ="left" width="300"><gmjsp:dropdown controlName="auditorId" SFFormName="frmFieldAuditEditUploadData" 
		    					SFSeletedValue="auditorId" SFValue="alAuditor" codeId = "USERNM"  codeName = "RUSERNM"  defaultValue= "[Choose One]"  />
		  	</td>
	  		<td colspan="3" width="150" align ="left"><fmtFieldAuditVerifyUploadData:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" gmClass="button" style="width: 6em" buttonType="Load" onClick="fnLoad();" /></td>
		</tr>
		<tr><td colspan="8" class="LLine" height="1"></td></tr>
		<%} %>
		<tr>
			<td colspan="8"  height="10" align="center">
				<display:table name="requestScope.frmFieldAuditEditUploadData.ldtResult" requestURI="/gmFieldAuditEditUploadData.do?method=verifyUploadData" defaultsort="1"  export= "true" id="currentRowObject" varTotals="totals"  decorator="com.globus.accounts.fieldaudit.displaytag.beans.DTFieldAuditEditUploadWrapper" freezeHeader="true" cellpadding="0" cellspacing="0" >
			 		<fmtFieldAuditVerifyUploadData:message key="DT_TAG_ID" var="varTagID"/><display:column property="TAGID" title="${varTagID}" headerClass="ShadeDarkGrayTDCenter" class="ShadeMedGrayTD" style="text-align: left; width:100;" sortable="true" comparator="com.globus.common.displaytag.beans.DTComparator"/>
			 		<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
			 		<fmtFieldAuditVerifyUploadData:message key="LBL_FIELD_SALES" var="varFieldSales"/><display:column property="DIST_NAME" title="${varFieldSales}" headerClass="ShadeDarkGrayTDCenter" class="ShadeMedGrayTD" style="text-align: left; width:90;"  sortable="true" maxLength="8" titleKey="DISTID" />
			 		<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
			 		<fmtFieldAuditVerifyUploadData:message key="DT_SET_ID" var="varSetID"/><display:column property="SETID" title="${varSetID}" headerClass="ShadeDarkGrayTDCenter" class="ShadeMedGrayTD" style="text-align: left; width:70;" sortable="true"  />
					<fmtFieldAuditVerifyUploadData:message key="DT_SET_DESC" var="varSetDesc"/><display:column property="SETDESC" title="${varSetDesc}"  headerClass="ShadeDarkGrayTD"  class="ShadeMedGrayTD" style="text-align: left; width:90;"  sortable="true" maxLength="10" titleKey="SETDESC"/>
					<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
					<fmtFieldAuditVerifyUploadData:message key="DT_PART" var="varPart"/><display:column property="PNUM" title="${varPart}" headerClass="ShadeDarkGrayTDCenter" class="ShadeMedGrayTD" style="text-align: left; width:60;" sortable="true" /> 
					<fmtFieldAuditVerifyUploadData:message key="DT_PART_DESC" var="varPartDesc"/><display:column property="PDESC" escapeXml="true" title="${varPartDesc}" headerClass="ShadeDarkGrayTD" class="ShadeMedGrayTD" style="text-align: left; width:90;"  sortable="true" maxLength="10" titleKey="PDESC"/>									
					<fmtFieldAuditVerifyUploadData:message key="DT_CONTROL" var="varControl"/><display:column property="CNUM" title="${varControl}" headerClass="ShadeDarkGrayTDCenter"  class="ShadeMedGrayTD" style="text-align: left; width:60;" sortable="true"   titleKey="CNUM"  />					
					<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
					<fmtFieldAuditVerifyUploadData:message key="DT_COMMENT" var="varComment"/><display:column property="COMMENTS" title="${varComment}" headerClass="ShadeDarkGrayTDCenter" class="ShadeMedBlueTD" style="text-align: left; width:80;"  sortable="true"  maxLength="8" titleKey="COMMENTS" />
					<fmtFieldAuditVerifyUploadData:message key="DT_STATUS" var="varStatus"/><display:column property="STATUS" title="${varStatus}" headerClass="ShadeDarkGrayTDCenter" class="ShadeMedBlueTD" style="text-align: left; width:80;" sortable="true"  maxLength="12" titleKey="STATUS" />
					<fmtFieldAuditVerifyUploadData:message key="DT_RETAG" var="varRetag"/><display:column property="RETAG" title="${varRetag}" headerClass="ShadeDarkGrayTDCenter" class="ShadeMedBlueTD" style="text-align: left; width:70;" sortable="true"  maxLength="8" titleKey="RETAG" />
					<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
					<fmtFieldAuditVerifyUploadData:message key="DT_FLAG" var="varFlag"/><display:column property="FLAGTYPE" title="${varFlag}" headerClass="ShadeDarkGrayTDCenter" class="ShadeLightBlueTD" style="text-align: left; width:80;" sortable="true"   maxLength="7" titleKey="FLAG" />
					<fmtFieldAuditVerifyUploadData:message key="DT_FLAG_DETAILS" var="varFlagDetails"/><display:column property="FLAGDET" title="${varFlagDetails}" headerClass="ShadeDarkGrayTDCenter" class="ShadeLightBlueTD" style="text-align: left; width:60;" sortable="true"  maxLength="6" titleKey="FDETAILS" />
					<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
					<fmtFieldAuditVerifyUploadData:message key="DT_LOCATION_TYPE" var="varLocationType"/><display:column property="LOCATIONTYPE" title="${varLocationType}" headerClass="ShadeDarkGrayTDCenter" class="ShadeMedBlueTD" style="text-align: left; width:80;"  sortable="true"  maxLength="10" titleKey="LOCATIONTYPE" />
					<fmtFieldAuditVerifyUploadData:message key="DT_LOCATION" var="varLocation"/><display:column property="LOCATION" title="${varLocation}"   headerClass="ShadeDarkGrayTDCenter" class="ShadeMedBlueTD" style="text-align: left; width:100;" sortable="true" maxLength="10" titleKey="LOCATION"/>
					<fmtFieldAuditVerifyUploadData:message key="DT_LOCATION_COMMENTS" var="varLocationComments"/><display:column property="LOCATION_COMMENTS" title="${varLocationComments}"   headerClass="ShadeDarkGrayTDCenter" class="ShadeMedBlueTD" style="text-align: left; width:90;" sortable="true" titleKey="LOCATION"/>
					<fmtFieldAuditVerifyUploadData:message key="DT_ADDRESS_TYPE" var="varAddressType"/><display:column property="ADDRESS_TYPE" title="${varAddressType}"   headerClass="ShadeDarkGrayTDCenter" class="ShadeMedBlueTD" style="text-align: left; width:80;" sortable="true" titleKey="ADDTYPE"/>
					<fmtFieldAuditVerifyUploadData:message key="DT_LOCATION_DETAILS" var="varLocationDetails"/><display:column property="LOCATION_DETAIL" title="${varLocationDetails}" headerClass="ShadeDarkGrayTDCenter" class="ShadeMedBlueTD" style="text-align: left; width:90;" sortable="true" maxLength="10" titleKey="LOCATION_DETAIL" />
					<display:column property="PLINE" title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
					<fmtFieldAuditVerifyUploadData:message key="DT_SET_COUNT" var="varSetCount"/><display:column property="SET_COUNT_FL" title="${varSetCount}"  headerClass="ShadeDarkGrayTD" class="ShadeLightBlueTD" style="text-align: left; width:50;" sortable="true" maxLength="5" titleKey="SETCOUNT" />
					<fmtFieldAuditVerifyUploadData:message key="DT_PART_COUNT" var="varPartCount"/><display:column property="PART_COUNT_FL" title="${varPartCount}"  headerClass="ShadeDarkGrayTD" class="ShadeLightBlueTD" style="text-align: left; width:50;" sortable="true" maxLength="5" titleKey="PARTCOUNT" />
					<fmtFieldAuditVerifyUploadData:message key="LBL_COUNTED_BY" var="varCountedBy"/><display:column property="COUNTEDBY" title="${varCountedBy}"  headerClass="ShadeDarkGrayTD" class="ShadeLightBlueTD" style="text-align: left; width:70;" sortable="true" maxLength="6" titleKey="COUNTEDBY"  />
					<fmtFieldAuditVerifyUploadData:message key="DT_COUNTED_DATE" var="varCountedDate"/><display:column property="COUNTEDDT" title="${varCountedDate}"  headerClass="ShadeDarkGrayTDCenter" class="ShadeLightBlueTD" style="text-align: left; width:60;" sortable="true" format="{0,date,MM/dd/yyyy}" />	
					
					
				
				<display:footer media="html">
				<tr><td class="Line" colspan="28"></td></tr>
					<tr>					
						<td class = "ShadeDarkGrayTD" colspan="3"> <B> <fmtFieldAuditVerifyUploadData:message key="LBL_TOTAL_RECORDS"/>: <%=rowsize %></B></td>					
						<td class = "ShadeDarkGrayTD" colspan="28" > </td>
					</tr>
					
				<tr><td class="Line" colspan="28"></td></tr>	
				</display:footer>
				</display:table>
			</td>
		</tr>
		<% if((!reportType.equals("SummaryRpt")) && (rowsize >= 1) ){%>  
		<tr>
			<td colspan="8" class="RightText" align="Center">
				<BR><b><fmtFieldAuditVerifyUploadData:message key="OPT_CHOOSE_ACTION"/>:</b>&nbsp;<select name="Cbo_Action" class="RightText" tabindex="2" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');">
					<option value="0" ><fmtFieldAuditVerifyUploadData:message key="OPT_CHOOSE_ONE"/>
					<option value="Edit"><fmtFieldAuditVerifyUploadData:message key="OPT_EDIT"/></option>
				</select>
				<fmtFieldAuditVerifyUploadData:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" buttonType="Load" onClick="fnSubmit();" tabindex="25"/>&nbsp;<BR><BR>
			</td>
		</tr>	
		<% } %>			
		<% if(reportType.equals("SummaryRpt")){ %>
		<tr>
			<td colspan="8" class="RightText" align="Center">
				<fmtFieldAuditVerifyUploadData:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="&nbsp;${varClose}&nbsp;" gmClass="button" buttonType="laod" onClick="fnClose();" tabindex="25"/>&nbsp;<BR><BR>
			</td>	
		</tr>
		<%} %>	
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>