<%
/*******************************************************************************
 * File		 	: GmFieldAuditPostingErrorRpt.jsp
 * Desc		 	: This screen is used for displayimg the Field Audit Posting Report.
 * Version	 	: 1.0
 * author		: Jignesh Shah
*******************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<%@ taglib prefix="fmtFieldAuditPostingErrorRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- accounts\fieldaudit\GmFieldAuditPostingErrorRpt.jsp -->
<fmtFieldAuditPostingErrorRpt:setLocale value="<%=strLocale%>"/>
<fmtFieldAuditPostingErrorRpt:setBundle basename="properties.labels.accounts.fieldaudit.GmFieldAuditPostingErrorRpt"/>


<bean:define id="ldtResult" name="frmFieldAuditReport" property="ldtResult" type="java.util.List"></bean:define>
<%

	String strAccountFaJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_FIELDAUDIT");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strWikiTitle = GmCommonClass.getWikiTitle("POSTING_ERROR_REPORT");
	int rowsize = ldtResult.size();
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Posting Error Report</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     
    table.DtTable1100 {
	width: 1100px;
	}
</style>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strAccountFaJsPath%>/GmFieldAuditPostingErrorRpt.js"></script>
<script>
var lblAuditList = '<fmtFieldAuditPostingErrorRpt:message key="LBL_AUDIT_LIST"/>';
var lblStatus = '<fmtFieldAuditPostingErrorRpt:message key="LBL_STATUS"/>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" >
 
<html:form  action="/gmFieldAuditPostingError.do?method=fetchPostingErrorRpt">
<html:hidden property="strOpt"/>

	<table border="0" bordercolor="red" class="DtTable1100" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="6" height="25" class="RightDashBoardHeader"><fmtFieldAuditPostingErrorRpt:message key="LBL_POSTING_ERROR_REPORT"/></td>
			<td  height="25" class="RightDashBoardHeader">
			<fmtFieldAuditPostingErrorRpt:message key="IMG_HELP" var="varHelp"/>
				<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr><td colspan="7" class="LLine" height="1"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr  class="shade" >		
			<td class="RightTableCaption" height="30" align ="Right" width="80"><font color="red">*</font><fmtFieldAuditPostingErrorRpt:message key="LBL_AUDIT_LIST"/>:</td>
		    <td align ="left" width="50">
		    	<gmjsp:dropdown controlName="auditId" SFFormName="frmFieldAuditReport" SFSeletedValue="auditId" 
					SFValue="alAuditName" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" onChange="fnReload();"/>
			</td>
			<td class="RightTableCaption" align ="Right" width="110"><fmtFieldAuditPostingErrorRpt:message key="LBL_FIELD_SALES"/>:</td>
			<td align ="left" width="70">
				<gmjsp:dropdown controlName="distId" SFFormName="frmFieldAuditReport" SFSeletedValue="distId" width="250"
					SFValue="aldist" codeId = "DISTID"  codeName = "DISTNAME"  defaultValue= "[Choose One]"  />&nbsp;&nbsp;
			</td>
			<td class="RightTableCaption" align ="Right" width="80"><font color="red">*</font><fmtFieldAuditPostingErrorRpt:message key="LBL_STATUS"/>:</td>
			<td align ="left" width="80">
				<gmjsp:dropdown controlName="statusId" SFFormName="frmFieldAuditReport" SFSeletedValue="statusId" 
					SFValue="alstatus" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"  />&nbsp;&nbsp;
			</td>
			<td align ="center" width="130">
		     	<fmtFieldAuditPostingErrorRpt:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" gmClass="button" style="width: 6em" buttonType="Load" onClick="fnLoad();" />
		    </td>
		</tr>
		<tr><td colspan="7" class="LLine" height="1"></td></tr>
		<tr>
			<td colspan="7"  height="10" >
				<display:table name="requestScope.frmFieldAuditReport.ldtResult" requestURI="/gmFieldAuditPostingError.do?method=fetchPostingErrorRpt" class="its" export="true" id="currentRowObject" freezeHeader="true" cellpadding="0" cellspacing="0" >
			 		<fmtFieldAuditPostingErrorRpt:message key="LBL_FIELD_SALES" var="varFieldSales"/><display:column property="DIST_NAME" title="${varFieldSales}"  class="alignleft" sortable="true" style="width:130" maxLength="15" />
			 		<fmtFieldAuditPostingErrorRpt:message key="DT_TAG_ID" var="varTagID"/><display:column property="TAGID" title="${varTagID}"  class="alignright" sortable="true" style="width:70"/>
			 		<display:column  class="alignright"  style="width:10"/>
			 		<fmtFieldAuditPostingErrorRpt:message key="DT_SET_ID" var="varSetID"/><display:column property="SETID" title="${varSetID}" class="alignleft" sortable="true"  style="width:70"/>
					<fmtFieldAuditPostingErrorRpt:message key="DT_SET_DESC" var="varSetDesc"/><display:column property="SETDESC" title="${varSetDesc}" class="alignleft" sortable="true" style="width:130" maxLength="15" />
					<fmtFieldAuditPostingErrorRpt:message key="DT_PART" var="varPart"/><display:column property="PNUM" title="${varPart}" class="alignright" sortable="true" style="width:70"/> 
					<display:column  class="alignright"  style="width:10"/>
					<fmtFieldAuditPostingErrorRpt:message key="DT_PART_DESC" var="varPartDesc"/><display:column property="PDESC" escapeXml="true" title="${varPartDesc}" class="alignleft" sortable="true" style="width:130" maxLength="15" />									
					<fmtFieldAuditPostingErrorRpt:message key="DT_REF_ID" var="varRefID"/><display:column property="TRANSFERID" title="${varRefID}" class="alignleft" sortable="true" titleKey="Ref Id" style="width:110" maxLength="15"/>					
					<fmtFieldAuditPostingErrorRpt:message key="DT_FLAG_TYPE" var="varFlagType"/><display:column property="REFTYPE" title="${varFlagType}" class="alignleft" sortable="true" titleKey="Flag Type" style="width:100" maxLength="15" />
					<fmtFieldAuditPostingErrorRpt:message key="DT_REASON" var="varReason"/><display:column property="REASON" title="${varReason}" class="alignleft" sortable="true" style="width:150" maxLength="20"/>
					<fmtFieldAuditPostingErrorRpt:message key="LBL_STATUS" var="varStatus"/><display:column property="STATUS" title="${varStatus}" class="alignleft" sortable="true" style="width:60" maxLength="8"/>
				</display:table>
			</td>
		</tr>
	</table>				
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>