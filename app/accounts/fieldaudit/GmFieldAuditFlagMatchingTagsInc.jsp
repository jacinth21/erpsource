
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtFieldAuditFlagMatchingTagsInc" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- accounts\fieldaudit\GmFieldAuditFlagMatchingTagsInc.jsp -->
<fmtFieldAuditFlagMatchingTagsInc:setLocale value="<%=strLocale%>"/>
<fmtFieldAuditFlagMatchingTagsInc:setBundle basename="properties.labels.accounts.fieldaudit.GmFieldAuditFlagDeviationTagsInc"/>
<% 
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	String strRptFmt = "{0,date,"+strApplDateFmt+"}";
%>
<table border="0" height="100%" width="1300" cellspacing="0"
	cellpadding="0">
	<tr>
		<td height="25" class="RightDashBoardHeader"><a
			href="javascript:fnShowFilters('tabMatch');" tabindex="-1"><IMG
				id="tabMatchimg" border=0 src="<%=strImagePath%>/plus.gif">
		</a>&nbsp;<fmtFieldAuditFlagMatchingTagsInc:message key="LBL_MATCHING_TAGS"/></td>
	</tr>
	<TR>
		<TD colspan="2">
			<table border="0" width="1300" height="100%" cellspacing="0"
				cellpadding="0" id="tabMatch" style="display: none">
				<tr>
					<td><display:table
							name="requestScope.frmFieldAuditFlagVariance.alMatchingReport"
							requestURI="/gmFieldAuditFlagVar.do?" excludedParams="strOpt"
							class="its">
							<fmtFieldAuditFlagMatchingTagsInc:message key="DT_TAG" var="varTag"/><display:column property="TAG_ID" title="${varTag}" />
							<fmtFieldAuditFlagMatchingTagsInc:message key="DT_CONTROL" var="varControl"/><display:column property="CNUM" title="${varControl}" />
							<fmtFieldAuditFlagMatchingTagsInc:message key="DT_PART" var="varPart"/><display:column property="PNUM" title="${varPart}" />
							<fmtFieldAuditFlagMatchingTagsInc:message key="DT_AUDIT_LOCATION" var="varAuditLocation"/><display:column property="AUDIT_LOC" title="${varAuditLocation}" />
							<fmtFieldAuditFlagMatchingTagsInc:message key="DT_RETAG" var="varReTag"/><display:column property="RETAG" title="${varReTag}" />
							<fmtFieldAuditFlagMatchingTagsInc:message key="DT_COUNTED_BY" var="varCountedBy"/><display:column property="COUNTED_BY" title="${varCountedBy}" />
							<fmtFieldAuditFlagMatchingTagsInc:message key="DT_COUNTED_DATE" var="varCountedDate"/><display:column property="COUNTED_DATE" title="${varCountedDate}" format="<%=strRptFmt %>"/>
							<fmtFieldAuditFlagMatchingTagsInc:message key="DT_COMMENTS" var="varComments"/><display:column property="COMMENTS" title="${varComments}" />
						</display:table>
					</td>
				</tr>
			</table></TD>
	</TR>
</table>
