<%
/****************************************************************************************************************************
	* File		 	: GmFieldAuditVarianceByDistRpt.jsp
	* Desc		 	: This screen is used to display the variance report by Distributer and Global variance By Set reports.
	* Version	 	: 1.0
	* author		: HReddi
*****************************************************************************************************************************/
%> 

<%@ page language="java" %>

<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtFieldAuditVarianceByDistRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- \accounts\fieldaudit\GmFieldAuditVarianceByDistRpt.jsp -->
<fmtFieldAuditVarianceByDistRpt:setLocale value="<%=strLocale%>"/>
<fmtFieldAuditVarianceByDistRpt:setBundle basename="properties.labels.accounts.fieldaudit.GmFieldAuditVarianceByDistRpt"/>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.List" %>

<bean:define id="reportType" name="frmFieldAuditReport" property="reportType" type="java.lang.String"> </bean:define>
<bean:define id="dispflaggedDet" name="frmFieldAuditReport" property="dispflaggedDet" type="java.lang.Boolean"> </bean:define>
<bean:define id="ldtResult" name="frmFieldAuditReport" property="ldtResult" type="java.util.List"></bean:define>
 
<%
String strWikiTitle		= "";
String strHeader		= "";
String flaggedDet		= dispflaggedDet.toString();
String header			= "";
String strTransferTo 	= "";
String strBorrowedFrom  = "";
String strExtra         = "";
String strReturns 		= "";
String strTransferFrom  = "";
String strLendTo        = "";
String strMissing       = "";
String strUnresolvedQty = "";
String strFlaggedQty 	= "";
String strNotAPosDev 	= "";
String strUnVoidTag 	= "";
String strVerifyPostCnt = "";
String strNotANegDev 	= "";
String strVoidTag 		= "";
HashMap hmLoop = new HashMap();
ArrayList alList = new ArrayList();
String strAccountFaJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_FIELDAUDIT");
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.accounts.fieldaudit.GmFieldAuditVarianceByDistRpt", strSessCompanyLocale);

	if (reportType.equals("globalSet")){
		 strWikiTitle = GmCommonClass.getWikiTitle("GLOBAL_SET");
		 strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_GLOBAL_VARIANCE_BY_SET"));
	}
	else{
		 strWikiTitle = GmCommonClass.getWikiTitle("VARIANCE_DISTRIBUTOR");
		 strHeader= GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_VARIANCE_REPORT_BY_DISTRIBUTOR"));
	}
	int rowsize = ldtResult.size();
	String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Field Sales Report</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     
    table.DtTable1000 {
	width: 1118px;
	}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strAccountFaJsPath%>/GmFieldAuditVarianceRpt.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script>
var dhxWins,w1;
var format = '<%=strApplnDateFmt%>';
</script>
</HEAD>

<BODY onkeyup="fnEnter();" leftmargin="20" topmargin="10" onmouseover="fnRowHighlight();">
 
<html:form  action="/gmFieldAuditReport.do?method=reportVarianceByDist"  >
<html:hidden property="strOpt"/>
<html:hidden property="reportType"/>
	<table border="0" class="DtTable1000" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" height="25" class="RightDashBoardHeader"><%=strHeader%></td>
			<td height="25" class="RightDashBoardHeader">
			<fmtFieldAuditVarianceByDistRpt:message key="IMG_HELP" var="varHelp"/>
				<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr>
			<td colspan="4" class="LLine" height="1"></td>
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr class="shade" >
		<% if(!reportType.equals("globalSet")){%>		
			<td height="30" align ="Right" class="RightTableCaption"><font color="red">*</font><fmtFieldAuditVarianceByDistRpt:message key="LBL_AUDIT_LIST" />:&nbsp;</td>
		<%} else { %>
			<td height="30" align ="Right" class="RightTableCaption"><font color="red"></font><fmtFieldAuditVarianceByDistRpt:message key="LBL_AUDIT_LIST"/>:&nbsp;</td>
		<%} %>
		    <td width="200"><gmjsp:dropdown controlName="auditId" SFFormName="frmFieldAuditReport" SFSeletedValue="auditId" 
								SFValue="alAuditName" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" onChange="fnReload()"/></td>
				
			<% if (!reportType.equals("globalSet")){	%>			
				<td height="30" align ="Right" class="RightTableCaption"><font color="red">*</font><fmtFieldAuditVarianceByDistRpt:message key="LBL_FIELD_SALES"/>:&nbsp;</td>
				<td  align ="left"><gmjsp:dropdown controlName="distId" SFFormName="frmFieldAuditReport" SFSeletedValue="distId" 
								SFValue="aldist" codeId = "DISTID"  codeName = "DISTNAME"  defaultValue= "[Choose One]"  /></td>
			<%} else { %>
				<td colspan="2"></td>					
			<% } %>			
		</tr>	
		<tr>
			<td colspan="4" class="LLine" height="1"></td>
		</tr>
		<tr>
			<td colspan="1">
				<jsp:include page="/accounts/fieldaudit/GmFieldAuditVarianceFilterRpt.jsp" >
					<jsp:param name="FORMNAME" value="frmFieldAuditReport" />
				</jsp:include>
			</td>
		</tr>		
		<tr>
			<td colspan="4" class="LLine" height="1"></td>
		</tr>
		<%if(rowsize >= 1){ %>
		<logic:equal name="frmFieldAuditReport" property="dispflaggedDet" value="true" >
		<tr><td colspan="8">
		<table border="0" class="DtTable1000" width="700" height="25" cellspacing="0" cellpadding="0">
			<tr>
				<td class = "ShadeDarkGrayTD" style="text-align:center;width:80;" >	</td> <td class = "ShadeDarkGrayTD" style="text-align:center;width:140;" >	</td>
				<td class = "ShadeDarkGrayTD" style="text-align:center;width:80;" >	</td> <td class = "ShadeDarkGrayTD" style="text-align:center;width:100;" >	</td>
				<td class = "ShadeDarkGrayTD" style="text-align:center;width:50;" >	</td> <td class = "ShadeDarkGrayTD" style="text-align:center;width:35;"  >	</td>
				<td class = "ShadeDarkGrayTD" style="text-align:center;width:35;" >	</td> <td class = "ShadeDarkGrayTD" style="text-align:center;width:35;"  >	</td>
				<td class = "ShadeDarkGrayTD" style="text-align:center;width:35;" >	</td> <td class = "ShadeDarkGrayTD" style="text-align:center;width:103;"  >	</td>
				<td class = "ShadeDarkGrayTD" style="text-align:center;width:165;">	<B><fmtFieldAuditVarianceByDistRpt:message key="LBL_POSITIVE_DEVIATION" /></B></td>
				<td class = "ShadeDarkGrayTD" style="text-align:center;width:230;">	<B><fmtFieldAuditVarianceByDistRpt:message key="LBL_NEGATIVE_DEVIATION" /></B></td>
				<td class = "ShadeDarkGrayTD" style="text-align:center;width:80;" >	</td> <td class = "ShadeDarkGrayTD" style="text-align:center;width:70;">	</td>
			</tr>
		</table>
		</td></tr>
		</logic:equal><%} %>	
		<tr>
			<td colspan="4" align= "center">		
				<display:table name="requestScope.frmFieldAuditReport.ldtResult" requestURI="/gmFieldAuditReport.do?method=reportVarianceByDist" export="true" decorator="com.globus.accounts.fieldaudit.displaytag.beans.DTFieldAuditRptWrapper" id="currentRowObject" varTotals="totals" freezeHeader="true" paneSize="500" cellpadding="0" cellspacing="0" >
					<fmtFieldAuditVarianceByDistRpt:message key="DT_SET_ID" var="varSetID"/><display:column property="SET_ID" 		 title="${varSetID}" 		  style="text-align: left; width:85;"  sortable="true" headerClass="ShadeDarkGrayTDCenter"  class="ShadeLightGrayTD"  />																	  
					<fmtFieldAuditVarianceByDistRpt:message key="DT_SET_DESCRIPTION" var="varSetDescription"/><display:column property="SET_DESC" 	 title="${varSetDescription}"  style="text-align: left; width:135;" sortable="true" maxLength="12" headerClass="ShadeDarkGrayTDCenter"   titleKey="SETDESC" class="ShadeLightGrayTD"  />
					<display:column property="PLINE" 		 title="" headerClass="Line" class="Line"  media="html" style="width:1"/>			 		
					<fmtFieldAuditVarianceByDistRpt:message key="DT_PART" var="varPart"/><display:column property="PNUM" 		 escapeXml="true" title="${varPart}" 		  style="text-align: left; width:60;" sortable="true" class="ShadeLightGrayTD" headerClass="ShadeDarkGrayTDCenter" />
					<fmtFieldAuditVarianceByDistRpt:message key="DT_PART_DESCRIPTION" var="varPartDescription"/><display:column property="PDESC" 		 title="${varPartDescription}" style="text-align: left; width:135;" sortable="true" maxLength="12" titleKey="PDESC"  class="ShadeLightGrayTD" headerClass="ShadeDarkGrayTDCenter" />
					<display:column property="PLINE" 		 title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
					
					
					
					<fmtFieldAuditVarianceByDistRpt:message key="DT_SYSTEM" var="varSystem"/><display:column property="SYSTEM" 		 title="${varSystem}" 		  style="text-align: right;width:30"   sortable="true" maxLength="5"  titleKey="SYSTEM"  class="ShadeLightGrayTD" headerClass="ShadeDarkGrayTDCenter" />
				    <display:column property="PLINE"         title="" headerClass="Line" class="Line"  media="html" style="width:1"/>
					<fmtFieldAuditVarianceByDistRpt:message key="DT_AUDIT_COUNT" var="varAuditCount"/><display:column property="AUDIT_QTY"     title="${varAuditCount}" 	  style="text-align: right;width:33;"  sortable="true" total="true"   headerClass="ShadeDarkBlueTD" class="ShadeLightBlueTD"/>				
					<fmtFieldAuditVarianceByDistRpt:message key="DT_TAG_QTY" var="varTagQty"/><display:column property="TAG_QTY"       title="${varTagQty}" 		  style="text-align: right;width:33;"  sortable="true" total="true"   headerClass="ShadeDarkBlueTD" class="ShadeMedBlueTD"/>
					<fmtFieldAuditVarianceByDistRpt:message key="DT_OPS_QTY" var="varOpsQty"/><display:column property="OPS_QTY"       title="${varOpsQty}" 		  style="text-align: right;width:33;"  sortable="true" total="true"   headerClass="ShadeDarkBlueTD" class="ShadeLightBlueTD"/>
					<fmtFieldAuditVarianceByDistRpt:message key="DT_SYSTEM_QTY" var="varSystemQty"/><display:column property="SYSTEM_QTY"    title="${varSystemQty}" 	  style="text-align: right;width:33;"  sortable="true" total="true"   headerClass="ShadeDarkBlueTD" class="ShadeMedBlueTD"/>
					<fmtFieldAuditVarianceByDistRpt:message key="DT_ABS_DEV" var="varAbsDev"/><display:column property="DEVIATION_QTY" title="${varAbsDev}" 		  style="text-align: right;width:33;"  sortable="true" total="true"   headerClass="ShadeDarkBlueTD" class="ShadeLightBlueTD" comparator="com.globus.common.displaytag.beans.DTComparator"/>
					<display:column property="PLINE" 		 title="" headerClass="Line" class="Line"  media="html" style="width:1"/>				
					<logic:equal name="frmFieldAuditReport" property="dispflaggedDet" value="true" >	
					    <fmtFieldAuditVarianceByDistRpt:message key="DT_TF" var="varTF"/><display:column property="TRANSFER_FROM"  title="${varTF}"  	style="text-align: right;width:35;" sortable="true" total="true" headerClass="ShadeDarkBrownTD" class="ShadeMedBrownTD" />
						<fmtFieldAuditVarianceByDistRpt:message key="DT_BF" var="varBF"/><display:column property="BORROWED_FROM"  title="${varBF}" 	style="text-align: right;width:35;" sortable="true" total="true" headerClass="ShadeDarkBrownTD" class="ShadeLightBrownTD" />
						<fmtFieldAuditVarianceByDistRpt:message key="DT_EXA" var="varExa"/><display:column property="EXTRA" 		  title="${varExa}" 	style="text-align: right;width:35;" sortable="true" total="true" headerClass="ShadeDarkBrownTD" class="ShadeMedBrownTD" />
						<fmtFieldAuditVarianceByDistRpt:message key="DT_ND" var="varND"/><display:column property="NOT_A_DEV_POS"  title="${varND}" 	style="text-align: right;width:35;" sortable="true" total="true" headerClass="ShadeDarkBrownTD" class="ShadeLightBrownTD" />
						<fmtFieldAuditVarianceByDistRpt:message key="DT_UV" var="varUV"/><display:column property="UNVOID_TAG" 	  title="${varUV}" 	style="text-align: right;width:35;" sortable="true" total="true" headerClass="ShadeDarkBrownTD" class="ShadeMedBrownTD" />		
						<display:column property="PLINE" 		  title="" 		style="width:1" headerClass="Line" class="Line"  media="html" />
						<fmtFieldAuditVarianceByDistRpt:message key="DT_RA" var="varRA"/><display:column property="RETURNS" 		  title="${varRA}" 	style="text-align: right;width:35;" sortable="true" total="true" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD" />
						<fmtFieldAuditVarianceByDistRpt:message key="DT_M" var="varM"/><display:column property="MISSING" 		  title="${varM}" 	style="text-align: right;width:35;" sortable="true" total="true" headerClass="ShadeDarkGreenTD" class="ShadeLightGreenTD" />
						<fmtFieldAuditVarianceByDistRpt:message key="DT_TT" var="varTT"/><display:column property="TRANSFER_TO" 	  title="${varTT}" 	style="text-align: right;width:35;" sortable="true" total="true" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD" />
						<fmtFieldAuditVarianceByDistRpt:message key="DT_LT" var="varLT"/><display:column property="LEND_TO" 		  title="${varLT}" 	style="text-align: right;width:35;" sortable="true" total="true" headerClass="ShadeDarkGreenTD" class="ShadeLightGreenTD" />
						<fmtFieldAuditVarianceByDistRpt:message key="DT_VPC" var="varVPC"/><display:column property="VER_POST_CNT"   title="${varVPC}" 	style="text-align: right;width:35;" sortable="true" total="true" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD" />
						<fmtFieldAuditVarianceByDistRpt:message key="DT_ND" var="varND"/><display:column property="NOT_A_DEV_NEG"  title="${varND}" 	style="text-align: right;width:35;" sortable="true" total="true" headerClass="ShadeDarkGreenTD" class="ShadeLightGreenTD" />
						<fmtFieldAuditVarianceByDistRpt:message key="DT_VT" var="varVT"/><display:column property="VOID_TAG" 	  title="${varVT}" 	style="text-align: right;width:35;" sortable="true" total="true" headerClass="ShadeDarkGreenTD" class="ShadeMedGreenTD" />
						<display:column property="PLINE" 		  title="" 		style="width:1" headerClass="Line" class="Line"  media="html"/>
						<fmtFieldAuditVarianceByDistRpt:message key="DT_FLAGGED_QTY" var="varFlaggedQty"/><display:column property="FLAGGED_QTY" 	  title="${varFlaggedQty}" 	sortable="true" style="width:50;" total="true" headerClass="ShadeDarkOrangeTD" class="ShadeLightOrangeTD" />
						<fmtFieldAuditVarianceByDistRpt:message key="DT_UNRESOLVED_QTY" var="varUnresolvedQty"/><display:column property="UNRESOLVED_QTY" title="${varUnresolvedQty}" sortable="true" style="width:75;" total="true" headerClass="ShadeDarkOrangeTD" class="ShadeMedOrangeTD" />
					</logic:equal>
					<logic:equal name="frmFieldAuditReport" property="dispflaggedDet" value="false">	
						<fmtFieldAuditVarianceByDistRpt:message key="DT_FLAGGED_QTY" var="varFlaggedQty"/><display:column property="FLAGGED_QTY" 	  title="${varFlaggedQty}" 	  sortable="true" style="width:100;text-align: right;" 	total="true" headerClass="ShadeDarkOrangeTD" class="ShadeLightOrangeTD"/>			
						<fmtFieldAuditVarianceByDistRpt:message key="DT_UNRESOLVED_QTY" var="varUnresolvedQty"/><display:column property="UNRESOLVED_QTY" title="${varUnresolvedQty}"  sortable="true" style="width:100;text-align: right;" 	total="true" headerClass="ShadeDarkOrangeTD" class="ShadeMedOrangeTD" />			
					</logic:equal>
					<display:footer media="html">				
					<%		
				    String strAuditQty 		= ((HashMap)pageContext.getAttribute("totals")).get("column9").toString();
					strAuditQty 			=   GmCommonClass.getStringWithCommas(strAuditQty,0);
					String strTagQty 		= ((HashMap)pageContext.getAttribute("totals")).get("column10").toString();
					strTagQty 				=   GmCommonClass.getStringWithCommas(strTagQty,0);
					String strOpsQty 		= ((HashMap)pageContext.getAttribute("totals")).get("column11").toString();
					strOpsQty 				=   GmCommonClass.getStringWithCommas(strOpsQty,0);
					String strSystemQty 	= ((HashMap)pageContext.getAttribute("totals")).get("column12").toString();
					strSystemQty 			= GmCommonClass.getStringWithCommas(strSystemQty,0);
					String strDeviationQty 	= ((HashMap)pageContext.getAttribute("totals")).get("column13").toString();
					strDeviationQty 		= GmCommonClass.getStringWithCommas(strDeviationQty,0);
					if(flaggedDet.equals("true")){
						 strTransferFrom 	= ((HashMap)pageContext.getAttribute("totals")).get("column15").toString();
						 strTransferFrom 	= GmCommonClass.getStringWithCommas(strTransferFrom,0);						 
						 strBorrowedFrom 	= ((HashMap)pageContext.getAttribute("totals")).get("column16").toString();
						 strBorrowedFrom 	= GmCommonClass.getStringWithCommas(strBorrowedFrom,0);
						 strExtra 			= ((HashMap)pageContext.getAttribute("totals")).get("column17").toString();
						 strExtra 			= GmCommonClass.getStringWithCommas(strExtra,0);
						 strNotAPosDev 		= ((HashMap)pageContext.getAttribute("totals")).get("column18").toString();
						 strNotAPosDev 		= GmCommonClass.getStringWithCommas(strNotAPosDev,0);
						 strUnVoidTag 		= ((HashMap)pageContext.getAttribute("totals")).get("column19").toString();
						 strUnVoidTag 		= GmCommonClass.getStringWithCommas(strUnVoidTag,0);
						 strReturns			= ((HashMap)pageContext.getAttribute("totals")).get("column21").toString();
						 strReturns 		= GmCommonClass.getStringWithCommas(strReturns,0);
						 strMissing			= ((HashMap)pageContext.getAttribute("totals")).get("column22").toString();
						 strMissing 		= GmCommonClass.getStringWithCommas(strMissing,0);
						 strTransferTo 		= ((HashMap)pageContext.getAttribute("totals")).get("column23").toString();
						 strTransferTo 		= GmCommonClass.getStringWithCommas(strTransferTo,0);
						 strLendTo			= ((HashMap)pageContext.getAttribute("totals")).get("column24").toString();
						 strLendTo 			= GmCommonClass.getStringWithCommas(strLendTo,0);
						 strVerifyPostCnt 	= ((HashMap)pageContext.getAttribute("totals")).get("column25").toString();
						 strVerifyPostCnt 	= GmCommonClass.getStringWithCommas(strVerifyPostCnt,0);
						 strNotANegDev		= ((HashMap)pageContext.getAttribute("totals")).get("column26").toString();
						 strNotANegDev 		= GmCommonClass.getStringWithCommas(strNotANegDev,0);
						 strVoidTag			= ((HashMap)pageContext.getAttribute("totals")).get("column27").toString();
						 strVoidTag 		= GmCommonClass.getStringWithCommas(strVoidTag,0);
						 strFlaggedQty		= ((HashMap)pageContext.getAttribute("totals")).get("column29").toString();
						 strFlaggedQty 		= GmCommonClass.getStringWithCommas(strFlaggedQty,0);
						 strUnresolvedQty	= ((HashMap)pageContext.getAttribute("totals")).get("column30").toString();	
						 strUnresolvedQty 	= GmCommonClass.getStringWithCommas(strUnresolvedQty,0);
						}
					else {
						 strFlaggedQty		= ((HashMap)pageContext.getAttribute("totals")).get("column15").toString();
						 strFlaggedQty 		= GmCommonClass.getStringWithCommas(strFlaggedQty,0);
						 strUnresolvedQty	= ((HashMap)pageContext.getAttribute("totals")).get("column16").toString();	
						 strUnresolvedQty 	= GmCommonClass.getStringWithCommas(strUnresolvedQty,0);
					}
					%>
					<tr ><td class="Line" colspan="30"></td></tr>
					<tr>
							<td class = "ShadeDarkGrayTD" colspan="8"> <B> <fmtFieldAuditVarianceByDistRpt:message key="LBL_TOTAL_RECORDS"/>: <%=rowsize %> 							</B> </td>
							<td class = "ShadeDarkGrayTD" style="text-align: right;width:33;" > <B>	<%=strAuditQty%>				</B> </td>
							<td class = "ShadeDarkGrayTD" style="text-align: right;width:33;" > <B>	<%=strTagQty%>					</B> </td>
							<td class = "ShadeDarkGrayTD" style="text-align: right;width:33;" > <B>	<%=strOpsQty%>					</B> </td>
							<td class = "ShadeDarkGrayTD" style="text-align: right;width:33;" > <B>	<%=strSystemQty%>				</B> </td> 
							<td class = "ShadeDarkGrayTD" style="text-align: right;width:33;" > <B>	<%=strDeviationQty%>			</B> </td> 
					<% if(flaggedDet.equals("true")){%>
							<td colspan="2" class ="ShadeDarkGrayTD" style="text-align: right;width:35;" ><B> <%=strTransferFrom%>	</B> </td>
							<td class = "ShadeDarkGrayTD" style="text-align: right;width:35;" > <B>	 <%=strBorrowedFrom%>			</B> </td>
	                        <td class = "ShadeDarkGrayTD" style="text-align: right;width:35;" > <B>	 <%=strExtra%>					</B> </td>
	                        <td class = "ShadeDarkGrayTD" style="text-align: right;width:35;" > <B>	 <%=strNotAPosDev%>				</B> </td>
	                        <td class = "ShadeDarkGrayTD" style="text-align: right;width:35;" > <B>	 <%=strUnVoidTag%>				</B> </td>
	                        <td colspan="2" class = "ShadeDarkGrayTD" style="text-align: right;width:40;" ><B><%=strReturns%> 		</B> </td>
	                        <td class = "ShadeDarkGrayTD" style="text-align: right;width:35;" > <B>	 <%=strMissing%> 				</B> </td>
	                        <td class = "ShadeDarkGrayTD" style="text-align: right;width:35;" >	<B>	 <%=strTransferTo%>				</B> </td>
	                        <td class = "ShadeDarkGrayTD" style="text-align: right;width:35;" >	<B>	 <%=strLendTo%>					</B> </td>
	                        <td class = "ShadeDarkGrayTD" style="text-align: right;width:35;" >	<B>	 <%=strVerifyPostCnt%>			</B> </td>
	                        <td class = "ShadeDarkGrayTD" style="text-align: right;width:35;" >	<B>	 <%=strNotANegDev%> 			</B> </td>
	                        <td class = "ShadeDarkGrayTD" style="text-align: right;width:35;" >	<B>	 <%=strVoidTag%>				</B> </td>
	                        <td colspan="2" class = "ShadeDarkGrayTD" style="text-align: right;width:50;" ><B><%=strFlaggedQty%>	</B> </td>
	                        <td class = "ShadeDarkGrayTD" style="text-align: right;width:75;" >	<B>	 <%=strUnresolvedQty%>			</B> </td>	                        
                       <%} else{ %>
						 	<td colspan="2" class = "ShadeDarkGrayTD" style="text-align: right;width:100;" ><B><%=strFlaggedQty%>	</B> </td>
						  	<td class = "ShadeDarkGrayTD" style="text-align: right;width:100;" > <B>  <%=strUnresolvedQty%> 		</B> </td>
						 <% } %>						 
					</tr>
					<tr><td class="Line" colspan="30"></td></tr>
					</display:footer>	
				</display:table>						
			</td>
		</tr>
		<% if((flaggedDet.equals("true")) && (rowsize >= 1) ){%> 	
		<tr><td class="Line" colspan="30"></td></tr>				
		<tr><td colspan="2" valign="top" >
			<b>Legend - Column Type:</b><BR>
			<table cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<tr> <td width="20" align="Right">&nbsp;&nbsp;<fmtFieldAuditVarianceByDistRpt:message key="DT_TF"/>  </td> <td width="400">- <fmtFieldAuditVarianceByDistRpt:message key="LBL_TRANSFER_FROM"/>  </td> <td width="20"><fmtFieldAuditVarianceByDistRpt:message key="LBL_RA"/>  </td> <td width="150">- <fmtFieldAuditVarianceByDistRpt:message key="LBL_RETURNS"/> 				</td> </tr>
						<tr> <td width="20" align="Right">&nbsp;&nbsp;<fmtFieldAuditVarianceByDistRpt:message key="DT_BF"/>  </td> <td width="400">- <fmtFieldAuditVarianceByDistRpt:message key="LBL_BORROWED_FROM"/>  </td> <td width="20"><fmtFieldAuditVarianceByDistRpt:message key="DT_M"/>   </td> <td width="150">- <fmtFieldAuditVarianceByDistRpt:message key="LBL_MISSING"/> 				</td> </tr>
						<tr> <td width="20" align="Right">&nbsp;&nbsp;<fmtFieldAuditVarianceByDistRpt:message key="DT_EXA"/> </td> <td width="400">- <fmtFieldAuditVarianceByDistRpt:message key="LBL_EXTRA"/>          </td> <td width="20"><fmtFieldAuditVarianceByDistRpt:message key="DT_TT"/>  </td> <td width="200">- <fmtFieldAuditVarianceByDistRpt:message key="LBL_TRANSFER_TO"/> 			</td> </tr>
						<tr> <td width="20" align="Right">&nbsp;&nbsp;<fmtFieldAuditVarianceByDistRpt:message key="DT_ND"/>  </td> <td width="400">- <fmtFieldAuditVarianceByDistRpt:message key="LBL_NOT_A_DEVIATION"/></td> <td width="20"><fmtFieldAuditVarianceByDistRpt:message key="DT_LT"/>  </td> <td width="200">- <fmtFieldAuditVarianceByDistRpt:message key="LBL_LEND_TO"/> 				</td> </tr>
						<tr> <td width="20" align="Right">&nbsp;&nbsp;<fmtFieldAuditVarianceByDistRpt:message key="DT_UV"/>  </td> <td width="400">- <fmtFieldAuditVarianceByDistRpt:message key="LBL_UNVOIDED_TAG"/>   </td> <td width="20"><fmtFieldAuditVarianceByDistRpt:message key="DT_VPC"/> </td> <td width="200">- <fmtFieldAuditVarianceByDistRpt:message key="LBL_VERIFIED_POST_COUNT"/> 	</td> </tr>
						<tr> <td width="20" align="Right">&nbsp;&nbsp;    </td> <td width="400">                 </td> <td width="20"><fmtFieldAuditVarianceByDistRpt:message key="DT_ND"/>  </td> <td width="200">- <fmtFieldAuditVarianceByDistRpt:message key="LBL_NOT_A_DEVIATION"/>		</td> </tr>
						<tr> <td width="20" align="Right">&nbsp;&nbsp;    </td> <td width="400">                 </td> <td width="20"><fmtFieldAuditVarianceByDistRpt:message key="DT_VT"/>  </td> <td width="200">- <fmtFieldAuditVarianceByDistRpt:message key="LBL_VOIDED_TAG_ONLY"/> 		</td> </tr>						
					</td>			
				</tr>
			</table>
			</td>							
		</tr>	
		<%} %>		
	</table>
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>