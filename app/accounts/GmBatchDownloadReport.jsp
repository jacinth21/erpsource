<%
/**********************************************************************************
 * File		 		: GmBatchDownloadReport.jsp
 * Desc		 		: Batch Report -- Download Invoice Detail Screen
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>

<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap, java.util.Calendar" %>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%> 
<%@ taglib prefix="fmtBatchDownloadReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmBatchDownloadReport.jsp -->
<fmtBatchDownloadReport:setLocale value="<%=strLocale%>"/>
<fmtBatchDownloadReport:setBundle basename="properties.labels.accounts.GmBatchDownloadReport"/>
<bean:define id="returnList" name="frmDownload" property="returnList" type="java.util.List"></bean:define>
 
 
<%
	ArrayList alSageVendorList = new ArrayList();
	 
	alSageVendorList = GmCommonClass.parseNullArrayList((ArrayList) returnList);
 
	int rowsize = alSageVendorList.size();
	double invoiceAmtTotal =0;
	
	GmCalenderOperations.setTimeZone(strGCompTimeZone); 
	String strWikiTitle = GmCommonClass.getWikiTitle("BATCH_REPORT_INVOICE_DETAIL");
	String strTodaysDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt);
	String strApplDateFmt = strGCompDateFmt;
	String strFromdaysDate = GmCalenderOperations.addMonths(-1, strApplDateFmt);
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Download Invoice Detail</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"	media="print" />
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
var format = '<%=strApplDateFmt%>';
function fnReload()
{     
//	alert(document.frmDownload.missingSageVenIds.value);
	var FromDT = document.frmDownload.batchDateFrom.value;
	var ToDT = document.frmDownload.batchDateTo.value;
	// check correct month format entered
		
    CommonDateValidation(document.frmDownload.batchDateFrom, format,message_accounts[272]);
    CommonDateValidation(document.frmDownload.batchDateTo, format,message_accounts[273]);
    
	// Validate Order From/To Dates in proper order
	if (dateDiff(FromDT, ToDT,format) < 0)
	{
		Error_Details(message_accounts[274]);
	}
	varBatchNumber = document.frmDownload.batchNumber.value;
	if(varBatchNumber != "")  
	chkValue(varBatchNumber);
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}	
	document.frmDownload.strOpt.value = "rptreload";
	fnStartProgress();
 	document.frmDownload.submit();   
} 
   
function chkValue(value){

   if (isNaN(value))
    {   	
    	Error_Details(message[35]);   	
    }
}

 function fnBatchdetails(strBatchId)
 { 
      windowOpener("/gmACDownload.do?method=loadInvoiceBatch&strOpt=batchDtlsFromRpt&batchNumber="+strBatchId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=820,height=400");
    }
 
 function fnCallAPPosting(strBatchId)
 { 
      windowOpener("/gmAPPostings.do?strOpt=reload&batchNumber="+strBatchId+"&batchDateFrom="+"<%=strFromdaysDate%>"+"&batchDateTo="+"<%=strTodaysDate%>","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=820,height=400");
    } 
   
  function   fnDefaultDate()
 {	
 	if(document.frmDownload.batchDateTo.value=="")
 	document.frmDownload.batchDateTo.value = "<%=strTodaysDate%>"; 
 	if(document.frmDownload.batchDateFrom.value=="")
 	document.frmDownload.batchDateFrom.value = "<%=strFromdaysDate%>"; 
 }
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnDefaultDate()">
<html:form action="/gmACDownload.do?method=loadInvoiceBatch">
	<html:hidden property="strOpt" value="" />  
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4"><fmtBatchDownloadReport:message key="LBL_BATCH_REPORT_INVOICE_DETAILS"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtBatchDownloadReport:message key="LBL_HELP" var="varHelp"/>
			 <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		<tr>
			<td bgcolor="#666666" height="1" colspan="5"></td>
		</tr>
		<tr>
			<td width="698" height="100" valign="top" colspan="5">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">  
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtBatchDownloadReport:message key="LBL_BATCH_NUMBER"/> :</td> 
                         <td>&nbsp;<html:text property="batchNumber"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;
			    		             </td> 
                    </tr>       
                    <tr><td colspan="4" class="ELine"></td></tr> 
		    <!-- Custom tag lib code modified for JBOSS migration changes -->
			                     <tr>
			                        <td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp;<fmtBatchDownloadReport:message key="LBL_BATCH_DOWNLOAD_DATE_FROM"/> :</td> 
				                     <td>&nbsp;
				                     <gmjsp:calendar SFFormName="frmDownload" controlName="batchDateFrom"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
				               &nbsp;&nbsp;	<fmtBatchDownloadReport:message key="LBL_TO"/>:
				               <gmjsp:calendar SFFormName="frmDownload" controlName="batchDateTo"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			    		            &nbsp;&nbsp;
			    		            <fmtBatchDownloadReport:message key="BTN_LOAD" var="varLoad"/>
			    		             <gmjsp:button value="${varLoad}" gmClass="button" buttonType="Load"  onClick="fnReload();" /> </td>
			                    </tr>
                     
		<tr>
			<td class="Line" height="1" colspan="5"></td>
		</tr>
		 
		<tr>
			<td colspan="5">
			<display:table name="requestScope.frmDownload.returnList" requestURI="/gmACDownload.do?method=loadInvoiceBatch" paneSize ="500" export= "true" class="its" id="currentRowObject" varTotals="totals" freezeHeader="true"  decorator="com.globus.accounts.displaytag.beans.DTBatchRptWrapper" >
			 <display:setProperty name="export.excel.filename" value="Batch Details Report.xls" />
 		 		<fmtBatchDownloadReport:message key="LBL_BATCH_NUMBER" var="varBatchNumber"/><display:column property="BATCHNUMBER" title="${varBatchNumber}"   />
				<fmtBatchDownloadReport:message key="LBL_BATCH_DATE_TIME" var="varBatchDateTime"/><display:column property="BATCH_DATETIME" title="${varBatchDateTime}"  class="alignleft" /> 
				<fmtBatchDownloadReport:message key="LBL_DOWNLAOD_IDENTIFIER" var="varDownloadIdentifier"/><display:column property="DOWNLOAD_IDENTIFIER" title="${varDownloadIdentifier}"  class="alignleft" /> 
				<fmtBatchDownloadReport:message key="LBL_INITIATED_BY" var="varInitiatedBy"/><display:column property="INITIATED_BY" title="${varInitiatedBy}"  class="alignleft" /> 
				<fmtBatchDownloadReport:message key="LBL_BATCH_AMOUNT" var="varBatchAmount"/><display:column property="BATCH_AMOUNT" title="${varBatchAmount}"  class="alignright" format="{0,number,$#,###,###.00}" total="true"/> 
				
				
			<display:footer media="html"> 
					<%
					String strVal ; 
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column5").toString();
					String strTotal = " $ " +  GmCommonClass.getStringWithCommas(strVal,2);
					 				
					%>
				<tr class = shade>
		  		<td colspan="4"> <B> <fmtBatchDownloadReport:message key="LBL_TOTAL"/> : </B></td> 
    	    	 <td class = "alignright" > <B><%=strTotal%></B></td>
	    	 	 
  				</tr>
  			</display:footer>
				 
			</display:table>
			</td>
		</tr>

		<tr>
			<td class="LLine" height="1" colspan="5"></td>
		</tr>
		 
		 
		 
	</table>
	</FORM>
	 
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>