<%
/**********************************************************************************
 * File		 		: GmBatchDownloadDtls.jsp
 * Desc		 		: Report for Batch details --Invoice Download
 * Version	 		: 1.0
 * author			: Xun
************************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtBatchDownloadDtls" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- accounts/datadownload/GmBatchDownloadDtls.jsp -->
<fmtBatchDownloadDtls:setLocale value="<%=strLocale%>"/>
<fmtBatchDownloadDtls:setBundle basename="properties.labels.accounts.datadownload.GmBatchDownloadDtls"/>

<bean:define id="hmReportValue" name="frmDownload"  property="hmInvoiceDownload" type="java.util.HashMap"></bean:define> 
<bean:define id="location" name="frmDownload" property="hlocation" type="java.lang.String"> </bean:define>

<%   
	String strWikiTitle = GmCommonClass.getWikiTitle("DEMAND_SHEET_SUMMARY");
 String strBatchNumber = (String) hmReportValue.get("BATCH_NUMBER");
 String strFileName = (String) hmReportValue.get("FILE_NAME");
 String strBatchDT = (String) hmReportValue.get("BATCH_DATETIME");
 String strInitBy	 = (String) hmReportValue.get("INITIATED_BY"); 
 
  	
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Batch details - Invoice Download</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">  
 

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
  
</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmACDownload.do?method=loadInvoiceBatch">
<html:hidden property="strOpt" value = ""/>

 
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtBatchDownloadDtls:message key="LBL_BATCH_DETAILS_INVOICES_DOWNLOAD"/></td>
			
			<td align="right" class=RightDashBoardHeader > 	
				  
				</td>
		</tr>
		<tr>
			<td width="698"   valign="top" colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr><td colspan="4" class="ELine"></td></tr> 
                    <tr>
						<td class="RightTableCaption" align="right" HEIGHT="30" > &nbsp;<fmtBatchDownloadDtls:message key="LBL_DOWNLOAD_IDENTIFIER"/>:</td>
						<td >&nbsp;
							<%=strBatchNumber%>												
						</td>
						<td class="RightTableCaption" align="right" HEIGHT="30" > &nbsp;<fmtBatchDownloadDtls:message key="LBL_FILE_NAME"/>:</td>
						<td >&nbsp;<%=strFileName%>
						 									
						</td>
						 
					</tr>	
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="30" > &nbsp;<fmtBatchDownloadDtls:message key="LBL_BATCH_DATA_TIME"/>:</td>
						<td >&nbsp;
							<%=strBatchDT%>											
						</td>
						<td class="RightTableCaption" align="right" HEIGHT="30" > &nbsp;<fmtBatchDownloadDtls:message key="LBL_LOCATION"/>:</td>
						<td >&nbsp;<%=location%>
						 									
						</td>
						 
					</tr>	
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="30" > &nbsp;<fmtBatchDownloadDtls:message key="LBL_INITIATED_BY"/>:</td>
						<td >&nbsp;
							<%=strInitBy%>											
						</td>
					 
						<td colspan =2>&nbsp; 
						 									
						</td>
						 
					</tr>	
					<tr><td colspan="4" class="LLine"></td></tr> 
					<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					<tr>
						<td colspan="4"> 		
						<jsp:include page= "/accounts/datadownload/GmInvoiceListBatchDownload.jsp" />
						</td>
					</tr>
					
				</table>
  			   </td>
  		  </tr>	 
    </table>	
    
    
    	     	
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>

