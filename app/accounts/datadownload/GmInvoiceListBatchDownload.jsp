<%
/**********************************************************************************
 * File		 		: GmInvoiceListBatchDownload.jsp
 * Desc		 		: This screen is used to display Invoices download list Detail
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%> 
 <%@ include file="/common/GmHeader.inc"%>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>

<%@ taglib prefix="fmtInvoiceListBatchDownload" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- accounts/datadownload/GmInvoiceListBatchDownload.jsp -->
<fmtInvoiceListBatchDownload:setLocale value="<%=strLocale%>"/>
<fmtInvoiceListBatchDownload:setBundle basename="properties.labels.accounts.datadownload.GmBatchDownloadDtls"/>


<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css"> 
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>

function fnPrint()
{
	window.print();
}
</script>
<%
String strApplDateFmt = strGCompDateFmt;
String strDateFmt = "{0,date,"+strApplDateFmt+"}";

HashMap hmCurrency = new HashMap();
hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
String strCurrSign =(String)hmCurrency.get("CMPCURRSMB");
String strApplCurrFmt = (String)hmCurrency.get("CMPCURRFMT");
String strCurrPos = GmCommonClass.getString("CURRPOS");
String strCurrFmt = "{0,number,"+strCurrSign+" "+strApplCurrFmt+"}";
%> 
 
 <table border="0" width="100%" cellspacing="0" cellpadding="0">
		 	
		<tr>
			<td colspan="5">
			<display:table name="requestScope.frmDownload.returnList" requestURI="/gmACDownload.do?method=loadInvoiceBatch"  class="its" id="currentRowObject" varTotals="totals"  > 
 		 		<fmtInvoiceListBatchDownload:message key="DT_SAGE_VENDOR_ID" var="varSageVendorID"/><display:column property="SAGE_VENDOR_ID" title="${varSageVendorID}"   />
				<fmtInvoiceListBatchDownload:message key="DT_VENDOR_NAME" var="varVendorName"/><display:column property="VENDOR_NAME" title="${varVendorName}"  class="alignleft" /> 
				<fmtInvoiceListBatchDownload:message key="DT_INVOICE_NUMBER" var="varInvoiceNumber"/><display:column property="INVOICE_NUMBER" title="${varInvoiceNumber}"  class="aligncenter" />
				<fmtInvoiceListBatchDownload:message key="DT_INVOICE_DATE" var="varInvoiceDate"/><display:column property="INVOICE_DT" title="${varInvoiceDate}"  class="alignleft" format= "<%=strDateFmt%>"/> 
				<fmtInvoiceListBatchDownload:message key="DT_INVOICE_AMT" var="varInvoiceAmt"/><display:column property="INVOICE_AMOUNT" title="${varInvoiceAmt}"  class="alignright" format="<%=strCurrFmt%>"  total="true"/> 
				
				
			<display:footer media="html"> 
					<%
					String strVal ; 
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column5").toString();
					String strTotal = strCurrSign +  GmCommonClass.getStringWithCommas(strVal,2);
					 				
					%>
				<tr class = shade>
		  		<td colspan="4"> <B><fmtInvoiceListBatchDownload:message key="LBL_TOTAL"/> : </B></td> 
    	    	 <td class = "alignright" > <B><%=strTotal%></B></td>
	    	 	 
  				</tr>
  			</display:footer>
				 
			</display:table>
			</td>
		</tr>
		
				<tr>
					<td class="LLine" height="1" colspan="6"></td>
				</tr> 
				<tr>
					<td  colspan="6" class = "aligncenter" ><p> &nbsp;</p>
					<fmtInvoiceListBatchDownload:message key="BTN_PRINT" var="varPrint"/><gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" buttonType="Load" onClick="fnPrint();" />
					</td>
				</tr> 
    <table>
 
 <p> &nbsp;</p>
  <%@ include file="/common/GmFooter.inc" %>
  
 