<%
/**********************************************************************************
 * File		 		: GmPALockTag.jsp
 * Desc		 		: This screen is used to display Lock Vs Tag Count
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap, java.util.Calendar" %>
 
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ taglib prefix="fmtPALockTag" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \accounts\physicalaudit\GmPALockTag.jsp -->
<fmtPALockTag:setLocale value="<%=strLocale%>"/>
<fmtPALockTag:setBundle basename="properties.labels.accounts.physicalaudit.GmPAInvSummaryRpt"/>
 
 
 
<HTML>
<HEAD>
<TITLE> Globus Medical: Lock vs. Tag</TITLE>

<%  
String strWikiTitle = GmCommonClass.getWikiTitle("LOCK_INVENTORY_VS_TAG_COUNT_VALIDATION");
	%> 

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>	
<script> 

  
 
 function fnReload()
{
	fnValidateDropDn('physicalAuditID',message_accounts[199]); 
	fnValidateDropDn('locationID',message_accounts[293]);
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	} 
	  
	document.frmLockTag.strOpt.value = "reload";
	conflag = '';
	objCheck = document.frmLockTag.displayOnDeviation;
	if(objCheck) {
	 
			    if(objCheck.checked)
			    {				    		   
				conflag = '1';
			 			
				}
			 
	//alert("	conflag is .." + conflag);	
		 if(conflag=="")
		 {		 
		 document.frmLockTag.hstatusNotcheck.value="true";
		 }
		 else
		 { 
		  document.frmLockTag.hstatusNotcheck.value="";
		 }
	}
	document.body.style.cursor = 'wait';
	fnStartProgress();
	document.frmLockTag.submit();
}
 

</script>
 
</HEAD>


<BODY leftmargin="20" topmargin="10" onLoad="enableByPart()">
 
<html:form action="/gmLockTag.do"  >
<html:hidden property="strOpt" value=""/> 
<html:hidden property="hinputReconAction" /> 
<html:hidden property="hstatusNotcheck" /> 
 
 <table class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" height="20" class="RightDashBoardHeader">&nbsp;<fmtPALockTag:message key="LBL_LOCK_INVENTORY_VS_TAG_COUNT_VALIDATION"/></td>
			<td  height="20" class="RightDashBoardHeader" align="right">
			<fmtPALockTag:message key="IMG_HELP" var="varHelp"/>
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		 
		</tr>
	 					
<jsp:include page="/accounts/physicalaudit/GmPAIncludeLockFilter.jsp" flush="true">
		<jsp:param name="FORMNAME" value="frmLockTag" />
		 
			</jsp:include>
	
 
  <logic:equal name="frmLockTag" property="strOpt" value="reload"> 
	
 <jsp:include page="/accounts/physicalaudit/GmPAIncludeLockRpt.jsp"  flush="true">
	<jsp:param name="FORMNAME" value="frmLockTag" />
 		</jsp:include>	
 	 	
 	</logic:equal>	 
 </table>
 
 
 	
  </html:form>
  

   <%@ include file="/common/GmFooter.inc" %>
  
  
</BODY>

</HTML>
