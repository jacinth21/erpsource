<%
	/**********************************************************************************
	 * File		 		: GmTag.jsp
	 * Desc		 		: This screen is used tag entry or edit
	 * Version	 		: 1.0
	 * author			: Xun
	 * 
	 ************************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ taglib prefix="fmtMoveTagLocation" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- \accounts\physicalaudit\GmMoveTagLocation.jsp -->
<fmtMoveTagLocation:setLocale value="<%=strLocale%>"/>
<fmtMoveTagLocation:setBundle basename="properties.labels.accounts.physicalaudit.GmGenerateHTMLTags"/>

<%

String strAccountPaJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_PHYSICALAUDIT");
	String strWikiTitle = GmCommonClass.getWikiTitle("PART_TAG_ENTRY");
%> 

<HTML>
<HEAD>
<TITLE>Globus Medical: Part Tag Info</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<bean:define id="gridData" name="frmTagReport" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="alDistributor" name="frmTagReport" property="alDistributor" type="java.util.ArrayList"> </bean:define>
<bean:define id="alRepList" name="frmTagReport" property="alRepList" type="java.util.ArrayList"> </bean:define> 
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strAccountPaJsPath%>/GmMoveTagLocation.js"></script>
<style type="text/css" media="all">
     @import url("styles/displaytag.css");
</style>

<script> 
gridObjData = '<%=gridData%>';
var RepLen = <%=alRepList.size()%>;
<%
	int intSize = alRepList.size();
 	HashMap hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alRepList.get(i);
%>
		RepArr<%=i%> ="<%=hcboVal.get("SID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>

var distLen = <%=alDistributor.size()%>;
<%
	HashMap hcboVals = new HashMap();
	for (int i=0;i<alDistributor.size();i++)
	{
		hcboVals = (HashMap)alDistributor.get(i);
%>
		DistArr<%=i%> ="<%=hcboVals.get("DID")%>,<%=hcboVals.get("NAME")%>";
<%
	}
%>
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();" onkeyup="fnEnter();">
<html:form action="/gmMoveTagLocation.do">
	<html:hidden property="strOpt" value="" />
	<html:hidden property="hinputString" value="" />
	<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4">&nbsp;<fmtMoveTagLocation:message key="LBL_MOVE_TAG_LOCATION"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtMoveTagLocation:message key="IMG_HELP" var="varHelp"/>
			  <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		 
	    <table border="0"   class="DtTable1000" cellspacing="0"  cellpadding="0"  >
	    <!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr class="shade">
			<td class="RightTableCaption" HEIGHT="25" align="right">&nbsp;<fmtMoveTagLocation:message key="LBL_OWNER"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="distributorId" SFFormName="frmTagReport" SFSeletedValue="distributorId" 
							SFValue="alDistributor" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"  />
			</td>
		</tr>
		<tr>
			<td class="RightTableCaption" HEIGHT="25" align="right">&nbsp;<fmtMoveTagLocation:message key="LBL_ACCOUNT_NAME"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="accId" SFFormName="frmTagReport" SFSeletedValue="accId" 
							SFValue="alAccList" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]"  />
			</td>
		</tr>
		<tr class="shade">
			<td class="RightTableCaption" HEIGHT="25" align="right">&nbsp;<fmtMoveTagLocation:message key="LBL_SET_ID"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="setID" SFFormName="frmTagReport" SFSeletedValue="setID" 
							SFValue="alSet" codeId = "ID"  codeName = "IDNAME"  defaultValue= "[Choose One]"  />
			</td>
		</tr>
		<tr>
		<td class="RightTableCaption" HEIGHT="25" align="right">&nbsp;<fmtMoveTagLocation:message key="LBL_TAG_RANGE_FROM"/>:</td>
		<td>&nbsp;<html:text property="tagIdFrom"  size="15" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
		&nbsp;&nbsp;<strong><fmtMoveTagLocation:message key="LBL_TO"/>:</strong>
		&nbsp;
		<html:text property="tagIdTo"  size="15" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
		</td>
		</tr>
	<tr class="shade">
			<td colspan=2 class="RightTableCaption" HEIGHT="30" align="center">&nbsp;<fmtMoveTagLocation:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnGo();" buttonType="Load" />&nbsp;	</td>
	</tr>		
	</table>
 		<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
	    <tr>
			<td colspan="6" width="100%">
				<div id="dataGridDiv" style="height:230px;width:998px;"></div>
			</td>
		</tr> 
		
			<%if(!gridData.equalsIgnoreCase("")){%>
		<tr>
			<td colspan="6" width="100%" align="center">
				<BR><BR><fmtMoveTagLocation:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnSubmit();" buttonType="Save" tabindex="6" /><BR><BR>
			</td>
		</tr>
		<%}%>
	</table>

</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>