 <%
/**********************************************************************************
 * File		 		: GmPATagDetail.jsp
 * Desc		 		: This screen is used for Generating the Tags
 * author			: VPrasath
 ************************************************************************************/
%>
  <%@ include file="/common/GmHeader.inc" %>
  <%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap, java.util.Calendar" %> 
  <%@ page import ="com.globus.common.beans.GmCommonControls"%>
  <%@ taglib prefix="fmtPATagDetail" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \accounts\physicalaudit\GmPATagDetail.jsp -->
<fmtPATagDetail:setLocale value="<%=strLocale%>"/>
<fmtPATagDetail:setBundle basename="properties.labels.accounts.physicalaudit.GmPATag"/>
 
 <bean:define id="returnList"  name="frmLockTag" property="returnList" type="java.util.ArrayList"></bean:define>
 <bean:define id="historyfl" name="frmLockTag" property="historyfl" type="java.lang.String"></bean:define>
 <bean:define id="piType" name="frmLockTag" property="piType" type="java.lang.String"></bean:define>
 <bean:define id="hPart" name="frmLockTag" property="hPart" type="java.lang.String"></bean:define>
 <bean:define id="hTransactionID" name="frmLockTag" property="hTransactionID" type="java.lang.String"></bean:define>
<%
String strWikiTitle = GmCommonClass.getWikiTitle("TAG_RECORDING");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Tag Recording</TITLE>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
var InvLocation ='<%=piType%>';
var lblWarehouse = '<fmtPATagDetail:message key="LBL_WAREHOUSE"/>';
var lblTagId = '<fmtPATagDetail:message key="LBL_TAG_ID"/>';
var lblQty = '<fmtPATagDetail:message key="LBL_QTY"/>';
function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}


function fnChangeFocus(frmElement)
 {
    var obj = eval("document.frmLockTag."+frmElement);
    var errorField = "";
    
	if(event.keyCode == 13)
	 {	 
		  if(changeListBoxValue(frmElement));
		 	{
			  if( frmElement == 'submit')
		    	{
				  if(document.frmLockTag.Btn_Save.disabled !=true)
		      		fnSave();
		    	}
		    	else
		    	{
		    	   if(obj.disabled != true)
		    	    {
						obj.focus();
					}
					else
					{
					 document.frmLockTag.tagQty.focus();
					}	
				}
			}			
	 }
 }
 
 function changeListBoxValue(frmElement)
 {
 var errorField = "";
 
 if(frmElement == "auditUserTemp")
		  {				 
		  if (document.frmLockTag.locationTemp.value == "")
		  {
		    return false;		    
		  } 
		  var iters = document.frmLockTag.locationID.length;
		  var locationTempValue = document.frmLockTag.locationTemp.value;
			var i;

		    if(locationTempValue > iters)
		     {
		        	 Error_Details(Error_Details_Trans(message_accounts[509],iters));		        	
		        	 document.frmLockTag.locationTemp.focus(); 
		     }
     	 else{
					for (i=0; i<iters; i++)
					{			
					var locationno = document.frmLockTag.locationID.options[i].text;
					
					var locationnosub = trim(locationno.substring(0,locationno.indexOf("-")));
		
					if(locationnosub == locationTempValue)
					 {				
						 document.frmLockTag.locationID.value = document.frmLockTag.locationID.options[i].value;
						 document.frmLockTag.locationTemp.value = "";
						 break;
					 }			
					}
					if( document.frmLockTag.locationTemp.value != "")
					 {
					   Error_Details(message_accounts[526]);	
					   document.frmLockTag.locationTemp.focus();				  			  
					 }
			 }
		 }  
		 
		 if(frmElement == "tagQty")
		  {
		  if (document.frmLockTag.auditUserTemp.value == "")
		  {
		    return false;		    
		  }
		  
		  	var iters = document.frmLockTag.auditUserID.length;
		  	var auditUserTempValue = document.frmLockTag.auditUserTemp.value;
		  	
			var i;

				for (i=0; i<iters; i++)
				{			
				var userno = document.frmLockTag.auditUserID.options[i].text;			
				var usernosub = trim(userno.substring(0,userno.indexOf("-")));
				
				if(usernosub == auditUserTempValue)
				 {				
					 document.frmLockTag.auditUserID.value = document.frmLockTag.auditUserID.options[i].value;
					 document.frmLockTag.auditUserTemp.value = "";
					 break;
				 }			
				}
				
				if( document.frmLockTag.auditUserTemp.value != "")
					 {
					   Error_Details(message_accounts[527]);
					   document.frmLockTag.auditUserTemp.focus();
					 }
			  
		  }
		  
		  if (ErrorCount > 0)  
			{
			Error_Show();
			Error_Clear();			
			return false;	
			}
		else
		  {
		    return true;
		  }	
				
 }
 
 function setFocus(errorField)
  {
    if(errorField != "")
     {
     	var obj = eval("document.frmLockTag."+errorField);
     	obj.focus();
     }	
  }
  
 function fnChangeDropDownFocus(frmElement)
 {
    var obj = eval("document.frmLockTag."+frmElement);
    obj.focus();
	
 }
 
function reload()
 {
   	 	var tagId = document.frmLockTag.tagID.value;
   	    document.frmLockTag.pnum.value = '';
   	    document.frmLockTag.transactionID.value ='';
     	document.frmLockTag.strOpt.value = 'RELOAD';             	
      	document.frmLockTag.submit();   
 }
 
 function reloadpart()
 {
	 if(InvLocation != ''){
		var partNum = document.frmLockTag.pnum.value;
    	document.frmLockTag.strOpt.value = 'RELOAD';        
    	document.frmLockTag.submit();
	 }
 }
 
 function reloadTransaction()
 {
	 if(InvLocation != ''){
	 var txn = document.frmLockTag.transactionID.value;
		if(txn.indexOf("GM-") != -1)
	    {
    	document.frmLockTag.strOpt.value = 'RELOAD';        
    	document.frmLockTag.submit();
	    }
	 }
 }
 
 function fnSave()
 { 
	fnValidateDropDn('locationID', lblWarehouse);
	fnValidateTxtFld('tagID',lblTagId);
	fnValidateTxtFld('tagQty',lblQty);
	
	if(document.frmLockTag.auditUserID.value == '0')
	{
	 Error_Details(message_accounts[290]);
	}
	
	if(document.frmLockTag.locationID.value == '0')
	{
	Error_Details(message_accounts[291]);
	}
	
	if(document.frmLockTag.tagQty.value == '0')
	{
		var qtyConfirm = confirm(message_accounts[292])
	        if (qtyConfirm==false)
	           return false;
	}
	
	var txn = document.frmLockTag.transactionID.value;
	
	if(txn.indexOf("GM-") != -1)
    {
      var qty = document.frmLockTag.tagQty.value;
      
      if  ( qty > 1 )
       {
         	Error_Details(message_accounts[528]);
       }
    }
    var maxqty = document.frmLockTag.tagQty.value;
    if(maxqty.length > 7)
    {
    	Error_Details(message_accounts[529]);
    }
    
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			document.frmLockTag.tagQty.value= '';
			return false;
	}
 	document.frmLockTag.transactionID.disabled = false;
 	document.frmLockTag.pnum.disabled = false;
 	document.frmLockTag.locationID.disabled = false;
 	document.frmLockTag.auditUserID.disabled = false;
 	document.frmLockTag.strOpt.value = 'SAVE';
 	fnStartProgress();
  	document.frmLockTag.submit();
 }
 
 function fnLoad()
 {
 		document.frmLockTag.tagQty.focus();
 		
 		var hpnum = '<%=hPart%>';
 		var hTransactionID = '<%=hTransactionID%>';
   	 if (document.frmLockTag.auditUserID.value == "0")
   	{
   		document.frmLockTag.auditUserID.disabled=true;
    	document.frmLockTag.auditUserTemp.focus();    	
   	}
   	else
   	{
	   	 document.frmLockTag.auditUserID.disabled=true;
	   	 document.frmLockTag.auditUserTemp.disabled=true;
   	}
   	
   	 if (document.frmLockTag.locationID.value == "0")
   	{
   		document.frmLockTag.locationID.disabled=true;
    	document.frmLockTag.locationTemp.focus();    	
   	}
   	else
   	{
   	 document.frmLockTag.locationID.disabled=true;
   	 document.frmLockTag.locationTemp.disabled=true;
   	}
   	
   	if (document.frmLockTag.transactionID.value == "" && InvLocation !="50660")
   	{
    	document.frmLockTag.transactionID.focus();    	
   	}
   	else
   	{
   	 document.frmLockTag.transactionID.disabled=true;
   	}

   	if (document.frmLockTag.pnum.value == ""  && InvLocation !="50661")
   	{
   		document.frmLockTag.pnum.focus();    	
   	}
   	else
   	{
   	 document.frmLockTag.pnum.disabled=true;
   	}
   	if(InvLocation =="50662"){
   		if(hpnum =="")
   			document.frmLockTag.pnum.disabled=false;
   		if(hTransactionID =="")
   			document.frmLockTag.transactionID.disabled=false;
   	} else if(InvLocation =="50660"){
   		if(hpnum =="")
   			document.frmLockTag.pnum.disabled=false;
   	} else if(InvLocation =="50661"){
   		if(hTransactionID =="")
   			document.frmLockTag.transactionID.disabled=false;
   	} else if(InvLocation ==""){
   			document.frmLockTag.pnum.disabled=false;
   			document.frmLockTag.transactionID.disabled=false;
   			document.frmLockTag.auditUserTemp.disabled=false;
   			document.frmLockTag.locationTemp.disabled=false;
   	}
  if (document.frmLockTag.tagID.value == "")
   {
    document.frmLockTag.tagID.focus();
    
   }
   var txn = document.frmLockTag.transactionID.value;
   
   if(txn.indexOf("GM-") != -1)
    {
	   if(document.frmLockTag.tagQty.value =='')
     	document.frmLockTag.tagQty.value = "1";
    }
      	
 }
 function fnVoid()
{
		document.frmLockTag.hTxnId.value = document.frmLockTag.tagID.value;
		document.frmLockTag.action ="/GmCommonCancelServlet";
		document.frmLockTag.hAction.value = "Load";		
		document.frmLockTag.hCancelType.value = 'VDTAG'
		document.frmLockTag.submit();
}

function fnReqHistory()
{
	  
	 varTagId = document.frmLockTag.tagID.value;
	  
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=1008&txnId="+varTagId,"","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
  }

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad();">
 
<html:form action="/gmEnterTag.do"   >
<html:hidden property="strOpt" value=""/> 
<html:hidden property="partDescription" />
<input type="hidden" name="hTxnId">
<input type="hidden" name="hCancelType">
<input type="hidden" name="hAction" >
 

 <table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
	<tr>
			<td height="25" class="RightDashBoardHeader" colspan="5"><fmtPATagDetail:message key="LBL_TAG_RECORDING"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtPATagDetail:message key="IMG_HELP" var="varHelp"/>
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
	</tr>
		
		<tr><td class="LLine" height="1" colspan="2"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td height="30" class="RightTableCaption" align="right"><font color="red">*</font><fmtPATagDetail:message key="LBL_TAG_ID"/> :</td>
			<td colspan="2">&nbsp;<html:text property="tagID"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" tabindex="1" onblur="changeBgColor(this,'#ffffff');"  onchange="javascript:reload();" onkeypress="javascript:fnChangeFocus('transactionID');" /></td>
			
			<td height="30" class="RightTableCaption" align="right"><fmtPATagDetail:message key="LBL_WAREHOUSE"/> :</td>
			<td>&nbsp;<input type="text" name="locationTemp" maxlength=2 size=3 class="InputArea" tabindex="3" onkeypress="javascript:fnChangeFocus('auditUserTemp');"  onChang="javascript:changeListBoxValue('auditUserTemp');" >&nbsp;<gmjsp:dropdown controlName="locationID" SFFormName="frmLockTag" SFSeletedValue="locationID" tabIndex="3" 
							SFValue="locationList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
			</td>			
		</tr>		

		<tr><td colspan="6" class="LLine"></td></tr>

		<tr class="Shade">
		<td height="30" class="RightTableCaption" align="right"><fmtPATagDetail:message key="DT_PART"/> :</td>
			<td colspan="2">&nbsp;<html:text property="pnum" tabindex="2" size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"  onchange="javascript:reloadpart();" onkeypress="javascript:fnChangeFocus('locationTemp');"  /></td>		
			<td height="30" class="RightTableCaption" align="right"><fmtPATagDetail:message key="DT_COUNTED_BY"/> :</td>
			<td colspan="2">&nbsp;<input type="text" name="auditUserTemp" size=6 class="InputArea" tabindex="4" onkeypress="javascript:fnChangeFocus('tagQty');" onChange="javascript:changeListBoxValue('tagQty');">&nbsp;<gmjsp:dropdown controlName="auditUserID" SFFormName="frmLockTag" SFSeletedValue="auditUserID" tabIndex="4" 
							SFValue="auditUserList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
			</td>					
		</tr>				  

		<tr><td colspan="6" class="LLine"></td></tr>
		<tr>								
		 		<td height="30" class="RightTableCaption" align="right"><fmtPATagDetail:message key="DT_REF_ID"/> :</td>
		 		<td colspan="2">&nbsp;<html:text property="transactionID" tabindex="2" size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"  onchange="javascript:reloadTransaction();" onkeypress="javascript:fnChangeFocus('locationTemp');"  /></td>	
		 		<td height="30" class="RightTableCaption" align="right"><fmtPATagDetail:message key="LBL_UOM"/> :</td>
		 		<td colspan="2">&nbsp;<bean:write property="uom" name="frmLockTag" /></td>
		 </tr>	
		 
		 <tr><td colspan="6" class="LLine"></td></tr>	
		 <tr class="Shade">								
		 	<td height="30" class="RightTableCaption" align="right"><fmtPATagDetail:message key="LBL_DESCRIPTION"/> :</td>
			<td colspan="2">&nbsp;<bean:write property="partDescription" name="frmLockTag" /> </td>
			<td height="30" class="RightTableCaption" align="right"><fmtPATagDetail:message key="LBL_QTY"/> :</td>
			<td colspan="2">&nbsp;<html:text property="tagQty" tabindex="5" maxlength="13" size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" onkeypress="javascript:fnChangeFocus('submit');" />
			<%  if ( historyfl.equals("Y") ) { %>
			<fmtPATagDetail:message key="IMG_REQUIRED_DATE_HISTORY" var="varRequiredDateHistory"/>
<img  id="imgEdit" style="cursor:hand" onclick="javascript:fnReqHistory();" title="${varRequiredDateHistory}"  
							src="<%=strImagePath%>/icon_History.gif" align="bottom"  height=18 width=18 />&nbsp;	
							
							<% } %>
			</td>
		</tr>	
					
		<tr><td colspan="6" class="LLine"></td></tr>
		<tr>								
		 		<td colspan="4" height="30" align="right">
		 		<logic:equal name="frmLockTag" property="strEnableSubmit" value="false">
		 		<fmtPATagDetail:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button  tabindex="6" value="${varSubmit}" disabled="true" name="Btn_Save" gmClass="button" onClick="javascript:fnSave();" buttonType="Save" />&nbsp;
		 		</logic:equal>
		 		<logic:equal name="frmLockTag" property="strEnableSubmit" value="true">
		 		<fmtPATagDetail:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button  tabindex="6" value="${varSubmit}" name="Btn_Save" gmClass="button" onClick="javascript:fnSave();" buttonType="Save" />&nbsp;
		 		</logic:equal>
		 		  </td>
		 		<td colspan="4" height="30" >&nbsp;
		 		<logic:equal name="frmLockTag" property="strEnableVoid" value="false">
		 		<fmtPATagDetail:message key="BTN_VOID" var="varVoid"/><gmjsp:button  tabindex="7" value="${varVoid}" disabled="true" name="Btn_void" gmClass="button" onClick="javascript:fnVoid();" buttonType="Save" />
		 		</logic:equal>
		 		<logic:equal name="frmLockTag" property="strEnableVoid" value="true">
		 		<fmtPATagDetail:message key="BTN_VOID" var="varVoid"/><gmjsp:button  tabindex="7" value="${varVoid}" name="Btn_void" gmClass="button" onClick="javascript:fnVoid();" buttonType="Save" />
		 		</logic:equal>
		 		</td>			
		</tr>
</table>
</html:form>
  <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>