<%
/**********************************************************************************
 * File		 		: GmTagStatus.jsp
 * Desc		 		: This screen is used for the asset attribute Report
 * Version	 		: 1.0
 * author			: Jignesh Shah
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtTagStatus" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \accounts\physicalaudit\GmTagStatus.jsp -->
<fmtTagStatus:setLocale value="<%=strLocale%>"/>
<fmtTagStatus:setBundle basename="properties.labels.accounts.physicalaudit.GmTagStatus"/> 

<% 
String strWikiTitle = GmCommonClass.getWikiTitle("USER_GROUP_MAPPING");
%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<HTML>
<HEAD>
<TITLE> Globus Medical: Tag Status </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmTagStatus.js"></script>
<script>
var lblTagRangeFrom = '<fmtTagStatus:message key="LBL_TAG_RANGE_FROM"/>';
var lblTagRangeTo ='<fmtTagStatus:message key="LBL_TAG_RANGE_TO"/>';
var lblStatus = '<fmtTagStatus:message key="LBL_STATUS"/>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmTagStatus.do"> 
<input type="hidden" name="strOpt" value="" />
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtTagStatus:message key="LBL_TAG_STATUS"/> </td>
			<td height="25" class="RightDashBoardHeader">
			<fmtTagStatus:message key="IMG_HELP" var="varHelp"/>
				<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr><td colspan="2" height="1" class="LLine"></td></tr> 
		<tr>					            
			<td class="RightTableCaption" align="right" HEIGHT="24" ><font color="red">*</font> <fmtTagStatus:message key="LBL_TAG_RANGE_FROM"/>:&nbsp;</td>	
			<td><html:text property="tagIdFrom"  size="15" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>												
		</tr>
		<tr><td colspan="2" height="1" class="LLine"></td></tr> 	
		<tr class="shade" >
			<td  class="RightTableCaption" align="right" HEIGHT="24" ><font color="red">*</font> <fmtTagStatus:message key="LBL_TAG_RANGE_TO"/>:&nbsp;</td>
			<td><html:text property="tagIdTo"  size="15" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>						
		</tr>
		<tr><td colspan="2" height="1" class="LLine"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td height="30"  class="RightTableCaption" align="right"><font color="red">*</font> <fmtTagStatus:message key="LBL_STATUS"/>:&nbsp;</td>
			<td  align ="left"><gmjsp:dropdown controlName="typeStatus" SFFormName="frmTagStatus" SFSeletedValue="typeStatus" 
								SFValue="alStatus" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" onChange="javascript:fnChkStatus(this)" />
			</td>
		</tr>
		<tr><td colspan="2" height="1" class="LLine"></td></tr>
		<tr>
			<td height="30"  class="RightTableCaption" align="right">&nbsp; &nbsp;</td>      	 	
			<td align="left" HEIGHT="30" ><fmtTagStatus:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="btn_Submit" value="${varSubmit}"  gmClass="button" onClick="fnSubmit();" buttonType="Save" /> </td>
		</tr>
		<tr><td colspan="2" height="1" class="LLine"></td></tr>
	</table>
		
 </html:form>
</BODY>
<%@ include file="/common/GmFooter.inc"%>
</HTML>

