
<%
	/**********************************************************************************
	 * File		 		: GmPAIncludeLockRpt.jsp
	 * Desc		 		: This screen is used for the displaying Lock vs Tag report
	 * Version	 		: 1.1
	 * author			: Xun
	 * 
	 ************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Iterator"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>

<%@ taglib prefix="fmtPAIncludeLockRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \accounts\physicalaudit\GmPAIncludeLockRpt.jsp -->
<fmtPAIncludeLockRpt:setLocale value="<%=strLocale%>"/>
<fmtPAIncludeLockRpt:setBundle basename="properties.labels.accounts.physicalaudit.GmPAIncludeLockFilter"/>
<bean:define id="returnList" name="frmLockTag" property="returnList" type="java.util.List"></bean:define>
<bean:define id="reconcileList" name="frmLockTag" property="reconcileList" type="java.util.List"></bean:define>
<bean:define id="maStatus" name="frmLockTag" property="maStatus" type="java.lang.String"> </bean:define>
<%
	ArrayList alList = new ArrayList();
	ArrayList alReconList = new ArrayList();
	alList = GmCommonClass.parseNullArrayList((ArrayList) returnList);
	alReconList = GmCommonClass.parseNullArrayList((ArrayList) reconcileList);

	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.accounts.physicalaudit.GmPAIncludeLockFilter", strSessCompanyLocale);
	log.debug("alReconList *** " + alReconList);
		 
	int intRows = alList.size();
	int intReconRows = alReconList.size();
	StringBuffer strValue = new StringBuffer();
	//below code is for reconcile dropdown
	// request.setAttribute("reconcile",reconcileList);
	request.setAttribute("mastatus",maStatus);
	String strDeviationlbl =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_DEVIATION"));
	String strUnitCostlbl =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_UNIT_COST"));
	String strExtendedCostlbl =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_EXTENDED_COST"));		
	HashMap hmCurrency = new HashMap();
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	// PMT-14859 (Locked Qty is Owner company currency - So, remove the currency symbol)
	String strCurrSign = "";
	String strApplCurrFmt = (String)hmCurrency.get("CMPCURRFMT");
	String strCurrFmt = "{0,number,"+strCurrSign+" "+strApplCurrFmt+"}";
	    String strDevTitle = strDeviationlbl+" "+strCurrSign;
	    String strUnitCost = strUnitCostlbl+" "+strCurrSign;
	    String strExtendedCost = strExtendedCostlbl+" "+strCurrSign;
	    
	HashMap hmVal = new HashMap();
	String strCodeID;
	String strSelected;
	String strID;
	strValue.setLength(0);

	strValue.append("<option value= >");
	strValue.append("[Choose One]</option>");

	for (int i = 0; i < intReconRows; i++) {
		hmVal = (HashMap) alReconList.get(i);
		strCodeID = (String) hmVal.get("CODEID");
		strValue.append("<option " + " value= ");
		strValue.append(strCodeID + ">" + hmVal.get("CODENM") + "</option>");
	}
	strValue.append("</select>");

	log.debug("strValue *** " + strValue);

	request.setAttribute("reconcilecombo", strValue.toString());
	
	
	
	
	// declare the variable for Grand total            
		double intBeforeCount = 0;
		double intAfterCount = 0;
		double intDevCount = 0;
		double intDevDollar = 0;
		
		double intfinalAfter = 0;
		double intfinalBefore = 0;
		double intfinalDev = 0;
		double intfinalDevDollar = 0;

	// sum the vlaues
		Iterator itr = alList.iterator();
		while(itr.hasNext()){
			DynaBean db = (DynaBean) itr.next();
			intBeforeCount = Double.parseDouble(db.get("BEFORECOUNT").toString());
			intfinalBefore= intfinalBefore + intBeforeCount;
			intAfterCount = Double.parseDouble(db.get("AFTERCOUNT").toString());
			intfinalAfter = intfinalAfter +intAfterCount;
			intDevCount= Double.parseDouble(db.get("DEVCOUNT").toString());
			intfinalDev = intfinalDev + intDevCount;
			intDevDollar= Double.parseDouble(db.get("DEVDOLLAR").toString());
			intfinalDevDollar = intfinalDevDollar + intDevDollar;
		}
		String strBeforeCount =String.valueOf(intfinalBefore);
		String strAfterCount =String.valueOf(intfinalAfter);
		String strDev =String.valueOf(intfinalDev);
		String strDevDollar =String.valueOf(intfinalDevDollar);
		strAfterCount = ""+ GmCommonClass.getStringWithCommas(strAfterCount,0);
		strDev = ""+ GmCommonClass.getStringWithCommas(strDev,0);
		strBeforeCount = ""+ GmCommonClass.getStringWithCommas(strBeforeCount,0);
		strDevDollar = strCurrSign + GmCommonClass.getStringWithCommas(strDevDollar,2);
		
		

%>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>



<script>


function fnSubmit()
{	
	var varRows = <%=intRows%>;
  //  var varDMId =  document.frmLockTag.physicalAuditID.value; 
	  
	var inputString = ''; 
	var errIDs = '';
	// Validate the mandatory field 
	fnValidateDropDn('physicalAuditID', message_accounts[199]);
	fnValidateDropDn('locationID', message_accounts[293]);
	
	if(!(document.frmLockTag.verifyLocation.checked)){
	for (var i =0; i < varRows; i++) 
	{
		objID = eval("document.frmLockTag.hPnum"+i);
		objRefID = eval("document.frmLockTag.hRefID"+i);
		objDev = eval("document.frmLockTag.hDevcount"+i);
	 	objRecon = eval("document.frmLockTag.setactiontype"+i);
	 	
	 	if ( objRecon != null) 
	 	{  
		 	varRecon = objRecon.value;
			varDev = objDev.value;
			varID = objID.value;
			varRefID = objRefID.value;

			if(eval(varDev) == 0){
				errIDs += varID+',';
			}else if(varRecon!="" )
			{
		    inputString = inputString + varID + '^'+ varRefID + '^' + varDev  +  '^'+ varRecon  + '|';
		 	   }
		 }	   
	}
	var length = inputString.length;
	// alert("input is " + inputString);
 	if(length > 4000)
 	{
 	alert(message_accounts[518]);
 	return false;
 	}
 	else{
	document.frmLockTag.strOpt.value = 'save';
	document.frmLockTag.hinputReconAction.value = inputString;
	 
	 if(errIDs != ''){
				Error_Details(message_accounts[519]);
				Error_Show();
				Error_Clear();
				return false;
			}
		}
	}
	else {
 	document.frmLockTag.strOpt.value = 'save';
	document.frmLockTag.hinputReconAction.value = inputString;
 	}
	fnStartProgress();
 	document.frmLockTag.submit();
}


function fnApplyAll(status)
{
// alert("selectedApplyAll is " + document.frmLockTag.cbo_allAction.selectedIndex);
 var selectedApplyAll = document.frmLockTag.cbo_allAction.selectedIndex;
 var size="<%=intRows%>";
 for (var i=0;i<size ;i++ )
	{
		 
		obj = eval("document.frmLockTag.setactiontype"+i);			
		 
		 if(obj!=null){
		   		obj.options[selectedApplyAll].selected = true;
		   			if(status)
		   			{
		   				obj.disabled = true;
		   			}else
		   			{ 
		   				obj.disabled = false;
		   			}
		 	}

		}
	 
}

function fnVerAll(){
	
	var frm = document.frmLockTag;
	var chk = frm.verifyLocation;
	var reconObj=frm.cbo_allAction;
	if(chk.checked){
			frm.displayOnDeviation.checked = true;
			frm.cbo_allAction.options[1].selected = true;
			fnApplyAll(true);
			reconObj.disabled = true;
	}else if(!chk.checked)
		 {
		    reconObj.disabled = false;
			frm.displayOnDeviation.checked = true;
			frm.cbo_allAction.options[0].selected =true;
			fnApplyAll(false);
	}

}

function fnViewDetails(strID)
 {
    var varauditID = document.frmLockTag.physicalAuditID.value;
    var varlocationID = document.frmLockTag.locationID.value;
    var varprojectID = document.frmLockTag.projectListID.value;
     var vartransactionID = document.frmLockTag.transactionID.value;
 //    alert("varauditID is .." + varauditID + "varlocationID is .."+varlocationID+ "varprojectID.." + varprojectID   +"vartransactionID ..." + vartransactionID);
      windowOpener("/gmIssueTag.do?strOpt=reload&physicalAuditID="+varauditID+"&locationID="+varlocationID+"&projectListID="+varprojectID+"&transactionID="+strID+"&auditUserID=0","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=1120,height=300");
    }
 
</script>
<%
	if (intRows != 0) {
%>

<tr>
	<td class="Line" height="1" colspan="5"></td>
</tr>
<tr>
	<td height="25" align="right" colspan="3" class="RightTableCaption">
	<fmtPAIncludeLockRpt:message key="LBL_VERIFY_WAREHOUSE"/> :&nbsp;<html:checkbox property="verifyLocation" onclick="fnVerAll();"/></td>

	<td colspan="5" align="right" height="25" class="RightTableCaption">
	<fmtPAIncludeLockRpt:message key="LBL_RECONCILED"/>: <select name="cbo_allAction" id="" class="RightText"
		tabindex=0 onChange="fnApplyAll()">
		<option value=><fmtPAIncludeLockRpt:message key="OPT_CHOOSE_ONE"/>
		<option value=50670><fmtPAIncludeLockRpt:message key="OPT_VERIFIED"/></option>
		<option value=50671><fmtPAIncludeLockRpt:message key="OPT_NOT_VERIFIED"/></option>
	</select> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
</tr>
<%
	}
%>
<tr>
	<td class="Line" height="1" colspan="5"></td>
</tr>
<tr>
	<td colspan="5"><display:table	pagesize="500"  name="requestScope.frmLockTag.returnList" class="its" id="currentRowObject" export="true" requestURI="/gmLockTag.do"	varTotals="totals"	decorator="com.globus.accounts.displaytag.beans.DTLockTagWrapper">
        <display:setProperty name="export.excel.filename" value="Lock vs Tag.xls" />    
		<fmtPAIncludeLockRpt:message key="DT_PART" var="varPart"/><display:column property="ID" title="${varPart}" sortable="true" class="alignleft" style="width:60px" />
		<fmtPAIncludeLockRpt:message key="DT_REF_ID" var="varRefID"/><display:column property="REFID" title="${varRefID}"  style="width:100" sortable="true"  class="alignleft"/>
		<fmtPAIncludeLockRpt:message key="LBL_WAREHOUSE" var="varWarehouse"/><display:column property="WAREHOUSE" title="${varWarehouse}"  style="width:100" sortable="true" class="aligncenter" />
		<fmtPAIncludeLockRpt:message key="DT_DESCRIPTION" var="varDescription"/><display:column property="DETAIL" title="${varDescription}" sortable="true"class="alignleft" />
		<fmtPAIncludeLockRpt:message key="DT_BEFORE" var="varBefore"/><display:column property="BEFORECOUNT" title="${varBefore}" sortable="true" class="alignright" total="true"/>
		<fmtPAIncludeLockRpt:message key="DT_AFTER_TAG" var="varAfterTag"/><display:column property="AFTERCOUNT" title="${varAfterTag}" sortable="true"   class="alignright" total="true"/>
		<fmtPAIncludeLockRpt:message key="LBL_DEVIATION_QTY" var="varDeviationQTY"/><display:column property="DEVCOUNT" title="${varDeviationQTY}" sortable="true"   class="alignright" total="true"/>
		<display:column property="DEVDOLLAR" title="<%=strDevTitle%>" sortable="true" class="alignright" format="<%=strCurrFmt%>" total="true" style="width:130px"/>
		<display:column property="UNITCOST" media="excel" title="<%=strUnitCost%>" sortable="true" class="alignright" format="<%=strCurrFmt%>" total="true"/>
		<display:column property="EXTENDEDCOST" media="excel" title="<%=strExtendedCost%>" sortable="true" class="alignright" format="<%=strCurrFmt%>" total="true"/>
		<fmtPAIncludeLockRpt:message key="LBL_RECONCILED" var="varReconciled"/><display:column property="RECONCILEID"   media="html" title="${varReconciled}" class="aligncenter" />
		<fmtPAIncludeLockRpt:message key="LBL_RECONCILED" var="varReconciled"/><display:column property="RECONCILENM"  media="csv excel" title="${varReconciled}" class="aligncenter" />
	
	<display:footer media="html"> 
					<%
					String strVal ;
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column5").toString();
					String strBeforeqty = "" + GmCommonClass.getStringWithCommas(strVal,0);
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column6").toString();
					String strAfterqty = "" + GmCommonClass.getStringWithCommas(strVal,0);
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column7").toString();
					String strDevqty = "" +  GmCommonClass.getStringWithCommas(strVal,0);
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column8").toString();
					String strDevCost = strCurrSign +  GmCommonClass.getStringWithCommas(strVal,2);
					 				
					%>
				<tr class = shade>
				<td colspan="2"> <B> <fmtPAIncludeLockRpt:message key="LBL_TOTAL"/> : </B></td>
				<td class = "alignright" > </td> 
	    	 	<td class = "alignright" > </td> 
		  		<td class = "alignright" ><B><%=strBeforeqty%></B></td>
		  		<td class = "alignright" ><B><%=strAfterqty%></B></td>
		  		<td class = "alignright" ><B><%=strDevqty%></B></td>  		  	 
    	    	 <td class = "alignright" > <B><%=strDevCost%></B></td>
	    	 	<td class = "alignright" > </td>  
	    	 	 
  				</tr>
  			
  				<tr class = shade>
  				<td colspan="2"> <B><fmtPAIncludeLockRpt:message key="LBL_GRAND_TOTAL"/> : </B></td>
  				<td class = "alignright" > </td> 
	    	 	<td class = "alignright" > </td> 
		  		<td class = "alignright" ><B><%=strBeforeCount%></B></td>
		  		<td class = "alignright" ><B><%=strAfterCount%></B></td>
		  		<td class = "alignright" ><B><%=strDev%></B></td>  		  	 
    	    	 <td class = "alignright" > <B><%=strDevDollar%></B></td>
	    	 	<td class = "alignright" > </td>  
	    	 	
  				</tr>


</display:footer >
  			
	</display:table></td>
</tr>

<%
	if (intRows != 0) {
%>
<tr>
	<td colspan="5" align="center" height="40">&nbsp;
	<logic:equal name="frmLockTag" property="strEnableSubmit" value="false">
	 
		<fmtPAIncludeLockRpt:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="verify" value="${varSubmit}" tabindex="35" gmClass="button" disabled="true"
		onClick="return fnSubmit();" buttonType="Save" />&nbsp;
	</logic:equal>
	<logic:equal name="frmLockTag" property="strEnableSubmit" value="true">
	 
		<fmtPAIncludeLockRpt:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="verify" value="${varSubmit}" tabindex="35" gmClass="button"
		onClick="return fnSubmit();" buttonType="Save" />&nbsp;
	</logic:equal>
		</td>
</tr>
<%
	}
%>

