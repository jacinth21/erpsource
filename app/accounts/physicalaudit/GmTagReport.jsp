<%@ page language="java" %>


<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<%@ taglib prefix="fmtTagReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page isELIgnored="false" %>
<!-- \accounts\physicalaudit\GmTagReport.jsp -->
<fmtTagReport:setLocale value="<%=strLocale%>"/>
<fmtTagReport:setBundle basename="properties.labels.accounts.physicalaudit.GmTagReport"/>
 <bean:define id="locationId" name="frmTagReport" property="locationId" type="java.lang.String"> </bean:define>
 <bean:define id="hmNames" name="frmTagReport" property="hmNames" type="java.util.HashMap"> </bean:define>
 <bean:define id="alPlantList" name="frmTagReport" property="alPlantList" type="java.util.ArrayList"> </bean:define>
 <bean:define id="palntNm" name="frmTagReport" property="palntNm" type="java.lang.String"> </bean:define>
 <bean:define id="companyNm" name="frmTagReport" property="companyNm" type="java.lang.String"> </bean:define>
 <bean:define id="alStatus" name="frmTagReport" property="alStatus" type="java.util.ArrayList"></bean:define>
 <bean:define id="typeStatus" name="frmTagReport" property="typeStatus" type="java.lang.String"> </bean:define>
<%
GmServlet gm = new GmServlet();
gm.checkSession(response,session);

	ArrayList alDistributor = new ArrayList();
	ArrayList alInhouseList = new ArrayList();
	String strDepartMentID	= GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
	String strApplDateFmt = strGCompDateFmt;
	String strDateFmt = "{0,date,"+strApplDateFmt+"}";
	
	String strClass ="";
	String strColSpan = "3";
	HashMap hcboVal = null;
	String strFunctionName ="fnGetNames()";
	
	if (strDepartMentID.equals("S")) {
		strFunctionName = "";
	}
	
	if (hmNames != null)
	{
		alDistributor = (ArrayList)hmNames.get("DISTLIST");
		alInhouseList = (ArrayList)hmNames.get("INHOUSELIST");
	}
	String strWikiTitle = GmCommonClass.getWikiTitle("TAG_REPORT");
	HashMap hmSystemMap = new HashMap();
	hmSystemMap.put("ID","CODEID");
	hmSystemMap.put("PID","CODEID");
	hmSystemMap.put("NM","CODENM");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Tag History</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     
     table.DtTable1300 {
	margin: 0 0 0 0;
	width: 1350px;
	border: 1px solid  #676767;
}
     
     
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/TagReport.js"></script>


<script>

var DistLen = <%=alDistributor.size()%>;
var EmpLen = <%=alInhouseList.size()%>;
var PlantLength = <%=alPlantList.size()%>;
var defaultCompany = '<%=companyNm%>';
var defaultPlant = '<%=palntNm%>';

<% hcboVal = new HashMap();
for (int i=0;i<alPlantList.size();i++){
	hcboVal = (HashMap)alPlantList.get(i); %>
var AllPlantArr<%=i%> ="<%=hcboVal.get("PLANTID")%>,<%=hcboVal.get("PLANTNM")%>";
<% } %>

<%
	hcboVal = new HashMap();
	
	for (int i=0;i<alDistributor.size();i++)
	{
		hcboVal = (HashMap)alDistributor.get(i);
%>
	var DistArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>

<%
	hcboVal = new HashMap();
	for (int i=0;i<alInhouseList.size();i++)
	{
		hcboVal = (HashMap)alInhouseList.get(i);
%>
	var EmpArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<%
	}
%>

</script>
</HEAD>

<BODY onkeyup="fnEnter();" onload="<%=strFunctionName%>;fnOnPageLoad();" leftmargin="20" topmargin="10" >
 
<html:form action="/gmTagReport.do" >
<html:hidden property="strOpt" />
<html:hidden property="locationId" value="<%=locationId%>"/>
<input type="hidden" name="hTxnId">
<input type="hidden" name="hCancelType">
<input type="hidden" name="hAction" >
<input type="hidden" name="hRedirectURL" >
<input type="hidden" name="hDisplayNm" >
<input type="hidden" name="hStatusGrp" value="<%=typeStatus%>">
	<table border="0" class="DtTable1300"  cellspacing="0" cellpadding="0">
		<tr><td><table cellspacing="0" cellpadding="0" width="100%">
		
		<tr>
			<td height="25" class="RightDashBoardHeader">
				<fmtTagReport:message key="LBL_TAG_REPORT"/>
			</td>
			<td colspan="1" height="25" class="RightDashBoardHeader">
			<fmtTagReport:message key="IMG_HELP" var="varHelp"/>
			<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		</table></td></tr>
		<tr><td colspan="8" class="Line" height="1"></td></tr>
		<tr><td><table border="0"  cellspacing="0" cellpadding="0" width="100%">
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr  class="shade" >
		<td height="30" class="RightTableCaption" align="right" width="10%">&nbsp;<fmtTagReport:message key="LBL_SET_ID"/>:</td>
		<td width="10%">&nbsp;<html:text property="setID"  size="15" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>
		<td height="30" class="RightTableCaption" align="right" width="10%">&nbsp;<fmtTagReport:message key="LBL_PART_NUMBER"/>:</td>
		<td align ="left" width="18%">&nbsp;<html:text property="partNum"  size="15" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>
		<td height="30" width="10%" class="RightTableCaption" align="right">&nbsp; <fmtTagReport:message key="LBL_COMPANY"/>:</td>
		<td align ="left" width="15%">&nbsp;<gmjsp:dropdown controlName="companyNm" SFFormName="frmTagReport" SFSeletedValue="companyNm" 
							SFValue="alCompanyList" codeId = "COMPANYID"  codeName = "COMPANYNM" onChange="fnGetPlantList(this);"  defaultValue= "[Choose One]"  /></td>
		<td height="30" width="10%" class="RightTableCaption" align="right">&nbsp;<fmtTagReport:message key="LBL_STATUS"/>:&nbsp;</td>
		<td align ="left" width="17%">&nbsp;<%=GmCommonControls.getChkBoxGroup("",alStatus,"Status",hmSystemMap)%>&nbsp;</td>		
		</tr>
		<tr><td bgcolor="#CCCCCC" height="1" colspan="8"></td></tr>
		<% 
			if (!strDepartMentID.equals("S")) {
				strClass = "shade";
		%>
		<tr>
		
		<td height="30"  class="RightTableCaption" align="right">&nbsp;<fmtTagReport:message key="LBL_LOCATION_TYPE"/>:</td>
		<td align ="left" >&nbsp;<gmjsp:dropdown controlName="locationType" SFFormName="frmTagReport" SFSeletedValue="locationType" 
							SFValue="alLocationType" codeId = "CODENMALT"  codeName = "CODENM"  defaultValue= "[Choose One]"  onChange="javascript:fnGetNames(this)"/></td>
		<td class="RightTableCaption" HEIGHT="23" align="right"><fmtTagReport:message key="LBL_LOCATION_NAMES"/>:</td>
			<td >&nbsp;<gmjsp:dropdown controlName="name"  SFFormName='frmTagReport' SFSeletedValue="name"  defaultValue= "[Choose One]" 
						width="200" SFValue="alLocationNames" codeId="ID" codeName="NAME" />      
		    </td>
		    <td height="30" class="RightTableCaption" align="right">&nbsp;<fmtTagReport:message key="LBL_PLANT"/> :</td>
 		<td align ="left">&nbsp;<gmjsp:dropdown controlName="palntNm" SFFormName="frmTagReport"  SFSeletedValue="palntNm" 
				SFValue="alPlantList" codeId = "PLANTID"  codeName = "PLANTNM"  defaultValue= "[Choose One]" />
		</td>
		    <td height="30" class="RightTableCaption" align="right">&nbsp; <fmtTagReport:message key="LBL_INVENTORY_TYPE"/>:</td>
 		<td align ="left">&nbsp;<gmjsp:dropdown controlName="inventoryType" SFFormName="frmTagReport"  SFSeletedValue="inventoryType" 
				SFValue="alInventoryType" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
		</td>
		    
		</tr>
		<tr><td bgcolor="#CCCCCC" height="1" colspan="8"></td></tr>
		<% } else {
			strClass = "";
			strColSpan = "0";
		%>
		    <input type ="hidden" name="locationType" value="0" />
		    <input type ="hidden" name="name" value="0" />
		    <input type ="hidden" name="inventoryType" value="0" />		     
		<% } %>
		<tr  class="<%=strClass%>">
		<td height="30"  class="RightTableCaption" align="right">&nbsp;<fmtTagReport:message key="LBL_TAG_RANGE_FROM"/>:</td>
		<td>&nbsp;<html:text property="tagIdFrom"  size="15" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>
		<td class="RightTableCaption" align="right"> <fmtTagReport:message key="LBL_TO"/>:</td>
		<td colspan="<%=strColSpan%>" width="30%">&nbsp;<html:text property="tagIdTo"  size="15" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/> (Leave blank to search for only one Tag)
		</td>		
		<% 
			if (strDepartMentID.equals("S")) {
				strClass = "shade";
		%>		
		<td height="30" class="RightTableCaption" align="right">&nbsp; <fmtTagReport:message key="LBL_PLANT"/>:</td>
 		<td align ="left">&nbsp;<gmjsp:dropdown controlName="palntNm" SFFormName="frmTagReport"  SFSeletedValue="palntNm" 
				SFValue="alPlantList" codeId = "PLANTID"  codeName = "PLANTNM"  defaultValue= "[Choose One]" />
		</td>
		<%}else{ %>
		
		<%} %>
		<td height="30"  class="RightTableCaption" align="right"><fmtTagReport:message key="LBL_VOIDED_TAGS"/>:</td>
		<td><html:checkbox property="voidedTags"/>&nbsp;&nbsp;<fmtTagReport:message key="BTN_LOAD" var="varLoad"/>
		<gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnGo();" buttonType="Load" />&nbsp;	
		</td>
		</tr>
		<tr><td bgcolor="#CCCCCC" height="1" colspan="8"></td></tr>
		</table></td></tr>
		<tr>		
		<td colspan="12" width="100%">
				<display:table name="requestScope.frmTagReport.rdReport.rows" export="true" freezeHeader="true" class="its" requestURI="/gmTagReport.do" id="currentRowObject" decorator="com.globus.accounts.displaytag.beans.DTAccTagReportWrapper" >
				<display:setProperty name="export.excel.filename" value="TagReport.xls" />
				<fmtTagReport:message key="DT_TAG" var="varTag"/><display:column property="TAGNUM" sortable="true" title="${varTag}" style="width:130;" class="alignleft"/>
 				<fmtTagReport:message key="DT_PART" var="varPart"/><display:column property="PARTNUM" title="${varPart}" class="alignleft" />
 				<fmtTagReport:message key="DT_PART_DESCRIPTION" var="varPartDescription"/><display:column property="PARTDESC" escapeXml="true" title="${varPartDescription}" class="alignleft" />
 				<fmtTagReport:message key="DT_CONTROL" var="varControl"/><display:column property="CONTROLNUM" title="${varControl}" class="alignleft" />
				<fmtTagReport:message key="DT_REF_ID" var="varRefID"/><display:column property="REFID" title="${varRefID}"  style="width:100;" class="alignleft" />
				<fmtTagReport:message key="LBL_SET_ID" var="varSetId"/><display:column property="SETID" title="${varSetId}" class="alignleft" />
				<fmtTagReport:message key="LBL_COMPANY" var="varCompany"/><display:column property="COMPANYID" title="${varCompany}" class="alignleft" />
				<fmtTagReport:message key="LBL_PLANT" var="varPlant"/><display:column property="PLANTID" title="${varPlant}" class="alignleft" />
				<fmtTagReport:message key="DT_LOCATION" var="varLocation"/><display:column property="LOCATION" title="${varLocation}" class="alignleft" />
				<fmtTagReport:message key="LBL_STATUS" var="varStatus"/><display:column property="STATUS" sortable="true" title="${varStatus}" class="alignleft" />
			    <fmtTagReport:message key="DT_Missing" var="varMissing"/><display:column property="MISSINGSINCE" sortable="true" title="${varMissing}" format= "<%=strDateFmt%>" class="alignleft" />
				<% 
					if (!strDepartMentID.equals("S")) {
				%>
				<fmtTagReport:message key="DT_LAST_UPDATED_BY" var="varLastUpdatedBy"/><display:column property="TAGGEDBY" title="${varLastUpdatedBy}" class="alignleft" />
				<fmtTagReport:message key="DT_LAST_UPDATED_DATE" var="varLastUpdatedDate"/><display:column property="CDATE" title="${varLastUpdatedDate}" style="width:100;" format= "<%=strDateFmt%>" class="alignleft" />
				<fmtTagReport:message key="DT_VOID_FLAG" var="varVoidFlag"/><display:column property="VOIDFL" title="${varVoidFlag}" style="width:20;" class="alignleft" />
				<%
					}
				%>
				
				<fmtTagReport:message key="DT_SUB_LOCATION" var="varSubLocation"/><display:column property="SUBLOCATION" title="${varSubLocation}" class="alignleft" />
				<% 
					if (!strDepartMentID.equals("S")) {
				%>
				<fmtTagReport:message key="DT_SUB_LOCATION_TYPE" var="varSubLocationType"/><display:column property="SUBLOCATION_TYPE" title="${varSubLocationType}" class="alignleft" />
				<%
					}
				%> 
				<fmtTagReport:message key="DT_ACCOUNT" var="varAccount"/><display:column property="ACCOUNT" title="${varAccount}" class="alignleft" /> 
				<% 
					if (!strDepartMentID.equals("S")) {
				%>
				<fmtTagReport:message key="DT_INVENTORY_TYPE" var="varInventoryType"/><display:column property="INVENTORYTYPE" title="${varInventoryType}" class="alignleft" />
				<%
					}
				%>
				</display:table>
			</td>		
		</tr>		
		</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>