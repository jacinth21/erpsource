
<%
	/**********************************************************************************
	 * File		 		: GmPACreateMA.jsp
	 * Desc		 		: This screen is used for the displaying Missing Flagged Part and create MA
	 * Version	 		: 1.1
	 * author			: Xun
	 * 
	 ************************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ taglib prefix="fmtPACreateMA" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \accounts\physicalaudit\GmPACreateMA.jsp -->
<fmtPACreateMA:setLocale value="<%=strLocale%>"/>
<fmtPACreateMA:setBundle basename="properties.labels.accounts.physicalaudit.GmGenerateHTMLTags"/>

<bean:define id="returnList" name="frmLockTag" property="returnList" type="java.util.List"></bean:define>
<bean:define id="maStatus" name="frmLockTag" property="maStatus" type="java.lang.String"> </bean:define>
<bean:define id="maIDList" name="frmLockTag" property="maIDList" type="java.util.ArrayList"></bean:define>
<bean:define id="location" name="frmLockTag" property="locationID" type="java.lang.String"> </bean:define>
<%
	ArrayList alList = new ArrayList();
	ArrayList almaIDList = new ArrayList();
	alList = GmCommonClass.parseNullArrayList((ArrayList) returnList);
	almaIDList = GmCommonClass.parseNullArrayList((ArrayList) maIDList);
	int rowsize = alList.size();
	int rowmasize = almaIDList.size();
	String strWikiTitle = GmCommonClass.getWikiTitle("VIEW_MISSING_FLAGGED_PART_GENERATE_MA");
	
	HashMap hmCurrency = new HashMap();
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	// PMT-14859 (Locked Qty is Owner company currency - So, remove the currency symbol)
	String strCurrSign = "";
	String strApplCurrFmt = (String)hmCurrency.get("CMPCURRFMT");
	String strCurrFmt = "{0,number,"+strCurrSign+" "+strApplCurrFmt+"}";
	
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();
	
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
	
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Create MA</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"
	media="print" />

<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<style type="text/css" media="all">
@import url("styles/screen.css");
</style>
<script>
function fnReload()
{  
	fnValidateDropDn('physicalAuditID', message_accounts[199]);
    fnValidateDropDn('locationID',message_accounts[293]);
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	} 
	document.frmLockTag.strOpt.value = "reload";
	fnStartProgress();
	document.frmLockTag.submit();  
  
}

 
  
function fnSubmit()
{
	if(document.frmLockTag.supervisiorID.value =='0'){
		Error_Details(message_accounts[505]);
	}
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	var agree=confirm(message_accounts[297]); 
if (agree) {
	fnStartProgress();
 	document.frmLockTag.strOpt.value = 'save';
	document.frmLockTag.submit();
	}
	else 
    return false ; 
}

 function fnMAdetails(strMAId)
 { 
      windowOpener("/gmMADetail.do?method=reportMADetail&strOpt=report&maID="+strMAId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=820,height=400");
    }
    
    
 function fetchCountedBy(){

	document.frmLockTag.submit();
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmPAManualAdjustment.do">
	<html:hidden property="strOpt" value="" />

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4"><fmtPACreateMA:message key="LBL_MISSING_FLAGGED_PART_GENERATE_MA"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtPACreateMA:message key="IMG_HELP" var="varHelp"/>
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		<tr>
			<td bgcolor="#666666" height="1" colspan="5"></td>
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td height="35" class="RightTableCaption" align="right">&nbsp;&nbsp;&nbsp;&nbsp;<fmtPACreateMA:message key="LBL_AUDIT_LIST"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="physicalAuditID" SFFormName="frmLockTag" SFSeletedValue="physicalAuditID"
				SFValue="physicalAuditList" codeId="CODEID" codeName="CODENM"  onChange="fetchCountedBy();" defaultValue= "[Choose One]"/></td>
			<td height="35" class="RightTableCaption" align="right"><fmtPACreateMA:message key="LBL_WAREHOUSE"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="locationID" SFFormName="frmLockTag" SFSeletedValue="locationID"
				SFValue="locationList" codeId="CODEID" codeName="CODENM"
				defaultValue="[Choose One]" /></td>

			<TD width=100 align="center"><fmtPACreateMA:message key="BTN_GO" var="varGo"/><gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" onClick="javascript:fnReload();"
				tabindex="3" gmClass="button" buttonType="Load"/></TD>
		</tr>
		<%
			if (rowsize > 0) {
		%>
		<tr>
			<td class="Line" height="1" colspan="5"></td>
		</tr>
		<tr>
			<td colspan="5"><display:table	name="requestScope.frmLockTag.returnList" class="its" id="currentRowObject"  export= "true" varTotals="totals" requestURI="/gmPAManualAdjustment.do">
                <display:setProperty name="export.excel.filename" value="Create MA.xls" />
				<fmtPACreateMA:message key="DT_PART_NUMBER" var="varPartNumber"/><display:column property="PNUM" title="${varPartNumber}" sortable="true"	class="alignleft" />
				<fmtPACreateMA:message key="DT_DESCRIPTION" var="varDescription"/><display:column property="PDESC" title="${varDescription}" class="alignleft" />

				<fmtPACreateMA:message key="DT_QTY_TO_ADJUST" var="varQtytoAdjust"/><display:column property="QTY" title="${varQtytoAdjust}" sortable="true"	class="alignright" total="true"/>
				<fmtPACreateMA:message key="DT_COST" var="varCost"/><display:column property="PRICE" title="${varCost}" sortable="true" class="alignright" />
				<fmtPACreateMA:message key="DT_EXT_COST" var="varExtCost"/><display:column property="EXTEND_COST" title="${varExtCost}" sortable="true" class="alignright" format="<%=strCurrFmt%>" total="true"/>
			
			<display:footer media="html"> 
					<%
					String strVal ;
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column3").toString();
					String strAdjqty = "" + GmCommonClass.getStringWithCommas(strVal,0);
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column5").toString();
					String strExtCost = strCurrSign +  GmCommonClass.getStringWithCommas(strVal,2);
					 				
					%>
				<tr class = shade>
		  		<td colspan="2"> <B> <fmtPACreateMA:message key="LBL_TOTAL"/> : </B></td>
		  		<td class = "alignright" ><B><%=strAdjqty%></B></td>
  		  		<td class = "alignright">  </td>
    	    	 <td class = "alignright" > <B><%=strExtCost%></B></td>
	    	 	 
  				</tr>
  			</display:footer>
			</display:table></td>
		</tr>

		<tr>
			<td class="Line" height="1" colspan="5"></td>
		</tr>
		 
		<tr>
			<td colspan="5" align="center">&nbsp;&nbsp; <p>&nbsp;</p> 
		<!-- Removed from below condition location.equals("20455")  because for 2011 Quarantine Alloc(Inv) and Quar Alloc(Scrap) has been combined to Quarantine -->	
		<% if (location.equals("20458")||location.equals("20465")||location.equals("20461") ||location.equals("20468")  ){ %>  	
		<p>&nbsp;<fmtPACreateMA:message key="LBL_NO_POSTING_FOR_THIS_LOCATION"/></p> 
		<%} else { %>
	   &nbsp;<font color="red">*</font>&nbsp;<b><fmtPACreateMA:message key="LBL_SUPERVISOR"/>:</b>&nbsp;<gmjsp:dropdown controlName="supervisiorID" SFFormName="frmLockTag" SFSeletedValue="supervisiorID"
				SFValue="supervisiorList" codeId="CODEID" codeName="CODENM"
				defaultValue="[Choose One]" />
		&nbsp;&nbsp;&nbsp;
		<logic:equal name="frmLockTag" property="strEnableSubmit" value="false">
		<% if (maStatus.equals("Y")){ %>  
		                    <fmtPACreateMA:message key="BTN_CREATE_MA" var="varCreateMA"/><gmjsp:button value="${varCreateMA}" gmClass="button"  disabled="true" buttonType="Save" onClick="fnSubmit();" />
		                  <%} else {%> 
                         <fmtPACreateMA:message key="BTN_CREATE_MA" var="varCreateMA"/><gmjsp:button value="${varCreateMA}" gmClass="button" disabled="true" buttonType="Save" onClick="fnSubmit();" />
                        <%} %>
        </logic:equal>	 
        <logic:equal name="frmLockTag" property="strEnableSubmit" value="true">
		<% if (maStatus.equals("Y")){ %>  
		                   <fmtPACreateMA:message key="BTN_CREATE_MA" var="varCreateMA"/><gmjsp:button value="${varCreateMA}" gmClass="button"  disabled="true" buttonType="Save" onClick="fnSubmit();" />
		                  <%} else {%> 
                        <fmtPACreateMA:message key="BTN_CREATE_MA" var="varCreateMA"/> <gmjsp:button value="${varCreateMA}" gmClass="button" buttonType="Save" onClick="fnSubmit();" />
                        <%} %>
         </logic:equal>
            <%} %>             
         </td>
		</tr>               
		 <%
			if (rowmasize > 0) {
		%>
		<tr>
			<td colspan="5"><display:table	name="requestScope.frmLockTag.maIDList" class="its" id="currentRowObject" requestURI="/gmPAManualAdjustment.do?companyInfo=<%=strCompanyInfo%>" decorator="com.globus.accounts.displaytag.beans.DTMAIDsWrapper">
				<fmtPACreateMA:message key="BTN_PARTS_ALREADY_CREATED" var="varPartsAlreadyCreated"/><display:column property="MAID" title="${varPartsAlreadyCreated}"  class="aligncenter" />
			 
			</display:table></td>
		</tr>

		<%
			}
		 
			}
		%>
	</table>
	</FORM>
	<%@ include file="/common/GmFooter.inc"%>

</html:form>
</BODY>

</HTML>
