<%@ page language="java" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtTagHistoryReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \accounts\physicalaudit\GmTagHistoryReport.jsp -->
<fmtTagHistoryReport:setLocale value="<%=strLocale%>"/>
<fmtTagHistoryReport:setBundle basename="properties.labels.accounts.physicalaudit.GmTagHistoryReport"/>

 
<%
GmServlet gm = new GmServlet();
gm.checkSession(response,session);
String strApplDateFmt = strGCompDateFmt;
strApplDateFmt = strApplDateFmt + " K:m:s ";
String strRptFmt = "{0,date,"+strApplDateFmt+"}";
String strDtFmt = "{0,date,"+strGCompDateFmt+"}";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Tag History</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script>

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" >
 
<html:form action="/gmTagHistory.do"  >

	<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="12" height="25" class="RightDashBoardHeader">
				<fmtTagHistoryReport:message key="LBL_TAG_REVISION_HISTORY_DETAILS"/>
				
			</td>
		</tr>
		<tr><td colspan="12" class="Line" height="1"></td></tr>		
	    <tr>
			<td colspan="12" width="100%">
				<display:table name="requestScope.frmTag.rdHistory.rows" export="false" class="its" requestURI="/gmTagHistory.do" id="currentRowObject" >
 				<fmtTagHistoryReport:message key="DT_TAG" var="varTag"/><display:column property="TAGNUM" title="${varTag}" class="alignleft"/>
 				<fmtTagHistoryReport:message key="DT_PART" var="varPart"/><display:column property="PARTNUM" title="${varPart}" class="alignleft" />
 				<fmtTagHistoryReport:message key="DT_CONTROL" var="varControl"/><display:column property="CONTROLNUM" title="${varControl}" class="alignleft" />
				<fmtTagHistoryReport:message key="DT_REF_ID" var="varRefID"/><display:column property="REFID" title="${varRefID}" class="alignleft" />
				<fmtTagHistoryReport:message key="DT_SET_ID" var="varSetID"/><display:column property="SETID" title="${varSetID}" class="alignleft" />
				<fmtTagHistoryReport:message key="DT_LOCATION" var="varLocation"/><display:column property="LOCATION" title="${varLocation}" class="alignleft" />
				<fmtTagHistoryReport:message key="DT_CREATED_BY" var="varCreatedBy"/><display:column property="CREATEDBY" title="${varCreatedBy}" class="alignleft" />
				<fmtTagHistoryReport:message key="DT_DATE_TIME" var="varDateTime"/><display:column property="DATETIME" title="${varDateTime}" class="alignleft" format= "<%=strRptFmt%>" />
				<fmtTagHistoryReport:message key="DT_STATUS" var="varStatus"/><display:column property="STATUS" title="${varStatus}" class="alignleft" />
				<fmtTagHistoryReport:message key="DT_MISSING_SINCE" var="varMissingSince"/><display:column property="MISSINGSINCE" format= "<%=strDtFmt%>" title="${varMissingSince}" class="alignleft" />
				<fmtTagHistoryReport:message key="DT_LOCK_FLAG" var="varLockFlag"/><display:column property="LOCKFL" title="${varLockFlag}" class="alignleft" />
				<fmtTagHistoryReport:message key="DT_COMPANY" var="varCompany"/><display:column property="COMPANYCD" title="${varCompany}" style="width:100;" class="alignleft" />
				<fmtTagHistoryReport:message key="DT_PLANT" var="varPlant"/><display:column property="PLANTID" title="${varPlant}" style="width:100;" class="alignleft" />
				<fmtTagHistoryReport:message key="DT_SUB_LOCATION" var="varSubLocation"/><display:column property="SUBLOCATION" title="${varSubLocation}" class="alignleft" />
				<fmtTagHistoryReport:message key="DT_SUB_LOCATION_TYPE" var="varSubLocationType"/><display:column property="SUBLOCATION_TYPE" title="${varSubLocationType}" class="alignleft" />
				<fmtTagHistoryReport:message key="DT_ACCOUNT" var="varAccount"/><display:column property="ACCOUNT" title="${varAccount}" class="alignleft" />
				<fmtTagHistoryReport:message key="DT_COMMENT" var="varComment"/><display:column property="COMMENTS" title="${varComment}" class="alignleft" />
				<fmtTagHistoryReport:message key="DT_RETAG_ID" var="varRetagID"/><display:column property="RETAGID" title="${varRetagID}" class="alignleft" />
				</display:table>
			</td>
		</tr> 
	</table>
	<table border="0" cellspacing="0" cellpadding="0">
	<tr><td colspan="12"></td></tr>
	<tr>
	<td align="center" height="30" colspan="12"><fmtTagHistoryReport:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="&nbsp;${varClose}&nbsp;" gmClass="button" onClick="window.close();" buttonType="Load"/></td>
	</tr>
	</table>
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>