 <%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.text.*" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtPATagControl" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \accounts\physicalaudit\GmPATagControl.jsp -->
<fmtPATagControl:setLocale value="<%=strLocale%>"/>
<fmtPATagControl:setBundle basename="properties.labels.accounts.physicalaudit.GmPATag"/>

<%
String strWikiTitle = GmCommonClass.getWikiTitle("TAG_CONTROL");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Tag Control</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script>
function fnChangeFocus(frmElement)
{
   var obj = eval("document.frmLockTag."+frmElement);
   var focusedElement = document.activeElement.value;
	if(event.keyCode == 13 )
	 {	
		
		if (frmElement == 'CountedBy' && focusedElement != 0){
			
			fnSave();
		}else{
			obj.focus();
			 }
	  }
}
	   
function chkValue(value){

   if (isNaN(value))
    {   	
    	Error_Details(message[28]);   	
    }
}

function fnSave()
{
	var fromtagvalue = document.frmLockTag.tagRangeFrom.value;
	var totagvalue = document.frmLockTag.tagRangeTo.value;
	
	fnValidateTxtFld('tagRangeFrom',message_accounts[287]);
	fnValidateTxtFld('tagRangeTo',message_accounts[288]);
	fnValidateDropDn('auditUserID',message_accounts[289]);
	chkValue(fromtagvalue);
	chkValue(totagvalue);	
	
	if(fromtagvalue > totagvalue)
	{
	  Error_Details(message_accounts[506]);
	}
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	
	document.frmLockTag.strOpt.value='Save';
	fnStartProgress();
	document.frmLockTag.submit();
}

function fetchCountedBy(){
//chk if value is not 0 then submit
	document.frmLockTag.strOpt.value='Load';
	document.frmLockTag.submit();
}
function fnLoad()
{
	if (document.frmLockTag.tagRangeFrom.value == "")
	   {
	    document.frmLockTag.tagRangeFrom.focus();
	    
	   }
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad();" onkeypress="javascript:fnChangeFocus('CountedBy');" >
<html:form action="/gmPATagControl.do" >

<input type="hidden" name="strOpt" >

<table border="0" width="700" cellspacing="0" cellpadding="0"  >
		<tr>
			<td rowspan="11" width="1" class="Line"></td>
			<td  height="1" bgcolor="#666666" colspan="2"></td>
			<td rowspan="11" width="1" class="Line"></td>
		</tr>
		<tr>
			<td  height="25" class="RightDashBoardHeader"  > 
<fmtPATagControl:message key="LBL_TAG_CONTROL"/> </td>
			<td  height="25" class="RightDashBoardHeader" align="right"><fmtPATagControl:message key="IMG_HELP" var="varHelp"/>
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		<tr><td class="LLine" height="1" colspan="2"></td></tr>
		<tr>
			<td  class="RightTableCaption" align="right" HEIGHT="24" >&nbsp; <fmtPATagControl:message key="LBL_PHYSICAL_AUDIT"/>:</td>
			<td   class="RightText" >
			&nbsp; 
						<bean:write name="frmLockTag" property="physicalAuditNM"/>
			</td>
		</tr>
		<tr><td class="LLine" height="1" colspan="2"></td></tr>
		<tr class="Shade">
			<td class="RightTableCaption" align="right" HEIGHT="24" >&nbsp;<font color="red">*</font><fmtPATagControl:message key="LBL_TAG_RANGE"/>: </td>
			<td class="RightText" >&nbsp;&nbsp;<fmtPATagControl:message key="LBL_FROM"/>:
			<html:text property="tagRangeFrom" size="10"  tabindex="1" styleClass="InputArea"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" onkeypress="javascript:fnChangeFocus('tagRangeTo');" />							
			&nbsp;&nbsp;&nbsp;<fmtPATagControl:message key="LBL_TO"/>:
			<html:text property="tagRangeTo" size="10"  tabindex="2" styleClass="InputArea"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" onkeypress="javascript:fnChangeFocus('auditUserID');" />							
			</td>
		</tr>
		<tr><td class="LLine" height="1" colspan="2"></td></tr>		
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td class="RightTableCaption" align="right" HEIGHT="24" width="200">&nbsp;<font color="red">*</font><fmtPATagControl:message key="DT_COUNTED_BY"/>:</td>
			<td class="RightText"> 
			&nbsp;<gmjsp:dropdown controlName="auditUserID" tabIndex="3" SFFormName='frmLockTag' SFSeletedValue="auditUserID"  defaultValue= "[Choose One]"	
						SFValue="auditUserList" codeId="CODEID" codeName="CODENM"/>
			</td>
		</tr>	
		<tr><td class="LLine" height="1" colspan="2"></td></tr>		
		<tr>
			<td colspan="2" align="center">
			<logic:equal name="frmLockTag" property="strEnableSubmit"  value="false">
			<fmtPATagControl:message key="BTN_SAVE" var="varSave"/><gmjsp:button value="&nbsp;&nbsp;${varSave}&nbsp;&nbsp;" disabled="true" name="Btn_Go" tabindex="4" gmClass="button" buttonType="Save" onClick="javascript:fnSave()" />
			</logic:equal>
			<logic:equal name="frmLockTag" property="strEnableSubmit"  value="true">
			<fmtPATagControl:message key="BTN_SAVE" var="varSave"/><gmjsp:button value="&nbsp;&nbsp;${varSave}&nbsp;&nbsp;" name="Btn_Go" gmClass="button" tabindex="4" buttonType="Save" onClick="javascript:fnSave()" />
			</logic:equal>
			</td>
		</tr>
		<tr><td class="Line" colspan="2"></td></tr>		
    </table>		     	
		
		
</html:form>
   <%@ include file="/common/GmFooter.inc" %>
</BODY>


</html>