 <%
/**********************************************************************************
 * File		 		: GmPAGenerateTags.jsp
 * Desc		 		: This screen is used for Generating the Tags
 * author			: VPrasath
 ************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap, java.util.Calendar" %> 
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import = "com.globus.accounts.physicalaudit.forms.GmLockTagForm" %>
<%@ taglib prefix="fmtPAGenerateTags" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \accounts\physicalaudit\GmPAGenerateTags.jsp -->
<fmtPAGenerateTags:setLocale value="<%=strLocale%>"/>
<fmtPAGenerateTags:setBundle basename="properties.labels.accounts.physicalaudit.GmGenerateHTMLTags"/>
 
 <bean:define id="returnList" name="frmLockTag" property="returnList" type="java.util.ArrayList"></bean:define>
 <bean:define id="missingPart" name="frmLockTag" property="missingPart"  type="java.util.ArrayList"> </bean:define>
<%
String strWikiTitle = GmCommonClass.getWikiTitle("MANUAL_GENERATE_TAGS");
int intExcludeParts = missingPart.size();
String strPartNumber = "";
String strWarehouse = "";
String strShade = "";

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Manual Generate Tags</TITLE>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>	

<script> 

function fnSubmit()
{ 
	var chk_length =  document.frmLockTag.chk_transactionID.length;
	var inputString = "";
	var location = "";
	var countedby ="";
	var type=document.frmLockTag.actionType.value;
	if(type=="0")
	{
		Error_Details(message_accounts[501]);
	}
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	
	if (chk_length == undefined)
	{
	   if(document.frmLockTag.chk_transactionID.checked)
	     {
	      if(document.frmLockTag.txt_tagCount.value == "")
	       {
	         Error_Details(message_accounts[507]);
	       }
	            location = document.frmLockTag.cbo_Location.value;
		        location = location == '0' ? '' : location;		       
			    countedby = document.frmLockTag.cbo_countedBy.value;
				countedby = countedby == '0' ? '' : countedby;
	            inputString = document.frmLockTag.chk_transactionID.id+ "^" +document.frmLockTag.txt_refid.value + "^" +location +"^"+ countedby + "^" + document.frmLockTag.txt_tagCount.value  + "|";
		 }
		else
		{ 
		   Error_Details(message_accounts[508]);
		}	 	
	}
	else
	{
	  for (i = 0; i < chk_length; i++) 
	  {			
			if (document.frmLockTag.chk_transactionID[i].checked ) 
			{
			if(document.frmLockTag.txt_tagCount[i].value == "")
		       {
		         Error_Details(message_accounts[507]);
		         break;
		       }		       
		       	location = document.frmLockTag.cbo_Location[i].value;
		        location = location == '0' ? '' : location;		 
				countedby = document.frmLockTag.cbo_countedBy[i].value;
				countedby = countedby == '0' ? '' : countedby;
			    inputString += document.frmLockTag.chk_transactionID[i].id + "^" +document.frmLockTag.txt_refid[i].value + "^" +location+"^"+ countedby + "^" + document.frmLockTag.txt_tagCount[i].value + "|";			  
			}
	   }	   
	   if(inputString == "")
	    {
	      Error_Details(message_accounts[508]);
	    }	   
	}
	
	   if(inputString.length >= 4000 )
	    {
	       Error_Details(message_accounts[510]);
	    }
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}		
	if(document.frmLockTag.locationID.value == '0')
	 { 
	  document.frmLockTag.locationID.value ="";	  
	 }

	document.frmLockTag.hinputString.value = inputString;
	document.frmLockTag.strOpt.value = type;	
	document.frmLockTag.submit();
}

function fnApplyAll()
{ 
	var chk_length =  document.frmLockTag.chk_transactionID.length;
	var inputString = "";
	
	if (chk_length == undefined)
	{
			document.frmLockTag.chk_transactionID.checked = document.frmLockTag.selectAll.checked;
	    	document.frmLockTag.cbo_countedBy.value = document.frmLockTag.auditUserID.value;
	    	document.frmLockTag.txt_tagCount.value = document.frmLockTag.tagCount.value	    	
	}
	else
	{
	  for (i = 0; i < chk_length; i++) 
	  {			
	  		document.frmLockTag.chk_transactionID[i].checked = document.frmLockTag.selectAll.checked;
			document.frmLockTag.cbo_countedBy[i].value = document.frmLockTag.auditUserID.value;
	    	document.frmLockTag.txt_tagCount[i].value = document.frmLockTag.tagCount.value
	   }
	}	
}

 function fnReload()
{
    var transactionID = document.frmLockTag.transactionID.value;
    var projectID = document.frmLockTag.projectListID.value;
    var blankTags = document.frmLockTag.blankTag.checked;
    var physicalAuditId =  document.frmLockTag.physicalAuditID.value;    
    var locationID = document.frmLockTag.locationID.value;
    var inputStringPno =  document.frmLockTag.inputStringPnum.value;
    var untaggedTxnId = document.frmLockTag.untaggedTxn.checked;
    
    var rep = new RegExp("\r\n","g");
    var inputStringPno = inputStringPno.replace(rep,"");
    document.frmLockTag.inputStringPnum.value = inputStringPno;

    var strUploadExcel =  document.frmLockTag.uploadExcel.value;
       var strUploadFile = strUploadExcel.substring(strUploadExcel.lastIndexOf("\\")+1,strUploadExcel.length);
    if(!strUploadFile == ''){
    	document.frmLockTag.locationID.value = "0";
    }
    
    if (inputStringPno == '')
    {
    	if (physicalAuditId == '')
    	{
     		Error_Details(message_accounts[511]);
    	}
  		if(strUploadFile.length==0){
	    	if (locationID == '0' && !blankTags){
       			Error_Details(message_accounts[512]);
    		}
    		else if(locationID != '0' && transactionID == '' && projectID == '0' && inputStringPno == '' && !untaggedTxnId && !blankTags){
     			Error_Details(message_accounts[513]);
    		}else if (untaggedTxnId  && transactionID != '' && projectID != '0' && inputStringPno != '' && locationID == '0'){
    			Error_Details(message_accounts[514]);
    		}else if (transactionID.indexOf("-") == -1 && transactionID != '' ){
    			Error_Details(message_accounts[515]);
    		}/*else{ 
    			Error_Details(" One of the following parameter is Mandatory <br> 1. Location <br> 2. Generate Blank Tags");
    		}*/
    	}
    }
    else{
    if(inputStringPno.length >= 4000 )
	    {
	       Error_Details(message_accounts[516]);
	    }
    if(locationID == '0'){
    	Error_Details(message_accounts[517]);
    }
    }
    
            
    if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	
	document.frmLockTag.blankTag.value = document.frmLockTag.blankTag.checked;	 		
	document.frmLockTag.strOpt.value = "RELOAD";	
	document.body.style.cursor = 'wait';
	fnStartProgress();
	document.frmLockTag.submit();
}

function fetchCountedBy(){
//chk if value is not 0 then submit
	document.frmLockTag.strOpt.value='Load';
	document.frmLockTag.submit();
}

function toggledisplay()
{
 var size =  '<%=returnList.size()%>';
 
 if( size > 0)
 {
  var objDivStyle = eval('document.all.applyall.style');
  objDivStyle.display = 'table-row';
  
  var objDivStyle = eval('document.all.applyalldetails.style');
  objDivStyle.display = 'block';
  
    var objDivStyle = eval('document.all.div_dtable.style');
  objDivStyle.display = 'block';
  
  var objDivStyle = eval('document.all.div_save.style');
  objDivStyle.display = 'block';
 }
 else 
 { 
  var objDivStyle1 = eval('document.all.applyall.style');
  objDivStyle1.display = 'none';
  
  var objDivStyle1 = eval('document.all.applyalldetails.style');
  objDivStyle1.display = 'none';
  
  var objDivStyle = eval('document.all.div_dtable.style');
  objDivStyle.display = 'none';
  
  var objDivStyle = eval('document.all.div_save.style');
  objDivStyle.display = 'none';
  }
}

</script>

</HEAD>
<BODY leftmargin="20" topmargin="10" onload="javascript:toggledisplay();" >
 
<html:form action="/gmGenerateTag.do" method="post" enctype="multipart/form-data"> 
<html:hidden property="strOpt" value=""/> 
<html:hidden property="hinputString" />

 <table border="0" class="DtTable1000" cellspacing="0" cellpadding="0" style="width: 51%;">
     <tbody style="display: block;">
	<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4"><fmtPAGenerateTags:message key="LBL_MANUAL_GENERATE_TAGS"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtPAGenerateTags:message key="IMG_HELP" var="varHelp"/>
		  	<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />  
	       </td>
	</tr>
	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<tr>
			<td height="30" class="RightTableCaption" align="right"><font color="red">*</font><fmtPAGenerateTags:message key="LBL_AUDIT_LIST"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="physicalAuditID" SFFormName="frmLockTag" SFSeletedValue="physicalAuditID"
							SFValue="physicalAuditList" codeId = "CODEID"  codeName = "CODENM"  onChange="fetchCountedBy();" defaultValue= "[Choose One]"/>
			</td>
			<td height="30" class="RightTableCaption" align="right"><fmtPAGenerateTags:message key="LBL_PROJECT_LIST"/>:</td>
			<td style="padding-left: 3px;"> <gmjsp:dropdown controlName="projectListID" SFFormName="frmLockTag" SFSeletedValue="projectListID"
							SFValue="projectList" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]"/> 		
			</td>
			<td>&nbsp;</td>			
		</tr>		

		<tr><td colspan="5" class="ELine" height="1"></td></tr>

		<tr  class="shade">
			<td height="30" class="RightTableCaption" align="right"><fmtPAGenerateTags:message key="LBL_WAREHOUSE"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="locationID" SFFormName="frmLockTag" SFSeletedValue="locationID"
							SFValue="locationList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"/>
			</td>			
			<td height="30" class="RightTableCaption" align="right"><html:checkbox property="blankTag" /></td>
			<td>&nbsp;<fmtPAGenerateTags:message key="LBL_GENERATE_BLANK_TAGS"/></td>
			<td>&nbsp;</td>
		</tr>				  

		 <tr><td colspan="5" class="ELine"></td></tr>	
		 <tr>					
			<td height="30" class="RightTableCaption" align="right"><fmtPAGenerateTags:message key="LBL_REF_ID"/>:</td>
			<td>&nbsp;<html:text property="transactionID"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
			
			<td height="30" class="RightTableCaption" align="right" style="white-space: nowrap;"><fmtPAGenerateTags:message key="LBL_PART_NUMBER"/>:</td>
			<td>
			&nbsp;<html:textarea property="inputStringPnum" rows="5" cols="45" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
			&nbsp;&nbsp;
			</td>
			<td>&nbsp;</td>
		</tr>	
			<tr><td colspan="5" class="ELine"></td></tr>		
		<tr class="shade">
			<td height="30" class="RightTableCaption" align="right" style="white-space: nowrap;"><fmtPAGenerateTags:message key="LBL_SELECT_FILE_TO_UPLOAD"/>:</td>
			<td height="30" style="padding-left: 3px;"><html:file property="uploadExcel" size="40"/></td>
			
			<td height="30" class="RightTableCaption" align="right"><html:checkbox property="untaggedTxn" /></td>
			<td>&nbsp;<fmtPAGenerateTags:message key="LBL_UNTAGGED_TRANSACTIONS"/>&nbsp;&nbsp;&nbsp;<fmtPAGenerateTags:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" name="Btn_Go" gmClass="button" buttonType="Load" onClick="javascript:fnReload();" /></td>
			<td>&nbsp;</td>
		</tr>	
		<tr id="applyall" style="display: none">
			<td  height="25"  class="RightDashBoardHeader" colspan="5" > <fmtPAGenerateTags:message key="LBL_APPLY_ALL"/> </td>
		</tr>	  
		
		 <tr id="applyalldetails" style="display: none">
		    <td colspan="4"><table > <tr>			
		    <td height="30" class="RightTableCaption" align="left"> <input type= "checkbox" name="selectAll" /> <fmtPAGenerateTags:message key="LBL_SELECT_ALL_TRANSACTIONS"/></td>		    
			<td height="30" class="RightTableCaption" align="right"><fmtPAGenerateTags:message key="LBL_COUNTED_BY"/> :</td>
			<td>&nbsp;<gmjsp:dropdown controlName="auditUserID" SFFormName="frmLockTag" SFSeletedValue="auditUserID"
							SFValue="auditUserList" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" />
			</td>
			<td height="30" class="RightTableCaption" align="right"><fmtPAGenerateTags:message key="LBL_TAG_COUNT"/> :</td>
			<td> &nbsp;<html:text property="tagCount"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
			</td>
			<td align="left">
				<fmtPAGenerateTags:message key="LBL_APPLY_ALL" var="varApplyAll"/><gmjsp:button value="${varApplyAll}" name="Btn_Apply_All" gmClass="button" onClick="javascript:fnApplyAll();" buttonType="Save" />
			</td>			
			</tr> </table> </td>	
		</tr>	
		
		<tr><td class="LLine" height="1" colspan="5"></td></tr>
		<tr id="div_dtable" style="display: none"><td colspan="5">
		 <display:table name="requestScope.frmLockTag.returnList" class="its" id="currentRowObject"  decorator = "com.globus.accounts.displaytag.beans.DTGenerateTagWrapper" >
		<fmtPAGenerateTags:message key="LBL_PART" var="varPart"/><display:column property="PNUM" title="${varPart}" style="width:100"/>
		<fmtPAGenerateTags:message key="LBL_REF_ID" var="varRefId"/><display:column property="REFID" title="${varRefId}" style="width:100"/>
		<fmtPAGenerateTags:message key="DT_DESCRIPTION" var="varDescription"/><display:column property="TXNDESC" title="${varDescription}" /> 
		<fmtPAGenerateTags:message key="DT_WAREHOSE" var="varWarehose"/><display:column property="LOCATIONNM" title="${varWarehose}"  />
		<fmtPAGenerateTags:message key="LBL_COUNTED_BY" var="varCountedBy"/><display:column property="COUNTED_BY" title="${varCountedBy}" />
		<fmtPAGenerateTags:message key="LBL_TAG_COUNT" var="varTagCount"/><display:column property="TAGCOUNT" title="${varTagCount}"  />		
		</display:table>
		</td></tr>
		<%if(intExcludeParts>0) {%>
			<tr><td  height="25"  class="RightDashBoardHeader" colspan="5" ><fmtPAGenerateTags:message key="LBL_EXCLUDED_PARTS"/> </td></tr>
			<tr class="ShadeBlueBk"><td class="RightTableCaption"><fmtPAGenerateTags:message key="LBL_PART"/> </td><td class="RightTableCaption"><fmtPAGenerateTags:message key="LBL_WAREHOUSE"/> </td></tr>
			<%
				HashMap hm1 = new HashMap();
				for(int i=0;i<intExcludeParts;i++){
					hm1 = (HashMap)missingPart.get(i);
					strPartNumber =GmCommonClass.parseNull((String) hm1.get("PART"));
					strWarehouse =GmCommonClass.parseNull((String) hm1.get("WAREHOUSE"));
			 		strShade = (i%2 !=0)?"class=shade":"";
			%>
	<tr <%=strShade %>>
		<td> <%=strPartNumber%> </td>
		<td> <%=strWarehouse%> </td>
	</tr>
	<%	}
		%>
		<tr><td colspan="5" class="ELine"></td></tr>
		 <%}%>
		
		<tr id="div_save" style="display: none">
		 <td  colspan= "6" height="25" align="center" style="display: block;">
					<fmtPAGenerateTags:message key="LBL_CHOOSE_ACTION"/>: <gmjsp:dropdown controlName="actionType" SFFormName="frmLockTag" SFSeletedValue="actionType"
							SFValue="alType" codeId="CODENM"  codeName="CODENM"  defaultValue= "[Choose One]"/>&nbsp;&nbsp;
  &nbsp;
  <logic:equal name="frmLockTag" property="strEnableSubmit"  value="false">
  <fmtPAGenerateTags:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" disabled="true" name="Btn_Sumbit" gmClass="button" onClick="fnSubmit();" buttonType="Save" />
  </logic:equal>
  <logic:equal name="frmLockTag" property="strEnableSubmit"  value="true">
  <fmtPAGenerateTags:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" name="Btn_Sumbit" gmClass="button" onClick="fnSubmit();" buttonType="Save" />
  </logic:equal>
			</td>	
		</tr></tbody>
	</table>		
 </html:form>
    <%@ include file="/common/GmFooter.inc" %>
 </BODY>
 </HTML>
