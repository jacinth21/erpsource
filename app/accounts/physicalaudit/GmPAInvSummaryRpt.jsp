 <%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.text.*" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtPAInvSummaryRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \accounts\physicalaudit\GmPAInvSummaryRpt.jsp -->
<fmtPAInvSummaryRpt:setLocale value="<%=strLocale%>"/>
<fmtPAInvSummaryRpt:setBundle basename="properties.labels.accounts.physicalaudit.GmPAInvSummaryRpt"/>
<%
String strWikiTitle = GmCommonClass.getWikiTitle("VIEW_LOCKED_PRICING");
String strCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
%>
<bean:define id="gridData" name="frmLockTag" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="alSummaryList" name="frmLockTag" property="alSummaryList" type="java.util.ArrayList"> </bean:define>
<HTML>
<HEAD>
<TITLE> Globus Medical: Inventory Value Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>


<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
function fnGo()
{

   fnValidateDropDn('physicalAuditID',message_accounts[285]);
		
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	
	if(document.frmLockTag.detail.checked==true)
	{
	document.frmLockTag.strOpt.value='detail';
	}else
	{
	document.frmLockTag.strOpt.value='summary';
	}
	
	document.body.style.cursor = 'wait';
	document.frmLockTag.action='/gmPAInvSummaryRpt.do';
	fnStartProgress();
	document.frmLockTag.submit();
	return false;
}



var objGridData;
var gridObj;
objGridData = '<%=gridData%>';
function fnOnPageLoad(){
	
	if(objGridData!='')
		{
		
	var footer = new Array();
	
		footer[0]=message_accounts[286];
		footer[1]="";
		footer[2]="{#stat_total}";
		footer[3]="{#stat_total}";
		footer[4]="{#stat_total}";
		footer[5]="{#stat_total}";
		footer[6]="{#stat_total}%";
		footer[7]="{#stat_total}";
		footer[8]="{#stat_total}%";
		footer[9]="{#stat_total}";
		footer[10]="{#stat_total}%";
		footer[11]="{#stat_total}";
		footer[12]="{#stat_total}%";
	
		  gridObj = initGridWithDistributedParsing('pisummarydata',objGridData,footer);
    
		   var count=0;
		   var var_qty_per='',abs_qty_per='',var_amt_per='',abs_amt_per='';
		  gridObj.attachEvent("onStatReady",function(){
			 
			  count++;
			  if(count==11)
			  {	  
				  var_qty_per=''+roundNumber(((fnRemoveUnWanted(gridObj.getFooterLabel(5))/fnRemoveUnWanted(gridObj.getFooterLabel(3)))*100),2)+'%';
				  abs_qty_per=''+roundNumber(((fnRemoveUnWanted(gridObj.getFooterLabel(7))/fnRemoveUnWanted(gridObj.getFooterLabel(3)))*100),2)+'%';
				  var_amt_per=''+roundNumber(((fnRemoveUnWanted(gridObj.getFooterLabel(9))/fnRemoveUnWanted(gridObj.getFooterLabel(2)))*100),2)+'%';
				  abs_amt_per=''+roundNumber(((fnRemoveUnWanted(gridObj.getFooterLabel(11))/fnRemoveUnWanted(gridObj.getFooterLabel(2)))*100),2)+'%';
				  gridObj.setFooterLabel(6,var_qty_per);	  
				  gridObj.setFooterLabel(8,abs_qty_per);
				  gridObj.setFooterLabel(10,var_amt_per);	  
				  gridObj.setFooterLabel(12,abs_amt_per);
				 
			  }
			  
			  });
		
		}
		
}
function fnRemoveUnWanted(value)
{
	var intVal=0;
	
	if(value!=null)
	{
		value=value.replace(new RegExp(',', 'g'),'');
		value=value.replace('$','');
		intVal=value;
		return intVal;
	}else
	{
	  return 0;	 
	}
}

function roundNumber(number, digits) {
	var multiple = Math.pow(10, digits);            
	var rndedNum = Math.round(number * multiple) / multiple;
	return rndedNum;        
}



</script>

</HEAD>

<BODY leftmargin="20" topmargin="10"  onLoad="fnOnPageLoad();">
<html:form action="/gmPAInvSummaryRpt.do" >

<input type="hidden" name="strOpt" >

<table border="0" width="700" cellspacing="0" cellpadding="0"  class="DtTable700">
		<tr>
			
			<td  height="1" bgcolor="#666666" colspan="2"></td>
			
		</tr>
		<tr>
			<td  height="25" class="RightDashBoardHeader"  > <fmtPAInvSummaryRpt:message key="LBL_INVENTORY_SUMMARY_REPORT"/> </td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtPAInvSummaryRpt:message key="IMG_HELP" var="varHelp"/>
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		<tr><td class="LLine" height="1" colspan="2"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td  >
			<table><tr><td class="RightTableCaption" align="left" HEIGHT="24"  width="40%">
			&nbsp;<font color="red">*</font><fmtPAInvSummaryRpt:message key="LBL_PHYSICAL_AUDIT"/>:
	
			&nbsp;<gmjsp:dropdown controlName="physicalAuditID"  SFFormName='frmLockTag' SFSeletedValue="physicalAuditID"  	
						tabIndex="1"  SFValue="physicalAuditList" codeId="CODEID" codeName="CODENM" defaultValue= "[Choose One]"/>
				</td><td class="RightTableCaption" align="center" HEIGHT="24" width="40%">		
				&nbsp;		<html:checkbox property="detail" /> <fmtPAInvSummaryRpt:message key="LBL_DETAIL_REPORT"/>
				
				</td><td width="30%" align="center">
							&nbsp;<fmtPAInvSummaryRpt:message key="BTN_GO" var="varGo"/><gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" name="Btn_Go" gmClass="button" onClick="return fnGo()" buttonType="Load" />
							</td></tr>
							</table>
			</td>
			
		</tr>
		
					
		
		<tr>
			<td colspan="2">
				 
				  <%if(!gridData.equals("")){%>
				 
					<tr>
						<td colspan="6">
							<div  id ="pisummarydata" width="1060px" height="450px"></div>
						</td>
					</tr>
					<tr>
			                <td colspan="6" align="center">
			               <div class='exportlinks'><fmtPAInvSummaryRpt:message key="LBL_EXPORT_OPTIONS"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
               onclick="gridObj.toExcel('/phpapp/excel/generate.php');"><fmtPAInvSummaryRpt:message key="LBL_EXCEL"/>  </a></div>
			                           
				</a></div>
			                </td>

					</tr>
					<%} %>
				 </td>
				 
					</tr>
					<% if(alSummaryList.size()>0) {%>
					
					<tr>
					
	<td colspan="5"><display:table	pagesize="500"  name="requestScope.frmLockTag.alSummaryList" class="its" id="currentRowObject" export="true" requestURI="/gmPAInvSummaryRpt.do"	varTotals="totals"	>
        <display:setProperty name="export.excel.filename" value="Inv Summary Report.xls" />
		<fmtPAInvSummaryRpt:message key="DT_REF_ID" var="varRefID"/><display:column property="REF_ID" title="${varRefID}"  style="width:100" sortable="true"  class="alignleft"/>
		<fmtPAInvSummaryRpt:message key="DT_WAREHOUSE_ID" var="varWarehouseID"/><display:column property="WAREHOUSE_ID" title="${varWarehouseID}"  style="width:100" sortable="true" class="aligncenter" />
		<fmtPAInvSummaryRpt:message key="DT_WARHOUSE" var="varWarhouse"/><display:column property="WAREHOUSE" title="${varWarhouse}" sortable="true"class="alignleft" />
		<fmtPAInvSummaryRpt:message key="DT_BEFORE" var="varBefore"/><display:column property="SYSTEM_QTY" title="${varBefore}" style="width:100" sortable="true" class="alignright" />
		<fmtPAInvSummaryRpt:message key="DT_AFTER_TAG" var="varAfterTag"/><display:column property="COUNT_QTY" title="${varAfterTag}" style="width:100" sortable="true"   class="alignright" />
		<fmtPAInvSummaryRpt:message key="DT_DEVIATION_QTY" var="varDeviationQTY"/><display:column property="VARIANCE_QTY" title="${varDeviationQTY}"  style="width:100" sortable="true"   class="alignright" />
		<fmtPAInvSummaryRpt:message key="DT_DEVIATION" var="varDeviation"/><display:column property="VARIANCE_AMOUNT" title="${varDeviation}" style="width:100" sortable="true" class="alignright"/>
							
	</display:table></td>
</tr>
  	<%} %>	
    </table>		     	
		
		
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
