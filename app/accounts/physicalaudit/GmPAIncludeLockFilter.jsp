 <%
/**********************************************************************************
 * File		 		: GmPAIncludeLockFilter.jsp
 * Desc		 		: This screen is used for the displaying Lock Filters
 * Version	 		: 1.1
 * author			: Xun
 * 
************************************************************************************/
%>
  <%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtPAIncludeLockFilter" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \accounts\physicalaudit\GmPAIncludeLockFilter.jsp -->
<fmtPAIncludeLockFilter:setLocale value="<%=strLocale%>"/>
<fmtPAIncludeLockFilter:setBundle basename="properties.labels.accounts.physicalaudit.GmPAIncludeLockFilter"/>

<%
 
 
	
%>
 
 
<script>
 
  
function fnSubmit()
{
	  
 
	document.frmLogTag.submit();
}

function fetchCountedBy(){
	document.frmLockTag.strOpt.value='load';
	document.frmLockTag.submit();
}

function enableByPart(){
	
var locationid=	document.frmLockTag.locationID.value;
    	if(locationid==20450 || locationid==4000114)
		{
	    	document.frmLockTag.byPart.disabled=false;
		}else
		{
			document.frmLockTag.byPart.disabled=true;
		}
    	
    	updateSubmitSts();
}
function updateSubmitSts()
{
	var bypart =document.frmLockTag.byPart.checked;
	 
	    if(bypart && document.frmLockTag.verify!=null)
		{
		     document.frmLockTag.verify.disabled=true;
		}else
		{
			
			if(document.frmLockTag.verify!=null && !document.frmLockTag.verify.disabled)
		    {
		     document.frmLockTag.verify.disabled=false;
		    }
		}
    
}
</script>
 

 
 
	 
		 
		<!-- Custom tag lib code modified for JBOSS migration changes --> 
		<tr>
			<td height="30" class="RightTableCaption" align="right">&nbsp;&nbsp;&nbsp; <font color="red">*</font><fmtPAIncludeLockFilter:message key="LBL_AUDIT_LIST"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="physicalAuditID" SFFormName="frmLockTag" SFSeletedValue="physicalAuditID"
							SFValue="physicalAuditList" codeId = "CODEID"  codeName = "CODENM" onChange="fetchCountedBy();" defaultValue= "[Choose One]"/>
			</td>
			<td height="30" class="RightTableCaption" align="right">&nbsp;<fmtPAIncludeLockFilter:message key="LBL_PART_REF_ID"/>:</td>
			<td>&nbsp;<html:text property="transactionID"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
			 
			 
			 
		</tr>
		
		<tr><td colspan="4" class="ELine"></td></tr>
		<tr>
			<td height="30" class="RightTableCaption" align="right">&nbsp;&nbsp;&nbsp; <font color="red">*</font><fmtPAIncludeLockFilter:message key="LBL_WAREHOUSE"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="locationID" SFFormName="frmLockTag" SFSeletedValue="locationID"
							SFValue="locationList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"  onChange="enableByPart();"/>
			</td>
			<td height="30" class="RightTableCaption" align="right">&nbsp;<fmtPAIncludeLockFilter:message key="LBL_PROJECT_LIST"/>:</td>
			<td> &nbsp;<gmjsp:dropdown controlName="projectListID" SFFormName="frmLockTag" SFSeletedValue="projectListID"
							SFValue="projectList" codeId = "ID"  codeName = "NAME"  defaultValue= "[All Projects]"/> 		
			</td>
			 
			 
		</tr>
		
		  <tr><td colspan="4" class="ELine"></td></tr>
		<tr>
			<td height="30" class="RightTableCaption" align="right">&nbsp;&nbsp;&nbsp;&nbsp;<fmtPAIncludeLockFilter:message key="LBL_IN_STOCK_BY_PART"/>:</td>
			<td> &nbsp;<html:checkbox  property="byPart" disabled="true"  onclick="updateSubmitSts();"/>
			</td>
			<td height="30" class="RightTableCaption" align="right">&nbsp;<fmtPAIncludeLockFilter:message key="LBL_VALUE_NOT_RECONCILED"/>:</td>
			<td>
			<table width="100%" ><tr>
			<td height="30" width="30%" class="RightTableCaption">&nbsp; <html:checkbox  property="valueNotReconciled" />	</td>
			
			<td height="30" width="40%" class="RightTableCaption" align="right">&nbsp;<fmtPAIncludeLockFilter:message key="LBL_DISPLAY_ONLY_DEVIATION"/>:</td>
			<td height="30" width="30%" class="RightTableCaption">&nbsp; <html:checkbox  property="displayOnDeviation" /></td></tr></table>
			</td>
			
			
			 
			 
		</tr>
		 <tr><td colspan="4" class="ELine"></td></tr>	
		<tr>
			<td height="30" class="RightTableCaption" align="right">&nbsp;&nbsp;&nbsp;&nbsp;<fmtPAIncludeLockFilter:message key="LBL_DEVIATION_VALUE"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="deviationOprID" SFFormName="frmLockTag" SFSeletedValue="deviationOprID"
								SFValue="deviationOperatorsList" codeId = "CODENMALT"  codeName = "CODENM"  defaultValue= "[Choose One]" />
							&nbsp;
							 <html:text property="deviationOprValue"  size="5" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
			</td>
			<td height="30" class="RightTableCaption"align="right">&nbsp;<fmtPAIncludeLockFilter:message key="LBL_DEVIATION_QTY"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="deviationOprQtyID" SFFormName="frmLockTag" SFSeletedValue="deviationOprQtyID"
								SFValue="deviationOperatorsList" codeId = "CODENMALT"  codeName = "CODENM"  defaultValue= "[Choose One]" />
							&nbsp;
							 <html:text property="deviationOprQtyValue"  size="5" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>	
							 &nbsp; <fmtPAIncludeLockFilter:message key="BTN_RELOAD" var="varReload"/><gmjsp:button value="&nbsp;&nbsp;${varReload}&nbsp;&nbsp;" onClick="javascript:fnReload();" tabindex="3" gmClass="button" buttonType="Load" /> 
			</td>
			 
			  
		</tr>
	 
 
 
