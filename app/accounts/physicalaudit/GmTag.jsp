<%
	/**********************************************************************************
	 * File		 		: GmTag.jsp
	 * Desc		 		: This screen is used tag entry or edit
	 * Version	 		: 1.0
	 * author			: Xun
	 * 
	 ************************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ taglib prefix="fmtTag" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmTag.jsp -->
<fmtTag:setLocale value="<%=strLocale%>"/>
<fmtTag:setBundle basename="properties.labels.custservice.ProcessRequest.GmProcessRequestItems"/>
<bean:define id="taggedReturnList" name="frmTag" property="taggedReturnList" type="java.util.List"></bean:define>
<bean:define id="refType" name="frmTag" property="refType" type="java.lang.String"></bean:define>
<bean:define id="strIntransitFlag" name="frmTag" property="strIntransitFlag" type="java.lang.String"></bean:define>
<bean:define id="unTaggedReturnList" name="frmTag" property="unTaggedReturnList" type="java.util.List"></bean:define>
 
<%
	DynaBean dbReturnDtls =null;
	ArrayList alList1 = new ArrayList();
	ArrayList alList2 = new ArrayList();	
	 
	alList1 = GmCommonClass.parseNullArrayList((ArrayList) taggedReturnList);  //Tagged part list
	alList2 = GmCommonClass.parseNullArrayList((ArrayList) unTaggedReturnList);  //UnTagged part list
	int rowsize1 = alList1.size();
	int rowsize2 = alList2.size();
	boolean buttonFlag=false;
	if(rowsize2!=0)
	{
		buttonFlag=true;
	}
	
	StringBuffer sbInput = new StringBuffer();
	String strPar = "";
	String strInput = "";
	int rowcount =0;  
 	
 	String strWikiTitle = GmCommonClass.getWikiTitle("PART_TAG_ENTRY");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Part Tag Info</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"
	media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script> 

function fnTagHistory(tagid)
{ 	 
	windowOpener("/gmTagHistory.do?tagID="+tagid,"TagHistory","resizable=yes,scrollbars=yes,top=250,left=300,width=1050,height=400");
} 
 
function fnTagVoid(tagid,tagStatus){
	if(tagStatus == '102401'){ // Transferred
		Error_Details(message[5044]);
	}
	
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
    document.frmTag.hCancelType.value = 'VODTG'
    document.frmTag.hTxnId.value = tagid;//document.frmTag.tagID.value;
    document.frmTag.action ="/GmCommonCancelServlet";
    document.frmTag.hAction.value = "Load";
    document.frmTag.submit();
}

function fnTagUnlink(tagid,tagStatus){
	if(tagStatus == '102401'){ // Transferred
		Error_Details(message_accounts[530]);
	}
	if (ErrorCount > 0){
		Error_Show();
		Error_Clear();
		return false;
	}
    document.frmTag.hCancelType.value = 'UNKTG'
    document.frmTag.hTxnId.value = tagid;//document.frmTag.tagID.value;
    document.frmTag.action ="/GmCommonCancelServlet";
    document.frmTag.hAction.value = "Load";
    document.frmTag.submit();
}


function fnSubmit()
					{
						objLoop = eval("document.frmTag.rowloop");
						var varRows =  objLoop.value  ;
					    
						var inputString = '';   
						var Location='';
						var refType=document.frmTag.refType.value;
						var item="";
						var missingFlag="";
						for (var k=0; k < varRows; k++) 
						{
							
							objTag = eval("document.frmTag.TagID"+k);
							 	
							
							objPartNum = eval("document.frmTag.PartNum"+k); 
							objControlNum = eval("document.frmTag.ControlNum"+k);
							objRefID = eval("document.frmTag.RefID"+k);
							objSetID = eval("document.frmTag.SetID"+k);
							//objLocationType = eval("document.frmTag.LocationType"+k);
						//objLocation = eval("document.frmTag.Location"+k);
							 
							 LocationType  = 40033;
							 missingFlag="";
							 if(refType=='51001')
							 {
							 	missingFlag=eval("document.frmTag.missingFlag"+k);
								if(missingFlag.checked == true)
								{
									missingFlag='Y';
								}
								else
								{
								missingFlag=""
								}
							 	item=eval("document.frmTag.strItemId"+k).value;
							 	inputString = inputString + objTag.value + '^'+ objControlNum.value +'^'+ objPartNum.value + '^'+ objSetID.value +'^' + document.frmTag.refType.value +'^' + objRefID.value +'^'+ LocationType+'^'+missingFlag+'^'+item
							 + '^'+ Location  +  '|'; 
							 }
							 else{
							inputString = inputString + objTag.value + '^'+ objControlNum.value +'^'+ objPartNum.value + '^'+ objSetID.value +'^' + document.frmTag.refType.value +'^' + objRefID.value +'^'+ LocationType
							 + '^'+ Location  +  '|'; 
							 }
						  if(objTag.value ==''&& missingFlag!='Y')
							{
							Error_Details(message[5045]);
							}
							if(objTag.value !=''&& missingFlag=='Y')
							{
							Error_Details(message[5046]);
							}
						} 
						if (ErrorCount > 0)
							{
									Error_Show();
									Error_Clear();
									return false;
							}  
					 //	 alert("varRows is " + varRows);
					//	 alert("document.frmTag.refType.value is " + document.frmTag.refType.value);
						document.frmTag.strOpt.value =  'save'; //'save';
						document.frmTag.hinputStr.value = inputString;
						fnStartProgress();
				 	  	document.frmTag.submit();	
					  }
					  
function fnClose()
	{ 
		window.close();
	}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmTag.do">
	<html:hidden property="strOpt" value="" />
	<input type="hidden" name="hTxnId" value=""/>
	<input type= "hidden" name="hTxnName" value=""/>
	<input type= "hidden"  name="hCancelType" value=""/>
	<input type="hidden" name="hAction" >
	<input type="hidden" name="hinputStr" >
	 
	<html:hidden property="pnum"   />
	<html:hidden property="refID" />
	<html:hidden property="refType" />
	<html:hidden property="missingFlag" />
	
	<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4"><fmtTag:message key="LBL_TAGHEADER"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtTag:message key="LBL_HELP" var="varHelp"/>
			  <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		 
		 <table border="0"   class="DtTable1000" cellspacing="0"  cellpadding="0"  >
					  <thead>
						<TR bgcolor="#EEEEEE"   class="ShadeRightTableCaption" height = "20"  colspan="10">
							<TH class="RightText"  >&nbsp;<fmtTag:message key="LBL_TAG"/> #</TH>
							
							<% 		if(refType.equals("51001")){%>
						<TH class="RightText"  ><fmtTag:message key="LBL_TAGMISSING"/></TH>
						<% 	}%>
							<TH class="RightText"  ><fmtTag:message key="LBL_PART"/> # </TH>
							<TH class="RightText"  ><fmtTag:message key="LBL_CONTROL"/> #</TH>
							<TH class="RightText"  >&nbsp;<fmtTag:message key="LBL_REFID"/></TH>
							<TH class="RightText"  ><fmtTag:message key="LBL_SETID"/></TH>
					<% 		if(rowsize1>0){%>
							<TH class="RightText"  ><fmtTag:message key="LBL_LOCATIONTYPE"/></TH>
							<TH class="RightText" ><fmtTag:message key="LBL_LOCATION"/></TH>
							<TH class="RightText"  ><fmtTag:message key="LBL_SUBLOCATIONTYPE"/></TH>
							<TH class="RightText" ><fmtTag:message key="LBL_DISTRIBUTORREP"/></TH> 
							<TH class="RightText" ><fmtTag:message key="LBL_ACCOUNT"/></TH>
								<% 	}%>
						</TR>	
						<tr>
						<% 		if(refType.equals("51001")){%>
						<td class="Line" height="1" colspan="11"></td>
							<% 	}else{%>
						<td class="Line" height="1" colspan="10"></td>
						<% 	}%>
						</tr>
					  </thead>
					  <TBODY>
					  <%
					  	String strTag = "";
					    String strTagStatus = "";
						String strPnum = "";
						String strControlNum = "";
						String strRefId = "";
						String strSetId = "";
						String strLocationType = "";
						String strLocation = ""; 
						String strLockfl = "";
						String strItemId ="";
						String strMissing="";
						String strHistfl="";
						String strSubLocation = ""; 
						String strSubLocationType = ""; 
						String strAccount = ""; 
						
						boolean chkFlag=false;
						
						
						String strShade= "";
						
						
						for (int i=0; i < rowsize1;i++)
						{ 	
							dbReturnDtls =  (DynaBean)alList1.get(i) ;
							strTag =  GmCommonClass.parseNull((String)dbReturnDtls.get("TAGID")) ;
							strTagStatus = GmCommonClass.parseNull(String.valueOf(dbReturnDtls.get("TAG_STATUS")));
							strPnum = GmCommonClass.parseNull((String)dbReturnDtls.get("PNUM"));
							strControlNum = GmCommonClass.parseNull((String)dbReturnDtls.get("CONTROLNUM"));
							strRefId = GmCommonClass.parseNull((String)dbReturnDtls.get("REFID"));
							strSetId = GmCommonClass.parseNull((String)dbReturnDtls.get("SETID"));
							strLocationType = GmCommonClass.parseNull((String)dbReturnDtls.get("LOCATION_TYPE"));
							strLocation = GmCommonClass.parseNull((String)dbReturnDtls.get("LOCATION")); 
							strLockfl = GmCommonClass.parseNull((String)dbReturnDtls.get("LOCKFL"));
							strHistfl = GmCommonClass.parseNull((String)dbReturnDtls.get("HISTORYFL")); 
							
							strSubLocation = GmCommonClass.parseNull((String)dbReturnDtls.get("SUBLOCATION")); 
							strSubLocationType = GmCommonClass.parseNull((String)dbReturnDtls.get("SUBLOCATION_TYPE")); 
							strAccount = GmCommonClass.parseNull((String)dbReturnDtls.get("ACCOUNT")); 
							
							strMissing="";
							if(refType.equals("51001"))
							{
								strMissing= GmCommonClass.parseNull((String)dbReturnDtls.get("MISSING_FL"));
							}
							strShade = (i%2 != 1)?"class=Shade":""; 
	                %> 
					<tr colspan="7" height="20"   <%=strShade %> >
						<td class="RightText" align="center">&nbsp;
						<%if((!strTag.equals("-")) && (!strIntransitFlag.equals("Y"))){%>
						<logic:equal name="frmTag" property="voidAccess" value="true">
						<img style='cursor:hand' src = '/images/void.jpg' onclick="javascript:fnTagVoid('<%=strTag%>','<%=strTagStatus%>')"  width='14' height='15' />  </a>
						</logic:equal>
						<%}if((!strTag.equals("-")) && (!strIntransitFlag.equals("Y"))){%>
						<logic:equal name="frmTag" property="unlinkAccess" value="true">						
						<img style='cursor:hand' src = '/images/tag_unlink.jpg' onclick="javascript:fnTagUnlink('<%=strTag%>','<%=strTagStatus%>')" width='14' height='15' />  </a>
						</logic:equal>
						<%}if(strHistfl.equals("Y")){%>
						<a href=javascript:fnTagHistory('<%=strTag%>')>  <img   style='cursor:hand' src='/images/icon_History.gif'  width='14' height='15' />  </a><%}%>
						<%=strTag%>  </td>
						<% if(refType.equals("51001")){%>
						<td class="RightText" align="center">&nbsp;<%=strMissing%></td>
						<% 	}%>
						<td class="RightText" align="center">&nbsp;<%=strPnum%></td>
						<td class="RightText" align="center">&nbsp;<%=strControlNum%></td>
						<td class="RightText" align="center">&nbsp;<%=strRefId%></td>
						<td class="RightText" align="center">&nbsp;<%=strSetId%></td>
						<td class="RightText" align="center">&nbsp;<%=strLocationType%></td>
						<td class="RightText" align="center">&nbsp;<%=strLocation%></td> 
						<td class="RightText" align="center">&nbsp;<%=strSubLocationType%></td>
						<td class="RightText" align="center">&nbsp;<%=strSubLocation%></td>  
						<td class="RightText" align="center">&nbsp;<%=strAccount%></td> 
					</tr>
					
		 
					<!--<tr><td bgcolor="#eeeeee" height="1" colspan="11"></td></tr>-->
				   <%
						 
					}
	               
				        int k=0;
						for (int j=0; j < rowsize2;j++)
						{
							
							dbReturnDtls = (DynaBean)alList2.get(j);
						 	String strItemQty =   GmCommonClass.parseZero(String.valueOf( dbReturnDtls.get("ITEM_QTY")))  ; 
						 	String strTagCount =   GmCommonClass.parseZero(String.valueOf(dbReturnDtls.get("TAGCOUNT")))  ;
						 	
						 
						 	int rowloop = Integer.parseInt(strItemQty) - Integer.parseInt(strTagCount);
						 	rowcount = rowloop + rowcount;
						// 	System.out.println("rowloop>>>>>>>>>>>>>........" + rowloop);
							for (int i=0; i < rowloop;i++)
							{ 	
								dbReturnDtls =  (DynaBean)alList2.get(j) ;
								strTag =  GmCommonClass.parseNull((String)dbReturnDtls.get("TAGID")) ; 
								strPnum = GmCommonClass.parseNull((String)dbReturnDtls.get("PNUM"));
								strControlNum = GmCommonClass.parseNull((String)dbReturnDtls.get("CONTROLNUM"));
								strRefId = GmCommonClass.parseNull((String)dbReturnDtls.get("REFID"));
								strSetId = GmCommonClass.parseNull((String)dbReturnDtls.get("SETID"));
								//strLocationType = GmCommonClass.parseNull((String)dbReturnDtls.get("LOCATION_TYPE"));
								//strLocation = GmCommonClass.parseNull((String)dbReturnDtls.get("LOCATION")); 
								
								strMissing="";
								if(refType.equals("51001"))
								{
									strItemId= GmCommonClass.parseNull((String)dbReturnDtls.get("ITEMID").toString());
									strMissing= GmCommonClass.parseNull((String)dbReturnDtls.get("MISSING_FL"));
									
								}

								strShade = (i%2 != 0)?"class=Shade":""; 
								 
		                %> 
						<tr colspan="7"  height="20"    <%=strShade %> >
							
							
					<% 	if(!strMissing.equals("Y"))
							{%>		
					<td class="RightText" align="center">&nbsp;<input type="text" size="15" class=InputArea value="<%=strTag%>" name="TagID<%=k%>"   onFocus="changeBgColor(this,'#AACCE8');"/> </td>
					<%}else{
						%>
						<td class="RightText" align="center">&nbsp;<input type="hidden"  class=InputArea value="<%=strTag%>" name="TagID<%=k%>"  /> </td>
					<%} %>	
						<% if(refType.equals("51001"))
						{ 
							if(strMissing.equals("Y"))
							{
						%>
						
						<td class="RightText" align="center">&nbsp;<INPUT TYPE=CHECKBOX NAME="missingFlag<%=k%>"  disabled checked="true" onchange(this)/></td>
						
							<%}
							else
							{
							%>
							<logic:equal name="frmTag" property="missingAccess" value="true">
							<td class="RightText" align="center">&nbsp;<INPUT TYPE=CHECKBOX NAME="missingFlag<%=k%>" /></td>
							</logic:equal>
							<logic:notEqual name="frmTag" property="missingAccess" value="true">
							<td class="RightText" align="center">&nbsp;<INPUT TYPE=CHECKBOX NAME="missingFlag<%=k%>" disabled /></td>
							</logic:notEqual>
						<%}
						}%>
							<td class="RightText" align="center">&nbsp;<input type=hidden value='<%=strPnum%>' name="PartNum<%=k%>"><%=strPnum%></td>
							<td class="RightText" align="center">&nbsp;<input type=hidden value='<%=strControlNum%>' name="ControlNum<%=k%>"><%=strControlNum%></td>
							<td class="RightText" align="center">&nbsp;<input type=hidden value='<%=strRefId%>' name="RefID<%=k%>"><%=strRefId%></td>
							<td class="RightText" align="center">&nbsp;<input type=hidden value='<%=strSetId%>' name="SetID<%=k%>"><%=strSetId%></td>
							<input type=hidden value='<%=strItemId%>' name="strItemId<%=k%>">

						</tr>
						<!--<tr><td bgcolor="#eeeeee" height="1" colspan="11"></td></tr>-->
					   <%
					   k++;
							}
							
						}
						if(rowsize1==0&&rowsize2==0)
						{
					%>
					
					<tr colspan="10"  height="20"  >
						 <td  class="RightText" align="center" colspan="7">
						 <fmtTag:message key="LBL_MESSAGE"/>
						 </td>
					</tr>
					
					<% 
							
						}
		               %>
		               <input type=hidden value='<%=rowcount%>' name="rowloop">
		               
					  </TBODY> 
					<tr>
					<%if(buttonFlag==true){%>
						<td colspan="10" align="center">&nbsp;&nbsp; <p>&nbsp;</p>  
									 <fmtTag:message key="BTN_SUBMIT" var="varSubmit"/>
									 <fmtTag:message key="BTN_CLOSE" var="varClose"/>
									 <%if(!strIntransitFlag.equals("Y")){%>  
			                         <gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save"  onClick="fnSubmit();" /> 
			                         <%}%>
			                         <gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="fnClose();" buttonType="Load" />
			          <%}%>
			                         <p>&nbsp;</p>
			              
			         </td>
					</tr> 
					</table>
	 
		
		 	
		 
	</table>
 
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>