<%
/**********************************************************************************
 * File		 		: GmPATag.jsp
 * Desc		 		: This screen is used to display Issue or missing Tag Count
 * Version	 		: 1.0
 * author			: Angela Xiang
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ taglib prefix="fmtPATag" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \accounts\physicalaudit\GmPATag.jsp -->
<fmtPATag:setLocale value="<%=strLocale%>"/>
<fmtPATag:setBundle basename="properties.labels.accounts.physicalaudit.GmPATag"/>
   
 
 <bean:define id="returnList" name="frmLockTag" property="returnList" type="java.util.List"></bean:define>
<%
String strApplDateFmt = strGCompDateFmt;
String strDateFmt = "{0,date,"+strApplDateFmt+"}";

	ArrayList alList = new ArrayList();
	 
	alList = GmCommonClass.parseNullArrayList((ArrayList) returnList);
	 
	int totalRows = alList.size();
	/*if(alList.size()>0)
	 { 
		for(int i=0;i<alList.size();i++)
		{
		BasicDynaBean hmReturn=(BasicDynaBean)alList.get(i);
     out.println("total rows---"+hmReturn.get("TID"));
		}
	 }*/
 
String strWikiTitle = GmCommonClass.getWikiTitle("AUTO_GENERATE_TAGS");
String strWikiTitleOut = GmCommonClass.getWikiTitle("OUTSTANDING_TAGS");
%>

<%@page import="org.apache.commons.beanutils.DynaClass"%>
<%@page import="org.apache.commons.beanutils.BasicDynaBean"%><HTML>
<HEAD>
<TITLE> Globus Medical: Issue or Missing Tag</TITLE>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>	
<script language="JavaScript"> 
var tagId;
// Calculate the Total number of the checked rows and their tag Id
var TotalNumCheckedRows = 0;
var TotalRows = 0;
//var checkedTagIdlist; // this is a list will be printed
var uncheckedTagIdList;
var totalTagIdList;
var missingTag;
var strTagids="";
var TagCount=0;
var totalTags="";
var lblPhysicalAudit ='<fmtPATag:message key="LBL_PHYSICAL_AUDIT"/>';
var lblChooseAction ='<fmtPATag:message key="LBL_CHOOSE_ACTION"/>';
function fnGetPrintedTagIDs(CheckValue)
{
	tagId = CheckValue.value;
	if (CheckValue.checked)
	{
		TotalNumCheckedRows += 1 ;
		//checkedTagIdlist.add(tagId);
	}
}	
function fnGo()
{
	var frm=document.frmLockTag;
	var tagRangeFrom=TRIM(document.frmLockTag.tagRangeFrom.value);
	var tagRangeTo=TRIM(document.frmLockTag.tagRangeTo.value);
	  if(document.frmLockTag.tagRangeFrom.value == ""&&document.frmLockTag.tagRangeTo.value == ""&&document.frmLockTag.locationID.value == 0){
		Error_Details(message_accounts[520]);
		}

		if((tagRangeFrom == "" && tagRangeTo != "") || 	 (tagRangeFrom != "" && tagRangeTo == ""))
		{
			Error_Details(message_accounts[521]);
		}

		if((tagRangeFrom != "" || tagRangeTo != "") )
		{
			if(!isNumeric(tagRangeFrom) || !isNumeric(tagRangeTo))
			{ 
				Error_Details(message_accounts[522]);
			}else
			{
				if(parseInt(tagRangeFrom)>parseInt(tagRangeTo))
				{
					Error_Details(message_accounts[523]);
				}
			}
		}

	
	fnValidateDropDn('physicalAuditID',lblPhysicalAudit);
	//fnValidateDropDn('locationID', 'Location');
	
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.body.style.cursor = 'wait';
	document.frmLockTag.strOpt.value  = "reload";
	fnStartProgress();
	document.frmLockTag.submit();
}
 function fnReload()
{
	document.frmLockTag.strOpt.value = "reload";
	document.frmLockTag.submit();
}

 function fnSubmit(){
		var type=document.frmLockTag.actionType.value;
		if(type=="0")
		{
			Error_Details(message_accounts[501]);
		}
		if (ErrorCount > 0)  
		{
				Error_Show();
				Error_Clear();
				return false;
		}
		document.frmLockTag.strOpt.value = type;
	
		countchkd();
	
	if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmLockTag.submit();
//windowOpener("/GmPageControllerServlet?strPgToLoad=gmSetMapping.do&demandSheetId="+varSheetId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=500,status=1");

//windowOpener("/gmIssueTag.do?strOpt=print","print","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=500,status=1");
}



function countchkd()
{
//var chkdvals = new Array();	
//var unchkdvals = new Array();
var chkdvals=""; ;
var unchkdvals ="";
 TotalRows = <%=totalRows%>;
	var chk=0;	
	var notchk=0;
	//alert("TotalRows "+TotalRows );
	for (var i=0;i<TotalRows ;i++ )
	{	
		obj = eval("document.frmLockTag.Chk_tagid"+i);		
			if (obj.checked == true){
				//chkdvals[chk] = obj.value;
				chkdvals += 'I'+','+obj.value+'|';			
					chk ++;			
				}
			else if(obj.checked != true){
			//	unchkdvals[notchk] = obj.value;
				unchkdvals += 'E'+','+obj.value+'|'	;
				notchk ++;
			}		
	}
	
	if (chk != 0 ){
		if (chk <= notchk || notchk ==0){ //pass chk ..Include
			 document.frmLockTag.includeTags.value = chkdvals;
			}		
		else if (chk > notchk){ //pass notchk ..Exclude
			 document.frmLockTag.excludeTags.value = unchkdvals;
			}	
	}else
	{ 
		   Error_Details(message_accounts[508]);
		}
	
	//alert('checked '+document.frmLockTag.includeTags.value);	
	//alert('unchecked '+document.frmLockTag.excludeTags.value);
}
function fetchCountedBy(){

	document.frmLockTag.submit();
}

function fnVoidTag(){
	 var frm=document.frmLockTag;
	 TotalRows = <%=totalRows%>;
	 var chkdvals=""; ;
	 var unchkdvals ="";
	 var chk=0;	
	 var notchk=0;
	 var inputString="";
	 var option="";

	 fnValidateDropDn('actionType',lblChooseAction);
	 
	 physicalAuditID=frm.physicalAuditID.value;
	 locationID=frm.locationID.value;
	 auditUserID=frm.auditUserID.value;
	 tagRangeFrom=TRIM(frm.tagRangeFrom.value);
	 tagRangeTo=TRIM(document.frmLockTag.tagRangeTo.value);
	 transactionID=frm.transactionID.value;
	 
	 for (var i=0;i<TotalRows ;i++ )
		{	
			obj = eval("document.frmLockTag.Chk_tagid"+i);		
				if (obj.checked == true){
					chkdvals += obj.value+',';			
						chk ++;			
					}
				else if(obj.checked != true){
					unchkdvals += obj.value+','	;
					notchk ++;
				}		
	 }

	 if(chk<=0)
	 {
		 Error_Details(message_accounts[524]);
	 }

	 

	 
	 if(parseInt(TotalRows)==parseInt(chk))
	 {
		 inputString="ALL^"+physicalAuditID+"^"+locationID+"^"+auditUserID+"^"+tagRangeFrom+"^"+tagRangeTo+"^"+transactionID+"|";
	 }else
	 {
		 if(parseInt(notchk)<parseInt(chk))
		 {
			 inputString="UNCHECKED^"+physicalAuditID+"^"+locationID+"^"+auditUserID+"^"+tagRangeFrom+"^"+tagRangeTo+"^"+transactionID+"|"+unchkdvals;
			 inputString=inputString.substring(0,inputString.length-1);
			 
		 }else
		 {
			 inputString="CHECKED^"+physicalAuditID+"^"+locationID+"^"+auditUserID+"^"+tagRangeFrom+"^"+tagRangeTo+"^"+transactionID+"|"+chkdvals;
			 inputString=inputString.substring(0,inputString.length-1);
		 }
	 }
	 

	 if(inputString.length>4000)
	 {
		 Error_Details(message_accounts[525]);	
	 }
		 
	 	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}

	 	if(locationID>0){
			option='reload';
		}else{
			option='MissingTag';
		}
     	 frm.action ="/GmCommonCancelServlet";
         frm.hTxnId.value = inputString;
         frm.hTxnName.value = 'Void Record';  
         frm.hCancelType.value='VOSTAG';
         frm.hCancelReturnVal.value = 'true';
         frm.hRedirectURL.value = "/gmIssueTag.do?auditUserID="+auditUserID+"&haction=MissingTag&locationID="+locationID+"&physicalAuditID="+physicalAuditID+"&strOpt="+option+"&"+fnAppendCompanyInfo();
         //frm.hAddnlParam.value = ''
         frm.hDisplayNm.value = 'Outstanding Tags Screen'
         frm.submit();
		 
	 
		 
	
}

function fnVoidRecords()
{
	 var frm=document.frmLockTag;
	 TotalRows = <%=totalRows%>;
	 
	 if(frm.haction.value=="MissingTag")
	 {
	 if(frm.Chk_selectAll!=null && parseInt(TotalRows)>0)
	 {
	 frm.Chk_selectAll.checked=true;
	 }
	 if(document.frmLockTag.strFlgShowVoidRec!=null && document.frmLockTag.strFlgShowVoidRec.checked==true)
	 {
		 document.getElementById("divSelectAll").style.visibility = 'hidden';
		 frm.actionType.disabled=true;
		 frm.Chk_selectAll.disabled=true;
		 frm.Btn_Submit.disabled=true;
		 
	 }else
	 {
		 document.getElementById("divSelectAll").style.visibility = 'visible'; 
		 frm.actionType.disabled=false;
		 frm.Chk_selectAll.disabled=false;
		 frm.Btn_Submit.disabled=false;
		 
	 }
	 for (var i=0;i<TotalRows ;i++)
	{
		 totalTags=totalTags+"~"+i+"~";	
	}
	 strTagids=totalTags;
	}
	
}

function isNumeric(value) {
	  if (value != null && !value.toString().match(/^[-]?\d*?\d*$/)) return false;
	  return true;
	}

function fnGetCount(value)
{
	
	
	obj = eval("document.frmLockTag.Chk_tagid"+value);		
	if (obj.checked == true){
		if(strTagids.indexOf(('~'+value+'~'))==-1)
		{
		strTagids=strTagids+"~"+value+"~";		
		}
				
		}
	else {
		strTagids=strTagids.replace(("~"+value+"~"),"");
		
	}


	
	
	if(strTagids.length==totalTags.length)
	{
		document.frmLockTag.Chk_selectAll.checked=true;
		
	}
	else
	{
		document.frmLockTag.Chk_selectAll.checked=false;
		
	}
	
	
}

function fnSelectAll(){
	  TotalRows = <%=totalRows%>;
	 for (var i=0;i<TotalRows ;i++ )
		{
			obj = eval("document.frmLockTag.Chk_tagid"+i);	
			obj.checked = document.frmLockTag.Chk_selectAll.checked;		 
		}
		if(document.frmLockTag.Chk_selectAll.checked)
	 		strTagids=totalTags;
		else
			strTagids="";
			
	}




</script>
 
</HEAD>

<bean:define id="haction" name="frmLockTag" property="haction" type="java.lang.String"></bean:define>

<BODY leftmargin="20" topmargin="10"  >
 
<html:form action="/gmIssueTag.do"  >
<html:hidden property="strOpt"/>
<html:hidden property="haction" value="<%=haction%>"/>  
<html:hidden property="includeTags" value=""/> 
<html:hidden property="excludeTags" value=""/> 
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VOSTAG"/>
<html:hidden property="hCancelReturnVal" value="true"/>
<html:hidden property="hRedirectURL" />
<html:hidden property="hAddnlParam" />
<html:hidden property="hDisplayNm" />

 <table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
  <logic:notEqual name="frmLockTag" property="strOpt" value="MissingTag"> 
		<tr>
			<td colspan="5" height="20" class="RightDashBoardHeader">&nbsp;<fmtPATag:message key="LBL_AUTO_GENERATE_TAGS"/></td>	 
			<td  height="20" class="RightDashBoardHeader" align="right">
			<fmtPATag:message key="IMG_HELP" var="varHelp"/>
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>	
		
 </logic:notEqual> 	
  <logic:equal name="frmLockTag" property="strOpt" value="MissingTag"> 
		<tr>
			<td colspan="5" height="20" class="RightDashBoardHeader">&nbsp;<fmtPATag:message key="LBL_OUTSTANDING_TAGS"/></td>	
			<td  height="20" class="RightDashBoardHeader" align="right">
			<fmtPATag:message key="IMG_HELP" var="varHelp"/>
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitleOut%>');" />
	       </td> 
		</tr>	
 </logic:equal> 	
 	<!-- Custom tag lib code modified for JBOSS migration changes -->				
	<tr>
			<td height="30"  class="RightTableCaption" align = "right">&nbsp;<font color="red">*</font><fmtPATag:message key="LBL_PHYSICAL_AUDIT"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="physicalAuditID" SFFormName="frmLockTag" SFSeletedValue="physicalAuditID"
							SFValue="physicalAuditList" codeId = "CODEID"  codeName = "CODENM" onChange="fetchCountedBy();" defaultValue= "[Choose One]" />
			</td>
			<td height="30" class="RightTableCaption" align = "right">&nbsp;&nbsp; <fmtPATag:message key="LBL_WAREHOUSE"/> :</td>
			<td>&nbsp;<gmjsp:dropdown controlName="locationID" SFFormName="frmLockTag" SFSeletedValue="locationID"
							SFValue="locationList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"/>
			</td>
			<td height="30" class="RightTableCaption" > <fmtPATag:message key="DT_COUNTED_BY"/>:&nbsp;<gmjsp:dropdown controlName="auditUserID" SFFormName="frmLockTag" SFSeletedValue="auditUserID"
							SFValue="auditUserList" codeId="CODEID"  codeName="CODENM"  defaultValue= "[Choose One]"/></td>
			<td> 	&nbsp;	
			</td>		 
		</tr>
		
		<tr><td colspan="6" class="ELine"></td></tr>
		<tr >
			<td height="30" class="RightTableCaption" align = "right"><fmtPATag:message key="LBL_PART_REF_ID"/>:&nbsp;</td>
			<td><html:text property="transactionID"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
			 	 
			<td height="30" class="RightTableCaption" align = "right"> <fmtPATag:message key="LBL_TAG_RANGE"/>: </td>
			<td> &nbsp;<fmtPATag:message key="LBL_FROM"/>:
			<html:text property="tagRangeFrom" size="10"  styleClass="InputArea"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />							
			&nbsp;&nbsp;&nbsp;<fmtPATag:message key="LBL_TO"/>:
			<html:text property="tagRangeTo" size="10"  styleClass="InputArea"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />		
			&nbsp;&nbsp;		
			</td>	
			<logic:equal name="frmLockTag" property="haction" value="MissingTag"> 
			<td class="RightTableCaption"><html:checkbox property="strFlgShowVoidRec" onclick="fnVoidRecords()"/>&nbsp;<fmtPATag:message key="LBL_SHOW_ONLY_VOIDED_RECORDS"/>  &nbsp;&nbsp; <fmtPATag:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnGo();" buttonType="Load" /></td>
			
			</logic:equal>
			<logic:notEqual name="frmLockTag" property="haction" value="MissingTag"> 
			<td class="RightText" >&nbsp;<fmtPATag:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnGo();" buttonType="Load" />&nbsp;</td>
			</logic:notEqual>
			<td >&nbsp;</td>
		</tr>	
		
		
		 <logic:equal name="frmLockTag" property="strOpt" value="reload"> 
 	 <logic:notEqual name="frmLockTag" property="haction" value="MissingTag"> 
 <tr>
  <td align="left"><fmtPATag:message key="LBL_SELECT_ALL"/> <input type="checkbox" name="Chk_selectAll" onClick="fnSelectAll();"></td>
 </tr> 	 	
<tr>
		<td  colspan="6" width="100%">
		<display:table name="requestScope.frmLockTag.returnList" export="true" class="its" requestURI="/gmIssueTag.do"  id="currentRowObject" decorator="com.globus.accounts.displaytag.beans.DTPrintTagWrapper">
		<display:setProperty name="export.excel.filename" value="AutoGenerate Tag.xls" /> 
		<fmtPATag:message key="DT_TAG_ID" var="varTagId"/><display:column property="TAGID"  title="${varTagId}" class="alignleft"  sortable="true" style="width:115px"/>
					<fmtPATag:message key="DT_PART" var="varPart"/><display:column property="PNUM" title="${varPart}" class="alignleft" sortable="true" style="width:100px" />
					<fmtPATag:message key="DT_REF_ID" var="varRefID"/><display:column property="REFID" title="${varRefID}" class="alignleft" sortable="true" style="width:115px" />
					<fmtPATag:message key="DT_TRANSACTION_DESC" var="varTransactionDesc"/><display:column property="TXNDESC" title="${varTransactionDesc}" class="alignleft" />
					<fmtPATag:message key="LBL_WAREHOUSE" var="varWarehouse"/><display:column property="LOCATIONNM" title="${varWarehouse}" class="alignleft" />
					<fmtPATag:message key="DT_COUNTED_QTY" var="varCountedQty"/><display:column property="QTY" title="${varCountedQty}" class="alignleft" />
					<fmtPATag:message key="DT_COUNTED_BY" var="varCountedBy"/><display:column property="COUNTED_BY" title="${varCountedBy}" class="alignleft" />
					<fmtPATag:message key="DT_UPDATED_BY" var="varUpdatedBy"/><display:column property="LASTUPDATEDBY" title="${varUpdatedBy}" class="alignleft" />
					 
		</display:table>
		</td>
		</tr> 
		 
  <tr> <td  colspan=6 align="center" height="30">
 <fmtPATag:message key="LBL_CHOOSE_ACTION"/>: <gmjsp:dropdown controlName="actionType" SFFormName="frmLockTag" SFSeletedValue="actionType"
							SFValue="alType" codeId="CODENM"  codeName="CODENM"  defaultValue= "[Choose One]"/>&nbsp;&nbsp;
  &nbsp;
  <logic:equal name="frmLockTag" property="strEnableSubmit" value="false">
  <fmtPATag:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" disabled="true" name="Btn_Sumbit" gmClass="button" onClick="fnSubmit();" buttonType="Save" />
  </logic:equal>
  <logic:equal name="frmLockTag" property="strEnableSubmit" value="true">
  <fmtPATag:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" name="Btn_Sumbit" gmClass="button" onClick="fnSubmit();" buttonType="Save" />
  </logic:equal>
   </td> </tr>
</logic:notEqual>	
</logic:equal>
<logic:equal name="frmLockTag" property="reloadMiss" value="ReloadMiss"> 
<logic:equal name="frmLockTag" property="haction" value="MissingTag"> 
 <logic:notEqual name="frmLockTag" property="strFlgShowVoidRec" value="on"> 
 <tr>
  <td align="left"> <div id="divSelectAll"><fmtPATag:message key="LBL_SELECT_ALL"/> <input type="checkbox" name="Chk_selectAll" onClick="fnSelectAll();"/> </div></td>
 </tr>
 </logic:notEqual> 
 <tr><td height="10"></td></tr>
 
<tr>
		<td  colspan="6" width="100%">
		<display:table name="requestScope.frmLockTag.returnList" export="true" class="its" requestURI="/gmIssueTag.do" id="currentRowObject" decorator="com.globus.accounts.displaytag.beans.DTPrintTagWrapper" >
         <display:setProperty name="export.excel.filename" value="Outstanding Tag.xls" />    
		<display:column property="TID" title="Tag ID" class="alignleft"  sortable="true" style="width:115px"/>
					
					<fmtPATag:message key="DT_PART" var="varPart"/><display:column property="PNUM" title="${varPart}" class="alignleft" sortable="true" style="width:100px"/>
					<fmtPATag:message key="DT_REF_ID" var="varRefID"/><display:column property="REFID" title="${varRefID}" class="alignleft" sortable="true" style="width:115px"/>
					<fmtPATag:message key="DT_TRANSACTION_DESC" var="varTransactionDesc"/><display:column property="TXNDESC" title="${varTransactionDesc}" class="alignleft" />
					<fmtPATag:message key="LBL_WAREHOUSE" var="varWarehouse"/><display:column property="LOCATIONNM" title="${varWarehouse}" class="alignleft" />
					<fmtPATag:message key="DT_COUNTED_QTY" var="varCountedQty"/><display:column property="QTY" title="${varCountedQty}" class="alignleft" />
					<fmtPATag:message key="DT_COUNTED_BY" var="varCountedBy"/><display:column property="COUNTED_BY" title="${varCountedBy}" class="alignleft" />
					<fmtPATag:message key="DT_UPDATED_BY" var="varUpdatedBy"/><display:column property="LASTUPDATEDBY" title="${varUpdatedBy}" class="alignleft" />
					<fmtPATag:message key="DT_UPDATED_DATE" var="varUpdatedDate"/><display:column property="LASTUPDATEDDT" title="${varUpdatedDate}" class="alignleft" format= "<%=strDateFmt%>"/>
					
		</display:table>
		</td>
		</tr> 	
		
	<tr><td height="10"></td></tr>	
<logic:notEqual name="frmLockTag" property="strFlgShowVoidRec" value="on">					
	<tr><td height="30" class="RightTableCaption"> </td>
	<td> 	</td>	
			<td class="RightText" >&nbsp;</td>
			<td height="30" ><fmtPATag:message key="LBL_CHOOSE_ACTION"/> : <gmjsp:dropdown controlName="actionType" SFFormName="frmLockTag" SFSeletedValue="actionType"
							SFValue="alAction" codeId="CODEID"  codeName="CODENM"  defaultValue= "[Choose One]"/> </td>
	<logic:equal name="frmLockTag" property="strEnableVoid" value="false">
	<td> <fmtPATag:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="Btn_Submit" value="&nbsp;${varSubmit}&nbsp;" disabled="true" gmClass="button" onClick="return fnVoidTag();" buttonType="Save" />		
			</td>
	</logic:equal>
	<logic:equal name="frmLockTag" property="strEnableVoid" value="true">
	<td> <fmtPATag:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="Btn_Submit" value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="return fnVoidTag();" buttonType="Save" />		
			</td>
	</logic:equal>	
			<td class="RightText" ></td>
			</tr>
</logic:notEqual>
</logic:equal>
</logic:equal>
 </table>
 
  </html:form>
   <%@ include file="/common/GmFooter.inc" %>
  
  
</BODY>

</HTML>
