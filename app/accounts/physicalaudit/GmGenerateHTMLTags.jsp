<%
/**********************************************************************************
 * File		 		: GmGenerateHTMLTags.jsp
 * Desc		 		: This screen is used to display Issue or missing Tag Count
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>

<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ taglib prefix="fmtGenerateHTMLTags" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \accounts\physicalaudit\GmGenerateHTMLTags.jsp -->
<fmtGenerateHTMLTags:setLocale value="<%=strLocale%>"/>
<fmtGenerateHTMLTags:setBundle basename="properties.labels.accounts.physicalaudit.GmGenerateHTMLTags"/>

<HTML>
<HEAD>
<TITLE> Globus Medical: Generate HTML files for Tags</TITLE>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>	
 <script>
	 function fnSubmit()
	 {
		 var type=document.frmLockTag.actionType.value;
	  		if(type=="0")
	  		{
	  			Error_Details(message_accounts[501]);
	  		}
	  		if(document.frmLockTag.locationID.value =='0'){
	  			Error_Details(message_accounts[502]);
	  		}
	  		if(document.frmLockTag.splitSize.value ==''){
	  			Error_Details(message_accounts[503]);
	  		}
	  		if(document.frmLockTag.splitSize.value !='' && document.frmLockTag.splitSize.value <500){
	  			Error_Details(message_accounts[504]);
	  		}
	  		if (ErrorCount > 0)  
	  		{
	  				Error_Show();
	  				Error_Clear();
	  				return false;
	  		}
	     var objDivStyle2 = eval('document.all.pgbar.style');
  		 objDivStyle2.display = 'block';
	     document.frmLockTag.strOpt.value =type;	 
	     document.frmLockTag.Btn_Sumbit.disabled = true;// To disable submit button
	     document.frmLockTag.submit();
	 }
 </script>
</HEAD>


<BODY leftmargin="20" topmargin="10" >
 
<html:form action="/gmGenerateHTMLTags.do"  >
<html:hidden property="strOpt" />

 <table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">   
		<tr>
			<td colspan="6" height="20" class="RightDashBoardHeader">&nbsp;<fmtGenerateHTMLTags:message key="LBL_GENERATE_HTML_TAGS"/>
			</td>	 
		</tr>	 	
		<!-- Custom tag lib code modified for JBOSS migration changes -->				
	<tr>
			<td height="30"  class="RightTableCaption" align="right">&nbsp;<font color="red">*</font>&nbsp;<fmtGenerateHTMLTags:message key="LBL_PHYSICAL_AUDIT"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="physicalAuditID" SFFormName="frmLockTag" SFSeletedValue="physicalAuditID"
							SFValue="physicalAuditList" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"/>
					&nbsp;&nbsp;&nbsp;&nbsp;<b> <fmtGenerateHTMLTags:message key="LBL_WAREHOUSE"/> : </b> 		
					&nbsp;<gmjsp:dropdown controlName="locationID" SFFormName="frmLockTag" SFSeletedValue="locationID"
							SFValue="locationList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"/>
			</td>						
		</tr>
		
		<tr><td colspan="6" class="ELine"></td></tr>
		<tr>						 	 
			<td height="30" class="RightTableCaption" align="right"> <fmtGenerateHTMLTags:message key="LBL_TAG_RANGE"/>: </td>
			<td> &nbsp;<fmtGenerateHTMLTags:message key="LBL_FROM"/>:
			<html:text property="tagRangeFrom" size="10"  styleClass="InputArea"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />							
			&nbsp;&nbsp;&nbsp;<fmtGenerateHTMLTags:message key="LBL_TO"/>:
			<html:text property="tagRangeTo" size="10"  styleClass="InputArea"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />		
			&nbsp;&nbsp;&nbsp;&nbsp;  <b> <fmtGenerateHTMLTags:message key="LBL_SPLIT_SIZE"/> : </b> <html:text property="splitSize" size="10"  styleClass="InputArea"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
			&nbsp;&nbsp;
			<fmtGenerateHTMLTags:message key="LBL_CHOOSE_ACTION"/>: <gmjsp:dropdown controlName="actionType" SFFormName="frmLockTag" SFSeletedValue="actionType"
							SFValue="alType" codeId="CODENM"  codeName="CODENM"  defaultValue= "[Choose One]"/>&nbsp;&nbsp;
  &nbsp;
  <fmtGenerateHTMLTags:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" name="Btn_Sumbit" gmClass="button" onClick="fnSubmit();" buttonType="Save" />					
			</td>	
		</tr>
		
		<tr id="pgbar" style="display: none">
		<td colspan=5 align="center">	
						<img src="<%=strImagePath%>/progress_bar.gif">
		</td></tr>
			
	 </table>
  
  </html:form>
   <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
