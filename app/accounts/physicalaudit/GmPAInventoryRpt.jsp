 <%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.text.*" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtPAInventoryRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \accounts\physicalaudit\GmPAInventoryRpt.jsp -->
<fmtPAInventoryRpt:setLocale value="<%=strLocale%>"/>
<fmtPAInventoryRpt:setBundle basename="properties.labels.accounts.physicalaudit.GmPAIncludeLockFilter"/>
<%
String strWikiTitle = GmCommonClass.getWikiTitle("VIEW_LOCKED_PRICING");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Inventory Value Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
function fnGo()
{
//	fnValidateTxtFld('tagRangeFrom',' tagRangeFrom ');
//	fnValidateTxtFld('tagRangeTo',' tagRangeTo ');
 /*  fnValidateDropDn('locationID',' warehouse ');
		
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}*/
	
	document.frmLockTag.strOpt.value='Fetch';
	document.body.style.cursor = 'wait';
	fnStartProgress();
	document.frmLockTag.submit();
}

function fetchCountedBy(){
//chk if value is not 0 then submit
	document.frmLockTag.strOpt.value='Load';
	document.frmLockTag.submit();
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10"  >
<html:form action="/gmPAInventoryRpt.do" >

<input type="hidden" name="strOpt" >

<table cellspacing="0" cellpadding="0"  class="DtTable1000">
		<tr>
			
			<td  height="1" bgcolor="#666666" colspan="2"></td>
			
		</tr>
		<tr>
			<td  height="25" class="RightDashBoardHeader"><fmtPAInventoryRpt:message key="LBL_INVENTORY_VALUE_REPORT"/>  </td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtPAInventoryRpt:message key="IMG_HELP" var="varHelp"/>
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		<tr><td class="LLine" height="1" colspan="2"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td  class="RightTableCaption" align="left" HEIGHT="24" colspan="2">&nbsp;<font color="red">*&nbsp;</font><fmtPAInventoryRpt:message key="LBL_PHYSICAL_AUDIT"/>:
			
			&nbsp;<gmjsp:dropdown controlName="physicalAuditID"  SFFormName='frmLockTag' SFSeletedValue="physicalAuditID"  	
						tabIndex="1"  SFValue="physicalAuditList" codeId="CODEID" codeName="CODENM" onChange="fetchCountedBy();" defaultValue= "[Choose One]"/>
			</td>
			
		</tr>
		<tr class="Shade"><td colspan="2" class="LLine"></td></tr>
		<tr>
			<td height="30" class="RightTableCaption">&nbsp;&nbsp;&nbsp;&nbsp;<fmtPAInventoryRpt:message key="LBL_WAREHOUSE"/>: 
			&nbsp;<gmjsp:dropdown controlName="locationID" SFFormName="frmLockTag" SFSeletedValue="locationID"
							SFValue="locationList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"/>
			</td>
			<td height="30" class="RightTableCaption"><fmtPAInventoryRpt:message key="LBL_PROJECT_LIST"/>:
			 <gmjsp:dropdown controlName="projectListID" SFFormName="frmLockTag" SFSeletedValue="projectListID"
							SFValue="projectList" codeId = "ID"  codeName = "NAME"  defaultValue= "[All Projects]"/> 		
				&nbsp;&nbsp;&nbsp;&nbsp;<fmtPAInventoryRpt:message key="BTN_GO" var="varGo"/><gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" name="Btn_Go"  gmClass="button" onClick="javascript:fnGo()" buttonType="Load" />
			</td> 
		</tr>	
					
		
		<tr>
			<td colspan="2">
					<display:table name="requestScope.frmLockTag.returnList" class="its"
						id="currentRowObject" requestURI="/gmPAInventoryRpt.do?" style="height:35" export="true" varTotals="totals">
                        <display:setProperty name="export.excel.filename" value="Inv Value Report.xls" />
							<fmtPAInventoryRpt:message key="DT_PART" var="varPart"/><display:column property="PNUM" title="${varPart}" style="width:40" sortable="true" />							
							<fmtPAInventoryRpt:message key="LBL_WAREHOUSE" var="varWarehouse"/><display:column property="WAREHOUSE" title="${varWarehouse}"  style="width:200" sortable="true" />
							<fmtPAInventoryRpt:message key="DT_PART_DESCRIPTION" var="varPartDescription"/><display:column property="PDESC" escapeXml="true" title="${varPartDescription}"  style="width:510" sortable="true" />
							<fmtPAInventoryRpt:message key="DT_LOCKED_QTY" var="varLockedQty"/><display:column property="LOCK_QTY" title="${varLockedQty}" style="width:80" class="alignright" sortable="true" total="true"/>
							<fmtPAInventoryRpt:message key="DT_ADJUSTED_QTY" var="varAdjustedQty"/><display:column property="ADJUSTMENT_QTY" title="${varAdjustedQty}" style="width:90" class="alignright" sortable="true" total="true"/>							
							<fmtPAInventoryRpt:message key="DT_LOCKED_COST" var="varLockedCost"/><display:column property="COGS_AMT" title="${varAdjustedQty}" style="width:80" class="alignright"  sortable="true" format="{0,number,#,###,###.00}" total="true"/>
							<fmtPAInventoryRpt:message key="DT_LOCKED_EXT_COST" var="varLockedExtCost"/><display:column property="LOCK_COSG" title="${varLockedExtCost}" style="width:110" class="alignright" sortable="true" format="{0,number,#,###,###.00}" total="true"/>
							<fmtPAInventoryRpt:message key="DT_ADJUSTED_VALUE" var="varAdjustedValue"/><display:column property="ADJUSTMENT_COSG" title="${varAdjustedValue}" style="width:110" class="alignright" format="{0,number,#,###,###.00}" sortable="true" total="true"/>
							<fmtPAInventoryRpt:message key="DT_IN_VALUE_AFT_RECON" var="varInvValueaftRecon"/><display:column property="TOTAL_COSG" title="${varInvValueaftRecon}" style="width:110" class="alignright" sortable="true" format="{0,number,#,###,###.00}" total="true"/>
					<display:footer media="html"> 
					<%
					String strVal ;
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column4").toString();
					String strLockqty = "" + GmCommonClass.getStringWithCommas(strVal,0);
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column5").toString();
					String strAdjustmentqty = "" + GmCommonClass.getStringWithCommas(strVal,0);
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column6").toString();
					String strCost="" + GmCommonClass.getStringWithCommas(strVal,0);
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column7").toString();
					String strLlockcos="" + GmCommonClass.getStringWithCommas(strVal,0);
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column8").toString();
					String strAdjustmentcos="" + GmCommonClass.getStringWithCommas(strVal,0);
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column9").toString();
					String strTotalcos="" + GmCommonClass.getStringWithCommas(strVal,0);					
					%>
				<tr class = shade>
		  		<td colspan="2"> <B> <fmtPAInventoryRpt:message key="LBL_TOTAL_BILL"/>: </B></td>
		  		<td class = "alignright" > </td> 
		  		<td class = "alignright" ><B><%=strLockqty%></B></td>
  		  		<td class = "alignright"> <B><%=strAdjustmentqty%></B></td>
    	    	<td class = "alignright"> <B><%=strCost%></B></td>
	    	 	<td class = "alignright"> <B><%=strLlockcos%></B></td>
    	 		<td class = "alignright"> <B><%=strAdjustmentcos%></B></td>
    	 		<td class = "alignright"> <B><%=strTotalcos%></B></td>
  				</tr>
  			</display:footer>
					</display:table> 

					 </td>
					</tr>
					
  		
    </table>		     	
		
		
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
