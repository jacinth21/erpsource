

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtICTSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmICTSummary.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtICTSummary:setLocale value="<%=strLocale%>"/>
<fmtICTSummary:setBundle basename="properties.labels.accounts.GmICTSummary"/>

<html>
<head>
<title>Consignment Details </title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>
<script>
function fnPrintInvoice(varinv)
{
windowOpener('/GmInvoiceInfoServlet?hAction=Print&hInv='+varinv,"PrntInv","resizable=yes,scrollbars=yes,top=150,left=200,width=750,height=500");
}

function fnViewOrder(val)
{
	windowOpener('/GmEditOrderServlet?hMode=PrintPrice&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=760,height=410");	
}



</script>

<BODY leftmargin="20" topmargin="10" >
	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td width="848" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
					<td colspan="2">
					<display:table name="requestScope.ICTRESULT.rows" class="its"
						id="currentRowObject"  style="height:35" decorator="com.globus.accounts.displaytag.DTICTSummaryWrapper">
							<fmtICTSummary:message key="LBL_TRANS" var="varTrans"/><display:column property="CONSIGNID" title="${varTrans} #" group="1" style="width:260"/>							
							<fmtICTSummary:message key="LBL_DATE_OF_SHIPMENT" var="varDateOfShipment"/><display:column property="SHIPDATE" title="${varDateOfShipment}" group="2" style="width:220"/>
							<fmtICTSummary:message key="LBL_TRANS_BY" var="varTransBy"/><display:column property="TRANSBY" title="${varTransBy}" group="3" style="width:220"/>
							<fmtICTSummary:message key="LBL_INVOICE_ID" var="varInvoiceId"/><display:column property="INVOICEID" title="${varInvoiceId}" style="width:340"/>
							<fmtICTSummary:message key="LBL_PO" var="varPO"/><display:column property="POID" title="${varPO}#"  style="width:220"/>
							<fmtICTSummary:message key="LBL_INVOCIE_TYPE" var="varInvoiceType"/><display:column property="INVTYPE" title="${varInvoiceType}" style="width:220"/>																					
							<fmtICTSummary:message key="LBL_CURRENCY" var="varCurency"/><display:column property="CURRENCY" title="${varCurency}" style="width:220"  />
							<fmtICTSummary:message key="LBL_INVOICE_AMT" var="varInvoiceAmt"/><display:column property="INVAMT" title="${varInvoiceAmt}" style="width:220" class="alignright" format="{0,number,#,###,###.00}"/>							
							<!-- display:column property="ORDERID" title="Order id<br>/TRANSID" style="width:220"/-->
							<fmtICTSummary:message key="LBL_ACCOUNT_DIST" var="varAccountDist"/><display:column property="ACCTYPE" title="${varAccountDist}" style="width:220"/>
					</display:table>  </td>
					</tr>
			   	</table>
  			   </td>
  		  </tr>	
    </table>		     	


</BODY>

</HTML>
