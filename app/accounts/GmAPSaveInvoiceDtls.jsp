<!--
/**********************************************************************************
 * File		 		: GmAPSaveInvoiceDtls.jsp
 * Desc		 		: AP Invoice Details
 * Version	 		: 1.0
 * author			: Basudev Vidyasankar 
************************************************************************************/
-->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtApSaveInoviceDtls" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmAPSaveInvoiceDtls.jsp -->
<fmtApSaveInoviceDtls:setLocale value="<%=strLocale%>"/>
<fmtApSaveInoviceDtls:setBundle basename="properties.labels.accounts.GmAPSaveInvoiceDtls"/>
<bean:define id="alAddlChrgType" name="frmAPInvoiceDtls" property="alAddlChrgType" scope="request" type="java.util.ArrayList"></bean:define>
<bean:define id="lDHR" name="frmAPInvoiceDtls" property="ldtDHR" scope="request" type="java.util.List"></bean:define>
<bean:define id="lAddlChrg" name="frmAPInvoiceDtls" property="ldtAddlChrg" scope="request" type="java.util.List"></bean:define>
<bean:define id="paymentAmt" name="frmAPInvoiceDtls" property="paymentAmt" scope="request" type="java.lang.String"></bean:define>
<bean:define id="paymentId" name="frmAPInvoiceDtls" property="hpaymentId" scope="request" type="java.lang.String"></bean:define>
<bean:define id="hpaymentStatus" name="frmAPInvoiceDtls" property="hpaymentStatus" scope="request" type="java.lang.String"></bean:define>
<bean:define id="strOpt" name="frmAPInvoiceDtls" property="strOpt" scope="request" type="java.lang.String"></bean:define>
<bean:define id="hmPODetails" name="frmAPInvoiceDtls" property="hmpurchaseOrdDtls" scope="request" type="java.util.HashMap"></bean:define>
<bean:define id="hpoNumber" name="frmAPInvoiceDtls" property="hpoNumber" scope="request" type="java.lang.String"></bean:define>

<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.List,java.text.DecimalFormat" %>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%
String strAccountJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS");	
try{
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	int intTRCount = 2;
	int intSize = 0;
	int intAlSize = 0;
	String strTotal = "";
	HashMap hcboVal = new HashMap();

	ArrayList alAddl = (ArrayList) alAddlChrgType;
	int iAlSize = 0;
	int ilDHRSize = 0;
	
	if(lAddlChrg != null){
		iAlSize = lAddlChrg.size();
	}
	
	if(lDHR != null){
		ilDHRSize = lDHR.size();
	}
	
	DynaBean dbAddlDtls;
	DynaBean dbDHRDtls;
	
	ArrayList alAddlList = new ArrayList();
	ArrayList alDHRList = new ArrayList();
	
	String strAddl= "";
	
	alAddlList = GmCommonClass.parseNullArrayList((ArrayList) lAddlChrg);
	alDHRList = GmCommonClass.parseNullArrayList((ArrayList) lDHR);
	
	HashMap hmTemp = new HashMap();
	HashMap hmGLNumber =  new HashMap();
	HashMap hmCurrency = new HashMap();
	
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strCurrSign =(String)hmCurrency.get("CMPCURRSMB");
	String strApplCurrFmt = (String)hmCurrency.get("CMPCURRFMT");
	
	
	int iAddlLen = alAddlList.size();
	
	for(int i=0;i<alAddlChrgType.size();i++)//For loading the combo box when Add Row button is clicked
	{
		hmTemp = (HashMap)alAddlChrgType.get(i);
		strAddl = strAddl + hmTemp.get("CODENM") + "^" + hmTemp.get("CODEID") + "^" + hmTemp.get("CODENMALT")+ "|";
		hmGLNumber.put(hmTemp.get("CODEID"),hmTemp.get("CODENMALT"));
	}
	
	if(iAddlLen < intTRCount)
	{
		iAddlLen = intTRCount;
	}
	
	//calculating total amounts
	float dAddlTotal = 0.0F;
	for(int i=0;i<iAlSize;i++){
		dbAddlDtls = (DynaBean)alAddlList.get(i);
		dAddlTotal = dAddlTotal + Float.parseFloat(String.valueOf(dbAddlDtls.get("ADDLAMOUNT")));
	}
	
	float dReadyTotal = 0.0F;
	float dDHRReadyTotal = 0.0F;
	float dDHRPendingTotal = 0.0F;
	String strPymtId = "";
	int iDHRFl = 0;
	int iSize = 0;
	
	for(int i=0;i<ilDHRSize;i++){
		dbDHRDtls = (DynaBean)alDHRList.get(i);
		Object obj = dbDHRDtls.get("PID");
		if(obj != null)
			strPymtId = obj.toString();
		if(!strPymtId.equals("")){
			iDHRFl = Integer.parseInt(String.valueOf(dbDHRDtls.get("SFL")));
			if(iDHRFl == 4){
				dDHRReadyTotal = dDHRReadyTotal + Float.parseFloat(String.valueOf(dbDHRDtls.get("SHELFCOST")));
			}	
			else{
				dDHRPendingTotal = dDHRPendingTotal + Float.parseFloat(String.valueOf(dbDHRDtls.get("RECCOST")));
			}	
		}
		strPymtId = "";
	}
	iSize = ilDHRSize + 7;
	
	dReadyTotal = dDHRReadyTotal + dAddlTotal;
	
	float dInvoiceAmount = 0.0F;
	float dBalance = 0.0F;
	
	if(!paymentAmt.equals("") && paymentAmt != null)
		dInvoiceAmount = Float.parseFloat(paymentAmt);
	
	dBalance =  dInvoiceAmount - dDHRReadyTotal - dDHRPendingTotal - dAddlTotal ; 
	 
	DecimalFormat moneyFormat = new DecimalFormat();
	
	moneyFormat = new DecimalFormat(strApplCurrFmt); //$NON-NLS-1$
	
	String strInvoiceAmount = moneyFormat.format(dInvoiceAmount);
	String strPPVImplants = moneyFormat.format(0);
	String strPPVInstruments = moneyFormat.format(0);
	String strReadyTotal = moneyFormat.format(dReadyTotal);
	//log.debug("DBBalance...."+dBalance);
	String strBalance = moneyFormat.format(dBalance);
	//log.debug("DBBalance...."+strBalance);
	String strAddlTotal = moneyFormat.format(dAddlTotal);
	String strDHRReadyTotal = moneyFormat.format(dDHRReadyTotal);
	String strDHRPendingTotal = moneyFormat.format(dDHRPendingTotal);
	
	String strVoidBtnDis = "";
	String strSaveBtnDis = "";
	String strAddRowBtnDis = "";
	String strGoBtnDis = "";
	
	String strStyleHidden = "";
	String strDis = "";
	
	if(hpaymentStatus.equals("20")||hpaymentStatus.equals("30")){
		strVoidBtnDis = "true";
		strSaveBtnDis = "true";
		strAddRowBtnDis = "true";
		strGoBtnDis = "true";
	}else if(hpaymentStatus.equals("")){
		strVoidBtnDis = "true";
	}
	
	if(strOpt.equals("viewInvc")){
		strStyleHidden = "visibility:hidden;";
		strDis = "disabled=true";
	}
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	String strVendorName = GmCommonClass.parseNull((String)hmPODetails.get("VNAME"));
	String strCurrency = GmCommonClass.parseNull((String)hmPODetails.get("CURR"));
	String strSesnDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt);
	
	
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();
	
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strAccountJsPath%>/GmDHRPayment.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<title>Post DHR Payment</title>
<style type="text/css">
	#dhtmltooltip{
	position: absolute;
	border: 1px solid red;
	width: 150px;
	padding: 2px;
	background-color: lightyellow;
	visibility: hidden;
	z-index: 100;
	filter: progid:DXImageTransform.Microsoft.Shadow(color=gray,direction=115);
	}
</style>
</head>
<body>
<html:form action="/gmAPSaveInvoiceDtls.do">
<html:hidden property="strOpt"/>
<html:hidden property="hpaymentId"/>
<html:hidden property="hpoNumber"/>
<html:hidden property="hdhrId"/>
<html:hidden property="haddlChrgId"/>
<html:hidden property="hvendorId"/>
<input type="hidden" name="hTxnId"/>
<input type="hidden" name="hCancelType"/>
<input type="hidden" name="hAction"/>
<input type="hidden" name="hIlDHRSize" value="<%=ilDHRSize%>" />
<input type="hidden" name="hTotalBalance" value="<%=dBalance%>"/>
<input type="hidden" name="hReadyTotal" value="<%=dReadyTotal%>"/>
<input type="hidden" name="hDHRReadyTotal" value="<%=dDHRReadyTotal%>"/>
<input type="hidden" name="hDHRPendingTotal" value="<%=dDHRPendingTotal%>"/>
<input type="hidden" name="hAddlTotalValue" value="<%=dAddlTotal%>"/>
<input type="hidden" name="hInvoiceAmount" value="<%=dInvoiceAmount%>"/>
<input type="hidden" name="hcnt" value="<%=iAddlLen%>"/>
<input type="hidden" name="hstraddl" value="<%=strAddl%>"/>
<input type="hidden" name="sessionDt" value="<%=strSesnDate%>"/>
		<table border="0" class="DtTable1055" cellspacing="0" cellpadding="0" style="WIDTH: 1150px">
				<tr>
					<td colspan="6" height="15" align="left" class="RightDashBoardHeader">&nbsp;<fmtApSaveInoviceDtls:message key="LBL_INVOICE_DETAILS"/></td>
				</tr>
				<tr><td colspan="6" class="Line"></td></tr>
				<tr>
					<td class="RightTableCaption" align="right" height="30"><fmtApSaveInoviceDtls:message key="LBL_VENDOR_NAME"/>:&nbsp;</td>
					<td colspan="3"  align="left">&nbsp;<%=strVendorName%></td>
  					<td class="RightTableCaption" align="right" height="30">&nbsp;<fmtApSaveInoviceDtls:message key="LBL_CURRENCY"/></td>
					<td colspan="3" align="left"><b><%=strCurrency%></b></td>
				</tr>
				<tr><td colspan="6" class="Line"></td></tr>				
				<tr>
					<logic:notEqual name="frmAPInvoiceDtls" property="strOpt" value="viewInvc">
						<tr class="shade">
							<td class="RightTableCaption" align="right" height="30"><fmtApSaveInoviceDtls:message key="LBL_PO_NUMBER"/>:&nbsp;</td>
							<td id="Lbl_PONumber" class="RightText" align="left">&nbsp;<a href="javascript:fnViewPO('<%=hpoNumber%>')"> <b><bean:write name="frmAPInvoiceDtls" property="hpoNumber"></bean:write></b></a></td>
							<td class="RightTableCaption" align="right" height="30"><fmtApSaveInoviceDtls:message key="LBL_INVOICE_NUMBER"/>:&nbsp;</td>
							<td id="Lbl_PONumber" class="RightText" align="left">&nbsp;<b><bean:write name="frmAPInvoiceDtls" property="invoiceId"></bean:write></b></td>				  			
							<td class="RightTableCaption" align="right" height="30"><fmtApSaveInoviceDtls:message key="LBL_SHOW_OPEN_DHR"/>:&nbsp;</td>
							<td align="left">
				   		            <html:checkbox  property="showOpenDHRFlag" tabindex = "-1"/>
								    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								    <fmtApSaveInoviceDtls:message key="LBL_GO" var="varGo"/>
								    <gmjsp:button value="${varGo}" name="Btn_FetchDHR" gmClass="Button" onClick="fnInvoiceGo(this.form);" tabindex = "-1" disabled="<%=strGoBtnDis%>" buttonType="Load" />
				  			</td>
						</tr>
							<tr><td colspan="6" class="Line"></td></tr>
						<!-- Custom tag lib code modified for JBOSS migration changes -->
						<tr>
							<td class="RightTableCaption" align="right" height="30"><fmtApSaveInoviceDtls:message key="LBL_INVOICE_NUMEBR"/>:&nbsp;</td>
							<td class="RightText" align="left">
				   		            <html:text property="invoiceId"  size="10" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex = "3"/>
							</td>					
							<td class="RightTableCaption" align="right" height="30"><fmtApSaveInoviceDtls:message key="LBL_INVOICE_DATE"/>:&nbsp;</td>
							<td>
							<gmjsp:calendar SFFormName="frmAPInvoiceDtls" controlName="invoiceDt"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="4"/>
							</td>
							<td class="RightTableCaption" align="right" height="30"><fmtApSaveInoviceDtls:message key="LBL_INVOICE_AMOUNT"/>:&nbsp;</td>
							<td  align="left">
				   		            <html:text property="paymentAmt"  size="15" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="javascript:fnFillInvoiceAmt(this,this.form)" tabindex = "5"/>
				  			</td>
						</tr>
						<div id="dhtmltooltip"></div>
						<tr><td colspan="6" class="ELine"></td></tr>		
						<!-- Struts tag lib code modified for JBOSS migration changes -->
						<tr class="shade">
							<td class="RightTableCaption" align="right" HEIGHT="20"></font><fmtApSaveInoviceDtls:message key="LBL_DETAILS"/>:&nbsp;</td>
							<td colspan="5" align="left">
							<html:textarea property="invoiceComments" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" rows="2" cols="90" tabindex = "6" ></html:textarea></td>
						</tr>
												<tr><td colspan="6" class="ELine"></td></tr>		
						<!-- Struts tag lib code modified for JBOSS migration changes -->
						<tr class="shade">
							<td class="RightTableCaption" align="right" HEIGHT="20"></font><fmtApSaveInoviceDtls:message key="LBL_INT_COMMENTS"/>:&nbsp;</td>
							<td colspan="5" align="left">
							<html:textarea property="invoiceIntComments" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" rows="2" cols="90" tabindex = "6" ></html:textarea></td>
						</tr>
					</logic:notEqual>
					<logic:equal name="frmAPInvoiceDtls" property="strOpt" value="viewInvc">
						<tr>
							<td class="RightTableCaption" align="right" height="30"><fmtApSaveInoviceDtls:message key="LBL_INVOICE_NUMBER"/>:&nbsp;</td>
							<td class="RightText"  align="left" id="Lbl_InvoiceId"><bean:write name="frmAPInvoiceDtls" property="invoiceId"></bean:write></td>
							<td class="RightTableCaption" align="right" height="30"><fmtApSaveInoviceDtls:message key="LBL_INVOICE_DATE"/>:&nbsp;</td>
							<td class="RightText"  align="left" id="Lbl_InvoiceDt"><bean:write name="frmAPInvoiceDtls" property="invoiceDt"></bean:write></td>
							<td class="RightTableCaption" align="right" height="30"><fmtApSaveInoviceDtls:message key="LBL_INVOICE_AMOUNT"/>:&nbsp;</td>
							<td class="RightText"  align="left" id="Lbl_PaymentAmt"><bean:write name="frmAPInvoiceDtls" property="paymentAmt"></bean:write></td>						
						</tr>
						<tr><td colspan="6" class="ELine"></td></tr>		
						<tr class="shade">
							<td class="RightTableCaption" align="right" HEIGHT="30"></font><fmtApSaveInoviceDtls:message key="LBL_DETAILS"/>:&nbsp;</td>
							<td class="RightText" colspan="5" align="left"><bean:write name="frmAPInvoiceDtls" property="invoiceComments"></bean:write></td>
						</tr>
												<tr><td colspan="6" class="ELine"></td></tr>		
						<tr class="shade">
							<td class="RightTableCaption" align="right" HEIGHT="30"></font><fmtApSaveInoviceDtls:message key="LBL_INT_COMMENTS"/>:&nbsp;</td>
							<td class="RightText" colspan="5" align="left"><bean:write name="frmAPInvoiceDtls" property="invoiceIntComments"></bean:write></td>
						</tr>
					</logic:equal>	
				</tr>
				<tr><td colspan="6" class="Line"></td></tr>				
				<tr>
					<td><input type="checkbox" name="selectAll" onClick="fnSelectAll(<%=ilDHRSize%>,this.form)" <%=strDis%> tabindex = "7"><fmtApSaveInoviceDtls:message key="LBL_SELECT_ALL"/></td>
				</tr>
				<tr>
					<td colspan="6">
						<iframe src="/gmAPSaveInvoiceDtls.do?strDHRListFlag=Y&showOpenDHRFlag=<bean:write name="frmAPInvoiceDtls" property="showOpenDHRFlag"></bean:write>&strOpt=<bean:write name="frmAPInvoiceDtls" property="strOpt"></bean:write>&hpoNumber=<bean:write name="frmAPInvoiceDtls" property="hpoNumber"></bean:write>&hpaymentId=<bean:write name="frmAPInvoiceDtls" property="hpaymentId"></bean:write>&companyInfo=<%=strCompanyInfo %>" 
						        id="DHRListFrame" scrolling="yes" align="left" marginheight="0"  WIDTH="1150" HEIGHT="200"></iframe>
						        <!-- PC-5306 for DHR grid scrolling issue before the scrolling was like this scrolling="no" now we update like this scrolling="yes" -->
					</td>
				</tr>
				<tr>
					<td colspan="6" height="15" align="left" class="RightDashBoardHeader">&nbsp;<fmtApSaveInoviceDtls:message key="LBL_ADDITIONAL_CHARGES"/></td>
				</tr>
				<tr>
					<td colspan="6">
					<div style="overflow:auto;height:82px;">
					<table border="0" cellspacing="0" class="AddlHeader" cellpadding="0" id="addlchrg">
					  <thead>
						<TR colspan="6">
							<TH class="RightText" align="left">&nbsp;#</TH>
							<TH class="RightText" align="left"><img border="0" Alt='Clear Cart' valign="center" src="<%=strImagePath%>/btn_remove.gif" height="10" width="9"></a></TH>
							<TH class="RightText" align="left">&nbsp;&nbsp;<fmtApSaveInoviceDtls:message key="LBL_TYPE"/></TH>
							<TH class="RightText" align="left">&nbsp;<fmtApSaveInoviceDtls:message key="LBL_AMOUNT"/></TH>
							<TH class="RightText" align="center"><fmtApSaveInoviceDtls:message key="LBL_GL_ACCOUNT_NUMBER"/></TH>
						</TR>	
					  </thead>
					  <TBODY>
					  <%
					  	String strType = "";
						String strAmount = "";
						String strGLAccNum = "";
						String strCboAddl = "";
						float dbTotal = 0.0F;
						
						if(intTRCount < iAlSize)
							intTRCount  = iAlSize; //When Additional charges are more than default no. 3 we have to loop that many times.
						
						for (int i=0; i < intTRCount;i++)
						{
							iSize++;
							strCboAddl = "Cbo_AddlChrgType" + i;
							if (i<iAlSize)
							{
								dbAddlDtls = (DynaBean)alAddlList.get(i);
								strType = String.valueOf(dbAddlDtls.get("ADDLTYPE"));
								strAmount = String.valueOf(dbAddlDtls.get("ADDLAMOUNT"));
								strGLAccNum = (String)hmGLNumber.get(strType);
							}
							else
							{
								strType = "";
								strAmount = "";
								strGLAccNum = "";
							}
							dbAddlDtls = null;
		
		                %>
						<tr colspan="6" height="20">
							<td class="RightText">&nbsp;<%=i+1%></td>
							<fmtApSaveInoviceDtls:message key="LBL_REMOVE_FROM_CART" var="varRemoveFromCart"/>
							<td class="RightText" align="left" <%=strDis%>><a href="javascript:fnRemoveItem(<%=i%>,this.form);" tabindex="-1"><img border="0" Alt='${varRemoveFromCart}' valign="left" src="<%=strImagePath%>/btn_remove.gif" height="10" width="9"></a></td>
							<td align="left" <%=strDis%>>&nbsp;<gmjsp:dropdown controlName="<%=strCboAddl%>" seletedValue="<%=strType%>" value="<%=alAddl%>" codeId = "CODEID"   codeName = "CODENM"  defaultValue= "[Choose One]" 
							onChange="fnPopulateGLNumber(this,this.form);" tabIndex="<%=iSize%>" disabled="true"/>
							<td align="left">&nbsp;<input type="text" size="7" value="<%=strAmount%>" class="InputArea" name="Txt_Amt<%=i%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="javascript:fnCalcAddlChrgTotal(this,<%=iAddlLen%>,this.form)" tabindex="<%=iSize + 1%>" <%=strDis%>></td>
							<td  colspan="2" id="Lbl_GLAccNo<%=i%>" class="RightText" align="center" tabindex="-1"><%=strGLAccNum%> </td> 
						</tr>
						<!--<tr><td bgcolor="#eeeeee" height="1" colspan="11"></td></tr>-->
					   <%
						}
						strTotal = ""+dbTotal;
		               %>
					  </TBODY>
					</table>
					</div>
					</td>
				</tr>
			
				<table border="0" class="DtTable1055" cellspacing="0" cellpadding="0" style="WIDTH: 1156px">
						<tr class="shade" height="23">
							<td class="RightText"  align="right" width="46%"><B><fmtApSaveInoviceDtls:message key="LBL_ADDITIONAL_CHARGES"/>:</B>&nbsp;</td>
							<td class="RightCaption"  colspan="4" align="left" id="Lbl_AddlTotal1">&nbsp;<B>&nbsp;<%=strAddlTotal%></B>&nbsp;</td>
							<td  align="left" height="25">
								<!-- add hTotal here -->
								<fmtApSaveInoviceDtls:message key="BTN_ADD_ROW" var="varAddRow"/>
								<gmjsp:button align="center" value="${varAddRow}" gmClass="Button" onClick="javascript:fnAddRow('addlchrg','Cbo_AddlChrgType',this.form);" tabindex="16" buttonType="Save" disabled="<%=strAddRowBtnDis%>" style="<%=strStyleHidden%>" />&nbsp;&nbsp;
							</td>
						</tr>
						<tr><td ></td></tr>
						<tr>
							<td class="RightText"  align="right" width="46%"><B><fmtApSaveInoviceDtls:message key="LBL_PPV_IMPLANT"/>:</B></td>
							<td class="RightCaption"  colspan="" align="left" id="Lbl_PPV_Implants">&nbsp;&nbsp;<%=strPPVImplants%>&nbsp;</td>
						</tr>
						<tr><td ></td></tr>
						<tr>
							<td class="RightText"   align="right" width="46%"><B><fmtApSaveInoviceDtls:message key="LBL_PPV_INSTRUEMENT"/>:</B></td>
							<td class="RightCaption"  colspan="" align="left" id="Lbl_PPV_Instruments">&nbsp;&nbsp;<%=strPPVInstruments%>&nbsp;</td>
						</tr>
						<tr>
							<td class="RightText"  colspan="5" align="right" width="85%"><B><fmtApSaveInoviceDtls:message key="LBL_TOTAL_INVOICE_AMOUNT"/>:</B>&nbsp;</td>
							<td class="RightCaption"  colspan="1" align="right" id="Lbl_InvcTotal">&nbsp;<B>&nbsp;<%=strInvoiceAmount%></B>&nbsp;</td>
						</tr>
						<tr>
							<td class="RightText" colspan="5" align="right" width="85%""><B><fmtApSaveInoviceDtls:message key="LBL_READY_DHR_AMOUNT"/>:</B>&nbsp;</td>
							<td class="RightCaption"  colspan="1" align="right" id="Lbl_DHRReadyAmt">&nbsp;<B> <%=strDHRReadyTotal%></B>&nbsp;</td>
						</tr>
						<tr>
							<td class="RightText" colspan="5" align="right" width="85%""><B><fmtApSaveInoviceDtls:message key="LBL_PENDING_DHR_AMOUNT"/>:</B>&nbsp;</td>
							<td class="RightCaption"  colspan="1" align="right" id="Lbl_DHRPendingAmt">&nbsp;<B> <%=strDHRPendingTotal%></B>&nbsp;</td>
						</tr>
						<tr>
							<td class="RightText" colspan="5" align="right" width="85%""><B><fmtApSaveInoviceDtls:message key="LBL_ADDITIONAL_CHARGES"/>:</B>&nbsp;</td>
							<td class="RightCaption"  colspan="1" align="right" id="Lbl_AddlTotal2">&nbsp;<B> <%=strAddlTotal%></B>&nbsp;</td>
						</tr>
						<tr> 
							<td class="RightText" colspan="5" align="right" width="85%"><B><fmtApSaveInoviceDtls:message key="LBL_READY_FOR_PAYMENT"/>:</B>&nbsp;</td>
							<td class="RightCaption"  colspan="1" align="right" id="Lbl_ReadyTotal">&nbsp;<B>&nbsp;<%=strReadyTotal%></B>&nbsp;</td>
						</tr>
						<tr>
							<td class="RightText" colspan="5" align="right" width="85%""><B><fmtApSaveInoviceDtls:message key="LBL_BALANCE"/>:</B>&nbsp;</td>
							<td class="RightCaption"  colspan="1" align="right" id="Lbl_Balance">&nbsp;<B> <%=strBalance%></B>&nbsp;</td>
						</tr>
						<tr><td colspan="7" class="Line"></td></tr>

						<tr>
						<fmtApSaveInoviceDtls:message key="BTN_VOID_INVOICE" var="varVoidInvoice"/>
						<fmtApSaveInoviceDtls:message key="BTN_SAVE" var="varSave"/>
						<fmtApSaveInoviceDtls:message key="BTN_SAVE_NEW_INVOICE" var="varSaveNewInvoice"/>
		   				    <logic:equal name="frmAPInvoiceDtls" property="strEnableVoidInvoice" value="true">						
							<td  align="right" height="25">
								
								<gmjsp:button name="voidInvoice" value="${varVoidInvoice}" gmClass="Button" buttonType="Save" onClick="javascript:fnVoidInvoice(this.form);" tabindex="15" disabled="<%=strVoidBtnDis%>" style="<%=strStyleHidden%>" />&nbsp;&nbsp;
							</td>
							</logic:equal>
		   				    <logic:equal name="frmAPInvoiceDtls" property="strEnableVoidInvoice" value="false">						
							<td  align="right" height="25">								
								<gmjsp:button name="voidInvoice" disabled="true" value="${varVoidInvoice}" buttonType="Save" gmClass="Button" onClick="javascript:fnVoidInvoice(this.form);" tabindex="15" style="<%=strStyleHidden%>" />&nbsp;&nbsp;
							</td>
							</logic:equal>							
		   				    <logic:equal name="frmAPInvoiceDtls" property="strEnableSaveInvoice" value="true">
								<td  align="left" height="25">		
									<gmjsp:button value="${varSave}"  name= "saveInvoice" buttonType="Save" gmClass="Button" onClick="javascript:fnSaveInvoice(this,this.form,'save');" tabindex="16" disabled="<%=strSaveBtnDis%>" style="<%=strStyleHidden%>" />&nbsp;&nbsp;
								</td>
							</logic:equal>
							<logic:equal name="frmAPInvoiceDtls" property="strEnableSaveInvoice" value="false">
								<td  align="left" height="25">
									<gmjsp:button value="${varSave}"  disabled="true" name= "saveInvoice" buttonType="Save" gmClass="Button" tabindex="16" style="<%=strStyleHidden%>"  />&nbsp;&nbsp;
								</td>
							</logic:equal>
 
							<logic:equal name="frmAPInvoiceDtls" property="strEnableSaveInvoice" value="true">
								<td  align="left" height="25">
									
									<gmjsp:button value="${varSaveNewInvoice}"  name= "saveAndNewInvoice" gmClass="Button" buttonType="Save" onClick="javascript:fnSaveInvoice(this,this.form,'saveAndNew');" tabindex="16" disabled="<%=strSaveBtnDis%>"  style="<%=strStyleHidden%>" />&nbsp;&nbsp;
								</td>
							</logic:equal>
							<logic:equal name="frmAPInvoiceDtls" property="strEnableSaveInvoice" value="false">
								<td  align="left" height="25">
									<gmjsp:button value="${varSaveNewInvoice}"  disabled="true" name= "saveAndNewInvoice" buttonType="Save"  gmClass="Button" tabindex="16" style="<%=strStyleHidden%>" />&nbsp;&nbsp;
								</td>
							</logic:equal>
						
						</tr>
				</table>
		</table>
			
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</body>
</html>
<%
}
catch(Exception e){
	e.printStackTrace();
}
%>