<%
/**********************************************************************************
 * File		 		: GmSecGrpModMapping.jsp
 * Desc		 		: This screen is used for the Module - Security Group Mapping 
 * Version	 		: 1.0
 * author			: Rajkumar J
************************************************************************************/
%>
 <!-- \user\GmSecGrpModMapping.jsp -->
 

<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>

<%@ page import="com.globus.common.servlets.GmServlet"%>

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<% 
String struserJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_USER");
String strWikiTitle = GmCommonClass.getWikiTitle("USER_GROUP_MAPPING");
String strHeader="User - Group Mapping";
%>

<HTML>
<HEAD>

<TITLE>Globus Medical: Module Secutiry Group Mapping</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=struserJsPath%>/GmUserGroup.js"></script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>

</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnLoad();">
<form name="frmUserGroup" method="post" action="/gmUserGroup.do">
<html:hidden name="frmUserGroup" property="strOpt"/>
<html:hidden name="frmUserGroup" property="haction"/>

<html:hidden name="frmUserGroup" property="grpMappingID"/>
<table border="0" class="DtTable680" cellspacing="0" cellpadding="0">
	<tr>
		<td height="25" class="RightDashBoardHeader" width="300">&nbsp;Module - Security Group Mapping</td>
		<td align="right" class=RightDashBoardHeader><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
	</tr>
	<tr class="Shade">
		<td colspan="2" bgcolor="#CCCCCC" height="1"></td>
	</tr>
	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<tr>
		<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>Module Name&nbsp;:</td>
		<td align="left">&nbsp;<gmjsp:dropdown width="200" controlId="moduleName" controlName="moduleName" SFFormName="frmUserGroup" SFSeletedValue="moduleName" SFValue="alModuleName" codeId="GROUPID" codeName="GROUPNAME" defaultValue="[Choose One]"  onChange="fnMDCallAjax(this);"/></td>
	</tr>
	<tr><td colspan="2" height="1" class="LLine"></td></tr>
	<tr class="Shade">
		<td class="RightTableCaption" align="right" HEIGHT="24">Module Description&nbsp;:</td>
		<td align="left">&nbsp;<span id="mdDesc"></span> </td>
	</tr>
	<tr>
		<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>Security Group Name&nbsp;:</td>
		<td align="left">&nbsp;<gmjsp:dropdown width="200"	controlName="groupName" SFFormName="frmUserGroup" SFSeletedValue="groupName" SFValue="alGroupName" codeId="GROUPID" codeName="GROUPNAME" defaultValue="[Choose One]"  onChange="fnCallAjax(this);"/></td>
	</tr>
	<tr>
		<td colspan="2" height="1" class="LLine"></td>
	</tr>
	<tr><td colspan="2" height="1" class="LLine"></td></tr>
	<tr class="Shade">
		<td class="RightTableCaption" align="right" HEIGHT="24">Security Group Description&nbsp;:</td>
		<td align="left">&nbsp;<span id="grpDesc"></span> </td>
	</tr>
	<tr><td colspan="2" height="1" class="LLine"></td></tr>
<%-- 	<tr >
	<td class="RightTableCaption" align="right" HEIGHT="24">Default Group&nbsp;:</td>
		<td align="left">&nbsp;<html:checkbox name="frmUserGroup" property="defaultGrp"></html:checkbox> &nbsp;</td>
	</tr>--%>
	<tr>
		<td colspan="2" align="center" height="24">
		<gmjsp:button value="&nbsp;Submit&nbsp;" gmClass="button" buttonType="Save" onClick="fnMSSubmit();"/>
		<gmjsp:button value="&nbsp;Reset&nbsp;" gmClass="button" buttonType="Save" onClick = "fnMSReset();"/>
	<logic:equal name="frmUserGroup" property="strOpt" value="SCMMAP">
		<gmjsp:button value="&nbsp;Void&nbsp;" gmClass="button" buttonType="Save" onClick="fnVoidGroupMapping();"/>
	</logic:equal>
	<logic:equal name="frmUserGroup" property="strOpt" value="save">
		<gmjsp:button value="&nbsp;Void&nbsp;" gmClass="button" buttonType="Save" onClick="fnVoidGroupMapping();"/>
	</logic:equal>
		</td>
	</tr>
</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
