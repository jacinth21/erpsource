<%
/**********************************************************************************
 * File		 		: GmUserGroupSetup.jsp
 * Desc		 		: This screen is used for the User Group Setup 
 * Version	 		: 1.0
 * author			: Gopinathan Saravanan
************************************************************************************/
%>
<!-- \user\GmUserGroupSetup.jsp -->
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ include file="/common/GmHeader.inc"%>

<% 
String struserJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_USER");
String strWikiTitle = GmCommonClass.getWikiTitle("USER_GROUP_MAPPING");
String strHeader="Security Group - User Mapping";
%>

<HTML>
<HEAD>

<TITLE>Globus Medical: Access Control Group</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=struserJsPath%>/GmUserGroup.js"></script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>

</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnCallAjax(document.frmUserGroup.groupName);">
<form name="frmUserGroup" method="post" action="/gmUserGroup.do">
<html:hidden name="frmUserGroup" property="strOpt"/>
<html:hidden name="frmUserGroup" property="grpMappingID"/>
<logic:equal name="frmUserGroup" property="haction" value="UAGM">
	 <html:hidden name="frmUserGroup" styleId="haction" property="haction" value="UAGM"/>
	 <%strHeader="User- Action Group Mapping"; %>
</logic:equal>
<logic:equal name="frmUserGroup" property="haction" value="ULMGM">
	 <html:hidden name="frmUserGroup" styleId="haction" property="haction" value="ULMGM"/>
	 <%strHeader="User-Left Menu Group Mapping"; %>
</logic:equal>
<table border="0" class="DtTable680" cellspacing="0" cellpadding="0">
	<tr>
		<td height="25" class="RightDashBoardHeader">&nbsp;<%=strHeader%></td>
		<td align="right" class=RightDashBoardHeader><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
	</tr>
	<tr>
		<td colspan="2" height="1" class="line"></td>
	</tr>
	<tr class="Shade">
		<td colspan="6" bgcolor="#CCCCCC" height="1"></td>
	</tr>
	<tr>
		<td colspan="2" height="1" class="LLine"></td>
	</tr>
	<tr>
	<!-- Custom tag lib code modified for JBOSS migration changes -->
		<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>Security Group Name&nbsp;:</td>
		<td align="left">&nbsp;<gmjsp:dropdown width="200"	controlName="groupName" SFFormName="frmUserGroup" SFSeletedValue="groupName" SFValue="alGroupName" codeId="GROUPID" codeName="GROUPNAME" defaultValue="[Choose One]"  onChange="fnCallAjax(this);"/></td>
	</tr>
	<tr><td colspan="2" height="1" class="LLine"></td></tr>
	<tr class="Shade">
		<td class="RightTableCaption" align="right" HEIGHT="24">Security Group Desc&nbsp;:</td>
		<td align="left">&nbsp;<span id="grpDesc"></span> </td>
	</tr>
	<tr><td colspan="2" height="1" class="LLine"></td></tr>
	<tr>
		<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>User&nbsp;:</td>
		<td width="60%" align="left">&nbsp;<gmjsp:dropdown width="200"	controlName="userNm" SFFormName="frmUserGroup" SFSeletedValue="userNm" SFValue="alUser" codeId="PARTYID" codeName="USERNAME" defaultValue="[Choose One]" /></td>
	</tr>
	<tr><td colspan="2" height="1" class="LLine"></td></tr>
	<logic:notEqual name="frmUserGroup" property="haction" value="UAGM"> 
	<%-- <tr class="Shade">
	<td class="RightTableCaption" align="right" HEIGHT="24">Default Group&nbsp;:</td>
		<td align="left">&nbsp;<html:checkbox name="frmUserGroup" property="defaultGrp"></html:checkbox> &nbsp;</td>
	</tr>--%>

	</logic:notEqual>
	<tr><td colspan="2" height="1" class="LLine"></td></tr>
	<tr >
		<td colspan="2" align="center" height="24">
		<gmjsp:button value="&nbsp;Submit&nbsp;" gmClass="button" buttonType="Save" onClick="fnSubmit();"/>
		<gmjsp:button value="&nbsp;Reset&nbsp;" gmClass="button" buttonType="Save" onClick = "fnReset();"  />
	<logic:equal name="frmUserGroup" property="strOpt" value="edit">
		<gmjsp:button value="&nbsp;Void&nbsp;" gmClass="button" buttonType="Save" onClick="fnVoidGroupMapping();"/>
	</logic:equal>
		</td>
	</tr>
	
</table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
