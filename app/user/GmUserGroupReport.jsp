<%
/**********************************************************************************
 * File		 		: GmAssetAtrribute.jsp
 * Desc		 		: This screen is used for the asset attribute Report
 * Version	 		: 1.0
 * author			: Gopinathan Saravanan
************************************************************************************/
%>

<!-- \user\GmUserGroupReport.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<% 
String strWikiTitle = GmCommonClass.getWikiTitle("USER_GROUP_MAPPING_REPORT");
String struserJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_USER");
%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<bean:define id="gridData" name="frmUserGroup" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="strAccessFl" name="frmUserGroup" property="strAccessFl" type="java.lang.String"> </bean:define>
<html>
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
	<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmProgress.css">
	<script language="JavaScript" src="<%=strJsPath%>/GmProgress.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/jquery.js?1"></script>
	<script language="JavaScript" src="<%=strJsPath%>/json2.js"></script>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
</html>
<HEAD>
<TITLE> Globus Medical: Access Control Group Report </TITLE>
	<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="STYLESHEET" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script><!-- PMT-36862 js file  -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=struserJsPath%>/GmUserGroup.js"></script>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"	media="print" />
<style>
table.DtTable700 {
	margin: 0 0 0 0;
	width: 700px;
	border: 1px solid  #676767;
}
</style>

<script>
	var objGridData;
	objGridData = '<%=gridData%>';
	var strAccess;
	strAccess = '<%=strAccessFl%>';
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoadRpt();" onkeydown="javaScript:return fnCallReport();">
<html:form action="/gmUserGroup.do"> 
<html:hidden name="frmUserGroup" property="strOpt" value="edit"/>
<html:hidden name="frmUserGroup" property="grpMappingID"/>
<logic:equal name="frmUserGroup" property="haction" value="hide">
	<html:hidden name="frmUserGroup" property="groupName"/>
	<html:hidden name="frmUserGroup" property="userNm"/>
	<html:hidden name="frmUserGroup" property="companyId"/>
</logic:equal>
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2" height="25" class="RightDashBoardHeader">&nbsp;Security Group - User Mapping Report </td>
			<td colspan="4" align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
		</tr>
		<logic:notEqual name="frmUserGroup" property="haction" value="hide">
		<tr height="30">
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<td class="RightTableCaption" align="right" HEIGHT="24" width="15%">Group Name:</td>
		<td align="left">&nbsp;<gmjsp:dropdown width="200"	controlName="groupName" SFFormName="frmUserGroup" SFSeletedValue="groupName" SFValue="alGroupName" codeId="GROUPID" codeName="GROUPNAME" defaultValue="[Choose One]" /></td>
		<td class="RightTableCaption" align="right" HEIGHT="24">User:</td>
		<td align="left">&nbsp;<gmjsp:dropdown width="200"	controlName="userNm" SFFormName="frmUserGroup" SFSeletedValue="userNm" SFValue="alUser" codeId="PARTYID" codeName="USERNAME" defaultValue="[Choose One]" /></td>		
		<td align="left"><gmjsp:button value="&nbsp;Load&nbsp;" gmClass="button" buttonType="Load" onClick="fnLoadReport();"/></td>
		</tr>
		<tr>
			<td colspan="5" height="1" class="LLine"></td>
		</tr>
		<tr height="30" class="Shade">
		<td class="RightTableCaption" align="right" HEIGHT="24">Company:</td>
		<td align="left">&nbsp;<gmjsp:dropdown width="200"	controlName="companyId" SFFormName="frmUserGroup" SFSeletedValue="companyId" SFValue="alCompany" codeId="COMPANYID" codeName="COMPANYNM" defaultValue="[Choose One]" /></td>
		<td colspan="3"></td>
		</tr>
		</logic:notEqual>
		<% if(gridData.indexOf("cell") != -1){%>
		<tr>
			<td colspan="5"  height="500"  >
				<div id="grpData" style="grid" height="500px" width="698px"></div>
				<div id="pagingArea"  height="500px" width="698px"></div>
		    </td>
		</tr>
		<tr height="50">
			<td align="center" colspan="5" ><div class="exportlinks">Export Options : <img
				src='img/ico_file_excel.png' />&nbsp;<a href="#"
				onclick="fnExportExcel();"> Excel </a>&nbsp;&nbsp;			
			 <img
				src='<%=strImagePath%>/pdf_icon.gif' />&nbsp;<a href="#"
				onclick="fnPDFDownload();"> PDF </a></div>
			</td>
		</tr>
		<% } else {%>
		<tr>
			<td align="center" height="30" class="RightText">
				No Data Available
			</td>
		</tr>
		<% } %>	
		</table>
			
  <%@ include file="/common/GmFooter.inc" %>
</BODY>
</html:form>
</HTML>

