
<%
	/**********************************************************************************
	 * File		 		: GmUserListReport.jsp
	 * Version	 		: 1.0
	 ************************************************************************************/
%>



 <!-- \user\GmUserListReport.jsp -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ page buffer="16kb" autoFlush="true"%>
<%@include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%
String struserJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_USER");
	String strWikiTitle = GmCommonClass.getWikiTitle("USER_LIST");
	
		GmServlet gm = new GmServlet();
		gm.checkSession(response, session);
		String strTimeZone = strGCompTimeZone;
		String strApplDateFmt = strGCompDateFmt;
		String strAccess = GmCommonClass.parseNull(request
				.getParameter("accesslevel"));
		String strFname = GmCommonClass.parseNull(request
				.getParameter("fname"));
		String strLname = GmCommonClass.parseNull(request
				.getParameter("lname"));
	
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: User List</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript"src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<bean:define id="gridData" name="frmUserList" property="gridXmlData" type="java.lang.String"></bean:define>

<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"	media="print" />

<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=struserJsPath%>/GmUserListReport.js"></script>

<script type="text/javascript">
var gridObjData ='<%=gridData%>';
var mygrid;  
document.onkeypress = function(){
	if(event.keyCode == 13){
		fnLoad();
	}
}
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form method="POST" action="/gmUserListAction">
	<html:hidden property="hAction" name="frmUserList" value="Reload" />
	<html:hidden property="strOpt" name="frmUserList" styleId="strOpt" />
	<html:hidden property="editUserAccess" name="frmUserList"/>
	<html:hidden property="switchUserAccess" name="frmUserList"/>
	
	<table border="0" class="DtTable1200" cellspacing="0" cellpadding="0">
		<tr>
			<td  colspan="4" height="25" class="RightDashBoardHeader">User List</td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			 <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		
		<tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
			<td class="RightText" align="right" HEIGHT="24" style="font-weight:bold">&nbsp;Type:</td>
			<td class="RightText">&nbsp;<gmjsp:dropdown controlId="usrtype"
				controlName="usrtype" SFFormName="frmUserList"
				SFSeletedValue="usrtype" tabIndex="1" width="150" SFValue="alType"
				codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" /></td>
				
			<td class="RightText" align="right" HEIGHT="24" style="font-weight:bold">&nbsp;Status:</td>
			<td class="RightText" colspan="2"><html:hidden property="strOpt"
				name="frmUserList" />&nbsp;<gmjsp:dropdown controlId="usrstat"
				controlName="usrstat" SFFormName="frmUserList"
				SFSeletedValue="usrstat" tabIndex="2" width="150" SFValue="alStatus"
				codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" /></td>
				
		</tr>
		<tr>
			<td colspan="5" height="1" class="LLine"></td>
		</tr>
		<tr class="Shade">
			<td class="RightText" align="right" HEIGHT="24" style="font-weight:bold">&nbsp;Dept:</td>
			<td class="RightText">&nbsp;<gmjsp:dropdown
				controlName="departmentid" SFFormName="frmUserList"
				SFSeletedValue="departmentid" tabIndex="3" width="150" SFValue="alDept"
				codeId="CODEID" codeName="CODENMALT" defaultValue="[Choose One]"
				controlId="departmentid"/></td>
			<td class="RightText" align="right" HEIGHT="24" style="font-weight:bold">&nbsp;Access
			Level:</td>
			<td class="RightText" colspan="2">&nbsp;<input type="text" size="3"
				value="<%=strAccess%>" name="accesslevel" class="InputArea" id="accesslevel"
				onFocus="changeBgColor(this,'#AACCE8');"
				onBlur="changeBgColor(this,'#ffffff');" tabindex=4>
				</td>
		</tr>
		<tr>
			<td colspan="5" height="1" class="LLine"></td>
		</tr>
		<tr>
			<td class="RightText" align="right" HEIGHT="24" style="font-weight:bold">&nbsp;First
			Name:</td>
			<td class="RightText">&nbsp;<input type="text" size="20"
				name="fname" class="InputArea" id="fname"
				onFocus="changeBgColor(this,'#AACCE8');" value="<%=strFname%>"
				onBlur="changeBgColor(this,'#ffffff');" tabindex=5></td>
			<td class="RightText" align="right" HEIGHT="24" style="font-weight:bold">&nbsp;Last Name:</td>
			<td class="RightText" valign="middle" colspan="2">&nbsp;<input type="text" size="20"
				value="<%=strLname%>" name="lname" class="InputArea" id="lname"
				onFocus="changeBgColor(this,'#AACCE8');"
				onBlur="changeBgColor(this,'#ffffff');" tabindex=6>
			</td>
		</tr>
			<tr>
				<td colspan="5" height="1" class="LLine"></td>
			</tr>
			<tr class="Shade" height="30">
				<td class="RightText" align="right" HEIGHT="24"
					style="font-weight: bold">&nbsp;Authentication:</td>
				<td class="RightText">&nbsp;<gmjsp:dropdown
						controlName="ldapID" SFFormName="frmUserList"
						SFSeletedValue="ldapID" SFValue="alLdap" codeId="ID"
						codeName="NAME" defaultValue="[Choose One]" tabIndex="7"></gmjsp:dropdown></td>
		            <td class="RightText" align="right" HEIGHT="24"
					style="font-weight: bold">Company:</td>
		             <td colspan="2"  class="RightText">&nbsp;<gmjsp:dropdown width="200"	controlName="companyId" SFFormName="frmUserList" SFSeletedValue="companyId" SFValue="alCompany" 
                      codeId="COMPANYID" codeName="COMPANYNM" defaultValue="[Choose One]" />
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		            <gmjsp:button value="&nbsp;Load&nbsp;" 
						gmClass="Button" buttonType="Load" onClick="fnLoad();"
						tabindex="8" /></td>
						
			</tr>
			<tr>
			<td colspan="5" height="1" class="LLine"></td>
		</tr>
		<% if(gridData.indexOf("cell") != -1){%>
		<tr>
			<td colspan="5" height="50" align="center">
			<div id="dataGridDiv" style="height: 450px"></div>
			</td>
		</tr>
		<tr>
			<td colspan="5" align="center">
			<div class='exportlinks'>Export Options : <img
				src='img/ico_file_excel.png' />&nbsp;<a href="#"
				onclick="fnExportExcel();"> Excel </a></div>
			</td>
		</tr>
		<% } else {%>
		<tr>
			<td colspan="5" align="center" height="30" class="RightText">
				No Data Available
			</td>
		</tr>
		<% } %>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
