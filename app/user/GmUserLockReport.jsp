<%
/**********************************************************************************
 * File		 		: GmUserLockReport.jsp 
 * Version	 		: 1.0
 * author			: Hardik Parikh
************************************************************************************/
%>
<!-- \user\GmUserLockReport.jsp-->
<%@ include file="/common/GmHeader.inc" %>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import="java.util.ArrayList,java.util.*" %>
<% String strWikiTitle = GmCommonClass.getWikiTitle("USER_LOCK_REPORT");%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Construct Builder </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
 
<bean:define id="gridData" name="frmUserLockReport" property="gridXmlData" type="java.lang.String"> </bean:define>
<% 
	boolean showGrid=true;	
	if(gridData!=null && gridData.indexOf("cell")==-1){
	   showGrid = false;			
	}	
%>
 

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/displaytag.css");
      div.gridbox_dhx_skyblue table.obj tr td{
     border-width:0px 1px 0px 0px;
     border-color : #A4BED4;
}
</style>
<script>

var mygrid; 
var showGridbln = '<%=showGrid%>';
function fnOnPageLoad() {

	var gridObjData = '<%=gridData%>';	
	if(showGridbln == 'true')
	{
	   	mygrid = initGrid('dataGridDiv',gridObjData);   
	}
}
function fnSubmit() {
	var gridrows =mygrid.getChangedRows(",");
	if(gridrows==''){
		
		Error_Details('To Unlock, please uncheck the <b>Lock Status</b> field.');
		
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}	
		
	}else {	
		var gridrowsarr = gridrows.toString().split(",");
		var arrLen = gridrowsarr.length;
		var isInValidPr =false;
		var strInputString = '';
		for (var i=0;i<arrLen;i++)
		{
	        rowId=gridrowsarr[i];	        
			lockstatus = mygrid.cellById(rowId,3).getValue();
			lockstatus=lockstatus=='1'?'Y':'N';
			
			strInputString = strInputString+ rowId +'^'+ lockstatus +'|';
		}
		   document.frmUserLockReport.hinputString.value =  strInputString;
	}	
	
	document.frmUserLockReport.strOpt.value =  'save';
	fnStartProgress();	 
	document.frmUserLockReport.submit();
}
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad()">
<html:form action="/gmUserLockReport.do">
<html:hidden property="strOpt" />
<html:hidden property="hinputString" value=""/> 

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">User Lock Report</td>	
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>		
		</tr>
		<tr><td colspan="4" height="1" class="line"></td></tr>
		<tr>
			<td  valign="top" colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    
			 	 <tr><td colspan="4" class="lline"></td></tr>
				   <tr>
				   <%if(showGrid){%>
			            <td colspan="4" valign="top">
			            <div id="dataGridDiv" style="height:350px;width:698px;"></div>
						</td>
				<%}else{ %>
					<td class="RightText" colspan="4">
				    	No records found to display.
					</td>	
				 <%}%>
	    	       </tr> 
    			 	<tr><td colspan="4" class="ELine"></td></tr> 
              		 <tr height="30">
              		   <%if(showGrid){%>
		               	<td colspan="4" align="center">
		                	 <gmjsp:button controlId="idSubmit" value="Unlock" gmClass="button" buttonType="Save" onClick="fnSubmit();" />  
			            </td>
	             <%} %>
	               </tr>
	  		 	</table>
  			   </td>
  		  </tr>	
    </table>	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>

