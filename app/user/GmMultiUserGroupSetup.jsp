<%
/**********************************************************************************
 * File		 		: GmMultiUserGroupSetup.jsp
 * Desc		 		: This screen is used for the asset attribute Report
 * Version	 		: 1.0
 * author			: Jignesh Shah
************************************************************************************/
%>
<!-- \user\GmMultiUserGroupSetup.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<bean:define id="alUserList" name="frmMultiUserGroup" property="alUserList" type="java.util.ArrayList"></bean:define>
<bean:define id="alSecGrp" name="frmMultiUserGroup" property="alSecGrp" type="java.util.ArrayList"></bean:define>
<bean:define id="strOpt" name="frmMultiUserGroup" property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="alCompany" name="frmMultiUserGroup" property="alCompany" type="java.util.ArrayList"></bean:define>
<bean:define id="strAccessFl" name="frmMultiUserGroup" property="strAccessFl" type="java.lang.String"></bean:define>
<%
String struserJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_USER");
String strWikiTitle = GmCommonClass.getWikiTitle("MULTI_USER");
%>
<%
	HashMap hmUser = new HashMap();
	hmUser.put("ID","");
	hmUser.put("PID","PARTYID");
	hmUser.put("NM","USERNAME");
	HashMap hmSecGrp = new HashMap();
	hmSecGrp.put("ID","");
	hmSecGrp.put("PID","GROUPID");
	hmSecGrp.put("NM","GROUPNAME");
	String strShade = "shade";
	String strHeaderTitle = "User to Security Groups Mapping";
	String strSwap = "Security Group to Users Mapping";
	
	if(strOpt.equals("SECUSR")){
		strHeaderTitle = "Security Group to Users Mapping";
		strSwap = "User to Security Groups Mapping";
	}
	
	GmCommonControls.setHeight(300);
	GmCommonControls.setWidth(300);
	String strSubmitDisable="";
	if(!strAccessFl.equals("Y")){
		strSubmitDisable = "true";
	}
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: User List</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/jquery.min.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=struserJsPath%>/GmMultiUserGroupSetup.js"></script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="onLoad('<%=strOpt%>');">
<form name="frmMultiUserGroup" action="/gmMultiUserGroup.do">
<html:hidden property="strOpt" name="frmMultiUserGroup" value="<%=strOpt%>"/>
<html:hidden property="hinputstring" name="frmMultiUserGroup" value=""/>
<html:hidden property="defaultgrp" name="frmMultiUserGroup" value=""/>
<html:hidden property="haction" name="frmMultiUserGroup" value=""/>



		<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
		 
					<tr>
						<td height="25" class="RightDashBoardHeader" width="250">&nbsp;<%=strHeaderTitle%></td>
						 <td align="right" class=RightDashBoardHeader colspan="2">
							<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
						</td>
					</tr>	
					<table class="DtTable700" border="0" cellspacing="0" cellpadding="0"><tr><td>	 				
					<tr><td height="1" colspan="2"></td></tr>			
					<tr height="30" class="shade">
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<%if(strOpt.equals("USRSEC")){ %>
						  <td  align="right" width="180px"><b><font color="red">*</font>Users:&nbsp;</b> </td>
						<td><gmjsp:dropdown controlName="userList" SFFormName="frmMultiUserGroup" SFSeletedValue="userList"
							SFValue="alUserList" codeId = "PARTYID"  codeName = "USERNAME"  width="300"  defaultValue= "[Choose One]" />	
						</td>
					<%}else{ %>
						<td  align="right" width="180px"><b><font color="red">*</font>Groups:&nbsp;</b></td>				
						<td><gmjsp:dropdown controlName="secGrp" SFFormName="frmMultiUserGroup" SFSeletedValue="secGrp"
							SFValue="alSecGrp" codeId = "GROUPID"  codeName = "GROUPNAME"  width="300" defaultValue= "[Choose One]" />	
						</td>
					<%} %>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>	 
						<tr height="30">			 
							<td  align="right" width="180px"><font color="red"><b>*</font>Company:&nbsp;</b></td>
							<td><gmjsp:dropdown controlName="companyId" SFFormName="frmMultiUserGroup" SFSeletedValue="companyId"  SFValue="alCompany"
								codeId="COMPANYID" codeName="COMPANYNM" defaultValue="[Choose One]"  width="300"/>&nbsp;&nbsp;
							<gmjsp:button value="Load" onClick="fnload();" buttonType="Load" gmClass="button" />	
							</td>							
						</tr>
					</td></tr></table>
					<table class="DtTable700" border="0" cellspacing="0" cellpadding="0"><tr><td>				 
					<%if(strOpt.equals("USRSEC")){ %>						
						<tr class="<%=strShade%>" >
						<td align="Right" width="180px" valign="top"><font color="red">*</font><b>Security Groups: &nbsp;</b></td> 
						<td>
     			  		<%=GmCommonControls.getChkBoxGroup("",alSecGrp,"sec",hmSecGrp)%>
						</td>
						</tr>
						
						<%
						GmCommonControls.setHeight(300);
						GmCommonControls.setWidth(300);
						%>							
					<%}else{%>
					
						<tr class="<%=strShade%>" >
						<td align="Right" width="180px" valign="top"><font color="red">*</font><b>User List:&nbsp;</b></td> 
						<td>
     			  		<%=GmCommonControls.getChkBoxGroup("",alUserList,"user",hmUser)%>
						</td>
						</tr>
					<%} %>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr>
						<td  height="50" align="center" colspan="2">&nbsp;
							<gmjsp:button style="width: 10em" value="Submit" disabled="<%=strSubmitDisable%>"  buttonType="Save" onClick="fnSubmit();" gmClass="button" />&nbsp;
							<gmjsp:button value="<%=strSwap%>" onClick="fnSwap();" buttonType="Load" gmClass="button" />&nbsp;
							<!-- Add History button for user and security group and call Js function -->	
						   <gmjsp:button style="width: 10em" value="History" name="btn_history"  buttonType="History" onClick="fnHistory();" gmClass="button" />&nbsp;          
					</tr>
					<tr><td height="1" colspan="2"></td></tr>
					</td></tr></table>
				</table>
</form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>