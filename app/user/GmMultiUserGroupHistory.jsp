<%
/**********************************************************************************
 * File		 		: GmMultiUserGroupHistory.jsp
 * Desc		 		: This screen is used for MultiUserGroupHistory
 * Version	 		: 1.0
 * author			: 
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
 <!-- \user\GmMultiUserGroupHistory.jsp-->
<% 
String strWikiTitle = GmCommonClass.getWikiTitle("MULTI_USER_HISTORY");
String struserJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_USER");
%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<bean:define id="gridData" name="frmMultiUserGroup" property="gridXmlData" type="java.lang.String"> </bean:define>
 <bean:define id="strOpt" name="frmMultiUserGroup" property="strOpt" type="java.lang.String"> </bean:define>
 <bean:define id="secGrpName" name="frmMultiUserGroup" property="secGrpName" type="java.lang.String"> </bean:define>
 <bean:define id="userName" name="frmMultiUserGroup" property="userName" type="java.lang.String"> </bean:define>
<%
String strHeaderTitle = "Security Group History";
String labelName="";

HashMap hmUser = new HashMap();
HashMap hmSecGrp = new HashMap();
if(strOpt.equals("USRSEC")){
	labelName=userName;
}
if(strOpt.equals("SECUSR")){
	labelName=secGrpName;
}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Access Control Group Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=struserJsPath%>/GmMultiUserGroupHistory.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>



<script>
	var objGridData;
	objGridData = '<%=gridData%>';
	
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoad()">
<form name="frmMultiUserGroup" action="/gmMultiUserGroup.do">
	<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2" height="25" class="RightDashBoardHeader">&nbsp;<%=strHeaderTitle%>-<%=labelName%> </td>
			<td colspan="4" align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
		</tr>
			
        <tr>
			<td colspan="5" height="1" class="LLine"></td>
		</tr>
		<% if(gridData.indexOf("cell") != -1){%>
		<tr>
			<td colspan="5" height="50" align="center">
			<div id="dataGridDiv" style="height: 450px"></div>
			</td>
		</tr>
		<tr>
			<td colspan="5" align="center">
			<div class='exportlinks'>Export Options : <img
				src='img/ico_file_excel.png' />&nbsp;<a href="#"
				onclick="fnExportExcel();"> Excel </a></div>
			</td>
		</tr>
		<% } else {%>
		<tr>
			<td colspan="5" align="center" height="30" class="RightText">
				No Data Available
			</td>
		</tr>
		<% } %>
    </table>		     	
 <form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>

