
<%
	/**********************************************************************************
	 * File		 		: GmUserListSetup.jsp
	 * Version	 		: 1.0
	 ************************************************************************************/
%>

<!-- \user\GmUserListSetup.jsp -->
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page buffer="16kb" autoFlush="true"%>
<%@include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="userId" name="frmUserList" property="huserid" type="java.lang.String"></bean:define>
<bean:define id="emailId" name="frmUserList" property="emailid" type="java.lang.String"></bean:define>
<bean:define id="alCompany" name="frmUserList" property="alCompany" type="java.util.ArrayList"></bean:define>
<bean:define id="strAccessFl" name="frmUserList" property="strAccessFl" type="java.lang.String"></bean:define>

<!-- PC:446: To get the current user information -->
<bean:define id="companyId" name="frmUserList" property="companyId" type="java.lang.String"></bean:define>
<bean:define id="fname" name="frmUserList" property="fname" type="java.lang.String"></bean:define>
<bean:define id="lname" name="frmUserList" property="lname" type="java.lang.String"></bean:define>
<%
String struserJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_USER");
String strProdmgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");

%>
<HTML>
<HEAD>
<TITLE>Globus Medical: User List Setup</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"	media="print" />
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strProdmgmntJsPath%>/GmRule.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=struserJsPath%>/GmUserProfileContainer.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>


<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>

<%

	boolean blEmailIDFl = false;
	String strWikiTitle = GmCommonClass.getWikiTitle("USER_EDIT");
	HashMap hmReturn= GmCommonClass.parseNullHashMap((HashMap)request.getAttribute ("USERDETAILS"));
	String strBTTeamAccess = GmCommonClass.parseNull((String)request.getAttribute ("USER_EDIT_ACCESS"));
	String strDeptHisFlg = GmCommonClass.parseNull((String)hmReturn.get("DEPT_HIS_FLG"));
	String strAccLvlHisFlg = GmCommonClass.parseNull((String)hmReturn.get("ACCLVL_HIS_FLG"));
	String strUserStatus = GmCommonClass.parseNull((String)hmReturn.get("USERSTATUSID"));
	String strEmailID = GmCommonClass.parseNull((String)hmReturn.get("EMAILID"));

	//Enable/disable button
	String strDisable = "";
	if(!strAccessFl.equals("Y")){
		strDisable = "true";
	}
%>

</HEAD>
<BODY onload="fnUserSetupPageLoad();">
<html:form method="POST" action="/gmUserListAction">
    <html:hidden property="strOpt" name="frmUserList"  />
	<html:hidden property="huserid" name="frmUserList"/>
	<html:hidden property="hCopyUserName" name="frmUserList"/><!-- PMT-51134--Copy Security Group access -->
	<html:hidden property="huserLoginName" name="frmUserList"/>
	<html:hidden property="hpassword" name="frmUserList" />
	<html:hidden property="hTxnId" value=""/>
	<html:hidden property="hCancelType" value="USRTRM"/>
	<html:hidden property="userStatus"/>
	<!-- PC:446: To auto refresh redis key while change company and name -->
	<html:hidden property="hOldCompanyId" name="frmUserList" value="<%=companyId %>"/>
	<html:hidden property="hOldFirstName" name="frmUserList" value="<%=fname %>"/>
	<html:hidden property="hOldLastName" name="frmUserList" value="<%=lname %>"/>

	<table style="margin: 0 0 0 0; width: 825px; border: 1px solid  #676767;" cellspacing="0" cellpadding="0">

		<tr>
			<td colspan="6" height="1" class="LLine"></td>
		</tr>
		<tr class="Shade"> 
			<td class="RightTableCaption" align="right" HEIGHT="24" width="30%">&nbsp;First Name:</td>
			<td class="RightText" id="divFName">&nbsp;<html:text size="25" name="frmUserList"	property="fname" 
				styleClass="InputArea"	onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" maxlength="20" />
		</td>
			 
		</tr>
		<tr>
			<td colspan="6" height="1" class="LLine"></td>
		</tr>
		<tr>
			 
			<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Last Name:</td>
			<td class="RightText" id="divLName" valign="middle">&nbsp;<html:text size="25" name="frmUserList"	property="lname" 
				styleClass="InputArea"	onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" maxlength="20" />
			</td>
		</tr>
		<tr>
			<td colspan="6" height="1" class="LLine"></td>
		</tr>
		<tr class="Shade">
			 
			<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;User	Name:</td>
			<td class="RightText">&nbsp;<html:text size="25" name="frmUserList"	property="userLoginName" 
				styleClass="InputArea"	onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');fnValidateUserName(this.value);" maxlength="20" onclick="fnUserToggle();"/>
				<span id="DivShowUsrAvl" style="vertical-align:middle; display:none;"> <img height="16" width="19" src="<%=strImagePath%>/success.gif"></img></span>
				 <span id="DivShowUsrNotAvl" style="vertical-align:middle;display: none;"><img height="12" width="12" src="<%=strImagePath%>/delete.gif"></img>
						 &nbsp;
						 </span>
			</td>
			 
		</tr>
		<tr>
			<td colspan="6" height="1" class="LLine"></td>
		</tr>
		<tr>
			 
			<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Short	Name:</td>
			<td class="RightText">&nbsp;<html:text size="25" name="frmUserList"	property="shortName" 
				styleClass="InputArea"	onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" maxlength="20" />
			</td>
			 
		</tr>
		<tr>
			<td colspan="6" height="1" class="LLine"></td>
		</tr>
		<tr class="Shade">
			 
			<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<font color="red">*</font>Email ID:</td>
			<td class="RightText">&nbsp;<html:text size="45" name="frmUserList"	property="emailid" disabled="<%=blEmailIDFl%>" styleClass="InputArea"	
				onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
			</td>
			 
		</tr>
		<tr>
			<td colspan="6" height="1" class="LLine"></td>
		</tr>
		<tr>
			 
			<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<font color="red">*</font>Company: </td>
			<td class="RightText">&nbsp;<gmjsp:dropdown
				controlName="companyId" SFFormName="frmUserList"
				SFSeletedValue="companyId"  width="200" SFValue="alCompany"
				codeId="COMPANYID" codeName="COMPANYNM" defaultValue="[Choose One]"
				controlId="companyId"/>
			</td>		 
		</tr>
		<tr>
			<td colspan="6" height="1" class="LLine"></td>
		</tr>
		<tr class="Shade">
			 <!-- Custom tag lib code modified for JBOSS migration changes -->
			<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<font color="red">*</font>Department ID:</td>
			<td class="RightText">&nbsp;<gmjsp:dropdown
				controlName="departmentid" SFFormName="frmUserList"
				SFSeletedValue="departmentid"  SFValue="alDept"
				codeId="CODEID" codeName="CODENMALT" defaultValue="[Choose One]"
				controlId="departmentid"/>
					<%if(strDeptHisFlg.equals("Y")){ %>
					&nbsp;<img  id="imgEdit" style="cursor:hand" onclick="javascript:fnLoadHistory ('901','<%=userId%>');" title="Click to open required dept history"  	src="<%=strImagePath%>/icon_History.gif" align="bottom"  height=15 width=18 />
					<%}%>
			</td>
			 
		</tr>
		<tr>
			<td colspan="6" height="1" class="LLine"></td>
		</tr>

		<tr>
			 
			<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<font color="red">*</font>Security Group: </td>
			<td class="RightText">&nbsp;<gmjsp:dropdown
				controlName="securityGrp" SFFormName="frmUserList"
				SFSeletedValue="securityGrp" SFValue="alSecGrp"
				codeId="GROUPID" codeName="GROUPNAME" defaultValue="[Choose One]"
				controlId="securityGrp"/>
			</td>		 
		</tr>
		<tr>
			<td colspan="6" height="1" class="LLine"></td>
		</tr>		
		<tr class="Shade">
		<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<font color="red">*</font>User Type:</td>
		<td class="RightText">&nbsp;<gmjsp:dropdown
		controlName="userType" SFFormName="frmUserList"
		SFSeletedValue="userType" width="150" SFValue="alUserType"
		codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"
		controlId="usertype" onChange="fnchkUsertype()"/></td>
		</tr>
		<tr>
			<td colspan="6" height="1" class="LLine"></td>
		</tr>
		
		<tr>
		 
		<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Title:</td>
			<td class="RightText">&nbsp;<gmjsp:dropdown
				controlName="title" SFFormName="frmUserList"
				SFSeletedValue="title" SFValue="alTitle"
				codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"
				controlId="title" onChange="fnUserTitle()" /></td>
			 
		</tr>
		<tr>
			<td colspan="6" height="1" class="LLine"></td>
		</tr>
	
		<tr class="Shade">
		  
			<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Work Phone No:</td>
			<td class="RightText">&nbsp;<html:text size="20" name="frmUserList"	property="workPhone" styleClass="InputArea"
				onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" maxlength="20" /></td>
		</tr>
		<tr>
			<td colspan="6" height="1" class="LLine"></td>
		</tr>
		
		<tr>
		 
		 <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<font color="red">*</font>Access Level:</td>
		 <!-- Struts tag lib code modified for JBOSS migration changes -->
			<td class="RightText">&nbsp;<html:text size="5" name="frmUserList"	property="accesslevel" styleClass="InputArea"
				onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" maxlength="2" />
				<%if(strAccLvlHisFlg.equals("Y")){ %>
				&nbsp;<img  id="imgEdit" style="cursor:hand" onclick="javascript:fnLoadHistory('900','<%= userId%>');" title="Click to open required accesslevel history"  	src="<%=strImagePath%>/icon_History.gif" align="bottom"  height=15 width=18 />
				<%}%>
				</td> 
		</tr>
		<tr>
			<td colspan="6" height="1" class="LLine"></td>
		</tr>
		<tr class="Shade">
		<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;External Access:</td>
		<td  class="RightText">&nbsp;<html:checkbox  name="frmUserList" property="externalAccFl" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/></td>
		</tr>
		<tr>
			<td colspan="6" height="1" class="LLine"></td>
		</tr>

		<tr> 
			<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Status:</td>
			<td class="RightText" id="divStatus">&nbsp;<bean:write property="userStatus" name="frmUserList"/></td>
		</tr>
		<tr>
			<td colspan="6" height="1" class="LLine"></td>
		</tr>
		<tr class="Shade"> 
			<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Auth Type:</td>
			<td class="RightText" id="divStatus">&nbsp;<bean:write property="authtype" name="frmUserList"/></td>
		</tr>
		<!--PMT-29459 Enable User PIN upon new user Creation-->
		
		<%if(!userId.equals("")){ %>
		<tr>
			<td colspan="6" height="1" class="LLine"></td>
		</tr>
		<tr>
			 
			<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Login PIN:</td>
			<td class="RightText"  valign="middle">&nbsp;<html:text size="4" name="frmUserList"	property="lockPin" 
				styleClass="InputArea"	onfocus="changeBgColor(this,'#AACCE8');" maxlength="4" />
			</td>
		</tr>
		
		
		<%} %>
		<tr>
		<td colspan="6" height="1" class="LLine"></td>
		</tr>
		<tr>
			 <!-- PMT-51134--Copy Security Group access -->
			<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Copy Access:</td>
			<td class="RightText">&nbsp;<html:text size="25" name="frmUserList"	property="copyUserName" 
				styleClass="InputArea"	onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');fnValidateCopyAccessUser(this.value);" maxlength="20" onclick="fnUserToggle();"/>
				<span id="DivCopyShowUsrAvl" style="vertical-align:middle; display:none;"> <img height="16" width="19" src="<%=strImagePath%>/success.gif"></img></span>
				 <span id="DivCopyUsrNotAvl" style="vertical-align:middle;display: none;"><img height="12" width="12" src="<%=strImagePath%>/delete.gif"></img>
						 &nbsp;
						 </span>
			</td>
			 
		</tr>
		<tr>
			<td colspan="6" height="1" class="LLine"></td>
		</tr>
		<tr>
				<td colspan="6" align="center" height="25">
                <jsp:include page="/common/GmIncludeLog.jsp" >
                	<jsp:param name="FORMNAME" value="frmUserList" />
					<jsp:param name="ALNAME" value="alLogReasons" />
					<jsp:param name="LogMode" value="Edit" />
					<jsp:param name="Mandatory" value="yes" />																					
				</jsp:include>	
				</td>
		</tr>
	 <%if(strBTTeamAccess.equals("Y")){%>
		<tr>
			<td colspan="6" height="1" class="LLine"></td>
		</tr>
		<tr>
           	<td colspan="6" align="center" height="30" >
		        <gmjsp:button controlId="Btn_Submit" value="Submit" disabled="<%=strDisable%>" gmClass="button" buttonType="Save" onClick="fnSubmit();" /> 
		        <gmjsp:button value="&nbsp;&nbsp;Reset&nbsp;&nbsp;"  disabled="<%=strDisable%>" gmClass="button" buttonType="Save" onClick="fnReset();" />
		        <%if(strUserStatus.equals("311")){ %>
 					<gmjsp:button controlId="Btn_Inact" value="&nbsp;&nbsp;Deactivate&nbsp;&nbsp;" disabled="<%=strDisable%>" gmClass="button" buttonType="Save" onClick="fnInActive();" />
				<%}%>
				<%if(strUserStatus.equals("312") || strUserStatus.equals("314") || strUserStatus.equals("317")){ %>
					<gmjsp:button controlId="Btn_Rollbck" value="&nbsp;&nbsp;Reactivate&nbsp;&nbsp;" disabled="<%=strDisable%>" gmClass="button" buttonType="Save" onClick="fnRollBack();" />
				<%}%>
		    </td>
		    </tr>
		 <%}%> 
		 <%--    </logic:notEqual> --%>
	</table>
	<script type="text/javascript">
var userNameUpdate= document.frmUserList.userLoginName.value;
var userName= parent.document.all.userLoginName.value;
if(userNameUpdate != userName){
	parent.document.all.userLoginName.value = userNameUpdate;
}
</script>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
