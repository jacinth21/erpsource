<%
/**********************************************************************************
 * File		 		: GmUserGroupReportExcel.jsp
 * Desc		 		: This screen is used to export excel for User Group report
 * Version	 		: 1.0
 * author			: ppandiyan
************************************************************************************/
%>

<!-- \user\GmUserGroupReportExcel.jsp -->

<%@ page import="java.util.Locale"%>


<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.text.*,java.lang.String" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>

<bean:define id="alGrpMapDtls" name="frmUserGroup" property="alGrpMapDtls" type="java.util.ArrayList"> </bean:define>
<bean:define id="exportExcelFl" name="frmUserGroup" property="exportExcelFl" type="java.lang.String"> </bean:define>
<%
int intLoop = 0;
HashMap hmLoop = new HashMap();

String strUserNm = "";
String strGroupNm = "";
String strCompanyNm ="";

if ((exportExcelFl != null) && (exportExcelFl.equals("Y"))) { 
response.setContentType("application/vnd.ms-excel;charset=UTF-8");
response.setHeader("Content-disposition","attachment;filename = UserGroupReportExcel.xls");
}
if (alGrpMapDtls != null)
	{
		intLoop = alGrpMapDtls.size();
	}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: User Group Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.RightTableCaption {
	FONT-SIZE: 15px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
}
TR.RightTableCaption{
	FONT-SIZE: 15px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
	background-color: #eeeeee;
}
.RightText {
	FONT-SIZE: 15px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
}
</style>

</HEAD>

<BODY leftmargin="20" topmargin="20" >
<form method="post" action="/gmUserGroup.do?">
	<table border="0" width="1100" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2" height="1" bgcolor="#666666"></td>
		</tr>
		<tr>
			<td colspan="2" align="center" height="29"><font size="+2">SECURITY GROUP USER MAPPING REPORT</font></td>
		</tr>
		<tr>
			<td colspan="2" valign="top">
				<table border="1" width="100%" cellspacing="0" cellpadding="0" valign="top">
					<tr class="RightTableCaption">
						<td  HEIGHT="30" align="center" width="100">User</td>
						<td  HEIGHT="30" align="center" width="100">Group Name</td>
						
						
						<td HEIGHT="30" align="center" width="100">Company</td>	
						
							
					</tr>
<%
			if (intLoop > 0)
			{

				
				for (int i = 0;i < intLoop ;i++ )
				{
					hmLoop = (HashMap)alGrpMapDtls.get(i);
					strUserNm = GmCommonClass.parseNull((String)hmLoop.get("RUSERNM"));
					strGroupNm = GmCommonClass.parseNull((String)hmLoop.get("RGROUPNAME"));

					strCompanyNm = GmCommonClass.parseNull((String)hmLoop.get("COMPANYNM"));
					

%>
						<tr>
							<td  width="100" align="left" HEIGHT="30" class="RightText">&nbsp;<%=strUserNm%></td>
							<td  width="100" align="left" HEIGHT="30" class="RightText">&nbsp;<%=strGroupNm%></td>
						
							<td  width="100" align="left" HEIGHT="30" class="RightText">&nbsp;<%=strCompanyNm%></td>
							
						</tr>
<%
				}
			} else	{
%>
					<tr>
						<td height="30" colspan="3" align="center" class="RightText">Nothing found to display</td>
					</tr>
<%
		}
%>
					<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
				</table>
			</td>
		</tr>
	</table>
			
</BODY>
</form>
</HTML>