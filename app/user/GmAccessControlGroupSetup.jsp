

<%
/**********************************************************************************
 * File		 		: GmAccessControlGroupSetup.jsp
 * Desc		 		: This screen is used for the Access Control Group Setup 
 * Version	 		: 1.0
 * author			: Gopinathan Saravanan
************************************************************************************/
%>
<!-- \user\GmAccessControlGroupSetup.jsp -->
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="grpType" name="frmAccessControlForm" property="groupType" type="java.lang.String"></bean:define>
 <bean:define id="strAccessFl" name="frmAccessControlForm" property="strAccessFl" type="java.lang.String"></bean:define> 

<% 
String struserJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_USER");
String strWikiTitle = GmCommonClass.getWikiTitle("ACCESS_CONTROL_GROUP");
String strHeader="Access Control Group";
String strLabels="Group";
String strGrpType=grpType;
if(strGrpType.equals("92264")){
	strLabels="Security Group";
    strHeader="Create Security Group";
    strWikiTitle = "CREATE_SECURITY_GROUP";
}
else if(strGrpType.equals("92265")){
	strLabels="Module";
	strHeader="Create Module";
	strWikiTitle = "CREATE_MODULE";
}
/*TO Enable/Disable Button Access-PMT 26730 -System Manager Changes*/
String strSubmitDisable="";
if(!strAccessFl.equals("Y")){
	strSubmitDisable = "true";
}
%>

<HTML>
<HEAD>

<TITLE>Globus Medical: Access Control Group</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=struserJsPath%>/GmAccessControlSetup.js"></script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>

</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="javascript:fndisable()">
<form name="frmAccessControlForm" method="post" action="/gmAccessControlGroup.do">
<html:hidden name="frmAccessControlForm" property="strOpt" value=""/>
<html:hidden name="frmAccessControlForm" property="groupType"/>


<table border="0" class="DtTable680" cellspacing="0" cellpadding="0">
<logic:equal name="frmAccessControlForm" property="haction" value="SCGRP">
	 <html:hidden name="frmAccessControlForm" styleId="haction" property="haction" value="SCGRP"/>
</logic:equal>
<logic:equal name="frmAccessControlForm" property="haction" value="MDGRP">
	 <html:hidden name="frmAccessControlForm" styleId="haction" property="haction" value="MDGRP"/>
</logic:equal>
	<tr>
		<td height="25" class="RightDashBoardHeader">&nbsp;<%=strHeader%></td>
		<td align="right" class=RightDashBoardHeader><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
	</tr>
	<tr>
		<td colspan="2" height="1" class="line"></td>
	</tr>
	<tr class="Shade">
		<td colspan="6" bgcolor="#CCCCCC" height="1"></td>
	</tr>
	<tr class="Shade">
	<!-- Custom tag lib code modified for JBOSS migration changes -->
		<td class="RightTableCaption" align="right" HEIGHT="24"><%=strLabels%> List&nbsp;:</td>
		<td align="left">&nbsp;
		<gmjsp:dropdown width="200"	controlName="groupId" SFFormName="frmAccessControlForm" SFSeletedValue="groupId" SFValue="alGroupName" codeId="GROUPID" codeName="GROUPNAME" defaultValue="[Choose One]"  onChange="fnCallAjax(this);"/></td>
	</tr>
	<tr>
		<td colspan="2" height="1" class="LLine"></td>
	</tr>
	<tr>
		<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font><%=strLabels%> Name&nbsp;:</td>
		<td width="60%" align="left">&nbsp;&nbsp;<html:text name="frmAccessControlForm" property="groupName" size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" /></td>
	</tr>
	<tr><td colspan="2" height="1" class="LLine"></td></tr>
	<tr class="Shade">
		<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font><%=strLabels%> Description&nbsp;:</td>
		<td align="left">&nbsp;&nbsp;<html:textarea name="frmAccessControlForm" property="groupDesc" cols="40" rows="6" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" /></td>
	</tr>
	<tr><td colspan="2" height="1" class="LLine"></td></tr>
	<tr>
		<td colspan="2" align="center" height="24">
		<gmjsp:button value="&nbsp;Submit&nbsp;" gmClass="button" disabled="<%=strSubmitDisable%>" buttonType="Save" onClick="fnSubmit();"/>
		<gmjsp:button value="&nbsp;Reset&nbsp;" gmClass="button" buttonType="Save" onClick="fnReset();" />
		<logic:notEqual name="frmAccessControlForm" property="groupId" value="">
		<logic:notEqual name="frmAccessControlForm" property="groupId" value="0">
		<gmjsp:button value="&nbsp;Void&nbsp;" disabled="<%=strSubmitDisable%>" gmClass="button" buttonType="Save" onClick="fnVoidGroup();"/>
		</logic:notEqual>
		</logic:notEqual>
		</td>
	</tr>
</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>