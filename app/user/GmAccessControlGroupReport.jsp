<%
/**********************************************************************************
 * File		 		: GmAccessControlGroupReport.jsp
 * Desc		 		: This screen is used for the Access Control Group Report
 * Version	 		: 1.0
 * author			: Gopinathan Saravanan
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
 <!-- \user\GmAccessControlGroupReport.jsp-->
<% 
String strWikiTitle = GmCommonClass.getWikiTitle("ACCESS_CONTROL_GROUP");
String struserJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_USER");
%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<bean:define id="gridData" name="frmAccessControlForm" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="grpType" name="frmAccessControlForm" property="groupType" type="java.lang.String"></bean:define>
<bean:define id="strAccessFl" name="frmAccessControlForm" property="strAccessFl" type="java.lang.String"></bean:define>
<% 
String strHeader="Access Control Group Report";
String strGrpType=grpType;
if(strGrpType.equals("92264")){
	strHeader="Security Group Report";
	strWikiTitle= "SECURITY_GROUP_REPORT";
}
else if(strGrpType.equals("92265")){
	strHeader="Module Report";
	strWikiTitle= "MODULE_REPORT";	
}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Access Control Group Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=struserJsPath%>/GmAccessControlSetup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>



<script>
	var objGridData;
	objGridData = '<%=gridData%>';
	var strAccessFl;
	strAccessFl='<%=strAccessFl%>'
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoad()">

<html:form action="/gmAccessControlGroup.do"> 
<html:hidden name="frmAccessControlForm" property="strOpt" value="edit"/>
<html:hidden name="frmAccessControlForm" property="groupId"/>
<html:hidden name="frmAccessControlForm" property="groupType"/>
	<table border="0" class="DtTable680" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2" height="25" class="RightDashBoardHeader">&nbsp;<%=strHeader%> </td>
			<td colspan="4" align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
		</tr>
			
        <tr>
           	<td colspan="6" height="50" >
				<div id="grpData" style="grid" width="680px" height="500px"></div>
		    </td>	
		</tr>	
    </table>		     	
 
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</html:form>
</HTML>

