<%
/**********************************************************************************
 * File		 		: GmUserADStatusReport.jsp 
 * Version	 		: 1.0
 * author			: Hardik Parikh
************************************************************************************/
%>
<!-- \user\GmUserADStatusReport.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import="java.util.ArrayList,java.util.*" %>
<% String strWikiTitle = GmCommonClass.getWikiTitle("USER_AD_STATUS_REPORT");%>

<HTML>
<HEAD>
<TITLE> Globus Medical: User AD Status Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

 
<bean:define id="gridData" name="frmUserADStatusReport" property="gridXmlData" type="java.lang.String"> </bean:define>
<% 
	boolean showGrid=true;	
	if(gridData!=null && gridData.indexOf("cell")==-1){
	   showGrid = false;			
	}	
%> 

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/displaytag.css");
      div.gridbox_dhx_skyblue table.obj tr td{
     border-width:0px 1px 0px 0px;
     border-color : #A4BED4;
}
</style>
<script>

var mygrid; 
function fnOnPageLoad() {

	var gridObjData = '<%=gridData%>';
	if(gridObjData!="")
	{
	   	mygrid = initGrid('dataGridDiv',gridObjData);   
	 }

}

function fnSubmit() {	
	
	document.frmUserADStatusReport.strOpt.value =  'search';	
	fnStartProgress(); 
	document.frmUserADStatusReport.submit();
}
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad()">
<html:form action="/gmUserADStatusReport.do">
<html:hidden property="strOpt" />
 

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">User AD Status Report</td>	
			
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>		
		</tr>
		
	 
		<tr><td colspan="4" height="1" class="line"></td></tr>
		<tr>
			<td  valign="top" colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    
                 <tr>
                 <!-- Custom tag lib code modified for JBOSS migration changes -->
					<td class="RightTableCaption" align="right" HEIGHT="30" >&nbsp;User:</td>						
					<td width="38%" ><gmjsp:dropdown controlName="selectedUser"  SFFormName="frmUserADStatusReport" SFSeletedValue="selectedUser"
						SFValue="alUserList"  codeId="USERID" codeName="USERNAME" defaultValue="[Choose One]"/>
           			</td>
           			
					<td class="RightTableCaption" align="right" HEIGHT="30" >&nbsp;Status:</td>						
					<td width="38%" ><gmjsp:dropdown controlName="selectedStatus"  SFFormName="frmUserADStatusReport" SFSeletedValue="selectedStatus"
						SFValue="alStatusList"  codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
						&nbsp;&nbsp; <gmjsp:button controlId="idSubmit" value="Load" gmClass="button" buttonType="Load" onClick="fnSubmit();" />
           			</td>
	
				</tr>
		   
			 	 <tr><td colspan="4" class="lline"></td></tr>
				   <tr>
				   <%if(showGrid){%>
				   <tr>
			            <td colspan="4" valign="top">
			            <div id="dataGridDiv" style="height:350px;width:698px;"></div>
						</td>
					</tr>
					<tr>
						<td colspan="4" align="center" height="30">	
							<div class='exportlinks'>Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="mygrid.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!--  | <img src='<%=strImagePath%>/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a>  -->
                    		</div>
						</td>
					</tr>	 
				<%}else{ %>
					<tr>
						<td class="RightText" colspan="4">
				    		No records found to display.
						</td>
					</tr>	
				 <%}%>
	    	       </tr> 
    			 	<tr><td colspan="4" class="ELine"></td></tr>              		
	  		 	</table>
  			   </td>
  		  </tr>	
    </table>	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>

