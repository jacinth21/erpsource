<%
/**********************************************************************************
 * File		 		: GmSecGrpModMapping.jsp
 * Desc		 		: This screen is used for the Module - Security Group Mapping 
 * Version	 		: 1.0
 * author			: Rajkumar J
************************************************************************************/
%>
<!-- \user\GmUserProfileContainer.jsp -->
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<% String struserJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_USER");
String strWikiTitle = GmCommonClass.getWikiTitle("USER_EDIT");
String strHeader="User - Group Mapping";
%>

<HTML>
<HEAD>

<TITLE>Globus Medical: Module Secutiry Group Mapping</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<link rel="stylesheet" type="text/css"
       href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=struserJsPath%>/GmUserProfileContainer.js"></script>
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.css">
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxToolbar/dhtmlxtoolbar.js"></script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<script>

function fnpageload(){

	myTabbar = new dhtmlXTabBar("my_tabbar");
	var frm = document.frmUserList;
	var  strOpt  = frm.strOpt.value;
	var ldapObj  = frm.ldapID;
	myTabbar.setSkin("winscarf");
	myTabbar.setImagePath("extweb/dhtmlx/dhtmlxTabbar/imgs/");
	myTabbar.addTab("NewUser", "New User");
	myTabbar.addTab("Company", "Company");
	global_param = "&companyInfo="+encodeURIComponent(JSON.stringify(top.gCompanyInfo));

	myTabbar.attachEvent("load", function(id,last_id){
		myTabbar.setTabActive("NewUser");
	});
	var frm = document.frmUserList;
	
	myTabbar.attachEvent("onSelect", function(id,last_id){
	if(id=="NewUser"){
	var strUserLoginName = frm.userLoginName.value;
	var strOpt = frm.strOpt.value;
			myTabbar.cells(id).attachURL("/gmUserListAction.do?userLoginName="+strUserLoginName+'&strOpt='+strOpt+'&ldapID='+ldapObj.value+global_param);

		}
	else if(id=="Company"){
		var strUserLoginName = frm.userLoginName.value;
		myTabbar.cells(id).attachURL("/gmPartyCompanyMapAction.do?strOpt=Company&userLoginName="+strUserLoginName+'&ldapID='+ldapObj.value+global_param);
		}	
	     return true;
	 });

	myTabbar.setTabActive("NewUser");
	 
	}
	
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnpageload();" onkeyup="fnEnter();">
<form name="frmUserList" method="post" action="/gmUserListAction.do">
<html:hidden property="strOpt" name="frmUserList"  />
<html:hidden property="huserLoginName" name="frmUserList"/>
<html:hidden property="strForwardName" name="frmUserList"/>
<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		  	<tr>
		   <td colspan="5">
		   <table  border="0" cellspacing="0" cellpadding="0"> 

		<td height="25" class="RightDashBoardHeader" width="350">&nbsp;User Profile</td>
		<td align="right" class=RightDashBoardHeader><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
	</table>
	</td>
	</tr>
	<tr >
			<td class="RightTableCaption" align="right" HEIGHT="30" width="200">&nbsp;<gmjsp:label type="MandatoryText" SFLblControlName="User Name:" td="false" /></td>
			<td class="RightText">&nbsp;<html:text size="20" name="frmUserList" property="userLoginName" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8') ;" onblur="changeBgColor(this,'#ffffff');" maxlength="20" tabindex="1"/></td>
			<td class="RightTableCaption" align="right" HEIGHT="30">&nbsp;<gmjsp:label type="MandatoryText" SFLblControlName="Authentication:" td="false" /></td>
			<td class="RightText">&nbsp;<gmjsp:dropdown controlName="ldapID" SFFormName="frmUserList" SFSeletedValue="ldapID" SFValue="alLdap" codeId="ID" codeName="NAME" tabIndex="2"></gmjsp:dropdown></td>
		    <td width="200">&nbsp;&nbsp;<gmjsp:button value="Load" gmClass="button" buttonType="Load" onClick="fnGo();" tabindex="3"/> 
		    </td>
		</tr>
		<tr >  
		<td colspan="5" id="my_tabbar"style="height:650Px;width:920px"></td>
	
		</tr>
</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
