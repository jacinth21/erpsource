<%
/**********************************************************************************
 * File		 		: GmSecGrpModMapping.jsp
 * Desc		 		: This screen is used for the Module - Security Group Mapping 
 * Version	 		: 1.0
 * author			: Rajkumar J
************************************************************************************/
%>

<!-- \user\GmPartyCompanyMap.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<bean:define id="alAvailableComp" name="frmUserList" property="alAvailableComp" type="java.util.ArrayList"></bean:define>
<bean:define id="alAsscompany" name="frmUserList" property="alAsscompany" type="java.util.ArrayList"></bean:define>
<bean:define id="strAccessFl" name="frmUserList" property="strAccessFl" type="java.lang.String"></bean:define>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ page import="com.globus.common.servlets.GmServlet"%>


<% 
String struserJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_USER");
String strWikiTitle = GmCommonClass.getWikiTitle("USER_GROUP_MAPPING");
String strHeader="User - Group Mapping";
HashMap hmValue = null;
//Enable/disable button
	String strDisable = "";
	if(!strAccessFl.equals("Y")){
		strDisable = "true";
	}
%>

<HTML>
<HEAD>

<TITLE>Globus Medical: Module Secutiry Group Mapping</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<link rel="stylesheet" type="text/css"
       href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCombo/dhtmlxcombo.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=struserJsPath%>/GmUserProfileContainer.js"></script>
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.css">
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxToolbar/dhtmlxtoolbar.js"></script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<script>
var FormVldArrLen = <%=alAvailableComp.size()%>;

<% 
hmValue = new HashMap();
for(int i=0; i<alAvailableComp.size(); i++){
	hmValue = (HashMap)alAvailableComp.get(i);
 %>
var FormVldArray<%=i%> = "<%=hmValue.get("COMPANYID")%>|<%=hmValue.get("COMPANYNM")%>";
<%}%>
var FormAssignedToLen = <%=alAsscompany.size()%>;
<% HashMap hmResult = new HashMap();
	for(int i=0; i<alAsscompany.size(); i++){
	hmResult = (HashMap)alAsscompany.get(i);
%>
var FormAssignedTo<%=i%> = "<%=hmResult.get("COMPANYID")%>|<%=hmResult.get("COMPANYNM")%>";
<%}%>

</script>
</HEAD>
<BODY  onload="fnOnPageLoadPlant();" >
<form name="frmUserList" method="post" action="/gmPartyCompanyMapAction.do">
<html:hidden property="strAssCompany" name="frmUserList"/>
<html:hidden property="strOpt" name="frmUserList"  />
<html:hidden property="userLoginName" name="frmUserList"  />

<table style="margin: 0 0 0 0; width: 820px; border: 1px solid  #676767;" border="0" cellspacing="0" cellpadding="0">
		
	<tr><td class="LLine" height="1" colspan="5"></td></tr>	 
	<tr class="shade" height="40" colspan="6">
		<td >
			<div   colspan="2" style="text-align:center; width:310px; height:10px; margin: 42px 0 0 105px;font:bold;">Available Companies</div>
		</td>
		<td ><div  colspan="4" style="text-align:center; width:290px; height:10px; margin-top:42px;font:bold;">Associated Companies</div></td>
	</tr>
	<tr class="shade" height="40" colspan="6">
		<td >
			<div   style="text-align:center; height:270px; margin: 30px 0 0 105px;">
            <select name="combo" id="AssingedNameCombo" size="15" multiple="multiple" style="width: 200px" ></select>
					&nbsp;&nbsp;&nbsp;
			<gmjsp:button align="middle" name="Add" value=">>" controlId="Add" gmClass="button" style="width: 2em;height:2em;font-weight:bold;font-size: 12px;vertical-align: 105px;" buttonType="Load" onClick="fnAssignedToNames();" />
			</div>
		</td>
		<td >
			<div  style="text-align:left; height:270px; margin-top:30px;display:">
				<gmjsp:button align="middle" name="Delete" value="<<" controlId="Delete" gmClass="button" style="width: 2em;height:2em;vertical-align: 105px;" buttonType="Load" onClick="fnRemoveNames();" />
					&nbsp;&nbsp;&nbsp;
				<select name="combo" id="AssignedToCombo" size="15" multiple="multiple" style="width: 200px;" >
			</div>
		</td>
	</tr>	
	<tr><td colspan="6" height="1" class="LLine"></td></tr>
	<tr>
        <td colspan="2" align="center" height="25">
            <jsp:include page="/common/GmIncludeLog.jsp" >
            <jsp:param name="FORMNAME" value="frmUserList" />
		    <jsp:param name="ALNAME" value="alLogReasons" />
		    <jsp:param name="LogMode" value="Edit" />	
		    <jsp:param name="Mandatory" value="yes" />																				
			</jsp:include>	
		</td>
	</tr>
	<tr>
		<td colspan="7" height="40" align="center">
	   		<div colspan="7" id="divHideButton">
			<gmjsp:button style="width: 5em"  value="Submit" name="Btn_Submit" buttonType="Save" disabled="<%=strDisable%>" gmClass="button" onClick="fnSubmitAss();" />&nbsp;
			</div>
		</td>
	</tr>				   	
</table>	
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
