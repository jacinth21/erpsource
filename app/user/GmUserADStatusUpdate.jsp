<%
/**********************************************************************************
 * File		 		: GmUserADStatusUpdate.jsp 
 * Version	 		: 1.0
 * author			: Hardik Parikh
************************************************************************************/
%>

<!-- \user\GmUserADStatusUpdate.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import="java.util.ArrayList,java.util.*" %>
<% String strWikiTitle = GmCommonClass.getWikiTitle("USER_AD_STATUS_UPDATE");%>

<HTML>
<HEAD>
<TITLE> Globus Medical: User AD Status Update </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
 
<bean:define id="gridData" name="frmUserADStatusUpdate" property="gridXmlData" type="java.lang.String"> </bean:define>
<% 
	boolean showGrid=true;	
	if(gridData!=null && gridData.indexOf("cell")==-1){
	   showGrid = false;			
	}	
%>
 

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/displaytag.css");
      div.gridbox_dhx_skyblue table.obj tr td{
     border-width:0px 1px 0px 0px;
     border-color : #A4BED4;
}
</style>
<script>

var mygrid; 
function fnOnPageLoad() {

	var gridObjData = '<%=gridData%>';
	if(gridObjData!="")
	{
	   	mygrid = initGrid('dataGridDiv',gridObjData);   
	 }

}

function fnLoad() {

	fnValidateTxtFld('searchUser',' User ');
	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}	
	
	document.frmUserADStatusUpdate.strOpt.value =  'load';	 
	fnStartProgress();
	document.frmUserADStatusUpdate.submit();
}

function fnSubmit() {
	var gridrows =mygrid.getChangedRows(",");
	if(gridrows==''){
		Error_Details(' No change(s) to Update.');
		
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
			
	}else {	
		var gridrowsarr = gridrows.toString().split(",");
		var arrLen = gridrowsarr.length;
		var isInValidPr =false;
		var strInputString = '';
		for (var i=0;i<arrLen;i++)
		{
	        rowId=gridrowsarr[i];	        
			adstatus = mygrid.cellById(rowId,4).getValue();	
			reason = mygrid.cellById(rowId,5).getValue();		
			
			strInputString = strInputString+ rowId +'^'+ adstatus +'^'+reason +'|';
		}
		   document.frmUserADStatusUpdate.hinputString.value =  strInputString;
	}	
	
	document.frmUserADStatusUpdate.strOpt.value =  'save';	
	fnStartProgress(); 
	document.frmUserADStatusUpdate.submit();
}
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad()">
<html:form action="/gmUserADStatusUpdate.do">
<html:hidden property="strOpt" />
<html:hidden property="hinputString" value=""/> 

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">User AD Status Update</td>	
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>		
		</tr>
		
	 
		<tr><td colspan="4" height="1" class="line"></td></tr>
		<tr>
			<td  valign="top" colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    
                 <tr>
					<td class="RightTableCaption" align="right"><font color="red">*</font>User Name:&nbsp;&nbsp;</td>
					<!-- Struts tag lib code modified for JBOSS migration changes -->
					<td><html:text property="searchUser"  size="10" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;&nbsp;
					<gmjsp:button controlId="idSubmit" value="Load" gmClass="button" buttonType="Load" onClick="fnLoad();" /></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
		   
			 	 <tr><td colspan="4" class="lline"></td></tr>
				   <tr>
				   <%if(showGrid){%>
			            <td colspan="4" valign="top">
			            <div id="dataGridDiv" style="height:350px;width:698px;"></div>
						</td>
				<%}else{ %>
					<td class="RightText" colspan="4">
				    	No records found to display.
					</td>	
				 <%}%>
	    	       </tr> 
    			 	<tr><td colspan="4" class="ELine"></td></tr> 
              		 <tr height="30">
              		   <%if(showGrid){%>
		               	<td colspan="4" align="center">
		                	 <gmjsp:button controlId="idSubmit" value="Update" gmClass="button" buttonType="Save" onClick="fnSubmit();" />  
			            </td>
	             <%} %>
	               </tr>
	  		 	</table>
  			   </td>
  		  </tr>	
    </table>	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>

