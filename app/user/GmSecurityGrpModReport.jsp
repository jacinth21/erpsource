<%
/**********************************************************************************
 * File		 		: GmSecurityGrpModReport.jsp
 * Desc		 		: This screen is used for Module Security Group Mapping attribute Report
 * Version	 		: 1.0
 * author			: Dhana Reddy
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

 <!-- \user\GmSecurityGrpModReport.jsp -->
<% 
String struserJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_USER");
String strWikiTitle = GmCommonClass.getWikiTitle("SCGRP_MODULE_MAPPING_REPORT");
%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<bean:define id="gridData" name="frmUserGroup" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="strAccessFl" name="frmUserGroup" property="strAccessFl" type="java.lang.String"> </bean:define>
<HTML>
<HEAD>
<TITLE> Globus Medical: Access Control Group Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=struserJsPath%>/GmUserGroup.js"></script>
<style>
table.DtTable700 {
	margin: 0 0 0 0;
	width: 700px;
	border: 1px solid  #676767;
}
</style>
<script>
	var objGridData;
	objGridData = '<%=gridData%>';
	var strAccess;
	strAccess='<%=strAccessFl%>'
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnPageLoad()">
<html:form action="/gmUserGroup.do"> 
<html:hidden name="frmUserGroup" property="grpMappingID"/>
<html:hidden name="frmUserGroup" property="strOpt" value="SCMMAP"/>
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2" height="25" class="RightDashBoardHeader">&nbsp;Module - Security Group Mapping Report</td>
			<td colspan="4" align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
		</tr>
	</table>
	<table width="700" cellpadding=0 cellspacing="0">
		<tr>
           	<td  height="500" >
				<div id="grpData" style="grid" height="500px"></div>
		   </td>	
		</tr>	
	</table>	
  <%@ include file="/common/GmFooter.inc" %>
  </html:form>
</BODY>

</HTML>

