
<%
	/**********************************************************************************
	 * File		 		: GmLogin.jsp
	 * Desc		 		: This screen is used for the Login Process
	 * Version	 		: 1.0 
	 * author			: Dhinakaran James
	 ************************************************************************************/
%>



<!--globusMedApp\GmLogin.jsp-->

<%@ include file="/common/GmHeader.inc" %>
<%@ include file="/common/GmPiwif.inc"%>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmFileReader"%>

<%
String metaInfo = "";
try{
	  metaInfo = GmFileReader.findVersionInfo();
	}
catch (Exception e) {
e.printStackTrace();
}
String strUserAgent = "";
	/* <--String strClientSysType = ""--> */
	String strShowBulletBoard = System.getProperty("ENV_NOTICE");

 String strGen = GmServlet.getCookieValue(request,"gen");
 
if(strGen != null )
{
	request.getRequestDispatcher("/GmLogonServlet").forward(request,response);
}
else 
{
	strUserAgent = GmCommonClass.parseNull((String)request.getHeader("user-agent"));
	strClientSysType = (strUserAgent.toUpperCase().indexOf("IPAD") != -1)?"IPAD":"PC";
%>
<!DOCTYPE html>

<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<jsp:include page="/GmIncPortalTitle.jsp"/>

<head>
	<meta charset="utf-8" />
	<title>Globus Medical - Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="stylesheet" media="all" href="_ui/css/main.css" />
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--[if lte IE 8]>
		<link rel="stylesheet" type="text/css" media="all" href="_ui/css/ie.css" />
	<![endif]-->
	<!--[if IE 9]>
    	<link rel="stylesheet" type="text/css" media="all" href="_ui/css/ie-9.css" />
    <![endif]-->
<link rel="apple-touch-icon" href="<%=strImagePath%>/SpineIT_Ipad.gif">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxTouch/codebase/touchui.js" type="text/javascript" charset="utf-8"></script>
<script>
dhx.ready(function() {
	dhx.ui.fullScreen();//To make the full screen when it loads from IPAD.				
});//end of ready
function fnSubmitLogon()
{
	Error_Clear();
	obj1 = document.fLogon.Txt_Username;
	obj2 = document.fLogon.Txt_Password;
	
	if (obj1.value == '')
	{
		Error_Details(message[5]);
	}
	if (obj2.value == '')
	{
		Error_Details(message[6]);
	}

	if (ErrorCount > 0)
	{
			Error_Show();
			return false;
	}
}

function fnCheckUserId(){
	varUserId = document.fLogon.Txt_Username.value;
	
	Error_Clear();
	if (varUserId == '')
	{
		Error_Details('Please provide your <B>Username</B> to continue...');
		Error_Show();
		return false;
	}
	else{
		return true;
		}
}

function fnForgotPass()
{
	var blIsUserIdEntered = false;
	blIsUserIdEntered = fnCheckUserId();
	if(blIsUserIdEntered){
		windowOpener("/GmLogonServlet?hAction=Forgot&Txt_Username="+varUserId,"FrgPwd","resizable=yes,scrollbars=yes,top=150,left=200,width=1000,height=600");
		//document.fLogon.hAction.value = "Forgot";
		//document.fLogon.action = "/GmLogonServlet";
		//document.fLogon.submit();
	}
}

function fnTxtFocus()
{
	obj1 = document.fLogon.Txt_Username;
	cbj1.focus();
}

function fnChangePass()
{
	var blIsUserIdEntered = false;
	blIsUserIdEntered = fnCheckUserId();
	varUserId = document.fLogon.Txt_Username.value;
	
	if(blIsUserIdEntered){
	windowOpener("/GmLogonServlet?hAction=Change&Txt_Username="+varUserId,"ChgPwd","resizable=yes,scrollbars=yes,top=150,left=200,width=1000,height=600");
	}
}

</script>
</head>

<body onLoad="document.fLogon.Txt_Username.focus();">
	<div class ="shownotice">
		<%if(!strShowBulletBoard.equals("")){%>
					<marquee scrollamount="2" behavior="alternate"> <font color="red"><%=strShowBulletBoard %></font> </marquee>
		<%}%>
	</div>
	<div class="container" >
	
	<div id="content">
		<div id="login-container">
			<div id="login-box" >
				<h1></h1>
				<h2>
					<strong>Login</strong>
				</h2>
				
				<form  name="fLogon" method="post" id="login-form" action="<%=strServletPath%>/GmLogonServlet">
				
				<input type="hidden" name="hAction">
				
					<ul>
						<li>
							<label for="username">Username:</label>
							<input type="text" name="Txt_Username" id="username" />
						</li>
						<li>
							<label for="username">Password:</label>
							<input type="password" name="Txt_Password" id="password" />
						</li>
						<li>
							<label>
							<%if(strClientSysType.equals("IPAD")){%>
								<span><input type="checkbox" name="Chk_Remember" style="width:30px;height:30px;" checked="checked"/> Remember Me</span>
							<%}else{%>
								<input type="checkbox" name="Chk_Remember" /> Remember Me
							<%}%>
							</label>
						</li>						
						<li class="center">
							<button type="submit" class="button">Sign in<span></span></button>
						</li>
						<li class="center">
						<label >
						<a href="javascript:fnChangePass();" class="HyperText">Reset Password</a>
						</label>	
						<label>
						<a href="javascript:fnForgotPass('<%=strCommonPath%>');" class="HyperText">Forgot Password</a>
						</label>					
						</li>			
					</ul>
				</form>
			</div>
			<!-- / login-box -->
						
		</div>
		<!-- / login-container -->
		<ul>
		<li class="center">
 
</li>
</ul>
      <div class="buildinfo center">
        <br/><%=metaInfo%>
      </div>
      
	</div>
	<!-- / content -->
	<hr />

</div>
<!-- / container -->


<script  src="<%=strJsPath%>/common/jquery.min.js">></script>
<script src="_ui/js/login.js"></script>

<!-- 
<table border="0" width="770" cellspacing="1" cellpadding="0" bgcolor="#CCCCCC">
	<tr>
		<!--<td bgcolor="white" align="center" colspan="2"><img src="<%=strImagePath%>/m_logo.gif" border="0"><br><br></td>
		<td bgcolor="white" align="center" colspan="2"><img src="<%=strImagePath%>/globus_header.jpg" border="0"></td>
	</tr>
	<tr>
		<td bgcolor="white" height="200" valign="top"></td>
		<td bgcolor="white" height="200" align="right" valign="top">
		<table border="0" width="300" cellspacing="0" cellpadding="0">
			<tr>
				<td rowspan="7" bgcolor="#666666" width="1"></td>
				<td colspan="2" bgcolor="#666666" height="1"></td>
				<td rowspan="7" bgcolor="#666666" width="1"></td>
			</tr>
			<tr>
				<td colspan="4" class="RightDashBoardHeader" height="20" align="center">Application Login</td>
			</tr>
			<tr height="25">
				<td class="TableText" align="right">Username :</td>
				<td>&nbsp;<input type="text" size="15" value="" name="Txt_Username" class="InputArea">
			</tr>
			<tr height="25">
				<td class="TableText" align="right">Password :</td>
				<td>&nbsp;<input type="password" size="15" value="" name="Txt_Password" class="InputArea">
			</tr>
			<tr height="25">
				<td class="TableText" align="right">Remember Me :</td>
				<td>&nbsp;<input type="checkbox"  name="Chk_Remember" >
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td height="30"><input type="submit" value="Submit" class="button">&nbsp; <input type="reset" value="Reset" class="button"></td>
			</tr>
			<tr>
				<td colspan="4" height="30" align="center"><a href="javascript:fnChangePass();" class="HyperText">Change Password</a>&nbsp;&nbsp;&nbsp; 
				<a href="javascript:fnForgotPass('<%=strCommonPath%>');" class="HyperText">Forgot Password</a></td>
			</tr>
			<tr>
				<td colspan="4" height="1" colspan="4" bgcolor="#666666"></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td bgcolor="white" align="center" height="50" colspan="2"><b><font color="red">Globus Employees : Use your Windows Username and Password for Login.</font></b>
		</td>
	</tr>
	
	<tr>
		<td bgcolor="white" align="center" height="30" class="FooterText" colspan="2">All Rights Reserved. &copy; 2004. Globus Medical Inc.</td>
	</tr>
</table>
 -->

</body>
</html>
<%
}
%>
