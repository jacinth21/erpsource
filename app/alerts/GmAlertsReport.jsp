<%
	/**********************************************************************************
	 * File		 		: GmAlertsReport.jsp
	 * Version	 		: 1.0
	 ************************************************************************************/
%>
 <!-- \alerts\GmAlertsReport.jsp -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@include file="/common/GmHeader.inc"%>
<%@page import="java.util.Date"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<bean:define id="gridData" name="frmAlerstReport" property="gridXmlData" type="java.lang.String"></bean:define>
<bean:define id="intAlertRptSize" name="frmAlerstReport" property="intAlertRptSize" type="java.lang.Integer"></bean:define>
<bean:define id="fromDate" name="frmAlerstReport" property="fromDate" type="java.lang.String"> </bean:define>
<bean:define id="toDate" name="frmAlerstReport" property="toDate" type="java.lang.String"> </bean:define>
 
<%
String stralertsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ALERTS");
	String strWikiTitle = GmCommonClass.parseNull((String)GmCommonClass.getWikiTitle("ALERTS"));
	String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	Date dtToday = GmCommonClass.getCurrentDate(strApplnDateFmt);
	String strTodaysDate = GmCommonClass.getStringFromDate(dtToday,strApplnDateFmt);
	
	// For pagination display in Jsp. R
	String strRowDisp    = GmCommonClass.parseNull((String) GmCommonClass.getRuleValue("ROWDSP","ROWDSP"));
	String strPageNation = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue("PGEDSP","PGEDSP"));
	
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Alert Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
 

<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"	media="print" />
<link rel="STYLESHEET" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=stralertsJsPath%>/GmAlertsReport.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>

<script type="text/javascript">
var gridObjData = '';
gridObjData = '<%=gridData%>'; 
var todaysdate  = '<%=strTodaysDate%>';
var strRowDisp = '<%=strRowDisp%>';
var strPageNation = '<%=strPageNation%>';
var fromdate = '<%=fromDate%>';
var todate = '<%=toDate%>';
var format = '<%=strApplnDateFmt%>';

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form method="POST" action="/gmAlertsReportAction.do">
<html:hidden property="strOpt" name="frmAlerstReport" />
<html:hidden property="alertID" name="frmAlerstReport" />
<html:hidden property="strInputAlertIds" name="frmAlerstReport" />
<html:hidden property="haction" name="frmAlerstReport" />
	
	<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr>
			<td  colspan="5" height="25" class="RightDashBoardHeader">Alerts</td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			 <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
	       </td>
		</tr>

		<tr> 
			<td class="RightText" align="right" HEIGHT="30" style="font-weight:bold">&nbsp;Type:</td>
			<td class="RightText">&nbsp;<gmjsp:dropdown controlId="alertsType"	controlName="alertsType" SFFormName="frmAlerstReport"
				SFSeletedValue="alertsType" tabIndex="1" width="150" SFValue="alType" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" /></td>
				
			<td class="RightText" align="right" HEIGHT="30" style="font-weight:bold">&nbsp;Reason:</td>
			<td class="RightText">&nbsp;<gmjsp:dropdown controlId="alertsReason"	controlName="alertsReason" SFFormName="frmAlerstReport"
				SFSeletedValue="alertsReason" tabIndex="2" width="150" SFValue="alReason" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" /></td>
			
			<td class="RightText" align="right" HEIGHT="30" style="font-weight:bold">&nbsp;Actionable:</td>
			<td class="RightText">&nbsp;<gmjsp:dropdown	controlName="alersActionable" SFFormName="frmAlerstReport"	
				SFSeletedValue="alersActionable" tabIndex="3" width="150" SFValue="alActionable"	codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"
				controlId="alersActionable"/></td>	
		</tr>
		<tr>
			<td colspan="6" height="1" class="LLine"></td>
		</tr>
		 <tr class="shade">
		      <td class="RightText" align="right" HEIGHT="30" style="font-weight:bold">&nbsp;From Date:</td>
				  <td>&nbsp;<gmjsp:calendar SFFormName="frmAlerstReport" controlName="fromDate" gmClass="InputArea" textValue="<%=(fromDate.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(fromDate,strApplnDateFmt).getTime())))%>" tabIndex="4" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			  </td>
			  <td class="RightText" align="right" HEIGHT="30" style="font-weight:bold">&nbsp;To Date:</td>    
				  <td>&nbsp;<gmjsp:calendar SFFormName="frmAlerstReport" controlName="toDate" gmClass="InputArea" textValue="<%=(toDate.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(toDate,strApplnDateFmt).getTime())))%>" tabIndex="5" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/> 		 	
			 </td>
			<td colspan="4">&nbsp;				
				<gmjsp:button name="Btn_Load" value="&nbsp;Load&nbsp;" gmClass="Button" buttonType="Load" onClick="javascript:fnReload(this);" tabindex="6" /><BR>				
			</td>
		  </tr>
		  	<%if(intAlertRptSize > 0){%>
		  		<tr>
					<td colspan="10" height="50" >
					    <div id="dataGridDiv" style="height: 450px;width: 998px"></div>
					    <div id="pagingArea" style="width: 870px"></div>
					</td>
				</tr>
			    	<tr class="shade">
		          		<td colspan="6" class="RightTableCaption" height="35" align="center" >&nbsp;Choose Action: 
				    	 &nbsp;<gmjsp:dropdown	controlName="readFlag" SFFormName="frmAlerstReport"	
						SFSeletedValue="readFlag" tabIndex="7" width="150" SFValue="alFlag"	codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"
						controlId="readFlag"/>&nbsp;&nbsp;&nbsp;	<gmjsp:button value="&nbsp;Submit&nbsp;"  gmClass="Button" buttonType="Save" onClick="javascript:fnSubmit();" tabindex="13" /></td>
		       		</tr>
		      	<%}else{%>
		      	<tr><td colspan="6" height="1" class="LLine"></td></tr>
		      		<tr><td align="center" colspan="6" height="30" height="35" class="RightTableCaption" style="color:red">Nothing found to display!</td></tr>
		       <%}%>
		</table>		
</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
