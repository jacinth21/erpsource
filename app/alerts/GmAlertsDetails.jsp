
<%
	/**********************************************************************************
	 * File		 		: GmAlertDetails.jsp
	 * Version	 		: 1.0
	 * Auther			: arajan
	 ************************************************************************************/
%>
<%@include file="/common/GmHeader.inc"%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Alert Details</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"	media="print" />
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script src="<%=strJsPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>


</HEAD>
<style>
table.DtTable440 {
	margin: 0 0 0 0;
	width: 440px;
	border: 1px solid  #676767;
}
</style>
<script>

function fnClose(){
	CloseDhtmlxWindow();
	return false;
}
</script>
<BODY leftmargin="20" topmargin="10" ">
<html:form method="POST" action="/gmAlertsReportAction.do">
	
	<table border="0" class="DtTable440" cellspacing="0" cellpadding="0">
		<tr> 
             <td colspan="2" class="RightDashBoardHeader" height="20">&nbsp;Alert Details</td>
        </tr>
        <tr> 
			<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Type:</td>
			<td class="RightText">&nbsp;<bean:write property="atype" name="frmAlerstReport"/></td>
			 
		</tr>
		<tr class="Shade"> 
			<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Reason:</td>
			<td class="RightText">&nbsp;<bean:write property="reason" name="frmAlerstReport"/></td>
			 
		</tr>
		<tr><td colspan="2" height="1" class="LLine"></td></tr>
		<tr> 
			<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Subject:</td>
			<td class="RightText">&nbsp;<bean:write property="subject" name="frmAlerstReport"/></td>
			 
		</tr>
		<tr><td colspan="2" height="1" class="LLine"></td></tr>
		<tr class="Shade"> 
			<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Date:</td>
			<td class="RightText" >&nbsp;<bean:write property="alert_date" name="frmAlerstReport"/></td>
			 
		</tr>
		<tr><td colspan="2" height="1" class="LLine"></td></tr>
		<tr> 
			<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Message:</td>
			<td class="RightText" >&nbsp;<bean:write property="message" name="frmAlerstReport"/></td>
			 
		</tr>
		<tr><td colspan="2" height="1" class="LLine"></td></tr>
		<tr class="Shade"> <td colspan="2" height="35" align="center"><gmjsp:button name="Close_button" value="Close" gmClass="button" buttonType="Load" onClick="fnClose();" /></td></tr>
		</table>		
</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
