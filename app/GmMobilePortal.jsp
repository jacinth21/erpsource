<%@page import="java.net.URLEncoder"%>
<%
/**********************************************************************************
 * File		 		: GmMobilePortal.jsp
 * Desc		 		: This screen is used for the Mobile Portals  - Frameset
 * Version	 		: 1.0
 * author			: cskumar
************************************************************************************/
%>

<!--globusMedApp\GmMobilePortal.jsp-->


<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import="com.globus.common.servlets.GmServlet" %>
<%
	GmServlet gmServlet = new GmServlet();
	gmServlet.checkSession(response,session);
	String strMenuPath = GmCommonClass.getString("GMMENU");
	String strPartyId =  GmCommonClass.parseNull((String) session.getAttribute("strSessPartyId"));
	String strPlantId =  GmCommonClass.parseNull((String) session.getAttribute("strSessPlantId"));
 	String strCompanyId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));
	String strJsDateFmt =  GmCommonClass.parseNull((String) session.getAttribute("strSessJSDateFmt"));

	String strTkn = GmCommonClass.parseNull((String) session.getAttribute("strSessToken"));
	String strUserID = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
	String strUserNm = GmCommonClass.parseNull((String) session.getAttribute("strSessUserNm"));
	String strDeptID = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
	String strDeptSeq = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptSeq"));
	String strSHNm = GmCommonClass.parseNull((String) session.getAttribute("strSessShName"));
	String strAcid = GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl"));
	String strLoginTS = GmCommonClass.parseNull((String) session.getAttribute("strSessLoginTS"));
	String strExtAccess = GmCommonClass.parseNull((String) session.getAttribute("strExtAccess"));
	String strDivID = GmCommonClass.parseNull((String) session.getAttribute("strSessDivId"));
	String strFName = GmCommonClass.parseNull((String) session.getAttribute("strSessFName"));
	String strLName = GmCommonClass.parseNull((String) session.getAttribute("strSessLName"));
	String strSwitchUserFl = GmCommonClass.parseNull((String) session.getAttribute("strSwitchUserFl"));
	String strSessRepDiv = GmCommonClass.parseNull((String) session.getAttribute("strSessRepDiv"));
	
%>

<html>
<head>
<title>Globus Medical: CRM Portal</title>
  <script language="javascript">
    var gCompanyInfo = {};
    gCompanyInfo.cmpid = "<%=strCompanyId%>";
    gCompanyInfo.plantid = "";
    gCompanyInfo.token = "";
    var gCompJSDateFmt = "<%=strJsDateFmt%>";
    gCompanyInfo.partyid="<%=strPartyId%>";  
    var crmInfo = {};
    crmInfo.token = "<%=strTkn%>";
    crmInfo.userid = "<%=strUserID%>";
    crmInfo.usernm = "<%=strUserNm%>";
    crmInfo.deptid = "<%=strDeptID%>";
    crmInfo.deptseq = "<%=strDeptSeq%>";
    crmInfo.shname = "<%=strSHNm%>";
    crmInfo.acid = "<%=strAcid%>";
    crmInfo.logints = "<%=strLoginTS%>";
    crmInfo.partyid = "<%=strPartyId%>";
    crmInfo.extaccess = "<%=strExtAccess%>";
    crmInfo.divid = "<%=strDivID%>";
    crmInfo.fname = "<%=strFName%>";
    crmInfo.lname = "<%=strLName%>";
    crmInfo.companyid = "<%=strCompanyId%>";
    crmInfo.switchuser = "<%=strSwitchUserFl%>";
    crmInfo.repdiv = "<%=strSessRepDiv%>";
</script>
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">

</head>

<FRAMESET border=1 frameSpacing=0 rows=80,*  MARGINWIDTH="0" MARGINHEIGHT="0" LEFTMARGIN="0" TOPMARGIN="0">
   <FRAME border=0 name=TopFrame marginWidth=0 marginHeight=0
      src= "<%=strMenuPath%>/GmPortalHeader.jsp?company=hide&plant=hide"
      frameBorder=no noResize scrolling=no LEFTMARGIN="0" TOPMARGIN="0" tabindex=-1>

 <FRAMESET border=1 name=fstMain framespacing=0>
    <FRAME border=1 name=LeftFrame
	src="GlobusCRM" frameBorder=0 bordercolor="#8B9ACF"
	scrolling="Yes"	marginHeight=0 tabindex=-1>    
 </FRAMESET>
</FRAMESET>
</html>
