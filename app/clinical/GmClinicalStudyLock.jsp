
<%
	/**********************************************************************************
	 * File		 		: GmClinicalStudyLock.jsp
	 * Desc		 		: This is a Transaction screen for Clinical Study Lock
	 * Version	 		: 1.0
	 * author			: 
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<bean:define id="gridData" name="frmClinicalStudyLockForm" property="gridXmlData" type="java.lang.String"></bean:define>
<bean:define id="submitAccess" name="frmClinicalStudyLockForm" property="submitAccess" type="java.lang.String"></bean:define>
<HTML>
<HEAD>
<TITLE>Globus Medical: Study Lock</TITLE>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalStudyLock.js"></script>
<script>
  	var objGridData = '';
	objGridData = '<%=gridData%>';
</script>
</HEAD>

<BODY leftmargin="10" topmargin="10" onLoad="fnOnPageLoad();">
<html:form action="/gmClinicalStudyLockAction.do?method=loadStudyLockDtls">
<html:hidden property="inputStr" value="" />
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value=""/>
<html:hidden property="strOpt"/>
<input type="hidden" name="hAction" value="" />
<input type="hidden" name="hRedirectURL" value="" />
<input type="hidden" name="hDisplayNm" value="" />
<input type="hidden" name="hStudyID" value="" />

		<table border="0" cellspacing="0" cellpadding="0" class="DtTable950">
			<tr style="height: 25px">
				<td style='height: 25px' class="Header" colspan="4">&nbsp;Study Lock</td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="dataGridDiv" class="grid" style="height: 383px;"></div></td>
			</tr>
			<% String strDisableBtnVal = submitAccess;
						if(strDisableBtnVal.equals("disabled")){
							strDisableBtnVal ="true";
						}
			%>
			<tr height="35">
			<td align="center" colspan="4" class="RightTableCaption">Choose Action:&nbsp;<gmjsp:dropdown controlName="statusId" SFFormName="frmClinicalStudyLockForm" SFSeletedValue="statusId" 
			 SFValue="alStudyStatus" codeId = "CODEID"  codeName = "CODENM" defaultValue="[Choose One]" />&nbsp;
			 <gmjsp:button gmClass="Button" buttonType="Save"value="Submit" name="btn_submit"onClick="fnSubmit();" disabled="<%=strDisableBtnVal%>" /></td>
			</tr>
			
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

