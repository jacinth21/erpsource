<%
/**********************************************************************************
 * File		 		: GmRptNDIPerImprove.jsp
 * Desc		 		: Report for Neck Disability Index % improvement from Pre-op
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>

<!-- clinical\GmRptNDIPerImprove.jsp -->
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ include file="/common/GmHeader.inc" %>

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
 
<bean:define id="hmCrossTabReport" name="frmOutcomeReport" property="hmCrossTabReport" type="java.util.HashMap"> </bean:define>
<%  
try{

	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	ArrayList alDrillDown = new ArrayList();
	
	
	ArrayList alHeader = GmCommonClass.parseNullArrayList((ArrayList)hmCrossTabReport.get("Header"));
	int headerSize = alHeader.size();
	for (int i = 0;i<headerSize; i+=3)
	{
		gmCrossTab.addLine((String)alHeader.get(i));
	} 
	
	for (int i = 1,j=3;j<headerSize; i+=3,j+=3)
	{
		gmCrossTab.add_RoundOff((String)alHeader.get(i),"0");
		gmCrossTab.add_RoundOff((String)alHeader.get(j),"1");
	} 
	
	// Setting the cross tab style 
	// Page Body Style
	gmCrossTab.setGeneralHeaderStyle("aaTopHeader");
	gmCrossTab.setDivHeight(200);
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setGroupingRequired(true);
	gmCrossTab.setValueType(0);
	gmCrossTab.setNoDivRequired(true); // this skips division for all columns
	gmCrossTab.addLine("Name");
	gmCrossTab.setNameWidth(140);
	gmCrossTab.setRowHighlightRequired(true);
	
		// Line Style information
//	gmCrossTab.setColumnDivider(false);
	gmCrossTab.setColumnLineStyle("borderDark");
	
	// Drill down link
	gmCrossTab.setLinkRequired(true);
	alDrillDown.add("LoanerDist");	
	gmCrossTab.setDrillDownDetails(alDrillDown);
	gmCrossTab.displayZeros(true);
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: NDI >=25% Impr</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>

function fnSubmit()
{
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmOutcomeReport.strOpt.value = 'save';
	document.frmOutcomeReport.submit();
}

function fnLoad()
{
	document.frmOutcomeReport.selectAll.checked = false;
	document.frmOutcomeReport.strOpt.value = "";
	document.frmOutcomeReport.action = "/gmRptOutcome.do?method=reportNDIPerImprove";
	document.frmOutcomeReport.submit();
}

function fnReport()
{
	document.frmOutcomeReport.action = "/gmRptOutcome.do?method=reportNDIPerImprove";
	document.frmOutcomeReport.strOpt.value = "Report";
	document.frmOutcomeReport.submit();
}	

function fnCallLoanerDistDetails(varId)
{
	document.frmOutcomeReport.action = "/gmRptOutcomeScores.do?method=reportNDIImproveDetail";
	document.frmOutcomeReport.treatmentId.value = varId;
	document.frmOutcomeReport.strOpt.value = "Report";
	document.frmOutcomeReport.submit();
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmRptOutcome.do"  >
<html:hidden property="strOpt" />
<html:hidden property="treatmentId" value="" />
	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">NDI >=25% Improvement</td>
		</tr>
		<tr>
			<td width="848" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
						<td colspan="2" align="center" height="30">&nbsp;
	                    	<jsp:include page="/clinical/GmStudyFilterInc.jsp" >
							<jsp:param name="FORMNAME" value="frmOutcomeReport" />
							</jsp:include>	
						</td>
					</tr>
			   	</table>
  			   </td>
  		  </tr>	  		
    </table>		    
    <table>
    <tr><td colspan="2" class="ELine"></td></tr> 
					<tr>
						<td colspan ="2" align=center> <%=gmCrossTab.PrintCrossTabReport(hmCrossTabReport) %></td>
					</tr>
	</table>					 	
</html:form>
<% } catch(Exception exp) {
exp.printStackTrace();
}
%>
</BODY>

</HTML>

