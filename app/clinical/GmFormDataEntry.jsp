 <%
 	/**********************************************************************************
 	 * File		 		: GmFormDataEntry.jsp
 	 * Desc		 		: This screen is used for the Forms Data entry
 	 * Version	 		: 1.0
 	 * author			: Dhinakaran James
 	 ************************************************************************************/
 %>
 
 <!-- clinical\GmFormDataEntry.jsp -->
 
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.clinical.beans.GmQuestionBean"%>
<%@ page import ="com.globus.clinical.data.GmQuestionMasterData"%>
<%@ page import ="com.globus.clinical.data.GmAnswerGrpData"%>
<%@page import="java.util.Date"%>
<%@page import="org.apache.commons.lang.SystemUtils"%>
<%@page import="java.text.DateFormat"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strTodaysDate = (String)session.getAttribute("strSessTodaysDate");
	String strAction = (String)request.getAttribute("hAction");
	response.setCharacterEncoding("UTF-8");
	ArrayList alStudyList = new ArrayList();
	ArrayList alPatientList = new ArrayList();
	ArrayList alFormsList = new ArrayList();
	ArrayList alPeriodList = new ArrayList();
	ArrayList alVerifyType = new ArrayList();
	ArrayList alVerifyList = new ArrayList();	
	ArrayList alFormVld = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALFRMVLD"));
	HashMap hmLists = new HashMap();
	HashMap hmReturn = new HashMap();
	HashMap hmQuestionsDetails = new HashMap();
	HashMap hmPatientFormDetails = new HashMap();
	HashMap hmPatients = new HashMap();

	String strFormNm = "";
	String strFormDt = "";
	String strInitials = "";
	String strCreatedBy = "";
	String strCreatedDt = "";
	String strLastUpdBy = "";
	String strLastUpdDt = "";
	String strVerifyFl = "";
	String strCalReqFl = "";
	String strCalculatedValue = "";
	String strMissingFollowUp = "";
	String strInitialNotReqFl = "";
	String strValidateFlds = "";
	String strEventFlag = "";
	String strDateFl = "";
	String strInitialsFl = "";
	String strPatListId = "";
	String strExamStatus = "";
	String strFormStatus = "";
	String strPatientIDE = "";
	String strVerfyLvlID = "";
	String strDeleteFl = "";
	String strStudyPeriodID = "";
	String strEventNo = "";
	String strVoidFormMsg = "<b><U><font color=\"#FF0000\"> This Form was Voided.</U></b>";
	String strSessPatientIDE = (String)session.getAttribute("strSessPatientId");
	String strButtonDisabled = "";
	boolean blNewFormFl = true;
	if (strAction == null)
	{
		strAction = (String)session.getAttribute("hAction");
	}
	strAction = (strAction == null)?"Load":strAction;
	
	hmLists = (HashMap)request.getAttribute("hmLists");
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	
	String strVoidAccessFl ="";
	String hOption = "";
	strVoidAccessFl = GmCommonClass.parseNull((String)request.getAttribute("VoidAccessFl"));
	hOption = GmCommonClass.parseNull((String)request.getAttribute("hOption"));
	strButtonDisabled = GmCommonClass.parseNull((String)request.getAttribute("strButtonDisabled"));
	if (hmReturn != null)
	{
		hmQuestionsDetails = (HashMap)hmReturn.get("QUESTIONDETAILS");
		hmPatientFormDetails = (HashMap)hmReturn.get("PATIENTFORMDETAILS");
		hmPatients = (HashMap)hmReturn.get("PATIENTDETAILS");
		alVerifyList = (ArrayList)hmReturn.get("VERIFYDETAILS");
		strCalReqFl = GmCommonClass.parseNull((String)hmReturn.get("CALREQFL"));
	}
	String strStudyId = (String)request.getAttribute("hStudyId") == null ?"":(String)request.getAttribute("hStudyId");
	String strPatientId = (String)request.getAttribute("hPatientId") == null ?"":(String)request.getAttribute("hPatientId");
	String strFormId = (String)request.getAttribute("hFormId") == null ?"":(String)request.getAttribute("hFormId");
	String strStudyPerId = (String)request.getAttribute("hStudyPeriod") == null ?"":(String)request.getAttribute("hStudyPeriod");
	String strDisblQNo = (String)request.getAttribute("hDisableQNo") == null ?"":(String)request.getAttribute("hDisableQNo");
	String strPatientInitials = (String)request.getAttribute("strPatientInitials") == null ?"":(String)request.getAttribute("strPatientInitials");
// get value form servlet for event dropdown.	
	String strEventLbl = (String)request.getAttribute("hEventLbl") == null ?"":(String)request.getAttribute("hEventLbl");
	String strAdvEventFl = (String)request.getAttribute("ADVEVEFL") == null ?"":(String)request.getAttribute("ADVEVEFL");
	
	if (hmLists != null)
	{
		//alStudyList = (ArrayList)hmLists.get("STUDYLIST");
		if (!strAction.equals("Load"))
		{
			//alPatientList = (ArrayList)hmLists.get("PATIENTLIST");
			alPeriodList = (ArrayList)hmLists.get("PERIODLIST");
			alFormsList = (ArrayList)hmLists.get("FORMLIST");
		}
	}
	if (!hmPatientFormDetails.isEmpty())
	{
		strFormDt = GmCommonClass.parseNull((String)hmPatientFormDetails.get("FORMDATE"));
		strMissingFollowUp = GmCommonClass.parseNull((String)hmPatientFormDetails.get("MISSINGFOLLOWUP"));
		strInitials = GmCommonClass.parseNull((String)hmPatientFormDetails.get("INITIALS"));
		strCreatedBy = GmCommonClass.parseNull((String)hmPatientFormDetails.get("CUSER"));
		strCreatedDt = GmCommonClass.parseNull((String)hmPatientFormDetails.get("CDATE"));
		strLastUpdBy = GmCommonClass.parseNull((String)hmPatientFormDetails.get("LUSER"));
		strLastUpdDt = GmCommonClass.parseNull((String)hmPatientFormDetails.get("LDATE"));
		strAction = "EditAns";
		strVerifyFl = GmCommonClass.parseNull((String)hmPatientFormDetails.get("VFL"));
		strVerifyFl = strVerifyFl.equals("Y")?"checked":"";
		strCalculatedValue = GmCommonClass.parseNull((String)hmPatientFormDetails.get("CVALUE"));
		strDateFl = GmCommonClass.parseNull((String)hmPatientFormDetails.get("DATEHISTORYFL"));
		strInitialsFl = GmCommonClass.parseNull((String)hmPatientFormDetails.get("INITIALSHISTORYFL"));
		strPatListId = GmCommonClass.parseNull((String)hmPatientFormDetails.get("PATLISTID"));
		strFormStatus = GmCommonClass.parseNull((String)hmPatientFormDetails.get("FORM_STATUS"));
		strExamStatus = GmCommonClass.parseNull((String)hmPatientFormDetails.get("EXAM_STATUS"));
		strDeleteFl = GmCommonClass.parseNull((String)hmPatientFormDetails.get("DELETEFL"));
		if(!strPatListId.equals("") && !strDeleteFl.equals("Y")){
			strExamStatus = strExamStatus.equals("")?"99999":strExamStatus;
			strExamStatus = "<img id=imgEdit style=cursor:hand src=" + strImagePath + "/" + strExamStatus + ".gif" +
			  "  align='bottom' title='Click to view Query Details' onClick=\"javascript:fnQueryDetails('','"+strPatListId+"','92423')\"/>";
		}else{
			strExamStatus = "";
		}
		
		if(!strPatListId.equals("")){
			strFormStatus = strFormStatus.equals("")?"99999":strFormStatus;
			strFormStatus = "<img id=imgEdit style=cursor:hand src=" + strImagePath + "/" + strFormStatus + ".gif" +
			  "  align='bottom' title='Click to view Query Details' onClick=\"javascript:fnQueryDetails('','"+strPatListId+"','92421')\"/>";
		}else{
			strFormStatus = "";
		}

	}
	if (!hmPatients.isEmpty())
	{
		strPatientIDE = GmCommonClass.parseNull((String)hmPatients.get("PATIENT_IDE"));	
	}
	
	strPatientIDE = strPatientIDE.equals("")?strSessPatientIDE:strPatientIDE;
	strMissingFollowUp = strMissingFollowUp.equals("Y") ? "checked" : "";
	//Fix for MNTTASK-1023
	if(strMissingFollowUp.equals("checked")){
		strFormDt = "";	
	}
	String strCodeID = "";
	String strSelected = "";
	
	int intSize = 0;
	int intQuesSize = 0;
	HashMap hcboVal = null;
	HashMap hFormVal = null;

	// Code for setting the Stydy Period Id and Event No for Loading the Form data after Un-Voiding the Form.-- MNTTASK-3766
	if(strVoidAccessFl.equals("Y")){
		strStudyPeriodID = (String)request.getAttribute("hStPerId") == null ?"":(String)request.getAttribute("hStPerId"); 
		strEventNo = (String)request.getAttribute("hEventNo") == null ?"":(String)request.getAttribute("hEventNo"); 
	}
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	Date dtFormDt = null;
	dtFormDt = GmCommonClass.getStringToDate(strFormDt, strApplDateFmt);
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Forms Data entry </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCombo/dhtmlxcombo_new.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmFormDataEntry.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmClinical.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCombo/dhtmlxcombo.js"></script>
<script>
var FormListLen = <%= alFormsList.size()%>;
var DisbQNoS = '<%=strDisblQNo%>';
var todaysDate = '<%=strTodaysDate%>';
window.dhx_globalImgPath = "<%=strExtWebPath%>/dhtmlx/dhtmlxCombo/imgs/";
<%	
	hFormVal = new HashMap();
	for (int i=0;i< alFormsList.size();i++)
	{
		hFormVal = (HashMap) alFormsList.get(i);
%>
	var FormArr<%=i%> ="<%=hFormVal.get("FORMID")%>,<%=hFormVal.get("FORMNM")%>,<%=GmCommonClass.parseNull((String)hFormVal.get("INIREQFL"))%>,<%=GmCommonClass.parseNull((String)hFormVal.get("FLAG"))%>,<%=hFormVal.get("PERIODID")%>,<%=hFormVal.get("STPERID")%>";
<%
		if (strFormId.equals((String)hFormVal.get("FORMID")))
		{
			strFormNm = (String)hFormVal.get("FORMNM"); 
			
			strEventFlag = GmCommonClass.parseNull((String)hFormVal.get("FLAG"));			
		}
	}
%>
// PMT-243 put the rule value in js array to validate form answer group
var FormVldArrLen = <%=alFormVld.size()%>;
<%
HashMap hmFormVld = new HashMap();
for(int i=0; i<alFormVld.size(); i++){
	hmFormVld = (HashMap)alFormVld.get(i);
%>
var FormVldArr<%=i%> = "<%=hmFormVld.get("CODENMALT")%> | <%=hmFormVld.get("CODENM")%>";
<%
	}
%>
</script>
</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad='fnLoadPage();'>
<FORM name="frmOrder" method="post" action = "<%=strServletPath%>/GmFormDataEntryServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hStudyId" value="<%=strStudyId%>">
<input type="hidden" name="hPatientId" value="<%=strPatientId%>">
<input type="hidden" name="hFormId" value="<%=strFormId%>">
<input type="hidden" name="hPeriodId" value="<%=strStudyPerId%>">
<input type="hidden" name="hStPerId" value="<%=strStudyPeriodID%>">
<input type="hidden" name="hEventNo" value="<%=strEventNo%>">
<input type="hidden" name="hPatListId" value="<%=strPatListId%>">
<input type="hidden" name="hFromName" value="<%=strFormNm%>">
<input type="hidden" name="hVoidFl" value="<%=strDeleteFl%>">
<input type="hidden" name="hIncludeVoidFrm" value="">

<input id="hFormLocked" type="hidden" name="hFormLocked" value="">

<input type="hidden" name="hTxnId" value="">
<input type="hidden" name="hCancelType" value="">
<input type="hidden" name="hTxnName" value="">
<input type="hidden" name="hFormDt" value="<%=strFormDt%>">
<input type="hidden" name="hVerifyFl" value="<%=strVerifyFl%>">  


<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">

<tr>
	<td   rowspan="2"></td>
	<td height="25" class="Header">&nbsp;FORMS DATA ENTRY - <%=strPatientIDE %></td>
	<td class="LLine" width="1" rowspan="2"></td>
	<td  rowspan="2"></td>
</tr>
<tr><td  height="100" valign="top">
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<% /******************************************************* 
		* Below code is used to fill all the combo box value  *
		*******************************************************/%>
	<tr><td>
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<%-- 
	<tr>
		<td class="RightText" align="right" HEIGHT="25" width="80">&nbsp;Study Name: </td>		
		<td >&nbsp;<select name="Cbo_Study" class="RightText"  onFocus="changeBgColor(this,'#AACCE8');" 
			onBlur="changeBgColor(this,'#ffffff');"  onChange="javascript:fnLoad();">  >
		<option value="0" >[Choose One]
<% 		//  To load the site information
		intSize = alStudyList.size();
		hcboVal = new HashMap();
		for (int i=0;i<intSize;i++)
		{
			hcboVal = (HashMap)alStudyList.get(i);
			strCodeID = (String)hcboVal.get("STUDYID");
			strSelected = strStudyId.equals(strCodeID)?"selected":"";
%>			<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("STUDYNM")%></option>
<% 		}%>
		</select> </td>
<%		if (strAction.equals("Reload") || strAction.equals("ReloadForm") || strAction.equals("LoadQues") || strAction.equals("EditAns") || strAction.equals("SaveAns"))
		{		
%>			<td class="RightText" align="right" width="80" >Patient ID:</td>
			<td width="220">&nbsp;<select name="Cbo_Patient" class="RightText"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
			<option value="0" >[Choose One]
<%		  	intSize = alPatientList.size();
			hcboVal = new HashMap();
			for (int i=0;i<intSize;i++)
			{
				hcboVal = (HashMap)alPatientList.get(i);
				strCodeID = (String)hcboVal.get("PATID");
				strSelected = strPatientId.equals(strCodeID)?"selected":"";
%>				<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("PATIDE")%></option>
<%	  		}
%>			</td></select>
<%		}
%>			
	</tr>
	--%>
<%	if (strAction.equals("Reload") || strAction.equals("ReloadForm") || strAction.equals("LoadQues") || strAction.equals("EditAns") || strAction.equals("SaveAns"))
	{
%>		<tr class="evenshade">
			<td class="RightText" align="right" HEIGHT="25">Period: </td>
			<td> &nbsp;<select name="Cbo_Period" class="RightText" onChange= "fnLoadFormsNoEvent(this);" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
				<option value="0" >[Choose One]
<% 		intSize = alPeriodList.size();
		hcboVal = new HashMap();
		for (int i=0;i<intSize;i++)
		{	hcboVal = (HashMap)alPeriodList.get(i);
			strCodeID = (String)hcboVal.get("PERIODID");
			strSelected = strStudyPerId.equals(strCodeID)?"selected":"";
%>				<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("PERIODDS")%></option>
<% 		}
%>			</select></td>
			<td class="RightText" align="right">Form: </td>
			<td >&nbsp;<select style='width: 275px;' name="Cbo_Form" class="RightText" onChange= "fnClearEventNo();" onFocus="changeBgColor(this,'#AACCE8');" 
				onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]</option></select>
		&nbsp;<%= strFormStatus %>&nbsp;<gmjsp:button gmClass="button" buttonType="Load" name="Btn_Go" value="Load" onClick ="fnLoadQuestions()"  
		style="WIDTH: 40px; HEIGHT: 16px"/>
		&nbsp; <a href="javascript:fnPatientLog()">Log</a>
		</td>
		<input type="hidden" name="hinitialNotReq" value='<%=strInitialNotReqFl%>'>
		<input type="hidden" name="hFormValidateFlds" value='<%=strValidateFlds%>'>
		<input type="hidden" name="hFlag" value="<%=strEventFlag%>">		
	 </tr>
<%	} %>
   
	</table>
	</td>	
	</tr>
	<%/************ Combo Box Code Ends Here **********************/%>
<%
 if(strFormNm.equals("SC10 - Operative Data Form"))
  {
    strFormNm += "<font color='red'><b> [Exam Date is Surgery Date; check that Surgery Date on Patient Setup is the same ]</b></font>";
  }

  if (strDeleteFl.equals("Y")){
	  strFormNm+= " :  "+strVoidFormMsg;
  }
// code is removed for to get dynamically rulevalue   
%>	
<%
	if (strAction.equals("LoadQues") || strAction.equals("EditAns") || strAction.equals("SaveAns"))
	{
%>	
<tr> <table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td height="1" class="LLine"></td>		
	</tr>
	<tr><td colspan="2">
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr class="ShadeRightTableCaption"><td height="25" colspan="3"><%=strFormNm%></td></tr>
		<tr><td colspan="3" height="1" class="LLine"></td></tr>
		<tr class="evenshade">
			<td colspan="3" height="20" class="RightText">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr class="oddshade">	
			<!-- Custom tag lib code modified for JBOSS migration changes -->			
				<td width=200 height="22" align="right" class="RightText"><%if (!strEventFlag.equals("D")) {%>&nbsp;Exam Date (mm/dd/yyyy):<%}else{ %><%=strEventLbl%><%}%> </td>
				<td width=250>
				<%if (!strEventFlag.equals("D"))
				{%>
					&nbsp;<gmjsp:calendar textControlName="Txt_FormDate"
											textValue="<%=(dtFormDt == null) ? null : new java.sql.Date(dtFormDt.getTime())%>"
											gmClass="InputArea"
											onFocus="changeBgColor(this,'#AACCE8');"
											onBlur="changeBgColor(this,'#ffffff');" />&nbsp;
					<%if(strDateFl.equals("Y"))
					{ %>
					<img  id="imgEdit" style="cursor:hand" onclick="javascript:fnLoadHistory('1017','<%=strPatListId%>');" title="Click to open required date history"  
					src="<%=strImagePath%>/icon_History.gif" align="bottom"  height=15 width=18 />&nbsp;
					<% }%><%= strExamStatus %>
				<%} 
				else{%>
					<input type="hidden" size="10" class="RightText" value="<%=(String)session.getAttribute("strSessTodaysDate")%>" name="hFormDate" 
					onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">&nbsp;
					<% 
						ArrayList alNoOfEvents = (ArrayList)request.getAttribute("alNoOfEvents");
						String strEventSelect = GmCommonClass.parseZero((String)request.getAttribute("hEventNo")) ;
					%>
					
					<gmjsp:dropdown controlName="cbo_EventNo"  seletedValue="<%= strEventSelect %>" 	
					  width="150" value="<%= alNoOfEvents%>" codeId = "EVENT_NO"  codeName = "DES"  defaultValue= "[New Event]" />
				<%} %>	
					
					<gmjsp:button gmClass="button" buttonType="Load" name="BtnGo" value="Load" onClick ="fnLoadQuestions()"  
					style="WIDTH: 40px; HEIGHT: 16px"/>
				</td>
				<td width=300 align="right" class="RightText">&nbsp;<%=strPatientInitials%>: </td>
				<td width="200">&nbsp;<input type="text" value="<%=strInitials%>" name="Txt_Initials" size="5" class="RightText" 
				onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
				<%if(strInitialsFl.equals("Y"))
					{ %>
				<img  id="imgEdit" style="cursor:hand" onclick="javascript:fnLoadHistory('1018','<%=strPatListId%>');" title="Click to open required Initials history"  
					src="<%=strImagePath%>/icon_History.gif" align="bottom"  height=15 width=18 />&nbsp;
				<% }%>
				</td>
			</tr>
			<tr class="oddshade"><td colspan="4" height="3"></td></tr>
			
<%		if (strAction.equals("Load"))
		{
%>			&nbsp;&nbsp;&nbsp;&nbsp;<gmjsp:button gmClass="button" buttonType="Load" name="BtnLoad" value="Load" onClick ="fnReload()"  style="WIDTH: 40px; HEIGHT: 16px"/>
<%		}
%>			</td></tr>											
          <tr class="oddshade"><td colspan="4">&nbsp;<input type="checkbox" name="Chk_MissingFollowUp" onClick="fnChangeType()" <%=strMissingFollowUp%> tabindex="-1">&nbsp; Missing Follow Up </td></tr>
		</table></td></tr>
								
			<tr><td colspan="3" height="1" class="LLine"></td></tr>
			<tr><td colspan="3" >
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
<%	/********************************************************* 
	 * Below code is used to fill Question and Answer Section*
	**********************************************************/
		GmQuestionMasterData gmQuestion = new GmQuestionMasterData();
		GmAnswerGrpData gmAnswerGpData = new GmAnswerGrpData();
		
		ArrayList alTemp = new ArrayList();
		ArrayList alQuestionList = (ArrayList)hmQuestionsDetails.get("QUESTIONLIST");
		ArrayList alAnswerList = (ArrayList)hmQuestionsDetails.get("ANSWERLIST");
		intQuesSize = alQuestionList.size();
		int intQuestionId = 0;
		int intQuestionSeqNo = 0;
		String strQuestionDispNo="";
		String strQuestionDS = "";
		String strQueryStatus = "";
		String strControlType = "";
		String strSecQuestion = "";
		String strControl = "";
		String strCheckboxPt1 = "<input type=checkbox class=RightText name=";
		String strListPt1 = "<select type=select class=RightText name=";
		String strListPt2 = "><option value=0>Choose One</option>";
		String strListPt3 = "</select>";
		String strTextPt1 = "<input type=text class=RightText name=";
		String strTextPt2 = "<input type=text readonly='true' class=RightText name=";
		String strTextAreaPt1 = "<textarea class=RightText rows=4 cols=105 name=";
		String strOptionNm = "";
		String strOptionId = "";
		String strAnswerValue = "";
		String strAnswerGrpId = "";
		String strAnswerGrpIdTemp = "";
		String strPatientAnsList = "";
		String strPatientAnsDs = "";
		boolean blHistoryFl = false;
		String strHistoryImg = "";
		int intPatAnsListId = 0;
		String strAnswerGrpCd = "";
		String strAnswerGrpCdTemp = "";
		String strBRValue = "";
		String strHistImgValue = "";
		int intAnsCnt = 0;
		String strAnswerGrpIds = "";
		String strCMTRequiredFL = "";
		boolean blRequiredFL = false;
		String strVerifyCode = "";
		String strVerifyDesc = "";
		String strCMTValue = " ";
		String strCMTValueTemp = "";
		String strCMTOnChange = "";
		String strHistIntImgValue = "<img id=imgEdit style=cursor:hand src=" + strImagePath + "/icon_History.gif " + 
						  "  align='bottom' title='Click to view answer Change details' width=18 height=18 ";
		boolean blMandatoryFl = false;
		String strFormValidateFn = "";

		for (int i=0;i<intQuesSize;i++)
		{
			gmQuestion = (GmQuestionMasterData) alQuestionList.get(i);
			intQuestionId = gmQuestion.getQuestionID();
			intQuestionSeqNo = gmQuestion.getQuestionSeqNo();
			strQuestionDispNo = GmCommonClass.parseNull(gmQuestion.getQuestionDispNo());
			strQuestionDS = gmQuestion.getQuestionDS();
			
			alTemp = gmQuestion.getAnswerGrp();
			intAnsCnt = alTemp.size();
			strAnswerGrpId = "";
			strAnswerGrpIds = "";
			String strShade = "oddshade";
			
			if(i%2 == 0)
			{
				strShade = "evenshade";
			}
			
			if(gmQuestion.getEcFlag())
			{
				strShade = "EditCheck";
			}

			
%>			<tr class="<%=strShade %>">
				<td width="25" class="RightText" height="22" rowspan="<%=intAnsCnt+1%>" valign="top">&nbsp;<b><%=strQuestionDispNo%></b>&nbsp;</td>
				<td valign="top" class="RightText"><b><%=strQuestionDS%></b><BR></td>
			</tr>	
			<tr class="<%=strShade %>"><td colspan="4" height="24" class="RightText">
			
<%	
			if (!strQuestionDispNo.equals("")){ 
%>				<span class="RightTextBlue"><b>A.</b></span>
<%			}else{
%>				<span class="RightTextBlue"><b>&nbsp;&nbsp;&nbsp;</b></span>

<%			}
			//Below code loops thru for each answers
			
			strBRValue = "";
			for (int j=0;j<intAnsCnt;j++)
			{
				String strEditChkImg = "";				
				gmAnswerGpData = (GmAnswerGrpData) alTemp.get(j);
				strControlType = gmAnswerGpData.getControlType();
				strAnswerGrpIdTemp = gmAnswerGpData.getAnswerGrpID();
				strAnswerGrpId = "hAnsGrpId".concat(strAnswerGrpIdTemp);
				strAnswerGrpIds = strAnswerGrpIds.concat(",").concat(strAnswerGrpIdTemp);
				strQueryStatus = GmCommonClass.parseNull(gmAnswerGpData.getQueryStatus());
				boolean bEditCheckFl = gmAnswerGpData.getEcFlag();
				//Answers
				strPatientAnsList = GmCommonClass.parseZero(gmAnswerGpData.getPatient_AnswerList());
				strPatientAnsDs = GmCommonClass.parseNull(gmAnswerGpData.getPatinet_AnswerDs());
				strPatientAnsDs = GmCommonClass.replaceForHTML(strPatientAnsDs);
				intPatAnsListId = gmAnswerGpData.getPatient_AnswerList_ID();
				if(bEditCheckFl)
				{
					strEditChkImg = " &nbsp;  <img id=imgEditChk style=cursor:hand src=" + strImagePath + "/editcheck.gif " + 
					  "  align='bottom' title='Click to view edit check details' onClick=\"javascript:fnEcDetails('" +strPatientId + "','10','"+strAnswerGrpIdTemp+"','"+intPatAnsListId+"')\"/>";
				}
				// to check the form already saved or not. If saved then set the new Form flag as false.
				if(intPatAnsListId != 0 && blNewFormFl){
					blNewFormFl = false;
				}
				
				if(intPatAnsListId != 0 && !strDeleteFl.equals("Y")){
					strQueryStatus = strQueryStatus.equals("")?"99999":strQueryStatus;
					strQueryStatus = "<img id=imgEdit style=cursor:hand src=" + strImagePath + "/" + strQueryStatus + ".gif" +
					  "  align='bottom' title='Click to view Query Details' onClick=\"javascript:fnQueryDetails('" +intPatAnsListId + "','','92422')\"/>";
				}else{
					strQueryStatus = "";
				}
				
				blHistoryFl = gmAnswerGpData.isHistoryFlag();
	 						
				strHistoryImg = blHistoryFl == true?strHistIntImgValue + " onClick=\"javascript:fnCallHistory(" +intPatAnsListId+")\"/>":"";
				strAnswerGrpCd = gmAnswerGpData.getAnswerGrpCD();
				
				blMandatoryFl = gmAnswerGpData.isMandatoryFlag();
				strFormValidateFn = GmCommonClass.parseNull(gmAnswerGpData.getFormValidateFn());
				
				// If the question is not associated with with combo box then will display below val
				if ( strAnswerGrpCd == null)
				{
					strSecQuestion = GmCommonClass.parseNull(gmAnswerGpData.getAnswerGrpDS()) ;
					strControlType = gmAnswerGpData.getControlType();
					if (strControlType.equals("CHECKBOX"))
					{
						strControl = strCheckboxPt1.concat(strAnswerGrpId).concat(" id=").concat(""+intQuestionId).concat(" "+strPatientAnsDs).concat(">");
						out.println( strBRValue  + strControl + "&nbsp;&nbsp;<span class='RightText'>" ) ;
						out.println( strSecQuestion  + "</span> "+ strEditChkImg  + "&nbsp;&nbsp;"+ strQueryStatus + "&nbsp;&nbsp;" + strHistoryImg );
					}
					else if (strControlType.equals("TEXTBOX"))
					{
						strControl = strTextPt1.concat(strAnswerGrpId).concat(" id=").concat(""+intQuestionId).concat(" value='"+strPatientAnsDs).concat("'>");
						out.println( strBRValue +  strControl + strEditChkImg  + "&nbsp;&nbsp;"+ strQueryStatus + "&nbsp;&nbsp;" + strHistoryImg );
						out.println( "<span class='RightText'>" + strSecQuestion + " </span>&nbsp;&nbsp;" ) ;
					}else if (strControlType.equals("TEXTREADONLY"))
					{
						strControl = strTextPt2.concat(strAnswerGrpId).concat(" id=").concat(""+intQuestionId).concat(" value='"+strPatientAnsDs).concat("'>");
						out.println( strBRValue +  strControl +  strEditChkImg  + "&nbsp;&nbsp;"+ strQueryStatus + "&nbsp;&nbsp;" +strHistoryImg );
						out.println( "<span class='RightText'>" + strSecQuestion + " </span>&nbsp;&nbsp;" ) ;
					}else if (strControlType.equals("TEXTAREA"))
					{
						strControl = strTextAreaPt1.concat(strAnswerGrpId).concat(" id=").concat(""+intQuestionId).concat(" value='"+strPatientAnsDs).concat("'>").concat(strPatientAnsDs).concat("</textarea>");
						out.println( strBRValue +  strControl +  strEditChkImg + "&nbsp;&nbsp;"+ strQueryStatus + "&nbsp;&nbsp;" + strHistoryImg );
					}
				}
				else{
					strControl = "";
			  		intSize = alAnswerList.size();
					hcboVal = new HashMap();
					blRequiredFL = false;
					strCMTOnChange  = "";
					strCMTValue = "<input tpye=text class=RightText size=50 ";
					for (int k=0;k<intSize;k++)
					{
						hcboVal = (HashMap)alAnswerList.get(k);
						strAnswerGrpCdTemp = (String)hcboVal.get("C605_ANSWER_GRP");
						
						if (strAnswerGrpCdTemp.equals(strAnswerGrpCd))
						{
							strOptionNm = (String)hcboVal.get("C605_ANSWER_LIST_NM");
							strOptionId = (String)hcboVal.get("C605_ANSWER_LIST_ID");
							strAnswerValue = (String)hcboVal.get("C605_ANSWER_VALUE");
							
							strCMTRequiredFL = GmCommonClass.parseNull((String)hcboVal.get("C605_CMT_REQUIRED_FL"));
							%><input type="hidden" name="hCmtRequiredFlg<%=strOptionId%>" value="<%=strCMTRequiredFL%>"><%
							if (strCMTRequiredFL.equals("Y") /*&& blRequiredFL == false	*/) {
								if (blRequiredFL == false){
									blRequiredFL = true;
									strCMTOnChange = " onChange=\"ShowHideInputField (this.value, " + " '" + strOptionId + "', " + 
									                 " this.form.txtOther" + strAnswerGrpIdTemp + " );\" " ;
									strCMTValueTemp = strCMTValue.concat("style=display:none name=");
								}
								if(strOptionId.equals(strPatientAnsList)){									
									strCMTOnChange = " onChange=\"ShowHideInputField (this.value, " + " '" + strOptionId + "', " + 
									                 " this.form.txtOther" + strAnswerGrpIdTemp + " );\" " ; 
									strCMTValueTemp = strCMTValue.concat(" name=");
								}
						    }
							strOptionId = strOptionId.equals(strPatientAnsList)?strOptionId.concat(" selected"):strOptionId;
							
							strOptionId = "<option value=".concat(strOptionId).concat(">");
							strControl = strControl.concat(strOptionId).concat(strAnswerValue + " - " + strOptionNm).concat("</option>");
						}
					}	
					strControl = strListPt1.concat(strAnswerGrpId).concat(strCMTOnChange).concat(strListPt2).concat(strControl).concat(strListPt3);
					strSecQuestion = gmAnswerGpData.getAnswerGrpDS();

					out.println( strBRValue + strControl + GmCommonClass.parseNull(strSecQuestion) + strEditChkImg + "&nbsp;&nbsp;" + strQueryStatus) ;
					out.println( "&nbsp;&nbsp;" + strHistoryImg );
					// Below code to display other information
					if (blRequiredFL) {
						out.println( "&nbsp;&nbsp;" + strCMTValueTemp + "txtOther" +   strAnswerGrpIdTemp + " value = \"" + strPatientAnsDs + "\" >" );
						out.println( "<input type=hidden name=hAnsComboOther" + strAnswerGrpIdTemp +  " value=true > ");
					}
					else{
						out.println( "<input type=hidden name=hAnsComboOther" + strAnswerGrpIdTemp +  " value=false > ");
					}
				}
%>				<input type="hidden" name="hAnsListId<%=strAnswerGrpIdTemp%>" value="<%=intPatAnsListId%>">
				<input type="hidden" name="hAnsSeq<%=strAnswerGrpIdTemp%>" id="<%=blMandatoryFl%>" value="<%=intQuestionSeqNo%>">
				<input type="hidden" name="hFormVal<%=strAnswerGrpIdTemp%>" value="<%=strFormValidateFn%>">
				
<%				strBRValue = "</td></tr><tr class='"+strShade+"'><td height=24 class=RightText >&nbsp;&nbsp;&nbsp;&nbsp;";
			} // end of Inner FOR LOOP Answer Loop 
%>							
			<input type="hidden" name="hQuesId<%=i%>" value="<%=intQuestionId%>,<%=intAnsCnt%>" id="<%=strAnswerGrpIds.substring(1)%>">
			<BR></td></tr>
			<tr><td colspan="2" height="1" class="LLine"></td></tr>
<%		} // END of Outer FOR Loop
		// If Selected form is new form then only set the Verify Description
		if (blNewFormFl) {
			strVerifyCode = "6190";
			strVerifyDesc = "Initial Release";
		}
%>		</table></td></tr>
		</table><input type="hidden" name="hQuestCnt" value="<%=intQuesSize%>">
		</td></tr>
<%		if (strCalReqFl.equals("Y")){
%>		
		<tr>
			<td colspan="3" class="RightTableCaption">Calculated Value:&nbsp;&nbsp;<input type="text" size="4" class="RightText" name="Txt_CalVal" value="<%=strCalculatedValue%>"></td>
		</tr>
<%		}
%>		
		
<%	/********************************************************* 
	 * Below code used to display Verficatin section*
	**********************************************************/
	if (strAction.equals("LoadQues") || strAction.equals("EditAns"))
	{	
		alVerifyType = (ArrayList)hmReturn.get("VERIFYVALUE");
	
%>		<tr><td colspan="3" > 
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr><td height="25" class="RightDashBoardHeader"  colspan="4">&nbsp;Verification /Comments Section</td></tr>
		<tr class="oddshade">
			<td height="22" align="left" class="RightText">&nbsp;Type:</td>
			<td align="left" class="RightText" colspan="3">&nbsp;<select name="Cbo_VerifyType" id="VerifyType" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" 
				onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
<% 				intSize = alVerifyType.size();
				hcboVal = new HashMap();
				for (int i=0;i<intSize;i++)
				{
					hcboVal = (HashMap)alVerifyType.get(i);				
					strCodeID = (String)hcboVal.get("CODEID");
					strSelected = strVerifyCode.equals(strCodeID)?"selected":"";					
%>				<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%  			}%>	</select></td></tr>
		<tr class="oddshade">
			<td align="left" height="22" class="RightText">&nbsp;Description:</td>
			<td align="left" class="RightText" colspan="3" >&nbsp;<textarea class="RightText" rows="4" cols="105" 
			name="txtaVerifyDesc" value=<%=strVerifyDesc%> ><%=strVerifyDesc%></textarea></td>
		</tr>
		<tr><td colspan="4" height="3" ></td></tr>
		<tr><td colspan="4" height="1" class="Line"></td></tr>
		<tr class="ShadeRightTableCaption"><td height="22" colspan="4">&nbsp;Verification History</td></tr>
		<tr><td colspan="4" height="1" class="Line"></td></tr>
		<tr><td colspan="4">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr class="oddshade">
				<td class="RightTableCaption" height="20">#</td>
				<td class="RightTableCaption">&nbsp;Type </td>
				<td class="RightTableCaption">&nbsp;By</td>
				<td class="RightTableCaption">&nbsp;Updated Date</td>
				<td class="RightTableCaption">&nbsp;Comments (If any)</td>
			</tr>
			<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>
<%				String strVerifyUser = "";
				String strVerifyDate = "";
				String strVerifyLvlId="";
				intSize = alVerifyList.size();
				hcboVal = new HashMap();
				if (intSize > 0)
				{
					for (int i=0;i<intSize;i++)
					{
						hcboVal = (HashMap)alVerifyList.get(i);
						strVerifyLvlId = GmCommonClass.parseNull((String)hcboVal.get("VLEVEL_ID"));
						
						if(strVerifyLvlId.equals("6195")){ // 6195 corresponds to Locked
							%>
							<script>
								document.getElementById("hFormLocked").value="locked";
							</script>
							
							<%
						}
%>				<tr>
					<td class="RightText" height="18">&nbsp;<%=i+1%></td>
					<td class="RightText">&nbsp;<%=hcboVal.get("VLEVEL")%></td>
					<td class="RightText">&nbsp;<%=hcboVal.get("CUSER")%></td>
					<td class="RightText">&nbsp;<%=hcboVal.get("VERDATE")%></td>
					<td class="RightText">&nbsp;<%=hcboVal.get("CMNTS")%></td>
				</tr>
<%	  				}
		  		}else{
%>					<tr><td class="RightText" height="30" colspan="5" align="center">&nbsp;This Form has not been verified as yet.</td></tr>
<%				}%>
			</table>
		</table></td></tr>
<%	}%>		
 <%
	String strDisableBtnVal = strButtonDisabled;
	if(strDisableBtnVal.equals("disabled"))
	{
		strDisableBtnVal = "true";
	}
	if(!(strPatListId.equals(""))){ //Disabling submit button if the form was already in missing follow up status
		if((strMissingFollowUp.equals("checked")) && (strVerifyFl.equals("checked"))){
			strDisableBtnVal = "true";
		}
	}
%>		
		<tr>
		
		<td colspan="2" height="25" align="center">
		<%
			if(!strDeleteFl.equals("Y")){ // for clinical void form 
		%>
			<gmjsp:button gmClass="button" buttonType="Save" name="Btn_Submit" value="Submit" onClick ="fnSubmit()" disabled="<%=strDisableBtnVal %>"/>
			 
			<%
				if(strVoidAccessFl.equals("Y")){ //The access only for particular users 
			%>
				&nbsp;&nbsp;<gmjsp:button gmClass="button" buttonType="Save" name="Btn_Void" value="Void" onClick ="fnVoid()" />
			<%} %>
		<%} else if(strVoidAccessFl.equals("Y") && strAdvEventFl.equals("Y")){ %>
			<gmjsp:button style="width: 40em;height: 25px" gmClass="button" buttonType="Save" name="Btn_Unvoid" value="UnVoid" onClick ="fnUnvoid()" />
		<%} %>
		</td>
		
		</tr>
<%		
	}
%>
				</table>
			</td>
		</tr>
		
	</table>
				

</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
