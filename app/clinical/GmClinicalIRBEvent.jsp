 <%
/**********************************************************************************
 * File		 		: GmClinicalIRBEvent.jsp
 * Desc		 		: This is a transaction screen used to Tack Event for an IRB Approval.
 * Version	 		: 1.0
 * author			: Rajeshwaran
************************************************************************************/
%>

<!-- clinical\GmClinicalIRBEventReport.jsp -->

<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ include file="/common/GmHeader.inc" %>					
<%@ page import="java.util.ArrayList"%>
<bean:define id="gridData" name="frmClinicalIRBEvent" property="gridData" type="java.lang.String"> </bean:define>
<bean:define id="studyName" name="frmClinicalIRBEvent" property="studyName" type="java.lang.String"> </bean:define>
<bean:define id="siteName" name="frmClinicalIRBEvent" property="siteName" type="java.lang.String"> </bean:define>
<bean:define id="approvalTypeName" name="frmClinicalIRBEvent" property="approvalTypeName" type="java.lang.String"> </bean:define>
<bean:define id="hActionTaken" name="frmClinicalIRBEvent" property="actionTaken" type="java.lang.String"> </bean:define>
<bean:define id="approvalReasonName" name="frmClinicalIRBEvent" property="approvalReasonName" type="java.lang.String"> </bean:define>
<bean:define id="lockedStudyDisable" name="frmClinicalIRBEvent" property="lockedStudyDisable" type="java.lang.String"> </bean:define>
<HTML>
<HEAD>
<TITLE> Globus Medical: Track Event Date </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>


<script>
	var objGridData;
	objGridData = '<%=gridData%>';
	
	var approvalType='<%=approvalTypeName%>';
</script>

</HEAD>

<script>
function fnSubmit(){

fnValidateDropDn('actionTaken','Action Taken');
fnValidateTxtFld('eventDate','Event Date');
DateValidateMMDDYYYY(document.frmClinicalIRBEvent.eventDate,'Please Enter <b>Valid Date Format</b>');

if(document.frmClinicalIRBEvent.actionTaken.value==60548
   || document.frmClinicalIRBEvent.actionTaken.value==60550){
	fnValidateDropDn('reviewType','Review Type');
	
	if(approvalType!='Advertisement'){
		fnValidateDropDn('reviewPeriod','Review Period');
	}
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
document.frmClinicalIRBEvent.strOpt.value="save_irb_event";
	fnStartProgress();
document.frmClinicalIRBEvent.submit();
}

function fnOnPageLoad()
{
	  if(objGridData.indexOf("cell", 0)==-1){
		  return true;
	  }
		
	  gridObj = initGrid('irbevent',objGridData);
	  gridObj.enableTooltips("false,true,true,true,true,true");
	 
	  gridObj.attachEvent("onRowSelect" ,function(rowId,cellIndex){
		  if(cellIndex == 0)
		  {
			fnLoadIRBEvent(rowId);
		  }
			
	  });
	 toggleReviewType();
}

function fnLoadIRBEvent(virbeventid)
{
	document.frmClinicalIRBEvent.actionTaken.value="";
	document.frmClinicalIRBEvent.eventDate.value="";
	document.frmClinicalIRBEvent.notes.value="";
	document.frmClinicalIRBEvent.reviewType.value="";
	document.frmClinicalIRBEvent.reviewPeriod.value="";
	document.frmClinicalIRBEvent.irbEventID.value = virbeventid;
	document.frmClinicalIRBEvent.strOpt.value = "edit_irb_event";
	document.frmClinicalIRBEvent.submit();
}

function toggleReviewType(){
	  obj = document.getElementById("divreviewperiod");
	  
	  if(document.frmClinicalIRBEvent.hActionTaken.value==60550){
      	document.getElementById("mediainfo").style.display = 'block';
      }
      
	  if(document.frmClinicalIRBEvent.actionTaken.value==60548){	
	     obj.style.display = 'block';
	     document.getElementById("mediainfo").style.display = 'none';
      }
      else if(document.frmClinicalIRBEvent.actionTaken.value==60550){       	
       	obj.style.display = 'block';       	
      }      
      else{
       	obj.style.display = 'none';       	  
       	document.getElementById("mediainfo").style.display = 'none';
	  }
	  
	  
	  
	  if(approvalType=='Advertisement'){
	  	  var reviewType = document.getElementById('reviewType');
		  for (i =0; i< reviewType.length ;  i++) {
		    if (reviewType.options[i].value== 60535) { //Continuing is not applicable for Advertisement
		      reviewType.remove(i);
		    }
		  }
		  
		  document.getElementById("revprd_id").style.display = 'none';
	  }
	  
}

function fnLoadMedia(){
 
 if(TRIM(document.frmClinicalIRBEvent.irbEventID.value).length==0){
	Error_Clear();
	Error_Details("Cannot Load Media Info. Please Click on Edit Icon of \"Approval\" Action Taken.");				
	Error_Show();
	Error_Clear();
	return false;
}
 document.frmClinicalIRBEvent.strOpt.value="load_irb_media";
 document.frmClinicalIRBEvent.action="/gmClinicalIRBMediaAction.do";
 document.frmClinicalIRBEvent.submit();
}

function fnResetEvent(){
document.frmClinicalIRBEvent.actionTaken.value="0";
document.frmClinicalIRBEvent.eventDate.value="";
document.frmClinicalIRBEvent.notes.value="";
toggleReviewType();

}

function fnVoidEvent()
{
		document.frmClinicalIRBEvent.action ="/GmCommonCancelServlet";
		document.frmClinicalIRBEvent.hTxnId.value = document.frmClinicalIRBEvent.irbEventID.value;	
		document.frmClinicalIRBEvent.hTxnName.value = document.frmClinicalIRBEvent.actionTaken.options[document.frmClinicalIRBEvent.actionTaken.selectedIndex].text;
		document.frmClinicalIRBEvent.hCancelType.value = 'VDENT';
		document.frmClinicalIRBEvent.hAction.value = 'Load';
		document.frmClinicalIRBEvent.submit();
}
</script>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad()" >
<html:form  action="/gmClinicalIRBEventAction.do">
<html:hidden property="strOpt" />
<html:hidden property="irbid"  />
<html:hidden property="irbEventID"  />
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value=""/>
<html:hidden property="hAction" value=""/>
<html:hidden property="hActionTaken" value="<%=hActionTaken%>"/>
<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">Track Event Date</td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>

		<tr>
		<td HEIGHT="30" colspan="4"> 
			<table width="100%">
				<tr class="evenshade">
					<td class="RightTableCaption" align="left" HEIGHT="30" ></font>&nbsp;Study:</td>						
					<td><%=studyName%>
					</td>
					<td class="RightTableCaption" align="left" HEIGHT="30" ></font>&nbsp;Site:</td>						
					<td><%=siteName%>
					</td>
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr class="oddshade">
					<td class="RightTableCaption" align="left" HEIGHT="30" ></font>&nbsp;Appr Type:</td>						
					<td><%=approvalTypeName%>
					</td>
					<td class="RightTableCaption" align="left" HEIGHT="30" ></font>&nbsp;Appr Reason:</td>						
					<td><%=approvalReasonName%>
					</td>				           					
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
				</tr>
			</table>
		</td>				
	</tr>
		<tr>
			<td height="100" valign="top">
				<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="evenshade">
						<td class="RightText" align="right" HEIGHT="30" width="30%"><font color="red">*</font>&nbsp;Action Taken:</td>						
							<td>&nbsp;<gmjsp:dropdown controlName="actionTaken"  SFFormName="frmClinicalIRBEvent" SFSeletedValue="actionTaken"
								SFValue="alActionTakenList" width="250" onChange="javascript:toggleReviewType()" codeId="CODEID" codeName="CODENM"  defaultValue="[Choose One]"/> &nbsp;&nbsp;							
            			</td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr class="oddshade">
						<td class="RightText" align="right" HEIGHT="30" width="30%"><font color="red">*</font>&nbsp;Event Date:</td>						
							<td>&nbsp;<gmjsp:calendar SFFormName="frmClinicalIRBEvent" controlName="eventDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />&nbsp;   &nbsp;&nbsp;	
            			</td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr class="evenshade">
						<td class="RightText" align="right" HEIGHT="30" width="30%"></font>&nbsp;Notes:</td>						
							<td>&nbsp;<html:textarea property="notes" rows="4" cols="50" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/> &nbsp;&nbsp;							
            			</td>
					</tr>
										
					</table>
					
					<table width="100%" id="divreviewperiod" style="display:none;" border="0" >
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr class="oddshade">
						<td class="RightText" align="right" HEIGHT="30" width="30%"><font color="red">*</font>&nbsp;Review Type:</td>						
							<td>&nbsp;<gmjsp:dropdown controlName="reviewType"  SFFormName="frmClinicalIRBEvent" SFSeletedValue="reviewType"
								SFValue="reviewTypeList" width="250" codeId="CODEID" codeName="CODENM"  defaultValue="[Choose One]"/> &nbsp;&nbsp;							
            			</td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr id="revprd_id" class="evenshade">
						<td class="RightText" align="right" HEIGHT="30" width="30%"><font color="red">*</font>&nbsp;Review Period:</td>						
							<td>&nbsp;<gmjsp:dropdown controlName="reviewPeriod"  SFFormName="frmClinicalIRBEvent" SFSeletedValue="reviewPeriod"
								SFValue="reviewPeriodList" width="250" codeId="CODEID" codeName="CODENM"  defaultValue="[Choose One]"/> &nbsp;&nbsp;							
            			</td>
					</tr>
					
					 </table>		     
      		  <table  width="100%" cellspacing="0" cellpadding="0" border=0 bordercolor=red>
				<tr>
				<td  align="center" HEIGHT="30" width="30%">&nbsp;</td>
				<td  align="center" HEIGHT="30" width="40%"><gmjsp:button value="Submit" gmClass="button" buttonType="Save" onClick="javascript:fnSubmit();" disabled="<%=lockedStudyDisable%>"/>
				&nbsp;&nbsp;<gmjsp:button value="Reset" gmClass="button" buttonType="Save" onClick="javascript:fnResetEvent();"/>&nbsp;&nbsp;
				<gmjsp:button value="Void" gmClass="button" buttonType="Save" onClick="javascript:fnVoidEvent();" disabled="<%=lockedStudyDisable%>"/>
				</td><td width="30%" id="mediainfo" style="display:none;" align="left" HEIGHT="30" >
				&nbsp;&nbsp;<gmjsp:button value="Enter Media Info" gmClass="button" buttonType="Save" onClick="javascript:fnLoadMedia();" disabled="<%=lockedStudyDisable%>"/></td>
				<td  align="center" HEIGHT="30" width="30%">&nbsp;</td>
				</tr>		
						
    </table>		     
      		  <table>	  
			<tr> 
				<td colspan="2">
			    	<div id="irbevent" style="grid" height="150px"></div>
				</td>	
    	 	</tr> 	  	
    	</table>	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>