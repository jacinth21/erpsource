<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ page import="com.globus.common.beans.GmCommonClass"%> 
<%@ taglib uri="gmjsp-taglib.tld" prefix="gmjsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ page buffer="16kb" autoFlush="true" %>

<%
String strCssPath = "";
response.setContentType("application/vnd.ms-excel");
response.addHeader("content-disposition", "attachment; filename = Request-QueryperPatient.xls");
%>
<html>
<head>
<TITLE> Globus Medical: Edit Check Incident Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
</head>
<style>
TR.Shade{
	background-color: #eeeeee;
}
</style>
<body topmargin="10" leftmargin="10">
<br>
<h3 align="center"> Globus Medical: Edit Check Incident Report </h3>
<table border="1" class="DtTable850" cellspacing="1" cellpadding="1">
  <td class = "Shade" colspan="9">
				<display:table name="ALRETURN"   class="its" id="currentRowObject" > 
				<display:column property="REFERENCE_ID" title="Reference Id"   />
				<display:column property="DESCRIPTION" title="Description" class="alignleft"  />
			  	<display:column property="SPEC" title="Spec" class="alignleft"   />
			  	<display:column property="STUDY_NAME" title="Study" class="alignleft"   />
			  	<display:column property="FORM_DS" title="Form" class="aligncenter"   />
			  	<display:column property="CREATED_BY" title="Created By" class="aligncenter"   />
			  	<display:column property="CREATED DATE" title="Created Date" class="aligncenter"   />
			  	<display:column property="STATUS" title="Status" class="aligncenter"   />
			  	<display:column property="INCIDENT_COUNT" title="Incident Count" class="aligncenter"   />
				</display:table> 
				</td>
     </tr>
 </table>
</body>
<html>