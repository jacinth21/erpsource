 <%
/**********************************************************************************
 * File		 		: GmClinicalSurgeonMap.jsp
 * Desc		 		: This is a report screen for Clinical Surgeon setup
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>

<!-- clinical\GmClinicalSurgeonMap.jsp -->


<%@ include file="/common/GmHeader.inc"%>
<%@ page import="org.apache.commons.beanutils.BasicDynaBean"%>
<%@ page import="org.apache.commons.beanutils.RowSetDynaClass"%>
<%@ page import="java.util.ArrayList"%>

<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="com.globus.common.beans.GmCommonConstants"%> 
<%@ page import="com.globus.common.beans.GmLogger"%> 
<%@ page import="org.apache.log4j.Logger"%> 
<bean:define id="lockedStudyDisable" name="frmClinicalSurgeonSetup" property="lockedStudyDisable" type="java.lang.String"> </bean:define>
<%
String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Clinical Surgeon Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmClinicalSurgeonMap.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad = "fnLoad();">
<html:form action="/gmClinicalSurgeonSetup.do">
<html:hidden property="strOpt" />
<html:hidden property="hsurgeonSetupID"/>
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value=""/>
<html:hidden property="hAction" value=""/>
<html:hidden property="lockedStudyDisable"/>
<html:hidden property="msg"/>

	<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">Surgeon Mapping</td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<logic:notEqual name="frmClinicalSurgeonSetup" property="msg" value="">
					<tr><td colspan="2" height="20" align="center" class="RightBlueText">
					<bean:write name="frmClinicalSurgeonSetup" property="msg"/></td>
										<tr><td class="Line" colspan="2"></td></tr>
					</tr>
					</logic:notEqual>	
					
					
					<tr><td colspan="2" class="LLine" height="1"></td></tr>					
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="evenshade">
						<td class="RightTableCaption" align="right" HEIGHT="30" width="30%"><font color="red">*</font>&nbsp;Surgeon List:</td>						
							<td>&nbsp;<gmjsp:dropdown controlName="surgeonNameList"  SFFormName="frmClinicalSurgeonSetup" SFSeletedValue="surgeonNameList"
							SFValue="alSurgeonNameList" onChange="fnSplitName();" width="250" codeId="ID" codeName="NAME" defaultValue="[Choose One]"/>
            			</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
                    <tr class="oddshade">
                        <td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;Surgeon First Name:</td>
                        <td>&nbsp;<html:text property="firstName" size="25" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td> 
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
                    <tr class="evenshade">
                        <td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;Surgeon Last Name:</td>
                        <td>&nbsp;<html:text property="lastName" size="25" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td> 
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
                    <tr class="oddshade">
						<td class="RightTableCaption" align="right" HEIGHT="30" width="30%"><font color="red">*</font>&nbsp;Surgeon Type:</td>						
							<td>&nbsp;<gmjsp:dropdown controlName="surgeonType"  SFFormName="frmClinicalSurgeonSetup" SFSeletedValue="surgeonType"
							SFValue="alSurgeonType" width="250" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
            			</td>
					</tr>
					 <tr><td colspan="2" class="LLine" height="1"></td></tr>                   
					 <tr class="evenshade">                        
                        <td colspan="2" align="center" height="30">&nbsp;
                        <gmjsp:button value="Submit" buttonType="Save" name="btn_submit" gmClass="button" onClick="fnSubmit();" disabled="<%=lockedStudyDisable %>"/>
                        <gmjsp:button value="Reset" buttonType="Save" name="btn_reset" gmClass="button" onClick="fnReset('reset');" disabled="<%=lockedStudyDisable %>"/>                        
                        <gmjsp:button value="Void" buttonType="Save" name="btn_void" gmClass="button" onClick="fnVoid();" disabled="<%=lockedStudyDisable %>"/>                        
                        </td>
                    </tr> 
      				<tr><td colspan="2" class="LLine" height="1"></td></tr>
      				<tr> 
            <td colspan="2">
           <display:table name="requestScope.SURGEONSITES.rows" requestURI="/gmClinicalSurgeonSetup.do" excludedParams="strOpt"  class="its" id="currentRowObject" decorator="com.globus.displaytag.beans.GmClinicalSurgeonSetupWrapper"> 
			<display:column property="SID" title="" sortable="true"  />
			<display:column property="NAME" title="Surgeon Name" sortable="true"  />
			<display:column property="SURGEONTYPE" title="Surgeon Type" sortable="true"  />
			</display:table>            
		</td>
    </tr> 	   </td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>

