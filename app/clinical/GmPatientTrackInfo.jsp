<%
/**********************************************************************************
 * File		 		: GmPatientTrackInfo.jsp
 * Desc		 		: Report for the Planned Surgerys By Date
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>

<!-- clinical\GmPatientTrackInfo.jsp-->


<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<bean:define id="gridData" name="frmStudyFilterForm" property="gridXmlData" type="java.lang.String"> </bean:define>

<% 
String strStudyId = "";
String strSiteId = "";
String strPatientId = "";

strStudyId = GmCommonClass.parseNull((String)session.getAttribute("strSessStudyId"));
strSiteId = GmCommonClass.parseNull((String)session.getAttribute("strSessSiteId"));
strPatientId = GmCommonClass.parseNull((String)session.getAttribute("strSessPatientId"));
String strSessAccLvl = (String)session.getAttribute("strSessAccLvl");
String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Patient Tracking Info</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css"> 
<script language="JavaScript" src="<%=strJsPath%>/GmCommonGrid.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css"> 

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmPatientTrackInfo.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmClinicalCommonScript.js"></script> 
<script>
	objGridData = '<%=gridData%>';	
</script>
<script>
var objGridData;
objGridData = '<%=gridData%>';
var studyId = '<%=strStudyId%>';
var siteId = '<%=strSiteId%>';
var patientId = '<%=strPatientId%>';
var accesslvl = '<%=strSessAccLvl%>';
var pgToLoad = '';
var actionUrl = '';
</script>
</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();">
<html:form>
	<table border="0" cellspacing="0" cellpadding="0" class="DtTable765">
  		<tr style="height:25px">
   			<td style='height:25px' class="Header">&nbsp;Time Points</td>
  		</tr>
  		<tr><td class="LLine" height="1"></td></tr>        	
  		<tr> 
         	<td>
          		<div id="dataGridDiv" class="grid" style="height : 287px;"></div>            
  	 	 	</td>   
 		 </tr>
 		 <tr style="height:96px">
			<td style="height:96px">
			<!-- <b>a.</b> -->&nbsp;&nbsp; <font color=red><strong> Red </strong></font></t>= Past Due <br>
			<!--<b>b.</b> -->&nbsp;&nbsp; <font color=Orange><strong> Orange </strong></font></t>= Pending, 1 Month <br>
			<!--<b>c.</b> -->&nbsp;&nbsp; <font color=Green><strong> Green </strong></font></t>= Current Date <br>
			<!-- <b>e.</b> &nbsp;&nbsp; <font color=#808080><strong> Grey </strong></font></t>= Not Applicable <br> -->
			</td>
		</tr> 
	<tr class="shade">
						<td class = "oddshade" colspan="3" align="center" height="30">	
							<div class='exportlinks'>Export Options : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!-- | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a>  -->
                            </div>

							
						</td>
					</tr>	     
    </table> 		     	
</html:form>

</BODY>

</HTML>

