<%
/**********************************************************************************
 * File		 		: GmRadiographUpload.jsp
 * Desc		 		: File to upload the Radiographs
 * Version	 		: 1.0
 * author			: Brinalg
************************************************************************************/
%>

<!-- clinical\GmRadiographUpload.jsp -->


<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%-- <%@ taglib uri="/WEB-INF/gmjsp-taglib.tld" prefix="gmjsp"%> --%>						
<%@ page import="java.util.ArrayList,java.util.HashMap" %>
<%@ page import="java.util.Date"%> 
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("RADIOGRAPH_UPLOAD");

	ArrayList alPatient = new ArrayList();
	ArrayList alPeriods = new ArrayList();
	ArrayList alType = new ArrayList();
	ArrayList alPatientRecord = new ArrayList();
	String strPatient = "";
	String strPatientRecId = "";
	String strPeriod = "";
	String strType = "";
	String strView = "";
	String strXRayDt ="";
	String strFileName ="";
	String strFileID="";
	String strHeader="";
	String strRadImgPath ="";
	String strTypeName="";
	String strButtonDisabled = "";
	
	String strAction = GmCommonClass.parseNull((String) request.getAttribute("hAction"));
	strButtonDisabled = GmCommonClass.parseNull((String)request.getAttribute("strButtonDisabled"));
	
	alPatientRecord = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alPatientRecord"));
	alPatient = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("AlPatient"));
	alPeriods = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("AlPeriod"));
	alType = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("AlTyp"));
	
	if (strAction.equals("overwrite_upload") || strAction.equals("edit_attributes")){
		HashMap hmPatientDetails = (HashMap) alPatientRecord.get(0);
		strPatient = GmCommonClass.parseNull((String)hmPatientDetails.get("PAT_ID"));
		strPatientRecId = GmCommonClass.parseNull((String)hmPatientDetails.get("PAT_REC_ID"));
		strPeriod = GmCommonClass.parseNull((String)hmPatientDetails.get("TIME_PNT_ID"));
		strType = GmCommonClass.parseNull((String)hmPatientDetails.get("RAD_TYPE_ID"));		
		strView = GmCommonClass.parseNull((String)hmPatientDetails.get("RAD_VIEW_ID"));		
		strXRayDt = GmCommonClass.parseNull((String)hmPatientDetails.get("RADIOGRP_DATE"));
		strFileName = GmCommonClass.parseNull((String)hmPatientDetails.get("FILE_NM"));
		strFileID = GmCommonClass.parseNull((String)hmPatientDetails.get("FILE_ID"));
		strTypeName = GmCommonClass.parseNull((String)hmPatientDetails.get("RAD_TYPE_NAME"));
		
		strRadImgPath = "/GmCommonFileOpenServlet?uploadString=RADIOLOGYUPLOADIR&appendPath="+strTypeName.replaceAll("/", "").toUpperCase()+"&sId="+strFileID;
	}
	
	ArrayList alXray = new ArrayList();
	alXray = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("X-RAY_list"));
	ArrayList alMRI = new ArrayList();
	alMRI = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("MRI_list"));
	ArrayList alCTScan = new ArrayList();
	alCTScan = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("CTSCAN_list"));
	
	if(strAction.equals("new_upload"))
    { 
		strHeader=" Add New";
		strPatient = GmCommonClass.parseNull((String)request.getAttribute("PatientID"));
		strPeriod = GmCommonClass.parseNull((String)request.getAttribute("PeriodID"));
		strType = GmCommonClass.parseNull((String)request.getAttribute("Type"));		
		strView = GmCommonClass.parseNull((String)request.getAttribute("View"));	
		strXRayDt = GmCommonClass.parseNull((String)request.getAttribute("XRayDt"));
    }
	else if(strAction.equals("overwrite_upload"))
    { 
		strHeader=" Overwrite";
    }
	else if(strAction.equals("edit_attributes"))
    { 
		strHeader=" Edit Attributes";
    }
	// customer tag
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	Date dtXRayDt = null;
	dtXRayDt = GmCommonClass.getStringToDate(strXRayDt, strApplDateFmt);	
	String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
	
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<TITLE> Globus Medical: Shipping Include </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/gmRadioGraphUpload.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
hAction = '<%=strAction%>';
type = '<%=strType%>';
view = '<%=strView%>';
var overwrite;
</script>
</head>

<body onload="fnLoad();" >
<form name="frmRadioGraphUpload" method="post" action = "<%=strServletPath%>/GmClinicalUploadServlet" enctype="multipart/form-data">
<input type="hidden" name ="hAction" value="<%=strAction%>" />
<input type="hidden" name ="hPatientide" />
<input type="hidden" name ="hPatientRecordId" value="<%=strPatientRecId%>" />
<input type="hidden" name ="hTypeName" value="" />
<input type="hidden" name ="hViewName" value="" />
<input type="hidden" name ="hPeriodName" value="" />
<input type="hidden" name ="fileName" value="<%=strFileName%>" />
<table border="0"  class="DtTable765" cellspacing="0" cellpadding="0" >
		<tr>
			<td height="25" class="Header">&nbsp;Radiographs : <%=strHeader %></td>
			<td align="right" class=dhx_base_dhx_skyblue colspan="3"></td>
		<tr>
		<tr><td class="LLine" colspan="4"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr class="evenshade">
			<td class="RightTableCaption" HEIGHT="23" align="right"><font color="red">*</font>&nbsp; Patient ID :</td>
			<td>&nbsp;
		        <gmjsp:dropdown controlName="cbo_Patient" controlId="cbo_Patient" seletedValue="<%=strPatient%>" width="150" value="<%= alPatient%>" codeId = "PATID"  codeName = "PATIDE"  defaultValue= "[Choose One]" />
		    </td>
		    <td class="RightTableCaption" HEIGHT="23" align="right"><font color="red">*</font>&nbsp; TimePoint :</td>
			<td>&nbsp;
		         <gmjsp:dropdown controlName="cbo_Period"  controlId="cbo_Period" seletedValue="<%= strPeriod%>"  width="150" value="<%= alPeriods%>" codeId = "PERIODID"  codeName = "PERIODDS"  defaultValue= "[Choose One]" onChange="fnShowDate(this.value);" />	
		    </td>
		</tr>
		<tr><td class="LLine" colspan="4"></td></tr>
		<tr class="oddshade">
			<td class="RightTableCaption" HEIGHT="23" align="right"><font color="red">*</font>&nbsp; Type  :</td>
			<td>&nbsp;
		        <gmjsp:dropdown controlName="cbo_Type" controlId="cbo_Type"  seletedValue="<%=strType%>" width="150" value="<%= alType%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" onChange="fnlistView(this.value);"/>
		    </td>
		    <td class="RightTableCaption" HEIGHT="23" align="right"><font color="red">*</font>&nbsp; View  :</td>
			<td>&nbsp;
		        <select  name="cbo_View"  id="cbo_View" >  
		        	<option value ="0">Choose One </option>
		        </select>
		    </td>
		   </tr>
		<tr><td class="LLine" colspan="4"></td></tr>
		<tr class="evenshade"> 
		    <td class="RightTableCaption" HEIGHT="23" align="right"><font color="red">*</font>&nbsp; Date of X Ray   :</td>
			<td colspan="4">&nbsp;<gmjsp:calendar
									textControlName="Txt_XRayDt"
									textValue="<%=(dtXRayDt == null) ? null : new java.sql.Date(dtXRayDt.getTime())%>"
									gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
									onBlur="changeBgColor(this,'#ffffff');" />&nbsp;	
		    </td>		    
		</tr>
		<% String strDisableBtnVal = strButtonDisabled;
			if(strDisableBtnVal.equals("disabled")){
				strDisableBtnVal ="true";
			}
	    %>
		<tr><td class="LLine" colspan="4"></td></tr>
		<%if(!strAction.equals("edit_attributes"))	{ %>
		<tr>
			<td colspan="3" height="10" >
				<div>
					<jsp:include page="/common/GmUpload.jsp" >
					<jsp:param name="Confirm_Radiographs" value="Yes" />
					</jsp:include>	
				</div>
			</td>
			
			<%if(strAction.equals("overwrite_upload"))	{ %>
			<td height="10" valign="middle">			
				<gmjsp:button name="AddNew" value="Next Image" style="WIDTH: 120px;"  gmClass="button" buttonType="Save" onClick="fnAdd_new();" disabled="<%=strDisableBtnVal %>"/>
			</td>
			<% }%>			
		</tr>		
		<% }%>
		<%if(!strAction.equals("new_upload"))	{ %>
				<%if(strAction.equals("edit_attributes"))	{ %>
				<tr>
				
					 <td colspan="4" align="center"><br>
						<gmjsp:button name="Submit" value="Submit" gmClass="button" buttonType="Save" onClick="upload();" disabled="<%=strDisableBtnVal %>"/>
					</td>
				</tr>	
				<% }%>	
			<tr>
				 <td colspan="4">
					<b>File Name:</b> <%=strFileName%>
				</td>
			</tr>	
			<tr>			
				<td align="center" colspan="4">
				<br>
					<img   src="<%=strRadImgPath%>"  border="0" align="middle" height="200" width="200" />&nbsp;
				</td>
			</tr>
		<% }%>
		
		<tr>	
			<td colspan="4">
			<!-- the foll. div is needed to populate the view dropdown on change of type dropdown -->
			<div id ="drpdwn" style="display:hidden">
				<gmjsp:dropdown controlName="cbo_XRay"    width="150" value="<%=alXray%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" controlId="cbo_XRay" />
				<gmjsp:dropdown controlName="cbo_MRI"    width="150" value="<%=alMRI%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" controlId="cbo_MRI"/>
				<gmjsp:dropdown controlName="cbo_CTScan"   width="150" value="<%=alCTScan%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" controlId="cbo_CTScan"/>
			</div>
			</td>			
		</tr>
	
		  </table>   
			
 <%@ include file="/common/GmFooter.inc" %>	
 </form>	
</body>

</html>