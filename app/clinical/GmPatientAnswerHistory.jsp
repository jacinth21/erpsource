 <%
/**********************************************************************************
 * File		 		: GmPatientAnswerHistory.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>

<!--clinical\querysystem\GmPatientAnswerHistory.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	
	ArrayList alHistory = new ArrayList();
	alHistory = (ArrayList)request.getAttribute("HISTORYLIST");
	int intSize = 0;
	
	if (alHistory != null)
	{
		intSize = alHistory.size();
	}
	HashMap hcboVal = null;
	String strId = "";
	String strAns1 = "";
	String strAns2 = "";
	String strShade = "";
	String strCreatedBy = "";
	String strCreatedDt = "";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Patient Answer History </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmOrder" method="post">
	<table border="0" width="800" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" colspan="3"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td>
				<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0">
					<tr class="RightTableCaption" bgcolor="#eeeeee">
						<td width="150" height="24">&nbsp;Status</td>
						<td	width="200">&nbsp;History</td>
						<td width="200">&nbsp;Others (if any)</td>
						<td width="100">&nbsp;Edited By</td>
						<td width="80">&nbsp;Edited Date</td>
					</tr>
					<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
<%
					hcboVal = new HashMap();

			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alHistory.get(i);
			  			strId = (String)hcboVal.get("DISPLAY_TEXT");
						strAns1 = (String)hcboVal.get("ANSLISTID");
						strAns2 = (String)hcboVal.get("ANSDS");
						strCreatedBy = (String)hcboVal.get("CUSER");
						strCreatedDt = (String)hcboVal.get("CDATE");

						strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
%>
					<tr <%=strShade%>>
						<td class="RightText" height="20"><%=strId%></td>
						<td class="RightText" align="left"><%=strAns1%></td>
						<td class="RightText" align="left"><%=strAns2%></td>
						<td class="RightText" align="left"><%=strCreatedBy%></td>
						<td class="RightText" align="center"><%=strCreatedDt%></td>
					</tr>
					<tr><td colspan="5" height="1" bgcolor="#eeeeee"></td></tr>
<%
					}
%>
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" colspan="3"></td>
		</tr>
    </table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
