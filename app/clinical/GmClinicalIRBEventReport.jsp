<%
/**********************************************************************************
 * File		 		: GmClinicalIRBEventReport.jsp
 * Desc		 		: This is a report screen for Clinical IRB Event Report
 * Version	 		: 1.0
 * author			: HParikh
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<bean:define id="gridData" name="frmClinicalIRBReport" property="gridXmlData" type="java.lang.String"> </bean:define>
<% 
	boolean showGrid=true;	
	if(gridData!=null && gridData.indexOf("cell")==-1){
	   showGrid = false;			
	}
	String strSiteId = GmCommonClass.parseNull((String)session.getAttribute("strSessSiteId"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Clinical Site Map </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">


<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="javascript/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalIRBReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script>
	var objGridData;
	objGridData = '<%=gridData%>';
	document.onkeypress = function(){
		if(event.keyCode == 13){
			fnEventSubmit();
		}
		
	}
</script>
</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad()" >
<html:form action="/gmClinicalIRBReportAction.do">
<html:hidden property="strOpt" />
<html:hidden property="msg"/>
	
	<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">IRB Events</td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
		<tr>
				<td width="848"  valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<logic:notEqual name="frmClinicalIRBReport" property="msg" value="">
					<tr><td colspan="4" height="20"  class="RightBlueText">
					<bean:write name="frmClinicalIRBReport" property="msg"/></td>
										<tr><td class="Line" colspan="4"></td></tr>
					</tr>
					</logic:notEqual>
					<%
						if(strSiteId.equals(""))
						{
					%>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="evenshade">
						<td class="RightTableCaption" align="right" HEIGHT="30" ></font>&nbsp;Site List:</td>						
						<td><gmjsp:dropdown controlName="siteId"  SFFormName="frmClinicalIRBReport" SFSeletedValue="siteId"
							SFValue="alSiteList" width="150" codeId="ID" codeName="NAME" defaultValue="[Choose One]"/>
            			</td>            								
						<td>&nbsp;</td>
                        <td>&nbsp;</td>  
            			<td rowspan="12" class="LLine" width="1"></td>
						<td rowspan="12" align="center"><gmjsp:button value="&nbsp;&nbsp;Load&nbsp;&nbsp;" gmClass="button" buttonType="Load" onClick="fnEventSubmit();" /> </td>
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<%} %>  
					<tr>
						<td class="RightTableCaption"  align="right" HEIGHT="30" ></font>&nbsp;Appr Type:</td>						
						<td><gmjsp:dropdown controlName="irbAppTypeId"  SFFormName="frmClinicalIRBReport" SFSeletedValue="irbAppTypeId"
							SFValue="alIRBApprovalTypeList" width="150" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
            			</td>
						<td class="RightTableCaption"  align="right" HEIGHT="30" ></font>&nbsp;Appr Reason:</td>						
							<td><gmjsp:dropdown controlName="irbReasonId"  SFFormName="frmClinicalIRBReport" SFSeletedValue="irbReasonId"
							SFValue="alIRBApprovalReasonList" width="150" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
            			</td>
            			<%
						if(!strSiteId.equals(""))
						{
						%>
            			<td rowspan="12" class="LLine" width="1"></td>
						<td rowspan="12" align="center"><gmjsp:button value="&nbsp;&nbsp;Load&nbsp;&nbsp;" gmClass="button" buttonType="Load" onClick="fnEventSubmit();" /> </td>
						<%} %>             			     			
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
                    <tr class="evenshade">
                    	<td class="RightTableCaption"  align="right" HEIGHT="30" ></font>&nbsp;Action Taken:</td>						
							<td><gmjsp:dropdown controlName="irbActionTakenId"  SFFormName="frmClinicalIRBReport" SFSeletedValue="irbActionTakenId"
							SFValue="alIRBActionTakenList" width="150" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
            			</td>
                    	<td class="RightTableCaption" align="right" HEIGHT="30">&nbsp;Date Range:</td>
						<td><gmjsp:calendar SFFormName="frmClinicalIRBReport" controlName="startDate" toControlName ="endDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
                        </td>                         
                    </tr> 
                    <tr><td colspan="4" class="LLine" height="1"></td></tr>
                   </table> 
    			</td>		
  		  </tr>
  		  <tr> 
			<%if(showGrid){%>
				<td colspan="4">
			    	<div id="dataGridDiv" class="grid" style="height:316px"></div>
				</td>	
			<%}else{ %>
				<td class="RightText" colspan="4">
			    	Nothing found to display.
				</td>	
			<%}%>
    	 	</tr>
  		  <tr><td colspan="2" bgcolor="#cccccc"></td></tr> 
  		 <tr colspan = "2" class = "oddshade">
						<td class = "oddshade" colspan="3" align="center" height="30">	
							<div >Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!-- | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a> -->
                     	</td>
		 </tr>
  		  </table>
    		
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

