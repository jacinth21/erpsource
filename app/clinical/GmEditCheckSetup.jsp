<?xml version="1.0" encoding="UTF-8" ?>
   <%@include file="/common/GmHeader.inc"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<bean:define id="lockedStudyDisable" name="frmEditCheckForm" property="lockedStudyDisable" type="java.lang.String"> </bean:define>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Edit Check SetUp</title>
<link rel="stylesheet" type="text/css" href="styles/GmCommon.css"/>
<script language="JavaScript" src="javascript/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalEditCheckSetup.js"></script>
<script language="JavaScript" src="javascript/date-picker.js"></script>
<script language="JavaScript" src="javascript/Message.js"></script>
<script language="JavaScript" src="javascript/Error.js"></script>

<%
String editID="";
if(session.getAttribute("editID")!=null){
	editID=session.getAttribute("editID").toString();
}
String strDisableBtnVal= "";
if (lockedStudyDisable!=""){
	strDisableBtnVal="true";
} 
%>
</head>
<body leftmargin="20" topmargin="10">
<html:form action="/gmEditCheckSetupAction.do" method="post" styleId="form">
	<table border="0" cellspacing="0" cellpadding="0" class="GtTable700" style="top: 50px; left: 50px;">
	<tr>
	<td class="RightDashBoardHeader" height="30" align="left">Edit Check Setup</td>
	<td height="25" class="RightDashBoardHeader" align="Right"><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%//=strWikiPath%>','<%//=strWikiTitle%>');" /></td>
	</tr>
		<tr class="shade">
			<td align="Right" height="30"> <b><font color="red">*</font>Reference ID:&nbsp;</b></td>
			<html:hidden name="frmEditCheckForm" property="strOpt" value="Save" styleId="strOpt"/>
			<html:hidden name="frmEditCheckForm" property="editID" styleId="editID" value="<%=editID%>"/>
			<td class="TableText"><html:text name="frmEditCheckForm" readonly="true" property="refId" styleId="refId" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" maxlength="20"/></td>
		</tr>
		<tr>
			<td valign="top" align="Right" height="30"><b>Description:&nbsp;</b> </td>		
			<td class="TableText">
			<html:textarea name="frmEditCheckForm" property="description" styleId="description" rows="5" cols="45" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
			</td>
		</tr>
		<tr class="shade">
		<td valign="top" align="Right" height="30"><b>Specification:&nbsp; </b></td>
		<td class="TableText"><html:textarea name="frmEditCheckForm" property="specification" rows="5" cols="45" disabled="true" styleId="specification" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>
		</tr>
		<tr>
		<td align="Right"><b>Active:&nbsp;</b> </td>
		<td class="TableText">
			<html:checkbox property="activeFlag" styleId="activeFlag" value="Y" />
		</td>
		</tr>
		<tr>
		<tr><td colspan="2"></td></tr>
		<td colspan="2">
		<center><gmjsp:button gmClass="button" buttonType="Save" name="saveBut" value="Submit" onClick="fnSubmit();" disabled="<%=strDisableBtnVal%>"/>&nbsp;&nbsp;&nbsp;&nbsp;
		<gmjsp:button gmClass="button" buttonType="Load" name="saveBut" value="Close" onClick="javascript:window.close();"/>
<!-- 		<gmjsp:button name="addBut" value="Add New" gmClass="button" buttonType="Save" onClick="fnAddNew();"/>&nbsp;&nbsp;&nbsp;&nbsp;
		<gmjsp:button name="voidButton" disabled="true" value="Void" gmClass="button" buttonType="Save" onClick=""/> --> </center>
		</td>
		</tr>
		<tr><td colspan="2"></td></tr>
	</table>
</html:form>

<script type="text/javascript">
//document.getElementById("activeFlag").checked=true;
</script>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>