<%
/**********************************************************************************
 * File		 		: GmStudyFilterInc.jsp
 * Desc		 		: Include JSP
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>

<!--clinical\querysystem\GmPEDStudyFilterInc.jsp -->

<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %> 
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ taglib uri="/WEB-INF/gmjsp-taglib.tld" prefix="gmjsp"%>						
<%@ page import="com.globus.common.beans.GmCommonConstants"%> 
<%@ page import="com.globus.common.beans.GmCommonClass"%> 
<%@ page import="org.apache.struts.action.ActionForm"%>
<%@ page import="java.util.*"%> 
<%@ include file="/common/GmHeader.inc" %>
<% 

String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
String strEnableSiteList = GmCommonClass.parseNull(request.getParameter("ENABLESITELIST"));
String strHideLoadButton = GmCommonClass.parseNull(request.getParameter("HIDELOADBUTTON"));
String strChangeFunction = "";
String strDashboard=GmCommonClass.parseNull(request.getParameter("HIDDENSITELIST"));
String strSiteId = GmCommonClass.parseNull((String)session.getAttribute("strSessSiteId"));
if(strEnableSiteList.equals(""))
{
	strChangeFunction = "fnLoad();";
}

%>
<bean:define id="siteIds" name="<%=strFormName%>" property="strSiteIds" type="java.lang.String"> </bean:define>
<bean:define id="strQuestionnairre" name="<%=strFormName%>" property="questionnairre" type="java.lang.String"> </bean:define>
<script>
var siteIds = '<%=siteIds%>';
function fnCheckAll()
{

	var varEnableSiteList = '<%=strEnableSiteList%>';
	var varAction = document.all.strOpt.value;

	if (varEnableSiteList != null && (varAction == '' || varAction == 'Load'  || varAction == 'Dashboard' || varAction.indexOf("Link") != -1))
	{
		if(siteIds == '')
		{
			fnSelectAll('selectAll');
		}
	}

}	

function fnDeselect()
{
objSelAll = document.all.selectAll;
if (objSelAll) {
	objSelAll.checked = false;
	}
}

function fnSelectAll(varCmd)
{
				objSelAll = document.all.selectAll;
				if (objSelAll ) {
				objCheckSiteArr = document.all.checkSiteName;
//				objCheckSiteArrLen = objCheckSiteArr.length;

				if ((objSelAll.checked || varCmd == 'selectAll') && objCheckSiteArr)
				{
					objSelAll.checked = true;
					objCheckSiteArrLen = objCheckSiteArr.length;
					for(i = 0; i < objCheckSiteArrLen; i ++) 
					{	
						objCheckSiteArr[i].checked = true;
					}
				}
				else if (!objSelAll.checked  && objCheckSiteArr ){
					objCheckSiteArrLen = objCheckSiteArr.length;
					for(i = 0; i < objCheckSiteArrLen; i ++) 
					{	
						objCheckSiteArr[i].checked = false;
					}
				}
				}
	}
</script>
<body onload="fnCheckAll();">
	<table border="0" cellspacing="0" cellpadding="0" >
		<tr>
			<td valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>	                    
<%	
	if(strSiteId.equals(""))
	{
		if(strEnableSiteList.equals("")) 
			{
			if(GmCommonClass.parseNull(request.getParameter("HIDDENSITELIST")).equals(""))
				{ 
%>    		  
			
    		             <td class="RightTableCaption" align="right" HEIGHT="20" width="100">Site List :&nbsp;</td> 
                         <td>
                             <table cellspacing="0" cellpadding="0">
	                        	<tr width="55%">
	                        		<td bgcolor="gainsboro">
									    <html:checkbox property="selectAll" onclick="fnSelectAll('toggle');" /><B>Select All </B> 
		     						</td>
		     						<td width="39%"></td>
								</tr>	    
							</table>
							<DIV style="display:visible;height: 100px; width: 304px;  overflow: auto; border-width:1px; border-style:solid;">
							 	<table cellspacing="0" cellpadding="0">
		                        	<logic:iterate id="sitelist" name="<%=strFormName%>" property="alSiteNameList">
		                        	<tr>
		                        		<td>
										    <htmlel:multibox property="checkSiteName" value="${sitelist.ID}" onclick="fnDeselect();"/>
										    <bean:write name="sitelist" property="NAME" />
			     						</td>
									</tr>	    
									</logic:iterate>
								</table>
							</DIV>
								
							</td>
						<%	if(strHideLoadButton.equals("")) { %>
						 	<td style="width:500px;"></td>
<%
  							}   
			}
		}
	} 
%>    
					</tr>
					<tr>
						<td colspan=4>&nbsp;</td>
					</tr>	
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
						<td colspan=4 class="RightTableCaption" valign="middle">
									&nbsp;Endpoint : 
    	                			&nbsp;<gmjsp:dropdown controlName="questionnairre" SFFormName="frmOutcomeReport" SFSeletedValue="questionnairre"
										SFValue="alQuestionnairre" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"  onChange="fnLoadQuestionnaire();"/>													
    		            			
    		            			&nbsp;&nbsp;&nbsp;&nbsp;Scores : 
									&nbsp;<gmjsp:dropdown controlName="score"  SFFormName='frmOutcomeReport' SFSeletedValue="score"
						      			SFValue="alScore"  codeId="CODENMALT" codeName="CODENM" defaultValue="[Choose One]"/>
						      			
<%
							if(strQuestionnairre.equals("60803")||strQuestionnairre.equals("60903")){
%>						      			
									&nbsp;&nbsp;&nbsp;&nbsp;Treatment :   
                  	                &nbsp;<gmjsp:dropdown controlName="treatmentId" SFFormName="frmOutcomeReport" SFSeletedValue="treatmentId"
						        		SFValue="alTreatment" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[All]"  />													
<%
							}
%>				  			   
				     				<logic:equal name="frmOutcomeReport" property="studyListId" value="GPR002">
				     				<logic:equal name="frmOutcomeReport" property="questionnairre" value="60802">
										<html:hidden property="treatmentId" value="" />
									</logic:equal>
				     				</logic:equal>
				     				<%	if(strHideLoadButton.equals("")) { %>
						 			&nbsp;&nbsp;<gmjsp:button value="Load" gmClass="button" buttonType="Load" onClick="fnReport();"/>
<%
  							}
%>
						</td>
					</tr>     
				 </table>
  			  </td>
  		  </tr>	
    </table>	
  </body>  	     	

<%@ include file="/common/GmFooter.inc" %>


