<%
/**********************************************************************************
 * File		 		: GmClinicalSiteMap.jsp
 * Desc		 		: This is a report screen for Clinical Site Map setup
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc" %>
<bean:define id="gridData" name="frmClinicalDashBoardDispatch" property="gridXmlData" type="java.lang.String"> </bean:define>

<% 
boolean showGrid=true;
if(gridData!=null && gridData.indexOf("cell")==-1){
   showGrid = false;			
}
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Clinical Site Map </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalOutOfWindow.js"></script>
<style type="text/css" media="all">
     @import url("styles/screen.css");
</style>

<script>
	objGridData = '<%=gridData%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<html:form action="/gmClinicalStudyAction.do">
<html:hidden property="strOpt" />

	<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;Patient Out of Window Form</td>
		</tr>
		<tr><td colspan="2" bgcolor="#cccccc"></td></tr> 
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
		<tr> 
        	<td colspan="2">
          		<jsp:include page= "/clinical/GmIncludeClinicalGrid.jsp"/>
			</td>
		</tr> 	   
			</td>
  		</tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

