<%
/**********************************************************************************
 * File		 		: GmRptPlanSurgery.jsp
 * Desc		 		: Report for the Planned Surgerys By Date
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>

<!--clinical\querysystem\GmPatientTrackList.jsp -->
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ include file="/common/GmHeader.inc" %> 
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>

<% 
	

	String strhAction = (String)request.getAttribute("hAction");
	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Planned Surgery Date </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>

function fnLoad()
{
	document.frmPatientTrack.strOpt.value = "";
	document.frmPatientTrack.action = "/gmRptPatientTracking.do?method=reportPatientTrackList";
	document.frmPatientTrack.checkSiteName.value = "";
	document.frmPatientTrack.submit();
}


function fnReport()
{
	document.frmPatientTrack.action = "/gmRptPatientTracking.do?method=reportPatientTrackList";
	document.frmPatientTrack.strOpt.value = "Report";
	document.frmPatientTrack.submit();
}	

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmRptPatientTracking.do"  >
<html:hidden property="strOpt" />

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0" >
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;Patient Tracking Report - Site</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('http://gwiki.intranet/wiki','Patient_Tracking _Report');" /> 
			</td>
		</tr>
		<tr>
			<td valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td colspan="2" class="ELine"></td></tr>
					<tr><td colspan="2">&nbsp;</td></tr> 
                    <tr>
						<td colspan="2" align="center">
						   	<jsp:include page="/clinical/GmPatientTrackInc.jsp" >
							<jsp:param name="FORMNAME" value="frmPatientTrack" />	
							</jsp:include>	
						</td>
					</tr>
					<tr><td colspan="2">&nbsp;</td></tr> 
					<tr>
						<td colspan ="2" align=center> 
						<display:table cellspacing="0" style="text-align:center;" cellpadding="10" name="requestScope.frmPatientTrack.ldtResult" export="true"  
							id="currentRowObject" freezeHeader="true" paneSize="500" requestURI="/gmRptPatientTracking.do?method=reportPatientTrackList" class="its"
							decorator="com.globus.displaytag.beans.DTPatientTrackWrapper">
							<div style="text-align:center;" >	
								<display:setProperty name="export.excel.decorator" value="com.globus.displaytag.beans.DTPatientTrackWrapper" />
								<display:column property="LINK" title="Patient ID" headerClass="aaTopHeaderLightG" style="text-align:center;" media="html"/>
								<display:column property="NAME" title="Patient ID" headerClass="aaTopHeaderLightG" style="text-align:center;" media="excel csv xml"/>
								<display:column headerClass="Line" class="Line"  style="width:1;" media="html"/>
								<display:column property="PERIOD" title="Time Point" headerClass="aaTopHeaderLightG" style="text-align:center;" media="html excel csv xml"/>
								<display:column headerClass="Line" class="Line"  style="width:1;" media="html"/>
								<display:column property="WOPEN" title="Window Opens" headerClass="aaTopHeaderLightG" style="text-align:center;" media="html"/>
								<display:column headerClass="Line" class="Line"  style="width:1;" media="html"/>
								<display:column property="WOPENEXP" title="Window Opens" media="excel csv xml"/>
								<display:column property="WCLOSE" title="Window Closes" headerClass="aaTopHeaderLightG" style="text-align:center;" media="html"/>
								<display:column property="WCLOSEEXP" title="Window Closes" media="excel csv xml"/>
								<display:column headerClass="Line" class="Line"  style="width:1;" media="html"/>
								<display:column property="MISSING_FL" title="Missing Visits" headerClass="aaTopHeaderLightG" style="text-align:center;" media="html excel csv xml"/>
							</div>	
						</display:table>  
						</td>
					</tr>
					<logic:notEmpty name="frmPatientTrack" property="ldtResult" >
					<tr>
						<td colspan="2">
						<b>a.</b><font color=red><strong> Red </strong></font></t>= Past Due <br>
						<b>b.</b><font color=Orange><strong> Orange </strong></font></t>= Pending, 1 Month <br>
						<b>c.</b><font color=Green><strong> Green </strong></font></t>= Current Date <br>
						<b>d.</b><strong> -------- </strong></t>= Window Date Passed <br>
						<b>e.</b><font color=#808080><strong> Grey </strong></font></t>= Not Applicable <br>
						</td>
					</tr>
					</logic:notEmpty>	
						</td>
					</tr>
			   	</table>
  			   </td>
  		  </tr>	
  		
    </table>		     	
</html:form>

</BODY>

</HTML>

