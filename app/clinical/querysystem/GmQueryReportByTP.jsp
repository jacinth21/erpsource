
<%
	/**********************************************************************************
	 * File		 		: GmQueryReportByTP.jsp
	 * Desc		 		: 
	 * Version	 		: 1.0
	 * author			: Vamsi Kiran Nagavarapu
	 ************************************************************************************/
%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %> 
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ taglib uri="/WEB-INF/gmjsp-taglib.tld" prefix="gmjsp"%>
<%@ page import="com.globus.common.beans.GmCommonConstants"%>
<%@ page import="com.globus.common.beans.GmCommonClass"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.beans.GmGridFormat"%>
<%@ page import="com.globus.common.beans.GmLogger"%>
<%@ page import="org.apache.log4j.Logger"%>

<bean:define id="hmReport" name="frmQueryReportForm"
	property="hmCrossTabReport" type="java.util.HashMap"></bean:define>
<bean:define id="strPatientId" name="frmQueryReportForm" property="patientId" type="java.lang.String"> </bean:define>
<bean:define id="patientIDE" name="frmQueryReportForm"
	property="patientIDE" type="java.lang.String" ></bean:define>
<bean:define id="strsite_id" name="frmQueryReportForm"
	property="strsite_id" type="java.lang.String"></bean:define>
	

<%
	Logger log = GmLogger.getInstance(GmCommonConstants.CLINICAL); // Instantiating the Logger 
	try {
		
		log.debug(" SITE ID IS "+strsite_id);
		//GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
		GmGridFormat gmCrossTab = new GmGridFormat();

		//Neccessary Path's

		String strWikiPath = GmCommonClass.getString("GWIKI");
		String strWikiTitle = GmCommonClass.getWikiTitle("QUERY_BY_TIMEPOINT");
		String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
		String strCssPath = GmCommonClass.getString("GMSTYLES");
		String strImagePath = GmCommonClass.getString("GMIMAGES");
		String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
		String strCommonPath = GmCommonClass.getString("GMCOMMON");
		ArrayList alDrillDown = new ArrayList();

		//Rename the Column Name
		//	gmCrossTab.reNameColumn("Name","Timepoint");
		//	gmCrossTab.setValueType(0);

		// Add Lines
		//gmCrossTab.setColumnLineStyle("borderDark");
		//gmCrossTab.addLine("Name");

		// Setting Display Parameter
		//	gmCrossTab.setAmountWidth(91);
		//	gmCrossTab.setNameWidth(350);
		//	gmCrossTab.setRowHeight(22);

		// Page Body Style
		//	gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD");
		//gmCrossTab.addStyle("Name","ShadeMedGrayTD","ShadeDarkGrayTD");
		//gmCrossTab.addStyle("Total","ShadeMedBlueTD","ShadeDarkBlueTD") ;
		//gmCrossTab.setDecorator("com.globus.crosstab.beans.GmQueryByTimePointDecorator");
		//gmCrossTab.setExport(true, pageContext, "/gmQueryReportPerPatient.do?method=fetchQueryByTP&strExcel=Excel", " ");

		//-------------------------------------------------------------------------------------------
		//Setting the cross tab style
		//	gmCrossTab.reNameColumn("Name","Time-point");
		//	gmCrossTab.setValueType(0);
		//	gmCrossTab.setColumnWidth("Name",90);

		//	gmCrossTab.setAmountWidth(42);
		//	gmCrossTab.setRoundOffAllColumnsPrecision("0");
		//	gmCrossTab.setRowHeight(20);

		// Page Body Style
		//	gmCrossTab.setGeneralHeaderStyle("aaTopHeaderLightG");
		//	gmCrossTab.setDivHeight(400);
		//	gmCrossTab.setRowHighlightRequired(true);
		//	gmCrossTab.setTotalRequired(true);
		//	gmCrossTab.setColumnTotalRequired(true);
		//	gmCrossTab.setGroupingRequired(true);
		//	gmCrossTab.setHideColumn("DueDate");
		//	gmCrossTab.setHideColumn("STUDYPERIODID");
		//gmCrossTab.setColumnLineStyle("borderDark");
		//	gmCrossTab.setDecorator("com.globus.crosstab.beans.GmPatientTrackDecorator");
		//gmCrossTab.setExport(true, pageContext, "/gmRptPatientTracking.do?method=reportPatientTrackForm&strExcel=Excel", "PatientFormTracks");
		//	gmCrossTab.setLinkRequired(false);

		//	gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD");
		//	gmCrossTab.addStyle("Name","ShadeMedGrayTD","ShadeDarkGrayTD");
		//gmCrossTab.addStyle("Total","ShadeMedBlueTD","ShadeDarkBlueTD") ;
		//gmCrossTab.setDecorator("com.globus.crosstab.beans.GmQueryByTimePointDecorator");

		gmCrossTab.setValueType(0);

		//Setting the cross tab style
		gmCrossTab.reNameColumn("Name", "Time-point");
		gmCrossTab.setColumnWidth("Name", 90);
		gmCrossTab.setColumnWidth("Time-point", 90);

		//gmCrossTab.setAmountWidth(42);
		gmCrossTab.setRoundOffAllColumnsPrecision("0");
		gmCrossTab.setRowHeight(20);

		// Page Body Style
		gmCrossTab.setGeneralHeaderStyle("aaTopHeaderLightG");
		gmCrossTab.setDivHeight(400);
		gmCrossTab.setRowHighlightRequired(true);
		gmCrossTab.setTotalRequired(true);
		gmCrossTab.setColumnTotalRequired(false);
		gmCrossTab.setGroupingRequired(true);
		//gmCrossTab.setHideColumn("DueDate");
		//gmCrossTab.setHideColumn("STUDYPERIODID");
		gmCrossTab.setColumnLineStyle("borderDark");
		//gmCrossTab.setDecorator("com.globus.crosstab.beans.GmPatientTrackDecorator");
		gmCrossTab.setDecorator("com.globus.crosstab.beans.GmQueryByTimePointDecorator");
		//gmCrossTab.setExport(true, pageContext, "/gmRptPatientTracking.do?method=reportPatientTrackForm&strExcel=Excel", "PatientFormTracks");
		gmCrossTab.setExport(true, pageContext, "/gmQueryReportPerPatient.do?method=fetchQueryByTP&strExcel=Excel", " ");

		gmCrossTab.setLinkRequired(false);

		String strGridData = gmCrossTab.generateGridXML(hmReport);

		log.debug(" Message " + strGridData);
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Query By Timepoint</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css"
	href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css"
	href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css"
	href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="javascript/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>






<script>
var objGridData = '<%=strGridData%>';
	
function fnOnPageLoad(){

	gridObj = initGrid('dataGridDiv',objGridData,1);
	
	
	gridObj.attachFooter("<div id ='Footer'align ='left'>Total</div> ,<div id='SC1' align = 'left'>0</div>,<div id='SC2' align = 'left'>0</div>,<div id='SC3' align = 'left'>0</div>,<div id='SC4' align = 'left'>0</div>,<div id='SC5'  align = 'left'>0</div>,<div id='SC6'  align = 'left'>0</div>,<div id='SC7'  align = 'left'>0</div>,<div id='SC8'  align = 'left'>0</div>,<div id='SC9'  align = 'left'>0</div>,<div id='SC10'  align = 'left'>0</div>,<div id='SC11'  align = 'left'>0</div>,<div id='SC12'  align = 'left'>0</div>,<div id='SC13'  align = 'left'>0</div>,<div id='SC14'  align = 'left'>0</div>,<div id='SC15'  align = 'left'>0</div>,<div id='SC36'  align = 'left'>0</div>,<div id='Total'  align = 'left'>0</div>", ["text-align:center;"]);
	
	var SC1 = document.getElementById("SC1");
    SC1.innerHTML = sumColumn(1);
	var SC2 = document.getElementById("SC2");
    SC2.innerHTML = sumColumn(2);
    var SC3 = document.getElementById("SC3");
    SC3.innerHTML = sumColumn(3);
    var SC4= document.getElementById("SC4");
    SC4.innerHTML = sumColumn(4);
    var SC5 = document.getElementById("SC5");
    SC5.innerHTML = sumColumn(5);
    var SC6 = document.getElementById("SC6");
    SC6.innerHTML = sumColumn(6);
	var SC7 = document.getElementById("SC7");
    SC7.innerHTML = sumColumn(7);
    var SC8 = document.getElementById("SC8");
    SC8.innerHTML = sumColumn(8);
    var SC9= document.getElementById("SC9");
    SC9.innerHTML = sumColumn(9);
    var SC10 = document.getElementById("SC10");
    SC10.innerHTML = sumColumn(10);
    var SC11 = document.getElementById("SC11");
    SC11.innerHTML = sumColumn(11);
	var SC12 = document.getElementById("SC12");
    SC12.innerHTML = sumColumn(12);
    var SC13 = document.getElementById("SC13");
    SC13.innerHTML = sumColumn(13);
    var SC14= document.getElementById("SC14");
    SC14.innerHTML = sumColumn(14);
    var SC15 = document.getElementById("SC15");
    SC15.innerHTML = sumColumn(15);
	var SC36= document.getElementById("SC36");
    SC36.innerHTML = sumColumn(16);
    var Total = document.getElementById("Total");
    Total.innerHTML = sumColumn(17);
}

function sumColumn(ind) {
    var out = 0;
    for (var i = 0; i < gridObj.getRowsNum(); i++) {
    
    	
    	if(!parseFloat(gridObj.cells2(i, ind).getValue()))
    	{
    		out += 0;
    	}
    	else
    	{
    	    out += parseFloat(gridObj.cells2(i, ind).getValue());
        }
    }
    return out;
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<html:form action="/gmQueryReportPerPatient.do">

	<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">Query Per Timepoint</td>
			<td height="25" class="Header" align="Right"><a href="javascript:history.go(-1);">&nbsp;Back&nbsp;</a></td>
		</tr>
	</table>
	<table class="DtTable765">
		<tr height="30" valign="center">
			<td colspan="2" class="RightTableCaption">&nbsp; Patient IDE # :
			<%=patientIDE%></td>
		</tr>
		<tr>
			<td>
			<div id="dataGridDiv" style="" height="300px"></div>
			</td>
		</tr>
		<tr>
						<td class = "shade" colspan="3" align="center" height="30">	
							Export Options : <a href="/gmQueryReportPerPatient.do?method=fetchQueryByTP&patientId=<%=strPatientId%>&strsite_id=<%=strsite_id%>&strExcel=Excel&patientIDE=<%=patientIDE%>"> <span class="export excel">Excel </span></a>
						</td>
					</tr>	
	</table>

</html:form>
<%
	} catch (Exception exp) {
		exp.printStackTrace();
	}
%>
</BODY>
</HTML>

