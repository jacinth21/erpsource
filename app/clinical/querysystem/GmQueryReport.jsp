 <%
/**********************************************************************************
 * File		 		: GmPatientAnswerHistory.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<bean:define id="gridData" name="frmQueryForm" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="strListQueries" name="frmQueryForm" property="listQueries" type="java.lang.String">
</bean:define>

<% 
boolean showGrid=true;
if(gridData!=null && gridData.indexOf("cell")==-1){
   showGrid = false;			
}
int queryAccess = Integer.parseInt((String)request.getAttribute("queryAccess"));
String strWikiTitle = GmCommonClass.getWikiTitle("QUERIES");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Clinical Site Map </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmQueryReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<style type="text/css" media="all">
     @import url("styles/screen.css");
</style>

<script>
	objGridData = '<%=gridData%>';
</script>
</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();">
<html:form action="/gmQuery.do">
	<html:hidden property="strOpt" />
	<html:hidden property="queryId" />
	<html:hidden property="patientListId" />
	<html:hidden property="patientAnsListId" />
	<html:hidden property="queryLevelId" />
	<input type="hidden" name="listQueries" value="<%=strListQueries%>" >

	<table border="0"  cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0">	
				<tr > 
					      <td class="Header"  align="left"  Height=25>&nbsp;&nbsp;Queries </td>			 		         		 
	 	            </tr>			
      				<tr > 
			            <td colspan="2">
			           		<div id="dataGridDiv" style="height:385px;"></div>
						</td>
					<%if(!showGrid){ %>							
						<tr><td height="30" valign="center"><strong>&nbsp;&nbsp;No query has been raised yet!!!<strong></td></tr>						
					<%}%>	
    				</tr> 	   
				</table>
			</td>
		</tr>
    </table>
    <BR>
    <%if (queryAccess > 0){  %>
    <div align="center">
		<gmjsp:button gmClass="button" buttonType="Load" value="&nbsp;New Query&nbsp;" onClick="fnNewQuery();"/>
	</div>
	<%}%>
	<%@ include file="/common/GmFooter.inc" %>
</html:form>
</BODY>
</HTML>
