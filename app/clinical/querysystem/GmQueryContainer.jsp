 <%
/**********************************************************************************
 * File		 		: GmEnterprisePortal.jsp
 * Desc		 		: This screen is used for the all Portals  - Frameset
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import="com.globus.common.beans.GmLogger"%>
<%@ page import="org.apache.log4j.Logger"%>

<%@ include file="/common/GmHeader.inc"%>
<%
	 
	String strPatientListId = GmCommonClass.parseNull((String)request.getParameter("patientListId"));
	String strPatientAnsListId = GmCommonClass.parseNull((String)request.getParameter("patientAnsListId"));
	String strQueryLevelId = GmCommonClass.parseNull((String)request.getParameter("queryLevelId"));
	String strListQueries = GmCommonClass.parseNull((String)request.getParameter("listQueries"));
	int queryAccess = Integer.parseInt((String)request.getAttribute("queryAccess"));
	
%>

<style type="text/css" media="all">
	@import url("styles/screen.css");
</style>
<html>
<head>
<title>Globus Medical: Enterprise Portal</title>
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
</head>
<% if(queryAccess>0){%>
	<FRAMESET border=0 name=fstMain1 rows="92%,8%" TARGET="_parent">
		<FRAMESET border=0 name=InnerFrame1 cols="37%,63%" framspacing="20">
	    	<FRAME border=0 scrolling="no" name=LeftFrame1  bordercolor="#8B9ACF" frameBorder=1 src="/gmQuery.do?method=fetchQueryReport&listQueries=<%=strListQueries%>&queryLevelId=<%=strQueryLevelId%>&patientAnsListId=<%=strPatientAnsListId%>&patientListId=<%=strPatientListId%>&fromContainer=true" scrolling="Auto" marginHeight=0>
	    	<FRAME border=0 scrolling="yes" name=RightFrame1 bordercolor="#8B9ACF" frameBorder=1 src="/gmQuery.do?method=fetchQueryDetails&listQueries=<%=strListQueries%>&patientListId=<%=strPatientListId%>&patientAnsListId=<%=strPatientAnsListId%>&queryId=&queryLevelId=<%=strQueryLevelId%>&fromContainer=true" scrolling="Auto" marginHeight=0>
		</FRAMESET>
		<FRAME border=0 name=LeftFrame1 frameBorder=1 bordercolor="#8B9ACF" src="/gmQuery.do?method=fetchQueryWindowClose" scrolling="Auto" marginHeight=0 >
	</FRAMESET>
<%}else{ %>
	<FRAMESET border=0 name=InnerFrame1 cols="37%,63%" framspacing="20">
    	<FRAME border=0 scrolling="no" name=LeftFrame1  bordercolor="#8B9ACF" frameBorder=1 src="/gmQuery.do?method=fetchQueryReport&listQueries=<%=strListQueries%>&queryLevelId=&patientAnsListId=&patientListId=" scrolling="Auto" marginHeight=0>
    	<FRAME border=0 scrolling="yes" name=RightFrame1 bordercolor="#8B9ACF" frameBorder=1 src="" scrolling="Auto" marginHeight=0>
	</FRAMESET>
<%}%>
</body>
</HTML>
