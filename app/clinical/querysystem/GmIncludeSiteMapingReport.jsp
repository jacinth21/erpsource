<%
/**********************************************************************************************
 * File		 		: GmIncludeContactInfo.jsp
 * Desc		 		: JSP file to display the contact display tag in setup and summary pages
 * Version	 		: 1.0
 * author			: Satyajit Thadeshwar
/***********************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<style type="text/css" media="all">
	@import url("styles/screen.css");
</style>
<% 
	String displayType = request.getParameter("TYPE");
	String strMedia = displayType.equalsIgnoreCase("Setup") ? "HTML" : "FALSE";
	boolean bSortable = displayType.equalsIgnoreCase("Setup") ? true : false;
	bSortable = false;
%>
<display:table name="requestScope.frmSiteMapingForm.lresult" class="gmits" id="currentRowObject"  export = "false" 
	decorator="com.globus.party.displaytag.DTSiteMappingInfoWrapper" > 
	<display:column property="PARTY_ID" title="" class="aligncenter" media="csv excel xml"/>
	<display:column property="ID" title="" class="aligncenter" />
	<display:column property="PARTY_NM" title="Site Coordinator Name" class="alignleft" /> 
	<display:column property="STUDY_NAME" title="Study Name" sortable="<%=bSortable%>" class="alignleft"/>
	<display:column property="ACC_NAME" title="Site Name" sortable="<%=bSortable%>" class="alignleft" />
	<display:column property="PRIMARYFL" title="Primary Coordinator" sortable="<%=bSortable%>"  class="aligncenter" />	
</display:table>