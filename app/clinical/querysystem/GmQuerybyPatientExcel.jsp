<%@ page import="com.globus.common.beans.GmCommonClass"%> 
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/gmjsp-taglib.tld" prefix="gmjsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ page buffer="16kb" autoFlush="true" %>

<%
String strPatientIde = GmCommonClass.parseNull(request.getParameter("patientIDE"));
String strCssPath = "";
response.setContentType("application/vnd.ms-excel");
response.addHeader("content-disposition", "attachment; filename = Request-QueryperPatient.xls");
%>
<html>
<head>
<TITLE> Query Per Patient </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
</head>

<body topmargin="10" leftmargin="10">
<br>
<h3 align="left"> Globus Medical: Query Per Patient </h3>
<table border="1" class="DtTable850" cellspacing="1" cellpadding="1">
<tr>
	<td colspan="9" height="30"> <b>Patient IDE : </b> <%=strPatientIde%></td>
</tr>
<tr>
  <td class = "Shade" colspan="9">
				<display:table name="ALRETURN"   class="its" id="currentRowObject" > 				
				<display:column property="QUERY_ID" title="Query Id" class="alignleft"   />
				<display:column property="STUDY_PERIOD" title="Timepoint" class="alignleft"  />
			  	<display:column property="FORM_DS" title="Form" class="alignleft"   />
			  	<display:column property="STATUS" title="Current Status" class="alignleft"   />
			  	<display:column property="ISSUE_DATE" title="Issue Date" class="aligncenter"   />
			  	<display:column property="RESOLVED_DATE" title="Resolved Date" class="aligncenter"   />
			  	<display:column property="OUTSTANDING_DAYS" title="Days Outstanding" class="aligncenter"   />
				</display:table> 
				</td>
</tr>
</table>
</body>

<html>

