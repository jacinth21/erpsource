<%
/**********************************************************************************
 * File		 		: GmQuerybyTimeoutExcel.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Vamsi Kiran Nagavarapu
************************************************************************************/
%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %> 
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ taglib uri="/WEB-INF/gmjsp-taglib.tld" prefix="gmjsp"%>
<%@ page import="com.globus.common.beans.GmCommonConstants"%> 
<%@ page import="com.globus.common.beans.GmCommonClass"%> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ page import="com.globus.common.beans.GmLogger"%> 
<%@ page import="org.apache.log4j.Logger"%> 

<bean:define id="hmReport" name="frmQueryReportForm" property="hmCrossTabReport" type="java.util.HashMap" ></bean:define>

<bean:define id="Type" name="frmQueryReportForm" property="type" type="java.lang.String" ></bean:define>

<bean:define id="patientIDE" name="frmQueryReportForm" property="patientIDE" type="java.lang.String" ></bean:define>


<% Logger log = GmLogger.getInstance(GmCommonConstants.CLINICAL);  // Instantiating the Logger 
try{	
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	ArrayList alStyle 		= new ArrayList();
	String strPatientIDE = "";
	//log.debug(" Size of hmReport in the beginning " + hmReport.size());

	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	String strCommonPath = GmCommonClass.getString("GMCOMMON");
	ArrayList alDrillDown 	= new ArrayList();
	

	String strhAction = (String)request.getAttribute("hAction");
	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}
	
	
		
	// Setting the cross tab style 
	// Page Body Style
	gmCrossTab.setGeneralHeaderStyle("aaTagHeader");
	gmCrossTab.setDivHeight(400);
	gmCrossTab.setRowHighlightRequired(true);
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setGroupingRequired(true);
	gmCrossTab.setValueType(0);
	gmCrossTab.setHideColumn("DueDate");
	gmCrossTab.add_RoundOff("Average # of Days Outstanding","1");
	//gmCrossTab.reNameColumn("Name","Time-point");
	
	if ( Type == "BYSTUDY"){
		gmCrossTab.reNameColumn("Name","Study");
		gmCrossTab.setHideColumn("Total");
	}
	else if (Type == "BYPATIENT"){
		gmCrossTab.reNameColumn("Name","Patient");
		gmCrossTab.setHideColumn("Total");
	}
	else if (Type == "BYSITE"){
		gmCrossTab.reNameColumn("Name","Site");	
		gmCrossTab.setHideColumn("Total");
	}
	else
	{
		strPatientIDE = " <b> Patient IDE # : " + patientIDE +"<b> <br> ";
		gmCrossTab.reNameColumn("Name","Time-point");
		gmCrossTab.setTotalRequired(true);
		gmCrossTab.setColumnTotalRequired(true);
		gmCrossTab.setDecorator("com.globus.crosstab.beans.GmPatientTrackDecorator");
	}

	gmCrossTab.setColumnLineStyle("borderDark");

//	gmCrossTab.setDecorator("com.globus.crosstab.beans.GmQueryReportDecorator");
	//gmCrossTab.setLinkRequired(false);
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Enrollment by month </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style>
/*****************************************************************************/
/* General Section*/
/*****************************************************************************/
BODY
{
    scrollbar-base-color:#8C8CC6;
    scrollbar-3dlight-color:#3C3C3C;
    scrollbar-arrow-color:#3C3C3C;
    scrollbar-darkshadow-color:#DBD7D0;
    scrollbar-face-color:#E5E1D9;
    scrollbar-highlight-color:#DBD7D0;
    scrollbar-shadow-color:#3C3C3C;
    scrollbar-track-color:#EFEFEF;
}

.GreyShadeTD{
	background-color: #808080;
	border: solid 1px white;
}


table.border {
	margin: 0 0 0 0;
	border: 1px solid  #676767;
}

body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.formfont {
	FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: Arial, Geneva, Helvetica
}

.TabMouseOver {
	COLOR : White;
	font-family: verdana, arial, sans-serif;
	font-size : 10px;
	font-weight : bold;
	font-style: normal;
}

A.TabMouseOver {
	COLOR : White; 
	font-family: verdana, arial, sans-serif;
	font-size : 10px;
	font-weight : bold;
	font-style: normal;	
	text-decoration: none;
	}
A.TabMouseOver:visited {
	COLOR: White;
}
A.TabMouseOver:hover {
	COLOR: #009CFF;
}

/***************************************************************************************
Used for Save and Updated Button 
***********************************************************************************************/
.Button{
	background-color : #2E73AC;
	border-left : 1 solid black;
	border-right : 1 solid black;
	border-top : 1 solid black;
	border-bottom : 1 solid black;
	font-family: verdana, arial, sans-serif;
	font-size : 10px;
	font-weight : bold;
	font-style: normal;
	COLOR: White;
	cursor:hand;
}

.ButtonNext{
	background-color : #00639C;
	border-left : 1 solid white;
	border-right : 1 solid black;
	border-top : 1 solid white;
	border-bottom : 1 solid black;
	font-family: Webdings;
	font-size : 11px;
	font-weight : bold;
	font-style: normal;
	COLOR: White;
	padding : -4 0 2 0;
	cursor:hand;
}

.ButtonReset{
	background-color : #C40000;
	border-left : 1 solid black;
	border-right : 1 solid black;
	border-top : 1 solid black;
	border-bottom : 1 solid black;
	font-family: verdana, arial, sans-serif;
	font-size : 10px;
	font-weight : bold;
	font-style: normal;
	COLOR: White;
	cursor:hand;
}
/*****************************************************************************/
/* Top frame Setting*/
/*****************************************************************************/
.TopTabHeader{
	background: #00009C;
	color : White;
}

.TopTableLayer{
	background: #00009C;
}

.TopSelectedTabHeader{
	color: #00009C;
	font-family: verdana, arial, sans-serif;
	font-size : 10px;
	font-weight : bold;
	font-style: normal;	
	background: White;

}

.TopHeaderCapition{
	BORDER-TOP-WIDTH: 0px;
	BORDER-LEFT-WIDTH: 0px;
	BORDER-BOTTOM-WIDTH: 0px;
	BORDER-RIGHT-WIDTH: 0px;
	BACKGROUND-COLOR: #6699cc;
	font-size : 14px;
	font-weight : bold;
	font-style: italic;
	TEXT-ALIGN: right;
	color : White;
}

/*****************************************************************************/
/* Left frame details*/
/*****************************************************************************/
.LeftTableLayer{
	background: #6699cc;
}

	
.LeftTableBody{
	background: #EAEAEA;
}

.LeftTableHeader{
	background: #eeeeee;
}

/*****************************************************************************/
/* Right frame details (Screen Settings)*/
/*****************************************************************************/
.RightScreenBody {
	background : White;
}

.RightTableBody{
	background: White;
}

.RightTableLayer{
	background: #6699cc;
}


.RightHeaderCaption {
	COLOR : #3074AC;
	font-family: verdana, arial, sans-serif;
	font-size : 10px;
	font-weight : bold;
	font-style: normal;
}

.RightBigHeader {
	COLOR : #0064A0;
	font-family: sans-serif;
	font-size : 16pt;
	font-weight : bold;
	font-style: italic;
}

.InputArea {
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 11px;
	font-style: normal;
	border-left : 1 solid black;
	border-right : 1 solid black;
	border-top : 1 solid black;
	border-bottom : 1 solid black;
}

.RightDisableArea{
	BORDER-TOP-WIDTH: 1px;
	BORDER-LEFT-WIDTH: 1px;
	BORDER-BOTTOM-WIDTH: thin;
	BORDER-RIGHT-WIDTH: thin;
	background-color=#eeeeee;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 11px;
	font-style: normal;
}
/* This Style sheet setting is only used by Avail Summary Screen 1.3.2 */

.RightTextAS {
	COLOR : Black;
	font-family: sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.RightDisableAreaAS{
	BORDER-TOP-WIDTH: 1px;
	BORDER-LEFT-WIDTH: 1px;
	BORDER-BOTTOM-WIDTH: thin;
	BORDER-RIGHT-WIDTH: thin;
	background-color=#eeeeee;
	COLOR : Black;
	font-family: sans-serif;
	font-size : 9px;
	font-style: normal;
}

.RightInputAreaAS {
	COLOR : Black;
	font-family: sans-serif;
	font-size : 9px;
	font-style: normal;
}

.listbox{
	background-color : White;
	color : Black;
	font : normal 8pt Courier New;
}

.RightTableHeader{
	font-family: verdana, arial, sans-serif;
	color: white;
	font-size : 12pt;
	font-style: normal;
	font-weight: bold;
	background : #8B9ACF;
}


/*****************************************************************************/
/* Sub Menu (Tree Structure )
/*****************************************************************************/
.SubMenuMouseOver {
	COLOR : #0064A0;
	font-family: verdana, arial, sans-serif;
	font-size : 10px;
	font-weight : bold;
	font-style: normal;
}

A.SubMenuMouseOver {
	COLOR : #0064A0;
	font-family: verdana, arial, sans-serif;
	font-size : 10px;
	font-weight : bold;
	font-style: normal;
	text-decoration: none;
}
A.SubMenuMouseOver:visited {
	COLOR: #0064A0;
}
A.SubMenuMouseOver:hover {
	COLOR: #008FE6;
}
/*****************************************************************************/
/* Added Offshore
/*****************************************************************************/
.RightTableText {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
	background-color: #eeeeee;
}

.TableText {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
}

.RightText {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
}

.ARightText {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
	TEXT-ALIGN: center;
}

.RightTextSmall {
	FONT-SIZE: 9px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
}

A.RightText {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
	text-decoration: underline;
}

A.RightText:hover {
	FONT-SIZE: 11px; 
	COLOR: blue; 
	FONT-FAMILY: verdana, arial, sans-serif;
	text-decoration: underline;
}

.MultiList{
	background-color : White;
	color : Black;
	font : normal 8pt monospace;
}

.RightTableCaption {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
}

.RightYellowCaption {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	background-color : bisque;
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
}

.RightBlueCaption {
	FONT-SIZE: 11px; 
	COLOR: black; 
	background-color : #d3e2e9;
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
}

.Line{
	background-color: #676767;
	height : 1;
}

.Empty{
	background-color: WHITE;
	height : 1;
}

.ELine{
	background-color: #eeeeee;
	height : 1;
}

.LLine{
	background-color: #cccccc;
	height : 1;
}

.AsofDate{
	background-color: #CCCCCC;
}

.TopNavTextOff{
	Font-Family: Arial, Helvetica, sans-serif;
	color:"#9B9BFF";
	background-color: #4646A9;
	FONT-SIZE: 10pt; 
}

.TopNavTextOn{
	Font-Family: Arial, Helvetica, sans-serif;
	font-weight: bold;
	color:"#FFFFFF";
	background-color: #4646A9;
	FONT-SIZE: 10pt; 
}

.TopHeaderTxt{
	color : white;
	Font-Family: Arial, Helvetica, sans-serif;
	font-weight: bold;	
	FONT-SIZE: 8pt;	
}

TR.Shade{
	background-color: #eeeeee;
}

TR.ShadeRightTableCaption{
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
	background-color: #cccccc;
}
.FooterText {
	FONT-SIZE: 9px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
}

.HyperText {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
	text-decoration: none;
}
A:hover {
        text-decoration: underline;
}

.RightTextRed { 
	FONT-SIZE: 11px; 
	COLOR: #FF0000; 
	FONT-FAMILY: verdana, arial, sans-serif;
	text-decoration: None;
}

A.RightTextRed { 
	FONT-SIZE: 11px; 
	COLOR: #FF0000; 
	FONT-FAMILY: verdana, arial, sans-serif;
	text-decoration: underline;
}

A.RightTextRed:hover { 
    text-decoration: underline;
	FONT-SIZE: 11px; 
	COLOR: blue; 
	FONT-FAMILY: verdana, arial, sans-serif;
}

.RightTableCaptionRed {
	FONT-SIZE: 11px; 
	COLOR: #FF0000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
}
.RightHeader { 
	FONT-SIZE: 13px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
}
TD.CustTableCaption{
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
	background-color: #cccccc;
}

.SubMenuSelected { 
	COLOR: #009CFF;
	font-family: verdana, arial, sans-serif;
	font-size : 10px;
	font-weight : bold;
	font-style: normal;
	text-decoration: none;
}

.RightDisableTextArea{
	background-color=#eeeeee;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 11px;
	font-style: normal;
}

.MenuItem {
 text-align: center;
 background: #2E73AC;
 border-left:1px #000000 solid;
 cursor:hand;
 vertical-align:middle;
 text-align: center;
}

.MenuItemActive {
 text-align: center;
 background: #4791C5;
 border-left:1px #000000 solid;
 vertical-align:middle;
 text-align: center;
}

.MenuItemText {
 font-family: Arial;
 font-size: 8pt;
 font-weight: bold;
 color: #ffffff;
 vertical-align:middle;
 text-align: center;
 text-decoration:none;

}

.PortalText {
	FONT-SIZE: 10px; 
	COLOR: #2E73AC; 
	FONT-FAMILY: verdana, arial, sans-serif;
	text-decoration: none;
}

.RightTextBlue {
	FONT-SIZE: 11px; 
	COLOR: blue; 
	FONT-FAMILY: verdana, arial, sans-serif;
}

.RightTextPurple {
	FONT-SIZE: 11px; 
	COLOR: #8000FF; 
	FONT-FAMILY: verdana, arial, sans-serif;
}

.RightTextGreen {
	FONT-SIZE: 11px; 
	COLOR: green; 
	FONT-FAMILY: verdana, arial, sans-serif;
}

.RightTextBrown {
	FONT-SIZE: 11px; 
	COLOR: brown; 
	FONT-FAMILY: verdana, arial, sans-serif;
}


.RightDashBoardHeader { 
	background: #4791C5;
	FONT-SIZE: 13px; 
	COLOR: #FFFFFF; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
}

.TableHeaderRow { 
	background: #4791C5;
	FONT-SIZE: 11px; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
}

TR.ShadeRightTableCaptionBlue{
	FONT-SIZE: 11px; 
	COLOR: #4791C5; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
	background-color: #AACCE8;
}

TR.ShadeBlue{
	background-color: #ffffff;
}

TR.ShadeBlueBk{
	background-color: #ecf6fe;
}

.RightTextSm {
	COLOR : Black;
	font-family: sans-serif;
	font-size : 9px;
	font-style: normal;
}

TR.ShadeRightTableCaptionSm{
	FONT-SIZE: 10px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
	background-color: #eeeeee;
}

DIV.gmFilter{
	width: 200px;
	height: 100px;
	overflow-Y: auto;
	overflow-X: hidden;
	border: 1px black solid;
}

a.credit {
	FONT-SIZE: 11px; 
	COLOR: RED; 
	FONT-FAMILY: verdana, arial, sans-serif;
}

a.regular {
	FONT-SIZE: 11px; 
	COLOR: GREEN; 
	FONT-FAMILY: verdana, arial, sans-serif;
}

a.OBO {
	FONT-SIZE: 11px; 
	COLOR: #8000FF; 
	FONT-FAMILY: verdana, arial, sans-serif;
}

.strikethruRedTD{
	COLOR : red;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
	text-decoration: line-through
}

.OrangeShadeTD{
	background-color: #FF8040;
	
}

.YellowShadeTD{
	background-color: #ffecb9;
	TEXT-ALIGN: left;
}


.GreyShadeTD{
	background-color: #808080;
	border: solid 1px white;
}

tbody tr.lightBrown td {
	FONT-WEIGHT: bold;   COLOR: #000; CURSOR: arrow; BORDER-TOP-COLOR: #3292fc; BACKGROUND-COLOR: #f1f1f1; BORDER-RIGHT-COLOR: #3292fc
	PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 2px; PADDING-TOP: 2px; BORDER-TOP:  #999999 1px solid; BORDER-BOTTOM: #999999 1px solid; FONT-FAMILY: verdana;
	BORDER-RIGHT: #999999 0px solid; BORDER-LEFT: #999999 0px solid
}

.MSToBeDone{
	background-color: #ffecb9;
}

.MSActual{
	FONT-WEIGHT: bold;
}

.MSPassed{
	FONT-WEIGHT: bold;
	COLOR : red;
}

table.CTFilterHeader {
	font-family: verdana, arial, sans-serif;
	border: 1px solid  #676767;	
	width: 840px;
}

table.CTFilterHeader thead th {
	FONT-SIZE: 11px; 
	FONT-WEIGHT: bold;
	text-align: left;
	margin: 0 0 0 0;
}

table.CTFilterHeader thead tr {
	background-color: #c3c5b8;
	height: 20px;
}

.HideShowLink {
	FONT-SIZE: 9px;
	FONT-WEIGHT: bold;
	font-family: verdana, arial, sans-serif;
	background-color: #FFFFFF;
}

.BoldFontSmall {
	FONT-SIZE: 9px;
	FONT-WEIGHT: bold;
	font-family: verdana, arial, sans-serif;
	background-color: #FFFFFF;
}

.BoldRedFontSmall {
	FONT-SIZE: 9px;
	FONT-WEIGHT: bold;
	font-family: verdana, arial, sans-serif;
	COLOR : red;
}
.RightInputCaption {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
	text-align:right;
}

/*	comments */
.ShadeMedRedGrid{
	background-color: #37cf4f;
	COLOR : White;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: center;
}
/* override */
.ShadeMedYellowGrid{
	background-color: #e16933	;
	COLOR : White;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: center;
}
/* both */
.ShadeDarkBrownGrid{
	background-color: #e787fc;
	COLOR : White;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: center;
}

.spDark {
	FONT-SIZE: 12px; FONT-FAMILY: Helvetica,sans-serif; BACKGROUND-COLOR: #c3c5b8
}
.spMed {
	FONT-SIZE: 12px; FONT-FAMILY: Helvetica,sans-serif; BACKGROUND-COLOR: #dee0d2
}
.spLight {
	FONT-SIZE: 12px; FONT-FAMILY: Helvetica,sans-serif; BACKGROUND-COLOR: #e9ebdb
}
.spWhite {
	FONT-SIZE: 12px; FONT-FAMILY: Helvetica,sans-serif; BACKGROUND-COLOR: #FFFFFF
}
.eeDark {
	FONT-SIZE: 12px; FONT-FAMILY: Helvetica,sans-serif; BACKGROUND-COLOR: #ccdd99
}
.eeMed {
	FONT-SIZE: 12px; FONT-FAMILY: Helvetica,sans-serif; BACKGROUND-COLOR: #e1f3a6
}
.eeLight {
	FONT-SIZE: 12px; FONT-FAMILY: Helvetica,sans-serif; BACKGROUND-COLOR: #ecffaf
}
.erDark {
	FONT-SIZE: 12px; FONT-FAMILY: Helvetica,sans-serif; BACKGROUND-COLOR: #a0b3de
}
.erMed {
	FONT-SIZE: 12px; FONT-FAMILY: Helvetica,sans-serif; BACKGROUND-COLOR: #e4e6f2
}
.erLight {
	FONT-SIZE: 12px; FONT-FAMILY: Helvetica,sans-serif; BACKGROUND-COLOR: #f1f2f7
}
.tsDark {
	FONT-SIZE: 12px; FONT-FAMILY: Helvetica,sans-serif; BACKGROUND-COLOR: #feb469
}
.tsMed {
	FONT-SIZE: 12px; FONT-FAMILY: Helvetica,sans-serif; BACKGROUND-COLOR: #ffecb9
}
.tsLight {
	FONT-SIZE: 12px; FONT-FAMILY: Helvetica,sans-serif; BACKGROUND-COLOR: #fef2cc
}
.hspDark {
	FONT-SIZE: 12px; FONT-FAMILY: Helvetica,sans-serif; BACKGROUND-COLOR: #c3c5b8
}
.heeDark {
	FONT-SIZE: 12px; FONT-FAMILY: Helvetica,sans-serif; BACKGROUND-COLOR: #ccdd99
}
.herDark {
	FONT-SIZE: 12px; FONT-FAMILY: Helvetica,sans-serif; BACKGROUND-COLOR: #a0b3de
}
.htsDark {
	FONT-SIZE: 12px; FONT-FAMILY: Helvetica,sans-serif; BACKGROUND-COLOR: #feb469
}

/*****************************************************************************/
/* Hierarchy Drill Down information 
/*****************************************************************************/
.ShadeLevelI{
	background-color: #e9ebdb;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	font-style: italic;
	height: 20px;
}

.ShadeLevelII{
	background-color: #c3c5b8;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeLevelIII{
	background-color: #eeeeee;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	font-style: italic;
	height: 20px;
}

a,a:visited,a:hover {
	color: black;
	text-decoration: none;
}

a:hover {
	text-decoration: underline;
	border: 1px ;
}

.aaTopHeader {
	FONT-WEIGHT: bold; 
	FONT-SIZE: 9px; 
	COLOR: white; 
	FONT-FAMILY: verdana, Helvetica,sans-serif; 
	BACKGROUND-COLOR: #666666;
}

.aaTopHeaderLightG {
	FONT-WEIGHT: bold; 
	FONT-SIZE: 9px; 
	COLOR: black; 
	FONT-FAMILY: verdana, Helvetica,sans-serif; 
	BACKGROUND-COLOR: #cccccc;
}

.aaTagHeader {
	FONT-WEIGHT: bold; 
	FONT-SIZE: 9px; 
	COLOR: #ffffff; 
	FONT-FAMILY: verdana, Helvetica,sans-serif; 
	BACKGROUND-COLOR: #666666;
	TEXT-ALIGN: center;
	height: 20px;
}
.borderDark {
	FONT-SIZE: 12px; 
	FONT-FAMILY: Helvetica,sans-serif; 
	BACKGROUND-COLOR: #7c816a;
	TEXT-ALIGN: right;
}

.borderLight {
	FONT-SIZE: 12px; 
	FONT-FAMILY: Helvetica,sans-serif; 
	BACKGROUND-COLOR: #cccccc;
	TEXT-ALIGN: right;
}

.ShadeWhite{
	background-color: #FFFFFF;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeDarkGrayTDCenter{
	background-color: #c3c5b8;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: center;
}

.ShadeDarkGrayTD{
	background-color: #c3c5b8;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: left;
}

.ShadeLightGrayTD{
	background-color: #e9ebdb;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeMedGrayTD{
	background-color: #dee0d2;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeDarkBlueTD{
	background-color: #a0b3de;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeLightBlueTD{
	background-color: #f1f2f7;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeMedBlueTD{
	background-color: #e4e6f2;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeMedBlueTDText{
	background-color: #e4e6f2;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: left;
}


.ShadeDarkGreenTD{
	background-color: #ccdd99;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeMedGreenTD{
	background-color: #e1f3a6;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeLightGreenTD {
	background-color: #ecffaf;
	color : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeDarkYellowTD{
	background-color: #ffff66;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeMedYellowTD{
	background-color: #ffff99;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeLightYellowTD {
	background-color: #ffffcc;
	color : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeDarkRedTD{
	background-color: #ff0000;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeMedRedTD{
	background-color: #ef8d7c;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeLightRedTD {
	background-color: #f4afa4;
	color : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeDarkOrangeTD{
	background-color: #feb469;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	font-weight: bold;
	height: 20px;
}

.ShadeMedOrangeTD{
	background-color: #ffecb9;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeMedOrangeTDText{
	background-color: #ffecb9;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeLightOrangeTD{
	background-color: #fef2cc;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeLightOrangeTDText{
	background-color: #fef2cc;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeDarkBrownTD{
	background-color: #BEFFFF;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}
.ShadeLightBrownTD{
	background-color: #E6FFFF;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeMedBrownTD{
	background-color: #D2FFFF;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeRightTableCaptionBlueTD{
	FONT-SIZE: 9px; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
	background-color: #AACCE8;
	TEXT-ALIGN: right;
}

.ShadeBlueTDSmall{
	background-color: #ecf6fe;
	COLOR : Black;
	font-family: sans-serif;
	font-size : 11px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}
 
.CrossRightText {
	FONT-SIZE: 9px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
	TEXT-ALIGN: right;
}

.RightTextRedSmall { 
	FONT-SIZE: 11px; 
	COLOR: #FF0000; 
	FONT-FAMILY:sans-serif;
	text-decoration: None;
	TEXT-ALIGN: right;
}

/*****************************************************************************/
/* Drill Down Mouse Over Style Sheet
/*****************************************************************************/
.ShadeMedGrayTDDrill{
	background-color: #dee0d2;
	COLOR : black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	TEXT-ALIGN: right;
}

A.ShadeMedGrayTDDrill {
	background-color: #dee0d2;
	COLOR : black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	text-decoration: none;
	TEXT-ALIGN: right;
}
A.ShadeMedGrayTDDrill:visited {

}
A.ShadeMedGrayTDDrill:hover {

}

.RightTextRedSalesSmall { 
	COLOR: #FF0000; 
	TEXT-ALIGN: right;
}

.RightTextSmallCT {
	FONT-SIZE: 9px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
	TEXT-ALIGN: right;
}

.RightTextSmallCR {
	FONT-SIZE: 9px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
	TEXT-ALIGN: center;
}


</style>

</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmQueryReportPerPatient.do"  >

<table width="600" border="1">
 <% 
 
 String strReport =	gmCrossTab.PrintCrossTabReport(hmReport);
 strReport = strPatientIDE + strReport;
 
 %>
					<tr> <td>
						<%=strReport %></td>
					</td> </tr>
</table>				    

</html:form>
<% } catch(Exception exp) {
exp.printStackTrace();
}
%>
</BODY>

</HTML>

