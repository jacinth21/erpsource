
<%
	/**********************************************************************************
	 * File		 		: GmPatientAnswerHistory.jsp
	 * Desc		 		: This screen is used for the 
	 * Version	 		: 1.0
	 * author			: Dhinakaran James
	 ************************************************************************************/
%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.ArrayList"%>
<%@ include file="/common/GmHeader.inc"%>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmQueryReport.js"></script>
<bean:define id="hmResult" name="frmQueryForm" property="hmResult" type="java.util.HashMap"></bean:define>
<bean:define id="strListQueries" name="frmQueryForm" property="listQueries" type="java.lang.String"></bean:define>
<bean:define id="fromContainer" name="frmQueryForm" property="fromContainer" type="java.lang.String">
</bean:define>
<%
	HashMap hmQueryHeaderBasic = (HashMap) hmResult.get("HeaderBasic");
	HashMap hmQueryHeaderDetail = (HashMap) hmResult.get("HeaderDetail");
	ArrayList alDescription = (ArrayList) hmResult.get("Description");
	int queryAccess = Integer.parseInt((String)request.getAttribute("queryAccess"));
	String strWikiTitle = GmCommonClass.getWikiTitle("QUERIES_DETAIL");
	
	String strPatientIdeNo = null;
	String strTimePoint = null;
	String strCRF = null;
	String strAE = null;
	String strQueryId = null;
	String strQueryLevelId = null;
	String strStatus = null;
	String strRaisedBy = null;
	String strRaisedDate = null;
	String strClosedBy = null;
	String strClosedDate = null;
	String strFormID=null;
	String strStudyPeriodID=null;
	String strPatientID=null;
	String strStudyPeriodPK=null;

	if (hmQueryHeaderBasic != null) {
		strPatientIdeNo = GmCommonClass.parseNull((String) hmQueryHeaderBasic.get("PATIENT_IDE_NO"));
		strTimePoint = GmCommonClass.parseNull((String) hmQueryHeaderBasic.get("PERIOD"));
		strCRF = GmCommonClass.parseNull((String) hmQueryHeaderBasic.get("CRF"));
		strAE = GmCommonClass.parseNull((String) hmQueryHeaderBasic.get("EVENT"));
		strQueryId = GmCommonClass.parseNull((String) hmQueryHeaderBasic.get("QUERY_ID"));
		strStatus = GmCommonClass.parseNull((String) hmQueryHeaderBasic.get("STATUS"));
		strRaisedBy = GmCommonClass.parseNull((String) hmQueryHeaderBasic.get("RAISED_BY"));
		strRaisedDate = GmCommonClass.parseNull((String) hmQueryHeaderBasic.get("RAISED_DATE"));
		strClosedBy = GmCommonClass.parseNull((String) hmQueryHeaderBasic.get("CLOSED_BY"));
		strClosedDate = GmCommonClass.parseNull((String) hmQueryHeaderBasic.get("CLOSED_DATE"));
		strFormID = GmCommonClass.parseNull((String) hmQueryHeaderBasic.get("FORM_ID"));
		strStudyPeriodID = GmCommonClass.parseNull((String) hmQueryHeaderBasic.get("STUDY_PERIOD_ID"));
		strPatientID = GmCommonClass.parseNull((String) hmQueryHeaderBasic.get("PATIENT_ID"));
		strStudyPeriodPK = GmCommonClass.parseNull((String) hmQueryHeaderBasic.get("STUDY_PERIOD_PK"));
		
			
	}

	if (hmQueryHeaderDetail != null) {
		strQueryId = GmCommonClass.parseNull((String) hmQueryHeaderDetail.get("QUERY_ID"));
		strQueryLevelId = GmCommonClass.parseNull((String) hmQueryHeaderDetail.get("QUERY_LEVEL_ID"));
		strStatus = GmCommonClass.parseNull((String) hmQueryHeaderDetail.get("STATUS"));
		strRaisedBy = GmCommonClass.parseNull((String) hmQueryHeaderDetail.get("RAISED_BY"));
		strRaisedDate = GmCommonClass.parseNull((String) hmQueryHeaderDetail.get("RAISED_DATE"));
		strClosedBy = GmCommonClass.parseNull((String) hmQueryHeaderDetail.get("CLOSED_BY"));
		strClosedDate = GmCommonClass.parseNull((String) hmQueryHeaderDetail.get("CLOSED_DATE"));
	}
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Clinical Site Map</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmQueryReport.js"></script>
<script>
	queryStatus = '<%=strStatus%>';	
	var fromContainer='<%=fromContainer%>';
	
function fnClose()
{
window.opener.fnReload();
window.close();
}


</script>
<style type="text/css" media="all">
@import url("styles/screen.css");
</style>
</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="fnReloadLeftFrame();">
<html:form action="/gmQuery.do">
	<html:hidden property="strOpt" />
	<html:hidden property="queryId" />
	<html:hidden property="patientListId" />
	<html:hidden property="patientAnsListId" />
	<html:hidden property="queryLevelId" value="<%=strQueryLevelId%>" />
	<html:hidden property="dataSaved" />
	<html:hidden property="fromContainer" />
	
	<html:hidden property="hTxnId" value="" />
	<html:hidden property="hTxnName" value="" />
	<html:hidden property="hCancelType" value="VQUERY" />
	
	<html:hidden property="hFormID" value="<%=strFormID%>" />
	<html:hidden property="hStudyPeriodID" value="<%=strStudyPeriodID%>" />
	<html:hidden property="hPatientID" value="<%=strPatientID%>" />
	<html:hidden property="hStudyPeriodPK" value="<%=strStudyPeriodPK%>" />

<input type="hidden" name="listQueries" value="<%=strListQueries%>" />
<input type="hidden" name="queryAccess" value="<%=queryAccess%>" />

	<table style="margin: 0 0 0 0; width: 100%; border: 1px solid  #c2d5dc;" cellspacing="0" cellpadding="0">
		<tr>
			<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr class=RightDashBoardHeader>
					<th colspan=4 class="RightTableCaption" align="left"  Height=25>		
						<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0">
				      		<tr> 
							      <th class="Header" align="left"  Height=25>&nbsp;&nbsp;Query Details</th>			 				         	
			 	            </tr>
						</table>						
					</th>
				</tr>
				<tr>
					<td height="1" colspan="4" class="LLine"></td>
				</tr>
				<tr class="evenshade">
					<td class="RightTableCaption" align="right"  Height=25>Patient IDE # :&nbsp;</td>
					<td width="25%"><%=strPatientIdeNo%></td>
					<td class="RightTableCaption" align="right"  Height=25>Time Point :&nbsp;</td>
					<td width="25%"><%=strTimePoint%></td>
				</tr>
				<tr ><td colspan="4" class="LLine" height="1"></td></tr>
				<tr class="oddshade">
					<td class="RightTableCaption" align="right"  Height=25>CRF # :&nbsp;</td>
					<td><%=strCRF%></td>
					<td class="RightTableCaption" align="right"  Height=25>AE :&nbsp;</td>
					<td><%=strAE%></td>
				</tr>
				<tr><td colspan="4" class="LLine" height="1"></td></tr>
				<tr class="evenshade">
					<td class="RightTableCaption" align="right"  Height=25>Query # :&nbsp;</td>
					<td><%=strQueryId%></td>
					<td class="RightTableCaption" align="right"  Height=25>Status :&nbsp;</td>
					<td><%=strStatus%></td>
				</tr>
				<tr ><td colspan="4" class="LLine" height="1"></td></tr>
				<tr class="oddshade">
					<td class="RightTableCaption" align="right"  Height=25>Raised By :&nbsp;</td>
					<td><%=strRaisedBy%></td>
					<td class="RightTableCaption" align="right"  Height=25>Raised Date :&nbsp;</td>
					<td><%=strRaisedDate%></td>
				</tr>
				<tr><td colspan="4" class="LLine" height="1"></td></tr>
				<tr class="evenshade">
					<td class="RightTableCaption" align="right"  Height=25>Closed By :&nbsp;</td>
					<td><%=strClosedBy%></td>
					<td class="RightTableCaption" align="right"  Height=25>Closed Date :&nbsp;</td>
					<td><%=strClosedDate%></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td bgcolor="#666666" colspan="4"></td>
				</tr>
				<tr class=ShadeRightTableCaption>
					<td colspan=4 class="RightTableCaption" align="left" width="150" Height=25>&nbsp;&nbsp;New Text :</td>
				</tr>
				<tr>
					<td height="1" colspan="4" bgcolor="#666666"></td>
				</tr>
				<tr>
					<td colspan="4" height="50">&nbsp;<textarea name="comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" rows=3 cols=65></textarea>
					</td>
				</tr>
				<tr>
					<%if(queryAccess>0&& 
							   (strStatus.equals("") || strStatus.equals("Answered"))
					   ){%>
					   <!-- Custom tag lib code modified for JBOSS migration changes -->
					<td class="RightTableCaption" align="left" Height="25">&nbsp;&nbsp;Status :&nbsp; 
						<gmjsp:dropdown controlName="status" SFFormName="frmQueryForm"
							SFSeletedValue="status" SFValue="alStatus" codeId="CODEID" codeName="CODENM" />
					</td>
					<%} if(queryAccess==0) {%>
					<td class="RightTableCaption" align="left" Height="25">&nbsp;&nbsp;Status :&nbsp; Answered
						<html:hidden property="status" value="92442" />
					</td>
					<%} %>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="4" align="center" height="24">
							<%if (queryAccess > 0){ %>
							<gmjsp:button name="formBtn" value="Go To Form" gmClass="button" buttonType="Load"
								onClick="fnLoadFormData();" />&nbsp;&nbsp;
							<% }
							// Scenario for CRA
							//Fix for 9511
							if ((!strStatus.equals("Closed")) && 
							   (queryAccess > 0 && 
							   (strStatus.equals("") || strStatus.equals("Answered"))
							   )||
							   (queryAccess == 0 && (!strStatus.equals("Answered")))
							   ){ %>
							
							<gmjsp:button name="submitBtn" value="Submit" gmClass="button" buttonType="Save"
								onClick="fnQueryDetailsSubmit();" />&nbsp;&nbsp;
							<% }							
							if(queryAccess>0 && strStatus.equals("New")){ %> 
								<gmjsp:button name="voidBtn" value="&nbsp;&nbsp;Void&nbsp;&nbsp;"
									gmClass="button" onClick="fnVoidQuery();" /> 
							<%}%>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td>
			<div style="overflow: auto; width: 100%; height: 126px;">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td bgcolor="#666666" colspan="4"></td>
				</tr>
				<tr class=ShadeRightTableCaption>
					<th colspan=4 class="RightTableCaption" align="left" Height=25>&nbsp;&nbsp;Description :</th>
				</tr>
				<%
					if (alDescription != null) {
						for (int i = 0; i < alDescription.size(); i++) {
							HashMap tempData = (HashMap) alDescription.get(i);
				%>
				<tr class="shade">
					<td colspan="4" height="25"><strong>&nbsp;<%=(String) tempData.get("HEADER")%></strong></td>
				</tr>
				<tr>
					<td colspan="4" height="25">&nbsp;<%=(String) tempData.get("TEXT")%></td>
				</tr>

				<%
					}
					} else {
				%>
				<tr>
					<td bgcolor="#666666" colspan="4"></td>
				</tr>
				<tr>
					<td colspan="4" height="25">&nbsp;No Data!!!</td>
				</tr>
				<%
					}
				%>
			</table>
		</div>
			</td>
		</tr>		
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
