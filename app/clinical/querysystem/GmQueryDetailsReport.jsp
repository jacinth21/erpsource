<%
/**********************************************************************************
 * File		 		: GmQueryReportByTP.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Vamsi Kiran Nagavarapu
************************************************************************************/
%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %> 
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ taglib uri="/WEB-INF/gmjsp-taglib.tld" prefix="gmjsp"%>
<%@ page import="com.globus.common.beans.GmCommonConstants"%> 
<%@ page import="com.globus.common.beans.GmCommonClass"%> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmGridFormat"%>
<%@ page import ="com.globus.clinical.querysystem.forms.GmQueryReportForm"%>
<%@ page import="com.globus.common.beans.GmLogger"%> 
<%@ page import="org.apache.log4j.Logger"%> 

<bean:define id="hmReport" name="frmQueryReportForm" property="hmCrossTabReport" type="java.util.HashMap"></bean:define>

<bean:define id="strReportId" name="frmQueryReportForm" property="strreportid" type="java.lang.String"></bean:define>
<bean:define id="strSiteId" name="frmQueryReportForm" property="strsite_id" type="java.lang.String"></bean:define>

<% Logger log = GmLogger.getInstance(GmCommonConstants.CLINICAL);  // Instantiating the Logger 
try{
	GmGridFormat gmCrossTab = new GmGridFormat();
	String strExcelQueryString = "";
	
	String strSessStudyId = "";
	String strSessSiteId = "";
	String strSessPatientId = "";


	strSessStudyId = GmCommonClass.parseNull((String)session.getAttribute("strSessStudyId"));
	strSessSiteId = GmCommonClass.parseNull((String)session.getAttribute("strSessSiteId"));
	strSessPatientId = GmCommonClass.parseNull((String)session.getAttribute("strSessPatientId"));
	
	//Neccessary Path's
	String strWikiPath = GmCommonClass.getString("GWIKI");
	String strWikiTitle = GmCommonClass.getWikiTitle("QUERY_PER_PATIENT");
	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	String strCommonPath = GmCommonClass.getString("GMCOMMON");
	
	//Set Value Type	
	gmCrossTab.setValueType(0);
	
	
	
	// Setting Display Parameter
	gmCrossTab.setColumnWidth("Name",70);	
	gmCrossTab.setColumnWidth("Total # of Queries Outstanding",88);	
	gmCrossTab.setColumnWidth("Average # of Days Outstanding",90);	
	gmCrossTab.setAmountWidth(42);
	gmCrossTab.setRoundOffAllColumnsPrecision("0");
	gmCrossTab.setRowHeight(20);
	
	//No Total Field Required
	gmCrossTab.setHideColumn("Total");
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setTotalRequired(false);
	
	//gmCrossTab.add_RoundOff("Average # of Days Outstanding","2");
	
	// Page Body Style
	gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD");
	gmCrossTab.addStyle("Name","ShadeMedGrayTD","ShadeDarkGrayTD");
	gmCrossTab.addStyle("Total","ShadeMedBlueTD","ShadeDarkBlueTD") ;
	
	//Set the Report Type
	//if ( strReportId == "BYSTUDY"){
	//	gmCrossTab.reNameColumn("Name","Study");
	//	strExcelQueryString = "/gmQueryReportPerPatient.do?method=fetchQueryByStudy&strExcel=Excel";		
	//	strWikiTitle = GmCommonClass.getWikiTitle("QUERY_BY_STUDY");
	//}
	//else if (strReportId == "BYPATIENT"){
	//	gmCrossTab.reNameColumn("Name","Patient");
	//	strExcelQueryString = "/gmQueryReportPerPatient.do?method=fetchQueryByPatient&strExcel=Excel";		
	//	strWikiTitle = GmCommonClass.getWikiTitle("QUERY_BY_PATIENT");
	//}
	//else if (strReportId == "BYSITE"){
	//	gmCrossTab.reNameColumn("Name","Site");
	//	strExcelQueryString = "/gmQueryReportPerPatient.do?method=fetchQueryBySite&strExcel=Excel";		
	//	strWikiTitle = GmCommonClass.getWikiTitle("QUERY_BY_SITE");
	//}
	
	//Decorator
	gmCrossTab.setDecorator("com.globus.crosstab.beans.GmQueryReportDecorator");
	//gmCrossTab.setColumnWidth();
	String strGridData = gmCrossTab.generateGridXML(hmReport);
	log.debug("XML STRING is "+strGridData);
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Query By Timeout </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="javascript/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script>
	
	var sessStudyId = '<%=strSessStudyId%>';
	var sessSiteId = '<%=strSessSiteId%>';
	var sessPatientId = '<%=strSessPatientId%>';
	
	var objGridData = '<%=strGridData%>';
	
	
	function fnOnPageLoad(){	
		gridObj = initGrid('dataGridDiv',objGridData);
	
	}


	
function fnStudy(id, name)
		{ 
		 //  alert(id);
		   id = id.replace("#","");
		   id = id.replace("#","");
		   
		   name = name.replace("#","");
		   name = name.replace("#","");
		   
			report_id = "<%=strReportId%>";
			//alert(report_id);

 			if(report_id == "BYSTUDY"){
 				document.frmQueryReportForm.action = "/gmQueryReportPerPatient.do?method=fetchQueryBySite";
 				document.frmQueryReportForm.submit();
 			}
 			if(report_id == "BYSITE"){
 				document.frmQueryReportForm.action = "/gmQueryReportPerPatient.do?method=fetchQueryByPatient&strsite_id=" + id;
 				document.frmQueryReportForm.submit();
 			}
 			if(report_id == "BYPATIENT"){
 				document.frmQueryReportForm.action = "/gmQueryReportPerPatient.do?method=fetchQueryPerPatient&strsite_id=<%=strSiteId%>&patientId="+id+"&patientIDE="+name;
 				document.frmQueryReportForm.submit();
 			}
 		}

function fnQueryBySite()
{
	if(sessPatientId == '')
	{
		history.go(-1);		
	}
	else
	{
		document.frmQueryReportForm.action = "/menu/GmClinicalLayout.jsp?strPgToLoad=/gmQueryReportPerPatient.do?method=fetchQueryBySite&strAccrd=reports&&strDhtmlx=true&strStudyId="+sessStudyId+"&strSiteId="+sessSiteId;
		document.frmQueryReportForm.submit();
 	}
}

function fnQueryByStudy()
{
	if(sessSiteId == '')
	{
		history.go(-1);		
	}
	else
	{
		document.frmQueryReportForm.action = "/menu/GmClinicalLayout.jsp?strPgToLoad=/gmQueryReportPerPatient.do?method=fetchQueryByStudy&strAccrd=reports&&strDhtmlx=true&strStudyId="+sessStudyId;
		document.frmQueryReportForm.submit();
 	}
}
 		
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();"/>
<html:form action="/gmQueryReportPerPatient.do"  >
		     	
<table border="0" class="DtTable765"  cellspacing="0" cellpadding="0">
 	 <tr>
		<%if ( strReportId == "BYSTUDY"){ %>
			<td height="25" class="Header"> Query By Study</td>
			<td height="25" class="Header" align="Right"></td>	
		<%} %>
		<%if ( strReportId == "BYSITE"){ %>
			<td height="25" class="Header"> Query By Site</td>
			<td height="25" class="Header" align="Right"><a href="javascript:fnQueryByStudy();">&nbsp;Back&nbsp;</a></td>	
		<%} %>
		<%if (strReportId == "BYPATIENT"){ %>
			<td height="25" class="Header"> Query By Patient</td>
			<td height="25" class="Header" align="Right"><a href="javascript:fnQueryBySite();">&nbsp;Back&nbsp;</a></td>	
		<%} %>
							
	</tr>
	<tr>
		<td colspan="3">
			<div id="dataGridDiv" class="grid" style="height:380px"></div>			
		</td>
	</tr>	
	<tr class="oddshade">
		<td colspan="2" align="center" height="30">	
					<div class='exportlinks'>Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a>
                                <!-- | <img src='images/pdf_icon.gif' /> <a href="#" onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a> -->
                    </div>
		</td>
		
	</tr>
</table>
</html:form>

<% } catch(Exception exp) {
exp.printStackTrace();
}
%>
</BODY>
</HTML>

