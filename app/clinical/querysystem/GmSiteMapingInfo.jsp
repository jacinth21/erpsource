<%
/**********************************************************************************
 * File		 		: GmGlobusAssociationSetup.jsp
 * Desc		 		: JSP for setting up Globus Association for party
 * Version	 		: 1.0
 * author			: Satyajit Thadeshwar
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script>
function fnLoad()
{
	document.frmSiteMapingForm.strOpt.value = "";
	document.frmSiteMapingForm.action = "/gmSiteMap.do?method=loadSiteMapingSetup";
	document.frmSiteMapingForm.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="/styles/GmClinical.css">
</head>
<bean:define id="partyType" name="frmSiteMapingForm" property="partyType" scope="request" type="java.lang.String" ></bean:define>
<%
partyType = partyType == null ? "" : partyType;

String strOddShade = "";
String strEvenShade = "";

  if (partyType.equals("7007"))
  {
	  strOddShade = "oddshade";
	  strEvenShade ="evenshade";
  }
  else
  {
	  strOddShade = "Shade";
	  strEvenShade ="";
  }
%>

<body leftmargin="20" topmargin="10">
<html:form action="/gmSiteMap.do">
<html:hidden property="strOpt" value=""/>
<html:hidden property="partyId" name="frmSiteMapingForm"/>
<html:hidden property="sitePartyId" name="frmSiteMapingForm"/>
<html:hidden property="partyType" name="frmSiteMapingForm"/>
<html:hidden property="includedPage" name="frmSiteMapingForm"/>
<html:hidden property="partyName" name="frmSiteMapingForm"/>

<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VDSCSM"/>

<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="2" height="15"></td>
	</tr>
	 <tr>
		 <td colspan="2" width="100%" height="100" valign="top">
		 	<table border="0" width="100%" cellspacing="0" cellpadding="0"> 
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<!-- Custom tag lib code modified for JBOSS migration changes -->
				<tr class="<%=strOddShade%>">
					<td class="RightTableCaption" align="right" height="24" width="40%"><font color="red">*</font>&nbsp;Study List&nbsp;:</td>
					<td width="60%">&nbsp; <gmjsp:dropdown controlName="studyListId" SFFormName="frmSiteMapingForm" SFSeletedValue="studyListId"
								SFValue="alStudyList" codeId = "CODENMALT"  codeName = "CODENM" defaultValue= "[Choose One]" onChange="fnGlobusLoadSite();" />													
					</td>
				</tr>
				<tr >
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
				</tr >
				<tr class="<%=strEvenShade%>">
					<td class="RightTableCaption" align="right" height="24" width="40%"><font color="red">*</font>&nbsp;Site List&nbsp;:</td>
					<td width="60%">&nbsp; <gmjsp:dropdown controlName="siteListId" SFFormName="frmSiteMapingForm" SFSeletedValue="siteListId"
								SFValue="alSiteNameList" codeId = "ACCOUNT_ID"  codeName = "NAME" defaultValue= "[Choose One]" />													
					</td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
                   	<tr class="<%=strOddShade%>">
                       	<td class="RightTableCaption" align="right" HEIGHT="24">Primary Coordinator&nbsp;:</td> 
                       	<td>&nbsp;<html:checkbox disabled="false" property="primaryFlag" /></td>
                   	</tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
                    <tr><td colspan="2" height="15"></td></tr>
                    <td colspan="2" >
                    	<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="FORMNAME" value="frmSiteMapingForm" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="Edit" />
						</jsp:include>
					</td>
                    </tr>                    
                    <tr><td colspan="2" height="15"></td></tr>
                    <tr>                        
                        <td colspan="2" align="center" height="24">
							<gmjsp:button name="submitBtn" value="Submit" gmClass="button" buttonType="Save" onClick="fnSiteMappingSubmit();"/>&nbsp;&nbsp;
							<gmjsp:button name="resetBtn" value="&nbsp;Reset&nbsp;" gmClass="button" buttonType="Save" onClick="fnSiteMappingReset();"/>&nbsp;&nbsp;
							<gmjsp:button name="voidBtn" value="&nbsp;&nbsp;Void&nbsp;&nbsp;" gmClass="button" buttonType="Save" onClick="fnSiteMappingVoid();"/>
                        </td>
                    </tr>
                    <tr><td colspan="2" height="15"></td></tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
    				<tr>
    					<td colspan="2">
    						<jsp:include page="/gmSiteMap.do?method=loadSiteMappingSetupReport" flush="true">
    							<jsp:param name="TYPE" value="Setup" />
    						</jsp:include>
    					</td>
    				</tr>  
                    
			</table>
		</td>
	</tr>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>