<%
/**********************************************************************************
 * File		 		: GmQueryperPatient.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Vamsi Kiran Nagavarapu
************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc" %>
<%@ page import="com.globus.common.beans.GmLogger"%> 
<%@ page import="org.apache.log4j.Logger"%> 
<%@ page import="com.globus.common.beans.GmCommonConstants"%> 
<%@ page import="com.globus.common.beans.GmCommonClass"%> 

<bean:define id="gridData" name="frmQueryReportForm" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="strSiteId" name="frmQueryReportForm" property="strsite_id" type="java.lang.String"> </bean:define>
<bean:define id="strPatientId" name="frmQueryReportForm" property="patientId" type="java.lang.String"> </bean:define>
<bean:define id="strPatientIde" name="frmQueryReportForm" property="patientIDE" type="java.lang.String"> </bean:define>

<%
	
	strWikiPath = GmCommonClass.getString("GWIKI");
	String strWikiTitle = GmCommonClass.getWikiTitle("QUERY_PER_PATIENT");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Query per Patient </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

<script>

function fnQueryDetailsByQueryId (patientListId, patientAnsListId, queryLevelId, queryId)
{
patientListId = patientListId.replace("#","");
patientListId = patientListId.replace("#","");

patientAnsListId = patientAnsListId.replace("#","");
patientAnsListId = patientAnsListId.replace("#","");

queryLevelId = queryLevelId.replace("#","");
queryLevelId = queryLevelId.replace("#","");

queryId = queryId.replace("#","");
queryId = queryId.replace("#","");

//alert(' patientListId ' + patientListId + ' patientAnsListId ' + patientAnsListId + ' queryLevelId ' + queryLevelId + ' queryId ' + queryId);
	//<cell><![CDATA[<a href="#" onclick=fnQueryDetailsByQueryId(&#39#$esc.xml($hmDetails.PATIENT_LIST_ID)#&#39,&#39#$esc.xml($hmDetails.PATIENT_ANS_LIST_ID)#&#39,&#39#$esc.xml($hmDetails.QUERY_LEVEL)#&#39,&#39#$esc.xml($hmDetails.QUERY_ID)#&#39)>$esc.xml($hmDetails.QUERY_ID)</a>]]></cell> 	
	windowOpener("/gmQuery.do?method=fetchQueryDetails&queryLevelId="+queryLevelId+"&patientAnsListId="+patientAnsListId+"&patientListId="+patientListId+"&queryId="+queryId,"","resizable=yes,scrollbars=yes,top=150,left=100,width=870,height=600");
}


function fnQueryDetails (patientListId, patientAnsListId, queryLevelId)
{
patientListId = patientListId.replace("#","");
patientListId = patientListId.replace("#","");

patientAnsListId = patientAnsListId.replace("#","");
patientAnsListId = patientAnsListId.replace("#","");

queryLevelId = queryLevelId.replace("#","");
queryLevelId = queryLevelId.replace("#","");

windowOpener("/gmQuery.do?method=fetchQueryContainer&queryLevelId="+queryLevelId+"&patientAnsListId="+patientAnsListId+"&patientListId="+patientListId,"","resizable=yes,scrollbars=yes,top=150,left=100,width=350,height=600");

//alert(' patientListId ' + patientListId + ' patientAnsListId ' + patientAnsListId + ' queryLevelId ' + queryLevelId );
	//<cell><![CDATA[<a href="#" onclick=fnQueryDetails(&#39#$esc.xml($hmDetails.PATIENT_LIST_ID)#&#39,&#39#$esc.xml($hmDetails.PATIENT_ANS_LIST_ID)#&#39,&#39#$esc.xml($hmDetails.QUERY_LEVEL)#&#39)>$esc.xml($hmDetails.QUERY_ID)</a>]]></cell> 		
}

	var objGridData = '<%=gridData%>';
	
	function fnOnPageLoad(){
		gridObj = initGrid('dataGridDiv',objGridData);
	}
	
	function fnTimePoint(id){	
	   id = id.replace("#","");
		   id = id.replace("#","");
		   
 		document.frmQueryReportForm.action = "/gmQueryReportPerPatient.do?method=fetchQueryByTP&strsite_id=<%=strSiteId%>&patientId=<%=strPatientId%>&patientIDE="+id;
 		document.frmQueryReportForm.submit();
 	}
 	
//	function fnPrintVer()
//	{
//		windowOpener("/gmQueryReportPerPatient.do?method=fetchQueryPerPatient&&patientId=<%=strPatientId%>&strExcel=Excel","Excel","menubar=yes,resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
//	}
</script>
</HEAD>
<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();"/>
<html:form action="/gmQueryReportPerPatient.do">
	<table border="0" class="DtTable765"  cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">Query Per Patient</td>
			<td height="25" class="Header" align="Right"><a href="javascript:history.go(-1);">&nbsp;Back&nbsp;</a></td>
		</tr>
		<tr><td colspan="3" class="LLine" height="1"></td></tr> 
		<tr><td colspan="3" height="30"> <b>Patient IDE : </b> <a href="#" onclick ="javascript:fnTimePoint('<%=strPatientIde%>');"><%=strPatientIde%></a> </td></tr>
		<tr><td colspan="3" class="LLine" height="1"></td></tr> 	
		<tr>
			<td colspan="3">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">                    
      				<tr><td colspan="2" bgcolor="#cccccc"></td></tr> 
      				<tr> 
			            <td colspan="3">
			           		<div id="dataGridDiv" class="grid" style="height:340px"></div>
						</td>
					</tr>
					<tr class = "shade">
						<td colspan="3" align="center" height="30">	
							<div class='exportlinks'>Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a>
                                <!--  | <img src='images/pdf_icon.gif' /> <a href="#"    onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a> -->
                            </div>

							
						</td>
					</tr>						
    			 </table>	 	   
    		 </td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

