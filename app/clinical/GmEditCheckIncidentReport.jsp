 <%
/**********************************************************************************
 * File		 		: GmCommonCurrConv.jsp
 * Desc		 		: This screen displys currency conversion
 * Version	 		: 1.0
 * author			: Himanshu Patel
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<bean:define id="strFormDataScr" name="frmStudyFilterForm" property="formDataScr" type="java.lang.String"> </bean:define>
<bean:define id="gridData" name="frmStudyFilterForm" property="gridXmlData" type="java.lang.String"> </bean:define>
 	
		 
		
<%
	String strStyle ="";
	boolean bFormScr = false; 
	strFormDataScr =  GmCommonClass.parseNull(strFormDataScr);
	if(strFormDataScr.equals("")) {
			strStyle =  "height:500px;width:900px;";
			
		}
	else
	{
		bFormScr = true;
		strStyle =  "height:200px;width:750px;";
	}
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strWikiTitle = GmCommonClass.getWikiTitle("COMMON_CURRENCY_CONVERSION_SETUP");
	String strSessPatientId = (String)session.getAttribute("strSessPatientId");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Enterprise Portal</TITLE>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="/styles/GmClinical.css">
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmEditCheckIncidentReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script>
	objGridData = '<%=gridData%>';
	
</script>
</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();">
<html:form action="/gmEditCheckReportAction.do">
	<html:hidden property="strOpt" />
	<html:hidden property="haction" value=""/> 
	<html:hidden property="hTxnId" value="" />
	<html:hidden property="hTxnName" value="" />
	<html:hidden property="hCancelType" value="" />
	<html:hidden property="studyListId" />
	<html:hidden property="siteList" />

	<table border="0" class="DtTable950" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header" colspan="6">&nbsp;Edit Check Incident Report</td>
		</tr>
		<% 
				if(!bFormScr) { 
		%> 
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr class="evenshade" height="25">
			<%
			if(strSessPatientId.equals(""))
			 {
			%>	
			<td class="RightTableCaption" align="right" >&nbsp;Patient:</td>
			<td >&nbsp;
				<gmjsp:dropdown controlName="patientId"  SFFormName="frmStudyFilterForm" SFSeletedValue="patientId"
				SFValue="alPatientList" codeId="PATID" codeName="PATIDE" defaultValue="[Choose One]"/>                 		                                        		
            </td>
            <%} %>
            <td class="RightTableCaption" align="right">&nbsp;Time Point:</td>
			<td >&nbsp;
				<gmjsp:dropdown controlName="periodId"  SFFormName="frmStudyFilterForm" SFSeletedValue="periodId"
				SFValue="alPeriodList" codeId="PERIODID" codeName="PERIODDS" onChange="fnReload();" defaultValue="[Choose One]"/>
			</td>
			<td class="RightTableCaption" align="right" >&nbsp;Form:</td>
			<td >&nbsp;
				<gmjsp:dropdown controlName="formId"  SFFormName="frmStudyFilterForm" SFSeletedValue="formId"
				SFValue="alStudyFormList" codeId="FORMID" codeName="FORMNM" defaultValue="[Choose One]"/>                 		                                        		
            </td>
            <%
			if(!strSessPatientId.equals(""))
			 {
			%>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
			<%} %>
		</tr>
		<tr><td colspan="6" class="LLine" height="1"></td></tr>
		<tr height="25" class="oddshade">
            <td class="RightTableCaption" align="right">&nbsp;Last Updated By:</td>
			<td >&nbsp;
				<gmjsp:dropdown controlName="craId"  SFFormName="frmStudyFilterForm" SFSeletedValue="craId"
				SFValue="alCraNames" codeId="ID" codeName="NAME" defaultValue="[Choose One]"/>
			</td>
			<td class="RightTableCaption" align="right">&nbsp;Reference Id:</td>
			<td>&nbsp;&nbsp;<html:text property="referenceId" /></td>
            <td class="RightTableCaption" align="right" >&nbsp;Status:</td>
			<td >&nbsp;
				<gmjsp:dropdown controlName="status"  SFFormName="frmStudyFilterForm" SFSeletedValue="status"
				SFValue="alStatus" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>                 		                                        		
            </td>
		</tr>
        <tr><td colspan="6" class="LLine" height="1"></td></tr>
		<tr height="25" class="evenshade">
		    <td class="RightTableCaption" align="right" >&nbsp;Reason For Closing:</td>
			<td >&nbsp;
				<gmjsp:dropdown controlName="reasonForClosing"  SFFormName="frmStudyFilterForm" SFSeletedValue="reasonForClosing"
				SFValue="alReasonForClosing" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>                 		                                        		
            </td>
			<td class="RightTableCaption" align="right" width=200px>&nbsp;Created Date :</td>
			<td colspan="3" width=300px>&nbsp;  
			<gmjsp:calendar SFFormName="frmStudyFilterForm" controlName="fromDate" toControlName ="toDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" /> 
			</td>		
			
            
		</tr>
        <tr><td colspan="6" class="LLine" height="1"></td></tr>
	    <tr height="30">
	           	<td colspan="6" align="center" height="30">
	                <gmjsp:button value="&nbsp;&nbsp;Load&nbsp;&nbsp;" gmClass="Button" buttonType="Load" onClick="fnLoadReport();" /> 
	            </td>
	    </tr>
	    <%
				}
	    %>
	    <tr><td colspan="6" class="LLine" height="1"></td></tr>
	    <tr><td colspan="6">
		    <table border="0" cellspacing="0" cellpadding="0">
			<tr> 
	        	<td colspan="6">
	          		<div id="dataGridDiv" class="grid" style="<%=strStyle%>"></div>
				</td>
			</tr> 	   
	    	</table>		
    	</td></tr>
    	
	<tr class = "oddshade">
						<td colspan="6" class = "oddshade" colspan="3" align="center" height="30">	
							<div >Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!-- | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a> -->
                            </div>

							
						</td>
					</tr>	
    	   	
    </table>
    
      <% 
				if(bFormScr) { 
		%>
		<table align="center">
			<tr>
				<td>
					 <gmjsp:button value="&nbsp;&nbsp;Close&nbsp;&nbsp;" gmClass="Button" buttonType="Load" onClick="fnClose();" />
				</td>
			</tr>
		</table>
		
		<%
				}
	    %>		
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
