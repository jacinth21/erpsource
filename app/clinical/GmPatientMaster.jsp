 <%
/**********************************************************************************
 * File		 		: GmPatientMaster.jsp
 * Desc		 		: This screen is used for Clinical Study to store patient master
 *					  Information.
 * Version	 		: 1.0
 * author			: Richardk
************************************************************************************/
%>


<!-- clinical\clinical\GmPatientMaster.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="java.util.Date"%>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="org.apache.commons.beanutils.DynaBean"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strAction = (String)request.getAttribute("hAction");
	if (strAction == null)
	{
		strAction = (String)session.getAttribute("hAction");
	}
	strAction = (strAction == null)?"Add":strAction;

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmLoad = new HashMap();

	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strButtonDisabled = "";
	strButtonDisabled = GmCommonClass.parseNull((String)request.getAttribute("strButtonDisabled"));
	String strMessage = GmCommonClass.parseNull((String)request.getAttribute("MSG"));
	
	ArrayList alSurgeryTypeList = new ArrayList();
	ArrayList alAccountList = new ArrayList();
	ArrayList alSurgeonName = new ArrayList();
	//ArrayList alStudyList = new ArrayList();
	ArrayList alPatientList = new ArrayList();

	HashMap hmPatientInfo = new HashMap();
	
	String strRepId = (String)request.getAttribute("hRepId") == null?"":(String)request.getAttribute("hRepId");
	String strRepNm = "";
	String strDistId = "";
	String strCatId = "";
	String strAddress1 = "";
	String strAddress2 = "";
	String strCity = "";
	String strState = "";
	String strCountry = "";
	String strPinCode = "";
	String strPhone = "";
	String strPager = "";
	String strEmail = "";
	String strGridData="";
	HashMap hmSalesRep = new HashMap();

	String strStudyId 		= (String)request.getAttribute("hStudyID") == null?"":(String)request.getAttribute("hStudyID");
	String strPatientID 	= (String)request.getAttribute("hPatientID") == null?"":(String)request.getAttribute("hPatientID");
	
	String strRPatientID 	= ""; //Retrieved Patiend ID
	String strSurgeryType 	= "";
	String strSurgeryDate 	= "";
	String strAccountID 	= (String)request.getAttribute("hSiteID") == null?"":(String)request.getAttribute("hSiteID");;
	String strSurgeonID 	= "";
	String strPatientPKey 	= "";
	String strSurgeryCompleted = "";
	String strPrincipalInv = "";
	Date dtSurgeryDate = null;
	// To load the combo box value
	if (hmReturn != null)
	{
		hmLoad = (HashMap)hmReturn.get("COMBOVALUE");
		alSurgeryTypeList 	= (ArrayList)hmLoad.get("SURGERYTYPELIST");
		alAccountList 		= (ArrayList)hmLoad.get("ACCOUNTLIST");
		alSurgeonName 		= (ArrayList)hmLoad.get("SURGEONNAME");
		//alStudyList 		= (ArrayList)hmLoad.get("STUDYLIST");;
		alPatientList 		= (ArrayList)hmLoad.get("PATIENTLIST");;
	}

		hmPatientInfo = (HashMap)hmReturn.get("EDITLOAD");
		if(hmPatientInfo != null)
		{	
			strRPatientID 	= GmCommonClass.parseNull((String)hmPatientInfo.get("PID")) ;
			strSurgeryType 	= GmCommonClass.parseNull((String)hmPatientInfo.get("S_TYPE"));
			strSurgeryDate 	= GmCommonClass.parseNull((String)hmPatientInfo.get("S_DATE"));
			strAccountID 	= GmCommonClass.parseNull((String)hmPatientInfo.get("A_ID"));
			strSurgeonID 	= GmCommonClass.parseNull((String)hmPatientInfo.get("S_ID"));
			strPatientPKey	= GmCommonClass.parseNull((String)hmPatientInfo.get("PKEY_ID"));
			strPrincipalInv	= GmCommonClass.parseNull((String)hmPatientInfo.get("PINV"));
			strSurgeryCompleted	= GmCommonClass.parseNull((String)hmPatientInfo.get("CFLAG"));
			strSurgeryCompleted = strSurgeryCompleted.equals("Y") ? "checked" : "";	
		}
	

     hmPatientInfo = (HashMap)hmReturn.get("RELOAD");
		
		if (hmPatientInfo != null )
		{	
			strRPatientID 	= GmCommonClass.parseNull((String)hmPatientInfo.get("PID")) ;	
			strPrincipalInv	= GmCommonClass.parseNull((String)hmPatientInfo.get("PINV"));
			strRPatientID	= GmCommonClass.parseNull((String)hmPatientInfo.get("PDEFAULT")) + strRPatientID ;
			//strAction = "Edit";
			
        }
   
     
	        strRPatientID 	= strRPatientID.equals("") ? GmCommonClass.parseNull((String)request.getParameter("Txt_RPatientIDE")) : strRPatientID;
			strSurgeryType 	= strSurgeryType.equals("") ? GmCommonClass.parseNull((String)request.getParameter("Cbo_SurgType")) : strSurgeryType;
			strSurgeryDate 	= strSurgeryDate.equals("") ? GmCommonClass.parseNull((String)request.getParameter("Txt_SurgeryDate")) : strSurgeryDate;
			strAccountID 	= strAccountID.equals("") ? GmCommonClass.parseNull((String)request.getParameter("Cbo_AccountID")) : strAccountID;
			strSurgeonID 	= strSurgeonID.equals("") ? GmCommonClass.parseNull((String)request.getParameter("Cbo_SurgeonID")) : strSurgeonID;
			strPatientPKey	= strPatientPKey.equals("") ? GmCommonClass.parseNull((String)request.getParameter("hPKeyID")) : strPatientPKey;			
		//	strSurgeryCompleted	= strSurgeryCompleted.equals("") ? GmCommonClass.parseNull((String)request.getParameter("Chk_SurgeryComplete")) : strSurgeryCompleted;
		//	strSurgeryCompleted = strSurgeryCompleted.equals("Y") ? "checked" : "";

	if(!strAccountID.equals("") && strPrincipalInv.equals(""))
    {
     strPrincipalInv = "-- No Prinical Investigator found for this Site --";
    }
    
     if(!strPatientPKey.equals(""))
     {
      strRPatientID = strPatientID;
     }
	int intSize = 0;
	HashMap hcboVal = null;
	DynaBean dbcboVal = null;
	strGridData = GmCommonClass.parseNull((String)hmReturn.get("GRIDDATA")) ;
	boolean showGrid=true;
	if(strGridData!=null && strGridData.indexOf("cell")==-1){
	   showGrid = false;	
	}
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	dtSurgeryDate = GmCommonClass.getStringToDate(strSurgeryDate, strApplDateFmt);
	String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
	// PMT-39479: Amnios study label changes
	String strSurgeryDateLabel = "Surgery Date:";
	String strCompleteLabel = "Surgery Completed:";
	String strSurgeonNameLabel = "Surgeon Name:";
	
	if (strStudyId.equals("GPR009")){
		strSurgeryDateLabel = "Injection Date:";
		strCompleteLabel = "Injection Completed:";
		strSurgeonNameLabel = "Investigator Name:";
	}
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Patient Setup </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmPatientMaster.js"></script>
<script>
var objGridData = '<%=strGridData%>';
var objAction ='<%=strAction%>';
var format = '<%=strApplDateFmt%>';
var lockedForm = '<%=strButtonDisabled%>';
var showGrid = '<%=showGrid%>';
</script>
</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();">
<FORM name="frmPatient" method="POST" action="<%=strServletPath%>/GmPatientMasterServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hStudyID" value="<%=strStudyId%>">
<input type="hidden" name="hPatientID" value="<%=strPatientID%>">
<input type="hidden" name="hTxnId" value="">
<input type="hidden" name="hCancelType" value="">
<input type="hidden" name="hSiteName" value="">
<input type="hidden" name="hSurgInvnNo" value="">
<input type="hidden" name="hPatientLId" value="">

<input type="hidden" name="hStrPatientID" value="<%=strPatientID%>" />
<input type="hidden" name="hPKeyID" value="<%=strPatientPKey%>">

	<table border="0"  class="DtTable765"  cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td  valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td class="LLine" colspan="2"></td></tr>
					<tr>
						<td colspan="2" height="25" class="Header">&nbsp;Patient Setup</td>
					</tr>
					
<%
				  if(!strMessage.equals(""))		
				   {				   
%>
					<tr><td  colspan="2"></td></tr>
					<tr><td colspan="2" class="RedText" align="center">
					<%=strMessage%>
					</td></tr>
					<tr><td  class="LLine" colspan="2"></td></tr>
<%
				   }
%>	
						
					<tr><td colspan="2"></td></tr>				
					<tr class="oddshade">
						<td class="RightText" align="right" HEIGHT="24">&nbsp;Patient IDE List:</td>
						<td>&nbsp;<select name="Cbo_PatientID" class="RightText"  onChange="javascript:fnChangePatientID(this.value);""
						onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" 
						><option value="0" >[Choose One]
<%
			  		intSize = alPatientList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alPatientList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strPatientID.equals(strCodeID)?"selected":"";
%>							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("ID")%></option>
<%			  		}
%>					</select></td>
					</tr>
					<tr><td colspan="2"></td></tr>
					<tr class="evenshade">
						<td class="RightText" align="right" HEIGHT="24">&nbsp;Patient IDE #:</td>
						<td width="320">&nbsp;<input type="text" size="10"  value="<%=strPatientID%>" name="Txt_PatientID" 
						  class="InputArea">&nbsp;&nbsp;<gmjsp:button value="Load" gmClass="button" buttonType="Load"
							onClick="fnLoadRep(Txt_PatientID.value);" />&nbsp;
						</td>
					</tr>
					<tr><td  colspan="2"></td></tr>	
					<tr class="oddshade">
						<td class="RightTextBlue" colspan="2" HEIGHT="24" align="center">
							Choose one from the list above to edit or enter details below for a new Patient
						</td>
					</tr>
					<tr><td class="Line" colspan="2"></td></tr>
					<% /************************************
					    * Below Section has Edit Informtion					    
					    ***********************************/
					%>	
										<tr class="evenshade"> 
						<td class="RightText" align="right" HEIGHT="24">Site List:</td>
						<td>&nbsp;
						
						
						<select name="Cbo_AccountID" id="Cbo_AccountID" class="RightText" 
						onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"  disabled="disabled" onChange="javascript:fnLoadAccount(Txt_PatientID.value);">
						<option value="0" >[Choose One]
<%			  		intSize = alAccountList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alAccountList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strAccountID.equals(strCodeID)?"selected":"";
%>							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%			  		}
%>					</select></td>						
					</tr>
					<tr><td  colspan="2"></td></tr>
					<tr class="oddshade">
						<td class="RightText" align="right" HEIGHT="24">Principal Inv:</td>
						<td>&nbsp;<%=strPrincipalInv%></td>
					</tr>
					<tr><td  colspan="2"></td></tr>				
					<tr class="evenshade">
						<td class="RightText" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;Patient IDE #:</td>
						<td width="320">&nbsp;<input type="text" size="10" value="<%=strPatientID%>" 
						name="Txt_RPatientIDE" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
						onBlur="changeBgColor(this,'#ffffff');" ></td>
					</tr>
					<tr><td  colspan="2"></td></tr>
					<tr class="oddshade">
						<td class="RightText" align="right" HEIGHT="24"><%=strSurgeonNameLabel %></td>
						<td width="320">&nbsp;<select name="Cbo_SurgeonID" id="Region" class="RightText"  
						onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
						<option value="0" >[Choose One]						
<%
			  		intSize = alSurgeonName.size();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			dbcboVal = (DynaBean)alSurgeonName.get(i);
			  			strCodeID = String.valueOf(dbcboVal.get("ID"));
						strSelected = strSurgeonID.equals(strCodeID)?"selected":"";
%>							<option <%=strSelected%> value="<%=strCodeID%>"><%=dbcboVal.get("NAME")%></option>
<%			  		}
%>					</select></td>						
					</tr>
					<tr><td  colspan="2"></td></tr>
					<tr class="evenshade">
						<td class="RightText" align="right" HEIGHT="24">&nbsp;Treatment:</td>
						<td width="320">&nbsp;<select name="Cbo_SurgType" id="Region" class="RightText" 
						onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
						<option value="0" >[Choose One]
<%// Code to Load Surgery Type Information
			  		intSize = alSurgeryTypeList.size();
			  		
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alSurgeryTypeList.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strSurgeryType.equals(strCodeID)?"selected":"";
%>						<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%			  		}
%>					</select></td>
					</tr>
					<tr><td  colspan="2"></td></tr>
					<tr class="oddshade">
						<td class="RightText" align="right" HEIGHT="24"><%=strSurgeryDateLabel %></td>
							<td width="320">&nbsp;<gmjsp:calendar
									textControlName="Txt_SurgeryDate"
									textValue="<%=(dtSurgeryDate == null) ? null : new java.sql.Date(dtSurgeryDate.getTime())%>"
									gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
									onBlur="changeBgColor(this,'#ffffff');" />
							</td>
						</tr>
					<tr><td  colspan="2"></td></tr>
					<tr class="evenshade">
						<td class="RightText" align="right" HEIGHT="24"><%=strCompleteLabel %></td>
						<td><input type="checkbox" <%=strSurgeryCompleted%> name="Chk_SurgeryComplete"></td>
					</tr>
					<tr><td class="Line" colspan="2"></td></tr>
				<%--Surgical Intervention report section --%>
				<%if(showGrid){ %>
					<tr class="ShadeRightTableCaption"><td colspan="2" height="24"> Surgical Intervention</td></tr>
					<tr><td colspan="2"> <div id="dataGridDiv"   height="100px"></div></td></tr>
					<tr><td class="Line" colspan="2"></td></tr>
					<%} %>
				   <tr>
			         <td colspan="2"> 
							<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="LogType" value="" />
							<jsp:param name="LogMode" value="Edit" />
							</jsp:include>
						</td>      		
					</tr>	
					 <% String strDisableBtnVal = strButtonDisabled;
						if(strDisableBtnVal.equals("disabled")){
							strDisableBtnVal ="true";
						}
				     %>
				   <tr>
						<td>&nbsp;</td>
						<td  height="30">&nbsp;
						<gmjsp:button value="Submit" name = "save" gmClass="button" buttonType="Save" onClick="fnSubmit();" disabled="<%=strDisableBtnVal %>"/>&nbsp;
						<gmjsp:button value="Reset" gmClass="button" buttonType="Save" onClick="fnReset();" disabled="<%=strDisableBtnVal %>"/>
						<gmjsp:button value="Void" gmClass="button" buttonType="Save" onClick="fnVoid();" disabled="<%=strDisableBtnVal %>"/>
						<gmjsp:button name="btn_patient_Tracking" value="Patient Tracking" gmClass="button" buttonType="Save" onClick="fnStudyBoard();" disabled="<%=strDisableBtnVal %>"/>
						<gmjsp:button name="btn_surg_Intrvn" value="Add Surgical Intervention" gmClass="button" buttonType="Save" disabled="true" onClick="fnLoadSurgicalDetails();"/>
						</td>
					</tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
