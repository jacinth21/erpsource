
<%
	/**********************************************************************************
	 * File		 		: GmStudyFilterInc.jsp
	 * Desc		 		: Include JSP
	 * Version	 		: 1.0
	 * author			: Joe P Kumar
	 ************************************************************************************/
%>

<!-- clinical\GmPatientTrackInc.jsp -->


<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ include file="/common/GmHeader.inc"%>

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel"%>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel"%>


<%@ page import="org.apache.struts.action.ActionForm"%>
<%@ page import="java.util.*"%>
<bean:define id="alStudyList" name="frmPatientTrack"
	property="alStudyList" type="java.util.ArrayList">
</bean:define>
<bean:define id="alSiteList" name="frmPatientTrack"
	property="alSiteNameList" type="java.util.ArrayList">
</bean:define>
<bean:define id="studyListId" name="frmPatientTrack"
	property="studyListId" type="String">
</bean:define>
<bean:define id="siteListId" name="frmPatientTrack"
	property="checkSiteName" type="String[]">
</bean:define>

<%
	String siteList = GmCommonClass.parseNull(siteListId[0]);
	String strFormName = GmCommonClass.parseNull(request
			.getParameter("FORMNAME"));
	String strEnableSiteList = GmCommonClass.parseNull(request
			.getParameter("ENABLESITELIST"));
	String strHideLoadButton = GmCommonClass.parseNull(request
			.getParameter("HIDELOADBUTTON"));
	String strChangeFunction = "";
	String strEnableTrack = GmCommonClass.parseNull(request
			.getParameter("ENABLETRACK"));
	String strDashboard = GmCommonClass.parseNull(request
			.getParameter("HIDDENSITELIST"));
	if (strEnableSiteList.equals("")) {
		strChangeFunction = "fnLoad();";
	}
	try {
%>
<body>
	<table border="0" cellspacing="0" cellpadding="0">
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightTableCaption" align="right" width="80">Study
							List :&nbsp;</td>
						<td><gmjsp:dropdown controlName="studyListId"
								SFFormName="<%=strFormName%>" SFSeletedValue="studyListId"
								SFValue="alStudyList" codeId="CODENMALT" codeName="CODENM"
								defaultValue="[Choose One]" onChange="<%=strChangeFunction%>" />
						</td>
						<%
							if (strEnableSiteList.equals("")) {
									if (GmCommonClass.parseNull(
											request.getParameter("HIDDENSITELIST")).equals("")) {
						%>
						<td class="RightTableCaption" align="right" HEIGHT="20">Site
							List :&nbsp;</td>
						<td>
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td><gmjsp:autolist controlName="checkSiteName"
											seletedValue="<%=siteList%>" tabIndex="2" width="250"
											value="<%=alSiteList%>" defaultValue="All" /> <%
 	if (strEnableTrack.equals("show")) {
 %>
										<a href="javascript:fnLoadPatienTrackList();"><img
											src="<%=strImagePath%>/ordsum.gif" alt="Site Tracker"></a>
										<%
											}
										%></td>
								</tr>
							</table>
						</td>
						<%
							if (strHideLoadButton.equals("")) {
						%>
						<td><gmjsp:button value="Load" gmClass="button"
								buttonType="Load" onClick="fnReport();" /></td>

						<%
							}
									}
								}
						%>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
<%
	} catch (Exception e) {
		e.printStackTrace();
	}
%>



