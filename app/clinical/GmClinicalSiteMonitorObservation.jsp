<%
/**********************************************************************************
 * File		 		: GmClinicalSiteMonitorObservation.jsp
 * Desc		 		: This is a Adding and Editing Site Monitoring Observation.
 * Version	 		: 1.0
 * author			: hparikh
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>


<HTML>
<HEAD>
<TITLE> Globus Medical: Clinical Site Monitoring </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalSiteMonitorObservation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>

<bean:define id="visitDate" name="frmClinicalObservation" property="visitDate" type="java.lang.String"> </bean:define>
<bean:define id="purpose" name="frmClinicalObservation" property="purpose" type="java.lang.String"> </bean:define>
<bean:define id="surgeonName" name="frmClinicalObservation" property="surgeonName" type="java.lang.String"> </bean:define>
<bean:define id="siteName" name="frmClinicalObservation" property="siteName" type="java.lang.String"> </bean:define>
<bean:define id="studyName" name="frmClinicalObservation" property="studyName" type="java.lang.String"> </bean:define>
<bean:define id="observationId" name="frmClinicalObservation" property="observationId" type="java.lang.String"> </bean:define>
<bean:define id="status" name="frmClinicalObservation" property="status" type="java.lang.String"> </bean:define>
<bean:define id="statusName" name="frmClinicalObservation" property="statusName" type="java.lang.String"> </bean:define>

<style type="text/css" media="all">
     @import url("styles/screen.css");
</style>


</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmClinicalObservation.do">
<html:hidden property="hsiteMonitorId" /> 
<html:hidden property="hstudyId" />
<html:hidden property="hstudySiteId" />
<html:hidden property="hpurpose" />
<html:hidden property="hstatus" />
<html:hidden property="hfromDate" />
<html:hidden property="htoDate" />
<html:hidden property="hcraId" />
<html:hidden property="observationId" />
<html:hidden property="statusName" />    
<html:hidden property="strOpt" />
<html:hidden property="haction" value=""/>
<html:hidden property="msg"/>
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VSMOBS"/>

	<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">Site Monitoring: Add Observations</td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td height="100" valign="top">
				<table border="0"  cellspacing="0" cellpadding="0">
					<logic:notEqual name="frmClinicalObservation" property="msg" value="">
					<tr><td colspan="6" height="20" align="center" class="RightBlueText">
					<bean:write name="frmClinicalObservation" property="msg"/></td>
										<tr><td class="Line" colspan="6"></td></tr>
					</tr>
					</logic:notEqual>
					<tr class="evenshade">
						<td class="RightTableCaption" align="right" HEIGHT="25" ></font>&nbsp;Study:</td>						
						<td><%=studyName%></td>
          					<td class="RightTableCaption" align="right" HEIGHT="25" ></font>&nbsp;Site:</td>						
						<td colspan="3"><%=siteName%></td>
          			</tr>
          			
	            				<tr><td colspan="6" class="LLine"></td></tr> 
	            					<tr class="oddshade">
									<td class="RightTableCaption" align="right" HEIGHT="25" ></font>&nbsp;Purpose of Visit:</td>						
									<td><%=purpose%></td>
	            					<td class="RightTableCaption" align="right" width="12%" HEIGHT="25" ></font>&nbsp;Date of Visit:</td>						
									<td><%=visitDate%></td>	
	            					<td class="RightTableCaption" align="right" HEIGHT="25" ></font>&nbsp;Surgeon:</td>						
									<td><%=surgeonName%></td>
	            					           					
	            					</tr>
									
					
					<tr><td colspan="6" class="LLine"></td></tr> 
					<tr class="evenshade">
						<td class="RightTableCaption" align="right" HEIGHT="25" ></font>&nbsp;&nbsp;Issue #:</td>						
							<td>&nbsp;<%=observationId%>
            			</td>
            			<td colspan="4" >&nbsp;</td>
					</tr>
					<tr><td colspan="6" class="LLine"></td></tr> 
					<tr class="oddshade">
						<td class="RightTableCaption" align="right" HEIGHT="25" ><font color="red">*</font>&nbsp;Patient ID:</td>						
							<td>&nbsp;<gmjsp:dropdown controlName="patientId"  SFFormName="frmClinicalObservation" SFSeletedValue="patientId"
							SFValue="alPatientList" codeId="PATID"  codeName="PATIDE" defaultValue="[Choose One]"/>
            			</td>
            			<td colspan="4">&nbsp;</td>
					</tr>
					
					<tr><td colspan="6" class="LLine"></td></tr> 
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="evenshade">
						<td class="RightTableCaption" align="right" HEIGHT="25" ><font color="red">*</font>&nbsp;TimePoint:</td>						
							<td>&nbsp;<gmjsp:dropdown controlName="periodId"  SFFormName="frmClinicalObservation" SFSeletedValue="periodId"
							SFValue="alPeriodList"  codeId="PERIODID" codeName="PERIODDS" defaultValue="[Choose One]"/>
            			</td>
            			<td colspan="4">&nbsp;</td>
					</tr>
					<tr><td colspan="6" class="LLine"></td></tr> 
                    <tr class="oddshade">
                        <td class="RightTableCaption" align="right" HEIGHT="25">Issue Description:</td>
                        <td>&nbsp;<html:textarea property="issueDescription" rows="3" cols="25" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td> 
                        <td colspan="4">&nbsp;</td>
                    </tr>
                   
                    <tr><td colspan="6" class="LLine"></td></tr> 
                    <tr class="evenshade">
						<td class="RightTableCaption" align="right" HEIGHT="25" ><font color="red">*</font>&nbsp;Action Required/Update:</td>						
							<td>&nbsp;<gmjsp:dropdown controlName="actionRequired"  SFFormName="frmClinicalObservation" SFSeletedValue="actionRequired"
							SFValue="alActionReqList"  width="150" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
            			</td>
            			<td colspan="4">&nbsp;</td>
					</tr>
					<tr><td colspan="6" class="LLine"></td></tr> 
					 <tr class="oddshade">
						<td class="RightTableCaption" align="right" HEIGHT="25" ></font>&nbsp;Responsible Person:</td>						
							<td>&nbsp;<gmjsp:dropdown controlName="responisbleBy"  SFFormName="frmClinicalObservation" SFSeletedValue="responisbleBy"
							SFValue="alResponisbleByList"  codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
            			</td>
            			<td colspan="4">&nbsp;</td>
					</tr>
					 
                    <tr><td colspan="6" class="LLine"></td></tr> 
                    <tr class="evenshade">
                        <td class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;Target Resolution Date:</td>
                        <td>&nbsp;
						<gmjsp:calendar SFFormName="frmClinicalObservation" controlName="targetResDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
							&nbsp;&nbsp; 
                        </td>
                        <td colspan="4">&nbsp;</td>
                    </tr>   
                    <tr><td colspan="4" class="LLine"></td></tr> 
                     <tr class="oddshade">
                        <td class="RightTableCaption" align="right" HEIGHT="25">Resolution:</td>
                        <td>&nbsp;<html:textarea property="resolution" rows="3" cols="25" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td> 
                        <td colspan="4">&nbsp;</td>
                    </tr>
                   
                    <tr><td colspan="6" class="LLine"></td></tr> 
                      <tr class="evenshade">
                        <td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;Actual Resolution Date:</td>
                        <td>&nbsp;
                        <gmjsp:calendar SFFormName="frmClinicalObservation" controlName="actualResDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />&nbsp;
							&nbsp;&nbsp; 
                        </td>
                        <td colspan="4">&nbsp;</td>
                      </tr>
                      
					<tr><td colspan="6" class="LLine"></td></tr> 
					<tr class="oddshade">
						<td class="RightTableCaption" align="right" HEIGHT="25" ></font>&nbsp;Status:</td>						
							<td>&nbsp;<gmjsp:dropdown controlName="status"  SFFormName="frmClinicalObservation" SFSeletedValue="status"
							SFValue="alStatusList"  codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
            			</td>
            			<td colspan="4">&nbsp;</td>
					</tr> 
					<%
                        String strobservationId = "fnVoid('"+observationId+"')";
					%>                           
                    <tr><td colspan="6" class="LLine"></td></tr> 
					 <tr>                        
                        <td colspan="6" align="center" height="25">&nbsp;
                        <gmjsp:button value="Submit" gmClass="button" buttonType="Save" onClick="fnSubmit();" />
                        <%if(statusName.equalsIgnoreCase("Open")){ %>
                         <gmjsp:button value="Void" gmClass="button" buttonType="Save" onClick="<%=strobservationId%>"/>
                         <gmjsp:button value="Add More" gmClass="button" buttonType="Save" onClick="fnAddMore();"/>
                        <%}else if(statusName.equalsIgnoreCase("Resolved")){%>
                        <a href="#" onclick="fnNextIssue();"> Go to next Issue from same visit</a>
                        <%}%>                        
                        </td>                      
                    </tr>
                  	
    			</td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

