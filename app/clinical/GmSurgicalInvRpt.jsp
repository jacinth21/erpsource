
<%
	/**********************************************************************************
	 * File		 		: GmSurgicalInvRpt.jsp
	 * Desc		 		: This screen is used for Surgical Intervention list report
	 * Version	 		: 1.0
	 * author			: Dhana Reddy
	 ************************************************************************************/
%>


<!-- clinical\GmSurgicalInvRpt.jsp -->

<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<bean:define id="gridXmlData" name="frmStudyFilterForm" property="gridXmlData" type="java.lang.String" />
<%

String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
boolean showGrid=true;
if(gridXmlData!=null && gridXmlData.indexOf("cell")==-1){
   showGrid = false;	
}
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Surgical Intervention Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmSurgicalInvRpt.js"></script>
<script type="text/javascript">
var objGridData='<%=gridXmlData%>';
var gridObj;
</script>
</HEAD>
<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();">
	<FORM action="/gmSurgicalInvRpt.do">
		<table border="0" class="DtTable950" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="2" height="25" class="Header">&nbsp;Surgical Interventions</td>
			</tr>
			<tr><td colspan="2"> <div id="dataGridDiv"  height="450px"></div></td></tr>
			<%if(showGrid){%>
			<tr class="oddshade">
				<td  colspan="2" align="center" height="30">	
						<div class='exportlinks'>Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
	                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a>
	                    </div>
				</td>
		   </tr>
		   <%} %>
		</table>
	</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
