<%
/**********************************************************************************
 * File		 		: GmClinicalIRBMediaReport.jsp
 * Desc		 		: This is a report screen for Clinical IRB Media Type Report
 * Version	 		: 1.0
 * author			: HParikh
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<bean:define id="gridData" name="frmClinicalIRBReport" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="apprTypeName" name="frmClinicalIRBReport" property="apprTypeName" type="java.lang.String"> </bean:define>
<% 
	boolean showGrid=true;	
	if(gridData!=null && gridData.indexOf("cell")==-1){
	   showGrid = false;			
	}
	String strSiteId = GmCommonClass.parseNull((String)session.getAttribute("strSessSiteId"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Clinical Site Map </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">


<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="javascript/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalIRBReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script>
	var objGridData;
	objGridData = '<%=gridData%>';
	document.onkeypress = function(){
		if(event.keyCode == 13){
			fnMediaTypeSubmit();
		}
		
	}
</script>
</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();fnCheckAppr();" >
<html:form action="/gmClinicalIRBReportAction.do">
<html:hidden property="strOpt" />
<html:hidden property="msg"/>
<html:hidden property="irbAppTypeId" value="60522"/>
	
	<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">IRB Advertisement By Media</td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
		<tr>
				<td width="848"  valign="top">
				<table border="0"  width="100%" cellspacing="0" cellpadding="0">
					<logic:notEqual name="frmClinicalIRBReport" property="msg" value="">
					<tr><td colspan="4" height="20"  class="RightBlueText">
					<bean:write name="frmClinicalIRBReport" property="msg"/></td>
										<tr><td class="Line" colspan="4"></td></tr>
					</tr>
					</logic:notEqual>
					<%
						if(strSiteId.equals(""))
						{
					%>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="evenshade">
						<td class="RightTableCaption" align="right" HEIGHT="30" ></font>Site List:&nbsp;</td>						
						<td width="20%"><gmjsp:dropdown controlName="siteId"  SFFormName="frmClinicalIRBReport" SFSeletedValue="siteId"
							SFValue="alSiteList" codeId="ID" codeName="NAME" defaultValue="[Choose One]"/>
            			</td>            								
						<td class="RightTableCaption" align="right" HEIGHT="30" ></font>Appr Type:&nbsp;</td>						
						<td><%=apprTypeName%></td>
						<td rowspan="12" class="LLine" width="1"></td>
						<td rowspan="12" align="center"><gmjsp:button value="&nbsp;&nbsp;Load&nbsp;&nbsp;" gmClass="button" buttonType="Load" onClick="fnMediaTypeSubmit();" /> </td>
					</tr>
					<%} else {%>
					<tr class="evenshade">
						<td class="RightTableCaption" align="right" HEIGHT="30" ></font>Appr Type:&nbsp;</td>						
						<td colspan="3"><%=apprTypeName%></td>
						<td rowspan="12" class="LLine" width="1"></td>
						<td rowspan="12" align="center"><gmjsp:button value="&nbsp;&nbsp;Load&nbsp;&nbsp;" gmClass="button" buttonType="Load" onClick="fnMediaTypeSubmit();" /> </td>
					</tr>
					<%} %>
					<tr><td colspan="4" class="LLine" height="1"></td></tr> 
					<tr class="oddshade">
						<td class="RightTableCaption" align="right" HEIGHT="30" ></font>Appr Reason:&nbsp;</td>						
							<td><gmjsp:dropdown controlId="irbReasonId"  controlName="irbReasonId"  SFFormName="frmClinicalIRBReport" SFSeletedValue="irbReasonId"
							SFValue="alIRBApprovalReasonList"  codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
            			</td>
            			 <td class="RightTableCaption" align="right" HEIGHT="30" ></font>Media Type:&nbsp;</td>						
							<td ><gmjsp:dropdown controlName="irbMediaTypeId"  SFFormName="frmClinicalIRBReport" SFSeletedValue="irbMediaTypeId"
							SFValue="alIRBMediaTypeList"  codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
            			</td>    			
					</tr>
                    <tr><td colspan="4" class="LLine" height="1"></td></tr>                   
                   </table> 
    			</td>		
  		  </tr>
  		  </tr>
 			<tr> 
			<%if(showGrid){%>
				<td colspan="4">
			    	<div id="dataGridDiv" class="grid" style="height:345px"></div>
				</td>	
			<%}else{ %>
				<td class="RightText" colspan="4">
			    	Nothing found to display.
				</td>	
			<%}%>
    	 	</tr>  		  
  		  <tr><td colspan="2" bgcolor="#cccccc"></td></tr> 
  		  <tr colspan = "2" class = "oddshade">
						<td class = "oddshade" colspan="3" align="center" height="30">	
							<div >Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!-- | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a> -->
                     	</td>
		 </tr>
  		  </table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

