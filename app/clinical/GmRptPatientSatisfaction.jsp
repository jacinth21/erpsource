<%
/**********************************************************************************
 * File		 		: GmRptPatientSatisfaction.jsp
 * Desc		 		: Report for Patient satisfaction
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>


<!-- clinical\GmRptPatientSatisfaction.jsp -->
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ include file="/common/GmHeader.inc"%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmGridFormat"%>

<bean:define id="hmCrossTabReport" name="frmOutcomeReportVAS" property="hmCrossTabReport" type="java.util.HashMap"> </bean:define>
<bean:define id="gridData" name="frmOutcomeReportVAS" property="gridXmlData" type="java.lang.String"> </bean:define>
<% 
String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
boolean show = false;
if(gridData.indexOf("cell")!=-1)
	show = true;
else
	show = false;

log.debug("xmlGridData  " +gridData);
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Patient Satisfaction </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmStudyFilterInc.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
document.onkeypress = function(){
		if(event.keyCode == 13){
			fnReport();
		}
		
	}
function fnSubmit()
{
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmOutcomeReportVAS.strOpt.value = "Report";
	fnStartProgress('Y');
	document.frmOutcomeReportVAS.submit();
}

function fnLoad()
{
	document.frmOutcomeReportVAS.selectAll.checked = false;
	document.frmOutcomeReportVAS.strOpt.value = "";
	document.frmOutcomeReportVAS.action = "/gmRptOutcomeScores.do?method=reportPatientSatisfaction";
	document.frmOutcomeReportVAS.submit();
}

function fnReport()
{
	fnValidateDropDn('typeId',' Period ');
	document.frmOutcomeReportVAS.action = "/gmRptOutcomeScores.do?method=reportPatientSatisfaction";
	fnSubmit();
}	

var objGridData = '<%=gridData%>';

function fnOnPageLoad(){
		<%if(show){%>	
			gridObj = initGrid('dataGridDiv',objGridData,1);
		<%}%>
	fnCheckAll();	
		
}	
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<html:form action="/gmRptOutcomeScores.do"  >
<html:hidden property="strOpt" />

	<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">Patient Satisfaction</td>
		</tr>
		<tr>
			<td width="848" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
						<td colspan="2" align="center" >&nbsp;
	                    	<jsp:include page="/clinical/GmStudyFilterInc.jsp" >
							<jsp:param name="FORMNAME" value="frmOutcomeReportVAS" />
							<jsp:param name="HIDELOADBUTTON" value="HIDE" />
							</jsp:include>	
						</td>
					</tr>	
					<tr><td height="5"></td></tr>
					<tr>
						 <td class="RightTableCaption" align="right" height="30" width="80" nowrap>Period :</td> 
    	                <td width="100%">&nbsp;<gmjsp:dropdown controlName="typeId" SFFormName="frmOutcomeReportVAS" SFSeletedValue="typeId"
								SFValue="alType" codeId = "SEQ"  codeName = "PERIOD" defaultValue= "[Choose One]"  />
								&nbsp;&nbsp;<gmjsp:button value="Load" gmClass="button" buttonType="Load" onClick="fnReport();"/>													
    		            </td>
					</tr>
					<%if(show){%>
                    <tr><td colspan="2" class="ELine"></td></tr> 
					<tr>
						<td colspan ="2" align=center> <div id="dataGridDiv" style="" height="160" ></div></td>
					</tr>
					<tr class="shade">
						<td class = "oddshade" colspan="3" align="center" height="30">	
							<div class='exportlinks'>Export Options : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!-- | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a>  -->
                            </div>

							
						</td>
					</tr>		
				<% } %>	
			   	</table>
  			   </td>
  		  </tr>	
  		
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>

