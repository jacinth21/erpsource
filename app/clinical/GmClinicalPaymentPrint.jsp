<%@ page language="java" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<bean:define id="hmPrintParam" name="frmClinicalPayment" property="hmPrintParam" type="java.util.HashMap"></bean:define>
<bean:define id="alPrintResult" name="frmClinicalPayment" property="alPrintResult" type="java.util.ArrayList"></bean:define>
<%
	String strHtmlJasperRpt = "";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Print Payment</TITLE>
<style type="text/css" media="all">
     @import url("styles/screen.css");
    table.DtTable1000 {
	width: 1200px;
	}
</style>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="javascript/Error.js"></script>
<script language="JavaScript" src="javascript/Message.js"></script>
<script>

function fnPrint()
{
var prtContent = document.getElementById("jasper");
var WinPrint = document.getElementById("ifmPrintContents").contentWindow;
WinPrint.document.open();
WinPrint.document.write(prtContent.innerHTML);
WinPrint.document.close();
WinPrint.focus();
WinPrint.print();
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" >
 
<html:form  action="/gmClinicalPayment.do"  >
 <table align="center" height="40" width="100%"><tr><td align="center">
 <gmjsp:button value="Print" gmClass="button" buttonType="Load" onClick="fnPrint();"/>
 </td></tr></table>	
 <iframe id="ifmPrintContents" style="height: 0px; width: 0px; position: absolute"></iframe>
<div id="jasper"> 
		<%
			String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
			GmJasperReport gmJasperReport = new GmJasperReport();								
			gmJasperReport.setRequest(request);
			gmJasperReport.setResponse(response);
			gmJasperReport.setJasperReportName("/GmClinicalPaymentPrint.jasper");
			gmJasperReport.setHmReportParameters(hmPrintParam);							
			gmJasperReport.setReportDataList(alPrintResult);				
			gmJasperReport.setBlDisplayImage(true);
			strHtmlJasperRpt = gmJasperReport.getHtmlReport();	
		%>
		<%=strHtmlJasperRpt %>				
</div>	

</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>