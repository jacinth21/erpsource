<%
/**********************************************************************************
 * File		 		: GmPatientFormTrack.jsp
 * Desc		 		: Report for the enrolled patients by month
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>

<!--clinical\querysystem\GmPatientFormTrack.jsp -->
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ include file="/common/GmHeader.inc" %>

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmGridFormat"%>

<bean:define id="hmReport" name="frmStudyFilterForm" property="hmCrossTabReport" type="java.util.HashMap"></bean:define>
<bean:define id="hmRules" name="frmStudyFilterForm" property="hmRules" type="java.util.HashMap"></bean:define>
<% 
	GmGridFormat gmCrossTab = new GmGridFormat();
	ArrayList alStyle 		= new ArrayList();
	//log.debug(" Size of hmReport in the beginning " + hmReport.size());

	
	ArrayList alDrillDown 	= new ArrayList();
	

	String strhAction = (String)request.getAttribute("hAction");
	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}
	
	String strPatientId = GmCommonClass.parseNull(request.getParameter("patient_Id"));
	String strPatientIde = GmCommonClass.parseNull(request.getParameter("patient_Ide"));
	
	gmCrossTab.setValueType(0);
	//No Total Field Required
	gmCrossTab.setHideColumn("Total");
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setTotalRequired(false);
	
	//Setting the cross tab style
	gmCrossTab.reNameColumn("Name","Time-point");
	gmCrossTab.setColumnWidth("Name",90);
	gmCrossTab.setColumnWidth("Time-point",90);
	gmCrossTab.setColumnWidth("Exten Films",50);
	gmCrossTab.setColumnWidth("MRI/CT Films",60);
	gmCrossTab.setColumnWidth("Comments",75);
	gmCrossTab.setAmountWidth(42);
	gmCrossTab.setRoundOffAllColumnsPrecision("0");
	gmCrossTab.setRowHeight(20);
	
	// Page Body Style
	gmCrossTab.setGeneralHeaderStyle("aaTopHeaderLightG");
	gmCrossTab.setDivHeight(400);
	gmCrossTab.setRowHighlightRequired(true);
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setGroupingRequired(true);
	gmCrossTab.setHideColumn("DueDate");
	gmCrossTab.setHideColumn("STUDYPERIODID");
	gmCrossTab.setColumnLineStyle("borderDark");
	gmCrossTab.setAdditionalValMap(hmRules);  
	gmCrossTab.setDecorator("com.globus.crosstab.beans.GmPatientTrackDecorator");
	gmCrossTab.setExport(true, pageContext, "/gmRptPatientTracking.do?method=reportPatientTrackForm&strExcel=Excel", "PatientFormTracks");
	gmCrossTab.setLinkRequired(false);
	
	String strGridData = gmCrossTab.generateGridXML(hmReport);
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Enrollment by month </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">


<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/GmCommonGrid.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css"> 

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
 
<script>
function fnViewfrmAdverseEvent(timePoint,strCboFormId,strCboPeriodId,strStPerId)
{
	//alert('::FormDataEntry::'+timePoint+"..."+strCboFormId+"..."+strCboPeriodId+' strStPerId '+strStPerId);
	document.frmStudyFilterForm.Cbo_Form.value=strCboFormId;
	document.frmStudyFilterForm.Cbo_Period.value=strCboPeriodId;
	document.frmStudyFilterForm.hStPerId.value=strStPerId;
	document.frmStudyFilterForm.action = "/GmFormDataEntryServlet";
	document.frmStudyFilterForm.submit();
}


function fnReport()
{
	document.frmStudyFilterForm.action = "/gmRptPatientTracking.do?method=reportPatientTrackForm";
	document.frmStudyFilterForm.strOpt.value = "Report";
	document.frmStudyFilterForm.submit();
}	

function fnCallAdverseEventReport(strTimePoint)
{
	document.frmStudyFilterForm.strOpt.value = "Report";
	document.frmStudyFilterForm.strTimePoint.value = strTimePoint;
	document.frmStudyFilterForm.action = "/gmRptAdverseEvent.do?method=reportAdverseEvent";
	document.frmStudyFilterForm.submit();
}


function fnLoad()
{
	document.frmStudyFilterForm.strOpt.value = "";
	document.frmStudyFilterForm.checkSiteName.value = "";
	document.frmStudyFilterForm.patient_Ide.value = "";
	document.frmStudyFilterForm.action = "/gmRptPatientTracking.do?method=reportPatientTrackForm";
	document.frmStudyFilterForm.submit();
}

function fnLoadPatienTrackList()
{
	document.frmStudyFilterForm.strOpt.value = "Report";
	document.frmStudyFilterForm.action = "/gmRptPatientTracking.do?method=reportPatientTrackList";
	document.frmStudyFilterForm.submit();
}

function fnLoadPatientTrackInfo()
{
	document.frmStudyFilterForm.action = "/gmRptPatientTracking.do?method=reportPatientTrackInfo";
	document.frmStudyFilterForm.strOpt.value = "Report";
	document.frmStudyFilterForm.submit();
}

function fnCallCommentSummaryReport(strStudyPeriodId)
{
	var patientId = document.frmStudyFilterForm.sessPatientPkey.value;

	document.frmStudyFilterForm.action = "/gmPatientLogSummary.do?strOpt=Load&strDhtmlx=true&patientId="+patientId+"&studyPeriod="+strStudyPeriodId;
	document.frmStudyFilterForm.submit();
}

var objGridData = '<%=strGridData%>';
	
function fnOnPageLoad(){
	gridObj = initGrid('dataGridDiv',objGridData,1);
}
</script>

</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();">
<html:form action="/gmRptPatientTracking.do"  >
<html:hidden property="strOpt" />
<input type="hidden" name="strTimePoint" />
<input type="hidden" name="selectAll" />
<html:hidden property="sessPatientPkey" />

<input type="hidden" name="hAction" value="LoadQues"/>
<input type="hidden" name="Cbo_Form"  />
<input type="hidden" name="Cbo_Period" />
<input type="hidden" name="hStPerId"  />


<input type="hidden" name="Txt_FormDate" value=""/>
<input type="hidden" name="hStudyPeriod" valie="6309"/>


	<table border="0"  class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">&nbsp;Patient Tracking Report</td>
		<tr>
    </table>		     	
	<table class="DtTable765">
		<tr>
			<td>
				<div id="dataGridDiv" style="grid" height="300px"></div>			
			</td>
		</tr>	
 		<tr style="height:96px">
			<td style="height:96px">
			<!-- <b>a.</b>  --> &nbsp;&nbsp;<font color=Black><strong> Blank " "</strong></font></t>= Form Not Received <br>
			<!-- <b>b.</b>  --> &nbsp;&nbsp;<font color=Green><strong> Green "*" </strong></font></t>= Form Received <br>
			<!-- <b>c.</b>  --> &nbsp;&nbsp;<font color=Black><strong> "xxx" </strong></font></t>= Not Applicable <br>
			</td>
		</tr>
		<tr class="oddshade">
		<td colspan="2" align="center" height="30">	
					<div class='exportlinks'>Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a>                               
                    </div>
		</td>
		
	</tr>				
	</table>
</html:form>

</BODY>
</HTML>

