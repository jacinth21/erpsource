<%
/**********************************************************************************
 * File		 		: GmDynamicDataLoad.jsp
 * Desc		 		: Report for the all the forms 
 * Version	 		: 1.0
 * author			: Brinal
************************************************************************************/
%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ include file="/common/GmHeader.inc" %> 
   
<bean:define id="hmDynamicDataLoad" name="frmDynamicDataLoad" property="hmCrossTabReport" type="java.util.HashMap"> </bean:define>

<% 

//Logger log = GmLogger.getInstance(GmCommonConstants.CLINICAL);  // Instantiating the Logger 
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();	
	
   //PMT-41230 : Amnios RT study - Common issue
	String strSessStudyId = GmCommonClass.parseNull((String)session.getAttribute("strSessStudyId"));
	String strSurgeonNameLabel = "Surgeon";
	String strSurgeryDateLabel = "Surgery Date";

	gmCrossTab.setGeneralHeaderStyle("ShadeDarkGrayTD");
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setNoDivRequired(true);
	gmCrossTab.setRoundOffAllColumnsPrecision("0");
	gmCrossTab.reNameColumn("Name","Patient ID");
	
	
	
	
	gmCrossTab.setHideColumn("Name");	
	gmCrossTab.displayZeros(true);
	gmCrossTab.setskipCalculationForAllColumns(true);
	
	
	gmCrossTab.setNameWidth(50);
	gmCrossTab.setColumnWidth("Treatment",180);
	gmCrossTab.setColumnWidth("Surgeon",120);
	gmCrossTab.setColumnWidth("SurgeryDate",100);
	gmCrossTab.setColumnWidth("Timepoint",100);	
	gmCrossTab.setColumnWidth("examDate",100);
	
	//gmCrossTab.setSortRequired(true);
	//gmCrossTab.addSortType("Treatment","String");
	gmCrossTab.addStyle("PatientId","ShadeLevel2NoBold","ShadeDarkGrayTD") ;
	gmCrossTab.addStyle("Treatment","ShadeLevel2NoBold","ShadeDarkGrayTD") ;
	gmCrossTab.addStyle("Surgeon","ShadeLevel2NoBold","ShadeDarkGrayTD") ;
	gmCrossTab.addStyle("SurgeryDate","ShadeLevel2NoBold","ShadeDarkGrayTD") ;
	gmCrossTab.addStyle("Timepoint","ShadeLevel2NoBold","ShadeDarkGrayTD") ;
	gmCrossTab.addStyle("ExamDate","ShadeLevel2NoBold","ShadeDarkGrayTD") ;
	
	// gmCrossTab.setExport(true, pageContext, "/gmDynamicDataLoad.do?method=reportDynamicData", "DynamicDataLoad");
	 
	if((String)request.getParameter("surgeryDate")== null)
	{
		gmCrossTab.setHideColumn("SurgeryDate"); 
	}
	
	if((String)request.getParameter("surgeon")== null)
	{
		gmCrossTab.setHideColumn("Surgeon"); 
	}
	
	if((String)request.getParameter("treatment")== null)
	{
		gmCrossTab.setHideColumn("Treatment"); 
	}
	if((String)request.getParameter("examDate")== null)
	{
		gmCrossTab.setHideColumn("ExamDate"); 
	}

	
	if(strSessStudyId.equals("GPR009")){
		gmCrossTab.reNameColumn("Surgeon","Investigator");
		gmCrossTab.reNameColumn("SurgeryDate","InjectionDate");
		strSurgeonNameLabel = "Investigator Name";
		strSurgeryDateLabel = "Injection Date";
	}

%>

<HTML>

<HEAD>
<TITLE> Globus Medical: Planned Surgery Date </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="/styles/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="javascript/Message.js"></script>
<script language="JavaScript" src="javascript/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />



<style type="text/css" media="all">
     @import url("styles/screen.css");
</style>

<script>

function fnGo(value)
{	
	fnValidate(value);
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}	
	document.frmDynamicDataLoad.action = "/gmDynamicDataLoad.do?method=reportDynamicData";
	document.frmDynamicDataLoad.strOpt.value = value;
	document.frmDynamicDataLoad.hExcel.value="";
	fnStartProgress('Y');
	document.frmDynamicDataLoad.submit();
}

function fnValidate(value){
	var questions = countchkd(document.frmDynamicDataLoad.checkQuestions);
	var answers = countchkd(document.frmDynamicDataLoad.checkAnswers);
	var timepoints = countchkd(document.frmDynamicDataLoad.checkTimePeriods);
	if (value == '60270')	//if timepoints  then only one question should be selected
		{
			if (questions > 1){
			 	Error_Details("Number of questions selected cannot be more than one if Dynamic column is Timepoints");
			}
			if (answers > 1){
			 	Error_Details("Number of answers selected cannot be more than one if Dynamic column is Timepoints");
			}		
		}	
	if(document.frmDynamicDataLoad.formList.value == 0){
		Error_Details("Pls select the Form");
	}
	if(timepoints == 0){
		Error_Details("Pls select the timepoint");
	}
	
	if(questions == 0){
		Error_Details("Pls select the Questions");
	}
	if(answers == 0){
		Error_Details("Pls select the Answers");
	}
			
		//matchValues(document.frmDynamicDataLoad.checkQuestions,document.frmDynamicDataLoad.checkAnswers);
}

function fnCheckAllAttributes(varObj)
{
	var varAction = document.all.strOpt.value;
	
	if ((varAction == '' || varAction == 'Load' || varAction.indexOf("Link") != -1))
	{
		fnSelectAllAttributes(varObj,'selectAll');
	}
	
}	

function fnDeselectAttributes(varObj)
{
	objSelAll = eval("document.all.selectAll"+varObj);
	if (objSelAll) {
	objSelAll.checked = false;
	}
}

function matchValues(varObj1,varObj2)
{
	var qArr = new Array();	
	objCheckArrLen1 = varObj1.length;	
	objCheckArrLen2 = varObj2.length;
	for(i = 0; i < objCheckArrLen1; i++) //get the array of questions selected 
	{		
		if(varObj1[i].checked == true)
		{
			qArr[i]= (varObj1[i].id).substring(5);
		}
	}
	for(i = 0; i < objCheckArrLen2; i++) //get the array of answers selected 
	{		
		if(varObj1[i].checked == true)
		{
			qArr[i]= (varObj1[i].id).substring(5);
		}
	}
}


function countchkd(varObj)
{
eval(varObj);
//alert("varObj "+ varObj.name);
	var ctr=0;	
	if (varObj != null){	
		objCheckArrLen = varObj.length;
		
		if (varObj.checked == true){
			ctr = 1
			return ctr;
		}
	for(i = 0; i < objCheckArrLen; i++) 
	{	
		if(varObj[i].checked == true)
		{	
		 ctr++;
		}
	}
	return ctr;	
}
}

function fnSelectAllAttributes(varObj, varCmd)
{
				objSelAll = eval("document.all.selectAll"+varObj);
				if (objSelAll ) {
				objCheckArr = eval("document.all.check"+varObj);
				if ((objSelAll.checked || varCmd == 'selectAll') && objCheckArr)
				{
					objSelAll.checked = true;
					objCheckArrLen = objCheckArr.length;	
					
					if (objCheckArr.checked == false){
						objCheckArr.checked = true;						
						return ;
					}
									
					for(i = 0; i < objCheckArrLen; i ++) 
					{	
						objCheckArr[i].checked = true;
					}
				}
				else if (!objSelAll.checked  && objCheckArr ){
					objCheckArrLen = objCheckArr.length;
					
					if (objCheckArr.checked == true){
						objCheckArr.checked = false;						
						return ;
					}
					for(i = 0; i < objCheckArrLen; i ++) 
					{	
						objCheckArr[i].checked = false;
					}
				}
				}
}

function fnReload()
{ 
 document.frmDynamicDataLoad.strOpt.value = "load";
 document.frmDynamicDataLoad.hExcel.value="";
 document.frmDynamicDataLoad.submit();
}

function frmOnLoad()
{
	if(document.frmDynamicDataLoad.formList.value == 0)
	{
		document.frmDynamicDataLoad.surgeryDate.checked=true;
		document.frmDynamicDataLoad.surgeon.checked = true;
		document.frmDynamicDataLoad.treatment.checked=true;	
	}	
	fnEnbDisb(document.frmDynamicDataLoad.dynamicColumn.value);		
}

function fnDownloadVer(value)
{
//windowOpener("/gmDynamicDataLoad.do?method=reportDynamicData&hExcel=Excel","INVEXCEL","resizable=yes,scrollbars=yes,top=300,left=200,width=800,height=490");

var timepoints = countchkd(document.frmDynamicDataLoad.checkTimePeriods);
var questions = countchkd(document.frmDynamicDataLoad.checkQuestions);
var answers = countchkd(document.frmDynamicDataLoad.checkAnswers);
if(questions > 0 && answers > 0 && timepoints >0){
	document.frmDynamicDataLoad.hExcel.value='Excel';
	document.frmDynamicDataLoad.strOpt.value = value;
 	document.frmDynamicDataLoad.submit();
 }
 else{
	Error_Details("No data available to load");
 }
 if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}	
}

function fnEnbDisb(value){
if (value == '60270')	//if timepoints  
	{
	document.frmDynamicDataLoad.selectAllQuestions.disabled=true;
	document.frmDynamicDataLoad.selectAllAnswers.disabled=true;
	document.frmDynamicDataLoad.selectAllQuestions.checked=false;
	document.frmDynamicDataLoad.selectAllAnswers.checked=false;
	document.frmDynamicDataLoad.examDate.disabled=true;
	}
else if(value == '60271') //if Answers
	{
	document.frmDynamicDataLoad.selectAllQuestions.disabled=false;
	document.frmDynamicDataLoad.selectAllAnswers.disabled=false;

	document.frmDynamicDataLoad.examDate.disabled=false;
	}


}
function fnUncheck(){

	document.frmDynamicDataLoad.selectAllTimePeriods.checked=false;
	document.frmDynamicDataLoad.selectAllQuestions.checked=false;
	document.frmDynamicDataLoad.selectAllAnswers.checked=false;
}

function fnLoad()
{
	document.frmDynamicDataLoad.selectAll.checked = false;
	document.frmDynamicDataLoad.strOpt.value = "";
	document.frmDynamicDataLoad.action = "/gmDynamicDataLoad.do?method=reportDynamicData";
	document.frmDynamicDataLoad.submit();
}

</script>

</HEAD>


<BODY leftmargin="0" topmargin="0" >
<html:form action="/gmDynamicDataLoad.do?method=reportDynamicData" >
<html:hidden property="strOpt" />
<html:hidden property="siteId" value="" />
<html:hidden property="hExcel" value="" />


	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td height="25" class="Header">Dynamic Form Load</td>
		</tr>
		<tr><td height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr><td class="ELine"></td></tr> 
                    <tr>
						<td colspan="4" align="center" valign="middle">&nbsp;
	                    	<jsp:include page="/clinical/GmStudyFilterInc.jsp" >
							<jsp:param name="FORMNAME" value="frmDynamicDataLoad" />	
							<jsp:param name="HIDELOADBUTTON" value="true" />	
							</jsp:include>								
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
		<td valign="top" colspan="2">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">			  
				<tr><td height="10"></td></tr>					   
				     <tr>				     
                         <td width="60%" valign="top" colspan="4">
                             <table cellspacing="0" cellpadding="0">
	                        	<tr>
	                        	<td>
	                        		<table cellpadding="0" cellspacing="0" border="0">
						<!-- Custom tag lib code modified for JBOSS migration changes -->
			             				<tr>
						             		<td class="RightTableCaption" align="right" >&nbsp;<font color="red">* </font>Form List :&nbsp</td>   
							         		<td  height="30" align="left">&nbsp;
							         		<gmjsp:dropdown controlName="formList"  SFFormName='frmDynamicDataLoad' SFSeletedValue="formList"
								       		SFValue="alFormList" codeId="STUDYLISTID" codeName="FORMNAME" defaultValue= "[Choose One]" onChange="fnUncheck();fnReload();" />
								     		</td>  	       
						       		</tr>
						   			</table>
						   		</td>	                        		
								</tr>	    
							</table>							
							</td>	
							<td width="40%">
			             	<table cellpadding="0" cellspacing="0" border="0">
			             		<tr>
						             <td class="RightTableCaption" align="right" >&nbsp; Dynamic Column:&nbsp</td>   
							         <td height="30" align="left" >&nbsp;<gmjsp:dropdown controlName="dynamicColumn"  SFFormName='frmDynamicDataLoad' SFSeletedValue="dynamicColumn"
								       	SFValue="alDynamicColumns" codeId="CODEID" codeName="CODENM" onChange="fnEnbDisb(this.value)"/>
								     </td>  	 
								          
						       	</tr>
						   </table>
						</td>				
				     </tr>
				     <tr><td height="10"></td></tr>
				</table>
			</td>
		</tr>
		<tr>
		<td valign="top" colspan="4">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr><td height="10"></td></tr>					
				     <tr>
					   <td width="150" valign="top" >
					   		<table cellspacing="0" cellpadding="0" >
	                        	<tr bgcolor="gainsboro">
	                        		<td>
										<html:checkbox property="selectAllTimePeriods" name="frmDynamicDataLoad" onclick="fnSelectAllAttributes('TimePeriods','toggle');"/><B>TimePoints </B> 
		     						</td>
								</tr>	    
							</table>
						<DIV style="display:visible;height: 100px; overflow: auto; border-width:1px; border-style:solid;">
	                         <table cellspacing="0" cellpadding="0">
		                        	<logic:iterate id="timeperiodlist" name="frmDynamicDataLoad" property="alTimePeriodList">
			                        	<tr>
			                        		<td>
											    <htmlel:multibox  property="checkTimePeriods" name="frmDynamicDataLoad" value="${timeperiodlist.PERIODID}" onclick="fnDeselectAttributes('TimePeriods');" />
											    <bean:write name="timeperiodlist" property="PERIODDESC" />
				     						</td>
										</tr>	    
									</logic:iterate>
							</table>
						</DIV>
						</td>
					   	<td align="left" width="150">
                             <table cellspacing="0" cellpadding="0" colspan="0">
	                        	<tr bgcolor="gainsboro">
	                        		<td>
									    <html:checkbox property="selectAllQuestions" name="frmDynamicDataLoad" onclick="fnSelectAllAttributes('Questions','toggle');fnReload();" /><B>Questions </B> 
		     						</td>
								</tr>	    
							</table>							
							<DIV style="display:visible;height: 100px; overflow: auto; border-width:1px; border-style:solid; align=right;">
		                         <table cellspacing="0" cellpadding="0">
			                        	<logic:iterate id="questionList" name="frmDynamicDataLoad" property="alQuestionList">
				                        	<tr>
				                        		<td>
												    <htmlel:multibox property="checkQuestions" name="frmDynamicDataLoad" styleId="${questionList.QNO}" value="${questionList.QID}" onclick="fnReload();" title="${questionList.QDESC}" />
												    <bean:write name="questionList" property="QNO" />
					     						</td>
											</tr>	    
										</logic:iterate>
									</table>
								</DIV>
							</td>	
						<td align="left" width="150">
                             <table cellspacing="0" cellpadding="0">
	                        	<tr bgcolor="gainsboro">
	                        		<td >
									    <html:checkbox property="selectAllAnswers" name="frmDynamicDataLoad" onclick="fnSelectAllAttributes('Answers','toggle');" /><B>Answers </B> 
		     						</td>
								</tr>	    
							</table>							
							<DIV style="display:visible;height: 100px; overflow: auto; border-width:1px; border-style:solid; align=right;">
		                         <table cellspacing="0" cellpadding="0">
			                        	<logic:iterate id="answersList" name="frmDynamicDataLoad" property="alAnswersList">
				                        	<tr>
				                        		<td>
												    <htmlel:multibox property="checkAnswers" name="frmDynamicDataLoad" styleId="${answersList.ANSNO}" value="${answersList.ANSGRPID}" title="${answersList.ANSDESC}" />
												    <bean:write name="answersList" property="ANSNO" />
					     						</td>
											</tr>	    
										</logic:iterate>
									</table>
								</DIV>
							</td>	
						<td align="left" width="150" >
							<table cellspacing="0" cellpadding="0" >
							<tr>
								<td>
									<html:checkbox property="treatment" name="frmDynamicDataLoad" /><B>Treatment</B>									
								</td>
							</tr>
							<tr>
								<td>
									<html:checkbox property="surgeon" name="frmDynamicDataLoad" /><B><%=strSurgeonNameLabel %></B>
								</td>
							</tr>
							<tr>
								<td>
									<html:checkbox property="surgeryDate" name="frmDynamicDataLoad" /><B><%=strSurgeryDateLabel %></B>
								</td>
							</tr>
							<tr>
								<td>
									<html:checkbox property="examDate" name="frmDynamicDataLoad" /><B>Exam Date</B>
								</td>
							</tr>
							<tr>
								<td>
								<gmjsp:button value="Load" gmClass="button" buttonType="Load" onClick="fnGo(frmDynamicDataLoad.dynamicColumn.value);"/>
								</td>						              
				     		</tr> 
				     		
    	        			</table>
  			 			</td>
  		  </tr>	  		
    </table>
    </td></tr></table>
    <table class="DtTable700">						
						<tr>
							<td colspan ="2" align=center> <%=gmCrossTab.PrintCrossTabReport(hmDynamicDataLoad) %></td>
						</tr>
						<tr>
						<td align="center">										
								<gmjsp:button value="Excel Download" gmClass="button" buttonType="Load"
								onClick="javascript:fnDownloadVer(frmDynamicDataLoad.dynamicColumn.value);"/>	
						</td>						
						</tr>	    
</table>	
</html:form>
<%@ include file="/common/GmFooter.inc" %>            
</BODY>

<script>
frmOnLoad();
</script>
</HTML>
