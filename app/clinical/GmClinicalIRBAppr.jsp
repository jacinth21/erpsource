 <%
 	/**********************************************************************************
 	 * File		 		: GmClinicalIRBAppr.jsp
 	 * Desc		 		: This is a transaction screen used to setup the IRB Approval
 	 * Version	 		: 1.0
 	 * author			: Rajeshwaran
 	 ************************************************************************************/
 %>
 <!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList"%>

<bean:define id="gridData" name="frmClinicalIRBApproval" property="gridData" type="java.lang.String"> </bean:define>
<bean:define id="statusName" name="frmClinicalIRBApproval" property="statusName" type="java.lang.String"> </bean:define>
<bean:define id="lockedStudyDisable" name="frmClinicalIRBApproval" property="lockedStudyDisable" type="java.lang.String"> </bean:define>
<HTML>
<HEAD>
<TITLE> Globus Medical: Clinical IRB Approval Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="styles/Globus.css">
<link rel="stylesheet" type="text/css" href="/styles/GmClinical.css">
<script language="JavaScript" src="javascript/GlobusCommonScript.js"></script>
<script language="JavaScript" src="javascript/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<style type="text/css" media="all">
     @import url("styles/screen.css");
</style>
<script>
	var objGridData;
	objGridData = '<%=gridData%>';
	var status='<%=statusName%>';
</script>
</HEAD>
<script>
function fnReload(){
	document.frmClinicalIRBApproval.approvalType.value="";
	document.frmClinicalIRBApproval.approvalReason.value="";	
	document.frmClinicalIRBApproval.strOpt.value="list_irb";
	document.frmClinicalIRBApproval.submit();
}
function fnSubmit(){

	fnValidateDropDn('studySiteID','Site List');
	fnValidateDropDn('approvalType','Approval Type');
	fnValidateDropDn('approvalReason','Approval Reason');
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmClinicalIRBApproval.strOpt.value="save_irb";
		fnStartProgress();
	document.frmClinicalIRBApproval.submit();
}
function fnOnPageLoad()
{	  
	  obj = document.getElementById("showbutton");
	  if(status=='Approved'){
	  	document.getElementById("subbtn").disabled=true;
	  }	 	
	  /*if(document.frmClinicalIRBApproval.irbid.value.length>0){	  	
		obj.style.display = 'block'; //Show Button		
	  }else{
	  	obj.style.display = 'none'; // Hide Button
	  }*/
	  
	  if(objGridData.indexOf("cell", 0)==-1){
	  	  obj.style.display = 'none'; // Hide Button
		  return true;
	  }
	  
	  gridObj = initGrid('irbapproval',objGridData);
	  gridObj.enableTooltips("false,true,true,true,true,true");	 
	  		
	fnCheckAppr();
	toggleMedia();
	
	
}

function fnLoadIRBAppr(virbid)
{
	document.frmClinicalIRBApproval.irbid.value = virbid;
	document.frmClinicalIRBApproval.strOpt.value = "edit_irb";
	document.frmClinicalIRBApproval.submit();
}
function fnLoadTrackEvent(virbid)
{	
	document.frmClinicalIRBApproval.irbid.value = virbid;	
	document.frmClinicalIRBApproval.strOpt.value = "load_irb_event";
	document.frmClinicalIRBApproval.action="/gmClinicalIRBEventAction.do";
	document.frmClinicalIRBApproval.submit();
}

function fnTrackEvent(){

if(TRIM(document.frmClinicalIRBApproval.irbid.value).length==0){
	Error_Clear();
	Error_Details("Cannot Load Track Event. Please Load the IRB Approval First.");				
	Error_Show();
	Error_Clear();
}
else{	
	document.frmClinicalIRBApproval.strOpt.value = "load_irb_event";
	document.frmClinicalIRBApproval.action="/gmClinicalIRBEventAction.do";
	document.frmClinicalIRBApproval.submit();
}

}
function fnResetAppr(){
	document.frmClinicalIRBApproval.irbid.value="";
	document.frmClinicalIRBApproval.studySiteID.value="0";
	document.frmClinicalIRBApproval.approvalType.value="0";
	document.frmClinicalIRBApproval.approvalReason.value="0";
	document.getElementById("statusdiv").style.display="none";
	document.frmClinicalIRBApproval.txt_LogReason.value="";
	document.frmClinicalIRBApproval.strOpt.value = "list_irb";
	document.frmClinicalIRBApproval.submit();
}

function fnVoidAppr()
{
		document.frmClinicalIRBApproval.action ="/GmCommonCancelServlet";
		document.frmClinicalIRBApproval.hTxnId.value = document.frmClinicalIRBApproval.irbid.value;	
		document.frmClinicalIRBApproval.hTxnName.value = document.frmClinicalIRBApproval.approvalType.options[document.frmClinicalIRBApproval.approvalType.selectedIndex].text;
		document.frmClinicalIRBApproval.hCancelType.value = 'VDIRT';
		document.frmClinicalIRBApproval.hAction.value = 'Load';
		document.frmClinicalIRBApproval.hRedirectURL.value = '/gmClinicalIRBApprovalAction.do?strOpt=onLoad';
		document.frmClinicalIRBApproval.hAddnlParam.value = '';
		document.frmClinicalIRBApproval.hDisplayNm.value = 'IRB Approval Setup';
		document.frmClinicalIRBApproval.submit();
}

var optRemoved='';
function fnCheckAppr(){
	if(document.frmClinicalIRBApproval.approvalType.value==60522){ // Advertisement
	
	  var apprReason = document.getElementById('approvalReason');
	  for (i =0; i< apprReason.length ;  i++) {
	    if (apprReason.options[i].value== 60525) { // Renewal is not applicable for Advertisement
	      apprReason.remove(i);
	      optRemoved = 'true';
	    }
	  }
	 }	else if(document.frmClinicalIRBApproval.approvalType.value==60523){
		  var apprReason = document.getElementById('approvalReason');
		  var len = apprReason.length;
		  for (i =0; i<apprReason.length;  i++) {
			  //alert(apprReason.length+ ' '+apprReason.options[i].value);	
			  if(apprReason.options[i].value != 60524 && apprReason.options[i].value != 0 )
		      {  
				  //alert(apprReason.length+ ' '+apprReason.options[i].value);
				    apprReason.remove(i);
				    i--;
					optRemoved = 'true';
			  }
			  
		  }		  
	}else{ if(optRemoved =='true'){
	 		var elSel = document.getElementById('approvalReason');
	 		var elOptNew = document.createElement('option');
	 		var elOptNew1 = document.createElement('option');
	 		var elOptOld;
			  if (elSel.selectedIndex >= 0) {
			    //var elOptNew = document.createElement('option');
			    elOptNew.text = 'Renewal';
			    elOptNew.value = '60525';
			    elOptOld = elSel.options[2];
			    elOptNew1.text = 'Revision';
			    elOptNew1.value = '60526';
			    elOptOld = elSel.options[3];
			    try {
			      elSel.add(elOptNew, elOptOld); // standards compliant; doesn't work in IE
			    }
			    catch(ex) {
			      elSel.add(elOptNew, 2); // IE only
			      elSel.add(elOptNew1,3);
			    }			  
			  }
			  optRemoved='';
		}	
		} 
}
function toggleMedia(){
	if(status=='Approved'&& document.frmClinicalIRBApproval.approvalType.value=="60522"){
		document.getElementById("mediainfo").style.display = 'block';
	}
	else{
		document.getElementById("mediainfo").style.display = 'none';
	}
}
function fnLoadMedia(){
	
	 document.frmClinicalIRBApproval.strOpt.value="load_irb_media";
	 document.frmClinicalIRBApproval.action="/gmClinicalIRBMediaAction.do";
	 document.frmClinicalIRBApproval.submit();
	
}
</script>

<BODY leftmargin="0" topmargin="0" onLoad="javascript:fnOnPageLoad()">
<html:form  action="/gmClinicalIRBApprovalAction.do">
<html:hidden property="strOpt" />
<html:hidden property="irbid"  />
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value=""/>
<html:hidden property="hAction" value=""/>
<html:hidden property="hRedirectURL" value=""/>
<html:hidden property="hDisplayNm" value=""/>
<html:hidden property="hAddnlParam" value=""/>

	<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">IRB Approval Setup</td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td width="848" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="evenshade">
						<td class="RightText" align="right" HEIGHT="30" width="30%"><font color="red">*</font>&nbsp;Site List:</td>						
							<td>&nbsp;<gmjsp:dropdown controlName="studySiteID"  SFFormName="frmClinicalIRBApproval" SFSeletedValue="studySiteID"
								SFValue="alSiteList" width="250" codeId="ID" codeName="NAME" onChange="javascript:fnReload();" defaultValue="[Choose One]"/> &nbsp;&nbsp;							
            			</td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="oddshade">
						<td class="RightText" align="right" HEIGHT="30" width="30%"><font color="red">*</font>&nbsp;Appr Type:</td>						
							<td>&nbsp;<gmjsp:dropdown  controlName="approvalType"  SFFormName="frmClinicalIRBApproval" SFSeletedValue="approvalType"
							SFValue="alApprovalTypeList" width="250" codeId="CODEID" codeName="CODENM" onChange="javascript:fnCheckAppr();toggleMedia();" defaultValue="[Choose One]"/> &nbsp;&nbsp;							
            			</td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr class="evenshade">
						<td class="RightText" align="right" HEIGHT="30" width="30%"><font color="red">*</font>&nbsp;Appr Reason:</td>						
							<td>&nbsp;<gmjsp:dropdown controlId="approvalReason" controlName="approvalReason"  SFFormName="frmClinicalIRBApproval" SFSeletedValue="approvalReason"
							SFValue="alApprovalReasonList" width="250" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/> &nbsp;&nbsp;							
            			</td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
										<tr class="oddshade">
						<td class="RightText" align="right" HEIGHT="30" width="30%">&nbsp;Status:</td>						
							<td id="statusdiv">&nbsp;<%=statusName%> &nbsp;&nbsp;
            			</td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>	
					<tr><td colspan="2"> 
					<jsp:include page="/common/GmIncludeLog.jsp" >
				<jsp:param name="LogType" value="" />
				<jsp:param name="FORMNAME" value="frmClinicalIRBApproval" />
				<jsp:param name="ALNAME" value="alLogReasons" />
				<jsp:param name="LogMode" value="Edit" />
				</jsp:include>
				</td></tr>	
				</table>
				<table border="0" bordercolor="blue" width="100%" cellspacing="0" cellpadding="0">
				<tr>
				<td width="20%" >&nbsp;</td>
				<td width="25%" align="right">
				<gmjsp:button controlId="subbtn" value="Submit" gmClass="button" buttonType="Save" onClick="javascript:fnSubmit();" disabled="<%=lockedStudyDisable%>"/>
				&nbsp;&nbsp;&nbsp;<gmjsp:button value="Reset" gmClass="button" buttonType="Save" onClick="javascript:fnResetAppr();"/>
				</td><td width="30%"  align="left">
				<div id="showbutton">
				&nbsp;&nbsp;&nbsp;<gmjsp:button value="Void" gmClass="button" buttonType="Save" onClick="javascript:fnVoidAppr();" disabled="<%=lockedStudyDisable%>"/>
				&nbsp;&nbsp;&nbsp;<gmjsp:button value="Track Event" gmClass="button" buttonType="Save" onClick="javascript:fnTrackEvent();" disabled="<%=lockedStudyDisable%>"/>
				</div>
				</td>
				<td width="30%" align="left"  >
				<div id="mediainfo" style="display:none;">
				<gmjsp:button value="Enter Media Info" gmClass="button" buttonType="Save" onClick="javascript:fnLoadMedia();" disabled="<%=lockedStudyDisable%>"/>
				</div>
				</td>
				</tr>				
    </table>		     
      		  <table>	  
			<tr> 
				<td colspan="2">
			    	<div id="irbapproval" style="grid" height="150px"></div>
				</td>	
    	 	</tr> 	  	
    	</table>	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

