
<%
	/**********************************************************************************
	 * File		 		: GmClinicalStudyListRpt.jsp
	 * Desc		 		: This is a report screen for Clinical Study List
	 * Version	 		: 1.0
	 * author			: 
	 ************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<bean:define id="gridData" name="frmClinicalStudyLockForm" property="gridXmlData" type="java.lang.String"></bean:define>
<bean:define id="screenName" name="frmClinicalStudyLockForm" property="screenName" type="java.lang.String"></bean:define>
<HTML>
<HEAD>
<TITLE>Globus Medical: Study List Report</TITLE>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">

<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalStudyLock.js"></script>
<script>
	var objGridData = '';
	objGridData = '<%=gridData%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="20" onLoad="fnOnPageLoad();">
<html:form action="/gmClinicalStudyLockAction.do?method=loadStudyListRpt">
<html:hidden property="strOpt" value=""/>
		<table border="0" cellspacing="0" cellpadding="0" class="DtTable950">
			<tr style="height: 25px">
				<td style='height: 25px' class="Header" colspan="4">&nbsp;<%=screenName%></td>
			</tr>
			<%
				if (gridData.indexOf("cell") != -1) {
			%>
			<tr>
				<td class="LLine" height="4"></td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="dataGridDiv" class="grid" style="height: 383px;"></div></td>
			</tr>
			<tr height="20">
				<td colspan="4" align="center">
					<div class='exportlinks'>
						Export Options : <img src='img/ico_file_excel.png' />&nbsp;<a
							href="#" onclick="fnDownloadXLS();">Excel</a>
					</div></td>
			</tr>
			<%}else{ %>
			<tr>
				<td colspan="4" class="LLine" height="1"></td>
			</tr>
			<tr height="30">
				<td colspan="4" class="RightText" align="center">No data found.</td>
			</tr>
			<%}
			
			if(screenName.equals("Study History Report")){%>
			<tr>
				<td colspan="4" class="LLine" height="1"></td>
			</tr>
			<tr height="30">
				<td colspan="4" class="RightText" align="center"><gmjsp:button gmClass="button" buttonType="Load" name="btn_Close" value="Close" onClick="window.close();"/> </td>
			</tr>
			
			<%} %>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>