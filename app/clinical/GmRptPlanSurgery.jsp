<%
/**********************************************************************************
 * File		 		: GmRptPlanSurgery.jsp
 * Desc		 		: Report for the Planned Surgerys By Date
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>



<!-- clinical\GmRptPlanSurgery.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<bean:define id="gridData" name="frmRptSurgeryByDate" property="gridXmlData" type="java.lang.String"> </bean:define>

<% 
String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
	String strhAction = (String)request.getAttribute("hAction");
	String strHeight="459px";
	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}
	String strSiteId = GmCommonClass.parseNull((String)session.getAttribute("strSessSiteId"));
	String strSiteIds = GmCommonClass.parseNull((String)request.getParameter("strSiteIds"));
	if(strSiteId.equals(""))
	{
		strHeight = "69%";
	}
	
	// PMT-39479: Amnios study label changes
	String strStudyId = GmCommonClass.parseNull((String)session.getAttribute("strSessStudyId"));
	String strTitleHeader = "Enrollment by Surgery Date";
	if( strStudyId. equals("GPR009")){
		strTitleHeader = "Enrollment by Injection Date";
	}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Planned Surgery Date </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmRptPlanSurgery.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmClinicalCommonScript.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script>
	objGridData = '<%=gridData%>';
	document.onkeypress = function(){
		if(event.keyCode == 13){
			fnGo();
		}
		
	}	

	//
	var studyIdVal = '<%=strStudyId%>';
</script>
</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();fnCheckAll();">
<html:form action="/gmRptSurgeryByDate.do">
<html:hidden property="strOpt" />
	<table border="0"  cellspacing="0" cellpadding="0" class="DtTable765">
		<tr>
			<td height="25" class="Header">&nbsp;<%=strTitleHeader %></td>
		</tr>
		<tr>
			<td valign="top">
				<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
                    <tr><td colspan="2" class="ELine"></td></tr> 
                    <tr>
						<td colspan="2" align="center">
						   	<jsp:include page="/clinical/GmStudyFilterInc.jsp" >
							<jsp:param name="FORMNAME" value="frmRptSurgeryByDate" />	
							<jsp:param name="HIDELOADBUTTON" value="true" />												
							</jsp:include>	
						</td>
					</tr>
			        <tr>
			             <td colspan="2">
			             	<table cellpadding="0" cellspacing="0" border="0">
					<!-- Custom tag lib code modified for JBOSS migration changes -->
			             		<tr>
						             <td class="RightTableCaption" align="right" width="70"></font>&nbsp;Treatment :</td>   
							         <td height="30">&nbsp;<gmjsp:dropdown controlName="cbo_Treatment"  SFFormName='frmRptSurgeryByDate' SFSeletedValue="cbo_Treatment"
								       	SFValue="alTreatment" codeId="CODEID" codeName="CODENM" defaultValue="[ALL]"/>
								     </td>  	       
								     <td class="RightTableCaption" align="right" ></font>&nbsp;Status :</td> 
								     <td width="328">&nbsp;<gmjsp:dropdown controlName="cbo_Status"  SFFormName='frmRptSurgeryByDate' SFSeletedValue="cbo_Status"
							       	SFValue="alStatus"  codeId="CODEID" codeName="CODENM" defaultValue="[ALL]"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<gmjsp:button value="&nbsp;&nbsp;Load&nbsp;&nbsp;" onClick="javascript:fnGo();" gmClass="button" buttonType="Load" />
							       	<td>							       	
						       	</tr>
						   </table>
						</td>
				     </tr>				     
		            <tr><td colspan="2" bgcolor="#cccccc"></td></tr> 
      				<tr> 
			            <td colspan="2">
			           		<div id="dataGridDiv" class="grid"></div>
						</td>
					</tr>
					  <tr colspan = "2" class = "oddshade">
						<td class = "oddshade" colspan="3" align="center" height="30">	
							<div >Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!-- | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a> -->
                     	</td>
					  </tr>	
			   	</table>
  			   </td>
  		  </tr>				
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

