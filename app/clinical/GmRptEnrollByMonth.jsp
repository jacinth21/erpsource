<%
/**********************************************************************************
 * File		 		: GmRptEnrollByMonth.jsp
 * Desc		 		: Report for the enrolled patients by month
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>


<!-- clinical\GmRptEnrollByMonth.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>


<bean:define id="hmReport" name="frmRptEnrollByMonth" property="hmRptEnrollByMonth" type="java.util.HashMap"> </bean:define>

<% 	
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	ArrayList alStyle 		= new ArrayList();
	//log.debug(" Size of hmReport in the beginning " + hmReport.size());

	ArrayList alDrillDown 	= new ArrayList();
	

	String strhAction = (String)request.getAttribute("hAction");
	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}
	
		
	// To Generate the Crosstab report
	gmCrossTab.setHeader("Enrollment By Month");
	
	gmCrossTab.setLinkType(strhAction);
	gmCrossTab.setLinkRequired(true);	// To Specify the link
	
	
	// To specify if its unit or valye 
	gmCrossTab.setValueType(0);

	// Setting Display Parameter
	gmCrossTab.setAmountWidth(91);
	gmCrossTab.setNameWidth(50);
	gmCrossTab.setRowHeight(22);
	
	// Page Body Style
	// Setting the cross tab style 
	// Page Body Style
	gmCrossTab.setGeneralHeaderStyle("aaTopHeaderLightG");
	gmCrossTab.addStyle("Name","ShadeLevel2NoBold","aaTopHeaderLightG") ;
	gmCrossTab.addStyle("Site Name","ShadeLevel2NoBold","aaTopHeaderLightG") ;
	gmCrossTab.addStyle("Principal Investigator","ShadeLevel2NoBold","aaTopHeaderLightG") ;
	gmCrossTab.addStyle("Total","ShadeLevel2NoBoldRight","aaTopHeaderLightG") ;
		gmCrossTab.setRowHighlightRequired(true);
	


// Line Style information
	gmCrossTab.setColumnDivider(false);
	gmCrossTab.setDivHeight(500);
	gmCrossTab.setColumnLineStyle("borderDark");

// Rename Columns		
	gmCrossTab.reNameColumn("Name","Site ID");

// Add Lines
	gmCrossTab.addLine("ID");
	gmCrossTab.addLine("Name");
	gmCrossTab.addLine("Site Name");
	gmCrossTab.addLine("Prinicipal Investigator");
	gmCrossTab.addLine("Total");

// set Column Width
    gmCrossTab.setColumnWidth("Site Name",250);
    gmCrossTab.setColumnWidth("Prinicipal Investigator",200);
    
// Adding Drilldown
alDrillDown.add("Account");
gmCrossTab.setDrillDownDetails(alDrillDown);  

gmCrossTab.setExport(true, pageContext, "/gmRptEnrollByMonth.do?method=reportEnrollSiteByMonth", "RptEnrollByMonthReport");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Enrollment by month </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>



<script>
document.onkeypress = function(){
		if(event.keyCode == 13){
			fnGo();
		}
		
	}
function fnGo()
{
	document.frmRptEnrollByMonth.action = "/gmRptEnrollByMonth.do?method=reportEnrollSiteByMonth";
	document.frmRptEnrollByMonth.strOpt.value = "Report";
	fnStartProgress();
	document.frmRptEnrollByMonth.submit();
}	

function fnCallAccount(id)
{
    document.frmRptEnrollByMonth.action = "/gmRptSurgeryByDate.do?method=reportSurgeryByDate";
	document.frmRptEnrollByMonth.strOpt.value = "Report";
	document.frmRptEnrollByMonth.siteId.value = id;
	document.frmRptEnrollByMonth.submit();
}
</script>

</HEAD>

<BODY leftmargin="0" topmargin="0" >
<html:form action="/gmRptEnrollByMonth.do"  >
<html:hidden property="strOpt" />
<html:hidden property="siteId" value="" />
<html:hidden property="hExcel" value="Excel" />

	<table class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">Enrollment by Month</td>
		</tr>
		<tr>
			<td valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0" bordercolor="red">
                    <%-- 
                    <tr>
						<td colspan="2" align="center">&nbsp;
	                    	<jsp:include page="/clinical/GmStudyFilterInc.jsp" >
							<jsp:param name="FORMNAME" value="frmRptEnrollByMonth" />
							<jsp:param name="ENABLESITELIST" value="false" />							
							</jsp:include>	
						</td>
					</tr>
					--%>
					 
					<tr>
						<td colspan="2" align="center">
               			    <jsp:include page="/include/GmSFFromToDate.jsp" >   
						    <jsp:param name="FORMNAME" value="frmRptEnrollByMonth" />
						    </jsp:include>								
						</td>
					</tr>
			   	</table>
  			   </td>
  		  </tr>	
  		 
  		
    </table>		     	
<table class="DtTable765">
<% if(!hmReport.isEmpty()) { 
%>    
					
					<tr>
						<td colspan ="2" align=center> <%=gmCrossTab.PrintCrossTabReport(hmReport) %></td>
					</tr>
<% }%>					    
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>

