<%
/**********************************************************************************
 * File		 		: gmRptOutcomeScoresScore.jsp
 * Desc		 		: Report for Outcomescore
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>

<!-- clinical\GmRptOutComeScore.jsp -->
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ include file="/common/GmHeader.inc" %>

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
					
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>

<bean:define id="hmCrossTabReport" name="frmOutcomeReportVAS" property="hmCrossTabReport" type="java.util.HashMap"> </bean:define>
<bean:define id="strScore" name="frmOutcomeReportVAS" property="score" type="java.lang.String"> </bean:define>
<bean:define id="strTypeId" name="frmOutcomeReportVAS" property="typeId" type="java.lang.String"> </bean:define>
<bean:define id="strSessStudyId" name="frmOutcomeReportVAS" property="sessStudyId" type="java.lang.String"> </bean:define>
<%
	//Logger log = GmLogger.getInstance(GmCommonConstants.CLINICAL);  // Instantiating the Logger
   	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
   	//String strCssPath = GmCommonClass.getString("GMSTYLES");
   	ArrayList alHeader = null;
   	try{
	ArrayList alDrillDown = new ArrayList();
	
	// PMT-43699: Amnios study - other outcome report changes
	// if study Amnios then, To round and show single digit values 
	String strAddRoudOff = "0";
	
	//PC-1809: Reflect To add the single digit values for all the column
	if(strSessStudyId.equals ("GPR009") || strSessStudyId.equals ("GPR010")){
		strAddRoudOff = "1";
	}
%>
<%
	if(!strTypeId.equals("61107") && !strTypeId.equals("61108") && !strTypeId.equals("61109")){
		alHeader = GmCommonClass.parseNullArrayList((ArrayList)hmCrossTabReport.get("Header"));
		int headerSize = alHeader.size();
		for (int i = 0;i<headerSize; i+=2)
		{
			gmCrossTab.addLine((String)alHeader.get(i));
		} 
	
		for (int i = 1,j=3;j<headerSize; i+=3,j+=3)
		{
			gmCrossTab.add_RoundOff((String)alHeader.get(i), strAddRoudOff);
			gmCrossTab.add_RoundOff((String)alHeader.get(j),"1");
		}
	}
	%>
<%if(strScore.equals("60922") && strTypeId.equals("61109")) {
	
	alHeader = GmCommonClass.parseNullArrayList((ArrayList)hmCrossTabReport.get("Header"));
	 int headerSize = alHeader.size();
	for (int i = 1;i<headerSize; i+=2)
	{
		gmCrossTab.addLine((String)alHeader.get(i));
		gmCrossTab.addStyle((String)alHeader.get(i),"RightTextSmallCR","aaTopHeaderLightG");
		gmCrossTab.add_RoundOff((String)alHeader.get(i),"0");
		gmCrossTab.addStyle("Name","ShadeLevel2NoBold","aaTopHeaderLightG") ;
		gmCrossTab.addStyle("Treatment","ShadeLevel2NoBold","aaTopHeaderLightG") ;
		gmCrossTab.reNameColumn("Name","Patient ID");

	}
}
%>
<%
	if(strScore.equals("60922") && strTypeId.equals("61108")) {
		alHeader = GmCommonClass.parseNullArrayList((ArrayList)hmCrossTabReport.get("Header"));
		int headerSize = alHeader.size();
		for (int i = 0;i<headerSize; i+=3)
		{
			gmCrossTab.addLine((String)alHeader.get(i));
		} 
		
		for (int i = 1,j=3;j<headerSize; i+=3,j+=3)
		{
			gmCrossTab.add_RoundOff((String)alHeader.get(i),"0");
			gmCrossTab.add_RoundOff((String)alHeader.get(j),"1");
		} 
		gmCrossTab.setLinkRequired(true);
		alDrillDown.add("LoanerDist");	
		gmCrossTab.setDrillDownDetails(alDrillDown);
	}
%>
<%
	if(strScore.equals("60922") && strTypeId.equals("61107")) {
		alHeader = GmCommonClass.parseNullArrayList((ArrayList)hmCrossTabReport.get("Header"));
		int headerSize = alHeader.size();
		for (int i = 0;i<headerSize; i+=2)
		{
			gmCrossTab.addLine((String)alHeader.get(i));
			log.debug(" (String)alHeader.get(i) " +(String)alHeader.get(i));
		} 
	}
%>
<%	


	// Setting the cross tab style 
	// Page Body Style
	gmCrossTab.setGeneralHeaderStyle("aaTopHeaderLightG");
	gmCrossTab.setDivHeight(200);
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setGroupingRequired(true);
	gmCrossTab.setValueType(1);// Setting up the value to Round off with one decimal place
	gmCrossTab.setNoDivRequired(true); // this skips division for all columns
	gmCrossTab.addLine("Name");
	gmCrossTab.setNameWidth(140);
	gmCrossTab.setRowHighlightRequired(true);
	
		// Line Style information
	//gmCrossTab.setColumnDivider(false);
	gmCrossTab.setColumnLineStyle("borderDark");
	
	gmCrossTab.displayZeros(true);
	// removed method in below code. Called existing method for Acadia. 
	gmCrossTab.setExport(true, pageContext, "/gmRptOutcomeScores.do", "RptOutComeScore");
%>



<HTML>
<HEAD>
<TITLE> Globus Medical: OutcomeReport </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
document.onkeypress = function(){
		if(event.keyCode == 13){
			fnReport();
		}
		
	}
function fnSubmit()
{
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmOutcomeReportVAS.strOpt.value = "Report";
	fnStartProgress();
	document.frmOutcomeReportVAS.submit();
}
function fnLoad()
{
	document.frmOutcomeReportVAS.selectAll.checked = false;
	document.frmOutcomeReportVAS.strOpt.value = "";
	document.frmOutcomeReportVAS.action = "/gmRptOutcomeScores.do?method=reportOutComeScore";
	document.frmOutcomeReportVAS.submit();
}

function fnReport()
{
	fnValidateDropDn('typeId',' Type ');
	
	objSelect = eval("document.frmOutcomeReportVAS.typeId");
	objSelectVal = objSelect[objSelect.selectedIndex].value;
	
	if(objSelectVal == 61107 || objSelectVal == 61108 || objSelectVal == 61109)
		fnValidateDropDn('score',' Score ');
	
	if(objSelectVal == 61107) //Oswestry Disability Index (ODI) 
		document.frmOutcomeReportVAS.action = "/gmRptOutcomeScores.do?method=reportNDIScore&score=60922"; // 60922: Acadia Score
	else if(objSelectVal == 61108) //ODI >=25 point Impr
		document.frmOutcomeReportVAS.action = "/gmRptOutcomeScores.do?method=reportNDIPerImprove&score=60922";
	else if(objSelectVal == 61109) //ODI % Impr Detail
		document.frmOutcomeReportVAS.action = "/gmRptOutcomeScores.do?method=reportNDIImproveDetail&score=60922";
	else	
		document.frmOutcomeReportVAS.action = "/gmRptOutcomeScores.do?method=reportOutComeScore";
	fnSubmit();
}	

function fnLoadType()
{
	objSelect = eval("document.frmOutcomeReportVAS.typeId");
	objSelectVal = objSelect[objSelect.selectedIndex].value;
	fnDisplayScoreDiv();
	//document.frmOutcomeReport.selectAll.checked = false;
	if(objSelectVal == 61107 || objSelectVal == 61108 || objSelectVal == 61109){
		document.frmOutcomeReportVAS.strOpt.value = "";
		document.frmOutcomeReportVAS.action = "/gmRptOutcomeScores.do?method=reportOutComeScore&score=60922";
		document.frmOutcomeReportVAS.submit();
	}
}
function fnOnLoad(){
	fnDisplayScoreDiv();
	fnCheckAll();
}
function fnDisplayScoreDiv(){
	objSelect = eval("document.frmOutcomeReportVAS.typeId");
	objSelectVal = objSelect[objSelect.selectedIndex].value;
	if(objSelectVal == 61107 || objSelectVal == 61108 || objSelectVal == 61109){
		document.getElementById("DispScore").style.display='inline';
	} else{
		document.getElementById("DispScore").style.display='none';
	}	
}
</script>

</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="javascript:fnOnLoad();" >
<html:form action="/gmRptOutcomeScores.do"  >
<html:hidden property="strOpt" />

	<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">Other Outcomes</td>
		</tr>
		<tr>
			<td width="1100" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
						<td colspan="2" align="center">&nbsp;
	                    	<jsp:include page="/clinical/GmStudyFilterInc.jsp" >
							<jsp:param name="FORMNAME" value="frmOutcomeReportVAS" />
							<jsp:param name="HIDELOADBUTTON" value="HIDE" />
							</jsp:include>	
						</td>
					</tr>	
					<tr><td height="5"></td></tr>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
						<td class="RightTableCaption" align="right" height="30" width="80" nowrap>Type :</td> 
    	                <td class="RightTableCaption"width="100%" >&nbsp;<gmjsp:dropdown controlName="typeId" SFFormName="frmOutcomeReportVAS" SFSeletedValue="typeId"
								SFValue="alType" codeId = "CODENMALT"  codeName = "CODENM" defaultValue= "[Choose One]"  onChange="fnLoadType();"/>
																				
    		            &nbsp;&nbsp;&nbsp;&nbsp;<div id="DispScore" style="display:inline"><b>Scores :</b>&nbsp;
    		            <gmjsp:dropdown controlName="score"  SFFormName='frmOutcomeReportVAS' SFSeletedValue="score"
						      			SFValue="alScore"  codeId="CODENMALT" codeName="CODENM" defaultValue="[Choose One]"/>
										      				      			
  		            <%if(strScore.equals("60922") && strTypeId.equals("61109")){%>						      			
							&nbsp;&nbsp;&nbsp;&nbsp;Treatment :   
                  	        &nbsp;<gmjsp:dropdown controlName="treatmentId" SFFormName="frmOutcomeReportVAS" SFSeletedValue="treatmentId"
						        		SFValue="alTreatment" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[All]"  />
						<%	}%></div>
				      	&nbsp;&nbsp;<gmjsp:button value="Load" gmClass="button" buttonType="Load" onClick="fnReport();"  />		
					</td>
    		            
					</tr>
                    <tr><td colspan="2" class="ELine"></td></tr> 
					<tr>
						<td colspan ="2" align=center> <%=gmCrossTab.PrintCrossTabReport(hmCrossTabReport) %></td>
					</tr>
			   	</table>
  			   </td>
  		  </tr>	
  		
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
<% } catch(Exception exp) {
exp.printStackTrace();
}
%>
</BODY>

</HTML>

