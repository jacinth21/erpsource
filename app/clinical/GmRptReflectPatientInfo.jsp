
<%
	/**********************************************************************************
	 * File		 		: GmRptReflectPatientInfo.jsp
	 * Desc		 		: Demographic Report for the Patient Information
	 * Version	 		: 1.0
	 * author			: mmuthusamy
	 ************************************************************************************/
%>

<!-- clinical\GmRptReflectPatientInfo.jsp -->
<!-- WEB-INF path corrected for JBOSS migration changes -->


<%@ include file="/common/GmHeader.inc"%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel"%>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel"%>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.beans.GmCrossTabFormat"%>

<%
	String strhAction = (String) request.getAttribute("hAction");
	if (strhAction == null) {
		strhAction = (String) session.getAttribute("hAction");
	}
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Reflect Patient Data</TITLE>


<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmRptAmniosPatientInfo.js"></script>


<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>

</HEAD>
<%
	// to set the SD deviation
	String ageSDTitle = "Age &nbsp;&nbsp;<br>(Yrs) <br>" + (char) 177 + "SD";
	String heightSDTitle = "Height <br>(Inches) <br>" + (char) 177 + "SD";
	String weightSDTitle = "Weight <br>(lbs) <br>" + (char) 177 + "SD";
%>
<BODY leftmargin="20" topmargin="10">
	<html:form action="/gmRptDgraph.do">
		<html:hidden property="strOpt" />

		<table border="0" class="DtTable1200" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="Header">Patient Data</td>
			</tr>
			<tr>
				<td valign="top">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td colspan="2" class="ELine"></td>
						</tr>
						<tr>
							<td colspan="2" align="center"><jsp:include
									page="/clinical/GmStudyFilterInc.jsp">
									<jsp:param name="FORMNAME" value="frmDgraphReport" />
								</jsp:include></td>
						</tr>
						<tr>
							<td height="5"></td>
						</tr>
						<tr>
							<td colspan="2" align=center><display:table
									name="requestScope.frmDgraphReport.ldtResult"
									style="width=75%;" cellpadding="0" cellspacing="0"
									requestURI="/gmRptDgraph.do?method=reportDgraph" export="true"
									decorator="com.globus.displaytag.beans.DTRowHighlightWrapper">							    								\
								<display:column headerClass="Empty" class="Empty"
										style="width:1;" />
									<display:column headerClass="Line" class="Line"
										style="width:1;" media="html"></display:column>
									<display:column property="TREATMENT" title="Treatment"
										headerClass="aaTopHeaderLightG" />
									<display:column headerClass="Line" class="Line"
										style="width:1;" media="html" />
									<display:column property="MALENO" title="Male &nbsp;&nbsp;<br> n"
										style="text-align:center;" headerClass="aaTopHeaderLightG"  />
									<display:column property="MALEPER" title="Male &nbsp;&nbsp;<br> %"
										style="text-align:right" headerClass="aaTopHeaderLightG" />
									<display:column headerClass="Line" class="Line"
										style="width:1;" media="html" />
									<display:column property="FEMALENO" title="Female &nbsp;&nbsp;<br> n"
										style="text-align:right" headerClass="aaTopHeaderLightG" />
									<display:column property="FEMALEPER" title="Female &nbsp;&nbsp;<br> %"
										style="text-align:right" headerClass="aaTopHeaderLightG" />
									<display:column headerClass="Line" class="Line"
										style="width:1;" media="html" />
									<display:column property="AGEAVG"
										title="Age &nbsp;&nbsp;<br>(Yrs) <br> Avg" style="text-align:right"
										headerClass="aaTopHeaderLightG" />
									<display:column property="AGESD" title="<%=ageSDTitle%>"
										style="text-align:right" headerClass="aaTopHeaderLightG" />
									<display:column headerClass="Line" class="Line"
										style="width:1;" media="html" />
									<display:column property="HEIGHTAVG"
										title="Height <br> (Inches) <br> Avg" style="text-align:right"
										headerClass="aaTopHeaderLightG" />
									<display:column property="HEIGHTSD" title="<%=heightSDTitle%>"
										style="text-align:right" headerClass="aaTopHeaderLightG" />
									<display:column headerClass="Line" class="Line"
										style="width:1;" media="html" />
									<display:column property="WEIGHTAVG"
										title="Weight <br> (lbs)<br> Avg" style="text-align:right"
										headerClass="aaTopHeaderLightG" />
									<display:column property="WEIGHTSD" title="<%=weightSDTitle%>"
										style="text-align:right" headerClass="aaTopHeaderLightG" />
									<display:column headerClass="Line" class="Line"
										style="width:1;" media="html" />
									<display:column property="RACE_ASIAN_NO"
										title="Race Asian<br> n " style="text-align:right"
										headerClass="aaTopHeaderLightG" />
									<display:column property="RACE_ASIAN_PER"
										title="Race Asian<br> %" style="text-align:right"
										headerClass="aaTopHeaderLightG" />
									<display:column headerClass="Line" class="Line"
										style="width:1;" media="html" />
									<display:column property="RACE_BLACK_NO"
										title="Race Black<br> n " style="text-align:right"
										headerClass="aaTopHeaderLightG" />
									<display:column property="RACE_BLACK_PER"
										title="Race  Black<br> %" style="text-align:right"
										headerClass="aaTopHeaderLightG" />
									<display:column headerClass="Line" class="Line"
										style="width:1;" media="html" />
									<display:column property="RACE_CAU_NO"
										title="Race Caucasian<br> n " style="text-align:right"
										headerClass="aaTopHeaderLightG" />
									<display:column property="RACE_CAU_PER"
										title="Race Caucasian<br> %" style="text-align:right"
										headerClass="aaTopHeaderLightG" />
									<display:column headerClass="Line" class="Line"
										style="width:1;" media="html" />
									<display:column property="RACE_HIS_NO"
										title="Race Hispanic<br> n " style="text-align:right"
										headerClass="aaTopHeaderLightG" />
									<display:column property="RACE_HIS_PER"
										title="Race Hispanic<br> %" style="text-align:right"
										headerClass="aaTopHeaderLightG" />
									<display:column headerClass="Line" class="Line"
										style="width:1;" media="html" />
									<display:column property="RACE_OTHER_NO"
										title="Race Other<br> n " style="text-align:right"
										headerClass="aaTopHeaderLightG" />
									<display:column property="RACE_OTHER_PER"
										title="Race Other<br> %" style="text-align:right"
										headerClass="aaTopHeaderLightG" />
									<display:column headerClass="Line" class="Line"
										style="width:1;" media="html" />
									<display:footer media="html">
										<tr>
											<td class="Empty"></td>
											<td colspan=44 class="Line"></td>
										</tr>
									</display:footer>
								</display:table></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</html:form>

</BODY>

</HTML>

