 <%
/**********************************************************************************
 * File		 		: GmPatientDashBoard.jsp
 * Desc		 		: This screen used to view Patient Dashboard with current status
 *					  
 * Version	 		: 1.0
 * author			: Richardk
************************************************************************************/
%>


<!-- clinical\GmStudyDashBoard.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.clinical.data.GmStudyPeriodData"%>
<%@ page import ="com.globus.clinical.data.GmStudyFormData"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%
try {
	
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	

	String strStudyID	= (String)request.getAttribute("hStudyID");
	String strPatientID	= (String)request.getAttribute("hPatientID");
	String strPkeyID	= (String)request.getAttribute("hPKeyID");

	ArrayList alReturn = (ArrayList)request.getAttribute("hResult");
	ArrayList alPeriod = new ArrayList();

	int intSize = 0;
	
	int intPeriodSize=0;
	GmStudyFormData gmStudy ;
	GmStudyPeriodData gmPeriod ;
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: DashBoard</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>
function fnFormDetails(valFormID, valStudyPeriod, valStudyPhase)
{
	document.frmPatient.Cbo_Form.value = valFormID;
	document.frmPatient.hStPerId.value = valStudyPeriod;
	document.frmPatient.Cbo_Period.value = valStudyPhase;
	document.frmPatient.submit();
}


</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmPatient" method="POST" action="<%=strServletPath%>/GmFormDataEntryServlet">
<input type="hidden" name="hAction" value="LoadQues">
<input type="hidden" name="Cbo_Study" value="<%=strStudyID%>">
<input type="hidden" name="Cbo_Patient" value="<%=strPkeyID%>">
<input type="hidden" name="Cbo_Form" value="">
<input type="hidden" name="hStPerId" value="">
<input type="hidden" name="Cbo_Period" value="">

<table border="0" width="900" cellspacing="0" cellpadding="0">
<tr>
	<td bgcolor="#666666" width="1"></td>
	<td height="100" valign="top">
		<table  width="100%"  cellspacing="0" cellpadding="0">
			<tr><td class="Line" colspan="2"></td></tr>
			<tr><td colspan="2">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr class="shade" >
					<td height="30" align="center" class="RightText">
						<font size="+1">SECURE&trade;-C Cervical Artifical Disc - PIVOTAL STUDY</font><td></tr>
					<tr class="shade">
						<td height="30" align="center" class="RightText">
							<font size="+1">Patient Worksheet&nbsp;</font></td>
					</tr>
				</table>
			</td></tr>
			<tr><td class="Line" colspan="2"></td></tr>
			<tr><td Class="RightTableCaption" colspan="2" height="30">
				<table cellpadding="1" cellspacing="0" border="0" width="100%">
				<tr><td Class="RightTableCaption" width="140"> &nbsp;&nbsp; Protocol Number </td>
					<td Class="RightTableCaption" width="70"> <%=strStudyID%> </td>
					<td Class="RightTableCaption" width="130"> &nbsp;&nbsp; Patient IDE No.:  </td>
					<td Class="RightTableCaption" > &nbsp;&nbsp;<%=strPatientID%>  </td>				
				</tr></table>	
			</td></tr>
			<tr><td  >
			<table width="100%" cellspacing="0" cellpadding="0">
				<tr><td class="borderDark" colspan="11"></td></tr>
				<tr class="ShadeDarkBlueTD" height="20">
					<th  align="center" class="ShadeDarkGrayTD" style="border-right: 1px solid #7c816a;" width="150">&nbsp; Phase </th>				
					<th  align="center" width=250> Form Name</th>
					<th  align="center" width="100"> Earliest Date</th>					
					<th  align="center" width="100"> Expected Date</th>					
					<th  align="center" width="100"> Latest Date</th>					
					<th  align="center" width="100"> Entry Date</th>
					<th  align="center"	width="50"> Verifed Fl</th>
					<th  align="center"	width="140"> Verifed By</th>
					<th  align="center"	width="40"> Due </th>
					<th  align="center"	width="40"> Edit</th>
				</tr>
				
			
<%				intSize = alReturn.size();
				String strCurrentStudyID = "";
				String strPreviousStudyID = "";
				String strShade="";
			  	for (int j=0;j<intSize;j++)
			  	{
			  		gmPeriod = (GmStudyPeriodData)alReturn.get(j);
			  		strCurrentStudyID = gmPeriod.getPeriodDS();
			  		
			  		if(j%2!=0){
			  			strShade="shade";
			  		}else{
			  			strShade="";
			  		}
%>					
					
<% 					if(!strCurrentStudyID.equals(strPreviousStudyID)){
%>					
					<tr><td class="borderDark" colspan="11"></td></tr><tr class="<%=strShade%>">
					<td  class="ShadeMedGrayTD"  width=150 align="left" style="border-right: 1px solid #7c816a;">&nbsp;
						<%=gmPeriod.getPeriodDS()%>
					</td>
<%						strPreviousStudyID = strCurrentStudyID; 	
					} else {
%>					<tr><td style="background-color: #dee0d2;" colspan="11"></td></tr><tr class="<%=strShade%>">
					<td width=150 align="left" style="border-right: 1px solid #7c816a; background-color: #dee0d2; height: 20px;">&nbsp;
					</td>
<%					} 
%>					
					<td align="left" width="250" rowspan=2 height"30">&nbsp;<%=gmPeriod.getStudyFormDs()%> <%=gmPeriod.getFormName()%></td>
					<td align="center" width="100"> <%=gmPeriod.getEarliestDate()== null?"-":gmPeriod.getEarliestDate()%></td>
					<td align="center" width="100"> <b><%=gmPeriod.getExpectedDate()== null?"-":gmPeriod.getExpectedDate()%> </b></td>
					<td align="center" width="100"> <%=gmPeriod.getLatestDate() == null?"-":gmPeriod.getLatestDate()%></td>
					<td align="center" width="100"> <%=gmPeriod.getFormReceivedDate()== null?"-":gmPeriod.getFormReceivedDate()%></td>
					<td align="center" width="50">    
						<% if (gmPeriod.isFormVerifiedFlag())
						   { %>	<img id="imgEdit" src="<%=strImagePath%>/dot_green.gif"  />
						<% } else if (gmPeriod.getFormReceivedDate() != null) {%>  
							<img id="imgEdit" src="<%=strImagePath%>/dot_red.gif"  />
						<% } else {%>
							-
						<% }%>		
					</td>
					<td align="center" width="140"><%=gmPeriod.getFormVerifiedBy()== null?"-":gmPeriod.getFormVerifiedBy()%> </td>
					<td align="center" width="40"> 
					<%
					if (gmPeriod.isDueFlag())
					 { %>  <img id="imgEdit" src="<%=strImagePath%>/Past_Due.gif" title="Form Entry is past due"
	 						width="12" height="12" /> 
	 				<% } else {%>
	 					   
	 				<% }%>
	 				</td>
					<td align="center" width="40"> <img id="imgEdit" style="cursor:hand" 
	 						src="<%=strImagePath%>/edit_icon.gif" title="Click to Edit Patient Question Information" 
	 						width="14" height="14" onClick="javascript:fnFormDetails(<%=gmPeriod.getFormID()%>, <%=gmPeriod.getStudyPeriodID()%>, <%=gmPeriod.getStudyPhase()%> )"/> </td>
					</tr>
					
<% 					
			  	}	
%>			<tr><td class="borderDark" colspan="11"></td></tr>
		</table></td></tr>
	</table></td>
	<td bgcolor="#666666" width="1"></td></tr>
 </table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
