 <%
/**********************************************************************************
 * File		 		: GmClinicalTrainingSummary.jsp
 * Desc		 		: This screen displys Clinical Training Summary
 * Version	 		: 1.0
 * author			: Himanshu Patel
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<bean:define id="gridData" name="frmStudyFilterForm" property="gridXmlData" type="java.lang.String"> </bean:define>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	//String strWikiTitle = GmCommonClass.getWikiTitle("");
	String strSiteId = GmCommonClass.parseNull((String)session.getAttribute("strSessSiteId"));
	boolean showGrid=true;
	if(gridData!=null && gridData.indexOf("cell")==-1){
	   showGrid = false;	
	   log.debug("showGrid value is "+showGrid);
	}
	
	
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Enterprise Portal</TITLE>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalTrainingReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

<script>



	objGridData = '<%=gridData%>';
	document.onkeypress = function(){
		if(event.keyCode == 13){
			fnLoadReport();
		}
		
	}
</script>
</HEAD>

<BODY onLoad="fnOnPageLoad();">
<html:form action="/gmClinicalTrainingAction.do">
	<html:hidden property="strOpt" />
	<html:hidden property="haction" value=""/> 

	<table border="0"  class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">&nbsp;Training Record Summary</td>			
			<td rowspan="2" class="LLine" ></td>
		</tr>
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		<tr>
		<td  valign="top">
				<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<%
			if(strSiteId.equals(""))
			{
		%>	
			<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr height="25" class="evenshade">			
						<td class="RightTableCaption" align="right" >&nbsp;Site List:</td>
						<td colspan="3">&nbsp;
							<gmjsp:dropdown controlName="siteList"  SFFormName="frmStudyFilterForm" SFSeletedValue="siteList"
							SFValue="alSiteNameList" codeId="ID" codeName="NAME" defaultValue="[Choose One]"/>                 		                                        		
			            </td>
			            <td rowspan="4" class="LLine" width="1" ></td>
						<td rowspan="2" align="center"><gmjsp:button value="&nbsp;&nbsp;Load&nbsp;&nbsp;" gmClass="button" buttonType="Load" onClick="fnLoadReport();" /> </td>
					</tr>
				
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
			<%} %>	
					<tr height="25">	
						<td class="RightTableCaption" align="right">&nbsp;Training For:</td>
						<td>&nbsp;
							<gmjsp:dropdown controlName="trainingFor" onChange="fnLoadReport();" SFFormName="frmStudyFilterForm" SFSeletedValue="trainingFor"
							SFValue="alTrainingFor" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
						</td>
						<td class="RightTableCaption" align="right">&nbsp;Training Reason:</td>
						<td >&nbsp;
							<gmjsp:dropdown controlName="trainingReason"  SFFormName="frmStudyFilterForm" SFSeletedValue="trainingReason"
							SFValue="alTrainingReason" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>                 		                                        		
			            </td>
			            <%
						if(!strSiteId.equals(""))
						{
						%>
            			<td rowspan="4" class="LLine" width="1" ></td>
						<td rowspan="2" align="center"><gmjsp:button value="&nbsp;&nbsp;Load&nbsp;&nbsp;" gmClass="button" buttonType="Load" onClick="fnLoadReport();" /> </td>
						<%} %> 
					</tr>		
			        <tr><td colspan="4" class="LLine" height="1"></td></tr>  
        		</table>
		</td>  
		</tr>
		<tr> 
        	<td colspan="4">	          		
          		<%if(showGrid == false){%>
          		<div id="dataGridDiv" class="grid" style="height:10px;">	          		
          		</div>
          		<br><center><h4>No Data Found or refine filters and click on the Load button</h4></center><br>
          		<% } else {%>
          		<div id="dataGridDiv" class="grid" style="height:350px;">	          		
          		</div>
          		<% } %>
			</td>
		</tr> 	 
		<tr class="oddshade">
				<td  colspan="2" align="center" height="30">	
						<div class='exportlinks'>Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
	                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!-- | <img src='images/pdf_icon.gif' /> <a href="#"
	                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a> -->
	                    </div>
				</td>
		   </tr>
		  
    </table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
