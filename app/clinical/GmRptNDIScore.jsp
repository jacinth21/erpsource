<%
/**********************************************************************************
 * File		 		: gmRptNDIScore.jsp
 * Desc		 		: Report for Neck Disability INdex scores
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>


<!-- clinical\GmRptNDIScore.jsp -->
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ include file="/common/GmHeader.inc" %> 
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>

<bean:define id="hmCrossTabReport" name="frmOutcomeReport" property="hmCrossTabReport" type="java.util.HashMap"> </bean:define>

<%  
try{

	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	
	// to draw lines for every other second column
	ArrayList alHeader = GmCommonClass.parseNullArrayList((ArrayList)hmCrossTabReport.get("Header"));
	int headerSize = alHeader.size();
	for (int i = 0;i<headerSize; i+=2)
	{
		gmCrossTab.addLine((String)alHeader.get(i));
		log.debug(" (String)alHeader.get(i) " +(String)alHeader.get(i));
	} 
	
	// Setting the cross tab style 
	// Page Body Style
	gmCrossTab.setGeneralHeaderStyle("aaTopHeader");
	gmCrossTab.setDivHeight(200);
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setGroupingRequired(true);
	gmCrossTab.setValueType(0);
	gmCrossTab.setNoDivRequired(true); // this skips division for all columns
	gmCrossTab.setRoundOffAllColumnsPrecision("1");
	gmCrossTab.addLine("Name");
	gmCrossTab.setRowHighlightRequired(true);
	
		// Line Style information
//	gmCrossTab.setColumnDivider(false);
	gmCrossTab.setColumnLineStyle("borderDark");
	gmCrossTab.displayZeros(true);
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Neck Disability Index Score (NDI) </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>

function fnSubmit()
{
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmOutcomeReport.strOpt.value = 'save';
	
	fnStartProgress();
	document.frmOutcomeReport.submit();
}

function fnLoad()
{
	document.frmOutcomeReport.selectAll.checked = false;
	document.frmOutcomeReport.strOpt.value = "";
	document.frmOutcomeReport.action = "/gmRptOutcome.do?method=reportNDIScore";
	fnStartProgress();
	document.frmOutcomeReport.submit();
}

function fnReport()
{
	document.frmOutcomeReport.action = "/gmRptOutcome.do?method=reportNDIScore";
	document.frmOutcomeReport.strOpt.value = "Report";
	fnStartProgress();
	document.frmOutcomeReport.submit();
}	

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmRptOutcome.do"  >
<html:hidden property="strOpt" />


	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Neck Disability Index (NDI)</td>
		</tr>
		<tr>
			<td width="848" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
						<td colspan="2" align="center" height="30">&nbsp;
	                    	<jsp:include page="/clinical/GmStudyFilterInc.jsp" >
							<jsp:param name="FORMNAME" value="frmOutcomeReport" />
							</jsp:include>	
						</td>
					</tr>
                    <tr><td colspan="2" class="ELine"></td></tr> 
					<tr>
						<td colspan ="2" align=center> <%=gmCrossTab.PrintCrossTabReport(hmCrossTabReport) %></td>
					</tr>
			   	</table>
  			   </td>
  		  </tr>	
  		
    </table>		     	
</html:form>
<% } catch(Exception exp) {
exp.printStackTrace();
}
%>
</BODY>

</HTML>

