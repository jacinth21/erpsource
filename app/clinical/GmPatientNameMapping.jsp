

<%
/**********************************************************************************
 * File		 		: GmPatientNameMapping.jsp
 * Desc		 		: This screen is used for the Patient Name entry 
 * Version	 		: 1.0
 * author			: Arockia Prasath
************************************************************************************/
%>


<!-- clinical\GmPatientNameMapping.jsp-->


<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>

<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ include file="/common/GmHeader.inc"%>

<%
String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
%>

<HTML>
<HEAD>

<TITLE>Globus Medical: Patient Details</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmPatientNameMapping.js"></script>
<script>
document.onkeypress = function(){
		if(event.keyCode == 13){
			fnLoadPage();
		}
		
	}
</script>


</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoad();">
<form name="frmPatientNameMapping" method="post" action="/gmPatientNameMapping.do">
<html:hidden name="frmPatientNameMapping" property="strOpt"/>
<html:hidden name="frmPatientNameMapping" property="patientId"/>

<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
	<tr>
		<td height="25" class="Header">&nbsp;Patient Name Mapping</td>
		<td colspan="3" height="1" class="Header"></td>
	</tr>
	
	
	<tr class="evenshade">
		<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>Patient IDE No&nbsp;:</td>
		<td width="60%" align="left">&nbsp;<html:text name="frmPatientNameMapping" property="patientIde" size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" />
		<logic:equal name="frmPatientNameMapping" property="patientId" value="">
		&nbsp;<gmjsp:button value="&nbsp;Load&nbsp;" gmClass="button" buttonType="Load" onClick="fnLoadPage();"/>
		</logic:equal>
		</td>
	</tr>
	<tr><td colspan="2" height="1" class="LLine"></td></tr>
	<tr>
		<td>
		<logic:equal name="frmPatientNameMapping" property="patientId" value="">		
		&nbsp;No data found
		</logic:equal></td>
	</tr>
	
	
	<logic:notEqual name="frmPatientNameMapping" property="patientId" value="">
	
	<tr class="oddshade">
		<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>Patient First Name&nbsp;:</td>
		<td width="60%" align="left">&nbsp;<html:text name="frmPatientNameMapping" property="firstName" maxlength="100" size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" /></td>
	</tr>
	<tr><td colspan="2" height="1" class="LLine"></td></tr>
    <tr class="evenshade">
		<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>Patient Last Name&nbsp;:</td>
		<td width="60%" align="left">&nbsp;<html:text name="frmPatientNameMapping" property="lastName" maxlength="100" size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" /></td>
	</tr>
	
	<tr><td colspan="2" height="1" class="LLine"></td></tr>
	<tr class="oddshade">
		<td colspan="2" align="center" height="24">
		<gmjsp:button value="&nbsp;Submit&nbsp;" gmClass="button" buttonType="Save" onClick="fnSubmit();"/>
		<gmjsp:button value="&nbsp;Reset&nbsp;" gmClass="button" buttonType="Save" onClick="fnReset();"/>
		
		
	
		</td>
	</tr>
	</logic:notEqual>
</table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
