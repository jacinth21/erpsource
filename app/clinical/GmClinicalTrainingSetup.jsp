 <%
/**********************************************************************************
 * File		 		: GmClinicalTrainingSetup.jsp
 * Desc		 		: This is a report screen for Clinical Training setup
 * Version	 		: 1.0
 * author			: VKiran
************************************************************************************/
%>

<!-- clinical\GmClinicalTrainingSetup.jsp -->



<%@ page import="org.apache.commons.beanutils.BasicDynaBean"%>
<%@ page import="org.apache.commons.beanutils.RowSetDynaClass"%>
<%@ page import="java.util.ArrayList"%>


<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<bean:define id="trainingRecordId" name="frmClinicalTrainingSetup" property="strTrainingRecordId" type="java.lang.String"></bean:define>
<bean:define id="currentDate" name="frmClinicalTrainingSetup" property="currentDate" type="java.lang.String"> </bean:define>
<bean:define id="lockedStudyDisable" name="frmClinicalTrainingSetup" property="lockedStudyDisable" type="java.lang.String"> </bean:define>


<% log = GmLogger.getInstance(GmCommonConstants.COMMON);  // Instantiating the Logger 
	String strWikiTitle = GmCommonClass.getWikiTitle("TRAINING_SETUP");
    String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Clinical Training Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/gmClinicalTraining.js"></script>
<script>
	var recordid = '<%=trainingRecordId%>';
	var currentDate = '<%=currentDate%>';
</script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

</HEAD>

<BODY onLoad = "fnLoad();">
<html:form action="/gmClinicalTrainingSetupAction.do">
<html:hidden property="strTrainingRecordId" />
<html:hidden property="strOpt" />
<html:hidden property="hAction" value=""/>
<html:hidden property="msg"/>
<html:hidden property="lockedStudyDisable"/>
 <input type="hidden" name="hTxnId" value="">
 <html:hidden property="hTxnName" value=""/>
 <input type="hidden" name="hCancelType" value="">


	<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header"><b>Training Records : Setup<b></td>
			<td align="right" class=dhx_base_dhx_skyblue ></td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
		<tr>
		<td>
					<logic:notEqual name="frmClinicalTrainingSetup" property="msg" value="">
						<tr>
							<td colspan="2" height="20" align="center" class="RightBlueText">
							<bean:write name="frmClinicalTrainingSetup" property="msg"/></td>
						</tr>
						<tr><td class="Line" colspan="2"></td></tr>
					</logic:notEqual>	
		</td></tr>			
					
					<tr><td colspan="2" bgcolor="#cccccc"></td></tr>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="evenshade">
						<td class="RightText" align="right" HEIGHT="30" width="30%"><font color="red">*</font>&nbsp;<b>Training For:</b></td>						
							<td>&nbsp;<gmjsp:dropdown controlName="trainingForList"  SFFormName="frmClinicalTrainingSetup" SFSeletedValue="trainingForList"
							SFValue="alTrainingForList" onChange="fnReload();" width="250" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
            			</td>
					</tr>
					<tr><td class="LLine" colspan="2"></td></tr>
					<tr class="oddshade">
						<td class="RightText" align="right" HEIGHT="30" width="30%"><font color="red">*</font>&nbsp;<b>Name of Trainee:</b></td>
							
									<td>&nbsp;<gmjsp:dropdown controlName="traineeNameList"  SFFormName="frmClinicalTrainingSetup" SFSeletedValue="traineeNameList"
									SFValue="alTraineeNameList"  width="250" codeId="ID" codeName="NAME" defaultValue="[Choose One]"/>
						
            			</td>
					</tr>
					<tr><td class="LLine" colspan="2"></td></tr>
					<tr class="evenshade">
						<td class="RightText" align="right" HEIGHT="30" width="30%"><font color="red">*</font>&nbsp;<b>Reason for Training:</b></td>						
							<td>&nbsp;<gmjsp:dropdown controlName="trainingReasonList"  SFFormName="frmClinicalTrainingSetup" SFSeletedValue="trainingReasonList"
							SFValue="alTrainingReasonList"  width="250" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
            			</td>
					</tr>
					<tr><td class="LLine" colspan="2"></td></tr>
                    <tr class="oddshade">
                        <td class="RightText" align="right" HEIGHT="24"><b>Training Scope:</b></td>
                        <td >&nbsp; 
                        <html:textarea property="trainingScope" cols="50" rows ="3" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td> 
                    </tr>
                    <tr><td class="LLine" colspan="2"></td></tr>
                    <tr class="evenshade">
                    <td class="RightText" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<b> Completion Date:</b></td>
                  
                        <td>&nbsp; 
                        <gmjsp:calendar SFFormName="frmClinicalTrainingSetup" controlName="completionDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
                        </td>
                    </tr>                                   
					 <tr>                        
                        <td align="center" height="30" colspan="2">&nbsp;
	                        <gmjsp:button value="Submit" gmClass="button" buttonType="Save" onClick="fnSubmit();" disabled="<%=lockedStudyDisable %>"/>&nbsp;&nbsp;
	                        <gmjsp:button controlId ="btnVoid" name="btnVoid" value="Void" gmClass="button" buttonType="Save" onClick="fnVoid();" style="visibility=hidden" />
	                        <gmjsp:button controlId ="btnReset" name="btnReset" value="Reset" gmClass="button" buttonType="Save" onClick="fnReset();" style="visibility=hidden" />                    
                        </td>
                    </tr> 
      					
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

