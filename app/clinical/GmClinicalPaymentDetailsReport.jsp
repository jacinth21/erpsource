 <%
/**********************************************************************************
 * File		 		: GmCommonCurrConv.jsp
 * Desc		 		: This screen displys currency conversion
 * Version	 		: 1.0
 * author			: Himanshu Patel
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
 
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	
%>
<bean:define id="gridData" name="frmClinicalPaymentReportForm" property="gridXmlData" type="java.lang.String"> </bean:define>
<HTML>
<HEAD>
<TITLE> Globus Medical: Enterprise Portal</TITLE>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="/styles/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalPaymentDetailReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script>
	objGridData = '<%=gridData%>';
	document.onkeypress = function(){
		if(event.keyCode == 13){
			fnLoadReport();
		}
		
	}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<html:form action="/gmClinicalPaymentReportAction.do">
	<html:hidden property="strOpt" />
	<html:hidden property="haction" value=""/> 
	<html:hidden property="hTxnId" value="" />
	<html:hidden property="hTxnName" value="" />
	<html:hidden property="hCancelType" value="" />
	<html:hidden property="paymentId" value=""/>
	
	<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header" colspan="4">&nbsp;Reimbursements Detail Report</td>
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr height="25" class="evenshade">	
			<td class="RightTableCaption" align="right">&nbsp;Request For:</td>
			<td >&nbsp;
				<gmjsp:dropdown controlName="requestType"  SFFormName="frmClinicalPaymentReportForm" SFSeletedValue="requestType"
				SFValue="alRequestType" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>                 		                                        		
            </td>
            <td class="RightTableCaption" align="right">&nbsp;Status:</td>
			<td >&nbsp;
				<gmjsp:dropdown controlName="status"  SFFormName="frmClinicalPaymentReportForm" SFSeletedValue="status"
				SFValue="alStatus" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>                 		                                        		
            </td>
		</tr>
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		<tr height="25" class="oddshade">	
		    <td class="RightTableCaption" align="right" >&nbsp;Reason For Payment:</td>
			<td >&nbsp;
				<gmjsp:dropdown controlName="reasonForPayment"  SFFormName="frmClinicalPaymentReportForm" SFSeletedValue="reasonForPayment"
				SFValue="alReasonForPayment" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>                 		                                        		
            </td>
			<td colspan=2></td>
		</tr>
        <tr><td colspan="4" class="LLine" height="1"></td></tr>   
	    <tr height="30">
	           	<td colspan="4" align="center" height="30">
	                <gmjsp:button value="&nbsp;&nbsp;Load&nbsp;&nbsp;" gmClass="Button" buttonType="Load" onClick="fnLoadReport();" /> 
	            </td>
	    </tr>
	    <tr><td colspan="4" class="LLine" height="1"></td></tr>
	    <tr><td colspan="4">
		    <table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr> 
	        	<td colspan="4">
	          		<div id="dataGridDiv" class="grid" style="height:325px"></div>
				</td>
			</tr> 
			<tr colspan = "2" class = "oddshade">
						<td class = "oddshade" colspan="3" align="center" height="30">	
							<div >Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!-- | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a> -->
                     	</td>
					  </tr>
	    	</table>		
    	</td></tr>
    </table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
