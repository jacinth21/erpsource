<%
/**********************************************************************************
 * File		 		: GmRadioLogyView.jsp
 * Desc		 		: This screen is used for the Radiology Single and Multiple Image View
 * Version	 		: 1.0
 * author			: Hardik Parikh
************************************************************************************/
%>

<!--clinical\querysystem\GmRadiologyView.jsp -->

<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.util.GmImageUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Iterator"%><HTML>

<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);		
	int intHeight = 0;
	int intWidth = 0;
	HashMap hmValues = new HashMap();
	hmValues = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturn"));
	String strhView = GmCommonClass.parseNull((String)request.getParameter("hView"));
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));

	String strFileId= "";
	String strTypeName= "";
	String strViewName= "";
	String strPatientId="";
	String strPatientIde= "";
	String strTimepointName= "";	
	String strXRayDate = "";
	String strFileName = "";
	//checking the strAction value for view_upload_images
	if(strAction.equalsIgnoreCase("view_upload_Images")){
		strhView = GmCommonClass.parseNull((String)request.getAttribute("hView")); 
		strFileId = GmCommonClass.parseNull((String)hmValues.get("FILE_ID"));
		strTypeName = GmCommonClass.parseNull((String)hmValues.get("RAD_TYPE_NAME"));
		strViewName = GmCommonClass.parseNull((String)hmValues.get("RAD_VIEW_NAME"));
		strPatientId = GmCommonClass.parseNull((String)hmValues.get("PAT_ID"));
		strPatientIde = GmCommonClass.parseNull((String)hmValues.get("PAT_IDE"));
		strTimepointName =GmCommonClass.parseNull((String)hmValues.get("TIME_PNT_NAME"));
		strXRayDate = GmCommonClass.parseNull((String)hmValues.get("RADIOGRP_DATE"));
		strFileName = GmCommonClass.parseNull((String)hmValues.get("FILE_NM"));
	}else{
		strFileId= GmCommonClass.parseNull((String)request.getParameter("fileId"));
		strTypeName= GmCommonClass.parseNull((String)request.getParameter("typeName"));
		strViewName= GmCommonClass.parseNull((String)request.getParameter("viewName"));
		strPatientId= GmCommonClass.parseNull((String)request.getParameter("patientId"));
		strPatientIde= GmCommonClass.parseNull((String)request.getParameter("patientIde"));
		strTimepointName= GmCommonClass.parseNull((String)request.getParameter("timepointName"));
		strFileName = GmCommonClass.parseNull(request.getParameter("fileName"));
	}
	

	String strUploadHome = GmCommonClass.getString("RADIOLOGYUPLOADIR");
	String strUploadPath = strUploadHome+strTypeName+"\\"+strFileName;
	if(strhView.equals("single")) { 
		HashMap hmReturn = GmImageUtil.getImageHeightWidth(strUploadPath,"JPG");
		intHeight = Integer.parseInt(GmCommonClass.parseZero((String)hmReturn.get("HEIGHT")));
		intWidth = Integer.parseInt(GmCommonClass.parseZero((String)hmReturn.get("WIDTH")));
		if(intHeight > 550)
			intHeight = 550;
		if(intWidth > 550)
			intWidth = 550;
	}
	int intTotalImages=0;
	
	ArrayList alFileIdList = new ArrayList();
	ArrayList alTypeNameList = new ArrayList();
	ArrayList alViewNameList = new ArrayList();
	ArrayList alPatientIdList = new ArrayList();
	ArrayList alPatientIdeList = new ArrayList();
	ArrayList alTimepointNameList = new ArrayList();
	ArrayList alFileNameList = new ArrayList();
	
	String strEncodedTimePointName="";
	String strEncodedTypeName="";
	String strEncodedViewName="";
	
	
	if(strhView.equals("multi")) {
		
		alFileNameList = GmCommonClass.getListFromString(strFileName,",");
		alFileIdList = GmCommonClass.getListFromString(strFileId,",");
		alTypeNameList = GmCommonClass.getListFromString(strTypeName,",");
		alViewNameList = GmCommonClass.getListFromString(strViewName,",");
		alPatientIdList = GmCommonClass.getListFromString(strPatientId,",");
		alPatientIdeList = GmCommonClass.getListFromString(strPatientIde,",");
		alTimepointNameList = GmCommonClass.getListFromString(strTimepointName,",");				
		intTotalImages = alFileIdList.size();				
	}
	
	%>



<HEAD>
<TITLE> Globus Medical: View Radiology </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
</HEAD>

<BODY leftmargin="20" topmargin="10" >


<table border="0" width="500" cellspacing="0" cellpadding="0">
<tr>
	<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width = "1" height = "1"></td>
	<td colspan="3" class="Line" height="1"></td>
	<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width = "1" height = "1"></td>
</tr>
<tr>
	<td bgcolor="#666666" width="1" rowspan="2"><img src="<%=strImagePath%>/spacer.gif" width = "1" height = "1"></td>
	<td height="25" class="RightDashBoardHeader">&nbsp;Image View</td>
	<td bgcolor="#666666" width="1" rowspan="2"></td>
	<td bgcolor="#666666" width="1" rowspan="2"><img src="<%=strImagePath%>/spacer.gif" width = "1" height = "1"></td>
</tr>
<tr><td width="748" height="100" valign="top" align="center">
	<% if(strhView.equals("single")) { %>	
	 <table>
	 <tr>
	 <td>
	 	<img src="/GmCommonFileOpenServlet?uploadString=RADIOLOGYUPLOADIR&appendPath=<%=strTypeName%>&sId=<%=strFileId%>" height=<%=intHeight%> width=<%=intWidth%> />
	 </td>
	 </tr>	 
	 <tr><td align='center'>
		 <%=strPatientIde%>&nbsp;-<%=strViewName %>&nbsp;-<%=strTimepointName%>
		</td>
	 </tr>
	 </table>	 
	 <%} else{ %>
	
	 <table>
	 <%
	 for(int i=0; i<intTotalImages;i++){ 
			strEncodedTimePointName = ((String)alTimepointNameList.get(i)).replaceAll(" ","%20");
			strEncodedTypeName = ((String)alTypeNameList.get(i)).replaceAll(" ","%20");
			strEncodedViewName = ((String)alViewNameList.get(i)).replaceAll(" ","%20");
	 %>
			 <tr>
			 <td>
			 	<a href="#" onclick=window.open("/clinical/GmRadiologyView.jsp?fileName=<%=(String)alFileNameList.get(i)%>&fileId=<%=(String)alFileIdList.get(i)%>&typeName=<%=strEncodedTypeName%>&viewName=<%=strEncodedViewName%>&patientId=<%=(String)alPatientIdList.get(i)%>&patientIde=<%=(String)alPatientIdeList.get(i)%>&hView=single&timepointName=<%=strEncodedTimePointName%>","<%=(String)alFileIdList.get(i)%>","resizable=yes,scrollbars=yes,width=650,height=650") ><img border="0" src="/GmCommonFileOpenServlet?uploadString=RADIOLOGYUPLOADIR&appendPath=<%=(String)alTypeNameList.get(i)%>&sId=<%=(String)alFileIdList.get(i)%>" height=200 width=225 /></a>
			 </td>
			 <% if(i+1 < intTotalImages){
					strEncodedTimePointName = ((String)alTimepointNameList.get(i+1)).replaceAll(" ","%20");
					strEncodedTypeName = ((String)alTypeNameList.get(i+1)).replaceAll(" ","%20");
					strEncodedViewName = ((String)alViewNameList.get(i+1)).replaceAll(" ","%20");
			%>
				 <td>
				
				 	<a href="#" onclick=window.open("/clinical/GmRadiologyView.jsp?fileName=<%=(String)alFileNameList.get(i+1)%>&fileId=<%=(String)alFileIdList.get(i+1)%>&typeName=<%=strEncodedTypeName%>&viewName=<%=strEncodedViewName%>&patientId=<%=(String)alPatientIdList.get(i+1)%>&patientIde=<%=(String)alPatientIdeList.get(i+1)%>&hView=single&timepointName=<%=strEncodedTimePointName%>","<%=(String)alFileIdList.get(i+1)%>","resizable=yes,scrollbars=yes,width=650,height=650") ><img border="0" src="/GmCommonFileOpenServlet?uploadString=RADIOLOGYUPLOADIR&appendPath=<%=(String)alTypeNameList.get(i+1)%>&sId=<%=(String)alFileIdList.get(i+1)%>" height=200 width=225 /></a>
				 </td>
			 <%}else{ %>
				 <td>
				 	&nbsp;
				 </td>
			 <%}%>
			 </tr>	 
			 <tr>			 
			 	<td align='center'>
				 <%=(String)alPatientIdeList.get(i)%>&nbsp;-<%=(String)alViewNameList.get(i)%>&nbsp;-<%=(String)alTimepointNameList.get(i)%>
				</td>
			<% if(i+1 < intTotalImages){ %>	
				<td align='center'>
				 <%=(String)alPatientIdeList.get(i+1)%>&nbsp;-<%=(String)alViewNameList.get(i+1)%>&nbsp;-<%=(String)alTimepointNameList.get(i+1)%>
				</td>
			<%}else{ %>
				 <td>
			 	&nbsp;
				 </td>
			 <%}%>			
			 </tr>
	 <%
		i++; }
	 %>
	 </table>	 
	
	 <%}%>	 
	 
	</td>	
</tr>


      		
<tr><td colspan="3" class="Line" height="1"></td></tr>
<tr>
<td colspan="3" > &nbsp; </td>
</tr>

<% if(strhView.equals("single")) { %>
<tr>
<td colspan="3" class="RightText" align="center">
	Zoom In: Press Ctrl and + Sign&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Zoom Out: Press Ctrl and  - Sign
</td>
</tr>
<%}%>
</table>


</BODY>

</HTML>
