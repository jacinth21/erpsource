<%
/**********************************************************************************
 * File		 		: GmClinicalStudy.jsp
 * Desc		 		: This is a report screen for Clinical Site Map setup
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<bean:define id="gridData" name="frmClinicalStudyForm" property="gridXmlData" type="java.lang.String"> </bean:define>
<HTML>
<HEAD>
<TITLE> Globus Medical: Clinical Site Map </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css"> 
<script language="JavaScript" src="<%=strJsPath%>/GmCommonGrid.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css"> 
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script> 

<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
 
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalStudy.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalCommonScript.js"></script>
<script>
  
	objGridData = '<%=gridData%>';		
</script>
</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();">
<form name="frmClinicalStudyForm" method="post">	

 <table border="0" cellspacing="0" cellpadding="0" class="DtTable765">
  <tr style="height:25px">
  	<td style='height:25px' class="Header">&nbsp;Study List</td>
  </tr>
  <tr><td class="LLine" height="1"></td></tr> 
  <tr> 
    <td>
    	<div id="dataGridDiv" class="grid" style="height : 383px;"></div>            
  	</td>   
  </tr>
 </table>        
    </form>
</BODY>
</HTML>

