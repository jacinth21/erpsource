 <%
/**********************************************************************************
 * File		 		: GmClincalVoidReport.jsp
 * Desc		 		: This screen is used for the Void Report Data
  * author			: Kulanthaivelu
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.clinical.beans.GmQuestionBean"%>
<%@ page import ="com.globus.clinical.data.GmQuestionMasterData"%>
<%@ page import ="com.globus.clinical.data.GmAnswerGrpData"%>
<%@page import="java.util.Date"%>
<%@page import="org.apache.commons.lang.SystemUtils"%>
<%@page import="java.text.DateFormat"%>

<bean:define id="gridData" name="frmStudyFilterForm" property="gridXmlData" type="java.lang.String"> </bean:define>
<% 

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strTodaysDate = (String)session.getAttribute("strSessTodaysDate");
	
	String strAction = (String)request.getAttribute("hAction");

	ArrayList alStudyList = new ArrayList();
	ArrayList alPatientList = new ArrayList();
	ArrayList alFormsList = new ArrayList();
	ArrayList alPeriodList = new ArrayList();
	ArrayList alVerifyType = new ArrayList();
	ArrayList alVerifyList = new ArrayList();	
	
	HashMap hmLists = new HashMap();
	HashMap hmReturn = new HashMap();
	HashMap hmQuestionsDetails = new HashMap();
	HashMap hmPatientFormDetails = new HashMap();
	HashMap hmPatients = new HashMap();

	String strFormNm = "";
	String strFormDt = "";
	String strInitials = "";
	String strCreatedBy = "";
	String strCreatedDt = "";
	String strLastUpdBy = "";
	String strLastUpdDt = "";
	String strVerifyFl = "";
	String strCalReqFl = "";
	String strPatListId = "";
	String strPatientIDE = "";
	String strVerfyLvlID = "";
	String strDeleteFl = "";
	String strColspan="colspan=\"2\"";
	String strSessPatientId = (String)session.getAttribute("strSessPatientId");
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Forms Data entry </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="/javascript/dhtmlx/dhtmlxCombo/dhtmlxcombo_new.css">
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="/javascript/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="/javascript/clinical/GmClinicalVoidReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="/javascript/dhtmlx/dhtmlxCombo/dhtmlxcombo.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script>

objGridData = '<%=gridData%>';

</script>
</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();">
<FORM method="post" action="/gmClinicalVoidReportAction.do" name="frmStudyFilterForm">
<input type="hidden" name="strOpt" value="">

<table border="0" class="DtTable1200" cellspacing="0" cellpadding="0">

<tr>
	<td height="25" class="Header" colspan="4">&nbsp;Voided Forms</td>
</tr>
<tr>
	<td height="1" colspan="4" class="LLine"></td>
</tr>
	<tr><td  height="100" valign="top" colspan="4">
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<tr class="evenshade" height="25">
			<%
			if(strSessPatientId.equals(""))
			 {
				strColspan="";
			%>	
			<td class="RightTableCaption" align="right" >&nbsp;Patient:</td>
			<td >&nbsp;<gmjsp:dropdown controlName="patientId"  SFFormName="frmStudyFilterForm" SFSeletedValue="patientId"
				SFValue="alPatientList" codeId="PATID" codeName="PATIDE" defaultValue="[Choose One]"/>                 		                                        		
            </td>
            <%}%>
            <td class="RightTableCaption" align="right">&nbsp;Time Point:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="periodId"  SFFormName="frmStudyFilterForm" SFSeletedValue="periodId"
				SFValue="alPeriodList" codeId="PERIODID" codeName="PERIODDS" onChange="fnReload();" defaultValue="[Choose One]"/>
			</td>
			<td class="RightTableCaption" align="right" >&nbsp;Form:</td>
			<td>&nbsp;<gmjsp:dropdown  controlName="formId"  SFFormName="frmStudyFilterForm" SFSeletedValue="formId"
				SFValue="alStudyFormList" codeId="FORMID" codeName="FORMNM" defaultValue="[Choose One]"/>                 		                                        		
            </td>
		</tr>
		<tr><td colspan="6" class="LLine" height="1"></td></tr>
		<tr height="25" class="oddshade">
		 <td class="RightTableCaption" align="right" width="9%">&nbsp;Created Date :</td>
			<td  width=16%>&nbsp;<gmjsp:calendar SFFormName="frmStudyFilterForm" controlName="fromDate" toControlName ="toDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" /> 
			</td>
			
            <td class="RightTableCaption" align="right" width="10%">&nbsp;Last Updated By:</td>
			<td >&nbsp;<gmjsp:dropdown controlName="craId"  SFFormName="frmStudyFilterForm" SFSeletedValue="craId"
				SFValue="alCraNames" codeId="USERNM" codeName="RUSERNM" defaultValue="[Choose One]"/>
			</td>
			<td class="RightTableCaption" align="right" width="10%">&nbsp;Reason For Closing:</td>
			<td >&nbsp;<gmjsp:dropdown controlName="reasonForClosing"  width="150" SFFormName="frmStudyFilterForm" SFSeletedValue="reasonForClosing"
				SFValue="alReasonForClosing" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	         <gmjsp:button value="&nbsp;Load&nbsp;" style="width: 5em; height: 20" gmClass="button" buttonType="Load" onClick="fnLoadReport();" />&nbsp;      
			</td>
		</tr>
		 <tr><td colspan="6" class="LLine" height="1"></td></tr>
		 <%if(gridData.indexOf("cell") != -1){%>
				<tr>
					<td colspan="6">
						<div  id="dataGridDiv" class="grid" style="height:500px;width:1200px;"></div>						
					</td>
				</tr>
							
			<%}else if(!gridData.equals("")){%>
				<tr><td colspan="6" align="center" class="RightText">Nothing found to display</td></tr>
			<%}else{%>
				<tr><td colspan="6" align="center" class="RightText">No data available</td></tr>
			<%} %>

		
		<%if( gridData.indexOf("cell") != -1) {%>
		<tr>
			<td colspan="6" align="center">
			    <div class='exportlinks'>Export Options : <img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnDownloadXLS();"> Excel </a></div>
			</td>
		</tr>
		<%} %>			
	</table>
	</td>
</tr>
</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
