<%
/**********************************************************************************
 * File		 		: GmRptAdverseEvent.jsp
 * Desc		 		: Report for Adverse Event
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>

<!-- clinical\GmRptAdverseEvent.jsp -->


<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ include file="/common/GmHeader.inc" %>

<%@ page import="com.globus.common.beans.GmCommonConstants"%> 
<%@ page import="com.globus.common.beans.GmCommonClass"%> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ page import="com.globus.common.beans.GmLogger"%> 
<%@ page import="org.apache.log4j.Logger"%> 
<bean:define id="gridData" name="frmAdverseEvent" property="gridXmlData" type="java.lang.String"> </bean:define>
<% 
Object bean = pageContext.getAttribute("frmAdverseEvent", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
String strSessStudyId = (String)session.getAttribute("strSessStudyId");
String strSessPatientId = (String)session.getAttribute("strSessPatientId");
//System.out.println("strSessPatientId========="+strSessPatientId);
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Adverse Events </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
var studyIdVal = '<%=strSessStudyId%>';

document.onkeypress = function(){
		if(event.keyCode == 13){
			fnLoad();
		}
		
	}

function fnLoad()
{
	document.frmAdverseEvent.action = "/gmRptAdverseEvent.do?method=reportAdverseEvent";
	document.frmAdverseEvent.selectAll.checked = false;
	document.frmAdverseEvent.strOpt.value = "";
	document.frmAdverseEvent.submit();
}

function fnReport()
{
	document.frmAdverseEvent.action = "/gmRptAdverseEvent.do?method=reportAdverseEvent";

	document.frmAdverseEvent.strOpt.value = "Report";
	fnStartProgress('Y');
	document.frmAdverseEvent.submit();
}	

function fnOpenForm(varPatientId, varEventId)
{
	
	if (studyIdVal == 'GPR002'){
		document.frmAdverseEvent.Cbo_Form.value = 14;
		document.frmAdverseEvent.hStPerId.value = 97;
	}else if (studyIdVal == 'GPR003'){
		document.frmAdverseEvent.Cbo_Form.value = 213;
		document.frmAdverseEvent.hStPerId.value = 104;
	}else if (studyIdVal == 'GPR004'){
		document.frmAdverseEvent.Cbo_Form.value = 50;
		document.frmAdverseEvent.hStPerId.value = 423;
	}else if (studyIdVal == 'GPR005'){
		document.frmAdverseEvent.Cbo_Form.value = 72;
		document.frmAdverseEvent.hStPerId.value = 453;
	}else if (studyIdVal == 'GPR006'){
		document.frmAdverseEvent.Cbo_Form.value = 98;
		document.frmAdverseEvent.hStPerId.value = 763;
	}else if (studyIdVal == 'GPR009'){
		// PMT-41229: Amnios RT study - Adverse Event form
		//Amnios study added the adverse event form
		
		document.frmAdverseEvent.Cbo_Form.value = 268;
		document.frmAdverseEvent.hStPerId.value = 1000;
	}
	
	document.frmAdverseEvent.Cbo_Study.value = studyIdVal;
	document.frmAdverseEvent.Cbo_Period.value = 6309; // 6309 is N/A
	document.frmAdverseEvent.hPatientId.value = varPatientId;
	document.frmAdverseEvent.hAction.value = "LoadQues";
	document.frmAdverseEvent.cbo_EventNo.value = varEventId;
	document.frmAdverseEvent.action = "/GmFormDataEntryServlet";
	document.frmAdverseEvent.submit();
	
}

var objGridData = '<%=gridData%>';

function initGridData(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);	
	gObj.enableMultiline(true);
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}

function fnOnPageLoad(){
		gridObj = initGridData('dataGridDiv',objGridData);
		
		// PMT-41229 - Amnios study - label changes
		if(studyIdVal =='GPR009'){
			gridObj.setColumnLabel( 3, "Injection Date");
			gridObj.setColumnLabel( 7, "Severity");
			gridObj.setColumnLabel( 8, "Relationship to injection product");
			gridObj.setColumnLabel( 9, "Relationship to injection procedure");
			gridObj.setColumnLabel( 10, "UAE");
		}
		
	}

</script>

</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();" >
<html:form action="/gmRptAdverseEvent.do"  >
<html:hidden property="strOpt" />
<html:hidden property="hAction" value="" />
<html:hidden property="Cbo_Form" value="" />
<html:hidden property="Cbo_Study" value="" />
<html:hidden property="hStPerId" value="" />
<html:hidden property="Cbo_Period" value="" />
<html:hidden property="Cbo_Patient" value="" />
<html:hidden property="Txt_FormDate" value="" />
<html:hidden property="cbo_EventNo" value="" />
<html:hidden property="hPatientId" value="" />
	<table border="0" class="DtTable1300" cellspacing="0" cellpadding="0">
	<%if(GmCommonClass.parseNull(request.getParameter("HIDDENSITELIST")).equals("")){ %> 	<tr>
			<td height="25" class="Header">Adverse Events</td>
		</tr><%} %>
		<tr><td class="LLine"></td></tr> 
		<tr>
			<td  valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
						<td colspan="2" align="center">
	                    	<jsp:include page="/clinical/GmStudyFilterInc.jsp" >
							<jsp:param name="FORMNAME" value="frmAdverseEvent" />
							<jsp:param name="HIDELOADBUTTON" value="HIDE" />
							</jsp:include>	
						</td>
					</tr>						
					<%if(GmCommonClass.parseNull(request.getParameter("HIDDENSITELIST")).equals(""))
						{
						 if(strSessPatientId.equals(""))
						 {
					%>
					<tr><td colspan="4" class="LLine"></td></tr> 					  
					<tr>
						
						<td class="RightTableCaption" align="right" height="30" width="80" nowrap>Patient ID :</td> 
						<td width="100%">&nbsp;
							<logic:equal name="frmAdverseEvent" property="studyListId" value="GPR002">
								<html:text property="patientId"  size="6" maxlength="4" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
							</logic:equal>
							<logic:equal name="frmAdverseEvent" property="studyListId" value="GPR003">
								<html:text property="patientId"  size="6" maxlength="6" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
							</logic:equal>
							<logic:equal name="frmAdverseEvent" property="studyListId" value="GPR004">
								<html:text property="patientId"  size="6" maxlength="6" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
							</logic:equal>
							<logic:equal name="frmAdverseEvent" property="studyListId" value="GPR005">
								<html:text property="patientId"  size="6" maxlength="6" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
							</logic:equal>
							<logic:equal name="frmAdverseEvent" property="studyListId" value="GPR009">
								<html:text property="patientId"  size="8" maxlength="8" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
							</logic:equal>
							&nbsp;<gmjsp:button value="Load" style="width: 5em; height: 22" gmClass="button" buttonType="Load" onClick="fnReport();"/>	
    		            </td>
					</tr>
					</table>
  			   </td>
  		  </tr>	
					<tr><td colspan="2" class="LLine"></td></tr> 
					<%}} %>                    
									
					<tr>
					  <%if(gridData.indexOf("cell") != -1){%>
						<td colspan="4" align = "left"><div id="dataGridDiv" class="grid" style="height:400px;"></div></td>
					  <%}else if(!gridData.equals("")){%>
						<td colspan="4" align="center" class="RightText">Nothing found to display</td>
					  <%}else{%>
						<td colspan="4" align="center" class="RightText">No data available</td>
					  <%}%>	
				  	</tr>
				  	<%if(gridData.indexOf("cell") != -1) {%>
					<tr class = "oddshade">
						<td class = "oddshade" colspan="3" align="center" height="30">	
							<div >Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> </div><!-- | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a> -->
                     	</td>
					  </tr>	
				    <%}%>
			   	
  		
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

