 <%
/**********************************************************************************
 * File		 		: GmPatientLog.jsp
 * Desc		 		: This screen displys Patient Log
 * Version	 		: 1.0
 * author			: Himanshu Patel
************************************************************************************/
%>

<!--clinical\querysystem\GmPatientLog.jsp -->


<%@ page language="java" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.ArrayList" %>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
 <%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%> 
<%
GmServlet gm = new GmServlet();
gm.checkSession(response,session);

String strSessPatientId = (String)session.getAttribute("strSessPatientId");
String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");


%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Enterprise Portal</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">

<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmPatientLog.js"></script>
<bean:define id="strOpt" name="frmPatientLogForm" property="strOpt" type="java.lang.String"> </bean:define>
<bean:define id="lockedStudyDisable" name="frmPatientLogForm" property="lockedStudyDisable" type="java.lang.String"> </bean:define>

</HEAD>

<BODY>
 
<html:form action="/gmClinicalPatientLog.do">
<html:hidden property="strOpt" />
<html:hidden property="hAction" value=""/> 
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VPLOG"/>



 
	<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">Comment Log : Enter Comments</td>
			<td align="right" class="dhx_base_dhx_skyblue" colspan="3"> 	
				</td>
				
		</tr>
		<%
		if(strSessPatientId.equals(""))
		{
		%>
			<tr><td colspan="4" class="LLine" height="1"></td></tr> 
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<tr class="evenshade">  
				<td class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;Patient ID:</td>
				<td colspan="3" HEIGHT="24">&nbsp;
					<gmjsp:dropdown controlName="patientId" SFFormName="frmPatientLogForm" SFSeletedValue="patientId"
							SFValue="alPatientId" codeId = "PATID"  codeName = "PATIDE"  defaultValue= "[Choose One]" />
				</td>
			</tr>	
	<%}
	else
	{%>		
		
		<tr><td colspan="4" class="LLine" height="1"></td></tr> 
		<tr class="evenshade">  
			<td class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;Patient ID:</td>
			<td colspan="3" HEIGHT="24">&nbsp;
					<gmjsp:dropdown controlName="patientId" SFFormName="frmPatientLogForm" SFSeletedValue="patientId"
							SFValue="alPatientId" codeId = "PATID"  codeName = "PATIDE"  defaultValue= "[Choose One]" disabled="disabled" />
				</td>
		</tr>
	<%}%>
		<tr><td colspan="4" class="LLine" height="1"></td></tr> 
		<tr class="oddshade">			
			<td class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;TimePoint:</td>
			<td HEIGHT="24" width="400">&nbsp;
				<gmjsp:dropdown controlName="studyPeriod" SFFormName="frmPatientLogForm" SFSeletedValue="studyPeriod"
						SFValue="alStudyPeriod" codeId = "PERIODID"  codeName = "PERIODDS"  defaultValue= "[Choose One]" />
			</td>
			<td class="RightTableCaption" align="right" HEIGHT="25">Form:</td>
			<td HEIGHT="24" width="400">&nbsp;
				<gmjsp:dropdown controlName="studyForm" SFFormName="frmPatientLogForm" SFSeletedValue="studyForm"
						SFValue="alFormList" codeId = "FORMID"  codeName = "FORMNM"  defaultValue= "ALL" />
			</td>				
		</tr>		
		<tr><td colspan="4" class="LLine" height="1"></td></tr> 	
		<tr class="evenshade">
						<td class="RightTableCaption" align="right" HEIGHT="30"></font>&nbsp;&nbsp;Comment#:</td>						
						<td colspan="3">&nbsp;<html:text property="patientLId" size="10" style="border:none;" readonly ="true" /></td>
		</tr>
		<tr><td colspan="4" class="LLine" height="1"></td></tr> 
		<tr class="oddshade">
					<td class="RightTableCaption" valign="top" HEIGHT="25" align="right"><BR><font color="red">*</font>&nbsp;Comments:</td>
					<td colspan="3">&nbsp;<html:textarea property="logDetail" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"	
					onblur="changeBgColor(this,'#ffffff');" rows="4" cols="50" /><br>
					</td>
		</tr>
		<tr><td colspan="4" class="LLine" height="1"></td></tr>		
    	<tr class="evenshade">
    		<td colspan="4" align="center" height="30">&nbsp;&nbsp;    		 
                <gmjsp:button value="Submit" gmClass="button" buttonType="Save" onClick="fnSubmit();" disabled="<%=lockedStudyDisable %>"/>                
                <%if(!strOpt.equals("Load")){%>
                	<gmjsp:button value="Add New" gmClass="button" buttonType="Save" onClick="fnReset();" disabled="<%=lockedStudyDisable %>"/> 
	                <gmjsp:button value="Void" gmClass="button" buttonType="Save" onClick="fnVoid();" disabled="<%=lockedStudyDisable %>"/>  
                <%}%>            
        	</td>
     	</tr>        		
    </table>		 
</html:form>
<%@ include file="/common/GmFooter.inc" %>   
</BODY>
</HTML>
