<%
/**********************************************************************************
 * File		 		: GmRptLevelTreated.jsp
 * Desc		 		: Demographic Report for the Patient Information
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>


<!-- clinical\GmRptLevelTreated.jsp -->

<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<bean:define id="gridXmlData" name="frmDgraphReport" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="strPartType" name="frmDgraphReport" property="cbo_PartType" type="java.lang.String" ></bean:define>

<% 
String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
log.debug("GD === "+gridXmlData.length());
	
	String strhAction = (String)request.getAttribute("hAction");
	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}
	log.debug("GD === "+gridXmlData.length());
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Level Treated </TITLE>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmStudyFilterInc.js"></script>

<%	log.debug("GD === "+gridXmlData.length()); %>

<script>

//alert(gridXmlData);
document.onkeypress = function(){
		if(event.keyCode == 13){
			fnReport();
		}
		
	}

function fnReport()
{
	document.frmDgraphReport.action = "/gmRptDgraph.do?method=reportLevelTreated";
	document.frmDgraphReport.strOpt.value = "rptLevelTreated";
	fnStartProgress('Y');
	document.frmDgraphReport.submit();
}	

function fnLoad()
{
	document.frmDgraphReport.selectAll.checked = false;
	document.frmDgraphReport.strOpt.value = "";
	document.frmDgraphReport.action = "/gmRptDgraph.do?method=reportLevelTreated";
	document.frmDgraphReport.submit();
}


	
function fnOnPageLoad(){
	var objGridData = '<%=gridXmlData%>';
		<% if(gridXmlData.length()>0) {%>
			gridObj = initGrid('dataGridDiv',objGridData);
		<%}%>
		fnCheckAll();
	}
</script>
</HEAD>


<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();">
<html:form action="/gmRptDgraph.do"  >
<html:hidden property="strOpt" />


	<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">Level Treated</td>
		</tr>
		<tr>
			<td valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr><td colspan="2" class="ELine"></td></tr> 
                    <tr>
						<td colspan="2" align="center">
						   	<jsp:include page="/clinical/GmStudyFilterInc.jsp" >
							<jsp:param name="FORMNAME" value="frmDgraphReport" />			
							<jsp:param name="HIDELOADBUTTON" value="HIDE" />					
							</jsp:include>	
						</td>
					</tr>
					<tr>
			             <td colspan="2">
			             	<table cellpadding="0" cellspacing="0" border="0">
					<!-- Custom tag lib code modified for JBOSS migration changes -->
			             		<tr>
						             <td class="RightTableCaption" align="right" width="80"></font>&nbsp;Part Type :</td>   
							         <td colspan ="3" height="30" align="left">&nbsp;<gmjsp:dropdown controlName="cbo_PartType"  SFFormName='frmDgraphReport' SFSeletedValue="cbo_PartType"
								       	SFValue="alPartTypes" codeId="CODEID" codeName="CODENM" />
								       	&nbsp;&nbsp;<gmjsp:button value="Load" gmClass="button" buttonType="Load" onClick="fnReport();"/>
								     </td>  	       
						       	</tr>
						   </table>
						</td>
				    </tr>
					<tr><td height="5"></td></tr>	
					<% if(gridXmlData.length()>0) {%>	        
					<tr>
						
						<td colspan="2">
			           		<div id="dataGridDiv" class="grid" style="height:340px"></div>
						</td>
						
					</tr>	
					 <tr class = "oddshade">
						<td class = "oddshade" colspan="3" align="center" height="30">	
							<div >Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!-- | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a> -->
                            </div>

							
						</td>
					</tr>	
					<%} %>
				  </td>
				 
					</tr>
			   	</table>
  			   </td>
  		  </tr>	  		
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>

</BODY>

</HTML>

