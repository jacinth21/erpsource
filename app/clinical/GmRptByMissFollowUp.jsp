<%
/**********************************************************************************
 * File		 		: GmRptByMissFollowUp.jsp
 * Desc		 		: Missing Followup Cross Tab
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>

<!-- clinical\GmRptByMissFollowUp.jsp -->


<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@include file="/common/GmHeader.inc"%> 
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
 <%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmGridFormat"%>
<bean:define id="hmCrossTabReport" name="frmRptByTreatement" property="hmCrossTabReport" type="java.util.HashMap"> </bean:define>
<bean:define id="sessStudyId" name="frmRptByTreatement" property="sessStudyId" type="java.lang.String"> </bean:define>

<% 
	GmGridFormat gmCrossTab = new GmGridFormat();
	int intSortColumn =  request.getParameter("hSortColumn") == null ? -1 : Integer.parseInt(request.getParameter("hSortColumn"));
	int intSortOrder = Integer.parseInt(GmCommonClass.parseZero(request.getParameter("hSortOrder")));	
	
	gmCrossTab.setGeneralHeaderStyle("aaTopHeaderLightG");
	gmCrossTab.setNameWidth(50);
	gmCrossTab.setColumnWidth("Surgeon Name",120);
	gmCrossTab.setColumnWidth("Treatment",150);	
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setNoDivRequired(true);
	gmCrossTab.setRoundOffAllColumnsPrecision("0");
	gmCrossTab.setRowHighlightRequired(true);
	
	
	gmCrossTab.addSortType("Name","String");
	gmCrossTab.setSortColumn(intSortColumn);
	gmCrossTab.setSortOrder(intSortOrder);
	gmCrossTab.setSortType("String");
	gmCrossTab.setSortRequired(true);
	
	gmCrossTab.addStyle("Name","ShadeLevel2NoBold","aaTopHeaderLightG") ;
	gmCrossTab.addStyle("Surgeon Name","ShadeLevel2NoBold","aaTopHeaderLightG") ;
	gmCrossTab.addStyle("Treatment","ShadeLevel2NoBold","aaTopHeaderLightG") ;
	
	gmCrossTab.addLine("Name");
	gmCrossTab.addLine("Surgeon Name");	
	gmCrossTab.addLine("Treatment");	
	gmCrossTab.reNameColumn("Name","Patient ID");
	gmCrossTab.setDivHeight(280);
	
	gmCrossTab.setDecorator("com.globus.crosstab.beans.GmRptByOutOfWindowDecorator");
	String strGridData = gmCrossTab.generateGridXML(hmCrossTabReport);
	String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
	
 %>

<HTML>
<HEAD>
<TITLE> Globus Medical: Missing Follow-up (NDI) </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmStudyFilterInc.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
</HEAD>
<script>

//PMT-39479 Amnios RT study label changes
var studyIdVal = '<%=sessStudyId%>';

document.onkeypress = function(){
		if(event.keyCode == 13){
			fnReport();
		}
		
	}
function fnLoad()
{
	document.frmRptByTreatement.selectAll.checked = false;
	document.frmRptByTreatement.strOpt.value = "";
	document.frmRptByTreatement.action = "/gmRptByTreatment.do?method=missingFollowUp";
	document.frmRptByTreatement.submit();
}

function fnReport()
{
	document.frmRptByTreatement.action = "/gmRptByTreatment.do?method=missingFollowUp";
	document.frmRptByTreatement.strOpt.value = "Report";
	fnStartProgress();
	document.frmRptByTreatement.submit();
}	

function fnSort(varColumnName, varSortOrder)
{	
	document.frmRptByTreatement.hSortColumn.value = varColumnName;
	document.frmRptByTreatement.hSortOrder.value = varSortOrder;
	fnReport();
}

var objGridData = '<%=strGridData%>';
	
function fnOnPageLoad(){

	gridObj = initGrid('dataGridDiv',objGridData,1);

	// to update the Amnios label 
	if( studyIdVal == 'GPR009'){
		gridObj.setColumnLabel( 1, "Investigator Name");
	}
}	
</script>


<BODY leftmargin="0" topmargin="0"  onLoad="fnOnPageLoad();">
<html:form action="/gmRptByTreatment.do"  >
<html:hidden property="strOpt" />
<input type="hidden" name="hSortColumn" value="<%=intSortColumn%>">
<input type="hidden" name="hSortOrder" value="<%=intSortOrder%>">
 
<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">Missing Follow-Up</td>
		</tr>
		<tr>
			<td  height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2" align="center">&nbsp;
	                    	<jsp:include page="/clinical/GmStudyFilterInc.jsp" >
							<jsp:param name="FORMNAME" value="frmRptByTreatement" />
							</jsp:include>	
						</td>
					</tr>
					<tr><td height="5"></td></tr>
					<tr>
						<td colspan ="2" align=center> <div id="dataGridDiv" style="" height="400" ></td>
					</tr>
					<tr class = "oddshade">
						<td class = "oddshade" colspan="3" align="center" height="30">	
							<div >Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!-- | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a> -->
                            </div>

							
						</td>
					</tr>	
			   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
