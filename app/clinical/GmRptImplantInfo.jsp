<%
/**********************************************************************************
 * File		 		: GmRptImplantInfo.jsp
 * Desc		 		: Report for Implant Information
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>

<!-- clinical\GmRptImplantInfo.jsp -->

<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ page import="java.text.NumberFormat"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<bean:define id="gridData" name="frmRptImplantInfo" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="strPartType" name="frmRptImplantInfo" property="cbo_PartType" type="java.lang.String" ></bean:define>

<% 
String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
	String strhAction = (String)request.getAttribute("hAction");
	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}
	boolean show = true;
	if(gridData.indexOf("cell", 0)==-1)
		show = false;
	log.debug(""+show);
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Implant Information </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmStudyFilterInc.js"></script>



<script>
document.onkeypress = function(){
		if(event.keyCode == 13){
			fnReport();
		}
		
	}
function fnReport()
{
	document.frmRptImplantInfo.action = "/gmRptImplantInfo.do?method=reportImplantInfo";
	document.frmRptImplantInfo.strOpt.value = "Report";
	fnStartProgress('Y');
	document.frmRptImplantInfo.submit();
}	

function fnLoad()
{
	document.frmRptImplantInfo.selectAll.checked = false;
	document.frmRptImplantInfo.strOpt.value = "";
	document.frmRptImplantInfo.action = "/gmRptImplantInfo.do?method=reportImplantInfo";
	document.frmRptImplantInfo.submit();
}

var objGridData = '<%=gridData%>';

	
function fnOnPageLoad(){
		<%if(show){%>
			gridObj = initGrid('dataGridDiv',objGridData);
		<%}%>
		fnCheckAll();
	}

</script>

</HEAD>



<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();">
<html:form action="/gmRptImplantInfo.do"  >
<html:hidden property="strOpt" />

<%
 String strTitle = "Part Type";
 
   if(strPartType.equals("60253") || strPartType.equals("60931"))
    {
     strTitle = "Footprint";
    }
    else if(strPartType.equals("60254"))
    {
      strTitle = "Shape";
    }
    else if(strPartType.equals("60255")|| strPartType.equals("60933"))
    {
     strTitle = "Core Height";
    }
   else if(strPartType.equals("60256"))
    {
     strTitle = "Allograft Type";
    }
%>

	<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">Implants Used</td>
		</tr>
		<tr>
			<td valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr><td colspan="2" class="ELine"></td></tr> 
                    <tr>
						<td colspan="2" align="center">
						   	<jsp:include page="/clinical/GmStudyFilterInc.jsp" >
							<jsp:param name="FORMNAME" value="frmRptImplantInfo" />
							<jsp:param name="HIDELOADBUTTON" value="HIDE" />								
							</jsp:include>	
						</td>
					</tr>
			        <tr>
			             <td colspan="2">
			             	<table cellpadding="0" cellspacing="0" border="0">
					<!-- Custom tag lib code modified for JBOSS migration changes -->
			             		<tr>
						             <td class="RightTableCaption" align="right" width="80"></font>&nbsp;Part Type :</td>   
							         <td colspan ="3" height="30" align="left">&nbsp;<gmjsp:dropdown controlName="cbo_PartType"  SFFormName='frmRptImplantInfo' SFSeletedValue="cbo_PartType"
								       	SFValue="alPartTypes" codeId="CODEID" codeName="CODENM"/>
								       	&nbsp;&nbsp;<gmjsp:button value="Load" gmClass="button" buttonType="Load" onClick="fnReport();"/>
								     </td>  	       
						       	</tr>
						   </table>
						</td>
				     </tr>
				   <%if(show){%>
				     
				     <tr>
						<td colspan="2"  align = "center">
												
				           		<div id="dataGridDiv" class="grid" style="height:100px; width:40%;"></div>
				        </td>
				           	
			        			     
		     		

				  	<tr/>
				  	
					<tr class = "oddshade">
						<td class = "oddshade" colspan="3" align="center" height="30">	
							<div >Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!-- | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a> -->
                            </div>

							
						</td>
					</tr>	
				<%}%>			
			   	</table>
  			   </td>
  		  </tr>	
  		
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>


</BODY>

</HTML>

