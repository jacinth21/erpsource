
<%
	/**********************************************************************************
	 * File		 		: GmClinicalDownload.jsp
	 * Desc		 		: Clinical Data Download Page
	 * Version	 		: 1.0
	 * author			: Hardik Parikh
	 ************************************************************************************/
%>

<!-- clinical\GmClinicalDownload.jsp -->



<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<bean:define id="alDownloadHistoryList" name="frmClinicalDownload" property="alDownloadHistoryList" type="java.util.ArrayList"> </bean:define>
<bean:define id="strOpt" name="frmClinicalDownload" property="strOpt" type="java.lang.String"> </bean:define>
<bean:define id="limitedAccess" name="frmClinicalDownload" property="limitedAccess" type="java.lang.String"> </bean:define>
<bean:define id="studyList" name="frmClinicalDownload" property="studyList" type="java.lang.String"> </bean:define>
<bean:define id="haction" name="frmClinicalDownload" property="haction" type="java.lang.String"> </bean:define>
<bean:define id="lockExist" name="frmClinicalDownload" property="strLockExist" type="java.lang.String"> </bean:define>
<bean:define id="selFormName" name="frmClinicalDownload" property="selFormName" type="java.lang.String"> </bean:define>
<bean:define id="downloadPath" name="frmClinicalDownload" property="downloadPath" type="java.lang.String"> </bean:define>
<bean:define id="lockedStudyDisable" name="frmClinicalDownload" property="lockedStudyDisable" type="java.lang.String"> </bean:define>
<html>
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("CLINICAL_DATADOWNLOAD");
	String strStudyId = (String)session.getAttribute("strSessStudyId")==null?"":(String)session.getAttribute("strSessStudyId");
	String strUploadString=strStudyId+"_"+selFormName+"_DOWNLOADLOC";
	String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
%>
<head>
<title>Download File</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<style TYPE="text/css">
td.TableCaption {
	font-family: Verdana, Helvetica, sans-serif;
	background: #EEEEEE;
	font-size: 11px;
	line-height: 20px;
	font-weight: bold;
}
</style>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmClinicalDownload.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script>
	
	function fnOpenForm(Id)
	{   	
		var action = '/GmCommonFileOpenServlet?sId='+Id+'&uploadString=<%=strUploadString%>';
		document.frmClinicalDownload.action =action  
		document.frmClinicalDownload.submit();
	}
</script>
</head>
<BODY onLoad="fnOnPageLoad()">
<html:form action="/gmClinicalDownload.do">
<html:hidden property="strOpt" />
<html:hidden property="selFormName" />
<html:hidden property="msg"/>
<html:hidden property="strUploadHome"/>
<html:hidden property="haction" />
<ul class="style1">
</ul>
	<table border="0"  class="DtTable765" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="Header" align="left">Clinical Data	Download</td>  
				<td height="25" class="dhx_base_dhx_skyblue" align="Right"></td>     
    		</tr>
			<tr>
				<td width="100%" height="30">
				<table >
					<logic:notEqual name="frmClinicalDownload" property="msg" value="">
					<tr>
						<td colspan="2" height="20" align="center" class="RightBlueText">
							<bean:write name="frmClinicalDownload" property="msg"/>
							<%if(downloadPath!=null && downloadPath.length()>0){ %>
							<br>To View File: <a href="<%=downloadPath%>" target="_blank">Click here</a>			
							<%} %>
						</td>
					</tr>
					<tr><td class="Line" colspan="4"></td></tr>
					</logic:notEqual>
					<!-- Custom tag lib code modified for JBOSS migration changes -->	
					<tr>						
						<td  align="left" height="30">&nbsp;&nbsp;								
						Form List: <gmjsp:dropdown controlName="formName"  SFFormName="frmClinicalDownload" SFSeletedValue="formName"
							SFValue="alFormList" width="150" codeId="ID" onChange="fnLoadDownloadHistory();"  codeName="NAME" defaultValue="[Choose One]" /></td>						
					</tr>
					</tr>
					</tr>
					<% if(limitedAccess.equals("true")){ %>
					<tr> 
			            
			            <td colspan="2" align="center">	
			            <div id="downloadDiv" style="display:none" >		            
			           		<gmjsp:button controlId="addBtn" gmClass="button" buttonType="Load" name="Btn_Download" value="Download" style="WIDTH: 100px; HEIGHT: 16px" onClick="fnDownload();" disabled="<%=lockedStudyDisable%>"/>
							</div>
						</td>						
					</tr>
					<%}%>
					
				</table>
				</td>
			</tr>
			<%
				if ((haction.equals("load_download_history")) && alDownloadHistoryList.size()>0) {
					if(alDownloadHistoryList.size()>0){
			%>
			<tr>
				<td class="TableCaption" colspan="4">Previous Downloads</td>
			</tr>
			<table border="0" bordercolor="red" class="DtTable765" height="450" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="2">
					<display:table  name="requestScope.frmClinicalDownload.alDownloadHistoryList" export="false" class="its"  requestURI="/gmClinicalDownload.do" id="currentRowObject" decorator="com.globus.displaytag.beans.DTClinicalDownloadWrapper" freezeHeader="true">
            		<display:column property="LINK" title="ID" class="alignleft" sortable="true"/>
            		<display:column property="FILE_NAME" title="File Name" class="alignleft" />
            		<display:column property="CREATED_DATE" title="Date" class="alignleft" />
            		<display:column property="CREATED_BY" title="Downloaded By" class="alignleft" />
            		<display:column property="CANCEL" title="Cancel" class="alignleft" />
            		<display:column property="COMMENTS" title="Cancel Reason" class="alignleft" />
            	</display:table>
            	</td>
			</tr>
			<tr><td class="LLine" colspan="2"></td></tr>
			<%					
					if(limitedAccess.equals("true") && lockExist.equals("true") ){ %>
					
					<tr height="30"> 
			            <td colspan="2" align="center">
			           		<gmjsp:button controlId="submitBtn" gmClass="button" buttonType="Save" name="Btn_Submit" value="Submit" style="WIDTH: 100px; HEIGHT: 16px" onClick="fnSubmit();" disabled="<%=lockedStudyDisable%>"/>
						</td>
					</tr>
						<%}
						}else{
						%>
						
						<tr> 
			            <td colspan="4" class="RightText" align="left">
			           		There are no records for download history.
						</td>
						</tr>
						
						<% }
						}
						%>
			</table>
			

				</table>				
			
</html:form>		
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>
