
<%
	/**********************************************************************************
	 * File		 		: GmClinicalUpload.jsp
	 * Desc		 		: Upload - Upload page
	 * Version	 		: 1.0
	 * author			: Tarika Chandure
	 ************************************************************************************/
%>

<!--clinical\querysystem\GmClinicalUpload.jsp -->




<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@ include file="/common/GmHeader.inc" %>
<html>
<%	
	ArrayList alFormList = GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("alFormList"));
	String strFormList = GmCommonClass.parseNull((String) request.getParameter("strFormList"));
	ArrayList alUploadinfo = GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("alUploadinfo"));
	String strRecordupdated = GmCommonClass.parseNull(request.getParameter("recordUpdated"));
	String strAction = GmCommonClass.parseNull(request.getParameter("hAction"));
	request.setAttribute("alUploadinfo", alUploadinfo);
	String strUploadHome = GmCommonClass.getString("UPLOADHOME");
	String strFormName = GmCommonClass.parseNull((String)request.getAttribute("strFormName"));
	String strStudyName = GmCommonClass.parseNull((String)request.getAttribute("strStudyName"));
	String strUploadString= "";
	if (strFormName.equals("sf36")) {
		strUploadHome = GmCommonClass.getString("CLINICAL_UPLOADHOME");
		strUploadString = "CLINICAL_UPLOADHOME";
	}
	else{
		strUploadString = "UPLOADHOME";
	}
	request.setAttribute("strUploadHome", strUploadHome);
%>
<head>
<title>Upload File</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script type="text/javascript">
function fnReload(){
	    document.upform.hAction.value="reload";
	    document.upform.action="/GmClinicalUploadServlet?hAction=reload&strFormList="+document.upform.alFormList.value;
	    
	    document.upform.strFormList.value=document.upform.alFormList.value;
	   	var formName = document.upform.alFormList.options[document.upform.alFormList.options.selectedIndex].text;
	    document.upform.hFormName.value = formName;
	    document.upform.submit();    
}
function fnOpenForm(Id)
{
    document.upform.sId.value=Id;
    var formName = document.upform.alFormList.options[document.upform.alFormList.options.selectedIndex].text
	document.upform.action = "/GmCommonFileOpenServlet?sId="+Id+"&uploadString=<%=strUploadString%>&appendPath="+formName;
	document.upform.submit();

}
function upload()
  {
    if(document.upform.uploadfile.value == "")
    { 
	      alert("Select a file to upload");      
	      return false;
    }
    else{
	    var formName = document.upform.alFormList.options[document.upform.alFormList.options.selectedIndex].text;
	    document.upform.hFormName.value = formName;
 		document.upform.todo.value="upload";
	    document.upform.hAction.value="upload";
	    document.upform.hDisplayNm.value = 'Data Upload Screen';
	    document.upform.action="/GmClinicalUploadServlet?hAction=upload&strFormList="+document.upform.alFormList.value;
	    document.upform.Submit.disabled = true;// To disable submit button
	    document.upform.submit();    
    }
    toggledisplay();
  
  }
</script>
</head>

<BODY>

<form method="post" name="upform" enctype="multipart/form-data" >
<input type="hidden" name="strUploadHome" value="<%=strUploadHome %>"> 
<input type="hidden" name="hAction">
<input type="hidden" name="strFormList">
<input type="hidden" name="sId"> 
<input type="hidden" name="strRecordupdated">
<input type="hidden" name="hRedirectURL">
<input type="hidden" name="hDisplayNm">
<input type="hidden" name="hFormName" value=<%=strFormName%>>
<input type="hidden" name="hStudyName" value=<%=strStudyName%>>
		
<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="Header">Clinical Data	Upload</td> 
       			<td height="25" class="dhx_base_dhx_skyblue" align="Right"></td>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<tr>
				<td  class="RightText" height="30" colspan="2">&nbsp;Form List:&nbsp;
					<gmjsp:dropdown controlName="alFormList" seletedValue="<%=strFormList%>" value="<%=alFormList%>" codeId="ID" codeName="NAME" defaultValue="[Choose One]" onChange="fnReload()" />
				</td>
			</tr>
			<%
				if ((strAction.equals("reload") || strAction.equals("upload")) && !strFormList.equals("0")) {
			%>
			<tr>
				<td width="100%" height="100" valign="top" colspan="2">
				<div>
					<jsp:include page="/common/GmUpload.jsp" /> 
				</div>
				</td>
			</tr>
			<tr>
				<td class="TableCaption" colspan="2">Previous Uploads</td>
			</tr>
			<tr><td class="LLine" colspan="2"></td></tr>
			<tr>
				<td colspan="2">
				<display:table  name="requestScope.alUploadinfo" export="false" class="its"   requestURI="/gmClinicalUploadServlet?" id="currentRowObject" decorator="com.globus.displaytag.beans.DTClinicalUploadWrapper" freezeHeader="true" >
            		<display:column property="ID" title="#" class="alignleft" />
            		<display:column property="NAME" title="File Name" class="alignleft" />
            		<display:column property="CDATE" title="Date" class="alignleft" />
            		<display:column property="UNAME" title="Uploaded By" class="alignleft" />            	
            	</display:table>					
				</td>
			</tr>		
			<tr>
				<td colspan="2" class="Line" height="1"></td>
			</tr>
			<%
				}
			%>
			</table>
			</form>
			<%@ include file="/common/GmFooter.inc"%>
</body>
</html>
