<%
/**********************************************************************************
 * File		 		: GmRptByTreatment.jsp
 * Desc		 		: Report for Expected and Actual follow ups
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>

<!-- clinical\GmRptByTreatment.jsp -->



<!-- WEB-INF path corrected for JBOSS migration changes -->
 <%@include file="/common/GmHeader.inc"%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
 <%@ page import="com.globus.common.beans.GmGridFormat"%>
 

<% 

	//GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	GmGridFormat gmCrossTab = new GmGridFormat();
	HashMap hmExpected = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HMEXPECTED"));
	HashMap hmActual = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HMACTUAL"));

	
	// Setting the cross tab style 
	// Page Body Style
	//gmCrossTab.setGeneralHeaderStyle("ShadeLevel5");
	gmCrossTab.setRowHeight(15);
	
	
	gmCrossTab.setDivHeight(200);
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setGroupingRequired(true);
	gmCrossTab.setValueType(0);
	gmCrossTab.addLine("Name");
	gmCrossTab.setColumnWidth("Name", 210);
	gmCrossTab.setColumnWidth("Time of Surgery", 65);
	gmCrossTab.setColumnWidth("6 wk", 65);
	gmCrossTab.setColumnWidth("3 mo", 65);
	gmCrossTab.setColumnWidth("6 mo", 65);
	gmCrossTab.setColumnWidth("12 mo", 65);
	gmCrossTab.setColumnWidth("24 mo", 65);
	gmCrossTab.setColumnWidth("36 mo", 65);
	gmCrossTab.setColumnWidth("48 mo", 82);
	gmCrossTab.addLine("Time of Surgery");
	
		// Line Style information
//	gmCrossTab.setColumnDivider(false);

	gmCrossTab.setColumnLineStyle("borderDark");
	//gmCrossTab.setDecorator("com.globus.crosstab.beans.GmQueryByTimePointDecorator");
	gmCrossTab.setDecorator("com.globus.crosstab.beans.GmRptByTreatmentDecorator");
	

	
	String strGridData = gmCrossTab.generateGridXML(hmExpected);
	
	String strGridDataActual = gmCrossTab.generateGridXML(hmActual);

	String xyz = gmCrossTab.PrintCrossTabReport(hmExpected);
	String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Follow-up by Treatement </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmStudyFilterInc.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
document.onkeypress = function(){
		if(event.keyCode == 13){
			fnReport();
		}
		
	}
function fnSubmit()
{
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmRptByTreatement.strOpt.value = 'save';
	document.frmRptByTreatement.submit();
}

function fnLoad()
{
	document.frmRptByTreatement.selectAll.checked = false;
	document.frmRptByTreatement.strOpt.value = "";
	document.frmRptByTreatement.action = "/gmRptByTreatment.do?method=expectedFollowUpReport";
	document.frmRptByTreatement.submit();
}

function fnReport()
{
	document.frmRptByTreatement.action = "/gmRptByTreatment.do?method=expectedFollowUpReport";
	document.frmRptByTreatement.strOpt.value = "Report";
	fnStartProgress();
	document.frmRptByTreatement.submit();
}	

function fnCallOutOfWindow()
{
	document.frmRptByTreatement.action = "/gmRptByTreatment.do?method=outOfWindow";
	document.frmRptByTreatement.strOpt.value = "Report";
	document.frmRptByTreatement.submit();
}	

function fnMissingFollowUp()
{
	document.frmRptByTreatement.action = "/gmRptByTreatment.do?method=missingFollowUp";
	document.frmRptByTreatement.strOpt.value = "Report";
	document.frmRptByTreatement.submit();
}


var objGridData = '<%=strGridData%>';
var objGridDataActual = '<%=strGridDataActual%>';
	
function fnOnPageLoad(){

	gridObj = initGrid('dataGridDiv',objGridData,1);
	gridObjActual = initGrid('dataGridDivActual',objGridDataActual,1);
	
	
}	

</script>

</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();">
<html:form action="/gmRptByTreatment.do"  >
<html:hidden property="strOpt" />

	<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">Follow-Up by Treatment</td>
		</tr>
		<tr>
			<td width="750" height="100" valign="top">
				<table border="0"  width="100%" cellspacing="0" cellpadding="0">
                    <tr><td colspan="2" class="ELine"></td></tr> 
                    <tr>
						<td colspan="2" align="center">&nbsp;
	                    	<jsp:include page="/clinical/GmStudyFilterInc.jsp" >
							<jsp:param name="FORMNAME" value="frmRptByTreatement" />
							</jsp:include>	
						</td>
					</tr>
					<tr><td height="5"></td></tr>
                    <tr><td colspan="2" class="ELine"></td></tr> 
<% if(!hmExpected.isEmpty()) {  %>    
					<tr>
						<td colspan ="2"  class="CrossTabDashBoardHeader" align="left"> Expected </td>
					</tr>                
					<tr align="center">
						<td colspan ="2" align=center> <div id="dataGridDiv" style="" height="160" ></div></td>
					</tr>
					<tr class="shade">
						<td class = "oddshade" colspan="3" align="center" height="20">	
							<div class='exportlinks'>Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!-- | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a>
                                -->
                            </div>

							
						</td>
					</tr>		
<% } if(!hmActual.isEmpty()) {  %>  
<tr><td colspan="2" class="ELine"></td></tr>                   
					<tr>
						<td colspan ="2" class="CrossTabDashBoardHeader" align="left"> Actual </td>
					</tr>                
					<tr>
						<td colspan ="2" align=center> <div id="dataGridDivActual" style="" height="200"></td>
						
					</tr>
					<tr class="shade">
						<td class = "oddshade" colspan="3" align="center" height="20">	
							<div class='exportlinks'>Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObjActual.toExcel('/phpapp/excel/generate.php');"> Excel </a>
                                <!-- | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObjActual.toExcel('/phpapp/pdf/generate.php');">PDF </a> -->
                            </div>

							
						</td>
					</tr>		
					<tr>
						<td align=center> <a href=# onClick="fnCallOutOfWindow();"> <B><I> Out-of-Window </I></B> </td>
						<td align=center> <a href=# onClick="fnMissingFollowUp();"> <B><I> Missing Follow-Up </I></B> </td>
					</tr>
					
					
<% } %>					
			   	</table>
  			   </td>
  		  </tr>	
  		
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>

