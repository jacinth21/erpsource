<%
/**********************************************************************************
 * File		 		: GmPatientFormTrack.jsp
 * Desc		 		: Report for the enrolled patients by month
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmCommonConstants"%> 
<%@ page import="com.globus.common.beans.GmCommonClass"%> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ page import="com.globus.common.beans.GmLogger"%> 
<%@ page import="org.apache.log4j.Logger"%> 
<bean:define id="hmCrossTabReport" name="frmStudyFilterForm" property="hmCrossTabReport" type="java.util.HashMap"></bean:define>
<% 
//MNTTASK-8991 DHTMLX grid changed to Cross tab report to show "-" in Excel
GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
ArrayList alHeader = null;
alHeader = GmCommonClass.parseNullArrayList((ArrayList)hmCrossTabReport.get("Header"));
	int headerSize = alHeader.size();
	for (int i = 0;i<headerSize; i+=3)
	{
		gmCrossTab.addLine((String)alHeader.get(i));
		gmCrossTab.add_RoundOff((String)alHeader.get(i),"2");
	}
	gmCrossTab.setGeneralHeaderStyle("aaTopHeaderLightG");
	gmCrossTab.setDivHeight(200);
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setGroupingRequired(true);
	gmCrossTab.setValueType(0);
	gmCrossTab.setNoDivRequired(true); // this skips division for all columns
	gmCrossTab.addLine("Name");
	//gmCrossTab.setNameWidth(140);
	gmCrossTab.setColumnWidth("Name",110);
	gmCrossTab.setRowHighlightRequired(true);
		// Line Style information
	//gmCrossTab.setColumnDivider(false);
	gmCrossTab.setColumnLineStyle("borderDark");
	gmCrossTab.displayZeros(true);
	gmCrossTab.setDecorator("com.globus.crosstab.beans.GmCrtlEndPointRptDecorator");
	gmCrossTab.setExport(true, pageContext, "/gmCrtlEndPoint.do", "CriticalEndpoint");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Enrollment by month </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">

<script language="JavaScript" src="javascript/GlobusCommonScript.js"></script>
<script language="JavaScript" src="javascript/Message.js"></script>
<script language="JavaScript" src="javascript/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
</HEAD>

<BODY leftmargin="0" topmargin="0">
<html:form action="/gmCrtlEndPoint.do"  >
<html:hidden property="strOpt" />
	<table border="0"  class="DtTable1100" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">&nbsp;Critical Endpoints</td>
		<tr>
 		<tr>
			
			<td align="center" style="height: 50"> <%=gmCrossTab.PrintCrossTabReport(hmCrossTabReport)%></td>
			
	    </tr>				
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>

