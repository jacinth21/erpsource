 <%
/**********************************************************************************
 * File		 		: GmPatientRecord.jsp
 * Desc		 		: This screen is used for the Radio Graph Report
 * Version	 		: 1.0
 * author			: Hardik Parikh
************************************************************************************/
%>

<!-- clinical\GmPatientRecord.jsp -->

<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<% 
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	boolean showGrid=true;
	String gridData = (String)request.getAttribute("gridData");
	if(gridData!=null && gridData.indexOf("cell")==-1){
	   showGrid = false;			
	}	
	
	String strAction = (String)request.getAttribute("hAction");	
	String strButtonDisabled = "";
	
	ArrayList alPatientList = new ArrayList();
	ArrayList alPeriodList = new ArrayList();	
	ArrayList alTypeList = new ArrayList();	
	
	if (strAction == null)
	{
		strAction = (String)session.getAttribute("hAction");
	}
	strAction = (strAction == null)?"load_radiograph":strAction;		
	
	
	alPatientList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("AlPatient"));
	alPeriodList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("AlPeriod"));			
	alTypeList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("AlTyp"));
	
	ArrayList alXray = new ArrayList();
	alXray = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("X-RAY_list"));
	ArrayList alMRI = new ArrayList();
	alMRI = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("MRI_list"));
	ArrayList alCTScan = new ArrayList();
	alCTScan = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("CTSCAN_list"));
	
	String strTimePointId = (String) request.getAttribute("strTimePointId") == null ?"":(String)request.getAttribute("strTimePointId");
	String strPatientId = (String) request.getAttribute("strPatientId") == null ?"":(String)request.getAttribute("strPatientId");
	String strType= (String) request.getAttribute("strTypeId")== null ?"":(String)request.getAttribute("strTypeId");
	String strView = (String) request.getAttribute("strViewId")== null ?"":(String)request.getAttribute("strViewId");
	if (strView.equals("")){
		strView="0";
	}
	
	String strPatientRecordId = (String) request.getAttribute("strPatientRecordId")== null ?"":(String)request.getAttribute("strPatientRecordId");
	strButtonDisabled = GmCommonClass.parseNull((String)request.getAttribute("strButtonDisabled"));
	
	String strSrcDir = GmCommonClass.getString("RADIOLOGYUPLOADIR");
	strSrcDir = strSrcDir.replaceAll("\\\\", "\\\\\\\\"); 
	String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
%>


<HTML>
<HEAD>
<TITLE> Globus Medical: Radiograph Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/gmRadioGraphUpload.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
	
<script>

objGridData = '<%=gridData%>';
type = '<%=strType%>';
view = '<%=strView%>';
isData = '<%=showGrid%>';
baseDir = '<%=strSrcDir%>';
servletPath = '<%=strServletPath%>';
var lockedStudy = '<%=strButtonDisabled%>'; 

document.onkeypress = function(){
		if(event.keyCode == 13){
			fnSearch();
		}
		
	}
	
</script>
</HEAD>

<BODY  onLoad='fnOnPageLoad()'>
<FORM name="frmUploadRadioGraph" method="post" action = "<%=strServletPath%>/GmClinicalUploadServlet"> 

 
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hTimePointId" value="<%=strTimePointId%>">
<input type="hidden" name="hPatientId" value="<%=strPatientId%>">
<input type="hidden" name="hTypeId" value="<%=strType%>">
<input type="hidden" name="hViewId" value="<%=strView%>">
<input type="hidden" name="hPatientRecordId" value="<%=strPatientRecordId%>">

 <input type="hidden" name="hSrcDir" value="">
 <input type="hidden" name="hDestDir" value="">
 <input type="hidden" name="hTxnId" value="">
 <input type="hidden" name="hCancelType" value="">
 <input type="hidden" name="hFileName" value="">
 <input type="hidden" name="hTxnName" value="">
 
<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
<tr>
	<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width = "1" height = "1"></td>
	
	<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width = "1" height = "1"></td>
</tr>
<tr>
	<td bgcolor="#666666" width="1" rowspan="2"><img src="<%=strImagePath%>/spacer.gif" width = "1" height = "1"></td>
	<td height="25" class="Header">&nbsp;Upload Radiographs</td>
	<td bgcolor="#666666" width="1" rowspan="2"></td>
	<td bgcolor="#666666" width="1" rowspan="2"><img src="<%=strImagePath%>/spacer.gif" width = "1" height = "1"></td>
</tr>
<tr><td  valign="top">
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<% /******************************************************* 
		* Below code is used to fill all the combo box value  *
		*******************************************************/%>
	<tr><td>
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr><td class="LLine" colspan="4"></td></tr>
	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<tr class="evenshade">
		<td class="RightText" align="right" HEIGHT="25" width="80">&nbsp;Patient ID: </td>		
		<td >&nbsp;<gmjsp:dropdown controlName="cbo_Patient"  controlId="cbo_Patient" seletedValue="<%=strPatientId%>" 	
				  value="<%= alPatientList%>" codeId = "PATID"  codeName = "PATIDE"  defaultValue="[Choose One]" />
		 </td>
		<td class="RightText" align="right" width="80" >TimePoint:</td>
			<td width="220">&nbsp;<gmjsp:dropdown controlName="cbo_Period"  controlId="cbo_Period"  seletedValue="<%=strTimePointId%>" 	
				  value="<%= alPeriodList%>" codeId = "PERIODID"  codeName = "PERIODDS"  defaultValue="[Choose One]" />
			</td>
	</tr>
	<tr><td class="LLine" colspan="4"></td></tr>
	<tr class="oddshade">
		<td class="RightText" align="right" HEIGHT="25" width="80">&nbsp;Type: </td>		
		<td >&nbsp;<gmjsp:dropdown controlName="cbo_Type" controlId="cbo_Type"  seletedValue="<%=strType%>" 	
				  value="<%= alTypeList%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue="[Choose One]" onChange="fnlistView(this.value)" />
		 </td>
		<td class="RightText" align="right" width="80" >View :</td>
			<td width="220">&nbsp;<select class="RightText" name="cbo_View"  Width="150" id="cbo_View" >  
		        	<option value ="0">[Choose One]</option>
		        </select>
				  &nbsp;<gmjsp:button  gmClass="button" name="Btn_Load" value="Load" buttonType="Load" onClick ="fnSearch()"  
		style="WIDTH: 40px; HEIGHT: 16px"/>
			</td>
	</tr>
	<tr><td class="LLine" colspan="4"></td></tr>
	<tr class="evenshade">	
		<td colspan="4">
		<div id ="drpdwn" style="display:hidden">
			<gmjsp:dropdown controlName="cbo_XRay"    width="150" value="<%=alXray%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" controlId="cbo_XRay" />
			<gmjsp:dropdown controlName="cbo_MRI"    width="150" value="<%=alMRI%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" controlId="cbo_MRI"/>
			<gmjsp:dropdown controlName="cbo_CTScan"   width="150" value="<%=alCTScan%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" controlId="cbo_CTScan"/>
		</div>
		</td>			
	</tr>
	
   
	</table>
	</td>	
	</tr>
	<%
	if(strAction.equals("search_radiograph")){ 
		if(showGrid){
					
	%>
      				<tr><td colspan="2" bgcolor="#cccccc"></td></tr> 
      				<tr> 
			            <td colspan="2">
			           		<div id="dataGridDiv" class="grid"></div>
						</td>
					</tr>
					 <!--<tr> 
			            <td colspan="2">
			          		&nbsp;
						</td>
					</tr>
					
					 <tr> 
			            <td colspan="2">
			          		&nbsp;
						</td>
					</tr>
					--><tr class="oddshade">
						<td colspan="4" align="center" height="30">	
							<div class='exportlinks'>Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!--  | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a>  -->
                    		</div>
						</td>
					</tr>	        
                    <tr> 
			            <td colspan="2">
			          		&nbsp;
						</td>
					</tr>            
                    
	<%
		}else{			
	%>
	 	 			<tr> 
			            <td colspan="2">
			          		&nbsp;
						</td>
					</tr>
	
	  				<tr> 
			            <td colspan="2">
								Nothing found to display.
						</td>
					</tr>	
	<%		
		}
	}%>
	 <% String strDisableBtnVal = strButtonDisabled;
		if(strDisableBtnVal.equals("disabled")){
			strDisableBtnVal ="true";
		}
	 %>
					<tr> 
			            <td colspan="2" align="center">
			           		<gmjsp:button controlId="addBtn" gmClass="button" name="Btn_Load" value="New Image" buttonType="Save"  
		style="WIDTH: 120px; HEIGHT: 16px" onClick="fnAddMore()" disabled="<%=strDisableBtnVal %>"/>&nbsp;&nbsp;
		<gmjsp:button controlId="viewSelected" gmClass="Button" buttonType="Load" name="Btn_Load" value="View Selected" onClick="fnViewSelected()"  
		style="WIDTH: 100px; HEIGHT: 16px"/>
						</td>
					</tr>
					<tr> 
			            <td colspan="2">
			          		&nbsp;
						</td>
					</tr>		
					
	</table>
</td>
</tr>

      		
	
</table>	

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
