 <%
/**********************************************************************************
 * File		 		: GmPatientLogSummry.jsp
 * Desc		 		: This screen displys Patient Log
 * Version	 		: 1.0
 * author			: RShah
************************************************************************************/
%>

<!-- clinical\GmPatientLogSummary.jsp -->


<%@ page language="java" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.ArrayList" %>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
 <%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%> 
<%
GmServlet gm = new GmServlet();
gm.checkSession(response,session);
String strSessPatientId = (String)session.getAttribute("strSessPatientId");
String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
%>
<bean:define id="gridData" name="frmPatientLogForm" property="gridXmlData" type="java.lang.String"> </bean:define>
<HTML>
<HEAD>
<TITLE> Globus Medical: Enterprise Portal</TITLE>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmPatientLog.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

<script>
	var objGridData;
	objGridData = '<%=gridData%>';
	document.onkeypress = function(){
		if(event.keyCode == 13){
			fnLoad();
		}
		
	}
</script>
<bean:define id="strOpt" name="frmPatientLogForm" property="strOpt" type="java.lang.String"> </bean:define>

</HEAD>

<BODY onLoad="fnOnPageLoad();">
 
<html:form action="/gmPatientLogSummary.do">
<html:hidden property="strOpt" />
	<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header" colspan="4">Comments Summary</td>
		</tr>
		<%
		if(strSessPatientId.equals(""))
		 {
		%>		
		<tr><td colspan="4" class="LLine" height="1"></td></tr> 
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>  
			<td class="RightTableCaption" align="right" HEIGHT="25" width="12%">&nbsp;Patient ID:</td>
			<td colspan="3" HEIGHT="24">&nbsp;<gmjsp:dropdown controlName="patientId" SFFormName="frmPatientLogForm" SFSeletedValue="patientId"
						SFValue="alPatientId" codeId = "PATID"  codeName = "PATIDE"  defaultValue= "[Choose One]" />
			</td>
		</tr>
		<%} %>
		<tr><td colspan="4" class="LLine" height="1"></td></tr> 
		<tr class="shade">			
			<td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;TimePoint:</td>
			<td HEIGHT="24">&nbsp;<gmjsp:dropdown controlName="studyPeriod" SFFormName="frmPatientLogForm" SFSeletedValue="studyPeriod"
						SFValue="alStudyPeriod" codeId = "PERIODID"  codeName = "PERIODDS"  defaultValue= "[Choose One]" />
			</td>
			<td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;Form:</td>
			<td HEIGHT="24">&nbsp;<gmjsp:dropdown controlName="studyForm" SFFormName="frmPatientLogForm" SFSeletedValue="studyForm"
						SFValue="alFormList" codeId = "FORMID"  codeName = "FORMNM"  defaultValue= "[Choose One]" />
			</td>				
		</tr>
		<tr><td colspan="4" class="LLine" height="1"></td></tr> 
		<tr>
	        <td colspan="1" class="RightTableCaption" align="right" HEIGHT="25">&nbsp;Date Range :</td>
	        <td colspan="2">&nbsp;<gmjsp:calendar SFFormName="frmPatientLogForm" controlName="logFrom" toControlName ="logTo" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />	&nbsp;&nbsp;
                     
            </td>
            <td>&nbsp;&nbsp;
            <gmjsp:button value="Load" style="width: 5em; height: 22" gmClass="button" buttonType="Load" onClick="fnLoad();" /></td>
            
        </tr>
        <tr><td colspan="4" class="LLine" height="1"></td></tr>
        <tr> 
           <%if(gridData.indexOf("cell") != -1){%>
				<td colspan="4">
		    		<div id="dataGridDiv" width="764" class="grid" style="height:330px;"></div>
				</td>
			<%}else if(!gridData.equals("")){%>
				<tr><td colspan="4" align="center" class="RightText">Nothing found to display</td></tr>
			<%}else{%>
				<tr><td colspan="4" align="center" class="RightText">No data available</td></tr>
			<%} %>
   	 	</tr> 	
   	 	<%if(gridData.indexOf("cell") != -1) {%>
   	 	<tr class="oddshade">
		<td colspan="4" align="center" height="30">	
					<div class='exportlinks'>Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="fnDownloadExcel();"> Excel </a> <!--  | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a>  -->
                    </div>
		</td>
		</tr>
		<%} %>	        		
    </table>		 
</html:form>
<%@ include file="/common/GmFooter.inc" %>   
</BODY>
</HTML>
