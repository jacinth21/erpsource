<%
/**********************************************************************************
 * File		 		: GmClinicalHome.jsp
 * Desc		 		: This screen is used for the Clincial Dashboard DashBoard Report
 * Version	 		: 1.0
 * author			: Richard K
************************************************************************************/
%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib uri="/WEB-INF/gmjsp-taglib.tld" prefix="gmjsp"%>						

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	String strCommonPath = GmCommonClass.getString("GMCOMMON");


	String strDeptId = (String)session.getAttribute("strSessDeptId")==null?"":(String)session.getAttribute("strSessDeptId");
	String strAccessLvl = (String)session.getAttribute("strSessAccLvl")==null?"":(String)session.getAttribute("strSessAccLvl");

	int intAccessLvl = Integer.parseInt(strAccessLvl);
	boolean bolAccess = false;
	int intSize = 0 ;
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmValue = new HashMap();
	ArrayList alPending = new ArrayList();
	String strTemp = "";
	
	if ((strDeptId.equals("O") && intAccessLvl > 3))
	{
		bolAccess = true;
	}

	//Common Variable used for looping
	String strAccountID = "";
	String strAccountNm = "";
	String strPreAccountID = ""; // Assigned with some dummy initial value 
	String strPeriodID = "";
	String strPrePeriodID = "";
	String strPeriodDS = "";
	String strPrePatientIDE = ""; // Assigned with some dummy initial value 				
	StringBuffer strDivTab  = new StringBuffer();
	String strStudyID = "";
	String strFromID = "";
	String strPatientPKey = "";
	String strStudyPKey = "";
	String strPatientIDE = "";
	String strPatientPeriod = "";
	int patientCount = 0;
	int formCount = 0;
	String strTempAccountId = "";
	
	ArrayList alCras = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("CRAS"));
	ArrayList alStudyList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("CRSTUDYLIST"));
	String strSelectedCraID = GmCommonClass.parseNull((String)request.getAttribute("CRAID"));
	
	String strSelectedStudyID = GmCommonClass.parseNull((String)request.getParameter("Cbo_StudyID"));
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Accounts Dashboard </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script>
function fnCallInv(po,id)
{
	document.frmAccount.hId.value = id;
	document.frmAccount.hPO.value = po;
	document.frmAccount.hMode.value = "INV";
	document.frmAccount.hPgToLoad.value = "GmInvoiceServlet";
	document.frmAccount.submit();
}

function Toggle(val)
{
	
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		//trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		//tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		//trobj.className="";
		//tabobj.style.background="#ffffff";
	}
}

function fnFormDetails(valFormID, valStudy, valStudyPeriodKey, valCboPeriod, valPatientID)
{
	//alert(valFormID + " = " + valStudy  + " = " + valStudyPeriodKey + " = " + valPatientID );
	document.frmClinicalDB.Cbo_Form.value = valFormID;
	document.frmClinicalDB.Cbo_Study.value = valStudy;
	document.frmClinicalDB.hStPerId.value = valStudyPeriodKey;
	document.frmClinicalDB.Cbo_Period.value = valCboPeriod;
	document.frmClinicalDB.Cbo_Patient.value = valPatientID;
	document.frmClinicalDB.submit();
}

function fnSubmit()
{
	 document.frmClinicalDB.action = "<%=strServletPath%>/GmClinicDashBoardServlet";
	 document.frmClinicalDB.hAction.value="Load";
     document.frmClinicalDB.submit();
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmClinicalDB" method="POST" action="<%=strServletPath%>/GmFormDataEntryServlet">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hPgToLoad" value="GmEditOrderServlet">
<input type="hidden" name="hSubMenu" value="Trans">
<input type="hidden" name="hMode" value="">
<input type="hidden" name="hFrom" value="Dashboard">
<input type="hidden" name="hPO" value="">
<input type="hidden" name="hAction" value="LoadQues">
<input type="hidden" name="Cbo_Form" value="">
<input type="hidden" name="Cbo_Period" value="">
<input type="hidden" name="hStPerId" value="">
<input type="hidden" name="Cbo_Study" value="">
<input type="hidden" name="Cbo_Patient" value="">


<BR>
<table border="0" width="900" cellspacing="0" cellpadding="0">
	<tr><td colspan="3" bgcolor="#666666"></td></tr>
	<tr>
	<td bgcolor="#666666" width="1"></td>
	<td width="898" valign="top">
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr>
	 <td  height="25" class="RightDashBoardHeader"><table><tr>
	<td width="75%" class="RightDashBoardHeader">Clinical Dashboard</td>
	<td align="right" class="RightDashBoardHeader">Study List :</td>
	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<td width="10%" align="left">
      <gmjsp:dropdown controlName="Cbo_StudyID" seletedValue="<%=strSelectedStudyID%>" 	
			onChange="fnSubmit();"	  value="<%= alStudyList%>" codeId = "ID"  codeName = "NAME"  defaultValue="ALL" />
	</td>
	<td align="right" class="RightDashBoardHeader">CRA Name : </td>
	<td width="10%" align="left">
      <gmjsp:dropdown controlName="Cbo_CraId" seletedValue="<%=strSelectedCraID%>" 	
			onChange="fnSubmit();"	  value="<%= alCras%>" codeId = "ID"  codeName = "NAME"  defaultValue="ALL" />
	</td>
	</tr></table></td>	
	
	</tr>	
	<tr><td class="Line" ></td></tr>
	<tr><td  height="20" ></td></tr>
	<tr><td class="Line" ></td></tr>
	<!--/************************ Below Code is to view the for *************** -->	
	<tr><td  height="25" class="RightDashBoardHeader">Dashboard - Pending Forms</td>	</tr>
	<tr><td class="Line" ></td></tr>
		
	<tr><td >
	<div style="overflow:auto; height:300px;">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
		 <thead>
		<tr class="aaTopHeader" height="18" style="position:relative; top:expression(this.offsetParent.scrollTop);">
			<th  align="center" width="180"> Account / Patient IDE </th>
			<th   align="center" width=320> Form Name</th>						
			<th  align="center" width="80"> Earliest Due Date </th>			
			<th  align="center" width="80"> <b>Expected Date</b></th>			
			<th  align="center"	width="80"> Latest Due Date</th>		
			<th  align="center"	width="40"> Edit</th>
		</tr>
		</thead>
		<tbody>
		<tr><td class="Line" colspan="8"></td></tr>
	<%			alPending = (ArrayList)hmReturn.get("PENDINGFORM");	
				intSize = alPending.size();
				
				strPreAccountID = "INT#"; // Assigned with some dummy initial value 
				strPrePatientIDE = "INT#"; // Assigned with some dummy initial value 				
				strPrePeriodID = "INT#"; // Assigned with some dummy initial value 				
				
				for (int i=0;i<intSize;i++)
				{
					hmValue = (HashMap)alPending.get(i);
					strAccountID = GmCommonClass.parseNull((String)hmValue.get("A_ID"));
					strAccountNm = GmCommonClass.parseNull((String)hmValue.get("A_NAME"));
					strAccountNm = strAccountNm.equals("")?"No Name":strAccountNm;
					strPeriodID  = GmCommonClass.parseNull((String)hmValue.get("S_P_ID"));
					strPeriodDS  = GmCommonClass.parseNull((String)hmValue.get("S_DS"));
					
					strStudyID 	= GmCommonClass.parseNull((String)hmValue.get("S_ID"));
					strFromID 	= GmCommonClass.parseNull((String)hmValue.get("F_ID"));
					strPatientPKey = GmCommonClass.parseNull((String)hmValue.get("P_KEY"));
					strStudyPKey = GmCommonClass.parseNull((String)hmValue.get("S_P_KEY"));
					strPatientIDE = GmCommonClass.parseNull((String)hmValue.get("P_IDE_NO"));
					strPatientPeriod = strPatientPKey +"-"+ strPeriodDS;

				//	System.out.println("Outer:"+strPatientPeriod);
					
					//****************************************************
					// Below section is used to create Account -- First Level 
					//****************************************************
					if (!strAccountID.equals(strPreAccountID) && !strAccountID.equals(strTempAccountId))
					{
						
						strDivTab.setLength(0);
						// To reset the old value 
						if ( !strPreAccountID.equals("INT#"))
						{
							strDivTab.append("</table></td></tr></table></td></tr>" );
							strPrePatientIDE = "INT#";
						}
						
						strDivTab.append("<tr id=tr" + i + "VA class=ShadeDarkGrayTD ><td colspan=8 align=left  height=20 > &nbsp;");
						strDivTab.append("<B><A class=RightText title='Click to Expand the Phase ' ");
						strDivTab.append("href=\"javascript:Toggle('");
						strDivTab.append(i);
						strDivTab.append("VA')\">");
						strDivTab.append(strAccountNm);
						strDivTab.append("</a> </B></td></tr>");
						strDivTab.append("<tr><td colspan=8 class=borderDark ></td></tr>");
						strDivTab.append("<tr><td colspan=8><div style=display:none ");
						strDivTab.append(" id=div" + i + "VA> ");						
						strDivTab.append(" 	<table width=100% cellpadding=0 cellspacing=0 border=0 id=tab" + i +  "VA > " );
						
						strPreAccountID = strAccountID;
						
						out.println(strDivTab.toString());						
					}				
					
					
					//****************************************************
					// Below section is used to create the Patient IDE -- Second Level
					//****************************************************
					if (!strPatientPeriod.equals(strPrePatientIDE))
					{
						strDivTab.setLength(0);
						// To reset the old value 
						if ( !strPrePatientIDE.equals("INT#"))
						{
							strDivTab.append("</table></td></tr>" );
						}
						
						strDivTab.append("<tr id=tr" + i + "VPI class=ShadeLightGrayTD ><td colspan=8 align=left height=20> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
						strDivTab.append("<A class=RightText title='Click to Expand the Form' ");
						strDivTab.append("href=\"javascript:Toggle('");
						strDivTab.append(i);
						strDivTab.append("VPI')\">");
						strDivTab.append(strPatientIDE +" - "+strPeriodDS);
						strDivTab.append("</a></td>");
						strDivTab.append("<tr><td colspan=8 class=borderDark ></td></tr>");
						strDivTab.append("<tr><td colspan=8><div style=display:none ");
						strDivTab.append(" id=div" + i + "VPI> ");
						strDivTab.append(" 	<table width=100% cellpadding=0 cellspacing=0 border=0 id=tab" + i +  "VPI > " );

						strPrePatientIDE = strPatientPeriod;
						out.println(strDivTab.toString());
						
					}
					
					
					
					
	%>				<tr class="RightTextSmall"> 
					<td align="left" width="150" height="20" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</td>
					<td align="left" width="320"> <%=GmCommonClass.parseNull((String)hmValue.get("F_NAME"))%></td>
     				<td align="left" width="80"> <%=GmCommonClass.parseNull((String)hmValue.get("EXP_START_DATE"))%></td>					
					<td align="center" width="80"> <b> <%=GmCommonClass.parseNull((String)hmValue.get("EXP_DATE"))%> </b></td>
					<td align="center" width="80"> <%=GmCommonClass.parseNull((String)hmValue.get("FINAL_DUE_DATE"))%></td>
				<!--	<td align="center" width="40">    
					<% strTemp = (String)hmValue.get("OVER_DUE");
					   if (strTemp.equals("Y"))
					   { %>
					   		<img id="imgEdit" src="<%=strImagePath%>/Past_Due.gif"  width="12" height="12" title="Form Overdue and crossed grace period"  />
					<% } else {%>
							-
					<% }%>		
					</td> -->
					<td align="center" width="40"> <img id="imgEdit" style="cursor:hand" 
	 						src="<%=strImagePath%>/edit_icon.gif" title="Click to Edit Patient Question Information" 
	 						width="14" height="14" 
	 						onClick="javascript:fnFormDetails('<%=strFromID%>', '<%=strStudyID%>', '<%=strStudyPKey%>' , '<%=strPeriodID%>',  '<%=strPatientPKey%>' )"/> </td>	 						
					</tr>
					<tr><td class="borderDark" colspan="8"></td></tr>
				 
	<%			}
		if (intSize <= 0 ) 
		{ 
			out.println(" <tr class=RightTableCaption align=center > <td colspan=8 height=30>");
			out.println(" No Pending Forms </td> <tr> ");
			out.println(" <tr><td class=borderDark colspan=8></td></tr>");
		} 
		else
		{
			out.println("</table></td></tr>"); // For the Account
			out.println("</table></td></tr>"); //  For the Phase  <!--Final Inner Table -->
		}
	%>	</tbody>			
		</table></td></tr> <!-- Pending Form Table -->

	<tr><td  height="20" ></td></tr>
	<!--/************************ Below Code is to List Pending Verification *************** -->	
	<tr><td  height="25" class="RightDashBoardHeader">Dashboard - Out-of-Window </td>	</tr>
	<tr><td class="Line" ></td></tr>
		
	<tr><td >
	<div style="overflow:auto; height:300px;">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
		 <thead>
		<tr class="aaTopHeader" height="18" style="position:relative; top:expression(this.offsetParent.scrollTop);">
			<th  align="center" width="180"> Account / Patient IDE </th>
			<th   align="center" width=320> Form Name</th>						
			<th  align="center" width="80"> Earliest Due Date </th>			
			<th  align="center" width="80"><b> Expected Date </b></th>			
			<th  align="center"	width="80"> Latest Due Date</th>		
			<th  align="center"	width="40"> Edit</th>			
		</tr>
		</thead>
		<tbody>
		<tr><td class="Line" colspan="8"></td></tr>
	<%			alPending = (ArrayList)hmReturn.get("OUTOFWINDOW");	
				intSize = alPending.size();
				strPreAccountID = "INT#"; // Assigned with some dummy initial value 
				strPrePatientIDE = "INT#"; // Assigned with some dummy initial value 				
				
				for (int i=0;i<intSize;i++)
				{
					hmValue = (HashMap)alPending.get(i);
					strAccountID = GmCommonClass.parseNull((String)hmValue.get("A_ID"));
					strAccountNm = GmCommonClass.parseNull((String)hmValue.get("A_NAME"));
					strAccountNm = strAccountNm.equals("")?"No Name":strAccountNm;
					strPeriodID  = GmCommonClass.parseNull((String)hmValue.get("S_P_ID"));
					strPeriodDS  = GmCommonClass.parseNull((String)hmValue.get("S_DS"));
					
					strStudyID 	= GmCommonClass.parseNull((String)hmValue.get("S_ID"));
					strFromID 	= GmCommonClass.parseNull((String)hmValue.get("F_ID"));
					strPatientPKey = GmCommonClass.parseNull((String)hmValue.get("P_KEY"));
					strStudyPKey = GmCommonClass.parseNull((String)hmValue.get("S_P_KEY"));
					strPatientIDE = GmCommonClass.parseNull((String)hmValue.get("P_IDE_NO"));
					strPatientPeriod = strPatientPKey +"-"+ strPeriodDS;
					
				//	System.out.println(strPeriodID + strPeriodDS);

					//****************************************************
					// Below section is used to create Phase/Per Group 
					//****************************************************
					if (!strAccountID.equals(strPreAccountID))
					{
						
						strDivTab.setLength(0);
						// To reset the old value 
						if ( !strPreAccountID.equals("INT#"))
						{
							strDivTab.append("</table></td></tr></table></td></tr>" );
							strPrePatientIDE = "INT#";
						}
						
						strDivTab.append("<tr id=tr" + i + "OP class=ShadeDarkGrayTD ><td colspan=8 align=left height=20 > &nbsp;");
						strDivTab.append("<B><A class=RightText title='Click to Expand the Phase ' ");
						strDivTab.append("href=\"javascript:Toggle('");
						strDivTab.append(i);
						strDivTab.append("OP')\">");
						strDivTab.append(strAccountNm );
						strDivTab.append("</a> </B></td></tr>");
						strDivTab.append("<tr><td colspan=8 class=borderDark ></td></tr>");
						strDivTab.append("<tr><td colspan=8><div style=display:none ");
						strDivTab.append(" id=div" + i + "OP> ");						
						strDivTab.append(" 	<table width=100% cellpadding=0 cellspacing=0 border=0 id=tab" + i +  "OP > " );
						
						strPreAccountID = strAccountID;
					
						out.println(strDivTab.toString());
						
					}				
					
					//****************************************************
					// Below section is used to create the Account Group 
					//****************************************************
					if (!strPatientPeriod.equals(strPrePatientIDE))
					{
						strDivTab.setLength(0);
						// To reset the old value 
						if ( !strPrePatientIDE.equals("INT#"))
						{
							strDivTab.append("</table></td></tr>" );
						}
						
						strDivTab.append("<tr id=tr" + i + "OA class=ShadeLightGrayTD ><td colspan=8 align=left height=20 > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
						strDivTab.append("<A class=RightText title='Click to Expand the Form' ");
						strDivTab.append("href=\"javascript:Toggle('");
						strDivTab.append(i);
						strDivTab.append("OA')\">");
						strDivTab.append(strPatientIDE + " - "+strPeriodDS);
						strDivTab.append("</a></td></tr>");
						strDivTab.append("<tr><td colspan=8 class=borderDark ></td></tr>");
						strDivTab.append("<tr><td colspan=8><div style=display:none ");
						strDivTab.append(" id=div" + i + "OA> ");
						strDivTab.append(" 	<table width=100% cellpadding=0 cellspacing=0 border=0 id=tab" + i +  "OA > " );
						
						strPrePatientIDE = strPatientPeriod;
					
						out.println(strDivTab.toString());
						
					}
					
	%>				<tr class="RightTextSmall"> 
					<td align="left" width="180" height="20" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td align="left" width="320"> <%=GmCommonClass.parseNull((String)hmValue.get("F_NAME"))%></td>
					<td align="left" width="80"> <%=GmCommonClass.parseNull((String)hmValue.get("EXP_START_DATE"))%></td>					
					<td align="center" width="50"> <b><%=GmCommonClass.parseNull((String)hmValue.get("EXP_DATE"))%></b></td>					
					<td align="center" width="80"> <%=GmCommonClass.parseNull((String)hmValue.get("FINAL_DUE_DATE"))%></td>
					<td align="center" width="40"> <img id="imgEdit" style="cursor:hand" 
	 						src="<%=strImagePath%>/edit_icon.gif" title="Click to Edit Patient Question Information" 
	 						width="14" height="14" 
	 						onClick="javascript:fnFormDetails('<%=strFromID%>', '<%=strStudyID%>', '<%=strStudyPKey%>' , '<%=strPeriodID%>' , '<%=strPatientPKey%>' )"/> </td>
					</tr>
					<tr><td class="borderDark" colspan="8"></td></tr>
	<%			}
	
			if (intSize <= 0 ) 
		{ 
			out.println(" <tr class=RightTableCaption align=center > <td colspan=8 height=30>");
			out.println(" No Out Of Window </td> <tr> ");
			out.println(" <tr><td class=borderDark colspan=8></td></tr>");
		} 
		else
		{
			out.println("</table></td></tr>"); // For the Account
			out.println("</table></td></tr>"); //  For the Phase  <!--Final Inner Table -->
		}
	
	%>	</tbody>			
		</table></td></tr> <!-- Pending Verification Table -->
		

<tr><td  height="20" ></td></tr>
	<!--/************************ Below Code is to List Pending Verification *************** -->	
	<tr><td  height="25" class="RightDashBoardHeader">Dashboard - Pending Verification </td>	</tr>
	<tr><td class="Line" ></td></tr>
		
	<tr><td >
		<div style="overflow:auto; height:300px;">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
		 <thead>
		<tr class="aaTopHeader" height="18" style="position:relative; top:expression(this.offsetParent.scrollTop);">
			<th  align="center" width="180"> Account / Patient IDE </th>
			<th   align="center" width=320> Form Name</th>						
			<!--<th  align="center" width="50"> Expected Date</th>	-->		
			<th  align="center"	width="70"> Exam Date</th>			
			<th  align="center"	width="70"> Entered By </th>			
			<th  align="center"	width="40"> Edit</th>
		</tr>
		</thead>
		<tbody>
		<tr><td class="Line" colspan="8"></td></tr>
	<%			alPending = (ArrayList)hmReturn.get("PENDINGVERFICATION");	
				intSize = alPending.size();
				strPreAccountID = "INT#"; // Assigned with some dummy initial value 
				strPrePatientIDE = "INT#"; // Assigned with some dummy initial value 				
				
				for (int i=0;i<intSize;i++)
				{
					hmValue = (HashMap)alPending.get(i);
					strAccountID = GmCommonClass.parseNull((String)hmValue.get("A_ID"));
					strAccountNm = GmCommonClass.parseNull((String)hmValue.get("A_NAME"));
					strAccountNm = strAccountNm.equals("")?"No Name":strAccountNm;
					strPeriodID  = GmCommonClass.parseNull((String)hmValue.get("S_P_ID"));
					strPeriodDS  = GmCommonClass.parseNull((String)hmValue.get("S_DS"));
					
					strStudyID 	= GmCommonClass.parseNull((String)hmValue.get("S_ID"));
					strFromID 	= GmCommonClass.parseNull((String)hmValue.get("F_ID"));
					strPatientPKey = GmCommonClass.parseNull((String)hmValue.get("P_KEY"));
					strStudyPKey = GmCommonClass.parseNull((String)hmValue.get("S_P_KEY"));
					strPatientIDE = GmCommonClass.parseNull((String)hmValue.get("P_IDE_NO"));
        			strPatientPeriod = strPatientPKey +"-"+ strPeriodDS;
					
				//	System.out.println(strPeriodID + strPeriodDS);

					//****************************************************
					// Below section is used to create Phase/Per Group 
					//****************************************************
					if (!strAccountID.equals(strPreAccountID))
					{
						
						strDivTab.setLength(0);
						// To reset the old value 
						if ( !strPreAccountID.equals("INT#"))
						{
							strDivTab.append("</table></td></tr></table></td></tr>" );
							strPrePatientIDE = "INT#";
						}
						
						strDivTab.append("<tr id=tr" + i + "P class=ShadeDarkGrayTD ><td colspan=8 align=left height=20 > &nbsp;");
						strDivTab.append("<B><A class=RightText title='Click to Expand the Phase ' ");
						strDivTab.append("href=\"javascript:Toggle('");
						strDivTab.append(i);
						strDivTab.append("P')\">");
						strDivTab.append(strAccountNm );
						strDivTab.append("</a> </B></td></tr>");
						strDivTab.append("<tr><td colspan=8 class=borderDark ></td></tr>");
						strDivTab.append("<tr><td colspan=8><div style=display:none ");
						strDivTab.append(" id=div" + i + "P> ");						
						strDivTab.append(" 	<table width=100% cellpadding=0 cellspacing=0 border=0 id=tab" + i +  "P > " );
						
						strPreAccountID = strAccountID;
					
						out.println(strDivTab.toString());
						
					}				
					
					//****************************************************
					// Below section is used to create the Account Group 
					//****************************************************
					if (!strPatientPeriod.equals(strPrePatientIDE))
					{
						strDivTab.setLength(0);
						// To reset the old value 
						if ( !strPrePatientIDE.equals("INT#"))
						{
							strDivTab.append("</table></td></tr>" );
						}
						
						strDivTab.append("<tr id=tr" + i + "A class=ShadeLightGrayTD ><td colspan=8 align=left height=20 > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
						strDivTab.append("<A class=RightText title='Click to Expand the Form' ");
						strDivTab.append("href=\"javascript:Toggle('");
						strDivTab.append(i);
						strDivTab.append("A')\">");
						strDivTab.append(strPatientIDE + " - "+strPeriodDS);
						strDivTab.append("</a></td></tr>");
						strDivTab.append("<tr><td colspan=8 class=borderDark ></td></tr>");
						strDivTab.append("<tr><td colspan=8><div style=display:none ");
						strDivTab.append(" id=div" + i + "A> ");
						strDivTab.append(" 	<table width=100% cellpadding=0 cellspacing=0 border=0 id=tab" + i +  "A > " );
						
						strPrePatientIDE = strPatientPeriod;
					
						out.println(strDivTab.toString());
						
					}
					
	%>				<tr class="RightTextSmall"> 
					<td align="left" width="180" height="20" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</td>
					<td align="left" width="320"> <%=GmCommonClass.parseNull((String)hmValue.get("F_NAME"))%></td>
					<!--<td align="center" width="50"> <%=GmCommonClass.parseNull((String)hmValue.get("EXP_DATE"))%></td> -->					
					<td align="center" width="70"> <%=GmCommonClass.parseNull((String)hmValue.get("E_DATE"))%></td>
					<td align="center" width="70"> <%=GmCommonClass.parseNull((String)hmValue.get("ENTERED_BY"))%></td>
					<td align="center" width="40"> <img id="imgEdit" style="cursor:hand" 
	 						src="<%=strImagePath%>/edit_icon.gif" title="Click to Edit Patient Question Information" 
	 						width="14" height="14" 
	 						onClick="javascript:fnFormDetails('<%=strFromID%>', '<%=strStudyID%>', '<%=strStudyPKey%>' , '<%=strPeriodID%>' , '<%=strPatientPKey%>' )"/> </td>
					</tr>
					<tr><td class="borderDark" colspan="8"></td></tr>
	<%			}
	
			if (intSize <= 0 ) 
		{ 
			out.println(" <tr class=RightTableCaption align=center > <td colspan=8 height=30>");
			out.println(" No Pending Verification </td> <tr> ");
			out.println(" <tr><td class=borderDark colspan=8></td></tr>");
		} 
		else
		{
			out.println("</table></td></tr>"); // For the Account
			out.println("</table></td></tr>"); //  For the Phase  <!--Final Inner Table -->
		}
	
	%>	</tbody>			
		</table></td></tr> <!-- Pending Verification Table -->
		
		
	</table></td>
	<td bgcolor="#666666" width="1"></td>
	</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
</table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
