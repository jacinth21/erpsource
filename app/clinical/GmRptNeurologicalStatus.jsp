<%
/**********************************************************************************
 * File		 		: gmRptOutcomeScoresScore.jsp
 * Desc		 		: Report for Outcomescore
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>


<!-- clinical\GmRptNeurologicalStatus.jsp -->
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@include file="/common/GmHeader.inc"%>
 
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmGridFormat"%>
 
<bean:define id="hmCrossTabReport" name="frmOutcomeReportVAS" property="hmCrossTabReport" type="java.util.HashMap"> </bean:define>
<% 
	
   	GmGridFormat gmCrossTab = new GmGridFormat();

   	ArrayList alHeader = null;

	ArrayList alDrillDown = new ArrayList();
	
	alHeader = GmCommonClass.parseNullArrayList((ArrayList)hmCrossTabReport.get("Header"));
	int headerSize = alHeader.size();
	for (int i = 0;i<headerSize; i+=3)
	{
		gmCrossTab.addLine((String)alHeader.get(i));
	} 
	
	for (int i = 1,j=3;j<headerSize; i+=3,j+=3)
	{
		gmCrossTab.add_RoundOff((String)alHeader.get(i),"0");
		gmCrossTab.add_RoundOff((String)alHeader.get(j),"1");
	} 
	
	// Setting the cross tab style 
	// Page Body Style
	gmCrossTab.setGeneralHeaderStyle("aaTopHeaderLightG");
	gmCrossTab.setDivHeight(200);
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setGroupingRequired(true);
	gmCrossTab.setValueType(0);
	gmCrossTab.setNoDivRequired(true); // this skips division for all columns
	gmCrossTab.addLine("Name");
	gmCrossTab.setNameWidth(140);
	gmCrossTab.setRowHighlightRequired(true);
	
		// Line Style information
	//gmCrossTab.setColumnDivider(false);
	gmCrossTab.setColumnLineStyle("borderDark");
	
	gmCrossTab.displayZeros(true);
	
	gmCrossTab.setDecorator("com.globus.crosstab.beans.GmRptByTreatmentDecorator");
	

	
	String strGridData = gmCrossTab.generateGridXML(hmCrossTabReport);

	strGridData = strGridData.replaceAll("<BR>","&lt;br&gt;");
	String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");

	
%>



<HTML>
<HEAD>
<TITLE> Globus Medical: NeurologicalStatusReport </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmStudyFilterInc.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
document.onkeypress = function(){
		if(event.keyCode == 13){
			fnReport();
		}
		
	}
function fnSubmit()
{
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmOutcomeReportVAS.strOpt.value = "Report";
	document.frmOutcomeReportVAS.submit();
}
function fnLoad()
{
	document.frmOutcomeReportVAS.selectAll.checked = false;
	document.frmOutcomeReportVAS.strOpt.value = "";
	document.frmOutcomeReportVAS.action = "/gmRptOutcomeScores.do?method=reportNeurologicalStatus";
	document.frmOutcomeReportVAS.submit();
}

function fnReport()
{
	document.frmOutcomeReportVAS.action = "/gmRptOutcomeScores.do?method=reportNeurologicalStatus";
	fnStartProgress();
	fnSubmit();
}	

var objGridData = '<%=strGridData%>';

	
function fnOnPageLoad(){

	
	gridObj = initGrid('dataGridDiv',objGridData,1);

	fnCheckAll();
}	

</script>

</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();">
<html:form action="/gmRptOutcomeScores.do"  >
<html:hidden property="strOpt" />

	<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">&nbsp;Neurological Status - Overall Stable/Improved</td>
		</tr>
		<tr><td class="LLine"></td></tr> 
		<tr>
			<td width="848" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
						<td colspan="2" align="center">&nbsp;
	                    	<jsp:include page="/clinical/GmStudyFilterInc.jsp" >
							<jsp:param name="FORMNAME" value="frmOutcomeReportVAS" />
							</jsp:include>	
						</td>
					</tr>	
					 <tr><td height="5"></td></tr>					 
					<tr>
						<td colspan ="2" align=center> <div id="dataGridDiv" style="" height="200" ></div></td>
					</tr>
					<tr class="shade">
						<td class = "oddshade" colspan="3" align="center" height="30">	
							<div class='exportlinks'>Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!-- | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a> -->
                            </div>

							
						</td>
					</tr>		
			   	</table>
  			   </td>
  		  </tr>	
  		
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>

