<%
/**********************************************************************************
 * File		 		: GmRptPatientInfo.jsp
 * Desc		 		: Demographic Report for the Patient Information
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>

<!-- clinical\GmRptPatientInfo.jsp -->
<!-- WEB-INF path corrected for JBOSS migration changes -->
 

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
 


<%  

	

	String strhAction = (String)request.getAttribute("hAction");
	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Patient Data </TITLE>


<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>


<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
document.onkeypress = function(){
		if(event.keyCode == 13){
			fnReport();
		}
		
	}
function fnReport()
{
	document.frmDgraphReport.action = "/gmRptDgraph.do?method=reportDgraph";
	document.frmDgraphReport.strOpt.value = "rptPatientInfo";
	fnStartProgress();
	document.frmDgraphReport.submit();
}	

function fnLoad()
{
	document.frmDgraphReport.selectAll.checked = false;
	document.frmDgraphReport.strOpt.value = "";
	document.frmDgraphReport.action = "/gmRptDgraph.do?method=reportDgraph";
	document.frmDgraphReport.submit();
}
</script>
</HEAD>
<%
  String ageSDTitle = "Age <br>(Yrs) <br>"+(char)177+"SD";
  String heightSDTitle = "Height <br>(Inches) <br>"+(char)177+"SD";
  String weightSDTitle = "Weight <br>(lbs) <br>"+(char)177+"SD";
%>
<BODY leftmargin="20" topmargin="10">
<html:form action="/gmRptDgraph.do"  >
<html:hidden property="strOpt" />

	<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">Patient Data</td>
		</tr>
		<tr>
			<td valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr><td colspan="2" class="ELine"></td></tr> 
                    <tr>
						<td colspan="2" align="center">
						   	<jsp:include page="/clinical/GmStudyFilterInc.jsp" >
							<jsp:param name="FORMNAME" value="frmDgraphReport" />								
							</jsp:include>	
						</td>
					</tr>
					<tr><td height="5"></td></tr>			        
					<tr>
						<td colspan ="2" align=center> 						 
							<display:table name="requestScope.frmDgraphReport.ldtResult" style="width=75%;" cellpadding="0" cellspacing="0" requestURI="/gmRptDgraph.do?method=reportDgraph" export="true" decorator="com.globus.displaytag.beans.DTRowHighlightWrapper" >							    								\
								<display:column headerClass="Empty" class="Empty" style="width:1;" />
     							<display:column headerClass="Line" class="Line" style="width:1;"  media="html"></display:column>
								<display:column property="TREATMENT" title="Treatment" headerClass="aaTopHeaderLightG"/>
								<display:column headerClass="Line" class="Line"  style="width:1;" media="html"/>
								<display:column property="MALENO" title="Male <br> n" style="text-align:right" headerClass="aaTopHeaderLightG"   />								
								<display:column property="MALEPER" title="Male <br> %" style="text-align:right" headerClass="aaTopHeaderLightG"   />
								<display:column headerClass="Line" class="Line"  style="width:1;" media="html"/>
								<display:column property="FEMALENO" title="Female <br> n" style="text-align:right" headerClass="aaTopHeaderLightG" />
								<display:column property="FEMALEPER" title="Female <br> %" style="text-align:right" headerClass="aaTopHeaderLightG" />
								<display:column headerClass="Line" class="Line"  style="width:1;" media="html"/>
								<display:column property="AGEAVG" title="Age <br>(Yrs) <br> Avg" style="text-align:right" headerClass="aaTopHeaderLightG"  />
								<display:column property="AGESD" title="<%=ageSDTitle%>"  style="text-align:right" headerClass="aaTopHeaderLightG"   />
								<display:column headerClass="Line" class="Line"  style="width:1;" media="html"/>
								<display:column property="HEIGHTAVG" title="Height <br> (feet) <br> Avg" style="text-align:right" headerClass="aaTopHeaderLightG" />
								<display:column property="HEIGHTSD" title="<%=heightSDTitle%>" style="text-align:right" headerClass="aaTopHeaderLightG"   />
								<display:column headerClass="Line" class="Line"  style="width:1;" media="html"/>
								<display:column property="WEIGHTAVG" title="Weight <br> (lbs)<br> Avg" style="text-align:right" headerClass="aaTopHeaderLightG" />
								<display:column property="WEIGHTSD" title="<%=weightSDTitle%>" style="text-align:right" headerClass="aaTopHeaderLightG" />
								<display:column headerClass="Line" class="Line"  style="width:1;" media="html"/>
								<display:footer media="html" > 
				  					<tr>
				  					    <td class="Empty"></td>
		        				  	    <td colspan="18" class="Line"></td>
				                    </tr>
	 				   			</display:footer>
							</display:table>						 
						</td>
					</tr>	
				  </td>
				</tr>
			   	</table>
  			   </td>
  		  </tr>	  		
    </table>		     	
</html:form>

</BODY>

</HTML>

