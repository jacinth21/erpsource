 <%
/**********************************************************************************
 * File		 		: GmClinicalIRBEvent.jsp
 * Desc		 		: This is a transaction screen used to Tack Event for an IRB Approval.
 * Version	 		: 1.0
 * author			: Rajeshwaran
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList"%>

<%@ page import="com.globus.common.beans.GmCommonConstants"%> 
<%@ page import="com.globus.common.beans.GmLogger"%> 
<%@ page import="org.apache.log4j.Logger"%> 

<bean:define id="gridData" name="frmClinicalIRBMedia" property="gridData" type="java.lang.String"> </bean:define>
<bean:define id="studyName" name="frmClinicalIRBMedia" property="studyName" type="java.lang.String"> </bean:define>
<bean:define id="siteName" name="frmClinicalIRBMedia" property="siteName" type="java.lang.String"> </bean:define>
<bean:define id="approvalTypeName" name="frmClinicalIRBMedia" property="approvalTypeName" type="java.lang.String"> </bean:define>
<bean:define id="approvalReasonName" name="frmClinicalIRBMedia" property="approvalReasonName" type="java.lang.String"> </bean:define>
<bean:define id="lockedStudyDisable" name="frmClinicalIRBMedia" property="lockedStudyDisable" type="java.lang.String"> </bean:define>
<HTML>
<HEAD>
<TITLE> Globus Medical: Track Event Date </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="styles/Globus.css">
<link rel="stylesheet" type="text/css" href="/styles/GmClinical.css">
<script language="JavaScript" src="javascript/GlobusCommonScript.js"></script>
<script language="JavaScript" src="javascript/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script>
	var objGridData;
	objGridData = '<%=gridData%>';

</script>


</HEAD>

<script>
function fnSubmit(){

fnValidateDropDn('mediaType','Media Type');
fnValidateTxtFld('irbMediaNotes','Notes');
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
document.frmClinicalIRBMedia.strOpt.value="save_irb_media";
fnStartProgress();
document.frmClinicalIRBMedia.submit();
}

function fnOnPageLoad()
{
	  if(objGridData.indexOf("cell", 0)==-1){
		  return true;
	  }
		
	  gridObj = initGrid('irbevent',objGridData);
	  gridObj.enableTooltips("false,true,true,true,true,true");
	 
	  gridObj.attachEvent("onRowSelect" ,function(rowId,cellIndex){
		  if(cellIndex == 0)
		  {
			fnLoadIRBMedia(rowId);
		  }
			
	  });
}

function fnLoadIRBMedia(virbmediaid)
{
	document.frmClinicalIRBMedia.mediaType.value="";
	document.frmClinicalIRBMedia.irbMediaNotes.value="";
	document.frmClinicalIRBMedia.irbMediaID.value = virbmediaid;
	document.frmClinicalIRBMedia.strOpt.value = "edit_irb_media";
	document.frmClinicalIRBMedia.submit();
}
function fnReset(){
	document.frmClinicalIRBMedia.mediaType.value="0";
	document.frmClinicalIRBMedia.irbMediaNotes.value="";
}

function fnVoidMedia()
{
		document.frmClinicalIRBMedia.action ="/GmCommonCancelServlet";
		document.frmClinicalIRBMedia.hTxnId.value = document.frmClinicalIRBMedia.irbMediaID.value;	
		document.frmClinicalIRBMedia.hTxnName.value = document.frmClinicalIRBMedia.mediaType.options[document.frmClinicalIRBMedia.mediaType.selectedIndex].text;
		document.frmClinicalIRBMedia.hCancelType.value = 'VDMTY';
		document.frmClinicalIRBMedia.hAction.value = 'Load';
		document.frmClinicalIRBMedia.submit();
}

</script>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad()" >
<html:form  action="/gmClinicalIRBMediaAction.do">
<html:hidden property="strOpt" />
<html:hidden property="irbid"  />
<html:hidden property="irbMediaID" />
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value=""/>
<html:hidden property="hAction" value=""/>

<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">Media Type</td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>

		<tr>
		<td HEIGHT="30" colspan="4"> 
			<table width="100%">
				<tr class="evenshade">
					<td class="RightTableCaption" align="left" HEIGHT="30" ></font>&nbsp;Study:</td>						
					<td><%=studyName%>
					</td>
					<td class="RightTableCaption" align="left" HEIGHT="30" ></font>&nbsp;Site:</td>						
					<td><%=siteName%>
					</td>
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>	
					<tr class="oddshade">
					<td class="RightTableCaption" align="left" HEIGHT="30" ></font>&nbsp;Appr Type:</td>						
					<td><%=approvalTypeName%>
					</td>
					<td class="RightTableCaption" align="left" HEIGHT="30" ></font>&nbsp;Appr Reason:</td>						
					<td><%=approvalReasonName%>
					</td>				           					
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>	
				</tr>
			</table>
		</td>				
	</tr>

		<tr>
			<td  height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="evenshade">
						<td class="RightText" align="right" HEIGHT="30" width="30%"><font color="red">*</font>&nbsp;Media Type:</td>						
							<td>&nbsp;<gmjsp:dropdown controlName="mediaType"  SFFormName="frmClinicalIRBMedia" SFSeletedValue="mediaType"
								SFValue="mediaTypeList" width="250" codeId="CODEID" codeName="CODENM"  defaultValue="[Choose One]"/> &nbsp;&nbsp;							
            			</td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>	
					<tr class="oddshade">
						<td class="RightText" align="right" HEIGHT="30" width="30%"><font color="red">*</font>&nbsp;Notes:</td>						
							<td>&nbsp;<html:textarea property="irbMediaNotes" rows="4" cols="50" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/> &nbsp;&nbsp;							
            			</td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>	
					</table>
      		  <table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
				<td  align="center" HEIGHT="30" width="10%">&nbsp;</td>
				<td  align="center" HEIGHT="30" ><gmjsp:button value="Submit" gmClass="button" buttonType="Save" onClick="javascript:fnSubmit();" disabled="<%=lockedStudyDisable %>"/>
				&nbsp;&nbsp;&nbsp;<gmjsp:button value="Reset" gmClass="button" buttonType="Save" onClick="javascript:fnReset();"/>&nbsp;&nbsp;&nbsp;
				<gmjsp:button value="Void" gmClass="button" buttonType="Save" onClick="javascript:fnVoidMedia();" disabled="<%=lockedStudyDisable %>"/>
				</td><td  align="center" HEIGHT="30" >&nbsp;</td>				
				<td  align="center" HEIGHT="30" >&nbsp;</td>
				</tr>				
    </table>		     
      		  <table>	  
			<tr> 
				<td colspan="2">
			    	<div id="irbevent" style="grid" height="150px"></div>
				</td>	
    	 	</tr> 	  	
    	</table>	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>