<%
/**********************************************************************************
 * File		 		: GmRptNDIImproveDetail.jsp
 * Desc		 		: Report for Neck Disability INdex improvement details
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>

<!-- clinical\GmRptNDIImproveDetail.jsp -->
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ include file="/common/GmHeader.inc" %>

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
 
<bean:define id="hmCrossTabReport" name="frmOutcomeReportVAS" property="hmCrossTabReport" type="java.util.HashMap"> </bean:define>
<%  
try{

	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	
	
	// to draw lines for every other second column
	ArrayList alHeader = GmCommonClass.parseNullArrayList((ArrayList)hmCrossTabReport.get("Header"));
	int headerSize = alHeader.size();
	for (int i = 1;i<headerSize; i+=2)
	{
		gmCrossTab.addLine((String)alHeader.get(i));
		gmCrossTab.addStyle((String)alHeader.get(i),"RightTextSmallCR","aaTopHeader");
		gmCrossTab.add_RoundOff((String)alHeader.get(i),"0");
	} 
	

	// Setting the cross tab style 
	// Page Body Style
	gmCrossTab.setGeneralHeaderStyle("aaTopHeader");
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setNameWidth(50);
	gmCrossTab.setColumnWidth("Treatment",180);
	gmCrossTab.setGroupingRequired(true);
	gmCrossTab.setDivHeight(700);
	gmCrossTab.setValueType(1);
	gmCrossTab.setNoDivRequired(true); // this skips division for all columns
//	gmCrossTab.setRoundOffAllColumnsPrecision("");
	gmCrossTab.addLine("Name");
	gmCrossTab.addLine("Treatment");	
	gmCrossTab.setRowHighlightRequired(true);
	
		// Line Style information
//	gmCrossTab.setColumnDivider(false);
	gmCrossTab.setColumnLineStyle("borderDark");
	gmCrossTab.addStyle("Name","ShadeLevel2NoBold","aaTopHeader") ;
	gmCrossTab.addStyle("Treatment","ShadeLevel2NoBold","aaTopHeader") ;
	gmCrossTab.displayZeros(true);
	gmCrossTab.reNameColumn("Name","Patient ID");
	
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: % Improvement Detail Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>

function fnSubmit()
{
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmOutcomeReportVAS.strOpt.value = 'save';
	document.frmOutcomeReportVAS.submit();
}

function fnReport()
{
	document.frmOutcomeReportVAS.action = "/gmRptOutcomeScores.do?method=reportNDIImproveDetail";
	document.frmOutcomeReportVAS.strOpt.value = "Report";
	document.frmOutcomeReportVAS.submit();
}	

function fnLoad()
{
	document.frmOutcomeReportVAS.selectAll.checked = false;
	document.frmOutcomeReportVAS.strOpt.value = "";
	document.frmOutcomeReportVAS.action = "/gmRptOutcomeScores.do?method=reportNDIImproveDetail";
	document.frmOutcomeReportVAS.submit();
}
</script>0

</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmRptOutcomeScores.do"  >
<html:hidden property="strOpt"/>

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">NDI % Improvement Detail</td>
		</tr>
		<tr>
			<td height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
						<td colspan="2" align="center">&nbsp;
	                    	<jsp:include page="/clinical/GmStudyFilterInc.jsp" >
							<jsp:param name="FORMNAME" value="frmOutcomeReportVAS" />							
							</jsp:include>	
						</td>
					</tr>	
					<tr>
			             <td colspan="2">
			             	<table cellpadding="0" cellspacing="0" border="0">
					<!-- Custom tag lib code modified for JBOSS migration changes -->
			             		<tr>	
									<td class="RightTableCaption" align="right" width="70"></font>&nbsp;Treatment :</td>   
                  	                <td height="30">&nbsp; 
	    	                            <gmjsp:dropdown controlName="treatmentId" SFFormName="frmOutcomeReportVAS" SFSeletedValue="treatmentId"
						        		SFValue="alTreatment" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[All]"  />													
    		                        </td>			    		           
			            		</tr>
						   </table>
						</td>
				     </tr>			
                    <tr><td colspan="4" class="ELine"></td></tr> 
					<tr>
						<td colspan ="4" align=center> <%=gmCrossTab.PrintCrossTabReport(hmCrossTabReport) %></td>
					</tr>
			   	</table>
  			   </td>
  		  </tr>	
  		
    </table>		     	
</html:form>
<% } catch(Exception exp) {
exp.printStackTrace();
}
%>
</BODY>

</HTML>

