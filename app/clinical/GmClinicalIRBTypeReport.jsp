<%
/**********************************************************************************
 * File		 		: GmClinicalIRBTypeReport.jsp
 * Desc		 		: This is a report screen for Clinical IRB Report By Approval Type
 * Version	 		: 1.0
 * author			: Hparikh
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<bean:define id="gridData" name="frmClinicalIRBReport" property="gridXmlData" type="java.lang.String"> </bean:define>
<% 
	boolean showGrid=true;	
	if(gridData!=null && gridData.indexOf("cell")==-1){
	   showGrid = false;			
	}
	String strSiteId = GmCommonClass.parseNull((String)session.getAttribute("strSessSiteId"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Clinical Site Map </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="javascript/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalIRBReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script>
	var objGridData;
	objGridData = '<%=gridData%>';
	document.onkeypress = function(){
		if(event.keyCode == 13){
			fnApprTypeSubmit();
		}
		
	}
</script>
</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad()" >
<html:form action="/gmClinicalIRBReportAction.do">
<html:hidden property="strOpt" />
<html:hidden property="msg"/>
	
	<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">IRB Approval Type</td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
		<tr>
				<td width="848" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<logic:notEqual name="frmClinicalIRBReport" property="msg" value="">
					<tr><td colspan="4" height="20"  class="RightBlueText">
					<bean:write name="frmClinicalIRBReport" property="msg"/></td>
										<tr><td class="Line" colspan="4"></td></tr>
					</tr>
					</logic:notEqual>
					<%
						if(strSiteId.equals(""))
						{
					%>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="evenshade">
						<td class="RightTableCaption" align="right"  HEIGHT="30" ></font>Site List:&nbsp;</td>						
						<td width="38%"><gmjsp:dropdown controlName="siteId"  SFFormName="frmClinicalIRBReport" SFSeletedValue="siteId"
							SFValue="alSiteList"  codeId="ID" codeName="NAME" defaultValue="[Choose One]"/>
            			</td>
            			<td>&nbsp;</td>
                        <td>&nbsp;</td>             								
						<td rowspan="12" class="LLine" width="1"></td>
						<td rowspan="12" align="center"><gmjsp:button value="&nbsp;&nbsp;Load&nbsp;&nbsp;" gmClass="button" buttonType="Load" onClick="fnApprTypeSubmit();" /> </td>            			
            			           			
					</tr>
					<%} %>
					<tr><td colspan="4" class="LLine"></td></tr> 
					<tr>
						<td class="RightTableCaption" align="right"  HEIGHT="30" ></font>&nbsp;Appr Reason:&nbsp;</td>						
							<td width="38%"><gmjsp:dropdown controlName="irbReasonId"  SFFormName="frmClinicalIRBReport" SFSeletedValue="irbReasonId"
							SFValue="alIRBApprovalReasonList"  codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
            			</td>
            			<td class="RightTableCaption" align="left" colspan="2" HEIGHT="30" ></font>Appr Type:&nbsp;						
						<gmjsp:dropdown controlName="irbAppTypeId"  SFFormName="frmClinicalIRBReport" SFSeletedValue="irbAppTypeId"
							SFValue="alIRBApprovalTypeList"  codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
            			</td>  
            			<%
						if(!strSiteId.equals(""))
						{
						%>
            			<td rowspan="12" class="LLine" width="1"></td>
						<td rowspan="12" align="center"><gmjsp:button value="&nbsp;&nbsp;Load&nbsp;&nbsp;" gmClass="button" buttonType="Load" onClick="fnApprTypeSubmit();" /> </td>
						<%} %>  			
					</tr>	
					<tr><td colspan="4" class="LLine"></td></tr> 									
					<tr class="evenshade">
					    <td class="RightTableCaption" align="right"  HEIGHT="50" ></font>&nbsp;Date Range:&nbsp;</td>						
						<td class="RightTableCaption" >&nbsp;Type:&nbsp;<gmjsp:dropdown controlName="dateTypeId"  SFFormName="frmClinicalIRBReport" SFSeletedValue="dateTypeId"
							SFValue="alDateTypeList" codeId="CODEID" codeName="CODENM" />&nbsp;<BR><BR>&nbsp;
							<gmjsp:calendar SFFormName="frmClinicalIRBReport" controlName="startDate" toControlName ="endDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
							
            			</td>                        
                        <td class="RightTableCaption" align="left" colspan="2"  HEIGHT="30" ></font>&nbsp;Status:&nbsp;						
							<gmjsp:dropdown controlName="irbStatusId"  SFFormName="frmClinicalIRBReport" SFSeletedValue="irbStatusId"
							SFValue="alIRBApprovalStatusList"  codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
            			</td>                         
                    </tr>  
                   <tr><td colspan="4" bgcolor="#cccccc"></td></tr>
                   </table> 
    			</td>		
  		  </tr>
  		  <tr> 
			<%if(showGrid){%>
				<td colspan="4">
			    	<div id="dataGridDiv" class="grid" style="height:295px"></div>
				</td>	
			<%}else{ %>
				<td colspan="4">
			    	Nothing found to display.
				</td>	
			<%}%>
    	 	</tr> 	  	
  		  <tr><td class="RightText" colspan="2" bgcolor="#cccccc"></td></tr> 
  		<tr colspan = "2" class = "oddshade">
						<td class = "oddshade" colspan="3" align="center" height="30">	
							<div >Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!-- | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a> -->
                     	</td>
					  </tr>
  		  </table>	
    		
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

