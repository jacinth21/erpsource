<%
/**********************************************************************************
 * File		 		: GmRptEnrollByTreatment.jsp
 * Desc		 		: Report about the patients enrolled for the site by Treatment
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>

<!-- clinical\GmRptEnrollByTreatment.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>

<%@ page import="java.util.ArrayList" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>


<% 


	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();

	
	ArrayList alDrillDown 	= new ArrayList();

	String strhAction = (String)request.getAttribute("hAction");
	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}

	// To Generate the Crosstab report
	gmCrossTab.setHeader("Enrollment By Treatment");
	
	gmCrossTab.setLinkType(strhAction);
	gmCrossTab.setLinkRequired(true);	// To Specify the link
	
	
	// To specify if its unit or valye 
	gmCrossTab.setValueType(0);

	// Setting Display Parameter
	gmCrossTab.setAmountWidth(91);
	gmCrossTab.setNameWidth(50);
	gmCrossTab.setRowHeight(22);
	
	// Page Body Style
	// Setting the cross tab style 
	// Page Body Style
	gmCrossTab.setGeneralHeaderStyle("aaTopHeaderLightG");
	gmCrossTab.addStyle("Name","ShadeLevel2NoBold","aaTopHeaderLightG") ;
	gmCrossTab.addStyle("Site Name","ShadeLevel2NoBold","aaTopHeaderLightG") ;
	gmCrossTab.addStyle("Prinicipal Investigator","ShadeLevel2NoBold","aaTopHeaderLightG") ;
	gmCrossTab.addStyle("Total","ShadeLevel2NoBoldRight","aaTopHeaderLightG") ;
	gmCrossTab.setRowHighlightRequired(true);
	


	// Line Style information
	gmCrossTab.setColumnDivider(false);
	gmCrossTab.setDivHeight(500);
	gmCrossTab.setColumnLineStyle("borderDark");

	// Rename Columns		
	gmCrossTab.reNameColumn("Name","Site ID");

	// Add Lines
	gmCrossTab.addLine("ID");
	gmCrossTab.addLine("Name");
	gmCrossTab.addLine("Site Name");
	gmCrossTab.addLine("Prinicipal Investigator");
	gmCrossTab.addLine("Total");

	// set Column Width
	//gmCrossTab.setColumnWidth("Site ID",200);
    gmCrossTab.setColumnWidth("Site Name",200);
    gmCrossTab.setColumnWidth("Prinicipal Investigator",250);
    
	// Adding Drilldown
	alDrillDown.add("Account");
	gmCrossTab.setDrillDownDetails(alDrillDown);   
	gmCrossTab.setExport(true, pageContext, "/gmRptEnrollByTreatment.do?method=reportEnrollByTreatment", "RptEnrollByTreatmentReport");

	
%>
<bean:define id="hmReport" name="frmRptEnrollByTreatement" property="hmRptEnrollByTreatement" type="java.util.HashMap"> </bean:define>
<HTML>
<HEAD>
<TITLE> Globus Medical: Planned Surgery Date </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />



<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
function fnGo()
{
	document.frmRptEnrollByTreatement.action = "/gmRptEnrollByTreatment.do?method=reportEnrollByTreatment";
	document.frmRptEnrollByTreatement.strOpt.value = "Report";
	document.frmRptEnrollByTreatement.submit();
}

function fnCallAccount(id)
{
    document.frmRptEnrollByTreatement.action = "/gmRptSurgeryByDate.do?method=reportSurgeryByDate";
	document.frmRptEnrollByTreatement.strOpt.value = "Report";
	document.frmRptEnrollByTreatement.siteId.value = id;
	document.frmRptEnrollByTreatement.submit();
}	

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmRptEnrollByTreatment.do"  >
<html:hidden property="strOpt" />
<html:hidden property="siteId" value="" />
<%-- 
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Enrollment By Treatment</td>
		</tr>
		<tr>
			<td valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr><td colspan="2" class="ELine"></td></tr> 
                    <tr>
						<td align="center" valign="middle">&nbsp;
	                    	<jsp:include page="/clinical/GmStudyFilterInc.jsp" >
							<jsp:param name="FORMNAME" value="frmRptEnrollByTreatement" />	
							<jsp:param name="ENABLESITELIST" value="false" />	
							</jsp:include>								
						</td> 
						<td align="left" valign="bottom" width="50%"><input type=button value=&nbsp;&nbsp;Go&nbsp;&nbsp; onclick="javascript:fnGo();" class=button ></td>
					</tr>
		    	</table>  
		  	</td>
		  </tr>
	</table>
--%>
	<div>
		<table class="DtTable765">
			<tr>
				<td height="25" class="Header">Enrollment By Treatment</td>
			</tr>
			<tr><td class="LLine"></td></tr> 
			<% if(!hmReport.isEmpty()) { %>   
			<tr>
				<td colspan ="2" align=center> <%=gmCrossTab.PrintCrossTabReport(hmReport) %></td>
			</tr>
			<% }%>					    
	</table>
	</div>
</html:form>


</BODY>

</HTML>