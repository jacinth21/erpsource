<%
/**********************************************************************************
 * File		 		: gmRptNDIScore.jsp
 * Desc		 		: Report for Neck Disability INdex scores
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>


<!-- clinical\GmRptPEDScore.jsp -->
<!-- WEB-INF path corrected for JBOSS migration changes -->


<%@ include file="/common/GmHeader.inc" %>

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>

<bean:define id="hmCrossTabReport" name="frmOutcomeReport" property="hmCrossTabReport" type="java.util.HashMap"> </bean:define>


<%  
try{
	
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	
	ArrayList alHeader = null;
%>
<bean:define id="strQuestionnairre" name="frmOutcomeReport" property="questionnairre" type="java.lang.String"> </bean:define>
<bean:define id="strScore" name="frmOutcomeReport" property="score" type="java.lang.String"> </bean:define>
<%

if(strQuestionnairre.equals("60801")||strQuestionnairre.equals("60901")){
	// to draw lines for every other second column
	alHeader = GmCommonClass.parseNullArrayList((ArrayList)hmCrossTabReport.get("Header"));
	int headerSize = alHeader.size();
	for (int i = 0;i<headerSize; i+=2)
	{
		gmCrossTab.addLine((String)alHeader.get(i));
		log.debug(" (String)alHeader.get(i) " +(String)alHeader.get(i));
	} 
	
	// Setting the cross tab style 
	// Page Body Style
	gmCrossTab.setGeneralHeaderStyle("aaTopHeaderLightG");
	gmCrossTab.setDivHeight(200);
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setGroupingRequired(true);
	gmCrossTab.setValueType(0);
	gmCrossTab.setNoDivRequired(true); // this skips division for all columns
	gmCrossTab.setRoundOffAllColumnsPrecision("1");
	gmCrossTab.addLine("Name");
	gmCrossTab.setRowHighlightRequired(true);
	
		// Line Style information
//	gmCrossTab.setColumnDivider(false);
	gmCrossTab.setColumnLineStyle("borderDark");
	gmCrossTab.displayZeros(true);
	
	if(strScore.equals("60851")||strScore.equals("60921")){
		gmCrossTab.setExport(true, pageContext, "/gmRptOutcome.do?method=reportNDIScore", "RptNDIScore");
	}
}
%>

<%
//PMT-43698: Amnios study primary endpoint report changes
// to add the Amnios question details
if(strQuestionnairre.equals("60811")||strQuestionnairre.equals("61811") || strQuestionnairre.equals("109380") || strQuestionnairre.equals("109381")){
	// to draw lines for every other second column
	alHeader = GmCommonClass.parseNullArrayList((ArrayList)hmCrossTabReport.get("Header"));
	int headerSize = alHeader.size();
	for (int i = 0;i<headerSize; i+=2)
	{
		gmCrossTab.addLine((String)alHeader.get(i));
		log.debug(" (String)alHeader.get(i) " +(String)alHeader.get(i));
	} 
	
	// Setting the cross tab style 
	// Page Body Style
	gmCrossTab.setGeneralHeaderStyle("aaTopHeaderLightG");
	gmCrossTab.setDivHeight(200);
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setGroupingRequired(true);
	gmCrossTab.setValueType(0);
	gmCrossTab.setNoDivRequired(true); // this skips division for all columns
	gmCrossTab.setRoundOffAllColumnsPrecision("1");
	gmCrossTab.addLine("Name");
	gmCrossTab.setRowHighlightRequired(true);
	
		// Line Style information
//	gmCrossTab.setColumnDivider(false);
	gmCrossTab.setColumnLineStyle("borderDark");
	gmCrossTab.displayZeros(true);
	gmCrossTab.setExport(true, pageContext, "/gmRptOutcome.do?method=reportNDIScore", "RptNDIScore");
}
%>	


<%
if(strQuestionnairre.equals("60803")||strQuestionnairre.equals("60903")){
// to draw lines for every other second column
alHeader = GmCommonClass.parseNullArrayList((ArrayList)hmCrossTabReport.get("Header"));
int headerSize = alHeader.size();
for (int i = 1;i<headerSize; i+=2)
{
	gmCrossTab.addLine((String)alHeader.get(i));
	gmCrossTab.addStyle((String)alHeader.get(i),"RightTextSmallCR","aaTopHeaderLightG");
	gmCrossTab.add_RoundOff((String)alHeader.get(i),"0");
} 


// Setting the cross tab style 
// Page Body Style
gmCrossTab.setGeneralHeaderStyle("aaTopHeaderLightG");
gmCrossTab.setTotalRequired(false);
gmCrossTab.setColumnTotalRequired(false);
gmCrossTab.setNameWidth(50);
gmCrossTab.setColumnWidth("Treatment",180);
gmCrossTab.setGroupingRequired(true);
gmCrossTab.setDivHeight(204);
gmCrossTab.setValueType(1);
gmCrossTab.setNoDivRequired(true); // this skips division for all columns
//gmCrossTab.setRoundOffAllColumnsPrecision("");
gmCrossTab.addLine("Name");
gmCrossTab.addLine("Treatment");	
gmCrossTab.setRowHighlightRequired(true);

	// Line Style information
//gmCrossTab.setColumnDivider(false);
gmCrossTab.setColumnLineStyle("borderDark");
gmCrossTab.addStyle("Name","ShadeLevel2NoBold","aaTopHeaderLightG") ;
gmCrossTab.addStyle("Treatment","ShadeLevel2NoBold","aaTopHeaderLightG") ;
gmCrossTab.displayZeros(true);
gmCrossTab.reNameColumn("Name","Patient ID");

	if(strScore.equals("60851")||strScore.equals("60921")){
		gmCrossTab.setExport(true, pageContext, "/gmRptOutcome.do?method=reportNDIImproveDetail", "NDIImproveDetails");
	}
}
%>

<%
if(strQuestionnairre.equals("60802")||strQuestionnairre.equals("60902")){
ArrayList alDrillDown = new ArrayList();

alHeader = GmCommonClass.parseNullArrayList((ArrayList)hmCrossTabReport.get("Header"));
int headerSize = alHeader.size();
for (int i = 0;i<headerSize; i+=3)
{
	gmCrossTab.addLine((String)alHeader.get(i));
} 

for (int i = 1,j=3;j<headerSize; i+=3,j+=3)
{
	gmCrossTab.add_RoundOff((String)alHeader.get(i),"0");
	gmCrossTab.add_RoundOff((String)alHeader.get(j),"1");
} 

// Setting the cross tab style 
// Page Body Style
gmCrossTab.setGeneralHeaderStyle("aaTopHeaderLightG");
gmCrossTab.setDivHeight(200);
gmCrossTab.setTotalRequired(false);
gmCrossTab.setColumnTotalRequired(false);
gmCrossTab.setGroupingRequired(true);
gmCrossTab.setValueType(0);
gmCrossTab.setNoDivRequired(true); // this skips division for all columns
gmCrossTab.addLine("Name");
gmCrossTab.setNameWidth(140);
gmCrossTab.setRowHighlightRequired(true);

	// Line Style information
//gmCrossTab.setColumnDivider(false);
gmCrossTab.setColumnLineStyle("borderDark");

// Drill down link
gmCrossTab.setLinkRequired(true);
alDrillDown.add("LoanerDist");	
gmCrossTab.setDrillDownDetails(alDrillDown);
gmCrossTab.displayZeros(true);

	if(strScore.equals("60851")||strScore.equals("60921")){
		gmCrossTab.setExport(true, pageContext, "/gmRptOutcome.do?method=reportNDIPerImprove", "NDIPerImprove");
	}
}
%>


<%
if(strQuestionnairre.equals("60812")||strQuestionnairre.equals("61812")){
	
ArrayList alDrillDown = new ArrayList();
alHeader = GmCommonClass.parseNullArrayList((ArrayList)hmCrossTabReport.get("Header"));
int headerSize = alHeader.size();
for (int i = 0;i<headerSize; i+=3)
{
	gmCrossTab.addLine((String)alHeader.get(i));
} 

for (int i = 1,j=3;j<headerSize; i+=3,j+=3)
{
	gmCrossTab.add_RoundOff((String)alHeader.get(i),"0");
	gmCrossTab.add_RoundOff((String)alHeader.get(j),"1");
} 

// Setting the cross tab style 
// Page Body Style
gmCrossTab.setGeneralHeaderStyle("aaTopHeaderLightG");
gmCrossTab.setDivHeight(200);
gmCrossTab.setTotalRequired(false);
gmCrossTab.setColumnTotalRequired(false);
gmCrossTab.setGroupingRequired(true);
gmCrossTab.setValueType(0);
gmCrossTab.setNoDivRequired(true); // this skips division for all columns
gmCrossTab.addLine("Name");
gmCrossTab.setNameWidth(140);
gmCrossTab.setRowHighlightRequired(true);

	// Line Style information
//gmCrossTab.setColumnDivider(false);
gmCrossTab.setColumnLineStyle("borderDark");
gmCrossTab.displayZeros(true);
gmCrossTab.setExport(true, pageContext, "/gmRptOutcome.do?method=reportZCQSuccess", "ZCQReport");
}
%>




<HTML>
<HEAD>
<TITLE> Globus Medical: Neck Disability Index Score (NDI) </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmRptPEDScore.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>

var questionnairre_Val = '<%=strQuestionnairre%>';
var score_Val = '<%=strScore%>'; 

</script>
</HEAD>
<BODY leftmargin="0" topmargin="0">
<html:form action="/gmRptOutcome.do"  >
<html:hidden property="strOpt" />

	<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">&nbsp;Primary Endpoint</td>
		</tr>
		<tr><td class="LLine" height="1"></td></tr>
		<tr>
			<td width="1100" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
						<td colspan="2" align="center" height="30">&nbsp;
	                    	<jsp:include page="/clinical/GmPEDStudyFilterInc.jsp" >
							<jsp:param name="FORMNAME" value="frmOutcomeReport" />
							</jsp:include>	
						</td>
					</tr>
					<tr>
						<td colspan ="2" align=center>&nbsp;</td>
					</tr>
					<tr>
						<td colspan ="2" align="center" style="height: 50"> <%=gmCrossTab.PrintCrossTabReport(hmCrossTabReport) %></td>
					</tr>
			   	</table>
  			   </td>
  		  </tr>	
  		
    </table>		     	
</html:form>
<% } catch(Exception exp) {
exp.printStackTrace();
}
%>
</BODY>
</HTML>