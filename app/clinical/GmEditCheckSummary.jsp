 <%
/**********************************************************************************
 * File		 		: GmCommonCurrConv.jsp
 * Desc		 		: This screen displys currency conversion
 * Version	 		: 1.0
 * author			: Himanshu Patel
************************************************************************************/
%>

<!-- clinical\GmEditCheckSummary.jsp -->


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<bean:define id="gridData" name="frmStudyFilterForm" property="gridXmlData" type="java.lang.String"> </bean:define>

 
<%
    
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strWikiTitle = GmCommonClass.getWikiTitle("COMMON_CURRENCY_CONVERSION_SETUP");
//System.out.println("Grid Data ==11======="+gridData)
    String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Enterprise Portal</TITLE>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmEditSummaryReport.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmClinicalEditCheckSetup.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmFormDataEntry.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script>
	var gridObj;
	objGridData = '<%=gridData%>';	
</script>
</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();">
<html:form action="/gmEditCheckReportAction.do" styleId="form">
   <html:hidden property="incidentStatus" /> 
	<html:hidden property="strOpt" />
	<html:hidden property="haction" value=""/> 
	<html:hidden property="hTxnId" value="" />
	<html:hidden property="hTxnName" value="" />
	<html:hidden property="hCancelType" value="" />

	<table border="0"  class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class=Header colspan="4">&nbsp;Edit Check Summary</td>
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr class="evenshade" height="25">	
			<td class="RightTableCaption" align="right">&nbsp;Form:</td>
			<td >&nbsp;
				<gmjsp:dropdown controlName="formId"  SFFormName="frmStudyFilterForm" SFSeletedValue="formId"
				SFValue="alStudyFormList" codeId="FORMID" codeName="FORMNM" defaultValue="[Choose One]"/>                 		                                        		
            </td>
            <td class="RightTableCaption" align="right">&nbsp;Status:</td>
			<td >&nbsp;
				<gmjsp:dropdown controlName="status"  SFFormName="frmStudyFilterForm" SFSeletedValue="status"
				SFValue="alStatus" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>                 		                                        		
            </td>
		</tr>
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		<tr height="25" class="oddshade">	
            <td class="RightTableCaption" align="right">&nbsp;Created By:</td>
			<td >&nbsp;
				<gmjsp:dropdown controlName="craId"  SFFormName="frmStudyFilterForm" SFSeletedValue="craId"
				SFValue="alCraNames" codeId="ID" codeName="NAME" defaultValue="[Choose One]"/>
			</td>
			<td class="RightTableCaption" align="right" >&nbsp;Reference Id:</td>
			<td>&nbsp;&nbsp;<html:text property="referenceId" /></td>
		</tr>
        <tr><td colspan="4" class="LLine" height="1"></td></tr>   
	    <tr height="30">
	           	<td colspan="4" align="center" height="30">
	                <gmjsp:button value="&nbsp;&nbsp;Load&nbsp;&nbsp;" gmClass="Button" buttonType="Load" onClick="fnLoadReport();" /> 
	            </td>
	    </tr>
	    
	    <tr><td colspan="4" class="LLine" height="1"></td></tr>
	    <tr><td colspan="4">

	          		<div id="dataGridDiv" class="grid" style="height:295px;width:900px"></div>
    	</td></tr>
    	<tr class = "shade" >
		<td colspan="4" align="center" height="30">	
					<div class='exportlinks'>Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!-- | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a> -->
                    </div>
		</td>
		
	</tr>
		
    </table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
