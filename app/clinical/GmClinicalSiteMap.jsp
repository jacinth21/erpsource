<%
/**********************************************************************************
 * File		 		: GmClinicalSiteMap.jsp
 * Desc		 		: This is a report screen for Clinical Site Map setup
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<bean:define id="gridData" name="frmClinicalSiteMap" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="approvalDate" name="frmClinicalSiteMap" property="approvalDate" type="java.lang.String"> </bean:define>
<bean:define id="currentDate" name="frmClinicalSiteMap" property="currentDate" type="java.lang.String"> </bean:define>
<bean:define id="lockedStudyDisable" name="frmClinicalSiteMap" property="lockedStudyDisable" type="java.lang.String"> </bean:define>
<HTML>
<HEAD>
<TITLE> Globus Medical: Clinical Site Map </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonGrid.js"></script>
<script language="JavaScript" src="javascript/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalSiteMap.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
	var objGridData;
	objGridData = '<%=gridData%>';
	
	var curDate = '<%=currentDate%>';
</script>
</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();">
<html:form action="/gmClinicalSiteMap.do">
<html:hidden property="strOpt" />
<html:hidden property="hsiteMapID"/>
<html:hidden property="currentDate"/>
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value=""/>
<html:hidden property="hAction" value=""/>
<html:hidden property="msg"/>
	<logic:notEqual name="frmClinicalSiteMap" property="strDhtmlx" value="true">
	<table border="0" class="DtTable950" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">Site Mapping Screen</td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<logic:notEqual name="frmClinicalSiteMap" property="msg" value="">
					<tr><td colspan="2" height="20" align="center" class="RightBlueText">
					<bean:write name="frmClinicalSiteMap" property="msg"/></td>
										<tr><td class="Line" colspan="2"></td></tr>
					</tr>
					</logic:notEqual>	
					<%-- 
					<tr>
						<td class="RightText" align="right" HEIGHT="30" width="30%"></font>&nbsp;Study List:</td>						
							<td>&nbsp;<gmjsp:dropdown ControlName="studyList"  SFFormName="frmClinicalSiteMap" SFSeletedValue="studyList"
							SFValue="alStudyList" 	Width="250" CodeId="ID" OnChange="fnReload();"  CodeName="NAME" DefaultValue="[Choose One]"/>
            			</td>
					</tr>
					--%>
					<tr class="evenshade">
						<td class="RightText" align="right" HEIGHT="30" width="30%"><font color="red">*</font>&nbsp;Site List:</td>						
							<td>&nbsp;<gmjsp:dropdown controlName="siteNameList"  SFFormName="frmClinicalSiteMap" SFSeletedValue="siteNameList"
							SFValue="alSiteNameList" width="250" codeId="ID" codeName="NAME" defaultValue="[Choose One]"/>
            			</td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
                    <tr class="oddshade">
                        <td class="RightText" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;Site ID:</td>
                        <td>&nbsp; 
                        <html:text property="siteId" size="5" maxlength="2" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td> 
                    </tr>
                    <tr><td colspan="3" class="LLine" height="1"></td></tr>
                    <tr class="evenshade">
                        <td class="RightText" align="right" HEIGHT="30"></font>&nbsp;IRB Approval Date:</td>
                       <td class="RightText" align="left" HEIGHT="30"></font>&nbsp;&nbsp;<%=approvalDate%>
                       &nbsp;                         
                        </td>
                    </tr>  
                    <tr><td colspan="3" class="LLine" height="1"></td></tr> 
                    <tr class="oddshade">
						<td class="RightText" align="right" HEIGHT="30" width="30%"><font color="red">*</font>&nbsp;CRA's List:</td>						
							<td>&nbsp;<gmjsp:dropdown controlName="craName"  SFFormName="frmClinicalSiteMap" SFSeletedValue="craName"
							SFValue="alCraNames" width="250" codeId="ID" codeName="NAME" defaultValue="[Choose One]"/>
            			</td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					 <tr class="evenshade">
						<td class="RightText" align="right" HEIGHT="30" width="30%"><font color="red">*</font>&nbsp;IRB Type:</td>						
							<td>&nbsp;<gmjsp:dropdown controlName="irbTypeName"  SFFormName="frmClinicalSiteMap" SFSeletedValue="irbTypeName"
							SFValue="alIRBType" width="250" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
            			</td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr class="oddshade">
						<td class="RightText" align="right" HEIGHT="30" width="30%"></font>&nbsp;Study Pkt Sent:</td>						
							<td>&nbsp;<gmjsp:calendar SFFormName="frmClinicalSiteMap" controlName="studyPktSent" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
							&nbsp;&nbsp; 
            			</td>
					</tr>
					 <tr class="evenshade">
                        <td class="RightText" align="right" HEIGHT="24">&nbsp;TIN:</td>
                        <td>&nbsp;<html:text property="tinNumber" size="34" maxlength="50" name="frmClinicalSiteMap" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td> 
                    </tr>
	
					
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					 <tr class="evenshade">                        
                        <td colspan="2" align="center" height="30">&nbsp;
                        <gmjsp:button value="Submit" gmClass="button" buttonType="Save" onClick="fnSubmit();" disabled="<%=lockedStudyDisable %>"/>
                        <gmjsp:button value="Reset" gmClass="button" buttonType="Save" onClick="fnReset('reset');"/>                        
                        <gmjsp:button value="Void" gmClass="button" buttonType="Save" onClick="fnVoid();" disabled="<%=lockedStudyDisable %>"/>                        
                        </td>
                    </tr> 
                   </table> 
    			</td>
  		  </tr>
  		  <tr><td colspan="2" bgcolor="#cccccc"></td></tr> 
  		  </table>
  		  </logic:notEqual>
  		  <table class="DtTable950">	  
		  <!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
			<tr> 
				<td colspan="2">
			    	<jsp:include page= "/clinical/GmIncludeClinicalGrid.jsp"/>
				</td>	
    	 	</tr> 	  	
    	</table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

