<%
/**********************************************************************************
 * File		 		: GmPatientTracking.jsp
 * Desc		 		: This is a report screen for Patient Tracking
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>

<!-- clinical\GmPatientTrackingList.jsp -->


<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<bean:define id="gridData" name="frmStudyFilterForm" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="sessAccLvl" name="frmStudyFilterForm" property="sessAccLvl" type="java.lang.String"> </bean:define>
<%
String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
// PMT-39479: Amnios study label changes
String strStudyId = GmCommonClass.parseNull((String)session.getAttribute("strSessStudyId"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Clinical Patient Tracking </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css"> 
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmStudyFilterInc.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonGrid.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css"> 

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmPatientTrackingList.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmClinicalCommonScript.js"></script> 
<script>
	var objGridData;
	objGridData = '<%=gridData%>';	
	
	sessAccLvl = '<%=sessAccLvl%>';
	var vSstudyId = '<%=strStudyId%>';
</script>
</HEAD>
<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();">
<html:form>
	<table border="0" cellspacing="0" cellpadding="0" class="DtTable765">	
		<tr style="height:25px">
			<td height="25" class="Header" >&nbsp;Patient Tracking</td>
		</tr>	
		<tr><td class="LLine" height="1"></td></tr>        	 
		<tr> 
        	<td>
          		<div id="dataGridDiv" class="grid" style="height : 450px;"></div>
			</td>
			
		</tr>
	 	<tr style="height:96px">
			<td style="height:96px">
			<!-- <b>a.</b>  --> &nbsp;&nbsp; <font color=red><strong> Red </strong></font></t>= Past Due <br>
			<!-- <b>b.</b>  --> &nbsp;&nbsp;  <font color=Orange><strong> Orange </strong></font></t>= Pending, 1 Month <br>
			<!-- <b>c.</b>  --> &nbsp;&nbsp;  <font color=Green><strong> Green </strong></font></t>= Current Date <br>
			</td>
		</tr>	
		
		<tr class="shade">
						<td class = "oddshade" colspan="3" align="center" height="30">	
							<div class='exportlinks'>Export Options : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!-- | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a>  -->
                            </div>

							
						</td>
					</tr>					
    
    
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

