 <%
/**********************************************************************************
 * File		 		: GmDExcel.jsp
 * Desc		 		: This screen is used to download the same in Excel format
 * Version	 		: 1.0
 * author			: Brinal
************************************************************************************/
%>

<!-- clinical\GmDynamicDataLoadExcel.jsp-->
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->

<bean:define id="hmDynamicDataLoad" name="frmDynamicDataLoad" property="hmCrossTabReport" type="java.util.HashMap"> </bean:define>


<%
	// To Generate the Crosstab report
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();	
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setNoDivRequired(true);
	gmCrossTab.setColumnTotalRequired(false);
	
	gmCrossTab.setRoundOffAllColumnsPrecision("0");
		
	gmCrossTab.setHideColumn("Name");	
	gmCrossTab.displayZeros(true);
	gmCrossTab.setskipCalculationForAllColumns(true);
	
	gmCrossTab.reNameColumn("Name","Patient ID");
	gmCrossTab.setNameWidth(50);	
	gmCrossTab.setColumnWidth("Treatment",180);
	gmCrossTab.setColumnWidth("Surgeon",120);
	gmCrossTab.setColumnWidth("SurgeryDate",100);
	gmCrossTab.setColumnWidth("Timepoint",100);	
	gmCrossTab.setColumnWidth("examDate",100);
		
	gmCrossTab.setDisableLine(true);
	
	if((String)request.getParameter("surgeryDate")== null)
	{
		gmCrossTab.setHideColumn("SurgeryDate"); 
	}
	
	if((String)request.getParameter("surgeon")== null)
	{
		gmCrossTab.setHideColumn("Surgeon"); 
	}
	
	if((String)request.getParameter("treatment")== null)
	{
		gmCrossTab.setHideColumn("Treatment"); 
	}
	if((String)request.getParameter("examDate")== null)
	{
		gmCrossTab.setHideColumn("ExamDate"); 
	}
	
%>
<HTML>

<HEAD>
<TITLE> Globus Medical: Dynamic Data Load</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style>
.borderDark {
	FONT-SIZE: 12px; 
	FONT-FAMILY: Helvetica,sans-serif; 
	BACKGROUND-COLOR: #7c816a
}

.borderLight {
	FONT-SIZE: 12px; 
	FONT-FAMILY: Helvetica,sans-serif; 
	BACKGROUND-COLOR: #cccccc
}

.ShadeDarkGrayTD{
	background-color: #c3c5b8;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeLightGrayTD{
	background-color: #dee0d2;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeMedGrayTD{
	background-color: #dee0d2;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeDarkBlueTD{
	background-color: #a0b3de;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}
.ShadeLightBlueTD{
	background-color: #f1f2f7;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeMedBlueTD{
	background-color: #e4e6f2;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeDarkGreenTD{
	background-color: #ccdd99;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeMedGreenTD{
	background-color: #e1f3a6;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeDarkOrangeTD{
	background-color: #feb469;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeMedOrangeTD{
	background-color: #ffecb9;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeLightOrangeTD{
	background-color: #fef2cc;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}
.ShadeBlueTD{
	background-color: #ecf6fe;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeRightTableCaptionBlueTD{
	FONT-SIZE: 9px; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
	background-color: #AACCE8;
}

.ShadeBlueTDSmall{
	background-color: #ecf6fe;
	COLOR : Black;
	font-family: sans-serif;
	font-size : 11px;
	font-style: normal;
	height: 20px;
}

.CrossRightText {
	FONT-SIZE: 9px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
}

.RightTextRedSmall { 
	FONT-SIZE: 11px; 
	COLOR: #FF0000; 
	FONT-FAMILY:sans-serif;
	text-decoration: None;
}

.RightTextSmall {
	FONT-SIZE: 9px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
}


	background-color: #dee0d2;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
</style>
</HEAD>

<TABLE cellSpacing=0 cellPadding=0  border=0 width="1000">
<TBODY>
	<tr>
		<td align=center> <%=gmCrossTab.PrintCrossTabReport(hmDynamicDataLoad) %></td>
		
	</tr>
</FORM>
</BODY>
<%@ include file="/common/GmFooter.inc" %>  
</HTML>
