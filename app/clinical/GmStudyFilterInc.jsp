<%
/**********************************************************************************
 * File		 		: GmStudyFilterInc.jsp
 * Desc		 		: Include JSP
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>

<!--clinical\querysystem\GmStudyFilterInc.jsp -->

<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ page import="org.apache.struts.action.ActionForm"%>
<%@ page import="java.util.*"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<% 

String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
String strEnableSiteList = GmCommonClass.parseNull(request.getParameter("ENABLESITELIST"));
String strHideLoadButton = GmCommonClass.parseNull(request.getParameter("HIDELOADBUTTON"));
String strChangeFunction = "";
String strDashboard=GmCommonClass.parseNull(request.getParameter("HIDDENSITELIST"));
String strSiteId = GmCommonClass.parseNull((String)session.getAttribute("strSessSiteId"));
if(strEnableSiteList.equals(""))
{
	strChangeFunction = "fnLoad();";
	
}
//System.out.println("strHideLoadButton========"+strHideLoadButton);
//System.out.println("strEnableSiteList========"+strEnableSiteList);
//System.out.println("strDashboard========"+strDashboard);
String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
%>
<bean:define id="siteIds" name="<%=strFormName%>" property="strSiteIds" type="java.lang.String"> </bean:define> 
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmStudyFilterInc.js"></script>
<script>
var siteIds = '<%=siteIds%>';
var varEnableSiteList = '<%=strEnableSiteList%>';
//Check the earlier version for the adverse event report - CheckAll function
</script>
<body onLoad="fnCheckAll();">
	<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
	
	<%
	if(strSiteId.equals(""))
	{
	if(strEnableSiteList.equals("")) 
	{
		if(GmCommonClass.parseNull(request.getParameter("HIDDENSITELIST")).equals(""))
		{ 
	%>
		<td class="RightTableCaption" align="right" HEIGHT="20" width="100">Site List :&nbsp;</td>
		<td>
             <table cellspacing="0" cellpadding="0">
               	<tr width="55%">
	                <td bgcolor="gainsboro">
						<html:checkbox property="selectAll" onclick="fnSelectAll('toggle');" /><B>Select All </B> 
		     		</td>
		     		<td width="39%"></td>
				</tr>	    
			</table>
			<DIV style="display:visible;height: 100px; width: 304px; overflow: auto; border-width:1px; border-style:solid;">
                <table cellspacing="0" cellpadding="0">
                  	<logic:iterate id="sitelist" name="<%=strFormName%>" property="alSiteNameList">
                	   	<tr>
                    		<td>
				    			<htmlel:multibox property="checkSiteName" value="${sitelist.ID}" onclick="fnDeselect();"/>
				    			<bean:write name="sitelist" property="NAME" />
  							</td>
						</tr>	    
					</logic:iterate>
				</table>
			</DIV>
		</td>
		<%	if(strHideLoadButton.equals("")) { %>
		<td style="width:500px;">&nbsp;&nbsp;<gmjsp:button value="Load" gmClass="button" buttonType="Load" onClick="fnReport();"/></td>
                        
<%
  }
		else
  		{
			%>
			<td style="width:500px;">&nbsp;&nbsp;</td>
			<% 
  		}
 }  
}} %>    			
	</table>	
  </body>  	     	
<%@ include file="/common/GmFooter.inc" %>




