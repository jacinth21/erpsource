<%
/**********************************************************************************
 * File		 		: GmSiteCoordinatorReport.jsp
 * Desc		 		: Report for the enrolled patients by month
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>


<!-- clinical\GmSiteCoordinatorReport.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>


<bean:define id="gridData" name="frmStudyFilterForm" property="gridXmlData" type="java.lang.String"> </bean:define>

<HTML>
<HEAD>
<TITLE> Globus Medical: Enterprise Portal </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

<script>
var objGridData = '<%=gridData%>';
	
function fnOnPageLoad(){
	gridObj = initGrid('dataGridDiv',objGridData);
}
</script>

</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();">
<html:form action="/gmSiteCoordinatorReportAction.do">
	<table border="0"  class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">&nbsp;Site Coordinator Report </td>
		<tr>
		<tr>
			<td>
				<div id="dataGridDiv" style="height:400px;width:765px;"></div>			
			</td>
		</tr>
		<tr class = "oddshade">
                <td colspan="7"  align="center" height="30"> 
                <div class='exportlinks'>Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!-- | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a> -->
                </div>
                </td>
</tr>
			
 	</table>
</html:form>

</BODY>
</HTML>

