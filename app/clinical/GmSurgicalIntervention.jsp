
<%
	/**********************************************************************************
	 * File		 		: GmSurgicalIntervention.jsp
	 * Desc		 		: This screen is used for to add/edit surgical intervention
	 * Version	 		: 1.0
	 * author			: Dhana Reddy
	 ************************************************************************************/
%>


<!-- clinical\GmSurgicalIntervention.jsp -->


<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="java.util.Date"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%
String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
	GmServlet gm = new GmServlet();
	gm.checkSession(response, session);
	String strSiteName = "";
	String strPatientID = "";
	String strStudyId = "";
	String strSurgeryDate = "";
	String strAction = "";
	String strSurgTypId = "";
	String strSurgLvlId = "";
	String strSurgInvnId = "";
	String strSurgDt = "";
	String strVoidAccess = "";
	String strSessAccLvl="";
	String strVerifyFlg ="";
	String strButtonDisabled = "";
	HashMap hmResult = new HashMap();
	ArrayList alType = new ArrayList();
	ArrayList alOrgInv = new ArrayList();
	ArrayList alVerifyList=new ArrayList();
	strStudyId = GmCommonClass.parseNull((String) request.getAttribute("hStudyID"));
	hmResult = GmCommonClass.parseNullHashMap((HashMap) request.getAttribute("hmReturn"));
	alType = GmCommonClass.parseNullArrayList((ArrayList)hmResult.get("SURGTYPE"));
	alOrgInv = GmCommonClass.parseNullArrayList((ArrayList) hmResult.get("ORLVLINV"));
	strSiteName = GmCommonClass.parseNull((String) hmResult.get("hSiteName"));
	strAction = GmCommonClass.parseNull((String) hmResult.get("hAction"));
	strSurgTypId = GmCommonClass.parseNull((String) hmResult.get("SURGTYPEID"));
	strSurgLvlId = GmCommonClass.parseNull((String) hmResult.get("ORGLVLINVID"));
	strSurgInvnId = GmCommonClass.parseNull((String) hmResult.get("SURGINVNO"));
	strVerifyFlg = GmCommonClass.parseNull((String) hmResult.get("VERIFYFL"));
	strVoidAccess = GmCommonClass.parseNull((String) hmResult.get("VOIDFL"));
	alVerifyList = GmCommonClass.parseNullArrayList((ArrayList)hmResult.get("VERIFYDETAILS"));
	strSurgDt = GmCommonClass.parseNull((String) hmResult.get("SURGDT"));
	strSessAccLvl = GmCommonClass.parseNull((String) hmResult.get("strSessAccLvl"));
	strPatientID = GmCommonClass.parseNull((String) request.getAttribute("hPatientID"));
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	strButtonDisabled = GmCommonClass.parseNull((String)request.getAttribute("strButtonDisabled"));
	Date dtSurgDt = null;
	dtSurgDt = GmCommonClass.getStringToDate(strSurgDt, strApplDateFmt);
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Surgical Intervention</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmSurgicalIntervention.js"></script>
<script type="text/javascript">
var format = '<%=strApplDateFmt%>';
</script>
</HEAD>
<BODY leftmargin="0" topmargin="0">
	<FORM name="frmSurgInvn" method="POST"
		action="<%=strServletPath%>/GmPatientMasterServlet">
		<input type="hidden" name="hAction" value="<%=strAction%>" /> 
		<input type="hidden" name="hStudyID" value="<%=strStudyId%>"> 
		<input type="hidden" name="hTxnId" value=""> 
		<input type="hidden" name="hCancelType" value=""> 
		<input type="hidden" name="hPatientID" value="<%=strPatientID%>" /> 
		<input type="hidden" name="hSurgInvnId" value="<%=strSurgInvnId%>">
		<input type="hidden" name="hTypeId" value="<%=strSurgTypId%>">
		<input type="hidden" name="hSurgDt" value="<%=strSurgDt%>">
		<input type="hidden" name="hOrgLvlId" value="<%=strSurgLvlId%>">
		
		<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
			<tr>
				<td bgcolor="#666666" width="1"></td>
				<td valign="top">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td class="LLine" colspan="2"></td>
						</tr>
						<tr>
							<td colspan="2" height="25" class="Header">&nbsp;Surgical
								Intervention Setup</td>
						</tr>
						<!-- Custom tag lib code modified for JBOSS migration changes -->
						<tr class="evenshade">
							<gmjsp:label type="BoldText"  SFLblControlName="Site Name:" td="true"/>
							<td width="70%">&nbsp;<%=strSiteName%></td>
						</tr>
						<tr class="oddshade">
							<gmjsp:label type="BoldText"  SFLblControlName="Patient IDE#:" td="true"/>
							<td>&nbsp;<%=strPatientID%></td>
						</tr>
						<tr class="evenshade">
							<gmjsp:label type="BoldText"  SFLblControlName="Type:" td="true"/>
							<td class="RightText" HEIGHT="24">&nbsp;<gmjsp:dropdown
									controlName="Cbo_Type" seletedValue="<%=strSurgTypId%>"
									defaultValue="[Choose One]" tabIndex="-1" value="<%=alType%>"
									codeId="CODEID" codeName="CODENM" />
							</td>
						</tr>
						<tr class="oddshade">
						<gmjsp:label type="BoldText"  SFLblControlName="Surgery
									Date:" td="true"/>
							<td>&nbsp;<gmjsp:calendar
									textControlName="Txt_SurgeryDate"
									textValue="<%=(dtSurgDt == null) ? null : new java.sql.Date(dtSurgDt.getTime())%>"
									gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
									onBlur="changeBgColor(this,'#ffffff');" />
							</td>
						</tr>
						<tr class="evenshade">
							<gmjsp:label type="BoldText"  SFLblControlName="Original level Intervention:" td="true"/>
							<td class="RightText" HEIGHT="24">&nbsp;<gmjsp:dropdown
									controlName="Cbo_Orglevel" seletedValue="<%=strSurgLvlId%>"
									defaultValue="[Choose One]" tabIndex="-1" value="<%=alOrgInv%>"
									codeId="CODEID" codeName="CODENM" />
							</td>
						</tr>
						
						<%--Comments section Start--%>
						<tr>
							<td colspan="2"><table border="0" width="100%" cellspacing="0"
									cellpadding="0">
									<tr>
										<td height="24" class="Header" colspan="4">&nbsp;Verification
											/Comments Section</td>
									</tr>
									<tr class="evenshade">
									<gmjsp:label type="BoldText"  SFLblControlName="Comments:" td="true"/>
										<td align="left" class="RightText" colspan="3">&nbsp;<textarea
												class="RightText" rows="4" cols="105" name="txtaVerifyDesc"
												 	onFocus="changeBgColor(this,'#AACCE8');"
								onBlur="changeBgColor(this,'#ffffff');"></textarea>
										</td>
									</tr>
									<tr>
										<td colspan="4" height="1" class="Line"></td>
									</tr>
									<tr class="ShadeRightTableCaption">
										<td height="22" colspan="4">&nbsp;Verification History</td>
									</tr>
									<tr>
										<td colspan="4" height="1" class="Line"></td>
									</tr>
									<tr>
										<td colspan="4">
											<table border="0" width="100%" cellspacing="0"
												cellpadding="0">
												<tr class="oddshade">
													<gmjsp:label type="BoldText"  SFLblControlName="#" td="true" align="left"/>
													<gmjsp:label type="BoldText"  SFLblControlName="Type" td="true" align="left"/>
													<gmjsp:label type="BoldText"  SFLblControlName="By" td="true" align="left"/>
													<gmjsp:label type="BoldText"  SFLblControlName="Updated Date" td="true" align="left"/>
													<gmjsp:label type="BoldText"  SFLblControlName="Comments (If any)" td="true" align="left"/>
												</tr>
												<tr>
													<td colspan="5" height="1" bgcolor="#cccccc"></td>
												</tr>
<%				String strVerifyUser = "";
				String strVerifyDate = "";
				String strVerifyLvlId="";
				int intSize = alVerifyList.size();
				HashMap hcboVal = new HashMap();
				if (intSize > 0)
				{
					for (int i=0;i<intSize;i++)
					{
						hcboVal = (HashMap)alVerifyList.get(i);
						strVerifyLvlId = GmCommonClass.parseNull((String)hcboVal.get("VLEVEL_ID"));
						
						if(strVerifyLvlId.equals("6195")){ // 6195 corresponds to Locked
							%>
							<script>
								document.getElementById("hFormLocked").value="locked";
							</script>
							
							<%
						}
%>				<tr>
					<td class="RightText" height="18">&nbsp;<%=i+1%></td>
					<td class="RightText">&nbsp;<%=hcboVal.get("VLEVEL")%></td>
					<td class="RightText">&nbsp;<%=hcboVal.get("CUSER")%></td>
					<td class="RightText">&nbsp;<%=hcboVal.get("VERDATE")%></td>
					<td class="RightText">&nbsp;<%=hcboVal.get("CMNTS")%></td>
				</tr>
<%	  				}
		  		}
%>				
												
											</table>
								</table>
							</td>
						</tr>
	<% String strDisableBtnVal = strButtonDisabled;
			if(strDisableBtnVal.equals("disabled")){
				strDisableBtnVal ="true";
			}
	%>
						<%--Comments section End --%>
						<tr>
							<td height="30" colspan="2" align="center">&nbsp; <gmjsp:button
								value="Submit" name="btn_submit" gmClass="button" buttonType="Save"	
								onClick="fnSubmit();" disabled="<%=strDisableBtnVal %>"/>&nbsp;
<%if(!strSurgInvnId.equals("") && strSessAccLvl.equals("4")&& !strVerifyFlg.equals("Y")){ %>
								 <gmjsp:button value="Verify" gmClass="button" buttonType="Save" onClick="fnVerify();" disabled="<%=strDisableBtnVal %>"/> 
<%
}
if (strVoidAccess.equals("Y")&& !strSurgInvnId.equals("")) {
 %>
								<gmjsp:button value="Void" gmClass="button" buttonType="Save" onClick="fnVoid();"/> <%
 	}
 %> 							<gmjsp:button value="Back" gmClass="button" buttonType="Load" onClick="fnBack();" /></td>
						</tr>
					</table></td>
			</tr>
		</table>
	</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
