 <%
/**********************************************************************************
 * File		 		: GmCommonCurrConv.jsp
 * Desc		 		: This screen displys currency conversion
 * Version	 		: 1.0
 * author			: Himanshu Patel
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.ArrayList" %>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
  <bean:define id="siteMonitorStatus" name="frmClinicalSiteMonitorForm" property="siteMonitorStatus" type="java.lang.String"></bean:define>
  <bean:define id="lockedStudyDisable" name="frmClinicalSiteMonitorForm" property="lockedStudyDisable" type="java.lang.String"> </bean:define>
<%
GmServlet gm = new GmServlet();
gm.checkSession(response,session);
String strWikiTitle = GmCommonClass.getWikiTitle("COMMON_CURRENCY_CONVERSION_SETUP");
HashMap hmReturn = new HashMap();
hmReturn = (HashMap)request.getAttribute("hmReturn");
HashMap hmSmDtls = new HashMap();
ArrayList alSiteMtrMap = new ArrayList();
ArrayList alSiteMtr = new ArrayList();
String flgCheckBox = "false";

String strSiteId = GmCommonClass.parseNull((String)hmReturn.get("SITEID"));
String strStudyId = GmCommonClass.parseNull((String)hmReturn.get("STUDYID"));
String strOpt = GmCommonClass.parseNull((String)hmReturn.get("strOpt"));
String strSiteMtrId = GmCommonClass.parseNull((String)hmReturn.get("strSiteMtrId"));
String strStatus = GmCommonClass.parseNull((String)hmReturn.get("STATUS"));
request.setAttribute("strSiteMtrId",strSiteMtrId);
if(strStatus.equals("60462") || strStatus.equals("60463")){
	flgCheckBox="true";
}
if(!strOpt.equals("") && !strOpt.equals("Load_Monitor")){
	
	
	alSiteMtrMap = (ArrayList)request.getAttribute("alSiteMtrMap");
	alSiteMtr = (ArrayList)request.getAttribute("alSiteMtr");
 
	
	ArrayList alSmDtl = (ArrayList)hmSmDtls.get("ALRETURN");
	 
}

try { %>
	<script>	
	function fnCheckAll()
	{		
		
		var varAction = '<%=strOpt%>';
		//alert('varAction:'+varAction);
		if (varAction == '' || varAction == 'Load_Monitor' ||  (document.all.selectAll && document.all.selectAll.checked) )
		{
		//alert('Inside condition varAction:'+varAction);
			fnSelectAll('selectAll');
		}
	}	

	function fnDeselect()
	{
		objSelAll = document.all.selectAll;
		if (objSelAll) {
			objSelAll.checked = false;
			}
	}

	function fnSelectAll(varCmd)
	{
					objSelAll = document.all.selectAll;
					if (objSelAll ) {
					objCheckSiteArr = document.all.otherMonitor;
					objCheckSiteArrLen = objCheckSiteArr.length;

					if ((objSelAll.checked || varCmd == 'selectAll') && objCheckSiteArr)
					{
						objSelAll.checked = true;
						//objCheckSiteArrLen = objCheckSiteArr.length;
						for(i = 0; i < objCheckSiteArrLen; i ++) 
						{	
							objCheckSiteArr[i].checked = true;
						}
					}
					else if (!objSelAll.checked  && objCheckSiteArr ){
						//objCheckSiteArrLen = objCheckSiteArr.length;
						for(i = 0; i < objCheckSiteArrLen; i ++) 
						{	
							objCheckSiteArr[i].checked = false;
						}
					}
					}
		}
		</script>
<HTML>
<HEAD>
<TITLE> Globus Medical: Enterprise Portal</TITLE>
<style type="text/css" media="all">
     @import url("styles/screen.css");
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">

<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalSiteMonitor.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>


</HEAD>

<BODY onload="fnCheckAll();fnDisable();">
 
<html:form action="/gmClinicalSiteMonitor.do">
<html:hidden property="strOpt" />
<html:hidden property="monitorString" value=""/>
<html:hidden property="siteMtrId" value=""/>
<html:hidden property="ID" value=""/>
<html:hidden property="hAction" value=""/> 
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value=""/>
<html:hidden property="sessSiteId" />
<html:hidden property="otherMonitor" />



 
	<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">Site Monitoring - Schedule Visit</td>
			<td align="right" class="Header" colspan="3"> 	
				</td>
				
		</tr>
		<tr><td class="LLine" height="1" colspan="2"></td></tr>		
		<tr class="evenshade">  
			<!-- Custom tag lib code modified for JBOSS migration changes -->
					<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;Date Of Visit:</td>
						<td HEIGHT="24" >&nbsp;
							<gmjsp:calendar SFFormName="frmClinicalSiteMonitorForm" controlName="dateVisited" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
						                    
						</td>
					</tr>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr> 
		<tr class="oddshade">  
					<td class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;Owner Of Visit:</td>																
				
						<td HEIGHT="24" >&nbsp;
							<gmjsp:dropdown controlName="ownerOfVisit" SFFormName="frmClinicalSiteMonitorForm" SFSeletedValue="ownerOfVisit"
									SFValue="alOwnerOfVisit" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" />
						</td>
		</tr>
		
			<tr><td colspan="2" class="LLine" height="1"></td></tr> 
		<tr class="evenshade">  
				<td class="RightTableCaption" align="right" HEIGHT="25">Add Monitor(s):</td>
					<td>
                             <table cellspacing="0" cellpadding="0" style="width: 200px;">
	                        	<tr bgcolor="gainsboro">
	                        		<td >
									    <html:checkbox property="selectAll" onclick="fnSelectAll('toggle');" /><B>Select All </B> 
		     						</td>
								</tr>	    
							</table>
							<DIV style="display:visible;height: 100px; width: 200px;overflow: auto; border-width:1px; border-style:solid;">
	                         <table cellspacing="0" cellpadding="0">
	                        	<logic:iterate id="monitorlist"  name="frmClinicalSiteMonitorForm" property="alOtherMonitor">
	                        	<tr>
	                        		<td>
									    <htmlel:multibox property="otherMonitor" value="${monitorlist.ID}"   onclick="fnDeselect();"/>
									    <bean:write name="monitorlist" property="NAME" />
		     						</td>
								</tr>	    
								</logic:iterate>
								</table>
								</DIV>
							</td>																
			
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr> 
		<tr class="oddshade">  
					<td class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;Purpose for Visit:</td>																
						<td HEIGHT="25" >&nbsp;
							<gmjsp:dropdown controlName="purposeOfVisit" SFFormName="frmClinicalSiteMonitorForm" SFSeletedValue="purposeOfVisit"
									SFValue="alPurposeOfVisit" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
						</td>
					
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr> 
		<tr class="evenshade">
					<td class="RightTableCaption" valign="top" HEIGHT="25" align="right"><BR>Notes for Visit:</td>
					<td>&nbsp;<html:textarea property="notesOfVisit" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"	
					onblur="changeBgColor(this,'#ffffff');" rows="4" cols="50" /><br>
					</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr> 		
		<tr class="oddshade">  
					<td class="RightTableCaption" align="right" HEIGHT="25">Status:</td>																
					<td HEIGHT="25" >&nbsp;
							<gmjsp:dropdown controlName="siteMonitorStatus" SFFormName="frmClinicalSiteMonitorForm" SFSeletedValue="siteMonitorStatus"
									SFValue="alSiteMonitorStatus" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
						</td>
			
		</tr>
         <tr><td colspan="2" class="LLine" height="1"></td></tr>   
        <%
                        String strvalId = "fnObservations('"+strSiteMtrId+"','"+strStudyId+"','"+strSiteId+"')";
                        String strMtrId = "fnSubmit('"+strSiteMtrId+"')";
                        String strSitMtrId = "fnVoid('"+strSiteMtrId+"')";
                        String strCancelId = "fnCancel('"+strSiteMtrId+"')";
        %>  
		
    <tr>
    <td colspan="2" align="center" height="30">&nbsp;&nbsp;
    		<%if(strStatus.equals("60463")){ %>
    			<gmjsp:button value="Add Observations" gmClass="button" buttonType="Save" onClick="<%=strvalId%>" disabled="<%=lockedStudyDisable %>"/>
            <%}else { %>
           	 
                <gmjsp:button value="Submit" gmClass="button" buttonType="Save" onClick="<%=strMtrId%>" disabled="<%=lockedStudyDisable %>"/>
                 
            <%} %>
            <%if(strStatus.equals("60461")){ %>
                <gmjsp:button value="Void" gmClass="button" buttonType="Save" onClick="<%=strSitMtrId%>" disabled="<%=lockedStudyDisable %>"/> 
                <gmjsp:button value="Reset" gmClass="button" buttonType="Save" onClick="fnLoad();" />
            <%}else if(strStatus.equals("60462")){ %>
                <gmjsp:button value="Cancel" gmClass="button" buttonType="Save" onClick="<%=strCancelId%>" disabled="<%=lockedStudyDisable %>"/>
                <gmjsp:button value="Reset" gmClass="button" buttonType="Save" onClick="fnLoad();" /> 
            <%} %>
        </td>
     </tr>   		
    </table>		 
</html:form>
   <%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
  <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
