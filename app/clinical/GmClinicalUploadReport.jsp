<%
	/**********************************************************************************
	 * File		 		: GmClinicalUploadReport.jsp
	 * Desc		 		: Upload - Upload Report page
	 * Version	 		: 1.0
	 * author			: Tarika Chandure
	 ************************************************************************************/
%>
<%
	Object bean = pageContext.getAttribute("frmClinicalDashBoardDispatch", PageContext.REQUEST_SCOPE);
	pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
%>
<script>
function fnSubmit3()
{


	//document.frmClinicalDashBoardDispatch.action = "/gmClinicalDashBoardDispatch.do?method=pendingForm";
	document.frmClinicalDashBoardDispatch.hAction.value="Load";
	document.frmClinicalDashBoardDispatch.action.value = "/gmClinicalDashBoardDispatch.do?method=pendingVerification&studyListId="+document.frmClinicalDashBoardDispatch.studyListId.value+"&craId="+document.frmClinicalDashBoardDispatch.craId.value+"&flag=U";
	document.frmClinicalDashBoardDispatch.submit();
}

</script>

<jsp:include page="/common/dashboard/GmPendingVerification.jsp" />

