<%
/**********************************************************************************
 * File		 		: GmRptDiagnosis.jsp
 * Desc		 		: Report for Diagnosis
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>

<!-- clinical\GmRptDiagnosis.jsp -->


<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmGridFormat"%>
<bean:define id="hmCrossTabReport" name="frmDgraphReport" property="hmCrossTabReport" type="java.util.HashMap"> </bean:define>
<% 

	GmGridFormat gmCrossTab = new GmGridFormat();
	
	// Setting the cross tab style 
	// Page Body Style
	gmCrossTab.setGeneralHeaderStyle("aaTopHeaderLightG");
	//gmCrossTab.setDivHeight(200);
	
	gmCrossTab.setColumnWidth("Name",120);
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setGroupingRequired(true);
	gmCrossTab.setValueType(0);
	gmCrossTab.setNoDivRequired(true); // this skips division for all columns
	gmCrossTab.setRoundOffAllColumnsPrecision("1");
	gmCrossTab.addLine("Name");
	gmCrossTab.addLine("a");
    gmCrossTab.addLine("b");
	gmCrossTab.addLine("c");
	gmCrossTab.addLine("d");
	gmCrossTab.addLine("ab");
	gmCrossTab.addLine("ac");
	gmCrossTab.addLine("ad");
	gmCrossTab.addLine("bc");
	gmCrossTab.addLine("bd");
	gmCrossTab.addLine("cd");
	gmCrossTab.addLine("abc");
	gmCrossTab.addLine("acd");
	gmCrossTab.addLine("abd");
	gmCrossTab.addLine("bcd");
	gmCrossTab.addLine("abcd");
	
		// Line Style information
//	gmCrossTab.setColumnDivider(false);
	gmCrossTab.setColumnLineStyle("borderDark");
	
	//Remove Decimals for all columns
	gmCrossTab.setRoundOffAllColumnsPrecision("0");
	
	gmCrossTab.setDecorator("com.globus.crosstab.beans.GmRptByTreatmentDecorator");
	

	
	String strGridData = gmCrossTab.generateGridXML(hmCrossTabReport);
	String strClinicalJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Diagnosis </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmStudyFilterInc.js"></script>



<script>

document.onkeypress = function(){
		if(event.keyCode == 13){
			fnLoad();
		}
		
	}

function fnSubmit()
{
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmDgraphReport.strOpt.value = 'save';
	document.frmDgraphReport.submit();
}

function fnLoad()
{
	document.frmDgraphReport.selectAll.checked = false;
	document.frmDgraphReport.strOpt.value = "";
	document.frmDgraphReport.action = "/gmRptDgraph.do?method=reportDiagnosis";
	document.frmDgraphReport.submit();
}

function fnReport()
{
	document.frmDgraphReport.action = "/gmRptDgraph.do?method=reportDiagnosis";
	document.frmDgraphReport.strOpt.value = "Report";
	fnStartProgress();
	document.frmDgraphReport.submit();
}	

var objGridData = '<%=strGridData%>';

	
function fnOnPageLoad(){

	gridObj = initGrid('dataGridDiv',objGridData,1);
	fnCheckAll();
	
}	
</script>

</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();">
<html:form action="/gmRptDgraph.do"  >
<html:hidden property="strOpt" />



	<table border="0" class="DtTable765" cellspacing="0" cellpadding="0" >
		<tr>
			<td height="25" class="Header">Diagnosis</td>
		</tr>
		<tr>
			<td width="848" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0" >
                    <tr>
						<td colspan="2" align="center">&nbsp;
	                    	<jsp:include page="/clinical/GmStudyFilterInc.jsp" >
							<jsp:param name="FORMNAME" value="frmDgraphReport" />
							</jsp:include>	
						</td>
					</tr>
					
                    <tr>
						<td colspan="2"  align = "center">
												
				           		<div id="dataGridDiv" class="grid" style="height:200px; width:100%;"></div>
				        </td>
					<tr class = "oddshade">
						<td class = "oddshade" colspan="3" align="center" height="30">	
							<div >Export Option : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!-- | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a> -->
                            </div>

							
						</td>
					</tr>	
			   	</table>
  			   </td>
  		  </tr>	
  		
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>

