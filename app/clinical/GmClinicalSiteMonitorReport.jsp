 <%
/**********************************************************************************
 * File		 		: GmCommonCurrConv.jsp
 * Desc		 		: This screen displys currency conversion
 * Version	 		: 1.0
 * author			: Himanshu Patel
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %>
 
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strWikiTitle = GmCommonClass.getWikiTitle("COMMON_CURRENCY_CONVERSION_SETUP");
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strSiteId = GmCommonClass.parseNull((String)hmReturn.get("SITEID"));
	String strStudyId = GmCommonClass.parseNull((String)hmReturn.get("STUDYID"));
%>
<bean:define id="gridData" name="frmClinicalSiteMonitorForm" property="gridXmlData" type="java.lang.String"> </bean:define>
<HTML>
<HEAD>
<TITLE> Globus Medical: Enterprise Portal</TITLE>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="/javascript/dhtmlx/dhtmlxCombo/dhtmlxcombo_new.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalSiteMonitorReport.js"></script>
<script language="JavaScript" src="javascript/dhtmlx/dhtmlxgrid_export.js"></script>
<script>
	objGridData = '<%=gridData%>';
	document.onkeypress = function(){
		if(event.keyCode == 13){
			fnLoadReport();
		}
		
	}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
 
<html:form action="/gmClinicalSiteMonitor.do">
	<html:hidden property="strOpt" />
	<html:hidden property="haction" value=""/>
	<html:hidden property="status" value=""/> 
	<html:hidden property="siteMtrId" value=""/>
	<html:hidden property="hTxnId" value=""/>
	<html:hidden property="hTxnName" value=""/>
	<html:hidden property="hCancelType" value=""/>

	<table border="0" class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header" colspan="4">Site Monitoring Summary</td>			
		</tr>		
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr height="25" class="evenshade">	
			<td class="RightTableCaption" align="right">&nbsp;Purpose of Visit:</td>
			<td width="0%">&nbsp;
				<gmjsp:dropdown controlName="purposeOfVisit"  SFFormName="frmClinicalSiteMonitorForm" SFSeletedValue="purposeOfVisit"
				SFValue="alPurposeOfVisit" codeId="CODEID"  codeName="CODENM" defaultValue="[Choose One]"/>
			</td>
			<td class="RightTableCaption" align="right" width="0%">&nbsp;Status:</td>
			<td >&nbsp;
				<gmjsp:dropdown controlName="siteMonitorStatus"  SFFormName="frmClinicalSiteMonitorForm" SFSeletedValue="siteMonitorStatus"
				SFValue="alSiteMonitorStatus" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>                 		                                        		
            </td>
		</tr>
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		<tr height="25" class="oddshade">	
			<td class="RightTableCaption" align="right">&nbsp;Date of Visit:</td>
			<td width="0%">
			&nbsp;<gmjsp:calendar SFFormName="frmClinicalSiteMonitorForm" controlName="fromDate" toControlName ="toDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" /> 
			</td>
			<td>&nbsp;</td>
			<td >&nbsp;&nbsp;<gmjsp:button value="&nbsp;&nbsp;Load&nbsp;&nbsp;" gmClass="button" buttonType="Load" onClick="fnLoadReport();" /> 
            </td>
		</tr>
        <tr><td colspan="4" class="LLine" height="1"></td></tr>   
	    
		    
			<tr> 
	        	<td colspan="4">
	          		<div id="dataGridDiv" class="grid" style="height:355px;"></div>
				</td>
			</tr> 
			
			<tr class="shade">
						<td class = "oddshade" colspan="4" align="center" height="30">	
							<div class='exportlinks'>Export Options : <img src='img/ico_file_excel.png' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel </a> <!-- | <img src='images/pdf_icon.gif' /> <a href="#"
                                onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a>  -->
                            </div>

							
						</td>
					</tr>		   
	    			
    	     	
    </table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
