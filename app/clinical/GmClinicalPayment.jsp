<%
/**********************************************************************************
 * File		 		: GmClinicalSiteMonitorObservation.jsp
 * Desc		 		: This is a Adding and Editing Site Monitoring Observation.
 * Version	 		: 1.0
 * author			: hparikh
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>


<HTML>
<HEAD>
<TITLE> Globus Medical: Clinical Site Monitoring </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/clinical/GmClinicalPayment.js"></script>
<script language="JavaScript" src="javascript/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_form.js"></script>
<script language="JavaScript" src="javascript/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript" src="javascript/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="javascript/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="javascript/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_markers.js"></script>
<script language="JavaScript" src="javascript/dhtmlx/dhtmlxMenu/dhtmlxmenu.js"></script>
<script language="JavaScript" src="javascript/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_undo.js"></script>
<script language="JavaScript" src="javascript/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript" src="javascript/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script language="JavaScript" src="javascript/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>

<bean:define id="paymentRequestId" name="frmClinicalPayment" property="paymentRequestId" type="java.lang.String"> </bean:define>
<bean:define id="requestTypeId" name="frmClinicalPayment" property="requestTypeId" type="java.lang.String"> </bean:define>
<bean:define id="payableCodeId" name="frmClinicalPayment" property="payableCodeId" type="java.lang.String"> </bean:define>
<bean:define id="payableCodeName" name="frmClinicalPayment" property="payableCodeName" type="java.lang.String"> </bean:define>
<bean:define id="gridXmlData" name="frmClinicalPayment" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="alPeriodList" name="frmClinicalPayment" property="alPeriodList" type="java.util.ArrayList"> </bean:define>

<script>
	var objGridData;
	objGridData = '<%=gridXmlData%>';
	
	

	// The all time points is getting in the Hashmap from alPeriodList.
	var PeriodListLen = <%=alPeriodList.size()%>;
<%
	int intSize = alPeriodList.size();
	
 	HashMap hcboVal = new HashMap();
 	
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alPeriodList.get(i);
%>
		TimePointArr<%=i%> ="<%=hcboVal.get("PERIODID")%>,<%=hcboVal.get("PERIODDS")%>";
<%
	}
%>
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad()">
<html:form action="/gmClinicalPayment.do">

<html:hidden property="paymentRequestId" />
<html:hidden property="strOpt" />
<html:hidden property="paymentDetailData" />
<html:hidden property="haction" value=""/>
<html:hidden property="msg"/>
<html:hidden property="tinNumber"/>
	<table  class="DtTable765" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="Header">Reimbursements</td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td width="848" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<logic:notEqual name="frmClinicalPayment" property="msg" value="">
					<tr><td colspan="2" height="20" align="center" class="RightBlueText">
					<bean:write name="frmClinicalPayment" property="msg"/></td>
										<tr><td class="Line" colspan="2"></td></tr>
					</tr>
					</logic:notEqual>
					<tr class="evenshade">
						<td class="RightTableCaption" align="right" HEIGHT="30" ></font>&nbsp;&nbsp;Payment Request ID#:</td>						
							<td>&nbsp;<%=paymentRequestId%>
            			</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr> 
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="oddshade">
						<td class="RightTableCaption" align="right" HEIGHT="30" ><font color="red">*</font>&nbsp;Request Type:</td>						
							<td>&nbsp;<gmjsp:dropdown controlName="requestTypeId"  SFFormName="frmClinicalPayment" SFSeletedValue="requestTypeId"
							SFValue="alPaymetReqList" 	 codeId="CODEID"  codeName="CODENM" defaultValue="[Choose One]" onChange="fnLoadPayable();"/>
            			</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr> 
					<tr class="evenshade">
						<td class="RightTableCaption" align="right" HEIGHT="30" ><font color="red">*</font>&nbsp;Payment To:</td>						
							<td>&nbsp;
							<gmjsp:dropdown controlName="paymenttoId"  SFFormName="frmClinicalPayment" SFSeletedValue="paymenttoId"
							SFValue="alPayableList"  codeId="<%=payableCodeId%>" codeName="<%=payableCodeName%>" defaultValue="[Choose One]" onChange="fnLoadPaymentTo();"/>
            			</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr> 
                    <tr class="oddshade">
                        <td class="RightTableCaption" align="right" HEIGHT="24">Comments:</td>
                        <td>&nbsp;
                        <html:textarea property="comments" rows="4" cols="50" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td> 
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr> 
                    <tr class="evenshade">
						<td class="RightTableCaption" align="right" HEIGHT="30" ><font color="red">*</font>&nbsp;Status:</td>						
							<td>&nbsp;<gmjsp:dropdown controlName="statusId"  SFFormName="frmClinicalPayment" SFSeletedValue="statusId"
							SFValue="alStatusList"  codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
            			</td>
					</tr>					
					<tr><td colspan="2" class="LLine" height="1"></td></tr> 
					<tr class="oddshade">
					    <td class="RightTableCaption" align="right" HEIGHT="50" ></font>&nbsp;Payment Detail:</td>						
						<td class="RightTableCaption" align="left" ><font color="red">*</font>&nbsp;Payable To:&nbsp;&nbsp;&nbsp;&nbsp;<html:text  property="payable" styleId="payable"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;<BR><BR>&nbsp;Payment Date:
							<gmjsp:calendar SFFormName="frmClinicalPayment" controlName="paymentDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />&nbsp;
							&nbsp;&nbsp; Check Number: <html:text property="checkNumber"  size="10" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
            			</td>                
                    </tr> 
                    <tr><td colspan="2" class="LLine" height="1"></td></tr> 
					<tr class="evenshade">
						<td class="RightTableCaption" align="right" HEIGHT="30" >&nbsp;TIN:</td>						
						<td  id="tin">&nbsp;<bean:write name="frmClinicalPayment" property="tinNumber"/>
						
						
					<logic:equal property="accessFlag" name="frmClinicalPayment" value="Y">
						   			&nbsp;<span><img src=/images/consignment.gif title="Reconfigure RA" id="ReconfRA" style="display: inline-block; vertical-align:top;"  onClick="javascript:fnShowTinReason()"></span>
            		</logic:equal>
            			</td>  
            			            
            			
					</tr>
                    
                    <tr class="oddshade" id="Reason" style="display: none;">
                    <td class="RightTableCaption" align="right" HEIGHT="30" ><font color="red">*</font>&nbsp;Reason:</td>
                     <td>&nbsp;
                        <html:textarea property="tinReason" styleId="tinReason" rows="4" cols="50" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td> 			
                    </tr>
                    
                                       
             		<tr><td colspan="2" class="LLine" height="1"></td></tr> 
                    <tr>
		                    <td class="RightTableCaption" align="left" colspan="2" height="20">
								<div id="payment_detail" style="display:inline" >
									<table cellpadding="1" cellspacing="1"  >
											<TR>
												<td class="RightTableCaption">
												&nbsp;<a href="#"> <img src="/images/dhtmlxGrid/add.gif" alt="Add Row" style="border: none;" onClick="javascript:addRow()"height="14"></a>
												&nbsp;<a href="#"> <img src="/images/dhtmlxGrid/delete.gif" alt="Remove Row" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a>
												</td>										
											</TR>
								
									</table>
								</div>
							</td>				
                    </tr>
                    <tr> 						
							<td colspan="2">
						    	<div id="dataGridDiv" class="grid" style="height:130px;width:762px"></div>
							</td>							
    	 			</tr> 
    	 			 <%
                        String strpaymentRequestId = "fnPrint('"+paymentRequestId+"')";
                        String strRequestId = "fnVoidPaymentRequest('"+paymentRequestId+"')";
                     %>  
					 <tr>                        
                        <td colspan="2" align="center" height="30">&nbsp;
                         <gmjsp:button name="Btn_Submit" value="Submit" gmClass="button" buttonType="Save" onClick="fnSubmit();" />   
                        <%if(!paymentRequestId.equalsIgnoreCase("")) {%>
                     	    &nbsp;<gmjsp:button value="Print" gmClass="button" buttonType="Load" onClick="<%=strpaymentRequestId%>" />     
                        	&nbsp;<gmjsp:button value="Void" gmClass="button" buttonType="Save" onClick="<%=strRequestId%>" />
                        <%}%>                                       
                        </td>
                    </tr>
                  
    			</td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

