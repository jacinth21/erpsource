<%
/**********************************************************************************
 * File		 		: GmPrintPrice.jsp
 * Desc		 		: This screen is used to display Order Summary
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@page import="java.util.Date"%>
<%@ taglib prefix="fmtPrintPrice" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmPrintPrice.jsp -->
<fmtPrintPrice:setLocale value="<%=strLocale%>"/>
<fmtPrintPrice:setBundle basename="properties.labels.custservice.GmPrintPrice"/>
<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strSessCompanyLangId =GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLangId"));
	GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmPrintPrice", strSessCompanyLocale);
	
	
	String strLotOverideAccess = GmCommonClass.parseNull((String)request.getAttribute("LOTOVERRIDEACCESS"));
	String strApplnDateFmt = strGCompDateFmt;
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	strCountryCode = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("COUNTRYCODE"));
	//String strApplDateFmt = GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT","DATEFORMAT"));
	String strSessDeptId = GmCommonClass.parseNull((String)session.getAttribute("strSessDeptId"));
	String strApplCurrFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplCurrFmt"));
	String strCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	String strOpt = GmCommonClass.parseNull((String)session.getAttribute("STROPT"));
	String strOvrideCurr = "";
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strParantForm =  GmCommonClass.parseNull((String)request.getAttribute("hParantForm"));
	String strFwdFlag =  GmCommonClass.parseNull((String)request.getAttribute("hFwdFlag"));
	String strhidePartPrice =  GmCommonClass.parseNull((String)request.getParameter("hidePartPrice"));
	String strOrderSource =  GmCommonClass.parseNull((String)request.getAttribute("OrderSrc"));
	
	String strOpenRTfl =  GmCommonClass.parseNull((String)request.getAttribute("OPENRTFL"));
	String strHideHold = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.HIDE_HOLD_ICON"));// To hide the hold icon
	String strViewAdkDetialsFl =  GmCommonClass.parseNull((String)request.getAttribute("VIEWADJDETAILFL"));
	// Get the included jsp of Invoice total section from rules table
	String strIncJSP =  GmCommonClass.parseNull((String)request.getAttribute("INCJSPNM"));
	strIncJSP = strIncJSP.equals("")?"/custservice/GmInvoiceTotalInclude.jsp":strIncJSP;
	String strShowAddrFl = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("SHOW_ADDR"));
	if(strhidePartPrice.equals(""))
	{
		strhidePartPrice =  GmCommonClass.parseNull((String)request.getAttribute("hidePartPrice"));
		strhidePartPrice = strhidePartPrice.equals("") ? "N" : strhidePartPrice;
	}
	HashMap hmOrderDetails = new HashMap();
	ArrayList alOrderSummary = new ArrayList();
	ArrayList alShipSummary = new ArrayList();
	ArrayList alComment = new ArrayList();
	ArrayList alConstructSummary = new ArrayList();
	ArrayList alInvoice = new ArrayList();
	ArrayList alPayment = new ArrayList();
	ArrayList alReturnDetails = new ArrayList();
	ArrayList alTagDetails = new ArrayList();
	ArrayList alControlNumDetails = new ArrayList();
	ArrayList alQuoteDetails = new ArrayList();
	ArrayList alOrderAtbDetails = new ArrayList();
	ArrayList alRevTrack = new ArrayList();
	HashMap hmRepReqType = new HashMap();
	String strAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strFromPage = GmCommonClass.parseNull((String)request.getAttribute("FromPage"));
	String strShade = "";

	String strAccNm = "";
	String strDistRepNm = "";
	String strBillAdd = "";
	java.sql.Date dtOrdDate = null;
	String strShipAdd = "";
	String strAccId = "";
	String strOrdId = "";

	String strPartNum = "";
	String strDesc = "";
	String strPrice = "";
	String strUnitPrice = "";
	String strQty = "";
	String strTotalPrice = "";
	String strItemOrdId = "";
	String strPartNums = "";
	String strControlNum = "";
	String strShipDate = "";
	String strShipCarr = "";
	String strShipMode = "";
	String strTrack = "";

	String strShippingCost = "";
	String strTotal = "";
	String strOrdMode = "";
	String strItemtype = "";
	
	String strOrdType = "";
	String strStatus = "";
	String strConFlag = "";
	String strConId = "";
	String strConNm = "";
	String strConValue = "";
	String strCmtId = "";
	String strUserNm = "";
	String strDT = "";
	String strComment = "";
	String strCommentType = "";
	String strExOrderType = "";
	String strInvTotal = "";
	String strDOType = "";
	String strDOTypeName = "";
	String strCaseID = "";
	String strTagID = "";
	String strPriceDisc = "";
	String strRepName = "";
	String strAdd1 = "";
	String strAdd2 = "";
	String strCity = "";
	String strState = "";
	String strZipcode = "";
	String strRepEmail="";
	String strOrderHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_ORDER_DETAILS"));
	String strRAId = "";
	Date dtReturnDate = null;
	Date dtCreditedDate = null;
	Date dtAccountCreditDate = null;
	Date dtSurgeryDate =null;
	String strReturnDate = "";
	String strCreditedDate = "";
	String strAccountCreditDate = "";
	String strRepID ="";
	String strSurgeryDt="";
	String strCaseInfoID="";
	String strColor = "RightText";
	String strDesColor = "RightTextAS";
	String strDiscrepancyImage = "<img id=imgDiscrepancy src=".concat(strImagePath).concat("/30.gif title='Discrepancy' height=18 width=20>");
	String strPriceDiscrepancyImage = "<img id=imgDiscrepancy src=".concat(strImagePath).concat("/30.gif title='Discrepancy' height=18 width=20>");
	String strCustomerPO = "";
	String strCurrPrice = "";
	String strParentOrderID = "";
	String strUsageFree ="";
	String strPartNumInputStr = "";
	String strHoldFl ="";
	String strCodeID = "";
	String strCodeNm = "";
	String strOrdredDate = "";
	String strStatusflID="";
	String strReplenishReq="";
	String strBeforeTotal = "";
	String strAfterTotal = "";
	String strAdjustAmount = "";
	String strStatusID = "";
	String strStatusNM = "";
	String strTrackType = "";
	String strUpdatedBy = "";
	String strUpdateDate = "";
	String strOrdSurgeryDate = "";
	Date dtOrdSurgeryDate =null;
	boolean blTagDiscrepancy = false;
	boolean blControlNumDiscrepancy = false;
	int intControlSize = 0; 
		
	boolean blEditUsage = false;
	boolean blEditTag = false; 
	boolean blEditShipping = false; 
	boolean blEditIndTag = false; 
	boolean blEditIndShipping = false; 
	boolean blDOFromApp = false;
	String strOrderAdjId = "";
	
	double dbBeforeItemTotal = 0.0;
	double dbBeforePriceTotal = 0.0;
	double dbAfterItemTotal = 0.0;
	double dbAfterPriceTotal = 0.0;
	double dbPrePrice = 0.0;
	double dbUnitPrice = 0.0;
	double dbUnitPriceAdj = 0.0;
	double dbNetUnitPriceAdj = 0.0;
	
	double dbBeforePrice = 0.0;
	double dbAfterPrice = 0.0;
	double dbAdjustPrice = 0.0;
	double dbSubTotal = 0.0;
	boolean blPDFUploadFL = false;
	boolean blLoterrormsg = false;

	
	String strBeforeItemTotal = "";
	String strAfterItemTotal = "";
	String strAfterPrice = "";
	String strItemQty = "";
	int intQty = 0;
	int intPreQty = 0;
	
	String strSubTotal = "";
	String strCalUnitPrice = "";
	int calPrice = 0;
	String strCalPrice = "";
	String strHideHoldIcon = "Y";
	String strCalNetUnitPrice = "";
	String strUnitPriceAdj = "";
	String strPortalPrice = "";
	String strLotOverrideFl = "";
	String strAccCurrSymb = "";
	String strControlNumFL = "";
	String strModeOfOrder = "";
	String strLotErrormsg = "";
	String inputStr = "";

	GmResourceBundleBean gmResourceBundleBean1 = GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
	
	String strShowSurgAtr = GmCommonClass.parseNull( gmResourceBundleBean.getProperty("DO.SHOW_SURG_ATRB_DET"));
	String strShowProformaInvoice = GmCommonClass.parseNull( gmResourceBundleBean1.getProperty("DO.SHOW_PROFORMA"));
	String strShowBOD = GmCommonClass.parseNull( gmResourceBundleBean1.getProperty("DO.SHOW_BOD"));
	if(strParantForm.equals("CASE"))
		strOrderHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_USAGE_DETAILS"));
	if (hmReturn != null)
	{
		hmOrderDetails = (HashMap)hmReturn.get("ORDERDETAILS");
		alOrderSummary = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ORDERSUMMARY"));
		alShipSummary	= GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SHIPSUMMARY"));
		alQuoteDetails	= GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("QUOTEDETAILS"));
		alOrderAtbDetails = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ORDATBDETAILS"));
		alComment = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALCOMMENTS"));
		alConstructSummary = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("CONSTRUCTDETAILS"));
		alInvoice = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALINVOICEID"));
		alPayment = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALPAYMENTDETAILS"));
		alTagDetails = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALTAGDETAILS"));
		alControlNumDetails =GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALCONTROLNUMDETAILS"));
		alReturnDetails = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALRETURNDETAILS"));
		strModeOfOrder = GmCommonClass.parseNull((String)hmOrderDetails.get("MODEOFORDER"));
		strHoldFl = GmCommonClass.parseNull((String)hmOrderDetails.get("HOLD_FL"));
		strOrdMode = GmCommonClass.parseNull((String)hmOrderDetails.get("RMODE"));
		strAccId = GmCommonClass.parseNull((String)hmOrderDetails.get("ACCID"));
		strOrdId = GmCommonClass.parseNull((String)hmOrderDetails.get("ID"));
		strOrderAdjId = strOrdId;
		strShipAdd = GmCommonClass.parseNull((String)hmOrderDetails.get("SHIPADD"));
		strBillAdd = GmCommonClass.parseNull((String)hmOrderDetails.get("BILLADD"));
		strAccNm = GmCommonClass.parseNull((String)hmOrderDetails.get("ANAME"));
		strDistRepNm = GmCommonClass.parseNull((String)hmOrderDetails.get("REPDISTNM"));
		strOrdredDate =GmCommonClass.getStringFromDate((java.sql.Date)hmOrderDetails.get("ODT"),strApplnDateFmt);
		dtSurgeryDate = (java.sql.Date)hmOrderDetails.get("SDT");
		strSurgeryDt = GmCommonClass.getStringFromDate(dtSurgeryDate,strApplnDateFmt);
		strTotal = (String)hmOrderDetails.get("SALES");
		strShippingCost = GmCommonClass.parseZero((String)hmOrderDetails.get("C501_SHIP_COST"));
		strDOType = GmCommonClass.parseZero((String)hmOrderDetails.get("ORDERTYPE"));
		strDOTypeName = GmCommonClass.parseNull((String)hmOrderDetails.get("ORDERTYPENAME"));
		strCaseID = GmCommonClass.parseNull((String)hmOrderDetails.get("CASE_ID"));
		strCaseInfoID = GmCommonClass.parseNull((String)hmOrderDetails.get("CASEINFO_ID"));
		strRepID = GmCommonClass.parseNull((String)hmOrderDetails.get("REPID"));
		strRepEmail= GmCommonClass.parseNull((String)hmOrderDetails.get("REPEMAIL"));
		strCustomerPO = GmCommonClass.parseNull((String)hmOrderDetails.get("PO"));
		strParentOrderID= GmCommonClass.parseNull((String)hmOrderDetails.get("PARENTORDID"));
		strStatus = GmCommonClass.parseNull((String)hmOrderDetails.get("STATUSFL"));
		strStatusflID = GmCommonClass.parseNull((String)hmOrderDetails.get("STATUSFL"));	   
		strLotOverrideFl = GmCommonClass.parseNull((String)hmOrderDetails.get("LOTOVERRIDEFL"));
		dtOrdSurgeryDate = (java.sql.Date)hmOrderDetails.get("ORD_SURG_DT");
		strOrdSurgeryDate = GmCommonClass.getStringFromDate(dtOrdSurgeryDate,strApplnDateFmt);
		
		strAccCurrSymb = GmCommonClass.parseNull((String)hmOrderDetails.get("ACC_CURR_SYMB"));
		strOvrideCurr = GmCommonClass.parseNull((String)hmOrderDetails.get("OVRIDE_CURR"));
		alRevTrack = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALREVENUETRACK"));
				
		strCurrSign = strAccCurrSymb.equals("")?strCurrSign:strAccCurrSymb; 
		strCurrSign = !strOvrideCurr.equals("")? strOvrideCurr: strCurrSign;
		
		if(strStatus.equals("0")){
			strStatus = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_BACK_ORDER"));
		}else if(strStatus.equals("1")){
			strStatus = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PROCESSING"));
		}else if(strStatus.equals("2")){
			strStatus = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_SHIPPED"));
		}else if(strStatus.equals("7")){
			strStatus = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PENDIGNRELEASE"));
		}else if(strStatus.equals("8")){
			strStatus = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PENDING_CS_CONFIRMATION"));
		}
		
		if(!strCaseID.equals("")){
			if(strDOType.equals("0")){
				strDOTypeName = "Bill and Ship";
			}
		}

		int intTagSize = alTagDetails.size();
		HashMap hmVal = new HashMap();
			for (int i=0;i<intTagSize;i++)
	  		{
				hmVal = (HashMap)alTagDetails.get(i);
	  			strTagID = GmCommonClass.parseNull((String)hmVal.get("TAG_ID"));
				if(strTagID.equals("")){
					blTagDiscrepancy = true;
					break;
				}
			}
		
		intControlSize = alControlNumDetails.size();
		hmVal = new HashMap();
			for (int i=0;i<intControlSize;i++)
			{
				hmVal = (HashMap)alControlNumDetails.get(i);
				strControlNum = GmCommonClass.parseNull((String)hmVal.get("CNUM"));
				strControlNumFL = GmCommonClass.parseNull((String)hmVal.get("CNUMFL"));
				strLotErrormsg = GmCommonClass.parseNull((String)hmVal.get("LOTERRORMSG"));
				inputStr = inputStr +  " <br> \n" + strLotErrormsg ;
				if(strControlNumFL.equals("Y") && strControlNum.equals("")){
		 			blControlNumDiscrepancy = true;
		 			break;
				}
				if(!inputStr.equals("")){
					blLoterrormsg = true;
				}
			}
			hmVal = new HashMap();
			for (int i=0;i<intControlSize;i++){
				hmVal = (HashMap)alControlNumDetails.get(i);
				strControlNum = GmCommonClass.parseNull((String)hmVal.get("CNUM"));
				strPartNum = GmCommonClass.parseNull((String)hmVal.get("PNUM"));
				strControlNumFL = GmCommonClass.parseNull((String)hmVal.get("CNUMFL"));
				if(strControlNumFL.equals("Y") && strControlNum.equals(""))
					strPartNumInputStr = strPartNumInputStr +strPartNum + ",";
			}
			if(strCountryCode.equals("jp") && strSessCompanyLangId.equals("103097")){   //103097:English
				  strBillAdd = strAccNm + "<BR>&nbsp;"+strBillAdd;
				}

	}

	int intSize = 0;
	int intCommentSize = 0;
	HashMap hcboVal = null;	
	HashMap hcmtVal = null;	
	double dbGrandTotal = 0.0;
	double dbAfterTotal = 0.0;
	double dbTotal = 0.0;
	double dbShipTotal = 0.0;
	double dbOrdTotal = 0.0;
	double dbInvTotal = 0.0;
	double dbInvGrandTotal = 0.0;
	double dbTaxTotal = 0.0;
	double dbTaxGrandTotal = 0.0;
	String strTaxTotal = "";
	String strTaxGrandTotal = "";
		
	String strHeaderLbl = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_DELIVERY_ORDER_SUMMARY"));
	String strDODtLbl = "DO Date";
	String strDOIdLbl = "DO ID";
	String strButtonLbl = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PRINT_PICK_SLIP"));	
	String strGrandTotal =  GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_TOTAL"));
	String strGrandBeforeTotal =  GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_TOTAL_BEFORE_ADJ"));
	String strGrandAfterTotal =  GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_TOTAL_AFTER_ADJ"));
	String strGrandAdjTotal ="Adjustments:";
	String strAllign = "right";
	String strUpAdjFl = "";
	String strAdjCode = "";
	String strAdjCodeID = "";
	String strNetUnitPrice = "";	
	if (strDOType.equals("2520"))
	{
		strHeaderLbl = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_QUOTE_SUMMARY"));
		strDODtLbl = "Quote Date";
		strDOIdLbl = "Quote ID ";
		strButtonLbl =  GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PRINT_QUOTE"));
		strGrandTotal = "Grand Total";
		strOrderHeader = "Details";
		strAllign = "left";
	}else if(strDOType.equals("101260")){
		strHeaderLbl = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_ACKNOWLEDGE_ORDER_SUMMARY"));
		strDODtLbl = "Ack. Date";
		strDOIdLbl = "Ack. ID ";
		strButtonLbl =  GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PRINT_ACK"));
		//strColSpanLbl = "";
	}
	
	if(!strParantForm.equals("CASE")){
		strDiscrepancyImage = "";
	}
	
	if(!strCountryCode.equals("en")){
		strButtonLbl =  GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("BTN_PRINT"));
	}
		
	String strColspan = "8";
	String strCollapseStyle = "display:block";
	String strCollapseImage = "/images/minus.gif";
	String strPriceDiscrepancy = "Please enter the control# before releasing the order";
	String strDisablePrint = "";
	// Disable the Print Pick slip button for bill only loaner
	strDisablePrint = strDOType.equals("2530")? "true": "";
	
	if (strParantForm.equals("CASE") && (strDOType.equals("2518")|| strDOType.equals("2519")) && strOrderSource.equals("103521") ) // 103521 : xApp
	{
		// This will determine if the Edit Option is to be displayed for Split Orders booked from DO App. 
		blEditIndTag = true;
		blEditIndShipping = true;
	}else if (strParantForm.equals("CASE") && (strDOType.equals("2518")|| strDOType.equals("2519")))  
	{ 
		blEditUsage = true; 
		blEditTag = true;
		blEditShipping = true;
		
	}
	if(strOrderSource.equals("103521")){
		 blDOFromApp =  true;
	}	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Order Summary</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css">
<link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css">
<script language="javascript" src="<%=strJsPath%>/customerservice/GmPrintPrice.js"></script>
<script language="JavaScript"src="<%=strJsPath%>/Accounts/GmCreditHoldInclude.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxWindows/dhtmlxcontainer.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxWindows/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>	
<script>
var lotOverrideAccess = '<%=strLotOverideAccess%>';
var lotOverRideFl = '<%=strLotOverrideFl%>'
var tdinnner = "";
var varShowIndTagEdit = '<%=blEditIndTag%>';
var varShowIndShipEdit= '<%=blEditIndShipping%>';
var varShowRepReqClmn = '<%=blDOFromApp%>';
var usageRowCnt = '<%=alOrderSummary.size()%>';
var tagRowCnt = '<%=alTagDetails.size()%>';
var shipRowCnt = '<%=alShipSummary.size()%>';
var ordId = '<%=strOrdId%>';
var caseId = '<%=strCaseID%>';
var accountId = '<%=strAccId%>';
var accountNm = '<%=strAccNm%>';
var repEmail='<%=strRepEmail%>';
var repId = '<%=strRepID%>';
var surDt = '<%=strSurgeryDt%>';
var OrderType = '<%=strDOTypeName%>';
var caseinfoID = '<%=strCaseInfoID%>';
var customerPO = '<%=strCustomerPO%>';
var parentOrder = '<%=strParentOrderID%>';
var hidePartPrice = '<%=strhidePartPrice%>';
var fwdFlag = '<%=strFwdFlag%>';
var blControlNumDiscrepancy = '<%=blControlNumDiscrepancy%>';
//for conformation message
var PartNumInputStr ='<%=strPartNumInputStr%>'; 
var intControlSize ='<%=intControlSize%>';
var strSessDeptId = '<%=strSessDeptId%>';
var ordStatusId = '<%=strStatusflID%>';
var dhxWins,w1; 
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();" onload="fnPageOnLoad();">
<FORM name="frmOrder" method="post" action="<%=strServletPath%>/GmOrderItemServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="hTxnId" value="">
<input type="hidden" name="hCancelType" value="">
<input type="hidden" name="hDOType" value="<%=strDOType %>">
<input type="hidden" name="hCntNum" value="">
<input type="hidden" name="hOrderType" value="">
<input type="hidden" name="haction" value="">
<input type="hidden" name="hFwdFlag" value="<%=strFwdFlag%>">
<% if(strClientSysType.equals("IPAD")){%>
	<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
<% }else{%>
	<table style="margin:0 0 0 0;width:900px;border: 1px solid  #676767;"  border="0" cellspacing="0" cellpadding="0">
<%} %>

		<tr>
			<td bgcolor="#666666" colspan="3"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr><td height="25" class="RightDashBoardHeader"><%=strHeaderLbl %></td></tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1" rowspan="4"></td>
		</tr>
			<td bgcolor="#666666" colspan="3"></td>
		<tr>
			<td width="100%" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr class="RightTableCaption" bgcolor="#eeeeee">
									<%if(strDOType.equals("2520")){ %>
										<td height="24" align="center" width="250">&nbsp;<fmtPrintPrice:message key="LBL_BILL_TO"/></td>
										<td bgcolor="#666666" width="1" rowspan="10"></td>
										<td align="center" width="250" height="24">&nbsp;<fmtPrintPrice:message key="LBL_SHIP_TO"/></td>
										<td bgcolor="#666666" width="1" rowspan="10"></td>
										<td width="100" align="center">&nbsp;<fmtPrintPrice:message key="LBL_QUOTE_DATE"/></td>
										<td width="150" align="center" >&nbsp;<fmtPrintPrice:message key="LBL_QUOTE_ID"/></td>
									<%}else{ %>
										<td height="24" align="center" width="350">&nbsp;<fmtPrintPrice:message key="LBL_BILL_TO"/></td>
										<td bgcolor="#666666" width="1" rowspan="10"></td>
										<td align="center" colspan="2">&nbsp;<fmtPrintPrice:message key="LBL_CASE_ID"/></td>
										<td bgcolor="#666666" width="1" rowspan="10"></td>
										<td align="center" colspan="1">&nbsp;<fmtPrintPrice:message key="LBL_SURGERY_DATE"/></td>
										<td align="center" colspan="1">&nbsp;<fmtPrintPrice:message key="LBL_DO_DATE"/></td>
										<td width="150" align="center">&nbsp;<fmtPrintPrice:message key="LBL_DO"/> #</td>
									<%} %>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="9"></td>
								</tr>
								<tr>
									<td class="RightText" rowspan="5" valign="top">&nbsp;<%=strBillAdd%></td>
									<%if(strDOType.equals("2520")){ %>
									<td rowspan="5" class="RightText" valign="top" width="250">&nbsp;<%=strShipAdd%></td>
									<%}else{ %>
									<td class="RightText"  colspan="2" align="center">&nbsp;<%=strCaseID%></td>
									<%} %>
									<td class="RightText"  colspan="1" align="center">&nbsp;<%=strOrdSurgeryDate%></td>
									<td height="25" class="RightText" align="center" colspan="1">&nbsp;<%=strOrdredDate%></td>
									<td class="RightTableCaption" align="center"><table><tr><td align="center"><%=strOrdId%></td>
									<%if(!strDOType.equals("2520")){ %><td align="center">&nbsp;<img src='/GmCommonBarCodeServlet?ID=<%=strOrdId%>&type=2d' height="40" width="40"/></td><%} %></tr></table></td>
								</tr>
					
								<tr>
									 <td bgcolor="#666666" height="1" colspan="10"></td>
								</tr>
								<tr bgcolor="#eeeeee" class="RightTableCaption">
									<%if(!strDOType.equals("2520")){ %>
										<td height="18" align="center" colspan=2 ><fmtPrintPrice:message key="LBL_STATUS"/></td>
									<%} %>
									<td align="center"><fmtPrintPrice:message key="LBL_FIELD_SALES"/></td>
									<td align="center"><fmtPrintPrice:message key="LBL_MODE_OF_ORDER"/></td>
									<td align="center"><fmtPrintPrice:message key="LBL_ACCOUNT_ID"/></td>
								</tr>
								<tr>
									 <td bgcolor="#666666" height="1" colspan="9"></td>
								</tr>
								<tr>
									<%if(!strDOType.equals("2520")){ %>
									<td></td>
									<td align="center" class="RightText">&nbsp;<%=strStatus%><Br></td>
									<%} %>								
									<td align="center" class="RightText">&nbsp;<%=strDistRepNm%><Br></td>
									<td align="center" height="25" class="RightText">&nbsp;<%=strModeOfOrder%></td>
									<td align="center" height="25" class="RightText">&nbsp;<%=strAccId%></td>
									
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr class="ShadeRightTableCaption"><td height="22" colspan="1" >
					<%if(!strDOType.equals("2520")){ %>
					&nbsp;<a href="javascript:fnShowFilters('tabUsage');"  tabindex="-1" >
					<IMG id="tabUsageimg" border=0 src="<%=strImagePath%>/minus.gif"></a>
					<%} %>&nbsp;<%=strOrderHeader %>
					
					<%if(blEditUsage){ 
						int priceDiscr =0;
						for (int i=0;i<alOrderSummary.size();i++)
				  		{
							hcboVal = new HashMap();
							hcboVal = (HashMap)alOrderSummary.get(i);
							String strAttr = GmCommonClass.parseNull((String)hcboVal.get("ATTR"));
							strPrice = (String)hcboVal.get("PRICE");
							strPartNum = (String)hcboVal.get("ID");
							strCurrPrice = GmCommonClass.parseZero((String)hcboVal.get("CURR_PRICE"));
							strUsageFree = GmCommonClass.parseNull((String)hcboVal.get("UFREE"));
							if ((!strPrice.equals(strCurrPrice) && !strPartNum.equals("GPODISC"))||(strPrice.equals("0") && !strPartNum.equals("GPODISC"))){
								if(!strUsageFree.equals("TRUE")){
									priceDiscr++;
								}	
							}	
				  		}
						if(priceDiscr == 0){
							strPriceDiscrepancyImage ="";
						}
						if(!strSessDeptId.equals("S")){%>
							<%=strPriceDiscrepancyImage%>	
						<%}%></td>  <% 
					if((strSessDeptId.equals("S") && strStatus.equals("Pending Release"))|| !strSessDeptId.equals("S")){%> 
					
					<td align="right" ><a href="javascript:fnEditUsageDetails();"><fmtPrintPrice:message key="LBL_EDIT"/></a>&nbsp;</td>
					<%}else{ %>
					<td></td>
					<%}}else{ %>
					<td></td>
					<%} %>
					</tr>
					<tr>
						<td align="center" colspan="2" valign="top" >
							<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0" id="tabUsage" style="display:block">
								<tr class="ShadeBlueBk" bgcolor="#eeeeee">
								<%
								if(!strSessDeptId.equals("S") && !strDOType.equals("2520"))
								{
								%>
									<td width="100" height="24" align="center">&nbsp;<fmtPrintPrice:message key="LBL_PACK"/><BR><fmtPrintPrice:message key="LBL_SLIP"/></td>
									<td width="1" class="Line"></td>
									<%} %>								
									<td  width="100" height="24" align="center">&nbsp;<fmtPrintPrice:message key="LBL_PART"/><BR><fmtPrintPrice:message key="LBL_NUMBER"/></td>
									<td width="1" class="Line"></td>
									<td  width="500">&nbsp;<fmtPrintPrice:message key="LBL_DESCRIPTION"/></td>
									<td width="1" class="Line"></td>
									<td align="center" width="40" style="display: none;"  id="repReqHdr" ><fmtPrintPrice:message key="LBL_REPLN"/><br> <fmtPrintPrice:message key="LBL_REQ"/></td>
									<td width="1" class="Line"  style="display: none;"  id="repReqHdrLine" ></td>
									<td align="center" width="80"><fmtPrintPrice:message key="LBL_QTY"/></td>
									<td width="1" class="Line"></td>
									<%
					if(!strSessDeptId.equals("S") && !strDOType.equals("2520"))
					{
					%>
									<td align="center" width="70"><fmtPrintPrice:message key="LBL_ITEM"/><br><fmtPrintPrice:message key="LBL_TYPE"/></td>
									<td width="1" class="Line"></td>
									<td align="center" width="50"><fmtPrintPrice:message key="LBL_CONST"/>.<br><fmtPrintPrice:message key="LBL_FLAG"/></td>
									<td width="1" class="Line"></td>
									<td align="center" width="120"><fmtPrintPrice:message key="LBL_CONTROL"/><BR><fmtPrintPrice:message key="LBL_NUMBER"/></td>
									<td width="1" class="Line"></td>
									<td align="center" width="80"><fmtPrintPrice:message key="LBL_STATUS"/></td>
									<td width="1" class="Line"></td>
									<td align="center" width="35"><fmtPrintPrice:message key="LBL_UNIT"/><BR> <fmtPrintPrice:message key="LBL_ADJ"/></td>
									<td width="1" class="Line"></td>
									<td align="center" width="35"><fmtPrintPrice:message key="LBL_ADJ"/><BR> <fmtPrintPrice:message key="LBL_CODE"/></td>
									<td width="1" class="Line"></td>									
									<%} %>
				            <%
				            if(strSessDeptId.equals("S")){
				            if(!strhidePartPrice.equals("N")){ %>
									<td align="center" width="100"><fmtPrintPrice:message key="LBL_DO_UNIT"/><Br><fmtPrintPrice:message key="LBL_PRICE"/></td>
									<td width="1" class="Line"></td>
									<td align="center" width="120" colspan="4"><fmtPrintPrice:message key="LBL_TOTAL"/><BR><fmtPrintPrice:message key="LBL_PRICE"/></td>
						<%}else{ %>
						<td align="center" colspan="3"> </td>
						<%}}else{ %>
						<td align="center" width="100"><fmtPrintPrice:message key="LBL_DO_UNIT"/><Br><fmtPrintPrice:message key="LBL_PRICE"/></td>
						<td width="1" class="Line"></td>
						<td align="center" width="120" colspan="4"><fmtPrintPrice:message key="LBL_TOTAL"/><BR><fmtPrintPrice:message key="LBL_PRICE"/></td>
						<%} %>
								</tr>
								<tr>
									<td colspan="27" height="1" bgcolor="#666666"></td>
								</tr>
<%
			  		intSize = alOrderSummary.size();
					hcboVal = new HashMap();
					String strHoldImage = "<img id=imgHold src=".concat(strImagePath).concat("/hold-icon.png title='Price Discrepancy'>");
					boolean blHold = false;					
					if(strHideHold.equals("YES")){
						strHoldImage = "";
					}
			  		for (int i=0;i<intSize;i++)
			  		{
			  			strHideHoldIcon = "Y";
			  			hcboVal = (HashMap)alOrderSummary.get(i);
			  			strOrdId = (String)hcboVal.get("ORDID");
			  			strPartNum = (String)hcboVal.get("ID");
						strDesc = (String)hcboVal.get("PDESC");
						strQty =  GmCommonClass.parseNull((String)hcboVal.get("QTY"));
						strControlNum = (String)hcboVal.get("CNUM");
						strUnitPrice = GmCommonClass.parseZero((String)hcboVal.get("UPRICE"));
						strPortalPrice = GmCommonClass.parseZero((String)hcboVal.get("CURRPORTPRICE"));
						strPrice = GmCommonClass.parseZero((String)hcboVal.get("PRICE"));						
						strTotalPrice = (String)hcboVal.get("EXT_UNIT_PRICE");
						strOrdType = GmCommonClass.parseNull((String)hcboVal.get("ORDTYP"));
						strReplenishReq = GmCommonClass.parseNull((String)hcboVal.get("REPLENISHREQ"));
						strStatus = (String)hcboVal.get("SFL");
						strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
						strItemtype = GmCommonClass.parseNull((String)hcboVal.get("ITEMTYPEDESC"));
						strConFlag = GmCommonClass.parseNull((String)hcboVal.get("CONFL"));
						strCurrPrice = GmCommonClass.parseZero((String)hcboVal.get("CURR_PRICE"));
						strExOrderType = GmCommonClass.parseNull((String)hcboVal.get("DOEXORDERTYPE"));
						strTagID = GmCommonClass.parseNull((String)hcboVal.get("TAG_ID"));
						strUsageFree = GmCommonClass.parseNull((String)hcboVal.get("UFREE"));
						strTaxTotal = GmCommonClass.parseZero((String)hcboVal.get("TAX_COST"));
						strUpAdjFl = GmCommonClass.parseNull((String)hcboVal.get("UADJFL"));
						strAdjCode = GmCommonClass.parseNull((String)hcboVal.get("ADJCODENAME"));
						strAdjCodeID = GmCommonClass.parseNull((String)hcboVal.get("ADJCODEID"));
						strUnitPriceAdj = GmCommonClass.parseZero((String)hcboVal.get("UPRICEADJ"));
						strNetUnitPrice = GmCommonClass.parseZero((String)hcboVal.get("NETUNITPRICE"));
						intQty = Integer.parseInt(strQty)+ intPreQty;
						strItemQty = ""+intQty;						
						
						// Caliculating the Total Before Adjustment 
						dbBeforeItemTotal = Double.parseDouble(strUnitPrice);
						dbBeforeItemTotal = intQty * dbBeforeItemTotal; // Multiply by Qty
						strBeforeItemTotal = ""+dbBeforeItemTotal;
						dbBeforePriceTotal = dbBeforePriceTotal + dbBeforeItemTotal;
						strBeforeTotal = "" + 	dbBeforePriceTotal;
						
						// Caliculating the Total After Adjustment 
						dbAfterItemTotal = Double.parseDouble(strPrice);
						dbAfterItemTotal = intQty * dbAfterItemTotal; // Multiply by Qty
						strAfterItemTotal = ""+dbAfterItemTotal;
						dbAfterPriceTotal = dbAfterPriceTotal + dbAfterItemTotal;
						strAfterPrice = "" + 	dbAfterPriceTotal;
						
						// Caliculating the Adjustment Amount
						dbBeforePrice = Double.parseDouble(strBeforeTotal);
						dbAfterPrice = Double.parseDouble(strAfterPrice);						
						dbAdjustPrice = dbAfterPrice - dbBeforePrice;						
						strAdjustAmount = ""+dbAdjustPrice;
						
						//log.debug(strPrice+":"+strCurrPrice);
						dbTotal = Double.parseDouble(strTotalPrice);
						dbTaxTotal = Double.parseDouble(strTaxTotal);
						dbOrdTotal = dbOrdTotal + dbTotal;						
						
						if (strExOrderType.equals("") ){
							dbInvTotal = dbInvTotal + dbTotal;
							dbTaxGrandTotal = dbTaxGrandTotal + dbTaxTotal;
						}
						if(strItemtype.equals("L")){
							
							String strRepReq = GmCommonClass.parseNull((String)hmRepReqType.get(strOrdId));
							if(!strRepReq.equals("Y")){
							hmRepReqType.put(strOrdId,strReplenishReq);
							}
							
						}else{
							hmRepReqType.put(strOrdId,"Y");
						}	
						
						// Calculation for hold icon
						dbUnitPrice = Double.parseDouble(strPortalPrice);
						dbUnitPriceAdj = Double.parseDouble(strUnitPriceAdj);
						dbNetUnitPriceAdj = Double.parseDouble(strNetUnitPrice);
						dbPrePrice = dbUnitPrice - dbUnitPriceAdj;
						strCalPrice = ""+dbPrePrice;
						strCalNetUnitPrice = "" +dbNetUnitPriceAdj;
						
						/* The following are the conditions that we are checking for Showing the Hold ICON
						   Here, by defalut we are putting HOLD IOCN as showing and then based on the below conditions we are hiding the same.
						   Scenarios need to TEST for Showing and Hiding the HOLD ICON are .... 
						   1. Part with Only Adjustemnt - should not show H Icon.
						   2. Part with Only Adjustemnt but Price Edited in text box - should show H Icon.
						   3. Part with Adjustemnt and Adjustment Code available - should show H Icon.
						   4. Part with Adjustemnt and Adjustment Code available and Edited the Price - should show H Icon.
						   5. Part without Adjustemnt and Adjustment Code available and Edited the Price - should show H Icon.
						   6. Part with both Unit Price Adjustment and Adjustemnt Value - should show H Icon.
						   7. Price Edited from the Net Unit Price Text box without any Unit Price Adj and Adjustment Code - should show H Icon.
						   8. Net Unit Price is NEGATIVE - should show H Icon.
						   9. By Default Net Unit Price loading the ZERO - should show H Icon.
						   10. Manually edit the Price to ZERO -should show H Icon. 
						   
						   What ever the conditon is going to add below for HOld Icon Showing and Hiding, must be need to add in GmSalesOrderAdjustmentRpt.vm file as we are handling the above scenarions here as well.
						 */						
						if(strAdjCodeID.equals("")){
							strHideHoldIcon = "N";
						}
						if((strHideHoldIcon.equals("N") && strUnitPriceAdj.equals("0")) || dbNetUnitPriceAdj < 0){
							strHideHoldIcon = "Y";
						}
						if(strHideHoldIcon.equals("N")){
							if(!strUnitPriceAdj.equals("0") && !strCalPrice.equals(strCalNetUnitPrice)){
								strHideHoldIcon = "Y";
							}							
						}
%>
								<tr>
								<%
					if(!strSessDeptId.equals("S") && !strDOType.equals("2520"))
					{
						strColspan = "26";
					%>
									<td class="RightText" height="20" nowrap>&nbsp;<%=strOrdId%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<%} %>
									<td class="RightText" height="20">&nbsp;<%=strPartNum%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightTextAS">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="center" width="40" style="display: none;"  id="repReqClmn<%=i%>">&nbsp;&nbsp;<%=strReplenishReq%></td>
									<td width="1" bgcolor="#eeeeee" style="display: none;"  id="repReqClmnLine<%=i%>"></td>
									<td class="RightText" align="center">&nbsp;<%=GmCommonClass.getRedText(strQty)%>&nbsp;</td>
									<td width="1" bgcolor="#eeeeee"></td>
									<%
					if(!strSessDeptId.equals("S") && !strDOType.equals("2520"))
					{
					%>
									<td class="RightTextAS" align="center">&nbsp;<%=strItemtype%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightTextAS" align="center">&nbsp;<%=strConFlag%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="center" class="RightText">&nbsp;<%=strControlNum%></td>
									<td width="1" bgcolor="#eeeeee"></td>
								    <td align="center" class="RightTextAS">&nbsp;<%=strStatus%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="center" class="RightTextAS">&nbsp;<%=strUpAdjFl%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="center" class="RightTextAS">&nbsp;<%=strAdjCode%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<%} %>
									 <%
									 if(strSessDeptId.equals("S")){
									 if(!strhidePartPrice.equals("N")){ 
									  %>
									<td class="RightText" align="right">&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strUnitPrice,2))%>&nbsp;</td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText" height="20" align="right" colspan="4" nowrap>	
									<%
									if ((!strPrice.equals(strCurrPrice) && !strPartNum.equals("GPODISC"))||((strPrice.equals("0")) && !strPartNum.equals("GPODISC")))
									{
										if(strHideHoldIcon.equals("Y")){
											if(!strUsageFree.equals("TRUE")){
											blHold = true;
											/*2522	Duplicate Order,
											  2523	Return Order,
											  2528  Credit Memo, 
											  2529  Sales Adjustment*/
											if(!strDOType.equals("2520") && 
											    !(strPrice.equals("0") 
											        && (strOrdType.equals("2522") || strOrdType.equals("2523") 
											        || strOrdType.equals("2528") || strOrdType.equals("2529"))
											        )){
										%>
										<%=strHoldImage %>
										<%
									}
											}
										}
									}
									%>
									<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strTotalPrice,2))%>&nbsp;</td>
									<% }else{%>
									<td class="RightText" height="20" align="right" colspan="3">
									<% }}else{%>
									<td class="RightText" align="right">&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strUnitPrice,2))%>&nbsp;</td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText" height="20" align="right" colspan="4" nowrap>
									<%
									if (((!strPrice.equals(strCurrPrice) && !strPartNum.equals("GPODISC")))||((strPrice.equals("0")) && !strPartNum.equals("GPODISC")))
									{
										if(strHideHoldIcon.equals("Y")){
											if(!strUsageFree.equals("TRUE")){
											blHold = true;
											/*2522	Duplicate Order,
											  2523	Return Order,
											  2528  Credit Memo, 
											  2529  Sales Adjustment*/
											if(!strDOType.equals("2520") && 
											    !(strPrice.equals("0") 
											        && (strOrdType.equals("2522") || strOrdType.equals("2523") 
											        || strOrdType.equals("2528") || strOrdType.equals("2529"))
											        )){
	%>									
										<%=strHoldImage %>
	<%
										}
											}
									}}
%>									
									<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strTotalPrice,2))%>&nbsp;</td>
									<%} %>
								</tr>
								<tr><td colspan="27" height="1" bgcolor="#eeeeee"></td></tr>								
<%
					}
			  		strTotal = ""+dbOrdTotal;
%>								
									<%
									if(blDOFromApp){
										strColspan = "26";
									}
									else
									{
										strColspan = "24";
									}
									 if(strSessDeptId.equals("S")){
									 if(!strhidePartPrice.equals("N")){ 
										 strColspan = "8";
									  %>
								<tr><td colspan="27" height="1" bgcolor="#666666"></td></tr>								
								<!-- Custom tag lib code modified for JBOSS migration changes -->
								<tr >
									<td  height="20" colspan="<%=strColspan %>" align="<%=strAllign %>" class="RightTableCaption"><%=strGrandBeforeTotal%></td>									
										<gmjsp:currency textValue="<%=strBeforeTotal%>" gmClass="RightTableCaption"  type="BoldCurrTextSign" currSymbol="<%=strCurrSign %>"/></tr>	
								<tr >
									<td  height="20" colspan="<%=strColspan %>" align="<%=strAllign %>" class="RightTableCaption">
										 <%if(strViewAdkDetialsFl.equals("Y")){ %>
										 	<fmtPrintPrice:message key="IMG_ALT_OPEN_ADJ" var="varOpnAdj"/>
											<a class="RightText" tabindex=-1 href="javascript:fnOpenAdjustment('<%=strOrderAdjId%>');">  <img id=imgDetail src="<%=strImagePath%>/magnifyGlassSmall.png" title='${varOpnAdj}'></a>
										  <%} %>	
									<fmtPrintPrice:message key="LBL_ADJUSTMENTS"/>: 
								  </td>
			     				  <td  Height="25"   class="RightTableCaption" align="right">&nbsp;<gmjsp:currency td="false" textValue="<%=strAdjustAmount%>"  gmClass="RightTableCaption"  type="BoldCurrTextSign" currSymbol="<%=strCurrSign %>"/></td>
								</tr>
								<tr >
									<td  height="20" colspan="<%=strColspan %>" align="<%=strAllign %>" class="RightTableCaption"><%=strGrandAfterTotal%></td>									
										<gmjsp:currency textValue="<%=strAfterPrice%>" gmClass="RightTableCaption"  type="BoldCurrTextSign" currSymbol="<%=strCurrSign %>"/></tr>
								<%}else{ %>		
								
								<%}} else{%>
								<tr><td colspan="27" height="1" bgcolor="#666666"></td></tr>								
								<tr >
									<td  height="20" colspan="<%=strColspan %>" align="<%=strAllign %>" class="RightTableCaption"><%=strGrandBeforeTotal%></td>									
										<gmjsp:currency textValue="<%=strBeforeTotal%>" gmClass="RightTableCaption"  type="BoldCurrTextSign" currSymbol="<%=strCurrSign %>"/></tr>	
								<tr >
									<td  height="20" colspan="<%=strColspan %>" align="<%=strAllign %>" class="RightTableCaption">
									<%if(strViewAdkDetialsFl.equals("Y")){ %>
											<fmtPrintPrice:message key="IMG_ALT_OPEN_ADJ" var="varOpnAdj"/>
											<a class="RightText" tabindex=-1 href="javascript:fnOpenAdjustment('<%=strOrderAdjId%>');">  <img id=imgDetail src="<%=strImagePath%>/magnifyGlassSmall.png" title='${varOpnAdj}'></a>
										  <%} %>										
									<fmtPrintPrice:message key="LBL_ADJUSTMENTS"/>:
									</td>
	     							<td  Height="25"   class="RightTableCaption" align="right">&nbsp;<gmjsp:currency td="false" textValue="<%=strAdjustAmount%>"  gmClass="RightTableCaption"  type="BoldCurrTextSign" currSymbol="<%=strCurrSign %>"/></td>
								</tr>								 
								<tr >
									<td  height="20" colspan="<%=strColspan %>" align="<%=strAllign %>" class="RightTableCaption"><%=strGrandAfterTotal%></td>									
										<gmjsp:currency textValue="<%=strAfterPrice%>" gmClass="RightTableCaption"  type="BoldCurrTextSign" currSymbol="<%=strCurrSign %>"/></tr>									
								<%} %>		
							</TABLE>
						</TD>
					</TR>
<% if(!strDOType.equals("2520")){ %>
					<%-- Code added by aprasath for showing the surgery Details to Edit --%>
			<%
			if (strShowSurgAtr.equals("YES"))
			{
			%>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr class="ShadeRightTableCaption"><td height="22" colspan="2" ><fmtPrintPrice:message key="LBL_SURGERY_DETAILS"/></td></tr>
					<tr>
						<td colspan="2">
						<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0">
							<tr>
								<%
									intSize = alQuoteDetails.size();
									hcboVal = new HashMap();
									if(intSize !=0){
									for (int i = 0; i < intSize; i++) 
										{			
											hcboVal = (HashMap) alQuoteDetails.get(i);
											strCodeID = (String) hcboVal.get("TRTYPE");
											strCodeNm = (String) hcboVal.get("ATTRVAL");
								%>							
							<td class="RightText" HEIGHT="24" align="right" width="15%"><%=strCodeID %>:&nbsp;&nbsp;</td> <td align="left" width="30%"><%=strCodeNm %></td>
								<%
									if(i%2 == 1){
								%>									
							</tr>
							<tr><td bgcolor="#CCCCCC" height="1" colspan="11"></td></tr>
							<tr>
								<%}
								}
								}else{
								%>
								<td  height="20" colspan="11" align="center" class="RightText">&nbsp; <fmtPrintPrice:message key="LBL_NO_DATA_AVAILABLE"/></td>
								<%} %>
							</tr>
						</table>
						</td>
					</tr>
				<%} %>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr class="ShadeRightTableCaption"><td height="22" colspan="1">&nbsp;<a href="javascript:fnShowFilters('tabTag');"  tabindex="-1" ><IMG id="tabTagimg" border=0 src="<%=strImagePath%>/plus.gif"></a>&nbsp;<fmtPrintPrice:message key="LBL_TAG_DETAILS"/>
					 <%if(blTagDiscrepancy && strParantForm.equals("CASE")&& (strDOType.equals("2518")|| strDOType.equals("2519"))){ %>
					<%=strDiscrepancyImage%>&nbsp;
					<%} %></td>
				  <%if(blEditTag || blEditIndTag){
					  if((strSessDeptId.equals("S") && strStatus.equals("Pending Release"))|| !strSessDeptId.equals("S")){%>
					<td align="right" ><a href="javascript:fnEditDetails('TagDetails')"><fmtPrintPrice:message key="LBL_EDIT"/></a>&nbsp;</td>
					<%}else{ %>
					<td></td>
					<%}}else{ %>
					<td></td>
					<%} %>
					</tr>
					<TR>
						<TD colspan="2">
							<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0" id="tabTag" style="display:none">
								<tr class="ShadeBlueBk" >
									<td width="80" height="20" align="center">&nbsp;<fmtPrintPrice:message key="LBL_PART"/><br><fmtPrintPrice:message key="LBL_NUMBER"/></td>
									<td width="1" class="Line"></td>
									<td  width="390">&nbsp;<fmtPrintPrice:message key="LBL_DESCRIPTION"/></td>
									<td width="1" class="Line"></td>
									<td align="center" width="40"><fmtPrintPrice:message key="LBL_QTY"/></td>
									<td width="1" class="Line"></td>
									<td align="center" width="120"><fmtPrintPrice:message key="LBL_TAG_ID"/></td>
									<td width="1" class="Line"></td>
									<td align="center" width="100"><fmtPrintPrice:message key="LBL_ITEM"/> <br><fmtPrintPrice:message key="LBL_TYPE"/></td>
									
								</tr>
								<tr><td bgcolor="#666666" height="1" colspan="11"></td></tr>
<%
			  		intSize = alTagDetails.size();
					hcboVal = new HashMap();
					if(intSize !=0){
					for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alTagDetails.get(i);
			  			strOrdId = (String)hcboVal.get("ORDER_ID");
			  			strPartNum = (String)hcboVal.get("PNUM");
						strDesc = (String)hcboVal.get("PDESC");
						strQty = (String)hcboVal.get("IQTY");
						strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
						strItemtype = GmCommonClass.parseNull((String)hcboVal.get("ITYPE"));
						strTagID = GmCommonClass.parseNull((String)hcboVal.get("TAG_ID"));
						if(strTagID.equals("") && strParantForm.equals("CASE")&& (strDOType.equals("2518")|| strDOType.equals("2519"))){
							strColor = "RightTextRed";
							strDesColor = "RightTextRed";
						}else {
							strColor = "RightText";
							strDesColor = "RightTextAs";
						}
						//log.debug(strPrice+":"+strCurrPrice);
					
%>
								<tr class="<%=strColor%>">
									<td  height="20">&nbsp;<%=strPartNum%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="<%=strDesColor%>">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td >&nbsp;<%=strQty%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td >&nbsp;<%=strTagID%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td  class="<%=strDesColor%>" align="center"><%=strItemtype%>&nbsp;&nbsp;</td>
									
								</tr>
<%								} // For Loop ends here					
					}else{
%>
<tr>
									<td  height="20" colspan="11" align="center" class="RightText">&nbsp; <fmtPrintPrice:message key="LBL_NO_DATA_AVAILABLE"/></td>
								</tr>
								<%} %>

							</table>
						</td>
					</tr>
					<%
					log.debug("strParantForm::"+strParantForm+"strDOType::"+strDOType+"strStatus:"+strStatus);
					%>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr>
						<td colspan="2">
							<jsp:include page="/custservice/GmDoSummaryCtrlNum.jsp">
								<jsp:param name="FRMNAME" value="frmOrder" />
							</jsp:include>
						</td>
					</tr>
						<!-- Show the usage lot error message below to Control Number Details section -->				
						<%if (blLoterrormsg){%>
							<tr><td colspan="15" class="Line" height="1"></td></tr>
							 	<tr id="DivHoldFl"><td align="center" colspan="15" class="RightTableCaption">
									<font color="red"><%=inputStr%></font></td>
								</tr>
						    <% }  %>
					<%
					if(!strSessDeptId.equals("S"))
					{
						if(alComment.size() == 0){
							strCollapseStyle = "display:none";
							strCollapseImage = "/images/plus.gif";
						}
					%>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr class="ShadeRightTableCaption"><td height="22" colspan="2" >&nbsp;<a href="javascript:fnShowFilters('tabComments');"  tabindex="-1" ><IMG id="tabCommentsimg" border=0 src="<%=strCollapseImage%>"></a>&nbsp;<fmtPrintPrice:message key="IMG_COMMENTS"/></td></tr>
					<TR>
						<TD colspan="2">
							<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0" id="tabComments" style="<%=strCollapseStyle%>">
								<tr class="ShadeBlueBk" bgcolor="#eeeeee">
									<td width="90" height="20">&nbsp;<fmtPrintPrice:message key="LBL_REF_ID"/></td>
									<td width="1" class="Line"></td>
									<td width="50" align="center"><fmtPrintPrice:message key="LBL_TYPE"/></td>
									<td width="1" class="Line"></td>
									<td width="120" align="center"><fmtPrintPrice:message key="LBL_USER_NAME"/></td>
									<td width="1" class="Line"></td>
									<td width="130" align="center"><fmtPrintPrice:message key="LBL_DATE"/></td>
									<td width="1" class="Line"></td>
									<td align="center"><fmtPrintPrice:message key="LBL_COMMENTS"/></td>									
								</tr>
								<tr><td bgcolor="#666666" height="1" colspan="11"></td></tr>
<%	
							intCommentSize = alComment.size();
							if (intCommentSize != 0)
							{
						  		for (int i=0;i<intCommentSize;i++)
						  		{
					  			hcmtVal = (HashMap)alComment.get(i);

					  			strCmtId = (String)hcmtVal.get("ID");
					  			strUserNm = GmCommonClass.parseNull((String)hcmtVal.get("UNAME"));
					  			strDT = GmCommonClass.parseNull((String) hcmtVal.get("DT"));
					  			strComment = GmCommonClass.parseNull((String)hcmtVal.get("COMMENTS"));	
					  			strCommentType = GmCommonClass.parseNull((String)hcmtVal.get("COMMENTTYPE"));
%>
								<tr>
									<td class="RightText" height="20">&nbsp;<%=strCmtId%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText" align="left">&nbsp;<%=strCommentType%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText" align="left">&nbsp;<%=strUserNm%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText" align="left">&nbsp;<%=strDT%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText" align="left">&nbsp;<%=strComment%></td>									
								</tr>
								<tr><td colspan="11" height="1" bgcolor="#eeeeee"></td></tr>
<%								} // For Loop ends here					
							}else
							{
%>		
							<tr>
									<td  height="20" colspan="11" align="center" class="RightText">&nbsp; <fmtPrintPrice:message key="LBL_NO_DATA_AVAILABLE"/></td>
								</tr>
<%					
							}
%>					
							</table>
						</td>
					</tr>

<%
					strCollapseStyle = "display:block";
					strCollapseImage = "/images/minus.gif";
					if(alRevTrack.size()==0){
						strCollapseStyle = "display:none";
						strCollapseImage = "/images/plus.gif";
					}
 %>
                    <tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr class="ShadeRightTableCaption"><td height="22" colspan="2">&nbsp;<a href="javascript:fnShowFilters('tabRevenueTrk');"  tabindex="-1" ><IMG id="tabRevenueTrkimg" border=0 src="<%=strCollapseImage%>"></a>&nbsp;<fmtPrintPrice:message key="LBL_REVENUE_TRACKING"/></td></tr>
					<TR>
						<TD colspan="2">
							<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0" id="tabRevenueTrk" style="<%=strCollapseStyle%>">
								<tr class="ShadeBlueBk" >
									<td align="center">&nbsp;<fmtPrintPrice:message key="LBL_STATUS"/></td>
									<td width="1" class="Line"></td>
									<td width="180" height="20" align="center">&nbsp;<fmtPrintPrice:message key="LBL_TRACKING_TYPE"/></td>
									<td width="1" class="Line"></td>
									<td align="center">&nbsp;<fmtPrintPrice:message key="LBL_UPDATED_BY"/></td>
									<td width="1" class="Line"></td>
									<td align="center">&nbsp;<fmtPrintPrice:message key="LBL_UPDATED_DATE"/></td>
									<td width="1" class="Line"></td>
																
								</tr>
								<tr><td bgcolor="#666666" height="1" colspan="11"></td></tr>
<%
						if (alRevTrack != null)
						{
							intSize = alRevTrack.size();
						}
							if (intSize != 0)
							{
						  		for (int i=0;i<intSize;i++)
						  		{
						  		hmReturn = (HashMap)alRevTrack.get(i);
					  			strStatusID = GmCommonClass.parseNull((String)hmReturn.get("PO_DO_DTLS"));
					  			strStatusNM = GmCommonClass.parseNull((String)hmReturn.get("PODOVAL"));
					  			strTrackType = GmCommonClass.parseNull((String)hmReturn.get("TRTYPE"));
					  			strUpdatedBy = GmCommonClass.parseNull((String) hmReturn.get("UPBY"));
					  			strUpdateDate = GmCommonClass.getStringFromDate((java.sql.Date)(hmReturn.get("UDATE")),strApplnDateFmt);
%>	
					  		<tr>
					  		    <td class="RightText" align="left">&nbsp;<%=strStatusNM%></td>
								<td width="1" bgcolor="#eeeeee"></td>
								<td class="RightText" align="left">&nbsp;<%=strTrackType%></td>
								<td width="1" bgcolor="#eeeeee"></td>
								<td class="RightText" align="left">&nbsp;<%=strUpdatedBy%></td>
								<td width="1" bgcolor="#eeeeee"></td>
								<td class="RightText" align="left">&nbsp;<%=strUpdateDate%></td>
								<td width="1" bgcolor="#eeeeee"></td>
													
							</tr>
							<tr><td colspan="11" height="1" bgcolor="#eeeeee"></td></tr>
<%								} // For Loop ends here					
							}else{
%>
								<tr>
									<td  height="20" colspan="7" align="center" class="RightText">&nbsp; <fmtPrintPrice:message key="LBL_NO_DATA_AVAILABLE"/></td>
								</tr>
<%					
							}
%>								
							</table>
						</td>
					</tr>
					<!-- The Below Code is added for PMT-5022,In DO Summary Screen There is new section Added ,
where it will display the payment details when a partial payment is done. -->
<% 
					strCollapseStyle = "display:block";
					strCollapseImage = "/images/minus.gif";
					if(alInvoice.size()==0){
						strCollapseStyle = "display:none";
						strCollapseImage = "/images/plus.gif";
					}
%> 
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr class="ShadeRightTableCaption"><td height="22" colspan="2">&nbsp;<a href="javascript:fnShowFilters('tabInvoice');"  tabindex="-1" ><IMG id="tabInvoiceimg" border=0 src="<%=strCollapseImage%>"></a>&nbsp;<fmtPrintPrice:message key="LBL_INOVICE_DETAILS"/></td></tr>
					<TR>
						<TD colspan="2">
							<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0" id="tabInvoice" style="<%=strCollapseStyle%>">
								<tr class="ShadeBlueBk" >
									<td width="80" height="20">&nbsp;<fmtPrintPrice:message key="LBL_PACK_SLIP"/></td>
									<td width="1" class="Line"></td>
									<td>&nbsp;<fmtPrintPrice:message key="LBL_CUSTOMER_PO"/></td>
									<td width="1" class="Line"></td>
									<td>&nbsp;<fmtPrintPrice:message key="LBL_INOVICE_ID"/></td>
									<td width="1" class="Line"></td>
									<td>&nbsp;<fmtPrintPrice:message key="LBL_GENERATED_BY"/></td>
									<td width="1" class="Line"></td>
									<td>&nbsp;<fmtPrintPrice:message key="LBL_GENERATED_DATE"/></td>
									<td width="1" class="Line"></td>
									
								</tr>
								<tr><td bgcolor="#666666" height="1" colspan="11"></td></tr>
<%
						if (alInvoice != null)
						{
							intSize = alInvoice.size();
						}
							if (intSize != 0)
							{
						  		for (int i=0;i<intSize;i++)
						  		{
					  			hcboVal = (HashMap)alInvoice.get(i);

					  			strOrdId = (String)hcboVal.get("ORDID");
					  			strConId = GmCommonClass.parseNull((String)hcboVal.get("CUSTPO"));
								strConNm = GmCommonClass.parseNull((String)hcboVal.get("SBID"));
								String strCreatedBy = GmCommonClass.parseNull((String)hcboVal.get("CREATEDBY"));
								String strInvoiceDate = GmCommonClass.parseNull((String)hcboVal.get("INVCREATEDDATE"));
%>
								<tr>
									<td class="RightText" height="20">&nbsp;<%=strOrdId%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText">&nbsp;<%=strConId%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText">&nbsp;<%=strConNm%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText">&nbsp;<%=strCreatedBy%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText">&nbsp;<%=strInvoiceDate%></td>
									<td width="1" bgcolor="#eeeeee"></td>
								</tr>
<%								} // For Loop ends here					
							}else{
%>
								<tr>
									<td  height="20" colspan="7" align="center" class="RightText">&nbsp; <fmtPrintPrice:message key="LBL_NO_DATA_AVAILABLE"/></td>
								</tr>
<%					
							}
%>								
							</table>
						</td>
					</tr>
					

<!-- The Below Code is added for PMT-5022,In DO Summary Screen There is new section Added ,
where it will display the payment details when a partial payment is done. -->
<% 
					strCollapseStyle = "display:block";
					strCollapseImage = "/images/minus.gif";
					if(alPayment.size()==0){
						strCollapseStyle = "display:none";
						strCollapseImage = "/images/plus.gif";
					}
%>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr class="ShadeRightTableCaption"><td height="22" colspan="2">&nbsp;<a href="javascript:fnShowFilters('tabPayment');"  tabindex="-1" ><IMG id="tabPaymentimg" border=0 src="<%=strCollapseImage%>"></a>&nbsp;<fmtPrintPrice:message key="LBL_PAYMENT_DETAILS"/></td></tr>
					<TR>
						<TD colspan="2">
							<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0" id="tabPayment" style="<%=strCollapseStyle%>">
								<tr class="ShadeBlueBk" bgcolor="#eeeeee">
									<td  height="20">&nbsp;<fmtPrintPrice:message key="LBL_INVOICE_ID"/></td>
									<td width="1" class="Line"></td>
									<td>&nbsp;<fmtPrintPrice:message key="LBL_AMOUNT"/></td>
									<td width="1" class="Line"></td>
									<td><fmtPrintPrice:message key="LBL_DEPOSIT"/>&nbsp;<fmtPrintPrice:message key="LBL_DATE"/></td>
									<td width="1" class="Line"></td>
									<td>&nbsp;<fmtPrintPrice:message key="LBL_MODE"/></td>
									<td width="1" class="Line"></td>
									<td width = "300">&nbsp;<fmtPrintPrice:message key="LBL_DETAILS"/></td>
									<td width="1" class="Line"></td>
									<td>&nbsp;<fmtPrintPrice:message key="LBL_POSTED"/>&nbsp;<fmtPrintPrice:message key="LBL_BY"/></td>
									<td width="1" class="Line"></td>
									<td>&nbsp;<fmtPrintPrice:message key="LBL_POSTED"/>&nbsp;<fmtPrintPrice:message key="LBL_DATE"/></td>
									<td width="1" class="Line"></td>
								</tr>
								<tr><td bgcolor="#666666" height="1" colspan="13"></td></tr>

<%
						if (alPayment != null)
						{
							intSize = alPayment.size();
						}
							
							if (intSize != 0)
							{
								String strAmount = "";
								String strDetails = "";
								String strInvoiceId = "";
								String strPaymentBy = "";
								String strMode = "";
								java.sql.Date strPaymentDate = null;
								java.sql.Date strDate = null;
								
						  		for (int i=0;i<intSize;i++)
						  		{
					  			hcboVal = (HashMap)alPayment.get(i);

					  			
								strInvoiceId = GmCommonClass.parseNull((String)hcboVal.get("INVID"));
								strAmount = GmCommonClass.parseNull((String)hcboVal.get("PAYAMT"));
								strDate = (java.sql.Date)(hcboVal.get("RDATE"));
								strMode = GmCommonClass.parseNull((String)hcboVal.get("RMODE"));
								strDetails = GmCommonClass.parseNull((String)hcboVal.get("DETAILS"));
								strPaymentBy = GmCommonClass.parseNull((String)hcboVal.get("CREATEDBY"));
								strPaymentDate = (java.sql.Date)(hcboVal.get("CREATEDDATE"));
%>

								<tr>
									<td class="RightText">&nbsp;<%=strInvoiceId%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="right" class="RightText"><%=GmCommonClass.getStringWithCommas(strAmount)%>&nbsp;&nbsp;</td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="center" class="RightText"><%=GmCommonClass.getStringFromDate(strDate,strApplnDateFmt)%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="center" class="RightText"><%=strMode%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td width = "300" class="RightText"><%=strDetails%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText"><%=strPaymentBy%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td align="center" class="RightText"><%=GmCommonClass.getStringFromDate(strPaymentDate,strApplnDateFmt)%></td>
									<td width="1" bgcolor="#eeeeee"></td>
								</tr>
<%								} // For Loop ends here					
							}else{
%>
								<tr>
									<td  height="20" colspan="13" align="center" class="RightText">&nbsp; <fmtPrintPrice:message key="LBL_NO_DATA_AVAILABLE"/></td>
								</tr>
<%					
							}
%>	
<!-- Code added for PMT-5022 ends here. -->	
</table>
						</td>
	</tr>
	<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
	<% 
					strCollapseStyle = "display:block";
					strCollapseImage = "/images/minus.gif";
					if(alConstructSummary.size()==0){
						strCollapseStyle = "display:none";
						strCollapseImage = "/images/plus.gif";
					}
%>
					<tr class="ShadeRightTableCaption"><td height="22" colspan="2" >&nbsp;<a href="javascript:fnShowFilters('tabConstruct');"  tabindex="-1" ><IMG id="tabConstructimg" border=0 src="<%=strCollapseImage%>"></a>&nbsp;<fmtPrintPrice:message key="LBL_CONSTRUCT_DETAILS"/></td></tr>
					<TR>
						<TD colspan="2">
							<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0" id="tabConstruct" style="<%=strCollapseStyle%>">
								<tr class="ShadeBlueBk" bgcolor="#eeeeee">
									<td width="80" height="20">&nbsp;<fmtPrintPrice:message key="LBL_PACK_SLIP"/></td>
									<td width="1" class="Line"></td>
									<td>&nbsp;<fmtPrintPrice:message key="LBL_CONSTRUCT_ID"/></td>
									<td width="1" class="Line"></td>
									<td>&nbsp;<fmtPrintPrice:message key="LBL_NAME"/></td>
									<td width="1" class="Line"></td>
									<td align="center"><fmtPrintPrice:message key="LBL_VALUE"/></td>
								</tr>
								<tr><td bgcolor="#666666" height="1" colspan="11"></td></tr>
<%	
						if (alConstructSummary != null)
						{
							intSize = alConstructSummary.size();
						}
							if (intSize != 0)
							{
						  		for (int i=0;i<intSize;i++)
						  		{
					  			hcboVal = (HashMap)alConstructSummary.get(i);

					  			strOrdId = (String)hcboVal.get("ORDID");
					  			strConId = GmCommonClass.parseNull((String)hcboVal.get("CONID"));
								strConNm = GmCommonClass.parseNull((String)hcboVal.get("CONNM"));
								strConValue = GmCommonClass.parseZero((String)hcboVal.get("CVALUE"));
%>
								<tr>
									<td class="RightText" height="20">&nbsp;<%=strOrdId%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText">&nbsp;<%=strConId%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText">&nbsp;<%=strConNm%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText" align="right">$<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strConValue,2))%>&nbsp;</td>
								</tr>
<%								} // For Loop ends here					
							}else{
%>
								<tr>
									<td  height="20" colspan="7" align="center" class="RightText">&nbsp; <fmtPrintPrice:message key="LBL_NO_DATA_AVAILABLE"/></td>
								</tr>
<%					
							}
%>								
							</table>
						</td>
					</tr>
					
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
<% 
					strCollapseStyle = "display:block";
					strCollapseImage = "/images/minus.gif";
					if(alReturnDetails.size()== 0){
						strCollapseStyle = "display:none";
						strCollapseImage = "/images/plus.gif";
					}
%>
					<tr class="ShadeRightTableCaption"><td height="22" colspan="2" >&nbsp;<a href="javascript:fnShowFilters('tabReturns');"  tabindex="-1" ><IMG id="tabReturnsimg" border=0 src="<%=strCollapseImage%>"></a>&nbsp;<fmtPrintPrice:message key="LBL_RETURN_DETAILS"/></td></tr>
					<TR>
						<TD colspan="2">
							<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0" id="tabReturns" style="<%=strCollapseStyle%>">
								<tr class="ShadeBlueBk" bgcolor="#eeeeee">
									<td width="80" height="20">&nbsp;<fmtPrintPrice:message key="LBL_RA_ID"/></td>
									<td width="1" class="Line"></td>
									<td>&nbsp;<fmtPrintPrice:message key="LBL_RETURN_DATE"/></td>
									<td width="1" class="Line"></td>
									<td>&nbsp;<fmtPrintPrice:message key="LBL_OPERATION_CREDITED_DATE"/></td>
									<td width="1" class="Line"></td>
									<td align="center"><fmtPrintPrice:message key="LBL_ACCOUNTING_CREDITED_DATE"/></td>
								</tr>
								<tr><td bgcolor="#666666" height="1" colspan="11"></td></tr>
<%	
						if (alReturnDetails != null)
						{
							intSize = alReturnDetails.size();
						}
							if (intSize != 0)
							{
						  		for (int i=0;i<intSize;i++)
						  		{
					  			hcboVal = (HashMap)alReturnDetails.get(i);

					  			strRAId = GmCommonClass.parseNull((String)hcboVal.get("RA_ID"));
					  			dtReturnDate = (Date) hcboVal.get("RETURN_DATE");
								dtCreditedDate = (Date) hcboVal.get("OP_CREDIT_DT");
								dtAccountCreditDate = (Date) hcboVal.get("ACC_CREDIT_DT");
								
								strReturnDate = GmCommonClass.getStringFromDate(dtReturnDate,strApplnDateFmt);
								strCreditedDate = GmCommonClass.getStringFromDate(dtCreditedDate,strApplnDateFmt);
								strAccountCreditDate = GmCommonClass.getStringFromDate(dtAccountCreditDate,strApplnDateFmt);
%>
								<tr>
									<td class="RightText" height="20">&nbsp;<%=strRAId%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText">&nbsp;<%=strReturnDate%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText">&nbsp;<%=strCreditedDate%></td>
									<td width="1" bgcolor="#eeeeee"></td>
									<td class="RightText" >&nbsp;<%=strAccountCreditDate %></td>
								</tr>
<%								} // For Loop ends here					
							}else{
%>
								<tr>
									<td  height="20" colspan="7" align="center" class="RightText">&nbsp; <fmtPrintPrice:message key="LBL_NO_DATA_AVAILABLE"/></td>
								</tr>
<%					
							}
%>								
							</table>
						</td>
					</tr>
<%
	} 
%>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr class="ShadeRightTableCaption"><td height="22" colspan="1" >&nbsp;<a href="javascript:fnShowFilters('tabShip');"  tabindex="-1" ><IMG id="tabShipimg"  border=0 src="<%=strImagePath%>/minus.gif"></a>&nbsp;<fmtPrintPrice:message key="LBL_SHIPPING_DETAILS"/></td>
					<%
					if( (strParantForm.equals("CASE") && (strDOType.equals("2518")|| strDOType.equals("2519"))) && strOrderSource.equals("") ){ 
						if((strSessDeptId.equals("S") && strStatus.equals("Pending Release"))|| !strSessDeptId.equals("S")){
							if(alShipSummary.size() >=2 ){
								blEditShipping = true;	
							}else{
								blEditShipping = true;	
							}
						}
					}
					
					if(blEditShipping){  %>
						<td align="right"><a href="javascript:fnEditShippDetails('single')"><fmtPrintPrice:message key="LBL_EDIT"/></a>&nbsp;</td>						
					<td></td>
					<%}else{ %>
					<td align="right">&nbsp;</td>
					<td></td>
					<%} %>
					</tr>
					<TR>
						<TD colspan="2">
							<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0" id="tabShip" style="display:block">
								<tr class="ShadeBlueBk" bgcolor="#eeeeee">
								<%
					if(!strSessDeptId.equals("S"))
					{
					%>
								    <td width="110" height="20">&nbsp;<fmtPrintPrice:message key="LBL_PACK"/>&nbsp;<fmtPrintPrice:message key="LBL_SLIP"/></td>
									<td width="1px" bgcolor="#676767"></td>
									<%} %>
									<td align="center" width="80"><fmtPrintPrice:message key="LBL_SHIP"/>&nbsp;<fmtPrintPrice:message key="LBL_DATE"/></td>
									<td width="1px" bgcolor="#676767"></td>
									<td align="center" width="100"><fmtPrintPrice:message key="LBL_SHIP"/>&nbsp;<fmtPrintPrice:message key="LBL_MODE"/></td>
									<td width="1px" bgcolor="#676767"></td>
									<td align="center" width="80"><fmtPrintPrice:message key="LBL_SHIP"/>&nbsp;<fmtPrintPrice:message key="LBL_CARRIER"/></td>
									<td width="1px" bgcolor="#676767"></td>
									<td align="center" width="280"><fmtPrintPrice:message key="LBL_SHIPPING"/><BR><fmtPrintPrice:message key="LBL_ADDRESS"/></td>	
									<td width="1px" bgcolor="#676767" style="display:none;"  id="shipHdrEditLine"></td>
									<td align="center" width="30" style="display:none;"  id="shipHdrEdit"><fmtPrintPrice:message key="LBL_EDIT"/></td>
									<td width="1px" bgcolor="#676767"></td>
									<td align="center" width="90"><fmtPrintPrice:message key="LBL_TRACKING"/>&nbsp;#</td>
									<td width="1px" bgcolor="#676767"></td>
									<td align="center" width="60"><fmtPrintPrice:message key="LBL_SHIPPING"/> <BR> <fmtPrintPrice:message key="LBL_CHARGES"/></td>
								</tr>
								<tr><td bgcolor="#666666" height="1" colspan="15"></td></tr>
<%	
							strColspan = "10";
							if(!strSessDeptId.equals("S"))
							{
								strColspan = "12";
							}
							intSize = alShipSummary.size();
							if (intSize != 0)
							{
						  		for (int i=0;i<intSize;i++)
						  		{
					  			hcboVal = (HashMap)alShipSummary.get(i);

					  			strOrdId = (String)hcboVal.get("ORDID");
					  			strShipDate =GmCommonClass.getStringFromDate((java.sql.Date)hcboVal.get("SDT"),strApplnDateFmt);
					  			//strShipDate = GmCommonClass.parseNull((String)hcboVal.get("SDT"));
								strShipMode = GmCommonClass.parseNull((String)hcboVal.get("SMODENM"));
								strShipCarr = GmCommonClass.parseNull((String)hcboVal.get("SCARNM"));
								strTrack = GmCommonClass.parseNull((String)hcboVal.get("STRK"));
								strPrice = GmCommonClass.parseZero((String)hcboVal.get("SCOST"));
								strRepName = GmCommonClass.parseNull((String)hcboVal.get("REP_NAME"));
								strAdd1 = GmCommonClass.parseNull((String)hcboVal.get("ADDRESS1"));
								strAdd2 = GmCommonClass.parseNull((String)hcboVal.get("ADDRESS2"));
								strCity = GmCommonClass.parseNull((String)hcboVal.get("CITY"));
								strState = GmCommonClass.parseNull((String)hcboVal.get("STATE"));
								strZipcode = GmCommonClass.parseNull((String)hcboVal.get("ZIPCODE"));
								String address = strRepName+(strAdd1.equals("")?"":"<br>&nbsp;")+strAdd1+(strAdd2.equals("")?"":"<br>&nbsp;")+strAdd2+(strCity.equals("")?"":"<br>&nbsp;")+strCity+(strState.equals("")?"":"<br>&nbsp;")+strState+(strCity.equals("")?"":"&nbsp;"+strZipcode);
								if(strShowAddrFl.equals("YES")){
									address = strShipAdd;
									}

								dbTotal = Double.parseDouble(strPrice);
								dbShipTotal = dbShipTotal + dbTotal;
								strOrdId = (String)hcboVal.get("ORDID");
								strShipDate = strShipDate.equals("")?"Not Available":strShipDate;
								String strRep = GmCommonClass.parseNull((String)hmRepReqType.get(strOrdId));
%>
								<tr>
								<%
					if(!strSessDeptId.equals("S"))
					{
					%>
									<td class="RightText" height="20">&nbsp;<%=strOrdId%></td>
									<td width="1px" bgcolor="#eeeeee"></td>
									<%} %>
									<td class="RightText">&nbsp;<%=strShipDate%></td>
									<td width="1px" bgcolor="#eeeeee"></td>
									<td class="RightText" align="center">&nbsp;<%=strShipMode%></td>
									<td width="1px" bgcolor="#eeeeee"></td>
									<td class="RightText" align="center">&nbsp;<%=strShipCarr%></td>
									<td width="1px" bgcolor="#eeeeee"></td>
									<td class="RightText" valign="top">&nbsp;<%=address%></td>
									<td width="1px" bgcolor="#eeeeee" style="display:none;"  id="shipDtlEditLine<%=i%>"></td>
									<% if (strRep.equals("Y")){ %>
									<td class="RightText" valign="top" style="display:none;" id="shipDtlEdit<%=i%>"><a href="javascript:fnEditShippInfo('single','<%=strOrdId%>')"><fmtPrintPrice:message key="LBL_EDIT"/></a>&nbsp;</td>
									<%} %>
									<td width="1px" bgcolor="#eeeeee"></td>
									<td align="center" class="RightText">&nbsp;<%=strTrack%></td>
									<td width="1px" bgcolor="#eeeeee"></td>
									<td class="RightText" align="right">&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strPrice,2))%>&nbsp;</td>
								</tr>
								<tr><td colspan="15" height="1" bgcolor="#eeeeee"></td></tr>
<%								} // For Loop ends here					
							}// Shipping details end here
							
							dbGrandTotal  = dbShipTotal + dbOrdTotal;
							strTotal = ""+dbGrandTotal;
							strTaxGrandTotal = ""+dbTaxGrandTotal;
							dbAfterTotal = Double.parseDouble(strAfterPrice);
							dbInvGrandTotal = (dbShipTotal + dbInvTotal + dbTaxGrandTotal)+dbAdjustPrice; 
							dbSubTotal = dbShipTotal + dbAfterTotal;
							strSubTotal = ""+dbSubTotal;
							strInvTotal = ""+dbInvGrandTotal;
%>								
<tr>
				<td colspan=14>
					<jsp:include page='<%=strIncJSP %>' >
						<jsp:param name="COLSPAN" value='<%=strColspan %>' />
						<jsp:param name="ORDERSOURCE" value='<%=strOrderSource %>' />
						<jsp:param name="STRSESSDEPTID" value='<%=strSessDeptId %>' />
						<jsp:param name="STRHIDEPARTPRICE" value='<%=strhidePartPrice %>' />
						<jsp:param name="INVTOTAL" value='<%=strInvTotal %>' />
						<jsp:param name="GRANDTOTAL" value='<%=strTaxGrandTotal %>' />
						<jsp:param name="SUBTOTAL" value='<%=strSubTotal %>' />
					</jsp:include>
				</td>
			</tr>

								 <%if(strParantForm.equals("CASE") && (!strDOType.equals("2518")) && strSessDeptId.equals("S")){ %>
			 						<tr><td colspan="13" class="Line" height="1"></td></tr>
			 						<fmtPrintPrice:message key="BTN_BACK" var="varBack"/>
									<tr><td colspan="13" align="center"> <gmjsp:button value="&nbsp;${varBack}&nbsp;" gmClass="button" name="Btn_Back" buttonType="Load" onClick="fnBack();" /> </td></tr>
							 <%} %>
							 <%if (blHold && (strHoldFl.equals("Y")|| strSessDeptId.equals("S")) && !strDOType.equals("2520")){%>
							 	<tr><td colspan="15" class="Line" height="1"></td></tr>
							 	<tr id="DivHoldFl"><td align="center" colspan="15" class="RightTableCaption">
									<font color="red"><fmtPrintPrice:message key="LBL_PRICING_DISCREPANCY"/></font></td>
								</tr>
							<% } %>	
							<!-- Code added for PMT-12763 checks For PDF and displays notification if PDF upload is not successful -->	
							<%
									intSize = alOrderAtbDetails.size();
									hcboVal = new HashMap();
									if(intSize !=0){
									for (int i = 0; i < intSize; i++) 
										{			
											hcboVal = (HashMap) alOrderAtbDetails.get(i);
											strCodeID = (String) hcboVal.get("TRTYPE");
											strCodeNm = (String) hcboVal.get("ATTRVAL");
										
							%>
							<%if (strCodeNm.equals("N")){
									blPDFUploadFL = true ;
							 } %>
							<% }  %>
							<% }  %>
							<%if (blPDFUploadFL){%>
							<tr><td colspan="15" class="Line" height="1"></td></tr>
							 	<tr id="DivHoldFl"><td align="center" colspan="15" class="RightTableCaption">
									<font color="red"><fmtPrintPrice:message key="LBL_DO_PDF_STATUS"/></font></td>
								</tr>
						    <% }  %>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td colspan="2" class="Line" height="1"></td></tr>
    </table>
    <%if(strParantForm.equals("CASE") && (strDOType.equals("2518")|| strDOType.equals("2519"))){ %>
	<table border="0" width="900px" cellspacing="0" cellpadding="0" >
	<%if(!strSessDeptId.equals("S") && !strDOType.equals("2520")){ %>
		<tr>
			<td align="center" height="30" colspan="2">
			<fmtPrintPrice:message key="BTN_CONFIRM" var="varConfirm"/>
			<fmtPrintPrice:message key="BTN_VOID" var="varVoid"/>
			<fmtPrintPrice:message key="BTN_BACK" var="varBack"/>
				<gmjsp:button value="&nbsp;${varConfirm}&nbsp;" name="Btn_Confirm" buttonType="Save" gmClass="button" onClick="fnConfirmDO();" />&nbsp;
				<gmjsp:button value="&nbsp;${varVoid}&nbsp;" gmClass="button" buttonType="Save" onClick="fnVoidDO();" />
				<%-- <gmjsp:button value="&nbsp;Cancel&nbsp;"  name ="Btn_Cancel" buttonType"save" gmClass="button" onClick="fnCloseSummary(8);" >--%>
				<gmjsp:button value="&nbsp;${varBack}&nbsp;" gmClass="button" name="Btn_Back" onClick="fnBack();" buttonType="Load" />
			</td>
		</tr>
		<tr>
				<td colspan="4" align="center">
				<jsp:include page="/accounts/GmCreditHoldInclude.jsp" >
					<jsp:param name="ALERT" value="N"/>
					<jsp:param name="SUBTYPE" value="104963"/>
					</jsp:include>
				</td>
		</tr>
		<%}else{ %>
		<tr>
			<td align="center" height="30" colspan="2">
				<br><%if(strStatus.equals("Pending Release")){%>
				<input type="checkbox" name="Chk_Submit" onclick="fnCheckSubmit();" >&nbsp;<b><fmtPrintPrice:message key="LBL_CS_FOR_VERIFICATION"/></b><br><br>
				<fmtPrintPrice:message key="BTN_SUBMIT" var="varSubmit"/>
				<fmtPrintPrice:message key="BTN_VOID" var="varVoid"/>
				<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" name="Sales_Submit" onClick="fnConfirmDO();" disabled="true" buttonType="Save" />&nbsp;
				 <gmjsp:button value="&nbsp;${varVoid}&nbsp;"  gmClass="button" buttonType="Save" onClick="fnVoidDO();" />
				<%} %>
			   <%if(!strhidePartPrice.equals("N")){ %>
			   <fmtPrintPrice:message key="BTN_HIDE_PRICE" var="varHidePrice"/>
				<gmjsp:button value="&nbsp;${varHidePrice}&nbsp;" buttonType="Load" gmClass="button" onClick="fnHidePrice();"/>
				<%}else{ %>
				<fmtPrintPrice:message key="BTN_SHOW_PRICE" var="varShowPrice"/>
				 <gmjsp:button value="&nbsp;${varShowPrice}&nbsp;"  gmClass="button" buttonType="Load" onClick="fnShowPrice();" />
				<%} %>
				<%--  <gmjsp:button value="&nbsp;Cancel&nbsp;"  gmClass="button" buttonType="Save" name ="Btn_Cancel" onClick="fnCloseSummary(7);" > --%>
				<fmtPrintPrice:message key="BTN_BACK" var="varBack"/>
				 <gmjsp:button value="&nbsp;${varBack}Back&nbsp;" gmClass="button" name="Btn_Back" onClick="fnBack();" buttonType="Load" />
			</td>
		</tr>	
		<%}  } %>	
		
		
		</table>
		
	<BR><BR>
<%} %>
	<%if(!strSessDeptId.equals("S")){ 
		if(!strDOType.equals("2520")){%>
	<table style="margin:0 0 0 0;width:900px;border: 1px solid  #676767;" border="0" width="750" cellspacing="0" cellpadding="0" >
		<tr>
			<td width="1" rowspan="7" class="Line"></td>
			<td colspan="2" class="Line" height="1"></td>
			<td width="1" rowspan="7" class="Line"></td>
		</tr>
<%
		if(strParantForm.equals("CASE")&& !strDOType.equals("2518")){
%>			
		<tr>
			<td align="center" height="30" colspan="2">
			<%if(strShowBOD.equals("YES") && !strOpenRTfl.equals("Y") && !strHoldFl.equals("Y")) {
			%>
			<fmtPrintPrice:message key="BTN_BILL_OF_DELIVERY" var="varBillofDelivery"/>
				<gmjsp:button value="&nbsp;${varBillofDelivery}&nbsp;" gmClass="button" buttonType="Save" onClick="fnPrintBODRecipt();" />
				<%}	%>
				<fmtPrintPrice:message key="BTN_BACK" var="varBack"/>
				<gmjsp:button value="<%=strButtonLbl%>" name="Btn_PicSlip"  gmClass="button" onClick="fnPicSlip();" buttonType="Load" />
				<gmjsp:button value="&nbsp;${varBack}&nbsp;"  name="Btn_Back" onClick="fnBack();" gmClass="button" buttonType="Load" />
		</td>
		</tr>
		<tr><td colspan="3" class="Line" height="1"></td></tr>		
<%		}
		}
		if (strAction.equals("OrdPlaced"))
		{
%>
		<tr>
			<td colspan="2">
				<jsp:include page="/common/GmIncludeLog.jsp" >
				<jsp:param name="LogType" value="" />
				<jsp:param name="LogWidth" value="900" />
				<jsp:param name="LogMode" value="View" />
				</jsp:include>
			</td>
		</tr>
		<tr>
			<td align="center" height="30" colspan="2">
			<%if(strShowBOD.equals("YES") && !strOpenRTfl.equals("Y") && !strHoldFl.equals("Y")) {%>
			<fmtPrintPrice:message key="BTN_BILL_OF_DELIVERY" var="varBillofDelivery"/>
				<gmjsp:button value="&nbsp;${varBillofDelivery}&nbsp;" gmClass="button" buttonType="Save" onClick="fnPrintBODRecipt();" />
				<%			
					}
					%>
							<gmjsp:button value="<%=strButtonLbl%>" name="Btn_PicSlip" disabled="<%=strDisablePrint %>" gmClass="button" onClick="fnPicSlip();" buttonType="Load" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<%if(strShowProformaInvoice.equals("YES") && !strOpenRTfl.equals("Y") && !strHoldFl.equals("Y")) {%>
							<fmtPrintPrice:message key="BTN_PROFOMA_INVOICE" var="varProfomaInvoice"/>
							<gmjsp:button value="&nbsp;${varProfomaInvoice}&nbsp;"  buttonType="Save" gmClass="button" onClick="fnProformaInv();" />
							<%			
								}
								%>
<%
						if (blHold && strDOType.equals("2520"))//Only for New Quote Screen 
						{
%>
							<fmtPrintPrice:message key="BTN_MOVE_TO_HOLD" var="varMoveToHold"/>
							<gmjsp:button value="${varMoveToHold}" name="Btn_Hold" gmClass="ButtonReset" onClick="fnMoveToHold();"  buttonType="Save" />
<%
						}
%>
			</td>
		</tr>
		<tr><td colspan="3" class="Line" height="1"></td></tr>
<%			
		}else if(!strParantForm.equals("CASE") && !strParantForm.equals("SALESDASHBOARD")){
%>
<tr>
			<td align="center" height="30" colspan="2" id="buttonTd">
			
			<%if(strShowBOD.equals("YES") && !strOpenRTfl.equals("Y") && !strHoldFl.equals("Y")) {%>
			<fmtPrintPrice:message key="BTN_BILL_OF_DELIVERY" var="varBillofDelivery"/>
				<gmjsp:button value="&nbsp;${varBillofDelivery}&nbsp;" gmClass="button" buttonType="Save" onClick="fnPrintBODRecipt();" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<%			
					}
					%>
				<fmtPrintPrice:message key="BTN_PRINT" var="varPrint"/>
				<fmtPrintPrice:message key="BTN_CLOSE" var="varClose"/>
				<gmjsp:button value="&nbsp;${varPrint}&nbsp;"  gmClass="button" buttonType="Load" onClick="fnPrint();" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<gmjsp:button value="&nbsp;${varClose}&nbsp;"  gmClass="button" buttonType="Load" onClick="fnClose();" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<%if(strShowProformaInvoice.equals("YES") && !strOpenRTfl.equals("Y") && !strHoldFl.equals("Y")) {%>
				<fmtPrintPrice:message key="BTN_PROFOMA_INVOICE" var="varProfomaInvoice"/>
				<gmjsp:button value="&nbsp;${varProfomaInvoice}&nbsp;" gmClass="button" buttonType="Save" onClick="fnProformaInv();" />
				<%			
					}
					%>
			</td>
		</tr>		
<%			
		}
		%>
		<tr >
		<td colspan="2">
		
		<% 
	}
		if((strParantForm.equals("CASE") || strParantForm.equals("SALESDASHBOARD") || !strCaseID.equals("")) && (!blDOFromApp))
		{
%>		
		<table border="0" width="900" cellspacing="0" cellpadding="0" >
		<tr >
			<td colspan="3" height="20"><a href="javascript:fnEmailDO();"><fmtPrintPrice:message key="LBL_CLICK_HERE_FOR_EMAIL"/></a></td>
		</tr>
		<tr>
			<td  colspan="3" height="30"> <a href="#" onClick="fnPrintDO();"><fmtPrintPrice:message key="LBL_CLICK_PDF_VERSION"/></a></td>
		</tr>
		<% if (strClientSysType.equals("IPAD") && (strFwdFlag.equals("CaseAction")||strFwdFlag.equals("DOAction"))){%>
		<tr align="center">
			<td colspan="3" height="25">
				<fmtPrintPrice:message key="BTN_BACK" var="varBack"/>
				<gmjsp:button value="&nbsp;${varBack}&nbsp;"  name="Btn_Back" gmClass="button" onClick="fnBack();" buttonType="Load" />
			</td>
		</tr>
		<%} %>
		</table>
		<% } %>
		<%if(!strSessDeptId.equals("S") && !strDOType.equals("2520")){ %>
		</td>
		</tr>
		<tr><td colspan="3" class="Line" height="1"></td></tr>
		<tr>
			<td>
				<table border="0" width="900" cellspacing="0" cellpadding="0">
					<tr>
			<td valign="top" width="30%">
			<b><fmtPrintPrice:message key="LBL_LEGEND_ITEM_TYPE"/>:</b><BR>
			&nbsp;&nbsp;<fmtPrintPrice:message key="LBL_PART_FROM_CONSIGNMENT"/><BR>
			&nbsp;&nbsp;<fmtPrintPrice:message key="PART_FROM_LOANER_KIT"/><BR>
			&nbsp;&nbsp;<fmtPrintPrice:message key="PART_FROM_HOSPITAL_KIT"/><BR>
			</td>
			<td valign="top" width="30%">
			<b><fmtPrintPrice:message key="LBL_LEGEND_ADJUSTMENT_CODE_TYPE"/>:</b><BR>
			&nbsp;&nbsp;<fmtPrintPrice:message key="LBL_WASTED"/> <BR>
			&nbsp;&nbsp;R &nbsp;- <fmtPrintPrice:message key="LBL_REVISION"/> <BR>
			</td>				
			</tr>
			</table>
			</td>
			</tr>
	</table>
		<%} 
	%>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
