

<!-- it\GmCodeGroupReport.jsp -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
 <%
 String strItJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_IT");
 %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<TITLE>Globus Medical: Cancel Report</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css"/>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script
	language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
 <script
	language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script
	language="JavaScript" src="<%=strItJsPath%>/GmCodeGroup.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>	
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css"/>
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("CODE_GROUP_REPORT");

%>
<bean:define id="gridData" name="frmCodeGroup" property="gridXmlData"
	type="java.lang.String">
</bean:define> <script>
var gridObjData='<%=gridData%>';
var mygrid;
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form method="post" action="/gmCodeGroup">
	<html:hidden property="strOpt" name="frmCodeGroup" />
	<html:hidden name="frmCodeGroup" property="codeGrpID" />
	<table border="0" class="GtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td  colspan="3" height="30" class="RightDashBoardHeader">&nbsp;Code
			Group Report</td>
			<td class=RightDashBoardHeader align="right"><img id='imgEdit'
				style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help'
				width='16' height='16'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr >
			<td height="30" align="center" colspan="2"><b>Group Name: </b>
			&nbsp;<html:text property="codeGrp"
				name="frmCodeGroup"
				onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
				onblur="changeBgColor(this,'#ffffff');" onkeypress="fnEnter(event);"/>
			<gmjsp:button
				name="btn_code_grp" value="&nbsp;&nbsp;Load&nbsp;&nbsp;"
				onClick="fnLoad();" gmClass="Button" buttonType="Load"/></td>
		</tr>
		<tr>
				<td colspan="4">
					<div id="dataGridDiv" height="500px" width="850px"></div>
				</td>
		</tr>
		<tr>
		<td colspan="2">&nbsp;</td>
		</tr>
		<logic:notEqual name="frmCodeGroup" property="strOpt" value="report">	
			<tr>
				<td align="center" colspan="2">
				<div class='exportlinks'>Export Options : <img
					src='img/ico_file_excel.png' />&nbsp; <a href="#"
					onclick="fnExport();"> Excel </a>
				</div>
				</td>
			</tr>
		</logic:notEqual>
		<tr>
		<td colspan="2">&nbsp;</td>
		</tr>		
	</table>
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</body>
</HTML>

