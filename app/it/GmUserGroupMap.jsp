
 <%
/**********************************************************************************
 * File		 		: GmUserGroupMap.jsp
 * Version	 		: 1.0
 * author			: Arockia Prasath
************************************************************************************/
%>

<!-- it\GmUserGroupMap.jsp -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="gridData" name="frmUserGroupMap" property="xmlData" type="java.lang.String"> </bean:define>
<bean:define id="funcID" name="frmUserGroupMap" property="funcID" type="java.lang.String"> </bean:define>
<bean:define id="funType" name="frmUserGroupMap" property="funType" type="java.lang.String"> </bean:define>
<bean:define id="grpType" name="frmUserGroupMap" property="grpType" type="java.lang.String"> </bean:define>
<%
String strItJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_IT");
String strHeader="";
String strLabel="";
if(grpType.equals("TSMD")){
	strHeader="Task - Module Mapping";
	strLabel = "Task";
}else if(grpType.equals("SESG")){
	strHeader="Security Event - Security Group Mapping";
	strLabel = "Security Event";
}else if(grpType.equals("92262")){
	strHeader="Task -Security Group Mapping Report";
	strLabel = "Security Group";	
}
else{
	strHeader= "Task - Security Group Mapping";
	strLabel = "Task";
}

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Function User / Group Screen </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />

<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strItJsPath%>/GmUserGroupMap.js"></script>	

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid_form.js "></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<!-- PMT-26730 system Manager Changes -->
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_mcol.js"></script>
<script>
	var objGridData;
	objGridData = '<%=gridData%>';
	var gridObj='';
	var grpType='<%=grpType%>';
</script>

<BODY leftmargin="20" topmargin="10" onLoad="return fnOnPageLoad(objGridData);">
<html:form action="/gmUserGroupMap.do?method=fchFunctDetails"  >
<html:hidden property="strOpt" value=""/>
<html:hidden property="funcID" value="<%=funcID%>"/>
<html:hidden property="funType" value="<%=funType%>"/>
<html:hidden property="grpType" value="<%=grpType%>"/>
<html:hidden property="hinputStr" value = ""/>
<html:hidden property="rowId" value = ""/>
<html:hidden property="hTaskNameStr" value = ""/>

	<table border="0"  class="border" width="900" cellspacing="0" cellpadding="0">
		
		<tr>
				<td height="25" width="40%" class="RightDashBoardHeader"> <%=strHeader %> </td>
				<td align="right" colspan="2" class=RightDashBoardHeader><img id='imgEdit'
				align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
				title='Help'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("GROUP_USER_MAP")%>');" />
			</td>
		</tr>
		
		<tr>
			<td height="25" width="50%" class="RightTableCaption" align="right"><%=strLabel%> Name:</td>
			<td width="50%">&nbsp;<bean:write name="frmUserGroupMap" property="funcName"/>
			</td>
		</tr>
		<tr>
			<td height="25" class="RightTableCaption" align="right"><%=strLabel%> Description:</td>
			<td>&nbsp;<bean:write name="frmUserGroupMap" property="funcDesc"/>
			</td>
		</tr>
		
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>	
		  <%if(!gridData.equals("")){%>
				 
					<tr>
						<td colspan="6">
							<div  id ="usergroupdata" width="900px" height="400px"></div>
						</td>
					</tr>
					<tr height="25">
			                <td colspan="6" align="center">
			      <div class='exportlinks'>Export Options : <img
				src='img/ico_file_excel.png' />&nbsp;<a href="#"
				onclick="fnExport();"> Excel
				</a>
				</div>
			                </td>
					</tr>
					<%} 
					
					if(!grpType.equals("92262") && !grpType.equals("92260")) {
					%>
		
		
		<tr >
			<td colspan="4" align="center"> <gmjsp:button value="Submit" gmClass="Button" buttonType="Save" onClick="return fnSubmit();" /> 
			 <%--<gmjsp:button value="Add People / Group" gmClass="Button" buttonType="Save" onClick="return fnAddGroup();" />--%>
			 
			 <%
			 if(funType.equals("92262")){  
			 		if(grpType.equals("TSMD")){%>
			 	<gmjsp:button name="add_module" controlId="add_module" value="Add Module" gmClass="Button" buttonType="Save" onClick="return fnAddModule();" />
			 	<% }else{%>
			 	<gmjsp:button name="add_sec" controlId="add_sec" value="Add Security Group" gmClass="Button" buttonType="Save" onClick="return fnAddSecGroup();" />
			<%
			 	}
			}else{ %>
				<gmjsp:button name="add_sec" controlId="add_sec" value="Add Security Group" gmClass="Button" buttonType="Save" onClick="return fnAddSecGroup();" />
			<%} %>
			<%
			if(grpType.equals("TSMD")){			
			%>			
			<gmjsp:button name="task_sec" controlId="task_sec" value="Task to Security Group Mapping" gmClass="Button" buttonType="Load" onClick="return fnToggle('TSSG');" />
			<%}else if(grpType.equals("TSSG")){ %>
				<gmjsp:button name="task_mod" controlId="task_mod" value="Task to Module Mapping" gmClass="Button" buttonType="Load" onClick="return fnToggle('TSMD');" />				
			<%} %>
			 </td>
		</tr>
		<%} %>
	</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>