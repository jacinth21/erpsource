<%
/**********************************************************************************
 * File		 		: GmAssetAtrribute.jsp
 * Desc		 		: This screen is used for the asset attribute Report
 * Version	 		: 1.0
 * author			: Gopinathan Saravanan
************************************************************************************/
%>

<!-- it\GmAssetAttributeReport.jsp -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ include file="/common/GmHeader.inc" %> 

<% 
String strItJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_IT");
String strWikiTitle = GmCommonClass.getWikiTitle("ASSET_ATTRIBUTE");
%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<bean:define id="gridData" name="frmAssetAtt" property="gridXmlData" type="java.lang.String"> </bean:define>
<HTML>
<HEAD>
<TITLE> Globus Medical: Asset Attribute Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
 
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strItJsPath%>/GmAssetAttribute.js"></script>

<style type="text/css" media="all">
     @import url("styles/screen.css");
</style>

<script>
	var objGridData;
	objGridData = '<%=gridData%>';
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoad()" onkeyup="fnEnter();">

<html:form action="/gmAssetAttribute.do"> 
<html:hidden property="strOpt" />
<html:hidden name="frmAssetAtt" property="attId" />
	<table border="0" class="DtTable680" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2" height="25" class="RightDashBoardHeader">&nbsp;Asset Attribute Report </td>
			<td colspan="4" align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
		</tr>
		<tr><td colspan="5" height="1" class="line"></td></tr> 
		<tr>	
			<td class="RightTableCaption" align="right" HEIGHT="24">Asset ID&nbsp;:</td>
			<td>&nbsp;<html:text name="frmAssetAtt" property="assetID" size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" /></td>
			<td colspan="2" class="RightTableCaption" align="right" HEIGHT="24">Attribute Type&nbsp;:</td>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<td>&nbsp; <gmjsp:dropdown	controlName="attTyp" SFFormName="frmAssetAtt" SFSeletedValue="attTyp" SFValue="alAssTyp" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" />&nbsp;&nbsp;</td>
			
		</tr>	    	 
		<tr><td colspan="5" height="1" class="LLine"></td></tr> 
			<tr class="Shade">	
			<td class="RightTableCaption" align="right" HEIGHT="24">Attribute value&nbsp;:</td>
			<td align="left">&nbsp;<html:text name="frmAssetAtt" property="attributeValue" size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" /></td>
			<td colspan="2" class="RightTableCaption" align="right" HEIGHT="24">Active Flag&nbsp;:</td>
			<td align="left">&nbsp;<html:checkbox name="frmAssetAtt" property="activeflag"></html:checkbox> &nbsp;&nbsp;&nbsp;&nbsp;<gmjsp:button value="Load" gmClass="Button" buttonType="Load" onClick="fnReload();" /> </td>
		</tr>
       	 	<tr>
           		<td colspan="5" height="50" >
					<div id="astAttData" style="grid" height="200px" width="680px"></div>
		    	</td>	
			</tr>	
   		</table>		     	
   		<%@ include file="/common/GmFooter.inc" %> 
 </BODY>
</html:form>
</HTML>

