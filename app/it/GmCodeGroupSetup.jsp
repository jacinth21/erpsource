<%
/**********************************************************************************
 * File		 		: GmCodeGroupSetup.jsp
 * Desc		 		: This screen is used for Zone and VP mapping 
 * Version	 		: 1.0
 * author			: Gopinathan Saravanan
************************************************************************************/
%>


<!-- it\GmCodeGroupSetup.jsp -->

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
 <%
 String strItJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_IT");
 %>
<HTML>
<HEAD>
<TITLE>Globus Medical: Code Group Setup</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strItJsPath%>/GmCodeGroup.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("CODE_GROUP_SETUP");
%>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnDisable();">
<form name="frmCodeGroup" method="post" action="/gmCodeGroup.do">
<html:hidden name="frmCodeGroup" property="strOpt" /> 
<html:hidden name="frmCodeGroup" property="codeGrpID" />
<html:hidden name="frmCodeGroup" property="strChk" /> 
<table border="0" class="GtTable680" cellspacing="0" cellpadding="0">
	<tr>
		<td height="25" class="RightDashBoardHeader">&nbsp;Code Group Setup</td>
		<td align="right" class=RightDashBoardHeader height="25" align="right">
		<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16'	onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		</td>
	</tr>
	<tr>
		<td class="RightTableCaption" align="right" HEIGHT="30"><font color="red">*</font><b>Code Group:</b>&nbsp;</td>
		<td width="60%" align="left">&nbsp;<html:text name="frmCodeGroup" property="codeGrp"  size="30" maxlength="6" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="1"/>&nbsp;&nbsp;
		<logic:notEqual name="frmCodeGroup" property="strOpt" value="edit">
		<gmjsp:button value="&nbsp;Check&nbsp;" gmClass="button" buttonType="Load" onClick="fnCheck();" tabindex="2"/>
		</logic:notEqual>
		<logic:equal name="frmCodeGroup" property="strChk" value="yes">
			<font color="red"> Not Available </font>
		</logic:equal>
		<logic:equal name="frmCodeGroup" property="strChk" value="no">
			<font color="green"> Available </font>
		</logic:equal>
		</td>
	</tr>
		
	<logic:equal name="frmCodeGroup" property="strOpt" value="edit">
	<tr class="Line"></tr>
	<tr class="shade">
		<td class="RightTableCaption" align="right" HEIGHT="30"><font color="red">*</font><b>Code Description:</b>&nbsp;</td>
		<td width="60%" align="left">&nbsp;<html:text name="frmCodeGroup" property="codeDes"  size="30" maxlength="50" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" /></td>
	</tr>
	<tr>
		<td class="RightTableCaption" align="right" HEIGHT="30"><font color="red">*</font><b>No. of Lookup's Needed:</b>&nbsp;</td>
		<td width="60%" align="left">&nbsp;<html:text property="lookupNeeded"  size="5" maxlength="2" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" value="10" /></td>
	</tr>
	<tr class="shade">
		<td colspan="2" align="center" height="30">
				<gmjsp:button value="&nbsp;Submit&nbsp;" gmClass="button" buttonType="Save" onClick="fnSubmit();"/>
				<gmjsp:button value="&nbsp;Reset&nbsp;" gmClass="button" buttonType="Save" onClick="fnReset();"/>
			<logic:notEqual name="frmCodeGroup" value="" property="codeGrpID">
				<gmjsp:button value="&nbsp;Lookup&nbsp;" gmClass="button" buttonType="Save" onClick="javascript:fnEditLookup(document.frmCodeGroup.codeGrp.value.toUpperCase());"/>
				<gmjsp:button value="&nbsp;Void&nbsp;" gmClass="button" buttonType="Save" onClick="fnVoid(document.frmCodeGroup.codeGrpID.value);"/>
			</logic:notEqual>
		</td>
	</tr>
	</logic:equal>
</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
