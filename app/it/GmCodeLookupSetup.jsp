<%
/**********************************************************************************
 * File		 		: GmCodeLookupSetup.jsp
 * Desc		 		: Setup the CodeLookup values
 * Version	 		: 1.0
 * author			: Gopinathan
************************************************************************************/
%>

<!-- it\GmCodeLookupSetup.jsp -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtCodeLkpSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- it\GmCodeLookupSetup.jsp -->
<fmtCodeLkpSetup:setLocale value="<%=strLocale%>"/>
<fmtCodeLkpSetup:setBundle basename="properties.labels.it.GmCodeLookup"/>

<bean:define id="gridData" name="frmCodeLookupSetup" property="gridXmlData" type="java.lang.String"> </bean:define>
<%
String strItJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_IT");
HashMap hmCodeGrp = new HashMap();
String strCodeGroup="";
String strCodeDesc="";
hmCodeGrp = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HMCODEGRPDETAIL"));
 strCodeGroup = GmCommonClass.parseNull((String)hmCodeGrp.get("CODEGROUP"));
 strCodeDesc=GmCommonClass.parseNull((String)hmCodeGrp.get("CODEDESC"));

%>
<html>
<head>
<title>Code Name Change</title>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strItJsPath%>/GmCodeLookupSetup.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>


<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
	<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<%
String strUserId = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));

%>
<script>
	var objGridData;
	objGridData = '<%=gridData%>';
	var strUserId = '<%=strUserId%>';
	var strCodeGrp = '<%=strCodeGroup%>';
	var strCodeDesc = '<%=strCodeDesc%>';
		
</script>
</head>
<body leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoad()">
<html:form action="/gmCodeLookupSetup">
<html:hidden name="frmCodeLookupSetup" property="inputString"/>
<html:hidden name="frmCodeLookupSetup" property="strOpt"/>
<html:hidden name="frmCodeLookupSetup" property="codeGrp"/>
<logic:notEqual name="frmCodeLookupSetup" property="strOpt" value="void">
<table border="0" class="GtTable680" cellspacing="0" cellpadding="0">
	<tr>
		<td  colspan="1" height="25" class="RightDashBoardHeader">&nbsp;<fmtCodeLkpSetup:message key="LBL_CODE_LKP_VALUES"/></td>
		<fmtCodeLkpSetup:message key="IMG_ALT_HELP" var = "varHelp"/>
		<td align="right" class=RightDashBoardHeader height="25" align="right">
		<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16'	onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("CODE_LOOKUP_SETUP")%>');" />
		</td>
	</tr>
</logic:notEqual>
<logic:equal name="frmCodeLookupSetup" property="strOpt" value="void">
<table border="0"  class="GtTable400" cellspacing="0" cellpadding="0">
<tr>
		<td height="25" class="RightDashBoardHeader">&nbsp;<fmtCodeLkpSetup:message key="LBL_EDIT_REASONS"/></td>
		<fmtCodeLkpSetup:message key="IMG_ALT_HELP" var = "varHelp"/>
		<td align="right" class=RightDashBoardHeader height="25" align="right">
		<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16'	onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("EDIT_REASONS")%>');" />
		</td>
	</tr>
</logic:equal>
	<tr>
				<td class="RightTableCaption" align="left" colspan="2" height="25">
				<div id="tool_data" style="display:inline" >
					<table cellpadding="0" cellspacing="0" border="0" width="200" >
						<TR>
							<fmtCodeLkpSetup:message key="IMG_ALT_ADD_ROW" var = "varAddRow"/>
							<fmtCodeLkpSetup:message key="IMG_ALT_RMV_ROW" var = "varRmvRow"/>
							<td>
							&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="${varAddRow}" style="border: none;" onClick="javascript:addRow()"height="14"></a>
							&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="${varRmvRow}" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a>
							</td>										
						</TR>
					</table>
				</div>
				</td>				
	</tr>
	<tr>
           <logic:notEqual name="frmCodeLookupSetup" property="strOpt" value="void">
           <td colspan="2">
				<div id="mygrid_container" style="grid" height="450px" width="680px"></div>
		    </td>
           </logic:notEqual>
           <logic:equal name="frmCodeLookupSetup" property="strOpt" value="void">
           	<td colspan="2">
				<div id="mygrid_container" style="grid" height="180px" width="400px"></div>
		    </td>
		   </logic:equal>
	</tr>	
	<tr >
		<td colspan="2" align="center" height="30">
		<fmtCodeLkpSetup:message key="BTN_SUBMIT" var = "varSubmit"/>
		<fmtCodeLkpSetup:message key="BTN_CLOSE" var = "varClose"/>
		<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="Button" buttonType="Save" onClick="fnSubmit();"/>
		<logic:equal name="frmCodeLookupSetup" property="strOpt" value="void">
			&nbsp;&nbsp;&nbsp; <gmjsp:button value="&nbsp;${varClose}&nbsp;" gmClass="Button" buttonType="Load" onClick="javascript:window.close();"/>
		</logic:equal>
		</td>
		
	</tr>
	<logic:notEqual name="frmCodeLookupSetup" property="strOpt" value="void">	
	<tr>
		<td align="center" height="30">
		<input type=radio value="all" name="scriptOpt" checked="checked"><fmtCodeLkpSetup:message key="LBL_ALL_ROWS"/></input>&nbsp;
		<input type=radio value="selected" name="scriptOpt"><fmtCodeLkpSetup:message key="LBL_SELECTED_ROW"/></input>&nbsp;
		<fmtCodeLkpSetup:message key="BTN_SCRIPT" var = "varScript"/>
		<gmjsp:button value="&nbsp;${varScript}&nbsp;" gmClass="Button" buttonType="Save" onClick="fnGScript();"/>
		</td>
	</tr>
	</logic:notEqual>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>