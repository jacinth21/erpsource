

<!-- it\GmFunctionListReportR.jsp -->


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="strXmlData" name="frmUserGroupMap" property="xmlData" type="java.lang.String"></bean:define>
<bean:define id="strOpt" name="frmUserGroupMap" property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="haction" name="frmUserGroupMap" property="haction" type="java.lang.String"></bean:define>
<%
String strItJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_IT");
String strHeader="";
if(strOpt.equals("TSMD")){
	strHeader="Task - Module Mapping";
}else if(strOpt.equals("SESG")){
	strHeader="Security Event - Security Group Mapping";
}else{
	strHeader= "Task - Security Group Mapping";	
}

if(haction.equals("SECGRP_SECEVN")){
	strHeader="Security Group - Security Event Mapping";
}
%>
<head>
<title>Control Number</title>
	<style type="text/css" media="all">
@import url("styles/screen.css");
</style>
<link rel="STYLESHEET" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script><!-- PMT-36862 js file  -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strItJsPath%>/GmFunctionListReport.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script type="text/javascript">
var objGridData;
var gridObj;
var strOpt = '<%=strOpt%>';
objGridData ='<%=strXmlData%>';
</script>

</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form action="/gmUserGroupMap.do" method="post" styleId="form">
	<html:hidden property="strOpt" name="frmUserGroupMap" />
	<html:hidden property="hVoidStr" name="frmUserGroupMap" />
	<html:hidden property="hTaskNameStr" name="frmUserGroupMap" />
	<html:hidden property="hTaskDescStr" name="frmUserGroupMap" />
	<html:hidden property="hTaskTypeStr" name="frmUserGroupMap" />
	<html:hidden property="haction" name="frmUserGroupMap" />

<%if(strOpt.equals("SESG")){%>
    <table cellspacing="0" cellpadding="0" class="DtTable950">
<%} else {%>
	<table cellspacing="0" cellpadding="0" class="DtTable1100">
<%}%>
		<tr>
			<td height="25" class="RightDashBoardHeader"><%=strHeader%></td>
			<td align="right" class="RightDashBoardHeader"><img id='imgEdit'
				style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help'
				width='16' height='16'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%//=strWikiTitle%>');" /></td>
		</tr>
	</table>
<%if(strOpt.equals("SESG")){%>
    <table cellspacing="0" cellpadding="0" class="DtTable950">
<%} else {%>
	<table cellspacing="0" border="0" cellpadding="0" class="DtTable1100">
<%}%>	
		<tr>
			<td>
<%if(strOpt.equals("SESG")){%>
			    <div id="GridData" height="470px" width="948"></div>
<%} else {%>
			    <div id="GridData" height="470px" width="1098"></div>
<%}%>
			</td>
		</tr>
		<tr>
			<td>
			<div><span id="pagingArea"></span>&nbsp;
				 <span id="infoArea"></span>
		    </div>
		    </td>
		</tr>
		<tr>
			<td align="center" width="1000px">
			<div class='exportlinks' align="center">Export Option : <img
				src='img/ico_file_excel.png' />&nbsp;<a href="#"
				onclick="fnDownloadXLS();"> Excel
			</a><!--| <img src='images/pdf_icon.gif' />&nbsp;<a href="#"
				onclick="gridObj.toExcel('/phpapp/pdf/generate.php');">PDF </a>
			--></div>
			</td>
		</tr>
		<tr>
		<%if(!strOpt.equals("TSSG") && !haction.equals("SECGRP_SECEVN")){%>
		<td align="center" width="1000px" height="25">
			<gmjsp:button value="&nbsp;Submit&nbsp;"  name="Btn_Submit" gmClass="Button" buttonType="Save" onClick="fnSubmit();"/>&nbsp;&nbsp;
			<br>
		</td>
		<%}%>
		</tr>
	</table>
</html:form>
</body>
<%@include file="/common/GmFooter.inc"%>