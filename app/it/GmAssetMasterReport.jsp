
<%
	/**********************************************************************************
	 * File		 		: GmAssetMaster.jsp
	 * Desc		 		: This screen is used for the 
	 * Version	 		: 1.0
	 * author			: Velu
	 ************************************************************************************/
%>

<!-- it\GmAssetMasterReport.jsp -->

<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>
<%@ page import="com.globus.common.beans.GmCommonClass"%>
<%@ page import="com.globus.common.beans.GmLogger"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ include file="/common/GmHeader.inc"%>


 <%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
 <bean:define id="gridData" name="frmAssetMaster" property="gridXmlData" type="java.lang.String"> </bean:define>
  <%
 String strItJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_IT");
 %>
<HTML>
<HEAD>
<TITLE>Globus Medical: Order Action</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">


<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strItJsPath%>/GmAssetMaster.js"></script>
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("ASSET_MASTER");
%>

<style type="text/css" media="all">
@import url("styles/screen.css");
</style>
<script>
	var objGridData;
	objGridData = '<%=gridData%>';
</script>
</HEAD>


<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnOnReportLoad();" onkeyup="fnEnter();">
<html:form action="/gmAssetMasterAction.do">

	<html:hidden property="strOpt" />
	
	<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">

		<tr>
			<td colspan="2" height="25" width="125" class="RightDashBoardHeader">Asset Report</td>
			<td colspan="3" align="right" class=RightDashBoardHeader><img id='imgEdit'	style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help'
				width='16' height='16'	onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
			
		</tr>

				<tr>
					<td colspan="4" height="1" class="line"></td>
				</tr>
				<tr>
					<td  class="RightTableCaption" align="right" HEIGHT="24">Asset ID:</td>
					<td>&nbsp;<html:text size="30" name="frmAssetMaster" property="assetID" styleClass="InputArea"	onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
					<td  class="RightTableCaption" align="right" HEIGHT="24">Asset Name:</td>
					<td>&nbsp;<html:text size="30" name="frmAssetMaster"	property="assetName" styleClass="InputArea"	onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
 				</tr>
				<tr><td colspan="4" height="1" class="LLine"></td></tr>
				<tr class="Shade">
					<td  class="RightTableCaption" align="right" HEIGHT="24">Asset Desc:</td>
					<td>&nbsp;<html:text size="30" name="frmAssetMaster" property="assetDesc" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
					<td class="RightTableCaption" align="right" HEIGHT="24">Active Flag&nbsp;:</td>
					<td align="left">&nbsp;<html:checkbox name="frmAssetMaster" property="activeFlag"></html:checkbox> &nbsp;&nbsp;&nbsp;&nbsp;<gmjsp:button value="Load" gmClass="button" buttonType="Load" onClick="fnReload();" /> </td>
				</tr>
       	 		<tr>
           			<td colspan="4" height="30" >
						<div id="employeedata" style="grid" width="800px" height="200px" ></div>
		    		</td>	
			</tr>	
			</table>
			
<%@ include file="/common/GmFooter.inc" %> 
</html:form>
</BODY>

</HTML>
