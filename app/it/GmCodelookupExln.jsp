

<!-- it\GmCodelookupExln.jsp -->

<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %> 
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmLogger"%>
 <%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtCodelookupExln" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- it\GmCodelookupExln.jsp -->

<fmtCodelookupExln:setLocale value="<%=strLocale%>"/>
<fmtCodelookupExln:setBundle basename="properties.labels.it.GmCodelookupExcln"/>

<bean:define id="alcompany" name="frmCodeGroup" property="alcompany" type="java.util.ArrayList"> </bean:define> 
<bean:define id="strOpt" name="frmCodeGroup" property="strOpt" type="java.lang.String"> </bean:define>
<%
	String strItJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_IT");
	String strWikiTitle = GmCommonClass.getWikiTitle("CODE_LOOKUP_EXCLUSION");
    String strApplDateFmt = strGCompDateFmt;
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Code Lookup Exclusion</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations.js"></script>
<script language="JavaScript" src="<%=strItJsPath%>/GmCodelookupExcln.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<script type="text/javascript">
var format = '<%=strApplDateFmt%>';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10">
<html:form action="/gmCodeLkpExcln.do?method=saveCodeLookupExclnValues" >
<html:hidden property="hsetstatusNotcheck" />
<html:hidden property="companyStr" />
	<table border="0" width="650" class="DtTable650" cellspacing="0"
		cellpadding="0" style="border-width: 1px ; border-style: solid;border-color: #000000">

		<tr>
			<td height=25 class=RightDashBoardHeader colspan=5><fmtCodelookupExln:message key="LBL_CODE_LOOKUP_EXCLUSION"/></td>
			
			<td align="right" class=RightDashBoardHeader><fmtCodelookupExln:message key="IMG_ALT_HELP" var = "varHelp"/>
			<img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}'
			 onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>

	 <tr>
			<td class="RightTableCaption" align="right" HEIGHT="80" style="width:40%"><font color="red">*</font><b><fmtCodelookupExln:message key="LBL_CODE_ID"/></b>&nbsp;</td>
			<td>
			  <html:text
					property="codeid" styleId="codeid" name="frmCodeGroup" 
					onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
					onblur="changeBgColor(this,'#ffffff');" tabindex="1" size="38"/>
			  
			</td>	&nbsp;&nbsp;	
    </tr>
   
    <tr class="Shade"  Height="350">
            <td class="RightTableCaption" align="right" HEIGHT="30" ><font color="red">*</font><b><fmtCodelookupExln:message key="LBL_COMPANY"/></b>&nbsp;</td>            
    
    <td> <DIV style="display: visible; height: 300px; width:250px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 1px; margin-right: 2px;">
			<table >
				<tr>
					<td><html:checkbox property="selectAll"
						onclick="fnSelectAll('toggle');" tabindex="2"/><B><fmtCodelookupExln:message key="LBL_SELECT"/></B></td>
				</tr>
			</table>

			<table>
				  <logic:iterate id="selectedCompanylist"
					name="frmCodeGroup" property="alcompany">
					<tr>
						<td><htmlel:multibox property="checkSelectedCompany" value="${selectedCompanylist.COMPANYID}" tabindex="3"/>
							 <bean:write name="selectedCompanylist" property="COMPANYNM" /></td>
					</tr>
				</logic:iterate>
			</table>
			</DIV>  
			
    </td>
    <td colspan=4></td>
    </tr>
            
				       
    
   
   <tr>
            <td class="RightTableCaption" align="right" HEIGHT="30"><font color="red">*</font><b><fmtCodelookupExln:message key="LBL_TYPE"/></b>&nbsp;</td> 
            <td>
            <gmjsp:dropdown controlName="type"
						SFFormName="frmCodeGroup" SFSeletedValue="type"
						SFValue="altype" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" tabIndex="4" width="120"/>
             </td>  
   </tr>
   
   
   
	
	

    <TR><TD colspan=6 height=1 class=Line></TD></TR>   					
		<tr>
			<td  height="40" colspan="2" align="center">&nbsp;
			<logic:notEqual name="frmCodeGroup" property="strOpt" value="save">	
			  <fmtCodelookupExln:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="Button" tabindex="5" buttonType="Save" onClick="fnSubmit();"/>
		
			&nbsp;&nbsp;&nbsp;  <fmtCodelookupExln:message key="BTN_RESET" var="varReset"/>
			<gmjsp:button value="&nbsp;${varReset}&nbsp;" gmClass="Button" tabindex="6" buttonType="Save" onClick="fnReset();"/>
	</logic:notEqual>
		</td>
		</tr>

   			
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>