<%
/**********************************************************************************
 * File		 		: GmLDAPCRDSetup.jsp
 * Desc		 		: This screen is used to setup AD Configuration
 * Version	 		: 1.0
 * author			: Gopinathan
************************************************************************************/
%>

<!-- it\GmLDAPCRDSetup.jsp -->

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%
String strItJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_IT");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Active Directory Authentication</TITLE>

<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strItJsPath%>/GmLDAPCRDSetup.js"></script>

</HEAD>

<!-- Struts tag lib code modified for JBOSS migration changes -->

<BODY leftmargin="20" topmargin="10" onload="fnPageLoad('<bean:write name="frmLDAPCRD" property="message"/>');" >
<html:form action="/gmLDAPCRD.do">
<html:hidden property="groupID"/>
<html:hidden property="strOpt" value="" />
<table border="0" style="word-wrap: break-word;" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;Active Directory Configuration</td>
			<td align="right" class=RightDashBoardHeader>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("AD_CONFIGURATION")%>');" />
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr class="evenshade">
			<td class="RightTableCaption" align="right" HEIGHT="24" width="35%"><font color="red">*</font>&nbsp;LDAP:</td>
			<td >&nbsp;<gmjsp:dropdown controlName="ldapID" SFFormName="frmLDAPCRD" SFSeletedValue="ldapID" SFValue="alLdap" codeId="ID" codeName="NAME" onChange="fnReload(this);" tabIndex="1"></gmjsp:dropdown>
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr class="oddshade">
			<td class="RightTableCaption" align="right" HEIGHT="24" width="35%"><font color="red">*</font>&nbsp;Connection Name:</td>
			<td >&nbsp;<html:text maxlength="50" property="name"  size="40" styleClass="InputArea"
					 	onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="1"/>
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr class="evenshade">
			<td class="RightTableCaption" align="right" HEIGHT="24" width="35%"><font color="red">*</font>&nbsp;User Name:</td>
			<td align="left">&nbsp;<html:text maxlength="50" property="userName"  size="40" styleClass="InputArea"
					 	onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="2"/>
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr class="oddshade">
			<td class="RightTableCaption" align="right" HEIGHT="24" width="35%"><font color="red">*</font>&nbsp;Password:</td>
			<td align="left">&nbsp;<html:password maxlength="50" property="password"  size="40"  styleClass="InputArea"
					 	onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="3"/>
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr class="evenshade">
			<td class="RightTableCaption" align="right" HEIGHT="24" width="35%"><font color="red">*</font>&nbsp;Host Name:</td>
			<td align="left">&nbsp;<html:text maxlength="50" property="hostName"  size="40" styleClass="InputArea"
					 	onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="4"/>
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr class="oddshade">
			<td class="RightTableCaption" align="right" HEIGHT="24" width="35%"><font color="red">*</font>&nbsp;Port #:</td>
			<td align="left">&nbsp;<html:text maxlength="50" property="port"  size="40"  styleClass="InputArea"
					 	onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="5"/>
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr class="evenshade">
			<td class="RightTableCaption" align="right" HEIGHT="24" width="35%"><font color="red">*</font>&nbsp;User DN:</td>
			<td align="left">&nbsp;<html:text maxlength="50" property="userdn"  size="40"  styleClass="InputArea"
					 	onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="6"/>
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr class="oddshade">
			<td class="RightTableCaption" align="right" HEIGHT="24" width="35%"><font color="red">*</font>&nbsp;Base:</td>
			<td align="left">&nbsp;<html:text maxlength="50" property="base"  size="40"  styleClass="InputArea"
					 	onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="6"/>
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr>
			<td  height="40" colspan="2" align="center">&nbsp;
				<logic:equal name="frmLDAPCRD" property="message" value="success">	
					<gmjsp:button controlId="btnsave" style="width: 5em" value="Save" gmClass="button" buttonType="Save" onClick="fnSubmit('save');" />
				</logic:equal>
				<logic:notEqual name="frmLDAPCRD" property="message" value="success">	
				<gmjsp:button controlId="btntest" value="Test Connection" gmClass="button" buttonType="Save" onClick="fnSubmit('test');" />
				</logic:notEqual>
			</td>
		</tr>
		<logic:equal name="frmLDAPCRD" property="strOpt" value="test">
			<tr><td colspan="2" class="Line" height="1"></td></tr>
			<tr>
				<td colspan="2" class="ShadeRightTableCaption">&nbsp;Message :-		
				</td>
			</tr>
			<tr><td colspan="2" class="Line" height="1"></td></tr>
			<logic:equal name="frmLDAPCRD" property="message" value="success">				
				<tr>
					<td height="30" colspan="2" style="color:green;font-weight:bold;" align="center">&nbsp;Test connection successful	
					</td>
				</tr>
			</logic:equal>	
			<logic:notEqual name="frmLDAPCRD" property="message" value="success">				
				<tr>
					<td style="width: 700px;" width="700" colspan="2" style="color:red;"><bean:write name="frmLDAPCRD" property="message"/>	
					</td>
				</tr>
			</logic:notEqual>			
		</logic:equal>	
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
