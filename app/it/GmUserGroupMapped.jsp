<%
/**********************************************************************************
 * File		 		: GmUserGroupMapped.jsp
 * Desc		 		: This screen is used for the asset attribute Report
 * Version	 		: 1.0
 * author			: Rajkumar Jayakumar
************************************************************************************/
%>


<!-- it\GmUserGroupMapped.jsp -->

<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<% 
String strWikiTitle = GmCommonClass.getWikiTitle("USER_GROUP_MAPPING");
%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<bean:define id="gridData" name="frmUserGroupMap" property="xmlData" type="java.lang.String"> </bean:define>
<%
String strUserJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_USER");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Access Control Group Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
 
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strUserJsPath%>/GmUserGroup.js"></script>
<style>
table.DtTable700 {
	margin: 0 0 0 0;
	width: 700px;
	border: 1px solid  #676767;
}
</style>

<script>
	var objGridData;
	objGridData = '<%=gridData%>';
</script>

</HEAD>
<html:form action="/gmUserGroup.do"> 
<html:hidden name="frmUserGroup" property="strOpt" value="edit"/>
<html:hidden name="frmUserGroup" property="grpMappingID"/>
<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoad()">
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2" height="25" class="RightDashBoardHeader">&nbsp;User - Group Mapping Report </td>
			<td colspan="4" align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
		</tr>
		</table>
		<table width="700" cellpadding=0 cellspacing="0">
		<tr>
           	<td  height="500"  >
				<div id="grpData" style="grid" height="500px"></div>
		    </td>	
		</tr>	
	</table>	
</BODY>
</html:form>
</HTML>

