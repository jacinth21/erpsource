
<!-- it\GmCodelookupExclnReport.jsp -->

<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtCodelookupExclnReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!--it\GmCodelookupExclnReport.jsp  -->
<fmtCodelookupExclnReport:setLocale value="<%=strLocale%>"/>
<fmtCodelookupExclnReport:setBundle basename="properties.labels.it.GmCodelookupExclnReport"/>
<bean:define id="strXmlString" name="frmCodeGroup" property="strXmlString" type="java.lang.String"> </bean:define>
<bean:define id="codeid" name="frmCodeGroup" property="codeid" type="java.lang.String"> </bean:define>
<bean:define id="codenm" name="frmCodeGroup" property="codenm"	type="java.lang.String"></bean:define>
<% 
String strItJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_IT");
String strWikiTitle = GmCommonClass.getWikiTitle("ASSET_ATTRIBUTE");
%>
<HTML>
<HEAD>
<TITLE>Code Company Mapping - Report</TITLE>

<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strItJsPath%>/GmCodelookupExcln.js"></script>
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script>
var objGridData;
objGridData = '<%=strXmlString%>';
var gridObj ='';
var lblCodeID = '<fmtCodelookupExclnReport:message key="LBL_CODE_ID"/>';
</script>
</HEAD>

<body leftmargin="20" topmargin="10" onload="fnOnPageLoad();">

<html:form method="post" action="/gmCodeLkpExcln.do?method=fetchCodeLookupExclnDtls">
	<html:hidden property="strOpt" name="frmCodeGroup" />
	<html:hidden name="frmCodeGroup" property="codeGrpID" />
<table border="0" class="border" class="DtTable500" cellspacing="0" cellpadding="0">
	<tr>
		<td height="25" class="RightDashBoardHeader">&nbsp;<fmtCodelookupExclnReport:message key="LBL_CODE_COMPANY_MAPPING"/></td>
		<td align="right" class=RightDashBoardHeader><fmtCodelookupExclnReport:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
	</tr>
	<tr>
		<td colspan="2" height="1" class="line"></td>
	</tr>
	
	<tr class="Shade">
		<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font><fmtCodelookupExclnReport:message key="LBL_CODE_ID"/>:</td>
		<td width="60%" align="left">&nbsp;<html:text name="frmCodeGroup" property="codeid" size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtCodelookupExclnReport:message key="BTN_LOAD" var="varLoad"/>&nbsp;<gmjsp:button style=" align=right" value="&nbsp;${varLoad}&nbsp;" gmClass="button" buttonType="Save" onClick="javascript:fnLoad();"/>
		</td>
		
	</tr>
	<tr>
		<td colspan="2" height="1" class="LLine"></td>
	</tr>
	<tr >
		<td class="RightTableCaption" align="right" HEIGHT="24"><fmtCodelookupExclnReport:message key="LBL_CODE_NAME"/>&nbsp;:&nbsp;</td>
		<td width="60%" align="left"><bean:write name="frmCodeGroup" property="codenm"></bean:write></td>
	</tr>
	<tr class="Shade">
				<td colspan="2">
					<div id="codelookupexclreport" height="500px" width="600px"></div>
				</td>
	</tr>
	
		<tr>
				<td colspan="8" align="center" height="25" width="100%">
					<div class='exportlinks'><fmtCodelookupExclnReport:message key="DIV_EXPORT_OPT"/>:&nbsp;<img src='img/ico_file_excel.png' /> <a href="#"
							onclick="javascript:fnExportExcel();"> <fmtCodelookupExclnReport:message key="DIV_EXCEL"/>  </a>
				</td>
			</tr>
</table>
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</body>




</HTML>
