
<%
	/**********************************************************************************
	 * File		 		: GmAssetMaster.jsp
	 * Desc		 		: This screen is used for the 
	 * Version	 		: 1.0
	 * author			: Velu
	 ************************************************************************************/
%>


<!-- it\GmAssetMaster.jsp -->

<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>
<%@ page import="com.globus.common.beans.GmCommonClass"%>
<%@ page import="com.globus.common.beans.GmLogger"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ include file="/common/GmHeader.inc"%>


<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<bean:define id="strOpt" name="frmAssetMaster"  property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="assetID" name="frmAssetMaster"  property="assetID" type="java.lang.String"></bean:define>
 <%
 String strItJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_IT");
 %>
<HTML>
<HEAD>
<TITLE>Globus Medical: Asset Master Action</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">


<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strItJsPath%>/GmAssetMaster.js"></script>
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("ASSET_MASTER");
%>

<style type="text/css" media="all">
@import url("styles/screen.css");
</style>

</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoad();">
<html:form action="/gmAssetMasterAction.do">

	<html:hidden property="strOpt" />
	<html:hidden property="hTxnId" value=""/>
	<html:hidden property="hCancelType" value="VASST"/>

	<table border="0" class="DtTable680" cellspacing="0" cellpadding="0">

		<tr>
			<td height="25"  class="RightDashBoardHeader">Asset Account</td>
			<td align="right" class=RightDashBoardHeader><img id='imgEdit'
				style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help'
				width='16' height='16'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr>
					<td colspan="2" height="1" class="line"></td>
				</tr>
				<tr>
					<td  class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>Asset ID:</td>
					<td>&nbsp;<html:text size="30" name="frmAssetMaster" property="assetID" styleClass="InputArea"	onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
				</tr>
				<tr><td colspan="2" height="1" class="LLine"></td></tr>
				<tr class="Shade">
					<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>Asset Name:</td>
					<td>&nbsp;<html:text size="30" name="frmAssetMaster"	property="assetName" styleClass="InputArea"	onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
				</tr>
				<tr><td colspan="2" height="1" class="LLine"></td></tr>
				<tr>
					<td  class="RightTableCaption" align="right" HEIGHT="24">Asset Desc:</td>
					<td>&nbsp;<html:textarea rows="5" cols="30" name="frmAssetMaster" property="assetDesc" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
				</tr>
				<tr><td colspan="2" height="1" class="LLine"></td></tr>
				<tr class="Shade">
					<td  class="RightTableCaption" align="right" HEIGHT="24">Active Flag:</td>
					<td>&nbsp;<html:checkbox name="frmAssetMaster" property="activeFlag" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
				</tr>
				<tr><td colspan="2" height="1" class="LLine"></td></tr>
       		<tr>
				<td colspan="2" align="center" height="25">&nbsp;
                <jsp:include page="/common/GmIncludeLog.jsp" >
                	<jsp:param name="FORMNAME" value="frmAssetMaster" />
					<jsp:param name="ALNAME" value="alLogReasons" />
					<jsp:param name="LogMode" value="Edit" />																					
				</jsp:include>	
				</td>
			</tr>		
        	<tr>
           	<td colspan="2" align="center" height="30" >
		        <gmjsp:button value="Submit" gmClass="button" buttonType="Save" onClick="fnSubmit();" /> 
		        <gmjsp:button value="&nbsp;&nbsp;Void&nbsp;&nbsp;" gmClass="button" buttonType="Save" onClick="fnVoid();" />
				<%if(strOpt.equals("edit")){ %>		        
		        <gmjsp:button value="Asset Attribute" gmClass="button" buttonType="Save" onClick="fnAssetAttribute();" />
		        <%}%>
		    </td>
		    </tr>
			</table>
			
<%@ include file="/common/GmFooter.inc" %> 
</html:form>
</BODY>

</HTML>
