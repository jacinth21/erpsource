<%
/**********************************************************************************
 * File		 		: GmAddUserGroup.jsp
 * Desc		 		: For Add User Group
 * Version	 		: 1.0
 * author			: Gopinathan
************************************************************************************/
%>


<!--it\GmAddUserGroup.jsp -->


<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<bean:define id="gridData" name="frmUserGroupMap" property="xmlString" type="java.lang.String"></bean:define>
<bean:define id="grpType" name="frmUserGroupMap" property="grpType" type="java.lang.String"></bean:define>
<%
String strItJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_IT");
String strHeader="";
String strLabel="";
if(grpType.equals("92265")){
	strHeader = "Module Search";
	strLabel="Search Module";
}else{
	strHeader = "Security Group Search";
	strLabel="Search Security Group";
}
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Add User Group</TITLE>

<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strItJsPath%>/GmAddUserGroup.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
var objGridData = '<%=gridData%>';
var grpType='<%=grpType%>';
document.onkeypress = function(){
	if(event.keyCode == 13){
		fnLoad();
	}
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<html:form action="/gmUserGroupMap.do?method=fetchAddUserGroup">
<html:hidden property="strOpt"/>
<html:hidden property="grpType"/>
<html:hidden property="funcID"/>
	<table class="DtTable700" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td height="25" width="40%"  class="RightDashBoardHeader"><%=strHeader %></td>
			<td align="right" colspan="2" class=RightDashBoardHeader><img id='imgEdit'
				align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
				title='Help'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("GROUP_USER_MAP")%>');" />
			</td>
		</tr>
		<tr>
			<td colspan="3" class="Line" height="1"></td>
		</tr>
		<tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
			<gmjsp:label type="MandatoryText"  SFLblControlName="<%=strLabel%>" td="true"/>
			<td height="30" width="30%">&nbsp;
				<html:text	property="filterData" size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
				 onblur="changeBgColor(this,'#ffffff');" />
			</td>
			<td align="left" width="40%">
				&nbsp;&nbsp;<gmjsp:button
					value="&nbsp;Load&nbsp;" name="submitbtn" tabindex="11"
						gmClass="button" buttonType="Load" onClick="javascript:fnLoad();"/>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<%-- 
					<%if(grpType.equals("92265")){%>
					
					<a href= javascript:fnEditDetails('MODRPT');><b>Module Report</b></a>	
					<%}else{%>
						<a href= javascript:fnEditDetails('SECGRP');><b>Security Group Report</b></a>
					<%}%>--%>
			</td>
		</tr>
		<tr>
			<td colspan="3" class="Line" height="1"></td>
		</tr>
		<tr>
				<td colspan="3" width="100%">
						<div id="dataGrid" style="height: 400px; width: 700px;"></div>
				</td>
		</tr>
		<tr>
			<td colspan="3" class="Line" height="1"></td>
		</tr>
		<tr>
			<td align="center" colspan="3" height="30">
				<gmjsp:button
					value="&nbsp;Add&nbsp;" name="Addbtn" tabindex="11"
						gmClass="button" buttonType="Save" onClick="javascript:fnAdd();"/>
			</td>
		</tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>