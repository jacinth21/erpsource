
<%
/**********************************************************************************
 * File		 		: GmAssetAtrribute.jsp
 * Desc		 		: This screen is used for the asset attribute 
 * Version	 		: 1.0
 * author			: Gopinathan Saravanan
 * Test Prog Build
************************************************************************************/
%>


<!-- it\GmAssetAttribute.jsp -->

<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>
<%@ page import="com.globus.common.beans.GmCommonClass"%>
<%@ page import="com.globus.common.beans.GmLogger"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ include file="/common/GmHeader.inc"%>

<% 
String strItJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_IT");
String strWikiTitle = GmCommonClass.getWikiTitle("ASSET_ATTRIBUTE");
%>


<bean:define id="strOpt" name="frmAssetAtt" property="strOpt" type="java.lang.String"></bean:define>
<HTML>
<HEAD>

<TITLE>Globus Medical: Asset Attribute</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strItJsPath%>/GmAssetAttribute.js"></script>
<style type="text/css" media="all">
@import url("styles/screen.css");
</style>


</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnDisable();">
<form name="frmAssetAtt" method="post" action="/gmAssetAttribute.do">
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hCancelType" value="VASAT"/>
<html:hidden name="frmAssetAtt" property="strOpt" /> 
<html:hidden name="frmAssetAtt" property="attId" />

<table border="0" class="DtTable680" cellspacing="0" cellpadding="0">
	<tr>
		<td height="25" class="RightDashBoardHeader">&nbsp;Asset Attribute</td>
		<td align="right" class=RightDashBoardHeader><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
	</tr>
	<tr>
		<td colspan="2" height="1" class="line"></td>
	</tr>
	<tr class="Shade">
		<td colspan="6" bgcolor="#CCCCCC" height="1"></td>
	</tr>
	<tr>
		<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>Asset ID&nbsp;:</td>
		<td align="left">&nbsp;<html:text name="frmAssetAtt" property="assetID" size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" /></td>
	</tr>
	<tr>
		<td colspan="2" height="1" class="LLine"></td>
	</tr>
	<tr class="Shade">
		<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>Attribute Type&nbsp;:</td>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<td width="60%" align="left">&nbsp;<gmjsp:dropdown	controlName="attTyp" SFFormName="frmAssetAtt" SFSeletedValue="attTyp" SFValue="alAssTyp" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" onChange="fnDropdown();" /> &nbsp;&nbsp;&nbsp;&nbsp;
		<logic:notEqual name="frmAssetAtt" property="strOpt" value="edit">
		<gmjsp:button value="Load" gmClass="button" buttonType="Load" onClick="fnLoad();" />
		</logic:notEqual>
		</td>
	</tr>
	<logic:equal name="frmAssetAtt" property="strOpt" value="edit">
	<tr><td colspan="2" height="1" class="LLine"></td></tr>
	<tr>
		<td class="RightTableCaption" align="right" HEIGHT="24">Attribute value&nbsp;:</td>
		<td align="left">&nbsp;<html:textarea name="frmAssetAtt" property="attributeValue" cols="30" rows="6" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" /></td>
	</tr>
	<tr><td colspan="2" height="1" class="LLine"></td></tr>
	<tr class="Shade">
		
		<td class="RightTableCaption" align="right" HEIGHT="24">Active Flag&nbsp;:</td>
		<td align="left">&nbsp;<html:checkbox name="frmAssetAtt" property="activeflag"></html:checkbox> &nbsp;</td>
	</tr>
	<tr><td colspan="2" height="1" class="LLine"></td></tr>
	<tr>
	</tr>
	<tr>
		<td colspan="2" align="center" height="24">
		<gmjsp:button value="&nbsp;Submit&nbsp;" gmClass="button" buttonType="Save" onClick="fnSubmit();"/>
		<gmjsp:button value="&nbsp;Void&nbsp;" gmClass="button" buttonType="Save" onClick="fnVoid();"/>
		<gmjsp:button value="&nbsp;Reset&nbsp;" gmClass="button" buttonType="Save" onClick="fnReset();"/>
		</td>
	</tr>
	</logic:equal>
</table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
