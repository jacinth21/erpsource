


<!--globusMedApp\GmIncPortalTitle.jsp-->

<%@ page language="java"%>
<%@ page import="com.globus.common.beans.GmCommonClass"%>
<link rel="icon" type="image/png" href="images/GlobusOneLogo.png"/>
<%
String strCountryCode = GmCommonClass.parseNull(GmCommonClass.getString("COUNTRYCODE")).toUpperCase();

if(strCountryCode.equalsIgnoreCase("EN")){
	strCountryCode = "US";
}
%>
<title>GlobusOne Enterprise Portal (<%=strCountryCode%>)</title>