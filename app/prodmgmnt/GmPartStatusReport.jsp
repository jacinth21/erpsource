<%
/**********************************************************************************
 * File		 		: GmPartStatusReport.jsp
 * Desc		 		: Displays the part reprot for publish purpose
 * Version	 		: 1.0
 * author			: Himanshu
************************************************************************************/
%>
<!-- prodmgmnt\GmPartStatusReport.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtPartStatusReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartStatusReport:setLocale value="<%=strLocale%>"/>
<fmtPartStatusReport:setBundle basename="properties.labels.prodmgmnt.GmPartStatusReport"/>


<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import="java.util.ArrayList,java.util.*" %>
<% String strWikiTitle = GmCommonClass.getWikiTitle("OP_GROUP_PART_MAP");%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Status Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_drag.js"></script>
 
<bean:define id="gridData" name="frmGroupPartStatusReport" property="gridXmlData" type="java.lang.String"> </bean:define> 

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/displaytag.css");
      div.gridbox_dhx_skyblue table.obj tr td{
     border-width:0px 1px 0px 0px;
     border-color : #A4BED4;
}
</style>
<script>
var arrGroupInp = new Array(1);
var mygrid;
 
function fnOnPageLoad() {

	var gridObjData = '<%=gridData%>';
	if(gridObjData!="")
	{
	   	mygrid = initMyGrid('dataGridDiv',gridObjData);
	   
	 }

}


function initMyGrid(divRef,gridData){
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");	
	gObj.init();		
	gObj.loadXMLString(gridData);		
	return gObj;
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad()">
<html:form action="/gmGroupPartStatusReport.do">

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtPartStatusReport:message key="LBL_PART_STATUS"/></td>
		</tr>
		<tr><td colspan="4" height="1" class="line"></td></tr>
		<tr>
			<td  valign="top" colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0"> 
				
				    
			 	 <tr><td colspan="4" class="lline"></td></tr>
				   <tr>
			            <td colspan="4" valign="top">
			            <div id="dataGridDiv" style="height:500px;width:698px;"></div>
						</td>
	    	       </tr> 
    			 	<tr><td colspan="4" class="ELine"></td></tr>
	  		 	</table>
  			   </td>
  		  </tr>	
    </table>	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>

