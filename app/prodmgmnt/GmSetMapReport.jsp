
<%
	/**********************************************************************************
	 * File		 		: GmSetMapReport.jsp
	 * Desc		 		: This screen is used for the Set and Part Number Mapping Report
	 * Version	 		: 1.0
	 * author			: Dhinakaran James
	 ************************************************************************************/
%>
<!-- prodmgmnt\GmSetMapReport.jsp -->
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtSetMapReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtSetMapReport:setLocale value="<%=strLocale%>"/>
<fmtSetMapReport:setBundle basename="properties.labels.prodmgmnt.GmSetMapReport"/>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>

<jsp:useBean id="brandBean"
	type="com.globus.common.beans.GmResourceBundleBean"
	class="com.globus.common.beans.GmResourceBundleBean" scope="request">
</jsp:useBean>
<%
	String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
	GmServlet gm = new GmServlet();
	gm.checkSession(response, session);
	
	HashMap hmReturn = (HashMap) request.getAttribute("hmReturn");
	HashMap hmDropDown = GmCommonClass.parseNullHashMap((HashMap) request.getAttribute("HMDROPDOWN"));
	HashMap hmParam = GmCommonClass.parseNullHashMap((HashMap) request.getAttribute("HMPARAM"));

	String strhAction = (String) request.getAttribute("hAction") == null? "": (String) request.getAttribute("hAction");
	String strOpt = GmCommonClass.parseNull((String) request.getAttribute("strOpt"));
	String strChecked = "";
	String strSetId = GmCommonClass.parseNull((String) request.getAttribute("hSetId"));
	String strSetName = GmCommonClass.parseNull((String) request.getAttribute("hSetNm"));
	String strFromPage = GmCommonClass.parseNull((String) request.getAttribute("hFromPage"));
	String strHidePaging = GmCommonClass.parseNull((String)request.getAttribute("hidePaging"));
	ArrayList alSetMap = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("SETMAPPEDLIST"));
	
	String strPartNum = "";
	String strDesc = "";
	String strQty = "";
	String strSetNm = "";
	String strLastUpdBy = "";
	String strLastUpdDt = "";
	String strInSetFl = "";
	String strCriticalFl = "";
	String strPriceHeader = "";
	String strHeader = "";
	int intMapSize = 0;

	ArrayList alSetList = new ArrayList();
	ArrayList alSetPartMapList = new ArrayList();
	ArrayList alSystem = GmCommonClass.parseNullArrayList((ArrayList) hmDropDown.get("SYSTEM"));
	ArrayList alHierarchy = GmCommonClass.parseNullArrayList((ArrayList) hmDropDown.get("HIERARCHY"));
	ArrayList alCategory = GmCommonClass.parseNullArrayList((ArrayList) hmDropDown.get("CATEGORY"));
	ArrayList alType = GmCommonClass.parseNullArrayList((ArrayList) hmDropDown.get("SETTYPE"));
	ArrayList alStatus = GmCommonClass.parseNullArrayList((ArrayList) hmDropDown.get("STATUS"));

	String strSearchSet = GmCommonClass.parseNull((String) hmParam.get("SETNAME"));
	String strSearchSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
	String strSystem = GmCommonClass.parseNull((String) hmParam.get("SYSTEM"));
	String strHierarchy = GmCommonClass.parseNull((String) hmParam.get("HIERARCHY"));
	String strCategory = GmCommonClass.parseNull((String) hmParam.get("CATEGORY"));
	String strType = GmCommonClass.parseNull((String) hmParam.get("SETTYPE"));
	String strStatus = GmCommonClass.parseNull((String) hmParam.get("SETSTATUS"));
	String strProjId =  GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
	String strProjNm =  GmCommonClass.parseNull((String) hmParam.get("PROJECTNAME"));
	String strLoadData = GmCommonClass.parseNull((String) hmParam.get("LOADDATA"));

	int intSize = 0;
	HashMap hcboVal = null;
	
	if (strhAction.equals("Drilldown")){
		strHeader = strSetId +"  :   "+ strSetName;
	}else{
		strHeader = brandBean.getProperty("Heading");
	}
	

	boolean strExport = true;

	if (strFromPage.equals("Parent"))		
	{
		strExport = false;
		
	}
%>
<script>
var stOpt;
stOpt = '<%=strOpt%>';
</script>

<HTML>
<HEAD>

<TITLE><%=brandBean.getProperty("Title")%></TITLE>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
#searchCbo_Proj {
position: relative;
top: -5px;
}
</style>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>	
<script language="javascript" src="<%=strProdMgmntJsPath%>/GmSetMapReport.js"></script>
</HEAD>
<BODY leftmargin="20" topmargin="10">
<FORM name="frmVendor" method="POST"
	action="<%=strServletPath%>/GmSetReportServlet"><input
	type="hidden" name="hAction" value="<%=strhAction%>"> <INPUT
	type="hidden" name="hSetId" value=""> <INPUT type="hidden"
	name="hSetNm" value=""> <INPUT type="hidden" name="hOpt"
	value="">
	 <input type="hidden" name="hLoadData" value="">
	<input type="hidden" name="hFromPage" value="<%=strFromPage%>">
	<input type="hidden" name="hidePaging" value="<%=strHidePaging%>">
	<input type="hidden" name="cbo_projName" id="cbo_projName" value="">
	
	
<!-- Custom tag lib code modified for JBOSS migration changes -->

<table border="0" CLASS="DtTable1100" cellspacing="0"
	cellpadding="0">
	<tr>
		<td colspan="9" class="Line" height="1"></td>
	</tr>
	<tr>
		<td height="25" colspan="9" class="RightDashBoardHeader">&nbsp;<%=strHeader%>
		</td>
	</tr>
	<%
		if (strhAction.equals("Load")) {
	%>
	<tr>
		<td bgcolor="#666666" height="1" colspan="9"></td>
	</tr>
	<tr class="shade">
		<td height="25" class="RightTableCaption" align="right"><fmtSetMapReport:message key="LBL_SET_NAME"/>:
		</td>
		<td>&nbsp;<input type="text" name="Txt_Setname"
			tabIndex="1" value="<%=strSearchSet%>"></td>

		<td class="RightTableCaption" align="right"><fmtSetMapReport:message key="LBL_SYSTEM"/>:</td>
		<td>&nbsp;<gmjsp:dropdown controlName="Cbo_System"
			seletedValue="<%= strSystem%>" tabIndex="2" value="<%=alSystem%>"
			codeId="ID" codeName="NAME" defaultValue="[Choose One]" /></td>

	</tr>
	<tr>
		<td bgcolor="#666666" height="1" colspan="9"></td>
	</tr>
	<tr>
		<td height="25" class="RightTableCaption" align="right">
		<fmtSetMapReport:message key="LBL_HIERARCHY"/>:</td>
		<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Hierarchy"
			seletedValue="<%= strHierarchy%>" tabIndex="3"
			value="<%=alHierarchy%>" codeId="CODEID" codeName="CODENMALT"
			defaultValue="[Choose One]" /></td>
		<td class="RightTableCaption" align="right"><fmtSetMapReport:message key="LBL_CATEGORY"/>:</td>
		<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Category"
			seletedValue="<%= strCategory%>" tabIndex="4" value="<%=alCategory%>"
			codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" /></td>

	</tr>
	<tr>
		<td bgcolor="#666666" height="1" colspan="9"></td>
	</tr>
	<tr class="shade">
		<td height="25" class="RightTableCaption" align="right"><fmtSetMapReport:message key="LBL_TYPE"/>:</td>
		<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Type"
			seletedValue="<%= strType%>" tabIndex="5" value="<%=alType%>"
			codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" /></td>
		<td class="RightTableCaption" align="right"><fmtSetMapReport:message key="LBL_STATUS"/>:</td>
		<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Status"
			seletedValue="<%= strStatus%>" tabIndex="6" value="<%=alStatus%>"
			codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" />
		
	</tr>
	<tr>
		<td bgcolor="#666666" height="1" colspan="9"></td>
	</tr>
	<tr>
	<td class="RightTableCaption" HEIGHT="25" align="right"><fmtSetMapReport:message key="LBL_PROJNAME"/>:</td>
						<td HEIGHT="25">&nbsp;
	<jsp:include page="/common/GmAutoCompleteInclude.jsp">
							<jsp:param name="CONTROL_NAME" value="Cbo_Proj" />
							<jsp:param name="METHOD_LOAD" value="loadProjectNameList&searchType=PrefixSearch" />
							<jsp:param name="WIDTH" value="400" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="TAB_INDEX" value="7"/>
							<jsp:param name="CONTROL_NM_VALUE" value="<%=strProjNm%>" /> 
							<jsp:param name="CONTROL_ID_VALUE" value="<%=strProjId%>" />
							<jsp:param name="SHOW_DATA" value="5" />
							<jsp:param name="AUTO_RELOAD" value="" />					
						</jsp:include>
		</td>
		<td height="25" class="RightTableCaption" align="right"><fmtSetMapReport:message key="LBL_SET_ID"/>:
		</td>
		<td>&nbsp;<input type="text" name="Txt_SetId" tabIndex="8"
			value="<%=strSearchSetId%>">&nbsp;&nbsp;&nbsp;&nbsp;<fmtSetMapReport:message key="BTN_LOAD" var="varGo"/><gmjsp:button tabindex="9" value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;"
			gmClass="button" buttonType="Load" onClick="fnGo();" /></td>

	</tr>
	<tr>
		<td bgcolor="#666666" height="1" colspan="7"></td>
	</tr>
	<% 
	if (strLoadData.equals("Y")) {
	%>
	<tr>
		<td width="1100" height="100" valign="top" colspan="9">
		<%
			String strIdTitle = brandBean.getProperty("CaptionId");
				String strIdName = brandBean.getProperty("CaptionName");
				log.debug(" Title " + strIdTitle);
				log.debug(" Title " + strIdName);
		%> <display:table name="SETMASTERLIST" class="its" export="true" id="data" uid="row"
			decorator="com.globus.displaytag.beans.DTProdSetListWrapper">
			<display:setProperty name="export.excel.filename" value="SetMapReport.xls" />
			<display:column class="alignleft"><a href="#" onClick="javascript:fnLookup('${row.ID}');"><img alt="Inventory Lookup" src="/images/location.png"  border="0"></a></display:column>
			<display:column property="ID" title="<%=strIdTitle%>" sortable="true"
				class="alignleft" />
			<display:column property="NAME" title="<%=strIdName%>"
				sortable="true" class="alignleft" />
			<%
				if (strOpt.equals("BOMRPT")) {
			%>
			<fmtSetMapReport:message key="LBL_PART_NUMBER" var="varPartNumber"/><display:column property="PARTNUMDESC" escapeXml="true" title="${varPartNumber}"
				sortable="true" class="alignleft" />
			<%
				}// end of strOpt.equals("BOMRPT")
			%>
			 <fmtSetMapReport:message key="LBL_SYSTEM" var="varSystem" /><display:column property="SYSTEMNAME" title="${varSystem}"
				sortable="true" class="alignleft" />
			<fmtSetMapReport:message key="LBL_PROJID" var="varProjID" /><display:column property="PROJID" title="${varProjID}"
				sortable="true" class="alignleft" />

			<fmtSetMapReport:message key="LBL_PROJNAME" var="varProjName" /><display:column property="PROJNAME" title="${varProjName}"
				sortable="true" class="alignleft" />

			<fmtSetMapReport:message key="LBL_HIERARCHY" var="varHierarchy"/><display:column property="HIERARCHY" title="${varHierarchy}"
				sortable="true" class="alignleft" />
			<fmtSetMapReport:message key="LBL_CATEGORY" var="varCategory"/><display:column property="CATEG" title="${varCategory}" sortable="true"
				class="alignleft" />
			<fmtSetMapReport:message key="LBL_TYPE" var="varType"/><display:column property="TYPE" title="${varType}" sortable="true"
				class="alignleft" />
			<fmtSetMapReport:message key="LBL_STATUS" var="varStatus"/><display:column property="STAT" title="${varStatus}" sortable="true"
				class="alignleft" />
			<fmtSetMapReport:message key="LBL_REV_LEVEL" var="varRevLevel"/><display:column property="REVLVL" title="${varRevLevel}" class="alignleft" />
			<fmtSetMapReport:message key="LBL_SET_TYPE" var="varSetType"/><display:column property="SETTYPENM" title="${varSetType}" sortable="true" class="alignleft" /> 
            <fmtSetMapReport:message key="LBL_BASELINE_SET" var="varBaselineSet"/><display:column property="BASELINENM" title="${varBaselineSet}" sortable="true" class="alignleft" />
			<fmtSetMapReport:message key="LBL_SHARED" var="varShared"/><display:column property="SHAREDSTATNM" title="${varShared}" sortable="true" class="alignleft" />			
		</display:table> <% 
		}
 	} else if (strhAction.equals("Drilldown")) {

 		strQty = brandBean.getProperty("CaptionQty");
 		strPriceHeader = brandBean.getProperty("CaptionPrice");
 		log.debug(" Price header " + strPriceHeader);
 %>
		<tr>
			<td width="848" height="100" valign="top"><display:table
				name="SETMAPPEDLIST" class="its" id="currentRowObject" export="<%=strExport %>" pagesize='<%=(!strHidePaging.equals("YES"))?30:alSetMap.size()%>' 
				decorator="com.globus.displaytag.beans.DTPartListPriceWrapper">
				<display:setProperty name="export.excel.filename" value="SetMapReport.xls" />
				<fmtSetMapReport:message key="LBL_PART_NUMBER" var="varPartNumber"/><display:column property="PNUM" title="${varPartNumber}"
					class="alignleft" />
				<fmtSetMapReport:message key="LBL_DESCRIPTION" var="varDescription"/><display:column property="PDESC" escapeXml="true" title="${varDescription}"
					class="alignleft" />
				<display:column property="QTY" title="<%=strQty%>"
					class="aligncenter" />
				<fmtSetMapReport:message key="LBL_IN_SET_FLAG" var="varInSetFlag"/><display:column property="INSETFL" title="${varInSetFlag}"
					class="aligncenter" />
				<fmtSetMapReport:message key="LBL_CRITICALFL" var="varCriticalFlag"/><display:column property="CRITICALFL" title="${varCriticalFlag}"
					class="aligncenter" />
				<display:column property="LPRICE" title="<%=strPriceHeader%>"
					class="alignright" />
			</display:table></td>
			
			 </tr>
			<%	
					}
			%>
		
		</td>
	</tr>
</table>
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
