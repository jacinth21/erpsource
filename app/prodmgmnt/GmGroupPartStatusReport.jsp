<%
/**********************************************************************************
 * File		 		: GmGroupPartStatusReport.jsp
 * Desc		 		: display Group/Part Status Report
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtGroupPartStatusReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\GmGroupPartStatusReport.jsp -->
<fmtGroupPartStatusReport:setLocale value="<%=strLocale%>"/>
<fmtGroupPartStatusReport:setBundle basename="properties.labels.prodmgmnt.GmGroupPartStatusReport"/>  
 
<HTML>
<HEAD>
<TITLE> Globus Medical: All Groups </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
 
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
 
</HEAD>

<BODY leftmargin="20" topmargin="10">

<html:form action="/gmGroupPartStatusReport.do"  > 
	<table border="0" class="DtTable680" cellspacing="0" cellpadding="0">
		<tr  >
			<td  colspan="2" height="25" class="RightDashBoardHeader">&nbsp;
			<logic:equal  name="frmGroupPartStatusReport" property="htype" value="group">
			<bean:write name="frmGroupPartStatusReport" property="groupnm" />  ( <bean:write name="frmGroupPartStatusReport" property="owner" />   )
			</logic:equal>
			<logic:equal  name="frmGroupPartStatusReport" property="htype" value="part">
			<bean:write name="frmGroupPartStatusReport" property="groupnm" /> ( <bean:write name="frmGroupPartStatusReport" property="owner" /> ) --- <bean:write name="frmGroupPartStatusReport" property="hid" /> 
			</logic:equal>
			 </td>
			 
		</tr>
	 
		<tr><td colspan="2" height="1" class="line"> 
		</td></tr>
		
		<TR bgcolor="#EEEEEE"   class="ShadeRightTableCaption" height = "20"  >
							<td class="RightText" width = '20%' align="center"  >&nbsp; </td> 
							<td class="RightText" align="center">&nbsp;&nbsp;<fmtGroupPartStatusReport:message key="LBL_STATUS"/></td> 
							 
							 
		</TR>	 
			 
		<tr class="shade">	
					<td   width="125" class="RightText" align="right" HEIGHT="24"><fmtGroupPartStatusReport:message key="LBL_PUBLISH"/>&nbsp;</td>
					<td align="center"><bean:write name="frmGroupPartStatusReport" property="publishfl" /> </td>
					 
		</tr>
	            <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
		<tr >
					<td   width="125" class="RightText" align="right" HEIGHT="24"><fmtGroupPartStatusReport:message key="LBL_CONSTRUCT"/>&nbsp;</td>
					<td align="center"><bean:write name="frmGroupPartStatusReport" property="constructfl" /> </td>
					 
					
		</tr>
		
	            <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
		<tr class="shade" > 
					<td  width="125"  class="RightText" align="right" HEIGHT="24"><fmtGroupPartStatusReport:message key="LBL_AD_VP_PRICE"/>&nbsp;</td>
					<td align="center"><bean:write name="frmGroupPartStatusReport" property="priceband" /> </td>
					 
		</tr>
				<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
				
		 
		<tr>
					<td  width="125"  class="RightText" align="right" HEIGHT="24"><fmtGroupPartStatusReport:message key="LBL_GROUP"/>&nbsp;</td>
					<td align="center"><bean:write name="frmGroupPartStatusReport" property="grouptype" /> </td>
					 
		</tr>
		<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
		 
        
		 
    </table>		     	
 
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</html:form>
</HTML>

