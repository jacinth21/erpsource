<%
/**********************************************************************************
 * File		 		: GmProjectsDashboard.jsp
 * Desc		 		: Shows the projects dashboard for Corporate Strategy team
 * Version	 		: 1.0
 * Author			: Satyajit Thadeshwar
 * Last modified by	: Satyajit Thadeshwar
***********************************************************************************/
%>
<!-- prodmgmnt\GmProjectsDashboard.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtProjectsDashboard" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtProjectsDashboard:setLocale value="<%=strLocale%>"/>
<fmtProjectsDashboard:setBundle basename="properties.labels.prodmgmnt.GmProjectsDashboard"/>

<% 
    String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
	String strWikiTitle = GmCommonClass.getWikiTitle("PROJECTS_DASHBOARD");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Projects Dashboard</title>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmProject.js"></script>

</head>
<body leftmargin="20" topmargin="10">
<html:form action="/gmProjectsDashboard.do">
<html:hidden property="strOpt"/>
	<table style="margin: 0 0 0 0; width: 100%; border: 1px solid  #676767;" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2" height="25" class="RightDashBoardHeader">&nbsp;
<fmtProjectsDashboard:message key="LBL_PROJECT_DASHBOARD"/></td>
			<td align="right" class=RightDashBoardHeader > 	<fmtProjectsDashboard:message key="IMG_ALT_HELP" var="varHelp"/>
 				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
        <tr><td colspan="3" height="15"></td></tr> 
        <tr><td colspan="3" class="LLine" height="1"></td></tr>
		<tr>
			<td colspan="3" width="100%">
				<display:table name="requestScope.frmProjectDetails.lresult" class="gmits" id="currentRowObject" 
				decorator="com.globus.prodmgmnt.displaytag.DTProjectsDashboardWrapper" requestURI="/gmProjectsDashboard.do">
					
<fmtProjectsDashboard:message key="LBL_ID" var="varId"/><display:column property="ID" title="${varId}" sortable="true" class="alignleft" style="width:70px;" headerClass="ShadeDarkGrayTD;ShadeMedGrayTD"/>
					
<fmtProjectsDashboard:message key="LBL_PROJECT_NAME" var="varProjectName"/><display:column property="NAME" title="${varProjectName}" sortable="true" class="alignleft;ShadeMedGrayTD" style="width:300px;" headerClass="ShadeDarkGrayTD" />
					
					
<fmtProjectsDashboard:message key="LBL_KICKOFF" var="varKickoffDate"/><display:column property="KICKOFFDT" title="${varKickoffDate}" sortable="true" class="aligncenter;ShadeMedGreenTD" headerClass="ShadeDarkGreenTD"/>
					
<fmtProjectsDashboard:message key="LBL_PRODUCT_LAUNCH" var="varProductLaunch"/><display:column property="LAUNCHDT" title="${varProductLaunch}" sortable="true" class="aligncenter;ShadeMedGreenTD" headerClass="ShadeDarkGreenTD"/>
					
<fmtProjectsDashboard:message key="LBL_DESIGN_STAFFING" var="varDesignStaffing"/><display:column property="DESIGNSTAFFINGDT" title="${varDesignStaffing}" sortable="true" class="aligncenter;ShadeMedGreenTD" headerClass="ShadeDarkGreenTD" />
					
<fmtProjectsDashboard:message key="LBL_PUG_STAFFING" var="varPUGStaffing"/><display:column property="PUGSTAFFINGDT" title="${varPUGStaffing}" sortable="true" class="aligncenter;ShadeMedGreenTD" headerClass="ShadeDarkGreenTD" />

					
<fmtProjectsDashboard:message key="LBL_DESIGN_PROJECTED" var="varDesignProjectedTeam"/><display:column property="DPCOUNT" title="${varDesignProjectedTeam}" sortable="true" class="alignright;ShadeMedBlueTD" headerClass="ShadeDarkBlueTD" />
					
<fmtProjectsDashboard:message key="LBL_DESIGN_ACTUAL_TEAM" var="varDesignActualTeam"/><display:column property="DACOUNT" title="${varDesignActualTeam}" sortable="true" class="alignright;ShadeMedBlueTD" headerClass="ShadeDarkBlueTD" />
					
<fmtProjectsDashboard:message key="LBL_PUG_PROJECTED" var="varPUGProjectedTeam"/><display:column property="PPCOUNT" title="${varPUGProjectedTeam}" sortable="true" class="alignright;ShadeMedBlueTD" headerClass="ShadeDarkBlueTD"/>
					
<fmtProjectsDashboard:message key="LBL_PUG_ACTUAL_TEAM" var="varPUGActualTeam"/><display:column property="PACOUNT" title="${varPUGActualTeam}" sortable="true" class="alignright;ShadeMedBlueTD" headerClass="ShadeDarkBlueTD"/>
				</display:table>
			</td>
		</tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>