 <%
/**********************************************************************************
 * File		 		: GmProjectLookupDtls.jsp
 * Desc		 		: This screen is used for the Project details 
 * Version	 		: 1.0
 * author			: mjayaraman
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@page import="java.util.Date"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- prodmgmnt\GmProjectLookupDtls.jsp -->
<%@ taglib prefix="fmtProjectLookupDtls" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtProjectLookupDtls:setLocale value="<%=strLocale%>"/>
<fmtProjectLookupDtls:setBundle basename="properties.labels.prodmgmnt.GmProjectDetailsSetup"/>

<%
 	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	HashMap hmReturn = new HashMap();
	hmReturn = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturn"));
	String strProjectID="";
	 String strProjectName="";
	 String strStatus="";
	 String strUpdatedBy="";
	 String strUpdatedDate=null; 
	if (hmReturn != null)
	{
	   strProjectID = (String) hmReturn.get("PRJID");
	   strProjectName = (String) hmReturn.get("PRJNM");
	   strStatus = (String) hmReturn.get("PRJSTATUS");
	   strUpdatedBy = (String) hmReturn.get("PRJLASTUPDUSRNM");  
	   strUpdatedDate= GmCommonClass.getStringFromDate( (Date) hmReturn.get("PRJLASTUPDATE"),strGCompDateFmt);		
	}

%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Project Details </TITLE>

<%-- <link rel="stylesheet" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css"> --%>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmProjectDtls" method="POST">

	<table border="0" bordercolor="red" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="5" width="1" class="Line"></td>
			<td bgcolor="#666666" height="1"></td>
			<td rowspan="5" width="1" class="Line"></td>
		</tr>
		
		<tr>
			<td height="15" colspan="4" class="RightDashBoardHeader">
				<table border="0" width="100%" >
					<tr>
						<td colspan="3" height="15" width="90%" class="RightDashBoardHeader"><fmtProjectLookupDtls:message key="LBL_PRJ_DTLS"/></td>
					</tr>
				</table>					
			</td>			
		</tr>
		<tr><td colspan="7" bgcolor="#666666" width="1" height="1"></td></tr>
        <tr>
			<td height="70" valign="top">
			<div  >
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				  <thead>
					<tr  class="ShadeRightTableCaption">
						<th HEIGHT="24" width="80" align="left"><fmtProjectLookupDtls:message key="LBL_PRJID"/></th>
						<th HEIGHT="24" width="100" align="left"><fmtProjectLookupDtls:message key="LBL_PRJNM"/></th>
						<th align="left" width="80"><fmtProjectLookupDtls:message key="LBL_PRJSTATUS"/></th>
						<th width="80" align="left"><fmtProjectLookupDtls:message key="LBL_PRJINIBY"/></th>
						<th width="100" align="left"><fmtProjectLookupDtls:message key="LBL_PRJCRDATE"/></th>
					</tr>
				  </thead>
				  <tbody>
						<td class="RightText" align="left" ><%=strProjectID%></td>
						<td class="RightText" align="left"><%=strProjectName%></td>
						<td class="RightText"  align="left"><%=strStatus%></td>					
						<td class="RightText" align="left"><%=strUpdatedBy%></td>
						<td class="RightText" align="left"><%=strUpdatedDate%></td>
					</tr>
					<tr><td colspan="11" height="1" bgcolor="#CCCCCC"></td></tr>
					</tbody>
				</table>
			</div>
		</td>
			</tr>
			<tr><td colspan="11" height="1" bgcolor="#CCCCCC"></td></tr>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
