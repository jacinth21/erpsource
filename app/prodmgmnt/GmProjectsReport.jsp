<%
/**********************************************************************************
 * File		 		: GmProjectsReport.jsp
 * Desc		 		: Shows the projects report
 * Version	 		: 1.0
 * Author			: Satyajit Thadeshwar
 * Last modified by	: Satyajit Thadeshwar
************************************************************************************/
%>
<!-- prodmgmnt\GmProjectsReport.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtProjectsReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtProjectsReport:setLocale value="<%=strLocale%>"/>
<fmtProjectsReport:setBundle basename="properties.labels.prodmgmnt.GmProjectsReport"/>
<%String strWikiTitle = GmCommonClass.getWikiTitle("PROJECTS_REPORT");%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>

<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ page import="java.util.HashMap" %>
<%@page import="com.globus.common.beans.GmSearchCriteria"%>
<%
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Projects Report</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmProject.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCrossTabFilter.js"></script>

</head>
<body leftmargin="20" topmargin="10">
<html:form action="/gmProjectsReport.do">
<html:hidden property="strOpt"/>
	<table style="margin: 0 0 0 0; width: 100%; border: 1px solid  #676767;" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" height="25" class="RightDashBoardHeader">&nbsp;<fmtProjectsReport:message key="LBL_PROJECTS_REPORT"/></td>
			<td align="right" class=RightDashBoardHeader > 	
 		<fmtProjectsReport:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />

			</td>
		</tr>
        <tr><td colspan="4" height="15"></td></tr> 
        <tr><td colspan="4" class="LLine" height="1"></td></tr>
		<tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
			<td class="RightTableCaption" width="40%">
				&nbsp;&nbsp;Project&nbsp;:&nbsp;
				<gmjsp:dropdown controlName="prjId" SFFormName="frmProjectsReport" SFSeletedValue="prjId"
				SFValue="alPrjList" codeId="ID" codeName="NAME" defaultValue="[Choose One]"/>&nbsp;&nbsp;
			</td>
			<td class="RightTableCaption" width="40%">
				<table>
				<tr>
					<td class="RightTableCaption"><fmtProjectsReport:message key="LBL_MILESTONE"/>&nbsp;:&nbsp;</td>
					<td>
						<div style="display: visible; height: 100px; width: 300px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 8px; margin-right: 8px;">
							<table>
								<logic:iterate id="alMilestones" name="frmProjectsReport" property="alMilestones">
									<tr>
										<td>
											<htmlel:multibox property="selectedMilestones" value="${alMilestones.CODEID}" />
											<bean:write name="alMilestones" property="CODENM" />
										</td>
									</tr>
								</logic:iterate>
							</table>
						</div>
					</td>
				</tr>
				</table>			
			</td>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<td width="15%" align="center">
				<gmjsp:dropdown controlName="strSelection" SFFormName="frmProjectsReport" SFSeletedValue="strSelection"
				SFValue="alSelection" codeId="CODENMALT" codeName="CODENM" defaultValue="[Choose One]" onChange="fnSelectMilestones();"/>
			</td>
			<td width="5%" align="center">
				<fmtProjectsReport:message key="BTN_GO" var="varGo"/><gmjsp:button name="btnGo" value="&nbsp;${varGo}&nbsp;" gmClass="button" buttonType="Save" onClick="fnShowProjectsReport();"/>
			</td>
		</tr>
		<tr><td colspan="4" class="LLine" height="1"></td></tr>
		<tr><td colspan="4" height="15"></td></tr> 

<%
	HashMap hmReportValue = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HMVALUES"));
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	
	gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD");
	//gmCrossTab.setGeneralHeaderStyle("aaTopHeader");
	
	gmCrossTab.setDisplayFilter(true);
	gmCrossTab.setPageContext(pageContext);
	gmCrossTab.setDefaultFilterValueType(GmSearchCriteria.DATE_TYPE);
	
	gmCrossTab.setFilterValueType("Status", GmSearchCriteria.STRING_TYPE);
	gmCrossTab.setFilterValueType("Name", GmSearchCriteria.STRING_TYPE);
	gmCrossTab.setFilterValueType("Segment", GmSearchCriteria.STRING_TYPE);
	gmCrossTab.setFilterValueType("Group", GmSearchCriteria.STRING_TYPE);
	gmCrossTab.setFilterValueType("Project Manager", GmSearchCriteria.STRING_TYPE);
	gmCrossTab.setFilterValueType("Product Manager", GmSearchCriteria.STRING_TYPE);
	
	
	gmCrossTab.setRowHighlightRequired(true);
	gmCrossTab.setSortRequired(false);
	
	gmCrossTab.setColumnWidth("Name",150);
	
	// Line Style information
	gmCrossTab.setColumnDivider(true);
	gmCrossTab.setColumnLineStyle("borderLight");

	gmCrossTab.setFixedHeader(true);

	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setDecorator("com.globus.crosstab.beans.GmProjectsReportDecorator");
	
	gmCrossTab.addLine("Name");
	gmCrossTab.addLine("Status");
	gmCrossTab.addLine("Project Manager");
	gmCrossTab.addLine("Product Manager");
	gmCrossTab.addLine("Segment");
	gmCrossTab.addLine("Group");
	gmCrossTab.setNoDivRequired(true);
	gmCrossTab.setRoundOffAllColumnsPrecision("0");
	gmCrossTab.displayZeros(true);
	
	gmCrossTab.addStyle("Name","ShadeMedGrayTD","ShadeDarkGrayTD");
	gmCrossTab.addStyle("Project Manager","ShadeLightGreenTD","ShadeDarkGreenTD");
	gmCrossTab.addStyle("Product Manager","ShadeLightGreenTD","ShadeDarkGreenTD");
	gmCrossTab.addStyle("Status","ShadeMedBlueTD","ShadeDarkBlueTD");
	gmCrossTab.addStyle("Segment","ShadeMedGrayTD","ShadeDarkGrayTD");
	gmCrossTab.addStyle("Group","ShadeMedGrayTD","ShadeDarkGrayTD");
	
	gmCrossTab.addStyle("Design Projected Count","ShadeLightGreenTD","ShadeDarkGreenTD");
	gmCrossTab.addStyle("Design Actual Count","ShadeLightGreenTD","ShadeDarkGreenTD");
	gmCrossTab.addStyle("PUG Projected Count","ShadeLightGreenTD","ShadeDarkGreenTD");
	gmCrossTab.addStyle("PUG Actual Count","ShadeLightGreenTD","ShadeDarkGreenTD");
%>
		<tr>
			<td colspan="4">	
				<%=gmCrossTab.PrintCrossTabReport(hmReportValue)%>
			</td>
		</tr>
 	</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>