 <%
/**********************************************************************************
 * File		 		: GmProjectSetup.jsp
 * Desc		 		: This screen is used for the Project Maintenance
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<!-- prodmgmnt\GmProjectSetup.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtProjectSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtProjectSetup:setLocale value="<%=strLocale%>"/>
<fmtProjectSetup:setBundle basename="properties.labels.prodmgmnt.GmProjectSetup"/>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strApprove = GmCommonClass.parseNull((String)request.getAttribute("BLAPPROVEACCESS"));
	String strMessage = GmCommonClass.parseNull((String)request.getAttribute("MESSAGE"));
	
	log.debug(" Approve " + strApprove + " and message " + strMessage);
	log.debug(" Values inside hmReturn in JSP is >>>> " + hmReturn);
	ArrayList alDevClass = new ArrayList();
	ArrayList alProjectType = new ArrayList();
    ArrayList alProjectStatus = new ArrayList();
    ArrayList alGroup = new ArrayList();
    ArrayList alSegment = new ArrayList();
	
	alDevClass = (ArrayList)hmReturn.get("DEVCLASS");
	alProjectType = (ArrayList)hmReturn.get("PROTYPE");
	alProjectStatus = (ArrayList)hmReturn.get("PROJECTSTATUS");
    alGroup = (ArrayList)hmReturn.get("GROUP");
    alSegment = (ArrayList)hmReturn.get("SEGMENT");
	
	int intSize = 0;
	HashMap hmVal = null;
	HashMap hcboVal = null;
	String strCodeID = "";
	String strhAction = "Add";
	String strProjId = "";
	String strProjName = "";
	//String strIdeaId = "";
	String strDesc = "";
	String strDevClass = "";
	String strPManager = "";
	String strProjectName = "1500";
	String strSelected = "";
	String strCodeId = "";
	String strStatus = "";
	String strGroup = "";
	String strSegment = "";
	String strProjectId = "";
	String strStatusId = "0";
	HashMap hmProjectDetails = (HashMap)hmReturn.get("PROJECTDETAILS");

	if (hmProjectDetails != null)
	{
		strProjName = GmCommonClass.parseNull((String)hmProjectDetails.get("NAME"));
		//strIdeaId	= (String)hmProjectDetails.get("IDEAID");
		strProjectId = GmCommonClass.parseNull((String)hmProjectDetails.get("ID"));
		strProjectName = GmCommonClass.parseNull((String)hmProjectDetails.get("PROJECTTYPE"));
		strDesc		= GmCommonClass.parseNull((String)hmProjectDetails.get("PDESC"));
		strDevClass	= GmCommonClass.parseNull((String)hmProjectDetails.get("DEVCLASS"));
		strPManager	= GmCommonClass.parseNull((String)hmProjectDetails.get("PMAN"));
		strStatus   = GmCommonClass.parseNull((String)hmProjectDetails.get("STATUSID"));
		strGroup  = GmCommonClass.parseNull((String)hmProjectDetails.get("GROUPID"));
		strSegment  = GmCommonClass.parseNull((String)hmProjectDetails.get("SEGMENTID"));
		strStatusId = GmCommonClass.parseZero((String)hmProjectDetails.get("STATUSIDID"));
		strhAction  = "Edit";
	}

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Project Setup </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
var lblGroup='<fmtProjectSetup:message key="LBL_GROUP"/>';
var lblProjectName='<fmtProjectSetup:message key="LBL_PROJECT_NAME"/>';
var lblSegment='<fmtProjectSetup:message key="LBL_SEGMENT"/>';
var lblProjectDescription='<fmtProjectSetup:message key="LBL_PROJECT_DESC"/>';
function fnSubmit()
{
	var arChk = document.frmProject.Chk_DevCl;
	var str = '';
	var len = arChk.length;
	for (i=0;i< len;i++ )
	{
		if (document.frmProject.Chk_DevCl[i].checked)
		{
			str = str + document.frmProject.Chk_DevCl[i].value + ',';
		}
	}
	len = str.length;
	str = str.substring(0,len-1);
	document.frmProject.hChkDevClass.value = str;
	
	fnValidateTxtFld('Txt_ProjNm',lblProjectName);
	fnValidateTxtFld('Txt_ProjDesc',lblProjectDescription);
	fnValidateDropDn('Cbo_Group',lblGroup);
	fnValidateDropDn('Cbo_Segment',lblSegment);
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	fnStartProgress('Y');
	document.frmProject.submit();
}

function fnLoad()
{
	var val = document.frmProject.hChkDevClass.value;
	var arr = val.split(",");
	var devcl = document.frmProject.Chk_DevCl;
	
	for (i=0;i<arr.length ;i++ )
	{
		for (j=0;j<devcl.length ;j++ )
		{
			if (arr[i] == devcl[j].value)
			{
				document.frmProject.Chk_DevCl[j].checked = true;
			}
		}
	}

}

function fnVoidProject()
{
		document.frmProject.action ="<%=strServletPath%>/GmCommonCancelServlet";
		document.frmProject.hAction.value = "Load";
		document.frmProject.submit();
}

function fnApproveProject()
{
	document.frmProject.hApprove.value = "approved";
	document.frmProject.submit();
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad();">
<FORM name="frmProject" method="POST" action="<%=strServletPath%>/GmProjectServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hTxnId" value="<%=strProjectId%>">
<input type="hidden" name="hCancelType" value="VDPRJ">
<input type="hidden" name="hChkDevClass" value="<%=strDevClass%>">
<input type="hidden" name="hApprove" value="">
<input type="hidden" name="hStatus" value="<%=strStatusId%>">
<input type="hidden" name="hOldProjName" value="<%=strProjName%>">


<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" width="500" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="10"></td>
			<td class="RightDashBoardHeader" height="28">&nbsp;<fmtProjectSetup:message key="LBL_PROJECT_SETUP"/></td>
			<td bgcolor="#666666" width="1" rowspan="10"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="498" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<% if (!strMessage.equals("")) { %>
					<tr>
						<td class="RightTableCaption" align="center" HEIGHT="24" colspan="2"> <font color = "green"> <%=strMessage%> </font></td>
					</tr>
				<% } %>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtProjectSetup:message key="LBL_PROJECT_NAME"/>:</td>
						<td width="320">&nbsp;&nbsp;<input type="text" size="41" value="<%=strProjName%>" name="Txt_ProjNm" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"  ></td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>				
					
					<tr>
						<td class="RightTableCaption" width="250" HEIGHT="30" align="right">&nbsp;<fmtProjectSetup:message key="LBL_PROJECT_TYPE"/>:</td>
						<td width="380">&nbsp;
							<gmjsp:dropdown controlName="Cbo_Proj"  seletedValue="<%=strProjectName%>" 	
							   value="<%=alProjectType%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"  />
						</td>	
					</tr>
					<!--
					<tr class="shade">
						<td class="RightText" align="right" HEIGHT="24">New Idea Log #:</td>
						<td width="320">&nbsp;<input type="text" size="10" value="" name="Txt_IdeaId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=2>
						<gmjsp:button value="Lookup" gmClass="button" buttonType="Load" tabindex="13" /></td>
					</tr>
					-->
					<tr>
						<td class="RightTableCaption" valign="top" align="right" HEIGHT="24"><fmtProjectSetup:message key="LBL_DEVICE_CLASSIFICATION"/>:</td>
						<td class="RightText" width="320">
<%
			  		intSize = alDevClass.size();
					hmVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hmVal = (HashMap)alDevClass.get(i);
			  			strCodeID = (String)hmVal.get("CODEID");
%>
						&nbsp;<input type="checkbox" value= "<%=strCodeID%>" name="Chk_DevCl"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" ><%=hmVal.get("CODENM")%><br>							
<%
			  		}
%>
						</td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>					
					<tr>
						<td class="RightTableCaption" valign="top" align="right" HEIGHT="24"><fmtProjectSetup:message key="LBL_PROJECT_SCOPE"/>:</td>
						<td width="320">&nbsp;&nbsp;<textarea name="Txt_ProjDesc" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" rows=5 cols=40 value="<%=strDesc%>"><%=strDesc%></textarea></td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>					
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtProjectSetup:message key="LBL_STATUS"/>:</td>
						<td width="320">&nbsp;
						<%=strStatus%>
						</td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtProjectSetup:message key="LBL_GROUP"/>:</td>
						<td width="320">&nbsp;
							<gmjsp:dropdown controlName="Cbo_Group"  seletedValue="<%=strGroup%>" 	
					  value="<%=alGroup%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"  />
						</td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtProjectSetup:message key="LBL_SEGMENT"/>:</td>
						<td width="320">&nbsp;
							<gmjsp:dropdown controlName="Cbo_Segment"  seletedValue="<%=strSegment%>" 	
						   value="<%=alSegment%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"  />
						</td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
					
					  <tr>
						<td>&nbsp;</td>
						<td  height="30">&nbsp;
						<fmtProjectSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnSubmit();" />&nbsp;
						<fmtProjectSetup:message key="BTN_RESET" var="varReset"/><gmjsp:button value="${varSubmit}" buttonType="Save" gmClass="button" />&nbsp;
						<% if (!strStatus.equals("")) {%>
						<fmtProjectSetup:message key="BTN_VOID" var="varVoid"/><gmjsp:button value="${varVoid}" gmClass="button" buttonType="Save" onClick="fnVoidProject();" />&nbsp;
						<% } if (strApprove.equals("true") && strStatus.equals("Initiated")) {%>
						<fmtProjectSetup:message key="BTN_APPROVE" var="varApprove"/><gmjsp:button value="${varApprove}" gmClass="button" buttonType="Save" onClick="fnApproveProject();" />&nbsp;
						<% } %>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</HTML>
