<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtIncludeSystemDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\GmIncludeSystemDetails.jsp -->
<fmtIncludeSystemDetails:setLocale value="<%=strLocale%>"/>
<fmtIncludeSystemDetails:setBundle basename="properties.labels.prodmgmnt.GmIncludeSystemDetails"/>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<bean:define id="alSetMapDtls" name="frmSystemMapping" property="alSetMapDtls" type="java.util.ArrayList"></bean:define>
<bean:define id="gridData" name="frmSystemMapping" property="gridData" type="java.lang.String"></bean:define>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>System Details</title>
</head>
<body leftmargin="20" topmargin="10">
<html:form action="/gmSystemMapping.do">
<html:hidden property="strOpt"/>



<table border="0"   width="700" cellspacing="0" cellpadding="0">
	<tr>
		<td height="25" colspan="2" class="RightDashBoardHeader" >&nbsp;&nbsp;<fmtIncludeSystemDetails:message key="LBL_MAPPING_DETAILS"/></td>		
	</tr>
	<tr>
		<td><div id="setMapDtlsData"  height="175px" width="100%" ></div></td> </tr>
	<tr>			
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>