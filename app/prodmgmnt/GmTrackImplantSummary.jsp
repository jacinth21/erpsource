 <%
/**********************************************************************************
 * File		 		: GmTrackImplant.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: V Prasath
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtTrackImplantSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\GmTrackImplantSummary.jsp -->
<fmtTrackImplantSummary:setLocale value="<%=strLocale%>"/>
<fmtTrackImplantSummary:setBundle basename="properties.labels.prodmgmnt.GmTrackImplantSummary"/>
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strSelectedGrpID = (String)request.getAttribute("strSelectedGrpID")==null?"":(String)request.getAttribute("strSelectedGrpID");
	ArrayList alGroup = request.getAttribute("alGroup") == null?new ArrayList() : (ArrayList)request.getAttribute("alGroup");
	ArrayList alImplantSummary =request.getAttribute("alImplantSummary")== null?new ArrayList() : (ArrayList)request.getAttribute("alImplantSummary");

	String strSelected = "";
	String strGrpID = "";
	String strShade = "";
	
	int intSize = 0;
	HashMap hcboVal = null;	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Tracking Summary </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script>
function fnGo()
{	
	fnStartProgress('Y');
	document.frmVendor.submit();
}
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmTrackImplantSummaryServlet">
<input type="hidden" name="hAction" value="Go">
<input type="hidden" name="hMode" value="">

	<table border="0" width="650" cellspacing="0" cellpadding="0">
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtTrackImplantSummary:message key="LBL_TRACKING_IMPLANT_SUMMARY"/></td>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td width="648" height="80" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightText" HEIGHT="30" width="250">&nbsp;<fmtTrackImplantSummary:message key="LBL_GORUP_NAME"/>:</td>
						&nbsp;<select name="Cbo_GrpID" id="Region" class="RightText" tabindex="3"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtTrackImplantSummary:message key="LBL_CHOOSE_ONE"/></option>						
<%
			  		intSize = alGroup.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alGroup.get(i);
			  			strGrpID = (String)hcboVal.get("ID");
						strSelected = strSelectedGrpID.equals(strGrpID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strGrpID%>"><%=hcboVal.get("NAME")%></option>
<%
			  		}
%>
					</select>							
						</td>
						 <td width="400" align="left">
						    <fmtTrackImplantSummary:message key="BTN_GO" var="varGo"/>
							<gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" gmClass="button" buttonType="Load" onClick="fnGo();" />
						 </td>
					</tr>					
					<tr><td colspan="2" class="Line" height="1"></td></tr>
					<%					
					if(alImplantSummary.size() > 0) {					
%>	
						<TR>
						<td colspan="2">					
						<display:table name="alImplantSummary" export="false" class="its" id="currentRowObject">
							<fmtTrackImplantSummary:message key="LBL_PART" var="varPart"/>
							<display:column property="PARTNUM" style="width:40%" title="${varPart}" class="ShadeWhite;alignleft" headerClass="ShadeLightGrayTD"  sortable="true" />
							<fmtTrackImplantSummary:message key="LBL_PART_DESCRIPTION" var="varPartDescription"/>
							<display:column property="PARTDESC" style="width:60%" title="${varPartDescription}" class="ShadeWhite;alignleft" headerClass="ShadeLightGrayTD" />
						</display:table>
					  </td>	
					</tr>
<%						
						}else {
%>
								<TR <%=strShade%>>
									<td height="21" colspan="2" align="center" class="RightTextRed"><fmtTrackImplantSummary:message key="LBL_NO_PARTS_AVAILABLE"/></td>
								<TR>
<%
						}

%>							
	
							</table>
						</td>
					</tr>
					
					<tr><td colspan="2" class="Line" height="1"></td></tr>
					
							</TABLE>
						</TD>
				   </TR>
				   
				</TABLE>
			</TD>
		</tr>		
		
    </table>

</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
