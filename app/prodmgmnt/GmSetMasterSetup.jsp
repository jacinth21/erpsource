
 <%
/**********************************************************************************
 * File		 		: GmSetMasterSetup.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="fmtSetMasterSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\GmSetMasterSetup.jsp -->
<fmtSetMasterSetup:setLocale value="<%=strLocale%>"/>
<fmtSetMasterSetup:setBundle basename="properties.labels.prodmgmnt.GmSetMasterSetup"/>
<jsp:useBean id="ResourceBundleBean" type="com.globus.common.beans.GmResourceBundleBean" class="com.globus.common.beans.GmResourceBundleBean" scope="request"> </jsp:useBean>
<%
log.debug("GmSetMasterSetup........ ");
	String strWikiTitle = GmCommonClass.getWikiTitle("QUALITY_MAINTENANCE");
	String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strSessOpt = (String)session.getAttribute("strSessOpt");
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction");
	HashMap hmLoop = new HashMap();
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.prodmgmnt.GmSetMasterSetup", strSessCompanyLocale);

	String strCurrentStatus =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CURRENT_STATUS"));
	String strQualityTeam =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_QUALITY_TEAM"));
	
	

	
	String strDeptId = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
	String strQltyDisabled = "disabled";
	String strSetStatusFlag = "";
	String strSetStatus = "20365";
	String strRevLevel = "";
	String strCoreSetFlag ="";
	String strCoreSetFlagStatus = "";
	String strConsRptId = "";
	String strProjId =	"";
	String strProjNm = "";
	String strSetId = "";
	String strSetNm = "";
	String strDesc = "";
	String strTemp = "";
	String strCategory = "";
	String strType = "";
	String strReLoadDisable = "Enable";
	String strMessage = "";
	String strSetStatusMsg ="";
	String strSystemStatusFl = "Enable";
	String strInspectionSheet = "";
	String strDetailDesc = "";
	String strDivisionId = "";
	String strLotTrackFlag = "";//PMT-42618
	String strLotTrackFlagChk = "";
	
	//PMT-42618
	ArrayList alProjList = new ArrayList();
	ArrayList alSetCategory = new ArrayList();
	ArrayList alSetType = new ArrayList();
	//ArrayList alSetList = new ArrayList();
	ArrayList alSetStatus = new ArrayList();
	//ArrayList alFileList = new ArrayList();
	ArrayList alDivision = new ArrayList();
	
	
	HashMap hmSetMasterDtls = new HashMap();
	int intPriceSize = 0;
	int intDivisionMapsize = 0;
	
	if (hmReturn != null)
	{
		//alSetList = (ArrayList)hmReturn.get("SETMASTERLIST");
		alProjList = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("PROJECTLIST"));
		alSetCategory = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SETCATEGORY"));
		alSetType = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SETTYPE"));
		alSetStatus = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SETSTATUS"));
		alDivision = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("DIVISION_LIST"));
		
		hmSetMasterDtls = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("SETMASTERDETAILS"));
		
		//alFileList= GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SETFILELIST"));
	}
	
	 if (alProjList != null) {
		intDivisionMapsize = alProjList.size();
	} 
	
	
	
	if (hmSetMasterDtls != null)
	{
		strProjId = GmCommonClass.parseNull((String)hmSetMasterDtls.get("PROJID"));
		strSetId = GmCommonClass.parseNull((String)hmSetMasterDtls.get("SETID"));
		strSetNm = GmCommonClass.parseNull((String)hmSetMasterDtls.get("SETNM"));
		if(strSetNm.contains("\"")){
		  strSetNm = strSetNm.replaceAll("\"","&quot;");
		}
		strProjNm = GmCommonClass.parseNull((String)hmSetMasterDtls.get("PROJNAME"));
		strDesc = GmCommonClass.parseNull((String)hmSetMasterDtls.get("SETDESC"));
		strDetailDesc = GmCommonClass.parseNull((String)hmSetMasterDtls.get("SETDETAILDESC"));
		strCategory = GmCommonClass.parseZero((String)hmSetMasterDtls.get("SETCAT"));
		strType = GmCommonClass.parseZero((String)hmSetMasterDtls.get("SETTYPE"));
		strSetStatus = GmCommonClass.parseNull((String)hmSetMasterDtls.get("SETSTATUS"));
		strRevLevel = GmCommonClass.parseNull((String)hmSetMasterDtls.get("REVLVL"));
		strCoreSetFlag = GmCommonClass.parseNull((String)hmSetMasterDtls.get("CORESETFL"));
		strSetStatusMsg = GmCommonClass.parseNull((String)hmSetMasterDtls.get("STATUSMSG"));
		strCoreSetFlagStatus = strCoreSetFlag.equals("Y") ? "checked" : "";
		strConsRptId = GmCommonClass.parseNull((String)hmSetMasterDtls.get("CONSRPTID"));
		strInspectionSheet = GmCommonClass.parseNull((String)hmSetMasterDtls.get("FILENAME"));
		strDivisionId = GmCommonClass.parseZero((String)hmSetMasterDtls.get("DIVISIONID"));
		//PMT-42618
		strLotTrackFlagChk = GmCommonClass.parseNull((String)hmSetMasterDtls.get("LOTTRACKFL"));
		strLotTrackFlag = strLotTrackFlagChk.equals("Y") ? "checked" : "";
		//PMT-42618
		//Should not display the inspection sheet if the set id is not there
		if(strSetId.equals("")){
		  strInspectionSheet = "";
		}
	}

	if (strhAction.equals("Reload"))
	{
		strReLoadDisable = "disabled";
	}
	
	log.debug("strSetStatus........ " + strSetStatus);
	log.debug("strSetStatusFlag " + strSetStatusFlag);
	
    if (!(strSetStatus.equals("20365") || strSetStatus.equals("20366") || strSetStatus.equals(""))) 
    {
        strSetStatusFlag = "disabled";
        strSystemStatusFl = "disabled";
        strMessage = strCurrentStatus + strSetStatusMsg + strQualityTeam;
    }
                
	 if(strDeptId.equals("Q"))
      {
          strQltyDisabled = "Enable";
          strSetStatusFlag = "Enable";
          strReLoadDisable = "Enable";
          strSystemStatusFl = "Enable";
      }                
	log.debug("After strSetStatus " + strSetStatus);
	log.debug("After strSetStatusFlag " + strSetStatusFlag);
	if (strSessOpt.equals("SYSSETUP")) 
    {
		strQltyDisabled = "disabled";
        strReLoadDisable = "disabled";
        strSystemStatusFl = "disabled";
    }
	String strTypeVal = ResourceBundleBean.getProperty("TypeVal");
	String strtdHeight = "320";
	String strtextareaclass = "HtmlTextArea";
	if(strTypeVal.equals("1602")){
		strtdHeight = "50";
		strtextareaclass = "InputArea";
	}
%>
<HTML>
<HEAD>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style type="text/css" media="all">
     table.DtTable760 {
	margin: 0 0 0 0;
	width: 755px;
	border: 1px solid  #676767;
}
</style>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmSetMasterSetup.js"></script>
<script language="javascript" type="text/javascript" src="<%=strExtWebPath%>/tiny_mce/tiny_mce.js"></script>
<script>
var number = "<%=ResourceBundleBean.getProperty("Number")%>";
var desc = "<%=ResourceBundleBean.getProperty("Desc")%>";
var title = "<%=ResourceBundleBean.getProperty("Title")%>";
var DivisionMapArr = new Array(1);
var DivisionMapSize = <%=alProjList.size()%>;
var newSetId = '<%=strSetId%>';
var newSetNm = '<%=strSetNm%>';

<%if (intDivisionMapsize > 0) {
	for (int i = 0; i < intDivisionMapsize; i++) {
		hmLoop = (HashMap) alProjList.get(i);%>
	DivisionMapArr[<%=i%>] = new Array("<%=hmLoop.get("DIVISIONID")%>","<%=hmLoop.get("DIVISIONNAME")%>","<%=hmLoop.get("ID")%>","<%=hmLoop.get("NAME")%>");
<%}
}%>

function fnHtmlEditor(){
	tinyMCE.init({
		mode : "specific_textareas",
	    editor_selector : "HtmlTextArea",
		theme : "advanced",
		theme_advanced_path : false,
		height : "300",
		width: "500" 
	});
}
</script>
</HEAD>
<BODY onLoad="fnEnableConsngFl();fnHtmlEditor();">
<FORM name="frmVendor" method="POST" enctype="multipart/form-data" action="<%=strServletPath%>/GmSetMasterServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hMode" value="">
<input type="hidden" name="hSetId" id="hSetId" value="<%=strSetId%>">
<input type="hidden" name="hTxnId" value="<%=strSetId%>">
<input type="hidden" name="hCancelType" value="VDSET"/>
<input type="hidden" name="hDivisionId" value="<%=strDivisionId%>">
<input type="hidden" name="h_uploadfile" value="false"/>
<input type="hidden" name="h_filename"/>
<input type="hidden" name="divisionmapsize" id="divisionmapsize" value="<%=intDivisionMapsize%>"/>
<input type="hidden" name="hOld_SetId" id="hOld_SetId" value ="<%=strSetNm%>">
<input type="hidden" name="hOld_InspSheet" id="hOld_InspSheet" value ="<%=strInspectionSheet%>">
<%=ResourceBundleBean.getProperty("Type")%>
	<table class="DtTable760" cellspacing="0" cellpadding="0">
		<tr id="displaytable">
			<td height="25" class="ShadeRightTableCaption" colspan="2"><a href="javascript:fnShowFilters('tabBasicDetails');" tabindex="-1" style="text-decoration:none; color:black" >
			<IMG id="tabBasicDetailsimg" border=0 src="/images/minus.gif">&nbsp;<fmtSetMasterSetup:message key="LBL_BASIC"/></a>		
		</td>
		</tr>
		<tr><td class="LLine" colspan="2" height="1"></td></tr>
		<tr id="tabBasicDetails" style="display:table-row;">	
			<td height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<%if (!strMessage.equals("") && !strSetStatusFlag.equals("Enable")){ %>
					<tr>
						<td colspan="2" class="RightTextRed" align="center">&nbsp;<%=strMessage%></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#CCCCCC"></td></tr>
		<% } %>	
				
					<tr class="evenshade">
						<td class="RightTableCaption" HEIGHT="25" align="right" width="20%">&nbsp;<font color="red">*</font> <fmtSetMasterSetup:message key="LBL_PROJECT_NAME"/>:</td>
						<td width="80%">
						<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="Cbo_Proj" />
							<jsp:param name="METHOD_LOAD" value="loadProjectNameList&searchType=PrefixSearch" />
							<jsp:param name="WIDTH" value="485" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="CONTROL_NM_VALUE" value="<%=strProjNm %>" /> 
							<jsp:param name="CONTROL_ID_VALUE" value="<%=strProjId %>" />
							<jsp:param name="SHOW_DATA" value="5" />
							<jsp:param name="AUTO_RELOAD" value="fnDivMap();" />					
						</jsp:include>
						<%-- <gmjsp:dropdown controlName="Cbo_Proj"  seletedValue="<%=strProjId%>" disabled="<%=strSetStatusFlag%>"	
							   value="<%=alProjList%>" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]" onChange="javascript:fnDivMap();" /> --%>
						</td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#CCCCCC"></td></tr>
					<tr class="oddshade">
						<td class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font> <%=ResourceBundleBean.getProperty("Number")%>&nbsp;</td>
						<td><input type="text" size="20" value="<%=strSetId%>"  name="Txt_SetId" <%=strReLoadDisable%> class="InputArea"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
						</td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#CCCCCC"></td></tr>
					<tr class="evenshade">
						<td class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font> <%=ResourceBundleBean.getProperty("Name")%>&nbsp;</td>
						<td><input type="text" size="85" style="width:500px;" value="<%=strSetNm%>" name="Txt_SetNm" <%=strSetStatusFlag%> class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
						</td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#CCCCCC"></td></tr>
					<tr class="oddshade">
						<td class="RightTableCaption" valign="middle" align="right" HEIGHT="<%=strtdHeight%>"><%=ResourceBundleBean.getProperty("Desc")%>&nbsp;</td>
						<td><textarea name="Txt_SetDesc" class="<%=strtextareaclass%>" <%=strSetStatusFlag%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" rows=3 cols=50 ><%=strDesc%></textarea></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#CCCCCC"></td></tr>
					<tr class="evenshade">
						<td class="RightTableCaption" valign="middle" align="right" HEIGHT="320"><%=ResourceBundleBean.getProperty("Detail")%>&nbsp;</td>
						<td><textarea name="Txt_SetDetailDesc" class="HtmlTextArea" <%=strSetStatusFlag%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" rows=3 cols=50 ><%=strDetailDesc%></textarea></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#CCCCCC"></td></tr>
					<tr class="oddshade">
							<td class="RightTableCaption" align="right" height="24"> <fmtSetMasterSetup:message key="LBL_STATUS"/>:&nbsp;</td>
							<td><gmjsp:dropdown controlName="Cbo_SetStatus"  seletedValue="<%=strSetStatus%>" disabled="<%=strQltyDisabled%>"	
								value="<%=alSetStatus%>" codeId = "CODEID"  codeName = "CODENM"/>
							</td>
						</tr>
						<tr><td colspan="2" height="1" bgcolor="#CCCCCC"></td></tr>
					<tr class="evenshade">
						<td class="RightTableCaption" valign="middle" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;<fmtSetMasterSetup:message key="LBL_DIVISION"/>:&nbsp;</td>
						<td><gmjsp:dropdown controlName="Cbo_Division"  seletedValue="<%=strDivisionId%>" 	
							   value="<%=alDivision%>" codeId = "DIVISION_ID"  codeName = "DIVISION_NAME" defaultValue= "[Choose One]"  onChange="javascript:fnValidateDivision();" /></td>
					</tr>
					</table>
				</tr>
				<tr><td class="ELine" colspan="2" height="1"></td></tr>
				<tr id="displaytable">
					<td height="25" class="ShadeRightTableCaption" colspan="2"><a href="javascript:fnShowFilters('tabSetDetails');" tabindex="-1" style="text-decoration:none; color:black" >
					<IMG id="tabSetDetailsimg" border=0 src="/images/minus.gif">&nbsp;<%=ResourceBundleBean.getProperty("Section")%></a>
					</td>
				</tr>	
				<tr id="tabSetDetails" style="display:table-row;">					
					<td colspan="2">
						<table border="0" width="100%" cellspacing="0" cellpadding="0" >
						<tr class="evenshade">
							<td class="RightTableCaption" HEIGHT="25" align="right" width="20%">&nbsp;<fmtSetMasterSetup:message key="LBL_CATEGORY"/>:</td>
							<td width="20%">&nbsp;<gmjsp:dropdown controlName="Cbo_Cat"  seletedValue="<%=strCategory%>" disabled="<%=strSystemStatusFl%>"
									value="<%=alSetCategory%>" codeId="CODEID"  codeName="CODENM" defaultValue= "[Choose One]"/>
							</td>
							<!-- PMT-42618 -->
						
						<td class="RightTableCaption" width="20%" align="right"><fmtSetMasterSetup:message key="LBL_LOTTRACK"/>:</td>
						<td align="left">&nbsp;<input type="checkbox"  name="Chk_Lottrack"<%=strLotTrackFlag%> /></td>
						
						<!-- PMT-42618 -->
						</tr>
						
						
						<tr><td colspan="4" height="1" bgcolor="#CCCCCC"></td></tr>
						<tr class="oddshade" >
							<td class="RightTableCaption"  HEIGHT="25" align="right">&nbsp;<fmtSetMasterSetup:message key="LBL_TYPE"/>:</td>
							<td colspan="3">&nbsp;<gmjsp:dropdown controlName="Cbo_Type" onChange="fnEnableConsngFl();" seletedValue="<%=strType%>" disabled="<%=strSystemStatusFl%>"
									value="<%=alSetType%>" codeId="CODEID"  codeName="CODENM" defaultValue= "[Choose One]"/>
							</td>
							
						</tr>						
						<tr><td colspan="4" height="1" bgcolor="#CCCCCC"></td></tr>
						<tr class="evenshade">
							<td class="RightTableCaption"  align="right" height="25"><fmtSetMasterSetup:message key="LBL_REV_LEVEL"/>:</td>
							<td colspan="3">&nbsp;<input type="text" size="10" value="<%=strRevLevel%>" <%=strQltyDisabled%> name="Txt_RevLvl" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
							</td>
						</tr>								
				   		<tr><td colspan="4" height="1" bgcolor="#CCCCCC"></td></tr>
						<tr class="oddshade" >
							<td class="RightTableCaption"  align="right" height="25"><fmtSetMasterSetup:message key="LBL_CORE_SET"/>:</td>
							<td colspan="3">&nbsp;<input type="checkbox" name="coreSetFl" <%=strCoreSetFlagStatus%> <%=strSetStatusFlag%> />
							</td>
						</tr>
						<tr><td colspan="4" height="1" bgcolor="#CCCCCC"></td></tr>
						<tr class="evenshade">
							<td class="RightTableCaption"  align="right" height="25"><fmtSetMasterSetup:message key="LBL_INSPECTION_SHEET"/>:</td>
							<td colspan="3">&nbsp;<input type="file" name="uploadfile" size="50" >
							</td>
					
						</tr>						
				   		<tr><td colspan="4" height="1" bgcolor="#CCCCCC"></td></tr>
						<tr class="oddshade" >
							<td class="RightTableCaption" align="right" height="25"></td>
							<td colspan="3"><%-- &nbsp;<gmjsp:dropdown controlName="cbo_inspection_sheets"  seletedValue="<%=strInspectionSheet%>" 	
										value="<%=alFileList%>" codeId = "NAME"  width="250" codeName = "NAME" defaultValue= "[Choose One]"/> --%>
							<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
								<jsp:param name="CONTROL_NAME" value="cbo_inspection_sheets" />
								<jsp:param name="METHOD_LOAD" value="loadInspectionSheetList&searchType=PrefixSearch" />
								<jsp:param name="WIDTH" value="485" />
								<jsp:param name="CSS_CLASS" value="search" />
								<jsp:param name="CONTROL_NM_VALUE" value="<%=strInspectionSheet %>" /> 
								<jsp:param name="CONTROL_ID_VALUE" value="<%=strInspectionSheet %>" />
								<jsp:param name="SHOW_DATA" value="5" />
							</jsp:include>
							</td>
						</tr>	
						<tr><td colspan="4" height="1" class="LLine"></td></tr>				
					</table>
				</td>
			</tr>	
			<tr><td class="ELine" colspan="2" height="1"></td></tr>								
				<tr>
					<td colspan="2">
	                    <jsp:include page="/common/GmIncludeLog.jsp" >
						<jsp:param name="LogType" value="" />
						<jsp:param name="LogMode" value="Edit" />
						<jsp:param name="Mandatory" value="yes" />
						</jsp:include>	
					</td>
				</tr>
<%
		String strDisableBtnVal = strSetStatusFlag;
	if(strDisableBtnVal.equals("disabled"))
	{
		strDisableBtnVal = "true";
	}
%>
			<tr>
						<td colspan="2" height="30" align="center">
							<fmtSetMasterSetup:message key="BTN_SUBMIT" var="varSubmit"/>
							<gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnSubmit(this.form,'Save');" disabled="<%=strDisableBtnVal%>" />
							<fmtSetMasterSetup:message key="BTN_RESET" var="varReset"/>
							&nbsp;&nbsp;<gmjsp:button value="${varReset}" gmClass="button" buttonType="Save" onClick="fnAddNew(this.form);" />
<% if((strSetStatus.equals("20365") || strSetStatus.equals("20366") || strSetStatus.equals("")) && strhAction.equals("Reload")) { 							%>
										<fmtSetMasterSetup:message key="BTN_VOID" var="varVoid"/>
							&nbsp;&nbsp;<gmjsp:button value="${varVoid}" gmClass="button" buttonType="Save" onClick="fnVoidSet(this.form);" />&nbsp;
<% } %>		
<%if(!strSetId.equals("")){ %>	
	<fmtSetMasterSetup:message key="IMG_INSPECT_MAP_REPORT" var="varInspectMapReport"/>			
				<a class="RightText" title="${varInspectMapReport}" href="javaScript:fnSetFileReport(this.form);" ><fmtSetMasterSetup:message key="LBL_INSPECTION_SHEET_MAPPING_REPORT"/></a>	
<%} %>
						</td>
					</tr>
					<tr><td colspan=2 height=1 bgcolor="#666666"></td></tr>
					<tr>
						<td colspan="2"><jsp:include page="/common/GmUpdationHistory.jsp" ></jsp:include></td>
					</tr>
				 </table>			
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
