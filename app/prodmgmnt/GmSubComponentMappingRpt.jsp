 <%
/*******************************************************************************************************
 * File		 		: GmSubComponentMappingRpt.jsp
 * Desc		 		: This is ment for Reporting the Sub Component Part Part Mapping Details
 * Version	 		: 1.0
 * author			: HReddi
*********************************************************************************************************/
%>

<%@ page language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="fmtSubComponentMappingReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\GmSubComponentMappingRpt.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtSubComponentMappingReport:setLocale value="<%=strLocale%>"/>
<fmtSubComponentMappingReport:setBundle basename="properties.labels.prodmgmnt.GmSubComponentMappingRpt"/>
<bean:define id="gridData" name="frmSubComponentMapping" property="strXMLGrid" type="java.lang.String"> </bean:define>
<% 
response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8");
String strWikiTitle = GmCommonClass.getWikiTitle("SUB_COMPONENT_MAPPING_REPORT");
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: Part Price Adjustment Initiate</title>
<style type="text/css">
table.DtTable1050 {
	margin: 0 0 0 0;
	width: 1050px;
	border: 1px solid  #676767;
}
</style>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmSubComponentMapping.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
 <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

<!-- The library provides support for different keyboard commands that let users to navigate through the grid. -->

<script>
var objGridData;
objGridData ='<%=gridData%>';
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form action="/gmSubComponentMapping.do?">
<html:hidden property="strOpt" name="frmSubComponentMapping" />
	<table border="0" class="DtTable950" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="3">&nbsp;<fmtSubComponentMappingReport:message key="LBL_SUBPART_MAPPING"/></td>
			<td  align="right" class="RightDashBoardHeader" width="10%">
				<fmtSubComponentMappingReport:message key="LBL_HELP" var="varHelp"/>
				<img align="right" id='imgEdit' style='cursor:pointer' src='<%=strImagePath%>/help.gif' title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"/>
			</td>
		</tr>
		<tr>
			<td height="25" width="3%" class="RightTableCaption" align="right"><fmtSubComponentMappingReport:message key="LBL_PART_NUMBER"/>:</td>
			<td width="10%">&nbsp;<html:text property="partNum" styleClass="InputArea" name="frmSubComponentMapping" size="15" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/>&nbsp;
			     <gmjsp:dropdown controlName="partLike" SFFormName="frmSubComponentMapping" SFSeletedValue="partLike" SFValue="alLikeOptions" codeId="CODEID" codeName="CODENM" defaultValue= "[Choose One]" />
		    </td>
		    <td height="25" width="4%" class="RightTableCaption" align="right"><fmtSubComponentMappingReport:message key="LBL_SUBCOMPONENT_NUMBER"/>:</td>
		    <fmtSubComponentMappingReport:message key="BTN_LOAD" var="varLoad"/>
			<td width="10%">&nbsp;<html:text property="subComponentNum" styleClass="InputArea" name="frmSubComponentMapping" size="15" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/>&nbsp;
			     <gmjsp:dropdown controlName="subPartLike" SFFormName="frmSubComponentMapping" SFSeletedValue="subPartLike" SFValue="alLikeOptions" codeId="CODEID" codeName="CODENM" defaultValue= "[Choose One]" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<gmjsp:button name="Btn_Load" value="${varLoad}" gmClass="button" buttonType="Load" onClick="fnReload();" style="width: 5em; height: 23px; font-family: Trebuchet MS, arial, sans-serif;font-size : 9pt"/>
		    </td>			
		</tr>		
		<%if(gridData.indexOf("cell") != -1){%>
				<tr>
					<td colspan="4"><div  id="dataGridDiv" style="" height="500px"></div></td>
				</tr>						
			<%}else if(!gridData.equals("")){%>
				<tr><td colspan="4" align="center" class="RightText"><fmtSubComponentMappingReport:message key="LBL_NOTHING_FOUND_DISPLAY"/></td></tr>
			<%}else{%>
				<tr><td colspan="4" align="center" class="RightText"><fmtSubComponentMappingReport:message key="LBL_NO_DATE_AVAILABLE"/></td></tr>
			<%} %>
		<tr><td height="1" colspan="4" class="LLine"></td></tr>
		<%if( gridData.indexOf("cell") != -1) {%>
		<tr>
			<td colspan="4" align="center" height="25">
			    <div class='exportlinks'><fmtSubComponentMappingReport:message key="DIV_EXPORT_OPTIONS"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnDownloadXLS();"> <fmtSubComponentMappingReport:message key="DIV_EXCEL"/> </a></div>
			</td>
		</tr>
		<%}%>			
			
	</table>
<%@ include file="/common/GmFooter.inc"%>
</html:form>
</body>
</html>