<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtSystemSetupContainer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\GmSystemSetupContainer.jsp -->
<fmtSystemSetupContainer:setLocale value="<%=strLocale%>"/>
<fmtSystemSetupContainer:setBundle basename="properties.labels.prodmgmnt.GmSystemSetupContainer"/>
<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("SYSTEM_SETUP");
%>
<bean:define id="setId" name="frmSystemDetails" property="setId" scope="request" type="java.lang.String"></bean:define>
<bean:define id="tabToLoad" name="frmSystemDetails" property="tabToLoad" type="java.lang.String"></bean:define>
<bean:define id="mapcount" name="frmSystemDetails" property="mapcount" type="java.lang.String"></bean:define>
<%
String strSetupTabClass ="selected";
String strMappingTabClass ="";
//while submitting the Mapping Tab we need to display the Mapping screen after reloading.
if(tabToLoad.equals("mapping")){
	 strSetupTabClass ="";
	 strMappingTabClass ="selected";
}

%>
<html>
<head>
<title>System Setup</title>

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/prodmgmnt/GmSystemDetail.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/prodmgmnt/GmSystemMapping.js"></script>
<script language="javascript" type="text/javascript" src="<%=strExtWebPath%>/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
	function fnHtmlEditor(){
		tinyMCE.init({
			mode : "specific_textareas",
		    editor_selector : "HtmlTextArea",
			theme : "advanced" ,
			theme_advanced_path : false,
			height : "300",
			width: "500" 
		});
}
	var mapCount = '<%=mapcount%>';   /*  Need to validation message show in PMT-37488 - System Publish for Marketing Collateral and Product Catalog  */
</script> 
<!-- PC-3967 - to avoid scroll in the grid -->
 <style type="text/css">
div#ajaxdivcontentarea.contentstyle {
overflow: hidden !important; 
}
</style> 
</head>
<body leftmargin="20" topmargin="10" onload="fnAjaxTabLoad();" > 
<html:form action="/gmSystemSetup.do">
<html:hidden property="hcurrentTab" value="Project_Setup"/>

<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtSystemSetupContainer:message key="LBL_SYSTEM_SETUP"/></td>
			<td align="right" class=RightDashBoardHeader > 	
				<fmtSystemSetupContainer:message key="IMG_ALT_HELP" var="varHelp"/>
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
		</tr>		
		<tr>
			<td class="RightTableCaption" align="right" height="30" width="35%"><font color="red">*</font>&nbsp;<fmtSystemSetupContainer:message key="LBL_SYSTEM_LIST"/>:</td>					
			<td width="65%">&nbsp;<gmjsp:dropdown controlName="setId" onChange="fnfetchSystemDet(this.form);" SFFormName="frmSystemDetails" SFSeletedValue="setId"
				SFValue="alSystemList" codeId="ID"  codeName="NAME"  defaultValue="[Choose One]" width="315"/>
			</td>
		</tr>
		<tr>
			<td colspan="2" height="1" class="LLine" ></td>
		</tr>
</html:form>		
		<tr>
			<td colspan="2">
				<ul id="maintab" class="shadetabs">
				<li onclick="fnUpdateTabIndex(this);"><a href="/gmSystemSetup.do?setId=<bean:write name="frmSystemDetails" property="setId"/>&strOpt=fetch" title="Details" rel="ajaxdivcontentarea" id="system"  class="<%=strSetupTabClass%>"><fmtSystemSetupContainer:message key="LBL_DETAILS"/></a></li>
				<logic:notEqual name="frmSystemDetails" property="setId" value="">
		 				<logic:notEqual name="frmSystemDetails" property="setId" value="0">
							<li ><a href="/gmSystemMapping.do?strOpt=fetch&systemId=<bean:write name="frmSystemDetails" property="setId"/>"
								rel="ajaxcontentarea" id="mapping" title="mapping" class="<%=strMappingTabClass%>"><fmtSystemSetupContainer:message key="LBL_MAPPING"/></a>
							</li>											
							<li><a href="/gmLanguageDetail.do?typeID=103092&refID=<bean:write name="frmSystemDetails" property="setId"/>&strOpt=report"
									rel="#iframe" id="language" title="Language"><fmtSystemSetupContainer:message key="LBL_LANGUAGE"/></a>
							</li>
							<li><a href="/gmUploadDetail.do?refID=<bean:write name="frmSystemDetails" property="setId"/>&strOpt=report&typeID=103092"
									rel="#iframe" id="upload" title="Upload"><fmtSystemSetupContainer:message key="LBL_UPLOADS"/></a>
							</li>
							<li><a href="/gmSystemPublish.do?method=loadSystemReleaseDtls&setGroupType=1600&setId=<bean:write name="frmSystemDetails" property="setId"/>"
								rel="#iframe" id="syscompmapping" title="System Release Mapping"><fmtSystemSetupContainer:message key="LBL_SYSTEM_RELEASE_MAPPING"/></a>
							</li>
						</logic:notEqual>
					</logic:notEqual>
				</ul>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div id="ajaxdivcontentarea" class="contentstyle" style="display:visible; overflow:auto;" >
				</div>
			</td>
		</tr>
	</table>

<%@ include file="/common/GmFooter.inc"%>
</body>
</html>