<%
/**********************************************************************************
 * File		 		: GmPartProjectMap.jsp
 * Desc		 		: This screen is used for the Part-Project Mapping 
 * Version	 		: 1.0
 * author			: Manikandan R
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtPartProjectMap" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmPartProjectMap.jsp -->
<fmtPartProjectMap:setLocale value="<%=strLocale%>"/>
<fmtPartProjectMap:setBundle basename="properties.labels.prodmgmnt.GmPartProjectMap"/>
<bean:define id="alAvailableProject" name="frmPartProjectMapDtls" property="alAvailableProject" type="java.util.ArrayList"></bean:define>
<bean:define id="alAssproject" name="frmPartProjectMapDtls" property="alAssproject" type="java.util.ArrayList"></bean:define>
<bean:define id="alCompany" name="frmPartProjectMapDtls" property="alCompany" type="java.util.ArrayList"></bean:define>
<bean:define id="gridXmlData" name="frmPartProjectMapDtls" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="companyId" name="frmPartProjectMapDtls" property="companyId" type="java.lang.String"> </bean:define>
<bean:define id="editDisable" name="frmPartProjectMapDtls" property="editDisable" type="java.lang.String"> </bean:define>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ page import="com.globus.common.servlets.GmServlet"%>


<% 
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
String strWikiTitle = GmCommonClass.getWikiTitle("PART_PROJECT_MAPPING");

%>

<HTML>
<HEAD>

<TITLE>Globus Medical: Part-Project Mapping</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmPartProjectMap.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>	
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxCombo.js"></script>

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<script>
var objGridData;
objGridData = '<%=gridXmlData%>';
var gridObj;
var assocprojectsize =<%=alAssproject.size()%>; 

</script>
</HEAD>
<BODY  onload="fnOnPageLoad();" >
<html:form action="/gmPartProjectMapDtls.do">
<html:hidden property="hassociatedprojects" name="frmPartProjectMapDtls"/>
<html:hidden property="hvoidString" name="frmPartProjectMapDtls"/>
<html:hidden property="strOpt" name="frmPartProjectMapDtls"/>
<html:hidden property="partnumber" name="frmPartProjectMapDtls"/>



<table border="0"   class="DtTable700" width="100%" cellspacing="0" cellpadding="0">
		
	<tr><td class="LLine" height="1" colspan="5"></td></tr>	
	<tr>
		<td height="25" class="RightDashBoardHeader" colspan="5">&nbsp;<fmtPartProjectMap:message key="LBL_PART_PROJECT_MAPPING"/></td>
			<td  height="25" class="RightDashBoardHeader">
			<fmtPartProjectMap:message key="IMG_ALT_HELP" var="varHelp"/>
					<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
	</tr>
	<tr colspan ="4" class=evenshade>
	<td class="RightTableCaption" HEIGHT="30" align="right">
	<fmtPartProjectMap:message key="LBL_COMPANY" var="varCompany"/>
						<gmjsp:label type="RegularText" SFLblControlName="${varCompany}:" td="false" /></td>&nbsp;
				<td >&nbsp;&nbsp;<gmjsp:dropdown controlName="companyId" SFFormName="frmPartProjectMapDtls" SFSeletedValue="companyId"
						SFValue="alCompany" codeId = "COMPANYID"  codeName = "COMPANYNM" defaultValue= "[Choose One]" tabIndex="1" width="300" onChange="fnChangeProjectList()"/>
						</td>
	</tr>
	<tr colspan ="4" class="oddshade" id="Row_AssPrjId">
	<fmtPartProjectMap:message key="LBL_ASSOC_PROJECT_LIST" var="varAssocProjectList"/>
		<gmjsp:label type="BoldText"  SFLblControlName="${varAssocProjectList}:" td="true"/>
				<td class="RightText">&nbsp;
					<gmjsp:autolist  comboType="DhtmlXCombo"  controlName="Cbo_AssocProjectId"  value="<%=alAvailableProject%>" codeId="PROJECTID" codeName="PROJECTNAME" 
				        		defaultValue="[Choose One]" tabIndex="2" width="350" onChange="fnProjectMap"/>
				 </td>							
	</tr> 
	<tr><td class="LLine" height="1" colspan="4"></td></tr>
	<tr>
		<td colspan="4">
			<div  id="ASSOCPROJECTDATA" style="height: 400px;width: 700px"></div>
		</td>
	</tr>
	<tr>
		<td colspan="4" height="40" align="center">
	   		<div colspan="4" id="divHideButton">
	   		<fmtPartProjectMap:message key="BTN_SUBMIT" var="varSubmit"/>
			<gmjsp:button style="width: 5em"  value="${varSubmit}" disabled="<%=editDisable %>" name="Btn_Submit" buttonType="Save"  gmClass="button" onClick="fnSubmitAss();" />&nbsp;
			</div>
		</td>
	</tr>				   	
</table>	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
