<%@ include file="/common/GmHeader.inc"%>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib prefix="fmtSystemMapping" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\GmSystemMapping.jsp -->
<fmtSystemMapping:setLocale value="<%=strLocale%>"/>
<fmtSystemMapping:setBundle basename="properties.labels.prodmgmnt.GmSystemMapping"/>
<bean:define id="alSetType" name="frmSystemMapping" property="alSetType" type="java.util.ArrayList"></bean:define>
<bean:define id="alSetShareType" name="frmSystemMapping" property="alSetShareType" type="java.util.ArrayList"></bean:define>
<bean:define id="alSetMapDtls" name="frmSystemMapping" property="alSetMapDtls" type="java.util.ArrayList"></bean:define>
<bean:define id="mapAccessFlag" name="frmSystemMapping" property="mapAccessFlag" type="java.lang.String"></bean:define>
<%
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.prodmgmnt.GmSystemMapping", strSessCompanyLocale);
String strlblChooseOne =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CHOOSE_ONE"));
String strlblYes =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_YES"));
String strlblNo =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_NO"));
String strlblCreateSystem =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CREATE_SYSTEM"));
String strlblSubmit =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SUBMIT"));
int setTypelen = alSetType.size();
int setShareTypelen = alSetShareType.size();
int alSetMapDtlslen = alSetMapDtls.size();
String strSelectStart = "<select>";
String strSelectEnd = "</select>";
String strChooseOne = "<option value=0>"+strlblChooseOne+"</option>";
String strCodeId="";
String strCodeName=""; 
String strTypeOptionString="";
String strShareOptionString="";
String strBaselineOptionString="";
if(setTypelen!=0){	
	for (int i=0; i<setTypelen; i++){
		HashMap hmSetType = (HashMap)alSetType.get(i);
		
		strCodeId = GmCommonClass.parseNull((String)hmSetType.get("CODEID"));
		strCodeName = GmCommonClass.parseNull((String)hmSetType.get("CODENM"));
		
		strTypeOptionString += " <option value="+strCodeId+">"+strCodeName+"</option> ";
	}
	strTypeOptionString = strChooseOne + strTypeOptionString;		
}

if(setShareTypelen!=0){	
	for (int i=0; i<setShareTypelen; i++)	{
		HashMap hmShareType = (HashMap)alSetShareType.get(i);
		
		strCodeId = GmCommonClass.parseNull((String)hmShareType.get("CODEID"));
		strCodeName = GmCommonClass.parseNull((String)hmShareType.get("CODENM"));
		
		strShareOptionString += " <option value="+strCodeId+">"+strCodeName+"</option> ";
	}
	strShareOptionString = strChooseOne + strShareOptionString;
}

strBaselineOptionString = strChooseOne+" <option value=Y>"+strlblYes+"</option><option value=N>"+strlblNo+"</option> ";
int intTRCount = 2;
String strBtnNm="";
if(alSetMapDtlslen ==0){
	strBtnNm =strlblCreateSystem;
}else{
	strBtnNm =strlblSubmit;
}
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>System Mapping</title>
</head>
<body leftmargin="20" topmargin="10">
<html:form action="/gmSystemMapping.do">
<html:hidden property="strOpt"/>
<html:hidden property="systemId"/>	
<html:hidden property="gridData"/>
<html:hidden property="haction" value=""/>

<html:hidden property="stdsetidsInputStr"/>	
<html:hidden property="addlsetidsInputStr"/>	
<html:hidden property="loanerstdsetidsInputStr"/>	
<html:hidden property="loaneraddlsetidsInputStr"/>	
	
<input type="hidden" name="hRowCnt" value="<%=intTRCount%>"/>
<input type="hidden" name="setMapDtlssize" value="<%=alSetMapDtlslen%>"/>


<table border="0" width="100%" cellspacing="0" cellpadding="0" height="100">
	<tr>
		<td colspan="2" class="LLine" height="1"></td>
	</tr>
	 <tr>
		 <td colspan="2" width="100%" height="100" valign="top">
			<table border="0" width="100%" cellspacing="0" cellpadding="0" id="SystemMapping">
			<THEAD>
				<tr  height="25" class="ShadeRightTableCaption">
					<td height="25" colspan="1" width="10"></td>
					<td height="25" colspan="2" >&nbsp;&nbsp;&nbsp;<font color="red">*</font><fmtSystemMapping:message key="LBL_SET_ID"/></td>
					<td height="25" colspan="1" >&nbsp;<font color="red">*</font><fmtSystemMapping:message key="LBL_SET_TYPE"/></td>
					<td height="25" colspan="1" >&nbsp;<font color="red">*</font><fmtSystemMapping:message key="LBL_BASELINE_SET"/></td>
					<td height="25" colspan="1" >&nbsp;<fmtSystemMapping:message key="LBL_SHARED"/></td>
				</tr>
			<THEAD>
			<TBODY>
				<tr>
				  <td colspan="6" align="center" height="1"> </td>
				</tr>
			<% for (int i=0; i<=intTRCount;i++ ){
				String setType="setType"+i;
				String setId="setId"+i;
				String setShared="setShared"+i;
				String setSetSearch="javascript:fnGetSetStatus("+i+",this.value);";
				String setSetBaseValid="javascript:fnBaselineValidate("+i+");";
			%>
			
				<tr >
					<fmtSystemMapping:message key="LBL_REMOVE" var="varRemove"/>
				    <td width="5%" align="right"><a  href="javascript:fnRemoveItem('<%=i%>');" tabindex="-1"><img border="0" Alt='Remove' title='${varRemove}' valign="left" src="<%=strImagePath%>/btn_remove.gif" height="10" width="9"></a></td>
					<td width="20%" align="center"><html:text property="<%=setId%>" value="" size="10" styleClass="InputArea" onblur="<%=setSetSearch%>" /><input type="hidden" name="hStatus<%=i%>" value=""></td>
					<fmtSystemMapping:message key="LBL_SET_AVAILABLE" var="varSetAvailable"/>
 					<td width="7%" align="left"><span id="validSetSuc<%=i%>" style="vertical-align:top; display: none;">&nbsp;<img tabindex="-1" height=16 width=19 title="${varSetAvailable}" src=<%=strImagePath%>/success.gif></img></span><span id="validSetErr<%=i%>" style="vertical-align:top; display: none;">&nbsp;<fmtSystemMapping:message key="LBL_SET_NOT_AVAILABLE" var="varSetNotAvailable"/><img tabindex="-1"  title="${varSetNotAvailable}" height=13 width=13 src=<%=strImagePath%>/error.gif></img></span>		</td>				
					<td width="23%"><gmjsp:dropdown controlName="<%=setType%>" SFFormName="frmSystemMapping" SFSeletedValue="setType" SFValue="alSetType"  codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]" onChange="<%=setSetBaseValid%>"/></td>
					<td width="20%"><select name="setBaseLine<%=i%>" id="" class="RightText" onchange="javascript:fnBaselineValidate('<%=i%>');"><option value=0 ><fmtSystemMapping:message key="LBL_CHOOSE_ONE"/></option><option  value="Y"><fmtSystemMapping:message key="LBL_YES"/></option><option value="N"><fmtSystemMapping:message key="LBL_NO"/></option></select>&nbsp;	</td>
					<td width="25%"><gmjsp:dropdown controlName="<%=setShared%>" SFFormName="frmSystemMapping" SFSeletedValue="setShared" SFValue="alSetShareType"  codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"/></td>
				</tr>
			<%} %>
			</TBODY>
			</table>
			</td></tr>	
			<tr>
				<fmtSystemMapping:message key="LBL_ADD" var="varAdd"/>
				<td height="25" colspan="6" class="RightText" id="addrow"><u><a  href="javascript:fnAddRow('this.form','<%=strTypeOptionString%>','<%=strBaselineOptionString%>','<%=strShareOptionString%>');" tabindex="-1"><img border="0" Alt='${varAdd}' valign="left" src="<%=strImagePath%>/plus.gif" height="10" width="9"><fmtSystemMapping:message key="LBL_ADD_ROW"/></a></u></td>
			</tr>
			<tr>
				<td colspan="6"> 
					<jsp:include page="/common/GmIncludeLog.jsp" >
					<jsp:param name="FORMNAME" value="frmSystemMapping" />
					<jsp:param name="ALNAME" value="alLogReasons" />
					<jsp:param name="LogMode" value="Edit" />
					<jsp:param name="Mandatory" value="yes" />
					</jsp:include>
				</td>
			</tr>
			<tr>
				  <td colspan="6" align="center" height="4">&nbsp; </td>
			</tr>
			<tr>
				  <td colspan="6" align="center" height="24">&nbsp;
					  <table colspan="3" align="center" height="24">
						  <tr>
						  	<td align="center">
						  	<logic:equal name="frmSystemMapping" property="mapAccessFlag" value="Y"> 
						  	
							  	<gmjsp:button  value="<%=strBtnNm%>" gmClass="button" buttonType="Save" onClick="fnSubmitMapping(this.form);"/>&nbsp;&nbsp;
							  	<fmtSystemMapping:message key="BTN_RESET" var="varReset"/>
							  	<gmjsp:button  value="${varReset}" gmClass="button" buttonType="Save" onClick="fnResetMapping(this.form);"/>&nbsp;&nbsp;
							  	<fmtSystemMapping:message key="BTN_VOID" var="varVoid"/>
			                  	<gmjsp:button disabled="true" controlId="btn_void" value="${varVoid}" buttonType="Save" gmClass="button" onClick="fnVoidMapping(this.form);"/>&nbsp;&nbsp;
		                  	</logic:equal> 
		                  	</td>
		                  </tr>
	                  </table>
				  </td>
			</tr>
			<tr>
				  <td colspan="6" align="center" height="4">&nbsp; </td>
			</tr>	
			<tr> 
		          <td colspan="6">
    					<jsp:include  page="/prodmgmnt/GmIncludeSystemDetails.jsp"></jsp:include>		            	
				  </td>
	    	</tr>
		
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>