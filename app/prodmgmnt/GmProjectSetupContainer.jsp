<!-- prodmgmnt\GmProjectSetupContainer.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtProjectSetupContainer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtProjectSetupContainer:setLocale value="<%=strLocale%>"/>
<fmtProjectSetupContainer:setBundle basename="properties.labels.prodmgmnt.GmProjectSetupContainer"/>

<% 
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
	String strWikiTitle = GmCommonClass.getWikiTitle("PROJECT_SETUP");
%>
<bean:define id="prjId" name="frmProjectDetails" property="prjId" scope="request" type="java.lang.String"></bean:define>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Project Setup</title>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmProject.js"></script> 
<script language="javascript" type="text/javascript" src="<%=strExtWebPath%>/tiny_mce/tiny_mce.js"></script>

<script language="javascript" type="text/javascript">
	function fnHtmlEditor(){
	tinyMCE.init({
		mode : "specific_textareas",
	    editor_selector : "HtmlTextArea",
		theme : "advanced" ,
		theme_advanced_path : false,
		height : "300",
		width: "500" 
	});
}
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnAjaxTabLoad();"> 
<html:form action="/gmPDProjectSetup.do?method=loadProjectInformation">
<html:hidden property="strOpt" value=""/>
<html:hidden property="hcurrentTab" value="Project_Setup"/>

<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtProjectSetupContainer:message key="LBL_PROJECT_SETUP"/></td>
			<td align="right" class=RightDashBoardHeader ><fmtProjectSetupContainer:message key="IMG_ALT_HELP" var = "varHelp"/>	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
		</tr>		
		<tr>
			<td class="RightTableCaption" align="right" height="30" width="30%"><font color="red">*</font>&nbsp;<fmtProjectSetupContainer:message key="LBL_PROJECT_LIST"/>:</td>					
			<td width="60%">&nbsp;<gmjsp:dropdown controlName="prjId" onChange="fnPrjFetch(this.form);" SFFormName="frmProjectDetails" SFSeletedValue="prjId"
				SFValue="alPrjList" codeId="ID"  codeName="NAME"  defaultValue="[Choose One]" width="315"/>
			</td>
		</tr>		
</html:form>		
		<tr>
			<td colspan="2">
				<ul id="maintab" class="shadetabs">
					<li onclick="fnUpdateTabIndex(this);"><a href="/gmPDProjectSetup.do?method=loadProjectInformation&prjId=<bean:write name="frmProjectDetails" property="prjId"/>&prjType=<bean:write name="frmProjectDetails" property="prjType"/>
					&strOpt=fetch" title="Project" rel="ajaxdivcontentarea" name="Project"><fmtProjectSetupContainer:message key="LBL_DETAILS"/></a></li>
					<logic:notEqual name="frmProjectDetails" property="prjId" value="">
		 				<logic:notEqual name="frmProjectDetails" property="prjId" value="0">
							<li><a href="/gmPDStaffingDetail.do?method=loadStaffingDetailsSetup&prjId=<bean:write name="frmProjectDetails" property="prjId"/>"
								rel="ajaxcontentarea" id="staffdtl" title="Staffing Details"><fmtProjectSetupContainer:message key="LBL_STAFFING_DETAILS"/></a>
							</li>							
							<li><a href="/gmLanguageDetail.do?typeID=103093&refID=<bean:write name="frmProjectDetails" property="prjId"/>&strOpt=report"
									rel="#iframe" id="language" title="Language"><fmtProjectSetupContainer:message key="LBL_LANGUAGE"/></a>
							</li>
							<li><a href="/gmUploadDetail.do?typeID=103093&refID=<bean:write name="frmProjectDetails" property="prjId"/>&strOpt=report&typeID=103093"
									rel="#iframe" id="upload" title="Upload"><fmtProjectSetupContainer:message key="LBL_UPLOADS"/></a>
							</li>
							<li><a href="/gmPDProjectSetup.do?method=loadProjectCompanyMapSetup&prjId=<bean:write name="frmProjectDetails" property="prjId"/>"
								rel="#iframe" id="PrjCompSetup" title="Project-Company Mapping Setup"><fmtProjectSetupContainer:message key="LBL_PROJECT_COMPANY_MAP"/></a>
							</li>
							<li><a href="/gmPDProjectSetup.do?method=loadProjectInformation&strOpt=projectCompanyMap&prjId=<bean:write name="frmProjectDetails" property="prjId"/>"
								rel="#iframe" id="PrjCompMap" title="Project-Company Mapping"><fmtProjectSetupContainer:message key="LBL_PROJECT"/></a>
							</li>	 
						</logic:notEqual>
					</logic:notEqual>
				</ul>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div id="ajaxdivcontentarea" class="contentstyle" style="display:visible; overflow:auto;" >
				</div>
			</td>
		</tr>
	</table>

<%@ include file="/common/GmFooter.inc"%>
</body>
</html>