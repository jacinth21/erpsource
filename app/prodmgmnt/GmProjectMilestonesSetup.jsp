<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- prodmgmnt\GmProjectMilestonesSetup.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtProjectMilestonesSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtProjectMilestonesSetup:setLocale value="<%=strLocale%>"/>
<fmtProjectMilestonesSetup:setBundle basename="properties.labels.prodmgmnt.GmProjectMilestonesSetup/"/>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<bean:define id="alRequest" name="frmPdPrjMilestoneSetup" property="alMilestoneList" type="java.util.ArrayList"></bean:define>
<%
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Milestone Setup</title>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
</head>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmProject.js"></script> 


<body leftmargin="0" topmargin="0">
<html:form action="/gmPDPrjMilestoneSetup.do">
<html:hidden property="strOpt"/>
<html:hidden property="prjId"/>
<html:hidden property="hinputString" value=""/>


<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
	 <tr>
		 <td colspan="2" width="100%" height="100" valign="top">
			<table border="0" width="100%" cellspacing="0" cellpadding="0"> 
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<%if (alRequest.size() > 0) 
					{
					%>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>					
					<tr class="Shade"> 
			            <td colspan="2">
			                
                            <display:table name="requestScope.frmPdPrjMilestoneSetup.alMilestoneList" sort="list" requestURI="/gmPDPrjMilestoneSetup.do" excludedParams="haction" class="gmits" id="currentRowObject"  decorator="com.globus.prodmgmnt.displaytag.DTProjectMilestonesWrapper">
				            <fmtProjectMilestonesSetup:message key="LBL_GROUP" var="varGroup"/><display:column property="GROUPNM" title="${varGroup}" class="alignleft" style="width:120px" sortable="true" /> 						
							<fmtProjectMilestonesSetup:message key="LBL_TASK_MILESTONE" var="varTaskMilestone"/><display:column property="MILESTONENM" title="${varTaskMilestone}" class="alignleft" style="width:190px" sortable="true"/> 
							<fmtProjectMilestonesSetup:message key="LBL_COMMENTS" var="varComments"/><display:column property="IMG" title="${varComments}" class="aligncenter" style="width:60px" sortable="true"/>
							<fmtProjectMilestonesSetup:message key="LBL_PROJECTED_DATE" var="varProjectedDate"/><display:column property="PRJDATE" title="${varProjectedDate}" class="alignleft" style="width:130px" />
							<fmtProjectMilestonesSetup:message key="LBL_ACTUAL_DATE" var="varACtualDate"/><display:column property="ACTDATE" title="${varACtualDate}" class="alignleft" style="width:130px" />
							</display:table>	
						</td>
		    	   </tr>		    	   
		    	   <tr><td colspan="2" class="LLine" height="1"></td></tr>		    	   
					<tr>
						<td colspan="2" align="center" height="24">&nbsp;
							<fmtProjectMilestonesSetup:message key="BTN_RESET" var="varReset"/><gmjsp:button value="${varReset}" gmClass="button" buttonType="Save" onClick="fnMileStoneReset(this.form);"/>&nbsp;&nbsp;
	                    	<fmtProjectMilestonesSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnMilestoneSubmit();"/>&nbsp;&nbsp;                   	
						</td>
					</tr>
					<%} else
					{
			  		%> 
			  		<tr><td colspan="2" class="LLine" height="1"></td></tr> 		
					<tr class="Shade">
			  		<td height="20"><fmtProjectMilestonesSetup:message key="LBL_NO_TASKS"/></td><td></td>
			  		</tr>
			  		<%} %>
			</table>
		</td>		
	</tr>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>