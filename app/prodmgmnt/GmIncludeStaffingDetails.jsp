<%
/**********************************************************************************************
 * File		 		: GmIncludeStaffingDetails.jsp
 * Desc		 		: JSP file to display the module details
 * Version	 		: 1.1
 * Author			: Ritesh Shah
 * Updated By		: Satyajit Thadeshwar
/***********************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtIncludeStaffingDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\GmIncludeStaffingDetails.jsp -->
<fmtIncludeStaffingDetails:setLocale value="<%=strLocale%>"/>
<fmtIncludeStaffingDetails:setBundle basename="properties.labels.prodmgmnt.GmIncludeStaffingDetails"/>
<%

String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
%>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmProject.js"></script>

<% 
	String displayType = GmCommonClass.parseNull(request.getParameter("TYPE"));
	String strMedia = displayType.equalsIgnoreCase("Setup") ? "HTML" : "FALSE";
%>
<display:table name="requestScope.frmStaffingDetails.alResult" requestURI="/gmPDStaffingDetail.do" excludedParams="haction" class="gmits" id="currentRowObject"  
	decorator="com.globus.prodmgmnt.displaytag.DTStaffingCountWrapper">
	<display:column property="ID" title="" class="alignleft" style="width:40px" media="<%=strMedia%>"/>
    <fmtIncludeStaffingDetails:message key="LBL_COMMENTS" var="varComments"/><display:column property="IMG" title="${varComments}" class="aligncenter" style="width:60px" />			            
    <fmtIncludeStaffingDetails:message key="LBL_MEMBER" var="varMember"/><display:column property="PARTYNM" title="${varMember}" class="alignleft" style="width:120px" /> 
	<fmtIncludeStaffingDetails:message key="LBL_ROLE" var="varRole"/><display:column property="ROLENM" title="${varRole}" class="alignleft" style="width:120px" /> 
	<fmtIncludeStaffingDetails:message key="LBL_STATUS" var="varStatus"/><display:column property="STATUSNM" title="${varStatus}" class="alignleft" style="width:120px"/>
</display:table>