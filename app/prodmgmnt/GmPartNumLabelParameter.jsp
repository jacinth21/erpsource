 <%
/**********************************************************************************
 * File		 		: GmPartNumLabelParameter.jsp
 * Desc		 		: This screen is used for save the Part Number Label Parameters
 * Version	 		: 1.0
 * author			: Bala
************************************************************************************/
%>
<!-- prodmgmnt\GmPartNumLabelParameter.jsp -->
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtGmPartNumLabelParameter" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtGmPartNumLabelParameter:setLocale value="<%=strLocale%>"/>
<fmtGmPartNumLabelParameter:setBundle basename="properties.labels.prodmgmnt.GmPartNumLabelParameter"/>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<bean:define id="hmAttrVal" name="frmPartAttributeSetup" property="hmAttrVal" type="java.util.HashMap" />
<bean:define id="editDisable" name="frmPartAttributeSetup" property="editDisable" type="java.lang.String"> </bean:define>
<bean:define id="alCPCList" name="frmPartAttributeSetup" property="alCPCList" type="java.util.ArrayList"> </bean:define>
<bean:define id="alProSpecType" name="frmPartAttributeSetup" property="alProSpecType" type="java.util.ArrayList"> </bean:define>
<%
	String strWikiTitle = "";
	HashMap hmLblParm = new HashMap();

	hmLblParm = GmCommonClass.parseNullHashMap((HashMap) hmAttrVal);	
	
	String strGeneralSpec = GmCommonClass.parseNull((String)hmLblParm.get("GENERALSPEC"));
	String strSize = GmCommonClass.parseNull((String)hmLblParm.get("LBLSIZE"));
	String strBoxLabel = GmCommonClass.parseNull((String)hmLblParm.get("BOXLABEL"));
	String strSizeFormat =  GmCommonClass.parseNull((String) hmLblParm.get("SIZEFMT"));
	String strPatientLabel = GmCommonClass.parseNull((String)hmLblParm.get("PRINTLABEL"));
	String strPackageLabel = GmCommonClass.parseNull((String)hmLblParm.get("PKGLABEL"));
	//String strCPCPatientNo = GmCommonClass.parseNull((String)hmLblParm.get("CPCPATIENTNO"));
	//String strCPCPackageNo =  GmCommonClass.parseNull((String) hmLblParm.get("CPCPACKAGENO"));
	String strSample = GmCommonClass.parseNull((String)hmLblParm.get("SAMPLE")).equals("Y")?"checked=checked":"";	
	//String strCPCLogoPath = GmCommonClass.parseNull((String)hmLblParm.get("CPCLOGOPATH"));		
	String strMaterialSpecLabel = GmCommonClass.parseNull((String)hmLblParm.get("MATERIALSPECLABEL"));
	String strPartDrawLabel = GmCommonClass.parseNull((String)hmLblParm.get("PARTDRAWLABEL"));
%>
<html>
<head>
<title>Globus Medical: Label Parameter</title>
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="javascript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
</head>

<body leftmargin="20" topmargin="10">
	<html:form action="/gmPartAttribute.do">
		<html:hidden property="strOpt" name="frmPartAttributeSetup" value="" />
		<html:hidden property="partnumber" name="frmPartAttributeSetup" />
		<html:hidden property="haction" name="frmPartAttributeSetup" />
		<html:hidden property="hLabelAttribute" name="frmPartAttributeSetup" />	
		
		<table class="DtTable950" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="RightDashBoardHeader" colspan="3"><fmtGmPartNumLabelParameter:message key="LBL_LABEL_PARAMETERS"/>
					</td>
				<td align="right" class=RightDashBoardHeader><fmtGmPartNumLabelParameter:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'
					height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="4" height="1"></td>
			</tr>
			<tr class="oddshade">
				<td class="RightTableCaption" height="25" align="right"> <font color="red">*</font> <fmtGmPartNumLabelParameter:message key="LBL_GENERIC_SPECIFICATION"/>:</td>
				<td colspan="3">&nbsp;<html:text property="genericspec" name="frmPartAttributeSetup" size="120" styleClass="InputArea" maxlength="255" onfocus="changeBgColor(this,'#AACCE8');"  onblur="changeBgColor(this,'#ffffff');" value="<%=strGeneralSpec%>" /></td>				 				
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="evenshade">
				<td class="RightTableCaption" height="25" align="right"> <font color="red">*</font><fmtGmPartNumLabelParameter:message key="LBL_SIZE"/> :</td>
				<td>&nbsp;<html:text property="size" name="frmPartAttributeSetup" size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" value="<%=strSize%>"/></td>
				<td class="RightTableCaption" height="25" align="right"><font color="red">*</font> <fmtGmPartNumLabelParameter:message key="LBL_SIZE_FORMAT"/>:</td>
				<td>&nbsp;<html:text property="sizefmt" name="frmPartAttributeSetup" size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" value="<%=strSizeFormat%>"/></td>				
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="oddshade">
				<td class="RightTableCaption" height="25" align="right"><fmtGmPartNumLabelParameter:message key="LBL_BOX_LABEL"/>:</td>
				<td>&nbsp;<html:text property="boxlabel" name="frmPartAttributeSetup" size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" value="<%=strBoxLabel%>"/></td>
				<td class="RightTableCaption" height="25" align="right"><fmtGmPartNumLabelParameter:message key="LBL_PATIENT_LABEL"/>:</td>
				<td>&nbsp;<html:text property="patientlabel" name="frmPartAttributeSetup" size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" value="<%=strPatientLabel%>"/></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="evenshade">
				<td class="RightTableCaption" height="25" align="right"><fmtGmPartNumLabelParameter:message key="LBL_PACKAGE_LABEL"/>:</td>
				<td>&nbsp;<html:text property="pkglabel" name="frmPartAttributeSetup" size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" value="<%=strPackageLabel%>"/></td>
				<%-- <td class="RightTableCaption" height="25" align="right">CPC Patient No:</td>
				<td>&nbsp;<html:text property="cpcpatientno" name="frmPartAttributeSetup" size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" value="<%=strCPCPatientNo%>"/></td> --%>
				<td class="RightTableCaption" height="25" align="right"> <fmtGmPartNumLabelParameter:message key="LBL_SAMPLE"/>:</td>
				<td>&nbsp;<input type="checkbox" id="sample" <%=strSample%>>
				</td>								
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="oddshade">
				<td class="RightTableCaption" height="25" align="right"><fmtGmPartNumLabelParameter:message key="LBL_MATERIAL_SPECIFICATION"/>:</td>
				<td>&nbsp;<html:text property="matlabel" name="frmPartAttributeSetup" size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" value="<%=strMaterialSpecLabel%>"/></td>
				<td class="RightTableCaption" height="25" align="right"><fmtGmPartNumLabelParameter:message key="LBL_PART_DRAWING"/>:</td>
				<td>&nbsp;<html:text property="partdrawlabel" name="frmPartAttributeSetup" size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" value="<%=strPartDrawLabel%>"/></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="evenshade">
				<td class="RightTableCaption" align="right" height="25"><fmtGmPartNumLabelParameter:message key="LBL_PROCESSING"/>:</td>
				<td>&nbsp;<gmjsp:dropdown controlName="proSpecType"  SFFormName="frmPartAttributeSetup" SFSeletedValue="proSpecType" SFValue="alProSpecType" codeId = "CODEID" codeName = "CODENM" defaultValue= "[Choose One]"/></td>
				<td class="RightTableCaption" align="right" height="25"><fmtGmPartNumLabelParameter:message key="LBL_CONTRACT_PROCESSING_CLIENT"/>:</td>
				<td>&nbsp;<gmjsp:dropdown controlName="cpcID" SFFormName="frmPartAttributeSetup" SFSeletedValue="cpcID" SFValue="alCPCList" codeId="ID" codeName="NAME" defaultValue="[Choose One]" /></td>
			</tr>
			<!-- Removing CPC related fields for BBATKT-30-->
			<%-- <tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="oddshade">
				<td class="RightTableCaption" height="25" align="right">CPC Package No:</td>
				<td>&nbsp;<html:text property="cpcpackageno" name="frmPartAttributeSetup" size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" value="<%=strCPCPackageNo%>"/></td>
				<td class="RightTableCaption" height="25" align="right"> Sample:</td>
				<td>&nbsp;<input type="checkbox" id="sample" <%=strSample%>>
				</td>				
			</tr>
				<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="evenshade">
				<td class="RightTableCaption" height="25" align="right">CPC Logo Path:</td>
				<td>&nbsp;<html:text property="cpclogopath" name="frmPartAttributeSetup" size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" value="<%=strCPCLogoPath%>"/></td>								
			</tr> --%>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr>
			<td colspan="4" width="948"> 
				<jsp:include page="/common/GmIncludeLog.jsp" >
					<jsp:param name="LogType" value="" />
					<jsp:param name="LogMode" value="Edit" />
					<jsp:param name="Mandatory" value="yes" />
				</jsp:include>
			</td>
		   </tr>		
		   <tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>	
			<tr height="30">
				<td colspan="4" align="center"><fmtGmPartNumLabelParameter:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" 
						name="btn_Submit" controlId="btn_Submit" gmClass="button"
						onClick="fnLblSubmit(this.form);" buttonType="Save" /></td>
			</tr>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</body>
</html>