<%
/**********************************************************************************
 * File		 		: GmProjectStaffingReport.jsp
 * Desc		 		: Shows the project staffing report
 * Version	 		: 1.0
 * Author			: Satyajit Thadeshwar
 * Last modified by	: Satyajit Thadeshwar
************************************************************************************/
%>
<!-- prodmgmnt\GmProjectStaffingReport.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtProjectStaffingReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtProjectStaffingReport:setLocale value="<%=strLocale%>"/>
<fmtProjectStaffingReport:setBundle basename="properties.labels.prodmgmnt.GmProjectStaffingReport"/>

<%
String strWikiTitle = GmCommonClass.getWikiTitle("PROJECTS_STAFFING_REPORT");
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Project Staffing Report</title>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmProject.js"></script>

</head>
<body leftmargin="20" topmargin="10">
<html:form action="/gmProjectStaffingReport.do">
<html:hidden property="strOpt"/>
<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table class="DtTable725" cellspacing="0" cellpadding="0">
		<tr height="25">
			<td class="RightDashBoardHeader">&nbsp;
<fmtProjectStaffingReport:message key="LBL_PROJECT_STAFFING"/></td>
			<td align="right" class=RightDashBoardHeader > 	
				<fmtProjectStaffingReport:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
        <tr><td colspan="2" height="15"></td></tr> 
        <tr><td colspan="2" class="LLine" height="1"></td></tr>			
		<tr height="24" class="Shade" >
			<td class="RightTableCaption" width="95%" colspan="2">
				&nbsp;<fmtProjectStaffingReport:message key="LBL_PROJECT"/>&nbsp;:&nbsp;
				<gmjsp:dropdown controlName="prjId" SFFormName="frmStaffingDetails" SFSeletedValue="prjId" width="250"
				SFValue="alPrjList" codeId="ID" codeName="NAME" defaultValue="[Choose One]"/>&nbsp;&nbsp;&nbsp;<fmtProjectStaffingReport:message key="LBL_TYPE"/>
				&nbsp;:&nbsp;
				<gmjsp:dropdown controlName="partyType" SFFormName="frmStaffingDetails" SFSeletedValue="partyType" width="110"
				SFValue="alPartyTypes" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>&nbsp;&nbsp;&nbsp;<fmtProjectStaffingReport:message key="LBL_STATUS"/>
				&nbsp;:&nbsp;
				<gmjsp:dropdown controlName="status" SFFormName="frmStaffingDetails" SFSeletedValue="status"
				SFValue="alStatus" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr height="24">
			<td class="RightTableCaption" width="95%">
				&nbsp;<fmtProjectStaffingReport:message key="LBL_ROLE"/>&nbsp;:&nbsp;
				<gmjsp:dropdown controlName="role" SFFormName="frmStaffingDetails" SFSeletedValue="role" width="140"
				SFValue="alRoles" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>&nbsp;&nbsp;
				
				<fmtProjectStaffingReport:message key="LBL_LAST_NAME"/>&nbsp;:&nbsp;
				<html:text property="lastName" size="20" name="frmStaffingDetails" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
				&nbsp;&nbsp;<fmtProjectStaffingReport:message key="LBL_FIRST_NAME"/>&nbsp;:&nbsp;
				<html:text property="firstName" size="20" name="frmStaffingDetails" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
			</td>
			<td width="5%" class="RightTableCaption" align="right">
				<fmtProjectStaffingReport:message key="BTN_GO" var="varGo"/><gmjsp:button name="btnGo" buttonType="Load" value="&nbsp;${varGo}&nbsp;" gmClass="button" onClick="fnStaffingReportSubmit();"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
		</tr>		
		<tr><td class="LLine" height="1" colspan="2"></td></tr>
		<tr>
			<td width="100%" colspan="2">
				<display:table name="requestScope.frmStaffingDetails.lresult" class="gmits" id="currentRowObject" 
				decorator="com.globus.prodmgmnt.displaytag.DTProjectStaffingWrapper" requestURI="/gmProjectStaffingReport.do">
					<display:column property="ID" title="" sortable="false" class="aligncenter"/>
					<fmtProjectStaffingReport:message key="LBL_MEMBER" var="varMemberName"/><display:column property="NAME" title="${varMemberName}" sortable="true" class="alignleft"/>
					<fmtProjectStaffingReport:message key="LBL_PROJECT" var="varProjectName"/><display:column property="PROJECT" title="${varProjectName}" sortable="true" class="alignleft"/>
					<fmtProjectStaffingReport:message key="LBL_ROLE" var="varRole"/><display:column property="ROLE" title="${varRole}" sortable="true" class="alignleft"/>
					<fmtProjectStaffingReport:message key="LBL_STATUS" var="varStatus"/><display:column property="STATUS" title="${varStatus}" sortable="true" class="alignleft"/>
				</display:table>
			</td>
			
		</tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>