
<%
/**********************************************************************************
 * File		 		: GmEntityReport.jsp
 * Desc		 		: Fetching and showing Company Details related to Region
************************************************************************************/
%>
<!-- prodmgmnt\GmEntityReport.jsp -->
<!-- PMT#40410-region mapping info on popup window -->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtSystemReleaseMapping"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page isELIgnored="false"%>
<fmtSystemReleaseMapping:setLocale value="<%=strLocale%>" />
<fmtSystemReleaseMapping:setBundle
	basename="properties.labels.prodmgmnt.GmSystemReleaseMapping" />

<bean:define id="regionId" name="frmSystemDetails" property="regionId"
	scope="request" type="java.lang.String"></bean:define>
<bean:define id="regionName" name="frmSystemDetails"
	property="regionName" scope="request" type="java.lang.String"></bean:define>


<%
	String strProdmgmntJsPath = GmFilePathConfigurationBean
			.getFilePathConfig("JS_PRODMGMNT");
	String strWikiTitle = GmCommonClass
			.getWikiTitle("SYSTEM_ENTITY_REPORT");

%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="<%=strCssPath%>/displaytag.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript"
	src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript"
	src="<%=strProdmgmntJsPath%>/GmEntityReport.js"></script>
</head>
<script>
var strRegionId = '<%=regionId%>';
</script>


<body leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
	<html:form>
		<table border="0" class="DtTable500" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="5" height="25" class="RightDashBoardHeader"><fmtSystemReleaseMapping:message
						key="LBL_ENTITY_REPORT" />&nbsp;&nbsp;(<%=regionName%>)</td>
				<td height="25" class="RightDashBoardHeader"><fmtSystemReleaseMapping:message
						key="IMG_HELP" var="varHelp" /><img align="right" id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td colspan="6">
					<div id="dataGridDiv" height="450px" width="500px"></div>
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="6" height="1"></td>
			</tr>
			
			<tr id="DivExportExcel" height="25">
				<td colspan="6" align="center">
				<div class="exportlinks"><fmtSystemReleaseMapping:message key="DIV_EXPORT_OPT"/> : <img
					src='img/ico_file_excel.png' />&nbsp;<a href="#"
					onclick="fnExportRegionMappingDtls();"><fmtSystemReleaseMapping:message key="DIV_EXCEL"/></a>
				</div>
				</td>
			</tr>
			 <tr><td colspan="6" align="center"  class="RegularText"><div  id="DivNothingMessage"><fmtSystemReleaseMapping:message key="NO_DATA_FOUND"/></div></td></tr>
			
			<tr>
				<td colspan="6" align="center" height="30"><fmtSystemReleaseMapping:message
						key="BTN_CLOSE" var="varClose" /> <gmjsp:button
						value="${varClose}" gmClass="button" onClick="fnClose();"
						tabindex="2" buttonType="load" /></td>
			</tr>
		</table>
	</html:form>
</body>
<%@ include file="/common/GmFooter.inc"%>
</html>
