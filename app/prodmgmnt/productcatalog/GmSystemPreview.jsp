<%
/****************************************************************
 * File		 		: GmSystemPreview.jsp
 * Desc		 		: Screen used to Preview the System Details
 * Version	 		: 1.0
 * author			: Harinadh Reddi N
*****************************************************************/
%>

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtSystemPreview" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\productcatalog\GmSystemPreview.jsp -->
<fmtSystemPreview:setLocale value="<%=strLocale%>"/>
<fmtSystemPreview:setBundle basename="properties.labels.prodmgmnt.productcatalo.GmSystemPreview"/>

<bean:define id="systemNm" name="frmSystemPreview" property="systemNm" type="java.lang.String"> </bean:define>
<bean:define id="systemId" name="frmSystemPreview" property="systemId" type="java.lang.String"> </bean:define>
<bean:define id="xmlGroupdData" name="frmSystemPreview" property="xmlGroupdData" type="java.lang.String"> </bean:define>
<bean:define id="strOpt" name="frmSystemPreview" property="strOpt" type="java.lang.String"> </bean:define>
<bean:define id="xmlSetData" name="frmSystemPreview" property="xmlSetData" type="java.lang.String"> </bean:define>
<bean:define id="sysimgid" name="frmSystemPreview" property="sysimgid" type="java.lang.String"> </bean:define>
<bean:define id="alImageList" name="frmSystemPreview" property="alImageList" type="java.util.ArrayList"></bean:define>
<bean:define id="sysDtlDesc" name="frmSystemPreview" property="sysDtlDesc" type="java.lang.String"> </bean:define>
<bean:define id="sysDesc" name="frmSystemPreview" property="sysDesc" type="java.lang.String"> </bean:define>

<%

    String strProdMgmntProductJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT_PRODUCTCATALOG");
	String strWikiTitle = GmCommonClass.getWikiTitle("SYSTEM_PREVIEW");	
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");
	request.setCharacterEncoding("UTF-8");
	String strSystemName = systemNm;
	String strSystemID = systemId;
	int imgCount = alImageList.size();
	String strTypeName = "";
	strTypeName = "System" + "\\" +strSystemID;
	String strImgName = "";
	String strImgID = "";
	String strMultiImgName = "";
	String strMultiImgID = "";
	String strMultiImgNameone = "";
	String strMultiImgNameTwo = "";
	String strSingleImgID = sysimgid;
	int intHeight = 200;
	int intWidth = 200;
	String strOtion = strOpt;
	String strFileName = GmCommonClass.parseNull(request.getParameter("fileName"));
	HashMap hmMultiImageValues = new HashMap();	
	for(int i=0; i<alImageList.size();i++){
		HashMap hmImageValues = new HashMap();
		hmImageValues = (HashMap)alImageList.get(i);
		strImgName = GmCommonClass.parseNull((String)hmImageValues.get("NAME"));
		strImgID = GmCommonClass.parseNull((String)hmImageValues.get("ID"));
	}	
%> 
<HTML>
<HEAD>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

<title>Globus Medical: Translate </title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strProdMgmntProductJsPath%>/GmSystemPreview.js"></script>	
<script>
var objGroupData;
var objSetData;
objGroupData = '<%=xmlGroupdData%>';
objSetData = '<%=xmlSetData%>';
var gridGrpObj = '';
var gridSetObj = '';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form action="/gmSystemPreview.do">
<html:hidden property="strOpt"/>
<html:hidden property="haction"/>
	<%if(!strOtion.equals("popup")){ %>
	<table border="0" class="DtTable800" cellspacing="0" cellpadding="0" >
		<tr class="shade">
			<td>
				<table  border="0" cellspacing="0" cellpadding="0" width="100%">
					<tr>
						<td  height="25" class="RightDashBoardHeader"><fmtSystemPreview:message key="LBL_PREVIEW"/> - <%=strSystemName%></td>
            			<td  height="25" class="RightDashBoardHeader">
                		<fmtSystemPreview:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
            			</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr class="ShadeRightTableCaption">
			<td height="25" colspan="3" >&nbsp;<fmtSystemPreview:message key="LBL_SYSTEM_DESCRIPTION"/></td>
		</tr>		
		<tr><td bgcolor="#666666" colspan="3" height="1"></td></tr>
		<tr><td colspan="3" height="45" align="left"><%=sysDesc%></td></tr>
		<tr>
		<td width="798" height="100" valign="top" align="center"><div align="left">			
		<div style="position: relative;  width: 770px; height:250px;   margin: 10px; overflow:auto">
		<table>
		<tr>
			<td><div style="overflow: scroll;"><table><tr>
		 <%for(int i=0; i<alImageList.size();i++){
			 hmMultiImageValues = (HashMap)alImageList.get(i);
			 strMultiImgNameone = GmCommonClass.parseNull((String)hmMultiImageValues.get("NAME"));
			 strMultiImgID = GmCommonClass.parseNull((String)hmMultiImageValues.get("ID"));			 
		 %>
			  <td><a href="#" onclick=window.open("/gmSystemPreview.do?strOpt=popup&systemId=<%=strSystemID%>&sysimgid=<%=strMultiImgID%>","","resizable=yes,scrollbars=yes,width=470,height=450") ><img src="/GmCommonFileOpenServlet?uploadString=PRODCATALOGUPLOADDIR&appendPath=<%=strTypeName%>&sId=<%=strMultiImgID%>" height=<%=intHeight%> width=<%=intWidth%>/></a></td>
			  <% if(i+1 < imgCount){				
					hmMultiImageValues = (HashMap)alImageList.get(i+1);
					strMultiImgNameTwo = GmCommonClass.parseNull((String)hmMultiImageValues.get("NAME"));
					strMultiImgID = GmCommonClass.parseNull((String)hmMultiImageValues.get("ID"));
			 %>
			 <td><a href="#" onclick=window.open("/gmSystemPreview.do?strOpt=popup&systemId=<%=strSystemID%>&sysimgid=<%=strMultiImgID%>","","resizable=yes,scrollbars=yes,width=470,height=450") ><img src="/GmCommonFileOpenServlet?uploadString=PRODCATALOGUPLOADDIR&appendPath=<%=strTypeName%>&sId=<%=strMultiImgID%>" height=<%=intHeight%> width=<%=intWidth%> /></a></td>
			  <%}else{ %>
				 <td>
				 	&nbsp;
				 </td>
			 <%}%>
			 				 
	 <%
		i++; }
	 %></tr></table></div>
	 </td>	
	 </tr>
	 </table>
	 </div>
	 </div>	
	 </td>
	 </tr>
		<tr><td bgcolor="#666666" colspan="3" height="1"></td></tr>
		<tr class="ShadeRightTableCaption">
			<td height="25" colspan="3" >&nbsp;<fmtSystemPreview:message key="LBL_DETAILED_DESCRIPTION"/></td>
		</tr>
		<tr><td bgcolor="#666666" colspan="3" height="1"></td></tr>
		<tr><td colspan="3" height="45" align="left"><%=sysDtlDesc%></td></tr>
		<tr><td bgcolor="#666666" colspan="3" height="1"></td></tr>
		<tr class="ShadeRightTableCaption">
			<td height="25" colspan="3" >&nbsp;<fmtSystemPreview:message key="LBL_AVAILABLE_GROUPS"/></td>
		</tr>
		<tr><td bgcolor="#666666" colspan="3" height="1"></td></tr>
		<tr>
			<td colspan="3"><div id="GroupDetails" height = "150px"></div></td>							
		</tr>
		<%if(xmlGroupdData.indexOf("cell") == -1){%>
		<tr>
			<td colspan="3" align="center"><div  id="ShowGroupDetails" align="center"></div></td>							
		</tr>		
		<%}%>
		<tr><td bgcolor="#666666" colspan="3" height="1"></td></tr>
		<tr class="ShadeRightTableCaption">
			<td height="25" colspan="3" >&nbsp;<fmtSystemPreview:message key="LBL_AVAILABLE_SETS"/></td>
		</tr>
		<tr><td bgcolor="#666666" colspan="3" height="1"></td></tr>
		<tr>
			<td colspan="3"><div id="SetDetails" height="150px"></div></td>							
		</tr>
		<%if(xmlSetData.indexOf("cell") == -1){%>
		<tr>
			<td colspan="3" align="center"><div  id="ShowSetDetails" align="center"></div></td>							
		</tr>		
		<%}%>	
		<tr><td bgcolor="#666666" colspan="3" height="1"></td></tr>
		<tr><td colspan="3" height="30" align="center">
				<fmtSystemPreview:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="&nbsp;${varClose}&nbsp;" style="width: 6em; height: 23" name="Btn_Close" buttonType="Load" gmClass="button" onClick="window.close();" />
		</tr>
	</table>		
	<%}else{ %>
		<table>
		<tr>
		 <td height="100" valign="top" align="left">
	 		<img src="/GmCommonFileOpenServlet?uploadString=PRODCATALOGUPLOADDIR&appendPath=<%=strTypeName%>&sId=<%=strSingleImgID%>" height=400 width=400 /></td>
	 	</tr>	 	
		</table>
		<%} %>		
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
