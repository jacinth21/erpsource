<%
/***********************************************************************************************
 * File		 		: GmTranslateByType.jsp
 * Desc		 		: Screen used to save the Lanugage details for Part/Set/System/Progect/Group
 * Version	 		: 1.0
 * author			: Harinadh Reddi N
*************************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc"%>

<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtTranslateByType" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\productcatalog\GmTranslateByType.jsp -->
<fmtTranslateByType:setLocale value="<%=strLocale%>"/>
<fmtTranslateByType:setBundle basename="properties.labels.prodmgmnt.productcatalog.GmTranslateByType"/>

<bean:define id="alTransTypeList" name="frmTranslateByType" property="alTransTypeList" type="java.util.ArrayList"></bean:define>
<bean:define id="xmlGridData" name="frmTranslateByType" property="xmlGridData" type="java.lang.String"> </bean:define>
<bean:define id="savevoidaccess" name="frmTranslateByType" property="savevoidaccess" type="java.lang.String"> </bean:define>
<bean:define id="enableVoidButton" name="frmTranslateByType" property="enableVoidButton" type="java.lang.String"> </bean:define>
<bean:define id="strPartdesc" name="frmTranslateByType" property="partdescription" type="java.lang.String"> </bean:define>
<%
   
   
   String strProdMgmntProductJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT_PRODUCTCATALOG");
	String strWikiTitle = GmCommonClass.getWikiTitle("TRANSLATE_BY_TYPE");
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");	
	String strBtnAccess = "true";
	if(savevoidaccess.equals("Y")){
		strBtnAccess = "enabled";
	}

	
%> 
<HTML>
<HEAD>
<title>Globus Medical: Translate </title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strProdMgmntProductJsPath%>/GmTranslateByType.js"></script>
	
<script>
var objGridData;
objGridData = '<%=xmlGridData%>';
var gridObj ='';
var voidaccess ='<%=enableVoidButton%>';
var lblpart ='<fmtTranslateByType:message key="LBL_PART"/>';
var lblProjectList = '<fmtTranslateByType:message key="LBL_PROJECT_LIST"/>';
var lblSystemList = '<fmtTranslateByType:message key="LBL_SYSTEM_LIST"/>';
var lblGroupList = '<fmtTranslateByType:message key="LBL_GROUP_LIST"/>';
var lblLangauge = '<fmtTranslateByType:message key="LBL_LANGUAGE"/>';
</script>

<script language="javascript" type="text/javascript" src="<%=strJsPath%>/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode : "specific_textareas",
	    editor_selector : "HtmlTextArea",
		theme : "advanced",
		theme_advanced_path : false,
		height : "300",
		width: "500" 
	});
</script>

</HEAD>
<style>
/* added style TranslateDetails  in in PC-3662-Edge Browser fixes for Quality Module */
	#TranslateDetails .objbox{
		height: auto !important;
	}
</style>	
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form action="/gmTranslateByType.do">
<html:hidden property="strOpt"/>
<html:hidden property="haction"/>
<html:hidden property="sysprogrpid"/>
<html:hidden property="metaid"/>
<html:hidden property="metaname"/>
	<table border="0" class="DtTable900" cellspacing="0" cellpadding="0" >
		<tr class="shade">
			<td>
				<table  border="0" cellspacing="0" cellpadding="0" width="100%">
					<tr>
						<td  height="25" class="RightDashBoardHeader" > <fmtTranslateByType:message key="LBL_TRANSLATE_BY_TYPE"/></td>
            			<td  height="25" class="RightDashBoardHeader">
                		<fmtTranslateByType:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
            			</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="0" width="100%">
					<tr class="ShadeRightTableCaption">
						<td height="25" colspan="3" >&nbsp;<fmtTranslateByType:message key="LBL_DETAILS"/></td>
					</tr>
					<tr><td class="LLine" height="1" colspan="3"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" height="25" width="10%"><font color="red">*</font>&nbsp;<fmtTranslateByType:message key="LBL_TYPE"/>:&nbsp;</td>					
						<td width="40%"><gmjsp:dropdown controlName="typeId" onChange="fnSetTypeList();" SFFormName="frmTranslateByType"
							SFSeletedValue="typeId" SFValue="alTranslateTypes" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"/>
						</td>				
					</tr>
					<tr><td class="LLine" colspan="3" height="1"></td></tr>			
					<tr id="part_typeid" class="shade" style="display:none">
						<td class="RightTableCaption" align="right" height="25"><font color="red">*</font>&nbsp;<fmtTranslateByType:message key="LBL_PART"/>:&nbsp;</td>
						<td colspan="2">
							<table border="0" cellspacing="0" cellpadding="0" width="100%">
								<tr>
									<td width="35%"><html:text property="typepartid"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
					        		<fmtTranslateByType:message key="BTN_LOAD" var="varLoad"/><td><gmjsp:button value="${varLoad}" gmClass="button" style="width: 5em; height: 22" buttonType="Load" onClick="fnTranslate(this);"/></td>
					        	</tr>
					        </table>
						</td>
					</tr>
					<logic:equal name="frmTranslateByType"  property="typeId" value="103090">
						<logic:equal name="frmTranslateByType"  property="strOpt" value="FetchTranslate">
						<tr><td class="LLine" colspan="3" height="1"></td></tr>
							<tr><td colspan="3">
								<table border="0" cellspacing="0" cellpadding="0" width="100%">
									<tr>
										<td width="33%"></td>
										<td align="left"><%=strPartdesc%></td>
									</tr>
								</table>
								</td>
							</tr>
						</logic:equal>	
					</logic:equal>	
					<tr id="set_typeid" class="shade" style="display:none">
						<td class="RightTableCaption" align="right" height="25"><font color="red">*</font>&nbsp;<fmtTranslateByType:message key="LBL_SET_ID"/>:&nbsp;</td>
						<td colspan="2">
							<table border="0" cellspacing="0" cellpadding="0" width="100%">
								<tr>
									<td width="35%"><html:text property="typesetid"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
					        		<td> <fmtTranslateByType:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" gmClass="button" style="width: 5em; height: 22" buttonType="Load" onClick="fnTranslate(this);"/></td>
					        	</tr>
					        </table>
						</td>
					</tr>						
					<logic:equal name="frmTranslateByType"  property="typeId" value="103091">
						<logic:equal name="frmTranslateByType"  property="strOpt" value="FetchTranslate">
							<tr>
								<td colspan="3">
									<table border="0" cellspacing="0" cellpadding="0" width="100%">
									<tr><td class="LLine" colspan="3" height="1"></td></tr>
										<tr>
											<td width="33%"></td>
											<td align="left"><%=strPartdesc%></td>
										</tr>
									</table>
								</td>
							</tr>
						</logic:equal>	
					</logic:equal>	
					<tr id="setbundle_id" class="shade" style="display:none">
						<td class="RightTableCaption" align="right" height="25"><font color="red">*</font>&nbsp;<fmtTranslateByType:message key="LBL_SET_BUNDLE_ID"/>:&nbsp;</td>
						<td colspan="2"><gmjsp:dropdown controlName="setbundleId"  SFFormName="frmTranslateByType" SFSeletedValue="setbundleId" SFValue="alTransTypeList"
					 		codeId="SETBUNDLEID"  codeName="SETBUNDLENM"  defaultValue="[Choose One]" width="450" onChange="fnTranslate(this);"/>
						</td>
					</tr>			
					<logic:equal name="frmTranslateByType"  property="typeId" value="26240459">
						<logic:equal name="frmTranslateByType"  property="strOpt" value="FetchTranslate">
							<tr>
								<td colspan="3">
									<table border="0" cellspacing="0" cellpadding="0" width="100%">
									<tr><td class="LLine" colspan="3" height="1"></td></tr>
										<tr>
											<td width="33%"></td>
											<td align="left"><%=strPartdesc%></td>
										</tr>
									</table>
								</td>
							</tr>
						</logic:equal>	
					</logic:equal>	
					<tr id="prj_typelist" class="shade" style="display:none">
						<td class="RightTableCaption" align="right" height="25"><font color="red">*</font>&nbsp;<fmtTranslateByType:message key="LBL_PROJECT_LIST"/>:&nbsp;</td>
						<td colspan="2"><gmjsp:dropdown controlName="typeprojectid"  SFFormName="frmTranslateByType" SFSeletedValue="typeprojectid" SFValue="alTransTypeList"
					 		codeId="ID"  codeName="NAME"  defaultValue="[Choose One]" width="450" onChange="fnTranslate(this);"/>
						</td>
					</tr>
					<tr id="sys_typelist" class="shade" style="display:none">
						<td class="RightTableCaption" align="right" height="25"><font color="red">*</font>&nbsp;<fmtTranslateByType:message key="LBL_SYSTEM_LIST"/>:&nbsp;</td>
						<td colspan="2"><gmjsp:dropdown controlName="typesystemid"  SFFormName="frmTranslateByType" SFSeletedValue="typesystemid" SFValue="alTransTypeList"
					 		codeId="ID"  codeName="NAME"  defaultValue="[Choose One]" width="250" onChange="fnTranslate(this);"/>
					</td>
					</tr>
					<tr id="grp_typelist" class="shade" style="display:none">
						<td class="RightTableCaption" align="right" height="25"><font color="red">*</font>&nbsp;<fmtTranslateByType:message key="LBL_GROUP_LIST"/>:&nbsp;</td>
						<td colspan="2"><gmjsp:dropdown controlName="typegroupid"  SFFormName="frmTranslateByType" SFSeletedValue="typegroupid" SFValue="alTransTypeList"
					 		codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]" width="315" onChange="fnTranslate(this);"/>
						</td>
					</tr>			
					<logic:equal name="frmTranslateByType" property="strOpt" value="FetchTranslate">
						<logic:notEqual name="frmTranslateByType" property="sysprogrpid" value="">
							<tr><td class="LLine" height="1" colspan="3"></td></tr>			
							<tr class="ShadeRightTableCaption">
								<td height="25" colspan="3" >&nbsp;<fmtTranslateByType:message key="LBL_TRANSLATE"/></td>	
							</tr>			
							<tr><td class="LLine" height="1" colspan="3"></td></tr>
							<tr>
								<td class="RightTableCaption" align="right" height="25"><font color="red">*</font>&nbsp;<fmtTranslateByType:message key="LBL_LANGUAGE"/>:&nbsp;</td>
								<td colspan="2"><gmjsp:dropdown controlName="langId"  SFFormName="frmTranslateByType" SFSeletedValue="langId" SFValue="alLanguageList"
						 			codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]" onChange="fnDisableSubmit(this);"/>
								</td>
							</tr>
							<tr><td colspan="3" height="1" bgcolor="#CCCCCC"></td></tr>
							<tr class="oddshade">
								<td class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font> <fmtTranslateByType:message key="LBL_NAME"/>:&nbsp;</td>
								<td><html:text property="langNm" maxlength="255" size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"	onblur="changeBgColor(this,'#ffffff');"/>
								</td>
							</tr>
							<tr><td colspan="3" height="1" bgcolor="#CCCCCC"></td></tr>
							<tr class="evenshade">
								<td class="RightTableCaption" valign="middle" align="right" HEIGHT="320"><fmtTranslateByType:message key="LBL_DESCRIPTION"/>:&nbsp;</td>
								<td><textarea name="description" id="desc" class="HtmlTextArea"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" rows=3 cols=50 ><bean:write name="frmTranslateByType" property="description"/></textarea></td>
							</tr>
							<tr><td colspan="3" height="1" bgcolor="#CCCCCC"></td></tr>
							<tr class="oddshade">
								<td class="RightTableCaption" valign="middle" align="right" HEIGHT="320"><fmtTranslateByType:message key="LBL_DETAILED_DESCRIPTION"/>:&nbsp;</td>
								<td><textarea name="dtl_description" id="dtlDesc"  class="HtmlTextArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" rows=5 cols=50 ><bean:write name="frmTranslateByType" property="dtl_description"/></textarea></td>
							</tr>
							<tr><td class="LLine" colspan="3" height="1"></td></tr>
							<tr>
								<td colspan="3" height="30" align="center">
								<fmtTranslateByType:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" style="width: 6em; height: 23" disabled="<%=strBtnAccess %>" buttonType="Save" name="Btn_Submit" gmClass="button" onClick="fnSubmitLanguage();" />&nbsp;
								<fmtTranslateByType:message key="BTN_RESET" var="varReset"/><gmjsp:button value="&nbsp;${varReset}&nbsp;" style="width: 6em; height: 23 " name="Btn_Reset" gmClass="button" buttonType="Save" onClick="fnReset();" />&nbsp;
								<fmtTranslateByType:message key="BTN_VOID" var="varVoid"/><gmjsp:button value="&nbsp;${varVoid}&nbsp;" style="width: 6em; height: 23 " disabled="true" name="Btn_Void" gmClass="button" buttonType="Save" onClick="fnVoidLanguage();" />
							</tr>
							<tr><td class="LLine" height="1" colspan="3"></td></tr>
							<tr class="ShadeRightTableCaption">
								<td height="25" colspan="3" >&nbsp;<fmtTranslateByType:message key="LBL_TRANSLATE_DETAILS"/></td>	
							</tr>							
							<tr>
								<td colspan="3"><div  id="TranslateDetails"></div></td>							
							</tr>
							<%if( xmlGridData.indexOf("cell") == -1){%>
							<tr>
								<td colspan="3" align="center"><div  id="ShowDetails" align="center"></div></td>							
							</tr>
							<%} %>
						</logic:notEqual>
					</logic:equal>
				</table>
			</td>	
		</tr> 
	</table>			
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
