<%
/**********************************************************************************
 * File		 		: GmLanguageDetail.jsp
 * Desc		 		: Screen will give language details
 * Version	 		: 1.0
 * author			: Jignesh Shah
************************************************************************************/
%>
<!-- \prodmgmnt\productcatalog\GmLanguageDetail.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<bean:define id="gridData" name="frmLanguageDetail" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="intResultSize" name="frmLanguageDetail" property="intResultSize" type="java.lang.Integer"> </bean:define>
<bean:define id="refId" name="frmLanguageDetail" property="refID" type="java.lang.String"> </bean:define>
<bean:define id="typeID" name="frmLanguageDetail" property="typeID" type="java.lang.String"> </bean:define>
<%
String strProdMgmntProductJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT_PRODUCTCATALOG");

response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8");
String strGridWidth = "";
// To adjust the width according to each screen. [103090: Part Number; 103091: Set; 103093: Project; 103094: Group]
if(typeID.equals("103090"))
	strGridWidth = "930px";
else if(typeID.equals("103091"))
	strGridWidth = "750px";
else if(typeID.equals("103093"))
	strGridWidth = "680px";
else if(typeID.equals("103094"))
	strGridWidth = "705px";
else if(typeID.equals("26240459"))  //Added condition typeID.equals("26240459") changes in PC-3662-Edge Browser fixes for Quality Module
	strGridWidth = "750px";
%>
 
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: Language</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strProdMgmntProductJsPath%>/GmLanguageDetail.js"></script>
<script>
	var objGridData;
	objGridData = '<%=gridData%>';
</script>
</HEAD>
<BODY onload="fnLoadLanguageGrid();">
<html:form action="/gmLanguageDetail.do">

<table  border="0" style="width: 750px" cellspacing="0" cellpadding="0" >
	<tr>	
		<td>
			<div id="LanguageDataGrid" class="grid" style="width:<%=strGridWidth%>;height: 600px;"></div>
		</td>
		</tr>
		<%if( gridData.indexOf("cell") == -1){%>
		<tr>
			<td colspan="3" align="center"><div  id="ShowDetails" align="center"></div></td>							
		</tr>
		<%} %>	  
</table>			
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>