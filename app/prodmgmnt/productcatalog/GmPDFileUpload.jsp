<%
/**********************************************************************************
 * File		 		: GmPDFileUpload.jsp
 * Desc		 		: Screen used to upload files for Product Catalog
 * Version	 		: 1.0
 * author			: Jignesh Shah
************************************************************************************/
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtPDFileUpload" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\productcatalog\GmPDFileUpload.jsp -->
<fmtPDFileUpload:setLocale value="<%=strLocale%>"/>
<fmtPDFileUpload:setBundle basename="properties.labels.prodmgmnt.productcatalog.GmPDFileUpload"/>


<bean:define id="strOpt" name="frmPDFileUpload" property="strOpt" type="java.lang.String"> </bean:define>
<bean:define id="gridData" name="frmPDFileUpload" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="intResultSize" name="frmPDFileUpload" property="intResultSize" type="java.lang.Integer"> </bean:define>
<bean:define id="uploadTypeId" name="frmPDFileUpload" property="uploadTypeId" type="java.lang.String"> </bean:define>
<bean:define id="partListId" name="frmPDFileUpload" property="partListId" type="java.lang.String"> </bean:define>
<bean:define id="setListId" name="frmPDFileUpload" property="setListId" type="java.lang.String"> </bean:define>
<bean:define id="strPartNumDesc" name="frmPDFileUpload" property="partNumDesc" type="java.lang.String"> </bean:define>
<bean:define id="strSetDesc" name="frmPDFileUpload" property="setDesc" type="java.lang.String"> </bean:define>
<bean:define id="prjListId" name="frmPDFileUpload" property="prjListId" type="java.lang.String"> </bean:define>
<bean:define id="sysListId" name="frmPDFileUpload" property="sysListId" type="java.lang.String"> </bean:define>
<bean:define id="grpListId" name="frmPDFileUpload" property="grpListId" type="java.lang.String"> </bean:define>
<bean:define id="refId" name="frmPDFileUpload" property="refId" type="java.lang.String"> </bean:define>
<bean:define id="sbListId" name="frmPDFileUpload" property="sbListId" type="java.lang.String"> </bean:define>
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("FILE_UPLOAD");
 String strProdMgmntProductJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT_PRODUCTCATALOG");

	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");
	
	String strRefId = "";
	if(strOpt.equals("FetchVoidUploadFiles")){
		strRefId = refId;
	}else{
	if (uploadTypeId.equals("103090")){
		strRefId = partListId;
	}else if(uploadTypeId.equals("103091")){
		strRefId = setListId;
	}else if(uploadTypeId.equals("103092")){
		strRefId = sysListId;
	}else if(uploadTypeId.equals("103093")){
		strRefId = prjListId;
	}else if(uploadTypeId.equals("103094")){
		strRefId = grpListId;
	}else if(uploadTypeId.equals("26240459")){
		strRefId = sbListId;
	}	
	}
	
%>
 
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: File Upload</title>

<link rel="STYLESHEET" type="text/css"	href="<%=strJsPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strProdMgmntProductJsPath%>/GmPDFileUpload.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<script>
var objGridData;
objGridData = '<%=gridData%>';
var lblPart = '<fmtPDFileUpload:message key="LBL_PART"/>';
var lblSetID ='<fmtPDFileUpload:message key="LBL_SET_ID"/>';
var lblSetBundleID ='<fmtPDFileUpload:message key="LBL_SET_BUNDLE_ID"/>';
</script>

</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();" >
<html:form action="/gmPDFileUpload.do" enctype="multipart/form-data" >
		
<html:hidden property="strOpt"/>
<html:hidden property="haction"/>
<html:hidden property="refId" value="<%=strRefId%>"/>
<html:hidden property="forwardAction"/>
<html:hidden property="fileInputStr" />
<html:hidden property="hTxnId" value="" />
<html:hidden property="hTxnName" value="" />
<html:hidden property="accessDcoed"/>
<html:hidden property="accessNonDcoed"/>
<html:hidden property="strSetGroupType"/>
<input type="hidden" name="hAction" value="" />
<input type="hidden" name="hCancelType" value="" />
<input type="hidden" name="hRedirectURL" value="" />
<input type="hidden" name="hDisplayNm" value="" />

<table border="0" class="DtTable1300" cellspacing="0" cellpadding="0" >
<tr class="shade">
	<td>
		<table  border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr>
				<td  height="25" class="RightDashBoardHeader" ><fmtPDFileUpload:message key="LBL_FILE_UPLOAD"/></td>
            	<td  height="25" class="RightDashBoardHeader">
                	<fmtPDFileUpload:message key="IMG_ALT_HELP" var = "varHelp"/><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
            	</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr class="ShadeRightTableCaption">
				<td height="25" colspan="3" >&nbsp;<fmtPDFileUpload:message key="LBL_UPLOAD_DETAILS"/></td>
			</tr>
			<tr>
				<td class="RightTableCaption" align="right" height="25" width="20%"><font color="red">*</font>&nbsp;<fmtPDFileUpload:message key="LBL_UPLOAD_TYPE"/>:</td>					
				<td width="60%">&nbsp;<gmjsp:dropdown controlName="uploadTypeId" onChange="fnSetTypeList();" SFFormName="frmPDFileUpload"
					SFSeletedValue="uploadTypeId" SFValue="alUploadTypes" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"/>
				</td>
			</tr>
			<tr id="part_typeid" class="shade" style="display:none">
				<td class="RightTableCaption" align="right" height="25"><font color="red">*</font>&nbsp;<fmtPDFileUpload:message key="LBL_PART"/>:</td>
				<td colspan="2" height="25">&nbsp;<html:text property="partListId"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					&nbsp;&nbsp;<fmtPDFileUpload:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" gmClass="Button" style="width: 5em; height: 22" buttonType="load" onClick="fnLoad();"/>
				</td>
			</tr>
			<logic:equal name="frmPDFileUpload"  property="uploadTypeId" value="103090">
				<logic:notEmpty name="frmPDFileUpload"  property="partListId" >
					<tr>
						<td></td>
						<td align="left" colspan="2" height="25">&nbsp;<%=strPartNumDesc%><input type="hidden" name="hTitle" value="<%=strPartNumDesc%>" /></td>
					</tr>
				</logic:notEmpty>
			</logic:equal>
			<tr id="set_typeid" class="shade" style="display:none">
				<td class="RightTableCaption" align="right" height="25"><font color="red">*</font>&nbsp;<fmtPDFileUpload:message key="LBL_SET_ID"/>:</td>
				<td colspan="2" height="25">&nbsp;<html:text property="setListId"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					&nbsp;&nbsp;<fmtPDFileUpload:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" gmClass="Button" style="width: 5em; height: 22" buttonType="load" onClick="fnLoad();"/>
				</td>
			</tr>
			<logic:equal name="frmPDFileUpload"  property="uploadTypeId" value="103091">
				<logic:equal name="frmPDFileUpload"  property="strOpt" value="FETCHUPLOADFILES">
					<logic:notEmpty name="frmPDFileUpload"  property="partListId" >
						<tr>
							<td></td>
							<td align="left" colspan="2" height="25">&nbsp;<%=strSetDesc%><input type="hidden" name="hTitle" value="<%=strSetDesc%>" /></td>
						</tr>
					</logic:notEmpty>
				</logic:equal>	
			</logic:equal>
			<tr id="set_bundleid" class="shade" style="display:none">
				<td class="RightTableCaption" align="right" height="25"><font color="red">*</font>&nbsp;<fmtPDFileUpload:message key="LBL_SET_BUNDLE_ID"/>:</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown controlName="sbListId"  SFFormName="frmPDFileUpload" SFSeletedValue="sbListId" SFValue="alSysTypeList"
					 codeId="SETBUNDLEID"  codeName="SETBUNDLENM"  defaultValue="[Choose One]" width="250" onChange="fnLoad();"/>
				</td>
			</tr>
			<logic:equal name="frmPDFileUpload"  property="uploadTypeId" value="26240459">
				<logic:equal name="frmPDFileUpload"  property="strOpt" value="FETCHUPLOADFILES">
					<logic:notEmpty name="frmPDFileUpload"  property="sbListId" >
						<tr>
							<td></td>
							<td align="left" colspan="2" height="25">&nbsp;<%=strSetDesc%><input type="hidden" name="hTitle" value="<%=strSetDesc%>" /></td>
						</tr>
					</logic:notEmpty>
				</logic:equal>	
			</logic:equal>
			<tr id="sys_typelist" class="shade" style="display:none">
				<td class="RightTableCaption" align="right" height="25"><font color="red">*</font>&nbsp;<fmtPDFileUpload:message key="LBL_SYSTEM_LIST"/>:</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown controlName="sysListId"  SFFormName="frmPDFileUpload" SFSeletedValue="sysListId" SFValue="alSysTypeList"
					 codeId="ID"  codeName="NAME"  defaultValue="[Choose One]" width="250" onChange="fnLoad();"/>
				</td>
			</tr>
			<tr id="prj_typelist" class="shade" style="display:none">
				<td class="RightTableCaption" align="right" height="25"><font color="red">*</font>&nbsp;<fmtPDFileUpload:message key="LBL_PROJECT_LIST"/>:</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown controlName="prjListId"  SFFormName="frmPDFileUpload" SFSeletedValue="prjListId" SFValue="alPrjTypeList"
					 codeId="ID"  codeName="NAME"  defaultValue="[Choose One]" width="450" onChange="fnLoad();"/>
				</td>
			</tr>
			<tr id="grp_typelist" class="shade" style="display:none">
				<td class="RightTableCaption" align="right" height="25"><font color="red">*</font>&nbsp;<fmtPDFileUpload:message key="LBL_GROUP_LIST"/>:</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown controlName="grpListId"  SFFormName="frmPDFileUpload" SFSeletedValue="grpListId" SFValue="alGrpTypeList"
					 codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]" width="315" onChange="fnLoad();"/>
				</td>
			</tr>
		</table>
	</td>
	
</tr>
<logic:equal name="frmPDFileUpload" property="strOpt" value="FETCHUPLOADFILES">
<tr>
	<td>
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr class="ShadeRightTableCaption">
				<td height="25" colspan="3" >&nbsp;<fmtPDFileUpload:message key="LBL_UPLOAD_FILES"/></td>
			</tr>
<!-- 			<tr>
				<td height="25" colspan="3" >&nbsp;<font color="#6699FF">Total filesize must be 15MB or less</font></td>
			</tr> -->
			<tr>
				<td>
					 <jsp:include page="/common/GmMultiFileUploadInclude.jsp" />
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr class="ShadeRightTableCaption">
				<td height="25" colspan="3" >&nbsp;<fmtPDFileUpload:message key="LBL_FILES_UPLOADED"/></td>
			</tr>
			<tr>
				<td>
					<jsp:include  page="/common/GmUploadedFileInclude.jsp"/>
				</td>
			</tr>
		</table>
	</td>
</tr>
</logic:equal>

<tr><td colspan="2" class="LLine" height="1"></td></tr>

</table>			
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>