<%@page import="java.net.URLEncoder"%>
<%
/**********************************************************************************
 * File		 		: GmUploadView.jsp
 * Desc		 		: This screen is used for the Single and Multiple Image View
 * Version	 		: 1.0
 * author			: Jignesh Shah
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtUploadView" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\productcatalog\GmUploadView.jsp -->
<fmtUploadView:setLocale value="<%=strLocale%>"/>
<fmtUploadView:setBundle basename="properties.labels.prodmgmnt.GmUploadView"/>
<%@ page import="java.util.ArrayList" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ page import ="com.globus.common.util.GmImageUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Iterator"%><HTML>

<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);		
	int intHeight = 0;
	int intWidth = 0;
	
	String strFileName = GmCommonClass.parseNull(request.getParameter("fileName"));
	String strFileId = GmCommonClass.parseNull(request.getParameter("fileId"));
	String strFileGrpNm = GmCommonClass.parseNull(request.getParameter("fileGrpNm"));
	String strFileRefId = GmCommonClass.parseNull(request.getParameter("fileRefId"));
	String strArtifactNm = GmCommonClass.parseNull(request.getParameter("artifactNm"));
	String strSubTypeNm = GmCommonClass.parseNull(request.getParameter("subTypeNm"));
	String strUploadHome = GmCommonClass.getString("PRODCATALOGUPLOADDIR");
	String strProdCatImageUrl = GmCommonClass.getString("PRODCATALOGIMAGEURL");
	String strFilePath = strFileGrpNm+"\\"+strFileRefId+"\\"+strArtifactNm+"\\"+strSubTypeNm;
	String strOutputDir = strFileName.substring(0,strFileName.lastIndexOf("."));
	String strViewType= GmCommonClass.parseNull(request.getParameter("viewType"));
	String strFileLists= GmCommonClass.parseNull(request.getParameter("fileLists"));
	String[] strFileNames = strFileLists.split(",");

%>

<HEAD>
<TITLE> Globus Medical: Uploaded File View </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript">
function fnZoom(fileUrl){
	
	window.open(fileUrl,"zoom","resizable=yes,scrollbars=yes,width=600,height=500");
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" >

<table border="0" width="500" cellspacing="0" cellpadding="0">
	<tr>
		<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width = "1" height = "1"></td>
		<td colspan="3" class="Line" height="1"></td>
		<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width = "1" height = "1"></td>
	</tr>
	<tr>
		<td bgcolor="#666666" width="1" rowspan="2"><img src="<%=strImagePath%>/spacer.gif" width = "1" height = "1"></td>
		<td height="25" class="RightDashBoardHeader">&nbsp;<fmtUploadView:message key="LBL_IMAGE_VIEW"/></td>
		<td bgcolor="#666666" width="1" rowspan="2"></td>
		<td bgcolor="#666666" width="1" rowspan="2"><img src="<%=strImagePath%>/spacer.gif" width = "1" height = "1"></td>
	</tr>
	<tr>
		<td width="748" height="100" valign="top" align="center">
		 <table>
		 	<% if (strViewType.equals("zoom")){%>
				 <tr>
				 <td>
				 	<img src="/GmCommonFileOpenServlet?uploadString=ZIPOUTPUTDIR&appendPath=<%=strOutputDir%>&fileName=<%=strFileLists%>" />
				 </td>
			 </tr>		 	
		 <%} else if (strViewType.equals("previewzip")){%>
		 <%
		 for(int i=0;i<strFileNames.length;i++){
			 if(i%2==0){%>
			 <% if(i!=0){ %>
				 </tr>
			<% }%>
				<tr>
			 <%}%>
			<td>
			 <a href="#" onclick=fnZoom("/prodmgmnt/productcatalog/GmUploadView.jsp?viewType=zoom&fileName=<%=strFileName%>&fileLists=<%=strFileNames[i]%>")>
			 <img src="/GmCommonFileOpenServlet?uploadString=ZIPOUTPUTDIR&appendPath=<%=strOutputDir%>&fileName=<%=strFileNames[i]%>" width= "200" height="200"></a>
			 </td>
		<% if (i+1 ==strFileNames.length){%>
		 	</tr>
			<%}%>
			
		<% }
		 }else{
		 
		 %>
		 
			 <tr>
				 <td>
				 	<img src="/GmCommonFileOpenServlet?uploadString=PRODCATALOGUPLOADDIR&appendPath=<%=strFilePath%>&sId=<%=strFileId%>" />
				 </td>
			 </tr>	
			 <%} %> 
		 </table>	 
		</td>	
	</tr>
	<tr><td colspan="3" class="Line" height="1"></td></tr>
	<tr>
		<td colspan="3" > &nbsp; </td>
	</tr>
	<tr>
		<td colspan="3" class="RightText" align="center">
			<fmtUploadView:message key="LBL_ZOOM_IN_OR_OUT"/>
		</td>
	</tr>

</table>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
