<% /**************************************************************** 
   * File : GmPDFileUploadReport.jsp 
   * Desc : Screen used to view the uploaded files 
   * Version : 1.0 
   * author : Agilan Sinagarvel 
   *****************************************************************/ %>

    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <%@ include file="/common/GmHeader.inc" %>
        <%@ page import="com.globus.common.beans.GmFilePathConfigurationBean" %>
            <%@ taglib prefix="fmtPDFileUploadReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
                <%@ page isELIgnored="false" %>
                    <!-- prodmgmnt\productcatalog\GmPDFileUploadReport.jsp -->
                    <fmtPDFileUploadReport:setLocale value="<%=strLocale%>" />
                    <fmtPDFileUploadReport:setBundle basename="properties.labels.prodmgmnt.productcatalog.GmPDFileUploadReport" />
                    <bean:define id="sysListId" name="frmPDFileUploadReport" property="sysListId" type="java.lang.String"> </bean:define>
                    <bean:define id="uploadTypeId" name="frmPDFileUploadReport" property="uploadTypeId" type="java.lang.String"> </bean:define>
                    <% String strWikiTitle=GmCommonClass.getWikiTitle("UPLOADED_FILES_REPORT"); String strProdMgmntProductJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT_PRODUCTCATALOG"); %>

                        <HTML>

                        <HEAD>
                            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                            <title>Globus Medical: File Upload</title>
                            <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
                            <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
                            <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
                            <link rel="STYLESHEET" type="text/css" href="<%=strJsPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
                            <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
                            <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
                            <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
                            <script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
                            <script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
                            <script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
                            <script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
                            <script language="JavaScript" src="<%=strProdMgmntProductJsPath%>/GmPDFileUploadReport.js"></script>
                            <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
                            <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">

                        </HEAD>

                        <BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
                            <html:form action="/gmPDFileUploadReport.do?method=loadUploadedFilesReport">
                                <html:hidden property="haction" />
                                <input type="hidden" name="hAction" value="" />

                                <table border="0" class="DtTable1300" cellspacing="0" cellpadding="0">
                                    <tr class="shade">
                                        <td>
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td height="25" class="RightDashBoardHeader">
                                                        <fmtPDFileUploadReport:message key="LBL_UPLOADED_FILE" />
                                                    </td>
                                                    <td height="25" class="RightDashBoardHeader">
                                                        <fmtPDFileUploadReport:message key="IMG_ALT_HELP" var="varHelp" /><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr class="ShadeRightTableCaption">
                                                    <td height="25" colspan="4">&nbsp;
                                                        <fmtPDFileUploadReport:message key="LBL_UPLOAD_DETAILS" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="RightTableCaption" align="right" height="25" width="120px">
                                                        <font color="red">*</font>&nbsp;
                                                        <fmtPDFileUploadReport:message key="LBL_UPLOAD_TYPE" />:
                                                    </td>
                                                    <td>&nbsp;
                                                        <gmjsp:dropdown controlName="uploadTypeId" onChange="fnSetTypeList();" SFFormName="frmPDFileUploadReport" SFSeletedValue="uploadTypeId" SFValue="alUploadTypes" codeId="CODEID" codeName="CODENM" />
                                                    </td>
                                                    <td class="RightTableCaption" align="right" height="25" width="100px">&nbsp;
                                                        <fmtPDFileUploadReport:message key="LBL_SYSTEM_LIST" />:</td>
                                                    <td>&nbsp;
                                                        <gmjsp:dropdown controlName="sysListId" SFFormName="frmPDFileUploadReport" SFSeletedValue="sysListId" SFValue="alSysTypeList" codeId="ID" codeName="NAME" defaultValue="[Choose One]" width="300" />
                                                    </td>
                                                </tr>
                                                <!--  <tr><td colspan="9" height="1" bgcolor="#cccccc"></td></tr> -->
                                                <tr id="sys_typelist" class="oddshade">
                                                    <td class="RightTableCaption" align="right" height="25" width="100px">&nbsp;
                                                        <fmtPDFileUploadReport:message key="LBL_ARTIFACT_TYPE" />:</td>
                                                    <td width="400px">&nbsp;
                                                        <gmjsp:dropdown controlName="fileTypeId" SFFormName="frmPDFileUploadReport" SFSeletedValue="fileTypeId" SFValue="alFileType" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" />
                                                    </td>
                                                    <td class="RightTableCaption" align="right" height="25" width="100px">&nbsp;
                                                        <fmtPDFileUploadReport:message key="LBL_SUB_TYPE" />:</td>
                                                    <td width="400px">&nbsp;
                                                        <gmjsp:dropdown controlName="typeListId" SFFormName="frmPDFileUploadReport" SFSeletedValue="typeListId" SFValue="alSubType" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" height="1" bgcolor="#cccccc"></td>
                                                </tr>
                                                <!--  <tr><td colspan="9" height="1" bgcolor="#cccccc"></td></tr> -->

                                                <tr id="part_typeid" class="shade">
                                                    <td class="RightTableCaption" align="right" height="25" width="100px">&nbsp;
                                                        <fmtPDFileUploadReport:message key="LBL_TITLE" />:</td>
                                                    <td colspan="1" height="25">&nbsp;
                                                        <html:text property="fileTitle" size="70" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
                                                        &nbsp;&nbsp;
                                                    </td>
                                                    <td class="RightTableCaption" align="right" height="25" width="100px">&nbsp;
                                                        <fmtPDFileUploadReport:message key="LBL_FILE_NAME" />:</td>
                                                    <td colspan="1" height="25">&nbsp;
                                                        <html:text property="fileName" size="70" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
                                                        &nbsp;&nbsp;</td>

                                                </tr>
                                                <tr>
                                                    <td colspan="4" height="1" bgcolor="#cccccc"></td>
                                                </tr>
                                                <tr class="oddshade">
                                                    <td class="RightTableCaption" align="right" height="25" width="100px">&nbsp;
                                                        <fmtPDFileUploadReport:message key="LBL_PART_NUMBER" />:</td>
                                                    <td colspan="2" height="25">&nbsp;
                                                        <html:text property="partNumId" size="70" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
                                                        &nbsp;&nbsp;
                                                        <gmjsp:dropdown controlName="strPartLikes" SFFormName="frmPDFileUploadReport" SFSeletedValue="strPartLikes" SFValue="alPartSearch" codeId="CODENMALT" codeName="CODENM" defaultValue="[Choose One]" />
                                                    </td>
                                                    <td align="center">
                                                        <fmtPDFileUploadReport:message key="BTN_LOAD" var="varLoad" />
                                                        <gmjsp:button value="${varLoad}" gmClass="Button" style="width: 5em; height: 22" buttonType="load" onClick="fnLoad();" />
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td colspan="4" height="1" bgcolor="#cccccc"></td>
                                                </tr>
                                                <tr class="evenshade">
                                                    <td colspan="4">
                                                        <div id="dataGridDiv" style="width:100%;"></div>
                                                    </td>
                                                </tr>

                                                <tr class="evenshade">
                                                    <td colspan="4" align="center">
                                                        <div class='exportlinks' id="DivExportExcel">
                                                            <fmtPDFileUploadReport:message key="LBL_EXPORT_OPTIONS" />:<a href="#" onclick="fnDownloadXLS();"><img src='img/ico_file_excel.png' /></a>&nbsp;<a href="#" onclick="fnDownloadXLS();">
                                                                <fmtPDFileUploadReport:message key="LBL_EXCEL" /></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr height="30" id="DivNothingMessage">
                                                    <td colspan="4">
                                                        <div style="text-align:center;color:blue">
                                                            <fmtPDFileUploadReport:message key="LBL_NO_DATA" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="LLine" height="1"></td>
                                    </tr>

                                </table>
                            </html:form>
                            <%@ include file="/common/GmFooter.inc" %>
                        </BODY>

                        </HTML>