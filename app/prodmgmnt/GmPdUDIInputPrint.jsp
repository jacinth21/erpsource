<%
	/***************************************************************************************************************************************
	 * File		 		: GmPdUDIInputPrint.jsp
	 * Desc		 		: This screen is used for .
	 * Version	 		: 1.0
	 * author			: rdinesh
	 ****************************************************************************************************************************************/
%>



<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtPdUDIInputPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\GmPdUDIInputPrint.jsp -->
<fmtPdUDIInputPrint:setLocale value="<%=strLocale%>"/>
<fmtPdUDIInputPrint:setBundle basename="properties.labels.prodmgmnt.GmPdUDIInputPrint"/>
<bean:define id="hmResult" name="frmUDIProductDevelopment" property="hmResult" type="java.util.HashMap"></bean:define>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);	
	HashMap hmHeader = GmCommonClass.parseNullHashMap((HashMap)hmResult.get("HEADER"));
	ArrayList alResult  = GmCommonClass.parseNullArrayList((ArrayList)hmResult.get("DETAILS"));
	String strHtmlJasperRpt = "";

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: PD UDI Print </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script type="text/javascript">

function fnPrint()
{
    window.print();
}
var tdinnner = "";
var strObject="";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screenOus.css">
</HEAD>
<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM method="post" action = "/gmUDIProductDevelopment.do">
<table align="center" width="100%">
<tr>
	<td align="center" id="button">
		<fmtPdUDIInputPrint:message key="BTN_PRINT" var="varPrint"/><gmjsp:button value="${varPrint}" gmClass="button" onClick="fnPrint();" buttonType="Load" />
 		<fmtPdUDIInputPrint:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" />
 	</td>
 </tr>
 </table>
<table>
<tr><td>
<div id="jasper"> 
		<%
		 
			String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
			GmJasperReport gmJasperReport = new GmJasperReport();								
			gmJasperReport.setRequest(request);
			gmJasperReport.setResponse(response);
			gmJasperReport.setJasperReportName("/GmPdUDIInputPrint.jasper");
			gmJasperReport.setHmReportParameters(hmHeader);							
			gmJasperReport.setReportDataList(alResult);
			gmJasperReport.setBlDisplayImage(true);
			strHtmlJasperRpt = gmJasperReport.getHtmlReport();
		%>				
		<%=strHtmlJasperRpt%>
</div>
</td></tr>
</table>
</FORM>
</BODY>
</HTML>