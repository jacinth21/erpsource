<%
/**********************************************************************************
 * File		 		: GmGroupsReport.jsp
 * Desc		 		: display all Groups information
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 

<%@ taglib prefix="fmtGroupsReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\GmGroupsReport.jsp -->
<fmtGroupsReport:setLocale value="<%=strLocale%>"/>
<fmtGroupsReport:setBundle basename="properties.labels.prodmgmnt.GmGroupsReport"/>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
  
<bean:define id="ldtResult" name="frmGroupsReport" property="ldtResult" type="java.util.List"></bean:define>
<%
String strWikiTitle = GmCommonClass.getWikiTitle("ALL_GROUPS");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: All Groups </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
 

<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
function fnGroupDetail(groupID, groupNM)
 
{   
	windowOpener("/gmGroupPartMap.do?strPartListFlag=Y&hviewEdit=view&groupId="+groupID+"&groupNM="+groupNM,"","resizable=yes,scrollbars=yes,top=250,left=300,width=950,height=400");
} 
 
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">

<html:form action="/gmGroupsReport.do"  > 
	<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
	<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtGroupsReport:message key="LBL_ALL_GROUPS"/></td>
			<td align="right" class=RightDashBoardHeader  >
			<fmtGroupsReport:message key="IMG_ALT_HELP" var="varHelp"/>
		<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				
		</td>
		</tr>
	 
		<tr><td colspan="5" height="1" class="line"> 
		</td></tr>
		
		 <tr>
	                     <td colspan="5">
	                     	<display:table  name="requestScope.frmGroupsReport.ldtResult" class="its" id="currentRowObject" freezeHeader="true" paneSize="600" requestURI="/gmGroupsReport.do" decorator="com.globus.displaytag.beans.DTGroupsReportWrapper" > 
							<fmtGroupsReport:message key="LBL_GROUP_NAME" var="varGroupName"/><display:column property="GROUPNM" title="${varGroupName}"    class="alignleft"/>
							<fmtGroupsReport:message key="LBL_GROUP_TYPE" var="varGroupType"/><display:column property="GROUPTYPE" title="${varGroupType}"    class="alignleft"/>
							<fmtGroupsReport:message key="LBL_SYSTEM" var="varSystem"/><display:column property="SETNM" title="${varSystem}"    class="alignleft"/>
							<fmtGroupsReport:message key="LBL_CREATED_BY" var="varCreatedBy"/><display:column property="CREATEDBY" title="${varCreatedBy}"    class="alignleft"/>
							<fmtGroupsReport:message key="LBL_CREATED_DATE" var="varCreatedDate"/><display:column property="CREATEDDATE" title="${varCreatedDate}"    class="alignleft"/> 
							</display:table>

    		             </td>
                    </tr> 
        
		 
    </table>		     	
 
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</html:form>
</HTML>

