 <%
/*********************************************************************************************************
 * File		 		: GmClassifyOrd.jsp
 * Desc		 		: This screen is used to define System Rule for Classify Order
 * Version	 		: 1.0
 * author			: Gopinathan
**********************************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtPublishSys" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- prodmgmnt\OrdClassification\GmPublishSys.jsp  -->

<fmtPublishSys:setLocale value="<%=strLocale%>"/>
<fmtPublishSys:setBundle basename="properties.labels.prodmgmnt.OrdClassification.GmPublishSys"/>

<%@ page import="java.util.ArrayList,java.util.HashMap"%>
<bean:define id="systemID" name="frmClassifyOrd" property="systemID" type="java.lang.String"> </bean:define>
<bean:define id="systemNM" name="frmClassifyOrd" property="systemNM" type="java.lang.String"></bean:define>
<bean:define id="editAccessFl" name="frmClassifyOrd" property="editAccessFl" type="java.lang.String"></bean:define>
<bean:define id="publishAccessFl" name="frmClassifyOrd" property="publishAccessFl" type="java.lang.String"></bean:define>
<bean:define id="hmReturn" name="frmClassifyOrd" property="hmReturn" type="java.util.HashMap"> </bean:define>
<%
String strHiddenToken = GmCommonClass.parseNull((String)session.getAttribute("Token"));
String strProdmgmntOrdJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT_ORDCLASSIFICATION");
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
%>
<html>
<head>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strProdmgmntOrdJsPath%>/GmClassifyOrd.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmRule.js"></script>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<%
String strMsgFl = "";
String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
java.util.Date publishedDate = null;
java.util.Date initiatedDate = null;
HashMap hmSysRule = (HashMap)hmReturn.get("hmSysRule");
ArrayList alResult = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALRESULT"));
if(hmSysRule != null){
	publishedDate = (java.util.Date) hmSysRule.get("PUBLISHEDDATE");
	initiatedDate = (java.util.Date) hmSysRule.get("INITIATEDDATE");
}
%>
<script type="text/javascript">
var listSize = '<%=alResult.size()%>';
</script>
<title>DO Classification Confirmation</title>
</head>
<html:form action="/gmClassifyOrd.do">
<body onload="" leftmargin="20" topmargin="10">
<html:hidden property="strOpt"/>
<html:hidden property="status"/>
<html:hidden property="systemID"/>
<html:hidden property="fwdGroupType"/>
<html:hidden property="fwdRuleTypeID"/>
<html:hidden property="hiddenToken" value="<%=strHiddenToken%>"/>

<!-- Custom tag lib code modified for JBOSS migration changes -->
<table  class="DtTable700" cellspacing="0" cellpadding="0" >
	<tr>
			<td colspan="4" class="RightDashBoardHeader"><fmtPublishSys:message key="LBL_DO_CLASSIFICATION_FOR"/> <%=systemNM%>: <fmtPublishSys:message key="LBL_CONFIRMATION_PAGE"/></td>
	</tr>
	<tr><td colspan="4" class="LLine" height="1"></td></tr> 
	<tr class="evenshade">
		<fmtPublishSys:message key="LBL_SYSYTEM" var="varSystem"/><gmjsp:label type="BoldText"  SFLblControlName="${varSystem}:" td="true"/>
		<td align="left">
			&nbsp;<%=systemNM%>
		</td>
		<fmtPublishSys:message key="LBL_STATUS" var="varStatus"/><gmjsp:label type="BoldText"  SFLblControlName="${varStatus}" td="true"/>
		<td align="left">
			<table>
				<tr>
					<td width="10">
						&nbsp;<bean:write name="frmClassifyOrd" property="statusNM" />
					</td>
					<td align="left">
						<logic:equal name="frmClassifyOrd" property="historyFL" value="Y"> 
							&nbsp;<fmtPublishSys:message key="IMG_ALT_OPEN" var = "varClickToOpen"/>
							<img  id="imgEdit" style="cursor:hand" onclick="javascript: fnLoadHistory ('1041','<%=systemID%>');" title="${varClickToOpen}" 	src="<%=strImagePath%>/icon_History.gif" align="bottom"  height=15 width=18 />
						</logic:equal>
					</td>
				</tr>
			</table>
		</td>
	<tr><td colspan="4" class="LLine" height="1"></td></tr> 
	<tr class="oddshade">
		<fmtPublishSys:message key="LBL_INITIATED_BY" var="varInitiatedBy"/><gmjsp:label type="BoldText"  SFLblControlName="${varInitiatedBy}" td="true"/>
		<td align="left">
			&nbsp;<bean:write name="frmClassifyOrd" property="initiatedNM" />
		</td>
		<fmtPublishSys:message key="LBL_INITIATED_BY" var="varInitiatedBy"/><gmjsp:label type="BoldText"  SFLblControlName="${varInitiatedBy}" td="true"/>
		<td align="left">
			&nbsp;<%=GmCommonClass.getStringFromDate(initiatedDate,strApplDateFmt)%>
		</td>		
	</tr>
	<tr><td colspan="4" class="LLine" height="1"></td></tr> 
	<tr class="evenshade">
		<fmtPublishSys:message key="LBL_PUBLISHED_BY" var="PublishedBy"/><gmjsp:label type="BoldText"  SFLblControlName="${PublishedBy}" td="true"/>
		<td align="left">
			&nbsp;<bean:write name="frmClassifyOrd" property="publishedNM" />
		</td>
		<fmtPublishSys:message key="LBL_PUBLISHED_BY" var="PublishedBy"/><gmjsp:label type="BoldText"  SFLblControlName="${PublishedBy}" td="true"/>
		<td align="left">
			&nbsp;<%=GmCommonClass.getStringFromDate(publishedDate,strApplDateFmt)%>
		</td>		
	</tr>
	<tr><td colspan="4" class="Line" height="1"></td></tr> 
	<tr>
		<td colspan="4">
			<table cellspacing="0" cellpadding="0">
			<tr  bgcolor="#e4e6f2" >
									<td class="RightTableCaption" align="left" HEIGHT="24" width="20%">&nbsp;<fmtPublishSys:message key="LBL_GROUP_TYPE"/></td>
									<td class="RightTableCaption" align="center" HEIGHT="24" width="10%">&nbsp;<fmtPublishSys:message key="LBL_MIN_QTY"/></td>
									<td class="RightTableCaption" align="left" HEIGHT="24" width="70%">&nbsp;&nbsp;&nbsp;<fmtPublishSys:message key="LBL_GROUP_NAME"/></td>
			</tr>
<%
for(int i=0;i<alResult.size();i++){
	HashMap hmLoop = new HashMap();
	ArrayList alLoop = new ArrayList();
	hmLoop =(HashMap)alResult.get(i);
	String strGroupTypeNM = GmCommonClass.parseNull((String)hmLoop.get("GROUPTYPENM"));
	String strQTY = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
	String strLinkedID = GmCommonClass.parseNull((String)hmLoop.get("LINKEDRULETYPEID")); 
	alLoop = GmCommonClass.parseNullArrayList((ArrayList)hmLoop.get("ALRULEGRP"));
	
%>
<%if(strLinkedID.equals("")){ %>
	<tr><td colspan="4" class="Line" height="1"></td></tr> 
	<tr class='<%=((i%2)==0)?"evenshade":"oddshade"%>'>
		<td align="left" width="20%" height="35">&nbsp;<%=strGroupTypeNM%></td>
		<td align="center" width="10%">&nbsp;<%=strQTY%>&nbsp;</td>
<%}else{ 
%>
	<tr><td colspan="4" class="LLine" height="1"></td></tr> 
	<tr class='<%=((i%2)==0)?"evenshade":"oddshade"%>'>
		<td align="left" width="20%" height="35">&nbsp;<%=strGroupTypeNM%></td>
		<td align="center" width="10%">&nbsp;</td>
<%} %>
		<td align="left" width="70%">
<%
	if(alLoop.size()>0){
		for(int j= 0;j<alLoop.size();j++){
			HashMap hmTemp = new HashMap();
			hmTemp = (HashMap)alLoop.get(j);
			String strGroupNM = (String)hmTemp.get("GROUPNM");
%>
			&nbsp;&nbsp;&nbsp;<%=strGroupNM%>
<%
			if(j==(alLoop.size()-1)){
%>
		</td>
	</tr>
<%				
			}else{
%>
				<BR>
<%				
			}
		}
	}else{
%>
				<input type=hidden value='<%=strGroupTypeNM%>' name="errGroup<%=i%>">	
<%			
	}
}

%>
			</table>
		</td>
	</tr>
	<tr><td colspan="4" class="Line" height="1"></td></tr> 
	<tr>                        
		<td colspan="4" align="center" height="30">
			<logic:equal name="frmClassifyOrd" property="strOpt" value="confirm"> 
				&nbsp;<fmtPublishSys:message key="BTN_BACK" var="varBack"/><gmjsp:button name="Btn_Back" value="&nbsp;${varBack}&nbsp;" gmClass="button" buttonType="Load" onClick="fnBack();"/>
			</logic:equal>
			<logic:equal name="frmClassifyOrd" property="status" value="11500"> 
<%if(publishAccessFl.equals("Y")){ %>
				&nbsp;<fmtPublishSys:message key="BTN_PUBLISH" var="varPublish"/><gmjsp:button name="Btn_Publish" value="${varPublish}" gmClass="button" buttonType="Save" onClick="fnPublish();"/>
<%} %>				
			</logic:equal>
			 <logic:equal name="frmClassifyOrd" property="strOpt" value="publish"> 
<%if(editAccessFl.equals("Y")|| publishAccessFl.equals("Y")){ %>			 
			&nbsp;<fmtPublishSys:message key="BTN_EDIT" var="varEdit"/><gmjsp:button name="Btn_Edit" value="&nbsp;${varEdit}&nbsp;" buttonType="Save" gmClass="button" onClick="fnEdit();"/>
<%} %>				
			</logic:equal>
		</td>
	</tr>
</table>
<%@ include file="/common/GmFooter.inc" %>
</html:form>
</body>
</html>