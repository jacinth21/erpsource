<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
	/**********************************************************************************
	 * File		 		: GmClassifyGrp.jsp
	 * Desc		 		: This screen is used for DO Classificatin Group Details
	 * Version	 		: 1.0
	 * author			: Gopinathan
	 ************************************************************************************/
%> 
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtClassifyGrp" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%> 
<!-- prodmgmnt\OrdClassification\GmClassifyGrp.jsp  -->
<fmtClassifyGrp:setLocale value="<%=strLocale%>"/>
<fmtClassifyGrp:setBundle basename="properties.labels.prodmgmnt.OrdClassification.GmClassifyGrp"/>


<bean:define id="alSetsGroup" name="frmClassifyGrp" property="alSetsGroup" type="java.util.List"></bean:define>
<bean:define id="alSysRuleGroup" name="frmClassifyGrp" property="alSysRuleGroup" type="java.util.List"></bean:define>
<bean:define id="curGroupType" name="frmClassifyGrp" property="curGroupType" type="java.lang.String"></bean:define>
<bean:define id="fwdGroupType" name="frmClassifyGrp" property="fwdGroupType" type="java.lang.String"></bean:define>
<bean:define id="screenTitle" name="frmClassifyGrp" property="screenTitle" type="java.lang.String"></bean:define>
<bean:define id="systemID" name="frmClassifyGrp" property="systemID" type="java.lang.String"></bean:define>
<bean:define id="systemNM" name="frmClassifyGrp" property="systemNM" type="java.lang.String"></bean:define>
<bean:define id="alSysAllRuleType" name="frmClassifyGrp" property="alSysAllRuleType" type="java.util.List"></bean:define>
<%
 
    String strProdmgmntOrdJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT_ORDCLASSIFICATION");
	ArrayList alListSetsGroup = new ArrayList();
	ArrayList alListSelGrpType = new ArrayList();	
	 
	alListSetsGroup = GmCommonClass.parseNullArrayList((ArrayList) alSetsGroup);  //System list
	alListSelGrpType = GmCommonClass.parseNullArrayList((ArrayList) alSysRuleGroup);  //Set list
	int rowsize1 = alListSetsGroup.size();
	int rowsize2 = alListSelGrpType.size(); 
	int ruleTypeSize = alSysAllRuleType.size(); 
	
	GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.prodmgmnt.OrdClassification.GmClassifyGrp", strSessCompanyLocale);
	
	HashMap hmSysLoop = new HashMap();
	HashMap hmSetLoop = new HashMap();
	HashMap hmSysLoopNxt = new HashMap();
	
	String strSystemId = "";
	String strSystemNm = "";
	String strGroupNm = ""; 
	String strGroupId = "";
	String tempsystem ="";
	String strNextSys =""; 
	String strLbl1 = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_PLEASE_SELECT"));
	String strLbl2 = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_USED_IN"));
	String strLbl3 = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_SYSTEM")); 
	String title =strLbl1+" "+screenTitle+" "+strLbl2+" "+systemNM+" "+strLbl2;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical:&nbsp;<%=title%></TITLE>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strProdmgmntOrdJsPath%>/GmClassifyGrp.js"></script>

<script>
var size ="<%=rowsize1 %>";
var setLen = <%=rowsize2%>;
var type = "<%=fwdGroupType%>";
var strSystemID = "<%=systemID%>";
var ruleTypeSize = <%=ruleTypeSize%>;

<%
HashMap hmGrpVal = new HashMap();
for (int i=0;i<alSysRuleGroup.size();i++)
{
	hmGrpVal = (HashMap)alSysRuleGroup.get(i);
%>
var	GroupArr<%=i%> = "<%=hmGrpVal.get("GROUPID")%>" ;
<%
}
%>

<%
HashMap hmRuleType = new HashMap();
for (int i=0;i<alSysAllRuleType.size();i++)
{
	hmRuleType = (HashMap)alSysAllRuleType.get(i);
%>
var	RuleType<%=i+1%> = "<%=hmRuleType.get("SYSRULETYPEID")%>,<%=hmRuleType.get("GRPTYPEID")%>" ;
<%
}
%>
</script>
</HEAD>
 
<BODY leftmargin="20" topmargin="10" onload="fnCheckSelections();">
<html:form action="/gmClassifyGrp.do" >
 <html:hidden property="strOpt"  />
 <html:hidden property="systemID"/>
 <html:hidden property="sysRuleTypeID"/>
 <html:hidden property="fwdRuleTypeID"/>
 <html:hidden property="curGroupType" value="<%=fwdGroupType%>"  />
 <html:hidden property="strSelectedGrpType"   />
 <html:hidden property="fwdGroupType"   />
 <html:hidden property="hinputStr"   />

   <div id="winVP" style="position: relative;  height: 1500px;   margin: 10px;">
	<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr >
						<td height="25" colspan="1" class="RightDashBoardHeader">&nbsp;<%=title%></td>
						 <td align="right" class=RightDashBoardHeader>
						</td> 
					</tr>
					<tr><td class="Line" height="1" colspan="2"></td></tr>
					 <tr>
						<td colspan="6">
							<table border="0" width="100%" cellspacing="0" cellpadding="0"   >
					<%
					int k = 0, j = 0, syscount = 0 ;
						for (int i = 0;i < rowsize1 ;i++ )
								{ 	
									hmSysLoop = (HashMap)alListSetsGroup.get(i);
									 
									 if (i< (rowsize1 -1))
									 {
										 hmSysLoopNxt = (HashMap)alListSetsGroup.get(i+1);
										 strNextSys = GmCommonClass.parseNull((String)hmSysLoopNxt.get("SYSTEMID")); 
										}
									  
									strSystemId = GmCommonClass.parseNull((String)hmSysLoop.get("SYSTEMID"));
									strSystemNm = GmCommonClass.parseNull((String)hmSysLoop.get("SYSTEMNM"));
									strGroupId = GmCommonClass.parseNull((String)hmSysLoop.get("GROUPID")); 
									strGroupNm = GmCommonClass.parseNull((String)hmSysLoop.get("GROUPNM"));
									
									if (!tempsystem.equals(strSystemId))
									{  j = 1;
										syscount++;
									%>
							<tr  class="oddshade" >
							  <td  class="RightTableCaption" colspan="8" align="left">  
							       	<input type="checkbox" name='sysCheck<%=strSystemId.replace(".","")%>'  onclick="fnShow('div<%=strSystemId %>','<%=strSystemId%>');">
								    <%=strSystemNm %> &nbsp; 
							 </td>
									<td  >&nbsp; 
									</td>
								</tr>		
							<tr><td colspan="9" height="1" bgcolor="#cccccc"></td></tr>
							<tr    >
							<td colspan="9">
							<table border="0" width="100%" cellspacing="0" cellpadding="0"  id="div<%=strSystemId%>"  style="display:none">
								<tr  ><td class="LLine" height="1" colspan="9"></td></tr>
						<%
									}
									
							 %>	
							 
								<tr     >
									<td   align="left" HEIGHT="24"   >
									<input type=hidden value='<%=strGroupId%>' name="GroupId<%=k%>">
									<input type=hidden value='<%=strSystemId%>' name="System<%=k%>">
									&nbsp;<%=strGroupNm%>
									</td>
									<td class="RightTableCaption" align="center" HEIGHT="24" width="10%"  > 	<input type="checkbox" name="check<%=k%>" onclick="fnUnCheckSelections('<%=strSystemId%>');" >  </td>
								</tr>	
								
								<tr><td colspan="9" height="1" bgcolor="#cccccc"></td></tr>
								
						<%		 	 
							if ((tempsystem.equals(strSystemId)&&!strNextSys.equals(strSystemId))||(i== rowsize1 -1)||(!tempsystem.equals(strSystemId)&&!strNextSys.equals(strSystemId)))
							{ 
						%>
								</table>
							</td>
						</tr>
						
						<% 
							}
						tempsystem = strSystemId;
						k ++;
						j ++;
						}
						%>		
							  
								
							</table>
						</td>
					</tr>
					 
					
					
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr  >
						  <td  class="RightTableCaption" align="left" height="25" colspan="2">
						  
						  				  
						  </td>
						 
					</tr> 
				
				
  
				   <tr>
						<td  height="40" colspan="2" align="center">&nbsp;
							<fmtClassifyGrp:message key="BTN_BACK" var="varBack"/><gmjsp:button style="width: 5em" value="${varBack}" gmClass="button" buttonType="Load" onClick="fnBack();" />&nbsp;
							<fmtClassifyGrp:message key="BTN_SAVE_DRAFT" var="varSaveDraft"/><gmjsp:button style="width: 8em" value="${varSaveDraft}" gmClass="button" buttonType="Save" onClick="fnSubmit();" />&nbsp;
							<fmtClassifyGrp:message key="BTN_NEXT" var="varNext"/><gmjsp:button style="width: 5em" value="${varNext}" gmClass="button" buttonType="Load" onClick="fnNext();" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
 
    </table></div>
<%@ include file="/common/GmFooter.inc" %>
</html:form>
</BODY>
</HTML>
 

