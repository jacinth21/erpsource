 <%
/*********************************************************************************************************
 * File		 		: GmClassifyOrd.jsp
 * Desc		 		: This screen is used to define System Rule for Classify Order
 * Version	 		: 1.0
 * author			: Gopinathan
**********************************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtClassifyOrd" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\OrdClassification\GmClassifyOrd.jsp  -->
<fmtClassifyOrd:setLocale value="<%=strLocale%>"/>
<fmtClassifyOrd:setBundle basename="properties.labels.prodmgmnt.OrdClassification.GmClassifyOrd"/>


<bean:define id="systemID" name="frmClassifyOrd" property="systemID" type="java.lang.String"> </bean:define>
<bean:define id="gridData" name="frmClassifyOrd" property="strXMLGrid" type="java.lang.String"> </bean:define>
<%
String strHiddenToken = GmCommonClass.parseNull((String)session.getAttribute("Token"));

String strProdmgmntOrdJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT_ORDCLASSIFICATION");
%>
<html>
<head>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strProdmgmntOrdJsPath%>/GmClassifyOrd.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<TITLE> Globus Medical:&nbsp;DO Classification</TITLE>
<script>
var objGridData;
objGridData ='<%=gridData%>';
</script>
</head>
<html:form action="/gmClassifyOrd.do">
<body onload="javascript:fnOnPageLoad();" leftmargin="20" topmargin="10">
<html:hidden property="strOpt"/>
<html:hidden property="status"/>
<html:hidden property="fwdGroupType"/>
 <html:hidden property="fwdRuleTypeID"/>
<html:hidden property="inputString"/>
<html:hidden property="hiddenToken" value="<%=strHiddenToken%>"/>
<input type= "hidden"  name="hDeletedRowStr" />
<table  class="DtTable700" cellspacing="0" cellpadding="0" >
	<tr>
			<td colspan="2" class="RightDashBoardHeader"><fmtClassifyOrd:message key="LBL_DO_CLASSIFICATION"/></td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr> 
	<tr>
		<td colspan="2">
			<table>
				<tr>
				<td width="35%" align="right" class="RightTableCaption" >
				<!-- Custom tag lib code modified for JBOSS migration changes -->
					<fmtClassifyOrd:message key="LBL_SYSTEM" var="varSystem"/><gmjsp:label type="BoldText" SFLblControlName="${varSystem}" td="false"/>
				</td>
				<td align="left">
					&nbsp;<gmjsp:dropdown controlName="systemID" SFFormName="frmClassifyOrd" SFSeletedValue="systemID" SFValue="alSystem" codeId="ID" codeName="NAME" defaultValue="[Choose One]" onChange="fnLoad(this.value);" />
				</td>
				</tr>
				<tr>
					<td class="RightTextBlue" colspan="2" align="center">
					
						<fmtClassifyOrd:message key="LBL_CHOOSE_ONE"/>				
			    	</td>
				</tr>
			</table>
		</td>
	</tr>
<%
if(!systemID.equals("")&& !systemID.equals("0")){
%>
	<tr><td colspan="2" class="Line" height="1"></td></tr> 
	<tr>
		<td class="RightTableCaption" align="left" colspan="2" height="20">
			<table cellpadding="1" cellspacing="1" id="addTbl">
				<TR>
					<td class="RightTableCaption">
						&nbsp;<a href="#"> <fmtClassifyOrd:message key="IMG_ALT_ADD" var = "varAdd"/><img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="${varAdd}" style="border: none;" onClick="javascript:addRow()"height="14"></a>
						&nbsp;<a href="#">  <fmtClassifyOrd:message key="IMG_ALT_REMOVE" var = "varRemove"/><img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="${varRemove}" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a>
					</td>										
				</TR>
			</table>
		</td>				
	</tr>
	<tr id="gridData"> 
		<td  colspan="2">
			<div id="dataGridDiv" height="400px" width="700px"></div>
		</td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr> 
	<tr>                        
		<td colspan="2" align="center" height="30">
		<logic:equal name="frmClassifyOrd" property="strOpt" value="edit"> 
			&nbsp;<fmtClassifyOrd:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button name="Btn_Submit" value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnSubmit();"/>&nbsp;
			&nbsp;<fmtClassifyOrd:message key="BTN_NEXT" var="varNext"/><gmjsp:button name="Btn_Next" value="${varNext}" gmClass="button" buttonType="Load" onClick="fnNext();"/>
		</logic:equal>
		</td>
	</tr>
<%
}
%>	
</table>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html:form>
</html>