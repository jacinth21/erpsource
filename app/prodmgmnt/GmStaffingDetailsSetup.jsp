
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ taglib prefix="fmtStaffingDetailsSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\GmStaffingDetailsSetup.jsp -->
<fmtStaffingDetailsSetup:setLocale value="<%=strLocale%>"/>
<fmtStaffingDetailsSetup:setBundle basename="properties.labels.prodmgmnt.GmStaffingDetailsSetup"/>
<bean:define id="alRequest" name="frmStaffingDetails" property="alResult" type="java.util.ArrayList"/>
<bean:define id="alRequest" name="frmStaffingDetails" property="alResult" type="java.util.ArrayList"/>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Staffing Detail Setup</title>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
</head>

<body leftmargin="20" topmargin="10">
<html:form action="/gmPDStaffingDetail.do">
<html:hidden property="strOpt"/>
<html:hidden property="prjId"/>
<html:hidden property="hstaffingdetailsId"/>
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value=""/>

<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="2" height="15"></td>
	</tr>
	 <tr>
		 <td colspan="2" width="100%" height="100" valign="top">
			<table border="0" width="100%" cellspacing="0" cellpadding="0"> 
				<tr>
					<td class="RightTableCaption" align="right" width="40%" height="24"><fmtStaffingDetailsSetup:message key="LBL_MEMBER_TYPE"/>&nbsp;:</td>
					<td width="60%">&nbsp; <gmjsp:dropdown controlName="partyType" SFFormName="frmStaffingDetails" SFSeletedValue="partyType"
					SFValue="alPartyTypes" onChange="fnLoadTeamRec(this.form);" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"/>
					</td>
				</tr>
				<tr><td colspan="2" height="15"></td></tr>
				<tr><td colspan="2" class="Line" height="1"></td></tr>
				<tr class="Shade">
					<td class="RightTableCaption" align="right" width="40%" height="24"><fmtStaffingDetailsSetup:message key="LBL_MEMBER_NAME"/>&nbsp;:</td>
					<td width="60%">&nbsp; <gmjsp:dropdown controlName="partyId" SFFormName="frmStaffingDetails" SFSeletedValue="partyId"
					SFValue="alPartyNames" codeId="ID"  codeName="NAME"  defaultValue="[Choose One]"/>
					</td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr>
                	<td class="RightTableCaption" align="right" width="40%" height="24"><fmtStaffingDetailsSetup:message key="LBL_ROLE"/>&nbsp;:</td>
					<td width="60%">&nbsp; <gmjsp:dropdown controlName="role" SFFormName="frmStaffingDetails" SFSeletedValue="role"
					SFValue="alRoles" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"/>
					</td>
                </tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr class="Shade">
                	<td class="RightTableCaption" align="right" width="40%" height="24"><fmtStaffingDetailsSetup:message key="LBL_STATUS"/>&nbsp;:</td> 
                	<td width="60%">&nbsp;&nbsp;<gmjsp:dropdown controlName="status" SFFormName="frmStaffingDetails" SFSeletedValue="status"
					SFValue="alStatus" codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"/></td>
                </tr>
                <tr><td colspan="2" class="LLine" height="1"></td></tr>
                <tr>
						<td colspan="2" align="left" HEIGHT="25">&nbsp;
	                    	<jsp:include page="/common/GmIncludeLog.jsp" >
		                    	<jsp:param name="FORMNAME" value="frmStaffingDetails" />
								<jsp:param name="ALNAME" value="alLogReasons" />
								<jsp:param name="LogMode" value="Edit" />																					
							</jsp:include>	
						</td>
				</tr>
		    	<tr>
					<td colspan="2" align="center" height="24">&nbsp;
						<fmtStaffingDetailsSetup:message key="BTN_SUBMIT" var="varSubmit"/>
						<gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnPrjTeamSubmit(this.form);"/>&nbsp;&nbsp;
						<fmtStaffingDetailsSetup:message key="BTN_RESET" var="varReset"/>
						<gmjsp:button value="${varReset}" gmClass="button" buttonType="Save" onClick="fnStaffReset(this.form);"/>&nbsp;&nbsp; 
						<fmtStaffingDetailsSetup:message key="BTN_VOID" var="varVoid"/>
						<gmjsp:button value="${varVoid}" gmClass="button" buttonType="Save" onClick="fnVoidStaff(this.form);"/>&nbsp;&nbsp; 
					</td>
				</tr>				
				<tr><td colspan="2" class="LLine" height="1"></td></tr>				
                <tr> 
		            <td colspan="2">
    					<jsp:include page="/gmPDStaffingDetail.do?method=loadStaffingDetailsReport" flush="true">
    						<jsp:param name="TYPE" value="Setup" />
    					</jsp:include>		            	
					</td>
	    	  	</tr>	
			</table>
		</td>
	</tr>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>