 <%
/**********************************************************************************
 * File		 		: GmProjectReport.jsp
 * Desc		 		: This screen is used for the Project Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<!-- prodmgmnt\GmProjectReport.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtProjectReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtProjectReport:setLocale value="<%=strLocale%>"/>
<fmtProjectReport:setBundle basename="properties.labels.prodmgmnt.GmProjectReport"/>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strWikiTitle = GmCommonClass.getWikiTitle("PROJECT_REPORT");
	
	ArrayList alProjectStatus = new ArrayList();
    ArrayList alGroup = new ArrayList();
    ArrayList alSegment = new ArrayList();
    ArrayList alTechList = new ArrayList();
	

	ArrayList alReturn = (ArrayList)request.getAttribute("alReturn");
	HashMap hmReturn = (HashMap)request.getAttribute("HMRETURN");
	HashMap hmParam = (HashMap)request.getAttribute("HMPARAM");
	
	alProjectStatus = (ArrayList)hmReturn.get("STATUS");
    alGroup = (ArrayList)hmReturn.get("GROUP");
    alSegment = (ArrayList)hmReturn.get("SEGMENT");
    alTechList = (ArrayList)hmReturn.get("TECHNIQUE");
    
	int intLength = alReturn.size();

	int intSize = 0;
	HashMap hmLoop = new HashMap();

	String strProjectStatus = "";
	String strGroupId = "";
	String strSegmentId = "";
	String strTechnique = "";
	String strDisplayTDateFmt = "{0,date,"+strGCompDateFmt+"}";
	
	String strShade = "";
	String strDeptId = (String)session.getAttribute("strSessDeptId")==null?"":(String)session.getAttribute("strSessDeptId");
	boolean bolAccess = false;
	if (strDeptId.equals("Z") || strDeptId.equals("P"))
	{
		bolAccess = true;
	}
	
	if (hmParam != null)
	{
		strProjectStatus = GmCommonClass.parseNull((String)hmParam.get("STATUSID"));
		strGroupId = GmCommonClass.parseNull((String)hmParam.get("GROUPID"));
		strSegmentId = GmCommonClass.parseNull((String)hmParam.get("SEGMENTID"));
		strTechnique = GmCommonClass.parseNull((String)hmParam.get("TECHNIQUE"));
	}
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Project Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style> 
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmProjectReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>


</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onkeypress="fnEnter();">
<FORM name="frmProject" method="POST" action="<%=strServletPath%>/GmProdReportServlet">
<input type="hidden" name="hColumn" value="">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hTxnId" value="">
<input type="hidden" name="hPgToLoad" value="GmProjectServlet">
<input type="hidden" name="hSubMenu" value="Setup">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hAttributeVal" value="">

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>	
		<tr>
			<td bgcolor="#666666" width="1" rowspan="5"></td>
			<td class="RightDashBoardHeader" colspan="4" height="28">&nbsp;<fmtProjectReport:message key="LBL_PROJECTS"/></td>
			<td align="right" class=RightDashBoardHeader > <fmtProjectReport:message key="IMG_ALT_HELP" var="varHelp"/>	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp; 
			</td>
			<td bgcolor="#666666" width="1" rowspan="5"></td>
		</tr>
		<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>			
		<tr>
			<td class="RightTableCaption"  align = "right" HEIGHT="30" width="10%"> &nbsp;<fmtProjectReport:message key="LBL_GROUP"/>:&nbsp; </td>
			<td width="40%"> <gmjsp:dropdown controlName="Cbo_Group"  seletedValue="<%=strGroupId%>" 	
					tabIndex="1"   value="<%=alGroup%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"  />
			</td>
			<td class="RightTableCaption"  align = "right" HEIGHT="30" width="15%"> &nbsp;<fmtProjectReport:message key="LBL_SEGMENT"/>:&nbsp; </td>
			<td width="35%" colspan="2"> <gmjsp:dropdown controlName="Cbo_Segment"  seletedValue="<%=strSegmentId%>" 	
					tabIndex="1"   value="<%=alSegment%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"  />
			</td>
		</tr>
		<tr><td colspan="6" class="LLine"></td></tr>
		<tr class="Shade">
			<td class="RightTableCaption"  align = "right" HEIGHT="30"> &nbsp;<fmtProjectReport:message key="LBL_STATUS"/>:&nbsp; </td>
			<td> <gmjsp:dropdown controlName="Cbo_Status"  seletedValue="<%=strProjectStatus%>" 	
					tabIndex="1"   value="<%=alProjectStatus%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"  />
			</td>
			<td class="RightTableCaption"  align = "right" HEIGHT="30"> &nbsp;<fmtProjectReport:message key="LBL_TECHNIQUE"/>:&nbsp; </td>
			<td colspan="2"> <gmjsp:dropdown controlName="Cbo_Technique"  seletedValue="<%=strTechnique%>" 	
					tabIndex="1"   value="<%=alTechList%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"  />
				&nbsp;&nbsp;&nbsp;	<fmtProjectReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}&nbsp;&nbsp;" name="Btn_Load" gmClass="button" buttonType="Load" onClick="fnGo();" tabindex="1" />
			</td>
		</tr>
		<tr>
			<td colspan="6" height="1" bgcolor="#666666"></td>
		</tr>
		<tr> <td colspan="6">
		<display:table name="alReturn" class="its" id="currentRowObject" export="true" decorator="com.globus.displaytag.beans.DTProjectReportWrapper" uid="row"> 
			<display:setProperty name="export.excel.decorator" value="com.globus.displaytag.beans.DTProjectReportWrapper" />
			<display:setProperty name="export.excel.filename" value="ProjectReport.xls" /> 
		<display:column class="alignleft"><a href="#" onClick="javascript:fnLookup('${row.ID}','${row.NAME}');"><img alt="Inventory Lookup" src="/images/location.png"  border="0"></a></display:column>
		<fmtProjectReport:message key="LBL_GROUP" var="varGroup"/><display:column property="GROUPNAMEEXCEL" title="${varGroup}" media="excel" sortable="true"  />	
		<fmtProjectReport:message key="LBL_SEGMENT" var="varSegment"/><display:column property="SEGMENTEXCEL" title="${varSegment}" media="excel" sortable="true"  />
		<fmtProjectReport:message key="LBL_TECHNIQUE" var="varTechnique"/><display:column property="TECHNIQUEEXCEL" title="${varTechnique}" media="excel" sortable="true"  />
		<fmtProjectReport:message key="LBL_GROUP" var="varGroup"/><display:column property="GROUPNAME" title="${varGroup}" media="html" sortable="true"  />	
		<fmtProjectReport:message key="LBL_SEGMENT" var="varSegment"/><display:column property="SEGMENT" title="${varSegment}" media="html" sortable="true"  />
		<fmtProjectReport:message key="LBL_TECHNIQUE" var="varTechnique"/><display:column property="TECHNIQUE" title="${varTechnique}" media="html" sortable="true"  />
		<fmtProjectReport:message key="LBL_PROJECT_ID" var="varProjectID"/><display:column property="ID" title="${varProjectID}" media="excel" sortable="true"  />
		<fmtProjectReport:message key="LBL_PROJECT_NAME" var="varProjectName"/><display:column property="NAME" title="${varProjectName}" media="excel" sortable="true"  />		
		<fmtProjectReport:message key="LBL_PROJECT_ID" var="varProjectID"/><display:column property="IDLINK" title="${varProjectID}" media="html" sortable="true"  />
		<fmtProjectReport:message key="LBL_PROJECT_NAME" var="varProjectName"/><display:column property="NAMELINK" title="${varProjectName}" media="html" sortable="true"  />
		<fmtProjectReport:message key="LBL_PROJECT_MANAGER" var="varProjectManager"/><display:column property="PMANAGER" title="${varProjectManager}" sortable="true" class="alignleft"/>
		<fmtProjectReport:message key="LBL_START_DATE" var="varStartDate"/><display:column property="CDATE" title="${varStartDate}" class="aligncenter" format="<%=strDisplayTDateFmt%>"/> <!-- Using ID alias Name to display the View Icon for corrresponding Action-->
		<fmtProjectReport:message key="LBL_STATUS" var="varStatus"/><display:column property="STATUS" title="${varStatus}" sortable="true"  />
		</display:table>
		</td> </tr>
    </table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
