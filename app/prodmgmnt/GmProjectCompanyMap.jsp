<%
/**********************************************************************************
 * File		 		: GmProjectCompanyMap.jsp
 * Desc		 		: This screen is used to fetch project company mapping details
 * Version	 		: 1.0
 * author			: rdinesh
************************************************************************************/
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- prodmgmnt\GmProjectCompanyMap.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtProjectCompanyMap" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtProjectCompanyMap:setLocale value="<%=strLocale%>"/>
<fmtProjectCompanyMap:setBundle basename="properties.labels.prodmgmnt.GmProjectCompanyMap"/>

<bean:define id="gridData" name="frmProjectDetails" property="gridXmlData" type="java.lang.String"> </bean:define> 
<%
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: Project - Company Mapping</title>
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">

<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmProject.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script>
	var objGridData;
	objGridData = '<%=gridData%>';
</script>

</HEAD>
<BODY onload="fnOnPageLoad();">

<table  border="0" width="100%" cellspacing="0" cellpadding="0" >
	<tr>	
		<td>
			<div id="dataGridDiv"  style="width:695px;height:500px;"></div>
		</td>
		</tr>
		<tr>						
		<td align="center"><br>
	             		<div class='exportlinks'><fmtProjectCompanyMap:message key="DIV_EXPORT_OPT"/>: <img src='img/ico_file_excel.png' />&nbsp; <a href="#" onclick="fnExport();"> 
<fmtProjectCompanyMap:message key="DIV_EXCEL"/> </a> </div> </td>
    <tr>
		<%if( gridData.indexOf("cell") == -1){%>
		<tr>
			<td colspan="3" align="center"><div  id="ShowDetails" align="center"></div></td>							
		</tr>
		<%} %>	  
</table>			

<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
