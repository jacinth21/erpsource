 <%
/**********************************************************************************
 * File		 		: GmMultiSetreport.jsp
 * Desc		 		: This screen is used for displaying MultiSet Part Report
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.*,java.text.*" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<!-- Imports for Logger -->

<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<%@ taglib prefix="fmtMultiSetPartReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\GmMultiSetPartReport.jsp -->
<fmtMultiSetPartReport:setLocale value="<%=strLocale%>"/>
<fmtMultiSetPartReport:setBundle basename="properties.labels.prodmgmnt.GmMultiSetPartReport"/>
<%
try {
 
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: MultiSet Part Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
 

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmMultiSetPartReport.do"  >
<html:hidden property="strOpt" />


	<table border="0" class="DtTable900" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtMultiSetPartReport:message key="LBL_MULTISET"/></td>
		</tr>
		
				
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td colspan="2">
			<display:table name="requestScope.frmMultiSetPartReport.ldtResult" requestURI="/gmMultiSetPartReport.do?method=multiSetPartReport" export="true" class="its" id="currentRowObject">
				<display:setProperty name="export.excel.filename" value="MultiSetPartReport.xls" /> 
			 	<fmtMultiSetPartReport:message key="LBL_PART_NUMBER" var="varPartNumber"/><display:column group="1" property="PNUM" title="${varPartNumber}" class="aligncenter" sortable="true" />
				<fmtMultiSetPartReport:message key="LBL_DESCRIPTION" var="varDescription"/><display:column group="2" property="PARTDESC" escapeXml="true" title="${varDescription}" class="alignleft"  />
				<fmtMultiSetPartReport:message key="LBL_SET_ID" var="varSetId"/><display:column property="SETID" title="${varSetId}" sortable="true" class="alignleft" />
				<fmtMultiSetPartReport:message key="LBL_SET_DESCRIPTION" var="varSetDescription"/><display:column property="SETDESC" title="${varSetDescription}" sortable="true" class="alignleft" />
				<fmtMultiSetPartReport:message key="LBL_SET_QTY" var="varSetQty"/><display:column property="SETQTY" title="${varSetQty}" sortable="true" class="aligncenter" />
				<fmtMultiSetPartReport:message key="LBL_IN_SET_FLAG" var="varInSetFlag"/><display:column property="INSETFL" title="${varInSetFlag}" sortable="true" class="aligncenter" />
				<fmtMultiSetPartReport:message key="LBL_CRITICAL_FLAG" var="varCriticalFlag"/><display:column property="CRITFL" title="${varCriticalFlag}" sortable="true" class="aligncenter" />
				<fmtMultiSetPartReport:message key="LBL_CRITICAL_QTY" var="varCriticalQty"/><display:column property="CRITQTY" title="${varCriticalQty}" sortable="true" class="aligncenter" />
			</display:table>
			</td>
		</tr>
    <table>
    
     

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</html:form>
</BODY>

</HTML>