
<%
/**********************************************************************************
 * File		 		: GmSystemReleaseMapping.jsp
 * Desc		 		: System Release Mapping
************************************************************************************/
%>
<!-- prodmgmnt\GmSystemReleaseMapping.jsp -->
<!--PMT#37505-system release to region in system setup-->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtSystemReleaseMapping"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page isELIgnored="false"%>
<fmtSystemReleaseMapping:setLocale value="<%=strLocale%>" />
<fmtSystemReleaseMapping:setBundle
	basename="properties.labels.prodmgmnt.GmSystemReleaseMapping" />

<bean:define id="setId" name="frmSystemDetails" property="setId"
	scope="request" type="java.lang.String"></bean:define>
<bean:define id="setGroupType" name="frmSystemDetails"
	property="setGroupType" scope="request" type="java.lang.String"></bean:define>
<bean:define id="frompage" name="frmSystemDetails"
	property="frompage" scope="request" type="java.lang.String"></bean:define>
<bean:define id="buttonAccessFl" name="frmSystemDetails" property="buttonAccessFl" type="java.lang.String"> </bean:define>

<%
	String strProdmgmntJsPath = GmFilePathConfigurationBean
			.getFilePathConfig("JS_PRODMGMNT");

	String strButtonFlg = "";
	String strButtonDisabled = "true";
	
	strButtonFlg = GmCommonClass.parseNull(buttonAccessFl);
	if(strButtonFlg.equals("Y"))
	{
		strButtonDisabled = "false";
	}
%>

<html>
<head>
<title>System Release Mapping</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="<%=strCssPath%>/displaytag.css">
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmGridCommon.js"></script>
<script language="javascript"
	src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript"
	src="<%=strProdmgmntJsPath%>/GmSystemReleaseMapping.js"></script>

<script>

var strSetId = '<%=setId%>';
var strGroupType = '<%=setGroupType%>';
//PMT_37772_Set release to Region in Set Setup screen
var strFromPage = '<%=frompage%>';
</script>

</head>
<body leftmargin="5" topmargin="5" onload="fnOnPageLoad();">
	<html:form action="/gmSystemPublish.do">
		<table border="0" class="DtTable600" cellspacing="0" cellpadding="0">


			<tr>
				<td colspan="6">
					<div id="dataGridDiv" height="550px" width="650px"></div>
				</td>
			</tr>

			<tr>
				<td class="LLine" colspan="6" height="1"></td>
			</tr>

			<tr>
			 	<td align="center" height="30">		   
					  <fmtSystemReleaseMapping:message key="BTN_SUBMIT" var="varSubmit" />
					  <gmjsp:button name="BTN_SUBMIT" value="${varSubmit}"
						gmClass="button" onClick="fnSubmit();" disabled="<%=strButtonDisabled %>" buttonType="save">
					 </gmjsp:button>
					   
					   	
					</td>
			</tr>

			</html:form>
</body>
<%@ include file="/common/GmFooter.inc"%>
</html>
