 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmPartNumMap.jsp
 * Desc		 		: This screen is used for mapping parent part to its sub part
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<!-- \prodmgmnt\GmPartNumMap.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.regex.Pattern,java.util.regex.Matcher" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>

<!-- Imports for Logger -->
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ taglib prefix="fmtPartNumMap" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmPartNumMap.jsp -->
<fmtPartNumMap:setLocale value="<%=strLocale%>"/>
<fmtPartNumMap:setBundle basename="properties.labels.prodmgmnt.GmPartNumMap"/>
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);


	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmParam = (HashMap)request.getAttribute("PARAM");
	HashMap hmPPartDetails = new HashMap();
	HashMap hmTemp;
	String strhAction = (String)request.getAttribute("hAction");
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strProjId =	"";
	String strProjNm = "";
	String strPartNum = "";
	String strPartNumDesc = "";
	String strPartLabel = "";
	String strPartDesc = "";
	String strPartType = "30031";
	String strPartQty = "";
	String strParentPNum = "";
	String strSubAsmPNum = "";
	String strCboCtrlName = "";
	String strPartMapId = "";
	String strType = "";
	String strSubParts = "";
	String strMessage = "";
	String strDisabled ="";	
	String strMasterDisable = "";
	String strSubAsmDisable = "";
	String strPartStatusMsg = "";
	String strRevLevel = "";

	ArrayList alProjList = new ArrayList();
	ArrayList alDataList = new ArrayList();
	ArrayList alPartList = new ArrayList();	
	ArrayList alPartType = new ArrayList();
	int intDataSize = 0;
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.prodmgmnt.GmPartNumMap", strSessCompanyLocale);
	String	strHeaderPartLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PART_DESCRIPTION"));
	String	strComponentLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SUB_COMPONENT_PART"));
	

	strProjId = GmCommonClass.parseNull((String)request.getAttribute("hProjId"));
	strPartNum = GmCommonClass.parseNull((String)request.getAttribute("hPartNum"));
	String strDeptId = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
	
	if (hmReturn != null)
	{
		alProjList = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("PROJECTS"));
		alPartList = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("PARTNUMS"));
		hmPPartDetails = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("PPARTDETAILS"));
		log.debug(" hmPPartDetails " + hmPPartDetails);
		
		strPartNumDesc = GmCommonClass.parseNull((String)hmPPartDetails.get("PDESC"));
		strPartStatusMsg = GmCommonClass.parseNull((String)hmPPartDetails.get("STATUSMSG"));
		strDisabled = GmCommonClass.parseNull((String)hmReturn.get("PDISABLED"));

		if (strhAction.equals("LoadPartDetails"))
		{
			alDataList = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("DATALIST"));
			alPartType = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("PARTTYPE"));
			intDataSize = alDataList.size();
		}
	}
	
	if (hmParam != null)
	{
		   strParentPNum = GmCommonClass.parseNull((String)hmParam.get("PPNUM"));
           strSubAsmPNum = GmCommonClass.parseNull((String)hmParam.get("SCPNUM")); 
           strType = GmCommonClass.parseNull((String)hmParam.get("TYPE")); 
           strSubParts = GmCommonClass.parseNull((String)hmParam.get("SUBPART")); 
        //   hmParam.put("TYPE",strType);
	}
	
	if (strType.equals("subasm"))
	{
		strHeaderPartLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SUBCOMPONENT_DESCRIPTION"));
		strComponentLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PARENT_PART"));
	}
	
	if(strDisabled.equals("disabled"))
	{
		strMessage =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CURENT_STATUS")) + strPartStatusMsg + GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PLEASE_CONTACT_SYSTEM_ADMINISTRATOR"));
	}
	log.debug(" strAction " + strhAction);
	log.debug(" strMessage " + strMessage);
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Number Assembly Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
function fnCallProject()
{
	document.frmPartNumMap.hOpt.value = 'LoadPartFromProject';
	document.frmPartNumMap.hAction.value = 'Dummy';
  	document.frmPartNumMap.submit();
}

function fnSetType(val)
{
	document.frmPartNumMap.hType.value = val;
}

function fnLoadPart()
{
	var varPartVal = '0';
	document.frmPartNumMap.hOpt.value = 'LoadPartFromProject';
	document.frmPartNumMap.hAction.value = 'LoadPartDetails';
	varsubasmpart = eval("document.frmPartNumMap.Txt_SubAsmPart");
	if (varsubasmpart)
		document.frmPartNumMap.Txt_SubAsmPart.value = '';

	var varProjName = document.frmPartNumMap.Cbo_Proj.value;
	var varPart = document.frmPartNumMap.Cbo_Part.value;
	var varPartNum = document.frmPartNumMap.Txt_PartNum.value;

	if (varProjName == '0' && varPart == '0' && varPartNum == '')
	{
		Error_Details(message[5526]);
		
	}
	
		if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
		fnStartProgress('Y');
	document.frmPartNumMap.submit();
}

function fnAddPart()
{
	document.frmPartNumMap.hOpt.value = 'LoadPartFromProject';
	document.frmPartNumMap.hAction.value = 'AddPartDetails';
	document.frmPartNumMap.submit();
}

function fnOpenPart()
{
		varPartNum = document.frmPartNumMap.Txt_PartNum.value; 
		windowOpener("/GmSearchServlet?hAction=SearchPart&Txt_AccId=PARTLOOKUP&Txt_PartNum="+encodeURIComponent(varPartNum)+"&hCount=1","Search","resizable=yes,scrollbars=yes,top=340,left=420,width=600,height=435");
}

function fnSubmit()
{
	Error_Clear();
	varsubasmpart = eval("document.frmPartNumMap.Txt_SubAsmPart");
	if (varsubasmpart)
		document.frmPartNumMap.Txt_SubAsmPart.value = '';
		
	var vardeptid = '<%=strDeptId%>';

	var varCount = document.frmPartNumMap.hCount.value;
	var inputString = '';
	var inputpart = document.frmPartNumMap.hInputPnum.value;
	var varType = document.frmPartNumMap.hPartType.value;
	var varAccessError = '';
	var varAccessErrorP = '';
	var varQtyError = '';
	
	for (var i =0; i < varCount; i++) 
	{
		obj = eval("document.frmPartNumMap.Txt_Qty"+i);
		qty = obj.value;
		partnum = obj.id;
		
		if (qty <=0 )
		{
			varQtyError = varQtyError + "," + partnum;
		}
		
		obj = eval("document.frmPartNumMap.Cbo_PartType"+i);
		partType = obj.value;

		obj = eval("document.frmPartNumMap.Chk_part"+i);		
		if (obj.checked && varType == 'pnum')
		{
			inputString = inputString + inputpart + '^'+ partnum+'^'+qty+'^'+partType + '|';
		}
		else if (obj.checked && varType == 'subasm')
		{
			inputString = inputString + partnum + '^'+ inputpart+'^'+qty+'^'+partType + '|';
		}

		if (obj.checked)
		{
			if ( vardeptid == 'W' && partType == '30031')
			{
				varAccessError = varAccessError + ","+ partnum;
			}
			else if (vardeptid == 'P' && partType != '30031') 
			{
				varAccessErrorP = varAccessErrorP + ","+ partnum;
			}			
		}

	}
	document.frmPartNumMap.hInputString.value = inputString;
	document.frmPartNumMap.hAction.value = 'SavePartDetails';
	
	if (varAccessErrorP != '')
	{
		Error_Details(Error_Details_Trans(message[5527],varAccessErrorP.substr(1,varAccessErrorP.length)));
	}
	else if (varAccessError != '')
	{
		Error_Details(Error_Details_Trans(message[5528],varAccessError.substr(1,varAccessError.length)));
	}
	if (varQtyError != '')
	{
		Error_Details(Error_Details_Trans(message[5529],varQtyError.substr(1,varQtyError.length)));
	}
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	fnStartProgress('Y');
	document.frmPartNumMap.submit();

	
}

function fndeSelect(obj)
{
	objSelAll = eval("document.frmPartNumMap.Chk_SelectAll");
	objSelAll.checked = false;
}

function fnSelectAll(obj)
{
	var len = document.frmPartNumMap.hCount.value;
	if (obj == 'all')
			{
				objSelAll = eval("document.frmPartNumMap.Chk_SelectAll");
				if (objSelAll)
				{	
					objSelAll.checked = true;
				}
				
			}
	for(var n=0;n<len;n++)
	{
			objId = eval("document.frmPartNumMap.Chk_part"+n); // The checkboxes would be like rad0, rad1 and so on
			if (obj == 'all')
			{
				objId.checked = true;
				
			}
			else if(objId)
			{			
				if(obj.checked)
				{
					objId.checked = true;
				}else
				{
					objId.checked = false;
				}
			}
	 }		
}

function fnLoad()
{
	var varToCheck = '<%=strType%>';
	if (varToCheck == 'subasm')
	{
		document.frmPartNumMap.rdtype[1].checked = true;
		fnSetType('subasm');
	}
	else
	{
		document.frmPartNumMap.rdtype[0].checked = true;
		fnSetType('pnum');
	}
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnLoad();">
<FORM name="frmPartNumMap" method="POST" action="<%=strServletPath%>/GmPartNumMapServlet">
<input type="hidden" name="hAction" value="">
<INPUT type="hidden" name="inputString" value="">
<INPUT type="hidden" name="hOpt" value="">
<INPUT type="hidden" name="hType" value="">
<INPUT type="hidden" name="hPartType" value="<%=strType%>">
<INPUT type="hidden" name="hCount" value="<%=intDataSize%>" >
<INPUT type="hidden" name="hInputString" value="">
<INPUT type="hidden" name="hInputPnum" value="<%=strParentPNum%>">
<INPUT type="hidden" name="hSubAsmPart" value="<%=strSubParts%>">
	<table border="0" width="800" cellspacing="0" cellpadding="0">
		<tr><td colspan="3" class="Line" height="1"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtPartNumMap:message key="LBL_PART_NUMBER_SUBCOMPONENT_MAPPING"/></td>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
		</tr>
		<tr>
			<td width="798" height="80" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td width="230" class="RightTableCaption" align="right" HEIGHT="30"><fmtPartNumMap:message key="LBL_PROJECT_NAME"/>&nbsp;:</td>
						<!-- Custom tag lib code modified for JBOSS migration changes -->
						   <!-- Added style="vertical-align: initial;" changes in PC-3662-Edge Browser fixes for Quality Module -->
						<td style="vertical-align: initial;">&nbsp;<gmjsp:dropdown controlName="Cbo_Proj"  seletedValue="<%=strProjId%>" onChange="javascript:fnCallProject();"
											value="<%=alProjList%>" codeId = "ID"  codeName = "IDNAME" defaultValue= "[Choose One]"/></td>
						<td rowspan="2"><input type="radio"  onClick="fnSetType(this.value);" name="rdtype" value="pnum" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" ><fmtPartNumMap:message key="LBL_PART_NUMBER"/>
							<br><input type="radio" onClick="fnSetType(this.value);" name="rdtype" value="subasm" onFocus="changeBgColor(this,'#AACCE8');"  onBlur="changeBgColor(this,'#ffffff');" ><fmtPartNumMap:message key="LBL_SUB_COMPONENT_ID"/>&nbsp;&nbsp;&nbsp;
							<fmtPartNumMap:message key="BTN_LOAD" var="varLoad"/>
							<gmjsp:button gmClass="button" name="Btn_Load"  accesskey="P" buttonType="Load" buttonTag="True" onClick="fnLoadPart();" value="&nbsp;${varLoad}  &nbsp;"/>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="30"><fmtPartNumMap:message key="LBL_PART_NUMBER"/>&nbsp;:</td>
						<td>
<%
					if (!strhAction.equals(""))
					{
%>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					&nbsp;<gmjsp:dropdown controlName="Cbo_Part"  seletedValue="<%=strParentPNum%>" onChange="fnGo();"
											value="<%=alPartList%>" codeId = "ID"  codeName = "ID" defaultValue= "[Choose One]"/>
<%
			  		}
%>
						<input type="text" value="<%=strParentPNum%>" class="InputArea" name="Txt_PartNum" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
						<fmtPartNumMap:message key="BTN_PART_LOOKUP" var="varPartLookup"/>
						&nbsp;&nbsp;<gmjsp:button gmClass="button" name="Btn_PartLookUp"  accesskey="P" tabindex="-1" buttonType="Load" buttonTag="True" onClick="fnOpenPart();" value="${varPartLookup}" />
						</td>
					</tr>
				
<%
					if (strhAction.equals("LoadPartDetails"))
					{
%>

					<tr>
						<td class="RightTableCaption" align="right" height="30"><%=strHeaderPartLabel%>&nbsp;:</td>
						<td colspan="2">&nbsp;<%=GmCommonClass.getStringWithTM(strPartNumDesc)%></td>
					</tr>
					<tr>
						<td class="RightTableCaption" height="30" align="right"><%=strComponentLabel%>&nbsp;:</td>
						<td colspan="2">&nbsp;<input type="text" value="<%=strSubAsmPNum%>" <%=strDisabled%>  size="20" class="InputArea" name="Txt_SubAsmPart" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
							<fmtPartNumMap:message key="BTN_ADD" var="varAdd"/>
							&nbsp;&nbsp;<gmjsp:button gmClass="button" name="Btn_Add" buttonType="Save" buttonTag="True" accesskey="A" onClick="fnAddPart();" value="${varAdd}" />&nbsp;
						</td>
					</tr>
<%					if (!strMessage.equals("")){ %>
					<tr>
						<td colspan="3" class="RightTextRed" align="center">&nbsp;<%=strMessage%></td>
					<tr>
<% } %>					
						<td colspan="3">
							<TABLE cellpadding="1" cellspacing="0" border="0" width="100%">
								<tr><td colspan="5" class="Line" height="1"></td></tr>								
								<TR class="ShadeRightTableCaption">
									<TD class="RightText"><input type=checkbox name=Chk_SelectAll <%=strDisabled%> onClick=fnSelectAll(this);> &nbsp;<fmtPartNumMap:message key="LBL_PART_NUM"/></TD>
									<TD class="RightText"><fmtPartNumMap:message key="LBL_DESCRPTION"/></TD>
									<TD class="RightText"><fmtPartNumMap:message key="LBL_QTY"/></TD>
									<TD class="RightText" ><fmtPartNumMap:message key="LBL_TYPE"/></TD>
									<TD class="RightText" width="100"><fmtPartNumMap:message key="LBL_REV_LEVEL"/></TD>
								</TR>
								<tr><td height="1" class="Line" colspan="5"></td></tr>
									
<% for (int intCount = 0; intCount < intDataSize ; intCount++ ) {
	hmTemp = new HashMap();
	hmTemp = (HashMap)alDataList.get(intCount);
	strPartMapId = GmCommonClass.parseNull((String)hmTemp.get("PMAPID")); 
	strPartLabel = GmCommonClass.parseNull((String)hmTemp.get("PNUM")); 
	strPartDesc = GmCommonClass.parseNull((String)hmTemp.get("PDESC")); 
	strPartQty = GmCommonClass.parseNull((String)hmTemp.get("QTY")); 
	strPartType = GmCommonClass.parseNull((String)hmTemp.get("PTYPE")); 
	strRevLevel = GmCommonClass.parseNull((String)hmTemp.get("REVLVL")); 
	strChecked = "checked";
            String strPatternVar = "\\b".concat(strPartLabel).concat("\\b");
            Pattern p = Pattern.compile(strPatternVar);
            Matcher m = p.matcher(strSubParts);
            if (m.find())
            {
            	strChecked = ""; // uncheck the checkbox
            }
            log.debug(" Subpart list " + strSubParts + " Patern var " + strPatternVar + " strChecked " + strChecked);
	if (strPartType.equals("0") && strDeptId.equals("P")){
		strPartType = "30031";
	}
	strCboCtrlName = "Cbo_PartType"+intCount;
 %>								
								<TR>
								
									<td width="100" valign="top"><input type="checkbox" <%=strDisabled%> size="10" name="Chk_part<%=intCount%>" value="<%=strPartMapId%>" <%=strChecked%> onClick="fndeSelect();">
									&nbsp;&nbsp;<%=strPartLabel%>
									</td>
									<td><%=strPartDesc%></td>
									<td  valign="top"><input type="text" size="6" class="InputArea" <%=strDisabled%> name="Txt_Qty<%=intCount%>" value="<%=strPartQty%>" id="<%=strPartLabel%>" ></td>
									<TD valign="top" >
									<!-- Custom tag lib code modified for JBOSS migration changes -->
									<gmjsp:dropdown controlName="<%=strCboCtrlName%>"  seletedValue="<%=strPartType%>" disabled="<%=strDisabled%>"
											value="<%=alPartType%>" codeId = "CODEID"  codeName = "CODENM" />
									</TD>
									<td align="center" valign="top"> <%=strRevLevel %></td>
								</TR>
								<tr><td height="1" colspan="5" bgcolor="#eeeeee"></td></tr>
<% } %>								
							</table>
						</td>
				   </tr>
				   <%
							String strDisableBtnVal = strDisabled;
						if(strDisableBtnVal.equals("disabled"))
						{
							strDisableBtnVal = "true";
						}
					%>
				   <tr>
						<td  align="center" height="35" colspan="3">
							<fmtPartNumMap:message key="BTN_SUBMIT" var="varSubmit"/>
							<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" buttonType="Save" disabled="<%=strDisableBtnVal%>" onClick="fnSubmit();" tabindex="16" />&nbsp;
						</td>
					</tr>
<%
					}
%>
				</table>
			</td>
		</tr>
		<tr>
			<td height="1" bgcolor="#666666" colspan="3"></td>
		</tr>
    </table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
