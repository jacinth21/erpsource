 <%
/**********************************************************************************
 * File		 		: GmPartSalesReportYTD.jsp
 * Desc		 		: This screen is used to display Part By Project
 * Version	 		: 1.0
 * author			: RichardK
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
 
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<!-- prodmgmnt\GmPartSalesReportYTD.jsp -->
<%@ taglib prefix="fmtPartSalesReportYTD" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartSalesReportYTD:setLocale value="<%=strLocale%>"/>
<fmtPartSalesReportYTD:setBundle basename="properties.labels.prodmgmnt.GmPartSalesReportYTD"/>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%
try {

	GmServlet gm = new GmServlet();
	ArrayList alProjList = new ArrayList();
	HashMap hmReturn = new HashMap();
	
	String strSelected = "";
	String strCodeID = "";
	String strProjNm = "";
	String strProjId = "";
	
	
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	
	gm.checkSession(response,session);

	
	
	String strShowArrow = GmCommonClass.parseZero((String)request.getAttribute("Chk_ShowArrowFl"));
	String strShowPercent = GmCommonClass.parseZero((String)request.getAttribute("Chk_ShowPercentFl"));
	String strHideAmount = GmCommonClass.parseZero((String)request.getAttribute("Chk_HideAmountFl"));
	String strHeader = (String)request.getAttribute("strHeader");
	String strProj = (String)request.getAttribute("Cbo_Proj");
	String strType = (String)request.getAttribute("Cbo_Type");
	
	
	String strhAction = (String)request.getAttribute("hAction");
	
	
		
	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}
	
	// To get the Project Combo Box value information 
	HashMap hmComonValue = (HashMap)request.getAttribute("hmComonValue");
	if (hmComonValue != null)
	{
		alProjList = (ArrayList)hmComonValue.get("PROJECTS");
	}
	
	strhAction = (strhAction == null)?"Load":strhAction;

	strProj = (strProj == null)?"0":strProj; // If no value selected will set the combo box value to [Choose One]
	strType = (strType == null)?"0":strType; // Setting combo box value
	
	// To Generate the Crosstab report
	gmCrossTab.setHeader("Purchase By Part By Month");
	
	// To display percent changed information
	if (strShowPercent.equals("1"))
	{
		gmCrossTab.setReportType(gmCrossTab.CROSSTAB_SALE_PER);	
	}	
	if (strShowArrow.equals("1") )
	{
		gmCrossTab.setShowArrow(true);; // To Display % arrow information
	}

	if (strHideAmount.equals("1"))
	{
		gmCrossTab.setHideAmount(true);	
	}	
	gmCrossTab.setValueType(Integer.parseInt(strType));

	
	
	// Setting Display Parameter
	gmCrossTab.setAmountWidth(80);
	gmCrossTab.setNameWidth(250);
	gmCrossTab.setRowHeight(22);
	hmReturn = (HashMap)request.getAttribute("hmReturn");

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: YTD #</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>
function fnGo()
{
	document.frmOrder.submit();
}
function fnSubmit()
{
	document.frmOrder.submit();
}
function fnCallDetail(val)
{
	document.frmOrder.DistributorID.value = val;
	document.frmOrder.hAction.value = "LoadPart";
	document.frmOrder.submit();
}

function fnLoad()
{
	document.frmOrder.Cbo_FromMonth.value = '<%=request.getAttribute("Cbo_FromMonth")%>';
	document.frmOrder.Cbo_ToMonth.value = '<%=request.getAttribute("Cbo_ToMonth")%>';
	document.frmOrder.Cbo_FromYear.value = '<%=request.getAttribute("Cbo_FromYear")%>';
	document.frmOrder.Cbo_ToYear.value = '<%=request.getAttribute("Cbo_ToYear")%>';
	document.frmOrder.Cbo_Proj.value = '<%=strProj%>';
	document.frmOrder.Cbo_Type.value = '<%=strType%>';
	
	document.frmOrder.Chk_ShowPercentFl.checked = <%=(strShowPercent == "0")?"false":"true"%>;
	document.frmOrder.Chk_ShowArrowFl.checked = <%=(strShowArrow == "0")?"false":"true"%>;
	document.frmOrder.Chk_HideAmountFl.checked = <%=(strHideAmount == "0")?"false":"true"%>;

}


</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad()">
<FORM name="frmOrder" method="post" action = "<%=strServletPath%>/GmPartSalesYTDReportServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="DistributorID" value="<%=request.getAttribute("strDistributorID")%>">
<TABLE cellSpacing=0 cellPadding=0  border=0 width="1000">
<TBODY>
	<tr>
		<td rowspan="10" class=Line noWrap width=1></td>
		<td class=Line noWrap height=1></td>
		<td rowspan="10" class=Line noWrap width=1></td>
	</tr>
	<tr class=Line><td height=25 class=RightDashBoardHeader><%=strHeader%> </td></tr>
	<tr class=Line><td noWrap height=1></td></tr>
	<!-- Addition Filter Information -->
	<tr>
		<td><jsp:include page="/common/GmCrossTabFilterInclude.jsp" />  </td>
	</tr>
	<tr class=borderDark>
		<td noWrap height=1>  </td>
	</tr>
	<!-- Screen Level Filter	-->
	<tr>
	<td class="RightText" colspan="2" HEIGHT="25"><fmtPartSalesReportYTD:message key="LBL_PROJECT_NAME"/>
		&nbsp;&nbsp;&nbsp;&nbsp;<select name="Cbo_Proj" id="Region" class="RightText" tabindex="9"><option value="0" ><fmtPartSalesReportYTD:message key="LBL_CHOOSE_ONE"/>
<%
		HashMap hcboVal = null;
			  		
		int intSize = alProjList.size();
		hcboVal = new HashMap();
		for (int i=0;i<intSize;i++)
		{
			hcboVal = (HashMap)alProjList.get(i);
			strCodeID = (String)hcboVal.get("ID");
			strProjNm = GmCommonClass.getStringWithTM((String)hcboVal.get("NAME"));
%>
			<option <%=strSelected%> value="<%=strCodeID%>"><%=strProjNm%></option>
<%
		}
%>
		</select>
		&nbsp;<fmtPartSalesReportYTD:message key="LBL_TYPE"/> 
		&nbsp;&nbsp;<select name="Cbo_Type" id="Type" class="RightText" tabindex="10">
			 <fmtPartSalesReportYTD:message key="LBL_UNIT"/><option value="0" ></option>
			 <fmtPartSalesReportYTD:message key="LBL_DOLLAR"/><option value="1" ></option>
		 </select>				
	</td>
	</tr>	
	<tr class=borderDark>
		<td noWrap height=1>  </td>
	</tr>
	<tr>
		<td noWrap height=1>  </td>
	</tr>	
	<tr>
		<td align=center> <%=gmCrossTab.PrintCrossTabReport(hmReturn) %></td>
	</tr>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
