<!-- prodmgmnt\GmSetMasterSetupContainer.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="fmtSetMasterSetupContainer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtSetMasterSetupContainer:setLocale value="<%=strLocale%>"/>
<fmtSetMasterSetupContainer:setBundle basename="properties.labels.prodmgmnt.GmSetMasterSetupContainer"/>
<% 
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
	String strWikiTitle = GmCommonClass.getWikiTitle("QUALITY_MAINTENANCE");
	String strSessOpt = (String)session.getAttribute("strSessOpt");
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction");
	String strDeptId = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
	String strForward = GmCommonClass.parseNull((String) request.getAttribute("hForward"));
	String strOpt = GmCommonClass.parseNull((String) request.getAttribute("hOpt"));
	String strJSONString = "{\"STROPT\":\""+strOpt+"\"}";
	String strSetId = "";
	String strSetNm = "";
	System.out.println("stropt:"+strOpt);
	String strTDHeight = "1070px";
	if(strOpt.equals("SET") || strOpt.equals("SETMAP")){
	  strTDHeight = "1325px";
	}
	String strTDWidth = "1100x";
	HashMap hmLoop = new HashMap();
	HashMap hmSetMasterDtls = new HashMap();
	hmSetMasterDtls = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("SETMASTERDETAILS"));
			
	if(strSetId.equals("")){
		strSetId = GmCommonClass.parseNull((String) request.getAttribute("SETID"));
	}
	if (hmSetMasterDtls != null){
		strSetNm = GmCommonClass.parseNull((String)hmSetMasterDtls.get("SETNM"));
		if(strSetNm.contains("\"")){
		  strSetNm = strSetNm.replaceAll("\"","&quot;");
		}
	}
	
%>
<HTML>
<HEAD>
<jsp:useBean id="ResourceBundleBean" type="com.globus.common.beans.GmResourceBundleBean" class="com.globus.common.beans.GmResourceBundleBean" scope="request"> </jsp:useBean>
<%=ResourceBundleBean.getProperty("Title")%>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxlayout.css">
<link rel="STYLESHEET" type="text/css"href="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.css">
<style type="text/css">
.dhx_tabcontent_zone{
  	min-width:1000px;
  	min-height:800px;
    /* overflow:hidden; */
}  
</style>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlxcommon.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmSetMasterSetup.js"></script>
<script language="javascript" type="text/javascript" src="<%=strExtWebPath%>/tiny_mce/tiny_mce.js"></script>

<script language="javascript" type="text/javascript">

var number = "<%=ResourceBundleBean.getProperty("Number")%>";
var desc = "<%=ResourceBundleBean.getProperty("Desc")%>";
var trSize = '<%=strTDHeight%>';
var setId = '<%=strSetId%>';
var hAction = '<%=strhAction%>';

function fnAjaxTabLoad(){
	myTab = new dhtmlXTabBar("my_tabbar");
	myTab.setSkin("winscarf");
	myTab.setImagePath("<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/imgs/");
	myTab.addTab("details",message_prodmgmnt[466]);
	/* Language and Uploads tab should load only if set id is there */
	if(setId != 0 && setId != ''){
		myTab.addTab("SetMap", message_prodmgmnt[479]);//PMT_37772_Set release to Region in Set Setup screen
		myTab.addTab("language", message_prodmgmnt[467]);
		myTab.addTab("uploads", message_prodmgmnt[468]);	
	}
	myTab.attachEvent("onSelect", function(id,last_id){		
		global_param = "&companyInfo="+encodeURIComponent(JSON.stringify(top.gCompanyInfo));
	    if(id=='details'){
	    	document.getElementById("my_tabbar").style.height = trSize;	
	        href= "/GmSetMasterServlet?hForward=DIVCONTAINER&Cbo_SetId="+setId+"&hAction="+hAction+"&hTabName=details"+global_param;
	        // myTab.cells(id).progressOff();
	    }else if(id=='SetMap' && setId != 0 && setId != ''){  //PMT_37772_Set release to Region in Set Setup screen
	    	document.getElementById("my_tabbar").style.height = "700px";
	    	href="/gmSystemPublish.do?method=loadSystemReleaseDtls&setGroupType=1601&setId="+setId+"&frompage=setsetup";
		}
	    else if(id=='language' && setId != 0 && setId != ''){
	    	document.getElementById("my_tabbar").style.height = "700px";
	    	href="/gmLanguageDetail.do?strOpt=report&typeID=103091&refID="+setId+"&hTabName=language"+global_param;
		}else if(id=='uploads' && setId != 0 && setId != ''){
			document.getElementById("my_tabbar").style.height = "700px";
			<%-- href="/gmUploadDetail.do?refID=<%=strSetId%>&typeID=103091&strOpt=report" rel="#iframe"><fmtSetMasterSetupContainer:message key="LBL_UPLOADS"/> --%>
			href="/gmUploadDetail.do?refID="+setId+"&typeID=103091&strOpt=report&hTabName=uploads"+global_param;
	    }
		
			    
	   myTab.cells(id).attachURL(href); //Loading the contents in tab
	   return true;
	   });
}

// Make the Details tab active on page load
function fnOnloadpage(){
	setId = document.getElementById("Cbo_SetId").value;
	document.getElementById("Txt_SetId").focus();
	myTab.setTabInActive(); 
	myTab.setTabActive("details"); //To set details tab active when loaing the page
}
	
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnAjaxTabLoad();fnOnloadpage();">
<input type="hidden" name="hOpt" id="hOpt" value="<%=strOpt%>">
<input type="hidden" name="hForward">
<input type="hidden" name="hcurrentTab" value="SETBOMSETUP">
<%=ResourceBundleBean.getProperty("Type")%>
<FORM name="frmVendor" method="POST" >
	<table class="DtTable800" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="3">&nbsp;<%=ResourceBundleBean.getProperty("Heading")%></td>
			<fmtSetMasterSetupContainer:message key="IMG_ALT_HELP" var="varHelp"/>
			<td class="RightDashBoardHeader"><img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
		</tr>					
		<tr>
			<td class="RightTableCaption" width="18%" height="40" align="right"><font color="red">*</font><%=ResourceBundleBean.getProperty("Namelist")%></td>
			<td align ="left" width ="13%"><input type="text" size="15" value="<%=strSetId%>" name="Txt_SetId" id="Txt_SetId" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" onChange = "javascrip:fnValidateSetBOMName(this.form);"></td>
			<td colspan="3" width ="75%"  height="30" align="left">
			<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
				<jsp:param name="CONTROL_NAME" value="Cbo_SetId" />
				<jsp:param name="METHOD_LOAD" value="loadSetNameList&searchType=PrefixSearch" />
				<jsp:param name="WIDTH" value="510" />
				<jsp:param name="CSS_CLASS" value="search" />
				<jsp:param name="CONTROL_NM_VALUE" value="<%=strSetNm %>" /> 
				<jsp:param name="CONTROL_ID_VALUE" value="<%=strSetId %>" />
				<jsp:param name="SHOW_DATA" value="5" />
				<jsp:param name="PARAMETER" value="<%= strJSONString%>" />
				<jsp:param name="AUTO_RELOAD" value="fnSetFetch(this.form)" />					
			</jsp:include>
			<%-- <gmjsp:dropdown controlName="Cbo_SetId"  seletedValue="<%=strSetId%>" 
					value="<%=alSetList%>" codeId = "ID"  codeName = "IDNAME" defaultValue= "[Choose One]" width="450" onChange="javascript:fnSetFetch(this.form);" />	 --%>			
			</td>
		</tr>	
	
		<tr>
			<td colspan="4" id="my_tabbar" style=" height:'<%=strTDHeight%>'; width:'<%=strTDWidth%>';" >
				
			</td>
		</tr>
		
	</table>
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>