<%@page import="com.globus.common.beans.GmResourceBundleBean"%>

 <%
/**********************************************************************************
 * File		 		: GmProjectSetup.jsp
 * Desc		 		: This screen is used for the Part Number frmPartSetuptenance
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%> 
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.net.URLEncoder" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ taglib prefix="fmtPartNumberSetupContainer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmPartNumberSetupContainer.jsp -->
<fmtPartNumberSetupContainer:setLocale value="<%=strLocale%>"/>
<fmtPartNumberSetupContainer:setBundle basename="properties.labels.prodmgmnt.GmPartNumberSetup"/>
<%
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("PART_NUMBER_SETUP"));
	String strhAction = "";
	String strQltyDisabled = "disabled";
	strhAction = (String)request.getAttribute("hAction")==null?"Save":(String)request.getAttribute("hAction");
	String strTest = GmCommonClass.parseNull((String)request.getAttribute("hQltyDisabled"));
	strQltyDisabled = (String)request.getAttribute("hQltyDisabled")== null?"disabled":(String)request.getAttribute("hQltyDisabled");
	
	HashMap hmReturn = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturn"));
	HashMap hmLoadDetails = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("LOADDETAILS"));
	
	ArrayList alPathway = new ArrayList();
	alPathway= GmCommonClass.parseNullArrayList((ArrayList) hmLoadDetails.get("PATHWAY"));
	
	ArrayList alRegulatory = new ArrayList();
	alRegulatory= GmCommonClass.parseNullArrayList((ArrayList) hmLoadDetails.get("REGULATORY"));
	 
	
	int intSize = 0;

	String strCodeID = "";
	HashMap hcboVal = null;
	String strPartNum = "";
	String strPartStatus = "20365"; 
	String strDCODisable = "";
	String strhPartNum = "";
	boolean bolFl = false;
	
	HashMap hmVal = null;
	HashMap hmTemp = null;
	String strCodeId ="";
	String strCodeGrp ="";
	String strCodeNm ="";
	String strPathwayId ="";
	String strPathwayGrp ="";
	String strPathwayNm ="";
	String strDINumberFl = "";
	String strStructType = "";	
	String strPresMethod = "";
	String strCompanyId = GmCommonClass.parseNull((String) gmDataStoreVO.getCmpid());
	String strCreatedCmpy = "";
	String strEditDisable = "";
	int psize = 0;
	
	ArrayList alTemp = null;
	
	 psize = alPathway.size();
     
     for (int i=0; i< psize; i++)
		  {
			   hmVal = (HashMap)alPathway.get(i);
			   strCodeGrp = (String)hmVal.get("CODENMALT"); 
		       strCodeId = (String)hmVal.get("CODEID");
		       strCodeNm = (String)hmVal.get("CODENM");

		       strPathwayId = strPathwayId + strCodeId + ",";            //80070,80071  (US, EU)
		       strPathwayGrp = strPathwayGrp + strCodeGrp + ",";    // USPAT, EUPAT    
		       strPathwayNm = strPathwayNm + strCodeNm + ",";  
		  }
		  
	if (strhAction.equals("Lookup"))
	{
		HashMap hmPartNumDetails = (HashMap)hmReturn.get("PARTNUMDETAILS");
		if (!hmPartNumDetails.isEmpty())
		{
			strPartNum = GmCommonClass.parseNull((String)hmPartNumDetails.get("PARTNUM"));
			strPartStatus = GmCommonClass.parseNull((String)hmPartNumDetails.get("PARTSTATUS"));
			strhPartNum = strPartNum;
			strDINumberFl = GmCommonClass.parseNull((String)hmPartNumDetails.get("DI_NUMBERFL"));
			strStructType = GmCommonClass.parseNull((String)hmPartNumDetails.get("STRCTYPE"));
			strPresMethod = GmCommonClass.parseNull((String)hmPartNumDetails.get("PRESERVMETHOD"));
			strCreatedCmpy = GmCommonClass.parseNull((String)hmPartNumDetails.get("CREATEDCMPNY"));
		}
		 
		log.debug("strPartStatus>>>"+strPartStatus+" strQltyDisabled>>>"+strQltyDisabled);
		if (!(strPartStatus.equals("20365") || strPartStatus.equals("20366") || strPartStatus.equals("")) && !strQltyDisabled.equals("Enable"))
		{
			strDCODisable = "true";
		} 
		   
	}
	String strContainerHgt;// To set the container height to make the alignment correct
	if(!strPartNum.equals("")){
		strContainerHgt = "955px";
	}else{
		strContainerHgt = "100%";
	}
	if(!strCreatedCmpy.equals("") && !strCompanyId.equals(strCreatedCmpy)){
	  strEditDisable = "true";
	}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Number Setup </TITLE>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />

<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxCombo.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmPartNumberSetup.js"></script>
<script language="javascript" type="text/javascript" src="<%=strExtWebPath%>/tiny_mce/tiny_mce.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" type="text/javascript">
	function fnHtmlEditor(){
	tinyMCE.init({
		mode : "specific_textareas",
	    editor_selector : "HtmlTextArea",
		theme : "advanced",
		theme_advanced_path : false,
		height : "300",
		width: "500" 
	});
}
//pathway
var pathwayId = '<%=strPathwayId %>';
var pathwayGrp = '<%=strPathwayGrp%>' ;
var pathwayNm = '<%=strPathwayNm%>' ;
var pathwaySize = '<%=psize %>';
var partAvailable = false;
var structType = '<%=strStructType%>';
var prevMethod = '<%=strPresMethod%>';
</script>
<script>
var lblPartDescription = '<fmtPartNumberSetupContainer:message key="LBL_PART_DESC"/>';
var lblsalesGroup='<fmtPartNumberSetupContainer:message key="LBL_SALES_GROUP"/>';
var lblMaterialSpec = '<fmtPartNumberSetupContainer:message key="LBL_MATER_SPEC"/>';
var lblMeasuringDevice='<fmtPartNumberSetupContainer:message key="LBL_MEASU_DEVICE"/>';
var lblUom='<fmtPartNumberSetupContainer:message key="LBL_UOM"/>';
var lblDrawingNum = '<fmtPartNumberSetupContainer:message key="LBL_DRAW_NUM"/>';
var lblInsertId = '<fmtPartNumberSetupContainer:message key="LBL_INSERT_ID"/>';
var lblStructuralType = '<fmtPartNumberSetupContainer:message key="LBL_STRU_TYPE"/>';
var lblPreseveMethos = '<fmtPartNumberSetupContainer:message key="LBL_PRESER_METHOD"/>';
var lblStorageTemp = '<fmtPartNumberSetupContainer:message key="LBL_STR_TEMP"/>';

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnAjaxTabLoad();" onkeydown="fnCallGo();">
<FORM name="frmPartSetup" method="POST" action="<%=strServletPath%>/GmPartNumServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hPartNum" value="<%=strhPartNum%>">
<input type="hidden" name="hcurrentTab">
<input type="hidden" name="strOpt">
	<table border="0" class="DtTable950" cellspacing="0" cellpadding="0" id="tableHeader">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr> <td width="100%" height="25" valign="top">
			<table border="0" width="100%" cellspacing="0" cellpadding="0"><tr>
				<td bgcolor="#666666" width="1" rowspan="3"></td>
				<td class="RightDashBoardHeader" height="25">&nbsp;<fmtPartNumberSetupContainer:message key="LBL_HEADER"/></td>
				<td  height="25" class="RightDashBoardHeader" align="right">
				<fmtPartNumberSetupContainer:message key="LBL_HELP" var="varHelp"/>
				<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
			       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			    </td> 
				<td bgcolor="#666666" width="1" rowspan="3"></td>
			</tr></table></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td valign="top">
				<table border="0" cellspacing="0" cellpadding="0" style="width:100%;">
				
					<tr>
					<td width="100%" valign="top" colspan="2"> 
					<table border="0" width="100%" cellspacing="0" cellpadding="0"><tr height="30">
						<td class="RightTableCaption" width="23%" align="right" HEIGHT="24"> <font color="red">*</font><fmtPartNumberSetupContainer:message key="LBL_NEW_PART"/>:</td>
						<td width="18%">&nbsp;<input type="text" size="20" value="<%=strPartNum%>" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8'); " onBlur="changeBgColor(this,'#ffffff');fnChkPartType(this.form, this.value);" >
						</td><td width="3%">
						<span id="DivShowPartAvail" style="vertical-align: middle;display: none;"> <img height="24" width="25" title="Part# available" src="<%=strImagePath%>/success.gif"></img></span>
						<span id="DivShowPartExists" style="vertical-align: middle;display: none;"> <img title="Part# already exists" src="<%=strImagePath%>/delete.gif"></img> </span>
						</td>
						<fmtPartNumberSetupContainer:message key="BTN_LOAD" var="varLoad"/>
						<td width="66%">&nbsp;&nbsp;<gmjsp:button value="${varLoad}" name="Btn_Load" style="vertical-align: middle;" gmClass="button" buttonType="Load"  onClick="fnCallLookup(this.form);" />
						</td> 
						</tr></table></td>
					</tr>
					
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
</FORM>		
					<tr>
						<td colspan="3">
							<ul id="maintab" class="shadetabs">
								<li onclick = "fnUpdateTabIndex(this);"><a href="/GmPartNumServlet?hOpt=DIVCONTAINER&hAction=Lookup&Txt_PartNum=<%=URLEncoder.encode(strPartNum)%>&editDisable=<%=strEditDisable%>" rel="ajaxdivcontentarea" id="partdetails" title="PartDetails" ><fmtPartNumberSetupContainer:message key="LBL_PART_DETAILS"/> </a></li>
									<%
										if(!strPartNum.equals("")) {
									%>
									<li onclick = "fnUpdateTabIndex(this);"><a
									 	href="/gmPartParameterDtls.do?method=dhrRequirementDtls&partNumber=<%=URLEncoder.encode(strPartNum)%>&editDisable=<%=strEditDisable%>"
									 	rel="#iframe" id="dhrReq"><fmtPartNumberSetupContainer:message key="LBL_LI_1"/></a></li> 
									<li onclick="fnUpdateTabIndex(this);"><a
										href="/gmLanguageDetail.do?strOpt=report&typeID=103090&refID=<%=URLEncoder.encode(strPartNum)%>"
										id="language" rel="#iframe"><fmtPartNumberSetupContainer:message key="LBL_LI_2"/></a></li>
									<li onclick="fnUpdateTabIndex(this);"><a
										href="/gmUploadDetail.do?refID=<%=URLEncoder.encode(strPartNum)%>&hParentForm=PartSetup&strOpt=report&typeID=103090"
										rel="#iframe" id="uploads"><fmtPartNumberSetupContainer:message key="LBL_LI_3"/></a></li>
									<li onclick="fnUpdateTabIndex(this);"><a
										href="/gmPartParameterDtls.do?method=udiParameterDtls&partNumber=<%=URLEncoder.encode(strPartNum)%>&editDisable=<%=strEditDisable%>"
										rel="#iframe" id="udiParam"><fmtPartNumberSetupContainer:message key="LBL_LI_4"/></a></li> 
									<% if(strCompanyId.equals("1001")){%>
									<li onclick="fnUpdateTabIndex(this);"><a
										href="/gmPartAttribute.do?haction=PartLabelParameter&partnumber=<%=URLEncoder.encode(strPartNum)%>&editDisable=<%=strEditDisable%>"
										rel="labelparameter" id="labelParam"><fmtPartNumberSetupContainer:message key="LBL_LI_5"/></a></li>
									<%} %>
									 <li onclick="fnUpdateTabIndex(this);"><a
										href="/gmUDIProductDevelopment.do?method=pdPartUDISetup&partNumber=<%=URLEncoder.encode(strPartNum)%>&editDisable=<%=strEditDisable%>"
										rel="#iframe" id="pd"><fmtPartNumberSetupContainer:message key="LBL_LI_6"/></a></li>
									<li onclick="fnUpdateTabIndex(this);"><a
										href="/gmPartParameterDtls.do?method=udiRegulatorySetup&partNumber=<%=URLEncoder.encode(strPartNum)%>&editDisable=<%=strEditDisable%>"
										rel="#iframe" id="regParam"><fmtPartNumberSetupContainer:message key="LBL_LI_7"/></a></li>	 	 
									<li onclick="fnUpdateTabIndex(this);"><a
										href="/gmPartProjectMapDtls.do?method=fetchProject&partnumber=<%=URLEncoder.encode(strPartNum)%>&editDisable=<%=strEditDisable%>"
										rel="#iframe" id="partProjectMap"><fmtPartNumberSetupContainer:message key="LBL_LI_8"/></a></li>	
									<%} %>
								</ul>
						</td>
					</tr>
		
					<tr>
			        	<td> 
			            	<div id="ajaxdivcontentarea" class="contentstyle" style="display:visible;overflow:auto;height:<%=strContainerHgt%>" ></div> 
			            </td>
			        </tr> 
    			</table>
   			 </td>
    	</tr>
    </table>
</FORM>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
