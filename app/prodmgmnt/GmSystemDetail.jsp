<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ taglib prefix="fmtSystemDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\GmSystemDetail.jsp -->
<fmtSystemDetails:setLocale value="<%=strLocale%>"/>
<fmtSystemDetails:setBundle basename="properties.labels.prodmgmnt.GmSystemDetail"/>
<bean:define id="alSystemGrpList" name="frmSystemDetails" property="alSystemGrpList" type="java.util.ArrayList"></bean:define>
<bean:define id="alSystemSgmtList" name="frmSystemDetails" property="alSystemSgmtList" type="java.util.ArrayList"></bean:define>
<bean:define id="alSystemTechList" name="frmSystemDetails" property="alSystemTechList" type="java.util.ArrayList"></bean:define>
<bean:define id="alSystemPubToList" name="frmSystemDetails" property="alSystemPubToList" type="java.util.ArrayList"></bean:define>
<bean:define id="hGroupIds" name="frmSystemDetails" property="hGroupIds" type="java.lang.String"></bean:define>
<bean:define id="hSegmentIds" name="frmSystemDetails" property="hSegmentIds" type="java.lang.String"></bean:define>
<bean:define id="hTechniqueIds" name="frmSystemDetails" property="hTechniqueIds" type="java.lang.String"></bean:define>
<bean:define id="hPublishInIds" name="frmSystemDetails" property="hPublishInIds" type="java.lang.String"></bean:define>
<bean:define id="setSysPublishAcc" name="frmSystemDetails" property="setSysPublishAcc" type="java.lang.String"></bean:define>

<bean:define id="message" name="frmSystemDetails" property="message" type="java.lang.String"></bean:define>
<bean:define id="mapcount" name="frmSystemDetails" property="mapcount" type="java.lang.String"></bean:define>
<bean:define id="publishtype" name="frmSystemDetails" property="publishtype" type="java.lang.String"></bean:define>
<%

String strCollapseImage = strImagePath+"/minus.gif";
HashMap hmSystemMap = new HashMap();
hmSystemMap.put("ID","CODEID");
hmSystemMap.put("PID","CODEID");
hmSystemMap.put("NM","CODENM");

String publishBtn="display:inline-block";
String unPublish="display:none";

if(publishtype.equals("108660")){
	unPublish ="display:inline-block";
	publishBtn="display:none";
}

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>System Setup</title>
</head>
<body leftmargin="20" topmargin="10">
<html:form action="/gmSystemSetup.do">
<html:hidden styleId="strOpt" property="strOpt"/>
<html:hidden property="setId" styleId="setId"/>	
<html:hidden property="groupAttrId"/>
<html:hidden property="segmentAttrId"/>
<html:hidden property="techniqueAttrId"/>
<html:hidden property="publishInAttrId"/>
<html:hidden property="hGroupIds" styleId="hGroupIds"/>
<html:hidden property="hSegmentIds" styleId="hSegmentIds"/>
<html:hidden property="hTechniqueIds" styleId="hTechniqueIds"/>
<html:hidden property="hPublishInIds" styleId="hPublishInIds"/>
<html:hidden property="setAttrInputStr"/>
<html:hidden property="attrTypeInputStr"/>

<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="2" class="LLine" height="1"></td>
	</tr>
	 <tr>
		 <td colspan="2" width="100%" height="100" valign="top">
			<table border="0" width="100%" cellspacing="0" cellpadding="0"> 
				<tr class="ShadeRightTableCaption">
					<td height="25" colspan="2" ><a href="javascript:fnShowFilters('tabSystem');" tabindex="-1" style="text-decoration:none; color:black" >
						<IMG id="tabSystemimg" border=0 src="<%=strCollapseImage%>">&nbsp;<fmtSystemDetails:message key="LBL_SYSTEM_DETAILS"/></a>
					</td>
				</tr>
				<tr>
				<td colspan="2">
				    <!-- Added style="display:table" changes in PC-3662-Edge Browser fixes for Quality Module -->
					<table border="0" bordercolor="lightblue" width="100%" cellspacing="0" cellpadding="0" id="tabSystem" style="display:table">
						<tr><td colspan="2" class="LLine" height="1"></td></tr>
						<tr>
							<td class="RightTableCaption" align="right" height="24"><font color="red">*</font>&nbsp;<fmtSystemDetails:message key="LBL_SYSTEM_NAME"/>&nbsp;:&nbsp;</td>
							<td width="80%" height="24" valign="top">
								<table>
									<tr>
										<td><html:text property="setNm" maxlength="200" size="50" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
											onblur="changeBgColor(this,'#ffffff');javascript:fnvalidateSystemName(this.value);" /></td>
										<td><IMG align="left" id="validateName" border=0	height="20" width="20" src="" style="display: none" /></td>
										<td><div id="validateText" class="RegularText"></div> </td>										
									</tr>
							     </table>
							</td>
						</tr>
						<tr><td colspan="2" class="LLine" height="1"></td></tr>
						<tr class="Shade">
		                	<td class="RightTableCaption" align="right" height="24"><fmtSystemDetails:message key="LBL_SYSTEM_ID"/>&nbsp;:&nbsp;</td> 
		                	<td><bean:write name="frmSystemDetails" property="setId"/></td>
		                </tr>						
		                <tr><td colspan="2" class="LLine" height="1"></td></tr>
						<tr>
							<td class="RightTableCaption" align="right" height="320"><fmtSystemDetails:message key="LBL_SYSTEM_DESCRIPTION"/>&nbsp;:&nbsp;</td>
							<td><textarea name="setDesc" class="HtmlTextArea"  onfocus="changeBgColor(this,'#AACCE8');" 
									onblur="changeBgColor(this,'#ffffff');"  rows="3" cols="50"><bean:write name="frmSystemDetails" property="setDesc"/></textarea> 
							</td>
						</tr>
						<tr><td colspan="2" class="LLine" height="1"></td></tr>
						<tr class="Shade">
		                	<td class="RightTableCaption" align="right" height="320"><fmtSystemDetails:message key="LBL_DETAILED_DESCRIPTION"/>&nbsp;:&nbsp;</td> 
		                	<td><textarea name="setDetailDesc" class="HtmlTextArea" onfocus="changeBgColor(this,'#AACCE8');" 
									onblur="changeBgColor(this,'#ffffff');"  rows="3" cols="50"><bean:write name="frmSystemDetails" property="setDetailDesc"/></textarea> 
							</td>
		                </tr>
		                <tr>
							<td class="RightTableCaption" align="right" height="24"><font color="red">*</font>&nbsp;<fmtSystemDetails:message key="LBL_DIVISION"/>:&nbsp;</td>
							<td><gmjsp:dropdown controlName="divisionId" SFFormName="frmSystemDetails" SFSeletedValue="divisionId"
							SFValue="alDivisionList" codeId="DIVISION_ID"  codeName="DIVISION_NAME"  defaultValue="[Choose One]"/>
							</td>
						</tr>
					</table>
				</td>
				</tr>
				<tr class="ShadeRightTableCaption">
					<td height="25" colspan="2" ><a href="javascript:fnShowFilters('tabSystemAttribute');" tabindex="-1" style="text-decoration:none; color:black" >
						<IMG id="tabSystemAttributeimg" border=0 src="<%=strCollapseImage%>">&nbsp;<fmtSystemDetails:message key="LBL_ATTRIBUTES"/></a>
					</td>
				</tr>
				<tr>
				<td colspan="2">
				    <!-- Added style="display:table" changes in PC-3662-Edge Browser fixes for Quality Module -->
					<table border="0" bordercolor="lightblue" width="100%" cellspacing="0" cellpadding="0" id="tabSystemAttribute" style="display:table">
						<tr><td colspan="4" class="LLine" height="1"></td></tr>
						<tr>
	                		<td class="RightTableCaption" align="right" width="15%" height="24"><fmtSystemDetails:message key="LBL_MARKET_GROUP"/>:&nbsp;</td> 
	                		<td width="30%" align="left" valign="middle">&nbsp;<%=GmCommonControls.getChkBoxGroup("",alSystemGrpList,"Group",hmSystemMap,"CODENM")%>&nbsp;</td>
	               			<td class="RightTableCaption" align="right" height="110" width="20%">&nbsp;<fmtSystemDetails:message key="LBL_SEGMENT_CATEGORY"/>:&nbsp;</td>
	                		<td align="left" width="35%">&nbsp;<%=GmCommonControls.getChkBoxGroup("",alSystemSgmtList,"Segment",hmSystemMap,"CODENM")%>&nbsp;</td>
	                	</tr>
						<tr><td colspan="4" class="LLine" height="1"></td></tr>
						<tr class="Shade">
							<td class="RightTableCaption" align="right" width="15%">&nbsp;<fmtSystemDetails:message key="LBL_TECHNIQUE"/>:&nbsp;</td> 
	                		<td width="30%" align="left" valign="middle">&nbsp;<%=GmCommonControls.getChkBoxGroup("",alSystemTechList,"Technique",hmSystemMap,"CODENM")%>&nbsp;</td>
	                		<td class="RightTableCaption" align="right" width="20%" height="110">&nbsp;&nbsp;</td> 
	                		<td width="35%" align="left" valign="middle" id="publishMsg"><%=message %> </td>
	                	</tr>
                	</table>
				</td>
				</tr>			
				<tr id="logsection">
					<td colspan="2"> 
						<jsp:include page="/common/GmIncludeLog.jsp" >
						<jsp:param name="FORMNAME" value="frmSystemDetails" />
						<jsp:param name="ALNAME" value="alLogReasons" />
						<jsp:param name="LogMode" value="Edit" />
						<jsp:param name="Mandatory" value="yes" />
						</jsp:include>
					</td>
				</tr>
				<tr><td colspan="2" height="15"></td></tr>
				<tr>
					<td colspan="2" align="center" height="24">					
		 				<logic:equal name="frmSystemDetails" property="setSetupSubmitAcc" value="Y"> 
			 				<fmtSystemDetails:message key="BTN_SUBMIT" var="varSubmit"/>
			 				<fmtSystemDetails:message key="BTN_RESET" var="varReset"/>
					    	<gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnSystemSubmit(this.form);"/>&nbsp;&nbsp;
                    		<gmjsp:button value="${varReset}" gmClass="button" buttonType="Save" onClick="fnSystemReset(this.form);"/>&nbsp;&nbsp;
                    	 </logic:equal>		 				
                    	<logic:notEqual name="frmSystemDetails" property="setId" value="">
		 				<logic:notEqual name="frmSystemDetails" property="setId" value="0"> 
                    		<logic:equal name="frmSystemDetails" property="setSetupPreviewAcc" value="Y">
                    			<fmtSystemDetails:message key="BTN_PREVIEW" var="varPreview"/>
	                    		<gmjsp:button value="${varPreview}" gmClass="button" buttonType="Load" onClick="fnPreviewSystem(this.form);"/>
	                    	</logic:equal>
	                    	<logic:equal name="frmSystemDetails" property="setSetupVoidAcc" value="Y"> 
	                    		<fmtSystemDetails:message key="BTN_VOID" var="varVoid"/>
		 						<gmjsp:button value="${varVoid}" gmClass="button" buttonType="Save" onClick="fnVoidSystem(this.form);" style="display: none"/>&nbsp;&nbsp; 
                    		</logic:equal>  
	                 	    
	                   		<fmtSystemDetails:message key="BTN_PUBLISH" var="varPublish"/>
	                   		<gmjsp:button controlId="publisBtn" value="${varPublish}" gmClass="button" buttonType="Save" onClick="fnSystemPublish(this.form,'108660');" disabled="<%=setSysPublishAcc%>" style="<%=publishBtn %>" />
					           
	                   		<fmtSystemDetails:message key="BTN_UNPUBLISH" var="varUnPublish"/>
	                   		<gmjsp:button controlId="unPublisBtn" value="${varUnPublish}" gmClass="button" buttonType="Save" onClick="fnSystemPublish(this.form,'108661');" disabled="<%=setSysPublishAcc%>" style="<%=unPublish %>"  />
	                    </logic:notEqual>
	                    </logic:notEqual>
	                    
                    </td>
				</tr>
				<tr><td colspan="2" height="15"></td></tr>
			</table>
		</td>
	</tr>			
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>