<%
/**********************************************************************************
 * File		 		: GmMultiProjectPartMap.jsp
 * Desc		 		: Enables the part to be available for multiple projects. 
 * Version	 		: 1.0
 * author			: Rajeshwaran Varatharajan
************************************************************************************/
%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtMultiProjectPartMap" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- prodmgmnt\GmMultiProjectPartMap.jsp -->
<fmtMultiProjectPartMap:setLocale value="<%=strLocale%>"/>
<fmtMultiProjectPartMap:setBundle basename="properties.labels.prodmgmnt.GmMultiProjectPartMap"/>


<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page errorPage="/common/GmError.jsp" %>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>


<%@ page import="org.apache.commons.beanutils.RowSetDynaClass, org.apache.commons.beanutils.BasicDynaBean"%>

 <bean:define id="ldtPartNumberDetails" name="frmMultiProjectPart" property="ldtPartNumberDetails" type="java.util.List"></bean:define>

<%

ArrayList alSubList = new ArrayList();

alSubList = GmCommonClass.parseNullArrayList((ArrayList) ldtPartNumberDetails);

int rowsize=0;
if(alSubList!=null)
	rowsize=alSubList.size();

//String strWikiPath = GmCommonClass.getString("GWIKI");
String strWikiTitle = GmCommonClass.getWikiTitle("PM_MULTIPRJ_PART_MAP");

%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Multiple Project Part Mapping </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
function fnSelectAll(){
  TotalRows = <%=rowsize%>;
 for (var i=0;i<TotalRows ;i++ )
	{	

		if(document.frmMultiProjectPart.Chk_selectAll.checked){
			document.frmMultiProjectPart.checkMultiProject[i].checked=true;
		}
		else{
			document.frmMultiProjectPart.checkMultiProject[i].checked=false;
			}	 
	}
}

function fnMasterCheckBox() {
	document.frmMultiProjectPart.Chk_selectAll.checked = true;
	var TotalRows =<%=rowsize%>;
	for (var i = 0; i < TotalRows; i++) {
		if (document.frmMultiProjectPart.checkMultiProject[i].checked == false) {
			document.frmMultiProjectPart.Chk_selectAll.checked = false;
		}
	}
}

function fnLoad()
{
	if(TRIM(document.frmMultiProjectPart.partNumbers.value) == ""){
		Error_Details(message_prodmgmnt[318]);
		}	
		if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmMultiProjectPart.haction.value = 'loadParts';
	fnStartProgress('Y');
	document.frmMultiProjectPart.submit();
}

function fnSubmit()
{
	var actionval = document.frmMultiProjectPart.haction.value;
	
	document.frmMultiProjectPart.haction.value =  'save';
	
	var hiddenLength = document.frmMultiProjectPart.checkMultiProject.length;

	var inputString =''; 
	
	if(hiddenLength==undefined)
	{
		var hpnum = eval("document.frmMultiProjectPart.hpartnum0").value;
		if(document.frmMultiProjectPart.checkMultiProject.checked){
				inputString+=hpnum+'#'+'Y';	
			}else{			
				inputString+=hpnum+'#'+'N';
			}
	}else{
		for(var i=0;i<hiddenLength;i++){
			var hpnum = eval("document.frmMultiProjectPart.hpartnum"+i).value;
			if(document.frmMultiProjectPart.checkMultiProject[i].checked){
				inputString+=hpnum+'#'+'Y';	
			}else{			
				inputString+=hpnum+'#'+'N';
			}
			if(i!=hiddenLength-1){
				inputString+=',';
			}
		}
	}
	document.frmMultiProjectPart.hinputString.value=inputString;
	fnStartProgress('Y');
	document.frmMultiProjectPart.submit();
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10"">
<html:form action="/gmOPMultiProjPartMap.do">

<html:hidden property="strOpt" />
<html:hidden property="haction" value="loadParts" />
<html:hidden property="hinputString" value=""/>

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtMultiProjectPartMap:message key="LBL_MULTIPROJECT"/></td>
			
			<td align="right" class=RightDashBoardHeader > 	
				<fmtMultiProjectPartMap:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> <!-- Question -->
				</td>
			
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="848" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24"><fmtMultiProjectPartMap:message key="LBL_ENABLE_SUB"/>:</td> 
                        <td>&nbsp;<html:checkbox property="enablesubcomponent" />
                         </td>
                    </tr>
                    <tr><td colspan="2" class="ELine"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtMultiProjectPartMap:message key="LBL_PART_NUMBERS"/>:</td> 
                        <td>&nbsp;
	                         <html:text property="partNumbers"  size="50" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/> &nbsp;
						     <gmjsp:dropdown controlName="search" SFFormName="frmMultiProjectPart" SFSeletedValue="search" SFValue="partNumberSearch" codeId="CODENMALT" codeName="CODENM" defaultValue="[Choose One]" />		
	                        &nbsp;&nbsp;&nbsp;<fmtMultiProjectPartMap:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;&nbsp;" gmClass="button" buttonType="Load" onClick="fnLoad();" />
                        </td>
                    </tr>       
                    <tr><td colspan="2" class="ELine"></td></tr>
                    
		<%
			if (rowsize > 1) {
		%>
		 <tr>
  			<td colspan="5" align="left"  ><input type="checkbox" name="Chk_selectAll" onClick="fnSelectAll();"><fmtMultiProjectPartMap:message key="LBL_SELECT_ALL"/></td>
 		</tr> 
 		<%
			}
		%>
           <tr> 
            <td colspan="2">
            
            <display:table name="requestScope.frmMultiProjectPart.ldtPartNumberDetails" requestURI="/gmOPMultiProjPartMap.do" 
			class="its" id="currentRowObject" 
			decorator="com.globus.prodmgmnt.displaytag.DTMultiProjectPartWrapper"> 
			<fmtMultiProjectPartMap:message key="LBL_MULTI_PROJECT" var="varMultiProject"/><display:column property="MPRJFLG" title="${varMultiProject}"  class="aligncenter" style="width:100px"/>
			<fmtMultiProjectPartMap:message key="LBL_PART_NUMBER" var="varPartNumber"/><display:column property="PNUM" title="${varPartNumber}" sortable="true" class="aligncenter" style="width:100px" /> 
			<fmtMultiProjectPartMap:message key="LBL_PART_DESCRIPTION" var="varPartDescription"/><display:column property="PDESC" title="${varPartDescription}" sortable="true" class="alignleft"/> 
			
			</display:table> 
			
		</td>
    </tr> 
    <tr><td colspan="2" class="ELine"></td></tr> 
                    <tr>
                    	<td colspan="2" align="center">		                    
		                    <fmtMultiProjectPartMap:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnSubmit();" /> 
		                </td>
                    </tr>
   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
<script>
	if (document.frmMultiProjectPart.Chk_selectAll != undefined) {
		fnMasterCheckBox();
	}
</script>
</BODY>

</HTML>

