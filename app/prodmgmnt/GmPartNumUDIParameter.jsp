<%@page import="com.globus.common.beans.GmResourceBundleBean"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtPartNumberUDIParam" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmPartNumUDIParameter.jsp -->
<fmtPartNumberUDIParam:setLocale value="<%=strLocale%>"/>
<fmtPartNumberUDIParam:setBundle basename="properties.labels.prodmgmnt.GmPartNumUDIParameter"/>
<bean:define id="hmAttrVal" name="frmPartParameterDtls" property="hmAttrVal" type="java.util.HashMap" />
<bean:define id="buttonAccess" name="frmPartParameterDtls" property="buttonAccess" type="java.lang.String"> </bean:define>
<bean:define id="editDisable" name="frmPartParameterDtls" property="editDisable" type="java.lang.String"> </bean:define>
<%
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
	String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("PART_UDI_PARAMETER"));
	HashMap hmUDIParam = new HashMap();
	String strButtonDisable = buttonAccess.equals("Y")?"":"true";
	if(editDisable.equalsIgnoreCase("true")){
	  strButtonDisable = "true";
	}
	hmUDIParam = GmCommonClass.parseNullHashMap((HashMap) hmAttrVal);

	String strPartNum = "";
	String strDINumber = "";
	String strSubjectDMExempt = "";
	String strDmDiPrimaryDI = "";
	String strDmDiNumber = "";
	String strHctp = "";
	String strForSingleUse = "";
	String strDevPackSterile = "";
	String strMriSafetyInfo = "";
	String strExpirationDate = "";
	String strRuleLotBatchVal = "";
	String strRuleManufactVal = "";
	String strRuleSerialNumVal = "";
	String strRuleDonationIdenNumVal = "";

	String strMriHistoryFl = "";
	String strSubjectDMHistoryFl = "";
	String strDmDiPrimaryHistoryFl = "";
	String strDmDiNumberHistoryFl = "";
	String strHctpHistoryFl = "";
	String strForSingleHistoryFL = "";
	String strExpirationHistoryFl = "";
	String strDevPackHistoryFl = "";
	
	String strDeviceCountHistoryFl = "";
	String strUnitUseDiNumHistoryFl = "";
	String strPackDiNumberHistoryFl = "";
	String strQuantityPackHistoryFl = "";
	String strContainDiPackHistoryFl = "";
	String strPackDiscontDtHistoryFl = "";
	String strOverrideCompHistoryFl = "";
	String strDunsNumHistoryFl = "";
	String strCustomPhoneHistoryFl = "";
	String strCustomEmailHistoryFl = "";
	String strLotBatchHistoryFl = "";
	String strManufactDtHistoryFl = "";
	String strSerialNumHistoryFl = "";
	String strDonationNumHistoryFl = "";
	String strSamplePattern = "";
	String strTodaysDate = GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate"));
	// to get the attribute values
	strPartNum = GmCommonClass.parseNull((String) hmUDIParam.get("PARTNUM"));
	// to get the history information
	strMriHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("MRI_HISTORY_FL"));
	strSubjectDMHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("SUB_HISTORY_FL"));
	strDmDiPrimaryHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("DMDI_PRIMARY__HISTORY_FL"));
	strDmDiNumberHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("DMDI_HISTORY_FL"));
	strHctpHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("HCTP_HISTORY_FL"));
	strForSingleHistoryFL = GmCommonClass.parseNull((String) hmUDIParam.get("FOR_SINGLE_HISTORY_FL"));
	strExpirationHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("EXPRIRATION_HISTORY_FL"));
	strDevPackHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("DEVICE_PACK_HISTORY_FL"));
	strRuleLotBatchVal = GmCommonClass.parseNull((String) hmUDIParam.get("RULELOTBATCHVAL"));
	strRuleSerialNumVal = GmCommonClass.parseNull((String) hmUDIParam.get("RULESERIALNUMVAL"));
	strRuleManufactVal = GmCommonClass.parseNull((String) hmUDIParam.get("RULEMANUFATURINGDTVAL"));
	strRuleDonationIdenNumVal = GmCommonClass.parseNull((String) hmUDIParam.get("RULEDONATIONIDENNUMVAL"));
	// default parameters
	strDeviceCountHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("DEVICE_CNT_HISTORY_FL"));
	strUnitUseDiNumHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("UNIT_USE_DI_HISTORY_FL"));
	strPackDiNumberHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("PACK_DI_NUM_HISTORY_FL"));
	strQuantityPackHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("QUANTITY_PACK_HISTORY_FL"));
	strContainDiPackHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("CONTAIN_DI_PACK_HISTORY_FL"));
	strPackDiscontDtHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("PACK_DISCON_HISTORY_FL"));
	strOverrideCompHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("OVERRIDE_COMP_HISTORY_FL"));
	strDunsNumHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("DUNS_NUM_HISTORY_FL"));
	strCustomPhoneHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("CONTACT_PHONE_HISTORY_FL"));
	strCustomEmailHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("CONTACT_EMAIL_HISTORY_FL"));
	strLotBatchHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("LOT_BATCH_HISTORY_FL"));
	strManufactDtHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("MANUF_DT_HISTORY_FL"));
	strSerialNumHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("SERIAL_NUM_HISTORY_FL"));
	strDonationNumHistoryFl = GmCommonClass.parseNull((String) hmUDIParam.get("DONATION_IDEN_HISTORY_FL"));
	strSamplePattern = GmCommonClass.parseNull((String) hmUDIParam.get("SAMPLEPATTERN"));
	
	strMriHistoryFl = strMriHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View MRI Safety Information' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1056'); border='0'>": "";
	strSubjectDMHistoryFl = strSubjectDMHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View Subject to DM but Exempt' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1057'); border='0'>": "";
	strDmDiPrimaryHistoryFl = strDmDiPrimaryHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View DM DI is not Primary DI' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1058'); border='0'>": "";
	strDmDiNumberHistoryFl = strDmDiNumberHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View DM DI Number' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1059'); border='0'>": "";
	strHctpHistoryFl = strHctpHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View HCTP' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1060'); border='0'>": "";
	strForSingleHistoryFL = strForSingleHistoryFL.equals("Y") ? "<img style='cursor:hand' alt='View For Single Use' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1061'); border='0'>": "";
	strExpirationHistoryFl = strExpirationHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View Expiration Date' src='"+strImagePath+"'/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1062'); border='0'>": "";
	strDevPackHistoryFl = strDevPackHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View Device Packaged as Sterile' src='"+strImagePath+"'/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1063'); border='0'>": "";
	// default parameters
	strDeviceCountHistoryFl = strDeviceCountHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View Device Count' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1064'); border='0'>": "";
	strUnitUseDiNumHistoryFl = strUnitUseDiNumHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View Unit of use DI Number' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1066'); border='0'>": "";
	strPackDiNumberHistoryFl = strPackDiNumberHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View Package DI Number' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1067'); border='0'>": "";
	strQuantityPackHistoryFl = strQuantityPackHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View Quantity per Package' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1068'); border='0'>": "";
	strContainDiPackHistoryFl = strContainDiPackHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View Contain DI Package' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1069'); border='0'>": "";
	strPackDiscontDtHistoryFl = strPackDiscontDtHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View Package Discontinue Date' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1070'); border='0'>": "";
	strOverrideCompHistoryFl = strOverrideCompHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View Override Company Name' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1071'); border='0'>": "";
	strDunsNumHistoryFl = strDunsNumHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View DUNS Number' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1072'); border='0'>": "";
	strCustomPhoneHistoryFl = strCustomPhoneHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View Customer Phone' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1073'); border='0'>": "";
	strCustomEmailHistoryFl = strCustomEmailHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View Customer Email' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1074'); border='0'>": "";
	strLotBatchHistoryFl = strLotBatchHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View Lot or Batch Number' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1075'); border='0'>": "";
	strManufactDtHistoryFl = strManufactDtHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View Manufacturing Date' src='"+strImagePath+"'/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1076'); border='0'>": "";
	strSerialNumHistoryFl = strSerialNumHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View Serial Number' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1077'); border='0'>": "";
	strDonationNumHistoryFl = strDonationNumHistoryFl.equals("Y") ? "<img style='cursor:hand' alt='View Donation Identification Number' src='"+strImagePath+"/icon_History.gif' onClick=fnShowHistory('"+ strPartNum + "','1065'); border='0'>": "";
%>
<html>
<head>
<title>Globus Medical: UDI Parameter</title>
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmPartNumberSetup.js"></script>
<script>
var lblmriSafetyInf = '<fmtPartNumberUDIParam:message key="LBL_UDI_MRI_SAFTEY"/>';
</script>
</head>
<body onload="fnLoadUdiParam();">
	<html:form action="/gmPartParameterDtls.do?method=udiParameterDtls">
		<html:hidden property="strOpt" name="frmPartParameterDtls" value="" />
		<html:hidden property="partNumber" name="frmPartParameterDtls" />
		<html:hidden property="haction" name="frmPartParameterDtls" />
		<html:hidden property="inputStr" name="frmPartParameterDtls" />
		<html:hidden property="applnDateFmt" name="frmPartParameterDtls" />
		<html:hidden property="diNumber" name="frmPartParameterDtls" />
		<html:hidden property="sterilePart" name="frmPartParameterDtls" />
		<input type="hidden" name="hRuleLotBatch" value="<%=strRuleLotBatchVal %>" />
		<input type="hidden" name="hRuleManufact" value="<%=strRuleManufactVal %>" />
		<input type="hidden" name="hRuleSerialNum" value="<%=strRuleSerialNumVal %>" />
		<input type="hidden" name="hRuleDonationIden" value="<%=strRuleDonationIdenNumVal %>" />
		<input type="hidden" name="hTodayDate" value="<%=strTodaysDate %>" />
		<input type="hidden" name="hAgencyPattern" value="<%=strSamplePattern %>" />
		<table border="0" width="948px" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="RightDashBoardHeader" colspan="3"><fmtPartNumberUDIParam:message key="LBL_UDI_HEADER"/></td>
				<fmtPartNumberUDIParam:message key="LBL_HELP" var="varHelp"/>
				<td align="right" class=RightDashBoardHeader><img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'
					height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="4" height="1"></td>
			</tr>
			<tr class="oddshade">
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_DI_NUM"/>:</td>
				<td>&nbsp;<bean:write name="frmPartParameterDtls" property="diNumber" /></td>
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_SUB_TO_DM"/>:</td>
				<td>&nbsp;<bean:write name="frmPartParameterDtls" property="subjectDmExempt" />&nbsp;<%=strSubjectDMHistoryFl%></td>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="evenshade">
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_DM_DI_PRI"/>:</td>
				<td>&nbsp;<bean:write name="frmPartParameterDtls" property="dmDiPrimaryDi" />&nbsp;<%=strDmDiPrimaryHistoryFl%></td>
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_DM_DI_NUM"/>:</td>
				<td>&nbsp;<bean:write name="frmPartParameterDtls" property="dmDiNumber" />&nbsp;<%=strDmDiNumberHistoryFl%></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="oddshade">
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_HCTP"/>:</td>
				<td>&nbsp;<bean:write name="frmPartParameterDtls" property="hctp" />&nbsp;<%=strHctpHistoryFl%></td>
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_SINGLE_USE"/>:</td>
				<td>&nbsp;<bean:write name="frmPartParameterDtls" property="singleUse" />&nbsp;<%=strForSingleHistoryFL%></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="evenshade">
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_DEVICE_PCK"/>:</td>
				<td>&nbsp;<bean:write name="frmPartParameterDtls" property="devPkgSterile" />&nbsp;<%=strDevPackHistoryFl%></td>
				<td class="RightTableCaption" height="25" align="right"><font color="red">*</font> <fmtPartNumberUDIParam:message key="LBL_UDI_MRI_SAFTEY"/>:</td>
				<td>&nbsp;<gmjsp:dropdown controlName="mriSafetyInformation"
						SFFormName="frmPartParameterDtls"
						SFSeletedValue="mriSafetyInformation" SFValue="alMriSafetyInfo"
						codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" />&nbsp;<%=strMriHistoryFl%></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr>
				<td height="26" class="ShadeRightTableCaption" colspan="4">&nbsp;<fmtPartNumberUDIParam:message key="LBL_UDI_DEFAULT_PARAM"/></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="evenshade">
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_DEVICE_COUNT"/>:</td>
				<td>&nbsp;<bean:write name="frmPartParameterDtls" property="deviceCount" />&nbsp;<%=strDeviceCountHistoryFl %></td>
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_UNI_DI_NUM"/>:</td>
				<td>&nbsp;<bean:write name="frmPartParameterDtls" property="unitUseDiNumber" />&nbsp;<%=strUnitUseDiNumHistoryFl %></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="oddshade">
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_PCKG_DI_NUM"/>:</td>
				<td>&nbsp;<bean:write name="frmPartParameterDtls" property="packDiNumber" />&nbsp;<%=strPackDiNumberHistoryFl %></td>
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_QNTY_PCKG"/>:</td>
				<td>&nbsp;<bean:write name="frmPartParameterDtls" property="quantityPerPack" />&nbsp;<%=strQuantityPackHistoryFl %></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="evenshade">
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_CON_DI_PCKG"/>:</td>
				<td>&nbsp;<bean:write name="frmPartParameterDtls" property="containsDiPack" />&nbsp;<%=strContainDiPackHistoryFl %></td>
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_PCKG_DIS_DATE"/>:</td>
				<td>&nbsp;<gmjsp:calendar SFFormName="frmPartParameterDtls"
						controlName="packageDisconDt" gmClass="InputArea"
						onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" />&nbsp;<%=strPackDiscontDtHistoryFl %>
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="oddshade">
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_OVERRIDE_CMP"/>:</td>
				<td>&nbsp;<gmjsp:dropdown controlName="overrideCompanyNm"
						SFFormName="frmPartParameterDtls"
						SFSeletedValue="overrideCompanyNm" defaultValue="[Choose One]"
						SFValue="alOverrideCompanyNm" codeId="CODEID" codeName="CODENM" />&nbsp;<%=strOverrideCompHistoryFl %>
				</td>
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_LAB_DUNS"/>:</td>
				<td>&nbsp;<bean:write name="frmPartParameterDtls" property="dunsNumber" />&nbsp;<%=strDunsNumHistoryFl %></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="evenshade">
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_CUST_CONT_PH"/>:</td>
				<td>&nbsp;<bean:write name="frmPartParameterDtls" property="custContactPhone" />&nbsp;<%=strCustomPhoneHistoryFl %></td>
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_CUST_CONT_EM"/>:</td>
				<td>&nbsp;<bean:write name="frmPartParameterDtls" property="custContactMail" />&nbsp;<%=strCustomEmailHistoryFl %></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="oddshade">
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_BATCH_NUM"/>:</td>
				<td>&nbsp;<gmjsp:dropdown controlName="lotOrBatchNum"
						SFFormName="frmPartParameterDtls" SFSeletedValue="lotOrBatchNum"
						defaultValue="[Choose One]" SFValue="alLotOrBatchNum"
						codeId="CODEID" codeName="CODENM" disabled="disabled" />&nbsp;<%=strLotBatchHistoryFl %>
				</td>
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_MANF_DATE"/>:</td>
				<td>&nbsp;<gmjsp:dropdown controlName="manufaturingDt"
						SFFormName="frmPartParameterDtls" SFSeletedValue="manufaturingDt"
						defaultValue="[Choose One]" SFValue="alManufaturingDt"
						codeId="CODEID" codeName="CODENM" disabled="disabled" />&nbsp;<%=strManufactDtHistoryFl %>
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="evenshade">
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_SERIAL_NUM"/>:</td>
				<td>&nbsp;<gmjsp:dropdown controlName="serialNum"
						SFFormName="frmPartParameterDtls" SFSeletedValue="serialNum"
						defaultValue="[Choose One]" SFValue="alSerialNum"
						codeId="CODEID" codeName="CODENM" disabled="disabled" />&nbsp;<%=strSerialNumHistoryFl %>
				</td>
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_UDI_EXPIRATION_DATE"/>:</td>
				<td>&nbsp;<gmjsp:dropdown controlName="expirationDt"
						SFFormName="frmPartParameterDtls" SFSeletedValue="expirationDt"
						defaultValue="[Choose One]" SFValue="alExpirationDt"
						codeId="CODEID" codeName="CODENM" disabled="disabled" />&nbsp;<%=strExpirationHistoryFl %>
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="oddshade">
				<td class="RightTableCaption" height="25" align="right"><fmtPartNumberUDIParam:message key="LBL_DONATION_IDENTIFICATION_NUM"/>:</td>
				<td>&nbsp;<gmjsp:dropdown controlName="donationIdenNum"
						SFFormName="frmPartParameterDtls" SFSeletedValue="donationIdenNum"
						defaultValue="[Choose One]" SFValue="alDonationIdenNum"
						codeId="CODEID" codeName="CODENM" disabled="disabled" />&nbsp;<%=strDonationNumHistoryFl %>
				</td>
				<td class="RightTableCaption" height="25" align="right" colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr>
				<td colspan="4"><jsp:include page="/common/GmIncludeLog.jsp">
						<jsp:param name="FORMNAME" value="frmPartParameterDtls" />
						<jsp:param name="ALNAME" value="alLogReasons" />
						<jsp:param name="Mandatory" value="yes" />
						<jsp:param name="LogMode" value="Edit" />
					</jsp:include></td>
			</tr>
			<tr height="30">
				<fmtPartNumberUDIParam:message key="BTN_SUBMIT" var="varSubmit"/>
				<td colspan="4" align="center"><gmjsp:button value="${varSubmit}"
						name="btn_Submit" controlId="btn_Submit" gmClass="button"
						onClick="fnUDISubmit(this.form);" buttonType="Save" disabled="<%=strButtonDisable %>" /></td>
			</tr>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</body>
</html>