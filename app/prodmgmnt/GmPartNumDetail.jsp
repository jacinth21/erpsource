<%
/**********************************************************************************
 * File		 		: GmPartNumDetail.jsp
 * Desc		 		: This screen is used to display Part Detail
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap, java.util.Calendar" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
 
<%@ page import ="com.globus.common.beans.GmCommonControls"%>

<%@ page import ="com.globus.common.beans.GmCalenderOperations"%>
<!-- prodmgmnt\GmPartNumDetail.jsp -->
<%@ taglib prefix="fmtPartNumDtls" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartNumDtls:setLocale value="<%=strLocale%>"/>
<fmtPartNumDtls:setBundle basename="properties.labels.prodmgmnt.GmPartNumDetail"/>  

<%
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8"); 	 
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strPart ="";
	String strPDesc ="";
	String strProjId ="";
	String strProjNM ="";
	String strProductF ="";
	String strRev ="";
	String strAfl ="";
	String strTieri ="";
	String strTierii ="";
	String strTieriii ="";
	String strTieriv ="";
	String strOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
	String strStatus = GmCommonClass.parseNull((String)request.getAttribute("status"));
		 
	if(hmReturn !=null)
	 {
	    strPart = GmCommonClass.parseNull((String)hmReturn.get("PNUM"));
		strPDesc = GmCommonClass.parseNull((String)hmReturn.get("PDESC"));
		strProjId = GmCommonClass.parseNull((String)hmReturn.get("PROID"));
		strProjNM = GmCommonClass.parseNull((String)hmReturn.get("PROJNM"));
		strProductF = GmCommonClass.parseNull((String)hmReturn.get("PRODF"));
		strRev = GmCommonClass.parseNull((String)hmReturn.get("REV"));
		strAfl = GmCommonClass.parseNull((String)hmReturn.get("AFL"));
		String strCurrSymb = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
		strTieri = strCurrSymb + GmCommonClass.getStringWithCommas(GmCommonClass.parseNull((String)hmReturn.get("TIERI"))) ;
		strTierii = strCurrSymb + GmCommonClass.getStringWithCommas(GmCommonClass.parseNull((String)hmReturn.get("TIERII")));
		strTieriii = strCurrSymb + GmCommonClass.getStringWithCommas(GmCommonClass.parseNull((String)hmReturn.get("TIERIII")));
		strTieriv = strCurrSymb + GmCommonClass.getStringWithCommas(GmCommonClass.parseNull((String)hmReturn.get("TIERIV")));
	 }
	
	String strApplDateFmt=(String)gmDataStoreVO.getCmpdfmt();
	String strFromdaysDate = GmCalenderOperations.getCalculatedDateFromToDate(6, Calendar.MONTH,strApplDateFmt);
	String strTodaysDate = (String)session.getAttribute("strSessTodaysDate");
 	 //strTodaysDate = GmCommonClass.getStringFromDate(new java.util.Date(strTodaysDate), strApplDateFmt);
 	strTodaysDate = GmCalenderOperations.getCurrentDate( strApplDateFmt);
	String strLimitedAccess= GmCommonClass.parseNull((String)request.getAttribute("DMNDSHT_LIMITED_ACC"));
	boolean bolLimitedAccess = false;
	if(strLimitedAccess!=null && strLimitedAccess.equalsIgnoreCase("true")){
		bolLimitedAccess = true;
	}
	
	%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Detail</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script>
//declaring company info
var jsdatefmt=null;
var compid=null;
var applDateFmt="<%=strApplDateFmt%>";
var plantid=null;
var partyid=null;
var cmplangid=null;
try{
	jsdatefmt=self.opener.top.gCompJSDateFmt;
	compid=self.opener.top.gCompanyInfo.cmpid;
	plantid=self.opener.top.gCompanyInfo.plantid;
	partyid=self.opener.top.gCompanyInfo.partyid;
	cmplangid=self.opener.top.gCompanyInfo.cmplangid;
}catch(err){
	jsdatefmt=null;
	compid=null;
	plantid=null;
	partyid=null;
	cmplangid=null;
}
		function fnViewDetails(setId, setNm)
		{
			 var temp = new Array();
		    temp = setNm.split('-');
		    setNm ="";
		    for(i=0; i<temp.length; i++)
		    {
		    	setNm = setNm + temp[i] + " ";
		    	}
		//	setNm = setNm.replaceAll("-", " ");
		 	 
			windowOpener("/GmSetReportServlet?hOpt=SETRPT&hAction=Drilldown&hSetId="+setId+"&hSetNm="+setNm,"PrntInv1","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
		}
		
		
		function fnViewC(setId)
		{
			 			 
			windowOpener("/GmSalesVirtualActualServlet?hOpt=BYCSET&hAction=Reload&Cbo_Id="+setId,"PrntInv1","resizable=yes,scrollbars=yes,top=250,left=300,width=965,height=600");
		}
		
		function fnSalebyS()
		{
			 			 
			windowOpener("/GmSalesYTDReportServlet?hAction=LoadRepA&Cbo_Type=0&hPartNumber="+encodeURIComponent('<%=strPart%>'),"PrntInv1","resizable=yes,scrollbars=yes,top=250,left=300,width=1050,height=600");
			
		}
		
		function fnSalebyA()
		{
			 			 
			windowOpener("/GmSalesYTDReportServlet?hAction=LoadAccountA&Cbo_Type=0&hPartNumber="+encodeURIComponent('<%=strPart%>'),"PrntInv1","resizable=yes,scrollbars=yes,top=250,left=300,width=1050,height=600");
			
		}
		function fnSalebyMonth()
		{
			//alert("strFromdaysDate is"+'<%=strFromdaysDate%>');		 
			windowOpener("/GmConsignSearchServlet?hOpt=4110&Cbo_Category=20210&Cbo_Type=20200&Cbo_Status=20230&hAction=Reload&Txt_ToDt="+'<%=strTodaysDate%>'+"&Txt_FrmDt="+'<%=strFromdaysDate%>'+"&Txt_PartNum="+'<%=strPart%>', "PrntInv1","resizable=yes,scrollbars=yes,top=250,left=300,width=950,height=600");
			
		}
		
		function fnMonthPart()
		{
			 			 
			windowOpener("/GmPartNumSearchServlet?hAction=Go&Txt_FromDate="+'<%=strFromdaysDate%>'+"&Txt_ToDate="+'<%=strTodaysDate%>'+"&Txt_PartNum="+ '<%=strPart%>', "PrntInv1","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=600");
			
		}

		function fnViewDetails()
		{
			windowOpener("/GmPartInvReportServlet?hAction=DrillDown&hOpt=OPEREQ&hPartNum="+encodeURIComponent('<%=strPart%>'),"INVPOP","resizable=yes,scrollbars=yes,top=300,left=300,width=710,height=200");
		}
	
		function fnTrend()
		{
			windowOpener("/gmLogisticsReport.do?method=loadTrendReport&strOpt=reload&salesGrpType=50271&trendType=50277&partNum="+encodeURIComponent('<%=strPart%>'),"INVPOP","resizable=yes,scrollbars=yes,top=300,left=300,width=1200,height=300");
		}
		 
	</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" >
 
<TABLE cellSpacing=0 cellPadding=0  border=0 width="500" class="its">
<TBODY>
	<tr>
		<td rowspan="16" class=Line noWrap width=1></td>
		<td class=Line noWrap height=1  ></td>
		<td rowspan="16" class=Line noWrap width=1></td>
	</tr>
	<tr class=Line >
	<td height=25 class=RightDashBoardHeader ><fmtPartNumDtls:message key="LBL_PART_DTLS"/> </td>
	</tr>
	<tr class=Line><td noWrap height=1 ></td></tr>
					<tr >
						<td ><table ><tr >
							<td class="RightTableCaption"  width=150 HEIGHT="24" ><fmtPartNumDtls:message key="LBL_PART_NUM"/>: </td>
							 
							<td>&nbsp; <%=strPart%>
							</td>
						</tr></table></td>
					</tr>
				 <tr><td   class="LLine"></td></tr>		
				 <tr class="even">
						<td ><table><tr>
							<td class="RightTableCaption" width=150 HEIGHT="24" ><fmtPartNumDtls:message key="LBL_PART_DESC"/>: </td>
							 
							<td>&nbsp; <%=strPDesc%>
							</td>
						</tr></table></td>
					</tr>
					
				 <tr><td   class="LLine"></td></tr>		
				 <tr>
						<td ><table><tr>
							<td class="RightTableCaption" width=150 HEIGHT="24"><fmtPartNumDtls:message key="LBL_PRJ_ID"/>: </td>
							 
							<td>&nbsp; <%=strProjId%> &nbsp; <%=strProjNM%>
							</td>
						</tr></table></td>
					</tr>
					
					<tr><td   class="LLine"></td></tr>		
				 <tr class="even">
						<td ><table><tr>
							<td class="RightTableCaption" width=150 HEIGHT="24"><fmtPartNumDtls:message key="LBL_PROD_FAMILY"/>: </td>
							 
							<td>&nbsp; <%=strProductF%>
							</td>
						</tr></table></td>
					</tr>
					<tr><td   class="LLine"></td></tr>	
				 <tr>
						<td ><table><tr>
							<td class="RightTableCaption" width=150 HEIGHT="24"><fmtPartNumDtls:message key="LBL_REV_NUM"/>: </td>
							 
							<td>&nbsp; <%=strRev%>
							</td>
						</tr></table></td>
					</tr>
					<tr><td   class="LLine"></td></tr>	
				<tr class="even">
						<td ><table><tr>
							<td class="RightTableCaption" width=150 HEIGHT="24"><fmtPartNumDtls:message key="LBL_ACTIVE"/>: </td>
							 
							<td>&nbsp; <%=strAfl%>
							</td>
						</tr></table></td>
					</tr>	
				 
			<tr>
			  <td></td>
	</tr>			
	<tr class=Line><td noWrap height=1 colspan=2></td></tr>
</table>	

<p> &nbsp;</p>
<TABLE cellSpacing=0 cellPadding=0  border=0   class="its">
<TBODY>
	 <tr>
		<td rowspan="16" class=Line noWrap width=1></td>
		<td class=Line noWrap height=1  ></td>
		<td rowspan="16" class=Line noWrap width=1></td>
	</tr>
	 
					<tr >
						<td ><table ><tr >
							<td class="RightTableCaption"   align=center   HEIGHT="15" ><fmtPartNumDtls:message key="LBL_TIER_I"/></td>
							<td class="RightTableCaption"   align=center HEIGHT="15" ><fmtPartNumDtls:message key="LBL_TIER_II"/></td>
							<td class="RightTableCaption"  align=center  HEIGHT="15" ><fmtPartNumDtls:message key="LBL_TIER_III"/></td> 
							<td class="RightTableCaption"   align=center HEIGHT="15" ><fmtPartNumDtls:message key="LBL_TIER_IV"/> </td>
						</tr></table></td>
					</tr>
				 <tr><td   class="LLine"></td></tr>		
				 <tr >
						<td ><table ><tr >
							<td align=center  ><%=strTieri%> </td>
							<td  align=center ><%=strTierii%></td>
							<td  align=center ><%=strTieriii%></td> 
							<td  align=center><%=strTieriv%> </td>
						</tr></table></td>
					</tr>
				 
			<tr>
			  <td></td>
	</tr>			
	<tr class=Line><td noWrap height=1 colspan=2></td></tr>
</table>	


<p> &nbsp;</p>
 
 <table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtPartNumDtls:message key="LBL_ORD_PLAN_GRP"/></td>
		</tr>
		
				
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td colspan="2">
			<display:table name="rdPartGroup" requestURI="/GmPartNumServlet"   class="its" id="currentRowObject" >
					<fmtPartNumDtls:message key="DT_PART_NUM" var="varPnum"/> 
 					<display:column property="PNUM" title="${varPnum}"  group="1"  class="alignleft"/>
 					<fmtPartNumDtls:message key="DT_GRP_NM" var="varGrpName"/>
					<display:column property="GROUPNAME" title="${varGrpName}"  group="2"  class="alignleft"/>
					<fmtPartNumDtls:message key="DT_PART_TYPE" var="varPartType"/> 
					<display:column property="PARTTYPE" title="${varPartType}"    class="alignleft"/>
			</display:table>
			</td>
		</tr>
    <table>


<p> &nbsp;</p>
 
 <table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtPartNumDtls:message key="LBL_SEL_DTLS"/></td>
		</tr>
		
				
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td colspan="2">
			<display:table name="rdSetDetail" requestURI="/GmPartNumServlet"   class="its" id="currentRowObject"  decorator="com.globus.displaytag.beans.DTParDetailWrapper"> 
 				<fmtPartNumDtls:message key="DT_SET_ID" var="varSetId"/> 
 				<display:column property="SETID" title="${varSetId}"  class="alignleft" />
 				<fmtPartNumDtls:message key="DT_SET_DESC" var="varSetDesc"/> 
				<display:column property="SETDESC" title="${varSetDesc}"  class="alignleft" />
				<fmtPartNumDtls:message key="DT_SET_QTY" var="varSetQty"/> 
				<display:column property="SETQTY" title="${varSetQty}"  class="aligncenter" />
				<fmtPartNumDtls:message key="DT_IN_SET_FL" var="varInSetFl"/>
				<fmtPartNumDtls:message key="DT_FLAG" var="varFlag"/>  
				<display:column property="INSETFL" title="${varInSetFl}<br> ${varFlag}"  class="aligncenter" />
				<fmtPartNumDtls:message key="DT_CRITICAL" var="varCritical"/> 
				<display:column property="CRITFL" title="${varCritical} <br> ${varFlag}"  class="aligncenter" />
				<fmtPartNumDtls:message key="DT_QTY" var="varQty"/> 
				<display:column property="CRITQTY" title="${varCritical} <br>${varQty}"  class="aligncenter" />
			</display:table>
			</td>
		</tr>
    <table>
 
 <p> &nbsp;</p>
 <%
 	if (!strOpt.equals("Search")
 	&&(!bolLimitedAccess))
 	{
 %>
 <table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtPartNumDtls:message key="LBL_VENDOR_PRICE_DTLS"/> </td>
		</tr>
		
				
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td colspan="2">
			<display:table name="rdVendorDetail" requestURI="/GmPartNumServlet"   class="its" id="currentRowObject"  > 
 				<fmtPartNumDtls:message key="DT_VENDOR_NM" var="varVendNm"/> 
				<display:column property="VNAME" title="${varVendNm}"  class="alignleft" />
				<fmtPartNumDtls:message key="DT_CURRENCY" var="varCurr"/> 
				<display:column property="CURRENCY" title="${varCurr}"  class="aligncenter" />
				<fmtPartNumDtls:message key="DT_COST" var="varCost"/> 
				<display:column property="CPRICE" title="${varCost}"  class="alignright" />
				<fmtPartNumDtls:message key="DT_QTY_QUOTED" var="varQtyQuote"/> 
				<display:column property="QTYQ" title="${varQtyQuote}"  class="aligncenter" />
				<fmtPartNumDtls:message key="DT_TIME_FRAME" var="varDFrame"/> 
				<display:column property="DFRAME" title="${varDFrame}"  class="aligncenter" />
				<fmtPartNumDtls:message key="DT_QUOTE_ID" var="varQuoteId"/> 
				<display:column property="QID" title="${varQuoteId}"  class="aligncenter" />
				<fmtPartNumDtls:message key="DT_UOM" var="varUOM"/> 
				<display:column property="UOM" title="${varUOM}"  class="aligncenter" />
				<fmtPartNumDtls:message key="DT_UOM_QTY" var="varUOMQty"/> 
				<display:column property="UOMQTY" title="${varUOMQty}"  class="aligncenter" />
				<fmtPartNumDtls:message key="DT_LOCK_FL" var="varLockFl"/> 
				<display:column property="LOCKFL" title="${varLockFl}"  class="aligncenter" />
				<fmtPartNumDtls:message key="LBL_ACTIVE" var="varActive"/> 
				<display:column property="AFL" title="${varActive}"  class="aligncenter" />
			</display:table>
			</td>
		</tr>
    <table>
<%
 	}
 %>
 
 <p> &nbsp;</p>
 
 <table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
	  
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtPartNumDtls:message key="LBL_PARENT_PART_DTLS"/></td>
		</tr>	
		 	
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td colspan="2">
			<display:table name="rdParentPartDetail" requestURI="/GmPartNumServlet"   class="its" id="currentRowObject" decorator="com.globus.operations.displaytag.beans.DTSubPartDetailWrapper"> 
 				<fmtPartNumDtls:message key="LBL_PART_NUM" var="varPartNum"/> 
 				<display:column property="PARTNUM" title="${varPartNum}"   />
 				<fmtPartNumDtls:message key="LBL_PARTDESC" var="varPartDesc"/> 
				<display:column property="DESCRIPTION" title="${varPartDesc}"  class="alignleft" />
				<fmtPartNumDtls:message key="DT_QTY" var="varQty"/>
				<display:column property="QTY" title="${varQty}"  class="alignright" />
				 
			</display:table>
			</td>
		</tr> 
    <table>
    
<p> &nbsp;</p>


<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
	   
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtPartNumDtls:message key="LBL_SUB_PART_DTLS"/></td>
		</tr>	
		 	
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td colspan="2">
			<display:table name="rdSubPartDetail" requestURI="/GmPartNumServlet"   class="its" id="currentRowObject" decorator="com.globus.operations.displaytag.beans.DTSubPartDetailWrapper"> 
 				<fmtPartNumDtls:message key="LBL_PART_NUM" var="varPartNum"/> 
 				<display:column property="PARTNUM" title="${varPartNum}"   />
 				<fmtPartNumDtls:message key="LBL_PARTDESC" var="varPartDesc"/> 
				<display:column property="DESCRIPTION" title="${varPartDesc}"  class="alignleft" />
				<fmtPartNumDtls:message key="LBL_PRJ_ID" var="varPorjID"/> 
				<display:column property="PROJID" title="${varPorjID}"  class="alignleft" />
				<fmtPartNumDtls:message key="LBL_PRJ_NAME" var="varProjName"/> 
				<display:column property="PROJNM" title="${varProjName}"  class="alignleft" />
				<fmtPartNumDtls:message key="LBL_REV_NUM" var="varRevNum"/> 
				<display:column property="REVNUM" title="${varRevNum}"  class="aligncenter" />
				<fmtPartNumDtls:message key="DT_QTY" var="varQty"/>
				<display:column property="QTY" title="${varQty}"  class="alignright" />
				 
			</display:table>
			</td>
		</tr>
    <table>
    <%if(!bolLimitedAccess){ %>
<p> &nbsp;</p>
<TABLE cellSpacing=0 cellPadding=0  border=0 width="500"  >
<TBODY>
	 
	<tr class=Line><td noWrap height=1 ></td></tr>
				 
			 
				 <tr >
						<td ><table ><tr >
							<td   ><a href= javascript:fnSalebyS()><fmtPartNumDtls:message key="HREF_SALE_SPINE_SPEC"/> </a> </td>
							<td   ><a href= javascript:fnSalebyA()><fmtPartNumDtls:message key="HREF_SALE_BY_ACCT"/></a></td>
							<td   ><a href= javascript:fnSalebyMonth()><fmtPartNumDtls:message key="HREF_SALES_CONS"/></a></td> 
							<td  ><a href= javascript:fnMonthPart()><fmtPartNumDtls:message key="HREF_MONTH_PART_TXN"/></a></td>
							<td  ><a href= javascript:fnViewDetails()><fmtPartNumDtls:message key="HREF_CURR_OPN_REQ"/></a></td>
							<td  ><a href= javascript:fnTrend()><fmtPartNumDtls:message key="HREF_TREND"/></a></td>
						</tr></table></td>
					</tr>
				 
			<tr>
			  <td></td>
	</tr>			
	<tr class=Line><td noWrap height=1 colspan=2></td></tr>
</table>	
<%} %>
<TABLE cellSpacing=0 cellPadding=0  border=0 >
<tr><td height="5"></td></tr>
<tr>
<fmtPartNumDtls:message key="BTN_CLOSE" var="varClose"/>
<td align="center">
<gmjsp:button value="&nbsp;${varClose}&nbsp;" style="width: 6em" name="Btn_Close" buttonType="Load" gmClass="button" onClick="window.close();" />
</td>
</tr>
</table> 
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
