<%@page import="com.globus.common.beans.GmResourceBundleBean"%>

 <%
/**********************************************************************************
 * File		 		: GmProjectSetup.jsp
 * Desc		 		: This screen is used for the Part Number frmPartSetuptenance
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
 <!-- \prodmgmnt\GmPartNumberSetup.jsp -->
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ taglib prefix="fmtPartNumberSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartNumberSetup:setLocale value="<%=strLocale%>"/>
<fmtPartNumberSetup:setBundle basename="properties.labels.prodmgmnt.GmPartNumberSetup"/>
<%
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("PART_NUMBER_SETUP"));
	String strhAction = "";
	String strQltyDisabled = "disabled";
	strhAction = (String)request.getAttribute("hAction")==null?"Save":(String)request.getAttribute("hAction");
	String strTest = (String)request.getAttribute("hQltyDisabled");
	log.debug(" Test disable " + strTest);
	strQltyDisabled = (String)request.getAttribute("hQltyDisabled")== null?"disabled":(String)request.getAttribute("hQltyDisabled");
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.prodmgmnt.GmPartNumberSetup", strSessCompanyLocale);
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmLoadDetails = (HashMap)hmReturn.get("LOADDETAILS");

	ArrayList alDevClass = new ArrayList();
	alDevClass = (ArrayList)hmLoadDetails.get("CLASS");
	ArrayList alProdMat = new ArrayList();
	alProdMat = (ArrayList)hmLoadDetails.get("PRODMAT");
	ArrayList alProjList = new ArrayList();
	alProjList = (ArrayList)hmLoadDetails.get("PROJLIST");
	ArrayList alProdFly = new ArrayList();
	alProdFly = (ArrayList)hmLoadDetails.get("PRODFLY");
	ArrayList alMeasDev = new ArrayList();
	alMeasDev = (ArrayList)hmLoadDetails.get("MEASDEV");
	ArrayList alMaterialList = new ArrayList();
	alMaterialList = (ArrayList)hmLoadDetails.get("MATSPEC");
	ArrayList alPackaging = new ArrayList();
	alPackaging = (ArrayList)hmLoadDetails.get("PACK");
	ArrayList alPartStatus = new ArrayList();
	alPartStatus = (ArrayList)hmLoadDetails.get("PARTSTATUS");
	ArrayList alPartCert = new ArrayList();
	alPartCert = (ArrayList)hmLoadDetails.get("PARTCERT");
	ArrayList alSalesGroup = new ArrayList();
	alSalesGroup = (ArrayList)hmLoadDetails.get("SALESGROUP");
	ArrayList alUOM = new ArrayList();
	alUOM= (ArrayList) hmLoadDetails.get("UOM");

	ArrayList alRFS = new ArrayList();
	alRFS= (ArrayList) hmLoadDetails.get("RFS");
	
	ArrayList alPathway = new ArrayList();
	alPathway= (ArrayList) hmLoadDetails.get("PATHWAY");
	
	ArrayList alRPF = new ArrayList();
	alRPF= (ArrayList) hmLoadDetails.get("RPF");
	
	ArrayList alRegulatory = new ArrayList();
	alRegulatory= (ArrayList) hmLoadDetails.get("REGULATORY");
	ArrayList alArtifacts = new ArrayList();
	alArtifacts= GmCommonClass.parseNullArrayList((ArrayList) hmLoadDetails.get("ALARTIFACTS"));	 
	
	ArrayList alStructType = new ArrayList();
	alStructType = (ArrayList)hmLoadDetails.get("STRUCTTYPE");
	ArrayList alPresMethod = new ArrayList();
	alPresMethod= GmCommonClass.parseNullArrayList((ArrayList) hmLoadDetails.get("PRESMETHOD"));	 
	ArrayList alStorTemp = new ArrayList();
	alStorTemp= GmCommonClass.parseNullArrayList((ArrayList) hmLoadDetails.get("STORTEMP"));	
	ArrayList alProSpecType = new ArrayList();
	alProSpecType= GmCommonClass.parseNullArrayList((ArrayList) hmLoadDetails.get("PROSPECTYPE"));
	ArrayList alContrProClient = new ArrayList();
	alContrProClient = GmCommonClass.parseNullArrayList((ArrayList)hmLoadDetails.get("ALACCLIST"));
	ArrayList alWOCreated = new ArrayList();
	ArrayList alUDIEtchRequired = new ArrayList();
	ArrayList alDIOnlyEtchRequired = new ArrayList();
	ArrayList alPIOnlyEtchRequired = new ArrayList();
	ArrayList alVendorDesign = new ArrayList();
	
	alWOCreated= GmCommonClass.parseNullArrayList((ArrayList) hmLoadDetails.get("ALWOCREATED"));
	alUDIEtchRequired= GmCommonClass.parseNullArrayList((ArrayList) hmLoadDetails.get("ALUDIETCHREQUIRED"));
	alDIOnlyEtchRequired= GmCommonClass.parseNullArrayList((ArrayList) hmLoadDetails.get("ALDIONLYETCHREQUIRED"));
	alPIOnlyEtchRequired= GmCommonClass.parseNullArrayList((ArrayList) hmLoadDetails.get("ALPIONLYETCHREQUIRED"));
	alVendorDesign= GmCommonClass.parseNullArrayList((ArrayList) hmLoadDetails.get("ALVENDORDESIGN"));

	int intSize = 0;

	String strCodeID = "";
	HashMap hcboVal = null;
	String strSelected = "";
	String strClassId = "4031";
	String strProjId = "";
	String strProdMat = "";
	String strStructType = "";
	String strProdFly = "";
	String strMeasId = "4080";
	String strProjName = "";
	String strPartNum = "";
	String srtPartDesc = "";
	String strDetailDesc = "";
	String strParentProj = "";
	String strParentProjId = "";
	String strMatSp = "";
	String strDraw = "";
	String strDrawRev = "";
	String strPrimPack = "";
	String strSecPack = "";
	String strInsertId = "";
	String strInsertIssueFl = "";
	String strHardTest = "";
	String strMatCert = "";
	String strCompCert = "";
	String strArtifacts = "";
	String strPresMethod = "";
	String strStorTemp = "";
	String strProSpecType = "";
	String strMastProd = "";
	String strContrProClient = "";
	String strInspectRev = "";
	String strTracImplChk = "";
	String strPartStatus = "20365";
	String strRelForSale = "";
	String strActiveFlag = "";
	String strDCODisable = "";
	String strDCOMessage = "";
	String strSupplyFlag = "";
	String strSubAssemblyFlag = "";
	String strFixedSizeFlag = "checked";
	String strTollerance = "";
	String strTolleranceEnable = "disabled";
	String strSalesGroup = "";
	String strPartStatusMsg = "";
	String strhPartNum = "";
	String strUOM="20511";
	String strRPFId = "";
	String strMinimumRev = "";
	
	String strDiNumberFl = "";
	String strDINumberChk = "";
	String strShelfLife = "";
	String strDIDisabled = "";
	String strWoCreated = "";
	String strUdiEtchReq = "";
	String strDiOnlyReq = "";
	String strPiOnlyReq = "";
	String strVendorDesign = "";

	boolean bolFl = false;
	
	HashMap hmVal = null;
	HashMap hmTemp = null;
	String strRFS = "";
	String strCodeId ="";
	String strCodeGrp ="";
	String strCodeNm ="";
	String strAttrType ="";
	String strAttrValue ="";
	String strShade = ""; 
	String strTaggableFL="";
	String strCreatedCmpy = "";
	String strEditDisable = "";
	String strserialNumFl = "";
	String strLotTrackFl = "";
	String strLotTrckChkEnable = "";
	String strSerialChkEnable = "";
	HashMap hmChkEnableVal = new HashMap();
	String strCompanyId = GmCommonClass.parseNull((String) gmDataStoreVO.getCmpid());
	
	int psize = 0;
	
	ArrayList alTemp = null;
	ArrayList alPathwayDetail = null;
	ArrayList alRegulatoryDtl = null;
	String strPathValue = "";
	HashMap hmPathDtls =null;
	 
	strRFS = GmCommonClass.parseNull((String)request.getAttribute("RFS"));
	alPathwayDetail=  (ArrayList)request.getAttribute("PathwayDetail");
	alRegulatoryDtl=  (ArrayList)request.getAttribute("RegulatoryDetail");
	hmChkEnableVal = GmCommonClass.parseNullHashMap((HashMap) request.getAttribute("chkboxEnable"));
	strLotTrckChkEnable = GmCommonClass.parseNull((String)hmChkEnableVal.get("LOTTRACKCHKFL"));
	strLotTrckChkEnable = strLotTrckChkEnable.equals("N") ? "disabled" : "";
	strSerialChkEnable = GmCommonClass.parseNull((String)hmChkEnableVal.get("SERIALCHKFL"));
	strSerialChkEnable = strSerialChkEnable.equals("N") ? "disabled" : "";
	
	 psize = alPathway.size();
     
     for (int i=0; i< psize; i++)
		  {
			   hmVal = (HashMap)alPathway.get(i);
			   strCodeGrp = (String)hmVal.get("CODENMALT"); 
		       strCodeId = (String)hmVal.get("CODEID");
		       strCodeNm = (String)hmVal.get("CODENM");

		  }
	
	HashMap hmMapRFS = new HashMap();
	hmMapRFS.put("ID","");
	hmMapRFS.put("PID","CODEID");
	hmMapRFS.put("NM","CODENM");	
	
	if (strhAction.equals("Lookup"))
	{
		HashMap hmPartNumDetails = (HashMap)hmReturn.get("PARTNUMDETAILS");
		if (!hmPartNumDetails.isEmpty())
		{
			strPartNum = (String)hmPartNumDetails.get("PARTNUM");
			srtPartDesc = (String)hmPartNumDetails.get("PDESC");
			strDetailDesc = (String)hmPartNumDetails.get("DETAILDESC");
			strParentProjId = (String)hmPartNumDetails.get("PROJID");
			strProdFly = GmCommonClass.parseNull((String)hmPartNumDetails.get("PFLY"));
			strProdMat = GmCommonClass.parseNull((String)hmPartNumDetails.get("PMAT"));
			strClassId = GmCommonClass.parseNull((String)hmPartNumDetails.get("PCLASS"));
			strMeasId = GmCommonClass.parseNull((String)hmPartNumDetails.get("MDEV"));
			strMatSp = GmCommonClass.parseNull((String)hmPartNumDetails.get("MSPEC"));
			strDraw = GmCommonClass.parseNull((String)hmPartNumDetails.get("DRAW"));
			strDrawRev = GmCommonClass.parseNull((String)hmPartNumDetails.get("REV"));
			strPrimPack = GmCommonClass.parseNull((String)hmPartNumDetails.get("PRIM"));
			strSecPack = GmCommonClass.parseNull((String)hmPartNumDetails.get("SEC"));
			strInsertId = GmCommonClass.parseNull((String)hmPartNumDetails.get("INSERTID"));
			strInsertIssueFl = GmCommonClass.parseNull((String)hmPartNumDetails.get("INSERTFL"));
			strMatCert = GmCommonClass.parseNull((String)hmPartNumDetails.get("MATCERT"));
			strHardTest = GmCommonClass.parseNull((String)hmPartNumDetails.get("HARDTEST"));
			strCompCert = GmCommonClass.parseNull((String)hmPartNumDetails.get("COMPCERT"));
			strArtifacts = GmCommonClass.parseNull((String)hmPartNumDetails.get("ARTIFACTS"));
			strInspectRev = GmCommonClass.parseNull((String)hmPartNumDetails.get("INSPREV"));
			strTracImplChk = GmCommonClass.parseNull((String)hmPartNumDetails.get("TRACIMPL")).equals("Y")?"checked":"";
			strPartStatus = GmCommonClass.parseNull((String)hmPartNumDetails.get("PARTSTATUS"));
		//	strRelForSale = GmCommonClass.parseNull((String)hmPartNumDetails.get("RELFORSALE")).equals("")?"":"checked";
			strActiveFlag = GmCommonClass.parseNull((String)hmPartNumDetails.get("AFL")).equals("")?"":"checked";
			strSupplyFlag = GmCommonClass.parseNull((String)hmPartNumDetails.get("SUPPLYFL")).equals("")?"":"checked";
			strSubAssemblyFlag = GmCommonClass.parseNull((String)hmPartNumDetails.get("SUBASSMFL")).equals("")?"":"checked";
			strSalesGroup = GmCommonClass.parseNull((String)hmPartNumDetails.get("SALESGROUP"));
			strPartStatusMsg = GmCommonClass.parseNull((String)hmPartNumDetails.get("STATUSMSG"));
			strUOM = GmCommonClass.parseNull((String)hmPartNumDetails.get("UOM"));
			
			strRPFId = GmCommonClass.parseNull((String)hmPartNumDetails.get("RPF"));
			strTaggableFL = GmCommonClass.parseNull((String)hmPartNumDetails.get("TAGGABLEFL")).equals("on")?"checked":"";
			strMinimumRev = GmCommonClass.parseNull((String)hmPartNumDetails.get("MINACCPREV"));
			strShelfLife = GmCommonClass.parseNull((String)hmPartNumDetails.get("SHELFLIFE"));
			strStructType = GmCommonClass.parseNull((String)hmPartNumDetails.get("STRCTYPE"));
			strPresMethod = GmCommonClass.parseNull((String)hmPartNumDetails.get("PRESERVMETHOD"));
			strStorTemp = GmCommonClass.parseNull((String)hmPartNumDetails.get("STORETEMP"));
			strProSpecType = GmCommonClass.parseNull((String)hmPartNumDetails.get("PROCSPECTYPE"));
			strMastProd = GmCommonClass.parseNull((String)hmPartNumDetails.get("MASTPROD"));
			strContrProClient = GmCommonClass.parseNull((String)hmPartNumDetails.get("CONTPROCLNT"));	
			strFixedSizeFlag = GmCommonClass.parseNull((String)hmPartNumDetails.get("FIXEDSIZE")).equals("N")?"":"checked";
			strTolleranceEnable = strFixedSizeFlag.equalsIgnoreCase("checked")?strTolleranceEnable:"";
			strTollerance = GmCommonClass.parseNull((String)hmPartNumDetails.get("TOLLERANCE"));
			strCreatedCmpy = GmCommonClass.parseNull((String)hmPartNumDetails.get("CREATEDCMPNY"));
			strLotTrackFl = GmCommonClass.parseNull((String)hmPartNumDetails.get("LOTTRACKFL")).equals("Y")?"checked=checked":"";
			strserialNumFl = GmCommonClass.parseNull((String)hmPartNumDetails.get("SERIALNUMNEEDFL")).equals("Y")?"checked=checked":"";
			
			strProjId = (String)request.getAttribute("hProjId")==null?"":(String)request.getAttribute("hProjId");
			bolFl = true;
			strhPartNum = strPartNum;
		}
		 

		if (!(strPartStatus.equals("20365") || strPartStatus.equals("20366") || strPartStatus.equals("")) && !strQltyDisabled.equals("Enable"))
		{
			strDCODisable = "disabled";
			strDCOMessage = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CURRENY_STATUS"))+ strPartStatusMsg + GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PLEASE_CONTACT"));
		}
		
		if(strQltyDisabled.equals("disabled")){
			strDCODisable = "disabled";	
		}
		
		if(!strCreatedCmpy.equals("") && !strCompanyId.equals(strCreatedCmpy)){
		 	strEditDisable = "true";
		}
		//PC-3259 - Enable lot track automatically from part setup
		if((strClassId.equals("4030")) && (strProdMat.equals("100845"))){
			//if(strLotTrackFl.equals("checked=checked")){
				strLotTrckChkEnable ="disabled";
				strLotTrackFl ="checked=checked";
			//}
		}   
	}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Number Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="javascript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="javascript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strProdMgmntJsPath%>/GmPartNumberSetup.js"></script>

<script>
var strSerialChkBox = '<%=strserialNumFl%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" >
<FORM name="frmPartSetup" method="POST" action="<%=strServletPath%>/GmPartNumServlet">
<input type="hidden" name="hActionForVali" value="<%=strhAction%>"> <!-- used for validating comments update  -->
<input type="hidden" name="hParentProjId" value="<%=strParentProjId%>">
<input type="hidden" name="hMatCert" value="<%=strMatCert%>">
<input type="hidden" name="hCompCert" value="<%=strCompCert%>">
<input type="hidden" name="PartNum" value="">
<input type="hidden" name="hMode" value=""> <!-- used for GmPartPriceEditServlet -- part pricing button -->
<input type="hidden" name="hId" value=""> <!-- used for GmPartPriceEditServlet -- part pricing button -->
<input type="hidden" name="hType" value="pnum"> <!-- used for GmPartNumMapServlet -- part mapping button pnum is for part -->

<input type="hidden" name="hChkRFS" value="<%=strRFS%>"> 
<input type="hidden" name="hPathway" value=""> 
<input type="hidden" name="hPartAttribute" value="">
<input type="hidden" name="hDINumberFl" value="<%=strDiNumberFl%>">

	<table border="0" width="947px" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td height="100" valign="top">
				<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
				
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr><td colspan="2">
					<table border="0" cellspacing="0" cellpadding="0" width="100%">
					<tr  class=ShadeBlueBk>
						<td class="RightTableCaption" align="right" height="24"><font color="red">*</font><fmtPartNumberSetup:message key="LBL_PRI_PROJECT"/>:&nbsp;</td>
						<td><select name="Cbo_Proj" <%=strDCODisable%> class="RightText" onChange="javascript:fnSetProjNum(this)">
								<option value="0" selected><fmtPartNumberSetup:message key="LBL_CHOOSE_ONE"/>
<%
			  		intSize = alProjList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alProjList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strProjId.equals(strCodeID)?"selected":"";
						strProjName = GmCommonClass.getStringWithTM((String)hcboVal.get("NAME"));
						
						if (strParentProjId.equals(strCodeID))
						{
							strParentProj = GmCommonClass.getStringWithTM((String)hcboVal.get("NAME"));
							strSelected = strParentProjId.equals(strCodeID)?"selected":"";
						}
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=strCodeID%> / <%=strProjName%></option>
<%
			  		}

%>
							</select>
						</td>
					</tr>
					
					
<% if (strDCODisable.equals("disabled")) { %>					
					<tr>
						<td colspan="2" class="RightTextRed" align="center">&nbsp;
							<%=strDCOMessage%>
						</td>	
					</tr>	
<% } %>	
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
<%
	if (strhAction.equals("Lookup") && bolFl)
	{	
%>
					<tr>
						<td height="24" align="right" class="RightTableCaption"><fmtPartNumberSetup:message key="LBL_PAR_PROJ"/>:&nbsp;</td>
						<td class="RightTextBlue" id="parproj">
						<%=strParentProj%></td>
					<tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
<%}%>
					<tr>
						<td colspan="2">
						  <DIV id="PerfDimDiv" style="display:none;">
							<table width="100%" border="0" cellspacing="0">
								<tr><td class="Line" colspan="6"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td  HEIGHT="24" colspan="6"><b><fmtPartNumberSetup:message key="LBL_PART_CREATOR"/></b></td>
								</tr>
								<tr>
									<td class="RightText"><fmtPartNumberSetup:message key="LBL_PART_TYPE"/> <br>
										<SELECT name="Cbo_Field1" class="InputArea">
											<OPTION value="1"><fmtPartNumberSetup:message key="LBL_TITANIUM_IMPLANTS"/>
											<OPTION value="2"><fmtPartNumberSetup:message key="LBL_STAINLESS_STEEL_IMPLANTS"/>
											<OPTION value="3"><fmtPartNumberSetup:message key="LBL_PEEK_IMPLANTS"/>
											<OPTION value="4"><fmtPartNumberSetup:message key="LBL_POLYMER_IMPLANTS"/>
											<OPTION value="5"><fmtPartNumberSetup:message key="LBL_CO_CR_MB"/>
											<OPTION value="6"><fmtPartNumberSetup:message key="LBL_INSTRUMENTS"/>
											<OPTION value="9"><fmtPartNumberSetup:message key="LBL_GRPHIC_CASES"/>
										</SELECT>
									</td>
									<td class="RightText"><fmtPartNumberSetup:message key="LBL_PROJECT_NUM"/>:<br>
										<input class="RightText" readonly type="text" name="Txt_Field2" size="3" value="">
									</td>
									<td class="RightText"><fmtPartNumberSetup:message key="LBL_FAMILY_NUMBER"/>:<br>
										<SELECT name="Cbo_Field3" class="InputArea">
											<OPTION value="0">0
											<OPTION value="1">1
											<OPTION value="2">2
											<OPTION value="3">3
											<OPTION value="4">4
											<OPTION value="5">5
											<OPTION value="6">6
											<OPTION value="7">7
											<OPTION value="8">8
											<OPTION value="9">9
										</SELECT>
									</td>
									<td class="RightText"><fmtPartNumberSetup:message key="LBL_SIZE"/>:<br>
										<input class="RightText" type="text" name="Txt_Field4" size="3" maxlength="2" value="">
									</td>
									<td>
										<fmtPartNumberSetup:message key="LBL_SAVE" var="varSave"/>
										<gmjsp:button  gmClass="InputArea" name="Btn_Create" value="Create" buttonType="${varSave}" onClick ="fnCreatePart(this.form)"   />
									</td>
									<td width="10%"></td>
								</tr>
								<tr><td class="Line" colspan="6"></td></tr>
							</table>
						 </DIV>
						</td>
					</tr>

					<tr>
						<td class="RightTableCaption" align="right" height="60"> <font color="red">*</font><fmtPartNumberSetup:message key="LBL_PART_DESC"/>: &nbsp;</td>
						<td><TEXTAREA type="text" rows=3 cols=50 <%=strDCODisable%> name="Txt_PartDesc"><%=srtPartDesc%></TEXTAREA></td>
					</tr>
 
					<tr><td colspan="2" class="LLine"></td></tr> 
                    <tr class="Shade">
                    	<td height="320" class="RightTableCaption" align="right" ><fmtPartNumberSetup:message key="LBL_DET_DESC"/>:&nbsp;</td>
						<td><TEXTAREA type="text" class="HtmlTextArea" rows=3 cols=50 <%=strDCODisable%> name="Txt_DetailDesc"><%=strDetailDesc%></TEXTAREA></td>	
                    </tr>
                    </table></td></tr>
					<tr><td colspan="2" bgcolor="#666666" height="1"></td></tr>                    
					<tr>
						<td colspan="2">
							<table width="100%" border="0" cellspacing="0">
									<tr class="ShadeRightTableCaption">
										<td Height="20" colspan="4">&nbsp;<fmtPartNumberSetup:message key="LBL_QA"/></td>
									</tr>
								<tr><td colspan="4" bgcolor="#666666" height="1"></td></tr>
								<tr class="Shade">
									<td Height="20" class="RightTableCaption" colspan="4">&nbsp;<IMG id="partdtlsimg" border=0 src="<%=strImagePath%>/minus.gif">&nbsp;<a href="javascript:fnShowFilters('partdtls');" tabindex="-1" class="RightText"><fmtPartNumberSetup:message key="LBL_PART_DETAILS"/></a></td>
								</tr>
								<tr><td colspan="4" bgcolor="#666666" height="1"></td></tr>
								<tr><td colspan="4">
								<table border="0" width="100%" cellspacing="0" cellpadding="0" id="partdtls"  >
								<tr class=ShadeBlueBk>
									<td class="RightTableCaption" align="right" height="24"> <font color="red">*</font> <fmtPartNumberSetup:message key="LBL_FAMILY"/>:</td>
									<td>&nbsp;<gmjsp:dropdown controlName="Cbo_ProdFly"  seletedValue="<%=strProdFly%>" disabled="<%=strDCODisable%>"	
											value="<%=alProdFly%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" onChange="fnChangeFamily(this.form,this.value);" />
									</td>
									<td class="RightTableCaption" align="right" height="24"> <font color="red">*</font><fmtPartNumberSetup:message key="LBL_STERILITY"/>: </td>
									<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Class"  seletedValue="<%=strClassId%>" disabled="<%=strDCODisable%>"	
										value="<%=alDevClass%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"  onChange="javascript:fnSterileChange(this.form)"/>
									</td>
								</tr>							
								<tr><td class="LLine" height="1" colspan="4"></td></tr>
								<tr>
									<td class="RightTableCaption" align="right" height="24"> <font color="red">*</font><fmtPartNumberSetup:message key="LBL_MATERIAL"/>: </td>
									<td>&nbsp;<gmjsp:dropdown controlName="Cbo_ProdMat"  seletedValue="<%=strProdMat%>" disabled="<%=strDCODisable%>"	
										value="<%=alProdMat%>" codeId = "CODEID" onChange="javascript:fnSetDefaults(this.form)"  codeName = "CODENM" defaultValue= "[Choose One]"/>
									</td>		
									<td class="RightTableCaption" align="right" height="24"><fmtPartNumberSetup:message key="LBL_STRU_TYPE"/>:</td>
									<td>&nbsp;<gmjsp:dropdown controlName="Cbo_StrucType"  seletedValue="<%=strStructType%>" disabled="<%=strDCODisable%>"	
										value="<%=alStructType%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"/>
									</td>							
								</tr>
									
								<tr><td class="LLine" height="1" colspan="4"></td></tr>
								<tr class=ShadeBlueBk>
								<td class="RightTableCaption" align="right" HEIGHT="24"> <font color="red">*</font><fmtPartNumberSetup:message key="LBL_MATER_SPEC"/>:</td>
									<td colspan="3">&nbsp;<gmjsp:dropdown controlName="Cbo_Mat"  seletedValue="<%=strMatSp%>" disabled="<%=strDCODisable%>"	
										value="<%=alMaterialList%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"/>
									</td>
									
									</tr>						
								 
								<tr><td class="LLine" height="1" colspan="4"></td></tr>
								<tr>
									<td class="RightTableCaption" align="right" height="24"> <font color="red">*</font><fmtPartNumberSetup:message key="LBL_MEASU_DEVICE"/>: </td>
									<td>&nbsp;<gmjsp:dropdown controlName="Cbo_MesDev"  seletedValue="<%=strMeasId%>" disabled="<%=strDCODisable%>" 	
										value="<%=alMeasDev%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"/>
									</td>
									<td class="RightTableCaption" width="200" align="right" HEIGHT="24"> <font color="red">*</font><fmtPartNumberSetup:message key="LBL_INSPEC_SHEET_REV"/>: </td>
									<td>&nbsp;<input type="text" size="3" value="<%=strInspectRev%>" name="Txt_InspRev" class="InputArea" <%=strDCODisable%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
									</td>
								</tr>
								<tr><td class="LLine" height="1" colspan="4"></td></tr>
								<tr class=ShadeBlueBk>
									<td class="RightTableCaption" width="200" align="right" HEIGHT="24"> <font color="red">*</font><fmtPartNumberSetup:message key="LBL_DRAW_NUM"/>: </td>
									<td>&nbsp;<input type="text" size="10" value="<%=strDraw%>" name="Txt_Draw" class="InputArea" <%=strDCODisable%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
									</td>
									<td class="RightTableCaption" width="200" align="right" HEIGHT="24"> <font color="red">*</font><fmtPartNumberSetup:message key="LBL_DRAW_REV"/>: </td>
									<td>&nbsp;<input type="text" size="3" value="<%=strDrawRev%>" name="Txt_Rev" class="InputArea" <%=strDCODisable%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
									</td>
								</tr>
								<tr><td class="LLine" height="1" colspan="4"></td></tr>
								<tr>
									<td class="RightTableCaption" width="200" align="right" HEIGHT="24"><font color="red">*</font><fmtPartNumberSetup:message key="LBL_UOM"/>: </td>
									<td>&nbsp;<gmjsp:dropdown controlName="Cbo_UOM"  seletedValue="<%=strUOM%>" disabled="<%=strQltyDisabled%>"	
										value="<%=alUOM%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"/>
									</td>
										<td class="RightTableCaption" width="200" align="right" HEIGHT="24"> <fmtPartNumberSetup:message key="LBL_M_A_REC_LEVEL"/>: </td>
									<td>&nbsp;<input type="text" size="3" value="<%=strMinimumRev%>" name="Txt_MinAccpRev" class="InputArea" <%=strDCODisable%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
									</td>
								</tr>	
								
								<tr><td class="LLine" height="1" colspan="4"></td></tr>
								<tr class=ShadeBlueBk>
									<td class="RightTableCaption" width="200" align="right" HEIGHT="24"> <font color="red">*</font> <fmtPartNumberSetup:message key="LBL_INSERT_ID"/>:</td>
									<td>&nbsp;<input  style="vertical-align: middle;" type="text" size="30" value="<%=strInsertId%>" name="Txt_InsertId" class="InputArea" <%=strDCODisable%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
									 <% if(strInsertIssueFl.equalsIgnoreCase("N")) {%>
									 	<span id="DivValidInserts"> <fmtPartNumberSetup:message key="LBL_VALID_INSERTS" var="varValidInsert"/><img style="padding:0px; vertical-align: middle;" height="20" width="22" title="${varValidInsert}" src="<%=strImagePath%>/success.gif"></img></span>
									 <%} else {%>
              						 	<span id="DivInValidInserts"><fmtPartNumberSetup:message key="LBL_INVALID_INSERTS" var="varInValidInsert"/><img style="padding:0px; vertical-align: middle;" title="${varInValidInsert}" src="<%=strImagePath%>/delete.gif"></img></span>
              						 <%} %>
              						 </td>	
									<td class="RightTableCaption" width="200" align="right" HEIGHT="24"><fmtPartNumberSetup:message key="LBL_TRACK_IMPLANT"/>:</td>
									<td>&nbsp;<input type="checkbox" <%=strTracImplChk%> name="Chk_TracImpl" <%=strDCODisable%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
									</td>
								</tr>	
								
								<tr><td class="LLine" height="1" colspan="4"></td></tr>
								
								<tr>
									<td class="RightTableCaption" align="right" height="24"> <font color="red">*</font><fmtPartNumberSetup:message key="LBL_SALES_GROUP"/>: </td>
									<td>&nbsp;<gmjsp:dropdown controlName="Cbo_SalesGroup"  seletedValue="<%=strSalesGroup%>" disabled="<%=strDCODisable%>"	
										value="<%=alSalesGroup%>" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]"/>
									</td>	
									<td class="RightTableCaption" width="200" align="right" HEIGHT="24"><fmtPartNumberSetup:message key="LBL_SUPPLY_FLAG"/>:</td>
									<td>&nbsp;<input type="checkbox" <%=strSupplyFlag%> name="Chk_SupplyFl" <%=strDCODisable%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
									</td>
								</tr>
								
								<tr><td class="LLine" height="1" colspan="4"></td></tr>
								<tr class=ShadeBlueBk>
									<td class="RightTableCaption" width="200" align="right" HEIGHT="24"><fmtPartNumberSetup:message key="LBL_SUB_ASS_FLAG"/>:</td> 
									<td>&nbsp;<input type="checkbox" <%=strSubAssemblyFlag%> name="Chk_SubAssemblyFl" <%=strDCODisable%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
									</td>	
									<td class="RightTableCaption" width="200" align="right" HEIGHT="24"><fmtPartNumberSetup:message key="LBL_TAG_PART"/>:</td>
									<td>&nbsp;<input type="checkbox" <%=strTaggableFL%> name="Chk_TaggablePartFl" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"  >
									</td>
								</tr>
								<tr><td class="LLine" height="1" colspan="4"></td></tr>
								<tr>
									<td class="RightTableCaption" align="right" height="24"><fmtPartNumberSetup:message key="LBL_ARTIFACTS"/>:</td>
									<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Artifacts"  seletedValue="<%=strArtifacts%>" disabled="<%=strDCODisable%>"	
										value="<%=alArtifacts%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"/>
									</td>
									<td class="RightTableCaption" align="right" height="24"><fmtPartNumberSetup:message key="LBL_SHELF_LIFE"/>:</td>
									<td>&nbsp;<input type="text" size="4" value="<%=strShelfLife%>" name="Txt_ShelfLife" class="InputArea" <%=strDCODisable%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >&nbsp;months
									</td>
								</tr>
								<tr><td class="LLine" height="1" colspan="4"></td></tr>
								<tr class=ShadeBlueBk>
									<td class="RightTableCaption" align="right" height="24">&nbsp;<fmtPartNumberSetup:message key="LBL_PRESER_METHOD"/>:</td>
									<td>&nbsp;<gmjsp:dropdown controlName="Cbo_PresMethod"  seletedValue="<%=strPresMethod%>" disabled="<%=strDCODisable%>"	
										value="<%=alPresMethod%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"/>
									</td>
									<td class="RightTableCaption" width="200" align="right" HEIGHT="24"> <fmtPartNumberSetup:message key="LBL_STR_TEMP"/>:</td>
									<td>&nbsp;<gmjsp:dropdown controlName="Cbo_StorTemp"  seletedValue="<%=strStorTemp%>" disabled="<%=strDCODisable%>"	
										value="<%=alStorTemp%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"/>
									</td>
								<tr>
								<tr><td class="LLine" height="1" colspan="4"></td></tr>
								<tr>
									<%--<td class="RightTableCaption" align="right" height="24">Processing Spec Type:</td>
									<td>&nbsp;<gmjsp:dropdown controlName="Cbo_ProSpecType"  seletedValue="<%=strProSpecType%>" disabled="<%=strDCODisable%>"	
										value="<%=alProSpecType%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]"/>
									</td> --%>
									<td class="RightTableCaption" align="right" height="24"><fmtPartNumberSetup:message key="LBL_TOLERENCE"/>:</td>
									<td>&nbsp;<input type="text" size="20" value="<%=strTollerance%>" <%=strTolleranceEnable%> name="Txt_Tollernace" class="InputArea" <%=strDCODisable%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >						        									 
				        			</td>
									<td class="RightTableCaption" width="200" align="right" HEIGHT="24"> <fmtPartNumberSetup:message key="LBL_MASTER_PRODUCT"/>:</td>
									<td>&nbsp; <input type="text" size="20" value="<%=strMastProd%>" name="Txt_MastProd" class="InputArea" <%=strDCODisable%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');fnValidateProduct(this.value);" >										        									
				        			</td>
								</tr>
								<tr><td class="LLine" height="1" colspan="4"></td></tr>
								<tr class="Shade">
									<fmtPartNumberSetup:message key="LBL_LOTTRK_REQ" var="varLotTrackNeeded"/>
									<td align="right" class="RightTableCaption" HEIGHT="24"><gmjsp:label
											type="RegularText" SFLblControlName=" ${varLotTrackNeeded}?:" td="false" /></td>
									<td class="RightTableCaption">&nbsp;<input type="checkbox" <%=strLotTrckChkEnable %> name="lotTrackFl" id="lotTrackFl" <%=strLotTrackFl%>></td>
									<fmtPartNumberSetup:message key="LBL_SERIAL_NEEDED" var="varSerialNumberNeeded"/>
									<td align="right" class="RightTableCaption" HEIGHT="24"><gmjsp:label
											type="RegularText" SFLblControlName=" ${varSerialNumberNeeded}?:" td="false" /></td>
									<td class="RightTableCaption">&nbsp;<input type="checkbox" <%=strSerialChkEnable %> name="serialNumFl" id="serialNumFl" <%=strserialNumFl%>></td>
								</tr>
								<tr><td class="LLine" height="1" colspan="4"></td></tr>
								<tr>
									   <!-- Contract Processing Client section is moved to Label parameters -->		
									   <%-- <td class="RightTableCaption" align="right" height="24"> Contract Processing Client:</td>
											<td>&nbsp;<gmjsp:autolist comboType="DhtmlXCombo" controlName="Cbo_ContProClnt" seletedValue="<%=strContrProClient%>" value="<%=alContrProClient%>" codeId="ID" codeName="NAME" defaultValue=" [Choose One]" tabIndex="1" width="300"/></td> --%>
				        			<td class="RightTableCaption" width="200" align="right" HEIGHT="24"><fmtPartNumberSetup:message key="LBL_IS_FIXED_SIZE"/>?</td> 
									<td>&nbsp;<input type="checkbox" <%=strFixedSizeFlag%> name="Chk_FixedSizeFl" <%=strDCODisable%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"></td>
									<td colspan="2"></td>
								</tr>
								</table>
								</td></tr>
							
								<tr><td colspan="4" bgcolor="#666666" height="1"></td></tr>
								<tr class="Shade">
									<td Height="20" class="RightTableCaption" colspan="4">&nbsp;<IMG id="pathwayimg" border=0 src="<%=strImagePath%>/minus.gif">&nbsp;<a href="javascript:fnShowFilters('pathway');" tabindex="-1" class="RightText"><fmtPartNumberSetup:message key="LBL_STATUS"/>:</a></td>
								</tr>
								<tr><td colspan="4" bgcolor="#666666" height="1"></td></tr>
								
								<tr><td colspan="4">
								<table border="0" width="100%" cellspacing="0" cellpadding="0" id="pathway"  >  
								<tr>
									<td class="RightTableCaption" align="right" height="24"> <font color="red">*</font><fmtPartNumberSetup:message key="LBL_STATUS"/>: </td>
									<td>&nbsp;<gmjsp:dropdown controlName="Cbo_PartStatus"  seletedValue="<%=strPartStatus%>" disabled="<%=strQltyDisabled%>" onChange="javascript:fnValidateActivefl(this.form)" 	
										value="<%=alPartStatus%>" codeId = "CODEID"  codeName = "CODENM"/>
									</td>
									<td colspan="2">&nbsp;</td>
								</tr>
								<tr><td class="LLine" height="1" colspan="4"></td></tr>
								
								<tr class=ShadeBlueBk>
									<td class="RightTableCaption" align="right" height="24"><fmtPartNumberSetup:message key="LBL_ACTIVE"/>:</td>
									<td>&nbsp;<input type="checkbox" <%=strActiveFlag%> <%=strQltyDisabled%> value="" name="Txt_AFl" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" ></td> 
									<td class="RightTableCaption" width="200" align="right" height="24"><fmtPartNumberSetup:message key="LBL_REL_FOR_SALE"/>:</td> 
									<td>&nbsp;<%=GmCommonControls.getChkBoxGroup("",alRFS,"RFS",hmMapRFS,"CODENM")%>						 	 
									</td>
								</tr>
								</table>
								</td></tr>							
								</table>
						</td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr> 
						<td colspan="2" align="center" height="30">&nbsp;
	                    	<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="LogType" value="" />
							<jsp:param name="LogMode" value="Edit" />
							</jsp:include>	
						</td>
					</tr>
					<%
								String strDisableBtnVal =strDCODisable;
							if(strDisableBtnVal.equals("disabled"))
							{
								strDisableBtnVal = "true";
							}
							
							if(strDisableBtnVal.equalsIgnoreCase("true") || strEditDisable.equalsIgnoreCase("true")){
							  strEditDisable = "true";
							}
					%>
					<tr><td colspan="2" height="10"></td></tr>
				   <tr>
				   
				   		 <fmtPartNumberSetup:message key="BTN_SUBMIT" var="varSubmit"/>
						 <fmtPartNumberSetup:message key="BTN_RESET" var="varReset"/>
						 <fmtPartNumberSetup:message key="BTN_PART_MAPPING" var="varPartMap"/>
						 <fmtPartNumberSetup:message key="BTN_PART_PRICING" var="varPartPrice"/>
						<td colspan="2" align="center" height="24">&nbsp;
						<gmjsp:button value="${varSubmit}" name="Btn_Submit" gmClass="Button" buttonType="Save"  disabled="<%=strEditDisable%>" onClick="javascript:fnSubmit(this.form);" />&nbsp;
						<gmjsp:button value="${varReset}" gmClass="Button" buttonType="Save"  disabled="<%=strDisableBtnVal%>"  onClick="javascript:fnReset(this.form);" />&nbsp;
						<gmjsp:button value="${varPartMap}" gmClass="Button" buttonType="Save" onClick="javascript:fnPartMapping(this.form);" />&nbsp;		
						<gmjsp:button value="${varPartPrice}" gmClass="Button" buttonType="Save" disabled="<%=strDisableBtnVal%>" onClick="javascript:fnPartPricing(this.form);" />&nbsp;
						
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
    </table>
</FORM>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
